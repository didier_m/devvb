VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmFileExport 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8295
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5925
   Icon            =   "frmFileExport.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8295
   ScaleWidth      =   5925
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdExport 
      Height          =   1455
      Left            =   0
      Picture         =   "frmFileExport.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   6840
      Width           =   5895
   End
   Begin VB.Frame Frame3 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   975
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   5895
      Begin VB.CheckBox Check1 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   4695
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   285
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   5655
      End
   End
   Begin VB.Frame Frame1 
      Height          =   5775
      Left            =   0
      TabIndex        =   0
      Top             =   960
      Width           =   5895
      Begin MSComctlLib.ListView ListView1 
         Height          =   5580
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   5655
         _ExtentX        =   9975
         _ExtentY        =   9843
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
End
Attribute VB_Name = "frmFileExport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Crypt As Integer
Dim NomFileToExport As String

Private Sub cmdExport_Click()

'---> On lance l'export
If Me.Check1.Value = 1 Then
    Crypt = 1
Else
    Crypt = 0
End If

'-> Export
ExportData Crypt

End Sub

Private Sub Form_Load()

'---> On genere un nom de fichier ascii pour l'export de la forme : _
PATH et UCID-IDENT-AAAAMMJJ.EXPORT
    
'-> Messprog
Me.Caption = GetMesseprog("CONSOLE", "53")

'-> Libelle
Me.Frame3.Caption = GetMesseprog("CONSOLE", "51")
Me.Check1.Caption = GetMesseprog("CONSOLE", "52")
Me.cmdExport.Caption = GetMesseprog("CONSOLE", "54")

'-> Nom du fichier sans le path
NomFileToExport = aConnexion.Uc_Id & "-" & aConnexion.Ident & "-" & Format(Now, "YYYYMMDD") & "-EXP.DATA"

'-> On genere un nom de fichier ASCII de replication
Me.Text1.Text = aConnexion.MainDir & "Replica\Ouput\" & NomFileToExport
    
    
End Sub

Private Sub ListView1_ItemCheck(ByVal Item As MSComctlLib.ListItem)
'--> Choix d'un fichier � exporter : donne le choix de l'index d'export
Dim Ligne As String
Dim OrdreOk As Boolean
Dim aBuffer As B3DBuffer

On Error GoTo GestError

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

Set Me.ListView1.SelectedItem = Item

'-> Ligne de cde B3D
Ligne = "B3D_SCHEMA_DES~1~0~" & Trim(Me.ListView1.SelectedItem)

'-> Initialisation pour demande
If Not aConnexion.InitDmd(True) Then
    '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
    MsgBox GetMesseprog("APPLIC", "51"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

'-> On envoie une demande de recup des fichiers du catalogue
'-> On lance la fonction d'analyse des locks brul�es
OrdreOk = aConnexion.PutInstruction(Ligne, True)

'-> Verifie si on a bien envoy� l'instruction
If Not OrdreOk Then
    '-> Positionner le code Erreur
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    GoTo GestError
End If

'-> On envoie l'ordre de lecture des instructions
Set aBuffer = New B3DBuffer
If Not aConnexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    aConnexion.ErrorNumber = "100000"
    aConnexion.ErrorLibel = "Error sending exchange"
    aConnexion.ErrorParam = "LoadAgentDOG"
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    GoTo GestError
End If

'-> Recup de la 1ere ligne
Ligne = aBuffer.Lignes(1)

If Entry(2, Ligne, "~") <> "OK" Then
    MsgBox GetMesseprog("CONSOLE", "55"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

'-> D�bloquer
Screen.MousePointer = 0
Me.Enabled = True

'-> affiche le schema des fichiers selectionnes dans le listview de visualisation
LoadSchemaInListView aBuffer

GestError:
    
    Screen.MousePointer = 0
    Me.Enabled = True
    

End Sub

Private Sub LoadSchemaInListView(aBuffer As B3DBuffer)

'---> Permet de charger un listview avec le schema de fichiers selectionn�s
Dim i As Integer
Dim j As Integer
Dim Fonction As String
Dim Ligne As String
Dim aIt As ListItem

On Error GoTo GestError

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> On nettoie le listview
frmChoix.ListView1.ColumnHeaders.Clear

'-> On cree les entete de colonnes du listview
'-> On cree les entetes de colonnes
frmChoix.ListView1.ColumnHeaders.Add , , GetMesseprog("CONSOLE", "56")
frmChoix.ListView1.ColumnHeaders.Add , , GetMesseprog("FRMSCHEMA", "4")

'-> On parcours les lignes retourn�es
For i = 2 To aBuffer.Lignes.Count

    '-> Recup de la ligne
    Fonction = Entry(1, aBuffer.Lignes(i), "~")
    Ligne = Entry(2, aBuffer.Lignes(i), "~")
    Fonction = Entry(3, Fonction, "_")
    
    '-> Selon la fonction que l'on traite
    Select Case Fonction
        Case "FNAME"
            '-> teste si index direct : on sort
            If Entry(6, Ligne, "�") <> "" Then
                '-> Formattage du listview
                Unload frmChoix
                GoTo GestError
            End If
        Case "INAME"
            '-> On cree une ligne pour la data en cours
            Set aIt = frmChoix.ListView1.ListItems.Add(, , Entry(2, Ligne, "�"))
            aIt.SubItems(1) = Entry(4, Ligne, "�")
    End Select

Next

'-> Formattage du listview
FormatListView frmChoix.ListView1

'-> Vider la variable
strRetour = ""

'-> Refresh du listview
Screen.MousePointer = 0
frmChoix.Show vbModal

'-> Tester le retour
If Trim(strRetour) = "" Then GoTo GestError

'-> recup de l'index et affichage dans le list
Me.ListView1.SelectedItem.SubItems(2) = strRetour

'-> Vider la variable de retour
strRetour = ""

GestError:
    Me.Enabled = True
    Screen.MousePointer = 0

End Sub


Private Sub ExportData(Crypt As Integer)
'---> permet d'exporter les datas dans un fichier ascii
Dim i As Integer
Dim OrdreOk As Boolean
Dim aBuffer As B3DBuffer
Dim Ligne As String
Dim hdlWait As Long
Dim Temp As String

On Error GoTo GestError

'-> Bloquer l'�cran
Screen.MousePointer = 11
Me.Enabled = False

For i = 1 To Me.ListView1.ListItems.Count
    
    '-> teste si fichier selectionn�
    If Me.ListView1.ListItems(i).Checked = False Then GoTo NextFichier
    
    '-> Initialisation pour demande
    If Not aConnexion.InitDmd(True) Then
        '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
        MsgBox GetMesseprog("APPLIC", "51"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
        GoTo GestError
    End If
    
    '-> Debut tempo
    frmWait.Label1 = GetMesseprog("CONSOLE", "58") & Me.ListView1.ListItems(i).Text & " " & Me.ListView1.ListItems(i).SubItems(1)
    DoEvents
    frmWait.Show
    hdlWait = frmWait.hwnd
    
    '-> On envoie une demande de recup des fichiers du catalogue
    '-> On lance la fonction d'analyse des locks brul�es
    OrdreOk = aConnexion.PutInstruction("B3D_EXPORT~" & aConnexion.Ident & "�" & Me.ListView1.ListItems(i).Text & "�" & Me.ListView1.ListItems(i).SubItems(2) & "�" & NomFileToExport & "�" & Crypt, True)
    
    '-> Verifie si on a bien envoy� l'instruction
    If Not OrdreOk Then
        '-> Positionner le code Erreur
        MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
        GoTo GestError
    End If
    
    '-> On envoie l'ordre de lecture des instructions
    Set aBuffer = New B3DBuffer
    If Not aConnexion.SendDemande(aBuffer) Then
        '-> Positionner le code Erreur
        aConnexion.ErrorNumber = "100000"
        aConnexion.ErrorLibel = "Error sending exchange"
        aConnexion.ErrorParam = "LoadAgentDOG"
        MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
        GoTo GestError
    End If
        
    '-> Recup de la 1ere ligne
    Ligne = aBuffer.Lignes(1)
    
    '-> Tester le retour
    If Entry(2, Entry(1, Ligne, "�"), "~") <> "OK" Then GoTo NextFichier
    
    '-> On renseigne le nb d'enregs extraits
    Me.ListView1.ListItems(i).SubItems(3) = Entry(4, Ligne, "�")

NextFichier:
Next

'-> Afficher un message
MsgBox GetMesseprog("CONSOLE", "59"), vbInformation + vbOKOnly, GetMesseprog("CONSOLE", "53")

GestError:
    
    '-> Fin tempo
    If IsWindow(hdlWait) = 1 Then SendMessage hdlWait, WM_CLOSE, 0, 0

    '-> D�bloquer l'�cran
    Me.Enabled = True
    Screen.MousePointer = 0
    
    '-> Afficher un message d'erreur
    If Err.Number <> 0 Then
        Temp = Replace(GetMesseprog("CONSOLE", "60"), "#ERRNUM#", Err.Number)
        Temp = Replace(Temp, "#ERRLIB#", Err.Description)
        MsgBox Temp, vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    End If
        


End Sub
