Attribute VB_Name = "fctReplicaExport"
Option Explicit

'-> Connexion en cours
Public aConnexion As B3DConnexion

'-> Parametrage de notre UC-ID
Public ParamUC_ID As String

'-> Collection des uc-id
Public UCIDs As Collection

'-> Fichier de replication
Public ReplicaFile As String

'-> Taille generale des fichiers de replication
Public ReplicaSize As Long

'-> Indique si on doit ziper les fichiers de replication
Public ZipFiles As Boolean

'-> API de temporisation
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
'-> API Pour gestion du temps
Public Declare Function GetTickCount& Lib "kernel32" ()


Sub Main()

'---> analyse de la ligne de command
Dim ZonCharge As String
Dim ParamFile As String
Dim ListeField As String
Dim aIt As ListItem
Dim lpBuffer As String
Dim res As Long
Dim OrdreOk As Boolean
Dim aBuffer As B3DBuffer
Dim Ligne As String
Dim i As Integer
Dim aDescro As B3DFnameDescriptif
Dim RepParam As String
Dim aUcId As clsUcId
Dim ListeTri As String
Dim ListeNonTri As String


'-> Tester la ligne de commande
If Trim(Command$) = "" Then End

On Error GoTo GestError

'-> Pointeur de souris
Screen.MousePointer = 11

'-> R�cup�rer les param�tres
ParamFile = Entry(1, Command$, "�")
ZonCharge = Entry(2, Command$, "�")
        
'-> Initialiser la connexion
Set aConnexion = New B3DConnexion

'-> Charger son param�trage
If Not aConnexion.ReadParamFormFile(ParamFile) Then GoTo GestError
        
'-> Affichage de la tempo
frmWait.Label1 = GetMesseprog("APPLIC", "56")
frmWait.Show
DoEvents
        
'-> Poser le nom du programme
InitialiseB3D aConnexion, "REPLICA_EXPORT", False, False

'-> R�cup�rer le catalogue
If Not GetIdentCatalogue(aConnexion, aConnexion.Ident, "") Then
    '-> Afficher un message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbCritical + vbOKOnly, "Error"
    '-> Fin du programme
    GoTo GestError
End If

'-> Initialisation pour demande
If Not aConnexion.InitDmd(True) Then
    '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
    MsgBox GetMesseprog("APPLIC", "51"), vbCritical + vbOKOnly, "Fatal Error"
    GoTo GestError
End If
        
'-> Recup de la taille generale des fichiers de replication
RepParam = GetIniFileValue("FILE", "SIZE", aConnexion.ApplicationDir & aConnexion.Ident & "\Param\B3D_REPLICA.INI", False)
If Trim(RepParam) <> "" Then
    ReplicaSize = CLng(Entry(2, RepParam, "="))
End If

'-> Acces au parametrage de la replica export
RepParam = GetIniFileValue("REPLICAT", aConnexion.Uc_Id, aConnexion.ApplicationDir & aConnexion.Ident & "\Param\B3D_REPLICA.INI", False)
'-> Lecture du retour pour tester si notre uc-id est referenc� dans les machines � repliquer
If RepParam = "" Then
    MsgBox GetMesseprog("APPLIC", "80"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "81")
    GoTo GestError
Else
    '-> On atrouve notre UC-ID et on ramene la liste des UC-ID de replication
    ParamUC_ID = Trim(UCase$(RepParam))
End If

'-> Verif si zip ou pas
RepParam = GetIniFileValue("FILE", "ZIP", aConnexion.ApplicationDir & aConnexion.Ident & "\Param\B3D_REPLICA.INI", False)
ZipFiles = CBool(Entry(2, RepParam, "="))


Set UCIDs = New Collection
'-> On cree une collection des UC-ID de replication
RepParam = GetIniFileValue("UC-ID", "", aConnexion.ApplicationDir & aConnexion.Ident & "\Param\B3D_REPLICA.INI", True)
For i = 1 To NumEntries(RepParam, Chr(0))

    '-> teste si fin de retour
    If Trim(Entry(i, RepParam, Chr(0))) = "" Then Exit For
    
    '-> Cree une nouvelle classe uc-id
    Set aUcId = New clsUcId
    aUcId.UcId = Trim(UCase$(Entry(1, Entry(i, RepParam, Chr(0)), "=")))
    aUcId.CC = Entry(3, Entry(2, RepParam, "="), "�")
    aUcId.MailTo = Entry(1, Entry(2, RepParam, "="), "�")
    '-> renseigne la collection des tous les uc-id et de leur parametrage
    UCIDs.Add aUcId, "UCID|" & aUcId.UcId
Next
         
'-> On envoie une demande de recup des fichiers du catalogue
'-> On lance la fonction d'analyse des locks brul�es
OrdreOk = aConnexion.PutInstruction("B3D_SCHEMA_LIST~1", True)

'-> Verifie si on a bien envoy� l'instruction
If Not OrdreOk Then
    '-> Positionner le code Erreur
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    GoTo GestError
End If

'-> On envoie l'ordre de lecture des instructions
Set aBuffer = New B3DBuffer
If Not aConnexion.SendDemande(aBuffer) Then GoTo GestError
    
'-> Recup de la 1ere ligne
Ligne = aBuffer.Lignes(1)

If Entry(2, Entry(1, Ligne, "�"), "~") <> "OK" Then
    MsgBox Entry(2, Ligne, "~") & " - " & Entry(3, Ligne, "~") & " - " & Entry(4, Ligne, "~") & " - " & Entry(5, Ligne, "~")
    GoTo GestError
End If

'-> On cree les entetes de colonnes
frmReplicaExport.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "64")
frmReplicaExport.ListView1.ColumnHeaders.Add , , GetMesseprog("FRMSCHEMA", "4")
frmReplicaExport.ListView1.ColumnHeaders.Add , , GetMesseprog("CONSOLE", "74")
frmReplicaExport.ListView1.ColumnHeaders.Add , , GetMesseprog("CONSOLE", "75")
frmReplicaExport.ListView1.ColumnHeaders.Add , , GetMesseprog("CONSOLE", "76")

'-> Analyse du retour des locks repertori�s
For i = 2 To aBuffer.Lignes.Count
    '-> Recup de la ligne
    Ligne = aBuffer.Lignes(i)
        
    '-> R�cup�rer les descriptif de la table
    Set aDescro = GetFnameDescriptif(aConnexion, aConnexion.Ident, Entry(1, Ligne, "�"), True)
                            
    If aDescro Is Nothing Then
        '-> Afficher un message d'erreur
        MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbCritical + vbOKOnly, "Error"
        '-> Fin du programme
        GoTo GestError
    End If
    
    '-> test si fichier � repliquer ou pas
    If aDescro.IsReplica Then
        If ListeNonTri = "" Then
            ListeNonTri = Format(aDescro.NiveauReplica, "0000") & "�" & aDescro.Fname & "�" & aDescro.Designation
        Else
            ListeNonTri = ListeNonTri & "|" & Format(aDescro.NiveauReplica, "0000") & "�" & aDescro.Fname & "�" & aDescro.Designation
        End If
    End If
    
    GoTo NextFichier
    

NextFichier:

Next

'-> Effecture un tri selon le niveau de r�plication
ListeTri = Tri(ListeNonTri, "|", "�")

'-> Cr�er la liste selon le niveau de r�plication
If ListeTri = "" Then
    MsgBox GetMesseprog("APPLIC", "83"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

'-> Cr�er selon l'ordre de tri
For i = 1 To NumEntries(ListeTri, "|")
    '-> On cree le listview des tables
    Set aIt = frmReplicaExport.ListView1.ListItems.Add(, , Entry(1, Entry(i, ListeTri, "|"), "�"))
    aIt.SubItems(1) = Entry(2, Entry(i, ListeTri, "|"), "�")
Next 'Pour toutes les tables

'-> On genere un nom de fichier ASCII de replication
frmReplicaExport.Text1.Text = aConnexion.MainDir & "Replica\Output\" & aConnexion.Uc_Id & "-" & aConnexion.Ident & "-" & Format(Now, "YYYYMMDD") & ".DATA"
ReplicaFile = frmReplicaExport.Text1.Text

'-> R�cup�rer le catalogue
If Not GetIdentCatalogue(aConnexion, aConnexion.Ident, "") Then
    '-> Afficher un message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbCritical + vbOKOnly, "Error"
    '-> Fin du programme
    GoTo GestError
End If

'-> Fin de la tempo
Unload frmWait
                        
'-> Formattage du listview
FormatListView frmReplicaExport.ListView1
            
'-> Charger la feuille
frmReplicaExport.Show

'-> Quitter
Screen.MousePointer = 0
Exit Sub

GestError:
    '-> Pointeur de souris
    Screen.MousePointer = 0
    End

        
End Sub

