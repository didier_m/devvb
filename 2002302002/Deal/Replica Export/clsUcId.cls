VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsUcId"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'----> Classe qui permet de stocker les uc-id et d'eclater leur ligne
'      de proprietes

'-> Destinataire du mail contenant le(s) fichier(s) de replication
Public MailTo As String
'-> Copies jointes
Public CC As String
'-> Pour retrouver sa clef
Public UcId As String
