VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmReplicaExport 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7725
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7950
   Icon            =   "frmReplicaExport.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7725
   ScaleWidth      =   7950
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Caption         =   "Lancer la R�plication Export"
      Height          =   855
      Left            =   5280
      TabIndex        =   7
      Top             =   6840
      Width           =   2535
   End
   Begin VB.Frame Frame3 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   615
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   7935
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   285
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   7695
      End
   End
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   975
      Left            =   0
      TabIndex        =   2
      Top             =   6720
      Width           =   5175
      Begin VB.CheckBox Check2 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   600
         Width           =   4695
      End
      Begin VB.CheckBox Check1 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   4695
      End
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   6015
      Left            =   0
      TabIndex        =   0
      Top             =   600
      Width           =   7935
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   4080
         Top             =   840
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   1
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmReplicaExport.frx":0CCA
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.ListView Listview1 
         Height          =   5655
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   7695
         _ExtentX        =   13573
         _ExtentY        =   9975
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
End
Attribute VB_Name = "frmReplicaExport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ListeReplica As String
Dim HdlFileDec As Integer
Dim Cpt2 As Integer


Private Sub Command2_Click()

'---> permet de lancer la r�plication export

'-> Envoi de la replication
Call ReplicaExport(Me.Check1.Value, Me.Check2.Value)

End Sub

Private Sub ReplicaExport(Crypt As Integer, RAZ As Integer)
'---> permet de creer un fichier de replication _
    d 'envoyer ce fichier a toutes les UC-IDs referenc�es dans le parametrage de notre UC-ID
    
Dim i As Integer
Dim j As Integer
Dim OrdreOk As Boolean
Dim aBuffer As B3DBuffer
Dim Ligne As String
Dim Rep As VbMsgBoxResult
Dim aApp As Object
Dim aMail As Object
Dim aUcId As clsUcId
Dim Taille As Long
Dim TotLignes As Long
Dim NbFichiers As Integer
Dim NbLignes As Long
Dim HdlFileRep As Integer
Dim cpt1 As Integer
Dim Cpt2 As Integer
Dim B3DProg As String
Dim ListeTri As String
Dim ListeNonTri As String

On Error GoTo GestError

'-> Bloquer la feuille
Me.Enabled = False
Screen.MousePointer = 11

'-> Cr�ation de la liste des tables � r�pliquer selon l'ordre de r�plication
For i = 1 To Me.ListView1.ListItems.Count
    
    '-> teste si fichier selectionn�
    If Me.ListView1.ListItems(i).Checked = False Then GoTo NextFichier
    
    '-> Initialisation pour demande
    If Not aConnexion.InitDmd(True) Then
        '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
        MsgBox GetMesseprog("APPLIC", "51"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
        GoTo GestError
    End If
    
    '-> Debut tempo
    frmWait.Label1 = GetMesseprog("APPLIC", "82") & Me.ListView1.ListItems(i).Text & " " & Me.ListView1.ListItems(i).SubItems(2)
    frmWait.Show
    
    '-> On envoie une demande de recup des fichiers du catalogue
    '-> On lance la fonction d'analyse des locks brul�es
    OrdreOk = aConnexion.PutInstruction("B3D_REPLICA_EXPORT~" & aConnexion.Ident & "�" & Me.ListView1.ListItems(i).Text & "�" & Trim(Me.Text1.Text) & "�" & RAZ & "�" & Crypt, True)
    
    '-> Verifie si on a bien envoy� l'instruction
    If Not OrdreOk Then
        '-> Positionner le code Erreur
        MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
        GoTo GestError
    End If
    
    '-> On envoie l'ordre de lecture des instructions
    Set aBuffer = New B3DBuffer
    If Not aConnexion.SendDemande(aBuffer) Then
        '-> Positionner le code Erreur
        aConnexion.ErrorNumber = "100000"
        aConnexion.ErrorLibel = "Error sending exchange"
        aConnexion.ErrorParam = "LoadAgentDOG"
        '-> Afficher le message d'erreur
        MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
        '-> Fin du programme
        GoTo GestError
    End If
        
    '-> Recup de la 1ere ligne
    Ligne = aBuffer.Lignes(1)
    
    If Entry(2, Entry(1, Ligne, "�"), "~") <> "OK" Then
        '-> Fin de la tempo
        Unload frmWait
        GoTo NextFichier
    End If
    
    '-> On renseigne les NBCREATE, NBUPDATE et NBDELETE pour chaque ligne
    Me.ListView1.ListItems(i).SubItems(2) = Entry(4, Ligne, "�")
    Me.ListView1.ListItems(i).SubItems(3) = Entry(5, Ligne, "�")
    Me.ListView1.ListItems(i).SubItems(4) = Entry(6, Ligne, "�")
    
    '-> Creation du total lignes
    TotLignes = TotLignes + CLng(Entry(4, Ligne, "�")) + CLng(Entry(5, Ligne, "�")) + CLng(Entry(5, Ligne, "�"))
    
    '-> Fin de la tempo
    Unload frmWait
    
NextFichier:
    
Next

'-> Analyse de la taille de fichier a envoyer pour decoupe _
    sinon on l'nvoie en l'etat
If ReplicaSize <> 0 Then
    
    '-> Ici ZIPER le fichier dans un fichier de manoeuvre pour calculer sa taille zippee THIERRY
    If ZipFiles Then
    
    End If
    
    '-> recup de la taille du fichier de replication zip�
    Taille = GetFileSizeVB(ReplicaFile)
    
    '-> teste si besoin de decouper le, fichier en fonction de la taille ref
    If Taille < ReplicaSize Then
        ListeReplica = ReplicaFile
        GoTo REPLICA
    End If
    
    '-> definir le nombre de fichiers
    NbFichiers = Fix(Taille / ReplicaSize) + 1
    
    '-> Definir le nombre de lignes par fichiers
    NbLignes = Fix(TotLignes / NbFichiers)
    If NbLignes = 0 Then NbLignes = 1
    
    '-> On ouvre le fichier de replication
    HdlFileRep = FreeFile
    Open ReplicaFile For Input As #HdlFileRep
    
    '-> Parcours du fichier de replication
    Do While Not EOF(HdlFileRep)
        '-> Recup d'une ligne de fichier de replica
        Line Input #HdlFileRep, Ligne
        '-> recup du prog
        If cpt1 = 0 And Cpt2 = 0 Then
            B3DProg = Ligne
            '-> Ouverture d'un nouveau fichier de replica
            OpenReplicaFile B3DProg, Cpt2
        End If
        
        '-> teste si ligne valide
        If Entry(1, Ligne, "~") <> "B3D_PROG" Then
            Print #HdlFileDec, Ligne
            cpt1 = cpt1 + 1
        End If
        
        If cpt1 = NbLignes Then
            cpt1 = 1
            '-> fermeture du fichier de decoupe en cours
            Close #HdlFileDec
            '-> Ouverture d'un nouveau fichier de replica
            OpenReplicaFile B3DProg, Cpt2
        End If
                    
    Loop
    Close #HdlFileRep
    '-> Supp du fichier de replication d'origine
    'Kill ReplicaFile
    
Else

    ListeReplica = ReplicaFile
    
End If

REPLICA:

'-> Penser a zipper les fichier � envoyer !!


'-> Tempo pour l'envoi des mails
frmWait.Label1 = GetMesseprog("APPLIC", "80")
frmWait.Show

'-> Pour gestion envoi des mails par OUTLOOK
'-> On declare une nouvelle application ( environnement ) OutLook
Set aApp = CreateObject("Outlook.Application")

'-> Lecture du parametrage de notre UC-ID pour voir a quelles UC-IDs on _
    doit envoyer des copies du fichier de replication
If UCase$(ParamUC_ID) = "ALL" Then
    '-> On envoie a tous les UC-IDs
    For Each aUcId In UCIDs
        '-> teste si on ne se l'envoi pas a nous-meme
        If aUcId.UcId = aConnexion.Uc_Id Then GoTo NextUcId
        '-> On cree un mail dans cet environnement
        Set aMail = aApp.CreateItem(olMailItem)
        '-> destinataire
        aMail.To = aUcId.MailTo
        '-> Copies jointes
        aMail.CC = aUcId.CC
        '-> Attachements
        For i = 1 To NumEntries(ListeReplica, ";")
            aMail.Attachments.Add Entry(i, ListeReplica, ";")
        Next
        '-> Envoi du mail
        aMail.Send
        
NextUcId:

    Next
Else
    '-> On envoie aux Uc-Ids repertori�s dans le parametrage de notre UC-ID
    For i = 1 To NumEntries(ParamUC_ID, "�")
        '-> On pointe sur l'ucid de destination dans la collection
        Set aUcId = UCIDs("UCID|" & Entry(i, ParamUC_ID, "�"))
        '-> On cree un mail dans cet environnement
        Set aMail = aApp.CreateItem(olMailItem)
        '-> destinataire
        aMail.To = aUcId.MailTo
        '-> Copies jointes
        aMail.CC = aUcId.CC
        '-> Attachements
        aMail.Attachments.Add ListeReplica
        '-> Envoi du mail
        aMail.Send
    Next
End If

'-> On supprime les fichiers de replication
For i = 1 To NumEntries(ReplicaFile, ";")
    'Kill Trim(Entry(i, ReplicaFile, ";")) 'THIERRY
Next

'-> Fin tempo
Unload frmWait
    
'-> Quitter la proc�dure
Exit Sub

GestError:

'-> Bloquer la feuille
Me.Enabled = False
Screen.MousePointer = 11
    
End Sub

Private Sub OpenReplicaFile(B3DProg As String, Cpt2 As Integer)
'---> Permet de creer un fichier de decoupe pour replica
   '-> Ouverture du fichier REPLICAFILE_XXX.REPLICA en ecriture
HdlFileDec = FreeFile
Cpt2 = Cpt2 + 1
Open Entry(1, ReplicaFile, ".") & "-" & Format(Cpt2, "000") & ".DATA" For Output As #HdlFileDec

'-> Envoi du prog
Print #HdlFileDec, B3DProg

'-> On stocke le nom du fichier genere pour Pieces Jointes du mail de replica
If ListeReplica = "" Then
    ListeReplica = ListeReplica & Entry(1, ReplicaFile, ".") & "-" & Format(Cpt2, "000") & ".DATA"
Else
    ListeReplica = ListeReplica & ";" & Entry(1, ReplicaFile, ".") & "-" & Format(Cpt2, "000") & ".DATA"
End If


End Sub


Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("CONSOLE", "68")

'-> Messprog
Me.Frame3.Caption = GetMesseprog("CONSOLE", "69")
Me.Frame2.Caption = GetMesseprog("CONSOLE", "70")
Me.Check1.Caption = GetMesseprog("CONSOLE", "71")
Me.Check2.Caption = GetMesseprog("CONSOLE", "72")
Me.Command2.Caption = GetMesseprog("CONSOLE", "73")

End Sub

