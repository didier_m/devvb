VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmVisualisation 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Visualisation des Locks Brul�s"
   ClientHeight    =   4245
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7965
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4245
   ScaleWidth      =   7965
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Caption         =   "Supprimer le fichier journal"
      Height          =   255
      Left            =   0
      TabIndex        =   2
      Top             =   3960
      Width           =   2295
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Imprimer"
      Height          =   255
      Left            =   6240
      TabIndex        =   1
      Top             =   3960
      Width           =   1695
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7935
      _ExtentX        =   13996
      _ExtentY        =   6800
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "frmVisualisation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Fichier As String

Private Sub Command1_Click()
'---> On supprime le fichier
If Dir$(Fichier) <> "" Then
    Kill Fichier
Else
    MsgBox "Fichier d�j� supprim�...", vbExclamation
    Exit Sub
End If
End Sub

Private Sub Command2_Click()
'---> On lance l'impression


End Sub


Private Sub TraiteFichierLog(Fichier As String)
'---> Fonction qui affiche le contenu du fichier .log
Dim HdlFileL As Integer
Dim Ligne As String
Dim aIt As ListItem
Dim A As String
Dim B As String

'-> Nouveau canal
HdlFileL = FreeFile
'-> Ouverture du fichier en lecture
Open Fichier For Input As #HdlFileL

'-> Teste si fichier vide
If EOF(HdlFileL) Then
    MsgBox "Aucun Lock � traiter...", vbExclamation
    Exit Sub
End If

'-> On cree les colonnes
frmVisualisation.ListView1.ColumnHeaders.Add , , "No Fichier"
frmVisualisation.ListView1.ColumnHeaders.Add , , "No Enregistrement"
frmVisualisation.ListView1.ColumnHeaders.Add , , "Fonction"
frmVisualisation.ListView1.ColumnHeaders.Add , , "Date"
frmVisualisation.ListView1.ColumnHeaders.Add , , "Heure"
frmVisualisation.ListView1.ColumnHeaders.Add , , "Op�rateur"
frmVisualisation.ListView1.ColumnHeaders.Add , , "Programme"

'-> Parcours du fichier pour creer un listview
Do While Not EOF(HdlFileL)
    '-> Recup d'une ligne
    Line Input #HdlFileL, Ligne
    A = Entry(1, Ligne, "~")
    B = Entry(2, Ligne, "~")
    
    '-> Insertion dans le listview
    Set aIt = frmVisualisation.ListView1.ListItems.Add(, , Entry(2, A, "�"))
    aIt.SubItems(1) = Entry(3, A, "�")
    aIt.SubItems(2) = Entry(1, B, "�")
    aIt.SubItems(3) = Format(Entry(3, B, "�"), "DD/MM/YYYY")
    aIt.SubItems(4) = Format(Entry(4, B, "�"), "00:00:00")
    aIt.SubItems(5) = Entry(8, B, "�")
    aIt.SubItems(6) = Entry(9, B, "�")
    
Loop

'-> Formattage du listview
FormatListView frmVisualisation.ListView1

'-> affichage de la visu
frmVisualisation.Show vbModal

End Sub

