Attribute VB_Name = "fctLockBrules"
Option Explicit

'-> Connexion en cours
Public aConnexion As B3DConnexion

'-> API de temporisation
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
'-> API Pour gestion du temps
Public Declare Function GetTickCount& Lib "kernel32" ()

Sub Main()
'------------------------------------------------------------------
'---- Procedure de suppression des locks brules dans la base ------
'------------------------------------------------------------------
Dim ZonCharge As String
Dim ParamFile As String
Dim ListeField As String
Dim aIt As ListItem
Dim lpBuffer As String
Dim res As Long
Dim HdlFileI As Integer
Dim aBuffer As B3DBuffer
Dim i As Integer
Dim DmdUnlock(5000) As String
Dim OrdreOk As Boolean
Dim Ligne As String
Dim Rep As VbMsgBoxResult

On Error GoTo GestError

'-> Tester la ligne de commande
If Trim(Command$) = "" Then End

'-> Pointeur de souris
Screen.MousePointer = 11

'-> R�cup�rer les param�tres
ParamFile = Entry(1, Command$, "�")
ZonCharge = Entry(2, Command$, "�")
            
'-> Initialiser la connexion
Set aConnexion = New B3DConnexion

'-> Charger son param�trage
If Not aConnexion.ReadParamFormFile(ParamFile) Then GoTo GestError

'-> Lancer la Temporisation
frmWait.Label1 = GetMesseprog("APPLIC", "76")
frmWait.Show

'-> Lecture de la premiere ligne du fichier pour voir si fichier de replication
HdlFileI = FreeFile

'-> Ouvertue fic
Open aConnexion.ApplicationDir & aConnexion.Ident & "\Param\LOCK_ANALYZER.LOG" For Append As #HdlFileI

'-> Poser le nom du programme
InitialiseB3D aConnexion, "LOCK_ANALYZER", False, False

If Not aConnexion.InitDmd(True) Then
    '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
    MsgBox GetMesseprog("APPLIC", "51"), vbCritical + vbOKOnly, "Fatal Error"
    GoTo GestError
End If

'-> On lance la fonction d'analyse des locks brul�es
OrdreOk = aConnexion.PutInstruction("B3D_GET_LOCK", True)

'-> Verifie si on a bien envoy� l'instruction
If Not OrdreOk Then
    '-> Positionner le code Erreur
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    GoTo GestError
End If

'-> Envoyer l'instruction et charger la matrice des champs
Set aBuffer = New B3DBuffer
If Not aConnexion.SendDemande(aBuffer) Then
    MsgBox GetMesseprog("APPLIC", "53"), vbCritical + vbOKOnly, "Fatal Error"
    GoTo GestError
End If

'-> Recup de la 1ere ligne
Ligne = aBuffer.Lignes(1)

If Entry(2, Ligne, "~") <> "OK" Then GoTo GestError
    
'-> Analyse du retour des locks repertori�s
For i = 2 To aBuffer.Lignes.Count
    '-> Recup de la ligne
    Ligne = aBuffer.Lignes(i)
    '-> Analyse de la r�ponse
    If Entry(4, Entry(1, Ligne, "~"), "�") = "1" And Entry(6, Entry(2, Ligne, "~"), "�") = "" Then
         Print #HdlFileI, Ligne
         '-> stockage d'une demande de unlock
         DmdUnlock(i) = "B3D_UNLOCK~" & Entry(1, Entry(1, Ligne, "~"), "�") & "�" & Entry(2, Entry(1, Ligne, "~"), "�") & "�" & Entry(3, Entry(1, Ligne, "~"), "�")
    End If
Next

If Not aConnexion.InitDmd(True) Then
    '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
    MsgBox GetMesseprog("APPLIC", "53"), vbCritical + vbOKOnly, "Fatal Error"
    GoTo GestError
End If

'-> On traite les demandes de unlock stock�es
For i = 2 To 5000
    If DmdUnlock(i) <> "" Then
        '-> On lance la fonction d'analyse des locks brul�es
        OrdreOk = aConnexion.PutInstruction(DmdUnlock(i), True)
        
        '-> Verifie si on a bien envoy� l'instruction
        If Not OrdreOk Then
            '-> Positionner le code Erreur
            MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
            GoTo GestError
        End If
        
    End If
Next

'-> Envoyer l'instruction et charger la matrice des champs
Set aBuffer = New B3DBuffer
If Not aConnexion.SendDemande(aBuffer) Then MsgBox GetMesseprog("APPLIC", "53"), vbCritical + vbOKOnly, "Fatal Error"

GestError:
    
    '-> Trap des erreurs
    Reset
    Unload frmWait
    '-> Pointeur de souris
    Screen.MousePointer = 0
    End
        
End Sub

