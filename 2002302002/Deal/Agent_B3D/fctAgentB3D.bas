Attribute VB_Name = "fctAgentB3D"
Option Explicit

'-> API de v�rification si fen�tre existante
Public Declare Function IsWindow& Lib "user32" (ByVal hwnd As Long)

'-> API de passage de zone de texte via un handle de communication
Public Declare Function SetWindowText& Lib "user32" Alias "SetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String)
Public Declare Function GetWindowText& Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String, ByVal cch As Long)


'-> API de temporisation
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

'-> Pour gestion du serveurDOG
Public ServeurDOG As clsDOG

'-> Pour cr�ation du fichier R�ponse
Public CreateRspFile As Boolean 'Indique si on doit cr�er ou non le fichier retour dans le cas de l'int�gration d'un interface


Public Sub ConnectDOG()

'---> Cette proc�dure cr�er une nouvelle instance du serveur DOG

Dim ErrorCode As String

On Error GoTo GestError

Set ServeurDOG = New clsDOG

'-> Charger le catalogue
ErrorCode = "Loading Catalogue"
If Not LoadCatalogueB3D(ServeurDOG, FichierCatalogue) Then GoTo BadLoading

'-> Charger le sch�ma
ErrorCode = "Loading sch�ma"
If Not LoadSchemaB3D(ServeurDOG, FichierSchema) Then GoTo BadLoading

'-> Charger le fichier des liens
ErrorCode = "Loading Link"
If Not LoadLinkB3D(ServeurDOG, FichierLink) Then GoTo BadLoading

'-> Charger les tables syst�mes
ErrorCode = "Loading sysFile"
If Not LoadSystemParam(ServeurDOG, B3DIniFile, Ident) Then GoTo BadLoading

'-> Postionner le code de l'ident
ServeurDOG.Ident = Ident

'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> Mode debug
    Call PrintDebug("Erreur dans l'initialisation du DOG " & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)
    
BadLoading:
    
    Call PrintDebug("Erreur dans le chargement du dog : " & ErrorCode)
    
    '-> Fin du programme
    End

End Sub

Public Sub Main()

'---> Point d'entr�e du programme

'-> Quitter s'il n'y a pas d'arguments dur la ligne de commande
If Trim(Command$) = "" Then End

'-> Get des s�parateurs d�cimaux
Call InitConvertion

'-> Lancer l'initialisation
Call Init(Command$)

'-> Connecter le serveur DOG
Call ConnectDOG

'-> Connecter la base
Call OpenConnexionBase

'-> Charger la feuille en m�moire
Load frmAgentB3D
                   
'-> Setting de la temporisation de l'agent B3D
frmAgentB3D.Timer1.Interval = AccesDir

'-> Position en attente
frmAgentB3D.Timer1.Enabled = True

End Sub



Public Function TestCondition(ByRef Id_Fname As String, ByRef strChamp As String, _
                               ByRef strCondition As String, ByRef strValMini As String, _
                               ByRef strValMaxi As String, ByVal Num_Enreg As String) As Boolean


Dim boolRetour As Boolean
Dim aRec As Object
Dim strTest As String
Dim aEmilieObj As Object
Dim ErrorCode As String

On Error GoTo GestError

ErrorCode = "TestCondition : Setting de l'emilieobj : " & Id_Fname & "  " & strChamp

'-> Pointer sur l'objet du catalogue
Set aEmilieObj = ServeurDOG.Catalogue(UCase$(Entry(1, Id_Fname, "�")) & "|" & UCase$(strChamp))

If aEmilieObj Is Nothing Then
    '-> Renvoyer faux
    TestCondition = False
    Exit Function
End If


ErrorCode = "TestCondition : Acces Data"

'-> Pointer sur DEALB3D_DATA
Set aRec = SetTable("SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & Num_Enreg _
                                   & "' AND [FIELD] = '" & strChamp & "'")

'-> PIERROT : PASSER lA VALEUR A COMPARER EN MAJUSCULES
strTest = Trim(UCase$(DeCrypt(aRec.Fields("ZON_DATA"))))

boolRetour = True

ErrorCode = "TestCondition : Test de la condition : " & strCondition

Select Case UCase$(strCondition)
        
    Case "EQ"
    
        Select Case UCase$(Left$(aEmilieObj.DataType, 3))
            Case "DEC"
                If CDbl(strTest) <> CDbl(strValMini) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) <> CInt(strValMini) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest <> strValMini Then boolRetour = False
        
        End Select
        
    Case "INC"
    
        Select Case UCase$(Left$(aEmilieObj.DataType, 3))
            Case "DEC"
                If CDbl(strTest) < CDbl(strValMini) Or CDbl(strTest) > CDbl(strValMaxi) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) < CInt(strValMini) Or CInt(strTest) > CInt(strValMaxi) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest < strValMini Or strTest > strValMaxi Then boolRetour = False
        End Select
        
    Case "NIN"
    
        Select Case UCase$(Left$(aEmilieObj.DataType, 3))
            Case "DEC"
                If CDbl(strTest) > CDbl(strValMini) Or CDbl(strTest) < CDbl(strValMaxi) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) > CInt(strValMini) Or CInt(strTest) < CInt(strValMaxi) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest > strValMini Or strTest < strValMaxi Then boolRetour = False
        End Select
            
    Case "DIF"
        
        Select Case UCase$(Left$(aEmilieObj.DataType, 3))
            Case "DEC"
                If CDbl(strTest) = CDbl(strValMini) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) = CInt(strValMini) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest = strValMini Then boolRetour = False
        End Select
        
    Case "LE"
    
        Select Case UCase$(Left$(aEmilieObj.DataType, 3))
            Case "DEC"
                If CDbl(strTest) > CDbl(strValMini) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) > CInt(strValMini) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest > strValMini Then boolRetour = False
        
        End Select
        
    Case "GE"
    
        Select Case UCase$(Left$(aEmilieObj.DataType, 3))
            Case "DEC"
                If CDbl(strTest) < CDbl(strValMini) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) < CInt(strValMini) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest < strValMini Then boolRetour = False
        
        End Select
        
    Case "LT"
    
        Select Case UCase$(Left$(aEmilieObj.DataType, 3))
            Case "DEC"
                If CDbl(strTest) >= CDbl(strValMini) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) >= CInt(strValMini) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest >= strValMini Then boolRetour = False
        
        End Select
        
    Case "GT"
    
        Select Case UCase$(Left$(aEmilieObj.DataType, 3))
            Case "DEC"
                If CDbl(strTest) <= CDbl(strValMini) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) <= CInt(strValMini) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest <= strValMini Then boolRetour = False
        
        End Select
        
    Case "BEG"
    
        If UCase$(Left$(aEmilieObj.DataType, 3)) = "CHA" Then
            If strValMini <> Left$(strTest, Len(strValMini)) Then boolRetour = False
        End If
        
        
    Case "MAT"
        
        If UCase$(Left$(aEmilieObj.DataType, 3)) = "CHA" Then
            If strTest = "" Then
                boolRetour = False
            Else
                If InStr(1, UCase$(strTest), UCase$(strValMini)) = 0 Then boolRetour = False
            End If
        End If

End Select

TestCondition = boolRetour

Exit Function

GestError:

    If Err.Number = 3146 Or Err.Number = 3260 Then
        Sleep (TempoLock)
        Resume
    End If
            
            
End Function



Public Sub Integration_Interface(Param)


'---> Cette proc�dure analyse un fichier ASCII, g�n�re un fichier de deamnde � int�grer


'PARAM :[IDENT] �  [FNAME] � [CODE INT] � [FICHIER ASCII] � [FICHIER DMD]


'---> Programme d'analyse d'un fichier interface

Dim TempFileName As String '-> Nom du fichier temporaire
Dim FichierASCII As String 'Fichier � int�grer
Dim hdlASCII As Integer '-> Handle du fichier ascii
Dim IntCode As String '-> Code de l'interface
Dim aInt As Object '-> Pointeur vers l'interface
Dim aIntField As Object '-> Pointeur vers un champ de l'interface
Dim Fname_Name As String '-> Nom de la table
Dim aFname As Object '-> Pointeur vers la table
Dim aIname As Object '-> Pointeur vers un index
Dim aIfield As Object '-> Pointeur vers un champ d'index
Dim FichierDemande As String '-> Nom du fichier demande � g�n�rer
Dim hdlDMD As Integer '-> Handle du fichier demande � g�n�rer
Dim Ligne As String '-> Pour lecture d'une ligne d'un fichier ascii

Dim Num_Field() As String '-> Matrice des champs dans chaque ligne
Dim Value_Field() As String '-> Matrice des valeurs de chaque champ
Dim Error_Field() As String '-> Code d'erreur champ � champ

Dim NbField As Integer '-> Nombre de champs contenu dans une ligne
Dim aEmilieObj As Object '-> Pointeur vers un objet du catalogue

Dim i As Integer, j As Integer, k As Integer

Dim Int_Error As Integer '-> Code d'une erreur
Dim Top_Error As Boolean '-> Indique si une erreur sur verif d'un champ
Dim strTempo As String

Dim Num_Update As String '-> Num�ro d'un enregistrement existant

Dim aRec As Object '-> Recordset

Dim NbLig As Long '-> Compteur de lignes

'-> Var pour acces sur index multi
Dim Soc_Ref As String
Dim Acces_Ref As String
Dim Num_Enreg_Ref As String
Dim Index_Value() As String
Dim Champ_ref As String
Dim strValue As String

Dim strSelect As String

Dim Find_Field As Boolean
Dim IsChampRef As Boolean

On Error GoTo GestError

'-> Recup des param
FichierASCII = Entry(4, Param, "�")
IntCode = Entry(3, Param, "�")
Fname_Name = Entry(2, Param, "�")
FichierDemande = Entry(5, Param, "�")
If CInt(Entry(6, Param, "�")) = 0 Then CreateRspFile = False

'-> Obtenir un nom de fichier temporaire
TempFileName = GetTempFileNameVB("INT")

'-> Ouvrir le fichier temporaire de r�ponse
hdlDMD = FreeFile
Open TempFileName For Output As #hdlDMD

'-> V�rifier que le fichier est rens�ign�
If FichierASCII = "" Then
    Print #hdlDMD, "B3D_REJET~Fichier ASCII non renseign� "
    GoTo GestError
End If

'-> V�rifier que le fihcier � int�grer existe
If Dir$(FichierASCII, vbNormal) = "" Then
    Print #hdlDMD, "B3D_REJET~Fichier ascii introuvable : " & FichierASCII
    GoTo GestError
End If

'-> V�rifier que la table existe
If Not ServeurDOG.IsFname(Fname_Name) Then
    Print #hdlDMD, "B3D_REJET~Table inexistante : " & Fname_Name & " pour l'ident " & Ident
    GoTo GestError
End If

'-> V�rifier que l'interface existe
If Not ServeurDOG.IsInt(Fname_Name, IntCode) Then
    Print #hdlDMD, "B3D_REJET~Interface inexistant : " & IntCode & " pour l'ident " & Ident
    GoTo GestError
End If

'-> Pointer sur la table
Set aFname = ServeurDOG.fNames(Ident & "|" & Fname_Name)

'-> Pointer sur l'interface
Set aInt = aFname.Interfaces("INT|" & IntCode)

'-> Ouverture du fichier � int�grer
hdlASCII = FreeFile
Open FichierASCII For Input As #hdlASCII

'-> Impression de la commande d'initialisation
Print #hdlDMD, "B3D_INIT~" & Operateur & "�INT[" & Entry(NumEntries(FichierASCII, "\"), FichierASCII, "\") & "]���"

Do While Not EOF(hdlASCII)
    
    '-> Lecture de la ligne
    Line Input #hdlASCII, Ligne
    
    '-> Compteur de ligne
    NbLig = NbLig + 1
    
    '-> Tester Ligne � blanc
    If Trim(Ligne) = "" Then GoTo NextLigne

    '-> Ecraser les matrices
    Erase Num_Field()
    Erase Value_Field()
    Erase Error_Field()
    Top_Error = False
    NbField = 0
    
    '-> Analyse du contenu de la ligne
    For Each aIntField In aInt.Fields
        
        '-> Incr�menter le compteur de champs
        NbField = NbField + 1
        
        '-> Redimensionner la matrice
        ReDim Preserve Num_Field(NbField)
        ReDim Preserve Value_Field(NbField)
        ReDim Preserve Error_Field(NbField)
        
        '-> Valeur du champ
        Num_Field(NbField) = aIntField.Num_Field
        
        If aIntField.Position <> 0 Then
            '-> Tester si o utilise un s�parateur
            If aInt.UseSeparateur Then
                '-> V�rifier que l'entry existe
                If NumEntries(Ligne, aInt.Separateur) >= aIntField.Position Then
                    '-> R�cup�rer l'entry
                    Value_Field(NbField) = Entry(aIntField.Position, Ligne, aInt.Separateur)
                End If
            Else
                '-> Tester si le substring OK
                If Len(Ligne) > aIntField.Position + aIntField.Longueur - 1 Then
                    '-> SubString
                    Value_Field(NbField) = Mid$(Ligne, aIntField.Position, aIntField.Longueur)
                End If
            End If
        End If
        
        '-> Teste de la valeur par defaut et constante
        If Trim(Value_Field(NbField)) = "" And Trim(aIntField.Defaut) <> "" Then
            Value_Field(NbField) = aIntField.Defaut
        End If
                
    Next 'Pour tous les champs de l'interface
        
    For i = 1 To NbField
    
        '-> Pointer sur le champ de la'interface
        Set aIntField = aInt.Fields(Num_Field(i))
                
        '-> S'il y a une table de correpsondance
        If aIntField.Table_Correspondance <> "" Then
            
            '-> Il ya une table de correpondance faire un acces � [DEALB3D_CORINT]
            strSelect = "SELECT TOP 1 * FROM [DEALB3D_CORINT] WHERE [ID_FNAME] = '" & Ident & "�" & Fname_Name & "' AND [FIELD] = '" & Num_Field(NbField) & "' AND " _
            & "[NUM_CORR] = '" & aIntField.Table_Correspondance & "' AND [ZON_ASC] = '" & Value_Field(NbField) & "'"
            
            '-> Recordset
            Set aRec = SetTable(strSelect)
            If Not aRec.EOF Then
                '-> on a trouv� une table de correspondance
                Value_Field(NbField) = RTrim(aRec.Fields("ZON_DATA"))
                '-> Tester s'il y a des champs � compl�ter pour cette valeur
                For j = 1 To 3
                    If aRec.Fields("FIELD_LNK_" & j) <> "" Then
                        '-> On v�rifie si le champ est d�ja cr�� : si c'est le cas ; on remplace par la valeur de corres
                        Find_Field = False
                        For k = 1 To NbField
                            If Num_Field(k) = aRec.Fields("FIELD_LNK_" & j) Then
                                Value_Field(k) = RTrim(aRec.Fields("VALUE_LNK_" & j))
                                Find_Field = True
                                Exit For
                            End If
                        Next
                        
                        '-> On n' a pas trouv� le champ : on le rajoute dans les champs � cr�er
                        If Not Find_Field Then
                            '-> Compteur de champ
                            NbField = NbField + 1
                            '-> Redimensionner les matrices
                            ReDim Preserve Num_Field(NbField)
                            ReDim Preserve Value_Field(NbField)
                            ReDim Preserve Error_Field(NbField)
                            
                            '-> Affecter la valeur de la table de correspondance
                            Num_Field(NbField) = aRec.Fields("FIELD_LNK_" & j)
                            Value_Field(NbField) = RTrim(aRec.Fields("VALUE_LNK_" & j))
                                        
                        End If 'Si on a trouv� le champ
                    End If '> Si champ de correspondabnce
                Next 'Pour les 3 champs compl�
            End If 'Si on a trouv� une correspondance
            aRec.Close
        
        End If 'S'il y a une table de correspondance
    
    Next 'Pour tous les champs de la ligne
    
        
    '-> Analyse des champs pour r�cup�rer la table associ�e
    For i = 1 To NbField
        '-> Pointer sur l'objet du catalogue
        Set aEmilieObj = ServeurDOG.Catalogue(Ident & "|" & Num_Field(i))
        '-> V�rifier s'il y a une table associ�e
        If aEmilieObj.Table_Correspondance <> "" And aEmilieObj.Table_Correspondance <> Fname_Name Then
            
            '-> Initialisation de la chaine de la table pour requete SQL
            strSelect = "SELECT TOP 1 * "
            
            '-> Pointer sur la table sp�cifi�e
            Set aFname = ServeurDOG.fNames(Ident & "|" & aEmilieObj.Table_Correspondance)
            
            '-> Cas ou la table est en acces direct
            If aFname.IndexDirect <> "" Then
            
                '-> Table � attaquer avec code de l'index direct
                strSelect = strSelect & "FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Ident & "�" & aEmilieObj.Table_Correspondance & "'"
                strSelect = strSelect & " AND [FIELD] ='" & aFname.IndexDirect & "' AND  [ZON_DATA] = '" & Value_Field(i) & "'"
            Else
                '-> Si index multiple
                
                '-> Mettre � blanc la variable de jointure
                Acces_Ref = ""
                Num_Enreg_Ref = ""
                Soc_Ref = ""
                Erase Index_Value()
                ReDim Index_Value(0)
                                               
                '-> Recherche de la soci�t� r�elle
                For j = 1 To NbField
                    If Num_Field(j) = ServeurDOG.GetSystemTableValue("CODSOC") Then
                        Soc_Ref = Value_Field(j)
                        Exit For
                    End If
                Next
                            
                '-> Get de la soci�t� de ref ( voir B3D_GET_DATA )
                '-> tester s'il y a un acc�s par r�f�rence sur le premier index de la table de correspondance
                IsChampRef = IsInameChampRef(aEmilieObj.Table_Correspondance)
                            
                                                                                                                                                
                '-> tester s'il y a un acc�s par r�f�rence
                If IsChampRef Then
                
                    '-> Recup du champ de ref
                    Champ_ref = aEmilieObj.MasterField
                    
                    If Champ_ref <> "" Then
                        For j = 1 To NbField
                            If Num_Field(j) = Champ_ref Then
                                Acces_Ref = Value_Field(j)
                                Exit For
                            End If
                        Next
                    End If 'Si acces par r�f�rence
                        
                    '-> Recup du num_enreg de la table de r�f�rence soci�t� pour acces � DEALB3D_DATA
                    Set aRec = SetTable("SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & _
                        Ident & "�" & ServeurDOG.GetSystemTableValue("REF_SOCIETE") & "' AND [ZON_INDEX_1] = '" & Soc_Ref & _
                        "' AND [ZON_INDEX_2] = '" & aEmilieObj.Table_Correspondance & "' AND [ZON_INDEX_3] = '" _
                        & Acces_Ref & "'")
                        
                    If Not aRec.EOF Then Num_Enreg_Ref = RTrim(DeCrypt(aRec.Fields("NUM_ENREG")))
                    aRec.Close
                    
                    '-> Acces a DEALB3D_DATA pour recup�rer la valeur de la soci�t� de r�f�rence
                    Set aRec = SetTable("SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & _
                    Ident & "�" & ServeurDOG.GetSystemTableValue("REF_SOCIETE") & "' AND [NUM_ENREG] = '" & Num_Enreg_Ref & "' AND [FIELD] = '" & ServeurDOG.GetSystemTableValue("SOCREF") & "'")
                            
                    If Not aRec.EOF Then Soc_Ref = RTrim(DeCrypt(aRec.Fields("ZON_DATA")))
                    aRec.Close
                                                                
                End If '-> Si champ d'acces de r�f�rence
                    
                '-> Construire l'index d'acc�s � DEALB3D_INDEX ( table secondaire ) avec les valeurs de DEALB3D_DATA ( sauf pour le SOCREF)
                '-> Chercher le num_enreg_join d'apres l'index THIERRY LIRE B3D pour recup de l'index � traiter
                                
                '-> Prendre le premier index par defaut
                Set aIname = aFname.Inames("001")
                                        
                '-> Construire la requete d'acc�s
                For Each aIfield In aIname.iFields
                
                    '-> En construisant , pour le champ SOCREF ne pas prendre DEALB3D_DATA mais DEALB3D_INDEX de Ref_SOCIETE
                    If aIfield.Num_Field = ServeurDOG.GetSystemTableValue("SOCREF") Then
                        strValue = RTrim(Soc_Ref)
                    Else
                    
                        For j = 1 To NbField
                            If Num_Field(j) = aIfield.Num_Field Then
                                strValue = RTrim(Value_Field(j))
                                Exit For
                            End If
                        Next
                      
                    End If
                    ReDim Preserve Index_Value(UBound(Index_Value()) + 1)
                    Index_Value(UBound(Index_Value())) = strValue
                    
                Next 'Pour tous les champs d'index
                                    
                ReDim Preserve Index_Value(15)
                
            
                '-> Acc�s � DEALB3D_INDEX pour r�cup�rer le num d'enregistrement pour acc�der � DEALB3D_DATA
                strSelect = "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & _
                        Ident & "�" & aEmilieObj.Table_Correspondance & "' AND [INAME] = '" & aIname.Num_Iname & _
                        "' AND [ZON_INDEX_1] = '" & Index_Value(1) & _
                        "' AND [ZON_INDEX_2] = '" & Index_Value(2) & _
                        "' AND [ZON_INDEX_3] = '" & Index_Value(3) & _
                        "' AND [ZON_INDEX_4] = '" & Index_Value(4) & _
                        "' AND [ZON_INDEX_5] = '" & Index_Value(5) & _
                        "' AND [ZON_INDEX_6] = '" & Index_Value(6) & _
                        "' AND [ZON_INDEX_7] = '" & Index_Value(7) & _
                        "' AND [ZON_INDEX_8] = '" & Index_Value(8) & _
                        "' AND [ZON_INDEX_9] = '" & Index_Value(9) & _
                        "' AND [ZON_INDEX_10] = '" & Index_Value(10) & _
                        "' AND [ZON_INDEX_11] = '" & Index_Value(11) & _
                        "' AND [ZON_INDEX_12] = '" & Index_Value(12) & _
                        "' AND [ZON_INDEX_13] = '" & Index_Value(13) & _
                        "' AND [ZON_INDEX_14] = '" & Index_Value(14) & _
                        "' AND [ZON_INDEX_15] = '" & Index_Value(15) & "'"
            End If
            
            '-> Analyse de la base
            Set aRec = SetTable(strSelect)
            If aRec.EOF Then
                '-> Erreur : on n' a pas trouv� l'enregistrement
                Error_Field(i) = "ENREG_NOT_FOUND"
                Top_Error = True
            End If
            aRec.Close
                                    
        End If '-> Si table de correpondance
    
    Next 'Pour tous les champs recup�r�s dans la ligne
            
    '-> Acces � la data ou � l'index pour voir s'il existe (UPDATE) ou Pas(CREATE) s'il pas d'erreurs (REJET)
                    
    '-> Tester s'il n'y a pas d'erreurs
    If Top_Error Then

        '-> Traiter le rejet
        strSelect = "B3D_REJET~NUM_LIGNE_ASCII�" & NbLig & "~"
        For i = 1 To NbField
            If Error_Field(i) <> "" Then
                strSelect = strSelect & Num_Field(i) & "�" & Value_Field(i) & "�" & Error_Field(i) & "�"
            End If
        Next
        '-> Se deplacer � la fin de la ligne
        GoTo PrintInterface
    End If
                                   
    'V2RIFICATION SI LA DATA EXISTE DEJA
                                   
    '-> Pointer sur la table propri�t�aire de l'interface
    Set aFname = ServeurDOG.fNames(Ident & "|" & Fname_Name)
                                                                                   
    '-> Cas ou la table est en acces direct
    If aFname.IndexDirect <> "" Then
    
        '-> R�cup�ration de la valeur du champ de l'index direct
        For i = 1 To NbField
            If Num_Field(i) = aFname.IndexDirect Then Exit For
        Next
        
        '-> Initialisation de la chaine de la table pour la clause select
        strSelect = "SELECT TOP 1 * "
        strSelect = strSelect & "FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Ident & "�" & aFname.Num_Fname & "'"
        strSelect = strSelect & " AND [FIELD] ='" & aFname.IndexDirect & "' AND  [ZON_DATA] = '" & Value_Field(i) & "'"
                
    Else '-> Si on utilise un index pour verifier si la data existe
    
        If aInt.UseIndex Then
            
            '-> Recup de la liste des champs
            strTempo = GetTriIndexField(Fname_Name, aInt)
            
            strSelect = "SELECT TOP 1 * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & Ident & "�" & aFname.Num_Fname & "' "
                        
            '-> Pour tous les champs
            For i = 1 To NumEntries(strTempo, "|")
                strSelect = strSelect & " AND [ZON_INDEX_" & i & "] = '"
                '-> Chercher la valeur pour le champ de l'index
                For j = 1 To NbField
                    If Num_Field(j) = Entry(i, strTempo, "|") Then
                        strSelect = strSelect & Value_Field(j) & "'"
                        Exit For
                    End If
                Next
            Next
                        
        Else
            '-> Rien de sp�cifier donc CREATE
            strSelect = "B3D_CREATE~" & Ident & "�" & Fname_Name & "~"
            For i = 1 To NbField
                strSelect = strSelect & Num_Field(i) & "�" & Value_Field(i)
                If i <> NbField Then strSelect = strSelect & "�"
            Next
            
            '-> Imprimer la demande
            GoTo PrintInterface
            
        End If
        
    End If
    
    '-> Analyse de la base
    Set aRec = SetTable(strSelect)
    If aRec.EOF Then
        '-> L'enreg n'existe pas : fonction CREATE
        strSelect = "B3D_CREATE~" & Ident & "�" & Fname_Name & "~"
        For i = 1 To NbField
            strSelect = strSelect & Num_Field(i) & "�" & Value_Field(i)
            If i <> NbField Then strSelect = strSelect & "�"
        Next
    Else
    
        '-> Recup du num�ro d'enregistrement
        Num_Update = Trim(aRec.Fields("NUM_ENREG"))
        
        '-> Indiquer le mode Update
        strSelect = "B3D_UPDATE_LOCK~" & Ident & "�" & Fname_Name & "�" & Num_Update
        strSelect = strSelect & "~"
        strTempo = ""
        For i = 1 To NbField
            '-> Pointer sur le champ
            Set aIntField = aInt.Fields(Num_Field(i))
            '-> Tester si modifiable
            If aIntField.IsModif Then
                If strTempo <> "" Then
                    strTempo = strTempo & "�" & Num_Field(i) & "�" & Value_Field(i)
                Else
                    strTempo = Num_Field(i) & "�" & Value_Field(i)
                End If
            End If
        Next
        strSelect = strSelect & strTempo
    End If
                       
PrintInterface:
                       
    '-> Imprimer la demande dans le fichier interface
    Print #hdlDMD, strSelect

'-> Etiquette de rejet
NextLigne:

Loop 'Boucle d'analyse du fichier


GoTo EndSub

Exit Sub

GestError:
    
    '-> Imprimer une erreur
    Print #hdlDMD, "B3D_REJET~NUM_LIGNE_ASCII�" & NbLig & "~FATAL_ERROR~" & Err.Number & " - " & Err.Description
    
EndSub:
    
    '-> Fermer le fichier temporaire
    Close #hdlDMD
    
    '-> Supprimer le fichier demande s'il existe
    If Dir$(IntPath & FichierDemande, vbNormal) <> "" Then Kill IntPath & FichierDemande
    
    '-> Le renommer sus le nom de fichier de demande
    Name TempFileName As IntPath & FichierDemande
    
    '-> Fermer le fichier ascii
    Close #hdlASCII

    '-> Supprimer une ancienne version du fichier si elle existe
    If Dir$(IntSovPath & Entry(NumEntries(FichierASCII, "\"), FichierASCII, "\"), vbNormal) <> "" Then _
        Kill IntSovPath & Entry(NumEntries(FichierASCII, "\"), FichierASCII, "\")

    '-> Le renommer dans le bon directory
    Name FichierASCII As IntSovPath & Entry(NumEntries(FichierASCII, "\"), FichierASCII, "\")
    
    '-> Imprimer la r�ponse si besoin
    If CreateRspFile Then Call PrintReponse("B3D_INTERFACE~" & IntPath & FichierDemande & "�" & IntSovPath & Entry(NumEntries(FichierASCII, "\"), FichierASCII, "\"))


End Sub


Private Function GetTriIndexField(ByVal Fname_Name As String, ByRef aInt As Object) As String

'---> Cette fonction retourne la liste des champs de l'index de l'interface tri� par num�ro d'ordre

'-> Pour tri des champs
Dim strTri As String
Dim strNonTri As String

'-> Variable d'acces au sch�ma
Dim aFname As Object
Dim aIname As Object
Dim aIfield As Object

'-> Pointer sur la table
Set aFname = ServeurDOG.fNames(Ident & "|" & Fname_Name)

'-> Pointer sur l'index de l'interface
Set aIname = aFname.Inames(aInt.Index)

'-> Cr�ation de la liste des champs non tri�e
For Each aIfield In aIname.iFields
    If strNonTri = "" Then
        strNonTri = Format(aIfield.Ordre, "0000") & "�" & aIfield.Num_Field
    Else
        strNonTri = strNonTri & "|" & Format(aIfield.Ordre, "0000") & "�" & aIfield.Num_Field
    End If
Next

'-> Tri de la liste des champs
strTri = Tri(strNonTri, "|", "�")

'-> Retourner la liste
GetTriIndexField = strTri

End Function


Public Function EndProg()

'---> Proc�dure qui envoie le fichier de fermeture

'-> Envoyer un message au handle de communication
Dim res As Long

'-> Envoyer un ordre de fin
res = SetWindowText(HdlCom, "AGENTB3D_END_SESSION|" & IdAgent)
res = SendMessage(HdlCom, WM_PAINT, 0, 0)
DoEvents

End Function
Public Function FormatZone(ByVal strToFormat As String, ByVal Num_Field As String) As String

'-> Cette fonction formatte une valeur d�cimale

Dim msk As String
Dim msk1 As String
Dim aEmilieObj As Object


On Error GoTo GestError

'-> Pointer sur l'objet du catalogue
Set aEmilieObj = ServeurDOG.Catalogue(Ident & "|" & Num_Field)

Select Case Left$(UCase$(aEmilieObj.DataType), 3)
    Case "DEC"
    Case "DAT"
        FormatZone = FormatDate(strToFormat)
        Exit Function
    Case Else
        GoTo GestError
End Select


If Left$(UCase$(aEmilieObj.DataType), 3) <> "DEC" Then GoTo GestError

'-> Convertir au format decimal local
strToFormat = Convert(strToFormat)


If Not IsNumeric(strToFormat) Then GoTo GestError
    

msk = Format(Abs(CDec(strToFormat)), "###,###,###,###,###,###,###.00000000")
msk = Space$(36 - Len(msk)) & msk
msk1 = Mid$(msk, 1, InStr(1, msk, SepDec) + 2)
msk1 = RTrim(msk1 & Replace(msk, "0", " ", 31))
If CDec(strToFormat) < 0 Then msk1 = msk1 & "-"

FormatZone = Trim(msk1)

Exit Function

GestError:
    FormatZone = strToFormat

End Function


Public Function B3D_Get_Data_Val(ByVal Id_Fname As String, ByVal NumEnregistrement As String, _
                             ByVal Top_Lock As Integer, ByVal VecteurData As String, TopStat As Boolean) As String

'---> Cette fonction r�cup�re les datas et renvoie une chaine formatt�e

Dim aRec As Object
Dim RecLock As Object
Dim aRec2 As Object
Dim i As Integer, k As Integer, j As Integer
Dim DefData As String
Dim DefField As String
Dim DefJoin As String
Dim strRetour As String
Dim aEmilieObj As EmilieObj
Dim aObj As EmilieObj
Dim File_Jointure  As String
Dim FielDEALB3D_INDEX As String
Dim Num_Enreg_join As String
Dim aFname As Fname
Dim aField As Field
Dim aIname As Iname
Dim aIfield As IFIELD
Dim Liste_Field_join() As String
Dim Champ_ref As String
Dim Acces_Ref As String
Dim Soc_Ref As String
Dim Num_Enreg_Ref As String
Dim Ident As String
Dim Index_Value() As String
Dim strValue As String
Dim ValueField As String
Dim ValueJoin As String
Dim IsChampRef As Boolean
Dim ListeFieldJoinToAdd As String
Dim ValExtent As String
Dim strStat As String

On Error GoTo GestError

Ident = UCase$(Entry(1, Id_Fname, "�"))
SQLValue = ""

If Top_Lock = "1" Then
    
    '-> Faire un find dans DEALB3D_LOCK pour v�rfier si pas utilis� par un autre process

    '-> Cr�er la requete
    SQLValue = "SELECT * FROM [DEALB3D_LOCK] WHERE [ID_FNAME] = '" & Id_Fname & _
                           "' AND [NUM_ENREG] = '" & NumEnregistrement & "'"
    
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_GET_DATA_VAL : Mode Lock et v�rification de l'existance d'un lock : " & Chr(13) & Chr(10) & SQLValue)

    '-> Ex�cuter la requete
    Set RecLock = SetTable(SQLValue)
    
    '-> Cr�er le lock s'il n'existe pas
    If RecLock.EOF Then
    
        '-> Cr�er la requete
        SQLValue = "INSERT INTO [DEALB3D_LOCK] ( ID_FNAME , NUM_ENREG , FONCTION , DAT_LOCK" & _
                    ", HEU_LOCK ,  HDL_LOCK  , NUM_LOCK  , OPE_MAJ , PRG_MAJ ) VALUES ( " & _
                    "'" & Id_Fname & "' , '" & NumEnregistrement & "' , '" & "GET" & "' , '" & GetDate & "' , '" & GetTime & "' , '" & _
                    frmAgentB3D.hwnd & "' , '" & NumLockSpe & "' ,'" & Operateur & "' , '" & Programme & "' )"
    
        '-> Mode debug
        Call PrintDebug("B3D_GET_DATA_VAL : Mode lock : Cr�ation du lock : " & Chr(13) & Chr(10) & SQLValue)
    
        '-> Cr�ation Lock
        CreateEnreg SQLValue
    Else
        If CLng(RecLock.Fields("HDL_LOCK")) <> frmAgentB3D.hwnd Then
            '-> V�rifier que le lock soit actif
            If IsWindow(CLng(RecLock.Fields("HDL_LOCK"))) = 1 Then
                '-> Le lock existe d�ja : valeur de retour d'erreur
                B3D_Get_Data_Val = NumEnregistrement & "~ERROR~90006~" & GetMesseprog("ERRORS", "90006") & "~" & RecLock.Fields("OPE_MAJ") & "�" & RecLock.Fields("DAT_HEU_MAJ") & "�" & RecLock.Fields("PRG_MAJ") & "�1"
            Else
                '-> Le lock existe d�ja mais il n'est plus actif
                B3D_Get_Data_Val = NumEnregistrement & "~ERROR~90006~" & GetMesseprog("ERRORS", "90006") & "~" & RecLock.Fields("OPE_MAJ") & "�" & RecLock.Fields("DAT_HEU_MAJ") & "�" & RecLock.Fields("PRG_MAJ") & "�0"
            End If
            '-> Fermer le query
            RecLock.Close
            '-> Quitter la proc�dure
            Exit Function
        End If
    End If
    '-> Fermer le query
    RecLock.Close
End If

For i = 1 To NumEntries(VecteurData, "�")

    '-> R�cup�rer une condition
    DefData = Entry(i, VecteurData, "�")
    DefField = UCase$(Trim(Entry(1, DefData, "�")))
    DefJoin = CInt(Trim(Entry(2, DefData, "�")))
   
    '-> R�cup�rer le libell� du champ
    Set aEmilieObj = ServeurDOG.Catalogue(Ident & "|" & DefField)
    aEmilieObj.CodeLangue = CodeLangue
   
RepJoin:
   
    '-> Recup�rer la liste des champs de jointure � retourner
    If DefJoin = 1 And NumEntries(DefData, "�") > 2 Then
        '-> Init de la matrice
        Erase Liste_Field_join()
        ReDim Liste_Field_join(0)
        For j = 3 To NumEntries(DefData, "�")
            ReDim Preserve Liste_Field_join(UBound(Liste_Field_join()) + 1)
            Liste_Field_join(UBound(Liste_Field_join())) = UCase$(Trim(Entry(j, DefData, "�")))
        Next
    Else
        '-> On teste si on doit ramener les champs F10 de la  jointure
        If DefJoin = 1 Then 'Cas ou on veut les jointures en automatique ( Champ F10 de la table associ�e)
            '-> V�rifier qu'il y 'ait une table de correspondace
            If aEmilieObj.Table_Correspondance <> "" Then
                '-> Pointer sur la table associ�e
                Set aFname = ServeurDOG.fNames(Ident & "|" & aEmilieObj.Table_Correspondance)
                '-> Pour tous les champs
                For Each aField In aFname.Fields
                    '-> Ne prendre que les chalmps de type F10
                    If aField.IsF10 Then
                        '-> Pointer sur le champ associ� dans le catalogue
                        Set aObj = ServeurDOG.Catalogue(Ident & "|" & aField.Num_Field)
                        '-> Champ multilangue
                        If aObj.MultiLangue <> 0 And aObj.MultiLangue <> CodeLangue Then GoTo NextField
                        '-> Ne pas reprendre le champ de jointure
                        If aField.Num_Field = aFname.IndexDirect Then GoTo NextField
                        '-> Ajouter dans la matrice des champs
                        If ListeFieldJoinToAdd = "" Then
                            ListeFieldJoinToAdd = aField.Num_Field
                        Else
                            ListeFieldJoinToAdd = ListeFieldJoinToAdd & "�" & aField.Num_Field
                        End If
                    End If 'Si champ en recherche
NextField:
                Next 'Pour tous les champs
                '-> Concatainer la chaine des jointures
                DefData = DefData & "�" & ListeFieldJoinToAdd
                '-> Vider la variable
                ListeFieldJoinToAdd = ""
                '->Repartir sur la cr�ation du tableau des jointures
                GoTo RepJoin
            End If 'Si table de correspondance
        End If 'si jointure en auto
    End If 'S'il y a des champs de jointure
    
    '-> R�cup�rer les donn�es dans DEALB3D_DATA
    
    '-> Cr�er la requete
    SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname _
                                     & "' AND [NUM_ENREG] = '" & NumEnregistrement _
                                     & "' AND [FIELD] = '" & DefField & "'"
    
    '-> Mode debug
    Call PrintDebug("B3D_GET_DATA_VAL : R�cup�ration des donn�es dans DEALB3D_DATA :" & Chr(13) & Chr(10) & SQLValue)
    
    '-> Ex�cuter la requete
    Set aRec = SetTable(SQLValue)
    
    '-> Recup�rer les donn�es
    If Not aRec.EOF Then
           
        '-> Tester le cas des champs de type Extent
        Do While Not aRec.EOF
       
            '-> La valeur existe : prendre soit la valeur lock�e ou normale ( si pas lock�)
            ValueField = RTrim(DeCrypt(aRec.Fields("ZON_DATA")))
                    
            If Trim(aRec.Fields("EXTENT")) <> "" Then
                ValExtent = aRec.Fields("FIELD") & "_" & aRec.Fields("EXTENT")
            Else
                ValExtent = aRec.Fields("FIELD")
            End If
                    
            '-> Mettre a barreau les variables
            ValueJoin = ""
            Num_Enreg_join = ""
            File_Jointure = ""
            
            '-> Ici faire acc�s jointure
            If UCase$(DefJoin) = "1" And aRec.Fields("ENREG_JOIN") <> "" Then
             
                '-> R�cup�r�r la table de jointure
                File_Jointure = Entry(2, aRec.Fields("ENREG_JOIN"), "�")
                 
                '-> r�cup�rer le num�ro d'enregistrement de jointure
                Num_Enreg_join = Entry(3, aRec.Fields("ENREG_JOIN"), "�")
             
                '-> Pour tous les champs de jointure de la table
                For k = 1 To UBound(Liste_Field_join())
                    '-> V�rifier si on trouve l'enregistrement sp�cifi� dans la jointure
                     
                    '-> Cr�ation de la requete
                    SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Ident & "�" & _
                                          File_Jointure & "' AND [NUM_ENREG] = '" & Num_Enreg_join & _
                                          "' AND [FIELD] = '" & Liste_Field_join(k) & "'"
                                    
                    '-> Mode debug
                    Call PrintDebug("B3D_GET_DATA_VAL : R�cup�ration des valeurs des champs de jointure :" & Chr(13) & Chr(10) & SQLValue)
                    
                    '-> Ex�cuter la requete
                    Set aRec2 = SetTable(SQLValue)
                    
                    '-> R�cup�rer la valeur de la jointure
                    If Not aRec2.EOF Then
                        '-> Si on veut les stat
                        If TopStat Then
                            strStat = aRec.Fields("OPE_MAJ") & "|" & aRec.Fields("PRG_MAJ") & "|" & aRec.Fields("DAT_HEU_MAJ")
                        Else
                            strStat = "||"
                        End If
                        If Trim(ValueJoin) = "" Then
                            ValueJoin = Liste_Field_join(k) & "|" & RTrim(DeCrypt(aRec2.Fields("ZON_DATA"))) & "|" & strStat
                        Else
                            ValueJoin = ValueJoin & "�" & Liste_Field_join(k) & "|" & RTrim(DeCrypt(aRec2.Fields("ZON_DATA"))) & "|" & strStat
                        End If
                    Else
                        '-> On ne doit jamais passer ici sinon DELETE sauvage effectu� sur la base
                        If Trim(ValueJoin) = "" Then
                            ValueJoin = Liste_Field_join(k) & "|||"
                        Else
                            ValueJoin = ValueJoin & "�" & Liste_Field_join(k) & "|||"
                        End If
                    End If
                    '-> Fermer le recordset
                    aRec2.Close
                Next 'Pour tous les champs de jointure
            End If
                     
            If Num_Enreg_join <> "" Then Num_Enreg_join = UCase$(Trim(Entry(2, File_Jointure, "�"))) & "|" & Num_Enreg_join
            
            '-> Mettre � jour le recordset si c'est notre prog qui lock l'enregistrement
            'If Top_Lock = "1" Then aRec.Update
            
            '-> Si on veut les stat
            If TopStat Then
                strStat = aRec.Fields("OPE_MAJ") & "|" & aRec.Fields("PRG_MAJ") & "|" & aRec.Fields("DAT_HEU_MAJ")
            Else
                strStat = "|||"
            End If
            
            '-> Concat�ner la chaine de retour
            If strRetour = "" Then
                strRetour = ValExtent & "�" & FormatZone(ValueField, DefField) & "�" & strStat & "�" & Num_Enreg_join & "�" & ValueJoin
            Else
                strRetour = strRetour & "�" & ValExtent & "�" & FormatZone(ValueField, DefField) & "�" & strStat & "�" & Num_Enreg_join & "�" & ValueJoin
            End If
        
            '-> Enreg suivant
            aRec.MoveNext
            
        Loop 'Pour tous les extent d'un champ
    Else
       '-> La valeur n'existe pas
       If strRetour = "" Then
           strRetour = DefField & "����"
       Else
           strRetour = strRetour & "�" & DefField & "����"
       End If
   End If 'Si on trouve l'enregistrement demand�
Next

'-> Renvoyer la valeur des champs
B3D_Get_Data_Val = NumEnregistrement & "~OK~" & strRetour

'-> Fermer le query
aRec.Close

Exit Function


GestError:

    '-> Gestion des conflits de lock
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Mode debug
    Call PrintDebug("GET_DATA : " & Err.Number & "  " & Err.Description & Chr(13) & "Vecteur Data : " & VecteurData)
    
    '-> Renvoyer une valeur d'erreur
    B3D_Get_Data_Val = NumEnregistrement & "~ERROR~" & Err.Number & "~" & Err.Description & "~"


End Function
