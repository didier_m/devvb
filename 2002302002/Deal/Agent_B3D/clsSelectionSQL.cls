VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSelectionSQL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'********************************************************************
'* Claase qui repr�sente un champ s�lectionn� dans une requette SQL *
'********************************************************************

Public Num_Field As String '-> Num�ro du champ
Public Navigation As Boolean '-> Acces en navigation
Public Direct As Boolean '-> Acc�s Direct
Public Historique As Boolean '-> Acces en historique
Public Cumul As Boolean '-> Acces en Cumul
Public IsFieldJointure As Boolean '-> Iidque si le champ est une jointure
Public Conditions As Collection '-> Collection des conditions sur ce champ
Public IsIndex As Boolean '-> Indique que c'est un champ d'index
Public ListFieldJointure As String '-> Liste des champs de jointure associ�

Private IndexCdt As Integer '-> Compteur de condition

Private Sub Class_Initialize()

'-> Intialiser la collection
Set Conditions = New Collection

End Sub

Public Function AddCondition(ByVal Operande As Integer, ByVal ValMini As String, ByVal ValMaxi As String) As String

'---> Cette fonction ajoute une conditon � un champ de s�lection

Dim aCdt As Condition

Set aCdt = New Condition
aCdt.cdt_Mini = ValMini
aCdt.cdt_Maxi = ValMaxi
aCdt.Operande = Operande
aCdt.Key = "CDT" & IndexCdt + 1

'-> Ajout dans la collection
Conditions.Add aCdt, "CDT" & IndexCdt + 1

'-> Renvoyer la cl�
AddCondition = aCdt.Key

'-> Incr�menter le compteur de condition
IndexCdt = IndexCdt + 1

End Function

Public Function GetLibelCondition(ByVal Id_Libel As Integer) As String

Select Case Id_Libel

    Case 0
        GetLibelCondition = "Egal"
    Case 1
        GetLibelCondition = "Compris entre"
    Case 2
        GetLibelCondition = "Non compris entre"
    Case 3
        GetLibelCondition = "Diff�rent"
    Case 4
        GetLibelCondition = "Inf�rieur ou �gal"
    Case 5
        GetLibelCondition = "Sup�rieur ou �gal"
    Case 6
        GetLibelCondition = "Inf�rieur"
    Case 7
        GetLibelCondition = "Sup�rieur"
    Case 8
        GetLibelCondition = "Commence par"
    Case 9
        GetLibelCondition = "Contient"
End Select



End Function
