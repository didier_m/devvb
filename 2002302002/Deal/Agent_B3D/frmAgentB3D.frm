VERSION 5.00
Begin VB.Form frmAgentB3D 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Agent B3D"
   ClientHeight    =   1245
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   2070
   Icon            =   "frmAgentB3D.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1245
   ScaleWidth      =   2070
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   120
      Top             =   120
   End
End
Attribute VB_Name = "frmAgentB3D"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'---> Pour Analyse du descriptif
Dim FnameBIS As Collection '
Dim CatalogueBIS As Collection ' Pour descriptif du catalogue

'---> Pour gestion des buffers
Public Buffers As Collection  'Collection des buffers m�moires

'-> Collection des instructions d'un fichier ASCII
Dim Instructions As Collection

Private Sub Form_Load()

'---> Initialiser la collection des buffers
Set Buffers = New Collection

End Sub

Private Sub Form_Unload(Cancel As Integer)

'-> Fermer les connexions base
Call CloseConnectB3D

'-> Quitter
End

End Sub
Private Function OpenDemande(FichierDemande As String) As Boolean

'-> Pour lecture du fichier ASCII
Dim hdlFile As Integer
Dim Ligne As String
Dim NbError As Integer

On Error GoTo GestError

'-> RAZ de la variable de compteur d'erreur
NbError = 0

'-> V�rifier que le fichier ASCII soir bien pr�sent
If Dir$(FichierDemande) = "" Then Exit Function

'-> Point d'entr�e de reprise
NextOpen:

'-> Ouverture du fichier ASCII
hdlFile = FreeFile
Open FichierDemande For Input As #hdlFile

'-> Initialiser la liste des instructions
Set Instructions = New Collection

'-> Lecture s�quentielle du fichier ASCII
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> Ne pas traiter les lignes diff�rentes de ""
    If Trim(Ligne) = "" Then GoTo NextLig
    '-> Ajouter dans la collection des demandes
    Instructions.Add DeCrypt(Ligne)
NextLig:
Loop

'-> Fermer le fichier ASCII contenant les instructions
Close #hdlFile

'-> Supprimer le fichier de donn�es
If Dir$(FichierDemande) <> "" Then Kill FichierDemande

'-> Retourner une valeur de succ�s
OpenDemande = True

'-> Quitter la proc�dure
Exit Function

'-> Gestion des erreur
GestError:
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure OpenDemande : " & Err.Number & " - " & Err.Description)
    
    '-> Traiter l'erreur
    Select Case Err.Number
        Case 52, 53, 55, 62, 75, 76
        
            '-> Incr�menter le compteur d'erreur
            NbError = NbError + 1
            
            '-> Temporiser
            Sleep 10
            DoEvents
            
            '-> Boucler en cas d'erreur
            If NbError < 5 Then GoTo NextOpen
            
            '-> Fermer le fichier de demande
            On Error Resume Next
            If hdlFile <> 0 Then Close #hdlFile
            
            '-> Essayer de le supprimer
            If Dir(FichierDemande) <> "" Then Kill FichierDemande
    End Select
        
    '-> Butter la collection des demandes
    Set Instructions = Nothing

    '-> Retourner une r�ponse
    Call PrintReponse("Erreur dans la proc�dure OpenCommande~ERROR~" & Err.Number & "~" & Err.Description)
    
    '-> Fermer la r�ponse
    Call CloseReponse
                                        
End Function


Private Sub AnalyseDemande()

'---> Cette proc�dure analyse les demandes d'un fichier ASCII

Dim i As Integer
Dim res As Long

'-> Pour identifiant de l'instuction � traiter
Dim B3DInstruction As String

On Error GoTo GestError

'-> Indiquer � la console que l'agent B3D est en train de travailler
res = SetWindowText(HdlCom, "AGENTB3D_START_WORKING|" & IdAgent)
res = SendMessage(HdlCom, WM_PAINT, 0, 0)

'-> DEBUGMODE
Call PrintDebug("Analyse Demande : Instructions � traiter : " & Instructions.Count)

'-> Ouvrir le fichier des r�ponse
Call OpenReponse

'-> Envoyer le compteur
WritePrivateProfileString "CPT", "MAX", CStr(Instructions.Count), CptFile

'-> Analyse et traitement des instructions B3D
For i = 1 To Instructions.Count
    '-> Envoyer l'instruction lue
    WritePrivateProfileString "CPT", "CRS", CStr(i), CptFile
    '-> R�cup�rer l'instruction
    B3DInstruction = Entry(1, Instructions(i), "~")
    '-> Mise � jour des LOG
    If TraceMode Then PrintTrace (Instructions(i))
    '-> DEBUGMODE
    Call PrintDebug("Traitement de l'instruction N� : " & i & " � " & Now & " -> : " & Instructions(i))
    '-> Selon l'instruction
    Select Case UCase$(Trim(B3DInstruction))
        '*******************************************
        '* Fonction SQL
        '*******************************************
        
        Case "B3D_SQL" '-> execution d'une requete SQL
            B3D_SQL Instructions(i)
        
        '*******************************************
        '* FONCTIONS DE PARAMETRAGE DE L'AGENT B3D *
        '*******************************************
        
        Case "B3D_LOCKSPE" 'Modification du num�ro de lock sp�cifique associ� � l'ident
            B3D_LOCKSPE
        Case "B3D_TRACE" 'Indiquer le niveau de stat
            B3D_TRACE Instructions(i)
        Case "B3D_PROG" 'Modifier le prog affect�
            B3D_PROG Instructions(i)
        Case "B3D_CRYPT" 'indique si on crypte ou non les �changes
            B3D_CRYPT Instructions(i)
        Case "B3D_DEBUG" 'Indique que l'on passe ou non en mode debug
            B3D_DEBUG Instructions(i)
        Case "B3D_EXIT" '-> Quitter l'agent B3D
            B3D_EXIT
                            
        '**************************************************
        '* FONCTIONS DE DESCRIPTIFS DE TABLE ET DE CHAMPS *
        '**************************************************
        Case "B3D_SCHEMA_CATALOGUE" 'Get d'une d�finition d'un champ
            B3D_SCHEMA_CATALOGUE Instructions(i)
        Case "B3D_FIELD_TABLE" 'R�cup�ration des champs associ�s � une table de correspondance F10
            B3D_FIELD_TABLE Instructions(i)
        Case "B3D_CATALOGUE" 'R�cup�ration des objets du catalogue
            B3D_CATALOGUE Instructions(i)
        Case "B3D_CATALOGUE_LINK" 'R�cup�ration des Liens entre Table et objets
            B3D_CATALOGUE_LINK
        Case "B3D_SCHEMA_LIST" 'R�cup�ration de la liste des tables pour un IDENT
            B3D_SCHEMA_LIST Instructions(i)
        Case "B3D_SCHEMA_DES" 'R�cup�ration du descriptif d'une table pour un IDENT
            B3D_SCHEMA_DES Instructions(i)
        Case "B3D_SCHEMA_LFIELD" 'Liens d'un champ dans le sch�ma
            B3D_SCHEMA_LFIELD Instructions(i)
        Case "B3D_SEEK_DATA" 'V�rifier si pour un champ donn� il existe au moins une valeur
            B3D_SEEK_DATA Instructions(i)
        Case "B3D_SEARCH_DATA" 'R�cup�re  pour un champ donn� les enreg
            B3D_SEARCH_DATA Instructions(i)
        Case "B3D_SEEK_JOIN" 'V�rification de la pr�sence d'une jointure
            B3D_SEEK_JOIN Instructions(i)
        Case "B3D_SEARCH_JOIN" 'Recherche des jointures pour un enreg
            B3D_SEARCH_JOIN Instructions(i)
        Case "B3D_LOAD_DOG" 'Permet de recharger le param�trage du dog en cours
            B3D_LOAD_DOG
            
        '********************************************
        '* FONCTIONS D'ACCES AUX BUFFERS DE DONNEES *
        '********************************************
        
        Case "B3D_ERASE_BUFFER" 'Supprimer une buffer
            B3D_ERASE_BUFFER Instructions(i)
        Case "B3D_SELECT" 'Renvoie de TOUS les num enreg d'une clause SELECT
            B3D_SELECT Instructions(i)
        
         '**********************************************
        '* FONCTIONS DE NAVIGATION ET DE CONSULTATION *
        '**********************************************
               
        Case "B3D_NAVIGA_SELECT" 'Cr�er un buffer d'une taille et initialise les pages de navigation
            B3D_NAVIGA_SELECT Instructions(i)
        Case "B3D_NAVIGA" 'Navigation dans un buffer
            B3D_NAVIGA Instructions(i)
            
        '***************************************
        '* FONCTIONS DE MISE A JOUR DE LA BASE *
        '***************************************
        Case "B3D_ERASE_FILE" '-> Vidage de la base
            B3D_ERASE_FILE Instructions(i)
        Case "B3D_INTERFACE" '->Int�gration d'un interface
            B3D_INTERFACE Instructions(i)
        Case "B3D_DELETE" 'Suppression d'un enregistrement
            B3D_DELETE Instructions(i)
        Case "B3D_UPDATE", "B3D_UPDATE_LOCK" 'Mise � jour avec ou sans lock
            B3D_UPDATE Instructions(i)
        Case "B3D_UPDATE_SPE" 'Mise � jour d'un champ d'index
            B3D_UPDATE_SPE Instructions(i)
        Case "B3D_REINDEX" 'Ordre de r�indexation de la base
            B3D_REINDEX Instructions(i)
        Case "B3D_UNLOCK" 'Ordre de Delock d'un enreg
            B3D_UNLOCK Instructions(i)
        Case "B3D_UNLOCK_SPE" 'Ordre de Delock d'un enreg
            B3D_UNLOCK_SPE Instructions(i)
        Case "B3D_CREATE" 'Cr�ation d'un nouvel enregistrement
            B3D_CREATE Instructions(i)
        
        '****************************************************
        '* FONCTIONS D'INFORMATIONS SUR LES ENREGISTREMENTS *
        '****************************************************
        Case "B3D_FIND_HISTO" 'R�cup�ration des histos � partir d'un numenreg
            B3D_FIND_HISTO Instructions(i)
        Case "B3D_GET_HISTO" 'R�cup�ration des historiques de numenreg et des fields
            B3D_GET_HISTO Instructions(i)
        Case "B3D_GET_ENREG" 'R�cup�ration d'un num�ro d'enregistrement
            B3D_GET_ENREG Instructions(i)
        Case "B3D_GET_LOCK" 'R�cup�ration de la table des locks en cours
            B3D_GET_LOCK
        Case "B3D_FIND_DATA" 'Acc�s � toutes les donn�es
            B3D_FIND_DATA Instructions(i)
        Case "B3D_GET_DATA" 'Acces aux donn�es sp�cifi�es
            B3D_GET_DATA Instructions(i)
        Case "B3D_GET_SOCREF" 'Get de la soci�t� de reference
            B3D_GET_SOCREF Instructions(i)
            
        '**************************************
        '* FONCTIONS DE REPLICATION ET EXPORT *
        '**************************************
        
        Case "B3D_REPLICA_EXPORT" 'Exporter la base de r�plication
            B3D_REPLICA_EXPORT Instructions(i)
        
        Case "B3D_REPLICA_CREATE" 'Pour cr�er un enreg � partir d'une r�plication
            B3D_REPLICA_FONCTION Instructions(i)
        
        Case "B3D_REPLICA_UPDATE" 'Pour cr�er un enreg � partir d'une r�plication
            B3D_REPLICA_FONCTION Instructions(i)
        
        Case "B3D_REPLICA_DELETE" 'Pour cr�er un enreg � partir d'une r�plication
            B3D_REPLICA_FONCTION Instructions(i)
        
        Case "B3D_EXPORT"  '-> Export d'une table pour r�plication
            B3D_EXPORT Instructions(i)
            
    End Select 'Selon l'instruction B3D � traiter
    
    '-> DEBUGMODE
    Call PrintDebug("Fin de traitement de l'instruction : " & i & " -> " & Now)
Next 'Pour toutes les instructions du fichier Demande B3D

'-> Fermer la r�ponse
Call CloseReponse

'-> Indiquer � la console que l'agent B3D n'est plus en train de travailler
res = SetWindowText(HdlCom, "AGENTB3D_STOP_WORKING|" & IdAgent)
res = SendMessage(HdlCom, WM_PAINT, 0, 0)

'-> Quitter la fonction
Exit Sub

GestError:

    '-> Faire un debug
    Call PrintDebug("Erreur dans la proc�dure Analyse Demande : " & Chr(13) & Chr(10) & Err.Number & " - " & Err.Description)

    '-> Fermer le fichier r�ponse
    Call ResetReponse

    '-> Envoyer un message d'erreur
    Call PrintReponse("Erreur dans la proc�dure Analyse Demande~ERROR~" & Err.Number & "~" & Err.Description)

    '-> Indiquer � la console que l'agent B3D n'est plus en train de travailler
    res = SetWindowText(HdlCom, "AGENTB3D_STOP_WORKING|" & IdAgent)
    res = SendMessage(HdlCom, WM_PAINT, 0, 0)
    
    '-> Vider la collection des instructions
    Set Instructions = Nothing

    '-> Envoyer la r�ponse
    Call CloseReponse
    
End Sub

Private Sub B3D_ERASE_FILE(Instruction As String)

'---> Cette proc�dure permet de  vider une base des enreg affect�s � une table donn�e

'-> Syntaxe : B3D_ERASE_FILE~[Ident]�[Fname]|[Fname] ...
'Si fname = "" toutes les tables

Dim strIdent As String
Dim strFname As String
Dim aFname As Fname
Dim strTable As String
Dim i As Integer
Dim aRec As Object
Dim SQLValue As String

On Error GoTo GestError

'-> Get de l'ident
strIdent = Entry(1, Entry(2, Instruction, "~"), "�")

'-> Get de la liste des tables
strFname = Entry(2, Entry(2, Instruction, "~"), "�")

'-> Si liste des tables � blanc -> Prendre toutes les tables
If Trim(strFname) = "" Then
    For Each aFname In ServeurDOG.fNames
        If Trim(strFname) = "" Then
            strFname = aFname.Num_Fname
        Else
            strFname = strFname & "|" & aFname.Num_Fname
        End If
    Next
End If

'-> Suppression des datas pour les tables s�lectionn�es
If Trim(strFname) = "" Then
    '-> Renvoyer un message d'erreur
    Call PrintReponse("B3D_ERASE_FILE~ERROR~90027~" & GetMesseprog("ERRORS", "90027") & "~")
    Exit Sub
End If

'-> Suppression pour toutes les tables
For i = 1 To NumEntries(strFname, "|")
    '-> Get d'une def de table
    strTable = Entry(i, strFname, "|")
    '-> Envoyer une requete DEALB3D_DATA
    SQLValue = "DELETE * FROM DEALB3D_DATA WHERE [ID_FNAME] LIKE '" & strIdent & "�" & strTable & "*'"
    CreateEnreg SQLValue
    '-> Envoyer une requete DEALB3D_INDEX
    SQLValue = "DELETE * FROM DEALB3D_INDEX WHERE [ID_FNAME] LIKE '" & strIdent & "�" & strTable & "*'"
    CreateEnreg SQLValue
    '-> Envoyer une requete DEALB3D_ENREG
    SQLValue = "DELETE * FROM DEALB3D_ENREG WHERE [ID_FNAME] LIKE '" & strIdent & "�" & strTable & "*'"
    CreateEnreg SQLValue
    '-> Envoyer une requete DEALB3D_HISTO
    SQLValue = "DELETE * FROM DEALB3D_HISTO WHERE [ID_FNAME] LIKE '" & strIdent & "�" & strTable & "*'"
    CreateEnreg SQLValue
    '-> Envoyer une requete DEALB3D_RCOPY
    SQLValue = "DELETE * FROM DEALB3D_RCOPY WHERE [ID_FNAME] LIKE '" & strIdent & "�" & strTable & "*'"
    CreateEnreg SQLValue
    '-> Envoyer une requete DEALB3D_CORINT
    SQLValue = "DELETE * FROM DEALB3D_CORINT WHERE [ID_FNAME] LIKE '" & strIdent & "�" & strTable & "*'"
    CreateEnreg SQLValue
Next 'Pour toutes les tables

'-> Envoyer une r�ponse de succ�s
Call PrintReponse("B3D_ERASE_FILE~OK~")

'-> Quitter la proc�dure
Exit Sub

GestError:

    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Vider la r�ponse
    Call ResetReponse
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure B3D_ERASE_FILE " & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description & "~" & SQLValue)

    '-> Renvoyer une r�ponse
    Call PrintReponse("B3D_ERASE_FILE~ERROR~" & Err.Number & "~" & Err.Description & "~")


End Sub

Private Sub B3D_LOAD_DOG()

'---> Cette proc�dure permet de recharcger le DOG

On Error GoTo GestError

'-> Dans un premier temps, vider le catalogue
Do While ServeurDOG.Catalogue.Count <> 0
    '-> Supprimer l'objet
    ServeurDOG.Catalogue.Remove (1)
Loop

'-> Vider les tables
Do While ServeurDOG.fNames.Count <> 0
    '-> Supprimer les objets
    ServeurDOG.fNames.Remove (1)
Loop

'-> Recharger le catalogue
Call ConnectDOG

'-> Renvoyer une valeur de succ�s
Call PrintReponse("B3D_LOAD_DOG~OK")

'-> Quitter la proc�dure
Exit Sub

GestError:

    Call ResetReponse
    Call PrintReponse("B3D_LOAD_DOG~ERROR~" & Err.Number & "~" & Err.Description & "~")
    

End Sub

Private Sub B3D_REPLICA_EXPORT(Instruction As String)

'---> Cette proc�dure exporte DEALB3D_DATA � partir de la table de r�plication
'Syntaxe : B3D_REPLICA_EXPORT~[Ident]�[Fname]�[Fichier ASCII]�[Top_RAZ]�[Top_Crypt]
'Retour = B3D_REPLICA_EXPORT~OK~[Id_Fname]�[FichierAscii]�[NbCreate]�[NbUpdate]�[NbDelete]

Dim Id_Fname As String
Dim AsciiFile As String
Dim hdlFile As Integer
Dim Param As String
Dim Top_Raz As Boolean
Dim aFname As Fname
Dim Top_Crypt As Boolean

Dim NbUpdate As Long
Dim NbCreate As Long
Dim NbDelete As Long
Dim Fonction As String

Dim aEmilieObj As EmilieObj
Dim Ligne As String
Dim Num_Enreg As String
Dim Rec_Data As Object
Dim Rec_Copy As Object

On Error GoTo GestError

'-> Analyse des param
Param = Entry(2, Instruction, "~")
Id_Fname = Entry(1, Param, "�") & "�" & Entry(2, Param, "�")
AsciiFile = Entry(3, Param, "�")
Top_Raz = CBool(Entry(4, Param, "�"))
Top_Crypt = CBool(Entry(5, Param, "�"))

'-> V�rifier si table � r�plication
Set aFname = ServeurDOG.fNames(Replace(Id_Fname, "�", "|"))
If Not aFname.IsReplica Then
    Call PrintReponse("B3D_REPLICA_EXPORT~ERROR~90003~" & GetMesseprog("ERRORS", "90003") & "~")
    Exit Sub
End If

'-> Pr�paration de la requete
SQLValue = "SELECT * FROM [DEALB3D_RCOPY] WHERE [ID_FNAME] =  '" & Id_Fname & "'"

'-> Mode debug
Call PrintDebug("Proc�dure B3D_REPLICA_EXPORT : Acc�s � la table de r�plication : " & Chr(13) & Chr(10) & SQLValue)

'-> Get d'un pointeur vers la table de r�plication
Set Rec_Copy = SetTable(SQLValue)

'-> Tester qu'il ya quelquechose � r�pliquer
If Rec_Copy.EOF Then
    Call PrintReponse("B3D_REPLICA_EXPORT~ERROR~90004~" & GetMesseprog("ERRORS", "90004") & "~")
    Exit Sub
End If

'-> Ouvrir le fichier ASCII de destination
hdlFile = FreeFile
Open AsciiFile For Append As #hdlFile

'-> Forcer la ligne d'instruction programme
Ligne = "B3D_PROG~REPLICA_" & Uc_ID & "-" & GetDate & "-" & GetTime
If Top_Crypt Then Ligne = Crypt(Ligne)
Print #hdlFile, Ligne

'-> Analyse de la table de replica
Do While Not Rec_Copy.EOF
    '-> Rendre la main � la CPU
    DoEvents
    '-> Selon la fonction de r�plication
    Fonction = Rec_Copy.Fields("FONCTION")
    Num_Enreg = Rec_Copy.Fields("NUM_ENREG")
    
    If Fonction = "DELETE" Then
        '-> Envoyer l'ordre dans le fichier ASCII
        Ligne = "B3D_REPLICA_DELETE~" & Id_Fname & "�" & Num_Enreg
        If Top_Crypt Then Ligne = Crypt(Ligne)
        Print #hdlFile, Ligne
        NbDelete = NbDelete + 1
    Else
        '-> Maj des stats
        If Fonction = "CREATE" Then
            NbCreate = NbCreate + 1
        Else
            NbUpdate = NbUpdate + 1
        End If
                
        Ligne = "B3D_REPLICA_" & Fonction & "~" & Id_Fname & "�" & Num_Enreg & "~"
        
        '-> Pr�parer la requete
        SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] =  '" & Id_Fname & "' AND [NUM_ENREG] =  '" & Num_Enreg & "'"
        
        '-> Mode debug
        Call PrintDebug("Proc�dure B3D_REPLICA_EXPORT -> Analyse des champs en r�plication :" & Chr(13) & Chr(10) & SQLValue)
        
        '-> Boucler pour tous les champs en r�plication dans DEALB3D_DATA
        Set Rec_Data = SetTable(SQLValue)
        
        Do While Not Rec_Data.EOF
            '-> Rendre la main � la CPU
            DoEvents
            '-> Tester que le champ est en r�plica
            If aFname.Fields(Rec_Data.Fields("FIELD")).IsReplica Then
                If Trim(Rec_Data.Fields("EXTENT")) = "" Then
                    Ligne = Ligne & Rec_Data.Fields("FIELD") & "�" & DeCrypt(Rec_Data.Fields("ZON_DATA")) & "�"
                Else
                    Ligne = Ligne & Rec_Data.Fields("FIELD") & "_" & Rec_Data.Fields("EXTENT") & "�" & DeCrypt(Rec_Data.Fields("ZON_DATA")) & "�"
                End If
            End If 'Si le champ est suivi en replica dans la table
            Rec_Data.MoveNext
        Loop 'Pour tous les enreg de DEALB3D_DATA
        '-> Fermer le recordset
        Rec_Data.Close
        '-> Envoyer la r�ponse dans le fichier ascii
        Ligne = Mid$(Ligne, 1, Len(Ligne) - 1)
        If Top_Crypt Then Ligne = Crypt(Ligne)
        Print #hdlFile, Ligne
    End If 'Selon la fonction
           
    '-> Si on n'export qu'une seule fois
    If Top_Raz Then Rec_Copy.Delete
    
    '-> Enreg suivant
    Rec_Copy.MoveNext
Loop

'-> Fermer le recordset
Rec_Copy.Close

'-> Fermer le fichier ASCII
Close #hdlFile

'-> Envoyer la r�ponse
Call PrintReponse("B3D_REPLICA_EXPORT~OK~" & Id_Fname & "�" & AsciiFile & "�" & NbCreate & "�" & NbUpdate & "�" & NbDelete)

'-> Quitter la fonction
Exit Sub

GestError:

    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Faire un reset de la reponse
    Call ResetReponse
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure B3D_REPLICA_EXPORT : " & Chr(13) & Chr(10) & Err.Number & "-" & Err.Description)
    
    '-> Retourner la r�ponse
    Call PrintReponse("B3D_REPLICA_EXPORT~ERROR~" & Err.Number & "~" & Err.Description & "~")

End Sub

Private Sub B3D_REPLICA_FONCTION(Instruction As String)

'---> Cette proc�dure met � jour DEALB3D_DATA � partir d'un fichier ASCII

'Syntaxe B3D_REPLICA_[FONCTION]~[Ident]�[Fname]�[Num_Enreg]~[Field]�[Value]�...

Dim Param As String
Dim Fonction As String
Dim Id_Fname As String
Dim Num_Enreg As String
Dim DefField As String
Dim Rec_Data As Object
Dim VecteurData As String
Dim aFname As Fname
Dim ValueField As String
Dim aRec As Object
Dim aIname As Iname
Dim Champ_Index(1 To 15) As String
Dim i As Integer
Dim aField As Field
Dim aIfield As IFIELD

On Error GoTo GestError

'-> Analyse des qparam�tres
Param = Entry(2, Instruction, "~")
Id_Fname = Entry(1, Param, "�") & "�" & Entry(2, Param, "�")
Num_Enreg = Trim(UCase$(Entry(3, Param, "�")))
VecteurData = Entry(3, Instruction, "~")

'---> Pierrot : Tester si l'on doit chercher l'enreg( exple : on recoit _
      des instructions REPLICA de l'ERP DEAL qui sont corrects sauf NoEnreg remplac� _
      par #SEARCH#
    
      
If Num_Enreg = "#SEARCH#" Then
    '-> recherche sur index
    '-> Pointer sur la table
    Set aFname = ServeurDOG.fNames(Replace(Id_Fname, "�", "|"))
    
    '-> Si table � acc�s direct
    If aFname.IndexDirect <> "" Then
        '-> Acc�s � DEALB3D_DATA avec le champ de r�f�rence pour v�rifier si pas d�ja existant
        ValueField = LookUpField(VecteurData, aFname.IndexDirect)
        
        '-> Pr�parer la requete
        SQLValue = "SELECT top 1 * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [FIELD] = '" & aFname.IndexDirect & "' AND [ZON_DATA] =  '" & ValueField & "'"

        '-> Acc�der � la base
        Set aRec = SetTable(SQLValue)
        
        '-> Si existe -> Erreur duplicate KEY
        If Not aRec.EOF Then
            '-> Cas ou l'enreg existe deja : on recupere
            Num_Enreg = aRec.Fields("NUM_ENREG")
            '-> Retourner une erreur
        Else
            '-> On passe le NoEnreg a "" pour indiquer que create
            Num_Enreg = ""
        End If
        '-> Fermer le query
        aRec.Close
        
    Else 'Table avec index
        
        For Each aIname In aFname.Inames
        
            '-> teste si index = index de jointure : alors on traite
            If aFname.IndexJointure = aIname.Num_Iname Then
            
        
                '-> Vider la matrice d'acc�s
                Erase Champ_Index()
                i = 1
                '-> Pour tous les champs
                For Each aIfield In aIname.iFields
                    Champ_Index(i) = LookUpField(VecteurData, aIfield.Num_Field)
                    i = i + 1
                Next 'Pour tous les champs de l'index
            
                '-> Faire un acc�s sur DEALB3D_INDEX pour trouver l'index
                '-> Pr�parer la requete
                SQLValue = "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & _
                                       UCase$(Id_Fname) & "' AND [INAME] = '" & aIname.Num_Iname & "' AND " & _
                                       "[ZON_INDEX_1] = '" & Champ_Index(1) & "' AND " & _
                                       "[ZON_INDEX_2] = '" & Champ_Index(2) & "' AND " & _
                                       "[ZON_INDEX_3] = '" & Champ_Index(3) & "' AND " & _
                                       "[ZON_INDEX_4] = '" & Champ_Index(4) & "' AND " & _
                                       "[ZON_INDEX_5] = '" & Champ_Index(5) & "' AND " & _
                                       "[ZON_INDEX_6] = '" & Champ_Index(6) & "' AND " & _
                                       "[ZON_INDEX_7] = '" & Champ_Index(7) & "' AND " & _
                                       "[ZON_INDEX_8] = '" & Champ_Index(8) & "' AND " & _
                                       "[ZON_INDEX_9] = '" & Champ_Index(9) & "' AND " & _
                                       "[ZON_INDEX_10] = '" & Champ_Index(10) & "' AND " & _
                                       "[ZON_INDEX_11] = '" & Champ_Index(11) & "' AND " & _
                                       "[ZON_INDEX_12] = '" & Champ_Index(12) & "' AND " & _
                                       "[ZON_INDEX_13] = '" & Champ_Index(13) & "' AND " & _
                                       "[ZON_INDEX_14] = '" & Champ_Index(14) & "' AND " & _
                                       "[ZON_INDEX_15] = '" & Champ_Index(15) & "'"
        
                '-> Ex�cuter la requete
                Set aRec = SetTable(SQLValue)
                
                '-> V�rifier qu'il n'existe pas
                If Not aRec.EOF Then
                    Num_Enreg = aRec.Fields("NUM_ENREG")
                Else
                    Num_Enreg = ""
                End If
                
                '-> Fermer le query
                aRec.Close
                
                Exit For
                
            End If '-> Test si index en cours = index jointure
NextIname:
        Next 'Pour tous les index
    End If 'Selon l'acc�s de la table
    
    '-> On genere un NoEnreg si on l'a pas trouve
    If Num_Enreg = "" Then
        Num_Enreg = Get_NUM_ENREG(Id_Fname)
    End If
    
    '-> On remplace la valeur #SEARCH# par le NoEnreg gener�
    Instruction = Replace(Instruction, "#SEARCH#", Num_Enreg)
    
End If


'-> Get de la fonction
Fonction = UCase$(Trim(Entry(3, Entry(1, Instruction, "~"), "_")))
If Fonction = "DELETE" Then 'DELETE
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_REPLICA_FONCTION : Delete ")
    '-> Ex�cuter
    Call B3D_DELETE(Instruction, True)
Else 'UPDATE et CREATE
    '-> Pr�parer la requete
    SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] =  '" & Id_Fname & "' AND [NUM_ENREG] =  '" & Num_Enreg & "'"
        
    '-> Mode debug
    Call PrintDebug("B3D_REPLICA_FONCTION : " & Fonction & " V�rifier si l'enreg que l'on traite n'existe pas d�ja" & Chr(13) & Chr(10) & SQLValue)
    
    '-> V�rifier si l'enreg que l'on traite n'existe pas d�ja
    Set Rec_Data = SetTable(SQLValue)
    
    If Rec_Data.EOF Then
        '-> L'enreg n'existe pas : Create
        Fonction = "B3D_CREATE~" & Id_Fname & "~" & Entry(3, Instruction, "~")
        '-> Mode debug
        Call PrintDebug("Proc�dure B3D_REPLICA_FONCTION : Create ")
        '-> Run de la fonction
        Call B3D_CREATE(Fonction, Num_Enreg)
    Else
        '-> l'enreg existe -> Update
        Fonction = "B3D_UPDATE_LOCK~" & Entry(2, Instruction, "~") & "~" & Entry(3, Instruction, "~")
        '-> Mode debug
        Call PrintDebug("Proc�dure B3D_REPLICA_FONCTION : Update ")
        '-> Run de la fonction
        Call B3D_UPDATE(Fonction, True)
    End If 'Si l'enreg existe
End If 'Selon la fonction � traiter


Exit Sub

GestError:

    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Vider la r�ponse
    Call ResetReponse
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure B3D_REPLICA " & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)

    '-> Renvoyer une r�ponse
    Call PrintReponse("B3D_REPLICA_FONCTION~ERROR~" & Err.Number & "~" & Err.Description & "~")

End Sub

Public Sub B3D_EXPORT(Instruction As String)

'-> Cette fonction exporte une table pour la remonter autre part
'-> Syntaxe : B3D_EXPORT~[Ident]�[Fname]�[Index]�[FichierASCII]�[Top_Crypt]

Dim Param As String
Dim Id_Fname As String
Dim FileExport As String
Dim ErrorCode As String
Dim Rec_Data As Object
Dim aFname As Object
Dim IndexParam As String
Dim Iname As Object
Dim Rec_Index As Object
Dim hdlFile As Integer
Dim Num_Enreg As String
Dim Ligne As String
Dim Top_Crypt As Boolean
Dim NbLignes As Long

On Error GoTo GestError

'-> R�cup�rer les param�tres
Param = Entry(2, Instruction, "~")
Id_Fname = Entry(1, Param, "�") & "�" & Entry(2, Param, "�")
IndexParam = Entry(3, Param, "�")
FileExport = Entry(4, Param, "�")
If Entry(5, Param, "�") = "1" Then Top_Crypt = True

'-> Constituer le nom du fichier
FileExport = ReplicaOutPutPath & FileExport

'-> Mode debug
Call PrintDebug("B3D_EXPORT :  Fichier � cr�er : " & FileExport)

'-> Pointer sur la table sp�cifi�e
Set aFname = ServeurDOG.fNames(Replace(Id_Fname, "�", "|"))

'-> Selon en acc�s direct ou � index
If aFname.IndexDirect <> "" Then '-> La table est en acc�s direct
    '-> Pr�paration de la requete
    SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] ='" & Id_Fname & "' AND [FIELD] = '" & aFname.IndexDirect & "'"

    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_EXPORT : Table index direct -> Recherche des NumEnreg : " & Chr(13) & Chr(10) & SQLValue)
    
    '-> ex�cuter la requete
    Set Rec_Index = SetTable(SQLValue)
Else
    '-> Pointer sur l'index sp�cifi�
    IndexParam = IsIname(aFname, IndexParam)
    If IndexParam = "" Then
        '-> Renvoyer un message d'erreur
        Call PrintReponse("B3D_EXPORT~ERROR~90002~" & GetMesseprog("ERRORS", "90002") & "~")
        '-> Quitter la proc�dure
        Exit Sub
    End If
    
    '-> Pr�parer la requete
    SQLValue = "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [INAME] = '" & IndexParam & "'"
    
    '-> Attaquer la table DEALB3D_INDEX pour get des num�ros d'enreg
    Call PrintDebug("Proc�dure B3D_EXPORT : Table � index  -> " & IndexParam & " Analyse de DEALB3D_INDEX : " & Chr(13) & Chr(10) & SQLValue)
    
    '-> Ex�cuter la requete
    Set Rec_Index = SetTable(SQLValue)
    
End If

'-> Ouvrir le fichier ASCII de destination
hdlFile = FreeFile
Open FileExport For Append As #hdlFile



Ligne = "B3D_PROG~REPLICA_" & Uc_ID & "-" & GetDate & "-" & GetTime
If Top_Crypt Then Ligne = Crypt(Ligne)
Print #hdlFile, Ligne


'-> Analyse de la table de replica
Do While Not Rec_Index.EOF
    '-> Rendre la main � la CPU
    DoEvents
    '-> Get du num_enreg
    Num_Enreg = Rec_Index.Fields("NUM_ENREG")
    NbLignes = NbLignes + 1
        
    Ligne = "B3D_REPLICA_CREATE~" & Id_Fname & "�" & Num_Enreg & "~"
    '-> Boucler pour tous les champs en r�plication dans DEALB3D_DATA
    Set Rec_Data = SetTable("SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] =  '" & Id_Fname & "' AND [NUM_ENREG] =  '" & Num_Enreg & "'")
    Do While Not Rec_Data.EOF
        '-> Rendre la main � la CPU
        DoEvents
        '-> Concatainer la ligne de retour
        If Trim(Rec_Data.Fields("EXTENT")) = "" Then
            Ligne = Ligne & Rec_Data.Fields("FIELD") & "�" & DeCrypt(Rec_Data.Fields("ZON_DATA")) & "�"
        Else
            Ligne = Ligne & Rec_Data.Fields("FIELD") & "_" & Trim(Rec_Data.Fields("EXTENT")) & "�" & DeCrypt(Rec_Data.Fields("ZON_DATA")) & "�"
        End If
        '-> Enreg suivant
        Rec_Data.MoveNext
    Loop 'Pour tous les enreg de DEALB3D_DATA
    '-> Fermer le recordset
    Rec_Data.Close
    '-> Envoyer la r�ponse dans le fichier ascii
    Ligne = Mid$(Ligne, 1, Len(Ligne) - 1)
    If Top_Crypt Then Ligne = Crypt(Ligne)
    Print #hdlFile, Ligne
    '-> Enreg suivant
    Rec_Index.MoveNext
Loop

'-> Fermer le recordset
Rec_Index.Close

'-> Fermer le fichier ASCII
Close #hdlFile

'-> Renvoyer une valeur de succ�s
Call PrintReponse("B3D_EXPORT~OK~" & Id_Fname & "�" & FileExport & "�" & NbLignes)

'-> Quitter la proc�dure
Exit Sub

GestError:
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Envoyer unj ordre de debug
    Call PrintDebug("Erreur dans la proc�dure B3D_EXPORT " & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description & Chr(13) & Chr(10) & ErrorCode & " : " & SQLValue)
    
    '-> Fermer la r�ponse
    Call PrintReponse("B3D_EXPORT~ERROR~" & Err.Number & "~" & Err.Description & "~")

End Sub

Private Function IsIname(aFname As Object, strIname As String) As String

'---> Cette fonction v�rifie qu'un index existe et sinon , retourne le premier index de la table

Dim aIname As Object

On Error GoTo GestError

'-> Analyser s�quentiellement les index
For Each aIname In aFname.Inames
    '-> Tester si on trouve l'index
    If aIname.Num_Iname = strIname Then
        '-> Quitter la fonction
        IsIname = strIname
        Exit Function
    End If
Next 'Pour tous les index de la table

'-> On n'a pas trouver l'index, retourner le premier
If aFname.Inames.Count <> 0 Then
    IsIname = aFname.Inames(1).Num_Iname
Else
    IsIname = ""
End If

'-> Quitter la fonction
Exit Function

GestError:
    IsIname = ""

End Function

Private Sub B3D_UNLOCK_SPE(Instruction As String)

'-> Lib�re un num�ro de lock specifique THIERRY


End Sub
Private Function LookUpField(Vecteur As String, FieldSearch As String) As String

'---> analyse dans une chaine pour get de la valeur d'un champ

'[FIELD]�[Value]� ...

Dim i As Integer
Dim Field As String
Dim ValueField As String

On Error GoTo GestError

'-> Tester la liste des champs
If Trim(Vecteur) = "" Then Exit Function

'-> Tester le champ de recherche
If Trim(FieldSearch) = "" Then Exit Function

For i = 1 To NumEntries(Vecteur, "�")
    '-> Get du field
    Field = Entry(1, Entry(i, Vecteur, "�"), "�")
    ValueField = Entry(2, Entry(i, Vecteur, "�"), "�")
    '-> Test
    If UCase$(Trim(Field)) = UCase$(Trim(FieldSearch)) Then
        '-> Renvoyer la valeur
        LookUpField = ValueField
        Exit Function
    End If
Next

'-> Quitter la focntion
GestError:
    LookUpField = ""

End Function


Private Sub B3D_CREATE(Instruction As String, Optional SetEnregSpe As String)

'-> Cette proc�dure cr�er un enreg
' B3D_CREATE~[Ident]�[Fname]~[Field]�[Value]�...

Dim VecteurData As String
Dim NumEnreg As String
Dim aRec As Object
Dim DefData As String
Dim DefField As String
Dim ValueField As String
Dim i As Integer
Dim Id_Fname As String
Dim aFname As Fname
Dim aField As Field
Dim aIname As Iname
Dim aIfield As IFIELD
Dim Champ_Index(1 To 15) As String  'Zones cl�s de INDEX DATA
Dim ToAdd As Boolean
Dim aEmilieObj As EmilieObj
Dim ValExtent As String
Dim Param As String

On Error GoTo GestError

'-> Sauter cette instruction si on n' a pas les privil�ges
If Not UpdateMode Then
    '-> Mode debug
    Call PrintDebug("B3D_CREATE : Mode lecture seule : Agent IDX non affect� -> :" & IdIdx)
    '-> Renvoyer la r�ponse
    Call PrintReponse("B3D_CREATE~ERROR~90001~" & GetMesseprog("ERRORS", "90001") & "~")
    '-> Quitter la proc�dure
    Exit Sub
End If

'-> R�cup des infos
Param = Entry(2, Instruction, "~")
Id_Fname = UCase$(Trim(Entry(1, Param, "�")) & "�" & Trim(Entry(2, Param, "�")))
VecteurData = Entry(3, Instruction, "~")

'-> DEBUGMODE
Call PrintDebug("Proc�dure B3D_CREATE : Acc�s � la table -> " & Id_Fname)

'-> Pointer sur la table
Set aFname = ServeurDOG.fNames(Replace(Id_Fname, "�", "|"))

'-> Si table � acc�s direct
If aFname.IndexDirect <> "" Then
    '-> Acc�s � DEALB3D_DATA avec le champ de r�f�rence pour v�rifier si pas d�ja existant
    ValueField = LookUpField(VecteurData, aFname.IndexDirect)
    
    '-> Pr�parer la requete
    SQLValue = "SELECT top 1 * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [FIELD] = '" & aFname.IndexDirect & "' AND [ZON_DATA] =  '" & ValueField & "'"
    
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_CREATE : Table � index direct -> V�rification DUPLICATE_KEY : " & Chr(13) & Chr(10) & SQLValue)
        
    '-> Acc�der � la base
    Set aRec = SetTable(SQLValue)
    
    '-> Si existe -> Erreur duplicate KEY
    If Not aRec.EOF Then
        '-> Mode debug
        Call PrintDebug("Proc�dure B3D_CREATE -> Duplicate Key")
        '-> Retourner une erreur
        Call PrintReponse("B3D_CREATE~ERROR~90005~" & GetMesseprog("ERRORS", "90005") & "~" & aRec.Fields("NUM_ENREG"))
        '-> Fermer le query
        aRec.Close
        '-> Quitter la proc�dure
        Exit Sub
    End If
    '-> Fermer le query
    aRec.Close
Else 'Table avec index
    For Each aIname In aFname.Inames
        '-> Vider la matrice d'acc�s
        Erase Champ_Index()
        i = 1
        '-> Ne prendre en compte que les index uniques
        If Not aIname.IsUnique Then GoTo NextIname
        '-> Pour tous les champs
        For Each aIfield In aIname.iFields
            Champ_Index(i) = LookUpField(VecteurData, aIfield.Num_Field)
            i = i + 1
        Next 'Pour tous les champs de l'index
    
        '-> Faire un acc�s sur DEALB3D_INDEX pour trouver l'index
        '-> Pr�parer la requete
        SQLValue = "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & _
                               UCase$(Id_Fname) & "' AND [INAME] = '" & aIname.Num_Iname & "' AND " & _
                               "[ZON_INDEX_1] = '" & Champ_Index(1) & "' AND " & _
                               "[ZON_INDEX_2] = '" & Champ_Index(2) & "' AND " & _
                               "[ZON_INDEX_3] = '" & Champ_Index(3) & "' AND " & _
                               "[ZON_INDEX_4] = '" & Champ_Index(4) & "' AND " & _
                               "[ZON_INDEX_5] = '" & Champ_Index(5) & "' AND " & _
                               "[ZON_INDEX_6] = '" & Champ_Index(6) & "' AND " & _
                               "[ZON_INDEX_7] = '" & Champ_Index(7) & "' AND " & _
                               "[ZON_INDEX_8] = '" & Champ_Index(8) & "' AND " & _
                               "[ZON_INDEX_9] = '" & Champ_Index(9) & "' AND " & _
                               "[ZON_INDEX_10] = '" & Champ_Index(10) & "' AND " & _
                               "[ZON_INDEX_11] = '" & Champ_Index(11) & "' AND " & _
                               "[ZON_INDEX_12] = '" & Champ_Index(12) & "' AND " & _
                               "[ZON_INDEX_13] = '" & Champ_Index(13) & "' AND " & _
                               "[ZON_INDEX_14] = '" & Champ_Index(14) & "' AND " & _
                               "[ZON_INDEX_15] = '" & Champ_Index(15) & "'"
        '-> Mode debug
        Call PrintDebug("Table � index : v�rification si index d�ja existant  :" & Chr(13) & Chr(10) & SQLValue)
        
        '-> Ex�cuter la requete
        Set aRec = SetTable(SQLValue)
        
        '-> V�rifier qu'il n'existe pas
        If Not aRec.EOF Then
            '-> Mode debug
            Call PrintDebug("Proc�dure B3D_CREATE -> Duplicate Key : " & aRec.Fields("NUM_ENREG") & " sur l'index : " & aIname.Num_Iname)
            '-> Retourner une erreur
            Call PrintReponse("B3D_CREATE~ERROR~90005~" & GetMesseprog("ERRORS", "90005") & "~" & aIname.Num_Iname & "�" & aRec.Fields("NUM_ENREG"))
            '-> Fermer le query
            aRec.Close
            '-> Se barrer
            Exit Sub
        End If
        '-> Fermer le query
        aRec.Close
NextIname:
    Next 'Pour tous les index
End If 'Selon l'acc�s de la table

'-> R�cup�ration du num�ro d'enregistrement 'ATTENTION SI SETENREGSPE <> "" CREATE � base de r�plication
If SetEnregSpe <> "" Then
    NumEnreg = SetEnregSpe
Else
    NumEnreg = Get_NUM_ENREG(Id_Fname)
End If

'-> Cr�ation du lock

'-> Pr�parer la requete
SQLValue = "INSERT INTO [DEALB3D_LOCK] ( ID_FNAME , NUM_ENREG , FONCTION , DAT_LOCK" & _
                    ", HEU_LOCK , HDL_LOCK , NUM_LOCK , OPE_MAJ , PRG_MAJ , DAT_RELEASE , HEU_RELEASE ) VALUES ( '" & Id_Fname & "' , '" & NumEnreg & "' , '" & _
                    "CREATE" & "' , '" & GetDate & "' , '" & GetTime & "' , '" & Me.hwnd & "' , '" & _
                    NumLockSpe & "' , '" & Operateur & "' , '" & Programme & "' , '' , '' )"
'-> DEBUGMODE
Call PrintDebug("Proc�dure B3D_CREATE Cr�ation du lock � : " & Now & Chr(13) & Chr(10) & SQLValue)

'-> Ex�cuter la requete
CreateEnreg SQLValue

'-> Analyse des champs � cr�er
For i = 1 To NumEntries(VecteurData, "�")

    '-> Def d'un champ
    DefData = Entry(i, VecteurData, "�")
   
    '-> Analyse de tous les champs � mettre � jour
    If InStr(1, Entry(1, DefData, "�"), "_") <> 0 Then
        DefField = Entry(1, Entry(1, DefData, "�"), "_")
        ValExtent = Entry(2, Entry(1, DefData, "�"), "_")
    Else
        DefField = Entry(1, DefData, "�")
        ValExtent = " "
    End If
    ValueField = RTrim(Entry(2, DefData, "�"))
        
    '--> dans le cas ou l'export avait un extent � null au lieu de blanc
    If Trim(ValExtent) = "" Then ValExtent = " "
        
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_CREATE -> Cr�ation du field : " & DefField & " pour la valeur : " & ValueField)
    
    '-> Tester la valeur
    If Trim(ValueField) <> "" Then
        '-> Pointer sur le catalogue
        Set aEmilieObj = ServeurDOG.Catalogue(Entry(1, Id_Fname, "�") & "|" & Trim(UCase$(DefField)))
        '-> Tester le format
        Select Case UCase$(Left$(aEmilieObj.DataType, 3))
            Case "DAT" '-> Formatter les dates
                If InStr(1, Trim(ValueField), "/") <> 0 Then
                    ValueField = Format(Year(ValueField), "0000") & Format(Month(ValueField), "00") & Format(Day(ValueField), "00")
                End If
                
            Case "DEC", "INT"
                If IsNumeric(Trim(Convert(ValueField))) Then
                    If CDbl(Trim(Convert(ValueField))) = 0 Then GoTo NextCreate
                    ValueField = CStr(CDbl(Convert(ValueField)))
                Else
                    '-> On attend un nombre : mauvaise pioche
                    GoTo NextCreate
                End If
        End Select
        If InStr(1, ValueField, "'") <> 0 Then ValueField = Replace(ValueField, "'", " ")
        
        '-> Cr�ation de la valeur dans DEALB3D_DATA Test cryptage
        Set aField = aFname.Fields(DefField)
        If aField.IsCryptage Then ValueField = Crypt(ValueField)
        
        '-> Pr�parer la requete
        SQLValue = "INSERT INTO [DEALB3D_DATA] ( ID_FNAME , NUM_ENREG , FIELD , ZON_DATA" & _
                    ", ZON_LOCK , OPE_MAJ , PRG_MAJ , DAT_HEU_MAJ , EXTENT ) VALUES ( '" & Id_Fname & "' , '" & NumEnreg & "' , '" & _
                    Trim(UCase$(DefField)) & "' , '" & ValueField & "' , '' , '" & Operateur & "' , '" & _
                    Programme & "' , 'IS_CREATE' , '" & ValExtent & "')"
        '-> Mode debug
        Call PrintDebug("Proc�dure B3D_CREATE -> Cr�ation du champ " & Trim(UCase$(DefField)) & " pour la valeur : " & ValueField & " pour l'enreg : " & NumEnreg)
        
        '-> Ex�cuter la requete
        CreateEnreg SQLValue
        
    End If
NextCreate:
Next

'-> Pr�parer la requete
SQLValue = "UPDATE [DEALB3D_LOCK] SET [DAT_RELEASE] =  '" & GetDate & "' , [HEU_RELEASE] =  '" & GetTime & "' , [DAT_HEU_MAJ] = '" & GetTickCount & "' WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG]= '" & NumEnreg & "'"

'-> DEBUGMODE
Call PrintDebug("Proc�dure B3D_CREATE -> Release du lock � : " & Now & Chr(13) & Chr(10) & SQLValue)

'-> Release du lock
CreateEnreg SQLValue

'-> Renvoyer la r�ponse
Call PrintReponse("B3D_CREATE~OK~" & NumEnreg)

'-> Quitter la proc�dure
Exit Sub

'-> Gestion des erreur
GestError:
          
    '-> G�rer les conflits de locks
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure B3D_CREATE :" & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)

    '-> Reset de la r�ponse
    Call ResetReponse
    
    '-> Renvoyer une valeur d'erreur
    Call PrintReponse("B3D_CREATE~ERROR~" & Err.Number & "~" & Err.Description & "~")

End Sub

Private Sub B3D_REINDEX(Instruction As String)

'-> Cette proc�dure recr�er un index sp�cifi�

'B3D_REINDEX~[Ident]�[Fname]�[Index]

Dim Param As String
Dim VecteurIndex As String
Dim VecteurData As String
Dim Id_Fname As String
Dim aRec As Object
Dim aFname As Object
Dim aIname As Object
Dim aIfield As Object
Dim NbIndex As Long

On Error GoTo GestError

'-> V�rifier si on a les droits
If Not UpdateMode Then
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_REINDEX -> Mode lecture seule : Agent IDX : " & IdIdx)
    '-> Retourner une r�ponse
    Call PrintReponse("B3D_REINDEX~ERROR~90001~" & GetMesseprog("ERRORS", "90001") & "~")
    '-> Quitter la proc�dure
    Exit Sub
End If

'-> Get des valeurs
Param = Entry(2, Instruction, "~")
Id_Fname = Trim(Entry(1, Param, "�")) & "�" & Trim(Entry(2, Param, "�"))
VecteurIndex = Entry(3, Param, "�") 'Num�ro de l'index

'-> Pr�parer la requete SQL
SQLValue = "SELECT TOP 1 * FROM [DEALB3D_LOCK] WHERE [ID_FNAME] = '" & Id_Fname & "'"

'-> Mode debug
Call PrintDebug("Proc�dure B3D_REINDEX : V�rification des locks : " & Chr(13) & Chr(10) & SQLValue)

'-> V�rifier si on est pas en train de travailler sur cet index ( qu'il reste des locks sur ce fichier )
Set aRec = SetTable(SQLValue)

'-> Tester qu'il y ait des enreg
If Not aRec.EOF Then
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_REINDEX : Lock in use")
    '-> Envoyer la r�ponse
    Call PrintReponse("B3D_REINDEX~ERROR~90006~" & GetMesseprog("ERRORS", "90006") & "~")
    '-> Fermer le recordset
    aRec.Close
    '-> Quitter la proc�dure
    Exit Sub
End If
'-> Fermer le recordset
aRec.Close

'-> Delete des index

'-> Pr�parer la requete
SQLValue = "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [INAME] = '" & VecteurIndex & "'"

'-> Mode debug
Call PrintDebug("Proc�dure B3D_REINDEX : Delete des index -> " & Chr(13) & Chr(10) & SQLValue)

'-> Ex�cuter la requete
Set aRec = SetTable(SQLValue)

If Not aRec.EOF Then
    Do While Not aRec.EOF
        '-> Delete
        aRec.Delete
        '-> Enreg suivant
        aRec.MoveNext
    Loop
End If
'-> Fermer le query
aRec.Close

'-> Delete des index cumuls

'-> Pr�parer la requete
SQLValue = "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & Id_Fname & "�" & VecteurIndex & "' AND [INAME] = '" & VecteurIndex & "'"

'-> Mode debug
Call PrintDebug("Proc�dure B3D_REINDEX : Delete des index de cumul : " & Chr(13) & Chr(10) & SQLValue)

'-> Ex�cuter la requete
Set aRec = SetTable(SQLValue)

If Not aRec.EOF Then
    Do While Not aRec.EOF
        '-> Delete
        aRec.Delete
        '-> Enreg suivant
        aRec.MoveNext
    Loop
End If
'-> Fermer le query
aRec.Close

'-> Delete des datas de cumuls

'-> Pr�parer la requete
SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname & "�" & VecteurIndex & "'"

'-> Mode de bug
Call PrintDebug("Proc�dure B3D_REINDEX -> Delete des datas de cumul : " & Chr(13) & Chr(10) & SQLValue)

'-> Ex�cuter la requete
Set aRec = SetTable(SQLValue)

If Not aRec.EOF Then
    Do While Not aRec.EOF
        '-> Delete
        aRec.Delete
        '-> Enreg suivant
        aRec.MoveNext
    Loop
End If
'-> Fermer le query
aRec.Close

'-> Recup du premier champ de l'index a retoucher
Set aFname = ServeurDOG.fNames(Entry(1, Id_Fname, "�") & "|" & Entry(2, Id_Fname, "�"))
Set aIname = aFname.Inames(VecteurIndex)

For Each aIfield In aIname.iFields
    If aIfield.Ordre = 1 Then
        VecteurData = aIfield.Num_Field
        Exit For
    End If
Next

'-> Pointer sur DEALB3D_DATA pour recr�er un DEALB3D_LOCK avec Num_Enreg

'-> Pr�parer la requete
SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [FIELD] = '" & VecteurData & "'"

'-> Mode de bug
Call PrintDebug("Proc�dure B3D_REINDEX Pointer sur DEALB3D_DATA pour recr�er un lock avec le numenreg :" & Chr(13) & Chr(10) & SQLValue)

'-> Ex�cuter la requete
Set aRec = SetTable(SQLValue)

NbIndex = 0

If Not aRec.EOF Then
    Do While Not aRec.EOF
        '-> Cr�ation de l'enregistrement
        
        '-> Cr�er la requete
        SQLValue = "INSERT INTO [DEALB3D_LOCK] ( ID_FNAME , NUM_ENREG , FONCTION , DAT_LOCK" & _
                    ", HEU_LOCK ,  HDL_LOCK  , NUM_LOCK , DAT_RELEASE , HEU_RELEASE  , OPE_MAJ , PRG_MAJ ) VALUES ( " & _
                    "'" & Id_Fname & "' , '" & aRec.Fields("NUM_ENREG") & "' , '" & "INDEX" & "' , '" & GetDate & "' , '" & GetTime & "' , '" & _
                    Me.hwnd & "' , '" & NumLockSpe & "' , '" & GetDate & "', '" & GetTime & "','" & Operateur & "' , 'REINDEX_" & VecteurIndex & "' )"
    
        '-> Mode debug
        Call PrintDebug("Proc�dure B3D_REINDEX : Cr�ation du lock : " & Chr(13) & Chr(10) & SQLValue)
        
        '-> Ex�cuter la requete
        CreateEnreg SQLValue
        
        '-> Enreg suivant
        aRec.MoveNext
        NbIndex = NbIndex + 1
    Loop
End If

'-> Fermer le query
aRec.Close

'-> Retourner
Call PrintReponse("B3D_REINDEX~OK~" & NbIndex)

'-> Quitter la fonction
Exit Sub

GestError:

   '-> G�rer les conflits de locks
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure B3D_REINDEX :" & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)

    '-> Reset de la r�ponse
    Call ResetReponse
    
    '-> Renvoyer une valeur d'erreur
    Call PrintReponse("B3D_REINDEX~ERROR~" & Err.Number & "~" & Err.Description & "~")


End Sub

Private Sub B3D_UNLOCK(Instruction As String)

'---> Cette proc�dure supprime un lock
'B3D_UNLOCK~[Ident]�[Fname]�[NumEnreg]

Dim Param As String
Dim Id_Fname As String
Dim NumEnreg As String
Dim aRec As Object, aRec2 As Object
Dim res As Long
Dim SQLValue As String

On Error GoTo GestError

'-> Get du param
Param = Entry(2, Instruction, "~")
Id_Fname = Trim(Entry(1, Param, "�")) & "�" & Trim(Entry(2, Param, "�"))
NumEnreg = Entry(3, Param, "�")

'-> Faire acc�s au DEALB3D_LOCK

'-> Pr�parer la requete
SQLValue = "SELECT * FROM [DEALB3D_LOCK] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & NumEnreg & "'"

'-> Mode debug
Call PrintDebug("B3D_UNLOCK : Acc�s aux Lock : " & Chr(13) & Chr(10) & SQLValue)

'-> Ex�cuter la requete
Set aRec = SetTable(SQLValue)

'-> Tester si le lock existe
If aRec.EOF Then
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_UNLOCK : Lock Innexistant")
    '-> Retouner une r�ponse d'erreur
    Call PrintReponse("B3D_UNLOCK~ERROR~90007~" & GetMesseprog("ERRORS", "90007") & "~")
    '-> Fermer le query
    aRec.Close
    '-> Quitter la proc�dure
    Exit Sub
End If

'-> Tester si le lock n 'est pas en cours de traitement
If aRec.Fields("DAT_RELEASE") <> "" Then
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_UNLOCK Lock en cours de traitement par l'IDX BUILD")
    '-> Le lock est d�ja en cours de traitement par un IDX_BUILD
    Call PrintReponse("B3D_UNLOCK~ERROR~90008~" & GetMesseprog("ERRORS", "90008") & "~" & NumEnreg & "�" & aRec.Fields("OPE_MAJ") & "�" & aRec.Fields("DAT_HEU_MAJ") & "�" & aRec.Fields("PRG_MAJ"))
    '-> Fermer le query
    aRec.Close
    '-> Quitter la proc�dure
    Exit Sub
End If

'-> Tester si on est propri�taire du lock
If CLng(Trim(aRec.Fields("HDL_LOCK"))) <> Me.hwnd Then
    '-> On n'est pas proprio du lock
    res = IsWindow(CLng(aRec.Fields("HDL_LOCK")))
    If res = 1 Then
        '-> Mode debug
        Call PrintDebug("Proc�dure B3D_UNLOCK On n'est pas propri�taire du lock et le programme est toujours actif  : Lock en cours de traitement par l'IDX BUILD")
        '-> Le programme est toujours actif
        Call PrintReponse("B3D_UNLOCK~ERROR~90008~" & GetMesseprog("ERRORS", "90008") & "~" & NumEnreg & "�" & aRec.Fields("OPE_MAJ") & "�" & aRec.Fields("DAT_HEU_MAJ") & "�" & aRec.Fields("PRG_MAJ"))
        aRec.Close
        '-> Aller � l'instruction suivante
        Exit Sub
    End If

    '-> Remettre � jour les champs ZON_DATA avec la valeur de ZON_LOCK pour tous _
    les DEALB3D_DATA avec NumEnregistrement
    
    '-> Pr�parer la requete
    SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & NumEnreg & "'"
    
    '-> Mode debug
    Call PrintDebug("B3D_UNLOCK : Mise � jour de DEALB3D_DATA de ZON_DATA avec ZON_LOCK : " & Chr(13) & Chr(10) & SQLValue)
    
    '-> Ex�cuter la requete
    Set aRec2 = SetTable(SQLValue)
    
    If Not aRec2.EOF Then
        '-> Mise � jour des DEALB3D_DATA
        Do While Not aRec2.EOF
            If Trim(UCase$(aRec.Fields("FONCTION"))) = "CREATE" Then
                '-> Mode debug
                Call PrintDebug("B3D_UNLOCK : FONCTION = Create : Delete")
                aRec2.Delete
            Else
                If aRec2.Fields("DAT_HEU_MAJ") = "IS_MODIFY" Then
                    '-> Setting de la requete
                    SQLValue = "UPDATE [DEALB3D_DATA] SET [ZON_DATA] =  '" & aRec2.Fields("ZON_LOCK") & "' , [ZON_LOCK] = '' , [DAT_HEU_MAJ] = '" & Get_Total_Time & "' " & _
                            "WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & NumEnreg & "'"
                    '-> Mode debug
                    Call PrintDebug("B3D_UNLOCK : Maj de Zon_Data par Zon_Lock :" & Chr(13) & Chr(10) & SQLValue)
                    '-> Envoie
                    CreateEnreg SQLValue
                End If
            End If
            '-> Lire l'enreg suivant
            aRec2.MoveNext
        Loop
    End If
    
    '-> Fermer le DEALB3D_DATA
    aRec2.Close
    
End If 'Si hdl Lock <> process en cours

Call PrintDebug("B3D_UNLOCK : Delete du lock")
'-> Supprimer le lock
aRec.Delete

'-> Fermer le canal query sur DEALB3D_UNLOCK
aRec.Close

'-> Renvoyer OK
Call PrintReponse("B3D_UNLOCK~OK~" & NumEnreg)

'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> G�rer les conflits de locks
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure B3D_UNLOCK :" & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)

    '-> Reset de la r�ponse
    Call ResetReponse
    
    '-> Renvoyer une valeur d'erreur
    Call PrintReponse("B3D_UNLOCK~ERROR~" & Err.Number & "~" & Err.Description & "~")


End Sub


Private Sub B3D_UPDATE_SPE(Instruction As String)

'THIERRY A faire pour MAJ sur LOCK sp�cifiqque

End Sub

Private Sub B3D_UPDATE(Instruction As String, Optional IsReplica As Boolean)

'-> Exemple : B3D_UPDATE~[Ident]�[Fname]�[NumEnreg]~[Field]�[ValueField]�...

Dim Top_Update As Integer
Dim Param As String
Dim NumEnreg As String
Dim VecteurData As String
Dim aRec As Object
Dim aRec2 As Object
Dim strRetour As String
Dim Id_Fname As String
Dim DefData As String
Dim DefField As String
Dim ValueField As String
Dim i As Integer
Dim aFname As Fname
Dim aEmilieObj As EmilieObj
Dim aField As Field
Dim ValExtent As String

On Error GoTo GestError

'-> V�rifier si on a les droits
If Not UpdateMode Then
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_UPDATE -> Mode lecture seule : Agent IDX : " & IdIdx)
    '-> Retourner une r�ponse
    Call PrintReponse("B3D_UNLOCK~ERROR~90001~" & GetMesseprog("ERRORS", "90001") & "~" & NumEnreg)
    '-> Quitter la proc�dure
    Exit Sub
End If

'-> V�rification pour delete d'un enreg issu d'une op�ration de r�plication
If Trim(UCase$(Mid$(NumEnreg, 1, Len(Trim(Uc_ID))))) <> Trim(UCase$(Uc_ID)) And IsReplica = False Then
    '-> On n'est pas proprio du numenreg : ne pouvoir le delete que si ReplicaUpdate est true
    If Not ReplicaUpdate Then
        '-> Mode debug
        Call PrintDebug("Proc�dure B3D_UPDATE : Modification de r�plication non autoris�e")
        '-> Envoyer la r�ponse
        Call PrintReponse("B3D_UPDATE~ERROR~90009~" & GetMesseprog("ERRORS", "90009") & "~" & NumEnreg)
        '-> Quitter la proc�dure
        Exit Sub
    End If
End If

'-> Setting du top
If UCase$(Entry(1, Instruction, "~")) = "B3D_UPDATE" Then
    Top_Update = 1
Else
    Top_Update = 2
End If

'-> R�cup�ration des datas
Param = Entry(2, Instruction, "~")
Id_Fname = UCase$(Trim(Entry(1, Param, "�")) & "�" & Trim(Entry(2, Param, "�")))
NumEnreg = Entry(3, Param, "�")
VecteurData = Entry(3, Instruction, "~")

'-> V�rifier si on est le propri�taire du lock
SQLValue = "SELECT TOP 1 * FROM [DEALB3D_LOCK] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & NumEnreg & "'"

'-> Mode debug
Call PrintDebug("Proc�dure B3D_UPDATE : V�rifier si on est propri�taire du lock :" & Chr(13) & Chr(10) & SQLValue)

'-> Ex�cuter la requete
Set aRec = SetTable(SQLValue)

'-> Refuser si l'enregistrement n'est pas lock�
If Not aRec.EOF Then 'CAS ou le LOCK existe
    '-> Tester si on est le proprio du lock
    If CLng(aRec.Fields("HDL_LOCK")) <> Me.hwnd Then
        '-> Le lock existe d�ja
        Call PrintDebug("Proc�dure B3D_UPDATE : Enregistrement lock� est on n'est pas propri�taire du lock : " & NumEnreg)
        '-> Envoyer la r�ponse
        Call PrintReponse("B3D_UPDATE~ERROR~90006~" & GetMesseprog("ERRORS", "90006") & "~" & NumEnreg & "�" & aRec.Fields("OPE_MAJ") & "�" & aRec.Fields("DAT_HEU_MAJ") & "�" & aRec.Fields("PRG_MAJ"))
        '-> Fermer le query
        aRec.Close
        '-> Quitter la proc�dure
        Exit Sub
    End If
Else
    If Top_Update = 1 Then '-> Retour Erreur Le lock n'existe pas
        '-> Mode debug
        Call PrintDebug("Proc�dure B3D_UPDATE : Lock introuvable ")
        '-> Ecrire le retour
        Call PrintReponse("B3D_UPDATE~ERROR~90007~" & GetMesseprog("ERRORS", "90007") & "~" & NumEnreg)
        '-> Fermer le query
        aRec.Close
        '-> Quitter la proc�dure
        Exit Sub
    Else
        '-> On est en mode Update_LOCK : il faut cr�er le lock
                
        '-> Pr�parer la requete
        SQLValue = "INSERT INTO [DEALB3D_LOCK] ( ID_FNAME , NUM_ENREG , FONCTION , DAT_LOCK" & _
                    ", HEU_LOCK , HDL_LOCK , NUM_LOCK , OPE_MAJ , PRG_MAJ , DAT_RELEASE , HEU_RELEASE ) VALUES ( '" & Id_Fname & "' , '" & NumEnreg & "' , '" & _
                    "GET" & "' , '" & GetDate & "' , '" & GetTime & "' , '" & frmAgentB3D.hwnd & "' , '" & _
                    NumLockSpe & "' , '" & Operateur & "' , '" & Programme & "' , '' , '' )"
        
        '-> Mode debug
        Call PrintDebug("Proc�dure B3D_UPDATE -> Mode update lock : Cr�ation du lock : " & Chr(13) & Chr(10) & SQLValue)
        
        '-> Ex�cuter la requete
        CreateEnreg SQLValue

    End If 'Selon le mode Update
End If 'Si le lock existe

'-> On est propri�taire du lock : Mettre � jour les datas

'-> Pointer sur la table
Set aFname = ServeurDOG.fNames(Replace(Id_Fname, "�", "|"))

'-> Analyse des champs � cr�er
For i = 1 To NumEntries(VecteurData, "�")
    '-> Def d'un champ
    DefData = Entry(i, VecteurData, "�")
    
    '-> Annalyse de tous les champs � mettre � jour
    If InStr(1, Entry(1, DefData, "�"), "_") <> 0 Then
        '-> Ici envoyer la procedure qui controle que deffield n'est pas dans un index
        DefField = Entry(1, Entry(1, DefData, "�"), "_")
        ValExtent = Entry(2, Entry(1, DefData, "�"), "_")
    Else
        DefField = Entry(1, DefData, "�")
        ValExtent = " "
    End If
    '    DefField = Entry(1, DefData, "�")
    ValueField = RTrim(Entry(2, DefData, "�"))
            
    
    '-> Pointer sur le catalogue
    Set aEmilieObj = ServeurDOG.Catalogue(Entry(1, Id_Fname, "�") & "|" & Trim(UCase$(DefField)))
    
    '-> Tester le format
    Select Case UCase$(Left$(aEmilieObj.DataType, 3))
        Case "DAT" '-> Formatter les dates
            If InStr(1, Trim(ValueField), "/") <> 0 Then
                ValueField = Format(Year(ValueField), "0000") & Format(Month(ValueField), "00") & Format(Day(ValueField), "00")
            End If
        Case "DEC", "INT"
            If Not IsNumeric(Trim(Convert(ValueField))) Then GoTo NextField
    End Select

    '-> V�rifier que le champ sp�cifi� n'existe pas d�ja
    
    '-> Pr�parer la requete
    SQLValue = "SELECT TOP 1 * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname _
                                  & "' AND [NUM_ENREG] = '" & NumEnreg _
                                  & "' AND [FIELD] = '" & DefField & "'" _
                                  & "  AND [EXTENT] = '" & ValExtent & "'"
                                  
                                  
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_UPDATE : V�rifier que le champ n'existe pas d�ja : " & Chr(13) & Chr(10) & SQLValue)
    
    '-> Ex�cuter la requete
    Set aRec2 = SetTable(SQLValue)
    
    '-> Le champ n'existe pas : le cr�er
    If aRec2.EOF Then
        
        '-> Mode debug
        Call PrintDebug("Proc�dure B3D_UPDATE : Le champ n'existe pas")
        
        If Trim(ValueField) <> "" Then
        
            '-> Mode debug
            Call PrintDebug("Proc�dure B3D_UPDATE : Test Cryptage")
        
            '-> Cr�ation de la valeur dans DEALB3D_DATA Test cryptage
            Set aField = aFname.Fields(DefField)
            If aField.IsCryptage Then ValueField = Crypt(ValueField)
        
            '-> Pr�parer la requete
            SQLValue = "INSERT INTO [DEALB3D_DATA] ( ID_FNAME , NUM_ENREG , FIELD , ZON_DATA" & _
                        ", ZON_LOCK , OPE_MAJ , PRG_MAJ , DAT_HEU_MAJ , EXTENT ) VALUES ( '" & Id_Fname & "' , '" & NumEnreg & "' , '" & _
                        Trim(UCase$(DefField)) & "' , '" & ValueField & "' , '' , '" & Operateur & "' , '" & _
                        Programme & "' , 'IS_MODIFY' , '" & ValExtent & "')"
            
            '-> Mode debug
            Call PrintDebug("Proc�dure B3D_UPDATE : Le champ n'existe pas d�ja : le cr�er : " & Chr(13) & Chr(10) & SQLValue)
        
            '-> Ex�cuter la requete
            CreateEnreg SQLValue
        End If 'Ne pas cr�er les enreg pour les champs � blanc
    Else
                
        '-> Ne pas traiter les champs non modifiables
        If aFname.Fields(DefField).NonModifiable Then GoTo NextField
        
        '-> V�rifier que la valeur re�ue soit diff�rente de la valeur existante
        If Trim(UCase$(ValueField)) = Trim(UCase$(DeCrypt(aRec2.Fields("ZON_DATA")))) Then GoTo NextField
        
        '-> Cr�ation de la valeur dans DEALB3D_DATA Test cryptage
        Set aField = aFname.Fields(DefField)
        If aField.IsCryptage Then ValueField = Crypt(ValueField)
        
        '-> l'enregistrement existe : le mettre � jour
        SQLValue = "UPDATE [DEALB3D_DATA] SET [ZON_DATA] = '" & ValueField & "' , [ZON_LOCK] ='" & aRec2.Fields("ZON_DATA") & "' , [DAT_HEU_MAJ] = 'IS_MODIFY'  WHERE [ID_FNAME] = '" & Id_Fname & _
                        "' AND [NUM_ENREG] = '" & NumEnreg & "' AND [FIELD] = '" & Trim(UCase$(DefField)) & "' AND [EXTENT] = '" & ValExtent & "'"
                        
        '-> Mode debug
        Call PrintDebug("Proc�dure B3D_UPDATE : Le champ existe d�ja : le jour : " & Chr(13) & Chr(10) & SQLValue)
        
        '-> Ex�cuter
        CreateEnreg SQLValue
    End If

    '-> Fermer le recordset
    aRec2.Close
NextField:
Next

'-> Mise � jour du lock

'-> Pr�parer la requete
SQLValue = "UPDATE [DEALB3D_LOCK] SET [DAT_RELEASE] =  '" & GetDate & "' , [HEU_RELEASE] =  '" & GetTime & "' , [FONCTION] =  'UPDATE' WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG]= '" & NumEnreg & "'"

'-> Mode debug
Call PrintDebug("Proc�dure B3D_UPDATE -> Mise � jour du lock : " & Chr(13) & Chr(10) & SQLValue)

'-> Ex�cuter la requete
CreateEnreg SQLValue

If Top_Update = 1 Then
    strRetour = "B3D_UPDATE~OK~" & NumEnreg
Else
    strRetour = "B3D_UPDATE_LOCK~OK~" & NumEnreg
End If

'-> Ecrire le retour
Call PrintReponse(strRetour)

'-> Quitter la proc�dure
Exit Sub

'-> Gestion des Erreurs
GestError:

    '-> G�rer les conflits de locks
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure B3D_UPDATE :" & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)

    '-> Reset de la r�ponse
    Call ResetReponse
    
    '-> Renvoyer une valeur d'erreur
    Call PrintReponse("B3D_UPDATE~ERROR~" & Err.Number & "~" & Err.Description & "~" & NumEnreg)

End Sub

Private Sub B3D_SEEK_JOIN(Instruction As String)

'---> Cette proc�dure v�rifie si un enreg sp�cifi� est joint
'-> B3D_SEEK_JOIN~[Ident]�[Fname]�[Num_Enreg]

Dim Param As String
Dim Id_Fname As String
Dim NumEnreg As String
Dim aRec As Object

On Error GoTo GestError

'-> R�cup�ration des datas
Param = Entry(2, Instruction, "~")
Id_Fname = UCase$(Trim(Entry(1, Param, "�")) & "�" & Trim(Entry(2, Param, "�")))
NumEnreg = Entry(3, Param, "�")

'-> Pr�parer la requete
SQLValue = "SELECT TOP  1 * FROM [DEALB3D_DATA] WHERE [ENREG_JOIN] = '" & Id_Fname & "�" & NumEnreg & "'"

'-> Mode debug
Call PrintDebug("B3D_SEEK_JOIN : V�rificartion de l'int�grit� : " & Chr(13) & Chr(10) & SQLValue)

'-> Ex�cuter la requete
Set aRec = SetTable(SQLValue)
If aRec.EOF Then
    Call PrintReponse("B3D_SEEK_JOIN~ERROR~90010~" & GetMesseprog("ERRORS", "90010") & "~")
Else
    Call PrintReponse("B3D_SEEK_JOIN~OK~")
End If

'-> Fermer le recordset
aRec.Close

'-> Quitter la fonction
Exit Sub

'-> Gestion des erreur
GestError:

    '-> G�rer les conflits de locks
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If

    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure B3D_SEEK_JOIN : " & Err.Number & " - " & Err.Description)
    
    '-> Faire un reset de la r�ponse
    Call ResetReponse

    '-> Retourner une r�ponse
    Call PrintReponse("B3D_SEEK_JOIN~ERROR~" & Err.Number & " - " & Err.Description & "~")

End Sub

Private Sub B3D_SEARCH_JOIN(Instruction As String)

'---> Cette proc�dure retourne tous les enregistrements de jointure
'-> B3D_SEARCH_JOIN~[Ident]�[Fname]�[Num_Enreg]

Dim Param As String
Dim Id_Fname As String
Dim NumEnreg As String
Dim aRec As Object
Dim strRetour As String

On Error GoTo GestError

'-> R�cup�ration des datas
Param = Entry(2, Instruction, "~")
Id_Fname = UCase$(Trim(Entry(1, Param, "�")) & "�" & Trim(Entry(2, Param, "�")))
NumEnreg = Entry(3, Param, "�")

'-> Controle de l'int�grit�

'-> Pr�parer la requete
SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ENREG_JOIN] = '" & Id_Fname & "�" & NumEnreg & "'"

'-> Mode debug
Call PrintDebug("Proc�dure B3D_SEARCH_JOIN -> V�rification de l'nt�grit� : " & Chr(13) & Chr(10) & SQLValue)

'-> Ex�cuter la requete
Set aRec = SetTable(SQLValue)

If aRec.EOF Then
    Call PrintReponse("B3D_SEARCH_JOIN~ERROR~90010~" & GetMesseprog("ERRORS", "90010") & "~")
Else
    '-> Indiquer que l'on a trouv� la jointure
    Call PrintReponse("B3D_SEARCH_JOIN~OK~FIND_JOIN")
    '-> Renvoyer les descriptifs
    Do While Not aRec.EOF
        '-> Rendre la main � la CPU
        DoEvents
        '-> Identifier la jointure
        strRetour = aRec.Fields("ID_FNAME") & "�" & aRec.Fields("NUM_ENREG") & "�" & aRec.Fields("FIELD")
        '-> Renvoyer la r�ponse
        Call PrintReponse(strRetour)
        '-> Enregistrement suivant
        aRec.MoveNext
    Loop 'Pour tous les enregistrements de jointure
End If

'-> Fermer le query

'-> Quitter la fonction
Exit Sub

GestError:

    '-> G�rer les conflits de locks
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If

    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure B3D_SEARCH_JOIN : " & Err.Number & " - " & Err.Description)
    
    '-> Faire un reset de la r�ponse
    Call ResetReponse

    '-> Retourner une r�ponse
    Call PrintReponse("B3D_SEARCH_JOIN~ERROR~" & Err.Number & " - " & Err.Description & "~")

End Sub


Private Sub B3D_DELETE(Instruction As String, Optional IsReplica As Boolean)

'---> Cette proc�dure supprime un enreg dans la base
'->B3D_DELETE~[Ident]�[Fname]�[Num_Enreg]

Dim Param As String
Dim Id_Fname As String
Dim NumEnreg As String
Dim aRec As Object
Dim strRetour As String
Dim OkToDel As Boolean

On Error GoTo GestError

'-> V�rifier si on a les droits
If Not UpdateMode Then
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_DELETE -> Mode lecture seule : Agent IDX : " & IdIdx)
    '-> Retourner une r�ponse
    Call PrintReponse("B3D_DELETE~ERROR~90001~" & GetMesseprog("ERRORS", "90001") & "~")
    '-> Quitter la proc�dure
    Exit Sub
End If

'-> R�cup�ration des datas
Param = Entry(2, Instruction, "~")
Id_Fname = UCase$(Trim(Entry(1, Param, "�")) & "�" & Trim(Entry(2, Param, "�")))
NumEnreg = Entry(3, Param, "�")

'-> V�rification pour delete d'un enreg issu d'une op�ration de r�plication
If Trim(UCase$(Mid$(NumEnreg, 1, Len(Trim(Uc_ID))))) <> Trim(UCase$(Uc_ID)) And IsReplica = False Then
    '-> On n'est pas proprio du numenreg : ne pouvoir le delete que si ReplicaUpdate est true
    If ReplicaUpdate Then
        OkToDel = True
    Else
        Call PrintReponse("B3D_DELETE~ERROR~90009~" & GetMesseprog("ERRORS", "90009") & "~" & NumEnreg)
        Exit Sub
    End If
End If

'-> Control de l'int�grit� des donn�es � supprimer : on v�rifie qu'il n 'existe pas une jointure _
active pour cet enreg

'-> Pr�parer la requete
SQLValue = "SELECT TOP  1 * FROM [DEALB3D_DATA] WHERE [ENREG_JOIN] = '" & Id_Fname & "�" & NumEnreg & "'"

'-> Mode debug
Call PrintDebug("B3D_DELETE : Controle de l'int�grit� de la donn�e : " & Chr(13) & Chr(10) & SQLValue)

'-> Ex�cuter la requete
Set aRec = SetTable(SQLValue)

If aRec.EOF Then OkToDel = True

'-> Pr�parer la requete
SQLValue = "SELECT TOP 1 * FROM [DEALB3D_LOCK] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & NumEnreg & "'"

'-> Mode debug
Call PrintDebug("B3D_DELETE : V�rification de l'appartenance du Lock" & Chr(13) & Chr(10) & SQLValue)

'-> V�rifier si on est le propri�taire du lock ou le cr�er
Set aRec = SetTable(SQLValue)

'-> Cr�er le lock s'il n'existe pas
If aRec.EOF Then
    '-> Test int�grit�
    If Not OkToDel Then
        '-> Retourner la r�ponse
        Call PrintReponse("B3D_DELETE~ERROR~90009~" & GetMesseprog("ERRORS", "90009") & "~")
        '-> Lib�rer le pointeur vers la base
        aRec.Close
        '-> Quitter la proc�dure
        Exit Sub
    End If
    
    '-> Pr�parer la requete
    SQLValue = "INSERT INTO [DEALB3D_LOCK] ( ID_FNAME , NUM_ENREG , FONCTION , DAT_LOCK" & _
                    ", HEU_LOCK , HDL_LOCK , NUM_LOCK , OPE_MAJ , PRG_MAJ , DAT_RELEASE , HEU_RELEASE ) VALUES ( '" & Id_Fname & "' , '" & NumEnreg & "' , '" & _
                    "DELETE" & "' , '" & GetDate & "' , '" & GetTime & "' , '" & Me.hwnd & "' , '" & _
                    NumLockSpe & "' , '" & Operateur & "' , '" & Programme & "' , '" & GetDate & "' , '" & GetTime & "' )"
    
    '-> Mode debug
    Call PrintDebug("B3D_DELETE : Cr�ation du lock : " & Chr(13) & Chr(10) & SQLValue)
    
    '-> Cr�ation du lock
    CreateEnreg SQLValue
Else
    If CLng(aRec.Fields("HDL_LOCK")) <> Me.hwnd Then
        '-> Il faut v�rifier si le lock est toujours actif
        If IsWindow(CLng(aRec.Fields("HDL_LOCK"))) = 1 Then
            '-> Mode debug
            Call PrintDebug("Proc�dure B3D_DELETE Lock en cours toujours actif ")
            '-> Le lock existe d�ja : valeur de retour d'erreur
            Call PrintReponse("B3D_DELETE~ERROR~90006~" & GetMesseprog("ERRORS", "90006") & "~" & aRec.Fields("OPE_MAJ") & "�" & aRec.Fields("DAT_HEU_MAJ") & "�" & aRec.Fields("PRG_MAJ") & "�1")
        Else
            '-> Le lock n'est pluas actif
            Call PrintDebug("Proc�dure B3D_DELETE Lock en cours process inactif ")
            '-> Le lock existe d�ja : valeur de retour d'erreur
            Call PrintReponse("B3D_DELETE~ERROR~90006~" & GetMesseprog("ERRORS", "90006") & "~" & aRec.Fields("OPE_MAJ") & "�" & aRec.Fields("DAT_HEU_MAJ") & "�" & aRec.Fields("PRG_MAJ") & "�0")
        End If
        '-> Fermer le query
        aRec.Close
        '-> Quitter la proc�dure
        Exit Sub
    Else
         '-> Test int�grit�
        If Not OkToDel Then
            '-> Envoyer une r�ponse d'erreur
            Call PrintReponse("B3D_DELETE~ERROR~90009~" & GetMesseprog("ERRORS", "90009") & "~")
            '-> Supprimer le lock
            aRec.Delete
            '-> Fermer le recordset
            aRec.Close
            '-> Quitter la fonction
            Exit Sub
        End If
        '-> Mettre � jour le Lock
        
        '-> Pr�parer la requete
        SQLValue = "UPDATE [DEALB3D_LOCK] SET [DAT_RELEASE] =  '" & GetDate & "' , [HEU_RELEASE] =  '" & GetTime & "' , [FONCTION] = 'DELETE' WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG]= '" & NumEnreg & "'"
        
        '-> Mode debug
        Call PrintDebug("Proc�dure B3D_DELETE : Mise � jour du lock " & Chr(13) & Chr(10) & SQLValue)
        
        '-> Ex�cuter la requete
        CreateEnreg SQLValue
        
        '-> Fermer le query
        aRec.Close
    End If
End If

'-> Renvoyer valeur de succ�s THIERRY
strRetour = "B3D_DELETE~OK~" & NumEnreg
Call PrintReponse(strRetour)

'-> Quitter la proc�dure
Exit Sub

GestError:

    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure B3D_DELETE " & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)
    
    '-> Fermer le fichier r�ponse
    Call ResetReponse
    
    '-> Renvoyer une r�ponse d'erreur
    Call PrintReponse("B3D_DELETE~ERROR~" & Err.Number & "~" & Err.Description & "~")

End Sub

Private Sub B3D_GET_LOCK()

'-> Cette proc�dure retourne l'etat de la table des Locks

Dim aRec As Object
Dim strRetour As String
Dim res As Long

On Error GoTo GestError

'-> R�cup�rer l'ensemble de la table des locks

'-> Pr�parer la requete
SQLValue = "SELECT * FROM [DEALB3D_LOCK]"

'-> Mode debug
Call PrintDebug("Proc�dure B3D_GET_LOCK -> R�cup�ration de la table des locks")

'-> Ex�cuter la requete
Set aRec = SetTable(SQLValue)

'-> Analyse du recordset
If Not aRec.EOF Then
    '-> Indiquer qu'il y a des locks
    Call PrintReponse("B3D_GET_LOCK~OK~")
    '-> Analyse en boucle
    Do While Not aRec.EOF
        strRetour = RTrim(aRec.Fields("ID_FNAME"))
        strRetour = strRetour & "�" & RTrim(aRec.Fields("NUM_ENREG"))
        res = IsWindow(aRec.Fields("HDL_LOCK"))
        If res = 1 Then
            strRetour = strRetour & "�0"
        Else
            strRetour = strRetour & "�1"
        End If
        strRetour = strRetour & "~" & RTrim(aRec.Fields("FONCTION"))
        strRetour = strRetour & "�" & RTrim(aRec.Fields("NUM_LOCK"))
        strRetour = strRetour & "�" & RTrim(aRec.Fields("DAT_LOCK"))
        strRetour = strRetour & "�" & RTrim(aRec.Fields("HEU_LOCK"))
        strRetour = strRetour & "�" & RTrim(aRec.Fields("HDL_LOCK"))
        strRetour = strRetour & "�" & RTrim(aRec.Fields("DAT_RELEASE"))
        strRetour = strRetour & "�" & RTrim(aRec.Fields("HEU_RELEASE"))
        strRetour = strRetour & "�" & RTrim(aRec.Fields("OPE_MAJ"))
        strRetour = strRetour & "�" & RTrim(aRec.Fields("PRG_MAJ"))
        strRetour = strRetour & "�" & RTrim(aRec.Fields("DAT_HEU_MAJ"))
        '-> Impression dans le fichier RSP
        Call PrintReponse(strRetour)
        '-> Lock suivant
        aRec.MoveNext
    Loop 'Lecture de la table des locks
Else '-> Si pas de lock
    Call PrintReponse("B3D_GET_LOCK~ERROR~900012~" & GetMesseprog("ERRORS", "90012") & "~")
End If 'Si lock

'-> Quitter la proc�dure
Exit Sub

GestError:

    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure B3D_GET_LOCK " & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)
    
    '-> Fermer le fichier r�ponse
    Call ResetReponse
    
    '-> Renvoyer une r�ponse d'erreur
    Call PrintReponse("B3D_GET_LOCK~ERROR~" & Err.Number & "~" & Err.Description & "~")


End Sub

Private Sub B3D_GET_ENREG(Instruction As String)

Dim Param As String
Dim DateMini As String
Dim DateMaxi As String
Dim VecteurData As String
Dim aRec As Object
Dim strRetour As String

'-> Retourne les stats par fichier et par date
' B3D_GET_ENREG~[Ident]�[DateMini]�[DateMaxi]

On Error GoTo GestError

'-> Get des param�tres
Param = Entry(2, Instruction, "~")
VecteurData = Entry(1, Param, "�")
DateMini = Entry(2, Param, "�")
DateMaxi = Entry(3, Param, "�")

'-> Pointer sur la date sp�cifi�e

'-> Pr�parer la requete
SQLValue = "SELECT * FROM [DEALB3D_ENREG] WHERE [ID_FNAME] LIKE '" & VecteurData & "*' AND [DAT_HISTO] <> '99999999'"

'-> Mode debug
Call PrintDebug("Proc�dure B3D_GET_ENREG -> r�cup�ration des stats : " & Chr(13) & Chr(10) & SQLValue)

'-> Ex�cuter la requete
Set aRec = SetTable(SQLValue)

'-> S'il ya des enregistrements
If Not aRec.EOF Then
    '-> Envoyer une r�ponse
    Call PrintReponse("B3D_GET_ENREG~OK~")
    '-> Envoyer toutes les zones
    Do While Not aRec.EOF
        If Trim(aRec.Fields("DAT_HISTO")) >= DateMini And Trim(aRec.Fields("DAT_HISTO")) <= DateMaxi Then
            '-> Concatainer la chaine de retour
            strRetour = RTrim(aRec.Fields("ID_FNAME"))
            strRetour = strRetour & "�" & FormatDate(RTrim(aRec.Fields("DAT_HISTO")))
            strRetour = strRetour & "�" & RTrim(aRec.Fields("NB_CREATE"))
            strRetour = strRetour & "�" & RTrim(aRec.Fields("NB_UPDATE"))
            strRetour = strRetour & "�" & RTrim(aRec.Fields("NB_DELETE"))
            '-> Impression dans le fichier RSP
            Call PrintReponse(strRetour)
        End If
        '-> Enreg suivant
        aRec.MoveNext
    Loop 'Pour tous les enregs
Else
    Call PrintReponse("B3D_GET_ENREG~ERROR~90012~" & GetMesseprog("ERRORS", "90012") & "~")
End If

'-> Quitter la proc�dure
Exit Sub

GestError:

    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure B3D_GET_ENREG " & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)
    
    '-> Fermer le fichier r�ponse
    Call ResetReponse
    
    '-> Renvoyer une r�ponse d'erreur
    Call PrintReponse("B3D_GET_ENREG~ERROR~" & Err.Number & "~" & Err.Description & "~")



End Sub

Private Sub B3D_FIND_HISTO(Instruction As String)

'-> Get des historiques � partir d'un numero d'enreg
' B3D_FIND_HSTO~[IDENT]�[FNAME]�[NUM_ENREG]�GetHisto

Dim Param As String
Dim Id_Fname As String
Dim NumEnreg As String
Dim SQLValue As String
Dim aRec As Object
Dim TopJoin As Integer
Dim ValueJoin As String
Dim FnameJoin As Fname
Dim aEmilieObj As EmilieObj
Dim RecJoin As Object
Dim File_Jointure As String
Dim Num_Enreg_join As String
Dim aFieldJoin As Field
Dim ValExtent As String

On Error GoTo GestError

'-> Analyse du param�trage
Param = Entry(2, Instruction, "~")
Id_Fname = UCase$(Trim(Entry(1, Param, "�")) & "�" & Trim(Entry(2, Param, "�")))
NumEnreg = Entry(3, Param, "�")
TopJoin = CInt(Entry(4, Param, "�"))

'-> Analyse de la table des histos
SQLValue = "SELECT * FROM [DEALB3D_HISTO] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & NumEnreg & "' ORDER BY [FIELD] , [DAT_HISTO] DESC , [HEU_HISTO] DESC"

'-> Mode debug
Call PrintDebug("Proc�dure B3D_FIND_HISTO -> Get des historiques " & Chr(13) & Chr(10) & SQLValue)

'-> Envoyer la requete
Set aRec = SetTable(SQLValue)

'-> Analyse de la requete
If aRec.EOF Then
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_FIND_HISTO -> Pas d'historiques")
    '-> Envoyer la reponse
    Call PrintReponse("B3D_FIND_HISTO~ERROR~90013~" & GetMesseprog("ERRORS", "90013") & "~")
    '-> Fermer le query
    aRec.Close
    '-> Quitter la proc�dure
    Exit Sub
End If

'-> Renvoyer une valeur de succ�s
Call PrintReponse("B3D_FIND_HISTO~OK~")

'-> Analyser le recordset
Do While Not aRec.EOF
    '-> De base on renvoie la valeur sans la jointure
    ValueJoin = FormatZone(RTrim(DeCrypt(aRec.Fields("ZON_DATA"))), aRec.Fields("FIELD"))
    '-> Tester si on doit r�cup�rer les jointures
    If TopJoin = 1 Then
        '-> V�rifier si on � un ROWID de jointure
        If aRec.Fields("ENREG_JOIN") <> "" Then
            '-> R�cup�r�r la table de jointure
            File_Jointure = Entry(2, aRec.Fields("ENREG_JOIN"), "�")
            '-> r�cup�rer le num�ro d'enregistrement de jointure
            Num_Enreg_join = Entry(3, aRec.Fields("ENREG_JOIN"), "�")
            Call PrintDebug("Proc�dure B3D_FIND_HISTO - Acc�s � la table de jointure : " & aRec.Fields("ENREG_JOIN"))
            '-> Pointer sur la table
            Set FnameJoin = ServeurDOG.fNames(Entry(1, aRec.Fields("ENREG_JOIN"), "�") & "|" & File_Jointure)
            '-> Analyse de tous les champs de type recherche
            For Each aFieldJoin In FnameJoin.Fields
                If aFieldJoin.IsF10 And aFieldJoin.Num_Field <> FnameJoin.IndexDirect Then
                    '-> Pr�parer la requete
                    SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Entry(1, aRec.Fields("ENREG_JOIN"), "�") & "�" & File_Jointure & "' AND [NUM_ENREG] = '" & Num_Enreg_join & _
                                          "' AND [FIELD] = '" & aFieldJoin.Num_Field & "'"
                    '-> Mode debug
                    Call PrintDebug("Proc�dure B3D_FIND_DATA - Recherche des valeurs de jointures pour l'enreg : " & Num_Enreg_join & Chr(13) & Chr(10) & SQLValue)
                    '-> Ex�cuter la requete
                    Set RecJoin = SetTable(SQLValue)
                    '-> Tester le retour
                    If Not RecJoin.EOF Then
                        '-> Concatainer la retour
                        ValueJoin = ValueJoin & " - " & FormatZone(RTrim(DeCrypt(RecJoin.Fields("ZON_DATA"))), aFieldJoin.Num_Field)
                    End If 'Si la requete a aboutis
                End If 'Si le champ est en recherche
            Next 'Pour tous les champs de la table de jointure
        End If 'Si rowid de jointure
    End If 'Si on doit r�cup�rer la jointure
    
    '-> Envoyer la reponse
    If Trim(aRec.Fields("EXTENT")) <> "" Then
        ValExtent = RTrim(aRec.Fields("FIELD")) & "_" & aRec.Fields("EXTENT")
    Else
        ValExtent = RTrim(aRec.Fields("FIELD"))
    End If
    Call PrintReponse(NumEnreg & "~" & ValExtent & "�" & ValueJoin & "�" & RTrim(aRec.Fields("FONCTION")) & "�" & SetDate(aRec.Fields("DAT_HISTO")) & "�" & SetHeure(aRec.Fields("HEU_HISTO")) & "�" & RTrim(aRec.Fields("OPE_MAJ")) & "�" & RTrim(aRec.Fields("PRG_MAJ")))
    
    aRec.MoveNext
Loop 'Pour tous les enreg

'-> Fermer le query
aRec.Close

'-> Quitter la proc�dure
Exit Sub

GestError:

    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure B3D_FIND_HISTO " & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)
    
    '-> Fermer le fichier r�ponse
    Call ResetReponse
    
    '-> Renvoyer une r�ponse d'erreur
    Call PrintReponse("B3D_FIND_HISTO~ERROR~" & Err.Number & "~" & Err.Description & "~")

End Sub

Private Sub B3D_GET_HISTO(Instruction As String)

'-> Ex : B3D_GET_HISTO~[Ident]�[Fname]�[Num_Enreg]�GetJoin~[Field]�[NbHisto]�......

Dim Param As String
Dim Id_Fname As String
Dim NumEnreg As String
Dim DefData As String
Dim DefField As String
Dim RetourEnreg As Integer
Dim i As Integer, j As Integer
Dim aRec As Object
Dim TopJoin As Integer
Dim ValueJoin As String
Dim FnameJoin As Fname
Dim aEmilieObj As EmilieObj
Dim RecJoin As Object
Dim File_Jointure As String
Dim Num_Enreg_join As String
Dim aFieldJoin As Field
Dim ValExtent As String

On Error GoTo GestError

'-> Analyse du param�trage
Param = Entry(2, Instruction, "~")
Id_Fname = UCase$(Trim(Entry(1, Param, "�")) & "�" & Trim(Entry(2, Param, "�")))
NumEnreg = Entry(3, Param, "�")
TopJoin = CInt(Entry(4, Param, "�"))
Param = Entry(3, Instruction, "~")

'-> Envoyer l'entete de la fonction
Call PrintReponse("B3D_GET_HISTO~OK~")

'-> R�cup�rer les histo pour tous les champs sp�cifi�s
For i = 1 To NumEntries(Param, "�")
    '-> Def d'un champ
    DefData = Entry(i, Param, "�")
    '-> Annalyse de tous les champs � mettre � jour
    DefField = Entry(1, DefData, "�")
    RetourEnreg = CInt(Trim(Entry(2, DefData, "�")))

    '-> Rechercher les histo du champ
    
    '-> Pr�parer la requete
    SQLValue = "SELECT * FROM [DEALB3D_HISTO] WHERE [ID_FNAME] = '" & Id_Fname _
                                  & "' AND [NUM_ENREG] = '" & NumEnreg _
                                  & "' AND [FIELD] = '" & DefField & "' ORDER BY [DAT_HISTO] DESC , [HEU_HISTO] DESC"
    
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_GET_HISTO -> Recherche des histos du champ : " & Chr(13) & Chr(10) & SQLValue)
    
    '-> Ex�cuter la requte
    Set aRec = SetTable(SQLValue)
    
    '-> Tester le retour
    If aRec.EOF Then
        '-> Imprimer le retour
        Call PrintReponse(NumEnreg & "~ERROR~90013~" & GetMesseprog("ERRORS", "90013") & "~" & DefField)
    Else
        '-> Analyse en boucle
        j = 0
        Do While Not aRec.EOF
            '-> De base on renvoie la valeur sans la jointure
            ValueJoin = FormatZone(RTrim(DeCrypt(aRec.Fields("ZON_DATA"))), aRec.Fields("FIELD"))
            '-> Tester si on doit r�cup�rer les jointures
            If TopJoin = 1 Then
                '-> V�rifier si on � un ROWID de jointure
                If aRec.Fields("ENREG_JOIN") <> "" Then
                    '-> R�cup�r�r la table de jointure
                    File_Jointure = Entry(2, aRec.Fields("ENREG_JOIN"), "�")
                    '-> r�cup�rer le num�ro d'enregistrement de jointure
                    Num_Enreg_join = Entry(3, aRec.Fields("ENREG_JOIN"), "�")
                    Call PrintDebug("Proc�dure B3D_FIND_HISTO - Acc�s � la table de jointure : " & aRec.Fields("ENREG_JOIN"))
                    '-> Pointer sur la table
                    Set FnameJoin = ServeurDOG.fNames(Entry(1, aRec.Fields("ENREG_JOIN"), "�") & "|" & File_Jointure)
                    '-> Analyse de tous les champs de type recherche
                    For Each aFieldJoin In FnameJoin.Fields
                        If aFieldJoin.IsF10 And aFieldJoin.Num_Field <> FnameJoin.IndexDirect Then
                            '-> Pr�parer la requete
                            SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Entry(1, aRec.Fields("ENREG_JOIN"), "�") & "�" & File_Jointure & "' AND [NUM_ENREG] = '" & Num_Enreg_join & _
                                                  "' AND [FIELD] = '" & aFieldJoin.Num_Field & "'"
                            '-> Mode debug
                            Call PrintDebug("Proc�dure B3D_FIND_DATA - Recherche des valeurs de jointures pour l'enreg : " & Num_Enreg_join & Chr(13) & Chr(10) & SQLValue)
                            '-> Ex�cuter la requete
                            Set RecJoin = SetTable(SQLValue)
                            '-> Tester le retour
                            If Not RecJoin.EOF Then
                                '-> Concatainer la retour
                                ValueJoin = ValueJoin & " - " & FormatZone(RTrim(DeCrypt(RecJoin.Fields("ZON_DATA"))), aFieldJoin.Num_Field)
                            End If 'Si la requete a aboutis
                        End If 'Si le champ est en recherche
                    Next 'Pour tous les champs de la table de jointure
                End If 'Si rowid de jointure
            End If 'Si on doit r�cup�rer la jointure
            '-> Imprimer le retour d'histo
            If Trim(aRec.Fields("EXTENT")) <> "" Then
                ValExtent = RTrim(aRec.Fields("FIELD")) & "_" & aRec.Fields("EXTENT")
            Else
                ValExtent = RTrim(aRec.Fields("FIELD"))
            End If
            Call PrintReponse(NumEnreg & "~" & ValExtent & "�" & ValueJoin & "�" & RTrim(aRec.Fields("FONCTION")) & "�" & SetDate(aRec.Fields("DAT_HISTO")) & "�" & SetHeure(aRec.Fields("HEU_HISTO")) & "�" & RTrim(aRec.Fields("OPE_MAJ")) & "�" & RTrim(aRec.Fields("PRG_MAJ")))
            
'            Call PrintReponse(NumEnreg & "~" & DefField & "�" & ValueJoin & "�" & RTrim(aRec.Fields("FONCTION")) & "�" & SetDate(aRec.Fields("DAT_HISTO")) & "�" & SetHeure(aRec.Fields("HEU_HISTO")) & "�" & RTrim(aRec.Fields("OPE_MAJ")) & "�" & RTrim(aRec.Fields("PRG_MAJ")))
            '-> Quitter si on a r�cupr� le bon nombre d'enregistrement
            If j > RetourEnreg And RetourEnreg <> 0 Then Exit Do
            '-> Enregistrement suivant
            aRec.MoveNext
        Loop
    End If
    '-> Fermer le query
    aRec.Close
Next

'-> Quitter la proc�dure
Exit Sub

GestError:

    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure B3D_GET_HISTO " & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)
    
    '-> Fermer le fichier r�ponse
    Call ResetReponse
    
    '-> Renvoyer une r�ponse d'erreur
    Call PrintReponse("B3D_GET_HISTO~ERROR~" & Err.Number & "~" & Err.Description & "~")


End Sub


Private Sub B3D_GET_DATA(Instruction As String)


'-> B3D_GET_DATA~[Ident]�[Fname]�[Num_Enreg]�[Top_Lock]�[Stat/Nostat]~[Fields]"

Dim Param As String
Dim Id_Fname As String
Dim NumEnreg As String
Dim TopLock As Integer
Dim strRetour As String
Dim TopStat As String

On Error GoTo GestError

'-> R�cup des infos
Param = Entry(2, Instruction, "~")
Id_Fname = UCase$(Trim(Entry(1, Param, "�")) & "�" & Trim(Entry(2, Param, "�")))
NumEnreg = Entry(3, Param, "�")
TopLock = Entry(4, Param, "�")
TopStat = Entry(5, Param, "�")

'-> Get des liste des champs de retour
Param = Entry(3, Instruction, "~")

If Trim(Param) = "" Then
    strRetour = NumEnreg & "~OK~"
Else
    '-> R�cup�rer les r�ponses
    strRetour = B3D_Get_Data_Val(Id_Fname, NumEnreg, TopLock, Param, CBool(TopStat))
End If

'-> Renvoyer la r�ponse
Call PrintReponse("B3D_GET_DATA~OK~")
Call PrintReponse(strRetour)

'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> Faire un reset du fichier r�ponse
    ResetReponse
    
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_GET_DATA : Erreur dans la proc�dure : " & Err.Number & " - " & Err.Description)
    
    '-> Reset de la r�ponse en cours
    Call ResetReponse
    
    '-> Retourner une valeur d'erreur
    Call PrintReponse("B3D_GET_DATA~ERROR~" & Err.Number & "~" & Err.Description)
    

End Sub

Private Sub B3D_FIND_DATA(Instruction As String)

'-> Cette proc�dure retourne pour un enregistrement sp�cifi� la liste des tous les enregs _
pr�sents dans la table DEALB3D_DATA

'B3D_FIND_DATA~[Ident]�[Fname]�[Num_Enreg]�[Lock/NoLock]�[Histo/Nohisto]�[Join/NoJoin]�[Stat/NoStat]


Dim Param As String
Dim Id_Fname As String
Dim NumEnreg As String
Dim aRec As Object
Dim aFname As Fname
Dim aField As Field
Dim strTri As String
Dim strNonTri As String
Dim i As Integer
Dim strRetour As String
Dim TopLock As Integer
Dim GetHisto As String
Dim TopHisto As Integer
Dim TopJoin As Integer
Dim ValueJoin As String
Dim FnameJoin As Fname
Dim aEmilieObj As EmilieObj
Dim RecJoin As Object
Dim File_Jointure As String
Dim Num_Enreg_join As String
Dim aFieldJoin As Field
Dim ValExtent As String
Dim TopStat As String

On Error GoTo GestError

'-> Recup des param
Param = Entry(2, Instruction, "~")
Id_Fname = UCase$(Trim(Entry(1, Param, "�")) & "�" & Trim(Entry(2, Param, "�")))
NumEnreg = Entry(3, Param, "�")
TopLock = CInt(Entry(4, Param, "�"))
TopHisto = CInt(Entry(5, Param, "�"))
TopJoin = CInt(Entry(6, Param, "�"))
TopStat = CInt(Entry(7, Param, "�"))

'-> Recherche des donn�es

'-> Pr�parer la requete
SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & NumEnreg & "'"

'-> Mode debug
Call PrintDebug("Proc�dure B3D_FIND_DATA -> Recherche des donn�es : " & Chr(13) & Chr(10) & SQLValue)

'-> Ex�cuter la requete
Set aRec = SetTable(SQLValue)

'-> Tester s'il y a des enregistrement
If aRec.EOF Then
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_FIND_DATA -> Recherche des donn�es : pas de donn�es")
    '-> Renvoyer une valeur d'erreur
    Call PrintReponse("B3D_FIND_DATA~ERROR~90014~" & GetMesseprog("ERRORS", "90014") & "~")
    '-> Fermer le query
    aRec.Close
    '-> Quitter la proc�dure
    Exit Sub
End If

'-> On a trouv� : renvoyer une valeur de succ�s
Call PrintReponse("B3D_FIND_DATA~OK~")

'-> On retourne les champs selon l'ordre de pr�sentation
Do While Not aRec.EOF
    '-> Pointer sur la table d'analyse
    Set aFname = ServeurDOG.fNames(Entry(1, Replace(Id_Fname, "�", "|"), "�"))
    '-> Get du num field
    Set aField = aFname.Fields(Trim(aRec.Fields("FIELD")))
    '-> Mettre � jour la liste des champs en histo
    '-> Pointer sur l'objet du catalogue
    Set aEmilieObj = ServeurDOG.Catalogue(aField.Ident & "|" & aField.Num_Field)
    aEmilieObj.CodeLangue = CodeLangue
    '-> V�rifier pour les champs � extent que l'on int�gre pas 2 fois le m�me champ
    If InStr(1, strNonTri, aField.Num_Field) = 0 Then
        If strNonTri = "" Then
            strNonTri = Format(aField.Ordre, "0000") & "�" & aField.Num_Field
        Else
            strNonTri = strNonTri & "|" & Format(aField.Ordre, "0000") & "�" & aField.Num_Field
        End If
    End If
    '-> Enreg suivant
    aRec.MoveNext
Loop

'-> Fermer le query
aRec.Close

'-> Trier la chaine
strTri = Tri(strNonTri, "|", "�")

'-> P�parer la chaine de retour pour le GetData
For i = 1 To NumEntries(strTri, "|")
    If strRetour = "" Then
        strRetour = Entry(i, strTri, "|") & "�" & CStr(TopJoin)
    Else
        strRetour = strRetour & "�" & Entry(i, strTri, "|") & "�" & CStr(TopJoin)
    End If
Next

'-> Faire maintenant un get data
Call PrintReponse(B3D_Get_Data_Val(Id_Fname, NumEnreg, CInt(TopLock), strRetour, CBool(TopStat)))

'-> Initialisaer la requete pour acc�s aux histos
If TopHisto Then B3D_FIND_HISTO "B3D_FIND_HISTO~" & Id_Fname & "�" & NumEnreg & "�" & CStr(TopJoin)

'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> Faire un reset du fichier r�ponse
    ResetReponse
    
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_FIND_DATA : Erreur dans la proc�dure : " & Err.Number & " - " & Err.Description)
    
    '-> Reset de la r�ponse en cours
    Call ResetReponse
    
    '-> Retourner une valeur d'erreur
    Call PrintReponse("B3D_FIND_DATA~ERROR~" & Err.Number & "~" & Err.Description)


End Sub

Private Sub B3D_ERASE_BUFFER(Instruction As String)

'-> Cette proc�dure lib�re un buffer

'B3D_ERASE_BUFFER~[Buffer Name]

Dim BufferName As String
Dim aBuffer As clsBuffer

On Error GoTo GestError

'-> R�cup�rer le nom du buffer
BufferName = Trim(UCase$(Entry(2, Instruction, "~")))

'-> V�rifier que le buffer sp�cifi� existe
If Not IsBuffer(BufferName) Then
    '-> Retourner un message d'erreur
    Call PrintReponse("B3D_ERASE_BUFFER~ERROR~90015~" & GetMesseprog("ERRORS", "90015") & "~")
Else
    '-> Supprimer le buffer
    Buffers.Remove ("BUF|" & BufferName)
    '-> Retouner une valeur de succ�s
    Call PrintReponse("B3D_ERASE_BUFFER~OK~")
End If

'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> Faire un reset du fichier r�ponse
    ResetReponse
    
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_ERASE_BUFFER : Erreur dans la proc�dure : " & Err.Number & " - " & Err.Description)
    
    '-> Reset de la r�ponse en cours
    Call ResetReponse
    
    '-> Retourner une valeur d'erreur
    Call PrintReponse("B3D_ERASE_BUFFER~ERROR~" & Err.Number & "~" & Err.Description)

End Sub

Private Sub B3D_SEEK_DATA(Instruction As String)

'-> Cette Proc�dure v�rifie si pour un champ donn� une valeur sp�cifi� est pr�sente dans la base

'Syntaxe : B3D_SEEK~[Num_Field]�[Value]

Dim aRec As Object
Dim aFname As Object
Dim Query As String
Dim strQuery As String
Dim Field As String
Dim ValueField As String
Dim Param As String
Dim aLink As Object
Dim aEmilieObj As Object

On Error GoTo GestError

'-> Eclater le param�trage
Param = Entry(2, Instruction, "~")
Field = Entry(1, Param, "�")
ValueField = Entry(2, Param, "�")

'-> V�rifier si une valeur existe pour un champ donn�

'-> Pr�parer la requete
SQLValue = "SELECT TOP 1 * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Ident & "�#FNAME#' AND [FIELD] = '" & Field & "' AND [ZON_DATA] = '" & ValueField & "'"

'-> Pointer sur l'objet en question
Set aEmilieObj = ServeurDOG.Catalogue(Ident & "|" & Field)

'-> Tester s'il ya des liens
If aEmilieObj.fNames.Count = 0 Then
    '-> Renvoyer une valeur de fin
    Call PrintReponse("B3D_SEEK_DATA~ERROR~90016~" & GetMesseprog("ERRORS", "90016") & "~")
    '-> Quitter la proc�dure
    Exit Sub
End If

'-> Analyse de la tabsle DEALB3D_DATA pour l'ident en cours et toutes les tables
For Each aLink In aEmilieObj.fNames
    '-> Rendre la main � la CPU
    DoEvents
    '-> Faire une copie du query
    strQuery = Replace(SQLValue, "#FNAME#", aLink.Num_Fname)
    '-> Pointer sur la table
    Set aFname = ServeurDOG.fNames(Ident & "|" & aLink.Num_Fname)
    '-> Positionner le code langue
    aFname.CodeLangue = CodeLangue
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_SEEK_DATA -> Recherche de la data : " & Chr(13) & Chr(10) & strQuery)
    '-> Faire pointer le recordset
    Set aRec = SetTable(strQuery)
    '-> Tester le retour
    If aRec.EOF Then
        '-> La valeur n'existe pas pour le champ sp�cifi�
        Call PrintReponse("B3D_SEEK_DATA~ERROR~90017~" & GetMesseprog("ERRORS", "90017") & "~" & Ident & "�" & aFname.Num_Fname & "�" & aFname.Designation)
    Else
        Call PrintReponse("B3D_SEEK_DATA~OK~" & Ident & "�" & aFname.Num_Fname & "�" & aFname.Designation)
    End If
    '-> Fermer le query
    aRec.Close
Next 'Pour toutes les tables dans le descruptif Fichier

'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> Faire un reset du fichier r�ponse
    ResetReponse
    
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_SEEK_DATA : Erreur dans la proc�dure : " & Err.Number & " - " & Err.Description)
    
    '-> Reset de la r�ponse en cours
    Call ResetReponse
    
    '-> Retourner une valeur d'erreur
    Call PrintReponse("B3D_SEEK_DATA~ERROR~" & Err.Number & "~" & Err.Description)

End Sub

Private Sub B3D_GET_SOCREF(Instruction As String)

'---> Cette proc�dure r�cup�re la soci�t� de r�f�rence pour une table donn�e et une soci�t� donn�e

' B3D_GET_SOCREF~[Valeur SOC en acces]�[Table]�[Value MasterField]

Dim aRec As Object
Dim Fname As String
Dim Soc_Ref As String
Dim Param As String
Dim Acces_Ref As String
Dim Num_Enreg_Ref As String

On Error GoTo GestError

'-> Get du param�trage
Param = Entry(2, Instruction, "~")
Soc_Ref = Entry(1, Param, "�")
Fname = Entry(2, Param, "�")
Acces_Ref = Entry(3, Param, "�")

'-> Recup du num_enreg de la table de r�f�rence soci�t� pour acces � DEALB3D_DATA

'-> Pr�parer la requete
SQLValue = "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & _
           Ident & "�" & ServeurDOG.GetSystemTableValue("REF_SOCIETE") & "' AND [ZON_INDEX_1] = '" & Soc_Ref & _
           "' AND [ZON_INDEX_2] = '" & Fname & "' AND [ZON_INDEX_3] = '" & Acces_Ref & "'"
'-> Mode debug
Call PrintDebug("Proc�dure B3D_GET_SOCREF -> Recherche de la table de r�f�rence : " & Chr(13) & Chr(10) & SQLValue)

'-> Ex�cuter la rquete
Set aRec = SetTable(SQLValue)
    
'-> Si pas de param�trage de soci�t� de r�f�rence ne rien faire
If Not aRec.EOF Then
    '-> Get du numerneg de r�f�rence
    Num_Enreg_Ref = RTrim(DeCrypt(aRec.Fields("NUM_ENREG")))
    '-> fermer le query
    aRec.Close
    '-> Acces a DEALB3D_DATA pour recup�rer la valeur de la soci�t� de r�f�rence
    '-> Pr�parer la requete
    SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Ident & "�" & ServeurDOG.GetSystemTableValue("REF_SOCIETE") & "' AND [NUM_ENREG] = '" & Num_Enreg_Ref & "' AND [FIELD] = '" & ServeurDOG.GetSystemTableValue("SOCREF") & "'"
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_GET_SOCREF Param�trage OK -> Recherche de la valeur de socref : " & Chr(13) & Chr(10) & SQLValue)
    '-> Ex�cuter la requete
    Set aRec = SetTable(SQLValue)
    '-> R�cup�rer la valeur
    If Not aRec.EOF Then Soc_Ref = RTrim(DeCrypt(aRec.Fields("ZON_DATA")))
    '-> Fermer le query
    aRec.Close
End If

'-> Retourner la valeur
Call PrintReponse("B3D_GET_SOCREF~OK~" & Soc_Ref)

'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> Faire un reset du fichier r�ponse
    ResetReponse
    
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_GET_SOCREF : Erreur dans la proc�dure : " & Err.Number & " - " & Err.Description)
    
    '-> Reset de la r�ponse en cours
    Call ResetReponse
    
    '-> Retourner une valeur d'erreur
    Call PrintReponse("B3D_GET_SOCREF~ERROR~" & Err.Number & "~" & Err.Description)



End Sub

Private Sub B3D_SEARCH_DATA(Instruction As String)

'-> Cette Proc�dure r�cup�re les num enreg pour la valeur donn�e d'un champ

'Syntaxe : B3D_SEARCH_DATA~[Num_Field]�[Value]

Dim aRec As Object
Dim aFname As Object
Dim Query As String
Dim strQuery As String
Dim Field As String
Dim ValueField As String
Dim Param As String
Dim aLink As Object
Dim aEmilieObj As Object

On Error GoTo GestError

'-> Eclater le param�trage
Param = Entry(2, Instruction, "~")
Field = Entry(1, Param, "�")
ValueField = Entry(2, Param, "�")

'-> Seeting de la requete SQL
SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Ident & "�#FNAME#' AND [FIELD] = '" & Field & "' AND [ZON_DATA] = '" & ValueField & "'"

'-> Pointer sur l'objet en question
Set aEmilieObj = ServeurDOG.Catalogue(Ident & "|" & Field)

'-> Renvoyer une valeur de succ�s
Call PrintReponse("B3D_SEARCH_DATA~OK~")

'-> Analyse de la tabsle DEALB3D_DATA pour l'ident en cours et toutes les tables
For Each aLink In aEmilieObj.fNames
    '-> Rendre la main � la CPU
    DoEvents
    '-> Faire une copie du query
    strQuery = Replace(SQLValue, "#FNAME#", aLink.Num_Fname)
    '-> Pointer sur la table
    Set aFname = ServeurDOG.fNames(Ident & "|" & aLink.Num_Fname)
    '-> Positionner le code langue
    aFname.CodeLangue = CodeLangue
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_SEARCH_DATA -> Analyse de DEALB3D_DATA : " & Chr(13) & Chr(10) & strQuery)
    '-> Faire pointer le recordset
    Set aRec = SetTable(strQuery)
    '-> Tester le retour
    Do While Not aRec.EOF
        Call PrintReponse(Ident & "�" & aFname.Num_Fname & "�" & aRec.Fields("NUM_ENREG"))
        '-> Enreg suivant
        aRec.MoveNext
    Loop 'Pour tous les enreg
    '-> Fermer le query
    aRec.Close
Next 'Pour toutes les tables dans le descruptif Fichier

'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> Faire un reset du fichier r�ponse
    ResetReponse
    
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_SEARCH_DATA : Erreur dans la proc�dure : " & Err.Number & " - " & Err.Description)
    
    '-> Reset de la r�ponse en cours
    Call ResetReponse
    
    '-> Retourner une valeur d'erreur
    Call PrintReponse("B3D_SEARCH_DATA~ERROR~" & Err.Number & "~" & Err.Description)
    
End Sub


Private Sub B3D_SELECT(Instruction As String)

'-> Cette proc�dure cr�er un recordset et retourne les ROWID dans le la r�ponse

'B3D_SELECT~[IDENT]�[FNAME]~[Requete SQL]~[Conditions sur champ hors Index]
                                            
Dim Param As String
Dim Id_Fname As String
Dim ReqSQL As String
Dim ReqCdt As String
Dim TempFileName As String
Dim hdlFile As Integer
Dim aRec As Object
Dim DefData As String
Dim DefField As String
Dim DefCondition As String
Dim DefValMini As String
Dim DefValMaxi As String
Dim i As Long
Dim NumEnregistrement As String

On Error GoTo GestError

'-> R�cup des infos
Param = Trim(Entry(2, Instruction, "~"))
Id_Fname = Trim(Entry(1, Param, "�")) & "�" & Trim(Entry(2, Param, "�"))
ReqSQL = Trim(Entry(3, Instruction, "~"))
ReqCdt = Trim(Entry(4, Instruction, "~"))
                                                                    
'-> Cr�ation de la requette sur les champs d'index ou DEALB3D_DATA si champ index direct sur fname

'-> Mode debug
Call PrintDebug("Proc�dure B3D_SELECT -> Ex�cution de la requete : " & Chr(13) & Chr(10) & ReqSQL)

'-> Ex�cuter la requete
Set aRec = SetTable(ReqSQL)
            
'-> Analyse du recordset
If Not aRec.EOF Then
    '-> Indiquer que la requete est OK
    Call PrintReponse("B3D_SELECT~OK~")
    Do While Not aRec.EOF
        NumEnregistrement = Trim(aRec.Fields("NUM_ENREG"))
            '-> On a trouv� les enregistrements dans l'index : v�rifier s'il y a des v�rifications � faire dans les champs
            If Trim(ReqCdt) <> "" Then
                '-> Si 3 ou 4 entr�es
                For i = 1 To NumEntries(ReqCdt, "�")
                    '-> R�cup�rer une condition
                    DefData = Entry(i, ReqCdt, "�")
                    DefField = Entry(1, DefData, "�")
                    DefCondition = Entry(2, DefData, "�")
                    DefValMini = Entry(3, DefData, "�")
                    If NumEntries(DefData, "�") = 4 Then
                        DefValMaxi = Entry(4, DefData, "�")
                    Else
                        DefValMaxi = DefValMini
                    End If
                    
                    '-> Tester les conditions d'exclusions
                    If TestCondition(Id_Fname, DefField, DefCondition, DefValMini, DefValMaxi, NumEnregistrement) Then
                        Call PrintReponse(NumEnregistrement)
                    End If
                Next 'Pour toutes les conditions des champs externes
            Else
                '-> Il n'y a pas  de champs � v�rifier dans DEALB3D_DATA
                Call PrintReponse(NumEnregistrement)
            End If 'Sil y a des vecteur data
        aRec.MoveNext
    Loop 'Pour le nombre d'entr�e
Else '-> S'il n'y a pas d'enreg
    Call PrintReponse("B3D_SELECT~ERROR~90014~" & GetMesseprog("ERRORS", "90014") & "~")
End If
                        
'-> Fermer le canal du query
aRec.Close
                                    
'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> Faire un reset du fichier r�ponse
    ResetReponse
    
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_SELECT : Erreur dans la proc�dure : " & Err.Number & " - " & Err.Description)
    
    '-> Reset de la r�ponse en cours
    Call ResetReponse
    
    '-> Retourner une valeur d'erreur
    Call PrintReponse("B3D_SELECT~ERROR~" & Err.Number & "~" & Err.Description)
                                    
                                    
End Sub



Private Sub B3D_NAVIGA(Instruction As String)

'-> Cette proc�dure r�cup�re les donn�es attach�es � un buffer et une page
'-> B3D_NAVIGA~[Nom_Buffer]�[Navigation]�[Num page pour DIRECT]~FIELDS .....

Dim BufferName As String
Dim aBuffer As clsBuffer
Dim Navigation As String
Dim NumPage As String

On Error GoTo GestError

'-> Get du nom du buffer
BufferName = Trim(UCase$(Entry(1, Entry(2, Instruction, "~"), "�")))
Navigation = Trim(UCase$(Entry(2, Entry(2, Instruction, "~"), "�")))

'-> V�rifier que le buffer existe
If Not IsBuffer(BufferName) Then
    '-> Retouner un message d'erreur
    Call PrintReponse("B3D_NAVIGA~ERROR~90015~" & GetMesseprog("ERRORS", "90015") & "~")
    '-> Quitter
    Exit Sub
End If 'Si le buffer existe

'-> Pointer sur le buffer
Set aBuffer = Buffers("BUF|" & BufferName)

'-> Tester qu'il y a au moins une page
If aBuffer.NbPages = 0 Then
     Call PrintReponse("B3D_NAVIGA~ERROR~90018~" & GetMesseprog("ERRORS", "90018") & "~")
    '-> Quitter
    Exit Sub
End If 'Si le buffer contient des pages

Select Case Entry(1, Navigation, "�")
    Case "FIRST"
        aBuffer.FirstPage Entry(3, Instruction, "~")
    Case "PREV"
        aBuffer.PrevPage Entry(3, Instruction, "~")
    Case "NEXT"
        aBuffer.NextPage Entry(3, Instruction, "~")
    Case "LAST"
        aBuffer.LastPage Entry(3, Instruction, "~")
    Case "DIRECT"
        '-> Get du num�ro de la page demand�
        NumPage = Entry(2, Entry(2, Instruction, "~"), "�")
        If Not IsNumeric(NumPage) Then
            Call PrintReponse("B3D_NAVIGA~ERROR~90019~" & GetMesseprog("ERRORS", "90019") & "~" & NumPage)
            Exit Sub
        End If
        '-> V�rifier les bornes
        If CDbl(NumPage) > aBuffer.NbPages Or CDbl(NumPage) < 1 Then
            Call PrintReponse("B3D_NAVIGA~ERROR~90019~" & GetMesseprog("ERRORS", "90019") & "~" & NumPage)
            Exit Sub
        End If
        '-> Envoyer la fonction
        aBuffer.ReturnPage CLng(NumPage), Entry(3, Instruction, "~")
    Case "CONTINU"
        aBuffer.Initialise_Buffer True
End Select

'-> Quitter la proc�dure
Exit Sub

GestError:
    
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_NAVIGA : Erreur dans la proc�dure : " & Err.Number & " - " & Err.Description)
    
    '-> Reset de la r�ponse en cours
    Call ResetReponse
    
    '-> Retourner une valeur d'erreur
    Call PrintReponse("B3D_NAVIGA~ERROR~" & Err.Number & "~" & Err.Description)

End Sub

Private Sub B3D_NAVIGA_SELECT(Instruction As String)

'-> Cette proc�dure cr�er un objet buffer et initialise la requete en PAGE
'-> B3D_NAVIGA_SELECT~[IDENT]�[FNAME]�[BUFFERNAME]�[BUFFERSIZE]�[PAGESIZE]~[Requete SQL]~[Conditions sur champ hors Index]

Dim Param As String
Dim aBuffer As clsBuffer

On Error GoTo GestError

'-> Cr�ation d'un objet Buffer
Set aBuffer = New clsBuffer

'-> R�cup des infos
Param = Trim(Entry(2, Instruction, "~"))
aBuffer.Id_Fname = Trim(Entry(1, Param, "�")) & "�" & Trim(Entry(2, Param, "�"))
aBuffer.BufferName = Trim(UCase$(Entry(3, Param, "�")))
aBuffer.BufferSize = CLng(Entry(4, Param, "�"))
aBuffer.BufferSizeValue = aBuffer.BufferSize
aBuffer.PageSize = CLng(Entry(5, Param, "�"))
aBuffer.ReqSQL = Trim(Entry(3, Instruction, "~"))
aBuffer.ReqCdt = Trim(Entry(4, Instruction, "~"))

'-> V�rfier que le buffer sp�cifi� n'existe pas d�ja
If IsBuffer(aBuffer.BufferName) Then
    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_NAVIGA_SELECT : Buffer existant : suppression du buffer :  " & aBuffer.BufferName)
    '-> Le buffer existe d�ja : le supprimer
    Buffers.Remove ("BUF|" & aBuffer.BufferName)
End If

'-> G�n�rer le buffer
aBuffer.Initialise_Buffer

'-> Mode debug
Call PrintDebug("Proc�dure B3D_NAVIGA_SELECT : Ajout du buffer dans la collection :  " & aBuffer.BufferName)

'-> Ajouter le buffer dans la collection
Buffers.Add aBuffer, "BUF|" & aBuffer.BufferName

'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure B3D_NAVIGA_SELECT : " & Chr(13) & Chr(10) & Err.Number & " - " & Err.Description)
    
    '-> Nettoyer le fichier R�ponse
    ResetReponse
    
    '-> Renvoyer une erreur
    Call PrintReponse("B3D_NAVIGA_SELECT~ERROR~" & Err.Number & " - " & Err.Description)

End Sub

Private Function IsBuffer(BufferName As String) As Boolean

'---> Cette proc�dure v�rifier si un buffer existe d�ja dans la collection

Dim aBuf As clsBuffer

On Error GoTo EndError

'-> Essayer de pointer sur l'objet
Set aBuf = Buffers("BUF|" & BufferName)

'-> Indiquer qu'il existe d�ja
IsBuffer = True

'-> Quitter la fonction
Exit Function

EndError:
    '-> Le buffer n'existe pas
    IsBuffer = False
End Function
Private Sub B3D_SCHEMA_CATALOGUE(Instruction As String)

'---> Cette proc�dure retourne la d�finition d'un champ

'Syntaxe  : B3D_SCHEMA_CATALOGUE~ORDERBY~[FIELD]�[FIELD]...

Dim Param As String
Dim OrderBy As String
Dim strTri As String
Dim strNonTri As String
Dim i As Integer
Dim aEmilieObj As EmilieObj
Dim DefData As String
Dim aFname As Fname

On Error GoTo GestError

'-> Get du param�trage
OrderBy = Entry(2, Instruction, "~")
Param = Entry(3, Instruction, "~")

'-> Se barrer si pas correct
If Trim(Param) = "" Then
    '-> On n'a pas pass� d'argument : r�cup�rer tout le catalogue
    For Each aEmilieObj In ServeurDOG.Catalogue
        If Param = "" Then
            Param = aEmilieObj.Num_Field
        Else
            Param = Param & "�" & aEmilieObj.Num_Field
        End If
    Next 'Pour tous les objets du catalogue
End If

'-> analyse de la liste des champs � r�cup�rer
For i = 1 To NumEntries(Param, "�")
    '-> Pointer sur le champ
    Set aEmilieObj = ServeurDOG.Catalogue(Ident & "|" & Entry(i, Param, "�"))
    '-> Cr�er la liste des champs tri�e dans le bon ordre
    Select Case OrderBy
        Case "0" 'Fill_Label (Designation)
            '-> Cr�er la chaine � trier
            If strNonTri = "" Then
                strNonTri = aEmilieObj.Fill_Label & "�" & aEmilieObj.Num_Field
            Else
                strNonTri = strNonTri & "|" & aEmilieObj.Fill_Label & "�" & aEmilieObj.Num_Field
            End If
            
        Case "1" 'Num_Field
            If strNonTri = "" Then
                strNonTri = aEmilieObj.Num_Field & "�" & aEmilieObj.Num_Field
            Else
                strNonTri = strNonTri & "|" & aEmilieObj.Num_Field & "�" & aEmilieObj.Num_Field
            End If
    End Select
Next

'-> Trier la liste
strTri = Tri(strNonTri, "|", "�")
If strNonTri = "" Then
    Call PrintReponse("B3D_SCHEMA_CATALOGUE~ERROR~90020~" & GetMesseprog("ERRORS", "90020") & "~")
    Exit Sub
End If

'-> Renvoyer une valeur de succ�s
Call PrintReponse("B3D_SCHEMA_CATALOGUE~OK~" & ServeurDOG.Catalogue.Count)

'-> Impression des objets du catalogue
For i = 1 To NumEntries(strTri, "|")
    '-> Pointer sur l'objet
    Set aEmilieObj = ServeurDOG.Catalogue(Ident & "|" & Entry(i, strTri, "|"))
    '-> Positionner le code langue
    aEmilieObj.CodeLangue = CodeLangue
    '-> Ligne de prop
    '-> Retourner la valeur de la r�ponse
    DefData = "B3D_SCHEMA_CATALOGUE~" & aEmilieObj.Ident & "�" & aEmilieObj.Num_Field & "�" _
                                      & aEmilieObj.Fill_Label & "�" & aEmilieObj.Progiciel & "�" _
                                      & aEmilieObj.DataType & "�" & aEmilieObj.WidgetFormat & "�" _
                                      & aEmilieObj.Filled_Type & "�" & aEmilieObj.Filled_Ascii & "�" _
                                      & aEmilieObj.Table_Correspondance & "�"

    '-> Pointer sur la table associ�e pour get du libell�
    If aEmilieObj.Table_Correspondance <> "" Then
        Set aFname = ServeurDOG.fNames(Ident & "|" & aEmilieObj.Table_Correspondance)
        DefData = DefData & aFname.Designation
    End If
    DefData = DefData & "�" & CInt(aEmilieObj.MultiLangue) & "�" & aEmilieObj.MasterField & "�" & aEmilieObj.Sep_Vecteur

    '-> Imprimer la ligne
    Call PrintReponse(DefData)
Next 'Pour tous les objets du catalogue

'-> Quitter la fonction
Exit Sub

GestError:
    '-> Annuler les r�ponses pr�c�dentes
    ResetReponse
    '-> Imprimer une trace
    Call PrintDebug("B3D_SCHEMA_CATALOGUE Erreur dans la proc�dure : " & Chr(13) & Chr(10) & Err.Number & " - " & Err.Description)
    '-> Renvoyer une valeur d'erreur
    Call PrintReponse("B3D_SCHEMA_CATALOGUE~ERROR~" & Err.Number & "~" & Err.Description & "~")
        
End Sub

Private Sub B3D_SCHEMA_DES(Instruction As String)

'-> Cette  proc�dure retourne le descriptif d'une tables ansi que le catalogue des champs ( et jointure ) _
qui la compose

'-> B3D_SCHEMA_DES~TRI[0 - Alpha / 1 - Num_Field ]~TABLE_CORRES[0 - Non / 1 - Oui] 2 -> Que le catalogue~[FNAME]� ... :

Dim TempoStr As String
Dim Param As String
Dim Analyse_Table As Boolean
Dim i As Integer
Dim aObj2 As Object
Dim aObj1 As Object
Dim aFname As Fname
Dim aEmilieObj As EmilieObj
Dim Continu As Boolean
Dim DefData As String
Dim strTri As String
Dim strNonTri As String
            
On Error GoTo GestError
            
'-> Initialiser les collections de tri
Set CatalogueBIS = New Collection
Set FnameBIS = New Collection

'-> R�cup�ration des donn�es pass�es en param�tre
TempoStr = Entry(2, Instruction, "~")
'-> Doit'on analyser les tables de correspondances
If Entry(3, Instruction, "~") = "0" Then
    Analyse_Table = False
Else
    Analyse_Table = True
End If
Param = Entry(4, Instruction, "~")
            
'-> Renvoyer un retour succ�s
Call PrintReponse("B3D_SCHEMA_DES~OK~")

For i = 1 To NumEntries(Param, "�")
    '-> Tester si la table n'est pas d�ja int�gr�e
    For Each aObj2 In FnameBIS
        '-> Tester si table d�ja trait�e
        If aObj2.Num = Entry(i, Param, "�") Then
            GoTo NextFname
            Exit For
        End If
    Next

    '-> Int�grer la table demand�e
    Call IntegreFname(Entry(i, Param, "�"), CodeLangue, TempoStr)

    '-> Int�grer les tables associ�es aux champs
    For Each aObj1 In CatalogueBIS
        '-> Si on doit tester la table associ�e � ce champ
        If Not aObj1.IsAnalysed Then
            '-> Indiqu� comme trait�
            aObj1.IsAnalysed = True
            '-> Pointer sur le champ du catalogue
            Set aEmilieObj = ServeurDOG.Catalogue(Ident & "|" & aObj1.Num)
            '-> Tester si on doit analyser les sous tables de correspondance
            If Analyse_Table Then
                '-> V�rifier si table associ�e
                If aEmilieObj.Table_Correspondance <> "" Then
                    '-> Analyse des tables d�ja trait�es
                    Continu = False
                    For Each aObj2 In FnameBIS
                        '-> Tester si table d�ja trait�e
                        If aObj2.Num = aEmilieObj.Table_Correspondance Then
                            Continu = True
                            Exit For
                        End If
                    Next
                    '-> Tester si on doit traiter la table
                    If Not Continu Then
                        '-> Lancer l'int�gration de la table
                        Call IntegreFname(aEmilieObj.Table_Correspondance, CodeLangue, TempoStr)
                    End If
                End If
            End If 'Si on doit analyser les sous tables
        End If 'Si l'objet n'est pas analys�
    Next 'Pour tous les objets dans la collection catalogue BIS
    
NextFname:
Next 'Pour toutes les tables � anaylser
                
'-> Trier les objets du catalogue par ordre alpha
strNonTri = ""

For Each aObj1 In CatalogueBIS

    '-> Pointer sur l'objet du catalogue
    Set aEmilieObj = ServeurDOG.Catalogue(Ident & "|" & aObj1.Num)
    aEmilieObj.CodeLangue = CodeLangue
    
    Select Case TempoStr

        Case "0" 'Fill_Label (Designation)
        
            '-> Cr�er la chaine � trier
            If strNonTri = "" Then
                strNonTri = aEmilieObj.Fill_Label & "�" & aEmilieObj.Num_Field
            Else
                strNonTri = strNonTri & "|" & aEmilieObj.Fill_Label & "�" & aEmilieObj.Num_Field
            End If
        
        Case "1" 'Num_Field
            If strNonTri = "" Then
                strNonTri = aEmilieObj.Num_Field & "�" & aEmilieObj.Num_Field
            Else
                strNonTri = strNonTri & "|" & aEmilieObj.Num_Field & "�" & aEmilieObj.Num_Field
            End If
    End Select

Next 'Pour tous les objets du catalogue bis
strTri = Tri(strNonTri, "|", "�")

If strTri <> "" Then
    '-> Impression des objets du catalogue
    For i = 1 To NumEntries(strTri, "|")
        '-> Pointer sur l'objet
        Set aEmilieObj = ServeurDOG.Catalogue(Ident & "|" & Entry(i, strTri, "|"))
        '-> Positionner le code langue
        aEmilieObj.CodeLangue = CodeLangue
        '-> Ligne de prop
        '-> Retourner la valeur de la r�ponse
        DefData = "B3D_SCHEMA_CATALOGUE~" & aEmilieObj.Ident & "�" & aEmilieObj.Num_Field & "�" _
                                          & aEmilieObj.Fill_Label & "�" & aEmilieObj.Progiciel & "�" _
                                          & aEmilieObj.DataType & "�" & aEmilieObj.WidgetFormat & "�" _
                                          & aEmilieObj.Filled_Type & "�" & aEmilieObj.Filled_Ascii & "�" _
                                          & aEmilieObj.Table_Correspondance & "�"
    
        '-> Pointer sur la table associ�e pour get du libell�
        If aEmilieObj.Table_Correspondance <> "" Then
            Set aFname = ServeurDOG.fNames(Ident & "|" & aEmilieObj.Table_Correspondance)
            DefData = DefData & aFname.Designation
        End If
        DefData = DefData & "�" & CInt(aEmilieObj.MultiLangue) & "�" & aEmilieObj.MasterField & "�" & aEmilieObj.Sep_Vecteur
    
        '-> Imprimer la ligne
        Call PrintReponse(DefData)
    
    Next 'Pour tous les objets du catalogue
End If

'-> Quitter la fonction
Exit Sub

GestError:

    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_SCHEMA_DES : Erreur dans la proc�dure : " & Err.Number & " - " & Err.Description)
    '-> Faire un reset de la r�ponse
    ResetReponse
    '-> Renvoyer un code Erreur
    Call PrintReponse("B3D_SCHEMA_DES~ERROR~" & Err.Number & "~" & Err.Description & "~")
    

End Sub


Private Sub B3D_SCHEMA_LFIELD(Instruction As String)

'-> Cette proc�dure r�cup�re les liens d'un objet du catalogue dans un schema

'-> B3D_SCHEMA_LFIELD~[Num_Field]
Dim Param As String
Dim aEmilieObj As Object
Dim aLinkF As Object
Dim aFname As Object
Dim aLinkI As Object
Dim aIname As Object
Dim aLinkInt As Object
Dim aInt As Object
Dim StrReponse As String
            
On Error GoTo GestError
            
Param = Trim(Entry(2, Instruction, "~"))

'-> Pointer sur l'objet du catalogue
Set aEmilieObj = ServeurDOG.Catalogue(Ident & "|" & Param)

'-> Testerr si ce champ est r�f�renc�
If aEmilieObj.fNames.Count = 0 Then
    '-> Renvoyer un message
    Call PrintReponse("B3D_SCHEMA_LFIELD~ERROR~90016~" & GetMesseprog("ERRORS", "90016") & "~")
    Exit Sub
End If

'-> Renvoyer une valeur de succ�s
Call PrintReponse("B3D_SCHEMA_LFIELD~OK~")

'-> Analyse du serveurDOG
For Each aLinkF In aEmilieObj.fNames
    '-> Pointer sur la table
    Set aFname = ServeurDOG.fNames(Ident & "|" & aLinkF.Num_Fname)
    aFname.CodeLangue = CodeLangue
    '-> Ajouter � la r�ponse
    Call PrintReponse("B3D_SCHEMA_LFIELD~" & aLinkF.Num_Fname & "�" & aFname.Designation & "��")
    '-> On tester s'il est r�f�renc� dans un index
    If aLinkF.LinkInames.Count <> 0 Then
        For Each aLinkI In aLinkF.LinkInames
            '-> Pointer sur l'index sp�cifi�
            Set aIname = aFname.Inames(aLinkI.Num_Iname)
            aIname.CodeLangue = CodeLangue
            '-> Ajouter � la r�ponse
            Call PrintReponse("B3D_SCHEMA_LFIELD~" & aLinkF.Num_Fname & "�" & aFname.Designation & "�" & aIname.Num_Iname & "�" & aIname.Designation & "�")
        Next 'Pour tous les liens vers les index
    End If
    '-> On teste s'il est r�f�renc� dans un interface
    If aLinkF.LinkInterfaces.Count <> 0 Then
        For Each aLinkInt In aLinkF.LinkInterfaces
            '-> Pointer sur l'interface sp�cifi�
            Set aInt = aFname.Interfaces("INT|" & aLinkInt.Name)
            aInt.CodeLangue = CodeLangue
            '-> Ajouter � la r�ponse
            Call PrintReponse("B3D_SCHEMA_LFIELD~" & aLinkF.Num_Fname & "�" & aFname.Designation & "��" & aInt.Name & "�" & aInt.Designation)
        Next 'Pour tous les liens vers les interfaces
    End If
Next 'Pour toutes les r�f�rences dans les tables de ce champ
 
'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_SCHEMA_LFIELD : Erreur dans la proc�dure : " & Err.Number & " - " & Err.Description)
    '-> Faire un reset de la r�ponse
    ResetReponse
    '-> Renvoyer un code Erreur
    Call PrintReponse("B3D_SCHEMA_LFIELD~ERROR~" & Err.Number & "~" & Err.Description & "~")
          

End Sub
Private Sub B3D_SCHEMA_LIST(Instruction As String)
                            
'-> Cette proc�dure donne la liste des tables associ�e � un ident
'-> B3D_SCHEMA_LIST~TRI[0 - Alpha / 1 - Num_Fname/ 2 - Mot_Cle] : Get de la liste des tables
            
Dim Param As String
Dim aFname As Fname
Dim strTri As String
Dim strNonTri As String
Dim i As Integer
            
On Error GoTo GestError

'-> R�cup�ration du param�trage
Param = Entry(2, Instruction, "~")

'-> Renvoyer message de retour
Call PrintReponse("B3D_SCHEMA_LIST~OK~" & ServeurDOG.fNames.Count)

'-> R�cup�rer la liste des tables dans le descriptif
For Each aFname In ServeurDOG.fNames
                        
    '-> Positionner le code langue
    aFname.CodeLangue = CodeLangue
                        
    '-> Effectuer un tri de la liste des tables
    Select Case Param
    
        Case "0" 'Tri par d�signation
            If strNonTri = "" Then
                strNonTri = aFname.Designation & "�" & aFname.Num_Fname
            Else
                strNonTri = strNonTri & "|" & aFname.Designation & "�" & aFname.Num_Fname
            End If
        
        Case "1" 'Tri par Num�ro
            If strNonTri = "" Then
                strNonTri = aFname.Num_Fname & "�" & aFname.Num_Fname
            Else
                strNonTri = strNonTri & "|" & aFname.Num_Fname & "�" & aFname.Num_Fname
            End If
        
    End Select

Next
'-> Quitter si pas de tables
If strNonTri = "" Then Exit Sub

'-> Trier la chaine
strTri = Tri(strNonTri, "|", "�")

For i = 1 To NumEntries(strTri, "|")
    '-> Pointer sur la table
    Set aFname = ServeurDOG.fNames(Ident & "|" & Entry(i, strTri, "|"))
    '-> Positionner le code langue de la table
    aFname.CodeLangue = CodeLangue
    '-> Renvoyer les informations
    Call PrintReponse(aFname.Num_Fname & "�" & aFname.Designation)
Next

'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_SCHEMA_LIST : Erreur dans la proc�dure : " & Err.Number & " - " & Err.Description)
    '-> Faire un reset de la r�ponse
    ResetReponse
    '-> Renvoyer un code Erreur
    Call PrintReponse("B3D_SCHEMA_LIST~ERROR~" & Err.Number & "~" & Err.Description & "~")
          

End Sub

Private Sub B3D_CATALOGUE_LINK()

'---> Affiche la liste de tous les liens de tous les objets du catalogue
'-> B3D_CATALOGUE_LINK~
            
Call Get_EmilieLink


End Sub

Private Sub B3D_CATALOGUE(Instruction As String)

'-> R�cup�re tout le catalogue ( ou le descro des chams sp�cifi�s) associ� � un ident

'-> B3D_CATALOGUE~TRI[0 - Alpha / 1 - Num_Field~[Num_FIELD] � ...
            ' Pour avoir tout le catalogue : Num_Field = ""
            
Dim Param As String
Dim TempoStr As String
Dim strNonTri As String
Dim strTri As String
Dim aEmilieObj As Object
Dim aFname As Object
Dim Continu As Boolean
Dim DefData As String
Dim i As Integer

On Error GoTo GestError
            
'-> Recup des param�trages
Param = Entry(3, Instruction, "~")   'Liste des champs
TempoStr = Entry(2, Instruction, "~")   'Ordre de tri

'-> Cr�er la chaine � trier
strNonTri = ""
For Each aEmilieObj In ServeurDOG.Catalogue

    '-> Teste selon le tri
    If Param <> "" Then
        '-> Ne pas ajouter par d�faut
        Continu = False
        '-> Il faut v�rifier si l'objet est sp�cifi� dans la liste des champs
        For i = 1 To NumEntries(Param, "�")
            If aEmilieObj.Num_Field = Entry(i, Param, "�") Then
                '-> On a trouv� : sortir de la boucle
                Continu = True
                Exit For
            End If
        Next 'Pour tous les champs sp�cifi�s
    Else
        Continu = True
    End If

    If Continu Then

        Select Case TempoStr

            Case "0" 'Fill_Label (Designation)
            
                '-> Cr�er la chaine � trier
                If strNonTri = "" Then
                    strNonTri = aEmilieObj.Fill_Label & "�" & aEmilieObj.Num_Field
                Else
                    strNonTri = strNonTri & "|" & aEmilieObj.Fill_Label & "�" & aEmilieObj.Num_Field
                End If
            Case "1" 'Num_Field
                If strNonTri = "" Then
                    strNonTri = aEmilieObj.Num_Field & "�" & aEmilieObj.Num_Field
                Else
                    strNonTri = strNonTri & "|" & aEmilieObj.Num_Field & "�" & aEmilieObj.Num_Field
                End If
        End Select
    End If 'Si on doit ajouter dans la matrice
Next 'Pour tous les objets du catalogue

'-> Effectuer un tri de la liste
strTri = Tri(strNonTri, "|", "�")

'-> Imprimer une ligne de retour
Call PrintReponse("B3D_CATALOGUE~OK~" & NumEntries(strTri, "|"))

'-> Analyse de la matrice de retour
For i = 1 To NumEntries(strTri, "|")
    
    '-> Pointer sur m'objet Emilie
    Set aEmilieObj = ServeurDOG.Catalogue(Ident & "|" & Entry(i, strTri, "|"))
    
    '-> Positionner le code langue
    aEmilieObj.CodeLangue = CodeLangue
    
    '-> Retourner la valeur de la r�ponse
    DefData = "B3D_SCHEMA_CATALOGUE~" & aEmilieObj.Ident & "�" & aEmilieObj.Num_Field & "�" _
                                      & aEmilieObj.Fill_Label & "�" & aEmilieObj.Progiciel & "�" _
                                      & aEmilieObj.DataType & "�" & aEmilieObj.WidgetFormat & "�" _
                                      & aEmilieObj.Filled_Type & "�" & aEmilieObj.Filled_Ascii & "�" _
                                      & aEmilieObj.Table_Correspondance & "�"
    
    '-> Pointer sur la table associ�e pour get du libell�
    If aEmilieObj.Table_Correspondance <> "" Then
        Set aFname = ServeurDOG.fNames(Ident & "|" & aEmilieObj.Table_Correspondance)
        DefData = DefData & aFname.Designation
    End If
    DefData = DefData & "�" & CInt(aEmilieObj.MultiLangue) & "�" & aEmilieObj.MasterField & "�" & aEmilieObj.Sep_Vecteur

    '-> Imprimer la ligne
    Call PrintReponse(DefData)

Next 'Pour tous les champs sp�cifi�s dans la matrice
            
'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> Mode debug
    Call PrintDebug("Proc�dure B3D_CATALOGUE : Erreur dans la proc�dure : " & Err.Number & " - " & Err.Description)
    '-> Faire un reset de la r�ponse
    ResetReponse
    '-> Renvoyer un code Erreur
    Call PrintReponse("B3D_CATALOGUE~ERROR~" & Err.Number & "~" & Err.Description & "~")
                 

End Sub

Private Sub B3D_FIELD_TABLE(Instruction As String)

'-> Permet de r�cup�rer les champs associ�s � une table ( table jointe ) pour faire les F10

'B3D_FIELD_TABLE~IDENT�NUM_FIELD�BEGIN_VALUE_FIELD
' BEGIN_VALUE_FIELD a blanc pour avoir tous les enregistrements
            
'-> R�cup�rer la table associ�e au champ
'-> Si pas de table : B3D_FIELD_TABLE~ERROR~TABLE_NOT_SET
'-> Si table n'est pas en acc�s direct : B3D_FIELD_TABLE~ERROR~TABLE_NOT_INDEXDIRECT
'-> Sinon : B3D_FIELD_TABLE~OK~NUM_FIELD�COL_LABEL�...
'-> Si pas de champs d'entete : B3D_FIELD_TABLE~ERROR~TABLE_NO_FIELD
'-> Liste des enregistrements :
' NUM_ENREG~VALUE_FIELD�VALUE_FIELD�...
        
            
Dim Id_Fname As String
Dim Param As String
Dim aEmilieObj As Object
Dim aFname As Object
Dim aField As Object
Dim DefData As String
Dim Num_Table As String
Dim Continu As Boolean
Dim strNonTri As String
Dim strTri As String
Dim strRetour As String
Dim i As Integer
Dim aRec As Object
Dim aRec2 As Object
Dim ValueField As String

On Error GoTo GestError

'-> Recup des informations
Id_Fname = Entry(1, Entry(2, Instruction, "~"), "�")
Param = Entry(2, Entry(2, Instruction, "~"), "�") 'NumField

'-> Test si valeur de recherche dans la table ( facultative )
If NumEntries(Entry(2, Instruction, "~"), "�") = 3 Then
       ValueField = Trim(Entry(3, Entry(2, Instruction, "~"), "�"))
End If

'-> Pointer sur le champ pour get de la table associ�e
Set aEmilieObj = ServeurDOG.Catalogue(Id_Fname & "|" & Param)

'-> V�rifier qu'il y a une table asscoci�e
If aEmilieObj.Table_Correspondance = "" Then
    PrintReponse ("B3D_FIELD_TABLE~ERROR~90022~" & GetMesseprog("ERRORS", "90022") & "~")
    Exit Sub
End If

'-> Pointer sur la table asscoi�e
Set aFname = ServeurDOG.fNames(Id_Fname & "|" & aEmilieObj.Table_Correspondance)

'-> V�rifier que table en acces directe
If aFname.IndexDirect = "" Then
    PrintReponse ("B3D_FIELD_TABLE~ERROR~90023~" & GetMesseprog("ERRORS", "90023") & "~")
    Exit Sub
Else
    DefData = aFname.IndexDirect
End If

'-> Recup�rer le num�ro de la table associ�e au champ � afficher
Num_Table = aEmilieObj.Table_Correspondance


'-> Cr�er une liste des champs d'entete � trier par num d'ordre
For Each aField In aFname.Fields
    '-> Tester si le champ est en recherche
    If aField.IsF10 Then
        '-> Pointer sur le champ pour r�cup�rer son mot cl�
        Set aEmilieObj = ServeurDOG.Catalogue(Id_Fname & "|" & aField.Num_Field)
        '-> Si champ en multi langue ne prendre que pour le code langue sp�cifi�
        If aEmilieObj.MultiLangue <> 0 Then
            '-> Tester si la champ sp�cifi� est celui de la langue demand�e
            If aEmilieObj.MultiLangue = CodeLangue Then
                Continu = True
            Else
                Continu = False
            End If
        Else ' Si champ en multi langue
            Continu = True
        End If
        '-> Ajouter si OK
        If Continu Then
            If strNonTri = "" Then
                strNonTri = Format(aField.Ordre, "0000") & "�" & aField.Num_Field
            Else
                strNonTri = strNonTri & "|" & Format(aField.Ordre, "0000") & "�" & aField.Num_Field
            End If
        End If
    End If
Next 'Pour tous les champs de la table

'-> Tester s'il y a des champs de navigation
If strNonTri = "" Then
    Call PrintReponse("B3D_FIELD_TABLE~ERROR~90024~" & GetMesseprog("ERRORS", "90024") & "~")
    Exit Sub
End If

'-> Trier la liste s'il y a des champs d'entetes
strTri = Tri(strNonTri, "|", "�")

'-> Imprimer l'entete
strRetour = "B3D_FIELD_TABLE~OK~"
For i = 1 To NumEntries(strTri, "|")
    '-> Pointer sur le champ sp�cifi�
    Set aEmilieObj = ServeurDOG.Catalogue(Id_Fname & "|" & Entry(i, strTri, "|"))
    '-> Positionner le code langue
    aEmilieObj.CodeLangue = CodeLangue
    '-> Ajouter dans la Liste d'entete
    strRetour = strRetour & Entry(i, strTri, "|") & "�" & aEmilieObj.Fill_Label
    If i <> NumEntries(strTri, "|") Then strRetour = strRetour & "�"
Next

'-> Imprimer la liste des champs d'entete
Call PrintReponse(strRetour)
                        
'-> R�cup�rer la liste des enregistrents dans la base

'-> Pr�parer la requete
If ValueField = "" Then
    SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname & "�" & Num_Table & _
                        "' AND [FIELD] = '" & DefData & "' ORDER BY [ZON_DATA]"
Else
    SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname & "�" _
                        & Num_Table & "' AND [FIELD] = '" & DefData & "' AND [ZON_DATA] LIKE '" & ValueField & "*' ORDER BY [ZON_DATA]"
                        
End If
'-> Mode debug
Call PrintDebug("Proc�dure B3D_FIELD_TABLE -> Recherche des datas : " & Chr(13) & Chr(10) & SQLValue)
'-> Ex�cuter la requete
Set aRec = SetTable(SQLValue)
               
If Not aRec.EOF Then
    '-> R�cup�rer les donn�es dans la base
    strRetour = ""
    Do While Not aRec.EOF
        '-> Get du num_enreg
        strRetour = aRec.Fields("NUM_ENREG") & "~"
        '-> R�cup�rer les datas que l'on veut afficher
        For i = 1 To NumEntries(strTri, "|")
            '-> Pointer sur l'enregistrement de la base
            
            '-> Pr�parer la requete
            SQLValue = "SELECT TOP 1 * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname & "�" & Num_Table & _
                            "' AND [FIELD] = '" & Entry(i, strTri, "|") & "' AND [NUM_ENREG] = '" & aRec.Fields("NUM_ENREG") & "'"
            '-> Mode debug
            Call PrintDebug("Proc�dure B3D_FIELD_TABLE -> Pointer sur les  datas : " & Chr(13) & Chr(10) & SQLValue)
            '-> Ex�cuter la requete
            Set aRec2 = SetTable(SQLValue)
            '-> Tester s'il y a des enreg
            If Not aRec2.EOF Then
                '-> Ajouter dans la ligne
                strRetour = strRetour & Entry(i, strTri, "|") & "�" & RTrim(aRec2.Fields("ZON_DATA"))
                '-> Ajouter un �
                If i <> NumEntries(strTri, "|") Then strRetour = strRetour & "�"
            End If
            '-> Fermer le query
            aRec2.Close
        Next 'Pour tous les champs rattach�s
        '-> Imprimer la ligne
        Call PrintReponse(strRetour)
        '-> Enreg suivant
        aRec.MoveNext
    Loop
     '-> Fermer le query
    aRec.Close
End If
                
'-> Quitter la proc�dure
Exit Sub

GestError:
    
    '-> G�rer les conflits de locks
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    '-> Envoyer un debug
    Call PrintDebug("B3D_FIELD_TABLE : Erreur dans la proc�dure." & Chr(13) & Chr(10) & SQLValue & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)
    '-> Faire un reste du fichier r�ponse en cours
    ResetReponse
    '-> Imprimer une Erreur
    Call PrintReponse("B3D_FIELD_TABLE~ERROR~" & Err.Number & "~" & Err.Description & "~")

End Sub

Private Sub B3D_INTERFACE(Instruction As String)

'---> Cette  proc�dure int�gre un interface

Call Integration_Interface(Entry(2, Instruction, "~"))


End Sub



Private Sub B3D_CRYPT(Instruction As String)

'---> Indiquer que l'on doit crypter/decrypter les echanges

'B3D_CRYPT~[0-> Pas Crypter] , [1-> Crypt matrice 1] ....

If Entry(2, Instruction, "~") = "1" Then    'THIERRY � GERER Le multi Crypt
    '-> Indiquer que l'on crypt
    Top_Crypt = True
    '-> Reponse
    Call PrintReponse("B3D_CRYPT~OK~CRYPT")
Else
    '-> Indiquer que l'on ne crypte pas
    Top_Crypt = False
    '-> Reponse
    Call PrintReponse("B3D_CRYPT~OK~UNCRYPT")
End If

End Sub

Private Sub B3D_TRACE(Instruction As String)

'---> Indiquer qu'il faut garder une trace des �changes

'B3D_TRACE~[1-> Oui] , [0-> Non]

'-> Inverser la propri�t�
If Entry(2, Instruction, "~") = "1" Then
    '-> On demande d'activer la trace
    If Not TraceMode Then
        '-> Indiquer que la tra�abilit� est ok
        TraceMode = True
        '-> Ouvrir le fichier trace
        Call OpenTrace
        '-> Envoyer la r�ponse
        Call PrintReponse("B3D_TRACE~OK~")
    Else
        '-> On demande d'activer la trace alors qu'elle est d�la
        Call PrintReponse("B3D_TRACE~OK~ALREADY_SET")
    End If
Else
    '-> D�sactiver la trace si elle est active
    If TraceMode Then
        '-> Envoyer la r�ponse
        Call PrintReponse("B3D_TRACE~OK~")
        '-> La trace est activ�e : la fermer
        Call CloseTrace
        '-> Trace bloqu�e
        TraceMode = False
    Else
        '-> On demande d'activer la trace alors qu'elle est d�la
        Call PrintReponse("B3D_TRACE~OK~ALREADY_STOPPED")
    End If
End If
    
End Sub

Private Sub B3D_DEBUG(Instruction As String)

'---> Indiquer qu'il faut passer ou non en mode debug

'B3D_DEBUG~[1-> Oui] , [0-> Non]

'-> Inverser la propri�t�
If Entry(2, Instruction, "~") = "1" Then
    '-> On demande d'activer le mode debug
    If Not DebugMode Then
        '-> Indiquer que le mode debug est OK
        DebugMode = True
        '-> Ouvrir le fichier debug
        Call OpenDebugFile
        '-> Envoyer la r�ponse
        Call PrintReponse("B3D_DEBUG~OK~")
    Else
        '-> On demande d'activer le debug alors qu'il est d�ja en cours
        Call PrintReponse("B3D_DEBUG~OK~ALREADY_SET")
    End If
Else
    '-> D�sactiver le mode debug
    If DebugMode Then
        '-> Fermer le fichier debug
        Call CloseDebug
        '-> Debug ferm�
        DebugMode = False
        '-> Envoyer la r�ponse
        Call PrintReponse("B3D_DEBUG~OK~")
    Else
        '-> On demande d'activer la trace alors qu'elle est d�la
        Call PrintReponse("B3D_DEBUG~OK~ALREADY_STOPPED")
    End If
End If
    
End Sub

Private Sub B3D_PROG(Instruction As String)
    
'---> Indique qu'il faut changer le nom du prog pour mlise � jour a base

'-> B3D_PROG~[Nom Prog]
'-> Retour
Programme = Entry(2, Instruction, "~")
Call PrintReponse("B3D_PROG~OK~" & Programme)

End Sub

Private Sub B3D_LOCKSPE()

'---> Sp�cialisation du num�ro de lock attach� � l'agent B3D

'-> SYNTAXE : B3D_LOCKSPE~
'-> Retour : Retourne le num�ro de lock SPE

'-> Cr�ation du num�ro
NumLockSpe = IDSession
If Len(IDSession) > 20 Then NumLockSpe = Mid$(IDSession, 1, 20)

'-> Retourner la r�ponse
Call PrintReponse("B3D_LOCKSPE~OK~" & NumLockSpe)

End Sub

Public Sub B3D_EXIT()

On Error Resume Next
            
'-> Fermer la connexion
CloseConnectB3D
            
'-> Lib�rer le serveur DOG
Set ServeurDOG = Nothing
            
'-> Fermer tous les fichiers ouverts
Reset
                                                
'-> Suppression du fichier DMD
If Dir$(DmdFile, vbNormal) <> "" Then Kill DmdFile

'-> Suppression du fichier TMP
If Dir$(TempFileName, vbNormal) <> "" Then Kill TempFileName

'-> Suppression du fichier RSP
If Dir$(RspFile, vbNormal) <> "" Then Kill RspFile

'-> Supprimer le fichier Compteur
If Dir$(CptFile, vbNormal) <> "" Then Kill CptFile

'-> Cr�er l'odre de fin de programme
Call EndProg
DoEvents

'-> Quitter le programme
End

End Sub

Private Sub Timer1_Timer()

'-> Analyse du r�pertoire de demande
If Dir$(DmdFile, vbNormal) <> "" Then
    '-> DEBUGMODE
    Call PrintDebug("Instructions re�ues : " & Now)
    '-> Rendre la main � la CPU
    DoEvents
    '-> Lancer l'analyse du fichier ASCII
    If Not OpenDemande(DmdFile) Then Exit Sub
    '-> Lancer l'analyse des instructions
    Call AnalyseDemande
End If
    
'-> Rendre la main � la CPU
DoEvents
    
End Sub

Private Function IsField(ByVal Num_Field As String, ByVal aCol As Collection) As Boolean

Dim aObj As Obj

For Each aObj In aCol
    If aObj.Num = Num_Field Then
        IsField = True
        Exit Function
    End If
Next

IsField = False

End Function

Private Sub IntegreFname(ByVal Num_Fname As String, ByVal CodeLangue As Integer, ByVal OrderTri As String)


Dim aFname As Fname
Dim aField As Field
Dim aIname As Iname
Dim aIfield As IFIELD
Dim aInt As Object
Dim aIntField As Object
Dim aObj1 As Obj
Dim i As Integer, j As Integer
Dim strTri As String
Dim strTri1 As String
Dim strNonTri As String
Dim strNonTri1 As String
Dim DefData As String
Dim aEmilieObj As EmilieObj

On Error GoTo GestError

'-> V�rifier si la table existe
If Not ServeurDOG.IsFname(Num_Fname) Then
    Call PrintReponse("B3D_SCHEMA_DES~ERROR~90025~" & GetMesseprog("ERRORS", "90025") & "~" & Num_Fname)
    Exit Sub
End If

'-> Analyse de la table pass�e en param
Set aFname = ServeurDOG.fNames(Ident & "|" & Num_Fname)

'-> Ajouter dans la collection des tables d�ja trait�es
Set aObj1 = New Obj
aObj1.Num = aFname.Num_Fname
FnameBIS.Add aObj1, "NUM|" & aFname.Num_Fname

'-> Code langue
aFname.CodeLangue = CodeLangue

'-> Valeur de retour
DefData = "B3D_SCHEMA_FNAME~" & aFname.Num_Fname & "�" & aFname.Name & "�" & _
aFname.Designation & "�" & CInt(aFname.IsReplica) & "�" & CInt(aFname.IsStat) & "�" & _
aFname.IndexDirect & "�" & aFname.IsChampRef & "�" & aFname.IndexJointure & "�" & CStr(aFname.NiveauReplica)

'-> Imprimer les propri�t�s de la table
Call PrintReponse(DefData)
                                                           
'-> Cr�er la liste des champs tri�e selon l'ordre de tri
strNonTri = ""
For Each aField In aFname.Fields

    If strNonTri = "" Then
        strNonTri = Format(aField.Ordre, "0000") & "�" & aField.Num_Field
    Else
        strNonTri = strNonTri & "|" & Format(aField.Ordre, "0000") & "�" & aField.Num_Field
    End If
            
Next

'-> trier la iste
strTri = Tri(strNonTri, "|", "�")
                    
                    
If strTri <> "" Then
    '-> analyse des champs
    For i = 1 To NumEntries(strTri, "|")
        '-> Pointer sur le champ
        Set aField = aFname.Fields(Entry(i, strTri, "|"))
        
        '-> Propri�t�s
        DefData = "B3D_SCHEMA_FIELD~" & aFname.Num_Fname & "�" & Format(aField.Ordre, "0000") & "�" & _
        aField.Num_Field & "�" & CInt(aField.IsReplica) & "�" & CInt(aField.IsCryptage) & "�" & _
        CInt(aField.IsHisto) & "�" & CInt(aField.IsF10) & "�" & CInt(aField.NonModifiable) & "�" & CInt(aField.IsExtent)
        
        '-> Imprimer la ligne de d�finition du champ
        Call PrintReponse(DefData)
        
        '-> Ajout dans la collection des champs s'il n'est pas d�ja r�f�renc�
        If Not IsField(aField.Num_Field, CatalogueBIS) Then
            '-> Le champ n'existe pas : l'ajouter dans la collection
            Set aObj1 = New Obj
            aObj1.Num = aField.Num_Field
            CatalogueBIS.Add aObj1, "NUM|" & aField.Num_Field
        End If
    Next 'Pour tous les champs de la table
End If

'-> Ajout de la liste des index associ�s � cette table
If aFname.IndexDirect = "" Then

    '-> Trier la liste des index par num�ro
    strNonTri = ""
    For Each aIname In aFname.Inames
        If strNonTri = "" Then
            strNonTri = Format(aIname.Num_Iname, "000") & "�" & Format(aIname.Num_Iname, "000")
        Else
            strNonTri = strNonTri & "|" & Format(aIname.Num_Iname, "000") & "�" & Format(aIname.Num_Iname, "000")
        End If
    Next
    
    '-> Tester s'il y a des index
    If strNonTri = "" Then GoTo EndSub
    
    '-> Trier la liste
    strTri = Tri(strNonTri, "|", "�")
    
    '-> Analyse des index
    For i = 1 To NumEntries(strTri, "|")
    
        '-> Pointer sur l'index
        Set aIname = aFname.Inames(Entry(i, strTri, "|"))
    
        '-> Code langue
        aIname.CodeLangue = aFname.CodeLangue
        
        '-> Ligne de prop
        DefData = "B3D_SCHEMA_INAME~" & aFname.Num_Fname & "�" & aIname.Num_Iname & "�" & _
        aIname.Code & "�" & aIname.Designation & "�" & CInt(aIname.IsUnique) & "�" & aIname.Groupe
        
        '-> Imprimer la ligne de propri�t�
        Call PrintReponse(DefData)
        
        '-> analyse des champs de l'index par num_Ordre
        strNonTri1 = ""
        For Each aIfield In aIname.iFields

            Select Case OrderTri
            
                Case "0" 'Fill_Label (Designation)
                
                    '-> Pointer sur l'objet Emilie associ�
                    Set aEmilieObj = ServeurDOG.Catalogue(Ident & "|" & aIfield.Num_Field)
                    aEmilieObj.CodeLangue = CodeLangue
                    
                    '-> Cr�er la chaine � trier
                    If strNonTri1 = "" Then
                        strNonTri1 = aEmilieObj.Fill_Label & "�" & aIfield.Num_Field
                    Else
                        strNonTri1 = strNonTri1 & "|" & aEmilieObj.Fill_Label & "�" & aIfield.Num_Field
                    End If
                
                Case "1", "2"  'Num_Field
                    If strNonTri1 = "" Then
                        strNonTri1 = aIfield.Num_Field & "�" & aIfield.Num_Field
                    Else
                        strNonTri1 = strNonTri1 & "|" & aIfield.Num_Field & "�" & aIfield.Num_Field
                    End If
                                
                Case "3" 'iOrdre
                    If strNonTri1 = "" Then
                        strNonTri1 = Format(aIfield.Ordre, "0000") & "�" & aIfield.Num_Field
                    Else
                        strNonTri1 = strNonTri1 & "|" & Format(aIfield.Ordre, "0000") & "�" & aIfield.Num_Field
                    End If
                    
            End Select
        Next
        strTri1 = Tri(strNonTri1, "|", "�")

        '-> Cr�er la ligne de d�fintion du champ
        For j = 1 To NumEntries(strTri1, "|")
        
            '-> Pointer sur le champ
            Set aIfield = aIname.iFields(Entry(j, strTri1, "|"))
            
            '-> Ligne de prop
            DefData = "B3D_SCHEMA_IFIELD~" & aFname.Num_Fname & "�" & aIname.Num_Iname & "�" & _
            Format(aIfield.Ordre, "0000") & "�" & aIfield.Num_Field & "�" & CInt(aIfield.Cumul) & "�" & _
            aIfield.TypeCondition & "�" & aIfield.ValMini & "�" & aIfield.ValMaxi
            
            '-> Imprimer la ligne de prorpi�t�
            Call PrintReponse(DefData)
            
            '-> Ajout dans la collection des champs s'il n'est pas d�ja r�f�renc�
            If Not IsField(aIfield.Num_Field, CatalogueBIS) Then
                '-> Le champ n'existe pas : l'ajouter dans la collection
                Set aObj1 = New Obj
                aObj1.Num = aIfield.Num_Field
                CatalogueBIS.Add aObj1, "NUM|" & aIfield.Num_Field
            End If
        
        Next 'Pour tous les champs
    Next 'Pour tous les index
    
End If 'Si pas d'index direct

'-> Ajout de la liste des interfaces associ�e � cette table
strNonTri = ""
For Each aInt In aFname.Interfaces
    If strNonTri = "" Then
        strNonTri = aInt.Name & "�" & aInt.Name
    Else
        strNonTri = strNonTri & "|" & aInt.Name & "�" & aInt.Name
    End If
Next

'-> Trier la liste des interfaces
strTri = Tri(strNonTri, "|", "�")

'-> Quitter si pas d'interface
If strTri = "" Then GoTo EndSub

'-> analyse de la liste des interfaces
For i = 1 To NumEntries(strTri, "|")
    '-> Pointer sur l'interface
    Set aInt = aFname.Interfaces("INT|" & Entry(i, strTri, "|"))
    '-> Code langue
    aInt.CodeLangue = aFname.CodeLangue
    '-> Print de la ligne
    DefData = "B3D_SCHEMA_INTNAME~" & aFname.Num_Fname & "�" & aInt.Name & "�" & aInt.Index & _
              "�" & aInt.Intput_Output & "�" & aInt.Tolerance & "�" & CInt(aInt.UseSeparateur) & "�" _
              & aInt.Separateur & "�" & CInt(aInt.UseIndex) & "�" & aInt.Designation
    Call PrintReponse(DefData)
    
    '-> Analyse de la liste des champs dans l'ordre des positions (kamasutra...JPL)
    strNonTri = ""
    
    For Each aIntField In aInt.Fields
        If strNonTri = "" Then
            strNonTri = Format(aIntField.Position, "0000") & "�" & aIntField.Num_Field
        Else
            strNonTri = strNonTri & "|" & Format(aIntField.Position, "0000") & "�" & aIntField.Num_Field
        End If
    Next
    
    '-> Trier la chaine
    strTri = Tri(strNonTri, "|", "�")
    
    If strTri = "" Then GoTo NextInt
    
    For j = 1 To NumEntries(strTri, "|")
    
        '-> Pointer sur le champ d'interface
        Set aIntField = aInt.Fields(Entry(j, strTri, "|"))
    
        '-> Cr�ation de la def d'un champ
        DefData = "B3D_SCHEMA_INTFIELD~" & aFname.Num_Fname & "�" & aInt.Name & "�" & aIntField.Num_Field & _
                  "�" & aIntField.Position & "�" & aIntField.Longueur & "�" & CInt(aIntField.IsModif) & _
                  "�" & aIntField.Defaut & "�" & aIntField.Table_Correspondance
        
        '-> Impression de la def du champ
        Call PrintReponse(DefData)
    Next 'Pour tous les champs de l'interface
                      
NextInt:
    
Next 'Pour tous les interfaces
EndSub:

'-> Quitter la fonction
Exit Sub

GestError:
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure IntegreFname : " & Err.Number & " - " & Err.Description)
    

End Sub


Private Function Format_Num(ByVal str As String, ByVal IdFormat) As String

'---> Cette fonction formatte un num�ro de champ, de table ou d'ordre avec des "0"

Dim i As Integer

If Not IsNumeric(str) Then
    Format_Num = str
    Exit Function
End If

'-> Convertir en entier
i = CInt(str)

'-> formatter la sortie
Select Case IdFormat

    Case 0 '-> Fname, num�ro d'ordre : sur 4
        Format_Num = Format(i, "0000")
    
    Case 1 '-> Num_Field, Num_Ifield , EmilieObj : sur 5
        Format_Num = Format(i, "00000")
    
    Case 2 '-> Iname : sur 3
        Format_Num = Format(i, "000")
        
End Select


End Function

Private Function IsIField(ByVal strFname As String, strIname As String, ByVal strField As String) As Boolean


'---> Cette fonction retourne si le champ sp�cifi� fait partie d'un index sur la table


Dim aFname As Object
Dim aIname As Object
Dim aIfield As Object
    
On Error GoTo GestError
    
'-> Pointer sur la table
Set aFname = ServeurDOG.fNames(Ident & "|" & strFname)

'-> Tester si la table est en acces direct
If aFname.IndexDirect <> "" Then
    IsIField = False
    Exit Function
End If

'-> Pointer sur l'index
Set aIname = aFname.Inames(strIname)

'-> analyser les champs
For Each aIfield In aIname.iFields
    If aIfield.Num_Field = strField Then
        IsIField = True
        Exit Function
    End If
Next

'-> Quitter la fonction
GestError:
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure IsField : " & Err.Number & " - " & Err.Description)

End Function




Private Function GetConditionValue(aCdt As Condition, aSel As clsSelectionSQL, Num_Id As Integer, aFname As Object) As String

'---> Cette fonction retourne sous forme de caractere une condition mathematique

Dim strTmp As String
Dim DefField As String
Dim DefZone As String

On Error GoTo GestError

'-> Tester si on est sur un champ d'index direct
If aFname.IndexDirect <> "" Then
    If aSel.Num_Field = aFname.IndexDirect Then
        DefField = "[ZON_DATA] "
    Else
        DefField = "[" & aSel.Num_Field & "] "
    End If
Else
    If Num_Id = 0 Then
        DefField = "[" & aSel.Num_Field & "] "
    Else
        DefField = "[ZON_INDEX_" & Num_Id & "] "
    End If
End If

Select Case aCdt.Operande

    Case 1 'EGAL
        strTmp = DefField & "='" & aCdt.cdt_Mini & "'"
                    
    Case 2 'INC
        strTmp = DefField & ">='" & aCdt.cdt_Mini & _
                 "' AND " & DefField & "<='" & aCdt.cdt_Maxi & "'"
    
    Case 3 'NIN
    
        strTmp = DefField & ">'" & aCdt.cdt_Maxi & _
                 "' AND " & DefField & "<'" & aCdt.cdt_Mini & "'"
    
    Case 4 'DIF
    
        strTmp = DefField & "<>'" & aCdt.cdt_Mini & "'"
        
    Case 5 'LE
        strTmp = DefField & "<='" & aCdt.cdt_Mini & "'"
        
    Case 6 'GE
        strTmp = DefField & ">='" & aCdt.cdt_Mini & "'"
        
    Case 7 'LT
        strTmp = DefField & "<'" & aCdt.cdt_Mini & "'"
        
    Case 8 'GT
        strTmp = DefField & ">'" & aCdt.cdt_Mini & "'"
    
    Case 9 'BEG
        strTmp = DefField & ">='" & aCdt.cdt_Mini & _
                 "' AND " & DefField & "<='" & aCdt.cdt_Mini & "Z'"
        
    Case 10 'MAT
        strTmp = ""
    
End Select
    
'-> Renvoyer la valeur
GetConditionValue = strTmp
    
'-> Quitter la proc�dure
Exit Function

GestError:
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure GetConditionValue : " & Err.Number & " - " & Err.Description)
    
End Function

Private Function GetOperande(ByVal Type_Condition As Integer) As String

Select Case Type_Condition
    Case 1, 2, 9
        GetOperande = " OR "
    Case 4
        GetOperande = " AND "
End Select

End Function

Private Function GetTriIndexField(ByVal aIname As Object) As String

'---> Cette proc�dure cr�er la liste des champs d'un index tri�e par num�ro d'ordre

Dim aIfield As Object
Dim strNonTri As String

On Error GoTo GestError

'-> Cr�er une chaine de tri
For Each aIfield In aIname.iFields
    If strNonTri = "" Then
        strNonTri = Format(aIfield.Ordre, "0000") & "�" & aIfield.Num_Field
    Else
        strNonTri = strNonTri & "|" & Format(aIfield.Ordre, "0000") & "�" & aIfield.Num_Field
    End If
Next

'-> Trier la chaine
GetTriIndexField = Tri(strNonTri, "|", "�")

'-> Quitter la fonction
Exit Function

GestError:
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure GetTriIndexField : " & Err.Number & " - " & Err.Description)
    '-> Returner la chane intiale
    GetTriIndexField = strNonTri

End Function



Private Function GetConditionProp(ByVal Index As Integer) As String

Select Case Index
    Case 1
        GetConditionProp = "EQ"
    Case 2
        GetConditionProp = "INC"
    Case 3
        GetConditionProp = "NIN"
    Case 4
        GetConditionProp = "DIF"
    Case 5
        GetConditionProp = "LE"
    Case 6
        GetConditionProp = "GE"
    Case 7
        GetConditionProp = "LT"
    Case 8
        GetConditionProp = "GT"
    Case 9
        GetConditionProp = "BEG"
    Case 10
        GetConditionProp = "MAT"
End Select


End Function

Private Sub Get_EmilieLink()

Dim aLinkFname As LinkFname
Dim aLinkIname As LinkIname
Dim aLinkInterface As LinkInterface
Dim TempoStr As String
Dim aEmilieObj As EmilieObj
Dim aFname As Fname
Dim aIname As Iname
Dim aInt As Interface
Dim strLibel As String

'-> Renvoyer une r�ponse de succ�s
Call PrintReponse("B3D_CATALOGUE_LINK~OK~")

'-> Analyse de l'ensemble des objets Emilie
For Each aEmilieObj In ServeurDOG.Catalogue
    '-> Affichage des liens de l'objet
    If aEmilieObj.fNames.Count <> 0 Then
        '-> Afficher les liens de l'objet
        For Each aLinkFname In aEmilieObj.fNames
            '-> R�cup�ration du libelle
            Set aFname = ServeurDOG.fNames(Ident & "|" & UCase$(aLinkFname.Num_Fname))
            aFname.CodeLangue = CodeLangue
            strLibel = "B3D_CATALOGUE_LINK~" & aEmilieObj.Num_Field & "�LINK_FNAME�" & aFname.Num_Fname & "�" & aFname.Designation
            '-> Imprimer la r�ponse
            Call PrintReponse(strLibel)
            '-> Tester si on doit ajouter le node des index
            If aLinkFname.LinkInames.Count <> 0 Then
                For Each aLinkIname In aLinkFname.LinkInames
                    '-> R�cup�ration du libelle
                    Set aIname = aFname.Inames(UCase$(aLinkIname.Num_Iname))
                    aIname.CodeLangue = CodeLangue
                    strLibel = "B3D_CATALOGUE_LINK~" & aEmilieObj.Num_Field & "�LINK_INAME�" & aIname.Num_Iname & "�" & aIname.Designation
                    '-> Imprimer l'objet
                    Call PrintReponse(strLibel)
                Next 'Pour tous les index r�f�renc�s
            End If 'Si on doit ajouter les index
            '-> Tester si on doit ajouter les interfaces
            If aLinkFname.LinkInterfaces.Count <> 0 Then
                For Each aLinkInterface In aLinkFname.LinkInterfaces
                    '-> r�cup�rer le libell�
                    Set aInt = aFname.Interfaces("INT|" & UCase$(aLinkInterface.Name))
                    aInt.CodeLangue = CodeLangue
                    strLibel = "B3D_CATALOGUE_LINK~" & aEmilieObj.Num_Field & "�LINK_INTERFACE�" & aInt.Name & "�" & aInt.Designation
                    '-> Imprimer la r�ponse
                    Call PrintReponse(strLibel)
                Next 'Pour tous les interfaces
            End If 'Si on doit ajouter les interfaces
        Next 'Pour tous les fnames associ�s
    End If 'Sil i y a des associations
Next 'Pour tous les objets du catalogue

'-> Quitter la fonction
Exit Sub

GestError:
    '-> Annuler les r�ponses pr�c�dentes
    ResetReponse
    '-> Imprimer une trace
    Call PrintDebug("B3D_CATALOGUE_LINK Erreur dans la proc�dure : " & Chr(13) & Chr(10) & Err.Number & " - " & Err.Description)
    '-> Renvoyer une valeur d'erreur
    Call PrintReponse("B3D_CATALOGUE_LINK~ERROR~" & Err.Number & "~" & Err.Description & "~")


End Sub


Private Sub B3D_SQL(Instruction As String)
'---> permet d'executer une requete SQL
'syntaxe : B3D_SQL~[MaRequete]

Dim Param As String

On Error GoTo GestError

'-> Recup de la requete
Param = Trim(Entry(2, Instruction, "~"))
If Param = "" Then Exit Sub

'-> Mode debug
Call PrintDebug("Procedure B3D_SQL : execution de la requete : " & Chr(13) & Chr(10) & Param)

'-> Execution de la requete
CreateEnreg Param
    
'-> Envoie de la reponse
Call PrintReponse("B3D_SQL~OK~")
    
Exit Sub

GestError:
    
    '-> G�rer les conflits de locks
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la Procedure B3D_SQL : " & Chr(13) & Chr(10) & Err.Number & " - " & Err.Description)
    
    '-> Renvoyer une erreur
    Call PrintReponse("B3D_SQL~ERROR~" & Err.Number & "~" & Err.Description & "~")
    
End Sub
