VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsBuffer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'************************************************************
'* Cette Classe est utilis�e pour g�rer un buffer de donn�e *
'************************************************************

Public Id_Fname As String 'Ident + table
Public BufferName As String 'Nom du buffer
Public BufferSizeValue 'Taille du buffer en enregistrement
Public PageSize As Long 'Taille d'une page en enregistrement
Public BufferSize As Long 'Nombre d'enreg dans le buffer
Public ReqSQL As String 'Requete SQL pour g�n�rer le recordset
Public ReqCdt As String 'Conditions � �valuer pour g�n�rer la requete SQL
Private aRec As Object 'Pointeur vers le recordset associ� � cette navigation
Public PageEnCours As Long 'Page en cours d'utilisation dans le buffer
Public Pages As Collection 'Collection des pages
Public NbPages As Long  'Nombre de page dans le buffer
Private EnregCount As Long

Private Sub Class_Initialize()

'-> Initialiser la collection des pages
Set Pages = New Collection

End Sub

Public Sub Initialise_Buffer(Optional Continu As Boolean)

Dim DefData As String
Dim DefField As String
Dim DefCondition As String
Dim DefValMini As String
Dim DefValMaxi As String
Dim i As Long
Dim NumEnregistrement As String

Dim aPage As clsPage
Dim aRowId As clsRowId

Dim ToCreate As Boolean
Dim PageCount As Long
Dim PageEnregCount As Long
Dim GetEnreg As Boolean

On Error GoTo GestError

If Not Continu Then
    '-> Cr�ation de la requette sur les champs d'index ou DEALB3D_DATA si champ index direct sur fname
    '-> Mode debug
    Call PrintDebug("Methode Initialise_Buffer : Mode continu = False : Cr�ation de la requete initiale : " & Chr(13) & Chr(10) & ReqSQL)
    '-> Ex�cuter la requete
    Set aRec = SetTable(ReqSQL)
    '-> Tester qu'il y a un retour
    If aRec.EOF Then
        '-> Imprimer une r�ponse n�gative
        Call PrintReponse("B3D_NAVIGA_SELECT~ERROR~90014~" & GetMesseprog("ERRORS", "90014") & "~")
        '-> Fermer le query
        aRec.Close
        '-> Quitter cette proc�dure
        Exit Sub
    End If 'S'il n'y a pas d'enregistrements
Else
    '-> Continuer le buffer
    Me.BufferSize = Me.BufferSize + Me.BufferSizeValue
End If
                    
'-> Indiquer que l'on doit cr�er une page
ToCreate = True
            
'-> Analyse du recordset
Do While Not aRec.EOF

    '-> on doit cr�er une page
    If ToCreate Then
        '-> Nouvelle instance de classe
        Set aPage = New clsPage
        '-> Incr�menter le compteur de page
        Me.NbPages = Me.NbPages + 1
        '-> Ajouter dans la collection des pages
        Me.Pages.Add aPage, "PAGE|" & Me.NbPages
        '-> Indiquer que la page est cr��e
        ToCreate = False
        '-> Remettre � 0 le compteur de RowId par Page
        PageEnregCount = 0
    End If

    '-> R�cup�ration du Row ID
    NumEnregistrement = Trim(aRec.Fields("NUM_ENREG"))
    
    '-> On a trouv� les enregistrements dans l'index : v�rifier s'il y a des v�rifications � faire dans les champs
    If Trim(ReqCdt) <> "" Then
        '-> Si 3 ou 4 entr�es
        For i = 1 To NumEntries(ReqCdt, "�")
            '-> R�cup�rer une condition
            DefData = Entry(i, ReqCdt, "�")
            DefField = Entry(1, DefData, "�")
            DefCondition = Entry(2, DefData, "�")
            '-> PIERROT : ON PASSE LES VALEURS A COMPARER EN MAJUSCULES
            DefValMini = UCase$(Entry(3, DefData, "�"))
            If NumEntries(DefData, "�") = 4 Then
                DefValMaxi = UCase$(Entry(4, DefData, "�"))
            Else
                DefValMaxi = UCase$(DefValMini)
            End If
            
            '-> Tester les conditions d'exclusions
            If TestCondition(Id_Fname, DefField, DefCondition, DefValMini, DefValMaxi, NumEnregistrement) Then
                '-> On garde l'enregistrement
                GetEnreg = True
            Else
                '-> Ne pas garder cet enregistrement
                GetEnreg = False
            End If
        Next 'Pour toutes les conditions des champs externes
    Else
        '-> On garde l'enregistrement
        GetEnreg = True
    End If 'Sil y a des vecteur data
    
    '-> Ajouter le RowId ou non
    If GetEnreg Then
        '-> Ajouter l'enregistrement dans la page en cours
        Set aRowId = New clsRowId
        aRowId.ROWID = NumEnregistrement
        aPage.RowIds.Add aRowId, "ROWID|" & aRowId.ROWID
        '-> Incr�menter le nombre d'enreg dans le buffer
        EnregCount = EnregCount + 1
        '-> Si on a atteint le nombre d'enregistrement dans le buffer -> Quitter sauf si buffer size = 0
        If EnregCount + 1 > Me.BufferSize And Me.BufferSize <> 0 Then Exit Do
        '-> Incr�menter le compteur d'enreg dans la page
        PageEnregCount = PageEnregCount + 1
        '-> Si on a d�passer le nombre d'enreg dans la page -> Cr�er une nouvelle page
        If PageEnregCount + 1 > Me.PageSize Then ToCreate = True
    End If 'Si on ajoute le RowId dans le buffer
    
    '-> Passer � l'enreg suivant
    aRec.MoveNext
Loop 'Pour le nombre d'entr�e
                        
'-> Renvoyer la clause SELECT
Call PrintReponse("B3D_NAVIGA_SELECT~OK~" & Me.NbPages & "�" & EnregCount)
                        
'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> G�rer les conflits de locks
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Mode debug
    Call PrintDebug("Proc�dure Initialise_Buffer : Erreur dans la proc�dure : " & Err.Number & " - " & Err.Description)
    
    '-> Reset de la r�ponse en cours
    Call ResetReponse
    
    '-> Retourner une valeur d'erreur
    Call PrintReponse("B3D_NAVIGA~ERROR~" & Err.Number & "~" & Err.Description & "~")
                        
End Sub

Public Sub FirstPage(strField As String)
'-> Renvoyer la page 1
ReturnPage 1, strField
End Sub

Public Sub PrevPage(strField As String)
'-> Renvoyer la page - 1
If Me.PageEnCours = 1 Then
    ReturnPage 1, strField
Else
    ReturnPage Me.PageEnCours - 1, strField
End If

End Sub

Public Sub NextPage(strField As String)
'-> Renvoyer la page + 1
If Me.PageEnCours = Me.NbPages Then
    ReturnPage Me.NbPages, strField
Else
    ReturnPage Me.PageEnCours + 1, strField
End If
End Sub

Public Sub LastPage(strField As String)
ReturnPage Me.NbPages, strField
End Sub


Public Sub ReturnPage(Page As Long, strField As String)

Dim aPage As clsPage
Dim aRow As clsRowId
Dim strRetour As String

On Error GoTo GestError

'-> Pointer sur la page
Set aPage = Me.Pages("PAGE|" & Page)

'-> Renvoyer la navigation
Call PrintReponse("B3D_NAVIGA~OK~" & Page)

'-> Analyser la page
For Each aRow In aPage.RowIds
    If Trim(strField) = "" Then
        strRetour = aRow.ROWID & "~ERROR~~~"
    Else
        '-> Recup�rer les data de cet enregistrement si on a demand� des champs
        strRetour = B3D_Get_Data_Val(Me.Id_Fname, aRow.ROWID, 0, strField, False)
    End If
    '-> Imprimmer la r�ponse
    Call PrintReponse(strRetour)
Next

'-> Positionner le pointeur
Me.PageEnCours = Page

'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> G�rer les conflits de locks
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Mode debug
    Call PrintDebug("Proc�dure Initialise_Buffer : Erreur dans la proc�dure : " & Err.Number & " - " & Err.Description)
    
    '-> Reset de la r�ponse en cours
    Call ResetReponse
    
    '-> Retourner une valeur d'erreur
    Call PrintReponse("B3D_NAVIGA~ERROR~" & Err.Number & "~" & Err.Description)

End Sub

