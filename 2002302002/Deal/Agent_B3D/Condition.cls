VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Condition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public cdt_Mini As String 'Conditin mini
Public cdt_Maxi As String 'Condition maxi
Public Operande As Integer 'Op�rande � utiliser
Public Key As String 'Cl� de stockage de la condition


'---> Les conditions sont num�rot�es de la mani�re suivante

'1 --->  "Egal"
'2 --->  "Compris entre"
'3 --->  "Non compris entre"
'4 --->  "Diff�rent"
'5 --->  "Inf�rieur ou �gal"
'6--->  "Sup�rieur ou �gal"
'7 --->  "Inf�rieur"
'8 --->  "Sup�rieur"
'9 --->  "Commence par"
'10 --->  "Contient"

