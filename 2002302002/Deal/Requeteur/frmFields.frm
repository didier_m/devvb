VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmFields 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Choix des champs et propri�t�s"
   ClientHeight    =   8235
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6165
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8235
   ScaleWidth      =   6165
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "Lancer la requ�te"
      Height          =   255
      Left            =   0
      TabIndex        =   6
      Top             =   7920
      Width           =   6135
   End
   Begin VB.Frame Frame2 
      Caption         =   "Visualisation de la requ�te"
      ForeColor       =   &H00800000&
      Height          =   2175
      Left            =   0
      TabIndex        =   4
      Top             =   5640
      Width           =   6135
      Begin MSComctlLib.ListView ListView1 
         Height          =   1815
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   5895
         _ExtentX        =   10398
         _ExtentY        =   3201
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin VB.Frame Frame1 
      Height          =   5535
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6135
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   5160
         Picture         =   "frmFields.frx":0000
         ScaleHeight     =   240
         ScaleWidth      =   240
         TabIndex        =   3
         Top             =   2040
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   4080
         Picture         =   "frmFields.frx":0342
         ScaleHeight     =   240
         ScaleWidth      =   240
         TabIndex        =   2
         Top             =   2160
         Visible         =   0   'False
         Width           =   240
      End
      Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
         Height          =   5295
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   5895
         _ExtentX        =   10398
         _ExtentY        =   9340
         _Version        =   393216
         Rows            =   1
         Cols            =   5
         FixedCols       =   0
      End
   End
End
Attribute VB_Name = "frmFields"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



Private Sub Command1_Click()
'---> On lance la requete
Dim OrdreOk As Boolean
Dim aBuffer As B3DBuffer
Dim Ligne As String
Dim i As Integer
Dim aField As B3DField
Dim j As Integer


frmWait.Label1 = "Envoi de la requete..."
frmWait.Show

'-> On defini l'ordre
Ligne = "B3D_BUILD_REQ~" & ReqFName & "�" & ReqIName & "�" & ReqCumul

'-> Initialisation pour demande
If Not aConnexion.InitDmd(True) Then
    '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
    MsgBox "Impossible d'initialiser une demande..." & Chr(13) & "Fin du traitement.", vbExclamation
    Exit Sub
End If

'-> On envoi l'ordre dans le fichier d'ordres
OrdreOk = aConnexion.PutInstruction(Ligne, True)
    
'-> Verifie si on a bien envoy� l'instruction
If Not OrdreOk Then
    '-> Positionner le code Erreur
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    Exit Sub
End If

Me.MSFlexGrid1.Col = 3

'-> parcours des champs sur lesquels on a des conditions
For i = 1 To Me.MSFlexGrid1.Rows - 1
    '-> Si on a des cdt
    If Me.MSFlexGrid1.CellPicture = Me.Picture2.Picture Then
        
       

        Ligne = "B3D_BUILD_SEL~" & Trim(UCase$(Me.MSFlexGrid1.TextMatrix(i, 1))) & "�0�0�0�0�0"
        '-> On envoi l'ordre dans le fichier d'ordres
        OrdreOk = aConnexion.PutInstruction(Ligne, True)
            
        '-> Verifie si on a bien envoy� l'instruction
        If Not OrdreOk Then
            '-> Positionner le code Erreur
            MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
        End If
        
        '->Recup du field pour envoi de la liste des cdt
        Set aField = aDescro.Fields(Trim(UCase$(Me.MSFlexGrid1.TextMatrix(i, 1))))
        
        '-> Analyse de la liste des cdt
        If aField.ZonLock <> "" Then
            For j = 1 To NumEntries(aField.ZonLock, "�")
                Ligne = "B3D_BUILD_CDT~" & aField.Field & "�" & Trim(Entry(1, Entry(1, Entry(j, aField.ZonLock, "�"), "�"), "-")) & "�"
                Ligne = Ligne & Entry(2, Entry(j, aField.ZonLock, "�"), "�") & "�"
                Ligne = Ligne & Entry(3, Entry(j, aField.ZonLock, "�"), "�")
       
                '-> Envoi de l'ordre dans le fichier d'instructions B3D
                OrdreOk = aConnexion.PutInstruction(Ligne, True)
                
                '-> Verifie si on a bien envoy� l'instruction
                If Not OrdreOk Then
                    '-> Positionner le code Erreur
                    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
                End If
            Next
        End If
        
    End If
Next
    
'-> Dernier ordre pour creer la requete
Ligne = "B3D_BUILD_SQL~���"


'-> Envoi de l'ordre dans le fichier d'instructions B3D
OrdreOk = aConnexion.PutInstruction(Ligne, True)

'-> Verifie si on a bien envoy� l'instruction
If Not OrdreOk Then
    '-> Positionner le code Erreur
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
End If

'-> On envoie l'ordre de lecture des instructions
Set aBuffer = New B3DBuffer
If Not aConnexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    aConnexion.ErrorNumber = "100000"
    aConnexion.ErrorLibel = "Error sending exchange"
    aConnexion.ErrorParam = "LoadAgentDOG"
    End
End If

'-> Recup de la 1ere ligne
Ligne = aBuffer.Lignes(1)

'If Entry(2, Ligne, "~") <> "OK" Then
'    MsgBox "Erreur dans l'envoi de l'ordre B3D...", vbExclamation
'    Exit Sub
'End If

For i = 1 To aBuffer.Lignes.Count
    
    MsgBox i & Chr(13) & aBuffer.Lignes(i)

Next


Unload frmWait

End Sub

Private Sub Form_Load()
'-> prechargement et formattage du flex

'-> Entetes de colonnes
Me.MSFlexGrid1.TextMatrix(0, 0) = "Ch. Index"
Me.MSFlexGrid1.TextMatrix(0, 1) = "No Field"
Me.MSFlexGrid1.TextMatrix(0, 2) = "D�signation"
Me.MSFlexGrid1.TextMatrix(0, 3) = "Afficher ?"
Me.MSFlexGrid1.TextMatrix(0, 4) = "Conditions"

'-> On formatte la largeur des colonnes
Me.MSFlexGrid1.ColWidth(0) = 500
Me.MSFlexGrid1.ColWidth(1) = 800
Me.MSFlexGrid1.ColWidth(2) = 3200
Me.MSFlexGrid1.ColWidth(3) = 500
Me.MSFlexGrid1.ColWidth(4) = 500

End Sub

Private Sub MSFlexGrid1_Click()
'-> on decie d'afficher le champ ou de lui affecter des conditions
Dim aField As B3DField
Dim Conditions As String
Dim i As Integer

'-> On ne change l'image que si click sur colonne 2
If ClickX = 3 Or ClickX = 4 Then
    '-> On change d'image selon que la case est selectionn�e ou pas
    
    If ClickX = 3 Then
        If Me.MSFlexGrid1.CellPicture = Me.Picture1.Picture Then
            Set Me.MSFlexGrid1.CellPicture = Me.Picture2.Picture
            '-> On ajoute le champs dans le listview des champs a afficher dans la requete
        Me.ListView1.ColumnHeaders.Add , "FIELD|" & Me.MSFlexGrid1.TextMatrix(Me.MSFlexGrid1.Row, 1), Me.MSFlexGrid1.TextMatrix(Me.MSFlexGrid1.Row, 2)
        Else
            Set Me.MSFlexGrid1.CellPicture = Me.Picture1.Picture
            '-> On ajoute le champs dans le listview des champs a afficher dans la requete
        Me.ListView1.ColumnHeaders.Remove "FIELD|" & Me.MSFlexGrid1.TextMatrix(Me.MSFlexGrid1.Row, 1)
        End If
        
    End If
    
    If ClickX = 4 Then
        If Me.MSFlexGrid1.CellPicture = Me.Picture1.Picture Then
            Set Me.MSFlexGrid1.CellPicture = Me.Picture2.Picture
        End If
        '-> teste si le champ a deja des conditions et les charge dnas le fenetre de conditions
        Set aField = aDescro.Fields(Me.MSFlexGrid1.TextMatrix(Me.MSFlexGrid1.Row, 1))
        If aField.ZonLock <> "" Then
            For i = 1 To NumEntries(aField.ZonLock, "�")
                Conditions = aField.ZonLock
                frmConditions.MSFlexGrid1.TextMatrix(i, 0) = Entry(1, Conditions, "�")
                frmConditions.MSFlexGrid1.TextMatrix(i, 1) = Entry(2, Conditions, "�")
                frmConditions.MSFlexGrid1.TextMatrix(i, 2) = Entry(3, Conditions, "�")
            Next
        End If
        
        frmConditions.NoField = Me.MSFlexGrid1.TextMatrix(Me.MSFlexGrid1.Row, 1)
        frmConditions.Command1.Caption = "Valider conditions : " & Me.MSFlexGrid1.TextMatrix(Me.MSFlexGrid1.Row, 1) & " - " & Me.MSFlexGrid1.TextMatrix(Me.MSFlexGrid1.Row, 2)
        frmConditions.Show vbModal
    End If
    
End If

End Sub


Private Sub MSFlexGrid1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'-> Get de la colonne et de la ligne
ClickX = GetCol(Me.MSFlexGrid1, x)
ClickY = GetRow(Me.MSFlexGrid1, y)
End Sub


