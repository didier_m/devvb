VERSION 5.00
Begin VB.Form frmVisuParam 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7140
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9615
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7140
   ScaleWidth      =   9615
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frame1 
      Height          =   7095
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9615
      Begin VB.TextBox Text6 
         Height          =   615
         Left            =   120
         TabIndex        =   13
         Top             =   3120
         Width           =   9375
      End
      Begin VB.TextBox txtComment 
         Height          =   285
         Left            =   1560
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   720
         Width           =   7935
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   7680
         Locked          =   -1  'True
         TabIndex        =   10
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox Text5 
         Height          =   1095
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   8
         Top             =   5880
         Width           =   9375
      End
      Begin VB.TextBox Text4 
         Height          =   1335
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   6
         Top             =   4080
         Width           =   9375
      End
      Begin VB.TextBox Text3 
         Height          =   1335
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   4
         Top             =   1320
         Width           =   9375
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   1560
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label Label7 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   2880
         Width           =   3135
      End
      Begin VB.Label Label6 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label Label5 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   5520
         Width           =   2295
      End
      Begin VB.Label Label4 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   3840
         Width           =   3135
      End
      Begin VB.Label Label3 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   1080
         Width           =   3135
      End
      Begin VB.Label Label2 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6600
         TabIndex        =   3
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label1 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   1455
      End
   End
End
Attribute VB_Name = "frmVisuParam"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub Form_Load()
Me.Caption = GetMesseprog("REQUETEUR", "56")
Me.Label1.Caption = GetMesseprog("REQUETEUR", "57")
Me.Label2.Caption = GetMesseprog("REQUETEUR", "58")
Me.Label6.Caption = GetMesseprog("REQUETEUR", "55")
Me.Label3.Caption = GetMesseprog("REQUETEUR", "59")
Me.Label4.Caption = GetMesseprog("REQUETEUR", "60")
Me.Label5.Caption = GetMesseprog("REQUETEUR", "61")
Me.Label7.Caption = GetMesseprog("REQUETEUR", "66")


End Sub
