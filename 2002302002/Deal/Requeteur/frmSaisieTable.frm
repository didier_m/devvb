VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmSaisieTable 
   Caption         =   "Form1"
   ClientHeight    =   6990
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6390
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   466
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   426
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Height          =   375
      Left            =   4560
      Picture         =   "frmSaisieTable.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   6120
      Width           =   855
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   5520
      Picture         =   "frmSaisieTable.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   6120
      Width           =   855
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   6015
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   10610
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "POPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuDelValue 
         Caption         =   "Delete Value"
      End
   End
End
Attribute VB_Name = "frmSaisieTable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public aEnreg As B3DEnreg
Public aDescro As B3DFnameDescriptif

Private Sub Command1_Click()

Dim ListeField As String

On Error GoTo GestError

Dim aObj As B3DObject

'-> Bloquer l'�cran
Screen.MousePointer = 11
Me.Enabled = False

'-> Effectuer les controles
If Me.Command1.Tag = "CREATE" Then
    '-> Pour une table en acc�s direct, v�rifier que le champ sp�cifi� soit rens�igfn�
    If Me.aDescro.IndexDirect <> "" Then
        '-> V�rifier que le champ sp�cifi� soit saisi
        If Trim(Me.aEnreg.Fields(Me.aDescro.IndexDirect).ZonLock) = "" Then
            '-> Pointer sur le champ
            Set aObj = Catalogues(Me.aDescro.IndexDirect)
            '-> Afficher un message d'erreur
            MsgBox "Saisie du champ : " & aObj.Fill_Label & " obligatoire", vbExclamation + vbOKOnly, "Erreur"
            '-> Quitter la proc�dure
            GoTo GestError
        End If 'V�rification de la cl� primaire sur table � index directe
    End If 'Si on est sur une table en acc�s direct
    
    '-> Faire un create sur l'enreg
    If Not CreateEnreg(aConnexion, MasterFname, Me.aEnreg) Then
        '-> Tester  le code erreur 90026 pas de modif
        If aConnexion.ErrorNumber = "90026" Then
            '-> Simmuler un annuler
            Command2_Click
            '-> quitter
            Exit Sub
        End If
        '-> Afficher une message d'erreur
        MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
        '-> Fin du programme
        GoTo GestError
    End If
Else '-> On est en mode update
    If Not UpdateEnreg(aConnexion, MasterFname, Me.aEnreg, False) Then
        '-> Tester  le code erreur 90026 pas de modif
        If aConnexion.ErrorNumber = "90026" Then
            '-> Simmuler un annuler
            Command2_Click
            '-> quitter
            Exit Sub
        End If
        '-> Afficher une message d'erreur
        MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
        '-> Fin du programme
        GoTo GestError
    End If
End If

'-> Mettre � jour l'affichage
DisplayEnregInListView Me.aEnreg, frmSaiTable.ListView1, aDescro

'-> Formatter le list view
FormatListView frmSaisieTable.ListView1

'-> D�charger la feuille
Unload Me

'-> Repositionner le pointeur
Screen.MousePointer = 0

'-> Quitter la sub
Exit Sub

GestError:
    '-> D�bloquer
    Me.Enabled = True
    Screen.MousePointer = 0
    Exit Sub
    
End Sub

Private Sub Command2_Click()

'-> Bloquer la feuille
Me.Enabled = False
Screen.MousePointer = 11

'-> Si on est en mode update : supprimer le lock
If Me.Command1.Tag = "UPDATE" Then
    '-> Envoyer un ordre Unlock
    UnlockEnreg aConnexion, MasterFname, Me.aEnreg.NumEnreg
End If

'-> D�charger la feuille
Unload Me

'-> D�bloquer le pointeur de l'�cran
Screen.MousePointer = 0

End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next

'-> Get de la zone cliente
GetClientRect Me.hwnd, aRect

'-> Redimension du browse
Me.ListView1.Left = 0
Me.ListView1.Top = 0
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom - 38
Me.Command1.Top = aRect.Bottom - 31
Me.Command1.Left = aRect.Right - 58
Me.Command2.Top = Me.Command1.Top
Me.Command2.Left = aRect.Right - 120


End Sub

Public Sub Init()

'---> Cr�ation de la liste des champs en saisie

Dim x As ListItem
Dim aField As B3DField
Dim aFieldJoin As B3DField
Dim temp As String

'-> Cr�er 2 Colonnes
Me.ListView1.ColumnHeaders.Add , , "Field"
Me.ListView1.ColumnHeaders.Add , , "Value"
Me.ListView1.ColumnHeaders.Add , , "Join Designation"

'-> Pour tous les champs
For Each aField In aEnreg.Fields
    temp = ""
    '-> Cr�er un nouvel Item
    Set x = Me.ListView1.ListItems.Add(, "FIELD|" & aField.Field)
    x.Text = aField.Libel
    '-> Charger sa valeur
    x.SubItems(1) = aField.ZonData
    '-> Gestion des jointures
    For Each aFieldJoin In aField.FieldJoins
        If temp = "" Then
            temp = aFieldJoin.ZonData
        Else
            temp = temp & " - " & aFieldJoin.ZonData
        End If
    Next 'Pour tous les champs de jointure
    If temp <> "" Then x.SubItems(2) = temp
Next 'Pour tous les champs

'-> Donner son nom � la table
Me.Caption = aDescro.Fname & " - " & aDescro.Designation

'-> Formatter le browse
FormatListView Me.ListView1

End Sub

Private Sub ListView1_DblClick()

'---> Modification

Dim aObj As B3DObject
Dim aField As B3DField
Dim aFieldJoin As B3DField
Dim NewField As B3DField
Dim aBuffer As B3DBuffer
Dim NewEnreg As B3DEnreg
Dim temp As String

'-> Ne Rien faire si pas d'�lement s�lectionn�
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Pointer sur le champ du catalogue
Set aObj = Catalogues(Entry(2, Me.ListView1.SelectedItem.Key, "|"))

'-> Pointer sur le champ de l'enregistrement
Set aField = aEnreg.Fields(Entry(2, Me.ListView1.SelectedItem.Key, "|"))

'-> Tester si on est sur une table � index direct et sur le champ d'inex direct
If aDescro.IndexDirect <> "" And aField.Field = aDescro.IndexDirect And Me.Command1.Tag = "UPDATE" Then Exit Sub

'-> Si le champ est en acc�s jointure
If aObj.Table_Correspondance <> "" And aObj.Table_Correspondance <> MasterFname Then
    
    '-> Initialiser un buffer
    Set aBuffer = New B3DBuffer
    
    '-> Effectuer le FieldTable
    If Not FieldTable(aConnexion, aBuffer, aConnexion.Ident, aField.Field) Then
        MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
        Exit Sub
    End If
    
    '-> Afficher la table de correspondance
    ShowFieldTableBrowse aBuffer, aField.Field & " - " & aField.Libel

    '-> Ne rien faire si Annuler
    If strRetour = "" Then Exit Sub
    
    '-> Pointer sur l'enreg sp�cifi�
    Set NewEnreg = aBuffer.Enregs(strRetour)
    
    '-> Concatainer la valeur de d�signation jointure
    For Each NewField In NewEnreg.Fields
        If NewField.Field <> aField.Field Then
            '-> Cr�er la chaine de concataination en affichage
            If temp = "" Then
                temp = NewField.ZonData
            Else
                temp = temp & " - " & NewField.ZonData
            End If
            '-> Pointer sur le champ de jointure
            Set aFieldJoin = aField.FieldJoins(NewField.Field)
            '-> Mettre a jour le champ de jointure
            aFieldJoin.ZonLock = NewField.ZonData
        Else
            '-> Mettre � jour le champ
            aField.ZonLock = NewField.ZonData
            '-> Mettre � jour l'affichage
            Me.ListView1.SelectedItem.SubItems(1) = NewField.ZonData
        End If
    Next 'Pour tous les champs en recherche
    
    '-> Mettre � jour la d�signation
    Me.ListView1.SelectedItem.SubItems(2) = temp
Else
    '-> Faire une saisie directe
    strRetour = InputBox(aObj.Fill_Label & " : ", "Input value", aField.ZonLock)
    '-> Ne rien faire
    If Trim(strRetour) = "" Then Exit Sub
    '-> Tester le retour de la zone par rapport � son descriptif THIERRY
    '-> Mettre � jour le champ
    aField.ZonLock = strRetour
    '-> Mettre � jour la valeur
    Me.ListView1.SelectedItem.SubItems(1) = strRetour
End If

'-> Vider la variable de retour
strRetour = ""

'-> Formatter le browse
FormatListView Me.ListView1

End Sub

Private Sub ListView1_KeyDown(KeyCode As Integer, Shift As Integer)

If KeyCode = 46 Then DelValue

End Sub

Private Sub ListView1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then ListView1_DblClick

End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

'-> Ne Rien faire si pas d'�lement s�lectionn�
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Ne traiter que le bouton droit
If Button <> vbRightButton Then Exit Sub

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu mnuPopup

'-> Quitter
If strRetour = "" Then Exit Sub

'-> Lancer un delete
DelValue

End Sub
Private Sub DelValue()

Dim aField As B3DField

'-> Pointer sur le champ
Set aField = aEnreg.Fields(Entry(2, Me.ListView1.SelectedItem.Key, "|"))

'-> Vider la valeur
aField.ZonLock = ""

'-> Mettre � jour l'affichage
Me.ListView1.SelectedItem.SubItems(1) = ""
Me.ListView1.SelectedItem.SubItems(2) = ""

'-> Mettre � jour la variable d'�change
strRetour = ""

End Sub

Private Sub mnuDelValue_Click()
strRetour = "DEL"
End Sub
