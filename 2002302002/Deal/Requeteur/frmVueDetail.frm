VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmVueDetail 
   ClientHeight    =   9465
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   16380
   LinkTopic       =   "Form1"
   ScaleHeight     =   631
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1092
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin MSComctlLib.ListView ListView1 
      Height          =   9015
      Left            =   0
      TabIndex        =   0
      Top             =   360
      Width           =   16335
      _ExtentX        =   28813
      _ExtentY        =   15901
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   960
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVueDetail.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVueDetail.frx":059A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVueDetail.frx":0B34
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVueDetail.frx":10CE
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVueDetail.frx":1668
            Key             =   "PROP"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVueDetail.frx":17C2
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Height          =   330
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   582
      ButtonWidth     =   609
      ButtonHeight    =   582
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FIRST"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PREV"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "NEXT"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "LAST"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PAGE"
            ImageIndex      =   5
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuPopUp 
      Caption         =   "POPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuAjout 
         Caption         =   "Ajouter"
      End
      Begin VB.Menu mnuModifier 
         Caption         =   "Modifier"
      End
      Begin VB.Menu mnuSep0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSupprimer 
         Caption         =   "Supprimer"
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuZoom 
         Caption         =   "Zoom"
      End
      Begin VB.Menu mnuSep4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExcel 
         Caption         =   "Export vers EXCEL"
      End
   End
   Begin VB.Menu mnuPopUpCum 
      Caption         =   "POPUPCUM"
      Visible         =   0   'False
      Begin VB.Menu mnuVisuDetail 
         Caption         =   "Visu D�tail"
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuZoomCum 
         Caption         =   "Zoom"
      End
      Begin VB.Menu mnuSep5 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExportXLCum 
         Caption         =   "Export Excel"
      End
   End
End
Attribute VB_Name = "frmVueDetail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Indique si requete de cumul
Public Cumul As Boolean
'-> requete de cumul
Public strReqDet As String
Public ListeChamps As String



Private Sub Form_Load()
Me.Caption = GetMesseprog("REQUETEUR", "62")
End Sub

Private Sub Form_Resize()
'-> resize de la zone de visu
Dim aRect As RECT

'-> recup de la zone  cliente de la fenetre
GetClientRect Me.hwnd, aRect

'-> Resize du list view
Me.ListView1.Top = Toolbar1.Height
Me.ListView1.Height = aRect.Bottom - Me.Toolbar1.Height
Me.ListView1.Width = aRect.Right

End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
'-> Permet d'activer le popup de gestion des enregs

On Error GoTo GestError

'-> teste si click droit
If Button <> 2 Then Exit Sub

strRetour = ""

'-> Affiche le popup
If Me.Cumul Then
    Me.PopupMenu Me.mnuPopUpCum
Else
    Me.PopupMenu Me.mnuPopUp
End If

'-> selon le choix dans le popup
Select Case strRetour
    
    Case "XL"
        '-> PIERROT : Penser a preciser les repertoires de fichiers XLS
        Me.Enabled = False
        Screen.MousePointer = 11
        '-> Export vers excel
        frmZoom.ExportListViewInXL Me.ListView1
        Screen.MousePointer = 0
        
    Case "DETAIL"
        '-> Permet de lancer le detail d'un cumul
        LoadDetailCumul

    Case "CREATE"

    Case "UPDATE"

    Case "DELETE"

    Case "ZOOM"
        '---> Envoi du zoom de l'enregistrement en cours
        If Cumul = True Then
            DisplayZoom aConnexion, ReqFName & "�" & ReqIName, Entry(2, Me.ListView1.SelectedItem.Key, "|"), True, False, aDescro, Me, True
        Else
            DisplayZoom aConnexion, ReqFName, Entry(2, Me.ListView1.SelectedItem.Key, "|"), True, False, aDescro, Me, True
        End If
        
End Select

'-> remise a blanc de la variable d'echange
strRetour = ""

GestError:
    Screen.MousePointer = 0
    Me.Enabled = True
    
End Sub

Private Sub mnuAjout_Click()
strRetour = "CREATE"
End Sub

Private Sub mnuExcel_Click()
'---> On envoie la visu de la requet dans excel
strRetour = "XL"
End Sub

Private Sub mnuExportXLCum_Click()
strRetour = "XL"
End Sub

Private Sub mnuModifier_Click()
strRetour = "UPDATE"
End Sub

Private Sub mnuSupprimer_Click()
strRetour = "DELETE"
End Sub

Private Sub mnuVisuDetail_Click()
strRetour = "DETAIL"
End Sub

Private Sub mnuZoom_Click()
strRetour = "ZOOM"
End Sub

Private Sub mnuZoomCum_Click()
strRetour = "ZOOM"
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim NavigaOk As Boolean

On Error GoTo GestError

'-> Gestion de l'�cran
Screen.MousePointer = 11
Me.Enabled = False

Select Case Button.Key
    Case "FIRST"
        NavigaOk = Naviga.NavigaFirst(aConnexion, Me.ListView1)
    Case "PREV"
        NavigaOk = Naviga.NavigaPrev(aConnexion, Me.ListView1)
    Case "NEXT"
        NavigaOk = Naviga.NavigaNext(aConnexion, Me.ListView1)
    Case "LAST"
        NavigaOk = Naviga.NavigaLast(aConnexion, Me.ListView1)
    Case "PAGE"
        NavigationContinue
        
End Select

GestError:

    Screen.MousePointer = 0
    Me.Enabled = True
    
End Sub


Private Sub NavigationContinue()
'---> Faire un naviga continu
Dim PageCount As Long
Dim i As Long

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> R�cup�rer le nombre de pages
PageCount = Naviga.PageCount

'-> Faire un B3D Continu
If Not Naviga.NavigaContinu(aConnexion) Then GoTo ErrorNaviga

'-> Ne rien faire si le nombre de page est le m�me
If Naviga.PageCount = PageCount Then GoTo ReleaseScreen

'-> Ajouter autant de page
For i = PageCount + 1 To Naviga.PageCount
    Me.ListView1.ListItems.Add , "PAGE|" & i, "Page " & i, "PAGE"
Next

'-> Lib�rer l'�cran
GoTo ReleaseScreen

ErrorNaviga:
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber, vbExclamation + vbOKOnly, "Error"
ReleaseScreen:

    '-> D�bloquer l'�cran
    Me.Enabled = True
    Screen.MousePointer = 0

End Sub

Private Sub listview1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    '-> Trier sur les entetes de colonne
    Me.ListView1.SortKey = ColumnHeader.Index - 1
    If Me.ListView1.SortOrder = lvwAscending Then
        Me.ListView1.SortOrder = lvwDescending
    Else
        Me.ListView1.SortOrder = lvwAscending
    End If
    Me.ListView1.Sorted = True
End Sub

Private Sub LoadDetailCumul()
'---> Permet de charger le detail d'un cumul
Dim aFrm As frmVueDetail
Dim aEnreg As B3DEnreg
Dim aField As B3DField
Dim Requete As String
Dim aObj As B3DObject
Dim aCol As ColumnHeader

On Error GoTo GestError

Screen.MousePointer = 11

'-> On declare une nouvelle fenetre de visu
Set aFrm = New frmVueDetail
aFrm.Cumul = False


'-> On cherche les data de cumul
Set aEnreg = FindData(aConnexion, aConnexion.Ident, ReqFName & "�" & ReqIName, Entry(2, Me.ListView1.SelectedItem.Key, "|"), False, False, aDescro, False)
Requete = Me.strReqDet

'-> On cree la requete qui alimente le detail de cumul
For Each aField In aEnreg.Fields
    '-> Cherche le champ dans la requete de cumul
    If InStr(1, Requete, "#" & Trim(UCase$(aField.Field)) & "#") <> 0 Then
        '-> le remplace par sa valeur
        Requete = Replace(Requete, "#" & aField.Field & "#", Trim(aField.ZonData))
    End If
Next

'-> On teste si premiere colonne est un champ decimal _
    en effet on ne peut aligner le premiere colonne d'un listview a droite
Set aObj = Catalogues(Entry(1, Entry(1, Me.ListeChamps, "�"), "�"))

If Trim(UCase$(aObj.DataType)) = "DECIMAL" Then
    '-> On rajoute alors une colonne
    Set aCol = aFrm.ListView1.ColumnHeaders.Add(, "INV")
    '-> On la rend invisible
    'aCol.Width = 0
End If

'-> On charge la nouvelle requete dans une navigation
Set Naviga = InitNaviga(aConnexion, aConnexion.Ident, ReqFName, "BUFFER_DETAIL", Requete, "", Me.ListeChamps, "", 500, 40, aDescro)

'-> Charger le browse des salaires
CreateListViewNaviga Naviga, aFrm.ListView1

'-> Fin de la temporiation
Unload frmWait
Screen.MousePointer = 0
Me.Enabled = True

'-> affichage de la requete
aFrm.Show vbModal

Exit Sub

GestError:
'-> Fin de la temporiation
Unload frmWait
Screen.MousePointer = 0
Me.Enabled = True '-> On lance la navigation

End Sub


'*************************************************************************************************
'*************************************************************************************************
'******GESTION DES TABLES ************************************************************************
'*************************************************************************************************
