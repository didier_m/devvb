VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmCompoRequete 
   BackColor       =   &H80000004&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Composition d'une requ�te"
   ClientHeight    =   10455
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   14160
   Icon            =   "frmCompoRequete.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10455
   ScaleWidth      =   14160
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdParamVue 
      Height          =   615
      Left            =   1440
      Picture         =   "frmCompoRequete.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   26
      Top             =   9840
      Width           =   735
   End
   Begin VB.CommandButton cmdOpenVue 
      Height          =   615
      Left            =   720
      Picture         =   "frmCompoRequete.frx":364C
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   9840
      Width           =   735
   End
   Begin VB.Frame Frame7 
      ForeColor       =   &H00800000&
      Height          =   1695
      Left            =   11040
      TabIndex        =   19
      Top             =   7080
      Width           =   3015
      Begin VB.CheckBox Check1 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   1320
         Width           =   2655
      End
      Begin VB.TextBox txtPage 
         Height          =   285
         Left            =   1920
         TabIndex        =   22
         Text            =   "50"
         Top             =   840
         Width           =   855
      End
      Begin VB.TextBox txtBuffer 
         Height          =   285
         Left            =   1920
         TabIndex        =   20
         Text            =   "500"
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label2 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   840
         Width           =   1695
      End
      Begin VB.Label Label1 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   360
         Width           =   1695
      End
   End
   Begin VB.CommandButton Command4 
      Height          =   615
      Left            =   0
      Picture         =   "frmCompoRequete.frx":5FCE
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   9840
      Width           =   735
   End
   Begin VB.CommandButton Command3 
      Height          =   615
      Left            =   13320
      Picture         =   "frmCompoRequete.frx":6898
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   9840
      Width           =   735
   End
   Begin VB.Frame Frame5 
      ForeColor       =   &H00800000&
      Height          =   1695
      Left            =   6120
      TabIndex        =   14
      Top             =   7080
      Width           =   4815
      Begin MSComctlLib.ListView ListView2 
         Height          =   1335
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   2355
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         AllowReorder    =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin VB.Frame Frame6 
      ForeColor       =   &H00800000&
      Height          =   975
      Left            =   0
      TabIndex        =   12
      Top             =   8880
      Width           =   14055
      Begin MSComctlLib.ListView ListView1 
         Height          =   615
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   13815
         _ExtentX        =   24368
         _ExtentY        =   1085
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin VB.Frame Frame4 
      ForeColor       =   &H00800000&
      Height          =   6975
      Left            =   6120
      TabIndex        =   11
      Top             =   0
      Width           =   7935
      Begin MSComctlLib.ListView ListView3 
         Height          =   6615
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   7695
         _ExtentX        =   13573
         _ExtentY        =   11668
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         AllowReorder    =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin VB.Frame Frame3 
      ForeColor       =   &H00800000&
      Height          =   5055
      Left            =   0
      TabIndex        =   6
      Top             =   3720
      Width           =   6015
      Begin VB.CommandButton cmdF10 
         Height          =   615
         Left            =   720
         Picture         =   "frmCompoRequete.frx":8562
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   4320
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Height          =   615
         Left            =   5280
         Picture         =   "frmCompoRequete.frx":8E2C
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   4320
         Width           =   615
      End
      Begin VB.CommandButton Command2 
         Height          =   615
         Left            =   120
         Picture         =   "frmCompoRequete.frx":9AF6
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   4320
         Width           =   615
      End
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   3600
         TabIndex        =   7
         Top             =   720
         Visible         =   0   'False
         Width           =   1815
      End
      Begin MSFlexGridLib.MSFlexGrid MSFlexGrid2 
         Height          =   4035
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   5820
         _ExtentX        =   10266
         _ExtentY        =   7117
         _Version        =   393216
         Rows            =   16
         Cols            =   4
         FixedCols       =   0
         BackColor       =   16777215
         TextStyleFixed  =   1
      End
   End
   Begin VB.Frame Frame2 
      ForeColor       =   &H00800000&
      Height          =   2895
      Left            =   0
      TabIndex        =   2
      Top             =   720
      Width           =   6015
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   4800
         Picture         =   "frmCompoRequete.frx":A7C0
         ScaleHeight     =   240
         ScaleWidth      =   240
         TabIndex        =   4
         Top             =   600
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   4800
         Picture         =   "frmCompoRequete.frx":AB02
         ScaleHeight     =   240
         ScaleWidth      =   240
         TabIndex        =   3
         Top             =   960
         Visible         =   0   'False
         Width           =   240
      End
      Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
         Height          =   2535
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   5775
         _ExtentX        =   10186
         _ExtentY        =   4471
         _Version        =   393216
         Rows            =   10
         Cols            =   4
      End
   End
   Begin VB.Frame Frame1 
      ForeColor       =   &H00800000&
      Height          =   615
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6015
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   5775
      End
   End
End
Attribute VB_Name = "frmCompoRequete"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Indique si on a charge les entetes ou pas
Dim EntetesOk As Boolean




Private Sub cmdF10_Click()
'---> lance un F10
If Me.Text1.Visible = True Then
    Me.MSFlexGrid2.TextMatrix(ClickY, ClickX) = Trim(Me.Text1.Text)
    Me.MSFlexGrid2.CellAlignment = 1
    Me.Text1.Visible = False
End If
LoadSearch
End Sub

Private Sub cmdOpenVue_Click()
'---> Permet d'ouvrir une vue existante
Dim FileName As String
Dim HdlFileReq As Integer
Dim lpBuffer As String
Dim Res As Long
Dim FicParam As String
Dim i As Integer
Dim aIt As ListItem

'-> On nettoie tout le reste
Me.ListView3.ListItems.Clear
Me.ListView2.ListItems.Clear
Me.ListView2.ColumnHeaders.Clear
Me.ListView3.ColumnHeaders.Clear

'-> On bloque lors de la modif de l'ecran
LockWindowUpdate Me.hwnd

'-> recharge les entetes de chacuns des tableaux de parametrages
LoadEntetes

'-> On debloque
LockWindowUpdate 0

'-> On recup le premier dans le repertoire
FileName = Dir$(aConnexion.ApplicationDir & aConnexion.Ident & "\Maq\*.VUEREQ")

'-> On ajoute les entete des colonnes
frmOuvrir.ListView1.ColumnHeaders.Add , , GetMesseprog("REQUETEUR", "1")
frmOuvrir.ListView1.ColumnHeaders(1).Width = 2500
frmOuvrir.ListView1.ColumnHeaders.Add , , GetMesseprog("REQUETEUR", "2")
frmOuvrir.ListView1.ColumnHeaders(2).Width = 3900

'-> On cherche a ouvrir les vues existantes
Do While FileName <> ""
    '-> On ajoute dans la liste des choix
    Set aIt = frmOuvrir.ListView1.ListItems.Add(, , Mid$(FileName, 1, InStr(1, FileName, ".VUEREQ") - 1), , 1)
    '-> Recup du fname  de la requete
    lpBuffer = Space$(500)
    Res = GetPrivateProfileString("PARAMETRAGE", "COMMENT", "Erreur", lpBuffer, Len(lpBuffer), aConnexion.ApplicationDir & aConnexion.Ident & "\Maq\" & FileName)
    aIt.SubItems(1) = Mid$(lpBuffer, 1, Res)
    '-> On passe au suivant dans le repertoire
    FileName = Dir
Loop

strRetour = ""
'-> On affiche le choix
frmOuvrir.Show vbModal
'-> sort si pas de vue saisie
If strRetour = "" Then Exit Sub
'-> On affecte le fichier de vue selectionn�
FicParam = aConnexion.ApplicationDir & aConnexion.Ident & "\Maq\" & "\" & strRetour & ".VUEREQ"

'-> debut tempo
frmWait.Label1 = GetMesseprog("REQUETEUR", "3")
frmWait.Show
Me.Enabled = False
Screen.MousePointer = 11

'-> On ouvre la vue
OpenVue FicParam


'-> fin tempo
Unload frmWait
Me.Enabled = True
Screen.MousePointer = 0

'-> affichage de la requete
frmVueDetail.Show vbModal

Exit Sub

GestError:
'-> On debloque
LockWindowUpdate 0
Unload frmWait
Screen.MousePointer = 0
Me.Enabled = True

End Sub

Private Sub cmdParamVue_Click()
'---> Affichage du parametrage de la requete apres execution

'-> test si parametrage complet
If Trim(ReqFName) = "" Or Trim(strReqDet) = "" Or ListeChamps = "" Then
    MsgBox GetMesseprog("REQUETEUR", "4"), vbExclamation + vbOKOnly, GetMesseprog("REQUETEUR", "5")
    Exit Sub
End If

'-> On charge les zone de la fenetre de visu des parametrages
frmVisuParam.Text1.Text = ReqFName
frmVisuParam.Text2.Text = ReqIName
'frmVisuParam.txtComment = pas disponible � ce niveau d'�xecution
frmVisuParam.Text3.Text = strReqDet
frmVisuParam.Text4.Text = strReqCum
frmVisuParam.Text5.Text = ListeChamps
frmVisuParam.Text6.Text = ListeConditionsHorsIndex


'-> On affiche le parametrage
frmVisuParam.Show vbModal

End Sub

Private Sub Combo1_Click()
'---> Permet de charger les index d'un fichier selectionn�
Dim i As Integer
Dim j As Integer

'-> teste si fichier de param charg�
If GloFname <> "" Then Exit Sub

'-> remise a blanc des parametres de la requete
ReqFName = ""
ReqIName = ""
ListeChamps = ""
ListeGroupes = ""
IndexCumul = False
strReqDet = ""
strReqCum = ""
GloCumul = False
IndexCumul = False

'-> Remise a blanc de toutes les conditions de la requete
For i = 1 To Me.MSFlexGrid1.Rows - 1
    Me.MSFlexGrid1.Row = i
    For j = 0 To Me.MSFlexGrid1.Cols - 1
        Me.MSFlexGrid1.Col = j
        If j = 0 Then Me.MSFlexGrid1.CellBackColor = Me.MSFlexGrid1.BackColorFixed
        Me.MSFlexGrid1.TextMatrix(i, j) = ""
        If Me.MSFlexGrid1.CellPicture <> 0 Then Set Me.MSFlexGrid1.CellPicture = Nothing
    Next
Next
For i = 1 To Me.MSFlexGrid2.Rows - 1
    For j = 0 To Me.MSFlexGrid1.Cols - 1
        Me.MSFlexGrid2.TextMatrix(i, j) = ""
    Next
Next

Me.ListView1.ListItems.Clear
Me.ListView2.ListItems.Clear
Me.ListView3.ListItems.Clear

'-> Charge les indexes si fichier a indexes multiples
LoadIndexes Trim(UCase$(Entry(1, Me.Combo1.Text, "-")))

End Sub

Private Sub LoadIndexes(Fichier As String)
'---> Choix d'un index pour la requete
Dim Ligne As String
Dim OrdreOk As Boolean
Dim aBuffer As B3DBuffer
Dim i As Integer
Dim Fonction As String
Dim NbIndex As Integer

'-> Acces au descriptif du fichier selectionn�
Set aDescro = GetFnameDescriptif(aConnexion, aConnexion.Ident, Fichier)
If aDescro Is Nothing Then
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam
    Exit Sub
End If

ReqFName = Fichier

'-> teste si fichier a index direct pour charger les indexes
If aDescro.IndexDirect = "" Then
    
    '-> On verifie si multi indexes
    '-> Ligne de cde B3D
    Ligne = "B3D_SCHEMA_DES~1~0~" & Trim(UCase$(Entry(1, Me.Combo1.Text, "-")))
    
    '-> Initialisation pour demande
    If Not aConnexion.InitDmd(True) Then
        '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
        MsgBox GetMesseprog("REQUETEUR", "6") & Chr(13) & GetMesseprog("REQUETEUR", "7"), vbExclamation
        Exit Sub
    End If
    
    '-> On envoie une demande de recup des fichiers du catalogue
    '-> On lance la fonction d'analyse des locks brul�es
    OrdreOk = aConnexion.PutInstruction(Ligne, True)
    
    '-> Verifie si on a bien envoy� l'instruction
    If Not OrdreOk Then
        '-> Positionner le code Erreur
        MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
        End
    End If
    
    '-> On envoie l'ordre de lecture des instructions
    Set aBuffer = New B3DBuffer
    If Not aConnexion.SendDemande(aBuffer) Then
        '-> Positionner le code Erreur
        aConnexion.ErrorNumber = "100000"
        aConnexion.ErrorLibel = "Error sending exchange"
        aConnexion.ErrorParam = "LoadAgentDOG"
        End
    End If
    
    '-> Recup de la 1ere ligne
    Ligne = aBuffer.Lignes(1)
    
    If Entry(2, Ligne, "~") <> "OK" Then
        MsgBox GetMesseprog("REQUETEUR", "8"), vbExclamation
        End
    End If
    
    '-> On parcours les lignes pour creer la list des indexes
    '-> On parcours les lignes retourn�es
    Me.MSFlexGrid1.Row = 1
    Me.MSFlexGrid1.Col = 0
    For i = 2 To aBuffer.Lignes.Count
        
        '-> recup de la ligne
        Fonction = Entry(1, aBuffer.Lignes(i), "~")
        Ligne = Entry(2, aBuffer.Lignes(i), "~")
        Fonction = Entry(3, Fonction, "_")
        
        Select Case Fonction
                
            Case "IFIELD"
                '-> Teste si champ d'index
                If CBool(Entry(5, Ligne, "�")) = True Then
                    Me.MSFlexGrid1.Row = Me.MSFlexGrid1.Row - 1
                    Me.MSFlexGrid1.Col = 2
                    Set Me.MSFlexGrid1.CellPicture = Me.Picture1.Picture
                    Me.MSFlexGrid1.CellPictureAlignment = 4
                    Me.MSFlexGrid1.Row = Me.MSFlexGrid1.Row + 1
                End If
                
                '-> recup du champ d'index
                If aDescro.Fields(Entry(4, Ligne, "�")).ZonData = "" Then
                    aDescro.Fields(Entry(4, Ligne, "�")).ZonData = Entry(2, Ligne, "�") & "�" & Entry(3, Ligne, "�") & "�" & Entry(5, Ligne, "�")
                Else
                    aDescro.Fields(Entry(4, Ligne, "�")).ZonData = aDescro.Fields(Entry(4, Ligne, "�")).ZonData & "�" & Entry(2, Ligne, "�") & "�" & Entry(3, Ligne, "�") & "�" & Entry(5, Ligne, "�")
                End If
                                         
            Case "INAME"
                '-> On cree une ligne pour la data en cours
                Me.MSFlexGrid1.Col = 0
                Me.MSFlexGrid1.TextMatrix(Me.MSFlexGrid1.Row, 0) = Entry(2, Ligne, "�")
                Me.MSFlexGrid1.Col = 1
                Me.MSFlexGrid1.RowHeight(Me.MSFlexGrid1.Row) = Me.ScaleY(16, 3, 1)
                Me.MSFlexGrid1.TextMatrix(Me.MSFlexGrid1.Row, 1) = Entry(4, Ligne, "�")
                Me.MSFlexGrid1.TextMatrix(Me.MSFlexGrid1.Row, 3) = Entry(6, Ligne, "�")
                Me.MSFlexGrid1.Row = Me.MSFlexGrid1.Row + 1
                '-> On incremente le compteur d'indexes
                NbIndex = NbIndex + 1
                
        End Select
            
NextLigne:
    
    Next
    
    '-> On debloque la saisie
    Me.Frame2.Enabled = True
    
    '-> teste si un seul index et lme selectionne directement
    If NbIndex = 1 Then
        Me.MSFlexGrid1.Row = 1
        '-> Selection auto et envoi vers la selection sur les champs d'index
        Call MSFlexGrid1_DblClick
    End If
    
    Exit Sub
    
Else
    
    '-> On debloque la frame du choix des champs
    Me.Frame3.Enabled = False
    
    '-> Fichier a index direct
    LoadConditions
    
    Exit Sub

End If

'-> Efface la fenetre de choix des fichiers
Unload Me

End Sub


Private Sub Command1_Click()
'---> On stocke les conditions au niveau du champ
'-> On stocke les conditions au niveau du champ en cours
Dim i As Integer
Dim j As Integer
Dim Field  As String
Dim aObj As B3DObject
Dim VarDate As String

'-> On valide automatiquement si l'utilisateur ne l'a pas fait
If Me.Text1.Visible = True Then
    Me.MSFlexGrid2.TextMatrix(Me.MSFlexGrid2.Row, Me.MSFlexGrid2.Col) = Trim(UCase$(Me.Text1.Text))
    Me.Text1.Visible = False
    Me.MSFlexGrid2.Enabled = True
End If

For i = 1 To Me.MSFlexGrid2.Rows - 1
    
    '-> teste si dernier champ d'index
    If Me.MSFlexGrid2.TextMatrix(i, 0) = "" Then Exit For
    
    Field = Me.MSFlexGrid2.TextMatrix(i, 0)
    
    '-> Recup du descro du field
    aDescro.Fields(Field).ZonLock = ""
    
    '-> test si fin des conditions
    If Me.MSFlexGrid2.TextMatrix(i, 0) = "" Then Exit For
    
    '-> On teste le type de donn�e du champ en cours
    Set aObj = Catalogues(Field)
    '-> test si zone de type DATE pour inverser le format
    If Trim(UCase$(aObj.DataType)) <> "DATE" Then
        '-> On consitue la liste des conditions
        aDescro.Fields(Field).ZonLock = "CDT_" & i & "�" & Me.MSFlexGrid2.TextMatrix(i, 2) & "�" & Me.MSFlexGrid2.TextMatrix(i, 3) & "�"
    Else
        '-> On reformatte les dates
        aDescro.Fields(Field).ZonLock = "CDT_" & i & "�" & Entry(3, Me.MSFlexGrid2.TextMatrix(i, 2), "/")
        aDescro.Fields(Field).ZonLock = aDescro.Fields(Field).ZonLock & Entry(2, Me.MSFlexGrid2.TextMatrix(i, 2), "/")
        aDescro.Fields(Field).ZonLock = aDescro.Fields(Field).ZonLock & Entry(1, Me.MSFlexGrid2.TextMatrix(i, 2), "/") & "�"
        aDescro.Fields(Field).ZonLock = aDescro.Fields(Field).ZonLock & Entry(3, Me.MSFlexGrid2.TextMatrix(i, 3), "/")
        aDescro.Fields(Field).ZonLock = aDescro.Fields(Field).ZonLock & Entry(2, Me.MSFlexGrid2.TextMatrix(i, 3), "/")
        aDescro.Fields(Field).ZonLock = aDescro.Fields(Field).ZonLock & Entry(1, Me.MSFlexGrid2.TextMatrix(i, 3), "/") & "�"
    End If
    
Next

'-> Chargement des champs PIERROT : penser aux groupes
LoadFields

End Sub

Private Sub Command3_Click()
'---> On lance la requete et on cree un listview de r�sultat
Dim Res As String
Dim i As Integer
Dim aField As B3DField
Dim ValMin As String
Dim ValMax As String



'-> teste si on a charge le parametrage
If aDescro Is Nothing Then
    MsgBox GetMesseprog("REQUETEUR", "9"), vbExclamation + vbOKOnly, GetMesseprog("REQUETEUR", "5")
    Exit Sub
End If

'->On re-initialise pour relancer
Set Naviga = Nothing

'-> Permet de preparer la requete
Res = PrepareRequeteSql

'-> On isole les valeurs de retour
strReqDet = Entry(1, Res, "|")
strReqCum = Entry(2, Res, "|")
ListeChamps = Entry(3, Res, "|")
ListeChampsCum = Entry(4, Res, "|")

'-> teste si au moins 1 champ de selectionn�
If ListeChamps = "" Then
    MsgBox GetMesseprog("REQUETEUR", "10"), vbExclamation
    Exit Sub
End If

frmWait.Label1 = GetMesseprog("REQUETEUR", "3")
frmWait.Show

'-> teste si requete de cumul ou pas
If strReqCum = "" Then
    '-> On cree une liste de conditions hors index
    ListeConditionsHorsIndex = ""
    For Each aField In aDescro.Fields
        If Entry(1, aField.ZonLock, "�") <> "HORS" Then GoTo NextField
        '-> On recupere les valeurs de cdt min et max
        ValMin = Trim(Entry(1, Entry(2, aField.ZonLock, "�"), "�"))
        ValMax = Trim(Entry(1, Entry(2, aField.ZonLock, "�"), "�"))
        '-> On cree la liste des cdt au format B3D
          If ValMin <> "" And ValMax <> "" Then
            If ListeConditionsHorsIndex = "" Then
                ListeConditionsHorsIndex = aField.Field & "�INC�" & ValMin & "�" & ValMax
            Else
                ListeConditionsHorsIndex = ListeConditionsHorsIndex & "�" & aField.Field & "�INC�" & ValMin & "�" & ValMax
            End If
        Else
            If ValMin <> "" Then
                If ListeConditionsHorsIndex = "" Then
                    ListeConditionsHorsIndex = aField.Field & "�EQ�" & ValMin & "�"
                Else
                    ListeConditionsHorsIndex = ListeConditionsHorsIndex & "�" & aField.Field & "�EQ�" & ValMin & "�"
                End If
            End If
        End If
        
NextField:
    Next
        
    '-> On lance la requete gener�e
    Set Naviga = InitNaviga(aConnexion, aConnexion.Ident, ReqFName, "BUFFER_DETAIL", strReqDet, ListeConditionsHorsIndex, ListeChamps, "", Me.txtBuffer.Text, Me.txtPage.Text, aDescro)
Else
    '-> On est sur une requete de cumul
    frmVueDetail.Cumul = True
    '-> On stocke la requete cumul pour acces au detail
    frmVueDetail.strReqDet = strReqDet
    '-> On envoie la liste des champs
    frmVueDetail.ListeChamps = ListeChamps
    '-> On lance la requete gener�e
    Set Naviga = InitNaviga(aConnexion, aConnexion.Ident, ReqFName & "�" & ReqIName, "BUFFER_CUMUL", strReqCum, "", ListeChampsCum, "", Me.txtBuffer.Text, Me.txtPage.Text, aDescro)
End If

'-> Tester le retour de navigation
If Naviga Is Nothing Then
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter la fonction
    GoTo GestError
End If

'-> Charger le browse des salaires
CreateListViewNaviga Naviga, frmVueDetail.ListView1

'-> Cache la tempo
Unload frmWait

'-> affichage de la requete
If IndexCumul = True Then
    frmVueDetail.Cumul = True
Else
    frmVueDetail.Cumul = False
End If
frmVueDetail.Show vbModal


'-> On remet tout a blanc
Naviga.ListeFieldToDisplay = ""
Naviga.ListeFieldToGet = ""

'-> On re-initialise les entetes de colonnes de la feuille de visu
frmVueDetail.ListView1.ListItems.Clear
frmVueDetail.ListView1.ColumnHeaders.Clear

'-> On remet a blanc la liste des champs a afficher
Me.ListView1.ColumnHeaders.Clear

'-> On de-check le item a afficher
For i = 1 To Me.ListView2.ListItems.Count
    Me.ListView2.ListItems(i).Checked = False
Next

For i = 1 To Me.ListView3.ListItems.Count
    Me.ListView3.ListItems(i).Checked = False
    aDescro.Fields(Me.ListView3.ListItems(i).Text).ZonData = ""
Next

'-> decharge le feuille de visu por re-init du listview et des indicateurs de cumul
Unload frmVueDetail

GestError:

'-> Fin de la temporiation
Unload frmWait
Screen.MousePointer = 0
Me.Enabled = True '-> On lance la navigation


End Sub

Private Sub Command4_Click()
'---> On sauvegarde la vue en cours dans un fichier ascii
Dim HdlFileReq As Integer
Dim Ligne As String
Dim Rep As String
Dim CommentVue As String

'-> teste si vue cree
If strReqDet = "" Or ReqFName = "" Then
    MsgBox GetMesseprog("REQUETEUR", "4"), vbCritical + vbOKOnly, GetMesseprog("REQUETEUR", "5")
    Exit Sub
End If

'-> saisie du nom et du commentaire de la vue
strRetour = ""
frmParamVue.Show vbModal
'-> teste si annulation
If Trim(strRetour) = "" Then Exit Sub

'-> Recup des infos de parametrage
Rep = Entry(1, strRetour, Chr(0))
CommentVue = Entry(2, strRetour, Chr(0))

'-> On sort si aucun nom saisi...
If Trim(Rep) = "" Then
    MsgBox GetMesseprog("REQUETEUR", "11"), vbCritical + vbOKOnly, GetMesseprog("REQUETEUR", "5")
    Exit Sub
End If

'-> Ouverture d'un canal
HdlFileReq = FreeFile
'-> On ouvre le fichier saisi
Open aConnexion.ApplicationDir & aConnexion.Ident & "\Maq\" & Rep & ".VUEREQ" For Output As #HdlFileReq

'-> On ecrit le parametrage dans le fichier
'-> sectionparametrage
Print #HdlFileReq, "[PARAMETRAGE]"
'-> Envoi du fichier sur lequel porte la requete
Print #HdlFileReq, "FNAME=" & ReqFName
'-> Envoi de l'index utilisae pour la requete
Print #HdlFileReq, "INAME=" & ReqIName
'-> Envoi de la requete de detail
Print #HdlFileReq, "REQDET=" & strReqDet
'-> Envoi de la requete de cumul
Print #HdlFileReq, "REQCUM=" & strReqCum
'-> Envoi de la liste des champs a afficher
'-> Envoi de la requete de detail
Print #HdlFileReq, "LISTECHAMPS=" & ListeChamps
'-> Envoi de la requete de cumul
Print #HdlFileReq, "LISTECHAMPSCUM=" & ListeChampsCum
'-> Commentaire de la requete
Print #HdlFileReq, "COMMENT=" & CommentVue
'-> Conditions hors index
Print #HdlFileReq, "CDTHINDEX=" & ListeConditionsHorsIndex

'-> On ferme le fichier
Close #HdlFileReq

'-> On rpevient de la fin de sauvegarde
MsgBox GetMesseprog("REQUETEUR", "13"), vbInformation, GetMesseprog("REQUETEUR", "12")

End Sub

Private Sub Form_Load()
'-> Charge les entetes de chacuns des tableaux de parametrage
LoadEntetes
'-> Chargement des libelles de la fenetre
Me.Frame1.Caption = GetMesseprog("REQUETEUR", "35")
Me.Frame2.Caption = GetMesseprog("REQUETEUR", "36")
Me.Frame3.Caption = GetMesseprog("REQUETEUR", "37")
Me.Frame4.Caption = GetMesseprog("REQUETEUR", "38")
Me.Frame5.Caption = GetMesseprog("REQUETEUR", "39")
Me.Frame6.Caption = GetMesseprog("REQUETEUR", "40")
Me.Frame7.Caption = GetMesseprog("REQUETEUR", "41")

Me.Label1.Caption = GetMesseprog("REQUETEUR", "42")
Me.Label2.Caption = GetMesseprog("REQUETEUR", "43")

Me.Caption = GetMesseprog("REQUETEUR", "44")

Me.Command2.ToolTipText = GetMesseprog("REQUETEUR", "45")
Me.cmdF10.ToolTipText = GetMesseprog("REQUETEUR", "46")
Me.Command1.ToolTipText = GetMesseprog("REQUETEUR", "47")
Me.Command4.ToolTipText = GetMesseprog("REQUETEUR", "48")
Me.cmdOpenVue.ToolTipText = GetMesseprog("REQUETEUR", "49")
Me.cmdParamVue.ToolTipText = GetMesseprog("REQUETEUR", "50")
Me.Command3.ToolTipText = GetMesseprog("REQUETEUR", "51")
Me.Check1.Caption = GetMesseprog("REQUETEUR", "63")

End Sub



Private Sub ListView2_ItemCheck(ByVal Item As MSComctlLib.ListItem)
'--->permet de selectionner tous les champs d'un groupe
Dim i As Integer

For i = 1 To Me.ListView3.ListItems.Count
    '-> teste si on est sur un champ du groupe selectionn�
    If Me.ListView3.ListItems(i).SubItems(1) = Item.Text And Me.ListView3.ListItems(i).Checked <> Item.Checked Then
        Me.ListView3.ListItems(i).Checked = Item.Checked
        If Me.ListView3.ListItems(i).Checked = True Then
            aDescro.Fields(Me.ListView3.ListItems(i).Text).ZonData = "EDIT"
            '-> On ajoute de la requete
            Me.ListView1.ColumnHeaders.Add , "FIELD|" & Trim(UCase$(Me.ListView3.ListItems(i).Text)), Me.ListView3.ListItems(i).SubItems(2)
        Else
            '-> On supprime de la requete
            Me.ListView1.ColumnHeaders.Remove "FIELD|" & Trim(UCase$(Me.ListView3.ListItems(i).Text))
            aDescro.Fields(Me.ListView3.ListItems(i).Text).ZonData = ""
        End If
    End If
Next

End Sub

Private Sub ListView3_DblClick()
'---> On ajoute des conditions hors-index

'-> On ne propose les conditions hors-index que si pas de cumul
If IndexCumul = True Then Exit Sub

'-> On lance la saisie en precisant le champ sur lequel on les applique
frmConditionsHorsIndex.RecField = Trim(UCase$(Me.ListView3.SelectedItem.Text))
frmConditionsHorsIndex.TypeField = Trim(UCase$(Me.ListView3.SelectedItem.SubItems(3)))

'-> On rpecharge les valeurs si il y a deja des cdto
frmConditionsHorsIndex.Text1.Text = Me.ListView3.SelectedItem.SubItems(4)
frmConditionsHorsIndex.Text2.Text = Me.ListView3.SelectedItem.SubItems(5)

'-> On affiche la fenetre de saisie
frmConditionsHorsIndex.Show vbModal

End Sub

Private Sub ListView3_ItemCheck(ByVal Item As MSComctlLib.ListItem)
'---> permet d'ajouter ou de supprimer un champ de l'apercu de la requete

If Item.Checked = False Then
    '-> On supprime de la requete
    Me.ListView1.ColumnHeaders.Remove "FIELD|" & Trim(UCase$(Item.Text))
    '-> permet de stocker le fait qu'on affichera pas le champ dans le resultat
    aDescro.Fields(Trim(UCase$(Item.Text))).ZonData = ""
Else
    '-> On ajoute de la requete
    Me.ListView1.ColumnHeaders.Add , "FIELD|" & Trim(UCase$(Item.Text)), Item.SubItems(1)
    '-> permet de stocker le fait qu'on affichera le champ dans le recultat
    aDescro.Fields(Trim(UCase$(Item.Text))).ZonData = "EDIT"
End If
End Sub

Private Sub MSFlexGrid1_Click()
'-> On coche si cumul

'-> On ne change l'image que si click sur colonne 2
If ClickX = 2 Then
    If IndexCumul = True Then
        '-> On change d'image selon que la case est selectionn�e ou pas
        If Me.MSFlexGrid1.CellPicture = Me.Picture1.Picture Then
            Set Me.MSFlexGrid1.CellPicture = Me.Picture2.Picture
        Else
            Set Me.MSFlexGrid1.CellPicture = Me.Picture1.Picture
        End If
    End If
End If

End Sub


Private Sub MSFlexGrid1_DblClick()
'---> Choix definitif d'un index

'-> Si on coche pour cumul : on valide pas
If ClickX = 2 Then Exit Sub

'-> On charge les fields
Me.MSFlexGrid1.Col = 0

'-> On stocke l'index et le fait qu'il soit cumul ou pas
IndexRequete = Trim(UCase$(Me.MSFlexGrid1.TextMatrix(Me.MSFlexGrid1.Row, 0)))
ReqIName = IndexRequete
Me.MSFlexGrid1.Col = 2
If Me.MSFlexGrid1.CellPicture = Me.Picture2.Picture Then
    '-> On a coche que index a cumul
    IndexCumul = True
End If

'-> On bloque une fois le choix effectu�
Me.Frame2.Enabled = False

'-> On colorie la ligne selectionn�
Me.MSFlexGrid1.Col = 0
Me.MSFlexGrid1.CellBackColor = &H800000

'-> on charge les fields dans le fenetre de choix des champs
LoadConditions

End Sub

Private Sub LoadConditions()
'-> prechargement et formattage du flex
Dim aField As B3DField
Dim i As Integer

'-> On se positionne au debut
Me.MSFlexGrid2.Rows = 16
Me.MSFlexGrid2.Row = 1
Me.MSFlexGrid2.Col = 0

'-> On selectionne les champs de l'index avec ou sans cumul
'-> On parcours les fields pour creer un me.MSFlexGrid2 grid
For Each aField In aDescro.Fields
    
    '-> teste si champ d'index
    If aDescro.IndexDirect = "" Then
        '-> cas de l'index multiple
        If aField.ZonData <> "" Then
            
            '-> On parcours les proprietes du champ en cours
            For i = 1 To NumEntries(aField.ZonData, "�")
                If Entry(1, Entry(i, aField.ZonData, "�"), "�") = IndexRequete Then
                    '-> On ne stocke que les donnes de l'index que l'on a choisi
                    aField.ZonData = Entry(i, aField.ZonData, "�")
                    Me.MSFlexGrid2.Row = Entry(2, aField.ZonData, "�")
                    '-> teste si champ d'index cumul
                    If IndexCumul = True Then
                         If Entry(3, Entry(2, aField.ZonData, "�"), "�") <> "-1" Then
                             GoTo NextField
                         End If
                     End If
                       
                    '-> On renseigne la ligne
                    Me.MSFlexGrid2.Col = 0
                    Me.MSFlexGrid2.Text = aField.Field
                    Me.MSFlexGrid2.CellAlignment = 4
                    Me.MSFlexGrid2.Col = 1
                    Me.MSFlexGrid2.Text = aField.Libel
                    Me.MSFlexGrid2.CellAlignment = 4
                    Exit For
                End If
            Next
        Else
            GoTo NextField
        End If
    Else
        '-> teste si champ de l'index
        '-> On parcours les proprietes du champ en cours
        For i = 1 To NumEntries(aField.ZonData, "�")
            If aField.Field = aDescro.IndexDirect Then
                '-> teste si champ de l'index direct
                '-> On ne stocke que les donnes de l'index que l'on a choisi
                aField.ZonData = Entry(i, aField.ZonData, "�")
                   
                '-> On renseigne la ligne
                Me.MSFlexGrid2.Col = 0
                Me.MSFlexGrid2.Text = aField.Field
                Me.MSFlexGrid2.CellAlignment = 4
                Me.MSFlexGrid2.Col = 1
                Me.MSFlexGrid2.Text = aField.Libel
                Me.MSFlexGrid2.CellAlignment = 4
                Me.MSFlexGrid2.Row = Me.MSFlexGrid2.Row + 1
                Exit For
            End If
        Next
    End If
    
NextField:
    
Next

'-> On debloque la saisie
Me.Frame3.Enabled = True

End Sub




Private Sub MSFlexGrid1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'-> Get de la colonne et de la ligne
ClickX = GetCol(Me.MSFlexGrid1, x)
ClickY = GetRow(Me.MSFlexGrid1, y)
End Sub

Private Sub MSFlexGrid2_DblClick()
'-> Selon la colonne on se place en saisie ou choix de type de cdt

If ClickY > 0 And ClickX > 1 Then
    '-> On se place en saisie sur les conditions sur les champs
    If Me.MSFlexGrid2.TextMatrix(ClickY, ClickX) <> "" Then
        Me.Text1.Text = Me.MSFlexGrid2.TextMatrix(ClickY, ClickX)
        Me.Text1.SelStart = 0
        Me.Text1.SelLength = Len(Me.Text1.Text)
    Else
        Me.Text1.Text = ""
    End If
    '-> On place le texte sur la cellule
    Me.Text1.Top = Me.MSFlexGrid2.CellTop + Me.MSFlexGrid2.Top
    Me.Text1.Height = Me.MSFlexGrid2.CellHeight
    Me.Text1.Left = Me.MSFlexGrid2.CellLeft + Me.MSFlexGrid2.Left
    Me.Text1.Width = Me.MSFlexGrid2.CellWidth
    Me.Text1.Visible = True
    Me.Text1.SetFocus
    Me.MSFlexGrid2.Enabled = False
End If
End Sub

Private Sub MSFlexGrid2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'-> Get de la colonne et de la ligne
ClickX = GetCol(Me.MSFlexGrid2, x)
ClickY = GetRow(Me.MSFlexGrid2, y)
End Sub



Private Sub Text1_KeyPress(KeyAscii As Integer)
If KeyAscii = 27 Then Me.Text1.Visible = False

If KeyAscii = 13 Then
    Me.MSFlexGrid2.Text = Me.Text1.Text
    Me.MSFlexGrid2.CellAlignment = 1
    Me.Text1.Visible = False
    Me.MSFlexGrid2.Enabled = True
End If

End Sub

Private Sub Command2_Click()
Dim i As Integer
For i = 1 To Me.MSFlexGrid2.Rows - 1
    Me.MSFlexGrid2.TextMatrix(i, 2) = ""
    Me.MSFlexGrid2.TextMatrix(i, 3) = ""
Next
End Sub


Private Sub LoadFields()
'---> permet de charger les fields d'un fichier donn�e dans la fenetre de visu
Dim aField As B3DField
Dim i As Integer
Dim aIt As ListItem
Dim aObj As B3DObject
Dim GroupeOk As Boolean

'-> On parcours les fields pour creer un flex grid
For Each aField In aDescro.Fields
    
    '-> On pointe sur le descro du champ du catalogue
    Set aObj = Catalogues(aField.Field)
    
    '-> test pour n'afficher que les champs dans la langue de cdonnexion
    If aObj.MultiLangue <> 0 And aObj.MultiLangue <> aConnexion.CodeLangue Then GoTo NextField
    
    '-> On cree une ligne dans le listview
    Set aIt = Me.ListView3.ListItems.Add(, "FIELD|" & aField.Field, aField.Field)
    aIt.SubItems(1) = aObj.Progiciel
    aIt.SubItems(2) = aField.Libel
    aIt.SubItems(3) = aObj.DataType
    
    GroupeOk = False
    '-> On cree le vecteur des groupes
    For i = 1 To NumEntries(ListeGroupes, "�")
        If Entry(i, ListeGroupes, "�") = aObj.Progiciel Then
            GroupeOk = True
            Exit For
        End If
    Next
    If GroupeOk = False Then
        If ListeGroupes = "" Then
            ListeGroupes = aObj.Progiciel
        Else
            ListeGroupes = ListeGroupes & "�" & aObj.Progiciel
        End If
    End If
 
NextField:
 
Next

'-> Formattage du listview
FormatListView Me.ListView3

'-> On charge le listview des groupes
LoadGroupes
'-> On debloque les ecrans de saisie
Me.Frame4.Enabled = True
Me.Frame5.Enabled = True
'-> On bloque le reste
'Me.frame1.Enabled = False
Me.Frame2.Enabled = False
Me.Frame3.Enabled = False

End Sub

Private Sub listview3_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    '-> Trier sur les entetes de colonne
    Me.ListView3.SortKey = ColumnHeader.Index - 1
    If Me.ListView3.SortOrder = lvwAscending Then
        Me.ListView3.SortOrder = lvwDescending
    Else
        Me.ListView3.SortOrder = lvwAscending
    End If
    Me.ListView3.Sorted = True
End Sub


Public Function PrepareRequeteSql() As String
'-> Permet de preparer une requete SQL et une liste de champs
Dim aField As B3DField
Dim Indice As Integer
Dim ValMini As String
Dim ValMaxi As String
Dim aObj As B3DObject

If aDescro.IndexDirect = "" Then
    '-> cas d'un fichier � index multiple avec cumul
    If IndexCumul = True Then
        
        '-> AVEC cumul
        strReqCum = "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & aConnexion.Ident & "�" & ReqFName & "�" & ReqIName & "' AND [INAME] = '" & ReqIName & "' "
        
        '-> On cherche les conditiuons sur champs d'index
        For Each aField In aDescro.Fields
            If aField.ZonLock <> "" Then
                Indice = CInt(Entry(2, Entry(1, aField.ZonLock, "�"), "_"))
                ValMini = Entry(1, Entry(2, aField.ZonLock, "�"), "�")
                ValMaxi = Entry(2, Entry(2, aField.ZonLock, "�"), "�")
                
                If ValMini <> "" And ValMaxi = "" Then
                    strReqCum = strReqCum & " AND [ZON_INDEX_" & Indice & "] = '" & ValMini & "'"
                End If
                
                If ValMaxi <> "" Then
                    strReqCum = strReqCum & " AND ( [ZON_INDEX_" & Indice & "] >= '" & ValMini & "') AND ( [ZON_INDEX_" & Indice & "] <= '" & ValMaxi & "')"
                End If
            End If
        Next
        
        '-> On cree la requete de detail associee
              '-> sans cumul
        strReqDet = "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & aConnexion.Ident & "�" & ReqFName & "' AND [INAME] = '" & ReqIName & "' "
        
        ListeChampsCum = ""
        
        '-> On cherche les conditiuons sur champs d'index
        For Each aField In aDescro.Fields
            If aField.ZonLock <> "" Then
                Indice = CInt(Entry(2, Entry(1, aField.ZonLock, "�"), "_"))
                strReqDet = strReqDet & " AND [ZON_INDEX_" & Indice & "] = '#" & aField.Field & "#'"
                If Trim(ListeChampsCum) = "" Then
                    ListeChampsCum = aField.Field & "�"
                Else
                    ListeChampsCum = ListeChampsCum & "�" & aField.Field & "�"
                End If
                
                '-> On va chercher dans le catalogue si table associ�e ?
                Set aObj = Catalogues(aField.Field)
                
                If aObj.Table_Correspondance <> "" And Me.Check1.Value = Checked Then
                    ListeChampsCum = ListeChampsCum & "1"
                Else
                    ListeChampsCum = ListeChampsCum & "0"
                End If
                
            End If
        Next
        
    Else
        '-> sans cumul
        strReqDet = "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & aConnexion.Ident & "�" & ReqFName & "' AND [INAME] = '" & ReqIName & "' "
        
        '-> On cherche les conditiuons sur champs d'index
        For Each aField In aDescro.Fields
            If aField.ZonLock <> "" Then
                If Trim(Entry(1, aField.ZonLock, "�")) = "HORS" Then GoTo NextField
                Indice = CInt(Entry(2, Entry(1, aField.ZonLock, "�"), "_"))
                ValMini = Entry(1, Entry(2, aField.ZonLock, "�"), "�")
                ValMaxi = Entry(2, Entry(2, aField.ZonLock, "�"), "�")
                
                If ValMini <> "" And ValMaxi = "" Then
                    strReqDet = strReqDet & " AND [ZON_INDEX_" & Indice & "] = '" & ValMini & "'"
                End If
                
                If ValMaxi <> "" Then
                    strReqDet = strReqDet & " AND ( [ZON_INDEX_" & Indice & "] >= '" & ValMini & "') AND ( [ZON_INDEX_" & Indice & "] <= '" & ValMaxi & "')"
                End If
            End If
NextField:
        Next
        
    End If
    
Else
    '-> Cas d'un fichier a index direct
    strReqDet = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & aConnexion.Ident & "�" & ReqFName & "' AND [FIELD] = '" & aDescro.IndexDirect & "'"
    '-> tester si on a des conditions sur l'index
    Set aField = aDescro.Fields(Trim(UCase$(aDescro.IndexDirect)))
    '-> teste si ce field a des conditions pour parcours des autres
    If aField.ZonLock <> "" Then
        ValMini = Entry(1, Entry(2, aField.ZonLock, "�"), "�")
        ValMaxi = Entry(2, Entry(2, aField.ZonLock, "�"), "�")
        
        If ValMini <> "" And ValMaxi = "" Then
            strReqDet = strReqDet & " AND [ZON_DATA] = '" & ValMini & "'"
        End If
        
        If ValMaxi <> "" Then
            strReqDet = strReqDet & " AND ( [ZON_DATA] >= '" & ValMini & "') AND ( [ZON_DATA] <= '" & ValMaxi & "')"
        End If
        
    End If
    
End If

'-> On remet la liste des champs a blanc avant de la recomposer
ListeChamps = ""
'-> On cree une liste de champs a afficher dans la requete
For Each aField In aDescro.Fields
    If aField.ZonData = "EDIT" Then
        If ListeChamps = "" Then
            ListeChamps = aField.Field & "�"
        Else
            ListeChamps = ListeChamps & "�" & aField.Field & "�"
        End If
        
        '-> On va chercher dans le catalogue si table associ�e ?
        Set aObj = Catalogues(aField.Field)
        
        If aObj.Table_Correspondance <> "" And Me.Check1.Value = Checked Then
            ListeChamps = ListeChamps & "1"
        Else
            ListeChamps = ListeChamps & "0"
        End If
        
        '-> Si on est en cumul alors on prend les decimaux
        If IndexCumul = True Then
            If Trim(UCase$(aObj.DataType)) = "DECIMAL" Then
                ListeChampsCum = ListeChampsCum & "�" & aField.Field & "�0"
            End If
        End If
    End If
Next

'-> On retourne la requete detail, la requete de cumul et la liste des champs a afficher
PrepareRequeteSql = strReqDet & "|" & strReqCum & "|" & ListeChamps & "|" & ListeChampsCum

End Function

Private Sub LoadGroupes()
'---> Permet de charger unlistview de tous les groupes de champs
Dim i As Integer

'-> ajoute les colonnes si on est sur la premiere requete
If Me.ListView2.ColumnHeaders.Count = 0 Then
    Me.ListView2.ColumnHeaders.Add , , GetMesseprog("REQUETEUR", "14")
    Me.ListView2.ColumnHeaders(1).Width = 3000
End If

For i = 1 To NumEntries(ListeGroupes, "�")
    Me.ListView2.ListItems.Add , , Entry(i, ListeGroupes, "�")
Next


End Sub

Public Sub LoadFname()
'--->Permet d'ouvrir le requeteur directement sur un fichier pass� en parametres
'-> teste si fichier precis� et envoie
Dim i As Integer

'-> teste si on precharge un fichier donn�
If GloFname <> "" Then
    '-> On se positionne sur le bon fichier dans le
    For i = 0 To Me.Combo1.ListCount - 1
        Me.Combo1.ListIndex = i
        If Trim(UCase$(Entry(1, Me.Combo1.Text, "-"))) = GloFname Then
            Exit For
        End If
    Next
    'Me.frame1.Enabled = False
    '-> On envoie le requeteur sur le fichier selectionn�
    LoadIndexes GloFname
End If

'-> teste si on precharge un index donn�
If GloIname <> "" Then
    
    '-> On charge les fields
    Me.MSFlexGrid1.Col = 0
    
    For i = 1 To Me.MSFlexGrid1.Rows - 1
        
        Me.MSFlexGrid1.Row = i
        
        '-> si on est sur le bon index
        If Trim(UCase$(Me.MSFlexGrid1.Text)) = GloIname Then
            '-> On colorie la ligne selectionn�
            Me.MSFlexGrid1.Col = 0
            Me.MSFlexGrid1.CellBackColor = &H800000
            '-> On affecte l'index de requete
            ReqIName = Trim(UCase$(Me.MSFlexGrid1.Text))
            IndexRequete = ReqIName
            If GloCumul = True Then
                Me.MSFlexGrid1.Col = 2
                Set Me.MSFlexGrid1.CellPicture = Me.Picture2.Picture
                IndexCumul = True
            End If
            '-> on charge les fields dans le fenetre de choix des champs
            LoadConditions
            Me.Frame2.Enabled = False
            Exit Sub
        End If
    
    Next
    
End If

End Sub


Private Sub LoadEntetes()
Dim i As Integer

'-> Entetes de colonnes
Me.MSFlexGrid1.TextMatrix(0, 0) = GetMesseprog("REQUETEUR", "15")
Me.MSFlexGrid1.TextMatrix(0, 1) = GetMesseprog("REQUETEUR", "16")
Me.MSFlexGrid1.TextMatrix(0, 2) = GetMesseprog("REQUETEUR", "17")
Me.MSFlexGrid1.TextMatrix(0, 3) = GetMesseprog("REQUETEUR", "14")

'-> On formatte la largeur des colonnes
Me.MSFlexGrid1.ColWidth(0) = 400
Me.MSFlexGrid1.ColWidth(1) = 3550
Me.MSFlexGrid1.ColWidth(2) = 600
Me.MSFlexGrid1.ColWidth(3) = 1100

'-> Entetes de colonnes
Me.MSFlexGrid2.TextMatrix(0, 0) = GetMesseprog("REQUETEUR", "18")
Me.MSFlexGrid2.TextMatrix(0, 1) = GetMesseprog("REQUETEUR", "19")
Me.MSFlexGrid2.TextMatrix(0, 2) = GetMesseprog("REQUETEUR", "20")
Me.MSFlexGrid2.TextMatrix(0, 3) = GetMesseprog("REQUETEUR", "21")

'-> On formatte la largeur des colonnes
Me.MSFlexGrid2.ColWidth(0) = 650
Me.MSFlexGrid2.ColWidth(1) = 2750
Me.MSFlexGrid2.ColWidth(2) = 1150
Me.MSFlexGrid2.ColWidth(3) = 1150

'-> On cree les entete de colonnes du listview
Me.ListView3.ColumnHeaders.Add , , GetMesseprog("REQUETEUR", "22")
Me.ListView3.ColumnHeaders.Add , , GetMesseprog("REQUETEUR", "14")
Me.ListView3.ColumnHeaders.Add , , GetMesseprog("REQUETEUR", "16")
Me.ListView3.ColumnHeaders.Add , , GetMesseprog("REQUETEUR", "64")
Me.ListView3.ColumnHeaders.Add , , GetMesseprog("REQUETEUR", "20")
Me.ListView3.ColumnHeaders.Add , , GetMesseprog("REQUETEUR", "21")

'-> Indique qu'on a cr�e les entetes
EntetesOk = True

End Sub

Private Sub LoadSearch()
'---> Envoi d'un f10 sur un champ
Dim aObj As B3DObject
Dim aBuffer As B3DBuffer
Dim Ok As Boolean
Dim aEnreg As B3DEnreg
Dim aDescroF10 As B3DFnameDescriptif
Dim aField As B3DField
Dim NumEnregF10 As String

On Error GoTo GestError

If ClickX > 0 Then
    
    '-> Teste si champ F10 et ramene un choix de valeurs
    Set aObj = Catalogues(Trim(UCase$(Me.MSFlexGrid2.TextMatrix(Me.MSFlexGrid2.Row, 0))))
    
    Set aBuffer = New B3DBuffer
    Set aEnreg = New B3DEnreg
    
    
    '-> Bloquer la feuille
    Me.Enabled = False
    Screen.MousePointer = 11

    '-> Vider la variable de retour
    strRetour = ""

    '-> On cree un buffer pour rapatrier les data du f10
    If Not FieldTable(aConnexion, aBuffer, aConnexion.Ident, Trim(UCase$(aObj.Num_Field)), Trim(Me.MSFlexGrid2.TextMatrix(ClickY, ClickX))) Then
        'THIERRY
        MsgBox GetMesseprog("REQUETEUR", "23"), vbCritical + vbOKOnly, GetMesseprog("REQUETEUR", "5")
        GoTo GestError
    End If
        
    Screen.MousePointer = 0
    
    '-> recup d'un descro du fichier de jointure
    Set aDescroF10 = GetFnameDescriptif(aConnexion, aConnexion.Ident, aObj.Table_Correspondance)
    If aDescroF10 Is Nothing Then
        MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam
        GoTo GestError
    End If
    
    '-> On affiche le resultat dans un browse
    ShowFieldTableBrowse aBuffer, GetMesseprog("REQUETEUR", "24") & " " & Trim(Me.MSFlexGrid2.TextMatrix(ClickY, ClickX)) & "*", aConnexion, aDescroF10
    
    '-> Tester dle retour
    If strRetour = "" Then GoTo GestError
    
    NumEnregF10 = strRetour
    
    strRetour = ""
    
    '-> On fait un finddata pour recup des data en fonction du NoEnreg
    Set aEnreg = GetData(aConnexion, aConnexion.Ident, aObj.Table_Correspondance, NumEnregF10, aObj.Num_Field, False, aDescroF10)
    
    For Each aField In aEnreg.Fields
        If aField.Field = aObj.Num_Field Then
            '-> On charge la valeur choisie dans la liste des conditions
            Me.MSFlexGrid2.TextMatrix(Me.MSFlexGrid2.Row, Me.MSFlexGrid2.Col) = aField.ZonData
            Me.MSFlexGrid2.CellAlignment = flexAlignLeftCenter '-> on aligne a gauche en l'etat
        End If
    Next
    Me.MSFlexGrid2.Enabled = True
    strRetour = ""
    
End If

GestError:
    Me.MSFlexGrid2.Enabled = True
    Me.Enabled = True
    Screen.MousePointer = 0
    
End Sub
