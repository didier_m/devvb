VERSION 5.00
Begin VB.Form frmConditionsHorsIndex 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   1755
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5940
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1755
   ScaleWidth      =   5940
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   1695
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5895
      Begin VB.CommandButton Command2 
         Height          =   615
         Left            =   4560
         Picture         =   "frmConditionsHorsIndex.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   960
         Width           =   615
      End
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   5160
         Picture         =   "frmConditionsHorsIndex.frx":0CCA
         ScaleHeight     =   240
         ScaleWidth      =   240
         TabIndex        =   7
         Top             =   600
         Width           =   240
      End
      Begin VB.CommandButton Command1 
         Height          =   615
         Left            =   5160
         Picture         =   "frmConditionsHorsIndex.frx":1054
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   960
         Width           =   615
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   5160
         Picture         =   "frmConditionsHorsIndex.frx":1D1E
         ScaleHeight     =   240
         ScaleWidth      =   240
         TabIndex        =   5
         Top             =   240
         Width           =   240
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   1320
         TabIndex        =   3
         Top             =   600
         Width           =   3735
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   1320
         TabIndex        =   1
         Top             =   240
         Width           =   3735
      End
      Begin VB.Label Label2 
         Caption         =   "Label1"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "frmConditionsHorsIndex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Champ de recherche
Public RecField As String
'-> Type du champ
Public TypeField As String





Private Sub Command1_Click()
'---> On valide les conditions au niveau du champ

If Trim(UCase$(Me.TypeField)) <> "DATE" Then
    '-> On consitue la liste des conditions
    aDescro.Fields(Me.RecField).ZonLock = "HORS" & "�" & Trim(Me.Text1.Text) & "�" & Trim(Me.Text2.Text) & "�"
Else
    '-> On reformatte les dates
    aDescro.Fields(Me.RecField).ZonLock = "HORS" & "�" & Entry(3, Me.Text1.Text, "/")
    aDescro.Fields(Me.RecField).ZonLock = aDescro.Fields(Me.RecField).ZonLock & Entry(2, Me.Text1.Text, "/")
    aDescro.Fields(Me.RecField).ZonLock = aDescro.Fields(Me.RecField).ZonLock & Entry(1, Me.Text1.Text, "/") & "�"
    aDescro.Fields(Me.RecField).ZonLock = aDescro.Fields(Me.RecField).ZonLock & Entry(3, Me.Text2.Text, "/")
    aDescro.Fields(Me.RecField).ZonLock = aDescro.Fields(Me.RecField).ZonLock & Entry(2, Me.Text2.Text, "/")
    aDescro.Fields(Me.RecField).ZonLock = aDescro.Fields(Me.RecField).ZonLock & Entry(1, Me.Text2.Text, "/") & "�"
End If

'-> On sort et on met a jour le listview des conditions hors index
frmCompoRequete.ListView3.SelectedItem.SubItems(4) = Me.Text1.Text
frmCompoRequete.ListView3.SelectedItem.SubItems(5) = Me.Text2.Text

'-> On ferme
Unload Me

End Sub

Private Sub Command2_Click()
'---> On supprime les conditions sur le champ
aDescro.Fields(Me.RecField).ZonLock = ""
'-> On remet les textes a blanc
Me.Text1.Text = ""
Me.Text2.Text = ""
End Sub



Private Sub Form_Load()
'-> On charge les libelle
Me.Label1.Caption = GetMesseprog("REQUETEUR", "20")
Me.Label2.Caption = GetMesseprog("REQUETEUR", "21")
Me.Caption = GetMesseprog("REQUETEUR", "65")
End Sub

Private Sub Picture2_Click()
'-> On lance une recherche sur les valeurs
LoadSearch Me.Text2
End Sub
Private Sub Picture1_Click()
'-> On lance une recherche sur les valeurs
LoadSearch Me.Text1
End Sub

Private Sub LoadSearch(aText As TextBox)
'---> Envoi d'un f10 sur un champ
Dim aObj As B3DObject
Dim aBuffer As B3DBuffer
Dim Ok As Boolean
Dim aEnreg As B3DEnreg
Dim aDescroF10 As B3DFnameDescriptif
Dim aField As B3DField
Dim NumEnregF10 As String

On Error GoTo GestError

'-> Bloquer la feuille
Me.Enabled = False
Screen.MousePointer = 11

'-> Teste si champ F10 et ramene un choix de valeurs
Set aObj = Catalogues(Trim(UCase$(Me.RecField)))

Set aBuffer = New B3DBuffer
Set aEnreg = New B3DEnreg

'-> Vider la variable de retour
strRetour = ""

'-> On cree un buffer pour rapatrier les data du f10
If Not FieldTable(aConnexion, aBuffer, aConnexion.Ident, Trim(UCase$(aObj.Num_Field)), Trim(aText.Text)) Then
    'THIERRY
    MsgBox GetMesseprog("REQUETEUR", "23"), vbCritical + vbOKOnly, GetMesseprog("REQUETEUR", "5")
    GoTo GestError
End If
    
Screen.MousePointer = 0

'-> recup d'un descro du fichier de jointure
Set aDescroF10 = GetFnameDescriptif(aConnexion, aConnexion.Ident, aObj.Table_Correspondance)
If aDescroF10 Is Nothing Then
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam
    GoTo GestError
End If

'-> On affiche le resultat dans un browse
ShowFieldTableBrowse aBuffer, GetMesseprog("REQUETEUR", "24") & " " & Trim(aText.Text) & "*", aConnexion, aDescroF10

'-> Tester dle retour
If strRetour = "" Then GoTo GestError

NumEnregF10 = strRetour

strRetour = ""

'-> On fait un finddata pour recup des data en fonction du NoEnreg
Set aEnreg = GetData(aConnexion, aConnexion.Ident, aObj.Table_Correspondance, NumEnregF10, aObj.Num_Field, False, aDescroF10)

For Each aField In aEnreg.Fields
    If aField.Field = aObj.Num_Field Then
        '-> On charge la valeur choisie dans la liste des conditions
        aText.Text = aField.ZonData
    End If
Next
strRetour = ""


GestError:
    Me.Enabled = True
    Screen.MousePointer = 0
    
End Sub


