VERSION 5.00
Begin VB.Form frmParamVue 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   1290
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6165
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1290
   ScaleWidth      =   6165
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   1215
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6135
      Begin VB.TextBox txtComment 
         Height          =   285
         Left            =   1200
         TabIndex        =   4
         Top             =   720
         Width           =   4815
      End
      Begin VB.TextBox txtNomVue 
         Height          =   285
         Left            =   1560
         TabIndex        =   2
         Top             =   240
         Width           =   4455
      End
      Begin VB.Label Label2 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   720
         Width           =   1215
      End
      Begin VB.Label Label1 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmParamVue"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



Private Sub Form_Load()
Me.Caption = GetMesseprog("REQUETEUR", "53")
Me.Label1.Caption = GetMesseprog("REQUETEUR", "54")
Me.Label2.Caption = GetMesseprog("REQUETEUR", "55")
End Sub

Private Sub txtComment_KeyPress(KeyAscii As Integer)
'---> Permet de valider le pmarametrage de la vue

If KeyAscii = 13 Then
    strRetour = Trim(UCase$(Me.txtNomVue.Text)) & Chr(0) & Trim(UCase$(Me.txtComment.Text))
    Unload Me
End If

End Sub

Private Sub txtNomVue_KeyPress(KeyAscii As Integer)
'---> Validation sur la zone nom de la requete
If KeyAscii = 13 Then
    Me.txtComment.SetFocus
End If

End Sub
