VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmRequeteur 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Choix d'un fichier pour la requ�te"
   ClientHeight    =   5670
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4695
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5670
   ScaleWidth      =   4695
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   1800
      Picture         =   "frmRequeteur.frx":0000
      ScaleHeight     =   240
      ScaleWidth      =   240
      TabIndex        =   2
      Top             =   960
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   720
      Picture         =   "frmRequeteur.frx":014A
      ScaleHeight     =   240
      ScaleWidth      =   240
      TabIndex        =   1
      Top             =   1080
      Visible         =   0   'False
      Width           =   240
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   5655
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   9975
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Image Image1 
      Height          =   240
      Left            =   1800
      Picture         =   "frmRequeteur.frx":048C
      Top             =   2760
      Width           =   240
   End
End
Attribute VB_Name = "frmRequeteur"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub ListView1_DblClick()
'---> Choix d'un fichier pour la requete
Dim Ligne As String
Dim OrdreOk As Boolean
Dim aBuffer As B3DBuffer
Dim i As Integer
Dim Fonction As String

'-> Acces au descriptif du fichier selectionn�
Set aDescro = GetFnameDescriptif(aConnexion, aConnexion.Ident, Trim(UCase$(Me.ListView1.SelectedItem.Text)))
If aDescro Is Nothing Then
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam
    Exit Sub
End If

ReqFName = Trim(UCase$(Me.ListView1.SelectedItem.Text))

'-> teste si fichier a index direct pour charger les indexes
If aDescro.IndexDirect = "" Then

    '-> On verifie si multi indexes
    '-> Ligne de cde B3D
    Ligne = "B3D_SCHEMA_DES~1~0~" & Trim(Me.ListView1.SelectedItem.Text)
    
    '-> Initialisation pour demande
    If Not aConnexion.InitDmd(True) Then
        '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
        MsgBox "Impossible d'initialiser une demande..." & Chr(13) & "Fin du traitement.", vbExclamation  'MESSPROG
        Exit Sub
    End If
    
    '-> On envoie une demande de recup des fichiers du catalogue
    '-> On lance la fonction d'analyse des locks brul�es
    OrdreOk = aConnexion.PutInstruction(Ligne, True)
    
    '-> Verifie si on a bien envoy� l'instruction
    If Not OrdreOk Then
        '-> Positionner le code Erreur
        MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
        End
    End If
    
    '-> On envoie l'ordre de lecture des instructions
    Set aBuffer = New B3DBuffer
    If Not aConnexion.SendDemande(aBuffer) Then
        '-> Positionner le code Erreur
        aConnexion.ErrorNumber = "100000"
        aConnexion.ErrorLibel = "Error sending exchange"
        aConnexion.ErrorParam = "LoadAgentDOG"
        End
    End If
    
    '-> Recup de la 1ere ligne
    Ligne = aBuffer.Lignes(1)
    
    If Entry(2, Ligne, "~") <> "OK" Then
        MsgBox "Erreur dans l'acc�s au sch�ma...", vbExclamation  'MESSPROG
        End
    End If
    
    '-> On parcours les lignes pour creer la list des indexes
    '-> On parcours les lignes retourn�es
    frmIndex.MSFlexGrid1.Row = 0
    frmIndex.MSFlexGrid1.Col = 0
    For i = 2 To aBuffer.Lignes.Count
        
        '-> recup de la ligne
        Fonction = Entry(1, aBuffer.Lignes(i), "~")
        Ligne = Entry(2, aBuffer.Lignes(i), "~")
        Fonction = Entry(3, Fonction, "_")
        
        Select Case Fonction
                
            Case "INAME"
                '-> On cree une ligne pour la data en cours
                frmIndex.MSFlexGrid1.AddItem Entry(2, Ligne, "�")
                frmIndex.MSFlexGrid1.Col = 1
                frmIndex.MSFlexGrid1.Row = frmIndex.MSFlexGrid1.Rows - 1
                frmIndex.MSFlexGrid1.RowHeight(frmIndex.MSFlexGrid1.Row) = Me.ScaleY(16, 3, 1)
                frmIndex.MSFlexGrid1.TextMatrix(frmIndex.MSFlexGrid1.Row, 1) = Entry(4, Ligne, "�")
                
            Case "IFIELD"
                '-> Teste si champ d'index
                If CBool(Entry(5, Ligne, "�")) = True Then
                    frmIndex.MSFlexGrid1.Col = 2
                    Set frmIndex.MSFlexGrid1.CellPicture = Me.Picture1.Picture
                    frmIndex.MSFlexGrid1.CellPictureAlignment = 4
                End If
                
                '-> recup du champ d'index
                If aDescro.Fields(Entry(4, Ligne, "�")).ZonData = "" Then
                    aDescro.Fields(Entry(4, Ligne, "�")).ZonData = Entry(2, Ligne, "�") & "�" & Entry(3, Ligne, "�") & "�" & Entry(5, Ligne, "�")
                Else
                    aDescro.Fields(Entry(4, Ligne, "�")).ZonData = aDescro.Fields(Entry(4, Ligne, "�")).ZonData & "�" & Entry(2, Ligne, "�") & "�" & Entry(3, Ligne, "�") & "�" & Entry(5, Ligne, "�")
                End If
                                        
        End Select
            
NextLigne:
    
    Next
    
    '-> On femre la fenetre du choix de fichiers
    Unload Me
    
    '-> Affiche la fenetre de choix des index
    frmIndex.Show vbModal
    
    Exit Sub
    
Else
    '-> Fichier a index direct
    LoadFields
    '-> On femre la fenetre du choix de fichiers
    Unload Me
    '-> On affiche directement le choix des champs
    frmFields.Show
    
    Exit Sub

End If

'-> Efface la fenetre de choix des fichiers
Unload Me


End Sub
