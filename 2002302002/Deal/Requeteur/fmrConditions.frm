VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmConditions 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Choix des conditions � appliquer"
   ClientHeight    =   4245
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6750
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4245
   ScaleWidth      =   6750
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Caption         =   "Supprimer les Conditions"
      Height          =   375
      Left            =   0
      TabIndex        =   5
      Top             =   3840
      Width           =   2295
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   2280
      TabIndex        =   4
      Top             =   3840
      Width           =   4455
   End
   Begin VB.Frame Frame1 
      Height          =   3855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6735
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   1560
         TabIndex        =   3
         Top             =   1200
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   840
         TabIndex        =   2
         Top             =   2280
         Visible         =   0   'False
         Width           =   2535
      End
      Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
         Height          =   3495
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   6540
         _ExtentX        =   11536
         _ExtentY        =   6165
         _Version        =   393216
         Rows            =   14
         Cols            =   3
         FixedCols       =   0
         TextStyleFixed  =   1
      End
   End
End
Attribute VB_Name = "frmConditions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public NoField As String


Private Sub Combo1_Click()
Me.MSFlexGrid1.Text = Me.Combo1.Text
Me.Combo1.Visible = False
End Sub

Private Sub Combo1_KeyPress(KeyAscii As Integer)
If KeyAscii = 27 Then Me.Combo1.Visible = False
End Sub

Private Sub Command1_Click()
'-> On stocke les conditions au niveau du champ en cours
Dim i As Integer

aDescro.Fields(Me.NoField).ZonLock = ""

For i = 1 To Me.MSFlexGrid1.Rows - 1
    '-> test si fin des conditions
    If Me.MSFlexGrid1.TextMatrix(i, 0) = "" Then Exit For
    '-> On consitue la liste des conditions
    If aDescro.Fields(Me.NoField).ZonLock = "" Then
        aDescro.Fields(Me.NoField).ZonLock = Me.MSFlexGrid1.TextMatrix(i, 0) & "�" & Me.MSFlexGrid1.TextMatrix(i, 1) & "�" & Me.MSFlexGrid1.TextMatrix(i, 2)
    Else
        aDescro.Fields(Me.NoField).ZonLock = "�" & Me.MSFlexGrid1.TextMatrix(i, 0) & "�" & Me.MSFlexGrid1.TextMatrix(i, 1) & "�" & Me.MSFlexGrid1.TextMatrix(i, 2)
    End If
Next

Unload Me

End Sub

Private Sub Command2_Click()
Dim i As Integer

For i = 1 To Me.MSFlexGrid1.Rows - 1
    Me.MSFlexGrid1.TextMatrix(i, 0) = ""
    Me.MSFlexGrid1.TextMatrix(i, 1) = ""
    Me.MSFlexGrid1.TextMatrix(i, 2) = ""
Next

'-> On reprecise au niveau du champ qu'il n'y a plus de cdt
Set frmFields.MSFlexGrid1.CellPicture = frmFields.Picture1.Picture

End Sub

Private Sub Form_Load()
'-> prechargement et formattage du flex
Dim i As Integer

'-> Entetes de colonnes
Me.MSFlexGrid1.TextMatrix(0, 0) = "Type"
Me.MSFlexGrid1.TextMatrix(0, 1) = "Val.Mini"
Me.MSFlexGrid1.TextMatrix(0, 2) = "Val.Maxi"

'-> On formatte la largeur des colonnes
Me.MSFlexGrid1.ColWidth(0) = 1500
Me.MSFlexGrid1.ColWidth(1) = 2500
Me.MSFlexGrid1.ColWidth(2) = 2500

Me.Combo1.AddItem "1  - Egal"   '-> 'EQ'
Me.Combo1.AddItem "2  - Incluant"    '-> 'INC'
Me.Combo1.AddItem "3  - N'incluant pas"    '-> 'NIN'
Me.Combo1.AddItem "4  - Diff�rent de"    '-> 'DIF'
Me.Combo1.AddItem "5  - Inf�rieur ou �gal"     '-> 'LE'
Me.Combo1.AddItem "6  - Sup�rieur ou �gal"     '-> 'GE'
Me.Combo1.AddItem "7  - Strictement Inf�rieur" '-> 'LT'
Me.Combo1.AddItem "8  - Strictement Sup�rieur" '->'GT'
Me.Combo1.AddItem "9  - Commence par"   '-> BEG
Me.Combo1.AddItem "10 - Comprend"   '-> 'MAT'
'-> On rempli le flex des types de cdt
For i = 1 To Me.MSFlexGrid1.Rows - 1
    Me.MSFlexGrid1.RowHeight(i) = Me.Combo1.Height
Next

End Sub

Private Sub Form_Unload(Cancel As Integer)

If aDescro.Fields(Me.NoField).ZonLock = "" Then Set frmFields.MSFlexGrid1.CellPicture = frmFields.Picture1.Picture

End Sub

Private Sub MSFlexGrid1_Click()
'-> Selon la colonne on se place en saisie ou choix de type de cdt


If ClickX = 0 Then
    '-> On ouvre la combo
    Me.Combo1.Top = Me.MSFlexGrid1.CellTop + Me.MSFlexGrid1.Top
    Me.Combo1.Left = Me.MSFlexGrid1.CellLeft + Me.MSFlexGrid1.Left
    Me.Combo1.Width = Me.MSFlexGrid1.CellWidth
    Me.Combo1.Visible = True
End If

If ClickX > 0 Then
    '-> On se place en saisie
    Me.Text1.Text = ""
    '-> On place le texte sur la cellule
    Me.Text1.Top = Me.MSFlexGrid1.CellTop + Me.MSFlexGrid1.Top
    Me.Text1.Height = Me.MSFlexGrid1.CellHeight
    Me.Text1.Left = Me.MSFlexGrid1.CellLeft + Me.MSFlexGrid1.Left
    Me.Text1.Width = Me.MSFlexGrid1.CellWidth
    Me.Text1.Visible = True
End If

End Sub

Private Sub MSFlexGrid1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'-> Get de la colonne et de la ligne
ClickX = GetCol(Me.MSFlexGrid1, x)
ClickY = GetRow(Me.MSFlexGrid1, y)
End Sub



Private Sub Text1_KeyPress(KeyAscii As Integer)
If KeyAscii = 27 Then Me.Text1.Visible = False

If KeyAscii = 13 Then
    Me.MSFlexGrid1.Text = Me.Text1.Text
    Me.Text1.Visible = False
End If

End Sub
