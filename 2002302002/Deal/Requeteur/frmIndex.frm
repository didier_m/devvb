VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmIndex 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Choix d'un index "
   ClientHeight    =   4605
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4830
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4605
   ScaleWidth      =   4830
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   4575
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4815
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   1200
         Picture         =   "frmIndex.frx":0000
         ScaleHeight     =   240
         ScaleWidth      =   240
         TabIndex        =   3
         Top             =   1560
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   2280
         Picture         =   "frmIndex.frx":0342
         ScaleHeight     =   240
         ScaleWidth      =   240
         TabIndex        =   2
         Top             =   1440
         Visible         =   0   'False
         Width           =   240
      End
      Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
         Height          =   4335
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   7646
         _Version        =   393216
         Rows            =   1
         Cols            =   3
      End
   End
End
Attribute VB_Name = "frmIndex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
'-> Formattage du msflexgrid


'-> Entetes de colonnes
Me.MSFlexGrid1.TextMatrix(0, 0) = "No Index"
Me.MSFlexGrid1.TextMatrix(0, 1) = "Désignation"
Me.MSFlexGrid1.TextMatrix(0, 2) = "Cumul"

'-> On formatte la largeur des colonnes
Me.MSFlexGrid1.ColWidth(0) = 1000
Me.MSFlexGrid1.ColWidth(1) = 2000
Me.MSFlexGrid1.ColWidth(2) = 1000

End Sub

Private Sub MSFlexGrid1_Click()
'-> On coche si cumul

'-> On ne change l'image que si click sur colonne 2
If ClickX = 2 Then
    '-> On change d'image selon que la case est selectionnée ou pas
    If Me.MSFlexGrid1.CellPicture = Me.Picture1.Picture Then
        Set Me.MSFlexGrid1.CellPicture = Me.Picture2.Picture
    Else
        Set Me.MSFlexGrid1.CellPicture = Me.Picture1.Picture
    End If
End If

End Sub

Private Sub MSFlexGrid1_DblClick()
'---> Choix definitif d'un index

'-> Si on coche pour cumul : on valide pas
If ClickX = 2 Then Exit Sub

'-> On charge les fields
Me.MSFlexGrid1.Col = 0

'-> On garde l'index choisi
ReqIName = Me.MSFlexGrid1.TextMatrix(ClickY, 0)
ReqCumul = "0"
Me.MSFlexGrid1.Col = 2
Me.MSFlexGrid1.Row = ClickY
If Me.MSFlexGrid1.CellPicture <> 0 Then
    If Me.MSFlexGrid1.CellPicture = Me.Picture2.Picture Then ReqCumul = "1"
End If

'-> on charge les fields dans le fenetre de choix des champs
LoadFields

'-> On ferme les indexs
Unload Me
'-> On affiche la fenetre de choix des champs
frmFields.Show

End Sub

Private Sub MSFlexGrid1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

'-> Get de la colonne et de la ligne
ClickX = GetCol(Me.MSFlexGrid1, x)
ClickY = GetRow(Me.MSFlexGrid1, y)

End Sub
