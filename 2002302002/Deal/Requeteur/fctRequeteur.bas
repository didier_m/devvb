Attribute VB_Name = "fctRequeteur"
Option Explicit

'-> Connexion en cours
Public aConnexion As B3DConnexion

'-> Objet de navigation
Public Naviga As B3DNavigation

'-> fichier de requete pass� en parametre
Public GloFname As String
Public GloIname As String
Public GloCumul As Boolean

'-> permet de stocker l'index selectionn�
Public IndexRequete As String
'-> Permet de stocker le fait que l'index de requete est un index cumul ou pas
Public IndexCumul As Boolean
'-> Liste des groupes
Public ListeGroupes As String
'-> Pour requetes
Public strReqDet As String
Public strReqCum As String
Public ListeChamps As String
Public ListeChampsCum As String
Public ListeConditionsHorsIndex As String

'-> Coordonn�es du curseur
Public ClickX As Integer
Public ClickY As Integer

Public Type POINTAPI
        x As Long
        y As Long
End Type

Public aDescro As B3DFnameDescriptif

'-> recup de la position du curseur souris
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long

'-> Pour garder en memoire l'index choisi et le fichier
Public ReqFName As String
Public ReqIName As String
Public ReqCumul As String


'-> API de temporisation
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
'-> API Pour gestion du temps
Public Declare Function GetTickCount& Lib "kernel32" ()


Sub Main()
'---> analyse de la ligne de command

'------------------------------------------------------------------------------------------
'-- PARAMETRAGE POUR LANCER REQUETEUR  ----------------------------------------------------
'- 1 : soit on ne precise rien -> Envoi du requeteur avec tous les fichiers
'- 2 : soit on precise un fichier ( FNAME=MONFICHIER ) -> Envoi du requeteur en blocage sur le fichier specifie
'- 3 : soit on precise une vue ( VUE=MAVUE.VUEREQ ) -> Envoi du requeteur sur la vue donn�e et rien d'autre
'------------------------------------------------------------------------------------------

Dim ParamFile As String
Dim ZonCharge As String
Dim OrdreOk As Boolean
Dim aBuffer As B3DBuffer
Dim Ligne As String
Dim i As Integer
Dim aIt As ListItem
Dim Param As String

'-> Tester la ligne de commande
If Trim(Command$) = "" Then End

'-> R�cup�rer les param�tres
ParamFile = Entry(1, Command$, "�")
ZonCharge = Entry(2, Command$, "�")

'-> Initialiser la connexion
Set aConnexion = New B3DConnexion

'-> Charger son param�trage
If Not aConnexion.ReadParamFormFile(ParamFile) Then End
        
'-> Poser le nom du programme
InitialiseB3D aConnexion, "REQUETEUR", False, False

'-> R�cup�rer le catalogue
If Not GetIdentCatalogue(aConnexion, aConnexion.Ident, "") Then
    '-> Afficher un message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbCritical + vbOKOnly, "Error"
    '-> Fin du programme
    End
End If

'-> Teste si on a pass� une vue existante en parametres
If Trim(UCase$(Entry(1, Entry(1, ZonCharge, "|"), "="))) = "VUE" Then
    '-> On envoie directement la requete
    '-> Affichage de la tempo
    frmWait.Label1 = GetMesseprog("REQUETEUR", "25") & " " & Trim(UCase$(Entry(2, Entry(1, ZonCharge, "|"), "=")))
    frmWait.Show
    '-> On ouvre la vue si existe
    If Dir$(aConnexion.ApplicationDir & aConnexion.Ident & "\Maq\" & Trim(UCase$(Entry(2, Entry(1, ZonCharge, "|"), "=")))) <> "" Then
        OpenVue aConnexion.ApplicationDir & aConnexion.Ident & "\Maq\" & Trim(UCase$(Entry(2, Entry(1, ZonCharge, "|"), "=")))
    Else
        MsgBox GetMesseprog("REQUETEUR", "27"), vbExclamation + vbOKOnly, GetMesseprog("REQUETEUR", "26")
        End
    End If
    '-> On decharge la tempo
    Unload frmWait
    '-> affichage de la requete
    frmVueDetail.Show vbModal
    '-> Fin de prog
    End
End If

'-> Affichage de la tempo
frmWait.Label1 = GetMesseprog("REQUETEUR", "28")
frmWait.Show

'-> teste si on a un fichier en parametres et envoi le requeteur sur ce fichier
If Trim(UCase$(Entry(1, Entry(1, ZonCharge, "|"), "="))) = "FNAME" Then
    '-> recup de la ligne de param
    Param = Trim(UCase$(Entry(2, Entry(1, ZonCharge, "|"), "=")))
    '-> Fichier passe en argument
    GloFname = Format(Entry(1, Param, "~"), "0000")
    '-> Index pass� en argument
    If Entry(2, Param, "~") <> "" Then
        GloIname = Format(Entry(2, Param, "~"), "000")
    End If
        
    '-> Top cumul passe en argument
    If Entry(3, Param, "~") = "-1" Then
        GloCumul = True
    Else
        GloCumul = False
    End If
    
    '-> On bloque l'acces a la selection du fichier
    frmCompoRequete.frame1.Enabled = False
    
End If

'-> Initialisation pour demande
If Not aConnexion.InitDmd(True) Then
    '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
    MsgBox GetMesseprog("REQUETEUR", "6") & Chr(13) & GetMesseprog("REQUETEUR", "7"), vbExclamation
    Exit Sub
End If

'-> On lance la fonction d'analyse des locks brul�es
OrdreOk = aConnexion.PutInstruction("B3D_SCHEMA_LIST~1", True)

'-> Verifie si on a bien envoy� l'instruction
If Not OrdreOk Then
    '-> Positionner le code Erreur
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    End
End If

'-> On envoie l'ordre de lecture des instructions
Set aBuffer = New B3DBuffer
If Not aConnexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    aConnexion.ErrorNumber = "100000"
    aConnexion.ErrorLibel = "Error sending exchange"
    aConnexion.ErrorParam = "LoadAgentDOG"
    End
End If

'-> Recup de la 1ere ligne
Ligne = aBuffer.Lignes(1)

If Entry(2, Entry(1, Ligne, "�"), "~") <> "OK" Then
    MsgBox GetMesseprog("REQUETEUR", "29"), vbExclamation
    End
End If

frmCompoRequete.Frame2.Enabled = False
frmCompoRequete.Frame3.Enabled = False
frmCompoRequete.Frame4.Enabled = False
frmCompoRequete.Frame5.Enabled = False

'-> Analyse du retour des locks repertori�s
For i = 2 To aBuffer.Lignes.Count
    '-> Recup de la ligne
    Ligne = aBuffer.Lignes(i)
    '-> On cree le listview des tables
    frmCompoRequete.Combo1.AddItem Entry(1, Ligne, "�") & " - " & Entry(2, Ligne, "�")
Next

'-> Si on a passe un fichier en parametres on le charge directement
If GloFname <> "" Then frmCompoRequete.LoadFname

'-> Fin de la tempo
Unload frmWait

'-> Affichage de la fenetre de composition de requete
frmCompoRequete.Show

        
End Sub


Public Function GetRow(ByRef Flex As MSFlexGrid, ByVal y As Long) As Integer
    '---> permet de recupere la ligne d'un flex en fonction du click souris
    Dim i As Long
    
    For i = 0 To Flex.Rows - 1
        If y >= Flex.RowPos(i) And y <= Flex.RowPos(i) + Flex.RowHeight(i) Then
            GetRow = i
            Exit Function
        End If
    Next
End Function

Public Function GetCol(ByRef Flex As MSFlexGrid, ByVal x As Long) As Integer
    '---> permet de recupere la colonne d'un flex en fonction du click souris
    Dim i As Long
    
    For i = 0 To Flex.Cols - 1
        If x >= Flex.ColPos(i) And x <= Flex.ColPos(i) + Flex.ColWidth(i) Then
            GetCol = i
            Exit Function
        End If
    Next
End Function

Public Sub OpenVue(FicParam As String)
'---> permet d'ouvrir une vue existante
Dim lpBuffer As String
Dim Res As Long


On Error GoTo GestError

'-> oN RECUPERE LES PARAMETRAGES
'-> Recup du fname  de la requete
lpBuffer = Space$(500)
Res = GetPrivateProfileString("PARAMETRAGE", "FNAME", "Erreur", lpBuffer, Len(lpBuffer), FicParam)
ReqFName = Mid$(lpBuffer, 1, Res)

'-> recup du INAME
lpBuffer = Space$(500)
Res = GetPrivateProfileString("PARAMETRAGE", "INAME", "Erreur", lpBuffer, Len(lpBuffer), FicParam)
ReqIName = Mid$(lpBuffer, 1, Res)

'-> recup de la, requete detail
lpBuffer = Space$(500)
Res = GetPrivateProfileString("PARAMETRAGE", "REQDET", "Erreur", lpBuffer, Len(lpBuffer), FicParam)
strReqDet = Mid$(lpBuffer, 1, Res)

'-> recup de la, requete cumul
lpBuffer = Space$(500)
Res = GetPrivateProfileString("PARAMETRAGE", "REQCUM", "Erreur", lpBuffer, Len(lpBuffer), FicParam)
strReqCum = Mid$(lpBuffer, 1, Res)

'-> recup de la liste des champs
lpBuffer = Space$(500)
Res = GetPrivateProfileString("PARAMETRAGE", "LISTECHAMPS", "Erreur", lpBuffer, Len(lpBuffer), FicParam)
ListeChamps = Mid$(lpBuffer, 1, Res)

'-> recup de la liste des champs cum
lpBuffer = Space$(500)
Res = GetPrivateProfileString("PARAMETRAGE", "LISTECHAMPSCUM", "Erreur", lpBuffer, Len(lpBuffer), FicParam)
ListeChampsCum = Mid$(lpBuffer, 1, Res)

'-> recup des conditions hors index
lpBuffer = Space$(500)
Res = GetPrivateProfileString("PARAMETRAGE", "CDTHINDEX", "Erreur", lpBuffer, Len(lpBuffer), FicParam)
ListeConditionsHorsIndex = Mid$(lpBuffer, 1, Res)

'-> Acces au descriptif du fichier selectionn�
Set aDescro = GetFnameDescriptif(aConnexion, aConnexion.Ident, ReqFName)
If aDescro Is Nothing Then
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam
    Exit Sub
End If

'-> teste si requete de cumul ou pas
If strReqCum = "" Then
    '-> On lance la requete gener�e
    Set Naviga = InitNaviga(aConnexion, aConnexion.Ident, ReqFName, "BUFFER_DETAIL", strReqDet, ListeConditionsHorsIndex, ListeChamps, "", 500, 40, aDescro)
Else
    '-> On est sur une requete de cumul
    frmVueDetail.Cumul = True
    '-> On stocke la requete cumul pour acces au detail
    frmVueDetail.strReqDet = strReqDet
    '-> On envoie la liste des champs
    frmVueDetail.ListeChamps = ListeChamps
    '-> On lance la requete gener�e
    Set Naviga = InitNaviga(aConnexion, aConnexion.Ident, ReqFName & "�" & ReqIName, "BUFFER_CUMUL", strReqCum, "", ListeChampsCum, "", 500, 40, aDescro)
End If

'-> Tester le retour de navigation
If Naviga Is Nothing Then
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter la fonction
    GoTo GestError
End If

'-> Charger le browse des salaires
CreateListViewNaviga Naviga, frmVueDetail.ListView1


Exit Sub

GestError:
    MsgBox GetMesseprog("REQUETEUR", "30"), vbExclamation + vbOKOnly, GetMesseprog("REQUETEUR", "5")
    Exit Sub
    
End Sub


