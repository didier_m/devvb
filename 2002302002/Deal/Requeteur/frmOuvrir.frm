VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmOuvrir 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4005
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6495
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4005
   ScaleWidth      =   6495
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   1680
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOuvrir.frx":0000
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3975
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   7011
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Menu mnuPopUp 
      Caption         =   "PopUp"
      Visible         =   0   'False
      Begin VB.Menu mnuParam 
         Caption         =   "Afficher le param�trage"
      End
      Begin VB.Menu mnuSep0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSupprimer 
         Caption         =   "Supprimer la vue"
      End
   End
End
Attribute VB_Name = "frmOuvrir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
Me.Caption = GetMesseprog("REQUETEUR", "52")
End Sub

Private Sub Form_Resize()
'-> Retaille de l'ecran
Dim aRect As RECT
'-> recup de la taille de l'ecran
GetClientRect Me.hwnd, aRect
'-> On affecte le listeview
Me.ListView1.Width = Me.ScaleX(aRect.Right, 3, 1)
Me.ListView1.Height = Me.ScaleX(aRect.Bottom, 3, 1)

End Sub

Private Sub ListView1_DblClick()
'-> On retourne la selection
strRetour = Trim(UCase$(Me.ListView1.SelectedItem.Text))
Unload Me
End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
'-> Propose d'ouvrir le fichier de parametrage

If Button <> 2 Then Exit Sub

strRetour = ""

'-> affiche le popup
Me.PopupMenu Me.mnuPopUp

'-> envoi la visu du parametrage
Select Case strRetour
    
    Case "DELETE"
        '-> permet la suupression d'une vue
        Call DeleteVue
    
    Case "PARAM"
        '-> On charge les paraetrages et on affiche la visu
        LoadParams
    
End Select

strRetour = ""

End Sub

Private Sub LoadParams()
'-> permet d'afficher les param�trages d'une vue donn�e
Dim lpBuffer As String
Dim Res As Long

'-> ON RECUPERE LES PARAMETRAGES

On Error GoTo GestError

Me.Enabled = False
Screen.MousePointer = 11

'-> Recup du fname  de la requete
lpBuffer = Space$(500)
Res = GetPrivateProfileString("PARAMETRAGE", "FNAME", "Erreur", lpBuffer, Len(lpBuffer), Trim(UCase$(aConnexion.ApplicationDir & aConnexion.Ident & "\Maq\" & Me.ListView1.SelectedItem.Text)) & ".VUEREQ")
frmVisuParam.Text1 = Mid$(lpBuffer, 1, Res)

'-> Recup du commentaire  de la requete
lpBuffer = Space$(500)
Res = GetPrivateProfileString("PARAMETRAGE", "COMMENT", "Erreur", lpBuffer, Len(lpBuffer), Trim(UCase$(aConnexion.ApplicationDir & aConnexion.Ident & "\Maq\" & Me.ListView1.SelectedItem.Text)) & ".VUEREQ")
frmVisuParam.txtComment = Mid$(lpBuffer, 1, Res)

'-> recup du INAME
lpBuffer = Space$(500)
Res = GetPrivateProfileString("PARAMETRAGE", "INAME", "Erreur", lpBuffer, Len(lpBuffer), Trim(UCase$(aConnexion.ApplicationDir & aConnexion.Ident & "\Maq\" & Me.ListView1.SelectedItem.Text)) & ".VUEREQ")
frmVisuParam.Text2 = Mid$(lpBuffer, 1, Res)

'-> recup de la, requete detail
lpBuffer = Space$(500)
Res = GetPrivateProfileString("PARAMETRAGE", "REQDET", "Erreur", lpBuffer, Len(lpBuffer), Trim(UCase$(aConnexion.ApplicationDir & aConnexion.Ident & "\Maq\" & Me.ListView1.SelectedItem.Text)) & ".VUEREQ")
frmVisuParam.Text3 = Mid$(lpBuffer, 1, Res)

'-> recup de la, requete cumul
lpBuffer = Space$(500)
Res = GetPrivateProfileString("PARAMETRAGE", "REQCUM", "Erreur", lpBuffer, Len(lpBuffer), Trim(UCase$(aConnexion.ApplicationDir & aConnexion.Ident & "\Maq\" & Me.ListView1.SelectedItem.Text)) & ".VUEREQ")
frmVisuParam.Text4 = Mid$(lpBuffer, 1, Res)

'-> recup de la liste des champs
lpBuffer = Space$(500)
Res = GetPrivateProfileString("PARAMETRAGE", "LISTECHAMPS", "Erreur", lpBuffer, Len(lpBuffer), Trim(UCase$(aConnexion.ApplicationDir & aConnexion.Ident & "\Maq\" & Me.ListView1.SelectedItem.Text)) & ".VUEREQ")
frmVisuParam.Text5 = Mid$(lpBuffer, 1, Res)

Me.Enabled = True
Screen.MousePointer = 0

'-> On affiche le parametrage
frmVisuParam.Show vbModal

Exit Sub

GestError:
    Me.Enabled = True
    Screen.MousePointer = 0

End Sub

Private Sub mnuParam_Click()
strRetour = "PARAM"
End Sub

Private Sub mnuSupprimer_Click()
strRetour = "DELETE"
End Sub

Private Sub DeleteVue()
'---> permet de supprimer une vue
Dim Rep As VbMsgBoxResult

Rep = MsgBox(GetMesseprog("REQUETEUR", "33") & " " & Me.ListView1.SelectedItem.Text & Chr(13) & GetMesseprog("REQUETEUR", "34"), vbQuestion + vbYesNo, "Suppression de Vue")
If Rep = vbNo Then Exit Sub
'-> Suppression physique
Kill aConnexion.ApplicationDir & aConnexion.Ident & "\Maq\" & Trim(UCase$(Me.ListView1.SelectedItem.Text)) & ".VUEREQ"
'-> Suppression de la vue dans liste
Me.ListView1.ListItems.Remove Me.ListView1.SelectedItem.Index
MsgBox GetMesseprog("REQUETEUR", "31"), vbInformation + vbOKOnly, GetMesseprog("REQUETEUR", "32")
End Sub
