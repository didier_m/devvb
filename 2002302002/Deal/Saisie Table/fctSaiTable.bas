Attribute VB_Name = "fctSaiTable"
Option Explicit

'-> Variable d'�change avec le B3D
Public aConnexion As B3DConnexion

'-> Variable de naviagtion dans les projets
Public aNavigation As B3DNavigation

'-> Collection des buffers associ�s
Public BufferFieldTables As Collection

'-> Indique la table maitresse du programme
Public ListeField As String

'-> Param�trage de la ligne de commande
Public ParamFile As String
Public ZoneCharge As String

'-> Pointeur vers un objet descriptif de la table en cours de travail
Public aDescro As B3DFnameDescriptif

'-> Param�trage
Public pFonction As String
Public pNumEnreg As String

Sub Main()

'-> Param�trage
'Zone charge de 1 -> CREATE/UPDATE
'Zone Charge de 2 -> Fname
'Zone Charge de 3 -> RowId en cas d'UPDATE

Dim aField As B3DField
Dim strFname As String

'-> Analyse de la ligne de commande
If Trim(Command$) = "" Then End

'-> Eclater la ligne de commande
ParamFile = Entry(1, Command$, "�")
ZoneCharge = Entry(2, Command$, "�")

'-> V�rifier si on trouve le fichier de param�trage
If Dir$(ParamFile) = "" Then End

'-> Eclater les zones charges
pFonction = UCase$(Trim(Entry(1, ZoneCharge, "~")))
strFname = Trim(UCase$(Entry(2, ZoneCharge, "~")))
pNumEnreg = Trim(Entry(3, ZoneCharge, "~"))

'-> Cr�ation de la connexion
Set aConnexion = New B3DConnexion

'-> Charger son param�trage
If Not aConnexion.ReadParamFormFile(ParamFile) Then End

'-> V�rifier les zones charges
If pFonction <> "CREATE" And pFonction <> "UPDATE" Then
    MsgBox GetMesseprog("SAITABLE", "1"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    End
End If

'-> Poser le nom du programme
InitialiseB3D aConnexion, "SAI_TABLE", False, False

'-> Intialiser le catalogue
GetIdentCatalogue aConnexion, aConnexion.Ident, ""

'-> R�cup�rer les descriptif de la table
Set aDescro = GetFnameDescriptif(aConnexion, aConnexion.Ident, strFname)

If aDescro Is Nothing Then
    '-> Afficher un message d'erreur
    MsgBox GetMesseprog("SAITABLE", "1"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    '-> Fin du programme
    End
End If
    
'-> Cr�er la liste des champs
For Each aField In aDescro.Fields
    If ListeField = "" Then
        ListeField = aField.Field & "�1"
    Else
        ListeField = ListeField & "�" & aField.Field & "�1"
    End If
Next 'Pour tous les champs

'-> Afficher la feuille de cr�ation ou d'update selon que l'on soit en Create ou update
Select Case pFonction
    Case "CREATE"
        Call CreateB3DEnreg(strFname)
    Case "UPDATE"
        Call UpdateB3DEnreg(strFname)
End Select

End Sub

Private Sub CreateB3DEnreg(strFname As String)

'---> Cette proc�dure sert � cr�er un nouvel enreg pour la table en cours

Dim aEnreg As B3DEnreg
Dim LibError As String

On Error GoTo GestError

'-> Afficher la feuille
frmSaisieTable.Show
DoEvents
LockWindowUpdate frmSaisieTable.ListView1.hwnd

'-> Afficher le texte dans la barre de statuts
frmSaisieTable.StatusBar1.Panels(1).Text = GetMesseprog("SAITABLE", "3")

'-> Bloquer les enreg
frmSaisieTable.Enabled = False
Screen.MousePointer = 11

'-> Positionner le descro
Set frmSaisieTable.aDescro = aDescro

'-> Positionner l'enreg en cours
Set frmSaisieTable.aEnreg = aDescro.GetEnreg

'-> Positionner la fonction en cours
frmSaisieTable.pFonction = "CREATE"

'-> Positionner les valeurs de l'enregistrement en cours
frmSaisieTable.Init

'-> D�bloquer l'�cran
Screen.MousePointer = 0
frmSaisieTable.Enabled = True
LockWindowUpdate 0

'-> Vider la variable de texte
frmSaisieTable.StatusBar1.Panels(1).Text = ""

'-> Afficher la feuille en modal
frmSaisieTable.Show

'-> Quitter la proc�dure
Exit Sub

GestError:
    '-> Fin du programme
    End

End Sub

Private Sub UpdateB3DEnreg(strFname As String)


'---> Cette proc�dure affiche la feuille de gestion de cr�ation

Dim aEnreg As B3DEnreg
Dim LibError As String


On Error GoTo GestError

'-> Afficher la feuille
frmSaisieTable.Show
DoEvents
LockWindowUpdate frmSaisieTable.ListView1.hwnd

'-> Afficher le texte dans la barre de statuts
frmSaisieTable.StatusBar1.Panels(1).Text = GetMesseprog("SAITABLE", "3")

'-> Positionner la fonction en cours
frmSaisieTable.pFonction = "UPDATE"

'-> Bloquer les enreg
frmSaisieTable.Enabled = False
Screen.MousePointer = 11

Rep:
'-> Faire un FindData lock sur l'enreg
Set aEnreg = FindData(aConnexion, aConnexion.Ident, strFname, pNumEnreg, True, False, aDescro, True)

'-> Tester le retour
If aEnreg Is Nothing Then
    '-> Tester si l'enregistrement n'est pas lock� par un autre
    If aConnexion.ErrorNumber = "90006" Then
        '-> v�rifier si le lock est actif
        If Entry(4, aConnexion.ErrorParam, "�") = "0" Then
            '-> Envoyer un B3D unlock
            If UnlockEnreg(aConnexion, strFname, pNumEnreg) Then
                '-> Reprise
                GoTo Rep
            End If
        Else
            '-> Message de lock
            LibError = Catalogues("00058").Fill_Label
            LibError = Replace(LibError, "$#EN#$", pNumEnreg)
            LibError = Replace(LibError, "$#OP#$", Entry(1, aConnexion.ErrorParam, "�"))
            LibError = Replace(LibError, "$#DAT#$", Entry(2, aConnexion.ErrorParam, "�"))
            LibError = Replace(LibError, "$#PRO#$", Entry(3, aConnexion.ErrorParam, "�"))
            MsgBox LibError, vbExclamation + vbOKOnly, "Error"
        End If
        '-> Fin de la fonction
        GoTo GestError
    End If
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter le programme
    GoTo GestError
End If

'-> Positionner le descro
Set frmSaisieTable.aDescro = aDescro

'-> Positionner l'enreg en cours
Set frmSaisieTable.aEnreg = aEnreg

'-> Positionner les valeurs de l'enregistrement en cours
frmSaisieTable.Init

'-> D�bloquer l'�cran
Screen.MousePointer = 0
frmSaisieTable.Enabled = True
LockWindowUpdate 0

'-> Vider la variable de texte
frmSaisieTable.StatusBar1.Panels(1).Text = ""

'-> Afficher la feuille en modal
frmSaisieTable.Show

'-> Quitter la proc�dure
Exit Sub

GestError:
    '-> Fin du programme
    End

End Sub


