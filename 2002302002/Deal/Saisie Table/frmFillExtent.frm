VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmFillExtent 
   BorderStyle     =   5  'Sizable ToolWindow
   ClientHeight    =   7560
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   7200
   Icon            =   "frmFillExtent.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   504
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   480
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   2
      Top             =   7185
      Width           =   7200
      _ExtentX        =   12700
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   1560
      Top             =   720
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFillExtent.frx":0CCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFillExtent.frx":19A4
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   7200
      _ExtentX        =   12700
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   1
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "OK"
            ImageIndex      =   1
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   6015
      Left            =   0
      TabIndex        =   0
      Top             =   720
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   10610
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "POPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuCreate 
         Caption         =   ""
      End
      Begin VB.Menu mnuEdit 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDel 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmFillExtent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Pointeur vers le champ en cours
Private MyField As B3DField
'-> Indique si pour ce cahlmp il ya une table jointe
Private IsJoinTable As Boolean
'-> S'il ya une table jointe : indique le champ maitre
Private FnameJoinField As String

Private Sub Form_Load()

'-> Libell�s des menus
Me.mnuCreate.Caption = GetMesseprog("MENU", "1")
Me.mnuEdit.Caption = GetMesseprog("MENU", "3")
Me.mnuDel.Caption = GetMesseprog("MENU", "2")

End Sub

Public Sub Init(aField As B3DField)

'---> Cette proc�dure charge la liste des extent pour un champ donn�

Dim aObject As B3DObject

'-> Titre de la feuille
Me.Caption = GetMesseprog("FRMEMILIE", "26") & " - " & aField.Field & " - " & aField.Libel

'-> Pointer sur l'objet associ� dans le catalogue
Set aObject = Catalogues(aField.Field)

'-> Si le champ est affect� � une table de correspondance cr�er 2 colonne
If aObject.Table_Correspondance <> "" Then
    '-> Indiquer que l'on est sur un extent � jointure
    IsJoinTable = True
    '-> Indiquer le champ d'acc�s direct
    FnameJoinField = aDescro.Descros(aObject.Table_Correspondance).IndexDirect
End If

'-> Charger le listView
DisplayExtentInListView aField, Me.ListView1

'-> Pointeur vers le listview
Set MyField = aField

'-> Pointeur de l'�cran
Screen.MousePointer = 0

End Sub


Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next

'-> Get de la zone cliente
GetClientRect Me.hwnd, aRect

'-> Redimension du browse
Me.ListView1.Left = 0
Me.ListView1.Top = Me.Toolbar1.Height
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom - Me.ListView1.Top - Me.StatusBar1.Height
Me.StatusBar1.Panels(1).Width = Me.ScaleX(aRect.Right, 3, 1)

End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

'-> Ne traiter que le bouton droit
If Button <> vbRightButton Then Exit Sub

'-> Si il y a des valeurs de saisie
If Me.ListView1.SelectedItem Is Nothing Then
    '-> Ne pas supprimer , Modifier
    Me.mnuDel.Enabled = False
    Me.mnuEdit.Enabled = False
Else
    '-> Pouvoir supprimer
    Me.mnuDel.Enabled = True
    Me.mnuEdit.Enabled = True
End If

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu Me.mnuPopup

'-> Selon le retour
Select Case strRetour
    Case "CREATE"
        '-> Ajouter un extent
        Call AddExtent
    Case "DELETE"
        '-> Mide � blanc d'un enregistrement
        Call DelExtent
    Case "EDIT"
        '-> Modifier un Extent
        Call UpdateExtent
End Select

'-> Vider la variable d'�change
strRetour = ""

End Sub
Private Sub DelExtent()

'---> Cette proc�dure remet � blanc la zone lock d'un enreg et de ses jointures

Dim aExtent As B3DField
Dim Rep As VbMsgBoxResult

'-> Pointer sur l'extent sp�cifi�
Set aExtent = MyField.Extents(UCase$(Trim(Me.ListView1.SelectedItem.Text)))

'-> Demander confirmation
Rep = MsgBox(Replace(GetMesseprog("SAITABLE", "17"), "#EXT#", Me.ListView1.SelectedItem.Text), vbExclamation + vbYesNo, GetMesseprog("APPLIC", "21"))
If Rep = vbNo Then Exit Sub

'-> Vider sa zone
aExtent.ZonLock = ""

'-> Mettre � jour l'affichage
Me.ListView1.ListItems.Remove Me.ListView1.SelectedItem.Index

End Sub

Private Sub UpdateExtent()

Dim aBuffer As B3DBuffer
Dim FieldTableDescro As B3DFnameDescriptif
Dim Rep As String
Dim aFieldJoin As B3DField
Dim Temp As String
Dim aObj As B3DObject

On Error GoTo GestError

'-> Pointer sur l'objet du catalogue
Set aObj = Catalogues(MyField.Field)

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> Ne faire un F10 sur le table que l'on est en train de modifier
If IsJoinTable Then

    '-> Initialiser un buffer
    Set aBuffer = New B3DBuffer
    
    '-> Afficher un messprog
    Me.StatusBar1.Panels(1).Text = GetMesseprog("SAITABLE", "10")
    
    '-> Effectuer le FieldTable
    If Not FieldTable(aConnexion, aBuffer, aConnexion.Ident, MyField.Field) Then
        MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
        GoTo GestError
    End If
            
    '-> R�cup�rer le descriptif sur le champ en cours
    Set FieldTableDescro = GetFnameDescriptif(aConnexion, aConnexion.Ident, FnameJoinField)
    
    '-> Vider la zone de texte
    Me.StatusBar1.Panels(1).Text = ""
    
    '-> Afficher la table de correspondance
    ShowFieldTableBrowse aBuffer, MyField.Field & " - " & MyField.Libel, aConnexion, FieldTableDescro

    '-> Ne rien faire si Annuler
    If strRetour = "" Then GoTo GestError
    
    '-> Lancer la mise � jour de l'enreg
    SetValueField MyField, Me.ListView1.SelectedItem.Text & "|" & strRetour, aDescro, aBuffer
    
    '-> Mettre � jour l'affichage avec le code de l'extent
    Me.ListView1.SelectedItem.Text = Me.ListView1.SelectedItem.Text
            
    '-> Mettre � jour la valeur de l'extent
    Me.ListView1.SelectedItem.SubItems(1) = MyField.Extents(UCase$(Trim(Me.ListView1.SelectedItem.Text))).ZonLock
            
    '-> Concatainer la valeur de d�signation jointure
    For Each aFieldJoin In MyField.Extents(UCase$(Trim(Me.ListView1.SelectedItem.Text))).FieldJoins
        '-> Cr�er la chaine de concataination en affichage
        If Trim(aFieldJoin.ZonData) <> "" Then
            If Temp = "" Then
                Temp = aFieldJoin.ZonData
            Else
                Temp = Temp & " - " & aFieldJoin.ZonData
            End If
        End If
    Next 'Pour tous les champs en recherche
    
    '-> Mettre � jour la valeur de la jointure
    Me.ListView1.SelectedItem.SubItems(2) = Temp
        
Else
    '-> Si on doit afficher un date : afficher le calendrier
    If UCase$(aObj.DataType) = "DATE" Then
        '-> vider la variable d'�change
        strRetour = ""
        
        '-> Afficher le calendrier
        If MyField.ZonLock = "" Then
            frmCalendrier.Calendar1.Value = Now
        Else
            frmCalendrier.Calendar1.Value = MyField.ZonLock
        End If
        frmCalendrier.Show vbModal
        
        '-> Tester le retour
        If strRetour = "" Then GoTo GestError
        
        '-> Setting de sa valeur
        SetValueField MyField, Me.ListView1.SelectedItem.Text & "|" & strRetour, aDescro, Nothing
        
        '-> Mettre � jour la valeur en affichage
        Me.ListView1.SelectedItem.SubItems(1) = strRetour
        
        '-> Quitter
        GoTo GestError
    End If

    '-> Faire une saisie directe
    strRetour = InputBox(aObj.Fill_Label & " : ", "Input value", MyField.ZonLock)
    
    '-> Ne rien faire
    If Trim(strRetour) = "" Then GoTo GestError
    
    '-> Tester les caract�res invalides
    If ChechInvalidChar(strRetour) Then
        '-> Afficher un message d'erreur
        Call MessageInvalidChar
        '-> Quitter la proc�dure
        GoTo GestError
    End If
    
    '-> Tester la validit� de la zone saisie
    Select Case UCase$(aObj.DataType)
        Case "CHARACTER"
        Case "DECIMAL", "INTEGER"
            '-> V�rifier que la zone saisie soit du type voulu
            If Not IsNumeric(Trim(strRetour)) Then
                '-> Afficher un message d'erreur
                MsgBox GetMesseprog("SAITABLE", "9"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
                GoTo GestError
            End If
    End Select
        
    '-> Setting de sa valeur
    SetValueField MyField, Me.ListView1.SelectedItem.Text & "|" & strRetour, aDescro, Nothing
    
    '-> Mettre � jour la valeur
    Me.ListView1.SelectedItem.SubItems(1) = strRetour
End If

GestError:
    '-> Vider la variable de retour
    strRetour = ""
    '-> Formatter le browse
    FormatListView Me.ListView1
    '-> Gestion de l'�cran
    Me.Enabled = True
    Screen.MousePointer = 0
    '-> Donner le focus au listview
    Me.ListView1.SetFocus
    Me.StatusBar1.Panels(1).Text = ""


End Sub


Private Sub AddExtent()

'---> Cette proc�dure ajoute un nouvel Extent

Dim aBuffer As B3DBuffer
Dim FieldTableDescro As B3DFnameDescriptif
Dim aObj As B3DObject
Dim aFieldJoin As B3DField
Dim Temp As String
Dim x As ListItem
Dim Rep As String

On Error GoTo GestError

'-> Pointer sur l'objet du catalogue
Set aObj = Catalogues(MyField.Field)

'-> Demander le code de l'extent
Rep = InputBox(GetMesseprog("SAITABLE", "15"), GetMesseprog("SAITABLE", "16"))
If Trim(Rep) = "" Then Exit Sub

'-> On v�rifier que l'extent saisi n'existe pas d�ja
For Each x In Me.ListView1.ListItems
    If UCase$(Trim(x.Text)) = UCase$(Trim(Rep)) Then
        MsgBox GetMesseprog("APPLIC", "2"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
        Exit Sub
    End If
Next

'-> V�rifier la validit� de saisie de l'extent
If ChechInvalidChar(Rep) Or InStr(1, Rep, "_") <> 0 Then
    '-> Afficher un message d'erreur
    Call MessageInvalidChar
    '-> Quitter la proc�dure
    GoTo GestError
End If

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> Ne faire un F10 sur le table que l'on est en train de modifier
If IsJoinTable Then

    '-> Initialiser un buffer
    Set aBuffer = New B3DBuffer
    
    '-> Afficher un messprog
    Me.StatusBar1.Panels(1).Text = GetMesseprog("SAITABLE", "10")
    
    '-> Effectuer le FieldTable
    If Not FieldTable(aConnexion, aBuffer, aConnexion.Ident, MyField.Field) Then
        MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
        GoTo GestError
    End If
            
    '-> R�cup�rer le descriptif sur le champ en cours
    Set FieldTableDescro = GetFnameDescriptif(aConnexion, aConnexion.Ident, FnameJoinField)
    
    '-> Vider la zone de texte
    Me.StatusBar1.Panels(1).Text = ""
    
    '-> Afficher la table de correspondance
    ShowFieldTableBrowse aBuffer, MyField.Field & " - " & MyField.Libel, aConnexion, FieldTableDescro

    '-> Ne rien faire si Annuler
    If strRetour = "" Then GoTo GestError
    
    '-> Ajouter l'enreg
    Set x = Me.ListView1.ListItems.Add(, , Rep)
    x.Selected = True
    
    '-> Lancer la mise � jour de l'enreg
    SetValueField MyField, Rep & "|" & strRetour, aDescro, aBuffer
    
    '-> Mettre � jour l'affichage avec le code de l'extent
    Me.ListView1.SelectedItem.Text = Rep
            
    '-> Mettre � jour la valeur de l'extent
    Me.ListView1.SelectedItem.SubItems(1) = MyField.Extents(UCase$(Trim(Rep))).ZonLock
            
    '-> Concatainer la valeur de d�signation jointure
    For Each aFieldJoin In MyField.Extents(UCase$(Trim(Rep))).FieldJoins
        '-> Cr�er la chaine de concataination en affichage
        If Trim(aFieldJoin.ZonData) <> "" Then
            If Temp = "" Then
                Temp = aFieldJoin.ZonData
            Else
                Temp = Temp & " - " & aFieldJoin.ZonData
            End If
        End If
    Next 'Pour tous les champs en recherche
    
    '-> Mettre � jour la valeur de la jointure
    Me.ListView1.SelectedItem.SubItems(2) = Temp
    
    
Else
    '-> Si on doit afficher un date : afficher le calendrier
    If UCase$(aObj.DataType) = "DATE" Then
        '-> vider la variable d'�change
        strRetour = ""
        
        '-> Afficher le calendrier
        If MyField.ZonLock = "" Then
            frmCalendrier.Calendar1.Value = Now
        Else
            frmCalendrier.Calendar1.Value = MyField.ZonLock
        End If
        frmCalendrier.Show vbModal
        
        '-> Tester le retour
        If strRetour = "" Then GoTo GestError
        
        '-> Ajouter l'enreg
        Set x = Me.ListView1.ListItems.Add(, , Rep)
        x.Selected = True
        
        '-> Setting de sa valeur
        SetValueField MyField, Rep & "|" & strRetour, aDescro, Nothing
        
        '-> Mettre � jour la valeur en affichage
        Me.ListView1.SelectedItem.SubItems(1) = strRetour
        
        '-> Quitter
        GoTo GestError
    End If

    '-> Faire une saisie directe
    strRetour = InputBox(aObj.Fill_Label & " : ", "Input value", MyField.ZonLock)
    
    '-> Ne rien faire
    If Trim(strRetour) = "" Then GoTo GestError
    
    '-> Tester les caract�res invalides
    If ChechInvalidChar(strRetour) Then
        '-> Afficher un message d'erreur
        Call MessageInvalidChar
        '-> Quitter la proc�dure
        GoTo GestError
    End If
    
    '-> Tester la validit� de la zone saisie
    Select Case UCase$(aObj.DataType)
        Case "CHARACTER"
        Case "DECIMAL", "INTEGER"
            '-> V�rifier que la zone saisie soit du type voulu
            If Not IsNumeric(Trim(strRetour)) Then
                '-> Afficher un message d'erreur
                MsgBox GetMesseprog("SAITABLE", "9"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
                GoTo GestError
            End If
    End Select
    
    '-> Ajouter l'enreg
    Set x = Me.ListView1.ListItems.Add(, , Rep)
    x.Selected = True
    
    '-> Setting de sa valeur
    SetValueField MyField, Rep & "|" & strRetour, aDescro, Nothing
    
    '-> Mettre � jour la valeur
    Me.ListView1.SelectedItem.SubItems(1) = strRetour
End If

GestError:

    '-> Vider la variable de retour
    strRetour = ""
    '-> Formatter le browse
    FormatListView Me.ListView1
    '-> Gestion de l'�cran
    Me.Enabled = True
    Screen.MousePointer = 0
    '-> Donner le focus au listview
    Me.ListView1.SetFocus
    Me.StatusBar1.Panels(1).Text = ""


End Sub

Private Sub mnuCreate_Click()
strRetour = "CREATE"
End Sub

Private Sub mnuDel_Click()
strRetour = "DELETE"
End Sub

Private Sub mnuEdit_Click()
strRetour = "EDIT"
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
Select Case Button.Key
    Case "OK"
        '-> D�charger la feuille
        Unload Me
End Select


End Sub
