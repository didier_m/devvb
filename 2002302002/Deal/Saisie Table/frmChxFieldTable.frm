VERSION 5.00
Begin VB.Form frmChxFieldTable 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   2550
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5175
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2550
   ScaleWidth      =   5175
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "Command2"
      Height          =   495
      Left            =   2640
      TabIndex        =   5
      Top             =   3120
      Width           =   975
   End
   Begin VB.CommandButton Command1 
      Height          =   855
      Left            =   4200
      Picture         =   "frmChxFieldTable.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   1560
      Width           =   855
   End
   Begin VB.Frame Frame1 
      Height          =   2535
      Left            =   0
      TabIndex        =   1
      Top             =   2
      Width           =   5175
      Begin VB.OptionButton Option2 
         Caption         =   "Option1"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   1320
         Width           =   3615
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   840
         TabIndex        =   0
         Top             =   840
         Width           =   3975
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Option1"
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   360
         Value           =   -1  'True
         Width           =   3615
      End
   End
End
Attribute VB_Name = "frmChxFieldTable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

Dim Tempo As String

'-> Tester l'option s�lectionn�e
If Me.Option1.Value Then
    '-> Tester la saisie du code de recherche
    If Trim(Me.Text1.Text) = "" Then
        strRetour = "ALL|"
    Else
        Tempo = Trim(Me.Text1.Text)
        strRetour = "CHX|" & ReplaceInvalidChar(Tempo)
    End If
Else
    strRetour = "ALL|"
End If

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("SAITABLE", "18")

'-> Lib�lles
Me.Option1.Caption = GetMesseprog("SAITABLE", "19")
Me.Option2.Caption = GetMesseprog("SAITABLE", "20")

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then Command1_Click
End Sub
