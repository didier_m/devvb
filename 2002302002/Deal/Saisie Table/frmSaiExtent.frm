VERSION 5.00
Begin VB.Form frmSaiExtent 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Extent"
   ClientHeight    =   1860
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5235
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1860
   ScaleWidth      =   5235
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command3 
      Height          =   375
      Left            =   4800
      Picture         =   "frmSaiExtent.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   525
      Width           =   375
   End
   Begin VB.CommandButton Command1 
      Default         =   -1  'True
      Height          =   735
      Left            =   4440
      Picture         =   "frmSaiExtent.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1080
      Width           =   735
   End
   Begin VB.CommandButton Command2 
      Height          =   735
      Left            =   3600
      Picture         =   "frmSaiExtent.frx":1994
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   1080
      Width           =   735
   End
   Begin VB.TextBox Text2 
      Height          =   285
      Left            =   1440
      TabIndex        =   1
      Top             =   600
      Width           =   3255
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   1440
      MaxLength       =   10
      TabIndex        =   0
      Top             =   120
      Width           =   3735
   End
   Begin VB.Label Label2 
      Caption         =   "Value : "
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   600
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Extent : "
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmSaiExtent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Indique sur quel champ on se trouve
Private aObject As B3DObject
Public ModeMaj As Boolean

Public Sub Init(ToUpdate As Boolean, NumField As String, Value As String)

'---> Cette proc�dure charge ou non selon que l'on soit en modif ou en ajout

'-> Affecter l'objet B3D
Set aObject = Catalogues(NumField)

'-> Indiquer le mode
ModeMaj = ToUpdate

'-> Mise � jour des valeurs
If ModeMaj Then 'Mode update
    '-> Bloquer la zone Extent
    Me.Text1.Enabled = False
    '-> Positionner les valeurs
    Me.Text1.Text = Entry(1, Value, Chr(0))
    Me.Text2.Text = Entry(2, Value, Chr(0))
    '-> Gestion de la table de correspondance
    If aObject.Table_Correspondance = "" Then Me.Text2.Tag = Entry(3, Value, Chr(0))
End If 'Si on est en mode ajout ou en mode update

'-> F10 ou non
If aObject.Table_Correspondance <> "" Then
    Me.Text2.Width = 3375
    Me.Text2.Enabled = False
Else
    Me.Text2.Width = 3735
    Me.Command3.Visible = False
End If


End Sub

Private Sub Command1_Click()

If Not ModeMaj Then '-> Si on est en mode ajout
    '-> V�rifier que l'extent soit saisi
    If Trim(Me.Text1.Text) = "" Then
        MsgBox GetMesseprog("SAITABLE", "7"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
        Me.Text1.SetFocus
        Exit Sub
    End If
    '-> Check des caract�res
    If ChechInvalidChar(Me.Text1.Text) Then
        Call MessageInvalidChar
        Me.Text1.SetFocus
        Exit Sub
    End If
    '-> Check du "_"
    If InStr(1, Me.Text1.Text, "_") <> 0 Then
        MsgBox GetMesseprog("SAITABLE", "8"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
        Me.Text1.SetFocus
        Exit Sub
    End If
End If 'Si on est en saisie

'-> V�rification de la zone
If Trim(Me.Text2.Text) <> "" Then
    '-> V�rifier les caract�res invalides
    If ChechInvalidChar(Me.Text2.Text) Then
        Call MessageInvalidChar
        Me.Text2.SetFocus
        Exit Sub
    End If
    '-> V�rifier la nature de la saisie THIERRY
    Select Case UCase$(aObject.DataType)
        Case "CHARACTER"
        Case "DECIMAL", "INTEGER"
            '-> V�rifier que la zone saisie soit du type voulu
            If Not IsNumeric(Trim(Me.Text2.Text)) Then
                '-> Afficher un message d'erreur
                MsgBox GetMesseprog("SAITABLE", "9"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
                Me.Text2.SetFocus
                Exit Sub
            End If
        Case "DATE"
            '-> V�rifier que la zone saisie soit du type voulu
            If Not IsDate(Trim(Me.Text2.Text)) Then
                '-> Afficher un message d'erreur
                MsgBox GetMesseprog("SAITABLE", "9"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
                Me.Text2.SetFocus
                Exit Sub
            End If
    End Select
    
End If

'-> Retourner la valeur
strRetour = Trim(Me.Text1.Text) & Chr(0) & Trim(Me.Text2.Text)
If aObject.Table_Correspondance <> "" Then strRetour = strRetour & Chr(0) & Me.Text2.Tag

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Command2_Click()
'-> D�charger la feuille
strRetour = ""
Unload Me
End Sub

Private Sub Command3_Click()

Dim aBuffer As B3DBuffer

'-> Initialiser un buffer
Set aBuffer = New B3DBuffer
Dim aEnreg As B3DEnreg
Dim NewField As B3DField
Dim Temp As String
Dim MyDescro As B3DFnameDescriptif

'-> Effectuer le FieldTable
If Not FieldTable(aConnexion, aBuffer, aConnexion.Ident, aObject.Num_Field) Then
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    Exit Sub
End If

'-> R�cup�rer la d�fintion de la table
Set MyDescro = GetFnameDescriptif(aConnexion, aConnexion.Ident, aObject.Table_Correspondance)

'-> Afficher la table de correspondance
ShowFieldTableBrowse aBuffer, aObject.Num_Field & " - " & aObject.Fill_Label, aConnexion, MyDescro

'-> Ne rien faire si Annuler
If strRetour = "" Then Exit Sub

'-> Pointer sur l'enreg sp�cifi�
Set aEnreg = aBuffer.Enregs(strRetour)

'-> Vider le tag du Fill-In
Me.Text2.Tag = ""
'-> Concatainer la valeur de d�signation jointure
For Each NewField In aEnreg.Fields
    '-> Cr�er la chaine de concataination en affichage
    If Temp = "" Then
        Temp = NewField.ZonData
    Else
        Temp = Temp & " - " & NewField.ZonData
    End If
    '-> Cr�er la zone de retour
    If Me.Text2.Tag = "" Then
        Me.Text2.Tag = NewField.Field & "~" & NewField.ZonData
    Else
        Me.Text2.Tag = Me.Text2.Tag & "|" & NewField.Field & "~" & NewField.ZonData
    End If
Next 'Pour tous les champs en recherche

'-> Afficher la valeur
Me.Text2.Text = Temp

'-> Vider la variable de retour
strRetour = ""

End Sub

Private Sub Text1_GotFocus()
SelectTxtBox Me.Text1
End Sub

Private Sub Text2_GotFocus()
SelectTxtBox Me.Text2
End Sub
