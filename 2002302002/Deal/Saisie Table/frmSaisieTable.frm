VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmSaisieTable 
   Caption         =   "Form1"
   ClientHeight    =   8415
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9015
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   561
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   601
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6120
      Top             =   960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSaisieTable.frx":0000
            Key             =   "SAVE"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSaisieTable.frx":0CDA
            Key             =   "EXIT"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSaisieTable.frx":19B4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSaisieTable.frx":268E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EXIT"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SAVE"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ZOOM"
            ImageIndex      =   3
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   1
      Top             =   8040
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   7215
      Left            =   0
      TabIndex        =   0
      Top             =   720
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   12726
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "POPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuDelValue 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmSaisieTable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public aEnreg As B3DEnreg 'Enreg en cours
Public aDescro As B3DFnameDescriptif 'Descritptof en cours
Public MasterFname As String 'Indique la table sur laquelle on travaille
Public pFonction As String 'Fonction pour laquelle on est rentr�e dans la saisie
Public IsModified As Boolean '-> Indique que l'enreg est modifi�

Private Sub SaveEnreg()

Dim ListeField As String
Dim aObj As B3DObject

On Error GoTo GestError

'-> Bloquer l'�cran
Screen.MousePointer = 11
Me.Enabled = False

'-> Effectuer les controles
If pFonction = "CREATE" Then
    '-> Pour une table en acc�s direct, v�rifier que le champ sp�cifi� soit rens�igfn�
    If Me.aDescro.IndexDirect <> "" Then
        '-> V�rifier que le champ sp�cifi� soit saisi
        If Trim(Me.aEnreg.Fields(Me.aDescro.IndexDirect).ZonLock) = "" Then
            '-> Pointer sur le champ
            Set aObj = Catalogues(Me.aDescro.IndexDirect)
            '-> Afficher un message d'erreur
            MsgBox "Saisie du champ : " & aObj.Fill_Label & " obligatoire", vbExclamation + vbOKOnly, "Erreur"
            '-> Quitter la proc�dure
            GoTo GestError
        End If 'V�rification de la cl� primaire sur table � index directe
    End If 'Si on est sur une table en acc�s direct
    
    '-> Faire un create sur l'enreg
    If Not CreateEnreg(aConnexion, MasterFname, Me.aEnreg) Then
        '-> Tester  le code erreur 90026 pas de modif
        If aConnexion.ErrorNumber = "90026" Then Exit Sub
        '-> Afficher une message d'erreur
        MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error" 'MESSPROG
        '-> Fin du programme
        GoTo GestError
    End If
Else '-> On est en mode update
    If Not UpdateEnreg(aConnexion, MasterFname, Me.aEnreg, False) Then
        '-> Tester  le code erreur 90026 pas de modif
        If aConnexion.ErrorNumber = "90026" Then GoTo GestError
        '-> Afficher une message d'erreur
        MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
        '-> Fin du programme
        GoTo GestError
    End If
End If

GestError:
    '-> D�bloquer
    Me.Enabled = True
    Screen.MousePointer = 0
    '-> Indiquer que l'on a enregistr�
    IsModified = False


End Sub

Private Sub ExitPrg()


Dim Rep As VbMsgBoxResult
Dim ToSave As Boolean

'-< Si le contenu de l'enreg a chang� demaner si on veut enregistrer
If IsModified Then
    Rep = MsgBox(GetMesseprog("SAITABLE", "4"), vbQuestion + vbYesNoCancel, GetMesseprog("APPLIC", "21"))
    '-> Selon le retour
    Select Case Rep
        Case vbYes
            '-> Enregistrer
            ToSave = True
        Case vbNo
            '-> Ne pas enregistrer
            ToSave = False
        Case vbCancel
            '-> Quitter
            Exit Sub
    End Select
End If

'-> Bloquer la feuille
Me.Enabled = False
Screen.MousePointer = 11

'-> Si on doit enregistrer : lancer l'enregistrement
If ToSave Then
    '-> Messprog
    Me.StatusBar1.Panels(1).Text = GetMesseprog("SAITABLE", "5")
    DoEvents
    '-> Lancer l'enreg
    SaveEnreg
End If

'-> Gestion des erreur
GestError:
    '-> Faire un unlock si on est en mode update
    If pFonction = "UPDATE" Then UnlockEnreg aConnexion, MasterFname, Me.aEnreg.NumEnreg

    '-> D�bloquer le pointeur de l'�cran
    Screen.MousePointer = 0
    
    '-> D�charger la feuille
    Unload Me


End Sub



Private Sub Form_Load()

'-> titre de la feuille
Me.Caption = GetMesseprog("SAITABLE", "6")

'-> Cr�er 3 Colonnes
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("REQUETEUR", "18")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "12")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("SAITABLE", "2")

'-> ToolTip des boutons
Me.Toolbar1.Buttons(1).ToolTipText = GetMesseprog("SAITABLE", "11")
Me.Toolbar1.Buttons(3).ToolTipText = GetMesseprog("SAITABLE", "12")
Me.Toolbar1.Buttons(4).ToolTipText = GetMesseprog("SAITABLE", "17")

'-> Formatter
FormatListView Me.ListView1
DoEvents

End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next

'-> Get de la zone cliente
GetClientRect Me.hwnd, aRect

'-> Redimension du browse
Me.ListView1.Left = 0
Me.ListView1.Top = Me.Toolbar1.Height
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom - Me.StatusBar1.Height - Me.Toolbar1.Height

'-> Redimensionner le Panels
Me.StatusBar1.Panels(1).Width = Me.ScaleX(aRect.Right, 3, 1)

End Sub

Public Sub Init()

'---> Cr�ation de la liste des champs en saisie

Dim x As ListItem
Dim aField As B3DField
Dim aFieldJoin As B3DField
Dim Temp As String

'-> Pour tous les champs
For Each aField In aEnreg.Fields
    Temp = ""
    '-> Cr�er un nouvel Item
    Set x = Me.ListView1.ListItems.Add(, "FIELD|" & aField.Field)
    x.Text = aField.Libel
    '-> Gestion des champs d'extent
    If aField.IsExtent Then
        x.SubItems(1) = "..."
    Else
        '-> Charger sa valeur
        x.SubItems(1) = aField.ZonData
    End If
    '-> Gestion des jointures
    For Each aFieldJoin In aField.FieldJoins
        If Trim(aFieldJoin.ZonData) <> "" Then
            If Temp = "" Then
                Temp = aFieldJoin.ZonData
            Else
                Temp = Temp & " - " & aFieldJoin.ZonData
            End If
        End If
    Next 'Pour tous les champs de jointure
    If Temp <> "" Then x.SubItems(2) = Temp
Next 'Pour tous les champs

'-> Donner son nom � la table
Me.Caption = Me.Caption & " [" & aDescro.Fname & " - " & aDescro.Designation & "]"

'-> Formatter le browse
FormatListView Me.ListView1

End Sub

Private Sub ListView1_DblClick()

'---> Modification

Dim aObj As B3DObject
Dim aField As B3DField
Dim aFieldJoin As B3DField
Dim NewField As B3DField
Dim aBuffer As B3DBuffer
Dim NewEnreg As B3DEnreg
Dim Temp As String
Dim IndexDirectField As String
Dim FieldTableDescro As B3DFnameDescriptif

'-> Ne Rien faire si pas d'�lement s�lectionn�
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> Pointer sur le champ de l'enregistrement
Set aField = aEnreg.Fields(Entry(2, Me.ListView1.SelectedItem.Key, "|"))

'-> V�rifier si le champ est modifiable
If aField.IsModifiable And pFonction = "UPDATE" Then
    '-> On ne peut pas modifier ce champ -> Message d'erreur
    MsgBox GetMesseprog("SAITABLE", "14"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

'-> Indiquer que l'on doit modifier
IsModified = True

'-> Pointer sur le champ du catalogue
Set aObj = Catalogues(Entry(2, Me.ListView1.SelectedItem.Key, "|"))

'-> Tester le cas d'un champ � extent
If aField.IsExtent Then
    '-> Afficher une feuille avec les extent existants
    Load frmFillExtent
    '-> Initialiser
    frmFillExtent.Init aField
    '-> afficher la feuille
    frmFillExtent.Show vbModal
    '-> Quitter la proc�dure
    GoTo GestError
End If 'Si le champ est a extent

'-> Si le champ est en acc�s jointure
'-> Ne faire un F10 sur le table que l'on est en train de modifier
If aObj.Table_Correspondance <> "" And aObj.Table_Correspondance <> MasterFname Then
            
    '-> Vider la variable d'�change
    strRetour = ""
    
    '-> Afficher la fen�tre de choix de la valeur
    Screen.MousePointer = 0
    frmChxFieldTable.Show vbModal

    '-> Tester le retour
    If strRetour = "" Then GoTo GestError
    
    '-> Initialiser un buffer
    Screen.MousePointer = 11
    Set aBuffer = New B3DBuffer
    
    '-> Afficher un messprog
    Me.StatusBar1.Panels(1).Text = GetMesseprog("SAITABLE", "10")
        
    '-> Effectuer le FieldTable
    If Not FieldTable(aConnexion, aBuffer, aConnexion.Ident, aField.Field, Entry(2, strRetour, "|")) Then
        MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
        GoTo GestError
    End If
            
    '-> R�cup�rer le descriptif sur le champ en cours
    Set FieldTableDescro = GetFnameDescriptif(aConnexion, aConnexion.Ident, aObj.Table_Correspondance)
    
    '-> Vider la zone de texte
    Me.StatusBar1.Panels(1).Text = ""
    
    '-> Afficher la table de correspondance
    ShowFieldTableBrowse aBuffer, aField.Field & " - " & aField.Libel, aConnexion, FieldTableDescro

    '-> Ne rien faire si Annuler
    If strRetour = "" Then GoTo GestError
    
    '-> Lancer la mise � jour de l'enreg
    SetValueField aField, strRetour, aDescro, aBuffer
    
    '-> Mettre � jour l'affichage
    Me.ListView1.SelectedItem.SubItems(1) = aField.ZonLock
            
    '-> Concatainer la valeur de d�signation jointure
    For Each aFieldJoin In aField.FieldJoins
        '-> Cr�er la chaine de concataination en affichage
        If Trim(aFieldJoin.ZonData) <> "" Then
            If Temp = "" Then
                Temp = aFieldJoin.ZonData
            Else
                Temp = Temp & " - " & aFieldJoin.ZonData
            End If
        End If
    Next 'Pour tous les champs en recherche
    
    '-> Mettre � jour la d�signation
    Me.ListView1.SelectedItem.SubItems(2) = Temp
Else
    '-> Si on doit afficher un date : afficher le calendrier
    If UCase$(aObj.DataType) = "DATE" Then
        '-> vider la variable d'�change
        strRetour = ""
        '-> Afficher le calendrier
        If aField.ZonLock = "" Then
            frmCalendrier.Calendar1.Value = Now
        Else
            frmCalendrier.Calendar1.Value = aField.ZonLock
        End If
        frmCalendrier.Show vbModal
        '-> Tester le retour
        If strRetour = "" Then GoTo GestError
        SetValueField aField, strRetour, aDescro, Nothing
        '-> Mettre � jour la valeur
        Me.ListView1.SelectedItem.SubItems(1) = strRetour
        '-> Quitter
        GoTo GestError
    End If

    '-> Faire une saisie directe
    strRetour = InputBox(aObj.Fill_Label & " : ", "Input value", aField.ZonLock)
    '-> Ne rien faire
    If Trim(strRetour) = "" Then GoTo GestError
    
    '-> Tester les caract�res invalides
    If ChechInvalidChar(strRetour) Then
        '-> Afficher un message d'erreur
        Call MessageInvalidChar
        '-> Quitter la proc�dure
        GoTo GestError
    End If
    
    '-> Tester la validit� de la zone saisie
    Select Case UCase$(aObj.DataType)
        Case "CHARACTER"
        Case "DECIMAL", "INTEGER"
            '-> V�rifier que la zone saisie soit du type voulu
            If Not IsNumeric(Trim(strRetour)) Then
                '-> Afficher un message d'erreur
                MsgBox GetMesseprog("SAITABLE", "9"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
                GoTo GestError
            End If
    End Select
    SetValueField aField, strRetour, aDescro, Nothing
    '-> Mettre � jour la valeur
    Me.ListView1.SelectedItem.SubItems(1) = strRetour
End If

GestError:

'-> Vider la variable de retour
strRetour = ""

'-> Formatter le browse
FormatListView Me.ListView1

    '-> Gestion de l'�cran
    Me.Enabled = True
    Screen.MousePointer = 0
    '-> Donner le focus au listview
    Me.ListView1.SetFocus
    Me.StatusBar1.Panels(1).Text = ""
    


End Sub

Private Sub ListView1_KeyDown(KeyCode As Integer, Shift As Integer)

If KeyCode = 46 Then DelValue

End Sub

Private Sub ListView1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then ListView1_DblClick

End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim aField As B3DField

'-> Ne Rien faire si pas d'�lement s�lectionn�
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Ne traiter que le bouton droit
If Button <> vbRightButton Then Exit Sub

'-> Tester si on est sur un champ de type extend
Set aField = aDescro.Fields(Entry(2, Me.ListView1.SelectedItem.Key, "|"))

'-> Ne rien faire si on est sur un champ de type Extend
If aField.IsExtent Then Exit Sub

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu mnuPopup

'-> Quitter
If strRetour = "" Then Exit Sub

'-> Lancer un delete
DelValue

End Sub
Private Sub DelValue()

Dim aField As B3DField

'-> Pointer sur le champ
Set aField = aEnreg.Fields(Entry(2, Me.ListView1.SelectedItem.Key, "|"))

'-> Vider la valeur
aField.ZonLock = ""

'-> Mettre � jour l'affichage
Me.ListView1.SelectedItem.SubItems(1) = ""
Me.ListView1.SelectedItem.SubItems(2) = ""

'-> Mettre � jour la variable d'�change
strRetour = ""

End Sub

Private Sub mnuDelValue_Click()
strRetour = "DEL"
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

Select Case Button.Key
    Case "EXIT"
        '-> Fin du programme
        Call ExitPrg
    Case "SAVE"
        '-> Enregistrer
        Call SaveEnreg
    Case "ZOOM"
        '-> Afficher le zoom
        DisplayZoomInAcces
End Select

End Sub

Private Sub DisplayZoomInAcces()

'---> Afficher Le zoom

'-> Afficher le libell� de recherche
Me.StatusBar1.Panels(1).Text = GetMesseprog("SAITABLE", "10")
'-> Afficher le zoom
DisplayZoom aConnexion, MasterFname, aEnreg.NumEnreg, True, False, aDescro, Me, True
'-> Lib�rer le lib�lle
Me.StatusBar1.Panels(1).Text = ""

End Sub

