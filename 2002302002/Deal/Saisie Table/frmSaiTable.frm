VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmSaiTable 
   Caption         =   "Gestion des tables"
   ClientHeight    =   3810
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4260
   Icon            =   "frmSaiTable.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   254
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   284
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSComctlLib.ListView ListView1 
      Height          =   3135
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3375
      _ExtentX        =   5953
      _ExtentY        =   5530
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   0
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "POPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuCreate 
         Caption         =   ""
      End
      Begin VB.Menu mnuUpdate 
         Caption         =   ""
      End
      Begin VB.Menu mnuZoom 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelete 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmSaiTable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()

'-> Libell�s des menus MESSPROG
Me.mnuCreate.Caption = "Ajouter" 'GetMesseprog("COMMUN", "17")
Me.mnuUpdate.Caption = "Modifier" 'GetMesseprog("COMMUN", "19")
Me.mnuZoom.Caption = "Zoom" 'GetMesseprog("COMMUN", "26")
Me.mnuDelete.Caption = "Supprimer" 'GetMesseprog("COMMUN", "18")


End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next

'-> Get de la zone cliente
GetClientRect Me.hwnd, aRect

'-> Redimension du browse
Me.ListView1.Left = 0
Me.ListView1.Top = 0
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom - Me.ListView1.Top

End Sub

Private Sub ListView1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

'-> Trier sur la colonne
Me.ListView1.SortKey = ColumnHeader.Index - 1
If Me.ListView1.SortOrder = lvwAscending Then
    Me.ListView1.SortOrder = lvwDescending
Else
    Me.ListView1.SortOrder = lvwAscending
End If
Me.ListView1.Sorted = True

End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)


'-> Quitter si pas bouton droit
If Button <> vbRightButton Then Exit Sub

'-> Selon le contenu
If Me.ListView1.SelectedItem Is Nothing Then
    Me.mnuDelete.Enabled = False
    Me.mnuZoom.Enabled = False
Else
    Me.mnuDelete.Enabled = True
    Me.mnuZoom.Enabled = True
End If

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu Me.mnuPopup

'-> Selon le retour
Select Case strRetour
    Case "CREATE"
        '-> Afficher la feuille de saisie de la table
        AddNewEnreg
    Case "DELETE"
        '-> Supprimer l'enregistrement en cours
        DeleteB3DEnreg
    Case "ZOOM"
        '-> Faire un zoom sur la feuille
        Zoom
    Case "UPDATE"
        '-> Faire un update
        UpdateB3DEnreg
End Select

'-> Vider la variable de retour
End Sub

Private Sub mnuCreate_Click()
strRetour = "CREATE"
End Sub

Private Sub mnuDelete_Click()
strRetour = "DELETE"
End Sub
Private Sub UpdateB3DEnreg()

Dim ListeField As String
Dim aField As B3DField
Dim aEnreg As B3DEnreg


'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11

Rep:
'-> Faire un FindData lock sur l'enreg
Set aEnreg = FindData(aConnexion, aConnexion.Ident, MasterFname, Entry(2, Me.ListView1.SelectedItem.Key, "|"), True, False, aDescro, True)

'-> Tester le retour
If aEnreg Is Nothing Then
    '-> Tester si l'enregistrement n'est pas lock� par un autre
    If aConnexion.ErrorNumber = "90006" Then
        '-> v�rifier si le lock est actif
        If Entry(4, aConnexion.ErrorParam, "�") = "0" Then
            '-> Envoyer un B3D unlock
            If UnlockEnreg(aConnexion, MasterFname, Entry(2, Me.ListView1.SelectedItem.Key, "|")) Then
                '-> Reprise
                GoTo Rep
            End If
        Else
            '-> Message de lock
            LibError = Catalogues("00058").Fill_Label
            LibError = Replace(LibError, "$#EN#$", Entry(2, Me.ListView1.SelectedItem.Key, "|"))
            LibError = Replace(LibError, "$#OP#$", Entry(1, aConnexion.ErrorParam, "�"))
            LibError = Replace(LibError, "$#DAT#$", Entry(2, aConnexion.ErrorParam, "�"))
            LibError = Replace(LibError, "$#PRO#$", Entry(3, aConnexion.ErrorParam, "�"))
            MsgBox LibError, vbExclamation + vbOKOnly, "Error"
        End If
        '-> Fin de la fonction
        GoTo GestError
    End If
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter le programme
    GoTo GestError
End If

'-> Basculer zondata dans zonlock
SetZonLockFromZonData aEnreg

'-> Charger la feuille en m�moire
Load frmSaisieTable

'-> Affecter l'enreg
Set frmSaisieTable.aEnreg = aEnreg

'-> Affecter son descriptif
Set frmSaisieTable.aDescro = aDescro

'-> Indiquer que l'on est en mode update
frmSaisieTable.Command1.Tag = "UPDATE"

'-> Lancer l'intialisation
frmSaisieTable.Init

'-> D�boquer les enreg
Me.Enabled = True
Screen.MousePointer = 0

'-> Afficher la feuille
frmSaisieTable.Show vbModal

'-> Redonner le focus au listview
Me.ListView1.SetFocus

GestError:
    '-> Bloquer les enreg
    Me.Enabled = True
    Screen.MousePointer = 0

End Sub



Private Sub Zoom()

'---> Cette fonction effectue un zoom sur un enregistrement

DisplayZoom aConnexion, MasterFname, Entry(2, Me.ListView1.SelectedItem.Key, "|"), True, False, aDescro, Me, True


End Sub


Private Sub DeleteB3DEnreg()

'---> Cette proc�dure effecture un delete de l'enreg s�lectionn�

'---> Cette proc�dure supprime un enreg

Dim MyEnreg As B3DEnreg
Dim LibError As String
Dim Rep As VbMsgBoxResult

On Error GoTo GestError

'-> Demander confirmation
Rep = MsgBox("Supprimer l'enregistrement : " & Entry(2, Me.ListView1.SelectedItem.Key, "|"), vbExclamation + vbYesNo, Catalogues("00053").Fill_Label)
If Rep <> vbYes Then Exit Sub

'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11

'-> Faire un DeleteData
Rep:
If Not DeleteEnreg(aConnexion, MasterFname, Entry(2, Me.ListView1.SelectedItem.Key, "|")) Then
    '-> Tester si l'enregistrement n'est pas lock� par un autre
    If aConnexion.ErrorNumber = "90006" Then
        '-> v�rifier si le lock est actif
        If Entry(4, aConnexion.ErrorParam, "�") = "0" Then
            '-> Envoyer un B3D unlock
            If UnlockEnreg(aConnexion, MasterFname, Entry(2, Me.ListView1.SelectedItem.Key, "|")) Then
                '-> Reprise
                GoTo Rep
            End If
        Else
            LibError = "Impossible de supprimer cet enregistrement , il est en cours de modification"
            MsgBox LibError, vbExclamation + vbOKOnly, "Error"
        End If
        '-> Fin de la fonction
        GoTo GestError
    End If
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter le programme
    GoTo GestError
End If

'-> Supprimer l'enreg du browse
Me.ListView1.ListItems.Remove (Me.ListView1.SelectedItem.Key)

GestError:
    '-> Bloquer les enreg
    Me.Enabled = True
    Screen.MousePointer = 0



End Sub

Private Sub AddNewEnreg()

'---> Cette proc�dure donne acc�s � la modification de l'�cran

Dim MyEnreg As B3DEnreg
Dim EnregRef As B3DEnreg

On Error GoTo GestError

'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11

'-> Faire un FindData sur l'enregistrement en cours
Set MyEnreg = aDescro.GetEnreg

'-> Charger la feuille
Load frmSaisieTable

'-> positionner sur le tag que l'on est en update
frmSaisieTable.Command1.Tag = "CREATE"

'-> Affecter l'enregistrement
Set frmSaisieTable.aEnreg = MyEnreg

'-> Affecter son descriptif
Set frmSaisieTable.aDescro = aDescro

'-> Lancer l'intialisation
frmSaisieTable.Init

'-> Pointeur de l'�cran
Screen.MousePointer = 0

'-> Afficher la feuille
frmSaisieTable.Show vbModal

'-> Redonner le focus au listview
Me.ListView1.SetFocus

GestError:
    '-> Bloquer les enreg
    Me.Enabled = True
    Screen.MousePointer = 0



End Sub

Private Sub mnuUpdate_Click()
strRetour = "UPDATE"
End Sub

Private Sub mnuZoom_Click()
strRetour = "ZOOM"
End Sub
