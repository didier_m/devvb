VERSION 5.00
Begin VB.Form frmSaiComment 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Saisie des Commentaires"
   ClientHeight    =   3030
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7230
   Icon            =   "frmSaiComment.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3030
   ScaleWidth      =   7230
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command1 
      Height          =   615
      Left            =   6480
      Picture         =   "frmSaiComment.frx":0442
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   2400
      Width           =   735
   End
   Begin VB.Frame Frame2 
      Caption         =   "Commentaires Employ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   2415
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7215
      Begin VB.PictureBox pctCalendar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   3240
         Picture         =   "frmSaiComment.frx":1404
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   6
         ToolTipText     =   "Choix d'une date"
         Top             =   360
         Width           =   255
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   285
         Left            =   1560
         MaxLength       =   10
         TabIndex        =   2
         Top             =   360
         Width           =   1575
      End
      Begin VB.TextBox Text3 
         Height          =   1485
         Left            =   1560
         MaxLength       =   250
         MultiLine       =   -1  'True
         TabIndex        =   1
         Top             =   840
         Width           =   5535
      End
      Begin VB.Label Label1 
         Caption         =   "Date  Commentaire"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label Label3 
         Caption         =   "Commentaire"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   3
         Top             =   840
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmSaiComment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
    If Me.Text1.Text <> "" And Me.Text3.Text <> "" Then
        strRetour = Me.Text1.Text & "�" & Me.Text3.Text   'Entry(3, Me.Text1.Text, "/") & Entry(2, Me.Text1.Text, "/") & Entry(1, Me.Text1.Text, "/") & "�" & Me.Text3.Text
    End If
    Unload Me
End Sub

Private Sub Form_Paint()
    If Me.Text1.Enabled = True Then
        Me.Text1.SetFocus
    Else
        Me.Text3.SetFocus
    End If
End Sub

Private Sub pctCalendar_Click()
'---> Gestion du calendrier
UseCalendar Me.Text1

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
    Dim dateok As Boolean
    
    If KeyAscii = 13 Then
        dateok = ControleDates(Me.Text1)
        If dateok Then Me.Text3.SetFocus
    End If
    
    If KeyAscii = 39 Then KeyAscii = 32
    
End Sub


Private Sub Text3_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then KeyAscii = 0
    
    If KeyAscii = 39 Then KeyAscii = 32
    
End Sub
