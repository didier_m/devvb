VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSalaire 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Suivi salaire employ�"
   ClientHeight    =   7335
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   10950
   Icon            =   "frmSalaire.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7335
   ScaleWidth      =   10950
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   "Donn�es relatives aux salaires"
      ForeColor       =   &H00800000&
      Height          =   7335
      Left            =   5640
      TabIndex        =   15
      Top             =   0
      Width           =   5295
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   1080
         Top             =   6600
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   1
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmSalaire.frx":08CA
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin VB.TextBox b3d_00025 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2160
         TabIndex        =   31
         Tag             =   "FIELD~0001~SALAIRE~25"
         Top             =   4080
         Width           =   735
      End
      Begin VB.TextBox txtAncienete 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3000
         TabIndex        =   30
         Top             =   4080
         Width           =   2175
      End
      Begin VB.PictureBox pctCalendar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   3840
         Picture         =   "frmSalaire.frx":11A4
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   29
         ToolTipText     =   "Choix d'une date"
         Top             =   360
         Width           =   255
      End
      Begin VB.TextBox b3d_00029 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "#�##0,00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1036
            SubFormatType   =   1
         EndProperty
         Enabled         =   0   'False
         Height          =   285
         Left            =   3000
         TabIndex        =   9
         Tag             =   "FIELD~0001~SALAIRE~29"
         Top             =   5040
         Width           =   2175
      End
      Begin VB.CommandButton Command2 
         Appearance      =   0  'Flat
         Height          =   615
         Left            =   3600
         Picture         =   "frmSalaire.frx":12EE
         Style           =   1  'Graphical
         TabIndex        =   13
         Tag             =   "CATALOGUE~51"
         Top             =   6600
         Width           =   735
      End
      Begin VB.CommandButton Command1 
         Height          =   615
         Left            =   4440
         Picture         =   "frmSalaire.frx":1438
         Style           =   1  'Graphical
         TabIndex        =   12
         Tag             =   "CATALOGUE~50"
         Top             =   6600
         Width           =   735
      End
      Begin VB.ComboBox b3d_00020 
         Height          =   315
         Left            =   2160
         TabIndex        =   3
         Tag             =   "FIELDTABLE~SALAIRE~20"
         Top             =   1800
         Width           =   3015
      End
      Begin VB.ComboBox b3d_00019 
         Height          =   315
         Left            =   2160
         TabIndex        =   2
         Tag             =   "FIELDTABLE~SALAIRE~19"
         Top             =   1320
         Width           =   3015
      End
      Begin VB.ComboBox b3d_00018 
         Height          =   315
         Left            =   2160
         TabIndex        =   1
         Tag             =   "FIELDTABLE~SALAIRE~18"
         Top             =   840
         Width           =   3015
      End
      Begin VB.TextBox b3d_00028 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3000
         TabIndex        =   8
         Tag             =   "FIELD~0001~SALAIRE~28"
         Top             =   4560
         Width           =   2175
      End
      Begin VB.TextBox b3d_00027 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3840
         TabIndex        =   11
         Tag             =   "FIELD~0001~SALAIRE~27"
         Top             =   6000
         Width           =   1335
      End
      Begin VB.TextBox b3d_00026 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3840
         TabIndex        =   10
         Tag             =   "FIELD~0001~SALAIRE~26"
         Top             =   5520
         Width           =   1335
      End
      Begin VB.TextBox b3d_00024 
         Height          =   285
         Left            =   3000
         TabIndex        =   7
         Tag             =   "FIELD~0001~SALAIRE~24"
         Top             =   3600
         Width           =   2175
      End
      Begin VB.TextBox b3d_00023 
         Height          =   285
         Left            =   3000
         TabIndex        =   5
         Tag             =   "FIELD~0001~SALAIRE~23"
         Top             =   3120
         Width           =   2175
      End
      Begin VB.TextBox b3d_00022 
         Height          =   285
         Left            =   2160
         TabIndex        =   6
         Tag             =   "FIELD~0001~SALAIRE~22"
         Top             =   2640
         Width           =   3015
      End
      Begin VB.TextBox b3d_00021 
         Height          =   285
         Left            =   2160
         TabIndex        =   4
         Tag             =   "FIELD~0001~SALAIRE~21"
         Top             =   2280
         Width           =   3015
      End
      Begin VB.TextBox b3d_00017 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2160
         TabIndex        =   0
         Tag             =   "FIELD~0001~SALAIRE~17~OB~"
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label Label9 
         Caption         =   "% et Mt Anciennete"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Tag             =   "CATALOGUE~25"
         Top             =   4080
         Width           =   1935
      End
      Begin VB.Label Label14 
         Caption         =   "Salaire / 12 Mois"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Tag             =   "CATALOGUE~28"
         Top             =   4560
         Width           =   3495
      End
      Begin VB.Label Label13 
         Caption         =   "Salaire / 13 Mois "
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Tag             =   "CATALOGUE~29"
         Top             =   5040
         Width           =   3375
      End
      Begin VB.Label Label11 
         Caption         =   "% Aug / Dernier"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   25
         Tag             =   "CATALOGUE~27"
         Top             =   6000
         Width           =   3615
      End
      Begin VB.Label Label10 
         Caption         =   "% Aug / Embauche"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   24
         Tag             =   "CATALOGUE~26"
         Top             =   5520
         Width           =   3495
      End
      Begin VB.Label Label8 
         Caption         =   "Salaire Mensuel"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Tag             =   "CATALOGUE~24"
         Top             =   3600
         Width           =   3375
      End
      Begin VB.Label Label7 
         Caption         =   "Salaire d'embauche"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   22
         Tag             =   "CATALOGUE~23"
         Top             =   3120
         Width           =   1695
      End
      Begin VB.Label Label6 
         Caption         =   "Coefficient"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Tag             =   "CATALOGUE~22"
         Top             =   2640
         Width           =   1575
      End
      Begin VB.Label Label5 
         Caption         =   "Position"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Tag             =   "CATALOGUE~21"
         Top             =   2280
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "Emploi"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Tag             =   "CATALOGUE~20"
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Service"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Tag             =   "CATALOGUE~19"
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "Qualification"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Tag             =   "CATALOGUE~18"
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Date Salaire"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Tag             =   "CATALOGUE~17"
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "EvolutionSalaire"
      ForeColor       =   &H00800000&
      Height          =   7335
      Left            =   0
      TabIndex        =   14
      Top             =   0
      Width           =   5655
      Begin MSComctlLib.ListView ListView 
         Height          =   6975
         Left            =   120
         TabIndex        =   28
         Top             =   240
         Width           =   5415
         _ExtentX        =   9551
         _ExtentY        =   12303
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "POPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuAdd 
         Caption         =   ""
      End
      Begin VB.Menu mnuModif 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDel 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuZoom 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmSalaire"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public aNavigation As B3DNavigation
Public CodeEmploye As String
Public NomEmploye As String
Public DateEnt As String
Public CurrentItem As ListItem
Public CurrentEnreg As B3DEnreg

Dim AncienSalaire As Double


Private Sub b3d_00021_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then Me.b3d_00022.SetFocus
End Sub

Private Sub b3d_00022_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then
    If Me.b3d_00023.Enabled = True Then
        '-> On ne passe sur cette zone que lors de la saisie du 1er salaire
        Me.b3d_00023.SetFocus
    Else
        Me.b3d_00024.SetFocus
    End If
End If
End Sub

Private Sub b3d_00023_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then
    '-> n'effectue pas les calculs si zone a blanc
    If Me.b3d_00023.Text = "" Then Exit Sub
    '-> formattage de la zone
    Me.b3d_00023.Text = Format(Me.b3d_00023.Text, "###,##0.00")
    '-> teste si valeur numerique
    If Not IsNumeric(Me.b3d_00023.Text) Then
        MsgBox "Saisie incorrecte...", vbExclamation
        Exit Sub
    End If
    '-> si premier salaire : on arrete ici la saisie
    If Me.ListView.ListItems.Count = 0 Then
        '-> On affecte le salaire mensuel au salaire d'embauche
        Me.b3d_00024.Text = Me.b3d_00023.Text
        '-> On effectue les calculs de pourcentages
        CalculePourcentages
    Else
        '-> Sinon on passe a la zone suivante
        Me.b3d_00024.SetFocus
    End If
End If
End Sub

Private Sub b3d_00024_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then
    '-> n'effectue pas les calculs si zone a blanc
    If Me.b3d_00024.Text = "" Then Exit Sub
    '-> formattage de la zone
    Me.b3d_00024.Text = Format(Me.b3d_00024.Text, "###,##0.00")
    '-> Si 1er salaire alors on a deja tout calcul� dans la zone precedente
    If Me.b3d_00023.Enabled = True Then Exit Sub
    
    '-> teste si valeur numerique
    If Not IsNumeric(Me.b3d_00024.Text) Then
        MsgBox "Saisie incorrecte...", vbExclamation
        Exit Sub
    End If
    
    '-> On effectue les calculs de pourcentages
    CalculePourcentages
   
End If
End Sub

Private Sub Command1_Click()

Dim strControle As String
Dim strParam As String
Dim strError As String
Dim aEnreg As B3DEnreg

On Error GoTo GestError

'-> Bloquer l'�cran
Screen.MousePointer = 11
Me.Enabled = False

'-> recup d'un enreg � vide si creation sinon on travaille avec l'enreg en cours
If strRetour = "CREATE" Then
    Set aEnreg = aDescro.GetEnreg
    '-> On renseigne les champs de clef pour l'index car _
    enreg � vide et ces donn�es ne sont pas dans le GetScreen
    aEnreg.Fields("00001").ZonLock = "SALAIRE"
    aEnreg.Fields("00002").ZonLock = CodeEmploye
    aEnreg.Fields("00003").ZonLock = NomEmploye
Else
    Set aEnreg = CurrentEnreg
End If

'-> get des champs qui ont chang�
strControle = GetScreen(aEnreg, "SALAIRE", Me, MasterFname, aDescro, FnameJoins)

'-> On reformatte la date
aEnreg.Fields("00017").ZonLock = Entry(3, Me.b3d_00017.Text, "/") & Entry(2, Me.b3d_00017.Text, "/") & Entry(1, Me.b3d_00017.Text, "/")


'-> Create ou update
If strRetour = "UPDATE" Then
    '-> Ok : faire l'update sur l'enreg
    If Not UpdateEnreg(aConnexion, MasterFname, aEnreg, True) Then
        '-> Tester  le code erreur 90026 pas de modif
        If aConnexion.ErrorNumber = "90026" Then GoTo GestError
        '-> Afficher une message d'erreur
        MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
        '-> Fin du programme
        GoTo GestError
    End If
Else
    '-> Faire un create
    If Not CreateEnreg(aConnexion, MasterFname, aEnreg) Then
        '-> Tester  le code erreur 90026 pas de modif
        If aConnexion.ErrorNumber = "90026" Then GoTo GestError
        '-> Afficher une message d'erreur
        MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
        '-> Fin du programme
        GoTo GestError
    End If

End If 'Si create ou update

'-> Mettre � jour l'affichage
DisplayEnregInListView aEnreg, Me.ListView, aDescro

Set Me.ListView.SelectedItem = Me.ListView.ListItems(Me.ListView.ListItems.Count)
Set CurrentItem = Me.ListView.ListItems(Me.ListView.ListItems.Count)
Me.ListView.SelectedItem.SmallIcon = 1

'-> Formatter le listview
FormatListView Me.ListView


GestError:
    '-> Re-initialise la fenetre
    InitFrame
    '-> D�bloquer
    Me.Enabled = True
    Me.ListView.Enabled = True
    Screen.MousePointer = 0
    Exit Sub
    

End Sub

Private Sub Command2_Click()
Dim aIt As ListItem
'-> Si on est rentr� en mode update -> Faire un unlock
If strRetour = "UPDATE" Then UnlockEnreg aConnexion, MasterFname, Entry(2, Me.ListView.SelectedItem.Key, "|")

'-> D�bloquer l'�cran
Me.ListView.Enabled = True

'-> Donner le focus sur le listview
Me.ListView.SetFocus

'-> Re-initialise la fenetre
InitFrame

End Sub


Private Sub Form_Load()

Dim aObj As B3DObject

'-> Charger les libell�s des menus
Me.mnuAdd.Caption = Catalogues("00052").Fill_Label
Me.mnuModif.Caption = Catalogues("00054").Fill_Label
Me.mnuDel.Caption = Catalogues("00053").Fill_Label
Me.mnuZoom.Caption = Catalogues("00055").Fill_Label

End Sub

Private Sub ListView_ItemClick(ByVal Item As MSComctlLib.ListItem)
'---> Affichage d'un salaire
If CurrentItem = Item Then Exit Sub
LoadSalaire Item
Set CurrentItem = Me.ListView.SelectedItem
End Sub

Private Sub listview_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
Dim aIt As ListItem

'-> Quitter si pas bouton droit
If Button <> vbRightButton Then Exit Sub

'-> On se positionne sur le bon element de la liste meme lors d'un click droit
Set aIt = Me.ListView.HitTest(x, y)
If Not aIt Is Nothing Then
    Set Me.ListView.SelectedItem = aIt
    '-> Test si on est deja sur le bon on ne recharge pas
    If CurrentItem <> Me.ListView.SelectedItem Then
        LoadSalaire aIt
        Set CurrentItem = Me.ListView.SelectedItem
    End If
End If

'-> Gestion des menus
If Me.ListView.SelectedItem Is Nothing Then
    '-> Bloquer
    Me.mnuDel.Enabled = False
    Me.mnuModif.Enabled = False
    Me.mnuZoom.Enabled = False
Else
    '-> Lib�rer
    Me.mnuDel.Enabled = True
    Me.mnuModif.Enabled = True
    Me.mnuZoom.Enabled = True
End If

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu Me.mnuPopUp

'-> Selon le type de retour
Select Case strRetour
    Case "CREATE"
        '-> Cr�er un nouvel Enreg
        Call CreateData
    Case "UPDATE"
        '-> Modifier un enreg
        Call EditData
    Case "DELETE"
        '-> Supprimer un enreg
        Call DeleteData
    Case "ZOOM"
        '-> Faire un zoom sur les donn�es
        Call ZoomData
End Select

End Sub

Private Sub mnuAdd_Click()
strRetour = "CREATE"
End Sub

Private Sub mnuDel_Click()
strRetour = "DELETE"
End Sub

Private Sub mnuModif_Click()
strRetour = "UPDATE"
End Sub

Private Sub mnuZoom_Click()
strRetour = "ZOOM"
End Sub

Private Sub DeleteData()

'---> Cette proc�dure supprime un enreg
Dim LibError As String
Dim Rep As VbMsgBoxResult

On Error GoTo GestError

'-> Demander confirmation
Rep = MsgBox(Catalogues("00059").Fill_Label, vbExclamation + vbYesNo, Catalogues("00053").Fill_Label)
If Rep <> vbYes Then Exit Sub

'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11

'-> Faire un DeleteData
Rep:
If Not DeleteEnreg(aConnexion, MasterFname, Entry(2, Me.ListView.SelectedItem.Key, "|")) Then
    '-> Tester si l'enregistrement n'est pas lock� par un autre
    If aConnexion.ErrorNumber = "90006" Then
        '-> v�rifier si le lock est actif
        If Entry(4, aConnexion.ErrorParam, "�") = "0" Then
            '-> Envoyer un B3D unlock
            If UnlockEnreg(aConnexion, MasterFname, Entry(2, Me.ListView.SelectedItem.Key, "|")) Then
                '-> Reprise
                GoTo Rep
            End If
        Else
            '-> Message de lock
            LibError = Catalogues("00058").Fill_Label
            LibError = Replace(LibError, "$#EN#$", Entry(2, Me.ListView.SelectedItem.Key, "|"))
            LibError = Replace(LibError, "$#OP#$", Entry(1, aConnexion.ErrorParam, "�"))
            LibError = Replace(LibError, "$#DAT#$", Entry(2, aConnexion.ErrorParam, "�"))
            LibError = Replace(LibError, "$#PRO#$", Entry(3, aConnexion.ErrorParam, "�"))
            MsgBox LibError, vbExclamation + vbOKOnly, "Error"
        End If
        '-> Fin de la fonction
        GoTo GestError
    End If
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter le programme
    GoTo GestError
End If

'-> Supprimer l'enreg du browse
Me.ListView.ListItems.Remove (Me.ListView.SelectedItem.Key)

'-> Faire un clear de la frame
InitFrame

GestError:
    '-> Bloquer les enreg
    Me.Enabled = True
    Screen.MousePointer = 0

End Sub

Private Sub CreateData()
'---> Cette proc�dure donne acc�s � la modification de l'�cran

On Error GoTo GestError

'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11

'-> creer un enregistrement a vide pour creation
Set CurrentEnreg = aDescro.GetEnreg

'-> Bloquer le browse
Me.ListView.Enabled = False

If Me.ListView.ListItems.Count > 0 Then
    '-> On precharge le dernier salaire rentr� comme base de saisie
    LoadSalaire Me.ListView.ListItems(Me.ListView.ListItems.Count)
    '-> On recupere l'ancien salaire pour calcul des pourcentages d'augmentation
    AncienSalaire = CDbl(Me.b3d_00024.Text)
Else
    '-> On vide la fenetre
    ClearFrame
End If

'-> Debloquer les zones � saisir et les boutons
LockZones True

GestError:
    '-> Bloquer les enreg
    Me.Enabled = True
    Screen.MousePointer = 0
End Sub

Private Sub EditData()
'---> Cette proc�dure donne acc�s � la modification de l'�cran
Dim LibError As String

On Error GoTo GestError

'-> Bloquer les enregs
Me.Enabled = False
Screen.MousePointer = 11

'-> Faire un FindData sur l'enregistrement en cours
Rep:
Set CurrentEnreg = FindData(aConnexion, aConnexion.Ident, MasterFname, Entry(2, Me.ListView.SelectedItem.Key, "|"), True, False, aDescro, False)

'-> Tester le retour
If CurrentEnreg Is Nothing Then
    '-> Tester si l'enregistrement n'est pas lock� par un autre
    If aConnexion.ErrorNumber = "90006" Then
        '-> v�rifier si le lock est actif
        If Entry(4, aConnexion.ErrorParam, "�") = "0" Then
            '-> Envoyer un B3D unlock
            If UnlockEnreg(aConnexion, MasterFname, Entry(2, Me.ListView.SelectedItem.Key, "|")) Then
                '-> Reprise
                GoTo Rep
            End If
        Else
            '-> Message de lock
            LibError = Catalogues("00058").Fill_Label
            LibError = Replace(LibError, "$#EN#$", Entry(2, Me.ListView.SelectedItem.Key, "|"))
            LibError = Replace(LibError, "$#OP#$", Entry(1, aConnexion.ErrorParam, "�"))
            LibError = Replace(LibError, "$#DAT#$", Entry(2, aConnexion.ErrorParam, "�"))
            LibError = Replace(LibError, "$#PRO#$", Entry(3, aConnexion.ErrorParam, "�"))
            MsgBox LibError, vbExclamation + vbOKOnly, "Error"
        End If
        '-> Fin de la fonction
        GoTo GestError
    End If
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter le programme
    GoTo GestError
End If

'-> Basculer zondata dans zonlock
SetZonLockFromZonData CurrentEnreg

'-> debloquer les zones
LockZones True

'-> Bloquer le browse
Me.ListView.Enabled = False

GestError:
    '-> Bloquer les enreg
    Me.Enabled = True
    Screen.MousePointer = 0
End Sub

Private Function ClearFrame()

Dim x As Control

On Error Resume Next

For Each x In Me.Controls
    '-< Selon le type d'objet
    If TypeOf x Is TextBox Then
        x.Text = ""
    ElseIf TypeOf x Is ComboBox Then
        x.ListIndex = -1
        x.Text = ""
    End If
Next 'Pour tous les controles

End Function

Private Function ZoomData()

'-> Afficher le zoom
DisplayZoom aConnexion, MasterFname, Entry(2, Me.ListView.SelectedItem.Key, "|"), True, False, aDescro, Me, True

End Function

Public Sub LoadSalaire(ByVal Item As MSComctlLib.ListItem)
'---> Permet de charger toutes les zones d'un salaire
Dim MyEnreg As B3DEnreg

'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11

'-> Faire un FindData sur l'enregistrement en cours
Set MyEnreg = FindData(aConnexion, aConnexion.Ident, MasterFname, Entry(2, Item.Key, "|"), False, False, aDescro, False)

'-> Tester le retour
If MyEnreg Is Nothing Then
    '-> Afficher un message d'erreur
    If aConnexion.ErrorNumber = "90006" Then
        MsgBox Catalogues("00056"), vbExclamation + vbOKOnly, "Error"
    Else
        MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    End If
    '-> fin du progr
    GoTo GestError
Else
    Set CurrentEnreg = MyEnreg
End If

'-> Afficher les donn�es
LoadScreen MyEnreg, "SALAIRE", Me, MasterFname, FnameJoins, aDescro, "UPDATE"

'-> recup du dernier salaire pour calculs
If Item.Index >= 2 Then
    Set MyEnreg = FindData(aConnexion, aConnexion.Ident, MasterFname, Entry(2, Me.ListView.ListItems(Item.Index - 1).Key, "|"), False, False, aDescro, False)
    AncienSalaire = MyEnreg.Fields("00024").ZonData
Else
    AncienSalaire = 0
End If

'-> On calcule les zones de calcul pour l'affichage
CalculeAnciennete
CalculePourcentages

GestError:
    '-> Bloquer les enreg
    Me.Enabled = True
    Screen.MousePointer = 0
End Sub

Public Sub LockZones(ByVal Bloque As Boolean)
'---> permet d'activer ou non certaines zones de saisie
Me.b3d_00018.Enabled = Bloque
Me.b3d_00019.Enabled = Bloque
Me.b3d_00020.Enabled = Bloque
Me.b3d_00021.Enabled = Bloque
Me.b3d_00022.Enabled = Bloque
Me.b3d_00023.Enabled = Bloque
Me.b3d_00024.Enabled = Bloque

'-> Si plusieurs salaires ou si modif : on bloque la zone de salaire d'embauche
If Me.ListView.ListItems.Count > 0 Then Me.b3d_00023.Enabled = False

'-> De meme pour les boutons de controle
Me.Command1.Enabled = Bloque
Me.Command2.Enabled = Bloque

'-> Idem calendrier
Me.pctCalendar.Enabled = Bloque

End Sub

Private Sub InitFrame()
'---> permet de re-initialiser la frame apres update ou create
Dim aIt As ListItem

'-> Bloque les zones
LockZones False


'-> Si pas de salaire on sort
If Me.ListView.ListItems.Count = 0 Then Exit Sub

'-> sinon : Charger le dernier en visu
Set aIt = Me.ListView.ListItems(Me.ListView.ListItems.Count)
Set Me.ListView.SelectedItem = aIt
'-> On charge
LoadSalaire aIt

End Sub

Private Sub pctCalendar_Click()
'---> saisie de la date dans le calendrier

strRetour2 = ""
'-> test si retour = ""
Do While strRetour2 = ""
    UseCalendar Me.b3d_00017
    If strRetour2 = "BLANC" Or strRetour2 = "" Then
        MsgBox "Vous devez saisir une date de salaire...", vbExclamation
        strRetour2 = ""
    End If
Loop

'-> Calcul du %anciennete
CalculeAnciennete

End Sub

Private Sub CalculeAnciennete()
'---> Permet de calculer le pourcentage d'anciennete _
    a appliquer sur le salaire mensuel
    
If Trim(Me.b3d_00017.Text) <> "" And Me.DateEnt <> "" Then
    If CDate(Me.b3d_00017.Text) - CDate(Me.DateEnt) >= 365 Then
        If CInt((CDate(Me.b3d_00017.Text) - CDate(Me.DateEnt) + 1) / 365) <= 5 Then
            Me.b3d_00025.Text = Format(CInt((CDate(Me.b3d_00017.Text) - CDate(Me.DateEnt) + 1) / 365), "0.00")
        Else
            Me.b3d_00025.Text = "5.00"
        End If
    Else
        Me.b3d_00025.Text = "0.00"
    End If
End If

End Sub

Private Sub CalculePourcentages()
'---> permet de calculer les pourcentages d'augmentation et le Mt Anciennete

'-> teste si zones a blanc
If Me.b3d_00024.Text = "" Or Me.b3d_00025.Text = "" Or Me.b3d_00025.Text = "" Then Exit Sub

'-> Calcul du Mt anciennete
Me.txtAncienete.Text = Format((Me.b3d_00024.Text * Me.b3d_00025.Text) / 100, "###,##0.00")

'-> calcul du salaire / 13
Me.b3d_00029.Text = Format(CDbl(Me.b3d_00024.Text) + CDbl(Me.txtAncienete.Text), "###,##0.00")

'-> calcul du salaire / 12
Me.b3d_00028.Text = Format(Round((CDbl(Me.b3d_00024.Text) + CDbl(Me.txtAncienete.Text)) * 13 / 12, 2), "###,##0.00")

'-> Pourcentage d'augmentation / salaire d'embauche
Me.b3d_00026.Text = Format(Round(((Me.b3d_00024.Text / Me.b3d_00023.Text) - 1) * 100, 2), "###,##0.00")

'-> Pourcentage par rapport au dernier salaire
If AncienSalaire <> 0 Then
    Me.b3d_00027.Text = Format(Round(((Me.b3d_00024.Text / AncienSalaire) - 1) * 100, 2), "###,##0.00")
Else
    Me.b3d_00027.Text = "0.00"
End If

End Sub


