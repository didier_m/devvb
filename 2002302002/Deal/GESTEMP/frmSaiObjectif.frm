VERSION 5.00
Begin VB.Form frmSaiObjectif 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   1980
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8190
   Icon            =   "frmSaiObjectif.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1980
   ScaleWidth      =   8190
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      Caption         =   "Objectif Employ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1935
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8175
      Begin VB.PictureBox pctCalendar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   1
         Left            =   3600
         Picture         =   "frmSaiObjectif.frx":08CA
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   10
         ToolTipText     =   "Choix d'une date"
         Top             =   1080
         Width           =   255
      End
      Begin VB.PictureBox pctCalendar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   0
         Left            =   3600
         Picture         =   "frmSaiObjectif.frx":0A14
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   9
         ToolTipText     =   "Choix d'une date"
         Top             =   360
         Width           =   255
      End
      Begin VB.TextBox Text4 
         Height          =   285
         Left            =   1920
         TabIndex        =   7
         Top             =   1440
         Width           =   6135
      End
      Begin VB.TextBox txtDate 
         Enabled         =   0   'False
         Height          =   285
         Index           =   0
         Left            =   1920
         MaxLength       =   10
         TabIndex        =   3
         Top             =   360
         Width           =   1575
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   1920
         TabIndex        =   2
         Top             =   720
         Width           =   6135
      End
      Begin VB.TextBox txtDate 
         Enabled         =   0   'False
         Height          =   285
         Index           =   1
         Left            =   1920
         MaxLength       =   10
         TabIndex        =   1
         Top             =   1080
         Width           =   1575
      End
      Begin VB.Label Label4 
         Caption         =   "Libell� Fin"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   8
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Date de d�but d'objectif"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   2175
      End
      Begin VB.Label Label2 
         Caption         =   "Libell� D�but"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   720
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "Date de fin d'objectif"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   1080
         Width           =   1815
      End
   End
End
Attribute VB_Name = "frmSaiObjectif"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Paint()
    If Me.txtDate(0).Enabled = True Then
        Me.txtDate(0).SetFocus
    Else
        Me.Text2.SetFocus
    End If
End Sub

Private Sub pctCalendar_Click(Index As Integer)
UseCalendar Me.txtDate(Index)
End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    Me.Text4.SetFocus
End If
End Sub

Private Sub Text4_KeyPress(KeyAscii As Integer)
    Dim i As Integer
    Dim SaisieOk As Boolean
    
    SaisieOk = True
    
    If KeyAscii = 39 Then KeyAscii = 32
    If KeyAscii = 13 Then
'        For i = 0 To Me.txtDate.Count - 1
'            If Me.txtDate(i).Text = "" Then SaisieOk = False
'        Next
        If SaisieOk = True Then strRetour = Trim(Me.txtDate(0).Text) & "�" & Trim(Me.Text2.Text) & "�" & Trim(Me.txtDate(1).Text) & "�" & Trim(Me.Text4.Text)
        Unload Me
    End If
End Sub
