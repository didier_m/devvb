Attribute VB_Name = "fctGesEmp"
Option Explicit

'-> Permet de recuperer les coordonn�es du curseur souris
Public Type POINTAPI
        x As Long
        y As Long
End Type

'-> Chemin de turbo
Public PrinterPath As String

'-> Constantes pour les API
Public Const SM_CYBORDER = 6
Public Const SM_CXBORDER = 5
Public Const SM_CXVSCROLL = 2

'-> API de gestion evenements utilisateur
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function ScreenToClient Lib "user32" (ByVal hwnd As Long, lpPoint As POINTAPI) As Long
Public Declare Function GetSystemMetrics Lib "user32" (ByVal nIndex As Long) As Long

'-> teste une reference de fenetre
Public Declare Function IsWindow Lib "user32" (ByVal hwnd As Long) As Long

'-> Collection des tables jointes
Public FnameJoins As Collection

'-> Pointeur vers la connexion active
Public aConnexion As B3DConnexion

'-> Variable d'echange pour le calendrier
Public strRetour2 As String

'-> Image de l'mploye en cours pour edition
Public CurrentImg As String

'-> Pointeur vers le descriptif de la table maitresse
Public aDescro As B3DFnameDescriptif

'-> Pointeur vers l'objet de navigation
Public aNavigation As B3DNavigation

'-> Table principale du programme
Public MasterFname As String

Sub Main()

'---> analyse de la ligne de command

Dim ZonCharge As String
Dim ParamFile As String
Dim ListeField As String
Dim aIt As ListItem
Dim lpBuffer As String
Dim res As Long

'-> Tester la ligne de commande
If Trim(Command$) = "" Then End

'-> R�cup�rer les param�tres
ParamFile = Entry(1, Command$, "�")
ZonCharge = Entry(2, Command$, "�")
            
'-> Initialiser la connexion
Set aConnexion = New B3DConnexion

'-> Charger son param�trage
If Not aConnexion.ReadParamFormFile(ParamFile) Then End
            
'-> Charger la feuille
mdiGesEmp.Show
mdiGesEmp.Enabled = False
              
'-> Affichage de la tempo
frmWait.Label1 = "Chargement des employ�s . Veuillez patienter..."
'-> On lance la tempo
frmWait.Show
            
'-> Recup du chemin de l'editeur turbo
lpBuffer = Space$(500)
res = GetPrivateProfileString("TURBO", "PATH", "Erreur", lpBuffer, Len(lpBuffer), App.Path & "\DATA\GESTEMP.INI")
'-> Emplacement de turbo
PrinterPath = Mid$(lpBuffer, 1, res)
            
'-> Poser le nom du programme
InitialiseB3D aConnexion, "DEAL_GESTEMP", False, False
             
'-> Postionner le code table principal
MasterFname = Trim(Entry(1, ZonCharge, "~"))

'-> R�cup�rer le catalogue
If Not GetIdentCatalogue(aConnexion, aConnexion.Ident, "") Then
    '-> Afficher un message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbCritical + vbOKOnly, "Error"
    '-> Fin du programme
    End
End If
                        
'-> R�cup�rer les descriptif de la table
Set aDescro = GetFnameDescriptif(aConnexion, aConnexion.Ident, MasterFname)
                        
If aDescro Is Nothing Then
    '-> Afficher un message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbCritical + vbOKOnly, "Error"
    '-> Fin du programme
    End
End If

'-> R�cup�ration des tables jointes
Set FnameJoins = New Collection
GetFieldTableFromVecteur aConnexion, FnameJoins, aConnexion.Ident, "18,19,20"

'-> Cr�ation de la liste des champs � afficher en navigation
ListeField = "00002�1�00003�1�00014�1"

'-> Cr�er un objet de navigation
Set aNavigation = InitNaviga(aConnexion, aConnexion.Ident, MasterFname, "BUFFER_0001", "SELECT * FROM [DEALB3D_INDEX]  WHERE [ID_FNAME] = '" & aConnexion.Ident & "�" & MasterFname & "' AND [INAME] = '001' AND [ZON_INDEX_1] = 'EMPLOYE'", "", ListeField, "", 5000, 5000, aDescro)

'-> Initialiser la navigation
CreateListViewNaviga aNavigation, mdiGesEmp.ListView1
                   
'-> On cree des icones pour le listview
For Each aIt In mdiGesEmp.ListView1.ListItems
    aIt.SmallIcon = 5
Next
                   
'-> On selectionne le premier
If mdiGesEmp.ListView1.ListItems.Count > 0 Then
    Set mdiGesEmp.ListView1.SelectedItem = mdiGesEmp.ListView1.ListItems(1)
End If
                   
'-> Afficher la fen�tre
mdiGesEmp.Picture1_Resize
mdiGesEmp.Show
mdiGesEmp.Enabled = True

'-> D�charger la feuille de temporisation
Unload frmWait
        
End Sub

Public Function ControleDates(ByRef tText As TextBox) As Boolean
'->Test si format de dates respect�
ControleDates = True
If NumEntries(tText, "/") <> 3 Then
    MsgBox "Format de la date incorrect : 'XX/XX/XXXX' impos�...", vbExclamation
    tText.Text = ""
    tText.Visible = True
    tText.SetFocus
    ControleDates = False
    Exit Function
End If

'-> Test si jour correct
If Len(Entry(1, tText, "/")) <> 2 Then
    MsgBox "Format du jour incorrect...", vbExclamation
    ControleDates = False
    Exit Function
End If
    
If Int(Entry(1, tText, "/")) > 31 Or Int(Entry(1, tText, "/")) < 1 Then
    MsgBox "Format du jour incorrect...", vbExclamation
    ControleDates = False
    Exit Function
End If

'-> Test si mois correct

If Len(Entry(2, tText, "/")) <> 2 Then
    MsgBox "Format du mois incorrect...", vbExclamation
    ControleDates = False
    Exit Function
End If

If Entry(2, tText, "/") <> "" Then
    If Int(Entry(2, tText, "/")) > 12 Or Int(Entry(2, tText, "/")) < 1 Then
        MsgBox "Format du mois incorrect...", vbExclamation
        ControleDates = False
        Exit Function
    End If
Else
    MsgBox "Format du mois incorrect...", vbExclamation
    ControleDates = False
    Exit Function
End If

'-> test si annee correcte

If Len(Entry(3, tText, "/")) <> 4 Then
    MsgBox "Format de l'ann�e incorrect...", vbExclamation
    ControleDates = False
    Exit Function
End If

If Entry(3, tText, "/") <> "" Then
    If Int(Entry(3, tText, "/")) > 2050 Or Int(Entry(3, tText, "/")) < 1850 Then
        MsgBox "Format de l'ann�e incorrect...", vbExclamation
        ControleDates = False
        Exit Function
    End If
Else
    MsgBox "Format de l'ann�e incorrect...", vbExclamation
    ControleDates = False
    Exit Function
End If

End Function

Public Sub ControleZone(tText As TextBox)
    '---> permet de remplacer ' par un espace
    
    Replace tText.Text, "'", " "
    
End Sub

Public Sub PrintDataInTurbo(MaqName As String, DataFile As String, MergeFile As String, Delete As Boolean)
'---> Fonction qui edite des donn�es B3D en fonction d'une maquette turbo et
'---> d'un fichier de donn�es fusionn�s ensembles
'---> Propose aussi la suppression du spool et du fichier de fucion si demand� par programmeur
Dim HdlFile1 As Integer
Dim HdlFile2 As Integer
Dim HdlFile3 As Integer
Dim LigneI As String
Dim LigneE As String
Dim i As Integer


'-> Ouverture de la maquette
HdlFile1 = FreeFile
Open MaqName For Input As #HdlFile1
'-> Ouverture du fichier de donn�es
HdlFile2 = FreeFile
Open DataFile For Input As #HdlFile2
'-> Ouverture du fichier de fusion
HdlFile3 = FreeFile
Open MergeFile For Output As #HdlFile3

'-> Envoi du marqueur de debut de spool dans le fichier .TURBO
Print #HdlFile3, "[SPOOL]"
'-> Envoi du marqueur de debut de maquette dans le fichier .TURBO
Print #HdlFile3, "[MAQ]"

'-> On parcours la maquette et on la duplique dans notre fichier de fusion
Do While Not EOF(HdlFile1)
    '-> recup de la ligne de maquette
    Line Input #HdlFile1, LigneI
    '-> Ecriture de cette ligne dans le fichier de fusion
    Print #HdlFile3, LigneI
Loop
'-> Envoi du marqueur de fin de maquette dans le fichier .TURBO
Print #HdlFile3, "[/MAQ]"

'-> On commence a envoyer dans le fichier de fusion les donn�es du fichier de datas
Do While Not EOF(HdlFile2)
    '-> Recup d'une ligne de data
    Line Input #HdlFile2, LigneI
    '-> ecriture dans le fichier
    Print #HdlFile3, LigneI
Loop

'-> Envoi du marqueur de fin de spool dans le fichier .TURBO
Print #HdlFile3, "[/SPOOL]"

'-> On ferme les canaux
Close #HdlFile1
Close #HdlFile2
Close #HdlFile3

'->on lance l'edition par turbograph
Shell PrinterPath & " " & MergeFile, vbMaximizedFocus

End Sub

Public Sub UseCalendar(ZoneToFill As Object)
'---> Permet d'afficher le calendrier et de faire une saisie de date _
      pour une zone donn�e
      
strRetour2 = ""
frmCalendrier.Show vbModal

'-> chargement de la zone
If strRetour2 = "BLANC" Then
    ZoneToFill.Text = ""
Else
    ZoneToFill.Text = strRetour2
End If
    
End Sub

Public Function CalculAgeAnciennete(DateEntree As String, DateNaiss As String) As String
'---> permet de calculer l'age et l'anciennete en fct des dates d'entree et de naissance
Dim Age As String
Dim Ancien As String
Dim Annee As Integer
Dim Mois As Integer


If DateNaiss <> "" Then
    '-> Calcul de l'age
    Annee = Fix(DateDiff("m", DateNaiss, Now) / 12)
    Mois = DateDiff("m", DateNaiss, Now) - (Annee * 12)
    Age = Annee & " ans et " & Mois & " mois "
End If

If DateEntree <> "" Then
    '-> Calcul de l'anciennete
    Annee = Fix(DateDiff("m", DateEntree, Now) / 12)
    Mois = DateDiff("m", DateEntree, Now) - (Annee * 12)
    Ancien = Annee & " ans et " & Mois & " mois"
End If
    
CalculAgeAnciennete = Age & "," & Ancien

End Function
