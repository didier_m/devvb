VERSION 5.00
Begin VB.Form frmSaiPrime 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   1590
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6045
   Icon            =   "frmSaiPrime.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1590
   ScaleWidth      =   6045
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      Caption         =   "Prime Employ�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1575
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6015
      Begin VB.PictureBox pctCalendar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   3000
         Picture         =   "frmSaiPrime.frx":0442
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   7
         ToolTipText     =   "Choix d'une date"
         Top             =   360
         Width           =   255
      End
      Begin VB.TextBox Text3 
         Height          =   285
         Left            =   1440
         TabIndex        =   3
         Tag             =   "FIELD~0001~PRIME~0030"
         Top             =   1080
         Width           =   4455
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   1440
         TabIndex        =   2
         Tag             =   "FIELD~0001~PRIME~0031"
         Top             =   720
         Width           =   1815
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   285
         Left            =   1440
         TabIndex        =   1
         Tag             =   "FIELD~0001~PRIME~0032"
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label Label3 
         Caption         =   "Libelle Prime"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Montant Prime"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   720
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Date de Prime"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmSaiPrime"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Paint()
    If Me.Text1.Enabled = True Then
        Me.Text1.SetFocus
    Else
        Me.Text2.SetFocus
    End If
End Sub

Private Sub pctCalendar_Click()
UseCalendar Me.Text1
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 39 Then KeyAscii = 32
    If KeyAscii = 13 Then
        Me.Text2.SetFocus
    End If
End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 39 Then KeyAscii = 32
    If KeyAscii = 13 Then
        If Not IsNumeric(Me.Text2.Text) Then
            MsgBox "Saisie incorrecte...", vbExclamation
            Me.Text2.SetFocus
            Exit Sub
        End If
        Me.Text2.Text = Format(Me.Text2.Text, "###,##0.00")
        Me.Text3.SetFocus
    End If
End Sub

Private Sub Text3_KeyPress(KeyAscii As Integer)
    If KeyAscii = 39 Then KeyAscii = 32
    If KeyAscii = 13 Then
        If Me.Text1.Text <> "" And Me.Text2.Text <> "" And Me.Text3.Text <> "" Then
            strRetour = Trim(Me.Text1.Text) & "�" & Trim(Me.Text2.Text) & "�" & Trim(Me.Text3.Text)
        End If
        Unload Me
    End If
End Sub
