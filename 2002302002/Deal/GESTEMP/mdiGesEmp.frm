VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.MDIForm mdiGesEmp 
   BackColor       =   &H00808080&
   Caption         =   "DEAL INFORMATIQUE - GESTION DES EMPLOYES "
   ClientHeight    =   10830
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   12720
   Icon            =   "mdiGesEmp.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Picture2 
      Align           =   3  'Align Left
      Height          =   10830
      Left            =   5025
      MousePointer    =   9  'Size W E
      ScaleHeight     =   10830
      ScaleWidth      =   45
      TabIndex        =   3
      Top             =   0
      Width           =   45
   End
   Begin VB.PictureBox Picture1 
      Align           =   3  'Align Left
      BorderStyle     =   0  'None
      Height          =   10830
      Left            =   0
      ScaleHeight     =   722
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   335
      TabIndex        =   1
      Top             =   0
      Width           =   5025
      Begin VB.PictureBox picTitre 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         Height          =   90
         Left            =   0
         ScaleHeight     =   6
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   128
         TabIndex        =   2
         Top             =   0
         Width           =   1920
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   6615
         Left            =   0
         TabIndex        =   0
         Top             =   240
         Width           =   5055
         _ExtentX        =   8916
         _ExtentY        =   11668
         View            =   3
         LabelEdit       =   1
         SortOrder       =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.Image Image1 
         Height          =   165
         Left            =   4680
         Picture         =   "mdiGesEmp.frx":0CCA
         ToolTipText     =   "fermer le navigateur client "
         Top             =   0
         Width           =   165
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7440
      Top             =   2040
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiGesEmp.frx":0E98
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiGesEmp.frx":1B72
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiGesEmp.frx":244C
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiGesEmp.frx":3126
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiGesEmp.frx":3E00
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiGesEmp.frx":4ADA
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiGesEmp.frx":57B4
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFichier 
      Caption         =   "Fichier"
      Begin VB.Menu mnuFermer 
         Caption         =   "Fermer"
      End
   End
   Begin VB.Menu mnuFenetre 
      Caption         =   "Fen�tre"
      Begin VB.Menu mnuNavCli 
         Caption         =   "Navigateur Client"
      End
      Begin VB.Menu mnusep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCascade 
         Caption         =   "Fenetres En Cascade "
      End
      Begin VB.Menu mnuMosHor 
         Caption         =   "Fen�tre en Mosa�que Verticale"
      End
      Begin VB.Menu mnuMosVert 
         Caption         =   "Fen�tre en Mosa�que Horizontale"
      End
   End
   Begin VB.Menu mnuHisto 
      Caption         =   "Historique des employ�s"
   End
   Begin VB.Menu mnuPopUp 
      Caption         =   "POPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuVisu 
         Caption         =   "Visualisation employ�"
      End
      Begin VB.Menu mnusep0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAjoutEmp 
         Caption         =   "Ajouter Employ�"
      End
      Begin VB.Menu mnuModifEmp 
         Caption         =   "Modifier Employ�"
      End
      Begin VB.Menu mnuSuppEmp 
         Caption         =   "Supprimer Employ�"
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuRecMat 
         Caption         =   "Recherche sur Matricule"
      End
      Begin VB.Menu mnuRecNom 
         Caption         =   "Recherche sur Nom"
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuZoom 
         Caption         =   "Zoom Donn�es"
      End
   End
End
Attribute VB_Name = "mdiGesEmp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Deplace_Ok As Boolean

Private Sub LockMenu(Blocage As Integer)
'--- >Permet de bloquer ou debloquer les menus popup employ�
Me.mnuModifEmp.Enabled = CBool(Blocage)
Me.mnuSuppEmp.Enabled = CBool(Blocage)
Me.mnuZoom.Enabled = CBool(Blocage)
Me.mnuRecMat.Enabled = CBool(Blocage)
Me.mnuRecNom.Enabled = CBool(Blocage)
End Sub

Private Sub ListView1_DblClick()
'---> acces par double-click � la visu d'un employ�
If Me.ListView1.SelectedItem.Tag = "" Then
    LoadEmploye
Else
    SendMessage Me.ListView1.SelectedItem.Tag, WM_CHILDACTIVATE, 0, 0
End If
End Sub

Private Sub ListView1_KeyPress(KeyAscii As Integer)
'---> Permet d'ouvrir la fenetre employ� en faisant "ENTER" sur un employe
If KeyAscii = 13 Then
    LoadEmploye "VISU"
End If
End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
Dim aIt As ListItem


'-> Tester le bouton
If Button <> vbRightButton Then Exit Sub

'-> Tester s'il y a un employ� au moins
If Me.ListView1.ListItems.Count = 0 Then
    '-> Bloquer tous les menus sauf ajouter
    LockMenu 0
Else
    '-> D�bloquer les menus
    LockMenu -1
End If

'-> On se positionne sur le bon element de la liste meme lors d'un click droit
Set aIt = Me.ListView1.HitTest(x, y)
If Not aIt Is Nothing Then Set Me.ListView1.SelectedItem = aIt

'-> Vider la variable de retour
strRetour = ""

'-> Afficher le menu
Me.PopupMenu Me.mnuPopUp

'-> Tester le retour
Select Case strRetour
    
    Case "CREATE"
        '-> ajout d'un employe
        Call CreateEmploye
    
    Case "ZOOM"
        '-> Afficher un zoom sur la feuille
        Call Zoom
        
    Case "SEARCHMAT"
        '-> Recherche par maticule
        Call FindEmp(0)
        
    Case "SEARCHNAME"
        '-> Recher sur le nom
        Call FindEmp(1)
        
    Case "VISU"
        '---> On affiche la fenetre employ�
        If Not Me.ListView1.SelectedItem Is Nothing Then
            If Me.ListView1.SelectedItem.Tag = "" Then
                LoadEmploye
            Else
                SendMessage Me.ListView1.SelectedItem.Tag, WM_CHILDACTIVATE, 0, 0
            End If
        End If
        
    Case "UPDATE"
        '---> On affiche la fenetre employ� en modif : acces aux zones
        If Me.ListView1.SelectedItem.Tag = "" Then
            LoadEmploye "UPDATE"
        Else
            SendMessage Me.ListView1.SelectedItem.Tag, WM_CHILDACTIVATE, 0, 0
        End If
    
    Case "DELETE"
        '---> On lance la suppression d'un employ�
        Call DeleteEmploye
    
End Select 'Selon le memnu


'-> Vider la varaible de retour
strRetour = ""


End Sub
Private Sub FindEmp(TypeSearch As Integer)
'---> Permet de rechercher un employ� sur son matricule et de le pointer dans la liste
Dim aList As ListItem
Dim Rep As String
Dim ValueRef As String

'-> deamnder le crit�re de recherche
Rep = InputBox("Valeur recherch�e :", "Recherche", "")
If Trim(Rep) = "" Then Exit Sub

'-> On pointe sur le matricule dans la liste
For Each aList In Me.ListView1.ListItems
    If TypeSearch = 0 Then
        ValueRef = aList.Text
    Else
        ValueRef = Entry(1, aList.SubItems(1), " ")
    End If
    If Trim(UCase$(Rep)) = Trim(UCase$(ValueRef)) Then
        Set Me.ListView1.SelectedItem = aList
        aList.EnsureVisible
        Me.ListView1.SetFocus
    End If
Next
End Sub


Private Sub Zoom()

'---> Cette fonction effectue un zoom sur un enregistrement

DisplayZoom aConnexion, MasterFname, Entry(2, Me.ListView1.SelectedItem.Key, "|"), False, True, aDescro, Me, True

End Sub

Private Sub MDIForm_Unload(Cancel As Integer)
End
End Sub

Private Sub mnuMosHor_Click()
    Me.Arrange 1
End Sub

Private Sub mnuCascade_Click()
    Me.Arrange 0
End Sub

Private Sub Image1_Click()
Me.ListView1.Visible = False
Me.Picture1.Visible = False
End Sub

Private Sub listview1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    '-> Trier sur les entetes de colonne
    Me.ListView1.SortKey = ColumnHeader.Index - 1
    If Me.ListView1.SortOrder = lvwAscending Then
        Me.ListView1.SortOrder = lvwDescending
    Else
        Me.ListView1.SortOrder = lvwAscending
    End If
    Me.ListView1.Sorted = True
End Sub

Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Dim Rep As VbMsgBoxResult

Rep = MsgBox("Quitter maintenant ? ", vbQuestion + vbYesNo)

If Rep = vbNo Then
    Cancel = 1
    Exit Sub
End If
    
End Sub

Private Sub MDIForm_Resize()
'-> Definition de l'ecran
Dim aRect As RECT
Dim res As Long
res = GetClientRect(Me.hwnd, aRect)
Me.ListView1.Height = Me.Picture1.ScaleY(aRect.Bottom, 3, 1)
End Sub


Private Sub mnuAjoutEmp_Click()
'-> Ajout d'un employ�
strRetour = "CREATE"
End Sub

Private Sub mnuMosVert_Click()
Me.Arrange 2
End Sub

Private Sub mnuNavCli_Click()
Me.ListView1.Visible = True
Me.Picture1.Visible = True
End Sub

Private Sub mnuFermer_Click()
Unload Me
End Sub
Private Sub mnuModifComment_Click()
strRetour = "UPDATE"
End Sub

Private Sub mnuModifEmp_Click()
strRetour = "UPDATE"
End Sub

Private Sub mnuRecMat_Click()
strRetour = "SEARCHMAT"
End Sub

Private Sub mnuRecNom_Click()
'-> Renvoyer le code menu
strRetour = "SEARCHNAME"
End Sub

'-----------------------------------------------------
' ------ GESTION DES DIFFERENTS MENUS POPUP ----------
'-----------------------------------------------------



Private Sub mnuSuppEmp_Click()
'---> Suppression d'un employ�
strRetour = "DELETE"
End Sub

Private Sub mnuVisu_Click()
'---> Visu employ�
strRetour = "VISU"
End Sub

Private Sub mnuZoom_Click()
strRetour = "ZOOM"
End Sub



Public Sub Picture1_Resize()

Dim aRect As RECT

GetClientRect Me.Picture1.hwnd, aRect
                
'-> Positionner l'entete de la fen�tre
Me.Image1.Left = aRect.Right - Image1.Width - 5
Me.picTitre.Left = 3
Me.picTitre.Top = 7
Me.picTitre.Width = Image1.Left - 6
Me.Image1.Top = Me.picTitre.Top - 2
    
Me.picTitre.Line (0, 0)-(Me.picTitre.Width - 1, 0), RGB(255, 255, 255)
Me.picTitre.Line (0, 1)-(Me.picTitre.Width - 1, 1), RGB(255, 255, 255)
Me.picTitre.Line (1, 1)-(Me.picTitre.Width - 1, 1), &HE0E0E0
Me.picTitre.Line (0, 3)-(Me.picTitre.Width - 1, 3), RGB(255, 255, 255)
Me.picTitre.Line (0, 4)-(Me.picTitre.Width - 1, 4), RGB(255, 255, 255)
Me.picTitre.Line (1, 4)-(Me.picTitre.Width - 1, 4), &HE0E0E0

'-> Positionner le listview
Me.ListView1.Left = Me.picTitre.Left
Me.ListView1.Top = Me.picTitre.Top * 2 + Me.picTitre.Height
Me.ListView1.Width = aRect.Right - Me.ListView1.Left * 2
Me.ListView1.Height = Me.Picture1.ScaleY(Me.Picture1.Height, 1, 3) - Me.ListView1.Top

End Sub

Private Sub LoadEmploye(Optional TypeTrt As String)
'---> Charge unbe fenetre employ�
Dim aEnreg As B3DEnreg
Dim aFrm As frmEmploye
Dim i As Integer

On Error GoTo GestError

'-> Bloquer tout
Me.Enabled = False
Screen.MousePointer = 11
LockWindowUpdate Me.hwnd

'-> Faire un FindData des donn�es de l'enreg
Set aEnreg = FindData(aConnexion, aConnexion.Ident, MasterFname, Entry(2, Me.ListView1.SelectedItem.Key, "|"), False, False, aDescro, False)

'-> Tester le retour
If aEnreg Is Nothing Then
    '-> Afficher les valeurs d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter la proc�dure
    GoTo GestError
End If

'-> Nouvelle fenetre employ�
Set aFrm = New frmEmploye

'-> Initialiser l'�cran
InitScreen aFrm, aDescro, FnameJoins

'-> Charg� la fen�tre avec l'enregistrement
LoadScreen aEnreg, "EMPLOYE", aFrm, MasterFname, FnameJoins, aDescro, "UPDATE"

'-> On renseigne age et anciennete
aFrm.txtAge = Entry(1, CalculAgeAnciennete(aFrm.txtDate(2), aFrm.txtDate(0)), ",")
aFrm.txtAnc = Entry(2, CalculAgeAnciennete(aFrm.txtDate(2), aFrm.txtDate(0)), ",")

'-> Postionner l'enregistrement en cours dans la feuille
Set aFrm.EnregCours = aEnreg

'-> Charger la photo
If Dir$(App.Path & "\photos\img" & Trim(aEnreg.Fields("00002").ZonData) & ".jpg") <> "" Then
    aFrm.Picture1.Picture = LoadPicture(App.Path & "\photos\img" & Trim(aEnreg.Fields("00002").ZonData) & ".jpg")
    CurrentImg = App.Path & "\photos\img" & Trim(aEnreg.Fields("00002").ZonData) & ".jpg"
End If

'->On passe la reference de la fenetre au listview
Me.ListView1.SelectedItem.Tag = aFrm.hwnd

'-> On precise a la fenetre la reference vers le listitem du listview
aFrm.Tag = Me.ListView1.SelectedItem.Key

'-> On precise le type de trt dans lequel on ouvre la fenetre
aFrm.TypeTrt = TypeTrt

'-> On positionne la fenetre employ� en fct de son type de trtr
Select Case TypeTrt
    
    '-> Visu : on bloque l'ecran sauf les boutons d'acces aux autre ecrans
    Case ""
        aFrm.LockCmdBtn True
        aFrm.LockF10 False
        aFrm.LockZones False
    
    '-> Mise a jopur on laisse que la possibilite de valider et saisir ( pas d'acces aux autres trt )
    Case "UPDATE"
        aFrm.LockCmdBtn False
        aFrm.LockF10 True
        aFrm.LockZones True
        '-> On bloque la saisie du matricule
        aFrm.txtMatricule.Enabled = False
        
        
    Case "CREATE"
        aFrm.LockCmdBtn False
        aFrm.LockF10 True
        aFrm.LockZones True
    
End Select

'---> On active tjrs les bouton OK / CANCEL
aFrm.Command1.Enabled = True
aFrm.Command2.Enabled = True
'-> On bloque systematiquement les zone de dates pour saisir par le calendrier
For i = 0 To aFrm.txtDate.Count - 1
    aFrm.txtDate(i).Enabled = False
Next

aFrm.txtAnc.Enabled = False
aFrm.txtAge.Enabled = False

'-> Afficher la feuille
aFrm.Show

GestError:

    '-> D�bloquer l'�cran
    LockWindowUpdate 0
    Screen.MousePointer = 0
    Me.Enabled = True

End Sub

Private Function CheckEmploye() As Boolean
'---> Verifie si une fenetre employ� n'est pas deja ouverte et l'active
If Me.ListView1.SelectedItem.Tag <> "" Then
    SendMessage Me.ListView1.SelectedItem.Tag, WM_CHILDACTIVATE, 0, 0
    CheckEmploye = True
End If
End Function

Private Sub DeleteEmploye()
'---> permet la suppression d'un employ� et des ses salaires, primes.....
Dim strReq As String
Dim Resp As VbMsgBoxResult
Dim aNaviga As B3DNavigation
Dim aPage As B3DBuffer
Dim aEnreg As B3DEnreg

On Error GoTo GestErrors

'-> Confirmation ?
Resp = MsgBox("Vous �tes sur le point de supprimer l'employ� " & Me.ListView1.SelectedItem.SubItems(1), vbExclamation + vbYesNo)
If Resp = vbNo Then Exit Sub

'-> Bloquer tout
Me.Enabled = False
Screen.MousePointer = 11
LockWindowUpdate Me.hwnd

'-> Recherche des enregistrements qui contiennent le matricule selectionn�
'strReq = "B3D_SEARCH_DATA~00002�" & Me.ListView1.SelectedItem.Text

strReq = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & aConnexion.Ident & "�" & MasterFname & "' AND [FIELD] = '00002' AND [ZON_DATA] = '" & Me.ListView1.SelectedItem.Text & "'"

'-> Charger la liste des salaires
Set aNaviga = InitNaviga(aConnexion, aConnexion.Ident, MasterFname, "BUFFER_DELETE", strReq, "", "00002�1", "", 50000, 50000, aDescro)

'-> Tester le retour de navigation
If aNavigation Is Nothing Then
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter la fonction
    GoTo GestErrors
End If

'-> On parcours chaque page ( buffer ) de la navigation
For Each aPage In aNaviga.Pages
    '-> Pour chaque page, chaque enreg
    For Each aEnreg In aPage.Enregs
        DeleteEnreg aConnexion, MasterFname, aEnreg.NumEnreg
    Next '-> chaque enreg
Next '-> Chaque page

'-> On supprime l'employ�
DeleteEnreg aConnexion, MasterFname, Entry(2, Me.ListView1.SelectedItem.Key, "|")

'-> On ferme sa fenetre si elle est ouverte
If Me.ListView1.SelectedItem.Tag <> "" Then
    SendMessage Me.ListView1.SelectedItem.Tag, WM_CLOSE, 0, 0
End If

'-> On supprime dans le listview
Me.ListView1.ListItems.Remove Me.ListView1.SelectedItem.Key

GestErrors:
    '-> D�bloquer l'�cran
    LockWindowUpdate 0
    Screen.MousePointer = 0
    Me.Enabled = True

End Sub

Private Sub mnuHisto_Click()
'---> On cree une liste de recap des donn�es
'---> Chargement des commentaires employ�
'---> Chargement des primes employe
Dim aNavigation As B3DNavigation
Dim aIt As ListItem
Dim ListeFieldsGet As String
Dim ListeFieldsDisp As String

'-> On met en attente
frmWait.Label1 = "Chargement du r�capitulatif des donn�es employ�s..."
frmWait.Show

'-> On empeche de cliquer n importe ou
Me.Enabled = False

On Error GoTo GestError

'-> Bloquer l'�cran
Screen.MousePointer = 11
Me.Enabled = False

'-> On cree une liste des champs a ramener et afficher
ListeFieldsGet = "00001�0�00002�0�00003�0�00011�0�00062�0�00017�0�00018�1�00038�00019�1�00038�00024�0�00032�0�00030�0�00033�0�00034�0�00043�0�00044�0"
ListeFieldsDisp = "00001�00002�00003�00011�00062�00017�00018�00019�00024�00032�00030�00033�00034�00043�00044"

'-> Charger la liste des salaires
Set aNavigation = InitNaviga(aConnexion, aConnexion.Ident, MasterFname, "BUFFER_RECAP", "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & aConnexion.Ident & "�" & MasterFname & "' AND [FIELD] = '00002' ", "", ListeFieldsGet, ListeFieldsDisp, 50000, 50000, aDescro)
'-> Tester le retour de navigation
If aNavigation Is Nothing Then
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter la fonction
    GoTo GestError
End If

'-> Charger le browse des salaires
CreateListViewNaviga aNavigation, frmRecap.ListView

'-> Fin de la temporiation
Unload frmWait
Screen.MousePointer = 0
Me.Enabled = True


'-> Affichage du recap
frmRecap.Show vbModal

Exit Sub

GestError:
    '-> Fin de la temporiation
    Unload frmWait
    Screen.MousePointer = 0
    Me.Enabled = True

End Sub

Private Sub Picture2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    '-> Active le mode deplacement de la barre de separation ecran
    Deplace_Ok = True
End Sub

Private Sub Picture2_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    '-> DesActive le mode deplacement de la barre de separation ecran
    Deplace_Ok = False
End Sub

Private Sub Picture2_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim aPoint As POINTAPI
    Dim Coord As Long
    Dim aRect As RECT
     
    '-> Permet de faire le deplacement du split entre treeview et fenetre
    If Not Deplace_Ok Then Exit Sub
    Coord = GetCursorPos(aPoint)
    Coord = ScreenToClient(Me.hwnd, aPoint)
    Coord = GetClientRect(Me.hwnd, aRect)
    '-> On bloque a + ou - 1 cm des bords de la fenetre
    If Me.Picture1.ScaleX(aPoint.x, 3, 7) <= 1 Then aPoint.x = 37
    If Me.Picture1.ScaleX(aPoint.x, 3, 7) > Me.Picture1.ScaleX(aRect.Right, 3, 7) - 1 Then aPoint.x = aRect.Right - 37
    Me.Picture1.Width = Me.Picture1.ScaleX(aPoint.x, 3, 1)
End Sub

Private Sub CreateEmploye()
'---> Permet la creation, d'un employe
Dim aFrm As frmEmploye

'-> On ouvre une fenetre employe en creation
Set aFrm = New frmEmploye

'-> On precise qu'on est en creation
aFrm.TypeTrt = "CREATE"

'-> On intialise les messprogs
InitScreen aFrm, aDescro, FnameJoins

'-> On affiche le feuille de saisie
aFrm.Visible = True


End Sub
