VERSION 5.00
Object = "{8E27C92E-1264-101C-8A2F-040224009C02}#7.0#0"; "MSCAL.OCX"
Begin VB.Form frmCalendrier 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Calendrier"
   ClientHeight    =   3885
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3420
   ControlBox      =   0   'False
   Icon            =   "frmCalendrier.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3885
   ScaleWidth      =   3420
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Caption         =   "Saisie de la date"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3375
      Begin VB.CommandButton cmdSuppdate 
         Height          =   495
         Left            =   2160
         Picture         =   "frmCalendrier.frx":0ABA
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "Remettre la zone a blanc"
         Top             =   3240
         Width           =   495
      End
      Begin VB.CommandButton cmdOk 
         Height          =   495
         Left            =   2640
         Picture         =   "frmCalendrier.frx":0EFC
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   3240
         Width           =   495
      End
      Begin MSACAL.Calendar Calendar1 
         Height          =   3015
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   3135
         _Version        =   524288
         _ExtentX        =   5530
         _ExtentY        =   5318
         _StockProps     =   1
         BackColor       =   -2147483633
         Year            =   2001
         Month           =   9
         Day             =   7
         DayLength       =   1
         MonthLength     =   2
         DayFontColor    =   0
         FirstDay        =   2
         GridCellEffect  =   1
         GridFontColor   =   10485760
         GridLinesColor  =   -2147483632
         ShowDateSelectors=   -1  'True
         ShowDays        =   -1  'True
         ShowHorizontalGrid=   -1  'True
         ShowTitle       =   0   'False
         ShowVerticalGrid=   -1  'True
         TitleFontColor  =   10485760
         ValueIsNull     =   0   'False
         BeginProperty DayFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty GridFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty TitleFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmCalendrier"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Jour As String
Public Mois As String
Public Annee As String

Private Sub cmdOK_Click()
    If Me.Calendar1.Day = 0 Or Me.Calendar1.Month = 0 Then
        MsgBox "Date saisie incompl�te...", vbExclamation
        Exit Sub
    End If
    If Me.Calendar1.Day < 10 Then
        strRetour2 = "0" & Me.Calendar1.Day & "/"
    Else
        strRetour2 = Me.Calendar1.Day & "/"
    End If
    If Me.Calendar1.Month < 10 Then
        strRetour2 = strRetour2 & "0" & Me.Calendar1.Month & "/"
    Else
        strRetour2 = strRetour2 & Me.Calendar1.Month & "/"
    End If
    strRetour2 = strRetour2 & Me.Calendar1.Year
    Unload Me
End Sub

Private Sub cmdSuppdate_Click()
strRetour2 = "BLANC"
Unload Me
End Sub

Private Sub Form_Load()
'---> On precharge si demand�
If Me.Jour <> "" And Me.Mois <> "" And Me.Annee <> "" Then
    Me.Calendar1.Day = Me.Jour
    Me.Calendar1.Month = Me.Mois
    Me.Calendar1.Year = Me.Annee
End If
End Sub
