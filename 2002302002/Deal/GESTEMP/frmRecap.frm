VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmRecap 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Récapitulatif des données Employés"
   ClientHeight    =   9585
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   16545
   Icon            =   "frmRecap.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   639
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1103
   WindowState     =   2  'Maximized
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   8040
      Top             =   720
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRecap.frx":27A2
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRecap.frx":307C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRecap.frx":3956
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRecap.frx":3DA8
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRecap.frx":4682
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView ListView 
      Height          =   9495
      Left            =   0
      TabIndex        =   0
      ToolTipText     =   "Double Cliquer pour afficher le zoom d'une ligne "
      Top             =   0
      Width           =   16455
      _ExtentX        =   29025
      _ExtentY        =   16748
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "frmRecap"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Resize()
'---> On redimensionne le listview si fenetre redimensionné
Dim Coord As Long
Dim aRect As RECT
Dim i As Integer
    
Coord = GetClientRect(Me.hwnd, aRect)
Me.ListView.Width = aRect.Right
Me.ListView.Height = aRect.Bottom

'-> On place les icones dans le listview
For i = 1 To Me.ListView.ListItems.Count
    If Me.ListView.ListItems(i).Text = "EMPLOYE" Then Me.ListView.ListItems(i).SmallIcon = 1
    If Me.ListView.ListItems(i).Text = "SALAIRE" Then Me.ListView.ListItems(i).SmallIcon = 2
    If Me.ListView.ListItems(i).Text = "PRIME" Then Me.ListView.ListItems(i).SmallIcon = 3
    If Me.ListView.ListItems(i).Text = "OBJECTIF" Then Me.ListView.ListItems(i).SmallIcon = 4
    If Me.ListView.ListItems(i).Text = "COMMENTAIRE" Then Me.ListView.ListItems(i).SmallIcon = 5
Next

End Sub

Private Sub listview_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
'-> Fonction de tri des colonnes
TriColonnes ColumnHeader
End Sub

Public Sub TriColonnes(ByVal ColumnHeader As ColumnHeader)
'---> Tri sur les colonnes du listview
Me.ListView.SortKey = ColumnHeader.Index - 1
If Me.ListView.SortOrder = lvwAscending Then
    Me.ListView.SortOrder = lvwDescending
Else
    Me.ListView.SortOrder = lvwAscending
End If
Me.ListView.Sorted = True
End Sub

Private Sub ListView_DblClick()
'---> On active le zoom sur la ligne selectionnée
DisplayZoom aConnexion, MasterFname, Entry(2, Me.ListView.SelectedItem.Key, "|"), True, False, aDescro, Me, True
End Sub





