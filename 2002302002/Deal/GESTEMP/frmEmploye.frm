VERSION 5.00
Begin VB.Form frmEmploye 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informations employ�"
   ClientHeight    =   10590
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7005
   Icon            =   "frmEmploye.frx":0000
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   10590
   ScaleWidth      =   7005
   Begin VB.CommandButton cmsCapture 
      Height          =   615
      Left            =   2880
      Picture         =   "frmEmploye.frx":49E2
      Style           =   1  'Graphical
      TabIndex        =   48
      Tag             =   "CATALOGUE~49"
      Top             =   9960
      Width           =   735
   End
   Begin VB.Frame Frame4 
      Height          =   1575
      Left            =   0
      TabIndex        =   41
      Top             =   6480
      Width           =   6975
      Begin VB.PictureBox pctCalendar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   5
         Left            =   6600
         Picture         =   "frmEmploye.frx":7184
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   47
         ToolTipText     =   "Choix d'une date"
         Top             =   1080
         Width           =   255
      End
      Begin VB.PictureBox pctCalendar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   4
         Left            =   6600
         Picture         =   "frmEmploye.frx":72CE
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   46
         ToolTipText     =   "Choix d'une date"
         Top             =   480
         Width           =   255
      End
      Begin VB.TextBox txtVehicule 
         Height          =   285
         Left            =   120
         TabIndex        =   18
         Tag             =   "FIELD~0001~EMPLOYE~0040"
         Top             =   1080
         Width           =   3375
      End
      Begin VB.TextBox txtMateriel 
         Height          =   285
         Left            =   120
         TabIndex        =   16
         Tag             =   "FIELD~0001~EMPLOYE~0039"
         Top             =   480
         Width           =   3375
      End
      Begin VB.TextBox txtDate 
         Enabled         =   0   'False
         Height          =   285
         Index           =   5
         Left            =   5040
         TabIndex        =   19
         Tag             =   "FIELD~0001~EMPLOYE~0042"
         Top             =   1080
         Width           =   1455
      End
      Begin VB.TextBox txtDate 
         Enabled         =   0   'False
         Height          =   285
         Index           =   4
         Left            =   5040
         TabIndex        =   17
         Tag             =   "FIELD~0001~EMPLOYE~0041"
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label Label11 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   45
         Tag             =   "CATALOGUE~40"
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label Label10 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   44
         Tag             =   "CATALOGUE~39"
         Top             =   240
         Width           =   3375
      End
      Begin VB.Label Label7 
         ForeColor       =   &H00800000&
         Height          =   495
         Left            =   3600
         TabIndex        =   43
         Tag             =   "CATALOGUE~42"
         Top             =   960
         Width           =   1335
      End
      Begin VB.Label Label6 
         ForeColor       =   &H00800000&
         Height          =   615
         Left            =   3600
         TabIndex        =   42
         Tag             =   "CATALOGUE~41"
         Top             =   240
         Width           =   1335
      End
   End
   Begin VB.CommandButton Command6 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   2160
      Picture         =   "frmEmploye.frx":7418
      Style           =   1  'Graphical
      TabIndex        =   40
      Tag             =   "CATALOGUE~48"
      Top             =   9960
      Width           =   735
   End
   Begin VB.CommandButton Command5 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   1440
      Picture         =   "frmEmploye.frx":785A
      Style           =   1  'Graphical
      TabIndex        =   39
      Tag             =   "CATALOGUE~47"
      Top             =   9960
      Width           =   735
   End
   Begin VB.CommandButton Command4 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   720
      Picture         =   "frmEmploye.frx":8124
      Style           =   1  'Graphical
      TabIndex        =   38
      Tag             =   "CATALOGUE~46"
      Top             =   9960
      Width           =   735
   End
   Begin VB.CommandButton Command3 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   0
      Picture         =   "frmEmploye.frx":8566
      Style           =   1  'Graphical
      TabIndex        =   37
      Tag             =   "CATALOGUE~45"
      Top             =   9960
      Width           =   735
   End
   Begin VB.CommandButton Command2 
      Appearance      =   0  'Flat
      Height          =   615
      Left            =   5520
      Picture         =   "frmEmploye.frx":8E30
      Style           =   1  'Graphical
      TabIndex        =   36
      Tag             =   "CATALOGUE~51"
      Top             =   9960
      Width           =   735
   End
   Begin VB.CommandButton Command1 
      Height          =   615
      Left            =   6240
      Picture         =   "frmEmploye.frx":8F7A
      Style           =   1  'Graphical
      TabIndex        =   35
      Tag             =   "CATALOGUE~50"
      Top             =   9960
      Width           =   735
   End
   Begin VB.Frame Frame2 
      ForeColor       =   &H00800000&
      Height          =   1935
      Left            =   0
      TabIndex        =   34
      Tag             =   "CATALOGUE~15"
      Top             =   8040
      Width           =   6975
      Begin VB.TextBox txtDiplomes 
         Height          =   1455
         Left            =   120
         MaxLength       =   250
         MultiLine       =   -1  'True
         TabIndex        =   20
         Tag             =   "FIELD~0001~EMPLOYE~0015"
         Top             =   360
         Width           =   6735
      End
   End
   Begin VB.Frame Frame1 
      Height          =   6375
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   6975
      Begin VB.TextBox txtAnc 
         Enabled         =   0   'False
         Height          =   285
         Left            =   5040
         TabIndex        =   62
         Top             =   4920
         Width           =   1815
      End
      Begin VB.TextBox txtAge 
         Enabled         =   0   'False
         Height          =   285
         Left            =   1680
         TabIndex        =   60
         Top             =   4920
         Width           =   1815
      End
      Begin VB.PictureBox pctCalendar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   6
         Left            =   3960
         Picture         =   "frmEmploye.frx":93BC
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   59
         ToolTipText     =   "Choix d'une date"
         Top             =   5640
         Width           =   255
      End
      Begin VB.TextBox txtNbMois 
         Height          =   285
         Left            =   1680
         TabIndex        =   58
         Tag             =   "FIELD~0001~EMPLOYE~0061"
         Top             =   5640
         Width           =   615
      End
      Begin VB.TextBox txtDate 
         Enabled         =   0   'False
         Height          =   285
         Index           =   7
         Left            =   5040
         MaxLength       =   10
         TabIndex        =   55
         Tag             =   "FIELD~0001~EMPLOYE~0062"
         Top             =   5640
         Width           =   1455
      End
      Begin VB.TextBox txtDate 
         Enabled         =   0   'False
         Height          =   285
         Index           =   6
         Left            =   2520
         MaxLength       =   10
         TabIndex        =   53
         Tag             =   "FIELD~0001~EMPLOYE~0060"
         Top             =   5640
         Width           =   1335
      End
      Begin VB.PictureBox pctCalendar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   1
         Left            =   6600
         Picture         =   "frmEmploye.frx":9506
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   52
         ToolTipText     =   "Choix d'une date"
         Top             =   4080
         Width           =   255
      End
      Begin VB.PictureBox pctCalendar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   3
         Left            =   6600
         Picture         =   "frmEmploye.frx":9650
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   51
         ToolTipText     =   "Choix d'une date"
         Top             =   4440
         Width           =   255
      End
      Begin VB.PictureBox pctCalendar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   0
         Left            =   3240
         Picture         =   "frmEmploye.frx":979A
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   50
         ToolTipText     =   "Choix d'une date"
         Top             =   4080
         Width           =   255
      End
      Begin VB.PictureBox pctCalendar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   2
         Left            =   3240
         Picture         =   "frmEmploye.frx":98E4
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   49
         ToolTipText     =   "Choix d'une date"
         Top             =   4440
         Width           =   255
      End
      Begin VB.TextBox txtDate 
         Enabled         =   0   'False
         Height          =   285
         Index           =   3
         Left            =   5040
         MaxLength       =   10
         TabIndex        =   14
         Tag             =   "FIELD~0001~EMPLOYE~0012"
         Top             =   4440
         Width           =   1455
      End
      Begin VB.TextBox txtMatricule 
         Height          =   285
         Left            =   1440
         MaxLength       =   4
         TabIndex        =   0
         Tag             =   "FIELD~0001~EMPLOYE~0002"
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox txtAdr1 
         Height          =   285
         Left            =   120
         TabIndex        =   3
         Tag             =   "FIELD~0001~EMPLOYE~0004"
         Top             =   1200
         Width           =   4695
      End
      Begin VB.TextBox txtAdr2 
         Height          =   285
         Left            =   120
         TabIndex        =   4
         Tag             =   "FIELD~0001~EMPLOYE~0005"
         Top             =   1560
         Width           =   4695
      End
      Begin VB.TextBox txtAdr3 
         Height          =   285
         Left            =   120
         TabIndex        =   5
         Tag             =   "FIELD~0001~EMPLOYE~0006"
         Top             =   1920
         Width           =   4695
      End
      Begin VB.TextBox txtAdr4 
         Height          =   285
         Left            =   120
         TabIndex        =   6
         Tag             =   "FIELD~0001~EMPLOYE~0007"
         Top             =   2280
         Width           =   4695
      End
      Begin VB.TextBox txtAdr5 
         Height          =   285
         Left            =   120
         TabIndex        =   7
         Tag             =   "FIELD~0001~EMPLOYE~0008"
         Top             =   2640
         Width           =   4695
      End
      Begin VB.TextBox txtTel 
         Height          =   285
         Left            =   2400
         TabIndex        =   8
         Tag             =   "FIELD~0001~EMPLOYE~0009"
         Top             =   3000
         Width           =   4455
      End
      Begin VB.TextBox txtNom 
         Height          =   285
         Left            =   1440
         TabIndex        =   2
         Tag             =   "FIELD~0001~EMPLOYE~0003"
         Top             =   600
         Width           =   3375
      End
      Begin VB.TextBox txtMail 
         Height          =   285
         Left            =   2400
         TabIndex        =   9
         Tag             =   "FIELD~0001~EMPLOYE~0010"
         Top             =   3360
         Width           =   4455
      End
      Begin VB.TextBox txtFille 
         Height          =   285
         Left            =   2400
         TabIndex        =   10
         Tag             =   "FIELD~0001~EMPLOYE~0037"
         Top             =   3720
         Width           =   4455
      End
      Begin VB.TextBox txtDate 
         Enabled         =   0   'False
         Height          =   285
         Index           =   0
         Left            =   1680
         MaxLength       =   10
         TabIndex        =   11
         Tag             =   "FIELD~0001~EMPLOYE~0016"
         Top             =   4080
         Width           =   1455
      End
      Begin VB.TextBox txtDate 
         Enabled         =   0   'False
         Height          =   285
         Index           =   2
         Left            =   1680
         MaxLength       =   10
         TabIndex        =   12
         Tag             =   "FIELD~0001~EMPLOYE~0011"
         Top             =   4440
         Width           =   1455
      End
      Begin VB.TextBox txtDate 
         Enabled         =   0   'False
         Height          =   285
         Index           =   1
         Left            =   5040
         MaxLength       =   10
         TabIndex        =   13
         Tag             =   "FIELD~0001~EMPLOYE~0014"
         Top             =   4080
         Width           =   1455
      End
      Begin VB.TextBox txtMotif 
         Height          =   285
         Left            =   1680
         TabIndex        =   15
         Tag             =   "FIELD~0001~EMPLOYE~00013"
         Top             =   6000
         Width           =   5175
      End
      Begin VB.Frame Frame3 
         ForeColor       =   &H00800000&
         Height          =   2655
         Left            =   4920
         TabIndex        =   21
         Top             =   240
         Width           =   1935
         Begin VB.PictureBox Picture1 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   2295
            Left            =   120
            ScaleHeight     =   2295
            ScaleWidth      =   1695
            TabIndex        =   22
            Tag             =   "0001"
            Top             =   240
            Width           =   1695
         End
      End
      Begin VB.Label Label16 
         Caption         =   "Anciennet�"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3840
         TabIndex        =   63
         Top             =   4920
         Width           =   855
      End
      Begin VB.Label Label15 
         Caption         =   "Age Employ�"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   61
         Top             =   4920
         Width           =   1455
      End
      Begin VB.Label Label14 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   57
         Tag             =   "CATALOGUE~61"
         Top             =   5640
         Width           =   1455
      End
      Begin VB.Label Label13 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5040
         TabIndex        =   56
         Tag             =   "CATALOGUE~62"
         Top             =   5280
         Width           =   1455
      End
      Begin VB.Label Label12 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2520
         TabIndex        =   54
         Tag             =   "CATALOGUE~60"
         Top             =   5280
         Width           =   1335
      End
      Begin VB.Label lblMatricule 
         Caption         =   "totot"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   33
         Tag             =   "CATALOGUE~2"
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label lblAdresse 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Tag             =   "CATALOGUE~4"
         Top             =   960
         Width           =   1335
      End
      Begin VB.Label lblTel 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Tag             =   "CATALOGUE~9"
         Top             =   3000
         Width           =   2175
      End
      Begin VB.Label Label1 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Tag             =   "CATALOGUE~3"
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label2 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Tag             =   "CATALOGUE~10"
         Top             =   3360
         Width           =   2055
      End
      Begin VB.Label Label3 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Tag             =   "CATALOGUE~37"
         Top             =   3720
         Width           =   1935
      End
      Begin VB.Label lblPort 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Tag             =   "CATALOGUE~16"
         Top             =   4200
         Width           =   1455
      End
      Begin VB.Label Label4 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3600
         TabIndex        =   26
         Tag             =   "CATALOGUE~14"
         Top             =   4080
         Width           =   1455
      End
      Begin VB.Label Label5 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Tag             =   "CATALOGUE~11"
         Top             =   4560
         Width           =   1455
      End
      Begin VB.Label Label8 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3600
         TabIndex        =   24
         Tag             =   "CATALOGUE~12"
         Top             =   4440
         Width           =   1455
      End
      Begin VB.Label Label9 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Tag             =   "CATALOGUE~13"
         Top             =   6000
         Width           =   1455
      End
   End
End
Attribute VB_Name = "frmEmploye"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public TypeTrt As String
Public EnregCours As B3DEnreg


Private Sub cmsCapture_Click()
'---> On genere un fichier de data pour lancer l'impression
Dim HdlFile As Integer
Dim aCont As Control
Dim LigneE As String

'-> On ouvre un nouveau fichier en ecriture
HdlFile = FreeFile
Open App.Path & "\DATA\DataFic.txt" For Output As #HdlFile

'-> On cree une ligne qui cree l'ordre d'impression de la section + dieses
LigneE = "[ST-SECTION1(TXT-Section)][]{"

'-> On envoie le fichier image
If CurrentImg <> "" Then LigneE = LigneE & "^0001" & CurrentImg

'-> parcours des datas pour les envoyer dans le fichier
For Each aCont In Me.Controls
    '-> si zone de texte on envoi le champ ( diese ) et sa valeur dans le fichier de datas
    If TypeOf aCont Is TextBox Then LigneE = LigneE & "^" & Format(Entry(4, aCont.Tag, "~"), "0000") & aCont.Text
Next

'-> On termine l'instruction
LigneE = LigneE & "}"

'-> On envoi l'ordre dans le fichier de data
Print #HdlFile, LigneE

'-> Fermeture du fichier
Close #HdlFile

'-> On envoie la fusion entre la maquette et le fichier data puis l'impression TURBO
PrintDataInTurbo App.Path & "\DATA\b3d_fiche_employe.maqgui", App.Path & "\DATA\DataFic.txt", App.Path & "\DATA\MyPrint.turbo", True

End Sub

Private Sub Command1_Click()
'---> Validation d'une saisie / modif employ�
Dim strControle As String
Dim aIt As ListItem

'-> si visu on sort
If Me.TypeTrt = "VISU" Then
    Unload Me
    Exit Sub
End If

'-> sinon : blocage ecran et mise a jour de l'enreg
On Error GoTo GestError

'-> Bloquer l'�cran
Screen.MousePointer = 11
Me.Enabled = False

If Me.TypeTrt = "CREATE" Then Set Me.EnregCours = aDescro.GetEnreg

'-> get des champs qui ont chang�s
strControle = GetScreen(Me.EnregCours, "EMPLOYE", Me, MasterFname, aDescro, FnameJoins)

'-> V�rifier s'il y eu une erreur
If Entry(1, strControle, "~") = "ERROR" Then
    '-> D�bloquer
    Me.Enabled = True
    '-> Positionner le focus
    If Me.Controls(Entry(2, strControle, "~")).Enabled Then Me.Controls(Entry(2, strControle, "~")).SetFocus
    '-> Envoyer vers la fin
    GoTo GestError
End If

'-> Create ou update
If Me.TypeTrt = "UPDATE" Then
    '-> Ok : faire l'update sur l'enreg
    If Not UpdateEnreg(aConnexion, MasterFname, Me.EnregCours, True) Then
        '-> Tester  le code erreur 90026 pas de modif
        If aConnexion.ErrorNumber = "90026" Then GoTo Rep
        '-> Afficher une message d'erreur
        MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
        '-> Fin du programme
        GoTo GestError
    End If
Else
    '-> Faire un create
    Me.EnregCours.Fields("00001").ZonLock = "EMPLOYE"
    If Not CreateEnreg(aConnexion, MasterFname, Me.EnregCours) Then
        '-> Tester  le code erreur 90026 pas de modif
        If aConnexion.ErrorNumber = "90026" Then GoTo Rep
        '-> Afficher une message d'erreur
        MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
        '-> Fin du programme
        GoTo GestError
    End If
    
    '-> On l'ajoute dans le listview et on pointe dessus
    Set aIt = mdiGesEmp.ListView1.ListItems.Add(, "ENREG|" & Me.EnregCours.NumEnreg, Me.EnregCours.Fields("00002").ZonData, , mdiGesEmp.ImageList1.ListImages(5).Index)
    aIt.SubItems(1) = Me.EnregCours.Fields("00003").ZonData
    aIt.SubItems(2) = Me.EnregCours.Fields("00014").ZonData
    
End If 'Si create ou update

'-> Etiquette de reprise
Rep:

GestError:
    '-> D�bloquer
    Me.Enabled = True
    Screen.MousePointer = 0
    Unload Me
    Exit Sub

End Sub

Private Sub Command2_Click()
'---> sortie de l'ecran
Unload Me
End Sub

Private Sub Command3_Click()

'---> Afficher pour cet employ� la saisie des salaires

Dim aNavigation As B3DNavigation
Dim aIt As ListItem

On Error GoTo GestError

'-> teste si date d'entree renseign�e ( pour calcul anciennete )
If Me.txtDate(2) = "" Then
    MsgBox "Vous devez d'abord saisir une date d'entr�e pour acc�der aux salaires...", vbExclamation
    Exit Sub
End If

'-> Bloquer l'�cran
Screen.MousePointer = 11
Me.Enabled = False

'-> Charger la liste des salaires
Set aNavigation = InitNaviga(aConnexion, aConnexion.Ident, MasterFname, "BUFFER_SALAIRE", "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & aConnexion.Ident & "�" & MasterFname & "' AND [INAME] = '002' AND [ZON_INDEX_1] = 'SALAIRE' AND [ZON_INDEX_2] = '" & EnregCours.Fields("00002").ZonData & "'", "", "00017�0�00018�1�00024�0", "", 50000, 50000, aDescro)

'-> Tester le retour de navigation
If aNavigation Is Nothing Then
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter la fonction
    GoTo GestError
End If

'-> Charger l'�cran en m�moire
Load frmSalaire

'-> Initialiser les libell�s et les F10
InitScreen frmSalaire, aDescro, FnameJoins

'-> Charger le browse des salaires
CreateListViewNaviga aNavigation, frmSalaire.ListView

'-> On cree des icones pour le listview
For Each aIt In frmSalaire.ListView.ListItems
    aIt.SmallIcon = 1
Next

'-> Lib�rer le pointeur
Screen.MousePointer = 0
'-> Postionner le code employe
frmSalaire.CodeEmploye = EnregCours.Fields("00002").ZonData
'-> Positionne le nom de l'employe
frmSalaire.NomEmploye = EnregCours.Fields("00003").ZonData
'-> On stocke la date d'entree pour calcul de l'anciennete
frmSalaire.DateEnt = Me.txtDate(2)
'-> On ajoute le nom dans le titre
If NumEntries(frmSalaire.Caption, "-") = 1 Then
    frmSalaire.Caption = frmSalaire.Caption & " - " & frmSalaire.NomEmploye
End If

'-> Charge les zones pour visu du 1er salaire
If frmSalaire.ListView.ListItems.Count > 0 Then
    frmSalaire.LoadSalaire frmSalaire.ListView.ListItems(1)
    Set frmSalaire.CurrentItem = frmSalaire.ListView.ListItems(1)
End If

'-> Bloaque la feuille en visu
frmSalaire.LockZones False

'-> Afficher la feuille
frmSalaire.Show vbModal

'-> Quitter la focntion
GestError:
    '-> Release de l'�cran
    Screen.MousePointer = 0
    Me.Enabled = True

End Sub

Private Sub Command4_Click()
'---> Chargement des primes employe
Dim aNavigation As B3DNavigation
Dim aIt As ListItem

On Error GoTo GestError

'-> Bloquer l'�cran
Screen.MousePointer = 11
Me.Enabled = False

'-> Charger la liste des salaires
Set aNavigation = InitNaviga(aConnexion, aConnexion.Ident, MasterFname, "BUFFER_PRIMES", "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & aConnexion.Ident & "�" & MasterFname & "' AND [INAME] = '003' AND [ZON_INDEX_1] = 'PRIME' AND [ZON_INDEX_2] = '" & EnregCours.Fields("00002").ZonData & "'", "", "00032�0�00031�1�00030�0", "", 50000, 50000, aDescro)

'-> Tester le retour de navigation
If aNavigation Is Nothing Then
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter la fonction
    GoTo GestError
End If

'-> Charger l'�cran en m�moire
Load frmPrime

'-> Charger le browse des salaires
CreateListViewNaviga aNavigation, frmPrime.ListView

'-> On cree des icones pour le listview
For Each aIt In frmPrime.ListView.ListItems
    aIt.SmallIcon = 1
Next

'-> Lib�rer le pointeur
Screen.MousePointer = 0
'-> Postionner le code employe
frmPrime.CodeEmploye = EnregCours.Fields("00002").ZonData
'-> Positionne le nom de l'employe
frmPrime.NomEmploye = EnregCours.Fields("00003").ZonData
'-> On ajoute le nom dans le titre
If NumEntries(frmSalaire.Caption, "-") = 1 Then
    frmPrime.Caption = frmPrime.Caption & " - " & frmPrime.NomEmploye
End If

'-> Charge les zones pour visu du 1er salaire
If frmPrime.ListView.ListItems.Count > 0 Then Set frmPrime.ListView.SelectedItem = frmPrime.ListView.ListItems(1)

'-> Afficher la feuille
frmPrime.Show vbModal

'-> Quitter la focntion
GestError:
    '-> Release de l'�cran
    Screen.MousePointer = 0
    Me.Enabled = True
End Sub

Private Sub Command5_Click()
'---> Chargement des objectifs employ�
'---> Chargement des primes employe
Dim aNavigation As B3DNavigation
Dim aIt As ListItem

On Error GoTo GestError

'-> Bloquer l'�cran
Screen.MousePointer = 11
Me.Enabled = False

'-> Charger la liste des salaires
Set aNavigation = InitNaviga(aConnexion, aConnexion.Ident, MasterFname, "BUFFER_OBJECTIFS", "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & aConnexion.Ident & "�" & MasterFname & "' AND [INAME] = '004' AND [ZON_INDEX_1] = 'OBJECTIF' AND [ZON_INDEX_2] = '" & EnregCours.Fields("00002").ZonData & "'", "", "00033�0�00034�1�00035�0�00036�0", "", 50000, 50000, aDescro)
'-> Tester le retour de navigation
If aNavigation Is Nothing Then
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter la fonction
    GoTo GestError
End If

'-> Charger l'�cran en m�moire
Load frmObjectif

'-> Charger le browse des salaires
CreateListViewNaviga aNavigation, frmObjectif.ListView

'-> On cree des icones pour le listview
For Each aIt In frmObjectif.ListView.ListItems
    aIt.SmallIcon = 1
Next

'-> Lib�rer le pointeur
Screen.MousePointer = 0
'-> Postionner le code employe
frmObjectif.CodeEmploye = EnregCours.Fields("00002").ZonData
'-> Positionne le nom de l'employe
frmObjectif.NomEmploye = EnregCours.Fields("00003").ZonData
'-> On ajoute le nom dans le titre
If NumEntries(frmSalaire.Caption, "-") = 1 Then
    frmObjectif.Caption = frmObjectif.Caption & " - " & frmObjectif.NomEmploye
End If

'-> Charge les zones pour visu du 1er salaire
If frmObjectif.ListView.ListItems.Count > 0 Then Set frmObjectif.ListView.SelectedItem = frmObjectif.ListView.ListItems(1)

'-> Afficher la feuille
frmObjectif.Show vbModal

'-> Quitter la focntion
GestError:
    '-> Release de l'�cran
    Screen.MousePointer = 0
    Me.Enabled = True

End Sub

Private Sub Command6_Click()
'---> Chargement des commentaires employ�
'---> Chargement des primes employe
Dim aNavigation As B3DNavigation
Dim aIt As ListItem

On Error GoTo GestError

'-> Bloquer l'�cran
Screen.MousePointer = 11
Me.Enabled = False

'-> Charger la liste des salaires
Set aNavigation = InitNaviga(aConnexion, aConnexion.Ident, MasterFname, "BUFFER_COMMENTAIRE", "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & aConnexion.Ident & "�" & MasterFname & "' AND [INAME] = '007' AND [ZON_INDEX_1] = 'COMMENTAIRE' AND [ZON_INDEX_2] = '" & EnregCours.Fields("00002").ZonData & "'", "", "00043�0�00044�0", "", 50000, 50000, aDescro)
'-> Tester le retour de navigation
If aNavigation Is Nothing Then
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter la fonction
    GoTo GestError
End If

'-> Charger l'�cran en m�moire
Load frmComment

'-> Charger le browse des salaires
CreateListViewNaviga aNavigation, frmComment.ListView

'-> On cree des icones pour le listview
For Each aIt In frmComment.ListView.ListItems
    aIt.SmallIcon = 1
Next

'-> Lib�rer le pointeur
Screen.MousePointer = 0
'-> Postionner le code employe
frmComment.CodeEmploye = EnregCours.Fields("00002").ZonData
'-> Positionne le nom de l'employe
frmComment.NomEmploye = EnregCours.Fields("00003").ZonData
'-> On ajoute le nom dans le titre
If NumEntries(frmSalaire.Caption, "-") = 1 Then
    frmComment.Caption = frmComment.Caption & " - " & frmComment.NomEmploye
End If

'-> Charge les zones pour visu du 1er salaire
If frmComment.ListView.ListItems.Count > 0 Then Set frmComment.ListView.SelectedItem = frmComment.ListView.ListItems(1)

'-> Afficher la feuille
frmComment.Show vbModal

'-> Quitter la focntion
GestError:
    '-> Release de l'�cran
    Screen.MousePointer = 0
    Me.Enabled = True
End Sub

Private Sub Form_Load()
'---> On positionne la fenetre dans la MDI
Me.Top = 0
Me.Left = 0
End Sub

Public Sub LockZones(ByVal TypeTrt As Boolean)
'---> Permet de bloquer toutes les zones de saisie ( visu ) ou de les debloquer ( update )
Dim aCont As Control
For Each aCont In Me.Controls
    If TypeOf aCont Is TextBox Then aCont.Enabled = TypeTrt
Next '-> Prochain controle
End Sub

Public Sub LockF10(ByVal TypeTrt As Boolean)
'---> Permet de bloquer toutes les zones de saisie ( visu ) ou de les debloquer ( update )
Dim aCont As Control
For Each aCont In Me.Controls
    If TypeOf aCont Is PictureBox Then aCont.Enabled = TypeTrt
Next '-> Prochain controle
End Sub

Public Sub LockCmdBtn(ByVal TypeTrt As Boolean)
'---> Permet de bloquer toutes les zones de saisie ( visu ) ou de les debloquer ( update )
Dim aCont As Control
For Each aCont In Me.Controls
    If TypeOf aCont Is CommandButton Then aCont.Enabled = TypeTrt
Next '-> Prochain controle
End Sub

Private Sub Form_Unload(Cancel As Integer)
'---> On precise que l'on a ferme cette fenetre employe
If Me.Tag <> "" Then
    mdiGesEmp.ListView1.ListItems(Me.Tag).Tag = ""
End If
End Sub








Private Sub pctCalendar_Click(Index As Integer)
'---> affichage du calendrier
Dim Jour As String
Dim Mois As String
Dim Annee As String
Dim i As Integer

UseCalendar txtDate(Index)

'-> Calcul de la date de fin de CDD
If Index = 6 Then
    
    If Me.txtDate(6).Text = "" Then Exit Sub
    
    Jour = Day(Me.txtDate(6).Text)
    
    If Month(Me.txtDate(6).Text) + 6 > 12 Then
        Mois = Month(Me.txtDate(6).Text)
        For i = 1 To CInt(Me.txtNbMois.Text)
            Mois = Mois + 1
            If Mois = 12 Then Mois = 0
        Next
        Annee = Year(Me.txtDate(6).Text) + 1
    Else
        Mois = Month(Me.txtDate(6).Text) + 6
        Annee = Year(Me.txtDate(6).Text)
    End If
    
    Me.txtDate(7).Text = Jour & "/" & Mois & "/" & Annee

End If

If Index = 0 Then
    '-> Calcul de l'age
    Me.txtAge.Text = Entry(1, CalculAgeAnciennete("", Me.txtDate(0)), ",")
End If

If Index = 2 Then
    '-> Calcul de l'age
    Me.txtAnc.Text = Entry(2, CalculAgeAnciennete(Me.txtDate(2), ""), ",")
End If

End Sub

Private Sub txtAdr1_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then Me.txtAdr2.SetFocus
End Sub

Private Sub txtNom_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then Me.txtAdr1.SetFocus
End Sub

Private Sub txtAdr2_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then Me.txtAdr3.SetFocus
End Sub

Private Sub txtAdr3_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then Me.txtAdr4.SetFocus
End Sub

Private Sub txtAdr4_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then Me.txtAdr5.SetFocus
End Sub

Private Sub txtAdr5_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then Me.txtTel.SetFocus
End Sub

Private Sub txtTel_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then Me.txtMail.SetFocus
End Sub

Private Sub txtMail_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then Me.txtFille.SetFocus
End Sub

Private Sub txtFille_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then Me.txtNbMois.SetFocus
End Sub

Private Sub txtNbMois_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then Me.txtMotif.SetFocus
End Sub


Private Sub txtMotif_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then Me.txtMateriel.SetFocus
End Sub

Private Sub txtMateriel_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then Me.txtVehicule.SetFocus
End Sub

Private Sub txtVehicule_KeyPress(KeyAscii As Integer)
'---> On passe a la zone suivante
If KeyAscii = 13 Then Me.txtDiplomes.SetFocus
End Sub




