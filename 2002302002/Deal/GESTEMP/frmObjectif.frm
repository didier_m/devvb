VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmObjectif 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Objectifs Employ� "
   ClientHeight    =   5055
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9510
   Icon            =   "frmObjectif.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5055
   ScaleWidth      =   9510
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmsCapture 
      Height          =   615
      Left            =   0
      Picture         =   "frmObjectif.frx":08CA
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Imprimer"
      Top             =   4440
      Width           =   735
   End
   Begin VB.CommandButton Command1 
      Height          =   615
      Left            =   8760
      Picture         =   "frmObjectif.frx":306C
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   4440
      Width           =   735
   End
   Begin VB.Frame Frame1 
      Caption         =   "Liste Objectis "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   4455
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9495
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   2880
         Top             =   3120
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   1
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmObjectif.frx":3D36
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.ListView Listview 
         Height          =   4095
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   9255
         _ExtentX        =   16325
         _ExtentY        =   7223
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin VB.Menu mnupopUp 
      Caption         =   "POPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuAjouter 
         Caption         =   "Ajouter"
      End
      Begin VB.Menu mnuModifier 
         Caption         =   "Modifier"
      End
      Begin VB.Menu mnusep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSupprimer 
         Caption         =   "Supprimer"
      End
      Begin VB.Menu mnusep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuZoom 
         Caption         =   "Zoom Donn�es"
      End
   End
End
Attribute VB_Name = "frmObjectif"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public aNavigation As B3DNavigation
Public CodeEmploye As String
Public NomEmploye As String

Private Sub Command1_Click()
'---> Sortie
Unload Me
End Sub

Private Sub listview_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
Dim aIt As ListItem

'-> On se positionne sur le bon element de la liste meme lors d'un click droit
Set aIt = Me.ListView.HitTest(x, y)
If Not aIt Is Nothing Then Set Me.ListView.SelectedItem = aIt

'-> Quitter si pas bouton droit
If Button <> vbRightButton Then Exit Sub

'-> Gestion des menus
If Me.ListView.SelectedItem Is Nothing Then
    '-> Bloquer
    Me.mnuSupprimer.Enabled = False
    Me.mnuModifier.Enabled = False
    Me.mnuZoom.Enabled = False
Else
    '-> Lib�rer
    Me.mnuSupprimer.Enabled = True
    Me.mnuModifier.Enabled = True
    Me.mnuZoom.Enabled = True
End If

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu Me.mnuPopUp

'-> Selon le type de retour
Select Case strRetour
    Case "CREATE"
        '-> Cr�er un nouvel Enreg
        Call CreateData
    Case "UPDATE"
        '-> Modifier un enreg
        Call EditData
    Case "DELETE"
        '-> Supprimer un enreg
        Call DeleteData
    Case "ZOOM"
        '-> Faire un zoom sur les donn�es
        Call ZoomData
End Select
End Sub

Private Function ZoomData()
'-> Afficher le zoom
DisplayZoom aConnexion, MasterFname, Entry(2, Me.ListView.SelectedItem.Key, "|"), True, False, aDescro, Me, True
End Function

Private Sub CreateData()
'---> Cette proc�dure donne acc�s � la modification de l'�cran
Dim aEnreg As B3DEnreg

On Error GoTo GestError

'-> creer un enregistrement a vide pour creation
Set aEnreg = aDescro.GetEnreg

'-> Cl� primaire
aEnreg.Fields("00001").ZonLock = "OBJECTIF"
aEnreg.Fields("00002").ZonLock = Me.CodeEmploye
aEnreg.Fields("00003").ZonLock = Me.NomEmploye

strRetour = ""
'-> On ouvre la fenetre de saisie
frmSaiObjectif.Show vbModal
'-> test si retour = rien
If strRetour = "" Then Exit Sub

'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11
'-> Bloquer le browse
Me.ListView.Enabled = False

'-> On recupere les zones saisies en retour
aEnreg.Fields("00033").ZonLock = Entry(1, strRetour, "�")
aEnreg.Fields("00034").ZonLock = Entry(2, strRetour, "�")
aEnreg.Fields("00035").ZonLock = Entry(3, strRetour, "�")
aEnreg.Fields("00036").ZonLock = Entry(4, strRetour, "�")

'-> On cree l'enreg
If Not CreateEnreg(aConnexion, MasterFname, aEnreg) Then
    '-> Tester  le code erreur 90026 pas de modif
    If aConnexion.ErrorNumber = "90026" Then GoTo GestError
    '-> Afficher une message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Fin du programme
    GoTo GestError
End If

'-> Mettre � jour l'affichage
DisplayEnregInListView aEnreg, Me.ListView, aDescro
Set Me.ListView.SelectedItem = Me.ListView.ListItems(Me.ListView.ListItems.Count)
Me.ListView.SelectedItem.SmallIcon = 1

'-> Formatter le listview
FormatListView Me.ListView

GestError:
    '-> Bloquer les enreg
    Me.Enabled = True
    Screen.MousePointer = 0
    '-> deBloquer le browse
    Me.ListView.Enabled = True
    
End Sub

Private Sub EditData()
'---> Cette proc�dure donne acc�s � la modification de l'�cran
Dim LibError As String
Dim aEnreg As B3DEnreg

On Error GoTo GestError

'-> recup de l'enreg a modifier
Rep:
Set aEnreg = FindData(aConnexion, aConnexion.Ident, MasterFname, Entry(2, Me.ListView.SelectedItem.Key, "|"), True, False, aDescro, False)

'-> Tester le retour
If aEnreg Is Nothing Then
    '-> Tester si l'enregistrement n'est pas lock� par un autre
    If aConnexion.ErrorNumber = "90006" Then
        '-> v�rifier si le lock est actif
        If Entry(4, aConnexion.ErrorParam, "�") = "0" Then
            '-> Envoyer un B3D unlock
            If UnlockEnreg(aConnexion, MasterFname, Entry(2, Me.ListView.SelectedItem.Key, "|")) Then
                '-> Reprise
                GoTo Rep
            End If
        Else
            '-> Message de lock
            LibError = Catalogues("00058").Fill_Label
            LibError = Replace(LibError, "$#EN#$", Entry(2, Me.ListView.SelectedItem.Key, "|"))
            LibError = Replace(LibError, "$#OP#$", Entry(1, aConnexion.ErrorParam, "�"))
            LibError = Replace(LibError, "$#DAT#$", Entry(2, aConnexion.ErrorParam, "�"))
            LibError = Replace(LibError, "$#PRO#$", Entry(3, aConnexion.ErrorParam, "�"))
            MsgBox LibError, vbExclamation + vbOKOnly, "Error"
        End If
        '-> Fin de la fonction
        GoTo GestError
    End If
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter le programme
    GoTo GestError
End If

'-> Basculer zondata dans zonlock
SetZonLockFromZonData aEnreg

'---> precharge les zones de la fenetre de saisie pour modif
frmSaiObjectif.txtDate(0).Text = Me.ListView.SelectedItem.Text
frmSaiObjectif.Text2.Text = Me.ListView.SelectedItem.SubItems(1)
frmSaiObjectif.txtDate(1).Text = Me.ListView.SelectedItem.SubItems(2)
frmSaiObjectif.Text4.Text = Me.ListView.SelectedItem.SubItems(3)
'-> On interdit la modif de la date ( car clef primaire )
frmSaiObjectif.pctCalendar(0).Enabled = False

strRetour = ""
'-> On ouvre la fenetre de saisie
frmSaiObjectif.Show vbModal
'-> test si retour = rien
If strRetour = "" Then Exit Sub

'-> Bloquer les enregs
Me.Enabled = False
Screen.MousePointer = 11
Me.ListView.Enabled = False

'-> On recupere les zones saisies en retour et renseigne l'enreg
aEnreg.Fields("00034").ZonLock = Entry(2, strRetour, "�")
aEnreg.Fields("00035").ZonLock = Entry(3, strRetour, "�")
aEnreg.Fields("00036").ZonLock = Entry(4, strRetour, "�")

'-> Ok : faire l'update sur l'enreg
If Not UpdateEnreg(aConnexion, MasterFname, aEnreg, True) Then
    '-> Tester  le code erreur 90026 pas de modif
    If aConnexion.ErrorNumber = "90026" Then GoTo GestError
    '-> Afficher une message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Fin du programme
    GoTo GestError
End If

'-> Mettre � jour l'affichage
DisplayEnregInListView aEnreg, Me.ListView, aDescro

'-> Formatter le listview
FormatListView Me.ListView

GestError:
    '-> Bloquer les enreg
    Me.Enabled = True
    Screen.MousePointer = 0
    Me.ListView.Enabled = True
    
End Sub


Private Sub DeleteData()

'---> Cette proc�dure supprime un enreg
Dim LibError As String
Dim Rep As VbMsgBoxResult

On Error GoTo GestError

'-> Demander confirmation
Rep = MsgBox(Catalogues("00059").Fill_Label, vbExclamation + vbYesNo, Catalogues("00053").Fill_Label)
If Rep <> vbYes Then Exit Sub

'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11

'-> Faire un DeleteData
Rep:
If Not DeleteEnreg(aConnexion, MasterFname, Entry(2, Me.ListView.SelectedItem.Key, "|")) Then
    '-> Tester si l'enregistrement n'est pas lock� par un autre
    If aConnexion.ErrorNumber = "90006" Then
        '-> v�rifier si le lock est actif
        If Entry(4, aConnexion.ErrorParam, "�") = "0" Then
            '-> Envoyer un B3D unlock
            If UnlockEnreg(aConnexion, MasterFname, Entry(2, Me.ListView.SelectedItem.Key, "|")) Then
                '-> Reprise
                GoTo Rep
            End If
        Else
            '-> Message de lock
            LibError = Catalogues("00058").Fill_Label
            LibError = Replace(LibError, "$#EN#$", Entry(2, Me.ListView.SelectedItem.Key, "|"))
            LibError = Replace(LibError, "$#OP#$", Entry(1, aConnexion.ErrorParam, "�"))
            LibError = Replace(LibError, "$#DAT#$", Entry(2, aConnexion.ErrorParam, "�"))
            LibError = Replace(LibError, "$#PRO#$", Entry(3, aConnexion.ErrorParam, "�"))
            MsgBox LibError, vbExclamation + vbOKOnly, "Error"
        End If
        '-> Fin de la fonction
        GoTo GestError
    End If
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
    '-> Quitter le programme
    GoTo GestError
End If

'-> Supprimer l'enreg du browse
Me.ListView.ListItems.Remove (Me.ListView.SelectedItem.Key)

GestError:
    '-> Bloquer les enreg
    Me.Enabled = True
    Screen.MousePointer = 0

End Sub

Private Sub mnuAjouter_Click()
strRetour = "CREATE"
End Sub

Private Sub mnuModifier_Click()
strRetour = "UPDATE"
End Sub

Private Sub mnuSupprimer_Click()
strRetour = "DELETE"
End Sub

Private Sub mnuZoom_Click()
strRetour = "ZOOM"
End Sub

Private Sub cmsCapture_Click()
'---> On envoie une impression des data salaire employ�
'---> On genere un fichier de data pour lancer l'impression

On Error GoTo GestError

Dim aEnreg As B3DEnreg
Dim aIt As ListItem
Dim ListeFields As String
Dim i As Integer
Dim HdlFile As Integer
Dim LigneE As String


'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11
Me.ListView.Enabled = False

'-> On ouvre un nouveau fichier en ecriture
HdlFile = FreeFile
Open App.Path & "\DATA\DataFic.txt" For Output As #HdlFile

'-> On envoi le diese
Print #HdlFile, "[ST-SECTION1(TXT-Section)][]{^0002" & Me.CodeEmploye & "}"
Print #HdlFile, "[TB-NouveauTableau2(BLK-NouveauBlock1)][\1]{}"

'-> On definit la liste des champs a imprimer
ListeFields = "00033,00034,00035,00036" '-> Date prime, libelle prime et montant prime

'---> Parcours toutes les lignes de prime pour creer les instruction turbo a envoyer
For Each aIt In Me.ListView.ListItems
    '-> Recup de l'enreg correspondant a la ligne de prime en cours
    Set aEnreg = FindData(aConnexion, aConnexion.Ident, MasterFname, Entry(2, aIt.Key, "|"), True, False, aDescro, False)

    '-> Tester le retour
    If aEnreg Is Nothing Then
        '-> Message d'erreur
        MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Error"
        '-> Quitter le programme
        GoTo GestError
    End If
    
    LigneE = "[TB-NouveauTableau2(BLK-NouveauBlock2)][\1]{"
    
    '-> On parcours les champs a imprimer et on les envoi dans le fichier d'instructions turbo
    For i = 1 To NumEntries(ListeFields, ",")
        '-> On parcours tous les champs de l'enreg pour creer la ligne d'impression turbo
        LigneE = LigneE & "^" & Format(aEnreg.Fields(Entry(i, ListeFields, ",")).Field, "0000") & Trim(aEnreg.Fields(Entry(i, ListeFields, ",")).ZonData)
    Next '-> On passe au prochain champ a imprimer sur cette ligne

    LigneE = LigneE & " }"
    '-> envoi de la ligne
    Print #HdlFile, LigneE

Next '-> On passe a la prochaine ligne de prime a imprimer

'-> Fermeture du fichier
Close #HdlFile

'-> On envoie la fusion entre la maquette et le fichier data puis l'impression TURBO
PrintDataInTurbo App.Path & "\DATA\b3d_objectifs_employe.maqgui", App.Path & "\DATA\DataFic.txt", App.Path & "\DATA\MyPrint.turbo", True

GestError:
    Me.Enabled = True
    Screen.MousePointer = 0
    Me.ListView.Enabled = True

End Sub


