VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmICum 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   6780
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5415
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6780
   ScaleWidth      =   5415
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command1 
      Height          =   615
      Left            =   4800
      Picture         =   "frmICum.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   6120
      Width           =   615
   End
   Begin VB.CommandButton Command2 
      Height          =   615
      Left            =   4080
      Picture         =   "frmICum.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   6120
      Width           =   615
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   6015
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   10610
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   1680
      Top             =   5880
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmICum.frx":1994
            Key             =   "OUI"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmICum.frx":266E
            Key             =   "NON"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmICum"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private aIname As Iname
Private aIdent As clsIdent

Private Sub Command1_Click()

Dim aField As Field
Dim X As ListItem

'-> Vider dans un premier temps la liste des champs � cumuls
Do While aIname.iCums.Count <> 0
    aIname.iCums.Remove (1)
Loop

'-> Enregistrer
For Each X In Me.ListView1.ListItems
    '-> S'il est s�lectionn�
    If X.Tag = "OUI" Then
        '-> Cr�er un nouveau champ
        Set aField = New Field
        aField.Num_Field = X.Text
        '-> Ajouter dans la collection
        aIname.iCums.Add aField, aField.Num_Field
    End If
Next

'-> Enregistrer le sch�ma
aIdent.ServeurDOG.Save_Schema

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("FRMSCHEMA", "38")

'-> Entete des colonnes
Me.ListView1.ColumnHeaders(1).Text = GetMesseprog("CONSOLE", "17")
Me.ListView1.ColumnHeaders(2).Text = GetMesseprog("FRMSCHEMA", "4")

End Sub


Public Sub Init(Index As Iname, aFname As Fname, Ident As clsIdent)

Dim aField As Field
Dim aEmilieObj As EmilieObj
Dim ToAdd As Boolean
Dim X As ListItem
Dim FieldCum As Field

'-> Afficher la liste des champs d�cimaux affect�s � cette table
For Each aField In aFname.Fields
    ToAdd = True
    '-> Pointer sur l'objet du catalogue
    Set aEmilieObj = Ident.ServeurDOG.Catalogue(Ident.Code & "|" & aField.Num_Field)
    '-> Si on doit ajouter en fonction du format
    If UCase$(aEmilieObj.DataType) <> "DECIMAL" And UCase$(aEmilieObj.DataType) <> "INTEGER" Then ToAdd = False
    '-> Ajouter
    If ToAdd Then
        '-> ajouter un item
        Set X = Me.ListView1.ListItems.Add(, , aField.Num_Field)
        '-> Positionner le code langue
        aEmilieObj.CodeLangue = OpeCodeLangue
        '-> Ajouter le libell�
        X.SubItems(1) = aEmilieObj.Fill_Label
        '-> Fait il partie ou non de l'index � cumul
        If Ident.ServeurDOG.IsFieldCumIname(Index, aField.Num_Field) Then
            X.Tag = "OUI"
            X.SmallIcon = "OUI"
        Else
            X.Tag = "NON"
            X.SmallIcon = "NON"
        End If
    End If
Next 'Pour tous les champs

'-> Initialiser les pointeurs
Set aIname = Index
Set aIdent = Ident

'-> Titre de la feuille
aIname.CodeLangue = OpeCodeLangue
Me.Caption = Me.Caption & " [" & aIname.Num_Iname & "-" & aIname.Designation & "]"

End Sub



Private Sub ListView1_ItemClick(ByVal Item As MSComctlLib.ListItem)

'-> Inverser les proprit�s
If Item.Tag = "OUI" Then
    Item.Tag = "NON"
Else
    Item.Tag = "OUI"
End If

'-> Afficher la bonne icone
Item.SmallIcon = Item.Tag

End Sub
