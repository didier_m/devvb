VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmGestConnect 
   Caption         =   "Form1"
   ClientHeight    =   7095
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9885
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   473
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   659
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7200
      Top             =   2640
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestConnect.frx":0000
            Key             =   "OPERAT"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      Height          =   975
      Left            =   0
      ScaleHeight     =   975
      ScaleWidth      =   8175
      TabIndex        =   1
      Top             =   0
      Width           =   8175
      Begin VB.CommandButton Command1 
         Height          =   870
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   60
         Width           =   1095
      End
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3255
      Left            =   0
      TabIndex        =   0
      Top             =   1080
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   5741
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   7
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Width           =   38100
      EndProperty
   End
   Begin VB.Image ServStop 
      Height          =   720
      Left            =   6000
      Picture         =   "frmGestConnect.frx":0CDA
      Top             =   3240
      Width           =   720
   End
   Begin VB.Image ServRun 
      Height          =   720
      Left            =   4680
      Picture         =   "frmGestConnect.frx":29A4
      Top             =   3000
      Width           =   720
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "POPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuDetectFail 
         Caption         =   ""
      End
      Begin VB.Menu mnuUnloadCon 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmGestConnect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()

'---> Lancer ou arreter le serveur de connexion

Dim Rep As VbMsgBoxResult

If ServConRunning Then 'Demande d'arret

    '-> Demander d'arreter
    Rep = MsgBox(GetMesseprog("APPLIC", "37"), vbExclamation + vbYesNo, GetMesseprog("APPLIC", "21"))
    If Rep = vbNo Then Exit Sub
    
    '-> Arreter le serveur d'acc�s
    frmCom.TimerAcces.Enabled = False

    '-> Indiquer que a console est arret�e
    ServConRunning = False

    '-> Icone
    Set Me.Command1.Picture = Me.ServStop.Picture

Else 'Demande de lancement
  
    '-> Lancer le serveur d'acces
    frmCom.TimerAcces.Interval = TempoAcces
    frmCom.TimerAcces.Enabled = True

    '-> Indiquer que la console tourne
    ServConRunning = True

    '-> Icone
    Set Me.Command1.Picture = Me.ServRun.Picture

End If 'Si on doit lancer arreter
    
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 116 Then Init
    
End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("CONSOLE", "7")

'-> Entete des colonnes
Me.ListView1.ColumnHeaders(1).Text = GetMesseprog("CONSOLE", "35") 'Agent
Me.ListView1.ColumnHeaders(2).Text = GetMesseprog("CONSOLE", "39") 'IdSession
Me.ListView1.ColumnHeaders(3).Text = GetMesseprog("APPLIC", "19") 'Operat
Me.ListView1.ColumnHeaders(4).Text = GetMesseprog("CONSOLE", "37") 'Ident
Me.ListView1.ColumnHeaders(5).Text = GetMesseprog("CONSOLE", "38") 'Date
Me.ListView1.ColumnHeaders(6).Text = GetMesseprog("CONSOLE", "21") 'hdlproces

'-> Libell�s des menus
Me.mnuDetectFail.Caption = GetMesseprog("CONSOLE", "94")
Me.mnuUnloadCon.Caption = GetMesseprog("CONSOLE", "95")

End Sub

Public Sub Init()

Dim aCon As clsConnectConsole
Dim x As ListItem

On Error GoTo GestError

'-> Bloquer la feuille
Me.Enabled = False
LockWindowUpdate Me.hWnd

'-> vider les connexions existantes
Me.ListView1.ListItems.Clear

'-> Propri�t�s de l'icone
If ServConRunning Then
    Set Me.Command1.Picture = Me.ServRun.Picture
Else
    Set Me.Command1.Picture = Me.ServStop.Picture
End If

'-> Afficher la liste des connexions en cours
For Each aCon In Connexions
    '-> Ajouter un ListItem
    Set x = Me.ListView1.ListItems.Add(, "CON|" & CStr(aCon.IndexAgent), , , "OPERAT")
    '-> Propri�t�s
    x.Text = Format(aCon.IndexAgent, "0000")
    x.SubItems(1) = aCon.IdSession
    x.SubItems(2) = aCon.Operat
    x.SubItems(3) = aCon.Ident
    x.SubItems(4) = aCon.DateConnexion
    x.SubItems(5) = aCon.hdlProcess
Next 'Pour toutes les agents connect�s

'-> Formatter le listView
FormatListView Me.ListView1

GestError:
    LockWindowUpdate 0
    Me.Enabled = True


End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next
GetClientRect Me.hWnd, aRect
Me.ListView1.Left = 0
Me.ListView1.Top = Me.Picture1.Height
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom - Me.ListView1.Top
Me.Picture1.Width = aRect.Right

End Sub


Private Sub DetectFailedProcess()

'-> V�rifier si l'agent B3D sp�cifi� est encore actif

Dim x As ListItem
Dim aCon As clsConnectConsole

'-> Analyse de toutes les connexions
For Each x In Me.ListView1.ListItems
    '-> Pointer sur la connexion
    Set aCon = Connexions("CON|" & CInt(x.Text))
    '-> V�rifier si le handle sp�cifi� est toujours valide
    If Not IsProcessRunning(aCon.hdlProcess) Then
        '-> Supprimer
        UnloadAgentB3D aCon
    End If
Next 'Pour toutes les connextions

'-> Relancer l'initialisation
Me.Init

'-> Actualiser l'affichage des identifiants
If IsWindow(hdlServIdents) = 1 Then frmGestIdent.Init

End Sub

Private Sub StopAgentB3D()

'---> Cette proc�dure relache un agent B3D

Dim aCon As clsConnectConsole
Dim hdlComAgent As Long

'-> Pointer sur la connexion
Set aCon = Connexions("CON|" & CInt(Me.ListView1.SelectedItem.Text))

'-> V�rifier si le handle est actif ou non
If Not IsProcessRunning(aCon.hdlProcess) Then
    '-> Supprimer
    UnloadAgentB3D aCon
Else
    '-> R�cup�rer le handle de la feuille principale de l'agent
    hdlComAgent = HandleFromProcessID(aCon.hdlProcess)
    '-> Envoyer un ordre de fin
    SendMessage hdlComAgent, WM_CLOSE, 0, 0
    '-> Supprimer la connexion
    UnloadAgentB3D aCon
End If

'-> Mettre � jour l'affichage
Me.Init

'-> Actualiser l'affichage des identifiants
If IsWindow(hdlServIdents) = 1 Then frmGestIdent.Init


End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

'-> Tester le bouton
If Button <> vbRightButton Then Exit Sub

'-> V�rifier qu'il y ait au moins une connexion
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu Me.mnuPOPUP

'-> Tester le retour
Select Case strRetour
    Case "DETECT"
        DetectFailedProcess
    Case "CLEAN"
        StopAgentB3D
End Select

'-> Vider la variable de retour = strretour = ""
strRetour = ""

End Sub

Private Sub mnuDetectFail_Click()
strRetour = "DETECT"
End Sub

Private Sub mnuUnloadCon_Click()
strRetour = "CLEAN"
End Sub
