Attribute VB_Name = "fctConsole"
Option Explicit

'-> D�claration des API pour gestion du scroll principal
Public Type POINTAPI
    X As Long
    Y As Long
End Type
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function ScreenToClient Lib "user32" (ByVal hWnd As Long, lpPoint As POINTAPI) As Long
Public Declare Function IsWindow Lib "user32" (ByVal hWnd As Long) As Long
Public Declare Function SendMessage& Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any)
Public Declare Function SetWindowText& Lib "user32" Alias "SetWindowTextA" (ByVal hWnd As Long, ByVal lpString As String)
Public Declare Function GetWindowText& Lib "user32" Alias "GetWindowTextA" (ByVal hWnd As Long, ByVal lpString As String, ByVal cch As Long)

'-> API pour run synchrone d'un prog
Const GW_HWNDFIRST = 0
Const GW_HWNDNEXT = 2
Const PROCESS_QUERY_INFORMATION = &H400
Private Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hWnd As Long, lpdwProcessId As Long) As Long
Private Declare Function GetParent Lib "user32" (ByVal hWnd As Long) As Long
Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Long, lpExitCode As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Private Declare Function GetWindow Lib "user32" (ByVal hWnd As Long, ByVal wCmd As Long) As Long

'-> Variables globales d'initialisation
Public IniPath As String '-> Emplacement du fichier B3D.ini
Public HeadPath As String '-> Emplacement de la console
Public MainDirectory As String '-> Emplacement du site B3D
Public ApplicationDirectory As String '-> Emplacement des applications
Public UcId As String '-> Identificateur de CPU
Public TempoAcces As Long '-> D�lai de scan du r�pertoire de connexion en MS
Public ConsoleMode As String '-> Type de console SERVEUR/MONOPOSTE
Public ConsoleSession As String '-> Pour console monoposte : Ident � ouvrir
Public B3DMode As String '-> Niveau de privil�ge B3D
Public IdentSession As String '-> Indent s�lectionn� au d�marrage
Public VecteurLock As String '-> Vecteur des erreurs g�n�r�es par un conflit de lock

'-> Variables de connexion op�rateur
Public OpeConsole As String '-> Op�rateur affect� � la console
Public OpePassword As String '-> Mot de passe
Public OpeCodeLangue As Integer '-> Code Langue
Public OpeDateLimit As String '-> Date de limitte de vaidit�
Public OpeMulti As Integer '-> Si multi Access
Public OpePhoto As String '-> Photo Associ�e
Public OpeListIdent As String '-> Liste des idents attach�s

'-> Variables pour le serveur
Public hdlServIdents As Long '-> Handle du gestionnaire d'ident
Public hdlServCon As Long  '-> Handle du gestionnaire de connexions
Public hdlServBases As Long '-> Handle du gestionnaire de base
Public ServConRunning As Boolean '-> Indique si le portail est actif ou non
Public hdlCatalogue As Long '-> Handle du catalogue
Public hdlSchema As Long '-> Handle du sch�ma
Public hdlSys As Long '-> Handle des tables systemes
Public hdlMessprog As Long '-> Handle des messprog
Public hdlGrp As Long '-> Handle des groupes

'-> Variables de debug
Public FichierDebugAgent As String  '-> Fichier de debou pour l'agent
Public FichierDebugProg As String  '-> Fichier de debug programme

'-> Matrice d'indexation des agents B3D et des IDX
Public Matrice_Num_Agent_B3D(1 To 5000) As Boolean '-> Matrice des numeros des agents
Public Matrice_Num_Agent_IDX(5001 To 9999) As Boolean '-> Matrice des numeros des index

'-> Matrice de communication
Public Matrice_Com_Idx(5001 To 9999) As Boolean    '-> Pour handle de communication
Public Matrice_Com_Agent(1 To 5000) As Boolean    '-> Pour handle de communication

'-> Collection des idents
Public Idents As Collection
'-> Collection des bases
Public Bases As Collection
'-> Collection des descripteurs de connexions
Public Connexions As Collection

'-> Indique le process en cours
Public hdlProcess As Long


Public Sub Main()

'---> Point d'entr�e du programme

Dim IniFile As String

'-> R�cup�rer la ligne de command
If Trim(Command$) = "" Then
    MsgBox "Erreur de param�trage", vbCritical + vbOKOnly, "Fatal Error"
    End
End If

'-> Affecter l'emplacement
HeadPath = Trim(Entry(1, Command$, "|"))
ConsoleMode = Trim(UCase$(Entry(2, Command$, "|")))

'-> V�rifier que l'on trouve le r�pertoire sp�cifi�
If GetAttr(HeadPath) <> vbDirectory Then
    MsgBox "Erreur de param�trage", vbCritical + vbOKOnly, "Fatal Error"
    End
End If

'-> V�rifier le mode affect� de la console
Select Case UCase$(Trim(ConsoleMode))
    Case "SERVEUR", "CLIENT", "MONOPOSTE"
    Case Else
        MsgBox "Erreur de param�trage", vbCritical + vbOKOnly, "Fatal Error"
        End
End Select

'-> Afficher en fond la feuille principale
mdiConsole.Show

'-> Initialiser les param�trages
Call Initialisation

'-> Lancer la connexion op�rateur
Call ConnectOperat

'-> Lancer le chargement des diff�rents menus
Call InitMenuConsole

'-> Initialiser la console selon le mode de traitement
Call InitConsole

 End Sub

Private Sub InitConsole()

'---> Cette proc�dure initialise la console selon les diff�rents modes

'-> Charger la liste des identifiants
Call LoadIdent
'-> Charger la liste des base
Call LoadBase

'-> Selon le type de console
Select Case UCase$(Trim(ConsoleMode))
    Case "SERVEUR"
        '-> Charger la feuille de communication
        Load frmCom
    Case "CLIENT", "MONOPOSTE"
        '-> Initialiser la console du type client
        InitConsoleClient
End Select

End Sub


Public Sub UnloadAgentConsole()

'---> Cette proc�dure ferme l'agent B3D affect� � la console dans le cadre des versions CLIENT/MONOPOSTE

Dim hdlFile As Integer
Dim TempFile As String

'-> Setting du fichier temporaire
TempFile = SessionDmdFile & ".tmp"

'-> Ouvrir un fichier temporaire
hdlFile = FreeFile
Open TempFile For Output As #hdlFile

'-> Ordre de fin
Print #hdlFile, "B3D_EXIT~"
Close #hdlFile

'-> V�rifier qu'une demande ne traine pas
If Dir$(SessionDmdFile, vbNormal) <> "" Then Kill SessionDmdFile

'-> Faire un rename du fichier
Name TempFile As SessionDmdFile

End Sub

Private Sub InitConsoleClient()

Dim aIdent As clsIdent
Dim ListeMenu As String
Dim MenuFile As String
Dim i As Integer
Dim DefMenu As String

'-> Bloquer l'�cran
mdiConsole.Enabled = False
Screen.MousePointer = 11

'-> Lancer une connexion vers un agent B3D en mode Client/Serveur
If Not ConnectAgentConsole(OpeConsole, OpePassword, IdentSession, "0", "0", "0") Then End

'-> Pointer sur l'ident associ� � la console
Set aIdent = Idents("IDENT|" & UCase$(Trim(IdentSession)))

'-> Charger le dog Associ� � cet ident
aIdent.ConnectDOG

'-> Charger le menu de l'op�rateur
ListeMenu = DeCrypt(GetIniFileValue(OpeConsole, "MENU_" & IdentSession, ApplicationDirectory & "B3D\Param\Operats.ini"))
If Trim(ListeMenu) <> "" Then
    '-> Pour tous les menus
    For i = 1 To NumEntries(ListeMenu, "|")
        '-> Charger le menu sp�cifi�
        Call LoadMenu(ApplicationDirectory & IdentSession & "\Param\" & Entry(i, ListeMenu, "|"))
    Next 'Pour tous les menus
End If 'S'il y a des menus

'-> Titre de la feuille
mdiConsole.Caption = mdiConsole.Caption & " [ " & IdentSession & " - " & DeCrypt(GetIniFileValue("IDENTS", IdentSession, ApplicationDirectory & "B3D\Param\Idents.ini")) & " ]"

'-> D�bloquer l'�cran
mdiConsole.Enabled = True
Screen.MousePointer = 0

End Sub
Private Sub LoadMenu(FileName As String)

'---> Cette prco�dure charge un menu

Dim ListeMenu As String
Dim DefMenu As String
Dim pMenu As String
Dim ParentMenu As String
Dim TypeMenu As Integer
Dim ProgMenu As String
Dim LibMenu As String
Dim ZoneMenu As String
Dim KeyNewMenu As String
Dim i As Integer
Dim aNode As Node

'-> V�rifier dans un premier temps que le fichier spoit rens�ign�
If Dir$(FileName) = "" Then Exit Sub

'-> R�cup�rer la liste des menus
ListeMenu = GetIniFileValue("MENU", "", FileName, True)

'-> Tester qu'il y ait quelquechose
If Trim(ListeMenu) <> "" Then
    '-> Analyse de tous les niveaux de menus
    For i = 1 To NumEntries(ListeMenu, Chr(0))
        '-> Get d'une d�finition
        DefMenu = Entry(i, ListeMenu, Chr(0))
        '-> Quitter si plus rien
        If Trim(DefMenu) = "" Then Exit For
        
        '-> Eclater en param�tres
        pMenu = DeCrypt(Mid$(DefMenu, Len(Entry(1, DefMenu, "=")) + 2))
        ParentMenu = Trim(Entry(1, pMenu, "�"))
        TypeMenu = Entry(2, pMenu, "�")
        ProgMenu = Entry(3, pMenu, "�")
        LibMenu = Entry(4, pMenu, "�")
        ZoneMenu = Entry(5, pMenu, "�")
        KeyNewMenu = Entry(6, pMenu, "�")
        
        '-> Cr�er un nouveau node
        Set aNode = mdiConsole.TreeNaviga.Nodes.Add(ParentMenu, 4, ParentMenu & "~" & KeyNewMenu)
                        
        '-> Positionner le libelle
        aNode.Text = Entry(OpeCodeLangue, LibMenu, "~")
        
        '-> Positionner les icones
        If (TypeMenu) = 0 Then
            '-> Icone
            aNode.Image = "CLOSE"
            aNode.ExpandedImage = "OPEN"
        Else
            '-> Icone
            aNode.Image = "APP"
            '-> Poser le tag
            aNode.Tag = "PROG" & Chr(0) & ProgMenu & Chr(0) & ZoneMenu
        End If
                                    
    Next 'Pour tous les niveaux de menu
End If 'S'il y a des menus

'-> D�bloquer la mise � jour de l'�cran
LockWindowUpdate 0




End Sub


Private Sub LoadBase()

'---> Cette eproc�dure charge la liste des bases de la console

Dim ListeBase As String
Dim res As Long
Dim lpBuffer As String
Dim i As Integer
Dim DefBase As String
Dim aBase As clsBase

'-> Get de la liste des bases
lpBuffer = Space$(32000)
res = GetPrivateProfileSectionNames(lpBuffer, Len(lpBuffer), ApplicationDirectory & "B3D\Param\Bases.ini")
If res = 0 Then Exit Sub

'-> Tronquer les blancs
lpBuffer = Mid$(lpBuffer, 1, res)

'-> Analyse de la liste des bases
For i = 1 To NumEntries(lpBuffer, Chr(0))
    '-> Get d'une d�finition
    DefBase = Entry(i, lpBuffer, Chr(0))
    If Trim(DefBase) = "" Then Exit For
    '-> Cr�er une nouvelle instance de base
    Set aBase = New clsBase
    '-> Init de ses propri�t�s
    aBase.Nom = DefBase
    aBase.TypeBase = DeCrypt(GetIniFileValue(DefBase, "TYPE", ApplicationDirectory & "B3D\Param\Bases.ini"))
    aBase.Pwd = DeCrypt(GetIniFileValue(DefBase, "PWD", ApplicationDirectory & "B3D\Param\Bases.ini"))
    aBase.User = DeCrypt(GetIniFileValue(DefBase, "USER", ApplicationDirectory & "B3D\Param\Bases.ini"))
    aBase.NumIdx = CInt(DeCrypt(GetIniFileValue(DefBase, "IDX", ApplicationDirectory & "B3D\Param\Bases.ini")))
    aBase.TempoLock = CLng(DeCrypt(GetIniFileValue(DefBase, "TEMPO_LOCK", ApplicationDirectory & "B3D\Param\Bases.ini")))
    aBase.Fichier = Replace(DeCrypt(GetIniFileValue(DefBase, "FILE", ApplicationDirectory & "B3D\Param\Bases.ini")), "$HEAD$", HeadPath)
    aBase.Serveur = DeCrypt(GetIniFileValue(DefBase, "SERVEUR", ApplicationDirectory & "B3D\Param\Bases.ini"))
    '-> Ajouter la base
    Bases.Add aBase, "BASE|" & UCase$(Trim(aBase.Nom))
Next 'Pour toutes les base


End Sub

Private Sub LoadIdent()

'---> Cette proc�dure initialise la collection des identifiants clients

Dim ListeIdents As String
Dim DefIdent  As String
Dim Ident As String
Dim i As Integer
Dim aIdent As clsIdent

'-> Afficher la liste des idents
ListeIdents = GetIniFileValue("IDENTS", "", ApplicationDirectory & "B3D\Param\Idents.ini", True)
If Trim(ListeIdents) = "" Then Exit Sub

'-> Pour tous les idents
For i = 1 To NumEntries(ListeIdents, Chr(0))
    '-> Get d'une d�finition
    DefIdent = Entry(i, ListeIdents, Chr(0))
    If Trim(DefIdent) = "" Then Exit For
    '-> Cr�er la liste des menus pour cet ident
    Ident = Entry(1, DefIdent, "=")
    '-> Cr�er une nouvelle instance d'un objet Ident
    Set aIdent = New clsIdent
    '-> Setting de ses propri�t�s
    aIdent.Code = Ident
    aIdent.Libel = Entry(2, DefIdent, "=")
    aIdent.FichierCatalogue = ApplicationDirectory & Ident & "\Param\CAT-" & Ident & ".dat"
    aIdent.FichierLink = ApplicationDirectory & Ident & "\Param\LNK-" & Ident & ".dat"
    aIdent.FichierSchema = ApplicationDirectory & Ident & "\Param\SCH-" & Ident & ".dat"
    aIdent.FichierMessprog = ApplicationDirectory & Ident & "\Param\Messprog-CODELANGUE.ini"
    aIdent.Limite_AgentB3D = CLng(DeCrypt(GetIniFileValue("PARAM", "ACCES_DEAL", ApplicationDirectory & Ident & "\Param\" & Ident & ".ini")))
    aIdent.Base = DeCrypt(GetIniFileValue("PARAM", "BASE", ApplicationDirectory & Ident & "\Param\" & Ident & ".ini"))
    aIdent.IniFile = ApplicationDirectory & Ident & "\Param\" & Ident & ".ini"
    '-> Ajouter l'ident dans la collection
    Idents.Add aIdent, "IDENT|" & UCase$(Trim(aIdent.Code))
Next 'Pour tous les idents

End Sub


Private Sub InitMenuConsole()

'---> Cette proc�dure charge le param�trage d'une console

Dim aNode As Node

Select Case UCase$(Trim(ConsoleMode))

    Case "SERVEUR"
    
        '-> Cr�er un node pour la gestion de la console
        Set aNode = mdiConsole.TreeNaviga.Nodes.Add(, , "GESTCONSOLE", , "CLOSEFOLDER")
        aNode.Text = GetMesseprog("CONSOLE", "6")
        aNode.ExpandedImage = "OPENFOLDER"
        aNode.Expanded = True
        
            '-> Gestion des idents
            Set aNode = mdiConsole.TreeNaviga.Nodes.Add("GESTCONSOLE", 4, "SERVEURIDENT", , "SHORTCUT")
            aNode.Text = GetMesseprog("CONSOLE", "16")
            aNode.Tag = "SERVEURIDENT"
            
            '-> Gestion des bases
            Set aNode = mdiConsole.TreeNaviga.Nodes.Add("GESTCONSOLE", 4, "SERVEURBASE", , "SHORTCUT")
            aNode.Text = GetMesseprog("CONSOLE", "23")
            aNode.Tag = "SERVEURBASE"
        
            '-> Portail
            Set aNode = mdiConsole.TreeNaviga.Nodes.Add("GESTCONSOLE", 4, "PORTAIL", , "SHORTCUT")
            aNode.Text = GetMesseprog("CONSOLE", "7")
            aNode.Tag = "PORTAIL"
        
        '-> Cr�er un node pour le param�trage console
        Set aNode = mdiConsole.TreeNaviga.Nodes.Add(, , "PARAMCONSOLE", , "CLOSEFOLDER")
        aNode.Text = GetMesseprog("CONSOLE", "8")
        aNode.ExpandedImage = "OPENFOLDER"
        aNode.Expanded = True
        
            '-> Portail
            Set aNode = mdiConsole.TreeNaviga.Nodes.Add("PARAMCONSOLE", 4, "GESTIDENT", , "SHORTCUT")
            aNode.Text = GetMesseprog("CONSOLE", "9")
            aNode.Tag = "PROGB3D" & Chr(0) & "B3D_GESTIDENT.EXE" & Chr(0) & ApplicationDirectory & "�" & OpeCodeLangue
            
            '-> Gestion des op�rateurs
            Set aNode = mdiConsole.TreeNaviga.Nodes.Add("PARAMCONSOLE", 4, "GESTOPE", , "SHORTCUT")
            aNode.Text = GetMesseprog("CONSOLE", "10")
            aNode.Tag = "PROGB3D" & Chr(0) & "B3D_GESTOPERAT.EXE" & Chr(0) & ApplicationDirectory & "�" & OpeCodeLangue
            
            '-> Gestion des menu
            Set aNode = mdiConsole.TreeNaviga.Nodes.Add("PARAMCONSOLE", 4, "GESTMENU", , "SHORTCUT")
            aNode.Text = GetMesseprog("CONSOLE", "11")
            aNode.Tag = "PROGB3D" & Chr(0) & "B3D_GESTMENU.EXE" & Chr(0) & ApplicationDirectory & "�" & OpeCodeLangue
    
            '-> Quitter la proc�dure
            Exit Sub
    
    Case "CLIENT" '-> On est en version Client : Acc�s � la gestion du param�trage de l'ident
        
        '-> Cr�er un node pour le param�trage B3D
        Set aNode = mdiConsole.TreeNaviga.Nodes.Add(, , "PARAMB3D", , "CLOSEFOLDER")
        aNode.Text = GetMesseprog("CONSOLE", "1")
        aNode.ExpandedImage = "OPENFOLDER"
        aNode.Expanded = True
        
            '-> Catalogue
            Set aNode = mdiConsole.TreeNaviga.Nodes.Add("PARAMB3D", 4, "CATALOGUE", , "SHORTCUT")
            aNode.Text = GetMesseprog("CONSOLE", "2")
            aNode.Tag = "CATALOGUE"
                
            '-> Schema
            Set aNode = mdiConsole.TreeNaviga.Nodes.Add("PARAMB3D", 4, "SCHEMA", , "SHORTCUT")
            aNode.Text = GetMesseprog("CONSOLE", "3")
            aNode.Tag = "SCHEMA"
            
            '-> Table Sys
            Set aNode = mdiConsole.TreeNaviga.Nodes.Add("PARAMB3D", 4, "DOGSYS", , "SHORTCUT")
            aNode.Text = GetMesseprog("CONSOLE", "4")
            aNode.Tag = "DOGSYS"
            
            '-> Gestion des groupes
            Set aNode = mdiConsole.TreeNaviga.Nodes.Add("PARAMB3D", 4, "DOGGRP", , "SHORTCUT")
            aNode.Text = GetMesseprog("CONSOLE", "96")
            aNode.Tag = "DOGGRP"
            
            '-> Message
            Set aNode = mdiConsole.TreeNaviga.Nodes.Add("PARAMB3D", 4, "MESSPROG", , "SHORTCUT")
            aNode.Text = GetMesseprog("CONSOLE", "5")
            aNode.Tag = "MESSPROG"
            
            '-> Agent B3D
            Set aNode = mdiConsole.TreeNaviga.Nodes.Add("PARAMB3D", 4, "AGENTCONSOLE", , "SHORTCUT")
            aNode.Text = GetMesseprog("CONSOLE", "40")
            aNode.Tag = "AGENTCONSOLE"
End Select

'-> Cr�er un node pour le param�trage B3D
Set aNode = mdiConsole.TreeNaviga.Nodes.Add(, , "NODEROOT", , "CLOSEFOLDER")
aNode.Text = GetMesseprog("CONSOLE", "12")
aNode.ExpandedImage = "OPENFOLDER"
aNode.Expanded = True

End Sub

Private Sub ConnectOperat()

'---> Cette proc�dure est charg�e d'�ffectuer la connexion op�rateur

On Error GoTo GestError

'-> Vider la variable de retour
strRetour = ""

'-> Charger la feuille en modal
frmConnexion.Show vbModal

'-> Tester le retour
If strRetour = "" Then End

'-> Charger le param�trage op�rateur
OpeConsole = strRetour
OpePassword = DeCrypt(GetIniFileValue(OpeConsole, "PWD", ApplicationDirectory & "B3D\Param\Operats.ini"))
OpeCodeLangue = CInt(DeCrypt(GetIniFileValue(OpeConsole, "LANGUE", ApplicationDirectory & "B3D\Param\Operats.ini")))
OpePhoto = App.Path & "\Img\" & DeCrypt(GetIniFileValue(OpeConsole, "PHOTO", ApplicationDirectory & "B3D\Param\Operats.ini"))
OpeMulti = CInt(DeCrypt(GetIniFileValue(OpeConsole, "ACCESS", ApplicationDirectory & "B3D\Param\Operats.ini")))
OpeDateLimit = DeCrypt(GetIniFileValue(OpeConsole, "LIMIT", ApplicationDirectory & "B3D\Param\Operats.ini"))
OpeListIdent = DeCrypt(GetIniFileValue(OpeConsole, "IDENTS", ApplicationDirectory & "B3D\Param\Operats.ini"))

'-> Initialiser le fichier Mespprog � charger
MessprogIniFile = ApplicationDirectory & "B3D\Param\Messprog-" & Format(OpeCodeLangue, "00") & ".ini"

'-> Quitter la proc�dure
Exit Sub

GestError:
    MsgBox "Error Connecting Failure", vbCritical + vbOKOnly, "Fatal Error"
    End

End Sub


Private Sub Initialisation()

'---> Cette proc�dure charge le param�trage de la console dans le r�pertoire :
' App.path\Param\B3D.ini

Dim ErrorCode As Integer
Dim ErrorString As String
Dim Temp As Integer

On Error GoTo GestError

'-> Initialiser les diff�rentes collections
Set Connexions = New Collection
Set Idents = New Collection
Set Bases = New Collection

'-> Construire le nom du fichier Ini
ErrorCode = 1
IniPath = HeadPath & "\Applications\B3D\Param\B3D.ini"

'-> Recherche du fichier de param�trage
If Dir$(IniPath) = "" Then GoTo GestError

'-> Get des vecteurs locks
ErrorCode = 2
VecteurLock = DeCrypt(GetIniFileValue("LOCK", "VECTEUR", IniPath))
If Trim(VecteurLock) = "" Then GoTo GestError
             
'-> Init des r�pertoires B3D
ErrorCode = 3
MainDirectory = Replace(DeCrypt(GetIniFileValue("DIRECTORY", "MAIN", IniPath)), "$HEAD$", HeadPath)
If Right$(MainDirectory, 1) <> "\" Then MainDirectory = MainDirectory & "\"
Temp = (GetAttr(MainDirectory) And vbDirectory)
If Temp <> vbDirectory Then GoTo GestError

'-> Init des r�pertoires d'application
ErrorCode = 4
ApplicationDirectory = Replace(DeCrypt(GetIniFileValue("DIRECTORY", "APP", IniPath)), "$HEAD$", HeadPath)
If Right$(ApplicationDirectory, 1) <> "\" Then ApplicationDirectory = ApplicationDirectory & "\"
Temp = (GetAttr(ApplicationDirectory) And vbDirectory)
If Temp <> vbDirectory Then GoTo GestError

'-> Init de l'UC_ID
ErrorCode = 5
UcId = DeCrypt(GetIniFileValue("SERVEUR", "UC_ID", IniPath))
If Trim(UcId) = "" Then GoTo GestError

'-> Init de la temporisation en acc�s
ErrorCode = 6
TempoAcces = DeCrypt(GetIniFileValue("TEMPORISATION", "ACCESS", IniPath))
If Not IsNumeric(TempoAcces) Then GoTo GestError

'-> Init du Type B3D
ErrorCode = 8
B3DMode = DeCrypt(GetIniFileValue("CONTROL", "B3D", IniPath))
If B3DMode <> "DEV" And B3DMode <> "USER" Then GoTo GestError

'-> Init de l'ident de session monoposte
ErrorCode = 9
ConsoleSession = DeCrypt(GetIniFileValue("CONTROL", "SESSION", IniPath))
If ConsoleMode = "MONOPOSTE" And ConsoleSession = "" Then GoTo GestError

'-> Quitter la proc�dure
Exit Sub

'-> Gestion des erreurs
GestError:
    Select Case ErrorCode
        Case 1
            ErrorString = "Head"
        Case 2
            ErrorString = "VecteurLock"
        Case 3
            ErrorString = "MainDirectory "
        Case 4
            ErrorString = "ApplicationDirectory"
        Case 5
            ErrorString = "UCID"
        Case 6
            ErrorString = "Console Acces"
        Case 7
            ErrorString = "Console Type"
        Case 8
            ErrorString = "B3D Type"
        Case 9
            ErrorString = "Console Monoposte"
        Case 10
            ErrorString = "ServeurParam"
    End Select
    
    '-> Message d'erreur
    MsgBox "Loading failed" & Chr(13) & ErrorString, vbCritical + vbOKOnly, "Fatal Error"
    End
    
End Sub

Public Function IsProcessRunning(Process As Long) As Boolean

Dim hdlProcessMain As Long

'-> Ne rien faire si process � 0
If Process = 0 Then Exit Function

'-> R�cup�rer le handle de la fn�tre
hdlProcessMain = HandleFromProcessID(Process)

'-> Tester si la fen�tre est active
If IsWindow(hdlProcessMain) = 1 Then
    IsProcessRunning = True
End If


End Function

Public Function HandleFromProcessID(lProcessID As Long) As Long

Dim lTempHWND As Long
Dim lTextLength As Long
Dim lTempProcessID As Long
Dim lParent As Long

'-> R�cup�rer le handle de la console
lTempHWND = GetWindow(mdiConsole.hWnd, GW_HWNDFIRST)

'-> Analyse � travers les handles de fen�tre
Do While lTempHWND > 0
    
    GetWindowThreadProcessId lTempHWND, lTempProcessID
    If lTempProcessID = lProcessID Then
        '-> Attention aux fen�tre filles
        If GetParent(lTempHWND) = 0 Then
            'no..
            HandleFromProcessID = lTempHWND
        Else
            'yes
            HandleFromProcessID = GetParent(lTempHWND)
         End If
        Exit Function
    End If

    lTempHWND = GetWindow(lTempHWND, GW_HWNDNEXT)
    
Loop

End Function

Public Sub RunApp(Programme As String, ZoneCharge As String, ExportAgent As Boolean)

'---> Cette proc�dure lance un programme en synchrone ou non

Dim Prog As String
Dim ParamFile As String
Dim TempState As Integer

'-> Tester s'il y a un programme ne cours
If IsProcessRunning(hdlProcess) Then
    MsgBox GetMesseprog("CONSOLE", "82"), vbExclamation + vbOKOnly, GetMesseprog("DOG", "17")
    Exit Sub
End If

'-> V�rifier si on trouve le programme
If UCase$(Entry(1, Programme, "_")) = "B3D" Then
    '-> Run d'un programme de type B3D
    Prog = ApplicationDirectory & "B3D\" & Programme
Else
    '-> Run d'un programme client
    Prog = ApplicationDirectory & IdentSession & "\" & Programme
End If

'-> V�rifier que l'on trouve le programme
If Dir$(Prog) = "" Then
    MsgBox GetMesseprog("APPLIC", "31") & " " & Prog, vbCritical + vbOKOnly, GetMesseprog("APPLIC", "6")
    Exit Sub
End If

'-> Exporter le param�trage de l'agent B3D
If ExportAgent Then
    ParamFile = ExportParamToFile()
    hdlProcess = Shell(Prog & " " & ParamFile & "�" & ZoneCharge, vbNormalFocus)
Else
    hdlProcess = Shell(Prog & " " & ZoneCharge, vbNormalFocus)
End If
        

End Sub


Public Function Get_Free_Agent(Agent_B3D As Boolean) As String

Dim i As Integer

If Agent_B3D Then
    For i = LBound(Matrice_Num_Agent_B3D()) To UBound(Matrice_Num_Agent_B3D())
        If Matrice_Num_Agent_B3D(i) = False Then
            Get_Free_Agent = Format(i, "0000")
            Matrice_Num_Agent_B3D(i) = True
            Exit Function
        End If
    Next
Else
    For i = LBound(Matrice_Num_Agent_IDX()) To UBound(Matrice_Num_Agent_IDX())
        If Matrice_Num_Agent_IDX(i) = False Then
            Get_Free_Agent = Format(i, "0000")
            Matrice_Num_Agent_IDX(i) = True
            Exit Function
        End If
    Next
End If

End Function

Public Function IsIDX(ByVal aBase As clsBase, ModeDebug As Boolean) As clsIdx

'-> Retourne le num�ro de l'agent Existant

Dim i As Integer
Dim aIdx As clsIdx
Dim List_Idx As String
Dim List_Idx_Tri As String
Dim strToAdd As String
Dim hdlCom As Long
Dim TempFileNameParam As String
Dim hdlFile As String
Dim res As Long

'-> Rechercher le premier num�ro d'agent libre
For i = 5001 To 9999

    '-> S'arreter sur le premier agent libre
    If Not Matrice_Num_Agent_IDX(i) Then
    
        '-> Indiquer que l'agent est OK
        Matrice_Num_Agent_IDX(i) = True
        
        '-> Charger son handle de communication s'il n'existe pas d�ja
        If Not Matrice_Com_Idx(i) Then
            Load frmCom.objCom(i)
            Matrice_Com_Idx(i) = True
        End If
        
        hdlCom = frmCom.objCom(i).hWnd
        
        '-> Cr�er un nouvel agent IDX
        Set aIdx = New clsIdx
        aIdx.Num_Idx = Format(i, "0000")
        
        '-> ajouter �galement dans la collection des agents IDX de la base
        aBase.Idx.Add aIdx, "IDX|" & Format(i, "0000")
        
        '-> Lancer ou non le mode debug
        If ModeDebug Then
            strToAdd = "1"
        Else
            strToAdd = "0"
        End If
        
        hdlFile = FreeFile
        TempFileNameParam = GetTempFileNameVB("IDX")
        Open TempFileNameParam For Output As #hdlFile
        
        '-> Param�trage g�n�raux
        Print #hdlFile, "[PARAM]"
        Print #hdlFile, "IDIDX=" & Crypt(Format(i, "0000"))
        Print #hdlFile, "UC_ID=" & Crypt(CStr(UcId))
        Print #hdlFile, "HDLCOM=" & Crypt(CStr(hdlCom))
        Print #hdlFile, "DEBUG=" & Crypt("1")
        Print #hdlFile, "LOCK=" & Crypt(VecteurLock)
        Print #hdlFile, "ACCESDIR=" & Crypt(CStr(TempoAcces))
                
        '-> Param�trage de la base
        Print #hdlFile, "[BASE]"
        Print #hdlFile, "NOM=" & aBase.Nom
        Print #hdlFile, "SERVEUR=" & aBase.Serveur
        Print #hdlFile, "TYPE=" & Crypt(aBase.TypeBase)
        Print #hdlFile, "USER=" & Crypt(aBase.User)
        Print #hdlFile, "PWD=" & Crypt(aBase.Pwd)
        Print #hdlFile, "FICHIER=" & Crypt(aBase.Fichier)
        Print #hdlFile, "TEMPO=" & Crypt(CStr(aBase.TempoLock))
            
        '-> Param�trage des fichiers d'�change
        Print #hdlFile, "[PATH]"
        Print #hdlFile, "DEBUG=" & Crypt(MainDirectory & "LogFile\" & Format(i, "0000") & ".dbg")
        Print #hdlFile, "B3DINI=" & Crypt(ApplicationDirectory & "B3D\Param\B3D.ini")
        Print #hdlFile, "IDENTINI=" & Crypt(ApplicationDirectory & "B3D\Param\Idents.ini")
        Print #hdlFile, "APPLICATIONPATH=" & Crypt(ApplicationDirectory)
        
        '-> Fermer le fichier
        Close #hdlFile
        
        '-> Lancer un nouvel agent IDX
        res = Shell(ApplicationDirectory & "B3D\B3D_IDX.EXE " & TempFileNameParam, vbHide)
        aIdx.hdlProcess = res
        
        '-> Renvoyer un pointeur vers l'agent IDX
        Set IsIDX = aIdx
        
        '-> Quitter la fonction
        Exit Function
        
    End If
Next
            
End Function

Public Sub UnloadIDX(aIdx As clsIdx, aBase As clsBase)

'---> Cette proc�dure est charg�e de supprimer un agent IDX
Dim res As Long

On Error GoTo GestError

'-> Envoyer un ordre de fin
res = SetWindowText(aIdx.Hdl_Retour_Com, "END_SESSION")
res = SendMessage(aIdx.Hdl_Retour_Com, WM_PAINT, 0, 0)

'-> Se mettre en attente de la r�ponse
Do While Not aIdx.EndIdx
    '-> Rendre la main � la cpu
    DoEvents
Loop

'-> Passer la matrice � false
Matrice_Num_Agent_IDX(CInt(aIdx.Num_Idx)) = False
'-> Supprimer l'objet de la collection des agents affect�s � la base
aBase.Idx.Remove "IDX|" & CInt(aIdx.Num_Idx)


GestError:

End Sub

Public Sub UnloadAgentB3D(aCon As clsConnectConsole)

'---> Cette proc�dure supprime un agent B3D du r�f�rencement

Dim aBase As clsBase
Dim aIdent As clsIdent
Dim aIdx As clsIdx

'-> Pointer sur l'ident pour faire - 1 sur le nombre d'agent Connect�
Set aIdent = Idents("IDENT|" & UCase$(Trim(aCon.Ident)))
aIdent.NbConnect = aIdent.NbConnect - 1

'-> Pointer sur la base associ�e
Set aBase = Bases("BASE|" & UCase$(Trim(aIdent.Base)))
aBase.NbConnect = aBase.NbConnect - 1

'-> Pointer sur l'agent IDX
Set aIdx = aBase.Idx("IDX|" & aCon.Num_Idx)
aIdx.NbAgent = aIdx.NbAgent - 1

'-> Passer la matrice � false
Matrice_Num_Agent_B3D(CInt(aCon.IndexAgent)) = False

'-> Supprimer de la collection
Connexions.Remove ("CON|" & aCon.IndexAgent)

End Sub

Public Function ChechInvalidChar(StrToAnalyse As String) As Boolean

'---> V�rifier la pr�sence des caract�res ~ ' " | � �

Dim ErrorC As Boolean

If InStr(1, StrToAnalyse, "~") <> 0 Then ErrorC = True
If InStr(1, StrToAnalyse, "'") <> 0 Then ErrorC = True
If InStr(1, StrToAnalyse, """") <> 0 Then ErrorC = True
If InStr(1, StrToAnalyse, "|") <> 0 Then ErrorC = True
If InStr(1, StrToAnalyse, "�") <> 0 Then ErrorC = True
If InStr(1, StrToAnalyse, "�") <> 0 Then ErrorC = True

ChechInvalidChar = ErrorC

End Function
