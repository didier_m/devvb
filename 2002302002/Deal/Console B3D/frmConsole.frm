VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.MDIForm mdiConsole 
   BackColor       =   &H8000000C&
   Caption         =   "Deal Informatique"
   ClientHeight    =   7125
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9465
   Icon            =   "frmConsole.frx":0000
   LinkTopic       =   "MDIForm1"
   Picture         =   "frmConsole.frx":0CCA
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   4
      Top             =   6750
      Width           =   9465
      _ExtentX        =   16695
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13626
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            TextSave        =   "18/04/2002"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picNaviga 
      Align           =   3  'Align Left
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   6750
      Left            =   0
      ScaleHeight     =   450
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   272
      TabIndex        =   1
      Top             =   0
      Width           =   4080
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   720
         Top             =   3480
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   6
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":9548
               Key             =   "APP"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":98E2
               Key             =   "CLOSE"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":9C7C
               Key             =   "OPEN"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":A016
               Key             =   "OPENFOLDER"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":A3B0
               Key             =   "CLOSEFOLDER"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":A74A
               Key             =   "SHORTCUT"
            EndProperty
         EndProperty
      End
      Begin VB.PictureBox picTitre 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         Height          =   90
         Left            =   360
         ScaleHeight     =   6
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   128
         TabIndex        =   3
         Top             =   240
         Width           =   1920
      End
      Begin MSComctlLib.TreeView TreeNaviga 
         Height          =   2295
         Left            =   120
         TabIndex        =   2
         Top             =   840
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   4048
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin VB.Image imgClose 
         Height          =   165
         Left            =   2040
         Picture         =   "frmConsole.frx":A8A4
         Top             =   120
         Width           =   165
      End
   End
   Begin VB.PictureBox picSplit 
      Align           =   3  'Align Left
      BorderStyle     =   0  'None
      Height          =   6750
      Left            =   4080
      MousePointer    =   9  'Size W E
      ScaleHeight     =   6750
      ScaleWidth      =   105
      TabIndex        =   0
      Top             =   0
      Width           =   105
   End
End
Attribute VB_Name = "mdiConsole"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)

'-> selon le mode de console

Dim aIdent As clsIdent

'-> V�rifier qu'un programme ne soit pas actif
If IsProcessRunning(hdlProcess) Then
    MsgBox GetMesseprog("CONSOLE", "82"), vbExclamation + vbOKOnly, GetMesseprog("DOG", "17")
    Cancel = 1
    Exit Sub
End If

Select Case UCase$(Trim(ConsoleMode))
    Case "SERVEUR"
        '-> V�rif des connexions
        If Not CheckUnloadOperat Then
            MsgBox GetMesseprog("APPLIC", "39"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
            Cancel = 1
            Exit Sub
        End If
        '-> V�rif des op�rateurss
        If Not CheckUnloadIdx Then
            MsgBox GetMesseprog("APPLIC", "40"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
            Cancel = 1
            Exit Sub
        End If
        '-> Tout est ferm� : on quitte
        End
    Case "CLIENT"
        '-> Fermer l'agent affect� � la console
        UnloadAgentConsole
    Case "MONOPOSTE"
        '-> Fermer l'agent affect� � la console
        
        
End Select


End Sub

Private Function CheckUnloadOperat() As Boolean

'---> Cette proc�dure qu'il n'y a plus d'op�rateurs de connect�

Dim aBase As clsBase

'-> V�rifier que tous les bases soient arr�t�s
For Each aBase In Bases
    '-> Tester qu'il n'y ait plus de connexion sur cette base
    If aBase.NbConnect <> 0 Then Exit Function
Next

'-> Il n'y a plus d'op�rateur de connect�
CheckUnloadOperat = True

End Function

Private Function CheckUnloadIdx() As Boolean

'---> Cette proc�dure v�rifie que toutes les bases soient stopp�es

Dim aBase As clsBase

'-> V�rifier que tous les bases soient arr�t�s
For Each aBase In Bases
    '-> Tester qu'il n'y ait plus d'idx
    If aBase.IsRunning Then Exit Function
Next

'-> Il n'y a plus d'op�rateur de connect�
CheckUnloadIdx = True

End Function



Private Sub MDIForm_Unload(Cancel As Integer)

End

End Sub

Private Sub picNaviga_Resize()

Dim aRect As RECT
Dim res As Long

On Error Resume Next

'-> R�cup�rer la taille de la zone cliente
res = GetClientRect(Me.picNaviga.hWnd, aRect)

'-> Positionner l'entete de la fen�tre
Me.picTitre.Left = 3
Me.picTitre.Top = 7
Me.picTitre.Width = aRect.Right - 6 - imgClose.Width
Me.imgClose.Left = aRect.Right - imgClose.Width
Me.imgClose.Top = Me.picTitre.Top - 2

'-> Positionner le treeview
Me.TreeNaviga.Left = Me.picTitre.Left
Me.TreeNaviga.Top = Me.picTitre.Top * 2 + Me.picTitre.Height
Me.TreeNaviga.Width = Me.picTitre.Width + Me.imgClose.Width + 6
Me.TreeNaviga.Height = Me.picNaviga.ScaleY(Me.picNaviga.Height, 1, 3) - 21
End Sub
Private Sub picTitre_Resize()

'---> Dessiner la barre de titre
Me.picTitre.Line (0, 0)-(Me.picTitre.Width - 1, 0), RGB(255, 255, 255)
Me.picTitre.Line (0, 1)-(Me.picTitre.Width - 1, 1), RGB(255, 255, 255)
Me.picTitre.Line (1, 1)-(Me.picTitre.Width - 1, 1), &HE0E0E0
Me.picTitre.Line (0, 3)-(Me.picTitre.Width - 1, 3), RGB(255, 255, 255)
Me.picTitre.Line (0, 4)-(Me.picTitre.Width - 1, 4), RGB(255, 255, 255)
Me.picTitre.Line (1, 4)-(Me.picTitre.Width - 1, 4), &HE0E0E0

End Sub
Private Sub picSplit_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)

Dim aPt As POINTAPI
Dim res As Long

If Button = vbLeftButton Then
    '-> R�cup�rer la position du curseur
    res = GetCursorPos(aPt)
    '-> Convertir en coordonn�es clientes
    res = ScreenToClient(Me.hWnd, aPt)
    '-> Tester les positions mini et maxi
    If aPt.x > Me.picNaviga.ScaleX(2, 7, 3) And aPt.x < Me.picNaviga.ScaleX(Me.Width, 1, 3) - Me.picNaviga.ScaleX(2, 7, 3) Then Me.picNaviga.Width = Me.picNaviga.ScaleX(aPt.x, 3, 1)
End If

End Sub


Private Sub TreeNaviga_DblClick()

'---> Cette proc�dure ex�cute un programme

'-> Tester qu'il y ait des nodes
If Me.TreeNaviga.Nodes.Count = 0 Then Exit Sub

'-> V�rifier qu'il y ait un node de s�lectionn�
If Me.TreeNaviga.SelectedItem Is Nothing Then Exit Sub

'-> Selon le tag du node
Select Case Entry(1, Me.TreeNaviga.SelectedItem.Tag, Chr(0))
    Case "SERVEURIDENT"
        Call DisplayServeurIdent
    Case "SERVEURBASE"
        Call DisplayServeurBase
    Case "PORTAIL"
        Call DisplayServeurCon
    Case "CATALOGUE"
        Call DisplayServeurCat
    Case "SCHEMA"
        Call DisplayServeurSch
    Case "DOGSYS"
        Call DisplayServeurSys
    Case "DOGGRP"
        Call DisplayServeurGrp
    Case "MESSPROG"
    
    Case "AGENTCONSOLE"
        frmAgent.Show vbModal
    Case "PROGB3D"
        RunApp Entry(2, Me.TreeNaviga.SelectedItem.Tag, Chr(0)), Entry(3, Me.TreeNaviga.SelectedItem.Tag, Chr(0)), False
    Case "PROG"
        RunApp Entry(2, Me.TreeNaviga.SelectedItem.Tag, Chr(0)), Entry(3, Me.TreeNaviga.SelectedItem.Tag, Chr(0)), True
End Select

End Sub

Private Sub DisplayServeurGrp()

Dim aFrm As frmDOG

'-> Afficher la feuille si elle est d�ja active
If hdlGrp <> 0 And IsWindow(hdlGrp) = 1 Then
    '-> La feuille existe encore : l'activer
    SendMessage hdlGrp, WM_CHILDACTIVATE, 0, 0
    Exit Sub
End If

'-> Bloquer la feuille
Me.Enabled = False
LockWindowUpdate Me.hWnd

'-> Cr�er une nouvelle instance de la feuille
Set aFrm = New frmDOG

'-> Lancer l'initialisation
aFrm.Init 5

'-> Afficher la feuille
aFrm.Show

'-> Positionner son handle
hdlGrp = aFrm.hWnd

GestError:
    Me.Enabled = True
    LockWindowUpdate 0


End Sub


Private Sub DisplayServeurSys()

Dim aFrm As frmDOG

'-> Afficher la feuille si elle est d�ja active
If hdlSys <> 0 And IsWindow(hdlSys) = 1 Then
    '-> La feuille existe encore : l'activer
    SendMessage hdlSys, WM_CHILDACTIVATE, 0, 0
    Exit Sub
End If

'-> Bloquer la feuille
Me.Enabled = False
LockWindowUpdate Me.hWnd

'-> Cr�er une nouvelle instance de la feuille
Set aFrm = New frmDOG

'-> Lancer l'initialisation
aFrm.Init 3

'-> Afficher la feuille
aFrm.Show

'-> Positionner son handle
hdlSys = aFrm.hWnd

GestError:
    Me.Enabled = True
    LockWindowUpdate 0

End Sub


Private Sub DisplayServeurSch()

Dim aFrm As frmDOG

'-> Afficher la feuille si elle est d�ja active
If hdlSchema <> 0 And IsWindow(hdlSchema) = 1 Then
    '-> La feuille existe encore : l'activer
    SendMessage hdlSchema, WM_CHILDACTIVATE, 0, 0
    Exit Sub
End If

'-> Bloquer la feuille
Me.Enabled = False
LockWindowUpdate Me.hWnd

'-> Cr�er une nouvelle instance de la feuille
Set aFrm = New frmDOG

'-> Lancer l'initialisation
aFrm.Init 1

'-> Afficher la feuille
aFrm.Show

'-> Positionner son handle
hdlSchema = aFrm.hWnd

GestError:
    Me.Enabled = True
    LockWindowUpdate 0

End Sub

Private Sub DisplayServeurCat()

Dim aFrm As frmDOG

'-> Afficher la feuille si elle est d�ja active
If hdlCatalogue <> 0 And IsWindow(hdlCatalogue) = 1 Then
    '-> La feuille existe encore : l'activer
    SendMessage hdlCatalogue, WM_CHILDACTIVATE, 0, 0
    Exit Sub
End If

'-> Bloquer la feuille
Me.Enabled = False
LockWindowUpdate Me.hWnd

'-> Cr�er une nouvelle instance de la feuille
Set aFrm = New frmDOG

'-> Lancer l'initialisation
aFrm.Init 2

'-> Afficher la feuille
aFrm.Show

'-> Positionner son handle
hdlCatalogue = aFrm.hWnd

GestError:
    Me.Enabled = True
    LockWindowUpdate 0

End Sub


Private Sub DisplayServeurCon()

'-> Afficher la feuille si elle est d�ja active
If hdlServBases <> 0 And IsWindow(hdlServCon) = 1 Then
    '-> La feuille existe encore : l'activer
    SendMessage hdlServCon, WM_CHILDACTIVATE, 0, 0
    Exit Sub
End If

'-> La feuille n'existe pas : la charger
LockWindowUpdate Me.hWnd
Load frmGestConnect
frmGestConnect.Init
LockWindowUpdate 0
frmGestConnect.Show

'-> Positionner le handle
hdlServCon = frmGestConnect.hWnd


End Sub

Private Sub DisplayServeurBase()

'---> Cette proc�dure affiche le getsionnaire de bases

If hdlServBases <> 0 And IsWindow(hdlServBases) = 1 Then
    '-> La feuille existe encore : l'activer
    SendMessage hdlServBases, WM_CHILDACTIVATE, 0, 0
    Exit Sub
End If

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> La feuille n'existe pas : la charger
Load frmGestBase

'-> Initialiser
frmGestBase.Init

'-> D�bloquer l'�cran
Me.Enabled = True
Screen.MousePointer = 0

'-> Afficher la feuille
frmGestBase.Show

'-> Positionner le handle
hdlServBases = frmGestBase.hWnd

End Sub

Private Sub DisplayServeurIdent()

'---> Cette proc�dure affiche le getsionnaire de clients

If hdlServIdents <> 0 And IsWindow(hdlServIdents) = 1 Then
    '-> La feuille existe encore : l'activer
    SendMessage hdlServIdents, WM_CHILDACTIVATE, 0, 0
    Exit Sub
End If

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> La feuille n'existe pas : la charger
Load frmGestIdent

'-> Initialiser
frmGestIdent.Init

'-> D�bloquer l'�cran
Me.Enabled = True
Screen.MousePointer = 0

'-> Afficher la feuille
frmGestIdent.Show

'-> Positionner le handle
hdlServIdents = frmGestIdent.hWnd

End Sub
