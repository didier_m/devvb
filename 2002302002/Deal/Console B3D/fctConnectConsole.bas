Attribute VB_Name = "fctConnectConsole"
Option Explicit

'-> Variables pour connexion de l'agent B3D
Public SessionDmdFile As String '-> Fichier de demande pour cet agent
Public SessionRspFile As String '-> Fichier de r�ponse pour cet agent
Public SessionCptFile As String '-> Fichier de compteur pour cet agent

'-> Pour lecture et �change
Public Reponses As Collection

Public pIdent As String
Public pOperat As String
Public pPwd As String
Public pIdAgent As String  '-> AgentB3D affect�
Public pUc_Id As String '-> Num�ro d'UC
Public pIdSession As String '-> Indentificateur de session
Public pCodeLangue As Integer '-> code langue de l'op�rateur
Public pIdx As String '-> Agent IDx affect�

'-> Indique les propri�t�s d'�change de l'agent connect� � la console
Public IsCrypt As Boolean '-> Indique si on est en crypt
Public IsTrace As Boolean  '-> Indique si on est en mode trace
Public IsDebug As Boolean '-> Indique si on est en mode debug

'-> Variables de debug pouyr l'agent B3D
Public DebugAgentDmdFile As String
Public DebugAgentRspFile As String
Public DebugAgentCptFile As String
Public DebugAgentDebugFile As String
Public DebugAgentTraceFile As String

'-> Pour interrompre demande
Public StopDemande As Boolean

Public Function RefreshDog()

'---> Cette prco�dure met � jour le DOG de l'agent B3D

'-> Envoyer la demande
SendDemande "B3D_LOAD_DOG~"

'-> Tester le retour
If Entry(2, Reponses(1), "~") <> "OK" Then
    '-> Afficher l'erreur
    MsgBox "Error : " & Chr(13) & Entry(3, Reponses(1), "~") & " -> " & Entry(4, Reponses(1), "~"), vbCritical + vbOKOnly, "Error"
Else
    MsgBox "Dog Updated", vbInformation + vbOKOnly, "Dog"
End If


End Function

Public Function SetTrace() As Boolean

'---> Cette proc�dure envoie l'ordre de debug

'-> Envoyer la demande
SendDemande "B3D_TRACE~" & CStr(Abs(Not IsTrace))

'-> Tester la r�ponse
If Entry(2, Reponses(1), "~") = "OK" Then IsTrace = Not IsTrace


End Function

Public Function SetDebug() As Boolean

'---> Cette proc�dure envoie l'ordre de debug

'-> Envoyer la demande
SendDemande "B3D_DEBUG~" & CStr(Abs(Not IsDebug))

'-> Tester la r�ponse
If Entry(2, Reponses(1), "~") = "OK" Then IsDebug = Not IsDebug

End Function

Public Function SetCrypt() As Boolean

'---> Cette proc�dure crypt ou decrypt un agent B3D

'-> Envoyer la demande
SendDemande "B3D_CRYPT~" & CStr(Abs(Not IsCrypt))

'-> Tester la r�ponse
If Entry(2, Reponses(1), "~") = "OK" Then IsCrypt = Not IsCrypt


End Function


Private Sub Seek_Data(Field As String, Value As String)

'---> Cette proc�dure fait un Seek Data dans la base






End Sub

Private Function Wait_File(ByVal FileToWait As String, ByVal Tempo As Long, NbTempo As Integer) As Boolean

'---> Cette fonction de met en attente d'un fichier ASCII

'Tempo -> Wait en mili second entre 2 appels
'NbTempo -> Nombre d'appels

Dim i As Double
Dim Wait_Infini As Boolean

If NbTempo = 0 Then Wait_Infini = True
Do
    '-> V�rifier si on n' pas interrompu la demande
    If StopDemande Then
        '-> Inverser l'ordre d'arret
        StopDemande = False
        Exit Function
    End If

    '-> Rendre la main � la cpu
    DoEvents
    '-> Analyse du fichier
    If Dir$(FileToWait, vbNormal) <> "" Then
        Wait_File = True
        Exit Function
    Else
        i = i + 1
        If Not Wait_Infini Then
            If i = NbTempo Then
                Wait_File = False
                Exit Function
            End If
        End If
        Sleep (Tempo)
        DoEvents
    End If
Loop

End Function

Public Function ConnectAgentConsole(Operat As String, Pwd As String, strIdent As String, pDebug As String, pTrace As String, pCrypt As String) As Boolean

'---> Cette proc�dure lance u agent de connexion vers la console serveur

Dim hdlFile As Integer
Dim strConnect As String
Dim CnrFile As String
Dim CnxFile As String
Dim CnxTemp As String
Dim Temp As Long
Dim Ligne As String


'-> Get d'un handle de session
Temp = GetTickCount

'-> Cr�er la chaine de connexion
strConnect = Operat & "�" & Pwd & "�" & strIdent & "�" & pDebug & "�" & pTrace & "�" & pCrypt & "�" & Temp

'-> Composer les noms de fichier
CnxFile = MainDirectory & "Input\" & Temp & ".cnx"
CnxTemp = CnxFile & ".tmp"
CnrFile = MainDirectory & "Input\" & Temp & ".cnr"

'-> Ouvrir un fichier temporaire
hdlFile = FreeFile
Open CnxTemp For Output As #hdlFile

'-> Imposer l'instruction
Print #hdlFile, strConnect

'-> Fermer le fichier temporaire
Close #hdlFile

'-> Renommer le fichier et se mettre en attente de la r�ponse
Name CnxTemp As CnxFile
DoEvents

'-> Attendre la r�ponse 1 minute
Wait_File CnrFile, 1000, 10

'-> si on n'pas trouv� le fichier renvoyer une valeur d'erreur
If Dir$(CnrFile, vbNormal) = "" Then
    '-> V�rifier si le fichier CNR existe bien
    If Dir$(CnxFile, vbNormal) <> "" Then Kill CnxFile
    '-> Positionner la variable d'erreur
    MsgBox GetMesseprog("APPLIC", "38"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    '-> Quitter la fonction
    Exit Function
End If

'-> Lecture de la r�ponse
hdlFile = FreeFile
Open CnrFile For Input As #hdlFile
Line Input #hdlFile, Ligne

'-> Fermer le fichier et le supprimer
Close #hdlFile
Kill CnrFile

'-> Analyse de la ligne de retour
Ligne = DeCrypt(Ligne)
If Trim(Ligne) = "" Then Exit Function

'-> Analyse de la ligne
If Entry(2, Ligne, "�") = "ERROR" Then
    '-> Message d'erreur
    MsgBox GetMesseprog("APPLIC", "38") & Chr(13) & Entry(3, Ligne, "�"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    '-> Quitter la fonction
    Exit Function
End If

'-> Mode de l'agent
IsCrypt = CBool(pCrypt)
IsTrace = CBool(pTrace)
IsDebug = CBool(pDebug)

'-> Variables de connexion
pIdent = strIdent
pOperat = Operat
pPwd = Pwd

'-> Get des param�tres en retour
pIdAgent = Entry(3, Ligne, "�")
pUc_Id = Entry(4, Ligne, "�")
pIdSession = Entry(5, Ligne, "�")
pCodeLangue = CInt(Entry(6, Ligne, "�"))
pIdx = Entry(7, Ligne, "�")

'-> Init des fichiers d'�change
SessionDmdFile = MainDirectory & "Output\" & pIdSession & ".dmd"
SessionRspFile = MainDirectory & "Input\" & pIdSession & ".rsp"
SessionCptFile = MainDirectory & "Logfile\" & pIdSession & ".cpt"

'-> Renvoyer une valeyr de succ�s
ConnectAgentConsole = True


End Function


Public Function ExportParamToFile() As String

'---> Cette proc�dure enregistre le pram�trage de la connexion en cours dans un fichier ASCII _
pour qu'il puisse �tre r�cup�r� depuis r�cup�r� par un autre programme


Dim hdlFile As Integer
Dim TempFileName As String
    
'-> Ouvrir un fichier temporaire
hdlFile = FreeFile
TempFileName = GetTempFileNameVB("DMD")
Open TempFileName For Output As #hdlFile

'-> Entete de la section
Print #hdlFile, "[PARAM]"

'-> Ident
Print #hdlFile, "IDENT=" & Crypt(pIdent)
'-> Operateur
Print #hdlFile, "OPERAT=" & Crypt(pOperat)
'-> Mot de passe
Print #hdlFile, "PWD=" & Crypt(pPwd)
'-> Code langue
Print #hdlFile, "LANGUE=" & Crypt(CStr(pCodeLangue))
'-> Fichier demande
Print #hdlFile, "DMD=" & Crypt(SessionDmdFile)
'-> Fichier R�ponse
Print #hdlFile, "RSP=" & Crypt(SessionRspFile)
'-> Index de l'agent
Print #hdlFile, "IDAGENT=" & Crypt(pIdAgent)
'-> Uc_ID
Print #hdlFile, "UC_ID=" & Crypt(pUc_Id)
'-> Index de session
Print #hdlFile, "IDSESSION=" & Crypt(pIdSession)
'-> Emplacement des applications
Print #hdlFile, "APPLICATIONDIR=" & Crypt(ApplicationDirectory)
'-> Emplacement des rep B3D
Print #hdlFile, "MAINDIR=" & Crypt(MainDirectory)
'-> Fichier Messprog
Print #hdlFile, "MESSPROGFILE=" & Crypt(MessprogIniFile)

'-> Fermer le fichier temporaire
Close #hdlFile

'-> Renvoyer une valeur de succ�s
ExportParamToFile = TempFileName
    

End Function

Public Function SendDemande(Demande As String, Optional IsDbg As Boolean) As Boolean

'---> Cette proc�dure lance une demande

Dim hdlFile As Integer
Dim Temp As String
Dim Ligne As String

On Error GoTo GestError

'-> Initialiser la collection des r�ponses
Set Reponses = New Collection

'-> Ouvrir le bon fihcier temporaire
If IsDbg Then
    Temp = DebugAgentDmdFile & ".tmp"
Else
    Temp = SessionDmdFile & ".tmp"
End If


hdlFile = FreeFile
Open Temp For Output As #hdlFile

'-> Poser l'instruction
Print #hdlFile, Demande

'-> Envoyer le fichier
Close #hdlFile

'-> Renommer pour le bon objet
If IsDbg Then
    If Dir$(DebugAgentDmdFile) <> "" Then Kill DebugAgentDmdFile
    Name Temp As DebugAgentDmdFile
Else
    If Dir$(SessionDmdFile) <> "" Then Kill SessionDmdFile
    Name Temp As SessionDmdFile
End If

'-> Se mettre en attente de la r�ponse
If IsDbg Then
    Wait_File DebugAgentRspFile, 10, 0
Else
    Wait_File SessionRspFile, 10, 0
End If

'-> Lire la r�ponse
hdlFile = FreeFile
If IsDbg Then
    Open DebugAgentRspFile For Input As #hdlFile
Else
    Open SessionRspFile For Input As #hdlFile
End If
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> Ajouter dans la collection
    Reponses.Add DeCrypt(Ligne)
Loop

Close #hdlFile

'-> Supprimer le fichier de r�ponse
If IsDbg Then
    Kill DebugAgentRspFile
Else
    Kill SessionRspFile
End If

'-> Renvoyer une r�ponses
SendDemande = True

GestError:


End Function

Public Function CreateDebugAgentFile() As String

Dim hdlFile As Integer
Dim TempFileNameParam As String
Dim aIdent As clsIdent
Dim aBase As clsBase
Dim AgentIdSession As String

'-> Cr�er un fichier de param�trage pour exporter les param�tres
hdlFile = FreeFile
TempFileNameParam = GetTempFileNameVB("B3D")
Open TempFileNameParam For Output As #hdlFile

'-> Pointer sur l'ident sp�cifi�
Set aIdent = Idents("IDENT|" & pIdent)

'-> Pointer sur la base affect�
Set aBase = Bases("BASE|" & aIdent.Base)

'-> R�cup�rer un IdSession
AgentIdSession = GetTickCount()

'-> Param�trage g�n�raux
Print #hdlFile, "[PARAM]"
Print #hdlFile, "IDSESSION=" & AgentIdSession
Print #hdlFile, "IDAGENT=" & pIdAgent
Print #hdlFile, "IDIDX=" & pIdx
Print #hdlFile, "OPERAT=" & pOperat
Print #hdlFile, "CODELANGUE=  " & CStr(pCodeLangue)
Print #hdlFile, "UC_ID=" & CStr(pUc_Id)
Print #hdlFile, "HDLCOM=0"
Print #hdlFile, "DEBUG=0"
Print #hdlFile, "TRACE=0"
Print #hdlFile, "CRYPT=0"
Print #hdlFile, "REPLICAUPDATE=" & GetIniFileValue("B3D", "ReplicationUpdate", ApplicationDirectory & aIdent.Code & "\Param\" & aIdent.Code & ".ini")
Print #hdlFile, "LOCK=" & VecteurLock
Print #hdlFile, "ACCESDIR=" & CStr(TempoAcces)

'-> Param�trage de l'ident
Print #hdlFile, "[IDENT]"
Print #hdlFile, "CODE=" & aIdent.Code
Print #hdlFile, "SCH=" & aIdent.FichierSchema
Print #hdlFile, "CAT=" & aIdent.FichierCatalogue
Print #hdlFile, "LNK=" & aIdent.FichierLink

'-> Param�trage de la base
Print #hdlFile, "[BASE]"
Print #hdlFile, "NOM=" & aBase.Nom
Print #hdlFile, "SERVEUR=" & aBase.Serveur
Print #hdlFile, "TYPE=" & aBase.TypeBase
Print #hdlFile, "USER=" & aBase.User
Print #hdlFile, "PWD=" & aBase.Pwd
Print #hdlFile, "FICHIER=" & aBase.Fichier
Print #hdlFile, "TEMPO=" & CStr(aBase.TempoLock)

'-> Param�trage des fichiers d'�change
Print #hdlFile, "[PATH]"
Print #hdlFile, "DMD=" & MainDirectory & "Output\" & AgentIdSession & ".dmd"
Print #hdlFile, "RSP=" & MainDirectory & "Input\" & AgentIdSession & ".rsp"
Print #hdlFile, "DEBUG=" & MainDirectory & "Logfile\" & AgentIdSession & ".dbg"
Print #hdlFile, "TRACE=" & MainDirectory & "Logfile\" & AgentIdSession & ".trc"
Print #hdlFile, "INT=" & MainDirectory & "Interface\"
Print #hdlFile, "INTSOV=" & MainDirectory & "Interface\Interface.Sov"
Print #hdlFile, "REPLICAIN=" & MainDirectory & "Replica\Input\"
Print #hdlFile, "REPLICAOUT=" & MainDirectory & "Replica\Output\"
Print #hdlFile, "B3DINI=" & ApplicationDirectory & "B3D\Param\B3D.ini"
Print #hdlFile, "RSPPATH=" & MainDirectory & "Input\"
Print #hdlFile, "MESSPROGINIFILE=" & MessprogIniFile
Print #hdlFile, "APPLICATIONPATHs=" & ApplicationDirectory
Print #hdlFile, "CPT=" & Crypt(MainDirectory & "Logfile\" & AgentIdSession & ".cpt")

'-> Positionner les variables de debug
DebugAgentDmdFile = MainDirectory & "Output\" & AgentIdSession & ".dmd"
DebugAgentRspFile = MainDirectory & "Input\" & AgentIdSession & ".rsp"
DebugAgentCptFile = MainDirectory & "Logfile\" & AgentIdSession & ".Cpt"
DebugAgentTraceFile = MainDirectory & "Logfile\" & AgentIdSession & ".trc"
DebugAgentDebugFile = MainDirectory & "Logfile\" & AgentIdSession & ".dbg"

'-> Fermer le fichier
Close #hdlFile

'-> Retourner le nom du fichier
CreateDebugAgentFile = TempFileNameParam

End Function

Public Function CreateDebugIdxFile() As String

Dim hdlFile As Integer
Dim TempFileNameParam As String
Dim aIdent As clsIdent
Dim aBase As clsBase
Dim aIdx As clsIdx

'-> Pointer sur l'ident sp�cifi�
Set aIdent = Idents("IDENT|" & pIdent)

'-> Pointer sur la base affect�
Set aBase = Bases("BASE|" & aIdent.Base)


'-> Cr�er un fichier de param�trage pour exporter les param�tres
hdlFile = FreeFile
TempFileNameParam = GetTempFileNameVB("B3D")
Open TempFileNameParam For Output As #hdlFile

'-> Param�trage g�n�raux
Print #hdlFile, "[PARAM]"
Print #hdlFile, "IDIDX=" & pIdx
Print #hdlFile, "UC_ID=" & pUc_Id
Print #hdlFile, "HDLCOM=" & 0
Print #hdlFile, "DEBUG=" & "1"
Print #hdlFile, "LOCK=" & CStr(VecteurLock)
Print #hdlFile, "ACCESDIR=" & CStr(TempoAcces)
        
'-> Param�trage de la base
Print #hdlFile, "[BASE]"
Print #hdlFile, "NOM=" & aBase.Nom
Print #hdlFile, "SERVEUR=" & aBase.Serveur
Print #hdlFile, "TYPE=" & aBase.TypeBase
Print #hdlFile, "USER=" & aBase.User
Print #hdlFile, "PWD=" & aBase.Pwd
Print #hdlFile, "FICHIER=" & aBase.Fichier
Print #hdlFile, "TEMPO=" & CStr(aBase.TempoLock)
    
'-> Param�trage des fichiers d'�change
Print #hdlFile, "[PATH]"
Print #hdlFile, "DEBUG=" & MainDirectory & "LogFile\" & pIdx & ".dbg"
Print #hdlFile, "B3DINI=" & ApplicationDirectory & "B3D\Param\B3D.ini"
Print #hdlFile, "IDENTINI=" & ApplicationDirectory & "B3D\Param\Idents.ini"
Print #hdlFile, "APPLICATIONPATH=" & ApplicationDirectory
    
'-> Fermer le fichier
Close #hdlFile

'-> Retourner le nom du fichier
CreateDebugIdxFile = TempFileNameParam

End Function
