VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmDOG 
   Caption         =   "Form1"
   ClientHeight    =   6450
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7485
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   430
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   499
   Begin MSComctlLib.ListView ListView1 
      Height          =   3255
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   5741
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Menu mnuPOPUP 
      Caption         =   "POPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuAdd 
         Caption         =   ""
      End
      Begin VB.Menu mnuEdit 
         Caption         =   ""
      End
      Begin VB.Menu mnuPrint 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDel 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmDOG"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Indique le mode dans lequel on est
Dim pMode As Integer

Private Sub Form_Load()

'---> Charger les messprogs des menus

Me.mnuAdd.Caption = GetMesseprog("MENU", "1")
Me.mnuDel.Caption = GetMesseprog("MENU", "2")
Me.mnuEdit.Caption = GetMesseprog("MENU", "3")
Me.mnuPrint.Caption = GetMesseprog("MENU", "7")

End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next
GetClientRect Me.hWnd, aRect
Me.ListView1.Left = 0
Me.ListView1.Top = 0
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom

End Sub

Public Sub Init(IdMode As Integer)

'---> Cette proc�dure affiche soit :
' 1 -> Sch�ma
' 2 -> Catalogue
' 3 -> Table Sys
' 4 -> Messprog
' 5 -> Groupes

Select Case IdMode
    Case 1 'Sch�ma
        LoadSchemaFrm
    Case 2 'Catalogue
        LoadCatalogueFrm
    Case 3 'Param Sys
        LoadSysFrm
    Case 4 'Messprog
    
    Case 5 'Groupes
        LoadGroupeFrm
End Select

pMode = IdMode

End Sub

Private Sub AddGrp()

'---> Cette proc�dure ajoute un groupe

Dim Rep As String
Dim aIdent As clsIdent
Dim aSys As clsSystemParam
Dim x As ListItem

'-> Demander le nouveua code
Rep = InputBox(GetMesseprog("APPLIC", "1"), GetMesseprog("MENU", "1"), "")
If Trim(Rep) = "" Then Exit Sub

'-> V�rifier si le nouveau code saisi n'existe pas d�ja
For Each x In Me.ListView1.ListItems
    If UCase$(Trim(x.Text)) = Trim(UCase$(Rep)) Then
        MsgBox GetMesseprog("APPLIC", "2"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
        Exit Sub
    End If
Next

'-> Ajouter dans le listview
Set x = Me.ListView1.ListItems.Add()
x.Text = Trim(Rep)
x.Selected = True

'-> Formatter le listview
FormatListView Me.ListView1

'-> Enregistrer la liste des groupes
SaveGroupe

End Sub

Private Sub SaveGroupe()

'---> Cette proc�dure enregistre la liste des groupes

Dim aIdent As clsIdent
Dim x As ListItem

'-> Pointer sur l'ident
Set aIdent = Idents("IDENT|" & pIdent)

'-> R�cup�rer le contenu de la section
WritePrivateProfileSection "GROUPE", vbNullString, aIdent.IniFile

'-> Enregistrer la liste des nouvels idents
For Each x In Me.ListView1.ListItems
    WritePrivateProfileString "GROUPE", CStr(x.Index), Crypt(x.Text), aIdent.IniFile
Next

End Sub

Private Sub DelGroupe()

'---> Supprimer un groupe
Dim aIdent As clsIdent
Dim Rep As VbMsgBoxResult

'-> Demander confirmation
Rep = MsgBox(Replace(GetMesseprog("APPLIC", "44"), "#OBJ#", Me.ListView1.SelectedItem.Text), vbQuestion + vbYesNo, GetMesseprog("APPLIC", "21"))
If Rep = vbNo Then Exit Sub

'-> Pointer sur l'ident en cours
Set aIdent = Idents("IDENT|" & pIdent)

'-> Supprimer de l'affichage
Me.ListView1.ListItems.Remove Me.ListView1.SelectedItem.Index

'-> Enregistrer
SaveGroupe

End Sub

Private Sub EditGrp()

Dim Rep As String

'-> Demande de modification
Rep = InputBox(GetMesseprog("APPLIC", "1"), GetMesseprog("MENU", "3"), Me.ListView1.SelectedItem.Text)
If Trim(Rep) = "" Then Exit Sub

'-> Modifier la valeur
Me.ListView1.SelectedItem.Text = Trim(Rep)

'-> Formatter le listview
FormatListView Me.ListView1

'-> Enregistrer
SaveGroupe

End Sub

Private Sub LoadGroupeFrm()


'---> Cette proc�dure charge la liste des groupes

'-> Titre de la feuille
Me.Caption = GetMesseprog("CONSOLE", "96")

'-> Cr�er les colonnes
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "12")

'-> Afficher la liste des groupes existants
DisplayGroupes

End Sub

Private Sub DisplayGroupes()

'---> Cette proc�dure affiche la liste des groupes existants

Dim lpBuffer As String
Dim DefNat As String
Dim i As Integer
Dim Temp As String
Dim aIdent As clsIdent

'-> Pointer sur l'ident
Set aIdent = Idents("IDENT|" & pIdent)

'-> R�cup�rer le contenu de la section
lpBuffer = GetIniFileValue("GROUPE", "", aIdent.IniFile, True)

For i = 1 To NumEntries(lpBuffer, Chr(0))
    '-> R�cup�rer une d�finition
    DefNat = Entry(i, lpBuffer, Chr(0))
    If Trim(DefNat) = "" Then Exit For
    Temp = DeCrypt(Entry(2, DefNat, "="))
    '-> Ajouter dans la liste
    Me.ListView1.ListItems.Add , , DeCrypt(GetIniFileValue("GROUPE", Entry(1, DefNat, "="), aIdent.IniFile))
Next

'-> Formatter
FormatListView Me.ListView1

End Sub

Private Sub LoadSysFrm()

'-> Titre de la feuille
Me.Caption = GetMesseprog("CONSOLE", "4")

'-> Cr�er les colonnes
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "11")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "12")

'-> Afficher le sch�ma
DisplaySystemTable

End Sub

Private Sub DisplaySystemTable()

Dim x As ListItem
Dim i As Integer
Dim DefParam As String

'-> Get de la table system
strRetour = Idents("IDENT|" & IdentSession).ServeurDOG.GetSystemTable

Me.ListView1.ListItems.Clear

'-> Quitter si pas de param�trage
If strRetour = "" Then Exit Sub

'-> Cr�ation des enregs
For i = 1 To NumEntries(strRetour, Chr(0))
    '-> Cr�er un nouvel enreg
    DefParam = Entry(i, strRetour, Chr(0))
    Set x = Me.ListView1.ListItems.Add(, "SYS|" & Entry(1, DefParam, "="), Entry(1, DefParam, "="))
    x.SubItems(1) = Entry(2, DefParam, "=")
Next

'-> Justifier les entetes de colonne
FormatListView Me.ListView1

End Sub

Private Sub LoadSchemaFrm()

'---> Chargement du sch�ma

'-> Titre de la feuille
Me.Caption = GetMesseprog("CONSOLE", "3")

'-> Cr�er les colonnes
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "7")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "2")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "8")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "9")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "10")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "16")

'-> Afficher le sch�ma
DisplaySchema


End Sub

Private Sub DisplaySchema()

Dim aFname As Object
Dim aIdent As clsIdent
Dim ItemX As ListItem
Dim i As Long

'-> Get d'un pointeur vers le serveur DOG
Set aIdent = Idents("IDENT|" & IdentSession)
                                        
'-> Vider le ListView
Me.ListView1.ListItems.Clear

'-> Bloquer la mise � jour
LockWindowUpdate Me.ListView1.hWnd

'-> Afficher la liste des codes dans l'odre alpha
For Each aFname In aIdent.ServeurDOG.fNames
    '-> Rendre la main � la CPU
    DoEvents
    '-> Ajouter le libelle
    Set ItemX = Me.ListView1.ListItems.Add(, "OBJ|" & aFname.Num_Fname)
    '-> Afficher ses propri�t�s
    DisplayFnamePropObj aFname, ItemX
Next

'-> Justifier en automatique les colonnes
FormatListView Me.ListView1

'-> D�bloquer l'�cran
LockWindowUpdate 0


End Sub

Private Sub DisplayFnamePropObj(aFname As Fname, ItemX As ListItem)

'-> Code langue
aFname.CodeLangue = OpeCodeLangue
'-> Code
ItemX.Text = aFname.Num_Fname
'-> Libelle
ItemX.SubItems(1) = aFname.Designation
'-> AccesDirect
If aFname.IndexDirect <> "" Then
    ItemX.SubItems(2) = "Yes"
Else
    ItemX.SubItems(2) = "No"
End If
'-> stat
If Not aFname.IsStat Then
    ItemX.SubItems(3) = "No"
Else
    ItemX.SubItems(3) = "Yes"
End If
'-> Replica
If Not aFname.IsReplica Then
    ItemX.SubItems(4) = "No"
Else
    ItemX.SubItems(4) = "Yes"
End If
'-> Niveau de r�plication
If aFname.IsReplica Then ItemX.SubItems(5) = CStr(aFname.NiveauReplica)

End Sub

Private Sub LoadCatalogueFrm()

'---> Chargement du catalogue

'-> Titre de la feuille
Me.Caption = GetMesseprog("CONSOLE", "2")

'-> Cr�er les colonnes
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "1")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "2")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "3")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "4")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "5")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "6")

'-> Charger le catalogue
DisplayCatalogue


End Sub


Private Sub DisplayCatalogue()

'---> Afficher le catalogue dans l'ordre alphab�tique

Dim aEmilieObj As Object
Dim aIdent As clsIdent
Dim ItemX As ListItem
Dim i As Long

'-> Get d'un pointeur vers le serveur DOG
Set aIdent = Idents("IDENT|" & IdentSession)

'-> Vider le ListView
Me.ListView1.ListItems.Clear
'-> Bloquer la mise � jour
LockWindowUpdate Me.ListView1.hWnd
'-> Afficher la liste des codes dans l'odre alpha
For Each aEmilieObj In aIdent.ServeurDOG.Catalogue
    '-> Rendre la main � la CPU
    DoEvents
    '-> Ajouter le libelle
    Set ItemX = Me.ListView1.ListItems.Add(, "OBJ|" & aEmilieObj.Num_Field)
    'ItemX.SmallIcon = "OBJ"
    DisplayObjProp aEmilieObj, ItemX, aIdent
Next
'-> Justifier les entetes de colonne
FormatListView Me.ListView1

'-> Maj de l'interface
LockWindowUpdate 0

        
End Sub

Private Sub DisplayObjProp(aEmilieObj As Object, ItemX As ListItem, aIdent As clsIdent)

Dim aFname As Object

'-> Code langue
aEmilieObj.CodeLangue = OpeCodeLangue
'-> Tri� de base sur le num_field
ItemX.Text = aEmilieObj.Num_Field
ItemX.SubItems(1) = aEmilieObj.Fill_Label
ItemX.SubItems(2) = aEmilieObj.DataType
ItemX.SubItems(3) = aEmilieObj.Progiciel
If aEmilieObj.MultiLangue <> 0 Then ItemX.SubItems(5) = Format(aEmilieObj.MultiLangue, "00")

If aEmilieObj.Table_Correspondance <> "" Then
    '-> Pointer sur la table associ�e
    Set aFname = aIdent.ServeurDOG.fNames(aIdent.Code & "|" & aEmilieObj.Table_Correspondance)
    aFname.CodeLangue = OpeCodeLangue
    ItemX.SubItems(4) = aEmilieObj.Table_Correspondance & " : " & aFname.Designation
End If

    
End Sub

Private Sub ListView1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

'-> Trier sur la colonne
Me.ListView1.SortKey = ColumnHeader.Index - 1
If Me.ListView1.SortOrder = lvwAscending Then
    Me.ListView1.SortOrder = lvwDescending
Else
    Me.ListView1.SortOrder = lvwAscending
End If
Me.ListView1.Sorted = True

End Sub

Private Sub ListView1_DblClick()

'---> Lancer la modif

'-> V�rifier qu'il y ait un item de s�lectionn�
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Editer selon le mode
Select Case pMode
    Case 1 'Sch�ma
        Call DisplayFnameProp
    Case 2 'Catalogue
        Call DisplayEmilieProp
    Case 3 'Sys
        Call SetSysParam
    Case 4 'Messprog
    
    Case 5 'Gestion des groupes
        Call EditGrp
    
End Select

End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)

If Button <> vbRightButton Then Exit Sub

'---> Afficher le menu contextuel
If Me.ListView1.ListItems.Count = 0 Then
    '-> Pas supprimer
    Me.mnuDel.Enabled = False
    '-> Pas Propri�t�s
    Me.mnuEdit.Enabled = False
    '-> Pas imprimer
    Me.mnuPrint.Enabled = False
Else
    '-> ok supprimer
    Me.mnuDel.Enabled = True
    '-> ok Propri�t�s
    Me.mnuEdit.Enabled = True
    '-> Pas imprimer
    Me.mnuPrint.Enabled = True
End If

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu Me.mnuPOPUP

'-> Traiter le retour
Select Case strRetour
    Case "ADD"
        Select Case pMode
            Case 1 'Sch�ma
                Call AddFname
            Case 2 'Catalogue
                Call AddEmilieObj
            Case 3 'Sys
                Call AddSysParam
            Case 4 'Messprog
            
            Case 5 'Groupes
                Call AddGrp
        End Select
    Case "DEL"
        Select Case pMode
            Case 1 'Sch�ma
                Call DeleteFname
            Case 2 'Catalogue
                Call DeleteEmilieObj
            Case 3 'Sys
                Call DelSysParam
            Case 4 'Messprog
            
            Case 5 'Groupes
                Call DelGroupe
        End Select
    Case "EDIT"
        Select Case pMode
            Case 1 'Sch�ma
                Call DisplayFnameProp
            Case 2 'Catalogue
                Call DisplayEmilieProp
            Case 3 'Sys
                Call SetSysParam
            Case 4 'Messprog
            
            Case 5 'Groupes
                Call EditGrp
            
        End Select
    Case "PRINT"
        '-> Run du programme d'impression
        Call PrintDOG
    
End Select

'-> Vider la variable d'�change
strRetour = ""

End Sub

Private Sub DelSysParam()

'---> Cette proc�dure suprime une valeur syst�me
Dim aSys As clsSystemParam
Dim aIdent As clsIdent
Dim Rep As VbMsgBoxResult

'-> Demander confirmation
Rep = MsgBox(Replace(GetMesseprog("APPLIC", "44"), "#OBJ#", Me.ListView1.SelectedItem.Text), vbQuestion + vbYesNo, GetMesseprog("APPLIC", "21"))
If Rep = vbNo Then Exit Sub

'-> Pointer sur l'ident en cours
Set aIdent = Idents("IDENT|" & IdentSession)

'-> Supprimer du parm�trage sys
aIdent.ServeurDOG.SystemParams.Remove (Me.ListView1.SelectedItem.Key)

'-> Enregistrer
aIdent.ServeurDOG.SaveSystemParam

'-> Supprimer sa valeur
Me.ListView1.ListItems.Remove (Me.ListView1.SelectedItem.Key)

'-> S'il reste des code syst�mes se positionner sur le premier
If Me.ListView1.ListItems.Count <> 0 Then Me.ListView1.ListItems(1).Selected = True

End Sub

Private Sub SetSysParam()

'---> Cette proc�dure donne une valeur � un code syst�me

Dim aSys As clsSystemParam
Dim aIdent As clsIdent

'-> Charger la feuille de saisie
frmSaisieVal.Init GetMesseprog("DOG", "11"), GetMesseprog("CONSOLE", "49"), Me.ListView1.SelectedItem.SubItems(1)
frmSaisieVal.Show vbModal

'-> Tester le retour
If strRetour = "" Then Exit Sub

'-> Pointer sur l'ident en cours
Set aIdent = Idents("IDENT|" & IdentSession)

'-> Pointer sur l'objet syst�me
Set aSys = aIdent.ServeurDOG.SystemParams(Me.ListView1.SelectedItem.Key)

'-> Mettre � jour sa valeur
aSys.Value = Entry(2, strRetour, Chr(0))

'-> Enregistrer
aIdent.ServeurDOG.SaveSystemParam

'-> Mettre � jour en affichage
Me.ListView1.SelectedItem.SubItems(1) = aSys.Value

'-> Formatter le listview
FormatListView Me.ListView1

'-> Vider la variable d'�change
strRetour = ""

End Sub

Private Sub AddSysParam()

'---> Cette proc�dure ajoute un code syst�me � l'ident sp�cifi�

Dim Rep As String
Dim aIdent As clsIdent
Dim aSys As clsSystemParam
Dim x As ListItem

'-> Demander le nouveua code
Rep = InputBox(GetMesseprog("APPLIC", "48"), GetMesseprog("APPLIC", "48"), "")
If Trim(Rep) = "" Then Exit Sub

'-> Pointer sur l'ident en cours
Set aIdent = Idents("IDENT|" & IdentSession)

'-> V�rifier si le nouveau code saisi n'existe pas d�ja
If aIdent.ServeurDOG.IsSysParam(Rep) Then
    MsgBox GetMesseprog("APPLIC", "2"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    Exit Sub
End If

'-> Cr�er une nouvelle instance d'un objet Sys
Set aSys = New clsSystemParam

'-> Setting de ses propri�t�s
aSys.Key = Trim(UCase$(Rep))

'-> Ajouter le nouveau code dans la collections
aIdent.ServeurDOG.SystemParams.Add aSys, "SYS|" & aSys.Key

'-> Enregistrer
aIdent.ServeurDOG.SaveSystemParam

'-> Ajouter dans le listview
Set x = Me.ListView1.ListItems.Add(, "SYS|" & aSys.Key, aSys.Key)
x.Selected = True

'-> Saisir sa valeur
Call SetSysParam

End Sub

Private Sub PrintDOG()

'---> Ce programme imprime le DOG

RunApp "B3D_PRINTDOG.EXE", "", True


End Sub

Private Sub DeleteFname()

Dim Rep As VbMsgBoxResult
Dim aFname As Object
Dim aEmilieObj As Object

'-> V�rifier qu'un objet soit s�lectionn�
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Pointer sur la table sp�cifi�e
Set aFname = Idents("IDENT|" & IdentSession).ServeurDOG.fNames(IdentSession & "|" & Me.ListView1.SelectedItem.Text)

'-> Demander copnfirmation
Rep = MsgBox(Replace(GetMesseprog("APPLIC", "44"), "#OBJ#", Me.ListView1.SelectedItem.Text & " - " & Me.ListView1.SelectedItem.SubItems(1)), vbExclamation + vbYesNo, GetMesseprog("APPLIC", "21"))
If Rep = vbNo Then Exit Sub

'-> V�rifier que la table sp�cifi�e n'est pas une table
For Each aEmilieObj In Idents("IDENT|" & IdentSession).ServeurDOG.Catalogue
    If Trim(aEmilieObj.Table_Correspondance) = aFname.Num_Fname Then
        '-> Message d'erreur
        MsgBox GetMesseprog("APPLIC", "49"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
        Exit Sub
    End If
Next 'Pour tous les objets du catalogue

'-> Supprimer l'objet du schem�
Idents("IDENT|" & IdentSession).ServeurDOG.fNames.Remove (IdentSession & "|" & Me.ListView1.SelectedItem.Text)

'-> Enregistrer le catalogue
Idents("IDENT|" & IdentSession).ServeurDOG.Save_Schema

'-> Supprimer l'objet du listView
Me.ListView1.ListItems.Remove Me.ListView1.SelectedItem.Index

End Sub



Private Sub AddFname()

'---> Cette proc�dure ajoute une table dans le sch�ma

Dim NumFname As String
Dim x As ListItem
Dim aFname As Fname

'-> R�cup�er le num�ro de la table
NumFname = Format(Idents("IDENT|" & IdentSession).ServeurDOG.FnameCount, "0000")

'-> Enregistrer la nouvelle table
Idents("IDENT|" & IdentSession).ServeurDOG.AddFname (NumFname)

'-> Enregistrer le catalogue
Idents("IDENT|" & IdentSession).ServeurDOG.Save_Schema

'-> Ajouter un Enreg dans le listeView
Set x = Me.ListView1.ListItems.Add(, "OBJ|" & NumFname, NumFname)
x.Selected = True

'-> Editer ses propri�t�s
DisplayFnameProp

'-> Pointer sur le Fname sp�cifi�
Set aFname = Idents("IDENT|" & IdentSession).ServeurDOG.fNames(IdentSession & "|" & NumFname)

'-> Code langue
aFname.CodeLangue = OpeCodeLangue

'-> Posiitonner son libell�
x.SubItems(1) = aFname.Designation

End Sub

Private Sub DisplayFnameProp()

Dim aFname As Object

'-> V�rifier qu'un objet soit s�lectionn�
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Charger la feuille en m�moire
Load frmGestSchema

'-> Charger les propri�t�s de cette table
frmGestSchema.Initialisation "IDENT|" & IdentSession, Entry(2, Me.ListView1.SelectedItem.Key, "|")

'-> Titre de la feuille
frmGestSchema.Caption = GetMesseprog("FRMSCHEMA", "1") & " :" & Entry(2, Me.ListView1.SelectedItem.Key, "|")

'-> Afficher la feuille
frmGestSchema.Show vbModal

'-> Pointer sur la table
Set aFname = Idents("IDENT|" & IdentSession).ServeurDOG.fNames(IdentSession & "|" & Entry(2, Me.ListView1.SelectedItem.Key, "|"))

'-> Afficher ses propri�t�s
DisplayFnamePropObj aFname, Me.ListView1.SelectedItem
    
End Sub



Private Sub mnuAdd_Click()
strRetour = "ADD"
End Sub

Private Sub mnuDel_Click()
strRetour = "DEL"
End Sub

Private Sub mnuEdit_Click()
strRetour = "EDIT"
End Sub

Private Sub mnuPrint_Click()
strRetour = "PRINT"
End Sub

Private Sub AddEmilieObj()

'-> Charger la feuille ne m�moire
Load frmEmilieObj
'-> Affecter son pointeur vers la feuille en cours
Set frmEmilieObj.aFrm = Me
'-> Affecter son ident
Set frmEmilieObj.aIdent = Idents("IDENT|" & IdentSession)
'-> Lancer l'initialisation
frmEmilieObj.Init ""
'-> Afficher en modal
frmEmilieObj.Show vbModal

End Sub

Private Sub DisplayEmilieProp()

Dim aIdent As clsIdent
Dim aEmilieObj As Object

'-> Charger la feuille de propri�t�
Load frmEmilieObj

'-> Init de son Ident
Set frmEmilieObj.aIdent = Idents("IDENT|" & IdentSession)

'-> Pointeur vers la feuille en cours
Set frmEmilieObj.aFrm = Me

'-> Initialisation
frmEmilieObj.Init Entry(2, Me.ListView1.SelectedItem.Key, "|")

'-> Afficher la feuille
strRetour = ""
frmEmilieObj.Show vbModal

'-> Tester la variable de retour
If strRetour = "" Then Exit Sub

'-> pointer sur l'ident
Set aIdent = Idents("IDENT|" & IdentSession)

'-> Pointer sur l'objet du catalogue
Set aEmilieObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & Entry(2, Me.ListView1.SelectedItem.Key, "|"))

'-> Afficher la propri�t�
DisplayObjProp aEmilieObj, Me.ListView1.SelectedItem, aIdent

End Sub

Private Sub DeleteEmilieObj()

'---> Cette proc�dure supprime un objet du catalogue

Dim Rep As VbMsgBoxResult
Dim aEmilieObj As Object

'-> V�rifier qu'un objet soit s�lectionn�
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Pointer sur l'objet
Set aEmilieObj = Idents("IDENT|" & IdentSession).ServeurDOG.Catalogue(Idents("IDENT|" & IdentSession).Code & "|" & Me.ListView1.SelectedItem.Text)

'-> v�rifier qu'il n'y a pas de liens
If aEmilieObj.fNames.Count <> 0 Then
    MsgBox GetMesseprog("APPLIC", "43"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    Exit Sub
End If

'-> Demander copnfirmation
Rep = MsgBox(Replace(GetMesseprog("APPLIC", "44"), "#OBJ#", Me.ListView1.SelectedItem.Text & " - " & Me.ListView1.SelectedItem.SubItems(1)), vbExclamation + vbYesNo, GetMesseprog("APPLIC", "21"))
If Rep = vbNo Then Exit Sub

'-> Supprimer l'objet du catalogue
Idents("IDENT|" & IdentSession).ServeurDOG.Catalogue.Remove (Idents("IDENT|" & IdentSession).Code & "|" & Me.ListView1.SelectedItem.Text)

'-> Enregistrer le catalogue
Idents("IDENT|" & IdentSession).ServeurDOG.Save_Catalogue

'-> Supprimer l'objet du listView
Me.ListView1.ListItems.Remove Me.ListView1.SelectedItem.Index

End Sub
