VERSION 5.00
Begin VB.Form frmAgent 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   10230
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   8940
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10230
   ScaleWidth      =   8940
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      Caption         =   "Agent B3D : "
      Height          =   2055
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   8895
      Begin VB.CommandButton cmdCrypt 
         Caption         =   "Crypt"
         Height          =   975
         Left            =   3000
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton cmdDebug 
         Caption         =   "Debug"
         Height          =   975
         Left            =   1680
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton cmdTrace 
         Caption         =   "Trace"
         Height          =   975
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   360
         Width           =   1335
      End
      Begin VB.CommandButton cmdUpdate 
         Caption         =   "Update"
         Height          =   975
         Left            =   4320
         Picture         =   "frmAgent.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   360
         Width           =   1215
      End
      Begin VB.Image Image3 
         Height          =   1500
         Left            =   7200
         Picture         =   "frmAgent.frx":0CCA
         Top             =   240
         Width           =   1500
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   3240
         TabIndex        =   19
         Top             =   1560
         Width           =   2295
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   1560
         Width           =   2175
      End
      Begin VB.Image Image1 
         Height          =   480
         Left            =   5520
         Picture         =   "frmAgent.frx":8884
         Top             =   360
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Image Image2 
         Height          =   480
         Left            =   6000
         Picture         =   "frmAgent.frx":954E
         Top             =   360
         Visible         =   0   'False
         Width           =   480
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Debug : "
      Height          =   8055
      Left            =   0
      TabIndex        =   0
      Top             =   2160
      Width           =   8895
      Begin VB.CommandButton Command6 
         Height          =   855
         Left            =   4920
         Picture         =   "frmAgent.frx":A218
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   7080
         Width           =   1215
      End
      Begin VB.TextBox Text5 
         Height          =   2175
         Left            =   240
         MultiLine       =   -1  'True
         TabIndex        =   20
         Top             =   4800
         Width           =   8535
      End
      Begin VB.CommandButton Command3 
         Height          =   375
         Left            =   8400
         Picture         =   "frmAgent.frx":AAE2
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   840
         Width           =   375
      End
      Begin VB.TextBox Text3 
         Height          =   405
         Left            =   2640
         TabIndex        =   8
         Top             =   840
         Width           =   5655
      End
      Begin VB.CommandButton Command2 
         Height          =   375
         Left            =   8400
         Picture         =   "frmAgent.frx":AE6C
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   1320
         Width           =   375
      End
      Begin VB.TextBox Text2 
         Height          =   405
         Left            =   2640
         TabIndex        =   6
         Top             =   1320
         Width           =   5655
      End
      Begin VB.TextBox Text1 
         Height          =   405
         Left            =   2640
         TabIndex        =   5
         Top             =   360
         Width           =   5655
      End
      Begin VB.CommandButton Command1 
         Height          =   375
         Left            =   8400
         Picture         =   "frmAgent.frx":B1F6
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   360
         Width           =   375
      End
      Begin VB.CommandButton Command5 
         Height          =   855
         Left            =   6240
         Picture         =   "frmAgent.frx":B580
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   7080
         Width           =   1215
      End
      Begin VB.CommandButton Command4 
         Height          =   855
         Left            =   7560
         Picture         =   "frmAgent.frx":C24A
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   7080
         Width           =   1215
      End
      Begin VB.TextBox Text4 
         Height          =   2175
         Left            =   240
         MultiLine       =   -1  'True
         TabIndex        =   1
         Top             =   2160
         Width           =   8535
      End
      Begin VB.Label Label4 
         Caption         =   "Label7"
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   4440
         Width           =   1815
      End
      Begin VB.Label Label3 
         Caption         =   "Label7"
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   1800
         Width           =   1815
      End
      Begin VB.Label Label9 
         Caption         =   "Label7"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   840
         Width           =   1815
      End
      Begin VB.Label Label8 
         Caption         =   "Label7"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   1320
         Width           =   1815
      End
      Begin VB.Label Label7 
         Caption         =   "Label7"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   360
         Width           =   1815
      End
   End
End
Attribute VB_Name = "frmAgent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCrypt_Click()

'-> Envoyer l'ordre de cryptage
SetCrypt

'-> Afficher la bonne icone
If IsCrypt Then
    Set Me.cmdCrypt.Picture = Me.Image2.Picture
Else
    Set Me.cmdCrypt.Picture = Me.Image1.Picture
End If


End Sub

Private Sub cmdDebug_Click()

'---> Passage en mode debug

'-> Envoyer l'ordre de debug
SetDebug

'-> Positionner l'icone
If IsDebug Then
    Set Me.cmdDebug.Picture = Me.Image2.Picture
Else
    Set Me.cmdDebug.Picture = Me.Image1.Picture
End If

End Sub

Private Sub cmdTrace_Click()

'---> Basculer la trace

'-> Envoyer l'ordre
SetTrace

'-> Icone
If IsTrace Then
    Set Me.cmdTrace.Picture = Me.Image2.Picture
Else
    Set Me.cmdTrace.Picture = Me.Image1.Picture
End If

End Sub

Private Sub cmdUpdate_Click()

On Error GoTo GestError

'-> Bloquer la feuille
Me.Enabled = False
Screen.MousePointer = 11

'-> Lancer un refresh
RefreshDog

GestError:
'-> D�bloquer l'�cran
Me.Enabled = True
Screen.MousePointer = 0

End Sub

Private Sub Command1_Click()

'-> Cr�er un nom de fichier de debug
Me.Text1.Text = CreateDebugAgentFile

End Sub

Private Sub Command2_Click()

Me.Text2.Text = ExportParamToFile

End Sub

Private Sub Command3_Click()

Me.Text3.Text = CreateDebugIdxFile

End Sub


Private Sub Command4_Click()

'---> constituer un fichier de demande

Dim i As Long

'-> V�rifier qu'il y ait des instructions
If Trim(Me.Text4.Text) = "" Then Exit Sub

'-> vider la r�ponse
Me.Text5.Text = "Envoie du fichier : " & DebugAgentDmdFile & " � : " & Now & Chr(13) & Chr(10) & "Attente du fichier : " & DebugAgentRspFile

'-> Variable d'arret
StopDemande = False

'-> Bloquer le bouton d'envoie
Me.Command4.Enabled = False

'-> Envoyer une demande
SendDemande Trim(Me.Text4.Text), True

'-> Imprimer une entete de r�ponse
Me.Text5.Text = Me.Text5.Text & Chr(13) & Chr(10) & "R�ponse re�ue � : " & Now

'-> Afficher les r�ponses
For i = 1 To Reponses.Count
    Me.Text5.Text = Me.Text5.Text & Chr(13) & Chr(10) & Reponses(i)
Next

'-> D�bloquer le bouton d'envoie
Me.Command4.Enabled = True

End Sub

Private Sub Command5_Click()

Me.Text5.Text = Me.Text5.Text & Chr(13) & Chr(10) & "Demande annul�e � : " & Now

'-> Positionner la variable sur stop
StopDemande = True

End Sub

Private Sub Command6_Click()
Me.Text5.Text = ""
End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("CONSOLE", "40")

'-> Libell�
Me.Label1.Caption = GetMesseprog("CONSOLE", "41")
Me.Label7.Caption = GetMesseprog("CONSOLE", "45")
Me.Label8.Caption = GetMesseprog("CONSOLE", "46")
Me.Label9.Caption = GetMesseprog("CONSOLE", "77")
Me.Label3.Caption = GetMesseprog("CONSOLE", "90")
Me.Label4.Caption = GetMesseprog("CONSOLE", "91")

'-> Valeur
Me.Label2.Caption = " " & Format(pIdAgent, "0000")

'-> Chargement des valeurs
If IsCrypt Then
    Set Me.cmdCrypt.Picture = Me.Image2.Picture
Else
    Set Me.cmdCrypt.Picture = Me.Image1.Picture
End If
    
If IsDebug Then
    Set Me.cmdDebug.Picture = Me.Image2.Picture
Else
    Set Me.cmdDebug.Picture = Me.Image1.Picture
End If

If IsTrace Then
    Set Me.cmdTrace.Picture = Me.Image2.Picture
Else
    Set Me.cmdTrace.Picture = Me.Image1.Picture
End If

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub Text5_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub
