VERSION 5.00
Begin VB.Form frmPropField 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3300
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4110
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3300
   ScaleWidth      =   4110
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command3 
      Height          =   615
      Left            =   2760
      Picture         =   "frmPropField.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   2640
      Width           =   615
   End
   Begin VB.CommandButton Command1 
      Height          =   615
      Left            =   3480
      Picture         =   "frmPropField.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   2640
      Width           =   615
   End
   Begin VB.Frame Frame9 
      Height          =   2535
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   4095
      Begin VB.CheckBox Extent 
         Alignment       =   1  'Right Justify
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   2160
         Width           =   3855
      End
      Begin VB.CheckBox Modif 
         Alignment       =   1  'Right Justify
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   1800
         Width           =   3855
      End
      Begin VB.CheckBox F10 
         Alignment       =   1  'Right Justify
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   1440
         Width           =   3855
      End
      Begin VB.CheckBox Replica 
         Alignment       =   1  'Right Justify
         Height          =   255
         Left            =   120
         TabIndex        =   0
         Top             =   360
         Width           =   3855
      End
      Begin VB.CheckBox Crypt 
         Alignment       =   1  'Right Justify
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   720
         Width           =   3855
      End
      Begin VB.CheckBox Histo 
         Alignment       =   1  'Right Justify
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   1080
         Width           =   3855
      End
   End
End
Attribute VB_Name = "frmPropField"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public X As ListItem
Public IsIndexField As Boolean

Public Sub Initialisation()

'-> Charger les propri�t�s du champ
If UCase$(X.SubItems(3)) = "YES" Then Me.Replica.Value = 1
If UCase$(X.SubItems(4)) = "YES" Then Me.Crypt.Value = 1
If UCase$(X.SubItems(5)) = "YES" Then Me.Histo.Value = 1
If UCase$(X.SubItems(6)) = "YES" Then Me.F10.Value = 1
If UCase$(X.SubItems(7)) = "YES" Then Me.Modif.Value = 1
If UCase$(X.SubItems(9)) = "YES" Then Me.Extent.Value = 1

'-> Attention : si le champ fait partie d'un index : ne pas pouvoir modifier sa propri�t� Extent
If IsIndexField Then
    '-> Pas de type Extent
    Me.Extent.Value = 0
    Me.Extent.Enabled = False
End If

End Sub

Private Sub Command1_Click()

'-> Mettre � jour l'affichage
If Me.Replica.Value = 1 Then
    X.SubItems(3) = "Yes"
Else
    X.SubItems(3) = "No"
End If
If Me.Crypt.Value = 1 Then
    X.SubItems(4) = "Yes"
Else
    X.SubItems(4) = "No"
End If
If Me.Histo.Value = 1 Then
    X.SubItems(5) = "Yes"
Else
    X.SubItems(5) = "No"
End If
If Me.F10.Value = 1 Then
    X.SubItems(6) = "Yes"
Else
    X.SubItems(6) = "No"
End If
If Me.Modif.Value = 1 Then
    X.SubItems(7) = "Yes"
Else
    X.SubItems(7) = "No"
End If
If Me.Extent.Value = 1 Then
    X.SubItems(9) = "Yes"
Else
    X.SubItems(9) = "No"
End If

strRetour = "OK"

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Command3_Click()
strRetour = ""
Unload Me
End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("FRMSCHEMA", "30")

'-> Libell� des propri�t�s
Me.Frame9.Caption = GetMesseprog("DOG", "13") & " : "
Me.Replica.Caption = GetMesseprog("FRMSCHEMA", "26")
Me.Crypt.Caption = GetMesseprog("FRMSCHEMA", "27")
Me.Histo.Caption = GetMesseprog("FRMSCHEMA", "28")
Me.F10.Caption = GetMesseprog("FRMSCHEMA", "29")
Me.Modif.Caption = GetMesseprog("FRMSCHEMA", "30")
Me.Extent.Caption = GetMesseprog("FRMSCHEMA", "32")

End Sub
