VERSION 5.00
Begin VB.Form frmLangue 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   4500
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   7410
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4500
   ScaleWidth      =   7410
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Height          =   615
      Left            =   6000
      Picture         =   "frmLangue.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   3840
      Width           =   615
   End
   Begin VB.CommandButton Command1 
      Height          =   615
      Left            =   6720
      Picture         =   "frmLangue.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   3840
      Width           =   615
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   9
      Left            =   2040
      TabIndex        =   19
      Top             =   3480
      Width           =   5295
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   8
      Left            =   2040
      TabIndex        =   17
      Top             =   3120
      Width           =   5295
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   7
      Left            =   2040
      TabIndex        =   15
      Top             =   2760
      Width           =   5295
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   6
      Left            =   2040
      TabIndex        =   13
      Top             =   2400
      Width           =   5295
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   5
      Left            =   2040
      TabIndex        =   11
      Top             =   2040
      Width           =   5295
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   4
      Left            =   2040
      TabIndex        =   9
      Top             =   1680
      Width           =   5295
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   3
      Left            =   2040
      TabIndex        =   7
      Top             =   1320
      Width           =   5295
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   2
      Left            =   2040
      TabIndex        =   5
      Top             =   960
      Width           =   5295
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   1
      Left            =   2040
      TabIndex        =   3
      Top             =   600
      Width           =   5295
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Index           =   0
      Left            =   2040
      TabIndex        =   1
      Top             =   240
      Width           =   5295
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Index           =   9
      Left            =   240
      TabIndex        =   18
      Top             =   3480
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Index           =   8
      Left            =   240
      TabIndex        =   16
      Top             =   3120
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Index           =   7
      Left            =   240
      TabIndex        =   14
      Top             =   2760
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Index           =   6
      Left            =   240
      TabIndex        =   12
      Top             =   2400
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Index           =   5
      Left            =   240
      TabIndex        =   10
      Top             =   2040
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Index           =   4
      Left            =   240
      TabIndex        =   8
      Top             =   1680
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Index           =   3
      Left            =   240
      TabIndex        =   6
      Top             =   1320
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Index           =   2
      Left            =   240
      TabIndex        =   4
      Top             =   960
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   2
      Top             =   600
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1575
   End
End
Attribute VB_Name = "frmLangue"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Sub DisplayLangue(strLangue As String)
Dim i As Integer
'-> Alimenter les variables
For i = 0 To 9
    Me.Text1(i).Text = Entry(i + 1, strLangue, "|")
Next

End Sub



Private Sub Command1_Click()
'---> Mettre � jour la feuille frmAdminCatalogue
Dim strMaj As String
Dim i  As Integer

For i = 0 To 9
    If i = 0 Then
        strMaj = Me.Text1(i).Text
    Else
        strMaj = strMaj & "|" & Me.Text1(i).Text
    End If
Next
'-> Variable de retour
strRetour = strMaj
'-> Decharger la feuille
Unload Me
End Sub

Private Sub Command2_Click()
strRetour = ""
Unload Me
End Sub

Private Sub Form_Load()

Dim i As Integer

'-> Titre de la feuille
Me.Caption = GetMesseprog("DOG", "2")

'-> Charger les libell�s des langues
For i = 1 To 10
    Me.Label1(i - 1) = GetMesseprog("PAYS", CStr(i))
Next
    
End Sub



Private Sub Text1_GotFocus(Index As Integer)
Text1(Index).SelStart = 0
Text1(Index).SelLength = 65535
End Sub
Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
If KeyAscii = 13 Then
    If Index <> 9 Then
        Text1(Index + 1).SetFocus
    Else
        Me.Command2.SetFocus
    End If
End If

End Sub
