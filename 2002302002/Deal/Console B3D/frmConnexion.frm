VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmConnexion 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Identifiaction"
   ClientHeight    =   4155
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6585
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4155
   ScaleWidth      =   6585
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConnexion.frx":0000
            Key             =   "IDENT"
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   855
      Left            =   4680
      Picture         =   "frmConnexion.frx":039A
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   3240
      Width           =   1815
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Ok"
      Height          =   855
      Left            =   4680
      Picture         =   "frmConnexion.frx":1064
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   2280
      Width           =   1815
   End
   Begin VB.TextBox Text2 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1080
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   480
      Width           =   3495
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1080
      TabIndex        =   0
      Top             =   120
      Width           =   3495
   End
   Begin VB.PictureBox Picture1 
      Height          =   2055
      Left            =   4680
      ScaleHeight     =   1995
      ScaleWidth      =   1755
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   120
      Width           =   1815
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3255
      Left            =   1080
      TabIndex        =   3
      Top             =   840
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   5741
      View            =   3
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Code         "
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "D�signation"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Password :"
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Login : "
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "frmConnexion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Essai As Integer
Dim Opecode As String

Private Sub Command1_Click()

Dim Pwd As String
Dim Access As String
Dim OpC As String

'-> Tester qu'un ident soit s�lectionn� sion n'est pas en mode serveuer
If ConsoleMode = "SERVEUR" Then
    Access = DeCrypt(GetIniFileValue(Opecode, "NV", ApplicationDirectory & "B3D\Param\Operats.ini"))
    If Access <> "ADM" Then
        MsgBox "Connecting Error", vbCritical + vbOKOnly, "Error"
        Exit Sub
    End If
Else
    If Me.ListView1.SelectedItem Is Nothing Then Exit Sub
End If

'-> Incr�menter le compteur d'essai
Essai = Essai + 1

'-> R�cup�rer le mot de passe
Pwd = DeCrypt(GetIniFileValue(Opecode, "PWD", ApplicationDirectory & "B3D\Param\Operats.ini"))

'-> Tester le contenu
If Trim(Pwd) = "" Then GoTo BadPass
If Pwd <> Me.Text2.Text Then GoTo BadPass

'-> Indiquer pour quel ident on travaille
If Not Me.ListView1.SelectedItem Is Nothing Then IdentSession = Me.ListView1.SelectedItem.Text
    
'-> Renvoyer le code opo�rateur saisi
strRetour = Opecode

'-> D�charger la feuille
Unload Me
    
'-> Quitter
Exit Sub

'-> Gestion essai
BadPass:

    MsgBox "Error Connecting " & Essai, vbCritical + vbOKOnly, "Fatal Error"
    If Essai = 3 Then
        End
    Else
        Me.Text2.SetFocus
    End If
    
End Sub

Private Sub Command2_Click()
strRetour = ""
Unload Me
End Sub

Private Sub Form_Load()

'-> Bloquer la liste des idents si on est en mode Serveur
If UCase$(Trim(ConsoleMode)) = "SERVEUR" Then Me.ListView1.Enabled = False

End Sub

Private Sub ListView1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

'-> Trier sur les entetes de colonne
Me.ListView1.SortKey = ColumnHeader.Index - 1
If Me.ListView1.SortOrder = lvwAscending Then
    Me.ListView1.SortOrder = lvwDescending
Else
    Me.ListView1.SortOrder = lvwAscending
End If
Me.ListView1.Sorted = True

End Sub

Private Sub ListView1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Command1_Click


End Sub

Private Sub Text1_GotFocus()
SelectTxtBox Me.Text1
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)

Dim PhotoFile As String
Dim Idents As String
Dim DefIdent As String
Dim i As Integer
Dim x As ListItem

If KeyAscii = 13 Then
    '-> R�cup�rer le code op�rateur
    Opecode = Me.Text1.Text
    '-> Charger la photo si on la trouve
    PhotoFile = DeCrypt(GetIniFileValue(Opecode, "PHOTO", ApplicationDirectory & "B3D\Param\Operats.ini"))
    '-> Tester s'il elle existe
    If Trim(PhotoFile) <> "" Then
        If Dir$(ApplicationDirectory & "B3D\img\" & PhotoFile) <> "" Then Me.Picture1.Picture = LoadPicture(ApplicationDirectory & "B3D\img\" & PhotoFile)
    End If
    '-> R�cup�rer la liste des identifiants
    Idents = DeCrypt(GetIniFileValue(Opecode, "IDENTS", ApplicationDirectory & "B3D\Param\Operats.ini"))
    '-> Vider le listview
    Me.ListView1.ListItems.Clear
    If Idents <> "" Then
        '-> Afficher la liste des idents
        For i = 1 To NumEntries(Idents, "|")
            '-> Get d'un d�finition d'ident
            DefIdent = Entry(i, Idents, "|")
            If Trim(DefIdent) = "" Then Exit For
            '-> Ajouter un ident
            Set x = Me.ListView1.ListItems.Add(, "IDENT|" & DefIdent, DefIdent, , "IDENT")
            '-> Get de son libell�
            x.SubItems(1) = DeCrypt(GetIniFileValue("PARAM", "LIBEL", ApplicationDirectory & "\" & DefIdent & "\Param\" & DefIdent & ".ini"))
        Next 'Pour tous les idents
        '-> Formatter le listview
        FormatListView Me.ListView1
    End If 'Si on trouve les idents
    '-> Donner le focus
    Me.Text2.SetFocus
End If

End Sub

Private Sub Text2_GotFocus()
SelectTxtBox Me.Text2
End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then
    If UCase$(Trim(ConsoleMode)) <> "CLIENT" Then
        Command1_Click
    Else
        Me.ListView1.SetFocus
    End If
End If

End Sub
