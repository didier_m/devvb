VERSION 5.00
Begin VB.Form frmPropInt 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   3660
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5895
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3660
   ScaleWidth      =   5895
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Height          =   615
      Left            =   5160
      Picture         =   "frmPropInt.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   3000
      Width           =   615
   End
   Begin VB.CommandButton Command3 
      Height          =   615
      Left            =   4440
      Picture         =   "frmPropInt.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   3000
      Width           =   615
   End
   Begin VB.Frame Frame1 
      Height          =   2895
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   5895
      Begin VB.ComboBox Combo2 
         Height          =   315
         Left            =   1680
         TabIndex        =   8
         Top             =   2400
         Width           =   3495
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Check1"
         Height          =   375
         Left            =   240
         TabIndex        =   7
         Top             =   2400
         Width           =   1335
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   4560
         TabIndex        =   6
         Top             =   1920
         Width           =   615
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   1920
         Width           =   4215
      End
      Begin VB.Frame Frame6 
         Height          =   615
         Left            =   1680
         TabIndex        =   2
         Top             =   1080
         Width           =   3495
         Begin VB.OptionButton OptOutPut 
            Caption         =   "Output"
            Height          =   255
            Left            =   1800
            TabIndex        =   4
            Top             =   240
            Width           =   855
         End
         Begin VB.OptionButton OptInput 
            Caption         =   "Input"
            Height          =   255
            Left            =   360
            TabIndex        =   3
            Top             =   240
            Value           =   -1  'True
            Width           =   855
         End
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   1680
         TabIndex        =   0
         Top             =   240
         Width           =   3495
      End
      Begin VB.Label Label4 
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   1200
         Width           =   1335
      End
      Begin VB.Label Label3 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1680
         TabIndex        =   11
         Top             =   600
         Width           =   3495
      End
      Begin VB.Label Label1 
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label2 
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Width           =   1335
      End
      Begin VB.Image Image10 
         Height          =   480
         Left            =   5280
         MouseIcon       =   "frmPropInt.frx":1994
         MousePointer    =   99  'Custom
         Picture         =   "frmPropInt.frx":265E
         Top             =   480
         Width           =   480
      End
   End
End
Attribute VB_Name = "frmPropInt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Indique si on est en ajout ou en edit
Private EditMode As Boolean
Private aFname As Fname
Public aIdent As clsIdent


Private Sub Check1_Click()

If Me.Check1.Value = 1 Then Me.Combo1.Enabled = True

End Sub

Private Sub Command1_Click()

'---> Ajouter l'interface

Dim aInt As Interface
Dim i As Integer

'-> V�rifier la coh�rence
If Not EditMode Then '-< On est en mode cr�ation
    '-> V�rifier que le code soit saisi
    If Trim(Me.Text1.Text) = "" Then
        '-> Code obligatoire
        MsgBox GetMesseprog("APPLIC", "1"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
        Me.Text1.SetFocus
        Exit Sub
    End If
    
    '-> V�rifier les carac�res invalides
    If ChechInvalidChar(Me.Text1.Text) Then
        '-> Caract�res invalides
        MsgBox GetMesseprog("APPLIC", "29"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
        Me.Text1.SetFocus
        Exit Sub
    End If
    
    '-> V�rifier que le code saisi n'existe pas d�ja
    For Each aInt In aFname.Interfaces
        '-> Comparer  le code
        If UCase$(Trim(aInt.Name)) = UCase$(Trim(Me.Text1.Text)) Then
            '-> Le code existe d�ja
            MsgBox GetMesseprog("APPLIC", "2"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
            Me.Text1.SetFocus
            Exit Sub
        End If
    Next 'Pour tous les interfaces
End If 'Selon le mode
    
'-> V�rifier le s�parateur
If Me.Check1.Value = 1 Then
    '-> V�rifier que l'on ait sp�cifi� un s�parateur
    If Me.Combo1.ListIndex = -1 Then
        '-> Mauvais s�parateur
        MsgBox GetMesseprog("FRMSCHEMA", "51"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
        Me.Combo1.SetFocus
        Exit Sub
    End If
End If 'Si on utilise un s�parateur

'-> V�rifier si on utilise un index
If Me.Check2.Value = 1 Then
    '-> V�rifier que l'on ait sp�cifi� un index
    If Me.Combo2.ListIndex = -1 Then
        '-> Mauvais index
        MsgBox GetMesseprog("FRMSCHEMA", "52"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
        Me.Combo2.SetFocus
        Exit Sub
    End If
End If 'Si on utilise un index

'-> Si on est en mode cr�ation : ajouter l'interface dans la liste
If Not EditMode Then
    '-> Cr�er un nouvel Interface
    aIdent.ServeurDOG.AddInterface UCase$(Trim(Me.Text1.Text)), aFname.Num_Fname
End If

'-> Pointer sur cet ident
Set aInt = aFname.Interfaces("INT|" & UCase$(Trim(Me.Text1.Text)))

'-> Mise � jour de ses propri�t�s
For i = 1 To 10
    '-> Positionner le code langue
    aInt.CodeLangue = i
    aInt.Designation = Entry(i, Me.Label3.Tag, "|")
Next
    
'-> Sens de l'interface
If Me.OptInput.Value Then
    aInt.Intput_Output = 0
Else
    aInt.Intput_Output = 1
End If

'-> Si on utilise ou non un s�parateur
If Me.Check1.Value = 1 Then
    aInt.UseSeparateur = True
    aInt.Separateur = Me.Combo1.Text
Else
    aInt.UseSeparateur = False
    aInt.Separateur = ""
End If

'-> Si on utilise un index
If Me.Check2.Value = 1 Then
    aInt.UseIndex = True
    aInt.Index = Trim(Entry(1, Me.Combo2.Text, "-"))
Else
    aInt.UseIndex = False
    aInt.Index = ""
End If

'-> Enregistrer
aIdent.ServeurDOG.Save_Schema

'-> Pluger la variable interface
strRetour = UCase$(Trim(aInt.Name))

'-> D�charger
Unload Me

End Sub

Private Sub Command3_Click()

'-> Quitter Unload me
Unload Me

End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("FRMSCHEMA", "49")

'-> Libell�s
Me.Label1.Caption = GetMesseprog("CONSOLE", "17")
Me.Label2.Caption = GetMesseprog("FRMSCHEMA", "4")
Me.Label4.Caption = GetMesseprog("FRMSCHEMA", "44")
Me.OptInput.Caption = GetMesseprog("FRMSCHEMA", "45")
Me.OptOutPut.Caption = GetMesseprog("FRMSCHEMA", "46")
Me.Check1.Caption = GetMesseprog("FRMSCHEMA", "47")
Me.Check2.Caption = GetMesseprog("FRMSCHEMA", "50")

End Sub

Public Sub Init(TopCreate As Boolean, aFnameParam As Fname, Optional aInt As Interface)

'---> Init de la feuille

Dim i As Integer
Dim aIname As Iname

'-> Indiquer si on est en mode cr�ate ou non
EditMode = Not TopCreate

'-> Positionner le pointeur vers la table
Set aFname = aFnameParam

'-> Charger la liste des s�parateurs
Me.Combo1.AddItem ","
Me.Combo1.AddItem ";"
Me.Combo1.AddItem "|"
Me.Combo1.AddItem "�"
Me.Combo1.AddItem "/"
Me.Combo1.AddItem "\"
Me.Combo1.AddItem "$"
Me.Combo1.AddItem "*"
Me.Combo1.AddItem "�"
Me.Combo1.AddItem ">"
Me.Combo1.AddItem "<"
Me.Combo1.AddItem "."
Me.Combo1.AddItem ":"
Me.Combo1.AddItem "!"
Me.Combo1.AddItem "#"
Me.Combo1.AddItem "~"
Me.Combo1.AddItem "-"
Me.Combo1.AddItem "_"
Me.Combo1.AddItem "="

'-> Charger la liste des index
If aFname.IndexDirect <> "" Then
    '-> On est sur une table � index directe
    Me.Check2.Enabled = False
    Me.Combo2.Enabled = False
Else
    '-> On est sur une table bas�e sur un index
    Me.Check2.Enabled = True
    Me.Combo2.Enabled = True
    For Each aIname In aFname.Inames
        '-> Positionner le libell�
        aIname.CodeLangue = OpeCodeLangue
        Me.Combo2.AddItem aIname.Num_Iname & " - " & aIname.Designation
    Next
End If

'-> Gestion des champs bloqu�s ou non
If TopCreate Then
    '-> D�bloquer le code
    Me.Text1.Enabled = True
    '-> Code langue � vide
    Me.Label3.Tag = "|||||||||"
Else
    '-> D�bloquer le code
    Me.Text1.Enabled = False

    '-> On est en modif : Positionner les objets
    Me.Text1.Text = aInt.Name
    
    '-> D�signation
    aInt.CodeLangue = OpeCodeLangue
    Me.Label3.Caption = aInt.Designation
    For i = 1 To 10
        aInt.CodeLangue = i
        If Me.Label3.Tag = "" Then
            Me.Label3.Tag = aInt.Designation
        Else
            Me.Label3.Tag = Me.Label3.Tag & "|" & aInt.Designation
        End If
    Next
    
    '-> Sens de l'interface
    If aInt.Intput_Output = 0 Then
        Me.OptInput.Value = True
    Else
        Me.OptOutPut.Value = True
    End If
    
    '-> S�parateur
    If aInt.UseSeparateur Then
        Me.Check1.Value = 1
        Me.Combo1.Enabled = True
        For i = 0 To Me.Combo1.ListCount - 1
            If Me.Combo1.List(i) = aInt.Separateur Then
                Me.Combo1.ListIndex = i
                Exit For
            End If
        Next
    Else
        Me.Check1.Value = 0
        Me.Combo1.Enabled = False
    End If 'Si on utilise un s�parateur
        
    '-> Gestion des index
    If aInt.UseIndex Then
        Me.Check2.Value = 1
        For i = 0 To Me.Combo2.ListCount - 1
            If UCase$(Trim(Entry(1, Me.Combo2.List(i), "-"))) = UCase$(Trim(aInt.Index)) Then
                '-> Positionner le code
                Me.Combo2.ListIndex = i
                Exit For
            End If
        Next 'Pour tous les index
    Else
        '-> Ne rien faire si on est en acc�s directe
        Me.Check2.Value = 0
    End If 'Si table en acc�s direct
    
End If 'Si on est en mode ajout

End Sub

Private Sub Image10_Click()

Dim i As Integer

'-> Charger la feuille
Load frmLangue

'-> Gestion du multiLangue
For i = 1 To 10
    '-> Charger les libell�s par langue
    frmLangue.Text1(i - 1) = Entry(i, Me.Label3.Tag, "|")
Next

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher la feuille
frmLangue.Show vbModal

'-> Tester le retour
If strRetour = "" Then Exit Sub

'-> Mise � jour de la propri�t� TAG
Me.Label3.Tag = strRetour

'-> Afficher le nouveau libell�
Me.Label3.Caption = Entry(OpeCodeLangue, strRetour, "|")

End Sub

Private Sub Text1_GotFocus()

SelectTxtBox Me.Text1

End Sub
