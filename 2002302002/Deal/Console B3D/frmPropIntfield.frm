VERSION 5.00
Begin VB.Form frmPropIntfield 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   7395
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6390
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7395
   ScaleWidth      =   6390
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command3 
      Height          =   615
      Left            =   5040
      Picture         =   "frmPropIntfield.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   6720
      Width           =   615
   End
   Begin VB.CommandButton Command1 
      Height          =   615
      Left            =   5760
      Picture         =   "frmPropIntfield.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   6720
      Width           =   615
   End
   Begin VB.Frame Frame1 
      Height          =   6615
      Left            =   0
      TabIndex        =   13
      Top             =   -30
      Width           =   6375
      Begin VB.Frame Frame6 
         Caption         =   "Frame6"
         Height          =   735
         Left            =   240
         TabIndex        =   8
         Top             =   3840
         Width           =   5895
         Begin VB.CheckBox Check1 
            Caption         =   "Check1"
            Height          =   255
            Left            =   240
            TabIndex        =   9
            Top             =   300
            Width           =   5415
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Frame5"
         Height          =   1695
         Left            =   240
         TabIndex        =   10
         Top             =   4680
         Width           =   5895
      End
      Begin VB.Frame Frame4 
         Caption         =   "Frame4"
         Height          =   855
         Left            =   240
         TabIndex        =   6
         Top             =   2880
         Width           =   5895
         Begin VB.TextBox Text5 
            Height          =   285
            Left            =   240
            TabIndex        =   7
            Top             =   360
            Width           =   5415
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Frame2"
         Height          =   975
         Left            =   240
         TabIndex        =   3
         Top             =   1800
         Width           =   5895
         Begin VB.TextBox Text4 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1920
            TabIndex        =   4
            Top             =   360
            Width           =   735
         End
         Begin VB.TextBox Text3 
            Height          =   285
            Left            =   4560
            TabIndex        =   5
            Top             =   360
            Width           =   735
         End
         Begin VB.Label Label6 
            Caption         =   "Label3"
            Enabled         =   0   'False
            Height          =   255
            Left            =   240
            TabIndex        =   19
            Top             =   360
            Width           =   1575
         End
         Begin VB.Label Label5 
            Caption         =   "Label3"
            Height          =   255
            Left            =   2880
            TabIndex        =   18
            Top             =   360
            Width           =   1575
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Frame2"
         Height          =   975
         Left            =   240
         TabIndex        =   0
         Top             =   720
         Width           =   5895
         Begin VB.TextBox Text2 
            Height          =   285
            Left            =   4560
            TabIndex        =   2
            Top             =   360
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Height          =   285
            Left            =   1920
            TabIndex        =   1
            Top             =   360
            Width           =   735
         End
         Begin VB.Label Label4 
            Caption         =   "Label3"
            Height          =   255
            Left            =   2880
            TabIndex        =   17
            Top             =   360
            Width           =   1575
         End
         Begin VB.Label Label3 
            Caption         =   "Label3"
            Height          =   255
            Left            =   240
            TabIndex        =   16
            Top             =   360
            Width           =   1575
         End
      End
      Begin VB.Label Label2 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1440
         TabIndex        =   15
         Top             =   360
         Width           =   4695
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   360
         Width           =   975
      End
   End
End
Attribute VB_Name = "frmPropIntfield"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Ident associ�
Public aIdent As clsIdent

'-> Interface associ�
Public aInterface As Interface


Private Sub Command1_Click()

'---> Effectuer les v�rifications

Dim aIntField As IntField

If aInterface.UseSeparateur Then 'Sep
    '-> V�rifier qu'il soit numeric
    If Not IsNumeric(Me.Text3.Text) Then
        MsgBox Replace(GetMesseprog("APPLIC", "3"), "#CODE#", Me.Text3.Text), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
        Me.Text3.SetFocus
        Exit Sub
    End If
    
    '-> Si egal = 0 il doit y avoir une valeur par d�faut
    If CDbl(Me.Text3.Text) = 0 Then
        '-> Tester qu'il y ait une valeur par d�faut
        If Trim(Me.Text5.Text) = "" Then
            MsgBox GetMesseprog("FRMSCHEMA", "63"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
            Me.Text5.SetFocus
            Exit Sub
        End If
    End If
Else 'Pos/Lng
    '-> V�rifier qu'il soit numeric
    If Not IsNumeric(Me.Text1.Text) Then
        MsgBox Replace(GetMesseprog("APPLIC", "3"), "#CODE#", Me.Text1.Text), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
        Me.Text1.SetFocus
        Exit Sub
    End If
    
    '-> Si egal = 0 il doit y avoir une valeur par d�faut
    If CDbl(Me.Text1.Text) = 0 Then
        '-> Tester qu'il y ait une valeur par d�faut
        If Trim(Me.Text5.Text) = "" Then
            MsgBox GetMesseprog("FRMSCHEMA", "63"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
            Me.Text5.SetFocus
            Exit Sub
        End If
    Else
        '-> La longueur ne doit pas �tre �gale � 0
        If Not IsNumeric(Me.Text2.Text) Then
            MsgBox Replace(GetMesseprog("APPLIC", "3"), "#CODE#", Me.Text2.Text), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
            Me.Text2.SetFocus
            Exit Sub
        Else
            If CDbl(Me.Text2.Text) = 0 Then
                MsgBox GetMesseprog("FRMSCHEMA", "64"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
                Me.Text2.SetFocus
                Exit Sub
            End If
        End If
    End If 'Si la position = 0
End If 'Si on utilise un s�parateur

'-> Pointer sur le champ associ�
Set aIntField = aInterface.Fields(Me.Label2.Tag)

'-> Propri�t�s
If aInterface.UseSeparateur Then
    aIntField.Position = CDbl(Me.Text3.Text)
    aIntField.Longueur = 0
Else
    aIntField.Position = CDbl(Me.Text1.Text)
    aIntField.Longueur = CDbl(Me.Text2.Text)
End If

'-> Valeur par d�faut
aIntField.Defaut = Me.Text5.Text

'-> Si modif
aIntField.IsModif = CBool(Me.Check1.Value)

'-> Enregistrer
aIdent.ServeurDOG.Save_Schema

'-> Renvoyer une valeur de succ�s
strRetour = "OK"

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Command3_Click()
strRetour = ""
Unload Me
End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("FRMSCHEMA", "53")

'-> Libell�s
Me.Label1.Caption = GetMesseprog("DOG", "1")
Me.Frame2.Caption = GetMesseprog("FRMSCHEMA", "59")
Me.Label3.Caption = GetMesseprog("FRMSCHEMA", "54")
Me.Label4.Caption = GetMesseprog("FRMSCHEMA", "55")
Me.Frame3.Caption = GetMesseprog("FRMSCHEMA", "47")
Me.Label6.Caption = GetMesseprog("FRMSCHEMA", "48")
Me.Label5.Caption = GetMesseprog("FRMSCHEMA", "60")
Me.Frame4.Caption = GetMesseprog("FRMSCHEMA", "57")
Me.Frame6.Caption = GetMesseprog("FRMSCHEMA", "61")
Me.Check1.Caption = GetMesseprog("FRMSCHEMA", "62")
Me.Frame5.Caption = GetMesseprog("FRMSCHEMA", "58")

End Sub

Public Sub Init(aIntField As IntField, aInt As Interface)

'---> Cette proc�dure initialise les parap�tres du champ d'interface

Dim aEmilieObj As EmilieObj

'-> Pointer sur le champ
Set aEmilieObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & aIntField.Num_Field)
aEmilieObj.CodeLangue = OpeCodeLangue

'-> Positionner le code de l'interface
Set aInterface = aInt

'-> Code du champ
Me.Label2.Caption = aEmilieObj.Num_Field & " - " & aEmilieObj.Fill_Label
Me.Label2.Tag = aEmilieObj.Num_Field

'-> Position longueur
If aInt.UseSeparateur Then
    '-> Bloquer les zones position longueur
    Me.Frame2.Enabled = False
    Me.Label3.Enabled = False
    Me.Label4.Enabled = False
    '-> Setting du s�parateur
    Me.Text4.Text = aInt.Separateur
    '-> Setting de la position associ�e
    Me.Text3.Text = aIntField.Position
    '-> debloquer le label
    Me.Label5.Enabled = True
Else
    '-> Bloquer la frame du s�parateur
    Me.Frame3.Enabled = False
    Me.Label5.Enabled = False
    '-> D�bloquer la fralme de saisie de la position longueur
    Me.Frame2.Enabled = True
    Me.Label3.Enabled = True
    Me.Label4.Enabled = True
    '-> Setting des positions et longueur
    Me.Text1.Text = aIntField.Position
    Me.Text2.Text = aIntField.Longueur
End If 'Si on utilise un s�parateur

'-> Valeur par d�faut
Me.Text5.Text = aIntField.Defaut

'-> Modification
Me.Check1.Value = Abs(CInt(aIntField.IsModif))

'-> Table de correspondance


End Sub


Private Sub Text1_GotFocus()
SelectTxtBox Me.Text1
End Sub

Private Sub Text2_GotFocus()
SelectTxtBox Me.Text2
End Sub

Private Sub Text3_GotFocus()
SelectTxtBox Me.Text3
End Sub

Private Sub Text4_GotFocus()
SelectTxtBox Me.Text4
End Sub

Private Sub Text5_GotFocus()
SelectTxtBox Me.Text5
End Sub

