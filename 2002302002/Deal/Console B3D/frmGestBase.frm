VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmGestBase 
   Caption         =   "Form1"
   ClientHeight    =   4665
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4530
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   311
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   302
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   840
      Top             =   3600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestBase.frx":0000
            Key             =   "BASE"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3255
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   5741
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   9
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Width           =   38100
      EndProperty
   End
End
Attribute VB_Name = "frmGestBase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 116 Then Init
    
End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("CONSOLE", "23")

'-> Entètes de colonnes
Me.ListView1.ColumnHeaders(1).Text = GetMesseprog("CONSOLE", "22") 'Base
Me.ListView1.ColumnHeaders(2).Text = GetMesseprog("CONSOLE", "24") 'Serveur
Me.ListView1.ColumnHeaders(3).Text = GetMesseprog("CONSOLE", "25") 'Type
Me.ListView1.ColumnHeaders(4).Text = GetMesseprog("CONSOLE", "26") 'User
Me.ListView1.ColumnHeaders(5).Text = GetMesseprog("CONSOLE", "27") 'Pwd
Me.ListView1.ColumnHeaders(6).Text = GetMesseprog("CONSOLE", "28") 'Tempo
Me.ListView1.ColumnHeaders(7).Text = GetMesseprog("CONSOLE", "29") 'Num_Idx
Me.ListView1.ColumnHeaders(8).Text = GetMesseprog("CONSOLE", "30") 'IsRunning
Me.ListView1.ColumnHeaders(9).Text = GetMesseprog("CONSOLE", "31") 'Fichier

End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next
GetClientRect Me.hwnd, aRect
Me.ListView1.Left = 0
Me.ListView1.Top = 0
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom


End Sub

Public Sub Init()

'---> Cette Procédure charge la liste des bases

Dim aBase As clsBase
Dim X As ListItem

On Error GoTo GestError

'-> Bloquer la feuille
Me.Enabled = False
LockWindowUpdate Me.hwnd

'-> Vider
Me.ListView1.ListItems.Clear

'-> Afficher la liste des bases
For Each aBase In Bases
    '-> Ajouter un nouvel Listitem
    Set X = Me.ListView1.ListItems.Add(, "BASE|" & UCase$(Trim(aBase.Nom)), , , "BASE")
    '-> Propriétés
    'Code
    X.Text = aBase.Nom
    'Serveur
    X.SubItems(1) = aBase.Serveur
    'Type
    X.SubItems(2) = DeCrypt(GetIniFileValue("TYPE", aBase.TypeBase, ApplicationDirectory & "B3D\Param\TypeBase.ini"))
    'User
    X.SubItems(3) = aBase.User
    'Pwd
    X.SubItems(4) = aBase.Pwd
    'Tempo
    X.SubItems(5) = aBase.TempoLock
    'NumIDx
    X.SubItems(6) = aBase.NumIdx
    'IsRunning
    If aBase.IsRunning Then
        X.SubItems(7) = GetMesseprog("CONSOLE", "32")
    Else
        X.SubItems(7) = GetMesseprog("CONSOLE", "33")
    End If
    'Fichier
    X.SubItems(8) = aBase.Fichier
Next 'Pour toutes les bases

'-> Formatter le listview
FormatListView Me.ListView1

GestError:
    LockWindowUpdate 0
    Me.Enabled = True

End Sub

Private Sub ListView1_DblClick()

'-> Ne rien faire si pas de base séléectionnée
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Charger la feuille de la propriété
Load frmBase
'-> Initialiser
frmBase.Init Me.ListView1.SelectedItem.Key
'-> Afficher la feuille
frmBase.Show vbModal
'-> Réinitialiser
Me.Init

End Sub
