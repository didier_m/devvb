VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsIdent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'---> Module de classe d'un identifiant

Public Code As String '-> Code de l'ident
Public Libel As String '-> Libell� de l'ident
Public FichierCatalogue As String '-> Fichier ascii de load du catalogue
Public FichierSchema As String '-> Fichier ascii de liad du DF
Public FichierLink As String '->  Liens entre les deux fichiers
Public FichierMessprog As String '-> Catalogue des messages de cet ident
Public Limite_AgentB3D As Integer '-> Nombre de connexion maxi par ident
Public Base As String '-> Nom de la base affect�e � cet ident
Public IniFile As String '-> Emplacement du fichier ini associ� � cet ident
Public ServeurDOG As clsDOG '-> Pointeur vers le serveur DOG associ�
Public NbConnect As Long '-> Indique le nombre de connexions actuelle au serveur
Public hdlCom As Long '-> Handle de communication interne � la console
Public hdlProcess As Long '-> Handle du process windows affect�
Public OkServeurDOG As Boolean '-> Indique si le chargement c'est bien pass�

Public Function ConnectDOG() As Boolean

'---> Cette proc�dure charge le B3D

Set ServeurDOG = New clsDOG

'-> Charger le catalogue
If Not LoadCatalogueB3D(ServeurDOG, FichierCatalogue) Then Exit Function

'-> Charger le sch�ma
If Not LoadSchemaB3D(ServeurDOG, FichierSchema) Then Exit Function

'-> Charger le fichier des liens
If Not LoadLinkB3D(ServeurDOG, FichierLink) Then Exit Function

'-> Charger les tables syst�mes
If Not LoadSystemParam(ServeurDOG, IniFile, Code) Then Exit Function

'-> Positionner les variables de fichier
ServeurDOG.FichierCatalogue = Me.FichierCatalogue
ServeurDOG.FichierSchema = Me.FichierSchema
ServeurDOG.FichierLink = Me.FichierLink
ServeurDOG.FichierMessprog = Me.FichierMessprog

'-> Charger le nombre des objets dans le catalogue
ServeurDOG.CatalogueCount = CInt(DeCrypt(GetIniFileValue("PARAM", "EMILIE_COUNT", Me.IniFile)))

'-> Charger le nombre des tables
ServeurDOG.FnameCount = CInt(DeCrypt(GetIniFileValue("PARAM", "FNAME_COUNT", Me.IniFile)))

'-> Positionner son fichier Ini
ServeurDOG.IdentIniFile = Me.IniFile
ServeurDOG.Ident = Me.Code

'-> Indiquer que le chargement est OK
OkServeurDOG = True


End Function
