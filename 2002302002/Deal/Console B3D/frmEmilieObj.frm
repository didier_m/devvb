VERSION 5.00
Begin VB.Form frmEmilieObj 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   7695
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7815
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7695
   ScaleWidth      =   7815
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   7695
      Left            =   0
      TabIndex        =   21
      Top             =   0
      Width           =   7815
      Begin VB.CommandButton Command4 
         Height          =   615
         Left            =   4200
         Picture         =   "frmEmilieObj.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   6840
         Width           =   615
      End
      Begin VB.CommandButton Command3 
         Height          =   615
         Left            =   4920
         Picture         =   "frmEmilieObj.frx":0CCA
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   6840
         Width           =   615
      End
      Begin VB.CommandButton Command2 
         Height          =   615
         Left            =   5640
         Picture         =   "frmEmilieObj.frx":1994
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   6840
         Width           =   615
      End
      Begin VB.CommandButton Command1 
         Height          =   615
         Left            =   6360
         Picture         =   "frmEmilieObj.frx":265E
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   6840
         Width           =   615
      End
      Begin VB.ComboBox Combo3 
         Enabled         =   0   'False
         Height          =   315
         Left            =   3240
         TabIndex        =   16
         Top             =   6240
         Width           =   3735
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Left            =   360
         TabIndex        =   15
         Top             =   6360
         Width           =   2775
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   6600
         TabIndex        =   0
         Top             =   840
         Width           =   375
      End
      Begin VB.ComboBox Combo2 
         Height          =   315
         Left            =   3240
         TabIndex        =   12
         Top             =   5160
         Width           =   3735
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   9
         Left            =   3240
         TabIndex        =   11
         Top             =   4800
         Width           =   3735
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   8
         Left            =   3240
         TabIndex        =   10
         Top             =   4440
         Width           =   3735
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   7
         Left            =   3240
         TabIndex        =   9
         Top             =   4080
         Width           =   3735
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   6
         Left            =   3240
         TabIndex        =   8
         Top             =   3720
         Width           =   3735
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   5
         Left            =   3240
         TabIndex        =   7
         Top             =   3360
         Width           =   3735
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   4
         Left            =   3240
         TabIndex        =   6
         Top             =   3000
         Width           =   3735
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   3
         Left            =   3240
         TabIndex        =   5
         Top             =   2640
         Width           =   3735
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   2
         Left            =   3240
         TabIndex        =   4
         Top             =   2280
         Width           =   3735
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   1
         Left            =   3240
         TabIndex        =   3
         Top             =   1920
         Width           =   3735
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   0
         Left            =   3240
         TabIndex        =   2
         Top             =   1560
         Width           =   3735
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   3240
         TabIndex        =   1
         Top             =   1200
         Width           =   3735
      End
      Begin VB.Image Image4 
         Height          =   240
         Left            =   7440
         Picture         =   "frmEmilieObj.frx":3328
         Top             =   5880
         Width           =   240
      End
      Begin VB.Image Image3 
         Height          =   240
         Left            =   7440
         Picture         =   "frmEmilieObj.frx":3472
         Top             =   5520
         Width           =   240
      End
      Begin VB.Image Image2 
         Height          =   240
         Left            =   7080
         Picture         =   "frmEmilieObj.frx":35BC
         Top             =   5880
         Width           =   240
      End
      Begin VB.Label Label10 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   3240
         TabIndex        =   14
         Top             =   5880
         Width           =   3735
      End
      Begin VB.Image Image1 
         Height          =   240
         Left            =   7080
         Picture         =   "frmEmilieObj.frx":3946
         Top             =   5520
         Width           =   240
      End
      Begin VB.Label Label9 
         Caption         =   "Label9"
         Height          =   255
         Left            =   360
         TabIndex        =   38
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label8 
         Caption         =   "Label3"
         Height          =   255
         Left            =   360
         TabIndex        =   37
         Top             =   5895
         Width           =   2775
      End
      Begin VB.Label Label7 
         Caption         =   "Label1"
         Height          =   255
         Left            =   360
         TabIndex        =   36
         Top             =   5520
         Width           =   2295
      End
      Begin VB.Label Label6 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   3240
         TabIndex        =   13
         Top             =   5520
         Width           =   3735
      End
      Begin VB.Label Label5 
         Caption         =   "Label3"
         Height          =   255
         Left            =   360
         TabIndex        =   35
         Top             =   5175
         Width           =   2775
      End
      Begin VB.Label Label4 
         Caption         =   "Label3"
         Height          =   255
         Index           =   9
         Left            =   360
         TabIndex        =   34
         Top             =   4800
         Width           =   2775
      End
      Begin VB.Label Label4 
         Caption         =   "Label3"
         Height          =   255
         Index           =   8
         Left            =   360
         TabIndex        =   33
         Top             =   4440
         Width           =   2775
      End
      Begin VB.Label Label4 
         Caption         =   "Label3"
         Height          =   255
         Index           =   7
         Left            =   360
         TabIndex        =   32
         Top             =   4080
         Width           =   2775
      End
      Begin VB.Label Label4 
         Caption         =   "Label3"
         Height          =   255
         Index           =   6
         Left            =   360
         TabIndex        =   31
         Top             =   3720
         Width           =   2775
      End
      Begin VB.Label Label4 
         Caption         =   "Label3"
         Height          =   255
         Index           =   5
         Left            =   360
         TabIndex        =   30
         Top             =   3360
         Width           =   2775
      End
      Begin VB.Label Label4 
         Caption         =   "Label3"
         Height          =   255
         Index           =   4
         Left            =   360
         TabIndex        =   29
         Top             =   3000
         Width           =   2775
      End
      Begin VB.Label Label4 
         Caption         =   "Label3"
         Height          =   255
         Index           =   3
         Left            =   360
         TabIndex        =   28
         Top             =   2640
         Width           =   2775
      End
      Begin VB.Label Label4 
         Caption         =   "Label3"
         Height          =   255
         Index           =   2
         Left            =   360
         TabIndex        =   27
         Top             =   2280
         Width           =   2775
      End
      Begin VB.Label Label4 
         Caption         =   "Label3"
         Height          =   255
         Index           =   1
         Left            =   360
         TabIndex        =   26
         Top             =   1920
         Width           =   2775
      End
      Begin VB.Label Label4 
         Caption         =   "Label3"
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   25
         Top             =   1560
         Width           =   2775
      End
      Begin VB.Label Label3 
         Caption         =   "Label3"
         Height          =   255
         Left            =   360
         TabIndex        =   24
         Top             =   1200
         Width           =   2775
      End
      Begin VB.Label Label2 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   5280
         TabIndex        =   23
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   255
         Left            =   360
         TabIndex        =   22
         Top             =   480
         Width           =   2295
      End
   End
End
Attribute VB_Name = "frmEmilieObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Indique si le champ est en ajout ou en modif
Private IsModif As Boolean
'-> Pointeur vers l'ident associ�
Public aIdent As clsIdent
'-> Feuille maitresse
Public aFrm As frmDOG

Public Sub Init(DefObject As String)

Dim i As Integer
Dim aEmilieObj As Object
Dim Emilie2 As Object

'-> Frame
Me.Frame1.Caption = GetMesseprog("DOG", "15")

'-> Propri�t�s du champ
Me.Label1.Caption = GetMesseprog("FRMEMILIE", "3")
Me.Label3.Caption = GetMesseprog("FRMEMILIE", "4")

'-> Libell� des langues
For i = 1 To 10
    Me.Label4(i - 1).Caption = GetMesseprog("PAYS", CStr(i))
Next

'-> Groupe
Me.Label5.Caption = GetMesseprog("FRMEMILIE", "7") & ":"
'-> Table associ�e
Me.Label7.Caption = GetMesseprog("FRMEMILIE", "8") & ":"
'-> Champ maitre
Me.Label8.Caption = GetMesseprog("FRMEMILIE", "9")
'-> S�parateur
Me.Label9.Caption = GetMesseprog("FRMEMILIE", "6")
'-> Champ multi
Me.Check1.Caption = GetMesseprog("FRMEMILIE", "13")
''-> Ok
'Me.Command1.Caption = GetMesseprog("DOG", "14")
''-> Annuler
'Me.Command2.Caption = GetMesseprog("DOG", "15")
''-> Pr�sence
'Me.Command3.Caption = GetMesseprog("FRMEMILIE", "11")
''-> Liens
'Me.Command4.Caption = GetMesseprog("FRMEMILIE", "12")

'-> Charger la liste des natures
LoadNature
'-> Charger la liste des groupes d'affectation
LoadGroupe
'-> Charger la liste des codes langues
LoadPays

'-> Initialiser les propri�t�s de l'objet s�lectionn�
If Trim(DefObject) <> "" Then
    '-> Indiquer que l'on est rentr� en mode modification
    IsModif = True
    '-> Pointer sur l'objet sp�cifi�
    Set aEmilieObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & DefObject)
    '-> Num_Field
    Me.Label2.Caption = aEmilieObj.Num_Field
    '-> Code Langue
    If aEmilieObj.MultiLangue <> 0 Then
        '-> S�lectionner la checkbox
        Me.Check1.Value = 1
        Me.Combo3.ListIndex = aEmilieObj.MultiLangue - 1
    Else
        '-> D�selectionner la combo
        Me.Check1.Value = 0
    End If
    '-> Libell�
    For i = 1 To 10
        aEmilieObj.CodeLangue = i
        Me.Text1(i - 1) = aEmilieObj.Fill_Label
    Next
    '-> Groupe d'affectation
    For i = 0 To Me.Combo2.ListCount - 1
        If aEmilieObj.Progiciel = Me.Combo2.List(i) Then
            Me.Combo2.ListIndex = i
        End If
    Next
    '-> Table Associ�e
    If aEmilieObj.Table_Correspondance <> "" Then
        Me.Label6.Caption = aEmilieObj.Table_Correspondance & " - " & aIdent.ServeurDOG.fNames(aIdent.Code & "|" & aEmilieObj.Table_Correspondance).Designation
        Me.Label6.Tag = aEmilieObj.Table_Correspondance
    End If
    '-> Nature de l'objet
    For i = 0 To Me.Combo1.ListCount - 1
        If aEmilieObj.DataType = Me.Combo1.List(i) Then
            Me.Combo1.ListIndex = i
        End If
    Next
    '-> Champ maitre
    If aEmilieObj.MasterField <> "" Then
        '-> Pointer sur le champ
        Set Emilie2 = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & aEmilieObj.MasterField)
        Emilie2.CodeLangue = OpeCodeLangue
        Me.Label10.Caption = aEmilieObj.MasterField & " - " & Emilie2.Fill_Label
        Me.Label10.Tag = aEmilieObj.MasterField
    End If
    '-> S�parateur
    Me.Text2.Text = aEmilieObj.Sep_Vecteur
Else
    '-> On est en mode ajout
    IsModif = False
    '-> Demander au serveurDOG le nouveau code du champ
    Me.Label2 = aIdent.ServeurDOG.GetEmilieNumField
End If

'-> Titre de la feuille
If IsModif Then
    Me.Caption = GetMesseprog("FRMEMILIE", "2")
Else
    Me.Caption = GetMesseprog("FRMEMILIE", "1")
End If

End Sub

Private Sub LoadNature()

Dim lpBuffer As String
Dim DefNat As String
Dim i As Integer

'-> R�cup�rer le conteu de la section
lpBuffer = GetIniFileValue("NATURE", "", MessprogIniFile, True)

For i = 1 To NumEntries(lpBuffer, Chr(0))
    '-> R�cup�rer une d�finition
    DefNat = Entry(i, lpBuffer, Chr(0))
    If Trim(DefNat) = "" Then Exit Sub
    '-> Ajouter dans la liste
    Me.Combo1.AddItem Entry(2, DefNat, "=")
Next

End Sub

Private Sub LoadGroupe()

Dim lpBuffer As String
Dim DefNat As String
Dim i As Integer
Dim Temp As String

'-> R�cup�rer le contenu de la section
lpBuffer = GetIniFileValue("GROUPE", "", aIdent.IniFile, True)

For i = 1 To NumEntries(lpBuffer, Chr(0))
    '-> R�cup�rer une d�finition
    DefNat = Entry(i, lpBuffer, Chr(0))
    If Trim(DefNat) = "" Then Exit Sub
    Temp = DeCrypt(Entry(2, DefNat, "="))
    '-> Ajouter dans la liste
    Me.Combo2.AddItem DeCrypt(GetIniFileValue("GROUPE", Entry(1, DefNat, "="), aIdent.IniFile))
Next

End Sub

Private Sub LoadPays()

Dim i As Integer

'-> Charger les images de codes langues
For i = 1 To 10
    Me.Combo3.AddItem GetMesseprog("PAYS", CStr(i))
Next

End Sub

Private Sub Check1_Click()
If Me.Check1.Value = 1 Then
    Me.Combo3.Enabled = True
Else
    Me.Combo3.Enabled = False
    Me.Combo3.ListIndex = -1
End If
    
End Sub


Private Sub Command1_Click()

Dim aEmilieObj As Object
Dim i As Integer
Dim x As ListItem
Dim FindOne As Boolean

'-> V�rifier la nature de la zone
If Me.Combo1.ListIndex = -1 Then
    MsgBox GetMesseprog("APPLIC", "41"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    Me.Combo1.SetFocus
    Exit Sub
End If

'-> V�rifier Groupe d'affectation
If Me.Combo2.ListIndex = -1 Then
    MsgBox GetMesseprog("APPLIC", "42"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    Me.Combo2.SetFocus
    Exit Sub
End If

'-> Selon que l'on soit en modif ou non
If IsModif Then
    '-> Pointer sur l'objet EmilieObj attach�
    Set aEmilieObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & Me.Label2.Caption)
    '-> Pointer sur sa repr�sentation physique
    Set x = aFrm.ListView1.SelectedItem
Else
    '-> Ajouter un objet EmilieObj dans le serveur DOG et pointer dessus
    aIdent.ServeurDOG.AddEmilieObj Me.Label2.Caption
    Set aEmilieObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & Me.Label2.Caption)
    '-> Ajouter un objet ListItem dans la feuille du servuer B3D
    Set x = aFrm.ListView1.ListItems.Add(, "OBJ|" & Me.Label2.Caption, Me.Label2.Caption)
End If

'-> Mettre � jour son libell�
For i = 1 To 10
    '-> Positionner le code langue
    aEmilieObj.CodeLangue = i
    aEmilieObj.Fill_Label = Me.Text1(i - 1).Text
Next 'Pour tous les codes langues
x.SubItems(1) = Me.Text1(OpeCodeLangue - 1).Text

'-> Mettre � jour sa nature
aEmilieObj.DataType = Me.Combo1.Text
x.SubItems(2) = Me.Combo1.Text

'-> Mettre � jour son groupe d'affectattion
aEmilieObj.Progiciel = Me.Combo2.Text
x.SubItems(3) = Me.Combo2.Text

'-> Mettre � jour son code langue
If Me.Check1.Value = 0 Then
    '-> Pas de code langue
    aEmilieObj.MultiLangue = 0
    x.SubItems(5) = ""
Else
    '-> Mettre � jour la propri�t� Multilangue
    aEmilieObj.MultiLangue = Me.Combo3.ListIndex + 1
    x.SubItems(5) = Format(aEmilieObj.MultiLangue, "00")
End If

'-> Mettre � jour sa table de correspondance
If Me.Label6.Caption = "" Then
    aEmilieObj.Table_Correspondance = ""
    x.SubItems(4) = ""
Else
    aEmilieObj.Table_Correspondance = Me.Label6.Tag
    x.SubItems(4) = Me.Label6.Caption
End If
    
'-> Mettre � jour son champ maitre
If Me.Label10.Caption = "" Then
    aEmilieObj.MasterField = ""
Else
    aEmilieObj.MasterField = Me.Label10.Tag
End If
    
'-> Separateur de vecteur
aEmilieObj.Sep_Vecteur = Trim(Me.Text2.Text)
    
'-> Enregistrer le catalogue
aIdent.ServeurDOG.Save_Catalogue

'-> Si on est en ajout, vider toutes les zones et demander le prochain num�ro de champ
If Not IsModif Then
    '-> Vider toutes les zones
    Me.Combo2.Text = ""
    Me.Combo2.ListIndex = -1
    Me.Combo1.Text = ""
    Me.Combo1.ListIndex = -1
    For i = 1 To 10
        Me.Text1(i - 1) = ""
    Next
    Me.Label6.Tag = ""
    Me.Label6.Caption = ""
    Me.Label10.Caption = ""
    Me.Label10.Tag = ""
    Me.Text2.Text = ""
    '-> Get du num�ro suivant
    Me.Label2.Caption = aIdent.ServeurDOG.GetEmilieNumField
    aFrm.ListView1.Refresh
Else
    '-> Pour maj
    strRetour = "OK"
    '-> Fermer la feuille
    Unload Me
End If


End Sub

Private Sub Command2_Click()
'-> Annuler
strRetour = ""
Unload Me
End Sub

Private Sub Command3_Click()

'---> Affiche les enregistrements

Dim ValueToSearch As String '

'-> Demander la valeur � saisir
ValueToSearch = InputBox(GetMesseprog("APPLIC", "47"), GetMesseprog("APPLIC", "47"), "")
If Trim(ValueToSearch) = "" Then Exit Sub

'-> Lancer la recherche
GetPresence Me.Label2.Caption, ValueToSearch

End Sub


Public Sub GetPresence(Field As String, Rep As String)

Dim Ligne As String
Dim Param As String
Dim i As Long
Dim ValueField As String
Dim x As ListItem
Dim IsErrorLig As Boolean

'On Error GoTo GestError

'-> Lancer la requete de l'agent B3D

'-> Initialiser une demande
SendDemande "B3D_SEEK_DATA~" & Field & "�" & Trim(Rep)

'-> Charger la feuille
Load frmLiens

'-> Afficher la liste des tables
For i = 1 To Reponses.Count
    '-> Get de la r�ponse
    Ligne = Reponses(i)
    IsErrorLig = False
    '-> selon si ligne en erreur ou non
    If Entry(2, Ligne, "~") = "ERROR" Then
        '-> La ligne est en erreur : selon le code erreur
        Select Case Entry(3, Ligne, "~")
            Case "90016" 'Champ non affect�� une table
                '-> Afficher un message d'erreur
                MsgBox GetMesseprog("CONSOLE", "92"), vbExclamation + vbOKOnly, GetMesseprog("DOG", "17")
                GoTo GestError
            Case "90017" 'Valeur non trouv�e pour cette table
                IsErrorLig = True
                Param = Entry(5, Ligne, "~")
            Case Else 'Erreur non r�f�renc�e
                '-> Afficher un message d'erreur
                MsgBox "Error : " & Chr(13) & Entry(3, Ligne, "~") & " -> " & Entry(4, Ligne, "~"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
                GoTo GestError
            End Select
    Else
        Param = Entry(3, Ligne, "~")
    End If
    '-> Ajouter une ligne
    Set x = frmLiens.ListView1.ListItems.Add(, , "")
    '-> Positionner le code de la table
    x.Text = Entry(1, Entry(2, Param, "�"), "�")
    '-> Positionner le libell�
    x.SubItems(1) = Entry(2, Param, "�")
    '-> Valeur
    If IsErrorLig Then
        x.SubItems(2) = Entry(4, Ligne, "~")
        x.SmallIcon = "NON"
    Else
        x.SubItems(2) = GetMesseprog("CONSOLE", "93")
        x.SmallIcon = "OUI"
    End If
Next

'-> Formatter les entetes
FormatListView frmLiens.ListView1

'-> Afficher la feuille
Screen.MousePointer = 0

'-> Afficher la feuille
frmLiens.Caption = GetMesseprog("FRMEMILIE", "16") & " : " & Field & " - " & GetMesseprog("FRMEMILIE", "24") & " : " & Rep
frmLiens.Show vbModal


Exit Sub

GestError:
    Screen.MousePointer = 0
    Unload frmLiens

End Sub


Private Sub Command4_Click()

'---> Cette proc�dure affiche la liste des liens

Dim i As Long
Dim Ligne As String
Dim x As ListItem
Dim DefVal As String

On Error GoTo GestError

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> Poser une demande
SendDemande "B3D_CATALOGUE_LINK~"

'-> Charger la feuille des liens en m�moire
Load frmLiens

'-> Analyse de la r�ponse
For i = 1 To Reponses.Count
    '-> Get de la ligne
    DefVal = Entry(2, Reponses(i), "~")
    If Entry(1, DefVal, "�") <> Me.Label2.Caption Then GoTo NextLig
    '-> Cr�er une nouvelle icone
    Set x = frmLiens.ListView1.ListItems.Add()
    '-> Type de liens
    Select Case Entry(2, DefVal, "�")
        Case "LINK_FNAME"
            x.Text = GetMesseprog("DOG", "7")
        Case "LINK_INAME"
            x.Text = GetMesseprog("FRMSCHEMA", "9")
        Case "LINK_INTERFACE"
            x.Text = GetMesseprog("FRMSCHEMA", "37")
    End Select
    '-> Code
    x.SubItems(1) = Entry(3, DefVal, "�")
    '-> Lieblle
    x.SubItems(2) = Entry(4, DefVal, "�")
    '-> Icone
    x.SmallIcon = "OUI"
NextLig:
Next

'-> Positionner les messprogs
frmLiens.ListView1.ColumnHeaders(1).Text = GetMesseprog("CONSOLE", "25")
frmLiens.ListView1.ColumnHeaders(2).Text = GetMesseprog("CONSOLE", "17")
frmLiens.ListView1.ColumnHeaders(3).Text = GetMesseprog("DOG", "2")

'-> Formatter le listview
FormatListView frmLiens.ListView1

'-> Liberrer le pointeur
Me.Enabled = True
Screen.MousePointer = 0

'-> Afficher la feuille
frmLiens.Show vbModal

'-> Quitter
Exit Sub

GestError:
    Me.Enabled = True
    Screen.MousePointer = 0

End Sub






Private Sub Image1_Click()

'---> Afficher la liste des tables dans lequel le champ est r�f�renc�

Dim aFname As Object
Dim x As ListItem

'-> Charger la feuille
Load frmLiens

'-> Cr�er les ent�tes de colonnes
frmLiens.ListView1.ColumnHeaders.Clear
frmLiens.ListView1.ColumnHeaders.Add , , GetMesseprog("FRMEMILIE", "15")
frmLiens.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "2")

'-> Parcourir la liste des tables
For Each aFname In aIdent.ServeurDOG.fNames
    '-> Positionner le code langue
    aFname.CodeLangue = OpeCodeLangue
    '-> Afficher dans le browse
    Set x = frmLiens.ListView1.ListItems.Add(, "FNAME|" & aFname.Num_Fname, aFname.Num_Fname)
    x.SubItems(1) = aFname.Designation
Next

'-> Formatter les entetes de colonnes
FormatListView frmLiens.ListView1

'-> vider la variable d'�change
strRetour = ""

'-> Afficher la feuille
frmLiens.Caption = GetMesseprog("FRMEMILIE", "14")
frmLiens.Show vbModal

'-> Tester le retour
If strRetour = "" Then Exit Sub

'-> Afficher la table de corres
Me.Label6.Caption = Entry(1, strRetour, Chr(0))

'-> Mettre � jour sa propri�t� TAG
Me.Label6.Tag = Entry(2, strRetour, Chr(0))
strRetour = ""

End Sub

Private Sub Image2_Click()

'---> Cette proc�dure affiche le catalogue

Dim aEmilieObj As Object
Dim x As ListItem
Dim aFname As Object
Dim i As Long

'-> Visu Ecran
Me.Enabled = False
Screen.MousePointer = 11

'-> Charger la feuille
Load frmLiens

'-> Charger le catalogue
For Each aEmilieObj In aIdent.ServeurDOG.Catalogue
    '-> Tester que l'on n'ajoute pas le champ en cours
    If aEmilieObj.Num_Field <> Me.Label6.Caption Then
        '-> Positionner le code langue
        aEmilieObj.CodeLangue = OpeCodeLangue
        '-> Ajouter dans le listeView
        Set x = frmLiens.ListView1.ListItems.Add(, "FIELD|" & aEmilieObj.Num_Field, aEmilieObj.Num_Field)
        '-> Libell�
        x.SubItems(1) = aEmilieObj.Fill_Label
        '-> Format
        x.SubItems(2) = aEmilieObj.DataType
        '-> Table de correspondance
        If aEmilieObj.Table_Correspondance <> "" Then
            '-> Pointer sur la table
            Set aFname = aIdent.ServeurDOG.fNames(aIdent.Code & "|" & aEmilieObj.Table_Correspondance)
            aFname.CodeLangue = OpeCodeLangue
            x.SubItems(3) = aEmilieObj.Table_Correspondance & " - " & aFname.Designation
        End If
        '-> Groupe
        x.SubItems(4) = aEmilieObj.Progiciel
    End If
Next

FormatListView frmLiens.ListView1

'-> Afficher la feuille
strRetour = ""
Screen.MousePointer = 0
frmLiens.Show vbModal

'-> Tester la variable de retour
Me.Label10.Caption = strRetour
Me.Label10.Tag = Entry(2, strRetour, Chr(0))

'-> Rendre la feuille OK
Me.Enabled = True

End Sub

Private Sub Image3_Click()
Me.Label6.Caption = ""
Me.Label6.Tag = ""
End Sub

Private Sub Image4_Click()
Me.Label10.Caption = ""
Me.Label10.Tag = ""
End Sub

Private Sub Text1_GotFocus(Index As Integer)
SelectTxtBox Me.Text1(Index)
End Sub
Private Sub Text2_GotFocus()
SelectTxtBox Me.Text2
End Sub
