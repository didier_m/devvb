VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsBase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Nom As String
Public TypeBase As String
Public Pwd As String
Public User As String
Public NumIdx As Integer
Public TempoLock As Long
Public Fichier As String
Public Serveur As String
Public Idx As Collection
Public IsRunning As Boolean
Public When As Date
Public NbConnect As Integer

Private Sub Class_Initialize()
Set Idx = New Collection
End Sub

Private Sub Class_Terminate()
Set Idx = Nothing
End Sub
Public Function Get_IDX() As String

'---> Cette fonction retourne l'agent IDX qui a le moins de charge pour cette base de donn�es

Dim aIdx As clsIdx
Dim List_Idx As String
Dim List_Idx_Tri  As String
Dim RowId As Integer
            
For Each aIdx In Idx
    '-> Le nombre n'est pas atteint : ajouter dans la liste
    If List_Idx = "" Then
        List_Idx = aIdx.NbAgent & "�" & aIdx.Num_Idx
    Else
        List_Idx = List_Idx & "|" & aIdx.NbAgent & "�" & aIdx.Num_Idx
    End If
Next 'Pour tous les agents
    
'-> Renvoyer un num � "" si pas d'IDX
If Trim(List_Idx) = "" Then
    Get_IDX = ""
    Exit Function
End If
    
'-> Trier la liste des agents IDX par ordre d'affectation
List_Idx_Tri = Tri(List_Idx, "|", "�")
    
'-> Pointer sur l'agent IDX
Set aIdx = Idx("IDX|" & Entry(1, List_Idx_Tri, "|"))
      
'-> Renvoyer le code de l'idx s�lectionn�
Get_IDX = aIdx.Num_Idx
      
End Function




