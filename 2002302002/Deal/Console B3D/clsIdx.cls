VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsIdx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Num_Idx As String '-> Num�ro d'identification de l'agent IDX
Public Hdl_Retour_Com As Long '-> Handle de communication de l'idx
Public hdlProcess As Long '-> PID du prog
Public EndIdx As Boolean  '-> Indique si l'agent IDX est mort
Public NbAgent As Integer '-> Nombre d'agent B3D affect� � cet IDX
Public IsWorking As Boolean  '-> Indique s'il est en cours de travail
