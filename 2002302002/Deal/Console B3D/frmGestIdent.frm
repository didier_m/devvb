VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmGestIdent 
   Caption         =   "Form1"
   ClientHeight    =   7140
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10575
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   476
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   705
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7440
      Top             =   4320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestIdent.frx":0000
            Key             =   "CLIENT"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3375
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   5953
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Width           =   38100
      EndProperty
   End
End
Attribute VB_Name = "frmGestIdent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 116 Then Init
    
End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("APPLIC", "32")

'-> Ent�tes de colonnes
Me.ListView1.ColumnHeaders(1).Text = GetMesseprog("CONSOLE", "17") 'Code
Me.ListView1.ColumnHeaders(2).Text = GetMesseprog("APPLIC", "10") 'Libel
Me.ListView1.ColumnHeaders(3).Text = GetMesseprog("CONSOLE", "18") 'NbMax
Me.ListView1.ColumnHeaders(4).Text = GetMesseprog("CONSOLE", "19") 'NbCours
Me.ListView1.ColumnHeaders(5).Text = GetMesseprog("CONSOLE", "22") 'Base


End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next
GetClientRect Me.hwnd, aRect
Me.ListView1.Left = 0
Me.ListView1.Top = 0
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom


End Sub

Public Sub Init()

'---> Cette Proc�dure charge la liste des identifiants

Dim aIdent As clsIdent
Dim X As ListItem

On Error GoTo GestError

'-> Bloquer
Me.Enabled = False
LockWindowUpdate Me.hwnd

'-> Vider
Me.ListView1.ListItems.Clear

'-> Afficher la liste des idents
For Each aIdent In Idents
    '-> Afficher l'ident sp�cifi�
    Set X = Me.ListView1.ListItems.Add(, "IDENT|" & Trim(UCase$(aIdent.Code)), aIdent.Code, , "CLIENT")
    '-> Libell�
    X.SubItems(1) = aIdent.Libel
    '-> Max
    X.SubItems(2) = aIdent.Limite_AgentB3D
    '-> En cours
    X.SubItems(3) = aIdent.NbConnect
    '-> Base affect�e
    X.SubItems(4) = aIdent.Base
Next 'Pour tous les identifiants

'-> Formatter le listview
FormatListView Me.ListView1

GestError:
    LockWindowUpdate 0
    Me.Enabled = True
    

End Sub

