VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmBase 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   8040
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6045
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8040
   ScaleWidth      =   6045
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   480
      Top             =   5160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBase.frx":0000
            Key             =   "IDX"
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Command1 
      Height          =   1335
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   3000
      Width           =   1095
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   5175
      Left            =   1320
      TabIndex        =   14
      Top             =   2880
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   9128
      View            =   3
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "    "
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label Label16 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   1320
      TabIndex        =   17
      Top             =   2040
      Width           =   4695
   End
   Begin VB.Label Label15 
      Caption         =   "Label15"
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   2040
      Width           =   855
   End
   Begin VB.Image RunBase 
      Height          =   720
      Left            =   240
      Picture         =   "frmBase.frx":0CDA
      Top             =   3720
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image StopBase 
      Height          =   720
      Left            =   360
      Picture         =   "frmBase.frx":29A4
      Top             =   4080
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Label Label14 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   4320
      TabIndex        =   13
      Top             =   1680
      Width           =   1695
   End
   Begin VB.Label Label13 
      Caption         =   "Etat : "
      Height          =   255
      Left            =   3240
      TabIndex        =   12
      Top             =   1680
      Width           =   975
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1320
      TabIndex        =   11
      Top             =   1680
      Width           =   1695
   End
   Begin VB.Label Label11 
      Caption         =   "Idx :"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   1680
      Width           =   975
   End
   Begin VB.Label Label10 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   4320
      TabIndex        =   9
      Top             =   1320
      Width           =   1695
   End
   Begin VB.Label Label9 
      Caption         =   "Password : "
      Height          =   255
      Left            =   3240
      TabIndex        =   8
      Top             =   1320
      Width           =   975
   End
   Begin VB.Label Label8 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1320
      TabIndex        =   7
      Top             =   1320
      Width           =   1695
   End
   Begin VB.Label Label7 
      Caption         =   "User : "
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   1320
      Width           =   975
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1320
      TabIndex        =   5
      Top             =   960
      Width           =   4695
   End
   Begin VB.Label Label5 
      Caption         =   "Serveur : "
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   960
      Width           =   1095
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1320
      TabIndex        =   3
      Top             =   600
      Width           =   4695
   End
   Begin VB.Label Label3 
      Caption         =   "Type : "
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1320
      TabIndex        =   1
      Top             =   240
      Width           =   4695
   End
   Begin VB.Label Label1 
      Caption         =   "Base : "
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   735
   End
End
Attribute VB_Name = "frmBase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim aBase As clsBase

Private Sub Command1_Click()

'---> Lancer ou arreter le serveur dog

Dim Rep As VbMsgBoxResult
Dim aIdx As clsIdx
Dim i As Integer
Dim x As ListItem

'-> Bloquer l'�cran
Me.Enabled = True
Screen.MousePointer = 11

If aBase.IsRunning Then
    '-> Demande d'arret du serveur
    Rep = MsgBox(GetMesseprog("APPLIC", "35") & Chr(13) & aBase.Nom, vbQuestion + vbYesNo, GetMesseprog("APPLIC", "21"))
    If Rep = vbNo Then GoTo EndSub
    
    '-> V�rifier qu'il n'y ait pas d'IDX en cours de travail
    For Each aIdx In aBase.Idx
        '-> Tester qu'il n'y en ait pas en cours de travail
        If aIdx.IsWorking Then
            MsgBox GetMesseprog("APPLIC", "36"), vbExclamation + vbOKOnly, GetMesseprog("CONSOLE", "23")
            GoTo EndSub
        End If
    Next

    '-> Arreter les idents
    For Each aIdx In aBase.Idx
        '-> Envoyer un message de demande de fin de traitement
        UnloadIDX aIdx, aBase
    Next

    '-> Indiquer que la base ne tourne plus
    aBase.IsRunning = False
    aBase.When = Now

    '-> Supprimer les objets du ListView
    Do While Me.ListView1.ListItems.Count <> 0
        Me.ListView1.ListItems.Remove (1)
    Loop
        
    '-> Icone
    Set Me.Command1.Picture = Me.RunBase.Picture
    
    '-> Libelle
    Me.Label14.Caption = GetMesseprog("CONSOLE", "33")
    
    '-> Mide � jour de l'�cran s'il est actif
    If IsWindow(hdlServBases) = 1 Then frmGestBase.Init
    
Else
    '-> Demande de d�marrage
    Rep = MsgBox(GetMesseprog("APPLIC", "34") & Chr(13) & aBase.Nom, vbQuestion + vbYesNo, GetMesseprog("APPLIC", "21"))
    If Rep = vbNo Then GoTo EndSub
                        
    '-> Pour tous les IDX
    For i = 1 To aBase.NumIdx
        '-> lancer les IDX
        Set aIdx = IsIDX(aBase, False)
        '-> Rendre la main � la CPU
        DoEvents
        '-> Ajouter
        Set x = Me.ListView1.ListItems.Add(, "IDX|" & aIdx.Num_Idx, aIdx.Num_Idx, , "IDX")
        x.SubItems(1) = aIdx.Hdl_Retour_Com
        x.SubItems(2) = aIdx.hdlProcess
        x.SubItems(3) = aIdx.NbAgent
    Next
                                
    '-> Indiquer que la base est lanc�e
    aBase.IsRunning = True
    aBase.When = Now
                                        
    '-> Icone
    Set Me.Command1.Picture = Me.StopBase.Picture
    
    '-> Libelle
    Me.Label14.Caption = GetMesseprog("CONSOLE", "32")
    
    '-> Mide � jour de l'�cran s'il est actif
    If IsWindow(hdlServBases) = 1 Then frmGestBase.Init
    
End If

'-> Formtter le listview
FormatListView Me.ListView1

EndSub:

    Me.Enabled = True
    Screen.MousePointer = 0

End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("APPLIC", "33")

'-> Libell�s des captions
Me.Label1.Caption = GetMesseprog("CONSOLE", "22")
Me.Label3.Caption = GetMesseprog("CONSOLE", "25")
Me.Label5.Caption = GetMesseprog("CONSOLE", "24")
Me.Label7.Caption = GetMesseprog("CONSOLE", "26")
Me.Label9.Caption = GetMesseprog("CONSOLE", "27")
Me.Label11.Caption = GetMesseprog("CONSOLE", "29")
Me.Label13.Caption = GetMesseprog("CONSOLE", "30")
Me.Label15.Caption = GetMesseprog("CONSOLE", "31")

'-> Libell�s des colonnes
Me.ListView1.ColumnHeaders(1).Text = GetMesseprog("CONSOLE", "34")
Me.ListView1.ColumnHeaders(2).Text = GetMesseprog("CONSOLE", "20")
Me.ListView1.ColumnHeaders(3).Text = GetMesseprog("CONSOLE", "21")
Me.ListView1.ColumnHeaders(4).Text = GetMesseprog("CONSOLE", "35")


End Sub

Public Sub Init(strBase As String)

'---> Cette proc�dure affciche les propri�t�sd de la base en cours

Dim aIdx As clsIdx
Dim x As ListItem

'-> Pointer sur la base
Set aBase = Bases(strBase)

'-> Prop
Me.Label2.Caption = " " & aBase.Nom
Me.Label4.Caption = " " & DeCrypt(GetIniFileValue("TYPE", aBase.TypeBase, ApplicationDirectory & "B3D\Param\TypeBase.ini"))
Me.Label6.Caption = " " & aBase.Serveur
Me.Label8.Caption = " " & aBase.User
Me.Label10.Caption = " " & aBase.Pwd
Me.Label12.Caption = aBase.NumIdx
Me.Label16.Caption = " " & aBase.Fichier
If aBase.IsRunning Then
    Me.Label14.Caption = GetMesseprog("CONSOLE", "32")
    Set Me.Command1.Picture = Me.StopBase.Picture
Else
    Me.Label14.Caption = GetMesseprog("CONSOLE", "33")
    Set Me.Command1.Picture = Me.RunBase.Picture
End If

'-> Afficher les constructeurs d'index
For Each aIdx In aBase.Idx
    '-> Ajouter une ligne
    Set x = Me.ListView1.ListItems.Add(, "IDX|" & aIdx.Num_Idx, aIdx.Num_Idx, , "IDX")
    x.SubItems(1) = aIdx.Hdl_Retour_Com
    x.SubItems(2) = aIdx.hdlProcess
    x.SubItems(3) = aIdx.NbAgent
Next 'Pour tous les idx

'-> Formatter le listeview
FormatListView Me.ListView1

End Sub


