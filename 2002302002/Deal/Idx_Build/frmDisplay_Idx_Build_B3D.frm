VERSION 5.00
Begin VB.Form frmDisplay 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Idx_Build_B3D"
   ClientHeight    =   975
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   1200
   Icon            =   "frmDisplay_Idx_Build_B3D.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   975
   ScaleWidth      =   1200
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Left            =   240
      Top             =   240
   End
End
Attribute VB_Name = "frmDisplay"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Compare Text

Dim Num_Enreg As String
Dim Id_Fname As String
Dim Fonction As String
Dim Top_Trt As Integer
Dim Top_Maj As Integer
Dim Tot_Data As Double
Dim Tot_Index As Double
Dim Fname As String
Dim aFname As Fname
Dim aEmilieObj As EmilieObj

Dim ErrorCode As String


Private Sub Form_Paint()

Dim res As Long
Dim lpBuffer As String

'-> Init du buffer
lpBuffer = Space$(200)

'-> Indiquer ici la demande de fin de session
res = GetWindowText(Me.hwnd, lpBuffer, Len(lpBuffer))
lpBuffer = UCase$(Trim(Entry(1, lpBuffer, Chr(0))))

Select Case lpBuffer

    Case "END_SESSION"
        WaitForClosing = True
        DoEvents
End Select


End Sub

Private Sub EndSession()

Dim aFname As Fname
Dim aRec As Object

On Error GoTo GestError

'-> Mettre � jour les stats dans DEALB3D_ENREG
Call PrintDebug("Mise � jour des stats")
'-> Tester si on a connecter un DOG
If ServeurDOG Is Nothing Then GoTo NextDog
For Each aFname In ServeurDOG.fNames
    '-> Tester si stat
    If aFname.Fonction_MAJ Then
        '-> Composer la cl� d'acces
        Id_Fname = UCase$(Trim(aFname.Ident)) & "�" & UCase$(Trim(aFname.Num_Fname))
        '-> Tester si la stat exist pour cr�ation ou maj
        SQLValue = "SELECT * FROM [DEALB3D_ENREG] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [DAT_HISTO] = '" & GetDate & "'"
        '-> Mode debug
        Call PrintDebug("Mise � jour des stats proc�dure Unload : " & Chr(13) & Chr(10) & SQLValue)
        Set aRec = SetTable(SQLValue)
        If aRec.EOF Then
            SQLValue = "INSERT INTO [DEALB3D_ENREG] ( ID_FNAME , DAT_HISTO , NB_CREATE  , NB_UPDATE  , NB_DELETE , NUM_ENREG , NUM_LOCK) VALUES " & _
                                                   "('" & Trim(Id_Fname) & "' , '" & Trim(GetDate) & "' , '" & Trim(CStr(aFname.Fonction_Create)) & "' , '" & Trim(CStr(aFname.Fonction_Update)) & "' , '" & Trim(CStr(aFname.Fonction_Delete)) & "' , '','' )"
            '-> Mode debug
            Call PrintDebug("Cr�taion des stats proc�dure unload � " & Now & Chr(13) & Chr(10) & SQLValue)
            
            '-> Il n'existe pas le cr�er
            CreateEnreg SQLValue
        Else
            SQLValue = "UPDATE [DEALB3D_ENREG] SET " & _
                "[NB_CREATE] = '" & CStr(CLng(Trim(aFname.Fonction_Create)) + CLng(Trim(aRec.Fields("NB_CREATE")))) & "' , " & _
                "[NB_UPDATE] = '" & CStr(CLng(Trim(aFname.Fonction_Update)) + CLng(Trim(aRec.Fields("NB_UPDATE")))) & "' , " & _
                "[NB_DELETE} = '" & CStr(CLng(Trim(aFname.Fonction_Delete)) + CLng(Trim(aRec.Fields("NB_DELETE")))) & "' " & _
                "WHERE [ID_FNAME] = '" & Id_Fname & "' AND [DAT_HISTO] = '" & GetDate & "'"
        
            '-> Mode debug
            Call PrintDebug("Stat existe d�ja proc�dure unload update � : " & Now & Chr(13) & Chr(10) & SQLValue)
            CreateEnreg SQLValue
        End If
        aRec.Close
        Set aRec = Nothing
    End If
Next 'Pour toutes les tables

NextDog:

'-> Fermer les connexions
Call PrintDebug("Fermeture de la base")
CloseConnectB3D

'-> Fermer le lien vers le serveurDOG
Call PrintDebug("Fermeture du serveur DOG")

'-> Envoyer un message de fin
SetWindowText HdlCom, "IDX_END_SESSION|0|" & NomBase
SendMessage HdlCom, WM_PAINT, 0, 0
DoEvents
    
'-> Fin du programme
Call PrintDebug("Fin du programme")
End

Exit Sub

GestError:
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure Endsession :" & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)
    '-> Envoyer un message de fin
    SetWindowText HdlCom, "IDX_END_SESSION|0|" & NomBase
    SendMessage HdlCom, WM_PAINT, 0, 0
    DoEvents
    '-> Quitter
    End

End Sub


Public Sub Exec_Num_Lock(ByRef Num_Lock As String, ByRef Top_Wait As String, ByVal Id_Session As String)

Dim aRec As Object

On Error GoTo GestError

'-> Pointer sur l'enregistrement du lock
Set aRec = SetTable("SELECT * FROM [DEALB3D_LOCK] WHERE [NUM_LOCK] = '" & Num_Lock & "'")

'-> Traiter le lock si on l'a trouv�
If Not aRec.EOF Then
    
    Do While Not aRec.EOF

        '-> R�cup�rer les data
        Id_Fname = Trim(aRec.Fields("ID_FNAME"))
        Num_Enreg = Trim(aRec.Fields("NUM_ENREG"))
        Fonction = Trim(aRec.Fields("FONCTION"))
    
        
        aRec.Fields("NUM_LOCK") = "999999999999999"
    
        '-> Lancer le traitement du lock
        Call Traitement
    
        '-> Delete du Lock
        aRec.Delete
        
        '-> Pointer sur le lock suivant
        aRec.MoveNext
        
    Loop

End If

'-> Fermer le recordset
aRec.Close

Exit Sub

GestError:

    '-> Mode debug
    Call PrintDebug("Exec_Num_Lock :  Erreur dans la proc�dure " & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)
    If GetConflitVecteurLock(Err.Number) Then
        Resume
    End If

End Sub

Public Sub AnalyseBase()

'---> Analyse de DEALB3D_LOCK pour trouver les enregistrements � traiter ( Dat_RELEASE <> "")


Dim Rec_Lock As Object
Dim aFname As Fname
Dim res As Long
Dim Current_Num_Enreg As String

On Error GoTo GestError

ErrorCode = "ANALYSE_BASE : Recherche du premier lock � traiter"

SelectSQL:
'-> Cr�ation de la requete SQL d'analyse des LOCKS
SQLValue = "SELECT * FROM [DEALB3D_LOCK] WHERE [DAT_RELEASE] <> '' AND [NUM_LOCK] = 'AGENTIDX_" & IdIdx & "' ORDER BY [DAT_LOCK],[HEU_LOCK] ,[NUM_ENREG] , [DAT_RELEASE] , [HEU_RELEASE]"

'-> Ex�cuter la requete
Set Rec_Lock = SetTable(SQLValue)
                            
'-> Analyse du recordset
Do While Not Rec_Lock.EOF
    
    '-> Debug de temps
    Call PrintDebug("Prise du lock: " & GetTickCount)
    
    '-> Heure du lock
    Call PrintDebug("Date de realse du lock par l'agent B3D : " & Rec_Lock.Fields("DAT_HEU_MAJ") & " Pour l'enreg " & Rec_Lock.Fields("NUM_ENREG"))
    
    '-> Indiquer que l'idx est en train de tourner � la console
    SetWindowText HdlCom, "IDX_START_WORKING|0|" & NomBase
    SendMessage HdlCom, WM_PAINT, 0, 0
    
    '-> Rendre la main � la cpu
    DoEvents
                                
    '-> Recup num d'enregistrement
    Id_Fname = Trim(Rec_Lock.Fields("ID_FNAME"))
    Num_Enreg = Trim(Rec_Lock.Fields("NUM_ENREG"))
    Fonction = Trim(Rec_Lock.Fields("FONCTION"))
    Programme = Trim(Rec_Lock.Fields("PRG_MAJ"))
    Operateur = Trim(Rec_Lock.Fields("OPE_MAJ"))
    
    '-> R�cup�ration du nom de l'ident
    Ident = Entry(1, Id_Fname, "�")

    '-> Essayer de connecter le servuer DOG li� � cet ident
    If Not ConnectDOG(Ident) Then
        '-> Mode debug
        Call PrintDebug("Proc�dure Analyse Base : Impossible de traiter le lock, ServeurDOG non connect�")
        '-> Analyser le lock suivant
        GoTo NextLock
    End If
    
    '-> Num_Lock
    SQLValue = "UPDATE [DEALB3D_LOCK] SET [NUM_LOCK] =  '999999999999999' , [HDL_LOCK] =  '" & Me.hwnd & "' " & _
                "WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & Num_Enreg & "'"
                
    '-> Mode debug
    Call PrintDebug("Proc�dure Analyse Base - Update sur le lock � :  " & GetTickCount & Chr(13) & Chr(10) & SQLValue)
    
    '-> Ex�cuter la requete
    CreateEnreg SQLValue
    
    '-> Mode debug
    Call PrintDebug("Proc�dure Analyse Base - Apr�s l'update : " & GetTickCount)
        
    '-> Lancer le traitement du lock
    Call Traitement
         
    '-> Mode debug
    Call PrintDebug("Proc�dure Analyse Base - Delete du lock pour le numenreg " & Num_Enreg)
    
    '-> Delete du Lock
    Debug.Print Rec_Lock.Fields("NUM_ENREG") & " - " & Rec_Lock.Fields("ID_FNAME")
    Rec_Lock.Delete
            
    '-> Pointer sur l'objet du catalogue
    Id_Fname = Replace(Id_Fname, "�", "|")
    Set aFname = ServeurDOG.fNames(Id_Fname)

    '-> Gestion des stats de l'index
    Select Case Fonction
        Case "CREATE"
            NbCreate = NbCreate + 1
        Case "UPDATE"
            NbUpdate = NbUpdate + 1
        Case "DELETE"
            NbDelete = NbDelete + 1
    End Select

    If aFname.IsStat Then
        '-> Mise � jour des stats
        Select Case Fonction
            Case "CREATE"
                aFname.Fonction_Create = aFname.Fonction_Create + 1
            Case "UPDATE"
                aFname.Fonction_Update = aFname.Fonction_Update + 1
            Case "DELETE"
                aFname.Fonction_Delete = aFname.Fonction_Delete + 1
        End Select

        '-> Indiquer que l'on doit cr�er la stat en partant
        aFname.Fonction_MAJ = True
    End If
        
    Set aFname = Nothing '-> Lib�rer le pointeur sur la table
            
NextLock:
            
    '-> Attaquer le lock suivant
    Rec_Lock.MoveNext

    '-> Indiquer que l'idx n'est plus en train de tourner
    SetWindowText HdlCom, "IDX_STOP_WORKING|0|" & NomBase
    SendMessage HdlCom, WM_PAINT, 0, 0
    
    '-> Rendre la main � la cpu
    DoEvents
Loop
              
'-> Fermer le query
If Not Rec_Lock Is Nothing Then Rec_Lock.Close

'-> V�rifier si on doit quitter le prog ou non
If WaitForClosing Then
    Call PrintDebug("Fin de traitement")
    '-> D�charger la feuille
    EndSession
    '-> Fin du programme
    End
End If

DoEvents

Exit Sub

GestError:

    '-> Gestion des conflits de base
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
            
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure AnalyseBase : " & Chr(13) & Chr(10) & Err.Number & " - " & Err.Description & Chr(13) & Chr(10) & ErrorCode)
        
    '-> Indiquer que l'idx n'est plus en train de tourner
    SetWindowText HdlCom, "IDX_STOP_WORKING|0|" & NomBase
    SendMessage HdlCom, WM_PAINT, 0, 0
    DoEvents
    

End Sub

Private Sub Traitement()


Dim aField As FIELD
Dim Rec_Data As Object
Dim Rec_Rcopy As Object
Dim Rec_Histo As Object
Dim Rec_Index As Object
Dim MatriceSql() As String
Dim iMatrice As Integer
Dim strEnregJoin As String


On Error GoTo GestError

'-> Eclater les valeurs
Ident = Trim(Entry(1, Id_Fname, "�"))
Fname = Trim(Entry(2, Id_Fname, "�"))


'-> Pointer sur le fname
Set aFname = ServeurDOG.fNames(UCase$(Ident) & "|" & UCase$(Fname))

'-> Test s'il y a un index direct car sinon il faut supprimer tous les DEALB3D_DATA, DEALB3D_INDEX et faire - dans _
les cumuls pour tous les champs des index � cumuls

If aFname.IndexDirect = "" Then
    '-> Selon la fonction
    If UCase$(Fonction) <> "CREATE" And UCase$(Fonction) <> "INDEX" Then
        '-> Pour soustraire les montants dans les cumuls
        Top_Maj = -1
        '-> lancer la maj
        Call MiseAJour
    End If 'si <> create
    
    If UCase$(Fonction) <> "DELETE" Then
        '-> Pour additionner les montants dans les cumuls
        Top_Maj = 1
        '-> lancer la maj
        Call MiseAJour
    End If 'si <> create
End If 'Si index direct

'-> Si Table suivie en r�plication
If Fonction <> "INDEX" Then
    If aFname.IsReplica Then
        
        '-> Cr�er l'enregistrement s'il n'existe pas
        ErrorCode = "Traitement :  Table suivie en r�plication. Recherche de l'ordre de r�plication."
        SQLValue = "SELECT * FROM [DEALB3D_RCOPY] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & Num_Enreg & "'"
        Set Rec_Rcopy = SetTable(SQLValue)
        If Not Rec_Rcopy.EOF Then
            '-> Il existe le mettre � jour
            ErrorCode = "Traitement : Mise � jour de l'ordre de r�plication"
            SQLValue = "UPDATE [DEALB3D_RCOPY] SET [FONCTION] = '" & Fonction & "' , [PRG_MAJ] = '" & Programme & "' , " & _
                        "[OPE_MAJ] = '" & Operateur & "' , [DAT_HEU_MAJ] = '" & Get_Total_Time & "' " & _
                        "WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & Num_Enreg & "'"
            CreateEnreg SQLValue
                       
        Else
            '-> Il n'existe pas : cr�er l'enregistrement
            ErrorCode = "Traitement : Cr�ation de l'ordre de r�plication"
            SQLValue = "INSERT INTO [DEALB3D_RCOPY] (ID_FNAME , NUM_ENREG , FONCTION , PRG_MAJ , OPE_MAJ , DAT_HEU_MAJ ) VALUES " & _
                        "('" & Id_Fname & "','" & Num_Enreg & "','" & Fonction & "','" & Programme & "','" & Operateur & "','" & Get_Total_Time & "')"
            CreateEnreg SQLValue
        End If
        '-> Fermer le recordset
        Rec_Rcopy.Close
    End If
End If

'-> Traitement des DEALB3D_DATA de l'enregistrement pour delete et creation DEALB3D_HISTO
Select Case Trim(UCase$(Fonction))

    Case "DELETE"
    
        ErrorCode = "Traitement DELETE : Recherche des data pour mise � jour de l'historique"
        SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & Num_Enreg & "'"
        Set Rec_Data = SetTable(SQLValue)
        If Not Rec_Data.EOF Then
            Do While Not Rec_Data.EOF
                ErrorCode = "Traitement DELETE : cr�ation de l'historique"
                SQLValue = "INSERT INTO [DEALB3D_HISTO] ( ID_FNAME , FIELD , NUM_ENREG , ZON_DATA , " & _
                            "FONCTION , DAT_HISTO , HEU_HISTO , PRG_MAJ , OPE_MAJ , DAT_HEU_MAJ , ENREG_JOIN , EXTENT ) " & _
                            "VALUES " & _
                            "('" & Trim(Rec_Data.Fields("ID_FNAME")) & "','" & Trim(Rec_Data.Fields("FIELD")) & "','" & _
                            Trim(Rec_Data.Fields("NUM_ENREG")) & "','" & Rec_Data.Fields("ZON_DATA") & "', '" & Fonction & "','" & _
                            GetDate & "','" & GetTime & "','" & Rec_Data.Fields("PRG_MAJ") & "','" & Rec_Data.Fields("OPE_MAJ") & "','" & Rec_Data.Fields("DAT_HEU_MAJ") & "' , '" & Rec_Data.Fields("ENREG_JOIN") & "' , '" & Rec_Data.Fields("EXTENT") & "' )"
                '-> creation de DEALB3D_HISTO
                CreateEnreg SQLValue
                                                                            
                '-> Supprimer le DEALB3D_DATA
                ErrorCode = "Traitement DELETE : Delete de la data"
                Rec_Data.Delete
                Rec_Data.MoveNext
                
            Loop
                        
            '-> Suppression de tous les index de l'enregistrement
            ErrorCode = "Traitement DELETE : Historiques -> Delete des index"
            SQLValue = "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & Num_Enreg & "'"
            Set Rec_Index = SetTable(SQLValue)
            Do While Not Rec_Index.EOF
                Rec_Index.Delete
                Rec_Index.MoveNext
            Loop
            Rec_Index.Close
            
        End If 'Si enregistrement
        
        Rec_Data.Close

    Case "CREATE"
    
        ErrorCode = "Traitement CREATE :  Mise � jour des data "
        SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname & _
                                "' AND [NUM_ENREG] = '" & Num_Enreg & "'"
        Set Rec_Data = SetTable(SQLValue)
                
        '-> Initialiser la matrice  des instructions SQL
        iMatrice = 0
        
        If Not Rec_Data.EOF Then
            Do While Not Rec_Data.EOF
                '-> Redimensionner la matrice
                iMatrice = iMatrice + 1
                ReDim Preserve MatriceSql(iMatrice)
                '-> Mode debug
                strEnregJoin = GetEnregJoin(Rec_Data.Fields("Id_FNAME"), Rec_Data.Fields("FIELD"), Rec_Data.Fields("ZON_DATA"), Rec_Data.Fields("NUM_ENREG"))
                MatriceSql(iMatrice) = "UPDATE [DEALB3D_DATA] SET [ZON_LOCK] = '' ," & _
                            "[PRG_MAJ] = '" & Programme & "' , [OPE_MAJ] = '" & Operateur & "', " & _
                            "[DAT_HEU_MAJ] = '" & Get_Total_Time & "' , [ENREG_JOIN] = '" & strEnregJoin & "' " & _
                            "WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & Num_Enreg & "' AND [FIELD] = '" & Rec_Data.Fields("FIELD") & "' AND [EXTENT] = '" & Rec_Data.Fields("EXTENT") & "'"
                                                    
                '-> Enreg suivant
                Rec_Data.MoveNext
            Loop
        End If
        Rec_Data.Close
    
        '-> Envoyer les ordres d'update pour tous les champs
        If iMatrice <> 0 Then
            ErrorCode = "TRAITEMENT : UPDATE sur DEALB3D_DATA"
            For iMatrice = 1 To UBound(MatriceSql())
                '-> Mode debug
                SQLValue = MatriceSql(iMatrice)
                '-> Envoyer l'instruction
                CreateEnreg MatriceSql(iMatrice)
            Next
        End If
        
    '-> Traitement des DEALB3D_DATA de l'enregistrement pour UPDATE et creation DEALB3D_HISTO si champ histo = O
    Case "UPDATE"
    
        ErrorCode = "Traitement UPDATE : Recherche des data"
        SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & Num_Enreg & "' AND [DAT_HEU_MAJ] ='IS_MODIFY' "
        Set Rec_Data = SetTable(SQLValue)
        
        If Not Rec_Data.EOF Then
        
            Do While Not Rec_Data.EOF
                '-> test si le field est suivi en historique creation DEALB3D_HISTO avec la valeur de ZON_LOCK
                '-> Pointer sur le field
                Set aField = aFname.Fields(Trim(UCase$(Rec_Data.Fields("FIELD"))))
                
                If Rec_Data.Fields("ZON_LOCK") <> Rec_Data.Fields("ZON_DATA") Then
                    
                    If aField.IsHisto And Trim(Rec_Data.Fields("ZON_LOCK")) <> "" Then
                    
                        ErrorCode = "Traitement UPDATE : Traitement des histo"
                        
                        '-> creation de DEALB3D_HISTO pour la data
                        SQLValue = "INSERT INTO [DEALB3D_HISTO] ( ID_FNAME , FIELD , NUM_ENREG , ZON_DATA , " & _
                                    "FONCTION , DAT_HISTO , HEU_HISTO , PRG_MAJ , OPE_MAJ , DAT_HEU_MAJ , ENREG_JOIN , EXTENT ) " & _
                                    "VALUES " & _
                                    "('" & Trim(Rec_Data.Fields("ID_FNAME")) & "','" & Trim(Rec_Data.Fields("FIELD")) & "','" & _
                                    Trim(Rec_Data.Fields("NUM_ENREG")) & "','" & Rec_Data.Fields("ZON_LOCK") & "',  '" & Fonction & "','" & _
                                    GetDate & "','" & GetTime & "','" & Rec_Data.Fields("PRG_MAJ") & "','" & Rec_Data.Fields("OPE_MAJ") & "','" & Rec_Data.Fields("DAT_HEU_MAJ") & "','" & Rec_Data.Fields("ENREG_JOIN") & "' , '" & Rec_Data.Fields("EXTENT") & "' )"
                        CreateEnreg SQLValue
                    End If 'si champ d'histo
                End If 'Si ZON_DATA <> ZON_LOCK
                
                '-> raz de la zone ZON_LOCK et Lib�ration de DEALB3D_DATA
                ErrorCode = "Traitement UPDATE : Raz de ZON_LOCK et lib�ration de DEALB3D_DATA."
                SQLValue = "UPDATE [DEALB3D_DATA] SET [ZON_LOCK] =  '' , " & _
                            "[PRG_MAJ] = '" & Programme & "', [OPE_MAJ] = '" & Operateur & "', " & _
                            "[ENREG_JOIN] = '" & GetEnregJoin(Rec_Data.Fields("Id_FNAME"), Rec_Data.Fields("FIELD"), Rec_Data.Fields("ZON_DATA"), Rec_Data.Fields("NUM_ENREG")) & "', [DAT_HEU_MAJ] =  '" & Get_Total_Time & "' " & _
                            " WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & Num_Enreg & "' AND [FIELD] = '" & Rec_Data.Fields("FIELD") & "' AND [EXTENT] = '" & Rec_Data.Fields("EXTENT") & "'"
                CreateEnreg SQLValue
                
                
                '-> Enreg suivant
                Rec_Data.MoveNext
                
            Loop
            
            '-> Fermer le query
            Rec_Data.Close
            
        End If 'Si enregistrement
End Select

Exit Sub

GestError:
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure traitement : " & ErrorCode & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description & Chr(13) & Chr(10) & SQLValue)
    '-> Traitement des conflits
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    


End Sub

Private Function GetEnregJoin(pIdFname As String, pField As String, pValueField As String, pNumEnreg As String) As String

'---> Cette fonction retourne l'acc�s � la jointure pour un champ donn�

Dim aEmilieObj As EmilieObj
Dim Id_Fname As String
Dim DefField As String
Dim File_Jointure As String
Dim aFname As Fname
Dim Field_Join  As String
Dim aRec1 As Object
Dim ValueField As String
Dim Enreg_Join As String
Dim Soc_Ref As String
Dim Index_Value() As String
Dim IsChampRef As Boolean
Dim Champ_ref As String
Dim Acces_ref As String
Dim Num_Enreg_Ref As String
Dim aIname As INAME
Dim aIfield As IFIELD
Dim strValue As String

On Error GoTo GestError

'-> R�cup�rer Ident + Fname
Id_Fname = pIdFname

'-> R�cup�rer le champ pour lequel on veut la jointure
DefField = pField

'-> R�cup�rer la valeur du champ pour lequel on veut la jointure
ValueField = pValueField

'-> Pointer sur l'objet du catalogue pour savoir s'il y a un fichier de jointure
Set aEmilieObj = ServeurDOG.Catalogue(UCase$(Entry(1, Id_Fname, "�")) & "|" & UCase$(DefField))

'-> V�rifier si jointure
If aEmilieObj.Table_Correspondance = "" Then Exit Function

'-> Recup de la table de jointure
File_Jointure = UCase$(Trim(Entry(1, aEmilieObj.Table_Correspondance, ":")))

'-< Ne pas se jointer sur soi m�me
If File_Jointure = Entry(2, Id_Fname, "�") Then Exit Function

'-> Acc�s pour v�rifier si fichier de jointure en index direct
Set aFname = ServeurDOG.fNames(UCase(Entry(1, Id_Fname, "�") & "|" & File_Jointure))

'-> On test si la table est � index direct
If aFname.IndexDirect <> "" Then

    '-> Composer l'acces � la jointure
    File_Jointure = UCase(Entry(1, Id_Fname, "�")) & "�" & File_Jointure
    
    '-> Acces par index direct : on r�cup�re le champ de jointure
    Field_Join = UCase$(Trim(aFname.IndexDirect))
    
    '-> R�cup�rer le num_enreg du champ de jointure dans DEALB3D_DATA
    Set aRec1 = SetTable("SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & File_Jointure _
                        & "' AND [FIELD] = '" & Field_Join & "' AND [ZON_DATA] = '" & _
                        ValueField & "'")
                    
    '-> Si on trouve la valeur
    If Not aRec1.EOF Then
                        
        '-> On a trouv� la DATA : composer l'adresse de la jointure sous le forme
        ' IDENT�FNAME_JOIN�NUM_ENREG_JOIN
        Enreg_Join = Trim(File_Jointure) & "�" & Trim(aRec1.Fields("NUM_ENREG"))
        '-> Fermer le recordset
        aRec1.Close
    End If
Else '-> Il y a un index sur la table
    '-> Si pas d'index : quitter la fonction
    If ServeurDOG.fNames(Entry(1, Id_Fname, "�") & "|" & aEmilieObj.Table_Correspondance).IndexJointure = "" Then Exit Function
    
    '-> Dimensionner la matrice des champs d'acc�s index
    Erase Index_Value()
    ReDim Index_Value(0)

    '-> Pour recherche d'une valeur sur un champ, v�rifier s'il y a une table _
    de correspondance attach� � ce champ, si la table attach�e n'est pas � index directe _
    prendre le premier index, v�rifier si le champ SOCREF est rens�ign� dans l'index _
    et si on le trouve, r�cup�rer le num�ro d'enregistrement pour acc�s � sa valeur _
    dans DEALB3D_DATA

    '-> Recup code societe re�l
    Set aRec1 = SetTable("SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & _
                Id_Fname & "' AND [NUM_ENREG] = '" & pNumEnreg & _
                "' AND [FIELD] = '" & ServeurDOG.GetSystemTableValue("CODSOC") & "'")

    '-> Si on a trouv� l'enreg : affecter la soci�t� de r�f�rence
    If Not aRec1.EOF Then Soc_Ref = RTrim(DeCrypt(aRec1.Fields("ZON_DATA")))
                    
    '-> Fermer le query
    aRec1.Close

    '-> Tester s'il y a un acc�s par r�f�rence sur le premier index de la table de correspondance
    IsChampRef = IsInameChampRef(aEmilieObj.Table_Correspondance)

    '-> s'il y a un champ de r�f�rence pour l'acc�s aux soci�t�s de regroupement ( ex : TypAux pour CodAux )
    If IsChampRef Then
    
        '-> Recup du champ de ref�rence
        Champ_ref = aEmilieObj.MasterField
                        
        '-> R�cup�ration de la valeur du champ de r�f�rence
        If Champ_ref <> "" Then
        
            '-> Get de sa valeur
            Set aRec1 = SetTable("SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & _
                        Id_Fname & "' AND [NUM_ENREG] = '" & pNumEnreg & _
                        "' AND [FIELD] = '" & Champ_ref & "'")
                        
            '-> Affecter la valeur si on a trouv�
            If Not aRec1.EOF Then Acces_ref = RTrim(DeCrypt(aRec1.Fields("ZON_DATA")))
            
            '-> Fermer le query
            aRec1.Close
            
        End If 'S'il y a un champ de r�f�rence

        '-> Recup du num_enreg de la table de r�f�rence soci�t� pour acces � DEALB3D_DATA
        Set aRec1 = SetTable("SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & _
            Ident & "�" & ServeurDOG.GetSystemTableValue("REF_SOCIETE") & "' AND [ZON_INDEX_1] = '" & Soc_Ref & _
            "' AND [ZON_INDEX_2] = '" & File_Jointure & "' AND [ZON_INDEX_3] = '" _
            & Acces_ref & "'")

        '-> Affecter le num�ro d'enreg
        If Not aRec1.EOF Then Num_Enreg_Ref = RTrim(DeCrypt(aRec1.Fields("NUM_ENREG")))
        
        '-> Fermer le query
        aRec1.Close

        '-> Acces a DEALB3D_DATA pour recup�rer la valeur de la soci�t� de r�f�rence
        Set aRec1 = SetTable("SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & _
                Entry(1, Id_Fname, "�") & "�" & ServeurDOG.GetSystemTableValue("REF_SOCIETE") & "' AND [NUM_ENREG] = '" & Num_Enreg_Ref & _
                "' AND [FIELD] = '" & ServeurDOG.GetSystemTableValue("SOCREF") & "'")
                
        '-> Affecter la valeur de la soci�t� de r�f�rence
        If Not aRec1.EOF Then Soc_Ref = RTrim(DeCrypt(aRec1.Fields("ZON_DATA")))
        
        '-> Fermer le query
        aRec1.Close
        
    End If 'Si acces par r�f�rence

    '-> Construire l'index d'acc�s � DEALB3D_INDEX ( table secondaire ) avec les valeurs de DEALB3D_DATA ( sauf pour le SOCREF)
    '-> Chercher le num_enreg_join d'apres l'index
    
    
    '-> Prendre l'index de jointure associ�
    Set aIname = aFname.Inames(aFname.IndexJointure)

    '-> Construire la requete d'acc�s
    For Each aIfield In aIname.iFields
        '-> En construisant , pour le champ SOCREF ne pas prendre DEALB3D_DATA mais DEALB3D_INDEX de Ref_SOCIETE
        If aIfield.Num_Field = ServeurDOG.GetSystemTableValue("SOCREF") Then
            '-> Get de la soci�t� de r�f�rence
            strValue = Soc_Ref
        Else
            '-> Acc�s � la valeur r�elle
            Set aRec1 = SetTable("SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & _
                        Id_Fname & "' AND [NUM_ENREG] = '" & pNumEnreg & "' AND [FIELD] = '" _
                        & aIfield.Num_Field & "'")
                        
            '-> Affecter la valeur
            If Not aRec1.EOF Then strValue = RTrim(DeCrypt(aRec1.Fields("ZON_DATA")))
            
            '-> Fermer le query
            aRec1.Close
        End If
            
        '-> affecter les valeurs d'acc�s pour les champs de l'index de jointure
        ReDim Preserve Index_Value(UBound(Index_Value()) + 1)
        Index_Value(UBound(Index_Value())) = strValue
        
    Next 'Pour tous les champs d'index

    '-> Redimensionner pour les 15 niveaux d'acc�s de la table
    ReDim Preserve Index_Value(15)

    '-> Acc�s � DEALB3D_INDEX pour r�cup�rer le num d'enregistrement pour acc�der � DEALB3D_DATA
    Set aRec1 = SetTable("SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & _
                        Entry(1, Id_Fname, "�") & "�" & File_Jointure & "' AND [INAME] = '" & aIname.Num_Iname & _
                        "' AND [ZON_INDEX_1] = '" & Index_Value(1) & _
                        "' AND [ZON_INDEX_2] = '" & Index_Value(2) & _
                        "' AND [ZON_INDEX_3] = '" & Index_Value(3) & _
                        "' AND [ZON_INDEX_4] = '" & Index_Value(4) & _
                        "' AND [ZON_INDEX_5] = '" & Index_Value(5) & _
                        "' AND [ZON_INDEX_6] = '" & Index_Value(6) & _
                        "' AND [ZON_INDEX_7] = '" & Index_Value(7) & _
                        "' AND [ZON_INDEX_8] = '" & Index_Value(8) & _
                        "' AND [ZON_INDEX_9] = '" & Index_Value(9) & _
                        "' AND [ZON_INDEX_10] = '" & Index_Value(10) & _
                        "' AND [ZON_INDEX_11] = '" & Index_Value(11) & _
                        "' AND [ZON_INDEX_12] = '" & Index_Value(12) & _
                        "' AND [ZON_INDEX_13] = '" & Index_Value(13) & _
                        "' AND [ZON_INDEX_14] = '" & Index_Value(14) & _
                        "' AND [ZON_INDEX_15] = '" & Index_Value(15) & "'")
    
    '-> Affecter la cl� d'acc�s � la jointure
    If Not aRec1.EOF Then Enreg_Join = Entry(1, Id_Fname, "�") & "�" & Trim(File_Jointure) & "�" & Trim(aRec1.Fields("NUM_ENREG"))
   
    '-> Fermer le query
    aRec1.Close
    
End If 'Si table en acc�s direct ou non

'-> Renvoyer la cl� d'acc�s � la jointure
GetEnregJoin = Enreg_Join

Exit Function

GestError:
    '-> Mode debug
    Call PrintDebug("Num_Enreg_Join : Erreur dans la proc�dure : " & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)

End Function



Private Sub MiseAJour()

Dim Champ_Index(15) As String 'Zones cl�s de INDEX DATA
Dim Champ_IndTo(15) As String 'zones cl�s des index de DATA CUMUL
Dim Champ_Name(15) As String 'Noms des champs de l'index

Dim aIname As INAME
Dim aIfield As IFIELD
Dim aField As FIELD
Dim strTest As String
Dim Top_Cumul As Boolean
Dim Top_Create As Boolean
Dim Num_Enreg_Cum As String
Dim i As Integer
Dim ErrorCode As String

'-> Recordsets
Dim Rec_Data As Object
Dim Rec_Rcopy As Object
Dim Rec_Histo As Object
Dim Rec_Index As Object
Dim Rec_Index_Cumul As Object
Dim Rec_Index_Cumul1 As Object
Dim Rec_Data_Cumul As Object
Dim Rec_Lock_Cum As Object
Dim Top_Error As Integer
Dim strEnregJoin As String
Dim RecExtent As String

Dim SQLValue As String


On Error GoTo GestError

'-> Si on est en mise � jour d'un champ existant
If Top_Maj = 1 And Fonction <> "CREATE" And Fonction <> "INDEX" Then

    ErrorCode = "MiseaJour : Suppression des index"
    SQLValue = "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & Num_Enreg & "'"
    '-> Suppression de tous les index de l'enregistrement
    Set Rec_Index = SetTable(SQLValue)
    Do While Not Rec_Index.EOF
        Rec_Index.Delete
        Rec_Index.MoveNext
    Loop
    Rec_Index.Close
End If

For Each aIname In aFname.Inames

    '-> Test de l'index � traiter
    If Fonction = "INDEX" And Programme <> "REINDEX_" & aIname.Num_Iname Then GoTo NextIname

    '-> Vider les matrices
    Erase Champ_Index
    Erase Champ_IndTo
    Top_Cumul = False
    
    For Each aIfield In aIname.iFields
        
        strTest = ""
        ErrorCode = "MiseaJour : Acces aux data pour constitution d'index"
        SQLValue = "SELECT TOP 1 * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & _
                    Id_Fname & "' AND [NUM_ENREG] = '" & Num_Enreg & "' AND [FIELD] = '" & UCase$(aIfield.Num_Field) & "'"
        Set Rec_Data = SetTable(SQLValue)

        '-> Selon la fonction r�cup�ration de la zone qui va bien
        If Not Rec_Data.EOF Then
            If Top_Maj = 1 Or Rec_Data.Fields("DAT_HEU_MAJ") <> "IS_MODIFY" Then
                strTest = RTrim(DeCrypt(Rec_Data.Fields("ZON_DATA")))
            Else
                strTest = RTrim(DeCrypt(Rec_Data.Fields("ZON_LOCK")))
            End If
        End If
        
        '-> Fermer le query
        Rec_Data.Close
        
        '-> Test condition
        Top_Create = True
        If aIfield.TypeCondition <> 0 Then
            If Not TestCondition(strTest, aIfield) Then
                Top_Create = False
                Exit For
            End If
        End If 'si on doit tester la condition de mise � jour du champ
        
        '-> La condition est remplie : cr�ation de l'index et saisie des champs associ�s
        Champ_Index(CInt(aIfield.Ordre)) = strTest
        Champ_Name(CInt(aIfield.Ordre)) = UCase$(aIfield.Num_Field)
        If aIfield.Cumul Then
            Champ_IndTo(CInt(aIfield.Ordre)) = strTest
            Top_Cumul = True
        End If
    Next 'Pour tous les champs d'index
    
    '-> Cr�ation de l'index
    If Top_Create Then
        '-> Si on est en CREATE ou UPDATE
        If Top_Maj = 1 Then
            '-> Mise � jour des stats
            NbIndex = NbIndex + 1
            ErrorCode = "MiseaJour : Cr�ation de l'index"
            SQLValue = "INSERT INTO [DEALB3D_INDEX] ( ID_FNAME , INAME , NUM_ENREG , ZON_INDEX_1 , " & _
                        "ZON_INDEX_2 , ZON_INDEx_3 , ZON_INDEX_4 , ZON_INDEX_5 , ZON_INDEX_6 , ZON_INDEX_7 , " & _
                        "ZON_INDEX_8 , ZON_INDEX_9 , ZON_INDEX_10 , ZON_INDEX_11 , ZON_INDEX_12 , ZON_INDEX_13 , ZON_INDEX_14 , ZON_INDEX_15  , PRG_MAJ  , OPE_MAJ , DAT_HEU_MAJ) VALUES (" & _
                        "'" & UCase$(Id_Fname) & "','" & UCase$(aIname.Num_Iname) & "','" & Num_Enreg & "','" & Champ_Index(1) & "','" & Champ_Index(2) & "','" & Champ_Index(3) & "','" & Champ_Index(4) & "','" & Champ_Index(5) & "','" & _
                        Champ_Index(6) & "','" & Champ_Index(7) & "','" & Champ_Index(8) & "','" & Champ_Index(9) & "','" & Champ_Index(10) & "','" & _
                        Champ_Index(11) & "','" & Champ_Index(12) & "','" & Champ_Index(13) & "','" & Champ_Index(14) & "','" & Champ_Index(15) & "','" & Programme & "','" & Operateur & "' , '" & Get_Total_Time & "')"
            CreateEnreg SQLValue
                        
        End If
    End If 'Si cr�ation d'index
    
    '-> Mise � jour si cumul auto
    If Top_Cumul And Top_Create Then
        ErrorCode = "MiseaJour : Recherche index de cumul"
        SQLValue = "SELECT * FROM [DEALB3D_INDEX] WHERE [ID_FNAME] = '" & _
                                UCase$(Id_Fname) & "�" & UCase$(aIname.Num_Iname) & "'AND [INAME] = '" & _
                                UCase$(aIname.Num_Iname) & "' AND " & _
                                "[ZON_INDEX_1] = '" & Champ_IndTo(1) & "' AND " & _
                                "[ZON_INDEX_2] = '" & Champ_IndTo(2) & "' AND " & _
                                "[ZON_INDEX_3] = '" & Champ_IndTo(3) & "' AND " & _
                                "[ZON_INDEX_4] = '" & Champ_IndTo(4) & "' AND " & _
                                "[ZON_INDEX_5] = '" & Champ_IndTo(5) & "' AND " & _
                                "[ZON_INDEX_6] = '" & Champ_IndTo(6) & "' AND " & _
                                "[ZON_INDEX_7] = '" & Champ_IndTo(7) & "' AND " & _
                                "[ZON_INDEX_8] = '" & Champ_IndTo(8) & "' AND " & _
                                "[ZON_INDEX_9] = '" & Champ_IndTo(9) & "' AND " & _
                                "[ZON_INDEX_10] = '" & Champ_IndTo(10) & "' AND " & _
                                "[ZON_INDEX_11] = '" & Champ_IndTo(11) & "' AND " & _
                                "[ZON_INDEX_12] = '" & Champ_IndTo(12) & "' AND " & _
                                "[ZON_INDEX_13] = '" & Champ_IndTo(13) & "' AND " & _
                                "[ZON_INDEX_14] = '" & Champ_IndTo(14) & "' AND " & _
                                "[ZON_INDEX_15] = '" & Champ_IndTo(15) & "'"
        Set Rec_Index_Cumul = SetTable(SQLValue)
        
        '-> Si l'index de cumul existe d�ja
        If Not Rec_Index_Cumul.BOF Then
            '-> r�cup�re le num�ro du cumul
            Num_Enreg_Cum = Rec_Index_Cumul.Fields("NUM_ENREG")

            
GET_LOCK_CUM:
            
            ErrorCode = "Mise � jour : Index de cumul existant : Num�ro d'enregistrement : " & Num_Enreg_Cum
            SQLValue = "SELECT TOP 1 * FROM [DEALB3D_LOCK] WHERE [ID_FNAME] = '" & _
                                UCase$(Id_Fname) & "�" & UCase$(aIname.Num_Iname) & "' " & _
                                "AND [NUM_ENREG] = '" & Num_Enreg_Cum & "'"
            
            '-> Tester s'il n'est pas lock� par un  autre
            Set Rec_Lock_Cum = SetTable(SQLValue)
                                                                                                                                                           
            '-> Cr�er le lock s'il n'existe pas
            If Not Rec_Lock_Cum.EOF Then
                '-> le lock exit : boucler
                Sleep (TempoLock)
                GoTo GET_LOCK_CUM
            Else
            
                SQLValue = "INSERT INTO [DEALB3D_LOCK] ( ID_FNAME , NUM_ENREG , FONCTION , DAT_LOCK , HEU_LOCK , " & _
                            "HDL_LOCK , NUM_LOCK , DAT_RELEASE , HEU_RELEASE , OPE_MAJ , PRG_MAJ ) VALUES (" & _
                            "'" & Id_Fname & "�" & aIname.Num_Iname & "', '" & Num_Enreg_Cum & "','UPDATE' , '" & _
                            GetDate & "','" & GetTime & "','" & Me.hwnd & "', '999999999999999' , '" & GetDate & _
                            "',' " & GetTime & "','" & IdIdx & "','IDX_BUILD')"
                            
                '-> Creer le lock de cumul
                ErrorCode = "LOCK_CUMUL : Cr�ation du lock si inexistant"
                              
                '-> Cr�ation Lock
                CreateEnreg SQLValue
                
            End If
            Rec_Lock_Cum.Close
            
            
        Else
            
            '-> CREATION DE L'INDEX DE CUMUL DANS DEALB3D_INDEX
            '-> R�cup�ration d'un num�ro d'enregistrement pour cr�ation des DEALB3D_DATA de CUMUL
            Num_Enreg_Cum = Uc_ID & Get_NUM_ENREG(UCase$(Id_Fname) & "�" & UCase$(aIname.Num_Iname))
                                                                    
                        
            '-> Cr�ation Lock
            ErrorCode = "Mise � jour : Cr�ation du lock pour l'index de cumul pour l'enreg : " & Num_Enreg_Cum
            SQLValue = "INSERT INTO [DEALB3D_LOCK] ( ID_FNAME , NUM_ENREG , FONCTION , DAT_LOCK , HEU_LOCK , " & _
                        "HDL_LOCK , NUM_LOCK , DAT_RELEASE , HEU_RELEASE , OPE_MAJ , PRG_MAJ ) VALUES (" & _
                        "'" & Id_Fname & "�" & aIname.Num_Iname & "', '" & Num_Enreg_Cum & "','UPDATE' , '" & _
                        GetDate & "','" & GetTime & "','" & Me.hwnd & "', '999999999999999' , '" & GetDate & _
                        "',' " & GetTime & "','" & IdIdx & "','IDX_BUILD') "
                                
            CreateEnreg SQLValue
                        
            '-> L'index de cumul n'existe pas : le cr�er
            ErrorCode = "MiseaJour : Cr�ation de l'index de cumul"
            SQLValue = "INSERT INTO [DEALB3D_INDEX] ( ID_FNAME , INAME , NUM_ENREG , ZON_INDEX_1 , " & _
                        "ZON_INDEX_2 , ZON_INDEX_3 , ZON_INDEX_4 , ZON_INDEX_5 , ZON_INDEX_6 , ZON_INDEX_7 , " & _
                        "ZON_INDEX_8 , ZON_INDEX_9 , ZON_INDEX_10 , ZON_INDEX_11 , ZON_INDEX_12 , ZON_INDEX_13 , ZON_INDEX_14 , ZON_INDEX_15  , PRG_MAJ  , OPE_MAJ , DAT_HEU_MAJ) VALUES (" & _
                        "'" & UCase$(Id_Fname) & "�" & UCase$(aIname.Num_Iname) & "','" & UCase$(aIname.Num_Iname) & "','" & Num_Enreg_Cum & "','" & Champ_IndTo(1) & "','" & Champ_IndTo(2) & "','" & Champ_IndTo(3) & "','" & Champ_IndTo(4) & "','" & Champ_IndTo(5) & "','" & _
                        Champ_IndTo(6) & "','" & Champ_IndTo(7) & "','" & Champ_IndTo(8) & "','" & Champ_IndTo(9) & "','" & Champ_IndTo(10) & "','" & _
                        Champ_IndTo(11) & "','" & Champ_IndTo(12) & "','" & Champ_IndTo(13) & "','" & Champ_IndTo(14) & "','" & Champ_IndTo(15) & "','" & Programme & "','" & Operateur & "' , '" & Get_Total_Time & "')"
                        
            '-> Mode debug
            CreateEnreg SQLValue
                
            '-> Cr�ation des champs dans DEALB3D_DATA des datas index de cumul
            For i = 1 To UBound(Champ_IndTo())
                If Champ_IndTo(i) <> "" Then
                    ErrorCode = "Get de la jointure"
                    strEnregJoin = GetEnregJoin(Id_Fname & "�" & UCase$(aIname.Num_Fname), Champ_Name(i), Champ_IndTo(i), Num_Enreg_Cum)
                    
                    ErrorCode = "MiseaJour : Cr�ation des data index de cumul"
                    SQLValue = "INSERT INTO [DEALB3D_DATA] ( ID_FNAME , NUM_ENREG , FIELD , ZON_DATA" & _
                                 ", ZON_LOCK , PRG_MAJ , OPE_MAJ , DAT_HEU_MAJ , EXTENT ,ENREG_JOIN) VALUES ( '" & Id_Fname & "�" & UCase$(aIname.Num_Iname) & "' , '" & Num_Enreg_Cum & "' , '" & _
                                 Champ_Name(i) & "' , '" & Champ_IndTo(i) & "' , '' , '" & Programme & "' , '" & _
                                 Operateur & "' , '" & Get_Total_Time & "' , ' ' , '" & strEnregJoin & "')"
                    CreateEnreg SQLValue
                End If 'Si valeur de champ  renseign�e
            Next 'Pour tous les champs de l'index
        End If
        
        '-> Fermer le recordset d'acces aux index de cumul
        Rec_Index_Cumul.Close
                
        '-> CREATION OU MAJ DES CHAMPS DE CUMULS DANS DEALB3D_DATA
        
        '-> Pointer sur DEALB3D_DATA avec NUM_ENREG
        For Each aField In aFname.Fields
            '-> pointer sur l'objet associ� du catalogue
            Set aEmilieObj = ServeurDOG.Catalogue(UCase$(Ident) & "|" & UCase$(aField.Num_Field))
            '-> V�rifier que c'est un d�cimal ( seule zone cumuable)
            If Left$(UCase$(aEmilieObj.DataType), 3) = "DEC" Then
            
                strTest = ""
                '-> R�cup�rer la data de DEALB3D_DATA
                ErrorCode = "MiseaJour : recherche des data decimal de cumul"
                SQLValue = "SELECT * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & _
                                       Id_Fname & "' AND [NUM_ENREG] = '" & Num_Enreg _
                                       & "' AND [FIELD] = '" & aField.Num_Field & "'"
                Set Rec_Data = SetTable(SQLValue)
                '-> R�cup�ration de la valeur
                If Not Rec_Data.EOF Then
                    If Top_Maj = 1 Or Rec_Data.Fields("DAT_HEU_MAJ") <> "IS_MODIFY" Then
                        strTest = DeCrypt(RTrim(Rec_Data.Fields("ZON_DATA")))
                    Else
                        strTest = DeCrypt(RTrim(Rec_Data.Fields("ZON_LOCK")))
                    End If
                    
                     '-> Get de la valeur de l'extent
                    RecExtent = Rec_Data.Fields("EXTENT")
                Else
                    RecExtent = " "
                End If
                
                '-> Fermer le query
                Rec_Data.Close
                
                '-> Tester si valeur <> "" : V�rifier si l'enregistrement de cumul existe d�ja
                If Trim(strTest) <> "" Then
                
                    ErrorCode = "Mise � Jour : Cr�ation des data de cumul"
                    SQLValue = "SELECT TOP 1 * FROM [DEALB3D_DATA] WHERE [ID_FNAME] = '" & Id_Fname & "�" & UCase$(aIname.Num_Iname) & "' AND " & _
                                           "[NUM_ENREG] = '" & Num_Enreg_Cum & "' AND [FIELD] = '" & UCase$(aField.Num_Field) & "' AND [EXTENT] = '" & RecExtent & "'"
                    Set Rec_Data_Cumul = SetTable(SQLValue)
                    '-> Tester si le DEALB3D_DATA de cumul existe d�ja
                    If Rec_Data_Cumul.EOF Then
                        ErrorCode = "Mise � jour : Cr�ation des datas de cumul."
                        SQLValue = "INSERT INTO [DEALB3D_DATA] ( ID_FNAME , NUM_ENREG , FIELD , ZON_DATA" & _
                                    ", ZON_LOCK , PRG_MAJ , OPE_MAJ , DAT_HEU_MAJ , EXTENT ) VALUES ( '" & Id_Fname & "�" & UCase$(aIname.Num_Iname) & "' , '" & Num_Enreg_Cum & "' , '" & _
                                    Trim(UCase$(aField.Num_Field)) & "' , '" & CStr(CDbl(Convert(strTest)) * Top_Maj) & "' , '' , '" & Programme & "' , '" & _
                                    Operateur & "' , '" & Get_Total_Time & "' , '" & RecExtent & "')"
                        '-> Debug
                        Call PrintDebug(ErrorCode & Chr(13) & Chr(10) & SQLValue)
                        '-> Le cr�er
                        CreateEnreg SQLValue
                    Else
                        ErrorCode = "Mise � Jour : Mise � jour des data de cumul"
                        SQLValue = "UPDATE [DEALB3D_DATA] SET [ZON_DATA] =  '" & CStr(CDbl(Convert(Rec_Data_Cumul.Fields("ZON_DATA")) + CDbl(Convert(strTest) * Top_Maj))) & "', " & _
                                    "[PRG_MAJ] = '" & Programme & "', [ZON_LOCK] = '', " & _
                                    "[OPE_MAJ] = '" & Operateur & "', [DAT_HEU_MAJ] = '" & Get_Total_Time & "' WHERE [ID_FNAME] = '" & Id_Fname & "�" & UCase$(aIname.Num_Iname) & "' AND " & _
                                    "[NUM_ENREG] = '" & Num_Enreg_Cum & "' AND [FIELD] = '" & UCase$(aField.Num_Field) & "'AND [EXTENT] = '" & RecExtent & "'"
                        '-> Debug
                        Call PrintDebug(ErrorCode & Chr(13) & Chr(10) & SQLValue)
                        Debug.Print SQLValue
                        '-> Exec de la requete
                        CreateEnreg SQLValue
                    End If 'S'il existe
                    Rec_Data_Cumul.Close
                End If 'Si valeur <> ""
            End If 'Si champ d�cimal
        Next 'Pour tous les champs du fname
        ErrorCode = "Mise � jour : Suppression du lock de cumul � : " & Now
        SQLValue = "SELECT TOP 1 * FROM [DEALB3D_LOCK] WHERE [ID_FNAME] = '" & _
                                UCase$(Id_Fname) & "�" & UCase$(aIname.Num_Iname) & "' " & _
                                "AND [NUM_ENREG] = '" & Num_Enreg_Cum & "'"
        '-> Supprimer le lock de cumul
        Set Rec_Lock_Cum = SetTable(SQLValue)
        
        '-> Supprimer le lock
        Rec_Lock_Cum.Delete
        Rec_Lock_Cum.Close
                        
    End If 'Si mise � jour du cumul!
    
NextIname:
    
Next 'Pour tous les index

Exit Sub

GestError:
    
    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure Mise � jour : " & ErrorCode & Chr(13) & Chr(10) & Err.Number & "-> " & Err.Description & Chr(13) & Chr(10) & SQLValue)
    '-> Gestion des conflits
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If


End Sub

Private Function TestCondition(ByRef strTest As String, ByRef aIfield As Object) As Boolean


Dim boolRetour As Boolean
Dim ErrorCode As String

On Error GoTo GestError

ErrorCode = "TestCondition : Acc�s � l'objet du catalogue :  " & aIfield.Num_Field

'-> Pointer sur le champ
Set aEmilieObj = ServeurDOG.Catalogue(UCase$(Ident) & "|" & UCase$(aIfield.Num_Field))

boolRetour = True

ErrorCode = "TestCondition : Test de la condition : " & aIfield.Num_Field & "  " & aIfield.TypeCondition

Select Case aIfield.TypeCondition
        
    Case 1
    
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(Convert(strTest)) <> CDbl(Convert(aIfield.ValMini)) Then boolRetour = False
            
            Case "INT"
                If CInt(Convert(strTest)) <> CInt(Convert(aIfield.ValMini)) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest <> aIfield.ValMini Then boolRetour = False
        
        End Select
        
    Case 2
    
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(Convert(strTest)) < CDbl(Convert(aIfield.ValMini)) Or CDbl(Convert(strTest)) > CDbl(Convert(aIfield.ValMaxi)) Then boolRetour = False
            
            Case "INT"
                If CInt(Convert(strTest)) < CInt(Convert(aIfield.ValMini)) Or CInt(Convert(strTest)) > CInt(Convert(aIfield.ValMaxi)) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest < aIfield.ValMini Or strTest > aIfield.ValMaxi Then boolRetour = False
        End Select
        
    Case 3
    
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(Convert(strTest)) > CDbl(Convert(aIfield.ValMini)) Or CDbl(Convert(strTest)) < CDbl(Convert(aIfield.ValMaxi)) Then boolRetour = False
            
            Case "INT"
                If CInt(Convert(strTest)) > CInt(Convert(aIfield.ValMini)) Or CInt(Convert(strTest)) < CInt(Convert(aIfield.ValMaxi)) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest > aIfield.ValMini Or strTest < aIfield.ValMaxi Then boolRetour = False
        End Select
            
    Case 4
        
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(Convert(strTest)) = CDbl(Convert(aIfield.ValMini)) Then boolRetour = False
            
            Case "INT"
                If CInt(Convert(strTest)) = CInt(Convert(aIfield.ValMini)) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest = aIfield.ValMini Then boolRetour = False
        End Select
        
    Case 5
    
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(Convert(strTest)) > CDbl(Convert(aIfield.ValMini)) Then boolRetour = False
            
            Case "INT"
                If CInt(Convert(strTest)) > CInt(Convert(aIfield.ValMini)) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest > aIfield.ValMini Then boolRetour = False
        
        End Select
        
    Case 6
    
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(Convert(strTest)) < CDbl(Convert(aIfield.ValMini)) Then boolRetour = False
            
            Case "INT"
                If CInt(Convert(strTest)) < CInt(Convert(aIfield.ValMini)) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest < aIfield.ValMini Then boolRetour = False
        
        End Select
        
    Case 7
    
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(Convert(strTest)) >= CDbl(Convert(aIfield.ValMini)) Then boolRetour = False
            
            Case "INT"
                If CInt(Convert(strTest)) >= CInt(Convert(aIfield.ValMini)) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest >= aIfield.ValMini Then boolRetour = False
        
        End Select
        
    Case 8
    
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(Convert(strTest)) <= CDbl(Convert(aIfield.ValMini)) Then boolRetour = False
            
            Case "INT"
                If CInt(Convert(strTest)) <= CInt(Convert(aIfield.ValMini)) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest <= aIfield.ValMini Then boolRetour = False
        
        End Select
        
    Case 9
    
        If Left$(aEmilieObj.DataType, 3) = "CHA" Then
            If aIfield.ValMini <> Left$(strTest, Len(aIfield.ValMini)) Then boolRetour = False
        End If
        
        
    Case 10
        
        If Left$(aEmilieObj.DataType, 3) = "CHA" Then
            If InStr(UCase$(aIfield.ValMini), UCase$(strTest)) = 0 Then boolRetour = False
        End If

End Select

TestCondition = boolRetour

Exit Function

GestError:

    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure TestCondition : " & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)

End Function

Private Sub Timer1_Timer()

'-> Lancer la boucle d'analyse
Me.AnalyseBase
DoEvents

End Sub
