Attribute VB_Name = "fct_Idx_Build_B3d"
Option Explicit

Public WaitForClosing As Boolean '-> En attente de fermeture
Public ServeurDOG As Object '-> Pointeur vers le serveur DOG

'-> API
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Public Declare Function SendMessage& Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any)
Public Declare Function SetWindowText& Lib "user32" Alias "SetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String)
Public Declare Function GetWindowText& Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String, ByVal cch As Long)

'-> Handle du fichier de retour
Private hdlFile As Integer

'-> Variables de path
Public IniPath As String '-> R�pertoire des fichiers ini
Public Tempo_Acces As Long '-> Variable qui indique l'interval d'analyse de D_LOCK

'-> Collection des idents
Public Idents As Collection

'-> Statistiques
Public NbCreate As Long
Public NbDelete As Long
Public NbUpdate As Long
Public NbIndex As Long

Sub Main()

'---> On a 2 cas : Soit lancer un Agent en tache de fond, soit une tache sp�cifique

'---> Ligne de commande de l'agent IDX
'[Handle de communication]  & "|" & [Numero de l'agent]  & "|" & [Base � connecter] & "|" & ModeDebug

Dim LigneCommande As String
Dim Num_Lock As String
Dim Top_Wait As String
Dim Id_Session As String
Dim res As Long
Dim ModeDebug As String

On Error GoTo GestError

'-> Ne rien faire si pas de fichier de param�trage
If Trim(Command$) = "" Then End

'-> V�rifier que l'on trouve le fichier
If Dir$(Command$) = "" Then End

'-> Initialiser la collection des idents
Set Idents = New Collection

'->Initialiser les convertions
Call InitConvertion

'-> Lancer l'initialisation
Call Init(Command$)

'-> Connecter la base
Call OpenConnexionBase

'-> Charger la feuille
Load frmDisplay

'-> Indiquer � la console le handle de la feuille
SetWindowText HdlCom, "IDX_RETOUR_HANDLE|" & frmDisplay.hwnd & "|" & NomBase
SendMessage HdlCom, WM_PAINT, 0, 0
DoEvents

'-> Lancer l'analyse de la base
frmDisplay.Timer1.Interval = 1

Exit Sub

GestError:

    '-> Renvoyer un message d'erreur
    SetWindowText HdlCom, "IDX_EXIT|" & Err.Number & " : " & Err.Description
    res = SendMessage(HdlCom, WM_PAINT, 0, 0)

    '-> Fin
    End

End Sub


Public Function ConnectDOG(strIdent As String) As Boolean

'---> Cette proc�dure charge un ident si celui-ci n'est pas charg�

Dim aIdent As Ident

If Not IsServeurDOG(strIdent) Then
    '-> Cr�er une nouvelle instance
    Set aIdent = New Ident
    '-> Init de ses propri�t�s
    aIdent.Ident = strIdent
    '-> Initialiser
    If Not LoadDog(aIdent) Then Exit Function
    '-> Ajouter dans la collection
    Idents.Add aIdent, "IDENT|" & strIdent
Else
    '-> Pointer sur l'ident sp�cifi�
    Set aIdent = Idents("IDENT|" & strIdent)
End If 'Si le serveur DOG existe d�ja

'-> Faire pointer le serveurDOG sur celui de cet ident
Set ServeurDOG = aIdent.ServeurDOG

'-> Renvoyer une valeur de succ�s
ConnectDOG = True

End Function
Private Function LoadDog(aIdent As Ident) As Boolean

'---> Cette proc�dure r�cup�re les fichiers de param�trage d'un ident

Dim fCatalogue As String
Dim fSchema As String
Dim fLink As String

'-> Get du fichier catalogue
fCatalogue = ApplicationPath & aIdent.Ident & "\Param\CAT-" & aIdent.Ident & ".DAT"
If Trim(fCatalogue) = "" Then Exit Function

'-> V�rifier si on trouve le fichier
If Dir$(fCatalogue) = "" Then Exit Function

'-> Get du fichier sch�ma
fSchema = ApplicationPath & aIdent.Ident & "\Param\SCH-" & aIdent.Ident & ".DAT"
If Trim(fSchema) = "" Then Exit Function

'-> V�rifier si on trouve le fichier
If Dir$(fSchema) = "" Then Exit Function

'-> get du fichier des liens
fLink = ApplicationPath & aIdent.Ident & "\Param\LNK-" & aIdent.Ident & ".DAT"
If Trim(fLink) = "" Then Exit Function

'-> V�rifier si on trouve le fichier
If Dir$(fLink) = "" Then Exit Function

'-> Initialiser le serveurDOG de cet ident
Set aIdent.ServeurDOG = New clsDOG

'-> Charger  le catalogue
If Not LoadCatalogueB3D(aIdent.ServeurDOG, fCatalogue) Then Exit Function

'-> Charger le sch�ma
If Not LoadSchemaB3D(aIdent.ServeurDOG, fSchema) Then Exit Function

'-> Charger le fichier Lien
If Not LoadLinkB3D(aIdent.ServeurDOG, fLink) Then Exit Function

'-> Charger les tables syst�mes
If Not LoadSystemParam(aIdent.ServeurDOG, ApplicationPath & aIdent.Ident & "\Param\" & aIdent.Ident & ".ini", aIdent.Ident) Then Exit Function

'-> Renvoyer une valeur de succ�s
LoadDog = True

'-> Quitter la fonction
Exit Function

GestError:
    

End Function



Private Function IsServeurDOG(strIdent As String) As Boolean

'---> Cette proc�dure v�rifier si un serveur DOG est d�ja connect ou non

Dim aIdent As Ident

On Error GoTo GestError

'-> Essayer de pointer sur l'ident
Set aIdent = Idents("IDENT|" & strIdent)

'-> Si pas d'erreur indiquer que l'ident sp�cifi� existe d�ja
IsServeurDOG = True

'-> Quitter la fonction
Exit Function

GestError:
    '-> Si erreur, l' ident sp�cifi� n'est pas charg�
    IsServeurDOG = False

End Function
