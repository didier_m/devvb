VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmVisualisation 
   Caption         =   "Visualisation des Locks Brul�s"
   ClientHeight    =   6315
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11700
   Icon            =   "frmVisualisation.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   421
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   780
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView ListView1 
      Height          =   4215
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7935
      _ExtentX        =   13996
      _ExtentY        =   7435
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Menu mnuPopUp 
      Caption         =   "PopUp"
      Visible         =   0   'False
      Begin VB.Menu mnuSupprimer 
         Caption         =   "Supprimer le Journal"
      End
      Begin VB.Menu mnuSep0 
         Caption         =   "-"
      End
      Begin VB.Menu MNUpRINT 
         Caption         =   "Imprimer le Journal"
      End
   End
End
Attribute VB_Name = "frmVisualisation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Fichier Associ�
Dim Fichier As String

Private Sub TraiteFichierLog()

'---> Fonction qui affiche le contenu du fichier .log

Dim hdlFileL As Integer
Dim Ligne As String
Dim aIt As ListItem
Dim a As String
Dim b As String
Dim TempFileName As String

'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11

'-> On cree les colonnes
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "64")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "65")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "67")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("CONSOLE", "38")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "77")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "19")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "9")


If Trim(Fichier) = "" Then GoTo Rep

If Dir$(Fichier) = "" Then GoTo Rep

'-> Ouvrir le fichier des locsk brul�s
hdlFileL = FreeFile
Open Fichier For Input As #hdlFileL


'-> Parcours du fichier pour creer un listview
Do While Not EOF(hdlFileL)
    '-> Recup d'une ligne
    Line Input #hdlFileL, Ligne
    If Trim(Ligne) = "" Then GoTo NextLigne
    
    a = Entry(1, Ligne, "~")
    b = Entry(2, Ligne, "~")
    
    '-> Insertion dans le listview
    Set aIt = frmVisualisation.ListView1.ListItems.Add(, , Entry(2, a, "�"))
    aIt.SubItems(1) = Entry(3, a, "�")
    aIt.SubItems(2) = Entry(1, b, "�")
    aIt.SubItems(3) = Mid$(Entry(3, b, "�"), 7, 2) & "/" & Mid$(Entry(3, b, "�"), 5, 2) & "/" & Mid$(Entry(3, b, "�"), 1, 4)
    aIt.SubItems(4) = Format(Entry(4, b, "�"), "00:00:00")
    aIt.SubItems(5) = Entry(8, b, "�")
    aIt.SubItems(6) = Entry(9, b, "�")
NextLigne:
Loop

'-> fermer le fichier d'analyse
Close #hdlFileL

Rep:

'-> Formattage du listview
FormatListView frmVisualisation.ListView1

'-> Bloquer les enreg
Me.Enabled = True
Screen.MousePointer = 0


End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("APPLIC", "78")

'-> Composer le nom du fichier des logs
Fichier = aConnexion.ApplicationDir & aConnexion.Ident & "\Param\LOCK_ANALYZER.LOG"

'-> Cr�er le listview des locks brul�s
TraiteFichierLog

End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next
GetClientRect Me.hwnd, aRect
Me.ListView1.Left = 0
Me.ListView1.Top = 0
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom

End Sub

Private Sub listview1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    '-> Trier sur les entetes de colonne
    Me.ListView1.SortKey = ColumnHeader.Index - 1
    If Me.ListView1.SortOrder = lvwAscending Then
        Me.ListView1.SortOrder = lvwDescending
    Else
        Me.ListView1.SortOrder = lvwAscending
    End If
    Me.ListView1.Sorted = True
End Sub

Private Sub ListView1_DblClick()
'---> Branchement du zomm sur l'enreg en cours
Dim aDescro As B3DFnameDescriptif

'-> R�cup�rer les descriptif de la table
Set aDescro = GetFnameDescriptif(aConnexion, aConnexion.Ident, Trim(Me.ListView1.SelectedItem.Text))
                        
If aDescro Is Nothing Then
    '-> Afficher un message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbCritical + vbOKOnly, "Error"
    '-> Fin du programme
    End
End If

DisplayZoom aConnexion, Trim(Me.ListView1.SelectedItem.Text), Trim(Me.ListView1.SelectedItem.SubItems(1)), False, True, aDescro, Me, True

End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'---> gestion des popup

Dim i As Integer

'-> Sort si bouton droit
If Button <> 2 Then Exit Sub

If Me.ListView1.SelectedItem Is Nothing Then
    Me.MNUpRINT.Enabled = False
Else
    Me.MNUpRINT.Enabled = True
End If

'-> remise a blanc  de la variable d'echange
strRetour = ""

'-> Affichage du menu popup
Me.PopupMenu Me.mnuPopUp

Select Case strRetour
    Case "PRINT"
        Call PrintLockBrules
    Case "DELETE"
        '---> On supprime le fichier
        If Dir$(Fichier) <> "" Then
            Kill Fichier
        Else
            MsgBox GetMesseprog("APPLIC", "79"), vbExclamation + vbOKOnly, "Message"
        End If
End Select

'-> remise a blanc  de la variable d'echange
strRetour = ""

End Sub

Private Sub PrintLockBrules()

'---> On lance l'impression
Dim aEnreg As B3DEnreg
Dim aIt As ListItem
Dim ListeFields As String
Dim i As Integer
Dim hdlFileL As Integer
Dim LigneE As String
Dim NbLignes As Integer
Dim TempFileName As String
Dim TempFileName2 As String

On Error GoTo GestError

'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11

'-> On ouvre un nouveau fichier en ecriture
hdlFileL = FreeFile
TempFileName = GetTempFileNameVB("TRB")
Open TempFileName For Output As #hdlFileL

'-> On envoi l'entete du tableau
LigneE = "[ST-Section1(TXT-SECTION1)][\1]{ ^0001" & aConnexion.Ident & " }"
Print #hdlFileL, LigneE

'---> Parcours toutes les lignes de catalogue pour creer les instructions turbo a envoyer
For Each aIt In Me.ListView1.ListItems
      '-> Saut de pages
    If NbLignes = 0 Then
          '-> Re Entete
          LigneE = "[TB-NouveauTableau2(BLK-NouveauBlock1)][\1]{"
          Print #hdlFileL, LigneE
      End If
    LigneE = "[TB-NouveauTableau2(BLK-NouveauBlock2)][\1]{"
    
    '-> On parcours les champs a imprimer et on les envoi dans le fichier d'instructions turbo
    For i = 1 To Me.ListView1.ColumnHeaders.Count
        
        '-> On parcours tous les champs de l'enreg pour creer la ligne d'impression turbo
        If i = 1 Then
            LigneE = LigneE & "^" & Format(i, "0000") & aIt.Text
        Else
            LigneE = LigneE & "^" & Format(i, "0000") & aIt.SubItems(i - 1)
        End If
        
    Next '-> On passe au prochain champ a imprimer sur cette ligne

    LigneE = LigneE & " }"
    '-> envoi de la ligne
    Print #hdlFileL, LigneE
    
    NbLignes = NbLignes + 1
        
    '-> gestion saut de page + entete
    If NbLignes = 45 Then
        LigneE = "[PAGE]"
        Print #hdlFileL, LigneE
        NbLignes = 0
    End If
    
Next '-> On passe a la prochaine ligne de prime a imprimer

'-> fermeture du fichier
Close #hdlFileL

'-> Cr�er le fichier de merge
TempFileName2 = GetTempFileNameVB("TRB")

'-> On envoie la fusion entre la maquette et le fichier data puis l'impression TURBO
PrintDataInTurbo MaqPath & "B3D_VISU_LOCK_BRULE.MAQGUI", TempFileName, TempFileName2, True

GestError:
    Me.Enabled = True
    Screen.MousePointer = 0

End Sub

Private Sub MNUpRINT_Click()
strRetour = "PRINT"
End Sub

Private Sub mnuSupprimer_Click()
strRetour = "DELETE"
End Sub
