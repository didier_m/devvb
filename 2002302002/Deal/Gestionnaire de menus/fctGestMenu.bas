Attribute VB_Name = "fctGestMenu"
Option Explicit

'-> D�claration des API
Public Type POINTAPI
    x As Long
    y As Long
End Type
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function ScreenToClient Lib "user32" (ByVal hwnd As Long, lpPoint As POINTAPI) As Long
Public Declare Function IsWindow Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function SendMessage& Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any)
Public Declare Function SetWindowText& Lib "user32" Alias "SetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String)
Public Declare Function GetWindowText& Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String, ByVal cch As Long)

'-> Variables de param�trage
Public ApplicationPath As String
Public CodeLangue As Integer
Public IdentMenuPath As String


Private Sub Main()

'---> Point d'entr�e du programme

'Ligne de commande Dir des applic "|" Ident  "|" Code Langue

'-> Setting des variables de connexions
ApplicationPath = Entry(1, Command$, "�")
CodeLangue = CInt(Entry(2, Command$, "�"))

'-> Lancer l'initialisation
Call Init

'-> Afficher le gestionnaire
mdiConsole.Show

End Sub

Private Sub Init()

'---> Proc�dure d'initialisation et de chargement de la liste des menus

Dim i As Integer
Dim FileName As String
Dim aNode As Node
Dim NodeRef As Node
Dim ListeIdents As String
Dim DefIdent  As String
Dim Ident As String

'-> Afficher la liste des idents
ListeIdents = GetIniFileValue("IDENTS", "", ApplicationPath & "B3D\Param\Idents.ini", True)
If Trim(ListeIdents) = "" Then Exit Sub

'-> Pour tous les idents
For i = 1 To NumEntries(ListeIdents, Chr(0))
    '-> Get d'une d�finition
    DefIdent = Entry(i, ListeIdents, Chr(0))
    If Trim(DefIdent) = "" Then Exit For
    '-> Cr�er un icone pour l'ident sp�cifi�
    Set NodeRef = mdiConsole.TreeView1.Nodes.Add(, , "IDENT|" & Entry(1, DefIdent, "="), Entry(1, DefIdent, "=") & " - " & DeCrypt(Entry(2, DefIdent, "=")), "IDENT")
    NodeRef.Tag = "IDENT"
    '-> Cr�er la liste des menus pour cet ident
    Ident = Entry(1, DefIdent, "=")
    '-> Positionner le r�pertoire des menus de l'identifiant
    IdentMenuPath = ApplicationPath & Ident & "\Param\"
    '-> Charger la liste des menus
    FileName = Dir$(IdentMenuPath & "*.men")
    '-> Analyse r�cursive de tous les menus
    Do While FileName <> ""
        '-> Ajouter une icone dans le listView
        Set aNode = mdiConsole.TreeView1.Nodes.Add(NodeRef.Key, 4, Ident & "|" & FileName, Entry(1, FileName, "."), "MENU")
        '-> Lire l'entr�e suivante
        FileName = Dir
    Loop 'Pour tous les menus
Next 'Pour tous les idents


'-> Positionner le fichier des messprog
MessprogIniFile = ApplicationPath & "B3D\Param\Messprog-" & Format(CodeLangue, "00") & ".ini"

'-> Gestion des fichiers
mdiConsole.MnuAddMenuFile.Caption = GetMesseprog("MENU", "1")
mdiConsole.mnuDelMenuFile.Caption = GetMesseprog("MENU", "2")
mdiConsole.mnuEditMenuFile.Caption = GetMesseprog("MENU", "3")

'-> Gestion des menus
mdiConsole.MnuAddMenu.Caption = GetMesseprog("MENU", "1")
mdiConsole.mnuDelMenu.Caption = GetMesseprog("MENU", "2")
mdiConsole.mnuEditMenu.Caption = GetMesseprog("MENU", "3")
mdiConsole.mnuListeLangue.Caption = GetMesseprog("MENU", "5")
For i = 1 To 10
    mdiConsole.mnuLangue(i).Caption = GetMesseprog("PAYS", CStr(i))
Next
mdiConsole.mnuSave.Caption = GetMesseprog("MENU", "6")



End Sub


