VERSION 5.00
Begin VB.Form frmAddMenu 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   6285
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   9750
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6285
   ScaleWidth      =   9750
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   "Programme : "
      Height          =   1335
      Left            =   4920
      TabIndex        =   14
      Top             =   0
      Width           =   4815
      Begin VB.OptionButton Option7 
         Caption         =   "En dessous"
         Height          =   255
         Left            =   3120
         TabIndex        =   18
         Top             =   360
         Width           =   1335
      End
      Begin VB.OptionButton Option6 
         Caption         =   "En dernier"
         Height          =   255
         Left            =   1560
         TabIndex        =   17
         Top             =   840
         Width           =   1215
      End
      Begin VB.OptionButton Option5 
         Caption         =   "En premier"
         Height          =   255
         Left            =   1560
         TabIndex        =   16
         Top             =   360
         Width           =   1215
      End
      Begin VB.OptionButton Option4 
         Caption         =   "Apr�s"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   840
         Width           =   1095
      End
      Begin VB.OptionButton Option3 
         Caption         =   "Avant"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   360
         Value           =   -1  'True
         Width           =   1095
      End
   End
   Begin VB.CommandButton cmdOK 
      Default         =   -1  'True
      Height          =   615
      Left            =   9120
      Picture         =   "frmAddMenu.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   29
      Top             =   5640
      Width           =   615
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Height          =   615
      Left            =   8400
      Picture         =   "frmAddMenu.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   30
      Top             =   5640
      Width           =   615
   End
   Begin VB.Frame Frame4 
      Caption         =   "Frame4"
      Height          =   4095
      Left            =   4920
      TabIndex        =   43
      Top             =   1440
      Width           =   4815
      Begin VB.TextBox Text2 
         Height          =   285
         Index           =   9
         Left            =   1200
         TabIndex        =   28
         Top             =   3600
         Width           =   3375
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Index           =   8
         Left            =   1200
         TabIndex        =   27
         Top             =   3240
         Width           =   3375
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Index           =   7
         Left            =   1200
         TabIndex        =   26
         Top             =   2880
         Width           =   3375
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Index           =   6
         Left            =   1200
         TabIndex        =   25
         Top             =   2520
         Width           =   3375
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Index           =   5
         Left            =   1200
         TabIndex        =   24
         Top             =   2160
         Width           =   3375
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Index           =   4
         Left            =   1200
         TabIndex        =   23
         Top             =   1800
         Width           =   3375
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Index           =   3
         Left            =   1200
         TabIndex        =   22
         Top             =   1440
         Width           =   3375
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Index           =   2
         Left            =   1200
         TabIndex        =   21
         Top             =   1080
         Width           =   3375
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Index           =   1
         Left            =   1200
         TabIndex        =   20
         Top             =   720
         Width           =   3375
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Index           =   0
         Left            =   1200
         TabIndex        =   19
         Top             =   360
         Width           =   3375
      End
      Begin VB.Label Label2 
         Caption         =   "Zone 10 :"
         Height          =   255
         Index           =   9
         Left            =   240
         TabIndex        =   53
         Top             =   3600
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Zone 9 :"
         Height          =   255
         Index           =   8
         Left            =   240
         TabIndex        =   52
         Top             =   3240
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Zone 8 :"
         Height          =   255
         Index           =   7
         Left            =   240
         TabIndex        =   51
         Top             =   2880
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Zone 7 :"
         Height          =   255
         Index           =   6
         Left            =   240
         TabIndex        =   50
         Top             =   2520
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Zone 6 :"
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   49
         Top             =   2160
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Zone 5 :"
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   48
         Top             =   1800
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Zone 4 :"
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   47
         Top             =   1440
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Zone 3 :"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   46
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Zone 2 :"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   45
         Top             =   720
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Zone 1 :"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   44
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Libell� : "
      Height          =   4095
      Left            =   0
      TabIndex        =   32
      Top             =   1440
      Width           =   4815
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   9
         Left            =   1200
         TabIndex        =   12
         Top             =   3600
         Width           =   3375
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   8
         Left            =   1200
         TabIndex        =   11
         Top             =   3240
         Width           =   3375
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   7
         Left            =   1200
         TabIndex        =   10
         Top             =   2880
         Width           =   3375
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   6
         Left            =   1200
         TabIndex        =   9
         Top             =   2520
         Width           =   3375
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   5
         Left            =   1200
         TabIndex        =   8
         Top             =   2160
         Width           =   3375
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   4
         Left            =   1200
         TabIndex        =   7
         Top             =   1800
         Width           =   3375
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   3
         Left            =   1200
         TabIndex        =   6
         Top             =   1440
         Width           =   3375
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   2
         Left            =   1200
         TabIndex        =   5
         Top             =   1080
         Width           =   3375
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   1
         Left            =   1200
         TabIndex        =   4
         Top             =   720
         Width           =   3375
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   0
         Left            =   1200
         TabIndex        =   3
         Top             =   360
         Width           =   3375
      End
      Begin VB.Label Label1 
         Caption         =   "Fran�ais : "
         Height          =   255
         Index           =   9
         Left            =   240
         TabIndex        =   42
         Top             =   3600
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Fran�ais : "
         Height          =   255
         Index           =   8
         Left            =   240
         TabIndex        =   41
         Top             =   3240
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Fran�ais : "
         Height          =   255
         Index           =   7
         Left            =   240
         TabIndex        =   40
         Top             =   2880
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Fran�ais : "
         Height          =   255
         Index           =   6
         Left            =   240
         TabIndex        =   39
         Top             =   2520
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Fran�ais : "
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   38
         Top             =   2160
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Fran�ais : "
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   37
         Top             =   1800
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Fran�ais : "
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   36
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Fran�ais : "
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   35
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Fran�ais : "
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   34
         Top             =   720
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Fran�ais : "
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   33
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Type de menu  :"
      Height          =   1335
      Left            =   0
      TabIndex        =   31
      Top             =   0
      Width           =   4815
      Begin VB.TextBox Text3 
         Enabled         =   0   'False
         Height          =   285
         Left            =   1800
         TabIndex        =   2
         Top             =   840
         Width           =   2895
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Programme"
         Height          =   255
         Left            =   480
         TabIndex        =   1
         Top             =   840
         Width           =   1215
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Titre"
         Height          =   255
         Left            =   480
         TabIndex        =   0
         Top             =   360
         Value           =   -1  'True
         Width           =   1215
      End
      Begin VB.Image Image2 
         Height          =   240
         Left            =   120
         Picture         =   "frmAddMenu.frx":0E14
         Top             =   360
         Width           =   240
      End
      Begin VB.Image Image1 
         Height          =   240
         Left            =   120
         Picture         =   "frmAddMenu.frx":119E
         Top             =   840
         Width           =   240
      End
   End
End
Attribute VB_Name = "frmAddMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-> Indique le mode associ� � l'�cran
Public ActionMode As Integer
Public NodeMode As String
'-> Si on est en mode edit, pointeur objet
Private MyMenu As clsMenu
'-> Pointeur vers la feuille associ�e
Public aFrm As frmMenu

Private Sub cmdCancel_Click()
Unload Me
End Sub

Public Sub Init(aMenu As clsMenu)

'---> Charger le param�trage

Dim i As Integer

'-> Bloquer
Me.Option1.Enabled = False
Me.Option2.Enabled = False

'-> Poser la valeur
If aMenu.TypeMenu = 0 Then
    Me.Option1.Value = True
    Me.Text3.Enabled = False
Else
    Me.Option2.Value = True
    Me.Text3.Enabled = True
    Me.Text3.Text = aMenu.Programme
End If

'-> Bloquer le positionnement
Me.Frame2.Enabled = False
frmAddMenu.Option3.Enabled = False
frmAddMenu.Option4.Enabled = False
frmAddMenu.Option5.Enabled = False
frmAddMenu.Option6.Enabled = False
frmAddMenu.Option7.Enabled = False

'-> Setting des libell�s
For i = 1 To 10
    Me.Text1(i - 1).Text = aMenu.Libel(i)
Next

'-> Setting des zones charges
For i = 1 To 10
    Me.Text2(i - 1).Text = aMenu.ZoneCharge(i)
Next

'-> Positionner la valeur
Set MyMenu = aMenu

End Sub

Private Sub cmdOK_Click()

Dim aMenu As clsMenu
Dim MenuRef As clsMenu
Dim i As Integer
Dim intAdd As Integer
Dim NodeRef As Node
Dim NewNode As Node

'-> Pointer sur sa repr�sentattion
Set NodeRef = aFrm.TreeView1.Nodes(Me.NodeMode)

If ActionMode = 0 Then
    '-> Pointer sur le menu de r�f�rence
    For i = 1 To NumEntries(Me.NodeMode, "~")
        If i = 1 Then
            Set MenuRef = aFrm.Menus(Entry(1, Me.NodeMode, "~"))
        Else
            Set MenuRef = MenuRef.Menus(Entry(i, Me.NodeMode, "~"))
        End If
    Next 'Pour tous les niveaux
    
    '-> Ajouter le nouveau menu
    Set aMenu = New clsMenu
    '-> Positionner ses propri�t�s
    aMenu.Code = "MENU|" & GetTickCount()
    If Me.Option2.Value Then aMenu.TypeMenu = 1
Else
    Set aMenu = MyMenu
End If

'-> Setting de son libell�
For i = 1 To 10
    aMenu.Libel(i) = Me.Text1(i - 1).Text
Next

'-> Setting du zone charge
For i = 1 To 10
    aMenu.ZoneCharge(i) = Me.Text2(i - 1).Text
Next

'-> Ajouter son programme
aMenu.Programme = Me.Text3.Text

If ActionMode = 0 Then
    '-> Ajouter dans la collection
    MenuRef.Menus.Add aMenu, aMenu.Code
    '-> Ajouter le node dans la fen�tre m�re
    If Me.Option3.Value Then
        intAdd = 3
    ElseIf Me.Option4.Value Then
        intAdd = 2
    ElseIf Me.Option5.Value Then
        intAdd = 0
    ElseIf Me.Option6.Value Then
        intAdd = 1
    Else
        intAdd = 4
    End If
    
    '-> Ajouter le node au bon endroit
    Set NewNode = aFrm.TreeView1.Nodes.Add(NodeRef.Key, intAdd, Me.NodeMode & "~" & aMenu.Code, aMenu.Libel(aFrm.Langue))
    
    '-> Selon prog ou Titre
    If Me.Option1.Value Then
        NewNode.Tag = "TITRE"
        NewNode.Image = "CLOSE"
        NewNode.ExpandedImage = "OPEN"
    Else
        NewNode.Tag = "PROG"
        NewNode.Image = "APP"
    End If
    
    '-> Afficher le node
    NewNode.Selected = True
    NewNode.EnsureVisible
Else
    '-> Mettre � jour son libelle
    NodeRef.Text = aMenu.Libel(aFrm.Langue)
End If

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Form_Load()

Dim i As Integer

'-> Gestion des messprog
Me.Caption = GetMesseprog("FRMIDENT", "33")

Me.Frame1.Caption = GetMesseprog("FRMIDENT", "37")
Me.Option1.Caption = GetMesseprog("FRMIDENT", "34")
Me.Option2.Caption = GetMesseprog("FRMIDENT", "35")

Me.Frame2.Caption = GetMesseprog("FRMIDENT", "36")

Me.Frame3.Caption = GetMesseprog("FRMIDENT", "38")
For i = 1 To 10
    Me.Label1(i - 1).Caption = GetMesseprog("PAYS", CStr(i))
Next

Me.Frame4.Caption = GetMesseprog("FRMIDENT", "39")

Me.Option3.Caption = GetMesseprog("FRMIDENT", "40")
Me.Option4.Caption = GetMesseprog("FRMIDENT", "41")
Me.Option5.Caption = GetMesseprog("FRMIDENT", "42")
Me.Option6.Caption = GetMesseprog("FRMIDENT", "43")
Me.Option7.Caption = GetMesseprog("FRMIDENT", "44")


End Sub


Private Sub Option1_Click()
Me.Text3.Enabled = False
End Sub

Private Sub Option2_Click()
Me.Text3.Enabled = True
End Sub

Private Sub Text1_GotFocus(Index As Integer)
SelectTxtBox Me.Text1(Index)
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
Select Case KeyAscii
    Case 61, 124, 126
        KeyAscii = 0
End Select
End Sub
Private Sub Text2_KeyPress(Index As Integer, KeyAscii As Integer)
Select Case KeyAscii
    Case 61, 124, 126
        KeyAscii = 0
End Select
End Sub
Private Sub Text2_GotFocus(Index As Integer)
SelectTxtBox Me.Text2(Index)
End Sub
Private Sub Text3_GotFocus()
SelectTxtBox Me.Text3
End Sub
Private Sub Text3_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
    Case 61, 124, 126
        KeyAscii = 0
End Select
End Sub
