VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Code As String
Public TypeMenu As Integer '0-> Titre , 1 -> Prog
Public Programme As String 'Nom du programme attach�
Public Root As Boolean  'Niveai maxi

'-> Pour gestion d'une matrice de libell�s
Private pLibel(1 To 10) As String
'-> Pour gestion d'une matrice des zones charges
Private pZone(1 To 10) As String

'-> Collection des sous menus
Public Menus As Collection

Public Property Get Libel(Index As Integer) As String
    Libel = pLibel(Index)
End Property

Public Property Let Libel(Index As Integer, ByVal vNewValue As String)
    pLibel(Index) = vNewValue
End Property

Public Property Get ZoneCharge(Index As Integer) As String
    ZoneCharge = pZone(Index)
End Property

Public Property Let ZoneCharge(Index As Integer, ByVal vNewValue As String)
    pZone(Index) = vNewValue
End Property

Private Sub Class_Initialize()
'-> Initialiser la collection des sous menus
Set Menus = New Collection
End Sub

Private Sub Class_Terminate()
'-> Lib�rer la collection des sous menus
Set Menus = New Collection
End Sub
