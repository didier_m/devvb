VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.MDIForm mdiConsole 
   BackColor       =   &H8000000C&
   Caption         =   "Deal Informatique"
   ClientHeight    =   7125
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9465
   Icon            =   "frmConsole.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   3
      Top             =   6750
      Width           =   9465
      _ExtentX        =   16695
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13626
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            TextSave        =   "10/04/2002"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picNaviga 
      Align           =   3  'Align Left
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   6750
      Left            =   0
      ScaleHeight     =   450
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   232
      TabIndex        =   1
      Top             =   0
      Width           =   3480
      Begin MSComctlLib.TreeView TreeView1 
         Height          =   2655
         Left            =   120
         TabIndex        =   4
         Top             =   480
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   4683
         _Version        =   393217
         LabelEdit       =   1
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   1200
         Top             =   4080
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   2
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":0CCA
               Key             =   "MENU"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":19A4
               Key             =   "IDENT"
            EndProperty
         EndProperty
      End
      Begin VB.PictureBox picTitre 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         Height          =   90
         Left            =   360
         ScaleHeight     =   6
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   128
         TabIndex        =   2
         Top             =   240
         Width           =   1920
      End
      Begin VB.Image imgClose 
         Height          =   165
         Left            =   2040
         Picture         =   "frmConsole.frx":267E
         Top             =   120
         Width           =   165
      End
   End
   Begin VB.PictureBox picSplit 
      Align           =   3  'Align Left
      BorderStyle     =   0  'None
      Height          =   6750
      Left            =   3480
      MousePointer    =   9  'Size W E
      ScaleHeight     =   6750
      ScaleWidth      =   105
      TabIndex        =   0
      Top             =   0
      Width           =   105
   End
   Begin VB.Menu mnuGestMenu 
      Caption         =   "MAIN"
      Visible         =   0   'False
      Begin VB.Menu MnuAddMenuFile 
         Caption         =   ""
      End
      Begin VB.Menu mnuEditMenuFile 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelMenuFile 
         Caption         =   ""
      End
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "MENU"
      Visible         =   0   'False
      Begin VB.Menu MnuAddMenu 
         Caption         =   ""
      End
      Begin VB.Menu mnuEditMenu 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelMenu 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuListeLangue 
         Caption         =   ""
         Begin VB.Menu mnuLangue 
            Caption         =   ""
            Index           =   1
         End
         Begin VB.Menu mnuLangue 
            Caption         =   ""
            Index           =   2
         End
         Begin VB.Menu mnuLangue 
            Caption         =   ""
            Index           =   3
         End
         Begin VB.Menu mnuLangue 
            Caption         =   ""
            Index           =   4
         End
         Begin VB.Menu mnuLangue 
            Caption         =   ""
            Index           =   5
         End
         Begin VB.Menu mnuLangue 
            Caption         =   ""
            Index           =   6
         End
         Begin VB.Menu mnuLangue 
            Caption         =   ""
            Index           =   7
         End
         Begin VB.Menu mnuLangue 
            Caption         =   ""
            Index           =   8
         End
         Begin VB.Menu mnuLangue 
            Caption         =   ""
            Index           =   9
         End
         Begin VB.Menu mnuLangue 
            Caption         =   ""
            Index           =   10
         End
      End
      Begin VB.Menu mnuSep5 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSave 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "mdiConsole"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Function CreateFile(FileToSet As String)

Dim hdlFile As Integer

hdlFile = FreeFile
Open FileToSet For Output As #hdlFile
Close #hdlFile

End Function

Private Sub EditMenuFile()

'---> Cette proc�dure edit un menu

Dim MenuFile As String
Dim ListeMenu As String
Dim i As Integer, j As Integer
Dim aFrm As frmMenu
Dim aNode As Node
Dim DefMenu As String
Dim ParentMenu As String
Dim TypeMenu As Integer
Dim LibMenu As String
Dim ZoneMenu As String
Dim aMenu As clsMenu
Dim MenuRef As clsMenu
Dim pMenu As String
Dim ProgMenu As String
Dim KeyNewMenu As String
Dim DecryptMenu As String

'-> V�rifier si la feuille n'est pas d�ja charg�e
If Me.TreeView1.SelectedItem.Tag <> "" Then
    If IsWindow(CLng(Me.TreeView1.SelectedItem.Tag)) = 1 Then
        '-> Activer le feuille
        SendMessage CLng(Me.TreeView1.SelectedItem.Tag), WM_CHILDACTIVATE, 0, 0
        Exit Sub
    End If
End If

'-> Positionner la variable d'acc�s
IdentMenuPath = ApplicationPath & Entry(2, Me.TreeView1.SelectedItem.Parent.Key, "|") & "\Param\"

'-> Cr�er le nom du menu
MenuFile = IdentMenuPath & Entry(2, Me.TreeView1.SelectedItem.Key, "|")

'-> Bloquer la mise � jour de l'�cran
LockWindowUpdate mdiConsole.hwnd

'-> Charger la feuille de gestion des menus
Set aFrm = New frmMenu

'-> Positionner le nom du fichier menu
aFrm.MenuFile = MenuFile
aFrm.Init

'-> R�cup�rer la liste des menus
ListeMenu = GetIniFileValue("MENU", "", MenuFile, True)

'-> Pointer sur le menu de r�f�rence
Set MenuRef = aFrm.Menus("NODEROOT")

'-> Tester qu'il y ait quelquechose
If Trim(ListeMenu) <> "" Then
    '-> Analyse de tous les niveaux de menus
    For i = 1 To NumEntries(ListeMenu, Chr(0))
        '-> Get d'une d�finition
        DefMenu = Entry(i, ListeMenu, Chr(0))
        '-> Quitter si plus rien
        If Trim(DefMenu) = "" Then Exit For
        
        '-> R�cup�rer la valeur non crypt�e de la zone
        pMenu = DeCrypt(Mid$(DefMenu, Len(Entry(1, DefMenu, "=")) + 2))
        
        '-> R�cup�rer sa valeur non crypt�e
        'pMenu = DeCrypt(GetIniFileValue("MENU", Entry(1, DefMenu, "="), MenuFile))
        
        '-> Eclater en param�tres
        'pMenu = DeCrypt(Entry(2, DefMenu, "="))
        ParentMenu = Trim(Entry(1, pMenu, "�"))
        TypeMenu = Entry(2, pMenu, "�")
        ProgMenu = Entry(3, pMenu, "�")
        LibMenu = Entry(4, pMenu, "�")
        ZoneMenu = Entry(5, pMenu, "�")
        KeyNewMenu = Entry(6, pMenu, "�")
        
        '-> Cr�er un nouvel objet menu
        Set aMenu = New clsMenu
        '-> Init de ses propri�t�s
        aMenu.Code = KeyNewMenu
        aMenu.TypeMenu = CInt(TypeMenu)
        aMenu.Programme = ProgMenu
        
        '-> Pointer sur le menu de r�f�rence
        For j = 1 To NumEntries(ParentMenu, "~")
            If j = 1 Then
                Set MenuRef = aFrm.Menus(Entry(1, ParentMenu, "~"))
            Else
                Set MenuRef = MenuRef.Menus(Entry(j, ParentMenu, "~"))
            End If
        Next 'Pour tous les niveaux

        '-> Ajouter le menu dans la collection
        MenuRef.Menus.Add aMenu, KeyNewMenu
        
        '-> Libelle
        For j = 1 To NumEntries(LibMenu, "~")
            aMenu.Libel(j) = Entry(j, LibMenu, "~")
        Next
        '-> Zone charge
        For j = 1 To NumEntries(ZoneMenu, "~")
            aMenu.ZoneCharge(j) = Entry(j, ZoneMenu, "~")
        Next
        
        '-> Cr�er un nouveau node
        Set aNode = aFrm.TreeView1.Nodes.Add(ParentMenu, 4, ParentMenu & "~" & KeyNewMenu)
                            
        '-> Positionner le libelle
        aNode.Text = Entry(1, LibMenu, "~")
        
        '-> Positionner les icones
        If aMenu.TypeMenu = 0 Then
            aNode.Image = "CLOSE"
            aNode.ExpandedImage = "OPEN"
            aNode.Tag = "TITRE"
        Else
            aNode.Image = "APP"
            aNode.Tag = "PROG"
        End If
                                    
    Next 'Pour tous les niveaux de menu
End If 'S'il y a des menus

'-> Se positionner sur le premier node
Set aNode = aFrm.TreeView1.Nodes("NODEROOT")
aNode.Selected = True
If aNode.Children <> 0 Then aNode.Expanded = True

'-> Positionner le handle de la feuille
Me.TreeView1.SelectedItem.Tag = aFrm.hwnd

'-> D�bloquer la mise � jour de l'�cran
LockWindowUpdate 0



End Sub

Private Sub DelMenuFile()

'---> Cette prco�dure supprime un fichier menu

Dim Rep As VbMsgBoxResult

'-> Positionner la variable d'acc�s
IdentMenuPath = ApplicationPath & Entry(2, Me.TreeView1.SelectedItem.Parent.Key, "|") & "\Param\"

'-> Demander confirmation
Rep = MsgBox(GetMesseprog("APPLIC", "18") & Chr(13) & Entry(2, Me.TreeView1.SelectedItem.Key, "|"), vbQuestion + vbYesNo, GetMesseprog("MENU", "2"))
If Rep = vbNo Then Exit Sub

'-> Supprimer le fichier
If Dir$(IdentMenuPath & Entry(2, Me.TreeView1.SelectedItem.Key, "|")) <> "" Then Kill IdentMenuPath & Entry(2, Me.TreeView1.SelectedItem.Key, "|")

'-> Si la feuille est ouverte : la fermer
If Me.TreeView1.SelectedItem.Tag <> "" Then
    '-> Fermer la fen�tre
    SendMessage CLng(Me.TreeView1.SelectedItem.Tag), WM_CLOSE, 0, 0
End If

'-> Supprimer le listview
Me.TreeView1.Nodes.Remove (Me.TreeView1.SelectedItem.Key)


End Sub


Private Sub AddMenuFile()

'---> Cette proc�dure ajoute un fichier de menu

Dim Rep As String
Dim hdlFile As Integer
Dim x As Node
Dim aFrm As frmMenu
Dim ParentNode As Node

'-> Positionner la variable d'acc�s
IdentMenuPath = ApplicationPath & Entry(2, Me.TreeView1.SelectedItem.Key, "|") & "\Param\"

'-> Demander le niom du nouveau fichier
Rep = InputBox(GetMesseprog("APPLIC", "1"), GetMesseprog("MENU", "1"), "")
If Trim(Rep) = "" Then Exit Sub

'-> V�rifier que le code soit correct
If InStr(1, Rep, "_") <> 0 Or Not IsLegalName(Rep) Then
    MsgBox Replace(GetMesseprog("APPLIC", "3"), "#CODE#", Rep), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    Exit Sub
End If

'-> V�rifier si le code saisi n'existe pas d�ja
If Dir$(IdentMenuPath & Rep & ".men") <> "" Then
    MsgBox GetMesseprog("APPLIC", "2"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    Exit Sub
End If

'-> Cr�er le nouveau fichier
hdlFile = FreeFile
Open IdentMenuPath & Rep & ".men" For Output As #hdlFile

'-> Positionner la premi�re zone
Print #hdlFile, "[MENU]"

'-> Fermer le fichier
Close #hdlFile

'-> Ajouter une icone dans le listview
Set x = Me.TreeView1.Nodes.Add(Me.TreeView1.SelectedItem.Root.Key, 4, Entry(2, Me.TreeView1.SelectedItem.Root.Key, "|") & Rep & ".men", Rep, "MENU")
x.Tag = "MENU"

'-> Charger la feuille
Set aFrm = New frmMenu

'-> Initialiser
aFrm.MenuFile = IdentMenuPath & Rep & ".men"
aFrm.Init
aFrm.ToSave = False

'-> Afficher la gestion des menus
aFrm.Show

'-> Positionner le handle
x.Tag = aFrm.hwnd

End Sub





Private Sub MnuAddMenu_Click()
strRetour = "ADD"
End Sub

Private Sub MnuAddMenuFile_Click()
strRetour = "ADD"
End Sub

Private Sub mnuDelMenu_Click()
strRetour = "DEL"
End Sub

Private Sub mnuDelMenuFile_Click()
strRetour = "DEL"
End Sub

Private Sub mnuEditMenu_Click()
strRetour = "EDIT"
End Sub

Private Sub mnuEditMenuFile_Click()
strRetour = "EDIT"
End Sub

Public Sub mnuLangue_Click(Index As Integer)

Dim i As Integer

'-> Supprimer le check
For i = 1 To 10
    mnuLangue(i).Checked = False
Next

'-> Ch�cker
mnuLangue(Index).Checked = True

'-> Positionner la variable de retour
strRetour = "LANGUE|" & CStr(Index)

End Sub

Private Sub mnuSave_Click()
strRetour = "SAVE"
End Sub

Private Sub picNaviga_Resize()

Dim aRect As RECT
Dim res As Long

On Error Resume Next

'-> R�cup�rer la taille de la zone cliente
res = GetClientRect(Me.picNaviga.hwnd, aRect)

'-> Positionner l'entete de la fen�tre
Me.picTitre.Left = 3
Me.picTitre.Top = 7
Me.picTitre.Width = aRect.Right - 6 - imgClose.Width
Me.imgClose.Left = aRect.Right - imgClose.Width
Me.imgClose.Top = Me.picTitre.Top - 2

'-> Positionner le treeview
Me.TreeView1.Left = Me.picTitre.Left
Me.TreeView1.Top = Me.picTitre.Top * 2 + Me.picTitre.Height
Me.TreeView1.Width = Me.picTitre.Width + Me.imgClose.Width + 6
Me.TreeView1.Height = Me.picNaviga.ScaleY(Me.picNaviga.Height, 1, 3) - 21
End Sub
Private Sub picTitre_Resize()

'---> Dessiner la barre de titre
Me.picTitre.Line (0, 0)-(Me.picTitre.Width - 1, 0), RGB(255, 255, 255)
Me.picTitre.Line (0, 1)-(Me.picTitre.Width - 1, 1), RGB(255, 255, 255)
Me.picTitre.Line (1, 1)-(Me.picTitre.Width - 1, 1), &HE0E0E0
Me.picTitre.Line (0, 3)-(Me.picTitre.Width - 1, 3), RGB(255, 255, 255)
Me.picTitre.Line (0, 4)-(Me.picTitre.Width - 1, 4), RGB(255, 255, 255)
Me.picTitre.Line (1, 4)-(Me.picTitre.Width - 1, 4), &HE0E0E0

End Sub
Private Sub picSplit_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim aPt As POINTAPI
Dim res As Long

If Button = vbLeftButton Then
    '-> R�cup�rer la position du curseur
    res = GetCursorPos(aPt)
    '-> Convertir en coordonn�es clientes
    res = ScreenToClient(Me.hwnd, aPt)
    '-> Tester les positions mini et maxi
    If aPt.x > Me.picNaviga.ScaleX(2, 7, 3) And aPt.x < Me.picNaviga.ScaleX(Me.Width, 1, 3) - Me.picNaviga.ScaleX(2, 7, 3) Then Me.picNaviga.Width = Me.picNaviga.ScaleX(aPt.x, 3, 1)
End If

End Sub


Private Sub TreeView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

'---> Gestion des menus

'-> Ne rien faire si pas de bouton droit
If Button <> vbRightButton Then Exit Sub

'-> Tester que l'on se trouve sur un menu
If Me.TreeView1.SelectedItem.Tag = "IDENT" Then
    '-> Autorisation des menus
    Me.mnuDelMenuFile.Enabled = False
    Me.mnuEditMenuFile.Enabled = False
Else
    Me.mnuDelMenuFile.Enabled = True
    Me.mnuEditMenuFile.Enabled = True
End If

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu Me.mnuGestMenu

'-> Selon le type de retour
Select Case strRetour
    Case "ADD"
        Call AddMenuFile
    Case "EDIT"
        Call EditMenuFile
    Case "DEL"
        Call DelMenuFile
End Select

'-> Vider la variable de retour
strRetour = ""

End Sub
