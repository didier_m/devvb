VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmMenu 
   Caption         =   "Form1"
   ClientHeight    =   7740
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11055
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   516
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   737
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7200
      Top             =   960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenu.frx":0000
            Key             =   "OPEN"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenu.frx":039A
            Key             =   "CLOSE"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenu.frx":0734
            Key             =   "APP"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   6735
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6975
      _ExtentX        =   12303
      _ExtentY        =   11880
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
End
Attribute VB_Name = "frmMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Code langue d'affichage du menu
Public Langue As Integer
'-> Collection des menus
Public Menus As Collection
'-> Indique que l'on doit enregistrer
Public ToSave As Boolean
'-> Indique le fichier menu associ�
Public MenuFile As String

Public Sub Init()

'---> Initialisation du menu pour cet Ident

Dim aNode As Node
Dim aMenu As clsMenu
Dim i As Integer

'-> De base CodeLangue
Langue = 1
mdiConsole.mnuLangue_Click 1

'-> Titre de la feuille
Me.Caption = GetMesseprog("APPLIC", "5") & " - " & MenuFile

'-> Dans un premier temps, cr�er le node root
Set aNode = Me.TreeView1.Nodes.Add(, , "NODEROOT", GetMesseprog("APPLIC", "6"), "CLOSE")
aNode.ExpandedImage = "OPEN"
aNode.Tag = "NODEROOT"

'-> Intialiser la collection des menus
Set Menus = New Collection

'-> Ajouter le premier niveau de menu
Set aMenu = New clsMenu
aMenu.Code = "NODEROOT"
aMenu.TypeMenu = 0
aMenu.Root = True
Menus.Add aMenu, "NODEROOT"
For i = 1 To 10
    aMenu.Libel(i) = GetMesseprog("APPLIC", "6")
Next

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

Dim Rep As VbMsgBoxResult

'-> Si on doit enregistrer, demander confirmation
If ToSave Then
    '-> Demander confirmation
    Rep = MsgBox(GetMesseprog("MENU", "6") & Chr(13) & Me.MenuFile, vbExclamation + vbYesNoCancel, GetMesseprog("MENU", "6"))
    Select Case Rep
        Case vbNo
            '-> Ne rien faire
        Case vbYes
            '-> Enregistrer
            Call SaveMenu
        Case vbCancel
            Cancel = 1
    End Select
End If 'Si on doit enregistrer


End Sub

Private Sub Form_Resize()

Dim aRect As RECT

GetClientRect Me.hwnd, aRect
Me.TreeView1.Left = 0
Me.TreeView1.Top = 0
Me.TreeView1.Height = aRect.Bottom
Me.TreeView1.Width = aRect.Right
End Sub

Private Sub TreeView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

'-> Ne rien faire si pas le bon bouton
If Button <> vbRightButton Then Exit Sub

'-> Gestion des menus
If Me.TreeView1.SelectedItem.Key = "NODEROOT" Then
    '-> Bloquer
    mdiConsole.mnuDelMenu.Enabled = False
    mdiConsole.mnuEditMenu.Enabled = False
Else
    '-> Bloquer
    mdiConsole.mnuDelMenu.Enabled = True
    mdiConsole.mnuEditMenu.Enabled = True
End If

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu mdiConsole.mnuMenu

'-> Tester le retour
Select Case Entry(1, strRetour, "|")
    Case "ADD"
        '-> Ajouter un nouveau menu
        Call AddMenu
    Case "DEL"
        '-> Supprimer le menu sp�cifi�
        Call DelMenu
    Case "EDIT"
        Call EditMenu
    Case "LANGUE"
        '-> Positionner le code langue
        Langue = CInt(Entry(2, strRetour, "|"))
        '-> Afficher les libell�s par langue
        SetLibelByLangue
    Case "SAVE"
        Call SaveMenu
End Select

End Sub

Private Sub SetLibelByLangue()

Dim aNode As Node
Dim aMenu As clsMenu
Dim i As Integer

For Each aNode In Me.TreeView1.Nodes
    '-> Pointer sur le menu associ�
    For i = 1 To NumEntries(aNode.Key, "~")
        If i = 1 Then
            Set aMenu = Me.Menus(Entry(1, aNode.Key, "~"))
        Else
            Set aMenu = aMenu.Menus(Entry(i, aNode.Key, "~"))
        End If
    Next 'Pour tous les niveaux
    '-> Positionner son code langue
    aNode.Text = aMenu.Libel(Langue)
Next

End Sub
Private Sub SaveMenu()

'---> Cette proc�dure enregistre un menu

Dim hdlFile As Integer
Dim aMenu As clsMenu
Dim Ligne As String
Dim i As Integer, j As Integer
Dim aNode As Node
Dim ListeNode As String
Dim TempNode As String
Dim MenuKey As String

'-> Ouvrir le fichier
hdlFile = FreeFile
Open MenuFile For Output As #hdlFile

'-> Imprimer l'entete de section
Print #hdlFile, "[MENU]"

'-> Cr�er dans un premier temps la liste de tous les index
For Each aNode In Me.TreeView1.Nodes
    '-> Ne pas traiter le node Root
    If aNode.Key = "NODEROOT" Then GoTo NextNode
    If TempNode = "" Then
        TempNode = aNode.Index
    Else
        TempNode = TempNode & "|" & aNode.Index
    End If
NextNode:
Next 'Pour tous les nodes

'-> Ne rien faire si pas de node
If TempNode = "" Then GoTo EndProc
    
'-> Analyse de tous les index de nodes
For i = 1 To Me.TreeView1.Nodes.Count - 1
    '-> Tester si le champ est d�ja r�f�renc� dans la matrice
    If Not IsInMatrice(CStr(Entry(i, TempNode, "|")), ListeNode) Then
        '-> On l'ajoute dans la matrice finale
        If ListeNode = "" Then
            ListeNode = CStr(Entry(i, TempNode, "|"))
        Else
            ListeNode = ListeNode & "|" & CStr(Entry(i, TempNode, "|"))
        End If
    End If '-> Si le node est d�ja r�f�renc� dans la matrice
    
    '-> Analyser ses enfants
    strRetour = GetChildIndex(Me.TreeView1.Nodes(CInt(Entry(i, TempNode, "|"))))
    
    '-> Ajouter � la liste des index d�ja analys�s
    If strRetour <> "" Then
        '-> Analyse de la liste
        For j = 1 To NumEntries(strRetour, "|")
            '-> Ajouter que si pas d�ja dans la matrice
            If Not IsInMatrice(CStr(Entry(j, strRetour, "|")), ListeNode) Then
                '-> On l'ajoute dans la matrice finale
                If ListeNode = "" Then
                    ListeNode = CStr(Entry(j, strRetour, "|"))
                Else
                    ListeNode = ListeNode & "|" & CStr(Entry(j, strRetour, "|"))
                End If
            End If '-> Si le node est d�ja r�f�renc� dans la matrice
        Next 'Pour tous les enfants
    End If 'S'il y a des enfants
Next

'-> Enregistrer la liste des nodex
For i = 1 To NumEntries(ListeNode, "|")
    '-> Pointer sur le node
    Set aNode = Me.TreeView1.Nodes(CInt(Entry(i, ListeNode, "|")))
    '-> R�cup�rer un pointeur vers le menu associ�
    For j = 1 To NumEntries(aNode.Key, "~")
        If j = 1 Then
            Set aMenu = Me.Menus(Entry(1, aNode.Key, "~"))
        Else
            Set aMenu = aMenu.Menus(Entry(j, aNode.Key, "~"))
        End If
    Next 'Pour tous les niveaux
    '-> Parent
    MenuKey = aNode.Index & "="
    Ligne = aNode.Parent.Key & "�"
    '-> Type
    Ligne = Ligne & CStr(aMenu.TypeMenu) & "�" & aMenu.Programme & "�"
    '-> Libell�
    TempNode = ""
    For j = 1 To 10
        If j <> 10 Then
            TempNode = TempNode & aMenu.Libel(j) & "~"
        Else
            TempNode = TempNode & aMenu.Libel(j)
        End If
    Next
    Ligne = Ligne & TempNode & "�"
    '-> Zone charge
    TempNode = ""
    For j = 1 To 10
        If j <> 10 Then
            TempNode = TempNode & aMenu.ZoneCharge(j) & "~"
        Else
            TempNode = TempNode & aMenu.ZoneCharge(j)
        End If
    Next
    Ligne = Ligne & TempNode & "�" & aMenu.Code
    '-> Enregistrer la ligne
    Print #hdlFile, MenuKey & Crypt(Ligne)
Next

'-> Fin de proc�dure
EndProc:

'-> Fermer le fichier
Close #hdlFile

'-> Indiquer que l'on a enregistrer
ToSave = False

End Sub
Private Function GetChildIndex(aNode As Node) As String

'---> Cette proc�cure r�cup�re la liste des index des enfants nodes

Dim strReturn  As String
Dim i As Integer

'-> Ne rien faire si pas de nodes enfants
If aNode.Children = 0 Then Exit Function

i = aNode.Child.Index
If aNode.Children = 1 Then
    strReturn = CStr(aNode.Child.Index)
Else
    '-> Analyse des sous nodes
    Do While i <> aNode.Child.LastSibling.Index
        '-> Mise � jour des zones
        If strReturn = "" Then
            strReturn = CStr(i)
        Else
            strReturn = strReturn & "|" & CStr(i)
        End If
        '-> Get de l'index suivant
        i = Me.TreeView1.Nodes(i).Next.Index
    Loop
End If

'-> Renvoyer la chaine
GetChildIndex = strReturn

End Function
Private Function IsInMatrice(ToSearch As String, MaRef As String) As Boolean

Dim i As Integer

If MaRef = "" Then Exit Function

For i = 1 To NumEntries(MaRef, "|")
    If Entry(i, MaRef, "|") = ToSearch Then
        IsInMatrice = True
        Exit Function
    End If
Next

End Function

Private Sub EditMenu()

Dim aMenu As clsMenu
Dim i As Integer

'-> Pointer sur le menu sp�cifi�
For i = 1 To NumEntries(Me.TreeView1.SelectedItem.Key, "~")
    If i = 1 Then
        Set aMenu = Me.Menus(Entry(1, Me.TreeView1.SelectedItem.Key, "~"))
    Else
        Set aMenu = aMenu.Menus(Entry(i, Me.TreeView1.SelectedItem.Key, "~"))
    End If
Next 'Pour tous les niveaux

'-> Charger la feuille en m�moire
Load frmAddMenu

'-> Positionner les pointeurs
frmAddMenu.ActionMode = 1
frmAddMenu.NodeMode = Me.TreeView1.SelectedItem.Key
Set frmAddMenu.aFrm = Me

'-> Lancer l'initialisation
frmAddMenu.Init aMenu

'-> Afficher la feuille
frmAddMenu.Show vbModal

'-> Indiquer que l'on doit enregistrer
ToSave = True

End Sub

Private Sub DelMenu()

'---> Cette proc�dure supprime un niveau et tout ce qu'il contient
Dim Rep As VbMsgBoxResult
Dim aMenu As clsMenu
Dim i As Integer
Dim ParentMenu As Node

'-> Demander confirmation
Rep = MsgBox(GetMesseprog("COMMUN", "31") & " " & Me.TreeView1.SelectedItem.Text, vbQuestion + vbYesNo, GetMesseprog("COMMUN", "12"))
If Rep = vbNo Then Exit Sub

'-> R�cup�rer le node parent
Set ParentMenu = Me.TreeView1.SelectedItem.Parent

'-> Pointer sur le parent du menu sp�cifi�
For i = 1 To NumEntries(Me.TreeView1.SelectedItem.Key, "~") - 1
    If i = 1 Then
        Set aMenu = Me.Menus(Entry(1, Me.TreeView1.SelectedItem.Key, "~"))
    Else
        Set aMenu = aMenu.Menus(Entry(i, Me.TreeView1.SelectedItem.Key, "~"))
    End If
Next 'Pour tous les niveaux
'-> Supprimer le dernier niveau
aMenu.Menus.Remove (Entry(NumEntries(Me.TreeView1.SelectedItem.Key, "~"), Me.TreeView1.SelectedItem.Key, "~"))

'-> Supprimer le node
Me.TreeView1.Nodes.Remove Me.TreeView1.SelectedItem.Key

'-> Pointer sur le parent
ParentMenu.Selected = True
If ParentMenu.Children = 0 Then ParentMenu.Expanded = False

'-> Indiquer que l'on doit enregistrer
ToSave = True

End Sub

Private Sub AddMenu()

'---> Cette proc�dure ajoute un menu dans la feuille

'-> Charger la feuille d'ajout en m�moire
Load frmAddMenu

'-> Setting de ses param�tres
frmAddMenu.ActionMode = 0 'Ajout
frmAddMenu.NodeMode = Me.TreeView1.SelectedItem.Key
Set frmAddMenu.aFrm = Me

'-> Afficher la feuille
frmAddMenu.Show vbModal

'-> Indiquer que l'on doit enregistrer
ToSave = True

End Sub
