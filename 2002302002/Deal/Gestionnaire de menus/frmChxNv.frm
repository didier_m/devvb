VERSION 5.00
Begin VB.Form frmChxNv 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   5130
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4230
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5130
   ScaleWidth      =   4230
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command1 
      Height          =   615
      Left            =   3480
      Picture         =   "frmChxNv.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   4440
      Width           =   735
   End
   Begin VB.OptionButton Option5 
      Caption         =   "Option1"
      Height          =   735
      Left            =   1200
      TabIndex        =   9
      Top             =   3480
      Width           =   2895
   End
   Begin VB.OptionButton Option4 
      Caption         =   "Option1"
      Height          =   735
      Left            =   1200
      TabIndex        =   8
      Top             =   2640
      Width           =   2895
   End
   Begin VB.OptionButton Option3 
      Caption         =   "Option1"
      Height          =   735
      Left            =   1200
      TabIndex        =   7
      Top             =   1800
      Width           =   2895
   End
   Begin VB.OptionButton Option2 
      Caption         =   "Option1"
      Height          =   735
      Left            =   1200
      TabIndex        =   6
      Top             =   960
      Width           =   2895
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Option1"
      Height          =   735
      Left            =   1200
      TabIndex        =   5
      Top             =   120
      Width           =   2895
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   750
      Index           =   3
      Left            =   240
      Picture         =   "frmChxNv.frx":0CCA
      ScaleHeight     =   720
      ScaleWidth      =   720
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   3480
      Width           =   750
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   750
      Index           =   2
      Left            =   240
      Picture         =   "frmChxNv.frx":2994
      ScaleHeight     =   720
      ScaleWidth      =   720
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   2640
      Width           =   750
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   750
      Index           =   1
      Left            =   240
      Picture         =   "frmChxNv.frx":465E
      ScaleHeight     =   720
      ScaleWidth      =   720
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   1800
      Width           =   750
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   750
      Index           =   0
      Left            =   240
      Picture         =   "frmChxNv.frx":6328
      ScaleHeight     =   720
      ScaleWidth      =   720
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   960
      Width           =   750
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   750
      Left            =   240
      Picture         =   "frmChxNv.frx":7FF2
      ScaleHeight     =   720
      ScaleWidth      =   720
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   120
      Width           =   750
   End
End
Attribute VB_Name = "frmChxNv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

If Me.Option1.Value Then 'Premier
    strRetour = "1"
ElseIf Me.Option2.Value Then 'Avant
    strRetour = "3"
ElseIf Me.Option3.Value Then 'Apr�s
    strRetour = "2"
ElseIf Me.Option4.Value Then 'Dernier
    strRetour = "1"
Else 'Dessous
    strRetour = "4"
End If

'-> Fermer la feuille
Unload Me

End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("APPLIC", "11")

'-> Charger les libell�s des niveaux
Me.Option1.Caption = GetMesseprog("APPLIC", "14")
Me.Option2.Caption = GetMesseprog("APPLIC", "12")
Me.Option3.Caption = GetMesseprog("APPLIC", "13")
Me.Option4.Caption = GetMesseprog("APPLIC", "15")
Me.Option5.Caption = GetMesseprog("APPLIC", "16")

End Sub

Private Sub Option1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then Command1_Click
End Sub

Private Sub Option2_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then Command1_Click
End Sub

Private Sub Option3_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then Command1_Click
End Sub

Private Sub Option4_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then Command1_Click
End Sub

Private Sub Option5_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then Command1_Click
End Sub
