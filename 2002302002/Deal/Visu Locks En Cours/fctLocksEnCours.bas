Attribute VB_Name = "fctLocksEnCours"
Option Explicit

'-> Connexion en cours
Public aConnexion As B3DConnexion

'-> Chemin de turbo.exe
Public PrinterPath As String

'-> Chemin d'acc�s des maquettes
Public MaqPath As String

'-> API de temporisation
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
'-> API Pour gestion du temps
Public Declare Function GetTickCount& Lib "kernel32" ()

Sub Main()
'------------------------------------------------------------------
'---- Procedure de suppression des locks brules dans la base ------
'------------------------------------------------------------------
Dim ZonCharge As String
Dim ParamFile As String
Dim ListeField As String
Dim aIt As ListItem
Dim lpBuffer As String
Dim res As Long
Dim HdlFileI As Integer
Dim aBuffer As B3DBuffer
Dim i As Integer
Dim DmdUnlock(5000) As String
Dim OrdreOk As Boolean
Dim Ligne As String
Dim Rep As VbMsgBoxResult
Dim a As String
Dim b As String

On Error GoTo GestError

'-> Tester la ligne de commande
If Trim(Command$) = "" Then End

'-> Pointeur de souris
Screen.MousePointer = 11

'-> R�cup�rer les param�tres
ParamFile = Entry(1, Command$, "�")
ZonCharge = Entry(2, Command$, "�")

'-> Initialiser la connexion
Set aConnexion = New B3DConnexion

'-> Charger son param�trage
If Not aConnexion.ReadParamFormFile(ParamFile) Then GoTo GestError

'-> Emplacement de turbo
PrinterPath = aConnexion.ApplicationDir & "Outils\Turbograph.exe"

'-> Emplacement des maquettes
MaqPath = aConnexion.ApplicationDir & "B3D\Maq\"

'-> Poser le nom du programme
InitialiseB3D aConnexion, "LOCK_ANALYZER", False, False

'-> R�cup�rer le catalogue
If Not GetIdentCatalogue(aConnexion, aConnexion.Ident, "") Then
    '-> Afficher un message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbCritical + vbOKOnly, "Error"
    '-> Fin du programme
    GoTo GestError
End If

If Not aConnexion.InitDmd(True) Then
    '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
    MsgBox GetMesseprog("APPLIC", "51"), vbExclamation + vbOKOnly, "Fatal Error"
    GoTo GestError
End If

'-> On lance la fonction d'analyse des locks brul�es
OrdreOk = aConnexion.PutInstruction("B3D_GET_LOCK", True)

'-> Verifie si on a bien envoy� l'instruction
If Not OrdreOk Then
    '-> Positionner le code Erreur
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation + vbOKOnly, "Fatal Error"
    GoTo GestError
End If

'-> Envoyer l'instruction et charger la matrice des champs
Set aBuffer = New B3DBuffer
If Not aConnexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    aConnexion.ErrorNumber = "100000"
    aConnexion.ErrorLibel = "Error sending exchange"
    aConnexion.ErrorParam = "LoadAgentDOG"
    GoTo GestError
End If

'-> Recup de la 1ere ligne
Ligne = aBuffer.Lignes(1)

If Entry(2, Ligne, "~") <> "OK" Then
    MsgBox GetMesseprog("APPLIC", "62") & Chr(13) & Ligne, vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "63")
    GoTo GestError
End If

'-> On cree les colonnes
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "64")
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "65")
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "66")
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "67")
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "68")
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "69")
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("CONSOLE", "21")
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "70")
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "71")
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "19")
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "9")

'-> Analyse du retour des locks repertori�s
For i = 2 To aBuffer.Lignes.Count
    '-> Recup de la ligne
    Ligne = aBuffer.Lignes(i)
    
    '-> teste si bon ident
    If Entry(1, Ligne, "�") <> aConnexion.Ident Then GoTo NextLock
    
    '-> Analyse de la r�ponse et affichage ecran
    a = Entry(1, Ligne, "~")
    b = Entry(2, Ligne, "~")
    
    '-> Insertion dans le listview
    Set aIt = frmVisualisation.ListView1.ListItems.Add(, , Entry(2, a, "�"))
    aIt.SubItems(1) = Entry(3, a, "�")
    If Entry(4, a, "�") = "1" Then
        aIt.SubItems(2) = GetMesseprog("APPLIC", "72")
    Else
        aIt.SubItems(2) = GetMesseprog("APPLIC", "73")
    End If
    
    aIt.SubItems(3) = Entry(1, b, "�")
    aIt.SubItems(4) = Mid$(Entry(3, b, "�"), 7, 2) & "/" & Mid$(Entry(3, b, "�"), 5, 2) & "/" & Mid$(Entry(3, b, "�"), 1, 4)
    aIt.SubItems(5) = Format(Entry(4, b, "�"), "00:00:00")
    aIt.SubItems(6) = Entry(5, b, "�")
    
    If Entry(6, b, "�") <> "" Then
        aIt.SubItems(7) = Mid$(Entry(6, b, "�"), 7, 2) & "/" & Mid$(Entry(6, b, "�"), 5, 2) & "/" & Mid$(Entry(6, b, "�"), 1, 4)
    End If
    
    If Entry(7, b, "�") <> "" Then
        aIt.SubItems(8) = Format(Entry(7, b, "�"), "00:00:00")
    End If
    
    aIt.SubItems(9) = Entry(8, b, "�")
    aIt.SubItems(10) = Entry(9, b, "�")

NextLock:

Next

'-> Formattage listview de visu
FormatListView frmVisualisation.ListView1

'-> Affichage de la visu
Screen.MousePointer = 0
frmVisualisation.Show

Exit Sub

GestError:

    '-> Fermer tous les fichiers ouverts
    Reset
    '-> Pointeur de l'�cran
    Screen.MousePointer = 0
    End
        
End Sub

Public Sub PrintDataInTurbo(MaqName As String, DataFile As String, MergeFile As String, Delete As Boolean)
'---> Fonction qui edite des donn�es B3D en fonction d'une maquette turbo et
'---> d'un fichier de donn�es fusionn�s ensembles
'---> Propose aussi la suppression du spool et du fichier de fucion si demand� par programmeur
Dim HdlFile1 As Integer
Dim HdlFile2 As Integer
Dim HdlFile3 As Integer
Dim LigneI As String
Dim LigneE As String
Dim i As Integer


'-> Ouverture de la maquette
HdlFile1 = FreeFile
Open MaqName For Input As #HdlFile1
'-> Ouverture du fichier de donn�es
HdlFile2 = FreeFile
Open DataFile For Input As #HdlFile2
'-> Ouverture du fichier de fusion
HdlFile3 = FreeFile
Open MergeFile For Output As #HdlFile3

'-> Envoi du marqueur de debut de spool dans le fichier .TURBO
Print #HdlFile3, "[SPOOL]"
'-> Envoi du marqueur de debut de maquette dans le fichier .TURBO
Print #HdlFile3, "[MAQ]"

'-> On parcours la maquette et on la duplique dans notre fichier de fusion
Do While Not EOF(HdlFile1)
    '-> recup de la ligne de maquette
    Line Input #HdlFile1, LigneI
    '-> Ecriture de cette ligne dans le fichier de fusion
    Print #HdlFile3, LigneI
Loop
'-> Envoi du marqueur de fin de maquette dans le fichier .TURBO
Print #HdlFile3, "[/MAQ]"

'-> On commence a envoyer dans le fichier de fusion les donn�es du fichier de datas
Do While Not EOF(HdlFile2)
    '-> Recup d'une ligne de data
    Line Input #HdlFile2, LigneI
    '-> ecriture dans le fichier
    Print #HdlFile3, LigneI
Loop

'-> Envoi du marqueur de fin de spool dans le fichier .TURBO
Print #HdlFile3, "[/SPOOL]"

'-> On ferme les canaux
Close #HdlFile1
Close #HdlFile2
Close #HdlFile3

'->on lance l'edition par turbograph
Shell PrinterPath & " " & MergeFile, vbMaximizedFocus

End Sub



