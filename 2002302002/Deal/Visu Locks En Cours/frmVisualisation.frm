VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmVisualisation 
   Caption         =   "Visualisation des Locks en Cours"
   ClientHeight    =   5895
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13245
   Icon            =   "frmVisualisation.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   393
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   883
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView ListView1 
      Height          =   3255
      Left            =   1200
      TabIndex        =   0
      Top             =   480
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   5741
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Menu mnuPopUp 
      Caption         =   "PopUp"
      Visible         =   0   'False
      Begin VB.Menu mnuprint 
         Caption         =   ""
      End
      Begin VB.Menu mnusep0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuZoom 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmVisualisation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()

'-> titre de la feuille
Me.Caption = GetMesseprog("APPLIC", "75")

'-> Menus de la feuille
Me.mnuprint.Caption = GetMesseprog("MENU", "7")
Me.mnuZoom.Caption = GetMesseprog("MENU", "4")

End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next
GetClientRect Me.hwnd, aRect
Me.ListView1.Left = 0
Me.ListView1.Top = 0
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom

End Sub
Private Sub listview1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    '-> Trier sur les entetes de colonne
    Me.ListView1.SortKey = ColumnHeader.Index - 1
    If Me.ListView1.SortOrder = lvwAscending Then
        Me.ListView1.SortOrder = lvwDescending
    Else
        Me.ListView1.SortOrder = lvwAscending
    End If
    Me.ListView1.Sorted = True
End Sub

Private Sub ListView1_DblClick()

'---> Branchement du zomm sur l'enreg en cours
Dim aDescro As B3DFnameDescriptif

'-> R�cup�rer les descriptif de la table
Set aDescro = GetFnameDescriptif(aConnexion, aConnexion.Ident, Trim(Me.ListView1.SelectedItem.Text))
                        
If aDescro Is Nothing Then
    '-> Afficher un message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbCritical + vbOKOnly, "Error"
    '-> Fin du programme
    End
End If
'-> Affichage du zoom
DisplayZoom aConnexion, Trim(Me.ListView1.SelectedItem.Text), Trim(Me.ListView1.SelectedItem.SubItems(1)), False, True, aDescro, Me, True

End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'---> gestion des popup

Dim i As Integer

'-> Sort si bouton droit
If Button <> 2 Then Exit Sub

'-> Verif s'il y a des locks
If Me.ListView1.ListItems.Count = 0 Then Exit Sub
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> remise a blanc  de la variable d'echange
strRetour = ""

'-> Affichage du menu popup
Me.PopupMenu Me.mnuPopUp

Select Case strRetour
    
    Case "PRINT"
        Call PrintLockEnCours
    Case "ZOOM"
        DisplayZoom aConnexion, Trim(Me.ListView1.SelectedItem.Text), Trim(Me.ListView1.SelectedItem.SubItems(1)), True, False, GetFnameDescriptif(aConnexion, aConnexion.Ident, Me.ListView1.SelectedItem.Text), Me, True
End Select

'-> remise a blanc  de la variable d'echange
strRetour = ""

End Sub

Private Sub PrintLockEnCours()

'---> On lance l'impression
Dim aEnreg As B3DEnreg
Dim aIt As ListItem
Dim ListeFields As String
Dim i As Integer
Dim hdlFileL As Integer
Dim LigneE As String
Dim NbLignes As Integer
Dim TempFileName As String
Dim TempFileName2 As String

On Error GoTo GestError

'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11

'-> On ouvre un nouveau fichier en ecriture
hdlFileL = FreeFile
TempFileName = GetTempFileNameVB("TRB")
Open TempFileName For Output As #hdlFileL


'-> On envoi l'entete du tableau
LigneE = "[ST-Section1(TXT-SECTION1)][\1]{ ^0001" & aConnexion.Ident & " }"
Print #hdlFileL, LigneE

'---> Parcours toutes les lignes de catalogue pour creer les instructions turbo a envoyer
For Each aIt In Me.ListView1.ListItems
      '-> Saut de pages
    If NbLignes = 0 Then
          '-> Re Entete
          LigneE = "[TB-NouveauTableau2(BLK-NouveauBlock1)][\1]{"
          Print #hdlFileL, LigneE
      End If
    LigneE = "[TB-NouveauTableau2(BLK-NouveauBlock2)][\1]{"
    
    '-> On parcours les champs a imprimer et on les envoi dans le fichier d'instructions turbo
    For i = 1 To Me.ListView1.ColumnHeaders.Count
        
        '-> On parcours tous les champs de l'enreg pour creer la ligne d'impression turbo
        If i = 1 Then
            LigneE = LigneE & "^" & Format(i, "0000") & aIt.Text
        Else
            LigneE = LigneE & "^" & Format(i, "0000") & aIt.SubItems(i - 1)
        End If
        
    Next '-> On passe au prochain champ a imprimer sur cette ligne

    LigneE = LigneE & " }"
    '-> envoi de la ligne
    Print #hdlFileL, LigneE
    
    NbLignes = NbLignes + 1
        
    '-> gestion saut de page + entete
    If NbLignes = 45 Then
        LigneE = "[PAGE]"
        Print #hdlFileL, LigneE
        NbLignes = 0
    End If
    
Next '-> On passe a la prochaine ligne de prime a imprimer

'-> fermeture du fichier
Close #hdlFileL

'-> Cr�er le fichier de destination
TempFileName2 = GetTempFileNameVB("TRB")

'-> On envoie la fusion entre la maquette et le fichier data puis l'impression TURBO
PrintDataInTurbo MaqPath & "B3D_VISU_LOCK_COURS.maqgui", TempFileName, TempFileName2, True

GestError:
    Me.Enabled = True
    Screen.MousePointer = 0

End Sub

Private Sub mnuZoom_Click()
strRetour = "ZOOM"
End Sub

Private Sub mnuprint_Click()
strRetour = "PRINT"
End Sub


