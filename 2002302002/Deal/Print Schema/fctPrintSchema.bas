Attribute VB_Name = "fctPrintschema"
Option Explicit

'-> Connexion en cours
Public aConnexion As B3DConnexion

'-> Chemin de turbo.exe
Public PrinterPath As String

'-> Chemin d'acc�s des maquettes
Public MaqPath As String

'-> API de temporisation
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
'-> API Pour gestion du temps
Public Declare Function GetTickCount& Lib "kernel32" ()

Sub Main()
'------------------------------------------------------------------
'---- Procedure de suppression des locks brules dans la base ------
'------------------------------------------------------------------
Dim ZonCharge As String
Dim ParamFile As String
Dim ListeField As String
Dim aIt As ListItem
Dim lpBuffer As String
Dim res As Long
Dim HdlFileI As Integer
Dim aBuffer As B3DBuffer
Dim i As Integer
Dim OrdreOk As Boolean
Dim Ligne As String
Dim aDescro As B3DFnameDescriptif


On Error GoTo GestError

'-> Tester la ligne de commande
If Trim(Command$) = "" Then GoTo GestError

'-> Pointeur de souris
Screen.MousePointer = 11

'-> R�cup�rer les param�tres
ParamFile = Entry(1, Command$, "�")
ZonCharge = Entry(2, Command$, "�")
            
'-> Initialiser la connexion
Set aConnexion = New B3DConnexion

'-> Charger son param�trage
If Not aConnexion.ReadParamFormFile(ParamFile) Then GoTo GestError

'-> Emplacement de turbo
PrinterPath = aConnexion.ApplicationDir & "Outils\Turbograph.exe"

'-> Emplacement des maquettes
MaqPath = aConnexion.ApplicationDir & "B3D\Maq\"

'-> Poser le nom du programme
InitialiseB3D aConnexion, "PRINT_DOG", False, False

'-> Initialisation pour demande
If Not aConnexion.InitDmd(True) Then
    '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
    MsgBox GetMesseprog("APPLIC", "51"), vbExclamation + vbOKOnly, "Fatal Error"
    GoTo GestError
End If

'-> On envoie une demande de recup des fichiers du catalogue
'-> On lance la fonction d'analyse des locks brul�es
OrdreOk = aConnexion.PutInstruction("B3D_SCHEMA_LIST~1", True)

'-> Verifie si on a bien envoy� l'instruction
If Not OrdreOk Then
    '-> Positionner le code Erreur
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    GoTo GestError
End If

'-> On envoie l'ordre de lecture des instructions
Set aBuffer = New B3DBuffer
If Not aConnexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    aConnexion.ErrorNumber = "100000"
    aConnexion.ErrorLibel = "Error sending exchange"
    aConnexion.ErrorParam = "LoadAgentDOG"
    GoTo GestError
End If

'-> Recup de la 1ere ligne
Ligne = aBuffer.Lignes(1)

If Entry(2, Entry(1, Ligne, "�"), "~") <> "OK" Then
    MsgBox GetMesseprog("APPLIC", "52"), vbExclamation + vbOKOnly, "Fatal Error"
    GoTo GestError
End If

'-> On cree les entetes de colonnes
frmPrintSchema.ListView1.ColumnHeaders.Add , , GetMesseprog("FRMSCHEMA", "2")
frmPrintSchema.ListView1.ColumnHeaders(1).Width = 1300
frmPrintSchema.ListView1.ColumnHeaders.Add , , GetMesseprog("FRMSCHEMA", "4")
frmPrintSchema.ListView1.ColumnHeaders(2).Width = 2500

'-> Analyse du retour des locks repertori�s
For i = 2 To aBuffer.Lignes.Count
    '-> Recup de la ligne
    Ligne = aBuffer.Lignes(i)
    '-> On cree le listview des tables
    Set aIt = frmPrintSchema.ListView1.ListItems.Add(, , Entry(1, Ligne, "�"))
    aIt.SubItems(1) = Entry(2, Ligne, "�")
Next

'-> R�cup�rer le catalogue
If Not GetIdentCatalogue(aConnexion, aConnexion.Ident, "") Then
    '-> Afficher un message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbCritical + vbOKOnly, "Error"
    '-> Fin du programme
    GoTo GestError
End If

'-> Formatter le listView
FormatListView frmPrintSchema.ListView1

'-> Affichage de la fenetre de choix d'impression
frmPrintSchema.Show

'-> Rendre la main � la CPU
DoEvents


'-> Pointeur de souris
Screen.MousePointer = 0

Exit Sub

GestError:
    MsgBox GetMesseprog("APPLIC", "53"), vbExclamation + vbOKOnly, "Fatal Error"
    Screen.MousePointer = 0
    End
        
End Sub


Public Sub PrintDataInTurbo(MaqName As String, DataFile As String, MergeFile As String, Delete As Boolean)
'---> Fonction qui edite des donn�es B3D en fonction d'une maquette turbo et
'---> d'un fichier de donn�es fusionn�s ensembles
'---> Propose aussi la suppression du spool et du fichier de fucion si demand� par programmeur
Dim HdlFile1 As Integer
Dim HdlFile2 As Integer
Dim HdlFile3 As Integer
Dim LigneI As String
Dim LigneE As String
Dim i As Integer


'-> Ouverture de la maquette
HdlFile1 = FreeFile
Open MaqName For Input As #HdlFile1
'-> Ouverture du fichier de donn�es
HdlFile2 = FreeFile
Open DataFile For Input As #HdlFile2
'-> Ouverture du fichier de fusion
HdlFile3 = FreeFile
Open MergeFile For Output As #HdlFile3

'-> Envoi du marqueur de debut de spool dans le fichier .TURBO
Print #HdlFile3, "[SPOOL]"
'-> Envoi du marqueur de debut de maquette dans le fichier .TURBO
Print #HdlFile3, "[MAQ]"

'-> On parcours la maquette et on la duplique dans notre fichier de fusion
Do While Not EOF(HdlFile1)
    '-> recup de la ligne de maquette
    Line Input #HdlFile1, LigneI
    '-> Ecriture de cette ligne dans le fichier de fusion
    Print #HdlFile3, LigneI
Loop
'-> Envoi du marqueur de fin de maquette dans le fichier .TURBO
Print #HdlFile3, "[/MAQ]"

'-> On commence a envoyer dans le fichier de fusion les donn�es du fichier de datas
Do While Not EOF(HdlFile2)
    '-> Recup d'une ligne de data
    Line Input #HdlFile2, LigneI
    '-> ecriture dans le fichier
    Print #HdlFile3, LigneI
Loop

'-> Envoi du marqueur de fin de spool dans le fichier .TURBO
Print #HdlFile3, "[/SPOOL]"

'-> On ferme les canaux
Close #HdlFile1
Close #HdlFile2
Close #HdlFile3

'->on lance l'edition par turbograph
Shell PrinterPath & " " & MergeFile, vbMaximizedFocus

End Sub

