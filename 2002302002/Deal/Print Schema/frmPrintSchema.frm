VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPrintSchema 
   ClientHeight    =   7920
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5625
   Icon            =   "frmPrintSchema.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   528
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   375
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView Listview1 
      Height          =   4095
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   7223
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Menu mnuPopUp 
      Caption         =   "PopUp"
      Visible         =   0   'False
      Begin VB.Menu mnuTous 
         Caption         =   "Tous Sch�ma"
      End
      Begin VB.Menu mnuAucun 
         Caption         =   "Aucun Sch�ma"
      End
      Begin VB.Menu mnusep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPrimtDOG 
         Caption         =   "Visualiser le Catalogue"
      End
      Begin VB.Menu mnuPrintSchema 
         Caption         =   "Visualiser le Sch�ma"
      End
   End
End
Attribute VB_Name = "frmPrintSchema"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next
GetClientRect Me.hwnd, aRect
Me.ListView1.Left = 0
Me.ListView1.Top = 0
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom

End Sub

Private Sub LoadCatalogueInListView(aBuffer As B3DBuffer)
'---> Charge le catalogue en visu dans un listview
Dim i As Integer
Dim Ligne As String
Dim aIt As ListItem

'-> On cree les entetes de colonnes
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("FRMSCHEMA", "2")
frmVisualisation.ListView1.ColumnHeaders(1).Width = 1000
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("FRMSCHEMA", "4")
frmVisualisation.ListView1.ColumnHeaders(2).Width = 3500
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("FRMSCHEMA", "20")
frmVisualisation.ListView1.ColumnHeaders(3).Width = 1500
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("FRMEMILIE", "4")
frmVisualisation.ListView1.ColumnHeaders(4).Width = 1000
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("FRMEMILIE", "8")
frmVisualisation.ListView1.ColumnHeaders(5).Width = 3500
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("DOG", "6")
frmVisualisation.ListView1.ColumnHeaders(6).Width = 500
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("FRMEMILIE", "9")
frmVisualisation.ListView1.ColumnHeaders(7).Width = 1000

'-> Parcours des lignes de retour pour affichage dans listview
For i = 2 To aBuffer.Lignes.Count
    '-> Recup de la ligne
    Ligne = aBuffer.Lignes(i)
    '-> On cree le listview des champs
    Set aIt = frmVisualisation.ListView1.ListItems.Add(, , Entry(2, Ligne, "�"))
    '-> D�signation
    aIt.SubItems(1) = Entry(3, Ligne, "�")
    '-> Groupe
    aIt.SubItems(2) = Entry(4, Ligne, "�")
    '-> Type Data
    aIt.SubItems(3) = Entry(5, Ligne, "�")
    '-> Table associee
    If Entry(1, Entry(9, Ligne, "�"), "�") <> "" Then
        aIt.SubItems(4) = Entry(1, Entry(9, Ligne, "�"), "�") & " - " & Entry(2, Entry(9, Ligne, "�"), "�")
    End If
    '-> Code Langue
    If Entry(10, Ligne, "�") <> "0" Then aIt.SubItems(5) = Entry(10, Ligne, "�")
    '-> Masterfield
    aIt.SubItems(6) = Entry(11, Ligne, "�")
Next


End Sub

Private Sub PrintCatalogue()
'---> On envoie une impression des data salaire employ�
'---> On genere un fichier de data pour lancer l'impression

On Error GoTo GestError

Dim aEnreg As B3DEnreg
Dim aIt As ListItem
Dim ListeFields As String
Dim i As Integer
Dim hdlFile As Integer
Dim LigneE As String
Dim TempFileName As String
Dim TempFileNameMerge As String

'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11
Me.ListView1.Enabled = False

'-> On ouvre un nouveau fichier en ecriture
TempFileName = GetTempFileNameVB("PRT")
hdlFile = FreeFile
Open TempFileName For Output As #hdlFile

'-> On envoi l'entete du tableau
LigneE = "[TB-NouveauTableau1(BLK-Block1)][\1]{"
Print #hdlFile, LigneE

'---> Parcours toutes les lignes de catalogue pour creer les instructions turbo a envoyer
For Each aIt In Me.ListView1.ListItems
    
    LigneE = "[TB-NouveauTableau1(BLK-Block2)][\1]{"
    
    '-> On parcours les champs a imprimer et on les envoi dans le fichier d'instructions turbo
    For i = 1 To Me.ListView1.ColumnHeaders.Count
        '-> On parcours tous les champs de l'enreg pour creer la ligne d'impression turbo
        If i = 1 Then
            LigneE = LigneE & "^" & Format(i, "0000") & aIt.Text
        Else
            LigneE = LigneE & "^" & Format(i, "0000") & aIt.SubItems(i)
        End If
    Next '-> On passe au prochain champ a imprimer sur cette ligne

    LigneE = LigneE & " }"
    '-> envoi de la ligne
    Print #hdlFile, LigneE

Next '-> On passe a la prochaine ligne de prime a imprimer

'-> Fermeture du fichier
Close #hdlFile

'-> R�cup�rattion d'un nouveau nom de fichier temporaire
TempFileNameMerge = GetTempFileNameVB("TRB")

'-> On envoie la fusion entre la maquette et le fichier data puis l'impression TURBO
PrintDataInTurbo MaqPath & "B3D_CATALOGUE.MAQGUI", TempFileName, TempFileNameMerge, True

GestError:
    Me.Enabled = True
    Screen.MousePointer = 0
    Me.ListView1.Enabled = True

End Sub


Private Sub Form_Load()

'-> titre de la feuille
Me.Caption = "  " & GetMesseprog("APPLIC", "54")

'-> Gestion des menus
Me.mnuAucun.Caption = GetMesseprog("APPLIC", "61")
Me.mnuTous.Caption = GetMesseprog("APPLIC", "60")
Me.mnuPrimtDOG.Caption = GetMesseprog("APPLIC", "55")
Me.mnuPrintSchema.Caption = GetMesseprog("APPLIC", "57")


End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'---> Gestion du menu PopUp sur les Schemas
Dim i As Integer

'-> Sort si bouton droit
If Button <> 2 Then Exit Sub

'-> Vider la variable d'echange
strRetour = ""

'-> Affichage du menu popup
Me.PopupMenu Me.mnuPopUp

Select Case strRetour

    Case "TOUS"
        For i = 1 To Me.ListView1.ListItems.Count
            Me.ListView1.ListItems(i).Checked = True
        Next
        
    Case "AUCUN"
        For i = 1 To Me.ListView1.ListItems.Count
            Me.ListView1.ListItems(i).Checked = False
        Next
    Case "PRINTDOG"
        '-> Afficher le catalogue � l'�cran
        Call PrintDOG
    Case "PRINTSCHEMA"
        '-> Afficher le schema
        Call PrintSchema
        
End Select

'-> Vider la variable d'echange
strRetour = ""

End Sub

Private Sub mnuAucun_Click()
strRetour = "AUCUN"
End Sub

Private Sub PrintDOG()
'---> Impression du catalogue
'---> On envoie l'edition du catalogue
Dim OrdreOk As Boolean
Dim aBuffer As B3DBuffer
Dim Ligne As String
Dim i As Integer

'-> Affichage de la tempo
frmWait.Label1 = GetMesseprog("APPLIC", "50")
frmWait.Show

frmVisualisation.Caption = GetMesseprog("APPLIC", "55")
frmVisualisation.TypeEdi = "CAT"

'-> Initialisation pour demande
If Not aConnexion.InitDmd(True) Then
    '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
    MsgBox GetMesseprog("APPLIC", "51"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    Exit Sub
End If

'-> On envoie une demande de recup des fichiers du catalogue
'-> On lance la fonction d'analyse des locks brul�es
OrdreOk = aConnexion.PutInstruction("B3D_CATALOGUE~1~", True)

'-> Verifie si on a bien envoy� l'instruction
If Not OrdreOk Then
    '-> Positionner le code Erreur
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    End
End If

'-> On envoie l'ordre de lecture des instructions
Set aBuffer = New B3DBuffer
If Not aConnexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    aConnexion.ErrorNumber = "100000"
    aConnexion.ErrorLibel = "Error sending exchange"
    aConnexion.ErrorParam = "LoadAgentDOG"
    End
End If

'-> Recup de la 1ere ligne
Ligne = aBuffer.Lignes(1)

If Entry(2, Ligne, "~") <> "OK" Then
    MsgBox GetMesseprog("APPLIC", "52"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    Exit Sub
End If

'-> Fin de la tempo
Unload frmWait

'-> On cree un listview pour lancer l'impression
LoadCatalogueInListView aBuffer

'-> Affichage du catalogue
frmVisualisation.Show vbModal
End Sub

Private Sub mnuPrimtDOG_Click()
strRetour = "PRINTDOG"
End Sub

Private Sub PrintSchema()
'---> Impression du schema
'---> Envoi de l'impression du ou des schemas selectionn�es
Dim Ligne As String
Dim i As Integer
Dim aBuffer As B3DBuffer
Dim OrdreOk As Boolean

'-> On parcours chaque fichier s�lectionn�
For i = 1 To Me.ListView1.ListItems.Count
    If Me.ListView1.ListItems(i).Checked Then
        If Ligne = "" Then
            Ligne = Trim(Me.ListView1.ListItems(i).Text)
        Else
            Ligne = Ligne & "�" & Trim(Me.ListView1.ListItems(i).Text)
        End If
    End If
Next

If Ligne = "" Then
    MsgBox "Veuillez s�lectionner au moins un fichier...", vbExclamation
    Exit Sub
End If

'-> Affichage de la tempo
frmWait.Label1 = GetMesseprog("APPLIC", "56")
frmWait.Show

frmVisualisation.Caption = GetMesseprog("APPLIC", "57")
frmVisualisation.TypeEdi = "SCH"

'-> Ligne de cde B3D
Ligne = "B3D_SCHEMA_DES~3~0~" & Ligne

'-> Initialisation pour demande
If Not aConnexion.InitDmd(True) Then
    '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
    MsgBox GetMesseprog("APPLIC", "51"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    Exit Sub
End If

'-> On envoie une demande de recup des fichiers du catalogue
'-> On lance la fonction d'analyse des locks brul�es
OrdreOk = aConnexion.PutInstruction(Ligne, True)

'-> Verifie si on a bien envoy� l'instruction
If Not OrdreOk Then
    '-> Positionner le code Erreur
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    End
End If

'-> On envoie l'ordre de lecture des instructions
Set aBuffer = New B3DBuffer
If Not aConnexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    aConnexion.ErrorNumber = "100000"
    aConnexion.ErrorLibel = "Error sending exchange"
    aConnexion.ErrorParam = "LoadAgentDOG"
    End
End If

'-> Recup de la 1ere ligne
Ligne = aBuffer.Lignes(1)

If Entry(2, Ligne, "~") <> "OK" Then
    MsgBox GetMesseprog("APPLIC", "53"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    Exit Sub
End If

'-> affiche le schema des fichiers selectionnes dans le listview de visualisation
LoadSchemaInListView aBuffer

'-> Fin de la tempo
Unload frmWait

'-> Affichage de la visualisation
frmVisualisation.Show vbModal
End Sub

Private Sub mnuPrintSchema_Click()
strRetour = "PRINTSCHEMA"
End Sub

Private Sub mnuTous_Click()
strRetour = "TOUS"
End Sub

Private Sub LoadSchemaInListView(aBuffer As B3DBuffer)
'---> Permet de charger un listview avec le schema de fichiers selectionn�s
Dim i As Integer
Dim j As Integer
Dim Fonction As String
Dim Ligne As String
Dim aIt As ListItem

'-> On nettoie le listview
frmVisualisation.ListView1.ColumnHeaders.Clear

'-> On cree les entete de colonnes du listview
'-> On cree les entetes de colonnes
frmVisualisation.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "58")
frmVisualisation.ListView1.ColumnHeaders.Add , , ""
frmVisualisation.ListView1.ColumnHeaders.Add , , ""
frmVisualisation.ListView1.ColumnHeaders.Add , , ""
frmVisualisation.ListView1.ColumnHeaders.Add , , ""
frmVisualisation.ListView1.ColumnHeaders.Add , , ""
frmVisualisation.ListView1.ColumnHeaders.Add , , ""
frmVisualisation.ListView1.ColumnHeaders.Add , , ""
frmVisualisation.ListView1.ColumnHeaders.Add , , ""
frmVisualisation.ListView1.ColumnHeaders.Add , , ""
frmVisualisation.ListView1.ColumnHeaders.Add , , ""

'-> On parcours les lignes retourn�es
For i = 2 To aBuffer.Lignes.Count
    
    '-> recup de la ligne
    Fonction = Entry(1, aBuffer.Lignes(i), "~")
    Ligne = Entry(2, aBuffer.Lignes(i), "~")
    
    Fonction = Entry(3, Fonction, "_")
    
    Select Case Fonction
        
        Case "CATALOGUE"
            GoTo NextLigne
            
        Case "FNAME"
            '-> On cree une ligne pour la data en cours
            Set aIt = frmVisualisation.ListView1.ListItems.Add(, , Fonction)
            For j = 1 To 8
                aIt.SubItems(j) = Entry(j, Ligne, "�")
                aIt.Bold = True
                '-> teste si valeur boole�ne
                If j >= 4 And j <= 5 Or j = 7 Then
                    If aIt.SubItems(j) = "0" Then
                        aIt.SubItems(j) = "NON"
                    Else
                        aIt.SubItems(j) = "OUI"
                    End If
                End If
            Next
            '-> Niveau de r�plication
            aIt.SubItems(9) = Entry(9, Ligne, "�")
            '-> On charge le libelle
            If aIt.SubItems(6) <> "" Then
                aIt.SubItems(10) = Catalogues(aIt.SubItems(6)).Fill_Label
            End If
            
        Case "FIELD"
            '-> On cree une ligne pour la data en cours
            Set aIt = frmVisualisation.ListView1.ListItems.Add(, , Fonction)
            For j = 1 To 9
                aIt.SubItems(j) = Entry(j, Ligne, "�")
                '-> teste si valeur boole�ne
                If j >= 4 And j <= 9 Then
                    If aIt.SubItems(j) = "0" Then
                        aIt.SubItems(j) = "NON"
                    Else
                        aIt.SubItems(j) = "OUI"
                    End If
                End If
            Next
            '-> On charge le libelle
            If aIt.SubItems(3) <> "" Then
                aIt.SubItems(10) = Catalogues(aIt.SubItems(3)).Fill_Label
            End If
            
        Case "INAME"
            '-> On cree une ligne pour la data en cours
            Set aIt = frmVisualisation.ListView1.ListItems.Add(, , Fonction)
            aIt.Bold = True
            For j = 1 To 5
                aIt.SubItems(j) = Entry(j, Ligne, "�")
                '-> teste si valeur boole�ne
                If j = 5 Then
                    If aIt.SubItems(j) = "0" Then
                        aIt.SubItems(j) = "NON"
                    Else
                        aIt.SubItems(j) = "OUI"
                    End If
                End If
            Next
            
        Case "IFIELD"
            '-> On cree une ligne pour la data en cours
            Set aIt = frmVisualisation.ListView1.ListItems.Add(, , Fonction)
            For j = 1 To 8
                aIt.SubItems(j) = Entry(j, Ligne, "�")
                '-> teste si valeur boole�ne
                If j = 5 Then
                    If aIt.SubItems(j) = "0" Then
                        aIt.SubItems(j) = "NON"
                    Else
                        aIt.SubItems(j) = "OUI"
                    End If
                End If
                If j = 6 Then
                    '-> Retourne le libelle d'un code condition
                    aIt.SubItems(j) = SelectCdt(CInt(aIt.SubItems(j)))
                End If
            Next
            '-> On charge le libelle
            If aIt.SubItems(4) <> "" Then
                aIt.SubItems(10) = Catalogues(aIt.SubItems(4)).Fill_Label
            End If
        
    End Select
    

NextLigne:

Next

'-> Formattage du listview
FormatListView frmVisualisation.ListView1

'-> Refresh du listview
frmVisualisation.ListView1.Refresh

End Sub

Private Function SelectCdt(CodeCdt As Integer) As String
'---> Permet d'etablir une correspondance entre une code condition et un libelle _
    exple : 1 = 'EGAL'
    
Select Case CodeCdt
        Case 0
            SelectCdt = ""
        Case 1
            SelectCdt = "EQ"
        Case 2
            SelectCdt = "INC"
        Case 3
            SelectCdt = "NIN"
        Case 4
            SelectCdt = "DIF"
        Case 5
            SelectCdt = "LE"
        Case 6
            SelectCdt = "GE"
        Case 7
            SelectCdt = "LT"
        Case 8
            SelectCdt = "GT"
        Case 9
            SelectCdt = "BEG"
        Case 10
            SelectCdt = "MAT"
    End Select
End Function
