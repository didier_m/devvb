VERSION 5.00
Begin VB.Form frmWait 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Deal Informatique"
   ClientHeight    =   1545
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   6210
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1545
   ScaleWidth      =   6210
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   1200
      Top             =   720
   End
   Begin VB.PictureBox picOk 
      Height          =   375
      Left            =   0
      ScaleHeight     =   315
      ScaleWidth      =   4395
      TabIndex        =   1
      Top             =   1080
      Width           =   4455
   End
   Begin VB.Image Image1 
      Height          =   1500
      Left            =   4680
      Picture         =   "frmWait.frx":0000
      Top             =   0
      Width           =   1500
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3735
   End
End
Attribute VB_Name = "frmWait"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim TailleLue As Long
Dim TailleTotale As Long

Private Sub Form_Load()

'-> Init de la temporisation
TailleTotale = 100

'-> Lancer le tempo
Me.Timer1.Enabled = True

End Sub

Private Sub Timer1_Timer()

'-> Incr�menter le conteur
TailleLue = TailleLue + 1

'-> Dessin de la temporisation
DrawLoading
DoEvents

'-> Mise � jour
If TailleLue = TailleTotale Then
    picOk.Cls
    TailleLue = 1
End If

End Sub

Private Sub DrawLoading()

'---> Cette proc�dure  dessine une barre de temporisation

DoEvents
picOk.Line (0, 0)-((TailleLue / TailleTotale) * picOk.ScaleWidth, picOk.ScaleHeight), &H800000, BF


End Sub
