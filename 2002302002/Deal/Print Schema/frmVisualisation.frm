VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmVisualisation 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9375
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   13095
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9375
   ScaleWidth      =   13095
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView ListView1 
      Height          =   9375
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   13095
      _ExtentX        =   23098
      _ExtentY        =   16536
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Menu mnuPopUp 
      Caption         =   "PopUp"
      Visible         =   0   'False
      Begin VB.Menu mnuPrint 
         Caption         =   "Imprimer"
      End
   End
End
Attribute VB_Name = "frmVisualisation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public TypeEdi As String



Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("APPLIC", "59")

End Sub

Private Sub listview1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    '-> Trier sur les entetes de colonne
    Me.ListView1.SortKey = ColumnHeader.Index - 1
    If Me.ListView1.SortOrder = lvwAscending Then
        Me.ListView1.SortOrder = lvwDescending
    Else
        Me.ListView1.SortOrder = lvwAscending
    End If
    Me.ListView1.Sorted = True
End Sub

Private Sub PrintCatalogueInTurbo()
'---> On envoie une impression des data salaire employ�
'---> On genere un fichier de data pour lancer l'impression

On Error GoTo GestError

Dim aEnreg As B3DEnreg
Dim aIt As ListItem
Dim ListeFields As String
Dim i As Integer
Dim hdlFile As Integer
Dim LigneE As String
Dim NbLignes As Integer
Dim TempFileName As String
Dim TempFileName2 As String

'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11
Me.ListView1.Enabled = False

'-> On ouvre un nouveau fichier en ecriture
hdlFile = FreeFile
TempFileName = GetTempFileNameVB("TRB")
Open TempFileName For Output As #hdlFile

'---> Parcours toutes les lignes de prime pour creer les instruction turbo a envoyer
For Each aIt In Me.ListView1.ListItems

    '-> On parcours les champs a imprimer et on les envoi dans le fichier d'instructions turbo
    For i = 1 To Me.ListView1.ColumnHeaders.Count
        
        If i = 1 Then
     
            If NbLignes = 0 Then
                '-> Envoi de l'entete
                '-> On cree une ligne qui cree l'ordre d'impression de la section + dieses
                LigneE = "[ST-SECTION2(TXT-Section)][]{^0001" & aConnexion.Ident & " }"
                '-> envoi de la ligne
                Print #hdlFile, LigneE
                LigneE = "[TB-NouveauTableau1(BLK-Block1)][\1]{}"
                Print #hdlFile, LigneE
            End If
            LigneE = "[TB-NouveauTableau1(BLK-Block2)][\1]{"
            LigneE = LigneE & "^" & Format(i, "0000") & aIt.Text
           
        Else
            LigneE = LigneE & "^" & Format(i, "0000") & aIt.SubItems(i - 1)
        End If
    Next '-> On passe au prochain champ a imprimer sur cette ligne

    LigneE = LigneE & " }"
    '-> envoi de la ligne
    Print #hdlFile, LigneE
    NbLignes = NbLignes + 1
    
    If NbLignes = 38 Then
        Print #hdlFile, "[PAGE]"
        NbLignes = 0
    End If
   
Next '-> On passe a la prochaine ligne de prime a imprimer

'-> Fermeture du fichier
Close #hdlFile

'-> Cr�er le fichier de destination
TempFileName2 = GetTempFileNameVB("TRB")

'-> On envoie la fusion entre la maquette et le fichier data puis l'impression TURBO
PrintDataInTurbo MaqPath & "B3D_CATALOGUE.MAQGUI", TempFileName, TempFileName2, True

GestError:
    
    Me.Enabled = True
    Screen.MousePointer = 0
    Me.ListView1.Enabled = True
    Unload Me

End Sub


Private Sub PrintSchemaInTurbo()
'---> On envoie une impression des data salaire employ�
'---> On genere un fichier de data pour lancer l'impression

On Error GoTo GestError

Dim aEnreg As B3DEnreg
Dim aIt As ListItem
Dim ListeFields As String
Dim i As Integer
Dim j As Integer
Dim hdlFile As Integer
Dim LigneE As String
Dim NbLignes As Integer
Dim FirstFname As Boolean
Dim TempFileName As String
Dim TempFileName2 As String

'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11
Me.ListView1.Enabled = False

'-> On ouvre un nouveau fichier en ecriture
hdlFile = FreeFile
TempFileName = GetTempFileNameVB("TRB")
Open TempFileName For Output As #hdlFile

'---> Parcours toutes les lignes de prime pour creer les instruction turbo a envoyer
For Each aIt In Me.ListView1.ListItems

     '-> Selon le type de ligne
     Select Case aIt.Text
     
         Case "FNAME"
             '-> On pose un test pour editer un Fname par page
             If FirstFname = True Then
                 Print #hdlFile, "[PAGE]"
             Else
                 FirstFname = True
             End If
             '-> On envoi les donn�es du fname dans la section de txt associee
             NbLignes = 0
             '-> Logo B3d
             LigneE = "[ST-IMG1(TXT-Section)][]{}"
             Print #hdlFile, LigneE
             
             LigneE = "[TB-FNAME(BLK-Block1)][\1]{"
             LigneE = LigneE & "^0012" & aConnexion.Ident
             LigneE = LigneE & "^0001" & aIt.SubItems(1)
             LigneE = LigneE & "^0003" & aIt.SubItems(3)
             LigneE = LigneE & "^0011" & Now
             LigneE = LigneE & " }"
             Print #hdlFile, LigneE
             
             LigneE = "[TB-FNAME(BLK-Block1)][\2]{"
             LigneE = LigneE & "^0004" & aIt.SubItems(4)
             LigneE = LigneE & "^0005" & aIt.SubItems(5)
             LigneE = LigneE & "^0007" & aIt.SubItems(7)
             LigneE = LigneE & " }"
             Print #hdlFile, LigneE
             
             LigneE = "[TB-FNAME(BLK-Block1)][\3]{"
             LigneE = LigneE & "^0009" & aIt.SubItems(9)
             LigneE = LigneE & "^0006" & aIt.SubItems(6)
             LigneE = LigneE & "^0010" & aIt.SubItems(10)
             LigneE = LigneE & "^0008" & aIt.SubItems(8)
             LigneE = LigneE & " }"
             Print #hdlFile, LigneE
             
             
         Case "FIELD"
             '-> On teste si on edite une entete de ligne de champ
             If NbLignes = 0 Then
                 LigneE = "[TB-FIELDS(BLK-Block1)][\1]{}"
                 Print #hdlFile, LigneE
                 LigneE = "[TB-FIELDS(BLK-Block1)][\2]{}"
                 Print #hdlFile, LigneE
             End If
             '-> On edite les proprietes du champ en cours
             LigneE = "[TB-FIELDS(BLK-Block2)][\1]{"
             For j = 1 To 10
                  LigneE = LigneE & "^" & Format(j, "0000") & aIt.SubItems(j)
             Next
             LigneE = LigneE & " }"
             Print #hdlFile, LigneE
             NbLignes = NbLignes + 1
             '-> On genere un saut de page si plus de 35 lignes
             If NbLignes = 36 Then
                 Print #hdlFile, "[PAGE]"
                 NbLignes = 0
             End If
             
         
         Case "INAME"
             '-> On edite une entete d'index
             LigneE = "[ST-INAME(TXT-Section)][]{"
             '-> On edite les proprietes de l'index
             LigneE = LigneE & "^0002" & aIt.SubItems(2)
             LigneE = LigneE & "^0004" & aIt.SubItems(4)
             LigneE = LigneE & "^0005" & aIt.SubItems(5) & " }"
             Print #hdlFile, LigneE
             '-> une entete d'index = 2 lignes
             NbLignes = NbLignes + 4
             LigneE = "[TB-IFIELDS(BLK-Block1)][\1]{}"
             Print #hdlFile, LigneE
             '-> On genere un saut de page si plus de 35 lignes
             If NbLignes = 36 Then
                 Print #hdlFile, "[PAGE]"
                 NbLignes = 0
             End If
             
         Case "IFIELD"
             '-> On cree les champs de l'index
             LigneE = "[TB-IFIELDS(BLK-Block2)][\1]{"
             For j = 1 To 10
                  LigneE = LigneE & "^" & Format(j, "0000") & aIt.SubItems(j)
             Next
             LigneE = LigneE & " }"
             Print #hdlFile, LigneE
             NbLignes = NbLignes + 1
             '-> On genere un saut de page si plus de 35 lignes
             If NbLignes = 36 Then
                 Print #hdlFile, "[PAGE]"
                 NbLignes = 0
                 LigneE = "[TB-IFIELDS(BLK-Block1)][\1]{}"
                 Print #hdlFile, LigneE
             End If
             '-> On genere un saut de page si plus de 35 lignes
             If NbLignes = 36 Then
                 Print #hdlFile, "[PAGE]"
                 NbLignes = 0
             End If
            
     End Select

Next '-> On passe a la prochaine ligne de prime a imprimer

'-> Fermeture du fichier
Close #hdlFile

'-> Cr�er le fichier de destination
TempFileName2 = GetTempFileNameVB("TRB")

'-> On envoie la fusion entre la maquette et le fichier data puis l'impression TURBO
PrintDataInTurbo MaqPath & "B3D_SCHEMA.MAQGUI", TempFileName, TempFileName2, True

GestError:
    
    Me.Enabled = True
    Screen.MousePointer = 0
    Me.ListView1.Enabled = True
    
    Unload Me

End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'---> Gestion du menu PopUp sur les Schemas
Dim i As Integer

'-> Sort si bouton droit
If Button <> 2 Then Exit Sub

strRetour = ""

'-> Affichage du menu popup
Me.PopupMenu Me.mnuPopUp

If strRetour = "PRINT" Then
    '---> Envoi de l'impression
    If Me.TypeEdi = "CAT" Then
        PrintCatalogueInTurbo
    Else
        PrintSchemaInTurbo
    End If
End If

strRetour = ""

End Sub

Private Sub mnuPrint_Click()
strRetour = "PRINT"
End Sub
