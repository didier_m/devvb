VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmStatEnreg 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7185
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4875
   Icon            =   "frmStatEnreg.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7185
   ScaleWidth      =   4875
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   7
      Top             =   6810
      Width           =   4875
      _ExtentX        =   8599
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8819
            MinWidth        =   8819
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   6015
      Left            =   0
      TabIndex        =   4
      Top             =   720
      Width           =   4875
      _ExtentX        =   8599
      _ExtentY        =   10610
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.TextBox txtDate 
      Enabled         =   0   'False
      Height          =   285
      Index           =   1
      Left            =   3000
      MaxLength       =   10
      TabIndex        =   3
      Tag             =   "FIELD~0001~EMPLOYE~0014"
      Top             =   360
      Width           =   1455
   End
   Begin VB.TextBox txtDate 
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   0
      MaxLength       =   10
      TabIndex        =   2
      Tag             =   "FIELD~0001~EMPLOYE~0016"
      Top             =   360
      Width           =   1455
   End
   Begin VB.PictureBox pctCalendar 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   0
      Left            =   1560
      Picture         =   "frmStatEnreg.frx":0CCA
      ScaleHeight     =   255
      ScaleWidth      =   255
      TabIndex        =   1
      ToolTipText     =   "Choix d'une date"
      Top             =   360
      Width           =   255
   End
   Begin VB.PictureBox pctCalendar 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   1
      Left            =   4560
      Picture         =   "frmStatEnreg.frx":0E14
      ScaleHeight     =   255
      ScaleWidth      =   255
      TabIndex        =   0
      ToolTipText     =   "Choix d'une date"
      Top             =   360
      Width           =   255
   End
   Begin VB.Label Label2 
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3000
      TabIndex        =   6
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label Label1 
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   0
      TabIndex        =   5
      Top             =   120
      Width           =   1335
   End
   Begin VB.Menu MnuPopUp 
      Caption         =   "PopUp"
      Visible         =   0   'False
      Begin VB.Menu mnuRequete 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuImprimer 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmStatEnreg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("APPLIC", "85")

'-> Messprog de la feuille
Me.Label1.Caption = GetMesseprog("APPLIC", "86")
Me.Label2.Caption = GetMesseprog("APPLIC", "87")

'-> Messprog des menus
Me.mnuImprimer.Caption = GetMesseprog("MENU", "7")
Me.mnuRequete.Caption = GetMesseprog("APPLIC", "88")

'-> Positionner les dates d'analyse
Me.txtDate(0).Text = Entry(1, Now, " ")
Me.txtDate(1).Text = Entry(1, Now, " ")

End Sub

Private Sub listview1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    '-> Trier sur les entetes de colonne
    Me.ListView1.SortKey = ColumnHeader.Index - 1
    If Me.ListView1.SortOrder = lvwAscending Then
        Me.ListView1.SortOrder = lvwDescending
    Else
        Me.ListView1.SortOrder = lvwAscending
    End If
    Me.ListView1.Sorted = True
End Sub

Private Sub mnuImprimer_Click()
strRetour = "PRINT"
End Sub

Private Sub mnuRequete_Click()
strRetour = "REQUETE"
End Sub

Private Sub pctCalendar_Click(Index As Integer)
'---> Saisie dans le calendrier

'-> Charger le calendrier
Load frmCalendrier

'-> Init de sa date
If Me.txtDate(Index).Text <> "" Then
    frmCalendrier.Calendar1.Value = Me.txtDate(Index).Text
    frmCalendrier.Label2.Caption = frmCalendrier.Calendar1.Value
End If

'-> Vider la variable de retour
strRetour = ""

'-> Afficher la feuille
frmCalendrier.Show vbModal

'-> Tester le retour
If strRetour = "" Then Exit Sub

'-> Mise � blanc de la date
If strRetour = "BLANC" Then Me.txtDate(Index).Text = ""

'-> Mise � jour de la date
If Entry(1, strRetour, Chr(0)) = "OK" Then Me.txtDate(Index).Text = Entry(2, strRetour, Chr(0))

'-> Vider la variable de retour
strRetour = ""


End Sub

Private Sub PrintStatistiques()
'---> On envoie une impression des data salaire employ�
'---> On genere un fichier de data pour lancer l'impression

Dim aEnreg As B3DEnreg
Dim aIt As ListItem
Dim ListeFields As String
Dim i As Integer
Dim hdlFile As Integer
Dim LigneE As String
Dim NbLignes As Integer
Dim TempFileName As String
Dim TempFileNameMerge As String

On Error GoTo GestError

'-> Bloquer les enreg
Me.Enabled = False
Screen.MousePointer = 11
Me.ListView1.Enabled = False

Me.StatusBar1.Panels(1).Text = GetMesseprog("APPLIC", "91")
DoEvents

'-> On ouvre un nouveau fichier en ecriture
TempFileName = GetTempFileNameVB("PRT")
hdlFile = FreeFile
Open TempFileName For Output As #hdlFile

'-> On envoi l'entete du tableau
LigneE = "[ST-Section1(TXT-SECTION1)][\1]{ ^0001" & aConnexion.Ident & "^0002" & Me.txtDate(0) & "^0003" & Me.txtDate(1) & " }"
Print #hdlFile, LigneE

'---> Parcours toutes les lignes de catalogue pour creer les instructions turbo a envoyer
For Each aIt In Me.ListView1.ListItems
      '-> Saut de pages
    If NbLignes = 0 Then
          '-> Re Entete
          LigneE = "[TB-NouveauTableau2(BLK-NouveauBlock1)][\1]{"
          Print #hdlFile, LigneE
      End If
    LigneE = "[TB-NouveauTableau2(BLK-NouveauBlock2)][\1]{"
    
    '-> On parcours les champs a imprimer et on les envoi dans le fichier d'instructions turbo
    For i = 1 To Me.ListView1.ColumnHeaders.Count
        '-> On parcours tous les champs de l'enreg pour creer la ligne d'impression turbo
        If i = 1 Then
            LigneE = LigneE & "^" & Format(i, "0000") & aIt.Text
        Else
            LigneE = LigneE & "^" & Format(i, "0000") & aIt.SubItems(i - 1)
        End If
    Next '-> On passe au prochain champ a imprimer sur cette ligne

    LigneE = LigneE & " }"
    '-> envoi de la ligne
    Print #hdlFile, LigneE
    
    NbLignes = NbLignes + 1
        
    '-> gestion saut de page + entete
    If NbLignes = 45 Then
        LigneE = "[PAGE]"
        Print #hdlFile, LigneE
        NbLignes = 0
    End If
    
Next '-> On passe a la prochaine ligne de prime a imprimer

'-> Fermeture du fichier
Close #hdlFile

'-> R�cup�rattion d'un nouveau nom de fichier temporaire
TempFileNameMerge = GetTempFileNameVB("TRB")

'-> On envoie la fusion entre la maquette et le fichier data puis l'impression TURBO
PrintDataInTurbo MaqPath & "B3D_STAT_ENREG.maqgui", TempFileName, TempFileNameMerge, True

GestError:
    Reset
    Me.Enabled = True
    Screen.MousePointer = 0
    Me.ListView1.Enabled = True
    Me.StatusBar1.Panels(1).Text = ""
    DoEvents
    


End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'---> gestion des popup

Dim i As Integer

'-> Sort si bouton droit
If Button <> 2 Then Exit Sub

'-> Tester qu'il y ait des stats
If Me.ListView1.ListItems.Count = 0 Then
    Me.mnuImprimer.Enabled = False
Else
    Me.mnuImprimer.Enabled = True
End If

'-> remise a blanc  de la variable d'echange
strRetour = ""

'-> Affichage du menu popup
Me.PopupMenu Me.MnuPopUp

Select Case strRetour
    
    Case "PRINT"
        '-> lance l'impression
        Call PrintStatistiques
        
    Case "REQUETE"
        Call LoadRequete
        
End Select

'-> remise a blanc  de la variable d'echange
strRetour = ""

End Sub


Private Sub LoadRequete()
'---> on recupere les stats
Dim OrdreOk As Boolean
Dim aBuffer As B3DBuffer
Dim Ligne  As String
Dim i As Integer
Dim aIt As ListItem

On Error GoTo GestError

If Me.txtDate(0) = "" Or Me.txtDate(1) = "" Then
    MsgBox GetMesseprog("APPLIC", "89"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    Exit Sub
End If

'-> Bloquer
Me.Enabled = False
Screen.MousePointer = 11

Me.StatusBar1.Panels(1).Text = GetMesseprog("APPLIC", "90")
DoEvents

'-> Ligne de commande pour get des statistiques
Ligne = "B3D_GET_ENREG~" & aConnexion.Ident & "�" & Format(Me.txtDate(0).Text, "YYYYMMDD") & "�" & Format(Me.txtDate(1).Text, "YYYYMMDD")

'-> On envoie une demande de recup des fichiers du catalogue
'-> On lance la fonction d'analyse des locks brul�es
OrdreOk = aConnexion.PutInstruction(Ligne, True)

'-> Verifie si on a bien envoy� l'instruction
If Not OrdreOk Then
    '-> Positionner le code Erreur
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    GoTo GestError
End If

'-> On envoie l'ordre de lecture des instructions
Set aBuffer = New B3DBuffer
If Not aConnexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    aConnexion.ErrorNumber = "100000"
    aConnexion.ErrorLibel = "Error sending exchange"
    aConnexion.ErrorParam = "LoadAgentDOG"
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    GoTo GestError
End If

'-> Recup de la 1ere ligne
Ligne = aBuffer.Lignes(1)

If Entry(2, Entry(1, Ligne, "�"), "~") <> "OK" Then
    MsgBox GetMesseprog("APPLIC", "52"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

'-> On cree les entetes de colonnes
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "64")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("CONSOLE", "38")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("CONSOLE", "74")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("CONSOLE", "75")
Me.ListView1.ColumnHeaders.Add , , GetMesseprog("CONSOLE", "76")

'-> Analyse du retour des locks repertori�s
For i = 2 To aBuffer.Lignes.Count
    '-> Recup de la ligne
    Ligne = aBuffer.Lignes(i)
    '-> On cree le listview des tables
    Set aIt = Me.ListView1.ListItems.Add(, , Entry(2, Ligne, "�"))
    aIt.SubItems(1) = Entry(3, Ligne, "�")
    aIt.SubItems(2) = Entry(4, Ligne, "�")
    aIt.SubItems(3) = Entry(5, Ligne, "�")
    aIt.SubItems(4) = Entry(6, Ligne, "�")
Next

'-> Formattage du listview
FormatListView Me.ListView1


GestError:
    
    '-> Bloquer
    Me.Enabled = True
    Screen.MousePointer = 0
    Me.StatusBar1.Panels(1).Text = ""
    DoEvents

End Sub
