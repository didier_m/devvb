Attribute VB_Name = "fctStatEnreg"
Option Explicit

'-> Connexion en cours
Public aConnexion As B3DConnexion

'-> Chemin de turbo.exe
Public PrinterPath As String

'-> Chemin d'acc�s des maquettes
Public MaqPath As String

'-> API de temporisation
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
'-> API Pour gestion du temps
Public Declare Function GetTickCount& Lib "kernel32" ()

Sub Main()
'------------------------------------------------------------------
'---- Procedure de suppression des locks brules dans la base ------
'------------------------------------------------------------------
Dim ZonCharge As String
Dim ParamFile As String
Dim ListeField As String
Dim aIt As ListItem
Dim HdlFileI As Integer
Dim aBuffer As B3DBuffer
Dim i As Integer
Dim OrdreOk As Boolean
Dim Ligne As String
Dim aDescro As B3DFnameDescriptif


On Error GoTo GestError

'-> Tester la ligne de commande
If Trim(Command$) = "" Then End

'-> R�cup�rer les param�tres
ParamFile = Entry(1, Command$, "�")
ZonCharge = Entry(2, Command$, "�")
        
'-> Initialiser la connexion
Set aConnexion = New B3DConnexion
'-> Charger son param�trage
If Not aConnexion.ReadParamFormFile(ParamFile) Then GoTo GestError

'-> Emplacement de turbo
PrinterPath = aConnexion.ApplicationDir & "Outils\Turbograph.exe"

'-> Emplacement des maquettes
MaqPath = aConnexion.ApplicationDir & "B3D\Maq\"

'-> Bloquer l'�cran
Screen.MousePointer = 11
frmStatEnreg.Show
frmStatEnreg.Enabled = False

'-> Affichage de la tempo
frmStatEnreg.StatusBar1.Panels(1).Text = GetMesseprog("APPLIC", "84")

'-> Poser le nom du programme
InitialiseB3D aConnexion, "STAT_ENREG", False, False

'-> Initialisation pour demande
If Not aConnexion.InitDmd(True) Then
    '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
    MsgBox GetMesseprog("APPLIC", "51"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

'-> D�bloquer l'�cran
frmStatEnreg.Enabled = True
Screen.MousePointer = 0
frmStatEnreg.StatusBar1.Panels(1).Text = ""

Exit Sub

GestError:
    
    Screen.MousePointer = 0
    End
        
End Sub


Public Sub PrintDataInTurbo(MaqName As String, DataFile As String, MergeFile As String, Delete As Boolean)
'---> Fonction qui edite des donn�es B3D en fonction d'une maquette turbo et
'---> d'un fichier de donn�es fusionn�s ensembles
'---> Propose aussi la suppression du spool et du fichier de fucion si demand� par programmeur
Dim HdlFile1 As Integer
Dim HdlFile2 As Integer
Dim HdlFile3 As Integer
Dim LigneI As String
Dim LigneE As String
Dim i As Integer


'-> Ouverture de la maquette
HdlFile1 = FreeFile
Open MaqName For Input As #HdlFile1
'-> Ouverture du fichier de donn�es
HdlFile2 = FreeFile
Open DataFile For Input As #HdlFile2
'-> Ouverture du fichier de fusion
HdlFile3 = FreeFile
Open MergeFile For Output As #HdlFile3

'-> Envoi du marqueur de debut de spool dans le fichier .TURBO
Print #HdlFile3, "[SPOOL]"
'-> Envoi du marqueur de debut de maquette dans le fichier .TURBO
Print #HdlFile3, "[MAQ]"

'-> On parcours la maquette et on la duplique dans notre fichier de fusion
Do While Not EOF(HdlFile1)
    '-> recup de la ligne de maquette
    Line Input #HdlFile1, LigneI
    '-> Ecriture de cette ligne dans le fichier de fusion
    Print #HdlFile3, LigneI
Loop
'-> Envoi du marqueur de fin de maquette dans le fichier .TURBO
Print #HdlFile3, "[/MAQ]"

'-> On commence a envoyer dans le fichier de fusion les donn�es du fichier de datas
Do While Not EOF(HdlFile2)
    '-> Recup d'une ligne de data
    Line Input #HdlFile2, LigneI
    '-> ecriture dans le fichier
    Print #HdlFile3, LigneI
Loop

'-> Envoi du marqueur de fin de spool dans le fichier .TURBO
Print #HdlFile3, "[/SPOOL]"

'-> On ferme les canaux
Close #HdlFile1
Close #HdlFile2
Close #HdlFile3

'->on lance l'edition par turbograph
Shell PrinterPath & " " & MergeFile, vbMaximizedFocus

End Sub


