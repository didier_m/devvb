VERSION 5.00
Object = "{8E27C92E-1264-101C-8A2F-040224009C02}#7.0#0"; "MSCAL.OCX"
Begin VB.Form frmCalendrier 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Calendrier"
   ClientHeight    =   6090
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6150
   Icon            =   "frmCalendrier.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6090
   ScaleWidth      =   6150
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   6015
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6135
      Begin VB.CommandButton cmdSuppdate 
         Height          =   735
         Left            =   4440
         Picture         =   "frmCalendrier.frx":0ABA
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "Remettre la zone a blanc"
         Top             =   5160
         Width           =   735
      End
      Begin VB.CommandButton cmdOk 
         Height          =   735
         Left            =   5280
         Picture         =   "frmCalendrier.frx":1784
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   5160
         Width           =   735
      End
      Begin MSACAL.Calendar Calendar1 
         Height          =   4815
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   5895
         _Version        =   524288
         _ExtentX        =   10398
         _ExtentY        =   8493
         _StockProps     =   1
         BackColor       =   -2147483633
         Year            =   2001
         Month           =   9
         Day             =   7
         DayLength       =   1
         MonthLength     =   2
         DayFontColor    =   0
         FirstDay        =   2
         GridCellEffect  =   1
         GridFontColor   =   10485760
         GridLinesColor  =   -2147483632
         ShowDateSelectors=   -1  'True
         ShowDays        =   -1  'True
         ShowHorizontalGrid=   -1  'True
         ShowTitle       =   0   'False
         ShowVerticalGrid=   -1  'True
         TitleFontColor  =   10485760
         ValueIsNull     =   0   'False
         BeginProperty DayFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty GridFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty TitleFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2280
         TabIndex        =   5
         Top             =   5160
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   5160
         Width           =   1935
      End
   End
End
Attribute VB_Name = "frmCalendrier"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub Calendar1_Click()

Me.Label2.Caption = Me.Calendar1.Value

End Sub

Private Sub cmdOK_Click()
    
strRetour = "OK" & Chr(0) & Me.Calendar1.Value
Unload Me

End Sub

Private Sub cmdSuppdate_Click()
strRetour = "BLANC"
Unload Me
End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("CONSOLE", "83")

'-> Libelle
Me.Label1.Caption = GetMesseprog("CONSOLE", "84")

End Sub
