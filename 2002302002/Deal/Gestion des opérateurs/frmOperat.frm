VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmOperat 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   8940
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6180
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8940
   ScaleWidth      =   6180
   Begin VB.Frame Frame2 
      Height          =   3495
      Left            =   0
      TabIndex        =   20
      Top             =   4680
      Width           =   6135
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   2880
         Top             =   720
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   2
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOperat.frx":0000
               Key             =   "CLOSE"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOperat.frx":039A
               Key             =   "IDENT"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.TreeView TreeView1 
         Height          =   3135
         Left            =   120
         TabIndex        =   21
         Top             =   240
         Width           =   5895
         _ExtentX        =   10398
         _ExtentY        =   5530
         _Version        =   393217
         LabelEdit       =   1
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
   End
   Begin VB.CommandButton Command3 
      Height          =   615
      Left            =   5400
      Picture         =   "frmOperat.frx":0734
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   8280
      Width           =   735
   End
   Begin VB.CommandButton Command2 
      Height          =   615
      Left            =   4560
      Picture         =   "frmOperat.frx":13FE
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   8280
      Width           =   735
   End
   Begin VB.Frame Frame1 
      Height          =   4575
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   6135
      Begin VB.ComboBox Combo2 
         Height          =   315
         Left            =   2040
         TabIndex        =   3
         Top             =   1320
         Width           =   2055
      End
      Begin VB.CommandButton Command4 
         Height          =   615
         Left            =   5160
         Picture         =   "frmOperat.frx":20C8
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   2400
         Width           =   855
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   1335
         Left            =   2040
         TabIndex        =   9
         Top             =   3120
         Width           =   3975
         _ExtentX        =   7011
         _ExtentY        =   2355
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Code"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "D�signation"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.CommandButton Command1 
         Height          =   615
         Left            =   4200
         Picture         =   "frmOperat.frx":2992
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   2400
         Width           =   855
      End
      Begin VB.PictureBox Picture1 
         Height          =   2055
         Left            =   4200
         ScaleHeight     =   1995
         ScaleWidth      =   1755
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         MaxLength       =   10
         TabIndex        =   0
         Top             =   240
         Width           =   2055
      End
      Begin VB.TextBox Text2 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2040
         MaxLength       =   10
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   600
         Width           =   2055
      End
      Begin VB.TextBox Text3 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2040
         MaxLength       =   10
         PasswordChar    =   "*"
         TabIndex        =   2
         Top             =   960
         Width           =   2055
      End
      Begin VB.TextBox Text4 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         MaxLength       =   10
         TabIndex        =   4
         Top             =   1680
         Width           =   1215
      End
      Begin VB.CheckBox Check1 
         Alignment       =   1  'Right Justify
         Caption         =   "Check1"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   2520
         Width           =   3975
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   2040
         TabIndex        =   5
         Top             =   2040
         Width           =   2055
      End
      Begin VB.Label Label7 
         Caption         =   "Label7"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label Label6 
         Caption         =   "Label6"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   3120
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "Label2"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   600
         Width           =   1815
      End
      Begin VB.Label Label3 
         Caption         =   "Label2"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   960
         Width           =   1815
      End
      Begin VB.Label Label4 
         Caption         =   "Label2"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   1680
         Width           =   1815
      End
      Begin VB.Image Image2 
         Height          =   480
         Left            =   3240
         Picture         =   "frmOperat.frx":365C
         Top             =   1605
         Width           =   480
      End
      Begin VB.Label Label5 
         Caption         =   "Label2"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   2040
         Width           =   1815
      End
      Begin VB.Image Image1 
         Height          =   240
         Left            =   3720
         Picture         =   "frmOperat.frx":4326
         Top             =   1730
         Width           =   240
      End
   End
End
Attribute VB_Name = "frmOperat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ToSave As Boolean

Public Sub Init(CodeOperateur As String)

Dim ParamOperat As String
Dim i As Integer, j As Integer
Dim Photo As String
Dim Idents As String
Dim DefIdent As String
Dim x As ListItem
Dim aNode As Node, NodeMenu As Node
Dim ListeMenu As String

'-> Titre de la feuille
Me.Caption = CodeOperateur

'-> Titre des Labels
Me.Label1.Caption = GetMesseprog("APPLIC", "19")
Me.Label2.Caption = GetMesseprog("APPLIC", "20")
Me.Label3.Caption = GetMesseprog("APPLIC", "21")
Me.Label4.Caption = GetMesseprog("APPLIC", "22")
Me.Label5.Caption = GetMesseprog("MENU", "5")
Me.Check1.Caption = GetMesseprog("APPLIC", "23")
Me.Label6.Caption = GetMesseprog("APPLIC", "24")
Me.Frame2.Caption = GetMesseprog("APPLIC", "5")
Me.Label7.Caption = GetMesseprog("APPLIC", "28")

'-> Charger la liste des code langues
For i = 1 To 10
    Me.Combo1.AddItem DeCrypt(GetMesseprog("PAYS", CStr(i)))
Next

'-> Charger les niveaux de s�curit�
Me.Combo2.AddItem GetMesseprog("CONSOLE", "13")
Me.Combo2.AddItem GetMesseprog("CONSOLE", "14")
Me.Combo2.AddItem GetMesseprog("CONSOLE", "15")

'-> Init des valeurs
Me.Text1.Text = CodeOperateur
Me.Text2.Text = DeCrypt(GetIniFileValue(CodeOperateur, "PWD", OperatsFile))
Me.Text3.Text = DeCrypt(GetIniFileValue(CodeOperateur, "PWD", OperatsFile))
Me.Text4.Text = DeCrypt(GetIniFileValue(CodeOperateur, "LIMIT", OperatsFile))
Me.Combo1.ListIndex = CInt(DeCrypt(GetIniFileValue(CodeOperateur, "LANGUE", OperatsFile))) - 1
Me.Check1.Value = CInt(DeCrypt(GetIniFileValue(CodeOperateur, "ACCESS", OperatsFile)))
Photo = DeCrypt(GetIniFileValue(CodeOperateur, "PHOTO", OperatsFile))
If Trim(Photo) <> "" And Dir$(ApplicationPath & "\B3D\img\" & Photo) <> "" Then
    '-> Charger la photo
    Me.Picture1.Picture = LoadPicture(ApplicationPath & "\B3D\img\" & Photo)
    '-> Positionner son tag
    Me.Picture1.Tag = Photo
End If

'-> Positionner son niveau de s�curit�
DefIdent = DeCrypt(GetIniFileValue(CodeOperateur, "NV", OperatsFile))
Select Case UCase$(DefIdent)
    Case "USER"
        Me.Combo2.ListIndex = 0
    Case "DEV"
        Me.Combo2.ListIndex = 1
    Case "ADM"
        Me.Combo2.ListIndex = 2
    Case Else
        Me.Combo2.ListIndex = 0
End Select

'-> R�cup�rer la liste des identifiants
Idents = DeCrypt(GetIniFileValue(CodeOperateur, "IDENTS", OperatsFile))
'-> Vider le listview
Me.ListView1.ListItems.Clear
If Idents <> "" Then
    '-> Afficher la liste des idents
    For i = 1 To NumEntries(Idents, "|")
        '-> Get d'un d�finition d'ident
        DefIdent = Entry(i, Idents, "|")
        If Trim(DefIdent) = "" Then Exit For
        '-> Ajouter un ident
        Set x = Me.ListView1.ListItems.Add(, "IDENT|" & DefIdent, DefIdent)
        '-> Get de son libell�
        x.SubItems(1) = DeCrypt(GetIniFileValue("PARAM", "LIBEL", ApplicationPath & "\" & DefIdent & "\Param\" & DefIdent & ".ini"))
        '-> AJouter un node dans la liste des menus
        Set aNode = Me.TreeView1.Nodes.Add(, , "IDENT|" & x.Text, x.Text & " - " & x.SubItems(1), "IDENT")
        aNode.Tag = "IDENT"
        '-> Charger la liste des menus affect�s � cet ident
        ListeMenu = DeCrypt(GetIniFileValue(CodeOperateur, "MENU_" & x.Text, OperatsFile))
        If Trim(ListeMenu) <> "" Then
            For j = 1 To NumEntries(ListeMenu, "|")
                Set NodeMenu = Me.TreeView1.Nodes.Add(aNode.Key, 4, DefIdent & "|" & Entry(j, ListeMenu, "|"), Entry(j, ListeMenu, "|"), "CLOSE")
                NodeMenu.Selected = True
                NodeMenu.Tag = "MENU"
            Next 'Pour tous les menus
        End If 'S''il ya des menus affect�s
    Next 'Pour tous les idents
    '-> Formatter le listview
    FormatListView Me.ListView1
End If 'Si on trouve les idents

'-> Ne pas enregistrer
ToSave = False

End Sub


Private Sub Check1_Click()
ToSave = True
End Sub

Private Sub Combo1_Click()

ToSave = True

End Sub

Private Sub Combo2_Click()
ToSave = True
End Sub

Private Sub Command1_Click()

Set Me.Picture1.Picture = Nothing
Me.Picture1.Tag = ""
ToSave = True

End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Command3_Click()

'-> Enregistrer
SaveOperat

End Sub

Private Sub SaveOperat()

Dim x As ListItem
Dim Temp As String
Dim aNode As Node
Dim i As Integer

'---> V�rification du mot de passe
If Trim(Me.Text2.Text) = "" Then
    MsgBox GetMesseprog("APPLIC", "29"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    Me.Text2.SetFocus
    Exit Sub
End If
If Trim(Me.Text3.Text) = "" Then
    MsgBox GetMesseprog("APPLIC", "29"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    Me.Text3.SetFocus
    Exit Sub
End If
If Me.Text2.Text <> Me.Text3.Text Then
    MsgBox GetMesseprog("APPLIC", "30"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    Me.Text2.SetFocus
    Exit Sub
End If

'---> V�rification du code langue
If Me.Combo1.ListIndex = -1 Then
    MsgBox GetMesseprog("APPLIC", "29"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    Me.Combo1.SetFocus
    Exit Sub
End If

'---> V�rification du niveau de s�curit�
If Me.Combo2.ListIndex = -1 Then
    MsgBox GetMesseprog("APPLIC", "29"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    Me.Combo2.SetFocus
    Exit Sub
End If

'---> Enregistrement des valeurs

'-> Supprimer dans un premier temps la section
WritePrivateProfileSection Me.Text1.Text, vbNullString, OperatsFile

'-> Mot de passe
WritePrivateProfileString Me.Text1.Text, "PWD", Crypt(Me.Text2.Text), OperatsFile

'-> Langue
WritePrivateProfileString Me.Text1.Text, "LANGUE", Crypt(CStr(Me.Combo1.ListIndex + 1)), OperatsFile

'-> Langue
WritePrivateProfileString Me.Text1.Text, "LIMIT", Crypt(Me.Text4.Text), OperatsFile

'-> Multi
WritePrivateProfileString Me.Text1.Text, "ACCESS", Crypt(CStr(Me.Check1.Value)), OperatsFile

'-> Photo
WritePrivateProfileString Me.Text1.Text, "PHOTO", Crypt(Me.Picture1.Tag), OperatsFile

'-> Niveau
Select Case Me.Combo2.ListIndex
    Case 0
        WritePrivateProfileString Me.Text1.Text, "NV", Crypt("USER"), OperatsFile
    Case 1
        WritePrivateProfileString Me.Text1.Text, "NV", Crypt("DEV"), OperatsFile
    Case 2
        WritePrivateProfileString Me.Text1.Text, "NV", Crypt("ADM"), OperatsFile
End Select
    
'-> Idents
Temp = ""
For Each x In Me.ListView1.ListItems
    If Temp = "" Then
        Temp = x.Text
    Else
        Temp = Temp & "|" & x.Text
    End If
Next
WritePrivateProfileString Me.Text1.Text, "IDENTS", Crypt(Temp), OperatsFile

'-> Menus par ident
For Each x In Me.ListView1.ListItems
    '-> Vider la variable
    Temp = ""
    '-> Pointer sur le node associ�
    Set aNode = Me.TreeView1.Nodes("IDENT|" & x.Text)
    '-> V�rifier s'il y a des nodes enfants
    If aNode.Children <> 0 Then
        '-> Get du dernier index
        i = aNode.Child.Index
        Temp = aNode.Child.Text
        Do While i <> aNode.Child.LastSibling.Index
            '-> Concatainer la liste des menus
            Temp = Temp & "|" & Me.TreeView1.Nodes(i).Next.Text
            i = Me.TreeView1.Nodes(i).Next.Index
        Loop 'Pour tous les enfants
        '-> Enregistrer
        WritePrivateProfileString Me.Text1.Text, "MENU_" & x.Text, Crypt(Temp), OperatsFile
    Else
        WritePrivateProfileString Me.Text1.Text, "MENU_" & x.Text, "", OperatsFile
    End If 'S'il y a des menus affect�s
Next 'Pour tous les idents affect�s
'WritePrivateProfileString Me.Text1.Text, "PWD", Crypt(Me.Text2.Text), OperatsFile

'-> Indiquer que l'on a enregistr�
ToSave = False


End Sub

Private Sub Command4_Click()

'-> Vider la variable de retour
strRetour = ""

'-> Afficher le choix de l'image
frmAddPicture.Show vbModal

'-> Tester le retour
If strRetour = "" Then Exit Sub

'-> Charger la photo
Set Me.Picture1.Picture = LoadPicture(ApplicationPath & "\B3D\img\" & strRetour)

'-> Positionner le tag
Me.Picture1.Tag = strRetour

'-> Vider la variable de retour
strRetour = ""
ToSave = True

End Sub



Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

Dim Rep As VbMsgBoxResult

'-> Demander si on doit enregistrer
If ToSave Then
    Rep = MsgBox(GetMesseprog("MENU", "6") & Chr(13) & Me.Text1.Text, vbQuestion + vbYesNoCancel, GetMesseprog("MENU", "6"))
    If Rep = vbCancel Then
        Cancel = 1
        Exit Sub
    ElseIf Rep = vbYes Then
        SaveOperat
    End If
End If 'Si on doit enregistrer

End Sub

Private Sub Image1_Click()

ToSave = True
Me.Text4.Text = "NO_LIMIT"

End Sub

Private Sub Image2_Click()

'-> Gestion de la date
strRetour = ""

'-> Gestion de la date limitte
frmCalendar.Show vbModal

'-> Tester le retour
If strRetour <> "" Then
    Me.Text4.Text = strRetour
    strRetour = ""
End If

'-> Indiquer que l'on doit enregistrer
ToSave = True


End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

'-> Ne rien faire si pas de bouton droit
If Button <> vbRightButton Then Exit Sub

'-> Autorisation des menus
If Me.ListView1.ListItems.Count = 0 Then
    mdiConsole.mnuDelIdent.Enabled = False
Else
    mdiConsole.mnuDelIdent.Enabled = True
End If
mdiConsole.MnuAddIdent.Enabled = True

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu mdiConsole.mnuGestIdent

'-> Selon le type de retour
Select Case strRetour
    Case "ADD"
        Call AddIdent
    Case "DEL"
        Call DelIdent
        
End Select

'-> Vider la variable de retour
strRetour = ""
End Sub

Private Sub AddIdent()

'---> Cette proc�dure autorise un ident pour un op�rateur

Dim ListeIdents As String
Dim DefIdent As String
Dim i As Integer
Dim x As ListItem
Dim aNode As Node

'-> Charger la feuille d'ajout
Load frmAdd

'-> Ajouter 2 colonnes
frmAdd.ListView1.ColumnHeaders.Add , , "Code"
frmAdd.ListView1.ColumnHeaders.Add , , "Designation"

'-> Charger la liste des idents
ListeIdents = GetIniFileValue("IDENTS", "", ApplicationPath & "B3D\Param\Idents.ini", True)

'-> Analyse de la liste des idents
If Trim(ListeIdents) <> "" Then
    '-> Pour tous les idents
    For i = 1 To NumEntries(ListeIdents, Chr(0))
        '-> Get d'une d�finition d'ident
        DefIdent = Entry(i, ListeIdents, Chr(0))
        If Trim(DefIdent) = "" Then Exit For
        '-> Ajouter un itel dans le listView
        Set x = frmAdd.ListView1.ListItems.Add(, , Entry(1, DefIdent, "="))
        x.SubItems(1) = DeCrypt(Entry(2, DefIdent, "="))
    Next '-> Pour tous les idents
End If 'S'il y a des idents

'-> Formatter le listview
FormatListView frmAdd.ListView1

'-> Vider la variable de retour
strRetour = ""

'-> Afficher la feuille
frmAdd.Show vbModal

'-> Tester la variable de retour
If strRetour = "" Then Exit Sub

'-> V�rifier si le code saisi est d�ja inscrit
For Each x In Me.ListView1.ListItems
    If Trim(UCase$(Entry(2, x.Key, "|"))) = UCase$(Trim(strRetour)) Then Exit Sub
Next 'Pour tous les idents affect�s

'-> Ajouter le nouvel Ident
Set x = Me.ListView1.ListItems.Add(, "IDENT|" & UCase$(Trim(strRetour)), strRetour)

'-> Positionner son libell�
x.SubItems(1) = GetIniFileValue("IDENTS", Trim(strRetour), ApplicationPath & "B3D\Param\Idents.ini", False)

'-> Ajouter un node dans le menu
Set aNode = Me.TreeView1.Nodes.Add(, , x.Key, strRetour & " - " & x.SubItems(1), "IDENT")
aNode.Tag = "IDENT"

'-> Formatter le listview
FormatListView Me.ListView1
ToSave = True


End Sub

Private Sub DelIdent()

'---> Cette proc�dure supprime un ident de la liste des idents affect�s � cet op�rateur

'-> Supprimer l'ident du listView
Me.TreeView1.Nodes.Remove (Me.ListView1.SelectedItem.Key)

'-> Supprimer du listview
Me.ListView1.ListItems.Remove (Me.ListView1.SelectedItem.Key)
ToSave = True

End Sub

Private Sub Text2_Change()
ToSave = True
End Sub

Private Sub Text2_GotFocus()
SelectTxtBox Me.Text2
End Sub

Private Sub Text3_Change()
ToSave = True
End Sub

Private Sub Text3_GotFocus()
SelectTxtBox Me.Text3
End Sub

Private Sub TreeView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

'-> Que le bouton droit
If Button <> vbRightButton Then Exit Sub

'-> Ne rien faire si pas de menu
If Me.TreeView1.Nodes.Count = 0 Then Exit Sub

'-> N'afficher que sur les nodes des idents
If Me.TreeView1.SelectedItem.Tag = "" Then Exit Sub

'-> Autorisation des menus
If Me.TreeView1.SelectedItem.Tag = "IDENT" Then
    mdiConsole.MnuAddIdent.Enabled = True
    mdiConsole.mnuDelIdent.Enabled = False
Else
    mdiConsole.MnuAddIdent.Enabled = False
    mdiConsole.mnuDelIdent.Enabled = True
End If

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu mdiConsole.mnuGestIdent

'-> Selon le type de retour
Select Case strRetour
    Case "ADD"
        Call AddMenu
    Case "DEL"
        Me.TreeView1.Nodes.Remove (Me.TreeView1.SelectedItem.Key)
        ToSave = True
End Select

'-> Vider la variable de retour
strRetour = ""

End Sub

Private Sub AddMenu()

Dim Filename As String
Dim x As ListItem
Dim aNode As Node

'-> Charger la feuille d'ajout
Load frmAdd

'-> Ajouter 2 colonnes
frmAdd.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "6")

'-> Charger la liste des menus
Filename = Dir$(ApplicationPath & Entry(2, Me.TreeView1.SelectedItem.Key, "|") & "\Param\*.men")

'-> Analyse r�cursive de tous les menus
Do While Filename <> ""
    '-> Ajouter une icone dans le listView
    Set x = frmAdd.ListView1.ListItems.Add(, , Filename)
    '-> Lire l'entr�e suivante
    Filename = Dir
Loop 'Pour tous les menus

'-> Formatter le listview
FormatListView frmAdd.ListView1

'-> Vider la variable de retour
strRetour = ""

'-> Afficher la feuille
frmAdd.Show vbModal

'-> Tester la variable de retour
If strRetour = "" Then Exit Sub

'-> V�rifier que ce niveau ne soit pas d�ja affect�
If IsMenu(Entry(2, Me.TreeView1.SelectedItem.Key, "|") & "|" & strRetour) Then Exit Sub

'-> Ajouter le menu
Set aNode = Me.TreeView1.Nodes.Add(Me.TreeView1.SelectedItem.Key, 4, Entry(2, Me.TreeView1.SelectedItem.Key, "|") & "|" & strRetour, strRetour, "CLOSE")
Me.TreeView1.SelectedItem.Expanded = True
aNode.Selected = True
aNode.Tag = "MENU"
ToSave = True


End Sub

Private Function IsMenu(Key As String) As Boolean

Dim aNode As Node

On Error GoTo GestError

Set aNode = Me.TreeView1.Nodes(Key)
IsMenu = True

Exit Function

GestError:

End Function
