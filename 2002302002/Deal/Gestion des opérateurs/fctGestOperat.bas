Attribute VB_Name = "fctGestOperat"
Option Explicit

'-> D�claration des API
Public Type POINTAPI
    x As Long
    y As Long
End Type
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function ScreenToClient Lib "user32" (ByVal hwnd As Long, lpPoint As POINTAPI) As Long
Public Declare Function IsWindow Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function SendMessage& Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any)
Public Declare Function SetWindowText& Lib "user32" Alias "SetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String)
Public Declare Function GetWindowText& Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String, ByVal cch As Long)

'-> Variables de param�trage
Public ApplicationPath As String
Public CodeLangue As Integer
Public OperatsFile As String
Public IdentsFile As String

Private Sub Init()

'---> Proc�dure d'initialisation et de chargement de la liste des menus

Dim i As Integer
Dim x As ListItem
Dim ListOperats As Long
Dim DefOperat As String
Dim lpBuffer As String

'-> Positionner le fichier des op�rateurs
OperatsFile = ApplicationPath & "B3D\Param\Operats.ini"
IdentsFile = ApplicationPath & "B3D\Param\Idents.ini"

'-> Positionner le fichier des messprog
MessprogIniFile = ApplicationPath & "B3D\Param\Messprog-" & Format(CodeLangue, "00") & ".ini"

'-> Messprog des menus
mdiConsole.MnuAddOperat.Caption = GetMesseprog("MENU", "1")
mdiConsole.MnuAddIdent.Caption = GetMesseprog("MENU", "1")
mdiConsole.mnuDelOperat.Caption = GetMesseprog("MENU", "2")
mdiConsole.mnuDelIdent.Caption = GetMesseprog("MENU", "2")
mdiConsole.mnuEditOperat.Caption = GetMesseprog("MENU", "3")

'-> R�cup�rer la liste des op�rateurs
lpBuffer = Space$(32000)
ListOperats = GetPrivateProfileSectionNames(lpBuffer, Len(lpBuffer), OperatsFile)
If ListOperats <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, ListOperats)
    For i = 1 To NumEntries(lpBuffer, Chr(0))
        '-> Quitter si pas de def
        If Trim(Entry(i, lpBuffer, Chr(0))) = "" Then Exit For
        '-> Ajouter dans le listview
        Set x = mdiConsole.ListView1.ListItems.Add(, "OPE|" & Entry(i, lpBuffer, Chr(0)), , "OPERAT")
        x.Text = Entry(2, x.Key, "|")
    Next 'Pour tous les op�rateurs
End If 'S'il y a des op�rateurs

End Sub


Public Sub Main()

'---> Point d'entr�e du programme

'Ligne de commande Dir des applic "�" Code Langue

'-> Setting des variables de connexions
ApplicationPath = Entry(1, Command$, "�")
CodeLangue = CInt(Entry(2, Command$, "�"))

'-> Lancer l'initialisation
Call Init

'-> Afficher le gestionnaire
mdiConsole.Show

End Sub
