VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.MDIForm mdiConsole 
   BackColor       =   &H8000000C&
   Caption         =   "Deal Informatique"
   ClientHeight    =   7125
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9465
   Icon            =   "frmConsole.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   3
      Top             =   6750
      Width           =   9465
      _ExtentX        =   16695
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13626
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            TextSave        =   "04/04/2002"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picNaviga 
      Align           =   3  'Align Left
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   6750
      Left            =   0
      ScaleHeight     =   450
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   232
      TabIndex        =   1
      Top             =   0
      Width           =   3480
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   1200
         Top             =   4080
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   1
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":0CCA
               Key             =   "OPERAT"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   2415
         Left            =   0
         TabIndex        =   4
         Top             =   600
         Width           =   3015
         _ExtentX        =   5318
         _ExtentY        =   4260
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Code"
            Object.Width           =   38100
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Libel"
            Object.Width           =   38100
         EndProperty
      End
      Begin VB.PictureBox picTitre 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         Height          =   90
         Left            =   360
         ScaleHeight     =   6
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   128
         TabIndex        =   2
         Top             =   240
         Width           =   1920
      End
      Begin VB.Image imgClose 
         Height          =   165
         Left            =   2040
         Picture         =   "frmConsole.frx":19A4
         Top             =   120
         Width           =   165
      End
   End
   Begin VB.PictureBox picSplit 
      Align           =   3  'Align Left
      BorderStyle     =   0  'None
      Height          =   6750
      Left            =   3480
      MousePointer    =   9  'Size W E
      ScaleHeight     =   6750
      ScaleWidth      =   105
      TabIndex        =   0
      Top             =   0
      Width           =   105
   End
   Begin VB.Menu mnuGestOperat 
      Caption         =   "OPERAT"
      Visible         =   0   'False
      Begin VB.Menu MnuAddOperat 
         Caption         =   ""
      End
      Begin VB.Menu mnuEditOperat 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelOperat 
         Caption         =   ""
      End
   End
   Begin VB.Menu mnuGestIdent 
      Caption         =   "IDENT"
      Visible         =   0   'False
      Begin VB.Menu MnuAddIdent 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelIdent 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "mdiConsole"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)

'---> Gestion des menus

Dim aX As ListItem

'-> Get du listitem sur lequel on a cliqu�
Set aX = Me.ListView1.HitTest(X, y)
If Not aX Is Nothing Then aX.Selected = True

'-> Ne rien faire si pas de bouton droit
If Button <> vbRightButton Then Exit Sub

'-> Autorisation des menus
If Me.ListView1.ListItems.Count = 0 Then
    Me.mnuDelOperat.Enabled = False
    Me.mnuEditOperat.Enabled = False
Else
    Me.mnuDelOperat.Enabled = True
    Me.mnuEditOperat.Enabled = True
End If

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu mdiConsole.mnuGestOperat

'-> Selon le type de retour
Select Case strRetour
    Case "ADD"
        Call AddOperat
    Case "EDIT"
        Call EditOperat
    Case "DEL"
        Call DelOperat
        
End Select

'-> Vider la variable de retour
strRetour = ""

End Sub
Private Sub DelOperat()

'---> Suppression d'un op�rateur

Dim Rep As VbMsgBoxResult

'-> Demander confirmation
Rep = MsgBox(GetMesseprog("MENU", "2") & Chr(13) & Me.ListView1.SelectedItem.Text, vbQuestion + vbYesNo, GetMesseprog("MENU", "6"))
If Rep = vbNo Then Exit Sub

'-> Supprimer du fichier
WritePrivateProfileSection Me.ListView1.SelectedItem.Text, vbNullString, OperatsFile

'-> Supprimer l'icone
Me.ListView1.ListItems.Remove Me.ListView1.SelectedItem.Key



End Sub

Private Sub AddOperat()

'---> Ajouter un op�rateur

Dim Rep As String
Dim X As ListItem

'-> Demander le nouveau code
Rep = InputBox(GetMesseprog("APPLIC", "1"), GetMesseprog("MENU", "1"), "")
If Trim(Rep) = "" Then Exit Sub

'-> V�rifier si le code saisie
Rep = UCase$(Trim(Rep))
If IsOperat(Rep) Then
    MsgBox GetMesseprog("APPLIC", "2"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    Exit Sub
End If

'-> Ajouter un nouveau Code
WritePrivateProfileString Rep, "PWD", Crypt("deal"), OperatsFile
WritePrivateProfileString Rep, "LANGUE", Crypt("1"), OperatsFile
WritePrivateProfileString Rep, "LIMIT", Crypt("NO_LIMIT"), OperatsFile
WritePrivateProfileString Rep, "ACCESS", Crypt("1"), OperatsFile
WritePrivateProfileString Rep, "NV", Crypt("USER"), OperatsFile

'-> Ajouter une nouvelle icone
Set X = Me.ListView1.ListItems.Add(, "OPE|" & Rep, Rep, "OPERAT")
X.Selected = True
X.EnsureVisible

'-> Lancer l'�dition de ce nouveau Code
EditOperat

End Sub

Private Function IsOperat(Code As String) As Boolean

'---> V�rifier si le code saisi existe d�ja

Dim X As ListItem

On Error GoTo GestError

'-> Essayer de pointer sur le listitem
Set X = Me.ListView1.ListItems("OPE|" & Code)

'-> Renvoyer une valeur de succ�s
IsOperat = True
Exit Function

GestError:
    
    

End Function

Private Sub EditOperat()

'---> Cette proc�dure edite un op�rateur

Dim aFrm As frmOperat

'-> Tester si la feuille n'est pas d�ja active
If Me.ListView1.SelectedItem.Tag <> "" Then
    If IsWindow(CLng(Me.ListView1.SelectedItem.Tag)) = 1 Then
        '-> Activer le feuille
        SendMessage CLng(Me.ListView1.SelectedItem.Tag), WM_CHILDACTIVATE, 0, 0
        Exit Sub
    End If
End If

'-> Bloquer l'�cran
LockWindowUpdate Me.hwnd

'-> Charger une nouvelle feuille
Set aFrm = New frmOperat
aFrm.Init Entry(2, Me.ListView1.SelectedItem.Key, "|")

'-> Initialiser
aFrm.Show

'-> Positionner son handle
Me.ListView1.SelectedItem.Tag = aFrm.hwnd

'-> Lib�rer le pointeur
LockWindowUpdate 0

End Sub

Private Sub MnuAddIdent_Click()
strRetour = "ADD"
End Sub

Private Sub MnuAddOperat_Click()
strRetour = "ADD"
End Sub

Private Sub mnuDelIdent_Click()
strRetour = "DEL"
End Sub

Private Sub mnuDelOperat_Click()
strRetour = "DEL"
End Sub

Private Sub mnuEditOperat_Click()
strRetour = "EDIT"
End Sub

Private Sub picNaviga_Resize()

Dim aRect As RECT
Dim res As Long

On Error Resume Next

'-> R�cup�rer la taille de la zone cliente
res = GetClientRect(Me.picNaviga.hwnd, aRect)

'-> Positionner l'entete de la fen�tre
Me.picTitre.Left = 3
Me.picTitre.Top = 7
Me.picTitre.Width = aRect.Right - 6 - imgClose.Width
Me.imgClose.Left = aRect.Right - imgClose.Width
Me.imgClose.Top = Me.picTitre.Top - 2

'-> Positionner le treeview
Me.ListView1.Left = Me.picTitre.Left
Me.ListView1.Top = Me.picTitre.Top * 2 + Me.picTitre.Height
Me.ListView1.Width = Me.picTitre.Width + Me.imgClose.Width + 6
Me.ListView1.Height = Me.picNaviga.ScaleY(Me.picNaviga.Height, 1, 3) - 21
End Sub
Private Sub picTitre_Resize()

'---> Dessiner la barre de titre
Me.picTitre.Line (0, 0)-(Me.picTitre.Width - 1, 0), RGB(255, 255, 255)
Me.picTitre.Line (0, 1)-(Me.picTitre.Width - 1, 1), RGB(255, 255, 255)
Me.picTitre.Line (1, 1)-(Me.picTitre.Width - 1, 1), &HE0E0E0
Me.picTitre.Line (0, 3)-(Me.picTitre.Width - 1, 3), RGB(255, 255, 255)
Me.picTitre.Line (0, 4)-(Me.picTitre.Width - 1, 4), RGB(255, 255, 255)
Me.picTitre.Line (1, 4)-(Me.picTitre.Width - 1, 4), &HE0E0E0

End Sub
Private Sub picSplit_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)

Dim aPt As POINTAPI
Dim res As Long

If Button = vbLeftButton Then
    '-> R�cup�rer la position du curseur
    res = GetCursorPos(aPt)
    '-> Convertir en coordonn�es clientes
    res = ScreenToClient(Me.hwnd, aPt)
    '-> Tester les positions mini et maxi
    If aPt.X > Me.picNaviga.ScaleX(2, 7, 3) And aPt.X < Me.picNaviga.ScaleX(Me.Width, 1, 3) - Me.picNaviga.ScaleX(2, 7, 3) Then Me.picNaviga.Width = Me.picNaviga.ScaleX(aPt.X, 3, 1)
End If

End Sub


