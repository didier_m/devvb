VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmAddPicture 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   6510
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   7740
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6510
   ScaleWidth      =   7740
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   1680
      Top             =   1080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   48
      ImageHeight     =   48
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAddPicture.frx":0000
            Key             =   "PIC"
         EndProperty
      EndProperty
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   5760
      TabIndex        =   3
      Top             =   0
      Width           =   1935
   End
   Begin VB.CommandButton Command1 
      Height          =   855
      Left            =   5760
      Picture         =   "frmAddPicture.frx":1CDA
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   5640
      Width           =   1935
   End
   Begin VB.PictureBox Picture1 
      Height          =   5175
      Left            =   5760
      ScaleHeight     =   5115
      ScaleWidth      =   1875
      TabIndex        =   1
      Top             =   360
      Width           =   1935
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   6495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5745
      _ExtentX        =   10134
      _ExtentY        =   11456
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "frmAddPicture"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Combo1_Click()

Dim Filename As String
Dim x As ListItem

On Error Resume Next

'-> Bloquer la mise � jour de l'�cran
LockWindowUpdate Me.ListView1.hwnd
Me.Enabled = False
Screen.MousePointer = 11

'-> Vider le listview
Me.ListView1.ListItems.Clear

'-> Analyse
Filename = Dir$(ApplicationPath & "B3D\Img\" & Me.Combo1.Text)
Do While Filename <> ""
    '-> Ajouter un item dans la liste
    Set x = Me.ListView1.ListItems.Add(, , Filename, "PIC")
    x.Tag = ApplicationPath & "B3D\Img\" & Filename
    Filename = Dir
Loop

'-> D�loquer la mise � jour de l'�cran
LockWindowUpdate 0
Me.Enabled = True
Screen.MousePointer = 0

'-> Donner le focus au listview
Me.ListView1.SetFocus

End Sub

Private Sub Command1_Click()

'-> Tester qu'une icone soit s�lectionn�e
If Me.ListView1.SelectedItem Is Nothing Then
    '-> Message
    MsgBox GetMesseprog("APPLIC", "27"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
    '-> Donner le focus au listview
    Me.ListView1.SetFocus
End If

'-> Retourner l'image s�lectionn�
strRetour = Me.ListView1.SelectedItem.Text

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Form_Load()

'---> Charger la liste des fichiers images que l'on peut affecter

Me.Combo1.AddItem "*.bmp"
Me.Combo1.AddItem "*.jpg"
Me.Combo1.AddItem "*.jpeg"
Me.Combo1.AddItem "*.gif"
Me.Combo1.AddItem "*.ico"

'-> Titre de la feuille
Me.Caption = GetMesseprog("APPLIC", "26")

End Sub

Private Sub ListView1_ItemClick(ByVal Item As MSComctlLib.ListItem)

Set Me.Picture1.Picture = LoadPicture(Item.Tag)

End Sub

Private Sub ListView1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Command1_Click

End Sub
