VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmAdd 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   213
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   312
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ListView ListView1 
      Height          =   1335
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   2355
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "frmAdd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()

'-> Titre de la fen�tre
Me.Caption = GetMesseprog("MENU", "1")


End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next

GetClientRect Me.hwnd, aRect

Me.ListView1.Left = 0
Me.ListView1.Top = 0
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom


End Sub

Private Sub ListView1_DblClick()

If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Retourner le code de la premi�re colonne
strRetour = Me.ListView1.SelectedItem.Text

'-> D�charger la feuille
Unload Me

End Sub
