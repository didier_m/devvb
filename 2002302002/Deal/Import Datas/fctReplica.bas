Attribute VB_Name = "fctReplica"
Option Explicit

'-> Fichier de r�plication
Public ReplicationLog As String

'-> Connexion en cours
Public aConnexion As B3DConnexion

'-> API de temporisation
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
'-> API Pour gestion du temps
Public Declare Function GetTickCount& Lib "kernel32" ()


Sub Main()

'---> analyse de la ligne de command

Dim ZonCharge As String
Dim ParamFile As String
Dim ListeField As String
Dim aIt As ListItem
Dim lpBuffer As String
Dim res As Long


On Error GoTo GestError

'-> Tester la ligne de commande
If Trim(Command$) = "" Then End

'-> Bloquer l'�cran
Screen.MousePointer = 11

'-> R�cup�rer les param�tres
ParamFile = Entry(1, Command$, "�")
ZonCharge = Entry(2, Command$, "�")
            
'-> Initialiser la connexion
Set aConnexion = New B3DConnexion

'-> Charger son param�trage
If Not aConnexion.ReadParamFormFile(ParamFile) Then End
            
'-> Positionner le journal de r�plication
ReplicationLog = aConnexion.ApplicationDir & "B3D\Param\ReplicaInput.log"
            
'-> Charger la feuille
frmReplicaImp.Show
        
'-> Gestion des erreurs
GestError:
    Screen.MousePointer = 0
        
End Sub


Public Sub PrintLog(Instruction As String)

Dim hdlFile As Integer

'-> Imprimer dans le journal des LOG
hdlFile = FreeFile
Open ReplicationLog For Append As #hdlFile
Print #hdlFile, Instruction
Close #hdlFile

End Sub

