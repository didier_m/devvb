VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmReplicaImp 
   Caption         =   "R�plication Import"
   ClientHeight    =   8820
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11925
   Icon            =   "frmReplicaImp.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   588
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   795
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   2
      Top             =   8445
      Width           =   11925
      _ExtentX        =   21034
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   26458
            MinWidth        =   26458
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7200
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmReplicaImp.frx":0CCA
            Key             =   "IDENT"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmReplicaImp.frx":19A4
            Key             =   "UCID"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmReplicaImp.frx":267E
            Key             =   "EXP"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmReplicaImp.frx":2F58
            Key             =   "REP"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   4335
      Left            =   5280
      TabIndex        =   1
      Top             =   0
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   7646
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   11
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Object.Width           =   38100
      EndProperty
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   8775
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   15478
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin VB.Menu mnuPOPUP 
      Caption         =   "POPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuImport 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmReplicaImp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub IntegreFileName(Fichier As String)

'-> Lancer les instructions dans le fichier d'echange avec l'agent
Dim hdlFile As Integer
Dim Ligne As String
Dim OrdreOK As Boolean
Dim aBuffer As B3DBuffer
Dim i As Integer
Dim ZoneTest As String
Dim Temp As String

Dim Uc As String
Dim Id As String
Dim Dt As String
Dim Ty As String
Dim NbLig As Long

On Error GoTo GestError

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> Get de son param�trage
Uc = Entry(1, Entry(1, Fichier, "."), "-")
Id = Entry(2, Entry(1, Fichier, "."), "-")
Dt = Entry(3, Entry(1, Fichier, "."), "-")
Ty = Entry(4, Entry(1, Fichier, "."), "-")

'-> Controle de la coherence du fichier
hdlFile = FreeFile
Open Fichier For Input As #hdlFile

'-> Lecture premiere ligne pour analyse
Line Input #hdlFile, Ligne

'-> Recherche de la chaine "B3D_PROG~REPLICA_" qui indique que fic de replica
If InStr(1, UCase$(Ligne), "B3D_PROG~REPLICA") = 0 Then
    '-> Fermer le fichier
    Close #hdlFile
    '-> Message d'erreur
    MsgBox GetMesseprog("CONSOLE", "63"), vbExclamation + vbExclamation, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

'-> Poser le nom du programme
InitialiseB3D aConnexion, Entry(2, Ligne, "~"), False, False

If Not aConnexion.InitDmd(True) Then
    '-> Fermer le fichier
    Close #hdlFile
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    GoTo GestError
End If

Do While Not EOF(hdlFile)

    '-> Lecture ligne pour analyse
    Line Input #hdlFile, Ligne

    '-> Ne pas traiter les lignes � vide
    If Trim(Ligne) = "" Then GoTo NextLig

    '-> Sinon : on boucle sur les lignes de notre fichier et on envoi  dans le fichier d'echange avec l'agent
    OrdreOK = aConnexion.PutInstruction(Ligne, True)

    '-> Verifie si on a bien envoy� l'instruction
    If Not OrdreOK Then
        '-> Fermer le fichier
        Close #hdlFile
        '-> Positionner le code Erreur
        MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
        '-> Quitter la proc�dure
        GoTo GestError
    End If
    
    '-> Compteur de lignes
    NbLig = NbLig + 1
    
    '-> Mettre � jour l'affichage
    Me.StatusBar1.Panels(1).Text = "Lecture de la ligne : " & NbLig
    Me.StatusBar1.Refresh
    DoEvents

NextLig:
Loop '-> Chaque ligne du fichier de replica

Me.StatusBar1.Panels(1).Text = GetMesseprog("CONSOLE", "64") & " " & Now
Me.StatusBar1.Refresh
DoEvents

'-> Envoyer l'instruction et charger la matrice des champs
Set aBuffer = New B3DBuffer
If Not aConnexion.SendDemande(aBuffer) Then
    '-> Fermer le fichier
    If hdlFile <> 0 Then Close #hdlFile
    '-> Afficher un message d'erreur
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    '-> Quitter la proc�dure
    GoTo GestError
End If

'-> Boucler sur les lignes renvoy�es par l'agent
For i = 1 To aBuffer.Lignes.Count
    '-> Recup de la ligne
    Ligne = aBuffer.Lignes(i)
    If Trim(Ligne) = "" Then GoTo NextLig
    '-> Analyse de la r�ponse
    If Entry(2, Ligne, "~") <> "OK" Then
        '-> Message d'erreur
        MsgBox "Error : " & Entry(3, Ligne, "~") & " - " & Entry(4, Ligne, "~") & " - " & Entry(5, Ligne, "~"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
        '-> Imprimer une trace dans le journal des LOG
        Temp = Crypt(Uc & "|" & Id & "|" & Dt & "|" & Ty & "|" & Now & "|" & aConnexion.IdAgent & "|" & aConnexion.Operateur & "|" & Fichier & "|ERROR|" & Entry(3, Ligne, "~") & "|" & Entry(4, Ligne, "~"))
        PrintLog Temp
        '-> ajouter la ligne dans le journal des LOG
        AddJournalLoginItem Temp
        '-> Quiter la proc�dure
        GoTo GestError
    End If
    DoEvents
NextLig:
Next

'-> L'int�gration c'est bien pass�e : ajouter une ligne Ok
Temp = Crypt(Uc & "|" & Id & "|" & Dt & "|" & Ty & "|" & Now & "|" & aConnexion.IdAgent & "|" & aConnexion.Operateur & "|" & Fichier & "|OK||")
'-> Ajouter la ligne dans le journal des LOG
AddJournalLoginItem Temp

'-> Afficher un message de succ�s
MsgBox GetMesseprog("CONSOLE", "81"), vbInformation + vbOKOnly, GetMesseprog("DOG", "17")


GestError:

    '-> Bloquer l'�cran
    Me.Enabled = True
    Screen.MousePointer = 0
    '-> Fermer les fichiers ouverts
    Reset


End Sub

Private Sub AddJournalLoginItem(Log As String)

Dim x As ListItem
Dim i As Integer

'-> Decrypter la ligne
Log = DeCrypt(Log)

'-> Ajouter
Set x = Me.ListView1.ListItems.Add(, , "")

'-> Analyse des lignes
For i = 1 To 8
    If i = 1 Then
        x.Text = Entry(1, Log, "|")
    Else
        x.SubItems(i - 1) = Entry(i, Log, "|")
    End If
Next


End Sub

Private Sub Form_Load()

Dim aNode As Node
Dim FileName As String
Dim Uc As String
Dim Id As String
Dim Dt As String
Dim Ty As String

'-> Titre de la feuille
Me.Caption = GetMesseprog("CONSOLE", "62")

'-> Titre des colonnes
Me.ListView1.ColumnHeaders(1).Text = GetMesseprog("CONSOLE", "36")
Me.ListView1.ColumnHeaders(2).Text = GetMesseprog("CONSOLE", "37")
Me.ListView1.ColumnHeaders(3).Text = GetMesseprog("NATURE", "2")
Me.ListView1.ColumnHeaders(4).Text = GetMesseprog("CONSOLE", "25")
Me.ListView1.ColumnHeaders(5).Text = GetMesseprog("CONSOLE", "65")
Me.ListView1.ColumnHeaders(6).Text = GetMesseprog("CONSOLE", "35")
Me.ListView1.ColumnHeaders(7).Text = GetMesseprog("APPLIC", "19")
Me.ListView1.ColumnHeaders(8).Text = GetMesseprog("CONSOLE", "31")
Me.ListView1.ColumnHeaders(9).Text = GetMesseprog("APPLIC", "66")
Me.ListView1.ColumnHeaders(10).Text = GetMesseprog("APPLIC", "4")
Me.ListView1.ColumnHeaders(11).Text = GetMesseprog("FRMEMILIE", "5")

'-> Libell� du menu
Me.mnuImport.Caption = GetMesseprog("CONSOLE", "62")

'-> Charger le Journal des LOG
LoadJournal

'-> Formatter le ListView
FormatListView Me.ListView1

'-> Analyse du r�pertoire d'importation de r�plication
FileName = Dir$(aConnexion.MainDir & "Replica\Input\*.data")
Do While FileName <> ""
    '-> R�cup�rer ses param
    Uc = Entry(1, Entry(1, FileName, "."), "-")
    Id = Entry(2, Entry(1, FileName, "."), "-")
    Dt = Entry(3, Entry(1, FileName, "."), "-")
    Ty = Entry(4, Entry(1, FileName, "."), "-")
    
    '-> Ne prendre que les UC ID <> de celle en cours
    If Trim(UCase$(Uc)) = Trim(UCase$(aConnexion.Uc_Id)) Then GoTo NextFile
    
    '-> Ne tenir compte que des idents en cours
    If Trim(UCase$(Id)) <> Trim(UCase$(aConnexion.Ident)) Then GoTo NextFile
    
    '-> D�terminer si l'Uc est d�ja  r�f�renc�e
    If Not IsUc(Uc) Then
        '-> Cr�er un nouveau node
        Set aNode = Me.TreeView1.Nodes.Add(, , "UC|" & Trim(UCase$(Uc)), GetMesseprog("CONSOLE", "61") & " " & Uc, "UCID")
        aNode.Expanded = True
    Else
        '-> Le node existe : pointer dessus
        Set aNode = Me.TreeView1.Nodes("UC|" & Trim(UCase$(Uc)))
    End If
    
    '-> D�terminer si l'ident
    If Not IsIdent(Uc, Id) Then
        '-> Cr�er un nouveau Node
        Set aNode = Me.TreeView1.Nodes.Add("UC|" & Trim(UCase$(Uc)), 4, "UC|" & Trim(UCase$(Uc)) & "�" & Id, Id, "IDENT")
        aNode.Expanded = True
    Else
        Set aNode = Me.TreeView1.Nodes("UC|" & Trim(UCase$(Uc)) & "�" & Id)
    End If

    '-> Ajouter le fichier
    Set aNode = Me.TreeView1.Nodes.Add(aNode.Key, 4, aNode.Key & "|" & FileName, , Trim(UCase$(Ty)))
    
    '-> Positionner la date
    aNode.Text = Mid$(Dt, 1, 4) & "/" & Mid$(Dt, 5, 2) & "/" & Mid$(Dt, 7, 2)
    aNode.Tag = "FICHIER"
    
NextFile:

    '-> Analyse du fichier suivant
    FileName = Dir
Loop


End Sub

Private Sub LoadJournal()

'---> Charger le journal des LOG

Dim hdlFile As Integer
Dim Ligne As String

'-> V�rifier si on trouve le fichier
If Dir$(ReplicationLog) = "" Then Exit Sub

'-> Ouvrir le fichier des logs
hdlFile = FreeFile
Open ReplicationLog For Input As #hdlFile

'-> Lecture du fichier journal
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> Ne pas prendre les lignes � blanc
    If Trim(Ligne) = "" Then GoTo NextLigne
    '-> Ajouter une ligne dans le listview
    AddJournalLoginItem Ligne
NextLigne:
Loop


End Sub



Private Function IsUc(Uc As String) As Boolean

Dim aNode As Node

On Error GoTo GestError

'-> Essayer de pointer sur le node
Set aNode = Me.TreeView1.Nodes("UC|" & Trim(UCase$(Uc)))

'-> Renvoyer une valeur de succ�s
IsUc = True

Exit Function

GestError:

End Function

Private Function IsIdent(Uc As String, Ident As String) As Boolean

Dim aNode As Node

On Error GoTo GestError

'-> Essayer de pointer sur le node
Set aNode = Me.TreeView1.Nodes("UC|" & Trim(UCase$(Uc)) & "�" & Trim(UCase$(Ident)))

'-> Renvoyer une valeur de succ�s
IsIdent = True

Exit Function

GestError:

End Function

Private Sub Form_Resize()

On Error Resume Next

Dim aRect As RECT

GetClientRect Me.hwnd, aRect

Me.TreeView1.Left = 0
Me.TreeView1.Top = 0
Me.TreeView1.Height = aRect.Bottom - Me.StatusBar1.Height

Me.ListView1.Top = 0
Me.ListView1.Height = Me.TreeView1.Height
Me.ListView1.Left = Me.TreeView1.Width + 2
Me.ListView1.Width = aRect.Right - Me.ListView1.Left


End Sub



Private Sub mnuImport_Click()
strRetour = "IMPORT"
End Sub

Private Sub TreeView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

'-> Tester que le bouton droit soit OK
If Button <> vbRightButton Then Exit Sub

'-> Qu'il y ait des donn�es
If Me.TreeView1.SelectedItem Is Nothing Then Exit Sub

'-> Que l'on soit bien sur un type fichier
If Me.TreeView1.SelectedItem.Tag <> "FICHIER" Then Exit Sub

'-> Vider la variable de retour
strRetour = ""

'-> Afficher le menu
Me.PopupMenu Me.mnuPOPUP

'-> Select case strretour
If strRetour = "IMPORT" Then
   '-> Lancer l'int�gration
   IntegreFileName aConnexion.MainDir & "Replica\Input\" & Entry(NumEntries(Me.TreeView1.SelectedItem.Key, "|"), Me.TreeView1.SelectedItem.Key, "|")
   '-> Vider la variable d'�change
   strRetour = ""
End If

End Sub
