Attribute VB_Name = "fctReIndex"
Option Explicit

'-> Connexion en cours
Public aConnexion As B3DConnexion

'-> API de temporisation
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
'-> API Pour gestion du temps
Public Declare Function GetTickCount& Lib "kernel32" ()


Sub Main()
'---> analyse de la ligne de command
Dim ParamFile As String
Dim ZonCharge As String
Dim OrdreOk As Boolean
Dim aBuffer As B3DBuffer
Dim Ligne As String
Dim i As Integer
Dim aIt As ListItem

On Error GoTo GestError

'-> Tester la ligne de commande
If Trim(Command$) = "" Then End

'-> R�cup�rer les param�tres
ParamFile = Entry(1, Command$, "�")
ZonCharge = Entry(2, Command$, "�")

'-> Initialiser la connexion
Set aConnexion = New B3DConnexion

'-> Charger son param�trage
If Not aConnexion.ReadParamFormFile(ParamFile) Then End
        
'-> Mise � jour de l'�cran
frmReIndex.Show
frmReIndex.Enabled = False
Screen.MousePointer = 11
LockWindowUpdate frmReIndex.ListView1.hwnd
        
'-> Libell� d'attente
frmReIndex.StatusBar1.Panels(1).Text = GetMesseprog("APPLIC", "56")
DoEvents
        
'-> Poser le nom du programme
InitialiseB3D aConnexion, "REINDEX", False, False

'-> Initialisation pour demande
If Not aConnexion.InitDmd(True) Then
    '-> Msgbox d'erreur
    MsgBox GetMesseprog("APPLIC", "51"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

'-> On lance la fonction d'analyse des locks brul�es
OrdreOk = aConnexion.PutInstruction("B3D_SCHEMA_LIST~1", True)

'-> Verifie si on a bien envoy� l'instruction
If Not OrdreOk Then
    '-> Positionner le code Erreur
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    GoTo GestError
End If

'-> On envoie l'ordre de lecture des instructions
Set aBuffer = New B3DBuffer
If Not aConnexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    aConnexion.ErrorNumber = "100000"
    aConnexion.ErrorLibel = "Error sending exchange"
    aConnexion.ErrorParam = "LoadAgentDOG"
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    GoTo GestError
End If

'-> Recup de la 1ere ligne
Ligne = aBuffer.Lignes(1)

If Entry(2, Entry(1, Ligne, "�"), "~") <> "OK" Then
    MsgBox GetMesseprog("CONSOLE", "85"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

'-> On cree les entetes de colonnes
frmReIndex.ListView1.ColumnHeaders.Add , , GetMesseprog("APPLIC", "64")
frmReIndex.ListView1.ColumnHeaders.Add , , GetMesseprog("FRMSCHEMA", "4")
frmReIndex.ListView1.ColumnHeaders.Add , , GetMesseprog("CONSOLE", "56")
frmReIndex.ListView1.ColumnHeaders.Add , , GetMesseprog("CONSOLE", "86")

'-> Analyse du retour des locks repertori�s
For i = 2 To aBuffer.Lignes.Count
    '-> Recup de la ligne
    Ligne = aBuffer.Lignes(i)
    
    '-> On cree le listview des tables
    Set aIt = frmReIndex.ListView1.ListItems.Add(, , Entry(1, Ligne, "�"))
    aIt.SubItems(1) = Entry(2, Ligne, "�")
    
    
NextFichier:

Next

'-> R�cup�rer le catalogue
If Not GetIdentCatalogue(aConnexion, aConnexion.Ident, "") Then
    '-> Afficher un message d'erreur
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbCritical + vbOKOnly, "Error"
    '-> Fin du programme
    GoTo GestError
End If

'-> Formattage du listview
FormatListView frmReIndex.ListView1
            
'-> Lib�rer l'�cran
Screen.MousePointer = 0
frmReIndex.Enabled = True
frmReIndex.StatusBar1.Panels(1).Text = ""
LockWindowUpdate 0

'-> Quitter la proc�dure
Exit Sub

'-> Gestion des erreurs
GestError:
    '-> Fermer tous les fichiers
    Reset
    Screen.MousePointer = 0
    End
    
        
End Sub


