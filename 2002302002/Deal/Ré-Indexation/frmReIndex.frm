VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmReIndex 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8220
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6900
   Icon            =   "frmReIndex.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8220
   ScaleWidth      =   6900
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   3
      Top             =   7845
      Width           =   6900
      _ExtentX        =   12171
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   12347
            MinWidth        =   12347
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdExport 
      Height          =   975
      Left            =   0
      TabIndex        =   2
      Top             =   6840
      Width           =   6855
   End
   Begin VB.Frame Frame1 
      Height          =   6735
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6855
      Begin MSComctlLib.ListView ListView1 
         Height          =   6540
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   6615
         _ExtentX        =   11668
         _ExtentY        =   11536
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
End
Attribute VB_Name = "frmReIndex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Crypt As Integer

Private Sub cmdExport_Click()
'---> On lance l'export

'-> Export
ReIndexFiles

End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("CONSOLE", "87")

'-> Libelle
Me.cmdExport.Caption = GetMesseprog("CONSOLE", "88")

End Sub

Private Sub ListView1_ItemCheck(ByVal Item As MSComctlLib.ListItem)
'--> Choix d'un fichier � exporter : donne le choix de l'index d'export
Dim Ligne As String
Dim OrdreOk As Boolean
Dim aBuffer As B3DBuffer

On Error GoTo GestError

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> Positionner l'item en cours comme �l�ment actif
Set Me.ListView1.SelectedItem = Item

'-> Ligne de cde B3D
Ligne = "B3D_SCHEMA_DES~1~0~" & Trim(Me.ListView1.SelectedItem)

'-> Initialisation pour demande
If Not aConnexion.InitDmd(True) Then
    '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
    MsgBox GetMesseprog("APPLIC", "51"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

'-> On envoie une demande de recup des fichiers du catalogue
'-> On lance la fonction d'analyse des locks brul�es
OrdreOk = aConnexion.PutInstruction(Ligne, True)

'-> Verifie si on a bien envoy� l'instruction
If Not OrdreOk Then
    '-> Positionner le code Erreur
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    GoTo GestError
End If

'-> On envoie l'ordre de lecture des instructions
Set aBuffer = New B3DBuffer
If Not aConnexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    aConnexion.ErrorNumber = "100000"
    aConnexion.ErrorLibel = "Error sending exchange"
    aConnexion.ErrorParam = "LoadAgentDOG"
    MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
    GoTo GestError
End If

'-> Recup de la 1ere ligne
Ligne = aBuffer.Lignes(1)

If Entry(2, Ligne, "~") <> "OK" Then
    MsgBox GetMesseprog("CONSOLE", "55"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

If Item.Checked = True Then
    '-> affiche le schema des fichiers selectionnes dans le listview de visualisation
    LoadSchemaInListView aBuffer
End If

'-> D�bloquer l'�cran
Screen.MousePointer = 0
Me.Enabled = True

Exit Sub

'-> Gestion des erreur
GestError:
    Me.Enabled = True
    Screen.MousePointer = 0
    Item.Checked = False


End Sub

Private Sub LoadSchemaInListView(aBuffer As B3DBuffer)
'---> Permet de charger un listview avec le schema de fichiers selectionn�s
Dim i As Integer
Dim j As Integer
Dim Fonction As String
Dim Ligne As String
Dim aIt As ListItem


'-> On parcours les lignes retourn�es
For i = 2 To aBuffer.Lignes.Count
    
    '-> recup de la ligne
    Fonction = Entry(1, aBuffer.Lignes(i), "~")
    Ligne = Entry(2, aBuffer.Lignes(i), "~")
    Fonction = Entry(3, Fonction, "_")
    
    Select Case Fonction
        Case "FNAME"
            '-> teste si index direct : on sort
            If Entry(6, Ligne, "�") <> "" Then
                If Me.ListView1.SelectedItem.Checked Then
                    Me.ListView1.SelectedItem.Checked = False
                    MsgBox GetMesseprog("CONSOLE", "89"), vbInformation + vbOKOnly, GetMesseprog("DOG", "17")
                End If
                '-> Quitter la proc�dure
                Exit Sub
            Else
                '-> On nettoie le listview
                frmChoix.ListView1.ColumnHeaders.Clear
                '-> On cree les entete de colonnes du listview
                frmChoix.ListView1.ColumnHeaders.Add , , GetMesseprog("CONSOLE", "56")
                frmChoix.ListView1.ColumnHeaders.Add , , GetMesseprog("FRMSCHEMA", "4")
            End If
            
        Case "INAME"
            '-> On cree une ligne pour la data en cours
            Set aIt = frmChoix.ListView1.ListItems.Add(, , Entry(2, Ligne, "�"))
            aIt.SubItems(1) = Entry(4, Ligne, "�")
    End Select
Next

'-> Formattage du listview
FormatListView frmChoix.ListView1

strRetour = ""
'-> Refresh du listview
Screen.MousePointer = 0
frmChoix.Show vbModal

'-> Recup de l'index et affichage dans le list
If strRetour = "" Then
    Me.ListView1.SelectedItem.Checked = False
Else
    Me.ListView1.SelectedItem.SubItems(2) = strRetour
    strRetour = ""
End If


End Sub


Private Sub ReIndexFiles()
'---> permet de reindexer les fichiers
Dim i As Integer
Dim OrdreOk As Boolean
Dim aBuffer As B3DBuffer
Dim Ligne As String
Dim X As ListItem
Dim FindOne As Boolean

On Error GoTo GestError

'-> Ne rien faire si pas de table s�lectionn�
For Each X In Me.ListView1.ListItems
    If X.Checked Then
        FindOne = True
        Exit For
    End If
Next
If Not FindOne Then Exit Sub

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

For i = 1 To Me.ListView1.ListItems.Count
    
    '-> teste si fichier selectionn�
    If Me.ListView1.ListItems(i).Checked = False Then GoTo NextFichier
    
    '-> Initialisation pour demande
    If Not aConnexion.InitDmd(True) Then
        '-> Imprime une ligne d'execution dans la zone de 'DEBUG' de la fenetre
        MsgBox GetMesseprog("APPLIC", "51"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
        GoTo GestError
    End If
    
    '-> Debut tempo
    Me.StatusBar1.Panels(1).Text = GetMesseprog("CONSOLE", "58") & " " & Me.ListView1.ListItems(i).Text & " " & Me.ListView1.ListItems(i).SubItems(1)
    
    '-> On envoie une demande de recup des fichiers du catalogue
    '-> On lance la fonction d'analyse des locks brul�es
    OrdreOk = aConnexion.PutInstruction("B3D_REINDEX~" & aConnexion.Ident & "�" & Me.ListView1.ListItems(i).Text & "�" & Me.ListView1.ListItems(i).SubItems(2), True)
    
    '-> Verifie si on a bien envoy� l'instruction
    If Not OrdreOk Then
        '-> Positionner le code Erreur
        MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
        GoTo GestError
    End If
    
    '-> On envoie l'ordre de lecture des instructions
    Set aBuffer = New B3DBuffer
    If Not aConnexion.SendDemande(aBuffer) Then
        '-> Positionner le code Erreur
        aConnexion.ErrorNumber = "100000"
        aConnexion.ErrorLibel = "Error sending exchange"
        aConnexion.ErrorParam = "LoadAgentDOG"
        MsgBox aConnexion.ErrorNumber & Chr(13) & aConnexion.ErrorLibel & Chr(13) & aConnexion.ErrorParam, vbExclamation
        GoTo GestError
    End If
        
    '-> Recup de la 1ere ligne
    Ligne = aBuffer.Lignes(1)
    
    If Entry(2, Entry(1, Ligne, "�"), "~") <> "OK" Then
        MsgBox Ligne
        '-> Fin de la tempo
        GoTo NextFichier
    End If
    '-> On renseigne le nb d'enregs extraits
    Me.ListView1.ListItems(i).SubItems(3) = Entry(3, Ligne, "~")
NextFichier:
Next

Me.StatusBar1.Panels(1).Text = GetMesseprog("APPLIC", "92")
DoEvents

GestError:
    Me.Enabled = True
    Screen.MousePointer = 0


End Sub
