Attribute VB_Name = "fctIntCor"
Option Explicit

'-> Variable d'�change avec le B3D
Public aConnexion As B3DConnexion

'-> Collection des buffers associ�s
Public BufferFieldTables As Collection

'-> Indique la table maitresse du programme
Public MasterFname As String

'-> Param�trage de la ligne de commande
Public ParamFile As String
Public ZoneCharge As String

'-> Path des Tables de correspondances
Public IntCorPath As String

'-> Pointeur vers un objet descriptif de la table en cours de travail
Public aDescro As B3DFnameDescriptif


Sub Main()


Dim aField As B3DField

'-> Analyse de la ligne de commande
If Trim(Command$) = "" Then End

'-> Eclater la ligne de commande
ParamFile = Entry(1, Command$, "�")
ZoneCharge = Entry(2, Command$, "�")

'-> V�rifier si on trouve le fichier de param�trage
If Dir$(ParamFile) = "" Then End

'-> Cr�ation de la connexion
Set aConnexion = New B3DConnexion

'-> Charger son param�trage
If Not aConnexion.ReadParamFormFile(ParamFile) Then End

'-> Cr�er le champ des tables de coorespondance
IntCorPath = aConnexion.ApplicationDir & aConnexion.Ident & "\Param\"

'-> Afficher le d�marrage
DisplayFieldIntCor


End Sub

Private Sub DisplayFieldIntCor()

Dim aObj As B3DObject
Dim x As ListItem

On Error GoTo GestError

'-> Charger la feuille en m�moire
frmIntCor.Show

'-> Bloquage
Screen.MousePointer = 11
frmIntCor.Enabled = False

'-> Messprog
frmIntCor.StatusBar1.Panels(1).Text = GetMesseprog("APPLIC", "50")
frmIntCor.StatusBar1.Refresh

'-> R�cup�rer le catalogue de l'ident
GetIdentCatalogue aConnexion, aConnexion.Ident, ""

'-> Afficher la liste des champs de type caract�res
For Each aObj In Catalogues
    '-> Que les champs caract�res
    If Mid$(UCase$(aObj.DataType), 1, 3) = "CHA" Then
        '-> Ajouter une icone
        Set x = frmIntCor.ListView1.ListItems.Add(, , aObj.Num_Field)
        x.SubItems(1) = aObj.Fill_Label
    End If
Next 'Pour tous les champs

'-> Vider le catalogue
frmIntCor.StatusBar1.Panels(1).Text = ""

'-> Formatter le listview
FormatListView frmIntCor.ListView1

GestError:
    frmIntCor.Enabled = True
    Screen.MousePointer = 0

End Sub
