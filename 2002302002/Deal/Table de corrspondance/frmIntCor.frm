VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmIntCor 
   Caption         =   "Form1"
   ClientHeight    =   9375
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10755
   Icon            =   "frmIntCor.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   625
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   717
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   9000
      Width           =   10755
      _ExtentX        =   18971
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   8415
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   14843
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView ListView3 
      Height          =   1215
      Left            =   3720
      TabIndex        =   2
      Top             =   0
      Width           =   6975
      _ExtentX        =   12303
      _ExtentY        =   2143
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView ListView2 
      Height          =   7215
      Left            =   3720
      TabIndex        =   3
      Top             =   1200
      Width           =   6975
      _ExtentX        =   12303
      _ExtentY        =   12726
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   5821
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   5821
      EndProperty
   End
   Begin VB.Menu mnuTablCor 
      Caption         =   "COR"
      Visible         =   0   'False
      Begin VB.Menu mnuAddcor 
         Caption         =   ""
      End
      Begin VB.Menu mnuEditCor 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelCor 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmIntCor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("INTCOR", "1")

'-> Entetes des colonnes pour liste des champs
Me.ListView1.ColumnHeaders(1).Text = GetMesseprog("CONSOLE", "17")
Me.ListView1.ColumnHeaders(2).Text = GetMesseprog("FRMSCHEMA", "4")

'-> Entetes de colonne pour la liste des correspondances
Me.ListView3.ColumnHeaders(1).Text = GetMesseprog("CONSOLE", "17")
Me.ListView3.ColumnHeaders(2).Text = GetMesseprog("FRMSCHEMA", "4")

'-> Entetes de colonne des cor
Me.ListView2.ColumnHeaders(1).Text = GetMesseprog("INTCOR", "2")
Me.ListView2.ColumnHeaders(2).Text = GetMesseprog("INTCOR", "3")

'-> Libell�s des menus
Me.mnuAddcor.Caption = GetMesseprog("MENU", "1")
Me.mnuDelCor.Caption = GetMesseprog("MENU", "2")
Me.mnuEditCor.Caption = GetMesseprog("MENU", "3")

End Sub

Private Sub AddTableCor()

'---> Cette proc�dure ajoute une table de correspondance

Dim Rep As String
Dim X As ListItem
Dim hdlFile As Integer

'-> Demander le nouveau Code
Rep = InputBox(GetMesseprog("APPLIC", "1"), GetMesseprog("INTCOR", "4"), "")
If Trim(Rep) = "" Then Exit Sub

'-> V�rif des char invalides
If ChechInvalidChar(Rep) Then
    MsgBox Replace(GetMesseprog("APPLIC", "3"), "#CODE#", Rep), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    Exit Sub
End If

'-> V�rifier si le code n'est pas d�ja saisi
For Each X In Me.ListView3.ListItems
    If UCase$(Trim(X.Text)) = Trim(UCase$(Rep)) Then
        '-> Le code existe d�ja
        MsgBox GetMesseprog("APPLIC", "2"), vbExclamation + vbOKOnly, GetMesseprog("APPLIC", "4")
        Exit Sub
    End If
Next

'-> Le code existe dans le listview
Set X = Me.ListView3.ListItems.Add(, , Trim(UCase$(Rep)))
X.Selected = True

'-> Vider le listview
Me.ListView2.ListItems.Clear

'-> Saisie de la d�signation
strRetour = ""

'-> Afficher la saisie
frmLangue.Show vbModal

'-> Tester le retour
If strRetour = "" Then Exit Sub

'-> Afficher le libelle
X.Tag = strRetour

'-> Positionner le libelle
X.SubItems(1) = Entry(aConnexion.CodeLangue, X.Tag, "|")

'-> Formatter le listview
FormatListView Me.ListView3

'-> Cr�er le fichier Ascii
hdlFile = FreeFile
Open IntCorPath & Me.ListView1.SelectedItem.Text & "_" & X.Text & ".intcor" For Output As #hdlFile

'-> Fermer le fichier
Close #hdlFile



End Sub

Private Sub Form_Resize()

On Error GoTo GestError

Dim aRect As RECT

GetClientRect Me.hwnd, aRect

'-> ListView
Me.ListView1.Height = aRect.Bottom - Me.StatusBar1.Height
Me.ListView3.Width = aRect.Right - Me.ListView3.Left
Me.ListView2.Width = aRect.Right - Me.ListView2.Left
Me.ListView2.Height = aRect.Bottom - Me.ListView2.Top

'-> Taille du status
Me.StatusBar1.Panels(1).Width = Me.ScaleX(aRect.Right, 3, 1)

GestError:

End Sub


Private Sub ListView1_ItemClick(ByVal Item As MSComctlLib.ListItem)

'---> Afficher la liste des tables de correspondances pour ce champ

Dim FileName As String
Dim X As ListItem

'-> Vider la liste des tables de cor associ�e � ce champ
Me.ListView3.ListItems.Clear
Me.ListView2.ListItems.Clear

'-> Analyse du r�pertoire
FileName = Dir$(IntCorPath & "*.intcor")
Do While FileName <> ""
    '-> Ajouter une icone
    If Entry(1, Entry(1, FileName, "."), "_") = Me.ListView1.SelectedItem.Text Then
        Set X = Me.ListView3.ListItems.Add(, , Entry(2, Entry(1, FileName, "."), "_"))
    End If
    
    '-> Ouvrir ici pour get du libelle du champ
    FileName = Dir
Loop 'Pour tous les fichiers


End Sub

Private Sub ListView2_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)

'-> Quitter si pas de bouton droit
If Button <> vbRightButton Then Exit Sub

'-> Ne rien faire si pas de table
If Me.ListView3.SelectedItem Is Nothing Then Exit Sub

'-> Gestion du menu
If Me.ListView2.SelectedItem Is Nothing Then
    Me.mnuDelCor.Enabled = False
    Me.mnuEditCor.Enabled = False
Else
    Me.mnuDelCor.Enabled = True
    Me.mnuEditCor.Enabled = True
End If
'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu contextuel
Me.PopupMenu Me.mnuTablCor

'-> Selon le retour menu
Select Case strRetour
    Case "ADD"
        '-> Ajouter une table de correspondance
        Call AddTableCor
    Case "DEL"
    Case "PROP"
End Select

End Sub

Private Sub ListView3_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)

'-> Quitter si pas de bouton droit
If Button <> vbRightButton Then Exit Sub

'-> Ne rien faire si pas de champ s�lmectionn�
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Gestion du menu
If Me.ListView3.SelectedItem Is Nothing Then
    Me.mnuDelCor.Enabled = False
    Me.mnuEditCor.Enabled = False
Else
    Me.mnuDelCor.Enabled = True
    Me.mnuEditCor.Enabled = True
End If
'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu contextuel
Me.PopupMenu Me.mnuTablCor

'-> Selon le retour menu
Select Case strRetour
    Case "ADD"
        '-> Ajouter une table de correspondance
        Call AddTableCor
    Case "DEL"
    Case "PROP"
End Select

End Sub

Private Sub mnuAddcor_Click()
strRetour = "ADD"
End Sub

Private Sub mnuDelCor_Click()
strRetour = "DEL"
End Sub

Private Sub mnuEditCor_Click()
strRetour = "PROP"
End Sub
