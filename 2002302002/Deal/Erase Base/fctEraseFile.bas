Attribute VB_Name = "fctEraseFile"
'-> Variable d'�change avec le B3D
Public aConnexion As B3DConnexion

'-> Param�trage de la ligne de commande
Public ParamFile As String
Public ZoneCharge As String

'-> Liste des tables
Public Fnames As Collection

Private Sub Main()

'---> Point d'entr�e du programme

Dim i As Integer
Dim X As ListItem

On Error GoTo GestError

'-> Analyse de la ligne de commande
If Trim(Command$) = "" Then End

'-> Eclater la ligne de commande
ParamFile = Entry(1, Command$, "�")
ZoneCharge = Entry(2, Command$, "�")

'-> V�rifier si on trouve le fichier de param�trage
If Dir$(ParamFile) = "" Then End

'-> Ecran
Screen.MousePointer = 11

'-> Cr�ation de la connexion
Set aConnexion = New B3DConnexion

'-> Charger son param�trage
If Not aConnexion.ReadParamFormFile(ParamFile) Then GoTo GestError

'-> R�cup�rer la liste des tables affect�es � cet Ident
If Not GetIdentSchemaList(aConnexion, aConnexion.Ident, 1, Fnames) Then
    MsgBox aConnexion.ErrorNumber & " - " & aConnexion.ErrorLibel, vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

'-> Charger la feuille de gestion des tables
frmEraseFile.Show

'-> Bloquer la feuille
frmEraseFile.Enabled = False

'-> Messprog
frmEraseFile.StatusBar1.Panels(1).Text = GetMesseprog("REQUETEUR", "28")

'-> Bloquer l'�cran
LockWindowUpdate frmEraseFile.ListView1.hwnd
DoEvents

'-> Charger la liste des tables
For i = 1 To Fnames.Count
    '-> Ajouter un objet
    Set X = frmEraseFile.ListView1.ListItems.Add(, , Entry(1, Fnames(i), "�"))
    X.SubItems(1) = Entry(2, Fnames(i), "�")
Next

'-> Formatter le listView
FormatListView frmEraseFile.ListView1

'-> Text
frmEraseFile.StatusBar1.Panels(1).Text = ""

'-> Repositionner le pointeur de souris
Screen.MousePointer = 0

'-> D�bloquer la mise � jour de l'�cran
LockWindowUpdate 0

'-> D�bloquer la feuille
frmEraseFile.Enabled = True

'-> Quitter la proc�dure
Exit Sub


GestError:
    '-> Repositionner le pointeur de l'�cran
    Screen.MousePointer = 0
    End

End Sub
