VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmEraseFile 
   Caption         =   "Form1"
   ClientHeight    =   6255
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5820
   Icon            =   "frmEraseFile.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   417
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   388
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   1
      Top             =   5880
      Width           =   5820
      _ExtentX        =   10266
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3255
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   5741
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   38100
      EndProperty
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "POPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuSelectAll 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDeleteBase 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmEraseFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("ERASE_FILE", "2")

'-> Titre des colonnes
Me.ListView1.ColumnHeaders(1).Text = GetMesseprog("CONSOLE", "17")
Me.ListView1.ColumnHeaders(2).Text = GetMesseprog("FRMSCHEMA", "4")

'-> Libell�s des menus
Me.mnuSelectAll.Caption = GetMesseprog("APPLIC", "60")
Me.mnuDeleteBase.Caption = GetMesseprog("ERASE_FILE", "5")


End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next
GetClientRect Me.hwnd, aRect
Me.ListView1.Left = 0
Me.ListView1.Top = 0
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom - Me.StatusBar1.Height

Me.StatusBar1.Panels(1).Width = Me.ScaleX(aRect.Right, 3, 1)

End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

'-> Bouton droit
If Button <> vbRightButton Then Exit Sub

'-> Qu'il y ait au moins une table dans le descro
If Me.ListView1.ListItems.Count = 0 Then Exit Sub

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu Me.mnuPopup

'-> Traiter le retour
Select Case strRetour
    Case "ALL"
        Call SelectAll
    Case "DELETE"
        Call EraseFile
End Select

'-> Vider la variable d'�change
strRetour = ""

End Sub

Private Sub EraseFile()

Dim aBuffer As B3DBuffer
Dim i As Long
Dim Ligne As String
Dim ListeFile As String
Dim Rep As VbMsgBoxResult
Dim Instruction As String

On Error GoTo GestError

'-> Cr�er la liste des tables � butter
For i = 1 To Me.ListView1.ListItems.Count
    If Me.ListView1.ListItems(i).Checked Then
        If ListeFile = "" Then
            ListeFile = Me.ListView1.ListItems(i).Text
        Else
            ListeFile = ListeFile & "|" & Me.ListView1.ListItems(i).Text
        End If
    End If
Next

'-> Quitter si pas de table s�lectionn�e
If Trim(ListeFile) = "" Then Exit Sub

'-> Demander confirmation
Rep = MsgBox(GetMesseprog("ERASE_FILE", "5"), vbExclamation + vbYesNo, GetMesseprog("APPLIC", "21"))
If Rep = vbNo Then Exit Sub

'-> Positionner le messprog
Me.StatusBar1.Panels(1).Text = GetMesseprog("ERASE_FILE", "3")
Screen.MousePointer = 11
Me.Enabled = False
DoEvents

'-> Initialiser une demande
If Not aConnexion.InitDmd Then
    MsgBox "Erase File : 100001 -> Error cr�ating demand", vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

'-> Demander un descriptif
Instruction = "B3D_ERASE_FILE~" & aConnexion.Ident & "�" & ListeFile
If Not aConnexion.PutInstruction(Instruction) Then
    MsgBox "Erase File : 100002 -> Error creating instruction", vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

'-> Initialiser un buffer de retour
Set aBuffer = New B3DBuffer

'-> Envoyer la demande
If Not aConnexion.SendDemande(aBuffer) Then
    MsgBox "Erase File : 100000 -> Error sending exchange", vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

'-> Lire la r�ponse
If Entry(2, aBuffer.Lignes(1), "~") <> "OK" Then
    MsgBox GetMesseprog("ERASE_FILE", "6") & Chr(13) & Entry(3, Ligne, "~") & " - " & Entry(4, Ligne, "~"), vbCritical + vbOKOnly, GetMesseprog("APPLIC", "4")
    GoTo GestError
End If

'-> Afficher le message de fin
Me.StatusBar1.Panels(1).Text = GetMesseprog("ERASE_FILE", "4")

GestError:
    Screen.MousePointer = 0
    Me.Enabled = True


End Sub

Private Sub SelectAll()

Dim x As ListItem

'-> S�lectionner tous les items
For Each x In Me.ListView1.ListItems
    x.Checked = True
Next

End Sub

Private Sub mnuDeleteBase_Click()
strRetour = "DELETE"
End Sub

Private Sub mnuSelectAll_Click()
strRetour = "ALL"
End Sub
