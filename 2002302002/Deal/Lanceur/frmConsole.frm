VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.MDIForm mdiConsole 
   BackColor       =   &H8000000C&
   Caption         =   "Deal Informatique"
   ClientHeight    =   7125
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9465
   Icon            =   "frmConsole.frx":0000
   LinkTopic       =   "MDIForm1"
   Picture         =   "frmConsole.frx":0CCA
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   4
      Top             =   6750
      Width           =   9465
      _ExtentX        =   16695
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13626
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            TextSave        =   "25/03/2002"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picNaviga 
      Align           =   3  'Align Left
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   6750
      Left            =   0
      ScaleHeight     =   450
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   272
      TabIndex        =   1
      Top             =   0
      Width           =   4080
      Begin VB.PictureBox picTitre 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         Height          =   90
         Left            =   360
         ScaleHeight     =   6
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   128
         TabIndex        =   3
         Top             =   240
         Width           =   1920
      End
      Begin MSComctlLib.ImageList INT 
         Left            =   1440
         Top             =   4080
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   20
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":9548
               Key             =   "CLOSE"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":98E2
               Key             =   "OPEN"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":9C7C
               Key             =   "STOPSESSION"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":A016
               Key             =   "REPLICA"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":A3B0
               Key             =   "RUNSESSION"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":A74A
               Key             =   "REPLICAIN"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":ADDC
               Key             =   "REPLICAOUT"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":B46E
               Key             =   "PORTAILB3D"
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":B808
               Key             =   "PARAM"
            EndProperty
            BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":BBA2
               Key             =   "INT"
            EndProperty
            BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":BF3C
               Key             =   "INTIN"
            EndProperty
            BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":C2D6
               Key             =   "INTOUT"
            EndProperty
            BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":C670
               Key             =   "REINDEX"
            EndProperty
            BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":CA0A
               Key             =   "GESTLOCK"
            EndProperty
            BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":CDA4
               Key             =   "SAVE"
            EndProperty
            BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":CEFE
               Key             =   "OUTIL"
            EndProperty
            BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":D058
               Key             =   "BOOKCLOSE"
            EndProperty
            BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":D3F2
               Key             =   "BOOKOPEN"
            EndProperty
            BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":D78C
               Key             =   "PROGRAMME"
            EndProperty
            BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConsole.frx":DB26
               Key             =   "IDENT"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.TreeView TreeNaviga 
         Height          =   2295
         Left            =   0
         TabIndex        =   2
         Top             =   600
         Width           =   3855
         _ExtentX        =   6800
         _ExtentY        =   4048
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         ImageList       =   "INT"
         Appearance      =   1
      End
      Begin VB.Image imgClose 
         Height          =   165
         Left            =   2040
         Picture         =   "frmConsole.frx":DEC0
         Top             =   120
         Width           =   165
      End
   End
   Begin VB.PictureBox picSplit 
      Align           =   3  'Align Left
      BorderStyle     =   0  'None
      Height          =   6750
      Left            =   4080
      MousePointer    =   9  'Size W E
      ScaleHeight     =   6750
      ScaleWidth      =   105
      TabIndex        =   0
      Top             =   0
      Width           =   105
   End
End
Attribute VB_Name = "mdiConsole"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)

Dim Rep As VbMsgBoxResult
Dim aBase As clsBase
Dim aIdent As clsIdent

'-> Demander confirmation
Rep = MsgBox(GetMesseprog("MESSAGE", "1"), vbQuestion + vbYesNo, "Deal Informatique")
If Rep = vbNo Then
    Cancel = 1
    Exit Sub
End If

'-> Si on est mode console, arreter la session
If ProductVersion = "MONOPOSTE" Then
    '-> Message dans la barre d'�tat
    mdiConsole.StatusBar1.Panels(1).Text = GetMesseprog("MESSAGE", "41")
    DoEvents
    '-> Tentative d'arret de la session
    If Not StopSession Then
        mdiConsole.StatusBar1.Panels(1).Text = ""
        Cancel = 1
        Exit Sub
    End If
End If
        
'-> V�rifier qu'il ne reste pas un agent IDX actif
For Each aBase In Bases
    If aBase.Idx.Count <> 0 Then
        If ProductVersion = "MONOPOSTE" Then
            MsgBox GetMesseprog("MESSAGE", "3"), vbCritical + vbOKOnly, "Deal Informatique"
        Else
            MsgBox GetMesseprog("MESSAGE", "2") & Chr(13) & aBase.Nom_Base, vbCritical + vbOKOnly, "Deal Informatique"
        End If
        Cancel = 1
        Exit Sub
    End If 'S'il reste des agents IDX en cours
Next 'Pour toutes les bases

'-> V�rifier s'il reste des connexions
If Connexions.Count <> 0 Then
    MsgBox GetMesseprog("MESSAGE", "4"), vbCritical + vbOKOnly, "Deal Informatique"
    Cancel = 1
    Exit Sub
End If

'-> V�rifier s'il reste des serveurs DOG Actifs
For Each aIdent In Idents
    '-> V�rifier qu'il soient tous ferm�s
    If aIdent.IsRunning Then
        MsgBox GetMesseprog("MESSAGE", "5"), vbCritical + vbOKOnly, "Deal Informatique"
        Cancel = 1
        Exit Sub
    End If
Next 'Pour tous les idents

'-> Fin de la session
End

End Sub

Private Sub picNaviga_Resize()

Dim aRect As RECT
Dim res As Long

On Error Resume Next

'-> R�cup�rer la taille de la zone cliente
res = GetClientRect(Me.picNaviga.hwnd, aRect)

'-> Positionner l'entete de la fen�tre
Me.picTitre.Left = 3
Me.picTitre.Top = 7
Me.picTitre.Width = aRect.Right - 6 - imgClose.Width
Me.imgClose.Left = aRect.Right - imgClose.Width
Me.imgClose.Top = Me.picTitre.Top - 2

'-> Positionner le treeview
Me.TreeNaviga.Left = Me.picTitre.Left
Me.TreeNaviga.Top = Me.picTitre.Top * 2 + Me.picTitre.Height
Me.TreeNaviga.Width = Me.picTitre.Width + Me.imgClose.Width + 6
Me.TreeNaviga.Height = Me.picNaviga.ScaleY(Me.picNaviga.Height, 1, 3) - 21
End Sub
Private Sub picTitre_Resize()

'---> Dessiner la barre de titre
Me.picTitre.Line (0, 0)-(Me.picTitre.Width - 1, 0), RGB(255, 255, 255)
Me.picTitre.Line (0, 1)-(Me.picTitre.Width - 1, 1), RGB(255, 255, 255)
Me.picTitre.Line (1, 1)-(Me.picTitre.Width - 1, 1), &HE0E0E0
Me.picTitre.Line (0, 3)-(Me.picTitre.Width - 1, 3), RGB(255, 255, 255)
Me.picTitre.Line (0, 4)-(Me.picTitre.Width - 1, 4), RGB(255, 255, 255)
Me.picTitre.Line (1, 4)-(Me.picTitre.Width - 1, 4), &HE0E0E0

End Sub
Private Sub picSplit_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)

Dim aPt As POINTAPI
Dim res As Long

If Button = vbLeftButton Then
    '-> R�cup�rer la position du curseur
    res = GetCursorPos(aPt)
    '-> Convertir en coordonn�es clientes
    res = ScreenToClient(Me.hwnd, aPt)
    '-> Tester les positions mini et maxi
    If aPt.X > Me.picNaviga.ScaleX(2, 7, 3) And aPt.X < Me.picNaviga.ScaleX(Me.Width, 1, 3) - Me.picNaviga.ScaleX(2, 7, 3) Then Me.picNaviga.Width = Me.picNaviga.ScaleX(aPt.X, 3, 1)
End If

End Sub


Private Sub TreeNaviga_DblClick()

'---> Gestion des options


'-> Se barrer si pas de node s�lectionn�
If Me.TreeNaviga.SelectedItem Is Nothing Then Exit Sub

'-> Lancer les programmes applicatifs
If Me.TreeNaviga.SelectedItem.Image = "PROGRAMME" Then
    '-> Si on est en version Monoposte : V�rifier que la session est en cours
    If ProductVersion = "MONOPOSTE" Then
        '-> Faire un run du programme
        RunApp Me.TreeNaviga.SelectedItem.Tag, Connexions(1)
        '-> Quitter
        Exit Sub
    Else
        '-> On est en version serveur Afficher le choix
        strRetour = ""
        frmChxAgent.Show vbModal
        If strRetour = "" Then Exit Sub
        '-> Faire un run du programme
        RunApp Me.TreeNaviga.SelectedItem.Tag, Connexions(strRetour)
        strRetour = ""
        '-> Quitter
        Exit Sub
        
    End If
End If 'Si c'est un programme

'-> Selon la cl�
Select Case Me.TreeNaviga.SelectedItem.Tag
    Case "MODIFPASSWORD" 'Mot de passe en version monoposte
        ModifPass
    Case "IDENT" 'Param�trage d'un ident
        DisplayIdent Me.TreeNaviga.SelectedItem.Key
    Case "PORTAILB3D" 'Acc�s ua portail B3D
        frmPortailB3D.Show
    Case "REPLICAIN" 'Import de r�plication
    
    Case "REPLICAOUT" 'Export pour r�plication
    
    Case "REPLICAGEST" 'Gestionnaire de r�plication
    
    Case "INTIN" 'Import via un interface
    
    Case "INTOUT" 'Export via un interface
    
    Case "GESTDOG" 'Gestionnaire DOG
    
    Case "GESTBASE" 'Propri�t�s de la base
        
    Case "SAVEBASE" 'Sauvegarde de la base
                
End Select


End Sub
Private Sub ModifPass()

'-> vider la variable de retour
strRetour = ""

'-> Afficher la feuille
frmPassWord.Show vbModal

'-> Tester le retour
If strRetour <> "PASSOK" Then
    strRetour = ""
    Exit Sub
End If

'-> Initialisation
Load frmNewOperat
frmNewOperat.Init OpeConsole
frmNewOperat.ModeOpen = 2

'-> Afficher la feuille
frmNewOperat.Show vbModal

End Sub

Private Sub DisplayIdent(IdentKey As String)

'---> Cette proc�dure affiche une page de propri�t� d'un identifiant

Dim aFrm As frmGestIdent
Dim aIdent As clsIdent

'-> Pointer sur l'ident associ�
Set aIdent = Idents(IdentKey)

'-> V�rifier si la page de propri�t� est ouverte
If aIdent.HdlForm <> 0 Then
    '-> V�rifier que la feuille existe encore
    If IsWindow(aIdent.HdlForm) <> 0 Then
        '-> La feuille existe encore : l'activer
        SendMessage aIdent.HdlForm, WM_CHILDACTIVATE, 0, 0
        Exit Sub
    End If
End If

'-> Charger une nouvelle instance de la feuille
Set aFrm = New frmGestIdent

'-> Lancer son initialisation
aFrm.Init (IdentKey)

'-> Mettre � jo�ur son handle
aIdent.HdlForm = aFrm.hwnd

End Sub
