VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmGestIdent 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   5190
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8670
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5190
   ScaleWidth      =   8670
   Begin VB.PictureBox Picture3 
      Height          =   495
      Left            =   360
      ScaleHeight     =   435
      ScaleWidth      =   435
      TabIndex        =   23
      Top             =   5640
      Width           =   495
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5175
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   9128
      _Version        =   393216
      Style           =   1
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "frmGestIdent.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Timer1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame3"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "picError"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "frmGestIdent.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Frame4"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Frame5"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Tab 2"
      TabPicture(2)   =   "frmGestIdent.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame6"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).ControlCount=   1
      Begin VB.Frame Frame6 
         Caption         =   "Frame6"
         Height          =   2175
         Left            =   -74880
         TabIndex        =   39
         Top             =   480
         Width           =   6255
         Begin VB.CommandButton Command2 
            Caption         =   "Command2"
            Height          =   735
            Left            =   4440
            TabIndex        =   44
            Top             =   1200
            Width           =   1575
         End
         Begin VB.Label Label27 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Label16"
            Height          =   255
            Left            =   4440
            TabIndex        =   43
            Top             =   840
            Width           =   1575
         End
         Begin VB.Label Label26 
            Caption         =   "Label15"
            Height          =   255
            Left            =   240
            TabIndex        =   42
            Top             =   840
            Width           =   3855
         End
         Begin VB.Label Label16 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Label16"
            Height          =   255
            Left            =   4440
            TabIndex        =   41
            Top             =   480
            Width           =   1575
         End
         Begin VB.Label Label15 
            Caption         =   "Label15"
            Height          =   255
            Left            =   240
            TabIndex        =   40
            Top             =   480
            Width           =   3855
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Frame5"
         Height          =   1455
         Left            =   120
         TabIndex        =   33
         Top             =   3600
         Width           =   8415
         Begin MSComctlLib.ImageList ImageList1 
            Left            =   5760
            Top             =   600
            _ExtentX        =   1005
            _ExtentY        =   1005
            BackColor       =   -2147483643
            ImageWidth      =   32
            ImageHeight     =   32
            MaskColor       =   12632256
            _Version        =   393216
            BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
               NumListImages   =   1
               BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmGestIdent.frx":0054
                  Key             =   "IDX"
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.ListView ListView1 
            Height          =   1095
            Left            =   3120
            TabIndex        =   36
            Top             =   240
            Width           =   5175
            _ExtentX        =   9128
            _ExtentY        =   1931
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            _Version        =   393217
            Icons           =   "ImageList1"
            SmallIcons      =   "ImageList1"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin VB.Label Label25 
            Caption         =   "Label22"
            Height          =   255
            Left            =   120
            TabIndex        =   38
            Top             =   960
            Width           =   1935
         End
         Begin VB.Label Label24 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   2280
            TabIndex        =   37
            Top             =   960
            Width           =   615
         End
         Begin VB.Label Label23 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   2280
            TabIndex        =   35
            Top             =   480
            Width           =   615
         End
         Begin VB.Label Label22 
            Caption         =   "Label22"
            Height          =   255
            Left            =   120
            TabIndex        =   34
            Top             =   480
            Width           =   1935
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Base de donn�es : "
         Height          =   3015
         Left            =   120
         TabIndex        =   24
         Top             =   480
         Width           =   8415
         Begin VB.Label Label21 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   240
            TabIndex        =   32
            Top             =   2640
            Width           =   4095
         End
         Begin VB.Label Label20 
            Caption         =   "Etat de la base : "
            Height          =   255
            Left            =   240
            TabIndex        =   31
            Top             =   2280
            Width           =   2775
         End
         Begin VB.Label Label19 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   3360
            TabIndex        =   30
            Top             =   720
            Width           =   4455
         End
         Begin VB.Label Label18 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   240
            TabIndex        =   29
            Top             =   720
            Width           =   2895
         End
         Begin VB.Label Label17 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   735
            Left            =   240
            TabIndex        =   28
            Top             =   1440
            Width           =   7575
         End
         Begin VB.Label Label13 
            Caption         =   "Fichier : "
            Height          =   255
            Left            =   240
            TabIndex        =   27
            Top             =   1080
            Width           =   2415
         End
         Begin VB.Label Label12 
            Caption         =   "Type de base : "
            Height          =   255
            Left            =   3360
            TabIndex        =   26
            Top             =   360
            Width           =   3255
         End
         Begin VB.Label Label11 
            Caption         =   "Cl� d'acc�s : "
            Height          =   255
            Left            =   240
            TabIndex        =   25
            Top             =   360
            Width           =   2295
         End
      End
      Begin VB.PictureBox picError 
         Height          =   4575
         Left            =   -74880
         ScaleHeight     =   4515
         ScaleWidth      =   8355
         TabIndex        =   20
         Top             =   480
         Visible         =   0   'False
         Width           =   8415
         Begin VB.Label Label10 
            Height          =   2655
            Left            =   360
            TabIndex        =   22
            Top             =   1080
            Width           =   7815
         End
         Begin VB.Label Label9 
            Height          =   735
            Left            =   1320
            TabIndex        =   21
            Top             =   360
            Width           =   6615
         End
         Begin VB.Image Image2 
            Height          =   720
            Left            =   360
            Picture         =   "frmGestIdent.frx":0D2E
            Top             =   240
            Width           =   720
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Frame3"
         Height          =   1215
         Left            =   -74880
         TabIndex        =   12
         Top             =   3840
         Width           =   8415
         Begin VB.PictureBox Picture2 
            AutoRedraw      =   -1  'True
            Height          =   255
            Left            =   4800
            ScaleHeight     =   195
            ScaleWidth      =   3435
            TabIndex        =   18
            Top             =   720
            Width           =   3495
         End
         Begin VB.TextBox Text3 
            Enabled         =   0   'False
            Height          =   285
            Left            =   3840
            TabIndex        =   16
            Top             =   720
            Width           =   735
         End
         Begin VB.TextBox Text2 
            Enabled         =   0   'False
            Height          =   285
            Left            =   3840
            TabIndex        =   14
            Top             =   360
            Width           =   735
         End
         Begin VB.Label Label8 
            Height          =   255
            Left            =   7200
            TabIndex        =   19
            Top             =   360
            Width           =   975
         End
         Begin VB.Label Label7 
            Caption         =   "Label7"
            Height          =   255
            Left            =   4800
            TabIndex        =   17
            Top             =   360
            Width           =   2295
         End
         Begin VB.Label Label6 
            Caption         =   "Label5"
            Height          =   255
            Left            =   240
            TabIndex        =   15
            Top             =   720
            Width           =   3495
         End
         Begin VB.Label Label5 
            Caption         =   "Label5"
            Height          =   255
            Left            =   240
            TabIndex        =   13
            Top             =   360
            Width           =   3495
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   10
         Left            =   -66960
         Top             =   2640
      End
      Begin VB.Frame Frame2 
         Caption         =   "Frame1"
         Height          =   1335
         Left            =   -74880
         TabIndex        =   8
         Top             =   2400
         Width           =   8415
         Begin VB.PictureBox Picture1 
            AutoRedraw      =   -1  'True
            Height          =   375
            Left            =   240
            ScaleHeight     =   315
            ScaleWidth      =   6555
            TabIndex        =   10
            Top             =   720
            Width           =   6615
         End
         Begin VB.CommandButton Command1 
            Caption         =   "Command1"
            Height          =   375
            Left            =   7080
            TabIndex        =   9
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Label1"
            Height          =   255
            Left            =   240
            TabIndex        =   11
            Top             =   360
            Width           =   3615
         End
      End
      Begin VB.Frame Frame1 
         Height          =   1815
         Left            =   -74880
         TabIndex        =   1
         Top             =   480
         Width           =   8415
         Begin VB.TextBox Text1 
            Height          =   285
            Index           =   2
            Left            =   2160
            TabIndex        =   7
            Top             =   960
            Width           =   5535
         End
         Begin VB.TextBox Text1 
            Height          =   285
            Index           =   1
            Left            =   2160
            TabIndex        =   6
            Top             =   600
            Width           =   5535
         End
         Begin VB.TextBox Text1 
            Height          =   285
            Index           =   0
            Left            =   2160
            TabIndex        =   5
            Top             =   240
            Width           =   5535
         End
         Begin VB.Image Image1 
            Height          =   720
            Left            =   240
            Picture         =   "frmGestIdent.frx":1BF8
            Top             =   480
            Width           =   720
         End
         Begin VB.Label Label4 
            Height          =   255
            Left            =   1200
            TabIndex        =   4
            Top             =   960
            Width           =   735
         End
         Begin VB.Label Label3 
            Height          =   255
            Left            =   1200
            TabIndex        =   3
            Top             =   600
            Width           =   735
         End
         Begin VB.Label Label2 
            Height          =   255
            Left            =   1200
            TabIndex        =   2
            Top             =   240
            Width           =   855
         End
      End
   End
End
Attribute VB_Name = "frmGestIdent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim aIdent As clsIdent
Dim aBase As clsBase
Dim TailleLue As Long
Dim TailleTotale As Long
Dim IdentKey As String

Private Sub Command1_Click()

'---> Lancer ou arreter le serveur dog

Dim Rep As VbMsgBoxResult
Dim aBase As clsBase
Dim aIdx As clsIdx
Dim i As Integer
Dim X As ListItem

Me.Enabled = True
Screen.MousePointer = 11

If aIdent.IsRunning Then
    '-> Demande d'arret du serveur
    Rep = MsgBox(GetMesseprog("GESTIDENT", "15"), vbQuestion + vbYesNo, GetMesseprog("COMMUN", "12"))
    If Rep = vbNo Then GoTo EndSub
    
    '-> V�rifier s'il y a des connexions associ�es � cet ident
    If aIdent.NbConnect <> 0 Then
        MsgBox GetMesseprog("MESSAGE", "23"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "9")
        GoTo EndSub
    End If
    
    '-> Indiquer que le serveur est en mode arret
    aIdent.IsEnding = True
    
    '-> Pointer sur la base
    Set aBase = Bases(aIdent.Base)
    
    '-> V�rifier qu'il n'y ait pas d'IDX en cours de travail
    For Each aIdx In aBase.Idx
        '-> Tester qu'il n'y en ait pas en cours de travail
        If aIdx.IsWorking Then
            MsgBox GetMesseprog("MESSAGE", "24"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "9")
            GoTo EndSub
        End If
    Next
    
    '-> Arreter les idents
    For Each aIdx In aBase.Idx
        '-> Envoyer un message de demande de fin de traitement
        UnloadIDX aIdx, aBase
    Next
    
    '-> Indiquer que la base ne tourne plus
    aBase.IsRunning = False
    aBase.When = Now
    
    '-> Mettre � jour l'affichage
    RunBase aBase
    
    '-> Supprimer les objets du ListView
    Do While Me.ListView1.ListItems.Count <> 0
        Me.ListView1.ListItems.Remove (1)
    Loop
    
    '-> R�cup�rer le nombre des tables
    aIdent.Fname_Count = aIdent.ServeurDOG.FnameCount
    aIdent.Emilie_Count = aIdent.ServeurDOG.CatalogueCount
    
    '-> Arreter le serveur DOG
    StopDOG aIdent
    
    '-> Mettre � jour l'affichage
    Me.Label1.Caption = GetMesseprog("PORTAILB3D", "6") & aIdent.When
    Me.Command1.Caption = GetMesseprog("COMMUN", "8")
    
    '-> Arreter l'animation
    Me.Timer1.Enabled = False
    
    '-> Indiquer que le serveur est en mode arret
    aIdent.IsEnding = False
    
    '-> Bloquer les onglets
    Me.SSTab1.TabEnabled(2) = False
        
Else
    '-> Demande de d�marrage
    Rep = MsgBox(GetMesseprog("GESTIDENT", "14"), vbQuestion + vbYesNo, GetMesseprog("COMMUN", "12"))
    If Rep = vbNo Then GoTo EndSub
    
    '-> Lancer la connexion vers le serveur DOG
    If Not InitServeurDOG(aIdent) Then
        MsgBox GetMesseprog("MESSAGE", "22"), vbCritical + vbOKOnly, GetMesseprog("COMMUN", "1")
        GoTo EndSub
    End If
                            
    '-> Pointer sur la base
    Set aBase = Bases(aIdent.Base)
    
    '-> Pour tous les IDX
    For i = 1 To aBase.Nb_Idx
        '-> lancer les IDX
        IsIDX aBase, False, aIdent
    Next
                                
    '-> Indiquer que la base est lanc�e
    aBase.IsRunning = True
    aBase.When = Now
    
    '-> Mettre � jour l'affichage
    RunBase aBase
    
    '-> Ajouter les objets du listView
    AddIdxList aBase
                                
    '-> Indiquer que le serveur est actif
    aIdent.IsRunning = True
    '-> R�cup�rer sa date
    aIdent.When = Now
                
    '-> Run de l'animation
    RunAnim
    
    '-> Mise � jour des libell�s
    Me.Label1.Caption = GetMesseprog("PORTAILB3D", "5") & aIdent.When
    Me.Command1.Caption = GetMesseprog("COMMUN", "9")
    
    '-> D�bloquer les onglets
    Me.SSTab1.TabEnabled(2) = True
End If

EndSub:

    Me.Enabled = True
    Screen.MousePointer = 0

End Sub

Private Sub Command2_Click()

Load frmDescriptif
frmDescriptif.IdentKey = IdentKey
frmDescriptif.Init
frmDescriptif.Show vbModal

End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = aIdent.Code & " - " & aIdent.Libel

'-> Frame des fichiers
Me.Frame1.Caption = GetMesseprog("COMMUN", "14")
Me.Label2.Caption = GetMesseprog("GESTIDENT", "4")
Me.Label3.Caption = GetMesseprog("GESTIDENT", "5")
Me.Label4.Caption = GetMesseprog("GESTIDENT", "6")


End Sub

Public Sub RunAnim()

'-> Nettoyer
Me.Picture1.Cls
'-> Donner la borne maxi
TailleTotale = 100
'-> Go
Me.Timer1.Enabled = True

End Sub

Public Sub Init(Ident As String)

'---> Charger les param�tres de l'ident sp�cifi�

'-> Pointer sur l'ident sp�cifi�
IdentKey = Ident
Set aIdent = Idents(Ident)

'-> Setting de l'ident de communication
aIdent.hdlCom = Me.Picture3.hwnd

'-> Libell� de l'onglet
Me.SSTab1.TabCaption(0) = aIdent.Libel

'-> Charger ses propri�t�s de fichier
Me.Text1(0).Text = aIdent.FichierCatalogue
Me.Text1(1).Text = aIdent.FichierSchema
Me.Text1(2).Text = aIdent.FichierLink

'-> Activit�
Me.Frame3.Caption = GetMesseprog("GESTIDENT", "10")
Me.Label5.Caption = GetMesseprog("GESTIDENT", "11")
Me.Label6.Caption = GetMesseprog("GESTIDENT", "12")
Me.Label7.Caption = GetMesseprog("GESTIDENT", "13")

'-> Etat du serveur
Me.Frame2.Caption = GetMesseprog("GESTIDENT", "9")

If aIdent.IsRunning Then
    '-> Label
    Me.Label1.Caption = GetMesseprog("PORTAILB3D", "5") & aIdent.When
    '-> Bouton
    Me.Command1.Caption = GetMesseprog("COMMUN", "9")
    '-> lancer l'animation
    RunAnim
Else
    '-> Bloquer les onglets
    Me.SSTab1.Tab = 0
    Me.SSTab1.TabEnabled(2) = False
    '-> Label
    Me.Label1.Caption = GetMesseprog("PORTAILB3D", "6") & aIdent.When
    '-> Bouton
    Me.Command1.Caption = GetMesseprog("COMMUN", "8")
    '-> V�rifier la pr�sence des fichiers
    If Not CheckIdent(aIdent) Then Exit Sub
End If

'-> Nombre de connexion
Me.Text2.Text = aIdent.Limite_AgentB3D
Me.Text3.Text = aIdent.NbConnect

'-> Dessin de l'activit�
DrawActivity

'-> Onglet de la base
Me.SSTab1.TabCaption(1) = GetMesseprog("BASE", "1")

'-> Frame de la base
Me.Frame4.Caption = GetMesseprog("BASE", "2")
Me.Label11.Caption = GetMesseprog("BASE", "3")
Me.Label12.Caption = GetMesseprog("BASE", "4")
Me.Label13.Caption = GetMesseprog("BASE", "5")
Me.Label20.Caption = GetMesseprog("BASE", "7")

'-> Constructeur d'index
Me.Frame5.Caption = GetMesseprog("BASE", "8")
Me.Label22.Caption = GetMesseprog("BASE", "9")
Me.Label25.Caption = GetMesseprog("BASE", "12")

'-> Pointer sur la base sp�cifi�e
Set aBase = Bases(aIdent.Base)

'-> Mise � jour des donn�es
Me.Label18.Caption = aIdent.Base

'-> Type de base
Me.Label19.Caption = aBase.Type_Base

'-> Fichier Base
Me.Label17.Caption = aBase.Fichier

'-> Nombre d'Idx maxi
Me.Label23.Caption = aBase.Nb_Idx

'-> Nombre d'idx en cours
Me.Label24.Caption = aBase.Idx.Count

'-> Ajouter les icones dans le listview
If aBase.IsRunning Then AddIdxList aBase

'-> Acc�s au descriptif de la base
Me.Frame6.Caption = GetMesseprog("GESTIDENT", "20")
Me.SSTab1.TabCaption(2) = GetMesseprog("GESTIDENT", "16")
Me.Label15.Caption = GetMesseprog("GESTIDENT", "17")
Me.Label26.Caption = GetMesseprog("GESTIDENT", "18")
Me.Label16.Caption = aIdent.Emilie_Count
Me.Label27.Caption = aIdent.Fname_Count

'-> Acc�s au descriptif
Me.Command2.Caption = GetMesseprog("GESTIDENT", "19")

End Sub
Private Sub AddIdxList(aBase As clsBase)

'---> Cette proc�dure ajoute une icone par Idx buid

Dim X As ListItem
Dim aIdx As clsIdx

For Each aIdx In aBase.Idx
    '-> Cr�er une icone
    Set X = Me.ListView1.ListItems.Add(, "IDX|" & aIdx.Num_Idx, aIdx.Num_Idx, "IDX")
Next 'Pour tous les idx

End Sub

Private Sub RunBase(aBase As clsBase)

'-> Base lanc�e, arret�e
If aBase.IsRunning Then
    Me.Label21.Caption = GetMesseprog("BASE", "11") & aBase.When
Else
    Me.Label21.Caption = GetMesseprog("BASE", "10") & aBase.When
End If

'-> Nombre d'idx en cours
Me.Label24.Caption = aBase.Idx.Count

End Sub

Private Function CheckIdent(aIdent As clsIdent) As Boolean

Dim ErroCode As Integer
Dim aBase As clsBase
Dim FileName As String

'-> Tester s'il y a eu une erreur dans le chargement de l'ident
If aIdent.ErrorLoading <> 0 Then
BadSession:
    '-> Bloquer l'onglet de la table
    Me.SSTab1.TabEnabled(1) = False
    '-> Afficher le titre du message
    Me.Label9.Caption = GetMesseprog("CHECKIDENT", "1")
    Me.Label10.Caption = GetMesseprog("CHECKIDENT", CStr(aIdent.ErrorLoading)) & Chr(13) & FileName
    Me.picError.Visible = True
    Exit Function
End If

'-> V�rifier si on trouve le sch�ma
aIdent.ErrorLoading = 4
FileName = aIdent.FichierSchema
If Dir$(aIdent.FichierSchema) = "" Then GoTo BadSession

'-> V�rifier si on trouve le catalogue
aIdent.ErrorLoading = 3
FileName = aIdent.FichierCatalogue
If Dir$(aIdent.FichierCatalogue) = "" Then GoTo BadSession

'-> V�rifier si on trouve les liens
aIdent.ErrorLoading = 5
FileName = aIdent.FichierLink
If Dir$(aIdent.FichierLink) = "" Then GoTo BadSession

'-> V�rifier si on trouve le constructeur d'index
aIdent.ErrorLoading = 11
FileName = ApplicationPath & "B3D_IDX.EXE"
If Dir$(FileName) = "" Then GoTo BadSession

'-> Pointer sur l'objet base
Set aBase = Bases(aIdent.Base)

'-> V�rifier si on trouve le fichier Base
aIdent.ErrorLoading = 12
FileName = aBase.Fichier
If Dir$(FileName) = "" Then GoTo BadSession

'-> Renvoyer une valeur de succ�s
aIdent.ErrorLoading = 0
CheckIdent = True

End Function



Private Sub DrawActivity()

'-> % Occupation
Me.Label8.Caption = Format(aIdent.NbConnect / aIdent.Limite_AgentB3D, "0.00 %")

'-> Dessin
Me.Picture2.Cls
If aIdent.NbConnect <> 0 Then DrawLoadding Me.Picture2, aIdent.NbConnect, aIdent.Limite_AgentB3D

End Sub



Private Sub Form_Unload(Cancel As Integer)

'-> Mettre � jour le handle de la feuille
aIdent.HdlForm = 0
aIdent.hdlCom = 0

End Sub


Private Sub Picture3_Paint()

Dim res As Long
Dim lpBuffer As String
Dim strOrdre As String

On Error Resume Next

'-> R�cup�ration du message
lpBuffer = Space$(2000)
res = GetWindowText(Me.Picture3.hwnd, lpBuffer, Len(lpBuffer))
strOrdre = UCase$(Trim(Entry(1, lpBuffer, Chr(0))))

'-> Quitter si pas de message
If strOrdre = "" Then Exit Sub

'-> Selon le retour
Select Case Entry(1, strOrdre, "|")
    Case "DRAW_ACTIVITY"
        '-> Mettre � jour le nombre des connexions utilisateur
        Me.Text3.Text = aIdent.NbConnect
        '-> Dessin du taux d'activit�
        DrawActivity
End Select

'-> Vider la zone
SetWindowText Me.Picture3.hwnd, ""

End Sub

Private Sub Text1_GotFocus(Index As Integer)
SelectTxtBox Me.Text1(Index)
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub Timer1_Timer()

'-> Incr�menter le conteur
TailleLue = TailleLue + 1

'-> Dessin de la temporisation
DrawLoadding Me.Picture1, TailleLue, TailleTotale
DoEvents

'-> Mise � jour
If TailleLue = TailleTotale Then
    Me.Picture1.Cls
    TailleLue = 1
End If

End Sub
