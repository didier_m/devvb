VERSION 5.00
Begin VB.Form frmNewOperat 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   3975
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4215
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3975
   ScaleWidth      =   4215
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Height          =   3975
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4215
      Begin VB.CommandButton Command1 
         Height          =   855
         Left            =   2880
         Picture         =   "frmNewOperat.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   3000
         Width           =   1215
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   2040
         TabIndex        =   8
         Top             =   2160
         Width           =   2055
      End
      Begin VB.CheckBox Check1 
         Alignment       =   1  'Right Justify
         Caption         =   "Check1"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   2640
         Width           =   3975
      End
      Begin VB.TextBox Text4 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2040
         MaxLength       =   10
         TabIndex        =   7
         Top             =   1680
         Width           =   1215
      End
      Begin VB.TextBox Text3 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2040
         MaxLength       =   10
         PasswordChar    =   "*"
         TabIndex        =   6
         Top             =   1200
         Width           =   2055
      End
      Begin VB.TextBox Text2 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2040
         MaxLength       =   10
         PasswordChar    =   "*"
         TabIndex        =   4
         Top             =   720
         Width           =   2055
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   2040
         MaxLength       =   10
         TabIndex        =   2
         Top             =   240
         Width           =   2055
      End
      Begin VB.Image Image3 
         Height          =   480
         Left            =   1200
         Picture         =   "frmNewOperat.frx":0CCA
         Top             =   3000
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Image Image1 
         Height          =   240
         Left            =   3720
         Picture         =   "frmNewOperat.frx":1994
         Top             =   1730
         Width           =   240
      End
      Begin VB.Label Label5 
         Caption         =   "Label2"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   2160
         Width           =   1815
      End
      Begin VB.Image Image2 
         Height          =   480
         Left            =   3240
         Picture         =   "frmNewOperat.frx":1ADE
         Top             =   1605
         Width           =   480
      End
      Begin VB.Label Label4 
         Caption         =   "Label2"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   1680
         Width           =   1815
      End
      Begin VB.Label Label3 
         Caption         =   "Label2"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   1200
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "Label2"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   1815
      End
   End
End
Attribute VB_Name = "frmNewOperat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public ModeOpen As Integer  '0-> Add , 1 -> Edit 2 MonoPoste

Private Sub Combo1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Me.Check1.SetFocus

End Sub

Private Sub Command1_Click()

'---> Effectuer les v�rifications

Dim lpBuffer As String
Dim Rep As VbMsgBoxResult
Dim AddIt As Boolean

'-> si on a saisi le code op�rateur
If Trim(Me.Text1.Text) = "" Then GoTo BadOperat
    
'-> Syntaxe
If InStr(1, Me.Text1.Text, "=") <> 0 Then GoTo BadOperat
If InStr(1, Me.Text1.Text, "�") <> 0 Then GoTo BadOperat
If InStr(1, Me.Text1.Text, "|") <> 0 Then GoTo BadOperat

'-> Si on a saisi le mot de passe
If Trim(Me.Text2.Text) = "" Then GoTo BadPassword
If Trim(Me.Text3.Text) = "" Then GoTo BadPassword

'-> V�rifier si mot de passe = confirmation
If Me.Text2.Text <> Me.Text3.Text Then GoTo BadConfirm

'-> V�rifier si on a saisi un code langue
If Me.Combo1.ListIndex = -1 Then GoTo BadLangue
    
'-> Indiquer que l'on doit ajouter une entr�e dans la listview
If ModeOpen = 0 Then AddIt = True

'-> V�rifier si le code op�rateur sp�cifi� existe d�ja
lpBuffer = GetIniFileValue("OPERATS", Trim(Me.Text1.Text), OPERATIniFile)
If Trim(lpBuffer) <> "" And ModeOpen = 0 Then
    '-> Le code op�rateur existe d�ja, demander si on modifier ou non
    Rep = MsgBox(GetMesseprog("MESSAGE", "16"), vbQuestion + vbYesNo, GetMesseprog("COMMUN", "12"))
    If Rep = vbNo Then Exit Sub
    '-> Ne pas ajouter d'entr�e : elle existe d�ja
    AddIt = False
End If

'-> Construire la chaine des op�rateurs
lpBuffer = Trim(Me.Text2.Text) & "|" & Me.Combo1.ListIndex + 1 & "|"
If Me.Text4.Text = "" Then
    lpBuffer = lpBuffer & "NO_LIMIT"
Else
    lpBuffer = lpBuffer & Me.Text4.Text
End If
lpBuffer = lpBuffer & "|" & CStr(Me.Check1.Value)

'-> Enregistrer
WritePrivateProfileString "OPERATS", Me.Text1.Text, Crypt(lpBuffer), OPERATIniFile

'-> Mettre � jour l'affichage de la console maitresse
If ModeOpen <> 2 Then
    If AddIt Then frmPortailB3D.ListView2.ListItems.Add , "OPE|" & Trim(UCase$(Me.Text1.Text)), Trim(Me.Text1.Text), , "OPE"
Else
    OpePassWord = Me.Text2.Text
End If

'-> Afficher les propri�t�s de cet op�rateur
If ModeOpen <> 2 Then frmPortailB3D.DisplayOpeProp Me.Text1.Text

'-> D�charger la feuille
Unload Me

Exit Sub

BadOperat:
    MsgBox GetMesseprog("MESSAGE", "12"), vbCritical + vbOKOnly, GetMesseprog("COMMUN", "1")
    Me.Text1.SetFocus
    Exit Sub
    
BadPassword:
    MsgBox GetMesseprog("MESSAGE", "13"), vbCritical + vbOKOnly, GetMesseprog("COMMUN", "1")
    Me.Text2.SetFocus
    Exit Sub
    
BadConfirm:
    MsgBox GetMesseprog("MESSAGE", "14"), vbCritical + vbOKOnly, GetMesseprog("COMMUN", "1")
    Me.Text3.SetFocus
    Exit Sub
    
BadLangue:
    MsgBox GetMesseprog("MESSAGE", "15"), vbCritical + vbOKOnly, GetMesseprog("COMMUN", "1")
    Me.Combo1.SetFocus
    Exit Sub

End Sub

Private Sub Form_Load()

Dim i As Integer

'-> Titre de la feuille
Me.Caption = GetMesseprog("NEWOPERAT", "1")

'-> Label
Me.Label1.Caption = GetMesseprog("PORTAILB3D", "7")
Me.Label2.Caption = GetMesseprog("PORTAILB3D", "13")
Me.Label3.Caption = GetMesseprog("NEWOPERAT", "2")
Me.Label4.Caption = GetMesseprog("PORTAILB3D", "15")
Me.Label5.Caption = GetMesseprog("PORTAILB3D", "14")
Me.Check1.Caption = GetMesseprog("PORTAILB3D", "16")

'-> Bouton
Me.Command1.Caption = GetMesseprog("COMMUN", "17")

'-> Charger les images de codes langues
For i = 1 To 10
    Me.Combo1.AddItem GetMesseprog("PAYS", CStr(i))
Next

'-> Par d�faut mode ajout
ModeOpen = 0

End Sub

Public Sub Init(Operat As String)

'---> Cette proc�dure est appell�e pour modifier les param�tres d'un op�rateur existant

Dim lpBuffer As String

'-> Afficher les propri�t�s de l'op�rateur
lpBuffer = DeCrypt(GetIniFileValue("OPERATS", Operat, OPERATIniFile))

'-> Code
Me.Text1.Text = Operat

'-> Pwd
Me.Text2.Text = Entry(1, lpBuffer, "|")

'-> Confirm
Me.Text3.Text = Entry(1, lpBuffer, "|")

'-> date
If Entry(3, lpBuffer, "|") = "NO_LIMIT" Then
    Me.Text4.Text = ""
Else
    Me.Text4.Text = Entry(3, lpBuffer, "|")
End If

'-> Code Langue
Me.Combo1.ListIndex = CInt(Entry(2, lpBuffer, "|")) - 1

'-> MultiLog
Me.Check1.Value = CInt(Entry(4, lpBuffer, "|"))

'-> Bloquer le code de l'op�rateur
Me.Text1.Enabled = False

'-> Indiquer que l'on est en mode modif
ModeOpen = 1

'-> Bouton
Me.Command1.Caption = GetMesseprog("COMMUN", "19")
Me.Command1.Picture = Me.Image3.Picture



End Sub

Private Sub Image1_Click()
Me.Text4.Text = ""
End Sub

Private Sub Image2_Click()

'---> Afficher la feuille de gestion du calendrier
frmGetDate.Show vbModal

'-> Mettre � jour si necessaire
If strRetour <> "" Then Me.Text4.Text = strRetour
    
'-> Donner le focus au code langue
Me.Combo1.SetFocus
    
End Sub

Private Sub Text1_GotFocus()
SelectTxtBox Me.Text1
End Sub
Private Sub Text1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then Me.Text2.SetFocus
End Sub
Private Sub Text2_GotFocus()
SelectTxtBox Me.Text2
End Sub
Private Sub Text2_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then Me.Text3.SetFocus
End Sub
Private Sub Text3_GotFocus()
SelectTxtBox Me.Text3
End Sub
