VERSION 5.00
Object = "{8E27C92E-1264-101C-8A2F-040224009C02}#7.0#0"; "MSCAL.OCX"
Begin VB.Form frmGetDate 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   4020
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4020
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Height          =   375
      Left            =   2520
      TabIndex        =   2
      Top             =   3600
      Width           =   975
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   3600
      TabIndex        =   1
      Top             =   3600
      Width           =   975
   End
   Begin MSACAL.Calendar Calendar1 
      Height          =   3615
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4695
      _Version        =   524288
      _ExtentX        =   8281
      _ExtentY        =   6376
      _StockProps     =   1
      BackColor       =   -2147483633
      Year            =   2001
      Month           =   12
      Day             =   17
      DayLength       =   1
      MonthLength     =   2
      DayFontColor    =   0
      FirstDay        =   2
      GridCellEffect  =   1
      GridFontColor   =   10485760
      GridLinesColor  =   -2147483632
      ShowDateSelectors=   -1  'True
      ShowDays        =   -1  'True
      ShowHorizontalGrid=   -1  'True
      ShowTitle       =   -1  'True
      ShowVerticalGrid=   -1  'True
      TitleFontColor  =   10485760
      ValueIsNull     =   0   'False
      BeginProperty DayFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty GridFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TitleFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmGetDate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Calendar1_NewMonth()
Me.Calendar1.Day = 1
End Sub
Private Sub Calendar1_NewYear()
Me.Calendar1.Month = 1
Me.Calendar1.Day = 1
End Sub
Private Sub Command1_Click()
strRetour = Me.Calendar1.Value
Unload Me
End Sub
Private Sub Command2_Click()
strRetour = ""
Unload Me
End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("COMMUN", "21")
Me.Command1.Caption = GetMesseprog("COMMUN", "4")
Me.Command2.Caption = GetMesseprog("COMMUN", "5")

Me.Calendar1.Value = Now

End Sub
