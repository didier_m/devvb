VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmChxAgent 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   3660
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   7560
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   244
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   504
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4920
      Top             =   360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmChxAgent.frx":0000
            Key             =   "AGENT"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3615
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   6376
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "frmChxAgent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Ident As String

Private Sub Form_Load()

Dim aCon As clsConnectConsole
Dim ToAdd As Boolean

'-> Titre de la feuille
Me.Caption = GetMesseprog("FRMAGENT", "1")

'-> Titre des ent�tes de colonne
Me.ListView1.ColumnHeaders(1).Text = GetMesseprog("PORTAILB3D", "7")
Me.ListView1.ColumnHeaders(2).Text = GetMesseprog("COMMUN", "13")
Me.ListView1.ColumnHeaders(3).Text = GetMesseprog("PORTAILB3D", "8")
Me.ListView1.ColumnHeaders(4).Text = GetMesseprog("PORTAILB3D", "9")
Me.ListView1.ColumnHeaders(5).Text = GetMesseprog("PORTAILB3D", "10")

'-> Ne charger que les agents consoles qui ne sont pas en trai de travailler
For Each aCon In ConnexionsDescripteur
    '-> De base on n'ajoute pas
    ToAdd = False
    '-> Agent console
    If aCon.IsRunConsole Then
        '-> Ne doit pas �tre en train de travailler
        If Not aCon.IsWorking Then
            '-> M�me ident
            If Ident = "" Then
                ToAdd = True
            Else
                If aCon.Ident = Ident Then ToAdd = True
            End If
            If ToAdd Then
                '-> Ajouter dans la liste des agants disponibles
                Set X = Me.ListView1.ListItems.Add(, "CON|" & aCon.IndexAgent, "    " & aCon.Operat, , "AGENT")
                X.SubItems(1) = aCon.DateConnexion
                X.SubItems(2) = Format(aCon.IndexAgent, "0000")
                X.SubItems(3) = aCon.Num_Idx
                X.SubItems(4) = aCon.hdlProcess
            End If 'Si m�me ident
        End If 'Si travaille
    End If 'Si agent conosle
Next

'-> Formatter l'entete
FormatListView Me.ListView1
Me.ListView1.Refresh

End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next

GetClientRect Me.hwnd, aRect

Me.ListView1.Left = 0
Me.ListView1.Top = 0
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom

End Sub


Private Sub ListView1_ItemClick(ByVal Item As MSComctlLib.ListItem)

strRetour = Me.ListView1.SelectedItem.Key
Unload Me

End Sub
