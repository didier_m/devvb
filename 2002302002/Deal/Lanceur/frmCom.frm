VERSION 5.00
Begin VB.Form frmCom 
   Caption         =   "Form1"
   ClientHeight    =   885
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   1560
   LinkTopic       =   "Form1"
   ScaleHeight     =   885
   ScaleWidth      =   1560
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer TimerAcces 
      Left            =   1080
      Top             =   360
   End
   Begin VB.PictureBox objCom 
      Height          =   495
      Index           =   0
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   795
      TabIndex        =   0
      Top             =   0
      Width           =   855
   End
End
Attribute VB_Name = "frmCom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub ObjCom_Paint(Index As Integer)

Dim res(1 To 9999) As Long
Dim lpBuffer(1 To 9999) As String
Dim strOrdre(1 To 9999) As String
Dim aIdx As clsIdx
Dim aBase As clsBase
Dim aCon As clsConnectConsole

'On Error Resume Next

'-> R�cup�ration du message
lpBuffer(Index) = Space$(2000)
res(Index) = GetWindowText(Me.objCom(Index).hwnd, lpBuffer(Index), Len(lpBuffer(Index)))
strOrdre(Index) = UCase$(Trim(Entry(1, lpBuffer(Index), Chr(0))))

Debug.Print strOrdre(Index)

'-> Quitter si pas de message
If strOrdre(Index) = "" Then Exit Sub
'-> Selon le retour
Select Case Entry(1, strOrdre(Index), "|")
    Case "IDX_RETOUR_HANDLE"
        '-> Pointer sur la base associ�e
        Set aBase = Bases(Entry(3, strOrdre(Index), "|"))
        '-> Pointer sur l'objet IDX associ�
        Set aIdx = aBase.Idx("IDX|" & Format(Index, "0000"))
        '-> Setting du handle de communication
        aIdx.Hdl_Retour_Com = CLng(Entry(2, strOrdre(Index), "|"))
    Case "IDX_START_WORKING"
        '-> Pointer sur la base associ�e
        Set aBase = Bases(Entry(3, strOrdre(Index), "|"))
        '-> Pointer sur l'objet IDX associ�
        Set aIdx = aBase.Idx("IDX|" & Format(Index, "0000"))
        '-> Indiquer qu'il est en train de travaille
        aIdx.IsWorking = True
    Case "IDX_STOP_WORKING"
        '-> Pointer sur la base associ�e
        Set aBase = Bases(Entry(3, strOrdre(Index), "|"))
        '-> Pointer sur l'objet IDX associ�
        Set aIdx = aBase.Idx("IDX|" & Format(Index, "0000"))
        '-> Indiquer qu'il n'est plus en train de travaille
        aIdx.IsWorking = False
    Case "IDX_END_SESSION" 'Fin d'un agent IDX
        '-> Pointer sur la base associ�e
        Set aBase = Bases(Entry(3, strOrdre(Index), "|"))
        '-> Pointer sur l'objet IDX associ�
        Set aIdx = aBase.Idx("IDX|" & Format(Index, "0000"))
        '-> Indiquer que l'IDX est fini
        aIdx.EndIdx = True
    Case "AGENTB3D_START_WORKING"
        '-> Pointer sur la connexion sp�cifi�e
        Set aCon = ConnexionsDescripteur("CON|" & CStr(CInt(Entry(2, strOrdre(Index), "|"))))
        '-> Indiquer qu'il a fini de travailler
        aCon.IsWorking = True
    Case "AGENTB3D_STOP_WORKING"
        '-> Pointer sur la connexion sp�cifi�e
        Set aCon = ConnexionsDescripteur("CON|" & CStr(CInt(Entry(2, strOrdre(Index), "|"))))
        '-> Indiquer qu'il a fini de travailler
        aCon.IsWorking = False
    Case "AGENTB3D_END_SESSION"
        '-> Pointer sur la connexion sp�cifi�e
        Set aCon = ConnexionsDescripteur("CON|" & CStr(CInt(Entry(2, strOrdre(Index), "|"))))
        '-> Indiquer que la connexion est morte
        aCon.EndProg = True
        DoEvents
End Select
'-> supprimer le contenu de la zone texte de l'objet
res(Index) = SetWindowText(Me.objCom(Index).hwnd, "")
DoEvents

End Sub

Private Sub TimerAcces_Timer()

'---> Analyse d'un fichier ascii et cr�ation d'un module d'acces aux bases

Dim CnXFile As String
Dim l_Operat As String
Dim l_Pwd As String
Dim l_Console As String
Dim hdlFile As Integer
Dim Ligne As String
Dim Ident As String
Dim DebugMode As String
Dim TraceMode As String
Dim CryptMode As String
Dim TempFileName As String
Dim TempFileNameParam As String
Dim ServeurDOG As Object
Dim Id_Session As String
Dim aBase As clsBase
Dim hdlAgent As Long
Dim res As Long
Dim hdlCnr As Integer
Dim aIdx As clsIdx
Dim l_Code As String
Dim aCon As clsConnectConsole
Dim aIdent As clsIdent
Dim x As ListItem
Dim RetunrSession As String

'-> Rendre la main � la CPU
DoEvents

'-> Analyse du r�pertoire
If Dir$(DmdPath & "*.cnx", vbNormal) <> "" Then
    '-> R�cup�ration du nom du fichier Ascii
    CnXFile = Dir$(DmdPath & "*.cnx", vbNormal)
            
    '-> Ouvrir le fichier CNX pour get du param�trage
    hdlFile = FreeFile
    Open DmdPath & CnXFile For Input As #hdlFile
    If Len(hdlFile) <> 0 Then Line Input #hdlFile, Ligne
    Close #hdlFile
    Kill DmdPath & CnXFile
    
    '-> Decrypter la ligne
    Ligne = DeCrypt(Ligne)
    
    '-> R�cup�rer les param�trages
    l_Operat = Entry(1, Ligne, "�") 'Code op�rateur
    l_Pwd = Entry(2, Ligne, "�") 'Mot de passe
    Ident = Trim(Entry(3, Ligne, "�")) 'Ident
    DebugMode = Trim(Entry(4, Ligne, "�")) 'Debug
    l_Console = Trim(Entry(5, Ligne, "�"))    'Console
    TraceMode = Trim(Entry(6, Ligne, "�"))  'Trace
    CryptMode = Trim(Entry(7, Ligne, "�"))
    RetunrSession = Trim(Entry(8, Ligne, "�"))   'Session pour r�ponse
        
    '-> Get d'un pointeur vers le serveur DOG
    Set aIdent = Idents("IDENT|" & Ident)
    
    '-> Ouvrir un handle de fichier retour
    hdlCnr = FreeFile
    TempFileName = GetTempFileNameVB("CNR")
    Open TempFileName For Output As #hdlCnr
    
    '-> V�rifier le nombre des connexions en cours
    If Connexions.Count + 1 > aIdent.Limite_AgentB3D Then
        '-> Renvoyer un fichier CNR de retour indiquant que la connexion a �chou�e
        Print #hdlCnr, "CONNEXION�ERROR�CONNEXION_FULL"
       '-> Fin
        GoTo SendCNR
    End If
    
    '-> V�rifier le mot de passe
    If Not CheckPassWord(l_Operat, l_Pwd) Then
        '-> Renvoyer un fichier CNR de retour indiquant que la connexion a �chou�e
        Print #hdlCnr, "CONNEXION�ERROR�INVALID_PASSWORD"
        '-> Fin
        GoTo SendCNR
    End If
    
    '-> V�rifier la date de vaidit� du code op�rateur
    If Not CheckDateValiditeOperat(l_Operat) Then
        '-> Renvoyer un fichier CNR de retour indiquant que la connexion a �chou�e
        Print #hdlCnr, "CONNEXION�ERROR�INVALID_DATE_OPERAT"
        '-> Fin
        GoTo SendCNR
    End If
        
    '-> V�rifier si le serveur DOG est actif
    If Not aIdent.IsRunning Then
        '-> le serveur n'est pas demmar�
        Print #hdlCnr, "CONNEXION�ERROR�SERVEURDOG_STOPPED"
        '-> Fin
        GoTo SendCNR
    End If
    
    '-> V�rifier que le serveur n'est pas en cours de fermeture
    If aIdent.IsEnding Then
            '-> le serveur n'est pas demmar�
        Print #hdlCnr, "CONNEXION�ERROR�SERVEURDOG_ASK_FOR_END"
        '-> Fin
        GoTo SendCNR
    End If
        
    '-> V�rifier si on a droit au multi-log sur ce compte op�rateur
    If Not CheckMultiLog(l_Operat) Then
        '-> V�rifier si une connexion existe d�ja pour ce mec
        For Each aCon In Connexions
            If aCon.Operat = l_Operat Then
                '-> Envoyer une erreur
                Print #hdlCnr, "CONNEXION�ERROR�LOGIN_ALREADY_USED"
                '-> Fin
                GoTo SendCNR
            End If
        Next 'Pour toutes les connexionbs
    End If 'Si op�rateur multi-log
    
    '-> Pointer sur la base sp�cifi�e
    Set aBase = Bases(aIdent.Base)
        
    '-> Get d'un num�ro de session bas� sur le temps
    res = GetTickCount()
    Id_Session = CStr(res)
    
    '-> Cr�er un nouvel Agent dans la collection
    Set aCon = New clsConnectConsole

    '-> Setting des propri�t�s
    aCon.Operat = l_Operat
    aCon.IdSession = res
    aCon.DateConnexion = Now
    aCon.Num_Idx = aBase.Get_IDX
    aCon.IndexAgent = Get_Free_Agent(True)
    aCon.Ident = Ident
    aCon.CodeLangue = CInt(Entry(4, DeCrypt(GetIniFileValue("OPERATS", l_Operat, OPERATIniFile)), "|"))
    
    '-> Mode debug
    If DebugMode = "1" Then aCon.DebugMode = True
    
    '-> Mode trace
    If TraceMode = "1" Then aCon.TraceMode = True
            
    '-> Mode Cryptage
    If CryptMode = "1" Then aCon.CryptMode = True
            
    '-> Connexion Ok : initialiser les noms des fichiers d'�change
    aCon.RspFile = RspPath & Id_Session & ".rsp"
    aCon.DmdFile = DmdPath & Id_Session & ".dmd"
        
    '-> Indiquer si on est en mode console
    aCon.IsRunConsole = CBool(l_Console)
        
    '-> Ajouter dans la collection des connections
    ConnexionsDescripteur.Add aCon, "CON|" & aCon.IndexAgent
        
    '-> Cr�er so handle de communication
    If Not Matrice_Com_Agent(aCon.IndexAgent) Then
        Load frmCom.objCom(aCon.IndexAgent)
        Matrice_Com_Agent(aCon.IndexAgent) = True
    End If
    
    '-> Cr�er un fichier de param�trage pour exporter les param�tres
    hdlFile = FreeFile
    TempFileNameParam = GetTempFileNameVB("B3D")
    Open TempFileNameParam For Output As #hdlFile
    
    '-> Param�trage g�n�raux
    Print #hdlFile, "[PARAM]"
    Print #hdlFile, "IDSESSION=" & Crypt(aCon.IdSession)
    Print #hdlFile, "IDAGENT=" & Crypt(Format(aCon.IndexAgent, "0000"))
    Print #hdlFile, "IDIDX=" & Crypt(aCon.Num_Idx)
    Print #hdlFile, "OPERAT=" & Crypt(aCon.Operat)
    Print #hdlFile, "CODELANGUE=  " & Crypt(CStr(aCon.CodeLangue))
    Print #hdlFile, "UC_ID=" & Crypt(CStr(Uc_Id))
    Print #hdlFile, "HDLCOM=" & Crypt(CStr(frmCom.objCom(aCon.IndexAgent).hwnd))
    Print #hdlFile, "DEBUG=" & Crypt(DebugMode)
    Print #hdlFile, "TRACE=" & Crypt(TraceMode)
    Print #hdlFile, "CRYPT=" & Crypt(CryptMode)
    Print #hdlFile, "REPLICAUPDATE=" & Crypt(CStr(ReplicaUpdate))
    Print #hdlFile, "LOCK=" & Crypt(VecteurLock)
    Print #hdlFile, "ACCESDIR=" & Crypt(CStr(Tempo_Dmd))
    
    '-> Param�trage de l'ident
    Print #hdlFile, "[IDENT]"
    Print #hdlFile, "CODE=" & Crypt(aIdent.Code)
    Print #hdlFile, "SCH=" & Crypt(aIdent.FichierSchema)
    Print #hdlFile, "CAT=" & Crypt(aIdent.FichierCatalogue)
    Print #hdlFile, "LNK=" & Crypt(aIdent.FichierLink)
    
    '-> Param�trage de la base
    Print #hdlFile, "[BASE]"
    Print #hdlFile, "NOM=" & aBase.Nom_Base
    Print #hdlFile, "SERVEUR=" & aBase.Serveur
    Print #hdlFile, "TYPE=" & Crypt(aBase.Type_Base)
    Print #hdlFile, "USER=" & Crypt(aBase.User)
    Print #hdlFile, "PWD=" & Crypt(aBase.Pwd)
    Print #hdlFile, "FICHIER=" & Crypt(aBase.Fichier)
    Print #hdlFile, "TEMPO=" & Crypt(CStr(aBase.Tempo_Lock))
        
    '-> Param�trage des fichiers d'�change
    Print #hdlFile, "[PATH]"
    Print #hdlFile, "DMD=" & Crypt(DmdPath & Id_Session & ".dmd")
    Print #hdlFile, "RSP=" & Crypt(RspPath & Id_Session & ".rsp")
    Print #hdlFile, "DEBUG=" & Crypt(LogPath & Id_Session & ".dbg")
    Print #hdlFile, "TRACE=" & Crypt(LogPath & Id_Session & ".trc")
    Print #hdlFile, "INT=" & Crypt(IntPath)
    Print #hdlFile, "INTSOV=" & Crypt(IntSovPath)
    Print #hdlFile, "REPLICAIN=" & Crypt(RepInputPath)
    Print #hdlFile, "REPLICAOUT=" & Crypt(RepOutputPath)
    Print #hdlFile, "B3DINI=" & Crypt(B3DIniFile)
    Print #hdlFile, "RSPPATH=" & Crypt(RspPath)
    Print #hdlFile, "MESSPROGINIFILE=" & Crypt(MessprogIniFile)
    
    '-> Fermer le fichier
    Close #hdlFile
        
    '-> Lancer un agent B3D
    hdlAgent = Shell(ApplicationPath & "\B3D_AGENT.exe " & TempFileNameParam, vbHide)
                                                  
    '-> Affecter le handle du process � l'objet agent
    aCon.hdlProcess = hdlAgent
    
    If aCon.Num_Idx <> "" Then
        '-> Pointer sur l'objet IDX pour incr�menter le nombre d'agents B3D affect�s � ce constructeur d'index
        Set aIdx = aBase.Idx("IDX|" & aCon.Num_Idx)
        aIdx.NbAgent = aIdx.NbAgent + 1
    End If
    
    '-> Mettre � jour l'interface si l'interface est visible
    If lpsConsole.IsVible Then
        Set x = frmPortailB3D.ListView1.ListItems.Add(, "CON|" & aCon.IndexAgent, "   " & aCon.Operat, , "AGENT")
        x.SubItems(1) = Now
        x.SubItems(2) = Format(aCon.IndexAgent, "0000")
        x.SubItems(3) = aCon.Num_Idx
        x.SubItems(4) = hdlAgent
    End If
    
    '-> Formater le browse
    FormatListView frmPortailB3D.ListView1
    
    '-> Mettre � jour ici le nombre de connexions clients et les % d'activit�
    aIdent.NbConnect = aIdent.NbConnect + 1
    
    '-> si la feuille de gestion des identifiants est ouverte , envoyer un ordre de r��criture
    If aIdent.HdlForm <> 0 Then
        res = SetWindowText(aIdent.hdlCom, "DRAW_ACTIVITY")
        res = SendMessage(aIdent.hdlCom, WM_PAINT, 0, 0)
    End If
        
    '-> Mise � jour de l'historique des connexions pour cet op�rateur
    PrintHisto aCon.Operat & "|" & Format(Now, "yyyymmdd") & "|" & aIdent.Code & "|" & aIdent.Base
        
    '-> Fermer le fichier CNR en renvoyant un message de succes et son hanle de session
    Print #hdlCnr, "CONNEXION�OK�" & aCon.IndexAgent & "�" & Uc_Id & "�" & Id_Session & "�" & aCon.CodeLangue
    
    '-> Envoyer la r�ponse
    GoTo SendCNR
    
End If 'Si on trouve un fichier Ascii de demande de connexion (CNX)

'-> Rendre la main � la cpu
DoEvents

'-> Quitter la fonction
Exit Sub

SendCNR:
    '-> Fermer le fichier
    Close #hdlCnr
    '-> Supprimer un fichier de reponse connexion s'il existe
    If Dir$(RspPath & RetunrSession & ".cnr", vbNormal) <> "" Then Kill RspPath & RetunrSession & ".cnr"
    '-> Le renommer sous la forme d'un fichier r�ponse
    Name TempFileName As RspPath & RetunrSession & ".cnr"
    DoEvents


End Sub


