VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmDescriptif 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   8655
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11235
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   577
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   749
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   6495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   11456
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "frmDescriptif.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "ListView1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "ImageList1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "frmDescriptif.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "ListView2"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Tab 2"
      TabPicture(2)   =   "frmDescriptif.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "ListView3"
      Tab(2).ControlCount=   1
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   4200
         Top             =   480
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         MaskColor       =   12632256
         _Version        =   393216
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   3135
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   5530
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView ListView2 
         Height          =   3255
         Left            =   -74040
         TabIndex        =   2
         Top             =   1200
         Width           =   4215
         _ExtentX        =   7435
         _ExtentY        =   5741
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView ListView3 
         Height          =   2535
         Left            =   -74280
         TabIndex        =   3
         Top             =   1680
         Width           =   5535
         _ExtentX        =   9763
         _ExtentY        =   4471
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Menu mnuCatalogue 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu mnuAddEmilie 
         Caption         =   ""
      End
      Begin VB.Menu mnuDelEmilie 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPropEmilie 
         Caption         =   ""
      End
      Begin VB.Menu mnuPrintEmilie 
         Caption         =   ""
      End
   End
   Begin VB.Menu mnuSchema 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu mnuAddSchema 
         Caption         =   ""
      End
      Begin VB.Menu mnuDelSchema 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPropSchema 
         Caption         =   ""
      End
      Begin VB.Menu mnuPrintSchema 
         Caption         =   ""
      End
   End
   Begin VB.Menu mnusys 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu mnuAddSys 
         Caption         =   ""
      End
      Begin VB.Menu mnuDelSys 
         Caption         =   ""
      End
      Begin VB.Menu mnusep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPropSys 
         Caption         =   ""
      End
      Begin VB.Menu mnuPrintSys 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmDescriptif"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public IdentKey As String


Public Sub Init()

On Error GoTo GestError

'-> Pointeur de souris et �cran
Me.Enabled = False
Screen.MousePointer = 11

'-> Afficher le catalogue
DisplayCatalogue

'-> Afficher le sch�ma
DisplaySchema

'-> Afficher les tables syst�mes
DisplaySystemTable

GestError:

'-> Pointeur de souris et �cran
Me.Enabled = True
Screen.MousePointer = 0

End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("FRMDESCRIPTIF", "1")

'-> Titres des onglets
Me.SSTab1.TabCaption(0) = GetMesseprog("GESTIDENT", "4")
Me.SSTab1.TabCaption(1) = GetMesseprog("GESTIDENT", "5")
Me.SSTab1.TabCaption(2) = GetMesseprog("FRMDESCRIPTIF", "2")

'-> Colonne du catalogue
Me.ListView1.ColumnHeaders(1).Text = GetMesseprog("FRMEMILIE", "16")
Me.ListView1.ColumnHeaders(2).Text = GetMesseprog("COMMUN", "22")
Me.ListView1.ColumnHeaders(3).Text = GetMesseprog("FRMEMILIE", "18")
Me.ListView1.ColumnHeaders(4).Text = GetMesseprog("FRMEMILIE", "7")
Me.ListView1.ColumnHeaders(5).Text = GetMesseprog("FRMEMILIE", "8")
Me.ListView1.ColumnHeaders(6).Text = GetMesseprog("PORTAILB3D", "14")
Me.ListView1.ColumnHeaders(7).Text = GetMesseprog("FRMEMILIE", "19")

'-> Menu du catalogue
Me.mnuAddEmilie.Caption = GetMesseprog("FRMDESCRIPTIF", "3")
Me.mnuDelEmilie.Caption = GetMesseprog("FRMDESCRIPTIF", "4")
Me.mnuPropEmilie.Caption = GetMesseprog("FRMDESCRIPTIF", "5")
Me.mnuPrintEmilie.Caption = GetMesseprog("FRMDESCRIPTIF", "6")

'-> Colonne du sch�ma
Me.ListView2.ColumnHeaders(1).Text = GetMesseprog("FRMEMILIE", "15")
Me.ListView2.ColumnHeaders(2).Text = GetMesseprog("COMMUN", "22")
Me.ListView2.ColumnHeaders(3).Text = GetMesseprog("FRMEMILIE", "20")
Me.ListView2.ColumnHeaders(4).Text = GetMesseprog("FRMEMILIE", "21")
Me.ListView2.ColumnHeaders(5).Text = GetMesseprog("FRMEMILIE", "22")

'-> Menu du sch�ma
Me.mnuAddSchema.Caption = GetMesseprog("FRMDESCRIPTIF", "7")
Me.mnuDelSchema.Caption = GetMesseprog("FRMDESCRIPTIF", "8")
Me.mnuPropSchema.Caption = GetMesseprog("FRMDESCRIPTIF", "5")
Me.mnuPrintSchema.Caption = GetMesseprog("FRMDESCRIPTIF", "6")

'-> Colonne des tables syst�mes
Me.ListView3.ColumnHeaders(1).Text = GetMesseprog("FRMEMILIE", "23")
Me.ListView3.ColumnHeaders(2).Text = GetMesseprog("FRMEMILIE", "24")

'-> Menu des tables systemes
Me.mnuAddSys.Caption = GetMesseprog("FRMDESCRIPTIF", "7")
Me.mnuDelSys.Caption = GetMesseprog("FRMDESCRIPTIF", "8")
Me.mnuPropSys.Caption = GetMesseprog("FRMDESCRIPTIF", "5")
Me.mnuPrintSys.Caption = GetMesseprog("FRMDESCRIPTIF", "6")

End Sub

Private Sub Form_Resize()

On Error Resume Next

Dim aRect As RECT

'-> R�cup�rer la taille de la zone cliente
GetClientRect Me.hwnd, aRect

'-> Redimensionner le ssatb
Me.SSTab1.Left = 0
Me.SSTab1.Top = 0
Me.SSTab1.Height = aRect.Bottom
Me.SSTab1.Width = aRect.Right
'-> Redimensionner le listView du catalogue
Me.ListView1.Left = 60
Me.ListView1.Top = 360
Me.ListView1.Height = Me.ScaleY(Me.SSTab1.Height, 3, 1) - Me.ListView1.Top - 60
Me.ListView1.Width = Me.ScaleX(Me.SSTab1.Width, 3, 1) - Me.ListView1.Left * 2
'-> Redimenssioner le listview du schema
Me.ListView2.Left = Me.ListView1.Left
Me.ListView2.Top = Me.ListView1.Top
Me.ListView2.Width = Me.ListView1.Width
Me.ListView2.Height = Me.ListView1.Height
'-> Redimensioner le listeview des tables syst�mes
Me.ListView3.Left = Me.ListView1.Left
Me.ListView3.Top = Me.ListView1.Top
Me.ListView3.Width = Me.ListView1.Width
Me.ListView3.Height = Me.ListView1.Height
End Sub

Private Sub DisplayCatalogue()

'---> Afficher le catalogue dans l'ordre alphab�tique

Dim aEmilieObj As Object
Dim aIdent As clsIdent
Dim ItemX As ListItem
Dim i As Long

'-> Get d'un pointeur vers le serveur DOG
Set aIdent = Idents(IdentKey)
'-> Vider le ListView
Me.ListView1.ListItems.Clear
'-> Bloquer la mise � jour
LockWindowUpdate Me.ListView1.hwnd
'-> Afficher la liste des codes dans l'odre alpha
For Each aEmilieObj In aIdent.ServeurDOG.Catalogue
    '-> Rendre la main � la CPU
    DoEvents
    '-> Ajouter le libelle
    Set ItemX = Me.ListView1.ListItems.Add(, "OBJ|" & aEmilieObj.Num_Field)
    'ItemX.SmallIcon = "OBJ"
    DisplayObjProp aEmilieObj, ItemX, aIdent
Next
'-> Justifier les entetes de colonne
FormatListView Me.ListView1
'-> Maj de l'interface
LockWindowUpdate 0
Me.ListView1.Refresh
        
End Sub

Private Sub DisplayObjProp(aEmilieObj As Object, ItemX As ListItem, aIdent As clsIdent)

Dim aFname As Object

'-> Code langue
aEmilieObj.CodeLangue = OpeCodeLangue
'-> Tri� de base sur le num_field
ItemX.Text = aEmilieObj.Num_Field
ItemX.SubItems(1) = aEmilieObj.Fill_Label
ItemX.SubItems(2) = aEmilieObj.DataType
ItemX.SubItems(3) = aEmilieObj.Progiciel
If aEmilieObj.MultiLangue <> 0 Then ItemX.SubItems(5) = Format(aEmilieObj.MultiLangue, "00")

If aEmilieObj.Table_Correspondance <> "" Then
    '-> Pointer sur la table associ�e
    Set aFname = aIdent.ServeurDOG.fNames(aIdent.Code & "|" & aEmilieObj.Table_Correspondance)
    aFname.CodeLangue = OpeCodeLangue
    ItemX.SubItems(4) = aEmilieObj.Table_Correspondance & " : " & aFname.Designation
End If

'-> S�parateur vectoriel
ItemX.SubItems(6) = aEmilieObj.Sep_Vecteur
    
End Sub

Private Sub DisplaySchema()

Dim aFname As Object
Dim aIdent As clsIdent
Dim ItemX As ListItem
Dim i As Long

'-> Get d'un pointeur vers le serveur DOG
Set aIdent = Idents(IdentKey)
                                        
'-> Vider le ListView
Me.ListView2.ListItems.Clear
'-> Bloquer la mise � jour
LockWindowUpdate Me.ListView2.hwnd
'-> Afficher la liste des codes dans l'odre alpha
For Each aFname In aIdent.ServeurDOG.fNames
    '-> Rendre la main � la CPU
    DoEvents
    '-> Ajouter le libelle
    Set ItemX = Me.ListView2.ListItems.Add(, "OBJ|" & aFname.Num_Fname)
    '-> Afficher ses propri�t�s
    DisplayFnamePropObj aFname, ItemX
Next
'-> Justifier en automatique les colonnes
FormatListView Me.ListView2
'-> D�bloquer l'�cran
LockWindowUpdate 0
Me.ListView2.Refresh
End Sub

Private Sub DisplayFnamePropObj(aFname As Object, ItemX As ListItem)

'-> Code langue
aFname.CodeLangue = OpeCodeLangue
'-> Code
ItemX.Text = aFname.Num_Fname
'-> Libelle
ItemX.SubItems(1) = aFname.Designation
'-> AccesDirect
If aFname.IndexDirect <> "" Then
    ItemX.SubItems(2) = "Yes"
Else
    ItemX.SubItems(2) = "No"
End If
'-> stat
If Not aFname.IsStat Then
    ItemX.SubItems(3) = "No"
Else
    ItemX.SubItems(3) = "Yes"
End If
'-> Replica
If Not aFname.IsReplica Then
    ItemX.SubItems(4) = "No"
Else
    ItemX.SubItems(4) = "Yes"
End If

End Sub
Private Sub DisplaySystemTable()

Dim x As ListItem
Dim i As Integer
Dim DefParam As String

'-> Get de la table system
strRetour = Idents(IdentKey).ServeurDOG.GetSystemTable

Me.ListView3.ListItems.Clear

'-> Quitter si pas de param�trage
If strRetour = "" Then Exit Sub

'-> Cr�ation des enregs
For i = 1 To NumEntries(strRetour, Chr(0))
    '-> Cr�er un nouvel enreg
    DefParam = Entry(i, strRetour, Chr(0))
    Set x = Me.ListView3.ListItems.Add(, "SYS|" & Entry(1, DefParam, "="), Entry(1, DefParam, "="))
    x.SubItems(1) = Entry(2, DefParam, "=")
Next

'-> Justifier les entetes de colonne
FormatListView Me.ListView3

End Sub


Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

If Button <> vbRightButton Then Exit Sub

'---> Afficher le menu contextuel
If Me.ListView1.ListItems.Count = 0 Then
    '-> Pas supprimer
    Me.mnuDelEmilie.Enabled = False
    '-> Pas Propri�t�s
    Me.mnuPropEmilie.Enabled = False
    '-> Pas imprimer
    Me.mnuPrintEmilie.Enabled = False
Else
    '-> ok supprimer
    Me.mnuDelEmilie.Enabled = True
    '-> ok Propri�t�s
    Me.mnuPropEmilie.Enabled = True
    '-> Pas imprimer
    Me.mnuPrintEmilie.Enabled = True
End If

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu Me.mnuCatalogue

'-> Traiter le retour
Select Case strRetour
    Case "ADD"
        AddEmilieObj
    Case "DEL"
        DeleteEmilieObj
    Case "PROP"
        DisplayEmilieProp
    Case "PRINT"
    
End Select

'-> Vider la variable d'�change
strRetour = ""

End Sub
Private Sub DeleteEmilieObj()

'---> Cette proc�dure supprime un objet du catalogue

Dim Rep As VbMsgBoxResult
Dim aEmilieObj As Object

'-> V�rifier qu'un objet soit s�lectionn�
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Pointer sur l'objet
Set aEmilieObj = Idents(IdentKey).ServeurDOG.Catalogue(Idents(IdentKey).Code & "|" & Me.ListView1.SelectedItem.Text)

'-> v�rifier qu'il n'y a pas de liens
If aEmilieObj.fNames.Count <> 0 Then
    MsgBox GetMesseprog("MESSAGE", "30"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "1")
    Exit Sub
End If

'-> Demander copnfirmation
Rep = MsgBox(GetMesseprog("MESSAGE", "31") & Chr(13) & Me.ListView1.SelectedItem.Text & " - " & Me.ListView1.SelectedItem.SubItems(1), vbExclamation + vbYesNo, GetMesseprog("COMMUN", "12"))
If Rep = vbNo Then Exit Sub

'-> Supprimer l'objet du catalogue
Idents(IdentKey).ServeurDOG.Catalogue.Remove (Idents(IdentKey).Code & "|" & Me.ListView1.SelectedItem.Text)

'-> Enregistrer le catalogue
Idents(IdentKey).ServeurDOG.Save_Catalogue

'-> Supprimer l'objet du listView
Me.ListView1.ListItems.Remove Me.ListView1.SelectedItem.Index

End Sub
Private Sub AddEmilieObj()

'-> Afficher la feuille de modif du catalogue
Load frmEmilieObj
Set frmEmilieObj.aIdent = Idents(IdentKey)
frmEmilieObj.Init ""
frmEmilieObj.Show vbModal

End Sub

Private Sub DisplayEmilieProp()

Dim aIdent As clsIdent
Dim aEmilieObj As Object

'-> Afficher la feuille
Set frmEmilieObj.aIdent = Idents(IdentKey)
frmEmilieObj.Init Entry(2, Me.ListView1.SelectedItem.Key, "|")
strRetour = ""
frmEmilieObj.Show vbModal

'-> Tester la variable de retour
If strRetour = "" Then Exit Sub

'-> pointer sur l'ident
Set aIdent = Idents(Me.IdentKey)
'-> Pointer sur l'objet du catalogue
Set aEmilieObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & Entry(2, Me.ListView1.SelectedItem.Key, "|"))
'-> Afficher la propri�t�
DisplayObjProp aEmilieObj, Me.ListView1.SelectedItem, aIdent

End Sub

Private Sub ListView1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

'-> Trier sur la colonne
Me.ListView1.SortKey = ColumnHeader.Index - 1
If Me.ListView1.SortOrder = lvwAscending Then
    Me.ListView1.SortOrder = lvwDescending
Else
    Me.ListView1.SortOrder = lvwAscending
End If
Me.ListView1.Sorted = True

End Sub

Private Sub ListView2_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

'-> Trier sur la colonne
Me.ListView2.SortKey = ColumnHeader.Index - 1
If Me.ListView2.SortOrder = lvwAscending Then
    Me.ListView2.SortOrder = lvwDescending
Else
    Me.ListView2.SortOrder = lvwAscending
End If
Me.ListView2.Sorted = True

End Sub

Private Sub ListView2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

If Button <> vbRightButton Then Exit Sub

'---> Afficher le menu contextuel
If Me.ListView2.ListItems.Count = 0 Then
    '-> Pas supprimer
    Me.mnuDelSchema.Enabled = False
    '-> Pas Propri�t�s
    Me.mnuPropSchema.Enabled = False
    '-> Pas imprimer
    Me.mnuPrintSchema.Enabled = False
Else
    '-> ok supprimer
    Me.mnuDelSchema.Enabled = True
    '-> ok Propri�t�s
    Me.mnuPropSchema.Enabled = True
    '-> Pas imprimer
    Me.mnuPrintSchema.Enabled = True
End If

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu Me.mnuSchema

'-> Traiter le retour
Select Case strRetour
    Case "ADD"
        AddFname
    Case "DEL"
        
    Case "PROP"
        DisplayFnameProp
    Case "PRINT"
    
End Select

'-> Vider la variable d'�change
strRetour = ""

End Sub
Private Sub AddFname()

'---> Cette proc�dure ajoute une table dans le sch�ma

Dim NumFname As String
Dim x As ListItem
Dim aFname As Fname

'-> R�cup�er le num�ro de la table
NumFname = Format(Idents(IdentKey).ServeurDOG.FnameCount, "0000")

'-> Enregistrer la nouvelle table
Idents(IdentKey).ServeurDOG.AddFname (NumFname)

'-> Enregistrer le catalogue
Idents(IdentKey).ServeurDOG.Save_Schema

'-> Ajouter un Enreg dans le listeView
Set x = Me.ListView2.ListItems.Add(, "OBJ|" & NumFname, NumFname)
x.Selected = True

'-> Editer ses propri�t�s
DisplayFnameProp

'-> Pointer sur le Fname sp�cifi�
Set aFname = Idents(IdentKey).ServeurDOG.fNames(Entry(2, IdentKey, "|") & "|" & NumFname)

'-> Code langue
aFname.CodeLangue = OpeCodeLangue

'-> Posiitonner son libell�
x.SubItems(1) = aFname.Designation

End Sub

Private Sub DisplayFnameProp()

Dim aFname As Object

'-> V�rifier qu'un objet soit s�lectionn�
If Me.ListView2.SelectedItem Is Nothing Then Exit Sub

'-> Charger la feuille en m�moire
Load frmGestSchema

'-> Charger les propri�t�s de cette table
frmGestSchema.Initialisation IdentKey, Entry(2, Me.ListView2.SelectedItem.Key, "|")

'-> Titre de la feuille
frmGestSchema.Caption = "Propri�t�s de la table : " & Entry(2, Me.ListView2.SelectedItem.Key, "|")

'-> Afficher la feuille
frmGestSchema.Show vbModal

'-> Pointer sur la table
Set aFname = Idents(Me.IdentKey).ServeurDOG.fNames(Entry(2, IdentKey, "|") & "|" & Entry(2, Me.ListView2.SelectedItem.Key, "|"))
'-> Afficher ses propri�t�s
DisplayFnamePropObj aFname, Me.ListView2.SelectedItem
    
End Sub




Private Sub mnuAddEmilie_Click()
strRetour = "ADD"
End Sub

Private Sub mnuAddSchema_Click()
strRetour = "ADD"
End Sub

Private Sub mnuAddSys_Click()
strRetour = "ADD"
End Sub

Private Sub mnuDelEmilie_Click()
strRetour = "DEL"
End Sub

Private Sub mnuDelSchema_Click()
strRetour = "DEL"
End Sub

Private Sub mnuDelSys_Click()
strRetour = "DEL"
End Sub

Private Sub mnuPrintEmilie_Click()
strRetour = "PRINT"
End Sub

Private Sub mnuPrintSchema_Click()
strRetour = "PRINT"
End Sub

Private Sub mnuPrintSys_Click()
strRetour = "PRINT"
End Sub

Private Sub mnuPropEmilie_Click()
strRetour = "PROP"
End Sub

Private Sub mnuPropschema_Click()
strRetour = "PROP"
End Sub

Private Sub mnuPropSys_Click()
strRetour = "PROP"
End Sub
