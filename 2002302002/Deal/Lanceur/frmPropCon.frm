VERSION 5.00
Begin VB.Form frmPropCon 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   6150
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   7950
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6150
   ScaleWidth      =   7950
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command5 
      Cancel          =   -1  'True
      Caption         =   "Command5"
      Height          =   375
      Left            =   4800
      TabIndex        =   23
      Top             =   6240
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   6135
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7935
      Begin VB.CommandButton Command6 
         Height          =   1215
         Left            =   2160
         Picture         =   "frmPropCon.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   4800
         Width           =   1335
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   8
         Left            =   2160
         TabIndex        =   21
         Top             =   4320
         Width           =   5655
      End
      Begin VB.CommandButton Command4 
         Height          =   1215
         Left            =   720
         Picture         =   "frmPropCon.frx":1CCA
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   4800
         Width           =   1335
      End
      Begin VB.CommandButton Command3 
         Height          =   1215
         Left            =   3600
         Picture         =   "frmPropCon.frx":3994
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   4800
         Width           =   1335
      End
      Begin VB.CommandButton Command2 
         Height          =   1215
         Left            =   5040
         Picture         =   "frmPropCon.frx":565E
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   4800
         Width           =   1335
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   7
         Left            =   2160
         TabIndex        =   16
         Top             =   3840
         Width           =   5655
      End
      Begin VB.CommandButton Command1 
         Height          =   1215
         Left            =   6480
         Picture         =   "frmPropCon.frx":7328
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   4800
         Width           =   1335
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   6
         Left            =   2160
         TabIndex        =   14
         Top             =   3360
         Width           =   5655
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   5
         Left            =   2160
         TabIndex        =   13
         Top             =   2880
         Width           =   5655
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   4
         Left            =   2160
         TabIndex        =   12
         Top             =   2400
         Width           =   5655
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   3
         Left            =   2160
         TabIndex        =   11
         Top             =   1920
         Width           =   5655
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   2
         Left            =   2160
         TabIndex        =   10
         Top             =   1440
         Width           =   5655
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   1
         Left            =   2160
         TabIndex        =   9
         Top             =   960
         Width           =   5655
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   0
         Left            =   2160
         TabIndex        =   8
         Top             =   480
         Width           =   5655
      End
      Begin VB.Label Label9 
         Caption         =   "Cryptage : "
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   4320
         Width           =   1335
      End
      Begin VB.Label Label8 
         Caption         =   "Mode debug : "
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   3840
         Width           =   1335
      End
      Begin VB.Label Label7 
         Caption         =   "Mode debug : "
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   3360
         Width           =   1335
      End
      Begin VB.Label Label6 
         Caption         =   "Constructeur d'index : "
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   2880
         Width           =   1935
      End
      Begin VB.Label Label5 
         Caption         =   "Code de l'agent : "
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   2400
         Width           =   1575
      End
      Begin VB.Label Label4 
         Caption         =   "Date de connexion : "
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   1920
         Width           =   2055
      End
      Begin VB.Label Label3 
         Caption         =   "Fichier R�ponse : "
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   1440
         Width           =   1575
      End
      Begin VB.Label Label2 
         Caption         =   "Fichier Demande : "
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "Op�rateur : "
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   480
         Width           =   975
      End
   End
End
Attribute VB_Name = "frmPropCon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim aCon As clsConnectConsole

Private Sub Command1_Click()

Dim Rep As VbMsgBoxResult
Dim aIdent As clsIdent

'-> Ne pouvoir deconnecter que si la connexion est une connexion console
If aCon.IsRunConsole Then
    '-> Demander confirmation
    Rep = MsgBox(GetMesseprog("MESSAGE", "25") & Format(aCon.IndexAgent, "0000"), vbExclamation + vbYesNo, GetMesseprog("COMMUN", "12"))
    If Rep = vbNo Then Exit Sub
    '-> V�rifier que celui-ci ne soit pas en train de travailler
    If aCon.IsWorking Then
        '-> Message d'erreur
        MsgBox GetMesseprog("MESSAGE", "26"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "11")
        Exit Sub
    End If
    
    '-> Bloquer l'�cran
    Me.Enabled = False
    Screen.MousePointer = 11
    
    '-> Pointer sur l'ident associ�
    Set aIdent = Idents("IDENT|" & aCon.Ident)
    
    '-> Lancer une demande de d�connexion
    UnloadAgent aCon, aIdent
        
    '-> Quitter la feuille
    Unload Me
    
    '-> Repositionner le pointeur
    Screen.MousePointer = 0
Else
    '-> Indiquer que l'on ne peut pas d�connecter un agent qui n'est pas console
    MsgBox GetMesseprog("MESSAGE", "27"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "11")
End If


End Sub

Private Sub Command2_Click()

'---> Cette proc�dure permet de basculer en mode Crypt ou Uncrypt

Dim MyCon As B3DConnexion

On Error GoTo GestError

'-> S�lectioner une agent
strRetour = ""
frmChxAgent.Ident = aCon.Ident
frmChxAgent.Show vbModal

'-> Pointer sur la connexion
If strRetour = "" Then Exit Sub
Set MyCon = Connexions(strRetour)

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> Demande de crypt ou non
If aCon.CryptMode Then
    CryptExchange MyCon, "0"
Else
    CryptExchange MyCon, "1"
End If

'-> Mettre � jo�ur l'affichage
aCon.CryptMode = Not aCon.CryptMode
Me.Text1(8).Text = aCon.CryptMode

GestError:
    Me.Enabled = True
    Screen.MousePointer = 0

End Sub

Private Sub Command3_Click()

'---> Cette proc�dure permet de basculer en mode trace ou non

Dim MyCon As B3DConnexion

On Error GoTo GestError

'-> S�lectioner une agent
strRetour = ""
frmChxAgent.Ident = aCon.Ident
frmChxAgent.Show vbModal

'-> Pointer sur la connexion
If strRetour = "" Then Exit Sub
Set MyCon = Connexions(strRetour)

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> Demande de crypt ou non
If aCon.TraceMode Then
    SetModeTrace MyCon, "0"
Else
    SetModeTrace MyCon, "1"
End If

'-> Mettre � jo�ur l'affichage
aCon.TraceMode = Not aCon.TraceMode
Me.Text1(7).Text = aCon.TraceMode

GestError:
    Me.Enabled = True
    Screen.MousePointer = 0


End Sub

Private Sub Command4_Click()

'---> Cette proc�dure permet de basculer en mode debug ou non

Dim MyCon As B3DConnexion

On Error GoTo GestError

'-> S�lectioner une agent
strRetour = ""
frmChxAgent.Ident = aCon.Ident
frmChxAgent.Show vbModal

'-> Pointer sur la connexion
If strRetour = "" Then Exit Sub
Set MyCon = Connexions(strRetour)

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> Demande de crypt ou non
LoadAgentDOG MyCon

GestError:
    Me.Enabled = True
    Screen.MousePointer = 0


End Sub

Private Sub Command5_Click()
Unload Me
End Sub

Private Sub Command6_Click()

'---> Cette proc�dure permet de basculer en mode debug ou non

Dim MyCon As B3DConnexion

On Error GoTo GestError

'-> S�lectioner une agent
strRetour = ""
frmChxAgent.Ident = aCon.Ident
frmChxAgent.Show vbModal

'-> Pointer sur la connexion
If strRetour = "" Then Exit Sub
Set MyCon = Connexions(strRetour)

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> Demande de crypt ou non
If aCon.DebugMode Then
    SetModeDebug MyCon, "0"
Else
    SetModeDebug MyCon, "1"
End If

'-> Mettre � jo�ur l'affichage
aCon.DebugMode = Not aCon.DebugMode
Me.Text1(6).Text = aCon.DebugMode

GestError:
    Me.Enabled = True
    Screen.MousePointer = 0

End Sub

Private Sub Form_Load()

'---> Titre de la feuille
Me.Caption = GetMesseprog("CONNECT", "8")
Me.Frame1.Caption = GetMesseprog("COMMUN", "14")

Me.Label1.Caption = GetMesseprog("PORTAILB3D", "7")
Me.Label2.Caption = GetMesseprog("CONNECT", "1")
Me.Label3.Caption = GetMesseprog("CONNECT", "2")
Me.Label4.Caption = GetMesseprog("CONNECT", "3")
Me.Label5.Caption = GetMesseprog("CONNECT", "4")
Me.Label6.Caption = GetMesseprog("CONNECT", "5")
Me.Label7.Caption = GetMesseprog("CONNECT", "6")
Me.Label8.Caption = GetMesseprog("NEWCONNECT", "4")
Me.Command1.Caption = GetMesseprog("CONNECT", "7")
Me.Command2.Caption = GetMesseprog("CONNECT", "9")
Me.Command3.Caption = GetMesseprog("CONNECT", "10")
Me.Command4.Caption = GetMesseprog("CONNECT", "11")
Me.Command6.Caption = GetMesseprog("CONNECT", "12")


End Sub

Public Sub Init(Connexion As String)
'---> Afficher les propri�t�s de la connexion
'-> Pointer sur la connexion
Set aCon = ConnexionsDescripteur(Connexion)
'-> Affichage des propri�t�s
Me.Text1(0) = aCon.Operat
Me.Text1(1) = aCon.DmdFile
Me.Text1(2).Text = aCon.RspFile
Me.Text1(3).Text = aCon.DateConnexion
Me.Text1(4).Text = Format(aCon.IndexAgent, "0000")
Me.Text1(5).Text = aCon.Num_Idx
Me.Text1(6).Text = aCon.DebugMode
Me.Text1(7).Text = aCon.TraceMode
Me.Text1(8).Text = aCon.CryptMode
Me.Caption = Me.Caption & Format(aCon.IndexAgent, "0000")
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)

KeyAscii = 0

End Sub
