VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsIdent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Module de classe d'un identifiant

Public Code As String '-> Code de l'ident
Public Libel As String '-> Libell� de l'ident
Public FichierCatalogue As String '-> Fichier ascii de load du catalogue
Public FichierSchema As String '-> Fichier ascii de liad du DF
Public FichierLink As String '->  Liens entre les deux fichiers
Public Fname_Count As Long '-> Nombre de table dans le DF
Public Emilie_Count As Long '-> Nombre de champ dans le catalogue
Public Base As String '-> Base associ�e
Public ServeurDOG As clsDOG '-> Pointeur vers le serveur DOG associ�
Public IsRunning As Boolean '-> Indique que le serveur est en cours
Public When As Date '-> Date d'arret ou de d�mmarrage
Public Limite_AgentB3D As Integer '-> Nombre de connexion maxi par ident
Public HdlForm As Long '-> Handle de visualisation de l'identifiant
Public NbConnect As Long '-> Indique le nombre de connexions actuelle au serveur
Public IsEnding As Boolean '-> Indiquer que l'on est en train de fermer
Public ErrorLoading As Integer '-> Indique  le code de l'erreur au chargement
Public hdlCom As Long '-> Handme de communication interne � la console
Public ApplicationPath As String '-> Path des applicatifs
