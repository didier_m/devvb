VERSION 5.00
Begin VB.Form frmPropIfield 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   4740
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4620
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4740
   ScaleWidth      =   4620
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Height          =   495
      Left            =   3600
      Picture         =   "frmPropIfield.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   4200
      Width           =   975
   End
   Begin VB.CommandButton Command3 
      Height          =   495
      Left            =   2520
      Picture         =   "frmPropIfield.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   4200
      Width           =   975
   End
   Begin VB.CheckBox Check2 
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   1440
      Width           =   2175
   End
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      Height          =   2655
      Left            =   0
      TabIndex        =   2
      Top             =   1440
      Width           =   4575
      Begin VB.TextBox Text2 
         Enabled         =   0   'False
         Height          =   285
         Left            =   1200
         TabIndex        =   5
         Top             =   2280
         Width           =   3255
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   285
         Left            =   1200
         TabIndex        =   4
         Top             =   1920
         Width           =   3255
      End
      Begin VB.ListBox List1 
         Enabled         =   0   'False
         Height          =   1425
         ItemData        =   "frmPropIfield.frx":0E14
         Left            =   120
         List            =   "frmPropIfield.frx":0E16
         TabIndex        =   3
         Top             =   360
         Width           =   4335
      End
      Begin VB.Label Label6 
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   2280
         Width           =   1215
      End
      Begin VB.Label Label5 
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1920
         Width           =   1215
      End
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   960
      Width           =   1095
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   1440
      TabIndex        =   9
      Top             =   480
      Width           =   3135
   End
   Begin VB.Label Label3 
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   3000
      TabIndex        =   7
      Top             =   120
      Width           =   1575
   End
   Begin VB.Label Label1 
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "frmPropIfield"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public iField As Object


Public Sub Init()

'-> Titre de la feuille
Me.Caption = "Propri�t�s du champ : " & iField.Num_Field

'-> Cumul
Me.Check1.Value = Abs(CInt(iField.cumul))

'-> Si condtion
If iField.typecondition = 0 Then
    '-> Pas de condition
    Me.Check2.Value = 0
Else
    Me.Check2.Value = 1
    '-> Condition
    Me.List1.ListIndex = iField.typecondition - 1
    '-> Val Mini
    Me.Text1.Text = iField.valmini
    '-> Val maxi
    Me.Text2.Text = iField.valmaxi
    
End If


End Sub

Private Sub Check2_Click()

'-> Condition ou non
If Me.Check2.Value = 1 Then
    '-> Rendre la liste Ok
    Me.List1.Enabled = True
    Me.Frame1.Enabled = True
    Me.Label5.Enabled = True
    Me.Label6.Enabled = True
    Me.Text1.Enabled = True
    Me.Text2.Enabled = True
    Me.List1.Selected(0) = True
Else
    '-> Bloquer la liste
    Me.List1.Enabled = False
    Me.Frame1.Enabled = False
    Me.Label5.Enabled = False
    Me.Label6.Enabled = False
    Me.Text1.Enabled = False
    Me.Text2.Enabled = False
End If

'-> Vider dans les 2 cas les zones
Me.Text1.Text = ""
Me.Text2.Text = ""


End Sub

Private Sub Command1_Click()


'-> Mettre � jour l'objet
If Me.Check1.Value = 1 Then
    iField.cumul = True
Else
    iField.cumul = False
End If

'-> SI index
If Me.Check2.Value = 0 Then
    '-> Pas de condition
    iField.typecondition = 0
    iField.valmini = ""
    iField.valmaxi = ""
Else
    '-> Condition
    iField.typecondition = Me.List1.ListIndex + 1
    iField.valmini = Me.Text1.Text
    If Me.List1.ListIndex = 1 Or Me.List1.ListIndex = 2 Then
        iField.valmaxi = Me.Text2.Text
    Else
        iField.valmaxi = ""
    End If
End If

strRetour = "OK"
Unload Me


End Sub

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("FRMSCHEMA", "31")

'-> Propri�t�s
Me.Label1.Caption = GetMesseprog("FRMEMILIE", "16")
Me.Label3.Caption = GetMesseprog("COMMUN", "22")
Me.Check1.Caption = GetMesseprog("FRMSCHEMA", "21")
Me.Check2.Caption = GetMesseprog("FRMIFIELD", "1")
Me.Label5.Caption = GetMesseprog("FRMIFIELD", "2")
Me.Label6.Caption = GetMesseprog("FRMIFIELD", "3")

End Sub

Private Sub Text1_GotFocus()
SelectTxtBox Me.Text1
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then Me.Text2.SetFocus
End Sub

Private Sub Text2_GotFocus()
SelectTxtBox Me.Text2
End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then Me.Command1.SetFocus
End Sub

