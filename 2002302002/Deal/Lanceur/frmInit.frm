VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmInit 
   BorderStyle     =   0  'None
   ClientHeight    =   6270
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   9885
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmInit.frx":0000
   ScaleHeight     =   6270
   ScaleWidth      =   9885
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Height          =   2415
      Left            =   1560
      ScaleHeight     =   2355
      ScaleWidth      =   1755
      TabIndex        =   5
      Top             =   2760
      Width           =   1815
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   4335
      Left            =   3600
      TabIndex        =   4
      Top             =   1800
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   7646
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H00FFC0C0&
      Height          =   285
      Left            =   1560
      TabIndex        =   0
      Top             =   1800
      Width           =   1815
   End
   Begin VB.TextBox Text2 
      BackColor       =   &H00FFC0C0&
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1560
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   2280
      Width           =   1815
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Login : "
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   600
      TabIndex        =   3
      Top             =   1800
      Width           =   735
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Password :"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   600
      TabIndex        =   2
      Top             =   2280
      Width           =   855
   End
End
Attribute VB_Name = "frmInit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim Essai As Integer
Dim UnloadM As Boolean

Private Sub Form_KeyPress(KeyAscii As Integer)
If KeyAscii = 27 Then Unload Me
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

Dim Rep As VbMsgBoxResult

If UnloadM Then Exit Sub

'-> Demander confirmation
Rep = MsgBox("Quitter maintenant", vbQuestion + vbYesNo, "Deal Informatique")
If Rep = vbNo Then
    Cancel = 1
Else
    End
End If

End Sub
Private Sub Form_Load()
LoadInit
End Sub

Private Sub Text1_GotFocus()
SelectTxtBox Me.Text1
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then Me.Text2.SetFocus
End Sub

Private Sub Text2_GotFocus()
SelectTxtBox Me.Text2
End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then
    '-> V�rifier le login et le mot de passe
    If Trim(Me.Text1.Text) = "" Then
        MsgBox "Invalid login", vbExclamation + vbOKOnly, "Error"
        Me.Text1.SetFocus
        Exit Sub
    End If
    If Trim(Me.Text2.Text) = "" Then
        MsgBox "Invalid password", vbExclamation + vbOKOnly, "Error"
        Me.Text2.SetFocus
        Exit Sub
    End If
    '-> Incr�menter le compteur d'essai
    Essai = Essai + 1
    '-> V�rifier le mot de passe
    If Not CheckPassWord(Me.Text1.Text, Me.Text2.Text) Then
        MsgBox "Acces Error", vbCritical + vbOKOnly, "Error"
        If Essai = 3 Then End
        Me.Text2.Text = ""
        Me.Text2.SetFocus
        Exit Sub
    End If
    '-> V�rifier la date de validit�
    If Not CheckDateValiditeOperat(Me.Text1.Text) Then
        MsgBox "Votre compte est expir�. Veuillez consulter votre administrateur syst�me.", vbCritical + vbOKOnly, "Error"
        If Essai = 3 Then End
        Me.Text2.Text = ""
        Me.Text2.SetFocus
        Exit Sub
    End If
    '-> Mettre � jour les variables globales de l'op�rateur
    OpeConsole = Trim(UCase$(Me.Text1.Text))
    OpePassWord = Me.Text2.Text
    '-> D�charger la feuille
    UnloadM = True
    Unload Me
    
End If

End Sub

