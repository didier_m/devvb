Attribute VB_Name = "fctRunB3D"
Option Explicit

'-> D�claration des API
Public Type POINTAPI
    X As Long
    y As Long
End Type
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function ScreenToClient Lib "user32" (ByVal hwnd As Long, lpPoint As POINTAPI) As Long
Public Declare Function IsWindow Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function SendMessage& Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any)
Public Declare Function SetWindowText& Lib "user32" Alias "SetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String)
Public Declare Function GetWindowText& Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String, ByVal cch As Long)

'-> Pour SendMessage
Public Const WM_PAINT& = &HF
Public Const LVM_SETCOLUMNWIDTH = 4126 '( LVM_FIRST + 30 )

'-> Dans la cas d'une version MonoPoste , indique si la session est active
Public IsRunningSession As Boolean

'-> Matrice des type de base [Type] + "=" + [Libel]
Public m_TypeBase() As String
'-> Matrice d'indexation des agents B3D et des IDX
Public Matrice_Num_Agent_B3D(1 To 5000) As Boolean '-> Matrice des numeros des agents
Public Matrice_Num_Agent_IDX(5001 To 9999) As Boolean '-> Matrice des numeros des index

'-> Matrice de communication
Public Matrice_Com_Idx(5001 To 9999) As Boolean    '-> Pour handle de communication
Public Matrice_Com_Agent(1 To 5000) As Boolean    '-> Pour handle de communication

'-> Collection des idents
Public Idents As Collection
'-> Collection des descripteurs de connexions
Public ConnexionsDescripteur As Collection
'-> Collection des connexions
Public Connexions As Collection
'-> Collection des bases
Public Bases As Collection

'-> Structure de param�trage de la console
Private Type Console
    IsRunning As Boolean
    Date As String
    IsVible As Boolean
End Type

'-> Variables de lancement de la session
Private IsServeurSessionRunning As Boolean
Private IsIdxbuildSessionRunning As Boolean
Private IsAgentB3DSessionRunning As Boolean

'-> Variable de fin de session
Public EndSessionMode As Boolean

'-> Impl�mentation de la console
Public lpsConsole As Console


Public Function Get_Free_Agent(Agent_B3D As Boolean) As String

Dim i As Integer

If Agent_B3D Then
    For i = LBound(Matrice_Num_Agent_B3D()) To UBound(Matrice_Num_Agent_B3D())
        If Matrice_Num_Agent_B3D(i) = False Then
            Get_Free_Agent = Format(i, "0000")
            Matrice_Num_Agent_B3D(i) = True
            Exit Function
        End If
    Next
Else
    For i = LBound(Matrice_Num_Agent_IDX()) To UBound(Matrice_Num_Agent_IDX())
        If Matrice_Num_Agent_IDX(i) = False Then
            Get_Free_Agent = Format(i, "0000")
            Matrice_Num_Agent_IDX(i) = True
            Exit Function
        End If
    Next
End If

End Function





Public Sub DrawLoadding(picOk As PictureBox, TailleLue As Long, TailleTotale As Long)

'---> Cette proc�dure  dessine une barre de temporisation

DoEvents
picOk.Line (0, 0)-((TailleLue / TailleTotale) * picOk.ScaleWidth, picOk.ScaleHeight), &H800000, BF


End Sub
Public Sub Main()

'-> Charger la feuille de gestion des modules de communication
Load frmCom

'-> Afficher la feuille MDI
mdiConsole.Show

'-> Afficher la feuille Init
frmInit.Show vbModal
DoEvents

'-> V�rifier si on trouve les fichiers Langues
If Dir$(App.Path & "\Param\Messprog-" & Format(OpeCodeLangue, "00") & ".ini") = "" Then
    MsgBox "Impossible de trouver le fichier langue de l'op�rateur.", vbCritical + vbOKOnly, "Erreur fatale"
    End
Else
    MessprogIniFile = App.Path & "\Param\Messprog-" & Format(OpeCodeLangue, "00") & ".ini"
End If

'-> Message dans la barre d'�tat
mdiConsole.StatusBar1.Panels(1).Text = GetMesseprog("MESSAGE", "40")
DoEvents

'-> Charger le menu de la console
LoadMenuConsole

'-> Charger les bases
LoadBases

'-> Charger les idents
LoadIdent

'-> Initialiser la collection des descripteurs de connexions
Set ConnexionsDescripteur = New Collection

'-> Initialiser la collection des connexions
Set Connexions = New Collection

'-> Initialiser les sessions si on est en mode MONOPOSTE
If ProductVersion = "MONOPOSTE" Then RunSession
    
'-> Message dans la barre d'�tat
mdiConsole.StatusBar1.Panels(1).Text = ""
DoEvents

End Sub

Private Function InitTypeBase() As Boolean

'---> Cette fonction charge une matrice avec les divers types de bases support�s
Dim res As Long
Dim lpBuffer As String
Dim i As Integer

'-> Ecraser la matrice des bases
Erase m_TypeBase()

'-> Recup�ration de tous les type de base
lpBuffer = Space$(2000)
res = GetPrivateProfileSection("TYPE_BASE", lpBuffer, Len(lpBuffer), BASEIniFile)

'-> Cr�ation de la matrice des type de bases
For i = 1 To NumEntries(lpBuffer, Chr(0))
    '-> Quitter si entry= ""
    If Trim(Entry(i, lpBuffer, Chr(0))) = "" Then Exit For
    '-> Redimensionner la matrice
    ReDim Preserve m_TypeBase(i)
    '-> Setting des informations
    m_TypeBase(i) = Entry(i, lpBuffer, Chr(0))
Next 'Pour tous les types de bases

End Function
Private Sub LoadBases()

'---> Cette proc�dure charge la liste des bases

'---> Cette fonction charge cr�er la collection des bases de donn�es qui se trouvent dans le fichier Bases.ini
Dim res As Long
Dim lpBuffer As String
Dim lpBase As String
Dim i As Integer
Dim NomBase As String
Dim aBase As clsBase

'-> Initialiser la collection des bases
Set Bases = New Collection

'-> Initialisation des types de bases
InitTypeBase

'-> R�cup�ration de la liste des bases dans le fichier
lpBuffer = Space$(2000)
res = GetPrivateProfileString(vbNullString, "", "NOT_FOUND", lpBuffer, Len(lpBuffer), BASEIniFile)
For i = 1 To NumEntries(lpBuffer, Chr(0))
    '-> Get du nom de la base
    NomBase = Trim(Entry(i, lpBuffer, Chr(0)))
    '-> Tester si <> ""
    If NomBase <> "" And UCase$(NomBase) <> "TYPE_BASE" Then
        '-> Cr�ation d'une instance de la classe base
        Set aBase = New clsBase
        
        '-> Nom de la base
        aBase.Nom_Base = DeCrypt(GetIniFileValue(NomBase, "NOM", BASEIniFile))
        
        '-> Serveur de la base
        aBase.Serveur = DeCrypt(GetIniFileValue(NomBase, "SERVEUR", BASEIniFile))
        
        '-> Type de la base
        aBase.Type_Base = DeCrypt(GetIniFileValue(NomBase, "TYPE", BASEIniFile))
        
        '-> User de la base
        aBase.User = DeCrypt(GetIniFileValue(NomBase, "USER", BASEIniFile))
        
        '-> Mot de passe de la base
        aBase.Pwd = DeCrypt(GetIniFileValue(NomBase, "PWD", BASEIniFile))
        
        '-> Temporisation de la base
        aBase.Tempo_Lock = DeCrypt(GetIniFileValue(NomBase, "TEMPO_LOCK", BASEIniFile))
        
        '-> Nombre d'agents IDX par base
        aBase.Nb_Idx = DeCrypt(GetIniFileValue(NomBase, "IDX", BASEIniFile))
        
        '-> Fichier associ� � la base
        aBase.Fichier = DeCrypt(GetIniFileValue(NomBase, "FILE", BASEIniFile))
        aBase.Fichier = Replace(aBase.Fichier, "$APP$", App.Path)
        
        '-> Cl� d'acces � la base dans le fichier INI
        aBase.Key = NomBase
        
        '-> Ajouter l'objet dans la collection
        Bases.Add aBase, UCase$(UCase$(aBase.Nom_Base))
        
    End If 'Si nom de la base renseign�e
Next 'Pour toutes les entrees dans le fichier



End Sub

Private Sub LoadIdent()

'---> Cette proc�dure charge les param�tres d'une ident

Dim ListeSection As String
Dim lpSection As String
Dim aIdent As clsIdent
Dim lpBuffer As String
Dim i As Integer
Dim res As Long
Dim aNode As Node
Dim ErrorCode As Integer

'-> Initialiser la collection des idents
Set Idents = New Collection

'-> Charger La liste des idents
ListeSection = Space$(2000)
res = GetPrivateProfileSectionNames(ListeSection, Len(ListeSection), IdentIniFile)

'-> Analyse de toutes les sections
For i = 1 To NumEntries(ListeSection, Chr(0))
    '-> Get d'une section
    lpSection = Entry(i, ListeSection, Chr(0))
    '-> Quitter si ""
    If Trim(lpSection) = "" Then Exit For
    
    '-< Ne pas traiter les groupes
    If UCase$(Trim(Entry(1, lpSection, "_"))) = "GROUPE" Then GoTo NextIdent
    
    '-> Initialiser un nouvel Ident
    Set aIdent = New clsIdent
    
    '-> Code de l'ident
    lpBuffer = DeCrypt(GetIniFileValue(lpSection, "CODE", IdentIniFile))
    If lpBuffer = "" Then GoTo BadIdent
    aIdent.Code = lpBuffer
    
    '-> Ajouter l'ident dans la collection
    Idents.Add aIdent, "IDENT|" & aIdent.Code
    
    '-> Libel de l'ident
    lpBuffer = DeCrypt(GetIniFileValue(lpSection, "LIBEL", IdentIniFile))
    aIdent.Libel = lpBuffer
    
    '-> Fichier catalogue
    ErrorCode = 3
    lpBuffer = DeCrypt(GetIniFileValue(lpSection, "FICHIERCATALOGUE", IdentIniFile))
    If lpBuffer = "" Then GoTo BadIdent
    aIdent.FichierCatalogue = App.Path & "\Serveur B3D\" & lpBuffer
    
    '-> Fichier Sch�ma B3D
    ErrorCode = 4
    lpBuffer = DeCrypt(GetIniFileValue(lpSection, "FICHIERSCHEMA", IdentIniFile))
    If lpBuffer = "" Then GoTo BadIdent
    aIdent.FichierSchema = App.Path & "\Serveur B3D\" & lpBuffer
    
    '-> Fichier des liens
    ErrorCode = 5
    lpBuffer = DeCrypt(GetIniFileValue(lpSection, "FICHIERLINK", IdentIniFile))
    If lpBuffer = "" Then GoTo BadIdent
    aIdent.FichierLink = App.Path & "\Serveur B3D\" & lpBuffer
    
    '-> Recup�rer le nombre d'objets EMilie
    ErrorCode = 7
    lpBuffer = DeCrypt(GetIniFileValue(lpSection, "EMILIE_COUNT", IdentIniFile))
    If lpBuffer = "" Then GoTo BadIdent
    aIdent.Emilie_Count = CInt(lpBuffer)
    
    '-> R�cup�rer le nombre de tables associ�es
    ErrorCode = 8
    lpBuffer = DeCrypt(GetIniFileValue(lpSection, "FNAME_COUNT", IdentIniFile))
    If lpBuffer = "" Then GoTo BadIdent
    aIdent.Fname_Count = CInt(lpBuffer)
    
    '-> R�cup�rer la base associ�e
    ErrorCode = 9
    lpBuffer = DeCrypt(GetIniFileValue(lpSection, "BASE", IdentIniFile))
    If lpBuffer = "" Then GoTo BadIdent
    aIdent.Base = lpBuffer

    '-> Chargement du nombre maxi de connexions B3D
    ErrorCode = 10
    lpBuffer = DeCrypt(GetIniFileValue(lpSection, "ACCES_DEAL", IdentIniFile))
    If lpBuffer = "" Then GoTo BadIdent
    aIdent.Limite_AgentB3D = CInt(lpBuffer)
        
    '-> Chargement du path applicatif
    aIdent.ApplicationPath = Replace(DeCrypt(GetIniFileValue("DIRECTORY", "APPLICATIONS", B3DIniFile)), "$APP$", App.Path)
        
    '-> Ajouter un node dans la console si on n'est pas en version
    If ProductVersion = "SERVEUR" Then
        '-> Ajouter un node
        Set aNode = mdiConsole.TreeNaviga.Nodes.Add("NODESERVEURIDENT", 4, "IDENT|" & aIdent.Code, aIdent.Code & " - " & aIdent.Libel, "REINDEX")
        aNode.Tag = "IDENT"
    End If 'Si on doit ajouter un node
NextIdent:
Next 'Pour tous les idents

Exit Sub

BadIdent:
    aIdent.ErrorLoading = ErrorCode
    

End Sub

Private Sub LoadMenuConsole()

'---> Cette proc�dure cr�er le menu de la console

Dim aNode As Node
Dim lpBuffer As String

'-> Son est en version Monoposte -> D�marrer une session
If ProductVersion <> "MONOPOSTE" Then
    '-> Ajouter le node session
    Set aNode = mdiConsole.TreeNaviga.Nodes.Add(, , "NODESESSION", GetMesseprog("MENU", "21"), "CLOSE")
    aNode.ExpandedImage = "OPEN"
    aNode.Expanded = True
    '-> Ajouter le node du serveur d'idents
    Set aNode = mdiConsole.TreeNaviga.Nodes.Add("NODESESSION", 4, "NODESERVEURIDENT", GetMesseprog("MENU", "19"), "IDENT")
    '-> Ajouter le node du serveur de connexion
    Set aNode = mdiConsole.TreeNaviga.Nodes.Add("NODESESSION", 4, "NODEPORTAILB3D", GetMesseprog("MENU", "4"), "PORTAILB3D")
    aNode.Tag = "PORTAILB3D"
Else
    '-> Ajouter le node de modification du mot de passe
    Set aNode = mdiConsole.TreeNaviga.Nodes.Add(, , "NODEPASSWORD", GetMesseprog("MENU", "20"), "REINDEX")
    aNode.Tag = "MODIFPASSWORD"
End If

'-> Ajouter le node Applicatifs DEAL
Set aNode = mdiConsole.TreeNaviga.Nodes.Add(, , "NODEAPPLICATIFS", GetMesseprog("MENU", "5"), "CLOSE")
aNode.ExpandedImage = "OPEN"
aNode.Expanded = True

'-> Ajouter le node d'export des donn�es
Set aNode = mdiConsole.TreeNaviga.Nodes.Add(, , "NODEIMPORTEXPORT", GetMesseprog("MENU", "8"), "CLOSE")
aNode.ExpandedImage = "OPEN"
aNode.Expanded = True

'-> Doit-on ajouter le node R�plication
If IsReplication Then
    '-> Ajouter le node de r�plication
    Set aNode = mdiConsole.TreeNaviga.Nodes.Add("NODEIMPORTEXPORT", 4, "NODEREPLICATION", GetMesseprog("MENU", "9"), "REPLICA")
    '-> Ajouter le node d'import des donn�es
    Set aNode = mdiConsole.TreeNaviga.Nodes.Add("NODEREPLICATION", 4, "NODEREPLICATIONIN", GetMesseprog("MENU", "10"), "REPLICAIN")
    aNode.Tag = "REPLICAIN"
    '-> Ajouter le node d'export des donn�es
    Set aNode = mdiConsole.TreeNaviga.Nodes.Add("NODEREPLICATION", 4, "NODEREPLICATIONOUT", GetMesseprog("MENU", "11"), "REPLICAOUT")
    aNode.Tag = "REPLICAOUT"
    '-> Ajouter le node d'acc�s au serveur de r�plication si version Serveur
    If ProductVersion = "SERVEUR" Then
        Set aNode = mdiConsole.TreeNaviga.Nodes.Add("NODEREPLICATION", 4, "NODEREPLICATIONGEST", GetMesseprog("MENU", "12"), "PARAM")
        aNode.Tag = "REPLICAGEST"
    End If 'Si on est en mode serveur
End If 'Si on a acc�s � la r�plication

'-> Ajouter la gestion des interfaces
Set aNode = mdiConsole.TreeNaviga.Nodes.Add("NODEIMPORTEXPORT", 4, "NODEINTERFACE", GetMesseprog("MENU", "13"), "INT")
'-> Ajouter le node d'import des donn�es
Set aNode = mdiConsole.TreeNaviga.Nodes.Add("NODEINTERFACE", 4, "NODEINTIN", GetMesseprog("MENU", "10"), "INTIN")
aNode.Tag = "INTIN"
'-> Ajouter le node d'export des donn�es
Set aNode = mdiConsole.TreeNaviga.Nodes.Add("NODEINTERFACE", 4, "NODEINTOUT", GetMesseprog("MENU", "11"), "INTOUT")
aNode.Tag = "INTOUT"

'-> Ajouter l'import Export d'une base
Set aNode = mdiConsole.TreeNaviga.Nodes.Add("NODEIMPORTEXPORT", 4, "NODEEXPORTBASE", GetMesseprog("MENU", "18"), "INT")
aNode.Tag = "EXPORTBASE"

'-> Charger le menu applicatif
LoadMenuApp

End Sub
Private Sub LoadMenuApp()

'---> Cette proc�dure charge le fichier Ini du menu du code langue de l'op�rateur

Dim lpBuffer As String
Dim MenuPath As String
Dim lpMenu As String
Dim i As Integer
Dim Menu As String
Dim Lib As String
Dim NodeRef As Node
Dim Node As Node
Dim NiveauCours As String
Dim Param As String
Dim Programme As String

'-> Setting du path du fichier ini
MenuPath = App.Path & "\param\menu-" & Format(OpeCodeLangue, "00") & ".ini"

'-> V�rifier que l'on trouve le fichier menu
If Dir$(MenuPath) = "" Then
    MsgBox GetMesseprog("MESSAGE", "6") & Chr(13) & MenuPath, vbCritical + vbOKOnly, GetMesseprog("COMMUN", "1")
    Exit Sub
End If
    
'-> Charger le menu
lpBuffer = GetIniFileValue("MENU", "", MenuPath, True)

'-> Cr�ation du menu
For i = 1 To NumEntries(lpBuffer, Chr(0))
    '-> Get d'une d�finition de menu
    lpMenu = Trim(DeCrypt(Entry(i, lpBuffer, Chr(0))))
    If lpMenu = "" Then Exit For
    '-> Supprimer la cl� pour ne garder que la valeur
    lpMenu = Entry(2, lpMenu, "=")
    '-> Eclater en menu , libell�s
    NiveauCours = Entry(1, Entry(1, lpMenu, "~"), "�")
    Menu = Entry(2, Entry(1, lpMenu, "~"), "�")
    Lib = Entry(2, lpMenu, "~")
    Programme = Entry(3, lpMenu, "~")
    Param = Entry(4, lpMenu, "~")
    '-> R�cup�rer le niveau pr�c�dent
    If NiveauCours = "" Then
        '-> On est dans un menu premier niveau : pointer sur le node parent
        Set NodeRef = mdiConsole.TreeNaviga.Nodes("NODEAPPLICATIFS")
    Else
        '-> Pointer sur le niveau sup�rieur
        Set NodeRef = mdiConsole.TreeNaviga.Nodes("NODEAPPLICATIFS|" & NiveauCours)
    End If
    '-> Ajouter le node de ce niveau
    Set Node = mdiConsole.TreeNaviga.Nodes.Add(NodeRef.Key, 4, NodeRef.Key & "|" & Menu, Lib)
    '-> Tester si on est programme
    If Trim(Param) <> "" Then
        '-> On est un programme
        Node.Image = "PROGRAMME"
    Else
        '-> On est sur un niveau de menu
        Node.Image = "BOOKCLOSE"
        Node.ExpandedImage = "BOOKOPEN"
    End If
    '-> Indiquer ce que l'on lancer
    Node.Tag = lpMenu
Next 'Pour toutes les lignes de menu

End Sub

Public Sub StopDOG(aIdent As clsIdent)

'---> Cette proc�dure est charg�e de mettre fin � l'activit� d'un ident

'-> Indiquer que le serveur ne tourne plus
aIdent.IsRunning = False
'-> Indiquer la date d'arret
aIdent.When = Now
'-> Lib�rer le pointeur
Set aIdent.ServeurDOG = Nothing

End Sub

Public Sub UnloadIDX(aIdx As clsIdx, aBase As clsBase)

'---> Cette proc�dure est charg�e de supprimer un agent IDX
Dim res As Long

On Error GoTo GestError

'-> Envoyer un ordre de fin
res = SetWindowText(aIdx.Hdl_Retour_Com, "END_SESSION")
res = SendMessage(aIdx.Hdl_Retour_Com, WM_PAINT, 0, 0)
'-> Se mettre en attente de la r�ponse
Do While Not aIdx.EndIdx
    '-> Rendre la main � la cpu
    DoEvents
Loop
'-> Passer la matrice � false
Matrice_Num_Agent_IDX(CInt(aIdx.Num_Idx)) = False
'-> Supprimer l'objet de la collection des agents affect�s � la base
aBase.Idx.Remove "IDX|" & CInt(aIdx.Num_Idx)

GestError:

End Sub

Public Sub UnloadAgent(aCon As clsConnectConsole, aIdent As clsIdent)

'---> Cette proc�dure est charg�e d'interrompre un agent B3D en cours de travail

Dim res As Long
Dim Connexion As B3DConnexion

On Error GoTo GestError

'-> Pointer sur le descripteur associ�
Set Connexion = Connexions("CON|" & aCon.IndexAgent)

'-> Envoyer l'ordre de d�connexion
If Not aCon.DisConnect Then Exit Sub

'-> Se mettre en attente de la r�ponse
Do While Not aCon.EndProg
    '-> Rendre la main � la cpu
    DoEvents
Loop

'-> Passer la matrice � false
Matrice_Num_Agent_B3D(CInt(aCon.IndexAgent)) = False
        
'-> Faire -1 sur le nombre d'agent B3D affect�s � cet ident
aIdent.NbConnect = aIdent.NbConnect - 1

'-> si la feuille de gestion des identifiants est ouverte , envoyer un ordre de r��criture
If aIdent.HdlForm <> 0 Then
    res = SetWindowText(aIdent.hdlCom, "DRAW_ACTIVITY")
    res = SendMessage(aIdent.hdlCom, WM_PAINT, 0, 0)
End If '-> Envoyer un message de mise � jour de l'activit�

'-> Supprimer de l'affichage si necessaire
If lpsConsole.IsVible Then
    '-> Supprimer du listview
    frmPortailB3D.ListView1.ListItems.Remove "CON|" & aCon.IndexAgent
    '-> formatter les entetes
    FormatListView frmPortailB3D.ListView1
End If

'-> Supprimer le descripteur de la collection des connexions en cours
ConnexionsDescripteur.Remove "CON|" & aCon.IndexAgent
Connexions.Remove "CON|" & aCon.IndexAgent


GestError:

End Sub

Public Sub RunSession()

'---> Cette proc�dure initialise une connexion Base (Idx_Build et agent B3D) et lance le serveur DOG associ�

Dim aIdent As clsIdent
Dim aBase As clsBase

On Error GoTo GestError

'-> V�rification de l'ident associ� � la session
If IdentSession = "" Then
ErrIdent:
    MsgBox GetMesseprog("SESSION", "8"), vbCritical + vbOKOnly, GetMesseprog("COMMUN", "6")
    End
End If
If Not IsIdent(IdentSession) Then GoTo ErrIdent

'-> V�rification de la base associ�e � la session
If BaseSession = "" Then
ErrBase:
    MsgBox GetMesseprog("SESSION", "9"), vbCritical + vbOKOnly, GetMesseprog("COMMUN", "6")
    End
End If
If Not IsBase(BaseSession) Then GoTo ErrBase

'-> Pointer sur le premier ident
Set aIdent = Idents("IDENT|" & IdentSession)

'-> V�rifier si on trouve le sch�ma
If Dir$(aIdent.FichierSchema) = "" Then
    MsgBox GetMesseprog("SESSION", "6") & Chr(13) & aIdent.FichierSchema, vbCritical + vbOKOnly, GetMesseprog("COMMUN", "6")
    End
End If

'-> V�rifier si on trouve le catalogue
If Dir$(aIdent.FichierCatalogue) = "" Then
    MsgBox GetMesseprog("SESSION", "6") & Chr(13) & aIdent.FichierCatalogue, vbCritical + vbOKOnly, GetMesseprog("COMMUN", "6")
    End
End If

'-> V�rifier si on trouve les liens
If Dir$(aIdent.FichierLink) = "" Then
    MsgBox GetMesseprog("SESSION", "6") & Chr(13) & aIdent.FichierLink, vbCritical + vbOKOnly, GetMesseprog("COMMUN", "6")
    End
End If

'-> V�rifier si on trouve le constructeur d'index
If Dir$(App.Path & "\Idx_build_B3D.exe") = "" Then
    MsgBox GetMesseprog("SESSION", "6") & Chr(13) & App.Path & "\Idx_build_B3D.exe", vbCritical + vbOKOnly, GetMesseprog("COMMUN", "6")
    End
End If

'-> V�rifier si on trouve l'agent B3D
If Dir$(ApplicationPath & "\B3D_AGENT.exe") = "" Then
    MsgBox GetMesseprog("SESSION", "6") & Chr(13) & App.Path & "\AgentB3D.exe", vbCritical + vbOKOnly, GetMesseprog("COMMUN", "6")
    End
End If

'-> Pointer sur la base et v�rifier l'emplacement du fichier physique
Set aBase = Bases(BaseSession)

'-> V�rifier le fichier associ� � la base existe bien
If aBase.Fichier = "" Then
ErrFileBase:
    MsgBox GetMesseprog("SESSION", "10"), vbCritical + vbOKOnly, GetMesseprog("COMMUN", "6")
    End
Else
    If Dir$(aBase.Fichier) = "" Then GoTo ErrFileBase
End If

'-> Lancer le ServeurDOG
If Not InitServeurDOG(aIdent) Then
    MsgBox GetMesseprog("SESSION", "7"), vbCritical + vbOKOnly, GetMesseprog("COMMUN", "6")
    End
End If

'-> Indiquer que le serveur DOG est lanc�
IsServeurSessionRunning = True

'-> Lancer un IDX_BUILD sur cette base
If Not RunIDX(aIdent) Then
    MsgBox GetMesseprog("SESSION", "11"), vbCritical + vbOKOnly, GetMesseprog("COMMUN", "6")
    GoTo GestError
End If

'-> Indiquer que le constructeur IDX est lanc�
IsIdxbuildSessionRunning = True

'-> Lancer un agent B3D
If Not RunAGENTB3D(aIdent) Then
    MsgBox GetMesseprog("SESSION", "12"), vbCritical + vbOKOnly, GetMesseprog("COMMUN", "6")
    GoTo GestError
End If

'->Indiquer que l'agent est actif
IsAgentB3DSessionRunning = True

'-> Quitter la proc�dure
Exit Sub

GestError:
    '-> Supprimer en m�moire tout ce qui est en train de tourner
    If aIdent Is Nothing Then End
        
    '-> Arretr la session
    StopSession

End Sub

Public Function StopSession() As Boolean

Dim aIdent As clsIdent
Dim aCon As B3DConnexion
Dim aIdx As clsIdx
Dim aBase As clsBase
Dim ConnectConsole As clsConnectConsole

'-> Pointer sur l'identifiant session
If IsIdent(IdentSession) Then
    '-> Pointer sur l'ident
    Set aIdent = Idents("IDENT|" & IdentSession)
Else
    End
End If
    
'-> Pointer sur la base
If IsBase(BaseSession) Then
    '-> Pointer sur la base
    Set aBase = Bases(BaseSession)
Else
    End
End If

'-> Pointer sur l'idx
Set aIdx = aBase.Idx(1)

'-> V�rifier que l'idx ne soit pas en cours de travail
If aIdx.IsWorking Then
    MsgBox GetMesseprog("SESSION", "13"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "11")
    StopSession = False
    Exit Function
End If
    
'-> Pointer sur la connexion
Set aCon = Connexions("CON|1")
    
'-> V�rifier que l'agent ne soit pas en cours de travail
If aCon.IsWorking Then
    MsgBox GetMesseprog("SESSION", "13"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "11")
    StopSession = False
    Exit Function
End If
    
'-> Arreter l'Idx si necessaire
If IsIdxbuildSessionRunning Then UnloadIDX aIdx, aBase

'-> Pointer sur l'agent de console associ�
Set ConnectConsole = ConnexionsDescripteur(1)
    
'-> Arreter  l'agent B3D si necessaire
If IsAgentB3DSessionRunning Then UnloadAgent ConnectConsole, aIdent
    
GestError:
End

End Function

Private Function IsIdent(sIdent As String) As Boolean

'---> Cette proc�dure v�rifie si l'ident sp�cifi� existe bien
Dim aIdent As clsIdent

On Error GoTo GestError
'-> Essayer de pointer sur l'ident
Set aIdent = Idents("IDENT|" & sIdent)
'-> Renvoyer une valeur de succ�s
IsIdent = True
Exit Function
GestError:

End Function
Private Function IsBase(sBase As String) As Boolean

'---> Cette proc�dure v�rifie si la base sp�cifi�e existe

Dim aBase As clsBase

On Error GoTo GestError

'-> Essayer de pointer sur la base
Set aBase = Bases(sBase)

'-> Renvoyer une valeur de succ�s
IsBase = True

Exit Function

GestError:

End Function


Private Function RunAGENTB3D(aIdent As clsIdent) As Boolean

Dim aCon As clsConnectConsole
Dim Connexion As B3DConnexion
Dim Id_Session As String
Dim res As Long
Dim hdlAgent As Long
Dim hdlCom As Long
Dim hdlFile As Integer
Dim TempFileNameParam As String
Dim aBase As clsBase

'---> Lancer un agent B3D dans le cadre d'une version monoposte

On Error GoTo GestError

'-> Composer l'identifiant de session
res = GetTickCount()
Id_Session = OpeConsole & "+" & res

'-> Pointer vers l'objet base associ�
Set aBase = Bases(aIdent.Base)

'-> Initialiser uen nouvelle connexion
Set aCon = New clsConnectConsole
aCon.Operat = OpeConsole
aCon.CodeLangue = OpeCodeLangue
aCon.IdSession = res
aCon.DateConnexion = Now
aCon.Num_Idx = "5001"
aCon.IndexAgent = "0001"
aCon.Ident = aIdent.Code

'-> Charger le premier handle de communication s'il n'existe pas d�ja
If Not Matrice_Com_Agent(1) Then
    Load frmCom.objCom(1)
    Matrice_Com_Agent(1) = True
End If

hdlCom = frmCom.objCom(1).hwnd


'-> Cr�er un fichier de param�trage pour exporter les param�tres vers l'agent B3D
hdlFile = FreeFile
TempFileNameParam = GetTempFileNameVB("B3D")
Open TempFileNameParam For Output As #hdlFile

'-> Param�trage g�n�raux
Print #hdlFile, "[PARAM]"
Print #hdlFile, "IDSESSION=" & Crypt(aCon.IdSession)
Print #hdlFile, "IDAGENT=" & Crypt(Format(aCon.IndexAgent, "0000"))
Print #hdlFile, "IDIDX=" & Crypt(aCon.Num_Idx)
Print #hdlFile, "OPERAT=" & Crypt(aCon.Operat)
Print #hdlFile, "CODELANGUE=  " & Crypt(CStr(aCon.CodeLangue))
Print #hdlFile, "UC_ID=" & Crypt(CStr(Uc_Id))
Print #hdlFile, "HDLCOM=" & Crypt(CStr(frmCom.objCom(aCon.IndexAgent).hwnd))
Print #hdlFile, "DEBUG=" & Crypt("0")
Print #hdlFile, "TRACE=" & Crypt("0")
Print #hdlFile, "REPLICAUPDATE=" & Crypt(CStr(ReplicaUpdate))
Print #hdlFile, "LOCK=" & Crypt(VecteurLock)
Print #hdlFile, "ACCESDIR=" & Crypt(CStr(Tempo_Dmd))

'-> Param�trage de l'ident
Print #hdlFile, "[IDENT]"
Print #hdlFile, "CODE=" & Crypt(aIdent.Code)
Print #hdlFile, "SCH=" & Crypt(aIdent.FichierSchema)
Print #hdlFile, "CAT=" & Crypt(aIdent.FichierCatalogue)
Print #hdlFile, "LNK=" & Crypt(aIdent.FichierLink)

'-> Param�trage de la base
Print #hdlFile, "[BASE]"
Print #hdlFile, "NOM=" & aBase.Nom_Base
Print #hdlFile, "SERVEUR=" & aBase.Serveur
Print #hdlFile, "TYPE=" & Crypt(aBase.Type_Base)
Print #hdlFile, "USER=" & Crypt(aBase.User)
Print #hdlFile, "PWD=" & Crypt(aBase.Pwd)
Print #hdlFile, "FICHIER=" & Crypt(aBase.Fichier)
Print #hdlFile, "TEMPO=" & Crypt(CStr(aBase.Tempo_Lock))
    
'-> Param�trage des fichiers d'�change
Print #hdlFile, "[PATH]"
Print #hdlFile, "DMD=" & Crypt(DmdPath & Id_Session & ".dmd")
Print #hdlFile, "RSP=" & Crypt(RspPath & Id_Session & ".rsp")
Print #hdlFile, "DEBUG=" & Crypt(LogPath & Id_Session & "dbg")
Print #hdlFile, "TRACE=" & Crypt(LogPath & Id_Session & "trc")
Print #hdlFile, "INT=" & Crypt(IntPath)
Print #hdlFile, "INTSOV=" & Crypt(IntSovPath)
Print #hdlFile, "REPLICAIN=" & Crypt(RepInputPath)
Print #hdlFile, "REPLICAOUT=" & Crypt(RepOutputPath)
Print #hdlFile, "B3DINI=" & Crypt(B3DIniFile)
Print #hdlFile, "RSPPATH=" & Crypt(RspPath)

'-> Fermer le fichier
Close #hdlFile

'-> Lancer le programme
hdlAgent = Shell(ApplicationPath & "\B3D_AGENT.exe " & TempFileNameParam, vbHide)

'-> Affecter son Handle
aCon.hdlProcess = hdlAgent

'-> Initialiser les fichiers d'�change
aCon.DmdFile = DmdPath & Id_Session & ".dmd"
aCon.RspFile = RspPath & Id_Session & ".rsp"

'-> Ajouter dans la collection
ConnexionsDescripteur.Add aCon, "CON|" & aCon.IndexAgent

'-> Cr�er un fichier de param�trage pour exporter les param�tres vers un agent de connexion
hdlFile = FreeFile
TempFileNameParam = GetTempFileNameVB("B3D")
Open TempFileNameParam For Output As #hdlFile

'-> Param�trage g�n�raux
Print #hdlFile, "[PARAM]"
Print #hdlFile, "IDENT=" & Crypt(aIdent.Code)
Print #hdlFile, "OPERAT=" & Crypt(aCon.Operat)
Print #hdlFile, "PWD=" & Crypt(OpePassWord)
Print #hdlFile, "LANGUE=  " & Crypt(CStr(aCon.CodeLangue))
Print #hdlFile, "DMD=" & Crypt(DmdPath & Id_Session & ".dmd")
Print #hdlFile, "RSP=" & Crypt(RspPath & Id_Session & ".rsp")
Print #hdlFile, "IDAGENT=" & Crypt(Format(aCon.IndexAgent, "0000"))
Print #hdlFile, "UC_ID=" & Crypt(CStr(Uc_Id))
Print #hdlFile, "IDSESSION=" & Crypt(CStr(aCon.IdSession))

'-> Fermerle fichier
Close #hdlFile

'-> Cr�er un agent de connexion
Set Connexion = New B3DConnexion

'-> Lire les param�trages
Connexion.ReadParamFormFile TempFileNameParam

'-> Ajouter dans la collection des connexions
Connexions.Add Connexion, "CON|" & aCon.IndexAgent

'-> Renvoyer une valeur de succ�s
RunAGENTB3D = True

'-> Quitter la proc�dure
Exit Function

GestError:

End Function

Public Function GetListeOperat() As String

'---> Cette proc�dure retourne la liste des op�rateurs

Dim lpBuffer As String
Dim i As Integer
Dim ReturnFile As String
Dim DefOperat As String

'-> Get de la liste des op�rateurs
lpBuffer = GetIniFileValue("OPERATS", "", OPERATIniFile, True)

'-> Retour de la liste
If lpBuffer = "" Then Exit Function

'-> Traiter la liste des op�rateurs
For i = 1 To NumEntries(lpBuffer, Chr(0))
    '-> Lecture d'un code op�rateur
    DefOperat = Entry(i, lpBuffer, Chr(0))
    DefOperat = Entry(1, DefOperat, "=")
    If Trim(DefOperat) = "" Then Exit For
    '-> Retourner son code op�rateur
    If ReturnFile = "" Then
        ReturnFile = Entry(1, DefOperat, "|")
    Else
        ReturnFile = ReturnFile & Chr(0) & Entry(1, DefOperat, "|")
    End If
Next

'-> Retourner la liste
GetListeOperat = ReturnFile

End Function

Public Function IsIDX(ByVal aBase As clsBase, ModeDebug As Boolean, aIdent As clsIdent) As clsIdx

'-> Retourne le num�ro de l'agent Existant

Dim i As Integer
Dim aIdx As clsIdx
Dim List_Idx As String
Dim List_Idx_Tri As String
Dim strToAdd As String
Dim hdlCom As Long
Dim TempFileNameParam As String
Dim hdlFile As String


'-> Rechercher le premier num�ro d'agent libre
For i = 5001 To 9999

    '-> S'arreter sur le premier agent libre
    If Not Matrice_Num_Agent_IDX(i) Then
    
        '-> Indiquer que l'agent est OK
        Matrice_Num_Agent_IDX(i) = True
        
        '-> Charger son handle de communication s'il n'existe pas d�ja
        If Not Matrice_Com_Idx(i) Then
            Load frmCom.objCom(i)
            Matrice_Com_Idx(i) = True
        End If
        
        hdlCom = frmCom.objCom(i).hwnd
        
        '-> Cr�er un nouvel agent IDX
        Set aIdx = New clsIdx
        aIdx.Num_Idx = Format(i, "0000")
        aIdx.Database = aBase.Key
        
        '-> ajouter �galement dans la collection des agents IDX de la base
        aBase.Idx.Add aIdx, "IDX|" & Format(i, "0000")
        
        '-> Lancer ou non le mode debug
        If ModeDebug Then
            strToAdd = "1"
        Else
            strToAdd = "0"
        End If
        
        hdlFile = FreeFile
        TempFileNameParam = GetTempFileNameVB("IDX")
        Open TempFileNameParam For Output As #hdlFile
        
        '-> Param�trage g�n�raux
        Print #hdlFile, "[PARAM]"
        Print #hdlFile, "IDIDX=" & Crypt(Format(i, "0000"))
        Print #hdlFile, "UC_ID=" & Crypt(CStr(Uc_Id))
        Print #hdlFile, "HDLCOM=" & Crypt(CStr(hdlCom))
        Print #hdlFile, "DEBUG=" & Crypt("1")
        Print #hdlFile, "REPLICAUPDATE=" & Crypt(CStr(ReplicaUpdate))
        Print #hdlFile, "LOCK=" & Crypt(VecteurLock)
        Print #hdlFile, "ACCESDIR=" & Crypt(CStr(Tempo_Dmd))
                
        '-> Param�trage de la base
        Print #hdlFile, "[BASE]"
        Print #hdlFile, "NOM=" & aBase.Nom_Base
        Print #hdlFile, "SERVEUR=" & aBase.Serveur
        Print #hdlFile, "TYPE=" & Crypt(aBase.Type_Base)
        Print #hdlFile, "USER=" & Crypt(aBase.User)
        Print #hdlFile, "PWD=" & Crypt(aBase.Pwd)
        Print #hdlFile, "FICHIER=" & Crypt(aBase.Fichier)
        Print #hdlFile, "TEMPO=" & Crypt(CStr(aBase.Tempo_Lock))
            
        '-> Param�trage des fichiers d'�change
        Print #hdlFile, "[PATH]"
        Print #hdlFile, "DEBUG=" & Crypt(LogPath & Format(i, "0000") & ".dbg")
        Print #hdlFile, "B3DINI=" & Crypt(B3DIniFile)
        Print #hdlFile, "IDENTINI=" & Crypt(IdentIniFile)
        Print #hdlFile, "SERVEURDOGPATH=" & Crypt(App.Path & "\Serveur B3D\")
        
        '-> Fermer le fichier
        Close #hdlFile
        
        '-> Lancer un nouvel agent IDX
        Shell ApplicationPath & "\B3D_IDX.EXE " & TempFileNameParam, vbHide

        '-> Renvoyer un pointeur vers l'agent IDX
        Set IsIDX = aIdx
        
        '-> Quitter la fonction
        Exit Function
        
    End If
Next
            
End Function

Private Function RunIDX(aIdent As clsIdent) As Boolean

Dim aIdx As clsIdx
Dim aBase As clsBase

On Error GoTo GestError

'-> Pointer sur la base
Set aBase = Bases(aIdent.Base)

'-> Lancer l'agent IDX dans le cadre d'une version monoposte
Set aIdx = IsIDX(aBase, True, aIdent)

'-> Renvoyer une valeur de succ�s
RunIDX = True

'-> Quitter la fonction
Exit Function

GestError:

End Function

Public Function InitServeurDOG(ByRef aIdent As clsIdent) As Boolean

'---> Cette fonction a pour but de lancer le serveur DOG affect� � un ident

Dim aServeurDOG As Object
Dim ServeurFile As String

'On Error GoTo Errorhandler

'-> Bloquer l'�cran + pointeur de souris
Screen.MousePointer = 11
mdiConsole.Enabled = False

'-> Initialiser le serveur DOG effect� � cet ident
Set aIdent.ServeurDOG = New clsDOG

'-> Lancer le chargement des diff�rents fichiers
LoadCatalogueB3D aIdent.ServeurDOG, aIdent.FichierCatalogue
LoadSchemaB3D aIdent.ServeurDOG, aIdent.FichierSchema
LoadLinkB3D aIdent.ServeurDOG, aIdent.FichierLink
LoadSystemParam aIdent.ServeurDOG, B3DIniFile, aIdent.Code

'-> Positionner pour le serveur DOG les variables de fichier
aIdent.ServeurDOG.FichierCatalogue = aIdent.FichierCatalogue
aIdent.ServeurDOG.FichierSchema = aIdent.FichierSchema
aIdent.ServeurDOG.FichierLink = aIdent.FichierLink

'-> Nombre de tables
aIdent.ServeurDOG.FnameCount = aIdent.Fname_Count

'-> Nombre d'objets
aIdent.ServeurDOG.CatalogueCount = aIdent.Emilie_Count

'-> Code de l'ident
aIdent.ServeurDOG.Ident = aIdent.Code

'-> Fichier Ini associ�
aIdent.ServeurDOG.IdentIniFile = IdentIniFile

'-> Indiquer que le serveur est actif
aIdent.IsRunning = True

'-> Quitter la proc�dure en renvoyant une valeur de succ�s
InitServeurDOG = True

'-> D�bloquer l'�cran + pointeur de souris
Screen.MousePointer = 0
mdiConsole.Enabled = True

Exit Function

Errorhandler:

    '-> Valeur d'erreur
    InitServeurDOG = False
    
    '-> D�bloquer l'�cran + pointeur de souris
    Screen.MousePointer = 0
    mdiConsole.Enabled = True
    
    '-> Lib�rer le pointeur vers le serveur ActiveX
    Set aIdent.ServeurDOG = Nothing

End Function


Public Sub PrintHisto(strToWrite As String)

'---> Ecrire un historique

On Error GoTo GestError

Dim hdlFile As Integer

'-> Get d'un handle de fichier
hdlFile = FreeFile

'-> Ouvrir le fichier histo
Open App.Path & "\Param\Data1.dat" For Append As #hdlFile

'-> Ajouter la ligne d'histo
Print #hdlFile, strToWrite

'-> Fermer le fichier ASCII
Close #hdlFile

Exit Sub

GestError:
    Reset
    
End Sub

Public Sub RunApp(ParamApp As String, aCon As B3DConnexion)

'---> Cette proc�dure est charg�e de lancer un exe � part

Dim Param As String
Dim NomProg As String
Dim TempFileName As String
Dim Thierry As String
Dim aIdent As clsIdent

'-> Get du nom du programme
NomProg = Entry(3, ParamApp, "~")
Param = Entry(4, ParamApp, "~")

'-> Pointer sur l'ident associ� � la connexion
Set aIdent = Idents("IDENT|" & aCon.Ident)

'-> V�rifier si on trouve les programmes
If Dir$(aIdent.ApplicationPath & NomProg) = "" Then
    MsgBox GetMesseprog("MESSAGE", "20") & Chr(13) & aIdent.ApplicationPath & NomProg, vbCritical + vbOKOnly, GetMesseprog("COMMUN", "1")
    Exit Sub
End If

'-> Enregistrer les param�trages
TempFileName = aCon.ExportParamToFile

'-> Afficher la ligne de commande
If B3DVersion = "DEV" Then
    Thierry = InputBox("", "", TempFileName & "�" & Param)
    If Thierry <> "" Then _
    Shell aIdent.ApplicationPath & NomProg & " " & TempFileName & "�" & Param, vbNormalFocus
Else
    Shell aIdent.ApplicationPath & NomProg & " " & TempFileName & "�" & Param, vbNormalFocus
End If

End Sub

Public Sub GetPresence(Field As String, aCon As clsConnectConsole, Rep As String)

'Dim Ligne As String
'Dim Param As String
'Dim i As Long
'Dim ValueField As String
'Dim X As ListItem
'
'On Error GoTo GestError
'
''-> Lancer la requete de l'agent B3D
'aCon.aExchange.InitExchange "DMD"
'
''-> Initialiser une demande
'aCon.aExchange.strWrite "B3D_SEEK_DATA~" & FIELD & "�" & Trim(Rep)
'If Not aCon.aExchange.SendExchange(True) Then
'    MsgBox GetMesseprog("MESSAGE", "34"), vbCritical + vbOKOnly, GetMesseprog("COMMUN", "1")
'    Exit Sub
'End If
'
''-> Charger la feuille
'Load frmLiens
'
''-> Butter les colonnes
'frmLiens.ListView1.ColumnHeaders.Clear
'
''-> Setting du controle image
'Set frmLiens.ListView1.SmallIcons = frmLiens.ImageList1
'
''-> Ajouter une colonne
'frmLiens.ListView1.ColumnHeaders.Add , , GetMesseprog("FRMEMILIE", "16")
'
''-> Afficher la liste des tables
'For i = 1 To aCon.aExchange.GetNbLignes
'    '-> Get de la r�ponse
'    Ligne = aCon.aExchange.GetLigne(i)
'    Param = Entry(3, Ligne, "~")
'    '-> Get de la valeur
'    ValueField = Entry(3, Ligne, "�")
'    '-> Ajouter une entr�e
'    Set X = frmLiens.ListView1.ListItems.Add(, , Entry(2, Entry(1, Param, "�"), "�") & " - " & Entry(1, Entry(2, Param, "�"), "�"))
'    If ValueField = "1" Then
'        X.SmallIcon = "FIND"
'    Else
'        X.SmallIcon = "NOTFIND"
'    End If
'Next
'
''-> Formatter les entetes
'FormatListView frmLiens.ListView1
'frmLiens.ListView1.Refresh
'
'GestError:
'Screen.MousePointer = 0
'
''-> Afficher la feuille
'frmLiens.Caption = GetMesseprog("FRMEMILIE", "16") & ": " & FIELD & GetMesseprog("FRMEMILIE", "24") & " : " & Rep
'frmLiens.Show vbModal


End Sub
