VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmGestSchema 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gestion du schema"
   ClientHeight    =   8685
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10965
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8685
   ScaleWidth      =   10965
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   8655
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10935
      _ExtentX        =   19288
      _ExtentY        =   15266
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   4
      TabHeight       =   520
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "frmGestSchema.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Command1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Command2"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "frmGestSchema.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame4"
      Tab(1).Control(1)=   "Frame3"
      Tab(1).ControlCount=   2
      Begin VB.CommandButton Command2 
         Caption         =   "Impression"
         Height          =   735
         Left            =   8880
         TabIndex        =   19
         Top             =   1560
         Width           =   1935
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Mise � jour"
         Height          =   735
         Left            =   8880
         TabIndex        =   18
         Top             =   600
         Width           =   1935
      End
      Begin VB.Frame Frame4 
         Height          =   5160
         Left            =   -74880
         TabIndex        =   14
         Top             =   3360
         Width           =   10695
         Begin MSComctlLib.ListView ListView3 
            Height          =   4755
            Left            =   120
            TabIndex        =   15
            Top             =   240
            Width           =   10455
            _ExtentX        =   18441
            _ExtentY        =   8387
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            OLEDragMode     =   1
            OLEDropMode     =   1
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            OLEDragMode     =   1
            OLEDropMode     =   1
            NumItems        =   7
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   6
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame Frame3 
         Height          =   2775
         Left            =   -74880
         TabIndex        =   12
         Top             =   480
         Width           =   10695
         Begin MSComctlLib.ListView ListView2 
            Height          =   2415
            Left            =   120
            TabIndex        =   13
            Top             =   240
            Width           =   10455
            _ExtentX        =   18441
            _ExtentY        =   4260
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame Frame2 
         Height          =   6120
         Left            =   120
         TabIndex        =   10
         Top             =   2460
         Width           =   10695
         Begin MSComctlLib.ListView ListView1 
            Height          =   5715
            Left            =   120
            TabIndex        =   11
            Top             =   240
            Width           =   10455
            _ExtentX        =   18441
            _ExtentY        =   10081
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            OLEDragMode     =   1
            OLEDropMode     =   1
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            OLEDragMode     =   1
            OLEDropMode     =   1
            NumItems        =   10
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   6
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   9
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.Frame Frame1 
         Height          =   1815
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   8655
         Begin VB.CheckBox Check1 
            Height          =   255
            Left            =   5400
            TabIndex        =   17
            Top             =   1080
            Width           =   2415
         End
         Begin VB.CheckBox chkFnameIndexDirect 
            Height          =   255
            Left            =   5400
            TabIndex        =   5
            Top             =   240
            Width           =   3015
         End
         Begin VB.TextBox fDeignation 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1200
            TabIndex        =   4
            Top             =   600
            Width           =   3495
         End
         Begin VB.CheckBox fStat 
            Enabled         =   0   'False
            Height          =   255
            Left            =   2760
            TabIndex        =   3
            Top             =   1080
            Value           =   1  'Checked
            Width           =   2175
         End
         Begin VB.CheckBox fReplica 
            Height          =   255
            Left            =   120
            TabIndex        =   2
            Top             =   1080
            Width           =   2295
         End
         Begin VB.Image Image3 
            Height          =   480
            Left            =   8160
            MouseIcon       =   "frmGestSchema.frx":0038
            MousePointer    =   99  'Custom
            Picture         =   "frmGestSchema.frx":0D02
            Top             =   1320
            Width           =   480
         End
         Begin VB.Image Image2 
            Height          =   480
            Left            =   8160
            MouseIcon       =   "frmGestSchema.frx":19CC
            MousePointer    =   99  'Custom
            Picture         =   "frmGestSchema.frx":2696
            Top             =   480
            Width           =   480
         End
         Begin VB.Label Label1 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   5400
            TabIndex        =   16
            Top             =   1440
            Width           =   2775
         End
         Begin VB.Label Label6 
            Height          =   255
            Left            =   120
            TabIndex        =   9
            Top             =   240
            Width           =   975
         End
         Begin VB.Label fNum 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   3000
            TabIndex        =   8
            Top             =   240
            Width           =   1695
         End
         Begin VB.Label fIndexDirect 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Left            =   5400
            TabIndex        =   7
            Top             =   600
            Width           =   2775
         End
         Begin VB.Image Image10 
            Height          =   480
            Left            =   4800
            MouseIcon       =   "frmGestSchema.frx":3360
            MousePointer    =   99  'Custom
            Picture         =   "frmGestSchema.frx":402A
            Top             =   480
            Width           =   480
         End
         Begin VB.Label Label2 
            Height          =   255
            Left            =   120
            TabIndex        =   6
            Top             =   600
            Width           =   1335
         End
      End
   End
   Begin VB.Menu mnuFname 
      Caption         =   "Fname"
      Visible         =   0   'False
      Begin VB.Menu mnuAddFnameField 
         Caption         =   "Ajouter"
      End
      Begin VB.Menu mnuEditFnameField 
         Caption         =   "Propri�t�s"
      End
      Begin VB.Menu mnusep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelFnameField 
         Caption         =   "Supprimer"
      End
   End
   Begin VB.Menu mnuIname 
      Caption         =   "Iname"
      Visible         =   0   'False
      Begin VB.Menu mnuAddIname 
         Caption         =   "Ajouter"
      End
      Begin VB.Menu mnuEditIname 
         Caption         =   "Propri�t�s"
      End
      Begin VB.Menu mnuReindex 
         Caption         =   "R�indexer"
      End
      Begin VB.Menu mnusep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelIname 
         Caption         =   "Supprimer"
      End
   End
   Begin VB.Menu mnuIField 
      Caption         =   "Ifield"
      Visible         =   0   'False
      Begin VB.Menu mnuAddIField 
         Caption         =   "Ajouter"
      End
      Begin VB.Menu mnuEditIField 
         Caption         =   "Propri�t�s"
      End
      Begin VB.Menu mnuSep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelIField 
         Caption         =   "Supprimer"
      End
   End
End
Attribute VB_Name = "frmGestSchema"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private aFname As Fname '-> Contient le code de la table en cours de lecture
Private aIdent As clsIdent '-> Classe d'acc�s au serveurDOG

Private Conditions(1 To 10) As String   '-> Matrice des conditions
Private DisplayCdtbyNum As Boolean
Private BadIndex() As Integer
Dim OkDD As Boolean '-> Indique si le DD est autoris�
Dim KeyDD As String '-> Cl� de la cl� que l'on d�sire d�placer
Dim IndexDD As Integer '-> Index de l'item que l'on d�sire d�placer

Public Sub Initialisation(IdentKey As String, strFname As String)

'---> Cette proc�dure charge les informartions sur une table

Dim i As Integer
Dim aField As Object
Dim aIname As Object
Dim x As ListItem

'-> Charger la liste des conditions
For i = 1 To 10
    Conditions(i) = GetMesseprog("CONDITIONS", CStr(i))
Next

'-> Pointer sur l'ident
Set aIdent = Idents(IdentKey)
    
'-> Pointer sur la table sp�cifi�e
Set aFname = aIdent.ServeurDOG.fNames(Entry(2, IdentKey, "|") & "|" & strFname)

'*******************************
'*  Mise � jour des propri�t�s *
'*******************************

'-> NumFname
Me.fNum = aFname.Num_Fname

'-> Libel
aFname.CodeLangue = OpeCodeLangue
Me.fDeignation = aFname.Designation

'-> Positionner la propri�t� TAG
For i = 1 To 10
    '-> Postionner le code langue
    aFname.CodeLangue = i
    '-> Concatainer dans la propri�t� TAG
    If Me.fDeignation.Tag = "" Then
        Me.fDeignation.Tag = aFname.Designation
    Else
        Me.fDeignation.Tag = Me.fDeignation.Tag & "|" & aFname.Designation
    End If
Next

'-> R�cplication
Me.fReplica.Value = Abs(CInt(aFname.IsReplica))

'-> Stat
Me.fStat.Value = Abs(CInt(aFname.IsStat))

'-> Index Direct
If aFname.IndexDirect <> "" Then
    '-> Cocher la case
    Me.chkFnameIndexDirect.Value = 1
    '-> Afficher le nom du champ de jointure
    aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & aFname.IndexDirect).CodeLangue = OpeCodeLangue
    Me.fIndexDirect.Caption = aFname.IndexDirect & " - " & aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & aFname.IndexDirect).Fill_Label
End If 'S'il ya une table en acc�s direct

'-> Index d'acc�s jointure
If aFname.IndexJointure <> "" Then
    '-> Pointer sur l'index associ�
    Set aIname = aFname.Inames(aFname.IndexJointure)
    aIname.CodeLangue = OpeCodeLangue
    '-> Afficher la propri�t�
    Me.Label1.Caption = aFname.IndexJointure & " - " & aIname.Designation
    Me.Label1.Tag = aFname.IndexJointure
    Me.Check1.Value = 1
End If

'*******************************
'* Charger la liste des champs *
'*******************************

DisplayFnameFields False, Me.ListView1, True

'***************************
'* Afficher les propri�t�s *
'***************************

For Each aField In aFname.Fields
    DisplayFieldProp aField
Next

'-> Trier sur l'ordre
Me.ListView1.SortKey = 2
Me.ListView1.SortOrder = lvwAscending
Me.ListView1.Sorted = True
Me.ListView1.Refresh

'-> De base autoris� le DD
OkDD = True

'*******************************
'* Charger la liste des index  *
'*******************************

For Each aIname In aFname.Inames
    '-> Ajouter un ListItem
    Set x = Me.ListView2.ListItems.Add(, "INAME|" & aIname.Num_Iname, aIname.Num_Iname)
    '-> Afficher ses propri�t�s
    DisplayInameProp aIname
Next 'Pour tous les index de la table

'-> Afficher les champs du premier index
If aFname.Inames.Count <> 0 Then ListView2_ItemClick Me.ListView2.ListItems(1)

'-> Justifier le  ListView
FormatListView Me.ListView2

'-> Formatter le ListView des champs
FormatListView Me.ListView3

'-> Trier sur l'ordre
Me.ListView3.SortKey = 2
Me.ListView3.SortOrder = lvwAscending
Me.ListView3.Sorted = True
Me.ListView3.Refresh


End Sub
Private Sub DisplayInameFields(aIname As Object)

'---> Cette proc�dure affiche la liste des champs composants un index

Dim aIfield As Object
Dim x As ListItem

'-> Vider le listview
Me.ListView3.ListItems.Clear

'-> Pour tous les champs d'index
For Each aIfield In aIname.iFields
    '-> Ajouter un nouvel objet
    Set x = Me.ListView3.ListItems.Add(, "IFIELD|" & aIfield.Num_Field, aIfield.Num_Field)
    '-> Afficher ses propri�t�s
    DisplayIFieldProp aIfield
Next

'-> Justifier le brose
FormatListView Me.ListView3

End Sub

Private Sub DisplayIFieldProp(aIfield As Object)

Dim x As ListItem
Dim aEmilieObj As Object

'-> Pointer sur l'objet associ� dans le browse
Set x = Me.ListView3.ListItems("IFIELD|" & aIfield.Num_Field)

'-> Designation
Set aEmilieObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & aIfield.Num_Field)
aEmilieObj.CodeLangue = OpeCodeLangue
x.SubItems(1) = aEmilieObj.Fill_Label
    
'-> Ordre
x.SubItems(2) = Format(aIfield.Ordre, "0000")
    
'-> Cumul
If aIfield.Cumul Then
    x.SubItems(3) = "Yes"
Else
    x.SubItems(3) = "No"
End If

'-> Condition
If aIfield.TypeCondition <> 0 Then
    '-> Afficher son libell� si on l'a
    If DisplayCdtbyNum Then
        x.SubItems(4) = aIfield.TypeCondition
    Else
        x.SubItems(4) = Conditions(aIfield.TypeCondition)
    End If
    '-> Mini
    x.SubItems(5) = aIfield.ValMini
    '-> Maxi
    x.SubItems(6) = aIfield.ValMaxi
Else
    x.SubItems(4) = ""
    x.SubItems(5) = ""
    x.SubItems(6) = ""
End If


End Sub

Private Sub DisplayInameProp(aIname As Object)

'---> Cette proc�dure affiche la feuille de propri�t� des index

Dim x As ListItem

'-> Pointer sur l'objet du ListView a   ffet�
Set x = Me.ListView2.ListItems("INAME|" & aIname.Num_Iname)

'-> Libell�
aIname.CodeLangue = OpeCodeLangue
x.SubItems(1) = aIname.Designation

'-> Unique
If aIname.IsUnique Then
    x.SubItems(2) = "Yes"
Else
    x.SubItems(2) = "No"
End If

'-> Groupe
x.SubItems(3) = aIname.Groupe


End Sub


Private Sub DisplayFieldProp(aField As Field)

'-> Cette proc�dure met � jour le listview des champs avec leur propri�t�

Dim x As ListItem
Dim aEmilieObj As Object

'-> Pointer sur le listItem associ�
Set x = Me.ListView1.ListItems("FIELD|" & aField.Num_Field)
'-> Ordre
x.SubItems(2) = Format(aField.Ordre, "0000")
'-> Replica
If CBool(aField.IsReplica) Then
    x.SubItems(3) = "Yes"
Else
    x.SubItems(3) = "No"
End If
'-> Crypt�
If CBool(aField.IsCryptage) Then
    x.SubItems(4) = "Yes"
Else
    x.SubItems(4) = "No"
End If
'-> Histo
If CBool(aField.IsHisto) Then
    x.SubItems(5) = "Yes"
Else
    x.SubItems(5) = "No"
End If
'-> Recherche
If CBool(aField.IsF10) Then
    x.SubItems(6) = "Yes"
Else
    x.SubItems(6) = "No"
End If
'-> Champ No modif
If CBool(aField.NonModifiable) Then
    x.SubItems(7) = "Yes"
Else
    x.SubItems(7) = "No"
End If
'-> Pointer sur l'objet du catalogue associ�
Set aEmilieObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & aField.Num_Field)
x.SubItems(8) = aEmilieObj.DataType
'-> Rajouter si le champ a extent
If CBool(aField.IsExtent) Then
    x.SubItems(9) = "Yes"
Else
    x.SubItems(9) = "No"
End If


End Sub



Private Sub Check1_Click()

Dim aEmilieObj As Object

'-> Mettre � jour ou non l'index de jointure
If Me.Check1.Value = 0 Then
    '-> Vider la variable de texte
    Me.Label1.Caption = ""
    '-> Mettre � jour la table
    aFname.IndexJointure = ""
    '-> Enregistrer
    aIdent.ServeurDOG.Save_Schema
End If


End Sub

Private Sub chkFnameIndexDirect_Click()

Dim aEmilieObj As Object

'-> Mettre � jour ou non la table en acc�s direct
If Me.chkFnameIndexDirect.Value = 0 Then
    '-> Vider la variable de texte
    Me.fIndexDirect.Caption = ""
    '-> Modifier l'objet lien affect� au catalogue
    If aFname.IndexDirect <> "" Then
        '-> Pointer sur l'objet du catalogue
        Set aEmilieObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & aFname.IndexDirect)
        '-> Indiquer qu'il n'est plus en acces directe
        aEmilieObj.fNames(aFname.Num_Fname).IsIndexDirect = False
        '-> Mettre � jour l'objet fname
        aFname.IndexDirect = ""
    End If
    '-> Enregistrer
    aIdent.ServeurDOG.Save_Schema
Else
    If aFname.Inames.Count <> 0 Then
        MsgBox GetMesseprog("MESSAGE", "32"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "1")
        chkFnameIndexDirect.Value = 0
        Exit Sub
    End If
End If

End Sub

Private Sub Command1_Click()

Dim aCon As B3DConnexion

'-> S�lectioner une agent
strRetour = ""
frmChxAgent.Ident = aIdent.Code
frmChxAgent.Show vbModal

'-> Pointer sur la connexion
If strRetour = "" Then Exit Sub
Set aCon = Connexions(strRetour)

'-> Faire un run du programme
RunApp "~~B3D_GESTABLE.EXE~" & aFname.Num_Fname & "|||||||||", aCon

End Sub

Private Sub Command2_Click()

Dim aField As Field
Dim aObj As EmilieObj

'-> Initialiser le printer
Printer.Print ""

Printer.Print "Champ    Recherche   Crypt   Histo   Replica     Non modif"

For Each aField In aFname.Fields
    '-> Pointer sur le champ
    Set aObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & aField.Num_Field)
    aObj.CodeLangue = 1
    Printer.Print aField.Num_Field & " : " & aObj.Fill_Label & " | " & aField.IsF10 & " | " & aField.IsCryptage & " | " & aField.IsHisto & " | " & aField.IsReplica & " | " & aField.NonModifiable
Next

Printer.EndDoc

End Sub

Private Sub Form_Load()

'---> Chargement des messprogs des menus

'-> Menu Champ
Me.mnuAddFnameField.Caption = GetMesseprog("COMMUN", "17")
Me.mnuDelFnameField.Caption = GetMesseprog("COMMUN", "18")
Me.mnuEditFnameField.Caption = GetMesseprog("COMMUN", "14")

'-> Menu Index
Me.mnuAddIname.Caption = GetMesseprog("COMMUN", "17")
Me.mnuDelIname.Caption = GetMesseprog("COMMUN", "18")
Me.mnuEditIname.Caption = GetMesseprog("COMMUN", "14")
Me.mnuReindex.Caption = GetMesseprog("COMMUN", "24")

'-> Menu Champ d'index
Me.mnuAddIField.Caption = GetMesseprog("COMMUN", "17")
Me.mnuDelIField.Caption = GetMesseprog("COMMUN", "18")
Me.mnuEditIField.Caption = GetMesseprog("COMMUN", "14")

'---> Chargement des messprogs de l'�cran
Me.Caption = GetMesseprog("FRMSCHEMA", "11")

'-> Onglets
Me.SSTab1.TabCaption(0) = GetMesseprog("FRMSCHEMA", "8")
Me.SSTab1.TabCaption(1) = GetMesseprog("FRMSCHEMA", "9")

'-> Propri�t�s de la table
Me.Frame1.Caption = GetMesseprog("FRMSCHEMA", "1")
Me.Label6.Caption = GetMesseprog("FRMSCHEMA", "2")
Me.chkFnameIndexDirect.Caption = GetMesseprog("FRMSCHEMA", "3")
Me.Label2.Caption = GetMesseprog("FRMSCHEMA", "4")
Me.fStat.Caption = GetMesseprog("FRMSCHEMA", "5")
Me.fReplica.Caption = GetMesseprog("FRMSCHEMA", "12")
Me.Check1.Caption = GetMesseprog("FRMSCHEMA", "6")
Me.Frame2.Caption = GetMesseprog("FRMSCHEMA", "7")

'-> Entetes des colonnes des champs de la table
Me.ListView1.ColumnHeaders(1).Text = GetMesseprog("FRMEMILIE", "16")
Me.ListView1.ColumnHeaders(2).Text = GetMesseprog("FRMEMILIE", "5")
Me.ListView1.ColumnHeaders(3).Text = GetMesseprog("FRMSCHEMA", "13")
Me.ListView1.ColumnHeaders(4).Text = GetMesseprog("FRMSCHEMA", "14")
Me.ListView1.ColumnHeaders(5).Text = GetMesseprog("FRMSCHEMA", "15")
Me.ListView1.ColumnHeaders(6).Text = GetMesseprog("FRMSCHEMA", "16")
Me.ListView1.ColumnHeaders(7).Text = GetMesseprog("FRMSCHEMA", "17")
Me.ListView1.ColumnHeaders(8).Text = GetMesseprog("FRMSCHEMA", "18")
Me.ListView1.ColumnHeaders(9).Text = GetMesseprog("FRMEMILIE", "17")
Me.ListView1.ColumnHeaders(10).Text = GetMesseprog("FRMEMILIE", "26")

'-> Index
Me.Frame3.Caption = GetMesseprog("FRMSCHEMA", "9")

'-> Entetes des colonnes des index
Me.ListView2.ColumnHeaders(1).Text = GetMesseprog("FRMSCHEMA", "25")
Me.ListView2.ColumnHeaders(2).Text = GetMesseprog("FRMSCHEMA", "4")
Me.ListView2.ColumnHeaders(3).Text = GetMesseprog("FRMSCHEMA", "19")
Me.ListView2.ColumnHeaders(4).Text = GetMesseprog("FRMSCHEMA", "20")

'-> Champs d'index
Me.Frame4.Caption = GetMesseprog("FRMSCHEMA", "10")

'-> Entetes des colonnes des champs d'index
Me.ListView3.ColumnHeaders(1).Text = GetMesseprog("FRMEMILIE", "16")
Me.ListView3.ColumnHeaders(2).Text = GetMesseprog("FRMSCHEMA", "4")
Me.ListView3.ColumnHeaders(3).Text = GetMesseprog("FRMSCHEMA", "25")
Me.ListView3.ColumnHeaders(4).Text = GetMesseprog("FRMSCHEMA", "21")
Me.ListView3.ColumnHeaders(5).Text = GetMesseprog("FRMSCHEMA", "22")
Me.ListView3.ColumnHeaders(6).Text = GetMesseprog("FRMSCHEMA", "23")
Me.ListView3.ColumnHeaders(7).Text = GetMesseprog("FRMSCHEMA", "24")


End Sub
Private Sub fReplica_Click()

'-> Mettre � jour la valeur
If Me.fReplica.Value = 1 Then
    aFname.IsReplica = True
Else
    aFname.IsReplica = False
End If

'-> Enregistrer
aIdent.ServeurDOG.Save_Schema


End Sub

Private Sub DisplayFnameFields(UnloadDec As Boolean, aList As ListView, Optional Init As Boolean)


'---> Cette proc�dure charge un listView avec des champs

Dim x As ListItem
Dim aField As Object
Dim aEmilieObj As Object
Dim i As Integer
Dim ToAdd As Boolean
Dim Fname As Object

'-> Ajouter la premi�re ligne pour les justification d'entete
Set x = aList.ListItems.Add(, , aList.ColumnHeaders(1).Text)
For i = 2 To aList.ColumnHeaders.Count
    x.SubItems(i - 1) = aList.ColumnHeaders(i).Text
Next

'-> Afficher la liste des champs pour l'acc�s direct
For Each aField In aFname.Fields
    '-> De base on ajoute
    ToAdd = True
    '-> Pointer sur l'objet du catalogue
    Set aEmilieObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & aField.Num_Field)
    '-> Ne prendre en compte que les champs de type CHAR
    If UnloadDec Then
        If UCase$(aEmilieObj.DataType) <> "CHARACTER" Then ToAdd = False
    End If
    If ToAdd Then
        aEmilieObj.CodeLangue = OpeCodeLangue
        '-> Ajouter un nouvel objet ListItem dans le ListView
        Set x = aList.ListItems.Add(, "FIELD|" & aField.Num_Field, aField.Num_Field)
        '-> Libell�
        x.SubItems(1) = aEmilieObj.Fill_Label
        If Not Init Then
            '-> Ajouter le format
            x.SubItems(2) = aEmilieObj.DataType
            '-> La table de correspondance
            If aEmilieObj.Table_Correspondance <> "" Then
                Set Fname = aIdent.ServeurDOG.fNames(aIdent.Code & "|" & aEmilieObj.Table_Correspondance)
                Fname.CodeLangue = OpeCodeLangue
                x.SubItems(3) = aEmilieObj.Table_Correspondance & " - " & Fname.Designation
            End If
            '-> Le progiciel
            x.SubItems(4) = aEmilieObj.Progiciel
        End If
    End If
Next 'Pour tous les champs affect�s � cette table

'-> Redimensionner les colonnes
FormatListView aList

'-> Supprime le premier �l�ment
aList.ListItems.Remove (1)

'-> Trier par odre de champ
aList.SortOrder = lvwAscending
aList.SortKey = 0
aList.Sorted = True

End Sub

Private Sub EditProp(Item As ListItem)

Dim aField As Field

'-> Modification des propri�t�s de champ
Load frmPropField
'-> Afficher la feuille
Set frmPropField.x = Item
frmPropField.Initialisation
'-> Afficher la feuille
frmPropField.Caption = GetMesseprog("FRMEMILIE", "16") & " : " & Entry(2, Item.Key, "|")
strRetour = ""
frmPropField.Show vbModal

'-> Pointer sur le champ
Set aField = aFname.Fields(Entry(2, Item.Key, "|"))

'-> Enregistrer les propri�t�s
If UCase$(Item.SubItems(3)) = "YES" Then
    aField.IsReplica = True
Else
    aField.IsReplica = False
End If

If UCase$(Item.SubItems(4)) = "YES" Then
    aField.IsCryptage = True
Else
    aField.IsCryptage = False
End If

If UCase$(Item.SubItems(5)) = "YES" Then
    aField.IsHisto = True
Else
    aField.IsHisto = False
End If

If UCase$(Item.SubItems(6)) = "YES" Then
    aField.IsF10 = True
Else
    aField.IsF10 = False
End If

If UCase$(Item.SubItems(7)) = "YES" Then
    aField.NonModifiable = True
Else
    aField.NonModifiable = False
End If

If UCase$(Item.SubItems(9)) = "YES" Then
    aField.IsExtent = True
Else
    aField.IsExtent = False
End If


'-> enregistere le schema
aIdent.ServeurDOG.Save_Schema

End Sub






Private Sub Image10_Click()

'-> Afficher le libell� en multilangue

Dim i As Integer

'-> Charger la feuille
Load frmLangue

'-> Gestion du multiLangue
For i = 1 To 10
    '-> Charger les libell�s par langue
    frmLangue.Text1(i - 1) = Entry(i, Me.fDeignation.Tag, "|")
Next

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher la feuille
frmLangue.Show vbModal

'-> Tester le retour
If strRetour = "" Then Exit Sub

'-> Mise � jour de la propri�t� TAG
Me.fDeignation.Tag = strRetour

'-> Afficher le nouveau libell�
Me.fDeignation.Text = Entry(OpeCodeLangue, strRetour, "|")

'-> Mettre � jour les codes langues
For i = 1 To 10
    aFname.CodeLangue = i
    aFname.Designation = Entry(i, Me.fDeignation.Tag, "|")
Next

'-> Enregistrer la table
aIdent.ServeurDOG.Save_Schema

End Sub

Private Sub Image2_Click()

Dim aEmilieObj As Object

'-> Ne rien faire si pas s�lectionn� l'index direct
If Me.chkFnameIndexDirect.Value = 0 Then Exit Sub

'-> vider la variable d'�change
strRetour = ""

'-> Charger la feuille
Load frmLiens

'-> Afficher la liste des champs
DisplayFnameFields True, frmLiens.ListView1

'-> Afficher la feuille
frmLiens.ListView1.Refresh
frmLiens.Show vbModal

'-> Tester la variable de retour
If strRetour = "" Then Exit Sub

'-> Mettre � jour la valeur
Me.fIndexDirect.Caption = strRetour

'-> Mettre � jour l'objet
aFname.IndexDirect = Entry(2, strRetour, Chr(0))

'-> Pointer sur l'objet du catalogue
Set aEmilieObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & aFname.IndexDirect)

'-> Pointer sur l'objet lien d'affectation pournindiquer qu'il est indexDirect
aEmilieObj.fNames(aFname.Num_Fname).IsIndexDirect = True

'-> Enregistrer les modifications
aIdent.ServeurDOG.Save_Schema

End Sub

Private Sub Image3_Click()

Dim aIname As Object
Dim x As ListItem

'-> Ne rien faire si table � index direct
If aFname.IndexDirect <> "" Then Exit Sub

'-> V�rifier qu'il y a des index
If aFname.Inames.Count = 0 Then Exit Sub

'-> V�rifier que l'on ait coch� Index de jointure
If Me.Check1.Value = 0 Then Exit Sub

'-> Charger la feuille
Load frmLiens

'-> Titre de la feuille
frmLiens.Caption = GetMesseprog("FRMSCHEMA", "6")

'-> Cr�er les ent�tes de colonnes
frmLiens.ListView1.ColumnHeaders.Clear
frmLiens.ListView1.ColumnHeaders.Add , , GetMesseprog("FRMSCHEMA", "9")
frmLiens.ListView1.ColumnHeaders.Add , , GetMesseprog("COMMUN", "22")

'-> Analyser la liste des index de la table sp�cifi�e pour ne retenir que les index uniques
For Each aIname In aFname.Inames
    '-> Tester si l'index est unique
    If aIname.IsUnique Then
        '-> Libell� de l'index
        aIname.CodeLangue = OpeCodeLangue
        '-> Ajouter dans le list View
        Set x = frmLiens.ListView1.ListItems.Add(, "INAME|" & aIname.Num_Iname, aIname.Num_Iname)
        x.SubItems(1) = aIname.Designation
    End If 'Si index est unique
Next 'Pour tous les index
'-> Formatter les entetes
FormatListView frmLiens.ListView1
frmLiens.ListView1.Refresh
'-> Vider la variable de retour
strRetour = ""
'-> Afficher la liste des index uniques
frmLiens.Show vbModal
'-> Tester la variable de retour
If strRetour = "" Then Exit Sub

'-> Afficher la valeur
Me.Label1.Caption = Entry(1, strRetour, Chr(0))
Me.Label1.Tag = Entry(2, strRetour, Chr(0))

'-> Affecter sa valeur
aFname.IndexJointure = Entry(2, strRetour, Chr(0))

'-> Enregistrer le sch�ma
aIdent.ServeurDOG.Save_Schema

End Sub

Private Sub ListView1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)


'-> Trier sur les entetes de colonne
Me.ListView1.SortKey = ColumnHeader.Index - 1
If Me.ListView1.SortOrder = lvwAscending Then
    Me.ListView1.SortOrder = lvwDescending
Else
    Me.ListView1.SortOrder = lvwAscending
End If
Me.ListView1.Sorted = True

'-> V�rifier que la colonne de tri soit bien la colonne d'ordre
If Me.ListView1.SortKey = 2 Then
    If Me.ListView1.SortOrder = lvwAscending Then
        OkDD = True
    Else
        OkDD = False
    End If
Else
    OkDD = False
End If

End Sub



Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim Item As ListItem
Dim Up As Boolean
Dim Down As Boolean
Dim Del As Boolean
Dim Prop As Boolean


'->Init par d�faut
Up = True
Down = True
Del = True
Prop = True

'-> Ne rien faire si pas de s�lection
If Me.ListView1.SelectedItem Is Nothing Then
    Up = False
    Down = False
    Del = False
    Prop = False
End If

'-> Quitter si pas de bouton droit
If Button <> vbRightButton Then Exit Sub

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu contextuel
Me.PopupMenu Me.mnuFname

'-> Selon le retour menu
Select Case strRetour
    Case "ADD"
        '-> Ajouter un champ dans le descriptif
        AddFieldInFname
    Case "DEL"
        '-> Supprimer un champ du descriptif
        DelFieldInFname Entry(2, Me.ListView1.SelectedItem.Key, "|")
    Case "PROP"
        '-> Afficher sa page de propri�t�
        Set Item = Me.ListView1.SelectedItem
        EditProp Item
    Case "UP"
    
    Case "DOWN"

End Select

End Sub
Private Sub DelFieldInFname(NumField As String)

'---> Cette proc�dure supprime un champ d'un descriptif

'-> V�rifier que le champ ne soit pas encore r�f�renc�
If Not aIdent.ServeurDOG.IsFieldRefFname(NumField, aFname.Num_Fname) Then
    MsgBox GetMesseprog("MESSAGE", "36"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "1")
        Exit Sub
End If

'-> Supprimer le champ du r�f�rentiel
aIdent.ServeurDOG.DelField aFname.Num_Fname, NumField

'-> Supprimer de l'interface
Me.ListView1.ListItems.Remove Me.ListView1.SelectedItem.Key

'-> Enregistrer le ServeurDog
aIdent.ServeurDOG.Save_Schema

End Sub

Private Sub AddFieldInFname()

Dim aEmilieObj As Object
Dim aField As Object
Dim ToAdd As Boolean
Dim x As ListItem
Dim i As Integer
Dim Fname As Object

Me.Enabled = False
Screen.MousePointer = 11

'-> Charger la feuille d'affichage des champs
Load frmLiens

'-> Ajouter un champ dans la table
For Each aEmilieObj In aIdent.ServeurDOG.Catalogue
    '-> Indiquer si le fichier
    ToAdd = Not IsFieldInTable(aEmilieObj.Num_Field)
    '-> Si on doit ajouter
    If ToAdd Then
        '-> Positionner le code langue
        aEmilieObj.CodeLangue = OpeCodeLangue
        '-> Si on doit ajouter dans la table
        Set x = frmLiens.ListView1.ListItems.Add(, "FIELD|" & aEmilieObj.Num_Field, aEmilieObj.Num_Field)
        '-> Ajouter le Libell�
        x.SubItems(1) = aEmilieObj.Fill_Label
        '-> Ajouter le format
        x.SubItems(2) = aEmilieObj.DataType
        '-> La table de correspondance
        If aEmilieObj.Table_Correspondance <> "" Then
            Set Fname = aIdent.ServeurDOG.fNames(aIdent.Code & "|" & aEmilieObj.Table_Correspondance)
            Fname.CodeLangue = OpeCodeLangue
            x.SubItems(3) = aEmilieObj.Table_Correspondance & " - " & Fname.Designation
        End If
        '-> Le progiciel
        x.SubItems(4) = aEmilieObj.Progiciel
    End If
Next 'Pour tous les objets du catalogue

'-> Justifier les colonnes
FormatListView frmLiens.ListView1

'-> Vider la variable de retour et afficher la feuille
Screen.MousePointer = 0
strRetour = ""
frmLiens.Show vbModal

'-> Tester  le retour
If strRetour = "" Then Exit Sub

'-> Pointer  sur l'objet du catalogue
Set aEmilieObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & Entry(2, strRetour, Chr(0)))

'-> Ajouter un champ dans le browse
Set x = Me.ListView1.ListItems.Add(, "FIELD|" & aEmilieObj.Num_Field, aEmilieObj.Num_Field)

'-> S�lectionner le champ que l'on vient d'ajouter
x.Selected = True
x.EnsureVisible

'-> Libell� du champ
aEmilieObj.CodeLangue = 1
x.SubItems(1) = aEmilieObj.Fill_Label

'-> Ajouter un champ dans la table
aIdent.ServeurDOG.AddField aFname.Num_Fname, aEmilieObj.Num_Field

'-> Mettre � jour son num�ro d'ordre
aFname.Fields(aEmilieObj.Num_Field).Ordre = Format(Me.ListView1.ListItems.Count, "0000")

'-> Enregistrer le sch�ma
aIdent.ServeurDOG.Save_Schema

'-> Afficher ses propri�t�s
DisplayFieldProp aFname.Fields(aEmilieObj.Num_Field)

'-> Afficher la feuille de propri�t� pour modification
EditProp x

'-> Ok la feuille
Me.Enabled = True

'-> Formatter les entetes
FormatListView Me.ListView1


End Sub


Private Function IsFieldInTable(NumField As String) As Boolean

'---> Cette fonction indique si un champ existe dans un champ

On Error GoTo GestError

Dim aField As Object

'-> Essayer de pointer sur le champ
Set aField = aFname.Fields(NumField)

'-> Renvoyer la valeur
IsFieldInTable = True

Exit Function

GestError:
    IsFieldInTable = False

End Function

Private Sub ListView1_OLEDragDrop(Data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single)

'---> Modification de l'ordre des champs

Dim i As Integer
Dim xItem As ListItem
Dim Cible As Integer
Dim aField As Object
Dim Libel As String

'-> Tester si DD initialis� correctement
If Not OkDD Then Exit Sub

'-> V�rifier que l'on n'a pas lach� n'importe o�
For i = 1 To UBound(BadIndex())
    If Me.ListView1.SelectedItem.Index = BadIndex(i) Then
        OkDD = False
        Exit Sub
    End If
Next

'-> r�cup�rer la cl� de la cible
Cible = Me.ListView1.SelectedItem.Index

'-> R�cup�rer le libell� du champ
Libel = Me.ListView1.ListItems(KeyDD).SubItems(1)

'-> Supprimer l'objet que l'on veut d�placer
Me.ListView1.ListItems.Remove (KeyDD)

'-> Ajouter un champ au bon endroit
If Cible > Me.ListView1.ListItems.Count Then
    '-> On ajoute en derni�re position
    Set xItem = Me.ListView1.ListItems.Add(, KeyDD, Entry(2, KeyDD, "|"))
    xItem.SubItems(1) = Libel
    xItem.SubItems(2) = Format(Cible, "0000")
Else
    '-> Test si on ajoute 1 ou non � l'index
    If IndexDD > Cible Then Cible = Cible + 1
    Set xItem = Me.ListView1.ListItems.Add(Cible, KeyDD, Entry(2, KeyDD, "|"))
    xItem.SubItems(1) = Libel
    xItem.SubItems(2) = Format(Cible, "0000")
End If

'-> Raffraichir l'�cran
Me.ListView1.Refresh

'-> Message des index
For Each xItem In Me.ListView1.ListItems
    '-> Pointer sur le champ associ�
    Set aField = aFname.Fields(Entry(2, xItem.Key, "|"))
    aField.Ordre = xItem.Index
    '-> Afficher ses propri�t�s
    DisplayFieldProp aField
Next

'-> Enregistrer le sch�ma
aIdent.ServeurDOG.Save_Schema

End Sub

Private Sub ListView1_OLEDragOver(Data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single, State As Integer)

Dim Item As ListItem
Dim i As Integer
Dim IsBadd As Boolean

'-> Essayer de r�cup�rer l'objet sur lequel on passe
Set Item = Me.ListView1.HitTest(x, y)
If Item Is Nothing Then Exit Sub

'-> S�lectionn� l'objet
Item.Selected = True

End Sub

Private Sub ListView1_OLEStartDrag(Data As MSComctlLib.DataObject, AllowedEffects As Long)

'-> Cr�er  La liste des index non disponibles
If Me.ListView1.SelectedItem.Index = Me.ListView1.ListItems.Count Then
    '-> On est sur le dernier, donc n'interdire que le dernier
    ReDim BadIndex(1 To 1)
    BadIndex(1) = Me.ListView1.SelectedItem.Index
ElseIf Me.ListView1.SelectedItem.Index = 1 Then
    ReDim BadIndex(1 To 1)
    BadIndex(1) = 1
Else
    '-> On n'a pas le droit � celui-en cours et celui du dessus
    ReDim BadIndex(1 To 2)
    BadIndex(1) = Me.ListView1.SelectedItem.Index
    BadIndex(2) = Me.ListView1.SelectedItem.Index - 1
End If

'-> Garder la cl� et l'index de l'�l�ment que l'on va d�placer
KeyDD = Me.ListView1.SelectedItem.Key
IndexDD = Me.ListView1.SelectedItem.Index

'-> Indiquer que l'on peut faire du DD
If Me.ListView1.SortKey = 2 And Me.ListView1.SortOrder = lvwAscending Then
    OkDD = True
Else
    OkDD = False
End If

End Sub

Private Sub ListView2_DblClick()

'---> Afficher les propri�t�s de l'index

Dim aIname As Object

'-> Quitter si pas d'index s�lectionn�
If Me.ListView2.SelectedItem Is Nothing Then Exit Sub

'-> Pointer sur l'index
Set aIname = aFname.Inames(Entry(2, Me.ListView2.SelectedItem.Key, "|"))

'-> charger la feuille de propri�t�
Load frmPropIndex
Set frmPropIndex.Iname = aIname

'-> Afficher ses propri�t�s
frmPropIndex.Init

'-> Afficher la feuille
frmPropIndex.Show vbModal

'-> Afficher les propri�t�s
DisplayInameProp aIname

'-> Enregistrer si necessaire
If strRetour <> "" Then aIdent.ServeurDOG.Save_Schema

'-> Formatter les entetes
FormatListView Me.ListView2
Me.ListView2.Refresh

End Sub

Private Sub ListView2_ItemClick(ByVal Item As MSComctlLib.ListItem)

Dim aIname As Object

'-> Pointer sur l'index sp�cifi�
Set aIname = aFname.Inames(Entry(2, Item, "|"))

'-> Afficher les champs qui compose cet index
DisplayInameFields aIname

End Sub

Private Sub ListView2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim ToDel As Boolean
Dim ToProp As Boolean
Dim ToAdd As Boolean
Dim ToReindex As Boolean

If Button <> vbRightButton Then Exit Sub

'-> Menu
If Not Me.ListView2.SelectedItem Is Nothing Then
    ToDel = True
    ToProp = True
    ToReindex = True
End If
If aFname.IndexDirect = "" Then ToAdd = True
Me.mnuDelIname.Enabled = ToDel
Me.mnuEditIname.Enabled = ToProp
Me.mnuAddIname.Enabled = ToAdd
Me.mnuReindex.Enabled = ToReindex

'-> Afficher le menu
strRetour = ""
Me.PopupMenu Me.mnuIname

'-> Tester la variable de retour
If strRetour = "" Then Exit Sub

'-> Selon le menu  s�lectionn�
Select Case strRetour
    Case "ADD"
        '-> Ajouter un index
        AddIname
    Case "DEL"
        '-> Supprimer l'index
        DelIname
    Case "PROP"
        '-> Afficher la page de propri�t�
        ListView2_DblClick
    Case "REINDEX"
        Reindex
End Select

'-> Vider la variable de retour
strRetour = ""

End Sub
Private Sub Reindex()

'-> Charger la feuille de r�indexation
Load frmReindex

'-> Initialiser les valeurs
frmReindex.Label2.Caption = aIdent.Code & " - " & aIdent.Libel
frmReindex.Label2.Tag = aIdent.Code

aFname.CodeLangue = OpeCodeLangue
frmReindex.Label4.Caption = aFname.Num_Fname & " - " & aFname.Designation
frmReindex.Label4.Tag = aFname.Num_Fname

frmReindex.Label6.Caption = Me.ListView2.SelectedItem.Text & " - " & Me.ListView2.SelectedItem.SubItems(1)
frmReindex.Label6.Tag = Me.ListView2.SelectedItem.Text

'-> Afficher la feuille
frmReindex.Show vbModal

End Sub

Private Sub DelIname()

'---> Cette proc�dure supprime un index de la table

Dim aIfield As Object
Dim aIname As Object
Dim Rep As VbMsgBoxResult
Dim aLinkFname As Object
Dim aEmilieObj As Object

'-> Pointer sur l'index
Set aIname = aFname.Inames(Entry(2, Me.ListView2.SelectedItem.Key, "|"))
aIname.CodeLangue = OpeCodeLangue

'-> V�rifier si ce n'est pas l'index de jointure
If aIname.Num_Iname = aFname.IndexJointure Then
    MsgBox "Impossible de supprimer l'index de jointure.", vbExclamation + vbOKOnly, "Erreur"
    Exit Sub
End If

'-> Demander confirmation 'MESSPROG
Rep = MsgBox("Supprimer l'index : " & aIname.Num_Iname & " -> " & aIname.Designation, vbQuestion + vbYesNo, "Confirmation")
If Rep = vbNo Then Exit Sub

'-> Supprimer toutes les r�f�rences des champs dans le catalogue
For Each aIfield In aIname.iFields
    '-> Pointer sur l'objet du catalogue associ�
    Set aEmilieObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & aIfield.Num_Field)
    '-> Pointer sur l'objet d'affectation de la table
    Set aLinkFname = aEmilieObj.fNames(aFname.Num_Fname)
    '-> Supprimer l'objet d'affectation d'index
    aLinkFname.LinkInames.Remove (aIname.Num_Iname)
Next 'Pour tous les champs de l'index

'-> Vider le browse des champs d'index
Me.ListView3.ListItems.Clear

'-> Supprimer du listview des index
Me.ListView2.ListItems.Remove ("INAME|" & aIname.Num_Iname)

'-> Supprimer l'objet Index
aFname.Inames.Remove (aIname.Num_Iname)

'-> Enregistrer le sch�ma
aIdent.ServeurDOG.Save_Schema

'-> S'il  reste un index : le s�lectionn�
If Me.ListView2.ListItems.Count <> 0 Then ListView2_ItemClick Me.ListView2.ListItems(1)

End Sub

Private Sub AddIname()

'---> Cette proc�dure ajoute un index � la table
Dim aIname As Object
Dim x As ListItem

'-> Ajouter via le serveur
Set aIname = aIdent.ServeurDOG.AddIname(aFname.Num_Fname)

'-> Enregistrer le sch�ma
aIdent.ServeurDOG.Save_Schema

'-> Ajouter dans le listview
Set x = Me.ListView2.ListItems.Add(, "INAME|" & aIname.Num_Iname, aIname.Num_Iname)
x.Selected = True

'-> Afficher ses champs
ListView2_ItemClick x

'-> Afficher sa liste de propri�t�
ListView2_DblClick

'-> Mettre  � jour ses propri�t�s
DisplayInameProp aIname

'-> Enregistrer si necessaire
If strRetour <> "" Then aIdent.ServeurDOG.Save_Schema

'-> Formatter les entetes
FormatListView Me.ListView2

End Sub


Private Sub ListView3_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

'-> Trier sur les entetes de colonne
Me.ListView3.SortKey = ColumnHeader.Index - 1
If Me.ListView3.SortOrder = lvwAscending Then
    Me.ListView3.SortOrder = lvwDescending
Else
    Me.ListView3.SortOrder = lvwAscending
End If
Me.ListView3.Sorted = True

'-> V�rifier que la colonne de tri soit bien la colonne d'ordre
If Me.ListView3.SortKey = 2 Then
    If Me.ListView3.SortOrder = lvwAscending Then
        OkDD = True
    Else
        OkDD = False
    End If
Else
    OkDD = False
End If

End Sub

Private Sub ListView3_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

'---> Afficher le menu

Dim ToProp As Boolean
Dim ToDel As Boolean

'-> Quitter si pas d'index s�lectionn�
If Me.ListView2.SelectedItem Is Nothing Then Exit Sub

'-> Quitter si pas bouton droit
If Button <> vbRightButton Then Exit Sub

'-> Init des menus
If Me.ListView3.SelectedItem Is Nothing Then
    Me.mnuEditIField.Enabled = False
    Me.mnuDelIField.Enabled = False
Else
    Me.mnuEditIField.Enabled = True
    Me.mnuDelIField.Enabled = True
End If
    
'-> Vider la variable de retour
strRetour = ""

'-> Afficher le menu
Me.PopupMenu Me.mnuIField

'-> Selon la retour
Select Case strRetour
    Case ""
        '-> Quitter
        Exit Sub
    Case "ADD"
        '-> Ajouter un champ
        AddIfield
    Case "DEL"
        '-> Supprimer un champ
        DelIField
    Case "PROP"
        '-> Propri�t� d'un champ
        DisplayFrmIfieldProp
End Select

End Sub

Private Function IsFieldIname(aIname As Object, strIfield As String) As Boolean

'---> Cette proc�dure indique si un champ fait partie d'un index

Dim aIfield As Object

On Error GoTo GestError

'-> Essayer de pointer sur le champ de l'index
Set aIfield = aIname.iFields(strIfield)

'-> Renvoyer une valeur de succ�s
IsFieldIname = True

Exit Function

GestError:
    

End Function

Private Sub DelIField()

'---> Cette proc�dure supprime un champ d'un index

Dim Rep As VbMsgBoxResult
Dim aIfield As Object
Dim aIname As Object
Dim aEmilieObj As Object
Dim aLinkFname As Object

'-> Pointer sur l'index
Set aIname = aFname.Inames(Entry(2, Me.ListView2.SelectedItem.Key, "|"))

'-> Pointer sur le champ de l'index
Set aIfield = aIname.iFields(Entry(2, Me.ListView3.SelectedItem.Key, "|"))

'-> Pointer sur l'objet associ� dans le catalogue
Set aEmilieObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & aIfield.Num_Field)
aEmilieObj.CodeLangue = OpeCodeLangue

'-> Demander confirmation MESSPROG
Rep = MsgBox("Supprimer le champ : " & aIfield.Num_Field & " : " & aEmilieObj.Fill_Label, vbQuestion + vbYesNo, "Confirmation")
If Rep = vbNo Then Exit Sub

'-> Supprimer sa r�f�rence dans l'objet du catalogue
Set aLinkFname = aEmilieObj.fNames(aFname.Num_Fname)
aLinkFname.LinkInames.Remove (aIname.Num_Iname)

'-> Supprimer de l'index
aIname.iFields.Remove (aIfield.Num_Field)

'-> Supprimer de l'affichage
Me.ListView3.ListItems.Remove (Me.ListView3.SelectedItem.Key)

'-> Enregistrer le schema
aIdent.ServeurDOG.Save_Schema

End Sub

Private Sub AddIfield()

'---> Cette proc�dure charge un browse avec la liste des champs

Dim aField As Object
Dim aIfield As Object
Dim aIname As Object
Dim aEmilieObj As Object
Dim Fname As Object
Dim x As ListItem
Dim ToAdd As Boolean


Me.Enabled = False
Screen.MousePointer = 11

'-> Charger la feuille d'affichage des champs
Load frmLiens

'-> Pointer sur l'index
Set aIname = aFname.Inames(Entry(2, Me.ListView2.SelectedItem.Key, "|"))

'-> Ajouter un champ dans la table
For Each aField In aFname.Fields
    '-> Indiquer si le fichier
    ToAdd = Not IsFieldIname(aIname, aField.Num_Field)
    '-> Si on doit ajouter
    If ToAdd Then
        '-> Pointer sur le champ associ� du catalogue
        Set aEmilieObj = aIdent.ServeurDOG.Catalogue(aIdent.Code & "|" & aField.Num_Field)
        '-> Positionner le code langue
        aEmilieObj.CodeLangue = OpeCodeLangue
        '-> Si on doit ajouter dans la table
        Set x = frmLiens.ListView1.ListItems.Add(, "FIELD|" & aEmilieObj.Num_Field, aEmilieObj.Num_Field)
        '-> Ajouter le Libell�
        x.SubItems(1) = aEmilieObj.Fill_Label
        '-> Ajouter le format
        x.SubItems(2) = aEmilieObj.DataType
        '-> La table de correspondance
        If aEmilieObj.Table_Correspondance <> "" Then
            Set Fname = aIdent.ServeurDOG.fNames(aIdent.Code & "|" & aEmilieObj.Table_Correspondance)
            Fname.CodeLangue = OpeCodeLangue
            x.SubItems(3) = aEmilieObj.Table_Correspondance & " - " & Fname.Designation
        End If
        '-> Le progiciel
        x.SubItems(4) = aEmilieObj.Progiciel
    End If
Next 'Pour tous les objets du catalogue

'-> Justifier les colonnes
FormatListView frmLiens.ListView1

'-> Vider la variable de retour et afficher la feuille
Screen.MousePointer = 0
strRetour = ""
frmLiens.Show vbModal

'-> Tester  le retour
If strRetour = "" Then Exit Sub

'-> Ajouter le champ dans l'index
aIdent.ServeurDOG.AddIfield Entry(2, strRetour, Chr(0)), aFname.Num_Fname, aIname.Num_Iname

'-> Pointer vers l'objet
Set aIfield = aIname.iFields(Entry(2, strRetour, Chr(0)))

'-> Num�ro d'ordre
aIfield.Ordre = aIname.iFields.Count

'-> Ajouter le lien
aIdent.ServeurDOG.AddLinkIname Entry(2, strRetour, Chr(0)), aFname.Num_Fname, aIname.Num_Iname

'-> Enregistrer le sch�ma
aIdent.ServeurDOG.Save_Schema

'-> Ajouter le nouvel objet
Set x = Me.ListView3.ListItems.Add(, "IFIELD|" & aIfield.Num_Field, aIfield.Num_Field)

'-> Afficher ses propri�t�s
DisplayIFieldProp aIfield

'-> Formatter les entetes de colonnes
FormatListView Me.ListView3

'-> Vider la variable de retour
strRetour = ""

End Sub

Private Sub DisplayFrmIfieldProp()

'---> Cette proc�dure affiche la page de propri�t� d'un champ d'index

Dim aIname As Object
Dim aIfield As Object
Dim i As Integer

'-> Pointer sur l'index
Set aIname = aFname.Inames(Entry(2, Me.ListView2.SelectedItem.Key, "|"))

'-> Pointer sur le champ de l'index
Set aIfield = aIname.iFields(Entry(2, Me.ListView3.SelectedItem.Key, "|"))

'-> Charger la feuille de propri�t�
Load frmPropIfield

'-> Setting de l'objet
Set frmPropIfield.iField = aIfield

'-> Charger la liste des conditions
For i = 1 To 10
    frmPropIfield.List1.AddItem Conditions(i)
Next

'-> Initialisation
frmPropIfield.Init

'-> Init des valeurs
frmPropIfield.Label2.Caption = aIfield.Num_Field
frmPropIfield.Label4.Caption = Me.ListView3.SelectedItem.SubItems(1)

'-> Vider la variable de retour
strRetour = ""

'-> Afficher la feuille
frmPropIfield.Show vbModal

'-> Enregistrer le sch�ma
If strRetour <> "" Then aIdent.ServeurDOG.Save_Schema

'-> Afficher la propri�t�  du champ
DisplayIFieldProp aIfield

'-> Formatter les entetes
FormatListView Me.ListView3
Me.ListView3.Refresh

'-> Vider la variable de retour
strRetour = ""

End Sub


Private Sub ListView3_OLEDragDrop(Data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single)

'---> Modification de l'ordre des champs

Dim i As Integer
Dim xItem As ListItem
Dim Cible As Integer
Dim aIfield As Object
Dim aIname As Object
Dim Libel As String

'-> Tester si DD initialis� correctement
If Not OkDD Then Exit Sub

'-> V�rifier que l'on n'a pas lach� n'importe o�
For i = 1 To UBound(BadIndex())
    If Me.ListView3.SelectedItem.Index = BadIndex(i) Then
        OkDD = False
        Exit Sub
    End If
Next

'-> r�cup�rer la cl� de la cible
Cible = Me.ListView3.SelectedItem.Index

'-> R�cup�rer le libell� du champ
Libel = Me.ListView3.ListItems(KeyDD).SubItems(1)

'-> Supprimer l'objet que l'on veut d�placer
Me.ListView3.ListItems.Remove (KeyDD)

'-> Ajouter un champ au bon endroit
If Cible > Me.ListView3.ListItems.Count Then
    '-> On ajoute en derni�re position
    Set xItem = Me.ListView3.ListItems.Add(, KeyDD, Entry(2, KeyDD, "|"))
    xItem.SubItems(1) = Libel
    xItem.SubItems(2) = Format(Cible, "0000")
Else
    '-> Test si on ajoute 1 ou non � l'index
    If IndexDD > Cible Then Cible = Cible + 1
    Set xItem = Me.ListView3.ListItems.Add(Cible, KeyDD, Entry(2, KeyDD, "|"))
    xItem.SubItems(1) = Libel
    xItem.SubItems(2) = Format(Cible, "0000")
End If

'-> Raffraichir l'�cran
Me.ListView3.Refresh

'-> Pointer sur l'index associ�
Set aIname = aFname.Inames(Entry(2, Me.ListView2.SelectedItem.Key, "|"))

'-> Message des index
For Each xItem In Me.ListView3.ListItems
    '-> Pointer sur le champ associ�
    Set aIfield = aIname.iFields(Entry(2, xItem.Key, "|"))
    aIfield.Ordre = xItem.Index
    '-> Afficher ses propri�t�s
    DisplayIFieldProp aIfield
Next

'-> Enregistrer le sch�ma
aIdent.ServeurDOG.Save_Schema


End Sub

Private Sub ListView3_OLEDragOver(Data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single, State As Integer)

Dim Item As ListItem
Dim i As Integer
Dim IsBadd As Boolean

'-> Essayer de r�cup�rer l'objet sur lequel on passe
Set Item = Me.ListView3.HitTest(x, y)
If Item Is Nothing Then Exit Sub

'-> S�lectionn� l'objet
Item.Selected = True

End Sub

Private Sub ListView3_OLEStartDrag(Data As MSComctlLib.DataObject, AllowedEffects As Long)

'-> Cr�er  La liste des index non disponibles
If Me.ListView3.SelectedItem.Index = Me.ListView3.ListItems.Count Then
    '-> On est sur le dernier, donc n'interdire que le dernier
    ReDim BadIndex(1 To 1)
    BadIndex(1) = Me.ListView3.SelectedItem.Index
ElseIf Me.ListView3.SelectedItem.Index = 1 Then
    ReDim BadIndex(1 To 1)
    BadIndex(1) = 1
Else
    '-> On n'a pas le droit � celui-en cours et celui du dessus
    ReDim BadIndex(1 To 2)
    BadIndex(1) = Me.ListView3.SelectedItem.Index
    BadIndex(2) = Me.ListView3.SelectedItem.Index - 1
End If

'-> Garder la cl� et l'index de l'�l�ment que l'on va d�placer
KeyDD = Me.ListView3.SelectedItem.Key
IndexDD = Me.ListView3.SelectedItem.Index

'-> Indiquer que l'on peut faire du DD
If Me.ListView3.SortKey = 2 And Me.ListView3.SortOrder = lvwAscending Then
    OkDD = True
Else
    OkDD = False
End If

End Sub

Private Sub mnuAddFnameField_Click()
strRetour = "ADD"
End Sub

Private Sub mnuAddIField_Click()
strRetour = "ADD"
End Sub

Private Sub mnuAddIname_Click()
strRetour = "ADD"
End Sub

Private Sub mnuDelFnameField_Click()
strRetour = "DEL"
End Sub

Private Sub mnuDelIField_Click()
strRetour = "DEL"
End Sub

Private Sub mnuDelIname_Click()
strRetour = "DEL"
End Sub

Private Sub mnuEditFnameField_Click()
strRetour = "PROP"
End Sub

Private Sub mnuEditIField_Click()
strRetour = "PROP"
End Sub

Private Sub mnuEditIname_Click()
strRetour = "PROP"
End Sub

Private Sub mnuReindex_Click()
strRetour = "REINDEX"
End Sub
