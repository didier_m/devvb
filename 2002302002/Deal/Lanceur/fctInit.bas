Attribute VB_Name = "fctInit"
Option Explicit

'-> Variables d'acc�s aux fichiers INI
Public B3DIniFile As String
Public IdentIniFile As String
Public BASEIniFile As String
Public OPERATIniFile As String
Public LISTIniFile As String

'-> Variable d'acc�s aux R�pertoires
Public DmdPath As String '-> Dir des demandes
Public RspPath As String '-> Dir des r�ponses
Public LogPath As String '-> Dir des log
Public IntPath As String '-> Dir des interfaces
Public IntSovPath As String '-> Dir de sauvegarde des interfaces
Public HelpPath As String '-> Dir des fichiers d'aide
Public RepInputPath As String '-> Dir des fichiers imports de r�plication
Public RepOutputPath As String '-> Dir des fichiers exports de r�plication
Public ApplicationPath As String '-> R�pertoire des Progr B3D

'-> Variables de gestion et de temporisation
Public Tempo_Acces As Integer
Public Tempo_Dmd As Integer
Public Uc_Id As String

'-> Pour gestion des versions
Public ProductVersion As String '-> Version du produit Monoposte/Serveur
Public B3DVersion As String '-> Version B3D User/ Dev

'-> En version Monoposte
Public IdentSession As String '-> Ident associ�
Public BaseSession As String '-> Base associ�e

'-> Pour gestion de la R�plication
Public IsReplication As Boolean '-> acc�s ou non � la r�plication
Public ReplicaUpdate As Boolean '-> Si on peut modifier les enreg issus de la r�plication

'-> Pour gestion des locks
Public VecteurLock As String

'-> Pour gestion des Erreurs
Public ErrorLoading As Integer

'1 = Erreur de r�cup�ration de la verion Console
'2 = Erreur de r�cup�ration de la verion B3D
'3 = Erreur de r�cup�ration de la verion Internet
'4 = Impossible de trouver le fichier de param�trage de la barre d'outils
'5 = Contenu du fichier barre d'outils vide
'6 = Erreur dans la proc�dure de cr�ation de la barre d'outils

'-> Indique l'op�rateur en cours
Public OpeConsole As String
Public OpeCodeLangue As Integer
Public OpePassWord As String

Public Sub LoadInit()

'---> Cette proc�dure Charger le fichier B3D.ini en m�moire

Dim temp As String

'-> Initialisation des fichiers
B3DIniFile = App.Path & "\Param\B3D.ini"
IdentIniFile = App.Path & "\Param\Idents.ini"
BASEIniFile = App.Path & "\Param\Bases.ini"
OPERATIniFile = App.Path & "\Param\Operats.ini"
LISTIniFile = App.Path & "\Param\List.ini"

'-> Chargement des r�pertoires
DmdPath = Replace(DeCrypt(GetIniFileValue("DIRECTORY", "Demandes", B3DIniFile)), "$APP$", App.Path)
RspPath = Replace(DeCrypt(GetIniFileValue("DIRECTORY", "Reponses", B3DIniFile)), "$APP$", App.Path)
LogPath = Replace(DeCrypt(GetIniFileValue("DIRECTORY", "Log", B3DIniFile)), "$APP$", App.Path)
IntPath = Replace(DeCrypt(GetIniFileValue("DIRECTORY", "Interface", B3DIniFile)), "$APP$", App.Path)
IntSovPath = Replace(DeCrypt(GetIniFileValue("DIRECTORY", "InterfaceSov", B3DIniFile)), "$APP$", App.Path)
HelpPath = Replace(DeCrypt(GetIniFileValue("DIRECTORY", "Help", B3DIniFile)), "$APP$", App.Path)
RepInputPath = Replace(DeCrypt(GetIniFileValue("DIRECTORY", "ReplicaInput", B3DIniFile)), "$APP$", App.Path)
RepOutputPath = Replace(DeCrypt(GetIniFileValue("DIRECTORY", "ReplicaOutput", B3DIniFile)), "$APP$", App.Path)
ApplicationPath = Replace(DeCrypt(GetIniFileValue("DIRECTORY", "APPLICATIONS", B3DIniFile)), "$APP$", App.Path)

'-> R�cup�ration de la version programme
ProductVersion = UCase$(Trim(DeCrypt(GetIniFileValue("CONTROL", "VERSION", B3DIniFile))))
If Trim(ProductVersion) = "" Then ErrorLoading = 1

'-> R�cup�rer l'ident associ� � la console monoposte
IdentSession = UCase$(Trim(DeCrypt(GetIniFileValue("CONTROL", "Ident_Session", B3DIniFile))))

'-> R�cup�rer la base associ�e en version MonoPoste
BaseSession = UCase$(Trim(DeCrypt(GetIniFileValue("CONTROL", "Base_Session", B3DIniFile))))

'-> R�cup�ration de la version B3D
B3DVersion = UCase$(Trim(DeCrypt(GetIniFileValue("B3D", "VERSION", B3DIniFile))))
If Trim(B3DVersion) = "" Then ErrorLoading = 2
    
'-> R�cup�ration du param�trage R�plication
temp = DeCrypt(GetIniFileValue("B3D", "REPLICATION", B3DIniFile))
If Trim(temp) <> "" Then
    If Entry(2, temp, "|") = "1" Then IsReplication = True
End If

'-> Si on peut modifier les enreg issus de la r�plication
temp = DeCrypt(GetIniFileValue("B3D", "REPLICATIONUPDATE", B3DIniFile))
If Trim(temp) = "1" Then ReplicaUpdate = True

'-> Get des locks
temp = DeCrypt(GetIniFileValue("LOCK", "VECTEUR", B3DIniFile))
VecteurLock = temp

'-> Chargement de la temporisation du scan du r�pertoires de connexion
temp = DeCrypt(GetIniFileValue("TEMPORISATION", "TEMPO_ACCES", B3DIniFile))
If temp = "" Then
    Tempo_Acces = 100
Else
    Tempo_Acces = CLng(temp)
End If

'-> Chargement de la temporisation du scan des r�poertoires des demandes pour l'agent B3D
temp = DeCrypt(GetIniFileValue("TEMPORISATION", "TEMPO_DEMANDE", B3DIniFile))
If temp = "" Then
    Tempo_Dmd = 500
Else
    Tempo_Dmd = CLng(temp)
End If

'-> Chargement du num�ro d'UC
temp = DeCrypt(GetIniFileValue("SERVEUR", "UC_ID", B3DIniFile))
If temp = "" Then ErrorLoading = 4
Uc_Id = temp

End Sub

Public Function CheckDateValiditeOperat(Operateur As String) As Boolean

'---> Cette proc�dure v�rifie la date de validit� de l'op�rateur

Dim lpBuffer As String

On Error GoTo BadOperat

'-> Get de la date de validit�
lpBuffer = DeCrypt(GetIniFileValue("OPERATS", Operateur, OPERATIniFile))
If lpBuffer = "" Then GoTo BadOperat
    
'-> V�rifier sa date limite de validation
lpBuffer = Entry(3, lpBuffer, "|")
If UCase$(Trim(lpBuffer)) = "NO_LIMIT" Then
    CheckDateValiditeOperat = True
    Exit Function
End If

'-> Effectuer la comparaison
If Year(Now) > Year(CDate(lpBuffer)) Then GoTo BadOperat
If Month(Now) > Month(CDate(lpBuffer)) Then GoTo BadOperat
If Day(Now) > Day(CDate(lpBuffer)) Then GoTo BadOperat

'-> Controle Ok
CheckDateValiditeOperat = True
Exit Function


BadOperat:
    CheckDateValiditeOperat = False
    
End Function
Public Function CheckMultiLog(Operat As String) As Boolean

'---> Cette proc�dure renvoie true si on � droit au multilog

Dim temp As String

'-> R�cup�rer le param�trage de l'op�rateur
temp = GetIniFileValue("OPERATS", Operat, OPERATIniFile)
If Trim(temp) = "" Then Exit Function

'-> Tester le multiLog
If Entry(4, temp, "|") = "1" Then CheckMultiLog = True
    
End Function

Public Function CheckPassWord(Operat As String, PassWord As String) As Boolean

'---> Cette proc�dure v�rifie le mot de passe sp�cifi� pour un op�rateur

Dim temp As String

'-> R�cup�rer le param�trage de l'op�rateur
temp = GetIniFileValue("OPERATS", Operat, OPERATIniFile)
If Trim(temp) = "" Then Exit Function

temp = DeCrypt(temp)

'-> V�rifier le password
If Entry(1, temp, "|") = PassWord Then
    '-> R�cup�rer son code op�rateur
    If Not IsNumeric(Entry(2, temp, "|")) Then
        OpeCodeLangue = 1
    Else
        OpeCodeLangue = CInt(Entry(2, temp, "|"))
    End If
    '-> Retourner une valeur de succ�s
    CheckPassWord = True
End If

End Function

