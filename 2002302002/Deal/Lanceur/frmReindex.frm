VERSION 5.00
Begin VB.Form frmReindex 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "R�indexation d'un fichier de la base"
   ClientHeight    =   2655
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   6990
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2655
   ScaleWidth      =   6990
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Propri�t�s : "
      Height          =   2655
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6975
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   10
         Left            =   240
         Top             =   1560
      End
      Begin VB.PictureBox picOk 
         AutoRedraw      =   -1  'True
         Height          =   975
         Left            =   1440
         ScaleHeight     =   915
         ScaleWidth      =   3675
         TabIndex        =   8
         Top             =   1440
         Width           =   3735
      End
      Begin VB.CommandButton Command1 
         Caption         =   "R�indexer"
         Height          =   975
         Left            =   5280
         Picture         =   "frmReindex.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   1440
         Width           =   1575
      End
      Begin VB.Label Label6 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1440
         TabIndex        =   6
         Top             =   1080
         Width           =   5415
      End
      Begin VB.Label Label5 
         Caption         =   "Index : "
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1440
         TabIndex        =   4
         Top             =   720
         Width           =   5415
      End
      Begin VB.Label Label3 
         Caption         =   "Table : "
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   720
         Width           =   1215
      End
      Begin VB.Label Label2 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1440
         TabIndex        =   2
         Top             =   360
         Width           =   5415
      End
      Begin VB.Label Label1 
         Caption         =   "Identifiant : "
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmReindex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim TailleLue As Long
Dim TailleTotale As Long

Private Sub Command1_Click()

Dim Rep As VbMsgBoxResult
Dim Date1 As Date
Dim Ligne As String

'-> Afficherle choix de l'agent
strRetour = ""
frmChxAgent.Ident = Me.Label2.Tag
frmChxAgent.Show vbModal

'-> Quitter si pas d'agent s�lectionn�
If strRetour = "" Then Exit Sub

'-> Pointer sur l'objet connexion
Set aCon = Connexions(strRetour)

'-> Demander confirmation MESSPROG
Rep = MsgBox(GetMesseprog("MESSAGE", "33"), vbExclamation + vbYesNo, GetMesseprog("COMMUN", "12"))
If Rep = vbNo Then Exit Sub

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

Date1 = Now
TailleTotale = 100
Me.Timer1.Enabled = True

'-> Initialiser une connexion
aCon.aExchange.InitExchange "RIX"
aCon.aExchange.strWrite "B3D_REINDEX~" & Me.Label2.Tag & "�" & Me.Label4.Tag & "�" & Me.Label6.Tag

'-> Se mettre en attente de la r�ponse
If Not aCon.aExchange.SendExchange(True) Then   'MESSPROG
    MsgBox GetMesseprog("MESSAGE", "34"), vbCritical + vbOKOnly, GetMesseprog("COMMUN", "1")
    Unload Me
    Exit Sub
End If

'-> Lecture de la ligne
Ligne = aCon.aExchange.GetLigne(1)

'-> Afficher la r�ponse
MsgBox GetMesseprog("MESSAGE", "35") & Chr(13) & "D�but : " & Date1 & Chr(13) & "Fin : " & Now & Chr(13) & "R�indexation : " & Entry(3, Ligne, "~"), vbExclamation + vbOKOnly, "Fin du traitement"

'-> D�bloquer l'�cran
Me.Enabled = True
Screen.MousePointer = 0
Me.Timer1.Enabled = False

End Sub

Private Sub Timer1_Timer()

'-> Incr�menter le conteur
TailleLue = TailleLue + 1

'-> Dessin de la temporisation
DrawLoadding
DoEvents

'-> Mise � jour
If TailleLue = TailleTotale Then
    picOk.Cls
    TailleLue = 1
End If

End Sub

Private Sub DrawLoadding()

'---> Cette proc�dure  dessine une barre de temporisation

DoEvents
picOk.Line (0, 0)-((TailleLue / TailleTotale) * picOk.ScaleWidth, picOk.ScaleHeight), &H800000, BF


End Sub

