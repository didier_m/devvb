VERSION 5.00
Begin VB.Form frmSession 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   2220
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4875
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2220
   ScaleWidth      =   4875
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picCom 
      Height          =   255
      Left            =   120
      ScaleHeight     =   195
      ScaleWidth      =   315
      TabIndex        =   5
      Top             =   1800
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   375
      Left            =   1080
      TabIndex        =   4
      Top             =   1800
      Width           =   1815
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   375
      Left            =   3000
      TabIndex        =   3
      Top             =   1800
      Width           =   1815
   End
   Begin VB.Image Image5 
      Height          =   240
      Left            =   480
      Picture         =   "frmSession.frx":0000
      Top             =   1680
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image Image4 
      Height          =   240
      Left            =   120
      Picture         =   "frmSession.frx":014A
      Top             =   1560
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Label Label3 
      Height          =   255
      Left            =   720
      TabIndex        =   2
      Top             =   1200
      Width           =   3855
   End
   Begin VB.Image Image3 
      Height          =   240
      Left            =   240
      Picture         =   "frmSession.frx":0294
      Top             =   1200
      Width           =   240
   End
   Begin VB.Label Label2 
      Height          =   255
      Left            =   720
      TabIndex        =   1
      Top             =   720
      Width           =   3855
   End
   Begin VB.Image Image2 
      Height          =   240
      Left            =   240
      Picture         =   "frmSession.frx":03DE
      Top             =   720
      Width           =   240
   End
   Begin VB.Label Label1 
      Height          =   255
      Left            =   720
      TabIndex        =   0
      Top             =   240
      Width           =   3855
   End
   Begin VB.Image Image1 
      Height          =   240
      Left            =   240
      Picture         =   "frmSession.frx":0528
      Top             =   240
      Width           =   240
   End
End
Attribute VB_Name = "frmSession"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



Private Sub Command2_Click()

'---> Demande d'arret de la session

Dim Rep As VbMsgBoxResult
Dim aCon As clsConnect
Dim aBase As clsBase
Dim aIdx As clsIdx
Dim aIdent As clsIdent

'-> Demander si on doit arreter la session
Rep = MsgBox(GetMesseprog("SESSION", "2"), vbQuestion + vbYesNo, GetMesseprog("SESSION", "1"))
If Rep = vbNo Then Exit Sub

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> Pointer sur la base
Set aBase = Bases(1)

'-> Pointer sur l'agent IDX
Set aIdx = aBase.Idx(1)

'-> Tenter d'arr�ter le serveur IDX
UnloadIDX aIdx, aBase

'-> Pointer sur l'objet de connexion
Set aCon = Connexions("CON|1")

'-> Envoyer un ordre de fin
UnloadAgent aCon

'-> Pointer sur l'ident
Set aIdent = Idents(1)

'-> Arreter le serveur DOG
StopDOG aIdent

'-> Mettre � jout le Libell� du tree
mdiConsole.TreeNaviga.Nodes("NODERUNSESSION").Text = GetMesseprog("MENU", "2")

'-> Mettre � jour son icone
mdiConsole.TreeNaviga.Nodes("NODERUNSESSION").Image = "RUNSESSION"

'-> Mettre � jour la propri�t� globale
IsRunningSession = False

'-> Mettre � jour les icones
SetImageNotOk

'-> D�bloquer les boutons
Me.Command1.Enabled = True
Me.Command2.Enabled = False

'-> D�bloquer l'�cran
Me.Enabled = True
Screen.MousePointer = 0

End Sub

Private Sub UnloadAgent(aCon As clsConnect)

'---> Cette proc�dure est charg�e d'interrompre un agent B3D en cours de travail

'-> Envoyer l'ordre de d�connexion
aCon.aExchange.Disconnect

'-> Se mettre en attente de la r�ponse
Do While Not aCon.EndProg
    '-> Rendre la main � la cpu
    DoEvents
Loop

'-> Passer la matrice � false
Matrice_Num_Agent_B3D(CInt(aCon.IndexAgent)) = False
        
'-> Supprimer l'objet de la collection des connexions en cours
Connexions.Remove "CON|" & aCon.IndexAgent

End Sub


Private Sub Form_Load()

'---> Gestion des Messprog
Me.Caption = GetMesseprog("MENU", "1")
Me.Label1.Caption = GetMesseprog("SESSION", "3")
Me.Label2.Caption = GetMesseprog("SESSION", "4")
Me.Label3.Caption = GetMesseprog("SESSION", "5")
Me.Command1.Caption = GetMesseprog("MENU", "2")
Me.Command2.Caption = GetMesseprog("MENU", "3")

'-> Images
If IsRunningSession Then
    '-> Mettre un check sur les images
    SetImageOk
    '-> Bouton Run � false
    Me.Command1.Enabled = False
Else
    '-> Bouton Stop � false
    Me.Command2.Enabled = False
End If

End Sub

Private Sub SetImageOk()

Me.Image1.Picture = Me.Image4.Picture
Me.Image2.Picture = Me.Image4.Picture
Me.Image3.Picture = Me.Image4.Picture

Me.Label1.Enabled = False
Me.Label2.Enabled = False
Me.Label3.Enabled = False

End Sub

Private Sub SetImageNotOk()

Me.Image1.Picture = Me.Image5.Picture
Me.Image2.Picture = Me.Image5.Picture
Me.Image3.Picture = Me.Image5.Picture

Me.Label1.Enabled = True
Me.Label2.Enabled = True
Me.Label3.Enabled = True

End Sub
