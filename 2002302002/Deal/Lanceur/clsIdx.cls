VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsIdx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Num_Idx As String '-> Num�ro d'identification de l'agent B3D
Public Hdl_Retour_Com As Long '-> Handle de communication de l'idx
Public Database As String '-> Nom de la base de donnees associ� � cet agent
Public EndIdx As Boolean  '-> Indique si l'agent IDX est mort
Public NbCreate As Long '-> Nb de cr�ate trait� par l'idx
Public NbDelete As Long '-> Nb delete trait� par l'idx
Public NbUpdate As Long '-> Nb d'update trait� par l'idx
Public NbIndex As Long '-> Nb d'index trait�
Public NbAgent As Integer '-> Nombre d'agent B3D affect� � cet IDX
Public IsWorking As Boolean '-> Indique que l'agent IDX est en train de tourner
Public NbLock As Long '-> Indique le nombre de lock trait�s depuis son lancement
