VERSION 5.00
Begin VB.Form frmConnect 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   2430
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6645
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2430
   ScaleWidth      =   6645
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "Command2"
      Height          =   615
      Left            =   5040
      TabIndex        =   14
      Top             =   2640
      Width           =   855
   End
   Begin VB.Frame Frame1 
      Height          =   2415
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   6615
      Begin VB.ComboBox Combo5 
         Height          =   315
         Left            =   1440
         TabIndex        =   12
         Top             =   1320
         Width           =   3735
      End
      Begin VB.ComboBox Combo4 
         Height          =   315
         Left            =   1440
         TabIndex        =   2
         Top             =   960
         Width           =   3735
      End
      Begin VB.TextBox Text1 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1440
         PasswordChar    =   "*"
         TabIndex        =   4
         Top             =   2040
         Width           =   3735
      End
      Begin VB.CommandButton Command1 
         Height          =   2055
         Left            =   5280
         Picture         =   "frmConnect.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   240
         Width           =   1215
      End
      Begin VB.ComboBox Combo3 
         Height          =   315
         Left            =   1440
         TabIndex        =   1
         Top             =   600
         Width           =   3735
      End
      Begin VB.ComboBox Combo2 
         Height          =   315
         Left            =   1440
         TabIndex        =   3
         Top             =   1680
         Width           =   3735
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   1440
         TabIndex        =   0
         Top             =   240
         Width           =   3735
      End
      Begin VB.Label Label5 
         Caption         =   "Label1"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   1370
         Width           =   1215
      End
      Begin VB.Label Label6 
         Caption         =   "Label1"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   1000
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "Label4"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   2040
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Label1"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   660
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Label1"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   1720
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmConnect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()

'---> V�rifier que tout soit saisi

Dim ErrorCode As String
Dim aCon As B3DConnexion
Dim Ligne As String
Dim TraceMode As String
Dim DebugMode As String
Dim CryptMode As String

'-> Effectuer les controles
If Me.Combo1.ListIndex = -1 Then
    MsgBox GetMesseprog("MESSAGE", "8"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "1")
    Me.Combo1.SetFocus
    Exit Sub
End If

If Me.Combo2.ListIndex = -1 Then
    MsgBox GetMesseprog("MESSAGE", "9"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "1")
    Me.Combo2.SetFocus
    Exit Sub
End If

If Me.Combo3.ListIndex = -1 Then
    MsgBox GetMesseprog("MESSAGE", "10"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "1")
    Me.Combo3.SetFocus
    Exit Sub
End If

If Me.Combo4.ListIndex = -1 Then
    MsgBox GetMesseprog("MESSAGE", "42"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "1")
    Me.Combo4.SetFocus
    Exit Sub
End If

If Me.Combo5.ListIndex = -1 Then
    MsgBox GetMesseprog("MESSAGE", "43"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "1")
    Me.Combo5.SetFocus
    Exit Sub
End If

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> Cr�er un fichier de demande de connexion
Set aCon = New B3DConnexion

'-> Initialisation des r�pertoires d'�change
aCon.DmdPath = DmdPath
aCon.RspPath = RspPath

'-> Setting du mode trace
If Me.Combo3.ListIndex = 0 Then
    TraceMode = "0"
Else
    TraceMode = "1"
End If

'-> Setting du mode debug
If Me.Combo4.ListIndex = 0 Then
    DebugMode = "0"
Else
    DebugMode = "1"
End If

'-> Setting du mode cryptage
If Me.Combo5.ListIndex = 0 Then
    CryptMode = "0"
Else
    CryptMode = "1"
End If

'-> Connecter un nouvel Agent B3D
If Not aCon.Connect(Me.Combo2.Text, Me.Text1.Text, Trim(UCase$(Entry(1, Me.Combo1.Text, "-"))), DebugMode, TraceMode, CryptMode, True) Then
    '-> Afficher un message d'erreur
    MsgBox GetMesseprog("MESSAGE", "11") & Chr(13) & Chr(10) & aCon.ErrorNumber, vbCritical + vbOKOnly, GetMesseprog("COMMUN", "1")
    '-> Fin de la proc�dure
    GoTo ReleaseInt
End If

'-> Connexion OK : ajouter dans la liste des connexions
Connexions.Add aCon, "CON|" & CInt(aCon.IdAgent)

Screen.MousePointer = 11
Unload Me

ReleaseInt:
    '-> D�bloquer l'�cran
    Me.Enabled = True
    Screen.MousePointer = 0

End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Form_Load()

Dim lpBuffer As String
Dim i As Integer
Dim aCon As clsConnectConsole
Dim aIdent As clsIdent

'-> Titre de la feuille
Me.Caption = GetMesseprog("NEWCONNECT", "1")

'-> Propri�t�s
Me.Label1.Caption = GetMesseprog("NEWCONNECT", "2")
Me.Label2.Caption = GetMesseprog("NEWCONNECT", "3")
Me.Label3.Caption = GetMesseprog("NEWCONNECT", "4")
Me.Label4.Caption = GetMesseprog("COMMUN", "15")
Me.Label6.Caption = GetMesseprog("NEWCONNECT", "7")
Me.Label5.Caption = GetMesseprog("NEWCONNECT", "8")

'-> Charger la liste des idents autoris�s
For Each aIdent In Idents
    Me.Combo1.AddItem aIdent.Code & " - " & aIdent.Libel
Next

'-> Charger la liste des op�rateurs
lpBuffer = GetListeOperat()
If Trim(lpBuffer) <> "" Then
    For i = 1 To NumEntries(lpBuffer, Chr(0))
        Me.Combo2.AddItem Entry(i, lpBuffer, Chr(0))
    Next
End If

'-> Charger la liste des traces
Me.Combo3.AddItem GetIniFileValue("TRACE", "1", MessprogIniFile)
Me.Combo3.AddItem GetIniFileValue("TRACE", "2", MessprogIniFile)

'-> Charger la liste des debug
Me.Combo4.AddItem GetIniFileValue("TRACE", "3", MessprogIniFile)
Me.Combo4.AddItem GetIniFileValue("TRACE", "4", MessprogIniFile)

'-> Charger les types de crypt
Me.Combo5.AddItem GetIniFileValue("TRACE", "5", MessprogIniFile)
Me.Combo5.AddItem GetIniFileValue("TRACE", "6", MessprogIniFile)

'-> Libell� du bouton
Me.Command1.Caption = GetMesseprog("NEWCONNECT", "5")

'-> Pour toutes le combos, s�lectionner la premi�re entr�e
If Me.Combo1.ListCount <> 0 Then Me.Combo1.ListIndex = 0
If Me.Combo2.ListCount <> 0 Then Me.Combo2.ListIndex = 0
If Me.Combo3.ListCount <> 0 Then Me.Combo3.ListIndex = 0
If Me.Combo4.ListCount <> 0 Then Me.Combo4.ListIndex = 0
If Me.Combo5.ListCount <> 0 Then Me.Combo5.ListIndex = 0

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Command1_Click

End Sub


