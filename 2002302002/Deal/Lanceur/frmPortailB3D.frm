VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPortailB3D 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   6615
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8310
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6615
   ScaleWidth      =   8310
   Begin TabDlg.SSTab SSTab1 
      Height          =   6615
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   11668
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "frmPortailB3D.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "ImageList2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Timer1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "ListView1"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Command2"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "ImageList1"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).ControlCount=   6
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "frmPortailB3D.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame2"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Tab 2"
      TabPicture(2)   =   "frmPortailB3D.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   4800
         Top             =   3360
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   1
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPortailB3D.frx":0054
               Key             =   "OPE"
            EndProperty
         EndProperty
      End
      Begin VB.Frame Frame2 
         Caption         =   "Frame2"
         Height          =   6015
         Left            =   -74880
         TabIndex        =   7
         Top             =   480
         Width           =   8055
         Begin VB.CommandButton Histo 
            Height          =   855
            Left            =   2760
            Picture         =   "frmPortailB3D.frx":03EE
            Style           =   1  'Graphical
            TabIndex        =   12
            Top             =   5040
            Width           =   1215
         End
         Begin VB.CommandButton EditOpe 
            Height          =   855
            Left            =   4080
            Picture         =   "frmPortailB3D.frx":10B8
            Style           =   1  'Graphical
            TabIndex        =   11
            Top             =   5040
            Width           =   1215
         End
         Begin VB.CommandButton AddOpe 
            Height          =   855
            Left            =   6720
            Picture         =   "frmPortailB3D.frx":1D82
            Style           =   1  'Graphical
            TabIndex        =   10
            Top             =   5040
            Width           =   1215
         End
         Begin VB.CommandButton DelOpe 
            Height          =   855
            Left            =   5400
            Picture         =   "frmPortailB3D.frx":2A4C
            Style           =   1  'Graphical
            TabIndex        =   9
            Top             =   5040
            Width           =   1215
         End
         Begin MSComctlLib.ListView ListView2 
            Height          =   4695
            Left            =   120
            TabIndex        =   8
            Top             =   240
            Width           =   7815
            _ExtentX        =   13785
            _ExtentY        =   8281
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            SmallIcons      =   "ImageList1"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.CommandButton Command2 
         Height          =   1215
         Left            =   6120
         Picture         =   "frmPortailB3D.frx":3716
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   600
         Width           =   2055
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   4575
         Left            =   120
         TabIndex        =   5
         Top             =   1920
         Width           =   8055
         _ExtentX        =   14208
         _ExtentY        =   8070
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList2"
         SmallIcons      =   "ImageList2"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   10
         Left            =   2640
         Top             =   720
      End
      Begin VB.Frame Frame1 
         Caption         =   "Frame1"
         Height          =   1335
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   5895
         Begin VB.CommandButton Command1 
            Caption         =   "Command1"
            Height          =   375
            Left            =   4560
            TabIndex        =   4
            Top             =   720
            Width           =   1215
         End
         Begin VB.PictureBox Picture1 
            Height          =   375
            Left            =   240
            ScaleHeight     =   315
            ScaleWidth      =   4155
            TabIndex        =   3
            Top             =   720
            Width           =   4215
         End
         Begin VB.Label Label1 
            Caption         =   "Label1"
            Height          =   255
            Left            =   240
            TabIndex        =   2
            Top             =   360
            Width           =   3615
         End
      End
      Begin MSComctlLib.ImageList ImageList2 
         Left            =   5400
         Top             =   360
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   1
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPortailB3D.frx":43E0
               Key             =   "AGENT"
            EndProperty
         EndProperty
      End
   End
End
Attribute VB_Name = "frmPortailB3D"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Dim TailleLue As Long
Dim TailleTotale As Long

Private Sub AddOpe_Click()

'-> Afficher la feuille
frmNewOperat.Show vbModal

End Sub

Private Sub Command1_Click()

'---> Cette proc�dure lance ou arrete le serveur d'acc�s
Dim Rep As VbMsgBoxResult

On Error GoTo GestError

If lpsConsole.IsRunning Then
    '-> Demande d'arret du serveur de connexion MESSPROG
    Rep = MsgBox(GetMesseprog("MESSAGE", "7"), vbExclamation + vbYesNo, GetMesseprog("COMMUN", "12"))
    If Rep = vbNo Then Exit Sub
    
    '-> Arreter le serveur d'acc�s
    frmCom.TimerAcces.Enabled = False
    
    '-> Arr�ter la temporisation
    Me.Timer1.Enabled = False
    
    '-> Mettre � jour les infos d'affichage
    lpsConsole.Date = Now
    
    '-> Indiquer que la console ne tourne plus
    lpsConsole.IsRunning = False
    
    '-> Mettre � jour l'affichage
    Me.Label1.Caption = GetMesseprog("PORTAILB3D", "6") & lpsConsole.Date
    
    '-> Libell� du bouton
    Me.Command1.Caption = GetMesseprog("COMMUN", "8")
            
Else
    '-> Demande de lancement du serveur de connexion
    
    '-> Bloquer l'interface
    Screen.MousePointer = 11
    mdiConsole.Enabled = False

    '-> Lancer le serveur d'acces
    frmCom.TimerAcces.Interval = Tempo_Acces
    frmCom.TimerAcces.Enabled = True
    
    '-> Donner la borne maxi
    TailleTotale = 100
        
    '-> Lancer l'animation
    Me.Timer1.Enabled = True
    
    '-> Indiquer que le serveur d'acces tourne
    lpsConsole.IsRunning = True
    lpsConsole.Date = Now
    
    '-> Mettre � jour l'affichage
    Me.Label1.Caption = GetMesseprog("PORTAILB3D", "5") & lpsConsole.Date
    
    '-> Libell� du bouton
    Me.Command1.Caption = GetMesseprog("COMMUN", "9")
    
End If

GestError:

'-> Debloquer l'interface
Screen.MousePointer = 0
mdiConsole.Enabled = True
End Sub

Private Sub Command2_Click()
'---> Afficher la feuille de connexion
frmConnect.Show vbModal
End Sub

Private Sub DelOpe_Click()

Dim Rep As VbMsgBoxResult

'-> Ne rien faire si pas d'op�rateur s�lectionn�
If Me.ListView2.SelectedItem Is Nothing Then
    MsgBox GetMesseprog("MESSAGE", "17"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "1")
    Me.ListView2.SetFocus
    Exit Sub
End If

'-> Demander confirmation
Rep = MsgBox(GetMesseprog("MESSAGE", "19") & Chr(13) & Entry(2, Me.ListView2.SelectedItem.Key, "|"), vbExclamation + vbYesNo, GetMesseprog("COMMUN", "12"))
If Rep = vbNo Then Exit Sub

'-> Supprimer dans le fichier ASCII
WritePrivateProfileString "OPERATS", Entry(2, Me.ListView2.SelectedItem.Key, "|"), vbNullString, OPERATIniFile

'-> Supprimer de la console
Me.ListView2.ListItems.Remove (Me.ListView2.SelectedItem.Key)

End Sub

Private Sub EditOpe_Click()

'-> Ne rien faire si pas d'op�rateur s�lectionn�
If Me.ListView2.SelectedItem Is Nothing Then
    MsgBox GetMesseprog("MESSAGE", "17"), vbExclamation + vbOKOnly, GetMesseprog("COMMUN", "1")
    Me.ListView2.SetFocus
    Exit Sub
End If

'-> Charger la feuille
Load frmNewOperat

'-> Initialiser la fen�tre
frmNewOperat.Init Entry(2, Me.ListView2.SelectedItem.Key, "|")

'-> Afficher la feuille
frmNewOperat.Show vbModal

End Sub

Private Sub Form_Load()

'-> Indiquer que la console est visible
lpsConsole.IsVible = True

'-> Bloquer l'onglet des stats
Me.SSTab1.TabVisible(2) = False

'-> Titre de la feuille
Me.Caption = GetMesseprog("PORTAILB3D", "1")

'-> Titre des onglets
Me.SSTab1.TabCaption(0) = GetMesseprog("PORTAILB3D", "1")
Me.SSTab1.TabCaption(1) = GetMesseprog("PORTAILB3D", "2")
Me.SSTab1.TabCaption(2) = GetMesseprog("PORTAILB3D", "3")

'-> Lancer une connexion
Me.Command2.Caption = GetMesseprog("PORTAILB3D", "11")

'-> Etat du portail
Me.Frame1.Caption = GetMesseprog("PORTAILB3D", "4")
If lpsConsole.IsRunning Then
    '-> Le portail est actif
    Me.Label1.Caption = GetMesseprog("PORTAILB3D", "5") & lpsConsole.Date
    '-> Activer l'animation
    Me.Timer1.Enabled = True
    '-> Libell� du bouton
    Me.Command1.Caption = GetMesseprog("COMMUN", "9")
Else
    '-> Le portail est inactif
    Me.Label1.Caption = GetMesseprog("PORTAILB3D", "6") & lpsConsole.Date
    '-> Libell� du bouton
    Me.Command1.Caption = GetMesseprog("COMMUN", "8")
End If

'-> Gestion des op�rateurs
Me.Frame2.Caption = GetMesseprog("PORTAILB3D", "2")
Me.AddOpe.Caption = GetMesseprog("COMMUN", "17")
Me.DelOpe.Caption = GetMesseprog("COMMUN", "18")
Me.EditOpe.Caption = GetMesseprog("COMMUN", "19")
Me.Histo.Caption = GetMesseprog("COMMUN", "20")

'-> Afficher la liste des op�rateurs
DispayListeOperats

'-> Afficher la liste des connexions en cours
DisplayListeConnexions

End Sub

Private Sub DispayListeOperats()

Dim lpBuffer As String
Dim i As Integer
Dim x As ListItem

'-> Entetes des colonnes du browse
Me.ListView2.ColumnHeaders(1).Text = GetMesseprog("PORTAILB3D", "7")
Me.ListView2.ColumnHeaders(2).Text = GetMesseprog("PORTAILB3D", "14")
Me.ListView2.ColumnHeaders(3).Text = GetMesseprog("PORTAILB3D", "15")
Me.ListView2.ColumnHeaders(4).Text = GetMesseprog("PORTAILB3D", "16")

'-> Get de la liste des op�rateurs
lpBuffer = GetListeOperat()

'-> Ajouter l'ensemble des op�rateurs
If Trim(lpBuffer) <> "" Then
    For i = 1 To NumEntries(lpBuffer, Chr(0))
        '-> Ajouter un objet Listitem
        Me.ListView2.ListItems.Add , "OPE|" & Trim(UCase$(Entry(i, lpBuffer, Chr(0)))), Entry(i, lpBuffer, Chr(0)), , "OPE"
        '-> Afficher ses propri�t�s
        DisplayOpeProp Entry(i, lpBuffer, Chr(0))
    Next
End If

'-> Formatter la largeur des colonnes
FormatListView Me.ListView1

End Sub

Public Sub DisplayListeConnexions()

Dim aCon As clsConnectConsole
Dim x As ListItem

'-> Titre des ent�tes de colonne
Me.ListView1.ColumnHeaders(1).Text = GetMesseprog("PORTAILB3D", "7")
Me.ListView1.ColumnHeaders(2).Text = GetMesseprog("COMMUN", "13")
Me.ListView1.ColumnHeaders(3).Text = GetMesseprog("PORTAILB3D", "8")
Me.ListView1.ColumnHeaders(4).Text = GetMesseprog("PORTAILB3D", "9")
Me.ListView1.ColumnHeaders(5).Text = GetMesseprog("PORTAILB3D", "10")

'-> Afficher la liste des connexions
For Each aCon In ConnexionsDescripteur
    '-> Ajouter un objet ListItem
    Set x = Me.ListView1.ListItems.Add(, "CON|" & aCon.IndexAgent, "   " & aCon.Operat, , "AGENT")
    x.SubItems(1) = aCon.DateConnexion
    x.SubItems(2) = Format(aCon.IndexAgent, "0000")
    x.SubItems(3) = aCon.Num_Idx
    x.SubItems(4) = aCon.hdlProcess
Next 'Pour toutes les connexions

'-> Formatter la largeur des colonnes
FormatListView Me.ListView1

End Sub
Private Sub Form_Unload(Cancel As Integer)

'-> Indiquer que la console n'est plus visible
lpsConsole.IsVible = False

End Sub

Private Sub ListView1_DblClick()

'-> V�rifier qu'il y une connexion
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Charger la feuille en m�moire
Load frmPropCon

'-> Initialiser
frmPropCon.Init Me.ListView1.SelectedItem.Key

'-> Afficher la feuille
frmPropCon.Show vbModal

End Sub

Private Sub ListView2_DblClick()

'-> Tester qu'il y a un objet s�lectionn�
If Me.ListView2.SelectedItem Is Nothing Then Exit Sub

'-> Editer
EditOpe_Click

End Sub

Private Sub Timer1_Timer()

'-> Incr�menter le conteur
TailleLue = TailleLue + 1

'-> Dessin de la temporisation
DrawLoadding Me.Picture1, TailleLue, TailleTotale
DoEvents

'-> Mise � jour
If TailleLue = TailleTotale Then
    Me.Picture1.Cls
    TailleLue = 1
End If

End Sub

Public Sub DisplayOpeProp(Operat As String)

'---> Cette fonction Affiche les propri�t�s d'un op�rateur

Dim x As ListItem
Dim lpBuffer As String

'-> Get du param�trage de cet op�rateur
lpBuffer = GetIniFileValue("OPERATS", Operat, OPERATIniFile)
If lpBuffer = "" Then Exit Sub

'-> Decrypter
lpBuffer = DeCrypt(lpBuffer)

'-> Pointer sur l'objet Listitem associ�
Set x = Me.ListView2.ListItems("OPE|" & UCase$(Trim(Operat)))

'-> Code langue
x.SubItems(1) = Format(Entry(2, lpBuffer, "|"), "00")

'-> Date limite
x.SubItems(2) = Entry(3, lpBuffer, "|")

'-> Multilog
If Entry(4, lpBuffer, "|") = "1" Then
    x.SubItems(3) = CStr(True)
Else
    x.SubItems(3) = CStr(False)
End If

End Sub
