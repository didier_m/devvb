VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmLiens 
   BorderStyle     =   5  'Sizable ToolWindow
   ClientHeight    =   4740
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   8145
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   316
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   543
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   5520
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLiens.frx":0000
            Key             =   "NOTFIND"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLiens.frx":015A
            Key             =   "FIND"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8535
      _ExtentX        =   15055
      _ExtentY        =   6800
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Width           =   38100
      EndProperty
   End
End
Attribute VB_Name = "frmLiens"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()

'-> Titre de la feuille
Me.Caption = GetMesseprog("FRMEMILIE", "25")

'-> Libell�s les ent�tes de colonnes
frmLiens.ListView1.ColumnHeaders(1).Text = GetMesseprog("FRMEMILIE", "16")
frmLiens.ListView1.ColumnHeaders(2).Text = GetMesseprog("COMMUN", "22")
frmLiens.ListView1.ColumnHeaders(3).Text = GetMesseprog("FRMEMILIE", "17")
frmLiens.ListView1.ColumnHeaders(4).Text = GetMesseprog("FRMEMILIE", "8")
frmLiens.ListView1.ColumnHeaders(5).Text = GetMesseprog("FRMEMILIE", "7")

End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next

GetClientRect Me.hwnd, aRect

Me.ListView1.Left = 0
Me.ListView1.Top = 0
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom

End Sub

Private Sub ListView1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

'-> Trier sur la colonne
Me.ListView1.SortKey = ColumnHeader.Index - 1
If Me.ListView1.SortOrder = lvwAscending Then
    Me.ListView1.SortOrder = lvwDescending
Else
    Me.ListView1.SortOrder = lvwAscending
End If
Me.ListView1.Sorted = True

End Sub

Private Sub ListView1_ItemClick(ByVal Item As MSComctlLib.ListItem)

On Error Resume Next

strRetour = Me.ListView1.SelectedItem.Text & " - " & Me.ListView1.SelectedItem.SubItems(1) & Chr(0) & Entry(2, Me.ListView1.SelectedItem.Key, "|")
Unload Me

End Sub
