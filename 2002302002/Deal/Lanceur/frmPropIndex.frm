VERSION 5.00
Begin VB.Form frmPropIndex 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   3300
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4050
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3300
   ScaleWidth      =   4050
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Height          =   495
      Left            =   3000
      Picture         =   "frmPropIndex.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   2760
      Width           =   975
   End
   Begin VB.CommandButton Command2 
      Height          =   495
      Left            =   1920
      Picture         =   "frmPropIndex.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   2760
      Width           =   975
   End
   Begin VB.ListBox List1 
      Height          =   645
      Left            =   120
      TabIndex        =   3
      Top             =   1800
      Width           =   3855
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Index unique"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   960
      Width           =   3855
   End
   Begin VB.TextBox Text1 
      Enabled         =   0   'False
      Height          =   285
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   3375
   End
   Begin VB.Label Label2 
      Caption         =   "Groupe : "
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   1440
      Width           =   1815
   End
   Begin VB.Image Image10 
      Height          =   480
      Left            =   3480
      Picture         =   "frmPropIndex.frx":0E14
      Top             =   360
      Width           =   480
   End
   Begin VB.Label Label1 
      Caption         =   "D�signation : "
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1935
   End
End
Attribute VB_Name = "frmPropIndex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Iname As Object '-> Index

Public Sub Init()

Dim i As Integer

'-> Titre de la feuille
Me.Caption = GetMesseprog("FRMINDEX", "1") & Iname.num_iname

'-> Charger la liste des groupes par ident
strRetour = GetIniFileValue("GROUPE_" & Iname.Ident, "", IDENTIniFile, True)
For i = 1 To NumEntries(strRetour, Chr(0))
    If Trim(Entry(i, strRetour, Chr(0))) = "" Then Exit For
    Me.List1.AddItem Entry(2, Entry(i, strRetour, Chr(0)), "=")
Next
strRetour = ""

'-> D�signation
For i = 1 To 10
    Iname.CodeLangue = i
    '-> Code langue de l'index
    If i = OpeCodeLangue Then Me.Text1.Text = Iname.designation
    If Me.Text1.Tag = "" Then
        Me.Text1.Tag = Iname.designation
    Else
        Me.Text1.Tag = Me.Text1.Tag & "|" & Iname.designation
    End If
Next
    
'-> Unique
Me.Check1.Value = Abs(CInt(Iname.IsUnique))

'-> Groupe
For i = 0 To Me.List1.ListCount - 1
    If UCase$(Me.List1.List(i)) = UCase$(Iname.groupe) Then Me.List1.ListIndex = i
Next
    


End Sub

Private Sub Command1_Click()

Dim i As Integer

'-> Mettre � jour les propri�t�s
For i = 1 To 10
    Iname.CodeLangue = i
    Iname.designation = Entry(i, Me.Text1.Tag, "|")
Next

'-> Si unique
If Me.Check1.Value = 1 Then
    Iname.IsUnique = True
Else
    Iname.IsUnique = False
End If

'-> groupe
If Me.List1.ListIndex <> -1 Then
    Iname.groupe = Me.List1.Text
End If

'-> Envoyer une valeur
strRetour = "OK"

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Command2_Click()
strRetour = ""
Unload Me
End Sub

Private Sub Image10_Click()

Dim i As Integer

'-> Charger la feuille
Load frmLangue

'-> Gestion du multiLangue
For i = 1 To 10
    '-> Charger les libell�s par langue
    frmLangue.Text1(i - 1) = Entry(i, Me.Text1.Tag, "|")
Next

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher la feuille
frmLangue.Show vbModal

'-> Tester la variable de retour
If strRetour = "" Then Exit Sub

'-> Mettre � jour la variable TAG
Me.Text1.Tag = strRetour

'-> Mettrre � jour l'affichage
Me.Text1.Text = Entry(OpeCodeLangue, strRetour, "|")


End Sub
