Attribute VB_Name = "fctNewProject"
Option Explicit

'-> Variable d'�change avec le B3D
Public aConnexion As B3DConnexion

'-> Variable de naviagtion
Public aNavigation As B3DNavigation

'-> Collection des buffers associ�s
Public BufferFieldTables As Collection

'-> Indique la table maitresse du programme
Public MasterFname As String

'-> Param�trage de la ligne de commande
Public ParamFile As String
Public ZoneCharge As String

'-> Pointeur vers un objet descriptif de la table en cours de travail
Public aDescro As B3DFnameDescriptif


Sub Main()


Dim aField As B3DField

'-> Analyse de la ligne de commande
If Trim(Command$) = "" Then End

'-> Eclater la ligne de commande
ParamFile = Entry(1, Command$, "�")
ZoneCharge = Entry(2, Command$, "�")

'-> V�rifier si on trouve le fichier de param�trage
If Dir$(ParamFile) = "" Then End

'-> Cr�ation de la connexion
Set aConnexion = New B3DConnexion

'-> Charger son param�trage
If Not aConnexion.ReadParamFormFile(ParamFile) Then End

End Sub
