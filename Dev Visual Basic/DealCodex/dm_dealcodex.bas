Attribute VB_Name = "dm_dealcodex"
Option Explicit

Public Declare Function PostMessage Lib "user32.dll" Alias "PostMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Public Tempo As Integer

Private hdlFile As Integer
Private TempFileName As String
Private Ligne As String

Private Const vbBackslash = "\"
Private Const ALL_FILES = "*.*"

Private Type FILETIME
   dwLowDateTime As Long
   dwHighDateTime As Long
End Type

Private Type WIN32_FIND_DATA
   dwFileAttributes As Long
   ftCreationTime As FILETIME
   ftLastAccessTime As FILETIME
   ftLastWriteTime As FILETIME
   nFileSizeHigh As Long
   nFileSizeLow As Long
   dwReserved0 As Long
   dwReserved1 As Long
   cFileName As String * 260
   cAlternate As String * 14
End Type

Private Type FILE_PARAMS
   bRecurse As Boolean
   bFindOrExclude As Long
   nCount As Long
   nSearched As Long
   sFileNameExt As String
   sFileRoot As String
End Type

Private Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long
   
Private Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
   
Private Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long

Private Declare Function lstrlen Lib "kernel32" Alias "lstrlenW" (ByVal lpString As Long) As Long

Private Declare Function PathMatchSpec Lib "shlwapi" Alias "PathMatchSpecW" (ByVal pszFileParam As Long, ByVal pszSpec As Long) As Long

Private fp As FILE_PARAMS
Private stopSearch As Boolean

Public Enum pTypeLigne
    pEntete
    pColumnHeader
    pData
    pSeparateur
End Enum

Sub Main()
Dim StrError As String

'-> on initialise le message d'erreur
StrError = "Une erreur est survenue lors du chargement"

'-> selon que l'on est en mode console ou pas
If ZonChargGet("CONSOLE") <> "" Then
    '->TODO!!!!!
    
    End
End If

'-> on ouvre la feuille de chargement du Ducs
df_DealCodex.init

End Sub

Public Function ZonChargGet(StrZonCharg As String) As String

'--> cette fonction retourne la valeur d'un zoncharge
Dim zcharge As String
Dim ztrav As String
Dim i As Integer

'-> on regarde si on a changer le command$
zcharge = Entry(2, Command$, "|")
zcharge = Replace(zcharge, "'", "")
zcharge = Replace(zcharge, Chr(34), "")

For i = 2 To NumEntries(zcharge, "_")
  ztrav = UCase$(Trim(Entry(1, Entry(i, zcharge, "_"), "=")))
  Select Case ztrav
    Case UCase(StrZonCharg)
        ZonChargGet = Trim(Entry(2, Entry(i, zcharge, "_"), "="))
        Exit For
  End Select
Next



End Function

Private Function Uncote(strValue As String) As String
'--> cette fonction va permettre de sortir les ''
Uncote = Mid(strValue, 2, Len(strValue) - 2)
End Function

Public Function FileExist(Fichier As String) As Boolean

On Error GoTo GestError

If Dir$(Fichier) <> "" Then FileExist = True
Exit Function

GestError:
    FileExist = False

End Function

Public Function searchExe(strFile As String) As String
'-> cette fonction va chercher les fichiers en fonction du param�trage
'   diff�rentes possibilit�s
'   rien
'   chemin spe|chemin std
'   gloident
Dim sPath As String
Dim sParam As String

stopSearch = False

If Command$ = "" Then
    sParam = "deal"
Else
    sParam = Command$
End If

If InStr(1, sParam, "|") <> 0 Then
    '-> on lit d'abord dans le spe puis le standard
    If Dir$(Entry(1, sParam, "|") & "\" & strFile) <> "" Then
        searchExe = Entry(1, sParam, "|") & "\" & strFile
    Else
        If Dir$(Entry(1, sParam, "|") & "\" & strFile) <> "" Then
            searchExe = Entry(1, sParam, "|") & "\" & strFile
        Else
            searchExe = App.Path & "\" & strFile
        End If
    End If
Else
    '-> � partir du gloident on va essayer de s'y retrouver!
    '-> on pointe sur le chemin de base
    sPath = App.Path
    '-> pour les test
    'sPath = "R:\V6\tools\exe"
    sPath = Replace(sPath, "/", "\")
    '-> on regarde si on est en v51 ou v52
    If InStr(1, sPath, "\V51\") <> 0 Or InStr(1, sPath, "\V52\") <> 0 Then
        If InStr(1, sPath, "\V51\") <> 0 Then
            sPath = Mid(sPath, 1, InStr(1, sPath, "\V51\", vbTextCompare) + 4)
        Else
            sPath = Mid(sPath, 1, InStr(1, sPath, "\V52\", vbTextCompare) + 4)
        End If
        '-> repertoire v52/sophie de l'ident
        If searchExe = "" Then searchExe = SearchForFiles(Replace(sPath, "\deal\", "\" & Trim(sParam) & "\", , , vbTextCompare) & "\Sophie\", strFile)
        '-> repertoire v52/sophie de deal
        If searchExe = "" Then searchExe = SearchForFiles(sPath & "\Sophie\", strFile)
    Else '-> on regarde si on est en V6
        If InStr(1, sPath, "\dog\", vbTextCompare) <> 0 Or InStr(1, sPath, "\tools\", vbTextCompare) <> 0 Then
            sPath = Replace(sPath, "\tools\", "\dog\", , , vbTextCompare)
            sPath = Mid(sPath, 1, InStr(1, sPath, "\dog\", vbTextCompare) + 4)
            '-> on cherche dans le repertoire maquette du client
            If searchExe = "" Then searchExe = SearchForFiles(Replace(sPath, "\dog\", "\" & Trim(sParam) & "\sophie\", , , vbTextCompare), strFile)
            '-> on cherche dans le repertoire spe du client
            If searchExe = "" Then searchExe = SearchForFiles(sPath & "\" & sParam & "\", strFile)
            '-> on cherche dans le repertoire des maquettes
            If searchExe = "" Then searchExe = SearchForFiles(Replace(sPath, "\dog\", "\maq\gui\sophie\", , , vbTextCompare), strFile)
        End If
    End If
    '-> on regarde dans le repertoire courant
    If searchExe = "" Then searchExe = SearchForFiles(App.Path & "\" & Trim(sParam) & "\", strFile)
    If searchExe = "" Then searchExe = SearchForFiles(App.Path & "\", strFile)
    '-> on ramene le repertoire courant
    If searchExe = "" Then searchExe = App.Path & "\" & strFile
End If

End Function

Private Function SearchForFiles(sRoot As String, sfile As String, Optional sFiltreDirectory As String) As String
    '--> cette fonction cherche des fichiers � partir d'une directorie
   Dim WFD As WIN32_FIND_DATA
   Dim hFile As Long
  
   With fp
      .sFileRoot = QualifyPath(sRoot)      'chemin de d�part
      .sFileNameExt = sfile                'fichier (* ? autoris�
      .bRecurse = 1                             'True = recherche recursive
      .bFindOrExclude = 1                       '0=inclure, 1=exclure
   End With
   
   
   hFile = FindFirstFile(sRoot & "*.*", WFD)
   If hFile <> -1 Then
      Do
        If stopSearch = True Then Exit Function
        DoEvents
        'si c'est un repertoire on boucle
         If (WFD.dwFileAttributes And vbDirectory) Then
            If Asc(WFD.cFileName) <> CLng(46) Then
                If fp.bRecurse Then
                    SearchForFiles = SearchForFiles(sRoot & TrimNull(WFD.cFileName) & vbBackslash, sfile)
                End If
            End If
         Else
           'doit etre un fichier..
            If MatchSpec(WFD.cFileName, fp.sFileNameExt) Then
                If sFiltreDirectory <> "" Then
                    If InStr(1, sRoot & TrimNull(WFD.cFileName), sFiltreDirectory) <> 0 Then
                        SearchForFiles = sRoot & TrimNull(WFD.cFileName)
                        stopSearch = True
                        Exit Do
                    End If
                Else
                    SearchForFiles = sRoot & TrimNull(WFD.cFileName)
                    stopSearch = True
                    Exit Do
                End If
            End If
         End If
      Loop While FindNextFile(hFile, WFD)
   End If
   Call FindClose(hFile)
End Function

Private Function QualifyPath(sPath As String) As String
   If Right$(sPath, 1) <> vbBackslash Then
      QualifyPath = sPath & vbBackslash
   Else
      QualifyPath = sPath
   End If
End Function

Private Function TrimNull(startstr As String) As String
   TrimNull = Left$(startstr, lstrlen(StrPtr(startstr)))
End Function

Private Function MatchSpec(sfile As String, sSpec As String) As Boolean
   MatchSpec = PathMatchSpec(StrPtr(sfile), StrPtr(sSpec)) = fp.bFindOrExclude
End Function

Public Sub SkinColorModify(aForm As Form)
'--> ceci est un essai de changement de couleur
Dim Lcolor As Long
Dim aControl As Control
Dim aPicture As PictureBox
Dim bKeepBorder As Boolean
Dim sKinName As String

On Error Resume Next

'-> V�rifier que l'on trouve le fichier d'application du skin
sKinName = searchExe("Skin.ini")
If Dir$(sKinName) = "" Then Exit Sub

'-> Appliquer un skin s'il y en a un
If GetIniFileValue("SKIN", "BODYBACKCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.BodyBackColor = getColor(GetIniFileValue("SKIN", "BODYBACKCOLOR", sKinName))
If GetIniFileValue("SKIN", "BODYFORECOLOR", sKinName) <> "" Then aForm.DogSkinObject1.BodyForeColor = getColor(GetIniFileValue("SKIN", "BODYFORECOLOR", sKinName))
If GetIniFileValue("SKIN", "BORDERCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.BorderColor = getColor(GetIniFileValue("SKIN", "BODYBACKCOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERBACKCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.HeaderBackColor = getColor(GetIniFileValue("SKIN", "HEADERBACKCOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERFORECOLOR", sKinName) <> "" Then aForm.DogSkinObject1.HeaderForeColor = getColor(GetIniFileValue("SKIN", "HEADERFORECOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERLINECOLOR", sKinName) <> "" Then aForm.DogSkinObject1.HeaderLineColor = getColor(GetIniFileValue("SKIN", "HEADERLINECOLOR", sKinName))
If GetIniFileValue("SKIN", "SHADOWCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.ShadowColor = getColor(GetIniFileValue("SKIN", "SHADOWCOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERUSELINE", sKinName) <> "" Then aForm.DogSkinObject1.HeaderUseLine = Val(GetIniFileValue("SKIN", "HEADERUSELINE", sKinName))
If GetIniFileValue("SKIN", "BODYFONTNAME", sKinName) <> "" Then aForm.DogSkinObject1.BodyFont.Name = GetIniFileValue("SKIN", "BODYFONTNAME", sKinName)
If GetIniFileValue("SKIN", "BODYFONTSIZE", sKinName) <> "" Then aForm.DogSkinObject1.BodyFont.Size = CDbl(GetIniFileValue("SKIN", "BODYFONTSIZE", sKinName))
If GetIniFileValue("SKIN", "BODYFONTBOLD", sKinName) <> "" Then aForm.DogSkinObject1.BodyFont.Bold = Val(GetIniFileValue("SKIN", "BODYFONTBOLD", sKinName))
If GetIniFileValue("SKIN", "BODYFONTITALIC", sKinName) <> "" Then aForm.DogSkinObject1.BodyFont.Italic = Val(GetIniFileValue("SKIN", "BODYFONTITALIC", sKinName))
If GetIniFileValue("SKIN", "HEADERICO", sKinName) <> "" Then
    '-> on regarde si on trouve bien l'image
    If Dir(GetIniFileValue("SKIN", "HEADERICO", sKinName)) <> "" Then
        aForm.DogSkinObject1.HeaderIco = LoadPicture(GetIniFileValue("SKIN", "HEADERICO", sKinName))
        aForm.DogSkinObject1.HeaderIcoNa = LoadPicture(GetIniFileValue("SKIN", "HEADERICO", sKinName))
    Else
        '-> on regarde si l'icone n'est pas dans le repertoire courant
        aForm.DogSkinObject1.HeaderIco = LoadPicture(App.Path & "\" & GetIniFileValue("SKIN", "HEADERICO", sKinName))
        aForm.DogSkinObject1.HeaderIcoNa = LoadPicture(App.Path & "\" & GetIniFileValue("SKIN", "HEADERICO", sKinName))
    End If
End If
If GetIniFileValue("SKIN", "HEADERICONA", sKinName) <> "" Then aForm.DogSkinObject1.HeaderUseLine = Val(GetIniFileValue("SKIN", "HEADERICONA", sKinName))
If GetIniFileValue("SKIN", "HEADERFONTNAME", sKinName) <> "" Then aForm.DogSkinObject1.HeaderFont.Name = GetIniFileValue("SKIN", "HEADERFONTNAME", sKinName)
If GetIniFileValue("SKIN", "HEADERFONTSIZE", sKinName) <> "" Then aForm.DogSkinObject1.HeaderFont.Size = CDbl(GetIniFileValue("SKIN", "HEADERFONTSIZE", sKinName))
If GetIniFileValue("SKIN", "HEADERFONTBOLD", sKinName) <> "" Then aForm.DogSkinObject1.HeaderFont.Bold = Val(GetIniFileValue("SKIN", "HEADERFONTBOLD", sKinName))
If GetIniFileValue("SKIN", "HEADERFONTITALIC", sKinName) <> "" Then aForm.DogSkinObject1.HeaderFont.Italic = Val(GetIniFileValue("SKIN", "HEADERFONTITALIC", sKinName))
If GetIniFileValue("SKIN", "KEEPBORDER", sKinName) = "1" Then bKeepBorder = True
'-> on parcours les differents elements pour faire une variation de couleur
For Each aControl In aForm
    If TypeOf aControl Is Shape Then
        If aControl.BackColor = &HF1CFB1 Or aControl.BackColor = 16708585 Then aControl.BackColor = aForm.DogSkinObject1.BodyBackColor
        If aControl.BorderColor = &HC78D4B Then aControl.BorderColor = aForm.DogSkinObject1.BorderColor
    End If
    If TypeOf aControl Is DealCmdButton Or TypeOf aControl Is PictureBox Or TypeOf aControl Is DealCheckBox Or TypeOf aControl Is TextBox Then
        If aControl.BackColor = &HF1CFB1 Or aControl.BackColor = 16579059 Then aControl.BackColor = aForm.DogSkinObject1.BodyBackColor
    End If
    If TypeOf aControl Is DealGrid Then
        aControl.BackColor = aForm.DogSkinObject1.BodyBackColor
        aControl.BackColorFixed = aForm.DogSkinObject1.ShadowColor
        aControl.BackColorSel = aForm.DogSkinObject1.ShadowColor
        aControl.GridLineColor = aForm.DogSkinObject1.HeaderForeColor
    End If
    If TypeOf aControl Is DealTextBox Or TypeOf aControl Is Label Then
        aControl.ForeColor = aForm.DogSkinObject1.HeaderForeColor
    End If
    If TypeOf aControl Is Label And bKeepBorder Then
        aControl.BorderStyle = 0
    End If

Next

End Sub

Public Function GetVariableEnv(strVariable As String) As String

Dim Res As Long
Dim lpbuffer As String

lpbuffer = Space$(500)
Res = GetEnvironmentVariable(strVariable, lpbuffer, Len(lpbuffer))
If Res <> 0 Then
    '-> Faire un trim sur le buffer
    lpbuffer = Mid$(lpbuffer, 1, Res)
Else
    lpbuffer = ""
End If

'-> Retouner la valeur
GetVariableEnv = lpbuffer

End Function


Public Function getColor(sColor As String) As Long
'--> cette fonction reccupere la couleur
Dim r, V, b As Integer
On Error Resume Next
'-> on decompose la couleur
r = Val("&H" & Mid(sColor, 5, 2))
V = Val("&H" & Mid(sColor, 7, 2))
b = Val("&H" & Mid(sColor, 9, 2))

getColor = RGB(r, V, b)

End Function

Public Function GetFileName(MyFile As String) As String

Dim i As Integer

i = InStrRev(MyFile, "\")
If i = 0 Then
    GetFileName = MyFile
Else
    GetFileName = Mid$(MyFile, i + 1, Len(MyFile) - i)
End If


End Function

Public Sub OUVERTURE_CODEX()
Dim zoappli As String
Dim ReturnValue
Dim i As Long
Dim x_C8ligne As String

Exit Sub
ChDir "C:\CODEX8\PROGRAM"                       '--- Activation Programme CODEX
zoappli = "C:\CODEX8\PROGRAM\CODEX.EXE"
ReturnValue = Shell(zoappli, 1)
AppActivate ReturnValue

For i = 1 To 150
    DoEvents
Next i

SendKeys "%{F}", True                           '--- Ouverture Fichier Image
SendKeys "{O}", True
If x_C8ligne = "5000" Then
    SendKeys "C:\codex8\masque\C8_ANTOM_LIGNE1{ENTER}", True
End If
If x_C8ligne = "10000" Then
    SendKeys "C:\codex8\masque\C8_ANTOM_LIGNE2{ENTER}", True
End If
If x_C8ligne = "LIGNE3" Then
    SendKeys "C:\codex8\masque\C8_ANTOM_LIGNE3{ENTER}", True
End If

'------ Positionnement sur Message de d�part (Masq -1)

SendKeys "{F6}", True                   '--- en mosa�que ...

End Sub

Public Function getField(Ligne, Champ) As String
Dim i As Integer

Select Case Champ
    Case "NumEnvoi"
        i = 1
    Case "StatusEnvoi"
        i = 2
    Case "Masq"
        i = 3
    Case "ORDRE_FAB"
        i = 4
    Case "Chateau_Mil"
        i = 5
    Case "Appela_Mil"
        i = 6
    Case "Centil"
        i = 7
    Case "Regie"
        i = 8
    Case "Gencod"
        i = 9
    Case "NCE"
        i = 10
    Case "Marquage1"
        i = 11
    Case "Marquage2"
        i = 12
    Case "NumLigneProCde"
        i = 13
    Case "QteCde"
        i = 14
    Case "NomPourTraite"
        i = 13
End Select

getField = Entry(i, Ligne, ";")

End Function


Sub XFER()
Dim zoappli As String
Dim ReturnValue
Dim i As Long
Dim j As Long
Dim k As Long
Dim zone As String
Dim z_masqpointeur As Integer
Dim x_C8ligne As String
Dim hdlFic As Integer
Dim Ligne As String
Dim t As Integer
On Error Resume Next

'---------------------------------
'TODO POINTER SUR UN FICHIER
'Set MaBd = MonEspaceDeTravail.OpenDatabase(CheminNomBDDAccess)
'Set TABU = MaBd.OpenRecordset(tab_bufi)                  '... Table Buffers xxxxx
t = Tempo
hdlFic = FreeFile
Open df_DealCodex.Text1.Text For Input As #hdlFic

If InStr(1, df_DealCodex.Text1.Text, "ligne1", vbTextCompare) <> 0 Then
    x_C8ligne = "5000"
End If
If InStr(1, df_DealCodex.Text1.Text, "ligne2", vbTextCompare) <> 0 Then
    x_C8ligne = "10000"
End If
If InStr(1, df_DealCodex.Text1.Text, "ligne3", vbTextCompare) <> 0 Then
    x_C8ligne = "LIGNE3"
End If

'TABU.MoveFirst
'z_masqpointeur = TABU![Masq]
z_masqpointeur = 1                  '//// modifi� le 18/07/02

ChDir "C:\CODEX8\MASQUE"                       '--- Activation Programme CODEX
zoappli = "C:\CODEX8\PROGRAM\CODEX.EXE"
ReturnValue = Shell(zoappli, 1)
Sleep (15000)
DoEvents
AppActivate "codex 2000"
AppActivate ReturnValue
'SendKeys "%{TAB}", True
Sleep (1000)
For i = 1 To 250
    DoEvents
Next i

SendKeys "%{F}", True
Sleep (1000)
For i = 1 To 250
    DoEvents
Next i
'--- Ouverture Fichier Image
SendKeys "{O}", True
For i = 1 To 250
    DoEvents
Next i
Sleep (1000)
If x_C8ligne = "5000" Then
    SendKeys "     C8_ANTOM_LIGNE1{ENTER}", True
End If
If x_C8ligne = "10000" Then
    SendKeys "     C8_ANTOM_LIGNE2{ENTER}", True
End If
If x_C8ligne = "LIGNE3" Then
    SendKeys "     C8_ANTOM_LIGNE3{ENTER}", True
End If
Sleep (1000)
'------ Positionnement sur Message de d�part (Masq -1)
For i = 1 To 250
    DoEvents
Next i

SendKeys "{F6}", True                   '--- en mosa�que ...
Sleep (1000)
For i = 1 To 100
    DoEvents
Next i

For i = 1 To (z_masqpointeur - 1)
    SendKeys "{RIGHT}", True
Next i

Sleep (t)

Do While Not EOF(hdlFic)
    '-> Lecture de la ligne
    Line Input #hdlFic, Ligne
    Sleep (t)

    For i = 1 To 200
        DoEvents
    Next i

    SendKeys "{ENTER}", True
    Sleep (t)
    DoEvents
    SendKeys "%{E}", True                   '--- Menu EDITION
    Sleep (t)
    SendKeys "{8}", True                    '--- Choix TEXT
    Sleep (t)
    For i = 1 To 200
        DoEvents
    Next i

    '======================== TEXT 1 ====
    SendKeys "{TAB}", True                  '--- Position sur champ suivant ...
    Sleep (t)
    DoEvents
    SendKeys "{ENTER}", True                '--- Ouverture du menu TEXT
    DoEvents
    SendKeys "{BACKSPACE 50} ", True        '--- Remise � z�ro du champ TEXT
    Sleep (t)
    DoEvents
    zone = " "
    zone = getField(Ligne, "Chateau_Mil")
    SendKeys zone, True
    
    For i = 1 To 300
        DoEvents
    Next i
    Sleep (t)
    SendKeys "{ENTER}", True
    Sleep t
    DoEvents
    '======================== TEXT 2 ====
    SendKeys "{TAB}", True                  '--- Position sur champ suivant ...
    Sleep (t)
    DoEvents
    SendKeys "{ENTER}", True                '--- Ouverture du menu TEXT
    Sleep (t)
    DoEvents
    SendKeys "{BACKSPACE 50} ", True        '--- Remise � z�ro du champ TEXT
    zone = ""
    Sleep (t)
    DoEvents
    zone = getField(Ligne, "Appela_Mil") & "{ENTER}"
    SendKeys zone, True
    Sleep (t)
    For i = 1 To 300
        DoEvents
    Next i
    '======================== TEXT 3 ====
    SendKeys "{TAB}", True                  '--- Position sur champ suivant ...
    Sleep (t)
    DoEvents
    SendKeys "{ENTER}", True                '--- Ouverture du menu TEXT
    Sleep (t)
    DoEvents
    SendKeys "{BACKSPACE 50} ", True        '--- Remise � z�ro du champ TEXT
    zone = ""
    zone = getField(Ligne, "Centil") & "^{ENTER}" & getField(Ligne, "Regie") & "{ENTER}"  'Retour chariot
    SendKeys zone, True
    Sleep (t)
    For i = 1 To 300
        DoEvents
    Next i

    '======================== TEXT 4 ====
    SendKeys "{TAB}", True                  '--- Position sur champ suivant ...
    Sleep (t)
    DoEvents
    SendKeys "{ENTER}", True                '--- Ouverture du menu TEXT
    Sleep (t)
    DoEvents
    SendKeys "{BACKSPACE 50} ", True        '--- Remise � z�ro du champ TEXT
    zone = ""

    'ici ...
    zone = getField(Ligne, "Marquage1") & "^{ENTER}" & getField(Ligne, "Marquage2") & "{ENTER}" 'Retour chariot
    SendKeys zone, True
    Sleep (t)
    For i = 1 To 300
        DoEvents
    Next i
    
    
    If Len(getField(Ligne, "Marqemb_div")) > 30 Then
        '    Stop
        zone = Mid(getField(Ligne, "Marqemb_div"), 1, 35)
        SendKeys zone, True
        Sleep (t)
        DoEvents
        SendKeys "%{F}", True
        Sleep (t)
        DoEvents
        SendKeys "%{T}", True
        For i = 1 To 300
            DoEvents
        Next i
        zone = "30"
        SendKeys zone, True
        Sleep (t)
        DoEvents
        SendKeys "{ENTER}", True
        For i = 1 To 300
            DoEvents
        Next i
        Else
        zone = getField(Ligne, "Marqemb_div")
        SendKeys zone, True
        Sleep (t)
        DoEvents
    End If
    
    SendKeys "{ENTER}", True
    Sleep (t)
    DoEvents
    
    For i = 1 To 300
        DoEvents
    Next i

    ' ...fin ici
    
    'Debug.Print "TABU![Gencod]=" & TABU![Gencod] & "   LEN=" & Len(TABU![Gencod]) & "  Masq=" & TABU![Masq]
    If Len(getField(Ligne, "Gencod")) = 13 Then
        '-------------------------- Traitement CODE A BARRES --------------
        SendKeys "%{E}", True                   '--- Menu EDITION
        Sleep t
        For i = 1 To 300     'ajoute
            DoEvents
        Next i
        
        SendKeys "{9}", True                    '--- Choix CODE A BARRES
        Sleep t
        For i = 1 To 300     'ajoute
            DoEvents
        Next i
        '======================== CODE BARRES 1 ====
        SendKeys "{TAB}", True                  '--- Position sur champ suivant ...
        Sleep t
        DoEvents
        SendKeys "{ENTER}", True                '--- Ouverture du menu TEXT
        Sleep t
        For i = 1 To 300     'ajoute
            DoEvents
        Next i
        SendKeys "%{T}", True
        Sleep t
        DoEvents
        SendKeys "{BACKSPACE 15} ", True        '--- Remise � z�ro du champ TEXT
        Sleep (t)
        DoEvents
        zone = ""
        zone = Mid(getField(Ligne, "Gencod"), 1, 12) & "{ENTER}"
        SendKeys zone, True
        Sleep (t)
        DoEvents
        '----------------------------
    Else
        '-------------------------- Traitement CODE A BARRES --------------
        SendKeys "%{E}", True                   '--- Menu EDITION
        For i = 1 To 300     'ajoute
            DoEvents
        Next i
        Sleep t
        SendKeys "{9}", True                    '--- Choix CODE A BARRES
        
        For i = 1 To 300     'ajoute
            DoEvents
        Next i
        '======================== CODE BARRES 1 ====
        SendKeys "{TAB}", True                  '--- Position sur champ suivant ...
        Sleep t
        DoEvents
        SendKeys "{DELETE}", True               '--- Suppression code � barres
        Sleep t
        DoEvents
    End If
    Sleep (t)
    For i = 1 To 400
        DoEvents
    Next i
    
    SendKeys "{F6}", True                   '--- en mosa�que ...
    Sleep (t)

    For i = 1 To 400
        DoEvents
    Next i
    Sleep (t)
    DoEvents
    SendKeys "{RIGHT}", True                '--- suivant ...
    'TABU.MoveNext
Loop
    
'------------------- Fermeture CODEX
For i = 1 To 400
    DoEvents
Next i

If GetIniFileValue("PARAM", "QUIT", searchExe("dealcodex.ini")) = "O" Then
    SendKeys "%{F}{ENTER}", True
    Sleep (t)
    DoEvents
    SendKeys "{Q}", True
    
    For i = 1 To 400
        DoEvents
    Next i
    Sleep (t)
    DoEvents
    
    SendKeys "{ENTER}", True
End If

'-> on ferme le fichier
Close #hdlFic

End Sub

