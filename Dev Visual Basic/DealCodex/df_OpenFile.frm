VERSION 5.00
Object = "{DAFD85F1-343B-47EE-8170-1C26723F7A5D}#2.0#0"; "dogskin.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form df_OpenFile 
   BorderStyle     =   0  'None
   Caption         =   "Deal Informatique"
   ClientHeight    =   6915
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   10875
   ControlBox      =   0   'False
   Icon            =   "df_OpenFile.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   461
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   725
   StartUpPosition =   1  'CenterOwner
   Begin VB.DriveListBox Drive1 
      Height          =   315
      Left            =   3600
      TabIndex        =   1
      Top             =   120
      Visible         =   0   'False
      Width           =   2775
   End
   Begin MSComctlLib.ListView ListView2 
      Height          =   3615
      Left            =   495
      TabIndex        =   2
      Top             =   1800
      Width           =   10080
      _ExtentX        =   17780
      _ExtentY        =   6376
      SortKey         =   1
      Arrange         =   2
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList2"
      ForeColor       =   10053171
      BackColor       =   16777215
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nom"
         Object.Width           =   7408
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Date"
         Object.Width           =   5556
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Taille"
         Object.Width           =   2910
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   -600
      Top             =   480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   48
      ImageHeight     =   48
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_OpenFile.frx":0ECA
            Key             =   "POSTE"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_OpenFile.frx":2BA4
            Key             =   "CDROM"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_OpenFile.frx":487E
            Key             =   "DDUR"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_OpenFile.frx":6558
            Key             =   "DNET"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_OpenFile.frx":8232
            Key             =   "DISQUETTE"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_OpenFile.frx":9F0C
            Key             =   "FOLDER"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_OpenFile.frx":BBE6
            Key             =   "OPENFOLDER"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_OpenFile.frx":D8C0
            Key             =   "FILE"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_OpenFile.frx":F59A
            Key             =   "WINFILE"
         EndProperty
      EndProperty
   End
   Begin DogSkin.DealCmdButton cmdOk 
      Height          =   480
      Left            =   6720
      TabIndex        =   3
      Top             =   5880
      Width           =   2025
      _ExtentX        =   3572
      _ExtentY        =   847
      Caption         =   ""
      ForeColor       =   0
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   0
      ButtonForm      =   0
      BackColor       =   16579059
   End
   Begin DogSkin.DealCmdButton cmdCancel 
      Height          =   480
      Left            =   8760
      TabIndex        =   4
      Top             =   5880
      Width           =   2025
      _ExtentX        =   3572
      _ExtentY        =   847
      Caption         =   ""
      ForeColor       =   0
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   0
      ButtonForm      =   0
      BackColor       =   16579059
   End
   Begin DogSkin.DogSkinObject DogSkinObject1 
      Left            =   0
      Top             =   0
      _ExtentX        =   979
      _ExtentY        =   953
      BorderColor     =   13077835
      BeginProperty HeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeaderForeColor =   10053171
      HeaderBackColor =   16637889
      HeaderLineColor =   15847345
      HeaderUseLine   =   -1  'True
      BeginProperty BodyFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BodyForeColor   =   10053171
      BodyBackColor   =   16579059
      HeaderIco       =   "df_OpenFile.frx":11274
      HeaderIcoNa     =   "df_OpenFile.frx":118F6
      ShadowColor     =   16637889
   End
   Begin VB.Image imgAffichage 
      Height          =   525
      Left            =   8040
      MouseIcon       =   "df_OpenFile.frx":11F78
      MousePointer    =   99  'Custom
      Picture         =   "df_OpenFile.frx":12C42
      Top             =   870
      Width           =   525
   End
   Begin VB.Image imgSearch 
      Height          =   525
      Left            =   6840
      MouseIcon       =   "df_OpenFile.frx":13C68
      MousePointer    =   99  'Custom
      Picture         =   "df_OpenFile.frx":14932
      Top             =   870
      Width           =   525
   End
   Begin VB.Image imgProp 
      Height          =   525
      Left            =   4560
      MouseIcon       =   "df_OpenFile.frx":15958
      MousePointer    =   99  'Custom
      Picture         =   "df_OpenFile.frx":16622
      Top             =   870
      Width           =   525
   End
   Begin VB.Image imgCurrentPath 
      Height          =   525
      Left            =   5040
      MouseIcon       =   "df_OpenFile.frx":17648
      MousePointer    =   99  'Custom
      Picture         =   "df_OpenFile.frx":18312
      Top             =   870
      Width           =   525
   End
   Begin VB.Image imgPosteTravail 
      Height          =   525
      Left            =   7440
      MouseIcon       =   "df_OpenFile.frx":19338
      MousePointer    =   99  'Custom
      Picture         =   "df_OpenFile.frx":1A002
      Top             =   870
      Width           =   525
   End
   Begin VB.Image imgNewFolder 
      Height          =   525
      Left            =   6240
      MouseIcon       =   "df_OpenFile.frx":1B028
      MousePointer    =   99  'Custom
      Picture         =   "df_OpenFile.frx":1BCF2
      Top             =   870
      Width           =   525
   End
   Begin VB.Image imgPrefolder 
      Height          =   525
      Left            =   5640
      MouseIcon       =   "df_OpenFile.frx":1CD18
      MousePointer    =   99  'Custom
      Picture         =   "df_OpenFile.frx":1D9E2
      Top             =   870
      Width           =   525
   End
   Begin VB.Image imgCours 
      Height          =   735
      Left            =   480
      Top             =   960
      Width           =   855
   End
   Begin VB.Label lblCours 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00996633&
      Height          =   615
      Left            =   1440
      TabIndex        =   0
      Top             =   1080
      Width           =   3855
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00996633&
      Height          =   5175
      Left            =   120
      Top             =   600
      Width           =   10575
   End
End
Attribute VB_Name = "df_OpenFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> R�cup�rer des informations sur un disque
Private Declare Function GetLogicalDriveStrings& Lib "kernel32" Alias "GetLogicalDriveStringsA" (ByVal nBufferLength As Long, ByVal lpbuffer As String)
Private Declare Function GetDriveType& Lib "kernel32" Alias "GetDriveTypeA" (ByVal nDrive As String)
Private Declare Function GetVolumeInformation& Lib "kernel32" Alias "GetVolumeInformationA" (ByVal lpRootPathName As String, ByVal lpVolumeNameBuffer As String, ByVal nVolumeNameSize As Long, lpVolumeSerialNumber As Long, lpMaximumComponentLength As Long, lpFileSystemFlags As Long, ByVal lpFileSystemNameBuffer As String, ByVal nFileSystemNameSize As Long)
Private Declare Function GetDiskFreeSpace& Lib "kernel32" Alias "GetDiskFreeSpaceA" (ByVal lpRootPathName As String, lpSectorsPerCluster As Long, lpBytesPerSector As Long, lpNumberOfFreeClusters As Long, lpTotalNumberOfClusters As Long)

'-> Constante de type de lecteur
Private Const DRIVE_CDROM& = 5
Private Const DRIVE_FIXED& = 3
Private Const DRIVE_REMOTE& = 4
Private Const DRIVE_REMOVABLE& = 2

'-> Indique le mode de mise � jour
Dim ModeMaj As Integer '0-> Open, 1-> Save

'-> Indique le mode de fermeture
Dim ModeClose As Integer '0-> Fermeture normale (defaut), 1-> Ouvre le display

'-> Selon l'extension
Dim Extension As String
Dim DisplaySelectedExtension As Boolean
Dim NiveauDisque As Boolean

'-> Indiquer que l'on affiche que les r�pertoires
Dim DisplayOnlyDirectory As Boolean

'-> API pour affichage d'une page de propri�t�s
Private Type SHELLEXECUTEINFO
             cbSize As Long
             fMask As Long
             hwnd As Long
             lpVerb As String
             lpFile As String
             lpParameters As String
             lpDirectory As String
             nShow As Long
             hInstApp As Long
             ' Optional fields
             lpIDList As Long
             lpClass As String
             hkeyClass As Long
             dwHotKey As Long
             hIcon As Long
             hProcess As Long
End Type

Private Const SEE_MASK_INVOKEIDLIST = &HC
Private Declare Function ShellExecuteEx Lib "shell32" (lpSEI As SHELLEXECUTEINFO) As Long

Public Sub init(InitMode As Integer, InitPath As String, Titre As String, DisplayExtentsion As String, Optional InitClose As Integer)

'---> Point d'entr�e de la feuille

Dim aIt As ListItem
Dim RemoteDriveName As String
Dim RemotePath As String
Dim i As Integer


Me.DogSkinObject1.Caption = Titre

'-> Libell�s MESSPROG
If InitMode = 0 Then
    Me.cmdOk.Caption = "Ouvrir"
    DisplayOnlyDirectory = False
ElseIf InitMode = 1 Then
    Me.cmdOk.Caption = "Enregistrer"
    DisplayOnlyDirectory = False
Else
    Me.cmdOk.Caption = "R�pertoire"
    DisplayOnlyDirectory = True
End If
Me.cmdCancel.Caption = "Fermer"

Me.imgCurrentPath.ToolTipText = "Emplacement en cours" 'MESSPROG
Me.imgNewFolder.ToolTipText = "Cr�ation d'un nouveau r�pertoire"
Me.imgPosteTravail.ToolTipText = "Poste de travail"
Me.imgPrefolder.ToolTipText = "Pr�c�dent"
Me.imgProp.ToolTipText = "Propri�t�s"
Me.imgSearch.ToolTipText = "Recherche"
Me.imgAffichage.ToolTipText = "Affichage"

'-> Positionner la variable
ModeMaj = InitMode

'-> Gestion des extensions
If Not DisplayOnlyDirectory Then
    '-> Doit on afficher une certaine extension
    If DisplayExtentsion = "" Then
        '-> Ne pas afficher d'extention particuli�re
        DisplaySelectedExtension = True
        Extension = "*.*"
    Else
        '-> Afficher une extentsion particuli�re
        DisplaySelectedExtension = True
        Extension = DisplayExtentsion
    End If

    '-> Poser les matrices des effets
    'Me.DogSkinObject1.SetMatriceEffect "Shape1|Shape2"
    '-> Afficher les zones de saisie
Else
    '-> Masquer les zones
    '-> Poser les matrices des effets
    Me.DogSkinObject1.SetMatriceEffect "Shape1"
End If

'-> Se positionner sur le r�pertoire pass� en argument
If InitPath = "" Then
    '-> Charger la liste des disques
    LoadDrive
    '-> Indiquer que l'on est au niveau disque
    NiveauDisque = True
Else
    '-> Gestion des disques r�seaux
    If Left(InitPath, 2) = "\\" Then
        '-> rechercher dans tous les lecteurs si on trouve la lettre de mapping
        For i = 0 To Me.Drive1.ListCount - 1
            '-> Tester s'il y a des "\\"
            If InStr(1, Me.Drive1.List(i), "\\") <> 0 Then
                '-> Get du nom r�seau
                RemoteDriveName = Entry(2, Me.Drive1.List(i), "[")
                RemoteDriveName = Mid$(RemoteDriveName, 1, Len(RemoteDriveName) - 1)
                '-> Chercher si on trouve la comparaison
                If InStr(1, UCase$(InitPath), UCase$(RemoteDriveName)) <> 0 Then
                    '-> On a trouve donc remplacer par la lettre
                    RemotePath = Mid$(InitPath, Len(RemoteDriveName) + 1, Len(InitPath) - Len(RemoteDriveName))
                    InitPath = UCase$(Trim(Entry(1, Me.Drive1.List(i), "["))) & RemotePath
                End If
            End If
        Next
    End If
    DisplayFileObject InitPath, Extension
End If

'-> Sauvegarder le mode de fermeture
ModeClose = InitClose

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
If KeyAscii = 27 Then Call cmdCancel_Click
End Sub

Private Sub Form_Load()

'-> on regardre si on doit mettre un skin
SkinColorModify Me

'-> Init du skin
Me.DogSkinObject1.Initialisation True, "", pMinMaxClose, True

End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next

'-> Bloquer la mise � jour de la feuille
LockWindowUpdate Me.hwnd

'-> R�cup�rer la taille de la zone cliente
GetClientRect Me.hwnd, aRect

'-> Positionner le fond blanc
Me.Shape1.Width = aRect.Right - 31
Me.Shape1.Height = aRect.Bottom - 97

'-> Liste des fichiers
Me.ListView2.Width = aRect.Right - 87
Me.ListView2.Height = aRect.Bottom - 201

'-> Placer le bouton annuler
Me.cmdCancel.Top = aRect.Bottom - 47
Me.cmdCancel.Left = Me.ScaleWidth - 146

'-> Placer le bouton Ok
Me.cmdOk.Left = Me.cmdCancel.Left - 132
Me.cmdOk.Top = Me.cmdCancel.Top

'-> Pr�c�dent
Me.imgProp.Left = aRect.Right - 296
'-> Poste de travail
Me.imgCurrentPath.Left = Me.imgProp.Left + 32
'-> Path
Me.imgPrefolder.Left = Me.imgCurrentPath.Left + 32
'-> Information
Me.imgNewFolder.Left = Me.imgPrefolder.Left + 32
'-> Create
Me.imgSearch.Left = Me.imgNewFolder.Left + 32
'-> Search
Me.imgPosteTravail.Left = Me.imgSearch.Left + 32
'-> Affichage
Me.imgAffichage.Left = Me.imgPosteTravail.Left + 32

'-> Lib�rer la mise � jour de la feuille
LockWindowUpdate 0

'-> Lib�rer la CPU
DoEvents

End Sub


Private Sub imgAffichage_Click()
ShowInList
End Sub

Private Sub imgCurrentPath_Click()

'-> Ne rien faire si pas d'icone en cours
If Me.ListView2.SelectedItem Is Nothing Then Exit Sub

'-> Afficher le path en cours
DisplayMessage "Emplacement en cours : " & Chr(13) & Me.lblCours.Tag, 3, 1, "Emplacement en cours"

End Sub

Private Sub imgNewFolder_Click()

'-> Demander le nom du nouveau dossier
Call CreateNewFolder

End Sub

Private Sub imgPosteTravail_Click()
'-> Afficher les disques
LoadDrive
End Sub

Private Sub imgPrefolder_Click()

Dim NewFilename As String
Dim i As Integer

'-> Ne rien faire si on est au niveau du disque
If NiveauDisque Then Exit Sub

'-> Supprimer le dernier slash et remonter d'un niveau
NewFilename = Me.lblCours.Tag
If NumEntries(NewFilename, "\") = 2 Then
    '-> Afficher les disques
    LoadDrive
Else
    '-> Cr�er le path � afficher
    NewFilename = Mid$(Me.lblCours.Tag, 1, Len(Me.lblCours.Tag) - 1)
    i = InStrRev(NewFilename, "\")
    NewFilename = Mid$(NewFilename, 1, i - 1)
    '-> Afficher le nouveau path
    DisplayFileObject NewFilename, Extension
End If


End Sub

Private Sub imgProp_Click()

If Me.ListView2.SelectedItem Is Nothing Then Exit Sub
ShowFileProperties Entry(2, Me.ListView2.SelectedItem.Key, "|"), Me.hwnd

End Sub

Private Sub imgSearch_Click()

Dim DateMini As String
Dim DateMaxi As String

'-> Chaine de retour : FICHIER|DATEMINI|DATEMAXI

'-> Ne rien faire si on est au niveau disque
If NiveauDisque Then Exit Sub

'-> rafraichir
Me.Refresh

'-> Analyse du retour
If strRetour = "" Then Exit Sub

'-> Lancer l'analyse du disque
DisplayFileObject Me.lblCours.Tag, Entry(1, strRetour, "|"), True, True, Entry(2, strRetour, "|"), Entry(3, strRetour, "|"), Entry(4, strRetour, "|")

'-> Vider la variable d'�change
strRetour = ""

End Sub

Private Sub cmdCancel_Click()
strRetour = ""
Unload Me
End Sub

Private Sub cmdOk_Click()

Dim Titre As String
Dim Rep As String

On Error GoTo BadFile

'-> Selon le mode d'ouverture
If ModeMaj = 0 Then
    '-> Retourner le r�pertoire s�lectionn�
    strRetour = Entry(2, Me.ListView2.SelectedItem.Key, "|")
    '-> on ferme la feuille
    Unload Me
End If

Exit Sub

BadFile:
    DisplayMessage "Fichier incorrecte", 3, 1, Titre
    

End Sub


Private Sub LoadDrive()


'-> Charger dans le tree la le poste de travail et les disques connect�s


Dim aIt As ListItem
Dim aDisk As String
Dim Res As Long
Dim lpbuffer As String
Dim i As Integer

Dim DiskInfo As String

'-> Vider la liste des items existants
Me.ListView2.ListItems.Clear

'-> Charger la liste des disques connect�s
lpbuffer = Space$(2000)
Res = GetLogicalDriveStrings&(Len(lpbuffer), lpbuffer)
lpbuffer = Mid$(lpbuffer, 1, Res)

'-> On est au niveau disque
NiveauDisque = True

'-> Analyser la liste des diques connect�s
For i = 1 To NumEntries(lpbuffer, Chr(0))
    '-> D�finition du disque
    aDisk = Entry(i, lpbuffer, Chr(0))
    '-> R�cup�ration des informations
    DiskInfo = GetDriveInformations(aDisk)
    '-> Ne rien faire si pas de donn�es
    If Trim(DiskInfo) = "" Then GoTo NextDisk
    '-> Ajouter l'objet dans le listview
    Set aIt = Me.ListView2.ListItems.Add(, "DRIVE|" & Entry(3, DiskInfo, "|"), Entry(2, DiskInfo, "|"), Entry(1, DiskInfo, "|"))
NextDisk:
Next

Me.ListView2.Arrange = lvwAutoTop
Me.ListView2.Refresh

'-> Indiquer que l'on est sur le poste de travail
Me.imgCours.Picture = Me.ImageList2.ListImages("POSTE").Picture

'-> Libell�
Me.lblCours.Tag = "Poste de travail"
Me.lblCours.Caption = "Poste de travail"

End Sub

Private Function GetDriveInformations(aDisk As String) As String

'-> Cette proc�dure retourne le type de disque


Dim TypeDrive As Long
Dim DiskText As String
Dim Res As Long
Dim lpbuffer As String
Dim i As Integer
Dim VolumeName As String
Dim SerialNumber As Long
Dim ComponentLenght As Long
Dim SystemFlags As Long
Dim SystemNameBuffer As String

If Trim(aDisk) = "" Then Exit Function

'-> R�cup�ration du type de lecteur
TypeDrive = GetDriveType(aDisk)
'-> Recup�rer les informations sur le type de disque
VolumeName = Space$(255)
SystemNameBuffer = Space$(250)
Res = GetVolumeInformation(aDisk, VolumeName, Len(VolumeName), SerialNumber, ComponentLenght, SystemFlags, SystemNameBuffer, Len(SystemNameBuffer))

'-> Afficher le nom du disque s'il est renseign�
If Trim(Entry(1, VolumeName, Chr(0))) <> "" Then
    VolumeName = Trim(Entry(1, VolumeName, Chr(0))) & "(" & aDisk & ")|" & aDisk
Else
    Select Case TypeDrive
        Case DRIVE_CDROM 'CDrom
            VolumeName = "Lecteur CD Rom" & Chr(13) & "(" & aDisk & ")|" & aDisk
        Case DRIVE_FIXED 'disque dur
            VolumeName = "Disque local" & Chr(13) & "(" & aDisk & ")|" & aDisk
        Case DRIVE_REMOTE 'Disque r�seau
            VolumeName = "Disque r�seau" & Chr(13) & "(" & aDisk & ")|" & aDisk
        Case DRIVE_REMOVABLE 'Disquette
            VolumeName = "Lecteur de disquette" & Chr(13) & " (" & aDisk & ")|" & aDisk
    End Select

End If

'-> Afficher la bonne icone
Select Case TypeDrive
    Case DRIVE_CDROM 'CDrom
        strRetour = "CDROM|" & VolumeName
    Case DRIVE_FIXED 'disque dur
        strRetour = "DDUR|" & VolumeName
    Case DRIVE_REMOTE 'Disque r�seau
        strRetour = "DNET|" & VolumeName
    Case DRIVE_REMOVABLE 'Disquette
        strRetour = "DISQUETTE|" & VolumeName
End Select

'-> Retourner
GetDriveInformations = strRetour
strRetour = ""

End Function

Private Sub ListView2_DblClick()

'---> Affichage de la liste des r�pertoires associ�s

'-> Ne rien faire si pas d'objet s�lectionn�
If Me.ListView2.SelectedItem Is Nothing Then Exit Sub

'-> Si on est sur un fichier, retourner le fichier
If Me.ListView2.SelectedItem.Tag = "FILE" Then
    Call cmdOk_Click
    Exit Sub
End If

'-> Indiquer le tag en cours
Me.imgCours.Tag = Me.ListView2.SelectedItem.Tag

'-> Afficher le contenu
Call DisplayFileObject(Entry(2, Me.ListView2.SelectedItem.Key, "|"), Extension)

End Sub

Public Sub DisplayFileObject(aPath As String, FindExtentsion As String, Optional HideFolder As Boolean, Optional CheckDate As Boolean, Optional strDateMini As String, Optional strDateMaxi As String, Optional FindText As String)

Dim FileName As String
Dim i As Integer
Dim ItemX As ListItem
Dim MyExtension As String
Dim ToAdd As Boolean
Dim LenFichierFile As Integer
Dim DateMiniOk As Boolean
Dim DateMaxiOk As Boolean
Dim FileDate As String

On Error GoTo GestError

'-> Bloquer l'�cran
Me.Enabled = False
Me.MousePointer = 11

'-> Positionner le path que l'on a modifi�
SavePath = aPath
'-> supprimer l'anti slash
If Right$(aPath, 1) = "\" Then SavePath = Mid$(SavePath, 1, Len(SavePath) - 1)

'-> Rajouter un "\" � la fin si necessaire
If Right(aPath, 1) <> "\" Then aPath = aPath & "\"

'-> Analyse de tous les fichiers
FileName = Dir$(aPath & "*.*", vbDirectory Or vbNormal)

'-> Positionner le path en cours
Me.lblCours.Caption = aPath 'Entry(NumEntries(aPath, "\") - 1, aPath, "\")

'-> Tester si on veut afficher un disque ou non
If NumEntries(aPath, "\") = 2 Then
    '-> Rajouter le \ du disque
    'Me.lblCours.Caption = Me.lblCours.Caption & "\"
    '-> Positionner l'icone � afficher
    Me.imgCours.Picture = Me.ImageList2.ListImages(Entry(1, GetDriveInformations(aPath), "|")).Picture
Else
    Me.imgCours.Picture = Me.ImageList2.ListImages("OPENFOLDER").Picture
End If

'-> Indiquer dans le tag le path en cours
Me.lblCours.Tag = aPath

'-> Vider le listview
Me.ListView2.ListItems.Clear

'-> Si on doit analyser les dates
If CheckDate Then
    '-> Analyse de la date mini
    If Trim(strDateMini) <> "" Then
        '-> Test si la date est OK
        If IsDate(strDateMini) Then
            '-> Formatter la date
            strDateMini = FormatCompareDate(strDateMini)
            '-> Indiquer que l'on travaille sur cette date
            DateMiniOk = True
        End If 'Si la date est valide
    End If
    '-> Analyse de la date maxi
    If Trim(strDateMaxi) <> "" Then
        '-> Test si la date est OK
        If IsDate(strDateMaxi) Then
            '-> Formatter la date
            strDateMaxi = FormatCompareDate(strDateMaxi)
            '-> Indiquer que l'on travaille sur cette date
            DateMaxiOk = True
        End If 'Si la date est valide
    End If
    '-> Si pas date valide, annuler la recherche en date
    If Not DateMiniOk And Not DateMaxiOk Then CheckDate = False
End If 'Si on doit analyser les dates

'-> Analyse des fichiers et r�pertoires
Do While FileName <> ""
    '-> De base on ajoute
    ToAdd = True
    '-> Ne pas afficher le command.com
    If FileName = "." Or FileName = ".." Or Trim(FileName) = "" Then GoTo NextDir
    '-> Tester si c'est un dossier ou un fichier
    If (GetAttr(aPath & FileName) And vbDirectory) = vbDirectory Then
        '-> Ne pas traiter les r�pertoires
        If HideFolder Then GoTo NextDir
        '-> Indiquer que c'est un r�pertoire
        Set ItemX = Me.ListView2.ListItems.Add(, "FOLDER|" & aPath & FileName, FileName, "FOLDER")
        ItemX.Tag = "FOLDER"
        ItemX.SubItems(1) = "a"
    Else
        '-> Analyse selon l'extension
        i = InStrRev(FileName, ".")
        If i <> 0 Then MyExtension = Mid$(FileName, i + 1, Len(FileName) - i)
        '-> Comparer l'extentsion
        If Trim(FindExtentsion) = "" Then
            ToAdd = False
        Else
            If Trim(FindExtentsion) = "*.*" Then
                ToAdd = True
            Else
                '-> Analyse par le debut
                If Trim(Entry(1, FindExtentsion, ".")) <> "*" Then
                    '-> Attention si * dans la premi�re partie
                    If InStr(1, FindExtentsion, "*") <> 0 Then
                        LenFichierFile = Len(Mid$(FindExtentsion, 1, InStr(1, FindExtentsion, "*") - 1))
                    Else
                        '-> Get du d�but
                        LenFichierFile = Len(Entry(1, FindExtentsion, "."))
                    End If
                    '-> Comparer
                    If UCase$(Trim(Mid$(FileName, 1, LenFichierFile))) = UCase$(Trim(Mid$(FindExtentsion, 1, LenFichierFile))) Then
                        ToAdd = True
                    Else
                        ToAdd = False
                    End If
                End If 'Si on doit comparer sur le d�but de la recherche
                
                '-> Comparer l'extentsion que si necessaire
                If Not ToAdd Then GoTo NextDir
                '-> Ne pas comparer l'extension si = "*"
                If Trim(Entry(2, FindExtentsion, ".")) <> "*" Then
                    If Trim(UCase$(Entry(2, FindExtentsion, "."))) = Trim(UCase$(MyExtension)) Then
                        ToAdd = True
                    Else
                        ToAdd = False
                    End If
                End If
            End If
        End If ' Si on doit tester le debut de recherche sur le nom du fichier
        
        '-> Si on doit ajouter le fichier
        If Not ToAdd Then GoTo NextDir
        
        '-> Analyse sur les dates
        If CheckDate Then
            '-> R�cup�rer la date du fichier
            FileDate = FormatCompareDate(FileDateTime(aPath & FileName))
            
            '-> Analyse selon les crit�res de date
            If DateMiniOk Then
                '-> Si la date maxi est rens�ign�e
                If DateMaxiOk Then
                    '-> Compris entre
                    If CDbl(FileDate) <= CDbl(strDateMaxi) And CDbl(FileDate) >= CDbl(strDateMini) Then
                        ToAdd = True
                    Else
                        ToAdd = False
                    End If
                Else
                    '-> Egalite
                    If CDbl(FileDate) = CDbl(strDateMini) Then
                        ToAdd = True
                    Else
                        ToAdd = False
                    End If
                End If 'Si la date maxi est rens�ign�e
            Else
                '-> La date maxi est forc�ment rens�ign�e : �galit�
                If CDbl(FileDate) = CDbl(strDateMaxi) Then ToAdd = True
            End If 'Si la date mini est rens�ign�e
        End If 'Si on doit analyser les dates
        
        '-> Si on doit ajouter le fichier
        If Not ToAdd Then GoTo NextDir
        
        '-> Analyse sur le text � rechercher DIDIER
        If Trim(FindText) <> "" Then
            '-> on lance la recherche du texte
            If FindSearchText(aPath & FileName, FindText) Then
                ToAdd = True
            Else '-> si on a pas trouv� le texte
                ToAdd = False
            End If
        End If
        
        '-> Si on doit ajouter le fichier
        If Not ToAdd Then GoTo NextDir
                
        '-> Afficher  le fichier test sur extension
        Set ItemX = Me.ListView2.ListItems.Add(, "FILE|" & aPath & FileName, FileName, "WINFILE")
        ItemX.Tag = "FILE"
        ItemX.SubItems(1) = "b"
    End If
    
NextDir:
    '-> Analyser lz prochaine entr�e
    FileName = Dir
Loop

'-> Indiquer que l'on n'est pas au niveau disque
NiveauDisque = False

'-> S�lectionner la premi�re icone
If Me.ListView2.ListItems.Count <> 0 Then Set Me.ListView2.SelectedItem = Me.ListView2.ListItems(1)

'-> Arranger la vue
Me.ListView2.Arrange = lvwAutoTop

'-> Quitter la proc�dure
GoTo EndProg

GestError:
    Select Case Err.Number
        Case 52 'Nom au lecteur incorrecte MESSPROG
            DisplayMessage "Impossible d'acc�der � ce disque ou r�pertoire.", 1, 1, "Erreur"
        Case Else
            DisplayMessage "Erreur dans la recherche des fichiers : " & Chr(13) & Err.Number & " - " & Err.Description, 1, 1, "Erreur"
    End Select
            
EndProg:

    Me.Enabled = True
    Me.MousePointer = 0

End Sub

Private Function FindSearchText(FileName As String, TextSearch As String) As Boolean
Dim hdlFile As Integer
Dim x As ListItem
Dim ValueEnreg As Integer
Dim LenEnreg As Integer
Dim StrValueEnreg As String

On Error Resume Next

'-> Ouvrir le fichier
hdlFile = FreeFile
Open FileName For Binary As #hdlFile

'-> Lecture du fichier
Do While Not EOF(hdlFile)
    '-> Lecture du code enregistrement
    Get #hdlFile, , ValueEnreg
    '-> Lecture de la longueur de l'enregistrement
    Get #hdlFile, , LenEnreg
    '-> Lecture de la valeur de l'enregistrement
    StrValueEnreg = GetAsciiByte(hdlFile, LenEnreg)
    '-> on regarde si on a trouv� la chaine
    If InStr(1, Trim(UCase(StrValueEnreg)), Trim(UCase(TextSearch))) <> 0 Then
        '-> valeur de succes
        FindSearchText = True
        GoTo GestError
    End If
Loop 'Pour tout le fichier

GestError:
'-> Fermer le fichier
Close #hdlFile
     
End Function

Private Sub ListView2_ItemClick(ByVal Item As MSComctlLib.ListItem)

'-> Ne rien faire si pas d'objet s�lectionn�
If Me.ListView2.SelectedItem Is Nothing Then Exit Sub

End Sub

Private Sub ListView2_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then ListView2_DblClick

End Sub

Private Sub CreateNewFolder()

'---> Tentative de suppression d'un r�pertoire ou d'un fichier

Dim Rep As String
Dim ItemX As ListItem

'-> Ne rien faire si on est au niveau disque
If NiveauDisque Then Exit Sub

'-> Demander le nom du r�pertoire
Rep = DisplayMessage("Veuillez saisir le nom du nouveau r�pertoire : ", 1, 1, "Cr�ation d'un nouveau r�pertoire", True)
If Rep = "ANNULER" Then Exit Sub

'-> V�rifier qu'il y ait une valeur
If Trim(InputValue) = "" Then
    DisplayMessage "Nom de r�pertoire incorrecte.", 3, 1, "Cr�ation d'un r�pertoire"
    Exit Sub
End If

'-> Enlever un "\" s'il ya en a un
InputValue = Trim(InputValue)
If Right(InputValue, 1) = "\" Then InputValue = Mid$(InputValue, 1, Len(InputValue) - 1)

'-> Essayer de cr�er le r�pertoire
If Not CreateDirectory(InputValue) Then
    DisplayMessage "Erreur lors de la cr�ation du r�pertoire : " & Chr(13) & Me.lblCours.Tag & Trim(InputValue), 1, 1, "Erreur de cr�ation"
    Exit Sub
End If

'-> Afficher le nouveau r�pertoire cr�er
Set ItemX = Me.ListView2.ListItems.Add(, "FILE|" & Me.lblCours.Tag & Trim(InputValue), Trim(InputValue), "FOLDER")
ItemX.Tag = "FOLDER"
ItemX.SubItems(1) = "b"
ItemX.Selected = True
ItemX.EnsureVisible

End Sub

Private Function CreateDirectory(NewDirectory As String) As Boolean

On Error GoTo GestError

'-> Essayer de cr�er le r�pertoire
MkDir Me.lblCours.Tag & NewDirectory

'-> renvoyer une valeur de succ�s
CreateDirectory = True

Exit Function

GestError:


End Function

Public Sub ShowFileProperties(ByVal ps_FileName As String, Owner As Long)
   
Dim lu_ShellExUDT As SHELLEXECUTEINFO

On Error Resume Next

With lu_ShellExUDT
    .hwnd = Owner
    .lpVerb = "properties"
    .lpFile = ps_FileName
    .fMask = SEE_MASK_INVOKEIDLIST
    .cbSize = Len(lu_ShellExUDT)
End With

Call ShellExecuteEx(lu_ShellExUDT)
   
End Sub

Public Sub ShowInList()
'--> cette fonction permet d'afficher les ficiers en liste
Dim aItem As ListItem
On Error Resume Next
If Me.ListView2.View = lvwIcon Then
    Me.ListView2.View = lvwReport
    For Each aItem In Me.ListView2.ListItems
        If IsDir(Me.lblCours.Tag & aItem.Text) Then
            'aItem.SubItems (1)
        Else
            aItem.SubItems(2) = FileDateTime(Me.lblCours.Tag & aItem.Text)
            aItem.SubItems(3) = FormatFileSize(FileLen(Me.lblCours.Tag & aItem.Text), 0)
        End If
    Next
Else
    Me.ListView2.View = lvwIcon
End If


End Sub

Private Function FormatFileSize(ByVal Size As Long, FormatType As Integer) As String
'--> cette fonction permet de transformer la taille d'un fichier
Dim sRet As String
Const KB& = 1024
Const MB& = KB * KB

'FormatType = 0 Short String Format
'FormatType = 1 Long String Format
'FormatType = 2 Dual String Format

If Size < KB Then
   sRet = Format(Size, "#,##0") & " Bytes"
Else
   Select Case Size \ KB
      Case Is < 10
         sRet = Format(Size / KB, "0.00") & " KB"
      Case Is < 100
         sRet = Format(Size / KB, "0.0") & " KB"
      Case Is < 1000
         sRet = Format(Size / KB, "0") & " KB"
      Case Is < 10000
         sRet = Format(Size / MB, "0.00") & " MB"
      Case Is < 100000
         sRet = Format(Size / MB, "0.0") & " MB"
      Case Is < 1000000
         sRet = Format(Size / MB, "0") & " MB"
      Case Is < 10000000
         sRet = Format(Size / MB / KB, "0.00") & " GB"
   End Select
   
   Select Case FormatType
     Case 0 'Short
       sRet = sRet
     Case 1 'Long
       sRet = Format(Size, "#,##0") & " Bytes"
     Case 2 'Dual
       sRet = sRet & " (" & Format(Size, "#,##0") & " Bytes)"
   End Select
End If

FormatFileSize = sRet
End Function

Public Sub ColumnOrder(aList As ListView, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
'--> cette procedure permet de trier les colonnes d'un listview
Dim aItem As ListItem
Dim aCol As ColumnHeader
Dim i As Integer
Dim j As Integer

'-> gestion des erreurs
On Error Resume Next

'-> on ajoute une colonne cachee pour le tri
aList.ColumnHeaders.Add , "Cache"
'-> on la masque
aList.ColumnHeaders("Cache").Width = 0

'on vide la colonne cach�e
For Each aItem In aList.ListItems
    aItem.SubItems(aList.ColumnHeaders.Count - 1) = ""
Next
'-> si on a des ruptures et pas sur la colonne en cours on envoi les donnees dans la colonne cachee
'-> on teste l'allignement
Select Case ColumnHeader.Alignment
 Case lvwColumnRight
    '-> on met des blancs devant on est sur des chiffres
     For Each aItem In aList.ListItems
        If Not (Entry(1, aItem.Key, "|") = "Rupture") Then  'And aItem.SubItems(ColumnHeader.Index - 1) = "") Then
            aItem.SubItems(aList.ColumnHeaders.Count - 1) = Right(Space(20) & FileLen(aItem.Key), 20) 'aItem.SubItems(aList.ColumnHeaders.Count - 1) + Right(Space(20) & Str((CDbl(Replace(Replace(Replace(aItem.SubItems(ColumnHeader.Index - 1), " Bytes", ""), " MB", "000000"), " KB", "000"))) * 1000), 20)
        Else '-> on est sur une ligne de rupture
            aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + Mid("ZZZZZZZZZZZZZZZZZZ", 1, 15 - CInt(Entry(2, aItem.Key, "|")))
        End If
     Next
 Case lvwColumnLeft
        '-> on est sur une chaine on regarde si on est sur une colonne de dates
        For Each aItem In aList.ListItems
            '-> on verifie si on a que des dates!!
            If ColumnHeader.Index <> 1 Then
                If Not IsDate(aItem.SubItems(ColumnHeader.Index - 1)) Then
                     '-> on copie la chaine tel quel
                         aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + aItem.SubItems(ColumnHeader.Index - 1)
                Else
                     '-> on est sur des dates on les formate yyyymmdd
                     aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + Format(aItem.SubItems(ColumnHeader.Index - 1), "yyyymmddhhmmss")
                End If
            Else '-> on est sur la premiere colonne
                If Not IsDate(aItem.Text) Then
                     '-> on copie la chaine tel quel
                     aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + aItem.Text
                Else
                     '-> on est sur des dates on les formate yyyymmdd
                     aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + Format(aItem.Text, "yyyymmdd")
                End If
            End If '-> on est sur la premiere colonne ou pas
            If Entry(1, aItem.Key, "|") = "Rupture" Then 'And aItem.SubItems(ColumnHeader.Index - 1) = "") Then
                '-> on est sur une ligne de rupture
                aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + Mid("ZZZZZZZZZZZZZZZZZZ", 1, 15 - CInt(Entry(2, aItem.Key, "|")))
            End If
        Next
End Select
'-> Trier sur les entetes de colonne
aList.SortKey = aList.ColumnHeaders("Cache").Index - 1
If aList.SortOrder = lvwAscending Then
    aList.SortOrder = lvwDescending
Else
    aList.SortOrder = lvwAscending
End If
aList.Sorted = True

'-> on supprime la colonne cach�e
aList.ColumnHeaders.Remove ("Cache")

End Sub

Private Sub ListView2_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
'-> Trier sur les entetes de colonne
ColumnOrder Me.ListView2, ColumnHeader

End Sub

