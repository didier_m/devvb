Attribute VB_Name = "dm_turbocsv"
Option Explicit


Public aFile As dc_file
Dim Lignes As Collection
Dim StrError As String
Public appTurbo As String

Public FileCsv As String

Public Declare Function PostMessage Lib "user32.dll" Alias "PostMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long

Private hdlFile As Integer
Private TempFileName As String
Private Ligne As String

Private Const vbBackslash = "\"
Private Const ALL_FILES = "*.*"

Private Type FILETIME
   dwLowDateTime As Long
   dwHighDateTime As Long
End Type

Private Type WIN32_FIND_DATA
   dwFileAttributes As Long
   ftCreationTime As FILETIME
   ftLastAccessTime As FILETIME
   ftLastWriteTime As FILETIME
   nFileSizeHigh As Long
   nFileSizeLow As Long
   dwReserved0 As Long
   dwReserved1 As Long
   cFileName As String * 260
   cAlternate As String * 14
End Type

Private Type FILE_PARAMS
   bRecurse As Boolean
   bFindOrExclude As Long
   nCount As Long
   nSearched As Long
   sFileNameExt As String
   sFileRoot As String
End Type

Private Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long
   
Private Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
   
Private Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long

Private Declare Function lstrlen Lib "kernel32" Alias "lstrlenW" (ByVal lpString As Long) As Long

Private Declare Function PathMatchSpec Lib "shlwapi" Alias "PathMatchSpecW" (ByVal pszFileParam As Long, ByVal pszSpec As Long) As Long

Private fp As FILE_PARAMS
Private stopSearch As Boolean

Public Enum pTypeLigne
    pEntete
    pColumnHeader
    pData
    pSeparateur
End Enum

Sub Main()

'-> on initialise le message d'erreur
StrError = "Une erreur est survenue lors du chargement"

Set aFile = New dc_file

'-> on charge les valeurs par defaut
If GetIniFileValue("PARAM", "Separator", searchExe("turbocsv.ini")) <> "" Then
    aFile.Separator = GetIniFileValue("PARAM", "Separator", searchExe("turbocsv.ini"))
Else
    aFile.Separator = ";"
End If

If Val(GetIniFileValue("PARAM", "NbColonneHeader", searchExe("turbocsv.ini"))) <> 0 Then
    aFile.NbColonneHeader = Val(GetIniFileValue("PARAM", "NbColonneHeader", searchExe("turbocsv.ini")))
Else
    aFile.NbColonneHeader = 1
End If

If Val(GetIniFileValue("PARAM", "NbEntete", searchExe("turbocsv.ini"))) <> 0 Then
    aFile.NbEntete = Val(GetIniFileValue("PARAM", "NbEntete", searchExe("turbocsv.ini")))
Else
    aFile.NbEntete = 8
End If

If Val(GetIniFileValue("PARAM", "NbColonneGauche", searchExe("turbocsv.ini"))) <> 0 Then
    aFile.NbColonneGauche = Val(GetIniFileValue("PARAM", "NbColonneGauche", searchExe("turbocsv.ini")))
Else
    aFile.NbColonneGauche = 2
End If

'-> selon que l'on est en mode console ou pas
If ZonChargGet("CONSOLE") <> "" Then
    aFile.FileName = ZonChargGet("FILENAME")
    aFile.FileOutPut = ZonChargGet("FILEOUTPUT")
    LoadCsv aFile.FileName
    Call CreateSpool
    End
End If

'-> on ouvre la feuille de chargement du Ducs
df_Turbocsv.init

End Sub

Public Function ZonChargGet(StrZonCharg As String) As String

'--> cette fonction retourne la valeur d'un zoncharge
Dim zcharge As String
Dim ztrav As String
Dim i As Integer

'-> on regarde si on a changer le command$
zcharge = Entry(2, Command$, "|")
zcharge = Replace(zcharge, "'", "")
zcharge = Replace(zcharge, Chr(34), "")

For i = 2 To NumEntries(zcharge, "_")
  ztrav = UCase$(Trim(Entry(1, Entry(i, zcharge, "_"), "=")))
  Select Case ztrav
    Case UCase(StrZonCharg)
        ZonChargGet = Trim(Entry(2, Entry(i, zcharge, "_"), "="))
        Exit For
  End Select
Next



End Function

Private Function Uncote(strValue As String) As String
'--> cette fonction va permettre de sortir les ''
Uncote = Mid(strValue, 2, Len(strValue) - 2)
End Function

Public Function FileExist(Fichier As String) As Boolean

On Error GoTo GestError

If Dir$(Fichier) <> "" Then FileExist = True
Exit Function

GestError:
    FileExist = False

End Function

Public Function searchExe(strFile As String) As String
'-> cette fonction va chercher les fichiers en fonction du param�trage
'   diff�rentes possibilit�s
'   rien
'   chemin spe|chemin std
'   gloident
Dim sPath As String
Dim sParam As String

stopSearch = False

If Command$ = "" Then
    sParam = "deal"
Else
    sParam = Command$
End If

If InStr(1, sParam, "|") <> 0 Then
    '-> on lit d'abord dans le spe puis le standard
    If Dir$(Entry(1, sParam, "|") & "\" & strFile) <> "" Then
        searchExe = Entry(1, sParam, "|") & "\" & strFile
    Else
        If Dir$(Entry(1, sParam, "|") & "\" & strFile) <> "" Then
            searchExe = Entry(1, sParam, "|") & "\" & strFile
        Else
            searchExe = App.Path & "\" & strFile
        End If
    End If
Else
    '-> � partir du gloident on va essayer de s'y retrouver!
    '-> on pointe sur le chemin de base
    sPath = App.Path
    '-> pour les test
    'sPath = "R:\V6\tools\exe"
    sPath = Replace(sPath, "/", "\")
    '-> on regarde si on est en v51 ou v52
    If InStr(1, sPath, "\V51\") <> 0 Or InStr(1, sPath, "\V52\") <> 0 Then
        If InStr(1, sPath, "\V51\") <> 0 Then
            sPath = Mid(sPath, 1, InStr(1, sPath, "\V51\", vbTextCompare) + 4)
        Else
            sPath = Mid(sPath, 1, InStr(1, sPath, "\V52\", vbTextCompare) + 4)
        End If
        '-> repertoire v52/sophie de l'ident
        If searchExe = "" Then searchExe = SearchForFiles(Replace(sPath, "\deal\", "\" & Trim(sParam) & "\", , , vbTextCompare) & "\Sophie\", strFile)
        '-> repertoire v52/sophie de deal
        If searchExe = "" Then searchExe = SearchForFiles(sPath & "\Sophie\", strFile)
    Else '-> on regarde si on est en V6
        If InStr(1, sPath, "\dog\", vbTextCompare) <> 0 Or InStr(1, sPath, "\tools\", vbTextCompare) <> 0 Then
            sPath = Replace(sPath, "\tools\", "\dog\", , , vbTextCompare)
            sPath = Mid(sPath, 1, InStr(1, sPath, "\dog\", vbTextCompare) + 4)
            '-> on cherche dans le repertoire maquette du client
            If searchExe = "" Then searchExe = SearchForFiles(Replace(sPath, "\dog\", "\" & Trim(sParam) & "\sophie\", , , vbTextCompare), strFile)
            '-> on cherche dans le repertoire spe du client
            If searchExe = "" Then searchExe = SearchForFiles(sPath & "\" & sParam & "\", strFile)
            '-> on cherche dans le repertoire des maquettes
            If searchExe = "" Then searchExe = SearchForFiles(Replace(sPath, "\dog\", "\maq\gui\sophie\", , , vbTextCompare), strFile)
        End If
    End If
    '-> on regarde dans le repertoire courant
    If searchExe = "" Then searchExe = SearchForFiles(App.Path & "\" & Trim(sParam) & "\", strFile)
    If searchExe = "" Then searchExe = SearchForFiles(App.Path & "\", strFile)
    '-> on ramene le repertoire courant
    If searchExe = "" Then searchExe = App.Path & "\" & strFile
End If

End Function

Private Function SearchForFiles(sRoot As String, sfile As String, Optional sFiltreDirectory As String) As String
    '--> cette fonction cherche des fichiers � partir d'une directorie
   Dim WFD As WIN32_FIND_DATA
   Dim hFile As Long
  
   With fp
      .sFileRoot = QualifyPath(sRoot)      'chemin de d�part
      .sFileNameExt = sfile                'fichier (* ? autoris�
      .bRecurse = 1                             'True = recherche recursive
      .bFindOrExclude = 1                       '0=inclure, 1=exclure
   End With
   
   
   hFile = FindFirstFile(sRoot & "*.*", WFD)
   If hFile <> -1 Then
      Do
        If stopSearch = True Then Exit Function
        DoEvents
        'si c'est un repertoire on boucle
         If (WFD.dwFileAttributes And vbDirectory) Then
            If Asc(WFD.cFileName) <> CLng(46) Then
                If fp.bRecurse Then
                    SearchForFiles = SearchForFiles(sRoot & TrimNull(WFD.cFileName) & vbBackslash, sfile)
                End If
            End If
         Else
           'doit etre un fichier..
            If MatchSpec(WFD.cFileName, fp.sFileNameExt) Then
                If sFiltreDirectory <> "" Then
                    If InStr(1, sRoot & TrimNull(WFD.cFileName), sFiltreDirectory) <> 0 Then
                        SearchForFiles = sRoot & TrimNull(WFD.cFileName)
                        stopSearch = True
                        Exit Do
                    End If
                Else
                    SearchForFiles = sRoot & TrimNull(WFD.cFileName)
                    stopSearch = True
                    Exit Do
                End If
            End If
         End If
      Loop While FindNextFile(hFile, WFD)
   End If
   Call FindClose(hFile)
End Function

Private Function QualifyPath(sPath As String) As String
   If Right$(sPath, 1) <> vbBackslash Then
      QualifyPath = sPath & vbBackslash
   Else
      QualifyPath = sPath
   End If
End Function

Private Function TrimNull(startstr As String) As String
   TrimNull = Left$(startstr, lstrlen(StrPtr(startstr)))
End Function

Private Function MatchSpec(sfile As String, sSpec As String) As Boolean
   MatchSpec = PathMatchSpec(StrPtr(sfile), StrPtr(sSpec)) = fp.bFindOrExclude
End Function

Public Sub SkinColorModify(aForm As Form)
'--> ceci est un essai de changement de couleur
Dim Lcolor As Long
Dim aControl As Control
Dim aPicture As PictureBox
Dim bKeepBorder As Boolean
Dim sKinName As String

On Error Resume Next

'-> V�rifier que l'on trouve le fichier d'application du skin
sKinName = searchExe("Skin.ini")
If Dir$(sKinName) = "" Then Exit Sub

'-> Appliquer un skin s'il y en a un
If GetIniFileValue("SKIN", "BODYBACKCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.BodyBackColor = getColor(GetIniFileValue("SKIN", "BODYBACKCOLOR", sKinName))
If GetIniFileValue("SKIN", "BODYFORECOLOR", sKinName) <> "" Then aForm.DogSkinObject1.BodyForeColor = getColor(GetIniFileValue("SKIN", "BODYFORECOLOR", sKinName))
If GetIniFileValue("SKIN", "BORDERCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.BorderColor = getColor(GetIniFileValue("SKIN", "BODYBACKCOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERBACKCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.HeaderBackColor = getColor(GetIniFileValue("SKIN", "HEADERBACKCOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERFORECOLOR", sKinName) <> "" Then aForm.DogSkinObject1.HeaderForeColor = getColor(GetIniFileValue("SKIN", "HEADERFORECOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERLINECOLOR", sKinName) <> "" Then aForm.DogSkinObject1.HeaderLineColor = getColor(GetIniFileValue("SKIN", "HEADERLINECOLOR", sKinName))
If GetIniFileValue("SKIN", "SHADOWCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.ShadowColor = getColor(GetIniFileValue("SKIN", "SHADOWCOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERUSELINE", sKinName) <> "" Then aForm.DogSkinObject1.HeaderUseLine = Val(GetIniFileValue("SKIN", "HEADERUSELINE", sKinName))
If GetIniFileValue("SKIN", "BODYFONTNAME", sKinName) <> "" Then aForm.DogSkinObject1.BodyFont.Name = GetIniFileValue("SKIN", "BODYFONTNAME", sKinName)
If GetIniFileValue("SKIN", "BODYFONTSIZE", sKinName) <> "" Then aForm.DogSkinObject1.BodyFont.Size = CDbl(GetIniFileValue("SKIN", "BODYFONTSIZE", sKinName))
If GetIniFileValue("SKIN", "BODYFONTBOLD", sKinName) <> "" Then aForm.DogSkinObject1.BodyFont.Bold = Val(GetIniFileValue("SKIN", "BODYFONTBOLD", sKinName))
If GetIniFileValue("SKIN", "BODYFONTITALIC", sKinName) <> "" Then aForm.DogSkinObject1.BodyFont.Italic = Val(GetIniFileValue("SKIN", "BODYFONTITALIC", sKinName))
If GetIniFileValue("SKIN", "HEADERICO", sKinName) <> "" Then
    '-> on regarde si on trouve bien l'image
    If Dir(GetIniFileValue("SKIN", "HEADERICO", sKinName)) <> "" Then
        aForm.DogSkinObject1.HeaderIco = LoadPicture(GetIniFileValue("SKIN", "HEADERICO", sKinName))
        aForm.DogSkinObject1.HeaderIcoNa = LoadPicture(GetIniFileValue("SKIN", "HEADERICO", sKinName))
    Else
        '-> on regarde si l'icone n'est pas dans le repertoire courant
        aForm.DogSkinObject1.HeaderIco = LoadPicture(App.Path & "\" & GetIniFileValue("SKIN", "HEADERICO", sKinName))
        aForm.DogSkinObject1.HeaderIcoNa = LoadPicture(App.Path & "\" & GetIniFileValue("SKIN", "HEADERICO", sKinName))
    End If
End If
If GetIniFileValue("SKIN", "HEADERICONA", sKinName) <> "" Then aForm.DogSkinObject1.HeaderUseLine = Val(GetIniFileValue("SKIN", "HEADERICONA", sKinName))
If GetIniFileValue("SKIN", "HEADERFONTNAME", sKinName) <> "" Then aForm.DogSkinObject1.HeaderFont.Name = GetIniFileValue("SKIN", "HEADERFONTNAME", sKinName)
If GetIniFileValue("SKIN", "HEADERFONTSIZE", sKinName) <> "" Then aForm.DogSkinObject1.HeaderFont.Size = CDbl(GetIniFileValue("SKIN", "HEADERFONTSIZE", sKinName))
If GetIniFileValue("SKIN", "HEADERFONTBOLD", sKinName) <> "" Then aForm.DogSkinObject1.HeaderFont.Bold = Val(GetIniFileValue("SKIN", "HEADERFONTBOLD", sKinName))
If GetIniFileValue("SKIN", "HEADERFONTITALIC", sKinName) <> "" Then aForm.DogSkinObject1.HeaderFont.Italic = Val(GetIniFileValue("SKIN", "HEADERFONTITALIC", sKinName))
If GetIniFileValue("SKIN", "KEEPBORDER", sKinName) = "1" Then bKeepBorder = True
'-> on parcours les differents elements pour faire une variation de couleur
For Each aControl In aForm
    If TypeOf aControl Is Shape Then
        If aControl.BackColor = &HF1CFB1 Or aControl.BackColor = 16708585 Then aControl.BackColor = aForm.DogSkinObject1.BodyBackColor
        If aControl.BorderColor = &HC78D4B Then aControl.BorderColor = aForm.DogSkinObject1.BorderColor
    End If
    If TypeOf aControl Is DealCmdButton Or TypeOf aControl Is PictureBox Or TypeOf aControl Is DealCheckBox Or TypeOf aControl Is TextBox Then
        If aControl.BackColor = &HF1CFB1 Or aControl.BackColor = 16579059 Then aControl.BackColor = aForm.DogSkinObject1.BodyBackColor
    End If
    If TypeOf aControl Is DealGrid Then
        aControl.BackColor = aForm.DogSkinObject1.BodyBackColor
        aControl.BackColorFixed = aForm.DogSkinObject1.ShadowColor
        aControl.BackColorSel = aForm.DogSkinObject1.ShadowColor
        aControl.GridLineColor = aForm.DogSkinObject1.HeaderForeColor
    End If
    If TypeOf aControl Is DealTextBox Or TypeOf aControl Is Label Then
        aControl.ForeColor = aForm.DogSkinObject1.HeaderForeColor
    End If
    If TypeOf aControl Is Label And bKeepBorder Then
        aControl.BorderStyle = 0
    End If

Next

End Sub

Public Function GetVariableEnv(strVariable As String) As String

Dim Res As Long
Dim lpbuffer As String

lpbuffer = Space$(500)
Res = GetEnvironmentVariable(strVariable, lpbuffer, Len(lpbuffer))
If Res <> 0 Then
    '-> Faire un trim sur le buffer
    lpbuffer = Mid$(lpbuffer, 1, Res)
Else
    lpbuffer = ""
End If

'-> Retouner la valeur
GetVariableEnv = lpbuffer

End Function

Public Function LoadCsv(aFilePath As String) As Boolean
Dim hdlFic As Integer
Dim Ligne As String
Dim aLigne As dc_ligne
Dim fileLength As Single 'pour le timer
Dim linelength As Single 'pour le timer
Dim tempTimer As Integer 'pour le timer
Dim i As Integer
Dim topFin As Boolean

On Error GoTo GestError:
aFile.FileName = aFilePath

'-> on se fait tout d'abord une petite rotation a l'interieur du fichier
aFilePath = rotateCSV(aFilePath)

fileLength = FileLen(aFilePath)

'-> on affiche le nom du fichier pour info
If ZonChargGet("CONSOLE") = "" Then
    df_Turbocsv.Label1.Caption = aFile.FileName
    df_Turbocsv.Text1.Text = aFile.FileName & ".turbo"
    aFile.FileOutPut = aFile.FileName & ".turbo"
End If

'-> Ouvrir le fichier binaire source
hdlFic = FreeFile
Open aFilePath For Input As #hdlFic
Line Input #hdlFic, Ligne

'-> on teste la premiere chaine pour v�rifier que l'on est bien sur un fichier Csv
If InStr(1, Ligne, ";") = 0 Then GoTo GestError
    
'-> on recharge le fichier csv contenant toute les lignes
Close #hdlFic
Open aFilePath For Input As #hdlFic
    
'-> on affiche le timer
df_Drloading.Show

'on initialise les champs
Set Lignes = New Collection

'-> on se charge les enregistrements du fichier � l'aide de la Structure que l'on vient de creer
'-> chaque ligne correspondant � un enregistrement
Do
    '-> Lecture de la ligne
    Line Input #hdlFic, Ligne
    '-> ce doevents sert a rafraichir le timer
    tempTimer = tempTimer + 1
    linelength = linelength + Len(Ligne) + 2
    If tempTimer = 1000 Then
        tempTimer = 0
        DoEvents
        df_Drloading.Label1.Caption = "Chargement en cours..." & CInt(linelength / fileLength * 100) & "%"
    End If
    '-> on se definit un nouvel objet ligne
    Set aLigne = New dc_ligne
    '-> on ecrit les donn�es de la ligne
    aLigne.Ligne = Ligne
    '-> on se memorise le nombre de colonnes de la ligne
    aLigne.NbCols = NumEntries(Ligne, aFile.Separator)
    '-> on determinera le type de la ligne dans une autre fonction servant de moteur
    Lignes.Add aLigne, "LIGNE|" & Lignes.Count + 1
Suite:
Loop While Not EOF(hdlFic)

'->on realise ici l'analyse des types de lignes
Call LoadTypeLigne

'->On se charge ici du prechargement de l'ecran avec les valeurs par defaut (� partir des types de lignes
df_Drloading.Label1.Caption = "Chargement de l'ecran..."
DoEvents
Call LoadListview

'-> on masque le timer
Unload df_Drloading

'-> on ferme le fichier
Close #hdlFic

'-> on affiche la feuille
If ZonChargGet("CONSOLE") = "" Then df_Turbocsv.Show

Exit Function
GestError:

'DisplayMessage StrError, dsmCritical, dsmOkOnly, "Erreur sur l'application"
'-> on ferme le fichier
Close #hdlFic
End
End Function

Private Function rotateCSV(aFilePath) As String
'-> cette fonction permet de faire une rotation a l'interieur d'un fichier csv
Dim hdlFic As Integer
Dim hdlFic2 As Integer
Dim Ligne As String
Dim strligne As String
Dim colLigne As Collection
Dim i As Long
Dim j As Long
Dim k As Long
Dim ll As Long

'-> Ouvrir le fichier binaire source
hdlFic = FreeFile
Open aFilePath For Input As #hdlFic
'-> on cree le fichier de destination
hdlFic2 = FreeFile
Open aFilePath & ".tmp" For Output As #hdlFic2

'-> on commence par ecrire les lignes de l'entete qui sont identiques
For i = 1 To aFile.NbEntete
    Line Input #hdlFic, Ligne
    Print #hdlFic2, Ligne
Next

'-> on va commencer par se metre a gauche les lignes
Set colLigne = New Collection
Do
    '-> Lecture de la ligne
    Line Input #hdlFic, Ligne
    colLigne.Add Ligne
Suite:
Loop While Not EOF(hdlFic)
ll = aFile.NbColonneGauche + 1
'-> maintenant on va chercher les lignes de rupture sur la premiere ligne
For i = 1 To NumEntries(colLigne(1), ";")
    If i <> 1 And Entry(i, colLigne(1), aFile.Separator) = "" Then
        '-> on se fait la rotation qui va bien
        '-> on commence par ecrire les colonnes de gauche
        For j = 1 To aFile.NbColonneGauche - 1
            strligne = strligne + aFile.Separator
        Next
        '-> c'est la derniere colonne tout les elements representent les entetes de colonne
        For k = 2 To colLigne.Count
            strligne = strligne + aFile.Separator + Entry(1, colLigne(k), aFile.Separator)
        Next
        '-> on ecrit la ligne des entetes de colonne
        Print #hdlFic2, strligne
        '-> maintenant on va ecrire les donn�es
        strligne = ""
        For j = ll To i
            If j = ll Then
                strligne = Entry(j - 1, colLigne(1), aFile.Separator)
            End If
            For k = 1 To colLigne.Count
                If NumEntries(colLigne(k), aFile.Separator) >= j Then
                    strligne = strligne + aFile.Separator + Entry(j, colLigne(k), aFile.Separator)
                Else
                    strligne = strligne + aFile.Separator
                End If
            Next
            '-> on ecrit la ligne des entetes de colonne
            Print #hdlFic2, strligne
            '-> maintenant on va ecrire les donn�es
            strligne = ""
        Next
        ll = i + 2
    Else
        '-> on continue de lire
        
    End If
Next
    
'-> on ferme les fichiers
Close #hdlFic
Close #hdlFic2

rotateCSV = aFilePath & ".tmp"

End Function

Public Function LoadListview()
'--> cette fonction permet de previsualiser la structure de l'edition
Dim i As Integer
Dim j As Integer
Dim aItem As ListItem

On Error Resume Next

'on vide les listview
df_Turbocsv.ListView1.ListItems.Clear
df_Turbocsv.ListView2.ListItems.Clear
df_Turbocsv.ListView3.ListItems.Clear
df_Turbocsv.ListView4.ListItems.Clear


'-> on charge dans le listview1 les entetes
For i = 1 To aFile.NbEntete
    Set aItem = df_Turbocsv.ListView1.ListItems.Add
    For j = 1 To Lignes(i).NbCols
        If j = 1 Then
            aItem.Text = Entry(j, Lignes(i).Ligne, ";")
        Else
            aItem.ListSubItems.Add , , Entry(j, Lignes(i).Ligne, ";")
        End If
    Next
Next
FormatListView df_Turbocsv.ListView1

'-> on se charge les entetes de colonnes
' ajout des colonnes
For i = 1 To Lignes(aFile.NbEntete + 1).NbCols
    df_Turbocsv.ListView4.ColumnHeaders.Add
Next
For i = aFile.NbEntete + 1 To aFile.NbEntete + aFile.NbColonneHeader
    Set aItem = df_Turbocsv.ListView4.ListItems.Add
    For j = 1 + aFile.NbColonneGauche To Lignes(i).NbCols
        If j = 1 Then
            aItem.Text = Entry(j, Lignes(i).Ligne, ";")
        Else
            aItem.ListSubItems.Add , , Entry(j, Lignes(i).Ligne, ";")
        End If
    Next
Next
FormatListView df_Turbocsv.ListView4

'-> on se charge les colonnes de gauche dans le listview
' ajout des colonnes
For i = 1 To aFile.NbColonneGauche
    df_Turbocsv.ListView2.ColumnHeaders.Add
Next
For i = aFile.NbEntete + aFile.NbColonneHeader + 1 To Lignes.Count
    'tant qu'on est sur des lignes de donn�es on continu
    If Lignes(i).TypeDeLigne = pData Then
        Set aItem = df_Turbocsv.ListView2.ListItems.Add
        For j = 1 To aFile.NbColonneGauche
            If j = 1 Then
                aItem.Text = Entry(j, Lignes(i).Ligne, ";")
            Else
                aItem.ListSubItems.Add , , Entry(j, Lignes(i).Ligne, ";")
            End If
        Next
    Else
        i = Lignes.Count
    End If
Next
FormatListView df_Turbocsv.ListView2

'-> on se charge les data dans le tablezu
' ajout des colonnes
For i = 1 To Lignes(aFile.NbEntete + 1).NbCols
    df_Turbocsv.ListView3.ColumnHeaders.Add
Next
For i = aFile.NbEntete + aFile.NbColonneHeader + 1 To Lignes.Count
    If Lignes(i).TypeDeLigne = pData Then
        Set aItem = df_Turbocsv.ListView3.ListItems.Add
        For j = 1 + aFile.NbColonneGauche To Lignes(i).NbCols
            If j = 1 Then
                aItem.Text = Entry(j, Lignes(i).Ligne, ";")
            Else
                aItem.ListSubItems.Add , , Entry(j, Lignes(i).Ligne, ";")
            End If
        Next
    Else
        i = Lignes.Count
    End If
Next
FormatListView df_Turbocsv.ListView3

End Function

Public Sub FormatListView(List As Object)

'---> Cette proc�dure formatte les entetes d'un listView

Dim i As Long
Dim x As Object

'-> Ne rien faire si pas de colonnes
If List.ColumnHeaders.Count = 0 Then Exit Sub

'-> De base toujours cr�er un enregistrement avec les entetes de colonnes
Set x = List.ListItems.Add(, "DEALENREGENTETE")

For i = 0 To List.ColumnHeaders.Count - 1
    '-> Ajouter le libelle de l'entete de la colonne
    If i = 0 Then
        x.Text = List.ColumnHeaders(1).Text
    Else
        x.SubItems(i) = List.ColumnHeaders(i + 1).Text
    End If
    SendMessage List.hwnd, LVM_SETCOLUMNWIDTH, i, 0
Next

'-> Supprimer le premier enregistrement
List.ListItems.Remove ("DEALENREGENTETE")

End Sub


Public Function LoadTypeLigne()
'--> cette fonction va permettre de definir le type de la ligne selon les
' categories entete, colonneheader, data
Dim aLigne As dc_ligne
Dim bEntete As Boolean
Dim bColonneHeader As Boolean
Dim bData As Boolean
Dim iPos As Integer
Dim j As Long

'-> par defaut au debut on est sur l'entete
bEntete = True
bColonneHeader = False
bData = False
iPos = 1
j = 1

For j = 1 To Lignes.Count
    '-> attention ne pas inverser l'ordre pour la logique
    Set aLigne = Lignes(j)
    aLigne.Index = j
    '-> cas des datas
    If bData Then
        If Trim(Replace(aLigne.Ligne, ";", "")) <> "" Then
            aLigne.TypeDeLigne = pData
        Else
            '-> une ligne blanche marque la fin des data
            bData = False
            bColonneHeader = True
            aLigne.TypeDeLigne = pSeparateur
        End If
    End If
    '-> cas des entetes de colonne
    If bColonneHeader Then
        If Trim(aLigne.Ligne) <> "" Then
            aLigne.TypeDeLigne = pColumnHeader
            If iPos = aFile.NbColonneHeader Then
                '-> on remet a barreau
                iPos = 1
                '-> on passe sur les data
                bColonneHeader = False
                bData = True
            Else
                aLigne.TypePos = iPos
                iPos = iPos + 1
            End If
        Else
            aLigne.TypeDeLigne = pSeparateur '-> on est encore sur une ligne vide
        End If
    End If
    '-> cas de l'entete
    If bEntete Then
        If j <= aFile.NbEntete Then
            aLigne.TypeDeLigne = pEntete
        Else
            bEntete = False
            bColonneHeader = True
            j = j - 1
        End If
    End If
Next

End Function

Public Function CreateSpool()
'--> cette fonction va generer la creation d'un spool
'-> pour cela on va utiliser un listview
Dim i As Long
Dim j As Long
Dim posLigne As Long
Dim posCol As Long
Dim bEntete As Boolean
Dim bColonneHeader As Boolean
Dim bData As Boolean
Dim bPrint As Boolean

df_Drloading.Show
df_Drloading.Label1.Caption = "Chargement..."
'-> on est en creation
If Dir(aFile.FileOutPut) <> "" Then Kill aFile.FileOutPut
bEntete = True
'-> on va remplir le listview tant qu'il y a de la place pour les lignes
For i = aFile.NbEntete To Lignes.Count
    '-> on ecrit les donnees des lignes
    If bData Then
        i = printData(i)
        bData = False
        bPrint = True
    End If
    '-> on ecrit les entetes de colonne
    If bColonneHeader Then
        i = PrintColonneHeader(i)
        bColonneHeader = False
        bData = True
    End If
    '-> on ecrit l'entete
    If bEntete Then
        '-> bon l'entete on se la met a gauche
        Call Printenteteligne
        bEntete = False
        bColonneHeader = True
    End If
    '-> on ecrit les donn�es dans le spool
    If bPrint Then
        '-> on se formatte bien les colonnes pour la largeur des colonnes en automatique
        Call CreateListview
        bPrint = False
        '-> et c'est pres a repartir
        bEntete = True
    End If
Next
Dim strBuffer As String

If ZonChargGet("CONSOLE") = "" Then ShellExecute df_Turbocsv.hwnd, "Open", aFile.FileOutPut, vbNullString, strBuffer, 1

'-> on masque le timer
Unload df_Drloading

End Function

Private Function printData(iLigne As Long) As Long
'-> on ecrit toutes les donn�es de la ligne dans le listview tempo
' a partir de la ligne specifi�e
Dim i As Long
Dim curHeight As Long
Dim aItem As ListItem

'-> on boucle tant que l'on est sur des donnees et que l'on est pas en bas de la page
Do While Lignes(iLigne).TypeDeLigne = pData 'And curHeight < 560
    Set aItem = df_Turbocsv.ListviewTrav.ListItems.Add
    '-> on ecrit toutes les donn�es
    For i = 1 To Lignes(iLigne).NbCols
        If i = 1 Then
            aItem.Text = Entry(i, Lignes(iLigne).Ligne, ";")
        Else
            aItem.ListSubItems.Add
            aItem.ListSubItems(i - 1).Text = Entry(i, Lignes(iLigne).Ligne, ";")
        End If
    Next
    curHeight = curHeight + df_Turbocsv.ListviewTrav.ListItems(1).Height
    iLigne = iLigne + 1
    If iLigne > Lignes.Count Then
        printData = iLigne
        Exit Function
    End If
Loop
'-> on retourne la ou on en est
printData = iLigne - 1
End Function

Private Function PrintColonneHeader(iLigne As Long) As Long
'-> on ecrit les entetes de colonne dans le listview tempo
Dim i As Long

'-> on ecrit toutes les donn�es
For i = 1 To Lignes(iLigne).NbCols
    df_Turbocsv.ListviewTrav.ColumnHeaders.Add
    df_Turbocsv.ListviewTrav.ColumnHeaders(i).Text = Trim(Entry(i, Lignes(iLigne).Ligne, ";"))
Next

PrintColonneHeader = iLigne

End Function

Private Function Printenteteligne() As Long
'-> cette fonction met en memoire la chaine a imprimer comme entete
Dim aLigne As dc_ligne
Dim i As Integer
aFile.strEntete = ""

For i = 1 To aFile.NbEntete
    aFile.strEntete = aFile.strEntete & Replace(Lignes(i).Ligne, ";", "")
Next
Printenteteligne = aFile.NbEntete
End Function

Private Function CreateListview()
'--> cette fonction par de listviewtrav vers listviewtoprint en fonction de la largeur des colonnes
Dim i As Long
Dim j As Long
Dim k As Long
Dim l As Long
Dim bPrint As Boolean
Dim bApend As Boolean
Dim bLeft As Boolean
Dim aItem As ListItem
Dim curWidth As Long
Dim curI As Long
Dim curJ As Long
        curI = 1
        bApend = False
        bLeft = True
        '-> on se formate le listview
        FormatListView df_Turbocsv.ListviewTrav
        '-> on se retaille la largeur des colonnes
        For i = aFile.NbColonneGauche + 1 To df_Turbocsv.ListviewTrav.ColumnHeaders.Count
            If df_Turbocsv.ListviewTrav.ColumnHeaders(i).Width > 150 Then
                df_Turbocsv.ListviewTrav.ColumnHeaders(i).Width = df_Turbocsv.ListviewTrav.ColumnHeaders(i).Width / 3
            End If
            If df_Turbocsv.ListviewTrav.ColumnHeaders(i).Width < 90 Then df_Turbocsv.ListviewTrav.ColumnHeaders(i).Width = 90
        Next
        i = 0
        '-> tant que l'on a pas tout envoyer on continue � imprimer des nouvelles pages
        Do While i < df_Turbocsv.ListviewTrav.ColumnHeaders.Count
            If bLeft Then
                curWidth = 0
                '-> on ecrit ensuite les libell�s des colonnes de gauche
                For k = 1 To df_Turbocsv.ListviewTrav.ListItems.Count
                    Set aItem = df_Turbocsv.ListviewTrav.ListItems(k)
                    df_Turbocsv.ListViewToPrint.ListItems.Add
                    df_Turbocsv.ListViewToPrint.ListItems(k).Text = aItem.Text
                    If k = 1 Then
                        curWidth = curWidth + df_Turbocsv.ListviewTrav.ColumnHeaders(1).Width
                        df_Turbocsv.ListViewToPrint.ColumnHeaders.Add
                    End If
                    If aFile.NbColonneGauche > 1 Then
                        For j = 1 To aFile.NbColonneGauche - 1
                            df_Turbocsv.ListViewToPrint.ListItems(k).ListSubItems.Add
                            df_Turbocsv.ListViewToPrint.ListItems(k).ListSubItems(j) = aItem.SubItems(j)
                            If k = 1 Then
                                curWidth = curWidth + df_Turbocsv.ListviewTrav.ColumnHeaders(j + 1).Width
                                df_Turbocsv.ListViewToPrint.ColumnHeaders.Add
                            End If
                        Next
                    End If
                Next
                If curI < aFile.NbColonneGauche Then curI = curI + aFile.NbColonneGauche
                bLeft = False
            End If
            curJ = aFile.NbColonneGauche
            For i = curI To df_Turbocsv.ListviewTrav.ColumnHeaders.Count '- 1   ATTENTION mise en com DIDIER pour probleme manque derniere colonne
                '-> maintenant on prend juste les colonnes qu'il faut pour cela rentre sur la page en bouclant sur les colonnes
                If curWidth + df_Turbocsv.ListviewTrav.ColumnHeaders(i).Width < 1060 Then
                    curWidth = curWidth + df_Turbocsv.ListviewTrav.ColumnHeaders(i).Width
                    df_Turbocsv.ListViewToPrint.ColumnHeaders.Add
                    df_Turbocsv.ListViewToPrint.ColumnHeaders(df_Turbocsv.ListViewToPrint.ColumnHeaders.Count).Text = df_Turbocsv.ListviewTrav.ColumnHeaders(i).Text
                    df_Turbocsv.ListViewToPrint.ColumnHeaders(df_Turbocsv.ListViewToPrint.ColumnHeaders.Count).Width = df_Turbocsv.ListviewTrav.ColumnHeaders(i).Width
                    df_Turbocsv.ListViewToPrint.ColumnHeaders(df_Turbocsv.ListViewToPrint.ColumnHeaders.Count).Alignment = lvwColumnRight
                    For j = 1 To df_Turbocsv.ListviewTrav.ListItems.Count
                        '-> on ecrit pour cela dans un autre listview
                        df_Turbocsv.ListViewToPrint.ListItems(j).ListSubItems.Add
                        df_Turbocsv.ListViewToPrint.ListItems(j).ListSubItems(curJ) = df_Turbocsv.ListviewTrav.ListItems(j).SubItems(i - 1)
                    Next
                Else
                    '-> on quitte la boucle pour imprimer
                    Exit For
                End If
                'on passe aussi a la colonne suivante dans le listview de destination
                curJ = curJ + 1
            Next
            curI = i
            bApend = True
            bLeft = True
            curWidth = 0
            '-> on se formate le listview
            'FormatListView df_Turbocsv.ListViewToPrint
            For l = 1 To aFile.NbColonneGauche
                df_Turbocsv.ListViewToPrint.ColumnHeaders(l).Width = df_Turbocsv.ListviewTrav.ColumnHeaders(l).Width
            Next
            '-> on se lance une epuration
            ListViewEpure df_Turbocsv.ListViewToPrint
            '-> on imprime le nouveau listview
            strRetour = aFile.strEntete
            df_Edition.PrintListView df_Turbocsv.ListViewToPrint, aFile.FileOutPut, "test", , bApend
            '-> on se revide le listview de travail
            df_Turbocsv.ListViewToPrint.ColumnHeaders.Clear
            df_Turbocsv.ListViewToPrint.ListItems.Clear
        Loop
        df_Turbocsv.ListviewTrav.ListItems.Clear
        df_Turbocsv.ListviewTrav.ColumnHeaders.Clear
End Function

Private Sub ListViewEpure(aListView As ListView)
'--> cette fonction vire les colonne vide du listview
Dim aItem As ListItem
Dim i As Integer
Dim j As Integer
Dim topSuppr As Boolean

'-> On va regarder sur toutes les colonnes des donn�es
For i = aFile.NbColonneGauche To aListView.ColumnHeaders.Count - 1
    topSuppr = True
    For Each aItem In aListView.ListItems
        If aItem.ListSubItems(i) <> "" Then topSuppr = False
    Next
    If topSuppr = True Then
        aListView.ColumnHeaders(i + 1).Width = 0
    End If
Next

'-> on va maintenant regarder sur les lignes des donn�es
For j = aListView.ListItems.Count To 1 Step -1
    topSuppr = True
    Set aItem = aListView.ListItems(j)
    If aItem.Text <> "" Then topSuppr = False
    For i = aFile.NbColonneGauche + 1 To aItem.ListSubItems.Count
        If Trim(aItem.ListSubItems(i)) <> "" Then topSuppr = False
    Next
    If topSuppr = True Then
        aListView.ListItems.Remove (aItem.Index)
    End If
Next

End Sub

Public Function getColor(sColor As String) As Long
'--> cette fonction reccupere la couleur
Dim r, V, b As Integer
On Error Resume Next
'-> on decompose la couleur
r = Val("&H" & Mid(sColor, 5, 2))
V = Val("&H" & Mid(sColor, 7, 2))
b = Val("&H" & Mid(sColor, 9, 2))

getColor = RGB(r, V, b)

End Function

