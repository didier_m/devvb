Attribute VB_Name = "Subclasser"
Option Explicit

 ' D�claration des API
 Private Declare Function SetWindowLong _
     Lib "user32" _
     Alias "SetWindowLongA" _
     ( _
     ByVal hWnd As Long, _
     ByVal nIndex As Long, _
     ByVal dwNewLong As Long _
     ) _
     As Long
 Public Declare Function CallWindowProc _
     Lib "user32" _
     Alias "CallWindowProcA" _
     ( _
     ByVal lpPrevWndFunc As Long, _
     ByVal hWnd As Long, _
     ByVal Msg As Long, _
     ByVal wParam As Long, _
     ByVal lParam As Long _
     ) _
     As Long

 Private Const GWL_WNDPROC = (-4)

 ' Variables utilis�e par le programme
 Private colScControls As New Collection

 Public Sub Subclass(hWindow As Long, ObjectToCall As Object, FunctionToCall As String)
     Dim OldWndProc As Long
     Dim SCW As CSubclassedWindow

     'Si aucun autre contr�le n'est d�j� sousclass�
     If IsMemberInCollection("hwnd" & hWindow, colScControls) = False Then
         Set SCW = New CSubclassedWindow
         Set SCW.ObjectToCall = ObjectToCall
         SCW.FunctionToCall = FunctionToCall
         SCW.hWnd = hWindow

         'Red�finit la proc�dure � laquelle les messages doivent �tre envoy�s
         OldWndProc = SetWindowLong(hWindow, GWL_WNDPROC, AddressOf WndProc)
         SCW.OldProc = OldWndProc

         'Ajoute l'objet collection
         colScControls.add SCW, "hwnd" & hWindow
     End If

 End Sub

' Proc�dure appel� lorsqu'un nouveau message est � traiter
 Private Function WndProc(ByVal hWnd As Long, ByVal uMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long

     ' Bool�en permettant de savoir s'il faut retourner la valeur par d�faut ou non
     Dim bChangeValue As Boolean, lngNewValue As Long
     Dim SCW As CSubclassedWindow
     
     On Error GoTo errhandler
     
     Set SCW = colScControls("hwnd" & hWnd)

     If Not SCW Is Nothing Then
         Dim MsgCopy As Long, WndCopy As Long, wParamCopy As Long, lParamCopy As Long
         WndCopy = hWnd
         MsgCopy = uMsg
         wParamCopy = wParam
         lParamCopy = lParam
         WndProc = CallByName(SCW.ObjectToCall, SCW.FunctionToCall, VbMethod, WndCopy, MsgCopy, wParamCopy, lParamCopy, bChangeValue)
     
         ' Si on ne veut pas red�finir la valeur retourn�e, on retourne la valeur que retourne la proc�dure par d�faut
         If bChangeValue = False Then WndProc = CallWindowProc(SCW.OldProc, hWnd, uMsg, wParam, lParam)
     End If
     
     On Error GoTo 0
     
errhandler:
     If Err.Number Then
         Debug.Print "Subclassing error : " & Err.Description
         'On (re)tente de passer le message � la proc�dure par d�faut
         WndProc = CallWindowProc(SCW.OldProc, hWnd, uMsg, wParam, lParam)
         'On tente de d�sousclasser la fen�tre posant probl�me
         UnSubclass hWnd
     End If
 End Function

 Public Sub UnSubclass(hWindow As Long)

     ' Si un contr�le a d�j� �t� souclass�
     If IsMemberInCollection("hwnd" & hWindow, colScControls) Then
         ' Red�finit la proc�dure � laquelle les messages doivent �tre envoy�s
         SetWindowLong hWindow, GWL_WNDPROC, colScControls("hwnd" & hWindow).OldProc
         ' Supprime la r�f�rence � la fen�tre de la collection
         colScControls.Remove "hwnd" & hWindow
     End If

 End Sub

 Public Sub UnSubclassAll()
     Dim SCW As CSubclassedWindow
     
     For Each SCW In colScControls
         UnSubclass SCW.hWnd
     Next SCW
 End Sub


 Private Function IsMemberInCollection(Member, Collection As Collection) As Boolean
     On Error Resume Next
     Collection.Item Member
     IsMemberInCollection = (Err.Number = 0)
     Err.Clear
 End Function




