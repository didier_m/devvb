Attribute VB_Name = "m_dattouch"
Option Explicit

Private Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long
   
Private Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
   
Private Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long

Private Declare Function lstrlen Lib "kernel32" Alias "lstrlenW" (ByVal lpString As Long) As Long

Private Declare Function PathMatchSpec Lib "shlwapi" Alias "PathMatchSpecW" (ByVal pszFileParam As Long, ByVal pszSpec As Long) As Long

Public Declare Function SetFileTime Lib "kernel32" (ByVal hFile As Long, lpcreation As FILETIME, lpLecture As FILETIME, lpLastWriteTime As FILETIME) As Long
Public Declare Function GetFileTime Lib "kernel32" (ByVal hFile As Long, lpCreationTime As FILETIME, lpLastAccessTime As FILETIME, lpLastWriteTime As FILETIME) As Long

Private Declare Function CreateFile Lib "kernel32" Alias "CreateFileA" (ByVal lpFileName As String, ByVal dwDesiredAccess As Long, ByVal dwShareMode As Long, ByVal NoSecurity As Long, ByVal dwCreationDisposition As Long, ByVal dwFlagsAndAttributes As Long, ByVal hTemplateFile As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long

Private Declare Function SetFileCreatedTime Lib "kernel32" Alias "SetFileTime" (ByVal hFile As Long, lpCreationTime As FILETIME, ByVal NullLastAccessTime As Long, ByVal NullLastWriteTime As Long) As Long
Private Declare Function SetFileAccessTime Lib "kernel32" Alias "SetFileTime" (ByVal hFile As Long, ByVal NullCreationTime As Long, lpLastAccessTime As FILETIME, ByVal NullWriteTime As Long) As Long
Private Declare Function SetFileModifiedTime Lib "kernel32" Alias "SetFileTime" (ByVal hFile As Long, ByVal NullCreationTime As Long, ByVal NullLastAccessTime As Long, lpLastWriteTime As FILETIME) As Long
Private Type FILETIME
    dwLowDateTime As Long
    dwHighDateTime As Long
End Type

Private Const GENERIC_READ = &H80000000
Private Const GENERIC_WRITE = &H40000000
Private Const FILE_SHARE_READ = &H1
Private Const FILE_SHARE_WRITE = &H2
Private Const OPEN_EXISTING = 3

Private Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Private Declare Function FileTimeToLocalFileTime Lib "kernel32" (lpFileTime As FILETIME, lpLocalFileTime As FILETIME) As Long
Private Declare Function LocalFileTimeToFileTime Lib "kernel32" (lpLocalFileTime As FILETIME, lpFileTime As FILETIME) As Long
Private Declare Function FileTimeToSystemTime Lib "kernel32" (lpFileTime As FILETIME, lpSystemTime As SYSTEMTIME) As Long
Private Declare Function SystemTimeToFileTime Lib "kernel32" (lpSystemTime As SYSTEMTIME, lpFileTime As FILETIME) As Long
'-> API de temporisation
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Private Declare Function SetFilePointer Lib "kernel32" _
(ByVal hFile As Long, ByVal lDistanceToMove As Long, lpDistanceToMoveHigh As Long, ByVal dwMoveMethod As Long) As Long
'-> API pour gestion des process
Public Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Public Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Long, lpExitCode As Long) As Long

Private fp As FILE_PARAMS
Private stopSearch As Boolean

Public Enum pTypeLigne
    pEntete
    pColumnHeader
    pData
    pSeparateur
End Enum

Public Type WIN32_FIND_DATA
   dwFileAttributes As Long
   ftCreationTime As FILETIME
   ftLastAccessTime As FILETIME
   ftLastWriteTime As FILETIME
   nFileSizeHigh As Long
   nFileSizeLow As Long
   dwReserved0 As Long
   dwReserved1 As Long
   cFileName As String * 260
   cAlternate As String * 14
End Type

Public Type FILE_PARAMS
   bRecurse As Boolean
   bFindOrExclude As Long
   nCount As Long
   nSearched As Long
   sFileNameExt As String
   sFileRoot As String
End Type

Private Declare Function MultiByteToWideChar Lib "kernel32.dll" ( _
    ByVal CodePage As Long, _
    ByVal dwFlags As Long, _
    ByVal lpMultiByteStr As Long, _
    ByVal cbMultiByte As Long, _
    ByVal lpWideCharStr As Long, _
    ByVal cchWideChar As Long _
) As Long

Public Const SEE_MASK_INVOKEIDLIST = &HC

Private Declare Function WideCharToMultiByte Lib "kernel32.dll" ( _
    ByVal CodePage As Long, _
    ByVal dwFlags As Long, _
    ByVal lpWideCharStr As Long, _
    ByVal cchWideChar As Long, _
    ByVal lpMultiByteStr As Long, _
    ByVal cbMultiByte As Long, _
    ByVal lpDefaultChar As Long, _
    ByVal lpUsedDefaultChar As Long _
) As Long

Public Declare Function ShellExecuteEx Lib "shell32" (lpSEI As SHELLEXECUTEINFO) As Long
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

'-> API pour affichage d'une page de propri�t�s
Public Type SHELLEXECUTEINFO
             cbSize As Long
             fMask As Long
             hWnd As Long
             lpVerb As String
             lpFile As String
             lpParameters As String
             lpDirectory As String
             nShow As Long
             hInstApp As Long
             ' Optional fields
             lpIDList As Long
             lpClass As String
             hkeyClass As Long
             dwHotKey As Long
             hIcon As Long
             hProcess As Long
   End Type

Public Const CP_ACP        As Long = 0          ' Default ANSI code page.
Public Const CP_UTF8       As Long = 65001      ' UTF8.
Public Const CP_UTF16_LE   As Long = 1200       ' UTF16 - little endian.
Public Const CP_UTF16_BE   As Long = 1201       ' UTF16 - big endian.
Public Const CP_UTF32_LE   As Long = 12000      ' UTF32 - little endian.
Public Const CP_UTF32_BE   As Long = 12001      ' UTF32 - big endian.

'-> Pour gestion des fichiers
Public Const OFS_MAXPATHNAME& = 128
Public Const OF_EXIST& = &H4000
Public Const OF_READ& = &H0

Public Declare Function GetStdHandle Lib "kernel32" _
(ByVal nStdHandle As Long) As Long

Private Declare Function WriteFile Lib "kernel32" _
(ByVal hFile As Long, _
lpBuffer As Any, _
ByVal nNumberOfBytesToWrite As Long, _
lpNumberOfBytesWritten As Long, _
lpOverlapped As Any) As Long

Public Const STD_OUTPUT_HANDLE = -11&

Private Type COORD
        x As Integer
        y As Integer
End Type

Private Type SMALL_RECT
        left As Integer
        top As Integer
        right As Integer
        bottom As Integer
End Type

Public Declare Function OpenFile& Lib "kernel32" (ByVal lpFileName As String, lpReOpenBuff As OFSTRUCT, ByVal wStyle As Long)
Public Declare Function GetFileSize& Lib "kernel32" (ByVal hFile As Long, lpFileSizeHigh As Long)
Private Declare Function ReadFile Lib "kernel32" _
(ByVal hFile As Long, lpBuffer As Any, ByVal nNumberOfBytesToRead As Long, lpNumberOfBytesRead As Long, ByVal lpOverlapped As Any) As Long
'-> Pour Maj d'une fen�tre
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long

Public Type OFSTRUCT
    cBytes As Byte
    fFixedDisk As Byte
    nErrCode As Integer
    Reserved1 As Integer
    Reserved2 As Integer
    szPathName(OFS_MAXPATHNAME) As Byte
End Type

Private Type CONSOLE_SCREEN_BUFFER_INFO
        dwSize As COORD
        dwCursorPosition As COORD
        wAttributes As Integer
        srWindow As SMALL_RECT
        dwMaximumWindowSize As COORD
End Type
Private Declare Function GetConsoleScreenBufferInfo Lib "kernel32" _
(ByVal hConsoleOutput As Long, _
lpConsoleScreenBufferInfo As CONSOLE_SCREEN_BUFFER_INFO) As Long

Private Declare Function SetConsoleTextAttribute Lib "kernel32" _
(ByVal hConsoleOutput As Long, ByVal wAttributes As Long) As Long

Private Const FOREGROUND_BLUE = &H1     '  text color contains blue.
Private Const FOREGROUND_GREEN = &H2     '  text color contains green.
Private Const FOREGROUND_INTENSITY = &H8     '  text color is intensified.
Private Const FOREGROUND_RED = &H4     '  text color contains red.
Private hOutput             As Long
Private Const vbBackslash = "\"
Private Const ALL_FILES = "*.*"
Private Const FILE_BEGIN = 0

'-> Pour gestion des process m�moire
Public Const PROCESS_QUERY_INFORMATION = &H400
Public Const STILL_ACTIVE = &H103

Public fList As String
Public quiet As Boolean
Public keepdate As Boolean
Public recursive As Boolean
Public version As Boolean
Public help As Boolean
Public wdir As String
Public rdir As String
Public edir As String
Public lfile As String
Public Convert As String
Public ismouchard As Boolean
Public isexport As Boolean
Public Mouchard As String
Public cpt As Long
Public hdlMouchard As Integer

'-->
Public topDiff As Boolean
Public topAddRef As Boolean
Public topAddOri As Boolean
Public topSame As Boolean

Public topRepSame As Boolean
Public topIdentSuppr As Boolean
Public topIdentPrio As Boolean
Public topHorodatage As Boolean
Public topHorodatageFiche As Boolean
Public ident As String
Public repignore As String
Public repautori As String
Public fileignore As String
Public fileautori As String
Public dateMini As String
Public dateMaxi As String

Public cptFileTot As Long
Public cptFileIdem As Long
Public cptFileRef As Long
Public cptFileWork As Long
Public cptFileRefFiltre As Long
Public cptFileWorkFiltre As Long

Public cptFileCopy As Long
Public cptFileListview As Long
Public cptFileNewer As Long
Public cptFileAbsRef As Long
Public cptFileModify As Long

Public cptFileAnachro As Long

Public quit As Boolean

Public Sub Main()
    Form1.Show
End Sub


Public Function CreateFileC(sRoot As String, refDir As String, sFile As String, Optional sFiltreDirectory As String) As String
    '--> cette fonction charge le listview a partir d'un chemin root
 Dim DSO As Object
 Dim WFD As WIN32_FIND_DATA
 Dim hFile As Long
 Dim sP As String
 Dim refFile As String
 Dim wFile As String
 Dim exportFile As String
 Dim oriFile As String
 Dim strRep As String
 Dim topExport As Boolean
 Dim i As Long
 Dim cpt As Long
 
 On Error Resume Next
 
 If quit Then
    Exit Function
 End If
 
 With fp
    .sFileRoot = QualifyPath(sRoot)      'chemin de d�part
    .sFileNameExt = sFile                'fichier (* ? autoris�
    .bRecurse = 1                             'True = recherche recursive
    .bFindOrExclude = 1                       '0=inclure, 1=exclure
 End With
 
 Set DSO = CreateObject("DSOFile.OleDocumentProperties")
 Dim cD As Date
 Dim aD As Date
 Dim mD As Date
 Dim cD2 As Date
 Dim aD2 As Date
 Dim mD2 As Date
 
 hFile = FindFirstFile(sRoot & "*.*", WFD)
 If hFile <> -1 Then
    Do
      'DoEvents
      If quit Then GoTo suite
        'si c'est un repertoire on boucle
        If (WFD.dwFileAttributes And vbDirectory) Then
           If Asc(WFD.cFileName) <> CLng(46) Then
               If fp.bRecurse Then
                   strRep = sRoot & TrimNull(WFD.cFileName) & vbBackslash
                   DoEvents
                   If repautori <> "" Then
                         For i = 1 To NumEntries(Replace(strRep, wdir, "", , , vbTextCompare), "\")
                            If InStr(1, repautori, Entry(i, Replace(strRep, wdir, ""), "\")) = 0 Then
                                 GoTo suite
                            End If
                         Next
                   End If
                   If recursive Then CreateFileC strRep, refDir, sFile
               End If
           End If
        Else
           '-> on est sur un fichier
           'doit etre un fichier..
           If MatchSpec(WFD.cFileName, fp.sFileNameExt) Then
              wFile = sRoot & TrimNull(WFD.cFileName)
              Form1.FileAddInListview wFile, refFile, "+>", ""
           End If
        End If
    Loop While FindNextFile(hFile, WFD)
 End If
suite:
Call FindClose(hFile)

DoEvents
End Function


' Purpose:  Take a string whose bytes are in the byte array <the_abytCPString>, with code page <the_nCodePage>, convert to a VB string.
Private Function FromCPString(ByRef the_abytCPString() As Byte, ByVal the_nCodePage As Long) As String

    Dim sOutput                     As String
    Dim nValueLen                   As Long
    Dim nOutputCharLen              As Long

    ' If the code page says this is already compatible with the VB string, then just copy it into the string. No messing.
    If the_nCodePage = CP_UTF16_LE Then
        FromCPString = the_abytCPString()
    Else

        ' Cache the input length.
        nValueLen = UBound(the_abytCPString) - LBound(the_abytCPString) + 1

        ' See how big the output buffer will be.
        nOutputCharLen = MultiByteToWideChar(the_nCodePage, 0&, VarPtr(the_abytCPString(LBound(the_abytCPString))), nValueLen, 0&, 0&)

        ' Resize output byte array to the size of the UTF-8 string.
        sOutput = Space$(nOutputCharLen)

        ' Make this API call again, this time giving a pointer to the output byte array.
        MultiByteToWideChar the_nCodePage, 0&, VarPtr(the_abytCPString(LBound(the_abytCPString))), nValueLen, StrPtr(sOutput), nOutputCharLen

        ' Return the array.
        FromCPString = sOutput

    End If

End Function

Private Function QualifyPath(sPath As String) As String
   If right$(sPath, 1) <> vbBackslash Then
      QualifyPath = sPath & vbBackslash
   Else
      QualifyPath = sPath
   End If
End Function

Private Function TrimNull(startstr As String) As String
   TrimNull = left$(startstr, lstrlen(StrPtr(startstr)))
End Function

Private Function MatchSpec(sFile As String, sSpec As String) As Boolean
   MatchSpec = PathMatchSpec(StrPtr(sFile), StrPtr(sSpec)) = fp.bFindOrExclude
End Function

Private Function GetFileTimes(ByVal file_name As String, _
    ByRef creation_date As Date, ByRef access_date As Date, _
    ByRef modified_date As Date, ByVal local_time As _
    Boolean) As Boolean
Dim file_handle As Long
Dim creation_filetime As FILETIME
Dim access_filetime As FILETIME
Dim modified_filetime As FILETIME
Dim file_time As FILETIME

    ' Assume something will fail.
    GetFileTimes = True

    ' Open the file.
    file_handle = CreateFile(file_name, GENERIC_READ, _
        FILE_SHARE_READ Or FILE_SHARE_WRITE, _
        0&, OPEN_EXISTING, 0&, 0&)
    If file_handle = 0 Then Exit Function

    ' Get the times.
    If GetFileTime(file_handle, creation_filetime, _
        access_filetime, modified_filetime) = 0 Then
        CloseHandle file_handle
        Exit Function
    End If

    ' Close the file.
    If CloseHandle(file_handle) = 0 Then Exit Function

    ' See if we should convert to the local
    ' file system time.
    If local_time Then
        ' Convert to local file system time.
        FileTimeToLocalFileTime creation_filetime, file_time
        creation_filetime = file_time

        FileTimeToLocalFileTime access_filetime, file_time
        access_filetime = file_time

        FileTimeToLocalFileTime modified_filetime, file_time
        modified_filetime = file_time
    End If

    ' Convert into dates.
    creation_date = FileTimeToDate(creation_filetime)
    access_date = FileTimeToDate(access_filetime)
    modified_date = FileTimeToDate(modified_filetime)

    GetFileTimes = False
End Function

Public Function SetFileTimes(ByVal file_name As String, _
    ByVal creation_date As Date, ByVal access_date As Date, _
    ByVal modified_date As Date, ByVal local_times As _
    Boolean) As Boolean
Dim file_handle As Long
Dim creation_filetime As FILETIME
Dim access_filetime As FILETIME
Dim modified_filetime As FILETIME
Dim file_time As FILETIME

    ' Assume something will fail.
    SetFileTimes = True

    ' Convert the dates into FILETIMEs.
    creation_filetime = DateToFileTime(creation_date)
    access_filetime = DateToFileTime(access_date)
    modified_filetime = DateToFileTime(modified_date)

    ' Convert the file times into system file times.
    If local_times Then
        LocalFileTimeToFileTime creation_filetime, file_time
        creation_filetime = file_time

        LocalFileTimeToFileTime access_filetime, file_time
        access_filetime = file_time

        LocalFileTimeToFileTime modified_filetime, file_time
        modified_filetime = file_time
    End If

    ' Open the file.
    file_handle = CreateFile(file_name, GENERIC_WRITE, _
        FILE_SHARE_READ Or FILE_SHARE_WRITE, _
        0&, OPEN_EXISTING, 0&, 0&)
    If file_handle = 0 Then Exit Function

'creation_date = FileTimeToDate(creation_filetime)

    ' Set the times.
    If SetFileTime(file_handle, creation_filetime, _
        access_filetime, modified_filetime) = 0 Then
        CloseHandle file_handle
        Exit Function
    End If

    ' Close the file.
    If CloseHandle(file_handle) = 0 Then Exit Function

    SetFileTimes = False
End Function

Private Function FileTimeToDate(file_time As FILETIME) As Date
Dim system_time As SYSTEMTIME

    ' Convert the FILETIME into a SYSTEMTIME.
    FileTimeToSystemTime file_time, system_time

    ' Convert the SYSTEMTIME into a Date.
    FileTimeToDate = SystemTimeToDate(system_time)
End Function

Private Function DateToFileTime(ByVal the_date As Date) As FILETIME
Dim system_time As SYSTEMTIME
Dim file_time As FILETIME

    ' Convert the Date into a SYSTEMTIME.
    system_time = DateToSystemTime(the_date)

    ' Convert the SYSTEMTIME into a FILETIME.
    SystemTimeToFileTime system_time, file_time
    DateToFileTime = file_time
End Function

Private Function SystemTimeToDate(system_time As SYSTEMTIME) As Date
    With system_time
        SystemTimeToDate = CDate( _
            Format$(.wDay) & "/" & _
            Format$(.wMonth) & "/" & _
            Format$(.wYear) & " " & _
            Format$(.wHour) & ":" & _
            Format$(.wMinute, "00") & ":" & _
            Format$(.wSecond, "00"))
    End With
End Function
' Convert a Date into a SYSTEMTIME.
Private Function DateToSystemTime(ByVal the_date As Date) As SYSTEMTIME
    With DateToSystemTime
        .wYear = Year(the_date)
        .wMonth = Month(the_date)
        .wDay = Day(the_date)
        .wHour = Hour(the_date)
        .wMinute = Minute(the_date)
        .wSecond = Second(the_date)
    End With
End Function

Public Function FileExist(Fichier As String) As Boolean

On Error GoTo GestError

If Dir$(Fichier) <> "" Then FileExist = True
Exit Function

GestError:
    FileExist = False

End Function

Private Function FileSame(ByVal file1 As String, ByVal file2 As String) As Boolean
'--> cette fonction compare 2 fichiers
FileSame = False
'-> on commence par comparer la taille
If FileLen(file1) <> FileLen(file2) Then
    Exit Function
End If
If OpenFileAPI(file1) = OpenFileAPI(file2) Then FileSame = True

End Function

Public Function OpenFileAPI(ByRef FileName As String) As String
    Dim hOrgFile As Long
    Dim nSize As Long
    Dim Ret As Long
    
    On Error GoTo ErrorHandler
    
    'R�cup�re un Handle pour manipuler le flux m�moire du fichier
    hOrgFile = CreateFile(FileName, _
                            GENERIC_READ, _
                            FILE_SHARE_READ Or FILE_SHARE_WRITE, _
                            ByVal 0&, OPEN_EXISTING, 0, 0)

    'Taille du fichier
    nSize = GetFileSize(hOrgFile, 0)

    DoEvents

    'Initialise le pointeur sur le fichier
    SetFilePointer hOrgFile, 0, 0, FILE_BEGIN

    OpenFileAPI = Space(nSize)

    'Charge le contenu du fichier dans la variable OpenFileAPI
    ReadFile hOrgFile, ByVal OpenFileAPI, nSize, Ret, ByVal 0&
     
    DoEvents
     
    'Ferme le fichier
    CloseHandle hOrgFile
    
    Exit Function
ErrorHandler:
    OpenFileAPI = ""
    CloseHandle hOrgFile
End Function


Public Sub Trace(sText As String)

End Sub

Public Function IsPidRunning(lngPid As Long) As Boolean

'---> Cette proc�dure v�rifie si un process sp�cifi� ( lngPid ) est encore actif

Dim hdlProcess As Long
Dim ExitCode As Long

'-> Ne rien faire si process � 0
If lngPid = 0 Then Exit Function
'-> R�cup�rer le handle du process affect� � ce pide
hdlProcess = OpenProcess(PROCESS_QUERY_INFORMATION, False, lngPid)
'-> V�rifier si le PID est toujours valide
GetExitCodeProcess hdlProcess, ExitCode
If ExitCode = STILL_ACTIVE Then IsPidRunning = True

End Function

