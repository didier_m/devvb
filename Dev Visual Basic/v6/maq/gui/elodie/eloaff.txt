
#PARGFICECR  ^ds-req01.ecr
#PARGFICENT ^Chapitre^Libelle^Num^Champ/Regle/master^ ^Date Type^Size^Format^^Table^ordre^Attribut^Tables.champs^Init (DGS)/Put-value^DGETSOC^Link obli^Find
#PARECR  ^stadet.ecr
PARAMETER00   ^ ?    ^     ^    ^   ^  ^     ^   ^^   ^ 1^^eloaff,cptgen,TABLES,auxili,soldes  

SIGNALETIQUE   ^ ?                   ^     ^                         ^   ^     ^    ^                 ^^         ^ 1    ^^       
SIGNALETIQUE   ^ dict-codsoc         ^ 005 ^ Codsoc                  ^ 0 ^     ^    ^                 ^^ eloaff  ^ 1    ^^ eloaff.Codsoc 
SIGNALETIQUE   ^ dict-codetb         ^ 010 ^ Codetb                  ^ 0 ^     ^    ^                 ^^ eloaff  ^ 2    ^^ eloaff.Codetb 
SIGNALETIQUE   ^ dict-codaff         ^ 015 ^ codaff                  ^ 0 ^     ^    ^                 ^^ eloaff  ^ 3    ^^ eloaff.Codaff 
SIGNALETIQUE   ^ Libell� abr�g�      ^ 020 ^ Libabr                  ^ 0 ^ char ^ 30   ^  x(30)       ^^ eloaff  ^ 4    ^^ eloaff.libarb 
SIGNALETIQUE   ^ libelle             ^ 021 ^ Libel[1]                ^ 0 ^ char ^ 30    ^ x(30)       ^^ eloaff  ^ 5    ^^ eloaff.libel|[1] 

dates   ^ ?                      ^     ^                             ^   ^      ^     ^              ^^         ^ 2    ^^       
dates   ^ Exercice debut         ^ 005 ^ execpt-deb                  ^ 0 ^ char ^  6  ^ x(4)         ^^ eloaff  ^ 1    ^^ eloaff.execpt-deb
dates   ^ P�riode  D�but         ^ 015 ^ percpt-deb                  ^ 0 ^ char ^  6  ^ x(4)         ^^ eloaff  ^ 2    ^^ eloaff.percpt-deb
dates   ^ Exercice de cloture    ^ 025 ^ execpt-clo                  ^ 0 ^ char ^  6  ^ x(4)         ^^ eloaff  ^ 3    ^^ eloaff.execpt-clo
dates   ^ P�riode  de cloture    ^ 035 ^ execpt-clo                  ^ 0 ^ char ^  6  ^ x(4)         ^^ eloaff  ^ 4    ^^ eloaff.percpt-clo
dates   ^ Exercice arr�t�        ^ 045 ^ execpt-arr                  ^ 0 ^ char ^  6  ^ x(4)         ^^ eloaff  ^ 5    ^^ eloaff.execpt-arr
dates   ^ P�riode  d'arr�t�      ^ 055 ^ execpt-arr                  ^ 0 ^ char ^  6  ^ x(4)         ^^ eloaff  ^ 6    ^^ eloaff.percpt-arr

ANALYTIQUE   ^ ?                   ^     ^                           ^   ^      ^     ^              ^^         ^ 3    ^^       
ANALYTIQUE   ^ dict-devise         ^ 005 ^ Devise [1]                ^ 0 ^      ^     ^              ^^ eloaff  ^ 1    ^^ eloaff.Devise [1]
ANALYTIQUE   ^ Taux devise         ^ 015 ^ Txdev  [1]                ^ 0 ^ dec  ^ 7   ^ >>9.999      ^^ eloaff  ^ 2    ^^ eloaff.Txdev  [1]
ANALYTIQUE   ^ Affaire rebond      ^ 025 ^ segana-arr [1]            ^ 0 ^ char ^ 15  ^ x(10)        ^^ eloaff  ^ 3    ^^ eloaff.segana-arr [1]
ANALYTIQUE   ^ Divers              ^ 035 ^ segana-arr [2]            ^ 0 ^ char ^ 15  ^ x(10)        ^^ eloaff  ^ 4    ^^ eloaff.segana-arr [2]

