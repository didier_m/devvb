/* V423ELO-FIC 828 Suppression de la selection mini/maxi sur l'etablissement. <EVO bes 23/04/02> */
/* V423ELO-FIC 818 programme de suppression des devises IN. <NEW bes 15/04/02> */
Def frame fr-col1.
form messprog.libel1 [4]  format "x(13)" skip
     messprog.libel1 [6]  format "x(13)" 
            with frame fr-col1 row 3 col 10 width 15 no-label overlay {v6frame.i}
            with title messprog.libel1 [1] .

Def frame fr-col2.
form soc-min skip
     exe-min skip   
            with frame fr-col2 row 3 col 24 width 5 no-label overlay {v6frame.i}
            with title messprog.libel1 [2] .

Def frame fr-col3.
form soc-max skip
     exe-max skip   
            with frame fr-col3 row 3 col 29 width 5 no-label overlay {v6frame.i}
            with title messprog.libel1 [3] .
            
Def frame fr-top.
form dev-top lib-top
            with frame fr-top row 8 col 10  no-label overlay {v6frame.i}
            with title messprog.libel1 [7] .            
            
Def frame fr-mouch.
form ma-impri
            with frame fr-mouch row 8 col 30  no-label overlay {v6frame.i}
            with title messprog.libel1 [11] .                        

