/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 620/elopro/inc/tbordcal.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 10825 01-10-04!New!air            !Nouveau module tableau de bord et saisie de budget                    !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!                      !                                     !tables                                          !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/* GESTION DES OPERANDES DE CALCUL SPECIFIQUES */

    when "SPECIF1"  /* ex. : SPECIF1-Z0201 : permet de recuperer le % saisi sur le niveau 1 de l'axe Z02 */
    then do :

        find tables where tables.codsoc = ""
                      and tables.etabli = ""
                      and tables.typtab = "ANA"
                      and tables.prefix = "AXE-" + entry ( 2 , t-calc.operande[ ii ] , "-" )
                      and tables.codtab = entry ( int ( substring ( entry ( 2 , t-calc.operande[ ii ] , "-" ) , 4 ,2 ) ) , dog-tbord.segment )
                      no-lock no-error.
        if available tables 
        then do cc = col-mini to col-maxi :
            assign cal-mtcpt[cc + jj] = cal-mtcpt[cc + jj] + ( tables.nombre[31] / 100 )
                   cal-mtdev[cc + jj] = cal-mtdev[cc + jj] + ( tables.nombre[31] / 100 ).
        end. /* do cc = col-mini to col-maxi */

    end. /* when "SPECIF-1" */

