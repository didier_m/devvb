/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 620/elopro/inc/x620-b00.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 10367 02-09-04!New!lbo            !TSPE38 - Flux intragroupe chez Sade.                                  !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*- x620-b00.i : Flux intragroupe [ DEFINITIONS ] -*/

Def {1} Shared Stream maquette.

Def {1} Shared var codsoc-min Like Mvtcpt.Codsoc                       no-undo.
Def {1} Shared var codsoc-max Like Mvtcpt.Codsoc     init "ZZZZ"       no-undo.
Def {1} Shared var codetb-min Like Mvtcpt.Codetb                       no-undo.
Def {1} Shared var codetb-max Like Mvtcpt.Codetb     init "ZZZZ"       no-undo.
Def {1} Shared var journal-min Like Mvtcpt.journal                       no-undo.
Def {1} Shared var journal-max Like Mvtcpt.journal     init "ZZZ"        no-undo.

Def {1} Shared var codgen-min Like Mvtcpt.codgen                       no-undo.
Def {1} Shared var codgen-max Like Mvtcpt.codgen     init "ZZZZZZZZZZ" no-undo.
Def {1} Shared var codgrp-min as char                                  no-undo.
Def {1} Shared var codgrp-max as char                init "ZZZZZZZZZZ" no-undo.

Def {1} Shared var x620-codsoc      Like Mvtcpt.Codsoc          no-undo.
Def {1} Shared var x620-codetb      Like Mvtcpt.Codetb          no-undo.
Def {1} Shared var x620-journal      Like Mvtcpt.journal          no-undo.
Def {1} Shared var x620-codaux      Like Mvtcpt.Codaux          no-undo.

Def {1} Shared var x620-mvtok       as char                     no-undo.
Def {1} Shared var x620-mvt-rowid   as rowid                    no-undo.
Def {1} Shared var indice-an       as int                      no-undo.
Def {1} Shared var indice-max      as int                      no-undo.
Def {1} Shared var test-date       as date                     no-undo.
Def {1} Shared var code-vue        as char format "x(3)"       no-undo.
Def {1} Shared var lib-vue         as char format "x(30)"      no-undo.
Def {1} Shared var s-exe           as char format "x(4)"       no-undo.
Def {1} Shared var s-per           as char format "x(3)"       no-undo.
Def {1} Shared var s-permax        as char format "x(3)"       no-undo.
Def {1} Shared var recap           as char format "x"          no-undo.
Def {1} Shared var x620-vue-maq     as char format "x(5)"       no-undo.
def {1} shared var x620-devcpt      as char format "x(4)"       no-undo.
def {1} shared var x620-seq         as int  format "ZZZZZZZZ"   no-undo.

Def {1} Shared var x620-nb-mvt      as int   no-undo.
Def {1} Shared var x620-montant     as dec   no-undo.
Def {1} Shared var x620-recid       as recid no-undo.


Def {1} Shared var x620-top-lettrage as char no-undo.
Def {1} Shared var tot-TTC-devcpt   as dec  no-undo.
Def {1} Shared var tot-TTC-devpie   as dec  no-undo.
Def {1} Shared var tot-TTC-devini   as dec  no-undo.

Def {1} Shared var tot-TVA-devcpt   as dec  no-undo.
Def {1} Shared var tot-TVA-devpie   as dec  no-undo.
Def {1} Shared var tot-TVA-devini   as dec  no-undo.

Def {1} Shared var tot-HT-devcpt    as dec  no-undo.
Def {1} Shared var tot-HT-devpie    as dec  no-undo.
Def {1} Shared var tot-HT-devini    as dec  no-undo.


Def {1} Shared var racine-4         as char no-undo.
Def {1} Shared var racine-tva       as char no-undo.
Def {1} Shared var racine-7         as char no-undo.

Def {1} shared variable m-sequence    as int  format "zzzzzzzz"          no-undo.

Def {1} Shared var x620-rech-codsoc      Like Mvtcpt.Codsoc          no-undo.
Def {1} Shared var x620-rech-codetb      Like Mvtcpt.Codetb          no-undo.
Def {1} Shared var x620-rech-journal     Like Mvtcpt.journal          no-undo.
Def {1} Shared var x620-rech-codaux      Like Mvtcpt.Codaux          no-undo.
Def {1} Shared var x620-rech-codgen      Like Mvtcpt.Codgen          no-undo.
Def {1} Shared var x620-rech-codlet      Like Mvtcpt.cod-lettrage    no-undo.
Def {1} Shared var x620-rech-ordre       as   char                   no-undo.

Def {1} Shared var x620-cg-tva           Like Mvtcpt.Codgen          no-undo.
Def {1} Shared var x620-ttax-tva         Like Mvtcpt.Typtax          no-undo.
Def {1} Shared var x620-type-edit        as   int format "9"         no-undo.
Def {1} Shared var x620-lib-type-edit    as   char format "x(30)"    no-undo.
Def {1} Shared var x620-type-tri         as   int  format "Z9"     extent 10 no-undo.
Def {1} Shared var x620-lib-type-tri     as   char format "x(30)"  extent 10 no-undo.
Def {1} Shared var x620-chx-tri          as   int  format "Z9"     extent 10 no-undo.
Def {1} Shared var x620-ordre-tri        as   int  format "Z9"     extent 10 no-undo.
Def {1} Shared var x620-rupt-tri         as   int  format "Z9"     extent 10 no-undo.


/*
/* Selection sur code axe */
{compana.i {1}}
{charaxe.i {1}}
*/

/* selection periode de simultaion */
{ sel-pers.i {1} }

Def {1} Shared var x620-vect-err    as char no-undo.
Def {1} Shared var x620-trt-log     as char no-undo.