/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 620/elopro/src/x620auxges.p                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  9415 15-06-04!New!lau            !Creation et MAJ de AUXGES sur interface TIERS et DOG                  !
!-------------------------------------------------------------------------------------------------------------!
!                             I N C L U D E S                !                    T A B L E S                 !
!------------------------------------------------------------+------------------------------------------------!
!connect.i             !                                     !auxili                                          !
!regs-cha.i            !                                     !AUXGES                                          !
!regs-rec.i            !                                     !tabges                                          !
!majmoucb.i            !auxges                               !                                                !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
{ connect.i }

def input parameter int-typaux like auxili.typaux . 
def input parameter int-codaux like auxili.codaux .
def input parameter int-codsoc like codsoc-soc.

def var sauv-codsoc like codsoc-soc .
def var soc-aux-elo like codsoc-soc .
def var soc-aux-nat like codsoc-soc .
def var vec-typaux  as char  .

vec-typaux = "A62,C62,CLI,F62,FOU,G62,IMO,ORG,P62,T62,TIE" .

if lookup( int-typaux , vec-typaux ) = 0 then return "". 

sauv-codsoc = codsoc-soc .

codsoc-soc  = int-codsoc .

regs-app = "ELODIE" .
{ regs-cha.i }

regs-fileacc = "AUXILI" + int-typaux .
{ regs-rec.i }
soc-aux-elo = regs-soc .

regs-app = "NATHALIE" .
{ regs-cha.i }

regs-fileacc = "AUXGES" .
{ regs-rec.i }
soc-aux-nat = regs-soc .

find auxili where auxili.codsoc = soc-aux-elo
              and auxili.typaux = int-typaux
              and auxili.codaux = int-codaux
  no-lock no-error .
if available auxili 
then do :

  find AUXGES where auxges.codsoc = soc-aux-nat
                and auxges.typaux = auxili.typaux
                and auxges.codaux = auxili.codaux
    exclusive-lock no-error .
  if not available auxges 
  then do:
    create auxges.
    assign auxges.codsoc     = soc-aux-nat
           auxges.typaux     = auxili.typaux
           auxges.codaux     = auxili.codaux
           auxges.typaux-fac = auxili.typaux                   
           auxges.codaux-fac = auxili.codaux   
           .
  end.

  assign  auxges.adres[ 1 ] = auxili.adres[ 1 ]
          auxges.adres[ 2 ] = auxili.adres[ 2 ]
          auxges.adres[ 3 ] = auxili.adres[ 3 ]
          auxges.adres[ 4 ] = auxili.adres[ 4 ] 
          auxges.adres[ 5 ] = auxili.adres[ 5 ]
          .

  assign  auxges.ttva       = auxili.ttva
          auxges.devise     = auxili.devise
          auxges.modreg     = auxili.mreg
          auxges.code-pays  = auxili.pays
          auxges.siret      = auxili.siret
          auxges.libabr     = auxili.libabr
          .

  find TABGES where tabges.codsoc    = ""
                and tabges.etabli    = ""
                and tabges.typtab    = "ECH"
                and tabges.prefix    = "CODECH"
                and tabges.libel1[2] = auxili.calech 
    no-lock no-error .
  if available tabges then auxges.codech = tabges.codtab .

  { majmoucb.i auxges }
   validate auxges .    
   release  auxges .    
end. /* available auxili */

codsoc-soc = sauv-codsoc .             

regs-app = "ELODIE" .                  
{ regs-cha.i }                        



