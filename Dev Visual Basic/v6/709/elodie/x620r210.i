/*- x620r210.i : Specifique Sade - Reprendre la premiere dom du tiers -*/

/* societe de regroupement */
regs-fileacc = "AUXILI" + l-mvtcpt.typaux.
{ regs-rec.i }

/*- Mise a jour de la zone tire pour tiers payeur -*/
Find Auxili Where Auxili.Codsoc = regs-soc
              and Auxili.Typaux = mvtcpt.typaux
              and Auxili.Codaux = mvtcpt.codaux
no-lock no-error.
if available Auxili and Auxili.Typaux-pai > ""
then Assign substr ( l-mvtcpt.tire , 1 , 14 ) = "#" 
                                              + String ( Auxili.Typaux-pai , "XXX" ) 
                                              + String ( Auxili.Codaux-pai , "X(10)" ) no-error.
    

/*- Acces a la domiciliation pour initialiser le mouvement -*/
For First Tabdom field (libel1 codtab)
                 Where Tabdom.codsoc = regs-soc
                   and Tabdom.etabli = ""
                   and Tabdom.typtab = mvtcpt.typaux
                   and Tabdom.prefix = mvtcpt.codaux 
                 no-lock:

  assign l-mvtcpt.dom-banque = right-trim(Tabdom.libel1[ 1 ])
         l-mvtcpt.dom-ville  = right-trim(Tabdom.libel1[ 2 ])
         l-mvtcpt.dom-codban = right-trim(Tabdom.libel1[ 3 ])
         l-mvtcpt.dom-codgui = right-trim(Tabdom.libel1[ 4 ])
         l-mvtcpt.dom-compte = right-trim(Tabdom.libel1[ 5 ])
         l-mvtcpt.dom-clerib = right-trim(Tabdom.libel1[ 6 ])
         l-mvtcpt.domech     = right-trim(Tabdom.codtab)
         /* l-mvtcpt.modreg     = right-trim(Tabdom.libel1[ 7 ]) */
         .

end.
