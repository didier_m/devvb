/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! deal/Elopro/inc/intf-spe.i                                                                  !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  4452 10-03-03!NEW!lau            !Lien nature analytique / Sous affaire + Controle en saisie analytique !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
!-------------------------------------------------------------------------------------------------------------!
!                                                  P R O C E D U R E S                                        !
!-------------------------------------------------------------------------------------------------------------!
!intf-ana              !                                                                                      !
&End
**************************************************************************************************************/
/* V423ELO-FIC 835 Modification test nature analytique en fonction d'un plan de nature <EVO/BUG LAU 30/04/02> */
/* V423ELO-FIC 782 Ajout controle interface pour BECHET <NEW LAU 15/03/02> */
/* V423ELO-FIC 634 Saisie d'une nature ana en fonction d'un plan de nature assiocie au <NEW/EVO yo 21/01/02> */
/* V423ELO-FIC 402 Changement analytique <EVO SEB 11/09/01> */
/* V422ELO-FIC 241 ajout d'un specif pour 417 pour controle sur analytique <EVO yo 17/07/00> */
/* ELO4-FIC 502 Traitement sur fichier ASCII et sous evenements  zint4005.p      - EVO - */ 
/* REP-CLIENT */ 
/* ELO4-FIC 398 Gestion de la bi-monnaie par interface ASCII EVO */ 
/* ELO4-FIC 372 Gestion des specif analytique par interface  EVO */ 
/* les procedures specif des clients Vous pouvez eviter les test suivants exemple couple next-ana <> 0  */ 
procedure intf-ana: 
   define input parameter i-ana as int no-undo . 
   /* Test Pour SCASO       pour rejet analytique {x405intf.i}  */
   /* Test pour GTMH        pour rejet analytique {xtmhintf.i } */ 
   /* test pour LATECOERE   pour rejet analytique {xlatintf.i } */ 
   /* test pour 417         pour rejet analytique run x417intf.p (i-ana). */  
   /* test pour 544 BECHET  pour rejet analytique run x544intf.i (i-ana). */
   /* Saisie d'une nature ana en fonction d'un plan de nature */
    {intf-pla.i} 
   /* run anaintf.p ( input i-ana ) .  /* Ctrl std Lien Affaire SsAffaire Nature et analaxe */ */
end procedure .
/* Les autres SPECIFS exemple substitution de sous evenement CAS CCAS    {xcca-eve.i} */  
