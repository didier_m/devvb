/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 620/elodie/src/x620-majcif.p                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 11319 16-11-04!New!air            !OP 13039 : Moulinette de MAJ du code CIF                              !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!d-brokdeal.i          !"new"                                !                                                !
!sel.i                 !"new"                                !                                                !
!bat-ini.i             !"NEW"                                !                                                !
!do-zoncha.i           !                                     !                                                !
!eo-charge.i           !                                     !                                                !
!dt-chxsel-1.i         !"'x620-majcif'" "'elodie'" "'*x620-ma!                                                !
!bat-zon.i             !"sel-applic" "sel-applic" "c" "''" "'!                                                !
!dt-batmaj.i           !"x620-majci1.p" "batch"              !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
!WAIT-FOR              !Affichage et attente de saisie                                                        !
!BEFORE-VUE            !Avant affichage                                                                       !
!_____________________________________________________________________________________________________________!
!                                                  P R O C E D U R E S                                        !
!_____________________________________________________________________________________________________________!
!BEFORE-VUE            !                                                                                      !
!TRANSAC-TRIGGER       !Triggers de la palette de transaction                                                 !
!LOCAL-TRIGGER         !Triggers specifiques au programme                                                     !
&End
**************************************************************************************************************/
{ d-brokdeal.i  "new"    }
{ sel.i        "new"     }      /* Definition des zones pour les selections */
{ bat-ini.i    "NEW"     }
{ do-zoncha.i            } /*zone charge*/
{ eo-charge.i } /*  chargement des variables caractere : operat, memlang ...  */

batch-tenu = YES .

{dt-chxsel-1.i "'x620-majcif'"           
               "'elodie'"                
               "'*x620-majcif'"        
               "'x620-majcif'"
               "'seledi'"                
               "''"                      
               "''"                      
               "'Fr-Gene'" } 


hdl-widget = DEAL-HANDLE ( "w-saisie" ) .
on f2 of hdl-widget anywhere
do :
    deal-event ( "go-transac" , "_goto" ) .
    RETURN NO-APPLY .
end.

/************************************************************************************************************
&DOC-WAIT-FOR Affichage et attente de saisie 
&END
************************************************************************************************************/
If not this-procedure:persistent
then do: 
  RUN INPUT IN Brokdeal ( "" , "update" , "" ) .
  WAIT-FOR CLOSE OF THIS-PROCEDURE.
End.

/*************************************************************************************************************
&DOC-BEFORE-VUE Avant affichage 
&END
*************************************************************************************************************/
PROCEDURE BEFORE-VUE :

 Deal-Event      ( "titre"          , "_Goto"  ).

END .
/*************************************************************************************************************
&PROC-TRANSAC-TRIGGER Triggers de la palette de transaction
  - P1 : Evenement a tester ( go, cancel, modify, delete, copy )
  - P2 : Le mot clef donne a la palette
  - P3 : Parametre lie au bouton choisi
&END
*************************************************************************************************************/
PROCEDURE TRANSAC-TRIGGER :
  Define input parameter i-Event  as char no-undo.
  Define input parameter i-Motcle as char no-undo.
  Define input parameter i-param  as char no-undo.

  Case i-event :

    When "Go"
    Then do:

       if retour <> "" and search ( retour ) = ?
      then do :  
               Broker-trav = deal-get-dictab ( "Activ-Sel[message]" , "designation" ) .
               Message broker-trav View-as alert-box.
               RETURN "error " .
      End.

      {eo-charge.i}

      {bat-zon.i  "sel-applic"           "sel-applic"      "c" "''" "''" }
      {bat-zon.i  "sel-filtre"           "sel-filtre"      "c" "''" "''" }
      {bat-zon.i  "sel-sequence"         "sel-sequence"    "c" "''" "''" }
      {bat-zon.i  "codsoc-soc"           "codsoc-soc"      "c" "''" "''" }
      {bat-zon.i  "codetb-etb"           "codetb-etb"      "c" "''" "''" }
      {bat-zon.i  "operat"               "operat"          "c" "''" "''" }
      {bat-zon.i  "mem-langue"           "mem-langue"      "c" "''" "''" }
      {bat-zon.i  "zone-charg"           "zone-charg"      "m" "9"  "''" }

      {dt-batmaj.i "x620-majci1.p" "batch" }

      RUN deal-init IN brokdeal ( THIS-PROCEDURE ) . 

      IF batch-ba = 2 
      THEN DO: 
         RUN deal-init IN brokdeal ( THIS-PROCEDURE ) . 
         RETURN "error" .

      END.

      Apply "close" to this-procedure.

    End. /* Go */

    When "Cancel"
    THEN DO :
      RUN sel-del.p(sel-applic,sel-filtre,sel-sequence,YES) .  
      Apply "close" to this-procedure.
    END.

 End CASE. /* Case i-Event */

END PROCEDURE. /* TRANSAC-TRIGGER */

/************************************************************************************************************
&PROC-LOCAL-TRIGGER Triggers specifiques au programme 
  - P1 : Evenement � tester 
  - P2 : Le mot clef du champ qui d�clanche le trigger 
  - P3 : Param�tre associe au champ
&END
************************************************************************************************************/
PROCEDURE LOCAL-TRIGGER :
  Define input parameter i-Event  as char no-undo.
  Define input parameter i-Motcle as char no-undo.
  Define input parameter i-param  as char no-undo.

  Case i-motcle :

    When "titre"
    Then do:
              deal-event ( "b-filter" , "_goto" ) .
    end.
  End CASE. /* Case i-Motcle */ 
  Return "" .

END PROCEDURE. /* LOCAL-TRIGGER */

