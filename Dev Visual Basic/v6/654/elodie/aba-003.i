/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 627/elopro/inc/aba-003.i                                                                    !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52 10039 27-07-04!Evo!lau            !Mise en place des spécifiques de AMEC SPIE RAIL 627 en graphique      !
!-------------------------------------------------------------------------------------------------------------!
!                             I N C L U D E S                !                    T A B L E S                 !
!------------------------------------------------------------+------------------------------------------------!
!x627aba3.i            !                                     !                                                !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/* V423ELO-FIC 1107 Repartition au prorata des heures GMAO <NEW LAU 28/10/02> */
/* V423ELO-FIC 56 Repartition analytique en fonction d'une base et d'un prorata <NEW LAU 16/03/01> */
/* V42ELO-FIC 452 Module des abonnements analytiques <CLI lau 28/10/99> */
/* V42ELO-FIC 398 Evolution du module des Abonnements Analytiques <EVO lau 12/10/99> */
/* V42ELO-FIC 51 Adaptation des abonnements analytiques pour MAS <EVO lau 26/04/99> */
/* ELO4-FIC 782 Abonnement analytique des frais generaux <NEW lau 12/02/99> */
/*  rep-client */
/* specif pour spi      code CIF dans eloafx                    {xspiaba3.i} */
/* specif pour mas      code CIF dans eloafx                    {xmasaba3.i} */
/* specif pour gtm/ds   code CIF dans eloafx + boucle sous aff  {xgdsaba3.i} */
/* specif pour LEFORT   code CIF dans eloafx                    {xlefaba3.i} */
/* specif pour NORD FRANCE CONSTRUCTION code CIF dans eloafx    {x444aba3.i} */
/* specif pour RINEAU   code CIF dans eloafx                    {x498aba3.i} */
/* specif pour IRSID    code CIF dans eloafx                    {x196aba3.i} */
/* specif pour SEMVAT   code CIF dans eloafx                    {x353aba3.i} */
/* specif pour AMEC SPIE RAIL code CIF dans eloafx              {x627aba3.i} */
/* specif pour Sade     code CIF dans eloafx                */  {x620aba3.i}


