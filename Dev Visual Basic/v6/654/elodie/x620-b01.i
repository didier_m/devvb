/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 620/elopro/inc/x620-b01.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 10871 05-10-04!New!lau            !Edition des flux intra-groupe                                         !
!V52 10367 02-09-04!New!lbo            !TSPE38 - Flux intragroupe chez Sade.                                  !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*- x620-b01.i : Flux intragroupe [ BAT-ZON ] -*/
/*--------------------------------------------------------------------*/
/*         mot cle       valeur        type  taille      libelle      */
/*--------------------------------------------------------------------*/
{bat-zon.i "lib-prog"       "lib-prog"      "c" "''" "''"  }
{bat-zon.i "ma-impri"       "ma-impri"      "c" "''" "''"  }
{bat-zon.i "mem-langue"     "mem-langue"    "c" "''" "''"  }
{bat-zon.i "mem-lang"       "mem-lang"      "c" "''" "''"  }
{bat-zon.i "ma-maquet"      "ma-maquet"     "c" "''" "''"  }
{bat-zon.i "ma-esc"         "ma-esc"        "c" "''"  "''" }
{bat-zon.i "s-exe"          "s-exe"         "c" "''"  "''" }
{bat-zon.i "s-permax"       "s-permax"      "c" "''"  "''" }
{bat-zon.i "lib-vue"        "lib-vue"       "c" "''"  "''" }
{bat-zon.i "code-vue"       "code-vue"      "c" "''"  "''" }
{bat-zon.i "x620-seq"        "x620-seq"       "c" "''"  "''" }
/*
{bat-zon.i "x620-type-edit"  "x620-type-edit" "c" "''"  "''" }
{bat-zon.i "x620-type-tri"   "x620-type-tri"  "m" "10"  "''" }
{bat-zon.i "x620-chx-tri"    "x620-chx-tri"   "m" "10"  "''" }

{bat-zon.i "x620-rupt-tri"   "x620-rupt-tri"  "m" "10"  "''" }
*/
/*/* pour les periodes de simulation */
 * {bat-zon.i "sel-persim"     "sel-persim"    "m" "110" "''" }*/

{bat-zon.i "selection-applic"   "selection-applic"   "c" "''" "''" }
{bat-zon.i "selection-filtre"   "selection-filtre"   "c" "''" "''" }
{bat-zon.i "selection-sequence" "selection-sequence" "c" "''" "''" }
{ bat-zon.i "zone-charg"         "zone-charg"   "m" "8"   "''" }

