/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! deal/elopro/inc/intf-200.i                                                                  !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  9168 26-05-04!New!lbo            !Prob. interface controle cptgen sans test available CPTGEN (sur Ias)  !
!V52  8781 26-04-04!Bug!lbo            !Appel maintenance VINCI Prob. controle Journal sur interface.         !
!                  !   !               !int-journal = "" !!!!! (il n'y avait pas de controle)                 !
!V52  6840 24-10-03!Evo!lau            !MISE EN PLACE DES NORMES IAS                                          !
!-------------------------------------------------------------------------------------------------------------!
!                             I N C L U D E S                !                    T A B L E S                 !
!------------------------------------------------------------+------------------------------------------------!
!                      !                                     !messprog                                        !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/* V423ELO-FIC 1535 Interface : Controle sur comptes collectifs O/N <BUG LBO 20/10/03> */
/* V423ELO-FIC 1520 Rajout de la possibilité de reprise de segments analytiques pour Immo. <BUG/EVO isa 23/09/03> */
/* V423ELO-FIC 1512 Rajout champs justification du refus de BAP dans l'interface. <EVO LBO 08/09/03> */
/* V423ELO-FIC 1495 Interface type de traitement 1 ( Mouvement et Tiers ). Maj TABDOM <EVO LBO 07/08/03> */
/* V423ELO-FIC 1478 Controle du code famille dans l'interface. <EVO/BUG LBO 16/07/03> */
/* V423ELO-FIC 1392 INterface : Pb suite fiche 1282 (Pb sur nom de buffer TABLES) <BUG air 28/04/03> */
/* V423ELO-FIC 1304 Probleme de 63k et ajouter un control supplementaire sur unicite pie. <EVO/BUG isa 21/02/03> */
/* V423ELO-FIC 1282 Le lien type de taxe et comptes generaux : pas de test dans interface <EVO mat 12/02/03> */
/* V423ELO-FIC 1279 Interface. Conflit sur controle entre tiers/compte/typtaxe. <BUG LBO 11/02/03> */
/* V423ELO-FIC 1040 Interface. Probleme sur Calcul echeance (recup param. calage) <BUG LBO 23/09/02> */
/* V423ELO-FIC 1039 Interface. Si parametrage en position longueur du numero document <EVO LBO 23/09/02> */
/* V423ELO-FIC 990 Suite fiche V423ELO-985 : Pb sur variables utilisees <BUG air 30/07/02> */
/* V423ELO-FIC 985 INtegration de fichier ASCII : document multi-journal sans erreur <BUGU air 18/07/02> */
/* V423ELO-FIC 784 Controle unicite de la piece doit etre fait apres le control du tier <BUG LAU 18/03/02> */
/* V422ELO-FIC 988 Interdire la saisie d'une devise a partir d'une date (exemple FRF,DEM) <EVO isa 10/12/01> */
/* V423ELO-FIC 256 Dans l'interface des immos, on vide le mouchard des immos, on active <EVO mu 27/06/01> */
/* V422ELO-FIC 834 Pont PROCUR : Ne pas tester Exercice/Periode + Mouchard sur PROGLIB <BUG air 10/04/01> */
/* V423ELO-FIC 89 Possibilite de debloquer un tiers pour certains journaux. <EVO yo 29/03/01> */
/* V422ELO-FIC 556 Integration interface. <BUG LBO 16/11/00> */
/* V422ELO-FIC 467 Integration interface + control interface = enlever les BLANC sur IMP <BUG isa 16/10/00> */
/* V422ELO-FIC 399 Control JOURNAL/OPERATEUR : ajout option de la fiche 397 <EVO isa 25/09/00> */
/* V422ELO-FIC 344 test du sens des comptes incorrect. <BUG YO 31/08/00> */
/* V422ELO-FIC 167 Interface. Probleme sur deux lignes identiques. <BUG lbo 07/06/00> */
/* V422ELO-FIC 158 Interface ( controle des imputations analytiques choix 4 ). <BUG lbo 02/06/00> */
/* V422ELO-FIC 65 Pont PROCUR (test compte par racine-cg, no chrono ds cle engagent) <EVO bru 19/04/00> */
/* V422ELO-FIC 59 Creation des engagements (PROCUR) a partir interface comptable <EVO bru 14/04/00> */
/* V422ELO-FIC 55 Controle Echeance : rajout test sur mode de reglement <EVO air 13/04/00> */
/* V42ELO-FIC 720 Table autorisation Journal / CG (jauc-000) : rajout 5 zones supplem. <EVO air 22/02/00> */
/* V42ELO-FIC 620 Interface. ( societe de regroupement pour correspondance banque ) <EVO/BUG lbo 19/01/00> */
/* V42ELO-FIC 536 Dans  l'interface enlever le test si tva exonere - base-ht = 0 <BUG isa 08/12/99> */
/* V42ELO-FIC 535 Interface ( controle des imputations analytiques choix 4 ) <EVO lbo 08/12/99> */
/* V42ELO-FIC 491 Interface. ( test du sens de l'ecriture ) <BUG lbo 24/11/99> */
/* V42ELO-FIC 323 Conditionner le "Readkey" si le traitement n'est pas en BATCH. <BUG/EVO lbo 06/09/99> */
/* V42ELO-FIC 272 Ajout controle dans l'integration interface <BUG isa 13/08/99> */
/* V42ELO-FIC 185 Integration sur autorisation analytique SOC/ETB format(xx/XXX) <BUG isa 08/07/99> */
/* V42ELO-FIC 92 Possilite de comptabiliser en interface  ASCII/BASE sur periodes SIMU <EVO isa 21/05/99> */
/* V42ELO-FIC 42 Suite de 41 - Controle echeance ajoutee dans SAISIE/BAP/INTERFACE <EVO isa 16/04/99> */
/* ELO4-FIC 886 Pb Boucle de 1 a 4 pour 5 choix possibles sur variable ff <BUG fre 10/03/99> */
/* ELO4-FIC 784 Analytique figee par compte - BUG dans la fiche 481 <BUG isa 15/02/99> */
/* ELO4-FIC 780 Probleme d'arrondi sur la base tva - lire la decimalisation DEVISE <BUG isa 12/02/99> */
/* ELO4-FIC 556 Interface par BASE - Ajouter la BI-MONNAIE et la trangulation EURO <EVO isa 18/11/98> */
/* ELO4-FIC 492 Sur analytique acces si unicite des couples pour le precharger    - EVO - */ 
/* ELO4-FIC 481 Ajouter dans un plan analytique figee saisie soc et etablissement - EVO - */ 
/* ELO4-FIC 398 Gestion de la bi-monnaie par interface ASCII EVO */ 
/* ELO4-FIC 372 Gestion des specif analytique par interface  EVO */ 
/* ELO4-FIC 343 End mal place sur le test du segment dans intf-200.i -BUG- */
/* ELO4-FIC 304 ajout controle Analytique couple EVO*/ 
/* ELO4-FIC 225 Ajout controle unicite sur le numero de piece */ 
/* ELO4-FIC 136 ajout des zones specif - remplacer pause - pb sur dom */ 
/* ELO4-JPL 291297 pour cle spe pour accepter les MVT a zero */
/* ELO4-FIC 112 Message d'affichage des montants trouves EVO */ 
/* ELO4-FIC 44 Amelioration trt - Batch mouchard   EVO */
/*----------------------------------*/
/* procedure INTF-200.I    V.1.3    */
/* Controle de la ligne interface   */
/*----------------------------------*/

/*--------------------------------*/
/* DEAL Informatique ISA - JPL    */
/*                                */
/* ecrite  le 04 10 1995          */
/* revisee le                     */
/*--------------------------------*/

/*------------------------------------------------------------------*/
/*     Debut des definitions variables et ecrans                    */
/*                   Def ET FRAME                                */
/*------------------------------------------------------------------*/

procedure intf-200 :

  Def var i-deci-cpt as int no-undo . 
  Def var i-deci-pie as int no-undo . 
  Def var i-deci-ini as int no-undo . 

  Def variable a as int no-undo .
  Def variable tva as dec decimals 3 no-undo.
  Def var ana-typtab as char no-undo.
  Def var ana-prefix as char no-undo. 

  Def var xxx-operat as char no-undo .  
  Def var x as int no-undo.

  Def var mai-ana as int no-undo . 
  Def var mai-cou as int no-undo . 

  Def var test-segment as char no-undo.
  Def var test-socetb  as char no-undo.
  Def var pas-de-nul   as logical no-undo init no .
  Def var ty-sta  like mvtint.ty-stat no-undo.
  Def var mt-stat like mvtint.mt-stat no-undo.

  Def var type-classe   as char no-undo.
  Def var type-tva      as char no-undo.
  Def var type-vte      as char no-undo.
  Def var type-cg-refus as char extent 10 no-undo.
  Def var ff as int no-undo . 
  Def var f-codsoc as char no-undo . 
  Def var f-etabli as char no-undo . 
  Def var lib-mess-err as char no-undo.
  def var regs-cptgen  as char no-undo . 
  lib-mess-err = messprog.libel1[10]. /* Err. Mode reglement */
  def buffer tax-tables for tables.
  def buffer tcg-tables for tables.

  if top-bat = 1 then status input off.

  /*--------------------------------------------------------------------*/
  /*                          Traitement                                */
  /*--------------------------------------------------------------------*/
  /* recherche de l'enregistrement message */

  if mouchard = "O" then do : Message "intf-200.p". If Zone-Charg[9] <> "BATCH" Then Readkey  pause tp-pause. end.

  /* find parint where recid ( parint ) = int-recid no-lock no-error. */


  /*---- premiere ligne du document -----*/
  If debdoc = 1   and (type-trt < 2 or type-trt = 8)
  then do transaction :
       if mouchard <> "V" 
       then do:   
            /* Insertion du document */
            { intf-doc.i }
          release docint .
       end.     
       assign Doc-dtcpt = 0
              Doc-Ctcpt = 0
              Int-chrono = ""
              debdoc = 0
              Ant-docume = Int-docume
              Ant-debdoc = Int-debdoc.
  end.

  /*---------------------------------------------*/
  /* + 1 dans le chrono document                 */
  if type-trt <> 9 then int-chrono = string ( decimal ( int-chrono ) + 1 , "99999" ).

  /*--------------------------------*/
  /*    Controle des anomalies      */
  /*--------------------------------*/
  assign int-err = 0 int-fat = 0 .

  /*--------------*/
  /* test societe */
  /*--------------*/

  if Int-Codsoc <> Ant-Codsoc
  then do :
       Find Tables where tables.codsoc = "  "
                     and tables.etabli = "   "
                     and tables.typtab = "SOC"
                     and tables.prefix = "SOCIETE"
                     and tables.codtab = INT-codsoc
       no-lock no-error.

       If not available tables  or int-codsoc = "00"
       then assign int-fat    = int-fat + 1
                   errdon[01] = int-codsoc
                   erreur[01] = 1.
       Else assign Ant-Codsoc  = Int-codsoc
                   codsoc-soc  = Int-codsoc
                   ant-codetb  = ""
                   ant-execpt  = ""
                   ant-percpt  = ""
                   ant-journal = "" .
  End.
  /* pour ne le faire qu'une seule fois */ 
  assign regs-soc = codsoc-soc
         regs-fileacc = "CPTGEN".
  { regs-rec.i }
  regs-cptgen = regs-soc. 


  /*--------------------*/
  /* test etablissement */
  /*--------------------*/
  Find Tables where tables.codsoc = codsoc-soc
                and tables.etabli = " "
                and tables.typtab = "ETA"
                and tables.prefix = "ETABLIS"
                and tables.codtab = Int-codetb
  no-lock no-error.

  If not available tables
  or ( tables.tecnic [10] <> "" and
       tables.tecnic [10] < int-execpt   )
  then assign int-fat    = int-fat + 1
              errdon[02] = int-codetb
              erreur[02] = 1 .



  assign Ant-codetb = Int-codetb ant-journal = "".


  /*-------------------------------*/
  /* test etablissement recepteur */
  /*-----------------------------*/
  If ant-recetb <> Int-recetb  and int-recetb <> ""
  Then do :

       Find Tables where tables.codsoc = codsoc-soc
                     and tables.etabli = " "
                     and tables.typtab = "ETA"
                     and tables.prefix = "ETABLIS"
                     and tables.codtab = Int-recetb
       no-lock no-error.

       If not available tables then assign int-fat    = int-fat + 1
                                           errdon[02] = int-recetb
                                           erreur[02] = 1 .                                 
                               Else assign Ant-recetb = Int-recetb ant-journal = "".
  End.

  if type-trt <> 8 then do :  /* lolo le 02/04/01 */
      /*------------------------------*/
      /* test de l'exercice comptable */
      /*------------------------------*/
      Find Tables where tables.codsoc = codsoc-soc
                    and tables.etabli = "   "
                    and tables.typtab = "PAR"
                    and tables.prefix = "EXERCICE"
                    and tables.codtab = Int-execpt
      no-lock no-error.
      /* test si exercice inexistant ou cloture */
      If not available tables   or tables.libel1[ 1 ] = "2"
      then do :
           assign int-fat    = int-fat + 1
                  errdon[03] = int-execpt
                  erreur[03] = 1 .

           if mouchard = "O"
           then do :
                Message messprog.mes [13] int-execpt .
                If Zone-Charg[9] <> "BATCH" Then Readkey  pause tp-pause.
           end.
      end.
      Else do :
           /* test de l'enregistrement document               */
           Find docint where recid ( docint ) = do-recid
           no-lock  no-error.
           if available docint and docint.execpt <> int-execpt
           then do :
                assign int-fat    = int-fat + 1
                       errdon[03] = int-execpt
                       erreur[03] = 1 .

                if mouchard = "O"
                then do :
                     Message messprog.mes [13] int-execpt  docint.execpt .
                     If Zone-Charg[9] <> "BATCH" Then Readkey  pause tp-pause.
                end.
           end. /* available docint */ 

           assign Ant-execpt  = Int-execpt
                  nbrper      = tables.nombre [ 1 ]   /* nombre de periodes autorisees */
                  multi-dev   = tables.nombre [ 2 ]   /* 0 = Multi devises 1 = Pas de devises */
                  tr-exe      = tables.nombre [ 5 ]   /* Indice Solde    temps reel */
                  top-cloture = tables.libel  [ 1 ]   /* Exercice cloture */
                  devcpt      = tables.libel1 [ 2 ].  


           Find Tables where tables.codsoc = "  "
                         and tables.etabli = "   "
                         and tables.typtab = "DEV"
                         and tables.prefix = "DEVISE"
                         and tables.codtab = devcpt
           no-lock no-error.
           if available tables then deci-devcpt = tables.nombre[ 2 ] .  /* nombre de decimales de la devise */               
                               else  deci-devcpt = 3.


           /*------------------*/
           /* controle periode */
           /*------------------*/
            If ( integer ( Int-percpt ) ) <= 0      
            or ( integer ( Int-percpt ) ) > tr-exe
            then assign int-fat    = int-fat + 1
                        erreur[04] = 1
                        errdon[04] = "(1)" + int-execpt + " " + int-percpt.   
            Else do: 

                 /* est ce une periode de simu */ 
                 if ( integer ( int-percpt ) ) > nbrper and 
                    ( integer ( int-percpt ) ) < tr-exe 
                 then  do :
                       find tables where tables.codsoc = ""           
                                     and tables.etabli = ""           
                                     and tables.typtab = "SPE"        
                                     and tables.prefix = "SIM-percpt" 
                                     and tables.codtab = int-operat
                       no-lock no-error.
                       if available tables 
                       then find tables where tables.codsoc = int-codsoc      
                                          and tables.etabli = ""              
                                          and tables.typtab = "PAR"           
                                          and tables.prefix = "PERIODE"       
                                          and tables.codtab = int-execpt + int-percpt + int-percpt
                       no-lock no-error.
                       if not available tables then assign int-fat    = int-fat + 1
                                                           erreur[04] = 1
                                                           errdon[04] = "(2)" + int-execpt + " " + int-percpt.

                 end. 

                 assign Int-percpt = string ( ( integer ( Int-percpt ) ) , "999" ) /* Ajouter les pre-zeros */
                        Ant-Percpt = Int-percpt.




                 /* test de l'enregistrement document               */
                 Find docint where recid ( docint ) = do-recid
                 no-lock  no-error.
                 if available docint 
                 then do :
                      /* controle par rapport aux zones de cle de DOCINT et du mouvement en cours de creation */
                      if docint.codsoc  <> int-codsoc    then assign errdon[01] = "-->"  + docint.codsoc + " <> " + int-codsoc
                                                                     erreur[01] = 1
                                                                     int-fat    = int-fat + 1.
                      if docint.codetb  <> int-codetb    then assign errdon[02] = "-->"  + docint.codetb + " <> " + int-codetb
                                                                     erreur[02] = 1
                                                                     int-fat    = int-fat + 1.
                      if docint.execpt  <> int-execpt    then assign errdon[03] = "-->"  + docint.execpt + " <> " + int-execpt
                                                                     erreur[03] = 1
                                                                     int-fat    = int-fat + 1.
                      if docint.journal <> int-journal   then assign errdon[05] = "-->"  + docint.journal + " <> " + int-journal
                                                                     erreur[05] = 1
                                                                     int-fat    = int-fat + 1.
                      if docint.percpt  <> int-percpt then assign    int-fat    = int-fat + 1
                                                                     errdon[04] = "(3)" + int-percpt
                                                                     erreur[04] = 1.

                      if mouchard = "O"
                      then do :
                           Message messprog.mes [13] int-execpt int-percpt docint.execpt docint.percpt .
                           If Zone-Charg[9] <> "BATCH" Then Readkey  pause tp-pause.
                      end.
                 End.  /* available docint */ 
            end. /* test tr-exe */ 
      end. /* available tables exercice  */ 
  end. /* if type-trt <> 8 */


  /*------------------*/
  /* controle journal */
  /*------------------*/ 
  If Ant-journal <> Int-journal or Int-journal = ""
  Then do :

       Find Tables where tables.codsoc = "  "
                     and tables.etabli = "   "
                     and tables.typtab = "JOU"
                     and tables.prefix = "JOURNAL"
                     and tables.codtab = Int-journal
       no-lock no-error.
       If not available tables
       then assign int-fat    = int-fat + 1
                   ant-ctrpie = 1 
                   erreur[05] = 1
                   errdon[05] = int-journal.
       Else If type-trt <> 8 
       then do :

            assign xxx-operat  = operat 
                   codsoc      = int-codsoc  
                   etabli      = int-codetb 
                   operat      = int-operat  
                   Ant-journal = Int-journal 
                   ant-ctrpie  = tables.nombre[3] .

            run out-jnl ( input int-execpt ,
                          input int-percpt ,
                          input int-journal + "_" + int-devini ).
            operat = xxx-operat .                
            if libel <> "$OK$"  
            then assign int-fat    = int-fat + 1
                        erreur[18] = 1
                        errdon[18] = libel 
                        ant-execpt = ""  ant-percpt = "" ant-journal = "".

       End.
  End.

/* REM 20/09/2002 --> deplace plus bas ( apres recup des donnes de la fiche auxili )
  /*--------------------------------------------------------------------------------------*/
  /*- Autorisation CG/JOURNAUX -*/
  /*--------------------------------------------------------------------------------------*/
*/

  /*----------------------------*/
  /* controle du type de piece  */
  /*----------------------------*/ 
  If Int-Typie <> Ant-typie
  Then do :
       Find Tables where tables.codsoc = "  "
                     and tables.etabli = "   "
                     and tables.typtab = "PIE"
                     and tables.prefix = "TYPE-PIECE"
                     and tables.codtab = Int-typie 
       no-lock no-error.
       If not available tables
       then assign erreur[06] = 1
                   errdon[06] = int-typie
                   int-fat    = int-fat + 1.
       else Ant-typie = Int-typie.
  End.

/* REM 20/09/2002 --> deplace plus bas ( apres recup des donnes de la fiche auxili )
  /* controle echeance */ 
*/


  /*run intf-208.  */

  /*--------------------------------------------------------------------*/
  /*                          Traitement                                */
  /*--------------------------------------------------------------------*/

  /* recherche de l'enregistrement message */ 
  { acc-mess.i "int-erreur" }

  if int-devcpt <> ""
  then do :
       find tables where tables.codsoc = ""          
                     and tables.etabli = ""          
                     and tables.typtab = "DEV"       
                     and tables.prefix = "DEVISE"    
                     and tables.codtab = int-devcpt
       no-lock no-error.
       if available tables
       then assign i-deci-cpt     = tables.nombre[2]   
                   int-dt-devcpt  = round ( int-dt-devcpt , int (tables.nombre [ 2 ])   )
                   int-ct-devcpt  = round ( int-ct-devcpt , int (tables.nombre [ 2 ])   )
                   int-ht-taxecpt = round ( int-ht-taxecpt ,int (tables.nombre [ 2 ])   )
                   .
  End.


  if int-devpie <> ""
  then do :
       find tables where tables.codsoc = ""        
                     and tables.etabli = ""          
                     and tables.typtab = "DEV"       
                     and tables.prefix = "DEVISE"    
                     and tables.codtab = int-devpie
      no-lock no-error.
      if available tables
      then assign i-deci-pie     = tables.nombre[2]   
                  int-dt-devpie  = round ( int-dt-devpie , int (tables.nombre [ 2 ])   )
                  int-ct-devpie  = round ( int-ct-devpie , int (tables.nombre [ 2 ])   )
                  int-ht-taxedev = round ( int-ht-taxedev ,int (tables.nombre [ 2 ])   )
                  .
  End.


  if int-devini <> ""
  then do :
       find tables where tables.codsoc = ""          
                     and tables.etabli = ""          
                     and tables.typtab = "DEV"       
                     and tables.prefix = "DEVISE"    
                     and tables.codtab = int-devini
       no-lock no-error.
       if available tables
       then assign i-deci-ini     = tables.nombre[2]      
                   int-dt-devini  = round ( int-dt-devini , int (tables.nombre [ 2 ])   )
                   int-ct-devini  = round ( int-ct-devini , int (tables.nombre [ 2 ])   )
                   int-ht-taxeini = round ( int-ht-taxeini ,int (tables.nombre [ 2 ])   )
                   .
  End.
  else assign int-devini     = int-devpie
              int-dt-devini  = int-dt-devpie
              int-ct-devini  = int-ct-devpie
              int-ht-taxeini = int-ht-taxedev.

  /* les poireaux ne passeront pas . on recalcule les taux de devises */ 
  If int-dt-devini <> 0 and int-dt-devpie <> 0
  then int-tx-devini = round ( int-dt-devpie / int-dt-devini , 6 ) .

  If int-ct-devini <> 0 and int-ct-devpie <> 0
  then int-tx-devpie = round ( int-ct-devpie / int-ct-devini , 6 ) .


  If int-dt-devcpt <> 0 and int-dt-devpie <> 0
  then int-tx-devpie = round ( int-dt-devcpt / int-dt-devpie , 6 ) .

  If int-ct-devcpt <> 0 and int-ct-devpie <> 0
  then int-tx-devpie = round ( int-ct-devcpt / int-ct-devpie , 6 ) .



  if mouchard = "O"
  then do :
       display
              "DEVINI"
              int-devini
              int-tx-devini   format "zz9.99999"
              int-dt-devini   format "zzzzzzzzzzz9.999-"
              int-ct-devini   format "zzzzzzzzzzz9.999-"
              int-ht-taxeini  format "zzzzzzzzzzz9.999-"  skip


              "DEVPIE"
              int-devpie
              int-tx-devpie   format "zz9.99999"
              int-dt-devpie   format "zzzzzzzzzzz9.999-"
              int-ct-devpie   format "zzzzzzzzzzz9.999-"
              int-ht-taxedev  format "zzzzzzzzzzz9.999-"  skip


              "DEVCPT"
              int-devcpt
                                     "  1.00000"
              int-dt-devcpt   format "zzzzzzzzzzz9.999-"
              int-ct-devcpt   format "zzzzzzzzzzz9.999-"
              int-ht-taxecpt  format "zzzzzzzzzzz9.999-"

              with frame zoa row 2 centered  with 2 col overlay {v6frame.i}
              with title messprog.mes [5].

              If Zone-Charg[9] <> "BATCH" Then Readkey  pause tp-pause.
  End.


  /* run intf-210. */
  /*----------------------------------*/
  /* procedure INTF-210.I   V.1.3     */
  /* Controle de la ligne interface   */
  /*----------------------------------*/

  /*----------------------------------------------------*/
  /* acces tables parametrage taxe selon pcg           */


  assign type-classe       = "4"
         type-tva          = "44"
         type-vte          = "7"
         type-cg-refus [1] = "44"
         type-cg-refus [2] = "409"
         type-cg-refus [3] = "419"
         .



  Find tables where tables.codsoc = regs-cptgen     
                and tables.etabli = ""           
                and tables.typtab = "TVA"        
                and tables.prefix = "RACINE-CG"  
                and tables.codtab = ""
  no-lock no-error.
  if available tables
  then assign type-classe       = tables.libel1[1]
              type-tva          = tables.libel1[2]
              type-vte          = tables.libel1[3]
              type-cg-refus [1] = tables.tecnic[1]
              type-cg-refus [2] = tables.tecnic[2]
              type-cg-refus [3] = tables.tecnic[3]
              .

  if top-bat = 1 then status input off.


  /*--------------------------------------------------------------------*/
  /*                          Traitement                                */
  /*--------------------------------------------------------------------*/

  /* recherche de l'enregistrement message */

  { acc-mess.i "intf-210" }                                           

  /*----------------------------*/
  /* Controle du compte general */
  /*----------------------------*/
  if Int-codgen > "" /* lbo 20/09/2002 */
  then do:

       Find cptgen where cptgen.codsoc = regs-cptgen
                     and cptgen.nocpte = Int-codgen
       no-lock no-error. 
       If not available cptgen
       then do :
            assign int-fat    = int-fat + 1
                   erreur[10] = 1
                   errdon[10] = int-codgen     . 
            if int-intro-bug = 0 then int-codgen = "".
       End.

       If available cptgen and cptgen.titre = "T"
       then do :
            assign int-fat    = int-fat + 1
                   erreur[10] = 1
                   errdon[10] = int-codgen.
            if int-intro-bug = 0 then int-codgen = "".
       End.

  end.

  /* REM 20/09/2002
  if available cptgen  and cptgen.titre <> "T" 
  then do : */
       Ant-codgen = int-codgen.
       /*----------------------------*/
       /* controle de l'auxiliaire   */
       /*----------------------------*/
       if   available cptgen and cptgen.typcol <> "O" then Assign int-typaux = "" int-codaux = "". /* 11/02/2003 */

       if ( available cptgen and cptgen.typcol = "O" and type-trt < 9 ) or ( int-codaux > "" and type-trt < 9 )
       then do :

            /* test acces au tiers sans le type auxiliaire par l'index tiers */
            if int-typaux = "" and int-codaux <> ""
            then do :
                 assign regs-soc     = int-codsoc  /* on prend le code regroupement societe */
                        regs-fileacc = "AUXILI" .
                 { regs-rec.i }

                 { "cadr-spe.i" "int-codaux" }


                 if mouchard = "O"
                 then do :
                      Message messprog.mes [1] " "
                      int-codsoc regs-soc int-codaux. If Zone-Charg[9] <> "BATCH" Then Readkey  pause tp-pause.
                 end.

                 find auxili use-index tiers where auxili.codsoc = regs-soc  
                                               and auxili.codaux = int-codaux
                 no-lock no-error.            
                 if available auxili then int-typaux = auxili.typaux.
            end.


            If Int-typaux <> "" and Ant-typaux <> Int-typaux
            Then do :
                 Find Tables where tables.codsoc = ""
                               and tables.etabli = ""
                               and tables.typtab = "AUX"
                               and tables.prefix = "TYPE-AUXIL"
                               and tables.codtab = Int-Typaux
                 no-lock no-error.
                 If not available tables
                 then assign int-fat    = int-fat + 1
                             erreur[11] = 1
                             errdon[11] = int-typaux.
            end.
            Ant-typaux = Int-typaux.
            if mouchard = "O" 
            then do :
                 Message messprog.mes [2] " " int-typaux int-codaux. 
                 If Zone-Charg[9] <> "BATCH" Then Readkey  pause tp-pause.
            end.

            /* acces au tiers */
            /* on prend le type auxiliaire du document si inexi dans ce mvt */
            /* on prend le type auxiliaire du document si inexi dans ce mvt */
            if available parint then find tables where tables.codsoc = "" 
                                                   and tables.etabli = "" 
                                                   and tables.typtab = "SPE" 
                                                   and tables.prefix = "INTF-AUX" 
                                                   and tables.codtab = parint.evenement 
                                     no-lock no-error .
                                     if not available tables and int-typaux = "" then int-typaux = doc-typaux.
                                     if not available tables and int-codaux = "" then int-codaux = doc-codaux.

            regs-soc = codsoc-soc.
            regs-fileacc = "AUXILI" + int-typaux.
            { regs-rec.i }
            assign doc-typaux = int-typaux
                   doc-codaux = int-codaux.

            /* cadrage du code auxiliaire */
            { "cadr-aux.i" "codsoc-soc" "int-typaux" "int-codaux" }

            If mouchard = "O"
            then do :
                 Message messprog.mes [3] " " codsoc-soc regs-soc regs-fileacc int-typaux int-codaux.
                 If Zone-Charg[9] <> "BATCH" Then Readkey  pause tp-pause.
            End.

            Find Auxili where auxili.codsoc = regs-soc      
                          and auxili.Typaux = Int-Typaux    
                          and auxili.codaux = Int-Codaux
            no-lock no-error.
            If not available Auxili 
            then do:    
                 If type-trt <> 1 then assign int-fat    = int-fat + 1
                                              erreur[12] = 1
                                              errdon[12] = int-codaux.
                 If type-trt = 8 then errdon[12] = errdon[12] + " " + int-adres[ 1 ].
            End.

            if type-trt = 1
            then do :
                 if not available auxili
                 then do:
                      If mouchard = "O"
                      then do :
                           Message "creation tiers mini avec " regs-soc int-typaux int-codaux int-adres [ 1 ].
                           If Zone-Charg[9] <> "BATCH" Then Readkey  pause tp-pause.
                      End.
                      if available parint then run intf-514.p. /* creation d'une mini fiche auxiliaire + fiche imo */ 
                 end.

                 run intf-515.p. /* Creation Tabdom */

                 Find Auxili where auxili.codsoc = regs-soc      
                               and auxili.Typaux = Int-Typaux    
                               and auxili.codaux = Int-Codaux    
                 no-lock no-error.

            end.


            if available auxili
            then do :

                 /* test si blocage auxiliaire */
                 find tabdom where tabdom.codsoc = regs-soc      
                               and tabdom.etabli = ""            
                               and tabdom.typtab = "#BL"         
                               and tabdom.prefix = int-typaux    
                               and tabdom.codtab = int-codaux
                 no-lock no-error.
                 if available tabdom and int-execpt + int-percpt 
                 > string ( tabdom.libel1[1] , "XXXX" ) + string ( tabdom.libel1[2] , "xxxx" )
                 then do:
                    /* recherche si autorisation pour ce journal */                     
                     FIND tables  WHERE tables.codsoc = ""
                                    AND tables.etabli = ""
                                    AND tables.typtab = int-typaux
                                    AND tables.prefix = int-codaux
                                    AND tables.codtab = int-journal
                     NO-LOCK NO-ERROR.
                     IF NOT AVAILABLE tables
                     THEN FIND tables WHERE tables.codsoc = regs-soc
                                        AND tables.etabli = ""
                                        AND tables.typtab = int-typaux
                                        AND tables.prefix = int-codaux
                                        AND tables.codtab = int-journal
                     NO-LOCK NO-ERROR.
                     IF NOT AVAILABLE tables
                     THEN assign int-fat    = int-fat + 1
                                 erreur[12] = 1
                                 errdon[12] = int-typaux + " " + int-codaux + " - bloque au "
                                              + tabdom.libel1[1] + tabdom.libel1[2].
                 END.
                 if auxili.fam <> ""
                 then do:
                      Find Tables where tables.codsoc = "   "
                                    and tables.etabli = "   "
                                    and tables.typtab = "AUX"
                                    and tables.prefix = "CODE-FAMIL"
                                    and tables.codtab = auxili.fam
                      no-lock no-wait no-error.
                      if available tables and tables.nombre[1]  = 2 
                      then do:
                          /* recherche si autorisation pour ce journal */                     
                         FIND tables WHERE tables.codsoc = ""
                                       AND tables.etabli = ""
                                       AND tables.typtab = int-typaux
                                       AND tables.prefix = int-codaux
                                       AND tables.codtab = auxili.fam + int-journal
                         NO-LOCK NO-ERROR.
                         IF NOT AVAILABLE tables
                         THEN FIND tables WHERE tables.codsoc = regs-soc
                                            AND tables.etabli = ""
                                            AND tables.typtab = int-typaux
                                            AND tables.prefix = int-codaux
                                            AND tables.codtab = auxili.fam + int-journal
                         NO-LOCK NO-ERROR.
                         IF NOT AVAILABLE tables
                         THEN assign int-fat    = int-fat + 1
                                  erreur[12] = 1
                                  errdon[12] = int-typaux + " " + int-codaux + " - bloque par "
                                             + " la famille " + auxili.fam .
                      END.
                 end.


                 If Int-adres [ 1 ] = ""
                 then Do a = 1 to 5 :
                         int-Adres [a] = auxili.Adres [a].
                 End.

                 auxili = int-codaux.

                 if int-domech <> ""
                 then do:
                      find tabdom where tabdom.codsoc = auxili.codsoc 
                                    and tabdom.etabli = ""         
                                    and tabdom.typtab = int-typaux 
                                    and tabdom.prefix = int-codaux 
                                    and tabdom.codtab = int-domech 
                      no-lock no-error .
                      if available tabdom 
                      then assign int-dom-banque = Tabdom.libel1 [ 1 ]
                                  int-dom-ville  = Tabdom.libel1 [ 2 ]
                                  int-dom-codban = Tabdom.libel1 [ 3 ]
                                  int-dom-codgui = Tabdom.libel1 [ 4 ]
                                  int-dom-compte = Tabdom.libel1 [ 5 ]
                                  int-dom-clerib = Tabdom.libel1 [ 6 ]
                                  int-banque     = Tabdom.libel1 [10 ] When int-banque = "".
                      else assign int-domech     = ""
                                  int-dom-banque = ""
                                  int-dom-ville  = ""
                                  int-dom-codban = ""
                                  int-dom-codgui = ""
                                  int-dom-compte = ""
                                  int-dom-clerib = ""
                               /* int-banque     = "" */
                                  .
                      if available tabdom and int-modreg = "" then int-modreg = tabdom.libel1[7] .
                 end. /* int-domech <> "" */



                 If int-modreg = "" then int-modreg = auxili.mreg.
                 If int-codgen = "" then int-codgen = auxili.cpt.

                 /* si mode reg = "" acces a la premiere dom */
                 if int-modreg = ""
                 then do :
                      regs-fileacc = "AUXILI" + auxili.typaux.
                      { regs-rec.i }

                      Find first Tabdom where tabdom.codsoc = REGS-SOC
                                          and Tabdom.etabli = "   "
                                          and Tabdom.typtab = auxili.Typaux
                                          and Tabdom.prefix = Auxili.codaux
                      no-lock  no-error.
                      if available tabdom then int-modreg = tabdom.libel1 [ 7 ].
                 end.

                 /* test si encaissement a tester */
                 if available parint and parint.encaissement <> ""
                 then do :
                      find tables where tables.codsoc   = ""        
                                    and tables.etabli   = ""        
                                    and tables.typtab   = "TAX"     
                                    and tables.prefix   = "TYP-TVA" 
                                    and tables.codtab   = auxili.ttva
                      no-lock no-error.
                      if available tables then top-encais = tables.nombre[1].
                                          else top-encais = 0.
                 end.

            end. /* available auxili */

            /* 20/09/2002 */
            /*----------------------------*/
            /* Controle du compte general */
            /*----------------------------*/
            if Ant-codgen = "" 
            then do:

                 Find cptgen where cptgen.codsoc = regs-cptgen
                               and cptgen.nocpte = Int-codgen
                 no-lock no-error. 
                 If not available cptgen
                 then do :
                      assign int-fat    = int-fat + 1
                             erreur[10] = 1
                             errdon[10] = int-codgen     . 
                      if int-intro-bug = 0 then int-codgen = "".
                 End.

                 If available cptgen and cptgen.titre = "T"
                 then do :
                      assign int-fat    = int-fat + 1
                             erreur[10] = 1
                             errdon[10] = int-codgen.
                      if int-intro-bug = 0 then int-codgen = "".
                 End.

            end.



       End. /* if available cptgen and cptgen.typcol = "O" and type-trt < 9 */

      /* REM 20/09/2002 */
      if available cptgen  and cptgen.titre <> "T" 
      then do :


       If available cptgen
       and cptgen.typcol = "O" and int-typaux = "" and type-trt < 9
       then assign int-fat    = int-fat + 1
                   erreur[13] = 1
                   errdon[13] = int-codgen.


       /*------------------*/
       /* controle taxe    */
       /*------------------*/

       if type-trt < 9
       then do :

            If available cptgen and cptgen.suitax <> "O" then Int-typtaxe = "".

            If available cptgen and cptgen.suitax = "O"
            then do :
                 Find Tables where tables.codsoc = "  "
                               and tables.etabli = "   "
                               and tables.typtab = "TAX"
                               and tables.prefix = "TYP-TAXE"
                               and tables.codtab = Int-Typtaxe
                 no-lock no-error.
                 If not available tables
                 then assign int-fat     = int-fat + 1
                             erreur[15]  = 1
                             errdon[15]  = int-typtaxe
                             int-typtaxe = "".
                 Else do :
                     find FIRST tax-tables where tax-tables.codsoc = regs-cptgen    
                                             and tax-tables.etabli = " "         
                                             and tax-tables.typtab = "TCG"      
                                             and tax-tables.prefix = int-codgen
                                             no-lock no-error .
                     IF AVAILABLE tax-tables 
                     THEN DO:
                         /* test si table controle avec cg */
                          Find  tcg-tables where tcg-tables.codsoc = regs-cptgen   and
                                                 tcg-tables.etabli = ""          and
                                                 tcg-tables.typtab = "TCG"       and
                                                 tcg-tables.prefix = int-codgen  and
                                                 tcg-tables.codtab = int-typtaxe
                         no-lock no-error.
                         if not available tcg-tables 
                         then do: 
                             assign int-fat     = int-fat + 1
                                    erreur[15]  = 1
                                    errdon[15]  = int-typtaxe
                                    int-typtaxe = "".                           
                         End.
                     End.
                     IF int-typtaxe <> "" 
                     THEN DO:
                         Ant-Typtaxe = Int-Typtaxe.
                         /* controle calcul ht ou tva */
                         if int-ht-taxeini = 0 and tables.nombre [ 1 ] <> 0
                         then do :
                              if int-dt-devini <> 0 then tva = int-dt-devini.
                              if int-ct-devini <> 0 then tva = int-ct-devini.

                              if int-codgen begins type-tva
                              then int-ht-taxeini  = round ( tva * 100 / tables.nombre[1] , i-deci-ini ).
                              else int-ht-taxeini  = round ( tva * tables.nombre[1] / 100 , i-deci-ini ).
                              /* calcul de la base comptable */
                              assign int-ht-taxedev = round ( int-ht-taxeini * int-tx-devini , i-deci-pie )
                                     int-ht-taxecpt = round ( int-ht-taxedev * int-tx-devpie , i-deci-cpt ).
                         end. /* fin du if int-ht */
                         if int-ht-taxedev = 0 and tables.nombre [ 1 ] <> 0
                         then do :
                              if int-dt-devpie <> 0 then tva = int-dt-devpie.
                              if int-ct-devpie <> 0 then tva = int-ct-devpie.

                              if int-codgen begins type-tva  
                              then int-ht-taxedev  = round ( tva * 100 / tables.nombre[1] , i-deci-pie ).
                              else int-ht-taxedev  = round ( tva * tables.nombre[1] / 100 , i-deci-pie ).
                              /* calcul de la base comptable */
                              int-ht-taxecpt = round ( int-ht-taxedev * int-tx-devpie , i-deci-cpt ).

                         end. /* fin du if int-ht */

                     END.
                 end. /* else do : */
            End. /* If available cptgen and cptgen.suitax = "O" */

            /*------------------*/
            /*  controle Qte    */
            /*------------------*/

            If available cptgen and cptgen.gesqte <> "O" then Int-code-Quant = "".

            If available cptgen and cptgen.gesqte = "O"
            then do :

                 Find Tables where tables.codsoc = "  "
                               and tables.etabli = "   "
                               and tables.typtab = "QTE"
                               and tables.prefix = "TYP-QUANT"
                               and tables.codtab = Int-Code-quant
                 no-lock no-error.
                 If not available tables
                 then assign erreur[16] = 1
                             int-fat    = int-fat + 1
                             errdon[16] =int-code-quant.
                 Else Ant-Code-quant = Int-Code-quant.

                 if mouchard = "O"
                 then do :
                      Message messprog.mes [4] " " int-code-quant.
                      If Zone-Charg[9] <> "BATCH" Then Readkey  pause tp-pause.
                 end.
            End.
       End . /* type-trt < 9 */

  End.  /* if available cptgen  and cptgen.titre <> "T" */ 


  /* 20/09/2002 */
  /********************************************************************************/
    /*--------------------------------------------------------------------------------------*/
  /*- Autorisation CG/JOURNAUX -*/
  /*--------------------------------------------------------------------------------------*/
  etabli = int-codetb .
  if int-recetb <> "" then etabli = int-recetb . 

  Find Tables Where Tables.Codsoc = int-Codsoc
                and Tables.Etabli = etabli 
                and Tables.Typtab = "JOU"
                and Tables.Prefix = "AUTCGJOU"
                and Tables.Codtab = int-Codgen
  no-lock no-error.
  If not available Tables 
  then Find Tables Where Tables.Codsoc = int-Codsoc
       		       and Tables.Etabli = ""
      		       and Tables.Typtab = "JOU"
      		       and Tables.Prefix = "AUTCGJOU"
      		       and Tables.Codtab = int-Codgen
  no-lock no-error.

  If available Tables 
  and lookup ( trim(int-journal) , trim(tables.libel1[ 1] ) + 
                                   trim(tables.libel1[ 2] ) +
                                   trim(tables.libel1[ 3] ) +
                                   trim(tables.libel1[ 4] ) +
                                   trim(tables.libel1[ 5] ) +
                                   trim(tables.libel1[ 6] ) +
                                   trim(tables.libel1[ 7] ) +
                                   trim(tables.libel1[ 8] ) +
                                   trim(tables.libel1[ 9] ) +
                                   trim(tables.libel1[10] ) +
                                   trim(tables.tecnic[ 1] ) + 
                                   trim(tables.tecnic[ 2] ) +
                                   trim(tables.tecnic[ 3] ) +
                                   trim(tables.tecnic[ 4] ) +
                                   trim(tables.tecnic[ 5] ) +
                                   trim(tables.tecnic[ 6] ) +
                                   trim(tables.tecnic[ 7] ) +
                                   trim(tables.tecnic[ 8] ) +
                                   trim(tables.tecnic[ 9] ) +
                                   trim(tables.tecnic[10] ) ) = 0

  then assign int-fat    = int-fat + 1
              erreur[18] = 1
              errdon[18] = "AUTCGJOU" .



    /* controle echeance */ 
  if int-echeance <> ? and lookup( "s-datech",this-procedure:internal-entries) <> 0 
  then do: 
       assign codtab = ""
              etabli = int-codetb . 

       if int-recetb <> "" then etabli = int-recetb. 
       libel = "BATCH,$ETB_" + etabli + "_" + int-modreg. 

       /* controle echeance */ 
       run s-datech ( input int-echeance , 
                      input int-datpie , 
                      input int-execpt , 
                      input int-percpt ,
                      input int-codgen , 
                      input libel,
                      output codtab )   .
       if codtab = "$rien$" 
       then assign int-fat    = int-fat + 1
                   erreur[18] = 1           
                   /* errdon[18] = messprog.libel1[10] + " " + string (int-echeance , "99/99/9999" ) */
                   errdon[18] = lib-mess-err + " " + string (int-echeance , "99/99/9999" ). 

  end. 



  /********************************************************************************/




  /* controle int-banque */
  if int-banque > ""
  then do:
       if lookup( "intf-520",this-procedure:internal-entries) <> 0 then run intf-520.
                                                                   else run intf-520.p.
       if libel = "$NON$" 
       then assign int-fat    = int-fat + 1
                   erreur[10] = 1          
                   errdon[10] = "Bq_" + int-banque. 
  end.

  /* controle unicite de la piece */  
  if ant-ctrpie  <>  1  and 
     int-piece   <> ""  and 
     ant-journal <> ""  and 
     (type-trt < 2 or type-trt = 8)
  then do: 
    run intf-201.
    if libel = "$NON$" 
    then assign int-fat    = int-fat + 1
                erreur[08] = 1 
                errdon[08] = int-piece . 
  end. 

  /* run intf-211.  */
  /*----------------------------------*/
  /* procedure INTF-211.I   V.1.3     */
  /* Controle de la ligne interface   */
  /*----------------------------------*/ 
  find tables where tables.codsoc = "" 
                and tables.etabli = "" 
                and tables.typtab = "SPE" 
                and tables.prefix = "INTF-ANA" 
                and tables.codtab = "CHARGE" 
  no-lock no-error .
  if available tables then pas-de-nul = yes.

  /*--------------------------------------------------------------------*/
  /*                          Traitement                                */
  /*--------------------------------------------------------------------*/
  if mouchard = "O" then do : message "intf-210.p". If Zone-Charg[9] <> "BATCH" Then Readkey  pause tp-pause.  end.


  /* find parint where recid ( parint ) = int-recid no-lock no-error. */

  /*----------------------------*/
  /* Controle du compte general */
  /*----------------------------*/
  Find cptgen where cptgen.codsoc = regs-cptgen
                and cptgen.nocpte = Int-codgen
  no-lock no-error.

  /*----------------------------*/
  /* controle devise piece      */
  /*----------------------------*/ 
  Find Tables where tables.codsoc = "  "
                and tables.etabli = "   "
                and tables.typtab = "DEV"
                and tables.prefix = "DEVISE"
                and tables.codtab = Int-devpie
  no-lock no-error.
  If not available tables or (multi-dev < 2 and tables.dattab[2] <> ? and int-datpie >= tables.dattab[2] ) 
  then do : 
       assign int-fat    = int-fat + 1
              erreur[14] = 1
              errdon[14] = int-devpie . 
       if int-intro-bug  = 0 then int-devpie = "".
  end. 
  Else Ant-Devpie = Int-Devpie.

  /*----------------------------*/
  /* controle devise initial    */
  /*----------------------------*/ 
  Find Tables where tables.codsoc = "  "
                and tables.etabli = "   "
                and tables.typtab = "DEV"
                and tables.prefix = "DEVISE"
                and tables.codtab = Int-devini
  no-lock no-error.
  If not available tables or ( tables.dattab[2] <> ? and int-datpie >= tables.dattab[2] ) 
  then do : 
       assign int-fat    = int-fat + 1
              erreur[14] = 1
              errdon[14] = int-devini . 
       if int-intro-bug = 0 then int-devini = "".
  end. 

  /* gestion bi-monnaie et mauvaise devise */ 
  if multi-dev = 2 and int-devpie <> bi-devetb 
  then assign int-fat    = int-fat + 1
              erreur[09] = 1
              errdon[09] = bi-devetb + "/" + int-devpie .



  /*----------------------------*/
  /* controle devise comptable  */
  /*----------------------------*/
  If Int-devcpt = "" then Int-devcpt = devcpt.

  If Int-Devcpt <> Ant-Devcpt
  Then do :
       Find Tables where tables.codsoc = "  "
                     and tables.etabli = "   "
                     and tables.typtab = "DEV"
                     and tables.prefix = "DEVISE"
                     and tables.codtab = Int-devcpt
       no-lock no-error.
       If not available tables
       then do : 
            if int-intro-bug = 0 then int-devcpt = "".
       end.     
       Else Ant-Devcpt = Int-Devcpt.
  End.


  /*----------------------------*/
  /* controle mode reglement    */
  /*----------------------------*/ 
  If int-modreg <> ""
  Then do :
       Find Tables where tables.codsoc = "  "
                     and tables.etabli = "   "
                     and tables.typtab = "REG"
                     and tables.prefix = "CODE-REGLM"
                     and tables.codtab = Int-Modreg
       no-lock no-error.
       If not available tables
       then do : 
            assign erreur[17] = 1
                   errdon[17] = int-modreg  . 
            if int-intro-bug  = 0 then int-modreg = "".
       end. 
       Else Ant-Modreg = Int-Modreg.
  End.


  if available parint and int-modreg = "" and parint.modreg-def <> ""
  then int-modreg = parint.modreg-def.

  /*----------------------------*/
  /* controle code famille      */
  /*----------------------------*/ 
  If int-fam <> ""
  Then do :
       Find Tables where tables.codsoc = "  "
                     and tables.etabli = "   "
                     and tables.typtab = "AUX"
                     and tables.prefix = "CODE-FAMIL"
                     and tables.codtab = Int-fam
       no-lock no-error.
       If not available tables
       then assign erreur[19] = 1
                   errdon[19] = int-fam     
                   int-fam    = "".
  End.


  /*----------------------------*/
  /* controle code pays         */
  /*----------------------------*/ 
  If Int-Pays <> Ant-PAys and int-pays <> ""
  Then do :
       Find Tables where tables.codsoc = "  "
                     and tables.etabli = "   "
                     and tables.typtab = "PAY"
                     and tables.prefix = "CODE-PAYS"
                     and tables.codtab = Int-pays
       no-lock no-error.
       If not available tables
       then assign erreur[19] = 1
                   errdon[19] = int-pays     
                   int-pays   = "".
       Else Ant-pays = Int-pays.
  End.



  if available cptgen
  then do :
      /*----------------------------*/
      /* controle analytique        */
      /*----------------------------*/

      If cptgen.suiana = "O"
      Then do :
             /***********************************************************************/ /* 12/99 */ 
            /* Recherche du type de controle a effectuer ( tables --> TYPE-COMPO ) */
           /***********************************************************************/ 
           Def var out-etabli        Like Mvtcpt.Codetb.
           Def var out-type-controle as int extent 10 init 0. 
           Def var out-p2            as char.
           Assign out-p2            = ""
                  out-type-controle = 0
                  out-etabli        = int-codetb
                  out-etabli        = int-recetb When int-recetb > "".
           run out-ana2 ( input  int-codsoc ,
                          input  int-codetb ,
                          input  out-etabli ,
                          input  int-codgen ,
                          output out-p2     ).                        
           Do a = 1 to num-entries ( out-p2 ).
              out-type-controle [ a ] = int(entry ( a , out-p2 )).
           End.
           /***********************************************************************/    
      End.


/*      If cptgen.suiana <> "O" and top-imo <> 1 then Int-ana-segment = "". */
      If cptgen.suiana = "O" or top-imo = 1
      Then do a = 1 to nbr-seg:

           /* test si prise segment valeur par defaut */
           if int-ana-segment [ a ] = "" then int-ana-segment [ a ] = seg-anadef [ a ].

           if Ant-ana-segment [ a ] <> Int-ana-segment [ a ] 
             and ( Int-ana-segment [ a ] <> "" and Int-ana-segment [ a ] <> "ZNUL" )
           Then do :
                codsoc = int-codsoc .
                if int-recetb = "" then etabli = int-codetb .
                                   else etabli = int-recetb .

                if soet-seg[ a ] = 1 then etabli = "".
                if soet-seg[ a ] = 0 then assign codsoc = ""
                                                 etabli = "".
                assign x          = 0  
                       num-seg    = string ( a , "99" )
                       ana-typtab = "ANA"
                       ana-prefix = "SEGMENT-" + num-seg .

                /* analytique avec des liens */ 
                if lien-seg [a]  <> 0 
                then assign ana-typtab = "L" + num-seg
                            ana-prefix = int-ana-segment[lien-seg[a]] .


                Find Tables where tables.codsoc = codsoc      
                              and tables.etabli = etabli      
                              and tables.typtab = ana-typtab  
                              and tables.prefix = ana-prefix  
                              and tables.codtab = Int-ana-segment [ a ]
                no-lock no-error.
                If not available tables
                    or (tables.dattab [1] <> ? and tables.dattab [1] > today) 

                    or (tables.dattab [2] <> ? and tables.dattab [2] < today) 

                    or (tables.dattab [3] <> ? and tables.dattab [4]  = ? and 
                        year(tables.dattab[3]) < int ( int-execpt ) )  

                    or (tables.dattab [3] <> ? and tables.dattab [4] <> ? and 
                        string(year(tables.dattab[3]) , "9999" ) +
                        string(year(tables.dattab[4]) , "999"  ) 
                        < int-execpt + int-percpt  )  
                then assign erreur[ a + 20 ] = 1
                            errdon[ a + 20 ] = int-ana-segment [ a ]
                            int-fat          = int-fat + 1.
                Else do:
                     if int-ana-segment[a] <> seg-anadef[a]
                     then do:
                          /* test si controle sur societe et etablissemnt */
                          test-segment = "".
                          if int-recetb = "" then test-socetb = string ( int-codsoc , "XXXX" ) 
                                                              + string ( int-codetb , "XXXX").
                          else test-socetb = string ( int-codsoc , "XXXX" ) 
                                           + string ( int-recetb , "XXXX").
                          do x = 1 to 9:
                             test-segment = test-segment + tables.tecnic [ x ].
                          end.

                          x = 0.
                          if test-segment <> ""
                          then do :
                               x = lookup ( test-socetb , test-segment ).
                               If x = 0
                               then do :
                                    test-socetb  = string ( int-codsoc , "XXXX" ).
                                    x = lookup ( test-socetb , test-segment ).
                               end.

                               if x = 0
                               then do :
                                    /* test avec version v1.2 reprise */
                                    test-segment = "".
                                    if int-recetb = "" 
                                    then test-socetb  = string ( int-codsoc , "XX" ) +
                                                        string ( int-codetb , "XXX").
                                    else test-socetb  = string ( int-codsoc , "XX" ) +
                                                        string ( int-recetb , "XXX").
                                    do x = 1 to 9 :
                                       test-segment = test-segment + tables.tecnic [ x ].
                                    end.
                                    x = 0.
                                    if test-segment <> ""
                                    then do :
                                         x = lookup ( test-socetb , test-segment ).
                                         If x = 0
                                         then do :
                                              test-socetb  = string ( int-codsoc, "XX" ).
                                              x = lookup ( test-socetb , test-segment ).
                                         end.
                                    end.
                               end. /* = 0 */

                               if x = 0
                               then do :
                                   assign erreur[ a + 20 ] = 1
                                          errdon[ a + 20 ] = int-ana-segment [ a ]  . 
                                   if int-intro-bug = 0 then int-ana-segment [ a ] = "".
                                   int-fat = int-fat + 1.
                               end.
                               else  Ant-ana-segment [ a ] = Int-ana-segment [ a ].

                          end.  /* test-segment  fiche 343 */ 

                          if int-spe-ana[a] <> "" 
                          then do : 
                               libel = ""  . 
                               run intf-ana(a) .
                               if libel = "$NEXT$"   then next . /* pas de test couple ou imputation*/
                          end.         

                          /* test si controle fige sur le cg 
                                  CAS I   = lecture par societe + etb 
                                  CAS II  =         par societe 
                                  CAS III =             regs-soc + etb 
                                  cas IV  =             regs-soc 
                                  cas V   =            

                          */ 
                          assign  f-codsoc = int-codsoc . 
                          if int-recetb <> "" then f-etabli = int-recetb . 
                                              else f-etabli = int-codetb . 

                          do ff = 1 to 5 : 
                            case ff : 
                              when 1 then . 
                              when 2 then f-etabli = "" . 
                              when 3 then do : 
                                          f-codsoc = regs-cptgen . 
                                          if int-recetb <> "" then f-etabli = int-recetb . 
                                                              else f-etabli = int-codetb . 
                                     end. 
                              when 4 then f-etabli = "" . 
                              when 5 then f-codsoc = "" . 
                            end case .

                            /* test si affectation figee societe + etablissement */
                            find first tables where tables.codsoc = f-codsoc  
                                                and tables.etabli = f-etabli  
                                                and tables.typtab = "A" + string ( a , "99" ) 
                                                and tables.prefix = int-codgen 
                                                and tables.codtab > ""
                            no-lock no-error .
                            if available tables 
                            then do : 
                                 find tables where tables.codsoc = f-codsoc  
                                               and tables.etabli = f-etabli  
                                               and tables.typtab = "A" + string ( a , "99" ) 
                                               and tables.prefix = int-codgen 
                                               and int-ana-segment [ a ] begins trim(tables.codtab) 
                                 no-lock no-error.             
                                 If not available tables then assign erreur[ a + 20 ] = 1
                                                                     errdon[ a + 20 ] = int-ana-segment [ a ]
                                                                     int-fat          = int-fat + 1.

                                 leave . 
                            end.
                          end. /* do ff */ 



                         /* type de controle : 4 ==> pas de controle de couple
                                               5 ==> impose par le compte est doit exister en couple */

                         out-p2 = "".
                         /* test couple je perturbe codsoc et etabli */ 
                         If out-type-controle [a] <> 4                   /* Pas de controle du couple */ /* 12/99 */
                         Then do x = 1 to num-entries(couple-inv[a]) : 
                                 mai-ana = int(entry(x,couple-inv[a])) .
                                 if mai-ana = 0 then next .
                                 mai-cou = a . 
                                 if mai-cou = 10 then mai-cou = 0 . /* cas du 10 */ 
                                 codsoc = int-codsoc .
                                 if int-recetb = "" then etabli = int-codetb .
                                                    else etabli = int-recetb .

                                 if soet-seg[ mai-ana ] = 1 then etabli = "".
                                 if soet-seg[ mai-ana ] = 0 then assign codsoc = ""
                                                                        etabli = "".
                                 /* voir unicite */                
                                 find tables where tables.codsoc = codsoc 
                                               and tables.etabli = etabli 
                                               and tables.typtab = "C" + string(mai-ana, "9" ) + 
                                                                         string(mai-cou, "9" ) 
                                               and tables.prefix = int-ana-segment[mai-ana] 
                                 no-lock no-error . 
                                 if available tables then int-ana-segment[a] = tables.codtab . 
                                 find tables where tables.codsoc = codsoc 
                                               and tables.etabli = etabli 
                                               and tables.typtab = "C" + string(mai-ana, "9" ) + 
                                                                         string(mai-cou, "9" ) 
                                               and tables.prefix = int-ana-segment[mai-ana] 
                                               and tables.codtab = int-ana-segment[a] 
                                 no-lock no-error . 
                                 If not available tables 
                                 then assign erreur[ a + 20 ] = 1
                                             errdon[ a + 20 ] = int-ana-segment [ a ]
                                             int-fat          = int-fat + 1 .  
                                 else Ant-ana-segment [ a ] = Int-ana-segment [ a ].
                         end. /* do x = 1 to  couple */ 

                     end. /* test-segment <> defaut */
                end. /* else do */ 
           End. /* ant */

           if pas-de-nul and int-ana-segment[a] = ""
           then assign erreur[ a + 20 ] = 1
                       errdon[ a + 20 ] = int-ana-segment [ a ]
                       int-fat          = int-fat + 1.
      End. /* suiana a = 1 to 10 */
      else Int-ana-segment = "". 

  End. /* cptgen */



  /* run intf-205. */
  /*----------------------------------*/
  /* procedure INTF-205.I   V.1.3     */
  /*----------------------------------*/


  /*--------------------------------------------------------------------*/
  /*                          Traitement                                */
  /*--------------------------------------------------------------------*/

  /* recherche de l'enregistrement message */

  { acc-mess.i "int-erreur" }

  /* find parint where recid ( parint ) = int-recid no-lock no-error. */


  /*----------------------------*/
  /* acces au    compte general */
  /*----------------------------*/
  If (type-trt < 2 or type-trt = 8)
  then Find cptgen where cptgen.codsoc = regs-cptgen
                     and cptgen.nocpte = Int-codgen
       no-lock no-error.

  /* test si comptabilisation */ 
  if (type-trt < 3 or type-trt = 8)
  then do :

       if mouchard = "O"
       then do :
            Message messprog.mes [14] regs-cptgen " "
            int-codgen . If Zone-Charg[9] <> "BATCH" Then Readkey  pause tp-pause.
       end.
       /*----------------------------*/
       /* controle montants          */
       /*----------------------------*/
       if int-dt-devcpt    =   0   and
          int-ct-devcpt    =   0   and
          int-dt-devpie    =   0   and
          int-ct-devpie    =   0   and
          int-dt-devini    =   0   and 
          int-ct-devini    =   0   and 
          int-quantite     =   0

       then do :

            /* test si montant a zero accepte */
            Find tables where tables.codsoc = ""           
                          and tables.etabli = ""           
                          and tables.typtab = "SPE"        
                          and tables.prefix = "S-MAJBAS"   
                          and tables.codtab = "OK-ZERO"
            no-lock no-error.                  
            if not available tables
            then do :  
                 if mouchard = "O"
                 then do :
                      Message color bright messprog.mes [15]. If Zone-Charg[9] <> "BATCH" Then Readkey  pause tp-pause.
                 end.
                 assign int-fat    = int-fat + 1
                        erreur[07] = 1.
            End. 
       end.

       /* test sens ecritre / au compte */                                                             
       if (available cptgen and (cptgen.sens = "11" or cptgen.sens = "13") and int-ct-devcpt <> 0) or
          (available cptgen and (cptgen.sens = "21" or cptgen.sens = "23") and int-dt-devcpt <> 0) or
          (available cptgen and (cptgen.sens = "13" OR cptgen.sens = "12") and int-dt-devcpt < 0)  or
          (available cptgen and (cptgen.sens = "23" OR cptgen.sens = "22") and int-ct-devcpt < 0)
       then assign int-fat    = int-fat + 1
                   erreur[07] = 1
                   errdon[07] = "SENS" .


       /*------------------------------*/
       /* controle montants  sans sens*/
       /*----------------------------*/
       if  int-dt-devcpt <> 0   
       and int-ct-devcpt <> 0
       then do :
            if mouchard = "O"
            then do :
                 Message color bright messprog.mes [16]. If Zone-Charg[9] <> "BATCH" Then Readkey  pause tp-pause.
            end.
            assign int-fat    = int-fat + 1
                   erreur[20] = 1.
       end.


       /*-------------------------------------------------------------------*/
       /* Cas du numero de document force, verif si existe deja dans doccpt */
       /*-------------------------------------------------------------------*/
       if intPAR-DOCUME > ""
       then do:       
            find doccpt where doccpt.codsoc = Ant-codsoc
                          and doccpt.codetb = Ant-codetb
                          and doccpt.docume = intPAR-DOCUME 
            no-lock no-error.
            if available doccpt
            then assign erreur[ 08 ] = 1                     
                        errdon[ 08 ] = "Doccpt existe : " + intPAR-DOCUME
                        int-fat      = int-fat + 1.          
       end.
       /*-------------------------------------------------------------------*/

      /*----------------------------*/
      /* controle code BAP         */
      /*----------------------------*/ 
      If Int-bap-code <> ""
      Then do :
           Find Tables where tables.codsoc = "  "
                         and tables.etabli = "   "
                         and tables.typtab = "BAP"
                         and tables.prefix = "CODE-BAP"
                         and tables.codtab = Int-bap-code
           no-lock no-error.
           If not available tables
           then assign erreur[19]   = 1
                       errdon[19]   = Int-bap-code     
                       Int-bap-code = "".
      End.

      if available cptgen and substring( cptgen.geslet , 1 , 1 ) = "I" 
      then run intf-ias.p .
      else int-ias = "" .

       /*- Controle specifique -*/
       if lookup( "intf-sp0",this-procedure:internal-entries) <> 0 then run intf-sp0.
                                                                   /* else run intf-sp0.p. */


       /*---------------------------------------*/
       /*    FIn du Controle des anomalies      */
       /*    Dans int-fat le nombre d'erreurs   */
       /*    sur cette ligne                    */
       /*---------------------------------------*/


       /*----------------------------------------------------------------*/
       /* test si on continue = pas d'erreur fatale  int-fat = 0        */
       /*--------------------------------------------------------------*/
       if mouchard = "O" 
       then do : 
            Message messprog.mes [17] " " int-fat .
            If Zone-Charg[9] <> "BATCH" Then Readkey  pause tp-pause .
       end.

       If int-fat  <> 0
       then do transaction :
               tot-err = tot-err + 1.
               if mouchard <> "V" 
               then do: 
                    { intf-log.i }
               end.    
       end.


       /*--------------------------------------------------*/
       /* creation mouvement et document dans l'interface */
       /*------------------------------------------------*/
       if (type-trt < 2 or type-trt = 8)
       then do :
            if mouchard <> "V" 
            then do: 
                 if int-fat <> 0
                 then do transaction :
                      if int-intro-bug = 1 then run intf-2a9. /* creat mvtint BUG*/
                                           else return.
                 End.
                 else do transaction : 
                         if available parint then run intf-209( parint.centralisable ) . /* creat mvtint OK */ 
                     else if available intspe then run intf-209( intspe.centralisable ) . /* creat mvtint OK */

                     If type-trt = 8 then docint.valid-top = "E" . /* Engagements */
                 end.         
                 release docint. 
                 release mvtint. 
            end. 
            nb-ecrit = nb-ecrit + 1.

            /* totalisation pour controle document des lignes comptabilisees */
            assign int-adres = ""
                   DOC-DTCPT = DOC-DTCPT + INT-DT-DEVCPT
                   DOC-CTCPT = DOC-CTCPT + INT-CT-DEVCPT.
       End.

  End.


  /* test si Bon a payer */
  if type-trt = 2
  then do transaction :

          assign erreur  = 0
                 errdon  = ""
                 int-fat = 0.

          if lookup( "intf-400",this-procedure:internal-entries) <> 0 then run intf-400.
                                                                      else run intf-400.p. 

          assign erreur  = 0
                 errdon  = ""
                 int-fat = 0.
          release prebap . 
  end.


  /* test si creation tiers */
  if type-trt    = 9
  then do transaction :
          /* effacement des erreurs inutiles pour la gestion auxiliaire */
          do a = 2   to 10 : erreur [ a] = 0. errdon [a] = "". end.
          do a = 20  to 30 : erreur [ a] = 0. errdon [a] = "". end.
          assign erreur [13] = 0 errdon [13] = ""
                 erreur [16] = 0 errdon [16] = "" int-fat = 0.

          if lookup( "intf-500",this-procedure:internal-entries) <> 0 then run intf-500.
                                                                      else run intf-500.p. 
          release auxili .
          release tabdom . 

          assign erreur  = 0
                 errdon  = ""
                 int-fat = 0 .
  end.

End procedure.

/* Recherche du type de compo/controle sur les segments */ /* 12/99 */
{ out-ana2.i }
procedure intf-201 : 
    {intf-201.i} 
end procedure . 

