/* export tables ana*/
OUTPUT TO tables.d .


FOR EACH tables WHERE typtab = "ana" :

    EXPORT tables .
END.


/* export tables spe*/
OUTPUT TO tablesspe.d .


FOR EACH tables WHERE typtab = "SPE" :

    EXPORT tables .
END.
 /* dossier d-dict d-dict */
OUTPUT TO d-dossier.d .

FOR EACH d-dossier  :

    EXPORT d-dossier .
END.

OUTPUT TO d-dict.d .

FOR EACH d-dict WHERE ident = "620" :

    EXPORT d-dict .
END.


OUTPUT TO d-dictab.d .

FOR EACH d-dictab WHERE ident = "620" :

    EXPORT d-dictab .


END.

/* analaxe */

OUTPUT TO analaxe.d .

FOR EACH analaxe  :

    EXPORT analaxe .
END.




