/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 620/elopro/inc/x620-b00.f                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 10367 02-09-04!New!lbo            !TSPE38 - Flux intragroupe chez Sade.                                  !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*- x620-b00.f : Flux intragroupe [ FRAME ] -*/

Def {1} shared frame Fr-codvue.
form code-vue 
     with frame fr-codvue row 2 col 6 no-label overlay
     {v6frame.i} with title Messprog.Libel1[1].

Def {1} shared frame Fr-libvue.
form lib-vue
     with frame fr-libvue row 2 col 10 no-label overlay
     {v6frame.i} with title Messprog.Libel1[2].

Def {1} shared frame Fr-per.
form s-exe s-permax at 8
     with frame fr-per row 2 col 45  width 16 no-label overlay
     {v6frame.i} with title Messprog.Libel1[3].


Def {1} shared frame Fr-maq.
form x620-vue-maq  
     with frame fr-maq row 16 col 30 width 15 no-label overlay
     {v6frame.i} with title Messprog.Libel1[8] .

Def {1} shared frame Fr-spo.
form ma-impri format "x(12)" 
      with frame fr-spo row 16 col 50 width 15 overlay 
      { v6frame.i } no-label  with title Messprog.Libel1[7].



