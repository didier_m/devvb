/* V423ELO-FIC 93 Autorisation des typaux dans le plan cg : travailler en racine <EVO gd1 30/03/01> */
/*  specif client    */
/* Parametrage du plan comptable :
   Liste des types de tiers : racine des typaux (F,C,S) */
/*int           typaux                   vecteur */    
{1} =  lookup(  substr({2} , 1 , 1 )   , {3}  ) . /* Ne tester que le premier caractere du typaux */

