/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! deal/nathalie/src/ne-bonedi.p                                                               !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 10455 10-09-04!Evo!erk            !pour ne pas traiter les bons bloqu�s                                  !
!V52 10391 06-09-04!Evo!erk            !ne pas gerer le nombre d'exemplaire en vente                          !
!V52   520 25-08-04!Evo!del            !pouvoir editer le tableau + lettre declaration adr                    !
!V52 10069 29-07-04!Evo!del            !fiche tereos n� 210604-001 : rajout edition date ceh calculer si = ? p!
!V52  9809 09-07-04!Evo!erk            !modif zone teleph � editer                                            !
!V52  9806 09-07-04!Evo!erk            !rajout tel du magasin                                                 !
!V52  9687 02-07-04!Evo!bp             !Ajout de diese dans le rang 15 , date commande,No commande            !
!V52  9574 24-06-04!Evo!erk            !lettre de voiture                                                     !
!V52  9557 23-06-04!New!erk            !edition des tourn�es                                                  !
!V52  9549 22-06-04!Evo!erk            !tournee                                                               !
!V52  9537 22-06-04!Evo!erk            !..                                                                    !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!d-brokmini.i          !                                     !tabges                                          !
!maquette.i            !                                     !BONENT                                          !
!varlect.i             !                                     !AUXGES                                          !
!ne-bonedi.i           !                                     !TYPBON                                          !
!n-inimin.i            !                                     !proglib                                         !
!n-tabsoc.i            !                                     !BONLIG                                          !
!saisie.i              !new                                  !ARTICS                                          !
!compana.i             !NEW                                  !BONENS                                          !
!n-declficadr.i        !                                     !BONLGS                                          !
!l-tiespe.i            !auxges  bonent.typaux  bonent.codaux !ARTICL                                          !
!maq-maj.i             !rg 1 proglib.libelle[i-i]            !nomfab                                          !
!maq-edi.i             !rg                                   !transp                                          !
!maq-maj1.i            !10  valeur                           !tables                                          !
!maq-edi.i             !10                                   !auxili                                          !
!n-nbdeci.i            !b-bonlig.usto  nb-dec-qt             !AFFADL                                          !
!maq-maj.i             !14 1 x-y                             !magasi                                          !
!maq-edi.i             !14                                   !                                                !
!n-creficadr.i         !                                     !                                                !
!maq-maj1.i            !14  valeur                           !                                                !
!maq-edi.i             !22                                   !                                                !
!maq-maj.i             !23  1 tabges.libel1[1]               !                                                !
!maq-edi.i             !23                                   !                                                !
!maq-maj.i             !"23" "1" "right-trim(proglib.libelle[!                                                !
!maq-maj1.i            !15  valeur                           !                                                !
!maq-edi.i             !15                                   !                                                !
!maq-edi.i             !21                                   !                                                !
!l-tiers.i             !auxili  bonent.typaux-fac  bonent.cod!                                                !
!maq-maj1.i            !rg valeur                            !                                                !
!maqpedi.i             !                                     !                                                !
!n-saitax.pp           !                                     !                                                !
!ne-edilig.pp          !                                     !                                                !
!n-ficsuit.pp          !                                     !                                                !
!ne-edicom.pp          !                                     !                                                !
!maq-maj1.i            !41  valeur                           !                                                !
!maq-edi.i             !41                                   !                                                !
!maq-maj1.i            !39  valeur                           !                                                !
!maq-edi.i             !39                                   !                                                !
!maq-maj.i             !39 2 valeur[2]                       !                                                !
!maq-maj1.i            !40  valeur                           !                                                !
!maq-edi.i             !40                                   !                                                !
!maq-maj1.i            !42 valeur                            !                                                !
!maq-edi.i             !42                                   !                                                !
!maq-edi.i             !51                                   !                                                !
!l-tiers.i             !auxili  bonent.typaux  bonent.codaux !                                                !
!maq-edi.i             !30                                   !                                                !
!maq-maj.i             !rg 39 bp                             !                                                !
!maq-edi.i             !rg                                   !                                                !
!charana.i             !                                     !                                                !
!n-lastday.pp          !                                     !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
!_____________________________________________________________________________________________________________!
!                                                  P R O C E D U R E S                                        !
!_____________________________________________________________________________________________________________!
!edition-adr           !                                                                                      !
!ENTETE                !                                                                                      !
!REPORT                !                                                                                      !
&End
**************************************************************************************************************/




/*==========================================================================*/
/*                            N E - B O N E D I . P                         */
/*                     Edition de tous les bons de Ventes ou Achats         */
/*==========================================================================*/

{ d-brokmini.i }
{ maquette.i   }
{ varlect.i    }

{ ne-bonedi.i  }

{ n-inimin.i   }
{ n-tabsoc.i   }
{saisie.i new}
{ compana.i NEW }

def buffer  TRN-TABGES  for  TABGES .
def buffer  B-AUXGES    for  AUXGES .
def buffer  HAB-TABGES  for  TABGES .
def buffer  B-BONLIG    for  BONLIG .
def buffer  B-BONENT    for  BONENT .
def buffer  B-TABGES    for  TABGES .


no-tournee = "" .
if rowid-tournee <> ? 
then do :
    FIND TRN-TABGES where rowid( trn-tabges ) = rowid-tournee no-lock no-error .
    if available trn-tabges then no-tournee = trn-tabges.codtab .
end .

def var aff-codsoc   as char               no-undo .
def var aff-etabli   as char               no-undo .

def var sav-ma-liguti     as int init 0                     no-undo .
def var y-a-debut         as logical init yes               no-undo .
def var y-a-2-entetes     as logical init yes               no-undo .
def var premier-report    as logical init yes               no-undo .
def var anc-numbon        as char                           no-undo .

def var last-chrono  as int                 no-undo .
def var rg           as int                 no-undo .
def var nb-tour      as int                 no-undo .
def var i-par        as char                no-undo .
def var o-par        as char                no-undo .
def var x-y          as char                no-undo .
def var x-z          as char                no-undo .

def var x-codech     as char                no-undo .
def var OK-Calech    as log                 no-undo.

def var no-tour      as int                 no-undo .
def var nb-exempl    as int                 no-undo .
def var nb-exempl-sav as int                 no-undo .
DEF VAR top-specif   AS CHAR                NO-UNDO .
def var nbre-lignes  as int                 no-undo .
DEF VAR duree        AS INT                 NO-UNDO .

def var mt-plafond     as dec   extent 9            no-undo .
DEF VAR base-workflow  AS CHAR                      NO-UNDO .
DEF VAR no-seg-adr     AS INT                       NO-UNDO .

assign  
    ma-entete = "entete" 
    ma-pied   = "report"
    .

def temp-table fic-lignes
    field seg-01         as char 
    field seg-02         as char 
    field seg-03         as char 
    field rowid          as rowid 
    index primaire is primary seg-01 seg-02 seg-03 
    . 

{ n-declficadr.i }   /* declaration du fichier adr  FICADR */

def var tx-frf-eur            as dec decimals 6                    no-undo .
def var x-bascule             as char                              no-undo .  

def var i-i                   as int                               no-undo .
def var j-j                   as int                               no-undo .
def var y-a-toxic             as logical init no                   no-undo .
def var top-toxic             as char                              no-undo .
def var x-code-plaque         as char                              no-undo .

def var ok-comment            as log                               no-undo .
def var x-x                   as char                              no-undo .  
def var opt-edilig            as int                               no-undo .  
def var x-seg01               as char                              no-undo .  

def var nb-dec-qtf            as int                               no-undo .
def var nb-dec-qt             as int                               no-undo .
def var nb-dec-pu             as int                               no-undo .
def var msq-qte               as char   extent 4                   no-undo .
def var msq-pu                as char   extent 4                   no-undo .

def var msk-pu                as char   init ">>>>>>>9.999-"       no-undo .
def var msk-qt                as char   init ">>>>>>>9.999-"       no-undo .
def var msk-mt                as char   init ">>>>>>>>9.99-"       no-undo .

DEF VAR trt-specif            AS CHAR                              NO-UNDO .
DEF VAR vect-signat           AS CHAR                              NO-UNDO .


DEF VAR cle-habili            AS CHAR                      NO-UNDO .
DEF VAR cle-mottyp            as char                      no-undo .                     
def var nb-habili             as int                       no-undo .

assign  msq-qte[ 1 ] = ">>>>>>>>>>9-" 
        msq-qte[ 2 ] = ">>>>>>>>9.9-"
        msq-qte[ 3 ] = ">>>>>>>9.99-"
        msq-qte[ 4 ] = ">>>>>>9.999-"
        msq-pu[ 1 ]  = ">>>>>>>>>>9-" 
        msq-pu[ 2 ]  = ">>>>>>>>9.9-"
        msq-pu[ 3 ]  = ">>>>>>>9.99-"
        msq-pu[ 4 ]  = ">>>>>>9.999-"
        .

assign  typtab = "PRM"  prefix = "MASQUES"  codtab = "NE-BONEDI" .
run n-chparax( output x-x ) .
IF x-x <> "" THEN
DO:
    IF ENTRY( 1 , x-x , "/" ) <> "" THEN msq-qte[ 1 ] = ENTRY( 1 , x-x , "/" ) .
    IF ENTRY( 2 , x-x , "/" ) <> "" THEN msq-qte[ 2 ] = ENTRY( 2 , x-x , "/" ) .
    IF ENTRY( 3 , x-x , "/" ) <> "" THEN msq-qte[ 3 ] = ENTRY( 3 , x-x , "/" ) .
    IF ENTRY( 4 , x-x , "/" ) <> "" THEN msq-qte[ 4 ] = ENTRY( 4 , x-x , "/" ) .
    IF ENTRY( 5 , x-x , "/" ) <> "" THEN msq-pu[ 1 ]  = ENTRY( 5 , x-x , "/" ) .
    IF ENTRY( 6 , x-x , "/" ) <> "" THEN msq-pu[ 2 ]  = ENTRY( 6 , x-x , "/" ) .
    IF ENTRY( 7 , x-x , "/" ) <> "" THEN msq-pu[ 3 ]  = ENTRY( 7 , x-x , "/" ) .
    IF ENTRY( 8 , x-x , "/" ) <> "" THEN msq-pu[ 4 ]  = ENTRY( 8 , x-x , "/" ) .
    IF ENTRY( 10 , x-x , "/" ) <> "" THEN msk-mt      = ENTRY( 10 , x-x , "/" ) .
END.

RUN charana .

/* Ceci sert par exemple pour irsid o� l'entete de premiere page ( rg 02 ) n'est pas la m�me que celle 
   des pages de report 
*/
y-a-2-entetes = no .
if lookup( "#032" , ma-posi[ 2 ] ) + lookup( "#033" , ma-posi[ 2 ] ) + 
   lookup( "#034" , ma-posi[ 2 ] ) + lookup( "#035" , ma-posi[ 2 ] ) <> 0 
   then y-a-2-entetes = yes .


FIND bonent where rowid( bonent ) = bonent-rowid no-lock no-error .
if not available bonent 
then do :
    message " Probl�me BONENT non trouv� ... NE-BONEDI.P " view-as alert-box . 
    return.
end .

/* Erik 10/09/2004 on ne traite pas les bons bloqu�s  */
do transaction : 
    FIND b-bonent where rowid( b-bonent ) = bonent-rowid exclusive-lock no-wait no-error .
    if locked b-bonent then return .
    FIND b-bonent where rowid( b-bonent ) = bonent-rowid no-lock no-error .
end . 

nb-exempl = 1 .

assign  typtab = "PRM"  prefix = "NBR-EX"  codtab = bonent.motcle + bonent.typcom .
run n-chparax( output x-x ) .
if x-x = ""
then do :
    assign  typtab = "PRM"  prefix = "NBR-EX"  codtab = bonent.motcle .
    run n-chparax( output x-x ) .
end .
if x-x <> "" and int( entry( 11 , x-x , "/" ) ) >= 1 
             and int( entry( 11 , x-x , "/" ) ) <= 9
   then nb-exempl = dec( entry( 11 , x-x , "/" ) ) .

IF x-x <> "" THEN trt-specif = entry( 31 , x-x , "/" ) .

if bonent.typaux + bonent.codaux <> ""
then do :
    { l-tiespe.i  auxges  bonent.typaux  bonent.codaux }

                        /* 03/09/2004 
                           Erik pour ne pas tenir compte du nombre d'exemplaire en bon de vente 
                           demande CORALIS 
                        */
    if available auxges and bonent.motcle begins "A" 
       and auxges.nb-fact >= 1 and auxges.nb-fact <= 9 then nb-exempl = auxges.nb-fact .
end .

if nb-exempl < 1 then nb-exempl = 1 .
if nb-exempl > 9 then nb-exempl = 9 .

IF typedi = "O"  THEN nb-exempl = 1 .  /* on force l'impression sous forme de brouillon */

top-specif = "" .
IF bonent.stat-valide = 0 THEN 
do:
    nb-exempl-sav = nb-exempl .
    nb-exempl = 1 .

    /* cas eurogem impression d�finitive que si l'operateur est l'un des 2 derniers signataires */
    /* dans le cas de pieces non valid�es  */


    REPEAT:
        IF typedi = "N" AND trt-specif = "599" THEN
        DO:
            /* si seulement 2 signataires, le 1er peut imprimer en definitif  si le total du bon est inf�rieur au montant  */
            /* autoris� au 2eme signataire */

            DO i-i = 2 TO 9 :
                IF lookup( bonent.signataire[ i-i ] , vect-signat ) = 0   THEN  
                    vect-signat = vect-signat  + "," +  bonent.signataire[ i-i ] .
            END.

            IF SUBSTR( vect-signat , 1 , 1) = "," THEN  SUBSTR( vect-signat , 1 , 1) = "" .

            IF NUM-ENTRIES( vect-signat) <> 2 THEN LEAVE.

            j-j = 0 .

            /* recherche code op�rateur acheteur de l'op�rateur */
            FIND FIRST tabges WHERE tabges.codsoc    = ""           
                              and   tabges.etabli    = ""           
                              and   tabges.typtab    = "APR"        
                              and   tabges.prefix    = "OPEACH"   
                              AND   tabges.libel1[2] = glo-operat
                              NO-LOCK NO-ERROR.        
            IF NOT available tabges THEN LEAVE .

            IF tabges.codtab <> ENTRY( 1 , vect-signat ) THEN LEAVE .

            /*  recherche du workflow */
            FOR EACH TABGES where tabges.codsoc = ""
                            and   tabges.etabli = ""
                            and   tabges.typtab = "APR"
                            and   tabges.prefix = "MTACH-AUTO"
                            no-lock :
                i-i = int( substring( tabges.codtab , 1 , 1 ) )  no-error .
                if i-i > 0  and  i-i <= 9  then  mt-plafond[ i-i ] = tabges.nombre[ 1 ] .
            END .

            /*  Chargement du parametrage workflow pour calcul sur HT ou TTC  */
            base-workflow = "TTC" .
            FIND tabges WHERE tabges.codsoc = ""
                        AND   tabges.etabli = ""
                        AND   tabges.typtab = "PRM"  
                        AND   tabges.prefix = "WORKFLOW"  
                        AND   tabges.codtab = "BASE-MTT" 
                        NO-LOCK NO-ERROR.
            IF AVAILABLE tabges  AND TRIM( tabges.libel1[1] ) = "HT" then base-workflow = "HT" .


            /* relecture du circuit du signataire 2 pour recherche du montant de la signature */
            x-motcle = bonent.motcle .
            x-typcom = bonent.typcom .

            IF x-motcle = "ACH" AND x-typcom = "FAC"  THEN
                ASSIGN  x-motcle = "ACC"
                        x-typcom = "CDF"
                        .

            cle-mottyp = x-motcle + " " .

            FIND typbon where typbon.codsoc = bonent.codsoc
                        and   typbon.motcle = x-motcle
                        and   typbon.typcom = X-typcom
                        no-lock  no-error .

            if available typbon  then
            do :
                if typbon.motcle = "ACC"  and  ( int( typbon.typiec ) >= 1  AND int( typbon.typiec ) <= 3  )
                    then  overlay( cle-mottyp , 4 , 1 ) = string( int( typbon.typiec ) , "9" ) .
                if substring( typbon.typaux , 1 , 1 ) = "*"  THEN  overlay( cle-mottyp , 4 , 1 ) = substring( typbon.typaux , 2 , 1 ) .
            end .

            cle-habili = cle-mottyp .
            cle-mottyp = right-trim( cle-mottyp ) .
            if typbon.cle-habilit <> ""  THEN  overlay( cle-habili , 4 , 1 ) = substring( typbon.cle-habilit , 1 , 1 ) .
            cle-habili = right-trim( cle-habili ) .

            FIND TABGES where tabges.codsoc = habili-soc
                        and   tabges.etabli = ""
                        and   tabges.typtab = "HAB"
                        and   tabges.prefix = "ACTION"
                        and   tabges.codtab = cle-habili
                        no-lock  no-error .
            if not available tabges then  cle-habili = "" .

            FIND LAST TABGES where tabges.codsoc = habili-soc
                             and   tabges.etabli = ""
                             and   tabges.typtab = "HAB"
                             and   tabges.prefix = "PRF-" + cle-habili
                             no-lock  no-error .
            IF AVAILABLE tabges THEN  nb-habili = int( substring( tabges.codtab , 1 , 1 ) ) .

            IF nb-habili = 0 THEN LEAVE.

            /*  lecture Fiche Habilitations  */
             FIND HAB-TABGES where hab-tabges.codsoc = habili-soc                                                       
                             and   hab-tabges.etabli = ""                                                               
                             and   hab-tabges.typtab = "HAB"                                                            
                             and   hab-tabges.prefix = "HAB-" + cle-habili                                              
                             and   hab-tabges.codtab = ENTRY( 2 , vect-signat )                                         
                             no-lock  no-error .                                                                        

             IF AVAILABLE hab-tabges THEN    

             do i-i = 2 to nb-habili :                                                                                  
                 IF bonent.signataire[ i-i ] = ENTRY( 2 , vect-signat )THEN                                             
                 DO:                                                                                                    
                     FIND TABGES where tabges.codsoc = habili-soc                                                       
                                 and   tabges.etabli = ""                                                               
                                 and   tabges.typtab = "HAB"                                                            
                                 AND   tabges.prefix = "PRF-" + cle-habili                                              
                                 AND   tabges.codtab = STRING( i-i , "9" )                                              
                                 NO-LOCK NO-ERROR .                                                                     

                     IF AVAILABLE tabges  THEN                                                                          
                     DO:                                                                                                

                         IF  tabges.nombre[ 1 ] <> 0 THEN                                                               
                         DO:                                                                                            
                              /*  Si code plafond est <> 0 et montant Commande  > Plafond : On grise la validation */   
                             j-j =  tabges.nombre[ 1 ] .                                                                
                             if j-j <= 0  or  j-j > 9   then  j-j = 0 .                                                 
                             if j-j <> 0  and  mt-plafond[ j-j ] > 0   then                                             
                             do :                                                                                       
                                 if ( bonent.ttc-bon * bonent.txdev < mt-plafond[ j-j ] and base-workflow = "TTC" ) or  
                                    ( bonent.ht-bon  * bonent.txdev < mt-plafond[ j-j ] and base-workflow = "HT" ) THEN 
                                 ASSIGN top-specif = "O"                                                                
                                        nb-exempl = nb-exempl-sav                                                       
                                        .                                                                               
                             end .                                                                                      
                         END.                                                                                           

                     END.                                                                                               
                     i-i = nb-habili . 

                 END.                                                                                                   
             END.                                                                                                       
        end .
        LEAVE.
    END.
END.

do no-tour = 1 to nb-exempl :

assign y-a-debut      = yes
       premier-report = yes
       .

/*  Chargement Taux EURO / FRF, Devise Societe, Devise Inverse, Top Bascule  */

assign  typtab = "PRM"  prefix = "TX-EUR-FRF"  codtab = "" .
run n-chparax( output x-x ) .
if x-x <> ""
then assign
    x-bascule  =      entry(  3 , x-x , "/" )
    tx-frf-eur = dec( entry( 11 , x-x , "/" ) )
    .

/* ------------------------  Traitement entete --------------------------- */

num-page = 0 .
run value( ma-entete ) .

    rg = 5 .


    if lookup( "#001" , ma-posi[ rg ] ) <> 0 AND num-page = 1
    then do :

        last-chrono = 0 .
        FOR LAST PROGLIB
            where proglib.motcle  = bonent.motcle
            and   proglib.codsoc  = bonent.codsoc
            and   proglib.etabli  = ""
            and   proglib.cle     = "COMMANDE"
            and   proglib.edition = "ENT" + string( bonent.typcom , "xxx" ) +
                                             string( bonent.numbon , "x(10)" )
                no-lock :
                last-chrono = proglib.chrono .
        end.
        FOR EACH PROGLIB
            where proglib.motcle  = bonent.motcle
            and   proglib.codsoc  = bonent.codsoc
            and   proglib.etabli  = ""
            and   proglib.cle     = "COMMANDE"
            and   proglib.edition = "ENT" + string( bonent.typcom , "xxx" ) +
                                             string( bonent.numbon , "x(10)" )
                no-lock :

            j-j = 16 .
            if proglib.chrono = last-chrono 
            then do i-i = 16 to 1 by -1  :
                j-j = i-i .
                if proglib.libelle[i-i] <> "" then leave .
            end .

            if j-j < 16 then j-j = j-j + 1 .
            do i-i = 1 to j-j :


       /*cris */
/*
                IF ma-nblig >= ma-liguti THEN  DO :

                   { maq-maj1.i "01" sov-valeur }
                   { maq-edi.i  "01" }

                END.
*/
                rg = 5 .
                { maq-maj.i rg 1 proglib.libelle[i-i] }
                { maq-edi.i  rg }

            end .

        end .

    end .


/* ------------------------  Traitement des Lignes  ------------------------- */

assign  total-c   = 0 
        top-toxic = "" .

/* Pour TRI eventuel des lignes */
assign  typtab = "PRM"  prefix = "ORD-EDILIG"  codtab = bonent.motcle + bonent.typcom .
run n-chparax( output x-x ) .
if x-x = ""  then
do :
    assign  typtab = "PRM"  prefix = "ORD-EDILIG"  codtab = bonent.motcle .
    run n-chparax( output x-x ) .
end .
if x-x <> ""  then
   opt-edilig = int( entry( 1 , x-x , "/" ) )  no-error .

nbre-lignes = 0 .
FOR EACH BONLIG 
    where bonlig.codsoc = bonent.codsoc
    and   bonlig.motcle = bonent.motcle
    and   bonlig.typcom = bonent.typcom
    and   bonlig.numbon = bonent.numbon
    use-index primaire  no-lock :

    /*  1 = Classement par emplacement magasin  */
    if opt-edilig = 1  and  bonlig.articl <> ""  then
    do :
        FIND ARTICS where artics.codsoc = articl-soc
                    and   artics.articl = bonlig.articl
                    and   artics.theme  = "EMPLAC-"
                    and   artics.chrono = 0
                    no-lock  no-error .
        if available artics
           then  x-seg01 = string( right-trim( artics.alpha[ 1 ] ) , "x(10)" ) + "|" +
                           string( right-trim( artics.alpha[ 2 ] ) , "x(10)" ) + "|" +
                           string( right-trim( artics.alpha[ 3 ] ) , "x(10)" ) + "|" +
                           string( right-trim( artics.alpha[ 4 ] ) , "x(10)" ) . 
           else  x-seg01 = "          |          |          |" .
    end .

    /* Cas des bons multiples */
    if bonlig.numbon begins "$$$$" then x-seg01 = bonlig.numbon-cde + string( bonlig.chrono-cde ) .

    /* ceci ne sert que lors de l'edition des bons de preparation */

/*
MESSAGE 
"no-tournee            "  no-tournee             skip
"trn-tabges.codtab     "  trn-tabges.codtab      skip
"trn-tabges.libel1[10] "  trn-tabges.libel1[10]
    VIEW-AS ALERT-BOX INFO BUTTONS OK.
*/

    if no-tournee <> "" and available trn-tabges and trn-tabges.libel1[10] = "" 
    then do :

        FIND BONENS 
            where bonens.codsoc = bonlig.codsoc
            and   bonens.motcle = bonlig.motcle
            and   bonens.typcom = bonlig.typcom
            and   bonens.numbon begins substring( bonlig.numbon, 1, 8 ) 
            and   bonens.theme  = "TOURNEE"
            and   bonens.chrono = 0
                no-lock no-error .
        if not available bonens then next .
        x-x = substring( bonens.alpha-cle , 11, 10 ) .
        if x-x <> no-tournee then next .

        FIND bonlgs 
            where bonlgs.codsoc = bonlig.codsoc
            and   bonlgs.motcle = bonlig.motcle
            and   bonlgs.typcom = bonlig.typcom
            and   bonlgs.numbon begins substring( bonlig.numbon, 1, 8 ) 
            and   bonlgs.theme  = "TOURNEE"
            and   bonlgs.chrono = bonlig.chrono
                no-lock no-error .
        if not available bonlgs then next .

/*
        MESSAGE 
"bonlig.numbon    "  bonlig.numbon     skip
"bonlig.chrono    "  bonlig.chrono     skip
"bonlig.articl    "  bonlig.articl     skip
"bonlig.articl    "  bonlig.articl     skip
"bonlig.qte[1]    "  bonlig.qte[1]     skip
"bonlgs.nombre[3] "  bonlgs.nombre[3]
            VIEW-AS ALERT-BOX INFO BUTTONS OK.
*/
        if bonlgs.nombre[3] = 0 then next .    /* il n'y a rien � pr�parer */

    end .

    CREATE FIC-LIGNES .
    assign  fic-lignes.seg-01 = x-seg01
            fic-lignes.seg-02 = string( bonlig.chrono , ">>>>9" )  
            fic-lignes.rowid  = rowid( bonlig ) .

    nbre-lignes = nbre-lignes + 1 .

END .  /*  EACH BONLIG  */ 

anc-numbon = "" .

FOR EACH FIC-LIGNES :

    nbre-lignes = nbre-lignes - 1 . /* on decremente le nombre de lignes restant � editer */

    find bonlig where rowid(bonlig) = fic-lignes.rowid no-lock .

    /* pour les bons multiples : edition de la reference du bon chaque entete de bon */
    if bonlig.numbon begins "$$$$" and anc-numbon <> bonlig.numbon-cde
    then do : 

        assign 
            x-x = "Notre r�f�rence de Bon : " + bonlig.numbon-cde
            valeur[ 43 ] = x-x
            valeur[ 45 ] = x-x
            .
        { maq-maj1.i  10  valeur }
        valeur = "" .
        { maq-edi.i  10 " " }

        anc-numbon = bonlig.numbon-cde .
    end .


    if bonlig.codlig = "T"
    then do :
        run ne-edicom( nom-edition , rowid(bonlig) ) .   /* edition des commentaires */
    end .
    ELSE  DO : 

        if bonlig.articl = "" then next .

        FIND ARTICL
            where articl.codsoc = articl-soc
            and   articl.articl = bonlig.articl
                 no-lock  no-error .
        if not available ARTICL
        then do : 
            message 
                "Probleme sur lecture Article dans Soc." articl-soc "->" bonlig.articl view-as alert-box .
            next .
        end . 

        run edi-detail .

        /* Pour ne pas editer un BON chiffre */

        if bonent.bon-valorise = "N" 
        then do :
            do i-i =  6 to 19 : valeur[i-i] = "" . end .
            do i-i = 21 to 23 : valeur[i-i] = "" . end .
        end .

        { maq-maj1.i  10  valeur }
        valeur = "" .
        { maq-edi.i  10 " " }

        do i-i = 1 to 5 :
            total-c[ i-i ] = total-c[ i-i ] + x-qte[ i-i ] .
        end .
        do i-i = 6 to 9 :
            total-c[ i-i ] = total-c[ i-i ] + x-ht[ i-i - 5 ] .
        end .

        if bonlig.motcle begins "V" 
        then assign 
            total-c[ 21 ] = total-c[ 21 ] + bonlig.fac-ht
            total-c[ 22 ] = total-c[ 22 ] + bonlig.fac-ttc .
        else assign
            total-c[ 21 ] = total-c[ 21 ] + bonlig.htnet
            total-c[ 22 ] = total-c[ 22 ] + ttc-lig .

        do i-i = 1 to 5 :
            total-c[ i-i + 100 ] = total-c[ i-i + 100 ] + x-pds[ i-i ] .
        end .

        if bonlig.motcle = "VTE"  then
        REPEAT :

            FIND BONLGS where bonlgs.codsoc = bonlig.codsoc
                        and   bonlgs.motcle = bonlig.motcle
                        and   bonlgs.typcom = bonlig.typcom
                        and   bonlgs.numbon = bonlig.numbon
                        and   bonlgs.chrono = bonlig.chrono
                        and   bonlgs.theme  = "FABRIQU"    
                        and   bonlgs.ss-chrono = 0
                        no-lock  no-error .
            if not available bonlgs  then  leave .

            FIND NOMFAB where nomfab.codsoc = bonlgs.codsoc
                        and   nomfab.articl = bonlgs.alpha-cle
                        and   nomfab.codrct = bonlgs.alpha[ 1 ]
                        and   nomfab.chrono = 0
                        no-lock  no-error .
            if not available nomfab  then  leave .
            /*  Test du statut : Edition des composants sur le B.L.  */
            if substring( nomfab.cod-valeur, 6 , 1 ) <> "1"  then  leave .

            FOR EACH B-BONLIG where b-bonlig.codsoc = bonlgs.codsoc
                              and   b-bonlig.motcle = bonlgs.alpha[ 2 ]
                              and   b-bonlig.typcom = bonlgs.alpha[ 3 ]
                              and   b-bonlig.numbon = bonlgs.alpha[ 4 ]
                              no-lock :

                { n-nbdeci.i  b-bonlig.usto  nb-dec-qt }
                msk-qt = msq-qte[ nb-dec-qt + 1 ] .

                assign  valeur       = ""
                        valeur[ 42 ] = right-trim( b-bonlig.articl )
                        valeur[ 43 ] = right-trim( b-bonlig.libart )
                        valeur[ 50 ] = b-bonlig.usto
                        valeur[ 52 ] = b-bonlig.usto
                        valeur[  2 ] = string( b-bonlig.qte[ 2 ] , msk-qt ) .

                { maq-maj1.i  10  valeur }
                valeur = "" .
                { maq-edi.i  10 }

            END .  /*  EACH B-BONLIG  */

            leave .

        END .  /*  Edition du d�tail des composants d'une fabrication  */

    END .  /*  bonlig.codlig <> "T"  */

/* Mise en commentaire BP le 13.02.2002 - suite pb double impression des lignes de texte    */
/*     ok-comment = no .                                                                    */
/*     if bonlig.codlig = "T"  and nom-edition begins "bp"                                  */
/*          and  ( bonlig.libart = ""  or  bonlig.libart = "*"  or                          */
/*          index( bonlig.libart , "P" ) <> 0 )  then  ok-comment = yes .                   */
/*     if bonlig.codlig = "T"  and  nom-edition begins "bl"  and                            */
/*        ( bonlig.libart = ""  or  bonlig.libart = "*"  or                                 */
/*          index( bonlig.libart , "L" ) <> 0 )  then  ok-comment = yes .                   */
/*     if bonlig.codlig = "T"  and  nom-edition begins "bc"  and                            */
/*        ( bonlig.libart = ""  or  bonlig.libart = "*"  or                                 */
/*          index( bonlig.libart , "C" ) <> 0 )  then  ok-comment = yes .                   */
/*                                                                                          */
/*     if ok-comment = yes                                                                  */
/*     then do :                                                                            */
/*         run ne-edicom( nom-edition , rowid(bonlig) ) .   /* edition des commentaires */  */
/*     end .                                                                                */

    if bonlig.codlig <> "T" 
       then run ne-edicom( "SUP" , rowid(bonlig) ) . /* edition des commentaires supplementaires li�s � la ligne */

    /* Edition des codes texte rattache � l'article */

    if available articl and articl.code-texte <> ""
       then run ne-edicom ( "ART" + articl-soc + "/G-ARS" + articl.code-texte , rowid(bonlig) ) .

        /* Impression saisie eclat�e */
    if lookup( "#001" , ma-posi[14] ) <> 0  
    THEN DO:
        FIND BONLGS where bonlgs.codsoc = bonlig.codsoc
                    and   bonlgs.motcle = bonlig.motcle 
                    and   bonlgs.typcom = bonlig.typcom
                    and   bonlgs.numbon = bonlig.numbon
                    and   bonlgs.chrono = bonlig.chrono
                    and   bonlgs.theme  = "QTE-ECLATE"
                    and   bonlgs.ss-chrono = 0
                    no-lock  no-error .
        if available bonlgs
        then DO:
           IF bonlgs.nombre[ 1 ] <> 0 
           THEN DO:
               x-y = trim ( bonlgs.alpha[ 4 ] ) + " " +
                     trim ( bonlgs.alpha[ 1 ] ) + " " +
                     trim ( bonlgs.alpha[ 2 ] ) + " " 
                     .
               IF bonlgs.nombre[ 3 ] <> 0 THEN x-y = x-y +  trim ( bonlgs.alpha[ 3 ] ) .
               { maq-maj.i  14 1 x-y }
               { maq-edi.i   14 " " }
            END.
        END.
    END.

    if available articl  
       and  articl.code-toxic <> ""  
/*       and  lookup( "#001" , ma-posi[39] ) = 0  /* La maquette ne prevoit pas une ADR a part */*/
       and  lookup( "#001" , ma-posi[39] ) <> 0  /*DELPHINE La maquette ne prevoit pas une ADR a part */
    then do :
        if lookup( right-trim( articl.code-toxic ) , top-toxic ) = 0  then
           top-toxic = top-toxic + "," + right-trim( articl.code-toxic ) .
    end .

    /* Pour Recap des plaques camion 
       les codes plaques peuvent etre renseignes en vecteur 
    */

    if lookup( "#001" , ma-posi[23] ) <> 0  
       and available articl  
       and right-trim(articl.code-plaque) <> "" 
    then do : 
        if x-code-plaque = "" 
            then x-code-plaque = right-trim(articl.code-plaque) .
        else do : 
            do i-i = 1 to num-entries( right-trim(articl.code-plaque) ) :
                if lookup( entry( i-i, right-trim(articl.code-plaque)), x-code-plaque) = 0
                then x-code-plaque = x-code-plaque + "," 
                                   + entry( i-i, right-trim(articl.code-plaque)) .
            end .
        end .
    end .

    /* Pour Edition des declarations ADR */

    if lookup( "#001" , ma-posi[39] ) <> 0  
       and available articl  
    then do : 
        { n-creficadr.i } /* include de cretion de la table temp. FICADR   */
                          /* commun aux modules ne-bonedi.p et ne-trnedi.p */
    end .

    delete fic-lignes .

END .  /*   FOR EACH FIC-LIGNES   */

/*  Traitement Libelle Toxicite                     */
/*--------------------------------------------------*/                          

/*DELPHINE : par defaut pas de toxicite */
y-a-toxic = no.
IF TOP-TOXIC <> ""  THEN
DO :
    /*DELPHINE : il y a de la toxicite  */
    y-a-toxic = yes.

    substring( top-toxic, 1, 1 ) = "" .
    do i-i = 1 to num-entries( top-toxic ) :
        FIND TABGES where tabges.codsoc = ""
                    and   tabges.etabli = ""
                    and   tabges.typtab = "ART"
                    and   tabges.prefix = "TOXICITE"
                    and   tabges.codtab = entry( i-i , top-toxic )
                    no-lock  no-error .
        if not available tabges  then  next .
        assign  valeur[ 1 ] = tabges.libel1[ 1 ]
                valeur[ 2 ] = tabges.libel2[ 2 ]          
                valeur[ 3 ] = tabges.libel2[ 3 ]          
                valeur[ 4 ] = tabges.libel2[ 4 ]          
                valeur[ 5 ] = tabges.libel2[ 5 ] .          

        { maq-maj1.i  14  valeur }
        { maq-edi.i   14 " " }
        valeur = "" .
    end . 
END .

/*  Edition des plaques camion  */

IF x-code-plaque <> ""  THEN
DO :
    { maq-edi.i 22 " " }
    do i-i = 1 to num-entries( x-code-plaque ) :
        FIND TABGES where tabges.codsoc = ""
                    and   tabges.etabli = ""
                    and   tabges.typtab = "ART"
                    and   tabges.prefix = "PLAQUE-TOX"
                    and   tabges.codtab = entry( i-i , x-code-plaque )
                    no-lock  no-error .
        if not available tabges  then  next .
        { maq-maj.i  23  1 tabges.libel1[1] }
        { maq-edi.i 23 " " }

        FIND PROGLIB 
            where proglib.motcle  = ""
            and   proglib.codsoc  = glo-codsoc
            and   proglib.etabli  = ""
            and   proglib.cle     = right-trim(tabges.prefix) + 
                                               right-trim(tabges.codtab)
            and   proglib.edition = ""
            and   proglib.chrono  = 0
                no-lock no-error .
        if available proglib 
        then do i-i = 1 to 15 : 
            if right-trim( proglib.libelle[i-i] ) = "" then next .
            { maq-maj.i "23" "1" "right-trim(proglib.libelle[i-i])" }
            { maq-edi.i 23 " " }
        end .
    end . 
END .

/* -------------------------  Traitement du Pied  --------------------------- */
/*                                                                            */
/*  Mise a Jour dans bonent : - Niveau                                        */
/*                            - Type d'edition, date et Operateur d'edition   */
/*----------------------------------------------------------------------------*/

/* Totaux */
do i-i = 1 to 5 :
    if total-c[ i-i ] <> 0 then
        valeur[ i-i ] = string( total-c[ i-i ] , msq-qte[ 1 ] ) .
end .

do i-i = 6 to 10 :
    if total-c[ i-i ] <> 0  then
       valeur[ i-i ] = string( total-c[ i-i ] , msk-mt ) .
end .

do i-i = 1 to 5 :
    if total-c[ 100 + i-i ] <> 0  then
       valeur[ 100 + i-i ] = string( total-c[ 100 + i-i ] , msq-qte[ 1 ] ) .
end .    

assign
    valeur[ 20 ] = string( bonent.qte, msk-mt )
    valeur[ 21 ] = bonent.adres-liv[ 1 ]
    valeur[ 22 ] = bonent.adres-liv[ 2 ]
    valeur[ 23 ] = bonent.adres-liv[ 3 ]
    valeur[ 24 ] = bonent.adres-liv[ 4 ]
    valeur[ 25 ] = bonent.adres-liv[ 5 ]
    .

valeur[ 30 ] = bonent.transp .
if lookup( "#031" , ma-posi[ 15 ] ) <> 0 then
do :
    FIND TRANSP where transp.codsoc = ""
                and   transp.transp = bonent.transp
                no-lock  no-error .
    if available transp then valeur[ 31 ] = transp.libel1[ 1 ] .
end .

if tx-frf-eur <> 0
then do :
    if x-bascule <> "O"
       then valeur[ 18 ] = string( total-c[ 6 ] / tx-frf-eur , msk-mt ) .
       else valeur[ 18 ] = string( total-c[ 6 ] * tx-frf-eur , msk-mt ) .
end .

/* on controle qu'on puisse editer le pied : edite le pied  exemple : cas des bons pour IRSID */

 If y-a-2-entetes    
   and ma-nblig + max-lig [ 15 ] + 5 > ma-liguti /* avant pas le + 5 , bp le 12.05.2004 pour client 196 */
   and ma-place [ 15 ] = 1
then do :
    ma-sov-rg = ma-rg .
    run value(ma-pied  ).
    ma-rg = ma-sov-rg .
end .


/* message                                   */
/*     "nbre-lignes   "  nbre-lignes   skip  */
/*     "ma-nblig      "  ma-nblig      skip  */
/*     "ma-liguti     "  ma-liguti     skip  */
/*     .                                     */


/* on controle que le pied ne va pas s'�diter sur une page seule : dans ce cas on fait le saut de page avant */
If nbre-lignes = 1   /* nombre de lignes restant */
   and ma-nblig + nbre-lignes = ma-liguti 
then do :
	ma-sov-rg = ma-rg .
	run value(ma-pied).
	ma-rg = ma-sov-rg .
end .

rg = 15 .

/* Acompte */

if bonent.type-acompte = "0" 
then assign
    valeur[ 17 ] = string( bonent.taux-acompte , "99" ) + "%"
    valeur[ 19 ] = string( total-c[ 6 ] * ( bonent.taux-acompte / 100 ) , msk-mt )
    .
else assign
    valeur[ 17 ] = "forfaitaire"
    valeur[ 19 ] = string( bonent.taux-acompte , msk-mt )
    .


if bonent.datliv <> ? then
       valeur[ 83 ] = string( bonent.datliv , "99/99/9999" ) .

assign
    valeur[ 27 ] = string( total-c[ 22 ] - total-c[ 21 ] , msk-mt )    /* total TVA */
    valeur[ 28 ] = string( total-c[ 21 ] , msk-mt )    /* total HT  */
    valeur[ 29 ] = string( total-c[ 22 ] , msk-mt )    /* total TTC */
    .

if tx-frf-eur <> 0
then do :
    if x-bascule <> "O"
       then assign
           valeur[ 35 ] = string( total-c[ 21 ] / tx-frf-eur , msk-mt ) 
           valeur[ 36 ] = string( total-c[ 22 ] / tx-frf-eur , msk-mt )
           valeur[ 37 ] = string( ( total-c[ 6 ] * ( bonent.taux-acompte / 100 )) / tx-frf-eur , msk-mt )
           .
       else assign
           valeur[ 35 ] = string( total-c[ 21 ] * tx-frf-eur , msk-mt ) 
           valeur[ 36 ] = string( total-c[ 22 ] * tx-frf-eur , msk-mt )
           valeur[ 37 ] = string( ( total-c[ 6 ] * ( bonent.taux-acompte / 100 )) * tx-frf-eur  , msk-mt )
           .
end .

valeur[ 38 ] = right-trim( bonent.devise ) .
valeur[ 39 ] = string( num-page      , ">>9" ) .



valeur[ 59 ] = bonent.modreg .

valeur[ 63 ] = string( bonent.codech , "xxx" ) .
if lookup( "#064" , ma-posi[ rg ] ) <> 0 then
do :
    FIND TABGES where tabges.codsoc = "  "
                and   tabges.etabli = "   "
                and   tabges.typtab = "ECH"
                and   tabges.prefix = "CODECH"
                and   tabges.codtab = bonent.codech
                no-lock  no-error .
    if available tabges then valeur[ 64 ] = tabges.libel1[ 1 ] .
end .

if lookup( "#065" , ma-posi[ rg ] ) <> 0  then
do :
    FIND TABLES where tables.codsoc = ""
                and   tables.etabli = ""
                and   tables.typtab = "REG"
                and   tables.prefix = "CODE-REGLM"
                and   tables.codtab = bonent.modreg
                no-lock  no-error .
    if available tables  then  valeur[ 65 ] = right-trim( tables.libel1[ 1 ] ) .
end .

valeur[ 66 ] = STRING( bonent.datbon , "99/99/9999" ) .
valeur[ 67 ] = SUBSTR( bonent.numbon , 1 , 8 ) .

if lookup( "#041" , ma-posi[ 15 ] ) <> 0
then do :
    FIND TABGES where tabges.codsoc = glo-codsoc
                and   tabges.etabli = ""
                and   tabges.typtab = "LIB"
                and   tabges.prefix = nom-edition
                and   tabges.codtab = ""
                no-lock  no-error .
    if available tabges
    then do :
        do i-i = 1 to 5 :
            valeur[ 40 + i-i ] = right-trim( tabges.libel1[ i-i ] ) .
        end .
    end .
end .

if lookup( "#046" , ma-posi[ 15 ] ) <> 0 then
do :
    FIND PROGLIB where proglib.motcle  = bonent.motcle
                 and   proglib.codsoc  = bonent.codsoc
                 and   proglib.etabli  = ""
                 and   proglib.clef    = "COMMANDE"
                 and   proglib.edition = "PIE" +
                                         string( bonent.typcom ,   "xxx" ) +
                                         string( bonent.numbon , "x(10)" )
                 and   proglib.chrono  = 0
                 no-lock  no-error .
    if available proglib  then
    do :                                    

        j-j = 16 .
        do i-i = 16 to 1 by -1  :
            j-j = i-i .
            if proglib.libelle[i-i] <> "" then leave .
        end .

        if j-j < 16 then j-j = j-j + 1 .
        do i-i = 1 to j-j :
            valeur[ 45 + i-i ] = right-trim( proglib.libelle[ i-i ] ) .

        end .

    end .
end .

/* edition du dernier signataire */
do i-i = 9 to 1 by -1 
while ( lookup( string( 170 , "#999" ) , ma-posi[ rg ] ) 
                             + lookup( string( 180 , "#999" ) , ma-posi[ rg ] )
                             + lookup( string( 200 , "#999" ) , ma-posi[ rg ] ) ) <> 0  :

    if bonent.signataire[ i-i ] = "" then next .

    valeur[ 170 ] = bonent.signataire[ i-i ] .   /* Code l'operateur */

    find tabges 
        where tabges.codsoc = ""
        and   tabges.etabli = ""
        and   tabges.typtab = "APR" 
        and   tabges.prefix = "OPEACH" 
        and   tabges.codtab = bonent.signataire[ i-i ]
            no-lock no-error .
    if available tabges
    then do :
        valeur[ 180 ] = tabges.libel1[1] .   /* nom de l'operateur */
        valeur[ 190 ] = tabges.libel1[6] .   /* code du service */
        if lookup( string( 200 , "#999" ) , ma-posi[ rg ] ) <> 0
           and tabges.libel1[6] <> ""
        then do :
            find b-tabges 
                where b-tabges.codsoc = ""
                and   b-tabges.etabli = ""
                and   b-tabges.typtab = "APR" 
                and   b-tabges.prefix = "SERVICE" 
                and   b-tabges.codtab = tabges.libel1[6]
                    no-lock no-error .
            if available b-tabges then valeur[ 200 ] = b-tabges.libel1[1] .   /* nom du service */
        end.
    end.

    find tabges 
        where tabges.codsoc = glo-codsoc
        and   tabges.etabli = ""
        and   tabges.typtab = "HAB" 
        and   tabges.prefix = "PRF-" + bonent.motcle
        and   tabges.codtab = string( i-i , "9" ) 
            no-lock no-error .
    if available tabges then valeur[ 210 ] = tabges.libel1[1] .

    leave .

end .

/* impression specif */
if lookup( string( 171 , "#999" ) , ma-posi[ rg ] ) <> 0 THEN
DO:
    valeur[ 171 ] = bonent.opecre .   /* Code l'operateur */

    find tabges where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "APR" 
                and   tabges.prefix = "OPEACH" 
                and   tabges.libel1[ 2 ] = bonent.opecre
                no-lock no-error .

    if available tabges
    then do :
        valeur[ 181 ] = tabges.libel1[1] .   /* nom de l'operateur */
        valeur[ 191 ] = tabges.libel2[10] .   /* code du service */
        if lookup( string( 201 , "#999" ) , ma-posi[ rg ] ) <> 0
           and tabges.libel2[10] <> ""
        then do :
            case soet-seg[ 1 ] :
                when 0 then assign aff-codsoc = ""
                                   aff-etabli = "" .
                when 1 then assign aff-codsoc = glo-codsoc
                                   aff-etabli = "" .
                when 2 then assign aff-codsoc = glo-codsoc
                                   aff-etabli = glo-etabli .
            end case .

            find tables where tables.codsoc = aff-codsoc 
                        and   tables.etabli = aff-etabli 
                        and   tables.typtab = "ANA" 
                        and   tables.prefix = "SEGMENT-01" 
                        and   tables.codtab = tabges.libel2[10]
                    no-lock no-error .
            if AVAILABLE tables then valeur[ 201 ] = tables.libel1[1] .   /* nom du service */
        end.
    END.
END.


/* edition des signataires */
do i-i = 1 to 9 :

    if lookup( string( 210 + i-i , "#999" ) , ma-posi[ rg ] ) 
                             + lookup( string( 220 + i-i , "#999" ) , ma-posi[ rg ] ) = 0  then next .

    if bonent.signataire[ i-i ] = "" then next .

    valeur[ 210 + i-i ] = bonent.signataire[ i-i ] .   /* Code l'operateur */

    find tabges 
        where tabges.codsoc = ""
        and   tabges.etabli = ""
        and   tabges.typtab = "APR" 
        and   tabges.prefix = "OPEACH" 
        and   tabges.codtab = bonent.signataire[ i-i ]
            no-lock no-error .
    if available tabges
    then do :
        valeur[ 220 + i-i ] = tabges.libel1[1] .   /* nom de l'operateur */
    end.

    leave .

end .

if lookup( string( 231 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 232 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 233 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 234 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 235 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 236 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 237 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 238 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 239 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 241 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 242 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 243 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 244 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 245 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 246 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 247 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 248 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 249 , "#999" ) , ma-posi[ rg ] )      <> 0  then
do :
    find first bonlig 
    where bonlig.codsoc = bonent.codsoc
    and   bonlig.motcle = bonent.motcle
    and   bonlig.typcom = bonent.typcom
    and   bonlig.numbon = bonent.numbon 
         no-lock no-error .

    do i-i = 1 to 9 while available bonlig :

        x-x  = string( i-i , "99" ) .
        valeur[ 230 + i-i ] = bonlig.segana[ i-i ] .

        case soet-seg[ i-i ] :
            when 0 then assign aff-codsoc = ""
                               aff-etabli = "" .
            when 1 then assign aff-codsoc = glo-codsoc
                               aff-etabli = "" .
            when 2 then assign aff-codsoc = glo-codsoc
                               aff-etabli = glo-etabli .
        end case .

        FIND TABLES 
            where tables.codsoc = aff-codsoc
            and   tables.etabli = aff-etabli
            and   tables.typtab = "ANA"
            and   tables.prefix = "SEGMENT-" + x-x 
            and   tables.codtab = bonlig.segana[ i-i ] 
               no-lock  no-error .
        if available tables  then valeur[ 240 + i-i ] = tables.libel1[ 1 ] .
    end .
end .

if lookup( string( 101 , "#999" ) , ma-posi[ rg ] ) + 
   lookup( string( 102 , "#999" ) , ma-posi[ rg ] ) <> 0 
then do :
    FIND BONENS                                   
        where bonens.codsoc = bonent.codsoc       
        and   bonens.motcle = bonent.motcle       
        and   bonens.typcom = bonent.typcom       
        and   bonens.numbon = bonent.numbon       
        and   bonens.theme  = "CONTRAT"        
        and   bonens.chrono = 0                   
            no-lock no-error .        
    if available bonens 
    then  
        assign  valeur[ 101 ] = string( ( total-c[ 21 ] / bonens.nombre[1] ) ,  msk-mt  )   /* cout mensuel contrat */
                valeur[ 102 ] = string( ( total-c[ 21 ] / bonens.nombre[1] ) * 12  ,  msk-mt )   /* date de fin de contrat */
                .

end .

ma-edipied = "O" .
{ maq-maj1.i  15  valeur }
    { maq-edi.i  15 " " }

assign 
    valeur = ""
    total-c = 0
    .

REPEAT TRANSACTION :

    FIND bonent where rowid( bonent ) = bonent-rowid  exclusive-lock  no-error .

    /*   MAJ du Nom de l'Edition dans bonent.edition  ( bcc, blc, bcf, brf, bch )  */

    if lookup( nom-edition , bonent.edition ) = 0 then
    do :
        if bonent.edition <> "" then bonent.edition = bonent.edition + "," .
        bonent.edition = bonent.edition + nom-edition .
    end .
    i-i = lookup( nom-edition , bonent.edition ) .

    assign  bonent.datedi[ i-i ] = today
            bonent.opedi[ i-i ]  = operat .

    leave .

END . /* REPEAT TRANSACTION */

/* Edition du pied */

ma-edipied = "O" .
{ maq-edi.i  21 " " }  /* FORM FEED */
ma-edipied = "" .
ma-nblig = 0 .

/* edition d'une page d'acompagnement apres le bon */

rg = 42 .
if lookup( "#001" , ma-posi[ rg ] ) <> 0
then do :

    assign
        ma-edient = "O"
        valeur    = ""
        .

    last-chrono = 0 .
    FOR LAST PROGLIB
        where proglib.motcle  = bonent.motcle
        and   proglib.codsoc  = bonent.codsoc
        and   proglib.etabli  = ""
        and   proglib.cle     = "COMMANDE"
        and   proglib.edition = "ZZZ" + string( bonent.typcom , "xxx" ) +
                                         string( bonent.numbon , "x(10)" )
            no-lock :
            last-chrono = proglib.chrono .
    end.

    if last-chrono <> 0  
    then do :
        rg = 41 .

        valeur[ 1 ] = string( day( today ) , ">9" ) + "-" +
                  entry( month( today ) , lib-mois ) + "-" +
                  string( year( today ) , "9999" ) .

        valeur[ 35 ] = substring( bonent.numbon, 1, 8  ) .


        do i-i = 1 to 5 :
            valeur[ i-i +  5 ] = bonent.adres[ i-i ] .
        end .

        /*      Chargement Theme 'INFO-EDIT' de AUXGSS              */
        /*      on determine si on edite l'adresse du tiers factur� */

        if available auxges  then
        do :
            i-par = "R|AUXGES|" + string( rowid( AUXGES ) ) + "|INFO-EDIT|0" .
            run n-ficsuit( input i-par , input-output o-par ) .
            if int( entry( 1 , o-par , "|" ) ) = 0  then
            do :
                do i-i = 3 to num-entries( o-par , "|" ) :

                    assign  broker-trav = entry ( i-i , o-par   , "|"  )   
                            x-x         = entry ( 1 , broker-trav , "="  )
                            x-y         = entry ( 2 , broker-trav , "="  ) .
                    if x-x = "A01" and x-y = "1" 
                    then do : 
                        { l-tiers.i  auxili  bonent.typaux-fac  bonent.codaux-fac }
                        if available auxili
                        then do i-i = 1 to 5 :
                            valeur[ i-i + 5 ] = auxili.adres[ i-i ] .
                        end .
                    end.
                end .
            end.
        end .

        valeur[ 92 ] = bonent.nom-contact .

        { maq-maj1.i rg valeur }
        { maq-edi.i  rg }

        rg = 42 .
        FOR EACH PROGLIB
            where proglib.motcle  = bonent.motcle
            and   proglib.codsoc  = bonent.codsoc
            and   proglib.etabli  = ""
            and   proglib.cle     = "COMMANDE"
            and   proglib.edition = "ZZZ" + string( bonent.typcom , "xxx" ) +
                                             string( bonent.numbon , "x(10)" )
                no-lock :

            j-j = 16 .
            if proglib.chrono = last-chrono 
            then do i-i = 16 to 1 by -1  :
                j-j = i-i .
                if proglib.libelle[i-i] <> "" then leave .
            end .

            if j-j < 16 then j-j = j-j + 1 .
            do i-i = 1 to j-j :

                { maq-maj.i rg 1 proglib.libelle[i-i] }
                { maq-edi.i  rg }

            end .

        end .

        rg = 43 . /* saut de page lettre d'accompagnement */
        { maq-edi.i  rg }
    end.

end .

/* return . bp le 17.07.2002 */

/* Edition declaration ADR */

if lookup( "#001" , ma-posi[39] ) <> 0  and y-a-toxic 
then do :
    run edition-adr .
end .


end . /* do no-tour = 1 to nb-exempl */

return .

/****************************
 *    P R O C E D U R E S   *
 ****************************/

{ maqpedi.i }

{ n-saitax.pp }

{ ne-edilig.pp } /* edition de la ligne */

{ n-ficsuit.pp }

{ ne-edicom.pp } /* edition des commentaires */

procedure edition-adr : 

    def var x-coef    as dec decimals 3          no-undo .
    def var cum-qte   as dec decimals 3 extent 3 no-undo .
    def var cum-poids as dec decimals 3 extent 3 no-undo .

    x-topadr = "O" .

    num-page = 0 .
    run value( ma-entete ) .

    for each ficadr 
        no-lock 
        break by ficadr.seg-01 
              by ficadr.seg-02 : 

        if first-of ( ficadr.seg-01 ) 
        then do : 

            /* le coefficent depend de la cat�gorie de transport du produit */
            /* 0 pas de transport - on ne pr�voit pas  */
            /* 1    poids autoris� 20 kg - coef = 50   */
            /* 2    poids autoris� 333 kg - coef = 2   */
            /* 3    poids autoris� 1000 kg - coef = 1  */

            x-coef = 1 .
            if ficadr.cattrp = "1" then x-coef = 50 .
            if ficadr.cattrp = "2" then x-coef = 3 .

            /* On edite d'abord un total des classes dangereuses */

            if ficadr.seg-01 begins "zzzzzzzzzz"
            then do :  
                valeur[1] = "A.D.R." .
                assign
                    valeur[16] = string( cum-qte[3] , ">>>>>>9.999-" )  
                    valeur[20] = string( cum-poids[3] , ">>>>>>9.999-" ) 
                    . 
                { maq-maj1.i  41  valeur }
                { maq-edi.i  41 " " }
                valeur = "" .
                assign         
                    cum-qte[3]   = 0
                    cum-poids[3] = 0
                    .
            end .

/*
"----------------------------------------------------------------------------------------------"
"Identification      denomination                      Classe         Groupe        Etiquette  "
"de la matiere                                                      d'emballage     de danger  "
"----------------------------------------------------------------------------------------------"
"RG39"
" #01           #02                                      #03           #04            #05      "
*/

            if ficadr.alpha[1] <> "" 
            then do :
                assign 
                    valeur[1] = ficadr.seg-03
                    valeur[2] = ficadr.libelle  
                    valeur[3] = ficadr.classe   
                    valeur[4] = ficadr.grpemb   
                    valeur[5] = ficadr.etiqdg   
                    .
                x-coef = 1 .
            end .
            else do :
                valeur[1] = ficadr.seg-03 .
                find TABGES 
                    where tabges.codsoc = ""
                    and   tabges.etabli = ""
                    and   tabges.typtab = "ART" 
                    and   tabges.prefix = "TOXICITE"
                    and   tabges.codtab = ficadr.seg-03
                        no-lock no-error .
                if available TABGES 
                then assign 
                    valeur[2] = tabges.libel1[1]
                    valeur[3] = tabges.libel3[3]
                    valeur[4] = tabges.libel3[2]
                    valeur[5] = tabges.libel3[1]
                    . 

                if available tabges and tabges.nombre[1] <> 0 
                   then x-coef = tabges.nombre[1] .
            end .

            if ficadr.seg-01 begins "zzzzzzzzzz" 
               then assign 
                   valeur[1] = "" 
                   valeur[1] = "Non reglemente" .

            { maq-maj1.i  39  valeur }
            { maq-edi.i 39 }
            valeur = "" .
            do i-i = 2 to 5 while available tabges : 
                if tabges.libel2[i-i] = "" then next .
                valeur[2] = tabges.libel2[i-i] .
                { maq-maj.i  39 2 valeur[2] }
                { maq-edi.i 39 }
            end .
            valeur = "" .
        end .                

        assign 
            valeur[ 2 ]  = string( ficadr.qte   , ">>>>>>9.999-" ) 
            valeur[ 42 ] = ficadr.articl
            valeur[ 43 ] = ficadr.libart
            valeur[ 51 ] = ficadr.ucde
            valeur[ 24 ] = ficadr.condit
            valeur[ 20 ] = string( x-coef * ficadr.poids , ">>>>>>9.999-" ) 
            .
        { maq-maj1.i  40  valeur }
        { maq-edi.i  40 " " }
        valeur = "" .

        assign         
            cum-poids[1] = cum-poids[1] + ( ficadr.poids * x-coef )
            cum-poids[2] = cum-poids[2] + ( ficadr.poids * x-coef )
            cum-poids[3] = cum-poids[3] + ( ficadr.poids * x-coef )
            cum-qte[1]   = cum-qte[1]   + ficadr.qte
            cum-qte[2]   = cum-qte[2]   + ficadr.qte
            cum-qte[3]   = cum-qte[3]   + ficadr.qte
            .

        if last-of ( ficadr.seg-01 ) 
        then do : 
            valeur[1]  = string( ficadr.seg-01 , "x(10)" ) .
            if ficadr.seg-01 begins "zzzzzzzzzz" then valeur[1] = "Non reglemente" .

            assign
                valeur[16] = string( cum-qte[1] , ">>>>>>9.999-" )  
                valeur[20] = string( cum-poids[1] , ">>>>>>9.999-" ) 
                . 
            { maq-maj1.i  41  valeur }
            { maq-edi.i  41 " " }
            valeur = "" .
            assign         
                cum-qte[1]   = 0
                cum-poids[1] = 0
                .
        end .

    end .

    assign
        valeur[2]  = string( ficadr.seg-01 , "x(10)" )
        valeur[16] = string( cum-qte[2] , ">>>>>>9.999-" )  
        valeur[20] = string( cum-poids[2] , ">>>>>>9.999-" ) 
        . 
    { maq-maj1.i  42 valeur }
    { maq-edi.i  42 " " }
    valeur = "" .

    ma-nblig = 0 .
    run value(ma-entete) .

    ma-edipied = "O" .
    { maq-edi.i 51 }
    assign
        ma-edipied = ""
        ma-nblig   = 0
        x-topadr   = ""
        .

end procedure .



PROCEDURE ENTETE :
/*==========================================================================*/
/*                            N E - B O N E N T . P                         */
/*==========================================================================*/

    def buffer b-auxges  for auxges .
    def buffer e-bonlig  for bonlig .

    def var i-i          as int                 no-undo .
    def var j-j          as int                 no-undo .
    def var last-chrono  as int                 no-undo .

    FIND bonent where rowid( bonent ) = bonent-rowid  no-lock .

    { l-tiers.i   auxili  bonent.typaux  bonent.codaux }
    { l-tiespe.i  auxges  bonent.typaux  bonent.codaux }

    assign 
        ma-edient = "O"
        valeur    = ""
        ma-nblig  = 0 
        num-page  = num-page + 1 
        .

    /* cela permet d'editer un fond de page en turbo */
    rg = 7 .
    { maq-edi.i  rg }

    rg = 1 .
    if x-topadr = "O" then rg = 31 .  /* Pour entete de la declaration ADR */

    if top-recap = "1"   /* Pour Entete du Recap par Article */
    then do :
        { maq-edi.i 30 }
        ma-edient = "" .
        return .
    end .

    sav-ma-liguti = 0 .
    /* on gere 2 types d'entete  rg = 01  ou  rg = 02                                     
       suivant que l'on est sur la premiere page ou sur les reports */
    if rg = 1 and y-a-2-entetes 
              and y-a-debut 
    then do :
        rg = 2 .
        sav-ma-liguti = ma-liguti .
        ma-liguti = 53 . /* avant 28 , bp le 12.05.2004 pour client 196 */
        y-a-debut = no .
    end .

    if y-a-2-entetes and not y-a-debut then ma-liguti =63 . /* sav-ma-liguti */ 

    assign valeur[  1 ] = trim( bonent.typaux )
           valeur[  2 ] = trim( bonent.codaux )
           valeur[ 11 ] = trim( bonent.typaux-liv )
           valeur[ 12 ] = trim( bonent.codaux-liv ) .
    do i-i = 1 to 5 :
        assign 
            valeur[ i-i +  5 ] = bonent.adres[ i-i ]
            valeur[ i-i + 15 ] = bonent.adres-liv[ i-i ] .
    end .

    IF bonent.adres-liv[ 1 ] + bonent.adres-liv[ 2 ] + bonent.adres-liv[ 3 ] + 
       bonent.adres-liv[ 4 ] + bonent.adres-liv[ 5 ] = "" THEN
    DO:
        no-seg-adr = 0 .
        assign  typtab = "PRM"  prefix = "AFFADL"  codtab = "SEGMENT" .
        run n-chparax( output x-x ) .
        if x-x <> ""  then  no-seg-adr = int( entry( 11 , x-x , "/" ) )  no-error . 
        IF no-seg-adr < 1 OR no-seg-adr > 10 THEN no-seg-adr = 1 .
        case soet-seg[no-seg-adr] :
            when 0 then assign aff-codsoc = ""
                               aff-etabli = "" .
            when 1 then assign aff-codsoc = glo-codsoc
                               aff-etabli = "" .
            when 2 then assign aff-codsoc = glo-codsoc
                               aff-etabli = glo-etabli .
        end case .

        FIND FIRST AFFADL where affadl.codsoc  = aff-codsoc
                          and   affadl.etabli  = aff-etabli
                          and   affadl.affaire = bonent.segana[no-seg-adr]
                          NO-LOCK NO-ERROR .
        if available AFFADL THEN
            assign  valeur[ 16 ] = affadl.adres[1]
                    valeur[ 17 ] = affadl.adres[2]
                    valeur[ 18 ] = affadl.adres[3]
                    valeur[ 19 ] = affadl.adres[4]
                    valeur[ 20 ] = affadl.adres[5] 
                    .
    END.

    /*  Si Un Client final livr� est stipul� : On le met dans l'adresse de livraison  */
    if bonent.motcle = "ACC"  and  bonent.codaux-fin <> ""  then
    do :
        if lookup( "#16" , ma-posi[ rg ] ) <> 0   or
           lookup( "#17" , ma-posi[ rg ] ) <> 0   or
           lookup( "#18" , ma-posi[ rg ] ) <> 0   or
           lookup( "#19" , ma-posi[ rg ] ) <> 0   or
           lookup( "#20" , ma-posi[ rg ] ) <> 0  then
        do :
            FIND B-AUXGES where b-auxges.codsoc = auxspe-soc
                          and   b-auxges.typaux = bonent.typaux-fin
                          and   b-auxges.codaux = bonent.codaux-fin
                          no-lock  no-error .
            if available b-auxges  then
               assign  valeur[ 11 ] = bonent.typaux-fin
                       valeur[ 12 ] = bonent.codaux-fin
                       valeur[ 16 ] = b-auxges.adres[ 1 ]
                       valeur[ 17 ] = b-auxges.adres[ 2 ]
                       valeur[ 18 ] = b-auxges.adres[ 3 ]
                       valeur[ 19 ] = b-auxges.adres[ 4 ]
                       valeur[ 20 ] = b-auxges.adres[ 5 ] .
        end .
    end .

    if lookup( "#003" , ma-posi[ rg ] ) <> 0   or
       lookup( "#004" , ma-posi[ rg ] ) <> 0   or
       lookup( "#005" , ma-posi[ rg ] ) <> 0  then
    do :
        if available auxges  then
           assign  valeur[ 03 ] = auxges.teleph
                   valeur[ 04 ] = auxges.fax
                   valeur[ 05 ] = auxges.e-mail .
    end .

    /*  AFFADL : T�l�phone - Destinataire  */
    if lookup( "#096" , ma-posi[ rg ] ) + lookup( "#097" , ma-posi[ rg ] ) <> 0  then
    do :
        no-seg-adr = 0 .
        assign  typtab = "PRM"  prefix = "AFFADL"  codtab = "SEGMENT" .
        run n-chparax( output x-x ) .
        if x-x <> ""  then  no-seg-adr = int( entry( 11 , x-x , "/" ) )  no-error . 
        IF no-seg-adr < 1 OR no-seg-adr > 10 THEN no-seg-adr = 1 .

        case soet-seg[ no-seg-adr ] :
            when 0 then assign aff-codsoc = ""
                               aff-etabli = "" .
            when 1 then assign aff-codsoc = glo-codsoc
                               aff-etabli = "" .
            when 2 then assign aff-codsoc = glo-codsoc
                               aff-etabli = glo-etabli .
        end case .

        FIND AFFADL where affadl.codsoc  = glo-codsoc      
                    and   affadl.etabli  = bonent.typaux   
                    and   affadl.affaire = bonent.codaux   
                    and   affadl.chrono  = bonent.num-adl  
                            no-lock  no-error .            
        if not available AFFADL 
        then FIND AFFADL where affadl.codsoc  = aff-codsoc
                         and   affadl.etabli  = aff-etabli
                         and   affadl.affaire = bonent.segana[no-seg-adr]
                         and   affadl.chrono  = bonent.num-adl
                         no-lock  no-error .
        if not available AFFADL 
        then FIND AFFADL where affadl.codsoc  = glo-codsoc
                    and   affadl.etabli  = glo-etabli
                    and   affadl.affaire = ""
                    and   affadl.chrono  = bonent.num-adl
                            no-lock  no-error .
        if available AFFADL  then
           assign  valeur[  96 ] = affadl.teleph
                   valeur[  97 ] = affadl.destin
                   valeur[ 300 ] = affadl.adres[ 1 ]
                   valeur[ 301 ] = affadl.adres[ 2 ]
                   valeur[ 302 ] = affadl.adres[ 3 ]
                   valeur[ 303 ] = affadl.adres[ 4 ]
                   valeur[ 304 ] = affadl.adres[ 5 ] .
    end .

    assign  valeur[ 21 ] = trim( bonent.typaux-fac )
            valeur[ 22 ] = trim( bonent.codaux-fac ) .
    if lookup( "#026" , ma-posi[ rg ] ) <> 0                 /* Tiers Facture */
    or lookup( "#027" , ma-posi[ rg ] ) <> 0
    or lookup( "#028" , ma-posi[ rg ] ) <> 0
    or lookup( "#029" , ma-posi[ rg ] ) <> 0
    or lookup( "#030" , ma-posi[ rg ] ) <> 0 
    then do :
        { l-tiers.i  auxili  bonent.typaux-fac  bonent.codaux-fac }
        if available auxili
        then do i-i = 1 to 5 :
            valeur[ i-i + 25 ] = auxili.adres[ i-i ] .
        end .
    end .

    /* pour envoi de fax - AVM */
    IF ya-fax = YES  THEN
    DO:
        x-y = auxges.nom-respon.
        IF x-y = ""  THEN x-y = bonent.adres[ 1 ] .
        x-z = bonent.fax-contact .
        IF x-z = "" THEN x-z = auxges.fax .
        valeur[15] = CHR(34) + "t:" + x-z + CHR(34) + "  " +
                    /* CHR(34) + "e:" + glo-operat-nom + chr(34) + "  " + */
                     CHR(34) + "d:" +  x-y       + CHR(34) + "  " +
                     CHR(34) + "a:SP" + CHR(34) + " "  + 
                     CHR(34) + "l:nologo" + CHR(34) + " " 
                     .
    END.
    /* pour Offres de prix de DEAL */
    if bonent.typcom = "OPX" 
    then do :
        if bonent.typaux-fac + bonent.codaux-fac <> bonent.typaux +  bonent.codaux  
        then do :
            valeur[ 2 ] = trim( bonent.codaux ) + " / " + trim( bonent.codaux-fac ) .
            { l-tiers.i  auxili  bonent.typaux-fac  bonent.codaux-fac }
            if available auxili
            then do :
                valeur[ 6 ] = right-trim(valeur[6]) + " / " +  right-trim(auxili.adres[1]) .
            end .
        end .
    end .

    FIND TYPBON 
        where typbon.motcle  =  bonent.motcle
        and   typbon.codsoc  =  glo-codsoc
        and   typbon.typcom  =  bonent.typcom
            no-lock no-error .
    if available typbon and typbon.typaux = "*M"  
    then do :
        assign  
            valeur[ 1 ] = ""
            valeur[ 2 ] = bonent.magasin-2
            lect-tab    = bonent.magasin-2
            .
        FIND MAGASI where magasi.codsoc = magasi-soc
                      and magasi.codtab = lect-tab
                      no-lock  no-error .
        if available magasi
        then do :
            valeur[ 6 ] = trim ( magasi.libel1[ 1 ] ) .
            do i-i = 1 to 5 :
                valeur[ i-i + 15 ] = magasi.adres[ i-i ] .
            end .
        end .
    end .

    assign 
        valeur[ 31 ] = bonent.motcle
        valeur[ 32 ] = bonent.typcom
        valeur[ 33 ] = bonent.numbon
        valeur[ 34 ] = substring( bonent.numbon, 1, 8  ) + "." + substring( bonent.numbon , 9 , 2  )
        valeur[ 35 ] = substring( bonent.numbon, 1, 8  )
        .

    /* Cas de bon regroupants plusieurs bons */
    if bonent.numbon begins "$$$$" 
    then assign
        valeur[ 33 ] = "Bon Multiple" 
        valeur[ 34 ] = "Bon Multiple" 
        valeur[ 35 ] = "Bon Multiple"
        .

    assign
        valeur[ 36 ] = bonent.numfac
        valeur[ 37 ] = string( num-page      , ">>9" )
        valeur[ 38 ] = right-trim( bonent.ref-tiers )
        valeur[ 39 ] = right-trim( bonent.ref-magasin )
        .

    valeur[ 41 ] = bonent.magasin .
    if lookup( "#042" , ma-posi[ rg ] ) <> 0 then
    do :
        FIND MAGASI where magasi.codsoc = magasi-soc
                    and   magasi.codtab = bonent.magasin
                    no-lock  no-error .
        if available magasi  then  valeur[ 42 ] = string ( magasi.libel1[ 1 ] , "x(25)" ) .
    end .

    /* Erik 22/04/2004  adresse du magasin - dmd SCAEL 111 � 115 ( Compl�t�e JCC 04/05/2004 ) */                         
    if lookup( "#111" , ma-posi[ rg ] ) <> 0   or
       lookup( "#112" , ma-posi[ rg ] ) <> 0   or
       lookup( "#113" , ma-posi[ rg ] ) <> 0   or
       lookup( "#114" , ma-posi[ rg ] ) <> 0   or
       lookup( "#115" , ma-posi[ rg ] ) <> 0  then
    do :
        FIND MAGASI where magasi.codsoc = magasi-soc
                      and magasi.codtab = bonent.magasin
                      no-lock  no-error .
        if available magasi
        then do :
            valeur[ 110 ] = magasi.teleph-mag .
            do i-i = 1 to 5 :
                valeur[ i-i + 110 ] = magasi.adres[ i-i ] .
            end .
        end .
    end .

    if bonent.ref-bon-orig <> ""  and
       ( lookup( "#116" , ma-posi[ rg ] ) <> 0  or  lookup( "#117" , ma-posi[ rg ] ) <> 0 )  then
    do :
        assign  x-x           = right-trim( substring( bonent.ref-bon-orig , 17 , 4 ) )
                valeur[ 116 ] = x-x
                valeur[ 118 ] = "Pour le compte de : " .
        FIND TABLES where tables.codsoc = ""
                    and   tables.etabli = ""
                    and   tables.typtab = "SOC"
                    and   tables.prefix = "SOCIETE"
                    and   tables.codtab = x-x
                    no-lock  no-error .
        if available tables  then  valeur[ 117 ] = tables.libel1[ 1 ] .
    end .

    valeur[ 43 ] = string( bonent.codech , "xxx" ) .
    if lookup( "#044" , ma-posi[ rg ] ) <> 0 then
    do :
        FIND TABGES where tabges.codsoc = "  "
                    and   tabges.etabli = "   "
                    and   tabges.typtab = "ECH"
                    and   tabges.prefix = "CODECH"
                    and   tabges.codtab = bonent.codech
                    no-lock  no-error .
        if available tabges then valeur[ 44 ] = tabges.libel1[ 1 ] .
    end .

    valeur[ 45 ] = bonent.repres[1] .
    if lookup( "#046" , ma-posi[ rg ] ) <> 0
    then do :
        FIND TABGES
            where tabges.codsoc = ""  
            and   tabges.etabli = ""
            and   tabges.typtab = "CLI"
            and   tabges.prefix = "REPRES"
            and   tabges.codtab = bonent.repres[ 1 ]
                no-lock  no-error .
        if available tabges  then valeur [ 46 ] = tabges.libel1[ 1 ] .

    end .

    valeur[ 47 ] = bonent.mag-respon .
    if lookup( "#048" , ma-posi[ rg ] ) <> 0
    then do :
        FIND MAGASI where magasi.codsoc = magasi-soc
                    and   magasi.codtab = bonent.mag-respon
                    no-lock  no-error .
        if available magasi then
           valeur[ 48 ] = string ( magasi.libel1[ 1 ] , "x(25)" ) .
    end .

    valeur[ 49 ] = string( bonent.transp , "xxx" ) .
    if lookup( "#050" , ma-posi[ rg ] ) <> 0   or
       lookup( "#076" , ma-posi[ rg ] ) <> 0   or
       lookup( "#077" , ma-posi[ rg ] ) <> 0   or
       lookup( "#078" , ma-posi[ rg ] ) <> 0   or
       lookup( "#079" , ma-posi[ rg ] ) <> 0  then
    do :
        FIND TRANSP where transp.codsoc = ""
                    and   transp.transp = bonent.transp
                    no-lock  no-error .
        if available transp  then
           assign  valeur[ 50 ] = transp.libel1[ 1 ]
                   valeur[ 76 ] = transp.libel1[ 2 ]
                   valeur[ 77 ] = transp.libel1[ 3 ]
                   valeur[ 78 ] = transp.libel1[ 4 ]
                   valeur[ 79 ] = transp.libel1[ 5 ] .
    end .

    valeur[ 51 ] = bonent.modliv .
    if lookup( "#052" , ma-posi[ rg ] ) <> 0
    then do :
        FIND TABGES 
            where tabges.codsoc = ""
            and   tabges.etabli = ""
            and   tabges.typtab = "APR"
            and   tabges.prefix = "MODLIV"
            and   tabges.codtab = bonent.modliv
                no-lock  no-error .
        if available tabges then valeur[ 52 ] = tabges.libel1[ 1 ] .
    end .

    valeur[ 53 ] = bonent.codpri .
    if lookup( "#054" , ma-posi[ rg ] ) <> 0
    then do :
        FIND TABGES 
            where tabges.codsoc = glo-codsoc
            and   tabges.etabli = ""
            and   tabges.typtab = "TAR"
            and   tabges.prefix = "CODE-TARIF"
            and   tabges.codtab = bonent.codpri
                no-lock  no-error .
        if available tabges then valeur[ 54 ] = tabges.libel1[ 1 ] .
    end .

    valeur[ 55 ] = bonent.chauffeur .
    if lookup( "#056" , ma-posi[ rg ] ) <> 0
    then do :
        FIND TABGES 
            where tabges.codsoc = ""
            and   tabges.etabli = ""
            and   tabges.typtab = "APR"
            and   tabges.prefix = "CHAUFFEUR"
            and   tabges.codtab = bonent.chauffeur
                no-lock  no-error .
        if available tabges then valeur[ 56 ] = tabges.libel1[ mem-langue ] .
    end .

    valeur[ 57 ] = bonent.repres[2] .

    valeur[ 59 ] = bonent.modreg .
    if lookup( "#065" , ma-posi[ rg ] ) <> 0  then
    do :
        FIND TABLES where tables.codsoc = ""
                    and   tables.etabli = ""
                    and   tables.typtab = "REG"
                    and   tables.prefix = "CODE-REGLM"
                    and   tables.codtab = bonent.modreg
                    no-lock  no-error .
        if available tables  then  valeur[ 65 ] = right-trim( tables.libel1[ 1 ] ) .
    end .


    if bonent.stat-valid = 0 then valeur[ 60 ] = "Bon non Valid�" .

    valeur[ 61 ] = right-trim( bonent.devise ) .
    if lookup( "#062" , ma-posi[ rg ] ) <> 0  then
    do :
        FIND TABLES where tables.codsoc = ""
                    and   tables.etabli = ""
                    and   tables.typtab = "DEV"
                    and   tables.prefix = "DEVISE"
                    and   tables.codtab = bonent.devise
                    no-lock  no-error .
        if available tables  then  valeur[ 62 ] = right-trim( tables.libel1[ 1 ] ) .
    end .

    valeur[ 63 ] = bonent.opecre .
    if lookup( "#064" , ma-posi[ rg ] ) <> 0  then
    do :
        FIND TABLES where tables.codsoc = ""
                    and   tables.etabli = ""
                    and   tables.typtab = "OPE"
                    and   tables.prefix = "OPERATEUR"
                    and   tables.codtab = bonent.opecre
                    no-lock  no-error .
        if available tables  then  valeur[ 64 ] = right-trim( tables.libel1[ 1 ] ) .
    end .

    /* libelle par no d'exemplaire */
    if lookup( "#066" , ma-posi[ rg ] ) <> 0  then
    do :
        assign  typtab = "PRM"  prefix = "NBR-EX"  codtab = bonent.motcle + bonent.typcom + "|" + bonent.motcle .
        run n-chparax( output x-x ) .

        /* libelle dans l'entete li� au no d'exemplaire libel1[ 1 a 9 ]*/
        IF num-entries( x-x , "/" ) >= no-tour THEN valeur[ 66 ] = entry(  no-tour , x-x , "/" ) .

        /* si piece non valide alors prendre libel1[10] pour impression du message "brouillon" */
        IF bonent.stat-valid = 0  AND nb-exempl = 1 AND top-specif = "" THEN 
        do: 
            IF num-entries( x-x , "/" ) >= 10 THEN valeur[ 66 ] = entry(  10 , x-x , "/" ) .
            IF RIGHT-TRIM( valeur[ 66 ] ) = ""  THEN valeur[ 66 ] = "BROUILLON - SANS VALEUR CONTRACTUELLE" .
        END.

        /*  forcer l'impression du brouillon */
        IF typedi = "O" THEN 
        DO:
            valeur[ 66 ] = "" .
            IF num-entries( x-x ) >= 10 THEN valeur[ 66 ] = entry(  10 , x-x , "/" ) .
            IF RIGHT-TRIM( valeur[ 66 ] ) = ""  THEN valeur[ 66 ] = "BROUILLON - SANS VALEUR CONTRACTUELLE" .
        END.

    end .

    valeur[70] = "" .
    if bonent.acheteur <> "" 
       then valeur[70] = valeur[70] + bonent.acheteur .
    if bonent.receptionnaire <> "" 
       then valeur[70] = valeur[70] + "/" + bonent.receptionnaire .
    if bonent.respon-qualite <> "" 
       then valeur[70] = valeur[70] + "/" + bonent.respon-qualite .
    if bonent.signataire[1] <> "" 
       then valeur[70] = valeur[70] + "/" + bonent.signataire[1] .
    if bonent.signataire[2] <> "" 
       then valeur[70] = valeur[70] + "/" + bonent.signataire[2] .
    if bonent.signataire[3] <> "" 
       then valeur[70] = valeur[70] + "/" + bonent.signataire[3] .
    if bonent.signataire[4] <> "" 
       then valeur[70] = valeur[70] + "/" + bonent.signataire[4] .
    if bonent.signataire[5] <> "" 
       then valeur[70] = valeur[70] + "/" + bonent.signataire[5] .

    if valeur[70] begins "/" then overlay( valeur[70],1,1) = "" .

    if lookup( "#071" , ma-posi[ rg ] ) <> 0
    then do : 
        find first e-bonlig 
            where e-bonlig.codsoc = bonent.codsoc
            and   e-bonlig.motcle = bonent.motcle
            and   e-bonlig.typcom = bonent.typcom
            and   e-bonlig.numbon = bonent.numbon 
                 no-lock no-error .
        if available e-bonlig
        then do :
            find first b-bonlig 
                where b-bonlig.codsoc = e-bonlig.codsoc
                and   b-bonlig.motcle = e-bonlig.motcle-cde
                and   b-bonlig.typcom = e-bonlig.typcom-cde
                and   b-bonlig.numbon = e-bonlig.numbon-cde 
                and   b-bonlig.chrono = e-bonlig.chrono-cde 
                    no-lock no-error .
            if available b-bonlig
                then valeur[ 71 ] = string( b-bonlig.numbon , "x(8)" ) .
        end .
        if trim(valeur[71]) = "" then valeur[71] = substring( bonent.ref-bon-orig , 7 ) .

    end .

    valeur[ 72 ] = bonent.lib-piefac .

    if bonent.datech <> ? then valeur[ 80 ] = string( bonent.datech , "99/99/9999" ) .

    else do :
        if BONENT.codech = ""
        then do :
            /* On prend le codech de la fiche client */
            FIND AUXGES NO-LOCK
                WHERE AUXGES.codsoc = auxspe-soc
                  AND AUXGES.typaux = BONENT.typaux
                  AND AUXGES.codaux = BONENT.codaux NO-ERROR.
            IF AVAILABLE AUXGES THEN x-codech = AUXGES.codech.
        end.
        else x-codech = BONENT.codech.
        /* Calcul de l'echeance */
        FIND TABGES where tabges.codsoc = ""
                    and   tabges.etabli = ""
                    and   tabges.typtab = "ECH"
                    and   tabges.prefix = "CODECH"
                    and   tabges.codtab = x-codech no-lock no-error .
        if available tabges  then ASSIGN calech = substring( tabges.libel1[ 2 ], 1, 8 )
                                         x-x    = right-trim( tabges.libel1[ 3 ] )
                                         x-z    = RIGHT-TRIM( tabges.libel1[ 4 ])
                                         .
        REPEAT :
            Ok-Calech = no .
            if x-x = ""  then  leave .
            echeance = BONENT.datdep .
            run last-day( input-output echeance ) .
            echeance = echeance + 1 .
            i-i = 0 .
            x-y = string( month( echeance ) , "99" ) + string( day( echeance ) , "99" ) .
            if x-x < x-y  then  i-i = 1 .
            /* pour passer � l'ann�e suivante */
            IF i-i = 0 AND x-z = "O" THEN i-i = 1.
            echeance = date( int( substring( x-x , 1 , 2 ) ) , int( substring( x-x , 3 , 2 ) ) , year( echeance ) + i-i )
                       no-error .
            if error-status:error = yes  and  x-x = "0229"  then
               do :  x-x = "0228" . error-status:error = no . next . end .
               if error-status:error = no  then  Ok-Calech = yes .
            leave .
        END .
        if Ok-Calech = no
        THEN DO :
            echeance = BONENT.datdep.
            RUN calech.p.
        END.
        if echeance <> ? then valeur[ 80 ] = string( echeance , "99/99/9999" ) .
    end.    

    if bonent.datdep <> ? then
           valeur[ 81 ] = string( bonent.datdep , "99/99/9999" ) .

    if bonent.datbon <> ? then
           valeur[ 82 ] = string( bonent.datbon , "99/99/9999" ) .

    if bonent.datliv <> ? then
           valeur[ 83 ] = string( bonent.datliv , "99/99/9999" ) .

    if bonent.datech <> ? then
       valeur[ 84 ] = string( day( bonent.datech ) , ">9" ) + "-" +
                      entry( month( bonent.datech ) , lib-mois ) + "-" +
                      string( year( bonent.datech ) , "9999" ) .

    if bonent.datdep <> ?  then
       valeur[ 85 ] = string( day( bonent.datdep ) , ">9" ) + "-" +
                      entry( month( bonent.datdep ) , lib-mois ) + "-" +
                      string( year( bonent.datdep ) , "9999" ) .

    if bonent.datbon <> ?  then
       valeur[ 86 ] = string( day( bonent.datbon ) , ">9" ) + "-" +
                      entry( month( bonent.datbon ) , lib-mois ) + "-" +
                      string( year( bonent.datbon ) , "9999" ) .

    i-i = lookup( nom-edition , bonent.edition ) .
    if i-i <> 0 then assign 
                    valeur[ 91 ] = string( bonent.datedi[ i-i ] , "99/99/9999" ) 
                    valeur[ 90 ] = "DUPLICATA" .

    assign  valeur[ 92 ] = bonent.nom-contact
            valeur[ 93 ] = bonent.tel-contact
            valeur[ 94 ] = bonent.fax-contact
            valeur[ 95 ] = bonent.port-contact .

    if lookup( "#100" , ma-posi[ rg ] ) <> 0
    then do : 
        /* libelle du type de bon */
        valeur[ 100 ] = "COMMANDE"  .
        if bonent.motcle = "VTE" then valeur[ 100 ] = "LIVRAISON" .
        if bonent.motcle = "ACH" then valeur[ 100 ] = "RECEPTION" .
        if bonent.typcom begins "TR" then valeur[ 100 ] = "TRANSFERT" .
    end .


    x-x = glo-codsoc .
    if available auxges  and  auxges.typtie[3] <> "" THEN x-x = auxges.typtie[3] .

    valeur[109] = x-x .
    FIND TABLES
      where tables.codsoc = x-x
      and   tables.etabli = glo-etabli
      and   tables.typtab = "ETA"
      and   tables.prefix = "ADRESSE"
      and   tables.codtab = ""
          no-lock no-error .
    if available tables
    then assign
        valeur [101] = tables.libel1[1]
        valeur [102] = tables.libel1[2]
        valeur [103] = tables.libel1[3]
        valeur [104] = tables.libel1[4]
        valeur [105] = tables.libel1[5]
        valeur [106] = tables.libel1[6]
        valeur [107] = tables.libel1[7]
        valeur [108] = tables.libel1[8] 
        .

    assign 
        valeur[ 120 ] = string( bonent.taux-esco, ">>9.99-" )
        valeur[ 121 ] = if bonent.typ-vente = ""
                         then "TE"
                         else string( bonent.typ-vente , "xxx" )
        .

    /* Fax du transporteur */
    if lookup( "#122" , ma-posi[ rg ] ) <> 0 and available transp 
    then do :
        FIND B-AUXGES 
            where b-auxges.codsoc = auxspe-soc
            and   b-auxges.typaux = transp.typaux
            and   b-auxges.codaux = transp.codaux
                no-lock no-error . 
        if available b-auxges then valeur[ 122 ] = b-auxges.fax .    
    end . 

    if lookup( "#141" , ma-posi[ rg ] ) <> 0 and bonent.typact <> ""
    then do :
        find tabges 
            where tabges.codsoc = ""
            and   tabges.etabli = ""
            and   tabges.typtab = "APR"
            and   tabges.prefix = "ACTIV"
            and   tabges.codtab = bonent.typact 
                no-lock no-error . 
        if available tabges 
        then assign
            valeur[ 141 ] = tabges.libel2[1]
            valeur[ 142 ] = tabges.libel2[2]
            valeur[ 143 ] = tabges.libel2[3] 
            valeur[ 144 ] = tabges.libel2[4]
            valeur[ 145 ] = tabges.libel2[5] 
            .
    end .

    
    if lookup( "#151" , ma-posi[ rg ] ) <> 0  then
    do :
        FIND PROGLIB
            where proglib.motcle  = bonent.motcle
            and   proglib.codsoc  = bonent.codsoc
            and   proglib.etabli  = ""
            /*and   proglib.cle     = string( bonent.typcom , "xxx" ) +
                                             string( bonent.numbon , "x(10)" )
            and   proglib.edition = "ENT-BC"*/
            and   proglib.cle     = "commande"
            and   proglib.edition begins "ENT" + string( bonent.typcom , "xxx" ) +
                                             string( bonent.numbon , "x(10)" )

            and   proglib.chrono  = 0
                no-lock  no-error .
        if available proglib
        then do i-i = 1 to 10 :
            valeur[ i-i + 150 ] = proglib.libelle[ i-i ] .
        end .
    end .
    

    valeur[ 160 ] = bonent.acheteur .
    if lookup( "#161" , ma-posi[ 01 ] ) <> 0 then
    do :
        FIND TABGES
            where tabges.codsoc = ""
            and   tabges.etabli = ""
            and   tabges.typtab = "AFF"
            and   tabges.prefix = "XTMH-ACH"
            and   tabges.codtab = bonent.acheteur
                no-lock  no-error .
        if not available tabges 
        then FIND TABGES
            where tabges.codsoc = ""
            and   tabges.etabli = ""
            and   tabges.typtab = "OPE"
            and   tabges.prefix = "XOPE-ACH"
            and   tabges.codtab = bonent.acheteur
                no-lock  no-error .
        if available tabges 
           then assign 
               valeur[ 161 ] = tabges.libel1[ 1 ]
               valeur[ 162 ] = tabges.libel2[ 5 ]    /* telephone */
               .
    end .

    do i-i = 1 to 9 :
        assign 
            valeur[ 170 + i-i ] = bonent.signataire[ i-i ] 
            .
        if lookup( string( 180 + i-i , "#999" ) , ma-posi[ rg ] ) <> 0  then
        do :
            find tabges 
                where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "APR" 
                and   tabges.prefix = "OPEACH" 
                and   tabges.codtab = bonent.signataire[ i-i ]
                    no-lock no-error .
            if available tabges
            then do :
                valeur[ 180 + i-i ] = tabges.libel1[1] .   /* nom de l'operateur */
                valeur[ 190 + i-i ] = tabges.libel1[6] .   /* code du service */
                valeur[ 210 + i-i ] = tabges.libel1[4] .   /* telephone de l'operateur */
                if lookup( string( 200 + i-i , "#999" ) , ma-posi[ rg ] ) <> 0
                   and tabges.libel1[6] <> ""
                then do :
                    find b-tabges 
                        where b-tabges.codsoc = ""
                        and   b-tabges.etabli = ""
                        and   b-tabges.typtab = "APR" 
                        and   b-tabges.prefix = "SERVICE" 
                        and   b-tabges.codtab = tabges.libel1[6]
                            no-lock no-error .

                if available b-tabges then valeur[ 200 + i-i ] = b-tabges.libel1[1] .   /* nom du service */
                end.
            end.

        end .
    end .

    if lookup( string( 221 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 222 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 223 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 224 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 225 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 226 , "#999" ) , ma-posi[ rg ] ) <> 0 
    then do :
        FIND BONENS                                   
            where bonens.codsoc = bonent.codsoc       
            and   bonens.motcle = bonent.motcle       
            and   bonens.typcom = bonent.typcom       
            and   bonens.numbon = bonent.numbon       
            and   bonens.theme  = "CONTRAT"        
            and   bonens.chrono = 0                   
                no-lock no-error .        
        if available bonens 
        then 
        do: 
            assign  valeur[ 221 ] = string( bonens.datte[1] , "99/99/9999" )   /* date de debut de contrat */
                    valeur[ 222 ] = string( bonens.datte[2] , "99/99/9999" )   /* date de fin de contrat */
                    valeur[ 223 ] = string( bonens.datte[3] , "99/99/9999" )   /* date de resiliation de contrat */

                    valeur[ 224 ] = string( bonens.nombre[1], ">9" )           /* periodicite */
                    valeur[ 225 ] = bonens.alpha[ 1 ] 
                    .
            duree = ( YEAR(  bonens.datte[2] ) * 12 ) + MONTH(  bonens.datte[2]) -
                    ( ( YEAR(  bonens.datte[1] ) * 12 ) + MONTH(  bonens.datte[1]) ) .

            IF DAY(bonens.datte[2]) > DAY(bonens.datte[1]) THEN duree = duree + 1 .

            valeur[ 226 ] = STRING( duree , ">>>" ) . /* dur�e en mois */
            valeur[ 227 ] = string( bonens.datte[6] , "99/99/9999" ) .  /* date prochaine facture */
        END.
    end .


    if lookup( string( 231 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 232 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 233 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 234 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 235 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 236 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 237 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 238 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 239 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 241 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 242 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 243 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 244 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 245 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 246 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 247 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 248 , "#999" ) , ma-posi[ rg ] ) + 
       lookup( string( 249 , "#999" ) , ma-posi[ rg ] )      <> 0  then
    do :

        find first e-bonlig 
        where e-bonlig.codsoc = bonent.codsoc
        and   e-bonlig.motcle = bonent.motcle
        and   e-bonlig.typcom = bonent.typcom
        and   e-bonlig.numbon = bonent.numbon 
             no-lock no-error .
        do i-i = 1 to 9 while available e-bonlig :

            x-x  = string( i-i , "99" ) .

            valeur[ 230 + i-i ] = e-bonlig.segana[ i-i ] .

            case soet-seg[ i-i ] :
                when 0 then assign aff-codsoc = ""
                                   aff-etabli = "" .
                when 1 then assign aff-codsoc = glo-codsoc
                                   aff-etabli = "" .
                when 2 then assign aff-codsoc = glo-codsoc
                                   aff-etabli = glo-etabli .
            end case .

            FIND TABLES 
                where tables.codsoc = aff-codsoc
                and   tables.etabli = aff-etabli
                and   tables.typtab = "ANA"
                and   tables.prefix = "SEGMENT-" + x-x 
                and   tables.codtab = e-bonlig.segana[ i-i ] 
                   no-lock  no-error .
            if available tables  then valeur[ 240 + i-i ] = tables.libel1[ 1 ] .
        end .
    end .

    FIND BONENS 
        where bonens.codsoc = bonent.codsoc
        and   bonens.motcle = bonent.motcle
        and   bonens.typcom = bonent.typcom
        and   bonens.numbon = bonent.numbon
        and   bonens.theme  = "PRE-RECEPT"
        and   bonens.chrono = 0
            no-lock no-error .
    if available BONENS
    then do :

        /* receptionnaire */
        valeur[ 250 ] = bonens.alpha[1] .
        if lookup( "#251" , ma-posi[ rg ] ) <> 0 then                           
        do :                                                                    
             FIND TABGES                                                         
                where tabges.codsoc = ""                                        
                and   tabges.etabli = ""                                        
                and   tabges.typtab = "APR"                                     
                and   tabges.prefix = "OPEACH"                                
                and   tabges.codtab = bonens.alpha[1]                           
                    no-lock  no-error .                                         
            if available tabges then valeur[ 251 ] = tabges.libel1[ 1 ] .       
        end .                                                                   

        valeur[ 252 ] = bonens.alpha[2] .
        valeur[ 253 ] = bonens.alpha[3] .
        valeur[ 254 ] = bonens.alpha[4] .
        valeur[ 255 ] = bonens.alpha[5].
        valeur[ 256 ] = bonens.alpha[6] .
        valeur[ 257 ] = bonens.alpha[7] .
        valeur[ 258 ] = bonens.alpha[8] .
        valeur[ 259 ] = bonens.alpha[9] .

        if bonens.alpha[5] = "O" then valeur[ 255 ] = "OUI"  .
        if bonens.alpha[5] = "N" then valeur[ 255 ] = "NON"  .
        if bonens.alpha[6] = "O" then valeur[ 256 ] = "OUI"  .
        if bonens.alpha[6] = "N" then valeur[ 256 ] = "NON"  .
        if bonens.alpha[7] = "O" then valeur[ 257 ] = "OUI"  .
        if bonens.alpha[7] = "N" then valeur[ 257 ] = "NON"  .

    end .

    if lookup( "#261" , ma-posi[ rg ] ) <> 0 then
    do :
        FIND PROGLIB where proglib.motcle  = bonent.motcle
                     and   proglib.codsoc  = bonent.codsoc
                     and   proglib.etabli  = ""
                     and   proglib.clef    = "COMMANDE"
                     and   proglib.edition = "PRE" +
                                             string( bonent.typcom ,   "xxx" ) +
                                             string( bonent.numbon , "x(10)" )
                     and   proglib.chrono  = 0
                     no-lock  no-error .
        if available proglib  then
        do :                                    

            j-j = 16 .
            do i-i = 16 to 1 by -1  :
                j-j = i-i .
                if proglib.libelle[i-i] <> "" then leave .
            end .

            if j-j < 16 then j-j = j-j + 1 .
            do i-i = 1 to j-j :
                valeur[ 260 + i-i ] = right-trim( proglib.libelle[ i-i ] ) .
            end .

        end .
    end .

    if lookup( string( 280 , "#999" ) , ma-posi[ rg ] ) <> 0 THEN
    DO:
        /* impression de l'agence de l'operateur de creation */
        find first tabges where tabges.codsoc = ""            
                          AND   tabges.etabli = ""           
                          and   tabges.typtab = "APR"        
                          and   tabges.prefix = "OPEACH" 
                          and   tabges.libel1[ 2 ] = bonent.opecre
                          NO-LOCK NO-ERROR.
        IF AVAILABLE tabges THEN
        do: 
           valeur[ 280 ] = tabges.libel2[10] .
           case soet-seg[1 ] :
                when 0 then assign aff-codsoc = ""
                                   aff-etabli = "" .
                when 1 then assign aff-codsoc = glo-codsoc
                                   aff-etabli = "" .
                when 2 then assign aff-codsoc = glo-codsoc
                                   aff-etabli = glo-etabli .
            end case .

            FIND TABLES 
                where tables.codsoc = aff-codsoc
                and   tables.etabli = aff-etabli
                and   tables.typtab = "ANA"
                and   tables.prefix = "SEGMENT-01"  
                and   tables.codtab = tabges.libel2[10] 
                   no-lock  no-error .
            if available tables  then valeur[ 281 ] = tables.libel1[ 1 ] .
        end.
    END.

    /* infos sur la tournee pour l'edition des bons de pr�paration */

    if available trn-tabges and lookup( string( 320 , "#999" ) , ma-posi[ rg ] ) <> 0 
    then do :

        assign 
            valeur[ 320 ] = trn-tabges.codtab                 /*  No de la torun�e     */
            valeur[ 325 ] = trn-tabges.libel1[1]              /*  Tracteur             */
            valeur[ 326 ] = trn-tabges.libel1[2]              /*  No de semi-remorque  */
            valeur[ 327 ] = trn-tabges.libel1[3]              /*  lieu de chargement   */
            valeur[ 328 ] = trn-tabges.libel1[4]              /*  heure de depart      */
            .

        /* Transporteur */
        valeur[ 321 ] = string( trn-tabges.libel1[5] , "xxx" ) .
        if lookup( "#322" , ma-posi[ rg ] ) <> 0
        then do :
            FIND TRANSP where transp.codsoc = ""
                        and   transp.transp = trn-tabges.libel1[5]
                        no-lock  no-error .
            if available transp  then
               assign  valeur[ 322 ] = transp.libel1[ 1 ] .
        end .

        /* Chauffeur */
        valeur[ 323 ] = trn-tabges.libel1[6] .
        if lookup( "#324" , ma-posi[ rg ] ) <> 0
        then do :
            FIND TABGES 
                where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "APR"
                and   tabges.prefix = "CHAUFFEUR"
                and   tabges.codtab = trn-tabges.libel1[6]
                    no-lock  no-error .
            if available tabges then valeur[ 324 ] = tabges.libel1[ mem-langue ] .
        end .

    end .

/*cris*/

    DEF VAR sov-valeur LIKE valeur .
    DEF VAR ii AS INT .

    DO ii = 1 TO 350 :
        sov-valeur[ii] = valeur[ii]  .
    END.
    { maq-maj1.i rg valeur }
    { maq-edi.i  rg }

    FIND AFFADL where affadl.codsoc  = glo-codsoc      
                and   affadl.etabli  = bonent.typaux   
                and   affadl.affaire = bonent.codaux   
                and   affadl.chrono  = bonent.num-adl  
                        no-lock  no-error .            
    rg = 5 .

    if AVAILABLE affadl 
       AND LOOKUP( "#001" , ma-posi[ rg ] ) <> 0 AND num-page = 1
    then do :
        x-x = string( bonent.codaux, "x(10)" ) + STRING( bonent.num-adl , "99999" ) . 

        last-chrono = 0 .
        FOR LAST PROGLIB
            where proglib.motcle  = bonent.typaux
            and   proglib.codsoc  = auxspe-soc
            and   proglib.etabli  = ""
            and   proglib.cle     = "AFFADL"
            and   proglib.edition = x-x 
                no-lock :
                last-chrono = proglib.chrono .
        end.



        FOR EACH PROGLIB
            where proglib.motcle  = bonent.typaux
            and   proglib.codsoc  = auxspe-soc
            and   proglib.etabli  = ""
            and   proglib.cle     = "AFFADL"
            and   proglib.edition = x-x
                no-lock :

            j-j = 16 .
            if proglib.chrono = last-chrono 
            then do i-i = 16 to 1 by -1  :
                j-j = i-i .
                if proglib.libelle[i-i] <> "" then leave .
            end .

            if j-j < 16 then j-j = j-j + 1 .
            do i-i = 1 to 3 :   /* maximum 3 lignes edit�es */

                rg = 5 .
                { maq-maj.i rg 1 proglib.libelle[i-i] }
                { maq-edi.i  rg }

            end .

        end .

    end .


    /* cela permet d'editer un bandeau de titre associe � l'entete */
    rg = 6 .

    { maq-edi.i  rg }

    assign 
        valeur    = ""  
        ma-edient = ""
        .

END PROCEDURE .  /* PROCEDURE ENTETE */


PROCEDURE REPORT :
/*==========================================================================*/
/*                           N E - B O N R E P . P                          */
/*             Edition   des   bons    ventes   et   achats     ( Report )  */
/*==========================================================================*/

def var rg as int no-undo .
DEF VAR bp AS CHAR .

ma-edipied = "O" .

rg = 20 .
if top-recap = "1"  then  rg = 45 .  /* Pour Recap par Article */
if x-topadr  = "O"  then  rg = 50 .  /* Pour Edition A.D.R.    */

bp = string( num-page      , ">>9" ) .

{ maq-maj.i rg 39 bp }
{ maq-edi.i rg }

assign
    ma-edipied = ""
    ma-nblig   = 0
    .

/*
if premier-report and sav-ma-liguti <> 0 
  then do :
      ma-liguti = sav-ma-liguti .
      premier-report = no . 
end .
*/

END PROCEDURE .

{ charana.i }       /*  Rechargement Analytique  */ /*&FIN*/ /*&FIN*/ /*&FIN*/ /*&FIN*/
{n-lastday.pp}
