<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<?php
/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! Deal/nathalie/php/ns_gesartnat_w3c.php                                                      !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!  99835 2017-04-21!Evo!Petit-Maire Wil![GC][273] dev spe fiche article V7 + nomenclature V7                  !
!V61 61760 22-11-16!Bug!pha            !Pb zone prix de vente modifibale affich�e a non systematiquement, m�em!
!                  !   !               !Copy en : 7.4 WF : 140035                                             !
!V61 61103 31-08-16!Evo!dmz            !DEV deal replace du script de s�curit�                                !
!V61 61049 27-09-16!New!pte            !Erreur sur remont�e de fiche                                          !
!V61 61047 27-09-16!Bug!337            !Retour bug                                                            !
!                  !   !               !Copy en : 7.4                                                         !
!V61 61006 24-09-16!Evo!pha            !Ajout d'une zone "Types de pi�ces interdits" afin de bloquer par exemp!
!V61 60775 07-09-16!Bug!pha            !Modification des param�tres de lancement du programme de recherche art!
!                  !   !               !Copy en : 7.3                                                         !
!V61 60678 31-08-16!Evo!bnj            !Substitutions WF : 123227                                             !
!V61 60309 20-07-16!Evo!bnj            !Substitutions WF : 125047                                             !
!V61 59862 22-06-16!New!pte            !Duplication des articles: ne pas dupliquer le gencod, gtin14...       !
!V61 59244 04-05-16!New!pte            !Controle existence GTIN14                                             !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
?>


<?php

#-----------------------------------------------------------------------------------------------
#   S�curit� : v�rification que l'utilisateur a respect� toutes les �tapes de connexion
include_once("../../../../siexe/deal/emilie/php/do_connect_w3c.php");

#fin de la s�curit�
#-----------------------------------------------------------------------------------------------

//appel de la bibliotheque outil
include($dealCommunPath);
$ses=session_name()."=".session_id();

//appel des classes utilis�es dans cette page
include(deal_commun::classesPath("do-classes_search_w3c.php"));
include(deal_commun::classesPath("do-classes_lect_bin_w3c.php"));
include(deal_commun::classesPath("do-classes_exchange_w3c.php"));
include(deal_commun::classesPath("do-toolsclient_w3c.php"));

require($screenDesignPath);

$date_jour  = date("d/m/Y");
$date_explose = explode("/", $date_jour);
$mois_cours = $date_explose[1];
$annee_cours = $date_explose[2];

//Pour echange PHP
$dogExchange    = search::searchExe("do-dogexchange_w3c.php",1)."?".session_name()."=".session_id() ;
$dogExchangeF10 = search::searchExe("do-dogexchangef10_w3c.php",1)."?".session_name()."=".session_id() ;
$Scandoc        = search::searchExe("ds_annexe_w3c.php",1)."?".session_name()."=".session_id() ;

$SCREEN = $_SCREEN ;

//test si on se trouve en WEBSERVICE
if ( $SystemExchange == 1 )
{
	//appel de la biblioth�que NUSOAP
	//include('nusoap.php');

	//acces au fichier xml de param�trage du wstk
	include ( search::searchExe("dealgate_nusoap_w3c.php",0)  ) ; //$client = new soapclient( $wsdlPath ,'wsdl');

}//on se trouve en WEBSERVICE

// if($_soc != "")
	// $c_codsoc = $_soc;

//----------------------------------------- R E C U P E R A T I O N   S O C I E T E    R E G R O U P E M E N T --------------------------------------------------------------------
if($_soc != "")
{
	$fonction_rech ="DOG_FUNCTION~GETSOCREG�D-CODSOC�0�~codsoc�" . $_soc .  "�Motcle�articl"  ;
	$context = "�CONTEXT�ID=" . session_id() . "|OPERAT=" . GetOperatValue() . "|LANGUE=" . $lang . "|CODSOC=" . $_soc . "|CODETB=" . $c_codetb . "|IP=" . getenv('REMOTE_ADDR') ;	
}
else
{
	$fonction_rech ="DOG_FUNCTION~GETSOCREG�D-CODSOC�0�~codsoc�" . $c_codsoc .  "�Motcle�articl"  ;
	$context = "�CONTEXT�ID=" . session_id() . "|OPERAT=" . GetOperatValue() . "|LANGUE=" . $lang . "|CODSOC=" . $c_codsoc . "|CODETB=" . $c_codetb . "|IP=" . getenv('REMOTE_ADDR') ;	
}

$_DogAction = "GET" ;
$_GetNbRow = 0 ;
//lancement de la fonction de demande
$typbonreponse = SendExchange (  $fonction_rech . $context , "" ) ;

$temprep=explode ( "�" , strstr($typbonreponse,"SOCREG�") );
$tempexp=explode( "�" ,$temprep[0] ) ;
$_socreg = $tempexp[1];	

$temprep=explode ( "�" , strstr($typbonreponse,"LIBEL�") );
$tempexp=explode( "�" ,$temprep[0] ) ;
$_libsocreg = $tempexp[1]; 
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//----------------------------------------- P A R A M E T R A G E    S O C I E T E    R E C E P T I O N    D U P L I C A T I O N --------------------------------------------------------------------
$fonction_rech ="DOG_FUNCTION~CHPARAX�n-parax�0�~CODSOC��TYPTAB�PRM�PREFIX�LST-SOCIETE�CODTAB�DUP-ARTAPR"  ;
//$context = "�CONTEXT�ID=" . session_id() . "|OPERAT=" . GetOperatValue() . "|LANGUE=" . $lang . "|CODSOC=" . $c_codsoc . "|CODETB=" . $c_codetb . "|IP=" . getenv('REMOTE_ADDR') ;
$_DogAction = "GET" ;
$_GetNbRow = 0 ;
//lancement de la fonction de demande
$reponse = SendExchange (  $fonction_rech , "" ) ;

$temprep=explode ( "�" , strstr($reponse,"REPONSE") );
$tempexp=explode( "�" ,$temprep[0] ) ;
$tempexp = explode("/",$tempexp[1]);
$_listSocs = $tempexp[0];
$_socPrincipale = $tempexp[1];	

// $temprep=explode ( "�" , strstr($reponse,"LIBEL1-2") );
// $tempexp=explode( "�" ,$temprep[0] ) ;
// $_socPrincipale = $tempexp[1];	
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

$opeBase 				= "" ;
$expOpeBase 		= explode ( "|" , strstr ( strtoupper ( $ope_base ) , strtoupper(trim ( $app_sel )) ) ) ;
$exp0 					= explode ( "*" , $expOpeBase[0] ) ;
$opeBase 				= $exp0[1] ;

//initialisation � 0 de l'�tat de visualisation
$n_etat_visu = 0 ;

//analyse des zones charges menus
if ( strtoupper ( $_action ) == "VISU" ) 
	$n_etat_visu = 1 ;
//pour le titre du menu
if($_libel_menu==""){
    $_libel_menu = lect_bin::getLabel ( "mess-prognat2_43" , "sl") ;
	if ( $n_etat_visu == 1 ) $_libel_menu = lect_bin::getLabel ( "mess-prognat2_42" , "sl")   ;
}

if ( $ident == "392" ) 
	$staticheight_spe = 300 ; 
else 
	$staticheight_spe = 180 ;
?>
<link rel="stylesheet" href="<?=search::searchExe("dv-params_w3c.css",1)?>"> 
<script src="<?=search::searchExe("do_prototype_w3c.js",1 )?>"></script> 
<script src="<?=search::searchExe("do-dogexchange_w3c.js",1 )?>"></script> 
<script src="<?=search::searchExe("do-dogexchangeinvoke_w3c.js",1 )?>"></script> 
<script src="<?=search::searchExe("do-toolsclient_w3c.js",1)?>"></script>

<SCRIPT LANGUAGE=JAVASCRIPT>
// document.oncontextmenu = DisableRightClick;

// function DisableRightClick() {return false;}

top.document.title="<?=$_libel_menu ?>"
lienDogExchange = "<?=$dogExchange?>";

var ImageD = new ImageDeal();

/************************ DEBUT VARIABLES *************************/
var Rowid = "<?=$_rowid?>" ;

var codelang = "<?=$lang?>" ; 

var typetrt = "" ;
var image = "";

<?if($_soc != ""){?>codsoc = "<?=$_soc?>";<?}else{?>codsoc = "<?=$c_codsoc?>";<?}?>
socConnexion = "<?=$c_codsoc?>";

// alert(codsoc)


var isFicheSoc = "<?=$_ficheSoc?>".toUpperCase();
if(isFicheSoc == 'YES')
	isFicheSoc = true;
else
	isFicheSoc = false;

var socParamMenu = "<?=$_soc?>".toUpperCase();

var lectureSeule = false;
if("<?=$n_etat_visu?>" == 1)
	lectureSeule = true;

var paramDupArtcl = false;
var listeSocsDup = [];

// alert("<?=$_listSocs?>")

if("<?=$_listSocs?>" != "")
{
	paramDupArtcl = true;
	listeSocsDup = "<?=$_listSocs?>".split(',');
}

var socPrincipale = "<?=$_socPrincipale?>".toUpperCase();


//Frame libel
var codart       = "";
var libart_1     = "";
var libart_2     = "";
var geslibel     = "";
var libabr       = "";
var condit       = "";
var libcondit    = "";
var generique    = "";
var typart       = "";
var libtypart    = "";
var typemag      = "";
var typeclient   = "";
var codbloc      = "";
var gesstock     = "";
var tenulot      = "";
var moismini     = "";
var moismaxi     = "";
var stockfab     = "";
var stockdang    = "";
var codebloc1    = "";  
var codebloc2    = "";  
var codebloc3    = ""; 
var codebloc4    = ""; 
var datebloc     = "";
var carbu        = "";
var supplementation  = "";
var soulte       = "";
var corapport    = "";
var corechsem    = "";
var zone         = "";

// Frame Unites
var unisto       = "U";
var libunisto    = "Unit�";
var unicde       = "";
var libunicde    = "";
var coefcde      = "";
var unifac       = "";
var libunifac    = "";
var coeffac      = "";
var surcondit    = "";
var typsurcond   = "";
var unihec       = "";
var homhec       = "";
var coeftaxe     = "";
var coefstat     = "";
var uniteStat    = "";

var matact       = ""; 
var classact     = "";
var jardin       = ""; 
var nomencgv     = ""; 

// Frame NOMENCLATURE
var famart     = "";
var soufam     = "";
var sssfam     = "";
var sssnv4     = "";
var sssnv5     = "";
var codtva     = "";
var tvapre     = "";
var fraisach   = "";
var libtvapre  = "";
var datchgtva  = "";
var puncagn    = "";    
var debcagn    = "";    
var fincagn    = "";    
var datchgart  = "";
var imputa     = "";
var imputv     = "";
var cathomo    = "";
var nohomo     = "";
var txabat     = "";
var nbjret     = "";
var blavoir    = "";
var codeamm    = "" ;
var categorie  = "" ;
var traitement = "" ;
var socapart   = "" ;

var famart2      = "";  
var sfamart2     = "";  
var ssfamart2    = "";    
var negatif      = "" ;
var gencod       = "";
var gtin14       = "";
var douane       = "";
var anccod1      = "";
var anccod2      = "" ;
var artunion	 = "";

// Frame DIVERS
var editar      = "";
var edietiq     = "";
var pxvtemod	= "";
var fourchpx    = "";
var franco      = "";
var typetrp     = "";

var poidsuv     = "";
var poidsbrut   = "";
var poidsnet    = "";
var volume      = "";
var gestpds     = "";
var tauxunion   = "";
var emplac      = "";
var stockmin    = "";
var stockmax    = "";
var stockalert  = "";
var stockmip    = "";
var qteecocde   = "";
//var typauxpa    = "";
//var codauxpa    = "";
//var libcodauxpa = "";
//var prixpa      = "";
//var devise      = "";

// Frame ARTICLES ASSOCIES
var codartstat		= "";
var codartstoc		= "";
var codarttar		= "";
var codartrem		= "";
var codartlie		= "";
var codartrempl1	= "";
var codartrempl2	= "";
var codartrempl3	= "";
var codartrat    	= "";
var codartcvo    	= "";
var typauxfab		= "";
var codauxfab		= "";
var typauxprinc		= "";
var codauxprinc		= "";
var codecumul		= "";

// Frame STATUS
var opecre    = "";
var datcre    = "";
var heucre    = "";
var opemaj    = "";
var datmaj    = "";
var heumaj    = "";
var typcondt  = "";
var articom   = "";



var classe        = "" ;
var classif       = "" ; 
var toxicite      = "" ;
var pointeclair   = "" ;
var tempfroid     = "" ;
var tempchaud     = "" ;    
var consecu       = "" ;   
var etiqdang_1    = "" ;   
var etiqdang_2    = "" ;   
var etiqdang_3    = "" ;   
var etiqdang_4    = "" ;    
var etiqdang_5    = "" ;   
var cattrp        = "" ;   
var symbdang_1    = "" ;   
var symbdang_2    = "" ;   
var symbdang_3    = "" ;    
var symbdang_4    = "" ;   
var symbdang_5    = "" ;   
var etatphy       = "" ;   
var codemb        = "" ;   
var autsoc 		  = "" ;

var descrip_1     = "" ; 
var descrip_2     = "" ; 
var descrip_3     = "" ; 
var descrip_4     = "" ; 
var descrip_5     = "" ; 
var descrip_6     = "" ; 
var carphys_1     = "" ; 
var carphys_2     = "" ; 
var carphys_3     = "" ; 
var carphys_4     = "" ; 
var carphys_5     = "" ; 
var carphys_6     = "" ; 
var carphys_7     = "" ; 
var carphys_8     = "" ; 
var nomtech_1     = "" ; 
var nomtech_2     = "" ; 
var nomtech_3     = "" ; 
var nomtech_4     = "" ; 
var insclasse     = "" ; 
var datefds       = "" ; 

var cat           = "" ;     
var grpemb        = "" ;    
var risque        = "" ;    
var speciale      = "" ;    
var qtelim        = "" ;    
var ident         = "" ; 
var plaque        = "" ;
var segana        = "" ;

// Frame Commentaires Editions + Saisie Bons
var bc_comment_1  = "" ;
var bp_comment_1  = "" ;
var bl_comment_1  = "" ;
var fac_omment_1 = "" ;
var sai_comment_1 = "" ;
var txt_comment_1 = "" ;

var bc_comment_2  = "" ;     
var bp_comment_2  = "" ;     
var bl_comment_2  = "" ;     
var fac_comment_2 = "" ;     
var sai_comment_2 = "" ;     
var txt_comment_2 = "" ;

var bc_comment_3  = "" ;     
var bp_comment_3  = "" ;     
var bl_comment_3  = "" ;     
var fac_comment_3 = "" ;     
var sai_comment_3 = "" ;     
var txt_comment_3 = "" ;

var bc_comment_4  = "" ;     
var bp_comment_4  = "" ;     
var bl_comment_4  = "" ;     
var fac_comment_4 = "" ;     
var sai_comment_4 = "" ;     
var txt_comment_4 = "" ;

var bc_comment_5  = "" ;     
var bp_comment_5  = "" ;     
var bl_comment_5  = "" ;     
var fac_comment_5 = "" ;     
var sai_comment_5 = "" ;     
var txt_comment_5 = "" ;

var isinsaisie = ""
 saisieEnCours = true

var comment = "" ;
var ind_1   = 0 ;
var ind_2   = 0 ;

var TabOnglet1;
if("<?=$_soc?>".toUpperCase() == "00")
	TabOnglet1 = ["DICT�libart_1","DICT�libart_2","DICT�geslibel","DICT�libabr","DICT�condit",
				  "DICT�typ-condt","DICT�txabat","DICT�nbjret","DICT�blavoir","DICT�carbu"				 
				 ];
else
	TabOnglet1 = ["DICT�libart_1","DICT�libart_2","DICT�geslibel","DICT�libabr","DICT�condit",
				  "DICT�typ-condt","DICT�codbloc","DICT�datebloc","DICT�codebloc_1","DICT�codebloc_2",
				  "DICT�codebloc_3","DICT�codebloc_4","DICT�autovtcpt", "DICT�autotypcom" , "DICT�autsoc","DICT�txabat","DICT�nbjret","DICT�blavoir",
				  "DICT�carbu"
				 ];

var TabLibOnglet1 = ["DICTLINK�typ-condt�libel","DICTLINK�condit�libel"];


var TabOnglet2 = ["DICT�unisto","DICT�unicde","DICT�coefcde","DICT�unifac",
				  "DICT�coeffac","DICT�unihec","DICT�homhec","DICT�surcondit","DICT�typ-surcond"
				  ];
	
var TabLibOnglet2 = ["DICTLINK�unisto�libel","DICTLINK�unicde�libel","DICTLINK�unifac�libel"];


var TabOnglet3 = ["DICT�famart","DICT�soufam","DICT�sssfam","DICT�sssnv4","DICT�sssnv5","DICT�famart2",
				  "DICT�sfamart2","DICT�ssfamart2","DICT�type-mag","DICT�type-client","DICT�jardin",
				  "DICT�coeftaxe","DICT�codeamm","DICT�art-union","DICT�douane","DICT�anccod1",
				  "DICT�anccod2","DICT�negatif","DICT�typart","DICT�gencod", "DICT�gtin14","DICT�matact","DICT�classact",
				  "DICT�nomencgv","DICT�soc-apart","DICT�categorie","DICT�traitement","DICT�epandage"
				  ];

var TabLibOnglet3 = ["DICTLINK�famart�libel","DICTLINK�soufam�libel","DICTLINK�sssfam�libel","DICTLINK�sssnv4�libel",
					 "DICTLINK�sssnv5�libel","DICTLINK�famart2�libel","DICTLINK�ssfamart2�libel",
					 "DICTLINK�sfamart2�libel","DICTLINK�type-client�libel","DICTLINK�classact�libel",
					 "DICTLINK�categorie�libel","DICTLINK�typart�libel","DICTLINK�traitement�libel"
					 ];
					 
var TabOnglet4 = ["DICT�imputa","DICT�imputv","DICT�codtva","DICT�tvapre","DICT�datchgtva","DICT�fraisach",
				  "DICT�segana"
				  ];

var TabLibOnglet4 = ["DICTLINK�imputa�libel","DICTLINK�imputv�libel","DICTLINK�codtva�libel",
					 "DICTLINK�tvapre�libel","DICTLINK�segana�libel"
					 ];
					 
var TabOnglet5 = ["DICT�cathomo","DICT�nohomo","DICT�editar","DICT�edietiq","DICT�pxvte-mod","DICT�taux-union","DICT�fourchpx",
				  "DICT�franco","DICT�typetrp"
				  ];

var TabLibOnglet5 = ["DICTLINK�typetrp�libel"];

					 
var TabOnglet6 = ["DICT�codart-stat","DICT�coef-stat","DICT�codart-rat","DICT�generique","DICT�codart-stoc",
				  "DICT�codart-tar","DICT�codart-rem","DICT�codart-lie","DICT�codart-remp_1","DICT�datchgart",
				  "DICT�codart-remp_2","DICT�codart-remp_3","DICT�codart-cvo","DICT�code-cumul","DICT�corapport",
				  "DICT�corechsem","DICT�supplementation","DICT�soulte","DICT�typaux-fab","DICT�codaux-fab",
				  "DICT�typaux-princ","DICT�codaux-princ"
				  ];

var TabLibOnglet6 = ["DICTLINK�codart-stat�libel","DICTLINK�codart-rat�libel","DICTLINK�codart-stoc�libel",
					 "DICTLINK�codart-tar�libel","DICTLINK�codart-rem�libel","DICTLINK�codart-lie�libel",
					 "DICTLINK�codart-remp_1�libel","DICTLINK�codart-remp_2�libel","DICTLINK�codart-remp_3�libel",
					 "DICTLINK�code-cumul�libel","DICTLINK�codaux-fab�libel","DICTLINK�codaux-princ�libel"					 
					];
					 
var TabOnglet7 = ["DICT�gesstock","DICT�tenulot","DICT�stock-min","DICT�stock-max","DICT�stock-mip","DICT�moismini",
				  "DICT�moismaxi","DICT�stock-alerte","DICT�qte-ecocde","DICT�poids","DICT�volume","DICT�gestpds","DICT�poids-brut",
				  "DICT�poids-net","DICT�emplac","DICT�stock-fab","DICT�stock-dang","DICT�recette-unique","DICT�art-nomenc"
				  ];
				  
				  					 
var TabOnglet8 = ["DICT�classe","DICT�classif","DICT�toxicite","DICT�pointeclair","DICT�tempfroid","DICT�tempchaud",
				  "DICT�consecu","DICT�etiqdang_1","DICT�etiqdang_2","DICT�etiqdang_3","DICT�etiqdang_4",
				  "DICT�etiqdang_5","DICT�cattrp","DICT�symbdang_1","DICT�symbdang_2","DICT�symbdang_3","DICT�symbdang_4",
				  "DICT�symbdang_5","DICT�etatphy","DICT�codemb","DICT�descrip_1","DICT�descrip_2","DICT�descrip_3",
				  "DICT�descrip_4","DICT�descrip_5","DICT�descrip_6","DICT�carphys_1","DICT�carphys_2","DICT�carphys_3",
				  "DICT�carphys_4","DICT�carphys_5","DICT�carphys_6","DICT�carphys_7","DICT�carphys_8","DICT�nomtech_1",
				  "DICT�nomtech_2","DICT�nomtech_3","DICT�nomtech_4","DICT�insclasse","DICT�datefds","DICT�cat",
				  "DICT�grpemb","DICT�risque","DICT�speciale","DICT�qtelim","DICT�ident","DICT�plaque"
				  ];

var TabLibOnglet8 = ["DICTLINK�classe�libel","DICTLINK�classif�libel","DICTLINK�toxicite�libel","DICTLINK�consecu�libel",
					 "DICTLINK�cattrp�libel","DICTLINK�symbdang_1�libel","DICTLINK�symbdang_2�libel","DICTLINK�symbdang_3�libel",
					 "DICTLINK�symbdang_4�libel","DICTLINK�symbdang_5�libel","DICTLINK�etatphy�libel","DICTLINK�codemb�libel",
					 "DICTLINK�insclasse�libel","DICTLINK�cat�libel","DICTLINK�grpemb�libel","DICTLINK�speciale�libel",
					 "DICTLINK�plaque�libel"
					];
					
var TabOnglet9 = ["DICT�articom"];

var TabOnglet10 = ["DICT�bc-comment_1","DICT�bp-comment_1","DICT�bl-comment_1","DICT�fac-comment_1","DICT�sai-comment_1","DICT�txt-comment_1",
				   "DICT�bc-comment_2","DICT�bp-comment_2","DICT�bl-comment_2","DICT�fac-comment_2","DICT�sai-comment_2","DICT�txt-comment_2",
				   "DICT�bc-comment_3","DICT�bp-comment_3","DICT�bl-comment_3","DICT�fac-comment_3","DICT�sai-comment_3","DICT�txt-comment_3",
				   "DICT�bc-comment_4","DICT�bp-comment_4","DICT�bl-comment_4","DICT�fac-comment_4","DICT�sai-comment_4","DICT�txt-comment_4",
				   "DICT�bc-comment_5","DICT�bp-comment_5","DICT�bl-comment_5","DICT�fac-comment_5","DICT�sai-comment_5","DICT�txt-comment_5"
					];
					
var TabAllOnglets = TabOnglet1.concat(TabOnglet2,TabOnglet3,TabOnglet4,TabOnglet5,
				    TabOnglet6,TabOnglet7,TabOnglet8,TabOnglet9,TabOnglet10);

var TabAllLibOnglets = TabLibOnglet1.concat(TabLibOnglet2,TabLibOnglet3,TabLibOnglet4,TabLibOnglet5,
				       TabLibOnglet6,TabLibOnglet8);
					   
var ongCours = -1;

var prgRechMagasin = "<?=search::searchExe("ar_magasi_w3c.php",1)?>"+"?<?=session_name()?>=<?=session_id()?>&PHPSESSID=<?=$PHPSESSID?>";
imgDecoche = "<img src='<?=$dealgateRelPath?>/site/img/sel_.png' style='width:15px'>";
imgCoche = "<img src='<?=$dealgateRelPath?>/site/img/sel.png' style='width:15px'>";

				 
/************************ FIN VARIABLES *************************/

function getValeurRsp(motcle, joint) {
	if(joint) {
		return setValueFromKeyword(motcle, true, joint, "", true).trim();
	} else {
		return setValueFromKeyword(motcle, false, "", "", true).trim();
	}
}

if("<?=$debugActif?>" == "oui")
	debug = true;
function trace(text)
{

	try{
	if(typeof text == "object")
	{
		console.log(text)
		// console.table(text)
	}
	else
		console.log(text);
	}
	catch(err){
	}
}

/************************ INITSCREEN *************************/
function InitScreen()
{
	loadTabIndex()
	loadFocus()
	document.onkeydown = bodyKeyDown
	ImageD.Init('ImageD', 'divImage1', 'grid');
	RazGen()
	
	if(!paramDupArtcl)
		$('divSocdup').style.visibility = 'hidden';
	
	gridMultiSel.initGrid();
	
	try{gridSpe.initGridSpe();}catch(ex){trace("---gridSpe.initGridSpe : "+ex)};
	
	if(lectureSeule)
	{
		$('BUT_DUPART').hide();
		$('bt_lie').hide();
		$('majide').hide();
		$('supide').hide();
	}
	
	if ( "<?=$_rowid?>" != "" )
	{
	     $("DICT�codart").value = "<?=$_articl?>"
	     dogexchange($("DICT�codart"),"GET")     
	     $("DICT�codart").readOnly = true
	     $("DICT�codart").className = "dogskininputreadonly" 
	     lancerecherche ()
	}
	
	if(isFicheSoc)
		$('BUT_FICHSOC').style.visibility = 'visible';
	
}
/************************ FIN INITSCREEN *************************/

/************************ BODYKEYDOWN *************************/
function bodyKeyDown(e)
{
	if (document.all)
	{
		keycode 	= event.keyCode  // IE
		ctrlkey 	= event.ctrlKey
		shiftkey 	= event.shiftKey
		strId 		= event.srcElement.id
	}
	else
	{
		keycode		= e.which        // FIREFOX
		ctrlkey 	= e.ctrlKey
		shiftkey 	= e.shiftKey
		strId 		= e.target
		strId 		= strId.id
	}
	
	if(strId.split('-')[0] == 'myGridSpe')
		return;
	
	//Selon la touche
	switch ( keycode )
	{
		case 9	 :
		case 13  :
		case 119 :
		case 120 :
		case 121 :
		case 118: //touche F7
			//On passe dans le bodyKeyDown du SPE si elle existe.
			try{
				if(!bodyKeyDownSpe(keycode))
					return false;
			}catch(ex){trace("----bodyKeyDownSpe : " + ex)};
				
            <?=$c_returnValue?>
			
            if ( strId == "" ) return
			try {
				natureId	= $(strId).getAttribute("NATURE")
				obliId		= $(strId).getAttribute("OBLI")
			} catch ( eXception) {}
			if ( keycode == 9 && shiftkey )
			{
				setInverseFocus ( TabFocusEntree , strId )
				return
			}
			if ( natureId == "SEARCH" )
			{
				trig = "GET"
				if ( keycode == 121 ) trig = "F10"
				if ( keycode == 118 ) trig = "F7"
				dogexchange ( $(strId) , trig )
			}
			else
			{
				if ( keycode == 13 ) validInput($(strId),obliId)
			}
			break
		case 27 :
			//On passe dans le bodyKeyDown du SPE si elle existe.
			try{
				if(!bodyKeyDownSpe(keycode))
					return false;
			}catch(ex){trace("----bodyKeyDownSpe : " + ex)};
			
			RazGen()
			break
		case 113 :
			//On passe dans le bodyKeyDown du SPE si elle existe.
			try{
				if(!bodyKeyDownSpe(keycode))
					return false;
			}catch(ex){trace("----bodyKeyDownSpe : " + ex)};
		
			UpdateArticl()
			break
			

	} //Selon la touche
}

function validInput ( Obj , obli )
{
	trace("--validInput - "+Obj.id+" --")
	
	//ctrlIdValue => function de premier controle de la valeur saisie en fonction de la nature du champ (DATE,STRING,NOMBRE,CHAMP.....)
	if ( !ctrlIdValue ( Obj.id ) ) return
	ind = Obj.id.split("_")
	ind_1 = ""
	try{
		ind_1 = ind[1]
	} catch ( eXception) {}
	
	if($(Obj.id).nodeName == 'TEXTAREA')
	{
		$(Obj.id).value += "\n";
		return;
	}
	
	//On passe dans le validInput du SPE si elle existe.
	try{
		if(!validInputSpe(Obj.id))
			return false;
	}catch(ex){trace("----bodyKeyDownSpe : " + ex)};
	
	switch(Obj.id)
	{
		case 'DICT�artdup':
			if($(Obj.id).value != "" && $('divSocdup').style.visibility == 'hidden')
				dupart();
			break;
		
        case "DICT�gtin14":
            if (!rechGtin()) return;
            break;
        case "DICT�carbu" :
			loadOng('2')
			break;
		case "DICT�typ-surcond" :
			loadOng('3')
			break;
		case "DICT�epandage" :
			loadOng('4')
			break;
		case "DICT�art-nomenc" :
			loadOng('8')
			break;
		case "DICT�bc-comment_"  + ind_1 :
		case "DICT�bp-comment_"  + ind_1 :
		case "DICT�bl-comment_"  + ind_1 :
		case "DICT�fac-comment_" + ind_1 :
		case "DICT�sai-comment_" + ind_1 :
			if ( $( "DICT�bc-comment_"  + ind_1 ).value == "N" &&
				 $( "DICT�bp-comment_"  + ind_1 ).value == "N" &&
				 $( "DICT�bl-comment_"  + ind_1 ).value == "N" &&
				 $( "DICT�fac-comment_" + ind_1 ).value == "N" &&
				 $( "DICT�sai-comment_" + ind_1 ).value == "N" )
			{
				$( "DICT�txt-comment_" + ind_1 ).value     = "" ;
				enableZone( "DICT�txt-comment_" + ind_1 , false )
			}
			else
			{
				enableZone( "DICT�txt-comment_" + ind_1 , true)
			}
			break ;

		case "DICT�txt-comment_"  + ind_1 :
			if ( $( "DICT�txt-comment_"  + ind_1 ).value == "" )
			{
				$( "DICT�bc-comment_"  + ind_1 ).value = "N" ;
				$( "DICT�bp-comment_"  + ind_1 ).value = "N" ;
				$( "DICT�bl-comment_"  + ind_1 ).value = "N" ;
				$( "DICT�fac-comment_" + ind_1 ).value = "N" ;
				$( "DICT�sai-comment_" + ind_1 ).value = "N" ;
				enableZone( "DICT�txt-comment_" + ind_1 , false)
			}
			break;	
		
		case "DICT�coefcde"  :
		case "DICT�coeffac"  : 
		case "DICT�surcondit" :  
		case "DICT�unihec" :     
		case "DICT�coeftaxe" : 
		case "DICT�coef-stat" : 
		case "DICT�homhec" :    
		case "DICT�poids" : 
		case "DICT�poids-brut" : 
		case "DICT�poids-net" : 
		case "DICT�volume" :
        case "DICT�gestpds" :
		case "DICT�taux-union" :
		case "DICT�fourchpx" :
		case "DICT�stock-min" :
		case "DICT�stock-max" :
		case "DICT�stock-alerte" :
		case "DICT�stock-mip" :
		case "DICT�qte-ecocde" :
		case "DICT�txabat"  :
		case "DICT�nbjretc"  : 
		case "DICT�blavoir" : 
		case "DICT�qtelim" :
			if ( isNaN($(Obj.id).value.trim()) &&  $(Obj.id).value.trim() != "" ) 
			{ 
				alert ( "<?=lect_bin::getLabel("mess-accept_24","sl")?>" );
				$(Obj.id).value = "" ;
				$(Obj.id).select();
				return ;
			}
			break;
		case     "DICT�moismini"  :
		case     "DICT�moismaxi"  : 
			if ( isNaN( $(Obj.id ).value.trim() ) &&  $(Obj.id ).value.trim() != "" ) 
			{ 
				alert ( "<?=lect_bin::getLabel("mess-accept_24","sl")?>" );
				$(Obj.id ).value = "" ;
				$(Obj.id ).select();
				return ;
			}
			if ( $(Obj.id ).value.trim() != "" && $(Obj.id ).value.trim() > "12" ) 
			{
				alert ( "le mois ne peut pas �tre sup�rieur � 12 " );
				$(Obj.id ).value = "" ;
				$(Obj.id ).select()
				return ;
			}
			break;
	}
	
	setFocus ( TabFocusEntree , Obj.id )
}
/************************ FIN BODYKEYDOWN *************************/


function dogexchange (myobj,trig)
{
	
	//Initialiser les variables d'option des �changes
	var trigger        = ""
	var strId 	       = ""
	paramFiltre	   	   = ""
	input 	       	   = ""
	dogFct 	       	   = ""
	var dogFontion	   = ""
	var inputParameter = ""
	if ( trig != "" && trig != undefined ) trigger = trig
	
	if ( IsSendingExchange )
	{
		if ( document.all ) event.returnValue = false
		else e.preventDefault()
		return
	}
	

	//Tester le type de l'objet qui est � l'origine
	if (myobj.getAttribute("F10") != null)
	{
		//Affecter l'dientifiant
		strId = myobj.getAttribute("F10")
		//Indiquer le trigger
		trigger = "F10" ;
		//Tester s'il n'est pas en ReadOnly
		if ( $(strId).readOnly == true) return ;
	}
	else
	{
		// Ne rien faire si la zone est en lecture seule
		 if ( myobj.readOnly == true ) return ;
		//Affecter l'identifiant
		strId = myobj.id ;
	}

    //D�terminer le trigger
	if ( trigger == "")
	{
		switch ( keycode)
		{
			case 9	:
			case 13	: //Touche entr�e
				trigger = "GET" ;
				break;
			case 121: //touche F10
				trigger = "F10" ;
				break;
		}
	}

	//Garder l'appel de l'ID
	oldId 		= strId
	oldTrigger 	= trigger

	//Modifier le nom de l'id pour poser le bon code d'appel
	switch ( strId )
	{
		case 'DICT�socdup':
			if(trigger == 'F10')
			{
				$('divMultiSel').style.visibility = 'visible';
				loadMultiSelSocietes();
				return;
			}
			break;
		
		//##337 10/08/16 - ajout GET sur les soci�t�s autoris�es.
		case "DICT�autsoc" :
			if(trigger == "GET")
			{
				if($(strId).value == "")
				{
					setFocus(TabFocusEntree, strId);
					return false;					
				}
				else
				{
					var value = $(strId).value;
					var nbSoc = value.split(';').size();
					var soc = value.split(';')[nbSoc-1];
					
					dogFct = "tables�d-tables";
					input  = "TYPTAB�SOC�PREFIX�SOCIETE�CODTAB�"+soc+"|"+soc;
				}
			}
			else
			{
				dogFct = "tables�d-tables";
				input = "TYPTAB�SOC�PREFIX�SOCIETE�CODTAB�|zzz";				
			}
		break;
		/* Code article */
		case "DICT�codart" : 
			if ( trigger == "F10" )
			{
				PhpArticl = "<?=search::searchExe('nr_articl_w3c.php',1)?>?PHPSESSID=<?=$PHPSESSID?>&_SCREEN=�libabr�"+$(strId).value + "&_codsoc=<?=$c_codsoc2?>&_codbloc=O&n_typart=A&choixart=2&SCREEN=��blocage�N�catalog�NON�typaux��2�codauxges��2�reffou��2�tarif-ref��2�motclef��2�typart�A�2�choixart��2" ;


				sFeatures = "dialogHeight:700px;dialogWidth:950px;status:no;help:no"
				retourExchange = showModalDialog ( PhpArticl , "" , sFeatures)
				if(retourExchange!=undefined){
					
					Rsp = retourExchange;
					$(strId).value = setValueFromKeyword ( "CODART" , false , "" , "" , true )
				}else return
				trigger = "GET"
			}
			if ( trigger == "F7" )
			{
				PhpArticl = "<?=search::searchExe("nr_arbnom_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>";
				sFeatures = "dialogHeight:742px;dialogWidth:900px;status:no;help:no";
				retourExchange = showModalDialog ( PhpArticl , "" , sFeatures) ;
				if(retourExchange!=undefined)
				{
					
                    // Rsp = retourExchange;
                    $(strId).value = retourExchange
				}else return
				trigger = "GET"
			}
			if(trigger=="AUTO") {
				dogFct = "f10-gesarticl�n-gesarticl";
				input = "CODSOC�"+codsoc+"�LIBABR�"+$(strId).value;
			}
			if(trigger == "GET")
			{
				input  	= "CODSOC�"+codsoc+"�CODART�" + $(strId).value
				if ( "<?=$_rowid?>" != "" && $("DICT�codart").value.trim()=="") input += "�ROWID�" + "<?=$_rowid?>" ;
				dogFct 	= "get-gesarticl�n-gesarticl"
				librech     = $("DICT�codart").value;
			}
            break ; 

		/* Conditionnement */
        case "DICT�condit": 
			dogFct = trigger + "-condit�n-tabges";
			input = "condit�" + $(strId).value.trim();
			break;
		/* Type de Conditionnement */
		case "DICT�typ-condt" : 
            input = "MOTCLE�TYP-CONDT�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
        	break;
		/* Unit�s */
		case "DICT�unisto" : 
		case "DICT�unicde" :  
		case "DICT�unifac" :
			dogFct = "typqte�d-tables"
			if(trigger == "GET") dogFct = "get-typqte�d-reftables";
			input = "typqte�" + $(strId).value.trim();
			break;
		/* Famille */
		case "DICT�famart" :
			dogFct = trigger + "-famart�n-tabges";
			input = "famart�" + $(strId).value.trim();
			break;
		/* Sous-Famille */
		case "DICT�soufam" :
			dogFct = trigger + "-soufam�n-tabges";
			input = "famart�" + $("DICT�famart").value.trim() + "�soufam�" + $(strId).value.trim();
			break;
		/* Sous Sous-Famille */
		case "DICT�sssfam" :
			dogFct = trigger + "-sssfam�n-tabges";
			input = "famart�" + $("DICT�famart").value.trim() + "�soufam�" + $("DICT�soufam").value.trim() + "�sssfam�" + $(strId).value.trim();
			break;
		/* Sous niveau 4 */
		case "DICT�sssnv4" :
			dogFct = trigger + "-sssnv4�n-nomart";
			input = "famart�" + $("DICT�famart").value.trim() + "�soufam�" + $("DICT�soufam").value.trim() + "�sssfam�" + $("DICT�sssfam").value.trim();
			input += "�SSSNV4�" + $(strId).value.trim();
			break;
		/* Sous niveau 5 */
		case "DICT�sssnv5" :
			dogFct = trigger + "-sssnv5�n-nomart";
			input = "famart�" + $("DICT�famart").value.trim() + "�soufam�" + $("DICT�soufam").value.trim() + "�sssfam�" + $("DICT�sssfam").value.trim();
			input += "�SSSNV4�" + $("DICT�sssnv4").value.trim() + "�SSSNV5�" + $(strId).value.trim();
			break;
		/* Code march� */	
		case "DICT�famart2" : 
			input = "MOTCLE�FAMILLE2-ART�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
            break;
        /* Sous march� */	
        case "DICT�sfamart2" :
        	input = "MOTCLE�SOUFAM2-ART�PREFIX�" + $("DICT�famart2").value.trim() + "�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
        	break;
		/* Sous Sous march� */		
        case "DICT�ssfamart2" :
        	input = "MOTCLE�SSFM2-ART�PREFIX�" + $("DICT�famart2").value.trim() + $("DICT�sfamart2").value.trim()+ "�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
        	break;	
		/* Type clientele */
        case "DICT�type-client" :
        	input = "�MOTCLE�TYPCLI�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";            
        	break;
		/* Gamme d'article */	
		case "DICT�typart" :
			input = "MOTCLE�TYPART�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�n-tabgen";
        	break;
		/* Classification activit� */
		case "DICT�classact": 
			input   = "�MOTCLE�ACTMET-EXP�prefix�APP�codtab�" + $(strId).value.trim();
            dogFct = trigger + "-TABGEN�a-tabgen";
			break;
		/* Cat�gories */	
		case "DICT�categorie" :
			input = "codtab�" + $(strId).value.trim() + "�motcle�ART-CATEG"
			dogFct = trigger + "-tabgen�n-tabgen"
			break;
		/* Traitement */	
		case "DICT�traitement" :
			input = "codtab�" + $(strId).value.trim() + "�motcle�ART-traitm"
			dogFct = trigger + "-tabgen�n-tabgen"
			break;
		/* Code Imputation Achats  */
		case "DICT�imputa": 
			input = "�TYPIMP��IMPUTA�" + $(strId).value.trim();
 	        dogFct = trigger + "-IMPUTA�n-impapr";
            break;
		/* Code Imputation Ventes  */
		case "DICT�imputv": 
        	input  = "�TYPIMP��IMPUTV�" + $(strId).value.trim();
 	        dogFct = trigger + "-IMPUTV�n-impapr";
            break;
		/* Code TVA precedent et Code TVA */
		case "DICT�tvapre": 
		case "DICT�codtva": 
        	dogFct = "codtva�d-tables"
			if(trigger == "GET")
			{
				dogFct = "get-codtva�d-reftables"
				input = "codtva�" + $(strId).value.trim()
			}
            break;
		/* Segment analytique 1*/
		case "DICT�segana" : 
			dogFct = "segana�d-segana"
			input = "codsoc�"+codsoc+"�codetb�<?=$c_codetb?>"
			break;
		/* Type de transport */
        case "DICT�typetrp": 
        	input = "�MOTCLE�TYPTRA�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
        	break;
		/* Codes articles */		
		case "DICT�codart-remp_1" : 
		case "DICT�codart-remp_2" : 
		case "DICT�codart-remp_3" : 
		case "DICT�codart-lie" : 
		case "DICT�codart-rem" : 
		case "DICT�codart-tar" : 
		case "DICT�codart-rat" : 
		case "DICT�codart-stoc" : 
		case "DICT�codart-stat" : 
			if ( trigger == "F10" )
			{
			   // PhpArticl = "<?=search::searchExe('nr_articl_w3c.php',1)?>?PHPSESSID=<?=$PHPSESSID?>&_SCREEN=�libart�"+$(strId).value
                  PhpArticl = "<?=search::searchExe('nr_articl_w3c.php',1)?>?PHPSESSID=<?=$PHPSESSID?>&_SCREEN=�libabr�"+$(strId).value + "&_codsoc=<?=$c_codsoc2?>&_codbloc=O&n_typart=A&choixart=2&SCREEN=��blocage�N�catalog�NON�typaux��2�codauxges��2�reffou��2�tarif-ref��2�motclef��2�typart�A�2�choixart��2" ;

				sFeatures = "dialogHeight:700px;dialogWidth:950px;status:no;help:no"
				retourExchange = showModalDialog ( PhpArticl , "" , sFeatures)
				if(retourExchange!=undefined){
					Rsp = retourExchange;
					$(strId).value = setValueFromKeyword ( "CODART" , false , "" , "" , true )
				}else return
				trigger = "GET"
			}
			input  	= "CODSOC�"+codsoc+"�CODART�" + $(strId).value
			dogFct 	= "GET-articl�n-articl"
			librech     = $(strId).value; 
			break ;	
		/* Code cumul */	
		case "DICT�code-cumul" :
            input = "�MOTCLE�CODE-CUMUL�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
			break ;	
		/* Type fournisseur */
		case "DICT�typaux-princ" :
		case "DICT�typaux-fab" :
			dogFct = "get-typaux�d-reftables"
			if(trigger == "F10") dogFct = "typaux�d-tables"
			input = "typaux�" + $(strId).value
			break;
		/* Code fournisseur fabricant */
		case "DICT�codaux-fab" :
			if($(strId).value.trim() == "" && trigger == "GET")
			{
				setFocus(TabFocusEntree, strId);
				return;
			}
			if($("DICT�typaux-fab").value.trim() == "")
			{
				alert ( "<?=lect_bin::getLabel('mess-zonoblicpt_22','sl')?>" ) ;
				$("DICT�typaux-fab").focus();
				return;
			}
			s_typaux =$("DICT�typaux-fab").value
			if(trigger == "F10")
			{
				codaux = $("DICT�codaux-fab").value
				rowid = ""
				//Afficher la fen�tre de saisie du mouvement comptable
				PhpAuxges = "<?=search::searchExe("nr_auxges_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_typaux="+$("DICT�typaux-fab").value + '&_evenement=FORCE' + '&_SCREEN=�libabr�' + $('DICT�codaux-fab').value;
				sFeatures = "dialogHeight:500px;dialogWidth:900px;status:no;help:no";
				var retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
				if(retourExchange!=undefined){
					Rsp = retourExchange;
					tmp = retourExchange[0];
					retourExchange = tmp;
				} 
				else return   
				//Mettre � jour le code auxiliaire
				var MonTb = getDataArrayFromData(Rsp[1] )
				$("DICT�codaux-fab").value = getValueFromDataArray ( MonTb , "CODAUXGES" , false , "" , true) ; 
				trigger = "GET"
				
			}
			if(trigger=="GET")
			{
				input = "CODSOC�"+codsoc+"�TYPAUX�"+s_typaux+"�CODAUX�"+$(strId).value+"�CODAUXGES�"+$(strId).value;
				dogFct = "get-auxges�n-auxges";
			}
			break
		/* Code fournisseur Principal */
		case "DICT�codaux-princ" :
			if($(strId).value.trim() == "" && trigger == "GET")
			{
				loadOng('7')
				setFocus(TabFocusEntree, strId);
				return;
			}
			if($("DICT�typaux-princ").value.trim() == "")
			{
				alert ( "<?=lect_bin::getLabel('mess-zonoblicpt_22','sl')?>" ) ;
				$("DICT�typaux-princ").focus();
				return;
			}
			s_typaux =$("DICT�typaux-princ").value
			if(trigger == "F10")
			{
				codaux = $("DICT�codaux-princ").value
				rowid = ""
				//Afficher la fen�tre de saisie du mouvement comptable
				PhpAuxges = "<?=search::searchExe("nr_auxges_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_typaux="+$("DICT�typaux-princ").value + '&_evenement=FORCE' + '&_SCREEN=�libabr�' + $('DICT�codaux-princ').value;
				sFeatures = "dialogHeight:500px;dialogWidth:900px;status:no;help:no";
				var retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
				if(retourExchange!=undefined){
					Rsp = retourExchange;
					tmp = retourExchange[0];
					retourExchange = tmp;
				} 
				else return   
				//Mettre � jour le code auxiliaire
				var MonTb = getDataArrayFromData(Rsp[1] )
				$("DICT�codaux-princ").value = getValueFromDataArray ( MonTb , "CODAUXGES" , false , "" , true) ; 
				trigger = "GET"
				
			}
			if(trigger=="GET")
			{
				input = "CODSOC�"+codsoc+"�TYPAUX�"+s_typaux+"�CODAUX�"+$(strId).value+"�CODAUXGES�"+$(strId).value;
				dogFct = "get-auxges�n-auxges";
			}
			break
		/* Classe danger */
		case "DICT�classe" :
        	input = "�MOTCLE�DGR-CLASSE�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
            break;
		/* Classification */ 	
        case "DICT�classif" :
        	input = "�MOTCLE�DGR-CLASSIF�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
        	break;
		/* Numero ONU */ 
		case "DICT�toxicite" :
        	input = "�MOTCLE�TOXICITE�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
            break;
        /* Num cons. secu */ 
		case "DICT�consecu" :
        	input = "�MOTCLE�DGR-CONSECU�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
        	break;
		/*  Cat. transport */
         case "DICT�cattrp" :
        	input = "�MOTCLE�DGR-CATTRP�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
            break;	
		/* Symbole danger */
		case "DICT�symbdang_1" :
        case "DICT�symbdang_2" :
        case "DICT�symbdang_3" :
        case "DICT�symbdang_4" :
        case "DICT�symbdang_5" :	
			dogFct = trigger + "-tabgen�a-tabgen"
			input = "�MOTCLE�DGR-SYMB�CODTAB�" + $(strId).value.trim()
			break;
		/* etat  physique */
		case "DICT�etatphy" :
        	input = "�MOTCLE�DGR-ETAT�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
            break;
		/* Groupe emballage */
		case "DICT�codemb" :
        	input = "�MOTCLE�DGR-EMB�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
        	break;
		/* Installation class�e */
		case "DICT�insclasse" :
        	input = "�MOTCLE�DGR-INSCLASSE�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
        	break;
		/* Cat�gorie */
		case "DICT�cat" :
        	input = "�MOTCLE�DGR-CAT�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
			break;
		/* Grp emballage */	
		case "DICT�grpemb" :
        	input = "�MOTCLE�DGR-GRPEMB�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
            break;
		/* Phr risque */
		case "DICT�risque" :
			if ( $(strId).value != "" && NumEntries( $(strId).value.trim()  , ",") >= 2 ) 
			{               
                  m = NumEntries( $(strId).value.trim()  , ",")  
                  travail = Entry( m ,$(strId).value.trim() , "," )   ;

            }
        	input = "�MOTCLE�DGR-RISQUE�CODTAB�" + travail;
 	        dogFct = trigger + "-TABGEN�a-tabgen";
        	break;
		/* Phr speciales */
		case "DICT�speciale" :
        	input = "�MOTCLE�DGR-SPECIALE�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
            break;
		/* Code plaques */
		case "DICT�plaque" :
        	input = "�MOTCLE�DGR-PLAQUETOX�CODTAB�" + $(strId).value.trim();
 	        dogFct = trigger + "-TABGEN�a-tabgen";
            break;
        /* Matieres Actives */
		case "DICT�matact" :
            if(strId.value == "" && trigger == "GET"){
				setFocus(TabFocusEntree, strId);
				return true;
			}

			input = "�MOTCLE�MAT-ACTIVE�CODTAB�" +  Entry(NumEntries($(strId).value,","),$(strId).value,",");
			dogFct = "get-TABGEN�a-tabgen";
			if (trigger == "F10" ) {
				input = "�MOTCLE�MAT-ACTIVE�CODTAB�"; ;
				dogFct = "f10-TABGEN�a-tabgen";
			}
			
            break;
        /* Commentaires */
        case "DICT�txt-comment_1" :
        case "DICT�txt-comment_2" :
        case "DICT�txt-comment_3" :
        case "DICT�txt-comment_4" :
        case "DICT�txt-comment_5" :
            dogFct = "f10-codmes�n-proglib";
			input   = "motcle�CTRV�clef�contrat�edition�COMART";
			break;
			
		case 'DICT�provenance':
			dogFct = "FULL-TABGES�n-tabges";
			input = "TYPTAB�ART�PREFIX�PROVENANCE";
			
			if(trigger == "GET")
				input += "�CODTAB�" + $(strId).value
			
			paramFiltre = "FIELD=CODTAB|LIBEL|" ;
			break;
			
		default:
			//On appel le dogexchange de l'onglet spe pour g�rer les zones SPE si existant.
			try
			{
				var retour = dogexchangeSPE($(strId),trigger);
				if(retour == true)
				{
					setFocus ( TabFocusEntree , strId );
					return;
				}
				else if(retour == false)
				{
					return;
				}
			} catch(ex){trace("---dogexchangeSPE : " + ex);};
			break;
	}
	
    // Cette fonction est charg�e d'ex�cuter les trigger d'�change DOG		
    switch ( trigger)
    {
        case "GET": //Touche entr�e				
            //Faire le Get		
            retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , input , dogFct , "", dogexchangecallback)	
            return
            break ;
        case "AUTO": //Touche entr�e				
            //Faire le Get	
			retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , input , dogFct , "" , "" )  
            return
            break								 			
        case "F10": //Touche F10	
            retourExchange = sendExchangeF10 ( "<?=$dogExchangeF10?>" , input , dogFct , strId , "" , paramFiltre )
            return		
            break
    } //Selon le trigger a ex�cuter
} //Fin de la fonction dogexchange

function dogexchangecallback()
{
	if ( oldTrigger == "AUTO" )return 
    if ( Rsp[0] != "OK")
	{
		//Gestion du != OK pour l'onglet SPE 
		try{dogexchangecallbackSPE($(oldId),Rsp[0])} catch(ex){trace("---dogexchangecallbackSPE : "+ ex)};
		
		if(oldId == "DICT�codart" /*&&  $(oldId).value.trim() != "" */)
		{
			if(lectureSeule)
				return;
			
			if(!confirm("Nouvel Enregistrement ?"))
				return;
			
			tout_clear()
			
			if($(oldId).value.trim() == "" )
				if(!creation_new())
					return

			//Initialise champs pour nouvel article.
			initNewArticle();
				
			tout_enable(true)
			enableZone(oldId , false)
			loadOng('1')
			saisieEnCours = true
			Rowid = ""
			
			setFocus(TabFocusEntree , oldId)
		}
		else if(oldId == 'DICT�autsoc') 
		{
			//##337 10/08/16 - ajout GET sur les soci�t�s autoris�es.
			var oldValue = $(oldId).value;
			var value = "";
			
			//On enl�ve la soci�t� saisie.
			for(var i=0; i<oldValue.split(';').size()-1; i++)
			{
				if(value == "")
					value += oldValue.split(';')[i];
				else
					value += ";"+oldValue.split(';')[i];
			}
			
			alert(Rsp[1]);
			$(oldId).value = value;
		}
		else
		{
			if(Rsp[0] == "NODATA")
				alert("<?=lect_bin::getLabel ("error-nodata", "sl");?>")
				
			if(oldTrigger == "GET" && $(oldId).value.trim() == "")
			{
				sOldId = oldId.split("�")
				try {clearZone("DICTLINK�" + sOldId[1] + "�libel")}catch(e){}
				if(oldId == "DICT�segana")loadOng('5')
				if(oldId == "DICT�typetrp")loadOng('6')
				if(oldId == "DICT�codaux-princ")loadOng('7')
				if(oldId == "DICT�plaque")loadOng('9')
				setFocus(TabFocusEntree , oldId)
			}
				
		}
		return	
	}
	
	//Gestion du cas OK pour l'onglet SPE
	try{dogexchangecallbackSPE($(oldId),Rsp[0])} catch(ex){trace("---dogexchangecallbackSPE : "+ ex)};
	
    switch (oldId)
    {
		case "DICT�autsoc" :	
			//##337 10/08/16 - ajout GET sur les soci�t�s autoris�es.
			if(oldTrigger == 'GET')
			{
				var value = $(strId).value;
				var nbSoc = value.split(';').size();
				var soc = value.split(';')[nbSoc-1];
				var vecteur = [];
				
				//On r�cup�re l'ancienne liste.
				var newListe = "";
				
				for(var i=0; i<value.split(';').size()-1; i++)
				{
					if(newListe == "")
						newListe += value.split(';')[i];
					else
						newListe += ";"+value.split(';')[i];
				}
				
				//On r�cup�re l'ancien vecteur soci�t�.
				for(var i=0; i<value.split(';').size()-1; i++)
				{
					vecteur.push(value.split(';')[i]);
				}

				//On regarde si la soci�t� saisie n'est pas d�j� dans le vecteur.
				if(vecteur.indexOf(soc) != -1)
				{
					alert("Article d�j� pr�sent dans la liste.");
					
					$(oldId).value = newListe;
				}
				else
				{
					if(newListe == "")
						$(oldId).value = getValeurRsp('CODTAB');
					else
						$(oldId).value = newListe + ";"+getValeurRsp('CODTAB');
				}
			}
			else
			{
				if($(oldId).value == "")
					$(oldId).value = getValeurRsp('CODTAB');
				else
					$(oldId).value += ";"+getValeurRsp('CODTAB');
			}
		break;
        case "DICT�codart" :
		
			if(!lectureSeule)
				tout_enable(true);
			
			enableZone("DICT�codart",false);
			
			Rowid  = setValueFromKeyword  ( "ROWID" , false , "" , "" , true ) ;
			isinsaisie = false ;
			
			if(!lectureSeule)
				saisieEnCours = true;
			
			setValueFromKeyword ( "CODART"  , false , "" , "DICT�codart" , true );
			
			/************************ onglet 1 ****************************/
			// libel 1/2 
			setValueFromKeyword ( "LIBART_1"  , false , "" , "DICTLINK�libart_1�libel" , true );
			setValueFromKeyword ( "LIBART_1"  , false , "" , "DICT�libart_1" , true );
			setValueFromKeyword ( "LIBART_2"  , false , "" , "DICT�libart_2" , true );
			// libel abreg� 
			setValueFromKeyword ( "LIBABR"  , false , "" , "DICT�libabr" , true );
			// Type de libell�
			setValueFromKeyword ( "GESLIBEL"  , false , "" , "DICT�geslibel" , true );
			// Conditionnement
			setValueFromKeyword ( "condit"  , false , "" , "DICT�condit" , true );
			setValueFromKeyword ( "LIBEL"  , true , "condit" , "DICTLINK�condit�libel" , true );
			// Type de conditionnement
            setValueFromKeyword ( "typ-condt"  , false , "" , "DICT�typ-condt" , true );
            // Blocage de l'article
			setValueFromKeyword ( "CODBLOC"  , false , "" , "DICT�codbloc" , true );
			// Date blocage
			setValueFromKeyword ( "datebloc"  , false , "" , "DICT�datebloc" , true );
			/*
			R�f�renc� , Autoris� en Commande Vente , Autoris� en Commande Achat , Autoris� apr�s �puisement Stock 
			(onglet 1)
			*/
			setValueFromKeyword ( "codebloc_1"  , false , "" , "DICT�codebloc_1" , true );
			setValueFromKeyword ( "codebloc_2"  , false , "" , "DICT�codebloc_2" , true );
			setValueFromKeyword ( "codebloc_3"  , false , "" , "DICT�codebloc_3" , true );
			setValueFromKeyword ( "codebloc_4"  , false , "" , "DICT�codebloc_4" , true );
			setValueFromKeyword ( "auto-vtcpt"  , false , "" , "DICT�autovtcpt" , true );
			setValueFromKeyword ( "autotypcom"  , false , "" , "DICT�autotypcom" , true );
			// Taux d'abattement pour Retours
			setValueFromKeyword ( "TXABAT"  , false , "" , "DICT�txabat" , true );
			// Nombre de Jours pour les Retours
			setValueFromKeyword ( "NBJRET"  , false , "" , "DICT�nbjret" , true );
			// Saisie BL/Avoir
			setValueFromKeyword ( "BLAVOIR"  , false , "" , "DICT�blavoir" , true );
			// Article Carburant***/
			setValueFromKeyword ( "CARBU"  , false , "" , "DICT�carbu" , true );
			
			//Autorisation soci�t�s
			setValueFromKeyword("AUTSOC", false, "", "DICT�autsoc", true);
			/************************ onglet 1 ****************************/
			
			
			/************************ onglet 2 ****************************/
			// Unit� de Stock (onglet 2)
			setValueFromKeyword ( "UNISTO"  , false , "" , "DICT�unisto" , true );
			setValueFromKeyword ( "LIBEL"  , true , "UNISTO" , "DICTLINK�unisto�libel" , true );
			// Unit� de Commande (onglet 2)
			setValueFromKeyword ( "UNICDE"  , false , "" , "DICT�unicde" , true );
			setValueFromKeyword ( "LIBEL"  , true , "UNICDE" , "DICTLINK�unicde�libel" , true );
			// Coeff. Commande (onglet 2)
			setValueFromKeyword ( "COEFCDE"  , false , "" , "DICT�coefcde" , true );
			// Unit� de Facturation (onglet 2)
			setValueFromKeyword ( "UNIFAC"  , false , "" , "DICT�unifac" , true );
			setValueFromKeyword ( "LIBEL"  , true , "UNIFAC" , "DICTLINK�unifac�libel" , true );
			// Coeff. Facturation (onglet 2)
			setValueFromKeyword ( "COEFFAC"  , false , "" , "DICT�coeffac" , true );
			// Dose  Pr�conis�e / Hectare (onglet 2)
			setValueFromKeyword ( "UNIHEC"  , false , "" , "DICT�unihec" , true );
			// Dose Homologation Hectare (onglet 2)
			setValueFromKeyword ( "homhec"  , false , "" , "DICT�homhec" , true );		
			// Surconditionnement (onglet 2)
			setValueFromKeyword ( "SURCONDIT"  , false , "" , "DICT�surcondit" , true );
			// Type Surconditionnement (onglet 2)
			$("DICT�typ-surcond").value = setValueFromKeyword ( "TYP-SURCOND"  , false , "" , "" , true ).toLowerCase();
			/************************ onglet 2 ****************************/
			
			/************************ onglet 3 ****************************/
			// Code famille (onglet 3)
			setValueFromKeyword ( "FAMART"  , false , "" , "DICT�famart" , true );
			setValueFromKeyword ( "LIBEL"  , true , "FAMART" , "DICTLINK�famart�libel" , true );
			// Code sous famille (onglet 3)
			setValueFromKeyword ( "SOUFAM"  , false , "" , "DICT�soufam" , true );
			setValueFromKeyword ( "LIBEL"  , true , "SOUFAM" , "DICTLINK�soufam�libel" , true );
			// Code sous sous famille (onglet 3)
			setValueFromKeyword ( "SSSFAM"  , false , "" , "DICT�sssfam" , true );
			setValueFromKeyword ( "LIBEL"  , true , "SSSFAM" , "DICTLINK�sssfam�libel" , true );
			// Sous niveau 4 et 5**/
			setValueFromKeyword ( "SSSNV4"  , false , "" , "DICT�sssnv4" , true );
			setValueFromKeyword ( "LIBEL"  , true , "SSSNV4" , "DICTLINK�sssnv4�libel" , true );
			setValueFromKeyword ( "SSSNV5"  , false , "" , "DICT�sssnv5" , true );
			setValueFromKeyword ( "LIBEL"  , true , "SSSNV5" , "DICTLINK�sssnv5�libel" , true );
			// Code march�**/
			setValueFromKeyword ( "FAMART2"  , false , "" , "DICT�famart2" , true );
			setValueFromKeyword ( "LIBEL"  , true , "FAMART2" , "DICTLINK�famart2�libel" , true );
			setValueFromKeyword ( "SFAMART2"  , false , "" , "DICT�sfamart2" , true );
			setValueFromKeyword ( "LIBEL"  , true , "SFAMART2" , "DICTLINK�sfamart2�libel" , true );
			setValueFromKeyword ( "SSFAMART2"  , false , "" , "DICT�ssfamart2" , true );
			setValueFromKeyword ( "LIBEL"  , true , "SSFAMART2" , "DICTLINK�ssfamart2�libel" , true );
			// Type D�p�t (onglet 3)
			setValueFromKeyword ( "TYPE-MAG"  , false , "" , "DICT�type-mag" , true );
			// Type clientele
			setValueFromKeyword ( "TYPE-CLIENT"  , false , "" , "DICT�type-client" , true );
			// Produit jardin (onglet 3)
			setValueFromKeyword ( "jardin"  , false , "" , "DICT�jardin" , true );
			// Coefficient de Taxe (onglet 3)
			setValueFromKeyword ( "coeftaxe"  , false , "" , "DICT�coeftaxe" , true );
			// Code A.M.M (onglet 3)
			setValueFromKeyword ( "CODEAMM"  , false , "" , "DICT�codeamm" , true );
			// Article union 
			setValueFromKeyword ( "ART-UNION"  , false , "" , "DICT�art-union" , true );
			// Code douane (onglet 3)
			setValueFromKeyword ( "DOUANE"  , false , "" , "DICT�douane" , true );
			// Ancien Code 1/2 (onglet 3)
			setValueFromKeyword ( "anccod1"  , false , "" , "DICT�anccod1" , true );
			setValueFromKeyword ( "anccod2"  , false , "" , "DICT�anccod2" , true );
			// n�gatif (onglet 3)
			setValueFromKeyword ( "NEGATIF"  , false , "" , "DICT�negatif" , true );
			// Gamme article
			setValueFromKeyword ( "TYPART"  , false , "" , "DICT�typart" , true );
			// Code gencod (onglet 3)
			setValueFromKeyword ( "GENCOD"  , false , "" , "DICT�gencod" , true );
			// Code gtin14 (onglet 3)
			setValueFromKeyword ( "GTIN14"  , false , "" , "DICT�gtin14" , true );			
			// Mati�res Actives (onglet 3)
			setValueFromKeyword ( "MATACT"  , false , "" , "DICT�matact" , true );
			// Classification activit� 
			setValueFromKeyword ( "CLASSACT"  , false , "" , "DICT�classact" , true );
			// Nomenclature GV (onglet 3)
			setValueFromKeyword ( "nomencgv"  , false , "" , "DICT�nomencgv" , true );
			// Societe d'appartenance (onglet 3)
			setValueFromKeyword ( "SOC-APART"  , false , "" , "DICT�soc-apart" , true );
			// Cat�gorie 
			setValueFromKeyword ( "CATEGORIE"  , false , "" , "DICT�categorie" , true );
			// Traitement
			setValueFromKeyword ( "TRAITEMENT"  , false , "" , "DICT�traitement" , true );
			// Epandage
			setValueFromKeyword ( "EPANDAGE"  , false , "" , "DICT�epandage" , true );
			/************************ onglet 3 ****************************/
			
			/************************ onglet 4 ****************************/
			// Code Imputation Achats (onglet 4)
			setValueFromKeyword ( "IMPUTA"  , false , "" , "DICT�imputa" , true );
			setValueFromKeyword ( "LIBEL"  , true , "IMPUTA" , "DICTLINK�imputa�libel" , true );
			// Code Imputation Ventes (onglet 4)
			setValueFromKeyword ( "IMPUTV"  , false , "" , "DICT�imputv" , true );
			setValueFromKeyword ( "LIBEL"  , true , "IMPUTV" , "DICTLINK�imputv�libel" , true );
			// Code tva (onglet 4)
			setValueFromKeyword ( "CODTVA"  , false , "" , "DICT�codtva" , true );
			setValueFromKeyword ( "LIBEL"  , true , "CODTVA" , "DICTLINK�codtva�libel" , true );
			// Code tva pr�c�dent (onglet 4)
			setValueFromKeyword ( "TVAPRE"  , false , "" , "DICT�tvapre" , true );
			setValueFromKeyword ( "LIBEL"  , true , "TVAPRE" , "DICTLINK�tvapre�libel" , true );
			// Date Changement TVA (onglet 4)
			setValueFromKeyword ( "datchgtva"  , false , "" , "DICT�datchgtva" , true );
			// Frais sur achat (onglet4)
			setValueFromKeyword ( "FRAISACH"  , false , "" , "DICT�fraisach" , true );
			// Segment Analytique 1
			setValueFromKeyword ( "SEGANA"  , false , "" , "DICT�segana" , true );
			/************************ onglet 4 ****************************/
			
			/************************ onglet 5 ****************************/
			// Cat�gorie/numero Homologation (onglet 5)
			setValueFromKeyword ( "cathomo"  , false , "" , "DICT�cathomo" , true );
			setValueFromKeyword ( "nohomo"  , false , "" , "DICT�nohomo" , true );
			// Edition du Tarif (onglet 5)
			setValueFromKeyword ( "EDITAR"  , false , "" , "DICT�editar" , true );
			// Edition d'�tiquettes  (onglet 5)
			setValueFromKeyword ( "EDIETIQ"  , false , "" , "DICT�edietiq" , true );
			//
			setValueFromKeyword ( "PXVTE-MOD"  , false , "" , "DICT�pxvte-mod" , true );
			// Taux Cession Union
			setValueFromKeyword ( "TAUX-UNION"  , false , "" , "DICT�taux-union" , true );
			// Fourchette prix (onglet 5)
			setValueFromKeyword ( "FOURCHPX"  , false , "" , "DICT�fourchpx" , true );
			// Franco de port (onglet 5)
			setValueFromKeyword ( "FRANCO"  , false , "" , "DICT�franco" , true );
			// Type de transport (onglet 5)
			setValueFromKeyword ( "typetrp"  , false , "" , "DICT�typetrp" , true );
			/************************ onglet 5 ****************************/
			
			/************************ onglet 6 ****************************/
			// Article statistiques (onglet 6)
			setValueFromKeyword ( "CODART-STAT"  , false , "" , "DICT�codart-stat" , true );
            setValueFromKeyword ( "LIBEL"  , true , "CODART-STAT" , "DICTLINK�codart-stat�libel" , true );
			// Coefficient Statistique (onglet 6)
			setValueFromKeyword ( "COEF-STAT"  , false , "" , "DICT�coef-stat" , true );
			
			//Unit� statistiques
			setValueFromKeyword ( "UNITE-STAT"  , false , "" , "DICT�unite_stat" , true );
			
			// Code article rattach� (onglet 6)
			setValueFromKeyword ( "CODART-RAT"  , false , "" , "DICT�codart-rat" , true );
            setValueFromKeyword ( "LIBEL"  , true , "CODART-RAT" , "DICTLINK�codart-rat�libel" , true );
			// Article g�n�rique (onglet 6)
			setValueFromKeyword ( "GENERIQUE"  , false , "" , "DICT�generique" , true );
			// Code article de stock (onglet 6)
			setValueFromKeyword ( "CODART-STOC"  , false , "" , "DICT�codart-stoc" , true );
            setValueFromKeyword ( "LIBEL"  , true , "CODART-STOC" , "DICTLINK�codart-stoc�libel" , true );
			// Article Tarif  (onglet 6)
			setValueFromKeyword ( "CODART-TAR"  , false , "" , "DICT�codart-tar" , true );
            setValueFromKeyword ( "LIBEL"  , true , "CODART-TAR" , "DICTLINK�codart-tar�libel" , true );
			// Article Remises (onglet 6)
			setValueFromKeyword ( "CODART-REM"  , false , "" , "DICT�codart-rem" , true );
            setValueFromKeyword ( "LIBEL"  , true , "CODART-REM" , "DICTLINK�codart-rem�libel" , true );
			// Article li� (onglet 6)
			setValueFromKeyword ( "CODART-LIE"  , false , "" , "DICT�codart-lie" , true );
            setValueFromKeyword ( "LIBEL"  , true , "CODART-LIE" , "DICTLINK�codart-lie�libel" , true );
			// Art. rempl. 1 � 3
			setValueFromKeyword ( "codart-remp_1"  , false , "" , "DICT�codart-remp_1" , true );
            setValueFromKeyword ( "LIBEL"  , true , "codart-remp_1" , "DICTLINK�codart-remp_1�libel" , true );
			setValueFromKeyword ( "codart-remp_2"  , false , "" , "DICT�codart-remp_2" , true );
            setValueFromKeyword ( "LIBEL"  , true , "codart-remp_2" , "DICTLINK�codart-remp_2�libel" , true );
			setValueFromKeyword ( "codart-remp_3"  , false , "" , "DICT�codart-remp_3" , true );
            setValueFromKeyword ( "LIBEL"  , true , "codart-remp_3" , "DICTLINK�codart-remp_3�libel" , true );
			// Date Remplacement
			setValueFromKeyword ( "datchgart"  , false , "" , "DICT�datchgart" , true );
			// Article CVO (onglet 6)
			setValueFromKeyword ( "CODART-CVO"  , false , "" , "DICT�codart-cvo" , true );
			// Code cumul (onglet 6)
			setValueFromKeyword ( "CODE-CUMUL"  , false , "" , "DICT�code-cumul" , true );
            setValueFromKeyword ( "LIBEL"  , true , "CODE-CUMUL" , "DICTLINK�code-cumul�libel" , true );
			// Correspondance Apport (onglet 6)
			setValueFromKeyword ( "CORAPPORT"  , false , "" , "DICT�corapport" , true );
			// Cor. Apport Ech. Sem
			setValueFromKeyword ( "corechsem"  , false , "" , "DICT�corechsem" , true );
			// Article Suppl�mentation
			setValueFromKeyword ( "supplementation"  , false , "" , "DICT�supplementation" , true );
			// Article Soulte
			setValueFromKeyword ( "SOULTE"  , false , "" , "DICT�soulte" , true );
			// Fournisseur Fabricant (onglet 6)
			setValueFromKeyword ( "TYPAUX-FAB"  , false , "" , "DICT�typaux-fab" , true );
			setValueFromKeyword ( "CODAUX-FAB"  , false , "" , "DICT�codaux-fab" , true );
			setValueFromKeyword ( "LIBEL"  , true , "CODAUX-FAB" , "DICTLINK�codaux-fab�libel" , true );
			// Fournisseur Principal (onglet 6)
			setValueFromKeyword ( "TYPAUX-PRINC"  , false , "" , "DICT�typaux-princ" , true );
			setValueFromKeyword ( "CODAUX-PRINC"  , false , "" , "DICT�codaux-princ" , true );
			setValueFromKeyword ( "LIBEL"  , true , "CODAUX-PRINC" , "DICTLINK�codaux-princ�libel" , true );
			/************************ onglet 6 ****************************/
			
			/************************ onglet 7 ****************************/
			//Tenu en stock (onglet 7)
			setValueFromKeyword ( "GESSTOCK"  , false , "" , "DICT�gesstock" , true );
			//Tenu en lot (onglet 7)
			setValueFromKeyword ( "TENULOT"  , false , "" , "DICT�tenulot" , true );
			// Stock mini maxi (onglet 7)
			setValueFromKeyword ( "stock-min"  , false , "" , "DICT�stock-min" , true );
			setValueFromKeyword ( "stock-max"  , false , "" , "DICT�stock-max" , true );
			setValueFromKeyword ( "stock-mip"  , false , "" , "DICT�stock-mip" , true );
			// Mois Mini/Maxi du stock mini (onglet 7)
			setValueFromKeyword ( "moismini"  , false , "" , "DICT�moismini" , true );
			setValueFromKeyword ( "moismaxi"  , false , "" , "DICT�moismaxi" , true );
			// Stock alerte
			setValueFromKeyword ( "stock-alerte"  , false , "" , "DICT�stock-alerte" , true );
			// Poids Brut Unit� Vente (onglet 7)
			setValueFromKeyword ( "POIDS"  , false , "" , "DICT�poids" , true );
			// Volume (onglet 7)
			setValueFromKeyword ( "VOLUME"  , false , "" , "DICT�volume" , true );
			// Volume (onglet 7)
			setValueFromKeyword ( "GESTPDS"  , false , "" , "DICT�gestpds" , true );
			// Poids Brut/Net (onglet 7)
			setValueFromKeyword ( "POIDS-BRUT"  , false , "" , "DICT�poids-brut" , true );
			setValueFromKeyword ( "POIDS-NET"  , false , "" , "DICT�poids-net" , true );
			// Emplacement 1
			setValueFromKeyword ( "emplac"  , false , "" , "DICT�emplac" , true );
			// Stock de Fabrication (onglet 7)
			setValueFromKeyword ( "stock-fab"  , false , "" , "DICT�stock-fab" , true );
			// Stock dangereux (onglet 7)
			setValueFromKeyword ( "stock-dang"  , false , "" , "DICT�stock-dang" , true );
			// Recette Unique
			setValueFromKeyword ( "recette-unique"  , false , "" , "DICT�recette-unique" , true );
			// Article Nomenclatur�
			setValueFromKeyword ( "art-nomenc"  , false , "" , "DICT�art-nomenc" , true );
			/************************ onglet 7 ****************************/
			
			/************************ onglet 8 ****************************/
			// Classe danger 
			setValueFromKeyword ( "classe"  , false , "" , "DICT�classe" , true );
			// Classification
			setValueFromKeyword ( "classif"  , false , "" , "DICT�classif" , true );
			// Num�ro ONU 
			setValueFromKeyword ( "toxicite"  , false , "" , "DICT�toxicite" , true );
			// Point �clair (onglet 8)
			setValueFromKeyword ( "pointeclair"  , false , "" , "DICT�pointeclair" , true );
			// T� sens. froid (onglet 8)
			setValueFromKeyword ( "tempfroid"  , false , "" , "DICT�tempfroid" , true );
			// T� sens. chaud (onglet 8)
			setValueFromKeyword ( "tempchaud"  , false , "" , "DICT�tempchaud" , true );
			// N� cons. secu 
			setValueFromKeyword ( "consecu"  , false , "" , "DICT�consecu" , true );
			// Etiq. danger 1 � 5 (onglet 8)
			setValueFromKeyword ( "etiqdang_1"  , false , "" , "DICT�etiqdang_1" , true );
			setValueFromKeyword ( "etiqdang_2"  , false , "" , "DICT�etiqdang_2" , true );
			setValueFromKeyword ( "etiqdang_3"  , false , "" , "DICT�etiqdang_3" , true );
			setValueFromKeyword ( "etiqdang_4"  , false , "" , "DICT�etiqdang_4" , true );
			setValueFromKeyword ( "etiqdang_5"  , false , "" , "DICT�etiqdang_5" , true );
			// Cat. transport 
			setValueFromKeyword ( "cattrp"  , false , "" , "DICT�cattrp" , true );
			// Symbole danger 1 � 5 
			setValueFromKeyword ( "symbdang_1"  , false , "" , "DICT�symbdang_1" , true );
			setValueFromKeyword ( "symbdang_2"  , false , "" , "DICT�symbdang_2" , true );
			setValueFromKeyword ( "symbdang_3"  , false , "" , "DICT�symbdang_3" , true );
			setValueFromKeyword ( "symbdang_4"  , false , "" , "DICT�symbdang_4" , true );
			setValueFromKeyword ( "symbdang_5"  , false , "" , "DICT�symbdang_5" , true );
			// Etat physique
			setValueFromKeyword ( "etatphy"  , false , "" , "DICT�etatphy" , true );
			// Groupe d'emballage
			setValueFromKeyword ( "codemb"  , false , "" , "DICT�codemb" , true );
			// DescriPTION 1 � 6 (onglet 8)
			setValueFromKeyword ( "DESCRIP_1"  , false , "" , "DICT�descrip_1" , true );
			setValueFromKeyword ( "DESCRIP_2"  , false , "" , "DICT�descrip_2" , true );
			setValueFromKeyword ( "DESCRIP_3"  , false , "" , "DICT�descrip_3" , true );
			setValueFromKeyword ( "DESCRIP_4"  , false , "" , "DICT�descrip_4" , true );
			setValueFromKeyword ( "DESCRIP_5"  , false , "" , "DICT�descrip_5" , true );
			setValueFromKeyword ( "DESCRIP_6"  , false , "" , "DICT�descrip_6" , true );
			// Car. physique 1 � 8 (onglet 8)
			setValueFromKeyword ( "CARPHYS_1"  , false , "" , "DICT�carphys_1" , true );
			setValueFromKeyword ( "CARPHYS_2"  , false , "" , "DICT�carphys_2" , true );
			setValueFromKeyword ( "CARPHYS_3"  , false , "" , "DICT�carphys_3" , true );
			setValueFromKeyword ( "CARPHYS_4"  , false , "" , "DICT�carphys_4" , true );
			setValueFromKeyword ( "CARPHYS_5"  , false , "" , "DICT�carphys_5" , true );
			setValueFromKeyword ( "CARPHYS_6"  , false , "" , "DICT�carphys_6" , true );
			setValueFromKeyword ( "CARPHYS_7"  , false , "" , "DICT�carphys_7" , true );
			setValueFromKeyword ( "CARPHYS_8"  , false , "" , "DICT�carphys_8" , true );
			// Nom technique 1 � 4 (onglet 8)
			setValueFromKeyword ( "NOMTECH_1"  , false , "" , "DICT�nomtech_1" , true );
			setValueFromKeyword ( "NOMTECH_2"  , false , "" , "DICT�nomtech_2" , true );
			setValueFromKeyword ( "NOMTECH_3"  , false , "" , "DICT�nomtech_3" , true );
			setValueFromKeyword ( "NOMTECH_4"  , false , "" , "DICT�nomtech_4" , true );
			// Installation class�e
			setValueFromKeyword ( "insclasse"  , false , "" , "DICT�insclasse" , true );
			// Date FDS
			setValueFromKeyword ( "datefds"  , false , "" , "DICT�datefds" , true );
			// Cat�gorie 
			setValueFromKeyword ( "cat"  , false , "" , "DICT�cat" , true );
			// Grp emballage
			setValueFromKeyword ( "grpemb"  , false , "" , "DICT�grpemb" , true );
			// Phr. risques (onglet 8)
			setValueFromKeyword ( "RISQUE"  , false , "" , "DICT�risque" , true );
			// Phr. Sp�ciales
			setValueFromKeyword ( "speciale"  , false , "" , "DICT�speciale" , true );
			// Qt� limit�e  (onglet 8)
			setValueFromKeyword ( "QTELIM"  , false , "" , "DICT�qtelim" , true );
			// N� id. danger 4 (onglet 8)
			setValueFromKeyword ( "IDENT"  , false , "" , "DICT�ident" , true );
			// Code Plaques
			setValueFromKeyword ( "plaque"  , false , "" , "DICT�plaque" , true );
			/************************ onglet 8 ****************************/
			
			/************************ onglet 9 ****************************/
			// onglet 9
			setValueFromKeyword ( "ARTICOM"  , false , "" , "DICT�articom" , true );
			/************************ onglet 9 ****************************/
			
			/************************ bouton donnees societe **************/
			$('DICT�codblocFICHESOC').value 	= getValeurRsp('CODBLOC');
			$('DICT�codebloc_1FICHESOC').value 	= getValeurRsp('CODEBLOC_1');
			$('DICT�provenance').value 			= getValeurRsp('PROVENANCE');
			$('DICT�provenanceLib').value 		= getValeurRsp('LIBEL','PROVENANCE');
			$('DICT�gestpds').value 			= getValeurRsp('ARTICOS');
			/************************ bouton donnees societe **************/

			// GENCOD-BIP
			// TOXIQUE��LIBEL�
			// REMISE-EXCEP

			
			p = new RegExp( "��" , "gi" ) ;
			
			comment = setValueFromKeyword ( "ARTICOM" , false , "" , "" , true ) ;
			comment = comment.replace( p , "\r\n" ) ;
			$( "DICT�articom" ).value = comment ;
			
			comment = setValueFromKeyword ( "ARTICOS" , false , "" , "" , true ) ;
			comment = comment.replace( p , "\r\n" ) ;
			$( "DICT�comSociete" ).value = comment ;
			
			for( ind_1 = 1 ; ind_1 <= 5 ; ind_1++ )
			{
			
				for( ind_2 = 1 ; ind_2 <= 5 ; ind_2++ )	
				{  
					switch( ind_2 )
					{
						case 1 : 
							$( "DICT�bc-comment_" + ind_1 ).value = setValueFromKeyword( "COMM-ETAT_" + ind_1 + ind_2 , false , "" , "" , true ) ;
							if ( $( "DICT�bc-comment_" + ind_1 ).value == "" ) $( "DICT�bc-comment_" + ind_1 ).value = "N" ;
							break ;
						case 2 : 
							$( "DICT�bp-comment_"  + ind_1 ).value = setValueFromKeyword( "COMM-ETAT_" + ind_1 + ind_2 , false , "" , "" , true ) ;
							if ( $( "DICT�bp-comment_" + ind_1 ).value == "" ) $( "DICT�bp-comment_" + ind_1 ).value = "N" ;
							break ;
						case 3 : 
							$( "DICT�bl-comment_"  + ind_1 ).value = setValueFromKeyword( "COMM-ETAT_" + ind_1 + ind_2 , false , "" , "" , true ) ;
							if ( $( "DICT�bl-comment_" + ind_1 ).value == "" ) $( "DICT�bl-comment_" + ind_1 ).value = "N" ;
							break ;
						case 4 : 
							$( "DICT�fac-comment_" + ind_1 ).value = setValueFromKeyword( "COMM-ETAT_" + ind_1 + ind_2 , false , "" , "" , true ) ;
							if ( $( "DICT�fac-comment_" + ind_1 ).value == "" ) $( "DICT�fac-comment_" + ind_1 ).value = "N" ;
							break ;
						case 5 : 
							$( "DICT�sai-comment_" + ind_1 ).value = setValueFromKeyword( "COMM-ETAT_" + ind_1 + ind_2 , false , "" , "" , true ) ;
							if ( $( "DICT�sai-comment_" + ind_1 ).value == "" ) $( "DICT�sai-comment_" + ind_1 ).value = "N" ;
							break ;
					}
				}
				if ( $( "DICT�bc-comment_"  + ind_1 ).value == "N" &&
					 $( "DICT�bp-comment_"  + ind_1 ).value == "N" &&
					 $( "DICT�bl-comment_"  + ind_1 ).value == "N" &&
					 $( "DICT�fac-comment_" + ind_1 ).value == "N" &&
					 $( "DICT�sai-comment_" + ind_1 ).value == "N" )
				{
					enableZone( "DICT�txt-comment_" + ind_1 , false)
				}
				else
				{
					comment = setValueFromKeyword( "ARTICOMX_" + ind_1 , false , "" , "" , true ) ;
					comment = comment.replace( p , "\r\n" ) ;
					$( "DICT�txt-comment_" + ind_1 ).value = comment ;
					enableZone( "DICT�txt-comment_" + ind_1 , true)
				}
			}
			
			try{chargerOngletSPE();} catch(ex){trace("---chargerOngletSPE() : " + ex)};
			
			ChargerImage("divImage1")
			loadOng('1')

            if ( "<?=$n_etat_visu?>" == 1 ) { tout_enable(false) ; }
			break ;
  															
        case "DICT�condit" : 
            setValueFromKeyword ( "CONDIT"  , false , "" , "DICT�condit"  , false);
			setValueFromKeyword ( "LIBEL"  , false , "" , "DICTLINK�condit�libel"  , true);
           break;

        case "DICT�type-client" :  
            setValueFromKeyword ( "CODTAB"  , false , "" , "DICT�type-client"  , false);
			setValueFromKeyword ( "LIBEL1-1"  , false , "" , "DICTLINK�type-client�libel"  , true);
			break;

         case "DICT�typetrp" :  
            setValueFromKeyword ( "CODTAB"  , false , "" , "DICT�typetrp"  , false);
			setValueFromKeyword ( "LIBEL1-1"  , false , "" , "DICTLINK�typetrp�libel"  , true);
			break;

         case "DICT�matact" :
			 if(oldTrigger=="F10"){
				if($(oldId).value.trim()=="")
					$(oldId).value = setValueFromKeyword ( "codtab"  , false , "" , ""  , false);	
				else
					$(oldId).value = $(oldId).value + "," + setValueFromKeyword ( "codtab"  , false , "" , ""  , false);				
			}
			$(oldId).value = $(oldId).value.replace(",,",",")
			
           break;

		// Code classification activit� du produit
        case "DICT�classact" :
			setValueFromKeyword("CODTAB",   false, "", "DICT�classact", false);
			setValueFromKeyword("LIBEL1-1", false, "", "DICTLINK�classact�libel", true);
            break;

	    case "DICT�unisto":  
			tabtmp = getDataArrayFromRow(1);
			$("DICTLINK�unisto�libel").value = getValueFromDataArray(tabtmp,"libel",false,"");
			$("DICT�unisto").value = getValueFromDataArray(tabtmp,"TYPQTE",false,"");
				
			//mettre a 1 par defaut les coeffs
			if ($("DICT�coefcde").value=="" ) $("DICT�coefcde").value=1;

			if ($("DICT�coeffac").value=="")  $("DICT�coeffac").value=1;

			if ($("DICT�surcondit").value=="")  $("DICT�surcondit").value=1;
  			
			if ($("DICT�unicde").value =="")
			{
				$("DICT�unicde").value =getValueFromDataArray(tabtmp,"TYPQTE",false,"");
				$("DICTLINK�unicde�libel").value = getValueFromDataArray(tabtmp,"libel",false,"");
			}
			 
			if ($("DICT�unifac").value =="")
			{
				$("DICT�unifac").value =getValueFromDataArray(tabtmp,"TYPQTE",false,"");
				$("DICTLINK�unifac�libel").value = getValueFromDataArray(tabtmp,"libel",false,"");
			}	
			break;
			
		case "DICT�unicde" : 
			tabtmp = getDataArrayFromRow(1);
			$("DICTLINK�unicde�libel").value = getValueFromDataArray(tabtmp,"libel",false,"");
			$("DICT�unicde").value = getValueFromDataArray(tabtmp,"TYPQTE",false,"");
			break;
			
		case "DICT�unifac" :
			tabtmp = getDataArrayFromRow(1);
			$("DICTLINK�unifac�libel").value = getValueFromDataArray(tabtmp,"libel",false,"");
			$("DICT�unifac").value = getValueFromDataArray(tabtmp,"TYPQTE",false,"");
			break;
			
		case "DICT�coeffac":
			tabtmp = getDataArrayFromRow(1);
			$("DICT�coeffac").value = getValueFromDataArray(tabtmp,"coeffac",false,"");
			break;
			
		case "DICT�surcondit":
			tabtmp = getDataArrayFromRow(1);
			$("DICT�surcondit").value = getValueFromDataArray(tabtmp,"surcondit",false,"");
			break;
			
		case "DICT�typ-surcond":
			tabtmp = getDataArrayFromRow(1);
			$("DICT�typ-surcond").value = getValueFromDataArray(tabtmp,"typ-surcond",false,"");
		    break;
			  	
		case "DICT�code-cumul" :
            setValueFromKeyword ( "CODTAB"    , false , "" , "DICT�code-cumul"  , false);
            setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�code-cumul�libel"  , true);
            break;
		
		case "DICT�codart-stat" : 
            setValueFromKeyword ( "CODART"  , false , "" , "DICT�codart-stat"  , false);
			setValueFromKeyword("LIBART_1",false,"","DICTLINK�codart-stat�libel",true);
		    break ;
			 
		case "DICT�codart-stoc" :
			setValueFromKeyword ( "CODART"  , false , "" , "DICT�codart-stoc"  , false);
			setValueFromKeyword("LIBART_1",false,"","DICTLINK�codart-stoc�libel",true);			   		 
			break ;
			 
		case "DICT�codart-tar" :
			setValueFromKeyword ( "CODART"  , false , "" , "DICT�codart-tar"  , false);
			setValueFromKeyword("LIBART_1",false,"","DICTLINK�codart-tar�libel",true);	 		 
			break ;	
			 
		case "DICT�codart-rem" : 
			setValueFromKeyword ( "CODART"  , false , "" , "DICT�codart-rem"  , false);
			setValueFromKeyword("LIBART_1",false,"","DICTLINK�codart-rem�libel",true);	   		 
			break ;
			 
		case "DICT�codart-lie" : 
			 setValueFromKeyword ( "CODART"  , false , "" , "DICT�codart-lie"  , false);
			setValueFromKeyword("LIBART_1",false,"","DICTLINK�codart-lie�libel",true);   		 
			 break ;
			 
		case "DICT�codart-remp_1" : 
			 setValueFromKeyword ( "CODART"  , false , "" , "DICT�codart-remp_1"  , false);
			setValueFromKeyword("LIBART_1",false,"","DICTLINK�codart-remp_1�libel",true);	    		 
			 break ;

		case "DICT�codart-remp_2" : 
		    setValueFromKeyword ( "CODART"  , false , "" , "DICT�codart-remp_2"  , false);
			setValueFromKeyword("LIBART_1",false,"","DICTLINK�codart-remp_2�libel",true);  		 
			 break ;	

		case "DICT�codart-remp_3" : 
		    setValueFromKeyword ( "CODART"  , false , "" , "DICT�codart-remp_3"  , false);
			setValueFromKeyword("LIBART_1",false,"","DICTLINK�codart-remp_3�libel",true);	     		 
			 break ;

        case "DICT�codart-rat" : 
			setValueFromKeyword ( "CODART"  , false , "" , "DICT�codart-rat"  , false);
			setValueFromKeyword("LIBART_1",false,"","DICTLINK�codart-rat�libel",true);	    		 
			break ;  	   
			
        case "DICT�codart-cvo" : 
			setValueFromKeyword ( "CODART"  , false , "" , "DICT�codart-cvo"  , false);
			setValueFromKeyword("LIBART_1",false,"","DICTLINK�codart-cvo�libel",true);	    		 
			break ;  
			
		case "DICT�typaux-fab" : 
			setValueFromKeyword ( "TYPAUX"  , false , "" , "DICT�typaux-fab"  , false);
            $("DICT�codaux-fab").value ="";
			$("DICTLINK�codaux-fab�libel").value =""; 
			break ;
			 
        case "DICT�codaux-fab" :
            setValueFromKeyword ( "CODAUXGES"  , false , "" , "DICT�codaux-fab"  , false);
			setValueFromKeyword ( "ADRESSE_1"  , false , "" , "DICTLINK�codaux-fab�libel"  , true);
			break ;
			 
		case "DICT�typaux-princ" :
			setValueFromKeyword ( "TYPAUX"  , false , "" , "DICT�typaux-princ"  , false);
			$("DICT�codaux-princ").value ="";
			$("DICTLINK�codaux-princ�libel").value ="";  	 
			break ;
			 
		case "DICT�codaux-princ" : 
			setValueFromKeyword ( "ADRESSE_1"  , false , "" , "DICTLINK�codaux-princ�libel"  , true);
			setValueFromKeyword ( "CODAUXGES"  , false , "" , "DICT�codaux-princ"  , false);
			loadOng('7')
			break ;

		case "DICT�typart" : 
			setValueFromKeyword ( "CODTAB"    , false , "" , "DICT�typart"  , false);
			setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�typart�libel"  , true);
			break;

        case "DICT�categorie" : 
             setValueFromKeyword ( "CODTAB"    , false , "" , "DICT�categorie"  , false);
             setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�categorie�libel"  , true);
            break;

        case "DICT�traitement" :
             setValueFromKeyword ( "CODTAB"    , false , "" , "DICT�traitement"  , false);
             setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�traitement�libel"  , true);
            break;


        case "DICT�famart" : 
			setValueFromKeyword ( "FAMART"  , false , "" , "DICT�famart"  , false);
			setValueFromKeyword ( "LIBEL"  , false , "" , "DICTLINK�famart�libel"  , true);
			if (setValueFromKeyword ( "CODTVA"  , false , "" , ""  , true) != "") { 
				setValueFromKeyword ( "CODTVA"  , false , "" , "DICT�codtva"  , false);
				setValueFromKeyword ( "LIBEL"  , true , "CODTVA" , "DICTLINK�codtva�libel"  , true);
			}
			if (setValueFromKeyword ( "IMPUTA"  , false , "" , ""  , true) != "") {        
				setValueFromKeyword ( "IMPUTA"  , false , "" , "DICT�imputa"  , false);
				setValueFromKeyword ( "LIBEL"  , true , "IMPUTA" , "DICTLINK�imputa�libel"  , true);
			}
			if (setValueFromKeyword ( "IMPUTV"  , false , "" , ""  , true) != "") { 
				setValueFromKeyword ( "IMPUTV"  , false , "" , "DICT�imputv"  , false);
				setValueFromKeyword ( "LIBEL"  , true , "IMPUTV" , "DICTLINK�imputv�libel"  , true);
			}
			break ;

        case "DICT�soufam" : 
			setValueFromKeyword ( "SOUFAM"  , false , "" , "DICT�soufam"  , false);
			setValueFromKeyword ( "LIBEL"  , false , "" , "DICTLINK�soufam�libel"  , true);
			if (setValueFromKeyword ( "CODTVA"  , false , "" , ""  , true) != "") { 
				setValueFromKeyword ( "CODTVA"  , false , "" , "DICT�codtva"  , false);
				setValueFromKeyword ( "LIBEL"  , true , "CODTVA" , "DICTLINK�codtva�libel"  , true);
			}
			if (setValueFromKeyword ( "IMPUTA"  , false , "" , ""  , true) != "") {        
				setValueFromKeyword ( "IMPUTA"  , false , "" , "DICT�imputa"  , false);
				setValueFromKeyword ( "LIBEL"  , true , "IMPUTA" , "DICTLINK�imputa�libel"  , true);
			}
			if (setValueFromKeyword ( "IMPUTV"  , false , "" , ""  , true) != "") { 
				setValueFromKeyword ( "IMPUTV"  , false , "" , "DICT�imputv"  , false);
				setValueFromKeyword ( "LIBEL"  , true , "IMPUTV" , "DICTLINK�imputv�libel"  , true);
			}
			break ;

        case "DICT�sssfam" : 
			setValueFromKeyword ( "SSSFAM"  , false , "" , "DICT�sssfam"  , false);
			setValueFromKeyword ( "LIBEL"  , false , "" , "DICTLINK�sssfam�libel"  , true);
			if (setValueFromKeyword ( "CODTVA"  , false , "" , ""  , true) != "") { 
			setValueFromKeyword ( "CODTVA"  , false , "" , "DICT�codtva"  , false);
			setValueFromKeyword ( "LIBEL"  , true , "CODTVA" , "DICTLINK�codtva�libel"  , true);
			}
			if (setValueFromKeyword ( "IMPUTA"  , false , "" , ""  , true) != "") {        
				setValueFromKeyword ( "IMPUTA"  , false , "" , "DICT�imputa"  , false);
				setValueFromKeyword ( "LIBEL"  , true , "IMPUTA" , "DICTLINK�imputa�libel"  , true);
			}
			if (setValueFromKeyword ( "IMPUTV"  , false , "" , ""  , true) != "") { 
				setValueFromKeyword ( "IMPUTV"  , false , "" , "DICT�imputv"  , false);
				setValueFromKeyword ( "LIBEL"  , true , "IMPUTV" , "DICTLINK�imputv�libel"  , true);
			}
			break ;
			
        case "DICT�sssnv4" : 
			setValueFromKeyword ( "SSSNV4"  , false , "" , "DICT�sssnv4"  , false);
			setValueFromKeyword ( "LIBEL"  , false , "" , "DICTLINK�sssnv4�libel"  , true);
			break;

		case "DICT�sssnv5" :
			setValueFromKeyword ( "SSSNV5"  , false , "" , "DICT�sssnv5"  , false);
			setValueFromKeyword ( "LIBEL"  , false , "" , "DICTLINK�sssnv5�libel"  , true);
			break;
			
        case "DICT�codtva" :
			setValueFromKeyword ( "CODTVA"  , false , "" , "DICT�codtva"  , false);
			setValueFromKeyword ( "LIBEL"  , false , "" , "DICTLINK�codtva�libel"  , true);
			break ;

        case "DICT�tvapre" : 
			setValueFromKeyword ( "CODTVA"  , false , "" , "DICT�tvapre"  , false);
			setValueFromKeyword ( "LIBEL"  , false , "" , "DICTLINK�tvapre�libel"  , true);
			break ;

        case "DICT�imputa" :
			setValueFromKeyword ( "CODIMP"  , false , "" , "DICT�imputa"  , false);
			setValueFromKeyword ( "LIBEL"  , false , "" , "DICTLINK�imputa�libel"  , true);
			break ;	

        case "DICT�imputv" : 
                setValueFromKeyword ( "CODIMP" , false , "" , "DICT�imputv"  , false);
                setValueFromKeyword ( "LIBEL"  , false , "" , "DICTLINK�imputv�libel"  , true);
                break ;

        case "DICT�famart2" :
                setValueFromKeyword ( "CODTAB"   , false , "" , "DICT�famart2"  , false);
                setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�famart2�libel"  , true);
                break ;

        case "DICT�sfamart2" : 
                setValueFromKeyword ( "CODTAB"   , false , "" , "DICT�sfamart2"  , false);
                setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�sfamart2�libel"  , true);
                break ;

        case "DICT�ssfamart2" :
                setValueFromKeyword ( "CODTAB"   , false , "" , "DICT�ssfamart2"  , false);
                setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�ssfamart2�libel"  , true);
                break ;

        case "DICT�segana" : 
                setValueFromKeyword ( "SEGANA"   , false , "" , "DICT�segana"  , false);
                setValueFromKeyword ( "LIBSEGANA" , false , "" , "DICTLINK�segana�libel"  , true);
                loadOng('5')
				break ;

        case "DICT�typetrp" :
                setValueFromKeyword ( "CODTAB"   , false , "" , "DICT�typetrp"  , false);
                setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�typetrp�libel"  , true);
				loadOng('6')
                break ;

        case "DICT�typ-condt" :  
                setValueFromKeyword ( "CODTAB"   , false , "" , "DICT�typ-condt"  , false);
                setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�typ-condt�libel"  , true);
                break ;

        case "DICT�classe" :
			setValueFromKeyword ( "CODTAB"   , false , "" , "DICT�classe"  , false);
			setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�classe�libel"  , true);   
			break;

		case "DICT�classif" :
			setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�classif"  , false);
			setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�classif�libel"  , true);    
        	break;
			
        case "DICT�toxicite" :
			setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�toxicite"  , false);
			setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�toxicite�libel"  , true);   
			break;

		case "DICT�consecu" :
			setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�consecu"  , false);
			setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�consecu�libel"  , true);		
        	break;

		case "DICT�cattrp" :
			setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�cattrp"  , false);
			setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�cattrp�libel"  , true);   	
        	break;
			
         case "DICT�symbdang_1": 
                setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�symbdang_1"  , false);
			    setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�symbdang_1�libel"  , true);
            break;
         case "DICT�symbdang_2": 
                setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�symbdang_2"  , false);
			    setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�symbdang_2�libel"  , true);
             break;

         case "DICT�symbdang_3": 
                setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�symbdang_3"  , false);
			    setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�symbdang_3�libel"  , true);
            break;

        case "DICT�symbdang_4": 
                setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�symbdang_4"  , false);
			    setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�symbdang_4�libel"  , true);
            break;

        case "DICT�symbdang_5":
                setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�symbdang_5"  , false);
			    setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�symbdang_5�libel"  , true);   
        	break;

        case "DICT�etatphy":
			setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�etatphy"  , false);
			setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�etatphy�libel"  , true);	
        	break;

        case "DICT�codemb": 
			setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�codemb"  , false);
			setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�codemb�libel"  , true);   	
        	break;

        case "DICT�insclasse": 
			setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�insclasse"  , false);
			setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�insclasse�libel"  , true);	
        	break;
        case "DICT�cat": 
			setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�cat"  , false);
			setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�cat�libel"  , true);   	
        	break;
        case "DICT�grpemb": 
			setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�grpemb"  , false);
			setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�grpemb�libel"  , true);   	
        	break;
        case "DICT�risque": 
			if ( $(strId).value == "" ) {
                $(strId).value = setValueFromKeyword("codtab",false,"","",true);
            }
            else {
             m = NumEntries( $(strId).value.trim()  , ",")  

             if ( Entry( m ,$(strId).value.trim() , "," ) ==  "" ) {
                 $(strId).value += setValueFromKeyword("codtab",false,"","",true);
             }
             else $(strId).value += ","+ setValueFromKeyword("codtab",false,"","",true);
            }

        	break;

       case "DICT�speciale" : 
			setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�speciale"  , false);
			setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�speciale�libel"  , true); 	
        	break;

       case "DICT�plaque": 
			setValueFromKeyword ( "CODTAB"   , false , "" ,  "DICT�plaque"  , false);
			setValueFromKeyword ( "LIBEL1-1" , false , "" , "DICTLINK�plaque�libel"  , true);			
        	loadOng('9')
			break;
			
        case "DICT�txt-comment_1":  
			comment = setValueFromKeyword("MESSAGE", false, "", "", true).replace("chr(10)", "\r\n")
            $("DICT�txt-comment_1").value = $("DICT�txt-comment_1").value + comment;
		 	break;

        case "DICT�txt-comment_2":  
            comment = setValueFromKeyword("MESSAGE", false, "", "", true).replace("chr(10)", "\r\n")
            $("DICT�txt-comment_2").value = $("DICT�txt-comment_2").value + comment;
            break;

        case "DICT�txt-comment_3":  
            comment = setValueFromKeyword("MESSAGE", false, "", "", true).replace("chr(10)", "\r\n")
            $("DICT�txt-comment_3").value = $("DICT�txt-comment_3").value + comment;
            break;

        case "DICT�txt-comment_4":  
            comment = setValueFromKeyword("MESSAGE", false, "", "", true).replace("chr(10)", "\r\n")
            $("DICT�txt-comment_4").value = $("DICT�txt-comment_4").value + comment;
            break;

        case "DICT�txt-comment_5":  
            comment = setValueFromKeyword("MESSAGE", false, "", "", true).replace("chr(10)", "\r\n")
            $("DICT�txt-comment_5").value = $("DICT�txt-comment_5").value + comment;
            break;
			
		case 'DICT�provenance':
			$(oldId).value = getValeurRsp('CODTAB');
			$(oldId+'Lib').value = getValeurRsp('LIBEL');
			break;
	}

	
	
	setFocus ( TabFocusEntree , oldId )	
} //Fin de la fonction dogexchange

/**
	Fonction initialise diff�rents champs pour pr�parer la saisie d'un nouvel article.
**/
function initNewArticle()
{
	$('DICT�generique').value = 'N'; 		//NON
	$('DICT�codart-cvo').value = '0'; 		//NON
	$('DICT�gesstock').value = '1';			//Oui tenu en stock
	
	UpdateArticl('', true)
}

function tout_enable(motcle)
{
	enableZone("ALL" , motcle , TabAllOnglets)
}

function tout_clear()
{
	clearZone("ALL" , TabAllOnglets)
	clearZone("ALL" , TabAllLibOnglets)
}

function lancerecherche () { 
	 if ( $('DICT�codart').value != "" )
	 {
	    str_val  = "CODSOC�"+codsoc+"�" 
		
		str_val += "CODART�" + $('DICT�codart').value
        retourExchange = sendExchangeInvoke ( "<?=$dogExchange?>" , str_val , "GET-ARTICL�n-articl" , "", "")	
		//Si on a ramener quelque chose on charge le tableau//
		//on sauvegarde le resultat
		RspSave=Rsp;
		if (retourExchange != "OK") return ;        
	 }
	
	//Raz de la variable
	Rsp = "" ;
	//On ouvre les zones
	loadOng('1')
} //Fin de la fonction de recherche d'un bon

function DeleteArticl()
{	
	//On appel la fonction saisieOKSpe() qui v�rifie qu'il n'y a aucune saisie dans l'onglet SPE 
	//(si la fonction existe)
	try{
		if(!saisieOKSpe())
			return false;
	}catch(ex){trace("---saisieOKSpe : " + ex)}
	
	// fonction de mise a jour de l'article 
	var reponse = "";
	
	// si pas d'article charg� on se casse 
	if ($("DICT�codart").value == "") 
		 return;

	 codarticl              = $("DICT�codart").value ;
	 inputParameter         = "ROWID�" + Rowid + "�CODSOC�" + codsoc + "�CODART�" + $("DICT�codart").value + "�DOGADMIN�3" ;
	 paramFonction          = "MAJ-GESARTICL�n-gesarticl" ;
	 // Vous �tes sur le point de supprimer l'article    Voulez-vous continuer ?
	if(confirm("<?=lect_bin::getLabel ( "mess-gesartnat_4" ,"sl" )?> "+$("DICT�codart").value+"\n"+"<?=lect_bin::getLabel ( "mess-gesartnat_5" ,"sl" )?>"))
	{
		//Faire la MAJ
		retourExchange = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter ,paramFonction , "" , "") ;
		// controler rsp pour savoir si delete ok
		if (retourExchange != "OK") 
			$("DICT�codart").value = codarticl ;					 
		else 
		    // L'article suivant est supprim� : 
			alert("<?=lect_bin::getLabel ( "mess-gesartnat_7" ,"sl" )?> "+$("DICT�codart").value );
			RazGen () ;	
	}
	$("DICT�codart").focus();
} // fin function delete article

function creation_new()
{
	//test si on se trouve en visu
	if ( "<?=$n_etat_visu?>" == 1 )
	{
		return false;
	}
	else
	{
		str_val = "�CODSOC�" + codsoc;

		retourExchange = sendExchangeInvoke( "<?=$dogExchange?>" , str_val  ,"INC-ARTICL�n-GESARTICL" , "" , "") ;
		
		if ( retourExchange != "OK" ) 
			return false;
		
		setValueFromKeyword("codart",false,"","DICT�codart",true);
		
		return true
	}					
}

/**
	Mise � jour de l'article.
	newArticle : bool�en TRUE => cr�ation nouvel article.
	
	newArticle = true si on a cr�� un nouvel article qui n'a pas �t� enregistr�.
	on sauvegarde les champs modifi� � loa cr�ation de l'article. (fct initNewArticle)
**/
function UpdateArticl(type, newArticle)
{
	trace("--UpdateArticl type: "+type+" - newArticle: "+newArticle+" --")
	
	//On appel la fonction saisieOKSpe() qui v�rifie qu'il n'y a aucune saisie dans l'onglet SPE 
	//(si la fonction existe)
	try{
		if(!saisieOKSpe())
			return false;
	}catch(ex){trace("---saisieOKSpe : " + ex)}
	
	
	
	if(newArticle == undefined)
		newArticle = false;
	
	//Si on est en lecture seule => pas de validation.
	//SAUF si on saisie l'onglet "Donnees societes"
	if ( "<?=$n_etat_visu?>" == 1 && !isSaisieFicheSoc) 
	{
		alert("ICI")
		return;
	}
	
	
    // si pas d'article charg� pas de MAJ 
	if ($("DICT�codart").value == "") return;

    // Si le code gtin14 n'est pas correct, pas de MAJ
    if (!rechGtin()) return;

    typetrt = type ;
	
	remplir_variable_mot_cle();

	
	if(!newArticle)
	{
		if ( unisto.trim() == "" || unicde.trim() == "" || unifac.trim() == ""  )
		{
		   alert ("<?=lect_bin::getLabel ( "mess-gesartnat_27" ,"sl" )?>");
		   return;
		}
		else
		{  
		   if (famart.trim() =="")
			{
			   // La famille dans le pav� nomenclature est une zone obligatoire !
			   alert ("<?=lect_bin::getLabel ( "mess-gesartnat_8" ,"sl" )?>");
			   return;
			}
			else
			{
			   if (soufam.trim() =="")
				{
				   // La sous-famille dans le pav� nomenclature est une zone obligatoire !
				   alert ("<?=lect_bin::getLabel ( "mess-gesartnat_9" ,"sl" )?>");
				   return;
				}
				else
				{
				   if (codtva.trim() =="")
					{
					   // Le code TVA dans le pav� nomenclature est une zone obligatoire !
					   alert ("<?=lect_bin::getLabel ( "mess-gesartnat_10" ,"sl" )?>");
					   return;
					}
					else
					{
					   if (imputa.trim() =="")
						{
						   // Le code imputation achat dans le pav� nomenclature est une zone obligatoire !
						   alert ("<?=lect_bin::getLabel ( "mess-gesartnat_11" ,"sl" )?>");
						   return;
						}
						else
						{
							if (gesstock.trim() =="")
							{
							   // La tenue en stock dans le pav� libell�s est une zone obligatoire !
							   alert ("<?=lect_bin::getLabel ( "mess-gesartnat_12" ,"sl" )?>");
							   return;
							}	
						}
					}
				}
			}
		}
	}
	


	codart = $("DICT�codart").value;

    	//Initialiser les variables d'option des �changes
	inputParameter          = "" ;
	paramFonction           = "MAJ-GESARTICL�n-gesarticl" ;
	
	if ( Rowid != "" )   inputParameter = "ROWID�" + Rowid + "�";

	inputParameter += "CODSOC�" + codsoc + "�CODART�" + $("DICT�codart").value + "�libart_1�" + libart_1 + "�libart_2�" +libart_2 + "�geslibel�"+ geslibel + " �condit�" + condit + "�generique�" + generique + "�typart�" +typart +  "�type-mag�" +typemag + "�jardin�" + jardin ;
	
	inputParameter += "�unisto�" + unisto +  "�type-client�" +typeclient + "�matact�" + matact + "�classact�" + classact + "�nomencgv�" + nomencgv + "�coeftaxe�" + coeftaxe + "�art-union�" + artunion  + "�coef-stat�" + coefstat  + "�UNITE-STAT�" + uniteStat;
	inputParameter += "�unicde�" + unicde ;
	inputParameter += "�coefcde�" + coefcde ;
	inputParameter += "�unifac�" + unifac + "�coeffac�" + coeffac + "�surcondit�" + surcondit + "�typ-surcond�" ;
	inputParameter += typsurcond + "�unihec�" + unihec + "�homhec�" + homhec + "�douane�" + douane + "�anccod1�" + anccod1 + "�anccod2�" + anccod2 ;
	
	//autorisation soci�t�s
	inputParameter += "�autsoc�"  + autsoc;
	inputParameter += "�famart�"+famart+"�soufam�"+soufam+"�sssfam�"+sssfam+"�sssnv4�"+sssnv4+"�sssnv5�"+sssnv5+"�codtva�"+codtva+"�tvapre�"+tvapre+"�datchgtva�"+datchgtva+"�imputa�"+imputa+"�imputv�"+imputv+"�fraisach�"+fraisach ;
	inputParameter += "�pun-cagn�"+puncagn+"�deb-cagn�"+debcagn+"�fin-cagn�"+fincagn
	inputParameter += "�famart2�"+famart2+"�sfamart2�"+sfamart2+"�ssfamart2�"+ssfamart2+"�negatif�" + negatif +"�gencod�" + gencod  + "�gtin14�" + gtin14 + "�codeamm�" + codeamm + "�soc-apart�" + socapart  + "�categorie�" + categorie  + "�traitement�" + traitement + "�epandage�" + epandage;
	
	inputParameter += "�poids�"+poidsuv+"�poids-brut�"+poidsbrut+"�poids-net�"+poidsnet+"�volume�"+volume+"�gestpds�"+gestpds+"�taux-union�"+tauxunion+"�emplac�"+emplac+"�stock-min�"+stockmin+"�stock-max�"+stockmax+"�stock-alerte�"+stockalert+"�qte-ecocde�"+qteecocde ;
	//inputParameter += "�typauxpa�"+typauxpa+"�codaux-pa�"+codauxpa+"�date�"+datearticl+"�prix-pa�"+prixpa+"�devise�"+devise;
	
	inputParameter += "�codart-stat�"+codartstat+"�codart-stoc�"+codartstoc+"�codart-tar�"+codarttar+"�codart-rem�"+codartrem+ "�codart-lie�" + codartlie+"�codart-remp_1�"+codartrempl1+"�codart-remp_2�"+codartrempl2+"�codart-remp_3�"+codartrempl3+"�codart-rat�"+codartrat+"�codart-cvo�"+codartcvo+ "�typaux-fab�" + typauxfab+"�codaux-fab�"+codauxfab+"�typaux-princ�"+typauxprinc+"�codaux-princ�"+codauxprinc;
	
	// inputParameter += "�opecre�"+opecre+"�datcre�"+datcre+"�heucre�"+heucre+"�opemaj�"+opemaj+"�datmaj�"+datmaj+"�heumaj�"+heumaj
	inputParameter += "�typ-condt�" +typcondt+ "�libabr�" + libabr;
	
	inputParameter += "�code-cumul�"+codecumul+"�articom�"+articom+"�gesstock�"+gesstock+"�tenulot�"+tenulot+"�moismini�"+ moismini + "�moismaxi�" + moismaxi+ "�stock-fab�" + stockfab + "�stock-dang�" + stockdang + "�recette-unique�" + recetteunique   + "�art-nomenc�" + artnomenc +"�stock-mip�"+stockmip;
	
	
	if(isFicheSoc)
	{
		inputParameter += "�codbloc�" 		+ $('DICT�codblocFICHESOC').value ;
		inputParameter += "�codebloc_1�" 	+ $('DICT�codebloc_1FICHESOC').value;
		inputParameter += "�provenance�" 	+ $('DICT�provenance').value;
		inputParameter += "�articos�" 		+ articos;
	}
	else
	{
		inputParameter += "�codbloc�" +codbloc;
		inputParameter += "�codebloc_1�" +codebloc1
	}
	
	
	inputParameter += "�codebloc_2�" +codebloc2 + "�codebloc_3�" +codebloc3 + "�codebloc_4�" +codebloc4 + "�auto-vtcpt�" + autovtcpt + "�autotypcom�" + autotypcom +"�datebloc�"+datebloc ;
	inputParameter += "�txabat�" + txabat + "�nbjret�" + nbjret + "�blavoir�" + blavoir ;
	inputParameter += "�datchgart�" + datchgart + "�editar�" + editar +"�edietiq�" + edietiq +"�pxvte-mod�"+pxvtemod+"�fourchpx�" + fourchpx +"�franco�" + franco +"�typetrp�" + typetrp + "�carbu�" + carbu + "�supplementation�" + supplementation  + "�soulte�" + soulte  + "�corapport�" + corapport + "�corechsem�" + corechsem ;
	
	inputParameter += "�classe�"+classe+"�classif�"+classif+"�toxicite�"+toxicite +"�pointeclair�"+pointeclair +"�tempfroid�"+tempfroid+"�tempchaud�"+tempchaud+"�consecu�"+consecu     ;
	inputParameter += "�etiqdang_1�"+etiqdang_1+"�etiqdang_2�"+etiqdang_2+"�etiqdang_3�"+etiqdang_3+"�etiqdang_4�"+etiqdang_4+"�etiqdang_5�"+etiqdang_5+"�cattrp�"+cattrp ;     
	inputParameter += "�symbdang_1�"+symbdang_1+"�symbdang_2�"+symbdang_2+"�symbdang_3�"+symbdang_3 +"�symbdang_4�"+symbdang_4+"�symbdang_5�"+symbdang_5+"�etatphy�"+etatphy+"�codemb�"+codemb      ;
	inputParameter += "�descrip_1�"+descrip_1+"�descrip_2�"+descrip_2+"�descrip_3�"+descrip_3+"�descrip_4�"+descrip_4+"�descrip_5�"+descrip_5+"�descrip_6�"+descrip_6;   
	inputParameter += "�carphys_1�"+carphys_1+"�carphys_2�"+carphys_2+"�carphys_3�"+carphys_3+"�carphys_4�"+carphys_4+"�carphys_5�"+carphys_5+"�carphys_6�"+carphys_6+"�carphys_7�"+carphys_7+"�carphys_8�"+carphys_8 ;
	inputParameter += "�nomtech_1�"+nomtech_1+"�nomtech_2�"+nomtech_2+"�nomtech_3�"+nomtech_3+"�nomtech_4�"+nomtech_4+"�insclasse�"+insclasse+"�datefds�"+datefds;         
	inputParameter += "�cat�"+cat+"�grpemb�"+grpemb+"�risque�"+risque+"�speciale�"+speciale+"�qtelim�"+qtelim+"�ident�"+ident+"�plaque�"+plaque ; 
	
	inputParameter += "�comm-etat_11�" + bc_comment_1 + "�comm-etat_12�" + bp_comment_1 + "�comm-etat_13�" + bl_comment_1 + "�comm-etat_14�" + fac_comment_1 + "�comm-etat_15�" + sai_comment_1 + "�articomx_1�" + txt_comment_1 ;
	inputParameter += "�comm-etat_21�" + bc_comment_2 + "�comm-etat_22�" + bp_comment_2 + "�comm-etat_23�" + bl_comment_2 + "�comm-etat_24�" + fac_comment_2 + "�comm-etat_25�" + sai_comment_2 + "�articomx_2�" + txt_comment_2 ;
	inputParameter += "�comm-etat_31�" + bc_comment_3 + "�comm-etat_32�" + bp_comment_3 + "�comm-etat_33�" + bl_comment_3 + "�comm-etat_34�" + fac_comment_3 + "�comm-etat_35�" + sai_comment_3 + "�articomx_3�" + txt_comment_3 ;
	inputParameter += "�comm-etat_41�" + bc_comment_4 + "�comm-etat_42�" + bp_comment_4 + "�comm-etat_43�" + bl_comment_4 + "�comm-etat_44�" + fac_comment_4 + "�comm-etat_45�" + sai_comment_4 + "�articomx_4�" + txt_comment_4 ;
	inputParameter += "�comm-etat_51�" + bc_comment_5 + "�comm-etat_52�" + bp_comment_5 + "�comm-etat_53�" + bl_comment_5 + "�comm-etat_54�" + fac_comment_5 + "�comm-etat_55�" + sai_comment_5 + "�articomx_5�" + txt_comment_5 ;
	
	
	//R�cup�ration requete de la maj des donn�es SPE.
	var requeteSPE = "";
	try
	{
		if(!isSaisieFicheSoc)
			requeteSPE = majEnregSPE();
	}catch(ex){trace("---majEnregSPE() : " + ex)};
	
	if(requeteSPE != undefined && requeteSPE != "")
		inputParameter += requeteSPE;
	
	//Faire la MAJ
	retourExchange = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , paramFonction , "" , "") ;
	if(retourExchange == "OK")
	{
		
		isSaisieFicheSoc = false;
		
		if(!newArticle)
		{
			alert("<?=lect_bin::getLabel ( "mess-gesartnat_13" ,"sl" )?> "+$("DICT�codart").value+" <?=lect_bin::getLabel ( "mess-gesartnat_15" ,"sl" )?>.");
			
			if ( typetrt != "RAFF")  
				RazGen()
		}
	}
    else
    {
        alert(Rsp[1]);
    }
} // fin function update

function remplir_variable_mot_cle()
{
	libart_1	   = $("DICT�libart_1").value;
	libart_2	   = $("DICT�libart_2").value;
	geslibel	   = $("DICT�geslibel").value;	
	libabr		   = $("DICT�libabr").value;	
	condit		   = $("DICT�condit").value;	
	libcondit	   = $("DICTLINK�condit�libel").value;	
	generique	   = $("DICT�generique").value;		
	typart		   = $("DICT�typart").value;		
	typemag        = $("DICT�type-mag").value;  
	typeclient     = $("DICT�type-client").value; 
	matact         = $("DICT�matact").value; 
	classact       = $("DICT�classact").value; 
	jardin         = $("DICT�jardin").value; 
	nomencgv       = $("DICT�nomencgv").value; 
	coeftaxe       = $("DICT�coeftaxe").value; 
	coefstat       = $("DICT�coef-stat").value;
	uniteStat       = $("DICT�unite_stat").value;
	datebloc       = $("DICT�datebloc").value;
	
	unisto         = $("DICT�unisto").value;  
	libunisto      = $("DICTLINK�unisto�libel").value; 
	unicde         = $("DICT�unicde").value;   
	libunicde      = $("DICTLINK�unicde�libel").value;  
	coefcde        = $("DICT�coefcde").value;   
	unifac         = $("DICT�unifac").value;  
	libunifac      = $("DICTLINK�unifac�libel").value;  
	coeffac        = $("DICT�coeffac").value;   
	surcondit      = $("DICT�surcondit").value;   
	typsurcond     = $("DICT�typ-surcond").value;  
	unihec         = $("DICT�unihec").value;   
	homhec         = $("DICT�homhec").value;   
	douane         = $("DICT�douane").value;  
	artunion       = $("DICT�art-union").value
	anccod1        = $("DICT�anccod1").value;  
	anccod2        = $("DICT�anccod2").value;  
	
	famart		   = $("DICT�famart").value;  
	soufam		   = $("DICT�soufam").value;  
	sssfam		   = $("DICT�sssfam").value; 
	sssnv4		   = $("DICT�sssnv4").value; 
	sssnv5		   = $("DICT�sssnv5").value; 
	famart2		   = $("DICT�famart2").value;  
	sfamart2       = $("DICT�sfamart2").value;       
	ssfamart2	   = $("DICT�ssfamart2").value; 
	negatif   	   = $("DICT�negatif").value;
	gencod  	   = $("DICT�gencod").value;  
	gtin14  	   = $("DICT�gtin14").value;  
	codtva		   = $("DICT�codtva").value;       
	imputa		   = $("DICT�imputa").value;    
	imputv		   = $("DICT�imputv").value;   
	fraisach	   = $("DICT�fraisach").value;
	cathomo 	   = $("DICT�cathomo").value;	
	nohomo 	       = $("DICT�nohomo").value; 
	txabat   	   = $("DICT�txabat").value;         
	nbjret    	   = $("DICT�nbjret").value;
	blavoir    	   = $("DICT�blavoir").value; 
	codeamm 	   = $("DICT�codeamm").value; 
	categorie 	   = $("DICT�categorie").value; 
	traitement 	   = $("DICT�traitement").value; 
	socapart	   = $("DICT�soc-apart").value; 
	epandage       = $("DICT�epandage").value; 
	
	editar         = $("DICT�editar").value;     
	edietiq        = $("DICT�edietiq").value;
	pxvtemod	   = $("DICT�pxvte-mod").value;
	fourchpx       = $("DICT�fourchpx").value;    
	franco         = $("DICT�franco").value;     
	typetrp        = $("DICT�typetrp").value;  
	
	poidsuv		   = $("DICT�poids").value;     
	poidsbrut      = $("DICT�poids-brut").value;     
	poidsnet	   = $("DICT�poids-net").value;     
	volume		   = $("DICT�volume").value;  
	gestpds		   = $("DICT�gestpds").value;  
	tauxunion      = $("DICT�taux-union").value; 
	emplac  	   = $("DICT�emplac").value;     
	
	stockmin	   = $("DICT�stock-min").value;     
	stockmax	   = $("DICT�stock-max").value;    
	stockalert	   = $("DICT�stock-alerte").value;   
	stockmip	   = $("DICT�stock-mip").value;
	qteecocde	   = $("DICT�qte-ecocde").value; 
	
	tvapre	       = $("DICT�tvapre").value;    
	datchgtva 	   = $("DICT�datchgtva").value;    
	try{
	puncagn 	   = $("DICT�pun-cagn").value;    
	debcagn 	   = $("DICT�deb-cagn").value;    
	fincagn 	   = $("DICT�fin-cagn").value;    
	}catch(ex){}
	segana	       = $("DICT�segana").value;    
	
	datchgart	   = $("DICT�datchgart").value;    
	//prixpa		   = $("DICT�prix-pa").value;     
	//devise		   = $("DICT�devise").value;     
		
	codartstat         = $("DICT�codart-stat").value;    
	codartstoc         = $("DICT�codart-stoc").value;    
	codarttar          = $("DICT�codart-tar").value;     
	codartrem          = $("DICT�codart-rem").value;     
	codartlie          = $("DICT�codart-lie").value;    
	codartrempl1       = $("DICT�codart-remp_1").value;       
	codartrempl2       = $("DICT�codart-remp_2").value;      
	codartrempl3       = $("DICT�codart-remp_3").value;     
	codartrat          = $("DICT�codart-rat").value;     
	codartcvo          = $("DICT�codart-cvo").value;     
	codecumul          = $("DICT�code-cumul").value;    
	typauxfab          = $("DICT�typaux-fab").value;    
	codauxfab          = $("DICT�codaux-fab").value;     
	typauxprinc        = $("DICT�typaux-princ").value;    
	codauxprinc        = $("DICT�codaux-princ").value;  
	/* 
	opecre	           = $("DICT�opecre").value;      
	datcre	           = $("DICT�datcre").value;          
	heucre	           = $("DICT�heucre").value;     
	opemaj	           = $("DICT�opemaj").value;    
	datmaj	           = $("DICT�datmaj").value;    
	heumaj	           = $("DICT�heumaj").value;     
	 */
	typcondt           = $("DICT�typ-condt").value;      
	codbloc	           = $("DICT�codbloc").value;   
	codebloc1          = $("DICT�codebloc_1").value;  
	codebloc2          = $("DICT�codebloc_2").value;  
	codebloc3          = $("DICT�codebloc_3").value;  
	codebloc4          = $("DICT�codebloc_4").value;
	autovtcpt		   = $("DICT�autovtcpt").value;
	autotypcom	       = $("DICT�autotypcom").value;
	gesstock           = $("DICT�gesstock").value; 
	tenulot            = $("DICT�tenulot").value; 
	moismini           = $("DICT�moismini").value; 
	moismaxi           = $("DICT�moismaxi").value; 
	stockfab           = $("DICT�stock-fab").value;
	stockdang          = $("DICT�stock-dang").value; 
	recetteunique      = $("DICT�recette-unique").value; 
	artnomenc          = $("DICT�art-nomenc").value; 
	carbu              = $("DICT�carbu").value; 
	supplementation    = $("DICT�supplementation").value; 
	soulte             = $("DICT�soulte").value; 
	corapport          = $("DICT�corapport").value; 
	corechsem          = $("DICT�corechsem").value; 
	
	classe        = $("DICT�classe").value;
	classif       = $("DICT�classif").value; 
	toxicite      = $("DICT�toxicite").value;
	pointeclair   = $("DICT�pointeclair").value;
	tempfroid     = $("DICT�tempfroid").value;
	tempchaud     = $("DICT�tempchaud").value;    
	consecu       = $("DICT�consecu").value;   
	etiqdang_1    = $("DICT�etiqdang_1").value;   
	etiqdang_2    = $("DICT�etiqdang_2").value;   
	etiqdang_3    = $("DICT�etiqdang_3").value;   
	etiqdang_4    = $("DICT�etiqdang_4").value;    
	etiqdang_5    = $("DICT�etiqdang_5").value;   
	cattrp        = $("DICT�cattrp").value;   
	symbdang_1    = $("DICT�symbdang_1").value;   
	symbdang_2    = $("DICT�symbdang_2").value;   
	symbdang_3    = $("DICT�symbdang_3").value;    
	symbdang_4    = $("DICT�symbdang_4").value;   
	symbdang_5    = $("DICT�symbdang_5").value;   
	etatphy       = $("DICT�etatphy").value;   
	codemb        = $("DICT�codemb").value;   
	autsoc		  = $("DICT�autsoc").value.trim();
	
	descrip_1     = $("DICT�descrip_1").value; 
	descrip_2     = $("DICT�descrip_2").value; 
	descrip_3     = $("DICT�descrip_3").value; 
	descrip_4     = $("DICT�descrip_4").value; 
	descrip_5     = $("DICT�descrip_5").value; 
	descrip_6     = $("DICT�descrip_6").value; 
	carphys_1     = $("DICT�carphys_1").value; 
	carphys_2     = $("DICT�carphys_2").value; 
	carphys_3     = $("DICT�carphys_3").value; 
	carphys_4     = $("DICT�carphys_4").value; 
	carphys_5     = $("DICT�carphys_5").value; 
	carphys_6     = $("DICT�carphys_6").value; 
	carphys_7     = $("DICT�carphys_7").value; 
	carphys_8     = $("DICT�carphys_8").value; 
	nomtech_1     = $("DICT�nomtech_1").value; 
	nomtech_2     = $("DICT�nomtech_2").value; 
	nomtech_3     = $("DICT�nomtech_3").value; 
	nomtech_4     = $("DICT�nomtech_4").value; 
	insclasse     = $("DICT�insclasse").value; 
	datefds       = $("DICT�datefds").value; 
	
	cat           = $("DICT�cat").value;     
	grpemb        = $("DICT�grpemb").value;    
	risque        = $("DICT�risque").value;    
	speciale      = $("DICT�speciale").value;    
	qtelim        = $("DICT�qtelim").value;    
	ident         = $("DICT�ident").value; 
	plaque        = $("DICT�plaque").value; 

    bc_comment_1  = $( "DICT�bc-comment_1"  ).value ;
    bp_comment_1  = $( "DICT�bp-comment_1"  ).value ;
    bl_comment_1  = $( "DICT�bl-comment_1"  ).value ;
    fac_comment_1 = $( "DICT�fac-comment_1" ).value ;
    sai_comment_1 = $( "DICT�sai-comment_1" ).value ;
    txt_comment_1 = $( "DICT�txt-comment_1" ).value ;

    bc_comment_2  = $( "DICT�bc-comment_2"  ).value ;    
    bp_comment_2  = $( "DICT�bp-comment_2"  ).value ;
    bl_comment_2  = $( "DICT�bl-comment_2"  ).value ;
    fac_comment_2 = $( "DICT�fac-comment_2" ).value ;
    sai_comment_2 = $( "DICT�sai-comment_2" ).value ;
    txt_comment_2 = $( "DICT�txt-comment_2" ).value ;

    bc_comment_3  = $( "DICT�bc-comment_3"  ).value ;
    bp_comment_3  = $( "DICT�bp-comment_3"  ).value ;
    bl_comment_3  = $( "DICT�bl-comment_3"  ).value ;
    fac_comment_3 = $( "DICT�fac-comment_3" ).value ;
    sai_comment_3 = $( "DICT�sai-comment_3" ).value ;
    txt_comment_3 = $( "DICT�txt-comment_3" ).value ;

    bc_comment_4  = $( "DICT�bc-comment_4"  ).value ;
    bp_comment_4  = $( "DICT�bp-comment_4"  ).value ;
    bl_comment_4  = $( "DICT�bl-comment_4"  ).value ;
    fac_comment_4 = $( "DICT�fac-comment_4" ).value ;
    sai_comment_4 = $( "DICT�sai-comment_4" ).value ;
    txt_comment_4 = $( "DICT�txt-comment_4" ).value ;

    bc_comment_5  = $( "DICT�bc-comment_5"  ).value ;
    bp_comment_5  = $( "DICT�bp-comment_5"  ).value ;
    bl_comment_5  = $( "DICT�bl-comment_5"  ).value ;
    fac_comment_5 = $( "DICT�fac-comment_5" ).value ;
    sai_comment_5 = $( "DICT�sai-comment_5" ).value ;
    txt_comment_5 = $( "DICT�txt-comment_5" ).value ;

	p       = new RegExp( "\r\n" , "gi" ) ;
	
    articom = $("DICT�articom" ).value ;   
    articom = articom.replace( p , "��" ) ;
	
    articos = $("DICT�comSociete" ).value ;   
    articos = articos.replace( p , "��" ) ;

    txt_comment_1 = txt_comment_1.replace( p , "��" ) ;
    txt_comment_2 = txt_comment_2.replace( p , "��" ) ;
    txt_comment_3 = txt_comment_3.replace( p , "��" ) ;
    txt_comment_4 = txt_comment_4.replace( p , "��" ) ;
    txt_comment_5 = txt_comment_5.replace( p , "��" ) ;

    if ( txt_comment_1 == "" )
    {
        bc_comment_1  = "N" ;
        bp_comment_1  = "N" ;
        bl_comment_1  = "N" ;
        fac_comment_1 = "N" ;
        sai_comment_1 = "N" ;
    }

    if ( txt_comment_2 == "" )
    {
        bc_comment_2  = "N" ;
        bp_comment_2  = "N" ;
        bl_comment_2  = "N" ;
        fac_comment_2 = "N" ;
        sai_comment_2 = "N" ;
    }

    if ( txt_comment_3 == "" )
    {
        bc_comment_3  = "N" ;
        bp_comment_3  = "N" ;
        bl_comment_3  = "N" ;
        fac_comment_3 = "N" ;
        sai_comment_3 = "N" ;
    }

    if ( txt_comment_4 == "" )
    {
        bc_comment_4  = "N" ;
        bp_comment_4  = "N" ;
        bl_comment_4  = "N" ;
        fac_comment_4 = "N" ;
        sai_comment_4 = "N" ;
    }

    if ( txt_comment_5 == "" )
    {
        bc_comment_5  = "N" ;
        bp_comment_5  = "N" ;
        bl_comment_5  = "N" ;
        fac_comment_5 = "N" ;
        sai_comment_5 = "N" ;
    }
 }//fin function remplir_variable_mot_cle

/************* BOUTONS D'ENTETE ****************/
function envoi_article_fou()
{
	var _visuInitArticl;

	codart = $("DICT�codart").value;
	libart = $("DICTLINK�libart_1�libel").value;

	if (codart != "" && libart!= "" )
	{
		_visuInitArticl = 0;
		
		if ( "<?=$n_etat_visu?>" == 1 )
			_visuInitArticl = 1;
        else { if(isinsaisie==true) UpdateArticl('RAFF') ; }
			
		PhpAuxges = "<?=search::searchExe("ns_trfartvte_w3c.php",1)?>"+"?<?=session_name()?>=<?=session_id()?>&PHPSESSID=<?=$PHPSESSID?>&_visuInitArticl="+_visuInitArticl+"&_codart="+codart+"&_codartlibel="+libart+"&_libel_menu=Tarif Vente Article ";
		
		if(lectureSeule)
			PhpAuxges += "&_VISU=YES";
		
		sFeatures = "dialogHeight:750px;dialogWidth:1050px;status:no;help:no";
		retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
	}
	else
	{
		// mess Vous n'avez pas saisi d'article
		alert ("<?=lect_bin::getLabel ( "mess-prognat_12" ,"sl" )?>");
	}		  
 }

function envoi_article_recette(){
	 var _visuInitArticl;
	 codart = $("DICT�codart").value;
	 libart = $("DICTLINK�libart_1�libel").value;
	 if (codart != "" && libart!= "" ){
		PhpAuxges = "<?=search::searchExe("ns_nomfab_w3c.php",1)?>"+"?<?=session_name()?>=<?=session_id()?>&PHPSESSID=<?=$PHPSESSID?>&_visuInitArticl="+_visuInitArticl+"&_codart="+codart+"&_codartlibel="+libart+"&_libel_menu=Nomenclature Recette ";
		
		if(lectureSeule)
			PhpAuxges += "&_VISU=YES";
		
		
		sFeatures = "dialogHeight:800px;dialogWidth:1000px;status:no;help:no";
		retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
	 } else {
		  // mess Vous n'avez pas saisi d'article
		  alert ("<?=lect_bin::getLabel ( "mess-prognat_12" ,"sl" )?>");
	 }		  
 }

function article_varlog(){
     var codart = $("DICT�codart").value;
	 
     if (codart != ""){
		PhpAuxges = "<?=search::searchExe("ns_varlog_w3c.php",1)?>"+"?<?=session_name()?>=<?=session_id()?>&PHPSESSID=<?=$PHPSESSID?>&codart="+codart+"&_libel_menu=Variantes logistiques";
		
		if(lectureSeule)
			PhpAuxges += "&_VISU=YES";
		
		sFeatures = "dialogHeight:250px;dialogWidth:950px;status:no;help:no;scroll:no";
		retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
	 } else {
		  // mess Vous n'avez pas saisi d'article
		  alert ("<?=lect_bin::getLabel ( "mess-prognat_12" ,"sl" )?>");
	 }		  
 }

function seuilMagasin(){
     codart = $("DICT�codart").value;
	 var libart = $("DICTLINK�libart_1�libel").value;
     if (codart != ""){
		PhpAuxges = "<?=search::searchExe("ns_seuilmag_w3c.php",1)?>"+"?<?=session_name()?>=<?=session_id()?>&PHPSESSID=<?=$PHPSESSID?>&codart="+codart+"&libart="+libart+"&_libel_menu=Seuil par magasin";
		
		if(lectureSeule)
			PhpAuxges += "&_VISU=YES";
		
		sFeatures = "dialogHeight:800px;dialogWidth:1000px;status:no;help:no;scroll:no";
		retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
	 } else {
		  // mess Vous n'avez pas saisi d'article
		  alert ("<?=lect_bin::getLabel ( "mess-prognat_12" ,"sl" )?>");
	 }		  
 }

function envoi_article_ach(){
	 var _visuInitArticl;
	 codart = $("DICT�codart").value;
	 libart = $("DICTLINK�libart_1�libel").value;
	 if (codart != "" && libart!= "" ){
		_visuInitArticl = 0;
		if ( "<?=$n_etat_visu?>" == 1 )
			_visuInitArticl = 1;
        	else 
        	{ if(isinsaisie==true) UpdateArticl('RAFF') ; }
		PhpAuxges = "<?=search::searchExe("ns_arfluc_w3c.php",1)?>"+"?<?=session_name()?>=<?=session_id()?>&PHPSESSID=<?=$PHPSESSID?>&_nathalie=O&_visuInitArticl="+_visuInitArticl+"&_codart="+codart+"&_codartlibel="+libart+"&_libel_menu=Tarif Achat Article&_typaux="+$("DICT�typaux-princ").value+"&_codauxges="+$("DICT�codaux-princ").value + "&_libcodauxges="+$("DICTLINK�codaux-princ�libel").value;
		
		if(lectureSeule)
			PhpAuxges += "&_visuInitArticl=1";
		
		sFeatures = "dialogHeight:550px;dialogWidth:700px;status:no;help:no";
		retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
	 } else {
		  // mess Vous n'avez pas saisi d'article
		  alert ("<?=lect_bin::getLabel ( "mess-prognat_12" ,"sl" )?>");
	 }		  
}


function envoi_taxe()
 {
	 var _visuInitArticl;

	 codart = $( "DICT�codart" ).value ;
	 libart = $( "DICTLINK�libart_1�libel" ).value ;

	 if ( codart != "" && libart!= "" )
	  {
		_visuInitArticl = 0;

        famart = $( "DICT�famart" ).value ;
        soufam = $( "DICT�soufam" ).value ;

		PhpAuxges = "<?=search::searchExe("nv_taxart_w3c.php",1)?>"+"?<?=session_name()?>=<?=session_id()?>&PHPSESSID=<?=$PHPSESSID?>&_visuInitArticl="+_visuInitArticl+"&_codart="+codart+"&_codartlibel="+libart+"&_famart="+famart+"&_soufam="+soufam+"&_libel_menu=Visu Taxes Article" ;
		sFeatures = "dialogHeight:600px;dialogWidth:800px;status:no;help:no";
		retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;

	  }
	  else
	  {
          // mess Vous n'avez pas saisi d'article
		  alert ("<?=lect_bin::getLabel ( "mess-prognat_12" ,"sl" )?>");
	  }		  
 }

function envoi_gencod()
 {
	 var _visuInitArticl;

	 codart = $("DICT�codart").value;
	 libart = $("DICTLINK�libart_1�libel").value;

	 if ( codart != "" && libart != "" )
	  {
		_visuInitArticl = 0;
		
        PhpAuxges = "<?=search::searchExe("ns_gencod_w3c.php",1)?>"+"?<?=session_name()?>=<?=session_id()?>&PHPSESSID=<?=$PHPSESSID?>&_visuInitArticl="+_visuInitArticl+"&_codart="+codart+"&_codartlibel="+libart+"&_libel_menu=Gencods Article ";
        
		if(lectureSeule)
			PhpAuxges += "&_VISU=YES";
		
		sFeatures = "dialogHeight:800px;dialogWidth:1000px;status:no;help:no";
        retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;

	  }
	  else
	  {
	      // mess Vous n'avez pas saisi d'article
		  alert ("<?=lect_bin::getLabel ( "mess-prognat_12" ,"sl" )?>");
	  }		  
 }

function ScanEntete()
{
	codart = $("DICT�codart").value;
	libart = $("DICTLINK�libart_1�libel").value;
	if (codart != "" && libart!= "" )
	{
		retour = "<?=$Scandoc?>&_progiciel=NATHALIE&_motcle=FICHIER&_typtab=ARTICLE&_prefix=" + codart ;
		retour = window.open ( retour , "" , "status=0,width=430px,height=465px,top=100px,left=100px" );
		return ;
	}
	else
    {
	   // mess Vous n'avez pas saisi d'article
	   alert ("<?=lect_bin::getLabel ( "mess-prognat_12" ,"sl" )?>");
    }		      	
}
/************* FIN BOUTONS D'ENTETE ****************/

function synchroTab()
{
	// Ne rien faire
}

function visu_stat()
{
     codart = $("DICT�codart").value;
	 libart = $("DICTLINK�libart_1�libel").value;

     strDate = new Date()
     dateTemp = new Date()
     dateTemp.setDate(strDate.getDate()-90);

     if ( dateTemp.getMonth()+ 1 < 10 )  datdeb  = Entry(1,"<?=$date_jour?>","/") + "/0"+(dateTemp.getMonth()+ 1)+"/"+(dateTemp.getFullYear() ) ;
     else datdeb = Entry(1,"<?=$date_jour?>","/") + "/"+(dateTemp.getMonth()+ 1)+"/"+(dateTemp.getFullYear() ) ;

	 if (codart != "" && libart!= "" )
	 {         	
		PhpAuxges = "<?=search::searchExe("nv_staart.php",1)?>"+"?<?=session_name()?>=<?=session_id()?>&PHPSESSID=<?=$PHPSESSID?>&_staetab=T&_codart="+codart+"&_datdeb="+datdeb+"&_libart="+libart+"&_libel_menu=" + "<?=lect_bin::getLabel ( "mess-titrenat_28" ,"sl" )?>";
		sFeatures = "dialogHeight:730px;dialogWidth:1050px;status:no;help:no";
		retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
	 }
	 else
	 {
	      // mess Vous n'avez pas saisi d'article
	      alert ("<?=lect_bin::getLabel ( "mess-prognat_12" ,"sl" )?>");
	 }		  
}

function gestDuplicationArticle()
{
	trace("--gestDuplicationArticle--")
	
	if($('DICT�socdup').value == "" )
		dupart();
	else if($('divSocdup').style.visibility == 'visible' || $('divSocdup').style.visibility == '' && paramDupArtcl)
	{
		var listeSoc = $('DICT�socdup').value.split(',');
		for(var i=0; i<listeSoc.size(); i++)
			dupart(listeSoc[i]);
	}
}

/**
	Duplication d'un article.
	socDuplication => Soci�t� r�ceptrice de la duplication.
**/
function dupart(socDuplication)
{	
	trace("--dupart - socDuplication : "+socDuplication+" --")

	if(socDuplication == undefined)
		socDuplication = "";
	
	var codart = $('DICT�codart').value;
	var codartDup = $('DICT�artdup').value;
	var dogFct = "";
	var requete = "";
	var retourExchange;
	
	// console.log(codart)
	// console.log(codartDup)
	
	//SI article et nouveau code article renseign�
	//ALORS duplication vers ce nouvel article
	if(codart != "" && codartDup != "")
	{
		dogFct = "DUP-GESARTICL�n-GESARTICL";
		requete = "�CODSOC�"+codsoc+"�CODART�" + codart + "�NEWCODART�" + codartDup;
		
		if(paramDupArtcl && socDuplication != undefined && socDuplication != "")
			requete += "�SOCREC�" + socDuplication;
		
        retourExchange = sendExchangeInvoke("<?=$dogExchange?>", requete, dogFct, "", "");

        if(retourExchange != "OK")
        {
            alert(Rsp[1]);
            return;
        }
		
		Rowid = getValeurRsp('ROwID');
		clearZone('DICT�gtin14');
		
		// Message: l'article XXX a �t� dupliqu� en YYY
        alert("<?=lect_bin::getLabel("mess-gesartnat_13", "sl")?> " + codart + " <?=lect_bin::getLabel("mess-gesartnat_14", "sl")?> " + codartDup + " sur la soci�t� " + socDuplication);
		
		$("DUPLICATION").style.visibility = "hidden";
        $("DICT�libart_1").focus();
	}
	//SINON => on cr�e le nouveau code article avec l'indentation automatique et on relance la m�thode de duplication.
	else
	{
		dogFct = "INC-ARTICL�n-GESARTICL";
		requete = "�CODSOC�" + codsoc;
		
		retourExchange = sendExchangeInvoke("<?=$dogExchange?>",requete,dogFct,"","");
		
		if(retourExchange == 'OK')
		{
			$('DICT�artdup').value = getValeurRsp('CODART');
			
			//Rappel de la m�thode de duplication
			dupart();
		} 
        else
		{
			// Message: Vous n'avez pas saisi d'article
			$("DUPLICATION").style.visibility = "hidden";
			alert("<?=lect_bin::getLabel( "mess-prognat_12", "sl")?>");
			$("DICT�codart").focus();
		}
	}
}


function RazGen()
{
	//On appel la fonction saisieOKSpe() qui v�rifie qu'il n'y a aucune saisie dans l'onglet SPE 
	//(si la fonction existe)
	try{
		if(!saisieOKSpe())
			return false;
	}catch(ex){trace("---saisieOKSpe : " + ex)}
	
	if(isSaisieFicheSoc && $('DIV_FICHESOC').style.visibility.toUpperCase() == 'VISIBLE')
	{
		if(!confirm("Une saisie dans la fen�tre \"Donn�es soci�t�\" est en cours.\nQuitter la saisie ?"))
			return false;
		else
		{
			isSaisieFicheSoc = false;
			ctrlFicheSoc('QUITTER');
		}
	}
	
    loadOng('1')
    ImageD.RazImage();


    isinsaisie = false ;
    saisieEnCours = false ;

    $("DICT�codart").value = ""
    $("DICT�codart").buffer = ""

    $("DICTLINK�libart_1�libel").value = ""
    Rowid = "" 

	enableZone ( "DICT�codart" , true  )
    selectZone("DICT�codart") 
	tout_enable(false)
	tout_clear();
	clearZone('DICT�artdup|DICT�socdup');
	
	if($('DIV_FICHESOC').style.visibility.toUpperCase() == 'VISIBLE')
	{
		clearZone('DICT�codblocFICHESOC|DICT�codebloc_1FICHESOC|DICT�provenance|DICT�provenanceLib|DICT�comSociete');
		$('DIV_FICHESOC').style.visibility = 'hidden';
	}
	
	try{
		//On appel la m�thode clearOngSpe() qui vide l'onglet SPE (si elle existe)
		clearOngSpe();
	}catch(ex){trace("---clearOngSpe : " + ex)}

}

function controle_zone(obj)
{
	if (obj.value=="")
	{
		switch (obj.id.toUpperCase())
		{
			case "DICT�FAMART" :
				$("DICTLINK�famart�libel").value ="";
				$("DICT�soufam").value           ="";	
				$("DICTLINK�soufam�libel").value ="";				
				$("DICT�sssfam").value           ="";				
				$("DICTLINK�sssfam�libel").value ="";			
				break ;
			case "DICT�SSSNV4" :
				$("DICTLINK�sssnv4�libel").value ="";
				$("DICT�sssnv5").value           ="";
				$("DICTLINK�sssnv5�libel").value ="";	
				break ;
			case "DICT�SOUFAM" :			
				$("DICTLINK�soufam�libel").value ="";
				$("DICT�sssfam").value           ="";				
				$("DICTLINK�sssfam�libel").value ="";	
				break ;
			case "DICT�SSSFAM" :
				$("DICTLINK�sssfam�libel").value ="";
				break ;
			case "DICT�CODTVA" :
				$("DICTLINK�codtva�libel").value ="";
				break ;
			case "DICT�IMPUTA" :
				$("DICTLINK�imputa�libel").value ="";
				break ;
			case "DICT�IMPUTV" :
				$("DICTLINK�imputv�libel").value ="";
				break ;
			case "DICT�FAMART2" :
				$("DICTLINK�famart2�libel").value ="";
				$("DICT�sfamart2").value           ="";	
				$("DICTLINK�sfamart2�libel").value ="";				
				$("DICT�ssfamart2").value           ="";				
				$("DICTLINK�ssfamart2�libel").value ="";			
				break ;
			case "DICT�SFAMART2" :			
				$("DICTLINK�sfamart2�libel").value ="";
				$("DICT�ssfamart2").value           ="";				
				$("DICTLINK�ssfamart2�libel").value ="";	
				break ;
			case "DICT�SSFAMART2" :
				$("DICTLINK�ssfamart2�libel").value ="";
				break ;
		}
	}
 }//fin function controle_zone

function testQuit(){
	if(saisieEnCours==true && "<?=$n_etat_visu?>" != 1){
		if(confirm("Enregistrer avant de quitter?")){
			UpdateArticl() ;	
		}	
	}
}


function loadOng(Flag)
{
	
	if(saisieEnCours && $('DIV_FICHESOC').style.visibility.toUpperCase() == 'VISIBLE')
	{
		alert("Fermer la fen�tre \"Donn�es soci�t�\" avant de changer d'onglet.")
		return false;
	}
	
	//Si on sort de l'onglet sp�cifique, on appel la m�thode de controle du SPE
	//si elle existe.
	if(ongCours == 11 && Flag != "11")
	{
		try{
			if(!ctrlSortieOngletSpe())
				return false;
		}catch(ex){trace("---ctrlSortieOngletSpe() : " + ex)}
	}
	
	// if(!saisieEnCours) return;
	for ( i = 1 ; i<=11 ; i++)
    {	  
        $ ( "ong" + i ).className          = "divArrondi2"  
        $("ong"+i).style.top = "120px" 
        $(i+"title").style.visibility = "hidden" 
        // setFocus ( TabFocusEntree , "FIRST" )
    }
    if ( Flag.trim() != "" )
    {
        i = parseInt ( Flag );

        $ ( "ong" + i ).className          = "divArrondi16"
        $("ong"+i).style.top = "110px"
		$(i+"title").style.visibility = "visible" 
		// razEnreg()		
	}
	
	//Chargement onglet SPE
	if(i == 11)
	{
		try{initOngSpe()}catch(ex){trace("---initOngSpe() : " + ex)}
	}
	
	ongCours = i;
}

function annuldupart()
{
	$('DUPLICATION').style.visibility='hidden'
	clearZone("DICT�artdup|DICT�socdup")
}

extensionsValides=new Array('png','jpg','jpeg','gif');

function getExtension(filename)
{
	var parts = filename.split(".");
	return (parts[(parts.length-1)]);
}    


// v�rifie l'extension d'un fichier upload�
// champ : id du champ type file
// listeExt : liste des extensions autoris�es
function verifFileExtension(champ,listeExt)
{
	filename = champ.toLowerCase();
	fileExt = getExtension(filename);
	for (i=0; i<listeExt.length; i++)
	{
		if ( fileExt == listeExt[i] ) 
		{
			return (true);
		}
	}
	return (false);
}

var myImg = new Array();
var chrono = 1;

function ChargerImage(divId) {

	ImageD.RazImage(1);

	//initialiaser les attributs
	ImageD.divId = divId;
	chrono = 1;

	var input = "�PROGICIEL�NATHALIE�MOTCLE�FICHIER�TYPTAB�ARTICLE�PREFIX�" + $("DICT�codart").value
			  
	var requete = "f10-annexe�d-annexeblobv7";
	var retourExchange = sendExchangeInvoke("<?=$dogExchange;?>", input, requete, "", "");

	// si aucune r�ponse
	if(Rsp[0] != "OK") {
		ImageD.RazImage(1);
		return;
	}

	ImageD.indicePhoto = 0;

	RspSave      = new Array();
	RspSave      = Rsp.concat();
	ImageD.myImg = new Array();
	ImageD.nbImg = 0;

	var bigChr = 1;

	for(key in RspSave) 
	{
		if(key == "each") break;
		if(key != 0) 
		{
			Rsp[1] = RspSave[key];
			if(verifFileExtension(setValueFromKeyword("NOMFICHIER", false, "", "", true),extensionsValides))
			{
				ImageD.myImg[ImageD.nbImg] = [
					'<?=$dogdealscanRelPath;?>' + setValueFromKeyword("NOMFICHIER", false, "", "", true)
				];
				
				ImageD.nbImg++;
				ImageD.indicePhoto++;
				if(chrono > bigChr) bigChr = chrono;
			}
		}
	}

	chrono = bigChr + 1;

	if(ImageD.nbImg > 1) ImageD.imagePresente = 1;
	var url = ImageD.myImg[0];
	ImageD.indicePhoto = 0;
	ImageD.AfficheImage(url, divId);
}

function ChargeNewImage() {
	$('ChgImage').show();
}

var nom_abs = "";
function SendFileToServer(file) {
	nom_abs = window.frames.IBlob.document.forms[0].elements["Fichier"].value.trim();

	for(var i=nom_abs.length; i>0; i--) {
		var test = nom_abs.substr(i, 1);
		if(test.charCodeAt() == 92) {
			nom_abs = nom_abs.substr(i + 1, nom_abs.length - i);
			break;
		}
	}

	window.frames.IBlob.document.forms[0].method = "post";
	window.frames.IBlob.document.forms[0].action = "<?=search::searchExe("ds_annexe_chargement_w3c.php",1)?>?<?=session_name()?>=<?=session_id()?>&_nom_abs="+escape(nom_abs)+"&_sufix=" + escape ( sufix )
	window.frames.IBlob.document.forms[0].submit();
}

function rechGtin()
{
    if ( $("DICT�gtin14").value == "") return true;

    requete = "CODSOC�"+codsoc+"�CODART�" + $("DICT�codart").value.trim() + "�THEME�GTIN14"+"�CHRONO�0�UNITE��ALPHACLE�" + $("DICT�gtin14").value.trim() + "�ORIGINE�GESARTNAT";
    dogFct  = "get-artics�n-varlog";
    retourExchange = sendExchangeInvoke("<?=$dogExchange?>", requete, dogFct, "", "");
    if (retourExchange != "OK")
    {
        alert(Rsp[1]);
        clearZone("DICT�gtin14");
        $("DICT�gtin14").focus();
        return false;
    }
    return true;
}

//DEAL_ANA_135_603
function substitutions()
{
	var codart = $( "DICT�codart" ).value.trim() ;
	var libart = $( "DICTLINK�libart_1�libel" ).value.trim() ;

	if ( codart == "" && libart == "" )
	{
        // mess Vous n'avez pas saisi d'article
		alert ("<?=lect_bin::getLabel ( "mess-prognat_12" ,"sl" )?>");
		return false;
	}		  
	var PhpArticl = "<?=search::searchExe('ns_substitutions_w3c.php',1)?>?PHPSESSID=<?=$PHPSESSID?>&_codart=" + codart + "&_libart=" + libart + "&_unite=" + $("DICT�unisto").value.trim() + "&_rowid=" + Rowid;
	
	if(lectureSeule)
		PhpArticl += "&_VISU=YES";
	
	var sFeatures = "dialogHeight:700px;dialogWidth:960px;status:no;help:no"
	var retourModale = showModalDialog ( PhpArticl , "" , sFeatures)
	if(retourModale!=undefined)
	{
		
	}
	else
	{
		
	}
}

function article_magasin()
{
	var codart = $( "DICT�codart" ).value.trim() ;
	var libart = $( "DICTLINK�libart_1�libel" ).value.trim() ;

	if ( codart == "" && libart == "" )
	{
        // mess Vous n'avez pas saisi d'article
		alert ("<?=lect_bin::getLabel ( "mess-prognat_12" ,"sl" )?>");
		return false;
	}		  
	var PhpArticl = "<?=search::searchExe('ns_articlemagasin_w3c.php',1)?>?PHPSESSID=<?=$PHPSESSID?>&_codart=" + codart + "&_libart=" + libart + "&_unite=" + $("DICT�unisto").value.trim() + "&_rowid=" + Rowid;
	
	if(lectureSeule)
		PhpArticl += "&_VISU=YES";
		
	var sFeatures = "dialogHeight:700px;dialogWidth:960px;status:no;help:no"
	var retourModale = showModalDialog ( PhpArticl , "" , sFeatures)
	if(retourModale!=undefined)
	{
		
	}
	else
	{
		
	}
}

/** FONCTIONS GESTION ECRAN BOUTON DONNES SOCIETES 
	si ficheSoc = YES
**/

var isSaisieFicheSoc = false;
function ctrlFicheSoc(etat)
{
	trace("--ctrlFicheSoc - " + etat + " --")
	
	switch(etat.toUpperCase())
	{
		case 'QUITTER':
			if(isSaisieFicheSoc && !confirm("Une saisie est en cours. Quitter la saisie ?"))
				return;
			
			// clearZone('DICT�codblocFICHESOC|DICT�codebloc_1FICHESOC|DICT�provenance|DICT�provenanceLib|DICT�comSociete');
			enableZone('DICT�codblocFICHESOC|DICT�codebloc_1FICHESOC|DICT�provenance|DICT�comSociete',false);
			$('DIV_FICHESOC').style.visibility = 'hidden';
			isSaisieFicheSoc = false;
			break;
			
		case 'VALIDER':
			if(!isSaisieFicheSoc)
				return;
			
			UpdateArticl();
			isSaisieFicheSoc = false;
			$('DIV_FICHESOC').style.visibility = 'hidden';
			break;
			
		case 'MODIFIER':
			if(isSaisieFicheSoc)
				return;
			
			enableZone('DICT�codblocFICHESOC|DICT�codebloc_1FICHESOC|DICT�provenance|DICT�comSociete',true);
			isSaisieFicheSoc = true;
			$('DICT�codblocFICHESOC').focus();
			break;
			
		case 'AFFICHER':
			if($('DIV_FICHESOC').style.visibility == 'hidden')
				$('DIV_FICHESOC').style.visibility = 'visible';
			else
				$('DIV_FICHESOC').style.visibility = 'hidden';
			break;
	}
}

function affichDuplication()
{
	//On appel la fonction saisieOKSpe() qui v�rifie qu'il n'y a aucune saisie dans l'onglet SPE 
	//(si la fonction existe)
	try{
		if(!saisieOKSpe())
			return false;
	}catch(ex){trace("---saisieOKSpe : " + ex)}
	
	$('DUPLICATION').style.visibility = 'visible';
	
	if(!paramDupArtcl)
		$('divSocdup').style.visibility = 'hidden';
	
	$('DICT�artdup').focus();	
}
</SCRIPT>

<!-- METHODES JAVASCRIPT DE L'ONGLET SPE -->
<script src="<?=search::searchExe("ns_gesartnat_spe_w3c.js",1)?>"></script> 

<HTML>
<BODY class=dogskinbody onload="InitScreen()" onbeforeunload="testQuit()">


<!-- //Zones cach�es// -->
<INPUT ID="DICT�codsoc"   TYPE=HIDDEN  >
<INPUT ID="DICT�rowid"    TYPE=HIDDEN  value="<?=$_rowid?>" >
<input id="mode_bon"      type="hidden">


<DIV class="divArrondiDeg29" style="position:absolute;width:950px;height:100px;left:5px;top:5px">
    <!-- soci�t� de regroupement -->
	<DIV  style="position:absolute;left:30px;top:3px">
		<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "socreg" , "sl"); ?> </P>
		<INPUT ID="DICT�socreg-con" value = "<?=$_socreg?>" TYPE=TEXT CLASS=dogskininputreadonly readonly style="top:5px;left:140px;width:110px"  >
		<INPUT CLASS=dogskininputreadonly readonly ID="librec-con" value = "<?=$_libsocreg?>" style="left:279px;top:5px;width:300px">
	</DIV>
	<!-- code article -->
	<DIV  style="position:absolute;left:30px;top:23px">
		<P id="codart" style="top:7px;width:200px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codart" , "sl"); ?> </P>
		<INPUT CLASS=dogskininputreadonly readonly ID="DICTLINK�libart_1�libel" style="left:279px;top:5px;width:300px">
		<INPUT ID="DICT�codart" onFocus="initAutoComplete($('DICT�codart'),'CODART,LIBART_1','CODART','',250,'3')" style="top:5px;left:140px;width:110px" TYPE=TEXT MAXLENGTH=6 CLASS=dogskininput NATURE=SEARCH title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
	</DIV>
	<DIV  style="position:absolute;left:300px;top:40px">
		<IMG id="razide" src="<?=$dealgateRelPath?>/site/img/transparent.png" class="navImg tailleImg30 gommeImg30" 		style="position:absolute;bottom:5px;left:605px;cursor:pointer" onclick="RazGen()" title="Quitter la saisie (Echap)">
		<IMG id="supide" src="<?=$dealgateRelPath?>/site/img/transparent.png" class="navImg tailleImg30 poubelleImg30" 		style="position:absolute;bottom:5px;left:570px;cursor:pointer" onclick="DeleteArticl('DELETE')" title="Supprimer (Suppr)">
		<IMG id="majide" src="<?=$dealgateRelPath?>/site/img/transparent.png" class="navImg tailleImg30 validImg30" 		style="position:absolute;bottom:5px;left:535px;cursor:pointer" onclick="UpdateArticl()" title="Valider (F2)">
		<IMG id="rafide" src="<?=$dealgateRelPath?>/site/img/transparent.png" class="navImg tailleImg30 refreshImg30" 		style="position:absolute;bottom:5px;left:500px;cursor:pointer" onclick="UpdateArticl('RAFF')" title="Rafraichissement">
		<IMG id="bt_lie" SRC="<?=$dealgateRelPath?>/site/img/di-link.png" title="lien bureautique" style="position:absolute;bottom:5px;left:465px;width:30px;height:30px;cursor:pointer" onMouseOut="this.src='<?=$dealgateRelPath?>/site/img/di-link.png'" onMouseOver="this.src='<?=$dealgateRelPath?>/site/img/di-link-over.png'"  onClick="ScanEntete()"></IMG>
		<IMG id="BUT_DUPART" src="<?=$dealgateRelPath?>/site/img/transparent.png" class="navImg tailleImg30 dupliImg30" 		style="position:absolute;bottom:5px;left:430px;cursor:pointer" onclick="affichDuplication()" title="Duplication">
	</DIV>
	<div class="divArrondi39" onmouseover="this.className='divArrondi3'" onmouseout="this.className='divArrondi39'" ID="BUT_ARTFOU" style="position:absolute;bottom:27px;left:5px;width:120px;height:18px;cursor:pointer" onclick="envoi_article_fou()">
		<P class=dogskinbodyfont style="top:2px;text-align:center;font-weight:600;width:120px">Acc�s Tarif Vente</P>
	</div>
	<div class="divArrondi39" onmouseover="this.className='divArrondi3'" onmouseout="this.className='divArrondi39'" ID="BUT_ARTFOU_2" style="position:absolute;bottom:27px;left:5px;width:120px;height:18px;cursor:pointer;left:140px" onclick="envoi_taxe()">
		<P class=dogskinbodyfont style="top:2px;text-align:center;font-weight:600;width:120px">Acc�s Taxes</P>
	</div>
	<div class="divArrondi39" onmouseover="this.className='divArrondi3'" onmouseout="this.className='divArrondi39'" ID="BUT_GENCOD" style="position:absolute;bottom:27px;left:5px;width:120px;height:18px;cursor:pointer;left:275px" onclick="envoi_gencod()">
		<P class=dogskinbodyfont style="top:2px;text-align:center;font-weight:600;width:120px">Acc�s Gencods</P>
	</div>
	<div class="divArrondi39" onmouseover="this.className='divArrondi3'" onmouseout="this.className='divArrondi39'" ID="BUT_ARTVTE" style="position:absolute;bottom:27px;left:5px;width:120px;height:18px;cursor:pointer;left:410px" onclick="envoi_article_ach()">
		<P class=dogskinbodyfont style="top:2px;text-align:center;font-weight:600;width:120px">Acces Tarif Achat</P>
	</div>
	<div class="divArrondi39" onmouseover="this.className='divArrondi3'" onmouseout="this.className='divArrondi39'" ID="BUT_ARTVTE" style="position:absolute;bottom:27px;left:5px;width:120px;height:18px;cursor:pointer;left:545px" onclick="envoi_article_recette()">
		<P class=dogskinbodyfont style="top:2px;text-align:center;font-weight:600;width:120px">Recette</P>
	</div>
    <div class="divArrondi39" onmouseover="this.className='divArrondi3'" onmouseout="this.className='divArrondi39'" ID="BUT_ARTVTE" style="position:absolute;bottom:27px;left:5px;width:120px;height:18px;cursor:pointer;left:675px" onclick="article_varlog()">
		<P class=dogskinbodyfont style="top:2px;text-align:center;font-weight:600;width:120px">Variantes logistiques</P>
	</div>
	<div class="divArrondi39" onmouseover="this.className='divArrondi3'" onmouseout="this.className='divArrondi39'" ID="BUT_ARTVTE" style="position:absolute;bottom:27px;left:5px;width:120px;height:18px;cursor:pointer;left:810px" onclick="seuilMagasin()">
		<P class=dogskinbodyfont style="top:2px;text-align:center;font-weight:600;width:120px">Seuil par magasin</P>
	</div>
	<div class="divArrondi39" onmouseover="this.className='divArrondi3'" onmouseout="this.className='divArrondi39'" ID="BUT_ARTFOU" style="position:absolute;bottom:2px;left:5px;width:120px;height:18px;cursor:pointer" onclick="substitutions()">
		<P class=dogskinbodyfont style="top:2px;text-align:center;font-weight:bold;width:120px">Substitutions</P>
	</div>
	<div class="divArrondi39" onmouseover="this.className='divArrondi3'" onmouseout="this.className='divArrondi39'" ID="BUT_ARTMAG" style="position:absolute;bottom:2px;left:140px;width:120px;height:18px;cursor:pointer" onclick="article_magasin()">
		<P class=dogskinbodyfont style="top:2px;text-align:center;font-weight:bold;width:120px">Magasin</P>
	</div>
	<div class="divArrondi39" onmouseover="this.className='divArrondi3'" onmouseout="this.className='divArrondi39'" ID="BUT_FICHSOC" style="visibility:hidden; position:absolute;bottom:2px;left:275px;width:120px;height:18px;cursor:pointer" onclick="ctrlFicheSoc('AFFICHER')">
		<P class=dogskinbodyfont style="top:2px;text-align:center;font-weight:bold;width:120px">Donn�es soci�t�</P>
	</div>
</DIV>

<!------------------------------------------------------ Onglets ------------------------------------------------------>
	<div id=ong1 class="divArrondi16" style="position:absolute;top:120px;left:5px;width:85px;height:45px;cursor:pointer" onClick="loadOng('1')">
		<P class=dogskinbodyfont style="top:2px;font-weight:600;width:85px;text-align:center"><?PHP print lect_bin::getLabel ( "mess-prognat2_44" , "sl"); ?></P>
	</div>
	<div id=ong2 class="divArrondi16" style="position:absolute;top:120px;left:90px;width:85px;height:45px;cursor:pointer" onClick="loadOng('2')">
		<P class=dogskinbodyfont style="top:2px;font-weight:600;width:85px;text-align:center"><?PHP print lect_bin::getLabel ( "mess-prognat2_45" , "sl"); ?></P>
	</div>		
	<div id=ong3 class="divArrondi16" style="position:absolute;top:120px;left:175px;width:85px;height:45px;cursor:pointer" onClick="loadOng('3')">
		<P class=dogskinbodyfont style="top:2px;font-weight:600;width:85px;text-align:center"><?PHP print lect_bin::getLabel ( "mess-prognat2_46" , "sl"); ?></P>
	</div>		
	<div id=ong4 class="divArrondi16" style="position:absolute;top:120px;left:260px;width:85px;height:45px;cursor:pointer" onClick="loadOng('4')">
		<P class=dogskinbodyfont style="top:2px;font-weight:600;width:85px;text-align:center"><?PHP print lect_bin::getLabel ( "mess-prognat2_90" , "sl"); ?></P>
	</div>		
	<div id=ong5 class="divArrondi16" style="position:absolute;top:120px;left:345px;width:85px;height:45px;cursor:pointer" onClick="loadOng('5')">
		<P class=dogskinbodyfont style="top:2px;font-weight:600;width:85px;text-align:center"><?PHP print lect_bin::getLabel ( "mess-prognat2_47" , "sl"); ?></P>
	</div>		
	<div id=ong6 class="divArrondi16" style="position:absolute;top:120px;left:430px;width:85px;height:45px;cursor:pointer" onClick="loadOng('6')">
		<P class=dogskinbodyfont style="top:2px;font-weight:600;width:85px;text-align:center"><?PHP print lect_bin::getLabel ( "mess-prognat2_48" , "sl"); ?></P>
	</div>		
	<div id=ong7 class="divArrondi16" style="position:absolute;top:120px;left:515px;width:85px;height:45px;cursor:pointer" onClick="loadOng('7')">
		<P class=dogskinbodyfont style="top:2px;font-weight:600;width:85px;text-align:center"><?PHP print lect_bin::getLabel ( "mess-prognat2_53" , "sl"); ?></P>
	</div>		
	<div id=ong8 class="divArrondi16" style="position:absolute;top:120px;left:600px;width:85px;height:45px;cursor:pointer" onClick="loadOng('8')">
		<P class=dogskinbodyfont style="top:2px;font-weight:600;width:85px;text-align:center">Danger</P>
	</div>		
	<div id=ong9 class="divArrondi16" style="position:absolute;top:120px;left:685px;width:85px;height:45px;cursor:pointer" onClick="loadOng('9')">
		<P class=dogskinbodyfont style="top:2px;font-weight:600;width:85px;text-align:center"><?=lect_bin::getLabel ("mess-prognat2_50", "sl");?></P>
	</div>		
	<div id=ong10 class="divArrondi16" style="position:absolute;top:120px;left:770px;width:85px;height:45px;cursor:pointer" onClick="loadOng('10')">
		<P class=dogskinbodyfont style="top:2px;font-weight:600;width:80px;text-align:center"><?=lect_bin::getLabel ("mess-auxges_89", "sl");?></P>
	</div>	
	<div id=ong11 class="divArrondi16" style="position:absolute;top:120px;left:855px;width:85px;height:45px;cursor:pointer" onClick="loadOng('11')">
		<P class=dogskinbodyfont style="top:2px;font-weight:600;width:85px;text-align:center"><?=lect_bin::getLabel ("mess-prognat2_93", "sl");?></P>
	</div>		
<!------------------------------------------------------ Fin Onglets ------------------------------------------------------>

<!-- //ONGLET 1// -->
<DIV ID="1title" class=divArrondiDeg28 style="position:absolute;width:950px;height:470px;top:150px;left:5px" >
	
	<DIV class=divArrondi29 style="position:absolute;width:630px;height:95px;top:5px;left:5px" >
		
		<DIV  style="position:absolute;left:5px;top:5px">
			<P id="libart1" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "libart_1" , "sl"); ?> </P>
			<INPUT ID="DICT�libart_1" style="top:5px;left:180px;width:300px"   TYPE=TEXT  CLASS=dogskininput >
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:25px">
			<P id="libart2" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "libart_2" , "sl"); ?> </P>
			<INPUT ID="DICT�libart_2" style="top:5px;left:180px;width:300px"   TYPE=TEXT CLASS=dogskininput >
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:45px">
			<P id="geslibel" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "geslibel" , "sl"); ?> </P>
			<SELECT id="DICT�geslibel" CLASS=dogskininput style="top:5px;left:180px;width:80px">
				<?PHP deal_commun::charge_select("geslibel",""); ?>
			</SELECT>
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:65px">
			<P id="libabr" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "libabr" , "sl"); ?> </P>
			<INPUT ID="DICT�libabr" TYPE=TEXT  CLASS=dogskininput style="top:5px;left:180px;width:300px" >
		</DIV>
	</div>
	<DIV class=divArrondi29 style="position:absolute;width:630px;height:55px;top:105px;left:5px" >
	
		<DIV  style="position:absolute;left:5px;top:5px">
			<P id="condit" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "condit" , "sl"); ?> </P>
			<INPUT ID="DICT�condit" TYPE=TEXT CLASS=dogskininput  style="top:5px;left:180px;width:80px" NATURE=SEARCH>
			<INPUT CLASS=dogskininputreadonly readonly ID="DICTLINK�condit�libel" style="left:289px;top:5px;width:191px">
		</DIV>

		<DIV  style="position:absolute;left:5px;top:25px">
			<P id="typ-condt" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "typ-condt" , "sl"); ?> </P>
			<INPUT ID="DICT�typ-condt" TYPE=TEXT CLASS=dogskininput  style="top:5px;left:180px;width:80px" NATURE=SEARCH >
			<INPUT CLASS=dogskininputreadonly readonly ID="DICTLINK�typ-condt�libel" style="left:289px;top:5px;width:191px">
		</DIV>
	</div>
	<DIV class=divArrondi29 style="position:absolute;width:630px;height:190px;top:165px;left:5px" >
		<DIV  style="position:absolute;left:5px;top:5px">
			<P id="codbloc" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "cer-blocage" , "sl"); ?> </P>
			<SELECT id="DICT�codbloc"  style="top:5px;left:180px" CLASS=dogskininputreadonly readonly disabled>
			<?PHP deal_commun::charge_select("cer-blocage",""); ?>
			</SELECT>
		</DIV>
		<DIV  style="position:absolute;left:5px;top:25px"> 
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "datapp" , "sl"); ?> du blocage</P>
			<INPUT CLASS=dogskininputreadonly readonly  ID="DICT�datebloc" style="left:180px;top:5px;width:80px"  NATURE=DATE>
		</DIV>

		<DIV  style="position:absolute;left:5px;top:45px">
			<P id="codebloc_1" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codebloc_1" , "sl"); ?> </P>
			<SELECT ID="DICT�codebloc_1" CLASS=dogskininputreadonly readonly disabled style="top:5px;left:180px;width:250px" >	
				<option value="0" selected>Oui</option>
				<option value="1" ><?PHP print lect_bin::getLabel ( "codebloc_5" , "sl"); ?></option>        	    
			</SELECT>
		</DIV>

		<DIV  style="position:absolute;left:5px;top:65px">
			<P id="codebloc_2" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codebloc_2" , "sl"); ?> </P>
			<SELECT ID="DICT�codebloc_2" CLASS=dogskininputreadonly readonly disabled style="top:5px;left:180px;width:250px" >	
				<option value="0" selected>Oui</option>
				<option value="1" ><?PHP print lect_bin::getLabel ( "codebloc_6" , "sl"); ?></option>        	    
			</SELECT>
		</DIV>

		<DIV  style="position:absolute;left:5px;top:85px">
			<P id="codebloc_3" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codebloc_3" , "sl"); ?> </P>
			<SELECT ID="DICT�codebloc_3" CLASS=dogskininputreadonly readonly disabled style="top:5px;left:180px;width:250px" >	
				<option value="0" selected>Oui</option>
				<option value="1" ><?PHP print lect_bin::getLabel ( "codebloc_7" , "sl"); ?></option>        	    
			</SELECT>
		</DIV>       	

		<DIV  style="position:absolute;left:5px;top:105px">
			<P id="codebloc_4" style="top:7px;width:250px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codebloc_4" , "sl"); ?> </P>
			<SELECT ID="DICT�codebloc_4" CLASS=dogskininputreadonly readonly disabled style="top:5px;left:180px;width:250px" >	
				<option value="0" selected>Oui</option>
				<option value="1" ><?PHP print lect_bin::getLabel ( "codebloc_8" , "sl"); ?></option>        	    
			</SELECT>
		</div>

		<DIV  style="position:absolute;left:5px;top:125px">
			<P id="autovtcpt" style="top:7px;width:250px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "autovtcpt" , "sl"); ?> </P>
			<SELECT ID="DICT�autovtcpt" CLASS=dogskininputreadonly readonly disabled style="top:5px;left:180px;width:250px" value=""><?PHP deal_commun::charge_select("combo-ouinon-on2",""); ?></SELECT>
		</div>


		<DIV  style="position:absolute;left:5px;top:145px">
			<P id="autotypcom" style="top:7px;width:250px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "autotypcom" , "sl"); ?></p>
			<INPUT ID="DICT�autotypcom" TYPE=TEXT CLASS=dogskininputreadonly readonly  style="top:5px;left:180px;width:250px" />
		</div>
		
		<DIV  style="position:absolute;left:5px;top:165px">
			<P id="autovtcpt" style="top:7px;width:250px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "autorisoc" , "sl"); ?></p>
			<INPUT ID="DICT�autsoc" TYPE=TEXT CLASS=dogskininputreadonly readonly  style="top:5px;left:180px;width:250px" NATURE="SEARCH" />
		</div>
		
	</div>
		

	<DIV class=divArrondi29 style="position:absolute;width:630px;height:100px;top:360px;left:5px" >
		<DIV  style="position:absolute;left:5px;top:5px">
			<P id="txabat" style="top:7px;width:250px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "txabat" , "sl"); ?> </P>
			<INPUT ID="DICT�txabat" TYPE=TEXT CLASS=dogskininput  style="top:5px;left:180px;width:80px"  >
		</DIV>

		<DIV  style="position:absolute;left:5px;top:25px">
			<P id="nbjret" style="top:7px;width:250px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "nbjret" , "sl"); ?> </P>
			<INPUT ID="DICT�nbjret" TYPE=TEXT CLASS=dogskininput  style="top:5px;left:180px;width:80px"  >
		</DIV>

		<DIV  style="position:absolute;left:5px;top:45px">
			<P id="blavoir" style="top:7px;width:250px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "blavoir" , "sl"); ?> </P>
			<SELECT id="DICT�blavoir" style="top:5px;left:180px;width:80px" CLASS=dogskininput >
			<?PHP deal_commun::charge_select("blavoir",""); ?>
			</SELECT>
		</DIV>

		<DIV  style="position:absolute;left:5px;top:65px">
			<P id="carbu" style="top:7px;width:250px" class=dogskinbodyfont>Article Carburant </P>
			<SELECT id="DICT�carbu"  CLASS=dogskininput style="top:5px;left:180px;width:80px" >
			<?PHP deal_commun::charge_select("combo-ouinon-on",""); ?>
			</SELECT>
		</DIV>
	</DIV>
		<div id="divImage" class="divArrondi" style="position:absolute;top:5px;right:5px;width:300px;height:400px;display:;">
			<div id="divImage1" class="divArrondiWhite" style="position:absolute;top:10px;left:10px;width:280px;height:340px;">
			</div>
			<iframe id="IBlob" name="IBlob" style="position:absolute;bottom:-15px;left:10px;height:60px;width:600px;display:none;" src="<?=search::searchExe('ds_annexe_w3c.php', 1) . '?' . $ses;?>" frameborder=0>
			</iframe>
			
		</div>
</div>

<!-- //ONGLET 2// -->
<DIV ID="2title" class=divArrondiDeg28 style="position:absolute;width:950px;height:240px;top:150px;left:5px" >
	
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:35px;top:5px;left:5px" >
		<DIV  style="position:absolute;left:5px;top:5px">
			<P id="unisto" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "unisto" , "sl"); ?> </P>
			<INPUT ID="DICT�unisto" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:80px"  NATURE=SEARCH >
			<INPUT CLASS=dogskininputreadonly readonly ID="DICTLINK�unisto�libel" style="left:249px;top:5px;width:300px" >
		</DIV>
	</div>
	
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:185px;top:45px;left:5px" >
		<P class=dogskinbodyfont style="top:5px;font-weight:600;width:85px;left:30px">Ventes</P>
	
		<DIV  style="position:absolute;left:5px;top:30px">
			<P id="unicde" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "unicde" , "sl"); ?> </P>
			<INPUT ID="DICT�unicde" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:80px"  NATURE=SEARCH>
			<INPUT CLASS=dogskininputreadonly readonly ID="DICTLINK�unicde�libel" style="left:249px;top:5px;width:300px" >
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:50px">
			<P id="coefcde" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "coefcde" , "sl"); ?> </P>
			<INPUT ID="DICT�coefcde" TYPE=TEXT style="top:5px;left:140px;width:80px"  CLASS=dogskininput >
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:70px">
			<P id="unifac" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "unifac" , "sl"); ?> </P>
			<INPUT ID="DICT�unifac" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:80px"  NATURE=SEARCH>
			<INPUT CLASS=dogskininputreadonly readonly ID="DICTLINK�unifac�libel" style="left:249px;top:5px;width:300px" >
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:90px">
			<P id="coeffac" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "coeffac" , "sl"); ?> </P>
			<INPUT ID="DICT�coeffac" TYPE=TEXT style="top:5px;left:140px;width:80px" CLASS=dogskininput   >
		</DIV>

		<DIV  style="position:absolute;left:5px;top:110px">
			<P id="unihec" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "unihec" , "sl"); ?> </P>
			<INPUT ID="DICT�unihec" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:80px"   >      		
		</DIV>

		<DIV  style="position:absolute;left:300px;top:110px">
			<P id="homhec" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "homhec" , "sl"); ?> </P>
			<INPUT ID="DICT�homhec" TYPE=TEXT CLASS=dogskininput style="top:5px;left:150px;width:80px"  >
			
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:130px">
			<P id="surcondit" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "surcondit" , "sl"); ?> </P>
			<INPUT ID="DICT�surcondit" TYPE=TEXT style="top:5px;left:140px;width:80px" CLASS=dogskininput >
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:150px">
			<P id="typ-surcond" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "typ-surcond" , "sl"); ?> </P>
			<SELECT id="DICT�typ-surcond" CLASS=dogskininput style="top:5px;left:140px" >
				<?PHP deal_commun::charge_select("typ-surcond",""); ?>
			</SELECT>
		</DIV>
	</DIV>     
	
</div>

<!-- //ONGLET 3// -->
<DIV ID="3title" class=divArrondiDeg28 style="position:absolute;width:950px;height:620px;top:150px;left:5px" >
	
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:180px;top:5px;left:5px" >
		<DIV  style="position:absolute;left:5px;top:5px">
			<P id="famart" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "famart" , "sl"); ?> </P>
			<INPUT ID="DICT�famart" NATURE=SEARCH TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:80px" onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�famart�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>
		<DIV  style="position:absolute;left:5px;top:25px">
			<P id="soufam" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "soufam" , "sl"); ?> </P>
			<INPUT ID="DICT�soufam" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput  style="top:5px;left:140px;width:80px" onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�soufam�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>
		<DIV  style="position:absolute;left:5px;top:45px">
			<P id="sssfam" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "sssfam" , "sl"); ?> </P>
			<INPUT ID="DICT�sssfam" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput  style="top:5px;left:140px;width:80px"  onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�sssfam�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>
		<DIV  style="position:absolute;left:5px;top:65px">
			<P id="sssnv4" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "sssnv4" , "sl"); ?> </P>
			<INPUT ID="DICT�sssnv4" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput  style="top:5px;left:140px;width:80px"  onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�sssnv4�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>
		<DIV  style="position:absolute;left:5px;top:85px">
			<P id="sssnv5" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "sssnv5" , "sl"); ?> </P>
			<INPUT ID="DICT�sssnv5" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput  style="top:5px;left:140px;width:80px"  onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�sssnv5�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>            	

		<DIV  style="position:absolute;left:5px;top:105px">
			<P id="famart2" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "famart2" , "sl"); ?> </P>
			<INPUT ID="DICT�famart2" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:80px" onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�famart2�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>

		<DIV  style="position:absolute;left:5px;top:125px">
			<P id="sfamart2" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "soufam2" , "sl"); ?> </P>
			<INPUT ID="DICT�sfamart2" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput  style="top:5px;left:140px;width:80px" onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�sfamart2�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>

		<DIV  style="position:absolute;left:5px;top:145px">
			<P id="ssfamart2" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "sssfam2" , "sl"); ?> </P>
			<INPUT ID="DICT�ssfamart2" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput  style="top:5px;left:140px;width:80px" onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�ssfamart2�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>
	</DIV>

	<DIV class=divArrondi29 style="position:absolute;width:940px;height:95px;top:190px;left:5px" >
		<DIV  style="position:absolute;left:5px;top:5px">
			<P id="type-mag" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "type-mag" , "sl"); ?> </P>
			<SELECT id="DICT�type-mag"  CLASS=dogskininput style="top:5px;left:140px;width:150px" >
			<?PHP deal_commun::charge_select("type-mag","0"); ?>
		   </SELECT>
		</DIV>

		 <DIV  style="position:absolute;left:5px;top:25px">
			<P id="type-client" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "type-client" , "sl"); ?> </P>
			<INPUT ID="DICT�type-client" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput  style="top:5px;left:140px;width:80px"  onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�type-client�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:45px">
			<P id="jardin" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "jardin" , "sl"); ?> </P>
			<SELECT id="DICT�jardin"  CLASS=dogskininput style="top:5px;left:140px;width:80px"  >
			<?PHP deal_commun::charge_select("jardin","0"); ?>
		   </SELECT>
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:65px">
			<P id="coeftaxe" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "coeftaxe" , "sl"); ?> </P>
			<INPUT ID="DICT�coeftaxe" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:80px"  >
		</DIV>
		<DIV  style="position:absolute;left:310px;top:65px">
			<P id="codeamm" style="top:7px" class=dogskinbodyfont>Code A.M.M. </P>
			<INPUT ID="DICT�codeamm" style="top:5px;left:140px;width:100px"   TYPE=TEXT CLASS=dogskininput>
		</DIV>
	
	</div>

	<DIV class=divArrondi29 style="position:absolute;width:940px;height:75px;top:290px;left:5px" >
		<DIV  style="position:absolute;left:5px;top:5px">
			<P id="artunion" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "art-union" , "sl"); ?> </P>
			<INPUT id="DICT�art-union"  CLASS=dogskininput style="top:5px;left:140px;width:80px" >
		</DIV>
		<DIV  style="position:absolute;left:5px;top:25px">
			<P id="douane" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "douane" , "sl"); ?> </P>
			<INPUT ID="DICT�douane" style="top:5px;left:140px;width:100px"   TYPE=TEXT CLASS=dogskininput >
		</DIV>
		<DIV  style="position:absolute;left:5px;top:45px">
			<P id="anccod1" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "douane_1" , "sl"); ?> </P>
			<INPUT ID="DICT�anccod1" style="top:5px;left:140px;width:100px"   TYPE=TEXT CLASS=dogskininput>
		</DIV>
		<DIV  style="position:absolute;left:300px;top:45px">
			<P id="anccod2" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "douane_2" , "sl"); ?> </P>
			<INPUT ID="DICT�anccod2" style="top:5px;left:140px;width:100px"   TYPE=TEXT CLASS=dogskininput>
		</DIV>
	</DIV>
	
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:210px;top:370px;left:5px" >
			
		<DIV  style="position:absolute;left:5px;top:5px">
			<P id="negatif" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "negatif" , "sl"); ?> </P>
			<SELECT id="DICT�negatif" style="top:5px;left:140px" CLASS=dogskininput  >
				<?PHP deal_commun::charge_select("negatif-car",""); ?>
			</SELECT>
		</DIV>

		<DIV  style="position:absolute;left:5px;top:25px">
			<P id="typart" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "typart-car" , "sl"); ?> </P>
			<INPUT ID="DICT�typart" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:80px" onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�typart�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>

		
		<DIV  style="position:absolute;left:5px;top:45px">
			<P id="gencod" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "gencod" , "sl"); ?> </P>
			<INPUT ID="DICT�gencod" style="top:5px;left:140px;width:300px"   TYPE=TEXT CLASS=dogskininput maxlength="13" />
		</DIV>
		
		<DIV  style="position:absolute;left:470px;top:45px">
			<P id="gtin14" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "gtin" , "sl"); ?> </P>
			<INPUT ID="DICT�gtin14" style="top:5px;left:140px;width:300px"   TYPE=TEXT CLASS=dogskininput maxlength="14" />
		</DIV>
	 
		 <DIV  style="position:absolute;left:5px;top:65px">
			<P id="matact" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "matact" , "sl"); ?> </P>
			<INPUT ID="DICT�matact" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput  style="top:5px;left:140px;width:300px"  onblur=controle_zone(this)>
		</DIV>

		<!-- ##Pte Remplacement de la zone famtech par "code classification activit�" -->
		<DIV  style="position:absolute;left:5px;top:85px">
			<P id="classact" style="top:7px" class=dogskinbodyfont>Classification activit�</P> 
			<INPUT ID="DICT�classact" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput  style="top:5px;left:140px;width:80px"  onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�classact�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>

		<DIV  style="position:absolute;left:5px;top:105px">
			<P id="nomencgv" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "nomencgv" , "sl"); ?> </P>
			<SELECT id="DICT�nomencgv"  CLASS=dogskininput style="top:5px;left:140px;width:80px" >
			<?PHP deal_commun::charge_select("nomencgv","0"); ?>
		   </SELECT>
		</DIV>
		
		
		<DIV  style="position:absolute;left:5px;top:125px">
			<P id="soc-apart" style="top:7px" class=dogskinbodyfont>Soci�t� d'Appartenance </P>
			<INPUT ID="DICT�soc-apart" style="top:5px;left:140px;width:100px"   TYPE=TEXT CLASS=dogskininput >
		</DIV>

		<DIV  style="position:absolute;left:5px;top:145px">
			<P id="categorie" style="top:7px" class=dogskinbodyfont>Cat�gorie</P>
			<INPUT ID="DICT�categorie" NATURE=SEARCH style="top:5px;left:140px;width:80px"   TYPE=TEXT CLASS=dogskininput >
			<INPUT ID="DICTLINK�categorie�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>
		<DIV  style="position:absolute;left:5px;top:165px">
			<P id="traitement" style="top:7px" class=dogskinbodyfont>Traitement </P>
			<INPUT ID="DICT�traitement" NATURE=SEARCH style="top:5px;left:140px;width:80px"   TYPE=TEXT CLASS=dogskininput >
			<INPUT ID="DICTLINK�traitement�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>

		<DIV  style="position:absolute;left:5px;top:185px">
			<P id="Epandage" style="top:7px" class=dogskinbodyfont>Epandage </P>
			<SELECT id="DICT�epandage" style="top:5px;left:140px" CLASS=dogskininput >
				<OPTION VALUE="N" >Non
				<OPTION VALUE="O" >Oui
			</SELECT>
		</DIV>
	</div>
</div>

<!-- //ONGLET 4// -->
<DIV ID="4title" class=divArrondiDeg28 style="position:absolute;width:950px;height:175px;top:150px;left:5px" >
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:160px;top:5px;left:5px" >
		<DIV  style="position:absolute;left:5px;top:5px">
			<P id="imputa" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "imputa" , "sl"); ?> </P>
			<INPUT ID="DICT�imputa" TYPE=TEXT CLASS=dogskininput  style="top:5px;left:140px;width:80px" NATURE=SEARCH onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�imputa�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:25px">
			<P id="imputv" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "imputv" , "sl"); ?> </P>
			<INPUT ID="DICT�imputv" TYPE=TEXT CLASS=dogskininput  style="top:5px;left:140px;width:80px"  NATURE=SEARCH onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�imputv�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:45px">
			<P id="codtva" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codtva" , "sl"); ?> </P>
			<INPUT ID="DICT�codtva" TYPE=TEXT CLASS=dogskininput  style="top:5px;left:140px;width:80px"  NATURE=SEARCH onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�codtva�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>
		<DIV  style="position:absolute;left:5px;top:65px">
			<P id="tvapre" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "tvapre" , "sl"); ?> </P>
			<INPUT ID="DICT�tvapre" TYPE=TEXT CLASS=dogskininput  style="top:5px;left:140px;width:80px" NATURE=SEARCH onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�tvapre�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>
		<DIV  style="position:absolute;left:5px;top:85px"> 
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "datchgtva" , "sl"); ?></P>
			<INPUT CLASS=dogskininput  ID="DICT�datchgtva" NATURE=DATE style="left:140px;top:5px;width:80px"  >
		</DIV>

		<DIV  style="position:absolute;left:5px;top:105px">
			<P id="fraisach" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "fraisach" , "sl"); ?> </P>
			<SELECT id="DICT�fraisach" style="top:5px;left:140px" CLASS=dogskininput >
				<?PHP deal_commun::charge_select("frais-achcar",""); ?>
			</SELECT>
		</DIV>
		<DIV  style="position:absolute;left:5px;top:125px">
			<P id="segana" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "segana_1" , "sl"); ?> </P>
			<INPUT ID="DICT�segana" TYPE=TEXT CLASS=dogskininput  style="top:5px;left:140px;width:80px"  NATURE=SEARCH onblur=controle_zone(this)>
			<INPUT ID="DICTLINK�segana�libel" style="left:249px;top:5px;width:300px" TYPE=TEXT CLASS=dogskininputreadonly readonly>
		</DIV>
	</div>
</div>

<!-- //ONGLET 5// -->
<DIV ID="5title" class=divArrondiDeg28 style="position:absolute;width:950px;height:240px;top:150px;left:5px" >
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:80px;top:5px;left:5px" >
		<P class=dogskinbodyfont style="top:5px;font-weight:600;width:85px;left:30px">Homologation</P>
		<DIV  style="position:absolute;left:5px;top:30px">
			<P id="cathomo" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "cathomo" , "sl"); ?> </P>
			<INPUT ID="DICT�cathomo" TYPE=TEXT  style="top:5px;left:140px;width:80px" CLASS=dogskininput  >
		</DIV>

		<DIV  style="position:absolute;left:5px;top:50px">
			<P id="nohomo" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "nohomo" , "sl"); ?> </P>
			<INPUT ID="DICT�nohomo" TYPE=TEXT  style="top:5px;left:140px;width:80px" CLASS=dogskininput >
		</DIV>
	</div>
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:140px;top:90px;left:5px" >
		<P class=dogskinbodyfont style="top:5px;font-weight:600;width:85px;left:30px">Divers</P>
		 <DIV  style="position:absolute;left:5px;top:30px">
			<P id="editar" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "editar" , "sl"); ?> </P>
			<SELECT id="DICT�editar"  CLASS=dogskininput style="top:5px;left:140px;width:80px" >
			<?PHP deal_commun::charge_select("editar",""); ?>
			</SELECT>
		</DIV>

		<DIV  style="position:absolute;left:350px;top:30px">
			<P id="edietiq" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "edietiq" , "sl"); ?> </P>
			<SELECT id="DICT�edietiq"  CLASS=dogskininput style="top:5px;left:140px;width:80px" >
			<?PHP deal_commun::charge_select("edietiq",""); ?>
			</SELECT>
		</DIV>

		<DIV  style="position:absolute;left:350px;top:50px">
			<P id="pxvte-mod" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "pxvtemod" , "sl"); ?> </P>
			<SELECT id="DICT�pxvte-mod"  CLASS=dogskininput style="top:5px;left:140px;width:80px" >
			<?PHP deal_commun::charge_select("combo-ouinon-on2",""); ?>
			</SELECT>
		</DIV>

		<DIV id="taux-union" style="position:absolute;left:5px;top:50px">
			<P style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "taux-union" , "sl"); ?> </P>
			<INPUT ID="DICT�taux-union" maxlength=6 TYPE=TEXT style="top:5px;left:140px;width:80px" CLASS=dogskininput  >
		</DIV>

		<DIV id="fourchpx" style="position:absolute;left:5px;top:70px">
			<P style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "fourchpx" , "sl"); ?> </P>
			<INPUT ID="DICT�fourchpx" TYPE=TEXT style="top:5px;left:140px;width:80px" CLASS=dogskininput  >
		</DIV>

		<DIV  style="position:absolute;left:5px;top:90px">
			<P id="franco" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "franco" , "sl"); ?> </P>
			<SELECT id="DICT�franco"  CLASS=dogskininput style="top:5px;left:140px;width:80px" >
			<?PHP deal_commun::charge_select("franco",""); ?>
			</SELECT>
		</DIV>

		<DIV  style="position:absolute;left:5px;top:110px">
			<P id="typetrp" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "typtrp" , "sl"); ?> </P>
			<INPUT ID="DICT�typetrp" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:110px" >
			<INPUT ID="DICTLINK�typetrp�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:279px;top:5px;width:300px">
		</DIV>
	</div>
</div>

<!-- //ONGLET 6// -->
<DIV ID="6title" class=divArrondiDeg28 style="position:absolute;width:950px;height:520px;top:150px;left:5px" >
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:380px;top:5px;left:5px" >
		<P class=dogskinbodyfont style="top:5px;font-weight:600;width:150px;left:30px"><?PHP print lect_bin::getLabel ( "mess-gesartnat_17" , "sl"); ?></P>
		<DIV  style="position:absolute;left:5px;top:30px">
			<P id="codart-stat" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codart-stat" , "sl"); ?> </P>
			<INPUT ID="DICT�codart-stat" NATURE=SEARCH TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:110px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�codart-stat�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:279px;top:5px;width:300px">
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:50px">
			<P id="coef-stat" style="top:7px" class=dogskinbodyfont>Coefficient Statistique </P>
			<INPUT ID="DICT�coef-stat" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:80px" >
		</DIV>
		
		<DIV  style="position:absolute;left:285px;top:50px">
			<P id="coef-stat" style="top:7px" class=dogskinbodyfont>Unit� Statistique </P>
			<INPUT ID="DICT�unite_stat" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:80px" >
		</DIV>
				
		<DIV  style="position:absolute;left:5px;top:70px">
			<P id="codart-rat" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codart-rat" , "sl"); ?> </P>
			<INPUT ID="DICT�codart-rat" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:110px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" NATURE=SEARCH>
			<INPUT ID="DICTLINK�codart-rat�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:279px;top:5px;width:300px">
		</DIV>

		<DIV  style="position:absolute;left:5px;top:90px">
			<P id="generique" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "generique" , "sl"); ?> </P>
			<SELECT id="DICT�generique"  CLASS=dogskininput style="top:5px;left:140px;width:120px" >
			<?PHP deal_commun::charge_select("generique","N"); ?>
			</SELECT>
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:110px">
			<P id="codart-stoc" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codart-stoc" , "sl"); ?> </P>
			<INPUT ID="DICT�codart-stoc" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:110px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" NATURE=SEARCH>
			<INPUT ID="DICTLINK�codart-stoc�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:279px;top:5px;width:300px">
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:130px">
			<P id="codart-tar" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codart-tar" , "sl"); ?> </P>
			<INPUT ID="DICT�codart-tar" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:110px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" NATURE=SEARCH>
			<INPUT ID="DICTLINK�codart-tar�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:279px;top:5px;width:300px">
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:150px">
			<P id="codart-rem" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codart-rem" , "sl"); ?> </P>
			<INPUT ID="DICT�codart-rem" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:110px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" NATURE=SEARCH>
			<INPUT ID="DICTLINK�codart-rem�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:279px;top:5px;width:300px">
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:170px">
			<P id="codart-lie" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codart-lie" , "sl"); ?> </P>
			<INPUT ID="DICT�codart-lie" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:110px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" NATURE=SEARCH>
			<INPUT ID="DICTLINK�codart-lie�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:279px;top:5px;width:300px">
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:190px">
			<P id="codart-remp_1" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codart-rempl_1" , "cl"); ?> </P>
			<INPUT ID="DICT�codart-remp_1" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:110px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" NATURE=SEARCH>
			<INPUT ID="DICTLINK�codart-remp_1�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:279px;top:5px;width:300px">
		</DIV>

		<DIV  style="position:absolute;left:5px;top:210px"> 
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "datchgart" , "sl"); ?></P>
			<INPUT CLASS=dogskininput  ID="DICT�datchgart" NATURE=DATE style="left:140px;top:5px;width:80px"  >
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:230px">
			<P id="codart-remp_2" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codart-rempl_2" , "cl"); ?> </P>
			<INPUT ID="DICT�codart-remp_2" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:110px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" NATURE=SEARCH>
			<INPUT ID="DICTLINK�codart-remp_2�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:279px;top:5px;width:300px">
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:250px">
			<P id="codart-remp_3" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codart-rempl_3" , "cl"); ?> </P>
			<INPUT ID="DICT�codart-remp_3" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:110px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" NATURE=SEARCH>
			<INPUT ID="DICTLINK�codart-remp_3�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:279px;top:5px;width:300px">
		</DIV>

		<DIV  style="position:absolute;left:5px;top:270px">
			<P id="codart-cvo" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "articl-cvo" , "sl"); ?> </P>
			<SELECT id="DICT�codart-cvo"  CLASS=dogskininput style="top:5px;left:140px;width:80px" >
			<?PHP deal_commun::charge_select("articl-cvo",""); ?>
			</SELECT>
		</DIV>

		<DIV  style="position:absolute;left:5px;top:290px">
			<P id="code-cumul" style="top:7px" class=dogskinbodyfont>Code Cumul </P>
			<INPUT ID="DICT�code-cumul" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:110px"  NATURE=SEARCH>
			<INPUT ID="DICTLINK�code-cumul�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:279px;top:5px;width:300px">
		</DIV>

		<DIV  style="position:absolute;left:5px;top:310px">
			<P id="corapport" style="top:7px" class=dogskinbodyfont>Correspondance Apport </P>
			<INPUT ID="DICT�corapport" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:80px" >
		</DIV>

		<DIV  style="position:absolute;left:5px;top:330px">
			<P id="corechsem" style="top:7px" class=dogskinbodyfont>Cor. Apport Ech. Sem.</P>
			<INPUT ID="DICT�corechsem" TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:80px" >
		</DIV>

		<DIV  style="position:absolute;left:5px;top:350px;visibility:hidden">
			<P id="supplementation" style="top:7px" class=dogskinbodyfont>Article Suppl�mentation </P>
			<SELECT id="DICT�supplementation"  CLASS=dogskininput style="top:5px;left:140px;width:140px" >
			<?PHP deal_commun::charge_select("combo-supplementation",""); ?>
			</SELECT>
		</DIV>

		<DIV  style="position:absolute;left:350px;top:350px;visibility:hidden">
			<P id="soulte" style="top:7px" class=dogskinbodyfont>Article Soulte </P>
			<SELECT id="DICT�soulte"  CLASS=dogskininput style="top:5px;left:80px;width:140px">
			<?PHP deal_commun::charge_select("combo-soulte",""); ?>
			</SELECT>
		</DIV>
	</div>
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:120px;top:390px;left:5px" >
		<P class=dogskinbodyfont style="top:5px;font-weight:600;width:150px;left:30px"><?PHP print lect_bin::getLabel ( "mess-gesartnat_16" , "sl"); ?></P>
		<DIV  style="position:absolute;left:5px;top:30px">
			<P id="codaux-fab" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codaux-fab" , "sl"); ?> </P>
			<INPUT ID="DICT�typaux-fab" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:80px" >
			<INPUT ID="DICT�codaux-fab" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:249px;width:80px" >
			<INPUT ID="DICTLINK�codaux-fab�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:140px;top:25px;width:409px">
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:70px">
			<P id="codaux-princ" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codaux-princ" , "sl"); ?> </P>
			<INPUT ID="DICT�typaux-princ" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:80px"  >
			<INPUT ID="DICT�codaux-princ" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:249px;width:80px" >
			<INPUT ID="DICTLINK�codaux-princ�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:140px;top:25px;width:409px">
		</DIV>
	</div>
</div>

<!-- //ONGLET 7// -->
<DIV ID="7title" class=divArrondiDeg28 style="position:absolute;width:950px;height:255px;top:150px;left:5px" >
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:240px;top:5px;left:5px" >
        <DIV  style="position:absolute;left:5px;top:5px">
			<P id="tenulot" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "cer-tenulot" , "sl"); ?> </P>
			<SELECT id="DICT�tenulot" style="top:5px;left:167px" CLASS=dogskininput >
				<?PHP deal_commun::charge_select("cer-tenulot",""); ?>
			</SELECT>
		</DIV>
        <DIV  style="position:absolute;left:320px;top:5px">
			<P id="gesstock" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "gesstock" , "sl"); ?> </P>
			<SELECT id="DICT�gesstock" style="top:5px;left:150px" CLASS=dogskininput >
				<?PHP deal_commun::charge_select("gesstockart",""); ?>
			</SELECT>
		</DIV>


		<DIV  style="position:absolute;left:5px;top:25px">
			<P id="stock-min" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "stock-min" , "sl"); ?> </P>
			<INPUT ID="DICT�stock-min" TYPE=TEXT style="top:5px;left:167px;width:80px" CLASS=dogskininput  >
		</DIV>
		
		<DIV  style="position:absolute;left:320px;top:25px">
			<P id="stock-max" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "stock-max" , "sl"); ?> </P>
			<INPUT ID="DICT�stock-max" TYPE=TEXT style="top:5px;left:150px;width:80px" CLASS=dogskininput >
		</DIV>
		
		<DIV  style="position:absolute;left:5px;top:45px">
			<P id="stock-mip" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "stock-mip" , "sl"); ?> </P>
			<INPUT ID="DICT�stock-mip" TYPE=TEXT style="top:5px;left:167px;width:80px" CLASS=dogskininput  >
		</DIV>

		<DIV  style="position:absolute;left:5px;top:65px">
			<P id="moismini" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "cer-mois_1" , "sl"); ?> </P>
			<INPUT ID="DICT�moismini" TYPE=TEXT  style="top:5px;left:167px;width:80px" CLASS=dogskininput  >
		</DIV>
		
		<DIV  style="position:absolute;left:320px;top:65px">
			<P id="moismaxi" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "cer-mois_2" , "sl"); ?> </P>
			<INPUT ID="DICT�moismaxi" TYPE=TEXT style="top:5px;left:150px;width:80px" CLASS=dogskininput  >
		</DIV>

		<DIV  style="position:absolute;left:5px;top:85px">
			<P id="stock-alerte" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "stock-alert" , "sl"); ?> </P>
			<INPUT ID="DICT�stock-alerte" TYPE=TEXT style="top:5px;left:167px;width:80px" CLASS=dogskininput >
		</DIV>
		
		<DIV  style="position:absolute;left:320px;top:85px">
			<P id="qte-ecocde" style="top:7px;width:200px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "qte-ecocde" , "sl"); ?> </P>
			<INPUT ID="DICT�qte-ecocde" style="top:5px;left:150px;width:80px" TYPE=TEXT CLASS=dogskininput   >
		</DIV>

		<DIV  style="position:absolute;left:5px;top:105px">
			<P id="poids" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-gesartnat_28" , "sl"); ?></P>
			<INPUT ID="DICT�poids" TYPE=TEXT CLASS=dogskininput   title = "<? print lect_bin::getLabel ( "mess-gesartnat_29" , "sl"); ?>" style="top:5px;left:167px;width:80px" >
		</DIV>
		
		<DIV  style="position:absolute;left:320px;top:105px">
			<P id="volume" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "volume" , "sl"); ?> </P>
			<INPUT ID="DICT�volume" TYPE=TEXT style="top:5px;left:150px;width:80px" CLASS=dogskininput  >
		</DIV>
		<DIV  style="position:absolute;left:320px;top:125px">
			<P id="gestpds" style="top:7px" class=dogskinbodyfont>Gestion en poids </P>
			<SELECT id="DICT�gestpds" style="top:5px;left:150px" CLASS=dogskininput >
				<?PHP deal_commun::charge_select("combo-ouinon-1-0",""); ?>
			</SELECT>
		</DIV>

		<DIV  style="position:absolute;left:5px;top:125px">
			<P id="poids-brut" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "poids-brut" , "sl"); ?> </P>
			<INPUT ID="DICT�poids-brut" TYPE=TEXT CLASS=dogskininput   title = "<? print lect_bin::getLabel ( "mess-gesartnat_30" , "sl"); ?>" style="top:5px;left:167px;width:80px" >
		</DIV>

		<DIV  style="position:absolute;left:5px;top:145px">
			<P id="poids-net" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "poids-net" , "sl"); ?> </P>
			<INPUT ID="DICT�poids-net" CLASS=dogskininput   TYPE=TEXT title = "<? print lect_bin::getLabel ( "mess-gesartnat_31" , "sl"); ?>" style="top:5px;left:167px;width:80px" >
		</DIV>

		<DIV  style="position:absolute;left:5px;top:165px">
			<P id="emplac" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "emplac_1" , "sl"); ?> </P>
			<INPUT ID="DICT�emplac" TYPE=TEXT style="top:5px;left:167px;width:80px" CLASS=dogskininput  >
		</DIV>
		<DIV  style="position:absolute;left:5px;top:185px">
			<P id="stock-fab" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "stock-fab" , "sl"); ?> </P>
			<SELECT id="DICT�stock-fab" style="top:5px;left:167px" CLASS=dogskininput >
				<?PHP deal_commun::charge_select("stock-fab",""); ?>
			</SELECT>
		</DIV>
		<DIV  style="position:absolute;left:320px;top:185px">
			<P id="stock-dang" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "stock-dang" , "sl"); ?> </P>
			<SELECT id="DICT�stock-dang" style="top:5px;left:150px" CLASS=dogskininput>
				<?PHP deal_commun::charge_select("stock-dang",""); ?>
			</SELECT>
		</DIV>
		<DIV  style="position:absolute;left:5px;top:205px">
			<P  style="top:7px" class=dogskinbodyfont>Recette Unique </P>
			<SELECT id="DICT�recette-unique" style="top:5px;left:167px" CLASS=dogskininput >
				<OPTION VALUE="U" >Oui
				<OPTION VALUE="" SELECTED>Non
			</SELECT>
		</DIV>
		<DIV  style="position:absolute;left:320px;top:205px">
			<P  style="top:7px" class=dogskinbodyfont>Article Nomenclatur� </P>
			<SELECT id="DICT�art-nomenc" style="top:5px;left:150px" CLASS=dogskininput >
				<OPTION VALUE="" >Non
				<OPTION VALUE="O" >Oui
			</SELECT>
		</DIV>
	</div>
</div>

<!-- //ONGLET 8// -->
<DIV ID="8title" class=divArrondiDeg28 style="position:absolute;width:950px;height:580px;top:150px;left:5px" >
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:570px;top:5px;left:5px" >
		<DIV  style="position:absolute;left:5px;top:5px">
			<P id="classe" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_1" , "sl"); ?> </P>
			<INPUT ID="DICT�classe" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:80px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�classe�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:249px;top:5px;width:300px">
		</DIV>
		<DIV  style="position:absolute;left:5px;top:25px">
			<P id="classif" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_2" , "sl"); ?> </P>
			<INPUT ID="DICT�classif" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:80px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�classif�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:249px;top:5px;width:300px">
		</DIV>
		<DIV  style="position:absolute;left:5px;top:45px">
			<P id="toxicite" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_3" , "sl"); ?> </P>
			<INPUT ID="DICT�toxicite" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:80px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>">
			<INPUT ID="DICTLINK�toxicite�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:249px;top:5px;width:300px">
		</DIV>
	   <DIV  style="position:absolute;left:5px;top:65px">
			<P id="pointeclair" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_4" , "sl"); ?> </P>
			<INPUT ID="DICT�pointeclair" TYPE=TEXT style="top:5px;left:140px;width:80px" CLASS=dogskininput >
		</DIV>
		<DIV  style="position:absolute;left:330px;top:65px">
			<P id="tempfroid" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_5" , "sl"); ?> </P>
			<INPUT ID="DICT�tempfroid" TYPE=TEXT style="top:5px;left:140px;width:80px" CLASS=dogskininput  >
		</DIV>
		<DIV  style="position:absolute;left:640px;top:65px">
			<P id="tempchaud" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_6" , "sl"); ?> </P>
			<INPUT ID="DICT�tempchaud" TYPE=TEXT style="top:5px;left:140px;width:80px" CLASS=dogskininput  >
		</DIV>
		<DIV  style="position:absolute;left:5px;top:85px">
			<P id="consecu" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_7" , "sl"); ?> </P>
			<INPUT ID="DICT�consecu" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:80px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�consecu�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:249px;top:5px;width:300px">
		</DIV>
		<DIV  style="position:absolute;left:5px;top:105px">
			<P id="etiqdang_1" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_8" , "sl"); ?> 1</P>
			<INPUT ID="DICT�etiqdang_1" TYPE=TEXT style="top:5px;left:140px;width:80px" CLASS=dogskininput  >
		</DIV>
		<DIV  style="position:absolute;left:330px;top:105px">
			<P id="etiqdang_2" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_8" , "sl"); ?> 2</P>
			<INPUT ID="DICT�etiqdang_2" TYPE=TEXT style="top:5px;left:140px;width:80px" CLASS=dogskininput  >
		</DIV>
		 <DIV  style="position:absolute;left:640px;top:105px">
			<P id="etiqdang_3" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_8" , "sl"); ?> 3</P>
			<INPUT ID="DICT�etiqdang_3" TYPE=TEXT style="top:5px;left:140px;width:80px" CLASS=dogskininput >
		</DIV>
		<DIV  style="position:absolute;left:5px;top:125px">
			<P id="etiqdang_4" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_8" , "sl"); ?> 4</P>
			<INPUT ID="DICT�etiqdang_4" TYPE=TEXT style="top:5px;left:140px;width:80px" CLASS=dogskininput  >
		</DIV>
		 <DIV  style="position:absolute;left:330px;top:125px">
			<P id="etiqdang_5" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_8" , "sl"); ?> 5</P>
			<INPUT ID="DICT�etiqdang_5" TYPE=TEXT style="top:5px;left:140px;width:80px" CLASS=dogskininput  >
		</DIV>
		<DIV  style="position:absolute;left:5px;top:145px">
			<P id="cattrp" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_9" , "sl"); ?> </P>
			<INPUT ID="DICT�cattrp" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:80px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�cattrp�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:249px;top:5px;width:300px">
		</DIV>
		<DIV  style="position:absolute;left:5px;top:165px">
			<P id="symbdang_1" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_10" , "sl"); ?> 1</P>
			<INPUT ID="DICT�symbdang_1"  TYPE=TEXT CLASS=dogskininput NATURE=SEARCH style="top:5px;left:140px;width:80px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�symbdang_1�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:250px;top:5px;width:200px">
		</DIV>
		<DIV  style="position:absolute;left:490px;top:165px">
			<P id="symbdang_2" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_10" , "sl"); ?> 2</P>
			<INPUT ID="DICT�symbdang_2"  TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:120px;width:80px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�symbdang_2�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:230px;top:5px;width:200px">
		</DIV>
		 <DIV  style="position:absolute;left:5px;top:185px">
			<P id="symbdang_3" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_10" , "sl"); ?> 3</P>
			<INPUT ID="DICT�symbdang_3"  TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:80px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�symbdang_3�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:250px;top:5px;width:200px">
		</DIV>
		<DIV  style="position:absolute;left:490px;top:185px">
			<P id="symbdang_4" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_10" , "sl"); ?> 4</P>
			<INPUT ID="DICT�symbdang_4"  TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:120px;width:80px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�symbdang_4�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:230px;top:5px;width:200px">
		</DIV>
		 <DIV  style="position:absolute;left:5px;top:205px">
			<P id="symbdang_5" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_10" , "sl"); ?> 5</P>
			<INPUT ID="DICT�symbdang_5" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:80px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�symbdang_5�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:250px;top:5px;width:200px">
		</DIV>
		<DIV  style="position:absolute;left:5px;top:225px">
			<P id="etatphy" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_11" , "sl"); ?></P>
			<INPUT ID="DICT�etatphy"  TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:80px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�etatphy�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:249px;top:5px;width:200px">
		</DIV>
		 <DIV  style="position:absolute;left:490px;top:225px">
			<P id="codemb" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_12" , "sl"); ?></P>
			<INPUT ID="DICT�codemb" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:120px;width:80px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�codemb�libel" TYPE=TEXT focusblock = 8 CLASS=dogskininputreadonly readonly style="left:229px;top:5px;width:200px">
		</DIV>  
		<DIV  style="position:absolute;left:5px;top:245px">
			<P id="descrip_1" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_13" , "sl"); ?> 1</P>
			<INPUT ID="DICT�descrip_1" TYPE=TEXT style="top:5px;left:140px;width:310px" CLASS=dogskininput  >
		</DIV>
		<DIV  style="position:absolute;left:490px;top:245px">
			<P id="descrip_2" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_13" , "sl"); ?> 2</P>
			<INPUT ID="DICT�descrip_2" TYPE=TEXT style="top:5px;left:120px;width:310px" CLASS=dogskininput >
		</DIV>
		 <DIV  style="position:absolute;left:5px;top:265px">
			<P id="descrip_3" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_13" , "sl"); ?> 3</P>
			<INPUT ID="DICT�descrip_3" TYPE=TEXT style="top:5px;left:140px;width:310px" CLASS=dogskininput >
		</DIV>
		<DIV  style="position:absolute;left:490px;top:265px">
			<P id="descrip_4" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_13" , "sl"); ?> 4</P>
			<INPUT ID="DICT�descrip_4" TYPE=TEXT style="top:5px;left:120px;width:310px" CLASS=dogskininput >
		</DIV>
		 <DIV  style="position:absolute;left:5px;top:285px">
			<P id="descrip_5" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_13" , "sl"); ?> 5</P>
			<INPUT ID="DICT�descrip_5" TYPE=TEXT style="top:5px;left:140px;width:310px" CLASS=dogskininput >
		</DIV>
			<DIV  style="position:absolute;left:490px;top:285px">
			<P id="descrip_6" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_13" , "sl"); ?> 6</P>
			<INPUT ID="DICT�descrip_6" TYPE=TEXT style="top:5px;left:120px;width:310px" CLASS=dogskininput  >
		</DIV>
		<DIV  style="position:absolute;left:5px;top:305px">
			<P id="carphys_1" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_14" , "sl"); ?> 1</P>
			<INPUT ID="DICT�carphys_1" TYPE=TEXT style="top:5px;left:140px;width:310px" CLASS=dogskininput  >
		</DIV>
		<DIV  style="position:absolute;left:490px;top:305px">
			<P id="carphys_2" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_14" , "sl"); ?> 2</P>
			<INPUT ID="DICT�carphys_2" TYPE=TEXT style="top:5px;left:120px;width:310px" CLASS=dogskininput >
		</DIV>
		 <DIV  style="position:absolute;left:5px;top:325px">
			<P id="carphys_3" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_14" , "sl"); ?> 3</P>
			<INPUT ID="DICT�carphys_3" TYPE=TEXT style="top:5px;left:140px;width:310px" CLASS=dogskininput >
		</DIV>
		<DIV  style="position:absolute;left:490px;top:325px">
			<P id="carphys_4" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_14" , "sl"); ?> 4</P>
			<INPUT ID="DICT�carphys_4" TYPE=TEXT style="top:5px;left:120px;width:310px" CLASS=dogskininput >
		</DIV>
		 <DIV  style="position:absolute;left:5px;top:345px">
			<P id="carphys_5" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_14" , "sl"); ?> 5</P>
			<INPUT ID="DICT�carphys_5" TYPE=TEXT style="top:5px;left:140px;width:310px" CLASS=dogskininput >
		</DIV>
			<DIV  style="position:absolute;left:490px;top:345px">
			<P id="carphys_6" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_14" , "sl"); ?> 6</P>
			<INPUT ID="DICT�carphys_6" TYPE=TEXT style="top:5px;left:120px;width:310px" CLASS=dogskininput  >
		</DIV>
		<DIV  style="position:absolute;left:5px;top:365px">
			<P id="carphys_7" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_14" , "sl"); ?> 7</P>
			<INPUT ID="DICT�carphys_7" TYPE=TEXT style="top:5px;left:140px;width:310px" CLASS=dogskininput  >
		</DIV>
			<DIV  style="position:absolute;left:490px;top:365px">
			<P id="carphys_8" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_14" , "sl"); ?> 8</P>
			<INPUT ID="DICT�carphys_8" TYPE=TEXT style="top:5px;left:120px;width:310px" CLASS=dogskininput  >
		</DIV>
		<DIV  style="position:absolute;left:5px;top:385px">
			<P id="nomtech_1" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_15" , "sl"); ?> 1</P>
			<INPUT ID="DICT�nomtech_1" TYPE=TEXT style="top:5px;left:140px;width:310px" CLASS=dogskininput  >
		</DIV>
		<DIV  style="position:absolute;left:490px;top:385px">
			<P id="nomtech_2" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_15" , "sl"); ?> 2</P>
			<INPUT ID="DICT�nomtech_2" TYPE=TEXT style="top:5px;left:120px;width:310px" CLASS=dogskininput >
		</DIV>
		 <DIV  style="position:absolute;left:5px;top:405px">
			<P id="nomtech_3" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_15" , "sl"); ?> 3</P>
			<INPUT ID="DICT�nomtech_3" TYPE=TEXT style="top:5px;left:140px;width:310px" CLASS=dogskininput >
		</DIV>
		<DIV  style="position:absolute;left:490px;top:405px">
			<P id="nomtech_4" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_15" , "sl"); ?> 4</P>
			<INPUT ID="DICT�nomtech_4" TYPE=TEXT style="top:5px;left:120px;width:310px" CLASS=dogskininput >
		</DIV>
		<DIV  style="position:absolute;left:5px;top:425px">
			<P id="insclasse" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_16" , "sl"); ?></P>
			<INPUT ID="DICT�insclasse" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:110px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�insclasse�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:279px;top:5px;width:300px">
		</DIV>  

		<DIV  style="position:absolute;left:5px;top:445px"> 
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-danger_17" , "sl"); ?></P>
			<INPUT CLASS=dogskininput  ID="DICT�datefds" NATURE=DATE style="left:140px;top:5px;width:80px"  >
		</DIV>

		<DIV  style="position:absolute;left:5px;top:465px">
			<P id="cat" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_18" , "sl"); ?></P>
			<INPUT ID="DICT�cat" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:110px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�cat�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:279px;top:5px;width:300px">
		</DIV>         
		<DIV  style="position:absolute;left:5px;top:485px">
			<P id="grpemb" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_19" , "sl"); ?></P>
			<INPUT ID="DICT�grpemb" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:140px;width:110px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�grpemb�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:279px;top:5px;width:300px">
		</DIV> 
		<DIV  style="position:absolute;left:5px;top:505px">
			<P id="risque" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_20" , "sl"); ?> </P>
			<INPUT ID="DICT�risque" TYPE=TEXT NATURE=SEARCH CLASS=dogskininput  style="top:5px;left:140px;width:300px" >
		</DIV>
		<DIV  style="position:absolute;left:490px;top:505px">
			<P id="speciale" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_21" , "sl"); ?></P>
			<INPUT ID="DICT�speciale"  TYPE=TEXT NATURE=SEARCH CLASS=dogskininput style="top:5px;left:120px;width:80px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�speciale�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:229px;top:5px;width:200px">
		</DIV> 
		<DIV  style="position:absolute;left:5px;top:525px">
			<P id="qtelim" style="top:7px;width:200px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_22" , "sl"); ?> </P>
			<INPUT ID="DICT�qtelim" style="top:5px;left:140px;width:80px" TYPE=TEXT CLASS=dogskininput   >
		</DIV>
		<DIV  style="position:absolute;left:490px;top:525px">
			<P id="ident" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_23" , "sl"); ?> 4</P>
			<INPUT ID="DICT�ident" TYPE=TEXT style="top:5px;left:120px;width:310px" CLASS=dogskininput  >
		</DIV>
		<DIV  style="position:absolute;left:5px;top:545px">
			<P id="plaque" style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "mess-danger_24" , "sl"); ?></P>
			<INPUT ID="DICT�plaque" NATURE=SEARCH TYPE=TEXT CLASS=dogskininput style="top:5px;left:140px;width:110px" title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>" >
			<INPUT ID="DICTLINK�plaque�libel" TYPE=TEXT CLASS=dogskininputreadonly readonly style="left:279px;top:5px;width:300px">
		</DIV> 
	</div>
</div>

<!-- //ONGLET 9// -->
<DIV ID="9title" class=divArrondiDeg28 style="position:absolute;width:950px;height:225px;top:150px;left:5px" >
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:210px;top:5px;left:5px" >
		<TEXTAREA ID="DICT�articom" style="left:10px;top:7px;width:900px;height:180px" class=dogskininput >
		</TEXTAREA>
	</div>
</div>

<!-- //ONGLET 10// -->
<DIV ID="10title" class=divArrondiDeg28 style="position:absolute;width:950px;height:685px;top:150px;left:5px" >
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:110px;top:5px;left:5px" >
		<P class=dogskinbodyfont style="top:5px;font-weight:600;width:150px;left:30px"><?PHP print lect_bin::getLabel ( "mess-auxges_69" , "sl"); ?> -1-</P>
		
		<!-- Edition sur Bon de Commande (O/N) -->
		<DIV style="position:absolute;left:5px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_92" , "sl" ); ?> </P>
			<SELECT id="DICT�bc-comment_1" CLASS=dogskininputreadonly readonly style="top:5px;left:85px;width:48px" >
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Edition sur Bon de Pr�paration (O/N) -->
		<DIV style="position:absolute;left:155px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_93" , "sl" ); ?> </P>
			<SELECT id="DICT�bp-comment_1" CLASS=dogskininputreadonly readonly style="top:5px;left:85px;width:48px" >
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Edition sur Bon de Livraison (O/N) -->
		<DIV style="position:absolute;left:305px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_94" , "sl" ); ?> </P>
			<SELECT id="DICT�bl-comment_1" CLASS=dogskininputreadonly readonly style="top:5px;left:85px;width:48px" >
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Edition sur Facture (O/N) -->
		<DIV style="position:absolute;left:455px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_95" , "sl" ); ?> </P>
			<SELECT id="DICT�fac-comment_1" CLASS=dogskininputreadonly readonly style="top:5px;left:115px;width:48px">
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Affichage en Saisie de Bons (O/N) -->
		<DIV style="position:absolute;left:640px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_96" , "sl" ); ?> </P>
			<SELECT id="DICT�sai-comment_1" CLASS=dogskininputreadonly readonly style="top:5px;left:150px;width:48px">
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Texte Commentaire ##Pte ajout F10 sur zone -->
		<DIV style="position:absolute;left:5px;top:40px" >
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_69" , "sl" ); ?> </P>
			<!--## ORIG
			<TEXTAREA CLASS=dogskininputreadonly readonly ID="DICT�txt-comment_1" style="left:2px;top:20px;width:830px;height:50px;background-color:<?=$blanc?>" onblur="ValidKey()" focusblock=10 focusrange=<?=$cpt_focusrange++;?>> </TEXTAREA>
			-->
			<TEXTAREA CLASS=dogskininputreadonly readonly ID="DICT�txt-comment_1" NATURE=SEARCH style="left:2px;top:20px;width:800px;height:40px" > </TEXTAREA>
		</DIV>
	</div>
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:110px;top:120px;left:5px" >
		<P class=dogskinbodyfont style="top:5px;font-weight:600;width:150px;left:30px"><?PHP print lect_bin::getLabel ( "mess-auxges_69" , "sl"); ?> -2-</P>
		
		<!-- Edition sur Bon de Commande (O/N) -->
		<DIV style="position:absolute;left:5px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_92" , "sl" ); ?> </P>
			<SELECT id="DICT�bc-comment_2" CLASS=dogskininputreadonly readonly style="top:5px;left:85px;width:48px" >
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Edition sur Bon de Pr�paration (O/N) -->
		<DIV style="position:absolute;left:155px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_93" , "sl" ); ?> </P>
			<SELECT id="DICT�bp-comment_2" CLASS=dogskininputreadonly readonly style="top:5px;left:85px;width:48px" >
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Edition sur Bon de Livraison (O/N) -->
		<DIV style="position:absolute;left:305px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_94" , "sl" ); ?> </P>
			<SELECT id="DICT�bl-comment_2" CLASS=dogskininputreadonly readonly style="top:5px;left:85px;width:48px" >
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Edition sur Facture (O/N) -->
		<DIV style="position:absolute;left:455px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_95" , "sl" ); ?> </P>
			<SELECT id="DICT�fac-comment_2" CLASS=dogskininputreadonly readonly style="top:5px;left:115px;width:48px">
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Affichage en Saisie de Bons (O/N) -->
		<DIV style="position:absolute;left:640px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_96" , "sl" ); ?> </P>
			<SELECT id="DICT�sai-comment_2" CLASS=dogskininputreadonly readonly style="top:5px;left:150px;width:48px">
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Texte Commentaire ##Pte ajout F10 sur zone -->
		<DIV style="position:absolute;left:5px;top:40px" >
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_69" , "sl" ); ?> </P>
			<TEXTAREA CLASS=dogskininputreadonly readonly ID="DICT�txt-comment_2" NATURE=SEARCH style="left:2px;top:20px;width:800px;height:40px" > </TEXTAREA>
		</DIV>
	</div>
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:110px;top:235px;left:5px" >
		<P class=dogskinbodyfont style="top:5px;font-weight:600;width:150px;left:30px"><?PHP print lect_bin::getLabel ( "mess-auxges_69" , "sl"); ?> -3-</P>
		
		<!-- Edition sur Bon de Commande (O/N) -->
		<DIV style="position:absolute;left:5px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_92" , "sl" ); ?> </P>
			<SELECT id="DICT�bc-comment_3" CLASS=dogskininputreadonly readonly style="top:5px;left:85px;width:48px" >
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Edition sur Bon de Pr�paration (O/N) -->
		<DIV style="position:absolute;left:155px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_93" , "sl" ); ?> </P>
			<SELECT id="DICT�bp-comment_3" CLASS=dogskininputreadonly readonly style="top:5px;left:85px;width:48px" >
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Edition sur Bon de Livraison (O/N) -->
		<DIV style="position:absolute;left:305px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_94" , "sl" ); ?> </P>
			<SELECT id="DICT�bl-comment_3" CLASS=dogskininputreadonly readonly style="top:5px;left:85px;width:48px" >
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Edition sur Facture (O/N) -->
		<DIV style="position:absolute;left:455px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_95" , "sl" ); ?> </P>
			<SELECT id="DICT�fac-comment_3" CLASS=dogskininputreadonly readonly style="top:5px;left:115px;width:48px">
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Affichage en Saisie de Bons (O/N) -->
		<DIV style="position:absolute;left:640px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_96" , "sl" ); ?> </P>
			<SELECT id="DICT�sai-comment_3" CLASS=dogskininputreadonly readonly style="top:5px;left:150px;width:48px">
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Texte Commentaire ##Pte ajout F10 sur zone -->
		<DIV style="position:absolute;left:5px;top:40px" >
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_69" , "sl" ); ?> </P>
			<TEXTAREA CLASS=dogskininputreadonly readonly ID="DICT�txt-comment_3" NATURE=SEARCH style="left:2px;top:20px;width:800px;height:40px" > </TEXTAREA>
		</DIV>
	</div>
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:110px;top:350px;left:5px" >
		<P class=dogskinbodyfont style="top:5px;font-weight:600;width:150px;left:30px"><?PHP print lect_bin::getLabel ( "mess-auxges_69" , "sl"); ?> -4-</P>
		
		<!-- Edition sur Bon de Commande (O/N) -->
		<DIV style="position:absolute;left:5px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_92" , "sl" ); ?> </P>
			<SELECT id="DICT�bc-comment_4" CLASS=dogskininputreadonly readonly style="top:5px;left:85px;width:48px" >
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Edition sur Bon de Pr�paration (O/N) -->
		<DIV style="position:absolute;left:155px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_93" , "sl" ); ?> </P>
			<SELECT id="DICT�bp-comment_4" CLASS=dogskininputreadonly readonly style="top:5px;left:85px;width:48px" >
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Edition sur Bon de Livraison (O/N) -->
		<DIV style="position:absolute;left:305px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_94" , "sl" ); ?> </P>
			<SELECT id="DICT�bl-comment_4" CLASS=dogskininputreadonly readonly style="top:5px;left:85px;width:48px" >
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Edition sur Facture (O/N) -->
		<DIV style="position:absolute;left:455px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_95" , "sl" ); ?> </P>
			<SELECT id="DICT�fac-comment_4" CLASS=dogskininputreadonly readonly style="top:5px;left:115px;width:48px">
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Affichage en Saisie de Bons (O/N) -->
		<DIV style="position:absolute;left:640px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_96" , "sl" ); ?> </P>
			<SELECT id="DICT�sai-comment_4" CLASS=dogskininputreadonly readonly style="top:5px;left:150px;width:48px">
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Texte Commentaire ##Pte ajout F10 sur zone -->
		<DIV style="position:absolute;left:5px;top:40px" >
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_69" , "sl" ); ?> </P>
			<TEXTAREA CLASS=dogskininputreadonly readonly ID="DICT�txt-comment_4" NATURE=SEARCH style="left:2px;top:20px;width:800px;height:40px" > </TEXTAREA>
		</DIV>
	</div>
	<DIV class=divArrondi29 style="position:absolute;width:940px;height:110px;top:465px;left:5px" >
		<P class=dogskinbodyfont style="top:5px;font-weight:600;width:150px;left:30px"><?PHP print lect_bin::getLabel ( "mess-auxges_69" , "sl"); ?> -5-</P>
		
		<!-- Edition sur Bon de Commande (O/N) -->
		<DIV style="position:absolute;left:5px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_92" , "sl" ); ?> </P>
			<SELECT id="DICT�bc-comment_5" CLASS=dogskininputreadonly readonly style="top:5px;left:85px;width:48px" >
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Edition sur Bon de Pr�paration (O/N) -->
		<DIV style="position:absolute;left:155px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_93" , "sl" ); ?> </P>
			<SELECT id="DICT�bp-comment_5" CLASS=dogskininputreadonly readonly style="top:5px;left:85px;width:48px" >
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Edition sur Bon de Livraison (O/N) -->
		<DIV style="position:absolute;left:305px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_94" , "sl" ); ?> </P>
			<SELECT id="DICT�bl-comment_5" CLASS=dogskininputreadonly readonly style="top:5px;left:85px;width:48px" >
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Edition sur Facture (O/N) -->
		<DIV style="position:absolute;left:455px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_95" , "sl" ); ?> </P>
			<SELECT id="DICT�fac-comment_5" CLASS=dogskininputreadonly readonly style="top:5px;left:115px;width:48px">
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Affichage en Saisie de Bons (O/N) -->
		<DIV style="position:absolute;left:640px;top:20px">
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_96" , "sl" ); ?> </P>
			<SELECT id="DICT�sai-comment_5" CLASS=dogskininputreadonly readonly style="top:5px;left:150px;width:48px">
			<?PHP deal_commun::charge_select( "combo-ouinon3" , "N" ); ?>
			</SELECT>
		</DIV>

		<!-- Texte Commentaire ##Pte ajout F10 sur zone -->
		<DIV style="position:absolute;left:5px;top:40px" >
			<P class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "mess-auxges_69" , "sl" ); ?> </P>
			<TEXTAREA CLASS=dogskininputreadonly readonly ID="DICT�txt-comment_5" NATURE=SEARCH style="left:2px;top:20px;width:800px;height:40px" > </TEXTAREA>
		</DIV>
	</div>
</div>

<!-- //ONGLET 11// -->
<DIV ID="11title" class=divArrondiDeg28 style="position:absolute;width:950px;height:655px;top:150px;left:5px" >
	<?
	   //V�rifier si on trouve le fichier spe dans le bon path  
		if ( file_exists ( search::searchExe("ns_gesartnat_spe_w3c.inc",0) ) )
			include( search::searchExe("ns_gesartnat_spe_w3c.inc",0) ) ;
	?>
</div>

<!--//Pav� du pied de bon -->	
<DIV ID="DUPLICATION" class="divArrondiDeg16" style="position:absolute;left:285px;height:70px;top:263px;width:350px;visibility:hidden" onmousedown="beginDrag(this,event);" onmousemove="drag(event);" onmouseup="endDrag();">
	<P class=dogskinbodyfont style="left:20px;top:7px;width:210px;font-weight:600;font-size:8pt">Nouveau code article </P>	
	
	<!-- CODE ARTICLE -->
	<DIV  style="position:absolute;left:20px;top:23px">
		<P class=dogskinbodyfont style="top:7px">Nouveau code article </P>
		<INPUT ID="DICT�artdup" TYPE=TEXT CLASS=dogskininput MAXLENGTH=6  style="top:5px;left:135px;width:80px">
	</DIV>
	
	<!-- SOCIETES RECEPTRICES -->
	<DIV id="divSocdup" style="position:absolute;left:20px;top:43px">
		<P class=dogskinbodyfont style="top:7px">Soci�t� r�ceptrice</P>
		<INPUT ID="DICT�socdup" NATURE=SEARCH TYPE=TEXT CLASS=dogskininput  style="top:5px;left:135px;width:80px" >
	</DIV>
	
	<IMG src="<?=$dealgateRelPath?>/site/img/transparent.png" class="navImg tailleImg30 validImg30" 		style="position:absolute;bottom:5px;right:5px;cursor:pointer" onclick="gestDuplicationArticle()"  title="Accepter">
	<IMG id="bt272" src="<?=$dealgateRelPath?>/site/img/transparent.png" class="navImg tailleImg30 annulImg30" 		style="position:absolute;bottom:5px;right:40px;cursor:pointer" onclick="annuldupart()"  title="Annuler">
</DIV>

<!--//Div affiche si _ficheSoc = YES -->	
<DIV ID="DIV_FICHESOC" class="divArrondiDeg16" style="visibility:hidden; position:absolute;left:50px;height:340px;top:170px;width:850px;">
	<img id="btn_ficheSoc_exit" src="<?=$dealgateRelPath;?>/site/img/transparent.png" class="navImg tailleImg20 annulImg20" title="Fermer la fen�tre" style="position:absolute;top:10px;right:10px;" onclick="ctrlFicheSoc('QUITTER')" />
	<img id="btn_ficheSoc_valid" src="<?=$dealgateRelPath;?>/site/img/transparent.png" class="navImg tailleImg30 validImg30" title="Valider et fermer la fen�tre" style="position:absolute;bottom:10px;right:10px;" onclick="ctrlFicheSoc('VALIDER')" />
	<img id="btn_ficheSoc_modif" src="<?=$dealgateRelPath;?>/site/img/transparent.png" class="navImg tailleImg30 updImg30" title="Modifier la saisie" style="position:absolute;bottom:10px; right:45px;" onclick="ctrlFicheSoc('MODIFIER')" />
	<P class=dogskinbodyfont style="left:20px;top:7px;width:210px;font-weight:600;font-size:8pt">Donn�es soci�t�</P>
	
	<!-- Blocage de l'article -->
	<DIV  style="position:absolute;left:5px;top:25px">
		<P style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "cer-blocage" , "sl"); ?> </P>
		<SELECT id="DICT�codblocFICHESOC"  style="top:5px;left:180px" CLASS=dogskininputreadonly readonly disabled>
			<?PHP deal_commun::charge_select("cer-blocage",""); ?>
		</SELECT>
	</DIV>
	
	<!-- R�f�renc� -->
	<DIV  style="position:absolute;left:5px;top:45px">
		<P style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "codebloc_1" , "sl"); ?> </P>
		<SELECT ID="DICT�codebloc_1FICHESOC" CLASS=dogskininputreadonly readonly disabled style="top:5px;left:180px;width:270px" >	
			<option value="0" selected>Oui</option>
			<option value="1" ><?PHP print lect_bin::getLabel ( "codebloc_5" , "sl"); ?></option>        	    
		</SELECT>
	</DIV>
	
	<!-- Provenance -->
	<DIV  style="position:absolute;left:5px;top:65px">
		<P style="top:7px" class=dogskinbodyfont><?PHP print lect_bin::getLabel ( "libart_1" , "sl"); ?> </P>
		<INPUT NATURE=SEARCH ID="DICT�provenance" style="top:5px;left:180px;width:40px"   TYPE=TEXT  CLASS=dogskininputreadonly readonly >
		<INPUT  			 ID="DICT�provenanceLib" style="top:5px;left:225px;width:220px"   TYPE=TEXT  CLASS=dogskininputreadonly readonly >
	</DIV>
	
	<!-- Commentaire societe -->
	<TEXTAREA ID="DICT�comSociete" style="left:10px; top:100px; width:825px; height:180px" class=dogskininputreadonly readonly >
	</TEXTAREA>
	
</DIV>		

<!-- GRID SELECTION MULTIPLE -->
<DIV id="divMultiSel" class="divArrondi" style="visibility:hidden;position:absolute;height:420px;top:40px;left:200px;background-color:<?=$bleu?>; visibility:hidden">	
	<IMG SRC="<?=$dealgateRelPath?>/site/img/di-valider.png" 	href="#" title="Valider (F2)" style="position:absolute;bottom:10px;left:290px;width:20px;height:20px;cursor:pointer" onMouseOut="this.src='<?=$dealgateRelPath?>/site/img/di-valider.png'" onMouseOver="this.src='<?=$dealgateRelPath?>/site/img/di-valider-over.png'" onClick="validMultiSel()"></IMG>
</div>

</BODY>
</HTML>
<script LANGUAGE=JAVASCRIPT>

/*************** G R I D    S E L E C T I O N    M U L T I P L E ***************/
var gridMultiSel 	= new AW.Grid.Extended;
var dataGridSel = [];
gridMultiSel.initGrid = function()
{
	valHeaderCelM = "Sel�Code�Libell�";
	valWidthCelM  = "20�50�150";

	var myHauteurM = 350;
	var myLargeurM = 300;

	createTableau("divMultiSel","myDivMultiSel", myHauteurM, myLargeurM, "gridMultiSel");
	//on charge le tableau vide avec les bons libell�s et les largeurs
	varHeaderTM = valHeaderCelM.split("�");
	varColWidthM = valWidthCelM.split("�");
	// gridTM.setStyle("left", "110px")

	gridMultiSel.setId("myDivMultiSel");

	initTableau(gridMultiSel, varHeaderTM, varColWidthM, myHauteurM, myLargeurM);

	gridMultiSel.setColumnCount(varHeaderTM.length);
	gridMultiSel.setColumnIndices([0,1,2]);

	
	
	this.onRowDoubleClicked = function(event,r)
	{
		if(gridMultiSel.getCellText  ( 0 , r) == imgDecoche)
		{
			gridMultiSel.setCellText  ( imgCoche , 0 , r)
		}
		else
		{
			gridMultiSel.setCellText  ( imgDecoche , 0 , r)	
		}
	}
}

function loadMultiSelSocietes()
{
	gridMultiSel.setRowCount(0);
	dataGridSel = [];
	
	//Sauvegarde de l'ancienne saisie dans un vecteur.
	var saveListeSoc = [];
	if($('DICT�socdup').value.trim() != "")
		saveListeSoc = $('DICT�socdup').value.split(',');
	
	//On r�cup�re tous les libell� des soci�t�s.
	var dogFct = "get-autsoc�d-codsoc";
	var retourExchange;
	for(var i=0; i<listeSocsDup.size(); i++)
	{
		requete = "OPERAT�<?=$opeBase?>�CODSOC�"+listeSocsDup[i];
		retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , requete , dogFct , "" , "" );
		
		if(retourExchange != 'OK')
			continue;
		
		dataGridSel.push([
			imgDecoche,
			getValeurRsp('CODSOC'),
			getValeurRsp('LIBSOC')
		]);
	}
	
	//On reporte l'ancienne saisie dans le grid.
	if(saveListeSoc.size() > 0)
	{
		for(var i=0; i<dataGridSel.size(); i++)
		{
			for(j=0; j<saveListeSoc.size(); j++)
			{
				if(dataGridSel[i][1] == saveListeSoc[j])
					dataGridSel[i][0] = imgCoche;
			}
		}		
	}
	
	//Chargement du grid
	gridMultiSel.setCellData(dataGridSel);
	gridMultiSel.setRowCount(dataGridSel.length);
	gridMultiSel.refresh();
	gridMultiSel.focus();
}

/**
	R�cup�re les soci�t�s coch�es, renvoie la liste dans l'input DICT�socdup
**/
function validMultiSel()
{
	var listeSocsChoisi = [];
	
	for(var i=0; i<gridMultiSel.getRowCount(); i++)
	{
		//Si case coch�e.
		if(gridMultiSel.getCellText(0,i) == imgCoche)
		{
			listeSocsChoisi.push(listeSocsDup[i]);
		}
	}
	
	$('DICT�socdup').value = listeSocsChoisi.join();
	$('divMultiSel').style.visibility = 'hidden';
}
</script>








