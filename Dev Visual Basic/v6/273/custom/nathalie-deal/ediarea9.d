/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! Deal/nathalie/tty/ediarea9.d                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 61120 04-10-16!Evo!pha            !report modifs 273                                                     !
!V61 50679 19-11-14!New!jb             !Int�gration des modifications CCO                                     !
!V61 50300 04-11-14!Evo!jb             !Modifications suite demandes CCO                                      !
!V61 49488 01-10-14!Evo!jb             !r�cup�rer combien d?UC dans le carton et combien dans la palette      !
!V61 49380 25-09-14!Evo!jb             !suppression test theme "pla"                                          !
!V61 49367 25-09-14!New!jb             !Integration des articles phytosanitaires                              !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*-------------------------------------------------------*/
/*            R.A.Z des variables                        */
/*-------------------------------------------------------*/
ASSIGN PENTRY_8     = ""
       PENTRY_9     = "" 
       PENTRY_12    = "" 
       PENTRY_26    = ""
       PENTRY_45    = ""
       PENTRY_76    = "" 
       PENTRY_102   = "" 
       PENTRY_105   = ""
       PENTRY_120   = "" 
       PENTRY_211   = "" 
       PENTRY_220   = ""
       PENTRY_223   = "" 
       PENTRY_232   = "" 
       PENTRY_235   = "" 
       PENTRY_241   = "" 
       PENTRY_244   = "" 
       PENTRY_250   = "" 
       PENTRY_253   = "" 
       PENTRY_259   = ""  
       PENTRY_295   = ""
       PENTRY_343   = ""
       PENTRY_465   = ""
       PENTRY_474   = ""
       TENTRY_28    = ""
       QENTRY_8     = ""
       QENTRY_58    = ""
       QENTRY_61    = ""
       QENTRY_79    = ""
       QENTRY_82    = ""
       QENTRY_97    = ""
       QENTRY_100   = ""
       QENTRY_103   = ""
       QENTRY_118   = ""
       QENTRY_121   = ""
       QENTRY_124   = ""
       QENTRY_139   = ""
       XENTRY_8     = ""
       XENTRY_11    = ""
       XENTRY_15    = ""
       XENTRY_18    = ""
       XENTRY_22    = ""
       XENTRY_29    = ""
       XENTRY_32    = ""
       XENTRY_36    = ""
       XENTRY_39    = ""
       XENTRY_45    = ""
       XENTRY_48    = ""
       XENTRY_51    = ""
       XENTRY_54    = ""
       XENTRY_57    = ""
       XENTRY_60    = ""
       XENTRY_62    = ""
       XENTRY_64    = ""
       XENTRY_66    = ""
       XENTRY_68    = ""
       XENTRY_70    = ""
       XENTRY_72    = ""
       XENTRY_75    = ""
       XENTRY_79    = ""
       XENTRY_114   = ""
       XENTRY_149   = ""
       XENTRY_184   = ""
       XENTRY_219   = ""
       XENTRY_254   = ""
       XENTRY_289   = ""
       XENTRY_359   = ""
       XENTRY_429   = ""
       XENTRY_499   = ""
       XENTRY_569   = ""
       XENTRY_639   = ""
       XENTRY_709   = ""
       XENTRY_779   = ""
       XENTRY_849   = ""       
       XENTRY_857   = ""
       XENTRY_887   = ""
       XENTRY_889   = ""
       XENTRY_939   = ""
       XENTRY_989   = ""
       XENTRY_993   = ""
       cumul-danger = "" 
       .

/*---------------------------------------------------------------------*/
/*        RECUPERATION DES DONNEES - ALIMENTATION DES VARIABLES        */
/*---------------------------------------------------------------------*/
PENTRY_8   = substring ( ttFicEdi.ligne,8,1    ) no-error . /*** Type de ligne. 1:cr�ation 2:effac� 5:Annule et remplace          */
PENTRY_9   = substring ( ttFicEdi.ligne,9,3    ) no-error . /*** Type de produit. CU:unit� consommateur DU:unit� logistique       */
PENTRY_12  = trim(substring ( ttFicEdi.ligne,12,14  )) no-error . /* Code EAN                                                           */
PENTRY_26  = substring ( ttFicEdi.ligne,26,2   ) no-error . /* Filler1                                                            */
PENTRY_45  = trim(substring ( ttFicEdi.ligne,45,14  )) no-error . /* Code EAN remplassement                                             */
PENTRY_76  = substring ( ttFicEdi.ligne,76,17  ) no-error . /* Code ean ou siret du fournisseur                                   */
PENTRY_93  = substring ( ttFicEdi.ligne,93,3   ) no-error . /* Type de code : 9=EAN 10A=Siret                                     */  
PENTRY_102 = substring ( ttFicEdi.ligne,102,3  ) no-error . /* Code TVA                                                           */
PENTRY_105 = substring ( ttFicEdi.ligne,105,9  ) no-error . /* Conditionnement                                                    */
PENTRY_114 = substring ( ttFicEdi.ligne,114,3  ) no-error . /* Unit� de conditionnement (litre ou kilo)                           */
PENTRY_120 = substring ( ttFicEdi.ligne,120,3  ) no-error . /* Unit� de commande (PCE)                                            */
PENTRY_211 = substring ( ttFicEdi.ligne,211,9  ) no-error . /*** Poids net                                                        */
PENTRY_220 = substring ( ttFicEdi.ligne,220,3  ) no-error . /*** Unit� Poids net                                                  */
PENTRY_223 = substring ( ttFicEdi.ligne,223,9  ) no-error . /* Poids brut                                                         */
PENTRY_232 = substring ( ttFicEdi.ligne,232,3  ) no-error . /*** Unit� Poids brut                                                 */
PENTRY_235 = substring ( ttFicEdi.ligne,235,6  ) no-error . /*** Hauteur                                                          */
PENTRY_241 = substring ( ttFicEdi.ligne,241,3  ) no-error . /*** Unit� hauteur                                                    */
PENTRY_244 = substring ( ttFicEdi.ligne,244,6  ) no-error . /*** largeur                                                          */
PENTRY_250 = substring ( ttFicEdi.ligne,250,3  ) no-error . /*** Unit� largeur                                                    */
PENTRY_253 = substring ( ttFicEdi.ligne,253,6  ) no-error . /*** Profondeur                                                       */
PENTRY_259 = substring ( ttFicEdi.ligne,259,3  ) no-error . /*** Unit� profondeur                                                 */
PENTRY_295 = substring ( ttFicEdi.ligne,295,7  ) no-error . /*** AMM                                                              */
PENTRY_343 = substring ( ttFicEdi.ligne,343,3  ) no-error . /*** Type d'unit� logistique                                          */
PENTRY_440 = substring ( ttFicEdi.ligne,440,17 ) no-error . /*** PH                                                               */
PENTRY_465 = substring ( ttFicEdi.ligne,465,9  ) no-error . /*** PH                                                               */
PENTRY_474 = substring ( ttFicEdi.ligne,474,17 ) no-error . /*** Code ean 13 de l'ul dans le carton et dans la palette            */

if available bf1-ttficedi then do :
   TENTRY_28   = substring ( bf1-ttficedi.ligne,28,35    ) no-error . /* Libell� standard                                                        */
end . /* if available bf1-ttficedi then do : */   

if available bf2-ttficedi then do :   
   IF substring ( bf2-ttFicEdi.ligne,8,8    ) = "00000000"   /* PRODAT V3 */
       THEN QENTRY_8    = substring ( bf2-ttFicEdi.ligne,142,8    ) no-error . /* Date de mise � jour des taxes */
       ELSE QENTRY_8    = substring ( bf2-ttFicEdi.ligne,8,8    ) no-error .   /* Date de mise � jour des taxes */

   QENTRY_58   = substring ( bf2-ttFicEdi.ligne,58,3   ) no-error . /* Code agence 3                 */
   QENTRY_61   = substring ( bf2-ttFicEdi.ligne,61,15  ) no-error . /* Redevance agence 3            */
   QENTRY_79   = substring ( bf2-ttFicEdi.ligne,79,3   ) no-error . /* Code agence 4                 */
   QENTRY_82   = substring ( bf2-ttFicEdi.ligne,82,15  ) no-error . /* Redevance agence 4            */
   QENTRY_97   = substring ( bf2-ttFicEdi.ligne,97,3   ) no-error . /* Unit� redevance agence 4      */
   QENTRY_100  = substring ( bf2-ttFicEdi.ligne,100,3  ) no-error . /* Code agence 5                 */
   QENTRY_103  = substring ( bf2-ttFicEdi.ligne,103,15 ) no-error . /* Redevance agence 5            */
   QENTRY_118  = substring ( bf2-ttFicEdi.ligne,118,3  ) no-error . /* Unit� redevance agence 5      */
   QENTRY_121  = substring ( bf2-ttFicEdi.ligne,121,3  ) no-error . /* Code agence 6                 */
   QENTRY_124  = substring ( bf2-ttFicEdi.ligne,124,15 ) no-error . /* Redevance agence 6            */
   QENTRY_139  = substring ( bf2-ttFicEdi.ligne,139,3  ) no-error . /* Unit� redevance agence 6      */
end . /* if available bf2-ttficedi then do : */

if available bf3-ttficedi then do :   
   XENTRY_8    = substring ( bf3-ttFicEdi.ligne,8,3    ) no-error . /* R�glementation ONU  : classe              */
   XENTRY_11   = substring ( bf3-ttFicEdi.ligne,11,4   ) no-error . /* R�glementation ONU  : classification      */
   XENTRY_15   = substring ( bf3-ttFicEdi.ligne,15,3   ) no-error . /* R�glementation ONU  : Groupe emballage    */
   XENTRY_18   = substring ( bf3-ttFicEdi.ligne,18,4   ) no-error . /* R�glementation ONU  : N� ONU              */
   XENTRY_22   = substring ( bf3-ttFicEdi.ligne,22,7   ) no-error . /* Filler                                    */
   XENTRY_29   = substring ( bf3-ttFicEdi.ligne,29,3   ) no-error . /* point Eclair                              */
   XENTRY_32   = substring ( bf3-ttFicEdi.ligne,32,4   ) no-error . /* Sensibilit� au froid                      */
   XENTRY_36   = substring ( bf3-ttFicEdi.ligne,36,3   ) no-error . /* Sensibilit� au chaud                      */
   XENTRY_39   = substring ( bf3-ttFicEdi.ligne,39,4   ) no-error . /* N. consigne de securite                   */
   XENTRY_45   = substring ( bf3-ttFicEdi.ligne,45,3   ) no-error . /* R�glementation ONU  : �tiquette danger 1  */
   XENTRY_48   = substring ( bf3-ttFicEdi.ligne,48,3   ) no-error . /* R�glementation ONU  : �tiquette danger 2  */
   XENTRY_51   = substring ( bf3-ttFicEdi.ligne,51,3   ) no-error . /* R�glementation ONU  : �tiquette danger 3  */
   XENTRY_54   = substring ( bf3-ttFicEdi.ligne,54,3   ) no-error . /* R�glementation ONU  : �tiquette danger 4  */
   XENTRY_57   = substring ( bf3-ttFicEdi.ligne,57,3   ) no-error . /* R�glementation ONU  : �tiquette danger 5  */
   XENTRY_60   = substring ( bf3-ttFicEdi.ligne,60,2   ) no-error . /* R�glementation ONU  : cat�gorie transport */
   XENTRY_62   = substring ( bf3-ttFicEdi.ligne,62,2   ) no-error . /* Toxicit� du produit : Symbole de danger 1 */
   XENTRY_64   = substring ( bf3-ttFicEdi.ligne,64,2   ) no-error . /* Toxicit� du produit : Symbole de danger 2 */
   XENTRY_66   = substring ( bf3-ttFicEdi.ligne,66,2   ) no-error . /* Toxicit� du produit : Symbole de danger 3 */
   XENTRY_68   = substring ( bf3-ttFicEdi.ligne,68,2   ) no-error . /* Toxicit� du produit : Symbole de danger 4 */
   XENTRY_70   = substring ( bf3-ttFicEdi.ligne,70,2   ) no-error . /* Toxicit� du produit : Symbole de danger 5 */
   XENTRY_72   = substring ( bf3-ttFicEdi.ligne,72,3   ) no-error . /* Etat physique : S=solide L=Liquide        */
   XENTRY_75   = substring ( bf3-ttFicEdi.ligne,75,4   ) no-error . /* R�glementation ONU  : Code emballage      */
   XENTRY_79   = substring ( bf3-ttFicEdi.ligne,79,35  ) no-error . /* Description 1                             */
   XENTRY_114  = substring ( bf3-ttFicEdi.ligne,114,35 ) no-error . /* Description 1                             */
   XENTRY_149  = substring ( bf3-ttFicEdi.ligne,149,35 ) no-error . /* Description 2                             */
   XENTRY_184  = substring ( bf3-ttFicEdi.ligne,184,35 ) no-error . /* Description 2                             */
   XENTRY_219  = substring ( bf3-ttFicEdi.ligne,219,35 ) no-error . /* Description 3                             */
   XENTRY_254  = substring ( bf3-ttFicEdi.ligne,254,35 ) no-error . /* Description 3                             */
   XENTRY_289  = substring ( bf3-ttFicEdi.ligne,289,70 ) no-error . /* Caract�ristique physique 1                */
   XENTRY_359  = substring ( bf3-ttFicEdi.ligne,359,70 ) no-error . /* Caract�ristique physique 2                */
   XENTRY_429  = substring ( bf3-ttFicEdi.ligne,429,70 ) no-error . /* Caract�ristique physique 3                */
   XENTRY_499  = substring ( bf3-ttFicEdi.ligne,499,70 ) no-error . /* Caract�ristique physique 4                */
   XENTRY_569  = substring ( bf3-ttFicEdi.ligne,569,70 ) no-error . /* Nom technique 1                           */
   XENTRY_639  = substring ( bf3-ttFicEdi.ligne,639,70 ) no-error . /* Nom technique 2                           */
   XENTRY_709  = substring ( bf3-ttFicEdi.ligne,709,70 ) no-error . /* Nom technique 3                           */
   XENTRY_779  = substring ( bf3-ttFicEdi.ligne,779,70 ) no-error . /* Nom technique 4                           */
   XENTRY_849  = substring ( bf3-ttFicEdi.ligne,849,8  ) no-error . /* Date de la FDS                            */ 
   XENTRY_857  = substring ( bf3-ttFicEdi.ligne,857,30 ) no-error . /* Installation class�e                      */
   XENTRY_887  = substring ( bf3-ttFicEdi.ligne,887,2  ) no-error . /* Categorie de danger                       */
   XENTRY_889  = substring ( bf3-ttFicEdi.ligne,889,50 ) no-error . /* Phrase de risque                          */
   XENTRY_939  = substring ( bf3-ttFicEdi.ligne,939,50 ) no-error . /* Phrase speciale                           */
   XENTRY_989  = substring ( bf3-ttFicEdi.ligne,989,4  ) no-error . /* Quantit� limit�e                          */
   XENTRY_993  = substring ( bf3-ttFicEdi.ligne,993,4  ) no-error . /* N. identification danger                  */
end . /* if available bf3-ttficedi then do : */



if available bf6-ttficedi then do :   


end . /* if available bf6-ttficedi then do : */



if available bf7-ttficedi then do :   


end . /* if available bf7-ttficedi then do : */




PENTRY_223DEC  = dec(PENTRY_223 ) no-error .
if error-status:error then PENTRY_223DEC = 0.

/* Pour tester l'existence des donn�es "danger" */
cumul-danger = XENTRY_8   + 
               XENTRY_11  + 
               XENTRY_15  + 
               XENTRY_18  + 
               XENTRY_29  + 
               XENTRY_32  + 
               XENTRY_36  + 
               XENTRY_39  + 
               XENTRY_45  + 
               XENTRY_48  + 
               XENTRY_51  + 
               XENTRY_54  + 
               XENTRY_57  + 
               XENTRY_60  + 
               XENTRY_62  + 
               XENTRY_64  + 
               XENTRY_66  + 
               XENTRY_68  + 
               XENTRY_70  + 
               XENTRY_72  + 
               XENTRY_75  + 
               XENTRY_79  + 
               XENTRY_114 + 
               XENTRY_149 + 
               XENTRY_184 + 
               XENTRY_219 + 
               XENTRY_254 + 
               XENTRY_289 + 
               XENTRY_359 + 
               XENTRY_429 + 
               XENTRY_499 + 
               XENTRY_569 + 
               XENTRY_639 + 
               XENTRY_709 + 
               XENTRY_779 + 
               XENTRY_849 + 
               XENTRY_857 + 
               XENTRY_887 +
               XENTRY_889 +
               XENTRY_939 +
               XENTRY_989 +
               XENTRY_993 .









