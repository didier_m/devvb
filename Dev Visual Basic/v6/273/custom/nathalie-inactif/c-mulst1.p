/*=========================================================================*/
/*                         C - E N C S T 1 . P                             */
/*                         +++++++++++++++++++                             */
/*  Recalcul : - Stock Temps Reel en Fonction du Dernier Arrete de Stock   */
/*             - En Cours de Commandes Clients                             */
/*             - En Cours de Commandes Fournisseurs                        */
/*=========================================================================*/
/*-------------------------------------------------- */
/*  Acces parametre lecture societe article ==> OK   */
/*-------------------------------------------------- */
{ connect.i }
{ aide.i new }

{ bat-ini.i  new }

def input parameter fic  as char format "x(12)" .

def buffer P-LIGNES  for  LIGNES .
def buffer ARTICL    for  ARTAPR .
def buffer B-STOCKS  for  STOCKS .

def var nom-applic       as char                 no-undo .

def var ya-ecart         as log                  no-undo .
def var recon-enco       as char                 no-undo .
def var date-arrete      like stoarr.date-arrete no-undo .
def var exe-arrete       as char  format "xxxx"  no-undo .
def var mois-arrete      as int                  no-undo .

def var x-qte            like stoarr.stock-arr   no-undo .
def var qte-arr          like stoarr.stock-arr   no-undo .
def var qte-enc          like stoarr.stock-arr   no-undo .
def var qte-enf          like stoarr.stock-arr   no-undo .
def var qte-sto          like stoarr.stock-arr   no-undo .
def var sqte-enc         like stoarr.stock-arr   no-undo .
def var sqte-enf         like stoarr.stock-arr   no-undo .
def var sqte-enct        like stoarr.stock-arr   no-undo .
def var sqte-enft        like stoarr.stock-arr   no-undo .
def var sqte-sto         like stoarr.stock-arr   no-undo .

def var ii               as int                  no-undo .

def var lib-oui          as char format "x"      no-undo .
def var lib-non          as char format "x"      no-undo .

def var nb-lus           as int                  no-undo .

def var even-stoarr      as char  format "x(6)"  no-undo .
def var magasin          like stoarr.magasin     no-undo .
def var ll-mag           as char                 no-undo .
def var art-min          as char  format "x(10)" no-undo .
def var art-max          as char  format "x(10)" no-undo .

def var lib-mess         as char  format "x(47)" no-undo .
def var lib-mess1        as char  format "x(18)" no-undo .
def var majour           as char  format "x"     no-undo .
def var nom-spool        as char format "x(15)"  no-undo .
def stream kan-edi .

def var libelle          as char   extent 49     no-undo .

def var mousto           as dec    extent 99     no-undo .
def var mouenc           as dec    extent 99     no-undo .
def var mouenf           as dec    extent 99     no-undo .
def var mouenc-tr        as dec    extent 99     no-undo .
def var mouenf-tr        as dec    extent 99     no-undo .

def var qte-part         as dec                  no-undo .
def var x-chrono-par     as int                  no-undo .

def var l-motcle         as char                 no-undo .

def var articl-soc       as char                 no-undo.

/* Prise des selections */

run bat-lec.p( fic ) .

{ bat-cha.i  "date-arrete"  "date-arrete"  "d" }
{ bat-cha.i  "nom-spool"    "nom-spool"    "a" }
{ bat-cha.i  "majour"       "majour"       "a" }
{ bat-cha.i  "art-min"      "art-min"      "a" }
{ bat-cha.i  "art-max"      "art-max"      "a" }

regs-app = "NATHALIE" .
{ regs-cha.i }

{ regs-inc.i "articl" }
articl-soc = regs-soc .

assign even-stoarr = "STOARR"
       l-motcle    = "VTE,ACH"
       .

aide-fichier = "prog-gen.aid" . aide-par = "<c-encst0>" . run aide-lec.p .

do ii = 1 to 49 :
  aide-m = "libel" + string( ii , "999" ) .
  { aide-lib.i }
  libelle[ ii ] = aide-lib .
end .

assign lib-oui = libelle[ 5 ]
       lib-non = libelle[ 6 ] .

exe-arrete  = string( year(  date-arrete ) , "9999" ) .
mois-arrete =         month( date-arrete ) .

message  codsoc-soc  date-arrete  majour  nom-spool  "   Debut le :"  today
         " a "  string( time , "hh:mm:ss" ) . pause 3 .

assign  typtab = "NAT"  prefix = "NOM-APPLIC"  codtab = "" .
run ch-param.p( output nom-applic ) .

assign  typtab = "STO"  prefix = "RECON-ENCO"  codtab = "" .
run ch-param.p( output recon-enco ) .

if nom-applic = "CBA" then l-motcle = l-motcle + ",AJS,AJE" .

OUTPUT STREAM KAN-EDI TO VALUE( NOM-SPOOL ) .

{ regs-inc.i  articl }
FOR EACH ARTICL where articl.codsoc  = articl-soc
                and   articl.articl  >= art-min
                and   articl.articl  <= art-max
                and   articl.tenusto = "1"
                and   articl.provenance = "U"
                use-index artapr-1  no-lock :

    if nb-lus modulo 10 = 0 then
    do :
        hide message . pause 0 .
        message string( nb-lus , ">>>>9" )  "  " articl.articl .
    end .
    nb-lus = nb-lus + 1 .

    ll-mag = "" .

    FOR EACH LIGNES where lignes.codsoc  = codsoc-soc
                    and   lignes.articl  = articl.articl
                    and   lookup ( lignes.motcle, l-motcle ) <> 0
                    and   lignes.tenusto = "1"
                    and   ( lignes.nivcom < "7"  or  lignes.nivcom = "8" )
                    use-index artdat no-lock :

        assign qte-enc = 0
               qte-enf = 0
               qte-sto = 0 .

        /*             Si Livre                  */

        if ( ( lignes.nivcom >= "3"  and  lignes.nivcom <= "6" ) or
           lignes.nivcom = "8" )  and  lignes.datdep > date-arrete  then
        do :

            qte-sto = lignes.qte[ 2 ] .
            if lignes.coef-cde < 0 then
               assign qte-sto = - ( qte-sto / lignes.coef-cde ) .
            if lignes.coef-cde > 0 then
               assign qte-sto =   ( qte-sto * lignes.coef-cde ) .

            x-chrono-par = lignes.chrono-par .
            REPEAT WHILE X-CHRONO-PAR <> 0 :

                FIND P-LIGNES where p-lignes.codsoc = lignes.codsoc
                              and   p-lignes.motcle = lignes.motcle
                              and   p-lignes.typcom = lignes.typcom
                              and   p-lignes.numbon = lignes.numbon
                              and   p-lignes.chrono = x-chrono-par
                              no-lock  no-error .
                if not available p-lignes  then  leave .
                if p-lignes.nivcom = "7"  then
                do :
                    qte-part = p-lignes.qte-fact .
                    if lignes.coef-cde < 0  then
                       qte-part = - ( qte-part / lignes.coef-cde ) .
                    if lignes.coef-cde > 0  then
                       qte-part =   ( qte-part * lignes.coef-cde ) .
                     qte-sto = qte-sto - qte-part .
                end .
                x-chrono-par = p-lignes.chrono-par .

            END .  /*  X-CHRONO-PAR <> 0  */

            if lignes.motcle = "VTE"  then  qte-sto = - qte-sto .

        end .

        /*             Si En Cours                  */

        if lignes.nivcom < "3"  then
        REPEAT :

            x-qte = lignes.qte[ 1 ] - lignes.qte[ 2 ] .

            if lignes.top-livre = "1"  then  x-qte = 0 .
            if x-qte = 0  then  leave .

            if lignes.motcle = "VTE"  then
            do :
                qte-enc = x-qte .
                if lignes.coef-cde < 0 then
                   assign qte-enc = - ( qte-enc / lignes.coef-cde ) .
                if lignes.coef-cde > 0 then
                   assign qte-enc =   ( qte-enc * lignes.coef-cde ) .
            end .

            else  do :
                qte-enf = x-qte .
                if lignes.coef-cde < 0 then
                   assign qte-enf = - ( qte-enf / lignes.coef-cde ) .
                if lignes.coef-cde > 0 then
                   assign qte-enf =   ( qte-enf * lignes.coef-cde ) .
            end .

            leave .

        END .  /*  REPEAT  */

        if qte-enc <> 0  or  qte-enf <> 0  or  qte-sto <> 0  then
        do :
            ii = lookup( string( lignes.magasin , "xxx" ) , ll-mag ) .
            if ii = 0 then
            do :
                if length(ll-mag) <> 0 then ll-mag = ll-mag + "," .
                assign ll-mag = ll-mag + string( lignes.magasin , "xxx" )
                       ii = lookup( string( lignes.magasin , "xxx" ) , ll-mag )
                       mousto[ ii ]    = 0
                       mouenc[ ii ]    = 0
                       mouenf[ ii ]    = 0
                       mouenc-tr[ ii ] = 0
                       mouenf-tr[ ii ] = 0 .
            end .
            mousto[ ii ] = mousto[ ii ] + qte-sto .

            if substring( lignes.typcom , 1 , 2 ) = "TR"
               then  assign  mouenc-tr[ ii ] = mouenc-tr[ ii ] + qte-enc
                             mouenf-tr[ ii ] = mouenf-tr[ ii ] + qte-enf .
               else  assign  mouenc[ ii ]    = mouenc[ ii ]    + qte-enc
                             mouenf[ ii ]    = mouenf[ ii ]    + qte-enf .
        end .

    END .  /*  FOR EACH LIGNES  */

    FOR EACH BASTAT where bastat.codsoc  =  codsoc-soc
                    and   bastat.articl  =  articl.articl
                    and   bastat.datdep  > date-arrete
                    and   lookup ( bastat.motcle, l-motcle ) <> 0
                    and   bastat.tenusto =  "1"
                    use-index artdat no-lock :

        qte-sto = bastat.qte .
        if bastat.motcle = "VTE"  or  bastat.motcle = "AJS"
           then  qte-sto = - qte-sto .

        ii = lookup( string( bastat.magasin , "xxx" ) , ll-mag ) .
        if ii = 0 then
        do :
            if length(ll-mag) <> 0 then ll-mag = ll-mag + "," .
            assign ll-mag = ll-mag + string( bastat.magasin , "xxx" )
                   ii     = lookup( string( bastat.magasin , "xxx" ) , ll-mag )
                   mousto[ ii ]    = 0
                   mouenc[ ii ]    = 0
                   mouenf[ ii ]    = 0
                   mouenc-tr[ ii ] = 0
                   mouenf-tr[ ii ] = 0 .
        end .
        assign mousto[ ii ] = mousto[ ii ] + qte-sto .

    END .  /*  FOR EACH BASTAT   */

    /*  Mise a Jour dans le fichier STOCKS de l'evenement = "" :
        ( Stock Temps Reel et En-Cours ) par Magasin :

        En deux etapes :
          1 - Par For each des magasins deja presents a l'arrete precedent .
          2 - Par creation des autres, pour lesquels des mouvements ont ete
              trouves depuis l'arrete precedent .
    */

    STOC1 :
    FOR EACH STOCKS where stocks.codsoc    = codsoc-soc
                    and   stocks.articl    = articl.articl
                    and   stocks.exercice  = ""
                    and   stocks.magasin   <> ""
                    and   stocks.evenement = ""
                    use-index stocks-1  exclusive-lock :

        assign  sqte-sto  = stocks.qte[ 2 ]
                sqte-enc  = stocks.qte[ 1 ]
                sqte-enf  = stocks.qte[ 3 ]
                sqte-enct = stocks.qte[ 4 ]
                sqte-enft = stocks.qte[ 5 ]
                ya-ecart  = no .

        /*    Acces au Stock du Dernier Arrete ( Exercice )   */

        qte-arr = 0 .
        FIND B-STOCKS where b-stocks.codsoc    = stocks.codsoc
                      and   b-stocks.articl    = stocks.articl
                      and   b-stocks.magasin   = stocks.magasin
                      and   b-stocks.exercice  = exe-arrete
                      and   b-stocks.evenement = even-stoarr
                      use-index stocks-1  no-lock  no-error .
        if available b-stocks  then  qte-arr = b-stocks.qte[ mois-arrete ] .

        ii = lookup( string( stocks.magasin , "xxx" ) , ll-mag ) .
        if ii <> 0 then
        do :
            /*    Ajout des Quantites trouvees dans les Lignes     */
            assign stocks.qte[ 2 ] = mousto[ ii ] + qte-arr
                   stocks.qte[ 1 ] = mouenc[ ii ]
                   stocks.qte[ 3 ] = mouenf[ ii ]
                   stocks.qte[ 4 ] = mouenc-tr[ ii ]
                   stocks.qte[ 5 ] = mouenf-tr[ ii ]
                   ii = index( ll-mag , string( stocks.magasin , "xxx" ) )
                   substring( ll-mag , ii, 3 ) = "   " .
        end .
        else assign stocks.qte[ 2 ] = qte-arr
                    stocks.qte[ 1 ] = 0
                    stocks.qte[ 3 ] = 0
                    stocks.qte[ 4 ] = 0
                    stocks.qte[ 5 ] = 0 .

          { majmoucb.i  "stocks" }

        lib-mess1 = '"' +
                    string( stocks.articl , "x(10)" ) + " " +
                    string( stocks.magasin , "xxx" )  + " : " .

        if sqte-enc <> stocks.qte[ 1 ]  and  recon-enco <> "O"  then
        do :
           lib-mess = lib-mess1 +
             " EC Vente :" + string( stocks.qte[ 1 ] , "zzzzzz9.999-" ) +
             "   Ecart : " + string( stocks.qte[ 1 ] - sqte-enc, "zzzzzz9.999-" ) +
             '"' .
            PUT STREAM KAN-EDI unformatted lib-mess skip .
            ya-ecart = yes .
        end .

        if sqte-enct <> stocks.qte[ 4 ]  and  recon-enco <> "O"  then
        do :
           lib-mess = lib-mess1 +
             " EC TRS :" + string( stocks.qte[ 4 ] , "zzzzzz9.999-" ) +
             "   Ecart : " + string( stocks.qte[ 4 ] - sqte-enct, "zzzzzz9.999-" ) +
             '"' .
            PUT STREAM KAN-EDI unformatted lib-mess skip .
            ya-ecart = yes .
        end .

        if sqte-enf <> stocks.qte[ 3 ]  and  recon-enco <> "O"  then
        do :
           lib-mess = lib-mess1 +
             " EC Achat :" + string( stocks.qte[ 3 ] , "zzzzzz9.999-" ) +
             "   Ecart : " + string( stocks.qte[ 3 ] - sqte-enf, "zzzzzz9.999-" ) +
             '"' .
            PUT STREAM KAN-EDI unformatted lib-mess skip .
            ya-ecart = yes .
        end .

        if sqte-enft <> stocks.qte[ 5 ]  and  recon-enco <> "O"  then
        do :
           lib-mess = lib-mess1 +
             " EC TRE :" + string( stocks.qte[ 5 ] , "zzzzzz9.999-" ) +
             "   Ecart : " + string( stocks.qte[ 5 ] - sqte-enft, "zzzzzz9.999-" ) +
             '"' .
            PUT STREAM KAN-EDI unformatted lib-mess skip .
            ya-ecart = yes .
        end .

        if sqte-sto <> stocks.qte[ 2 ] then
        do :
           lib-mess = lib-mess1 +
             " Stock TR :" + string( stocks.qte[ 2 ] , "zzzzzz9.999-" ) +
             "   Ecart : " + string( stocks.qte[ 2 ] - sqte-sto, "zzzzzz9.999-" ) +
             '"' .
            PUT STREAM KAN-EDI unformatted lib-mess skip .
            ya-ecart = yes .
        end .

        if majour <> lib-oui  or  ya-ecart = no  then  undo STOC1 , next STOC1 .

    END .  /*  FOR EACH STOCKS   */

    ii = 0 .
    STOC2 :
    REPEAT :

        if ll-mag = "" then leave .
        ii = ii + 1 .
        assign  magasin = substring( ll-mag , 1 , 3 )
                ya-ecart = no .

        if magasin <> ""  then
        do :
            qte-arr = 0 .
            FIND B-STOCKS where b-stocks.codsoc    = codsoc-soc
                          and   b-stocks.articl    = articl.articl
                          and   b-stocks.magasin   = magasin
                          and   b-stocks.exercice  = exe-arrete
                          and   b-stocks.evenement = even-stoarr
                          use-index stocks-1  no-lock  no-error .
            if available b-stocks then qte-arr = b-stocks.qte[ mois-arrete ] .

            CREATE STOCKS .
            assign stocks.codsoc    = codsoc-soc
                   stocks.articl    = articl.articl
                   stocks.magasin   = magasin
                   stocks.exercice  = ""
                   stocks.evenement = ""
                   stocks.qte[ 2 ] = mousto[ ii ] + qte-arr
                   stocks.qte[ 1 ] = mouenc[ ii ]
                   stocks.qte[ 3 ] = mouenf[ ii ]
                   stocks.qte[ 4 ] = mouenc-tr[ ii ]
                   stocks.qte[ 5 ] = mouenf-tr[ ii ] .

              { majmoucb.i  "stocks" }

            lib-mess1 = '"' +
                        string( stocks.articl , "x(10)" ) + " " +
                        string( stocks.magasin , "xxx" )  + " : " .

            if stocks.qte[ 1 ] <> 0  and  recon-enco <> "O"  then
            do :
                lib-mess = lib-mess1 +
                " EC Vente :" + string( stocks.qte[ 1 ] , "zzzzzz9.999-" ) +
                "   Ecart : " + string( stocks.qte[ 1 ] , "zzzzzz9.999-" ) +
                '"' .
                PUT STREAM KAN-EDI unformatted lib-mess skip .
                ya-ecart = yes .
            end .

            if stocks.qte[ 4 ] <> 0  and  recon-enco <> "O"  then
            do :
                lib-mess = lib-mess1 +
                " EC TRS :" + string( stocks.qte[ 4 ] , "zzzzzz9.999-" ) +
                "   Ecart : " + string( stocks.qte[ 4 ] , "zzzzzz9.999-" ) +
                '"' .
                PUT STREAM KAN-EDI unformatted lib-mess skip .
                ya-ecart = yes .
            end .

            if stocks.qte[ 3 ] <> 0  and  recon-enco <> "O"  then
            do :
                lib-mess = lib-mess1 +
                " EC Achat :" + string( stocks.qte[ 3 ] , "zzzzzz9.999-" ) +
                "   Ecart : " + string( stocks.qte[ 3 ] , "zzzzzz9.999-" ) +
                '"' .
                PUT STREAM KAN-EDI unformatted lib-mess skip .
                ya-ecart = yes .
            end .

            if stocks.qte[ 5 ] <> 0  and  recon-enco <> "O"  then
            do :
                lib-mess = lib-mess1 +
                " EC TRE :" + string( stocks.qte[ 5 ] , "zzzzzz9.999-" ) +
                "   Ecart : " + string( stocks.qte[ 5 ] , "zzzzzz9.999-" ) +
                '"' .
                PUT STREAM KAN-EDI unformatted lib-mess skip .
                ya-ecart = yes .
            end .

            if stocks.qte[ 2 ] <> 0 then
            do :
                lib-mess = lib-mess1 +
                " Stock TR :" + string( stocks.qte[ 1 ] , "zzzzzz9.999-" ) +
                "   Ecart : " + string( stocks.qte[ 2 ] , "zzzzzz9.999-" ) +
                '"' .
                ya-ecart = yes .
                PUT STREAM KAN-EDI unformatted lib-mess skip .
            end .

        end .  /*   magasin <> ""   */

        substring( ll-mag , 1 , 4 ) = "" .
        if majour <> lib-oui  or  ya-ecart = no  then  undo STOC2 , next STOC2 .

    END .  /*  REPEAT   */

END .  /*  FOR EACH ARTICL    */

OUTPUT STREAM KAN-EDI CLOSE .

message  "   Fin le :"  today  " a "  string( time , "hh:mm:ss" ) .
pause 0 .
