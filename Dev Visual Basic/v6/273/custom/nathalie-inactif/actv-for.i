/*----------------------------------------------------------------------------*/
/*                           PROJET ACTION VENTES                             */
/*                                                                            */
/* include :  actv-for.i :contient le for each de lecture de la table des     */
/* =========              clients par commercial pour cr�er les fichiers      */
/*                        faisant reference aux clients des societes 01 et 05 */
/*                                                                            */
/* DGR 07-03-01                                                               */
/*----------------------------------------------------------------------------*/

FOR EACH auxapr use-index auxapr-1
WHERE auxapr.codsoc = soc
AND   auxapr.typaux = "TIE"
AND   lookup(auxapr.repres[1], l-repres) <> 0

NO-LOCK :
