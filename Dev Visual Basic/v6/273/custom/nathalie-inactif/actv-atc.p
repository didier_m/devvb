/*----------------------------------------------------------------------------*/
/*                           PROJET ACTION VENTES                             */
/*                                                                            */
/* programme: actv-atc.p : liste des commerciaux                              */
/* =========                                                                  */
/*                                                                            */
/* EVI 26-11-02                                                               */
/*----------------------------------------------------------------------------*/
{connect.i}
{top-asec.i}
{actv-000.i}

message "actv-atc.p: extraction fichier Commerciaux: debut a " + STRING (TIME, "HH:MM:SS") .

wfic = wrep + "atc.txt".

FOR EACH tabges
WHERE tabges.codsoc = "  "
AND   tabges.etabli = "   "
AND   tabges.typtab = "CLI"
AND   tabges.prefix = "REPRES"
/*
AND   lookup(tabges.codtab,l-repres) <> 0
*/
NO-LOCK :

    /* ligne du fichier de sortie ... */
    /* Evi le 29/08/02 ajout du trim a codaux pour suppression des blancs */
    chaine = trim(tabges.codtab)                + ";" +
             trim(tabges.libel1[1])              .

    /* ecriture de la ligne dans le fichier global */
    run actv-glo.p(input wfic, input chaine).
END.

message "actv-atc.p: extraction fichier commerciaux: fin a " + STRING (TIME, "HH:MM:SS") .
