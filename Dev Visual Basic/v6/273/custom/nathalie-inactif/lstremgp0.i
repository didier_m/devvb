/* lstremgp0.i : Liste des remises GP */
/* variables */

def {1} shared var i            as int                              no-undo .
def {1} shared var i-1          as int                              no-undo .
def {1} shared var i-2          as int                              no-undo .
def {1} shared var j            as int                              no-undo .
def {1} shared var z-choix      as char                             no-undo .

def {1} shared var wdatdeb            as date format "99/99/9999" no-undo.
def {1} shared var wdatfin            as date format "99/99/9999" no-undo.

def {1} shared var wcpteca            as int                      no-undo.
def {1} shared var wcptlus            as int                      no-undo.

/* variables pour edition */
def {1} shared var periph       as char format "x(12)"          no-undo.
def {1} shared var sav-periph   as char format "x(12)"          no-undo.
def {1} shared var sequence     as char format "x(05)"          no-undo.
def {1} shared var sel-sequence as char format "x(12)"          no-undo.

def {1} shared frame fr-saisie .

form "Date debut                     : " wdatdeb    format "99/99/9999" skip
     "Date fin                       : " wdatfin    format "99/99/9999" skip
     "Fichier                        : " periph     format "x(15)"
     with frame fr-saisie
     row 3 centered overlay no-label {v6frame.i} .
