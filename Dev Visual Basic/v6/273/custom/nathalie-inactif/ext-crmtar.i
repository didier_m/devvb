/*==========================================================================*/
/*                           E X T - C R M T A R  . I                       */
/* Extraction des donnees pour VENTES PARTNER                               */
/*--------------------------------------------------------------------------*/
/* Extraction Tarifs                                                        */
/*==========================================================================*/
    Code = trim(codsoc-trait[nsoc]) + "/" + artapr.articl.
    Tarif = tarcli.valeur.
    Tarif_Datmaj = tarcli.datmaj.
/*------------------------------------*/
/* Ecriture dans le fichier crm-tar   */
/*------------------------------------*/
    export stream crm-tar delimiter ";"
           Code Tarif Tarif_Datmaj.                                     