/* -------------------------------------------------------------------------- */
/*              creart-edi.i CREATION AUTOMATIQUE ARTICLES :                  */
/* -------------------------------------------------------------------------- */
/* module d'edition (commun a l'extraction et a l'integration)                */
/* -------------------------------------------------------------------------- */
/* DGR 180506 : creation auto en societe 01,05,08                             */
/* DGR 051208 : famart2                                                       */

  export stream file1 delimiter ","
    wtopcre                                          /* top creation oui / non */
    warticl                                          /* code article DEAL */
    warticl-fou                                      /* code-article plateforme */
    wlib                                             /* libelle article */
    wlibred                                          /* libelle court */
    wfamart                                          /* famille */
    wsoufam                                          /* sous famille */
    wsssfam                                          /* sous sous famille */
    wpxv-indics                                      /* PVI LISADIS       */
    wuca                                             /* unite de commande  */
    wcondita                                         /* conditionnement achat */
    wsurcondita                                      /* surcondit obligatoire / facultatif */
    wcoefcdeas                                       /* coef unite de commande unite de vente */
    wuvte                                            /* unite de stock */
    wufac                                            /* unite de facturation */
    wcoefvtefacs                                     /* coef usto / ufac */
    wconditv                                         /* conditionnement vente */
    wsurconditv                                      /* surcondit obligatoire / facultatif */
    wvolumes                                         /* Volume */
    wpdss                                            /* poids */
    wedieti                                          /* Edition Etiquette */
    wprxmodif                                        /* prix modifiable */
    wtypo                                            /* typologie */
    wassort                                          /* Assortiment */
    wcodblo                                          /* code blocage */
    wprovenance                                      /* provenance */
    wstocke                                          /* stock plateforme */
    wstatut                                          /* statut plateforme */
    wcodtva                                          /* TVA */
    wtxtva                                           /* TVA */
    wgencod                                          /* Gencod */
    wpmp-inits                                       /* pamp */
    wdatcs                                           /* date creation plate-forme */
    wdatms                                           /* date de modif plate forme */
    wcodfou1                                         /* code fournisseur plate-forme */
    wlibfou1                                         /* libelle fournisseur plateforme */
    wreffou                                          /* reference article fournisseur plate-forme */
    wstat-rayon                                      /* coef stat-rayon */
    wartrpl2                                         /* article rempl. */
    l-cresoc                                         /* societes a creer */
    wfamart2                                         /* famille 2 */
    wsoufam2                                         /* sous famille 2 */
    wsssfam2                                         /* sous sous famille 2 */
    wmotifrej                                        /* motif de rejet */
    wz1
    wz2    .
