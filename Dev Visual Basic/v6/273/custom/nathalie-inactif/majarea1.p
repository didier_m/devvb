/* Prog : majarea1.p Modif code fournisseur */
output to majarea1.txt append.
def var wbon  as char format "x(10)" initial "0004061800".
def var wt    as char format "xxx" initial "CDF".
def var wm    as char format "xxx" initial "ACH" .
def var ws    as char format "xx"  initial "45".
def var wmaj  as char format "x"  initial "N".
def var wcod  as char format "xxxxxx".
def var ancienne-reservation  as char format "x(15)".
def var new-reservation  as char format "x(15)".

repeat :

update wt.
update wbon.
update wmaj.
update wcod.
update ancienne-reservation.
update new-reservation.

find auxapr where 
     auxapr.codsoc = ws 
 and auxapr.typaux = "TIE"
 and auxapr.codaux = "    " + wcod
 no-lock no-error.
if not available auxapr then do:
   message "Code fournisseur inexistant " wcod. 
   pause.
   next.
   end.
   
for each entete where 
 codsoc = ws    and
 motcle = wm    and 
 typcom = wt    and
 numbon = wbon
 exclusive-lock:

message typcom numbon codaux datcre opecre adres[1] wcod auxapr.adres[1]. 
pause.

for each lignes where 
lignes.codsoc = entete.codsoc and
lignes.motcle = entete.motcle and
lignes.typcom = entete.typcom and
lignes.numbon = entete.numbon exclusive-lock:
message "Lig " chrono articl codaux ref-marche.
pause.
put "export lignes avant" skip.
export lignes.
if wmaj = "O" then do:
   lignes.codaux = auxapr.codaux.
   if lignes.ref-marche = ancienne-reservation then do:
      lignes.ref-marche = new-reservation. 
      put "Marche chang� ancien : " ancienne-reservation  " nouveau : " new-reservation skip .
   end.   
   put "export lignes apr�s" skip.
   export lignes.
end.

end.
export entete.
if wmaj = "O" then do:
   entete.codaux = auxapr.codaux.
   entete.adres[1] = auxapr.adres[1].
   entete.adres[2] = auxapr.adres[2].
   entete.adres[3] = auxapr.adres[3].
   entete.adres[4] = auxapr.adres[4].
   entete.adres[5] = auxapr.adres[5].
end.

end.

end.
