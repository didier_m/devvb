
{ bat-ini.i  "new" }

def input parameter fichier as char .

output to lanceGV.log.

def var heudeb as int.
def var heu1   as int.
def var heu2   as int.

heudeb = time.
put unformatted 
  "lanceGV.P === DEBUT changement nomenclature GV le " 
  string (today,"99/99/9999")
  " a " string(heudeb,"hh:mm:ss") skip(1).

heu1 = time.
put unformatted 
  "supnomenc.p === suppression Nomenclature <= 010 ----" 
  " debut a " string(heu1,"hh:mm:ss") .

run supnomenc.p. 

heu2 = time.
put unformatted 
  "  fin a " string(heu2 ,"hh:mm:ss") 
  "  DUREE = " string(heu2 - heu1, "hh:mm:ss")
  skip(1).

heu1 = time.
put unformatted 
  "crenomenc-2.p === Creation Nomenclature <= 010 ----" 
  " debut a " string(heu1,"hh:mm:ss") .

run crenomenc-2.p. 

heu2 = time.
put unformatted 
  "  fin a " string(heu2 ,"hh:mm:ss") 
  "  DUREE = " string(heu2 - heu1, "hh:mm:ss")
  skip(1).

heu1 = time.
put unformatted 
  "majnomgv1.p === MAJ articles LISADIS ----" 
  " debut a " string(heu1,"hh:mm:ss") .

run majnomgv1.p. 

heu2 = time.
put unformatted 
  "  fin a " string(heu2 ,"hh:mm:ss") 
  "  DUREE = " string(heu2 - heu1, "hh:mm:ss")
  skip(1).

heu1 = time.
put unformatted 
  "majnomgv2.p === MAJ articles NON LISADIS ----" 
  " debut a " string(heu1,"hh:mm:ss") .

run majnomgv2.p.

heu2 = time.
put unformatted 
  "  fin a " string(heu2 ,"hh:mm:ss") 
  "  DUREE = " string(heu2 - heu1, "hh:mm:ss")
  skip(1).


put unformatted 
  "  TRAITEMENT TERMINE le " string(today,"99/99/9999")
  " a " string(time,"hh:mm:ss" )
  "  DUREE = " string(time - heudeb, "hh:mm:ss")
  skip(1).
