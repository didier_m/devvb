/****************************************************************************/
/*==========================================================================*/
/*                            I M M - A G I L 0 . P                         */
/* Envoi imm�diat des donnees pour AGIL totalit� des infos produit          */
/*--------------------------------------------------------------------------*/
/* Extraction imm�diate d'un article et de ses tarifs                       */
/* Principe de traitement :                                                 */
/*    Saisie d'un article seul : on envoie les donn�es articles ainsi que   */
/*        tous les tarifs en cours ou � venir � tous les magasins           */
/*    Sinon, on envoie les tarifs au magasin ou au groupe de magasin saisi  */
/* Ce programme valide la saisie des donn�es et appelle le programme        */
/* imm-agilt1.p qui constitue les fichiers                                  */
/*==========================================================================*/
{ connect.i        }
{ bat-ini.i  "new" }
{ aide.i     "new" }
def buffer tabges-art for tabges.

/*-------------------------------------*/
/* Definition des variables de travail */
/*-------------------------------------*/
def var soc-groupe       like artapr.codsoc init "00".
def var l-article-cvo    as char init "980635,980636,980637,981104,981105".
def var I_PARAM as char.

/*-------------------------------------*/
/* Definition des variables de saisie  */
/*-------------------------------------*/
def var warticle                as char format "x(6)".
def var type-magasin     as char format "x".
def var wmagasin                as char format "xxx".
def var wlibart                 as char format "x(40)".
def var wlibmag                 as char format "x(40)".
def var WOK                     as char format "x"     no-undo.

/*-------------------------------------------*/
/* saisie des donn�es                        */
/*-------------------------------------------*/

do with frame a: /* do saisie */
form
  "  " skip
  "Article               : " at 01 warticle   no-label wlibart no-label
  skip
  "Type de Magasin       : " at 01 type-magasin  no-label
  skip
  "Magasin               : " at 01 wmagasin  no-label wlibmag no-label
  skip
  skip
  "Validation            : " at 01 WOK no-label
with title "imm-agil1.p : Envoi imm�diat de donn�es aux magasins " centered.

        update warticle.
/*-------------------------------------------*/
/*  controle du code article                 */
/*-------------------------------------------*/
        If warticle = "" 
                then do :
                        message "Article obligatoire".
                        pause.
                        undo, retry.
                end.
        
        find artapr where artapr.codsoc = soc-groupe
                                and   artapr.articl = warticle
                no-lock no-error.
        
        wlibart = "".
        if available artapr then
           wlibart = artapr.libart1[1].
        display wlibart.
           
                If not available artapr 
                        then do :
                                warticle = "".
                                message "Article inconnu ".
                                pause.
                                undo, retry.
                        end.
                
/*-------------------------------------------*/
/*  controle des magasins                    */
/*-------------------------------------------*/
        update type-magasin help "G=Groupe Magasin, M=Magasin sp�cifique, T=Tous magasins Agil".
                
        If type-magasin <> "" and 
                type-magasin <> "G" and         
                type-magasin <> "M" and
                type-magasin <> "T"
                then do :
                        type-magasin = "".
                        message "Type magasin incorrect G, M ou T ".
                        pause.
                        undo, retry.
        end.

        update wmagasin help "Code Groupe de Magasin ou Code Magasin".

        If (type-magasin = "G" or       
           type-magasin = "M") and
           wmagasin = ""
                then do :
                        message "Code magasin obligatoire ".
                        pause.
                        undo, retry.
        end.

        If type-magasin = "" and 
                wmagasin <> ""
                then do :
                        message "Type magasin incorrect G, M ou T ".
                        pause.
                        undo, retry.
        end.
        
        If type-magasin = "G" then do :
                find tabges where tabges.codsoc = ""
                                          and tabges.etabli = "" 
                                          and tabges.typtab = "MAG"
                                          and tabges.prefix = "GROUPE-PRO"
                      and tabges.codtab = wmagasin
                no-lock no-error.
                if not available tabges
                   then do :
                                message "Groupe Magasin incorrect".
                                pause.
                                undo, retry.
                        end.
                wlibmag = "".
                if available tabges then
                        wlibmag = tabges.libel1[1].
                display wlibmag.
        end.
        
        If type-magasin = "M" then do :
                find magasi where magasi.codsoc = ""
                      and magasi.codtab = wmagasin
                no-lock no-error.
                wlibmag = "".
                if available magasi then
                        wlibmag = magasi.libel1[1].
                display wlibmag.
                if not available magasi or
                   magasi.equipe <> "A"
                   then do :
                                message "Magasin non Agil".
                                pause.
                                undo, retry.
                        end.
        end.

        update WOK 
                help "Confirmation Oui / Non"
                validate (WOK = "O" or WOK = "N" , "Valeurs autorisees : O ou N").
        
        if WOK = "N"
           then leave.


end.   /* Fin de Saisie  */

/*---------------------------------------------------*/
/* Appel du programme de constitution des fichiers   */
/*---------------------------------------------------*/
        I_PARAM = "OPERAT=" + operat + "|TYPMAG=" +  type-magasin + "|MAGASIN=" + wmagasin + "|ARTICLE=" + warticle.

        run imm-agilt1.p ( INPUT I_PARAM ) .