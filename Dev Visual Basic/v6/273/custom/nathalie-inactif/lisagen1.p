/* Programme lisagen1.p Liste des ventes d'un article /client" */
/* modif dgr 27/02/02 : on prend aussi les codes atc a blanc   */
/*   modif DGR 241104 pour V52                                 */
/*   modif cco 07032012 pour MAT 93                            */
{connect.i}
def stream s-edition.
def var wauxilicodsoc as char              format "x(2)"             no-undo.
def var scodsoc       as char              format "x(2)"             no-undo.
def var sdtfacmin        like              bastat.datfac             no-undo.
def var sdtfacmax        like              bastat.datfac             no-undo.
def var sequence      as char              format "x(5)"             no-undo.
def var sel-sequence  as char              format "x(12)"            no-undo.
def var sedifile      as char              format "x(12)"            no-undo.
def var valid         as char              format "x".

define var wcodaux as char format 'x(6)'.
define var wdsart  as char format 'x(20)'.
define var wnom    as char.
define var montant as char.
define var quant   as char.
def var wdtpam     as date.
def var sdtpam     as date.
def var wpamp      as dec decimals 3 format "->>>>>>>9.999".
def var xpamp      as char.
   
scodsoc = codsoc-soc.
sdtfacmin = date(01,01,1901).
sdtfacmax = date(12,31,2901).
if scodsoc <> "93" then do:
   message "Le programme lisagen1.p ne concerne que le MAT_93, faites f4".
   pause(5).
end.
else do with frame a: /* do saisie */
/* determination No sequence operateur pour spoolname debut */
assign codsoc = " "
       etabli = " "
       typtab = "ope"
       prefix = "nume-spool"
       codtab = operat.
run opeseq.p ("req",output sequence, output sel-sequence).
/* determination No sequence operateur pour spoolname fin */
sedifile = sel-sequence.

substr(sedifile,10,3) = "csv".

form
     "soc"        at 01      scodsoc       colon  5 no-label
     skip
     "facture du" at 01      sdtfacmin     colon 12 no-label
     "au"         at 30      sdtfacmax     colon 33 no-label
     skip
     "imprimante" at 01      sedifile      colon 12 no-label
     skip
     "Validation ..... : " at 01    valid     colon 20 no-label
          skip
     with title
     "lisagen1.p : EUREACOOP MAT Ventes par agent, impression laser".
display scodsoc sdtfacmin sdtfacmax sedifile.
update sdtfacmin
       help "date facture minimale"
       validate (sdtfacmin <> ?,"blanc refuse").
update sdtfacmax
       help "date facture maximale"
       validate (sdtfacmax <> ?,"blanc refuse").
update valid
       help "Validation O / N"
       validate (valid <> " ", "blanc refuse")
       auto-return.
                     
If valid = "N" then leave.

regs-app = "ELODIE".
{ regs-cha.i }

regs-app = "NATHALIE" .
{ regs-cha.i }

regs-fileacc = "AUXILI" + "TIE".
{ regs-rec.i }
wauxilicodsoc = regs-soc .

/*          
wauxilicodsoc = "01".
if codsoc-soc = "08" then wauxilicodsoc = codsoc-soc.
*/
output stream s-edition to value(sedifile).

export stream s-edition delimiter ";"
       "Agent" "Mag" "Fam" "Sfa" "Article" "Designation" 
       "Date_Fact" "N_FACT" "Typbon" "Numbon" "Client" "Nom_Client" "Quantite" 
       "Montant_HT" "Typ" "Date_Pamp" "Pamp".

message "lisagen1.p recherche les informations dans bastat".
for each bastat where
	    bastat.codsoc = "93"
	and bastat.motcle = "vte"
	and bastat.datfac >= sdtfacmin
	and bastat.datfac <= sdtfacmax
	     no-lock,
       each pronat.artapr where
	    pronat.artapr.codsoc = bastat.codsoc
	and pronat.artapr.articl = bastat.articl
	    no-lock
break  by bastat.repres
       by artapr.famart
       by artapr.soufam
       by bastat.articl:
       wcodaux = substring(bastat.codaux,5,6).
       wdsart  = pronat.artapr.libart1[1].

wnom = "?????".
find auxili where 
     auxili.codsoc = wauxilicodsoc and
     auxili.typaux = bastat.typaux and
     auxili.codaux = bastat.codaux 
no-lock no-error.     

if available auxili then wnom = auxili.adres[1].

montant = string(bastat.fac-ht, "->>>>>>>>9.99").
montant = replace(montant,".",",").

quant = string(bastat.qte, "->>>>>>>>9.999").
quant = replace(quant,".",",").

wdtpam  = ?.
wpamp   = 0. 
xpamp   = "".
sdtpam = bastat.datdep.
  
find stocks where 
     stocks.codsoc    = bastat.codsoc
 and stocks.articl    = bastat.articl
 and stocks.magasin   = ""
 and stocks.evenement = "STOARR"
 and stocks.exercice  = string(year(sdtpam),"9999")
     no-lock no-error.
if available stocks then
   assign wdtpam = sdtpam
          wpamp  = stocks.prix[month(sdtpam)] .
xpamp = string(wpamp, "->>>>>>>>9.999").
xpamp = replace(xpamp,".",",").

export stream s-edition delimiter ";"
    bastat.repres
    bastat.magasin 
    artapr.famart
    artapr.soufam 
    bastat.articl 
    wdsart 
    bastat.datfac
    bastat.numfac 
    bastat.typcom 
    bastat.numbon 
    wcodaux
    wnom  
    quant 
    montant
    bastat.typbon
    wdtpam
    xpamp
    .
   end. /* for each */

output stream s-edition close.

message "lisagen1.p liste ajoutee en" sedifile .

end. /* do  saisie */
