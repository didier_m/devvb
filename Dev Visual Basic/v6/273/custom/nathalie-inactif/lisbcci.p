/* Prog : lisbcci.p Liste des bons CCI famille 012 ou autres secheresse 2011 */

{connect.i}

def var tot as dec decimals 2.
def var wetoi as char format "xxx" .
def var nbb as int.

def var l-niv as char initial ",1".

def var wsoc  as char. 
def var wniv  as char.

def var aa    as int.
def var bb    as int.
def var wdate as date initial today .
def var wfam  as char initial "012" format "xxx".
def var sequence     as char no-undo .
def var sel-sequence as char no-undo .
def var sedifile      as char              format "x(12)"            no-undo.

/* determination No sequence operateur pour spoolname debut */
assign codsoc = " "
       etabli = " "
       typtab = "ope"
       prefix = "nume-spool"
       codtab = operat.
run opeseq.p ("req",output sequence, output sel-sequence).
sedifile = sel-sequence.

output to value(sedifile) .

update wdate.
update wfam.

wsoc = codsoc-soc.

do bb = 1 to num-entries( l-niv ) :
wniv = entry( bb, l-niv ) .

for each entete
where entete.codsoc = wsoc
and   entete.motcle = "VTE"
and   entete.typcom = "CCI"
and   entete.nivcom = wniv
and   entete.datbon >= wdate
no-lock by datcre by heucre :
for each lignes where
lignes.codsoc = entete.codsoc and
lignes.motcle = entete.motcle and
lignes.typcom = entete.typcom and
lignes.numbon = entete.numbon and
lignes.articl <> ""
no-lock:

find artapr where 
artapr.codsoc = lignes.codsoc and
artapr.articl = lignes.articl
no-lock.
if artapr.famart <> wfam then next.
display
lignes.codsoc no-label format "xx" 
typcom no-label numbon no-label
entete.zon-int-4 column-label "V" format "9"
env-pla format "xxx" column-label "Env"
lignes.datcre lignes.heucre lignes.opecre column-label "Ope"
lignes.articl format "xxxxxx" lignes.qte[1] column-label "Qte" format ">>>>9"
lignes.usto no-label format "xx"
libart1[1] format "x(16)".

end.   /* lignes */

end.   /* entete */
               
end.   /* niv */

output close.