/* visusoldtiers.i   Table Temporaire */

def {1} shared temp-table soldcli
    field mag    as char format "xxx"
    field codaux as char format "x(6)"
    field nom    as char
    field nomtri as char
    field ble    as int format "->>>>9"
    field mais   as int format "->>>>9"
    field orge   as int format "->>>>9"
    field tritic as int format "->>>>9"
    field apport as int format ">>>>>9-"
    field sortie as int format ">>>>9-"
    field remere as int format ">>>>>9-"
    field t-chrono as int    
index cli codaux
index nomcli nomtri
index chrono t-chrono.