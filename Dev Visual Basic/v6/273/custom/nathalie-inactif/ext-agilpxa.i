/*==========================================================================*/
/*                           E X T - A G I L P X A  . I                     */
/* Extraction des donnees pour AGIL                                         */
/*--------------------------------------------------------------------------*/
/* Extraction des Prix d'achat                                              */
/* VLE 10022010 : PA = PA - 0,01 pour palier au bug arrondi Agil            */
/* VLE 24032011  Modification de la date de debut d'application             */
/*               du prix d'achat                                            */
/* VLE 04012012  Mise en place des soldes	    		            */
/* VLE 14022012  modification date applis en fin de soldes                  */
/* VLE 25062012  On ne traite que le nombre de d�marques renseign�es dans   */
/*               la table des dates de soldes                               */
/* VLE 18062013  gestion du changement de prix pendant la p�riode de solde  */
/*               on ne change pas les prix d'achat pendant la periode de    */
/*               soldes, on positionne le nouveau prix d'achat � date de fin*/
/*               de soldes + 1 jour                                         */ 
/* VLE 31072013  correction anomalie suite � modif ci-dessus, la date       */
/*               d 'application n'etait plus reseign�e en periode hors solde*/
/* VLE 22102013  traitement des nouveaux codes tva                          */
/*==========================================================================*/

if lookup(artapr.famart, l-fam-gp) <> 0 then do :
/*------------------------------------------------------------------------------------------------------*/                        
/*  Ecriture du d�tail de section Prix d'achat calcul� � partir du HTC                                  */
/*------------------------------------------------------------------------------------------------------*/ 
	
   PX_ACHAT = "0,00".
   DT_APPLI_TARIF_ACHAT = {tarcli.i}.date-debut.
   ID_ARTICLE = CAPS(substr({tarcli.i}.articl, 1, 6)).
   tx-srvp = 0.
   tx-demarq = 0.

   find tabges-taux where tabges-taux.codsoc = " "
    and tabges-taux.etabli = " "
    and tabges-taux.typtab = "ART"
    and tabges-taux.prefix = "SOUFAM2" + artapr.famart2
    and tabges-taux.codtab = artapr.soufam2
   no-lock no-error.
	
   if available tabges-taux then do :
	tx-srvp = tabges-taux.nombre[1].
	tx-demarq[1] = tabges-taux.nombre[6].
	tx-demarq[2] = tabges-taux.nombre[7].
	tx-demarq[3] = tabges-taux.nombre[8]. 
   end.
	
   /*  on enleve 0.01 pour les pb de calcul d'arrondi avec Agil */
   do indtva = 1 to 20 :
      if artapr.tva = code-tva[indtva] then do :  
         val-tarif = round(val-tarif / ( 1 + (taux-tva[indtva] / 100)),2).
         leave.
      end.
   end.
   val-tarif = val-tarif - 0.01.

   /* traitement des prix d'achat hors p�riode de soldes */
   if soldes-actives = "N" then do :
      if {tarcli.i}.date-debut < today
	   then DT_APPLI_TARIF_ACHAT = today.
      val-prix = round(val-tarif - (val-tarif * tx-srvp / 100),2) - 0.01 .
	PX_ACHAT = trim(replace(string(val-prix, "->>>>9.99"), ".", ",")).
	put stream temp_tar unformatted
	    Type_Ligne_Det ID_ARTICLE "~011" DT_APPLI_TARIF_ACHAT "~011" PX_ACHAT "~015" skip. 
   end.

   /* traitement des prix d'achat pendant les soldes */	
   else do :

      /* traitement d'initialisation des soldes */
      if soldes-agil = "O" then do :  
     	   /* on traite les 3 d�marques */
	   do ind-sold = 1 to 3 :
             if dt-deb-dem[ind-sold] <> ? then do:     
		 DT_APPLI_TARIF_ACHAT = dt-deb-dem[ind-sold].
		 val-prix = round(val-tarif - (val-tarif * tx-demarq[ind-sold] / 100),2) - 0.01 .
		 PX_ACHAT = trim(replace(string(val-prix, "->>>>9.99"), ".", ",")).
		 put stream temp_tar unformatted
		     Type_Ligne_Det ID_ARTICLE "~011" DT_APPLI_TARIF_ACHAT "~011" PX_ACHAT "~015" skip. 
	       end.
         end.   
	   /* on g�n�re le PA normal � date de fin de soldes + 1 */
	   DT_APPLI_TARIF_ACHAT = dt-fin-soldes + 1.
         val-prix = round(val-tarif - (val-tarif * tx-srvp / 100),2) - 0.01 .
	   PX_ACHAT = trim(replace(string(val-prix, "->>>>9.99"), ".", ",")).
	   put stream temp_tar unformatted
	       Type_Ligne_Det ID_ARTICLE "~011" DT_APPLI_TARIF_ACHAT "~011" PX_ACHAT "~015" skip.
	end.
	else do : /* hors traitement d'initialisation */
         /* on ne modifie pas les PA pendant la p�riode de soldes */
         if {tarcli.i}.date-debut >= dt-deb-soldes and 
            {tarcli.i}.date-debut <= dt-fin-soldes  
            then DT_APPLI_TARIF_ACHAT = dt-fin-soldes + 1.
         if {tarcli.i}.date-debut > dt-fin-soldes
            then DT_APPLI_TARIF_ACHAT = {tarcli.i}.date-debut.
         val-prix = round(val-tarif - (val-tarif * tx-srvp / 100),2) - 0.01 .
	   PX_ACHAT = trim(replace(string(val-prix, "->>>>9.99"), ".", ",")).
	   put stream temp_tar unformatted
	       Type_Ligne_Det ID_ARTICLE "~011" DT_APPLI_TARIF_ACHAT "~011" PX_ACHAT "~015" skip. 
      end.  

   end. /* soldes actives */

end. /* fin px achat seulement sur GP */