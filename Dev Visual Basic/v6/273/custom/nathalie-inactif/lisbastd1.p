/* Programme lisbastd1.p Liste des ventes d'un article /client" */
/*   modif DGR 221104 pour V52                                                */
/*   modif DGR 170910 pour rajouter LIM,LIC,LSC                               */
/* -------------------------------------------------------------------------- */
/* Acces parametre lecture societe article ==> OK                             */
/* Acces parametre lecture societe auxili  ==> OK                             */
/* -------------------------------------------------------------------------- */

{connect.i}
def stream s-edition.
def stream etiquette.
def var tot-art       as deci decimals 2   format ">>>,>>>,>>9.99-"  no-undo.
def var tot-art-qte   as deci decimals 2   format ">>>,>>>,>>9.99-"  no-undo.
def var hp            as char              format "x(8)"            no-undo.
def var hp-ital       as char              format "x(8)" initial "~033&l1O"
                                                                     no-undo.
def var hp-16po       as char              format "x(8)" initial "~033&k2S"
                                                                     no-undo.
def var wpage-size    as integer                                     no-undo.
def var wdat1            like              bastat.datfac             no-undo.
def var wdat2            like              bastat.datfac             no-undo.
def var scodsoc       as char              format "x(2)"             no-undo.
def var sarticl       as char              format "x(10)"            no-undo.
def var narticl       as integer           format "999999"           no-undo.
def var xarticl       as char              format "x(6)"             no-undo.
def var sdsart        as char              format "x(32)"            no-undo.
def var sdtfacmin        like              bastat.datfac             no-undo.
def var sdtfacmax        like              bastat.datfac             no-undo.
def var simp          as logical           format "O/N"              no-undo.
def var seti          as logical           format "O/N"              no-undo.
def var stri          as char              format "x"                no-undo.
def var strilib       as char              format "x(30)"            no-undo.
def var sequence      as char              format "x(5)"             no-undo.
def var sel-sequence  as char              format "x(12)"            no-undo.
def var setifile      as char              format "x(12)"            no-undo.
def var sedifile      as char              format "x(12)"            no-undo.
def var snbeti        as integer           format ">>>>>>"           no-undo.
def var snbetilib     as char              format "x(25)"            no-undo.

def var articl-soc    like artapr.codsoc.
def var auxili-soc    like auxili.codsoc.
def var type-tiers    like auxili.typaux init "TIE".

regs-app = "ELODIE" .
{ regs-cha.i }

regs-app = "NATHALIE" .
{ regs-cha.i }

{ regs-inc.i "articl" }
articl-soc = regs-soc .

regs-fileacc = "AUXILI" + type-tiers.
{regs-rec.i}
auxili-soc = regs-soc .

scodsoc = codsoc-soc .
sdtfacmin = date(01,01,1901).
sdtfacmax = date(12,31,2901).
stri = "1".
seti = false.
do with frame a: /* do saisie */
/* determination No sequence operateur pour spoolname debut */
assign codsoc = " "
       etabli = " "
       typtab = "ope"
       prefix = "nume-spool"
       codtab = operat.
run opeseq.p ("req",output sequence, output sel-sequence).
/* determination No sequence operateur pour spoolname fin */
sedifile = sel-sequence.
repeat with frame a:
form
     "soc"        at 01      scodsoc       colon  5 no-label
     "art"        at 08      sarticl       colon 12 no-label
                             sdsart        colon 23 no-label
     skip
     "facture du" at 01      sdtfacmin     colon 12 no-label
     "au"         at 23      sdtfacmax     colon 26 no-label
     skip
     "tri       " at 01      stri          colon 12 no-label
                             strilib       colon 26 no-label
     skip
     "etiquettes" at 01      seti          colon 12 no-label
                             setifile      colon 26 no-label
                             snbeti        colon 36 no-label
                             snbetilib     colon 45 no-label
     skip
     "imprimante" at 01      simp          colon 12 no-label
                             sedifile      colon 26 no-label
     skip
     "total ht  " at 01      tot-art       colon 12 no-label
     skip
     "total qte " at 01      tot-art-qte   colon 12 no-label
     with title
     "lisbastd1.p : Liste des ventes d'un article /client,version 15 09 97".
display scodsoc stri seti simp.
update sarticl
       help "Code article"
       validate (sarticl <> " ","blanc refuse")
       auto-return.
/* narticl = integer(sarticl).
sarticl = string(narticl,"999999"). */
display sarticl.
find artapr where
     artapr.codsoc = articl-soc
 and artapr.articl = sarticl
     no-lock no-error.
if not available artapr then do:
   sdsart = "<- inconnu". display sdsart.
   message "code article inconnu".
   next.
end.
sdsart = artapr.libart1[1].
display sdsart.
update sdtfacmin
       help "date facture minimale"
       validate (sdtfacmin <> ?,"blanc refuse").
update sdtfacmax
       help "date facture maximale"
       validate (sdtfacmax <> ?,"blanc refuse").
update stri
       help "1 tri /No client, 2 /Depot/client ".
if stri = "1" then do:
   strilib = "tri sur numero client".
end.
if stri <> "1" then do:
   strilib = "tri sur depot, client".
end.
display strilib.
update seti
       help "N pas d'etiquettes,O avec"
       auto-return.
update simp
       help "O imprimante laser (format italien),N non laser (portrait)"
       auto-return.
display sedifile.
setifile = " ". snbeti = 0. snbetilib = " ".
if seti = true then do:
   assign codsoc = " " etabli = " " typtab = "ope" prefix = "nume-spool"
          codtab = operat.
   run opeseq ("eti",output sequence,output sel-sequence).
   setifile = sel-sequence.
end.
display setifile snbeti snbetilib.
tot-art = 0.
display tot-art.
tot-art-qte = 0.
display tot-art-qte.
wdat1 = sdtfacmin. wdat2 = sdtfacmax.
hp = hp-16po. wpage-size = 60.
if simp then hp = hp-ital. wpage-size = 42.
output stream s-edition to value(sedifile)
                        page-size value(wpage-size) paged append.
if seti = true then do : /* etiquette de presentation */
   output stream etiquette to value(setifile).
   put stream etiquette
       "lisbastd1.p, etiqcoli.maq, " setifile
       ", liste des ventes de " scodsoc " " sarticl " " sdsart strilib
       " de " wdat1 " au " wdat2
       skip.
end. /* etiquette de presentation */
form header hp
     today "Liste des ventes de" scodsoc sarticl sdsart strilib skip
     "Lisbastd1.p" sedifile "std132li.maq Date debut :"
     wdat1 " Date de fin :" wdat2
     with frame s page-top width 132.
view stream s-edition frame s.
message "lisbastd1.p recherche les informations dans bastat".
if stri = "1" then do: /* stri 1 */
for each bastat where
         bastat.codsoc = scodsoc
     and bastat.articl = sarticl
     and bastat.motcle = "VTE"
/*   and bastat.typcom begins "c" */
     and lookup(bastat.typcom,"CDL,LAS,LAV,LIV,LDP,LFA,LIC,LSC,LIM") <> 0
     and bastat.datfac <= wdat2
     and bastat.datfac >= wdat1
         use-index article
         no-lock  ,
    each auxili where
         auxili.codsoc = auxili-soc
     and auxili.typaux = "tie"
     and auxili.codaux = bastat.codaux
         no-lock
         break
      BY bastat.codaux:
form auxili.adres[1] column-label "nom" colon 1 with width 132.
accumulate bastat.qte    (total by bastat.codaux).
accumulate bastat.fac-ht (total by bastat.codaux).

if last-of(bastat.codaux) then do :
   display stream s-edition
           auxili.adres[1]      /*  column-label "Nom"   */
           auxili.adres[5]          column-label "Ville"
           bastat.codaux            column-label "Client"
           accum total by bastat.codaux
           bastat.qte               column-label "Total!Qte!Client"     format ">>>,>>>,>>9.99-"
           accum total by bastat.codaux
           fac-ht                   column-label "Total!Mont HT!Client" format ">>>,>>>,>>9.99-".
   if seti = true then do:
      snbeti = snbeti + 1.
      put stream etiquette
/*          scodsoc " " auxili.typaux " " auxili.codaux " " today
          "      " */
          "                                "
          auxili.adres[1]
          auxili.adres[2]
          auxili.adres[3]
          auxili.adres[4]
          auxili.adres[5]
          skip.
   end.
end.
tot-art = tot-art + fac-ht.
tot-art-qte = tot-art-qte + bastat.qte.
end. /* end for each */
end. /* stri 1 */

if stri <> "1" then do: /* stri <> 1 */
for each bastat where
         bastat.codsoc = scodsoc
     and bastat.articl = sarticl
     and bastat.motcle = "VTE"
/*     and bastat.typcom begins "cd" */
     and lookup(bastat.typcom,"CDL,LAS,LAV,LIV,LDP,LFA,LIC,LSC,LIM") <> 0
     and bastat.datfac <= wdat2
     and bastat.datfac >= wdat1
         use-index article
         no-lock  ,
    each auxili where
         auxili.codsoc = auxili-soc
     and auxili.typaux = bastat.typaux
     and auxili.codaux = bastat.codaux
         no-lock  ,
    each auxapr where
         auxapr.codsoc = bastat.codsoc
     and auxapr.typaux = bastat.typaux
     and auxapr.codaux = bastat.codaux
         no-lock
         break
      by auxapr.magasin
      by bastat.codaux:
form auxapr.magasin column-label "Mag" colon 1 with width 132.

accumulate bastat.qte    (total by auxapr.magasin).
accumulate bastat.fac-ht (total by auxapr.magasin).

accumulate bastat.qte    (total by auxapr.magasin
                                by bastat.codaux).
accumulate bastat.fac-ht (total by auxapr.magasin
                                by bastat.codaux).
if last-of(bastat.codaux) then do :
   display stream s-edition
           auxapr.magasin       /*  column-label "Mag"   */
           auxili.adres[1]          column-label "Nom"
           auxili.adres[5]          column-label "Ville"
           bastat.codaux            column-label "Client"
           accum total by bastat.codaux
           bastat.qte               column-label "Total!Qte" format ">>>,>>>,>>9.99-"
           accum total by bastat.codaux
           bastat.fac-ht            column-label "Total!Mont HT" format ">>>,>>>,>>9.99-".
   if seti = true then do:
      snbeti = snbeti + 1.
      put stream etiquette
/*          scodsoc " " auxili.typaux " " auxili.codaux " " today
          " " auxapr.magasin "  " */
          "                                "
          auxili.adres[1]
          auxili.adres[2]
          auxili.adres[3]
          auxili.adres[4]
          auxili.adres[5]
          skip.
   end.
end.
if last-of(auxapr.magasin) then do :
   display stream s-edition
           auxapr.magasin               no-label
           "                                    "
           "total magasin                   "
           "          "
           accum total by auxapr.magasin
           bastat.qte                   no-label
           accum total by auxapr.magasin
           bastat.fac-ht                no-label.
end.
tot-art = tot-art + fac-ht.
tot-art-qte = tot-art-qte + bastat.qte.
end. /* end for each */
end. /* stri <> 1 */

display stream s-edition
        "TOTAL ARTICLE qte: "
        tot-art-qte          no-label
        skip
        "               ht: "
        tot-art              no-label
        with frame f-total-art.
display tot-art.
display tot-art-qte.

output stream s-edition close.
if seti = true then do :
   output stream etiquette close.
   if snbeti > 0 then snbetilib = "etiquettes".
   if snbeti = 1 then snbetilib = "etiquette".
   display snbeti snbetilib.
end.
message "lisbastd1.p liste ajoutee en" sedifile .
end. /* repeat */
end. /* do  saisie */
