/*==========================================================================*/
/*                        E X T - S T A T M A G 0 . I                       */
/* Creation table STARTI pour les statistiques sous Magasins                */
/*--------------------------------------------------------------------------*/
/* Variables et Frames                                                      */
/*==========================================================================*/

/*--------------------------------------------------------------------------*/
/*                         Definition des variables                         */
/*--------------------------------------------------------------------------*/

DEF VAR ind AS INT NO-UNDO.

DEF {1} SHARED VAR libelle    AS CHAR EXTENT 30           NO-UNDO.
DEF {1} SHARED VAR l-magasins AS CHAR FORMAT "x(45)"      NO-UNDO.
DEF {1} SHARED VAR date-debut AS DATE FORMAT "99/99/9999" NO-UNDO.
DEF {1} SHARED VAR date-fin   AS DATE FORMAT "99/99/9999" NO-UNDO.

/*--------------------------------------------------------------------------*/
/*                           Dessin des Frames                              */
/*--------------------------------------------------------------------------*/

DEF {1} SHARED FRAME fr-saisie.

FORM libelle[01] FORMAT "x(20)" l-magasins SKIP
     libelle[02] FORMAT "x(20)" date-debut SKIP
     libelle[03] FORMAT "x(20)" date-fin
     WITH FRAME fr-saisie ROW 5 CENTERED NO-LABEL OVERLAY { v6frame.i }.