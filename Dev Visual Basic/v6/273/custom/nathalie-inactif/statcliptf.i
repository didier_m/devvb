
def {1} shared var l-soc        as char                      no-undo.
def {1} shared var l-lib        as char                      no-undo.
def {1} shared var l-represko   as char                             no-undo.

def {1} shared var wdatemin     as date                      no-undo.
def {1} shared var wdatemax     as date                      no-undo.

def {1} shared var i            as int                              no-undo .
def {1} shared var j            as int                              no-undo .
def {1} shared var k            as int                              no-undo .

def {1} shared var z-choix      as char                             no-undo .
def {1} shared var zone         as char                             no-undo .
def {1} shared var libelle      as char                 extent 99   no-undo .

def {1} shared var wnomfic      as char                      no-undo .
def {1} shared var type-trt     as char                      no-undo .


def {1} shared frame fr-saisie .

form skip (1)
     "TYPE DE TRAITEMENT ===>: " type-trt  format "x(7)" skip(1)
     
     "Soci�t�s a Traiter ... : " l-soc     format "x(10)" skip
     "                         " l-lib     format "x(30)" skip (1)
     "Date debut periode ... : " wdatemin  format "99/99/9999" skip 
     "Date fin periode ..... : " wdatemax  format "99/99/9999" skip (1)

     "Fichier de sortie .... : " wnomfic   format "x(50)"      skip (1)

     with frame fr-saisie
row 3  centered  overlay  no-label  { v6frame.i } .