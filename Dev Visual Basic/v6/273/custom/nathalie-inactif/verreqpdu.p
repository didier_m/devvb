{ bat-ini.i  "new" }
def input parameter fichier as char .

def var wexercice as char.
def var wservice as char.

output to verreqpdu.02102009.dgr.
export delimiter ";"
    "codsoc" "typaux" "codaux" "adres[1]" "repres[1]"
    "motcle" "typcom" "numbon" "datfac" "datdep" "exercice"
    "articl" "libart" "famart" "soufam "
    "fac-ht" "wservice".
for each auxapr
where auxapr.codsoc = "01"
and   trim(auxapr.repres[1]) <> ""
no-lock,
  each bastat  /* use-index codaux  */
where bastat.codsoc = auxapr.codsoc
and   bastat.motcle = "vte"
and   bastat.datfac >= 07/01/2006
and   bastat.datfac <= 06/30/2009
and   bastat.typaux-fac = auxapr.typaux
and   bastat.codaux-fac = auxapr.codaux
no-lock,
  each artapr
where artapr.codsoc = auxapr.codsoc
and   artapr.articl = bastat.articl 
no-lock:
  if bastat.datfac >= 07/01/2006 and bastat.datfac <= 06/30/2007 then
    wexercice = "2006-2007".

  if bastat.datfac >= 07/01/2007 and bastat.datfac <= 06/30/2008 then
    wexercice = "2007-2008".
    
  if bastat.datfac >= 07/01/2008 and bastat.datfac <= 06/30/2009 then
        wexercice = "2008-2009".
  wservice = "".
  if artapr.famart = "080" and artapr.soufam = "008" then wservice = "oui".
  export delimiter ";"
    auxapr.codsoc auxapr.typaux auxapr.codaux
    auxapr.adres[1] auxapr.repres[1]
    bastat.motcle bastat.typcom bastat.numbon
    bastat.datfac bastat.datdep wexercice 
    artapr.articl artapr.libart artapr.famart artapr.soufam 
    bastat.fac-ht 
    wservice.

end.