 /* programme : liscliacti.p liste des clients selon leurs codes activit� */

{connect.i}

define stream edition.
def var hp      as character format "x(16)" initial "~033&l1O~033&k2S".
def var scodsoc as character format "x(2)" initial "01".
def var acodsoc as character format "x(2)" initial "08".
def var lib as character format "x(30)".
def var scodact as character format "x(3)".
def var wcptlig as integer.
def var wcptpag as integer format '>>>9'.
def var sequence as Char         format "x(5)".
def var sel-sequence as Char     format "x(12)".

/* output stream edition to liscliacti.bfa .
*/
wcptlig= 44.
scodsoc= codsoc-soc.

/* d�termination du N� de s�quence operateur */
assign codsoc = " "
       etabli = " "
       typtab = "ope"
       prefix = " nume-spool"
       codtab = operat.
run opeseq.p ("req", output sequence, output sel-sequence).

/* fin */
do with frame a: /* do saisie */
form
"act"   at  01 scodact     colon 05 no-label
with title "listcliacti.p liste de tous les clients d'une activite ".

update scodact
       validate (scodact <> " ","blanc refuse" ) auto-return.

/* view stream s-edition frame s.
*/
output stream edition to value(sel-sequence).

if scodsoc <> "08" then acodsoc = "01".

for each auxdet where auxdet.codsoc =scodsoc and
                      auxdet.motcle = "ACT"  and
                      auxdet.codtab = scodact
                      no-lock ,
          each auxili where auxili.codsoc = acodsoc
           and auxili.typaux = auxdet.typaux
           and auxili.codaux = auxdet.codaux
           no-lock by auxili.libabr by auxili.codaux.

find auxapr where auxapr.codsoc = scodsoc
              and auxapr.typaux = auxdet.typaux
              and auxapr.codaux = auxdet.codaux
              no-lock.
lib = "                              ".
if auxdet.calf10 <> " "
then  do :
        FIND PROGLIB where proglib.motcle = " "
                       and proglib.codsoc = acodsoc
                       and proglib.etabli = " "
                       and proglib.cle    = "AUX-" + scodact +
                                                    auxdet.typaux + auxdet.codaux
                       and proglib.edition = " "
                       and proglib.chrono = 0

                       no-lock no-wait no-error.
        if available proglib then lib = substr(proglib.libelle[1],1,30).
end.
if auxdet.calf10 = " "
then lib = " ".

wcptlig = wcptlig + 1.

if wcptlig > 44 then do :
        wcptlig = 5.
        wcptpag = wcptpag + 1.
        put stream edition hp.
        put stream edition
           "SOCIETE " SCODSOC
           "  Liste des clients dont le code activite est  " SCODACT
                      "  imprime le : " today
        skip.
        put stream edition
        "programme : liscliacti.p bfa                    "
        "                                                        page " wcptpag
        skip.
        put stream edition
            "CODAUX           NOM                                     ADRESSE "
            "                              TELEPHONE      TAR. BAREME"
            "              REP ECH REG TEXTE    C* COMMENTAIRE ACTIVITE"
        skip.
end.
put stream edition
        substring(auxapr.codaux,5,6)    format "x(6)" " "
        substring(auxili.adres[1],1,30) format "x(30)" " "
        substring(auxili.adres[3],1,25) format "x(25)" " "
        substring(auxili.adres[5],1,30) format "x(30)" " "
        substring(auxapr.teleph,1,14)   format "x(14)" " "
        auxapr.code-tarif " "
        auxapr.bareme[1]
        auxapr.bareme[2]
        auxapr.bareme[3]
        auxapr.bareme[4]
        auxapr.bareme[5]
        auxapr.bareme[6]
        auxapr.bareme[7]
        auxapr.bareme[8]
        auxapr.bareme[9]
        auxapr.bareme[10] " "
/*        auxapr.sect-geo " "  Enleve le 11/07/2000 par CCO
          auxapr.magasin " "  */
        auxapr.repres[1] " "
        auxapr.codech " "
        auxapr.modreg  " "
        substr(auxdet.CALF14,1,8) format "x(8)" " "
        substring(auxdet.CALF10,1,1) format "x(1)" " "
        lib
skip.
end.
end.