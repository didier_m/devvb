/*==========================================================================*/
/*                           E X T - C F I A N 0 . i                        */
/* Extraction des Fiches Cartes Fidelite Magasin ayant droit a remise       */
/*--------------------------------------------------------------------------*/
/* Variables et Frames                                        CC 12/09/2006 */
/*==========================================================================*/

DEF VAR ind        AS INT  no-undo .
DEF VAR tour-soc   AS INT  no-undo .
DEF VAR date-tarif AS DATE no-undo .

DEF {1} SHARED VAR libelle         AS CHAR EXTENT 40      no-undo .
DEF {1} SHARED VAR ean13-operation AS CHAR FORMAT "x(13)" no-undo .