/*=======================================================================*/
/*                      E X T - S T A T M A G D A T . I                  */
/*      Creation table STARTI pour les statistiques sous Magasins        */
/*-----------------------------------------------------------------------*/
/*      Gestion date de debut et de fin d'extraction                     */
/*=======================================================================*/
/* DGR 040107 : correction month - 1 si janvier                          */
/* DGR 010307 : correction J2 = 28 si on est en mars                     */

/*--------------------------------------------------------------------------*/
/*                         Definition des variables                         */
/*--------------------------------------------------------------------------*/

DEF VAR j2 AS INT.
DEF VAR j1 AS INT.
DEF VAR wannee AS INT.

/*--------------------------------------------------------------------------*/
/*                          Gestion date de debut                           */
/*--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------*/
/*                           Gestion date de fin                            */
/*--------------------------------------------------------------------------*/

/*--------------------------------------*/
/*    Gestion du mois de janvier -1     */
/*        qui entraine ann�e -1         */
/*--------------------------------------*/

wannee = year( today ).

IF month( today ) = 01
THEN DO :
    date-debut = date( 12, 01, year( today ) - 1 ).
    date-fin   = date( 12, 31, year( today ) - 1 ).   
END.
else do:
    date-debut = date( month( today ) - 1, 01, year( today ) ).

   /*--------------------------------------*/
   /* Gestion du mois de f�vrier en tenant */
   /*    compte des ann�es bissextiles     */
   /*--------------------------------------*/

    wannee = year( today ).

    IF month( today ) = 02
    OR month( today ) = 04
    OR month( today ) = 06
    OR month( today ) = 08
    OR month( today ) = 09
    OR month( today ) = 11
    THEN 
       j2 = 31.
    ELSE j2 = 30.    

    IF month( today ) = 03
    THEN
      IF wannee modulo 4 = 0
      THEN j2 = 29.
      ELSE    j2 = 28.
 

    date-fin = date( month( today ) - 1, j2, year( today ) ).
  end.