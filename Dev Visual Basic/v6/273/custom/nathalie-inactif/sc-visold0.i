/*---------------------------------------------------------------------------*/
/*                      sc-visold0.i                                         */
/*---------------------------------------------------------------------------*/

lect = "{2}" .
if lect <> ""  and  lect <> "999"
   then  do:
        FIND soldcli
        use-index chrono no-lock no-error .
   end.
   else  do:
        FIND {1} soldcli 
        use-index chrono no-lock no-error.
   end.