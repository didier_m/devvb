/* prog : lisstoar.p  */
/*   modif DGR 251104 pour V52                                                */
def var arti as char format "x(6)".
def var soc  as char format "xx".
output to  /applic/li/prs/hispamp.prs.
put "&l1O&k16H&k2S&l6D&k6H" skip.

repeat :
update soc  column-label "Societe".
update arti column-label "Article".

/*
for each stoarr where codsoc = soc
and magasi = ""
and articl = arti
no-lock:
display articl date-arrete
pamp datmaj opemaj opecre datcre .
*/
for each stocks
where codsoc = soc
and magasi = ""
and evenement = "PXACHA"
and articl = arti
no-lock:
display exercice column-label "ANNEE"
        prix[1] column-label "JANVIER"
        prix[2] column-label "FEVRIER"
        prix[3] column-label "MARS"
        prix[4] column-label "AVRIL"
        prix[5] column-label "MAI"
        prix[6] column-label "JUIN"
        prix[7] column-label "JUILLET"
        prix[8] column-label "AOUT"
        prix[9] column-label "SEPTEMBRE"
        prix[10] column-label "OCTOBRE"
        prix[11] column-label "NOVEMBRE"
        prix[12] column-label "DECEMBRE"
        datmaj opemaj opecre datcre with width 300.

end .
end.
