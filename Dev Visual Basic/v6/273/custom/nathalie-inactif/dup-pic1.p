/* ------------------------------------------------------------------------- */
/* duplication des creations / modifications articles / tarifs / gencods     */
/* de la societe 01 sur la societe 43                                        */
/* ------------------------------------------------------------------------- */
/* dup-pic1.p : programme de duplication                                     */
/* VLE 300806 : Duplication de 01 en 43 sauf les tarifs qui sont             */
/*              dupliqu�s � partir de la soci�t� 05                          */ 
/* VLE 061106 : ajout gestion date de dernier traitement                     */
/* DGR 100108 : PICQ DUPLIQUE A PARTIR DE LA SOC 01                          */
/* ------------------------------------------------------------------------- */
/*-------------------------------------------------- */
/*  Acces parametre lecture societe article ==> OK   */
/*-------------------------------------------------- */

{connect.i}
{ bat-ini.i  new }

def var periph           as char        format "x(15)"       no-undo.
def var wdate            as date        format "99/99/99"    no-undo.
def var nb-lus           as int                              no-undo.
def var nb-artaprlus     as int                              no-undo.
def var nb-artaprcre     as int                              no-undo.
def var nb-artaprmaj     as int                              no-undo.
def var nb-artbisvtecre  as int                              no-undo.
def var nb-artbisvtemaj  as int                              no-undo.
def var nb-artbisachcre  as int                              no-undo.
def var nb-artbisachmaj  as int                              no-undo.
def var nb-artbisacucre  as int                              no-undo.
def var nb-artbisacumaj  as int                              no-undo.
def var nb-artbisacecre  as int                              no-undo.
def var nb-artbisacemaj  as int                              no-undo.
def var nb-tabgesvtecre  as int                              no-undo.
def var nb-tabgesvtemaj  as int                              no-undo.
def var nb-tabgesachcre  as int                              no-undo.
def var nb-tabgesachmaj  as int                              no-undo.
def var nb-tarclivtecre  as int                              no-undo.
def var nb-tarclivtemaj  as int                              no-undo.
def var nb-tarcliachcre  as int                              no-undo.
def var nb-tarcliachmaj  as int                              no-undo.
def var nb-tarclirpdcre  as int                              no-undo.
def var nb-tarclirpdmaj  as int                              no-undo.
def var nb-codbarcre     as int                              no-undo.
def var nb-codbarmaj     as int                              no-undo.
def var wdatmaj       like artapr.datmaj                  no-undo.
def var wheumaj       like artapr.heumaj                  no-undo.
def var wopemaj       like artapr.opemaj                  no-undo.
def var nb-opecre        as int                              no-undo.
def var nb-opemaj        as int                              no-undo.
def var heure-debut      as int .
def var wcodetar         as char                             no-undo.

def buffer b-artapr for artapr.
def buffer b-artbis for artbis.
def buffer b-tabges for tabges.
def buffer b-tarcli for tarcli.
def buffer b-codbar for codbar.
def buffer date-tabges for tabges.

def stream sortie .

def input parameter  fichier  as char format "x(12)".

run bat-lec.p ( fichier ) .

{ bat-cha.i  "wdate"      "wdate"      "d" }
{ bat-cha.i  "periph"     "periph"     "a" }

output stream sortie to value(periph).

/*-----------------------------------------------------*/
/* Recup. date traitement avec lock enreg. pendant trt */
/*-----------------------------------------------------*/
/*   { dup-picdat.i "exclusive-lock" } */

assign typtab = "PRM"
       prefix = "DUP-PIC"
       codtab = "DERN-TRAIT".

FIND DATE-TABGES where date-tabges.codsoc = ""
and   date-tabges.etabli = ""
and   date-tabges.typtab = typtab
and   date-tabges.prefix = prefix
and   date-tabges.codtab = codtab
exclusive-lock no-error.

  heure-debut = time.
  wdatmaj = today.
  wheumaj = string(time,"hh:mm").
  wopemaj = "PI1".

  /* tour ARTAPR */

  for each artapr use-index date-maj
  where artapr.codsoc = "01"
  and   artapr.datmaj >= wdate
  no-lock:
    nb-artaprlus = nb-artaprlus + 1.
    find b-artapr
    where b-artapr.codsoc = "43"
    and   b-artapr.articl = artapr.articl
    exclusive-lock no-error.
    if not available b-artapr then do:
      nb-artaprcre = nb-artaprcre + 1.
      {dup-picc.i "artapr" }
      assign b-artapr.divers = "1".
    end.
    else do:
      nb-artaprmaj = nb-artaprmaj + 1.
      {dup-picm.i "artapr" }
      assign b-artapr.divers = "1".
    end.

    /* tour ARTBIS VTE */
    find artbis
    where artbis.codsoc = artapr.codsoc
    and   artbis.motcle = "VTE"
    and   artbis.typaux = ""
    and   artbis.codaux = ""
    and   artbis.articl = artapr.articl
    no-lock no-error.
    if available artbis then do:
      find b-artbis
      where b-artbis.codsoc = "43"
      and   b-artbis.motcle = "VTE"
      and   b-artbis.typaux = ""
      and   b-artbis.codaux = ""
      and   b-artbis.articl = artapr.articl
      exclusive-lock no-error.
      if not available artbis then do :
        nb-artbisvtecre = nb-artbisvtecre + 1.
        {dup-picc.i "artbis" }
      end.
      else do:
        nb-artbisvtemaj = nb-artbisvtemaj + 1.
        {dup-picm.i "artbis" }
      end.
    end.
    find tabges
    where tabges.codsoc = artapr.codsoc
    and   tabges.etabli = ""
    and   tabges.typtab = "ART"
    and   tabges.prefix = "SUIT-FICHE"
    and   tabges.codtab = artapr.articl
    no-lock no-error.
    if available tabges then do:
      find b-tabges
      where b-tabges.codsoc = "43"
      and   b-tabges.etabli = ""
      and   b-tabges.typtab = "ART"
      and   b-tabges.prefix = "SUIT-FICHE"
      and   b-tabges.codtab = artapr.articl
      exclusive-lock no-error.
      if not available b-tabges then do:
        {dup-picc.i "tabges" }
        nb-tabgesvtecre = nb-tabgesvtecre + 1.
      end.
      else do:
        {dup-picm.i "tabges" }
        nb-tabgesvtemaj = nb-tabgesvtemaj + 1.
      end.
    end.

    /* fiche fournisseur LISADIS */
    find b-artbis
    where b-artbis.codsoc = "43"
    and   b-artbis.motcle = "ACH"
    and   b-artbis.typaux = "tie"
    and   b-artbis.codaux = "    802190"
    and   b-artbis.articl = artapr.articl
    exclusive-lock no-error.
    if not available b-artbis then do :
      find artbis
      where artbis.codsoc = "01"
      and   artbis.motcle = "ACH"
      and   artbis.typaux = "tie"
      and   artbis.codaux = "    802190"
      and   artbis.articl = artapr.articl
      no-lock no-error.
      if available artbis then do:
        nb-artbisachcre = nb-artbisachcre + 1.
        {dup-picc.i "artbis" }

        find tabges
        where tabges.codsoc = artbis.codsoc
        and   tabges.etabli = "ACH"
        and   tabges.typtab = "ART"
        and   tabges.prefix = "SUIT-FICHE"
        and   tabges.codtab = artbis.typaux + artbis.codaux + artbis.articl
        no-lock no-error.
        if available tabges then do:
          find b-tabges
          where b-tabges.codsoc = "43"
          and   b-tabges.etabli = "ACH"
          and   b-tabges.typtab = "ART"
          and   b-tabges.prefix = "SUIT-FICHE"
          and   b-tabges.codtab = artbis.typaux + artbis.codaux + artbis.articl
          exclusive-lock no-error.
          if not available b-tabges then do:
            {dup-picc.i "tabges" }
            nb-tabgesachcre = nb-tabgesachcre + 1.
          end.
        end.
      end.
    end.

    find b-artbis
    where b-artbis.codsoc = "43"
    and   b-artbis.motcle = "ACH"
    and   b-artbis.typaux = "tie"
    and   b-artbis.codaux = "    804500"
    and   b-artbis.articl = artapr.articl
    exclusive-lock no-error.
    if not available b-artbis then do:
      /* fiche fournisseur EURENA */
      find artbis
      where artbis.codsoc = "01"
      and   artbis.motcle = "ACH"
      and   artbis.typaux = "tie"
      and   artbis.codaux = "    804500"
      and   artbis.articl = artapr.articl
      no-lock no-error.
      if available artbis then do :
        nb-artbisacucre = nb-artbisacucre + 1.
        {dup-picc.i "artbis" }
      end.
    end.

    /* fiche fournisseur ED */
    find b-artbis
    where b-artbis.codsoc = "43"
    and   b-artbis.motcle = "ACH"
    and   b-artbis.typaux = "tie"
    and   b-artbis.codaux = "    999100"
    and   b-artbis.articl = artapr.articl
    exclusive-lock no-error.
    if not available b-artbis then do:
      /* fiche fournisseur EURENA */
      find artbis
      where artbis.codsoc = "01"
      and   artbis.motcle = "ACH"
      and   artbis.typaux = "tie"
      and   artbis.codaux = "    999100"
      and   artbis.articl = artapr.articl
      no-lock no-error.
      if available artbis then do :
        nb-artbisacecre = nb-artbisacecre + 1.
        {dup-picc.i "artbis" }
      end.
    end.
  end.

  /* fiche fournisseur LISADIS */
  for each artbis
  where artbis.codsoc = "01"
  and   artbis.motcle = "ACH"
  and   artbis.typaux = "tie"
  and   artbis.codaux = "    802190"
  and   artbis.datmaj >= wdate
  no-lock :
    find b-artbis
    where b-artbis.codsoc = "43"
    and   b-artbis.motcle = "ACH"
    and   b-artbis.typaux = "tie"
    and   b-artbis.codaux = artbis.codaux
    and   b-artbis.articl = artbis.articl
    exclusive-lock no-error.
    if not available b-artbis then do :
      nb-artbisachcre = nb-artbisachcre + 1.
      {dup-picc.i "artbis" }
    end.
    else do:
      nb-artbisachmaj = nb-artbisachmaj + 1.
      {dup-picm.i "artbis" }
    end.

    find tabges
    where tabges.codsoc = artbis.codsoc
    and   tabges.etabli = "ACH"
    and   tabges.typtab = "ART"
    and   tabges.prefix = "SUIT-FICHE"
    and   tabges.codtab = artbis.typaux + artbis.codaux + artbis.articl
    no-lock no-error.
    if available tabges then do:
      find b-tabges
      where b-tabges.codsoc = "43"
      and   b-tabges.etabli = "ACH"
      and   b-tabges.typtab = "ART"
      and   b-tabges.prefix = "SUIT-FICHE"
      and   b-tabges.codtab = artbis.typaux + artbis.codaux + artbis.articl
      exclusive-lock no-error.
      if not available b-tabges then do:
        {dup-picc.i "tabges" }
        nb-tabgesachcre = nb-tabgesachcre + 1.
      end.
      else do:
        {dup-picm.i "tabges" }
        nb-tabgesachmaj = nb-tabgesachmaj + 1.
      end.
    end.
  end.

  /* fiche fournisseur EURENA */
  for each artbis
  where artbis.codsoc = "01"
  and   artbis.motcle = "ACH"
  and   artbis.typaux = "tie"
  and   artbis.codaux = "    804500"
  and   artbis.datmaj >= wdate
  no-lock :
    find b-artbis
    where b-artbis.codsoc = "43"
    and   b-artbis.motcle = "ACH"
    and   b-artbis.typaux = "tie"
    and   b-artbis.codaux = artbis.codaux
    and   b-artbis.articl = artbis.articl
    exclusive-lock no-error.
    if not available b-artbis then do :
      nb-artbisacucre = nb-artbisacucre + 1.
      {dup-picc.i "artbis" }
    end.
    else do:
      nb-artbisacumaj = nb-artbisacumaj + 1.
      {dup-picm.i "artbis" }
    end.
  end.

  /* fiche fournisseur EUREA DISTRI */
  for each artbis
  where artbis.codsoc = "01"
  and   artbis.motcle = "ACH"
  and   artbis.typaux = "tie"
  and   artbis.codaux = "    999100"
  and   artbis.datmaj >= wdate
  no-lock :
    find b-artbis
    where b-artbis.codsoc = "43"
    and   b-artbis.motcle = "ACH"
    and   b-artbis.typaux = "tie"
    and   b-artbis.codaux = artbis.codaux
    and   b-artbis.articl = artbis.articl
    exclusive-lock no-error.
    if not available b-artbis then do :
      nb-artbisacecre = nb-artbisacecre + 1.
      {dup-picc.i "artbis" }
    end.
    else do:
      nb-artbisacemaj = nb-artbisacemaj + 1.
      {dup-picm.i "artbis" }
    end.
  end.

  for each codbar use-index datmaj
  where codbar.codsoc = "01"
  and   codbar.datmaj >= wdate
  no-lock :
    find b-codbar
    where b-codbar.codsoc = "43"
    and   b-codbar.code-barre = codbar.code-barre
    exclusive-lock no-error.
    if not available codbar then do:
      {dup-picc.i "codbar" }
      nb-codbarcre = nb-codbarcre + 1.
    end.
    else do:
      {dup-picm.i "codbar" }
      nb-codbarmaj = nb-codbarmaj + 1.
    end.
  end.

  for each tarcli use-index k-datmaj
  where tarcli.codsoc = "01"
  and   tarcli.motcle = "VTE"
  and   tarcli.datmaj >= wdate
  and   tarcli.typaux = ""
  and   tarcli.codaux = ""
  and   tarcli.opecre <> "DUP"
  no-lock:
    wcodetar = tarcli.code-tarif.
    if wcodetar = "HT5" then wcodetar = "HTC".

    find b-tarcli
    where b-tarcli.codsoc = "43"
    and   b-tarcli.motcle = "VTE"
    and   b-tarcli.typaux = ""
    and   b-tarcli.codaux = ""
    and   b-tarcli.opecre <> "DUP"
    and   b-tarcli.articl = tarcli.articl
    and   b-tarcli.code-tarif = wcodetar
    and   b-tarcli.date-debut = tarcli.date-debut
/*
    and   b-tarcli.chrono = tarcli.chrono
*/
    exclusive-lock no-error.
    if not available b-tarcli then do:
      create b-tarcli.
      buffer-copy tarcli
        except tarcli.codsoc tarcli.code-tarif
               tarcli.datcre   tarcli.heucre  tarcli.opecre
               tarcli.datmaj  tarcli.heumaj  tarcli.opemaj
      to b-tarcli
        assign b-tarcli.codsoc = "43"
               b-tarcli.code-tarif = wcodetar
               b-tarcli.datcre = wdatmaj  b-tarcli.heucre = wheumaj
               b-tarcli.opecre = wopemaj 
               b-tarcli.datmaj = wdatmaj  b-tarcli.heumaj = wheumaj
               b-tarcli.opemaj = wopemaj.
      release b-tarcli.
      nb-tarclivtecre = nb-tarclivtecre + 1.

    end.
    else do:
      if tarcli.code-tarif = "HTC" then next.
      assign b-tarcli.codsoc = "43"
             b-tarcli.code-tarif = wcodetar
             b-tarcli.datmaj = wdatmaj  b-tarcli.heumaj = wheumaj
             b-tarcli.opemaj = wopemaj.
      release b-tarcli.

      nb-tarclivtemaj = nb-tarclivtemaj + 1.
    end.
  end.

  for each tarcli use-index k-datmaj
  where tarcli.codsoc = "01"
  and   tarcli.motcle = "ACH"
  and   tarcli.datmaj >= wdate
  and   tarcli.typaux = "TIE"
  and   tarcli.codaux = "    802190"
  no-lock:
    find b-tarcli
    where b-tarcli.codsoc = "43"
    and   b-tarcli.motcle = "ACH"
    and   b-tarcli.typaux = tarcli.typaux
    and   b-tarcli.codaux = tarcli.codaux
    and   b-tarcli.articl = tarcli.articl
    and   b-tarcli.code-tarif = tarcli.code-tarif
    and   b-tarcli.date-debut = tarcli.date-debut
    and   b-tarcli.chrono = tarcli.chrono
    exclusive-lock no-error.
    if not available b-tarcli then do:
      {dup-picc.i "tarcli" }
      nb-tarcliachcre = nb-tarcliachcre + 1.
    end.
    else do:
      {dup-picm.i "tarcli" }
      nb-tarcliachmaj = nb-tarcliachmaj + 1.
    end.
  end.

/*-----------------------------------------------------------*/
/*    Duplication de TARCLI RPD                              */
/*-----------------------------------------------------------*/

  for each tarcli use-index k-datmaj
  where tarcli.codsoc = "01"
  and   tarcli.motcle = "VTE"
  and   tarcli.datmaj >= wdate
  and   tarcli.typaux = "TAX"
  and   tarcli.codaux = ""
  and   tarcli.code-tarif = "RPD"
  no-lock:
    find b-tarcli
    where b-tarcli.codsoc = "43"
    and   b-tarcli.motcle = tarcli.motcle
    and   b-tarcli.typaux = tarcli.typaux
    and   b-tarcli.codaux = tarcli.codaux
    and   b-tarcli.articl = tarcli.articl
    and   b-tarcli.code-tarif = tarcli.code-tarif
    and   b-tarcli.date-debut = tarcli.date-debut
    and   b-tarcli.chrono = tarcli.chrono
    exclusive-lock no-error.
    if not available b-tarcli then do:
      {dup-picc.i "tarcli" }
      nb-tarclirpdcre = nb-tarclirpdcre + 1.
    end.
    else do:
      {dup-picm.i "tarcli" }
      nb-tarclirpdmaj = nb-tarclirpdmaj + 1.
    end.
  end.

  for each tabges use-index date-maj
  where tabges.codsoc = "01"
  and   tabges.etabli = ""
  and   tabges.typtab = "SOP"
  and   tabges.prefix = "TARECART"
  and   tabges.datmaj >= today
  no-lock:
    find b-tabges
    where b-tabges.codsoc = "43"
    and   b-tabges.etabli = ""
    and   b-tabges.typtab = "SOP"
    and   b-tabges.prefix = "TARECART"
    and   b-tabges.codtab = tabges.codtab
    exclusive-lock no-error.
    if not available b-tabges then do:
      {dup-picc.i "tabges" }
      nb-opecre = nb-opecre + 1.
    end.
    else do:
      {dup-picm.i "tabges" }
      nb-opemaj = nb-opemaj + 1.
    end.
  end.

/*---------------------*/
/* MAJ date extraction */
/*---------------------*/

/* La date debut est egale a date stockee + 1 a chaque lancement */
                                                                                     

    
if heure-debut < 64800                   /* Deb. av. 18 h - Fin meme j  */
   then do :
   date-tabges.dattab[1] = today - 1.   /* Pour repartir meme jour     */
   { majmoucb.i date-tabges }
end.
else                                    
   if heure-debut >= 64800 and time < 64800 /* Deb. ap. 18 h - Fin a j + 1 */
      then do :
      date-tabges.dattab[1] = today - 1.   /* Pour repartir de j + 1      */
      { majmoucb.i date-tabges }
   end.
   else 
   if heure-debut >= 64800 and time < 86400 /* Deb. ap. 18 h - Fin meme j  */
      then do :
      date-tabges.dattab[1] = today.       /* Pour repartir de j + 1      */
      { majmoucb.i date-tabges }
   end.

  disp "                       Nb CRE     Nb MAJ" no-label skip
       "                       ------     ------" no-label skip
       "ARTAPR ........ : " nb-artaprcre    no-label nb-artaprmaj    no-label skip
       "CODBAR ........ : " nb-codbarcre    no-label nb-codbarmaj    no-label skip
       "ARTBIS VTE .... : " nb-artbisvtecre no-label nb-artbisvtemaj no-label skip
       "TABGES VTE .... : " nb-tabgesvtecre no-label nb-tabgesvtemaj no-label skip
       "TARCLI VTE .... : " nb-tarclivtecre no-label nb-tarclivtemaj no-label skip
       "ARTBIS ACH lisa : " nb-artbisachcre no-label nb-artbisachmaj no-label skip
       "TABGES ACH .... : " nb-tabgesachcre no-label nb-tabgesachmaj no-label skip
       "TARCLI ACH .... : " nb-tarcliachcre no-label nb-tarcliachmaj no-label skip
       "TARCLI RPD .... : " nb-tarclirpdcre no-label nb-tarclirpdmaj no-label skip
       "ARTBIS ACH usi. : " nb-artbisacucre no-label nb-artbisacumaj no-label skip
       "ARTBIS ACH ED.. : " nb-artbisacecre no-label nb-artbisacemaj no-label skip
       "OPERATION       : " nb-opecre       no-label nb-opemaj       no-label skip
.
  pause.

