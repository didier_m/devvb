/*      lanceur pour le traitement de tva du 1er juillet 2014
        ce traitement creation des nouveaux prix des produits 
		changeant de taux de tva */
{ connect.i       }
{ bat-ini.i "new" }

def var topmaj   as log.
def var valid as log.
def var l-soc as char format "x(20)".

l-soc = "01,08,59".
topmaj = no.
valid  = no.
update l-soc topmaj valid with frame toto  centered
   title "maj prix suite chgt tva 010714  famille 006 et 007".

if valid = no then leave.
else do:
  batch-tenu = yes. /* Traitement en Batch */
  { bat-zon.i  "topmaj"         "topmaj"         "l" "''" "'TOPMAJ'" }
  { bat-zon.i  "l-soc"          "l-soc"          "l" "''" "'L-SOC'" }
  { bat-maj.i  "tva2014_D.p" }
  leave.
  end.
        
hide frame fr-titre no-pause.
hide frame fr-cadre no-pause.
