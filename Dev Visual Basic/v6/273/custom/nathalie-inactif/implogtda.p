/* Programme : implogrec.p  */

def stream file0.
def stream sortie.
def var wficlu   as char format "x(600)". 
def var wnomlog  as char format "x(30)".
def var wrep     as char initial "/applic/li/travail/".
def var wcodtab  as char.
def var wprefix  as char.
def var wsoc     as char.

message "programme implogrec.p". 
message "valable pour cedrecep ou cedliv". 
pause.

wnomlog = "cedrecep.log20140709-085028".

update wnomlog.

wnomlog = wrep + wnomlog.

message wnomlog.
pause.

output stream sortie to implogrec.txt.

/*
Creation entete RCF : 45 - ACH - RCF - 0004780000
Creation entete : 45 - VTE - LIV - 0016675700
*/

input  stream file0 from value(wnomlog) .

repeat :
import stream file0 unformatted wficlu .

if substr(wficlu, 1,15) <> "Creation entete" then next.

wcodtab = "".

if substr(wficlu, 28,3) = "ACH" then do:
   substr(wcodtab,1,3)  = substr(wficlu, 28,3). 
   substr(wcodtab,4,3)  = substr(wficlu, 34,3).
   substr(wcodtab,7,10) = substr(wficlu, 40,10).
   wsoc                 = substr(wficlu, 23,2).
   wprefix = "RECEP".
end.
else do:
   substr(wcodtab,1,3)  = substr(wficlu, 24,3). 
   substr(wcodtab,4,3)  = substr(wficlu, 30,3).
   substr(wcodtab,7,10) = substr(wficlu, 36,10).
   wsoc                 = substr(wficlu, 19,2).
   wprefix = "LIVRA".
end.

find tabges where
     tabges.codsoc = wsoc
 and tabges.etabli = "N"
 and tabges.typtab = "EUR"
 and tabges.prefix = wprefix
 and tabges.codtab = wcodtab 
exclusive-lock no-error.
if not available tabges then do:
   create tabges.
   assign 
     tabges.codsoc = wsoc
     tabges.etabli = "N"
     tabges.typtab = "EUR"
     tabges.prefix = wprefix
     tabges.codtab = wcodtab
     tabges.datcre = today. 
     
   export stream sortie tabges.

end.                        


end.
