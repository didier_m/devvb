 /* programme : lisclial2.p liste des clients avec un contrat / d�pot  */

 {connect.i}

define stream edition.
def var valid             as log format "o/n"             no-undo.
def var hp      as character format "x(16)" initial "~033&l1O~033&k2S".
def var scodsoc as character format "x(2)". /* initial "01".*/
def var acodsoc as character format "x(2)". /* initial "08".*/
def var lib as character format "x(30)".
def var sdep as character format "x(3)".
def var scont as character format "x(1)".
def var wcptlig as integer.
def var i as integer.
def var wcptpag as integer format '>>>9'.
def var sequence as Char         format "x(5)".
def var wdat1 like bastat.datfac.
def var wdat2 like bastat.datfac.
def var sdat1 like bastat.datfac.
def var sdat2 like bastat.datfac.
def var sel-sequence as Char     format "x(12)".
def var sact  as char format "xxx".
def var tope  as char.
def var wech  as char format "xxx".
def var wreg  as char format "xxx".
def var wact  as char format "xxx".

wcptlig = 44.

sdat1 = date(07,01,1996).
sdat2 = date(06,30,1997).

/* d�termination du N� de s�quence operateur */
 assign codsoc = " "
       etabli = " "
       typtab = "ope"
       prefix = " nume-spool"
       codtab = operat.
run opeseq.p ("req", output sequence, output sel-sequence).
/* fin */

scodsoc = codsoc-soc.
acodsoc = codsoc-soc.

do with frame a: /* do saisie */
repeat with frame a:
form
"D�pot"         at 01 sdep    colon 12 no-label

"Contrat "      at 01 scont   colon 12 no-label

"activite "     at 01 sact   colon 12 no-label

"Spool    "     at 01 sel-sequence  colon 12 no-label

"validation"    at 01 valid  colon 12 no-label

with  title "lisclial2.p liste des clients par d�pot avec un contrat ".

update sdep
       help " tapez un code d�pot (3 car) ou blanc pour tous les d�pots  ".
update scont
       help " tapez la lettre du contrat  "
        validate (scont <> " ", "blanc refus�").
update sact
       help " tapez un code activite (3 car) si n�cessaire".
display sel-sequence.

update valid
  validate( valid = "o" or valid = "n",
              "Valeurs Possibles : o ou n").
                
if valid then leave.

end.
end.


output stream edition to value(sel-sequence).

for each auxapr where auxapr.codsoc =scodsoc and
                      auxapr.typaux = "TIE"
                      no-lock :
if sdep <> " " then do :
        if auxapr.magasin <> sdep then next.
end.

if scodsoc <> "08" then acodsoc = "01".

find auxili where 
     auxili.codsoc = acodsoc
     and auxili.typaux = auxapr.typaux
     and auxili.codaux = auxapr.codaux
     no-lock no-error.

if not available auxili then next.

tope = "".

do i = 1 to 10 :
if substr(auxapr.bareme[i],1,1) = scont then tope = "O".

/*            if substr(bareme[i],2,1) <> "0" then do : */

end.

if tope = "" then next.

wact = "".
wech = "".
wreg = "".

if sact <> "" then do:

   find auxdet where 
     auxdet.codsoc = auxapr.codsoc and
     auxdet.motcle = "ACT"         and
     auxdet.codtab = sact          and
     auxdet.typaux = auxapr.typaux and
     auxdet.codaux = auxapr.codaux   
     no-lock no-error.
   if available auxdet then 
      do:
      wact = auxdet.codtab. wech = auxdet.codech. wreg = auxdet.modreg. 
      end.
   else next.
end.

wcptlig = wcptlig + 1.

if wcptlig > 44 then do :
        wcptlig = 5.
        wcptpag = wcptpag + 1.
        put stream edition hp.
        put stream edition
           "SOCIETE " SCODSOC
           "  Liste des clients avec le contrat " scont " pour le depot "  Sdep
                      "  imprime le : " today
        skip.
        put stream edition
        "programme : lisclial2.p bfa   ==> pour la selection activite : " sact
        "                                                    "
        "                                           "
        "   page " wcptpag "        ACTIVITE"
        skip.
        put stream edition
            "CODAUX           NOM                                  ADRESSE "
            "                                                                     TELEPHONE      TAR. BAREME"
            "            REP ECH REG ECH REG"
                    skip.
end.
put stream edition
        substring(auxapr.codaux,5,6)    format "x(6)" " "
        substring(auxili.adres[1],1,30) format "x(30)" " "
        substring(auxili.adres[2],1,20) format "x(20)" " "
        substring(auxili.adres[3],1,22) format "x(22)" " "
        substring(auxili.adres[4],1,20) format "x(17)" " "
        substring(auxili.adres[5],1,30) format "x(30)" " "
        substring(auxapr.teleph,1,14)   format "x(14)" " "
        auxapr.code-tarif " "
        auxapr.bareme[1]
        auxapr.bareme[2]
        auxapr.bareme[3]
        auxapr.bareme[4]
        auxapr.bareme[5]
        auxapr.bareme[6]
        auxapr.bareme[7]
        auxapr.bareme[8]       
        auxapr.bareme[9]     
/*        auxapr.bareme[10] " " */
/*        auxapr.sect-geo " " */
        " " auxapr.repres[1] " "
        auxapr.codech " "
        auxapr.modreg  " "
        wech " " wreg
   skip.
end.

