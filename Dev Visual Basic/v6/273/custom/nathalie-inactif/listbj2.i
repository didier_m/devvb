/* DGR 05122008 : Modif pour nomenclature 2                                   */
/* DGR 23012009 : Modif pour JARDIVERT "951" => St CHAMOND "016"              */
      
/* detail magasin */
find  t-bastat
where t-bastat.magasin = wmagasin
and   t-bastat.fam2 = artapr.famart2
exclusive-lock no-error.

if not available t-bastat then do:
  create t-bastat.
  assign t-bastat.societe = wsoc
         t-bastat.magasin = wmagasin
         t-bastat.fam2 = artapr.famart2.

  if trim(artapr.famart2 ) = "" then 
    t-bastat.groupe = "0".
  else 
    if substr(artapr.famart2,1,1) >= "0" and
       substr(artapr.famart2,1,1) <= "9" then
      t-bastat.groupe = "0".
    else 
      t-bastat.groupe = "Z".
             
end.

/* nombre de passages en caisse */
find  x-bastat
where x-bastat.magasin = wmagasin
and   x-bastat.fam2 = "9ZZ"
exclusive-lock no-error.

if not available x-bastat then do:

  create x-bastat.
  assign x-bastat.societe = wsoc
         x-bastat.magasin = wmagasin
         x-bastat.fam2 = "9ZZ"
         x-bastat.libfam = "NOMBRE DE PASSAGES EN CAISSE"
         x-bastat.groupe = "9".
end.
