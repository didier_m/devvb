/*----------------------------------------------------------------------------*/
/*                           PROJET ACTION VENTES                             */
/*                                                                            */
/*  include : actv-edi.i : contient la serie de tests permettant en fonction  */
/* =========               du commercial de rediriger vers le programme       */
/*                         d'edition permettant d'ecrire dans le bon fichier  */
/*                                                                            */
/* DGR 07-03-01                                                               */
/* DGR 29-07-04 : plus de 15 atc => rajout actv-ed4.p                         */
/*----------------------------------------------------------------------------*/

/* com = code commercial, fichier = fichier rattach� au com, chaine = ligne   */
/* a ecrir dans fichier du com                                                */

/* 5 premiers trait�s par actv-ed1.p */

IF com = "01" OR com = "02" OR com = "03" OR
   com = "04" OR com = "05 "
THEN DO:

  RUN actv-ed1.p (INPUT com, INPUT fichier, INPUT chaine).
END .

/* 5 suivants trait�s par actv-ed2.p */
IF com = "06" OR com = "07" OR com = "08" OR com = "09" OR
   com = "10"
THEN DO:
  RUN actv-ed2.p (INPUT com, INPUT fichier, INPUT chaine).
END.

/* 5 suivants trait�s actv-ed3.p */
IF com = "25" or com = "12" or com = "13" or com = "14" or
   com = "21"
THEN DO:
  RUN actv-ed3.p (INPUT com, INPUT fichier, INPUT chaine).
END.

/* 5 suivants trait�s actv-ed4.p */
IF com = "16" or com = "17" or com = "18" or com = "19" or com = "20"
THEN DO:
  RUN actv-ed4.p (INPUT com, INPUT fichier, INPUT chaine).
END.

/* 5 suivants trait�s actv-ed5.p */
IF com = "99" or com = "22" or com = "23" THEN DO:
  RUN actv-ed5.p (INPUT com, INPUT fichier, INPUT chaine).
END.
