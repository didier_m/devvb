/*==========================================================================*/
/*                           E X T - A G I L G E N  . I                     */
/* Extraction des donnees pour AGIL                                         */
/*--------------------------------------------------------------------------*/
/* Extraction codes barre                                                   */
/*==========================================================================*/

/*-------------------------------------------*/                           
/*  Ecriture du d�tail de section Code Barre */
/*-------------------------------------------*/ 

ID_ARTICLE = caps(trim(codbar.articl)).           
CD_BARRE = codbar.code-barre.

put stream agil unformatted
                      Type_Ligne_Det ID_ARTICLE "~011" CD_BARRE "~015" skip. 
