/* Programme : ncodfam3.p */
/*-------------------------------------------------------------*/
/* creation du referencier tarif des articles recodifies       */
/* � partir de la table de correspondance entre ancien et      */
/* nouveau code duplication des enregistrements Tarcli ACH et  */               
/* VTE et de Remcli ACH et VTE                                 */
/* Boucle sur les deux soci�t�s 01 et 08 ( l-soc-trait)        */
/* Le referencier CBA est dupliqu� en Agricel, Picq            */ 
/* et Jardivert(l-soc-dup)                                     */ 
/* Mise � jour de la date de traitement dans la table de       */
/*    correspondance                                           */ 
/*    - libel3[2] Referencier Tarif  associe � dattab[2]       */
/*-------------------------------------------------------------*/            
{ connect.i }
{ bat-ini.i  "new" }


def buffer b-tarcli for tarcli.
def buffer b-remcli for remcli.

def var pfamart          as char format "x(3)".
def var psoufam-min      as char format "x(3)".
def var psoufam-max      as char format "x(3)".
def var trt-test         as log.

def input parameter zfichier as char format "x(12)" .

run bat-lec.p ( zfichier ) .

{ bat-cha.i "trt-test"     "trt-test"           "l" }
{ bat-cha.i "pfamart"      "pfamart"            "a" }
{ bat-cha.i "psoufam-min"  "psoufam-min"        "a" }
{ bat-cha.i "psoufam-max"  "psoufam-max"        "a" }

def var ind              as integer.
def var i                as integer.
def var j                as integer.
def var k                as integer.
def var codsoc-trait     as char.
def var codsoc-dup       as char.
def var wmotcle          as char.
def var l-soc-trait      as char no-undo.
def var l-soc-dup        as char no-undo.
def var l-motcle         as char no-undo.
def var l-tarif          as char.
def var wtypaux          as char.
def var wcodaux          as char.
def var wtarif           as char.
def var wdate-euro       as date.
def var wspecif          as char.
def var wfamart-min      as char no-undo.
def var wfamart-max      as char no-undo.
def var wsoufam-min      as char no-undo.
def var wsoufam-max      as char no-undo.

def var cptok    as integer.
def var cpt1     as integer.
def var cpt2     as integer.
def var cpt3     as integer.
def var cpt4     as integer.
def var cpt5     as integer.
def var cpt6     as integer.
def var cpt7     as integer.
def var cpt8     as integer.
def var cpt9     as integer.
def var cpt10     as integer.
def var cpt11     as integer.
def var cpt12     as integer.
def var cpt13     as integer.

def stream file1.
def stream file2.
def stream file3.

def temp-table remises
    field codsoc       like remcli.codsoc
    field motcle       like remcli.motcle
    field typaux       like remcli.typaux
    field codaux       like remcli.codaux
index tiers is primary unique codsoc motcle typaux codaux.
     
def var wheu as char .
def var wnomfic as char.

wheu = string(today,"999999") + "." + string(time,"hh:mm:ss").
wnomfic = "suivi-ncodfam3." + wheu + ".dgr".
wnomfic = replace(wnomfic,":","").
output to value(wnomfic).

wnomfic = "ncodfam3_anomalie." + wheu + ".dgr".
wnomfic = replace(wnomfic,":","").
output STREAM file1 to value(wnomfic).

wnomfic = "ncodfam3_OK." + wheu + ".dgr".
wnomfic = replace(wnomfic,":","").
output STREAM file2 to value(wnomfic).

wnomfic = "ncodfam3_log." + wheu + ".dgr".
wnomfic = replace(wnomfic,":","").
output STREAM file3 to value(wnomfic).

assign l-soc-trait = "01,08".
assign l-motcle = "ACH,VTE,VTP".
/*---------------------------*/
/* traitement des parametres */
/*---------------------------*/
if pfamart <> " " then
   assign wfamart-min = pfamart
          wfamart-max = pfamart.
else
   assign wfamart-min = "001"
          wfamart-max = "099".
if  psoufam-min <> " " and psoufam-max <> " "  then
   assign wsoufam-min = psoufam-min
          wsoufam-max = psoufam-max.
else
   assign wsoufam-min = "001"
          wsoufam-max = "999".

/*-----------------------------------------*/
/* Initilaisation de la table tiers remise */
/*-----------------------------------------*/
l-soc-dup = "01,08,43".

do i = 1 to num-entries(l-soc-dup):
 
   codsoc-dup = entry(i,l-soc-dup).
   if codsoc-dup = ""
      then next.
/*--------------------------------------*/
/*  Boucle sur les motcle � dupliquer   */
/*--------------------------------------*/

  do j = 1 to num-entries(l-motcle):
    
    wmotcle = entry(j,l-motcle).
    if wmotcle = ""
       then next.

    if wmotcle = "VTP"
       then next.
       
    for each remcli where
        remcli.codsoc = codsoc-dup and
        remcli.motcle = wmotcle and
        remcli.typaux = "tie" and
        remcli.codaux > "" and
        (remcli.date-fin = ? or remcli.date-fin > today) and
        remcli.articl <> " " 
    no-lock break by remcli.codaux:
    
      if first-of(remcli.codaux) then do :
      
         find remises use-index tiers where
              remises.codsoc = remcli.codsoc and
              remises.motcle = remcli.motcle and
              remises.typaux = remcli.typaux and
              remises.codaux = remcli.codaux
         exclusive-lock no-error.
         if not available remises then do:
           create remises.
           assign remises.codsoc = remcli.codsoc
                  remises.motcle = remcli.motcle
                  remises.typaux = remcli.typaux
                  remises.codaux = remcli.codaux.
         end.
display remises.         
      end.  /* first-of codaux */ 
    end.  /* for each remcli */
    
  end. /* boucle sur motcle */
end. /* boucle sur societe */
    
/*--------------------------------------------------------------*/
/*    Boucle de traitement sur les 2 soci�t�s � traiter         */
/*--------------------------------------------------------------*/

do ind = 1 to  num-entries( l-soc-trait ) :

  codsoc-trait = entry(ind,l-soc-trait).
  if codsoc-trait = "" then next .

  if codsoc-trait = "01" then
     l-soc-dup = "01,43".
  else
     l-soc-dup = "08".

/*----------------------------------------------------*/
/*    Recherche des articles � dupliquer              */
/*----------------------------------------------------*/
  
  for each tabges where
      tabges.codsoc = codsoc-trait and
      tabges.etabli = " " and
      tabges.typtab = "ART" and
      tabges.prefix = "NEW-CODIF" and
      tabges.dattab[2] = ? and
      substring(tabges.libel1[2], 1, 3) >= wfamart-min and
      substring(tabges.libel1[2], 1, 3) <= wfamart-max and
      substring(tabges.libel1[2], 5, 3) >= wsoufam-min and
      substring(tabges.libel1[2], 5, 3) <= wsoufam-max
      
  exclusive-lock:
  
    cpt1 = cpt1 + 1.
    export stream file3 delimiter ";"
           "enreg correspondance lu : "
            tabges.codsoc tabges.codtab tabges.libel1[1].
            
    if cpt1 modulo 100 = 0 then disp cpt1.

/*----------------------------------------*/    
/*  Boucle sur les societes � dupliquer   */
/*----------------------------------------*/ 

  do i = 1 to num-entries(l-soc-dup):
  
     codsoc-dup = entry(i,l-soc-dup).
     if codsoc-dup = ""
        then next.
/*--------------------------------------*/
/*  Boucle sur les motcle � dupliquer   */
/*--------------------------------------*/

    do j = 1 to num-entries(l-motcle):
      
      wmotcle = entry(j,l-motcle).
      if wmotcle = ""
         then next.
         
      if wmotcle = "ACH" then 
         assign l-tarif = "DIR,STE,PRE"
                wdate-euro = 01/01/01.

      if wmotcle = "VTE" then
         assign l-tarif = "CLI,HTC,REV,PRO,PIE,CTE"
                wdate-euro = 12/15/01.

      if wmotcle = "VTP" then 
         assign l-tarif = "PRO"
                wdate-euro = 12/15/01.
            
/*-----------------------------------------------------------*/
/*    Recherche des enreg tarcli futurs � dupliquer          */
/*-----------------------------------------------------------*/  
/* recherche des anciens articles */

    for each tarcli where
        tarcli.codsoc = codsoc-dup and 
        tarcli.motcle = wmotcle and
        lookup(tarcli.code-tarif, l-tarif) <> 0 and
        tarcli.opecre <> "DUP" and
        tarcli.date-debut > today and
        tarcli.articl = tabges.codtab
    use-index article
    no-lock :
    export stream file3
      "for each" tarcli.codsoc tarcli.motcle tarcli.code-tarif 
             tarcli.articl tarcli.typaux tarcli.codaux. 
    
    RUN CREATION-TARCLI.

    end. /* for each tarcli */
    
/*-------------------------------------------------------------*/
/*    Recherche des enreg tarcli en cours � dupliquer          */
/*-------------------------------------------------------------*/
/*--------------------------------*/
/*    Boucle sur les codes tarifs */
/*--------------------------------*/

    do k = 1 to num-entries(l-tarif):
    
       wtarif = entry(k,l-tarif).
       if wtarif = ""
          then next.
  
       find first tarcli where 
            tarcli.codsoc = codsoc-dup and
            tarcli.motcle = wmotcle and
            tarcli.date-debut <= today and
            tarcli.date-debut > wdate-euro and
            tarcli.opecre <> "DUP" and
            tarcli.code-tarif = wtarif and
            tarcli.articl = tabges.codtab
       no-lock no-error.
       
       if available tarcli then do :
          export stream file3
            "find first" tarcli.codsoc tarcli.motcle tarcli.code-tarif
                   tarcli.articl tarcli.typaux tarcli.codaux.
          RUN CREATION-TARCLI.
       end.   
          
    end. /* boucle sur codes tarifs  */

/*----------------------------------------------------*/
/*    Recherche des enreg remcli � dupliquer          */
/*----------------------------------------------------*/  

/*-----------------------------------------------------------------*/
/*     Boucle sur la table tiers renseign�e en debut de programme  */
/*-----------------------------------------------------------------*/
    
   if wmotcle <> "VTP" then do:
     for each remises where
         remises.codsoc = codsoc-dup and
         remises.motcle = wmotcle
     no-lock:    

       /*-------------------------------------------*/
       /* recherche des remises futures � dupliquer */
       /*-------------------------------------------*/
       /* recherche des anciens articles */

       for each remcli where
          remcli.codsoc = codsoc-dup and 
          remcli.motcle = wmotcle and
          remcli.typaux = remises.typaux and
          remcli.codaux = remises.codaux and
          (remcli.date-debut > today or remcli.date-fin > today) and
          remcli.articl = tabges.codtab 
       no-lock :
    
         export stream file3
            "for each remcli " remcli.codsoc remcli.motcle
                      remcli.codaux remcli.articl remcli.typaux remcli.codaux.
         RUN CREATION-REMCLI.
      
       end. /* for each remcli */
      
       /*--------------------------------------------*/
       /* recherche des remises en cours � dupliquer */
       /*--------------------------------------------*/
       /* recherche des anciens articles */

       if wmotcle = "ACH" then
           wspecif = " ".
       else
           wspecif = "?".     

       find first remcli where
             remcli.codsoc = codsoc-dup and
             remcli.motcle = wmotcle and
             remcli.typaux = remises.typaux and
             remcli.codaux = remises.codaux and
             remcli.specif <> wspecif and
             remcli.date-debut <= today and
             remcli.date-debut > 12/15/01 and
             remcli.articl = tabges.codtab
       no-lock no-error.
                                             
       if available remcli then do :
         export stream file3
             "find first remcli " remcli.codsoc remcli.motcle 
                    remcli.codaux remcli.articl remcli.typaux remcli.codaux.
         RUN CREATION-REMCLI.
       end.

     end. /* for each remises */
    end. /* if moptcle <> "VTP" */
              
    end. /* Boucle sur motcle */
    
   end. /* Boucle societes � dupliquer */    

/*-------------------------------------------------*/
/* maj de la date de creation du Referencier Tarif */
/*-------------------------------------------------*/

  if trt-test= no then
     assign tabges.dattab[2] = today
            tabges.opemaj = "BVL".
         
  end.  /* for each tabges */
 
 end. /* Boucle Societes */

disp      "NB enreg tables lus             : " cpt1 no-label
          skip
          "NB tarcli vte lus               : " cpt2 no-label
          skip
          "Nb tarcli ach lus               : " cpt3 no-label
          skip
          "NB tarcli vte deja crees        : " cpt4 no-label
          skip
          "NB tarcli ach deja crees        : " cpt5 no-label
          skip
          "NB tarcli vte crees             : " cpt6 no-label
          skip
          "NB tarcli ach crees             : " cpt7 no-label
          skip
          "NB remcli vte lus               : " cpt8 no-label
          skip
          "Nb remcli ach lus               : " cpt9 no-label
          skip
          "NB remcli vte deja crees        : " cpt10 no-label
          skip
          "NB remcli ach deja crees        : " cpt11 no-label
          skip
          "NB remcli vte crees             : " cpt12 no-label
          skip
          "NB remcli ach crees             : " cpt13 no-label
          skip.
          
output stream file1 close.
output stream file2 close.
output stream file3 close.
          
PROCEDURE CREATION-TARCLI.

    export stream file3 delimiter ";"
           "Enreg tarcli lu : "
           tarcli.codsoc tarcli.code-tarif tarcli.date-debut tarcli.articl
           tarcli.typaux tarcli.codaux.
               
    if tarcli.motcle = "VTE" then
       cpt2 = cpt2 + 1.
    else   
       cpt3 = cpt3 + 1.
       
/* recherche du nouvel article */      

      find b-tarcli where
           b-tarcli.codsoc = tarcli.codsoc and
           b-tarcli.motcle = tarcli.motcle and
           b-tarcli.typaux = tarcli.typaux and
           b-tarcli.codaux = tarcli.codaux and
           b-tarcli.articl = tabges.libel1[1] and
           b-tarcli.code-tarif = tarcli.code-tarif and
           b-tarcli.date-debut = tarcli.date-debut and
           b-tarcli.chrono = tarcli.chrono
      no-lock no-error.
      
      if available b-tarcli then do:
         if tarcli.motcle = "VTE" then
            cpt4 = cpt4 + 1.
         else
            cpt5 = cpt5 + 1.   
         export stream file1 delimiter ";"
                "Article deja cree sur tarcli : "  
                b-tarcli.codsoc b-tarcli.code-tarif b-tarcli.articl
                b-tarcli.typaux b-tarcli.codaux.
         return.       
      end.

/* creation du nouvel article */         

      export stream file2 delimiter ";"
             "Enreg tarcli duplique : "
             tarcli.codsoc tarcli.code-tarif tarcli.date-debut tarcli.articl
             tarcli.typaux tarcli.codaux "sur " tabges.libel1[1].
      
      if trt-test = no then do:
                    
         create b-tarcli.
         buffer-copy tarcli
          except tarcli.articl
                 tarcli.datcre
                 tarcli.opecre
                 tarcli.heucre
                 tarcli.datmaj
                 tarcli.heumaj
                 tarcli.opemaj
          to b-tarcli
          assign b-tarcli.articl =  tabges.libel1[1]
                 b-tarcli.datcre = today
                 b-tarcli.opecre = "BVL"
                 b-tarcli.heucre = "00:00"
                 b-tarcli.datmaj = today
                 b-tarcli.heumaj = "00:00"
                 b-tarcli.opemaj = "BVL".

          if tarcli.motcle = "VTE" then do :
             CASE tabges.libel1[3]:
                  when "-100" then
                       assign b-tarcli.valeur = b-tarcli.valeur / 100. 
                  when "10" then
                       assign b-tarcli.valeur = b-tarcli.valeur * 10. 
                  when "1000" then
                       assign b-tarcli.valeur = b-tarcli.valeur * 1000. 
             END CASE. 
          end.                 
       end. /* trt-test = no */    
       
       if tarcli.motcle = "VTE" then
          cpt6 = cpt6 + 1.
       else          
          cpt7 = cpt7 + 1.       


END PROCEDURE.

PROCEDURE CREATION-REMCLI.


    export stream file3 delimiter ";"
           "Enreg remcli lu : "
           remcli.codsoc remcli.motcle remcli.articl remcli.codaux.
                  
    if remcli.motcle = "VTE" then
       cpt8 = cpt8 + 1.
    else   
       cpt9 = cpt9 + 1.
       
/* recherche du nouvel article */      

      find b-remcli where
           b-remcli.codsoc = remcli.codsoc and
           b-remcli.motcle = remcli.motcle and
           b-remcli.typaux = remcli.typaux and
           b-remcli.codaux = remcli.codaux and
           b-remcli.articl = tabges.libel1[1] and
           b-remcli.specif = remcli.specif and
           b-remcli.date-debut = remcli.date-debut 
      no-lock no-error.
      
      if available b-remcli then do:
         if remcli.motcle = "VTE" then
            cpt10 = cpt10 + 1.
         else
            cpt11 = cpt11 + 1.   
         export stream file1 delimiter ";"
                "Article deja cree sur remcli : "  
                b-remcli.codsoc b-remcli.motcle b-remcli.articl
                b-remcli.specif b-remcli.typaux b-remcli.codaux.
         return.       
      end.

/* creation du nouvel article */         

      export stream file2 delimiter ";"
             "Enreg remcli cree : "
              remcli.codsoc remcli.motcle remcli.articl
              remcli.specif remcli.codaux.
      
      if trt-test = no then do:
                    
         create b-remcli.
         buffer-copy remcli
          except remcli.articl
                 remcli.datcre
                 remcli.opecre
                 remcli.heucre
                 remcli.datmaj
                 remcli.heumaj
                 remcli.opemaj
          to b-remcli
          assign b-remcli.articl = tabges.libel1[1]
                 b-remcli.datcre = today
                 b-remcli.opecre = "BVL"
                 b-remcli.heucre = "00:00"
                 b-remcli.datmaj = today
                 b-remcli.heumaj = "00:00"
                 b-remcli.opemaj = "BVL".
                 
        end. /* trrt-test = no */
       if remcli.motcle = "VTE" then
          cpt12 = cpt12 + 1.
       else          
          cpt13 = cpt13 + 1.       

END PROCEDURE.