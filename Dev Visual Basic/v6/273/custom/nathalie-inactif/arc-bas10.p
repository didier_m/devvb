/*=========================================================================*/
/*                         ARC-BAS10.p                                     */
/*                         +++++++++++                                     */
/*   Archivage BASTAT                                                      */
/*=========================================================================*/

{ connect.i       }
{ arc-bas10.i new }
{ bat-ini.i   new }
{ aide.i      new }
{ compo.f     new }
{ chx-opt.f       }

aide-fichier = "arc-bas10.aid" . aide-par = "" . run aide-lec.p .

do ind = 1 to 18 :
  aide-m = "libel" + string( ind , "999" ) .
  { aide-lib.i }
  libelle[ ind ] = aide-lib .
end .

{ fr-cadre.i lib-prog }

color display message libelle[ 1 for 3 ] with frame fr-saisie .
      display         libelle[ 1 for 3 ] with frame fr-saisie .

/* Saisie parametres */
saisie-generale :
repeat:
   /* parametres */

   run arc-bas1q.p.

   if keyfunction(lastkey) = "end-error" then leave saisie-generale.

   /* acceptation, refus, modification */
    {standard.i -chxopt std-chxbas }

    assign chx-milieu = 62
           chx-def    = 1
           chx-ombre  = "O"
           .

    { chx-opt.i "3" "std-chxbas" "16" "15" "17" " " "fr-acc" }

    if chx-trait = 1 then leave saisie-generale.     /*   Validation    */
    if chx-trait = 2 then leave saisie-generale.     /*   Refus         */
    if chx-trait = 3 then next  saisie-generale.     /*   Modification  */

end .   /* SAISIE-GENERALE  */

/* Lancement execution si choix 1 */
if chx-trait = 1 and keyfunction( lastkey ) <> "end-error"
then repeat :

   batch-tenu = yes . /* Gestion en BATCH */

   /* Sauvegarde parametres saisis pour batch */

   { bat-zon.i "datmax"       "datmax"       "d" "''" "libelle[02]" }
   { bat-zon.i "motcles"      "motcles"      "c" "''" "libelle[01]" }
   { bat-zon.i "basenameo"    "basenameo"    "c" "''" "libelle[03]" }
   { bat-zon.i "lib-prog"     "lib-prog"     "c" "''" "libelle[13]" }
   { bat-zon.i "codsoc-soc"   "codsoc-soc"   "c" "''" "libelle[14]" }

   /* Execution */
   { bat-maj.i "arc-bas11.p" }

   leave.

end.

/* Effacement ecrans */
hide frame fr-saisie no-pause.
hide frame fr-titre no-pause.
hide frame fr-cadre no-pause.