/*==========================================================================*/
/*                        VENTES DES SALARIES                               */
/*--------------------------------------------------------------------------*/
/* vtesal0.i : variables communes aux programmes vtesal*.p                  */
/*--------------------------------------------------------------------------*/
/* DGR 030903                                                               */
/* DGR 180405 => traitement semestriel                                      */
/* VLE 061104 => ajout Jardivert                                            */ 
/*==========================================================================*/

/* variables pour societes */
def {1} shared var    wcodsoc    as char    format "x(02)" no-undo.
def {1} shared var    libsoc     as char   extent 4        no-undo.
def {1} shared var    l-soc      as char                   no-undo.
def {1} shared var    l-auxsoc   as char   initial "01,08" no-undo.
def {1} shared var    typ-trt    as char    format "x"     no-undo.
def {1} shared var    lib-trt    as char    format "x(40)" no-undo.
def {1} shared var    typ-per    as char    format "x"     no-undo.
def {1} shared var    lib-per    as char    format "x(40)" no-undo.

def {1} shared var    i          as int                    no-undo.
def {1} shared var    j          as int                    no-undo.
def {1} shared var    ddatdeb    as date    format "99/99/9999"  no-undo.
def {1} shared var    ddatfin    as date    format "99/99/9999"  no-undo.
def {1} shared var    ddat1      as date    format "99/99/9999"  no-undo.
def {1} shared var    ddat2      as date    format "99/99/9999"  no-undo.
def {1} shared var    edit       as logical format "O/N"        no-undo.
def {1} shared var    libedit    as char    format "x(40)"      no-undo.

def {1} shared var wcodaux like auxapr.codaux no-undo.
def {1} shared var wcattc as deci decimals 2 format ">>>>>9.99-" no-undo.
def {1} shared var wcarem as deci decimals 2 format ">>>>>9.99-" no-undo.
def {1} shared var wcatotsal    as deci decimals 2 format ">>>>>9.99-" no-undo.
def {1} shared var wcaremsal    as deci decimals 2 format ">>>>>9.99-" no-undo.
def {1} shared var wcatotsoc    as deci decimals 2 format ">>>>>>>>9.99-" extent 4 no-undo.
def {1} shared var wcaremsoc    as deci decimals 2 format ">>>>>>>>9.99-" extent 4 no-undo.

def {1} shared var wmacompte    as deci decimals 2 format ">>>>>9.99-" no-undo.
def {1} shared var wdacompte    as date                                no-undo.

def {1} shared var wtaux as deci no-undo.
def {1} shared var wnocarte     as char format "x(13)"           no-undo.
def {1} shared var wlibtie      as char format "x(25)"           no-undo.
def {1} shared var wadres2      as char format "x(32)"           no-undo.
def {1} shared var wadres3      as char format "x(32)"           no-undo.
def {1} shared var wadres4      as char format "x(32)"           no-undo.
def {1} shared var wadres5      as char format "x(32)"           no-undo.
def {1} shared var lok          as log                           no-undo.
def {1} shared var wsoc as char.

/* variables pour edition */
def {1} shared var sequence     as char format "x(05)"           no-undo.
def {1} shared var sel-sequence as char format "x(12)"           no-undo.
def {1} shared var fichier2     as char format "x(12)"           no-undo.

