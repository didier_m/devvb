/*==========================================================================*/
/*                           E X T - A G I L C L I  . I                     */
/* Extraction des donnees pour AGIL                                         */
/*--------------------------------------------------------------------------*/
/* Extraction Clients identifies cartes fidelite et cartes salsarie         */
/*==========================================================================*/
/* 19052010 VLE : correction init idcust et idcontact                       */
/* 06102010 VLE : suppression trt soci�t� 43                                */
/* 20122010 VLE : Gestion de la carte ancien salari�                        */
/* 28012011 VLE : Ajout de la societe 59 VITRY                              */
/* 14022011 VLE : Ajout traitement carte jardins ouvriers                   */
/* 04052011 VLE : Nouvelle version Agil, adresse norme Afnor                */
/*                interdiction de se vendre � soi-m�me                      */
/* 18052011 VLE : modification acc�s auxili pour Vitry                      */
/* 28032012 VLE : Mise � blanc pr�nom si non renseigne dans DEAL            */
/*==========================================================================*/

CASE code-table :
/*-------------------------------------------*/
/*  Mise en forme donn�es client identifies  */
/*-------------------------------------------*/
  when "CL" then do :
  
        ID_CLIENT= trim(auxapr.codsoc) + trim(auxili.codaux).
        ID_CONTACT = trim(auxapr.codsoc) + trim(auxili.codaux).
        NO_CARTE = trim(auxapr.codsoc) + trim(auxili.codaux).
        CASE auxapr.codsoc :
                        when "01" then ID_DYNINFO_003 = "PRE".
                        when "08" then ID_DYNINFO_003 = "PRA".
    /* VLE 28012011*/   when "59" then ID_DYNINFO_003 = "PRV". 
                        otherwise ID_DYNINFO_003 = "SCA".
        END CASE.
        
		if auxapr.type-adh = "8"
		   then ID_DYNINFO_003 = "JOU".		/* VLE 14022011 carte Jardins Ouvriers */
        /*-------------------------------------------------*/
        /*  recherche du numero de carte � valider ensuite */
        /*-------------------------------------------------*/
        find auxdet where auxdet.codsoc = auxdet-soc
                                and   auxdet.motcle = "CFI"
                                and   auxdet.typaux = auxili.typaux
                                and   auxdet.codaux = auxili.codaux
                no-lock no-error.
        if available auxdet
                then do :
                        NO_CARTE = auxdet.codtab.
                        ID_DYNINFO_003 = "ADH".
                end.
        
        /*------------------------------------------------------------*/
        /*  codification de la civilit� et extraction nom et prenom   */
        /*------------------------------------------------------------*/
        CD_CIVILITE = "".
        NM_CLIENT = auxili.adres[1].
        PNM_CLIENT = "".
        if auxili.adres[1] begins "MR "
                then CD_CIVILITE = "01". 
        if auxili.adres[1] begins "MME "
                then CD_CIVILITE = "02". 
        if auxili.adres[1] begins "MLE " or auxili.adres[1] begins "Melle "
                then CD_CIVILITE = "03". 

        if auxili.adres[1] begins "MR " 
                then do :
                        NM_CLIENT = "".
                        do k = 4 to 32 :
                                if substr(auxili.adres[1], k, 1) = " "
                                        then do :
                                        k = k + 1.
                                        PNM_CLIENT = substr(auxili.adres[1], k, 32).
                                        leave.
                                end.
                                else
                                        substr(NM_CLIENT, k, 1)  = substr(auxili.adres[1], k, 1).
                        end.
                end.    
        
        if auxili.adres[1] begins "MME " or auxili.adres[1] begins "MR  " or auxili.adres[1] begins "MLE " 
                then do :
                        NM_CLIENT = "".
                        do k = 5 to 32 :
                                if substr(auxili.adres[1], k, 1) = " "
                                then do :
                                        k = k + 1.
                                        PNM_CLIENT = substr(auxili.adres[1], k, 32).
                                        leave.
                                end.
                                else
                                        substr(NM_CLIENT, k, 1)  = substr(auxili.adres[1], k, 1).
                        end.
                end.
        
        if auxili.adres[1] begins "MME  "  
                then do :
                        NM_CLIENT = "".
                        do k = 6 to 32 :
                                if substr(auxili.adres[1], k, 1) = " "
                                then do :
                                        k = k + 1.
                                        PNM_CLIENT = substr(auxili.adres[1], k, 32).
                                        leave.
                                end.
                                else
                                        substr(NM_CLIENT, k, 1)  = substr(auxili.adres[1], k, 1).
                        end.
                end.
        
        if auxili.adres[1] begins "Melle "
                then do :
                        NM_CLIENT = "".
                        do k = 7 to 32 :
                                if substr(auxili.adres[1], k, 1) = " "
                                then do :
                                        k = k + 1.
                                        PNM_CLIENT = substr(auxili.adres[1], k, 32).
                                        leave.
                                end.
                                else
                                        substr(NM_CLIENT, k, 1)  = substr(auxili.adres[1], k, 1).
                        end.
                end.
        
        NM_CLIENT = left-trim(substr(NM_CLIENT, 1, 30)).
        PNM_CLIENT = right-trim(PNM_CLIENT).
        if PNM_CLIENT = ""
           then PNM_CLIENT = " ".
		LB_ADRCLI2 = right-trim(substr(auxili.adres[2], 1, 38)).
        LB_ADRCLI3 = right-trim(substr(auxili.adres[3], 1, 38)).
        LB_ADRCLI4 = right-trim(substr(auxili.adres[4], 1, 38)).

        if LB_ADRCLI1 = ""
           then LB_ADRCLI1 = " ".
        if LB_ADRCLI2 = ""
           then LB_ADRCLI2 = " ".
		if LB_ADRCLI3 = ""
           then LB_ADRCLI3 = " ".
		if LB_ADRCLI4 = ""
           then LB_ADRCLI4 = " ".
		
        CD_POSTAL_CLI = substr(auxili.adres[5], 1, 5). 
        LB_VILLE_CLI = right-trim(substr(auxili.adres[5], 7, 26)).
        
        FL_NPAI = no.
        FL_BLOCAGE = no.
        if auxapr.blocage = "1"  or
           auxapr.cpt-ferme = "1" or
           auxapr.cpt-ferme-g = "1" or
           auxapr.contentieux = "3"
                then FL_BLOCAGE = yes.
        NO_TEL_CLI = auxapr.teleph.
        DT_NAISSANCE = "".
        if auxapr.date-nais <> ?
                then DT_NAISSANCE = string(auxapr.date-nais, "99/99/9999").
                else DT_NAISSANCE = "". 
        LB_ADRESSE_MAIL = right-trim(auxapr.alpha-5).
        FL_CONTACT = yes.
        CD_MAG_RECRUTEUR = auxapr.magasin.
        /*  on affecte les clients des autres d�pots aux magasins de tests pour qu'ils soient connus dans agil */
        if lookup(auxapr.magasin, l-mag-gv) =0 and
           lookup(auxapr.magasin, l-mag-asec) = 0
           then do :
                        CASE auxapr.codsoc :
                                when "01" then CD_MAG_RECRUTEUR = "991".
                                when "08" then CD_MAG_RECRUTEUR = "998".
             /* VLE 28012011*/  when "59" then CD_MAG_RECRUTEUR = "136". 
                                otherwise CD_MAG_RECRUTEUR = "".
                        END CASE.
                end.
        
        if auxili.datcre <> ?
                then DT_RECRUTEMENT = string(auxili.datcre, "99/99/9999").
                else DT_RECRUTEMENT = "".
        NB_POINTS = 0.
        DT_MAJ_POINTS = "".
        DT_FIN_POINTS = "".
        DT_MAJ_CLIENT = string(auxili.datmaj, "99/99/9999").
        ID_DYNINFO_001 = "CLI".
        ID_DYNINFO_002 = auxapr.codsoc.
        ID_DYNINFO_005 = auxapr.telex.
        
        put stream agil unformatted
                        Type_Ligne_Det ID_CLIENT "~011" ID_CONTACT "~011" NO_CARTE "~011" CD_CIVILITE "~011" 
                        NM_CLIENT "~011"  PNM_CLIENT "~011" LB_ADRCLI1 "~011" LB_ADRCLI2 "~011" 
						LB_ADRCLI3 "~011" LB_ADRCLI4 "~011" CD_POSTAL_CLI "~011" 
                        LB_VILLE_CLI "~011" CD_PAYS "~011" CD_ETAT_PAYS "~011" FL_NPAI "~011" FL_BLOCAGE "~011" 
                        NO_TEL_CLI "~011" DT_NAISSANCE "~011" LB_ADRESSE_MAIL "~011" FL_CONTACT "~011" CD_LANGUE "~011" 
                        CD_MAG_RECRUTEUR "~011" DT_RECRUTEMENT "~011" NB_POINTS "~011" DT_MAJ_POINTS "~011" 
                        DT_FIN_POINTS "~011" DT_MAJ_CLIENT 
						"~011" ID_DYNINFO_001 "~011" ID_DYNINFO_002 "~011" ID_DYNINFO_003 
                        "~011" ID_DYNINFO_005 "~015" skip. 
  end.
/*--------------------------------------------------*/
/*  Mise en forme donn�es cartes fidelite clients   */
/*--------------------------------------------------*/
  when "CF" then do :
	
        if auxdet.calf14 = "" 
                        then do :
                                if auxdet.codsoc = "08"
                                        then ID_CLIENT = trim(auxdet.codsoc) + substr(auxdet.codtab, 6, 8).
                                        else ID_CLIENT = "01" + substr(auxdet.codtab, 6, 8).
                                ID_CONTACT= ID_CLIENT.
                                end.
                        else assign ID_CLIENT = entry(1, auxdet.calf14)
                                                ID_CONTACT= entry(2, auxdet.calf14).
        NO_CARTE = auxdet.codtab.
        
        /*------------------------------------------------------------*/
        /*  codification de la civilit� et extraction nom et prenom   */
        /*------------------------------------------------------------*/
        CD_CIVILITE = "".
        NM_CLIENT = "".
        PNM_CLIENT = "".
        if auxdet.calf02 begins "MR" or auxdet.calf02 ="M"
                then CD_CIVILITE = "01". 
        if auxdet.calf02 begins "MME"
                then CD_CIVILITE = "02". 
        if auxdet.calf02 begins "MLE" or auxdet.calf02 begins "MLLE"
                then CD_CIVILITE = "03". 
                        
        NM_CLIENT = entry(1, auxdet.calf03).
        NM_CLIENT = left-trim(substr(NM_CLIENT, 1, 30)).
        if num-entries(auxdet.calf03) > 1 
                then PNM_CLIENT = entry(2, auxdet.calf03).
                else PNM_CLIENT = " ".
        
        If auxdet.calf01 = "9" and NM_CLIENT = ""
                then NM_CLIENT = "EN ATTENTE DE CREATION".

        LB_ADRCLI1 = right-trim(substr(auxdet.calf04, 1, 38)).
        LB_ADRCLI2 = right-trim(substr(auxdet.calf05, 1, 38)).
		LB_ADRCLI3 = right-trim(substr(auxdet.calf06, 1, 38)).
		LB_ADRCLI4 = right-trim(substr(auxdet.calf07, 1, 38)).
        
		if LB_ADRCLI1 = ""
           then LB_ADRCLI1 = " ".
        if LB_ADRCLI2 = ""
           then LB_ADRCLI2 = " ".
		if LB_ADRCLI3 = ""
           then LB_ADRCLI3 = " ". 
		if LB_ADRCLI4 = ""
           then LB_ADRCLI4 = " ".
		   
        CD_POSTAL_CLI = substr(auxdet.calf08, 1, 5). 
        LB_VILLE_CLI = right-trim(substr(auxdet.calf08, 7, 26)).
        
        if auxdet.cdat01 <> ? 
                then FL_NPAI = yes.
                else FL_NPAI = no.
                
        if auxdet.cdat02 <> ? 
                then FL_BLOCAGE = yes.
                else FL_BLOCAGE = no. 
                
        NO_TEL_CLI = entry( 1, auxdet.calf20).
        
        if auxdet.cdat03 <> ?
                then DT_NAISSANCE = string(auxdet.cdat03, "99/99/9999").
                else DT_NAISSANCE = "".
                
        if auxdet.calf16 = "@"
                then LB_ADRESSE_MAIL = "".
                else LB_ADRESSE_MAIL = auxdet.calf16.
                
        FL_CONTACT = yes.
        CD_MAG_RECRUTEUR = auxdet.calf09.
        if lookup(auxdet.calf09, l-mag-ferme) <> 0 and substr(auxdet.codtab, 1, 6) = "291000"
                then CD_MAG_RECRUTEUR = "998".
        if lookup(auxdet.calf09, l-mag-gv) = 0 and substr(auxdet.codtab, 1, 6) = "293000"
                then CD_MAG_RECRUTEUR = "991".
        
        if auxdet.cdat06 <> ? 
                then DT_RECRUTEMENT = string(auxdet.cdat06, "99/99/9999").
                else DT_RECRUTEMENT  = "".
                
        NB_POINTS = auxdet.cnum02.
        DT_MAJ_POINTS = string(auxdet.datmaj, "99/99/9999").
        DT_FIN_POINTS = "".
        DT_MAJ_CLIENT = string(auxdet.datmaj, "99/99/9999").
        ID_DYNINFO_001 = "GP".
        
        if auxdet.codsoc = ""
                then do :
                        ID_DYNINFO_002 = "01".
                        ID_DYNINFO_003 = "GAM".
     /* VLE 28012011*/  if lookup(auxdet.calf09, l-mag-vitry) <> 0
                                then ID_DYNINFO_002 = "59".  
                end.
                else do :
                        ID_DYNINFO_002 = auxdet.codsoc.
                        ID_DYNINFO_003 = "AGR".
                end.
		/* VLE 20122010 carte ancien salari� */	
		if auxdet.calf11 = "A"
		   then do :
				ID_DYNINFO_001 = "ASA".
				ID_DYNINFO_003 = "ASA".
		   end.
        
        if auxdet.cnum09 = -1
           then ID_DYNINFO_004 = "NC".
           else ID_DYNINFO_004 = string(auxdet.cnum09).
           
        if num-entries(auxdet.calf20) > 1 
                then ID_DYNINFO_005 = entry( 2, auxdet.calf20).
                else ID_DYNINFO_005 = "".
                
        if auxdet.codech = ""
           then ID_DYNINFO_006 = "00".
           else ID_DYNINFO_006 = "0" + substr(auxdet.codech, 1, 1).
           
        if auxdet.domapr = ""
           then ID_DYNINFO_007 = "00".
           else ID_DYNINFO_007 = "0" + substr(auxdet.domapr, 1, 1).
        
        do ind-mkt = 1 to 38 :
         if substr(auxdet.calf18, ind-mkt, 1) = "0" or substr(auxdet.calf18, ind-mkt, 1) = " "  or substr(auxdet.calf18, ind-mkt, 1) = ""
                then ID_DYNINFO_MKT[ind-mkt] = no.
                else ID_DYNINFO_MKT[ind-mkt] = yes.
        end.
        
        put stream agil unformatted
                        Type_Ligne_Det ID_CLIENT "~011" ID_CONTACT "~011" NO_CARTE "~011" CD_CIVILITE "~011" 
                        NM_CLIENT "~011"  PNM_CLIENT "~011" LB_ADRCLI1 "~011" LB_ADRCLI2 "~011" 
						LB_ADRCLI3 "~011" LB_ADRCLI4 "~011" CD_POSTAL_CLI "~011" 
                        LB_VILLE_CLI "~011" CD_PAYS "~011" CD_ETAT_PAYS "~011" FL_NPAI "~011" FL_BLOCAGE "~011" 
                        NO_TEL_CLI "~011" DT_NAISSANCE "~011" LB_ADRESSE_MAIL "~011" FL_CONTACT "~011" CD_LANGUE "~011" 
                        CD_MAG_RECRUTEUR "~011" DT_RECRUTEMENT "~011" NB_POINTS "~011" DT_MAJ_POINTS "~011" 
                        DT_FIN_POINTS "~011" DT_MAJ_CLIENT 
						"~011" ID_DYNINFO_001 "~011" ID_DYNINFO_002 "~011" ID_DYNINFO_003 "~011" ID_DYNINFO_004
                        "~011" ID_DYNINFO_005 "~011" ID_DYNINFO_006 "~011" ID_DYNINFO_007 
                        "~011" ID_DYNINFO_MKT[1] "~011" ID_DYNINFO_MKT[2] "~011" ID_DYNINFO_MKT[3]
                        "~011" ID_DYNINFO_MKT[4] "~011" ID_DYNINFO_MKT[5] "~011" ID_DYNINFO_MKT[6] 
                        "~011" ID_DYNINFO_MKT[7] "~011" ID_DYNINFO_MKT[8] "~011" ID_DYNINFO_MKT[9]
                        "~011" ID_DYNINFO_MKT[10] "~011" ID_DYNINFO_MKT[11] "~011" ID_DYNINFO_MKT[12]
                        "~011" ID_DYNINFO_MKT[13] "~011" ID_DYNINFO_MKT[14] "~011" ID_DYNINFO_MKT[15]
                        "~011" ID_DYNINFO_MKT[16] "~011" ID_DYNINFO_MKT[17] "~011" ID_DYNINFO_MKT[18] 
                        "~011" ID_DYNINFO_MKT[19] "~011" ID_DYNINFO_MKT[20] "~011" ID_DYNINFO_MKT[21]
                        "~011" ID_DYNINFO_MKT[22] "~011" ID_DYNINFO_MKT[23] "~011" ID_DYNINFO_MKT[24]
                        "~011" ID_DYNINFO_MKT[25] "~011" ID_DYNINFO_MKT[26] "~011" ID_DYNINFO_MKT[27]
                        "~011" ID_DYNINFO_MKT[28] "~011" ID_DYNINFO_MKT[29] "~011" ID_DYNINFO_MKT[30]
                        "~011" ID_DYNINFO_MKT[31] "~011" ID_DYNINFO_MKT[32] "~011" ID_DYNINFO_MKT[33]
                        "~011" ID_DYNINFO_MKT[34] "~011" ID_DYNINFO_MKT[35] "~011" ID_DYNINFO_MKT[36]
                        "~011" ID_DYNINFO_MKT[37] "~011" ID_DYNINFO_MKT[38]
                        "~015" skip. 
        
  end.
/*-------------------------------------------*/
/*  Mise en forme donn�es cartes salarie     */
/*-------------------------------------------*/
  when "CS" then do :
  
        If auxdet.calf14 = "" 
                        then do :
                                ID_CLIENT = trim(auxdet.codsoc) + substr(auxdet.codtab, 6, 8).
                                ID_CONTACT= ID_CLIENT.
                                end.
                        else assign ID_CLIENT = entry(1, auxdet.calf14)
                                                ID_CONTACT= entry(2, auxdet.calf14).
        NO_CARTE = auxdet.codtab.
        
        /*------------------------------------------------------------*/
        /*  codification de la civilit� et extraction nom et prenom   */
        /*------------------------------------------------------------*/
        CD_CIVILITE = "".
        NM_CLIENT = "".
        PNM_CLIENT = "".
        if auxdet.calf02 begins "MR" or auxdet.calf02 ="M"
                then CD_CIVILITE = "01". 
        if auxdet.calf02 begins "MME"
                then CD_CIVILITE = "02". 
        if auxdet.calf02 begins "MLE" or auxdet.calf02 begins "MLLE"
                then CD_CIVILITE = "03". 
                
        NM_CLIENT = entry(1, auxdet.calf03).
        NM_CLIENT = left-trim(substr(NM_CLIENT, 1, 30)).
        if num-entries(auxdet.calf03) > 1 
                then PNM_CLIENT = entry(2, auxdet.calf03).
                else PNM_CLIENT = " ".
                                
        NM_CLIENT = left-trim(substr(NM_CLIENT, 1, 30)).
        PNM_CLIENT = right-trim(PNM_CLIENT).
        
        LB_ADRCLI1 = right-trim(substr(auxdet.calf04, 1, 38)).
        LB_ADRCLI2 = right-trim(substr(auxdet.calf05, 1, 38)).
		LB_ADRCLI3 = right-trim(substr(auxdet.calf06, 1, 38)).
		LB_ADRCLI4 = right-trim(substr(auxdet.calf07, 1, 38)).
        
        if LB_ADRCLI1 = ""
           then LB_ADRCLI1 = " ".
        if LB_ADRCLI2 = ""
           then LB_ADRCLI2 = " ".
		 if LB_ADRCLI3 = ""
           then LB_ADRCLI3 = " ". 
		if LB_ADRCLI4 = ""
           then LB_ADRCLI4 = " ".
		   
        CD_POSTAL_CLI = substr(auxdet.calf08, 1, 5). 
        LB_VILLE_CLI = right-trim(substr(auxdet.calf08, 7, 26)).
		
        FL_NPAI = no.
        if auxdet.cdat02 <> ? 
                then FL_BLOCAGE = yes.
                else FL_BLOCAGE = no. 
        NO_TEL_CLI = "".
        DT_NAISSANCE = "".
        LB_ADRESSE_MAIL = "".
        FL_CONTACT = yes.
        CD_MAG_RECRUTEUR = "".
        DT_RECRUTEMENT  = "".
        NB_POINTS = 0.
        DT_MAJ_POINTS = "".
        DT_FIN_POINTS = "".
        DT_MAJ_CLIENT = string(auxdet.datmaj, "99/99/9999").
        ID_DYNINFO_001 = "SAL".
        ID_DYNINFO_002 = auxdet.codsoc.
        ID_DYNINFO_003 = "SAL".
		ID_DYNINFO_C08 = auxdet.codtab.
 
        put stream agil unformatted
                        Type_Ligne_Det ID_CLIENT "~011" ID_CONTACT "~011" NO_CARTE "~011" CD_CIVILITE "~011" 
                        NM_CLIENT "~011"  PNM_CLIENT "~011" LB_ADRCLI1 "~011" LB_ADRCLI2 "~011" 
						LB_ADRCLI3 "~011" LB_ADRCLI4 "~011" CD_POSTAL_CLI "~011" 
                        LB_VILLE_CLI "~011" CD_PAYS "~011" CD_ETAT_PAYS "~011" FL_NPAI "~011" FL_BLOCAGE "~011" 
                        NO_TEL_CLI "~011" DT_NAISSANCE "~011" LB_ADRESSE_MAIL "~011" FL_CONTACT "~011" CD_LANGUE "~011" 
                        CD_MAG_RECRUTEUR "~011" DT_RECRUTEMENT "~011" NB_POINTS "~011" DT_MAJ_POINTS "~011" 
                        DT_FIN_POINTS "~011" DT_MAJ_CLIENT 
                    "~011" ID_DYNINFO_001 "~011" ID_DYNINFO_002 "~011" ID_DYNINFO_003 "~011" ID_DYNINFO_C08 "~015" skip. 
        
        end.
END CASE.     



