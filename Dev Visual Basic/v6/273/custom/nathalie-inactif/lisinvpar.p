/* Programme : lisinvpar.p */
output to lisinvpar-59.csv.
def var wtot as dec decimals 3.
def var ecart as dec decimals 3.
def var xx as char format "xxx".                               
def var wsoc as char initial "59".

def var toto as integer.
def var l-mag1 as char initial "006".
def var l-mag8 as char initial "407".

def var l-mag as char.
def var wmag  as char.
def var aa    as int.

def var l-mag0 as char.
def var l-soc0 as char.

for each magasi where magasi.codsoc = ""
and magasi.societe = wsoc
no-lock:
l-mag0 = l-mag0 + "," + magasi.codtab.
l-soc0 = l-soc0 + "," + magasi.societe. 
end.

l-mag0 = substr(l-mag0,2).
l-soc0 = substr(l-soc0,2).
def var wsoc0 as char.

if wsoc = "01" then l-mag = l-mag1. else l-mag = l-mag8.

for each artapr where 
artapr.codsoc = wsoc 
no-lock:

do aa = 1 to num-entries( l-mag0 ) :

wmag = entry( aa, l-mag0 ) .
wsoc0 = entry(aa, l-soc0 ) .
if wsoc0 <> wsoc then next.

FOR EACH STOINV where 
    stoinv.codsoc  = artapr.codsoc
and stoinv.articl  = artapr.articl 
and stoinv.magasin = wmag
and stoinv.date-invent >= date(07,01,2015)
and evenement = "INP"
no-lock  use-index primaire:

export delimiter ";" stoinv.

end.

end.

end.

display toto.
