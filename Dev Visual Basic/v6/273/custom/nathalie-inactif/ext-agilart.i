/*==========================================================================*/
/*                           E X T - A G I L A R T  . I                     */
/* Extraction des donnees pour AGIL                                         */
/*--------------------------------------------------------------------------*/
/* Extraction Articles                                                      */
/*==========================================================================*/
/*================================================================================*/
/*    Modifications                                                               */
/* VLE 27102009 ne plus autoriser la vente des anciens ch�ques cadeau dans Agil   */
/* VLE 16112010 les articles ayant une taxe CVO ou DIA sont non vendables AGIL    */
/* VLE 07072011 On met le libelle long de Deal dans le libelle court Agil qui     */ 
/*              est imprime sur le ticket                                         */  
/* VLE 27062012 les produits agricoles ne sont plus remisables dans Agil          */
/* VLE 07012013 gestion de l'indicateur remise exceptionnelle interdite           */
/* VLE 16012013 gestion de l'indicateur remise exceptionnelle ds ext-agil1.p      */
/* VLE 29042013  mise en place de la taxe eco mobilier                            */
/* VLE 11062013  ajout d'une propri�t� article pour les aniamaux vivants          */
/* VLE 22102013 traitement des nouveaux codes tva                                 */   
/*================================================================================*/
/*----------------------------------------*/                      
/*  Ecriture du d�tail de section Modeles */
/*----------------------------------------*/ 

ID_MODELE = caps(trim(artapr.articl)).            
/*  recherche si le produit est un produit agricole pour pouvoir le diff�rencier dans le libelle */
find tabges-agr where tabges-agr.codsoc = soc-groupe
    and   tabges-agr.etabli = ""
    and   tabges-agr.typtab = "LOI"
    and   tabges-agr.prefix = "IMPETIQ"
    and   tabges-agr.codtab = artapr.articl
no-lock no-error.
if available tabges-agr
    then NM_MODELE = string(artapr.libart1[1], "x(29)") + "$".
    else NM_MODELE = string(artapr.libart1[1], "x(30)").
ID_TVA = substr(artapr.tva, 2, 2).   
ID_UNITE = artapr.type-condit.

/*  Produits rentrant dans le calcul du nombre d'articles sur le ticket de caisse */
/*  QTE = Oui,  ZERO = Non   */
TY_MAJ_NB_ART = "QTE".

/*  Produits rentrant dans le calcul ddes statistiques */
/*  QTE = Oui,  ZERO = Non   */
TY_MAJ_STAT = "QTE".

/* Propri�t� mod�le sp�cifique pour la cr�ation des op�rations commerciales   */
If (artapr.famart >= "001" and artapr.famart <= "010") or
    artapr.famart = "022"  
        then ID_PRO_MODELE0 = "GP".
        else ID_PRO_MODELE0 = "AUT".
   
ID_PRO_MODELE1 = artapr.famart2.
ID_PRO_MODELE2  = artapr.famart2 + artapr.soufam2.
ID_PRO_MODELE3 = artapr.famart2 + artapr.soufam2 + artapr.sssfam2.
ID_PRO_MODELE4 = "".

/*----------------------------------------------------------------------------------*/
/* on r�cup�re la provenance de la soci�t� 01 sinon on prend celle de la soci�t� 08 */
/*----------------------------------------------------------------------------------*/
do ind-soc = 1 to num-entries( l-soc-article ) :
	find artapr-soc where artapr-soc.codsoc = entry( ind-soc, l-soc-article )
        and artapr-soc.articl = artapr.articl
        no-lock no-error.
        
        if not available artapr-soc
           then next.
           
        do i = 1 to 100 :
           if artapr.provenance = indic-provenance[i]
                then do : 
                    ID_PRO_MODELE4 = code-provenance[i].
                    leave.
                end.
        end.   
end.   /*  boucle sur soci�t� article   */              


put stream agil unformatted 
                        Type_Ligne_Det ID_MODELE "~011" NM_MODELE "~011" ID_TVA "~011" ID_GUELTE "~011" ID_UNITE "~011"         
                        TY_MAJ_STOCK "~011" TY_MAJ_NB_ART "~011" TY_MAJ_STAT "~011" FL_REMISE "~011" 
                        ID_PRO_MODELE0 "~011" ID_PRO_MODELE1 "~011" 
                        ID_PRO_MODELE2 "~011" ID_PRO_MODELE3 "~011" ID_PRO_MODELE4 "~015" skip. 

/*-----------------------------------------*/                     
/*  Ecriture du d�tail de section Articles */
/*-----------------------------------------*/ 

ID_ARTICLE = caps(trim(artapr.articl)).            
LB_COURT = string(artapr.libart1[1], "x(27)").
LB_LONG = string(artapr.libart1[1], "x(45)").

CD_TRAIT = " ".
If substr(artapr.articl, 7, 1) = "S" or                                         /*  articles supprimes  */
   artapr.caract[3] <> " " or                                                   /*  articles non destines � la vente  */
   lookup(artapr.articl-lie, l-article-cvo) <> 0 or                             /*  articles CVO  */
   artapr.famart2 = "P94" or                                                    /*  familles et sous-familles non traitees  */
   artapr.famart2 = "P83" or
   artapr.famart2 = "P86" or
   artapr.famart2 = "P87" or
   artapr.famart2 = "9RE" or
   artapr.soufam2 = "098" or
   (artapr.famart2 = "P85" and artapr.soufam2 = "030") or                       /* VLE 27102009 anciens ch�ques cadeau  */
   (artapr.famart2 = "9AU" and artapr.soufam2 = "099") or
   (artapr.soufam2 = "097" and artapr.sssfam2 <> "01") or
   (artapr.code-blocage = "B" or artapr.code-blocage = "F") or                  /*  articles bloques */
   artapr.type-client = "PRO" or                                                /* Produits dangereux seulement pour professionnnel */
   artapr.usto = "BX" or                                                        /* produits conditionn�s en box  */
   lookup(artapr.tva, l-tva-interdit) <> 0 or                                                        /* prestations de transport   */
   produit-eaj = no                                                             /* Produits dangereux seulement pour professionnnel */
   then 
        CD_TRAIT = "NS".  

if artapr.famart = "006" and
   lookup(artapr.soufam, l-animalerie) <> 0
   then   ID_PRO_ARTICLE_ANV = "1".   											/* top animalerie vivante  */
		
/* Debut VLE 16112010  */		
/* Recherche s'il existe une taxe DIA dans ce cas le produit est non vendable Agil  */
do ind-soc = 1 to num-entries( l-soc-article ) :    
    find first tarcli-tax 
		where tarcli-tax.codsoc = entry( ind-soc, l-soc-article )
        and   tarcli-tax.motcle = "VTE"
        and   tarcli-tax.typaux = "TAX"
        and   tarcli-tax.codaux = ""
        and   tarcli-tax.articl = artapr.articl                       
        and   tarcli-tax.code-tarif = "DIA"
        and   tarcli-tax.chrono = 0
    no-lock no-error.
    if not available tarcli-tax
        then next.
        else do:
			CD_TRAIT = "NS".
            leave.
        end.
end. /* boucle sur soci�t� */

/* Recherche s'il existe une taxe CVO dans ce cas le produit est non vendable Agil  */
do ind-soc = 1 to num-entries( l-soc-article ) :    
    find first tarcli-tax 
		where tarcli-tax.codsoc = entry( ind-soc, l-soc-article )
            and   tarcli-tax.motcle = "VTE"
            and   tarcli-tax.typaux = "TAX"
            and   tarcli-tax.codaux = ""
            and   tarcli-tax.articl = artapr.articl                       
            and   tarcli-tax.code-tarif = "CVO"
            and   tarcli-tax.chrono = 0
    no-lock no-error.
    if not available tarcli-tax
        then next.
        else do:
			CD_TRAIT = "NS".
            leave.
        end.
end. /* boucle sur soci�t� */
/* Fin VLE 16112010  */	

ID_MODELE = caps(trim(artapr.articl)).    
put stream temp_art unformatted
                        Type_Ligne_Det ID_ARTICLE "~011" LB_COURT "~011" LB_LONG "~011" CD_TRAIT "~011"                    
                        ID_MODELE "~011" FL_GENERIQUE "~011" ID_PRO_ARTICLE_D3E "~011" 
						ID_PRO_ARTICLE_RPD "~011" ID_PRO_ARTICLE_MOB "~011" ID_PRO_ARTICLE_ANV "~015" skip. 