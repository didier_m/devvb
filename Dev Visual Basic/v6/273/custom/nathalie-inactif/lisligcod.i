	
find entete 
where  entete.codsoc = lignes.codsoc
and entete.motcle = lignes.motcle
and entete.typcom = lignes.typcom
and entete.numbon=  lignes.numbon
no-lock.
if not available entete then do:
	message "Entete inexistant " lignes.typcom lignes.numbon . pause.
	next.
end.

wmag = "".
wtypbon = "".
do aa = 1 to 5:
	wadres [aa] = "".
end.

wcodcli = "".

FIND LIGSPE  
where ligspe.codsoc    = lignes.codsoc
and   ligspe.motcle    = lignes.motcle + "/BED"
and   ligspe.typcom    = lignes.typcom
and   ligspe.numbon    = lignes.numbon
and   ligspe.chrono    = lignes.chrono
and   ligspe.ss-chrono = 0
no-lock  no-error .
if not available ligspe  then do:
	message "Commande client Direct" lignes.typcom lignes.numbon 
			lignes.codaux entete.adres[1]. pause 5.
	next.
end.

/* 
message "CCO " ligspe.alpha-3 " " ligspe.alpha-4. pause.
*/
FIND b-ENTETE  
where b-entete.codsoc = ligspe.alpha-1
and   b-entete.motcle = ligspe.alpha-2
and   b-entete.typcom = ligspe.alpha-3
and   b-entete.numbon = ligspe.alpha-4
no-lock  no-error .
if available b-entete then  do: 
    wmag = b-entete.magasin. 
    wtypbon = b-entete.typbon. 
    do aa = 1 to 5:
        wadres [aa] = b-entete.adres[aa].
    end.
    wcodcli = b-entete.codaux.   
end.
if lookup ( wmag , wlislivmag ) = 0 then next.

wjliv = "".
if lookup ( wmag ,lislivmag [1] ) <> 0 then wjliv = "Mardi" .
if lookup ( wmag ,lislivmag [2] ) <> 0 then wjliv = "Mercredi" .
if lookup ( wmag ,lislivmag [3] ) <> 0 then wjliv = "Jeudi" .
if lookup ( wmag ,lislivmag [4] ) <> 0 then wjliv = "Vendredi" .
/* lecture ligne cdc de la SD */
wcdcpub = 0.
wcdcpun = 0.

find b-ligcdcsd 
where b-ligcdcsd.codsoc = ligspe.alpha-1 
and   b-ligcdcsd.motcle = ligspe.alpha-2 
and   b-ligcdcsd.typcom = ligspe.alpha-3 
and   b-ligcdcsd.numbon = ligspe.alpha-4
and   b-ligcdcsd.chrono = ligspe.int-1     
no-lock no-error.
if available b-ligcdcsd then do:
	wcdcpub = b-ligcdcsd.pu-brut.
	wcdcpun = b-ligcdcsd.pu-net.
end.

/* RELIQUAT */
if lignes.fac-httot = 1 then wrel = "N".
else if lignes.fac-httot = 0 then wrel = "O".
     else wrel = " ".
if lignes.top-livre <> "1" 
   then wqteral = lignes.qte[1] - lignes.qte[2].
   else wqteral = 0.

wpoids-cde  = lignes.qte[1] * artapr.pds.
wpoids-liv  = lignes.qte[2] * artapr.pds.
wpoids-ral  = wqteral * artapr.pds.

wcodfou = "".
wnomfou = "".
wartfouA = "".
wartfouU = "".

find artbis 
where artbis.motcle = "ACH"         
and   artbis.codsoc = lignes.codsoc
and   artbis.typaux = "TIE"         
and   artbis.codaux = "    811000"
and   artbis.articl = lignes.articl 
no-lock no-error.
if available artbis then wartfouA = artbis.articl-fou.
                    else wartfouA = "non reference AREA".
					
find artbis 
where artbis.motcle = "ACH"         
and   artbis.codsoc = lignes.codsoc
and   artbis.typaux = "TIE"         
and   artbis.codaux = "    811999"
and   artbis.articl = lignes.articl 
no-lock no-error.
if available artbis then wartfouU = artbis.articl-fou.
                    else wartfouU = "non reference UTA".


   
/* CCO le 01/10/2012  */
wqtecdef  = 0.
wqtecdefm = 0.
for each magasi 
where magasi.codsoc  = "" 
and   magasi.societe = lignes.codsoc 
no-lock:
	find stocks 
	where stocks.codsoc    = lignes.codsoc
	and stocks.articl    = lignes.articl
	and stocks.magasin   = magasi.codtab
	and stocks.evenement = ""
	and stocks.exercice  = ""
	no-lock no-error.
	if available stocks then 
		wqtecdef = wqtecdef + stocks.qte[3].

	if available stocks and  stocks.magasin = lignes.magasin then 
		wqtecdefm = wqtecdefm + stocks.qte[3].

end.

export stream file1 delimiter ";" 
	lignes.magasin 
	wmag
	lignes.numbon 
	entete.ref-magasin format "x(15)"
	lignes.datbon
	lignes.datdep
	artapr.famart artapr.soufam artapr.sssfam 
	lignes.articl lignes.libart format "x(30)" 
	lignes.qte[1] format ">>>>>9-"
	lignes.qte[2] format ">>>>>9-"
	wqteral format ">>>>>9-"
	artapr.pds
	wpoids-cde
	wpoids-liv
	wpoids-ral
	entete.edition
	entete.env-plat
	lignes.qte-tarif
	artapr.provenance
	wartfouA
	wartfouU
	wqtecdefm
    wjliv
	skip
.