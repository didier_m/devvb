/*==========================================================================*/
/*                           E X T - S T A T M A G S P . P                  */
/* Creation table STARTI pour les statistiques sous Magasins                */
/*--------------------------------------------------------------------------*/
/* Saisie Parametres                                                        */
/*==========================================================================*/

{ connect.i        }
{ aide.i           }
{ ext-statmag0.i   }
{ compo.f    "new" }

/*--------------------------------------------------------------------------*/
/*                         Definition de variables                          */
/*--------------------------------------------------------------------------*/

DEF VAR valeur AS CHAR.

/*--------------------------------------------------------------------------*/
/*                         Recup. date de traitement                        */
/*--------------------------------------------------------------------------*/

{ ext-statmagdat.i "no-lock" }

/*--------------------------------------------------------------------------*/
/*                             Saisie zones                                 */
/*--------------------------------------------------------------------------*/

assign compo-vecteur = "<01>,1,2,3"
       compo-bloque  = no.

DISPLAY l-magasins
        date-debut
        date-fin  with frame fr-saisie.

SAISIE :
repeat :

    { compo.i }
    /*----------------------------*/
    /*     Magasins a Traiter     */
    /*----------------------------*/
    
    repeat while compo-motcle = 1 with frame fr-saisie :
    if l-magasins = ""
    then l-magasins = libelle[06].
    
    { fncompo.i l-magasins fr-saisie }
    { z-cmp.i   l-magasins   "' '"   }  
    
    if l-magasins = ""
    then do :
        bell. bell.
        message libelle[08].
        readkey pause 2.
        undo, retry.
    end.
    
    do ind = 1 to num-entries(l-magasins) :
        valeur = entry(ind, l-magasins).
        if lookup(valeur, libelle[06]) = 0
        then do :
            bell. bell.
            message valeur libelle[07].
            readkey pause 2.
            undo, retry.
        end.
    end.
            
    l-magasins = caps(l-magasins).
    DISPLAY l-magasins. 

    next saisie.

end. /* repeat : 1 */

/*--------------------------------------------------------------------------*/
/*                             Gestion de la Date de debut                  */
/*--------------------------------------------------------------------------*/

repeat while compo-motcle = 2 with frame fr-saisie :
    
    { fncompo.i date-debut fr-saisie }
    { z-cmp.i date-debut "' '"       }  
    
    if date-debut = ?
    then do :
        bell. bell.
        message date-debut libelle[08].
        readkey pause 2.
        undo, retry.
    end.

    if date-debut > today
    then do :
        bell. bell.
        message date-debut libelle[09].
        readkey pause 3.
        undo, retry.
    end. /* fin du do du dessus */    

    NEXT SAISIE.    

end.  /*     repeat 2    */

/*--------------------------------------------------------------------------*/
/*                             Gestion de la Date de FIN                    */
/*--------------------------------------------------------------------------*/

repeat while compo-motcle = 3 with frame fr-saisie :

    { fncompo.i date-fin fr-saisie }
    { z-cmp.i   date-fin  "' '"    } 
        
    if date-fin = ?
    then do :
         bell. bell.
         message date-fin libelle[07].
         readkey pause 3.
    undo, retry.
    end.
    
    if date-fin > today
    then do :
         bell. bell.
         message date-debut libelle[09].
         readkey pause 3.
         undo, retry.
    end. /* fin du do du dessus */     
                
    IF date-debut > date-fin
    then do :
    bell. bell.
    message date-fin libelle[10].
    readkey pause 3.
    undo, retry.
    end. /* fin du DO du dessus */
                                                    
    NEXT SAISIE.
                                                        
end.  /*     repeat 3    */

   end.  /* SAISIE-GENERALE */