/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/src/ex-mjfre.p                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 15175 21-08-06!Bug!cch            !Compl�ments corrections suite fiche 15165                             !
!V52 15165 10-08-06!Bug!cch            !Correction probleme zone date DTEXT utilisee au lieu de DTBON         !
!V52 15055 12-07-06!Evo!cch            !Acc�s ARTAPR avec soci�t� de regroupement suite mise en place ED      !
!V52 14705 26-04-06!Evo!cch            !Suppression contr�le par rapport � la date arr�te vente               !
!V52 14335 20-02-06!Evo!cch            !Ajout d'un statut de cl�ture manuel sp�cifique aux VENTES             !
!V52 13247 07-09-05!Evo!cch            !Ajout option pour calcul de LIGNES.HTNET                              !
!V52 11538 02-12-04!Bug!cch            !Correction probl�mes suite migration en V52                           !
!V52 11260 08-11-04!Evo!cch            !Adaptation interfaces pour passage en V52                             !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !tabges                                          !
!varlect.i             !                                     !entete                                          !
!ex-depot.i            !                                     !lignes                                          !
!majsto-0.i            !new                                  !proglib                                         !
!x-calr.i              !new                                  !artapr                                          !
!ex-liglu.i            !                                     !artbis                                          !
!regs-cha.i            !                                     !                                                !
!regs-inc.i            !"articl"                             !                                                !
!ex-rejet.i            !                                     !                                                !
!ex-date.i             !dtbon x-datbon                       !                                                !
!majmoucb.i            !entete                               !                                                !
!x-defaut.i            !                                     !                                                !
!l-articl.i            !                                     !                                                !
!majmoucb.i            !lignes                               !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*-------------------------------------------------------------------------*/
/* Extractions des infos prodepot                                          */
/* Construit a partir du dossier GCBREPRO                                  */
/* MAJ des fichiers                                                        */
/* EX-MJFRE.P        Traitement des mvts de freinte                        */
/*-------------------------------------------------------------------------*/
{ connect.i }
{ varlect.i }
{ ex-depot.i new}
{ majsto-0.i new }
{ x-calr.i new }

{ bat-ini.i  "new" }

def var wfamart          as char format "x(3)".
def var wsoufammin          as char format "x(3)".
def var wsoufammax          as char format "x(3)".
def var wmaj            as log.
def var sup-soc          as char format "x(2)".
def var sup-bon          as char format "x(10)".

def input parameter zfichier as char format "x(12)" .

run bat-lec.p ( zfichier ) .
{ bat-cha.i "wfamart"      "wfamart"            "a" }
{ bat-cha.i "wsoufammin"      "wsoufammin"            "a" }
{ bat-cha.i "wsoufammax"      "wsoufammax"            "a" }
{ bat-cha.i "wmaj"         "wmaj"               "l" }
{ bat-cha.i "sup-soc"      "sup-soc"            "a" }
{ bat-cha.i "sup-bon"      "sup-bon"            "a" }


def var wfamart-min      as char no-undo.
def var wfamart-max      as char no-undo.
def var wsoufam-min      as char no-undo.
def var wsoufam-max      as char no-undo.

def var wnomfic as char.
def var x-datbon   as date no-undo .
def var articl-soc as char no-undo .
def var codsoc-sav as char no-undo .
def var x-numbon like entete.numbon                no-undo.
def var l-soc as char initial "01,08,43"     no-undo. 
def var l-mag as char                              no-undo.
def var l-art as char                              no-undo.
def var wart  as char                              no-undo.
def var wmag  as char                              no-undo.
def var i     as int                               no-undo.
def var j     as int                               no-undo.
def var k     as int                               no-undo.
def var wqte  as deci                              no-undo.
def var nolig as int                               no-undo.
def var topentete as log                           no-undo.
def var z-message as char                          no-undo.    
def var wusto as char extent 2                     no-undo.
def var soc-tabges like tabges.codsoc              no-undo.

def var compteur  as int initial  9999             no-undo.
def var topmaj    as log                           no-undo.

def buffer dgr-entete for entete.
def buffer dgr-lignes for lignes.

def var nb-tabgeslus  as int.
def var nb-dejaok     as int.
def var nb-bon        as int.
def var nb-rejet      as int.
def var nb-traite     as int.
 
topmaj = yes.

message " MISE � JOUR => " topmaj .                            
if wfamart <> " " then
  assign wfamart-min = wfamart
         wfamart-max = wfamart.
else
  assign wfamart-min = "001"
         wfamart-max = "099".

if wsoufammin <> " " then
  assign wsoufam-min = wsoufammin.
else
  assign wsoufam-min = "001".

if wsoufammax <> " " then
  assign wsoufam-max = wsoufammax.
else
  assign wsoufam-max = "999".

                                                    
GENERAL :
do i = 1 to num-entries(l-soc) on error undo , leave :

  codsoc = entry(i,l-soc).

  wnomfic = "crergs.rejet." + codsoc + "." + string(today,"999999")  + string(time,"hh:mm:ss") + ".dgr".
  wnomfic = replace(wnomfic,":","").
  output stream kan-rej to value(wnomfic).
  message "Fichier rej => " wnomfic.
  
  wnomfic = "crergs.log." + codsoc + "." + string(today,"999999")  + string(time,"hh:mm:ss") + ".dgr".
  wnomfic = replace(wnomfic,":","").
  output stream kan-log to value(wnomfic).
  message "Fichier log => " wnomfic.
  
  assign ok-maj-bon  = no 
         codsoc-soc  = codsoc
         niveau      = 2
         ent-motcle  = "VTE"
         ent-typcom  = "RGS" 
         date-arrete = today - 1.

  FIND LAST TABGES 
  where tabges.codsoc      = codsoc-soc
  and   tabges.etabli      = ""
  and   tabges.typtab      = "STO"
  and   tabges.prefix      = "CALEND"
  and   tabges.nombre[ 1 ] = 1
  no-lock no-error .

  if available tabges then date-arrete = tabges.dattab[ 1 ] .  

  FIND TABGES 
  where tabges.codsoc = codsoc-soc
  and   tabges.etabli = ""
  and   tabges.typtab = "STO"
  and   tabges.prefix = "DAT-ARRINV"
  and   tabges.codtab = ""
  no-lock no-error .

  if available tabges and tabges.dattab[ 1 ] > date-arrete then 
    date-arrete = tabges.dattab[ 1 ] .

  regs-app = "NATHALIE" .
  { regs-cha.i }

  { regs-inc.i "articl" }
  articl-soc = regs-soc .

  l-mag = "".
  for each magasi 
  where magasi.societe = codsoc
  no-lock:
    l-mag = l-mag + magasi.codtab + ",".
  end.  


  l-mag = substr(l-mag,1,length(l-mag) - 1).

  /*  boucle magasin */
  
  do j = 1 to num-entries(l-mag):

    wmag = entry(j,l-mag).
    message "JE TRAITE LE MAGASIN " wmag " DE LA SOCIETE : " codsoc.

    assign nb-tabgeslus = 0
           nb-dejaok    = 0
           nb-bon       = 0
           nb-traite    = 0
           nb-rejet     = 0.

    
            x-datbon = today.  


    /* Verification par rapport a l'arrete */                                         
    if x-datbon <= date-arrete then x-datbon = date-arrete + 1 .                      
    topentete = no.

    if codsoc = "08" then soc-tabges = "08".
    else soc-tabges = "01".
    
    tab:   
    for each tabges
    where tabges.codsoc = soc-tabges
    and   tabges.etabli = ""
    and   tabges.typtab = "ART"
    and   tabges.prefix = "NEW-CODIF"
    and   substring(tabges.libel1[2], 1, 3) >= wfamart-min 
    and   substring(tabges.libel1[2], 1, 3) <= wfamart-max 
    and   substring(tabges.libel1[2], 5, 3) >= wsoufam-min 
    and   substring(tabges.libel1[2], 5, 3) <= wsoufam-max
    and   index(tabges.libel3[13],codsoc) = 0
    no-lock :

      nb-tabgeslus = nb-tabgeslus + 1.
      
      find  stocks
      where stocks.codsoc = codsoc
      and   stocks.magasin = wmag
      and   stocks.evenement = ""
      and   stocks.exercice = ""
      and   stocks.articl = tabges.codtab 
      and   stocks.qte[2] <> 0
      exclusive-lock no-error.
      if not available stocks then next tab.
      if stocks.opecre matches "*ED*"  then do:
        nb-dejaok = nb-dejaok + 1.
        next tab.
      end.
      l-art = tabges.codtab + ',' + string(tabges.libel1[1] ,"x(6)").
              /* Chargement des zones de articl et de artbis */

      assign codsoc-sav = codsoc-soc
             codsoc-soc = articl-soc.

      assign wusto[1] = ""
             wusto[2] = "".

      do k = 1 to 2:
        wart = entry(k, l-art).
        codtab     = wart.
        find artapr 
        where artapr.codsoc = articl-soc
        and   artapr.articl = codtab
        no-lock no-error .

        if not available artapr then do :
          nb-rejet = nb-rejet + 1.
          x-message = codsoc-sav + " " +  
                      wart + " : Article Inexistant sur Societe " + articl-soc + 
                      "---" + l-art.
                      
          { ex-rejet.i }
          undo , next tab. 
        end .
        wusto[k] = artapr.usto.
      end.

      if wusto[1] <> wusto[2] then do:
        nb-rejet = nb-rejet + 1.
        z-message = "*** attention unites differents sur " + codsoc 
                  + "ancien code = " + tabges.codtab + " usto = "   
                  + wusto [1]
                  + "  <>  nouveau code = " + tabges.libel1[1] + " usto = "   
                  + wusto [2] 
                  .
        put stream kan-log unformatted z-message skip.
      end.

      codsoc-soc = codsoc-sav.
                  
      if topentete = no then do:

        if topmaj = yes then do:
          x-numbon = "" .
          run nocom.p ( ent-typcom , x-datbon , 2 , output x-numbon ) .
          x-numbon = x-numbon + "00" .
        end.
        else do:        
          compteur = compteur + 1.
          x-numbon = string(compteur,"99999999") + "00".
        end.
                
        /* Entete */
        FIND dgr-entete 
        where dgr-entete.codsoc = codsoc-soc
        and   dgr-entete.motcle = ent-motcle
        and   dgr-entete.typcom = ent-typcom
        and   dgr-entete.numbon = x-numbon
        exclusive-lock no-error .

        if available dgr-entete then do:
         
          x-message = " Bon deja existant " + ent-motcle + " " + ent-typcom +
                      " " + x-numbon + " sur " + codsoc-soc.
          { ex-rejet.i }
          undo , leave general .
        end .
        nb-bon = nb-bon + 1.
        
        z-message = " je cree l'entete " + codsoc + " "
                  + wmag + " " + x-numbon + " " + string(niveau) .         

        Put stream kan-log unformatted z-message skip.
        
        CREATE dgr-entete .        

        assign dgr-entete.codsoc      = codsoc-soc 
               dgr-entete.motcle      = ent-motcle 
               dgr-entete.typcom      = ent-typcom
               dgr-entete.numbon      = x-numbon
               dgr-entete.datdep      = x-datbon  
               dgr-entete.dattar      = x-datbon  
               dgr-entete.ref-magasin = ""
               dgr-entete.ref-tiers   = ""
               dgr-entete.typaux      = ""        
               dgr-entete.codaux      = "" 
               dgr-entete.devise      = "EUR"
               dgr-entete.txdev       = 1
               dgr-entete.magasin     = wmag
               dgr-entete.rec-ase     = "" 
               dgr-entete.transp      = ""
               dgr-entete.modliv      = ""
               dgr-entete.nivcom      = "4"
               dgr-entete.top-modif   = "1"
               dgr-entete.edition     = "val"
               dgr-entete.datedi[ 1 ] = today
               dgr-entete.opedi[ 1 ]  = "BDG"
               entete-recid       = recid( dgr-entete )
               dgr-entete.typbon      = "8"
               topentete = yes
               nolig = 0.

        { majmoucb.i dgr-entete }
      end. /* if topentete = no ... */

      find dgr-entete
      where recid(dgr-entete) = entete-recid
      exclusive-lock no-error.

      wqte = stocks.qte[2].


      do k = 1 to 2:
        wart = entry(k,l-art).
        codtab     = wart.
        find artapr 
        where artapr.codsoc = articl-soc
        and   artapr.articl = codtab
        no-lock no-error .

        nolig = nolig + 1.
        
        CREATE dgr-lignes .
  
        /* Chargement des zones de lignes identiques a l'entete */

        assign dgr-lignes.codsoc    = dgr-entete.codsoc
               dgr-lignes.motcle    = dgr-entete.motcle
               dgr-lignes.typcom    = dgr-entete.typcom   
               dgr-lignes.numbon    = dgr-entete.numbon   
               dgr-lignes.typaux    = dgr-entete.typaux   
               dgr-lignes.codaux    = dgr-entete.codaux
               dgr-lignes.magasin   = dgr-entete.magasin  
               dgr-lignes.magasin-2 = dgr-entete.magasin-2  
               dgr-lignes.datbon    = dgr-entete.datbon    
               dgr-lignes.datdep    = dgr-entete.datdep      
               dgr-lignes.datech    = dgr-entete.datech
               dgr-lignes.codech    = dgr-entete.codech
               dgr-lignes.codpri    = dgr-entete.codpri
               dgr-lignes.codlig    = "N"
                 .                                                              



        assign dgr-lignes.chrono      = nolig
               dgr-lignes.articl      = wart
               dgr-lignes.txdev       = dgr-entete.txdev
               dgr-lignes.nivcom      = "3"
               dgr-lignes.typbon      = "8"
               dgr-lignes.mvtsto[ 1 ] = dgr-entete.modliv.


        assign dgr-lignes.tenusto       = artapr.tenusto
               dgr-lignes.usto          = artapr.usto
               dgr-lignes.ttva          = artapr.tva
               dgr-lignes.atelier       = artapr.atelier
               dgr-lignes.pdsuni        = artapr.pds
               dgr-lignes.libart        = artapr.libart1[ 1 ]
               dgr-lignes.imput         = artapr.impvte
               dgr-lignes.qte[ niveau ] = wqte.

        FIND ARTBIS 
        where artbis.motcle = "VTE"         
        and   artbis.codsoc = articl-soc    
        and   artbis.articl = dgr-lignes.articl 
        and   artbis.typaux = ""            
        and   artbis.codaux = "" 
        no-lock no-error .

        if available artbis then 
          assign dgr-lignes.ufac     = artbis.ufac
                 dgr-lignes.ucde     = artbis.ucde
                 dgr-lignes.coef-cde = artbis.coef-cde
                 dgr-lignes.coef-fac = artbis.coef-fac
                          .
        else 
          assign dgr-lignes.ufac     = "PI"
                 dgr-lignes.ucde     = "PI"
                 dgr-lignes.coef-cde = 1
                 dgr-lignes.coef-fac = 1 .

        if dgr-lignes.coef-cde > 0 then 
          dgr-lignes.qte[ niveau ] = dgr-lignes.qte[ niveau ] / dgr-lignes.coef-cde .
        else 
          dgr-lignes.qte[ niveau ] = dgr-lignes.qte[ niveau ] * ( - dgr-lignes.coef-cde ) .

        /* Calcul des prix brut , prix net et chargement des remises */
        assign dgr-lignes.top-prix = "1"
               dgr-lignes.top-ttc  = "1"
               dgr-lignes.fac-ht   = 0
               dgr-lignes.fac-ttc  = 0
               dgr-lignes.pu-brut  = 0
               dgr-lignes.pu-net   = 0.


        if k = 2 then dgr-lignes.qte[ niveau ] = - dgr-lignes.qte[ niveau ]. 

        if topmaj = yes then do:
          if dgr-lignes.tenusto = "1" and dgr-lignes.qte[ niveau ] <> 0 then do :

            /*  400 =  +  dans Entree Regul. Sortie */
            assign majsto-magasin = dgr-lignes.magasin
                   majsto-articl  = dgr-lignes.articl
                   majsto-mvtsto  = "400"
                   majsto-date    = dgr-lignes.datdep
                   majsto-qte     = dgr-lignes.qte[ niveau ].

            if dgr-lignes.coef-cde < 0 then 
              majsto-qte = majsto-qte / ( - dgr-lignes.coef-cde ) .
            else 
              majsto-qte = majsto-qte * dgr-lignes.coef-cde .

            run majsto-0.p .
  
          end.  
        end.

                     
         z-message = " je cree la ligne " + string(dgr-lignes.chrono,">>>9") + " " 
                  + string(dgr-lignes.articl,"x(6)")  + " " 
                  + string(artapr.libart1[1], "x(35)")
                  + " " + string(dgr-lignes.qte[2],">>>>9-") 
                  + "   tenusto = " + dgr-lignes.tenusto.
 
        put stream kan-log unformatted z-message skip.

        { majmoucb.i dgr-lignes }

      end . /* do k = 1 to 2 ... */

      nb-traite = nb-traite + 1.

      if topmaj = yes then 
        assign stocks.opecre = stocks.opecre + "ED"
               .
      
    end. /* for each tabges ... */
    message "SOCIETE : " codsoc "   MAGASIN => " wmag.
    message " NOMBRE TABGES LUS => " nb-tabgeslus .
    message " NOMBRE DEJA OK    => " nb-dejaok    .
    message " NOMBRE REJETS     => " nb-rejet     .
    message " NOMBRE TRAITE     => " nb-traite    .
    message " NOMBRE BONS CREES => " nb-bon       .


  END . /* do j= 1 to num-entries (l-mag) */
  if topmaj = yes then do:
    for each tabges
    where tabges.codsoc = soc-tabges
    and   tabges.etabli = ""
    and   tabges.typtab = "ART"
    and   tabges.prefix = "NEW-CODIF"
    and   substring(tabges.libel1[2], 1, 3) >= wfamart-min 
    and   substring(tabges.libel1[2], 1, 3) <= wfamart-max 
    and   substring(tabges.libel1[2], 5, 3) >= wsoufam-min 
    and   substring(tabges.libel1[2], 5, 3) <= wsoufam-max
    and   index(tabges.libel3[13],codsoc) = 0
    exclusive-lock :
      assign tabges.libel3[13] = tabges.libel3[13] + codsoc + ",".
    end.
  end.

  ok-maj-bon = yes .
  output stream kan-log close.
  output stream kan-rej close.

END . /* REPEAT GENERAL */

