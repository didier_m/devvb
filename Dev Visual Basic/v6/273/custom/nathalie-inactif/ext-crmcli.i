/*==========================================================================*/
/*                           E X T - C R M C L I  . I                       */
/* Extraction des donnees pour VENTES PARTNER                               */
/*--------------------------------------------------------------------------*/
/* Extraction Clients                                                       */
/* DGR 220108 : EUREA COOP                                                  */
/*==========================================================================*/

/*------------------------------------------*/
/*  constitution du libell� de la soci�t�   */
/*------------------------------------------*/
CASE codsoc-trait[nsoc]:
  when "01" then
     Reseau = "EUREA COOP".
/*
  when "05" then
     Reseau = "AGRICEL 43".    
*/
  when "08" then
     Reseau = "ASEC".
  when "30" then
     Reseau = "EURENA".
  when "41" then
     Reseau = "ALIREA".
  when "42" then
     Reseau = "FOREZ GRAINS".
  when "43" then
     Reseau = "PICQ".     
END CASE.     
/*-------------------------------------------*/
/*  initialisation des autres informations   */
/*-------------------------------------------*/
No_Pacage  = " ".
Statut = "Client".
/* Statut_PV = "Client". */
No_client = trim(codsoc-trait[nsoc]) + "/" + substr(auxapr.codaux,5,6).
Nom_Exploit = auxili.adres[1].
if substring(auxili.adres[1],1,3) = "MLE" or
   substring(auxili.adres[1],1,3) = "Mle" or
   substring(auxili.adres[1],1,3) = "MME" or
   substring(auxili.adres[1],1,4) = "MR  " or
   substring(auxili.adres[1],1,4) = "Mr  " or
   substring(auxili.adres[1],1,3) = "MRS" then do:
   Nom_exploit = substring(auxili.adres[1],5,28).
end.
if substring(auxili.adres[1],1,3) = "MR " or
   substring(auxili.adres[1],1,3) = "ME " then do:
   Nom_exploit = substring(auxili.adres[1],4,29).
end.
if substring(Nom_exploit,1,1) = " " then
   Nom_exploit = substring(Nom_exploit,2,27).
Adres_1 = auxili.adres[2].
Adres_2 = auxili.adres[3].
Adres_3 = auxili.adres[4].
Code_Postal = substr(auxapr.adres[5],1,5).
Commune = substr(auxapr.adres[5],7,26). 
Telephone = auxapr.teleph.
Fax = auxapr.fax.
TC1 = auxapr.repres[1].
TC2 = auxapr.repres[2].
TC3 = auxapr.repres[3].
Code_Mag = auxapr.magasin.
Tarif = auxapr.code-tarif.
Fiabilite = auxapr.contentieux.
Distributeur = " ". 
Ptf_TC1 = auxapr.code-edition[5].
Ptf_TC2 = auxapr.code-edition[6].
Ptf_TC3 = auxapr.code-edition[7].
Segmentation = "Relationnel".
find tabges
where tabges.codsoc = ""
and   tabges.etabli = ""
and   tabges.typtab = "CLI"
and   tabges.prefix = "typ-segcrm"
and   tabges.codtab = auxapr.code-edition[4]
no-lock no-error.
If available tabges then 
  Segmentation = tabges.libel1[1].

/*------------------------------------------*/
/*  ecriture dans le fichier crm-cli        */
/*------------------------------------------*/  
export stream crm-cli delimiter ";"
       No_Pacage Reseau Statut No_client Nom_Exploit
       Adres_1 Adres_2 Adres_3 Code_Postal Commune Telephone 
       Fax TC1 TC2 TC3 Code_Mag Tarif Fiabilite Distributeur
       Ptf_TC1 Ptf_TC2 Ptf_TC3 Segmentation. 

