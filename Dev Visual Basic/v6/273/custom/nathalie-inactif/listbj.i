/*   modif DGR 191104 pour V52                                                */
/* DGR 05122008 : Modif pour nomenclature 2                                   */

/* Tableau de bord journalier */

/* variables pour societes */
def {1} shared var    magasi-soc as char                   no-undo.
def {1} shared var    type-trt   as char                   no-undo.
def {1} shared var    lib-trt    as char                   no-undo.
def {1} shared var    wcodsoc    as char                   no-undo.
def {1} shared var    wlibsoc    like libsoc-soc           no-undo.
def {1} shared var    wope       as char                   no-undo.
def {1} shared var    wlibope    as char format "x(30)"    no-undo.
def {1} shared var    wdatej     as date format "99/99/9999" no-undo.
def {1} shared var    wdateh     as date format "99/99/9999" no-undo.
def {1} shared var    wdateh1    as date format "99/99/9999" no-undo.
def {1} shared var    wdatem     as date format "99/99/9999" no-undo.
def {1} shared var    wdatea     as date format "99/99/9999" no-undo.
def {1} shared var    wdatederarr as date format "99/99/9999" no-undo.

def {1} shared var    l-typcom   as char                   no-undo.
def {1} shared var    wtypcom    like bastat.typcom        no-undo.
def {1} shared var    wprefix    as char                   no-undo.
def {1} shared var    libelle    as char                 extent 99   no-undo .

def {1} shared var periph       as char format "x(12)"              no-undo .
def {1} shared var periph2      as char format "x(12)"              no-undo .

def {1} shared var z-choix      as char                             no-undo .
def {1} shared var zone         as char                             no-undo .
def {1} shared var sav-periph   as char format "x(12)"              no-undo .
def {1} shared var sav-periph2  as char format "x(12)"              no-undo .
def {1} shared var l-soc as char.

def {1} shared var nb-lus       as int                              no-undo .
def {1} shared var i            as int                              no-undo .
def {1} shared var j            as int                              no-undo .
def {1} shared var k            as int                              no-undo .


/*------------------------------------------------------------------------*/
def {1} shared frame fr-saisie .

form libelle[  1 ] format "x(33)" l-soc   format "x(8)"          skip
     libelle[  2 ] format "x(33)" wope    format "x(4)" wlibope  skip
     libelle[  3 ] format "x(33)" type-trt format "x"  lib-trt format "x(6)" skip
     libelle[  4 ] format "x(33)" wdatej                         skip
     libelle[  5 ] format "x(33)" wdateh                         skip
     libelle[  6 ] format "x(33)" wdatem                         skip
     libelle[  7 ] format "x(33)" wdatea                         skip
     libelle[  8 ] format "x(33)" wdateh1                        skip
     libelle[  9 ] format "x(33)" l-typcom format "x(35)"        skip
     libelle[ 10 ] format "x(33)" periph                         skip
     libelle[ 11 ] format "x(33)" periph2                        skip
     with frame fr-saisie
     row 3  centered  overlay  no-label  { v6frame.i } .