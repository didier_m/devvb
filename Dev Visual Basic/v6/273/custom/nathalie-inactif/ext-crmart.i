/*==========================================================================*/
/*                           E X T - C R M A R T  . I                       */
/* Extraction des donnees pour VENTES PARTNER                               */
/*--------------------------------------------------------------------------*/
/* Extraction Articles                                                      */
/* DGR 220108 : EUREA COOP                                                  */
/* DGR 271211 : TVA                                                         */
/*==========================================================================*/

   Lib_famille       = " ".
   Lib_Ss_Famille    = " " .
   Lib_Ss_Ss_Famille = " ".
   Conditionnement   = " ".
/*-----------------------------------------------*/
/*  Recherche du libelle de la famille article   */
/*-----------------------------------------------*/
   find tabges where 
        tabges.codsoc = " " 
    and tabges.etabli = " " 
    and tabges.typtab = "ART"
    and tabges.prefix = "FAMART"
    and tabges.codtab = artapr.famart
    no-lock no-error.
    
    if available tabges then
       Lib_famille = tabges.libel1[1].
/*----------------------------------------------------*/
/*  Recherche du libelle de la sous famille article   */
/*----------------------------------------------------*/         
    find tabges where
         tabges.codsoc = " "
     and tabges.etabli = " "
     and tabges.typtab = "ART"
     and tabges.prefix = "SOUFAM" + artapr.famart
     and tabges.codtab = artapr.soufam
     no-lock no-error.
              
     if available tabges then
        Lib_Ss_famille = tabges.libel1[1].
/*---------------------------------------------------------*/
/*  Recherche du libelle de la sous sous famille article   */
/*---------------------------------------------------------*/
    find tabges where
         tabges.codsoc = " "
     and tabges.etabli = " "
     and tabges.typtab = "ART"
     and tabges.prefix = "SSFM" + artapr.famart + artapr.soufam
     and tabges.codtab = artapr.sssfam
     no-lock no-error.
              
    if available tabges then
       Lib_Ss_Ss_famille = tabges.libel1[1].
/*------------------------------------------------------------*/
/*  Recherche du libelle du type de conditionnement article   */
/*------------------------------------------------------------*/       
    find tabges where
         tabges.codsoc = " "
     and tabges.etabli = " "
     and tabges.typtab = "ART"
     and tabges.prefix = "TYP-CONDT"
     and tabges.codtab = artapr.type-condit
     no-lock no-error.
              
   if available tabges then
      Conditionnement = tabges.libel1[1].
/*---------------------------------------*/      
/*  Recherche de l'unite de facturation  */
/*---------------------------------------*/
    find artbis where
         artbis.codsoc = articl-soc and
         artbis.motcle = "VTE" and
         artbis.typaux = " " and
         artbis.codaux = " " and
         artbis.articl = artapr.articl
    no-lock no-error.
    if available artbis then do:
       Unite_Vente = artbis.ufac.
       Colisage = artbis.surcondit.
    end.        
/*----------------------------------------------*/      
/*  Initialisations des autres infos            */
/*  le code article est prefixe de la societe   */
/*----------------------------------------------*/       
    Code = trim(codsoc-trait[nsoc]) + "/" + caps(artapr.articl).
    Type = "D".
    If artapr.tenusto = "1" then
       Gere_Stock = "O".
    else 
       Gere_Stock = "N".
    If artapr.code-blocage = "F" or
       artapr.code-blocage = "B" then    
       Code_Blocage = "O".
    else   
       Code_blocage = "N". 


/*---------------------------------------*/      
/*  Recherche de la TVA de l'article     */
/*  modif  DGR: recherche dans TABLES    */
/*---------------------------------------*/
/*
    if tva = "005" then
       Taux_TVA = 19.6.
    else
       Taux_TVA = 5.5.
*/
    assign  Taux_TVA = 5.5.
    Find TABLES
    where tables.codsoc = ""
    and   tables.etabli = ""
    and   tables.typtab = "TAX"
    and   tables.prefix = "TYP-TAXE"
    and   tableS.codtab = artapr.tva
    no-lock no-error.
    if available tables then
      assign  Taux_TVA = tables.nombre[1].

    CASE  codsoc-trait[nsoc]:
      when "01" then
         Reseau-art = "EUREA COOP".
/*
      when "05" then
         Reseau-art = "AGRICEL 43".
*/
      when "08" then
         Reseau-art = "ASEC".
      when "30" then
         Reseau-art = "EURENA".
      when "41" then
         Reseau-art = "ALIREA".
      when "42" then
         Reseau-art = "FOREZ GRAINS".
      when "43" then
         Reseau-art = "PICQ".
    END CASE.   
/*------------------------------------*/       
/* Ecriture dans le fichier crm-art   */
/*------------------------------------*/
    export stream crm-art delimiter ";"
        Code artapr.libart1[1] Taux_TVA 
        artapr.famart Lib_Famille artapr.soufam Lib_Ss_Famille 
        artapr.sssfam Lib_Ss_Ss_Famille
        Conditionnement Type Gere_Stock Code_Blocage
        Unite_Vente
        Colisage Reseau-art.              
       
