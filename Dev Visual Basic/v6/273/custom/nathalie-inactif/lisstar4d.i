/* variables pour edition */
def var i as int.
def var sequence     as char format "x(05)"             no-undo.
def var sel-sequence as char format "x(12)"             no-undo.
def var snomfic      as char format "x(12)"             no-undo.
def var wficlog      as char format "x(12)"             no-undo.

def var wcodsoc as char format 'xx' no-undo.

def var wtype1 as char format "x" no-undo.
def var wtype2 as char format "x" no-undo.
def var wtype2g as char format "x" no-undo.
def var wtype3 as char format "x" no-undo.
def var wtype4 as char format "x" no-undo.
def var wtype4g as char format "x" no-undo.
def var wdatsto as date format "99/99/9999" no-undo.
def var wtypsto as char format "x" no-undo.
def var wlibsto as char format "x(20)" no-undo.
def var l-fam   as char format "x(40)" no-undo.
def var wfam    as char no-undo.
def var i-fam   as int no-undo.

def var wfic1 as char format "x(12)" no-undo.
def var wfic2 as char format "x(12)" no-undo.
def var wfic2g as char format "x(16)" no-undo.
def var wfic3 as char format "x(12)" no-undo.
def var wfic4 as char format "x(12)" no-undo.
def var wfic4g as char format "x(16)" no-undo.

def var wdatdeba as date format "99/99/9999" no-undo.
def var wdatfina as date format "99/99/9999" no-undo.
def var wdatdebv as date format "99/99/9999" no-undo.
def var wdatfinv as date format "99/99/9999" no-undo.

