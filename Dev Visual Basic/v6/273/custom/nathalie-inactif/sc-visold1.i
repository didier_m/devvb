/*---------------------------------------------------------------------------*/
/*                         sc-visold1.i                                      */
/*---------------------------------------------------------------------------*/

lig-affich = string(soldcli.nom    , "x(17)")   + "|" +
             string(soldcli.ble    , "->>>>9")  + "|" +
             string(soldcli.orge   , "->>>>9")  + "|" +
             string(soldcli.tritic , "->>>>9")  + "|" +
             string(soldcli.mais   , "->>>>9")  + "|" +
             string(soldcli.apport , ">>>>>9-") + "|" +
             string(soldcli.sortie , ">>>>>9-") + "|" +
             string(soldcli.remere , ">>>>>9-") .            
           
display soldcli.codaux @ lig-chrono lig-affich  
with frame FR-SCROLX .

lig-affich = "" .
