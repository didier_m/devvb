/****************************************************************************/
/*==========================================================================*/
/*                            E X T - A G I L 1 . P                         */
/* Extraction des donnees pour AGIL                                         */
/*--------------------------------------------------------------------------*/
/* Extraction journali�re des clients, articles, gencod, magasins, tarifs   */
/*                        hierarchie produit, Redevance Pollution Diffuse   */
/*                        cartes fidelite clients et salaries               */
/* Seules les donn�es cr��s ou modifi�es depuis la date de dernier          */
/* traitement sont envoy�es                                                 */
/*==========================================================================*/
/*============================================================================================*/
/*   VLE 21102009  reset des codes barres et envoi de la totalit� des gencod tous les soirs   */
/*   DGR 21122009  arrondi sur PX_ACHAT + tarif CLI et PRO GRP qui ne descendaient pas        */
/*   VLE 20012010  suppression du controle sur la zone type de tiers pour l'envoi des clients */
/*   VLE 06052010  ajout de la gestion des magasins test                                      */
/*   VLE 06052010  boucle sur Auxapr en plus d'Auxili pour la mise � jour des clients         */
/*   DGR 03082010  modif acces tarif agc sur date-debut au lieu de datmaj                     */
/*   DGR 03082010  reinit_tar_pa rajout acces a tarcli-agc avec le code article               */
/*   VLE 06102010  ajout magaasin 523 et suppression des traitements ste 43                   */
/*   VLE 16112010  produit non vendable agil si taxe DIA ou CVO                               */
/*   VLE 20122010  D�sactivation des cartes supprim�es dans Deal                              */
/*   VLE 25012011  blocage des clients dont la date de suppression est renseignee             */
/*   VLE 04022011  Ajout ecriture message dans la log                                         */ 
/*   VLE 17022011  traitement de la societe 59 VITRY                                          */
/*   VLE 24032011  Modification de la date de debut d'application du prix d'achat             */
/*   VLE 18042011  Gestion de la suppression des Gencod pour ne plus envoyer tous les soirs   */
/*                 la totalit� des gencod � Agil                                              */
/*   VLE 04052011  Mise en place nouvelle version Agil, adresse � la norme Afnor              */
/*                 interdiction de se vendre a soi-m�me                                       */
/*                 ne pas g�n�rer de mouvement pour le depot d'Allentin                       */
/*   VLE 18052011  modification acc�s auxili pour Vitry                                       */
/*   VLE 04102011  ajout de l'envoi des points acquis dans les lim                            */
/*   VLE 02012012  suppression de la gestion des PA faite dans ext-agil2.p                    */
/*                 la mise � jour de la date de traitement se fait dans ext-agil2.p           */
/*                 ajout des DO TRANSACTION pour ne pas locker tous les enregistrements       */ 
/*   VLE 16012012  traitement des societ�s 08 et 59 pour les prix d'achat                     */ 
/*   VLE 27062012  les produits agricoles ne sont plus remisables ds Agil                     */
/*   VLE 16012013  gestion des articles remisables                                            */  
/*   VLE 05032013  chgt enseigne CDV pour 407,409,411,508,512,618                             */ 
/*   VLE 08042013  chgt enseigne CDV pour 418                                                 */
/*   VLE 29042013  mise en place de la taxe eco mobilier                                      */
/*   VLE 11062013  ajout de l'envoi des stocks                                                */
/*                 ajout d'une propri�t� article pour les animaux vivants                     */
/*   DGR 12082013  prise en compte des ecarts d'inv pour les stocks                           */
/*                 si trt <> exploit : ecriture dans /applic/li/operat pour eviter            */
/*                 les erreurs  "** Impossible d'ouvrir le fichier : /applic/li/vle/...."     */
/*   VLE 22102013  traitement des nouveaux codes tva                                          */
/*   VLE 02032015  changement enseigne 413,502,518,614                                        */ 
/*   VLE 04082015  ajout des magasins 524 et 525                                              */
/*   VLE 05102015  changement enseigne 515                                                    */
/*   DGR 26102015  batch perio                                                                */
/*   VLE 11012016  traitement de la famille P22 pour les soldes                               */
/*   VLE 02062016  ajout des cartes 398xxx et modification du libelle motif points            */ 
/*   VLE 26012017  envoi des taxes pour tous les produits y compris les non vendables         */
/*                 pour ne pas avoir � traiter de rejet sur le serveur Agil                   */ 
/*   VLE 26062017  enlever le commentaire sur l'envoi des PA                                  */
/*   VLE 10012018  gestion des soldes pour les familles Bxx                                   */
/*   VLE 10042018  ajout du magasin 087                                                       */ 
/*   DGR 09072018  enlever majart00.p (lance dans extraction CYLANDE )                        */ 
/*============================================================================================*/
/************************************************************************/
/*     les fichiers de test sont crees dans applic/li/vle               */
/************************************************************************/
/*==========================================================================*/ 
{ connect.i        }
{ bat-ini.i  "new" }
{ aide.i     "new" }
{ ext-agil0.i "new" }
{ sel.i        NEW }  /* batch perio */

DEFINE VARIABLE cco-datheur     AS CHARACTER   NO-UNDO.
DEFINE VARIABLE cco-fictrace    AS CHARACTER   NO-UNDO.
   
cco-datheur = STRING(YEAR(TODAY),"9999") + STRING(MONTH(TODAY),"99") + STRING(DAY(TODAY),"99") + "-" + STRING(TIME,"hh:mm:ss").
cco-datheur = REPLACE(cco-datheur, ":", "").
cco-fictrace = "/applic/li/travail/ext-agil1.log" + cco-datheur .
OUTPUT TO VALUE(cco-fictrace) .

def buffer tarcli-rpd for tarcli.
def buffer tarcli-tax for tarcli.
def buffer tarcli-agc for tarcli.
def buffer tarcli-maj for tarcli.
def buffer tabges-art for tabges.
def buffer tabges-taux for tabges.
def buffer tabges-sold for tabges.
def buffer tabges-rpd for tabges.
def buffer artapr-soc for artapr.
def buffer artapr-maj for artapr.
def buffer tabges-agr for tabges.
/*-------------------------------------*/
/* Definition des param�tres d'appel   */
/*-------------------------------------*/
def input parameter fichier as char format "x(12)".
def var p-soc               as char.
def var p-typaux            as char.
def var p-codaux            as char.

/*-------------------------------------*/
/* Definition des variables de travail */
/*-------------------------------------*/
def var articl-soc    like artapr.codsoc.
def var auxili-soc    like auxili.codsoc.
def var type-tiers    like auxili.typaux init "TIE".
def var soc-groupe    like artapr.codsoc init "00".
def var auxdet-soc    like auxili.codsoc.
def var warticl       like artapr.articl.
def var exploit          as log .
def var codsoc-trait     as char  extent 7 .
def var heure-debut      as int .
def var nsoc             as int .
def var nom-table        as char .
def var code-table       as char .
def var nb-lus           as int .
def var per-modulo       as int .
def var i                as int. 
def var j                as int.
def var k                as int.
def var ind-srvp         as int.
def var ind-soc          as int.
def var notour           as int.
def var w-prefix         as char.
def var lg-prefix        as int. 
def var l-motcle         as char .
def var l-tarif          as char  extent 5 .
def var type-tarif       as int   extent 5 .
def var code-mag         as char .
def var type-promo       as char .
def var val-tarif        as dec.
def var i-param          as char .
def var o-param          as char .
def var l-fam-gp         as char init "001,002,003,004,005,006,007,008,009,010,022".
def var l-article-cvo    as char init "980635,980636,980637,981104,981105,981387,981392".
def var l-mag-asec       as char init "524,525,801,998".
/* mag fermes avant demarrage Agil ou passes de Asec � GV ayant encore des cartes 291000  */
def var l-mag-ferme      as char init "405,407,409,410,411,414,416,417,418,419,423,506,508,510,511,512,513,514,515,516,521,522,601,618,626,705,802".
def var l-mag-gv         as char init "001,002,003,006,008,009,011,014,015,016,017,018,020,022,025,026,027,028,038,039,057,081,082,083,084,085,086,087,088,089,136,405,407,409,410,411,413,416,417,418,419,423,502,508,511,512,515,516,518,521,523,614,618,626,705,991".
def var l-mag-test	     as char init "991,997,998".
def var l-mag-vitry      as char init "136".
def var l-soc-client     as char init "01,08,59".
def var l-soc-article    as char init "01,08,59". 
def var l-pro-agri       as char init "011,012,013,014,021,030,032".
def var l-animalerie     as char init "091,092,093,094,096".              /* sous familles animalerie vivante */

/* DGR 12082013 */
def var date-invmag      as date.
def var stophy           as dec .


/* la recherche des prix d'achat se fait � partir de la soci�t� 08 et 59 pour le GP, car la 59 contient tous les produits de 01 plus les sp�cifiques 59 */
def var l-soc-pxa        as char init "08,59".   

def var l-soc-cartes-cli as char init "  ,08".
def var soc-cartes-sal   as char init "07".
def var l-type-tiers     as char init "ADH,CLI, ".
def var valeur-rpd       as dec.
def var cd-bassin        as char format "x".
def var taux-tva         as dec extent 20.
def var code-tva         as char extent 20.
def var indtva           as int.  
def var l-tva-interdit   as char init "006,00A,00B".
def var nb-mag           as int extent 3.
def var produit-eaj      as log. 
def var annee            as int format "9999".
def var mois             as int format "99".
def var jour             as int format "99".
def var heure            as char.
def var indic-provenance as char extent 100.
def var code-provenance  as char extent 100.

/* def var code-famille     as char extent 400.
def var code-ssfamille   as char extent 400.*/

def var tx-srvp          as dec.
def var tx-demarq        as dec extent 3.
def var px-achat-ok		 as log.

def var val-prix         as dec.
def var wnom             as char.
def var date-rpd         as date.
def var ind-mkt          as int.
def var ind-sold          as int.
def var nbart			 as int.
def var nbcli			 as int.
def var nbmag			 as int.
def var nbpro			 as int.
def var nbgen			 as int.
def var nbpts    		 as int.
def var nbpxa		 as int.
def var nbtar			 as int.
def var nbstock			 as int.
def var nbgamme           as int.

def var soldes-actives   as char.
def var soldes-agil      as char.
def var dt-deb-soldes    as date.
def var dt-fin-soldes    as date.
def var dt-deb-dem       as date extent 3.
def var dt-fin-dem       as date extent 3.

/*----------------------------------------*/
/* Description des zones D�but de section */
/*----------------------------------------*/
def var Type_Ligne_Deb        as char format "x" init "@".
def var Nom_Section_Deb       as char format "x(20)".
def var Nom_Section_PA        as char format "x(20)".
def var Cd_Propriete_deb      as char format "xxx".
def var Type-Integration      as char init "MAJOUT".

def var Type-Donnees-Stock    as char init "BRUT".
def var DT_MAJ_STOCKS         as date.
def var HR_MAJ_STOCKS         as char init "00:00:00".

def var CD_PRO_MODELE0        as char format "xxx" init "TYP".
def var CD_PRO_MODELE1        as char format "xxx" init "FAM".
def var CD_PRO_MODELE2        as char format "xxx" init "SFA".
def var CD_PRO_MODELE3        as char format "xxx" init "SSF".
def var CD_PRO_MODELE4        as char format "xxx" init "FAP".
def var CD_PRO_ARTICLE1       as char format "xxx" init "D3E".
def var CD_PRO_ARTICLE2       as char format "xxx" init "RPD".
def var CD_PRO_ARTICLE3       as char format "xxx" init "MOB".
def var CD_PRO_ARTICLE4       as char format "xxx" init "ANV".    /* animalerie vivante */
def var CD_DYN_CLI01          as char format "xxx" init "C01".
def var CD_DYN_CLI02          as char format "xxx" init "C02".
def var CD_DYN_CLI03          as char format "xxx" init "C03".
def var CD_DYN_CLI04          as char format "xxx" init "C04".
def var CD_DYN_CLI05          as char format "xxx" init "C05".
def var CD_DYN_CLI06          as char format "xxx" init "C06".
def var CD_DYN_CLI07          as char format "xxx" init "C07".
def var CD_DYN_CLIC08         as char format "xxx" init "C08".
def var CD_DYN_CLI08          as char format "xxx" init "R01".
def var CD_DYN_CLI09          as char format "xxx" init "R02".
def var CD_DYN_CLI10          as char format "xxx" init "R03".
def var CD_DYN_CLI11          as char format "xxx" init "R04".
def var CD_DYN_CLI12          as char format "xxx" init "R05".
def var CD_DYN_CLI13          as char format "xxx" init "R06".
def var CD_DYN_CLI14          as char format "xxx" init "R07".
def var CD_DYN_CLI15          as char format "xxx" init "R08".
def var CD_DYN_CLI16          as char format "xxx" init "R09".
def var CD_DYN_CLI17          as char format "xxx" init "R10".
def var CD_DYN_CLI18          as char format "xxx" init "R11".
def var CD_DYN_CLI19          as char format "xxx" init "R12".
def var CD_DYN_CLI20          as char format "xxx" init "E01".
def var CD_DYN_CLI21          as char format "xxx" init "E02".
def var CD_DYN_CLI22          as char format "xxx" init "E03".
def var CD_DYN_CLI23          as char format "xxx" init "E04".
def var CD_DYN_CLI24          as char format "xxx" init "E05".
def var CD_DYN_CLI25          as char format "xxx" init "E06".
def var CD_DYN_CLI26          as char format "xxx" init "E07".
def var CD_DYN_CLI27          as char format "xxx" init "A01".
def var CD_DYN_CLI28          as char format "xxx" init "A02".
def var CD_DYN_CLI29          as char format "xxx" init "A03".
def var CD_DYN_CLI30          as char format "xxx" init "A04".
def var CD_DYN_CLI31          as char format "xxx" init "A05".
def var CD_DYN_CLI32          as char format "xxx" init "A06".
def var CD_DYN_CLI33          as char format "xxx" init "A07".
def var CD_DYN_CLI34          as char format "xxx" init "A08".
def var CD_DYN_CLI35          as char format "xxx" init "L01".
def var CD_DYN_CLI36          as char format "xxx" init "L02".
def var CD_DYN_CLI37          as char format "xxx" init "L03".
def var CD_DYN_CLI38          as char format "xxx" init "L04".
def var CD_DYN_CLI39          as char format "xxx" init "L05".
def var CD_DYN_CLI40          as char format "xxx" init "L06".
def var CD_DYN_CLI41          as char format "xxx" init "L07".
def var CD_DYN_CLI42          as char format "xxx" init "L08".
def var CD_DYN_CLI43          as char format "xxx" init "L09".
def var CD_DYN_CLI44          as char format "xxx" init "L10".
def var CD_DYN_CLI45          as char format "xxx" init "L11".

/*-----------------------------------------*/
/* Description des zones D�tail de section */
/*-----------------------------------------*/
def var Type_Ligne_Det        as char  format "x".

/* D�tail Propri�t� Mod�le  */
def var ID_PRO_MODELE0        as char format "xxx".
def var ID_PRO_MODELE1        as char format "xxx".
def var ID_PRO_MODELE2        as char format "x(6)".
def var ID_PRO_MODELE3        as char format "x(9)".
def var ID_PRO_MODELE4        as char format "xxx".
def var LB_PRO_MODELE1        as char format "x(40)".
def var LB_PRO_MODELE2        as char format "x(70)".
def var LB_PRO_MODELE3        as char format "x(70)".
def var LB_PRO_MODELE4        as char format "x(40)".

/* D�tail Mod�le  */
def var ID_MODELE             as char format "x(15)".
def var NM_MODELE             as char format "x(30)".
def var ID_TVA                as char format "xx".
def var ID_GUELTE             as char format "xx".
def var ID_UNITE              as char format "xx".  
def var TY_MAJ_STOCK          as char format "x(5)" init "ZERO".
/* def var TY_MAJ_STOCK       as char format "x(5)" init "QTE". a remettre pour la gestion des stocks  */
def var TY_MAJ_NB_ART         as char format "x(5)" init "QTE".
def var TY_MAJ_STAT           as char format "x(5)" init "QTE".
def var FL_REMISE             as log init NO.

/* D�tail Propri�t� Article  */
def var ID_PRO_ARTICLE_D3E    as char format "x(13)".
def var LB_PRO_ARTICLE-D3E    as char format "x(99)".
def var ID_PRO_ARTICLE_RPD    as char format "x(13)".
def var LB_PRO_ARTICLE-RPD    as char format "x(99)".
def var ID_PRO_ARTICLE_MOB    as char format "x(13)".
def var LB_PRO_ARTICLE-MOB    as char format "x(99)".
def var ID_PRO_ARTICLE_ANV    as char format "x(13)".

/* D�tail Article  */
def var ID_ARTICLE            as char format "x(20)".
def var LB_COURT              as char format "x(27)".
def var LB_LONG               as char format "x(45)".
def var CD_TRAIT              as char format "xx".
def var FL_GENERIQUE          as log init NO.

/* D�tail Code Barre  */
def var CD_BARRE              as char format "x(20)".

/* D�tail Magasins  */
def var ID_MAGASIN            as char format "xxxx".
def var NM_MAGASIN            as char format "x(30)".
def var LB_ADRESSE1           as char format "x(30)".
def var LB_ADRESSE2           as char format "x(30)".
def var LB_ADRESSE3           as char format "x(30)".
def var CD_POSTAL             as char format "x(5)".
def var LB_VILLE              as char format "x(30)".
def var LB_PAYS               as char format "x(20)".
def var NO_TEL                as char format "x(20)".
def var NO_FAX                as char format "x(20)".
def var FL_RECHARGE           as log init NO.
def var FL_SAISIE_PVSL        as log init NO.               
def var ID_LANGUE             as char format "xx" init "FR".
def var ID_PAYS               as char format "xx" init "FR".
def var ID_TYPE_MAGASIN       as char format "xxxx".
def var ID_MAG_ENSEIGNE       as char format "xxx".
def var ID_MAG_SOCIETE        as char format "xx".

/* D�tail Gamme Magasin  */
def var CD_GAMME              as char.
def var CD_GAMME_991          as char.
def var TY_GAMME              as char init "M".

/* D�tail Stock Magasin  */
def var QTE-STOCK          as char.    

/* D�tail Prix de vente Sp�cial Central  */
def var DT_APPLI_TARIF        as date.
def var PX_VTE_SPE_CENTRAL    as char.

/* D�tail Prix de vente Promo  */
def var DT_DEB_PROMO          as date.
def var DT_FIN_PROMO          as date.
def var PX_VTE_SPE_PROMO      as char.

/* D�tail des clients          */
def var ID_CLIENT             as char.
def var ID_CONTACT            as char.
def var NO_CARTE              as char format "x(20)".
def var CD_CIVILITE           as char format "xx".
def var NM_CLIENT             as char format "x(30)".
def var PNM_CLIENT            as char format "x(30)".
def var LB_ADRCLI1            as char format "x(38)".
def var LB_ADRCLI2            as char format "x(38)".
def var LB_ADRCLI3            as char format "x(38)".
def var LB_ADRCLI4            as char format "x(38)".
def var CD_POSTAL_CLI         as char format "x(10)".
def var LB_VILLE_CLI          as char format "x(30)".
def var CD_PAYS               as char format "xx" init "FR".
def var CD_ETAT_PAYS          as char format "xx" init "".
def var FL_NPAI               as log.
def var FL_BLOCAGE            as log.
def var NO_TEL_CLI            as char format "x(20)".
def var DT_NAISSANCE          as char.
def var LB_ADRESSE_MAIL       as char format "x(60)".
def var FL_CONTACT            as log.
def var CD_LANGUE             as char format "xx" init "FR".
def var CD_MAG_RECRUTEUR      as char format "x(4)".
def var DT_RECRUTEMENT        as char.
def var NB_POINTS             as int.
def var DT_MAJ_POINTS         as char.
def var DT_FIN_POINTS         as char.
def var DT_MAJ_CLIENT         as char.
def var ID_DYNINFO_001        as char.              /* type de client   */
def var ID_DYNINFO_002        as char.              /* ste d'appartenance */
def var ID_DYNINFO_003        as char.              /* type de carte  */
def var ID_DYNINFO_004        as char.              /* nb enfants  */
def var ID_DYNINFO_005        as char.              /* telephone portable  */
def var ID_DYNINFO_006        as char.              /* profession */  
def var ID_DYNINFO_007        as char.              /* tps de d�placement   */
def var ID_DYNINFO_MKT        as log extent 38.
def var ID_DYNINFO_C08        as char.              /* code op�rateur  */

/* D�tail Point Delta  */
def var DT_POINTS             as char.
def var HR_POINTS             as char.
def var TY_POINTS             as char init "1".
def var NB_POINTS_DEAL        as int.
def var CD_MOTIF_POINTS       as char.
def var LB_MOTIF_POINTS       as char.

/* D�tail Prix d'achat  */
def var DT_APPLI_TARIF_ACHAT  as date.
def var PX_ACHAT              as char.

/*--------------------------------------*/
/* Description des zones fin de section */
/*--------------------------------------*/
def var Type_Ligne_Fin        as char  format "x" init "&".
def var Nom_Section_Fin       as char format "x(20)".

/*-------------------------------------------*/
/* Definition des streams temporaires :      */
/*-------------------------------------------*/
def var fic-temp-art    as char.
def var fic-temp-gam    as char.
def var fic-temp-tar    as char.
def var fic-temp-gam_991    as char.

def stream temp_art. 
def stream temp_gam.
def stream temp_tar.
def stream temp_gam_991.

/*----------------------------------------*/
/* Definition des streams de sortie :     */
/*----------------------------------------*/
def var fic-agil       as char.  
def var fic-agil-ech   as char.  
def stream agil. 

session:system-alert-boxes = yes.
message "Debut programme" program-name( 1 ) .
message " " .
Message "Debut extraction des donn�es pour Agil le " today "a" string ( time , "hh:mm:ss" ).
message " " .

/*--------------------------*/
/* Chargement aide et texte */
/*--------------------------*/
aide-fichier = "ext-agil0.aid".
run aide-lec.p .
do ind = 1 to 30 :
    aide-m = "libel" + string(ind,"99") .
    libelle[ind] = aide-m.
    { aide-lib.i }
    libelle[ind] = aide-lib.
end.

/*---------------------------*/
/* Chargement des selections */
/*---------------------------*/
run bat-lec.p ( fichier ).
{ bat-cha.i "type-trt"   "type-trt"   "a" }
{ bat-cha.i "l-soc"      "l-soc"      "a" }
{ bat-cha.i "l-tables"   "l-tables"   "a" }
{ bat-cha.i "date-debut" "date-debut" "d" }
/* debut ajout pour batch periodique */
{ bat-cha.i "sel-applic"   "sel-applic"   "a" }
{ bat-cha.i "sel-filtre"   "sel-filtre"   "a" }
{ bat-cha.i "sel-sequence" "sel-sequence" "a" }

/* fin ajout pour batch periodique */ 

/*--------------------*/
/* initialisations    */
/*--------------------*/
if type-trt = entry(1, libelle[16]) then exploit = yes. /* Trt Exploitation */
                                    else exploit = no.
do ind = 1 to num-entries( l-soc ) :
    codsoc-trait[ind] = entry( ind, l-soc ).
end.

/*-----------------------------------------------------*/
/* Recup. date traitement sans enreg. pendant trt      */
/* la maj de la date est faite dans ext-agil2.p        */
/*-----------------------------------------------------*/
if exploit
   then do :
    { ext-agildat.i "no-lock" }
end.

/*----------------------------------------------------------------------*/
/* initialisation des noms de Fichiers et des enregistrements entete    */
/*----------------------------------------------------------------------*/
annee = year(today).
mois = month(today).
jour = day(today).
heure = replace(string(time, "hh:mm:ss"), ":", "").

fic-agil = "/applic/li/depart/agil_fichier_" + string(annee) + string(mois, "99") + string(jour, "99")  + "_" + heure + ".tmp" .
fic-agil-ech = "/applic/li/depart/agil_fichier_" + string(annee) + string(mois, "99") + string(jour, "99")  + "_" + heure + ".ech" .
fic-temp-art = "/applic/li/travail/agil_fichier_temp_art.tmp" .
fic-temp-gam = "/applic/li/travail/agil_fichier_temp_gam.tmp" .
fic-temp-tar = "/applic/li/travail/agil_fichier_temp_tar.tmp" .
fic-temp-gam_991 = "/applic/li/travail/agil_fichier_temp_gam_991.tmp" .

if not exploit then do :

/*
        fic-agil = "/applic/li/vle/agil_fichier_tst_" + string(annee) + string(mois, "99") + string(jour, "99")  + "_" + heure + ".tmp" .
        fic-agil-ech = "/applic/li/vle/agil_fichier_tst_" + string(annee) + string(mois, "99") + string(jour, "99")  + "_" + heure + ".ech" .
        fic-temp-art = "/applic/li/vle/agil_fichier_temp_art_tst.tmp" .
        fic-temp-gam = "/applic/li/vle/agil_fichier_temp_gam_tst.tmp" .
        fic-temp-tar = "/applic/li/vle/agil_fichier_temp_tar_tst.tmp" .
	fic-temp-gam_991 = "/applic/li/vle/agil_fichier_temp_gam_991_tst.tmp" .
*/
        fic-agil = "/applic/li/" + trim(operat) + "/agil_fichier_tst_" + string(annee) + string(mois, "99") + string(jour, "99")  + "_" + heure + ".tmp" .
        fic-agil-ech = "/applic/li/" + trim(operat) + "/agil_fichier_tst_" + string(annee) + string(mois, "99") + string(jour, "99")  + "_" + heure + ".ech" .
        fic-temp-art = "/applic/li/" + trim(operat) + "/agil_fichier_temp_art_tst.tmp" .
        fic-temp-gam = "/applic/li/" + trim(operat) + "/agil_fichier_temp_gam_tst.tmp" .
        fic-temp-tar = "/applic/li/" + trim(operat) + "/agil_fichier_temp_tar_tst.tmp" .
	fic-temp-gam_991 = "/applic/li/" + trim(operat) + "/agil_fichier_temp_gam_991_tst.tmp" .
end.
output stream agil to value( fic-agil ).
output stream temp_art to value( fic-temp-art ). 
output stream temp_gam to value( fic-temp-gam ). 
output stream temp_tar to value( fic-temp-tar ). 
output stream temp_gam_991 to value( fic-temp-gam_991 ). 

heure-debut = time.
Type_Ligne_Det = " ".

/* DGR 09072018 traitement lanc� dans extraction CYLANDE 

/*------------------------------------------------------------------------------------------------*/
/* Mise � jour des codes blocage sur artapr 00 en fonction des diff�rents codes blocage societe   */
/*------------------------------------------------------------------------------------------------*/
if exploit
    then do :
		Message "Maj code blocage ste 00".
		run majart00.p.
end.
*/


/*--------------------------------------------------------*/
/* Mise en memoire des diff�rents taux de TVA             */
/*--------------------------------------------------------*/
i=0.
for each tables where tables.codsoc = " "
    and   tables.etabli = " "
    and   tables.typtab = "TAX"
    and   tables.prefix = "TYP-TAXE"
    and   tables.codtab > "000"
    and   tables.codtab < "900"
no-lock.
    i = i + 1.
    taux-tva[i] = tables.nombre[1].
    code-tva[i] = tables.codtab.
end.
/*
/*--------------------------------------------------------*/
/* Mise en memoire des taux de SRVP                       */
/*--------------------------------------------------------*/
i = 0.
for each tabges where tabges.codsoc = " "
    and   tabges.etabli = " "
    and   tabges.typtab = "ART"
    and   ((tabges.prefix > "SOUFAM2001" 
    and   tabges.prefix <= "SOUFAM210A")
    or tabges.prefix = "9AU")
no-lock.
        
    i = i + 1.
    code-famille[i] = substr(tabges.prefix, 8, 3).
    code-ssfamille[i] = tabges.codtab.
    tx-srvp[i] = tabges.nombre[1].
    tx-demarq[i] = tabges.nombre[6].
    tx-demarq[i] = tabges.nombre[7].
    tx-demarq[i] = tabges.nombre[8].
end. */
/*--------------------------------------------------------*/
/* Mise en memoire de la p�riode de solde active          */
/*--------------------------------------------------------*/
soldes-actives = "N".
soldes-agil= "N".
find first tabges where tabges.codsoc = " "
    and   tabges.etabli = " "
    and   tabges.typtab = "ART"
    and   tabges.prefix = "SOLDES"
    and   tabges.libel2[ 1 ] = "O"
no-lock no-error.
if available tabges then do :
	soldes-actives = tabges.libel2[ 1 ].
	soldes-agil = tabges.libel2[ 2 ].
	dt-deb-soldes = tabges.dattab[ 1 ].
	dt-fin-soldes = tabges.dattab[ 2 ].
	dt-deb-dem[1] = tabges.dattab[ 3 ].
	dt-fin-dem[1] = tabges.dattab[ 4 ].
	dt-deb-dem[2] = tabges.dattab[ 5 ].
	dt-fin-dem[2] = tabges.dattab[ 6 ].
	dt-deb-dem[3] = tabges.dattab[ 7 ].
	dt-fin-dem[3] = tabges.dattab[ 8 ].
end.
   
/*--------------------------------------------------------*/
/* Mise en memoire des codes provenance                   */
/*--------------------------------------------------------*/
i = 0.
for each tabges where tabges.codsoc = " "
                                and   tabges.etabli = " "
                                and   tabges.typtab = "ART"
                                and   tabges.prefix = "PROVENANCE"
        no-lock.
        i = i + 1.
        indic-provenance[i] = tabges.codtab.
        code-provenance[i] = tabges.libel2[mem-langue].
end.

/*==========================================================*/    
/*T R A I T E M E N T   D E S   D O N N E E S   G R O U P E */
/*==========================================================*/
/*--------------------------------------------------------*/
/* Extraction des proprietes modeles                      */
/*--------------------------------------------------------*/
if lookup("PM", l-tables) <> 0
    then do :
	Message "Extraction Proprietes modeles Nomenclature ".
    assign nom-table  = "TABGES NOMENC."
           code-table = "FA"
           nb-lus     = 0
           per-modulo = 10
           Nom_Section_Deb = "PROPRIETESMODELES"
           .                          
    run aff-trt.

    /* Extraction de la nomenclature 2                        */
    do notour = 1 to 3 :
        case notour :
            when 1 then do :
                w-prefix = "FAMART2".
                CD_Propriete_deb = "FAM".
                lg-prefix = 0.
            end.                        
            when 2 then do : 
                w-prefix = "SOUFAM2".
                CD_Propriete_deb = "SFA".
                lg-prefix = 9.
            end.
            when 3 then do :
                w-prefix = "SSF2".
                CD_Propriete_deb = "SSF".
                lg-prefix = 0.
            end.
        end case.
                        
        Nom_Section_Fin = "PROPRIETESMODELES".
        put stream agil unformatted
            Type_Ligne_Deb Nom_Section_Deb "~011" CD_Propriete_deb "~011" Type-Integration "~015" skip. 

        for each tabges where tabges.codsoc = ""
           and   tabges.datmaj >= date-debut
           and   tabges.etabli = ""
           and   tabges.typtab = "ART"
           and   tabges.prefix begins w-prefix
           and   length(tabges.prefix) > lg-prefix
           use-index date-maj
        no-lock :
                                                        
            RUN ECRITURE-STREAM.
            lib-table = tabges.typtab + " " + tabges.prefix + " " +
				tabges.codtab + " " + tabges.libel1[mem-langue].
            run aff-trt.

        end. /* for each tabges */
                        
        put stream agil unformatted
            Type_Ligne_Fin Nom_Section_Fin "~015" skip. 
                                        
    end. /* do notour = 1 to 3 : */

    /* Extraction des fili�res d'appro                        */       
    assign  nom-table  = "TABGES APPRO."
                        code-table = "AP"
                        nb-lus     = 0
                        per-modulo = 10
            .
    run aff-trt.
        
    CD_Propriete_deb = "FAP".    
    put stream agil unformatted
        Type_Ligne_Deb Nom_Section_Deb "~011" CD_Propriete_deb "~011" Type-Integration "~015" skip. 
                
    for each tabges where tabges.codsoc = ""
       and   tabges.datmaj >= date-debut
       and   tabges.etabli = ""
       and   tabges.typtab = "ART"
       and   tabges.prefix = "PROVENANCE"
       use-index date-maj
    no-lock :
                                                        
        RUN ECRITURE-STREAM.
		lib-table = tabges.typtab + " " + tabges.prefix + " " +
                    tabges.codtab + " " + tabges.libel1[mem-langue].
        run aff-trt.

    end. /* for each tabges */
                        
    put stream agil unformatted
        Type_Ligne_Fin Nom_Section_Fin "~015" skip.
		
end. /* Proprietes modeles */

/*--------------------------------------------------------*/
/* Extraction des proprietes articles                     */
/*--------------------------------------------------------*/
if lookup("PA", l-tables) <> 0
    then do :
	Message "Extraction Proprietes article Ecotaxe et RPD ".
    assign nom-table  = "TABGES Ecotaxe."
           code-table = "PA_D3E"
           nb-lus     = 0
           per-modulo = 10
           Nom_Section_Deb = "PROPRIETESARTICLES"
           Nom_Section_Fin = "PROPRIETESARTICLES"
           CD_Propriete_deb  = "D3E"
            .                         
    run aff-trt.

    /* Extraction de l'Ecotaxe                                */                    
    put stream agil unformatted
        Type_Ligne_Deb Nom_Section_Deb "~011" CD_Propriete_deb "~011" Type-Integration "~015" skip. 

    for each tabges where tabges.codsoc = soc-groupe
       and   tabges.etabli = ""
       and   tabges.typtab = "ART"
       and   tabges.prefix = "SUIT-FICHE"
       and   tabges.nombre[15] <> 0
	   and   tabges.datmaj >= date-debut
       use-index date-maj
    no-lock :
                                        
    /* On envoie l'�cotaxe que pour les produits r�pondant aux crit�res de s�lection         */
		warticl = tabges.codtab.
     /*   {ext-agilart2.i}  */
                                                        
        RUN ECRITURE-STREAM.                
        lib-table = tabges.typtab + " " + tabges.prefix + " " +
                    tabges.codtab + " " + tabges.libel1[mem-langue].
		run aff-trt.

    end. /* for each tabges */
        
    put stream agil unformatted
        Type_Ligne_Fin Nom_Section_Fin "~015" skip. 
        
    /* Extraction de l'ECO_Mobilier    	*/ 

	assign nom-table  = "TABGES Eco Mobilier."
           code-table = "PA_MOB"
           nb-lus     = 0
           per-modulo = 10
           Nom_Section_Deb = "PROPRIETESARTICLES"
           Nom_Section_Fin = "PROPRIETESARTICLES"
           CD_Propriete_deb  = "MOB"
            .                         
    run aff-trt.	
    put stream agil unformatted
        Type_Ligne_Deb Nom_Section_Deb "~011" CD_Propriete_deb "~011" Type-Integration "~015" skip. 

    for each tabges where tabges.codsoc = soc-groupe
       and   tabges.etabli = ""
       and   tabges.typtab = "ART"
       and   tabges.prefix = "SUIT-FICHE"
       and   tabges.nombre[24] <> 0
	   and   tabges.datmaj >= date-debut
       use-index date-maj
    no-lock :
                                        
    /* On envoie l'�co mobilier que pour les produits r�pondant aux crit�res de s�lection         */
		warticl = tabges.codtab.
      /*  {ext-agilart2.i} */
                                                        
        RUN ECRITURE-STREAM.                
        lib-table = tabges.typtab + " " + tabges.prefix + " " +
                    tabges.codtab + " " + tabges.libel1[mem-langue].
		run aff-trt.

    end. /* for each tabges */
        
    put stream agil unformatted
        Type_Ligne_Fin Nom_Section_Fin "~015" skip. 
		
    /* Extraction de a Redevance de Pollution Diffuse         */     
    assign nom-table  = "TARCLI RPD."
           code-table = "PA_RPD"
           nb-lus     = 0
           per-modulo = 10
           Nom_Section_Deb = "PROPRIETESARTICLES"
           Nom_Section_Fin = "PROPRIETESARTICLES"
           CD_Propriete_deb  = "RPD"
           .                      
    run aff-trt.
        
    put stream agil unformatted
        Type_Ligne_Deb Nom_Section_Deb "~011" CD_Propriete_deb "~011" Type-Integration "~015" skip. 
        
    do ind-soc = 1 to num-entries( l-soc-article ) :
        
        for each tarcli-rpd where tarcli-rpd.codsoc = entry( ind-soc, l-soc-article )
           and   tarcli-rpd.motcle = "VTE"
           and   tarcli-rpd.typaux = "TAX"
           and   tarcli-rpd.codaux = ""            
           and   tarcli-rpd.code-tarif = "RPD"
           and   tarcli-rpd.datmaj >= date-debut
           and   tarcli-rpd.chrono = 3
           use-index k-datmaj no-lock :

			/* On envoie la RPD que pour les produits r�pondant aux crit�res de s�lection            */     
			warticl = tarcli-rpd.articl.
		/*	{ext-agilart2.i} */
        
			RUN ECRITURE-STREAM.
                        
			lib-table = tarcli-rpd.codsoc + " " + tarcli-rpd.articl .
			run aff-trt.
                        
        end. /* for each tarcli */   
    end. /* boucle sur soci�t� */
                                
    put stream agil unformatted
        Type_Ligne_Fin Nom_Section_Fin "~015" skip.                                        

end. /* Proprietes articles */

/*--------------------------------------------*/
/* Extraction des donn�es Modeles et Articles */
/*--------------------------------------------*/
if lookup("AR", l-tables) <> 0
   then do :
	Message "Extraction Modeles et articles ".
   assign nom-table  = "ARTAPR"
          code-table = "AR"
          nb-lus     = 0
          per-modulo = 100
          .
   run aff-trt.

    Nom_Section_Deb = "MODELES".
    put stream agil unformatted
        Type_Ligne_Deb Nom_Section_Deb "~011" Type-Integration "~011"
        CD_PRO_MODELE0 "~011" CD_PRO_MODELE1 "~011" 
        CD_PRO_MODELE2 "~011" CD_PRO_MODELE3 "~011" CD_PRO_MODELE4 "~015" skip. 
                          
    Nom_Section_Deb = "ARTICLES".
    put stream temp_art unformatted
       Type_Ligne_Deb Nom_Section_Deb "~011" Type-Integration "~011" 
	   CD_PRO_ARTICLE1 "~011" CD_PRO_ARTICLE2 "~011" CD_PRO_ARTICLE3 "~011" CD_PRO_ARTICLE4 "~015" skip. 
                                                  
    for each artapr where artapr.codsoc = soc-groupe
        and   artapr.datmaj >= date-debut
        use-index date-maj
    no-lock :
                                        
        find tabges-art where tabges-art.codsoc = soc-groupe
            and   tabges-art.etabli = " "
            and   tabges-art.typtab = "ART"
            and   tabges-art.prefix = "suit-fiche"
            and   tabges-art.codtab = artapr.articl
        no-lock no-error.
                                                
        ID_PRO_ARTICLE_D3E = "".
		ID_PRO_ARTICLE_MOB = "".
		ID_PRO_ARTICLE_ANV = "".
        produit-eaj = yes.
	    FL_REMISE = no.

	  if lookup(artapr.famart, l-pro-agri) <> 0
     		then FL_REMISE = yes.
          if artapr.articl = "S70843" then FL_REMISE = no.      

        if available tabges-art 
           then do :
		    if tabges-art.libel2[16] = "0"
                   then produit-eaj = no.
                if tabges-art.nombre[15] <> 0
                   then ID_PRO_ARTICLE_D3E = "D3E" + tabges-art.codtab.
				if tabges-art.nombre[24] <> 0
                   then ID_PRO_ARTICLE_MOB = "MOB" + tabges-art.codtab.
                if tabges-art.libel3[2] = "1"
			 then FL_REMISE = yes.
            end.

		/*  Recherche de la propriete RPD on prend par defaut la valeur en 01         */

        ID_PRO_ARTICLE_RPD = "".
        do ind-soc = 1 to num-entries( l-soc-article ) :
        
            find first tarcli-rpd where tarcli-rpd.codsoc = entry( ind-soc, l-soc-article )
                 and   tarcli-rpd.motcle = "VTE"
                 and   tarcli-rpd.typaux = "TAX"
                 and   tarcli-rpd.codaux = ""
                 and   tarcli-rpd.articl = artapr.articl                       
                 and   tarcli-rpd.code-tarif = "RPD"
                 and   tarcli-rpd.chrono = 3
                 and   tarcli-rpd.date-debut <= date-rpd
            no-lock no-error.

			if not available tarcli-rpd
               then next.
               else do:
                    ID_PRO_ARTICLE_RPD = "RPD" + artapr.articl.
                    leave.
               end.

        end. /* boucle sur soci�t� */

        RUN ECRITURE-STREAM.
                
        lib-table = artapr.codsoc + " " + artapr.articl + " " +
                        artapr.libart1[mem-langue].
        run aff-trt.
                
    end. /* for each artapr */
   
	Nom_Section_Fin = "MODELES".
    put stream agil unformatted
        Type_Ligne_Fin Nom_Section_Fin "~015" skip. 
        
    Nom_Section_Fin = "ARTICLES".
    put stream temp_art unformatted
        Type_Ligne_Fin Nom_Section_Fin "~015" skip. 
                                  
    /* Fermeture Fichiers */
    output stream agil close.
    output stream temp_art close.

    /* Fusion des fichiers */
    os-append value(fic-temp-art) value(fic-agil).  
        
    /* Reouverture du fichier Agil en append */
    output stream agil to value( fic-agil )append.
        
end. /* Articles */   

/*--------------------------*/
/* Codes barre              */
/*--------------------------*/
if lookup("GE", l-tables) <> 0
   then do :
	Message "Extraction Gencod ".  
        assign nom-table  = "CODBAR"
          code-table = "GE"
          nb-lus     = 0
          per-modulo = 100
          .
        run aff-trt.

    /* DEB modif 18/04/2011  ==> On traite d'abord les suppressions de Gencod en societe 00*/
    Nom_Section_Deb = "CODE_BARRE_DELETE".
    put stream agil unformatted 
               Type_Ligne_Deb Nom_Section_Deb "~015" skip. 
			   
	DO TRANSACTION :	
		for each dupsui
			where nomfic = "CODBAR"
		exclusive-lock:
		
		if dupsui.codsoc = "00"
			then RUN ECRITURE-STREAM.
			
		/* On supprime TOUS les enregistrements CODBAR de la table dupsui */		
		if exploit then	delete dupsui.
			
		end.
	END.
	
    Nom_Section_Fin = "CODE_BARRE_DELETE".
    put stream agil unformatted
               Type_Ligne_Fin Nom_Section_Fin "~015" skip. 
                          
    /* FIN modif 18/04/2011 */    

    Nom_Section_Deb = "CODE_BARRE".
    put stream agil unformatted 
               Type_Ligne_Deb Nom_Section_Deb "~011" Type-Integration "~015" skip. 
                          
    for each codbar where codbar.codsoc = soc-groupe
        and   codbar.datmaj >= date-debut    
        use-index datmaj
    no-lock :

    /* On ne transmet que les codes barres des articles r�pondant aux crit�res de s�lection  */    
        warticl = codbar.articl.
        {ext-agilart2.i}
        
        RUN ECRITURE-STREAM.
           
        lib-table = codbar.codsoc + " " + codbar.articl + " " + codbar.code-barre.
        run aff-trt.
       
	end. /* for each CODBAR */
        
    Nom_Section_Fin = "CODE_BARRE".
    put stream agil unformatted
             Type_Ligne_Fin Nom_Section_Fin "~015" skip. 
                          
end . /* Code barre */

/*============================================================*/
/*T R A I T E M E N T   D E S   D O N N E E S   S O C I E T E */
/*============================================================*/
/*--------------------------*/
/* Clients identifies       */
/*--------------------------*/
if lookup("CL", l-tables) <> 0
   then do :
 	Message "Extraction Clients professionnels ". 	
    assign nom-table  = "CLIENTS"
           code-table = "CL"
           nb-lus     = 0
           per-modulo = 100
           .
	run aff-trt.
   
    Nom_Section_Deb = "CUSTOMER".
    put stream agil unformatted 
        Type_Ligne_Deb Nom_Section_Deb "~011" CD_DYN_CLI01 "~011" CD_DYN_CLI02 "~011" CD_DYN_CLI03 
        "~011" CD_DYN_CLI05 "~015" skip. 
        
    do j = 1 to num-entries( l-soc-client ) :
	
		codsoc-soc = entry ( j, l-soc-client ).
			
		regs-app = "NATHALIE".
		{ regs-cha.i }
   
		regs-app = "ELODIE".
		{ regs-cha.i }
			
		regs-fileacc = "AUXILI" + type-tiers.
		{ regs-rec.i }
		auxili-soc = regs-soc .
	
		/*    boucle sur Auxili            */
		for each auxili where auxili.codsoc = auxili-soc
		and   auxili.typaux = type-tiers
		and   auxili.datmaj >= date-debut
		use-index date-maj
		no-lock :
                                        
			find auxapr where auxapr.codsoc = entry ( j, l-soc-client )
			and   auxapr.typaux = auxili.typaux
			and   auxapr.codaux = auxili.codaux
			/* VLE 20012010     and   (lookup( auxapr.type-tiers, l-type-tiers) <> 0 or auxapr.type-tiers = "") */
			and   auxapr.type-adh <> "6"  /* pour ne pas cr�er les salari�s comme des clients identifi�s  */
			no-lock no-error.
			if not available auxapr
				then next.
			If substr(auxili.codaux, 5, 3) <> "998"    /*    client comptant magasin  */
				and auxapr.env-depot = "9"  /* clents � ne pas envoyer en magasin  */
				then next.

			if auxapr.codsoc = "01"
				then auxdet-soc = "".
				else auxdet-soc = auxapr.codsoc.
                                        
			RUN ECRITURE-STREAM.
           
			lib-table = auxili.codsoc + " " + auxili.typaux + " " +
                        auxili.codaux.
			run aff-trt.
       
        end. /* for each AUXILI */

		/*    boucle sur Auxapr            */
		for each auxapr where auxapr.codsoc = entry ( j, l-soc-client )
           and   auxapr.typaux = type-tiers
           and   auxapr.datmaj >= date-debut
           and   auxapr.type-adh <> "6"  /* pour ne pas cr�er les salari�s comme des clients identifi�s  */
           use-index date-maj
        no-lock :
			find auxili where auxili.codsoc = auxili-soc
              and   auxili.typaux = auxapr.typaux
              and   auxili.codaux = auxapr.codaux
            no-lock no-error.
            if not available auxili
                then next.
            If substr(auxili.codaux, 5, 3) <> "998"    /*    client comptant magasin  */
               and auxapr.env-depot = "9"  /* clents � ne pas envoyer en magasin  */
               then next.

            if auxapr.codsoc = "01"
               then auxdet-soc = "".
               else auxdet-soc = auxapr.codsoc.
                                        
            RUN ECRITURE-STREAM.
           
            lib-table = auxili.codsoc + " " + auxili.typaux + " " +
                        auxili.codaux.
            run aff-trt.
       
        end. /* for each AUXAPR */
                
    end. /* boucle sur les soci�t�s client   */
 
    Nom_Section_Fin = "CUSTOMER".
    put stream agil unformatted
       Type_Ligne_Fin Nom_Section_Fin "~015" skip. 
                          
end . /* Clients identifies */

/*-------------------------*/
/* Cartes fidelites CIENTS */
/*-------------------------*/
/*   Debut modif 04102011

         ON NE MET PLUS A JOUR LES CARTES DE FIDELITE A PARTIR DE DEAL 
		 TOUT EST FAIT MAINTENANT DANS AGIL
		 
if lookup("CF", l-tables) <> 0
   then do :
 	Message "Extraction Cartes fid�lite ".  
        assign nom-table  = "CARTES CFI"
              code-table = "CF"
              nb-lus     = 0
              per-modulo = 100
              .
		run aff-trt.
   	
    Nom_Section_Deb = "CUSTOMER".
    put stream agil unformatted 
        Type_Ligne_Deb Nom_Section_Deb "~011" CD_DYN_CLI01 "~011" CD_DYN_CLI02 "~011" CD_DYN_CLI03 
        "~011" CD_DYN_CLI04 "~011" CD_DYN_CLI05 "~011" CD_DYN_CLI06 "~011" CD_DYN_CLI07 
        "~011" CD_DYN_CLI08 "~011" CD_DYN_CLI09 "~011" CD_DYN_CLI10 "~011" CD_DYN_CLI11 "~011" CD_DYN_CLI12
        "~011" CD_DYN_CLI13 "~011" CD_DYN_CLI14 "~011" CD_DYN_CLI15 "~011" CD_DYN_CLI16 "~011" CD_DYN_CLI17
        "~011" CD_DYN_CLI18 "~011" CD_DYN_CLI19 
        "~011" CD_DYN_CLI20 "~011" CD_DYN_CLI21 "~011" CD_DYN_CLI22 "~011" CD_DYN_CLI23 "~011" CD_DYN_CLI24
        "~011" CD_DYN_CLI25 "~011" CD_DYN_CLI26
        "~011" CD_DYN_CLI27 "~011" CD_DYN_CLI28 "~011" CD_DYN_CLI29 "~011" CD_DYN_CLI30 "~011" CD_DYN_CLI31
        "~011" CD_DYN_CLI32 "~011" CD_DYN_CLI33 "~011" CD_DYN_CLI34
        "~011" CD_DYN_CLI35 "~011" CD_DYN_CLI36 "~011" CD_DYN_CLI37 "~011" CD_DYN_CLI38 "~011" CD_DYN_CLI39
        "~011" CD_DYN_CLI40 "~011" CD_DYN_CLI41 "~011" CD_DYN_CLI42 "~011" CD_DYN_CLI43 "~011" CD_DYN_CLI44
        "~011" CD_DYN_CLI45 "~015" skip. 
                          
    do nsoc = 1 to  num-entries( l-soc-cartes-cli ) :
        
        for each auxdet where auxdet.codsoc = entry(nsoc, l-soc-cartes-cli)
           and   auxdet.motcle = "CFI"
		   and   auxdet.typaux = ""
		   and   auxdet.codaux = ""
        /*  and   auxdet.cdat02 = ? 	VLE 25012011 TRT des cartes desactivees  */   
           and   auxdet.datmaj >= date-debut
        no-lock :
                        
            RUN ECRITURE-STREAM.
           
            lib-table = auxdet.codsoc + " " + auxdet.codtab + " " + "CFI CLI".
            run aff-trt.                    
                
        end. /* for each auxdet */
                                        
    end. /* Boucle Cartes de fidelit� */
		
    Nom_Section_Fin = "CUSTOMER".
    put stream agil unformatted
        Type_Ligne_Fin Nom_Section_Fin "~015" skip.
						  
	/* VLE 20122010 Desactivation des cartes   */
	
    Nom_Section_Deb = "CUSTOMER_DISABLED_CARD".
        put stream agil unformatted
		    Type_Ligne_Deb Nom_Section_Deb "~015" skip. 
                          
    do nsoc = 1 to  num-entries( l-soc-cartes-cli ) :
        
        for each auxdet where auxdet.codsoc = entry(nsoc, l-soc-cartes-cli)
           and   auxdet.motcle = "CFI"
		   and   auxdet.typaux = ""
		   and   auxdet.codaux = ""
           and   auxdet.cdat02 <> ?
           and   auxdet.datmaj >= date-debut
        no-lock :
           
            lib-table = auxdet.codsoc + " " + auxdet.codtab + " " +
                       "Desactivation CFI CLI".
            run aff-trt.                    
                
			if auxdet.calf14 = "" 
				then do :
				if auxdet.codsoc = "08"
					then ID_CLIENT = trim(auxdet.codsoc) + substr(auxdet.codtab, 6, 8).
					else ID_CLIENT = "01" + substr(auxdet.codtab, 6, 8).
					ID_CONTACT= ID_CLIENT.
				end.
			else assign ID_CLIENT = entry(1, auxdet.calf14)
					    ID_CONTACT= entry(2, auxdet.calf14).
			put stream agil unformatted
				Type_Ligne_Det ID_CLIENT "~011" ID_CONTACT "~011" auxdet.codtab "~011" auxdet.cdat02 "~011" 
                        auxdet.heumaj "~011"  "~011" "~015" skip.
        end. /* for each auxdet */
                                        
	end. /* Boucle Cartes de fidelit� */
        
    Nom_Section_Fin = "CUSTOMER_DISABLED_CARD".
    put stream agil unformatted
        Type_Ligne_Fin Nom_Section_Fin "~015" skip.

	/* VLE 20122010 fin Desactivation des cartes   */
	
end. /*  cartes fidelite clients  */

 Fin modif du 04102011 */
 
/*---------------------------*/
/* Cartes fidelites SALARIES */
/*---------------------------*/
if lookup("CS", l-tables) <> 0
   then do :
  	Message "Extraction Cartes Salarie ".
        assign nom-table  = "CARTES CS"
               code-table = "CS"
               nb-lus     = 0
               per-modulo = 100
               .
		run aff-trt.
   
    Nom_Section_Deb = "CUSTOMER".
    put stream agil unformatted
        Type_Ligne_Deb Nom_Section_Deb "~011" CD_DYN_CLI01 "~011" CD_DYN_CLI02 "~011" CD_DYN_CLI03 
        "~011" CD_DYN_CLIC08 "~015" skip. 
                          
    for each auxdet where auxdet.codsoc = soc-cartes-sal
       and   auxdet.motcle = "CFI"
       and   auxdet.datmaj >= date-debut
    no-lock :
                        
        RUN ECRITURE-STREAM.
           
        lib-table = auxdet.codsoc + " " + auxdet.codtab + " " + "CFI CS".
        run aff-trt.                    

    end. /* for each auxdet */
        
    Nom_Section_Fin = "CUSTOMER".
    put stream agil unformatted
        Type_Ligne_Fin Nom_Section_Fin "~015" skip. 
		
	/* VLE 11052011 Desactivation des cartes   */
	
    Nom_Section_Deb = "CUSTOMER_DISABLED_CARD".
        put stream agil unformatted
		    Type_Ligne_Deb Nom_Section_Deb "~015" skip. 
                          
        
        for each auxdet where auxdet.codsoc = soc-cartes-sal
           and   auxdet.motcle = "CFI"
           and   auxdet.cdat02 <> ?
           and   auxdet.datmaj >= date-debut
        no-lock :
           
            lib-table = auxdet.codsoc + " " + auxdet.codtab + " " +
                       "Desactivation CFI SAL".
            run aff-trt.                    
                
			If auxdet.calf14 = "" 
                        then do :
                            ID_CLIENT = trim(auxdet.codsoc) + substr(auxdet.codtab, 6, 8).
                            ID_CONTACT= ID_CLIENT.
                        end.
                        else assign ID_CLIENT = entry(1, auxdet.calf14)
                                                ID_CONTACT= entry(2, auxdet.calf14).
			put stream agil unformatted
				Type_Ligne_Det ID_CLIENT "~011" ID_CONTACT "~011" auxdet.codtab "~011" auxdet.cdat02 "~011" 
                        auxdet.heumaj "~011"  "~011" "~015" skip.
        end. /* for each auxdet */
        
    Nom_Section_Fin = "CUSTOMER_DISABLED_CARD".
    put stream agil unformatted
        Type_Ligne_Fin Nom_Section_Fin "~015" skip.

	/* VLE 11052011 fin Desactivation des cartes   */
                                        
end. /*  cartes fidelite salaries  */

/*-----------------*/
/* Boucle Societes */
/*-----------------*/
do nsoc = 1 to  num-entries( l-soc ) :

   if codsoc-trait[nsoc] = "" then next .
   
   codsoc-soc = codsoc-trait[nsoc].
   { top-asec.i }
   regs-app = "NATHALIE".
   { regs-cha.i }
   { regs-inc.i "articl" }
   /* on force le code soci�t� article avec le code soci�t� groupe */
   articl-soc = soc-groupe .
   regs-app = "ELODIE".
   { regs-cha.i }
   regs-fileacc = "AUXILI" + type-tiers.
   {regs-rec.i}
   auxili-soc = regs-soc .
    
/*--------------------------*/
/* Magasins                 */
/*--------------------------*/
    if lookup("DE", l-tables) <> 0
    then do :
	Message "Extraction magasins " codsoc-trait[nsoc].
        assign nom-table =  "DEPOTS"
               code-table = "DE"
               nb-lus     = 0
               per-modulo = 10
               .
        run aff-trt.
       
		Nom_Section_Deb = "MAGASINS".
        put stream agil unformatted
            Type_Ligne_Deb Nom_Section_Deb "~011" Type-Integration "~015" skip.
                          
        for each magasi where magasi.codsoc = ""
          and   magasi.datmaj >= date-debut
          and   magasi.societe = codsoc-trait[nsoc]
          and   magasi.equipe = "A"
		  and   (lookup(magasi.codtab, l-mag-gv) <> 0 or lookup(magasi.codtab, l-mag-asec) <> 0)
          use-index date-maj
        no-lock :

			RUN ECRITURE-STREAM.
           lib-table = magasi.societe + " " + magasi.codtab + " "  +
                       magasi.libel1[mem-langue].
           run aff-trt.
        end. /* for each magasi */
                
        Nom_Section_Fin = "MAGASINS".
        put stream agil unformatted 
            Type_Ligne_Fin Nom_Section_Fin "~015" skip. 
                          
    end. /* Magasins */ 

/*--------------------------*/
/* Points acquis sur DEAL   */
/*--------------------------*/
    if lookup("PT", l-tables) <> 0
    then do :
	Message "Extraction Points " codsoc-trait[nsoc].
        assign nom-table =  "POINTS"
               code-table = "PT"
               nb-lus     = 0
               per-modulo = 10
               .
        run aff-trt.
       
		Nom_Section_Deb = "PT_DELTA".
		Nom_Section_Fin = "PT_DELTA".
                          
        for each entspe 
			where entspe.codsoc = codsoc-trait[nsoc]
			and   entspe.motcle = "VTE/CFI"
			and   entspe.typcom = "LIM"
			and   (substr(entspe.alpha-1, 1, 3) = "291" or
			       substr(entspe.alpha-1, 1, 3) = "293" or
                               substr(entspe.alpha-1, 1, 3) = "398")
			and   entspe.int-1 <> 0
			and   entspe.date-1 >= date-debut
			use-index datdep
		no-lock:

			RUN ECRITURE-STREAM.
           lib-table = entspe.codsoc + " " + entspe.alpha-1 + " ".
           run aff-trt.
        end. /* for each entspe */
                          
    end. /* Points */ 
        
  
/*--------------------------*/
/* STOCKS                   */
/*--------------------------*/
if lookup("ST", l-tables) <> 0
    then do :
	Message "Extraction Stock ".
        assign nom-table =  "STOCKS"
               code-table = "ST"
               nb-lus     = 0
               per-modulo = 10
               .
        run aff-trt.
                          
    for each magasi where magasi.codsoc = ""
        and   magasi.societe = codsoc-trait[nsoc]
        and   magasi.equipe = "A"
		and   (lookup(magasi.codtab, l-mag-gv) <> 0 or lookup(magasi.codtab, l-mag-asec) <> 0)
    no-lock :

        /* DGR 12082013 */        
        assign date-invmag = ?  .
        FIND MAGASS 
        where magass.codsoc     = codsoc-trait[nsoc]
        and   magass.codtab     = magasi.codtab
        and   magass.theme      = "INVMAG"
        and   magass.chrono     = 0
        and ( magass.alpha[ 1 ] = "V"  or  magass.alpha[ 1 ] = "X" )
        use-index primaire  no-lock  no-error .
        if available magass  then  
           date-invmag = magasi.datfin-inv .   
       /* fin DGR 12082013 */        

        Nom_Section_Deb = "STOCKS".
        DT_MAJ_STOCKS = today.
        put stream agil unformatted
            Type_Ligne_Deb Nom_Section_Deb "~011" Type-Donnees-Stock "~011" magasi.codtab "~011" 
             DT_MAJ_STOCKS "~011" HR_MAJ_STOCKS "~015" skip.
                
        /*  recherche des Stocks Magasins modifi�s         */
        for each stocks where stocks.codsoc = codsoc-trait[nsoc]
           and   stocks.datmaj >= date-debut
           and   stocks.evenement = ""
           and   stocks.exercice = ""
           and   stocks.magasin = magasi.codtab
           use-index date-maj
        no-lock :
        
	    /* On ne transmet que les codes barres des articles r�pondant aux crit�res de s�lection  */
            warticl = stocks.articl.
            {ext-agilart2.i}

            /* DGR 12082013  ecart inventaire */
            
            stophy = stocks.qte[2].
            if date-invmag <> ?  then do :
                FIND STOINV 
                where stoinv.codsoc      = codsoc-trait[nsoc]
                and   stoinv.magasin     = magasi.codtab
                and   stoinv.date-invent = date-invmag
                and   stoinv.articl      = warticl
                and   stoinv.evenement   = "INA"
                no-lock  no-error .
                if available stoinv then
                   stophy = stophy + ( stoinv.qte-invent - stoinv.qte-stock ) .
             end .
             
            /* Fin DGR 12082013 */
                                
            RUN ECRITURE-STREAM.

            lib-table = magasi.societe + " " + magasi.codtab + " "  +                         
                       magasi.libel1[mem-langue].
            run aff-trt.
                   
        end.  /* for each stocks */
 
        Nom_Section_Fin = "STOCKS".
        put stream agil unformatted 
        Type_Ligne_Fin Nom_Section_Fin "~015" skip. 
                          
    end. /* for each magasi */
                
end. /* STOCKS */ 


/*---------------------------------*/
/* Tarifs CLI et PRO et RDP        */
/*---------------------------------*/
if lookup("TC", l-tables) <> 0 or
   lookup("PD", l-tables) <> 0
    then do :
	Message "Extraction tarifs ste " codsoc-trait[nsoc].
        assign nom-table    = "TARCLI VTE"
               code-table   = "TC"
               l-motcle     = "VTE,VTP"
               l-tarif[ 1 ] = "CLI,PRO"   /* codes tarifs generaux */
               l-tarif[ 2 ] = "CLI,PRO"       /* code tarif promo par magasin */
               l-tarif[ 3 ] = l-tarif[ 1 ] + "," + l-tarif[ 2 ]
               nb-lus       = 0
               per-modulo   = 100.
	run aff-trt.
                
    /*--------------------------------------------------*/  
    /* Recherche du type de tarif : 0 ==> HT, 1 ==> TTC */
    /*--------------------------------------------------*/          
    do i = 1 to num-entries( l-tarif[ 3 ] ) :
        find tabges where tabges.codsoc = codsoc-trait[nsoc]
            and   tabges.etabli = ""
            and   tabges.typtab = "TAR"
            and   tabges.prefix = "CODE-TARIF"
            and   tabges.codtab = entry( i, l-tarif[ 3 ] )
        no-lock no-error.
        if not available tabges
            then do :
                message codsoc-trait[nsoc] entry( i, l-tarif[ 3 ] ) libelle[24] .
                type-tarif[ i ] = 9 .
                next .
            end.
        type-tarif[ i ] = tabges.nombre[ 3 ] .
    end .
	
	nb-mag[nsoc] = 0.
	Nom_Section_Deb = "GAMME".
    CD_GAMME = "GV".
    if codsoc-trait[nsoc] = "08"
        then CD_GAMME = "ASEC".
	if codsoc-trait[nsoc] = "59"
        then CD_GAMME = "VITRY". 		
    put stream temp_gam unformatted 
        Type_Ligne_Deb Nom_Section_Deb "~011" CD_GAMME "~011" TY_GAMME "~011" Type-Integration "~015" skip.
	
	CD_GAMME_991 = "TOTALE".
	put stream temp_gam_991 unformatted 
        Type_Ligne_Deb Nom_Section_Deb "~011" CD_GAMME_991 "~011" TY_GAMME "~011" Type-Integration "~015" skip.
		
    /*--------------------------------------------------*/  
    /* On traite les tarifs par magasin                 */
	/*--------------------------------------------------*/  
    for each magasi where magasi.codsoc  = ""
       and   magasi.societe = codsoc-trait[nsoc]
       and   (lookup(magasi.codtab, l-mag-gv) <> 0 or lookup(magasi.codtab, l-mag-asec) <> 0)
    no-lock :
	
        ID_MAGASIN = magasi.codtab.
        nb-mag[nsoc] = nb-mag[nsoc] + 1.
		
		/* Recherche du bassin d'appartenance pour calcul de la Redevance Pollution Diffuse */
		cd-bassin = " ".
        find magass where magass.codsoc = " "
			and   magass.theme = "SUITE-FICHE"
            and   magass.chrono = 0
            and   magass.codtab = magasi.codtab
        no-lock no-error.
        if available magass
            then cd-bassin = substr(magass.alpha[5], 1, 1).
            else cd-bassin = "3".
                        
        Nom_Section_Deb = "TAR_PVSC".
        put stream agil unformatted 
            Type_Ligne_Deb Nom_Section_Deb "~011" ID_MAGASIN "~011" Type-Integration "~015" skip. 
                          
        Nom_Section_Deb = "TAR_PVSP".
        put stream temp_tar unformatted 
            Type_Ligne_Deb Nom_Section_Deb "~011" ID_MAGASIN "~011" today "~011" Type-Integration "~015" skip. 
                   
        /*---------------------------------------------------------*/   
        /*  Traitement des tarifs Magasins VTE puis VTP            */
        /*---------------------------------------------------------*/
        if lookup("TC", l-tables) <> 0  /* Traitement des mises � jour des tarifs */
           then do :
                         
            do i = 1 to num-entries( l-motcle ) :
				case i :
					when 2 then assign type-promo = "M"           /* PROMO par Magasin */
                                      code-mag   = magasi.codtab
                                      .
					otherwise   assign type-promo = ""            /* tarifs g�n�raux CLI et PRO */
                                       code-mag   = ""
                                       .
                end case .
                    
                for each {tarcli.i} where {tarcli.i}.motcle =  entry( i, l-motcle )
                   and   {tarcli.i}.codsoc =  codsoc-trait[nsoc]
                   and   {tarcli.i}.datmaj >= date-debut
                   and   {tarcli.i}.typaux =  type-promo
				   and   {tarcli.i}.codaux =  code-mag
                   and   lookup( {tarcli.i}.code-tarif, l-tarif[ i ] ) <> 0
                use-index k-datmaj no-lock :

                    if type-tarif[ lookup( right-trim( {tarcli.i}.code-tarif ), l-tarif[ 3 ] ) ] = 9
                        then next .
                    if {tarcli.i}.valeur = 0 or {tarcli.i}.valeur = 0.001
                        then next.

                    /* On ne transmet que les tarifs des articles r�pondant aux crit�res de s�lection  */
					warticl =  {tarcli.i}.articl.
					{ext-agilart2.i}
					val-tarif = {tarcli.i}.valeur.
					
					/* On v�rifie s'il existe un tarif Agil AGC correspondant au tarif cli             */
                    If {tarcli.i}.code-tarif = "CLI"
                        then do :
							RUN RECHERCHE-AGC.
                    end.  /*  tarif AGC  */
                
					/* On ajoute � chaque tarif le montant de la redevance Pollution Diffuse en cours  */
					date-rpd = today.
                    if {tarcli.i}.date-debut > today
                        then date-rpd = {tarcli.i}.date-debut.
					RUN LECT-RPD.
                    RUN CALC-TARIF-RPD-TTC. 
                                                                                                
                    RUN ECRITURE-STREAM.

					lib-table = "V5 " + {tarcli.i}.codsoc + " " + {tarcli.i}.motcle + " " + {tarcli.i}.articl + " "
                                + {tarcli.i}.typaux + " " + {tarcli.i}.codaux + " " + {tarcli.i}.code-tarif + " " +
                                string({tarcli.i}.date-debut, "99/99/9999").
                    run aff-trt.

                end . /* for each {tarcli.i} */
                                        
            end . /* do i = 1 to num-entries( l-motcle ) */
                                
            /*---------------------------------------------------------*/   
            /*  Traitement des tarifs Groupe de Magasins Promo         */
			/*---------------------------------------------------------*/                                   
            type-promo = "G".
			if type-tarif[ 2 ] <> 9
                then do :
                                           
                do k = 1 to num-entries( magasi.type-com ) :
                                                
                    for each {tarcli.i} where {tarcli.i}.motcle =  entry( 2, l-motcle )
                       and   {tarcli.i}.codsoc =  codsoc-trait[nsoc]
                       and   {tarcli.i}.datmaj >= date-debut
                       and   {tarcli.i}.typaux =  type-promo
                       and   {tarcli.i}.codaux =  entry( k,  magasi.type-com )
                       and   lookup( {tarcli.i}.code-tarif, l-tarif[ 2 ] ) <> 0
/*                     and   {tarcli.i}.code-tarif =  l-tarif[ 2 ] */
                    use-index k-datmaj no-lock :
					
                        if {tarcli.i}.valeur = 0 or {tarcli.i}.valeur = 0.001
                            then next.              
						/* On ne transmet que les tarifs des articles r�pondant aux crit�res de s�lection  */
						warticl =  {tarcli.i}.articl.
						{ext-agilart2.i}
                        val-tarif = {tarcli.i}.valeur.

                        /* On ajoute � chaque tarif le montant de la redevance Pollution Diffuse en cours  */
                        date-rpd = today.
                        if {tarcli.i}.date-debut > today
                            then date-rpd = {tarcli.i}.date-debut.                                                  
                        RUN LECT-RPD.
                        RUN CALC-TARIF-RPD-TTC.
                                                                                        
                        RUN ECRITURE-STREAM.

                        lib-table = "V5 " + {tarcli.i}.codsoc + " " + {tarcli.i}.motcle + " " + {tarcli.i}.articl + " "
                                  + {tarcli.i}.typaux + " " + {tarcli.i}.codaux + " " + {tarcli.i}.code-tarif + " " +
                                  string({tarcli.i}.date-debut, "99/99/9999").
                        run aff-trt.
                                                                
                    end . /* for each {tarcli.i} */
                                                
                end. /* boucle sur le nombre de groupes renseignes pour le magasin trait� */
                                                
            end.  /* type-tarif[ 2 ] <> 9 */
                                        
        end. /*  lookup("TC", l-tables) <> 0 */
                        
		/*---------------------------------------*/     
        /*  Modification dela Taxe RPD           */
        /*---------------------------------------*/
        if lookup("PD", l-tables) <> 0  /* Traitement des mises � jour de la RPD */
            then do :
            assign code-table   = "PD"
					nb-lus       = 0
					per-modulo   = 100.
			run aff-trt.
	
            for each tarcli-rpd where tarcli-rpd.codsoc = codsoc-trait[nsoc]
               and   tarcli-rpd.motcle = "VTE"
               and   tarcli-rpd.typaux = "TAX"
               and   tarcli-rpd.codaux = ""            
               and   tarcli-rpd.code-tarif = "RPD"
               and   tarcli-rpd.datmaj >= date-debut
               and   tarcli-rpd.chrono = integer(cd-bassin)
               use-index k-datmaj no-lock :

				/* On ne recalcule les tarifs que pour les articles r�pondant aux crit�res de s�lection  */
                warticl = tarcli-rpd.articl.
                {ext-agilart2.i} 
				valeur-rpd = tarcli-rpd.valeur / 1000. /* valeur HT */
                                       
                /* Recalcul des tarifs Magasins VTE puis PROMO Magasin futurs     */
				do i = 1 to num-entries( l-motcle ) :
					case i :
                        when 2 then assign type-promo = "M"           /* PROMO par Magasin */
										code-mag   = ID_MAGASIN
										.
						otherwise   assign type-promo = ""            /* tarifs g�n�raux CLI et PRO */
									code-mag   = ""
                                    .
                    end case .

                    /*  On extrait tous les tarifs cli futurs et pro futurs et encours          */
                    for each {tarcli.i} where {tarcli.i}.motcle =  entry( i, l-motcle )
                       and   {tarcli.i}.codsoc =  codsoc-trait[nsoc]
                       and   {tarcli.i}.articl =  tarcli-rpd.articl
                       and   ({tarcli.i}.date-debut > tarcli-rpd.date-debut
                             or {tarcli.i}.date-fin >= tarcli-rpd.date-debut)
                       and   {tarcli.i}.typaux =  type-promo
                       and   {tarcli.i}.codaux =  code-mag
                       and   lookup( {tarcli.i}.code-tarif, l-tarif[ i ] ) <> 0
                    no-lock :

                        if type-tarif[ lookup( right-trim( {tarcli.i}.code-tarif ), l-tarif[ 3 ] ) ] = 9
                            then next . 
                        if {tarcli.i}.valeur = 0 or {tarcli.i}.valeur = 0.001
                            then next.
                        val-tarif = {tarcli.i}.valeur.
                                                        
                        /* On v�rifie s'il existe un tarif Agil AGC correspondant au tarif cli             */
                        If {tarcli.i}.code-tarif = "CLI"
                            then do :
							RUN RECHERCHE-AGC.
                        end.  /*  tarif AGC  */
                                                        
                        /* On ajoute � chaque tarif le montant de la redevance Pollution Diffuse en cours  */
						RUN CALC-TARIF-RPD-TTC. 
                        RUN ECRITURE-STREAM.

						lib-table = "V5 " + {tarcli.i}.codsoc + " " + {tarcli.i}.motcle + " " + {tarcli.i}.articl + " "
                                + {tarcli.i}.typaux + " " + {tarcli.i}.codaux + " " + {tarcli.i}.code-tarif + " " +
                                string({tarcli.i}.date-debut, "99/99/9999").
                        run aff-trt.
                                                        
                    end.  /* for each tarcli tarifs futurs */
                                                
                end . /* do i = 1 to num-entries( l-motcle ) */

                /*  On recalcule  le tarif cli actif                */
                find first {tarcli.i} where {tarcli.i}.motcle = "VTE"
                     and   {tarcli.i}.codsoc =  codsoc-trait[nsoc]
                     and   {tarcli.i}.articl =  tarcli-rpd.articl
                     and   {tarcli.i}.date-debut <= tarcli-rpd.date-debut
                     and   {tarcli.i}.date-fin = ? 
                     and   {tarcli.i}.typaux =  ""
                     and   {tarcli.i}.codaux =  ""
                     and   {tarcli.i}.code-tarif = "CLI"
                no-lock no-error.
                                
                if available {tarcli.i} and
                    {tarcli.i}.valeur <> 0 and {tarcli.i}.valeur <> 0.001
                    then do :
                        val-tarif = {tarcli.i}.valeur.
                                                        
						/* On v�rifie s'il existe un tarif Agil AGC correspondant au tarif cli             */
                        RUN RECHERCHE-AGC.
                                                        
                        /* On ajoute � chaque tarif le montant de la redevance Pollution Diffuse en cours  */
                        RUN CALC-TARIF-RPD-TTC. 
                        RUN ECRITURE-STREAM.

                        lib-table = "V5 " + {tarcli.i}.codsoc + " " + {tarcli.i}.motcle + " " + {tarcli.i}.articl + " "
                                  + {tarcli.i}.typaux + " " + {tarcli.i}.codaux + " " + {tarcli.i}.code-tarif + " " +
                                  string({tarcli.i}.date-debut, "99/99/9999").
                        run aff-trt.
                                                        
                end.  /* recalcul du tarif CLI actif */
                                                
				/* Recalcul des tarifs  PROMO de groupe             */
                type-promo = "G" .
				if type-tarif[ 2 ] <> 9
                    then do :
                                                
                    do k = 1 to num-entries( magasi.type-com ) :
                                                 
                        for each {tarcli.i} where {tarcli.i}.motcle =  entry( 2, l-motcle )
                           and   {tarcli.i}.codsoc =  codsoc-trait[nsoc]
                           and   {tarcli.i}.articl =  tarcli-rpd.articl
                           and   ({tarcli.i}.date-debut >=  tarcli-rpd.date-debut
                                 or {tarcli.i}.date-fin >=  tarcli-rpd.date-debut) 
                           and   {tarcli.i}.typaux =  type-promo
                           and   {tarcli.i}.codaux =  entry( k,  magasi.type-com )
                           and   {tarcli.i}.code-tarif =  l-tarif[ 2 ]
                        no-lock :
                                                                        
                            if {tarcli.i}.valeur = 0 or {tarcli.i}.valeur = 0.001
                                then next.      
                                                                
                            /* On ajoute � chaque tarif le montant de la redevance Pollution Diffuse en cours  */
                            val-tarif = {tarcli.i}.valeur.
                            RUN CALC-TARIF-RPD-TTC. 
                            RUN ECRITURE-STREAM.

                            lib-table = "V5 " + {tarcli.i}.codsoc + " " + {tarcli.i}.motcle + " " + {tarcli.i}.articl + " "
                                     + {tarcli.i}.typaux + " " + {tarcli.i}.codaux + " " + {tarcli.i}.code-tarif + " " +
                                     string({tarcli.i}.date-debut, "99/99/9999").
                            run aff-trt.
                                                
                        end . /* for each {tarcli.i} */
                                                
                    end. /* boucle sur le nombre de groupes renseignes pour le magasin trait� */
                                                
                end.  /* type-tarif[ 2 ] <> 9 */
                                        
            end . /* for each tarcli RPD */
                                        
        end. /*  lookup("PD", l-tables) <> 0 */

		Nom_Section_fin = "TAR_PVSC".
        put stream agil unformatted 
            Type_Ligne_Fin Nom_Section_Fin "~015" skip.  
                          
        Nom_Section_fin = "TAR_PVSP".
        put stream temp_tar unformatted 
            Type_Ligne_Fin Nom_Section_Fin "~015" skip. 
                          
    end . /* for each magasi */
        
    end . /* Tarifs Ventes  et RPD */   
	
	Nom_Section_fin = "GAMME".
    put stream temp_gam unformatted 
        Type_Ligne_Fin Nom_Section_Fin "~015" skip.
	put stream temp_gam_991 unformatted 
        Type_Ligne_Fin Nom_Section_Fin "~015" skip.
                        
end. /* Boucle Societes */

output stream agil close.
output stream temp_gam close.
output stream temp_gam_991 close.
output stream temp_tar close.

/* Fusion des fichiers */
os-append value(fic-temp-gam) value(fic-agil).
os-append value(fic-temp-gam_991) value(fic-agil).
os-append value(fic-temp-tar) value(fic-agil).  
        
/* Reouverture du fichier Agil en append */
output stream agil to value( fic-agil )append.
output stream temp_tar to value( fic-temp-tar ).

/*--------------------------------------------------------*/
/* Extraction des tarifs achat                            */
/*--------------------------------------------------------*/
if lookup("TA", l-tables) <> 0
    then do :
	Message "Extraction tarifs achat ".
        assign nom-table    = "TARCLI ACH"
               code-table   = "TA"
               l-motcle     = "VTE"
               l-tarif[ 1 ] = "CLI"   /* codes tarifs generaux */
               nb-lus       = 0
               per-modulo   = 100.
	run aff-trt.

	Nom_Section_Deb = "TAR_PA".
	put stream temp_tar unformatted
        Type_Ligne_Deb Nom_Section_Deb "~011" Type-Integration "~015" skip.


	/* descente totale des soldes dans Agil  */
	if soldes-agil = 'O'
		then do :	
		for each tabges where tabges.codsoc = ""
			and   tabges.etabli = ""
			and   tabges.typtab = "ART"
			and   tabges.prefix begins "SOUFAM2"
			and   (substr(tabges.prefix, 8, 3) <= "110"
                         or substr(tabges.prefix, 8, 1) = "B"    
                         or substr(tabges.prefix, 8, 3) = "P22")
			and   length(tabges.prefix) > 9
		no-lock :
                                       
        run REINIT_TAR_PA. 
                        
        lib-table = tabges.typtab + " " + tabges.prefix + " " +
		tabges.codtab + " " + tabges.libel1[mem-langue].
        run aff-trt.

		end. /* for each tabges */
		do transaction :
			find first tabges-sold where tabges-sold.codsoc = " "
				and   tabges-sold.etabli = " "
				and   tabges-sold.typtab = "ART"
				and   tabges-sold.prefix = "SOLDES"
				and   tabges-sold.libel2[ 2 ] = "O"
			exclusive-lock no-error.
			if available tabges-sold
			then assign tabges-sold.libel2[2] = "T".
		end. 
	end. /* descente totale des soldes */
	
	else do :
	/* autres cas de modification des PA */
	
		/* Extraction de la nomenclature 2    SOUFAM2 modifi�es   */

		for each tabges where tabges.codsoc = ""
			and   tabges.datmaj >= date-debut
			and   tabges.etabli = ""
			and   tabges.typtab = "ART"
			and   tabges.prefix begins "SOUFAM2"
			and   (substr(tabges.prefix, 8, 3) <= "110"
                            or substr(tabges.prefix, 8, 1) = "B"
                            or substr(tabges.prefix, 8, 3) = "P22")
			and   length(tabges.prefix) > 9
			use-index date-maj
		no-lock :
                                       
			run REINIT_TAR_PA. 
                        
			lib-table = tabges.typtab + " " + tabges.prefix + " " +
			tabges.codtab + " " + tabges.libel1[mem-langue].
			run aff-trt.

		end. /* for each tabges */

		/* Extraction des articles GP modifi�s   */
                                          
		for each artapr-maj where artapr-maj.codsoc = soc-groupe
			and   artapr-maj.datmaj >= date-debut
			and   lookup(artapr-maj.famart, l-fam-gp) <> 0
			use-index date-maj
		no-lock :

		/* On ne transmet que les prix d'achat des articles r�pondant aux crit�res de s�lection  */
			warticl =  artapr-maj.articl.
			{ext-agilart2.i}                                    
    
			run MAJ_TAR_PA. 
                
		end. /* for each artapr */  
	
		/* Extraction des tarifs modifi�s  */

		do nsoc = 1 to  num-entries( l-soc-pxa ) :

			for each tarcli-maj where tarcli-maj.motcle =  "VTE"
				and   tarcli-maj.codsoc =  entry( nsoc, l-soc-pxa )
				and   tarcli-maj.datmaj >= date-debut
				and   tarcli-maj.typaux =  ""
				and   tarcli-maj.codaux =  ""
				and   tarcli-maj.code-tarif = "CLI"
			use-index k-datmaj no-lock :

			if tarcli-maj.valeur = 0 or tarcli-maj.valeur = 0.001
               then next.

				/* On ne transmet que les tarifs des articles r�pondant aux crit�res de s�lection  */
				warticl =  tarcli-maj.articl.
				{ext-agilart2.i}
				
				run MAJ_TAR_PA.
		
			end. /* for each tarcli  */
		
		end. /* Boucle Societes Pxa*/

	end. /* autres cas de modification des PA */
	Nom_Section_Fin = "TAR_PA".
	put stream temp_tar unformatted
		Type_Ligne_Fin Nom_Section_Fin "~015" skip. 
		
end. /* Prix d'achat */ 

output stream agil close.
output stream temp_tar close.

/* Fusion des fichiers */
os-append value(fic-temp-tar) value(fic-agil).  

/* Rename du fichier agil en .ech */
os-rename value(fic-agil) value(fic-agil-ech).

message " " .
Message "Nb articles extraits               : " nbart.
Message "Nb clients CF, Cl, CS extraits     : " nbcli.
Message "Nb magasins extraits               : " nbmag.
Message "Nb propri�t�s mod, art extraites   : " nbpro.
Message "Nb gencod extraits                 : " nbgen.
Message "Nb LIM avec points extraites       : " nbpts.
Message "Nb tarifs extraits                 : " nbtar.
Message "Nb stocks extraits                 : " nbstock.
Message "Nb enreg gamme                     : " nbgamme.
Message "Nb prix achat extraits             : " nbpxa .
Message "Fin extraction des donn�es ext-agil1.p le " today "a" string ( time , "hh:mm:ss" ).
message " " .
session:system-alert-boxes = no.

/*---------------------*/
/* MAJ date extraction */
/*---------------------*/
if exploit
   then DO TRANSACTION :
   
    { ext-agildat.i "exclusive-lock" }
   
    /* La date debut est egale a date stockee + 1 a chaque lancement */
    if heure-debut < 64800                   /* Deb. av. 18 h - Fin meme j  */
       then do :
       date-tabges.dattab[1] = today - 1.   /* Pour repartir meme jour     */
       { majmoucb.i date-tabges }
       leave.
    end.
    if heure-debut >= 64800 and time < 64800 /* Deb. ap. 18 h - Fin a j + 1 */
       then do :
       date-tabges.dattab[1] = today - 1.   /* Pour repartir de j + 1      */
       { majmoucb.i date-tabges }
       leave.
    end.
    if heure-debut >= 64800 and time < 86400 /* Deb. ap. 18 h - Fin meme j  */
       then do :
       date-tabges.dattab[1] = today.       /* Pour repartir de j + 1      */
       { majmoucb.i date-tabges }
       leave.
    end.
end .
/* Supprimer les selections */
RUN sel-del.p(sel-applic, sel-filtre, sel-sequence, YES).
/*============================================================================================================*/
/*                                     P R O C E D U R E S                                                    */
/*============================================================================================================*/
PROCEDURE ECRITURE-STREAM :
case code-table :

	when "AR" /* Articles */
        then do :
             { ext-agilart.i }
			 nbart = nbart + 1.
        end .
        
	when "CL" or when "CF" or when "CS" /* Magasins, cartes fidelite clients et salaries */
        then do :
             { ext-agilcli.i }
			 nbcli = nbcli + 1.
        end .
                
	when "DE" /* Magasins */
        then do :
             { ext-agildep.i }
			 nbmag = nbmag + 1.
        end .
        
	when "FA" or when "AP" or when "PA_D3E" or when "PA_RPD" or when "PA_MOB"/* Proprietes modeles et articles */
        then do :
             { ext-agilpro.i }
			 nbpro = nbpro + 1.
        end .
  
	when "GE"  /* Code Barre */
        then do :
             { ext-agilgen.i }
			 nbgen = nbgen + 1.
        end.
	
	when "PT"  /* Points Deal */
        then do :
             { ext-agilpts.i }
			 nbpts = nbpts + 1.
        end.
	
	when "TC" or when "PD" /* Tarifs ou modification de la Redevance Pollution Diffuse */
        then do :
             { ext-agiltar.i }
			 nbtar = nbtar + 1.
        end.
	when "TA" /* Prix d'achat */
        then do :
             { ext-agilpxa.i }
		end.
   when "ST" /* Stocks Magasins */
        then do :
             { ext-agilsto.i }
			 nbstock = nbstock + 1.
        end .
   otherwise return.
end case.
END PROCEDURE. /* ECRITURE-STREAM */
/*============================================================================================================*/
PROCEDURE AFF-TRT :

    if lib-table <> "" then nb-lus = nb-lus + 1.

    if nb-lus modulo ( per-modulo ) = 0
    then do :
        lib-table = code-table + " " + nom-table + " " + lib-table + " " +
                    string( nb-lus, ">>>,>>9" ).
        display lib-table with frame fr-trt.
    end.

    lib-table = "".
END PROCEDURE. /* AFF-TRT */
/*============================================================================================================*/
PROCEDURE LECT-RPD :
/*------------------------------------*/
/* Recherche de la taxe RPD en cours  */
/*------------------------------------*/
find first tarcli-rpd where tarcli-rpd.codsoc = {tarcli.i}.codsoc
     and   tarcli-rpd.motcle = "VTE"
     and   tarcli-rpd.typaux = "TAX"
     and   tarcli-rpd.codaux = ""
     and   tarcli-rpd.articl = {tarcli.i}.articl                   
     and   tarcli-rpd.code-tarif = "RPD"
     and   tarcli-rpd.chrono = integer(cd-bassin)
     and   tarcli-rpd.date-debut <= date-rpd
no-lock no-error.

    if not available tarcli-rpd
        then valeur-rpd = 0 .
        else valeur-rpd = tarcli-rpd.valeur / 1000. /* valeur HT */
                                
END PROCEDURE. /* LECT-RPD */
/*============================================================================================================*/
PROCEDURE CALC-TARIF-RPD-TTC :
/*------------------------------------*/
/* Calcul de la taxe RPD TTC          */
/*------------------------------------*/
do indtva = 1 to 20 :
    if artapr.tva = code-tva[indtva] then do : 
       valeur-rpd = valeur-rpd * ( 1 + (taux-tva[indtva] / 100)).
       leave.
    end.   
end.    
val-tarif = val-tarif + valeur-rpd.

END PROCEDURE. /* CALC-TARIF-RPD-TTC */      
/*============================================================================================================*/
PROCEDURE REINIT_TAR_PA :
/*-----------------------------------------------------------------------------------*/
/* Reinitialisation des prix plancher en cas de modification de sous-famille         */
/*-----------------------------------------------------------------------------------*/
                
for each artapr where artapr.codsoc = soc-groupe
   and   substr(artapr.articl, 7, 1) <> "S"             /*  articles supprimes  */
   and   artapr.caract[3] = " "                         /*  articles destines � la vente  */
   and   artapr.type-client <> "PRO"                    /*  Produits r�serv�s aux clients professionnels  */
   and   lookup(artapr.articl-lie, l-article-cvo) = 0   /*  articles CVO  */
   and   artapr.usto <> "BX"                            /* Box  */
   and   artapr.famart2 = substr(tabges.prefix, 8, 3)   /* appartenant � la sous famille mise � jour  */
   and   artapr.soufam2 = substr(tabges.codtab, 1, 3)
   and   artapr.code-blocage <> "B"						/* non bloqu�s et non interdits � la vente  */ 
   and   artapr.code-blocage <> "F"  
   and  lookup(artapr.famart, l-fam-gp) <> 0            /* prix d'achat seuelement pour le GP */ 
no-lock :

    find tabges-art where tabges-art.codsoc = soc-groupe
		and   tabges-art.etabli = " "
        and   tabges-art.typtab = "ART"
        and   tabges-art.prefix = "suit-fiche"
        and   tabges-art.codtab = artapr.articl
    no-lock no-error.
                                                
    if available tabges-art and
		tabges-art.libel2[16] = "0"   /* produit dangereux */
        then next. 

	RUN MAJ_TAR_PA.	
            
end.  /* for each artapr */
END PROCEDURE. /* REINIT_TAR_PA */
/*============================================================================================================*/
PROCEDURE MAJ_TAR_PA :

do ind-soc = 1 to num-entries( l-soc-pxa ) :
	
    val-tarif = 0.       
/*-----------------------------------------------*/ 	
/*  On extrait tous les tarifs futurs cli        */
/*-----------------------------------------------*/	
	for each {tarcli.i} where {tarcli.i}.motcle =  "VTE"
		and   {tarcli.i}.codsoc =  entry(ind-soc, l-soc-pxa)
		and   {tarcli.i}.articl =  artapr.articl
		and   ({tarcli.i}.date-debut > today
			or {tarcli.i}.date-fin >= today)
		and   {tarcli.i}.typaux =  ""
		and   {tarcli.i}.codaux =  ""
		and   {tarcli.i}.code-tarif = "CLI"
	no-lock :
		
		if {tarcli.i}.valeur = 0 or {tarcli.i}.valeur = 0.001
			then next.        
		val-tarif = {tarcli.i}.valeur.
		
		RUN RECHERCHE-AGC.
		
	/* On ajoute � chaque tarif le montant de la redevance Pollution Diffuse en cours  */
		date-rpd = today.
		RUN LECT-RPD.
		RUN CALC-TARIF-RPD-TTC.   
		nbpxa = nbpxa + 1.
	
		RUN ECRITURE-STREAM.  
			
   end.   /*  boucle sur tarifs futurs   */
        
	/*--------------------------------------------------*/ 	
	/*  On extrait tous les tarifs cli actifs           */
	/*--------------------------------------------------*/	
	val-tarif = 0.
	find first {tarcli.i} where {tarcli.i}.motcle = "VTE"
			and   {tarcli.i}.codsoc =  entry(ind-soc, l-soc-pxa)
			and   {tarcli.i}.articl =  artapr.articl
			and   {tarcli.i}.date-debut <= today
			and   {tarcli.i}.date-fin = ? 
			and   {tarcli.i}.typaux =  ""
			and   {tarcli.i}.codaux =  ""
			and   {tarcli.i}.code-tarif = "CLI"
		no-lock no-error.
				
		if not available {tarcli.i}
			then next.
		if {tarcli.i}.valeur = 0 or {tarcli.i}.valeur = 0.001
			then next.
			
		val-tarif = {tarcli.i}.valeur.
        RUN RECHERCHE-AGC.
		/* On ajoute � chaque tarif le montant de la redevance Pollution Diffuse en cours  */
		date-rpd = today.
		RUN LECT-RPD.
		RUN CALC-TARIF-RPD-TTC.   
		nbpxa = nbpxa + 1.

		RUN ECRITURE-STREAM.
		
    end.   /*  boucle sur soci�t� article   */                                  
END PROCEDURE. /* MAJ_TAR_PA */  
/*============================================================================================================*/
/* recherche du tarif AGC */
PROCEDURE RECHERCHE-AGC.    
/* On v�rifie s'il existe un tarif Agil AGC correspondant au tarif cli             */
		find tarcli-agc 
            where tarcli-agc.motcle =  {tarcli.i}.motcle
            and   tarcli-agc.codsoc =  {tarcli.i}.codsoc
			/* DGR 03082010   and   tarcli-agc.datmaj =  {tarcli.i}.datmaj */
            and   tarcli-agc.date-debut =  {tarcli.i}.date-debut
            and   tarcli-agc.typaux =  {tarcli.i}.typaux
            and   tarcli-agc.codaux =  {tarcli.i}.codaux
            and   tarcli-agc.code-tarif = "AGC"
			/* rajout DGR 03082010 acces avec code article */
            and   tarcli-agc.articl =  {tarcli.i}.articl
        no-lock no-error.
		if available tarcli-agc
            then assign val-tarif = tarcli-agc.valeur.
END PROCEDURE. /* recherche tarif AGC */   