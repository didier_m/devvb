/*==========================================================================*/
/*                           E X T - C R M S T A T  . I                     */
/* Extraction des donnees pour VENTES PARTNER                               */
/*--------------------------------------------------------------------------*/
/* Extraction statistiques                                                  */
/*==========================================================================*/

   Lib_famille = " ".
   Lib_Ss_Famille = " " .
   Lib_Ss_Ss_Famille = " ".
   Designation = " ".
   Famille = " ".
   Ss_Famille = " ".
   Ss_Ss_Famille = " ".
/*----------------------------------------------------------------------------*/
/* si l'article est bloque, il n'a pas �t� transmis lors de l'initialisation  */
/* donc il faut transmettre dans ce cas son libelle et les infos nomenclature */
/*----------------------------------------------------------------------------*/
  if artapr.code-blocage = "F" or
     artapr.code-blocage = "B" then do:
/*-----------------------------------------------*/     
/*  Recherche du libelle de la famille article   */
/*-----------------------------------------------*/
   find tabges where
        tabges.codsoc = " "
    and tabges.etabli = " "
    and tabges.typtab = "ART"
    and tabges.prefix = "FAMART"
    and tabges.codtab = artapr.famart
    no-lock no-error.
                               
    if available tabges then
       Lib_famille = tabges.libel1[1].
 
   find tabges where
        tabges.codsoc = " "
    and tabges.etabli = " "
    and tabges.typtab = "ART"
    and tabges.prefix = "SOUFAM" + artapr.famart
    and tabges.codtab = artapr.soufam
    no-lock no-error.
             
    if available tabges then
       Lib_Ss_famille = tabges.libel1[1].
 
    find tabges where
         tabges.codsoc = " "
     and tabges.etabli = " "
     and tabges.typtab = "ART"
     and tabges.prefix = "SSFM" + artapr.famart + artapr.soufam
     and tabges.codtab = artapr.sssfam
     no-lock no-error.
              
    if available tabges then
       Lib_Ss_Ss_famille = tabges.libel1[1].

    Famille = artapr.famart.
    Ss_Famille = artapr.soufam.
    Ss_Ss_Famille = artapr.sssfam.
    Designation = artapr.libart1[1].
    
    end. /* si libelle � transmettre */
    
    No_client = trim(stacli.codsoc) + "/" + substr(stacli.codaux,5,6).
    No_article = trim(stacli.codsoc) + "/" + caps(stacli.famart).
    
    i = month(date-ext).
    
   if stacli.qte[i] = 0 and
     stacli.fac-ht[i] = 0 then
     next.
     
   Qte = stacli.qte[i].
   
/*-----------------------------------------------------------*/   
/* On transforme le poids en tonnage pour certaines familles */
/* sauf pour les doses de ma�s dans la famille 011           */
/*-----------------------------------------------------------*/   
   if artapr.pds <> 0 and 
      (lookup(artapr.famart, "012,013,030,032") <> 0 or
       (artapr.famart = "011" and artapr.soufam <> "003"))  then
      Qte = stacli.qte[i] * artapr.pds / 1000.
            
   Montant = stacli.fac-ht[i].
   wmois = string(i,"99").
   wannee = integer(stacli.annee) - 2000.
   wannee-plus = integer(stacli.annee) - 1999.
   wannee-moins = integer(stacli.annee) - 2001.
       
   Annee_Mois = stacli.annee + "-" + wmois.
    
   if i < 7 then
      Campagne = string(wannee-moins, "99") + "-" + 
                 string(wannee, "99").
   else
      Campagne = string(wannee, "99") + "-" +
                 string(wannee-plus, "99"). 
         
   Cle = No_Client + Campagne + Annee_Mois + No_article.
    
    if artapr.famart = "012" or
       artapr.famart = "030" or
       artapr.famart = "032" then
          export stream crm-stat-pa delimiter ";"
          No_client Annee_Mois No_article
          Qte Montant Campagne
          Cle
          Designation Famille Lib_Famille 
          Ss_Famille Lib_Ss_Famille Ss_Ss_Famille Lib_Ss_Ss_Famille.
    else          
          export stream crm-stat-pv delimiter ";"
          No_client Annee_Mois No_article 
          Qte Montant Campagne
          Cle
          Designation Famille Lib_Famille 
          Ss_Famille Lib_Ss_Famille Ss_Ss_Famille Lib_Ss_Ss_Famille.
