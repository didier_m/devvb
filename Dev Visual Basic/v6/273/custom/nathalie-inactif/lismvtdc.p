/* lismvtdc.p Extraction des mouvements pour le QUALITE DEPOT CENTRAL    */
/* DGR 03102003 : rajout nb de lignes en sortie + nb total               */
/*                suppression CDS                                        */
/* DGR 24112004 : pour V52                                               */
/*-------------------------------------------------- */
/*  Acces parametre lecture societe article ==> OK   */
/*-------------------------------------------------- */

 {connect.i}
def var sdtmin        like                 lignes.datdep             no-undo.
def var sdtmax        like                 lignes.datdep             no-undo.

def var sequence     as Char     format "x(5)".
def var sel-sequence as Char     format "x(12)".
def var l-typ        as char extent 3 .
def var ii           as integer.
def var wart as char format "x".
def var wsoc as char format "xxxx".

def var wtyp    as char format "xxx".

def var wcpt    as int no-undo.
def var wcpt1   as int no-undo.

def var wmont as dec decimals 2 format "999999999.99-".

def var wlib as char format "x(30)".
def var Famt as char format "x(9)".
def var wfam as char format "x(3)".
def var wsfa as char format "x(3)".
def var wssf as char format "x(3)".
def var wqte1 as dec decimals 3 format ">>>,>>>,>>9.999-".
def var wqte2 as dec decimals 3 format ">>>,>>>,>>9.999-".
def var warticl as char format "x(6)".
def var articl-soc as char no-undo.

def buffer b-lignes for lignes.
def stream fsortie.
/* d�termination du N� de s�quence operateur */
 assign codsoc = " "
       etabli = " "
       typtab = "ope"
       prefix = " nume-spool"
       codtab = operat.
run opeseq.p ("ldc", output sequence, output sel-sequence).
/* fin */

output stream fsortie to value( sel-sequence) .

sdtmin = today - 1 .
sdtmax = today - 1 .

form
     "  " skip
     "Societe ..... : " at 01 codsoc-soc colon 17 no-label
     skip
     "Mouvements du : " at 01 sdtmin     colon 17 no-label
     "au"               at 33      sdtmax        colon 36 no-label
     skip
     skip
/* DGR 03102003 : on n'extrait pas les CDS
     "Bons concernes : CDC,CDS,TRS / Sites concernes : 004,005 "
            at 01
*/
/*
     "Bons concernes : CDC,TRS / Sites concernes : 004,005 "
*/
     "Bons concernes : LIV,TRS / Sites concernes : 004,005 "
            at 01
     skip skip
     "Spool ....... : "       at 01      sel-sequence  colon 17 no-label

       with title "lismvtdc.p : Liste des Qtes Livrees < Qtes Commandees sur l'encours  ".

regs-app = "NATHALIE" .
{ regs-cha.i }

{ regs-inc.i "articl" }
articl-soc = regs-soc .

display sdtmin sdtmax codsoc-soc sel-sequence.
update sdtmin
       help "date creation minimale"
       validate (sdtmin <> ?,"blanc refuse").
update sdtmax
       help "date creation maximale"
       validate (sdtmax <> ?,"blanc refuse").

/* DGR 03102003 : on n'extrait pas les CDS
l-typ[1] = "CDC". l-typ[2] = "CDS". l-typ[3] = "TRS".
*/

l-typ[1] = "LIV". l-typ[2] = "TRS".

export stream fsortie delimiter ";"
"So" "Mcle" "Mag" "Typ" "Numbon" "Niv" "Client"
"Date Liv" "Date fact" "FAMT" "Fam" "Sfa" "SSf" "Article" "Libelle"
"Qte Cdee" "Qte Liv"
"Prix net" "Montant".

assign wcpt = 0 wcpt1 = 0.

do ii = 1 to 2:

rech-lignes:
for each lignes where lignes.codsoc = codsoc-soc  and
                      lignes.motcle = "VTE"       and
                      lignes.typcom = l-typ[ii]   and
                       sdtmin <= lignes.datdep    and
		       sdtmax >= lignes.datdep    and
                      (magasin = "004" or magasin = "005" ) and
                      nivcom >= "3" and nivcom < "7"
                      /* use-index datdep */ no-lock :
wcpt = wcpt + 1.

find b-lignes
where b-lignes.codsoc = lignes.codsoc
and   b-lignes.motcle = lignes.motcle
and   b-lignes.typcom = lignes.typcom-reliq
and   b-lignes.numbon = lignes.numbon-reliq
and   b-lignes.chrono = lignes.chrono-reliq
no-lock no-error.
if not available b-lignes then next rech-lignes.
WQTE1 = b-lignes.qte[1].
WQTE2 = lignes.qte[2].


if wqte1 < 0 then wqte1 = wqte1 * -1.
if wqte2 < 0 then wqte2 = wqte2 * -1.

If wqte2 >= wqte1 then next rech-lignes .

wcpt1 = wcpt1 + 1.

find artapr where artapr.codsoc = articl-soc and
                  artapr.articl = lignes.articl no-lock no-error.

if not available artapr then do:
                             wlib = "? ? ?".
                             famt = "? ? ?".
                             wfam = "?".
                             wsfa = "?".
                             wssf = "?".
                             end.
                        else do:
                             wlib = artapr.libart1[1].
                             wfam = artapr.famart.
                             wsfa = artapr.soufam.
                             wssf = artapr.sssfam.
                             famt = wfam + wsfa + wssf.
                            end.
wmont = lignes.htnet.
warticl = caps(lignes.articl).
export stream fsortie delimiter ";"
lignes.codsoc
lignes.motcle
lignes.magasin
lignes.typcom
lignes.numbon format "x(12)"
lignes.nivcom
lignes.codaux
lignes.datdep
lignes.datfac
famt
wfam
wsfa
wssf
warticl format "X(6)"
wlib
b-lignes.qte[1]
lignes.qte[2]
lignes.pu-net
wmont.
end.


end.
export stream fsortie delimiter ";"
"nb lignes en sortie" wcpt1 "nb lignes total " wcpt.
