/*=======================================================================*/
/*                      E X T - S T A T M A G 0 . P                      */
/*      Creation table STARTI pour les statistiques sous Magasins        */
/*-----------------------------------------------------------------------*/
/*      Programme Principal                                              */
/*=======================================================================*/

{ connect.i            }
{ ext-statmag0.i "new" }
{ bat-ini.i      "new" }
{ aide.i         "new" }
{ chx-opt.f            }

/*-----------------------------------------------------------------------*/
/*                        Definition de variables                        */
/*-----------------------------------------------------------------------*/

DEF VAR std-chxbas AS CHAR NO-UNDO.

/*-----------------------------------------------------------------------*/
/*                       Chargement aide et texte                        */
/*-----------------------------------------------------------------------*/

aide-fichier = "ext-statmag0.aid".
aide-par     = "".

RUN aide-lec.p.

DO ind = 1 TO 30 :
    aide-m = "libel" + STRING( ind , "99" ).
    { aide-lib.i }
    libelle[ ind ] = aide-lib.
END.

/*-----------------------------------------------------------------------*/
/*                          Affichage ecrans                             */
/*-----------------------------------------------------------------------*/

libsoc-soc = "".
{ fr-cadre.i lib-prog }

      DISPLAY         libelle[1 for 3] WITH FRAME fr-saisie.
COLOR DISPLAY MESSAGE libelle[1 for 3] WITH FRAME fr-saisie.

/*-----------------------------------------------------------------------*/
/*                        Parametres de la Saisie                        */ 
/*-----------------------------------------------------------------------*/

SAISIE-GENERALE :
REPEAT :
    
    RUN ext-statmagsp.p .
    IF keyfunction(lastkey) = "end-error" THEN LEAVE SAISIE-GENERALE.
    
    /*----------------------------------*/
    /* Acceptation, Refus, Modification */
    /*----------------------------------*/
    
    { standard.i -chxopt std-chxbas }
    
    ASSIGN chx-milieu = 61
           chx-def    = 1
           chx-ombre  = "0".
           
    { chx-opt.i "3" "std-chxbas" "15" "15" "17" " " "fr-acc" }
           
    IF chx-trait = 1 THEN LEAVE SAISIE-GENERALE.   /* Acceptation  */
    IF chx-trait = 2 THEN LEAVE SAISIE-GENERALE.   /* Refus        */
    IF chx-trait = 3 THEN NEXT  SAISIE-GENERALE.   /* Modification */

END.  /* du repeat */

/*-----------------------------------------------------------------------*/
/*                       Lancement du traitement                         */
/*-----------------------------------------------------------------------*/

IF        chx-trait = 1 
AND       keyfunction(lastkey) <> "end-error"
THEN DO :
          batch-tenu = yes.  /* Gestion en batch */
          
          /*-----------------------------------------*/
          /* Sauvegarde parametres saisis pour batch */
          /*-----------------------------------------*/
          
          { bat-zon.i "l-magasins" "l-magasins" "c" "''" "libelle[1] "   } 
          { bat-zon.i "date-debut" "date-debut" "d" "''" "libelle[2] "   }
          { bat-zon.i "date-fin"   "date-fin"   "d" "''" "libelle[3] "   }

          /*------------------*/  
          /*     Execution    */
          /*------------------*/
          
          { bat-maj.i "ext-statmag1.p" }

END. /* fin do */

/*-----------------------------------------------------------------------*/
/*                            Effacement ecrans                          */
/*-----------------------------------------------------------------------*/

HIDE FRAME fr-saisie NO-PAUSE.
HIDE FRAME fr-titre  NO-PAUSE.
HIDE FRAME fr-cadre  NO-PAUSE.

/*-----------------------------------------------------------------------*/
/*                   Suite dans le programme ext-statmag1.p              */
/*-----------------------------------------------------------------------*/