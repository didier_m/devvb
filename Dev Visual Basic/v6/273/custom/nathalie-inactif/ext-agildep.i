/*==========================================================================*/
/*                           E X T - A G I L D E P  . I                     */
/* Extraction des donnees pour AGIL                                         */
/*--------------------------------------------------------------------------*/
/* Extraction Magasins                                                      */
/*==========================================================================*/
/*==========================================================================*/
/*   VLE 06052010  ajout de la gestion des magasins test                    */
/*   VLE 06102010  suppression trt societe 43                               */
/*   VLE 28012011  ajout VITRY societe 59                                   */
/*   VLE 03012012 :la gamme du magasin 991 contient tous les produits de    */
/*                toutes les soci�t�s pour tester toutes les OP (TOTALE)    */ 
/*==========================================================================*/

/*-------------------------------------------*/                           
/*  Ecriture du d�tail de section Magasins   */
/*-------------------------------------------*/ 

ID_MAGASIN = magasi.codtab.                       
NM_MAGASIN = magasi.adres[2].
if magasi.adres[2] = " " 
   then NM_MAGASIN = magasi.adres[1].
NM_MAGASIN = replace(NM_MAGASIN, "Magasin", "Mag.").
LB_ADRESSE1     = substr(magasi.adres[1], 1, 30). 
LB_ADRESSE2 = substr(magasi.adres[3], 1, 30).
CD_POSTAL = substr(magasi.adres[5],1,5).
LB_VILLE = substr(magasi.adres[5],7,26).
if substr(magasi.adres[5],1,5) = " " or
   substr(magasi.adres[5],1,5) = "(Ferm" or
   substr(magasi.adres[5],1,5) = "Ferme"
   then do :
                CD_POSTAL = substr(magasi.adres[4],1,5).
                LB_VILLE = substr(magasi.adres[4],7,26).
end.
LB_PAYS = "FRANCE".
NO_TEL = magasi.teleph-mag.
NO_FAX = magasi.fax-mag.

If magasi.equipe = "A" 
        then ID_TYPE_MAGASIN = "BTQ".
        else ID_TYPE_MAGASIN = "FER".

if lookup(magasi.codtab, l-mag-test) <> 0
	then ID_TYPE_MAGASIN = "TEST".

put stream agil unformatted
                      Type_Ligne_Det ID_MAGASIN "~011" NM_MAGASIN "~011" LB_ADRESSE1 "~011" LB_ADRESSE2 "~011"                    
                          LB_ADRESSE3 "~011" CD_POSTAL "~011" LB_VILLE "~011" LB_PAYS "~011" NO_TEL "~011" NO_FAX "~011" FL_RECHARGE "~011"     
                          FL_SAISIE_PVSL "~011" ID_LANGUE "~011" ID_PAYS "~011" ID_TYPE_MAGASIN "~015" skip. 
                          
/*-------------------------------------------*/                           
/*  Ecriture de la section Gamme Magasin     */
/*-------------------------------------------*/ 

Nom_Section_Deb = "GAMME_MAGASIN".
put stream temp_gam unformatted
        Type_Ligne_Deb Nom_Section_Deb "~011" ID_MAGASIN "~015" skip.

CD_GAMME = "GV".
if magasi.societe = "08"
   then CD_GAMME = "ASEC".
/* VLE 28012011  */
if magasi.societe = "59"
    then CD_GAMME = "VITRY".
if lookup(magasi.codtab, l-mag-test) <> 0
	then CD_GAMME = "TOTALE".
put stream temp_gam unformatted
        Type_Ligne_Det CD_GAMME "~015" skip.

Nom_Section_Fin = "GAMME_MAGASIN".
put stream temp_gam unformatted 
        Type_Ligne_Fin Nom_Section_Fin "~015" skip. 