/* lecart1.p : Liste des bons avec ecart de prix par rapport au CLI ou au PRO */
/*-------------------------------------------------- */
/*  Acces parametre lecture societe article ==> OK   */
/*-------------------------------------------------- */

{connect.i}
{ bat-ini.i  new }
{ lecart0.i   new }

def input parameter  fichier  as char format "x(12)" .

def new shared stream sortie.
def var wttc               as deci format ">>>>>>9.99" no-undo.
def var wht                as deci format ">>>>>>9.99" no-undo.
def var wtarttc            as deci format ">>>>>>9.99" no-undo.
def var wtarpro            as deci format ">>>>>>9.99" no-undo.
def var wdatttc            as date format "99/99/9999" no-undo.
def var wdatpro            as date format "99/99/9999" no-undo.
def var articl-soc         as char                     no-undo.  

if keyfunction( lastkey ) = "end-error" then return.

/* Chargement parametres selection */

run bat-lec.p ( fichier ) .

{ bat-cha.i  "lib-prog"    "lib-prog"    "a" }
{ bat-cha.i  "codsoc-soc"  "codsoc-soc"  "a" }
{ bat-cha.i  "libsoc-soc"  "libsoc-soc"  "a" }
{ bat-cha.i  "wdatdeb"     "wdatdeb"     "d" }
{ bat-cha.i  "wdatfin"     "wdatfin"     "d" }
{ bat-cha.i  "wtopfam"     "wtopfam"     "a" }
{ bat-cha.i  "periph"      "periph"      "a" }

regs-app = "NATHALIE" .
{ regs-cha.i }

{ regs-inc.i "articl" }
articl-soc = regs-soc .

message "lecart1.p - extraction liste des ecarts - debut a : " string(time,"hh:mm").
message " " .

output stream sortie to value(periph).
    export stream sortie delimiter ";"
      "Societe"
      "Magasin"
      "Article"
      "Designation"
      "Famille"
      "Sous Fam"
      "SS Fam"
      "Numero Bon"
      "Type Bon"
      "Date creation"
      "Date Facturation"
      "Quantite"
      "CA total HT"
      "CA total TTC"
      "Px Vente Unit TTC"
      "Px Vente Unit HT"
      "Date CLI"
      "Tarif CLI"
      "Date PRO"
      "Tarif PRO".

wcpteca = 0.
wcptlus = 0.

for each artapr
where artapr.codsoc = articl-soc
and   ( (wtopfam = "a" and artapr.famart <= "010" ) or
        (wtopfam = "b" and artapr.famart >  "010" ) or
        wtopfam = "c" )
no-lock,
    each bastat use-index article
where bastat.codsoc = codsoc-soc
and   bastat.articl = artapr.articl
and   bastat.datdep >= wdatdeb
and   bastat.datdep <= wdatfin
and   bastat.motcle = "VTE"
and   bastat.typcom = "LIM"
no-lock :
  wcptlus = wcptlus + 1.
  wttc = round (bastat.fac-ttc / bastat.qte ,2).
  wht = round (bastat.fac-ht / bastat.qte ,2).
  wtarttc = 0.
  wdatttc = ?.

  find first tarcli
  where tarcli.codsoc = bastat.codsoc
  and   tarcli.motcle = "VTE"
  and   tarcli.typaux = ""
  and   tarcli.codaux = ""
  and   tarcli.code-tarif = "CLI"
  and   tarcli.articl = bastat.articl
  and   tarcli.date-debut <= bastat.datbon
  no-lock no-error.
  if available tarcli then do:
    wtarttc = tarcli.valeur.
    wdatttc = tarcli.date-debut.
    if tarcli.date-debut < 12/16/2001 then
      wtarttc = round( wtarttc / 6.55957,2).
  end.

  wtarpro = 0.
  wdatpro = ?.
  find first tarcli use-index articl
  where tarcli.codsoc = bastat.codsoc
  and   tarcli.motcle = "VTE"
  and   tarcli.typaux = ""
  and   tarcli.codaux = ""
  and   tarcli.code-tarif = "PRO"
  and   tarcli.articl = bastat.articl
  and   tarcli.date-debut <= bastat.datbon
  and   tarcli.date-fin >= bastat.datbon
  no-lock no-error.
  if available tarcli then do:
    wdatpro = tarcli.date-debut.
    wtarpro = tarcli.valeur.
    if tarcli.date-debut < 12/16/2001 then
      wtarpro = round( wtarpro / 6.55957,2).
  end.

  if wtarttc <= 0.001 and
     wtarpro <= 0.001 then next.

  /* abs permet de ne pas selectionner les avoirs */
  if abs(wttc) <> wtarttc then do:
    if abs(wttc) <> wtarpro then do:
      export stream sortie delimiter ";"
             bastat.codsoc
             bastat.magasin
             artapr.articl
             artapr.libart
             artapr.famart
             artapr.soufam
             artapr.sssfam
             bastat.numbon
             bastat.typcom
             bastat.datbon
             bastat.datfac
             bastat.qte
             bastat.fac-ht
             bastat.fac-ttc
             wttc
             wht
             wdatttc
             wtarttc
             wdatpro
             wtarpro.
      wcpteca = wcpteca + 1.
      if wcpteca modulo 100 = 0 then message "Nb lus " wcptlus "Nb ecarts" wcpteca .
    end.
  end.
end. /* for each */
output stream sortie close.

message "Nb lus " wcptlus "Nb ecarts" wcpteca .
message " " .

message "lecart1.p - extraction liste des ecarts - fin a   : " string(time,"hh:mm").
