/*==========================================================================*/
/*                          A C T V - D A T . I                             */
/*==========================================================================*/
/* Chargement des Dates d'Exercice                                          */
/*   a partir des param ACTV dans TABGES                                    */
/*==========================================================================*/

def var date-butoir     as date     format 99/99/9999   no-undo .
def var date-pre-deb    as date     format 99/99/9999   no-undo .
def var date-pre-fin    as date     format 99/99/9999   no-undo .

/* lecture TABGES */
Find TABGES
where tabges.codsoc = " "
and   tabges.etabli = " "
and   tabges.typtab = "PAR"
and   tabges.prefix = "ACTV"
and   tabges.codtab = "ACTV"
no-lock no-error .

if available TABGES
then do :
    if tabges.dattab[1] <> ? then assign date-butoir = tabges.dattab[1] .
    if tabges.dattab[2] <> ? then assign date-pre-deb = tabges.dattab[2] .
    if tabges.dattab[3] <> ? then assign date-pre-fin = tabges.dattab[3] .
end .
ELSE DO :
    date-butoir  = DATE ("0109" + string(year(today)) ) .
    date-pre-deb = DATE ("0107" + string(year(today) - 1) ) .
    date-pre-fin = DATE ("3006" + string(year(today)) ) .
END.

/* interpretation des dates recuperees ci-dessus : la date butoir permet de  */
/* determiner a partir de quelle date on passe a l'exercice suivant          */
/* on recupere les nbexe derniers exercices a partir :                       */
/*  - soit de l'exercice en cours                                            */
/*  - soit de l'exercice suivant                                             */

IF TODAY < date-butoir  /* on considere l'exercice en cours */
THEN DO j = 1 TO nbexe:
  date-deb[j] = INT(YEAR(date-pre-deb)) * 100 +
                INT(MONTH(date-pre-deb))      +
                (j - nbexe) * 100.
  date-fin[j] = INT(YEAR(date-pre-fin)) * 100 +
                INT(MONTH(date-pre-fin))      +
                (j - nbexe) * 100.
END.
ELSE DO j = 1 TO nbexe:    /* on considere l'exercice suivant */
  date-deb[j] = INT(YEAR(date-pre-deb) + 1) * 100 +
                INT(MONTH(date-pre-deb))      +
                (j - nbexe) * 100.
  date-fin[j] = INT(YEAR(date-pre-fin) + 1) * 100 +
                INT(MONTH(date-pre-fin))      +
                (j - nbexe) * 100.
END.

/* pour l'exercice en cours on s'arrete a la date d'extraction */
date-fin[nbexe] = int(year(date-ext) * 100 + month(date-ext)).

campmax = year(date-ext).
if month (date-ext) >= 7 and month (date-ext) <= 12 then
  campmax = campmax + 1.
