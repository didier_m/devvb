/* Tableau de bord journalier */

/* variables pour societes */
def {1} shared var    wcodsoc    as char                   no-undo.
def {1} shared var    wlibsoc    like libsoc-soc           no-undo.
def {1} shared var    l-soc      as char                   no-undo.
def {1} shared var    l-mag      as char format "x(120)"    no-undo.
def {1} shared var    wmag       as char                   no-undo.
def {1} shared var    l-libmag   as char                   no-undo.

def {1} shared var    wdatdeba     as date format "99/99/9999" no-undo.
def {1} shared var    wdatfina     as date format "99/99/9999" no-undo.
def {1} shared var    wdatdebl     as date format "99/99/9999" no-undo.
def {1} shared var    wdatfinl     as date format "99/99/9999" no-undo.
def {1} shared var    wdatdebv     as date format "99/99/9999" no-undo.
def {1} shared var    wdatfinv     as date format "99/99/9999" no-undo.
def {1} shared var    wdatederarr as date format "99/99/9999" no-undo.

def {1} shared var    wprefix    as char                   no-undo.
def {1} shared var    libelle       as char                 extent 99   no-undo .

def {1} shared var periph       as char format "x(12)"              no-undo .
def {1} shared var periph2      as char format "x(12)"              no-undo .

def {1} shared var z-choix      as char                             no-undo .
def {1} shared var zone         as char                             no-undo .
def {1} shared var sav-periph   as char format "x(12)"              no-undo .
def {1} shared var sav-periph2   as char format "x(12)"              no-undo .

def {1} shared var nb-lus       as int                              no-undo .
def {1} shared var i            as int                              no-undo .
def {1} shared var k            as int                              no-undo .

def {1} shared var wtypaux      like auxapr.typaux                  no-undo .
def {1} shared var wcodaux      like auxapr.codaux                  no-undo .
def {1} shared var wlibaux      like auxapr.adres[1]                no-undo .
def {1} shared var wtyppro      as char format "x"                  no-undo .
def {1} shared var wlibpro      as char format "x(20)"              no-undo .
def {1} shared var wcodpro      as char format "XXXXX"              no-undo .
def {1} shared var wrefpro      as char format "x(30)"              no-undo .
def {1} shared var wcodpro2     as char format "XXXXX"              no-undo .
def {1} shared var wrefpro2     as char format "x(30)"              no-undo .
/*------------------------------------------------------------------------*/
def {1} shared frame fr-saisie .

form libelle[  1 ] format "x(32)" wcodsoc format "xx" wlibsoc colon 38 skip
     libelle[  2 ] format "x(32)" l-soc                                skip
     libelle[  3 ] format "x(32)" wtyppro wlibpro                colon 38 skip
     libelle[  4 ] format "x(32)" wcodpro wrefpro format "x(30)" colon 38 skip
     libelle[  5 ] format "x(32)" wcodpro2 wrefpro2 format "x(30)" colon 38 skip
     libelle[  6 ] format "x(32)" wcodaux wlibaux                 skip
     libelle[  7 ] format "x(32)" wdatdeba                        skip
     libelle[  8 ] format "x(32)" wdatfina                        skip
     libelle[  9 ] format "x(32)" wdatdebl                        skip
     libelle[ 10 ] format "x(32)" wdatfinl                        skip
     libelle[ 11 ] format "x(32)" wdatdebv                        skip
     libelle[ 12 ] format "x(32)" wdatfinv                        skip
     libelle[ 13 ] format "x(32)" l-mag  format "x(40)"           skip
     libelle[ 14 ] format "x(32)" periph                          skip
     libelle[ 15 ] format "x(32)" periph2                         skip
     with frame fr-saisie
     row 3  centered  overlay  no-label  { v6frame.i } .