/*==========================================================================*/
/*                           arc-bas10.i                                    */
/* Archivage Bastat                                                         */
/*--------------------------------------------------------------------------*/
/* Variables et Frames                                       Jdq 08/06/2000 */
/*==========================================================================*/

/* Variables */
def var ind         as int                      no-undo.
def var wok         as char                     no-undo.
def var std-chxbas  as char format "x(80)"      no-undo.

def {1} shared var libelle      as char extent 30              no-undo.
def {1} shared var motcles      as char format "x(40)"         no-undo.
def {1} shared var datmax       as date format "99/99/9999"    no-undo.
def {1} shared var datmin       as date format "99/99/9999"    no-undo.
def {1} shared var basenameo    as char format "x(16)"         no-undo.
def {1} shared var fieldo       as char format "x(16)"         no-undo.
def {1} shared var wpgm         as char format "x(20)"         no-undo.
def {1} shared var commande     as char format "x(75)"         no-undo.
def {1} shared var wcpt         as integer                     no-undo.
def {1} shared var wcptl        as integer                     no-undo.
def {1} shared var wcpte        as integer                     no-undo.
def {1} shared var wcptm        as integer                     no-undo.
def {1} shared var i            as integer                     no-undo.

/* pour opeseq.p */
def {1} shared var sequence     as char format "x(5)"          no-undo.
def {1} shared var sel-sequence as char format "x(12)"         no-undo.

/* Dessin des Frames */
def {1} shared frame fr-saisie.

Form libelle[01] format "x(16)" motcles             skip
     libelle[02] format "x(16)" datmax              skip
     libelle[03] format "x(16)" basenameo           skip
     with frame fr-saisie row 7 centered no-label overlay { v6frame.i } .
