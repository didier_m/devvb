/* DGR 27062011 : on veut maintenant les stat des la fam 021/005 */
      if artapr.famart <= "010"
         or (artapr.famart = "011" and 
            lookup(artapr.soufam, "000,001,006,020,097,099,ZZZ") <> 0)
         or (artapr.famart = "012" and
            lookup(artapr.soufam, "001,008,020,096,097,098") <> 0)
         or (artapr.famart = "013" and
            lookup(artapr.soufam, "001,007,008,012,020,097,098,099") <> 0)
         or (artapr.famart = "014" and
            lookup(artapr.soufam, "001,009,010,020,097,098,099") <> 0)
         or artapr.famart = "015"
         or artapr.famart = "016"
         or (artapr.famart = "017" and
             lookup(artapr.soufam, "001,002,004,005,020,097,098") <> 0)
         or (artapr.famart = "018" and
            lookup(artapr.soufam, "001,020,097,098") <> 0)
         or (artapr.famart = "019" and
            lookup(artapr.soufam, "001,003,020,097") <> 0)
         or (artapr.famart = "019" and artapr.soufam = "002" and
            lookup(artapr.sssfam, "01,03") <> 0)
         or artapr.famart = "020"
         or (artapr.famart = "021" and
            lookup(artapr.soufam, "001,002,009,012,013,014,015,020,097") <> 0)
         or (artapr.famart = "021" and artapr.soufam = "007" and
            lookup(artapr.sssfam, "11,12,13,14,15,16,18") <> 0)    
         or (artapr.famart = "030" and
            lookup(artapr.soufam, "001,020,097,098") <> 0)
         or (artapr.famart = "032" and
            lookup(artapr.soufam, "001,020,096,097,098") <> 0)
         or artapr.famart = "034"
         or artapr.famart = "049"   
         or (artapr.famart = "052" and
            lookup(artapr.soufam, "001") <> 0)
         or (artapr.famart = "081" and
            lookup(artapr.soufam, "002,003,004") <> 0)
         or artapr.famart >= "082"
         then next.
