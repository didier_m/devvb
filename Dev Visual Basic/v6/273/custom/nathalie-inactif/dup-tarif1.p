/* ------------------------------------------------------------------------- */
/* duplication des creations / modification tarifs                           */
/* de la societe 01 sur les societes 05 08 pour la famille 15                */
/* de la societe 01 sur la societe 05 pour les familles GP                   */
/* DGR 100108 : on ne duplique plus rien sur 05                              */
/* DGR 080311 : on duplique les CLI et HTC fam <=010 de 01==> 08             */
/* DGR 040815 : batch perio                                                  */
/* CCO 12072019 : ajout {tarcli.i}                                           */
/* ------------------------------------------------------------------------- */
/* dup-tarif1.p : programme de duplication                                   */
/* ------------------------------------------------------------------------- */

{connect.i}
{ bat-ini.i  "new" }
{ sel.i      "new" }

def buffer date-tabges for tabges.
def var type-trt   as char.
def var periph           as char        format "x(15)"       no-undo.
def var wdate-deb        as date        format "99/99/99"    no-undo.
def var wdate-fin        as date        format "99/99/99"    no-undo.
def var nb-lus           as int                              no-undo.
def var nb-tarclilus     as int                              no-undo.
def var nb-tarcli08cre   as int                              no-undo.
def var nb-tarcli08maj   as int                              no-undo.
def var wdatmaj          like {tarcli.i}.datmaj                  no-undo.
def var wheumaj          like {tarcli.i}.heumaj                  no-undo.
def var wopemaj          like {tarcli.i}.opemaj                  no-undo.
def var wcodsoc          like {tarcli.i}.codsoc                  no-undo.
def var nb-opecre        as int                              no-undo.
def var nb-opemaj        as int                              no-undo.
def var lcode-tarif      as char                             no-undo.
def var lcode-fam        as char                             no-undo.
def var heure-debut      as int .

def buffer b-tarcli for {tarcli.i}.

def stream sortie .

def input parameter  fichier  as char format "x(12)".

run bat-lec.p ( fichier ) .
{ bat-cha.i  "type-trt"    "type-trt"   "a" }
{ bat-cha.i  "periph"      "periph"     "a" }
/* debut ajout pour batch periodique */
{ bat-cha.i "sel-applic"   "sel-applic"   "a" }
{ bat-cha.i "sel-filtre"   "sel-filtre"   "a" }
{ bat-cha.i "sel-sequence" "sel-sequence" "a" }

/* fin ajout pour batch periodique */ 

output stream sortie to value(sel-sequence).

assign 	typtab = "TAR"
		prefix = "DUP-TAR"
		codtab = "DERN-TRAIT".

FIND DATE-TABGES 
where date-tabges.codsoc = ""
and   date-tabges.etabli = ""
and   date-tabges.typtab = typtab
and   date-tabges.prefix = prefix
and   date-tabges.codtab = codtab
  no-lock no-error.

if not available date-tabges then do :
	create date-tabges.
	assign 	date-tabges.codsoc    = ""
			date-tabges.etabli    = ""
			date-tabges.typtab    = typtab
			date-tabges.prefix    = prefix
			date-tabges.codtab    = codtab
			date-tabges.dattab[1] = today - 1 .
end.

assign	wdate-deb = date-tabges.dattab[1] + 1
		wdate-fin = today.
 
 
heure-debut = time.
wdatmaj = today.
wheumaj = string(time,"hh:mm").
wopemaj = "BVL".
assign lcode-tarif = "CLI,HTC,PRO,CTE"
       lcode-fam = "001,002,003,004,005,006,007,008,009,010,015".  


message "Duplication Tarif 01 -> 08 --- debut traitement du : " wdatmaj " a: "  string ( time, "hh:mm:ss")  "--- tarifs modifi�s entre le " wdate-deb "et le " wdate-fin.


/*-----------------------------------------------*/
/* recherche des tarifs 01 VTE cr��s ou modifi�s */
/*-----------------------------------------------*/

for each {tarcli.i} use-index k-datmaj
where {tarcli.i}.codsoc = "01"
and   {tarcli.i}.motcle = "VTE"
and   {tarcli.i}.typaux = ""
and   {tarcli.i}.codaux = ""
and   {tarcli.i}.articl <> " "
/*     and   lookup({tarcli.i}.code-tarif, "CLI,HTC,REV,PRO,CTE") <> 0 */
and   lookup({tarcli.i}.code-tarif,lcode-tarif ) <> 0 
and   {tarcli.i}.opecre <> "DUP" 
and   {tarcli.i}.datmaj >= wdate-deb 
and   {tarcli.i}.datmaj <= wdate-fin
no-lock:
  
nb-tarclilus = nb-tarclilus + 1.
  
	/*---------------------------------*/  
	/* recherche de la famille article */
	/*---------------------------------*/
    find artapr 
    where  artapr.codsoc = "01" 
    and    artapr.articl = {tarcli.i}.articl 
    and    lookup(artapr.famart, lcode-fam)  <> 0
    no-lock no-error.

    if not available artapr then next.
    
    /* si article famille <= 010, on ne duplique que les CLI et HTC,
       **************** SURTOUT PAS LES PRO **********
       pour la famille 015, on duplique  tous les codes tarifs
     */
       
    if artapr.famart <> "015" and
       lookup({tarcli.i}.code-tarif, "CLI,HTC") = 0 then next.
    
	/*----------------------------------*/
	/* recherche du tarif en soci�t� 08 */
	/*----------------------------------*/
    wcodsoc = "08".  
    
	/*----------------------------------*/
	/* Recherche de l'article en ste 08 */
	/*----------------------------------*/
    find artapr 
	where artapr.codsoc = "08" 
	and   artapr.articl = {tarcli.i}.articl
    no-lock no-error.
    if not available artapr then next.    
  
    find first b-tarcli
    where 	b-tarcli.codsoc = wcodsoc
    and		b-tarcli.motcle = {tarcli.i}.motcle
    and  	b-tarcli.typaux = {tarcli.i}.typaux
    and   	b-tarcli.codaux = {tarcli.i}.codaux
    and   	b-tarcli.articl = {tarcli.i}.articl
    and   	b-tarcli.code-tarif = {tarcli.i}.code-tarif
    and   	b-tarcli.date-debut = {tarcli.i}.date-debut
    exclusive-lock no-error. 
    
	/*-----------------*/
	/* maj du tarif 08 */
	/*-----------------*/
    if not available b-tarcli then do:
		display "creation en 08"
               {tarcli.i}.code-tarif {tarcli.i}.articl {tarcli.i}.date-debut.
		{dup-tarifc.i "tarcli" } 
		nb-tarcli08cre = nb-tarcli08cre + 1.
    end.
    else do:
		display "maj en 08" 
               {tarcli.i}.code-tarif {tarcli.i}.articl {tarcli.i}.date-debut.
		{dup-tarifm.i "tarcli" } 
		nb-tarcli08maj = nb-tarcli08maj + 1.
    end.
end. /* for each {tarcli.i} VTE */

/*------------------------------*/
/* maj de la date de traitement */
/*------------------------------*/

FIND DATE-TABGES 
where 	date-tabges.codsoc = ""
and 	date-tabges.etabli = ""
and 	date-tabges.typtab = "TAR"
and 	date-tabges.prefix = "DUP-TAR"
and 	date-tabges.codtab = "DERN-TRAIT"
exclusive-lock no-error.
                        
/* La date est egale a date stockee + 1 a chaque lancement */
if heure-debut < 64800                   /* Deb. av. 18 h - Fin meme j  */
then do :
    date-tabges.dattab[1] = today - 1.   /* Pour repartir meme jour     */
    { majmoucb.i date-tabges }
end.

if heure-debut >= 64800 and time < 64800 /* Deb. ap. 18 h - Fin a j + 1 */
then do :
    date-tabges.dattab[1] = today - 1.   /* Pour repartir de j + 1      */
    { majmoucb.i date-tabges }
end.

if heure-debut >= 64800 and time < 86400 /* Deb. ap. 18 h - Fin meme j  */
then do :
    date-tabges.dattab[1] = today.       /* Pour repartir de j + 1      */
    { majmoucb.i date-tabges }
end.

disp "                       Nb CRE     Nb MAJ" no-label skip
     "                       ------     ------" no-label skip
     "TARCLI 08 .... : " nb-tarcli08cre no-label nb-tarcli08maj no-label skip.

pause 2 .

message "Duplication Tarif 01 -> 08 --- fin traitement du : " wdatmaj " a: "  string (time, "hh:mm:ss")  "--- tarifs modifi�s entre le " wdate-deb "et le " wdate-fin.

/* Supprimer les selections */
RUN sel-del.p(sel-applic, sel-filtre, sel-sequence, YES).
leave.