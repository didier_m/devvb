/*==========================================================================*/
/*                           del-bas10.i                                    */
/* Delete Bastat                                                            */
/*--------------------------------------------------------------------------*/
/* Variables et Frames                                       Jdq 26/06/2000 */
/*==========================================================================*/

/* Variables */
def var ind         as int                      no-undo.
def var std-chxbas  as char format "x(80)"      no-undo.

def {1} shared var libelle      as char extent 30              no-undo.
def {1} shared var datmax       as date format "99/99/9999"    no-undo.
def {1} shared var basenameo    as char format "x(16)"         no-undo.
def {1} shared var wcptl        as integer                     no-undo.
def {1} shared var wcpts        as integer                     no-undo.
def {1} shared var wtypbon      as char  format "x(11)"        no-undo.

/* Dessin des Frames */
def {1} shared frame fr-saisie.

Form libelle[02] format "x(16)" datmax              skip
     libelle[03] format "x(16)" basenameo           skip
     with frame fr-saisie row 7 centered no-label overlay { v6frame.i } .
