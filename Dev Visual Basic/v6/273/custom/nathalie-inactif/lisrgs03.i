/* lisrgs03.i liste ajustements stocks /justificatif/fam/sfam/art/datdep
           et liste recapitulative par justificatif
*/
/*   modif DGR 091208 etat tri� par nomenclature 2                            */

def var scodsoc       as char              format "x(2)"             no-undo.
def var slibsoc-soc   as char              format "x(32)"            no-undo.
def var sdtmin        like                 lignes.datcre             no-undo.
def var sdtmax        like                 lignes.datcre             no-undo.
def var sdtpam        like                 stoarr.date-arrete        no-undo.
def var stypnomenc    as char              format "x"                no-undo.
def var simp          as char              format "x"                no-undo.
def var sedifile      as char              format "x(12)"            no-undo.
def var wcptpag       as integer           format ">>>>"             no-undo.

def {1} shared frame fr-saisie .

form
     "soc"        at 01      scodsoc       colon  5 no-label
                             slibsoc-soc   colon 13 no-label
     skip
     "bons du   " at 01      sdtmin        colon 13 no-label
     "au"         at 25      sdtmax        colon 28 no-label
     skip
/* 
     "date pamp  " at 01      sdtpam        colon 13 no-label
     skip 
*/
     "Type Nomenc" at 01     stypnomenc    colon 13 no-label
     "imprimante" at 01      simp          colon 13 no-label
                             sedifile      colon 28 no-label
     "page(s)   " at 45      wcptpag       colon 54 no-label
     with frame fr-saisie with title
     "lisrgs03.p : Ajust stock /justif./nomenclature 1 ou 2/article ver 12/2008"
     centered
     { v6frame.i } .
