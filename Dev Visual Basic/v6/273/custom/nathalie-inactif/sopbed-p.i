/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/genbed-p.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 16134 06-08-07!Evo!cch            !Gestion des articles remises ( Provenance = 'R' )                     !
!V52 16127 03-08-07!Bug!cch            !Suite fiche 16122 -> modif. dans nocom.p                              !
!V52 16122 02-08-07!Bug!cch            !Correction probl�me no de bon retourn� � blanc                        !
!V52 15945 11-06-07!Evo!cch            !Modifications EUREA sur site semaine 200723                           !
!V52 15870 03-05-07!Evo!cch            !Ajout gestion des commandes camions complets ( REC et CCC )           !
!V52 15849 20-04-07!Evo!cch            !Ajouit traitement des provenances U, P et K                           !
!V52 15823 04-04-07!Evo!cch            !Affectation mag. de cde sur E.D. suivant mag. affect�s ds fiche articl!
!V52 15622 23-01-07!New!cch            !Traitement g�n�ration des bons pour EUREA DISTRI                      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/* GENBED-0.I - GENERATION DES BONS EN S.D. + E.D. ( Projet EUREA DISTRI )    */
/*----------------------------------------------------------------------------*/
/* Procedures Communes                                  09/2006 - EUREA - 273 */
/******************************************************************************/

/* Chargement Table temporaire */
PROCEDURE CHARG-TEMPO :

    def var prov-art  as char no-undo .
    def var seuil-qte as dec  no-undo .

    FOR EACH INI-LIGNES where ini-lignes.codsoc = ini-entete.codsoc
                        and   ini-lignes.motcle = ini-entete.motcle
                        and   ini-lignes.typcom = ini-entete.typcom
                        and   ini-lignes.numbon = ini-entete.numbon
                        use-index primaire no-lock :

        FIND B-INI-LIGNES where recid( b-ini-lignes ) = recid( ini-lignes )
                          exclusive-lock no-wait no-error .

        if not available b-ini-lignes
        then do :
            ok-trt-bon = no .
            return .
        end .

        assign codsoc-lect = articl-soc-sd
               code-art    = ini-lignes.articl
               .
        run ctrl-artapr .
        if not available artapr then next .

        if artapr.provenance = ""
        then do :
            mess-err = codsoc-lect + " " + code-art + libelle[ 59 ] .
            run trt-rejet .
            next .
        end .

/* ***
        if artapr.provenance = prov-rem and prov-art = ""
        then do :
            mess-err = string( ini-lignes.chrono ) + libelle[ 94 ] .
            run trt-rejet .
            next .
        end .
***** */
        if not ok-trt-bon then next .
/*
        if artapr.provenance <> prov-rem /* Si remise on garde la provenance article precedente */
           then if artapr.provenance = prov-edx
                then do :         
                    seuil-qte = dec( artapr.caract[ 5 ] ) no-error .
                    if seuil-qte <> 0 and ini-lignes.qte[ niveau-trt ] < seuil-qte
                       then prov-art = prov-edl .
                       else prov-art = prov-edd .                
                end .
                else prov-art = artapr.provenance .
*/
        prov-art = "L".
        
        CREATE LIGNES-TRAV .

        assign lignes-trav.provenance = prov-art
               lignes-trav.tenusto    = artapr.tenusto
               lignes-trav.chrono     = ini-lignes.chrono
               lignes-trav.recid-enr  = recid( ini-lignes )
               .

    END . /* FOR EACH INI-LIGNES */

END PROCEDURE . /* CHARG-TEMPO */

/* Chargement no de bon */
PROCEDURE CHARG-NO-BON :

    REPEAT :

        run nocom.p( rech-typcom, today, 2, output numbon-trt ) .
        numbon-trt = numbon-trt + "00" .

        FIND ENTETE where entete.codsoc = codsoc-trt
                    and   entete.motcle = motcle-trt
                    and   entete.typcom = typcom-trt
                    and   entete.numbon = numbon-trt
                    no-lock no-error .

        if available entete then next .

        FIND FIRST BASTAT where bastat.codsoc = codsoc-trt
                          and   bastat.motcle = motcle-trt
                          and   bastat.typcom = typcom-trt
                          and   bastat.numbon = numbon-trt
                          use-index primaire no-lock no-error .

        if available bastat then next .

        leave .

    END .

END PROCEDURE . /* CHARG-NO-BON */

/* Controle Tiers Gestion */
PROCEDURE CTRL-AUXAPR :

    FIND AUXAPR where auxapr.codsoc = codsoc-lect
                and   auxapr.typaux = typaux-trt
                and   auxapr.codaux = codaux-trt 
                no-lock no-error .

    if not available auxapr
    then do :
        mess-err = codsoc-lect + " " + typaux-trt + " " + codaux-trt + libelle[ 64 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-AUXAPR */

/* Controle Tiers Compta. */
PROCEDURE CTRL-AUXILI :

    regs-fileacc = "AUXILI" + typaux-trt .
    { regs-rec.i }

    FIND AUXILI where auxili.codsoc = regs-soc
                and   auxili.typaux = typaux-trt
                and   auxili.codaux = codaux-trt 
                no-lock no-error .

    if not available auxili
    then do :
        mess-err = regs-soc + " " + typaux-trt + " " + codaux-trt + libelle[ 65 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-AUXILI */

/* Controle Magasin */
PROCEDURE CTRL-MAGASI :

    FIND MAGASI where magasi.codsoc = codsoc-lect
                and   magasi.codtab = code-mag
                no-lock no-error .

    if not available magasi
    then do :
        mess-err = code-mag + libelle[ 62 ] .
        run trt-rejet .
        return .
    end .

    case zone :

        when "1" then if magasi.typaux-cli = "" or magasi.codaux-cli = ""
                      then do :
                          mess-err = code-mag + " " + libelle[ 63 ] .
                          run trt-rejet .
                          return .
                      end .

        when "2" then if magasi.mag-log = ""
                      then do :
                          mess-err = code-mag + " " + libelle[ 88 ] .
                          run trt-rejet .
                          return .
                      end .

    end case .

END PROCEDURE . /* CTRL-MAGASI */

/* Controle Article */
PROCEDURE CTRL-ARTAPR :

    FIND ARTAPR where artapr.codsoc = codsoc-lect
                and   artapr.articl = code-art
                no-lock no-error .

    if not available artapr or code-art = ""
    then do :
        mess-err = codsoc-lect + " " + code-art + libelle[ 58 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-ARTAPR */

/* Controle Article Vente */
PROCEDURE CTRL-ARTBIS-VTE :

    FIND ARTBIS where artbis.motcle = "VTE"
                and   artbis.codsoc = codsoc-lect
                and   artbis.articl = code-art
                and   artbis.typaux = ""    
                and   artbis.codaux = ""
                no-lock no-error .

    if not available artbis
    then do :
        mess-err = codsoc-lect + " " + code-art + libelle[ 66 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-ARTBIS-VTE */

/* Controle Article Achat */
PROCEDURE CTRL-ARTBIS-ACH :

    /* zone = "1" -> inexistant accepte */

    FIND ARTBIS where artbis.motcle = "ACH"
                and   artbis.codsoc = codsoc-soc
                and   artbis.articl = code-art
                and   artbis.typaux = typaux-trt    
                and   artbis.codaux = codaux-trt
                no-lock no-error .

    if not available artbis and zone <> "1"
    then do :
        mess-err = codsoc-soc + " " + code-art + libelle[ 67 ] + typaux-trt + " " + codaux-trt + " ..." .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-ARTBIS-ACH */

/* Controle Devise */
PROCEDURE CTRL-DEVISE :

    if auxili.devise <> dev-gescom
    then do :
        mess-err = auxili.codsoc + " " + auxili.typaux + " " + auxili.codaux + " " + auxili.devise + " " +
                   libelle[ 68 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-DEVISE */

/* Affectation magasin logistique */
PROCEDURE AFFECT-MAG :

    /* Bulk CCI */
    if zone = "3"
       then if code-modliv <> ""
            then do :

                FIND TABGES where tabges.codsoc = ""
                            and   tabges.etabli = ""
                            and   tabges.typtab = "APR"
                            and   tabges.prefix = "MODLIV"
                            and   tabges.codtab = code-modliv
                            no-lock no-error .

                if not available tabges
                then do :
                    mess-err = code-modliv + libelle[ 87 ] .
                    run trt-rejet .
                    return .
                end .

                if tabges.libel2[ 5 ] <> "" then mag-trt = tabges.libel2[ 5 ] .
                                            else zone = "2" .
            end .
            else zone = "2" .

    /* Bulk REI ou CCI sans modliv ou sans mag. bulk associe au modliv */
    if zone = "2"
    then do :

        FIND MAGASS where magass.codsoc = magasi-soc-sd
                    and   magass.codtab = code-mag
                    and   magass.theme  = "SUITE-FICHE"
                    and   magass.chrono = 0
                    no-lock no-error .

        if not available magass
        then do :
            mess-err = code-mag + libelle[ 89 ] .
            run trt-rejet .
            return .
        end .

        if magass.alpha[ 3 ] = ""
        then do :
            mess-err = code-mag + libelle[ 93 ] .
            run trt-rejet .
            return .
        end .

        mag-trt = magass.alpha[ 3 ] .

        return .

    end .

    /* ELF E.D. stockes */
    FIND TABGES where tabges.codsoc = articl-soc-ed
                and   tabges.etabli = ""
                and   tabges.typtab = "ART"
                and   tabges.prefix = "SUIT-FICHE"
                and   tabges.codtab = code-art
                no-lock no-error .

    if not available tabges or tabges.libel2[ ind ] = ""
       then return . /* Mag. Log. principal */

    case num-entries( tabges.libel2[ ind ] ) :

        when 1 then do :

            case tabges.libel2[ ind ] :

                when "LOG" then do : /* Mag. Log. du mag. demandeur */
                    assign codsoc-lect = magasi-soc-sd
                           zone        = "2"
                           .
                    run ctrl-magasi .
                    if not ok-trt-bon then return .
                    mag-trt = magasi.mag-log .
                end .

                otherwise mag-trt = tabges.libel2[ ind ] . /* Mag. Log. fiche article */

            end case .

        end . /* when 1 */

        otherwise do :

            FIND MAGASS where magass.codsoc = magasi-soc-sd
                        and   magass.codtab = code-mag
                        and   magass.theme  = "SUITE-FICHE"
                        and   magass.chrono = 0
                        no-lock no-error .

            if not available magass
            then do :
                mess-err = code-mag + libelle[ 89 ] .
                run trt-rejet .
                return .
            end .

            if magass.alpha[ 1 ] = ""
            then do :
                mess-err = code-mag + libelle[ 90 ] .
                run trt-rejet .
                return .
            end .

            ind-2 = 0 .
            ind-2 = int( magass.alpha[ 1 ] ) no-error .
            if ind-2 = 0 or ind-2 = ?
            then do :
                mess-err = magass.alpha[ 1 ] + libelle[ 91 ] + code-mag + " ..." .
                run trt-rejet .
                return .
            end .

            zone = "" .
            zone = entry( ind-2, tabges.libel2[ ind ] ) no-error .
            if zone = ""
            then do :
                mess-err = string( ind-2 ) + libelle[ 92 ] + code-mag + " / " +
                           articl-soc-ed + " " + code-art + " ..." .
                run trt-rejet .
                return .
            end .  

            case zone :

                when "LOG" then do :
                    assign codsoc-lect = magasi-soc-sd
                           zone        = "2"
                           .
                    run ctrl-magasi .
                    if not ok-trt-bon then return .
                    mag-trt = magasi.mag-log .
                end .

                otherwise mag-trt = zone .

            end case .

        end . /* otherwise do : */

    end case . /* case num-entries( tabges.libel2[ ind ] ) : */

    mag-trt = right-trim( mag-trt ) .
    if l-mag-trt = "" then l-mag-trt = mag-trt .
                      else if lookup( mag-trt, l-mag-trt ) = 0 then l-mag-trt = l-mag-trt + "," + mag-trt .

END PROCEDURE . /* AFFECT-MAG */

/* Traitement des Rejets */
PROCEDURE TRT-REJET :

    if nb-err = 0
    then do :
        output stream s-fic-rej to value( fic-rej ) .
        mess-lib = chr( 27 ) + "&l1O" + "/* " + /* Mise a l'italienne */
                   libelle[ 23 ] + string( today ) + " " + string( time, "hh:mm:ss" ) .
        put stream s-fic-rej unformatted mess-lib skip( 1 ) .
        mess-lib = "/* " + libelle[ 24 ] + fic-rej .
        put stream s-fic-rej unformatted mess-lib( 2 ) skip( 1 ) .
    end .

    if not maj-bon
       then if ind = 53 then mess-lib = libelle[ ind ] .
                        else mess-lib = codsoc-soc + libelle[ ind ] .
       else mess-lib = ini-entete.codsoc + " " + ini-entete.motcle + " " + ini-entete.typcom + " " +
                       ini-entete.numbon  + libelle[ 55 ] .

    put stream s-fic-rej unformatted mess-lib skip .
    put stream s-fic-rej unformatted mess-err skip( 1 ) .

    assign nb-err     = nb-err + 1
           ok-trt-bon = no
           .

END PROCEDURE . /* TRT-REJET */

