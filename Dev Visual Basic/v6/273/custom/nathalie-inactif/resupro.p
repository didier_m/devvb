/* Tableau de bord journalier */
/* -------------------------------------------------------------------------- */
/* Acces parametre lecture societe article ==> OK                             */
/* -------------------------------------------------------------------------- */

{connect.i}
{ bat-ini.i  new }
{ resupro.i new }

def input parameter  fichier  as char format "x(12)".
run bat-lec.p ( fichier ) .

{ bat-cha.i  "wcodsoc"      "wcodsoc"     "a" }
{ bat-cha.i  "wtypaux"      "wtypaux"     "a" }
{ bat-cha.i  "wcodaux"      "wcodaux"     "a" }
{ bat-cha.i  "wlibaux"      "wlibaux"     "a" }
{ bat-cha.i  "wtyppro"      "wtyppro"     "a" }
{ bat-cha.i  "wlibpro"      "wlibpro"     "a" }
{ bat-cha.i  "wcodpro"      "wcodpro"     "a" }
{ bat-cha.i  "wrefpro"      "wrefpro"     "a" }
{ bat-cha.i  "wcodpro2"     "wcodpro2"    "a" }
{ bat-cha.i  "wrefpro2"     "wrefpro2"    "a" }
{ bat-cha.i  "periph"       "periph"      "a" }
{ bat-cha.i  "periph2"      "periph2"     "a" }
{ bat-cha.i  "wdatederarr"  "wdatederarr" "d" }
{ bat-cha.i  "wdatdebv"     "wdatdebv"    "d" }
{ bat-cha.i  "wdatfinv"     "wdatfinv"    "d" }
{ bat-cha.i  "wdatdeba"     "wdatdeba"    "d" }
{ bat-cha.i  "wdatfina"     "wdatfina"    "d" }
{ bat-cha.i  "wdatdebl"     "wdatdebl"    "d" }
{ bat-cha.i  "wdatfinl"     "wdatfinl"    "d" }
{ bat-cha.i  "l-soc"        "l-soc"       "a" }
{ bat-cha.i  "l-mag"        "l-mag"       "a" }
{ bat-cha.i  "l-libmag"     "l-libmag"    "a" }

def var articl-soc    like artapr.codsoc.
def var nbj as deci.
def var debut as int.
def var temps as int.
def var xsoc as char.
def var j    as int.
debut = time.

def var wtypbon as char.
def var wdatem1 as date.
def var wdatea1 as date.
def var wmagpbas as char.
def var wvalpbas as deci.
def var wmagphaut as char.
def var wvalphaut as deci.

def var    wcpt       as integer format '>>>>>9'.
def var    wcpt2      as integer format '>>>>>9'.

def stream file1.
def stream file2.
def var wlibmag as char.
def temp-table t-cumul
 field soc        like artapr.codsoc
 field mag        as char format "x(3)"
 field libmag     as char format "x(30)"
 field famart     like artapr.famart
 field soufam     like artapr.soufam
 field sssfam     like artapr.sssfam
 field articl     like artapr.articl
 field libart     like lignes.libart
 field codfou     like artbis.articl-fou
 field codpro     as char format "x(15)"
 field pastd      like tarcli.valeur
 field papro      like tarcli.valeur
 field qtea       like bastat.qte
 field fac-hta    like bastat.fac-ht
 field qtev       like bastat.qte
 field fac-htv    like bastat.fac-ht
 field fac-ttcv   like bastat.fac-ttc
 field pvstd      like tarcli.valeur
 field prix       like lignes.pu-net
 field pvpro      like tarcli.valeur
 field margeht    like bastat.fac-ht
 field txmargeht  as deci
 index toto3 soc mag articl
 index toto2 soc mag famart soufam sssfam libart ascending
 index toto1 famart soufam sssfam libart  ascending
 .
def temp-table t-articl
 field soc        like artapr.codsoc
 field articl     like artapr.articl
 field libart     like lignes.libart
 field famart     like artapr.famart
 field soufam     like artapr.soufam
 field sssfam     like artapr.sssfam
 field codfou     like artbis.articl-fou
 field codpro     as char format "x(15)"
 field pastd      like tarcli.valeur
 field papro      like tarcli.valeur
 field pvstd      like tarcli.valeur
 field prix       like lignes.pu-net
 field pvpro      like tarcli.valeur
 index titi is unique soc articl ascending
 .
def var wtaux     as deci decimals 2.
def var wpvpro      like tarcli.valeur  .
def var wpapro      like tarcli.valeur  .
def var wpvstd      like tarcli.valeur  .
def var wpastd      like tarcli.valeur  .
def var l-pro       as char format "x(15)".
def var xpro        as char format "x(15)".

output stream file1 to value(periph).
output stream file2 to value(periph2).

message "debut de traitement : " string(debut,"hh:mm:ss") .

def buffer b-lignes for lignes.
def buffer b-entete for entete.

def var wcodfou like artbis.articl-fou.

wtypbon = wtyppro.

l-pro = wcodpro.
if wcodpro2 <> "" then l-pro = l-pro + "," + wcodpro2.

do i = 1 to num-entries(l-pro):
  xpro = entry(i, l-pro).

  if wtyppro = "P" then
    wtypbon  = wtyppro + xpro.

  find entete
  where entete.codsoc = wcodsoc
  and   entete.motcle = "VTP"
  and   entete.typcom = "PRO"
  and   entete.numbon = "000" + xpro + "00"
  and   entete.typbon = wtyppro
  no-lock no-error.
  if available entete then do:
    do j = 1 to num-entries (l-soc) :
      xsoc = entry(j, l-soc).
      if xsoc = "" then next.
      
      codsoc-soc = xsoc.
      regs-app = "NATHALIE" .
      { regs-cha.i }

      { regs-inc.i "articl" }
      articl-soc = regs-soc .

      /* tour mag */
      for each lignes
      where lignes.codsoc = entete.codsoc
      and   lignes.motcle = entete.motcle
      and   lignes.typcom = entete.typcom
      and   lignes.numbon = entete.numbon
      no-lock:
        assign wpastd = ?
               wpapro = ?
               wpvstd = ?
               wpvpro = ?
               wcodfou = "?".
        find artbis
        where artbis.codsoc = xsoc
        and   artbis.motcle = "ACH"
        and   artbis.typaux = wtypaux
        and   artbis.codaux = wcodaux
        and   artbis.articl = lignes.articl
        no-lock no-error.
        if available artbis then wcodfou = artbis.articl-fou.
        find artapr
        where artapr.codsoc = articl-soc
        and   artapr.articl = lignes.articl
        no-lock no-error.

        
        find first tarcli
        where tarcli.codsoc = xsoc
        and   tarcli.motcle = "VTE"
        and   tarcli.typaux = ""
        and   tarcli.codaux = ""
        and   tarcli.articl = lignes.articl
        and   tarcli.code-tarif = "PRO"
        and   tarcli.date-debut >= wdatdebv
        and   tarcli.date-fin <= wdatfinv
        no-lock no-error.
        if available tarcli then
          assign wpvpro = tarcli.valeur.

        find first tarcli
        where tarcli.codsoc = xsoc
        and   tarcli.motcle = "VTE"
        and   tarcli.typaux = ""
        and   tarcli.codaux = ""
        and   tarcli.articl = lignes.articl
        and   tarcli.code-tarif = "CLI"
        and   tarcli.date-debut <= wdatdebv
        and   ( tarcli.date-fin = ? or
                tarcli.date-fin >= wdatfinv )
        no-lock no-error.
        if available tarcli then
          assign wpvstd = tarcli.valeur.

        if wcodfou <> "?" then do:

          find first tarcli
          where tarcli.codsoc = xsoc
          and   tarcli.motcle = "ACH"
          and   tarcli.typaux = wtypaux
          and   tarcli.codaux = wcodaux
          and   tarcli.code-tarif = "PRE"
          and   tarcli.articl = lignes.articl
          and   tarcli.date-debut >= wdatdeba
          and   tarcli.date-fin <= wdatfina
          no-lock no-error.
          if available tarcli then
            assign wpapro = tarcli.valeur.

          if not available tarcli then do:
        
            find first tarcli use-index primaire
            where tarcli.codsoc = xsoc
            and   tarcli.motcle = "ACH"
            and   tarcli.typaux = wtypaux
            and   tarcli.codaux = wcodaux
            and   tarcli.code-tarif = "PRE"
            and   tarcli.articl = lignes.articl
            and   tarcli.date-debut <= wdatdeba
            and   tarcli.date-fin <= wdatfina
            no-lock no-error.
            if available tarcli then

              assign wpapro = tarcli.valeur.
          end.
          find first tarcli use-index primaire
          where tarcli.codsoc = xsoc
          and   tarcli.motcle = "ACH"
          and   tarcli.typaux = wtypaux
          and   tarcli.codaux = wcodaux
          and   tarcli.code-tarif = "STE"
          and   tarcli.articl = lignes.articl
          and   tarcli.date-debut <= wdatdeba
          and   ( tarcli.date-fin = ? or
                  tarcli.date-fin >= wdatfina )
          no-lock no-error.
          if available tarcli then
            assign wpastd = tarcli.valeur.
        end.

        find t-articl
        where t-articl.soc = lignes.codsoc
        and   t-articl.articl = lignes.articl
        exclusive-lock no-error.
        if not available t-articl then do:
          create t-articl.
          assign t-articl.soc    = lignes.codsoc
                 t-articl.articl = artapr.articl
                 t-articl.libart = lignes.libart
                 t-articl.famart = artapr.famart
                 t-articl.soufam = artapr.soufam
                 t-articl.sssfam = artapr.sssfam
                 t-articl.codfou = wcodfou
                 t-articl.codpro = wtypbon
                 t-articl.pastd  = wpastd
                 t-articl.papro  = wpapro
                 t-articl.pvstd  = wpvstd
                 t-articl.pvpro  = wpvpro
                 t-articl.prix   = lignes.pu-net.
        end.
        else do:
          if lookup (wtypbon, t-articl.codpro) = 0 then
            t-articl.codpro = t-articl.codpro + "," + wtypbon.
        end.
      end.
    end.
  end.
end.


for each t-articl
no-lock:
do i = 1 to num-entries(l-soc):
  xsoc = entry(i,l-soc).

  if xsoc = "" then next.

  /* recuperation des ventes */
  lect-mag:
  do k = 1 to num-entries (l-mag):
    wmag = entry(k,l-mag).
    if wmag = "" then next.
    wlibmag = entry(k,l-libmag).

    find magasi
    where magasi.codsoc = ""
    and   magasi.codtab = wmag
    and   magasi.societe = xsoc
    no-lock no-error.
    if not available magasi then next lect-mag.

    find t-cumul use-index toto3
    where t-cumul.soc = xsoc
    and   t-cumul.mag = wmag
    and   t-cumul.articl = t-articl.articl
    no-error.

    if not available t-cumul then do:
      create t-cumul.
      assign t-cumul.soc = xsoc
             t-cumul.mag = wmag
             t-cumul.libmag = wlibmag
             t-cumul.famart = t-articl.famart
             t-cumul.soufam = t-articl.soufam
             t-cumul.sssfam = t-articl.sssfam
             t-cumul.articl = t-articl.articl
             t-cumul.libart = t-articl.libart
             t-cumul.codfou = t-articl.codfou
             t-cumul.prix   = t-articl.prix
             t-cumul.pastd  = t-articl.pastd
             t-cumul.papro  = t-articl.papro
             t-cumul.pvstd  = t-articl.pvstd
             t-cumul.pvpro  = t-articl.pvpro
             t-cumul.codpro = t-articl.codpro.
    end.
  

    if wdatederarr < wdatdebl then do:
      if t-cumul.codfou <> "?" then do:
        for each b-entete
        where b-entete.codsoc = xsoc
        and   b-entete.motcle = "ACH"
        and   b-entete.typcom = "RCF"
        and   lookup(b-entete.typbon, t-articl.codpro) <> 0
        and   b-entete.nivcom >= "6"
        and   b-entete.datdep >= wdatdebl
        and   b-entete.datdep <= wdatfinl
        and   b-entete.typaux = wtypaux
        and   b-entete.codaux = wcodaux 
              
        and   b-entete.magasin = wmag
        no-lock ,
          each b-lignes
        where b-lignes.codsoc = b-entete.codsoc
        and   b-lignes.articl = t-articl.articl
        and   b-lignes.motcle = b-entete.motcle
        and   b-lignes.typcom = b-entete.typcom
        and   b-lignes.numbon = b-entete.numbon
        no-lock:
          assign t-cumul.qtea = t-cumul.qtea + b-lignes.qte[2]
                 t-cumul.fac-hta = t-cumul.fac-hta + b-lignes.fac-ht
                 .
        end.
      end.
      else do:
        for each b-lignes use-index article
        where b-lignes.codsoc = xsoc
        and   b-lignes.articl = t-articl.articl
        and   b-lignes.motcle = "ACH"
        and   b-lignes.typcom = "RCF"
        and   b-lignes.datdep >= wdatdebl
        and   b-lignes.datdep <= wdatfinl
        and   b-lignes.nivcom >= "6"
        and   b-lignes.magasin = wmag
        no-lock,
           each b-entete
        where b-entete.codsoc = xsoc
        and   b-entete.motcle = "ACH"
        and   b-entete.typcom = "RCF"
        and   lookup(b-entete.typbon, t-articl.codpro) <> 0
        and   b-entete.numbon = b-lignes.numbon
        no-lock :
          assign t-cumul.qtea = t-cumul.qtea + b-lignes.qte[2]
                 t-cumul.fac-hta = t-cumul.fac-hta + b-lignes.fac-ht
                 .
        end.

      end.
    end.
    else do:
      if wdatederarr > wdatfinl then do:
        if t-cumul.codfou <> "?" then do:
          for each bastat use-index codaux
          where bastat.codsoc = xsoc
          and   bastat.articl = t-articl.articl
          and   bastat.datdep >= wdatdebl
          and   bastat.datdep <= wdatfinl
          and   bastat.motcle = "ACH"
          and   bastat.typcom = "RCF"
          and   bastat.typaux = wtypaux
          and   bastat.codaux = wcodaux 
          and   lookup(bastat.typbon , t-articl.codpro) <> 0
          and   bastat.magasin = wmag
          no-lock:
            assign t-cumul.qtea = t-cumul.qtea + bastat.qte
                   t-cumul.fac-hta = t-cumul.fac-hta + bastat.fac-ht
                   .
          end.
        end.
        else do:

          for each bastat  use-index artdat
          where bastat.codsoc = xsoc
          and   bastat.articl = t-articl.articl
          and   bastat.datdep >= wdatdebl
          and   bastat.datdep <= wdatfinl
          and   bastat.motcle = "ACH"
          and   bastat.typcom = "RCF"
          /* and   lookup(bastat.typbon, t-articl.codpro) <> 0 */
          and   bastat.magasin = wmag
          no-lock:
            assign t-cumul.qtea = t-cumul.qtea + bastat.qte
                   t-cumul.fac-hta = t-cumul.fac-hta + bastat.fac-ht.
          end.
        end.
      end.
      else do:
        if t-cumul.codfou <> "?" then do:
          for each bastat  use-index codaux
          where bastat.codsoc = xsoc
          and   bastat.articl = t-articl.articl
          and   bastat.datdep >= wdatdebl
          and   bastat.datdep <= wdatederarr
          and   bastat.motcle = "ACH"
          and   bastat.typcom = "RCF"
          and   lookup(bastat.typbon, t-articl.codpro) <> 0
          and   bastat.typaux = wtypaux
          and   bastat.codaux = wcodaux 
          and   bastat.magasin = wmag
          no-lock:
            assign t-cumul.qtea = t-cumul.qtea + bastat.qte
                   t-cumul.fac-hta = t-cumul.fac-hta + bastat.fac-ht.
          end.

          for each b-entete
          where b-entete.codsoc = xsoc
          and   b-entete.motcle = "ACH"
          and   b-entete.typcom = "RCF"
          and   lookup(b-entete.typbon, t-articl.codpro) <> 0
          and   b-entete.nivcom >= "6"
          and   b-entete.datdep >= wdatederarr
          and   b-entete.datdep <= wdatfinl
          and   b-entete.typaux = wtypaux
          and   b-entete.codaux = wcodaux 
          and   b-entete.magasin = wmag
          no-lock ,
            each b-lignes
          where b-lignes.codsoc = b-entete.codsoc
          and   b-lignes.articl = t-articl.articl
          and   b-lignes.motcle = b-entete.motcle
          and   b-lignes.nivcom >= "6"
          and   b-lignes.typcom = b-entete.typcom
          and   b-lignes.numbon = b-entete.numbon
          no-lock:
            assign t-cumul.qtea = t-cumul.qtea + b-lignes.qte [2]
                   t-cumul.fac-hta = t-cumul.fac-hta + b-lignes.fac-ht.
          end.
        end.
        else do:
          for each bastat  use-index artdat
          where bastat.codsoc = xsoc
          and   bastat.articl = t-articl.articl
          and   bastat.datdep >= wdatdebl
          and   bastat.datdep <= wdatederarr
          and   bastat.motcle = "ACH"
          and   bastat.typcom = "RCF"
          /* and   lookup(bastat.typbon, t-articl.codpro) <> 0 */
          and   bastat.magasin = wmag
          no-lock:
            assign t-cumul.qtea = t-cumul.qtea + bastat.qte
                   t-cumul.fac-hta = t-cumul.fac-hta + bastat.fac-ht.
          end.

          for each b-lignes use-index article
          where b-lignes.codsoc = xsoc
          and   b-lignes.articl = t-articl.articl
          and   b-lignes.motcle = "ACH"
          and   b-lignes.typcom = "RCF"
          and   b-lignes.datdep >= wdatederarr
          and   b-lignes.datdep <= wdatfinl
          and   b-lignes.nivcom >= "6"
          and   b-lignes.magasin = wmag
          no-lock,
              each b-entete
          where b-entete.codsoc = xsoc
          and   b-entete.motcle = "ACH"
          and   b-entete.typcom = "RCF"
          and   lookup(b-entete.typbon, t-articl.codpro) <> 0
          and   b-entete.numbon = b-lignes.numbon
          no-lock :

            assign t-cumul.qtea = t-cumul.qtea + b-lignes.qte [2]
                   t-cumul.fac-hta = t-cumul.fac-hta + b-lignes.fac-ht.
          end.
        end.
      end.
    end.

    if wdatederarr < wdatdebv then do:
      for each b-lignes /* use-index article */
      where b-lignes.codsoc = xsoc
      and   b-lignes.articl = t-articl.articl
      and   b-lignes.nivcom >= "3"
      and   b-lignes.datdep >= wdatdebv
      and   b-lignes.datdep <= wdatfinv
      and   b-lignes.motcle = "VTE"
      and lookup(b-lignes.typcom, "CDL,LIV,LAS,LAV,LDP,LIM,LIC,LSC") <> 0
      and   b-lignes.qte[2] <> 0
      no-lock:
        find b-entete
        where b-entete.codsoc = b-lignes.codsoc
        and   b-entete.motcle = b-lignes.motcle
        and   b-entete.typcom = b-lignes.typcom
        and   b-entete.numbon = b-lignes.numbon
        and   b-entete.mag-respon = wmag
        no-lock no-error.
        if not available b-entete then next.

        assign t-cumul.qtev = t-cumul.qtev + b-lignes.qte[2]
               t-cumul.fac-htv = t-cumul.fac-htv + b-lignes.fac-ht
               t-cumul.fac-ttcv = t-cumul.fac-ttcv + b-lignes.fac-ttc.
      end.
    end.
    else do:
      if wdatederarr > wdatfinv then do:
        for each bastat /* use-index artdat */
        where bastat.codsoc = xsoc
        and   bastat.articl = t-articl.articl
        and   bastat.datdep >= wdatdebv
        and   bastat.datdep <= wdatfinv
        and   bastat.motcle = "VTE"
        and lookup(bastat.typcom, "CDL,LIV,LAS,LAV,LDP,LIM,LIC,LSC") <> 0
        and   bastat.mag-respon = wmag
        no-lock:
          assign t-cumul.qtev = t-cumul.qtev + bastat.qte
                 t-cumul.fac-htv = t-cumul.fac-htv + bastat.fac-ht
                 t-cumul.fac-ttcv = t-cumul.fac-ttcv + bastat.fac-ttc.
        end.
      end.
      else do:
        for each bastat /* use-index artdat */
        where bastat.codsoc = xsoc
        and   bastat.articl = t-articl.articl
        and   bastat.datdep >= wdatdebv
        and   bastat.datdep <= wdatederarr
        and   bastat.motcle = "VTE"
        and lookup(bastat.typcom, "CDL,LIV,LAS,LAV,LDP,LIM,LIC,LSC") <> 0
        and   bastat.mag-respon = wmag
        no-lock:
          assign t-cumul.qtev = t-cumul.qtev + bastat.qte
                 t-cumul.fac-htv = t-cumul.fac-htv + bastat.fac-ht
                 t-cumul.fac-ttcv = t-cumul.fac-ttcv + bastat.fac-ttc.
        end.
        for each b-lignes /* use-index article */
        where b-lignes.codsoc = xsoc
        and   b-lignes.articl = t-articl.articl
        and   b-lignes.nivcom >= "3"
        and   b-lignes.datdep > wdatederarr
        and   b-lignes.datdep <= wdatfinv
        and   b-lignes.motcle = "VTE"
        and lookup(b-lignes.typcom, "CDL,LIV,LAS,LAV,LDP,LIM,LIC,LSC") <> 0
        and   b-lignes.qte[2] <> 0
        no-lock:
          find b-entete
          where b-entete.codsoc = b-lignes.codsoc
          and   b-entete.motcle = b-lignes.motcle
          and   b-entete.typcom = b-lignes.typcom
          and   b-entete.numbon = b-lignes.numbon
          and   b-entete.mag-respon = wmag
          no-lock no-error.
          if not available b-entete then next.
          assign t-cumul.qtev = t-cumul.qtev + b-lignes.qte [2]
                 t-cumul.fac-htv = t-cumul.fac-htv + b-lignes.fac-ht
                 t-cumul.fac-ttcv = t-cumul.fac-ttcv + b-lignes.fac-ttc.
        end.
      end.
    end.
  end.
end.
end.

  def var wcumvmag  as deci.
  def var wcumamag  as deci.
  def var wmarmag   as deci.
  def var wtxmarmag as deci.

  export stream file2 delimiter ";"
    "PROMO : " +  wtyppro + " " + l-pro + " - " +
    wrefpro + "  --- Fournisseur : "  + wtypaux + wcodaux + " " + wlibaux +
    "  --- Debut periode COMMANDES : " + string(wdatdeba,"99/99/9999")  +
    "  Fin periode COMMANDES : " + string(wdatfina,"99/99/9999") +
    "  --- Debut periode LIVRAISON : " + string(wdatdebl,"99/99/9999")  +
    "  Fin periode LIVRAISON : " + string(wdatfinl,"99/99/9999") +
    "  --- Debut periode VENTES : " + string(wdatdebv,"99/99/9999") +
    "  Fin periode VENTES : " + string(wdatfinv,"99/99/9999") .

  export stream file2 "".
  export stream file2 "".
  export stream file2  delimiter ";"
    "Societe"
    "Code Magasin"
    "Nom Magasin"
    "Famille"
    "Sous Famille"
    "Sous Sous Fam"
    "Code Article"
    "Designation"
    "Code Art Fourn."
    "Promo"
    "PA std"
    "PA pro"
    "Qte ACHATS"
    "Montant HT ACHATS"
    "Qte VENTES"
    "Montant HT VENTES"
    "Montant TTC VENTES"
    "PV std"
    "Prix Vente promo"
    "PV pro REEL"
    "marge ht"
    "tx marge"
    .

  for each t-cumul use-index toto2 no-lock
  break by (t-cumul.mag):

    t-cumul.margeht = (t-cumul.fac-htv - t-cumul.qtev * t-cumul.papro) .
    wtaux = (t-cumul.margeht / (t-cumul.fac-htv )).
    t-cumul.txmargeht = wtaux .

    export stream file2 delimiter ";" t-cumul.

    assign wcumvmag  = wcumvmag  + t-cumul.fac-htv
           wcumamag  = wcumamag  + t-cumul.fac-hta .

    if last-of(t-cumul.mag) then do:

      export stream file2  delimiter ";"
       t-cumul.soc
       t-cumul.mag
       t-cumul.libmag
       ""
       ""
       ""
       ""
       " TOTAL MAGASIN => "
       ""
       ""
       ""
       ""
       ""
       ""
       wcumamag
       ""
       wcumvmag
       ""
       ""
       ""
       ""
       ""
       ""
       ""
       "".
       assign wcumvmag = 0 wcumamag = 0 wmarmag = 0 wtxmarmag = 0       .

    end.
  end.


  export stream file1 delimiter ";"
    "PROMO : " +  wtyppro + " " + l-pro " - " +
    wrefpro + "  --- Fournisseur : "  + wtypaux + wcodaux + " " + wlibaux +
    "  --- Debut periode COMMANDES : " + string(wdatdeba,"99/99/9999")  +
    "  Fin periode COMMANDES : " + string(wdatfina,"99/99/9999") +
    "  --- Debut periode LIVRAISON : " + string(wdatdebl,"99/99/9999")  +
    "  Fin periode LIVRAISON : " + string(wdatfinl,"99/99/9999") +
    "  --- Debut periode VENTES : " + string(wdatdebv,"99/99/9999") +
    "  Fin periode VENTES : " + string(wdatfinv,"99/99/9999") .

  export stream file1 "".
  export stream file1 "".
  export stream file1  delimiter ";"
    "Societe"
    ""
    "Magasin"
    "Famille"
    "Sous Famille"
    "Sous Sous Fam"
    "Code Article"
    "Designation"
    "Code Art Fourn."
    "Promo"
    "PA std"
    "PA pro"
    "Qte ACHATS"
    "Montant HT ACHATS"
    "Qte VENTES"
    "Montant HT VENTES"
    "Montant TTC VENTES"
    "PV std"
    "Prix Vente promo"
    "PV pro REEL"
    "marge ht"
    "tx marge".

  def var wcumvqte  as deci.
  def var wcumvht   as deci.
  def var wcumvttc  as deci.
  def var wcumaqte  as deci.
  def var wcumaht   as deci.
  def var wmarge    as deci.
  def var wtxmarge  as deci.

  for each t-cumul use-index toto1 no-lock
  break by (t-cumul.articl):

    assign wcumvqte = wcumvqte  + t-cumul.qtev
           wcumvht  = wcumvht   + t-cumul.fac-htv
           wcumvttc = wcumvttc + t-cumul.fac-ttcv
           wcumaqte = wcumaqte  + t-cumul.qtea
           wcumaht  = wcumaht   + t-cumul.fac-hta
             .

    if last-of(t-cumul.articl ) and not (wcumvqte = 0 and wcumaqte = 0) then do:

      wmarge = (wcumvht - wcumvqte * t-cumul.papro) .
      wtaux = (wmarge / wcumvht).

      export stream file1  delimiter ";"
        l-soc
        " "
        "Tous MAGASINS"
        t-cumul.famart
        t-cumul.soufam
        t-cumul.sssfam
        t-cumul.articl
        t-cumul.libart
        t-cumul.codfou
        t-cumul.codpro
        t-cumul.pastd
        t-cumul.papro
        wcumaqte
        wcumaht
        wcumvqte
        wcumvht
        wcumvttc
        t-cumul.pvstd
        t-cumul.prix
        t-cumul.pvpro
        wmarge
        wtaux.


      assign wcumvqte = 0
             wcumvht  = 0
             wcumvttc  = 0
             wcumaqte = 0
             wcumaht  = 0
             wmarge = 0
             wtaux = 0.
    end.
  end.
output stream file1 close.
output stream file2 close.
message "fin de traitement : " string(time,"hh:mm:ss")
" temps de traitement : " string(time - debut ,"hh:mm:ss").
pause(0).
