/* Prog : expaff84.p Extraction affacturage EUROFACTOR                      */
/*                                                                          */
/* 04/07/2013 cco Creation                                                  */
/*                                                                          */

{connect.i}

def stream spool1.
def stream excel1.
def stream felimin.

def var sel-sequence   as char              format "x(12)"            no-undo.
def var sequence       as char              format "x(5)"             no-undo.
def var sedifil1       as char              format "x(12)"            no-undo.
def var sedifil2       as char              format "x(12)"            no-undo.
def var sexcel1        as char              format "x(60)"            no-undo.
def var wok    as char format "x".
def var wsoc    as char initial "84".
def var l-cpt   as char initial "411100,413080".
def var wcpt    as char. 
def var aa      as int.
def var wtypact as char format "X".
def var wcatecr as char format "XXX".
def var wsens   as char format "X".
def var wpiec   as char format "X(14)".
def var wdatp   as date.
def var wdate   as date.
def var wdb     as dec decimals 2 format ">>>>>,>>>,>>9.99-". 
def var wcr     as dec decimals 2 format ">>>>>,>>>,>>9.99-". 
def var wdif    as dec decimals 2 format ">>>>>,>>>,>>9.99-".
def var topeli  as char format "X" initial "N".

def var wmont   as dec .
def var auxili-soc as char.
def var savsoc  as char.
def var wtvaintra as char format "x(25)".
def var nb      as int.
def var nbe     as int.

do with frame a: /* do saisie */

form
  "  " skip
    "Societe ....................... :" at 01 wsoc no-label
      skip
    "Trace des elements elimines ... :" at 01 topeli      no-label
      skip
    "Fichier EUROFACTOR ............ :" at 01 sedifil1     no-label
      skip
    "Fichier R�sultats et divers ... :" at 01 sedifil2     no-label
      skip
    "Fichier Excel WEB ............. :" at 01 sexcel1      no-label
      skip
    "Validation .................... :" at 01 wok          no-label
      skip
      with title
     "expaff84.p : Extraction MDC EUROFACTOR" centered.

display wsoc topeli.

/* determination No sequence operateur pour spoolname debut */
assign codsoc = " "
       etabli = " "
       typtab = "ope"
       prefix = "nume-spool"
       codtab = operat.
       run opeseq.p ("exp",output sequence, output sel-sequence).
/* determination No sequence operateur pour spoolname fin */

sedifil2 = sel-sequence. 

sedifil1 = "AGY70731.txt".

sexcel1 = "lig" + substr(sel-sequence, 4, 5) + ".csv".   
sexcel1 = "/applic/partageweb/xcspool/" + trim(operat) + "/" + sexcel1.

display sedifil1 sedifil2 sexcel1.

update topeli
    validate (topeli = "O" or topeli = "N" , "Valeurs autorisees : O ou N").

display topeli.

update wok
    validate (wok = "O" or wok = "N" , "Valeurs autorisees : O ou N").

display wok.

if wok = "N" then leave.

end. /* repeat saisie*/

output stream spool1  to value(sedifil1).
output stream excel1  to value(sexcel1).
output stream felimin to value(sedifil2).

put stream spool1 unformatted "67185" space (16)  
                  "AGY" space (54) 
                  string( year ( today ), "9999" ) format "xxxx"
                  string( month( today ), "99" ) format "xx"   
                  string( day  ( today ), "99" ) format "xx"
                  space (154) 
                  skip.

export stream excel1 delimiter ";"
               "Num_Cli" "Typ_Act" "Code_Tiers" "Nom"
               "Categ" "Reference" "Libel"
               "Date_Fac" "Date-Ech"
               "Sens" "Montant"
               "Dev" "Siren" "Date" 
               "Mont" "Tva_intra".

nb = nb + 1.

do aa = 1 to num-entries( l-cpt ) :

wcpt = entry( aa, l-cpt ) .

/*----------------------------------------------*/
/* recherche de l'exercice comptable en cours   */
/* Libel1[1] = " " exercice en cours            */
/* Libel1[1] = "1" exercice en cours de cloture */
/* Libel1[1] = "2" exercice termin�             */
/*----------------------------------------------*/
/* xxxxxxx
find first tables where
           tables.codsoc = wsoc and
           tables.etabli = ""          and
           tables.typtab = "PAR"       and
           tables.prefix = "EXERCICE" and
           tables.libel1[1] = " "
           no-lock no-error.
                                                                  
if available tables then do:
xxxxxxxxxxxxxx */

for each mvtcpt where
    mvtcpt.codsoc = wsoc and
    mvtcpt.codgen = wcpt and
    mvtcpt.typaux = "TIE"
/* Index top-lettrage 
CODSOC
CODGEN
TYPAUX
CODAUX
DEVPIE
TOP-LETTRAGE
COD-LETTRAGE
ECHEANCE
*/
    no-lock use-index top-lettrage :

/* Acces parametre lecture societe auxili  ==> OK   */

savsoc = codsoc-soc.
codsoc-soc = wsoc.
 
regs-app = "ELODIE" .
{ regs-cha.i }

regs-app = "NATHALIE" .
{ regs-cha.i }

regs-fileacc = "AUXILI" + "TIE".
{regs-rec.i}
auxili-soc = regs-soc .

codsoc-soc = savsoc.
 
find auxili
where auxili.codsoc = auxili-soc and
      auxili.typaux = mvtcpt.typaux and
      auxili.codaux = mvtcpt.codaux
      no-lock no-error.
if not available auxili then next.

find auxapr
where auxapr.codsoc = wsoc and
      auxapr.typaux = mvtcpt.typaux and
      auxapr.codaux = mvtcpt.codaux
      no-lock no-error.
if not available auxapr then next.

if auxapr.code-edition[1]  <> "A" then next.

if top-lettrage = "2" then do:
                           if topeli = "O" then do:
                              export stream felimin delimiter ";" mvtcpt.
                              end.
                           nbe = nbe + 1.
                           next.
                           end.
    
wtypact = "D".

Case mvtcpt.typie:
     when "FAC" then wcatecr = "FAC".
     when "AVO" then wcatecr = "AVO".
     when "31"  then wcatecr = "EFA".
     otherwise  wcatecr = "AUT".
end.

if DT-DEVCPT < 0 then 
   do:
   message "Debit negatif !           " wpiec " tiers " mvtcpt.codaux. 
   put stream felimin "Debit negatif ! " wpiec " tiers " mvtcpt.codaux 
       " Type de piece " mvtcpt.typie skip.
   export stream felimin delimiter ";" mvtcpt.
   put stream felimin "---" skip.
   pause.
   next.
   end.

if CT-DEVCPT < 0 then 
   do:
   message "Credit negatif !          " wpiec " tiers " mvtcpt.codaux. 
   put stream felimin "Credit negatif ! " wpiec " tiers " mvtcpt.codaux 
       " Type de piece " mvtcpt.typie skip.
   export stream felimin delimiter ";" mvtcpt.
   put stream felimin "---" skip.
   pause.
   next.
   end.


if DT-DEVCPT <> 0 then do: 
                       wsens = "D".
                       wmont = DT-DEVCPT * 100.
                       wdb   = wdb + DT-DEVCPT.
                       end.
                  else do:
                       wsens = "C".                   
                       wmont = CT-DEVCPT * 100.
                       wcr   = wcr + CT-DEVCPT.
                       end.
wtvaintra = "".

if mvtcpt.piece = "" then wpiec = "REG" + string( year ( mvtcpt.datpie ), "9999" )
                                        + string( month( mvtcpt.datpie ), "99" )
                                        + string( day  ( mvtcpt.datpie ), "99" )
                                        + "   ".
                                        
                     else wpiec = string(mvtcpt.piece,"X(14)") .

wdate = mvtcpt.echeance.
if wdate = ? then 
   do:
   message "Facture sans echeance  ! " wpiec " tiers " mvtcpt.codaux. 
   put stream felimin "Facture sans echeance  ! " wpiec " tiers " mvtcpt.codaux skip.
   export stream felimin delimiter ";" mvtcpt.
   put stream felimin "---" skip.
   pause.
   next.
   end.

if mvtcpt.datpie >= mvtcpt.echeance then wdatp = mvtcpt.echeance - 1.
                                    else wdatp = mvtcpt.datpie.

if wcatecr = "FAC" and wsens = "C" then 
   do:
   message "Facture et sens credit ! " wpiec " tiers " mvtcpt.codaux. 
   put stream felimin "Facture et sens credit ! " wpiec " tiers " mvtcpt.codaux skip.
   export stream felimin delimiter ";" mvtcpt.
   put stream felimin "---" skip.
   pause.
   next.
   end.

if wcatecr = "AVO" and wsens = "D" then 
   do:
   message "Avoir et sens debit ! " wpiec " tiers " mvtcpt.codaux. 
   put stream felimin "Avoir et sens debit ! " wpiec " tiers " mvtcpt.codaux skip.
   pause.
   next.
   end.

put stream spool1 unformatted "67185" wtypact 
                 string( mvtcpt.codaux, "x(10)" ) format "x(10)"  
                 space (5)
                 substr(mvtcpt.adres1,1,20) format "x(20)" 
                 wcatecr 
                 wpiec 
                 substr(mvtcpt.libel,1,20) format "x(20)" 
                 string( year ( wdatp ), "9999" ) format "xxxx"   
                 string( month( wdatp ), "99" ) format "xx"   
                 string( day  ( wdatp ), "99" ) format "xx"
                 string( year ( wdate ), "9999" ) format "xxxx"   
                 string( month( wdate ), "99" ) format "xx"   
                 string( day  ( wdate ), "99" ) format "xx"
                 wsens wmont format "999999999999999" mvtcpt.devcpt 
                 auxili.siret format "x(9)"
                 space (12)
                 string( year ( today ), "9999" ) format "xxxx"
                 string( month( today ), "99" ) format "xx"   
                 string( day  ( today ), "99" ) format "xx"
                 space (10) space (10) space (3)
                 wmont format "999999999999999"
                 wtvaintra format "x(25)"
                 space (35)
                 skip
                 .
nb = nb + 1.

export stream excel1 delimiter ";"
               "67185" 
               wtypact mvtcpt.codaux  
               substr(mvtcpt.adres1,1,20)  
               wcatecr 
               wpiec 
               substr(mvtcpt.libel,1,20) 
               wdatp
               wdate 
               wsens wmont 
               mvtcpt.devcpt 
               auxili.siret 
               today
               wmont
               wtvaintra.

end.

end.

message "Nombre Enregistrements Ecrits " nb " Elimin�s " nbe . pause.

put stream felimin " Compteurs lecture" skip.

put stream felimin 
 "- Nombre Enregistrements Ecrits   " nb  skip
 "- Nombre Enregistrements Elimines " nbe skip
 " " skip.

wdif = wdb - wcr.

put stream felimin "Tableau total DEBIT / CREDIT" skip
 "- Total DEBIT  : " wdb  skip
 "- Total CREDIT : " wcr  skip
 "- Difference   : " wdif skip.




