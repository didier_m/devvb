  find artbis
  where artbis.codsoc = artapr.codsoc
  and   artbis.motcle = "VTE"
  and   artbis.articl = artapr.articl
  and   artbis.typaux = ""
  and   artbis.codaux = ""
  no-lock no-error.
  if not available artbis then message "pas dans artbis".

  find tabges 
  where tabges.codsoc = artapr.codsoc
  and   tabges.etabli = " " 
  and   tabges.typtab = "ART" 
  and   tabges.prefix = "suit-fiche"
  and   tabges.codtab = artapr.articl
  no-lock no-error.
  if available tabges then
    assign woldart [1] = tabges.libel2[1]
           woldart [2] = tabges.libel2[8]
           wdeee       = tabges.nombre[15]
           wstodang    = tabges.libel2[11]
           wmaglog [1] = tabges.libel2[14]
           wmaglog [2] = tabges.libel2[15]
           wamm        = trim(tabges.libel2[18])
           wcoefrpd    = tabges.nombre[16] 
           wcoefstat   = tabges.nombre[9] 
           weaj     = (if tabges.libel2[16] = "0" then "N"      
            else "O").
  else
    assign woldart [1] = ""
           woldart [2] = ""
           wdeee       = 0
           wmaglog [1] = ""
           wmaglog [2] = ""
           wamm = ""
           wcoefrpd = ?
           wcoefstat = ?
           weaj = "?"
           wstodang = "".


  assign wcodtax = ""
          wlibtax = ""
          wdebtax = ?  
          wvaltax = ?.

  do itax = 1 to num-entries (l-codtax):

    wtax = entry(itax,l-codtax) .

    wchrono = 0.    
    if wtax = "RPD" then wchrono = 3.

    assign wrpd3 = ? wrpd5 = ? wdaterpd3 = ? wdaterpd5 = ?.
    find first tarcli 
    where tarcli.codsoc = artapr.codsoc
    and   tarcli.motcle = "VTE"
    and   tarcli.typaux = "TAX"
    and   tarcli.codaux = ""
    and   tarcli.articl = artapr.articl
    and   tarcli.date-debut <= today
    and   tarcli.code-tarif = wtax
    and   tarcli.chrono = wchrono
    no-lock no-error.
    if available tarcli then do:
      if wtax = "RPD" then do:
        assign wrpd3 = tarcli.valeur / 1000
                wdaterpd3 = tarcli.date-debut.
    
        find b-tarcli 
        where b-tarcli.codsoc = artapr.codsoc
        and   b-tarcli.motcle = "VTE"
        and   b-tarcli.typaux = "TAX"
        and   b-tarcli.codaux = ""
        and   b-tarcli.articl = tarcli.articl
        and   b-tarcli.date-debut = tarcli.date-debut
        and   b-tarcli.code-tarif = tarcli.code-tarif
        and   b-tarcli.chrono = 5
        no-lock no-error.
        if available b-tarcli then  
          assign wrpd5 = b-tarcli.valeur / 1000
                 wdaterpd5 = b-tarcli.date-debut.
      end.
      else  
      assign wcodtax [itax] = entry(itax,l-codtax)
             wlibtax [itax] = entry(itax,l-libtax)
             wvaltax [itax] = tarcli.valeur / 1000
             wdebtax [itax] = tarcli.date-debut.
    end.
  end.  

  wclasse = "".

  if artapr.code-toxic <> "" then do:
    find tabges
    where tabges.codsoc = ""
    and   tabges.etabli = ""
    and   tabges.typtab = "art"
    and   tabges.prefix = "toxicite"
    and   tabges.codtab = artapr.code-toxic
    no-lock no-error.
    if available tabges then
      wclasse = tabges.libel3[3].
    else
      wclasse = "Code toxicite inconnu".
  end.

  if artapr.code-blocage = ""  or
  artapr.code-blocage = "O"
  then wblo = "O" .
  else wblo = artapr.code-blocage.

  assign wqten[1] = 0 wcan[1] = 0 
         wqten[2] = 0 wcan[2] = 0 
         .
  if wanneedeb[1] <> 0 then do k = 1 to 2 :
    for each starti
    where starti.codsoc = codsoc-soc
    and   starti.motcle = "vte"       
    and   starti.articl = artapr.articl
    and   starti.annee  >= string(wanneedeb[k])
    and   starti.annee  <= string(wanneefin[k])
    no-lock:
      if wanneedeb[k] = wanneefin[k] then do:
        do i = wmoisdeb to wmoisfin :
          assign wqten[k] = wqten[k] + starti.qte[i]
	         wcan[k]  = wcan[k]  + starti.fac-ht[i]      .
        end.	
      end.
      else do:
        if starti.annee = string(wanneedeb[k]) then do:
	  do i = wmoisdeb to 12:
            assign wqten[k] = wqten[k] + starti.qte[i]
                   wcan[k]  = wcan[k]  + starti.fac-ht[i]      .
          end.
        end.
        if starti.annee = string(wanneefin[k]) then do:
	  do i = 1 to wmoisfin:
            assign wqten[k] = wqten[k] + starti.qte[i]
                   wcan[k]  = wcan[k]  + starti.fac-ht[i]      .
          end.
        end.
      end.
    end.
  end.

  wgen1 = "".
  wgen2 = "".
  wgen3 = "".
  for each codbar 
  where  codbar.codsoc = artapr.codsoc 
  and    codbar.articl = artapr.articl    
  no-lock by datmaj desc:
    if wgen1 = "" 
      then wgen1 = codbar.code-barre.
    else if wgen2 = "" then wgen2 = codbar.code-barre.
                        else if wgen3 = "" then wgen3 = codbar.code-barre. 
  end.


  wcommgrp = "".
  FIND PROGLIB
  where proglib.codsoc = artapr.codsoc
  and   proglib.motcle = ""
  and   proglib.etabli = ""
  and   proglib.edition = ""
  and   proglib.chrono  = 0
  and   proglib.clef    = "ARTI-COM" + artapr.articl
  no-lock  no-error .
  if available proglib then wcommgrp = proglib.libelle[ 1 ].
  
  wcommsoc = "".
  FIND PROGLIB
  where proglib.codsoc = artapr.codsoc
  and   proglib.motcle = ""
  and   proglib.etabli = ""
  and   proglib.edition = ""
  and   proglib.chrono  = 0
  and   proglib.clef    = "ARTI-COS" + artapr.articl
  no-lock  no-error .
  if available proglib then wcommsoc = proglib.libelle[ 1 ].


  export stream sortie delimiter ";"
          artapr.articl
          artapr.codsoc
          artapr.libart1[1]
          artapr.libart
          artapr.famart
          artapr.soufam
          artapr.sssfam
          woldart
          artapr.typart
          artapr.tva
          (if codsoc-soc = "00" then "" 
                                else artapr.provenance)
          wblo          
          artapr.type-condit
          artapr.usto
          (if available artbis then artbis.ufac
                              else "????" )
          (if available artbis then artbis.coef-fac
                              else ? )
          (if available artbis then artbis.surcondit
                              else ?) 
          artapr.edit-tarif
          artapr.bareme
          artapr.code-cumul
          artapr.edi-etiq
          artapr.typologie
          artapr.prx-modif
          artapr.qte-lin
          artapr.pcb
          artapr.volume
          artapr.pds
          artapr.tenusto
          artapr.code-toxic
          wclasse
          artapr.va
          artapr.typaux-ref[2] + artapr.codaux-ref[2]
          artapr.typaux-ref[1] + artapr.codaux-ref[1]
          artapr.articl-lie
          artapr.stat-rayon
          artapr.alpha-1
          artapr.caract[01]
          artapr.libart2[1]
          artapr.articl-usine
          artapr.datcre
          artapr.datmaj
          artapr.caract[03]
          artapr.caract[05]
          artapr.caract[06]
          wmaglog
          wdeee
          artapr.date-tarif[1]
          wrpd3 wdaterpd3
          wrpd5 wdaterpd5
          wamm wcoefrpd weaj
          wcan[1] wcan[2] 
	  wqten[1] wqten[2] 
          artapr.type-mag
          wstodang
          artapr.famart2
          artapr.soufam2
          artapr.sssfam2
          wgen1 wgen2 wgen3
          wcoefstat
          artapr.unite-hectare
          artapr.type-client
          artapr.impvte
          artapr.impach
          wcodtax [1] wlibtax [1] wvaltax [1] wdebtax [1] 
          wcodtax [2] wlibtax [2] wvaltax [2] wdebtax [2]
          wcodtax [3] wlibtax [3] wvaltax [3] wdebtax [3] 
          wcodtax [4] wlibtax [4] wvaltax [4] wdebtax [4]
          wcodtax [5] wlibtax [5] wvaltax [5] wdebtax [5]
          artapr.code-texte wcommgrp wcommsoc.
  
  wcpt = wcpt + 1.
  if wcpt modulo 100 = 0 then display wcpt with frame a.

  