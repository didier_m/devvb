
def {1} shared var wcodsoc     as char no-undo.
def {1} shared var l-soc       as char no-undo.
def {1} shared var l-soccre    as char no-undo.
def {1} shared var wsoc        AS CHAR no-undo.
def {1} shared var i           as int no-undo.

def {1} shared var sel-sequence     as char format "x(12)"        no-undo.
def {1} shared var sequence         as char format "x(05)"        no-undo.
def {1} shared var wnomfic     as CHAR no-undo.
def {1} shared var wsim        as log  format "oui/non" no-undo.
def {1} shared var wlibsim     as char no-undo.
def {1} shared var wvalid      as log  format "oui/non"  no-undo.
def {1} shared var wcodaux     as char no-undo.
def {1} shared var wlibaux     as char no-undo.
def {1} shared var wope        as char no-undo.

