/* Programme : Liste des mouvements TDA apr�s int�gration */

{connect.i}

def stream frecep.
def stream flivra.

def stream edition.
/*
def stream editionl.
*/

def buffer b-tabges for tabges.
output stream frecep to cedrecep.csv.
output stream flivra to cedlivra.csv.
def var wqte as dec decimals 0.
def var went       as int. 

def var wmot as char .
def var wtyp as char .
def var wnum as char .
def var hp as character format "x(16)" initial "~033&l1O~033&k12H~033&k4S".

def var wok        as char format "x"     no-undo.
def var re-edition as char format "x"     no-undo.
def var wdate      as date format "99/99/9999"                        no-undo.
def var wdatef     as date format "99/99/9999"                        no-undo.
def var warta      as char format "x(7)"                              no-undo.
def var wartt      as char format "x(6)"                              no-undo.

def var sel-sequence   as char              format "x(12)"            no-undo.
def var sequence       as char              format "x(5)"             no-undo.
def var sedifile       as char              format "x(12)"            no-undo.

wdate  = today .
wdatef = today .
re-edition = "N".

do with frame a: /* do saisie */
  form
  "  " skip
    "Edition (N) ou RE-Edition (O) .... : "  at 01 re-edition   no-label
      skip
    "Date de creation debut ........... : "  at 01 wdate        no-label
      skip
    "Date de creation fin ............. : "  at 01 wdatef       no-label
      skip
    "Validation ....................... : "  at 01 wok          no-label
      skip
    "Spool ............................ : " at 01 sedifile      no-label
      skip
      with title
     "lismvttda.p : Liste des Mouvements TDA" centered.

update re-edition
    help "Confirmation Oui / Non"
    validate (re-edition = "O" or re-edition = "N" , "Valeurs autorisees : O ou N").

display re-edition.

if re-edition = "O" then do:

  update wdate
    help "Entrez la date de selection debut".

  update wdatef
    help "Entrez la date de selection fin".

  display wdate wdatef .

end.

  update wok
    help "Confirmation Oui / Non"
    validate (wok = "O" or wok = "N" , "Valeurs autorisees : O ou N").
  display wok.

if wok = "N" then leave.

/* Mouvements reception bellevile */

went = 0.

/* determination No sequence operateur pour spoolname debut */
assign codsoc = " "
         etabli = " "
         typtab = "ope"
         prefix = "nume-spool"
         codtab = operat.
  run opeseq.p ("tdr",output sequence, output sel-sequence).
/* determination No sequence operateur pour spoolname fin */

sedifile = sel-sequence.

display sedifile.

output stream edition to value(sedifile) page-size 60 paged  .

    form header hp
      "LISTE DES ENTREES TDA (cedrecp.txt) " "(Prog:lismvttda.p)"
      skip
      " Selection date du : " wdate " au :" wdatef "     Spool : " sedifile
      skip
      " Edite Le :" today "par :" operat "page :" 
      string(page-number(edition),">9")
      with frame s page-top width 170.
      view stream edition frame s.

for each tabges where
         tabges.codsoc = "45"
     and tabges.etabli = re-edition
     and tabges.typtab = "EUR"
     and tabges.prefix = "RECEP"
no-lock:

if re-edition  = "O" then do:
if tabges.datcre < wdate or
   tabges.datcre > wdatef then next.
end.

wmot = substr(tabges.codtab, 1, 3).
wtyp = substr(tabges.codtab, 4, 3).
wnum = substr(tabges.codtab, 7, 10).

for each entete where 
    entete.codsoc = tabges.codsoc
and entete.motcle = wmot
and entete.typcom = wtyp
and entete.numbon = wnum  
    no-lock :

for each lignes where 
    lignes.codsoc = entete.codsoc 
and lignes.motcle = entete.motcle
and lignes.typcom = entete.typcom
and lignes.numbon = entete.numbon
no-lock :

if lignes.coef-cde < 0 then wqte = lignes.qte[2] / abs(lignes.coef-cde).
                       else wqte = lignes.qte[2] * lignes.coef-cde.

 /* Code produit interne AREA */
FIND FIRST artbis
     WHERE  artbis.motcle = "ACH"
       AND  artbis.codsoc = lignes.codsoc
       AND  artbis.articl = lignes.articl
       AND  artbis.typaux = lignes.typaux
       AND  artbis.codaux = lignes.codaux
       NO-LOCK NO-ERROR.
IF AVAILABLE artbis THEN warta = artbis.articl-fou.
                    ELSE warta = "".                          

 /* Code produit interne TDA */
FIND FIRST artbis
     WHERE  artbis.motcle = "ACH"
       AND  artbis.codsoc = lignes.codsoc
       AND  artbis.articl = lignes.articl
       AND  artbis.typaux = "TIE"
       AND  artbis.codaux = "    811999"
       NO-LOCK NO-ERROR.
IF AVAILABLE artbis THEN wartt = artbis.articl-fou.
                    ELSE wartt = "".                          

if went = 0 then do:
   went = 1. 
   export stream frecep delimiter ";"
          "Typ" "Numbon" "BL-TDA" "Date-Rcf" "Mag" "Codaux" "Nom"
          "Articl" "Art-Area" "Art-UTA" "Libelle" "Qte-ED" "Un-Ed" 
          "Coef" "Qte-Area" "Un-Area" "Typ-Cde" "Num-Cde" "Datcre".
   end.

export stream frecep delimiter ";"
lignes.typcom lignes.numbon entete.ref-tiers 
entete.datdep lignes.magasin entete.codaux entete.adres[1]
lignes.articl warta wartt lignes.libart wqte lignes.usto 
lignes.coef-cde lignes.qte[2] lignes.ufac 
lignes.typcom-reliq lignes.numbon-reliq
lignes.datcre
.
form lignes.typcom colon 1 with width 170.
display stream edition
lignes.typcom column-label "Typ" lignes.numbon 
substr(entete.ref-tiers,1,10) column-label "Ref-TDA" 
entete.datdep entete.adres[1] format "x(15)" column-label "Fournis." 
lignes.articl format "xxxxxx"
warta format "xxxxxxx"
wartt format "xxxxxx" 
lignes.libart wqte column-label "Qte" lignes.usto   
lignes.typcom-reliq column-label "Typ!Cde" lignes.numbon-reliq
.

end.

end.                                        

if re-edition = "N" and tabges.etabli = "N" then do:
   
   FIND b-tabges where rowid( b-tabges ) = rowid( tabges )
                 exclusive-lock no-error .
                
   if available b-tabges then b-tabges.etabli = "O".

end.

end.

output stream edition close.

/* Mouvements livraison bellevile */

went = 0.

/* xxxxxxxxxxxxxx

/* determination No sequence operateur pour spoolname debut */
assign codsoc = " "
         etabli = " "
         typtab = "ope"
         prefix = "nume-spool"
         codtab = operat.
  run opeseq.p ("tdl",output sequence, output sel-sequence).
  /* determination No sequence operateur pour spoolname fin */

sedifile = sel-sequence.

display sedifile.
xxxxxxxxxxxx */

output stream edition to value(sedifile) page-size 60 paged append.

    form header hp
      "LISTE DES SORTIES TDA (cedliv.txt) " "(Prog:lismvttda.p)"
      skip
      " Selection date du : " wdate " au :" wdatef "     Spool : " sedifile
      skip
      " Edite Le :" today "par :" operat "page :" 
      string(page-number(edition),">9")
      with frame s2 page-top width 170.
      view stream edition frame s2.

for each tabges where
         tabges.codsoc = "45"
     and tabges.etabli = re-edition
     and tabges.typtab = "EUR"
     and tabges.prefix = "LIVRA"
no-lock:

if re-edition  = "O" then do:
if tabges.datcre < wdate or
   tabges.datcre > wdatef then next.
end.

wmot = substr(tabges.codtab, 1, 3).
wtyp = substr(tabges.codtab, 4, 3).
wnum = substr(tabges.codtab, 7, 10).

for each entete where 
    entete.codsoc = tabges.codsoc
and entete.motcle = wmot
and entete.typcom = wtyp
and entete.numbon = wnum  
    no-lock :

for each lignes where 
    lignes.codsoc = entete.codsoc 
and lignes.motcle = entete.motcle
and lignes.typcom = entete.typcom
and lignes.numbon = entete.numbon
no-lock :

if lignes.coef-cde < 0 then wqte = lignes.qte[2] / abs(lignes.coef-cde).
                       else wqte = lignes.qte[2] * lignes.coef-cde.

 /* Code produit interne AREA */
FIND FIRST artbis
     WHERE  artbis.motcle = "ACH"
       AND  artbis.codsoc = lignes.codsoc
       AND  artbis.articl = lignes.articl
       AND  artbis.typaux = "TIE"
       AND  artbis.codaux = "    811000"
       NO-LOCK NO-ERROR.
IF AVAILABLE artbis THEN warta = artbis.articl-fou.
                    ELSE warta = "".                          

 /* Code produit interne TDA */
FIND FIRST artbis
     WHERE  artbis.motcle = "ACH"
       AND  artbis.codsoc = lignes.codsoc
       AND  artbis.articl = lignes.articl
       AND  artbis.typaux = "TIE"
       AND  artbis.codaux = "    811999"
       NO-LOCK NO-ERROR.
IF AVAILABLE artbis THEN wartt = artbis.articl-fou.
                    ELSE wartt = "".                          

if went = 0 then do:
   went = 1. 
   export stream flivra delimiter ";"
          "Typ" "Numbon" "BL-TDA" "Date-Rcf" "Mag" "Codaux" "Nom"
          "Articl" "Art-Area" "Art-UTA" "Libelle" "Qte-ED" "Un-Ed" 
          "Coef" "Qte-Area" "Un-Area" "Typ-Cde" "Num-Cde" "Datcre".
   end.

export stream flivra delimiter ";"
lignes.typcom lignes.numbon entete.ref-tiers 
entete.datdep lignes.magasin entete.codaux entete.adres[1]
lignes.articl lignes.libart wqte lignes.usto 
lignes.coef-cde lignes.qte[2] lignes.ufac 
lignes.typcom-reliq lignes.numbon-reliq
lignes.datcre
.
form lignes.typcom colon 1 with width 170.
display stream edition
lignes.typcom column-label "Typ" lignes.numbon 
substr(entete.ref-tiers,1,10) column-label "Ref-TDA" 
entete.datdep substr(entete.codaux,5,6) format "x(6)" column-label "Tiers/Mag" 
lignes.articl format "xxxxxx" 
warta format "xxxxxxx"
wartt format "xxxxxx"
lignes.libart wqte column-label "Qte" lignes.usto   
lignes.typcom-reliq column-label "Typ!Cde" lignes.numbon-reliq
.

end.

end.                                        

if re-edition = "N" and tabges.etabli = "N" then do:
   
   FIND b-tabges where rowid( b-tabges ) = rowid( tabges )
                 exclusive-lock no-error .
                
   if available b-tabges then b-tabges.etabli = "O".

end.

end.

output stream edition close.


end. /* repeat saisie*/

