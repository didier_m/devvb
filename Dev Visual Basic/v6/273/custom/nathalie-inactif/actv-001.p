/*==========================================================================*/
/*                          A C T V - 0 0 1 . P                             */
/*                   Ewtraction des Fichiers ACTIONS VENTES                 */
/* Traitement                                                               */
/*==========================================================================*/

{ connect.i      }
{ actv-000.i new }
{ aide.i     new }
{ bat-ini.i  new }
{ select.i   new }

def input parameter fic as char format "x(12)" .


/* Chargement du fichier aide */
aide-fichier = "actv-000.aid" . aide-par = "" . run aide-lec.p .
app-fichier = "gescom.sai" .
do i = 1 to 60 :
  aide-m = "libel" + string( i , "999" ) .
  { aide-lib.i }
  libelle[ i ] = aide-lib .
end .

/* Prise des selections */
run bat-lec.p ( fic ) .

{ bat-cha.i "periph"       "periph"             "a" }
{ bat-cha.i "l-soc"        "l-soc"              "a" }
{ bat-cha.i "l-repres"     "l-repres"           "a" }
{ bat-cha.i "date-ext"     "date-ext"           "d" }

/* Societes de Regroupement */
regs-app = "NATHALIE" .
{ regs-cha.i }

{ regs-inc.i "magasi" }
magasi-soc = regs-soc .

hide message no-pause .
message "actv-001.P -- EXTRACTION DES FICHIERS ACTION VENTES : Debut du programme "
   string ( today , "99/99/9999" ) " "
   string ( time  , "hh:mm:ss" ).

/*==========================================================================*/
/*                           Debut du traitement                            */
/*==========================================================================*/
output to value(periph).

/* nombre d'exercices consideres */
/* nbexe = 1. pour tests ... */
nbexe = 3 .

/* chargement des dates limites d'exercice */
{ actv-dat.i }

run actv-his.p . /* historisation des fichiers precedents */
run actv-exe.p . /* fichier campagne */
run actv-cer.p . /* fichier cereale */
run actv-pro.p . /* fichier produits */
/*
run actv-cli.p . /* fichier clients */
run actv-det.p . /* fichier detailcours */
run actv-app.p . /* fichier appros */
*/
run actv-col.p . /* fichier collecte */
/*
run actv-vtp.p . /* fichier ventesproduits */
*/
run actv-fam.p . /* fichiers famille et sousfamille */
run actv-mag.p . /* fichier magasin et magatc */
run actv-atc.p . /* fichier atc */

output close.
hide message no-pause .
message libelle[ 32 ] nb-lus .

hide message no-pause .
message "actv-001.p -- EXTRACTION DES FICHIERS ACTION VENTES : Fin du programme "
   string ( today , "99/99/9999" ) " "
   string ( time  , "hh:mm:ss" ).

