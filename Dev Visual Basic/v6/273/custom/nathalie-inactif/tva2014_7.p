/****************************************************************************/
/*==========================================================================*/
/*                            TVA2014_7.p                                   */
/* creation des exceptions de taux pour g�rer le pb des engrais qui         */
/* passeraient � 20 au lieu de 10                                           */
/*--------------------------------------------------------------------------*/

{ connect.i          }
{ bat-ini.i    "new" }

def input parameter  fichier  as char format "x(12)" .
def var topmaj   as log.

def stream file1.
input stream file1 from /applic/li/vle/tva_exception_taux.csv.

def var warticl as char.
def var wtaux as char.
def var cpt-lus as int.
def var cpt-ecrit as int.

run bat-lec.p ( fichier ) .
{ bat-cha.i  "topmaj"          "topmaj"           "l" }

/* boucle sur le fichier csv*/
repeat :

    import stream file1 delimiter ";" warticl wtaux.

    cpt-lus = cpt-lus + 1.
        
    if topmaj= yes then do :    
        create tabges.
        assign tabges.codsoc = ""
       tabges.etabli = ""
       tabges.typtab = "LOI"
       tabges.prefix = "EXCEPTVA2014"
       tabges.codtab = warticl
       tabges.libel1[1] = wtaux
       tabges.datcre = today
       tabges.heucre = "00:00"
       tabges.opecre = "BVL"
       tabges.datmaj = today
       tabges.heumaj = "00:00"
       tabges.opemaj = "BVL".
       cpt-ecrit = cpt-ecrit + 1.
    end.
end.
        
output stream file1 close.
display "NB enreg LUS : " cpt-lus " NB enreg ecrits : " cpt-ecrit. 
pause.
