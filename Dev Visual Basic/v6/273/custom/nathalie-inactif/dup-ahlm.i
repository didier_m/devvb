/* ------------------------------------------------------------------------- */
/* duplication des creations / modifications articles / fournisseurs         */
/*                                           tarifs /a achats                */
/* de la societe 01 sur la societe 05                                        */
/* ------------------------------------------------------------------------- */
/* dup-ahlm.i : modification                                                 */
/* ------------------------------------------------------------------------- */
buffer-copy {1}
  except {1}.codsoc
         {1}.datmaj
         {1}.heumaj
         {1}.opemaj
to b-{1}
  assign b-{1}.codsoc = "05"
         b-{1}.datmaj = wdatmaj
         b-{1}.heumaj = wheumaj
         b-{1}.opemaj = wopemaj.
