/*==========================================================================*/
/*                           E X T - C R M S O L  . I                       */
/* Extraction des donnees pour VENTES PARTNER                               */
/*--------------------------------------------------------------------------*/
/* Extraction Solde Financier                                               */
/* On recherche le solde financier sur le compte 411100 et s'il n'existe pas*/
/* on recherche sur le compte 453100 si la societe est CBA ou AGRICEL       */
/* DGR 220108 : EUREA COOP                                                  */
/*==========================================================================*/
Solde = 0.
Solde_No_client = trim(codsoc-trait[nsoc]) + "/" + substring(auxapr.codaux,5,6).
/*----------------------------------------------*/
/* recherche de l'exercice comptable en cours   */
/* Libel1[1] = " " exercice en cours            */
/* Libel1[1] = "1" exercice en cours de cloture */
/* Libel1[1] = "2" exercice termin�             */
/*----------------------------------------------*/
find first tables where 
           tables.codsoc = codsoc-trait[nsoc] and
           tables.etabli = ""          and
           tables.typtab = "PAR"       and
           tables.prefix = "EXERCICE" and
           tables.libel1[1] = " "
           no-lock no-error.

if available tables then do:
   find soldes where
        soldes.codsoc = codsoc-trait[nsoc] and
        soldes.codetb = "001" and
        soldes.execpt = tables.codtab and
        soldes.codgen = "411100" and
        soldes.typaux = auxapr.typaux and
        soldes.codaux = auxapr.codaux and
        soldes.devise = "EUR"
        no-lock no-error.
    
    if not available soldes then do:
    
       if codsoc-trait[nsoc] <> "01" 
/*       and codsoc-trait[nsoc] <> "05"  */
          then next.
            
       find soldes where
            soldes.codsoc = codsoc-trait[nsoc] and
            soldes.codetb = "001" and
            soldes.execpt = tables.codtab and
            soldes.codgen = "453100" and
            soldes.typaux = auxapr.typaux and
            soldes.codaux = auxapr.codaux and
            soldes.devise = "EUR"
            no-lock no-error.   
                
       if not available soldes 
          then next.
    
    end.  /* soldes 411100 not available  */

    do i = 1 to 17:
       Solde = Solde + soldes.dt-cpt[i] - soldes.ct-cpt[i].
    end.
        
    export stream crm-sol delimiter ";"
           Solde_No_client Solde.

end. /* exercice en cours available */
