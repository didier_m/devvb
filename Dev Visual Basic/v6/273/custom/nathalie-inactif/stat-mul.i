def {1} shared var l-soc       as char initial "01,05,08"                  no-undo.
def {1} shared var wsoc        as char                                     no-undo.
def {1} shared var wdatdeb     as date format "99/99/9999"                 no-undo.
def {1} shared var wdatfin     as date format "99/99/9999"                 no-undo.
def {1} shared var wdatsto     as date format "99/99/9999"                 no-undo.
def {1} shared var wvalid      as char format "x"                          no-undo.
def {1} shared var wnomfic1    as char format "x(30)" extent 3             no-undo.
def {1} shared var wnomfic2    as char format "x(30)" extent 3             no-undo.
