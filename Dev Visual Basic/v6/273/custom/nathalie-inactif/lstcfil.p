{ connect.i      }
{ bat-ini.i  new }


def var wdatmin as date format "99/99/9999" .
def var wdatmax as date format "99/99/9999" .

def var i as int.
def var j as int.

def var wsoc as char.
def var wmag as char format "x(50)".
def var l-mag as char .
def var xmag as char format "xxx" .
def var toprechach as log format "Oui/non"      .
def var valid as log format "Oui/non"      .

toprechach = No.

/* variables pour edition */
def var sequence     as char format "x(05)"           no-undo.
def var sel-sequence as char format "x(12)"           no-undo.
def var wnomfic      as char format "x(12)"           no-undo.

do with frame toto:
  repeat with frame toto :
    form 
     "Societe ...... : " wsoc no-label skip 
     "Magasins ..... : " wmag no-label format "x(50)" skip
     "Achats ....... : " toprechach no-label skip
     "..... date min : " wdatmin no-label skip
     "..... date max : " wdatmax no-label skip
     "Fichier ...... : " wnomfic no-label skip
     "Validation ... : " valid   no-label
   
     with Title "Liste XL des Cartes" centered.
  
    update wsoc 
      help "Blanc pour Carte GV, 08 pour Carte des Saisons"  
      validate ( wsoc = ""  or wsoc = "08" , " Valeurs autorises: blanc ou 08").

 
    update wmag format "x(50)"  help "Liste mag separes par , blanc pour tous".
    l-mag = "".
    if trim (wmag) <> "" then do:
      do i = 1 to num-entries(wmag): 
        if wmag = "" then next.
        xmag = entry(i,wmag).
        {cadra-d.i wmag 3 0}.
        if l-mag = "" then l-mag = wmag.
        else l-mag = l-mag + "," + wmag.
      end.
    end.

    update toprechach help "Recherche du CA total ou pas ".
    if toprechach then do:
      update wdatmin
        help "Saisissez la date de debut de periode JJ/MM/SSAA "
        validate(wdatmin <> " ","blanc refuse").

      if wdatmin = ? then do:
        message "Date Min erronee".
        bell. bell .
        readkey pause 2.
        undo , retry.
      end.
                    
      update wdatmax
        help "Saisissez la date de fin de periode JJ/MM/SSAA "
        validate(wdatmax <> " ","blanc refuse").
                    
      if wdatmax = ? or wdatmax < wdatmin then do:
        message "Date de fin erronee".
        bell. bell .
        readkey pause 2.
        undo , retry.
      end.
    end.
  
    /* determination du nom du fichier spool */
    assign codsoc = " " etabli = " " typtab = "ope" prefix = "nume-spool"
           codtab = operat.
    run opeseq.p ("cfi" , output sequence, output wnomfic).
   
    display wnomfic.

    valid= yes.
      update valid
      help "Validation o = oui n = non".
    if not valid then leave.


   batch-tenu = yes. /* Traitement en Batch */
   { bat-zon.i  "wsoc"         "wsoc"          "c" "''" "''" }
   { bat-zon.i  "l-mag"        "l-mag"         "c" "''" "''" }
   { bat-zon.i  "toprechach"   "toprechach"    "l" "''" "''" }                  
   { bat-zon.i  "wdatmin"      "wdatmin"          "d" "''" "''" }
   { bat-zon.i  "wdatmax"      "wdatmax"          "d" "''" "''" }
   { bat-zon.i  "wnomfic"      "wnomfic"          "c" "''" "''" }


   { bat-maj.i  "lstcfi1.p" }
   
    leave.      
  end.   /*  repeat */
end.
