/*----------------------------------------------------------------------------*/
/*                           PROJET ACTION VENTES                             */
/*                                                                            */
/*  include : actv-000.i : contient les variables communes � tous les prog de */
/* =========               l'application                                      */
/*                                                                            */
/* DGR 07-03-01                                                               */
/*----------------------------------------------------------------------------*/
/* DGR 18-12-03 : rajout tot dans noms de fichiers                            */
/*----------------------------------------------------------------------------*/

def {1} shared var libelle     as char extent 60                    no-undo .
def {1} shared var t-pf1       as char initial "pf1,"               no-undo .
def {1} shared var code-tarif  as char format "xxx"                 no-undo .
def {1} shared var date-mini   as date format "99/99/9999"          no-undo .
def {1} shared var date-maxi   as date format "99/99/9999"          no-undo .
def {1} shared var magasins    as char format "x(51)"               no-undo .
def {1} shared var mag-exclus  as char                              no-undo .
def {1} shared var periph      as char format "x(12)"               no-undo .
def {1} shared var sav-periph  as char                              no-undo .
def {1} shared var magasi-soc  as char                              no-undo .


/* variable de lecture des societes */
DEF {1} shared VAR soc             AS CHAR                  no-undo.
DEF {1} shared VAR wauxilicodsoc   AS CHAR                  no-undo.
DEF {1} shared VAR l-soc           AS CHAR INITIAL "01"     no-undo.
DEF {1} shared VAR l-repres        AS CHAR INITIAL ""       no-undo.

DEF {1} shared VAR ind   AS INTEGER .

/* variable pour les calculs des dates d'exercice */
DEF {1} shared VAR date-ext AS date NO-UNDO .
DEF {1} shared VAR date-deb AS INT NO-UNDO EXTENT 5 .
DEF {1} shared VAR date-fin AS INT NO-UNDO EXTENT 5 .
DEF {1} shared VAR nbexe    AS INT NO-UNDO.
DEF {1} shared VAR i        AS INT NO-UNDO.
DEF {1} shared VAR j        AS INT NO-UNDO.
DEF {1} shared VAR k        AS INT NO-UNDO.
DEF {1} shared VAR l        AS INT NO-UNDO.
DEF {1} shared VAR nb-lus   AS INT NO-UNDO.

/* variables pour l'edition dans les fichiers */
def {1} shared var wrep     as char initial "/applic/li/depart/"        no-undo.
def {1} shared var wreph    as char initial "/applic/li/depart/traite/" no-undo.
def {1} shared var l-nomfic as char initial "produitstot,clientstot,detailscourstot,approtot,collectetot,ventesproduitstot,famille,sousfamille,atc,magasin,magatc,campagne,appro12-32-Qtot" no-undo.
def {1} shared var wnomfic  as char no-undo.
def {1} shared var wnomfich as char no-undo.
def {1} shared var wfic     as char no-undo.
def {1} shared var wfic2    as char no-undo.
def {1} shared var commande as char no-undo.
def {1} shared var campmax  as int  no-undo.

DEF {1} shared VAR com  AS CHAR NO-UNDO.
DEF {1} shared VAR chaine AS CHAR NO-UNDO.
DEF {1} shared VAR xchaine AS CHAR NO-UNDO.
DEF {1} shared VAR fichier AS CHAR NO-UNDO.

def {1} shared frame fr-saisie .

form libelle[ 01 ] format "x(15)" l-repres format "x(60)"      skip
     libelle[ 02 ] format "x(15)" date-ext format "99/99/9999" skip
     libelle[ 03 ] format "x(15)" periph   format "x(15)"
     with frame fr-saisie
     row 3 centered overlay no-label {v6frame.i} .
