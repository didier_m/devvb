/* lisrgs02.i liste des ajustements de stocks par code justificatif
                    depot,famille sous-famille, article, numero de bon
              liste recapitulative des totaux par justif/depot
*/
/*   modif DGR 091208 etat tri� par nomenclature 2                            */

def var scodsoc       as char              format "x(2)"             no-undo.
def var slibsoc-soc   as char              format "x(32)"            no-undo.
def var sdtmin        like                 lignes.datdep             no-undo.
def var sdtmax        like                 lignes.datdep             no-undo.
def var sdtpam        like                 stoarr.date-arrete        no-undo.
def var smagasin      like                 lignes.magasin            no-undo.
def var stype         like                 lignes.typbon             no-undo.
def var simp          as char              format "x"                no-undo.
def var stypnomenc    as char              format "x"                no-undo.
def var sedifile      as char              format "x(12)"            no-undo.
def var wcptpag       as integer           format ">>>>"             no-undo.

def {1} shared frame fr-saisie .

form
     "soc"        at 01      scodsoc       colon  5 no-label
                             slibsoc-soc   colon 13 no-label
     skip
     "bons du   " at 01      sdtmin        colon 13 no-label
     "au"         at 25      sdtmax        colon 28 no-label
     skip
/*
     "date pamp " at 01      sdtpam        colon 13 no-label
     skip
*/
     "code depot" at 01      smagasin      colon 13 no-label
     skip
     "type ajust" at 01      stype         colon 13 no-label
     "Type Nomenc" at 01      stypnomenc   colon 13 no-label
     "imprimante" at 01      simp          colon 13 no-label
                             sedifile      colon 28 no-label
     "page(s)   " at 45      wcptpag       colon 54 no-label
     with frame fr-saisie with title
     "lisrgs02.p : Ajust stock /justificatif/depot/Nomenc 1 ou 2/article ver 12/08"
     centered
     { v6frame.i } .
