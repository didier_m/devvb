
def stream maquette .

def var wclasse       as char format "x(15)"        no-undo.
def var wblo          as char format "x"            no-undo.
def var articl-soc    as char                       no-undo.
def var l-soc         as char                       no-undo.
def var isoc          as int                        no-undo.
def var wanneedeb     as int format "9999" extent 2 no-undo.
def var wanneefin     as int format "9999" extent 2 no-undo.
def var wmoisdeb      as int format "99"   extent 2 no-undo.
def var wmoisfin      as int format "99"   extent 2 no-undo.
def var wqten         as deci extent 2              no-undo.
def var wcan          as deci extent 2              no-undo.
def var woldart       as char extent 2              no-undo.
def var wstodang      as char                       no-undo.
def var i             as int                        no-undo.
def var k             as int                        no-undo.
def var wzone1        as char                       no-undo.
def var wzone2        as char                       no-undo.
def var wzone3        as char                       no-undo.
def var wzone4        as char                       no-undo.
def var wartfermes    as char                       no-undo.
def var wsegmen       as char                       no-undo.
def var wventes       as char                       no-undo.
def var x-nomenc      as char                       no-undo.
def var wmaglog       as char extent 2              no-undo.
def var wdeee         as deci                       no-undo.
def var wecom         as deci                       no-undo.
def var wcoefrpd      as deci                       no-undo.
def var wrpd5         as deci                       no-undo.
def var wdaterpd5     as date                       no-undo.
def var wrpd3         as deci                       no-undo.
def var wdaterpd3     as date                       no-undo.
def var wamm          as char                       no-undo.
def var weaj          as char                       no-undo.
def var wgen1         as char                       no-undo.
def var wgen2         as char                       no-undo.
def var wgen3         as char                       no-undo.
def var wcoefstat     as deci                       no-undo.

def var l-codtax      as char                       no-undo. 
def var l-libtax      as char                       no-undo. 
def var wchrono       as int                        no-undo.
def buffer b-tarcli   for tarcli. 
def var itax          as int                        no-undo. 

def var wtax          as char                       no-undo.
def var wcodtax       as char  extent 5             no-undo.
def var wlibtax       as char  extent 5             no-undo.
def var wdebtax       as date  extent 5             no-undo.
def var wvaltax       as dec   extent 5             no-undo.


def var wcodtar       as char  extent 2 			no-undo.
def var wvaltar       as dec   extent 2 			no-undo.
def var wdattar       as date  extent 2 			no-undo.

def var wtoler        as dec                        no-undo. 

def var wcommgrp      as char                       no-undo.
def var wcommsoc      as char                       no-undo.
def var wrej          as char                       no-undo.

def var wsto	      as dec  extent 2 				no-undo.
def var l-mag         as char                       no-undo.
def var topmvt        as log                        no-undo.

def var wsoc          as char                       no-undo.

message "Debut extraction articles pour stockit" view-as alert-box.

message "TRAITEMENT EN COURS .... MERCI DE PATIENTER ..........".



wsoc  = "45"  .  

/*
assign wzone1 = "CA N " + string(wanneedeb[1],"9999") + string(wmoisdeb[1],"99") +
                " a " + string(wanneefin[1],"9999") + string(wmoisfin[1],"99") 
       wzone2 = "CA N-1 " + string(wanneedeb[2],"9999") + string(wmoisdeb[2],"99") +
                " a " + string(wanneefin[2],"9999") + string(wmoisfin[2],"99") 
       wzone3 = "Qte N " + string(wanneedeb[1],"9999") + string(wmoisdeb[1],"99") +
                " a " + string(wanneefin[1],"9999") + string(wmoisfin[1],"99") 
	   wzone4 = "Qte N-1 " + string(wanneedeb[2],"9999") + string(wmoisdeb[2],"99") +
                " a " + string(wanneefin[2],"9999") + string(wmoisfin[2],"99") .

*/

assign wzone1 = "Z1"
       wzone2 = "Z2"
       wzone3 = "Z3"
	   wzone4 = "Z4".

OUTPUT STREAM maquette TO art-stockit.csv.

export stream maquette delimiter ";"
    "Code"            "Societe"          "Libelle"         "Lib.reduit No 1"      "Famille"     
    "Sous Famille"    "Sous sous Fam."   "Ancien code 01"  "Ancien code (08)"     "Type d'Article"
    "Code TVA"        "Provenance"       "Code Blocage"    "Type Condit. "        "Unite de Stock"
    "Un. Fact."       "Coef. Cde/Fact."  "Condit de Vente" "Edition Tarif"        "Baremes"
    "Code Cumul"      "Edit Etiquette"   "Typologie"       "Prix Modifiable"      "Nb Unit/Palette"
    "Nb Unite/Colis"  "Volume"           "Poids"           "Tenu en Stock"        "Code Toxicite"
    "Classe danger"   "Plaque Toxicite"  "Tiers Fabricant" "Tiers Pr Livr. "      "Article Lie"
    "Code Stat Rayon" "Taxe d'equariss"  "Code Message"    "Libelle 2"            "Code Usine "
    "Date Creation"   "Date Modif"       "Segment"         "Seuil"                "Code Reseau"
    "Mag Log REI"     "Mag Log CCI"      "Eco-taxe"        "M A D"                "RPD Loi/Bre"
    "Date Applic"     "RPD Rho/Med/Cor"  "Date Applic"     "AMM"                  "coef RPD"
    "EAJ"             wzone1             wzone2            wzone3                 wzone4
    "type depot lie"  "Stock Dangereux"  "Fam 2"           "SFam 2"               "SSFam 2"
    "Gencod1"         "Gencod2"          "Gencod3"         "Coef-Stat"            "Unites-Hectare"
    "Type-client"     "Code imput VTE"   "Code imput ACH"  
    "Code taxe 1"     "lib taxe 1"        "val taxe 1"     "appli taxe 1" 
    "Code taxe 2"     "lib taxe 2"        "val taxe 2"     "appli taxe 2" 
    "Code taxe 3"     "lib taxe 3"        "val taxe 3"     "appli taxe 3" 
    "Code taxe 4"     "lib taxe 4"        "val taxe 4"     "appli taxe 4" 
    "Code taxe 5"     "lib taxe 5"        "val taxe 5"     "appli taxe 5" 
    "top comment "    "Comment GRP"       "Comment SOC"
    "Taxe Eco-M"
    "code tarif 1"    "Date appli 1"      "Valeur 1"
    "code tarif 2"    "Date appli 2"      "Valeur 2"
    "Tenu en lot"     "unite regroup"     "Gestion DLUO"	
    "Nb J DLUO"       "Pick Interdit"     "%age toler.liv"	
    "stock 970"       "stock 985"
    "type Condit Vte" "Emplacement" .


    l-codtax = "CVO,DIA,RPD".
    l-libtax = "CVO,DIA,RPD".

    rech-art:
    for each artapr 
    where artapr.codsoc = wsoc
    no-lock:

	    /* recup des stocks des dépots 970 et 985 */
		assign l-mag   = "970,985".
		do i = 1 to 2:
		
			assign  wsto [i] = 0 .
					
			find stocks
			where stocks.codsoc    = artapr.codsoc
			and   stocks.articl    = artapr.articl
			and   stocks.magasin   = entry ( i , l-mag)
			and   stocks.evenement = ""
			and   stocks.exercice  = ""
			no-lock no-error.
			if available stocks then 
				assign wsto [ i ] = stocks.qte [2].
						
		end.
		
		/* si pas de stocks, recherche d'un mvt depuis le 01/01/2013 */
		if wsto [ 1 ] = 0 and wsto [ 2 ] = 0 then do:
			topmvt = no .

		    rech-vte:
			do i = 1 to 2:
				find first bastat use-index article
				where bastat.codsoc = artapr.codsoc
				and   bastat.articl = artapr.articl
				and   bastat.magasin = entry ( i , l-mag )
				and   bastat.datdep >= 01/01/2013
				no-lock no-error.
				if available bastat then do:
					assign topmvt = yes.
					leave rech-vte.
				end.
			end .
		end .
		
		if wsto [ 1 ] = 0 and wsto [ 2 ] = 0 and topmvt = no then next rech-art.		

		find artbis
        where artbis.codsoc = artapr.codsoc
        and   artbis.motcle = "VTE"
        and   artbis.articl = artapr.articl
        and   artbis.typaux = ""
        and   artbis.codaux = ""
        no-lock no-error.

        find tabges 
        where tabges.codsoc = artapr.codsoc
        and   tabges.etabli = " " 
        and   tabges.typtab = "ART" 
        and   tabges.prefix = "suit-fiche"
        and   tabges.codtab = artapr.articl
        no-lock no-error.
        if available tabges then
            assign woldart [1] = tabges.libel2[1]
                   woldart [2] = tabges.libel2[8]
                   wdeee       = tabges.nombre[15]
                   wecom       = tabges.nombre[24]
                   wstodang    = tabges.libel2[11]
                   wmaglog [1] = tabges.libel2[14]
                   wmaglog [2] = tabges.libel2[15]
                   wamm        = trim(tabges.libel2[18])
                   wcoefrpd    = tabges.nombre[16] 
                   wcoefstat   = tabges.nombre[9] 
                   weaj     = (if tabges.libel2[16] = "0" then "N"      
														  else "O")
		   wtoler   = tabges.nombre [22] .
        else
            assign woldart [1] = ""
                   woldart [2] = ""
                   wdeee       = 0
                   wecom       = 0
                   wmaglog [1] = ""
                   wmaglog [2] = ""
                   wamm = ""
                   wcoefrpd = ?
                   wcoefstat = ?
                   weaj = "?"
                   wstodang = ""
				   wtoler   = 0.

        /* recuperation des taxes eventuelles */ 
        assign wcodtax = ""
               wlibtax = ""
               wdebtax = ?  
               wvaltax = ?.


        do itax = 1 to num-entries (l-codtax):
            wtax = entry(itax,l-codtax) .

            wchrono = 0.    
            if wtax = "RPD" then wchrono = 3.
            assign wrpd3 = ? wrpd5 = ? wdaterpd3 = ? wdaterpd5 = ?.

            find first tarcli 
            where tarcli.codsoc = artapr.codsoc
            and   tarcli.motcle = "VTE"
            and   tarcli.typaux = "TAX"
            and   tarcli.codaux = ""
            and   tarcli.articl = artapr.articl
            and   tarcli.date-debut <= today
            and   tarcli.code-tarif = wtax
            and   tarcli.chrono = wchrono
            no-lock no-error.
            if available tarcli then do:
                if wtax = "RPD" then do:
                    assign wrpd3     = tarcli.valeur / 1000
                           wdaterpd3 = tarcli.date-debut.

                    find first b-tarcli 
                    where b-tarcli.codsoc = artapr.codsoc
                    and   b-tarcli.motcle = "VTE"
                    and   b-tarcli.typaux = "TAX"
                    and   b-tarcli.codaux = ""
                    and   b-tarcli.articl = tarcli.articl
                    and   b-tarcli.date-debut = tarcli.date-debut
                    and   b-tarcli.code-tarif = tarcli.code-tarif
                    and   b-tarcli.chrono = 5
                    no-lock no-error.
                    if available b-tarcli then  
                        assign wrpd5 = b-tarcli.valeur / 1000
                               wdaterpd5 = b-tarcli.date-debut.
                end.
                else  
                    assign wcodtax [itax] = entry(itax,l-codtax)
                           wlibtax [itax] = entry(itax,l-libtax)
                           wvaltax [itax] = tarcli.valeur / 1000
                           wdebtax [itax] = tarcli.date-debut.
            end.
        end.  

		/* recup tarif CLI//HTC  */
		if artapr.codsoc <> "" then do:
			assign  wcodtar = ""
					wvaltar = ?
					wdattar = ?.

			do i = 1 to 2:
				if i = 1 then wcodtar [i] = "CLI".
				if i = 2 then wcodtar [i] = "HTC".
  
  
				find first tarcli 
				where tarcli.codsoc = artapr.codsoc
				and   tarcli.motcle = "VTE"
				and   tarcli.typaux = ""
				and   tarcli.codaux = ""
				and   tarcli.articl = artapr.articl
				and   tarcli.date-debut <= today
				and   tarcli.code-tarif = wcodtar[i]
				no-lock no-error.
				if available tarcli then do:
					assign wvaltar[i] = tarcli.valeur 
						wdattar[i] = tarcli.date-debut.
		
				end.
			end .
		end .

		
        /* recup des infos de dangerosité */
        wclasse = "".
        if artapr.code-toxic <> "" then do:
            find tabges
            where tabges.codsoc = ""
            and   tabges.etabli = ""
            and   tabges.typtab = "art"
            and   tabges.prefix = "toxicite"
            and   tabges.codtab = artapr.code-toxic
            no-lock no-error.
            if available tabges then
                wclasse = tabges.libel3[3].
            else
                wclasse = "Code toxicite inconnu".
        end.

        /* harmonisation code-blocage " " et "o" */ 
        if artapr.code-blocage = ""  or
           artapr.code-blocage = "O"
        then wblo = "O" .
        else wblo = artapr.code-blocage.

        /* recuperation des ventes si wventes = "O" */
        assign wqten[1] = 0 wcan[1] = 0 
               wqten[2] = 0 wcan[2] = 0     .
    
    
        if wventes = "O" then do:
            if wanneedeb[1] <> 0 then do k = 1 to 2 :
                for each starti
                where starti.codsoc = artapr.codsoc
                and   starti.motcle = "vte"       
                and   starti.articl = artapr.articl
                and   starti.annee  >= string(wanneedeb[k])
                and   starti.annee  <= string(wanneefin[k])
                no-lock:
                    if wanneedeb[k] = wanneefin[k] then do:
                        do i = wmoisdeb[1] to wmoisfin[1] :
                            assign wqten[k] = wqten[k] + starti.qte[i]
	                               wcan[k]  = wcan[k]  + starti.fac-ht[i]      .
                        end.	
                    end.
                    else do:
                        if starti.annee = string(wanneedeb[k]) then do:
	                        do i = wmoisdeb[1] to 12:
                                assign wqten[k] = wqten[k] + starti.qte[i]
                                       wcan[k]  = wcan[k]  + starti.fac-ht[i]      .
                            end.
                        end.
                        if starti.annee = string(wanneefin[k]) then do:
	                        do i = 1 to wmoisfin[1]:
                                assign wqten[k] = wqten[k] + starti.qte[i]
                                       wcan[k]  = wcan[k]  + starti.fac-ht[i]      .
                            end.
                        end.
                    end.
                end.
            end.
        end.

        /* recuperation des gencod (3max) de l'article */ 
        assign wgen1 = ""     wgen2 = ""       wgen3 = "".
        for each codbar 
        where  codbar.codsoc = artapr.codsoc 
        and    codbar.articl = artapr.articl    
        no-lock by codbar.datmaj desc:
            if wgen1 = ""  then wgen1 = codbar.code-barre.
            else if wgen2 = "" then wgen2 = codbar.code-barre.
                 else if wgen3 = "" then wgen3 = codbar.code-barre. 
        end.
    
        /* recup commentaire fiche groupe */
        wcommgrp = "".
        FIND PROGLIB
        where proglib.codsoc = artapr.codsoc
        and   proglib.motcle = ""
        and   proglib.etabli = ""
        and   proglib.edition = ""
        and   proglib.chrono  = 0
        and   proglib.clef    = "ARTI-COM" + artapr.articl
        no-lock  no-error .
        if available proglib then wcommgrp = proglib.libelle[ 1 ].
		

        /* recup commentaire fiche Societe  */
        wcommsoc = "".
        FIND PROGLIB
        where proglib.codsoc = artapr.codsoc
        and   proglib.motcle = ""
        and   proglib.etabli = ""
        and   proglib.edition = ""
        and    proglib.chrono  = 0
        and   proglib.clef    = "ARTI-COS" + artapr.articl
        no-lock  no-error .
        if available proglib then wcommsoc = proglib.libelle[ 1 ].

        /* ecriture fichier de sortie */
        export stream maquette delimiter ";" 
            artapr.articl      artapr.codsoc          artapr.libart1[1]          artapr.libart
            artapr.famart      artapr.soufam          artapr.sssfam              woldart
            artapr.typart      artapr.tva
            artapr.provenance 
            wblo               artapr.type-condit     artapr.usto
   
            (if available artbis then artbis.ufac
                                 else "????" )
            (if available artbis then artbis.coef-fac
                                 else ? )
            (if available artbis then artbis.surcondit
                                 else ?) 
            artapr.edit-tarif  artapr.bareme          artapr.code-cumul          artapr.edi-etiq
            artapr.typologie   artapr.prx-modif       artapr.qte-lin             artapr.pcb
            artapr.volume      artapr.pds             artapr.tenusto             artapr.code-toxic
            wclasse            artapr.va    
            artapr.typaux-ref[2] + artapr.codaux-ref[2]
            artapr.typaux-ref[1] + artapr.codaux-ref[1]
            artapr.articl-lie  artapr.stat-rayon      artapr.alpha-1             artapr.caract[01]
            artapr.libart2[1]  artapr.articl-usine    artapr.datcre              artapr.datmaj
            artapr.caract[03]  artapr.caract[05]      artapr.caract[06]          wmaglog
            wdeee              artapr.date-tarif[1]   wrpd3                      wdaterpd3
            wrpd5              wdaterpd5              wamm                       wcoefrpd 
            weaj               wcan[1]                wcan[2]                    wqten[1] 
            wqten[2]           artapr.type-mag        wstodang                   artapr.famart2
            artapr.soufam2     artapr.sssfam2         wgen1                      wgen2 
            wgen3              wcoefstat              artapr.unite-hectare       artapr.type-client
            artapr.impvte      artapr.impach          wcodtax [1]                wlibtax [1] 
            wvaltax [1]        wdebtax [1]            wcodtax [2]                wlibtax [2] 
            wvaltax [2]        wdebtax [2]            wcodtax [3]                wlibtax [3] 
            wvaltax [3]        wdebtax [3]            wcodtax [4]                wlibtax [4] 
            wvaltax [4]        wdebtax [4]            wcodtax [5]                wlibtax [5] 
            wvaltax [5]        wdebtax [5]            artapr.code-texte  
            wcommgrp           wcommsoc
            wecom
			wcodtar [1]        wdattar [1 ] 		  wvaltar [1]
			wcodtar [2]        wdattar [2 ] 		  wvaltar [2]
			artapr.tenulot	   artapr.caract[07]  	  artapr.caract[08]      	 
			artapr.caract[09]  artapr.caract[10]      wtoler
			wsto [ 1 ]		   wsto [ 2 ]

            artbis.type-surcondit artapr.emplac
                        .

    end.   /* for each artapr */
output stream maquette close.

message "Ficher genere -> art-stockit.csv" view-as alert-box.

