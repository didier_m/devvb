/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! Deal/nathalie/tty/facarea0.i                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/*              GESTION DES ECHANGES DE DONNEES INFORMATISEES                 */
/*----------------------------------------------------------------------------*/
/* Variables Communes                                       NATHALIE STANDART */
/******************************************************************************/


def var i-i              as int           no-undo .
def var j-j              as int           no-undo . 
def var k-k              as int           no-undo . 
def var l-l              as int           no-undo . 
def var sav-opeimp       as char          no-undo . 

def var nb-lu            as int           no-undo . 
def var nb-trt           as int           no-undo . 

def var nb-err           as int           no-undo . 
def var nb-enreg         as int           no-undo . 
def var mess-lib         as char          no-undo . 
def var lib-err          as char extent 2 no-undo .
def var l-unites         as char          no-undo .

def var ean13-destin     as char          no-undo . 
def var ean13-art        as char          no-undo . 
def var ean13-mag        as char          no-undo . 

def var no-interch       as dec           no-undo . 
def var sav-no-interch   as dec           no-undo . 
def var sav-date-ech     as date          no-undo . 
def var sav-heure-ech    as char          no-undo . 

def var ok-edit          as log           no-undo . 
def var ok-err           as log           no-undo . 
def var ok-maj-interch   as log           no-undo . 
def var ok-rej-bon       as log           no-undo . 
def var ok-rej-ent       as log           no-undo . 
def var ok-rej-interch   as log           no-undo . 
def var ok-interch       as log           no-undo . 
def var ok-fic           as log           no-undo .
def var ok-bon           as log           no-undo . 
def var ok-lignes        as log           no-undo . 
def var ok-zone          as log           no-undo . 
def var ouv-fic          as log           no-undo . 
def var type-enr         as char          no-undo . 
def var enreg            as char          no-undo . 
def var enreg-inter      as char          no-undo . 
def var enreg-ent        as char          no-undo . 

def var typaux-edi       as char          no-undo . 
def var codaux-edi       as char          no-undo . 

def var i-param          as char          no-undo . 
def var o-param          as char          no-undo . 
def var fic-ext          as char          no-undo . 

def var fic-rej          as char          no-undo . 
def var fic-blo          as char          no-undo . 
def var fic-log          as char          no-undo . 


def {1} shared var libelle        as char extent 350          no-undo .
def {1} shared var nom-applic     as char                     no-undo .
def {1} shared var type-trt       as char                     no-undo .
def {1} shared var auxapr-soc     as char                     no-undo .
def {1} shared var articl-soc     as char                     no-undo .
def {1} shared var codbar-soc     as char                     no-undo .
def {1} shared var magasi-soc     as char                     no-undo .
def {1} shared var connect-soc    as char                     no-undo .
def {1} shared var ean13-emet     as char                     no-undo .
def {1} shared var siren-emet     as char                     no-undo .
def {1} shared var ident-cee-emet as char                     no-undo .
def {1} shared var mess-err       as char                     no-undo .
def {1} shared var repert-dep     as char                     no-undo .
def {1} shared var repert-arriv   as char                     no-undo .
def {1} shared var repert-traite  as char                     no-undo .
def {1} shared var repert-traite-cde  as char                 no-undo .
def {1} shared var repert-rejet   as char                     no-undo .
def {1} shared var repert-tra     as char                     no-undo .
def {1} shared var repert-rech    as char                     no-undo .
def {1} shared var fic-rej-trt    as char format "x(70)"      no-undo .
def {1} shared var fic-non-ext    as char                     no-undo .
def {1} shared var fic-lis        as char                     no-undo .
def {1} shared var fic-lis-2      as char                     no-undo .
def {1} shared var fic-sav        as char                     no-undo .
def {1} shared var nomimp         as char                     no-undo .

def {1} shared var date-j         as char                     no-undo .
def {1} shared var dev-gescom     as char                     no-undo .
def {1} shared var ext-rej        as char                     no-undo .
def {1} shared var ext-log        as char                     no-undo .
def {1} shared var ext-trt        as char                     no-undo .
def {1} shared var ext-lis        as char                     no-undo .

def {1} shared var unite-def      as char                     no-undo .
def {1} shared var motcle-ach     as char                     no-undo .
def {1} shared var l-soc          as char                     no-undo .
def {1} shared var l-tiers        as char                     no-undo .
def {1} shared var nomfic-cyrus   as char                     no-undo .
def {1} shared var nomfic-facture as char                     no-undo .
              
def {1} shared var trt-rejet      as log                      no-undo .
def {1} shared var trt-test       as log                      no-undo .

def {1} shared var repert-depit   as char                     no-undo .

