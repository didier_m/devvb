/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! Deal/nathalie/tty/gesgvs-p.i                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/*              GESTION DES ECHANGES DE DONNEES INFORMATISEES                 */
/*----------------------------------------------------------------------------*/
/* Procedures E.D.I.                                        NATHALIE STANDART */
/******************************************************************************/

/* Lecture Correspondances E.D.I. ( DEAL -> EDI ) */
PROCEDURE LECT-COR :

    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "EDI"
                and   tabges.prefix = prefix
                and   tabges.codtab = codtab
                no-lock no-error .

    if available tabges and tabges.libel1[ mem-langue ] <> ""
       then codtab = entry( 1, right-trim( tabges.libel1[ mem-langue ] ) ) .

END PROCEDURE .


/* Chmod 777 sur Fichier genere */
PROCEDURE CHMOD :

    if opsys <> "UNIX" then return .

    def input parameter nom-fic as char no-undo .

    def var commande as char no-undo .

    if search( nom-fic ) = ?
    then do :
        output to value( nom-fic ) .
        output close .
        commande = "chmod 777 " + nom-fic .
        os-command silent value( commande ) .
    end .

END PROCEDURE .

/* Generation Fichier Blocage */
PROCEDURE GENE-FIC-BLO :

    fic-blo = repert-tra + type-trt + ".blo" .

    if search( fic-blo ) <> ?
    then do :
        if zone-charg[ 9 ] <> "BATCH"
        then do :
            bell . bell .
            message caps( type-trt ) entry( 1, libelle[ 17 ] ) skip
                    entry( 2, libelle[ 17 ] ) view-as alert-box warning .
        end.
        else do :
            message caps( type-trt ) entry( 1, libelle[ 17 ] ) .
            message entry( 2, libelle[ 17 ] ) .
        end .
        ok-fic = no .
        return .
    end .

    output to value( fic-blo ) .
    output close .

    ok-fic = yes .

END PROCEDURE .

/* MAJ No Interchange */
PROCEDURE MAJ-INTERCH :

    codtab = caps( string ( type-trt, "x(10)" )     + "-"   + 
                   string ( ean13-emet  , "x(13)" ) + "-"   + 
				   string ( ean13-destin, "x(13)" ) )  .

    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "EDI"
                and   tabges.prefix = "INTERCHANG"
                and   tabges.codtab = codtab
                exclusive-lock no-error .

    if not available tabges
    then do :

        CREATE TABGES .

        assign tabges.codsoc = ""
               tabges.etabli = ""
               tabges.typtab = "EDI"
               tabges.prefix = "INTERCHANG"
               tabges.codtab = codtab
               .

    end .

    if ok-maj-interch
    then do :

        assign sav-no-interch     = tabges.nombre[ 1 ]
               sav-date-ech       = tabges.dattab[ 1 ]
               sav-heure-ech      = tabges.libel1[ 1 ]
               tabges.nombre[ 1 ] = tabges.nombre[ 1 ] + 1
               no-interch         = tabges.nombre[ 1 ]
               tabges.dattab[ 1 ] = today
               tabges.libel1[ 1 ] = string( time, "hh:mm:ss" )
               .

        { majmoucb.i tabges }

        VALIDATE TABGES .
        RELEASE TABGES .

    end .

    else if no-interch = tabges.nombre[ 1 ]   then do :

             assign tabges.nombre[ 1 ] = sav-no-interch    
                    tabges.dattab[ 1 ] = sav-date-ech      
                    tabges.libel1[ 1 ] = sav-heure-ech     
                    .

             { majmoucb.i tabges }

             if tabges.nombre[ 1 ] = 0 then DELETE TABGES .

             VALIDATE TABGES .
             RELEASE TABGES .

    end .

END PROCEDURE .


