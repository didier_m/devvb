/*==========================================================================*/
/*                           E X T C Y L - I . I                            */
/* Extraction des donnees pour Cylande                                      */
/*--------------------------------------------------------------------------*/
/* interface POINTS  -> UR                               */
/*==========================================================================*/


CREATE SAX-WRITER hSAXWriter.
hSAXWriter:ENCODING = "UTF-8".
hSAXWriter:FORMATTED = false.  /* 	si TRUE: fichier de sortie lisible mais ajout de blancs a la place des sauts de lignes a l'integration dans UR */

lOK = hSAXWriter:SET-OUTPUT-DESTINATION ( "file", wficsortie ).
lOK = hSAXWriter:START-DOCUMENT ( ).
lOK = hSAXWriter:WRITE-COMMENT ( "POINTS ACQUIS SUR DEAL" ) .
lOK = hSAXWriter:START-ELEMENT ( "loyaltyMvtListType" ).

/* ecriture fichier points */       
  
for each t-ptslim
where t-ptslim.soc-cfi = soc-trt
no-lock:
   nblus = nblus + 1.
   wzone = string ( year( t-ptslim.dat-pts) ) + "-" + string ( month( t-ptslim.dat-pts), "99" ) + "-" + string ( day( t-ptslim.dat-pts), "99" ) .
   
   ASSIGN 	
      lOK = hSAXWriter:START-ELEMENT ( "values" )	
      lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "mvtDate" , wzone ).
      if t-ptslim.nb-pts > 0 then 
         assign
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "action" , "C" ).
      else
         assign 
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "action" , "D" ).
      assign
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type" , "MANU" )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "state" , "INTE" )
            lOK = hSAXWriter:START-ELEMENT ( "loyaltyProgram")
       
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code" , entry ( i-soc, l-progfid )  )
            lOK = hSAXWriter:END-ELEMENT ( "loyaltyProgram")
            
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "loyaltySupportId" , t-ptslim.no-cfi )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "value" , string (t-ptslim.nb-pts) )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "reason" , "DEAL" )
      lOK = hSAXWriter:END-ELEMENT ( "values" )	.             /* fin etiquette  "values" */
   if lOK then  nbecr = nbecr + 1.
end.      
     
   /* if nbenreg > 50 then leave. */
lOK = hSAXWriter:END-ELEMENT ( "loyaltyMvtListType" ) .
lOK = hSAXWriter:END-DOCUMENT () .
DELETE OBJECT hSAXWriter. 
