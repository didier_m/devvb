/******************************************************************************/
/* GESCYLP.I - GESTION DES ECHANGES CYLANDE - NATHALIE APPROS                  */
/*----------------------------------------------------------------------------*/
/* Procedures Communes                                  04/2009 - EUREA - 273 */
/******************************************************************************/

/* Initialisations */
PROCEDURE CHARG-PARAM :

    def var repert-trav  as char no-undo .
    def var l-fic-rac    as char no-undo .
    def var l-fic-ext    as char no-undo .
    def var ext-blo      as char no-undo .
    def var l-separateur as char no-undo .
    def var operat-maj   as char no-undo .
    def var nomimp       as char no-undo .
    def var l-motcle     as char no-undo .
    def var l-typcom     as char no-undo .
    def var l-param-div  as char no-undo .
    def var l-param-rem  as char no-undo .

    if type-charg <> "1"
    then do :

        /* Chargement aide et texte */
        assign aide-fichier = "gescyl0.aid"
               aide-par     = ""
               .

        run aide-lec.p .
        do ind = 1 to 140 :
            aide-m = "libel" + string( ind, "999" ) .
            { aide-lib.i }
            libelle[ ind ] = aide-lib .
        end .

    end .

    assign ok-trt     = yes
           codsoc-soc = ""
           .

    /* Chargement parametres CYLANDE */
    assign  typtab = "CYL"  prefix = "GES-CYL"  codtab = "PARAMETRES" .
    run ch-paray.p( output o-param ) .

    if o-param = ""
    then do :
        bell . bell .
        message libelle[ 51 ] skip libelle[ 52 ] skip libelle[ 53 ]
                view-as alert-box error .
        ok-trt = no .
        return .
    end .

    if zone-charg[ 1 ] = "REJET" then trt-rejet = yes .
    if zone-charg[ 2 ] = "TEST"  then trt-test  = yes .

    case type-charg :

        when "0" then if not trt-rejet then return .                                       /* Lance depuis gescyl0.p ( Interactif ) */

        when "1" then do :                                                                 /* Traitement Batch repetitif             */
            nb-sec-att = int( entry( 11 , o-param, "|" ) ) .                               /* Nbre secondes attente rech. fichier    */
            if nb-sec-att <= 0 or int( entry( 12 , o-param, "|" ) ) = 1 then ok-trt = no . /* Arret Traitement                       */
            return .
        end .

    end case .

    assign repert-arriv  = entry(  1, o-param, "|" ) /* Rep. import fichiers                       */
           repert-traite = entry(  2, o-param, "|" ) /* Rep. sauvegarde fichiers                   */
           repert-rejet  = entry(  3, o-param, "|" ) /* Rep. rejets fichiers                       */
           repert-trav   = entry(  4, o-param, "|" ) /* Rep. de travail                            */
           l-fic-rac     = entry(  5, o-param, "|" ) /* Racine Nom Fichier                         */
           l-fic-ext     = entry(  6, o-param, "|" ) /* Extensions Fichiers                        */
           l-section     = entry(  7, o-param, "|" ) /* Liste Sections possibles                   */
                         + entry(  8, o-param, "|" )                                                 
           l-sect-code   = entry(  9, o-param, "|" ) /* Liste Sections en Codes                    */
           l-sect-exclus = entry( 10, o-param, "|" ) /* Liste Sections exclues                     */
           l-separateur  = entry( 31, o-param, "|" ) /* Liste Separateurs                          */
           l-motcle      = entry( 32, o-param, "|" ) /* Liste Motcles                              */
           l-typcom      = entry( 33, o-param, "|" ) /* Liste Types Bons generes                   */
           l-param-div   = entry( 34, o-param, "|" ) /* Liste Parametres divers                    */
           l-codpri[ 1 ] = entry( 35, o-param, "|" ) /* Liste Codes Prix CYLANDE                      */
           l-codpri[ 2 ] = entry( 36, o-param, "|" ) /* Liste Codes Prix NATHALIE                  */
           l-typrem[ 1 ] = entry( 37, o-param, "|" ) /* Liste Types Remises CYLANDE                */
           l-typrem[ 2 ] = entry( 38, o-param, "|" ) /* Liste Types Remises NATHALIE               */
           l-top-trt     = entry( 39, o-param, "|" ) /* Liste Tops trt mvts treso.                 */
           operat-maj    = entry( 40, o-param, "|" ) /* Operateur MAJ                              */
           nomimp        = entry( 41, o-param, "|" ) /* Imprimante                                 */
           l-nature[ 1 ] = entry( 42, o-param, "|" ) /* Liste Natures Ventes                       */
           l-nature[ 2 ] = entry( 43, o-param, "|" ) /* Liste Natures Paiement                     */
                                                     /* entry( 44, o-param ) -> gencyl0.p         */
           l-soc         = entry( 45, o-param, "|" ) /* Liste Societes CYLANDE                     */
           l-top-lisadis = entry( 46, o-param, "|" ) /* Tops envoi stocks + qtes vendues a LISADIS */
           l-profession  = entry( 47, o-param, "|" ) /* Liste Codes Professions                    */
           l-tps-deplac  = entry( 48, o-param, "|" ) /* Liste Codes Temps Deplacement vers Magasin */
           l-top-market  = entry( 49, o-param, "|" ) /* Liste Valeurs Tops Marketing               */
           l-typclt[ 1 ] = entry( 50, o-param, "|" ) /* Liste Types Clients CYLANDE                */
           l-typpts      = entry( 51, o-param, "|" ) /* Liste Types Points                         */
           l-typclt[ 2 ] = entry( 52, o-param, "|" ) /* Liste Types Clients NATHALIE               */
           l-civilite    = entry( 53, o-param, "|" ) /* Liste Civilites                            */
           l-socdeal     = entry( 54, o-param, "|" ) /* Soc DEAL                                   */

           date-j        = string( today, "999999" )                  + "-" +
                           substr( string( time, "hh:mm:ss" ), 1, 2 ) +
                           substr( string( time, "hh:mm:ss" ), 4, 2 ) +
                           substr( string( time, "hh:mm:ss" ), 7, 2 )
           .

    assign ext-trt = entry( 1, l-fic-ext )
           ext-rej = entry( 2, l-fic-ext ) 
           ext-blo = entry( 3, l-fic-ext )
           no-error .

    if ext-trt = "" then ext-trt = ".ECH" .
    if ext-rej = "" then ext-rej = ".rej" .
    if ext-blo = "" then ext-blo = ".blo" .

    if trt-rejet and type-charg = "0" then return . /* Appel depuis gescyl0.p */

    fic-rac = entry( 1, l-fic-rac ) .

    if fic-rac = "" then fic-rac = "mvt-cyl" .

    assign fic-blo[ 1 ] = repert-trav + fic-rac + ext-blo
           fic-blo[ 2 ] = repert-trav + fic-rac + "-b" + ext-blo
           .

    assign separat-ent       = entry( 1, l-separateur, "/" )
           separat-pied      = entry( 2, l-separateur, "/" )
           separat-dec[ 1 ]  = entry( 3, l-separateur, "/" )
           separat-dec[ 2 ]  = entry( 4, l-separateur, "/" )
           separat-zone[ 1 ] = entry( 6, l-separateur, "/" )
           separat-zone[ 2 ] = entry( 7, l-separateur, "/" )
           separat-chp       = int( entry( 5, l-separateur, "/" ) )
           no-error .

    if separat-ent       = "" then separat-ent       = "@" .
    if separat-pied      = "" then separat-pied      = "&" .
    if separat-dec[ 1 ]  = "" then separat-dec[ 1 ]  = "," .
    if separat-dec[ 2 ]  = "" then separat-dec[ 2 ]  = "." .
    if separat-chp       = 0  then separat-chp       = 9 .   /* Tabulation */
    if separat-zone[ 1 ] = "" then separat-zone[ 1 ] = "," .
    if separat-zone[ 2 ] = "" then separat-zone[ 2 ] = ";" .

    assign section-vte          = entry( 1, l-section )
           section-annulvte     = entry( 2, l-section )
           section-treso        = entry( 3, l-section )
           section-annultreso   = entry( 4, l-section )
           section-paidiff      = entry( 5, l-section )
           section-annulpaidiff = entry( 6, l-section )
           section-creationcfi  = entry( 7, l-section )
           section-modifcfi     = entry( 8, l-section )
           section-pointscfi    = entry( 9, l-section )
           no-error .

    if section-vte          = "" then section-vte          = "VENTE" .
    if section-annulvte     = "" then section-annulvte     = "ANNULEVENTE" .
    if section-treso        = "" then section-treso        = "TRESORERIE" .
    if section-annultreso   = "" then section-annultreso   = "ANNULETRESO" .
    if section-paidiff      = "" then section-paidiff      = "TRESORERIE_PAIDIFF" .
    if section-annulpaidiff = "" then section-annulpaidiff = "ANNULETRESO_PAIDIFF" .
    if section-creationcfi  = "" then section-creationcfi  = "CUSTOMER_NEW_CARD" .
    if section-modifcfi     = "" then section-modifcfi     = "CUSTOMER" .
    if section-pointscfi    = "" then section-pointscfi    = "PT_DELTA" .

    if operat-maj <> "" then operat = operat-maj .
    if nomimp     <> "" then opeimp = nomimp .

    assign motcle-vte = entry( 1, l-motcle )
           motcle-cyl = entry( 2, l-motcle )
           motcle-tax = entry( 3, l-motcle )
           motcle-lis = entry( 4, l-motcle )
           no-error .

    if motcle-vte = "" then motcle-vte = "VTE" .
    if motcle-cyl = "" then motcle-cyl = "/AGI" .
    if motcle-tax = "" then motcle-tax = "/TAX" .
    if motcle-lis = "" then motcle-lis = "/LIS" .

    assign typcom-vte = entry( 1, l-typcom )
           typcom-van = entry( 2, l-typcom )
           typcom-vab = entry( 3, l-typcom )
           typcom-ter = entry( 4, l-typcom )
           
           no-error .

    if typcom-vte = "" then typcom-vte = "CYC" .
    if typcom-ter = "" then typcom-ter = "CYT" .
    if typcom-van = "" then typcom-van = "CYS" .
    if typcom-vab = "" then typcom-vab = "CYA" .

    assign typlig-vte     = entry( 1 , l-param-div )
           typlig-rti     = entry( 2 , l-param-div )
           nivcom-vte     = entry( 3 , l-param-div )
           nivcom-vab     = entry( 4 , l-param-div )
           typbon-vte     = entry( 5 , l-param-div )
           typaux-def     = entry( 6 , l-param-div )
           codpri-cli     = entry( 7 , l-param-div )
           codlig-normale = entry( 8 , l-param-div )
           codret-gav     = entry( 9 , l-param-div )
           codmvt-sto     = entry( 10, l-param-div )
           codrem-rfa     = entry( 11, l-param-div )
           codoperat-deb  = entry( 12, l-param-div )
           codreg-mix     = entry( 13, l-param-div )
                         /* entry( 14, l-param-div ) -> gencyl.p */
           top-false      = entry( 15, l-param-div )
           top-true       = entry( 16, l-param-div )
           val-enf-nc     = entry( 17, l-param-div )
           codreg-att     = entry( 18, l-param-div )
           nivcom-ter     = entry( 19, l-param-div )
           
           no-error .

    if typlig-vte     = "" then typlig-vte     = "1-2" .   /* 1 vente, 2 retour */
    if typlig-rti     = "" then typlig-rti     = "6" .     /* 6 remise ticket */
    if typlig-ng      = "" then typlig-ng      = "7" .     /* 6 remise ticket */
    if nivcom-vte     = "" then nivcom-vte     = "5" . 
    if nivcom-vab     = "" then nivcom-vab     = "7" .
    if nivcom-ter     = "" then nivcom-ter     = "4-3" .
    if typbon-vte     = "" then typbon-vte     = "C" .
    if typaux-def     = "" then typaux-def     = "TIE" .
    if codpri-cli     = "" then codpri-cli     = "CLI" .
    if codlig-normale = "" then codlig-normale = "N" .
    if codret-gav     = "" then codret-gav     = "GAV" .
    if codmvt-sto     = "" then codmvt-sto     = "250" .
    if codrem-rfa     = "" then codrem-rfa     = "RFA" .
    if codoperat-deb  = "" then codoperat-deb  = "MO;" .
    if codreg-mix     = "" then codreg-mix     = "MIX" .
    if top-false      = "" then top-false      = "FALSE" .
    if top-true       = "" then top-true       = "TRUE" .
    if val-enf-nc     = "" then val-enf-nc     = "NC" .
    if codreg-att     = "" then codreg-att     = "ATT" .
   
    nb-jint = 10.
    if entry( 20, l-param-div ) <> "" then do:
      nb-jint       = int (  entry( 20, l-param-div ) ) no-error.
      if error-status:error or nb-jint = 0 or nb-jint = ? then 
         nb-jint = 10.      
    end.
    
    if l-codpri[ 1 ] = "" then l-codpri[ 1 ] = "C,D,L,P".
    if l-codpri[ 2 ] = "" then l-codpri[ 2 ] = "G,PRO,PRO,CMP".

    
    /* uniquement des remises en valeurs ( les remises en % sont traduites en montant )  */ 
    if l-typrem[ 1 ] = "" then l-typrem[ 1 ] = "V" .
    if l-typrem[ 2 ] = "" then l-typrem[ 2 ] = "2" .

    if l-sect-code = "" then l-sect-code = ",,T,AT,P,AP" .

    if l-top-trt   = "" then l-top-trt   = "O,N,T" .

    if l-nature[ 1 ] = "" then l-nature[ 1 ] = "01" .
    if l-nature[ 2 ] = "" then l-nature[ 2 ] = "00" . 

    if l-soc = "" then l-soc = "01,08,59,88" .
    if l-socdeal = "" then l-socdeal = "01,08,59" .

    if l-top-lisadis = "" then l-top-lisadis = "E,T,R" .

    if l-profession = "" then l-profession = "0,1,2,3,4,5,6" .

    if l-tps-deplac = "" then l-tps-deplac = "0,1,2,3" .

    if num-entries( l-top-market ) <> 2 then l-top-market = "0,1" .

    if l-typclt[ 1 ] = "" then l-typclt[ 1 ] = "GP" .
    if l-typclt[ 2 ] = "" then l-typclt[ 2 ] = "P" .

    if l-typpts = "" then l-typpts = "12,31,41" .

    if l-civilite = "" then l-civilite = "MR,MME,MLE" .

    /* Chargement Devise Gestion */                      
    assign  typtab = "PRM"  prefix = "TX-EUR-FRF"  codtab = "" .
    run ch-param.p( output dev-gescom ) .
    if dev-gescom = "" then dev-gescom = "EUR" .

    /* Chargement Parametres Cartes de Fidelite */                      
    assign  typtab = "PRM"  prefix = "CARTE-FID"  codtab = "" .
    run ch-parax.p( output o-param ) .

    assign motcle-cfi          = entry( 1, o-param, "/" ) 
           top-envoi-cfi       = entry( 2, o-param, "/" )
           top-attente-cfi     = entry( 3, o-param, "/" )
           orig-cre-cfi        = entry( 4, o-param, "/" )
           l-soc-cfi[ 1 ]      = entry( 5, o-param, "/" )
           l-soc-cfi[ 2 ]      = entry( 6, o-param, "/" )
           l-debut-no-cfi      = entry( 7, o-param, "/" )
           top-envoye-cfi      = entry( 9, o-param, "/" )
           pts-bvenue-cfi[ 1 ] = dec( entry( 11, o-param, "/" ) )
           pts-bvenue-cfi[ 2 ] = dec( entry( 12, o-param, "/" ) )
           no-error .

    if error-status:error or num-entries( l-soc-cfi[ 2 ] ) < 2 or num-entries( l-debut-no-cfi ) < 2
    then do :
        bell . bell .
        message libelle[ 51 ] skip libelle[ 78 ] skip libelle[ 53 ]
                view-as alert-box error .
        ok-trt = no .
        return .
    end .

    if motcle-cfi = "" then motcle-cfi = "CFI" .

    /* Chargement Parametres Plateformes */                      
    assign  typtab = "PLA"  prefix = "GES-PLATEF"  codtab = "PARAMETRES" .
    run ch-paray.p( output o-param ) .

    if o-param = ""
    then do :
        bell . bell .
        message libelle[ 51 ] skip libelle[ 117 ] skip libelle[ 53 ]
                view-as alert-box error .
        ok-trt = no .
        return .
    end .

    assign ver-extvtlisadis = int( entry( 12, o-param, "|" ) )
           ok-mess          = yes
           .

END PROCEDURE . /* CHARG-PARAM */

/* Traitement des Rejets */
PROCEDURE TRT-REJET :

    def var mess-lib as char no-undo .

    if nb-err = 0 /* Ouverture Fichier Erreurs */
    then do :
        output stream s-fic-rej to value( fic-rej ) .
        mess-lib = "/* " + chr( 27 ) + "&l1O" + libelle[ 24 ] + string( today ) + " " + string( time, "hh:mm:ss" ) . /* Mise a l'italienne */
        put stream s-fic-rej unformatted mess-lib skip( 1 ) .
        mess-lib = "/* " + libelle[ 25 ] + " " + fic-rej .
        put stream s-fic-rej unformatted mess-lib skip .
        mess-lib = "/* " + libelle[ 26 ] + " " + fic-trt .
        put stream s-fic-rej unformatted mess-lib skip( 2 ) .
    end .

    if ok-mess
    then do :
        mess-lib = libelle[ 31 ] .
        if available lignes-trav then mess-lib = mess-lib + string( lignes-trav.no-enreg ) .
                                 else mess-lib = mess-lib + string( nb-enr-lu ) .
        put stream s-fic-rej unformatted mess-lib skip .
    end .

    mess-err = "/* " + mess-err .
    put stream s-fic-rej unformatted mess-err skip .

    FOR EACH LIGNES-TRAV no-lock : 

        mess-lib = libelle[ 31 ] + string( lignes-trav.no-enreg ) .
        put stream s-fic-rej unformatted mess-lib skip .

        put stream s-fic-rej unformatted lignes-trav.enreg skip .        

    END .

    put stream s-fic-rej unformatted skip( 1 ) .

    EMPTY temp-table LIGNES-TRAV .

    assign nb-err = nb-err + 1
           ok-mvt = no
           .

END PROCEDURE . /* TRT-REJET */









