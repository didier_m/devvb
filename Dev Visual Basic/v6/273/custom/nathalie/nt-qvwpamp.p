/*  Nt-qvwpamp.p   pgm permettant de g�n�rer les fichiers des pamp valid�s  */
/*                 afin de les int�grer ensuite dans qlikview               */
/*  le programme cr�e un fichier par soci�t� � traiter et le d�pose dans    */
/*  applic/li/depart; Il est ensuite r�cup�rer automatiquement dans la nuit */
/****************************************************************************/
/* VLE 18112014 : ajout Delphine dans envoi de mail                         */
/* VLE 28122017 : correction envoi de mail skip(1)                          */
/* DGR 16042019 : le programme lance la creation des tarifs INT 01 08 59    */
/* CCO 23072019 : ajout acces table MAIL                                    */

/*  deb en + pour lanceur web */
{ d-brokmini.i new }   

def frame fr-saisie .

{ bat-ini.i  new }
{ select.i     new }
{ aide.i       new }
{ maquette.i   new }  

DEFINE STREAM maquette.
def stream qvwpamp.
def stream pampmail.

def stream entree.
 
DEFINE VARIABLE x-prgclient AS CHARACTER   NO-UNDO.

def var    x-alpha1         as char       no-undo .
def var    x-alpha2         as char       no-undo .
def var    x-alpha3         as char       no-undo .
def var    x-alpha4         as char       no-undo .
def var    x-alpha5         as char       no-undo .
def var    x-alpha6         as char       no-undo .
def var    x-alpha7         as char       no-undo .
def var    x-alpha8         as char       no-undo .
def var    x-alpha9         as char       no-undo .
DEF VAR    x-alpha10        as char       no-undo .

DEF VAR    repert-mail      AS CHAR       NO-UNDO.

/*  fin en + pour lanceur web */
def input parameter  fichier  as char format "x(12)".
run bat-lec.p ( fichier ) .

{ bat-cha.i  "sel-applic"     "selection-applic"   "a"          }
{ bat-cha.i  "sel-filtre"     "selection-filtre"   "a"          }
{ bat-cha.i  "sel-sequence"   "selection-sequence" "a"          }
{ bat-cha.i  "codsoc"         "glo-codsoc"         "a"          }
{ bat-cha.i  "etabli"         "glo-etabli"         "a"          }

{ bat-cha.i  "ma-impri"      "ma-impri"            "a"          }   
{ bat-cha.i  "ma-maquet"      "ma-maquet"          "a"          } 
{ bat-cha.i  "lib-prog"       "lib-prog"           "a"          }

{ bat-cha.i  "prgclient"     "x-prgclient"         "a"          }   

{ bat-cha.i  "alpha1"       "x-alpha1"           "a"          }   
{ bat-cha.i  "alpha2"       "x-alpha2"           "a"          }
{ bat-cha.i  "alpha3"       "x-alpha3"           "a"          }
{ bat-cha.i  "alpha4"       "x-alpha4"           "a"          }
{ bat-cha.i  "alpha5"       "x-alpha5"           "a"          }
{ bat-cha.i  "alpha6"       "x-alpha6"           "a"          }
{ bat-cha.i  "alpha7"       "x-alpha7"           "a"          }
{ bat-cha.i  "alpha8"       "x-alpha8"           "a"          }
{ bat-cha.i  "alpha9"       "x-alpha9"           "a"          }
{ bat-cha.i  "alpha10"      "x-alpha10"          "a"          }

run select-c.p.  

regs-app = "elodie".
{regs-cha.i }
regs-app = "nathalie".
{regs-cha.i}

deal-setsocreg ( "" ) .
deal-setsocreg ( glo-codsoc ) .

run sel-del.p ( selection-applic, selection-filtre, selection-sequence, yes ) .  

run traitement.

PROCEDURE TRAITEMENT:

/* donn�es saisies */
def var wsoc         as char format "x(14)"         no-undo.
def var wexercice    as char format "xxxx"          no-undo.
def var wperdebval   as char format "x(6)"          no-undo.
def var wperfinval   as char format "x(6)"          no-undo.
def var wvalid       as char format "x"             no-undo.

def var l-soc as char init "01,08,42,58,59".
def buffer artapr-soc for artapr.
def var nbpamp             as int extent 6.
def var wtaux              as dec.
def var perdeb             as char.
def var perfin             as char.
def var derpamp            as dec.
def var i                  as int.
def var j                  as int.
def var k                  as int. 
def var l-soufamtx         as char init "097,098".
def var l-famtx            as char init "080,081,082,083,084,085".

def var wperdebprev  as char format "x(6)".
def var wperfinprev  as char format "x(6)".
def var fic-qvw           as char.
def var fic-mail           as char.
def var fic-mail-tmp           as char.
def var annee            as int format "9999".
def var mois             as int format "99".
def var jour             as int format "99".
def var heure            as char.
def var wficlogcreint    as char.

session:system-alert-boxes = yes.
message "Debut programme" program-name( 1 ) .
message " " .
Message "Debut extraction des pamp pour QVW le "
        today "a" string ( time ,"hh:mm:ss" ). 
message " " . 

/* recup parametre web */
  wsoc        = trim(x-alpha1).  /* codes societe */
  wexercice   = trim(x-alpha2).  /* exercice budget*/
  wperdebval  = trim(x-alpha3).  /* periode de d�but validation */
  wperfinval  = trim(x-alpha4).  /* periode de fin validation */
  wvalid      = trim(x-alpha5).  /* Validation OUI==> maj */

  /* Verif presence du parametre TABGES pour la validation des pamp */
  find tabges
  where tabges.codsoc = ""
  and   tabges.etabli = ""
  and   tabges.typtab = "PRM"
  and   tabges.prefix = "PRG-CLIENT"
  and   tabges.codtab = "QVW-PAMP"
  no-lock no-error.
  if not available tabges then do:
     message "PARAMETRAGE TABGES QVW-PAMP INEXISTANT".
     message "SORTIE ANORMALE DU PROGRAMME".
    leave.
  end.     

/* conrtrole des donn�es saisies */

  if wsoc = ? then do:
     message "CODE SOCIETE NON RENSEIGNE".
     message "SORTIE ANORMALE DU PROGRAMME".
    leave.
  end.  
  if wexercice = ? then do:
     message "EXERCICE NON RENSEIGNE".
     message "SORTIE ANORMALE DU PROGRAMME".
    leave.
  end.
  if wperdebval = ? then do:
     message "DEBUT VALIDATION NON RENSEIGNEE".
     message "SORTIE ANORMALE DU PROGRAMME".
    leave.
  end.
  if wperfinval = ? then do:
     message "DEBUT VALIDATION NON RENSEIGNEE".
     message "SORTIE ANORMALE DU PROGRAMME".
    leave.
  end.
  
  do i = 1 to num-entries(wsoc) :
     if lookup(entry(1, wsoc), l-soc) = 0 then do :
	    message "CODE SOCIETE INCORRECTE".
        message "SORTIE ANORMALE DU PROGRAMME".
       leave.
    end.
  end.
  if substr(wperdebval, 5, 2) <> "07"  then do:
     message "MOIS DE DEBUT DIFFERENT DE 07".
     message "SORTIE ANORMALE DU PROGRAMME".
    leave.
  end.
  if substr(wperfinval, 5, 2) < "01" or
     substr(wperfinval, 5, 2) > "12"  then do:
     message "MOIS DE FIN VALIDATION INCORRECT".
     message "SORTIE ANORMALE DU PROGRAMME".
    leave.
  end.  
  if wperfinval < wperdebval then do:
     message "DEBUT VALIDATION ET FIN VALIDATION INCOMPATIBLES".
     message "SORTIE ANORMALE DU PROGRAMME".
    leave.
  end.
  
  if wvalid = "N" then leave.

/* initialisation des p�riodes pr�visionnelles */
wperfinprev = "209912".
if substr(wperfinval, 5, 2) <> "12" then
   assign wperdebprev = string(int(wperfinval) + 1).
else
   assign substr(wperdebprev, 1, 4) = string(int(substr(wperfinval, 1, 4)) + 1)
	      substr(wperdebprev, 5, 2) = "01".
		  
/* fichier mail */
annee = year(today).
mois = month(today).
jour = day(today).
heure = replace(string(time, "hh:mm:ss"), ":", "").

FIND tabges WHERE tabges.codsoc = ""
            AND   tabges.etabli = ""
            AND   tabges.typtab = "PRM"  
            AND   tabges.prefix = "ENVOI-MAIL"  
            AND   tabges.codtab = "" 
            NO-LOCK NO-ERROR.
if AVAILABLE tabges THEN  
   ASSIGN repert-mail = TRIM( tabges.libel1[1] + tabges.libel1[2] ) .

/* Presence nouveau repertoire erp */

if search( "/applic/erp/src/tty/nathalie/ch-param.p" ) <> ? THEN DO:
   FIND tabges WHERE tabges.codsoc = ""
               AND   tabges.etabli = ""
               AND   tabges.typtab = "PRM"  
               AND   tabges.prefix = "ENVOI-MAIL"  
               AND   tabges.codtab = "new" 
               NO-LOCK NO-ERROR.
   if AVAILABLE tabges THEN  
      ASSIGN repert-mail = TRIM( tabges.libel1[1] + tabges.libel1[2] ) .
END.

fic-mail-tmp = repert-mail + "validpamp_" + string(annee) + string(mois, "99") + string(jour, "99")  + "_" + heure + ".tmp" .
fic-mail     = repert-mail + "validpamp_" + string(annee) + string(mois, "99") + string(jour, "99")  + "_" + heure + ".mail" .
output stream pampmail to value( fic-mail-tmp ).

/* traitement de chargement des pamp        */
/* on cree un fichier par societe � traiter */
		  
do i = 1 to num-entries(wsoc) :

  fic-qvw = "/applic/li/depart/QVW_PAMP_"  + string(entry(i, wsoc)) + ".txt".
  output stream qvwpamp to value( fic-qvw ).
  export stream qvwpamp delimiter ";"
  "codsoc" "article" "perdeb" "perfin" "pamp" "validation" "taux" "marge" "typetrt".

  for each artapr-soc
  where artapr-soc.codsoc = entry(i, wsoc)
  no-lock:
  
  find artapr 
  where artapr.codsoc = '00'
  and artapr.articl = artapr-soc.articl
  no-lock no-error.
  
  if not available artapr
  then next.
  
  /* traitement des non tenus en stock et prestations  
  on prend le taux de marge dans la sous famille */        

  if lookup(artapr-soc.soufam, l-soufamtx) <> 0 or
     lookup(artapr-soc.famart, l-famtx) <> 0 then do :
         
         wtaux = 0.
         find tabges 
         where tabges.codsoc = ""
         and   tabges.etabli = ""
         and   tabges.typtab = "art"
         and   tabges.prefix = "soufam" + artapr-soc.famart
         and   tabges.codtab = artapr-soc.soufam
         no-lock no-error.          
         if available tabges 
            then wtaux = tabges.nombre[2].
         /* pamp valid� */
         export stream qvwpamp delimiter ";" 
         artapr-soc.codsoc artapr-soc.articl wperdebval wperfinval 0 "O" wtaux 0 "T".
         /* pamp previsionnel */
         export stream qvwpamp delimiter ";" 
         artapr-soc.codsoc artapr-soc.articl wperdebprev wperfinprev 0 "N" wtaux 0 "T".
         next.

  end.  /* traitement des non tenus en stock et prestations */         
 
  /* pour les autres produits on prend le pamp */ 
  
  derpamp = 999999.999.
     
  /* recherche du pamp de d�but d'exercice  */

   if substring(wperdebval, 1, 4) = substring(wperfinval, 1, 4)
      then k = int(substring(wperfinval, 5, 2)). /* dernier mois � traiter  */
      else k = 12.  /* on traite jusqu'en d�cembre  */

   find stocks
   where stocks.codsoc = entry(i, wsoc)
   and   stocks.exercice = substring(wperdebval, 1, 4)
   and   stocks.magasin = ""
   and   stocks.evenement = "STOARR"
   and   stocks.articl = artapr-soc.articl
   no-lock no-error.

   if available stocks then do :

    do j = 7 to k :
        if j = 7 then 
        assign derpamp = stocks.prix[j]
               perdeb = stocks.exercice + string(j, "99").
        nbpamp[i] = nbpamp[i] + 1.
        If derpamp <> stocks.prix[j] then  do :
           perfin = stocks.exercice + string(j - 1, "99").
           export stream qvwpamp delimiter ";" 
           stocks.codsoc stocks.articl perdeb perfin derpamp "O" 0 0 "P".
           assign derpamp = stocks.prix[j]
                  perdeb = stocks.exercice + string(j, "99").        
        end.
    end.
    perfin = stocks.exercice + string(j - 1, "99").
    export stream qvwpamp delimiter ";" 
    stocks.codsoc stocks.articl perdeb perfin derpamp "O" 0 0 "P".
   
   end. /* pamp debut d'exercice */

   /* recherche du pamp de fin d'exercice  si necessaire */
   if substring(wperdebval, 1, 4) <> substring(wperfinval, 1, 4)
      then do :
      
   k = int(substring(wperfinval, 5, 2)). /* dernier mois � traiter */

   find stocks
   where stocks.codsoc = entry(i, wsoc)
   and   stocks.exercice = substring(wperfinval, 1, 4)
   and   stocks.magasin = ""
   and   stocks.evenement = "STOARR"
   and   stocks.articl = artapr-soc.articl
   no-lock no-error.
   
   if available stocks then do :
     
     do j = 1 to k :
        if j = 1 then 
        assign derpamp = stocks.prix[j]
               perdeb = stocks.exercice + string(j, "99").
        nbpamp[i] = nbpamp[i] + 1.
        If derpamp <> stocks.prix[j] then  do :
           perfin = stocks.exercice + string(j - 1, "99").
           export stream qvwpamp delimiter ";" 
           stocks.codsoc stocks.articl perdeb perfin derpamp "O" 0 0 "P".
           assign derpamp = stocks.prix[j]
                  perdeb = stocks.exercice + string(j, "99").        
         end.
     end.
     perfin = stocks.exercice + string(j - 1, "99").
     export stream qvwpamp delimiter ";" 
     stocks.codsoc stocks.articl perdeb perfin derpamp "O" 0 0 "P".
   end. /* available stocks */
   end. /* pamp fin exercice */
 
   /* creation du pamp pr�visionnel */
   /* on reporte le dernier pamp si il existe*/
   if derpamp <> 999999.999
      then do :    
      export stream qvwpamp delimiter ";" 
      artapr-soc.codsoc artapr-soc.articl wperdebprev wperfinprev derpamp "N" 0 0 "P".
      next.
   end.

   /* sinon on recherche le taux de marge dans la table des taux pr�visionnels */
   /* d'abord au niveau de la sous sous famille si ils existent*/

   find tabges 
   where tabges.codsoc = entry(i, wsoc)
   and   tabges.etabli = ""
   and   tabges.typtab = "LOI"
   and   tabges.prefix = "QVWTX" + wexercice
   and   tabges.codtab = artapr-soc.famart2 + artapr-soc.soufam2 + 
   artapr-soc.sssfam2
   no-lock no-error.

   if available tabges then do :
      if tabges.nombre[1] <> 0 then
         export stream qvwpamp delimiter ";" 
         artapr-soc.codsoc artapr-soc.articl wperdebprev wperfinprev 0 "N" tabges.nombre[1] tabges.nombre[2] "T".
      else  
         export stream qvwpamp delimiter ";" 
         artapr-soc.codsoc artapr-soc.articl wperdebprev wperfinprev 0 "N" tabges.nombre[1] tabges.nombre[2] "M".
      next.
   end.
    
  /* sinon au niveau de la sous famille si ils existent*/

   find tabges 
   where tabges.codsoc = entry(i, wsoc)
   and   tabges.etabli = ""
   and   tabges.typtab = "LOI"
   and   tabges.prefix = "QVWTX" + wexercice
   and   tabges.codtab = artapr-soc.famart2 + artapr-soc.soufam2
   no-lock no-error.

   if available tabges then do :
      if tabges.nombre[1] <> 0 then
         export stream qvwpamp delimiter ";" 
         artapr-soc.codsoc artapr-soc.articl wperdebprev wperfinprev 0 "N" tabges.nombre[1] tabges.nombre[2] "T".
      else  
         export stream qvwpamp delimiter ";" 
         artapr-soc.codsoc artapr-soc.articl wperdebprev wperfinprev 0 "N" tabges.nombre[1] tabges.nombre[2] "M".
      next.
   end.

   /* sinon au niveau de la famille */

   find tabges 
   where tabges.codsoc = entry(i, wsoc)
   and   tabges.etabli = ""
   and   tabges.typtab = "LOI"
   and   tabges.prefix = "QVWTX" + wexercice
   and   tabges.codtab = artapr-soc.famart2
   no-lock no-error.

   if available tabges then do :
      if tabges.nombre[1] <> 0 then
         export stream qvwpamp delimiter ";" 
         artapr-soc.codsoc artapr-soc.articl wperdebprev wperfinprev 0 "N" tabges.nombre[1] tabges.nombre[2] "T".
      else  
         export stream qvwpamp delimiter ";" 
         artapr-soc.codsoc artapr-soc.articl wperdebprev wperfinprev 0 "N" tabges.nombre[1] tabges.nombre[2] "M".
      next.
   end.
   
  end. /* Boucle artapr societe */
 output stream qvwpamp close.
end. /* boucle societe */

Message " " .
do i = 1 to num-entries( wsoc ) :
        Message "Nb pamp "  entry(i, wsoc) " extraits                : " nbpamp[i].
end. 

/* DGR 16/04/2019 : lancement creation tarif INT */
if search ( "cre-int.p") <> ? or search ("cre-int.r") <> ? then do:
   if wvalid = "O" then do:
      do i = 1 to num-entries( wsoc ) :
         if lookup ( entry ( i,wsoc ) , "01,08,59" )  <> 0 then do:
         
            if wficlogcreint = "" then wficlogcreint = "cre-int." + entry ( i,wsoc )  + "-" + wperfinval + ".txt".
                                  else wficlogcreint = wficlogcreint + ";" + "cre-int." + entry ( i,wsoc )  + "-" + wperfinval + ".txt".
            message "".
            message "LANCEMENT PROGRAMME DE CREATION DES TARIFS INT (cre-int.p ) pour " entry ( i,wsoc) " -- "  string (time, "HH:MM:SS") .
            run cre-int.p ( entry ( i,wsoc ) , wperfinval ,"cre-int." + entry ( i,wsoc )  + "-" + wperfinval + ".txt").
            message "".
            message "FIN PROGRAMME DE CREATION DES TARIFS INT (cre-int.p ) pour " entry ( i,wsoc) " -- "  string (time, "HH:MM:SS") .
            message "".
         end.
      end. 
   end.  
end.

/* ecriture du mail de fin de traitement */

put stream pampmail unformatted "DEALQVW@groupe-eurea.com" skip.
put stream pampmail unformatted "v.letalenet@groupe-eurea.com;d.beniere@groupe-eurea.com" skip.
put stream pampmail unformatted ( wficlogcreint ) skip.
put stream pampmail unformatted "Validation des pamp pour Qlikview" skip.
put stream pampmail unformatted "societe " wsoc skip.
put stream pampmail unformatted "annee budget " wexercice skip.
put stream pampmail unformatted "periode debut validation " wperdebval skip.
put stream pampmail unformatted "periode fin validation " wperfinval skip.
put stream pampmail unformatted "periode debut previsionnel " wperdebprev skip.
put stream pampmail unformatted "periode fin previsionnel " wperfinprev skip.

output stream pampmail close.

/* Rename du fichier pampmail en .mail */
os-rename value(fic-mail-tmp) value(fic-mail).

Message "Fin extraction des pamp pour QVW le " 
        today "a" string ( time , "hh:mm:ss" ). 
Message "Fin programme " program-name( 1 ) .
message " " .
session:system-alert-boxes = no.   

END.
/*  
{ selectpt.i }
*/


