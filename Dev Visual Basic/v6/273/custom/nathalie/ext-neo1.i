/*==========================================================================*/
/*                           E X T - N E O 1 . I                            */
/*                                                                          */
/* Extraction des exploitations                                             */
/*                liens tiers <=> exp associes                              */
/*--------------------------------------------------------------------------*/
/* EXPLOITATIONS */

    zone = trim ( auxapr.codaux )                                          + ";".

    /* supression de la civilite */
    wlib = auxili.adres [1] .
    if wlib begins "MR " or wlib begins "M. " then 
        wlib = substr( wlib, 4 ).
                    
    if wlib begins "MME " or wlib begins "MLE " or wlib begins "MRS "  then 
        wlib = substr( wlib, 5 ).                    

    wlib = replace ( wlib, "  "," ").        

    assign zone = zone 
         + trim ( string ( wlib , "x(32)" ))                          + ";"
         + trim ( auxapr.codaux )                                          + ";"  
         + " "                                                             + ";"   /* forme juridique non GERE SUR DEAL */
         + "EXP"                                                           + ";" 
         + trim ( auxili.siret )                                           + ";"
         + trim ( auxili.adres[2] )                                        + "\\n"
         + trim ( auxili.adres[3] )                                        + "\\n"
         + trim ( auxili.adres[4] )                                        + ";"
         + substr ( auxili.adres[5] ,1,5)                                  + ";" 
         + string ( int ( substr( auxapr.onic , 1 , 6) ), "99999")         + ";" .
         
   
    /* telephone */
    wlib = trim( auxapr.teleph ).
    wlib = replace (wlib," ","").
    
    assign zone = zone                                           
         + wlib                                                       + ";"  .


    /* portable */
    wlib = trim ( auxapr.telex ).
    wlib = replace (wlib," ","").
    
    assign zone = zone                                           
         + wlib                                                       + ";"  .

    /* fax */
    wlib = trim ( auxapr.fax ).
    wlib = replace (wlib," ","").
    assign zone = zone                                           
         + wlib                                                       + ";"  .

    /* mail */          
    wlib = trim ( substr ( auxapr.alpha-5 , 1 , 50)).
    if index ( wlib, "@") = 0 then wlib = "".                                         /* si pas d'@ : ce nest pas une adresse mail */
    
    /* si pas de mail + soc = 01, on force l'adresse, pour recuperer les preco terra conceil dans le portail ADH */
    if auxapr.codsoc = "01" and wlib = "" then wlib = "adherent@eureacoop-adh.fr".    

    assign zone = zone                                           
                + wlib                                                             + ";"              /* mail */
                + string( today ,"99/99/9999") + " " + string ( heure-debut , "hh:mm:ss" )  + ";"              /* date mise a jour + heure */
                + "31/12/2050" .
   
    put stream str1 unformatted zone skip .
    assign zone = ""
           nbexp = nbexp + 1.


    /* LIENS EXPLOITATIONS <=> TC AGREO */    
    /* envoi des TC actuels */
    woldtc = "".   
    do i-agreo2 = 1 to 3:

      /* Uniquement si le TC fait partie des TC AGREO */ 
      if lookup( auxapr.repres [i-agreo2],l-tcagreo) <> 0 then do:

        if woldtc = "" then woldtc = auxapr.repres [i-agreo2].
        else 
          if woldtc = auxapr.repres [i-agreo2]  then next.
   
        zone = trim( auxapr.repres [i-agreo2] )                                + ";" 
             + trim ( auxapr.codaux )                                          + ";"
             + "01/01/2012"                                                    + ";"
             + "31/12/2050"                                                    + ";"
             + string( today ,"99/99/9999")                                    + ";"         
             + "" .
        put stream str2 unformatted zone skip .
        zone = "".   
        nbexptie = nbexptie + 1.
      end.
    end.      

    /* LIENS EXPLOITATIONS <=> TC WEB */    
    /* envoi des TC actuels */
    if codsoc-trait[nsoc] = "01" then do:
      woldtc = "".   
      do i-web2 = 1 to 3:

        /* Uniquement si le TC fait partie des TC WEB */ 
        if lookup(auxapr.repres[i-web2],l-tcweb) <> 0 
           and lookup(auxapr.repres[i-web2],l-tcagreo) = 0   /* non AGREO car si AGREO le lien est deja redescendu */
        then do:

          if woldtc = "" then woldtc = auxapr.repres [i-web2].
          else  if woldtc = auxapr.repres [i-web2]  then next .
    
          zone = trim( auxapr.repres [i-web2] )                                  + ";" 
               + trim ( auxapr.codaux )                                          + ";"
               + "01/01/2012"                                                    + ";"
               + "31/12/2050"                                                    + ";"
               + string( today ,"99/99/9999")                                    + ";"         
                + "" .
         
          put stream str2 unformatted zone skip .
          zone = "".
          nbexptie = nbexptie + 1.
    
        end.
      end.      
    end.
