/* Verification des stocks apres bascule INVENTAIRE */
def var wsoc as char initial "01".
def var wstoinv    as dec decimals 3 format ">>>>>>,>>9.999-".
def var wstoarr    as dec decimals 3 format ">>>>>>,>>9.999-".
def var wstomag    as dec decimals 3 format ">>>>>>,>>9.999-".
def var dif1       as dec decimals 3 format ">>>>>>,>>9.999-".
def var dif2       as dec decimals 3 format ">>>>>>,>>9.999-".
def var cdif1      as dec decimals 3 format ">>>>>>,>>9.999-".
def var cdif2      as dec decimals 3 format ">>>>>>,>>9.999-".
def var wstoinvcum as dec decimals 3 format ">>>>>>,>>9.999-".
def var nb1     as int.
def var nb2     as int.
def var nb3     as int.
def buffer b-stocks for stocks.

def var numpas  as int.
def var numpasx as char format "x".
def var exe     as char format "xxxx".
def var wdatinv as date.
def var wzone   as char format "x(18)".

wdatinv = date (06,30,YEAR( TODAY )).

exe = STRING( YEAR( TODAY ) , "9999").

update wsoc.
 
update numpas.

numpasx = string(numpas, "9").


def stream sortie1.
wzone = "verifsto1." + numpasx + "-" + wsoc + ".txt".
output stream sortie1 to value(wzone).

def stream sortie2.
wzone = "verifsto2." + numpasx + "-" + wsoc + ".txt".
output stream sortie2 to value(wzone).

def stream sortie3.
wzone = "verifsto3." + numpasx + "-" + wsoc + ".txt".
output stream sortie3 to value(wzone).

message "Prog ==> VERIFSTOC.P" wsoc "-" exe "-" wdatinv.

pause.

for each
artapr where artapr.codsoc = wsoc 
no-lock:

wstoinvcum = 0.

for each magasi where 
(magasi.codsoc = "" and magasi.societe = wsoc)
or (magasi.codsoc = ""  and magasi.codtab = "005")
or (magasi.codsoc = ""  and magasi.codtab = "048") 
no-lock:


wstoinv = 0. 
FIND STOINV where stoinv.codsoc      = wsoc
and   stoinv.articl      = artapr.articl
and   stoinv.magasin     = magasi.codtab
and   stoinv.date-invent = wdatinv
and   stoinv.evenement   = "INV"
no-lock no-error .
if available stoinv then do: nb1 = nb1 + 1. 
                             wstoinv = stoinv.qte-invent.
                         end.
wstomag = 0.

find b-stocks where 
     b-stocks.codsoc = wsoc and
     b-stocks.articl = artapr.articl and
     b-stocks.magasin = magasi.codtab and
     b-stocks.evenement = "stoarr" and
     b-stocks.exercice = exe
no-lock no-error.
if available b-stocks then do: nb2 = nb2 + 1.
                               wstomag = b-stocks.qte[6].
                           end.
dif1 = wstoinv - wstomag.                                                     
cdif1 = cdif1 + dif1.
if wstoinv <> wstomag
then do:
put stream sortie1 "Mag " wsoc " " artapr.articl " " 
    magasi.codtab " " wstoinv " " wstomag " " dif1 " tenusto=" tenusto 
    " " artapr.libart1[1] " " artapr.code-blocage skip.
export stream sortie2 delimiter ";" 
       wsoc
       artapr.articl 
       magasi.codtab
       wstoinv
       wstomag 
       dif1.
end.

wstoinvcum = wstoinvcum + wstoinv.

end.

wstoarr = 0.

find b-stocks where 
     b-stocks.codsoc = wsoc and
     b-stocks.articl = artapr.articl and
     b-stocks.magasin = "" and
     b-stocks.evenement = "stoarr" and
     b-stocks.exercice = exe
no-lock no-error.
if available b-stocks then do:
                              nb3 = nb3 + 1.
                              wstoarr = b-stocks.qte[6].
                           end.   
dif2 = wstoinvcum - wstoarr.
cdif2 = cdif2 + dif2.
if wstoinvcum <> wstoarr
then do:
put stream sortie1 
    "Cum " wsoc " " artapr.articl "     " wstoinvcum " " wstoarr
    " " dif2 skip.
export stream sortie3 delimiter ";" 
       wsoc
       artapr.articl 
       wstoinvcum
       wstoarr
       dif2.
end.

end.
put stream sortie3  "Lus 1 " nb1 " Lus 2 " nb2 " Lus 3 " nb3 " " skip.
put stream sortie3  "Fin " cdif1 " " cdif2 skip.


