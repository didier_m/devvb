/* DGR 180219 : remise exceptionnelle                                        */
/* CCO 09072019 : ajout {tarcli.i}                                           */
/* DGR 17102019 : correction RPD */

            if wsegmen <> "" and  artapr.caract[3] <> wsegmen  then next.
            if wartfermes = "N" and artapr.code-blocage = "F"  then next.
                                                            
            find artbis
            where artbis.codsoc = artapr.codsoc
            and   artbis.motcle = "VTE"
            and   artbis.articl = artapr.articl
            and   artbis.typaux = ""
            and   artbis.codaux = ""
            no-lock no-error.

            find tabges 
            where tabges.codsoc = artapr.codsoc
            and   tabges.etabli = " " 
            and   tabges.typtab = "ART" 
            and   tabges.prefix = "suit-fiche"
            and   tabges.codtab = artapr.articl
            no-lock no-error.
            if available tabges then
                assign woldart [1] = tabges.libel2[1]
                       woldart [2] = tabges.libel2[8]
                       wdeee       = tabges.nombre[15]
                       wecom       = tabges.nombre[24]
                       wstodang    = tabges.libel2[11]
                       wmaglog [1] = tabges.libel2[20]
                       wmaglog [2] = tabges.libel2[17]
                       wamm        = trim(tabges.libel2[18])
                       wcoefrpd    = tabges.nombre[16] 
                       wcoefstat   = tabges.nombre[9] 
                       weaj     = (if tabges.libel2[16] = "0" 
                                   then "N"      
                                   else "O")
                       wtoler   = tabges.nombre [22] 
                       wemplac  = tabges.libel3[1] 
                       wrem     = tabges.libel3 [2].
            else
                assign woldart [1] = ""
                       woldart [2] = ""
                       wdeee       = 0
                       wecom       = 0
                       wmaglog [1] = ""
                       wmaglog [2] = ""
                       wamm = ""
                       wcoefrpd = ?
                       wcoefstat = ?
                       weaj = "?"
                       wstodang = ""
                       wtoler   = 0
                       wemplac = ""
                       wrem = "".

            if wrem = "1"  then wrem = wrem + " = non".
            else wrem = wrem + " = oui".
            /* recuperation des taxes eventuelles */ 
            assign wcodtax = ""
                   wlibtax = ""
                   wdebtax = ?  
                   wvaltax = ?.

            assign wrpd3 = ? wrpd5 = ? wdaterpd3 = ? wdaterpd5 = ?.
            do itax = 1 to num-entries (l-codtax):
                wtax = entry(itax,l-codtax) .
    
                wchrono = 0.    
                if wtax = "RPD" then wchrono = 3.

                find first {tarcli.i} 
                where {tarcli.i}.codsoc = artapr.codsoc
                and   {tarcli.i}.motcle = "VTE"
                and   {tarcli.i}.typaux = "TAX"
                and   {tarcli.i}.codaux = ""
                and   {tarcli.i}.articl = artapr.articl
                and   {tarcli.i}.date-debut <= today
                and   {tarcli.i}.code-tarif = wtax
                and   {tarcli.i}.chrono = wchrono
                no-lock no-error.
                if available {tarcli.i} then do:
                    if wtax = "RPD" then do:
                        assign wrpd3     = {tarcli.i}.valeur / 1000
                               wdaterpd3 = {tarcli.i}.date-debut.
    
                        find first b-tarcli 
                        where b-tarcli.codsoc = artapr.codsoc
                        and   b-tarcli.motcle = "VTE"
                        and   b-tarcli.typaux = "TAX"
                        and   b-tarcli.codaux = ""
                        and   b-tarcli.articl = {tarcli.i}.articl
                        and   b-tarcli.date-debut = {tarcli.i}.date-debut
                        and   b-tarcli.code-tarif = {tarcli.i}.code-tarif
                        and   b-tarcli.chrono = 5
                        no-lock no-error.
                        if available b-tarcli then  
                            assign wrpd5 = b-tarcli.valeur / 1000
                                   wdaterpd5 = b-tarcli.date-debut.
                    end.
                    else  
                        assign wcodtax [itax] = entry(itax,l-codtax)
                               wlibtax [itax] = entry(itax,l-libtax)
                               wvaltax [itax] = {tarcli.i}.valeur / 1000
                               wdebtax [itax] = {tarcli.i}.date-debut.
                end.
            end.  
            
            /* recup tarif CLI//HTC  */
            if artapr.codsoc <> "" then do:
                assign wcodtar = ""
                       wvaltar = ?
                       wdattar = ?.
            
                do i = 1 to 2:
                    if i = 1 then wcodtar [i] = "CLI".
                    if i = 2 then wcodtar [i] = "HTC".
               
               
                    find first {tarcli.i} 
                    where {tarcli.i}.codsoc = artapr.codsoc
                    and   {tarcli.i}.motcle = "VTE"
                    and   {tarcli.i}.typaux = ""
                    and   {tarcli.i}.codaux = ""
                    and   {tarcli.i}.articl = artapr.articl
                    and   {tarcli.i}.date-debut <= today
                    and   {tarcli.i}.code-tarif = wcodtar[i]
                    no-lock no-error.
                    if available {tarcli.i} then do:
                        assign wvaltar[i] = {tarcli.i}.valeur 
                            wdattar[i] = {tarcli.i}.date-debut.
        
                    end.
                end .
            end .

            /* recup des infos de dangerosité  */ 
            /* DGR 26022016 info dans ARTICS......
            wclasse = "".
            if artapr.code-toxic <> "" then do:
                find tabges
                where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "art"
                and   tabges.prefix = "toxicite"
                and   tabges.codtab = artapr.code-toxic
                no-lock no-error.
                if available tabges then
                    wclasse = tabges.libel3[3].
                else
                    wclasse = "Code toxicite inconnu".
            end.
            */ 
            assign  wclasse = ""
                    wonu    = ""
                    wsymb   = ""
                    wicpe   = ""
                    wetaphy = ""
                    wdatfds = ?.
            
            find artics use-index theme
            where artics.codsoc = artapr.codsoc
            and   artics.theme = "DANGER"
            and   artics.alpha-cle = ""
            and   artics.articl = artapr.articl
            and   artics.chrono = 0
            no-lock no-error.
            if available artics then do:
                assign wclasse      = artics.alpha [ 1 ]
                       wonu         = artics.alpha [ 3 ] 
                       wsymb [ 1 ]  = artics.alpha [ 14 ]
                       wsymb [ 2 ]  = artics.alpha [ 15 ]
                       wsymb [ 3 ]  = artics.alpha [ 16 ]
                       wsymb [ 4 ]  = artics.alpha [ 17 ]
                       wsymb [ 5 ]  = artics.alpha [ 18 ] 
                       wetaphy      = artics.alpha [ 19 ] 
                       .
                
                if trim ( artics.alpha [1] ) <> "" then do:
                    FIND tabges WHERE tabges.codsoc = ""
                    AND tabges.etabli = ""
                    AND tabges.typtab = "DGR"
                    AND tabges.prefix = "CLASSE"
                    AND tabges.codtab = artics.alpha[1]
                    NO-LOCK NO-ERROR.
                    IF AVAILABLE tabges THEN wclasse = wclasse + "-" + tabges.libel1[ 1 ] .
                end.
                
                if trim ( artics.alpha [3] ) <> "" then do:
                    FIND tabges WHERE tabges.codsoc = ""
                    AND tabges.etabli = ""
                    AND tabges.typtab = "DGR"
                    AND tabges.prefix = "NUMONU"
                    AND tabges.codtab = artics.alpha[3]
                    NO-LOCK NO-ERROR.
                    IF AVAILABLE tabges THEN wonu = wonu + "-" + tabges.libel1[ 1 ] .
                end.
                
                do i = 1 to 5 :
                    if trim ( wsymb [i] ) = "" then next.
                    FIND tabges WHERE tabges.codsoc = ""
                    AND tabges.etabli = ""
                    AND tabges.typtab = "DGR"
                    AND tabges.prefix = "SYMB"
                    AND tabges.codtab = artics.alpha[ i + 13 ]
                    NO-LOCK NO-ERROR.
                   IF AVAILABLE tabges THEN wsymb [ i ] =  wsymb [ i ]  + "-" +  tabges.libel1[ 1 ] .
                end.
            end.
            
            find artics use-index theme
            where artics.codsoc = artapr.codsoc
            and   artics.theme = "DANGER"
            and   artics.alpha-cle = ""
            and   artics.articl = artapr.articl
            and   artics.chrono = 1
            no-lock no-error.
            if available artics then do:
                assign wicpe =  artics.alpha [ 19 ] .
                wdatfds      = artics.datte[1].
                if trim ( artics.alpha [19] ) <> "" then do:

                    FIND tabges WHERE tabges.codsoc = ""
                    AND tabges.etabli = ""
                    AND tabges.typtab = "DGR"
                    AND tabges.prefix = "INSCLASSE"
                    AND tabges.codtab = artics.alpha[19]
                    NO-LOCK NO-ERROR.
                    IF AVAILABLE tabges THEN wicpe = wicpe + "-" + tabges.libel1[ 1 ].
                end.
            end.
            
            if topfds = "b" and wdatfds = ? then next.
            if topfds = "c" and wdatfds <> ? then next.
            
    
            /* harmonisation code-blocage " " et "o" */ 
            if artapr.code-blocage = ""  or
               artapr.code-blocage = "O"
            then wblo = "O" .
            else wblo = artapr.code-blocage.

            /* recuperation des ventes si wventes = "O" */
            assign wqten[1] = 0 wcan[1] = 0 
                   wqten[2] = 0 wcan[2] = 0     .
        
        
            if wventes = "O" then do:
                if wanneedeb[1] <> 0 then do k = 1 to 2 :
                    for each starti
                    where starti.codsoc = artapr.codsoc
                    and   starti.motcle = "vte"       
                    and   starti.articl = artapr.articl
                    and   starti.annee  >= string(wanneedeb[k])
                    and   starti.annee  <= string(wanneefin[k])
                    no-lock:
                        if wanneedeb[k] = wanneefin[k] then do:
                            do i = wmoisdeb[1] to wmoisfin[1] :
                                assign wqten[k] = wqten[k] + starti.qte[i]
                                       wcan[k]  = wcan[k]  + starti.fac-ht[i]      .
                            end.    
                        end.
                        else do:
                            if starti.annee = string(wanneedeb[k]) then do:
                                do i = wmoisdeb[1] to 12:
                                    assign wqten[k] = wqten[k] + starti.qte[i]
                                           wcan[k]  = wcan[k]  + starti.fac-ht[i]      .
                                end.
                            end.
                            if starti.annee = string(wanneefin[k]) then do:
                                do i = 1 to wmoisfin[1]:
                                    assign wqten[k] = wqten[k] + starti.qte[i]
                                           wcan[k]  = wcan[k]  + starti.fac-ht[i]      .
                                end.
                            end.
                        end.
                    end.
                end.
            end.

            /* recuperation des gencod (3max) de l'article */ 
            assign wgen1 = ""     wgen2 = ""       wgen3 = "".
            for each codbar 
            where  codbar.codsoc = artapr.codsoc 
            and    codbar.articl = artapr.articl    
            no-lock by codbar.datmaj desc:
                if wgen1 = ""  then wgen1 = codbar.code-barre.
                else if wgen2 = "" then wgen2 = codbar.code-barre.
                     else if wgen3 = "" then wgen3 = codbar.code-barre. 
            end.
        
            /* recup commentaire fiche groupe */
            wcommgrp = "".
            FIND PROGLIB
            where proglib.codsoc = artapr.codsoc
            and   proglib.motcle = ""
            and   proglib.etabli = ""
            and   proglib.edition = ""
            and   proglib.chrono  = 0
            and   proglib.clef    = "ARTI-COM" + artapr.articl
            no-lock  no-error .
            if available proglib then 
				assign 	wcommgrp [1] = proglib.libelle[ 1 ]
						wcommgrp [2] = proglib.libelle[ 2 ]
						wcommgrp [3] = proglib.libelle[ 3 ].
          
            /* recup commentaire fiche Societe  */
            wcommsoc = "".
            FIND PROGLIB
            where proglib.codsoc = artapr.codsoc
            and   proglib.motcle = ""
            and   proglib.etabli = ""
            and   proglib.edition = ""
            and    proglib.chrono  = 0
            and   proglib.clef    = "ARTI-COS" + artapr.articl
            no-lock  no-error .
            if available proglib then 
				assign 	wcommsoc [1] = proglib.libelle[ 1 ]
						wcommsoc [2] = proglib.libelle[ 2 ]
						wcommsoc [3] = proglib.libelle[ 3 ].

            /* 21112016 : ajout PAN */
            wpan = "".
            find artics
            where artics.codsoc = artapr.codsoc
            and   artics.articl = artapr.articl
            and   artics.theme = "PAN"
            and   artics.chrono = 0
            no-lock no-error.
            if available artics then wpan = artics.alpha-cle.
                

            
            /* ecriture fichier de sortie */
            export stream maquette delimiter ";" 
                artapr.articl      artapr.codsoc          artapr.libart1[1]          artapr.libart
                artapr.famart      artapr.soufam          artapr.sssfam              woldart
                artapr.typart      artapr.tva
                (if codsoc-soc = "00" then ""  
                                      else artapr.provenance)
                wblo               
                artapr.bloque 
                artapr.type-condit     
                artapr.usto 
                artapr.qte-lin /* unite / palette */ 
                artapr.pds                          
                artapr.articl-usine    
                wgen1 
                artapr.tenulot     artapr.caract[07]      artapr.caract[08]          
                artapr.caract[09]  artapr.caract[10]      wtoler
                artapr.emplac wemplac
                artbis.type-surcondit 
                
                (if available artbis then artbis.ufac
                                     else "????" )
                (if available artbis then artbis.coef-fac
                                     else ? )
                (if available artbis then artbis.surcondit
                                     else ?) 
                artapr.edit-tarif  artapr.bareme          artapr.code-cumul          artapr.edi-etiq
                artapr.typologie   wpan
                artapr.prx-modif   artapr.pcb
                artapr.volume      artapr.tenusto             
                /* artapr.code-toxic              wclasse            artapr.va     */
                artapr.typaux-ref[2] + artapr.codaux-ref[2]
                artapr.typaux-ref[1] + artapr.codaux-ref[1]
                artapr.articl-lie  artapr.stat-rayon      artapr.alpha-1             artapr.caract[01]
                artapr.libart2[1]  artapr.datcre              artapr.datmaj
                artapr.caract[03]  artapr.caract[05]      artapr.caract[06]          wmaglog
                wdeee              artapr.date-tarif[1]   wrpd3                      wdaterpd3
                wrpd5              wdaterpd5              wamm                       wcoefrpd 
                weaj               
                wclasse             wonu                   wsymb                     
                wicpe 				wetaphy
                wcan[1]                wcan[2]                    wqten[1] 
                wqten[2]           artapr.type-mag        wstodang                   artapr.famart2
                artapr.soufam2     artapr.sssfam2                               wgen2 
                wgen3              wcoefstat              artapr.unite-hectare       artapr.type-client
                artapr.impvte      artapr.impach          wcodtax [1]                wlibtax [1] 
                wvaltax [1]        wdebtax [1]            wcodtax [2]                wlibtax [2] 
                wvaltax [2]        wdebtax [2]            wcodtax [3]                wlibtax [3] 
                wvaltax [3]        wdebtax [3]            wcodtax [4]                wlibtax [4] 
                wvaltax [4]        wdebtax [4]            wcodtax [5]                wlibtax [5] 
                wvaltax [5]        wdebtax [5]            artapr.code-texte  
                wcommgrp[1]        wcommsoc [1]
                wecom
                wcodtar [1]        wdattar [1 ]           wvaltar [1]
                wcodtar [2]        wdattar [2 ]           wvaltar [2]
                wcommgrp[2]        wcommgrp[3]        
                wcommsoc[2] 		wcommsoc [3]
                wdatfds
                wrem
				.
  