/*==========================================================================*/
/*                           E X T C Y L - E . I                            */
/* Extraction des donnees pour Cylande                                      */
/*--------------------------------------------------------------------------*/
/* interface ARTICLES ET CONDITIONNEMENTS                                   */
/*==========================================================================*/
/* DGR 25052018 : tst sur operateur de creation gencod pour que les gencods */ 
/*                grassot en 00000 ne soient pas cr��s en gencods princ     */
/* DGR 29052018 : TVA deplacee au niveau produit au lieu du niveau item     */ 
/* DGR 04062018 : ajout Reffab grassot + uppre de diverses zones            */
/* DGR 07062018 : envoi provenance 9 si provenance hors liste prov auto     */
/* DGR 14062018 : correction ucde/usto                                      */
/* DGR 29062018 : modif en cas de code-barre 'anormal'                      */
/* DGR 14082018 : avant des artciles avec suppressions de gencods en 1er    */
/* VLE 03102018 : gestion du statut au niveau societe et non groupe         */
/* DGR 02112018 : trim du code fournisseur dans unite logistique            */
/* DGR 19112018 : seuil split a 1                                           */
/* DGR 23112018 : mise en commentaire seuil split a 1                       */
/* DGR 27112018 : produit non eaj => non vendables                          */
/*                remis la RPD qui avait disparue                           */ 
/* DGR 29112018 : famille interdite en remise                               */
/*                articles gratuits                                         */
/* DGR 10012019 : statut WINSTORE stocke dans ARTICS, envoi uniquement des  */
/*                changements                                               */
/* CCO 15072019 : ajout {tarcli.i}                                          */

define buffer ARTBIS-ACH for artbis.
define buffer ARTAPR-SOC for artapr.
define buffer ARTICS-MAJ for artics.
def stream ficlog.
def stream ficsor.


def var nbgencodp    as log   no-undo.
                              
/* def var wmag         as char  no-undo. */
def var wamm         as char  no-undo.
def var wemplac-ie   as char  no-undo.
def var weaj         as char  no-undo.
def var wucde        as char  no-undo.
def var wusto        as char  no-undo.
def var wtyp-condit  as char  no-undo.
def var wpan         as char  no-undo.
def var zchar        as char  no-undo.
def var zz           as int   no-undo.
def var isup         as int   no-undo.
def var wsup         as char  no-undo.


/* Debut VLE 03102018 */
def var l-statut-mag as char no-undo.
def var istatut as int no-undo.
/* Fin VLE 03102018 */

/* articles */
CREATE SAX-WRITER hSAXWriter.
hSAXWriter:ENCODING = "UTF-8".
hSAXWriter:FORMATTED = false .  /* 	si TRUE: fichier de sortie lisible mais ajout de blancs 
									a la place des sauts de lignes a l'integration dans UR */

lOK = hSAXWriter:SET-OUTPUT-DESTINATION ( "file", wficsortie ).
lOK = hSAXWriter:START-DOCUMENT ( ).
lOK = hSAXWriter:WRITE-COMMENT ( "EXTRACTION DES ARTICLES" ) .
lOK = hSAXWriter:START-ELEMENT ( "productListType" ).

/* DGR 14022019 : on n'envoie plus les unit�s logistiques 

CREATE SAX-WRITER hSAXWriter2.
hSAXWriter2:ENCODING = "UTF-8".
hSAXWriter2:FORMATTED = false.  /* 	si TRUE: fichier de sortie lisible mais ajout de blancs a la place des sauts de lignes a l'integration dans UR */


lOK = hSAXWriter2:SET-OUTPUT-DESTINATION ( "file", wficsortie2 ).
lOK = hSAXWriter2:START-DOCUMENT ( ).
lOK = hSAXWriter2:WRITE-COMMENT ( "EXTRACTION DES conditionnements" ) .
lOK = hSAXWriter2:START-ELEMENT ( "logisticUnitListType" ).

*/

do isup = 1 to 2 :
   if isup = 1 then wsup = "OUI".
   else wsup = "".

   for each t-art use-index i-sup
   where t-art.supgencod = wsup
   no-lock:

      find artapr
      where rowid ( artapr ) = t-art.rowart
      no-lock no-error.
      if not available artapr then next.
  
      assign   wucde       = ""
               wamm        = ""
               wemplac-ie  = ""
               weaj        = ""
               wpan        = ""
               wdomaine    = ""
               wrayon      = ""
               wnature     = ""
               wrpd = 0.

   
      nblus = nblus + 1.
      
      /* dgr 29/11/2018 familles agricoles interdites de remise .... */
      if lookup ( artapr.famart2, l-famnorem )  <> 0 then wnature = "1" .
   
      /* if nblus modulo 1000 = 0 then disp nblus string (time,"hh:mm:ss").* */
      /* disp nblus artapr.articl artapr.libart1 [1] . pause (0). */
      /* chargement parametre GENERAL CYLANDE */
   
   
      find artbis 
      where artbis.codsoc = artapr.codsoc
      and artbis.motcle = "VTE"
      and artbis.typaux = ""
      and artbis.codaux = ""
      and artbis.articl = artapr.articl
      no-lock no-error.
      if not available artbis then next.
 
      wusto  = upper ( trim ( artapr.usto ) ).   
      if wusto = "" then wusto = "UN".
      wtyp-condit = artapr.type-condit .
      if wtyp-condit = "" then wtyp-condit = "UN".
      
      assign 	codsoc-soc = ""
               typtab = "EUR"
               prefix = "CORUNCYL"
               codtab = wtyp-condit + wusto
               xx-param = "" .
    
      run ch-paray.p( output xx-param ) .
      if xx-param = ""  then wuvte = "UN" .
      else  assign  wuvte = entry	( 1,  xx-param, "|" ) .        /* correspondance unit� UR  */
      
      /*  dgr 03032019 : si article ferm� en grp envoi etat ferme sur chaque soci�t� */
      if artapr.code-blocage = "F" then do:
         find t-codblo 
         where t-codblo.codsoc = artapr.codsoc
         and   t-codblo.articl = artapr.articl
         no-error.
         if not available t-codblo then do:
            create t-codblo.
            assign t-codblo.codsoc = artapr.codsoc
                   t-codblo.articl = artapr.articl
                   t-codblo.etat   = "FED".
         end.
     
      end.
      
      wecom = 0.
      wd3e = 0.
      find tabges 
      where tabges.codsoc = artapr.codsoc
      and   tabges.etabli = ""
      and   tabges.typtab = "ART"
      and   tabges.prefix = "SUIT-FICHE"
      and   tabges.codtab = artapr.articl
      no-lock no-error.
      if available tabges then do :
         wamm = tabges.libel2[18].
         wemplac-ie = tabges.libel3[1].
         weaj = tabges.libel2[16].
         if tabges.libel3[2] = "1" then wnature = "1". /* non remisable */
         if tabges.nombre[24] <> 0 and tabges.nombre[24] <> ?  then do:
            wecom = tabges.nombre[24] no-error.
            if error-status:error or wecom = ? then wecom = 0.
         end.
         if tabges.nombre[15] <> 0 and tabges.nombre[15] <> ?  then do:
            wd3e = tabges.nombre[15] no-error.
            if error-status:error or wd3e = ? then wd3e = 0.
         end.
      end.
      
      /* si le taux de remise de l'article est a 0 alors article non remisable */
      if wnature <> "1" then do:
         find t-tarach
         where t-tarach.articl = artapr.articl
         no-lock no-error.
         if available t-tarach and  t-tarach.taux = 0 then wnature = "1".
      end.

      /* recherche RPD */
      wrpd = 0.
      rech-rpd:
      do ii-soc = 1 to num-entries( l-socdeal ) :
         find first {tarcli.i}
         where {tarcli.i}.codsoc = entry( ii-soc, l-socdeal )
         and   {tarcli.i}.motcle = "VTE"
         and   {tarcli.i}.typaux = "TAX"
         and   {tarcli.i}.codaux = ""
         and   {tarcli.i}.code-tarif = "RPD"
         and   {tarcli.i}.articl = artapr.articl
         and   {tarcli.i}.date-debut <= today
         and   {tarcli.i}.chrono = 3
         no-lock no-error.
         if available {tarcli.i} then do:
            wrpd = {tarcli.i}.valeur / 1000 no-error. 
            if error-status:error or wrpd = ? then wrpd = 0.
            if wrpd <> 0 then leave rech-rpd.
         end.   
      end.
      find artics 
      where artics.codsoc = artapr.codsoc
      and   artics.articl = artapr.articl
      and   artics.theme  = "PAN"
      and   artics.chrono = 0
      no-lock no-error .
      if available artics
         then wpan = artics.alpha-cle.
      
      assign wpattern = "" wtype = "".
      if lookup(artapr.articl, l-art-poubelle) <> 0 then
         assign  wpattern = "SGPAI" wtype = "INCO".
      else
         assign  wpattern = "SGPPF" wtype = "PRDF".

      assign wetat = ""
             wcodblo = "" 
             wstatut = "".
          
      case artapr.code-blocage:
         when "I" then  assign wetat = "IDA"  wcodblo = upper(artapr.code-blocage)  wstatut = "4".
         when "B" then  assign wetat = "IDV"  wcodblo = upper(artapr.code-blocage)  wstatut = "7".
         when "F" then  assign wetat = "FED"  wcodblo = upper(artapr.code-blocage)  wstatut = "8".
         otherwise      assign wetat = "OUV"  wcodblo = "O"                         wstatut = "1".
      end.
   
      lOK = hSAXWriter:START-ELEMENT ( "values" ).
   
      /* Produit */
      assign 
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", upper (TRIM(artapr.articl)))
         lOK = hSAXWriter:START-ELEMENT ( "shortDescription" )
            lOK = hSAXWriter:WRITE-CDATA ( upper ( TRIM(artapr.libart)) )
         lOK = hSAXWriter:END-ELEMENT ( "shortDescription" )
         lOK = hSAXWriter:START-ELEMENT ( "description" )
            lOK = hSAXWriter:WRITE-CDATA ( upper (TRIM(artapr.libart1[1] ) ))
         lOK = hSAXWriter:END-ELEMENT ( "description" )
      
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "pattern", wpattern )
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "category", "SGP" )
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", wtype )
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "referencing", "TDRE" )
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "state", wetat )
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "stateLocal", wetat ) 
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "salesUnit", wuvte  ) 
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "purchaseUnit", wuvte  ) 
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "inventoryUnit", wuvte )
      .
      
      /* articles */
      assign
         lOK = hSAXWriter:START-ELEMENT ( "items")
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", trim ( artapr.articl ))
            lOK = hSAXWriter:START-ELEMENT ( "posDescriptions")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "language", "FR" )
               lOK = hSAXWriter:START-ELEMENT ( "shortDescription")
                  lOK = hSAXWriter:WRITE-CDATA ( TRIM(artapr.libart1[1] ))
               lOK = hSAXWriter:END-ELEMENT ( "shortDescription")
               lOK = hSAXWriter:START-ELEMENT ( "description")
                  lOK = hSAXWriter:WRITE-CDATA ( TRIM(artapr.libart1[1] ))
               lOK = hSAXWriter:END-ELEMENT ( "description")
            lOK = hSAXWriter:END-ELEMENT ( "posDescriptions")
         
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "state", wetat )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "stateLocal", wetat ) .

         if artapr.prof <> 0 then do :
            assign 
               lOK = hSAXWriter:START-ELEMENT ( "height")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "unit", "ME" )
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "value", string(artapr.prof) )
               lOK = hSAXWriter:END-ELEMENT ( "height").
         end.
      
         if artapr.long <> 0 then do :
            assign 
               lOK = hSAXWriter:START-ELEMENT ( "length")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "unit", "ME" )
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "value", string(artapr.long) )
               lOK = hSAXWriter:END-ELEMENT ( "length").
         end.

         if artapr.larg <> 0 then do :
            assign 
               lOK = hSAXWriter:START-ELEMENT ( "width")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "unit", "ME" )
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "value", string(artapr.larg) )
               lOK = hSAXWriter:END-ELEMENT ( "width").
      
         end.
         if artapr.pds <> 0 then do :
            assign 
               lOK = hSAXWriter:START-ELEMENT ( "netWeight")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "unit", "KG" )
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "value", string(artapr.pds) )
               lOK = hSAXWriter:END-ELEMENT ( "netWeight").
         end.
         if artapr.volume <> 0 then do :
         /* Calcul prix au kg/l */            
            assign 
               lOK = hSAXWriter:START-ELEMENT ( "volume")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "unit", "LIT" )
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "value", string(artapr.volume) )
               lOK = hSAXWriter:END-ELEMENT ( "volume").
         end.
         
         if t-art.l-gencodsup <> "" then do:
            do i = 1 to num-entries ( t-art.l-gencodsup ):
               wcode = entry ( i , t-art.l-gencodsup ).
               if wcode = "" then next.
               if wcode = ? then next.
               assign 
                  lOK = hSAXWriter:START-ELEMENT ( "barcodes")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", "EANP" )
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "ean", wcode )
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "remove", "true" )
                  lOK = hSAXWriter:END-ELEMENT ( "barcodes")
                  lOK = hSAXWriter:START-ELEMENT ( "barcodes")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", "EANS" )
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "ean", wcode )
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "remove", "true" )
                  lOK = hSAXWriter:END-ELEMENT ( "barcodes").
        
            end.
         end.
         /* code barre  */
         nbgencodp = true.
         for each codbar use-index articl
         where codbar.codsoc = artapr.codsoc
         and codbar.articl = artapr.articl
         no-lock 
         by codbar.datcre:
            /* les gencod cr�� par un operateur particulier ne peuvent pas �tre affect�s en principal => gencod GRASSOT commen�ant par 00000 */ 
            if codbar.code-barre = "" then next.
            if codbar.code-barre = ? then next.
            if nbgencodp = true and ( x-opegencod = "" or ( x-opegencod <> "" and lookup( codbar.opecre, x-opegencod ) = 0 ) ) then do :
               assign 
                  lOK = hSAXWriter:START-ELEMENT ( "barcodes")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", "EANP" )
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "ean", trim(codbar.code-barre)  )
                  lOK = hSAXWriter:END-ELEMENT ( "barcodes").
               nbgencodp = false.
            end.
            else assign 
               lOK = hSAXWriter:START-ELEMENT ( "barcodes")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", "EANS" )
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "ean", trim(codbar.code-barre)  )
               lOK = hSAXWriter:END-ELEMENT ( "barcodes").
         
         end.
      
         /* Attributs de gestion au niveau articles */
         /* RPD */
         if wrpd <> 0  then do :
            assign 
               lOK = hSAXWriter:START-ELEMENT ( "datas")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "RPD"  )
                  lOK = hSAXWriter:START-ELEMENT ( "data")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "stringData", string(wrpd) )
                  lOK = hSAXWriter:END-ELEMENT ( "data")
               lOK = hSAXWriter:END-ELEMENT ( "datas").
         end.

         if artapr.tenusto <> "1" then do : /* non tenu en stock */
            if wnature = "1" 
               then wnature = "3". /* non gere en stock et non remisable */
               else wnature = "2". /* non g�r� en stock */
         end. 
      
         if wnature <> "" then do :
            assign 
               lOK = hSAXWriter:START-ELEMENT ( "datas")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "NATURE" )
                  lOK = hSAXWriter:START-ELEMENT ( "data")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", wnature )
                  lOK = hSAXWriter:END-ELEMENT ( "data")
               lOK = hSAXWriter:END-ELEMENT ( "datas").
         end.
      
         zchar =  string( wd3e ) no-error.
         if error-status:error then zchar = "".
         assign 
            lOK = hSAXWriter:START-ELEMENT ( "datas")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "DEEE" )
               lOK = hSAXWriter:START-ELEMENT ( "data")
                  /* lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "stringData", string( wd3e ) ) */
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "stringData", zchar )
               lOK = hSAXWriter:END-ELEMENT ( "data")
            lOK = hSAXWriter:END-ELEMENT ( "datas").
   
         zchar = string ( wecom ) no-error.
         if error-status:error then zchar = "".
         assign 
            lOK = hSAXWriter:START-ELEMENT ( "datas")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "ECOMOB"  )
               lOK = hSAXWriter:START-ELEMENT ( "data")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "stringData", zchar )
               lOK = hSAXWriter:END-ELEMENT ( "data")
            lOK = hSAXWriter:END-ELEMENT ( "datas").
               
         /* Attributs de gestion au niveau articles */
         assign 
            lOK = hSAXWriter:START-ELEMENT ( "datas")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "CODBLO" )
               lOK = hSAXWriter:START-ELEMENT ( "data")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", wcodblo )
               lOK = hSAXWriter:END-ELEMENT ( "data")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "remove", "false" )
            lOK = hSAXWriter:END-ELEMENT ( "datas")
			.
			   
         /* tva niveau groupe */
         assign 
            lOK = hSAXWriter:START-ELEMENT ( "vatCode")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "businessUnit", "1" )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "vatCode", trim(artapr.tva))
            lOK = hSAXWriter:END-ELEMENT ( "vatCode").
   
         wrdd = "N" .
         wcode = "" .
         wcircuit = "".
         do ii-soc = 1 to num-entries( l-socdeal ) :
      
            find artapr-soc 
            where artapr-soc.codsoc = entry( ii-soc, l-socdeal )
            and   artapr-soc.articl = artapr.articl
            no-lock no-error.
            if not available artapr-soc then next.
         

            /* TVA � g�n�rer sur les toutes les bu 
            case artapr-soc.codsoc:
               when "01" then  wcode = "12,121,122".
               when "08" then  wcode = "13,131,132".
               when "59" then  wcode = "14,141".
               when "88" then  wcode = "11,111".
            end case. 
            */
            /* zz = lookup ( trim( artapr-soc.codsoc), l-socdeal ) . /*01,08,59,88 */ */
            wcircuit[ii-soc] = entry ( lookup ( trim( artapr-soc.codsoc), l-socdeal ) , l-circuit ) . /* DGR 04102018 */
            wrdd [ii-soc] = "O".
            
            if wcode = "" then  wcode = entry ( lookup ( trim( artapr-soc.codsoc), l-socdeal ), l-bu-s ) + "," + entry ( lookup ( trim( artapr-soc.codsoc), l-socdeal ) , l-bu-e , ";") .
            else wcode = wcode + "," + entry ( lookup ( trim( artapr-soc.codsoc), l-socdeal ), l-bu-s ) + "," + entry ( lookup ( trim( artapr-soc.codsoc), l-socdeal ) , l-bu-e , ";") .   
         
            assign   wetat = ""
                     wcodblo = "" 
                     wstatut = "" .
            case artapr-soc.code-blocage:
               when "I" then  assign wetat = "IDA" wcodblo = artapr-soc.code-blocage wstatut = "4".
               when "B" then  assign wetat = "IDV" wcodblo = artapr-soc.code-blocage wstatut = "7".
               when "F" then  assign wetat = "FED" wcodblo = artapr-soc.code-blocage  wstatut = "8".
               otherwise      assign wetat = "OUV" wcodblo = "O"  wstatut = "1".
            end.
         
            wcodblo  = upper ( trim ( wcodblo )) .
            wprov = upper ( trim ( artapr-soc.provenance ) ) .
            if lookup ( wprov , l-prov ) = 0 then wprov = "9".   /* DGR 07062018 */

            /* if artapr-soc.provenance = "5" then wprov = "9". */
            wzone = "" .
            case artapr-soc.codsoc :
               when "01" then wzone = "EC".
               when "08" then wzone = "ASEC".
               when "59" then wzone = "VIT".
               when "88" then wzone = "JG".
            end.
			
          
         
            /* Debut modif VLE le 03102018 */
            /*
            l-statut-mag = "" .
               case artapr-soc.codsoc :
                  when "01" then l-statut-mag = l-statut-mag-01.
                  when "08" then l-statut-mag = l-statut-mag-08.
                  when "59" then l-statut-mag = l-statut-mag-59.
                  when "88" then l-statut-mag = l-statut-mag-88.
               end.
            */
         

            /* On rend le statut non vendable sur les caisses dans certains cas :
               - produits non autoris�s jardin
               - articles r�serv�s aux professionnels
               - articles dont l'unit� est BX
               - articles de certaines familles */
            
            if (wstatut = "1" or wstatut = "4") and
               (weaj = "0"    or  artapr.type-client = "PRO" or  artapr.usto = "BX")   
            then wstatut = "7".
			
            find artics use-index primaire
            where artics.codsoc = artapr-soc.codsoc
            and   artics.articl = artapr-soc.articl
            and   artics.theme  = "STATUTWST"
            and   artics.chrono = 0
            no-lock no-error.
            if not available artics or 
               ( available artics and artics.alpha-cle <> wstatut  )
            then do:
               
               
               find t-statut 
               where t-statut.codsoc = artapr-soc.codsoc
               and   t-statut.articl = artapr-soc.articl
               no-error.
               if not available t-statut then do:
                  create t-statut.
                  assign t-statut.codsoc = artapr-soc.codsoc
                         t-statut.articl = artapr-soc.articl
                         t-statut.statut = wstatut.
               end.
               if type-trt = "E" then do:
                  if available artics then do:
                     
                     find ARTICS-MAJ
                     where rowid ( ARTICS-MAJ ) = rowid ( artics )
                     exclusive-lock no-wait no-error.
                     
                     if available ARTICS-MAJ then do:
                        
                        assign artics-maj.alpha-cle = wstatut
                               artics-maj.datmaj    = today
                               artics-maj.heumaj    = string( time, "hh:mm" )
                               artics-maj.opemaj    = "BCY".
                               
                        validate artics-maj.
                     end.
                  end.
                  else do:
                     create ARTICS-MAJ.
                     assign ARTICS-MAJ.codsoc = artapr-soc.codsoc
                            ARTICS-MAJ.articl = artapr-soc.articl
                            ARTICS-MAJ.theme  = "STATUTWST"
                            ARTICS-MAJ.chrono = 0
                            artics-maj.alpha-cle = wstatut
                            ARTICS-MAJ.datcre    = today
                            ARTICS-MAJ.heucre    = string( time, "hh:mm" )
                            ARTICS-MAJ.opecre    = "BCY"
                            ARTICS-MAJ.datmaj    = today
                            ARTICS-MAJ.heumaj    = string( time, "hh:mm" )
                            ARTICS-MAJ.opemaj    = "BCY".
                     validate artics-maj.
                  end.
               end.
            end.
            
            if wcodblo <> "" then do:
               /* code blocage */
               assign 
                  lOK = hSAXWriter:START-ELEMENT ( "datas")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "CODBLO" + wzone )
                     lOK = hSAXWriter:START-ELEMENT ( "data")
                        lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", wcodblo )
                     lOK = hSAXWriter:END-ELEMENT ( "data")
                  lOK = hSAXWriter:END-ELEMENT ( "datas").         
            end.
         
            if wprov <> "" then  do:
               /* provenance */
               assign 
                  lOK = hSAXWriter:START-ELEMENT ( "datas")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "PROVEN" + wzone )
                     lOK = hSAXWriter:START-ELEMENT ( "data")
                        lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", wprov )
                     lOK = hSAXWriter:END-ELEMENT ( "data")
                  lOK = hSAXWriter:END-ELEMENT ( "datas").

            end.
            
            
            wfournisseur = "880047".
            if entry( ii-soc, l-socdeal ) = soc-fullcyl then do:
               assign
                  lOK    = hSAXWriter:START-ELEMENT ( "mainSuppliers" )
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "priceType", "STE" )
                     lOK = hSAXWriter:START-ELEMENT ( "supplier")
                        lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "externalReference", wfournisseur )
                     lOK = hSAXWriter:END-ELEMENT ( "supplier")
                     /* lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "supplyChainCode", wcircuit[lookup ( entry ( k, l-soc ), l-socdeal )] ) */
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "supplyChainCode", wcircuit[ii-soc] )
                  lOK = hSAXWriter:END-ELEMENT ( "mainSuppliers" ).
            end.            
                        
            assign
               lOK = hSAXWriter:START-ELEMENT ( "suppliers" )
                  lOK = hSAXWriter:START-ELEMENT ( "supplier")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "externalReference",  wfournisseur  )
                  lOK = hSAXWriter:END-ELEMENT ( "supplier")
               lOK = hSAXWriter:START-ELEMENT ( "supplyChains")
                  /* lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "supplyChain", wcircuit[lookup ( entry ( k, l-soc ), l-socdeal )] ) */
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "supplyChain", wcircuit[ii-soc] )
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "currency", "EUR" )
                  lOK = hSAXWriter:END-ELEMENT ( "supplyChains")
               lOK = hSAXWriter:START-ELEMENT ( "items")
                  lOK = hSAXWriter:START-ELEMENT ( "item")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", trim ( artapr.articl ))
                  lOK = hSAXWriter:END-ELEMENT ( "item")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "available", "true").
                  
            assign
                  lOK = hSAXWriter:END-ELEMENT ( "items")              
               lOK = hSAXWriter:END-ELEMENT ( "suppliers").
            
             
         end. 
         
         if wpan <> "" then do :
               assign 
               lOK = hSAXWriter:START-ELEMENT ( "datas")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "PAN"  )
                  lOK = hSAXWriter:START-ELEMENT ( "data")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", wpan )
                  lOK = hSAXWriter:END-ELEMENT ( "data")
               lOK = hSAXWriter:END-ELEMENT ( "datas").
         end.
  
         /* ref fab  */
         if wreffab <> ""  then do :
            assign 
               lOK = hSAXWriter:START-ELEMENT ( "datas")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "REFFAB"  )
                  lOK = hSAXWriter:START-ELEMENT ( "data")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "stringData", wreffab )
                  lOK = hSAXWriter:END-ELEMENT ( "data")
               lOK = hSAXWriter:END-ELEMENT ( "datas").
         end.

         assign lOK = hSAXWriter:END-ELEMENT ( "items") .
      
         do i = 1 to num-entries ( wcode ):
            assign 
               lOK = hSAXWriter:START-ELEMENT ( "vatCode")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "businessUnit", entry( i, wcode ) )
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "vatCode", trim(artapr.tva))
               lOK = hSAXWriter:END-ELEMENT ( "vatCode").
         end.

         /* r�seaux de distribution autoris�s */
         wzone = "EC,ASEC,V,JG".
         do k = 1 to 4 :
         
            assign 
               lOK = hSAXWriter:START-ELEMENT ( "datas")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "RDD" + entry ( k , wzone ) )
                  lOK = hSAXWriter:START-ELEMENT ( "data")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", wrdd [k] )
                  lOK = hSAXWriter:END-ELEMENT ( "data")
               lOK = hSAXWriter:END-ELEMENT ( "datas").
      
         end.
   
         assign 
            /* imputation vente */
            lOK = hSAXWriter:START-ELEMENT ( "datas")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "IMPVEN" )
               lOK = hSAXWriter:START-ELEMENT ( "data")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", upper ( trim( artapr.impvte )))
               lOK = hSAXWriter:END-ELEMENT ( "data")
            lOK = hSAXWriter:END-ELEMENT ( "datas")
            /* imputation ACH */ 
            lOK = hSAXWriter:START-ELEMENT ( "datas")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "IMPACH" )
               lOK = hSAXWriter:START-ELEMENT ( "data")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", upper (trim( artapr.impach )))
               lOK = hSAXWriter:END-ELEMENT ( "data")
            lOK = hSAXWriter:END-ELEMENT ( "datas")
         
            lOK = hSAXWriter:START-ELEMENT ( "datas")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "TYPART" )
               lOK = hSAXWriter:START-ELEMENT ( "data")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", upper ( trim( artapr.typart )))
               lOK = hSAXWriter:END-ELEMENT ( "data")
            lOK = hSAXWriter:END-ELEMENT ( "datas") .
         
         wcode = upper (trim( artapr.provenance )).
         if lookup (wcode , l-prov ) = 0 then wcode = "9".
         assign 
            lOK = hSAXWriter:START-ELEMENT ( "datas")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "PROVEN" )
               lOK = hSAXWriter:START-ELEMENT ( "data")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", wcode )
               lOK = hSAXWriter:END-ELEMENT ( "data")
            lOK = hSAXWriter:END-ELEMENT ( "datas")
         
            lOK = hSAXWriter:START-ELEMENT ( "datas")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "TYPOLO" )
               lOK = hSAXWriter:START-ELEMENT ( "data")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", upper (trim( artapr.typologie )))
               lOK = hSAXWriter:END-ELEMENT ( "data")
            lOK = hSAXWriter:END-ELEMENT ( "datas")      
         
            lOK = hSAXWriter:START-ELEMENT ( "datas")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "EDIETI" )
               lOK = hSAXWriter:START-ELEMENT ( "data")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", trim( artapr.edi-etiq ))
               lOK = hSAXWriter:END-ELEMENT ( "data")
            lOK = hSAXWriter:END-ELEMENT ( "datas")
         
            lOK = hSAXWriter:START-ELEMENT ( "datas")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "EDITAR" )
               lOK = hSAXWriter:START-ELEMENT ( "data")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", trim( artapr.edit-tarif ))
               lOK = hSAXWriter:END-ELEMENT ( "data")
            lOK = hSAXWriter:END-ELEMENT ( "datas").           
         
         
         if artapr.emplac <> "" then do :
            assign 
               lOK = hSAXWriter:START-ELEMENT ( "datas")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "EMPLAC" )
                  lOK = hSAXWriter:START-ELEMENT ( "data")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", upper (trim( artapr.emplac )))
                  lOK = hSAXWriter:END-ELEMENT ( "data")
               lOK = hSAXWriter:END-ELEMENT ( "datas").
         end.
         if wemplac-ie <> "" then do :
            assign 
               lOK = hSAXWriter:START-ELEMENT ( "datas")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "EMPIEDZ" )
                  lOK = hSAXWriter:START-ELEMENT ( "data")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", upper (trim( wemplac-ie )))
                  lOK = hSAXWriter:END-ELEMENT ( "data")
               lOK = hSAXWriter:END-ELEMENT ( "datas").
   
         end.
         /* code amm */
         if wamm <> "" then do :
         assign
               lOK = hSAXWriter:START-ELEMENT ( "datas")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "CODAMM" )
                  lOK = hSAXWriter:START-ELEMENT ( "data")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", trim( wamm ) )
                  lOK = hSAXWriter:END-ELEMENT ( "data")
               lOK = hSAXWriter:END-ELEMENT ( "datas").
   
         end.
   
         /* produit autorise jardin */
         assign
            lOK = hSAXWriter:START-ELEMENT ( "datas")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "PROJAR" )
               lOK = hSAXWriter:START-ELEMENT ( "data")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", trim( weaj ) )
               lOK = hSAXWriter:END-ELEMENT ( "data")
            lOK = hSAXWriter:END-ELEMENT ( "datas").
         
         /* DGR 23/11/2018
         /* SEUIL Split -> 1 par d�faut */
         assign
            lOK = hSAXWriter:START-ELEMENT ( "datas")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "SEUIL" )
               lOK = hSAXWriter:START-ELEMENT ( "data")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "intData", "1" )
               lOK = hSAXWriter:END-ELEMENT ( "data")
            lOK = hSAXWriter:END-ELEMENT ( "datas").
         */
         /* nomenclature */
         assign
            lOK = hSAXWriter:START-ELEMENT ( "hierarchies")
               lOK = hSAXWriter:START-ELEMENT ( "hierarchy")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "PRO" )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "detailCode", upper (trim(artapr.famart2) + trim(artapr.soufam2) + trim(artapr.sssfam2) ))
               lOK = hSAXWriter:END-ELEMENT ( "hierarchy")
            lOK = hSAXWriter:END-ELEMENT ( "hierarchies")    
         .

         if artapr.volume <> 0 and artapr.volume <> 1 then do :
         /* Calcul prix au kg/l */            
            assign 
               lOK = hSAXWriter:START-ELEMENT ( "datas")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "CONTEN")
                  lOK = hSAXWriter:START-ELEMENT ( "data")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "intData", string(artapr.volume * 1000) )
                  lOK = hSAXWriter:END-ELEMENT ( "data")                                     
               lOK = hSAXWriter:END-ELEMENT ( "datas")
               
   
               lOK = hSAXWriter:START-ELEMENT ( "datas")
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "UNICON")
                  lOK = hSAXWriter:START-ELEMENT ( "data")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "EZ" )
                  lOK = hSAXWriter:END-ELEMENT ( "data")
               lOK = hSAXWriter:END-ELEMENT ( "datas").
               
         end.

         
         /* argumentaire de vente */
         /*
         assign
            lOK = hSAXWriter:START-ELEMENT ( "texts")
               lOK = hSAXWriter:START-ELEMENT ( "text")
                  lOK = hSAXWriter:WRITE-EMPTY-ELEMENT ( "title" )
                  lOK = hSAXWriter:WRITE-EMPTY-ELEMENT ( "contents" )
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", "ARG") 
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "documentType", "3") 
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "businessEntity", "PRODUCTVIEW") 
               lOK = hSAXWriter:END-ELEMENT ( "text")
            lOK = hSAXWriter:END-ELEMENT ( "texts").
         */

         assign lOK = hSAXWriter:END-ELEMENT ( "values").
         nbecr = nbecr + 1.
   end. /* for each t-art */
end.  /* do i-sup = 1 to 2 ... */
assign lOK = hSAXWriter:END-ELEMENT ( "productListType").
lOK = hSAXWriter:END-DOCUMENT () .

/*
lOK = hSAXWriter2:END-ELEMENT ( "logisticUnitListType" ) .
lOK = hSAXWriter2:END-DOCUMENT ().
DELETE OBJECT hSAXWriter2. 
*/
DELETE OBJECT hSAXWriter. 


do ii-soc = 1 to num-entries (l-socdeal):
   
   find first t-statut
   where t-statut.codsoc = entry ( ii-soc, l-socdeal)
   no-lock no-error.
   if not available t-statut then next.
   
   l-statut-mag = l-mag-actifs [ii-soc]. 
   
   CREATE SAX-WRITER hSAXWriter3.
   hSAXWriter3:ENCODING = "UTF-8".
   hSAXWriter3:FORMATTED = false .  /* 	si TRUE: fichier de sortie lisible mais ajout de blancs a la place des sauts de lignes a l'integration dans UR */
   lOK = hSAXWriter3:SET-OUTPUT-DESTINATION ( "file", wficsortie3[ii-soc] ).
   lOK = hSAXWriter3:START-DOCUMENT ( ).
   lOK = hSAXWriter3:WRITE-COMMENT ( "Statuts magasins" ) .
   lOK = hSAXWriter3:START-ELEMENT ( "productListType" ).
   
   for each t-statut
   where t-statut.codsoc = entry ( ii-soc, l-socdeal)
   no-lock:
      do istatut = 1 to num-entries(l-statut-mag) :
      
         assign
            lOK = hSAXWriter3:START-ELEMENT ( "values" )
               lOK = hSAXWriter3:WRITE-DATA-ELEMENT ( "code", upper (TRIM(t-statut.articl)))   
               lOK = hSAXWriter3:START-ELEMENT ( "items" )
                  lOK = hSAXWriter3:START-ELEMENT ( "datas")
                     lOK = hSAXWriter3:WRITE-DATA-ELEMENT ( "code", "STATUT" )
                     lOK = hSAXWriter3:START-ELEMENT ( "data")
                        lOK = hSAXWriter3:WRITE-DATA-ELEMENT ( "code", t-statut.statut )
                     lOK = hSAXWriter3:END-ELEMENT ( "data")
                     lOK = hSAXWriter3:START-ELEMENT ( "entities")
                        lOK = hSAXWriter3:START-ELEMENT ( "values")
                           lOK = hSAXWriter3:WRITE-DATA-ELEMENT ( "businessEntity", "MED" )
                           lOK = hSAXWriter3:WRITE-DATA-ELEMENT ( "entityCode", "S" )
                           lOK = hSAXWriter3:WRITE-DATA-ELEMENT ( "entityDetailCode", entry(istatut, l-statut-mag) )
                        lOK = hSAXWriter3:END-ELEMENT ( "values")
                     lOK = hSAXWriter3:END-ELEMENT ( "entities")
                     lOK = hSAXWriter3:WRITE-DATA-ELEMENT ( "remove", "false" )
                  lOK = hSAXWriter3:END-ELEMENT ( "datas")
               lOK = hSAXWriter3:END-ELEMENT ( "items")
            lOK = hSAXWriter3:END-ELEMENT ( "values")                  
                  .
         
      end.
   end.
   assign lOK = hSAXWriter3:END-ELEMENT ( "productListType")
          lOK = hSAXWriter3:END-DOCUMENT () .
          
   DELETE OBJECT hSAXWriter3.
end.
do ii-soc = 1 to num-entries (l-socdeal):   
   /*  dgr 14022019 : etat par soci�t� */
   find first t-codblo
   no-lock no-error.
   if not available t-codblo then next.
   
   CREATE SAX-WRITER hSAXWriter3.
   hSAXWriter3:ENCODING = "UTF-8".
   hSAXWriter3:FORMATTED = false .  /* 	si TRUE: fichier de sortie lisible mais ajout de blancs a la place des sauts de lignes a l'integration dans UR */
   lOK = hSAXWriter3:SET-OUTPUT-DESTINATION ( "file", wficsortie4[ii-soc] ).
   lOK = hSAXWriter3:START-DOCUMENT ( ).
   lOK = hSAXWriter3:WRITE-COMMENT ( "Etat societe" ) .
   lOK = hSAXWriter3:START-ELEMENT ( "productListType" ).

/*   
<?xml version="1.0" encoding="UTF-8"?>
<productListType>
	<values>
		<code>X80095</code>
		<stateLocal>FED</stateLocal>
		<items>
			<code>X80095</code>
		<stateLocal>FED</stateLocal>
		</items>
	</values>
</productListType>

*/   
   
   
   for each t-codblo   
   no-lock:
      assign
         lOK = hSAXWriter3:START-ELEMENT ( "values" )
            lOK = hSAXWriter3:WRITE-DATA-ELEMENT ( "code", upper (TRIM(t-codblo.articl)))   
            lOK = hSAXWriter3:WRITE-DATA-ELEMENT ( "stateLocal",upper (TRIM(t-codblo.etat)))   
            lOK = hSAXWriter3:START-ELEMENT ( "items" )
               lOK = hSAXWriter3:WRITE-DATA-ELEMENT ( "code", upper (TRIM(t-codblo.articl)))   
               lOK = hSAXWriter3:WRITE-DATA-ELEMENT ( "stateLocal",upper (TRIM(t-codblo.etat)))   
            lOK = hSAXWriter3:END-ELEMENT ( "items" )
         lOK = hSAXWriter3:END-ELEMENT ( "values")   .
   end.
   assign lOK = hSAXWriter3:END-ELEMENT ( "productListType")
          lOK = hSAXWriter3:END-DOCUMENT () .
          
   DELETE OBJECT hSAXWriter3.
   /* fin dgr 14022019*/
end.

