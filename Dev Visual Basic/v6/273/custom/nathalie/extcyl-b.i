/*==========================================================================*/
/*                           E X T C Y L - B . I                            */
/* Extraction des donnees pour Cylande                                      */
/*--------------------------------------------------------------------------*/
/* interface CLIENTS EN COMPTE                                              */
/*==========================================================================*/
/* VLE 20180913 Ajout du code externe WINSTORE sous la forme                */
/*              0000 + codsoc + codaux sur 6 pour Web Service DEAL          */
/* VLE 20180919 Ajout du pgm fidelite client en compte                      */ 
/* VLE 20180925 Ajout du code contentieux                                   */
/* VLE 20181008 Modif type de donnees CONT data et non StringData           */
/* VLE 20181015 Reouverture de client ajout date histo specifique           */ 
/* DGR 20181116 correction caracteres accentues                             */
/* DGR 20190122 si code mag ko sur la société on envoie blanc               */
/*==========================================================================*/


wdate = string (  year ( today ) , "9999" )	+ "-" 
	  + string (  month ( today ) , "99" )	+ "-" 
	  + string (  day  ( today ) , "99" )	
	  + "T00:00:00+02:00".
	  
	  

CREATE SAX-WRITER hSAXWriter.
hSAXWriter:ENCODING = "UTF-8".
hSAXWriter:FORMATTED = false.  /* 	si TRUE: fichier de sortie lisible mais ajout de blancs a la place des sauts de lignes a l'integration dans UR */

lOK = hSAXWriter:SET-OUTPUT-DESTINATION ( "file", wficsortie ).
lOK = hSAXWriter:START-DOCUMENT ( ).
lOK = hSAXWriter:WRITE-COMMENT ( "EXTRACTION DES CLIENS EN COMPTE - Customer" ) .
lOK = hSAXWriter:START-ELEMENT ( "customerListType" ).

CREATE SAX-WRITER hSAXWriter2.
hSAXWriter2:ENCODING = "UTF-8".
hSAXWriter2:FORMATTED = false.  /* 	si TRUE: fichier de sortie lisible mais ajout de blancs a la place des sauts de lignes a l'integration dans UR */

lOK = hSAXWriter2:SET-OUTPUT-DESTINATION ( "file", wficsortie2 ).
lOK = hSAXWriter2:START-DOCUMENT ( ).
lOK = hSAXWriter2:WRITE-COMMENT ( "EXTRACTION DES CLIENTS EN COMPTE - Organization" ) .
lOK = hSAXWriter2:START-ELEMENT ( "organizationListType" ).


lect:
for each t-cli use-index i-2
where t-cli.codsoc = soc-trt
no-lock :


	FIND auxapr
	where rowid ( auxapr ) = t-cli.rowauxapr
	no-lock no-error.
	if not available auxapr then next.
	find auxili 
	where rowid ( auxili ) = t-cli.rowauxili
	no-lock no-error.
	if not 	AVAILABLE auxili then next.
		
	nblus = nblus + 1.
              
   ii-soc = lookup ( trim (auxapr.codsoc) , l-socdeal ) no-error.
   if error-status:error then message auxapr.codsoc l-socdeal auxapr.typaux auxapr.codaux view-as alert-box.
   
   assign wmag       		= trim(auxapr.magasin ).
   if lookup ( wmag , l-mag-actifs [ii-soc] )  = 0 then do:
      put stream ano unformatted   "CL  - Code magasin inactif : " wmag " sur le tiers :" soc-trt " " auxapr.typaux " " auxapr.codaux " " auxili.libabr " " auxapr.cpt-ferme " " auxapr.datmaj " " auxapr.opemaj skip.
      toprej = yes.
      wmag = "".
   end.   

   /* referencé par défaut */
	wcode  = trim( auxapr.codsoc )  + trim ( auxapr.codaux ).
	wferme = "R".
  /* Debut de modif VLE 20180925 */
  /* Si contentieux HUISSIER ==> O sinon N */
    if auxapr.contentieux = "3" 
		then wcontentieux = "O".
		else wcontentieux = "N".
  /* Fin de modif VLE 20180925 */
	wnom   = trim( auxili.adres[1] ).
	wprenom = "".
   wcivilite = "".
   assign 	wtypecli   		= "S"	
            .
   
   /* analyse des 2 premiers mots de l'adresse pour extraire la civilite le nom le prenom */
   wmot1  = upper ( trim( entry ( 1 , auxili.adres [1] , " ") ) ) no-error.
	if error-status:error then wmot1 = "".
   
   wmot2  = upper ( trim( entry ( 2 , auxili.adres [1] , " ") ) ) no-error.
	if error-status:error then wmot2 = "".
   
   
	if trim ( wmot1 ) <> "" then do:
		if wmot1 matches "*MR*" then
         assign 	wcivilite = "MR".
      if  wmot1 matches "*MME*" or wmot1 matches "*MLE*" or   wmot1 matches "*MLLE*" then
         assign 	wcivilite = "MME".
      if wcivilite <> "" then do:
         wprenom 	 = upper ( trim ( replace ( wnom , wmot1 ,  "" ) ) ) .
      
         if trim ( wmot2 ) <> "" then 
            assign 
               wnom = wmot2
               wprenom 	 = upper ( trim ( replace ( wprenom , wmot2 ,  "" ) ) ) .
      end.
   end .
	
   wnom  = trim ( substr ( wnom, 1 , 32) ).
	if wprenom <> "" then wprenom  = trim ( substr ( wprenom, 1 , 32) ).
   
	wcp = entry( 1, auxili.adres[5] , " " ) no-error.
	if error-status:error then wcp = "00000".
	wville = trim ( substr ( auxili.adres[5] , length( wcp ) + 1  ) ) .
	wadres = trim ( auxili.adres[2] ) + " " + trim ( auxili.adres[3] ) + trim ( auxili.adres[4] ).

	/* ecriture fichier oraganization */
	ASSIGN 	
		/* debut etiquette "values" */
		lOK = hSAXWriter2:START-ELEMENT ( "values" )	
			lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "code" , wcode )
			lOK = hSAXWriter2:START-ELEMENT ( "externalCodes" )
				lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "typeCode", "DEAL" )
				lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "code", wcode )
			lOK = hSAXWriter2:END-ELEMENT("externalCodes")
			
			lOK = hSAXWriter2:START-ELEMENT ( "name" )
				lOK = hSAXWriter2:WRITE-CDATA (  trim( auxili.libabr ) )
			lOK = hSAXWriter2:END-ELEMENT ( "name" )

			lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "description" , "Organisation sur entit" + chr (233) + " op" + chr ( 233) + "rationnelle")
			lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "type" , "INC" )

			lOK = hSAXWriter2:WRITE-COMMENT ( "etat" )
			lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "state" , wferme )
			lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "localState" , wferme ) 
			lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "validStateTime" , wdate )
			lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "custAccount" , "true" ) 
		
			lOK = hSAXWriter2:START-ELEMENT ( "addresses" )
				lOK = hSAXWriter2:START-ELEMENT ( "address" )
            	lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "postalCode", wcp )
				
					lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "postalCode", wcp )
					lOK = hSAXWriter2:START-ELEMENT ( "city" )
						lOK = hSAXWriter2:WRITE-CDATA (  wville )
					lOK = hSAXWriter2:END-ELEMENT ( "city" )
					lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "country" , "FR" ) 
					lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "type" , "POST" ) 
					lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "description" , "Adresse postale" )
					lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "main" , "true" ) 
					/*
					lOK = hSAXWriter2:START-ELEMENT ( "description" )
						lOK = hSAXWriter2:WRITE-CDATA (  wadres )
					lOK = hSAXWriter2:END-ELEMENT ( "description" )
					*/
				lOK = hSAXWriter2:END-ELEMENT ( "address" )
			lOK = hSAXWriter2:END-ELEMENT ( "addresses" )

				
			lOK = hSAXWriter2:START-ELEMENT ( "members" )
				lOK = hSAXWriter2:START-ELEMENT ( "organization" )
					lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "code" , wcode ) 
				lOK = hSAXWriter2:END-ELEMENT ( "organization" )
				lOK = hSAXWriter2:START-ELEMENT ( "customer" )
					lOK = hSAXWriter2:START-ELEMENT ( "externalCodes" )
						lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "typeCode", "DEAL" )
						lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "code", wcode )
					lOK = hSAXWriter2:END-ELEMENT ( "externalCodes" )
				lOK = hSAXWriter2:END-ELEMENT ( "customer" )
				lOK = hSAXWriter2:WRITE-DATA-ELEMENT ( "startDate" , wdate)
			lOK = hSAXWriter2:END-ELEMENT ( "members" )
		lOK = hSAXWriter2:END-ELEMENT ( "values" ).
   
   if lOK then    nbecr = nbecr + 1 .
   else 	do:
      put stream ano unformatted 
         "CL -- probleme ecriture fichier organization  " 
         soc-trt " " 
         auxapr.typaux " " 
         auxapr.codaux " " 
         auxapr.libabr skip.
      toprej = yes.
      next lect.
	end.

   assign wtoppref = "true"
          wtopprinc = "true".
	/* ecriture fichier customer */ 
	ASSIGN 	
		/* debut etiquette "values" */
		lOK = hSAXWriter:START-ELEMENT ( "values" )	
			lOK = hSAXWriter:START-ELEMENT ( "externalCodes" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "typeCode", "DEAL" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", wcode )
			lOK = hSAXWriter:END-ELEMENT ( "externalCodes" )
		/* Debut de modif VLE 20180913 */
			lOK = hSAXWriter:START-ELEMENT ( "externalCodes" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "typeCode", "WST3" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", "0000" + wcode )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "main", "true" )
			lOK = hSAXWriter:END-ELEMENT ( "externalCodes" )
		/* Fin de modif VLE 20180913 */
		
                /* Debut de modif VLE 20180919 */
                        lOK = hSAXWriter:START-ELEMENT ( "loyaltyIDs" )
                                lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "loyaltyId", wcode )
                                lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "loyaltyProgramCode", "5" )
                                lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "loyaltySupportType", "CCT" )
                                lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "main", "true" )
                        lOK = hSAXWriter:END-ELEMENT ( "loyaltyIDs" )
                        lOK = hSAXWriter:START-ELEMENT ( "loyaltyAccounts" )
                                lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "loyaltyProgramCode", "5" )
                                lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "loyaltyAccountType", "5" )
                        lOK = hSAXWriter:END-ELEMENT ( "loyaltyAccounts" )
                        lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "mainLoyaltyProgram" , "5" )
                /* Fin de modif VLE 20180919 */
                	
			lOK = hSAXWriter:START-ELEMENT ( "person" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "gender", wcivilite )
				lOK = hSAXWriter:START-ELEMENT ( "firstName" )
					lOK = hSAXWriter:WRITE-CDATA ( wprenom )
				lOK = hSAXWriter:END-ELEMENT ( "firstName" )
				lOK = hSAXWriter:START-ELEMENT ( "lastName" )
					lOK = hSAXWriter:WRITE-CDATA ( wnom )
				lOK = hSAXWriter:END-ELEMENT ( "lastName" )
			
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "livingCountry", "FR" )
				lOK = hSAXWriter:WRITE-EMPTY-ELEMENT ( "sex" )
			lOK = hSAXWriter:END-ELEMENT ( "person" )
			
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "custAccount" , "1" ) 
         lOK = hSAXWriter:START-ELEMENT ( "issuedInSupport" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "nature" , wtypecli ) 
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "id" , wmag )
			lOK = hSAXWriter:END-ELEMENT ( "issuedInSupport").

         wzone = "".
			if trim ( auxapr.alpha-5 ) <> "" and index ( auxapr.alpha-5 , "@") <> 0 then do:
            wzone = trim( auxapr.alpha-5 ).
            
            /* . avant @ interdit */
            if index ( wzone , ".@" ) <> 0 then do:
               put stream ano unformatted   "CL  - erreur @mail '.@' sur le tiers :" soc-trt " " auxapr.typaux " " auxapr.codaux " " auxili.libabr  " " wzone skip.
               toprej = yes.
               wzone = "".
            end.   
            
            /* pas de . apres @ */
            else if index ( entry ( 2, wzone , "@") , "." ) = 0 then do:
                  put stream ano unformatted   "CL  - erreur @mail pas de '.' apres '@' sur le tiers :" soc-trt " " auxapr.typaux " " auxapr.codaux " " auxili.libabr  " " wzone skip.
                  toprej = yes.
                  wzone = "".
               end.   
               else if substr ( wzone , length( wzone ), 1 ) = "." then do :
                  put stream ano unformatted   "CL  - erreur @mail finit par un '.' sur le tiers :" soc-trt " " auxapr.typaux " " auxapr.codaux " " auxili.libabr  " " wzone skip.
                  toprej = yes.
                  wzone = "".
               end. 
            if wzone <>  "" then do:
               assign 
                  lOK = hSAXWriter:START-ELEMENT ( "customerEmails" )
                     /* lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "remove", "true" ) */
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "contactMethodTypeCode", "MAIL")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "description" , "Adresse Mail" )
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "preferredContactMethod" , wtoppref ) 
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "main" , wtopprinc )
                     lOK = hSAXWriter:START-ELEMENT ( "email" )
                        lOK = hSAXWriter:WRITE-CDATA ( trim ( wzone ) )
                     lOK = hSAXWriter:END-ELEMENT ( "email" )
                  lOK = hSAXWriter:END-ELEMENT ( "customerEmails" ).						
               assign wtoppref = "false".
            end.
         end. 
         wzone = "".
			if trim ( auxapr.telex ) <> "" then do:
            wzone = trim( auxapr.telex ).
            do j = 1 to length( wzone ):
                x = substr( wzone , j , 1 ).
                if not((asc(x) >= 48 and asc(x) <=57 ) or asc( x ) = 33 or asc( x ) = 40 or asc( x ) = 41 )then
                    overlay(wzone , j, 1) = " ".
            end.
            wzone = trim( replace(wzone ,"  "," ")).

            if wzone <> "" then
               assign 
                  lOK = hSAXWriter:START-ELEMENT ( "customerPhones" )
                     /* lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "toRemove", "true" ) */
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "contactMethodTypeCode", "MOB")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "description" , "Telephone mobile" )
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "preferredContactMethod" , wtoppref ) 
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "main" , wtopprinc )
                     lOK = hSAXWriter:START-ELEMENT ( "phone" )
                        lOK = hSAXWriter:WRITE-CDATA ( substr ( wzone  , 1 , 20 )  )
                     lOK = hSAXWriter:END-ELEMENT ( "phone" )
                  lOK = hSAXWriter:END-ELEMENT ( "customerPhones" )
                  wtoppref = "false"
                  wtopprinc = "false".						
         end.
         
         wzone = "".
         		
         if trim ( auxapr.teleph ) <> "" then do:
            wzone = trim( auxapr.teleph ).
            do j = 1 to length( wzone ):
                x = substr( wzone , j , 1 ).
                if not((asc(x) >= 48 and asc(x) <=57 ) or asc( x ) = 33 or asc( x ) = 40 or asc( x ) = 41 )then
                    overlay(wzone , j, 1) = " ".
            end.
            wzone = trim( replace(wzone ,"  "," ")).

            if wzone <> "" then 
            
               assign 
                  lOK = hSAXWriter:START-ELEMENT ( "customerPhones" )
                     /* lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "toRemove", "true" )  */
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "contactMethodTypeCode", "FIXE")
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "description" , "Telephone Fixe" )
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "preferredContactMethod" , wtoppref ) 
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "main" , wtopprinc )
                     lOK = hSAXWriter:START-ELEMENT ( "phone" )
                        lOK = hSAXWriter:WRITE-CDATA ( substr ( wzone , 1 , 20)  )
                     lOK = hSAXWriter:END-ELEMENT ( "phone" )
                  lOK = hSAXWriter:END-ELEMENT ( "customerPhones" )
                  wtoppref = "false" 
                  wtopprinc = "false".						
         end.
			assign
				lOK = hSAXWriter:START-ELEMENT ( "customerAddresses" )
               /* lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "remove", "true" ) */
					lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "contactMethodTypeCode", "POST")
					lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "description" , "Adresse postale" )
					lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "main" , "true" )
					lOK = hSAXWriter:START-ELEMENT ( "address" )
						lOK = hSAXWriter:START-ELEMENT ( "addressLine3" )
							lOK = hSAXWriter:WRITE-CDATA (  trim( auxili.adres[2] ) )
						lOK = hSAXWriter:END-ELEMENT ( "addressLine3" )
						/*  DGR 12042018 
                  lOK = hSAXWriter:START-ELEMENT ( "addressLine4" )
							lOK = hSAXWriter:WRITE-CDATA (  trim ( auxili.adres[3] ) )
						lOK = hSAXWriter:END-ELEMENT ( "addressLine4" ) */
                  lOK = hSAXWriter:START-ELEMENT ( "highwayLabel" )
							lOK = hSAXWriter:WRITE-CDATA (  trim ( auxili.adres[3] ) )
						lOK = hSAXWriter:END-ELEMENT ( "highwayLabel" ) 
                  lOK = hSAXWriter:START-ELEMENT ( "addressLine5" )
							lOK = hSAXWriter:WRITE-CDATA (  trim ( auxili.adres[4] ) )
						lOK = hSAXWriter:END-ELEMENT ( "addressLine5" )
						
						lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "postalCode", wcp )
						lOK = hSAXWriter:START-ELEMENT ( "city" )
							lOK = hSAXWriter:WRITE-CDATA (  wville )
						lOK = hSAXWriter:END-ELEMENT ( "city" )
						lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "country" , "FR" ) 
					
					lOK = hSAXWriter:END-ELEMENT ( "address" )
				lOK = hSAXWriter:END-ELEMENT ( "customerAddresses" )
				
				/* Debut de modif VLE 20180925 */
				lOK = hSAXWriter:START-ELEMENT ( "datas" )
					lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code" , "CONT" )
                                        lOK = hSAXWriter:START-ELEMENT ( "data" ) 
					        lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code" , wcontentieux )
                                        lOK = hSAXWriter:END-ELEMENT ( "data" ) 
				lOK = hSAXWriter:END-ELEMENT ( "datas" )
				/* Fin de modif VLE 20180925 */
				lOK = hSAXWriter:START-ELEMENT ( "datas" )
					lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code" , "COMPT" )
                                        lOK = hSAXWriter:START-ELEMENT ( "data" ) 
					        lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "stringData" , wcode )
                                        lOK = hSAXWriter:END-ELEMENT ( "data" )         
				lOK = hSAXWriter:END-ELEMENT ( "datas" ).
			
            if auxapr.cpt-ferme = "1" then wferme = "S".

            /* Debut modif VLE 20181015 */
            if wferme = "R" then 
               assign
               lOK = hSAXWriter:START-ELEMENT ( "histoDates" )
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "deletionTime" , "1898-01-01T01:01:01+01:00" )         
               lOK = hSAXWriter:END-ELEMENT ( "histoDates" )                                   
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "localDeletionTime" , "1898-01-01T01:01:01+01:00" ).
            /* Fin modif VLE 20181015  */
                               
            assign
               lOK = hSAXWriter:WRITE-COMMENT ( "etat" )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "state" , wferme )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "stateLocal" , wferme ) .
			
         lOK = hSAXWriter:END-ELEMENT ( "values" ).
		
	if lOK then 
		nbecr2 = nbecr2 + 1 .
	else do:
      put stream ano unformatted 
         "CL -- probleme ecriture fichier Customer  " 
         soc-trt " " 
         auxapr.typaux " " 
         auxapr.codaux " " 
         auxapr.libabr skip.
      toprej = yes.
      next lect.
	end.	
   
END. /* for each t-cli */

lOK = hSAXWriter:END-ELEMENT ( "customerListType" ) .
lOK = hSAXWriter:END-DOCUMENT () .

lOK = hSAXWriter2:END-ELEMENT ( "organizationListType" ) .
lOK = hSAXWriter2:END-DOCUMENT ().


DELETE OBJECT hSAXWriter. 
DELETE OBJECT hSAXWriter2. 

						