/*==========================================================================*/
/*                           E X T C Y L - C . I                            */
/* Extraction des donnees pour Cylande                                      */
/*--------------------------------------------------------------------------*/
/* interface TAXES    ne sert plus ......... :-(                            */
/*==========================================================================*/

/* fichier des TAXES : 
   NB: utilisation de put au lieu des commandes progress pour pouvoir envoyer le symbole Euro correctement 
*/

output stream fichier to value ( wficsortie ).

put stream fichier  
    "<?xml version='"'1.0'"' encoding='"'UTF-8'"'?>" skip
    "<ecoMaterialTypeListType>" skip
    .
    
assign nblus = 0 nbecr = 0.

do i-tax = 1 to num-entries ( l-tax ) :

    boucle:

    for each t-ecotax
    where t-ecotax.wtype = entry ( i-tax , l-tax )
    no-lock :   
        nblus = nblus + 1.
        wcode = trim( t-ecotax.wtype + t-ecotax.wcode ) .
        /* remove du code si il existe deja pour eviter erreur intégration Cylande */
        put stream fichier unformatted 
            "<values>" skip
                "<remove>true</remove>" skip
                "<code>" wcode "</code>" skip
            "</values>" skip.
    
        put stream fichier unformatted 
            "<values>" skip
                "<code>" wcode "</code>" skip.
        
        wcode = t-ecotax.wtype + " " + trim ( string ( t-ecotax.wvaleur, ">>9.999999" ) ) + " "  /*  chr(128) */ . 
        
        put stream fichier unformatted
                "<description>"  wcode chr ( 50082 , "iso8859-15" , "utf-8") chr ( 49794 , "iso8859-15" , "utf-8") chr ( 49836 , "iso8859-15" , "utf-8")  "</description>" skip
                "<shortDescription>" wcode chr ( 50082 , "iso8859-15" , "utf-8") chr ( 49794 , "iso8859-15" , "utf-8") chr ( 49836 , "iso8859-15" , "utf-8") "</shortDescription>" skip .
        
        wcode = trim( t-ecotax.wtype + t-ecotax.wcode ) . 
        put stream fichier unformatted 
                "<tariff>" skip
                    "<ecoMaterial>" wcode "</ecoMaterial>" skip
                    "<maxWeight>0.0</maxWeight>" skip
                    "<tariff>" trim ( string ( t-ecotax.wvaleur, ">>9.999999" ) ) "</tariff>" skip
                    "<startDate>" wdate "</startDate>" skip
                "</tariff>" skip
                "<amountType>FIXEDAMOUNT</amountType>" skip
                "<posSystemExternalCode>" trim ( t-ecotax.wcodecyl ) "</posSystemExternalCode>" skip
                "<ecoTaxTypeCode>" trim( t-ecotax.wtype ) "</ecoTaxTypeCode>" skip
            "</values>" skip.

        nbecr = nbecr + 1 .
            
        /* if nbecr >= 5 then leave boucle. */
    end. /* for each t-ecotax ... */
        
END. /* do i-tax = 1 ....*/
put stream fichier unformatted "</ecoMaterialTypeListType>" skip.
output stream fichier close.
                        