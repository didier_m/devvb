/*==========================================================================*/
/*                           E X T C Y L - F . I                            */
/* Extraction des donnees pour Cylande                                      */
/*--------------------------------------------------------------------------*/
/* interface TARIFS ACHAT                                                   */
/*==========================================================================*/
/* Prog : extcytar.p Export tarifs d'achat  GRASSOT reprise format XML      */
/* VLE le 16/11/2017     */

CREATE SAX-WRITER hSAXWriter.
hSAXWriter:ENCODING = "UTF-8".
hSAXWriter:FORMATTED = FALSE .  /* 	si TRUE: fichier de sortie lisible mais ajout de blancs 
									a la place des sauts de lignes a l'integration dans UR */

lOK = hSAXWriter:SET-OUTPUT-DESTINATION ( "file", wficsortie ).
lOK = hSAXWriter:START-DOCUMENT ( ).
lOK = hSAXWriter:WRITE-COMMENT ( "EXTRACTION DES TARIFS ACHATS ").
lOK = hSAXWriter:START-ELEMENT ( "productListType" ).
   
for each t-tarach 
no-lock:
   nblus = nblus + 1.
   assign
            lOK = hSAXWriter:START-ELEMENT ( "values" )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code" , TRIM (t-tarach.articl ) )
               lOK = hSAXWriter:START-ELEMENT ( "suppliers" )
                  lOK = hSAXWriter:START-ELEMENT ( "supplier" )
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "externalReference", "880047" )
                  lOK = hSAXWriter:END-ELEMENT ( "supplier" ).

   find tables
   where tables.codsoc = ""
   and   tables.etabli = ""
   and   tables.typtab = "TAX"
   and   tables.prefix = "TYP-TAXE"
   and   tables.codtab  = t-tarach.tva
   no-lock no-error.
   if available tables then
       txtva = 1 + ( tables.nombre[1] / 100 ).
   
   do i-soc = 1 to num-entries ( t-tarach.codsoc) :
      wsoc = entry ( i-soc, t-tarach.codsoc) .
      find artapr
      where artapr.codsoc = wsoc
      and   artapr.articl = t-tarach.articl
      no-lock no-error.
      if not available artapr then next.
      
      assign 
         lOK = hSAXWriter:START-ELEMENT ( "supplyChains")
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "supplyChain" , entry (  lookup ( wsoc , l-socdeal) , l-circuit )  ).
            
         
      find first {tarcli.i} 
      where {tarcli.i}.motcle =  "VTE"
		and   {tarcli.i}.codsoc =  wsoc
		and   {tarcli.i}.articl =  artapr.articl
		and   ( {tarcli.i}.date-debut <= today and
             {tarcli.i}.date-fin = ? ) 
      and   {tarcli.i}.typaux =  ""
		and   {tarcli.i}.codaux =  ""
		and   {tarcli.i}.code-tarif = "CLI"
      no-lock no-error.
      if available {tarcli.i} then do:
         /*
         if {tarcli.i}.valeur = 0 or {tarcli.i}.valeur = 0.001
         then next.        
         */
         val-tarif = {tarcli.i}.valeur.
         
         find agc-tarcli 
         where agc-tarcli.motcle =  {tarcli.i}.motcle
         and   agc-tarcli.codsoc =  {tarcli.i}.codsoc
			/* DGR 03082010   and   agc-tarcli.datmaj =  {tarcli.i}.datmaj */
         and   agc-tarcli.date-debut =  {tarcli.i}.date-debut
         and   agc-tarcli.typaux =  {tarcli.i}.typaux
         and   agc-tarcli.codaux =  {tarcli.i}.codaux
         and   agc-tarcli.code-tarif = "AGC"
			/* rajout DGR 03082010 acces avec code article */
          and   agc-tarcli.articl =  {tarcli.i}.articl
         no-lock no-error.
         if available agc-tarcli
            then assign val-tarif = agc-tarcli.valeur.         
      
         wdate = string ( year  ( {tarcli.i}.date-debut ) , "9999" ) + "-" +
                 string ( month ( {tarcli.i}.date-debut ) , "99" ) +   "-" +
                 string ( day   ( {tarcli.i}.date-debut ) , "99") 
            .
   
         wvaleur = val-tarif / txtva no-error.
         if error-status:error then next.
         
         if t-tarach.taux <> 0 and t-tarach.taux <> ? then 
            wvaleur = round(wvaleur - (wvaleur * t-tarach.taux / 100),3)  .         
         if wvaleur = ? then next.
         wval = string ( wvaleur ) no-error.
         if error-status:error then next.

         assign 
                     lOK = hSAXWriter:START-ELEMENT ( "itemPurchasePrices")
                        lOK = hSAXWriter:START-ELEMENT ( "item")
                           lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", TRIM(t-tarach.articl) )
                        lOK = hSAXWriter:END-ELEMENT ( "item")
                        lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", "STE" )
                        lOK = hSAXWriter:START-ELEMENT ( "historyPurchasePrices").
                        if trim ( wdate ) <> "" then 
                           lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "startDate" , wdate ).
                        else 
                           lOK = hSAXWriter:WRITE-EMPTY-ELEMENT ( "startDate" ).
                        assign
                           lOK = hSAXWriter:START-ELEMENT ( "purchasePrice")
                              lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "currency", "EUR" )
                              lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "value", wval ) 
                           lOK = hSAXWriter:END-ELEMENT ( "purchasePrice")
                        lOK = hSAXWriter:END-ELEMENT ( "historyPurchasePrices")
                     lOK = hSAXWriter:END-ELEMENT ( "itemPurchasePrices")
                  .
         nbecr = nbecr + 1.
         nbtarach  [i-soc] = nbtarach  [i-soc] + 1.
         
      end.
      for each {tarcli.i} 
      where {tarcli.i}.motcle =  "VTE"
		and   {tarcli.i}.codsoc =  wsoc
		and   {tarcli.i}.articl =  artapr.articl
		and   {tarcli.i}.date-debut > today      
		and   {tarcli.i}.typaux =  ""
		and   {tarcli.i}.codaux =  ""
		and   {tarcli.i}.code-tarif = "CLI"
      no-lock :
         if {tarcli.i}.valeur = 0 or {tarcli.i}.valeur = 0.001
         then next.        
         
         val-tarif = {tarcli.i}.valeur.
         
         find agc-tarcli 
         where agc-tarcli.motcle =  {tarcli.i}.motcle
         and   agc-tarcli.codsoc =  {tarcli.i}.codsoc
			/* DGR 03082010   and   agc-tarcli.datmaj =  {tarcli.i}.datmaj */
         and   agc-tarcli.date-debut =  {tarcli.i}.date-debut
         and   agc-tarcli.typaux =  {tarcli.i}.typaux
         and   agc-tarcli.codaux =  {tarcli.i}.codaux
         and   agc-tarcli.code-tarif = "AGC"
			/* rajout DGR 03082010 acces avec code article */
          and   agc-tarcli.articl =  {tarcli.i}.articl
         no-lock no-error.
         if available agc-tarcli
            then assign val-tarif = agc-tarcli.valeur.         
      
         wdate = string ( year  ( {tarcli.i}.date-debut ) , "9999" ) + "-" +
                 string ( month ( {tarcli.i}.date-debut ) , "99" ) +   "-" +
                 string ( day   ( {tarcli.i}.date-debut ) , "99") 
            .
   
         wvaleur = val-tarif / txtva no-error.
         if error-status:error then next.
         
         if t-tarach.taux <> 0 and t-tarach.taux <> ? then 
            wvaleur = round(wvaleur - (wvaleur * t-tarach.taux / 100),3)  .         
         if wvaleur = ? then next.
         wval = string ( wvaleur ) no-error.
         if error-status:error then next.

         assign
            /* lOK = hSAXWriter:START-ELEMENT ( "values" )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code" , TRIM (t-tarach.articl ) )
               lOK = hSAXWriter:START-ELEMENT ( "suppliers" )
                  lOK = hSAXWriter:START-ELEMENT ( "supplier" )
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "externalReference", "880047" )
                  lOK = hSAXWriter:END-ELEMENT ( "supplier" )
                  lOK = hSAXWriter:START-ELEMENT ( "supplyChains") 
                     /* lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "supplyChain" , wcircuit ) */
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "supplyChain" , entry (  lookup ( wsoc , l-socdeal) , l-circuit )  ) */
                     lOK = hSAXWriter:START-ELEMENT ( "itemPurchasePrices")
                        lOK = hSAXWriter:START-ELEMENT ( "item")
                           lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", TRIM(t-tarach.articl) )
                        lOK = hSAXWriter:END-ELEMENT ( "item")
                        lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", "STE" )
                        lOK = hSAXWriter:START-ELEMENT ( "historyPurchasePrices").
                        if trim ( wdate ) <> "" then 
                           lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "startDate" , wdate ).
                        else 
                           lOK = hSAXWriter:WRITE-EMPTY-ELEMENT ( "startDate" ).
                        assign
                           lOK = hSAXWriter:START-ELEMENT ( "purchasePrice")
                              lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "currency", "EUR" )
                              lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "value", wval ) 
                           lOK = hSAXWriter:END-ELEMENT ( "purchasePrice")
                        lOK = hSAXWriter:END-ELEMENT ( "historyPurchasePrices")
                     lOK = hSAXWriter:END-ELEMENT ( "itemPurchasePrices")
                  /* lOK = hSAXWriter:END-ELEMENT ( "supplyChains")
               lOK = hSAXWriter:END-ELEMENT ( "suppliers") 
            lOK = hSAXWriter:END-ELEMENT ( "values") */.                   
            nbtarach  [i-soc] = nbtarach  [i-soc] + 1.
         nbecr = nbecr + 1.
      end.     
      assign 
         lOK = hSAXWriter:END-ELEMENT ( "supplyChains").
   end.
   assign 
           lOK = hSAXWriter:END-ELEMENT ( "suppliers") 
         lOK = hSAXWriter:END-ELEMENT ( "values").                  

end. /* for each t-tarach */
lOK = hSAXWriter:END-ELEMENT ( "productListType").

lOK = hSAXWriter:END-DOCUMENT () .

DELETE OBJECT hSAXWriter. 


