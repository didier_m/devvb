/*==========================================================================*/
/*                           E X T C Y L - 0 . I                            */
/* Extraction des donnees pour Cylande                                      */
/*--------------------------------------------------------------------------*/
/* Variables et Frames et proc�dures communes                                */
/*==========================================================================*/
/* DGR 25052018 : ajout operateur specifique pour ne pas prendre les gencods*/
/*                grassot 0000 en principal                                 */
/* DGR 01062018 : ajout filiere fournisseur                                 */
/* DGR 07062018 : liste provenance autorisees en parametre                  */
/* DGR 08062018 : extraction des articles MAJ en SD                         */
/* DGR 14082018 : avant des artciles avec suppressions de gencods en 1er    */
/* VLE 25092018 : Ajout du code contentieux pour client en compte           */
/* DGR 12102018 : GLN du codaux-fac                                         */
/* DGR 16112018 : conversion des stocks si u2 u3                            */
/* DGR 20112018 : mouvements fid par soc: 08 ou autre                       */
/* DGR 29112018 : famille interdite en remise                               */
/*                articles gratuits                                         */
/* DGR 17122018 : envoi statut winstore                                     */
/* DGR 14022019 : envoi code blocage SD                                     */
/* DGR 07032019 : taux de remise de l'article                               */
/* CCO 15072019 : ajout {tarcli.i}                                          */

/* Variables */
def var ind                      as int                           no-undo.
def var lib-trt                  as char format "x(15)"           no-undo.
def var lib-table                as char format "x(76)"           no-undo.

def {1} shared var libelle       as char extent 300               no-undo.
def {1} shared var type-trt      as char format "x"               no-undo.
def {1} shared var l-soc         as char format "x(32)"           no-undo.
def {1} shared var l-tables      as char format "x(45)"           no-undo.
def {1} shared var date-debut    as date format "99/99/9999"      no-undo.

def {1} shared buffer date-tabges for tabges .

/* Dessin des Frames */
def {1} shared frame fr-saisie.
def {1} shared frame fr-trt.


Form libelle[01] format "x(18)" type-trt lib-trt  skip
     libelle[02] format "x(18)" l-soc             skip
     libelle[03] format "x(18)" l-tables          skip
     libelle[04] format "x(18)" date-debut
     with frame fr-saisie row 5 centered no-label overlay { v6frame.i }.

/* variable pour les fichiers */
def stream mail .
def stream log .
def stream ano .
def stream sto . 

def var toprej                   as log  .
def stream fichier .

def var i                as int. 
def var j                as int.
def var k                as int.
def var x                as char.


Form skip lib-table skip
     with frame fr-trt row 14 centered no-label overlay { v6frame.i }.

def var magasi-soc    		      as char 									no-undo.
def var l-mag-equipe             as char  	initial "A"     		no-undo.
def var l-mag-actifs             as char     extent 5             no-undo.
def var l-mag-stocks             as char     extent 5             no-undo.
def var i-mag-stocks             as int                           no-undo.
def var w-mag-stocks             as char                          no-undo.
def var date-invmag              as date                          no-undo.
def var stophy                   as dec      decimals 3           no-undo.               
def var warticl                  as char                          no-undo.
def var wtoppref                 as char                          no-undo.
def var wtopprinc                as char                          no-undo.
def var wtopremov                as char                          no-undo.
def var l-socfid                 as char                          no-undo.
def var l-progfid                as char                          no-undo.
def var l-famnorem               as char                          no-undo.
def var l-famrem                  as char                          no-undo.
def var l-famremssf              as char                          no-undo.
def var l-famtxssf2              as char                          no-undo.
def var l-famtxsfa2              as char                          no-undo.
def var i-fam                    as int                           no-undo.
def var l-nomgrat                as char                          no-undo.
def var l-artpoub                as char                          no-undo.
def var txtva                    as dec      decimals 3           no-undo.
def var val-tarif                as dec      decimals 3           no-undo.

/* date et heure de traitement */
def var wdat-trt as date.
def var wficdat-trt as char.
def var wheu-trt as char.

def var date-arrete              as date   extent 10              no-undo. 
def var l-socdeal                as char                          no-undo.
def var repert-dep   		      as char 									no-undo.
def var repert-deptrt  	         as char 									no-undo.
def var repert-arr    		      as char 									no-undo.
def var repert-arrtrt  	         as char 									no-undo.
def var repert-rejet 		      as char 									no-undo.
def var repert-tra 	  		      as char 									no-undo.
def var repert-mail   		      as char 									no-undo.
def var destin-mail    	         as char 									no-undo.
def var l-param-divers           as char                          no-undo.
def var trt-dupsui               as char                          no-undo.
def var x-opegencod              as char                          no-undo.

def var fictmp   			         as char									no-undo.
def var ficmail  			         as char									no-undo.
def var ficjoint 			         as char									no-undo.
def var wficlog  			         as char									no-undo.
def var wficano  			         as char									no-undo.
def var wficsortie 			      as char									no-undo.
def var wficfinal 			      as char									no-undo.
def var wficsortie2			      as char									no-undo.
def var wficfinal2 			      as char									no-undo.
def var wficsortie3			      as char extent 4  					no-undo.
def var wficfinal3			      as char extent 4						no-undo.
def var wficsortie4			      as char extent 4  					no-undo.
def var wficfinal4			      as char extent 4						no-undo.
def var nblus				         as int 									no-undo.
def var nbecr				         as int 									no-undo.
def var nbecr2				         as int 									no-undo.

def var nbtarach    as dec          extent 5                      no-undo.

 /* Debut de modif VLE 20180925 */
/* variable CLIENTS */
def var wcontentieux             as char                          no-undo.
 /* Fin  de modif VLE 20180925 */
 

/* VARIABLES SALARIES     */  
def var soc-cartes-sal		      as char 									no-undo.
def var wtypecli   			      as char 									no-undo.
def var wmag       			      as char 									no-undo.
def var wnom       			      as char format 	"x(32)"			no-undo.
def var wprenom    			      as char format 	"x(32)" 			no-undo.
def var wferme     			      as char format 	"X"				no-undo.
def var wbudget    			      as dec 				  					no-undo. 
   
/* variable fou */
def var wsoc                     as char                          no-undo. 
def var wblo                     as char                          no-undo. 
def var aa                       as int                           no-undo.
def var bb                       as int                           no-undo.
def var nb                       as int                           no-undo.
def var nbt                      as int                           no-undo.
def var wpays                    as char format "xxx"             no-undo.
def var code-barre               as char                          no-undo.
def var gencod-ok                as log                          no-undo.

def buffer aux-fac               for auxapr.

def var wremo01                  as char                          no-undo.
def var wremo08                  as char                          no-undo.
def var wremo59                  as char                          no-undo.
def var wremo88                  as char                          no-undo.

def var wmain01                  as char                          no-undo.
def var wmain08                  as char                          no-undo.
def var wmain59                  as char                          no-undo.
def var wmain88                  as char                          no-undo.
   

def buffer soc-artapr for artapr.
def buffer agc-tarcli for {tarcli.i}.
def var ii-soc                   as int                           no-undo.
def var xx-param                 as char                          no-undo.
def var l-u2dec                  as char                          no-undo .
def var l-u3dec                  as char                          no-undo .
def var wrdd                     as char extent 5                 no-undo.
def var wudeal                   as char                          no-undo.
def var wuvte                    as char                          no-undo.
def var wfouprinc                as char                          no-undo .
def var i-soc				         as int 									no-undo.
def var soc-trt				      as char 									no-undo.
def var soc-auxili			      as char 									no-undo.
def var soc-art-grp 		         as char 									no-undo.
def var soc-fullcyl 		         as char 									no-undo.
def var wrpd                     as dec                           no-undo.   
def var wnature                  as char                          no-undo.
def var wzone                    as char                          no-undo.
def var wreference               as char                          no-undo.
def var wcode				         as char 									no-undo.
def var wart				         as char 									no-undo.
def var wmot1				         as char 									no-undo.
def var wmot2				         as char 									no-undo.
def var wcivilite			         as char 									no-undo.
def var wcp     			         as char 									no-undo.
def var wville     		         as char 									no-undo.
def var wadres     		         as char 									no-undo.
def var wdate                    as char                          no-undo.                  
def var wdate2                   as char                          no-undo.                  
def var lOK 				         as log 									no-undo.
def var wval                     as char                          no-undo.                  

def VAR hSAXWriter 			      AS HANDLE 								NO-UNDO.
def VAR hSAXWriter2 		         AS HANDLE 								NO-UNDO.
def VAR hSAXWriter3 		         AS HANDLE 								NO-UNDO.

def var l-domaine                as char                          no-undo .
def var wdomaine                 as char                          no-undo .
def var l-nomenc                 as char                          no-undo .
def var wnomenc                  as char                          no-undo .

/* variables gencod */
def var gen-code-1  as int.
def var gen-code-2  as int.
def var gen-code-3  as int.
def var gen-code-4  as int.
def var gen-code-5  as int.
def var gen-code-6  as int.
def var gen-code-7  as int.
def var gen-code-8  as int.
def var gen-code-9  as int.
def var gen-code-10 as int.
def var gen-code-11 as int.
def var gen-code-12 as int.
def var gen-code-13 as int.
def var gen-pair    as int.
def var gen-impair  as int.
def var gen-gencod  as int.
def var gen-sup     as int.
def var gen-cle     as int.
def var valeur      as dec.
def var gencod-art like codbar.code-barre.



def var wetat                    as char                          no-undo .
def var wrayon                   as char                          no-undo .
def var wpattern                 as char                          no-undo .
def var wcategory                as char                          no-undo .
def var wcodblo                  as char                          no-undo .
def var l-prov                   as char                          no-undo .
def var wprov                    as char                          no-undo .
def var wstatut                  as char                          no-undo .
def var wtype                    as char                          no-undo .
def var wbloUR                   as char                          no-undo .
def var wcodbu                   as char                          no-undo .
def var l-bu-g                   as char                          no-undo .
def var l-bu-s                   as char                          no-undo .
def var l-bu-e                   as char                          no-undo .
def var wreffab                  as char                          no-undo.
/* def var wsoc as char. */

def var wfournisseur             as char                          no-undo .
def var wtarif                   as char                          no-undo .
def var wvaleur                  as dec decimals 3                no-undo .

def var wecom                    as dec decimals 5                no-undo .
def var wd3e                     as dec decimals 5                no-undo .

def var l-art-poubelle as char init "710105,710118,710205,710218,SAV001,004823,710318,710418,710518,710618,739959,739960,739961,710705,710718,737746,737747,730103,712421,779264,711005,733397".


def var wcircuit                 as char extent 5                 no-undo .
def var l-circuit                as char                          no-undo .

/* tables des clients a extraire */
def temp-table t-cli
	field codsoc		like auxapr.codsoc
	field typaux		like auxapr.typaux
	field codaux		like auxapr.codaux
	field rowauxapr     as rowid
	field rowauxili     as rowid
	field env-depot	    like auxapr.env-depot
index i-1 codsoc typaux codaux env-depot
index i-2 codsoc env-depot
.

/* tables des fournisseurs a extraire */
def temp-table t-fou
    field t1codaux     as char
    field t1codauxfac  as char
    field t1soc01      as char
    field t1fer01      as char
    field t1soc08      as char
    field t1fer08      as char
    field t1soc59      as char
    field t1fer59      as char
    field t1soc88      as char
    field t1fer88      as char
    field t1tel        as char
    field t1fax        as char
    field t1gln        as char
    field rowauxili     as rowid
    index i-tempo-1 is unique primary t1codaux.

/* tables des taxes a extraire */  
def var wcodtax  as char extent 2.
def var wvaltax  as dec extent 2.
def var l-tax as char initial "DEEE,DEA".
def var l-taxcyl as char initial "99,98".
def var i-tax as int.

def temp-table t-ecotax
	field wtype as char 
	field wcode as char 
	field wcodecyl as char 
	field wvaleur as dec
index i-1 is primary wtype wcode .


def temp-table t-art no-undo
   field rowart      as rowid 
   field l-gencodsup as char 
   field supgencod   as char
index i-sup supgencod. 

def temp-table t-statut no-undo
   field codsoc      as char 
   field articl      as char 
   field statut      as char
index i-socart codsoc articl. 

def temp-table t-codblo no-undo
   field codsoc       as char 
   field articl       as char 
   field etat         as char
index i-socart codsoc articl. 

   
def temp-table t-tar no-undo
   field rowtar   as rowid 
   field motcle   as char
   field articl   as char 
   field tva      as char
   field free     as char
index i-mot  motcle articl.

def temp-table t-tarach no-undo
   field codsoc   as char
   field articl   as char
   field taux     as dec decimals 3
   field tva      as char
index i-art articl.

def temp-table t-ptslim no-undo
   field soc-cfi   as char
   field no-cfi   as char
   field dat-pts  as date
   field nb-pts   as dec 
index i-clidat soc-cfi no-cfi dat-pts.

/* procedure trt-fou : creation table tempo des fournisseurs */
procedure trt-fou :
   FIND t-fou 
   where t-fou.t1codaux = auxili.codaux
   exclusive-lock no-error .
   if not available t-fou then do :
      CREATE t-fou .
      assign t-fou.t1codaux = auxili.codaux
             t-fou.rowauxili = rowid ( auxili ).
   end.       
   
   
   if t-fou.t1tel = "" then t-fou.t1tel = auxapr.teleph.
   if t-fou.t1fax = "" then t-fou.t1fax = auxapr.fax.    
   
   /*
   if t-fou.t1tel <> auxapr.teleph and auxapr.teleph <> "" then 
      put stream ano unformatted "FO-02-T�lephone diff�rent entre soci�t�s " auxili.codaux "-" t-fou.t1tel "-" wsoc "-" auxapr.teleph skip.
   
   if t-fou.t1fax <> auxapr.fax and auxapr.fax <> "" then 
      put stream ano unformatted "FO-03-Fax       diff�rent entre soci�t�s "  auxili.codaux "-" t-fou.t1fax "-" wsoc "-" auxapr.fax skip.

   if t-fou.t1gln <> auxapr.gencod and auxapr.gencod <> "" then 
      put stream ano unformatted "FO-04-Code GLN  diff�rent entre soci�t�s " auxili.codaux "-" t-fou.t1gln "-" wsoc "-" auxapr.gencod skip.
   */
   
   if auxapr.codsoc = "01" then do:
      t-fou.t1soc01 = "O".
      if auxapr.cpt-ferme = "1" then t-fou.t1fer01 = "F". else t-fou.t1fer01 = "O".     
   end.
   if auxapr.codsoc = "08" then do:
      t-fou.t1soc08 = "O".
      if auxapr.cpt-ferme = "1" then t-fou.t1fer08 = "F". else t-fou.t1fer08 = "O".
   end.
   if auxapr.codsoc = "59" then do:
      t-fou.t1soc59 = "O".
      if auxapr.cpt-ferme = "1" then t-fou.t1fer59 = "F". else t-fou.t1fer59 = "O".   
   end.
   if auxapr.codsoc = soc-fullcyl then do:
      t-fou.t1soc88 = "O".
      if auxapr.cpt-ferme = "1" then t-fou.t1fer88 = "F". else t-fou.t1fer88 = "O".
   end.

   if trim( auxapr.codaux-fac )  <> "" and auxapr.codaux-fac <> auxapr.codaux  then do:      
      if t-fou.t1codauxfac = "" then t-fou.t1codauxfac = auxapr.codaux-fac.
      find aux-fac 
      where aux-fac.codsoc = auxapr.codsoc
      and   aux-fac.typaux = "TIE"
      and   aux-fac.codaux = auxapr.codaux-fac
      no-lock no-error.
      if available aux-fac then do:
         if t-fou.t1gln = "" then t-fou.t1gln = aux-fac.gencod. 
      end.
   end.
   else do:
   
      if t-fou.t1gln = "" then t-fou.t1gln = auxapr.gencod. 
   end.
end.


/* procedure trt-cli : creation table tempo des clients */

procedure trt-cli :

   /* table des clients */ 
	find t-cli
	where t-cli.codsoc = auxapr.codsoc
	and   t-cli.typaux = auxapr.typaux
	and   t-cli.codaux = auxapr.codaux
	no-error.
	if not available t-cli then do:
		create 	t-cli.
		assign 	t-cli.codsoc = auxapr.codsoc
				t-cli.typaux = auxapr.typaux
				t-cli.codaux = auxapr.codaux
				t-cli.rowauxapr = rowid ( auxapr )
				t-cli.rowauxili = rowid ( auxili )
				t-cli.env-depot = auxapr.env-depot.
	end.

end.

/* procedure trt-art : creation table tempo des articles */
procedure trt-art :
   find artapr
   where artapr.codsoc = soc-art-grp /* DGR 12022019 codbar.codsoc */
   and   artapr.articl = warticl
   no-lock no-error.
   if not available artapr then next.
   if artapr.famart2 = "ZZZ" or artapr.soufam2 = "ZZZ" or  artapr.sssfam2 = "ZZ" then next .      
   
   wnomenc = artapr.famart2 + artapr.soufam2 + trim ( artapr.sssfam2 ) .
   if lookup( wnomenc , l-nomenc ) = 0 then next.
   
   find t-art
   where t-art.rowart = rowid ( artapr )
   no-error.
   if available t-art then next .
   if not available t-art then do:
      create t-art.
      assign t-art.rowart = rowid ( artapr ).
   end.
end.

/* procedure trt-art : creation table tempo des articles */
procedure trt-tarach :
   if available artapr then do :
      find t-tarach
      where t-tarach.articl = artapr.articl
      no-error.
      if not available t-tarach then do:
         /* le taux de remise est a 0 d'office pour tous les produits */
         create t-tarach.
         assign t-tarach.articl = artapr.articl
               t-tarach.tva = artapr.tva
               t-tarach.taux = 0.
         
         /* si la famille de l'article fait partie des familles interdites en remise => taux = 0 */ 
         if lookup ( artapr.famart2 , l-famnorem ) <> 0 then do:
            assign t-tarach.taux = 0.
         end.
         else do:
            /* on verifie que l'article est autoris� en remise exc */
            find tabges 
            where tabges.codsoc = artapr.codsoc
            and   tabges.etabli = ""
            and   tabges.typtab = "ART"
            and   tabges.prefix = "SUIT-FICHE"
            and   tabges.codtab = artapr.articl
            no-lock no-error.
            if available tabges and tabges.libel3[2] = "1" then t-tarach.taux = 0.  /* non remisable */
            else do:
               /* familles dont les taux de remises sont a la sous sous famille 2 */
               if lookup ( artapr.famart2, l-famremssf) <> 0 then do:
                  find tabges 
                  where tabges.codsoc = ""
                  and   tabges.etabli = ""
                  and   tabges.typtab = "ART"
                  and   tabges.prefix = "SSF2" + artapr.famart2 + artapr.soufam2  
                  and   trim ( tabges.codtab ) = trim ( artapr.sssfam2 )
                  no-lock no-error.
                  if available tabges  then t-tarach.taux = tabges.nombre [1].
               end.
               /* sinon les taux sont a la sous famille 2 */ 
               else do:
                  find tabges 
                  where tabges.codsoc = ""
                  and   tabges.etabli = ""
                  and   tabges.typtab = "ART"
                  and   tabges.prefix = "SOUFAM2" + artapr.famart2 
                  and   trim ( tabges.codtab ) = artapr.soufam2  
                  no-lock no-error.
                  if available tabges then t-tarach.taux = tabges.nombre [1].
               end.
            end.
         end. 
      end.
      if lookup( soc-trt, t-tarach.codsoc) = 0 then do:
         if t-tarach.codsoc = "" then t-tarach.codsoc = soc-trt.
                                 else t-tarach.codsoc = t-tarach.codsoc + "," + soc-trt.
      end.
   end.
end.
