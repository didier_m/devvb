/* ------------------------------------------------------------------------- */
/* duplication des creations / modification tarifs                           */
/* de la societe 01 sur les societes 05 08 pour la famille 15                */
/* de la societe 01 sur la societe 05 pour les autres familles               */
/* ------------------------------------------------------------------------- */
/* dup-tarifc.i : creation                                                   */
/* ------------------------------------------------------------------------- */
/* DGR : 21112013 : on conserve opemaj de 01 (pour TVA notamment)            */

create b-{1}.
buffer-copy {1}
  except {1}.codsoc
         {1}.datmaj
         {1}.heumaj
/*         {1}.opemaj */
         {1}.datcre
         {1}.heucre
         {1}.opecre
to b-{1}
  assign b-{1}.codsoc = wcodsoc
         b-{1}.datcre = wdatmaj
         b-{1}.heucre = wheumaj
         b-{1}.opecre = wopemaj
         b-{1}.datmaj = wdatmaj
         b-{1}.heumaj = wheumaj
/*         b-{1}.opemaj = wopemaj */ .
