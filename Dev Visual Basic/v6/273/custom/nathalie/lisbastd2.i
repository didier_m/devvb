/* include   lisbastd2.i Liste des mvts d'un article
             include du for each selon bastat.magasin ou bastat.mag-respon
*/

/* -------------------------------------------------------------------------- */
/* Acces parametre lecture societe auxili  ==> OK                             */
/* -------------------------------------------------------------------------- */

for each bastat where
         bastat.codsoc = scodsoc
     and bastat.articl = sarticl
     and (smotcle      = " " or smotcle      = bastat.motcle)
     and (stypcom[01]  = " " or stypcom[01]  = bastat.typcom
                             or stypcom[02]  = bastat.typcom
                             or stypcom[03]  = bastat.typcom
                             or stypcom[04]  = bastat.typcom
                             or stypcom[05]  = bastat.typcom
                             or stypcom[06]  = bastat.typcom
                             or stypcom[07]  = bastat.typcom
                             or stypcom[08]  = bastat.typcom
                             or stypcom[09]  = bastat.typcom
                             or stypcom[10]  = bastat.typcom)
     and (smagasin[01] = " "

      or (stypmag = "l" and smagasin[01] = bastat.magasin)
      or (stypmag = "l" and smagasin[02] = bastat.magasin)
      or (stypmag = "l" and smagasin[03] = bastat.magasin)
      or (stypmag = "l" and smagasin[04] = bastat.magasin)
      or (stypmag = "l" and smagasin[05] = bastat.magasin)
      or (stypmag = "l" and smagasin[06] = bastat.magasin)
      or (stypmag = "l" and smagasin[07] = bastat.magasin)
      or (stypmag = "l" and smagasin[08] = bastat.magasin)
      or (stypmag = "l" and smagasin[09] = bastat.magasin)
      or (stypmag = "l" and smagasin[10] = bastat.magasin)

      or (stypmag = "r" and smagasin[01] = bastat.mag-respon)
      or (stypmag = "r" and smagasin[02] = bastat.mag-respon)
      or (stypmag = "r" and smagasin[03] = bastat.mag-respon)
      or (stypmag = "r" and smagasin[04] = bastat.mag-respon)
      or (stypmag = "r" and smagasin[05] = bastat.mag-respon)
      or (stypmag = "r" and smagasin[06] = bastat.mag-respon)
      or (stypmag = "r" and smagasin[07] = bastat.mag-respon)
      or (stypmag = "r" and smagasin[08] = bastat.mag-respon)
      or (stypmag = "r" and smagasin[09] = bastat.mag-respon)
      or (stypmag = "r" and smagasin[10] = bastat.mag-respon))

     and ((stypdt = "facture" and sdtmin <= bastat.datfac
                              and sdtmax >= bastat.datfac)
       or (stypdt = "depart"  and sdtmin <= bastat.datdep
                              and sdtmax >= bastat.datdep))
         use-index article
         no-lock  ,
    each auxili where
         auxili.codsoc = auxili-soc
     and auxili.typaux = "tie"
     and auxili.codaux = bastat.codaux
         no-lock
break by {&mag}
      by bastat.datdep
      by bastat.motcle
      by bastat.typcom
      by bastat.numbon    : /* for each */
   if first-of({&mag}) then do: /* entete magasin */
      wmagqte = 0. wmagfac-ht = 0.
   end. /* entete magasin */
   if wcptlig > wpage-size then do: /* entete page */
      wcptpag = wcptpag + 1. wcptlig = 8.
      put stream edition wsaut skip.
      put stream edition
          hp " " today ", lisbastd2.p, " sedifile ", std172nr.maq, societe "
          codsoc-soc " " libsoc-soc ", page " wcptpag skip.
      put stream edition
          "Mouvements " smotcle " de " sarticl " " sdsart
          ", dates " stypdt " du " sdtmin " au " sdtmax
          ", selection tri et cumul par magasin " wlibtypmag
          skip.
      put stream edition
          "mag res mot typ date liv mod tra "
          "date bon numero bon rep "
          "date fac numero facture  mod date ech t/c client nom"
          "qte        mt ht        prix" at 149
          skip.
      put stream edition
          "liv" at 26
          "reg" at 83
          skip.
      put stream edition " " skip.
   end. /* entete page */
   wcptlig = wcptlig + 1.
   wbonqte = bastat.qte. wbonfac-ht = bastat.fac-ht.
   if bastat.motcle = "vte" then do :
      wbonqte = - wbonqte. wbonfac-ht = - wbonfac-ht.
   end.
   wprix = 0. if wbonqte <> 0 then wprix = wbonfac-ht / wbonqte.
   wcodaux = substring(bastat.codaux,5,6).
   put stream edition
       bastat.magasin                       " "
       bastat.mag-respon                    " "
       bastat.motcle                        " "
       bastat.typcom                        " "
       bastat.datdep   format "99/99/99"    " "
       bastat.modliv                        " "
       bastat.transp                        " "
       bastat.datbon   format "99/99/99"    " "
       bastat.numbon                        " "
       bastat.repres                        " "
       bastat.datfac   format "99/99/99"    " "
       bastat.numfac                        " "
       bastat.modreg                        " "
       bastat.datech   format "99/99/99"    " "
       bastat.typbon                        " "
       wcodaux                              " "
       auxili.adres[1]                      " "
       wbonqte
       wbonfac-ht
       wprix

       skip.
   ssocqte = ssocqte + bastat.qte. ssocfac-ht = ssocfac-ht + bastat.fac-ht.
   wsocqte = wsocqte + bastat.qte. wsocfac-ht = wsocfac-ht + bastat.fac-ht.
   wmagqte = wmagqte + bastat.qte. wmagfac-ht = wmagfac-ht + bastat.fac-ht.
   if last-of({&mag}) then do: /* pied magasin */
      wprix = 0. if wmagqte <> 0 then wprix = wmagfac-ht / wmagqte.
      put stream edition
          "total magasin" at 107
          wmagqte         at 140
          wmagfac-ht      at 153
          wprix           at 166
          skip.
      put stream edition " " skip.
      wcptlig = wcptlig + 2.
   end. /* pied magasin */
end. /* for each */

