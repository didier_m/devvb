/* ------------------------------------------------------------------------- */
/* Extraction des stocks excel                                               */
/* ------------------------------------------------------------------------- */
/* stoall0.p : module lanceur                                                */
/* ------------------------------------------------------------------------- */

{ connect.i      }
{ bat-ini.i  new }

/*----------------------------*/
/* Chargement du fichier aide */
/*----------------------------*/

def var l-soc         as char format "x(17)" init "01,08,42,45,58,59".
def var wmag          as char format "x(6)" init "DETAIL".
def var l-fam         as char format "x(24)".
def var l-exe         as char format "x(14)".

def var sequence      as char                             no-undo.
def var sel-sequence  as char                             no-undo.
def var periph        as char        format "x(15)"       no-undo.

assign  codsoc = ""
        etabli = ""
        typtab = "OPE"
        prefix = "NUME-SPOOL"
        codtab = operat .

run opeseq.p ( "stoall" , output  sequence, output sel-sequence) .
periph = sel-sequence.

/* affichage ecran standard */
{ fr-cadre.i "'Extraction Excel des stocks'" }

/*------------------------*/
/* saisie des donn�es     */
/*------------------------*/
  disp "Societe (01,08,42,45,58,59)             : " l-soc no-label skip
       "Magasin (CUMUL ou DETAIL ou 1 Magasin   : " wmag no-label skip
       "Famille (xxx,yyy, 6max ou Rien = toutes : " l-fam no-label skip
       "Exercice (2010,2011,2012 3 ans maxi)    : " l-exe no-label
  with frame toto centered.

  update l-soc with frame toto.
  if l-soc = "" then do:
    message "Societe incorrecte".
    bell. bell.
    readkey pause 2.
    undo, retry.
  end.
  
  update wmag with frame toto.
  if wmag = "" then do:
    message "Magasin incorrect".
    bell. bell.
    readkey pause 2.
    undo, retry.
  end.
  
  update l-fam with frame toto.
  update l-exe with frame toto.
  if l-exe = "" then do:
    message "Exercice incorrect".
    bell. bell.
    readkey pause 2.
    undo, retry.
  end.	

  batch-tenu = yes. /* Traitement en Batch */
        { bat-zon.i "l-soc"      "l-soc"      "c" "''" "''" }
        { bat-zon.i "wmag"       "wmag"       "c" "''" "''" }
        { bat-zon.i "l-fam"      "l-fam"      "c" "''" "''" }
        { bat-zon.i "l-exe"      "l-exe"      "c" "''" "''" }
        { bat-zon.i "periph"     "periph"     "c" "''" "''" }
        { bat-maj.i  "stoall.p" }

hide frame fr-titre no-pause.
hide frame fr-cadre no-pause.
