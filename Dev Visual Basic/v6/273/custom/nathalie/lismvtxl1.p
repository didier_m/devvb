/* lismvtxl1.p Extraction des mouvements  => Extraction des mouvements */
/*   modif DGR 241104 pour V52                                                */

/* -------------------------------------------------------------------------- */
/* Acces parametre lecture societe article ==> OK                             */
/* Acces parametre lecture societe auxili  ==> OK                             */
/* -------------------------------------------------------------------------- */

{ connect.i }
{ lismvtxl0.i new }
{ bat-ini.i  new }
{ top-asec.i}

def input parameter  fichier  as char format "x(12)" .

def var wtax as dec decimals 5 extent 2.
def var articl-soc    like artapr.codsoc.
def var auxili-soc    like auxili.codsoc.
def var type-tiers    like auxili.typaux init "TIE".

/* Chargement parametres selection */

run bat-lec.p ( fichier ) .
{ bat-cha.i  "lib-prog"     "lib-prog"      "a" }
{ bat-cha.i  "stypdt"       "stypdt"        "a" }
{ bat-cha.i  "sdtmin"       "sdtmin"        "d" }
{ bat-cha.i  "sdtmax"       "sdtmax"        "d" }
{ bat-cha.i  "wmot"         "wmot"          "a" }
{ bat-cha.i  "wtyp"         "wtyp"          "a" }
{ bat-cha.i  "wmag"         "wmag"          "a" }
{ bat-cha.i  "top-extract"  "top-extract"   "a" }
{ bat-cha.i  "sel-sequence" "sel-sequence"  "a" }

regs-app = "ELODIE" .
{ regs-cha.i }

regs-app = "NATHALIE" .
{ regs-cha.i }

{ regs-inc.i "articl" }
articl-soc = regs-soc .

regs-fileacc = "AUXILI" + type-tiers.
{regs-rec.i}
auxili-soc = regs-soc .

output to value(sel-sequence) .

/* if top-asec = "O" then socauxili = "08".
else socauxili = "01". */

export delimiter ";"
  "Societe"
  "Motcle"
  "Magasin"
  "Mag Respon"
  "Typcom"
  "Numbon"
  "No Facture"
  "No Ligne Bon"
  "Nivcom"
  "client cde"
  "Nom client cde"
  "Client facture"
  "Nom client fac"
  "Client livre"
  "Nom client liv"
  "Nom liv spe alpha"
  "Nom liv spe code"
  "Date liv"
  "Date fact"
  "Code Article"
  "Designation"
  "Famille"
  "Sous Fam"
  "Sous Sous F"
  "Quantite"
  "PU net"
  "Montant HT"
  "montant TTC"
  "taxe HT"
  "taxe TTC"
  "total HT" 
  "total TTC" .

  if top-extract = "1" or top-extract = "3" then do:
    for each lignes
    where lignes.codsoc = codsoc-soc
    and   (wmot = " " or wmot = lignes.motcle)
    and   (wtyp = " " or wtyp = lignes.typcom )
    and   ((stypdt = "fac" and
            sdtmin <= lignes.datfac and
            sdtmax >= lignes.datfac)
        or (stypdt = "liv"  and
            sdtmin <= lignes.datdep
            and sdtmax >= lignes.datdep)
        or (stypdt = "xxx"  and
             (sdtmin <= lignes.datdep and
              sdtmax >= lignes.datdep)
             or (sdtmin <= lignes.datfac and
                 sdtmax >= lignes.datfac) ) )
    and   (wmag = " " or wmag = lignes.magasin )
    and   lignes.nivcom >= "3"
    and   lignes.nivcom < "7"
    and   lignes.codlig  <> "T"
    /* use-index primaire */ no-lock :

      find artapr
      where artapr.codsoc = articl-soc
      and   artapr.articl = lignes.articl
      no-lock no-error.

      if not available artapr then do:
        wlib = "? ? ?".
        wfam = "?".
        wsfa = "?".
        wssf = "?".
      end.
      else do:
        wlib = artapr.libart1[1].
        wfam = artapr.famart.
        wsfa = artapr.soufam.
        wssf = artapr.sssfam.
      end.
      wmont = lignes.htnet.
      warticl = caps(lignes.articl).

      assign wmag-respon = "?".
      do k = 1 to 3:
        wnom[k] = "?".
      end.

      find entete
      where entete.codsoc = lignes.codsoc
      and   entete.motcle = lignes.motcle
      and   entete.typcom = ligneS.typcom
      and   entete.numbon = lignes.numbon
      no-lock no-error.
      if available entete then do:
        wmag-respon = entete.mag-respon.
        l-codaux = entete.codaux + ","
                 + entete.codaux-fac + ","
                 + (if entete.codaux-liv = "" then entete.codaux
                                             else entete.codaux-liv).
      end.

      do k = 1 to num-entries(l-codaux):
        wcodaux = entry(k,l-codaux).
        if wcodaux = "" then next.

        find auxili
        where auxili.codsoc = auxili-soc
        and  auxili.typaux = "TIE"
        and  auxili.codaux = wcodaux
        no-lock no-error.
        if available auxili then do:
          assign wnom[k] = auxili.adres[1]  .
        end.
        if k = 3 then do:
          if auxili-soc <> "08" and
           (wcodaux begins "    125" or wcodaux begins "    025" ) then do:
            Find magasi
            where magasi.codsoc = ""
            and   magasi.codtab = substr(wcodaux,8,3)
            no-lock no-error.
            if available magasi then do:
              wnom[k + 1 ] = magasi.libel1[1] + " " + wcodaux.
              wnom[k + 2] = wcodaux + " " + magasi.libel1[1] .
            end.
            next.
          end.
          else do:
            wnom[k + 1 ] = auxili.adres[1] + " " + wcodaux.
            wnom[k + 2] = wcodaux + " " + auxili.adres[1] .
          end.
        end.
      end.
      wtax = 0.
      for each ligspe
      where ligspe.codsoc =  lignes.codsoc
      and   ligspe.motcle    = lignes.motcle + "/TAX"
      and   ligspe.typcom    = lignes.typcom
      and   ligspe.numbon    = lignes.numbon
      and   ligspe.chrono    = lignes.chrono
      no-lock:
         wtax[1] = wtax[1] + ligspe.dec-2.
         wtax[2] = wtax[2] + ligspe.dec-3.
      end.
      
      export delimiter ";"
        lignes.codsoc
        lignes.motcle
        lignes.magasin
        wmag-respon
        lignes.typcom
        lignes.numbon format "x(12)"
        lignes.numfac
        lignes.chrono
        lignes.nivcom
        lignes.codaux
        wnom[1]
        entry(2,l-codaux)
        wnom[2]
        entry(3,l-codaux)
        wnom[3]
        wnom[4]
        wnom[5]
        lignes.datdep
        lignes.datfac
        warticl format "X(6)"
        wlib
        wfam
        wsfa
        wssf
        lignes.qte[2]
        lignes.pu-net
        wmont
        lignes.fac-ttc
        wtax
        wmont + wtax[1] 
        lignes.fac-ttc + wtax[2] .

    end. /* for each lignes */
  end. /* if top-extract = 1 or 3 */

  if top-extract = "2" or top-extract = "3" then do:
    for each bastat
    where bastat.codsoc = codsoc-soc
    and   ( wmot = " " or wmot = bastat.motcle )
    and   ( wtyp = " " or wtyp = bastat.typcom )
    and   ((stypdt = "fac" and
            sdtmin <= bastat.datfac and
            sdtmax >= bastat.datfac)
            or (stypdt = "liv"  and
                sdtmin <= bastat.datdep and
                sdtmax >= bastat.datdep)
            or (stypdt = "xxx"  and
               (sdtmin <= bastat.datdep and
                sdtmax >= bastat.datdep)
                or (sdtmin <= bastat.datfac and
                    sdtmax >= bastat.datfac) ) )
    and   ( wmag = " " or wmag = bastat.magasin )
    /* use-index primaire */ no-lock :

      find artapr
      where artapr.codsoc = articl-soc
      and   artapr.articl = bastat.articl
      no-lock no-error.

      if not available artapr then do:
        wlib = "? ? ?".
        wfam = "?".
        wsfa = "?".
        wssf = "?".
      end.
      else do:
        wlib = artapr.libart1[1].
        wfam = artapr.famart.
        wsfa = artapr.soufam.
        wssf = artapr.sssfam.
      end.
      wmont = fac-ht.
      warticl = caps(bastat.articl).

      do k = 1 to 3:
        wnom[k] = "?".
      end.
      l-codaux = bastat.codaux + ","
               + bastat.codaux-fac + ","
               + bastat.codaux .

      do k = 1 to num-entries(l-codaux):
        wcodaux = entry(k,l-codaux).
        if wcodaux = "" then next.

        find auxili
        where auxili.codsoc = auxili-soc
        and  auxili.typaux = "TIE"
        and  auxili.codaux = wcodaux
        no-lock no-error.
        if available auxili then do:
          assign wnom[k] = auxili.adres[1]  .
        end.
        if k = 3 then do:
          if auxili-soc <> "08" and
           (wcodaux begins "    125" or wcodaux begins "    025" ) then do:
            Find magasi
            where magasi.codsoc = ""
            and   magasi.codtab = substr(wcodaux,8,3)
            no-lock no-error.
            if available magasi then do:
              wnom[k + 1 ] = magasi.libel1[1] + " " + wcodaux.
              wnom[k + 2] = wcodaux + " " + magasi.libel1[1] .
            end.
            next.
          end.
          else do:
            wnom[k + 1 ] = auxili.adres[1] + " " + wcodaux.
            wnom[k + 2] = wcodaux + " " + auxili.adres[1] .
          end.
        end.
      end.

      do k = 1 to num-entries(l-codaux):
        wcodaux = entry(k,l-codaux).
        if wcodaux = "" then next.
        find auxili
        where auxili.codsoc = auxili-soc
        and  auxili.typaux = "TIE"
        and  auxili.codaux = wcodaux
        no-lock no-error.
        if available auxili then
          assign wnom[k] = auxili.adres[1]  .
      end.

      wtax = 0.
      for each ligspe
      where ligspe.codsoc    = bastat.codsoc
      and   ligspe.motcle    = bastat.motcle + "/TAX"
      and   ligspe.typcom    = bastat.typcom
      and   ligspe.numbon    = bastat.numbon
      and   ligspe.chrono    = bastat.chrono
      no-lock:
         wtax[1] = wtax[1] + ligspe.dec-2.
         wtax[2] = wtax[2] + ligspe.dec-3.
      end.

      export delimiter ";"
        bastat.codsoc
        bastat.motcle
        bastat.magasin
        bastat.mag-respon
        bastat.typcom
        bastat.numbon format "x(12)"
        bastat.numfac
        bastat.chrono
        "7"
        bastat.codaux
        wnom[1]
        bastat.codaux-fac
        wnom[2]
        bastat.codaux /* -liv */
        wnom[3]
        wnom[4]
        wnom[5]
        bastat.datdep
        bastat.datfac
        warticl
        wlib
        wfam
        wsfa
        wssf
        bastat.qte
        "0"
        wmont
        bastat.fac-ttc
        wtax
        wmont + wtax [1]
        bastat.fac-ttc + wtax [2]
        .
        
    end. /* for each bastat */
  end. /* if top-extract = 2 or 3 */
output close.