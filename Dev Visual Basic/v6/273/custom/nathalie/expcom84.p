/* Prog : expcom84.p Extraction Fact + regl pour commissions Repr�sentants  */
/*                                                                          */
/* 10/10/2013 cco Creation                                                  */
/* 19/11/2015 cco Ajout type 91                                             */
/* 30/11/2015 cco Ajout type CEE                                            */
/*                                                                          */

{connect.i}
{ bat-ini.i  new }

def input parameter  fichier  as char format "x(12)" .

def stream fentete.
def stream flignes.
def stream fimputr.
def stream flog.

def var sel-sequence   as char              format "x(12)"            no-undo.
def var sequence       as char              format "x(5)"             no-undo.
def var filcome        as char              format "x(12)"            no-undo.
def var filcoml        as char              format "x(12)"            no-undo.
def var filcomi        as char              format "x(12)"            no-undo.
def var sedifil2       as char              format "x(12)"            no-undo.
def var wok            as char format "x".
def var wsoc           as char initial "84".
def var l-cpt          as char initial "411100".

/*
05  Vir Recus
02  CHQ emis
31  Effet � rec
04  vers especes
91  OD Regul          Ajout le 19/11/15
CEE Cheque a echeance Ajout le 30/11/15
A1  SEPA              Ajout le 29/11/16 EMOGE
*/

def var l-typ   as char initial "05,02,31,04,91,CEE,A1". 
def var wcpt    as char. 
def var aa      as int.
def var sav-codsoc as char.
def var trace   as char initial "O".
def var wtopreg as char.

def var auxili-soc as char.

def var wtypact as char format "X".
def var wcatecr as char format "XXX".
def var wsens   as char format "X".
def var wpiec   as char format "X(14)".
def var wdatp   as date.
def var wdate   as date.
def var wexe    as char format "XXXX".
def var wper    as char format "XXX".
def var wcresul as char. 
def var wexperf as char.
def var wexpers as char.

def var wmont   as dec .
def var savsoc  as char.
def var wtvaintra as char format "x(25)".
def var nbecr   as int.
def var nblus   as int.
def var nbdel1  as int.
def var wnext   as char.
def var nbtab   as int.
def var wdev    as char.
def var wmodreg as char.
def var wnumreg as dec decimals 0.


def temp-table t-tempo-1
    field t1codsoc  as char
    field t1codaux  as char
    field t1docume  as char
    field t1chrono  as char   
    field t1codlet  as char
    field t1codgen  as char
    field t1piece   as char
    field t1datpie  as date
    field t1devcpt  as char 
    field t1modreg  as char 
    field t1db      as dec decimals 3
    field t1cr      as dec decimals 3
    field t1typie   as char
    field t1next    as char
    field t1datech  as date
    index i-tempo-1 is unique primary t1codsoc t1codaux t1docume t1chrono.

def var totdb  as dec decimals 3.
def var totcr  as dec decimals 3.
def var wdbcr  as dec decimals 3.

def var totreg as dec decimals 3.
def var wmil   as char.
def var wdateff as     date. 

run bat-lec.p ( fichier ) .

{ bat-cha.i  "wsoc"       "wsoc"       "a"  }
{ bat-cha.i  "wexe"       "wexe"       "a"  }
{ bat-cha.i  "wper"       "wper"       "a"  }
{ bat-cha.i  "filcome"    "filcome"    "a"  }
{ bat-cha.i  "filcoml"    "filcoml"    "a"  }
{ bat-cha.i  "filcomi"    "filcomi"    "a"  }
{ bat-cha.i  "sedifil2"   "sedifil2"   "a"  }

message wsoc wexe wper filcome filcoml filcomi sedifil2
 view-as alert-box.

wexpers = wexe + wper.

output stream fentete to value(filcome).
output stream flignes to value(filcoml).
output stream fimputr to value(filcomi).
output stream flog    to value(sedifil2).

put stream flog "D�but programme expcom84.p " 
                string ( today , "99/99/9999" ) " - " 
                string( time, "hh:mm:ss" ) skip.
put stream flog " " skip.
put stream flog "Exercice : " wexe skip.
put stream flog "Periode  : " wper skip.
put stream flog " " skip.

/* Si besoin en test

if wexe = "2014" and wper = "001" then do: 
   for each tabges where 
            tabges.codsoc = wsoc
        and tabges.etabli = ""
        and tabges.typtab = "MDC" 
        and tabges.prefix = "REGLEMENT" 
        exclusive-lock:
   delete tabges.
   nbdel1 = nbdel1 + 1.
   end.

   message "Delete tabges MDC " nbdel1. pause.

end.
*/

sav-codsoc = codsoc-soc.
codsoc-soc = wsoc.

regs-app = "ELODIE" .
{ regs-cha.i }

regs-fileacc = "AUXILI" + "TIE".
{regs-rec.i}
auxili-soc = regs-soc .

for each auxili where
    auxili.codsoc = auxili-soc and
    auxili.typaux = "TIE"
    /* and auxili.codaux begins "PASQUI"
    */
    no-lock:

do aa = 1 to num-entries( l-cpt ) :

wcpt = entry( aa, l-cpt ) .

for each mvtcpt where
    mvtcpt.codsoc = wsoc and
    mvtcpt.codgen = wcpt and
    mvtcpt.typaux = auxili.typaux and
    mvtcpt.codaux = auxili.codaux and
    mvtcpt.devpie = "EUR"     and
    mvtcpt.top-lettrage = "2" and
    mvtcpt.execpt <= wexe
/* Index top-lettrage 
CODSOC
CODGEN
TYPAUX
CODAUX
DEVPIE
TOP-LETTRAGE
COD-LETTRAGE
ECHEANCE
*/
    no-lock use-index top-lettrage break by cod-lettrage by typie by datpie descending:


if first-of(mvtcpt.cod-lettrage) then do:

   /* vidage de la table lettrage */

   empty temp-table t-tempo-1.
   totdb = 0.
   totcr = 0.
   if trace = "O" then put stream flog "Vidage de la table let " mvtcpt.codaux " " mvtcpt.cod-lettrage skip. 

end.

nblus = nblus + 1.

/* remplisage table lettrage */

if trace = "O" then put stream flog "Remplissage de la table let " mvtcpt.codaux " " mvtcpt.cod-lettrage skip.

wexperf = mvtcpt.execpt + mvtcpt.percpt.
if wexperf > wexpers then wnext = "O". else wnext = "N".

FIND T-tempo-1 where 
           t-tempo-1.t1codsoc    = mvtcpt.codsoc
     and   t-tempo-1.t1codaux    = mvtcpt.codaux
     and   t-tempo-1.t1docume    = mvtcpt.docume
     and   t-tempo-1.t1chrono    = mvtcpt.chrono
     exclusive-lock no-error .
                                                                             
if not available t-tempo-1
     then do :
     CREATE T-tempo-1 .
     assign t-tempo-1.t1codsoc    = mvtcpt.codsoc
            t-tempo-1.t1codaux    = mvtcpt.codaux
            t-tempo-1.t1docume    = mvtcpt.docume
            t-tempo-1.t1chrono    = mvtcpt.chrono
            t-tempo-1.t1codlet    = mvtcpt.cod-lettrage
            t-tempo-1.t1typie     = mvtcpt.typie
            t-tempo-1.t1next      = wnext
            t-tempo-1.t1db        = mvtcpt.dt-devcpt
            t-tempo-1.t1cr        = mvtcpt.ct-devcpt
            t-tempo-1.t1codgen    = mvtcpt.codgen  
            t-tempo-1.t1piece     = mvtcpt.piece     
            t-tempo-1.t1datpie    = mvtcpt.datpie
            t-tempo-1.t1devcpt    = mvtcpt.devcpt  
            t-tempo-1.t1modreg    = mvtcpt.modreg  
            t-tempo-1.t1datech    = mvtcpt.echeance     
            .
     if trace = "O" then put stream flog "Ecriture de la table let " mvtcpt.codaux " " mvtcpt.docume " " mvtcpt.chrono skip. 
     end.
     else do:
          message "Anomalie chargement table " mvtcpt.codaux.
          pause.
          end.

if last-of(mvtcpt.cod-lettrage) then do:

/* traitement de la table lettrage */

   wtopreg = "N".
   wcresul = "".
   
   for each t-tempo-1 where t-tempo-1.t1next = "N" no-lock:

   if lookup(t-tempo-1.t1typie,l-typ) <> 0 and t1cr <> 0 then do:
   
      wcresul = trim(t-tempo-1.t1codaux) + t-tempo-1.t1docume + t-tempo-1.t1chrono .

      find tabges where 
                tabges.codsoc = wsoc
         and    tabges.etabli = ""
         and    tabges.typtab = "MDC"
         and    tabges.prefix = "REGLEMENT"
         and    tabges.codtab = wcresul 
         exclusive-lock no-error.
      if available tabges and  wtopreg = "N" then 
         do:   
         wtopreg = "O".
         end.
         
   end.   /* if lookup(t-tempo-1.t1typie,l-typ) <> 0 and t1cr <> 0 */   

   totdb = totdb + t-tempo-1.t1db.
   totcr = totcr + t-tempo-1.t1cr.

   end.   /* for each t-tempo-1 */

   if trace = "O" then put stream flog "Montant total DB et CR pour Voir " totdb totcr skip.  
   
   if totdb <> totcr then do:
      if trace = "O" then put stream flog "... Sortie DB diff CR " mvtcpt.codaux " " mvtcpt.docume " " mvtcpt.chrono skip.  
   end.   
   else if wtopreg = "O" then do:
           if trace = "O" then put stream flog "... Reglement deja pris en compte " mvtcpt.codaux " " mvtcpt.docume " " mvtcpt.chrono skip.
        end.
        else if wcresul <> "" then do:
                create tabges.
                assign tabges.codsoc = wsoc
                       tabges.etabli = ""
                       tabges.typtab = "MDC"
                       tabges.prefix = "REGLEMENT"
                       tabges.codtab = wcresul
                       tabges.datcre = today.
                if trace = "O" then put stream flog unformatted "Ecriture dans la table Tabges-MDC-REGLEMENT.. " wcresul skip.
                run ecr-reglement.
             end.
   
end.   /* if last-of(mvtcpt.cod-lettrage) */

end.

end.

end.

codsoc-soc = sav-codsoc.

message "Nombre Enreg. Lus " nblus " Ecrits " nbecr . pause.

put stream flog " Compteurs lecture" skip.

put stream flog 
 "- Nombre Enregistrements Lus      " nblus  skip
 "- Nombre Enregistrements Ecrits   " nbecr  skip
 .
put stream flog " " skip.
put stream flog "Fin programme expcom84.p " 
                string ( today , "99/99/9999" ) " - " 
                string( time, "hh:mm:ss" ) skip.

Procedure ecr-reglement:

totreg = 0.
nbtab  = 0.
wnumreg = 0.

for each t-tempo-1 where t-tempo-1.t1next = "N" 
    no-lock by t1typie by t1cr descending :

/* Ecriture lignes de reglement */
if lookup(t1typie,l-typ) <> 0 and t1cr <> 0 then do:
   nbtab = nbtab + 1.
   if nbtab = 1 then 
      wnumreg = dec( trim(t1docume) + trim(t1chrono) ) .

   if trace = "O" then put stream flog unformatted "Ecriture lignes de REGLEMENT. Type piece=" t1typie " Datech " t1datech " Datpie " t1datpie skip.
   
   /* Modif du 19/11/2013 par cco */
   if t1typie = "31" then wdateff = t1datech.
                     else wdateff = t1datpie.
   put stream flignes unformatted
           wnumreg ';'
           string(nbtab, "999") ';"'
           t1codgen '";"'
           t1codaux '";"'
           t1piece  '";'
           wdateff format "99/99/9999" ';'
           t1cr 
           skip.
   totreg  = totreg + t1cr.
   wmodreg = t1modreg.
   wdev    = t1devcpt.
end.
   
/* Ecriture lignes imputation reglement */
else do:
     if t1db <> 0 then wdbcr = t1db.
                  else wdbcr = t1cr * -1. 
     put stream fimputr unformatted
                wnumreg ';'
                string(nbtab, "999") ';"'
                t1piece  '";'
                wdbcr
                skip.
end.   

end.

/* Ecriture entete de reglement */

if wmodreg = "" then do:
   put stream flog unformatted "Mode de r�glement non renseign� " wmodreg " " wnumreg skip.
   wmodreg = "CHH" .   
end.

put stream fentete unformatted
           wnumreg  ';"'
           wmodreg '";"'
           wdev    '";'
           totreg
           skip.

nbecr = nbecr + 1.

end procedure.
