
find entete where
         entete.codsoc = lignes.codsoc
     and entete.motcle = lignes.motcle
     and entete.typcom = lignes.typcom
     and entete.numbon=  lignes.numbon
     no-lock.
if not available entete then do:
   message "Entete inexistant " lignes.typcom lignes.numbon . pause.
   next.
   end.
   
/* lecture de la SD VTE CDC */
wmag = "".
wtypbon = "".
do aa = 1 to 5:
wadres [aa] = "".
end.
wcodcli = "".

FIND LIGSPE  where ligspe.codsoc    = lignes.codsoc
                   and   ligspe.motcle    = lignes.motcle + "/BED"
                   and   ligspe.typcom    = lignes.typcom
                   and   ligspe.numbon    = lignes.numbon
                   and   ligspe.chrono    = lignes.chrono
                   and   ligspe.ss-chrono = 0
     no-lock  no-error .
if not available ligspe  then do:
   message "Commande client Direct" lignes.typcom lignes.numbon 
           lignes.codaux entete.adres[1]. pause 5.
   next.
   end.

if opt3 = "M" and ligspe.alpha-3 <> "CDF" then next.
if opt3 = "C" and ligspe.alpha-3 <> "CDC" then next.

/* 
message "CCO " ligspe.alpha-3 " " ligspe.alpha-4. pause.
*/

FIND b-ENTETE  where b-entete.codsoc = ligspe.alpha-1
               and   b-entete.motcle = ligspe.alpha-2
               and   b-entete.typcom = ligspe.alpha-3
               and   b-entete.numbon = ligspe.alpha-4
     no-lock  no-error .
if available b-entete  
   then  do: 
         wmag = b-entete.magasin. 
         wtypbon = b-entete.typbon. 
         do aa = 1 to 5:
            wadres [aa] = b-entete.adres[aa].
            end.
         wcodcli = b-entete.codaux.   
         end.

/* lecture ligne cdc de la SD */
wcdcpub = 0.
wcdcpun = 0.

find b-ligcdcsd where 
     b-ligcdcsd.codsoc = ligspe.alpha-1 and
     b-ligcdcsd.motcle = ligspe.alpha-2 and    
     b-ligcdcsd.typcom = ligspe.alpha-3 and    
     b-ligcdcsd.numbon = ligspe.alpha-4 and    
     b-ligcdcsd.chrono = ligspe.int-1     
     no-lock no-error.
if available b-ligcdcsd 
   then do:
   wcdcpub = b-ligcdcsd.pu-brut.
   wcdcpun = b-ligcdcsd.pu-net.
   end.
 
/* lecture de la 45 ACH PCF */

wpcf1 = "".
wpcf2 = "".
wpcf3 = "".
wpcf4 = "".

FIND b-ligspe  where b-ligspe.codsoc    = "45"     
                   and   b-ligspe.motcle    = "ACP/BED"
                   and   b-ligspe.alpha-1   = ligspe.alpha-1
                   and   b-ligspe.alpha-2   = ligspe.alpha-2   
                   and   b-ligspe.alpha-3   = ligspe.alpha-3   
                   and   b-ligspe.alpha-4   = ligspe.alpha-4   
                   and   b-ligspe.int-1     = ligspe.int-1  
                   and   b-ligspe.ss-chrono = 0
     use-index ligspe-2 no-lock  no-error .
if not available b-ligspe  then 
   do:
   /*
   message "Cde PCF Inexistante (ligspe)" ligspe.alpha-1 ligspe.alpha-2
           ligspe.alpha-3 ligspe.alpha-4.
           pause 5.
   */
   end.
   else do:
        FIND b-ENTETE  where b-entete.codsoc = b-ligspe.codsoc  
                       and   b-entete.motcle = "ACP"
                       and   b-entete.typcom = b-ligspe.typcom 
                       and   b-entete.numbon = b-ligspe.numbon 
        no-lock  no-error .  
        if available b-entete  
           then  do:
                 wpcf1    = b-entete.typcom.
                 wpcf2    = b-entete.numbon.
                 wpcf3    = b-entete.nivcom.
                 end.
           else do:
                message "je ne trouve pas la PCF dans ligspe".
                pause.
                end.
        find b-lignes where 
             b-lignes.codsoc = b-entete.codsoc and
             b-lignes.motcle = b-entete.motcle and
             b-lignes.typcom = b-entete.typcom and
             b-lignes.numbon = b-entete.numbon and
             b-lignes.chrono = b-ligspe.chrono    
             no-lock no-error.
        if available b-lignes then wpcf4 = b-lignes.nivcom.

        end.

/* lecture de la 45 ACH CDF Cde fournisseur */

wcdf1 = "".
wcdf2 = "".
wcdf3 = 0.
wcdf4 = "".
wcdf5 = "".

FIND b-ligspe  where b-ligspe.codsoc    = "45"     
                   and   b-ligspe.motcle    = "ACH/BED"
                   and   b-ligspe.alpha-1   = ligspe.alpha-1
                   and   b-ligspe.alpha-2   = ligspe.alpha-2   
                   and   b-ligspe.alpha-3   = ligspe.alpha-3   
                   and   b-ligspe.alpha-4   = ligspe.alpha-4   
                   and   b-ligspe.int-1     = ligspe.int-1  
                   and   b-ligspe.ss-chrono = 0
     use-index ligspe-2 no-lock  no-error .
if not available b-ligspe  then 
   do:
   /*
   message "Cde CDF Inexistante (ligspe)" ligspe.alpha-1 ligspe.alpha-2
           ligspe.alpha-3 ligspe.alpha-4.
           pause 5.
   */
   end.
   else do:
        FIND b-ENTETE  where b-entete.codsoc = b-ligspe.codsoc  
                       and   b-entete.motcle = "ACH"
                       and   b-entete.typcom = b-ligspe.typcom 
                       and   b-entete.numbon = b-ligspe.numbon 
        no-lock  no-error .  
        if available b-entete  
           then  do:
                 wcdf1 = b-entete.typcom.
                 wcdf2 = b-entete.numbon.
                 wcdf4 = b-entete.codaux.
                 wcdf5 = b-entete.typaux.
                 end.
           else do:
                message "je ne trouve pas la CDF dans ligspe".
                pause.
                end.
        find b-lignes where 
             b-lignes.codsoc = b-entete.codsoc and
             b-lignes.motcle = b-entete.motcle and
             b-lignes.typcom = b-entete.typcom and
             b-lignes.numbon = b-entete.numbon and
             b-lignes.chrono = b-ligspe.chrono    
             no-lock no-error.
        if available b-lignes then wcdf3 = b-lignes.chrono.

        end.
        
/* RELIQUAT */
if lignes.fac-httot = 1 then wrel = "N".
else if lignes.fac-httot = 0 then wrel = "O".
     else wrel = " ".
if lignes.top-livre <> "1" 
   then wqteral = lignes.qte[1] - lignes.qte[2].
   else wqteral = 0.

wpoids-cde  = lignes.qte[1] * artapr.pds.
wpoids-liv  = lignes.qte[2] * artapr.pds.
wpoids-ral  = wqteral * artapr.pds.

wcodfou = "".
wnomfou = "".
wartfou = "".

if wcdf4 = "" then do:
find first artbis where 
     artbis.motcle = "ACH"         and
     artbis.codsoc = lignes.codsoc and
     artbis.typaux = "TIE"         and
     artbis.articl = lignes.articl 
     no-lock no-error.
if available artbis then 
   do:
   wcodfou = artbis.codaux.
   wartfou = artbis.articl-fou.
   find auxapr where 
        auxapr.codsoc = lignes.codsoc and
        auxapr.typaux = artbis.typaux and
        auxapr.codaux = artbis.codaux    
        no-lock no-error.
   if available auxapr then 
      do:
      wnomfou = auxapr.adres[1].
      end.
   end.
end.    /* if wcdf4 <> "" */
else do:
   wcodfou = wcdf4.
   find artbis where 
     artbis.motcle = "ACH"         and
     artbis.codsoc = lignes.codsoc and
     artbis.typaux = "TIE"         and
     artbis.codaux = wcodfou       and
     artbis.articl = lignes.articl 
     no-lock no-error.
   if available artbis then wartfou = artbis.articl-fou.
   
   find auxapr where 
        auxapr.codsoc = lignes.codsoc and
        auxapr.typaux = wcdf5         and
        auxapr.codaux = wcdf4   
        no-lock no-error.
   if available auxapr then 
      do:
      wnomfou = auxapr.adres[1].
      end.

end.
l-fou= "".
for each artbis where 
     artbis.motcle = "ACH"         and
     artbis.codsoc = lignes.codsoc and
     artbis.typaux = "TIE"         and
     artbis.articl = lignes.articl    
     no-lock:
l-fou = l-fou + "," + trim(artbis.codaux).     
end.
if length(l-fou) <> 0 then l-fou = substring(l-fou, 2).


/* recherche de la LIV la Plus R�cente */
wdatderliv = ?.
for each liv-lignes
where liv-lignes.codsoc = lignes.codsoc
and   liv-lignes.motcle = lignes.motcle
and   liv-lignes.typcom-reliq = lignes.typcom
and   liv-lignes.numbon-reliq = lignes.numbon
and   liv-lignes.chrono-reliq = lignes.chrono
no-lock:
  if wdatderliv = ? then assign wdatderliv = liv-lignes.datdep.
  else 
    if liv-lignes.datdep > wdatderliv then 
      wdatderliv = lignes.datdep.
end.
   
/* CCO le 01/10/2012  */
wqtecdef  = 0.
wqtecdefm = 0.
for each magasi where 
    magasi.codsoc  = "" and 
    magasi.societe = lignes.codsoc 
    no-lock:
find stocks where
     stocks.codsoc    = lignes.codsoc
 and stocks.articl    = lignes.articl
 and stocks.magasin   = magasi.codtab
 and stocks.evenement = ""
 and stocks.exercice  = ""
 no-lock no-error.
if available stocks then wqtecdef = wqtecdef + stocks.qte[3].

if available stocks and 
   stocks.magasin = lignes.magasin 
   then wqtecdefm = wqtecdefm + stocks.qte[3].

end.

export stream file1 delimiter ";" 
lignes.codsoc
lignes.magasin 
wmag
lignes.typcom
lignes.numbon 
entete.ref-magasin format "x(15)"
entete.ref-tiers
lignes.datbon
lignes.datdep
entete.datliv
artapr.famart artapr.soufam artapr.sssfam 
lignes.articl lignes.libart format "x(30)" 
lignes.qte[1] format ">>>>>9-"
lignes.qte[2] format ">>>>>9-"
wqteral format ">>>>>9-"
lignes.partiel format "X"
lignes.qte-fact format ">>>>>9-"
wrel
artapr.pds
wpoids-cde
wpoids-liv
wpoids-ral
wcodfou wnomfou
wpcf1 wpcf2 wpcf4
wcdf1 wcdf2 wcdf3
l-fou
entete.edition
entete.env-plat
wtypbon
lignes.top-facdir
lignes.qte-tarif
wcodcli
wadres 
artapr.provenance
wcdcpub wcdcpun
wdatderliv
wartfou
wqtecdefm
wqtecdef
skip
.
if opt1 = "O" then 
   tsoufam = artapr.soufam. 
   else tsoufam = "".
if opt2 = "O" then 
   tsssfam = artapr.sssfam.
   else tsssfam = "".

FIND T-tempo-1 where 
           T-tempo-1.t1partiel = lignes.partiel
     and   T-tempo-1.t1famart  = artapr.famart
     and   T-tempo-1.t1soufam  = tsoufam
     and   T-tempo-1.t1sssfam  = tsssfam
     exclusive-lock no-error .
                                                                             
if not available T-tempo-1
           then do :
     CREATE T-tempo-1 .
     assign T-tempo-1.t1partiel = lignes.partiel
            T-tempo-1.t1famart  = artapr.famart
            T-tempo-1.t1soufam  = tsoufam
            T-tempo-1.t1sssfam  = tsssfam.
     end.       

              
T-tempo-1.t1qte   = T-tempo-1.t1qte   + wqteral.
T-tempo-1.t1poids-cde = T-tempo-1.t1poids-cde + wpoids-cde.
T-tempo-1.t1poids-liv = T-tempo-1.t1poids-liv + wpoids-liv.
T-tempo-1.t1poids-ral = T-tempo-1.t1poids-ral + wpoids-ral.

