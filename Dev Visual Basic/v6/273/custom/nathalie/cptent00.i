/* cptent00.i
   Include comptage des entetes des depots pour detection journees manquantes
*/

def var snumbonmin    as char              format "x(10)"              no-undo.
def var snumbonmax    as char              format "x(10)"              no-undo.
def var scodsoc       as char              format "x(2)"               no-undo.
def var sel-sequence  as char              format "x(12)"              no-undo.
def var sdatbonmin    as char              format "x(08)"              no-undo.
def var sdatbonmax    as char              format "x(08)"              no-undo.
def var sdatedimax    as char              format "x(08)"              no-undo.
def var stimedebut    as char              format "x(08)"              no-undo.
def var stimefin      as char              format "x(08)"              no-undo.
def var wcptpag       as integer           format ">>>9"               no-undo.

def {1} shared frame fr-saisie.

form
     "societe "             at 01 scodsoc        colon  9
     "numbon initial:"      at 12 snumbonmin     colon 31
     "date bon min:"        at 45 sdatbonmin     colon 60
     skip
     "numbon final  :"      at 12 snumbonmax     colon 31
     "date bon max:"        at 45 sdatbonmax     colon 60
     skip
     "date edit.max"        at 45 sdatedimax     colon 60
     "fichier edition    :" at 01 sel-sequence   colon 31
     skip
     "nombre de pages    :" at 01 wcptpag        colon 30
     "heure depart:"        at 45 stimedebut     colon 60
     skip
     "heure fin   :"        at 45 stimefin       colon 60
     with title
     "cptent01.p : comptage des entetes vte cdl >= numbon initial"
     with frame fr-saisie overlay no-label { v6frame.i } .
