/*============================================================================*/
/*                          I N V - C A L  0 . I                              */
/* GESTION de L ' INVENTAIRE : Calcul inventaire a la date d'inventaire siege */
/* pour les magasins � inventaires decales                                    */
/*----------------------------------------------------------------------------*/
/* Variables et Frames                                          CC 02/07/2001 */
/*============================================================================*/

def var i          as int  no-undo .
def var no-magasin as char no-undo .
def var no-famille as char no-undo .

def {1} shared var libelle       as char    extent 40               no-undo .
def {1} shared var date-inv      as date    format 99/99/9999       no-undo .
def {1} shared var type-calc     as int     format "9"              no-undo .
def {1} shared var lib-typcalc   as char    format "x(47)"          no-undo .
def {1} shared var mags          as char    format "x(40)"          no-undo .
def {1} shared var fams          as char    format "x(40)"          no-undo .
def {1} shared var mag-exclus    as char                            no-undo .
def {1} shared var fam-exclus    as char                            no-undo .
def {1} shared var z-choix       as char                            no-undo .
def {1} shared var magasi-soc    as char                            no-undo .
def {1} shared var nom-applic    as char                            no-undo .
def {1} shared var wdatcal       as date    format 99/99/9999       no-undo .

def {1} shared frame fr-saisie .

form libelle[ 11 ] format "x(26)" date-inv              skip (1)
     libelle[ 12 ] format "x(26)" type-calc lib-typcalc skip (1)
     libelle[ 13 ] format "x(26)" mags                  skip (1)
     libelle[ 14 ] format "x(26)" fams
     with frame fr-saisie
     row 3  centered  overlay  no-label  { v6frame.i } .
