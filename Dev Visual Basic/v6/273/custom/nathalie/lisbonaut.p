/* Programme : lisbonaut Liste des receptions automatiques pour les magasins */
/*---------------------------------------------------------------------------*/
/*  CCO 17102011 : Pour enlever les TRF                                      */
/*  CCO 24012010 : Pour enlever les RAF                                      */
/*  VLE 28012011 : Ajout du traitement de la societe 59 VITRY                */
/*  CCO 24082011 : Pour enlever les RECEPTIONS non magasins (clients)        */
/*  DGR 08082014 : batch periodique                                          */
/*  VLE 10122014 : Gestion des magasins 524 et 525                           */
/*  DGR 06092016 : enlever les bons RET                                      */
/*  DGR 13092019 : ajout GRASSOT                                             */
/*---------------------------------------------------------------------------*/
{connect.i}
{ bat-ini.i  new }
{ sel.i      "new" }

def input parameter  fichier  as char format "x(12)" .
run bat-lec.p ( fichier ) .


/* debut ajout pour batch periodique */
{ bat-cha.i "sel-applic"   "sel-applic"   "a" }
{ bat-cha.i "sel-filtre"   "sel-filtre"   "a" }
{ bat-cha.i "sel-sequence" "sel-sequence" "a" }

/* fin ajout pour batch periodique */ 




/* Italienne */
def var hpIT as character format "x(16)" initial "~033&l1O" no-undo.
/* sortie Italienne */
def var hpFR as character format "x(16)" initial "~033&l0O" no-undo.
def var hp            as char      format "x(16)"                   no-undo.
def var hp-fran       as char      format "x(8)" initial "~033&l0O" no-undo.
def var hp-ital       as char      format "x(8)" initial "~033&l1O" no-undo.
def var hp-16po       as char      format "x(8)" initial "~033&k2S" no-undo.
def var hp-12po       as char      format "x(8)" initial "~033&k4S" no-undo.
def var hp-10po       as char      format "x(8)" initial "~033&k0S" no-undo.
/* saut de page */
def var sautlas  as character format "x(01)" initial "".
def var wsautlas as character format "x(01)".
def var wpage-size    as integer                                    no-undo.
def var wcptlig       as integer                                    no-undo.
def var wsaut         as char      format "x"                       no-undo.
/*
def var sel-sequence   as char              format "x(12)"            no-undo.
*/
def var sequence       as char              format "x(5)"             no-undo.
def var sedifile       as char              format "x(12)"            no-undo.

def var wcptpag        as integer           format ">>>9"             no-undo.

def stream sortie.
def stream etatmona.
def var nbpag as int.
def var nblig as int.
def var wmag as char format "xxx".
def var wsoc as char format "xx" .
def var l-soc as char initial "01,08,59,88".
def var aa    as int. 
def var wdat1 as date.
def var wlibmag as char format "x(30)".
def var wdatf as date.
def var wheuf as char.
def var wqte  as dec decimals 0.
def var wnomfic as char.
FIND TABGES where 
     tabges.codsoc = ""
 and tabges.etabli = ""
 and tabges.typtab = "loi"
 and tabges.prefix = "lisbonjou"
 and tabges.codtab = "lisbonjou"
exclusive-lock no-error .
                                        
if not available TABGES
   then do :
        CREATE TABGES .
        assign tabges.codsoc = ""
               tabges.etabli = ""
               tabges.typtab = "loi"
               tabges.prefix = "lisbonjou"
               tabges.codtab = "lisbonjou"
               tabges.dattab[1] = date(10, 06, 2009)
               tabges.libel1[1] = "01:14".
               .
         message "j'ai cree tabges".
         end.

message "Programme : lisbonaut.p Liste des receptions Auto magasins".         
message "---------".
message "Date derniere impression " tabges.dattab[1] " a " tabges.libel1[1].

wdatf = tabges.dattab[1].  
wheuf = tabges.libel1[1]. 

output stream sortie to lisbonaut.csv.

export stream sortie delimiter ";"
"Ope" "Date_bon" "Date_Cre" "Heu_cre"
"MotCle" "Typ" "Numbon" "Code_Tiers" "Nom"
"Mag" "Mag_2" "Ref_1" "Ref_2" "Article" "Libelle" "Qte". 

wcptpag    = 0.
wpage-size = 40.
wcptlig    = 100.
wsautlas   = " ".

do aa = 1 to num-entries( l-soc ) :

wsoc = entry( aa, l-soc ) .

wnomfic = "/applic/li/monarch/lis-rec-auto" + wsoc + ".txt".

output stream etatmona   to value(wnomfic).

for each entete where
    entete.codsoc = wsoc 
and entete.motcle = "ACH"
and entete.datmaj >= wdatf
and entete.datcre >= wdatf
and entete.opecre <> "k23"   /* Sorbiers   */
and entete.opecre <> "e22"   /* Veauche    */
and entete.opecre <> "m14"   /* St Laurent */
and not entete.typcom begins "CA"
and entete.typcom <> "CDF"
and entete.typcom <> "RPA"
and entete.typcom <> "REI"
and entete.typcom <> "REC"
and entete.typcom <> "CDI"
and entete.typcom <> "RGF"
and entete.typcom <> "ZZF"
and entete.typcom <> "RGV"
and entete.typcom <> "RPA"
and entete.typcom <> "RGT"
and entete.typcom <> "AVF"   /* Ajout le 20/11/2009 par cco */
and entete.typcom <> "TRF"   /* Ajout le 17/10/2011 par cco */
and entete.typcom <> "RET"   /* Ajout le 06/09/2016 par dgr */
and entete.heucre <> ""
no-lock  use-index date-maj break by entete.magasin by entete.codaux 
         by entete.datcre by entete.heucre by entete.numbon :

if first-of(entete.magasin) 
   then 
   do:
   wcptpag    = 0.
   wcptlig    = 100.
   find magasi where 
     magasi.codsoc = "" and
     magasi.codtab = entete.magasin
   no-lock no-error.
   if not available magasi then next.

   if magasi.equipe <> "A" 
      and lookup(magasi.codtab, "524,525") = 0
      then next.

   wmag = entete.magasin.
   wlibmag = magasi.libel1[1].
   end.

if entete.magasin <> wmag then next.

if entete.datcre = wdatf and entete.heucre <= wheuf then next.

if entete.opecre = wmag then next.

for each lignes where
    lignes.codsoc = entete.codsoc and
    lignes.motcle = entete.motcle and
    lignes.typcom = entete.typcom and
    lignes.numbon = entete.numbon 
    no-lock :
    
/* le 24/01/2010 par CCO pour enlever les RAF */
Find LIGSPE where 
     ligspe.codsoc  = lignes.codsoc
 and ligspe.motcle  = "ACH/BED"
 and ligspe.typcom  = lignes.typcom
 and ligspe.numbon  = lignes.numbon
 and ligspe.chrono  = lignes.chrono
 and ligspe.alpha-3 = "RAF"
 no-lock no-error .
if available LIGSPE then 
   do:
   put stream sortie "RAF elimin� " lignes.codsoc " " lignes.typcom " "
              lignes.numbon " " lignes.chrono skip.  
   next.
   end.

/* CCO 24082011 : Pour enlever les RECEPTIONS non magasins (clients) */
Find LIGSPE where 
     ligspe.codsoc  = lignes.codsoc
 and ligspe.motcle  = "ACH/BED"
 and ligspe.typcom  = lignes.typcom-reliq
 and ligspe.numbon  = lignes.numbon-reliq
 and ligspe.chrono  = lignes.chrono-reliq
 and ligspe.alpha-3 = "CDC"
 no-lock no-error .
if available LIGSPE then 
   do:
   put stream sortie "CDC elimin� " lignes.codsoc " " lignes.typcom " "
              lignes.numbon " " lignes.chrono skip.  
   next.
   end.
   
export stream sortie delimiter ";"
entete.opecre entete.datdep entete.datcre entete.heucre 
entete.motcle entete.typcom entete.numbon 
entete.codaux entete.adres[1] entete.magasin entete.magasin-2
entete.ref-tiers entete.ref-magasin  
lignes.articl libart lignes.qte[2].

if wcptlig > wpage-size then
  do:
  wcptpag = wcptpag + 1.
  put stream etatmona wsautlas hpIT skip
                 "Societe " wsoc
                 " Liste des RECEPTIONS AUTOMATIQUES du JOUR "
                 "                       le " today format "99/99/9999"
                 " Page : " wcptpag skip
                 space (13) skip skip.
  put stream etatmona "Magasin : " wmag " " wlibmag skip.
  put stream etatmona "Typ Numero bon Date_Recep Code_F Fournisseur"
                  "          Mag Refer_Tiers . "
                  "Refer_Mag....  Code  Libelle                       "
                  "    Qte   Coef  Qte_2  Uv" skip.
  put stream etatmona "--- ---------- ---------- ------ "
                  "-------------------- "
                  "--- ------------- ------------- "
                  "------ ------------------------------ "
                  "------- ----- ------- --" skip. 
  wcptlig = 6.
  wsautlas = sautlas.
  end.
if lignes.coef-cde >= 0   
   then wqte = lignes.qte[2] * lignes.coef-cde .
   else wqte = lignes.qte[2] / (lignes.coef-cde * -1).
put stream etatmona
lignes.typcom " "
lignes.numbon " " 
entete.datdep " "
substr(entete.codaux ,5, 6) format "xxxxxx" " "
entete.adres[1] format "x(20)" " "
entete.magasin-2 " "
entete.ref-tiers format "x(13)" " "
entete.ref-magasin format "x(13)" " " 
articl format "xxxxxxx"
lignes.libart format "x(30)" lignes.qte[2] format ">>>>>>9-"
lignes.coef-cde format ">>>>9-" wqte format ">>>>>>9-" " " 
lignes.usto 
skip.

wcptlig = wcptlig + 1.


end.
export stream sortie delimiter ";" "Fin Bon".
put stream etatmona " " skip.
wcptlig = wcptlig + 1.

end.  /* each entete   */

output stream etatmona close .

end.  /* boucle l-soc  */

tabges.dattab[1] = today.
tabges.libel1[1] = string ( time , "hh:mm" ).

message "Date derniere impression " tabges.dattab[1] " a " tabges.libel1[1].

/* Supprimer les selections */
RUN sel-del.p(sel-applic, sel-filtre, sel-sequence, YES).
