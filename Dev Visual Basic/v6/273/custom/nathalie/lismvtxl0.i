/* lismvtxl0.i Extraction des mouvements  => variables communes */

def {1} shared var sdtmin        like                 lignes.datdep no-undo.
def {1} shared var sdtmax        like                 lignes.datdep no-undo.

def {1} shared var sequence     as Char     format "x(5)".
def {1} shared var sel-sequence as Char     format "x(12)".
/*
def {1} shared var codsoc-soc as char format "xx".
*/
def {1} shared var wart as char format "x".
def {1} shared var wsoc as char format "xxxx".

def {1} shared var wtyp    as char format "xxx".
def {1} shared var wmot    as char format "x(3)".
def {1} shared var wmag    as char format "x(3)".
def {1} shared var wmont as dec decimals 2 format "999999999.99-".
def {1} shared var top-extract as char format "x" no-undo.
def {1} shared var lib-extract as char format "x(32)" no-undo.

def {1} shared var wmag-respon    as char format "x(3)".
def {1} shared var wnom           like auxili.adres[1] extent 5.
def {1} shared var socauxili      as char.
def {1} shared var k              as int.
def {1} shared var wcodaux        like auxili.codaux.
def {1} shared var l-codaux       as char.

def {1} shared var wlib as char format "x(30)".
def {1} shared var wfam as char format "x(3)".
def {1} shared var wsfa as char format "x(3)".
def {1} shared var wssf as char format "x(3)".
def {1} shared var wqte1 as dec decimals 3 format ">>>,>>>,>>9.999-".
def {1} shared var stypdt as char format "x(3)"             no-undo.

/* Ajout le 10/09/1999 par CCO */
def {1} shared var warticl as char format "x(6)".
/* Ajout le 08/06/2009 par CCO */
def {1} shared var wcdpos         like auxili.adres[5] extent 5.

