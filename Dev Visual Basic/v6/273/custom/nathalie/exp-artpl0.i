
def {1} shared var i             as int                              no-undo.
def {1} shared var j             as int                              no-undo.
def {1} shared var wcpt          as int                              no-undo.

def {1} shared var wcodsoc       as char format "x(02)"              no-undo.
def {1} shared var wcodaux       as char format "x(06)"              no-undo.
def {1} shared var wadres        as char format "x(32)"              no-undo.
def {1} shared var wfournisseur  as char format "x(9)" extent 5      no-undo.
def {1} shared var wfour         as char format "x(70)"              no-undo.

def {1} shared var sfamille      as char format "x(3)" extent 8      no-undo.
def {1} shared var ssousfam      as char format "x(3)" extent 8      no-undo.
def {1} shared var sssousfa      as char format "x(2)" extent 8      no-undo.
def {1} shared var wnomenc       as char format "x(5)" extent 8      no-undo.

def {1} shared var wmag          as char format "x(3)" extent 20     no-undo.
def {1} shared var l-mag         as char format "x(150)"              no-undo.
def {1} shared var topmag as log no-undo.

def {1} shared var wdatedebN     as char format "xxxxxx"             no-undo.
def {1} shared var wdatefinN     as char format "xxxxxx"             no-undo.

def {1} shared var wdatedebN-1   as char format "xxxxxx"             no-undo.
def {1} shared var wdatefinN-1   as char format "xxxxxx"             no-undo.

def {1} shared var wdate1        as date                             no-undo.
def {1} shared var wdate2        as date                             no-undo.

def {1} shared var wdatesto       as date format "99/99/9999"         no-undo.

def {1} shared var wtypsto       as char format "x"                  no-undo.

def {1} shared var wtypsor       as char format "x"                  no-undo.
def {1} shared var wlibsor       as char                             no-undo.

def {1} shared var sequence      as Char format "x(5)"               no-undo.
def {1} shared var sel-sequence  as Char format "x(12)"              no-undo.

def {1} shared var valid         as char format "x(1)"               no-undo.
