
/* DGR 24/07/2012 == Mse a jour des pamp a partir d'un fichier csv            */
/* le programme se lance en mode CARACTERES et WEB                            */
/*                                                                            */
/*      ===> prog CARACTERES                                                  */
/*            lanceur CARACTERES /applic/src/tty/eurea/maj-pmpl.p             */
/*            prog CARACTERES    /applic/src/tty/eurea/maj-pmp.p              */
/*                                                                            */
/*      ===> procedure commune WEB & CARACTERES                               */
/*        -------> /applic//273/nathalie/nt-majpmp.i                          */
/* DGR 11102018 : ajout soc 88 + creation enregistrement stock si inexistant  */

PROCEDURE TRAITEMENT:


  def var wsoc         as char                      no-undo.
  def var wart         as char                      no-undo.
  def var wevenement   as char                      no-undo.
  def var wannee       as char                      no-undo.
  def var wmois        as char                      no-undo.
  def var svaleur      as char                      no-undo.
  def var wvaleur      as dec   decimals 3          no-undo.
    

  def var wlibart      as char                      no-undo.
  def var wstock-id    as recid                     no-undo.
  def var wmessage     as char                      no-undo.
  def var toprej       as log                       no-undo.
  def var woldvaleur   as dec   decimals 3          no-undo.

  def var l-soc        as char                      no-undo.

  
  def buffer maj-stocks for stocks.
    
  def var wheu         as char                      no-undo.
  def var wdate        as date                      no-undo.

  def var wnomfic2     as char                      no-undo .

  def var nblu        as int                        no-undo.
  def var i as int                                  no-undo.
  def var j as int                                  no-undo.
  def var k as int                                  no-undo.
  def var l as int                                  no-undo.
 


  assign l-soc = "01,08,35,42,45,46,58,59,88".

  /* verif si le Fichier en entree existe */
  if search (wnomfic) = ? then do:
     message "FICHIER INEXISTANT => " wnomfic.
     message "SORTIE ANORMALE DU PROGRAMME".
     leave.
  end.     
  
  OUTPUT STREAM maquette TO VALUE( ma-impri ) .

  if topmaj then do:
    wnomfic2 = "/applic/li/" + trim(lower(operat)) + "/sav-majpmp." + string(today,"99999999") + "-" + replace(string(time,"HH:MM:SS"),":","") .
    output stream sauve to value(wnomfic2) .
  end.  
  export stream maquette delimiter ";" 
    "top maj" "codsoc" "articl" "libart" "evenement" "annee" "mois" "ancienne valeur" "nouvelle valeur" "action" "fichier init".
    

  assign wdate  = today
         wheu   = string(time,"HH:MM")
         nblu = 0 .
        
  input stream entree from value (wnomfic).
  lect:
  repeat:

    assign wsoc       = ""       wart       = ""    wlibart  = ""
           wevenement = ""       wannee     = ""    wmois    = ""
           wvaleur    = ?        wmessage   = ""    toprej   = No
           j          =?         k          = ?        
           svaleur    = "".

    
    nblu = nblu + 1.
    /* import de la ligne du fichier en entree */
    import stream entree delimiter ";" 
      wsoc wart wevenement wannee wmois svaleur
      .

/*
    message nblu wsoc wart wevenement wannee wmois  svaleur.
  
*/
    wsoc = trim (wsoc).
    wart = trim(upper(wart)).


    {cadra-d.i wsoc 2 0}
    {cadra-d.i wart 6 0}


    /* Controle des donnees du fichier */
    
    /* verif societe autorisee */
    if lookup(wsoc,l-soc)  = 0 then 
        wmessage = wmessage + "| PROBLEME CODE SOCIETE " .


    /* VERIFICATION de l'existence de l'article */

    if wart <> "" then do:
      find artapr
      where artapr.codsoc = wsoc
      and   artapr.articl = wart
      no-lock no-error.
      if not available artapr then do:
        wmessage = wmessage + "| ARTICLE INEXISTANT"  .
      end.
      else assign wlibart = artapr.libart1[1].
      
    end.

    /* verification du mois et de l'annee */
    j = int(wmois) no-error.
    if error-status:error then 
        wmessage = wmessage + "| MOIS ERRONE "  .

    if j > 12 or j < 1 then 
        wmessage = wmessage + "| MOIS ERRONE "  .

    k = int(wannee) no-error.
    if error-status:error then 
        wmessage = wmessage + "| ANNEE ERRONEE "  .

    if k > 2025 or k <  2005 then 
        wmessage = wmessage + "| ANNEE ERRONEE "  .

    if year(today) = k and month(today) <= j then
        wmessage = wmessage + "| ANNEE MOIS superieur  a TODAY"  .

    if year(today) < k then 
        wmessage = wmessage + "| ANNEE superieure  a ANNEE EN COURS"  .
    
    if lookup(wevenement , "STOARR,STOARR-VAL") = 0 then 
        wmessage = wmessage + "| EVENEMENT NON AUTORISE "  .

    /* verification du nouveau Prix */
    svaleur  = replace(svaleur," ","").

    wvaleur = dec(svaleur) no-error.
    if error-status:error then 
      wmessage = wmessage + "| VALEUR INCORRECTE"  .
   
   
    wstock-id = ?.

    /* verif si l'enreg existe */
    find stocks
    where stocks.codsoc = wsoc
    and   stocks.evenement = wevenement
    and   stocks.articl = wart 
    and   stocks.magasin = ""
    and   stocks.exercice  = wannee
    no-lock no-error.
    if not available stocks then do:
      if topmaj then do:
         create maj-stocks.
         assign maj-stocks.codsoc = wsoc
               maj-stocks.evenement = wevenement
               maj-stocks.articl = wart 
               maj-stocks.magasin = ""
               maj-stocks.exercice  = wannee.
         { majmoucb.i maj-stocks}
         validate maj-stocks.
         release maj-stocks.
      end.
      
    end.
    
    find stocks
    where stocks.codsoc = wsoc
    and   stocks.evenement = wevenement
    and   stocks.articl = wart 
    and   stocks.magasin = ""
    and   stocks.exercice  = wannee
    no-lock no-error.
    if not available stocks then 
      wmessage = wmessage + "| ENREGISTREMENT STOCK INEXISTANT"  .
    else assign wstock-id = recid(stocks)
                woldvaleur = stocks.prix[j].
        

    if wmessage <> "" then 
      assign toprej = yes
             wmessage = "**** REJET ===> " + wmessage.
    else 
       assign toprej = no 
              wmessage = "OK pour MISE A JOUR".

    export stream maquette delimiter ";"
      topmaj
      wsoc wart wlibart
      wevenement wannee wmois woldvaleur  wvaleur wmessage wnomfic.

    /* Mise a jour si topmaj = "O" et pas d'erreur */
    if topmaj and toprej = no then do:
                 
      find maj-stocks
      where recid(maj-stocks) = wstock-id
      no-error.
      if not available maj-stocks then 
        message "PB ACCES STOCKS en EXCLUSIVE-LOCK".

      else do:
        export stream sauve 
          maj-stocks.codsoc 
          maj-stocks.articl 
          maj-stocks.magasin 
          maj-stocks.evenement
          maj-stocks.exercice
          j
          maj-stocks.prix[j].

        assign maj-stocks.prix[j] = wvaleur.
         
        { majmoucb.i maj-stocks}
      end.

    end.   /* if topmaj .... */ 

  end. /* repeat */

  input stream entree close.

  if topmaj then   output stream sauve close.
  output stream maquette close.

End .
