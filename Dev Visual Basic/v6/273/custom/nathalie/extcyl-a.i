/*==========================================================================*/
/*                           E X T C Y L - A . I                            */
/* Extraction des donnees pour Cylande                                      */
/*--------------------------------------------------------------------------*/
/* interface CLIENTS SALARIES                                               */
/*==========================================================================*/
/* 17052018 : on n'initialise plus le budget                                */
/* 05062018 : en traitement Z, on envoie uniquement la balise BUD en remove */
/* 29102019 : ajout du l'identifiant Winstore pour l'interdiction de se     */
/*            vendre a soi-meme                                             */
/*            reinitialisation du budget de remise a 0 en trt Z             */   

 
/* VARIABLES SALARIES     */
assign 	wtypecli   		= "S"	
         wcivilite       = ""
         wmag       		= "006"
         wnom     		= ""
         wprenom  		= ""
         wferme     		= ""
         wbudget    		= 350.0 . 

	
CREATE SAX-WRITER hSAXWriter.
hSAXWriter:ENCODING = "UTF-8".
hSAXWriter:FORMATTED = FALSE .  /* 	si TRUE: fichier de sortie lisible mais ajout de blancs 
									a la place des sauts de lignes a l'integration dans UR */

lOK = hSAXWriter:SET-OUTPUT-DESTINATION ( "file", wficsortie ).
lOK = hSAXWriter:START-DOCUMENT ( ).
lOK = hSAXWriter:WRITE-COMMENT ( "EXTRACTION DES SALARIES" ) .
lOK = hSAXWriter:START-ELEMENT ( "customerListType" ).

lect:
for each auxdet 
where auxdet.codsoc = soc-cartes-sal
and   auxdet.motcle = "CFI"
and   ( lookup( type-trt ,"I,Z" ) <> 0  or auxdet.datmaj >= date-debut )
/* and   auxdet.codtab = "2980000918377" */ 
no-lock :
                        
	if lookup( type-trt ,"I,Z" ) <> 0  and auxdet.cdat02 <> ?  then next.

	nblus = nblus + 1.
	/* referencé par défaut */
	wferme = "R".
	if auxdet.cdat02 <> ? and auxdet.cdat02 <= today then wferme = "S". /* sinon supprimé */

	assign wcivilite = trim ( upper ( auxdet.calf02 ) ).
	
	assign wnom = trim ( upper ( entry ( 1 , auxdet.calf03 ) ) ) no-error.
	if error-status:error then wnom = "*** PROBLEME NOM ***".
	assign wprenom = trim ( upper ( entry ( 2 , auxdet.calf03 ) ) ) no-error.
	if error-status:error then wprenom = "*** PROBLEME PRENOM ***".
	
	if lookup ( wcivilite, "MME,MR" ) = 0 	or 
		wnom = "*** PROBLEME NOM ***" 		or 
		wprenom = "*** PROBLEME PRENOM ***" 
	then do:
		wcivilite = "".
      toprej = yes.
		put stream ano unformatted 
         "CS -- Problème nom/prenom/civilite CARTE -> " 
         auxdet.codtab        " "
			auxdet.calf02 			" "  
			auxdet.calf03 			 
		skip .
		next lect.
	end .
	
 
   /* DGR 05062018 : en cas de traitement Z : remise a zero du 30/06, on supprime le budget */ 
   /* VLE 29102019 : le remove et la remise a 0 doivent être dans 2 balises values différentes */
   if type-trt = "Z" then do:  
   
	  assign 	
		/* debut etiquette "values" */
		lOK = hSAXWriter:START-ELEMENT("values")	
		/* données salarié 	 */
			lOK = hSAXWriter:WRITE-COMMENT ( "donnees salarie" )
			lOK = hSAXWriter:START-ELEMENT ( "externalCodes" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "typeCode", "DEAL" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", auxdet.codtab )
			lOK = hSAXWriter:END-ELEMENT("externalCodes") .			
      assign 
         lOK = hSAXWriter:START-ELEMENT ("datas")
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code" , "BUD" )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "remove" , "true" )
         lOK = hSAXWriter:END-ELEMENT ("datas") 
         .
		/* fin etiquette "values" */
		lOK = hSAXWriter:END-ELEMENT("values").	
		
	   assign 	
		/* debut etiquette "values" */
		lOK = hSAXWriter:START-ELEMENT("values")	
		/* données salarié 	 */
			lOK = hSAXWriter:WRITE-COMMENT ( "donnees salarie" )
			lOK = hSAXWriter:START-ELEMENT ( "externalCodes" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "typeCode", "DEAL" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", auxdet.codtab )
			lOK = hSAXWriter:END-ELEMENT("externalCodes") .
	   assign	
		lOK = hSAXWriter:START-ELEMENT ("datas")
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code" , "BUD" )
				lOK = hSAXWriter:START-ELEMENT ("data")
					lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "doubleData" , "0.00" )
				lOK = hSAXWriter:END-ELEMENT ("data")
			lOK = hSAXWriter:END-ELEMENT ("datas") .			
		/* fin etiquette "values" */
		lOK = hSAXWriter:END-ELEMENT("values").
   end.
   
   /* sinon on envoie l'enreg salarié, sans le budget */
   else do: 
	  assign 	
		/* debut etiquette "values" */
		lOK = hSAXWriter:START-ELEMENT("values")	
		/* données salarié 	 */
			lOK = hSAXWriter:WRITE-COMMENT ( "donnees salarie" )
			lOK = hSAXWriter:START-ELEMENT ( "externalCodes" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "typeCode", "DEAL" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", auxdet.codtab )
			lOK = hSAXWriter:END-ELEMENT("externalCodes") .
      assign 			
			lOK = hSAXWriter:START-ELEMENT ( "person" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "gender", wcivilite )
				lOK = hSAXWriter:START-ELEMENT ( "firstName" )
					lOK = hSAXWriter:WRITE-CDATA ( wprenom )
				lOK = hSAXWriter:END-ELEMENT ( "firstName" )
				lOK = hSAXWriter:START-ELEMENT ( "lastName" )
					lOK = hSAXWriter:WRITE-CDATA ( wnom ) 
				lOK = hSAXWriter:END-ELEMENT ( "lastName" )
			lOK = hSAXWriter:END-ELEMENT ( "person" )
			
			lOK = hSAXWriter:START-ELEMENT ( "issuedInSupport" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "nature" , wtypecli ) 
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "id" , wmag )
			lOK = hSAXWriter:END-ELEMENT ( "issuedInSupport")
			
			lOK = hSAXWriter:WRITE-COMMENT ( "etat" )
			lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "state" , wferme )
			lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "stateLocal" , wferme ) 
			
			/* Attributs codes et valeurs */
		 	lOK = hSAXWriter:WRITE-COMMENT ("Attributs codes et valeurs")
         
        /* si traitement Init, suppression du budget 
         <customerListType>
            <values>
               <id>0010000000372</id>
               <datas>
                  <code>BUD</code>
                  <remove>true</remove>
               </datas>
            </values>
         </customerListType>
         */
			lOK = hSAXWriter:START-ELEMENT ("datas")
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code" , "SAL" )
				lOK = hSAXWriter:START-ELEMENT ("data")
					lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "intData" , substr(auxdet.codtab, 8, 5) )
				lOK = hSAXWriter:END-ELEMENT ("data")
			lOK = hSAXWriter:END-ELEMENT ("datas") .
        
            
			assign 
			/* Fidelite */
			lOK = hSAXWriter:WRITE-COMMENT (" Infos Fidelite")
			
			lOK = hSAXWriter:START-ELEMENT ("loyaltyIDs")
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "loyaltyId" , auxdet.codtab )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "loyaltyProgramCode" , "1" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "loyaltySupportType" , "SAL" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "main" , "true" )
			lOK = hSAXWriter:END-ELEMENT ("loyaltyIDs")
				
			lOK = hSAXWriter:START-ELEMENT ("loyaltyAccounts")
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "loyaltyProgramCode" , "1" )
				lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "loyaltyAccountType" , "1" )
			lOK = hSAXWriter:END-ELEMENT ("loyaltyAccounts")
							
			lOK = hSAXWriter:WRITE-DATA-ELEMENT( "mainLoyaltyProgram" , "1" ) .
			
	        assign
			/* fin etiquette "values" */
			lOK = hSAXWriter:END-ELEMENT("values").
   end.
   

		
	if lOK then 
		nbecr = nbecr + 1 .
	else do:
      toprej = yes.
		put stream ano unformatted  
         "CS -- probleme ecriture salarie " 
         auxdet.codtab " " 
         auxdet.calf02 " " 
			auxdet.calf03 skip.
	end.	
	
END. /* for each auxdet */
lOK = hSAXWriter:END-ELEMENT("customerListType").
lOK = hSAXWriter:END-DOCUMENT().

DELETE OBJECT hSAXWriter. 

						