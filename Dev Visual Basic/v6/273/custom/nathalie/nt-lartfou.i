/* DGR 071217 : ajout nomenclature 2                                                                         */
/* CCO 15072019 : ajout {tarcli.i}                                                                           */

find artapr 
where artapr.codsoc = artbis.codsoc
and   artapr.articl = artbis.articl
and   lookup ( artapr.famart, l-codfam ) <> 0
and   ( topferme = "O" or artapr.code-blocage <>  "F" )
no-lock no-error.
if available artapr then do:
                                   
    wgencod = "".       
    for each codbar
    where codbar.codsoc = artapr.codsoc
    and   codbar.articl = artapr.articl
    no-lock:
        wgencod = wgencod + codbar.code-barre + ",".
    end .
    wgencod = right-trim(wgencod,",").
    
    /* recherche tarif de vente */
    assign wdatcli = ? wvalcli = ?.
    find first {tarcli.i}
    where {tarcli.i}.codsoc = artbis.codsoc
    and   {tarcli.i}.motcle = "VTE"
    and   {tarcli.i}.typaux = ""
    and   {tarcli.i}.codaux = ""
    and   {tarcli.i}.articl = artapr.articl
    and   {tarcli.i}.code-tarif = wcodtar
    no-lock no-error.
    if available {tarcli.i} then
        assign  wdatcli = {tarcli.i}.date-debut
                wvalcli = {tarcli.i}.valeur.
    
    /* recherche STE */
    assign wdatste = ? wvalste = 0.
    find first {tarcli.i}
    where {tarcli.i}.codsoc = artbis.codsoc
    and   {tarcli.i}.motcle = "ACH"
    and   {tarcli.i}.typaux = artbis.typaux
    and   {tarcli.i}.codaux = artbis.codaux
    and   {tarcli.i}.articl = artapr.articl
    and   {tarcli.i}.code-tarif = "STE"
    no-lock no-error.
    if available {tarcli.i} then
        assign wdatste = {tarcli.i}.date-debut
            wvalste = {tarcli.i}.valeur.
    
           
    /* recherche remises fournisseur */
    assign wdatrfc = ? 
        wremrfc = 0.
    
    find first REMCLI 
    where remcli.motcle = "ach"
    and   remcli.codsoc =  artbis.codsoc    
    and   remcli.typaux =  artbis.typaux
    and   remcli.codaux =  artbis.codaux
    and   remcli.articl =  artapr.articl
    no-lock no-error.
    if available remcli then do:
        do irem = 1 to 10:
            do jrem = 1 to 10:
                if remcli.codrem[ irem ] <> "RFC" then next.
                if remcli.valeur [((irem - 1) * 10) + jrem]  = 0 then next.
                wdatrfc = remcli.date-debut.
                wremrfc = - remcli.valeur [((irem - 1) * 10) + jrem].
            end.
        end.
    end.
    find tabges
    where tabges.codsoc = artbis.codsoc
    and   tabges.etabli = "ACH"
    and   tabges.typtab = "ART"
    and   tabges.prefix = "SUIT-FICHE"
    and   tabges.codtab = "TIE    802190" + artbis.articl
    no-lock no-error.
    if available tabges then 
        wartfou = tabges.libel1[4].
    else 
        wartfou = "".
     
    wmincdeMS = ?.
    if artbis.codaux = "    802190" and artbis.articl-fou <> "" and artbis.articl-fou <> "000000" then do :
        FIND FIRST ARTICS
        where artics.codsoc   = ""
        AND artics.articl   = artbis.articl-fou
        AND artics.theme    = "PLA-" + "TIE" + "-" + artbis.codaux
        AND artics.chrono   = 1
        use-index primaire  
        no-lock no-error .
        if available artics  then wmincdeMS = artics.nombre [14].
    end.
    wpan = "".
    find artics
    where artics.codsoc = artapr.codsoc
    and   artics.articl = artapr.articl
    and   artics.theme = "PAN"
    and   artics.chrono = 0
    no-lock no-error.
    if available artics then wpan = artics.alpha-cle.
           
    export stream maquette delimiter ";"
        artbis.articl-fou   artapr.articl   artapr.libart   artbis.codsoc       artbis.typaux       artbis.codaux       wlibfou
        artapr.famart       artapr.soufam   artapr.sssfam   artapr.provenance   artapr.code-blocage artapr.typologie    wpan 
        wgencod             artapr.prx-modif    wdatcli         wvalcli         artbis.ucde         artbis.coef-cde     artbis.ufac         artbis.coef-fac 
        wmincdeMS           wdatste         wvalste         wdatrfc             wremrfc             artbis.libart2      artapr.libart1[1]
        wartfou             artapr.tva      artbis.libart1
        artapr.famart2       artapr.soufam2   artapr.sssfam2  .
    
    nblus = nblus + 1.
end. /* if available artapr */