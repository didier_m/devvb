 /*-------------------------------------------------- */
/*  Acces parametre lecture societe article ==> OK   */
/*-------------------------------------------------- */
/* DGR 291107 : correction prolb�me pamp < 0                                  */
/* DGR 190911 : PAMP dans STOARR                                              */
/* CCO 080719 : ajout {tarcli.i}                                              */

      wcpt = wcpt + 1.
      find artbis
      where artbis.codsoc = articl-soc
      and   artbis.motcle = "VTE"
      and   artbis.articl = artapr.articl
      no-lock no-error.

      wclasse = "".
      if artapr.code-toxic <> "" then do:
        find tabges
        where tabges.codsoc = ""
        and   tabges.etabli = ""
        and   tabges.typtab = "art"
        and   tabges.prefix = "toxicite"
        and   tabges.codtab = artapr.code-toxic
        no-lock no-error.
        if available tabges then
          wclasse = tabges.libel3[3].
        else
          wclasse = "Code toxicite inconnu".
      end.

      if wcpt modulo 100 = 0 then message "nb articles lus " wcpt.

      rech-bastat:
      for each bastat use-index artdat where
      bastat.codsoc  = wcodsoc
      and bastat.articl  = artapr.articl
      and bastat.motcle  = "vte"
      and bastat.typaux  = "tie"
      and bastat.datfac >= ddatedeb             /* periode du  */
      and bastat.datfac <= ddatefin             /* au          */
      and (    smagasin[1]  = " "
               or smagasin[1]  = bastat.mag-respon or smagasin[2]  = bastat.mag-respon
               or smagasin[3]  = bastat.mag-respon or smagasin[4]  = bastat.mag-respon
               or smagasin[5]  = bastat.mag-respon or smagasin[6]  = bastat.mag-respon
               or smagasin[7]  = bastat.mag-respon or smagasin[8]  = bastat.mag-respon
               or smagasin[9]  = bastat.mag-respon or smagasin[10] = bastat.mag-respon
               or smagasin[11] = bastat.mag-respon or smagasin[12] = bastat.mag-respon
               or smagasin[13] = bastat.mag-respon or smagasin[14] = bastat.mag-respon
               or smagasin[15] = bastat.mag-respon or smagasin[16] = bastat.mag-respon
               or smagasin[17] = bastat.mag-respon or smagasin[18] = bastat.mag-respon
               or smagasin[19] = bastat.mag-respon or smagasin[20] = bastat.mag-respon
           )
      no-lock :

        if topdivers = no and bastat.codaux = "    099999" then next rech-bastat.

        if lookup(bastat.typcom,l-typcom ) = 0 then next rech-bastat.
        wprix = 0.
        if bastat.qte <> 0 then wprix = bastat.fac-ht / bastat.qte.
        wpamp = ?.

        ddatepamp = bastat.datdep.

        if ddatepamp <= 06/30/2011 then weve = "PXACHA".
        else weve = "STOARR".
       
        
        find stocks
        where stocks.codsoc = wcodsoc
        and   stocks.articl = artapr.articl
/*         and   stocks.evenement = "PXACHA" */
        and   stocks.evenement = weve
        and   stocks.magasin = ""
        and   stocks.exercice = string(year(ddatepamp))
        no-lock no-error.
        if available stocks then wpamp = stocks.prix[month(ddatepamp)].
        whtc = ?. wcli = ?.
        find first {tarcli.i}
        where {tarcli.i}.codsoc = bastat.codsoc
        and   {tarcli.i}.motcle = "VTE"
        and   {tarcli.i}.typaux = ""
        and   {tarcli.i}.codaux = ""
        and   {tarcli.i}.articl = bastat.Articl
        and   {tarcli.i}.date-debut <= bastat.datdep
        and   ({tarcli.i}.date-fin = ? or
               {tarcli.i}.date-fin >= bastat.datdep)
        and   {tarcli.i}.code-tarif = "HTC"
        no-lock no-error.
        if available {tarcli.i} then
          whtc = {tarcli.i}.valeur.

        find first {tarcli.i}
        where {tarcli.i}.codsoc = bastat.codsoc
        and   {tarcli.i}.motcle = "VTE"
        and   {tarcli.i}.typaux = ""
        and   {tarcli.i}.codaux = ""
        and   {tarcli.i}.articl = bastat.Articl
        and   {tarcli.i}.date-debut <= bastat.datdep
        and   ({tarcli.i}.date-fin = ? or
        {tarcli.i}.date-fin >= bastat.datdep)
        and   {tarcli.i}.code-tarif = "CLI"
        no-lock no-error.
        if available {tarcli.i} then
          wcli = {tarcli.i}.valeur.

        put stream file1
          '"' bastat.codsoc        '" "'
              bastat.mag-respon             '" "'
              bastat.codaux-fac    '" "'
              artapr.famart        '" "'
              artapr.soufam        '" "'
              bastat.articl        '" "'
              bastat.qte           '" "'
              bastat.numbon        '" "'
              bastat.datbon        '" "'
              bastat.numfac        '" "'
              bastat.datfac        '" "'
              bastat.datdep        '" "'
              artapr.libart1[1]    '" "'
              bastat.magasin       '" "'
              artapr.sssfam        '" "'
              bastat.fac-ht        '" "'
              bastat.modliv        '" "'
              bastat.modreg        '" "'
              bastat.datech        '" "'
              bastat.typbon        '" "'
              wprix                '" "'
              artapr.type-condit   '" "'
              artapr.usto          '" "'
              artbis.ufac          '" "'
              artbis.coef-fac      '" "'
              artbis.surcondit     '" "'
              wcli                 '" "'
              whtc                 '" "'
              wpamp format ">>>9.999-" '" "'
              artapr.bareme        '" "'
              artapr.code-toxic    '" "'
              wclasse              '" "'
              artapr.va            '" "'
              artapr.toxicite      '" "'
              artapr.caract[01]    '" "'
              bastat.typcom        '" "'
              artapr.pds           '"'
              skip.
      end.
