
DEFINE STREAM sortie .        /* log et fichier de sortie */

DEFINE VARIABLE cco-trace         AS CHARACTER   initial "NON".  /* OUI */
if cco-trace = "OUI" then do:
end.  /* Fin trace */

DEFINE VARIABLE cco-fictrace    AS CHARACTER   NO-UNDO .
DEFINE VARIABLE cco-datheur     AS CHARACTER   NO-UNDO.

/* Fichier de trace */
/* Date et heure du traitement */
cco-datheur = STRING(YEAR(TODAY),"9999") + STRING(MONTH(TODAY),"99") + STRING(DAY(TODAY),"99") + "-" + STRING(TIME,"hh:mm:ss").
cco-datheur = REPLACE(cco-datheur, ":", "").
cco-fictrace = "trace.log" + cco-datheur .

OUTPUT STREAM sortie TO VALUE(cco-fictrace).
    PUT STREAM sortie UNFORMATTED FILL("=", 100) SKIP .
    PUT STREAM sortie UNFORMATTED "Traitement ediorisx.p : Extraction du fichier RMAG (Articles) ORISON" SKIP .
    PUT STREAM sortie UNFORMATTED " " skip.
    PUT STREAM sortie UNFORMATTED "==> Debut de traitement : " + STRING(TODAY) + " heure : " + STRING(TIME, "hh:mm:ss") SKIP .
    PUT STREAM sortie UNFORMATTED " " skip.
OUTPUT STREAM sortie CLOSE .

