/* prog : excrmasa2.p Affecte le code regroupement tiers */
/* DGR 24062016 : modif pour code tiers regroupement verif existance sur la societe */ 
/* CCO 15062019 : modif pour int�grer soc 29 ATRIAL dans CRM ASAPE                  */ 

{connect.i}

def stream fsor1.
def stream flog.

def var wfent1   as char format "x(40)".
def var wfsor1   as char format "x(37)".
def var wtime    as char.
def var nblus    as int.
def var nbecr    as int.

def var cli-regroup as char.
def var l-soc    as char initial "30,01,08,42,45,58,59" .
def var l-socreg as char .
def var rsoc     as char.
def var bb       as int.

def buffer grp-auxapr for auxapr.

def var zcodcli as char.
def var z2      as char.
def var z3      as char.
def var z4      as char.
def var z5      as char.
def var z6      as char.
def var z7      as char.
def var z8      as char.
def var z9      as char.
def var z10     as char.
def var z11     as char.

def temp-table t-tempo-1
    field t1codcli  as char
    field t1mt1     as dec decimals 0
    field t1mt2     as dec decimals 0
    field t1mt3     as dec decimals 0
    field t1mt4     as dec decimals 0
    field t1mt5     as dec decimals 0
    field t1mt6     as dec decimals 0
    index i-tempo-1 is unique primary t1codcli.

def var ccDateHeure as char.
def var ccFicLog    as char.
def var wsoc        as char.

/* Fichier de log */
/* Date et heure du traitement */
ccDateHeure = STRING(YEAR(TODAY),"9999") + STRING(MONTH(TODAY),"99") + STRING(DAY(TODAY),"99") + "-" + STRING(TIME,"hh:mm:ss").
ccDateHeure = REPLACE(ccDateHeure, ":", "").
ccFicLog = "/applic/li/travail/excrmasa2.log." + ccDateHeure .

if TRIM(codsoc-soc) = "29" then l-socreg = l-soc.
                           else l-socreg = TRIM(codsoc-soc).

/*
000408;0029;NE; 00000000603; 00000000000; 00000000000; 00000000000; 00000000000; 00000000000;N;*;
000497;0029;NE; 00000003774; 00000003774; 00000003774; 00000000000; 00000000000; 00000000000;N;*;
*/

wtime = string(time,"hh:mm:ss").

wfent1 = "/applic/li/depart/crmisa/finance-ori.csv".
wfsor1 = "/applic/li/depart/crmasa/si_finance.txt".

input from value(wfent1).

/* Ca fonctionne a condition que la soci�t� 29 passe en premier dans les traitements batchs associ�s */
if codsoc-soc = "29" then output stream fsor1 to value(wfsor1) .
                     else output stream fsor1 to value(wfsor1) append .
                     
output stream flog  to value(ccFicLog) .

put stream flog "Debut traitement " string(today, "99/99/9999") " " string(time,"hh:mm:ss") " Societe " l-socreg skip.

put stream flog "Nom du fichier d'entree " wfent1 skip.

repeat:
  
import delimiter ";" zcodcli z2 z3 Z4 z5 z6 z7 z8 z9 z10 z11.

nblus = nblus + 1.

/* recherche code regroupement */
cli-regroup = zcodcli.
do bb = 1 to num-entries( l-socreg ) :

rsoc = entry( bb, l-socreg ) .

find auxapr where 
     auxapr.codsoc = rsoc  and
     auxapr.typaux = "TIE" and
     auxapr.codaux = "    " + zcodcli 
     no-lock no-error.
if available auxapr and auxapr.codaux-fiscal <> "" then do:
	
      /* DGR 24062016 : verif que le code de regroupement existe sur la societe sinon : lui meme */
      find grp-auxapr 
	  where grp-auxapr.codsoc = auxapr.codsoc
          and   grp-auxapr.typaux = auxapr.typaux-fiscal
          and   grp-auxapr.codaux = auxapr.codaux-fiscal
      no-lock no-error.
      if available grp-auxapr then do:
      cli-regroup = substr(auxapr.codaux-fiscal,5,6).
      put stream flog "Regroupement Tiers " zcodcli "-" cli-regroup skip.
      leave.
      end.
      end.
        
end.

FIND t-tempo-1 where t-tempo-1.t1codcli = cli-regroup
     exclusive-lock no-error .                                                                             
if not available t-tempo-1
     then do :
     CREATE t-tempo-1 .
     assign t-tempo-1.t1codcli = cli-regroup.
     end.       

t-tempo-1.t1mt1 = t-tempo-1.t1mt1 + dec(z4).
t-tempo-1.t1mt2 = t-tempo-1.t1mt2 + dec(z5).
t-tempo-1.t1mt3 = t-tempo-1.t1mt3 + dec(z6).
t-tempo-1.t1mt4 = t-tempo-1.t1mt4 + dec(z7).
t-tempo-1.t1mt5 = t-tempo-1.t1mt5 + dec(z8).
t-tempo-1.t1mt6 = t-tempo-1.t1mt6 + dec(z9).

end.

if codsoc-soc = "29" then put stream fsor1 unformatted
      "Cle_unique;Tiers;Activite;Mont1;Mont2,Mont3;Mont4;Mont5;Mont6;Fsup;etoile"
      skip. 

for each t-tempo-1 :

if TRIM(codsoc-soc) = "29" then wsoc  = "30".
else wsoc  = TRIM(codsoc-soc).

put stream fsor1 unformatted
      TRIM(wsoc) t-tempo-1.t1codcli ";"  
      TRIM(wsoc) t-tempo-1.t1codcli ";NE;"  
      string(t-tempo-1.t1mt1, "-99999999999") ";" 
      string(t-tempo-1.t1mt2, "-99999999999") ";" 
      string(t-tempo-1.t1mt3, "-99999999999") ";" 
      string(t-tempo-1.t1mt4, "-99999999999") ";" 
      string(t-tempo-1.t1mt5, "-99999999999") ";" 
      string(t-tempo-1.t1mt6, "-99999999999") ";N;*;"
      skip. 
       
nbecr = nbecr + 1.

end.

put stream flog "Nbre Lus ......" nblus skip.
put stream flog "Nbre Ecrits ..." nbecr skip.
put stream flog " " skip.

put stream flog "Fin traitement " string(today, "99/99/9999") " " string(time,"hh:mm:ss") skip.

put stream flog " " skip.

