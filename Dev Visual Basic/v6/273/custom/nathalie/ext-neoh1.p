/*****************************************************************************/
/*==========================================================================*/
/*                            E X T - N E O H 1 . P                         */
/* Extraction hebdo des donnes pour neotic portail adh : CA et CAPITAL      */
/*--------------------------------------------------------------------------*/
/* DGR 10032015: Creation extraction CA hebdo                               */
/*               extraction CAPTIAL hebdo au lieu de tous les jours         */                       
/*==========================================================================*/ 

{ connect.i        }
{ bat-ini.i   "new" }
{ aide.i      "new" }
{ sel.i       "new" }
{ ext-neoh0.i "new" }


/*-------------------------------------*/
/* Definition des param�tres d'appel   */
/*-------------------------------------*/

def input parameter fichier as char format "x(12)".

/*-------------------------------------*/
/* Definition des variables de travail */
/*-------------------------------------*/
def var auxili-soc       like auxili.codsoc                 no-undo .
def var magasi-soc       like auxili.codsoc                 no-undo .
def var type-tiers       like auxili.typaux init "TIE"      no-undo .
def var exploit          as logical                         no-undo .

def buffer b-auxapr for auxapr.
def buffer b-auxili for auxili.
def buffer b-auxdet for auxdet.
def buffer b-auxgss for auxgss.
def buffer z-tabges for tabges.

def var i-param          as char                            no-undo .
def var o-param          as char                            no-undo .
def var zone             as char                            no-undo .
def var wlib             as char                            no-undo .
def var wlogin           as char                            no-undo .
def var wlib2            as char                            no-undo .
def var wmdp             as char                            no-undo .
def var wdatdebd         as date                            no-undo . 
def var wdatfind         as date                            no-undo .
def var topagreo         as logical                         no-undo .
def var heure-debut      as int                             no-undo .
def var nsoc             as int                             no-undo .
def var nom-table        as char                            no-undo .
def var code-table       as char                            no-undo .
def var nb-lus           as int                             no-undo .
def var per-modulo       as int                             no-undo .
def var i                as int                             no-undo . 
def var a                as int                             no-undo . 
def var i-carac          as int                             no-undo .
def var l-carac          as char extent 14                  no-undo .
def var l-libcar         as char extent 14                  no-undo .
def var l-mvtapp         as char                            no-undo .
def var l-cerealKO       as char                            no-undo .
def var j                as int                             no-undo .
 def var wcodapp         as char                            no-undo .
def var x                as char                            no-undo . 
def var wcamp            as char                            no-undo . 
def var wsolde           as decimal                         no-undo .                             
def var wca-adh          as decimal                         no-undo .                             
def var wmt              as decimal                         no-undo .                             

def var wdatdeb          as date                            no-undo .
def var wdatfin          as date                            no-undo .
def var wdatdebb         as date                            no-undo .
def var wdatfinb         as date                            no-undo .
def var wdatdebl         as date                            no-undo .
def var wdatfinl         as date                            no-undo .
def var wderarr          as date                            no-undo .
 
def stream str1.
def stream str2.
def stream str3. 
def stream str4.
def stream str5.

def var fic-neo1 as char .
def var fic-neo2 as char .
def var fic-neo3 as char .
def var fic-neo4 as char .
def var fic-neo5 as char .

/* compteurs */ 
def var topfic           as log                             no-undo .
def var commande         as char                            no-undo .

def var nbcap            as int                             no-undo . 
def var nbca             as int                             no-undo . 

def buffer b-tabges   for  tabges .
def var taux-coef     as dec                                   no-undo .
def var cod-coef      as char                                  no-undo .
def var top-valide 	  as log								no-undo .


session:system-alert-boxes = yes .


/*--------------------------*/
/* Chargement aide et texte */
/*--------------------------*/
aide-fichier = "ext-neoh0.aid".
run aide-lec.p .

do inddgr = 1 to 30 :
    aide-m = "libel" + string( inddgr ,"99") .
    libelle[inddgr] = aide-m.
    { aide-lib.i }
    libelle[inddgr] = aide-lib.
end.

/*---------------------------*/
/* Chargement des selections */
/*---------------------------*/

run bat-lec.p ( fichier ).

{ bat-cha.i "type-trt"   "type-trt"   "a" }
{ bat-cha.i "l-soc"      "l-soc"      "a" }
{ bat-cha.i "l-tables"   "l-tables"   "a" }
{ bat-cha.i "sel-applic"   "sel-applic"   "a" }
{ bat-cha.i "sel-filtre"   "sel-filtre"   "a" }
{ bat-cha.i "sel-sequence" "sel-sequence" "a" }

/*-----------------*/
/* Initialisations */
/*-----------------*/
if type-trt = entry(1, libelle[16]) then exploit = yes. /* Trt Exploitation */
                                    else exploit = no.


assign 	date-debut = today
		heure-debut = time.

message "extraction hebdo NEOTIC depuis le " date-debut.

codsoc-soc = "01".

/* Societes de Regroupement */
regs-app = "NATHALIE".
{ regs-cha.i }
  
regs-app = "ELODIE".
{ regs-cha.i }

regs-fileacc = "AUXILI" + type-tiers.
{regs-rec.i}
auxili-soc = regs-soc .

{ regs-inc.i "magasi" }
magasi-soc = regs-soc .

if month ( date-debut) <= 7 then
	assign 	wdatdeb = date ( 07 , 01 , year ( date-debut ) - 1 )
			wdatfin = date ( 06 , 30 , year ( date-debut )     ).
else		
	assign 	wdatdeb = date ( 07 , 01 , year ( date-debut )     )
			wdatfin = date ( 06 , 30 , year ( date-debut ) + 1 ).
			
 
FIND last TABGES
where tabges.codsoc = codsoc-soc
and   tabges.etabli = ""
and   tabges.typtab = "STO"
and   tabges.prefix = "CALEND"
and   tabges.nombre[1] = 1
and   tabges.dattab[1] < wdatfin
no-lock no-error .
if available tabges  then  do:
	wderarr = tabges.dattab [ 1 ] .
end.
 
assign wdatdebb = ? wdatfinb = ? wdatdebl = ? wdatfinl = ?.

if  wderarr < wdatdeb then do :
	assign wdatdebb = ? wdatfinb = ? wdatdebl = wdatdeb wdatfinl = wdatfin.
end.
else do:
	if wderarr = wdatfin then do:
		assign wdatdebb = wdatdeb wdatfinb = wdatfin wdatdebl = ? wdatfinl = ?.	
	end .
	else do:
		assign wdatdebb = wdatdeb 
			   wdatfinb = wderarr 
			   wdatdebl = wderarr + 1 
			   wdatfinl = wdatfin.	
	end.
end.

if wdatfinl <> ? and wdatfinl > date-debut then wdatfinl = date-debut.

 
if lookup ("CS", l-tables ) <> 0 then do:
	/* Capital Social */ 
	assign nbcap = 0.
	/* fichiers de sortie */ 
	assign 	fic-neo1 = "/applic/li/depart/neotic/"  +  codsoc-soc  + "/[CAPSOC]."	+ codsoc-soc    +  ".exp" .
	if not exploit then
		assign 	fic-neo1 = "/applic/li/dgr/[CAPSOC]."	+ codsoc-soc    +  ".tst"

	topfic = yes.
	if search (fic-neo1) = ? then topfic = no.
	output stream str1  to value(fic-neo1) append.  /* Capital*/
	if topfic = no then                             /* le fichier du capital  n'est trait� qu'a partir de la 2�me ligne .....*/ 
		put stream str1 "CODE RESULTAT;DATE;CODE EXPLOIT;MOUVEMENT;MONTANT;SOLDE" skip  .   /* LIGNE D'ENTETE */ 
end.

 if lookup ("CA", l-tables) <> 0 then do:
	/* Ca exercice en cours */ 
	assign nbca = 0.
	/* fichiers de sortie */ 
	assign 	fic-neo2 = "/applic/li/depart/neotic/"  +  codsoc-soc  + "/[REM]."	+ codsoc-soc    +  ".exp" .
	if not exploit then
		assign 	fic-neo2 = "/applic/li/dgr/[REM]."	+ codsoc-soc    +  ".tst"

	topfic = yes.
	if search (fic-neo2) = ? then topfic = no.
	output stream str2  to value(fic-neo2) append.  /* CA */
	if topfic = no then                             /* le fichier du CA  n'est trait� qu'a partir de la 2�me ligne .....*/ 
		put stream str2 "CODE DU RESULTAT;DATE;CODE EXPLOIT;TYPE MOUVEMENT;CA DE BASE;TAUX;MONTANT;" skip  .   /* LIGNE D'ENTETE */ 

	message "Date-extraction :" today skip
			"Dernier arrete  :" wderarr skip
			"PERIODE du :" wdatdeb " au " wdatfin skip
			"Rech BASTAT du : " wdatdebb " au " wdatfinb skip
			"Rech LIGNES du : " wdatdebl " au " wdatfinl skip
        .
/*
	output to toto.dgr.
		put  "Date-extraction :" today skip
			"Dernier arrete  :" wderarr skip
			"PERIODE du :" wdatdeb " au " wdatfin skip
			"Rech BASTAT du : " wdatdebb " au " wdatfinb skip
			"Rech LIGNES du : " wdatdebl " au " wdatfinl skip.
		
	output close.
*/

end.	



 /* recuperation tiers abonnes */
for each auxgss
where auxgss.codsoc = codsoc-soc
and   auxgss.typaux = "TIE"
and   auxgss.theme = "EXTRANET"
and   auxgss.chrono = 0
no-lock:
         
    if auxgss.alpha-cle =  "O" and auxgss.datte[1] > date-debut then next . /* pas encore abonne */
    if auxgss.alpha-cle <> "O" and auxgss.datte[1] < date-debut then next . /* plus abonne */

	find auxapr
	where auxapr.codsoc = auxgss.codsoc
	and   auxapr.typaux = auxgss.typaux
	and   auxapr.codaux = auxgss.codaux
	no-lock no-error.
	if not available auxapr then next.
	

	find auxili
	where auxili.codsoc = auxili-soc 
	and   auxili.typaux = auxapr.typaux 
	and   auxili.codaux = auxapr.codaux
	no-lock no-error.
	if not available auxili then next.
	
	if lookup ("CS", l-tables) <> 0   then do:
		/* CAPITAL */
		assign wsolde = 0.

		/* acces a la table de d�tail des operations en capital */ 
		for each capopep
		where capopep.codsoc = "01"
		and   capopep.codetb = "001"
		and   capopep.typaux = "tie"
		and   capopep.codaux = auxapr.codaux
		no-lock :
			case capopep.sens :
				when "M" then 
					assign 	wsolde = wsolde - capopep.cap-souscrit
							wmt    = - capopep.cap-souscrit .
				otherwise  
					assign 	wsolde = wsolde + capopep.cap-souscrit
							wmt    = capopep.cap-souscrit.
			end case . 
					
			/* code du resultat = cle de capopep : codsoc codetb typaux codaux chrono */ 
			assign zone = trim ( capopep.codsoc )                             /* code du resultat */
						+ trim ( capopep.codetb )                     + "-"
						+ trim ( capopep.codaux )                     + "-"
						+ string ( capopep.chrono , "9999")           + ";"
						+ string ( capopep.date-op , "99/99/9999")    + ";"        /* date du mouvement */
						+ trim ( auxapr.codaux )                   + ";"        /* code tiers */ 
						.
			/* type de mouvement (libelle) pris dans l'operation si renseign� car plus explicite  */
			wlib = "".          
			if trim ( capopep.libelle ) <> "" then 
				wlib =  upper ( trim ( capopep.libelle ) ) .
			else do:        /* sinon recup du libelle de mouvement en fonction du code mouvement */ 
				find tables
				where tables.codsoc = ""
				and   tables.etabli = ""
				and   tables.typtab = "CAP"
				and   tables.prefix = "OPE-PART"
				and   tables.codtab = capopep.code-particulier
				no-lock no-error.
				if available tables then assign wlib = trim( tables.libel1 [1] ).
			end.
			wlib = substr ( trim ( wlib ) , 1 , 50 ).
			assign zone = zone 
						+ trim ( wlib )                               +    ";"     /* type de mouvement */ 
						+ trim ( string ( wmt, "->>>>>9.99" ) )       +    ";"     /* montant du mouvement */
						+ trim ( string ( wsolde, "->>>>>9.99" ) )                 /* solde a date */
						.
				
			put stream str1 unformatted zone skip.
			zone = "".               
			nbcap = nbcap  + 1.
		end.
		
	end.

	if lookup ("CA", l-tables) <> 0  then do:
	/* extraction du CA */	
		
		for each b-auxapr
		where b-auxapr.codsoc = codsoc-soc
		and   b-auxapr.typaux = "tie"
		and  ( ( b-auxapr.codaux = auxapr.codaux  )        or
		       ( b-auxapr.codaux-fiscal = auxapr.codaux  ) 
			 )
		no-lock:
		        
			assign wca-adh  = 0 .		
			if wdatdebb <> ? then do:
				FOR EACH BASTAT 
				where bastat.codsoc = codsoc-soc
				and   bastat.motcle = "VTE"
				and   bastat.typaux = b-auxapr.typaux
				and   bastat.codaux = b-auxapr.codaux
				and   bastat.datdep >= wdatdebb
				and   bastat.datdep <= wdatfinb
				/* use-index CODAUX */ no-lock  :
	
					FIND ARTAPR 
					where artapr.codsoc = bastat.codsoc
					and artapr.articl = bastat.articl
					use-index artapr-1 no-lock no-error .
	
					if not available artapr then next .
	
					assign cod-coef  = " " 	taux-coef = 0	   top-valide = no.
	
					/* 1- Test sur le code coef de l'Article                  */
					if artapr.rist-adherent[3] <> " " then cod-coef = artapr.rist-adherent[3] .
	
					/* 2- si Code-Coef = " " Test sur le code coef. de la Sous-Famille */
					if cod-coef = " " then do :
						FIND TABGES where tabges.codsoc = ""
						and   tabges.etabli = ""
						and   tabges.typtab = "ART"
						and   tabges.prefix = "SOUFAM" + artapr.famart
						and   tabges.codtab = artapr.soufam
						no-lock  no-error .
						if available tabges  then do :
							if substring(tabges.libel3[4], 1, 2) <> "  "   /* tabges.libel2[4], 1, 2 */
								then cod-coef = substring(tabges.libel3[4], 1, 2) .  /* tabges.libel2[4], 1, 2 */
						end .
					end .
	
					/* 3- si Code-Coef = " " Test sur le code coef. de la Famille */
					if cod-coef = " " then do :
						FIND TABGES where tabges.codsoc = ""
						and   tabges.etabli = ""
						and   tabges.typtab = "ART"
						and   tabges.prefix = "FAMART"
						and   tabges.codtab = artapr.famart
						no-lock  no-error .
						if available tabges then do :
							if substring(tabges.libel2[4], 1, 2) <> "  "
								then cod-coef = substring(tabges.libel2[4], 1, 2) .
						end .
					end .
	
					if cod-coef <> " " then top-valide = yes .
					if top-valide = no then do:
						if bastat.codaux = "    006098" then message bastat.codaux bastat.articl bastat.fac-ht view-as alert-box.
						
						next.
					end.
					wca-adh = wca-adh + bastat.fac-ht.
				end.
			end.
			if wdatdebl <> ? then do:
				FOR EACH lignes 
				where lignes.codsoc = codsoc-soc
				and   lignes.motcle = "VTE"
				and   lignes.typaux = b-auxapr.typaux
				and   lignes.codaux = b-auxapr.codaux
				and   lignes.nivcom >= "4"
				and   lignes.nivcom <= "6"
				and   lignes.datdep >= wdatdebl
				and   lignes.datdep <= wdatfinl
				no-lock  :
	
					FIND ARTAPR 
					where artapr.codsoc = lignes.codsoc
					and artapr.articl = lignes.articl
					use-index artapr-1 no-lock no-error .
	
					if not available artapr then next .
		
					assign cod-coef  = " "	taux-coef = 0  top-valide = no.
		
					/* 1- Test sur le code coef de l'Article                  */
					if artapr.rist-adherent[3] <> " " then cod-coef = artapr.rist-adherent[3] .
	
					/* 2- si Code-Coef = " " Test sur le code coef. de la Sous-Famille */
					if cod-coef = " " then do :
						FIND TABGES 
						where tabges.codsoc = ""
						and   tabges.etabli = ""
						and   tabges.typtab = "ART"
						and   tabges.prefix = "SOUFAM" + artapr.famart
						and   tabges.codtab = artapr.soufam
						no-lock  no-error .
	
						if available tabges then do :
							if substring(tabges.libel3[4], 1, 2) <> "  "   /* tabges.libel2[4], 1, 2 */
								then cod-coef = substring(tabges.libel3[4], 1, 2) .  /* tabges.libel2[4], 1, 2 */
						end .
					end .
		
					/* 3- si Code-Coef = " " Test sur le code coef. de la Famille */
					if cod-coef = " " then do :
						FIND TABGES 
						where tabges.codsoc = ""
						and   tabges.etabli = ""
						and   tabges.typtab = "ART"
						and   tabges.prefix = "FAMART"
						and   tabges.codtab = artapr.famart
						no-lock  no-error .
						if available tabges then do :
							if substring(tabges.libel2[4], 1, 2) <> "  "
								then cod-coef = substring(tabges.libel2[4], 1, 2) .
						end .
					end .
	
					if cod-coef <> " " then top-valide = yes .
					
					if top-valide = no then do:
						next.
					end.			
					wca-adh = wca-adh + lignes.fac-ht.
				end. 
			end.
			assign zone = trim  ( b-auxapr.codaux ) 
						+ "-EVO-CA-Exercice"  											+ ";"
						+ string ( today, "99/99/9999") 				 				+ ";"
						+ trim  ( b-auxapr.codaux ) 									+ ";"						
						+ "CA Exercice : " 
						+ string ( year ( wdatdeb ) , "9999" )          				+ "-" 
						+ string ( year ( wdatfin ) , "9999" )						 	+ ";"
						+ trim ( string ( wca-adh, "->>>>>9.99" ) )       				+ ";"     /* montant CA en cours  */
						+	";"	.
					
				put stream str2 unformatted zone skip.
				zone = "".               
				nbca = nbca  + 1.
				
			
		end.
	
		/* ecriture du fichier  */
		/**
		CODE DU RESULTAT;DATE;CODE EXPLOIT;TYPE MOUVEMENT;CA DE BASE;TAUX;MONTANT;		
		091265-EVO-CA-Exercice;06/03/2015;091265;CA Exercice : 2014-2015;999999.99;;;
		*/
	
			
	end.
end.
/* fin extraction */

disp 	codsoc-soc	format "xx"    column-label "Soc"
		nbcap       format ">>>>>9"  column-label "Solde Capital" 
		nbca        format ">>>>>9"  column-label "Evo Ca " 
with frame capital title "EXTRACTION HEBDO CAPITAL - CA" + string( today, "99/99/9999") + " " + string( time , "hh:mm:ss") .


if lookup ("CS", l-tables) <> 0  then output stream str1 close.
if lookup ("CA", l-tables) <> 0  then output stream str2 close.


pause 2 .

/* fermeture des fichiers de sortie */
output stream str1 close.       
output stream str2 close.       
	
run sel-del.p ( sel-applic, sel-filtre, sel-sequence, yes ) .
session:system-alert-boxes = no.
