/*==========================================================================*/
/*                           E X T - N E O D . I                            */
/* Extraction des donnes pour asatic : agreo + portail adh                  */
/*--------------------------------------------------------------------------*/
/* Chargement date minimum de traitement                                    */
/*==========================================================================*/

/* {1} = no-lock ou exclusive-lock */

assign typtab = "PRM"
       prefix = "ext-asa"
       codtab = "DERN-TRAIT"
       .

FIND DATE-TABGES where date-tabges.codsoc = ""
                 and   date-tabges.etabli = ""
                 and   date-tabges.typtab = typtab
                 and   date-tabges.prefix = prefix
                 and   date-tabges.codtab = codtab
                 {1} no-error.

if not available date-tabges
then do :
    create date-tabges .
    assign date-tabges.codsoc    = ""
           date-tabges.etabli    = ""
           date-tabges.typtab    = typtab
           date-tabges.prefix    = prefix
           date-tabges.codtab    = codtab

           date-tabges.dattab[1] = today - 1
           .
end.

date-debut = date-tabges.dattab[1] + 1.
