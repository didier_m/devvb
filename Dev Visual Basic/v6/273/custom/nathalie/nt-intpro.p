/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! Deal/nathalie/cmm/nt-prgclient.p                                                            !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intercvention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 19614 30-06-10!New!bp             !mise en place d'un lanceur pour programmes maison des clients         !
!-------------------------------------------------------------------------------------------------------------!
!                             I N C L U D E S                !                    T A B L E S                 !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!d-brokmini.i          !new                                  !                                                !
!bat-ini.i             !new                                  !                                                !
!select.i              !new                                 !                                                !
!aide.i                !new                                  !                                                !
!maquette.i            !new                                  !                                                !
!bat-cha.i             !"sel-applic" "selection-applic" "a"  !                                                !
!regs-cha.i            !                                     !                                                !
!selectpt.i            !                                     !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
!_____________________________________________________________________________________________________________!
!                                                  P R O C E D U R E S                                        !
!_____________________________________________________________________________________________________________!
!TRAITEMENT            !                                                                                      !
&End
**************************************************************************************************************/



{ d-brokmini.i new }


{ bat-ini.i    new }
{ select.i     new }
{ aide.i       new }
{ maquette.i   new }  

/* { connect.i      } */

{ edigvse0.i NEW }


DEFINE stream maquette.   /*  canal maquette */
define stream entree .
define stream sortiea .
define stream sortiep .

DEFINE VARIABLE x-prgclient AS CHARACTER   NO-UNDO.
def var    x-alpha1         as char       no-undo . 
def var    x-alpha2         as char       no-undo .
def var    x-alpha3         as char       no-undo .
def var    x-alpha4         as char       no-undo .
def var    x-alpha5         as char       no-undo .
def var    x-alpha6         as char       no-undo .
def var    x-alpha7         as char       no-undo .
def var    x-alpha8         as char       no-undo .
def var    x-alpha9         as char       no-undo .
DEF VAR    x-alpha10        as char       no-undo .

def input parameter  fichier as char format "x(12)" .

run bat-lec.p ( fichier ) .


/*---------------------------------------------------------*/
/* Prise des selections et chargement des bornes mini-maxi */
/*---------------------------------------------------------*/


{ bat-cha.i  "sel-applic"     "selection-applic"   "a"          }
{ bat-cha.i  "sel-filtre"     "selection-filtre"   "a"          }
{ bat-cha.i  "sel-sequence"   "selection-sequence" "a"          }
{ bat-cha.i  "codsoc"         "glo-codsoc"         "a"          }
{ bat-cha.i  "etabli"         "glo-etabli"         "a"          }

{ bat-cha.i  "ma-impri"       "ma-impri"           "a"          }   
{ bat-cha.i  "ma-maquet"      "ma-maquet"          "a"          }   
{ bat-cha.i  "lib-prog"       "lib-prog"           "a"          }

{ bat-cha.i  "prgclient"      "x-prgclient"        "a"          }   

{ bat-cha.i  "alpha1"         "x-alpha1"           "a"          }   
{ bat-cha.i  "alpha2"         "x-alpha2"           "a"          }
{ bat-cha.i  "alpha3"         "x-alpha3"           "a"          }   
{ bat-cha.i  "alpha4"         "x-alpha4"           "a"          }
{ bat-cha.i  "alpha5"         "x-alpha5"           "a"          }   
{ bat-cha.i  "alpha6"         "x-alpha6"           "a"          }
{ bat-cha.i  "alpha7"         "x-alpha7"           "a"          }   
{ bat-cha.i  "alpha8"         "x-alpha8"           "a"          }
{ bat-cha.i  "alpha9"         "x-alpha9"           "a"          }   
{ bat-cha.i  "alpha10"        "x-alpha10"          "a"          }


run select-c.p.  

regs-app = "elodie".
{regs-cha.i }
regs-app = "nathalie".
{regs-cha.i}

deal-setsocreg ( "" ) .
deal-setsocreg ( glo-codsoc ) .


run sel-del.p ( selection-applic, selection-filtre, selection-sequence, yes ) .  



RUN traitement .


PROCEDURE TRAITEMENT :
    
    
    OUTPUT /* STREAM maquette */  TO VALUE( ma-impri ) .

    def var wtyptrt        as char                       no-undo .
    def var wlibtrt        as char                       no-undo .
    
    def var wtopmaj        as char                       no-undo .
    def var wlibmaj        as char                       no-undo .
        
    def var wmessage       as char                       no-undo .
    def var x-x            as char                       no-undo .
    
    
    def var liglu          as char                       no-undo .
    def var liglu-1        as char                       no-undo .
    
    def var wzone          as char                       no-undo .
    def var wligI          as char                       no-undo .
    
       
    def var wficent        as char initial "/applic/li/arrivee/gvse/promo" .
    def var wficsor        as char initial "/applic/li/arrivee/gvse/advpromo" .
    def var wfictrav1      as char initial "/applic/li/travail/advpromo" .
    def var wfictrav2      as char initial "/applic/li/travail/xpromo" .

    def var topfic1        as log.
    def var topfic2        as log.
    
    def var wficsauv       as char .
    def var nbliglu        as int .
    def var nbliga         as int .
    def var nbligp         as int .
    
    assign wmessage = "" .

    
    /* recup des paramètres */  
    assign wtyptrt = upper (trim ( x-alpha1 )  )
           wtopmaj = upper (trim ( x-alpha2 )  )
           .
    
    if wtyptrt = "E" then wlibtrt = "Epuration".
                     else wlibtrt = "Selection".

    if wtopmaj = "O" then wlibmaj = "Traitement avec MAJ".
                     else wlibmaj = "Traitement SIMULATION".
    
    /* verif prersence fichier promo */
    
    if search ( wficent ) = ? then 
        assign wmessage = " Pas de fichier en provenance de GVSE a traiter ".
        
    if wmessage <> "" then do:
        message /* stream maquette  */  " **** FIN ANORMALE DU TRAITEMENT ****" .
        message /* stream maquette */  wmessage .
        leave .
    end.
                  
    wmessage = "".
    
    message /* stream maquette  */ "Integration des Promos GVSE le :" string (today, "99/99/9999")  " a "  string ( time, "hh:mm") .
    
    message /* stream maquette */  "Traitement : " wtyptrt  "-"  wlibtrt .
    message /* stream maquette */  "MAJ        : " wtopmaj  "-"  wlibmaj skip .
    
    input stream entree from value ( wficent ) .
    output stream sortiea to value ( wfictrav1 ) .
    output stream sortiep to value ( wfictrav2 ) .
    
    assign wligI = "" wzone = "".
    
    lect-fic:
    
    repeat:
        
        import stream entree unformatted wzone.
        
        nbliglu = nbliglu + 1 .
        
        if substr ( wzone, 1 , 1 ) = "I" then do:
            /* if wligI <> "" then nbligIeli = nbligIeli + 1 . */
            wligI = wzone .
            next lect-fic.
        end.
    
        x-x = substr ( wzone , 2 , 5 ) .
        { select-z.i  "numero"  "x-x" }
        run select-t.p.
        if selection-ok <> "" then do :
            if nbligp = 0 then do :
                put stream sortiep unformatted wligI skip.
                nbligp = nbligp + 1 .
            end.
            put stream sortiep unformatted wzone skip.
            assign nbligp = nbligp + 1 .
            next lect-fic.
        end. 
        else do :
            if nbliga = 0 then do:
                put stream sortiea unformatted wligI skip.
                assign nbliga = nbliga + 1 .
            end.
            put stream sortiea unformatted wzone skip.
            assign nbliga = nbliga + 1 .
            next lect-fic.
        end.
    end.
    
    input stream entree close.
    output stream sortiea close .
    output stream sortiep close .
    
    message "Fichier en entree : " wficent  skip
            "Nombre de lignes lues: " nbliglu skip ( 1 ).
    
    message "- Nombre de lignes a traiter ( integration ou purge ) : " nbliga skip 
            "- Nombre de lignes a sauvegarder  : " nbligp skip .
            
        
        
    If wtyptrt = "S" then do:   /* si type de traitement = Selection */
        if nbliga = 0 then do:
            message "aucune ligne selectionnée....".
            os-delete value ( wfictrav2 ) .         
            os-delete value ( wfictrav1 ) .         
        end.
        else do:
            message "lignes selectionnes :" nbliga wtopmaj.
            output close.
            os-append value ( wfictrav1 ) value ( ma-impri ).
            output to value ( ma-impri) append.
                
            if wtopmaj = "N" then do:
                
                os-delete value ( wfictrav1 ) .         
                message " Traitement SANS MAJ, le fichier PROMO initial de GVSE reste inchange" .
                os-delete value ( wfictrav2 ) .         
                
            end.
            else do:

                assign wficsauv = "/applic/li/arrivee/gvse/traite/promo"  + "-" + string ( today , "99999999") + "-" + replace ( string ( time , "hh:mm:ss") , ":" , "" ) .
            
                message " Traitement d' Integration AVEC MAJ , sauvegarde du fichier PROMO initial de GVSE  " skip 
                        " Sauvegarde du fichier : " wficent skip 
                        "en : " wficsauv .
            
                os-rename value ( wficent ) value ( wficsauv ) .

                message " Copie du nouveau fichier PROMO GVSE ...." skip
                        wfictrav2 "===>" wficent .
                    
                os-rename value ( wfictrav2 ) value ( wficent ) .
				
				if os-error = 10 then 
					os-append value ( wfictrav2 ) value ( wficent ) .
                
				os-command value ( " chmod 777 " + wficent ) .
                if os-error <> 0 then message "erreur n° " os-error.
            
                assign wficsauv = "/applic/li/arrivee/gvse/advpromo" .
            
                message " Copie du fichier a integrer en REEL   ...." skip
                        wfictrav1 "===>" wficsauv  .
            
                os-rename value ( wfictrav1 ) value ( wficsauv ) .
                if os-error = 10 then 
					os-append  value ( wfictrav1 ) value ( wficsauv ) .
                
            end .
        end.
    end .
    else do :
        if nbliga = 0 then do:
            message "aucune ligne selectionnée....".
            os-delete value ( wfictrav2 ) .         
            os-delete value ( wfictrav1 ) .         
        end.
        else do:
            message "lignes selectionnes :" nbliga.
            output close.
            os-append value ( wfictrav1) value ( ma-impri).
            output to value ( ma-impri) append.
                
            if wtopmaj = "N" then do :
                            
                message " Traitement de purge sans maj, le fichier PROMO de GVSE reste inchange" skip
                        " supression des fichiers de travail .... " .
            
                os-delete value ( wfictrav1 ) .
                os-delete value ( wfictrav2 ) .
            end.
            else do:
                assign wficsauv =  "/applic/li/arrivee/gvse/traite/promo"  + "-" + string ( today , "99999999") + "-" + replace ( string ( time , "hh:mm:ss") , ":" , "" ) .
            
                message " Traitement de purge AVEC MISE A JOUR ...." skip
                        " Sauvegarde du fichier : " wficent skip 
                        " en : " wficsauv .
                os-rename value ( wficent ) value ( wficsauv ) .
    			if os-error = 10 then 
					os-append value ( wficent ) value ( wficsauv ) .
    
                assign wficsauv =  "/applic/li/arrivee/gvse/traite/pur_promo"  + "-" + string ( today , "99999999") + "-" + replace ( string ( time , "hh:mm:ss") , ":" , "" ) .        
                message " Archivage du  fichier des lignes purgees ..." .
                os-rename value ( wfictrav1 ) value ( wficsauv ) .
				if os-error = 10 then 
					os-append value ( wfictrav1 ) value ( wficsauv ) .
				
                message " Copie du nouveau fichier PROMO GVSE ..." .
                os-rename value ( wfictrav2 ) value ( wficent ) .
				if os-error = 10 then 
					os-append value ( wfictrav1 ) value ( wficent ) .
				
                os-command value ( "chmod 777 " + wficent ).
            
            end .
        end .
    end.
    
    message "Fin selection des promos PROMO GVSE le " string (today,"99/99/9999") "a" string ( time , "HH:MM:SS" ).

    output stream maquette close.

end. /* fin de la procedure */
{ selectpt.i }

RUN sel-del.p(selection-applic, selection-filtre, selection-sequence, YES).

/* FIN */  
