/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! Deal/nathalie/tty/s-codaux.i                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 26964 20-10-11!Bug!cch            !Correction ligne 68 jamais atteinte ( 2 fois undo, retry )            !
!V52 15928 04-06-07!Evo!cch            !Modifications sur site EUREA semaine 200721                           !
!V52 15898 16-05-07!Bug!cch            !Correction pble Tiers inex. si recherche sur Tiers final ( EUREA DISTR!
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
        /*---------------------------------------*/
        /* include parametre de la saisie codaux */
        /*---------------------------------------*/

regs-app = "ELODIE" .
{ regs-cha.i }
regs-fileacc = "AUXILI" + typaux .
{regs-rec.i}
codsoc-aux = regs-soc .

libel = '{3}' .

REPEAT :
        if substring( {1} , 1 , 1 ) = "*"  then
           substring( {1} , 1 , 1 ) = "" .

        else do :
            { fncompo.i {1} {2} {4} }

            if keyfunction( lastkey ) <> "RETURN"    and
               keyfunction( lastkey ) <> "FIND "     and
               keyfunction( lastkey ) <> "NEW-LINE"  and
               {1} entered    then  undo , retry .

            if keyfunction( lastkey ) = "END-ERROR"   or
               keyfunction( lastkey ) = "NEW-LINE"    or
               keyfunction( lastkey ) = "CURSOR-UP"  then  leave .

            if keyfunction( lastkey ) = "FIND"  then
            do :
                regs-fileacc = "auxspe" .
                { regs-rec.i }
                assign libel   = {1}
                       codsoc  = regs-soc .
                { run.i tie-re00.p }
                if codtab = "$rien$"  or  codtab = ?  then  undo , retry .
                {1} = codtab .
            end.

            if {1} = "" and libel <> "" then leave .
            if {1} = "" then undo , retry .

            { cadr-aux.i   "regs-soc"  "typaux"  "{1}" }
        end .

        display {1} with frame {2} .

        Find Auxili Where Auxili.codsoc = codsoc-aux AND
                          Auxili.typaux = typaux     AND
                          Auxili.codaux = {1}        no-lock no-error.

        if not available auxili
        then do:
             message typaux {1} ": Inexistant en Compta. ..." . bell . bell .
             readkey pause 2 . undo , retry .
        end .
/*
        if program-name( 1 ) <> "auxi-009.p" and
           program-name( 1 ) <> "auxg-007.p"
        then do :
            { l-tiespe.i "auxspe" "typaux" "{1}" }
        end .
        else do :
            { l-tiespe.i "auxapr" "typaux" "{1}" }
        end .

        if not available auxspe
        then do :
           message typaux {1} ": Inexistant en Gestion ..." . bell . bell .
           readkey pause 2 . undo , retry .
        end .
*/
        leave .
end .

