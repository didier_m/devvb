/* Programme : lisinv.p visu des inventaires */

def var wsoc as char format "xx" column-label "Soci�t�".
def var wmag as char format "xxx".
def var wart as char format "xxxxxx" column-label "Article".
def var iart as int  format ">>>>>9".
def var weve as char format "xxx" initial "INV" column-label "Evenement".
def var ilig as int.
def var tot1 as dec decimals 3.
def var tot2 as dec decimals 3.

repeat:

ilig = 0.

update weve.
update wsoc.
/*
update wmag.
update iart.
wart = string(iart , "999999").
*/
update wart.

tot1 = 0.
tot2 = 0.

FOR EACH STOINV where stoinv.codsoc       = wsoc
                  /* and stoinv.magasin      = wmag */
                  and stoinv.date-invent >= date(05, 15, year(today) )
                  and stoinv.articl       = wart
                  and stoinv.evenement    = weve
                  no-lock use-index primaire:
ilig = ilig + 1.
if ilig > 11
  then do:
       pause.
       ilig = 1.
       end.
tot1 = tot1 + qte-stock.
tot2 = tot2 + qte-invent.

display
/* evenement format "xxx" column-label "Eve" */
codsoc column-label "So" format "xx"
magasin column-label "Mag"
date-inv
opemaj column-label "OPE!Maj" heumaj
articl format "xxxxxx"
qte-stock format "ZZZZZ9.999-"
qte-invent format "ZZZZZ9.999-"
stat-regul column-label "SR"
prx-invent  format ">>>>9.999"
.

end.

message "FIN.. Total Qte STOCK" tot1 " Total Qte INVENT" tot2. pause.

end.