/*==========================================================================*/
/*                           E X T - D S Y 0 . I                            */
/* Extraction des donnees pour Daisy                                        */
/*--------------------------------------------------------------------------*/
/* Variables et Frames                                                      */
/*==========================================================================*/

/* Variables */
def var ind       as int                 no-undo.
def var lib-table as char format "x(76)" no-undo.

def {1} shared var libelle      as char extent 30           no-undo.
def {1} shared var date-debut   as date format "99/99/9999" no-undo.
def {1} shared var wcodsoc      as char format "xx"         no-undo.
def {1} shared var wlibsoc      as char format "x(30)"      no-undo.
def {1} shared var periph       as char format "x(12)"      no-undo.
def {1} shared var topmaj       as log format  "oui/non"    no-undo.
def {1} shared var wfichier     as char format "x(40)"      no-undo.

def {1} shared var lissoc       as char                     no-undo initial "01,26,08".

/* Dessin des Frames */
def {1} shared frame fr-saisie.
def {1} shared frame fr-trt.

Form "Societe � importer :"  wcodsoc wlibsoc skip
     "Fichier � importer :"  wfichier   skip
     "Date Appli TARIFS  :"  date-debut skip
     "Mise a jour        :"  topmaj     skip
     "Fichier Log        :"  periph     skip
     with frame fr-saisie row 3 centered no-label overlay { v6frame.i }.

Form skip lib-table skip
     with frame fr-trt row 17 centered no-label overlay { v6frame.i }.
     