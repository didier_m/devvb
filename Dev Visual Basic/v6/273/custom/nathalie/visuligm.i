/* modifie par CCO le 27/07/2009 demande Sandrine CHAPUIS */

/* Ajout index libelle article lib-qte devant articl      */

def {1} shared temp-table sortie
field t-chrono as int
field clelig   as char      /* codsoc, motcle, typcom, numbon, chrono */
field climag   as char format "x"
field famille  as char format "xxx"
field mag      as char format "xxx"
field statut   as char format "x"
field log      as char format "xxx"
field bon      as int format ">>>>>9"
field chrono   as int
field articl   as char format "x(6)"
field lib-qte  as char format "x(17)"
field qteral   as dec format ">>>>9-"
field partiel  as char format "x"
field ligpart  like lignes.partiel
field rel      as char format "x"
field datdep   as date format "99/99/99"
field datliv   as date format "99/99/99"
field ref-cli  as char format "x(20)"
field depot    as char format "x(3)"
index chrono 
      t-chrono
index cleligtab 
      clelig
index cde 
      bon descending chrono
index date 
      datdep bon chrono
index art 
      lib-qte articl bon chrono
index statut 
      ligpart bon chrono
index client 
      ref-cli bon articl chrono.