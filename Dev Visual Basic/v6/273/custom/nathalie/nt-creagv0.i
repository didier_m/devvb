/* -------------------------------------------------------------------------- */
/*              creart-e01.i CREATION AUTOMATIQUE ARTICLES :                  */
/* -------------------------------------------------------------------------- */
/* variables communes a l'extraction et a l'integration                       */
/* -------------------------------------------------------------------------- */
/* DGR 100204 : rajout top pour extraire articles deja crees                  */
/* DGR 060904 : suppression des " et ' dans designation                       */
/* DGR 180506 : creation auto en societe 01,05,08                             */
/* DGR 051208 : famart2                                                       */
/* DGR 100610 : creer les produits resau GV sur 08                            */
/* DGR 121216 : code PAN                                                      */ 
/* DGR 06062019 : ajout gestion soci�t� grassot                               */
/* -------------------------------------------------------------------------- */

/* variables pour edition */

def {1} shared var b-rej            AS LOGICAL                    no-undo.
def {1} shared var date-stoarr      like stoarr.date-arrete       no-undo.

def {1} shared var i                as int                        no-undo.
def {1} shared var ind              AS INTEGER                    no-undo.

def {1} shared var l-soc            AS CHAR     no-undo initial "00,01,08,88".
def {1} shared var l-cresoc         AS CHAR                       no-undo.
def {1} shared var l-versoc         AS CHAR                       no-undo.
def {1} shared var lib-l-soc        AS CHAR format "x(20)"        no-undo.
def {1} shared var l-soc2           AS CHAR                       no-undo.
def {1} shared var wsoc             AS CHAR                       no-undo.
def {1} shared var wsoc2            as char                       no-undo.

def {1} shared var lisplf           as char                       no-undo initial "    802190".
def {1} shared var libplf           as char                       no-undo initial "LISADIS".

def {1} shared var lisreseauGV      as char                       no-undo initial "9004,9005,9007,9008".
def {1} shared var lisreseauASEc    as char                       no-undo initial "9001,9002,9003,9004,9005,9007,9008,9010".

def {1} shared var lok              as log                        no-undo.
def {1} shared var sel-sequence     as char format "x(12)"        no-undo.
def {1} shared var sequence         as char format "x(05)"        no-undo.
def {1} shared var snomfic          as char format "x(12)"        no-undo.

def {1} shared var valid            as logical format "O/N"       no-undo.
def {1} shared var wcodaux          as char format "x(10)"        no-undo.
def {1} shared var wcodfou          as char format "x(30)"        no-undo.
def {1} shared var wcodfou1         as char format "x(9)"         no-undo.
def {1} shared var wauxilicodsoc    as char                       no-undo.
def {1} shared var wcodtva          like artapr.tva               no-undo.
def {1} shared var wtxtva           as dec                        no-undo.
def {1} shared var wcresoc          AS CHAR                       no-undo.
def {1} shared var wedi-etiq        LIKE artapr.edi-etiq          no-undo.
def {1} shared var wfic             AS CHAR FORMAT "x(32)"        no-undo.
def {1} shared var wficok           AS CHAR FORMAT "x(30)"        no-undo.
def {1} shared var wficre           AS CHAR FORMAT "x(30)"        no-undo.
def {1} shared var wgencod          LIKE artapr.gencod            no-undo.
def {1} shared var wlib             AS CHAR FORMAT "x(32)"        no-undo.
def {1} shared var wlibred          AS CHAR FORMAT "x(20)"        no-undo.
def {1} shared var wlibaux          as char format "x(30)"        no-undo.
def {1} shared var wlibdat          as char format "x(30)"        no-undo.
def {1} shared var wlibfou          as char format "x(30)"        no-undo.
def {1} shared var wlibfou1         as char format "x(30)"        no-undo.
def {1} shared var wfourpl          as char format "x(30)"        no-undo.

def {1} shared var wdejacree        AS char format "x"            no-undo.
def {1} shared var wlibdeja         as char format "x(32)"        no-undo.

def {1} shared var wdeee          as dec decimals 4 no-undo .
def {1} shared var wecom          as dec decimals 2 no-undo .
def {1} shared var wtxtvas        as char no-undo .
def {1} shared var zarticl like artapr.articl no-undo.



def {1} shared var wlibsim          AS CHAR FORMAT "x(32)"        no-undo.
def {1} shared var wsim             AS char format "x"            no-undo.
def {1} shared var wtitre           as char format "x(32)"        no-undo.
def {1} shared var wtopcre          as char                       no-undo.

def {1} shared var wtypdat          as char format "x"             no-undo.
def {1} shared var ddatdeb          as date format "99/99/9999"   no-undo.
def {1} shared var ddatfin          as date format "99/99/9999"   no-undo.

def {1} shared var wmotif           AS CHAR FORMAT "x(100)"       no-undo.
def {1} shared var wmotifrej        as char                       no-undo.
def {1} shared var wnbcre           AS INT  FORMAT ">>>>9"        no-undo.
def {1} shared var wnblus           AS INT  FORMAT ">>>>9"        no-undo.
def {1} shared var wnbrej           AS INT  FORMAT ">>>>9"        no-undo.

DEF {1} shared BUFFER b-artapr FOR artapr.
DEF {1} shared BUFFER b-artbis FOR artbis.
DEF {1} shared BUFFER b-artpla FOR artpla.

def {1} shared var wpds             LIKE artapr.pds               no-undo.
def {1} shared var wpmp-init        LIKE artapr.pmp-init          no-undo.
def {1} shared var wprovenance      LIKE artapr.provenance        no-undo.
def {1} shared var wcodblo          LIKE artapr.code-blocage      no-undo.
def {1} shared var wstatut          AS CHAR format "x"            no-undo.
def {1} shared var wprx-modif       LIKE artapr.prx-modif         no-undo.
def {1} shared var wpxv-indic       LIKE artapr.pxv-indic         no-undo.
def {1} shared var wreffou          as char                       no-undo.
def {1} shared var wfamart          LIKE artapr.famart            no-undo.
def {1} shared var wsoufam          LIKE artapr.soufam            no-undo.
def {1} shared var wsssfam          LIKE artapr.sssfam            no-undo.
def {1} shared var wfamart2         LIKE artapr.famart2           no-undo.
def {1} shared var wsoufam2         LIKE artapr.soufam2           no-undo.
def {1} shared var wsssfam2         LIKE artapr.sssfam2           no-undo.
def {1} shared var wstocke          as char format "x"            no-undo.
def {1} shared var wtypo            LIKE artapr.typologie         no-undo.
def {1} shared var wassort          as char format "x(5)"         no-undo.
def {1} shared var wpan             as char format "x(5)"         no-undo.

def {1} shared var wsurcondita      as char                       no-undo.
def {1} shared var wcondita         as char                       no-undo.
def {1} shared var wconditai        like artbis.surcondit         no-undo.
def {1} shared var wconditas        as char                       no-undo.

def {1} shared var wuca             as char                       no-undo.
def {1} shared var wuvte            as char                       no-undo.
def {1} shared var wusto            as char                       no-undo.
def {1} shared var wufac            as char                       no-undo.
def {1} shared var wufaca           as char                       no-undo.

def {1} shared var wcoefcdea        as int                        no-undo.
def {1} shared var wcoefcdeas       as char                       no-undo.

def {1} shared var wsurconditv      as char                       no-undo.
def {1} shared var wconditv         as char                       no-undo.
def {1} shared var wconditvi        like artbis.surcondit         no-undo.
def {1} shared var wconditvs        as char                       no-undo.


def {1} shared var wzone1           AS CHAR                       no-undo.
def {1} shared var wzone2           AS CHAR                       no-undo.
def {1} shared var zone-trav        as char                       no-undo.
def {1} shared var warticl-fou      like artapr.articl            no-undo.
def {1} shared var warticl          like artapr.articl            no-undo.

def {1} shared var wvolume          like artapr.volume            no-undo.
def {1} shared var wedieti          like artapr.edi-etiq          no-undo.
def {1} shared var wprxmodif        like artapr.prx-modif         no-undo.
def {1} shared var wcoefvtefac      as int                        no-undo.
def {1} shared var wcoefvtefacs     as char                       no-undo.
def {1} shared var wstat-rayon      like artapr.stat-rayon        no-undo.
def {1} shared var wdeci            as deci                       no-undo.
def {1} shared var wpxv-indics      as char                       no-undo.
def {1} shared var wvolumes         as char                       no-undo.
def {1} shared var wpdss            as char                       no-undo.
def {1} shared var wpmp-inits       as char                       no-undo.
def {1} shared var wdatcs           as char                       no-undo.
def {1} shared var wdatms           as char                       no-undo.
def {1} shared var wartrpl          as char format "x(6)"         no-undo.
def {1} shared var wartrpl2         as char format "x(6)"         no-undo.

/* variable pour maj date et heure modif */
def {1} shared var wdatc            like artapr.datcre            no-undo.
def {1} shared var wdatm            like artapr.datmaj            no-undo.
def {1} shared var wope             LIKE artapr.opecre            no-undo.
def {1} shared var wheu             LIKE artapr.heumaj            no-undo.


/* variables pour EDI directe */
def {1} shared var val-min-uvc      as dec decimals 3             no-undo.
def {1} shared var val-min-fou      as dec decimals 3             no-undo.
def {1} shared var val-min-franco   as dec decimals 3             no-undo.
def {1} shared var par-comb-uvc     as dec decimals 3             no-undo.
def {1} shared var delai            as dec decimals 3             no-undo.
def {1} shared var un-min-fou       as char                       no-undo.
def {1} shared var un-min-franco    as char                       no-undo.
def {1} shared var filiere-art      as char                       no-undo.
def {1} shared var wz1              as char                       no-undo.
def {1} shared var wz2              as char                       no-undo.

def {1} shared var version          as char                       no-undo initial "v7 du 30/12/2016".

PROCEDURE sup-car:
  def input-output parameter chaine1 as char.
  def var liste as char initial '"'.
  def var i as int.
  do i = 1 to num-entries (liste):
    do while  INDEX(chaine1,entry(i,liste)) <> 0:
      overlay( chaine1, INDEX(chaine1,entry(i,liste)), 1 , "CHARACTER") = " ".
    end.
  end.
end procedure.
