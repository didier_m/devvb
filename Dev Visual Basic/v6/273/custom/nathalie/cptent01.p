/* cptent01.p
   Lanceur comptage des entetes des depots pour detection journees manquantes
*/

{ connect.i      }
{ bat-ini.i  new }
{ cptent00.i new }

def var sequence as char format "x(5)" no-undo.

{ fr-cadre.i lib-prog }

/* determination No sequence operateur pour spoolname debut */
assign codsoc = ""
       etabli = ""
       typtab = "ope"
       prefix = "nume-spool"
       codtab = operat
       .

run opeseq.p ("req", output sequence, output sel-sequence).
/* determination No sequence operateur pour spoolname fin */

scodsoc = codsoc-soc .

repeat :

    display scodsoc snumbonmin sdatbonmin snumbonmax sdatbonmax sdatedimax wcptpag
    	    sel-sequence stimedebut stimefin with frame fr-saisie .

    update sdatbonmin
           help "date bon inferieure"
           auto-return with frame fr-saisie .

    update sdatbonmax
           help "date bon superieure"
           auto-return with frame fr-saisie .

    leave .

end. /* repeat */

if keyfunction( lastkey ) <> "END-ERROR"
then do :

    batch-tenu = yes . /* Gestion en batch */

    { bat-zon.i  "codsoc"        "codsoc-soc"    "c"  "''"  "'Societe'" }
    { bat-zon.i  "lib-prog"      "lib-prog"      "c"  "''"  "'Traitement'" }
    { bat-zon.i  "sdatbonmin"    "sdatbonmin"    "c"  "''"  "'Date-Minimum'" }
    { bat-zon.i  "sdatbonmax"    "sdatbonmax"    "c"  "''"  "'Date-Maximum'" }
    { bat-zon.i  "sel-sequence"  "sel-sequence"  "c"  "''"  "'Fichier-Edition'" }

    { bat-maj.i "cptent02.p" }

end .

hide frame fr-saisie no-pause .
hide frame fr-titre  no-pause .
hide frame fr-cadre  no-pause .
