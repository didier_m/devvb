/* Prog: visuligm.p | MPO | Liste des lignes de commandes Eurea Distri */

/* DGR 23092011 : test sur soc pour nom des clients BIOAGRI */
/* CCO 02022016 : Ajout boucle CDR,CDC et filtre sous famille */
/* CCO 25022016 : Ne pas faire apparaitre ligne qteral = 0    */

{ connect.i     }
{ aide.i     new}
{ visuligm.i new}

/* VARIABLES */

def buffer c-sortie  for sortie.
def buffer b-lignes  for lignes.
def buffer b-ligspe  for ligspe.

def var x-recid      as recid.
def var delta-lign   as int      no-undo .

def var wgo          as char format "x" initial "O".
def var Log          as char format "xxx" initial "970".
def var Mag          as char format "xxxxxx".
def var Etat         as char format "x" initial "*".
def var Tri          as char format "x" initial "C".
def var C-M          as char format "x" initial "".
def var bon          as int  format ">>>>>9".
def var wclelig      as char.
def var nbl          as int.
def var ligtab       as int.
def var Fam          as char format "xxx".
def var wrel         as char format "x".
def var wlib-qte     as char format "x(17)".
def var wpartiel     as char format "x".
def var wmag         as char format "xxx".
def var wref         as char format "x(11)".
def var wqteral      as dec format ">>>>9-".
def var wdep         as char format "xxx".
def var l-typ        as char initial "CDC,CDR".
def var wtyp         as char.
def var aa           as int.
def var sFam         as char format "xxx".

def var auxili-soc   as char.


/* SAISIE */

repeat :

Log = "ent".
Etat="*".
Tri="C".
C-M="".
Fam = "".
sFam = "".
wgo = "O".


DO WITH FRAME fr-saisie :
    REPEAT WITH FRAME fr-saisie CENTERED no-label :
    
        FORM    
            "Magasin  : " wmag skip
            "D�pots   : " log  skip
            "Statut   : " Etat skip
            "Tri      : " Tri  skip
            "Cli/Mag  : " C-M  skip
            "Familles : " Fam  " Sous Famille : " sFam skip
            "Lancement: " wgo  skip
        with title "Liste des commandes d'un magasin" centered.    
            
if zone-charg[1] <> "MAG" then do :
    update wmag
    help "Code Magasin 'xxx'".
    FIND  magasi
    WHERE magasi.codsoc = ""
    AND   magasi.codtab = wmag
    NO-LOCK NO-ERROR.
    if not available magasi then do :
        message "Magasin '" + wmag "' inexistant".
        pause. bell. bell. undo, retry.
    end.
    else Mag = magasi.codaux-cli .
end.
else do :

    FIND tabges
    WHERE    tabges.codsoc = ""
    AND      tabges.etabli = ""
    AND      tabges.typtab = "APR"
    AND      tabges.prefix = "OPERATEUR"
    AND      tabges.codtab = operat
    NO-LOCK NO-ERROR.
    
    IF AVAILABLE tabges THEN
        wmag = tabges.libel2[ 3 ] .
        
    display wmag.

    FIND  magasi
    WHERE magasi.codsoc = ""
    AND   magasi.codtab = wmag
    NO-LOCK NO-ERROR.
    if not available magasi then do :
        message "Magasin '" + wmag "' inexistant".
        pause. bell. bell. undo, retry.
    end.
    else Mag = magasi.codaux-cli .
end.

display "'   ' : Tout" skip
        "'ent' : Tout sauf 976" skip
        "'976' : Direct"              
skip with frame bubu.


for each magasi
where magasi.codsoc = ""
and   magasi.societe = "45"
and   ( magasi.type-mag begins "LOG" or
        magasi.type-mag begins "BLK" )
and magasi.codtab <> "979"        /* Multipro Attente */
and magasi.codtab <> "980"        /* Vernay           */
and magasi.codtab <> "976"        /* Direct           */
no-lock :

    display magasi.codtab no-label ":" magasi.libel1[1] no-label skip.

end.

update Log
help "Depot logistique".
if Log <> "" and Log <> "ent" 
then do :

    find  magasi
    where magasi.codsoc = ""
    and   magasi.societe = "45"
    and   ( magasi.type-mag begins "LOG" or
            magasi.type-mag begins "BLK" ) 
    and   magasi.codtab = Log
    no-lock no-error.
    if not available magasi then do :
        message "D�pot inexistant". pause. bell. bell. undo, retry.
    end.        

end. 
       
hide frame bubu.
       
display "Statut :" skip
        "   '*' = Tout" skip
        "   ' ' = Non Trait�" skip
        "   'R' = Reserv�" skip
        "   'O' = Livrable" skip
        "   'X' = BP Edite sur Logistique" skip
        "   'Z' = Supprim�" skip
with frame toto.

update Etat
       validate (Etat = "*" or Etat = " " or Etat = "R" or Etat = "O"
              or Etat = "X" or Etat = "Z" ,
       "Valeurs autorisees : *, blanc, R, O, X ou Z").

hide frame toto.

update Tri
       help "Tri par : C=Cde, S=Statut, A=Article, D=DateLiv, L=Client"
       validate (tri = "C" or tri = "S" or tri = "A"or tri = "D" or tri = "L",
       "Valeurs autorisees : C, P, A, D ou L").
update C-M
       help "Cde Client ou Magasin : ' '=Tout, 'C'=Client, 'M'=Magasin"
       validate (C-M = "" or C-M = "C" or C-M = "M" ,
       "Valeurs autorisees : Blanc ou C ou M").

display "Choix possibles :" skip
        "" skip
        "  - ' ' pour toutes." skip
        "  - 'GP' pour les familles Grand Public (001 � 010)." skip
        "  - 'AGR' pour les familles > 011." skip
        "  - un Code famille." 
with frame titi.         

update Fam.
    if Fam <> "" and Fam <> "GP" and Fam <> "AGR" 
    then do :
        FIND  tabges
        WHERE tabges.codsoc = ""
        AND   tabges.etabli = ""
        AND   tabges.typtab = "ART"
        AND   tabges.prefix = "FAMART"
        AND   tabges.codtab = Fam
        NO-LOCK NO-ERROR.
        IF NOT AVAILABLE tabges
        THEN DO :
            MESSAGE "Famille "
            Fam " interdite ou inexistante".
            PAUSE. BELL . BELL . READKEY PAUSE 1. UNDO, RETRY.
        END.
    end.

update sFam.

hide frame titi.    

update wgo
help "Validation <o>ui ou <n>on"
validate(wgo = "o" or wgo = "n" , "Valeurs autoris�es : 'O' ou 'N'").

if wgo = "O" then leave.

end. /* repeat saisie */
end. /* do saisie */

if keyfunction(lastkey) = "END-ERROR" then return.


/* DEBUT DU TRAITEMENT */

if C-M = "M" then C-M = "R".

/* on vide la table temp */

for each sortie
exclusive-lock :
delete sortie.
end.

do aa = 1 to num-entries( l-typ ) :

wtyp = entry( aa, l-typ ) .

lig:
for each lignes where
    lignes.codsoc = "45"
    and lignes.motcle = "vte"
    and lignes.typcom = wtyp
    and lignes.typaux = "TIE"
    and lignes.codaux = Mag
    and lignes.top-livre <> "1"
    and (if Etat <> "*" then lignes.partiel = Etat else Etat = Etat)
    and (if (Log <> "ent" and Log <> "")
         then lignes.magasin = Log
         else Log = Log )
no-lock :

if Log = "ent" and lignes.magasin = "976" then next lig.

find entete where entete.codsoc = lignes.codsoc
and entete.motcle =lignes.motcle
and entete.typcom =lignes.typcom
and entete.numbon= lignes.numbon
no-lock no-error.
if not available entete then do: message "erreur ent". next.  end.

bon = int(substr(lignes.numbon,3,6)).
wclelig = lignes.codsoc + lignes.motcle + lignes.typcom +
          lignes.numbon + string (lignes.chrono, "999999").

if ( C-M <> "" and substr(entete.ref-magasin,1,1) = C-M )
or  C-M = "" then
do:

    if Fam <> "" then do :

        find  artapr
        where artapr.codsoc = lignes.codsoc
        and   artapr.articl = lignes.articl
        and   ( if Fam = "GP" 
                then ( artapr.famart >= "001" and artapr.famart <= "010" )
                else (if Fam = "AGR"
                then artapr.famart >= "011" 
                else artapr.famart = Fam ))
        no-lock no-error.
        if not available artapr then next lig.
        if Sfam <> "" and SFam <> artapr.soufam then next lig.        
    end.
           
    if lignes.qte-fact = 0
    then do :
        wrel = " " .
        wlib-qte = lignes.libart .
        wpartiel = " ".
    end.
    else do :
        wlib-qte = substring(lignes.libart,1,10) + "|"
                 + string(lignes.qte-fact,">>>>9-") .
        wpartiel = "P".
        if fac-httot = 1 then wrel = "N". else wrel = "O". 
    end.

    wdep = lignes.magasin.
    
    if substring(entete.ref-magasin,1,2) = "CC"
    then 
        do :
        FIND LIGSPE
        where ligspe.codsoc    = lignes.codsoc
        and   ligspe.motcle    = lignes.motcle + "/BED"
        and   ligspe.typcom    = lignes.typcom
        and   ligspe.numbon    = lignes.numbon
        and   ligspe.chrono    = lignes.chrono
        no-lock no-error.
        if available ligspe then 
            do :
            FIND ENTETE
            where entete.codsoc = ligspe.alpha-1
            and   entete.motcle = ligspe.alpha-2
            and   entete.typcom = ligspe.alpha-3
            and   entete.numbon = ligspe.alpha-4
            no-lock  no-error .
            if available entete then 
                do :
                auxili-soc = "01".
                /* if entete.codsoc = "08" then auxili-soc = entete.codsoc. */
                find  auxili
                where auxili.codsoc = auxili-soc
                and   auxili.typaux = entete.typaux
                and   auxili.codaux = entete.codaux
                no-lock no-error.
                if available auxili then wref = auxili.adres[1] .
                else wref = "".
                end.
                else wref = "".

            Find b-ligspe where
              b-ligspe.alpha-1 = ligspe.alpha-1  and
              b-ligspe.alpha-2 = ligspe.alpha-2  and
              b-ligspe.alpha-3 = ligspe.alpha-3  and
              b-ligspe.alpha-4 = ligspe.alpha-4  and
              b-ligspe.int-1   = ligspe.int-1    and
              b-ligspe.motcle = "ACP/BED"
              no-lock no-error.
            if available b-ligspe                             
               then do: 
                    find b-lignes where
                         b-lignes.codsoc = b-ligspe.codsoc and
                         b-lignes.motcle = "ACP" and
                         b-lignes.typcom = b-ligspe.typcom and
                         b-lignes.numbon = b-ligspe.numbon and
                         b-lignes.chrono = b-ligspe.chrono
                         no-lock no-error.
                    if available b-lignes
                       then do:
                            if b-lignes.nivcom <> "9" then wdep = "97X" .
                            end.
                    end.

            end. /* end de available ligspe */
            else wref = "".

        end.
        else do: 
         wref = substring(entete.ref-magasin, 1, length(entete.ref-magasin) - 3). 
                                
         FIND LIGSPE
         where ligspe.codsoc    = lignes.codsoc
         and   ligspe.motcle    = lignes.motcle + "/BED"
         and   ligspe.typcom    = lignes.typcom
         and   ligspe.numbon    = lignes.numbon
         and   ligspe.chrono    = lignes.chrono
         no-lock no-error.
         if available ligspe then do :
                                 
         Find b-ligspe where
              b-ligspe.alpha-1 = ligspe.alpha-1  and
              b-ligspe.alpha-2 = ligspe.alpha-2  and
              b-ligspe.alpha-3 = ligspe.alpha-3  and
              b-ligspe.alpha-4 = ligspe.alpha-4  and
              b-ligspe.int-1   = ligspe.int-1    and
              b-ligspe.motcle = "ACP/BED"
              no-lock no-error.
         if available b-ligspe                             
            then do: 
                                         
                 find b-lignes where
                      b-lignes.codsoc = b-ligspe.codsoc and
                      b-lignes.motcle = "ACP" and
                      b-lignes.typcom = b-ligspe.typcom and
                      b-lignes.numbon = b-ligspe.numbon and
                      b-lignes.chrono = b-ligspe.chrono
                      no-lock no-error.
                 if available b-lignes
                    then do:
                                                 
                         if b-lignes.nivcom <> "9" then wdep = "97X" .
                         end.
                 end.
         end.     
         end.               

    wqteral = lignes.qte[1] - lignes.qte[2].
    
    if wqteral = 0 then next lig.

    find  sortie
    where sortie.clelig = wclelig
    use-index cleligtab
    exclusive-lock no-error.
    if not available sortie then do :
    
        create sortie.
        assign 
            sortie.bon     = bon
            sortie.clelig  = wclelig
            sortie.mag     = wmag
            sortie.statut  = Etat
            sortie.log     = log 
            sortie.climag  = C-M
            sortie.famille = Fam
            sortie.chrono  = lignes.chrono
            sortie.articl  = lignes.articl
            sortie.lib-qte = wlib-qte
            sortie.qteral  = wqteral
            sortie.partiel = wpartiel
            sortie.ligpart = lignes.partiel
            sortie.rel     = wrel
            sortie.datdep  = lignes.datdep
            sortie.datliv  = entete.datliv
            sortie.ref-cli = wref
            sortie.depot   = wdep.
            
    end.        
    
end.
else next lig.
end.

END.

/* AFFICHAGE ECRAN SUIVANT LE TRI */

nbl = 0.

if Tri = "C" then do :
     ligtab = 0.
     for each sortie exclusive-lock use-index cde:
     ligtab = ligtab + 1.
     sortie.t-chrono = ligtab.
     end. 
end.
else if Tri = "S" then do :
     ligtab = 0.
     for each sortie exclusive-lock use-index statut:
     ligtab = ligtab + 1.
     sortie.t-chrono = ligtab.
     end. 
end.
else if Tri = "A" then do :
     ligtab = 0.
     for each sortie exclusive-lock use-index art:
     ligtab = ligtab + 1.
     sortie.t-chrono = ligtab.
     end. 
end.
else if Tri = "D" then do :
     ligtab = 0.
     for each sortie exclusive-lock use-index date:
     ligtab = ligtab + 1.
     sortie.t-chrono = ligtab.
     end. 
end.
else if Tri = "L" then do :
     ligtab = 0.
     for each sortie exclusive-lock use-index client:
     ligtab = ligtab + 1.
     sortie.t-chrono = ligtab.
     end. 
end.

     find first sortie where
          sortie.bon = sortie.bon
     use-index chrono
     no-lock no-error .
                    
     assign  x-recid  = recid( sortie ).
     run scae-ed0.p ( input  x-recid ).

end. /*repeat */

