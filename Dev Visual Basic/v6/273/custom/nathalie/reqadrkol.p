
{ connect.i       }
{ bat-ini.i "new" }
{ sel.i     "new" }

{ fr-cadre.i lib-prog }

batch-tenu = yes. /* Traitement en Batch */

/* debut ajout pour batch periodique */

def var sequence as int.

/* Initialisation  */
assign  sel-applic   = "nathalie"
        sel-filtre   = "adrko". 

/* Prise Numero sequence par operateur */
/*-------------------------------------*/
 assign  codsoc = ""
         etabli = ""
         typtab = "OPE"
         prefix = "NUME-SPOOL"
         codtab = operat .
                

run opeseq.p ( "adr" , output  sequence, output sel-sequence) .

{ bat-zon.i "sel-applic"   "sel-applic"   "c" "''" "''"          }
{ bat-zon.i "sel-filtre"   "sel-filtre"   "c" "''" "''"          }
{ bat-zon.i "sel-sequence" "sel-sequence" "c" "''" "''"          }

/* fin ajout pour batch périodique */

{ bat-maj.i  "reqadrko.p" }

hide frame fr-titre no-pause.
hide frame fr-cadre no-pause.
