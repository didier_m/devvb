/* Prog : nt-ctrvte.p Export des contrats de vente */

/* CCO le 24/03/2018  */

/*  deb en + pour lanceur web */
/* modif le 13/12/2018 pour corriger date de fin qui n'est pas bonne */


{ d-brokmini.i new }   

def frame fr-saisie .

{ bat-ini.i  new }
{ select.i     new }
{ aide.i       new }
{ maquette.i   new }  

DEFINE STREAM maquette.
def stream  sortie.

DEFINE VARIABLE x-prgclient AS CHARACTER   NO-UNDO.

def var    x-alpha1         as char       no-undo .
def var    x-alpha2         as char       no-undo .
def var    x-alpha3         as char       no-undo .
def var    x-alpha4         as char       no-undo .
def var    x-alpha5         as char       no-undo .
def var    x-alpha6         as char       no-undo .
def var    x-alpha7         as char       no-undo .
def var    x-alpha8         as char       no-undo .
def var    x-alpha9         as char       no-undo .
DEF VAR    x-alpha10        as char       no-undo .

/*  fin en + pour lanceur web */

def input parameter  fichier  as char format "x(12)".
run bat-lec.p ( fichier ) .

{ bat-cha.i  "sel-applic"     "selection-applic"   "a"          }
{ bat-cha.i  "sel-filtre"     "selection-filtre"   "a"          }
{ bat-cha.i  "sel-sequence"   "selection-sequence" "a"          }
{ bat-cha.i  "codsoc"         "glo-codsoc"         "a"          }
{ bat-cha.i  "etabli"         "glo-etabli"         "a"          }

{ bat-cha.i  "ma-impri"      "ma-impri"            "a"          }   
{ bat-cha.i  "ma-maquet"      "ma-maquet"          "a"          } 
{ bat-cha.i  "lib-prog"       "lib-prog"           "a"          }

{ bat-cha.i  "prgclient"     "x-prgclient"         "a"          }   

{ bat-cha.i  "alpha1"       "x-alpha1"           "a"          }   
{ bat-cha.i  "alpha2"       "x-alpha2"           "a"          }
{ bat-cha.i  "alpha3"       "x-alpha3"           "a"          }
{ bat-cha.i  "alpha4"       "x-alpha4"           "a"          }
{ bat-cha.i  "alpha5"       "x-alpha5"           "a"          }
{ bat-cha.i  "alpha6"       "x-alpha6"           "a"          }
{ bat-cha.i  "alpha7"       "x-alpha7"           "a"          }
{ bat-cha.i  "alpha8"       "x-alpha8"           "a"          }
{ bat-cha.i  "alpha9"       "x-alpha9"           "a"          }
{ bat-cha.i  "alpha10"      "x-alpha10"          "a"          }

run select-c.p.  

regs-app = "elodie".
{regs-cha.i }
regs-app = "nathalie".
{regs-cha.i}

deal-setsocreg ( "" ) .
deal-setsocreg ( glo-codsoc ) .

run sel-del.p ( selection-applic, selection-filtre, selection-sequence, yes ) .  

run traitement.

PROCEDURE TRAITEMENT:

  /* donn�es saisies */
  def var wsoc        as char                           no-undo.
  def var wnomfic     as char                           no-undo. 
  def var wdeb        as int                            no-undo.

  /* donn�es utilisees */ 

  def buffer b-lignes for lignes.
  def var mprix as dec decimals 3 format ">>>>>9.999".
  def var cumqt as dec decimals 3 format ">>>>>9.999".
  def var wval0 as char format "x(11)".
  def var wval1 as char format "x(11)".
  def var wval2 as char format "x(11)".
  def var wvalc as char format "x(11)".
  def var wval3 as char format "x(11)".
  def var wval4 as char format "x(11)".
  def var wval5 as char format "x(11)".
  def var wcle  as char.
  def var e-chrono as char.
  def var wd1   as date.
  def var wd2   as date.
   
  message "Debut programme" program-name( 1 ) .
  message " " .

  wdeb = time.

  Message "Export des contrats de vente --- debut le " 
  today "a" string ( wdeb , "hh:mm:ss" ).
  message " " .

  /* recup parametre web */
  wsoc      = trim(x-alpha1).    

  wnomfic = ma-impri.

  message wsoc .

  output stream maquette to value(ma-impri) .

  put stream maquette UNFORMATTED 
    "Soc" ";" "Mcle" ";" "Typ" ";" "Contrat" ";" 
    "Date_Deb_Contrat" ";"
    "Date_Fin_Contrat" ";" "Mag" ";" "Ref-Contrat" ";"
    "Ccli-CT" ";" "Ncli-CT" ";" 
    "Articl" ";" "Libelle" ";" "Qt�_R�serv�e" ";" "Qt�_Cd�e_Aff." ";"
    "Qt�_Livr�e Aff." ";" "Prix-CT" ";" "Qt�-Cde-DEAL" ";"
    "Date-Cre-CT" ";" "Opecre" ";" "Date-Maj-CT" ";" "Opemaj" skip.

  for each lignes where 
           lignes.codsoc = wsoc
       and lignes.motcle = "VTM"
       and lignes.typcom = "MAC"
  no-lock by numbon by chrono descending:

  find entete where 
       entete.codsoc = lignes.codsoc
   and entete.motcle = lignes.motcle
   and entete.typcom = lignes.typcom
   and entete.numbon = lignes.numbon
   no-lock no-error.
  if not available entete then do:
     message "erreur ent" lignes.numbon lignes.datdep
             lignes.articl lignes.qte[2] lignes.pu-net.
     next.
  end.       

  e-chrono = string(lignes.chrono, "9999").
  if substr(e-chrono, 4,1) <> "0" then do:
     mprix = lignes.pu-net.
     wd1   = lignes.datbon.
     wd2   = lignes.datdep.
     next.
  end.

  cumqt = 0.
  wcle = trim(lignes.codsoc) + "  " + lignes.motcle + lignes.typcom + lignes.numbon 
       + substr(string(lignes.chrono, "99999"), 1,3).

  for each b-lignes where
      b-lignes.ref-marche begins wcle and
      b-lignes.typcom begins "C"
  no-lock:

  cumqt = cumqt + b-lignes.qte[1].
  wval3 = string(b-lignes.qte[1], ">>>>>>9.999-").
  wval3 = replace(wval3,".",",").

  wval4 = string(cumqt, ">>>>>>9.999-").
  wval4 = replace(wval4,".",",").

  end.

  wvalc = string(cumqt, ">>>>>>9.999").
  wvalc = replace(wvalc,".",",").

  wval0 = string(lignes.qte[1], ">>>>>>9.999").
  wval0 = replace(wval0,".",",").

  wval5 = string(lignes.qte[2], ">>>>>>9.999").
  wval5 = replace(wval5,".",",").

  wval1 = string(lignes.qte-fact, ">>>>>>9.999").
  wval1 = replace(wval1,".",",").

  wval2 = string(mprix, ">>>>>>9.999").
  wval2 = replace(wval2,".",",").

  export stream maquette delimiter ";"
         lignes.codsoc
         lignes.motcle
         lignes.typcom
         substr(lignes.numbon, 1, 8) 
         wd1 wd2
         lignes.magasin
         entete.ref-tiers
         lignes.codaux
         entete.adres[1]
         lignes.articl 
         lignes.libart
         wval0
         wval1
         wval5
         wval2
         wvalc
         lignes.datcre
         lignes.opecre
         lignes.datmaj
         lignes.opemaj
         .

  end.

  message "Export des contrats de vente --- Fin le " 
          today "a" string ( time , "hh:mm:ss" )
          "   --- duree = " string(time - wdeb, "hh:mm:ss").
  message " " .
  output stream maquette close.

END.


