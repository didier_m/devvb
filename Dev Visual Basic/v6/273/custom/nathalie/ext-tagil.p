/*==============================================================*/
/*   extraction des donn�es tr�sorerie Agil                     */
/*      - ecart de caisse                                       */
/* VLE 121015 : ajout magasins 524 525                          */
/* VLE 020518 : ajout magasin 087                               */  
/* VLE 130319 : ajout societe 88                                */
/*==============================================================*/
{ connect.i }

def stream ecart.

def var wnomfic as char.
def var wmagasin as char format "xxx".
def var wdatedeb as date format "99/99/9999".
def var wdatefin as date format "99/99/9999".
def var annee as int.
def var mois as int.
def var jour as int.
def var i as int.
def var j as int.

def var l-soc as char init "01,08,59,88".

def var l-mag01 as char init "001,002,003,006,008,009,011,014,015,016,017,018,020,021,022,025,026,027,028,038
,039,057,081,082,083,084,085,086,087,088,089".

def var l-mag59 as char init "136".

def var l-mag88 as char init "133".

def var l-mag08 as char init "402,405,407,409,410,411,413,416,417,418,419,423,502,505,507,508,511,512,515,516
,518,521,523,524,525,614,618,626,705,801".

def var l-mag as char.

update l-soc format "x(12)" wmagasin wdatedeb wdatefin.
annee = year(wdatedeb).
mois = month(wdatedeb).
jour = day(wdatedeb).

if wdatedeb = ?
then message "la date de debut doit �tre renseignee".
if wdatefin = ?
then wdatefin = wdatedeb.

wnomfic = "EXT_AGIL_ECART_CAISSE_" + string(annee) + 
string(mois, "99") + string(jour, "99") + ".csv".
output stream ecart to value (wnomfic).

/*==================================================*/
/*   recherche des mouvements ecarts de caisse      */
/*   - ecarts nature 12                             */
/*==================================================*/
export stream ecart delimiter ";"
       "SOC" "MAG" "DATE" "NATURE" "MDP" "MTT" "TOP_TRT".

do i = 1 to num-entries(l-soc) :

  if wmagasin <> ""
     then l-mag = wmagasin.
  else   
  if entry(i, l-soc) = "01"
     then l-mag = l-mag01.
  else 
     if entry(i, l-soc) = "08"
        then l-mag = l-mag08.
     else
        if entry(i, l-soc) = "59"
           then l-mag = l-mag59.
           else
           if entry(i, l-soc) = "88"
              then l-mag = l-mag88. 
           else l-mag = "".   
 
  do j = 1 to num-entries(l-mag) :
   
    FOR EACH MVTAGIL 
        where mvtagil.codsoc = entry(i, l-soc)
        and   mvtagil.id_magasin = entry(j, l-mag)
        and   mvtagil.dt_reference >= wdatedeb
        and   mvtagil.dt_reference <= wdatefin
        and   mvtagil.mtt_ligne    <> 0
        and   mvtagil.cd_paiement <> "AVO"
        and   mvtagil.cd_paiement <> "RAV"
        and   cd_nature = "1200" 
        use-index CD_GROUPE no-lock
        break by mvtagil.dt_reference:

    export stream ecart delimiter ";"
       mvtagil.codsoc id_magasin dt_reference
       substr(cd_nature, 1, 2)  cd_paiement
       mtt_ligne  top_trt.
    
    end. /* boucle mvtagil */
  end. /* boucle magasin */  
end. /* boucle soc */

output stream ecart close.