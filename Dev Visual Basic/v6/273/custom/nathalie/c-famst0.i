/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! Deal/nathalie/tty/c-encst0.i                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 30363 03-05-12!Evo!jcc            !Ajout option et date de base                                          !
!V52 16340 22-10-07!Evo!cch            !Ajout proposition par d�faut d'un fichier LOG horodat�                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*=========================================================================*/
/*                         C - E N C S T 0 . I                             */
/*                         +++++++++++++++++++                             */
/*  Recalcul : - Stock Temps Reel en Fonction du Dernier Arrete de Stock   */
/*             - En Cours de Commandes Clients                             */
/*             - En Cours de Commandes Fournisseurs                        */
/*=========================================================================*/

def {1} shared var date-arrete      like stoarr.date-arrete no-undo .

def {1} shared var ii               as int                  no-undo .

def {1} shared var art-min          as char  format "x(10)" no-undo .
def {1} shared var art-max          as char  format "x(10)" no-undo .
def {1} shared var majour           as char  format "x"     no-undo .
def {1} shared var valida           as char  format "x"     no-undo .
def {1} shared var x-option         as char  format "x"     no-undo .
def {1} shared var nom-spool        as char format "x(35)"  no-undo .

def {1} shared var lib-oui          as char format "x"      no-undo .
def {1} shared var lib-non          as char format "x"      no-undo .

def {1} shared var libelle          as char   extent 49     no-undo .

