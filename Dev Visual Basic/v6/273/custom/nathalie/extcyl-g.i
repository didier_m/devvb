/*==========================================================================*/
/*                           E X T C Y L - G . I                            */
/* Extraction des donnees pour Cylande                                      */
/*--------------------------------------------------------------------------*/
/* interface TARIFS DE VENTE                                                */
/*==========================================================================*/
/* Prog : extcytvt.p.p Export tarif de vente GRASSOT reprise format XML     */
/* VLE le 16/11/2017     */
/* DGR 31122018 : rejet quand tarif avec date-debut > date de fin           */
/* DGR 29012019 : modif tarif AGC par magasin + envoi des derniers tarifs   */
/* DGR 31012019 : test date-debut > date de fin dans extcyl-1.p /immcyl-1.p */ 
/* CCO 15072019 : ajout {tarcli.i}                                          */

def var wagcid as recid.

CREATE SAX-WRITER hSAXWriter.
hSAXWriter:ENCODING = "UTF-8".
hSAXWriter:FORMATTED = FALSE .  /* 	si TRUE: fichier de sortie lisible mais ajout de blancs 
									a la place des sauts de lignes a l'integration dans UR */

lOK = hSAXWriter:SET-OUTPUT-DESTINATION ( "file", wficsortie ).
lOK = hSAXWriter:START-DOCUMENT ( ).
lOK = hSAXWriter:WRITE-COMMENT ( "EXTRACTION DES TARIFS ").
lOK = hSAXWriter:START-ELEMENT ( "productListType" ).
   

/* traitement des tarifs GENERIQUES VTE -> CLI  */
for each t-tar use-index i-mot
where t-tar.motcle = "VTE"
no-lock 
break by t-tar.motcle + t-tar.articl :

	find {tarcli.i} 
	where rowid ( {tarcli.i} ) = t-tar.rowtar
	no-lock no-error.
	if not available {tarcli.i} then next.
      
   if  first-of ( t-tar.motcle + t-tar.articl )  then do:
      assign
      lOK = hSAXWriter:START-ELEMENT ( "values" )
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", TRIM(t-tar.articl) )
         lOK = hSAXWriter:START-ELEMENT ( "items" )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", TRIM(t-tar.articl) )
            lOK = hSAXWriter:START-ELEMENT ( "itemSellingPrices" ) .
      
      wcodbu = "".
      
   end.

   
   wagcid = ?.
   find first agc-tarcli
   where agc-tarcli.codsoc = {tarcli.i}.codsoc
   and   agc-tarcli.motcle = {tarcli.i}.motcle
   and   agc-tarcli.date-debut = {tarcli.i}.date-debut
   and   agc-tarcli.articl = {tarcli.i}.articl
   and   agc-tarcli.typaux = {tarcli.i}.typaux
   and   agc-tarcli.codaux = {tarcli.i}.codaux
   and   agc-tarcli.code-tarif = "AGC"
   no-lock no-error.
   if available agc-tarcli then wagcid = recid ( agc-tarcli ).
   
   if wagcid <> ? then do:
      find {tarcli.i}
      where recid ( {tarcli.i} ) = wagcid
      no-lock no-error.
      if not available {tarcli.i} then next.
   end.
                  
   /*
   wcodbu = entry ( lookup(  trim( {tarcli.i}.codsoc ) ,  l-socdeal  ) , l-bu-s ) .
   wcodbu = "".
   */
   wzone = entry ( lookup(trim( {tarcli.i}.codsoc ),  l-socdeal  ) , l-bu-s ) .   
   if wcodbu = "" then wcodbu = wzone.
   else  
     if lookup ( wzone , wcodbu ) = 0 then wcodbu = wcodbu + "," + wzone.

   
	wdate = string(year( {tarcli.i}.date-debut)) + "-" + string(month( {tarcli.i}.date-debut),'99') + "-" + string(day( {tarcli.i}.date-debut),'99').
   
   wdate2 = "".
   if {tarcli.i}.date-fin <> ? then do:
      wdate2 = string(year( {tarcli.i}.date-fin)) + "-" + string(month( {tarcli.i}.date-fin),'99') + "-" + string(day( {tarcli.i}.date-fin),'99').
   end .
   
   /* 
	wtarif = upper ( trim ( {tarcli.i}.code-tarif ) ) . 
   */
   wtarif = "CLI".
   if {tarcli.i}.code-tarif = "PRO" then wtarif = {tarcli.i}.code-tarif.

	wvaleur = {tarcli.i}.valeur.
   
   nblus = nblus + 1.

   assign
      /* lOK = hSAXWriter:START-ELEMENT ( "values" )
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", TRIM( {tarcli.i}.articl) )
         lOK = hSAXWriter:START-ELEMENT ( "items" )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", TRIM( {tarcli.i}.articl) )
            lOK = hSAXWriter:START-ELEMENT ( "itemSellingPrices" ) 
            */
               lOK = hSAXWriter:START-ELEMENT ( "businessUnitItemSellingPrices" ) 
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "businessUnit", wzone )
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", wtarif )
                  lOK = hSAXWriter:START-ELEMENT ( "sellingPrice" ) 
                     lOK = hSAXWriter:START-ELEMENT ( "price" ) 
                        lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "currency", "EUR" )      
                        lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "value", string (wvaleur) )
                     lOK = hSAXWriter:END-ELEMENT ( "price" ) 
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "free", t-tar.free )
                     lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "startDate", wdate ).
                     if trim( wdate2 ) <> "" then lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "endDate", wdate2 ).                     
                  assign
                  lOK = hSAXWriter:END-ELEMENT ( "sellingPrice" ) 
               lOK = hSAXWriter:END-ELEMENT ( "businessUnitItemSellingPrices" ) .
               
   if  last-of ( t-tar.motcle + t-tar.articl )  then do:
      assign 
               lOK = hSAXWriter:END-ELEMENT ( "itemSellingPrices" ) 
               lOK = hSAXWriter:END-ELEMENT ( "items" ).
      
      if wcodbu <> "" then do:
         do k = 1 to num-entries ( wcodbu ):
            assign 
            lOK = hSAXWriter:START-ELEMENT ( "vatCode" ) 
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "businessUnit", entry ( k, wcodbu ) )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "vatCode", t-tar.tva )
            lOK = hSAXWriter:END-ELEMENT ( "vatCode" ).
         end. 
      end.
      
      /*
      assign 
               lOK = hSAXWriter:END-ELEMENT ( "itemSellingPrices" ) 
         lOK = hSAXWriter:END-ELEMENT ( "items" ) 
         lOK = hSAXWriter:START-ELEMENT ( "vatCode" ) 
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "businessUnit", wcodbu )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "vatCode", t-tar.tva )
         lOK = hSAXWriter:END-ELEMENT ( "vatCode" )             
      lOK = hSAXWriter:END-ELEMENT ( "values" ) no-error.     
      */
      assign lOK = hSAXWriter:END-ELEMENT ( "values" ) no-error.     
   end.
   if lOK then nbecr = nbecr + 1.
   
end.

/* traitement des tarifs SPECIFIQUES VTE -> CLI et PRO */

for each t-tar use-index i-mot
where t-tar.motcle = "VTP"
break by t-tar.motcle + t-tar.articl :


   find {tarcli.i}
	where rowid ( {tarcli.i} ) = t-tar.rowtar
	no-lock no-error.
	if not available {tarcli.i} then next.   
   
   if  first-of (t-tar.motcle + t-tar.articl )
   then  do:
      wcodbu = "" .
      assign
         lOK = hSAXWriter:START-ELEMENT ( "values" )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", TRIM( {tarcli.i}.articl) )
            lOK = hSAXWriter:START-ELEMENT ( "items" )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", TRIM( {tarcli.i}.articl) )
               lOK = hSAXWriter:START-ELEMENT ( "itemSellingPrices" ) .

	end.

   
   wagcid = ?.
   find first agc-tarcli
   where agc-tarcli.codsoc = {tarcli.i}.codsoc
   and   agc-tarcli.motcle = {tarcli.i}.motcle
   and   agc-tarcli.date-debut = {tarcli.i}.date-debut
   and   agc-tarcli.articl = {tarcli.i}.articl
   and   agc-tarcli.typaux = {tarcli.i}.typaux
   and   agc-tarcli.codaux = {tarcli.i}.codaux
   and   agc-tarcli.code-tarif = "AGC"
   no-lock no-error.
   if available agc-tarcli then wagcid = recid ( agc-tarcli ).
   
   if wagcid <> ? then do:
      find {tarcli.i}
      where recid ( {tarcli.i} ) = wagcid
      no-lock no-error.
      if not available {tarcli.i} then next.
   end.

   
   wzone = entry ( lookup(trim( {tarcli.i}.codsoc ),  l-socdeal  ) , l-bu-s ) .   
   if wcodbu = "" then wcodbu = wzone.
   else  
     if lookup ( wzone , wcodbu ) = 0 then wcodbu = wcodbu + "," + wzone.

   /* tarif "magasin" */
   
   wcode = "".
   if {tarcli.i}.typaux = "M"  then wcode = trim( {tarcli.i}.codaux ).
   else do:
      find tabges
      where tabges.codsoc = ""
      and   tabges.etabli = ""
      and   tabges.typtab = "MAG"
      and   tabges.prefix = "GROUPE-PRO"
      and   tabges.codtab = {tarcli.i}.codaux 
      no-lock no-error.
      if available tabges then do:
      
         for each magasi 
         where magasi.codsoc = "01"
         and     ( magasi.dat-fin-act = ? or magasi.dat-fin-act > date-debut )
         and   lookup (magasi.equipe , l-mag-equipe ) <> 0
         and   lookup(trim(tabges.codtab), magasi.type-com) <> 0
         and   magasi.societe = trim( {tarcli.i}.codsoc )
         /* and   lookup ( magasi.codtab, "991,998" ) = 0*/
         no-lock :
            if wcode = "" then wcode = magasi.codtab.
            else wcode = wcode + "," + magasi.codtab.
               
         end.
      end.
   end.
   if wcode <> "" then do :
      
      wdate = "".
      wdate = string(year( {tarcli.i}.date-debut)) + "-" + string(month( {tarcli.i}.date-debut),'99') + "-" + string(day( {tarcli.i}.date-debut),'99') no-error.
      if error-status:error then  wdate = "" .
      
      wdate2 = "".
      if {tarcli.i}.date-fin <> ? then do:
         wdate2 = string(year( {tarcli.i}.date-fin)) + "-" + string(month( {tarcli.i}.date-fin),'99') + "-" + string(day( {tarcli.i}.date-fin),'99') no-error.
         if error-status:error then  wdate2 = "" .   
      end .

      wtarif = "CLI".
      if {tarcli.i}.code-tarif = "PRO" then wtarif = {tarcli.i}.code-tarif.

      /* wtarif = upper ( trim ( {tarcli.i}.code-tarif ) ) . */
      wvaleur = {tarcli.i}.valeur.
      
      nblus = nblus + 1.
      do k = 1 to num-entries ( wcode ):
         assign 
            lOK = hSAXWriter:START-ELEMENT ( "salesMediumItemSellingPrices" ) 
               lOK = hSAXWriter:START-ELEMENT ( "salesMedium" ) 
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "nature", "S" )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", entry ( k,wcode) )
            lOK = hSAXWriter:END-ELEMENT ( "salesMedium" ) 
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", wtarif )
            lOK = hSAXWriter:START-ELEMENT ( "sellingPrice" ) 
               lOK = hSAXWriter:START-ELEMENT ( "price" ) 
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "currency", "EUR" )      
                  lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "value", string (wvaleur) )
               lOK = hSAXWriter:END-ELEMENT ( "price" ) 
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "free", "false" )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "startDate", wdate ).
               if trim( wdate2 ) <> "" then lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "endDate", wdate2 ).
            assign
            lOK = hSAXWriter:END-ELEMENT ( "sellingPrice" ) 
         lOK = hSAXWriter:END-ELEMENT ( "salesMediumItemSellingPrices" ) .
         
      end.
   end.

   if  last-of ( t-tar.motcle + t-tar.articl )  then do:
      
      assign 
            lOK = hSAXWriter:END-ELEMENT ( "itemSellingPrices" ) 
         lOK = hSAXWriter:END-ELEMENT ( "items" ) no-error.
         
      if error-status:error then do:
         message " erreur 2" {tarcli.i}.articl {tarcli.i}.motcle view-as alert-box. 
      end.
       
      if wcodbu <> "" then do:
         do k = 1 to num-entries ( wcodbu ):
            assign 
            lOK = hSAXWriter:START-ELEMENT ( "vatCode" ) 
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "businessUnit", entry ( k, wcodbu ) )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "vatCode", t-tar.tva )
            lOK = hSAXWriter:END-ELEMENT ( "vatCode" ).
         end. 
      end.
      
      lOK = hSAXWriter:END-ELEMENT ( "values" ).

   end.
                        
   if lOK then nbecr = nbecr + 1.
end.

assign lOK = hSAXWriter:END-ELEMENT ( "productListType").

lOK = hSAXWriter:END-DOCUMENT () .

DELETE OBJECT hSAXWriter. 