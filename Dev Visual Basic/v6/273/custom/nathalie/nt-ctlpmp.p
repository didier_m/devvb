/* prog : ctlpamp.p Controle PAMP */
/* DGR 010208 : modif pour recup pamp periode-1 quand on change d'annee */
/* CCO 070911 : Changement motcle PXACHA en STOARR                      */
/* CCO 101111 : Acces RECFOU au lieu de qte PXACHA et                   */
/*              test sur wqteach = 0 pour run p-impression              */
/* CCO 151111 : Anlever acces RECFOU remplac� par STARTI                */
/* DGR 101114 : boucle soci�t�s                                         */
/* DGR 190919 : modif chemin spool web                                  */

/*  deb en + pour lanceur web */
{ d-brokmini.i new }   
def frame fr-saisie .
{ bat-ini.i  new }
{ select.i     new }
{ aide.i       new }
{ maquette.i   new }  

def STREAM maquette.
def stream entree.
def stream rej.
 
DEFINE VARIABLE x-prgclient AS CHARACTER   NO-UNDO.
def var    x-alpha1         as char       no-undo .
def var    x-alpha2         as char       no-undo .
def var    x-alpha3         as char       no-undo .
def var    x-alpha4         as char       no-undo .
def var    x-alpha5         as char       no-undo .
def var    x-alpha6         as char       no-undo .
def var    x-alpha7         as char       no-undo .
def var    x-alpha8         as char       no-undo .
def var    x-alpha9         as char       no-undo .
DEF VAR    x-alpha10        as char       no-undo .
/*  fin en + pour lanceur web */
def input parameter  fichier  as char format "x(12)".
run bat-lec.p ( fichier ) .
{ bat-cha.i  "sel-applic"     "selection-applic"   "a"          }
{ bat-cha.i  "sel-filtre"     "selection-filtre"   "a"          }
{ bat-cha.i  "sel-sequence"   "selection-sequence" "a"          }
{ bat-cha.i  "codsoc"         "glo-codsoc"         "a"          }
{ bat-cha.i  "etabli"         "glo-etabli"         "a"          }
{ bat-cha.i  "ma-impri"      "ma-impri"            "a"          }   
{ bat-cha.i  "ma-maquet"      "ma-maquet"          "a"          } 
{ bat-cha.i  "lib-prog"       "lib-prog"           "a"          }
{ bat-cha.i  "prgclient"     "x-prgclient"         "a"          }   
{ bat-cha.i  "alpha1"       "x-alpha1"           "a"          }   
{ bat-cha.i  "alpha2"       "x-alpha2"           "a"          }
{ bat-cha.i  "alpha3"       "x-alpha3"           "a"          }
{ bat-cha.i  "alpha4"       "x-alpha4"           "a"          }
{ bat-cha.i  "alpha5"       "x-alpha5"           "a"          }
{ bat-cha.i  "alpha6"       "x-alpha6"           "a"          }
{ bat-cha.i  "alpha7"       "x-alpha7"           "a"          }
{ bat-cha.i  "alpha8"       "x-alpha8"           "a"          }
{ bat-cha.i  "alpha9"       "x-alpha9"           "a"          }
{ bat-cha.i  "alpha10"      "x-alpha10"          "a"          }
run select-c.p.  
regs-app = "elodie".
{regs-cha.i }
regs-app = "nathalie".
{regs-cha.i}
deal-setsocreg ( "" ) .
deal-setsocreg ( glo-codsoc ) .
run sel-del.p ( selection-applic, selection-filtre, selection-sequence, yes ) .  
def stream etat11 .
def stream mouchard .
def stream ficpmp.
   
def var wmouc as char.
    
DEF VAR sequence     AS CHAR FORMAT "x(05)"           NO-UNDO.
DEF VAR sel-sequence AS CHAR FORMAT "x(12)"           NO-UNDO.
DEF VAR sel-sequenc2 AS CHAR FORMAT "x(12)"           NO-UNDO.
def var wfic         as char format "x(12)"           NO-UNDO.
def var articl-soc   LIKE artapr.codsoc.
def var valid        as char format "x".
def var optd as char format "x" initial "N".
def var annee        as char format "xxxx".
def var wannee       as char format "xxxx".
def var periode      as int  format "99".
def var annee-1      as char format "xxxx".
def var periode-1    as int  format "99".
def var wvalo1       as dec decimals 2 format ">>>>>>>>>>>9.99-". 
def var wvalo2       as dec decimals 2 format ">>>>>>>>>>>9.99-". 
def var wtoteca      as dec decimals 2 format ">>>>>>>>>>>9.99-". 
def var wecart1      as dec decimals 3.
def var wecart2      as dec decimals 3.
def var wqteach      as dec decimals 0 format ">>>>>>>>>>>>9-".

def var wecaval      as int.
def var wpour        as int.
def var wpourc       as dec decimals 3 format ">>>>>9.9-".
def var l-col        as char.
def var nolig        as int.        
def var topvar       as logical.
def var topv         as logical.
def var topr         as logical.
def var tops         as logical.
def var topimp       as logical.
def var topvid       as logical.
def var topmch       as logical.
def var wfam1        as char format "xxx".
def var wfam2        as char format "xxx".

def buffer b-lignes  for lignes.
def buffer b-bastat  for bastat.
def buffer b-stocks  for stocks.
def var plig         as char format "xxx".

def var sdebdat      as date.
def var sfindat      as date.
def var sdebdat01    as date.

def var pamp12n      as dec extent 12 decimals 3 format ">>>>9.999-".
def var pamp12s      as dec extent 12 decimals 3 format ">>>>9.999-".
def var aa           as int.
def var bb           as int.
def var cc           as int.
def var nbper        as int.
def var pamp06       as dec decimals 3 format ">>>>9.999-".
def var pampref      as dec decimals 3 format ">>>>9.999-".
def var wpmp1        as dec decimals 3 format ">>>>9.999-".
def var wpmp2        as dec decimals 3 format ">>>>9.999-".
def var pmpb         as dec decimals 3 format ">>>>9.999-".
def var pmph         as dec decimals 3 format ">>>>9.999-".
def var pampvar      as dec decimals 3 format ">>>>9.999-".
def var colvar       as int format ">99".

def var l-fam        as char .
def var i-fam        as int.

def var i            as int  no-undo .
def var l-soc        as char no-undo .
    
    
run traitement.

PROCEDURE TRAITEMENT:
    regs-app = "NATHALIE" .
    { regs-cha.i }
    
    { regs-inc.i "articl" }
    articl-soc = regs-soc .
    
    def var codsoc-soc-sav like codsoc-soc.
   
    assign codsoc-soc-sav = codsoc-soc.
    /* recuperation des parametres WEB */
    /* codsoc-soc  = trim(x-alpha1)   .    */
    
    /* articl-soc    = trim (x-alpha1)   .    */
    l-soc         = trim (x-alpha1)   .    

    sdebdat       = date ( x-alpha2 ) .   
    sfindat       = date ( x-alpha3 ) .    
    annee         = trim ( x-alpha4 ) .   
    periode       = int  ( trim ( x-alpha5 ) ) .   
    wecaval       = int  ( trim ( x-alpha6 ) ) .
    wpour         = int  ( trim ( x-alpha7 ) ) .
    wmouc         = trim ( x-alpha8 ) .
    

    do i = 1 to num-entries ( l-soc ):

        articl-soc = entry ( i , l-soc ).
        {cadra-d.i articl-soc 2 0}
    
        sel-sequenc2 = ma-impri.
        sel-sequenc2 = replace ( sel-sequenc2 , operat + ".csv" , articl-soc + "-" + operat ) + ".csv"  .
        
        sel-sequence = "/applic/li/" + operat + "/" + replace(sel-sequenc2 , ".csv" , "" ).
    
        wfic = "/applic/li/" + operat + "/pamp" + trim(articl-soc) + ".txt".
        
        message "ma-impri = " ma-impri 
                 "sel-sequenc2 = " sel-sequenc2.
                 
        sel-sequenc2 =   "/applic/partageweb/xcspool/" + operat + "/" + sel-sequenc2 .
        IF SEARCH( "/applic/erp/src/tty/nathalie/ch-param.p" ) <> ? THEN 
            sel-sequenc2 = REPLACE (sel-sequenc2, "/applic/", "/applic/erp/").      
        
        if periode = 1  then  assign periode-1 = 12.
                        else  assign periode-1 = periode - 1.   
        annee-1 = string ( int (annee) - 1).
   
        l-fam = "".
        for each tabges
        where tabges.codsoc = ""
        and   tabges.etabli = ""
        and   tabges.typtab = "ART"
        and   tabges.prefix = "FAMART"
        no-lock :
            { select-z.i "famart"     "tabges.codtab" }
            run select-t.p.
            if selection-ok <> ""  then  next .
            if  l-fam = ""  then l-fam = trim ( tabges.codtab ).
                            else l-fam = l-fam + ","  + trim ( tabges.codtab ).
        end.
    
    
        sdebdat01 = date (month(sfindat), 01, year(sfindat) ).
    
        /* message sel-sequence
                sel-sequenc2
                wfic view-as alert-box . */
        OUTPUT STREAM etat11 TO VALUE(sel-sequenc2).
    
    
        export stream etat11 delimiter ";" 
            "Liste des PAMP Societe =>" articl-soc 
            "Date debut Receptions  =>" sdebdat    
            "Date fin   Receptions  =>" sfindat      
            "Annee =" annee "Periode =" periode     
            "Ecart � >="  wecaval "Ecart % >=" wpour 
            "Selection Famille => " l-fam.
        
        export stream etat11 delimiter ";" 
            "Fichier des PAMPS  =>" wfic .
        if wmouc = "N" then 
            export stream etat11 delimiter ";" 
                "Pas de Fichier Mouchard  ".
        else 
            export stream etat11 delimiter ";" 
                "Fichier Mouchard => " sel-sequence.
    
        export stream etat11 delimiter ";" 
                "" .
        output stream ficpmp to VALUE(wfic).
        
        if wmouc = "O" then do:
            OUTPUT STREAM mouchard TO VALUE(sel-sequence).
    
            put stream mouchard "Liste des PAMP variant + ou - de " wecaval " EUROS" skip.
            put stream mouchard "Liste des PAMP variant + ou - de " wpour " Pourcent" skip.
            put stream mouchard " " skip.
            put stream mouchard "Annee " annee " Periode " periode skip.
            put stream mouchard " " skip.
            put stream mouchard "TopS = Yes si un PAMP = ZERO sur la p�riode" skip.
            put stream mouchard "TopV = Yes si une Vente, TOPR = Yes si une Reception sur la p�riode" skip.
            put stream mouchard "TopVar = Yes si depassement variation pourcentage avec PAMP REFERENCE" skip.
    
            put stream mouchard " " skip.
            put stream mouchard "So   Code       TopS TopV topR Pamref    Topvar    PampVAR Colvar" skip.
        end.
    
    
        if periode = 07 then
            export stream etat11 delimiter ";" "Soc" "Fam" "ARTICLE" "LIBELLE" "Val" 
                "Pamp_M_06" "Ind" "Col_Ecart" "Ec_%" "Total_Ecart" "Pamp_M_07" "Pamp_M_A_07" 
                "Pamp_REF"  
                .
        if periode = 08 then
            export stream etat11 delimiter ";" "Soc" "Fam" "ARTICLE" "LIBELLE" "Val" 
                "Pamp_M_06" "Ind" "Col_Ecart" "Ec_%" "Total_Ecart" 
                "Pamp_M_08" "Pamp_M_A_08" "Pamp_M_07" "Pamp_M_A_07" 
                "Pamp_REF"                  .
    
        if periode = 09 then
            export stream etat11 delimiter ";" "Soc" "Fam" "ARTICLE" "LIBELLE" "Val" 
                "Pamp_M_06" "Ind" "Col_Ecart" "Ec_%" "Total_Ecart" 
                "Pamp_M_09" "Pamp_M_A_09" "Pamp_M_08" "Pamp_M_A_08" "Pamp_M_07" "Pamp_M_A_07" 
                "Pamp_REF"     .

        if periode = 10 then
            export stream etat11 delimiter ";" "Soc" "Fam" "ARTICLE" "LIBELLE" "Val" 
                "Pamp_M_06" "Ind" "Col_Ecart" "Ec_%" "Total_Ecart" 
                "Pamp_M_10" "Pamp_M_A_10" "Pamp_M_09" "Pamp_M_A_09" "Pamp_M_08" "Pamp_M_A_08" "Pamp_M_07" "Pamp_M_A_07" 
                "Pamp_REF"  .
    
        if periode = 11 then
            export stream etat11 delimiter ";" "Soc" "Fam" "ARTICLE" "LIBELLE" "Val" 
                "Pamp_M_06" "Ind" "Col_Ecart" "Ec_%" "Total_Ecart" 
                "Pamp_M_11" "Pamp_M_A_11"
                "Pamp_M_10" "Pamp_M_A_10" "Pamp_M_09" "Pamp_M_A_09" "Pamp_M_08" "Pamp_M_A_08" "Pamp_M_07" "Pamp_M_A_07" 
                "Pamp_REF"    . 

        if periode = 12 then
            export stream etat11 delimiter ";" "Soc" "Fam" "ARTICLE" "LIBELLE" "Val" 
                "Pamp_M_06" "Ind" "Col_Ecart" "Ec_%" "Total_Ecart" 
                "Pamp_M_12" "Pamp_M_A_12" "Pamp_M_11" "Pamp_M_A_11"
                "Pamp_M_10" "Pamp_M_A_10" "Pamp_M_09" "Pamp_M_A_09" "Pamp_M_08" "Pamp_M_A_08" "Pamp_M_07" "Pamp_M_A_07" 
                "Pamp_REF"     .

        if periode = 01 then
            export stream etat11 delimiter ";" "Soc" "Fam" "ARTICLE" "LIBELLE" "Val" 
                "Pamp_M_06" "Ind" "Col_Ecart" "Ec_%" "Total_Ecart" 
                "Pamp_M_01" "Pamp_M_A_01" "Pamp_M_12" "Pamp_M_A_12" "Pamp_M_11" "Pamp_M_A_11"
                "Pamp_M_10" "Pamp_M_A_10" "Pamp_M_09" "Pamp_M_A_09" "Pamp_M_08" "Pamp_M_A_08" "Pamp_M_07" "Pamp_M_A_07" 
                "Pamp_REF"     .

        if periode = 02 then
            export stream etat11 delimiter ";" "Soc" "Fam" "ARTICLE" "LIBELLE" "Val" 
                "Pamp_M_06" "Ind" "Col_Ecart" "Ec_%" "Total_Ecart" 
                "Pamp_M_02" "Pamp_M_A_02" "Pamp_M_01" "Pamp_M_A_01" "Pamp_M_12" "Pamp_M_A_12" "Pamp_M_11" "Pamp_M_A_11"
                "Pamp_M_10" "Pamp_M_A_10" "Pamp_M_09" "Pamp_M_A_09" "Pamp_M_08" "Pamp_M_A_08" "Pamp_M_07" "Pamp_M_A_07" 
                "Pamp_REF" 
                .
        if periode = 03 then
            export stream etat11 delimiter ";" "Soc" "Fam" "ARTICLE" "LIBELLE" "Val" 
                "Pamp_M_06" "Ind" "Col_Ecart" "Ec_%" "Total_Ecart" 
                "Pamp_M_03" "Pamp_M_A_03"
                "Pamp_M_02" "Pamp_M_A_02" "Pamp_M_01" "Pamp_M_A_01" "Pamp_M_12" "Pamp_M_A_12" "Pamp_M_11" "Pamp_M_A_11"
                "Pamp_M_10" "Pamp_M_A_10" "Pamp_M_09" "Pamp_M_A_09" "Pamp_M_08" "Pamp_M_A_08" "Pamp_M_07" "Pamp_M_A_07" 
                "Pamp_REF" 
                .
        if periode = 04 then
            export stream etat11 delimiter ";" "Soc" "Fam" "ARTICLE" "LIBELLE" "Val" 
                "Pamp_M_06" "Ind" "Col_Ecart" "Ec_%" "Total_Ecart"
                "Pamp_M_04" "Pamp_M_A_04" "Pamp_M_03" "Pamp_M_A_03"
                "Pamp_M_02" "Pamp_M_A_02" "Pamp_M_01" "Pamp_M_A_01" "Pamp_M_12" "Pamp_M_A_12" "Pamp_M_11" "Pamp_M_A_11"
                "Pamp_M_10" "Pamp_M_A_10" "Pamp_M_09" "Pamp_M_A_09" "Pamp_M_08" "Pamp_M_A_08" "Pamp_M_07" "Pamp_M_A_07" 
                "Pamp_REF" 
                .
        if periode = 05 then
            export stream etat11 delimiter ";" "Soc" "Fam" "ARTICLE" "LIBELLE" "Val" 
                "Pamp_M_06" "Ind" "Col_Ecart" "Ec_%" "Total_Ecart"
                "Pamp_M_05" "Pamp_M_A_05" "Pamp_M_04" "Pamp_M_A_04" "Pamp_M_03" "Pamp_M_A_03"
                "Pamp_M_02" "Pamp_M_A_02" "Pamp_M_01" "Pamp_M_A_01" "Pamp_M_12" "Pamp_M_A_12" "Pamp_M_11" "Pamp_M_A_11"
                "Pamp_M_10" "Pamp_M_A_10" "Pamp_M_09" "Pamp_M_A_09" "Pamp_M_08" "Pamp_M_A_08" "Pamp_M_07" "Pamp_M_A_07" 
                "Pamp_REF" 
            .
        if periode = 06 then
            export stream etat11 delimiter ";" "Soc" "Fam" "ARTICLE" "LIBELLE" "Val" 
                "Pamp_M_06" "Ind" "Col_Ecart" "Ec_%" "Total_Ecart"
                "Pamp_M_06" "Pamp_M_A_06" "Pamp_M_05" "Pamp_M_A_05" "Pamp_M_04" "Pamp_M_A_04" "Pamp_M_03" "Pamp_M_A_03"
                "Pamp_M_02" "Pamp_M_A_02" "Pamp_M_01" "Pamp_M_A_01" "Pamp_M_12" "Pamp_M_A_12" "Pamp_M_11" "Pamp_M_A_11"
                "Pamp_M_10" "Pamp_M_A_10" "Pamp_M_09" "Pamp_M_A_09" "Pamp_M_08" "Pamp_M_A_08" "Pamp_M_07" "Pamp_M_A_07" 
                "Pamp_REF" 
                .
    
        do i-fam = 1 to num-entries (l-fam) :
            for each artapr 
            where artapr.codsoc = articl-soc 
            and   artapr.famart = entry (i-fam, l-fam)
            use-index artapr-4 
            no-lock 
            by artapr.famart by artapr.soufam by artapr.sssfam by artapr.libart by artapr.articl :
    
                /* initialisations */
                do aa = 1 to 12: 
                    pamp12n[aa ] = 0. 
                    pamp12s[aa ] = 0. 
                end. 
                assign  nolig = 0    topvar = no       topv = no        topr = no         tops = no       topimp = no
                        plig = ""    pamp06 = 0        pampref = 0      topvid = yes      pampvar = 0     colvar = 0
                        l-col = ""   wtoteca = 0       topmch = no      wpourc = 0.
    
                /* recherche des PAMP dans STOARR */
                /* recherche 6 premiers mois si periode saisie de 7 a 12 */
                if periode >= 7 and periode <= 12   then do:
                    find stocks 
                    where stocks.codsoc    = artapr.codsoc
                    and   stocks.articl    = artapr.articl
                    and   stocks.magasin   = ""
                    and   stocks.evenement = "STOARR"
                    and   stocks.exercice  = annee
                    no-lock no-error.
                    if available stocks then do:
                        do aa = 7 to periode: 
                            pamp12n[aa - 6] = stocks.prix[aa]. 
                            if stocks.prix[aa] = 0 then tops = yes.
                        end. 
                        pamp06 = stocks.prix[6].     
                    end.
                    if periode-1 <> 6 then do:
                        find stocks 
                        where stocks.codsoc  = artapr.codsoc
                        and stocks.articl    = artapr.articl
                        and stocks.magasin   = ""
                        and stocks.evenement = "STOARR-VAL"
                        and stocks.exercice  = annee
                        no-lock no-error.
                        if available stocks then do:
                            do aa = 7 to periode-1: 
                                pamp12s[aa - 6] = stocks.prix[aa]. 
                                if stocks.prix[aa] = 0 then tops = yes.
                                if stocks.prix[aa] <> 0 then pampref = stocks.prix[aa] .
                            end. 
                        end.
                    end. /* fin if periode-1 <> 6 */
                end.
                /* recherche nb periodes < a 6 (periode = 1 a 6 ) */
                else do:
                    /* periode 7 a 12 */
                    find stocks 
                    where stocks.codsoc    = artapr.codsoc
                    and   stocks.articl    = artapr.articl
                    and   stocks.magasin   = ""
                    and   stocks.evenement = "STOARR"
                    and   stocks.exercice  = annee-1
                    no-lock no-error.
                    if available stocks then do:
                        do aa = 7 to 12: 
                            pamp12n[aa - 6] = stocks.prix[aa]. 
                            if stocks.prix[aa] = 0 then tops = yes.
                        end. 
                        pamp06 = stocks.prix[6].     
                    end.
                    find stocks 
                    where stocks.codsoc    = artapr.codsoc
                    and   stocks.articl    = artapr.articl
                    and   stocks.magasin   = ""
                    and   stocks.evenement = "STOARR-VAL"
                    and   stocks.exercice  = annee-1
                    no-lock no-error.
                    if available stocks then do:
                        do aa = 7 to 12: 
                            pamp12s[aa - 6] = stocks.prix[aa]. 
                            if stocks.prix[aa] = 0 then tops = yes.
                            if stocks.prix[aa] <> 0 then pampref = stocks.prix[aa] .
                        end. 
                    end.
                    /* periode 1 a 6 */
                    find stocks 
                    where stocks.codsoc    = artapr.codsoc
                    and stocks.articl    = artapr.articl
                    and stocks.magasin   = ""
                    and stocks.evenement = "STOARR"
                    and stocks.exercice  = annee
                    no-lock no-error.
                    if available stocks then do:
                        do aa = 1 to periode: 
                            pamp12n[aa + 6] = stocks.prix[aa]. 
                            if stocks.prix[aa] = 0 then tops = yes.
                        end. 
                    end.
                    if periode-1 <> 12 then do:
                        find stocks 
                        where stocks.codsoc    = artapr.codsoc
                        and stocks.articl    = artapr.articl
                        and stocks.magasin   = ""
                        and stocks.evenement = "STOARR-VAL"
                        and stocks.exercice  = annee
                        no-lock no-error.
                        if available stocks then do:
                            do aa = 1 to periode-1: 
                                pamp12s[aa + 6] = stocks.prix[aa]. 
                                if stocks.prix[aa] = 0 then tops = yes.
                                if stocks.prix[aa] <> 0 then pampref = stocks.prix[aa] .
                            end. 
                        end.
                    end. /* fin if periode-1 <> 12  */
                end.
    
                find first lignes 
                where lignes.codsoc = artapr.codsoc
                and   lignes.motcle = "ach"
                and   lignes.articl = artapr.articl
                and   ( lignes.typcom = "RCF" or 
                        lignes.typcom = "RAF" or 
                        lignes.typcom = "RAR" or 
                        lignes.typcom = "RAD" )
                and lignes.datdep >= sdebdat  
                and lignes.datdep <= sfindat
                use-index artdat 
                no-lock  no-error.
                if available lignes then topr = yes.
                else do :
                    find first bastat 
                    where bastat.codsoc = artapr.codsoc
                    and   bastat.motcle = "ach"
                    and   bastat.articl = artapr.articl
                    and   ( bastat.typcom = "RCF" or 
                            bastat.typcom = "RAF" or 
                            bastat.typcom = "RAR" or 
                            bastat.typcom = "RAD" )
                    and   bastat.datdep >= sdebdat  
                    and   bastat.datdep <= sfindat
                    use-index artdat 
                    no-lock  no-error.
                    if available bastat then topr = yes.
                end .
          
                find first b-lignes
                where b-lignes.codsoc = artapr.codsoc
                and   b-lignes.motcle = "vte"
                and   b-lignes.articl = artapr.articl
                and   b-lignes.typcom begins "L"
                and   b-lignes.datdep >= sdebdat01
                and   b-lignes.datdep <= sfindat
                use-index artdat no-lock  no-error .
                if available b-lignes then topv = yes. 
                else do:
                    find first b-bastat 
                    where b-bastat.codsoc = artapr.codsoc
                    and   b-bastat.motcle = "vte"
                    and   b-bastat.articl = artapr.articl
                    and   b-bastat.typcom begins "L"
                    and   b-bastat.datdep >= sdebdat01
                    and   b-bastat.datdep <= sfindat
                    use-index artdat no-lock  no-error .
                    if available b-bastat then topv = yes. 
                end.
    
                /* Rechercher le PAMP de r�f�rence */
                if periode = 07 then pampref = pamp06.
    
                /* determination nombre de periode */
    
                if periode >= 7 and periode <= 12   then nbper = periode - 6.
                                                    else nbper = periode + 6.
    
                do aa = 1 to nbper :
                    if pamp12n[aa] <> 0 then do: 
                        topvid = no. leave. 
                    end.
                end.
                if topvid = yes then do:
                    do aa = 1 to nbper :
                        if pamp12s[aa] <> 0 then do: 
                            topvid = no. leave. 
                        end.
                    end.
                end.
    
                /* Pas de lignes si pas de reception, pas de vente, et tous les PAMP � ZERO */
    
                if topvid = yes and topr = no and topv = no and pampref = 0 then next.
    
                if topr = yes then plig = "R". 
                if topv = yes then 
                    if plig = ""    then plig = plig + "V". 
                                    else plig = plig + "+V".
       
                do aa = 1 to nbper  :
                    if nbper <= 6 then do: 
                        wannee = annee . 
                        bb = aa + 6.
                    end.
                    else if aa <= 6 then do:
                        wannee = annee-1.
                        bb = aa + 6.
                    end.
                    else do:
                        wannee = annee.
                        bb = aa - 6. 
                    end. 
                  
                    wqteach = 0.
                    cc = bb - 1.
                    /* Prendre la quantite achete dans STARTI */
                    for each starti 
                    where starti.codsoc    = artapr.codsoc
                    and   starti.articl    = artapr.articl
                    and   starti.motcle    = "ACH"
                    and   starti.annee     = wannee
                    use-index primaire no-lock :
                        wqteach = wqteach + starti.qte[bb]. 
                    end.
                    /* ajout le 23/10/2012 par CCO demande JM BRUN */   
                    for each lignes 
                    where lignes.codsoc = artapr.codsoc
                    and   lignes.motcle = "ach"
                    and   lignes.articl = artapr.articl
                    and   lignes.nivcom < "7"
                    and   ( lignes.typcom = "RCF" or
                            lignes.typcom = "RAF" or
                            lignes.typcom = "RAR" or
                            lignes.typcom = "RAD" )
                    and   lignes.datdep >= sdebdat
                    and   lignes.datdep <= sfindat
                    and    month(lignes.datdep) = bb 
                    use-index artdat
                    no-lock:
                        wqteach = wqteach + lignes.qte[2]. 
                    end.   
                 
                    wvalo1 = pamp12n[aa] * wqteach .
                    wpmp1  = pamp12n[aa] .
        
                    if aa = nbper then do: 
                        wvalo2 = pampref * wqteach. 
                        wpmp2  = pampref.  
                    end.
                    else do: 
                        wvalo2 = pamp12s[aa] * wqteach.
                        wpmp2  = pamp12s[aa].
                    end. 
    
                    pmpb = wpmp1 * ( 1 - (wpour * 0.01) ).
                    pmph = wpmp1 * ( 1 + (wpour * 0.01) ).
        
                    if wmouc = "O" then
                        put stream mouchard artapr.articl nbper aa wqteach wvalo1 wvalo2 wvalo1 - wvalo2 format ">>>>>>>>>>>9.99-" 
                            wpmp1 wpmp2 pmpb pmph skip.
              
                    if wqteach = 0 then do:
                        if (wpmp2 < pmpb or wpmp2 > pmph) and wpmp2 <> 0 and wpmp1 <> 0 then do: 
                            wtoteca = wtoteca + abs (wvalo1 - wvalo2). 
                            topvar = yes.
                            pampvar = pamp12n[aa].
                            colvar  = bb.
                            l-col   = string(colvar, "99").
                            topimp = yes.
                            if wpmp2 <> 0 then wpourc = round ( (wpmp1 - wpmp2) / wpmp2, 3).
                            run p-impression.
                        end. 
                    end.
                    else do:
                        if ( abs (wvalo1 - wvalo2) > wecaval and wpmp2 <> 0 and wpmp1 <> 0 ) and
                            ( (wpmp2 < pmpb or wpmp2 > pmph) and wpmp2 <> 0 and wpmp1 <> 0)
                        then do: 
                            wtoteca = wtoteca + abs (wvalo1 - wvalo2). 
                            topvar = yes.
                            pampvar = pamp12n[aa].
                            colvar  = bb.
                            l-col   = string(colvar, "99").
                            topimp = yes.
                            if wpmp2 <> 0 then wpourc = round ( (wpmp1 - wpmp2) / wpmp2, 3).
                            run p-impression.
                        end.
                    end.
                end. /* aa = 1 to nbper */
                if topimp = no and ( ((tops = yes or topv = yes or topr = yes or pampref = 0 ) and wecaval = 0 and wpour = 0 ) )
                then 
                    run p-impression.
            end.

        END. /* Frame a */
        if wmouc = "O" then 
            output stream mouchard close.
            output stream ficpmp close.
            output stream etat11 close.
  
    end. /* do i = 1 to num-entries (l) */ .
    
end. /* fin proc traitement */ 
procedure p-impression:
   nolig = nolig + 1.
   
   if periode = 07 then export stream etat11 delimiter ";" 
         artapr.codsoc artapr.famart artapr.articl artapr.libart1[1] plig 
         pamp06 nolig l-col wpourc wtoteca 
         pamp12n[1] pamp12s[1] 
         pampref 
         .
         /* tops topv topr pampref topvar pampvar. */
         
   if periode = 08 then export stream etat11 delimiter ";" 
         artapr.codsoc artapr.famart artapr.articl artapr.libart1[1] plig 
         pamp06 nolig l-col wpourc wtoteca 
         pamp12n[2] pamp12s[2] 
         pamp12n[1] pamp12s[1] 
         pampref 
         .
   if periode = 09 then export stream etat11 delimiter ";" 
         artapr.codsoc artapr.famart artapr.articl artapr.libart1[1] plig 
         pamp06 nolig l-col wpourc wtoteca 
         pamp12n[3] pamp12s[3]
         pamp12n[2] pamp12s[2]
         pamp12n[1] pamp12s[1] 
         pampref 
         .
   if periode = 10 then export stream etat11 delimiter ";" 
         artapr.codsoc artapr.famart artapr.articl artapr.libart1[1] plig 
         pamp06 nolig l-col wpourc wtoteca 
         pamp12n[4] pamp12s[4]
         pamp12n[3] pamp12s[3]
         pamp12n[2] pamp12s[2]
         pamp12n[1] pamp12s[1] 
         pampref 
         .
   if periode = 11 then export stream etat11 delimiter ";" 
         artapr.codsoc artapr.famart artapr.articl artapr.libart1[1] plig 
         pamp06 nolig l-col wpourc wtoteca
         pamp12n[5] pamp12s[5] 
         pamp12n[4] pamp12s[4]
         pamp12n[3] pamp12s[3]
         pamp12n[2] pamp12s[2]
         pamp12n[1] pamp12s[1]
         pampref 
         .
   if periode = 12 then export stream etat11 delimiter ";" 
         artapr.codsoc artapr.famart artapr.articl artapr.libart1[1] plig 
         pamp06 nolig l-col wpourc wtoteca 
         pamp12n[6] pamp12s[6] 
         pamp12n[5] pamp12s[5]
         pamp12n[4] pamp12s[4]
         pamp12n[3] pamp12s[3]
         pamp12n[2] pamp12s[2]
         pamp12n[1] pamp12s[1]
         pampref 
         .
   if periode = 01 then export stream etat11 delimiter ";" 
         artapr.codsoc artapr.famart artapr.articl artapr.libart1[1] plig 
         pamp06 nolig l-col wpourc wtoteca 
         pamp12n[7] pamp12s[7] 
         pamp12n[6] pamp12s[6]
         pamp12n[5] pamp12s[5]
         pamp12n[4] pamp12s[4]
         pamp12n[3] pamp12s[3]
         pamp12n[2] pamp12s[2]
         pamp12n[1] pamp12s[1]
         pampref 
         .
   if periode = 02 then export stream etat11 delimiter ";" 
         artapr.codsoc artapr.famart artapr.articl artapr.libart1[1] plig 
         pamp06 nolig l-col wpourc wtoteca 
         pamp12n[8] pamp12s[8] 
         pamp12n[7] pamp12s[7]
         pamp12n[6] pamp12s[6]
         pamp12n[5] pamp12s[5]
         pamp12n[4] pamp12s[4]
         pamp12n[3] pamp12s[3]
         pamp12n[2] pamp12s[2]
         pamp12n[1] pamp12s[1]
         pampref 
         .
   if periode = 03 then export stream etat11 delimiter ";" 
         artapr.codsoc artapr.famart artapr.articl artapr.libart1[1] plig 
         pamp06 nolig l-col wpourc wtoteca 
         pamp12n[9] pamp12s[9] 
         pamp12n[8] pamp12s[8]
         pamp12n[7] pamp12s[7]
         pamp12n[6] pamp12s[6]
         pamp12n[5] pamp12s[5]
         pamp12n[4] pamp12s[4]
         pamp12n[3] pamp12s[3]
         pamp12n[2] pamp12s[2]
         pamp12n[1] pamp12s[1]
         pampref 
         .
   if periode = 04 then export stream etat11 delimiter ";" 
         artapr.codsoc artapr.famart artapr.articl artapr.libart1[1] plig 
         pamp06 nolig l-col wpourc wtoteca 
         pamp12n[10] pamp12s[10] 
         pamp12n[9] pamp12s[9]
         pamp12n[8] pamp12s[8]
         pamp12n[7] pamp12s[7]
         pamp12n[6] pamp12s[6]
         pamp12n[5] pamp12s[5]
         pamp12n[4] pamp12s[4]
         pamp12n[3] pamp12s[3]
         pamp12n[2] pamp12s[2]
         pamp12n[1] pamp12s[1]
         pampref 
         .
   if periode = 05 then export stream etat11 delimiter ";" 
         artapr.codsoc artapr.famart artapr.articl artapr.libart1[1] plig 
         pamp06 nolig l-col wpourc wtoteca 
         pamp12n[11] pamp12s[11]
         pamp12n[10] pamp12s[10]
         pamp12n[9] pamp12s[9]
         pamp12n[8] pamp12s[8]
         pamp12n[7] pamp12s[7]
         pamp12n[6] pamp12s[6]
         pamp12n[5] pamp12s[5]
         pamp12n[4] pamp12s[4]
         pamp12n[3] pamp12s[3]
         pamp12n[2] pamp12s[2]
         pamp12n[1] pamp12s[1]
         pampref 
         .
   if periode = 06 then export stream etat11 delimiter ";" 
         artapr.codsoc artapr.famart artapr.articl artapr.libart1[1] plig 
         pamp06 nolig l-col wpourc wtoteca 
         pamp12n[12] pamp12s[12]
         pamp12n[11] pamp12s[11]
         pamp12n[10] pamp12s[10]
         pamp12n[9] pamp12s[9]
         pamp12n[8] pamp12s[8]
         pamp12n[7] pamp12s[7]
         pamp12n[6] pamp12s[6]
         pamp12n[5] pamp12s[5]
         pamp12n[4] pamp12s[4]
         pamp12n[3] pamp12s[3]
         pamp12n[2] pamp12s[2]
         pamp12n[1] pamp12s[1]
         pampref 
         .
   if topmch = no then 
      do: put stream ficpmp artapr.articl skip.
      topmch = yes.
      end.
   if wmouc = "O" then
       put stream mouchard artapr.codsoc " " artapr.articl " " 
           tops "  " topv "  " topr "  " pampref " " topvar "  " pampvar " " 
           nolig l-col wtoteca "Topimp=" topimp skip.
end .