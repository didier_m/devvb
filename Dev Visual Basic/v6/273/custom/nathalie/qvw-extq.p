/*==========================================================================*/
/*                           Q V W - E X T Q . P                            */
/* Extraction Journaliere des donnees pour QVW                              */
/*--------------------------------------------------------------------------*/
/* Saisie Parametres                                                        */
/*==========================================================================*/

{ connect.i        }
{ aide.i           }
{ qvw-ext0.i      }
{ compo.f    "new" }

def var valeur as char.

/*------------------------*/
/* Recup. date traitement */
/*------------------------*/
{ qvw-extdat.i "exclusive-lock" }

/*--------------*/
/* Saisie zones */
/*--------------*/
assign compo-vecteur = "<01>,1,2,3,4"
       compo-bloque  = no
       .

SAISIE :
repeat :

    { compo.i }
    /*--------------------*/
    /* Type de traitement */
    /*--------------------*/
    repeat while compo-motcle = 1 with frame fr-saisie :

        if type-trt = "" then type-trt = entry(1, libelle[16]).

        { fncompo.i type-trt fr-saisie }
        { z-cmp.i type-trt "' '" }

/*        if lookup(type-trt, libelle[16] ) = 0  */
        if lookup(type-trt, libelle[16] + ",X" ) = 0 
        then do :
            bell. bell.
            message type-trt libelle[20].
            readkey pause 2.
            undo, retry.
        end .

        if type-trt = entry(1, libelle[16]) /* Traitement Exploitation */
        then do :
            assign compo-bloque    = yes
                   compo-bloque[1] = no
                   l-soc           = libelle[17]
                   l-tables        = libelle[18]
                   .
            display l-soc l-tables date-debut.
        end.
        else compo-bloque = no.

        if type-trt = entry(1, libelle[16])
           then lib-trt = entry(1, libelle[15]).
           else lib-trt = entry(2, libelle[15]).

        type-trt = caps(type-trt).
        display type-trt lib-trt.

        next saisie.

    end. /* repeat : 1 */
     
    /*--------------------*/
    /* Societes a traiter */
    /*--------------------*/
    repeat while compo-motcle = 2 with frame fr-saisie :

        if l-soc = "" then l-soc = libelle[17].

        { fncompo.i l-soc fr-saisie }
        { z-cmp.i l-soc "' '" }

        if l-soc = ""
        then do :
            bell. bell.
            message libelle[21].
            readkey pause 2.
            undo, retry.
        end.

        do ind = 1 to num-entries(l-soc) :
            valeur = entry(ind, l-soc).
            if lookup(valeur, libelle[17]) = 0
            then do :
                bell. bell.
                message valeur libelle[20].
                readkey pause 2.
                undo, retry.
            end.
        end .

        next saisie.

    end. /* repeat : 2 */

    /*-------------------*/
    /* Tables a extraire */
    /*-------------------*/
    repeat while compo-motcle = 3 with frame fr-saisie :

        if l-tables = "" then l-tables = libelle[18].

        { fncompo.i l-tables fr-saisie }
        { z-cmp.i l-tables "' '" }

        if l-tables = ""
        then do :
            bell. bell.
            message libelle[21].
            readkey pause 2.
            undo, retry.
        end.

        do ind = 1 to num-entries(l-tables) :
            valeur = entry(ind, l-tables).
            if lookup(valeur, libelle[18]) = 0
            then do :
                bell. bell.
                message valeur libelle[20].
                readkey pause 2.
                undo, retry.
            end.
        end .

        l-tables = caps(l-tables).
        display l-tables.

        next saisie.

    end. /* repeat : 3 */

    /*-----------------------*/
    /* Date debut extraction */
    /*-----------------------*/
    repeat while compo-motcle = 4 with frame fr-saisie :

        { fncompo.i date-debut fr-saisie }
        { z-cmp.i date-debut "' '" }

        if date-debut = ?
        then do :
            bell. bell.
            message date-debut libelle[21].
            readkey pause 2.
            undo, retry.
        end .
        if date-debut > today and type-trt <> "X"
        then do :
            bell. bell.
            message date-debut libelle[20].
            readkey pause 2.
            undo, retry.
        end.

        next saisie.

    end. /* repeat : 4 */

end. /* SAISIE-GENERALE */

if type-trt = "X" then type-trt = "E".
