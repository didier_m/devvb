/*---------------------------------------------------------------------------*/
/*                          S C A E - E D 1 . I                              */
/*---------------------------------------------------------------------------*/

lig-affich = string( sortie.articl , "999999")  + " " +
             string(sortie.lib-qte,"x(17)")     + " " + 
             string(sortie.qteral,">>>>>9-")    + " " +
             string(sortie.partiel,"x")         + " " + 
             string(sortie.ligpart,"x")         + " " + 
             string(sortie.rel,"x")             + " " +
             string(sortie.datliv)              + " " +
             string(sortie.ref-cli,"x(17)")     + " " +
             string(sortie.depot,"x(3)") .
             
display  sortie.bon @ lig-chrono  lig-affich  with frame FR-SCROLX .

lig-affich = "" .
