/*==========================================================================*/
/*                           E X T C Y L - D . I                            */
/* Extraction des donnees pour Cylande                                      */
/*--------------------------------------------------------------------------*/
/* interface fournisseurs                                                   */
/*==========================================================================*/
/* DGR 01062018 : ajout filiere fournisseur                                 */

CREATE SAX-WRITER hSAXWriter.
hSAXWriter:ENCODING = "UTF-8".
hSAXWriter:FORMATTED = FALSE .  /* 	si TRUE: fichier de sortie lisible mais ajout de blancs 
									a la place des sauts de lignes a l'integration dans UR */

lOK = hSAXWriter:SET-OUTPUT-DESTINATION ( "file", wficsortie ).
lOK = hSAXWriter:START-DOCUMENT ( ).
lOK = hSAXWriter:WRITE-COMMENT ( "EXTRACTION DES FOURNISSEURS" ) .
lOK = hSAXWriter:START-ELEMENT ( "supplierListType" ).

For each t-fou 
no-lock: 

   nblus = nblus + 1.
      
   aa = 0.
   if t1soc01 = "O" then aa = aa + 1.
   if t1soc08 = "O" then aa = aa + 1.
   if t1soc59 = "O" then aa = aa + 1.
   if t1soc88 = "O" then aa = aa + 1.

   bb = 0.
   if t1fer01 = "F" then bb = bb + 1.
   if t1fer08 = "F" then bb = bb + 1.
   if t1fer59 = "F" then bb = bb + 1.
   if t1fer88 = "F" then bb = bb + 1.
   if aa = bb then wblo = "F". else wblo = "O".

   code-barre = t1gln.
   if code-barre <> "" then do:
      run calgenco.p ( code-barre, output gencod-OK) .
      if not gencod-ok then code-barre = "".     
   end.
   if code-barre = "" then do:
      if trim( t-fou.t1codauxfac ) <> "" then   
         code-barre = "0000" + soc-fullcyl + trim( t-fou.t1codauxfac ) + " " .
      else 
         code-barre = "0000" + soc-fullcyl + trim( t-fou.t1codaux ) + " " .
      
      /* Controle Gencod numerique sur 13 */
      valeur = dec(code-barre ) no-error.
		if error-status:error or valeur = 0 or length(code-barre ) <> 13	then leave.

		/* Controle Cle Gencod = Cle calculee */
		Assign 	gen-code-13 = int( substr( code-barre , 1, 1 ) )
					gen-code-12 = int( substr( code-barre , 2, 1 ) )
					gen-code-11 = int( substr( code-barre , 3, 1 ) )
					gen-code-10 = int( substr( code-barre , 4, 1 ) )
					gen-code-9  = int( substr( code-barre , 5, 1 ) )
					gen-code-8  = int( substr( code-barre , 6, 1 ) )
					gen-code-7  = int( substr( code-barre , 7, 1 ) )
					gen-code-6  = int( substr( code-barre , 8, 1 ) )
					gen-code-5  = int( substr( code-barre , 9, 1 ) )
					gen-code-4  = int( substr( code-barre , 10, 1 ) )
					gen-code-3  = int( substr( code-barre , 11, 1 ) )
					gen-code-2  = int( substr( code-barre , 12, 1 ) )
					gen-code-1  = int( substr( code-barre , 13, 1 ) )
             
					gen-pair    = gen-code-12 + gen-code-10 + gen-code-8 +
								  gen-code-6  + gen-code-4  + gen-code-2
					gen-pair    = gen-pair * 3
         
					gen-impair  = gen-code-13 + gen-code-11 + gen-code-9 +
								  gen-code-7  + gen-code-5  + gen-code-3
		
					gen-gencod = gen-pair + gen-impair
					gen-sup    = gen-gencod / 10
					gen-sup    = ( gen-sup ) * 10.
                                      
		If gen-sup < gen-gencod then gen-sup = gen-sup + 10.

		gen-cle = gen-sup - gen-gencod.
		assign substr(code-barre ,13,1) = string(gen-cle).

		run calgenco.p ( code-barre , output lok ).
		if not lok then do:
         put stream ano unformatted "FO-probl�me affectation auto GLN " t-fou.t1codaux  skip.
         next.
      end.
   end.

   find auxili 
   where  rowid (auxili ) = t-fou.rowauxili
   no-lock no-error.
   if not available auxili then do:
      put stream ano unformatted   "FO - Tiers non trouve sur table comptable auxili " t1codaux skip.
      toprej = yes.
      next.
   end.   
   if trim (substr ( auxili.adres[5] , 7 ) ) = "" then do:
      put stream ano unformatted   "FO - pas de ville : " auxili.codsoc " " auxili.typaux " " auxili.codaux " " auxili.libabr skip.
      toprej = yes.
      next.
   end.

   /* correspondance codes pays */
   wpays = auxili.pays.
   if wpays = "F" then wpays = "FR".
   if auxili.pays = "B"     then wpays = "BE". 
   if auxili.pays = "CAN"   then wpays = "CA". 
   if auxili.pays = "D"     then wpays = "DK". 
   if auxili.pays = "E"     then wpays = "ES". 
   if auxili.pays = "GRECE" then wpays = "GR". 
   if auxili.pays = "I"     then wpays = "IT". 
   if auxili.pays = "JPN"   then wpays = "JP". 
   if auxili.pays = "PB"    then wpays = "NL". 
   if auxili.pays = "S"     then wpays = "CH". 
   if auxili.pays = "USA"   then wpays = "US". 

   if TRIM ( auxili.adres[2]) <> "" then wadres = TRIM(auxili.adres[2]). 
                                    else wadres = "." .
   /* Les CIRCUITS */
   /* Valeurs possibles Circuit GRASSOT=CIRCGRA                                   */
   /*                   Circuit ECoop  =CIRCEC                                    */
   /*                   Circuit Asec   =CIRCASEC                                  */
   /*                   Circuit Vitry  =CIRCVIT                                   */
   /* R�gle il ne faut qu'un seul fournisseur principal, mais il en faut toujours 1 */
   
   /* 13032018 par defaut on envoie tous les circuits, avec toujours CIRGRA en principal             
                    
   IF wblo = "F" THEN DO:               /* le fournisseur est ferm� sur toute les soci�t�s trait�es de 1 a n */         
      wmain88 = "false".
      wmain01 = "false".
      wmain08 = "false".
      wmain59 = "false".

      wremo88 = "true".
      wremo01 = "true".
      wremo08 = "true".
      wremo59 = "true".
    
      if t1soc01 = "O" then assign wmain01 = "true"  wremo01 = "false". 
         else if t1soc08 = "O" then assign wmain08 = "true" wremo08 = "false". 
            else if t1soc59 = "O" then assign  wmain59 = "true" wremo59 = "false".
               else if t1soc88 = "O" then assign wmain88 = "true" wremo88 = "false". 
   END.
   ELSE DO:                             /* le fournisseur est au moins ouvert sur une soci�t�       */

      wmain88 = "false".
      wmain01 = "false".
      wmain08 = "false".
      wmain59 = "false".
 
      /* t1ferxx : "O" ==> Ouvert, "F" ==> Ferm�                          */
  
      if t1fer01 = "O" then wremo01 = "false". else wremo01 = "true". 
      if t1fer08 = "O" then wremo08 = "false". else wremo08 = "true".
      if t1fer59 = "O" then wremo59 = "false". else wremo59 = "true".
      if t1fer88 = "O" then wremo88 = "false". else wremo88 = "true".

      if t1fer01 = "O" then wmain01 = "true".
         else if t1fer08 = "O" then wmain08 = "true".            
            else if t1fer59 = "O" then wmain59 = "true".              
               else if t1fer88 = "O" then wmain88 = "true".
   END.
   
   */ 
   
   assign   wmain01 = "false"
            wmain08 = "false"
            wmain59 = "false"
            wmain88 = "true".
                                   
   /* ecriture fichier supplierListType */ 
	ASSIGN 
		/* debut etiquette "values" */
		lOK = hSAXWriter:START-ELEMENT ( "values" )	
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "externalReference", TRIM(t1codaux) )
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", TRIM(t1codaux) )
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "account", TRIM(t1codaux) ) /* DGR 24052018 */
         lOK = hSAXWriter:START-ELEMENT ( "name" )
				lOK = hSAXWriter:WRITE-CDATA ( TRIM(auxili.adres[1]) )
			lOK = hSAXWriter:END-ELEMENT ( "name" )
         lOK = hSAXWriter:START-ELEMENT ( "description" )
				lOK = hSAXWriter:WRITE-CDATA ( TRIM(auxili.adres[1]) )
			lOK = hSAXWriter:END-ELEMENT ( "description" )
         
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "category", "PRO" )      /* Valeurs possibles PRO, SER, CON, TRA                                     */
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", "FOUR" )         /* Valeurs possibles FOUR, TRIN, TREX                                       */
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "orderManagement", "REBL" )         /* Valeurs possibles REBL                                       */
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "state", wblo )          /* Valeurs possibles O, F                                                   */
         lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "stateLocal", wblo )     /* Valeurs possibles O, F                                                   */ 

         lOK = hSAXWriter:START-ELEMENT ( "addresses" )
            lOK = hSAXWriter:START-ELEMENT ( "address" )
               lOK = hSAXWriter:START-ELEMENT ( "addressLine1" )
                  lOK = hSAXWriter:WRITE-CDATA ( wadres )
               lOK = hSAXWriter:END-ELEMENT ( "addressLine1" )

               lOK = hSAXWriter:START-ELEMENT ( "addressLine2" )
                  lOK = hSAXWriter:WRITE-CDATA (  TRIM(auxili.adres[3]) )
               lOK = hSAXWriter:END-ELEMENT ( "addressLine2" )

               lOK = hSAXWriter:START-ELEMENT ( "addressLine3" )
                  lOK = hSAXWriter:WRITE-CDATA ( TRIM(auxili.adres[4]) )
               lOK = hSAXWriter:END-ELEMENT ( "addressLine3" )
               
               lOK = hSAXWriter:START-ELEMENT ( "postalCode" )
                  lOK = hSAXWriter:WRITE-CDATA ( SUBSTR(auxili.adres[5], 1, 5) )
               lOK = hSAXWriter:END-ELEMENT ( "postalCode" )
               
               lOK = hSAXWriter:START-ELEMENT ( "city" )
                  lOK = hSAXWriter:WRITE-CDATA ( SUBSTR(auxili.adres[5], 7) )
               lOK = hSAXWriter:END-ELEMENT ( "city" )
               
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "country", wpays )
               
               lOK = hSAXWriter:START-ELEMENT ( "phone" )
                  lOK = hSAXWriter:WRITE-CDATA ( substr( TRIM(t-fou.t1tel) , 1 , 32) )
               lOK = hSAXWriter:END-ELEMENT ( "phone" )

               lOK = hSAXWriter:START-ELEMENT ( "fax" )
                  lOK = hSAXWriter:WRITE-CDATA ( substr( TRIM(t-fou.t1fax) , 1 , 32 ) )
               lOK = hSAXWriter:END-ELEMENT ( "fax" )
               
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "globalLocationNumber", code-barre )

               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "main", "true" )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", "FOUR" )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "description", "Adresse" )

            lOK = hSAXWriter:END-ELEMENT ( "address" )
         lOK = hSAXWriter:END-ELEMENT ( "addresses" ).
         
      assign 
         lOK = hSAXWriter:START-ELEMENT ( "supplyChains" )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "main", wmain01 )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", "CIRCEC" )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "incoterm", "INC1" )
         lOK = hSAXWriter:END-ELEMENT ( "supplyChains" )
         lOK = hSAXWriter:START-ELEMENT ( "supplyChains" )
            /* lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "remove", wremo08 ) */
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "main", wmain08 )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", "CIRCASEC" )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "incoterm", "INC1" )
         lOK = hSAXWriter:END-ELEMENT ( "supplyChains" )
         lOK = hSAXWriter:START-ELEMENT ( "supplyChains" )
            /* lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "remove", wremo59 ) */
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "main", wmain59 )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", "CIRCVIT" )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "incoterm", "INC1" )
         lOK = hSAXWriter:END-ELEMENT ( "supplyChains" )
         lOK = hSAXWriter:START-ELEMENT ( "supplyChains" )
            /* lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "remove", wremo88 ) */
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "main", wmain88 )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "type", "CIRCGRA" )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "incoterm", "INC1" )
         lOK = hSAXWriter:END-ELEMENT ( "supplyChains" ).

      assign 
         lOK = hSAXWriter:START-ELEMENT ( "currencies" )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "currency", "EUR" )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "main", "true" )
         lOK = hSAXWriter:END-ELEMENT ( "currencies" ).

      /* DGR 01062018 :filiere */ 
      wcode = trim( t-fou.t1codaux ).
      if t-fou.t1codauxfac <> "" then wcode = trim ( t-fou.t1codauxfac ).
            
      assign 
         lOK = hSAXWriter:START-ELEMENT ( "orderChannels" )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "shortDescription", "FILIERE" )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "description", "FILIERE" )
            lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "main", "true" )
            lOK = hSAXWriter:START-ELEMENT ( "orderAgent" )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", trim( t-fou.t1codaux ) )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "externalReference", trim ( t-fou.t1codaux ) )
            lOK = hSAXWriter:END-ELEMENT ( "orderAgent" )
            
            lOK = hSAXWriter:START-ELEMENT ( "paymentAgent" )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", trim ( t-fou.t1codaux ) )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "externalReference", trim ( t-fou.t1codaux ) )
            lOK = hSAXWriter:END-ELEMENT ( "paymentAgent" )

            lOK = hSAXWriter:START-ELEMENT ( "paymentSupplier" )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "code", wcode )
               lOK = hSAXWriter:WRITE-DATA-ELEMENT ( "externalReference", wcode )
            lOK = hSAXWriter:END-ELEMENT ( "paymentSupplier" )
         
         lOK = hSAXWriter:END-ELEMENT ( "orderChannels" ).
         
   assign 
      lOK = hSAXWriter:END-ELEMENT ( "values" ).
 	if lOK then 
		nbecr = nbecr + 1 .
	else do:
		put stream ano unformatted 
         "probleme ecriture fichier fournisseur  " 
         soc-trt " " 
         auxili.typaux " " 
         auxili.codaux " " 
         auxili.libabr skip.
      toprej = yes.
	end.	

            
end.

lOK = hSAXWriter:END-ELEMENT ( "supplierListType" ) .
lOK = hSAXWriter:END-DOCUMENT () .

DELETE OBJECT hSAXWriter. 


