
/* liste des ventes LIV et LIM avec code echeance CAP                         */
/* lit uniquement entete et lignes (pas de code echeance dans bastat)         */
/* prog : lisechcapl.p (lanceur)                                              */
         

{connect.i}
{ bat-ini.i "new" }

def var sequence         as char format "x(05)"              no-undo.
def var wfic             as char format "x(45)"              no-undo.

def var l-typ as char initial "LIM,LIV".
def var l-soc as char format "x(20)" initial "01,08,58,59".

def var aa    as int.
def var bb    as int.
def var nbr   as int format ">>>>9".
def var wtyp  as char.
def var wsoc  as char.
def var wdatedeb as date.
def var wdatefin as date.
DEF VAR wgo        AS CHAR FORMAT "x".
DEF VAR wweb       AS CHAR FORMAT "x".
def stream sortie1.

/* REPEAT : */

  DO WITH FRAME fr-saisie :
 
    FORM  "Soc a traiter ... : " l-soc no-label 
          SKIP
          "Date mini Facture : " wdatedeb no-label 
          SKIP
          "Date mini Facture : " wdatefin no-label 
          SKIP
          "Spool WEB ....... : " wweb no-label 
          SKIP
          "Validation ...... : " wgo no-label 
          skip
          "Fichier type csv  : " wfic no-label
          skip
          WITH TITLE "Liste des livraisons LIM et LIV -- ECHEANCE CAP" 
          CENTERED .
        

    update l-soc
         help "liste des societes"
         validate (l-soc <> "",
         "obligatoire").
    
    wdatedeb = today.
    wdatefin = today.

    update wdatedeb
         help "Date de facture mini ".

    update wdatefin
         help "Date de facture maxi "
         validate (wdatefin >= wdatedeb,
         "Date de fin doit etre superieur date debut").
 
    /* spool Web */
        
    wweb = "O".
    UPDATE wweb HELP " 'O' : spool Web ; 'N' : spool Car".
    wweb = CAPS(wweb). DISPLAY wweb.

    IF wweb <> "O" AND wweb <> "N" THEN DO :
      MESSAGE "Valeur 'O' ou 'N' seulement".
      PAUSE.
      BELL . BELL .
      READKEY PAUSE 1.
      UNDO, RETRY.
    END.

    /* Validation */
        
    wgo = "O".
    UPDATE wgo HELP " 'O' : Lancement ; 'N' : Retour ".
    wgo = CAPS(wgo). DISPLAY wgo.

    IF wgo <> "O" AND wgo <> "N" THEN DO :
      MESSAGE "Valeur 'O' ou 'N' seulement".
      PAUSE.
      BELL . BELL .
      READKEY PAUSE 1.
      UNDO, RETRY.
    END.
    ELSE IF wgo = "N" THEN UNDO, RETRY .
    If wgo = "O" then do:
      assign codsoc = " " etabli = " " typtab = "ope" prefix = "nume-spool"
             codtab = operat.
      run opeseq.p ("cap" , output sequence, output wfic).

      substr(wfic,length(wfic) - 2, 3) = "csv".

      if wweb =  "O" then do:
                     wfic = "/applic/partageweb/xcspool/" + trim(lower(operat)) + "/" + wfic.
                     IF SEARCH( "/applic/erp/src/tty/nathalie/ch-param.p" ) <> ? THEN DO:
                        wfic  = REPLACE (wfic, "/applic/", "/applic/erp/").
                     END.
                     end.
                     else wfic = "/applic/li/" + trim(lower(operat)) + "/" + wfic.
      
      disp wfic.
    end.
  end.
/* end. */
if wgo <> "O" then leave.

                         
batch-tenu = yes. /* Traitement en Batch */

{ bat-zon.i  "l-soc"      "l-soc"      "c" "''" "''" }
{ bat-zon.i  "wdatedeb"   "wdatedeb"   "d" "''" "''" }
{ bat-zon.i  "wdatefin"   "wdatefin"   "d" "''" "''" }
{ bat-zon.i  "wfic"       "wfic"       "c" "''" "''" }

{ bat-maj.i  "lisechcap.p" }
