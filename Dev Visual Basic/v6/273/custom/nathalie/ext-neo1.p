/*****************************************************************************/
/*==========================================================================*/
/*                            E X T - N E O  1 . P                          */
/* Extraction des donnes pour neotic : agreo + portail adh                  */
/*--------------------------------------------------------------------------*/
/* DGR 07112013: les mvts apports sont sign�s                               */
/* DGR 12112013: si type-adh = 6, on n'envoie que TIERS, EXP, EXP-TIERS     */
/* DGR 26112013: si type-adh = 6, on envoie login et licence par contre     */
/*               on prend ident specif si renseigne en fiche tiers          */
/* DGR 15012015: on envoie les suppression de lien avant les affectations   */
/* DGR 24032015: on n'envoie rien dans la civilit� ( au lieu de " ")        */
/* DGR 05012017: heure de maj dans interface tiers.                         */
/* DGR 23102018: on ecrit 01 et 08 dans m�mes fichiers 01                   */
/*==========================================================================*/ 
{ connect.i        }
{ bat-ini.i  "new" }
{ aide.i     "new" }
{ sel.i      "new" }
{ ext-neo0.i "new" }
{ cereal.i        }
{ cal-echcer.i     }
{ cal-echcer.pp } /* procedure Chargement soldes REMERE, SOULTE, TAF */


/*-------------------------------------*/
/* Definition des param�tres d'appel   */
/*-------------------------------------*/

def input parameter fichier as char format "x(12)".

/*-------------------------------------*/
/* Definition des variables de travail */
/*-------------------------------------*/
def var auxili-soc       like auxili.codsoc                 no-undo .
def var magasi-soc       like auxili.codsoc                 no-undo .
def var type-tiers       like auxili.typaux init "TIE"      no-undo .
def var exploit          as logical                         no-undo .
def var codsoc-trait     as char  extent 2                  no-undo .

def var l-socagreo       as char initial  "01,08"           no-undo .
def var i-agreo          as int                             no-undo .
def var i-agreo2         as int                             no-undo .
def var l-tcagreo        as char                            no-undo .
def var l-tcweb          as char                            no-undo .
def var i-web            as int                             no-undo .
def var i-web2           as int                             no-undo .
def var l-tc08           as char                            no-undo .

def var wtypliaison      as char                            no-undo .
def var wtiers-m         like auxapr.codaux                 no-undo . 

def buffer b-auxapr for auxapr.
def buffer b-auxili for auxili.
def buffer b-auxdet for auxdet.
def buffer b-auxgss for auxgss.
def buffer z-tabges for tabges.

def var woldtc           as char                            no-undo .

def var i-param          as char                            no-undo .
def var o-param          as char                            no-undo .
def var zone             as char                            no-undo .
def var wlib             as char                            no-undo .
def var wlogin           as char                            no-undo .
def var wlib2            as char                            no-undo .
def var wmdp             as char                            no-undo .
def var wdatdebd         as date                            no-undo . 
def var wdatfind         as date                            no-undo .
def var topagreo         as logical                         no-undo .
def var heure-debut      as int                             no-undo .
def var nsoc             as int                             no-undo .
def var nom-table        as char                            no-undo .
def var code-table       as char                            no-undo .
def var nb-lus           as int                             no-undo .
def var per-modulo       as int                             no-undo .
def var i                as int                             no-undo . 
def var a                as int                             no-undo . 
def var i-carac          as int                             no-undo .
def var l-carac          as char extent 14                  no-undo .
def var l-libcar         as char extent 14                  no-undo .
def var l-mvtapp         as char                            no-undo .
def var l-cerealKO       as char                            no-undo .
def var j                as int                             no-undo .
 def var wcodapp         as char                            no-undo .
def var x                as char                            no-undo . 
def var wcamp            as char                            no-undo . 
def var wsolde           as decimal                         no-undo .                             
def var wmt              as decimal                         no-undo .                             
def var xx-pds-brut      like cermvt.pds-brut               no-undo .
def var xx-pds-norm      like cermvt.pds-norm               no-undo .
 
def stream str1.
def stream str2.
def stream str3. 
def stream str4.
def stream str5.

def var fic-neo1 as char .
def var fic-neo2 as char .
def var fic-neo3 as char .
def var fic-neo4 as char .
def var fic-neo5 as char .

/* compteurs */ 
def var nbexp            as int                             no-undo . 
def var nbexptie         as int                             no-undo . 
def var nbtie            as int                             no-undo . 
def var nblog            as int                             no-undo . 
def var nblic            as int                             no-undo . 

def var nbsil            as int                             no-undo . 
def var nbcar            as int                             no-undo . 
def var nbpro            as int                             no-undo . 
def var nbapp            as int                             no-undo . 

def var nbsolde          as int                             no-undo . 
def var topfic           as log                             no-undo .
def var commande         as char                            no-undo .

def var wlicence         as char                            no-undo .
def var nbcap            as int                             no-undo . 

def temp-table t-aux
    field codsoc   like auxapr.codsoc
    field codaux   like auxapr.codaux
    field codaux-m like auxapr.codaux
    field datdeb   as   date
    field datfin   as   date
    field topini   as   log
    field mdp      as char format "x(10)"
index i-aux codsoc codaux.

session:system-alert-boxes = yes .

/*--------------------------*/
/* Chargement aide et texte */
/*--------------------------*/
aide-fichier = "ext-neo0.aid".
run aide-lec.p .

do inddgr = 1 to 30 :
    aide-m = "libel" + string( inddgr ,"99") .
    libelle[inddgr] = aide-m.
    { aide-lib.i }
    libelle[inddgr] = aide-lib.
end.

/*---------------------------*/
/* Chargement des selections */
/*---------------------------*/

run bat-lec.p ( fichier ).

{ bat-cha.i "type-trt"   "type-trt"   "a" }
{ bat-cha.i "l-soc"      "l-soc"      "a" }
{ bat-cha.i "l-tables"   "l-tables"   "a" }
{ bat-cha.i "sel-applic"   "sel-applic"   "a" }
{ bat-cha.i "sel-filtre"   "sel-filtre"   "a" }
{ bat-cha.i "sel-sequence" "sel-sequence" "a" }

/*-----------------*/
/* Initialisations */
/*-----------------*/
if type-trt = entry(1, libelle[16]) then exploit = yes. /* Trt Exploitation */
                                    else exploit = no.


/*-----------------------------------------------------*/
/* Recup. date traitement avec lock enreg. pendant trt */
/*-----------------------------------------------------*/
if exploit then do :
    { ext-neod.i "exclusive-lock" }
end.
else do:
    { ext-neod.i "no-lock" }
end.
message "extraction NEOTIC depuis le " date-debut.

heure-debut = time.

/*-------------------------------------*/
/* Chargement de la liste des TC 08    */
/*-------------------------------------*/
if lookup ("TI", l-tables) <> 0 then do:

    assign  typtab = "PRM"  prefix = "EUREA"  codtab = "TCAGREO" .
    run ch-paray.p( output o-param ) .
    if o-param <> "" then do:

  
        /* TC ASEC */
        assign l-tcagreo = entry( 2, o-param, "|" ).
        if entry(4, o-param, "|" ) <> "" then
            assign l-tcagreo = l-tcagreo + entry( 4, o-param, "|" ).

    end.


    assign l-tc08 = l-tcagreo 
           l-tcweb = "" .
    for each auxdet
    where auxdet.codsoc = ""
    and   auxdet.motcle = "WEB/REP"
    and   auxdet.typaux = "REP"
    and   auxdet.calf09 = "O"
    no-lock:
        /* TC agreo */ 
        if auxdet.calf03 = "O" and (auxdet.cdat03 = ? or auxdet.cdat03 >= date-debut ) then do:
            if l-tcagreo = "" then l-tcagreo = auxdet.codaux.
            else l-tcagreo = l-tcagreo + "," + auxdet.codaux.
        end.
        /* TC WEB */ 
        if auxdet.calf04 = "O" and (auxdet.cdat04 = ? or auxdet.cdat04 >= date-debut ) then do:
            if l-tcweb = "" then l-tcweb = auxdet.codaux.
            else l-tcweb = l-tcweb + "," + auxdet.codaux.
       end.
    end.
end. /* if lookup ("TI", l-tables) <> 0 then do: */

do inddgr = 1 to num-entries( l-soc ) :
    codsoc-trait[inddgr] = entry( inddgr, l-soc ).
end.

message "liste des tc agreo : " l-tcagreo.
message "liste des tc web   : " l-tcweb. 

/*************************** T R A I T E M E N T ******************************/

/*-----------------*/
/* Boucle Societes */
/*-----------------*/

do nsoc = 1 to  num-entries( l-soc ) :


    if codsoc-trait[nsoc] = "" then next .
   
    codsoc-soc = codsoc-trait[nsoc].

    /* Societes de Regroupement */
    regs-app = "NATHALIE".
    { regs-cha.i }
   
    regs-app = "ELODIE".
    { regs-cha.i }
   
    regs-fileacc = "AUXILI" + type-tiers.
    {regs-rec.i}
    auxili-soc = regs-soc .

    { regs-inc.i "magasi" }
     magasi-soc = regs-soc .

     
    /* Prechargmenets des  tiers et des exploitations A ENVOYER traitement commun pour 01 et 08 */ 
    for each auxapr
    where auxapr.codsoc = codsoc-soc
    and   auxapr.typaux = "tie"
    and   auxapr.datmaj >= date-debut
    no-lock, 
        each auxili
    where auxili.codsoc = auxili-soc 
    and   auxili.typaux = auxapr.typaux 
    and   auxili.codaux = auxapr.codaux
    no-lock:
        find t-aux 
        where t-aux.codsoc = auxapr.codsoc
        and   t-aux.codaux = auxapr.codaux
        no-error.
        if not available t-aux then do :
           run cre-t-aux.
           /* message "A -- Creation t-aux" auxapr.codsoc auxapr.typaux auxapr.codaux auxapr.codaux-fiscal t-aux.mdp
           view-as alert-box. */
       end.
    end.

    for each auxili
    where auxili.codsoc = auxili-soc
    and   auxili.typaux = "tie"
    and   auxili.datmaj >= date-debut
    no-lock, 
        each auxapr
    where auxapr.codsoc = codsoc-soc
    and   auxapr.typaux = auxili.typaux
    and   auxapr.codaux = auxili.codaux
    no-lock :
        find t-aux 
        where t-aux.codsoc = auxapr.codsoc
        and   t-aux.codaux = auxapr.codaux
        no-error.
        if not available t-aux then do :
           run cre-t-aux.
           /* message "B -- Creation t-aux" auxapr.codsoc auxapr.typaux auxapr.codaux auxapr.codaux-fiscal t-aux.mdp
           view-as alert-box. */
        end.
    end.

    if lookup ("TI", l-tables) <> 0 then do:
         /* Fichiers communs AGREO et PORTAIL     */
        /*
        assign fic-neo1 = "/applic/li/depart/neotic/"  +  codsoc-trait[nsoc]  + "/exploitations."  + codsoc-trait[nsoc]  +  ".exp" 
               fic-neo2 = "/applic/li/depart/neotic/"  +  codsoc-trait[nsoc]  + "/exp_tiers."      + codsoc-trait[nsoc]  +  ".exp" .

        if not exploit then
            assign fic-neo1 = "/applic/li/dgr/exploitations."   +  codsoc-trait[nsoc]  + ".tst"
                   fic-neo2 = "/applic/li/dgr/exp_tiers."       + codsoc-trait[nsoc]   + ".tst" .
        
        */
        
        assign fic-neo1 = "/applic/li/depart/neotic/01/exploitations.01.exp" 
               fic-neo2 = "/applic/li/depart/neotic/01/exp_tiers.01.exp" .

        if not exploit then
            assign fic-neo1 = "/applic/li/dgr/exploitations.01.tst"
                   fic-neo2 = "/applic/li/dgr/exp_tiers.01.tst" .
                                                                                      
        output stream str1 to value(fic-neo1) append.
        output stream str2 to value(fic-neo2) append.

        assign nbexp    = 0 
               nbexptie = 0
               nbtie = 0
               nblog = 0 
               nblic = 0 . 
        
        /* Fichiers Specifiques PORTAIL ADHERENT   */
        if codsoc-soc = "01" then do: 
            assign fic-neo3 = "/applic/li/depart/neotic/"   +  codsoc-trait[nsoc]  + "/tiers."         + codsoc-trait[nsoc]  +  ".exp" 
                   fic-neo4 = "/applic/li/depart/neotic/"   +  codsoc-trait[nsoc]  + "/login."         + codsoc-trait[nsoc]  +  ".exp" 
                   fic-neo5 = "/applic/li/depart/neotic/"   +  codsoc-trait[nsoc]  + "/licence_login." + codsoc-trait[nsoc]  +  ".exp" .
            if not exploit then
                assign fic-neo3 = "/applic/li/dgr/tiers."         + codsoc-trait[nsoc]  + ".tst"
                       fic-neo4 = "/applic/li/dgr/login."         + codsoc-trait[nsoc]   + ".tst" 
                       fic-neo5 = "/applic/li/dgr/licence_login." + codsoc-trait[nsoc]   + ".tst" .
                                                                                      
            output stream str3  to value(fic-neo3) append.
            output stream str4  to value(fic-neo4) append.
            output stream str5 to value(fic-neo5) append.

        end.  /* if codsoc-soc = "01" .... fichiers specifiques au portail */ 
   
        /* relecture de la tables des tiers maj a extraire */
        for each t-aux
        where t-aux.codsoc =  codsoc-trait[nsoc]
        :
    
            find auxapr
            where auxapr.codsoc = t-aux.codsoc
            and   auxapr.typaux = "TIE"
            and   auxapr.codaux = t-aux.codaux
            no-lock no-error.
        
            find auxili
            where auxili.codsoc = auxili-soc
            and   auxili.typaux = "TIE"
            and   auxili.codaux = t-aux.codaux
            no-lock no-error.       

            /* LIENS EXPLOITATIONS <=> ANCIENS TC POUR DESACTIVATION DANS AGREO */    
    
            /* envoi des eventuels changements de TC, quel que soit le TC*/
            find auxdet
            where auxdet.codsoc = auxapr.codsoc
            and   auxdet.typaux = auxapr.typaux
            and   auxdet.codaux = auxapr.codaux
            and   auxdet.motcle = "WEB/TIERS"
            and   auxdet.codtab = ""
            and   trim ( auxdet.calf20) <> "" 
            /* no-lock  */ no-error.
            if available auxdet then do:
        
                do i-agreo2 = 1 to num-entries ( auxdet.calf20 ):
                    woldtc = entry ( i-agreo2 , auxdet.calf20 ).
                    if woldtc = "" then next .


                    if codsoc-trait[nsoc] = "01"  and lookup ( trim ( entry ( 1, woldtc,"|" ) ) , l-tcweb ) = 0  and lookup ( trim ( entry (1, woldtc,"|" ) )  , l-tcagreo) = 0  then next.
                    if codsoc-trait[nsoc] = "08"  and lookup ( trim ( entry ( 1, woldtc,"|" ) )  , l-tcagreo ) = 0 then next.

                    if lookup ( trim ( entry(1, woldtc,"|")),",998,999") = 0  then do:

                        zone = trim ( entry( 1, woldtc,"|"))                                + ";" 
                             + trim ( auxapr.codaux )                                       + ";"
                             + "01/01/2012"                                                 + ";"
                             + entry ( 2, woldtc,"|")                                       + ";"
                             + string ( today ,"99/99/9999")                                + ";"         
                             + "".
                        put stream str2 unformatted zone skip .
                        zone = "".
                        nbexptie = nbexptie + 1.
                    end.
                end.

                if exploit /* remise a blanc des anciens TC si trt exploitation */
                then do :
       
                    assign auxdet.calf20 = "".                                    
                    { majmoucb.i auxdet }
                end .    
            end. /* available auxdet */

            topagreo = no.

            /* envoi des 
                - exploitations 
                - liaisons TC  agreo et WEB <=> exploitations */
            if ( lookup ( auxapr.repres[1], l-tcagreo) <> 0 or
                 lookup ( auxapr.repres[2], l-tcagreo) <> 0 or
                 lookup ( auxapr.repres[3], l-tcagreo) <> 0 ) 
            or
               ( codsoc-trait[nsoc] = "01"  and
                 ( lookup ( auxapr.repres[1], l-tcweb) <> 0 or
                   lookup ( auxapr.repres[2], l-tcweb) <> 0 or
                   lookup ( auxapr.repres[3], l-tcweb) <> 0 ))       
            then do:
                topagreo = yes  . /* indique qu'on a d�ja extrait la fiche exploit */
                { ext-neo1.i }    /* fiche EXploitation */
            end.
    

            /* Donnes specif portail ADHERENT */ 
            /* pour un adherent on peut avoir plusieurs exploitations rattach�es */ 
            /* le code tiers 'principal' est dans auxapr.codaux-fiscal           */
            /* si codaux-fiscal = ""     : on est sur un tiers                   */
            /*                  = codaux : on est sur un tiers                   */ 
            /* si codaux <> codaux-fiscal : on est sur une exploitation          */ 
        
            if auxapr.codsoc = "01"  then do:
                /* message "C -- envoi exploit" auxapr.codsoc auxapr.typaux auxapr.codaux auxapr.codaux-fiscal t-aux.mdp t-aux.datdeb t-aux.datfin
                 view-as alert-box. */
           
                /* envoi de la fiche exploitation de l'exploit */                
                if t-aux.datdeb = ? and t-aux.datfin = ? then next.
                                
                if t-aux.datfin < date-debut then next.   /* on n'envoie pas les soldes ECA des exploitations non autoris�es au portail */
                
                if topagreo = no then do:  /* seulement si elle n'a pas d�j� et� redescendu dans la selection sur TC agreo // web */ 
                    { ext-neo1.i } /* fiche EXploitation */
                end.
     
                /* envoi de la liaison tie-exp */
                zone = trim( t-aux.codaux-m)                                   + ";"              /* code tiers externe */
                     + trim ( auxapr.codaux )                                  + ";"              /* code exploitation */
                     + string( t-aux.datdeb,"99/99/9999")                      + ";"              /* date de debut des droits */
                     + string( t-aux.datfin,"99/99/9999")                      + ";"              /* date de fin des droits */ 
                     + string( today ,"99/99/9999")                            + ";"         
                     + "" .
 
                put stream str2 unformatted zone skip.
                zone = "".
                nbexptie = nbexptie + 1.

                /* traitement specifique dela fiche tiers */
                if t-aux.codaux <> t-aux.codaux-m then next.  
                
                /* recuperation code tiers principal */     
                wtiers-m = t-aux.codaux-m.  
    
                if trim ( wtiers-m) = "" then  wtiers-m = t-aux.codaux.   /* lui-m�me */
                assign wmdp = t-aux.mdp.
                
                /* message "envoi fiche tiers" t-aux.codaux t-aux.codaux-m t-aux.mdp wmdp view-as alert-box. */
                
                /* recuperation des infos du tiers principal auxapr.codaux-fiscal */ 
                find b-auxili 
                where b-auxili.codsoc = auxili-soc
                and   b-auxili.typaux = "tie"
                and   b-auxili.codaux = wtiers-m
                no-lock no-error.
                if not available b-auxili then next .
                 
                find b-auxapr
                where b-auxapr.codsoc = auxapr.codsoc
                and   b-auxapr.typaux = "tie"
                and   b-auxapr.codaux = wtiers-m
                no-lock no-error.
                if not available b-auxapr then next .
                /*  fiche tiers */
                /* TIERS */
                zone = trim( b-auxapr.codaux )                                         + ";"              /* code tiers externe */
                     + "PORTAIL"                                                       + ";"   .          /* code type profil = PORTAIL par d�faut */  

                /* supression de la civilite */             

                wlib = b-auxili.adres[1] .
                if wlib begins "MR " or wlib begins "M. " then 
                    wlib = substr( wlib, 4 ).
                    
                if wlib begins "MME " or wlib begins "MLE " or wlib begins "MRS "  then 
                    wlib = substr( wlib, 5 ).                    
                wlib = replace(wlib, "  "," ").        
                
                assign zone = zone 
                            + trim ( string ( wlib , "x(32)" ))                            + ";"              /* nom+ pr�nom ensemble  */
                            + ""                                                          + ";"              /* Prenom non g�r� sur DEAL */ 
/*                            + " "                                                          + ";"              /* civilit� non g�r�e SUR DEAL */ */
                            + ""                                                           + ";"              /* civilit� non g�r�e SUR DEAL */
                            + trim ( b-auxili.adres[2])                                    + "\\n"            /* adresse  concaten�e  */ 
                            + trim ( b-auxili.adres[3])                                    + "\\n"
                            + trim ( b-auxili.adres[4])                                    + ";"
                            + substr ( b-auxili.adres[5],1,5)                              + ";"              /* CODE POSTAL */
                            + string ( int ( substr( b-auxapr.onic , 1 , 6) ), "99999")    + ";" .            /* CODE COMMUNE INSEE */     
  
                /* telephone */
                wlib = trim ( b-auxapr.teleph).
                wlib = replace (wlib," ","").
                assign zone = zone                                           
                            + wlib                                                + ";"  .

                /* portable */
                wlib = trim( b-auxapr.telex).
                wlib = replace (wlib," ","").    
                assign zone = zone                                           
                            + wlib                                                + ";"  .
                /* fax */
                wlib = trim( b-auxapr.fax).
                wlib = replace (wlib," ","").  
                assign zone = zone                                           
                            + wlib                                                + ";"  .
                /* mail */          
                wlib = trim ( substr ( b-auxapr.alpha-5, 1 , 50)).
                if index ( wlib, "@") = 0 then wlib = "".                         /* si pas d'@ : ce nest pas une adresse mail */
                /* si pas de mail + soc = 01, on force l'adresse, pour recuperer les preco terra conceil dans le portail ADH */
                if b-auxapr.codsoc = "01" and wlib = "" then wlib = "adherent@eureacoop-adh.fr".    
                        
                assign zone = zone                                           
                            + wlib                                                + ";"              /* mail */
                            /* 
							+ string( today ,"99/99/9999")                        + ";"          
                            + "31/12/2050" .  
							*/
  	                        + string( today ,"99/99/9999") + " " + string ( heure-debut, "hh:mm:ss")  + ";"              /* date mise a jour + heure */
                            + "31/12/2050 00:00:00" .

    
                put stream str3 unformatted zone skip .
                zone = "".
                nbtie = nbtie + 1. 
                
/*
                message "envoi fiche login " t-aux.codaux b-auxapr.codaux 
                                             t-aux.codaux-m t-aux.mdp wmdp view-as alert-box.
*/
               assign wlogin = trim ( b-auxapr.codaux ) 
                       wlicence = "PORT_ADH".
                
                if b-auxapr.type-adh = "6" then do:
                    assign wlicence = "PORT_SAL".
                    /*recuperation identifiant particulier renseigne sur la fiche tiers */
                    find first auxgss
                    where auxgss.codsoc = b-auxapr.codsoc
                    and   auxgss.typaux = b-auxapr.typaux
                    and   auxgss.codaux = b-auxapr.codaux
                    and   auxgss.theme = "IDENTNET"
                    no-lock no-error.
                    if available auxgss and trim (auxgss.alpha-cle) <> "" then 
                        assign wlogin = trim (auxgss.alpha-cle).
                end.                

                /* Login */            
                /* zone = trim( b-auxapr.codaux  )                                         + ";"               /* code login = code tiers externe       */  */
                zone = wlogin                                                           + ";"               /* code login = code tiers externe       */
                     + trim( t-aux.mdp )                                                + ";"               /* mot de passe             */ 
                     + trim( b-auxapr.codaux )                                          + ";"               /* code tiers externe       */
                     + string ( t-aux.datdeb ,"99/99/9999")                             + ";"               /* date de debut des droits */
                     + string ( t-aux.datfin ,"99/99/9999")                             + ";"               /* date de fin des droits   */ 
                     + string ( today ,"99/99/9999")                                    + ";"         
                     + "" .
         
                put stream str4 unformatted zone skip .
                zone = "".               
                nblog = nblog + 1. 
                

                /* envoi du lien login // licence */
                /* zone = trim ( b-auxapr.codaux )                                         + ";"              /* code login = code tiers externe       */ 
                     + "PORT_ADH"                                                       + ";"              /* code licence             */                  */
                     
                zone = wlogin                                                           + ";"              /* code login = code tiers externe       */ 
                     + wlicence                                                         + ";"              /* code licence             */                  
                     + string ( t-aux.datdeb ,"99/99/9999")                             + ";"              /* date de debut des droits */
                     + string ( t-aux.datfin ,"99/99/9999")                             + ";"              /* date de fin des droits   */ 
                     + string ( today    ,"99/99/9999")                                 + ";"         
                     + "" .
         
                put stream str5 unformatted zone skip .
                zone = "".               
                nblic = nblic + 1.
            end.
        end. /* for each t-aux */
    
        /* traitement specif pour 01 */ 
        if codsoc-trait[nsoc] = "01" then do: 
            /* selections des tc */
            for each auxdet
            where auxdet.codsoc = ""
            and   auxdet.motcle = "WEB/REP"
            and   auxdet.typaux = "REP"
            /* and   auxdet.datmaj >= date-debut */
            no-lock:
                find tabges
                where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "CLI"
                and   tabges.prefix = "REPRES"
                and   tabges.codtab = auxdet.codaux 
                no-lock no-error.
                if not available tabges then next.
                if auxdet.datmaj < date-debut and tabges.datmaj < date-debut then next.
                wlogin = trim ( auxdet.calf05 ) .
                if trim ( wlogin ) = "" then next.
                wmdp = auxdet.calf02.
                if wmdp = "" then do:
                    wmdp = "1234567890".
                    if exploit then do:    
                        wmdp = "" .
                        do while length ( wmdp ) < 10:
                            j = random (48,122).
                            if (j > 57 and j < 65 )  or (j > 90 and j < 97)  then next .
                            x = chr (j).
                            wmdp = wmdp + x.
                        end.
                        find b-auxdet
                        where b-auxdet.codsoc = auxdet.codsoc
                        and   b-auxdet.motcle = auxdet.motcle 
                        and   b-auxdet.typaux = auxdet.typaux
                        and   b-auxdet.codaux = auxdet.codaux
                        and   b-auxdet.codtab = auxdet.codtab 
                        no-error.
                        if available b-auxdet then do:
                            assign b-auxdet.calf02 = wmdp.
                            { majmoucb.i b-auxdet }
                        end.
                    end.
                end.
     
                wdatdebd = auxdet.datcre.
                wdatfind = 12/31/2050.
     
                /* REP desactiv� */  
                if auxdet.calf09 <> "O" then do:
                    wdatfind = today.
                end.
     
          
                /*  fiche tiers */
                /* TIERS */
                wlib = trim(tabges.libel1[1]).
                zone = trim( auxdet.codaux )                                            + ";"              /* code tiers externe */
                     + "PORTAIL"                                                        + ";"              /* code type profil = PORTAIL par d�faut */  
                     + trim ( string ( wlib , "x(32)" ))                                + ";"              /* nom+ pr�nom ensemble  */
                     + ""                                                               + ";"              /* Prenom non g�r� sur DEAL */ 
                     + ""                                                               + ";"              /* civilit� non g�r�e SUR DEAL */
                     + ""                                                               + ";"              /* adresse non geree pour les TC */
                     + ""                                                               + ";"              /* CP non geree pour les TC */
                     + ""                                                               + ";"              /* Code insee non geree pour les TC */
                     + ""                                                               + ";"              /* tel non geree pour les TC */
                     + ""                                                               + ";"              /* portable non geree pour les TC */
                     + ""                                                               + ";"              /* fax non geree pour les TC */
                     + trim ( tabges.libel2[8] )                                        + ";"              /* email  */
                     + string( today ,"99/99/9999") + " " + string ( heure-debut, "hh:mm:ss")  + ";"              /* date mise a jour + heure */
                     + "31/12/2050 00:00:00" .
    
                put stream str3 unformatted zone skip .
                zone = "".
                nbtie = nbtie + 1.

                /* Login */             
                zone = trim( wlogin )                                        + ";"              /* code login = code tiers externe       */
                     + trim( wmdp )                                          + ";"              /* mot de passe             */ 
                     + trim( auxdet.codaux )                                 + ";"              /* code tiers externe       */
                     + string( wdatdebd,"99/99/9999")                         + ";"             /* date de debut des droits */
                     + string( wdatfind,"99/99/9999")                         + ";"             /* date de fin des droits   */ 
                     + string(today ,"99/99/9999")                           + ";"         
                     + "" .
         
                put stream str4 unformatted zone skip.
                zone = "".               
                nblog = nblog + 1.  
     
                /* TC PORTAIL ADH */  
                /* envoi du lien login // licence */
                if codsoc-trait[nsoc] = "01" and ( auxdet.cdat04 <> ? or auxdet.calf04 = "O" ) then do:      
                    wdatfind = auxdet.cdat04.    
                    if wdatfind = ? and auxdet.calf04 = "O" then wdatfind = 12/31/2050.
                    zone = trim( wlogin )                                 + ";"              /* code login = code tiers externe       */
                         + "PORT_SAL"                                     +  ";"              /* code licence             */ 
                         + string ( wdatdebd ,"99/99/9999" )              + ";"              /* date de debut des droits */
                         + string ( wdatfind ,"99/99/9999" )              + ";"              /* date de fin des droits   */ 
                         + string ( today ,"99/99/9999" )                 + ";"         
                         + "" .
         
                    put stream str5 unformatted zone skip.
                    zone = "".               
                    nblic = nblic + 1 . 
                end.
            end.
        end.
     
        disp codsoc-trait[nsoc] format "xx"    column-label "Soc"
             nbexp              format ">>>9"  column-label "Exp" 
             nbexptie           format ">>>9"  column-label "Exp-tie" 
             nbtie              format ">>>9"  column-label "tie" 
             nblog              format ">>>9"  column-label "Log" 
             nblic              format ">>>9"  column-label "Lic" 
             with frame tiers title "TIERS " + string( today, "99/99/9999") + " " + string( time , "hh:mm:ss") .

        pause 2.
        
        /* fermeture des fichiers communs agreo et portail */
        output stream str1 close.
        output stream str2 close.
 
        /* fermeture des fichiers specif portail */
        if codsoc-trait[nsoc] = "01" then do: 
            output stream str3 close. 
            output stream str4 close. 
            output stream str5 close.
            
        end. 
        
 
    end. /*   if lookup ("TI", l-tables) <> 0 then do: */
    /* fin traitements des TIERS -- EXPLOIT.... */
    
    /* extraction des apports */ 
    if lookup ("AP", l-tables) <> 0 and codsoc-soc = "01"  then do:

        assign nbsil = 0 
               nbcar = 0
               nbpro = 0 
               nbapp = 0 .
        /* fichiers de sortie */ 
        assign fic-neo1 = "/applic/li/depart/neotic/"  +  codsoc-trait[nsoc]  + "/silos."               + codsoc-trait[nsoc]    +  ".exp" 
               fic-neo2 = "/applic/li/depart/neotic/"  +  codsoc-trait[nsoc]  + "/apports_carac_ana."   + codsoc-trait[nsoc]    +  ".exp" 
               fic-neo3 = "/applic/li/depart/neotic/"  +  codsoc-trait[nsoc]  + "/produits."            + codsoc-trait[nsoc]    +  ".exp" 
               fic-neo4 = "/applic/li/depart/neotic/"  +  codsoc-trait[nsoc]  + "/apports."             + codsoc-trait[nsoc]    +  ".exp" .

        if not exploit then
            assign fic-neo1 = "/applic/li/dgr/silos."               + codsoc-trait[nsoc]    +  ".tst" 
                   fic-neo2 = "/applic/li/dgr/apports_carac_ana."   + codsoc-trait[nsoc]    +  ".tst" 
                   fic-neo3 = "/applic/li/dgr/produits."            + codsoc-trait[nsoc]    +  ".tst" 
                   fic-neo4 = "/applic/li/dgr/apports."             + codsoc-trait[nsoc]    +  ".tst" .
                                                                                     
        output stream str1  to value(fic-neo1) append.  /* silos */
        output stream str2  to value(fic-neo2) append.  /* caracteristique */
        output stream str3  to value(fic-neo3) append.  /* produits */ 
        output stream str4  to value(fic-neo4) append.  /* apports */ 

        /* determination de la campagne APPORT en cours */      
        if month( today ) >= 7 then wcamp = string( year( today ), "9999" ) .
                               else wcamp = string( year( today ) - 1, "9999" ) .
        
        
        /* extraction des silos */
        FOR EACH MAGASI 
        where magasi.codsoc  = magasi-soc
        /* and   magasi.societe = codsoc-soc   pas de test sur la soc d'appartenance, on collecte pour EUREACOOP sur des silos 08 pas exemple.*/ 
        and lookup(  "SIL", magasi.type-mag ) <> 0
        no-lock :
            if exploit and  magasi.datmaj < date-debut then next.   /* si trt exploit, extraction uniquement des silos maj */ 
            
            zone = string ( magasi.codtab, "xxx" )                          + ";"  /* code externe du silo */
                 + trim ( string ( upper ( magasi.libel1[1] ),"x(50)" ) )   + ";"  /* libell� du silo */
                 + string ( magasi.datmaj, "99/99/9999" )                   + ";"  /* date de maj   */ 
                 + "31/12/2050".                                                   /* date de fin des droits.*/

            put stream str1 unformatted zone skip.
            zone = "".    
            nbsil = nbsil + 1.          
        end.               

        /* extraction des caract�ristiques */ 
        for each tabcer
        where tabcer.codsoc = ""
        and   tabcer.typtab = "CER"
        and   tabcer.prefix = "caract"
        no-lock :
            if exploit and  tabcer.datmaj < date-debut then next.   /* si trt exploit, extraction uniquement des caract maj */ 
            zone = trim ( tabcer.codtab )                                    + ";"  /* code caracteristique  */
                 + trim ( string ( upper ( tabcer.libel1[1] ) , "x(50)" ) )   + ";"  /* libell� caracteristique */
                 + ""                                                         + ";"  /* unit� (non renseign�)   */ 
                 + ""                                                         + ";"  /* val min (non renseign�)   */ 
                 + ""                                                  .      /* val max(non renseign�)   */ 
                 
            put stream str2 unformatted zone skip.
            zone = "".    
            nbcar = nbcar + 1.          

        end.

        /* extraction des c�r�ales */ 
        assign l-cerealKO = libelle[25].
        for each cereal
        where cereal.codsoc = codsoc-soc
        and   cereal.region = "001"
        and   cereal.campagne = wcamp
        and   lookup ( trim ( cereal.codcer ) , l-cerealKO ) = 0    /* on ne prend pas les cereales exclues */
        no-lock:
            if exploit and  cereal.datmaj < date-debut then next.   /* si trt exploit, extraction uniquement des cereales maj */ 
            do i-carac = 1 to 14:
                assign l-carac [i-carac] = cereal.cod-etat [i-carac]
                       l-libcar [i-carac] = "".
            end.
            do i-carac = 1 to 14:
                l-carac[i-carac] = trim( l-carac[i-carac] ).
                if l-carac[i-carac] <> "" then do :
                    find tabcer
                    where tabcer.codsoc = ""
                    and   tabcer.typtab = "CER"
                    and   tabcer.prefix = "caract"
                    and   tabcer.codtab = l-carac[i-carac] 
                    no-lock no-error.
                    if available tabcer then 
                        assign l-libcar[i-carac] = trim ( string ( upper ( tabcer.libel1[1] ) , "x(50)" ) ) .
                    else 
                        assign l-carac[i-carac] = ""
                               l-libcar[i-carac] = "".
                end.
            end.
            
            assign zone = trim ( cereal.codcer )                       + ";"  /* code produit */
                        + trim ( upper ( cereal.libach[1] ) )          + ";"  /* designation */
                        + ""                                           + ";"  /* campagne: laisser vide */
                        + trim ( cereal.codcer )                       + ";"  /* code produit */
                        + trim ( upper ( cereal.libach[1] ) )          + ";"  /* designation */
                        + "1"                                          + ";"  /* actif */
                        + "1"                                          + ";"  /* actif client */
                        + ""                                           + ";"  /* gestion esp/variete : laisser vide */
                        + "APPORT"                                     + ";"  /* code type analyse : APPORT  */
                        + "COL"                                        + ";"  /* CODE ESPECE COL par defaut */ 
                        .

            do i-carac = 1 to 14:
                assign zone = zone + l-carac[i-carac]                  + ";" /* code caracteristique 1 a 14 */
                .             
            end.
            do i-carac = 1 to 14:
                assign zone = zone + l-libcar[i-carac]                 + ";" /* libelle caracteristique 1 a 14 */
                .             
            end.
            put stream str3 unformatted zone skip.
            zone = "".      
            nbpro = nbpro + 1.          
        end.
        
        /* extraction des mouvements , liste chargee  a partir du fichier */ 
        l-mvtapp = libelle[12].
        for each cermvt 
        where cermvt.codsoc = codsoc-soc
        and   cermvt.campagne = wcamp
        and   lookup ( cermvt.codmvt , l-mvtapp ) <> 0              /* uniquement les mouvements param�tr�s dans fichier aid */
        and   lookup ( trim ( cermvt.codcer ) , l-cerealKO ) = 0    /* on ne prend pas les cereales exclues */
        no-lock,
             EACH MAGASI 
        where magasi.codsoc  = magasi-soc
        /* and   magasi.societe = codsoc-soc   pas de test sur la soc d'appartenance, on collecte pour EUREACOOP sur des silos 08 par exemple.*/ 
        and   lookup(  "SIL", magasi.type-mag ) <> 0
        and   magasi.codtab = cermvt.codmag
        no-lock ,        
            each auxapr
        where auxapr.codsoc = cermvt.codsoc
        and   auxapr.typaux = cermvt.typaux
        and   auxapr.codaux = cermvt.codaux
        no-lock:
            find t-aux
            where t-aux.codsoc = auxapr.codsoc
            and ( ( auxapr.codaux-fiscal <> "" and t-aux.codaux = auxapr.codaux-fiscal ) or 
                  ( auxapr.codaux-fiscal = "" and t-aux.codaux = auxapr.codaux ))  
            and   ( t-aux.datdeb <> ? or t-aux.datfin <> ? )
            no-lock no-error.
            if available t-aux then do:             
                /* DGR 28072015: on extrait les mouvements avec date-1 pour prendre en compte les mouvements de la veille qui ont pu �tre saisis apres l'extraction de 19h30 */
                if exploit and  cermvt.datmaj < date-debut - 1 and t-aux.topini <> yes  /* si trt exploit, extraction uniquement des mouvements maj sauf si init*/ 
                    then next .    

                if  t-aux.datfin < date-debut then next.   /* on n'envoie pas les mouvements des exploitations non autoris�es au portail */
            end.

            else do:
                /* verification que l'apporteur est bien autoris� au portail adh*/ 
                 /* recuperation des dates d'abonnement du tiers  */ 
                find auxgss
                where auxgss.codsoc = auxapr.codsoc
                and   auxgss.typaux = auxapr.typaux
                and   ( ( trim (auxapr.codaux-fiscal)  <> "" and  auxgss.codaux = auxapr.codaux-fiscal)  or
                          auxgss.codaux = auxapr.codaux ) 
                and   auxgss.theme = "EXTRANET"
                and   auxgss.chrono = 0
                no-lock no-error.
                if not available auxgss then next.               /* pas d'abonnement */ 
                else do:
                    if auxgss.alpha-cle = "O" then do:           /* si abonnement = oui: la date est une date de debut d'abonnement */
                        assign wdatdebd = auxgss.datte[1]
                               wdatfind = 12/31/2050.
                        if wdatdebd = ? then wdatdebd = today.               
                    end.
                    else do:                                     /* si abonnement = non: la date est une date de fin d'abonnement */
                        assign wdatdebd = 01/01/2013
                               wdatfind = auxgss.datte[1].
                        if wdatfind = ? then wdatfind = today.               
                    end.                    
                end.
                if wdatfind < date-debut then next.
                if exploit and   cermvt.datmaj < date-debut   /* si trt exploit, extraction uniquement des mouvements maj */ 
                then next .   

            end .     
            /* construction du code apport : codsoc + codmag + no-ticket + datsai + chrono  */
            assign wcodapp =    trim ( cermvt.codsoc )  +  "-" +  trim ( cermvt.codmag )                    .
            
            wlib = trim ( cermvt.no-ticket ) .   /* certains n� sont sur 8 => cadrage sur 10 */
            { cadra-g.i "wlib" "10" "0" }
            
            assign wcodapp = wcodapp + "-" + wlib + "-" + string ( cermvt.datsai , "99999999" ) + "-" 
                           + string ( cermvt.chrono , "999999" ) .
            
            /* calcul poids aux normes */

            FIND CEREAL 
            where cereal.codsoc    = cermvt.codsoc
            and   cereal.campagne  = cermvt.campagne
            and   cereal.region    = "001"
            and   cereal.codcer    = cermvt.codcer
            no-lock  no-error .
            if not available cereal  then  next .
                                                            

            pds-norm    = cermvt.pds-brut - cermvt.tare - (cermvt.pds-embal * cermvt.nb-embal).
            tx-humidite = cermvt.val-etat[mat-hum] .
            tx-impurete = cermvt.val-etat[mat-imp] .
            tx-poidspe  = cermvt.val-etat[3] .

            assign pdsmvt-recid = recid ( cermvt )
                   cereal-recid = recid ( cereal ) .

            do a = 5 to 14 :
               x-val-etat[a] = cermvt.val-etat[a] .
            end .
            run pds-norm.p.         
            /* fin du pav� calcul poids aux normes */ 
            
            
            /* DGR 07/11/13  recuperation du type de mouvement : 1 = entree, 2 = sortie */
            xx-pds-brut = cermvt.pds-brut.
            xx-pds-norm = pds-norm.
            
            find tabcer
            where tabcer.codsoc   = ""
            and   tabcer.etabli   = ""
            and   tabcer.typtab   = "CER"
            and   tabcer.prefix   = "CERMVT"
            and   tabcer.codtab   = cermvt.codmvt
            no-lock no-error.
            if available tabcer then do:
               if tabcer.nombre [21 ] = 2 then 
                   assign xx-pds-brut = - xx-pds-brut 
                          xx-pds-norm = - xx-pds-norm .
               
            end.

            
            assign zone = wcodapp                                                                      + ";" /* code apport = tous les champs de l'index unique "ticket" */
                        + string ( cermvt.datsai , "99/99/9999" )                                      + ";" /* Date de l'apport */
                        + trim ( string ( round ( xx-pds-brut , 3 ) , "->>>>9.999") )                  + ";" /* Poids Brut (en tonne) 12/09/13: 3 decimales au lieu de 2 */
                        + trim ( string ( round ( xx-pds-norm , 3 ) , "->>>>9.999") )                  + ";" /* Poids aux normes (en tonne) 12/09/13: 3 decimales au lieu de 2 */
                        + trim ( cermvt.codaux )                                                       + ";" /* code exploitation */
                        + trim ( cermvt.codmag )                                                       + ";" /* code silo  */
                        + trim ( cermvt.codcer )                                                       + ";" /* code cereale   */
                        + trim ( cermvt.campagne )                                                     + ";" /* campagne  */
                        + trim ( string ( round ( cermvt.prix , 5 ) , ">>>>9.99999") )                 + ";" /* Prix Unit  */
                        + trim ( string ( round ( cermvt.prix * xx-pds-norm , 5 ),"->>>>9.9999") )     + ";" /* Prix Total  */
                        + ""                                                                           + ";" /* Numero de contrat : laisser vide   */
                        + ""                                                                           + ";" /* Ligne contrat : laisser vide   */
                        + ""                                                                           + ";" /* Num�ro contrat AGREO : laisser vide   */
                        + "0"                                                                          + ";" /* Top supprim�  */
                        + "1"                                                                          + ";" /* Actif : mettre 1 par defaut */
                        + "1"                                                                          + ";" /* Valide : mettre 1 par defaut */
                        + "SEMB"                                                                       + ";" /* Code type pes�e : mettre SEMB par defaut */
                        + "CONT"                                                                       + ";" /* Code Carac Apport 1 : CONT */
                        + "TRANS"                                                                      + ";" /* Code Carac Apport 2 : TRANS */
                        + "MOUV"                                                                       + ";" /* Code Carac Apport 3  : MOUV */
                        + ""                                                                           + ";" /* Code Carac Apport 4  : non renseign�e */
                        + trim ( cermvt.contrat )                                                      + ";" /* Valeur Carac Apport 1 : CONTRAT */ 
                        .

            /* Recuperation du libelle du type de transport */                      
            wlib = "".
            find tabcer 
            where tabcer.codsoc = ""
            and   tabcer.etabli = ""
            and   tabcer.typtab = "CER"
            and   tabcer.prefix = "TRANSPORT"
            and   tabcer.codtab = cermvt.codtrp
            no-lock no-error.
            if available tabcer then 
                assign wlib = trim ( cermvt.codtrp ) + " - " + upper ( trim ( tabcer.libel1 [1] )). 
            
            assign zone = zone 
                        + trim ( wlib )                                                                + ";" /* Valeur Carac Apport 2 : TRANSPORT */                       
                        .
            
            /* Recuperation du libelle du code mouvement */                                       
            wlib = "".
            find tabcer 
            where tabcer.codsoc = ""
            and   tabcer.etabli = ""
            and   tabcer.typtab = "CER"
            and   tabcer.prefix = "CERMVT"
            and   tabcer.codtab = cermvt.codmvt
            no-lock no-error.
            if available tabcer then 
                assign wlib = trim ( cermvt.codmvt ) + " - " + upper ( trim ( tabcer.libel1 [1] )). 
            assign zone = zone 
                        + trim ( wlib )                                                                + ";" /* Valeur Carac Apport 3 : MOUVEMENT */                       
                        .
            assign zone = zone + ""                                                                    + ";". /* Valeur Carac Apport 4 : Non renseign� */                                                                                          
            do i-carac = 1 to 14: 
                wlib = "".
                if cermvt.val-etat [i-carac] <> ?  then do:
                    wlib  =  trim ( string ( round ( cermvt.val-etat [ i-carac ],2 ) , "->>>>>9.99") )  no-error .
                    if (error-status:error) or wlib = "0.00" then wlib = "".                                /* mettre ";;" au lieu de "0.00" si NR */
                    
                end.
                assign zone = zone 
                            + wlib                                                                    + ";" /* code caracteristique 1 a 14 */
                            .             
            end.
            put stream str4 unformatted zone skip.
            zone = "".               
            nbapp = nbapp + 1.
        end.

        disp codsoc-trait[nsoc] format "xx"    column-label "Soc"
             nbsil              format ">>>9"  column-label "Silo" 
             nbcar              format ">>>9"  column-label "Caract" 
             nbpro              format ">>>9"  column-label "Prod." 
             nbapp             format ">>>9"  column-label "App." 
             with frame apport title " APPORTS " + string( today, "99/99/9999") + " " + string( time , "hh:mm:ss") .

        assign nbsil = 0 nbcar = 0 nbpro = 0 nbapp = 0.
        pause 2 .

         /* fermeture des fichiers de sortie */
        output stream str1 close.       
        output stream str2 close.       
        output stream str3 close.       
        output stream str4 close.       
    end.  /* fin traitemnt des apports */

    /* EXTRACTION DES soldes ECA */ 
    if lookup ("EC", l-tables) <> 0 and codsoc-soc = "01"  then do:
        topfic = no.
        assign nbsolde = 0.
        /* fichiers de sortie */ 
        assign fic-neo1 = "/applic/li/depart/neotic/"  +  codsoc-trait[nsoc]  + "/[ECA]."                 + codsoc-trait[nsoc]    +  ".exp" .

        if not exploit then
            assign fic-neo1 = "/applic/li/dgr/[ECA]."    + codsoc-trait[nsoc]    +  ".tst" .
                                 
        if search (fic-neo1) = ? then 
            topfic = no.
            
        output stream str1  to value(fic-neo1) append.  /* soldes ECA */
        
        if topfic = no then                             /* le fichier des ECA n'est trait� qu'a partir de la 2�me ligne .....*/ 
           put stream str1 "CODE RESULTAT;DATE;CODE EXPLOIT;MOUVEMENT;CEREALE;SOLDE (T);CAMPAGNE" skip  .   /* LIGNE D'ENTETE */ 
        
        /* determination de la campagne APPORT en cours */      
        if month( today ) >= 7 then wcamp = string( year( today ), "9999" ) .
                               else wcamp = string( year( today ) - 1, "9999" ) .
        

        
        for each auxgss
        where auxgss.codsoc = codsoc-soc
        and   auxgss.theme = "REMERE"
        and   auxgss.typaux = "TIE" /* auxapr.typaux */
        use-index theme 
        no-lock , 
            each auxapr
        where auxapr.codsoc = auxgss.codsoc 
        and   auxapr.typaux = auxgss.typaux
        and   auxapr.codaux = auxgss.codaux
        no-lock:
            wtiers-m = "".
            find t-aux
            where t-aux.codsoc = auxapr.codsoc
            and ( ( auxapr.codaux-fiscal <> "" and t-aux.codaux = auxapr.codaux-fiscal ) or 
                  ( auxapr.codaux-fiscal = "" and t-aux.codaux = auxapr.codaux ))  
            and   ( t-aux.datdeb <> ? or t-aux.datfin <> ? )
            no-lock no-error.
            if available t-aux then do:

                wtiers-m = t-aux.codaux-m.
                
                if  t-aux.datfin < date-debut then next.   /* on n'envoie pas les mouvements des exploitations non autoris�es au portail */
            end.

            else do:
                /* verification que l'apporteur est bien autoris� au portail adh*/ 
                 /* recuperation des dates d'abonnement du tiers  */ 
                find b-auxgss
                where b-auxgss.codsoc = auxapr.codsoc
                and   b-auxgss.typaux = auxapr.typaux
                and   ( ( trim (auxapr.codaux-fiscal)  <> "" and  b-auxgss.codaux = auxapr.codaux-fiscal)  or
                          b-auxgss.codaux = auxapr.codaux ) 
                and   b-auxgss.theme = "EXTRANET"
                and   b-auxgss.chrono = 0
                no-lock no-error.
                if not available b-auxgss then next.               /* pas d'abonnement */ 
                else do:
                    if b-auxgss.alpha-cle = "O" then do:           /* si abonnement = oui: la date est une date de debut d'abonnement */
                        assign wdatdebd = b-auxgss.datte[1]
                               wdatfind = 12/31/2050.
                        if wdatdebd = ? then wdatdebd = today.               
                    end.
                    else do:                                     /* si abonnement = non: la date est une date de fin d'abonnement */
                        assign wdatdebd = 01/01/2013
                               wdatfind = b-auxgss.datte[1].
                        if wdatfind = ? then wdatfind = today.               
                    end.                    
                    wtiers-m = b-auxgss.codaux.
                end.
                if wdatfind < date-debut then next.
                
            end .     
            if wtiers-m <> auxgss.codaux then next.
        
            i-paramet = "O"                                         + "," + /* - 1- Chargement REMERE - "O" = Oui   */
                        "O"                                         + "," + /* - 2- Chargement SOULTE - "O" = Oui   */  
                        "O"                                         + "," + /* - 3- Chargement TAF - "O" = Oui      */  
                        "3"                                         + "," + /* - 4- Type Programme - "1" = Commande - "2" = Livraison - "3" = Consultation echanges  */
                        codsoc-soc                                  + "," + /* - 5- Societe Appros                                                                  */
                        codsoc-soc                                  + "," + /* - 6- Societe Apport                                                                  */ 
                        "*"                                         + "," + /* - 7- Societe regroupement Article Apport - Si "*" alors chgt ds procedure            */
                        "*"                                         + "," + /* - 8- Societe regroupement Article Appros - Si "*" alors chgt ds procedure            */
                        auxili-soc                                  + "," + /* - 9- Societe regroupement Tiers gestion - Si "*" alors chgt ds procedure             */
                        auxgss.typaux                               + "," + /* -10- Type Tiers                                                                      */
                        auxgss.codaux                               + "," + /* -11- Code Tiers                                                                      */
                        "*"                                         + "," + /* -12- Code Article Appros - Si "*" pas d'Article                                      */
                        "*"                                         + "," + /* -13- Date Cde Appros - "AAAAMMJJ" - Si "*" alors Today                               */
                        "*"                                         + "," + /* -14- Date Tarif Appros - "AAAAMMJJ" - Si "*" alors Today                             */
                        "*"                                         + "," + /* -15- No Contrat Apport - Si "*" alors tous                                           */
                        ""                                          .       /* -16- Rowid ligne de commande si type programme = "2"                                 */            
            
            run cal-echcer( input i-paramet ) .

            for each t-echcer
            no-lock:
                if t-echcer.typech <= 2 and t-echcer.qte-ent[1] = 0 and        /* si REMERE ou SOULTE et toutes les qte = 0 => pas de REMERE ni de SOULTE */
                   t-echcer.qte-ent[2] = 0 and t-echcer.qte-sor = 0 and 
                   t-echcer.qte-sol = 0  then next.  
                if t-echcer.typech = 3 and t-echcer.qte-ent[1] = 0 then next . /* si TAF pas d'entree => pas de TAF */
                
                /* code du resultat = CAMPAGNE  4 
                   (25 de Long max) + CODAUX    6 
                                    + typech    3
                                    + ARTICLE   6 max
                                    + CONTRAT   6 max
                */
                
                wlib = trim ( t-echcer.articl) .
                {cadra-d.i wlib 6 0}

                wlib2 = trim ( t-echcer.contrat) .
                {cadra-d.i wlib2 4 0}
                
                assign zone = wcamp                                       +    "-"     /* CODE DU RESULTAT */
                            + trim ( wtiers-m )                           +    "-"
                            + trim ( string ( t-echcer.typech, "999" ))   +    ""
                            + trim ( wlib )                               +    ""
                            + trim ( wlib2 )                              +    ";"  
                            + string ( today , "99/99/9999" )             +    ";"     /* date du solde */
                            + trim ( wtiers-m )                           +    ";"     /* code exploitation */.
                
                wlib = trim ( string ( t-echcer.typech, "999" ))    +    " "     /* type de mouvement */
                     + trim ( t-echcer.libech )                     +    " " .
                     
                /* si soulte acc�s au contrat pour recuperer le type de contrat */ 
                if t-echcer.typech = 2 then do:
                    find cerctr
                    where cerctr.codsoc = codsoc-soc
                    and   cerctr.type = "A"
                    and   cerctr.contrat = t-echcer.contrat
                    no-lock no-error.
                    if available cerctr then 
                       assign wlib = wlib + "CONTRAT: " + trim ( cerctr.contrat ) + " " + cerctr.typcon .
                end .               
                
                assign zone = zone 
                            + trim ( wlib )                               +    ";"  .

                /* si remere pas de cereale associee*/ 
                if t-echcer.typech = 1 then  /* remere */ 
                    wlib = "".
                else do:
                    if t-echcer.typech = 2 then /* Soulte */
                       wlib = trim ( t-echcer.articl) +  " "     
                            + trim ( t-echcer.libart ) .
                    else do:                                                        /* TAF */ 
                         if  t-echcer.articl = "3" then 
                             wlib = trim ( t-echcer.articl) +  " TRITICALE"   .      /* TRIT  dans la TABLE.... */
                         else 
                             wlib = trim ( t-echcer.articl) +  " "     
                                  + trim ( t-echcer.libart ) .
                    end.
                end.     

                assign zone = zone              
                            + trim ( wlib )                               +    ";"     /* C�r�ales */
                            . 
                assign wsolde = 0.
                if t-echcer.typech = 3 then do:
                   assign  wsolde = t-echcer.qte-ent [1]  / 1000 no-error.
                   if error-status:error then wsolde = 0.                  
                end.
                else do:
                   assign  wsolde = t-echcer.qte-sol / 1000 no-error.
                   if error-status:error then wsolde = 0.                  
                end.

                assign zone = zone                                             
                            + trim ( string( wsolde, "->>>>>>9.999" ) )  +    ";"       /* Solde */
                            + wcamp .                                                   /* Campagne */              
                
                put stream str1 unformatted zone skip.
                zone = "".               
                nbsolde = nbsolde  + 1.
            end.

        end.  /* for each */
        
        disp codsoc-trait[nsoc] format "xx"    column-label "Soc"
             nbsolde            format ">>>9"  column-label "Soldes ECA" 
             with frame eca title "SOLDES ECA " + string( today, "99/99/9999") + " " + string( time , "hh:mm:ss") .

             assign nbsolde = 0.
        pause 2 .

         /* fermeture des fichiers de sortie */
        output stream str1 close.       
    
    end.  /* fin traitemnt SOLDES ECA*/
    
    /* Capital Social */ 
    if lookup ("CS", l-tables) <> 0 and codsoc-soc = "01"  then do:
        topfic = no.
        assign nbcap = 0.
        /* fichiers de sortie */ 
        assign fic-neo1 = "/applic/li/depart/neotic/"  +  codsoc-trait[nsoc]  + "/[CAPSOC]."                 + codsoc-trait[nsoc]    +  ".exp" .

        if not exploit then
            assign fic-neo1 = "/applic/li/dgr/[CAPSOC]."    + codsoc-trait[nsoc]    +  ".tst" .
                                 
        topfic = yes.
        if search (fic-neo1) = ? then 
            topfic = no.
            
        output stream str1  to value(fic-neo1) append.  /* Capital*/
        if topfic = no then                             /* le fichier du capital  n'est trait� qu'a partir de la 2�me ligne .....*/ 
           put stream str1 "CODE RESULTAT;DATE;CODE EXPLOIT;MOUVEMENT;MONTANT;SOLDE" skip  .   /* LIGNE D'ENTETE */ 
              

        for each t-aux
        where t-aux.codsoc = codsoc-soc
        and   ( t-aux.datdeb <> ? or t-aux.datfin <> ? )        
        no-lock :
            if t-aux.codaux <> t-aux.codaux-m then next.  
            
            if t-aux.datfin < date-debut then next.   /* on n'envoie pas le capital des exploitations non autoris�es au portail */

            assign wsolde = 0.
            
            /* acces a la table de d�tail des operations en capital */ 
            for each capopep
            where capopep.codsoc = "01"
            and   capopep.codetb = "001"
            and   capopep.typaux = "tie"
            and   capopep.codaux = t-aux.codaux-m
            no-lock :
                
                case capopep.sens :
                   when "M" then 
                      assign wsolde = wsolde - capopep.cap-souscrit
                             wmt    = - capopep.cap-souscrit .
                   otherwise  
                      assign wsolde = wsolde + capopep.cap-souscrit
                             wmt    = capopep.cap-souscrit.
                end case . 
                
                /* code du resultat = cle de capopep : codsoc codetb typaux codaux chrono */ 
                assign zone = trim ( capopep.codsoc )                             /* code du resultat */
                            + trim ( capopep.codetb )                     + "-"
                            + trim ( capopep.codaux )                     + "-"
                            + string ( capopep.chrono , "9999")           + ";"
                            + string ( capopep.date-op , "99/99/9999")    + ";"        /* date du mouvement */
                            + trim ( t-aux.codaux )                   + ";"        /* code tiers */ 
                            .
                /* type de mouvement (libelle) pris dans l'operation si renseign� car plus explicite  */
                wlib = "".          
                if trim ( capopep.libelle ) <> "" then 
                    wlib =  upper ( trim ( capopep.libelle ) ) .
                else do:        /* sinon recup du libelle de mouvement en fonction du code mouvement */ 
                    find tables
                    where tables.codsoc = ""
                    and   tables.etabli = ""
                    and   tables.typtab = "CAP"
                    and   tables.prefix = "OPE-PART"
                    and   tables.codtab = capopep.code-particulier
                    no-lock no-error.
                    if available tables then assign wlib = trim( tables.libel1 [1] ).
                end.
                wlib = substr ( trim ( wlib ) , 1 , 50 ).
                assign zone = zone 
                            + trim ( wlib )                               +    ";"     /* type de mouvement */ 
                            + trim ( string ( wmt, "->>>>>9.99" ) )       +    ";"     /* montant du mouvement */
                            + trim ( string ( wsolde, "->>>>>9.99" ) )                 /* solde a date */
                            .
            
                put stream str1 unformatted zone skip.
                zone = "".               
                nbcap = nbcap  + 1.
            end.

        end.  /* for each */
        
        disp codsoc-trait[nsoc] format "xx"    column-label "Soc"
             nbcap              format ">>>9"  column-label "Solde Capital" 
             with frame capital title "CAPITAL " + string( today, "99/99/9999") + " " + string( time , "hh:mm:ss") .

             assign nbcap = 0.
        pause 2 .

         /* fermeture des fichiers de sortie */
        output stream str1 close.       
    
    end.  /* fin traitemnt des apports */
    

end.

/*---------------------*/
/* MAJ date extraction */
/*---------------------*/

if exploit
   then do :

    /* La date debut est egale a date stockee + 1 a chaque lancement */

    if heure-debut < 64800                   /* Deb. av. 18 h - Fin meme j  */
       then do :
       date-tabges.dattab[1] = today - 1.   /* Pour repartir meme jour     */
       { majmoucb.i date-tabges }
       leave.
    end.

    if heure-debut >= 64800 and time < 64800 /* Deb. ap. 18 h - Fin a j + 1 */
       then do :
       date-tabges.dattab[1] = today - 1.   /* Pour repartir de j + 1      */
       { majmoucb.i date-tabges }
       leave.
    end.

    if heure-debut >= 64800 and time < 86400 /* Deb. ap. 18 h - Fin meme j  */
       then do :
       date-tabges.dattab[1] = today.       /* Pour repartir de j + 1      */
       { majmoucb.i date-tabges }
       leave.
    end.

end .
run sel-del.p ( sel-applic, sel-filtre, sel-sequence, yes ) .

procedure cre-t-aux:
    find t-aux
    where t-aux.codsoc = auxapr.codsoc
    and   t-aux.codaux = auxapr.codaux
    no-error.
    if available t-aux then next.
    else do:
        create t-aux.
        assign t-aux.codsoc = auxapr.codsoc
               t-aux.codaux = auxapr.codaux.
                   
        if auxapr.codsoc = "01" then do:           /* recuperation du code tiers principal pour portail ADH */
            if auxapr.codaux-fiscal <> "" then 
                assign t-aux.codaux-m = auxapr.codaux-fiscal.
            else
                assign t-aux.codaux-m = auxapr.codaux.
                    
            /* recuperation des dates d'abonnement du tiers  */ 
            find auxgss
            where auxgss.codsoc = auxapr.codsoc
            and   auxgss.typaux = auxapr.typaux
            and   auxgss.codaux = t-aux.codaux-m
            and   auxgss.theme = "EXTRANET"
            and   auxgss.chrono = 0
            no-lock no-error.
            if not available auxgss then next.               /* pas d'abonnement */ 
            else do:
                if auxgss.alpha-cle = "O" then do:           /* si abonnement = oui: la date est une date de debut d'abonnement */
                    assign wdatdebd = auxgss.datte[1]
                           wdatfind = 12/31/2050.
                    if wdatdebd = ? then wdatdebd = today.               
                end.
                else do:                                     /* si abonnement = non: la date est une date de fin d'abonnement */
                    assign wdatdebd = 01/01/2013
                           wdatfind = auxgss.datte[1].
                    if wdatfind = ? then wdatfind = today.               
                end.
                assign t-aux.datdeb = wdatdebd
                       t-aux.datfin = wdatfind.
                
            end.
            /* if auxapr.type-adh = "6"  then next. */
            assign t-aux.mdp = "".
            find tabges
            where tabges.codsoc = auxapr.codsoc
            and   tabges.etabli = ""     
            and   tabges.typtab = "NEO"
            and   tabges.prefix = "MOTPASSE"
            and   tabges.codtab = t-aux.codaux-m
            no-error .
            if available tabges then 
                assign t-aux.mdp = tabges.libel1 [1]
                       t-aux.topini = no.                       
            else do:            
                /* recup et envoi du mot de passe */
                if not exploit then  do:
                    assign t-aux.topini = yes
                           t-aux.mdp = "1234567890".
                end.
                else do:    
                    wmdp = "" .

                    cre-mdp:
                    repeat:
                        do while length ( wmdp ) < 10:
                            j = random (48,122).
                            if (j > 57 and j < 65 )  or (j > 90 and j < 97)  then next .
                            x = chr (j).
                            wmdp = wmdp + x.
                        end.
                        find z-tabges
                        where z-tabges.codsoc = auxapr.codsoc
                        and   z-tabges.etabli = ""
                        and   z-tabges.typtab = "NEO"
                        and   z-tabges.prefix = "DGRMDP"
                        and   z-tabges.codtab = wmdp
                        no-error.
                        if available z-tabges then assign wmdp = "" .
                        if wmdp <> "" then do:
                            create z-tabges.
                            assign z-tabges.codsoc = auxapr.codsoc
                                   z-tabges.etabli = ""
                                   z-tabges.typtab = "NEO"
                                   z-tabges.prefix = "DGRMDP"
                                   z-tabges.codtab = wmdp .
                            leave cre-mdp .
                        end.        
                        else next   cre-mdp.                    
                    end.
                    
                    /* message "zzz- CREATION MOT DE PASSE" t-aux.codaux t-aux.codaux-m wmdp view-as alert-box. */                  
                    create tabges.
                    assign tabges.codsoc = auxapr.codsoc 
                           tabges.etabli = ""            
                           tabges.typtab = "NEO"         
                           tabges.prefix = "MOTPASSE"    
                           tabges.codtab = t-aux.codaux-m
                           tabges.libel1 [1] = wmdp.
            
                   { majmoucb.i tabges }      
                   
                    assign t-aux.mdp = tabges.libel1 [1]
                           t-aux.topini = yes.
                end.
            end.        
            /* message "creation t-aux" t-aux.codaux t-aux.codaux-m t-aux.mdp view-as alert-box.*/
        end.
    end .
end .
session:system-alert-boxes = no.
