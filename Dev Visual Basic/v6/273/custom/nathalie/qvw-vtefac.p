/****************************************************************************/
/*==========================================================================*/
/*                            Q V W - V E N T E S . P                       */
/* Extraction des ventes pour QVW                                           */
/*--------------------------------------------------------------------------*/
/* Extraction journali�re des donn�es entete et lignes VTE                  */
/*==========================================================================*/
/* 13032014 VLE : changement index sur lecture lignes <> LIC                */
/* 30102018 VLE : Ajout des CYC et CYT                                      */
/* 21112018 VLE : Integration de la societe 88                              */
/* 02122019 VLE : on recupere les ventes facturees des caisses de J-2 pour  */
/*                prendre celles integrees lors des clotures magasin        */ 
/*                on repositionne la date en fin de traitement              */
/*==========================================================================*/
def var nbentete-fac       as int extent 7.
def var nblignes-fac       as int extent 7.
def var nbentspe           as int extent 7.
def var i                  as int.
def var j                  as int.
def var k                  as int.
def var numbon-prec            as char init "".
def input parameter exploit as log.
def input parameter l-soc  as char.
def input parameter date-debut  as date.
def var l-fac as char init "CYT,LIM,LIV,LAS,LAV,LDP,RPV".
def var l-fac-GP as char init "CYC,LIC".

/*----------------------------------------*/
/* Definition des streams de sortie :     */
/*----------------------------------------*/
def stream file1.
def stream file2.
def stream file3.
if exploit then do :
        output stream file1 to value("/applic/li/depart/QVW-ENTETE-VENTES-fac.txt").
        output stream file2 to value("/applic/li/depart/QVW-LIGNES-VENTES-fac.txt").
        output stream file3 to value("/applic/li/depart/QVW-ENTSPE.txt") append.
end.
else do :
        output stream file1 to value("/applic/li/travail/QVW-ENTETE-VENTES-fac.txt").
        output stream file2 to value("/applic/li/travail/QVW-LIGNES-VENTES-fac.txt").
        output stream file3 to value("/applic/li/travail/QVW-ENTSPE.txt") append.
end.
date-debut = date-debut - 1.
session:system-alert-boxes = yes.
message "Debut programme" program-name( 1 ) .
Message " " .
Message "Debut extraction des donn�es entete et lignes FAC VTE pour QVW le "
        today "a" string ( time ,"hh:mm:ss" ). 
Message " " .
Message "Extraction des factures CYT,LIM,LIV,LAS,LAV,CYC,LIC,LSC" date-debut.
Message " " .

export stream file1 delimiter "~011"
"motcle" "codsoc" "etabli" "typcom" "numbon" "typaux" "codaux" "typaux-liv"
"codaux-liv" "typaux-fac" "codaux-fac" "codsoc-fin" "typaux-fin" "codaux-fin" 
"adres-liv 1" "adres-liv 2" "adres-liv 3" "adres-liv 4" "adres-liv 5" "adres 1"
"adres 2" "adres 3" "adres 4" "adres 5" "magasin" "magasin-2" "mag-respon" "transp"
"tourne" "taux-esco" "cout-trp" "mttrp" "typ-mttrp" "repres 1" "repres 2" "repres 3"
"modreg" "datbon" "datdep" "datliv" "devise" "txdev" "totht" "fac-ht"
"totpds" "type-tva" "numfac-fin" "numfac" "datfac" "nivcom" "clefac" "cleliv"
"code-texte" "opecre" "datcre" "heucre" "opemaj" "datmaj" "heumaj" "edition"
"datedi 1" "datedi 2" "datedi 3" "datedi 4" "datedi 5" "datedi 6" "datedi 7" "datedi 8"
"datedi 9" "datedi 10" "datedi 11" "datedi 12" "datedi 13" "datedi 14" "datedi 15"
"opedi 1" "opedi 2" "opedi 3" "opedi 4" "opedi 5" "opedi 6" "opedi 7" "opedi 8" 
"opedi 9" "opedi 10" "opedi 11" "opedi 12" "opedi 13" "opedi 14" "opedi 15" 
"cee-pays " "cee-ident" "lieu-depart" "cee-regime" "affaire" "cee-livrai" 
"service" "cee-departement" "piece-av" "topfact" "date-ediliv" "numbl" 
"env-plat" "statut" "rem-codrem 1" "rem-codrem 2" "rem-codrem 3" "rem-codrem 4"
"rem-codrem 5" "rem-codrem 6" "rem-codrem 7" "rem-codrem  8" "rem-codrem 9" 
"rem-codrem 10" "rem-typapp 1" "rem-typapp 2" "rem-typapp 3" "rem-typapp 4" 
"rem-typapp 5" "rem-typapp 6" "rem-typapp 7" "rem-typapp 8" "rem-typapp 9" 
"rem-typapp 10" "rem-typrem 1" "rem-typrem 2" "rem-typrem 3" "rem-typrem 4"
"rem-typrem 5" "rem-typrem 6" "rem-typrem 7" "rem-typrem 8" "rem-typrem 9" 
"rem-typrem 10" "rem-px 1" "rem-px 2" "rem-px 3" "rem-px 4" "rem-px 5" 
"rem-px 6" "rem-px 7" "rem-px 8" "rem-px 9" "rem-px 10" "rem-mont 1" 
"rem-mont 2" "rem-mont 3" "rem-mont 4" "rem-mont 5" "rem-mont 6" "rem-mont 7"
"rem-mont 8" "rem-mont 9" "rem-mont 10" "ttc" "nb-etiq" "colis" 
"prx-attente" "dattar" "codpri" "typact" "modliv" "datech"
"codech" "ref-tiers" "ref-magasin" "livdir" "domapr" 
"env-li 1" "env-li 2" "env-li 3" "env-li 4" "env-li 5"
"bon-valorise" "typbon" "rec-ase" "top-modif" "env-pdp" "typ-vente"
"pf-tva" "pf-cpta" "pf-statut" "mt-acompte" "mt-paye-brut" "mt-paye-esc" 
"mt-facture" "mt-paye-fact" "code-cms" "no-contrat" "mode-prepa" "edi-concde"
"regr-fact" "ref-marche" "magliv-ini" "cod-lettrage"
"lib-piefac" "rgp-fact" "zon-alfa-6" "zon-alfa-7" "zon-alfa-8" "zon-alfa-9" 
"mt-paye-esfac" "mont-coupon" "zon-dec-3" "zon-dec-4" "zon-dec-5" "zon-int-1"
"zon-int-2" "zon-int-3" "zon-int-4" "zon-int-5" "gest-reliq" . 

export stream file2 delimiter "~011"
"codsoc" "motcle" "typcom" "numbon" "chrono" "articl" "magasin" "typaux"
"codaux" "datbon" "datdep" "libart" "usto" "ucde" "ufac" "coef-cde" 
"coef-fac" "qte 1" "qte 2" "qte-fact" "qte-factot" "pu-brut" "pu-net" "top-ttc"
"ttva" "imput" "codpri" "txdev" "rem-codrem 1" "rem-codrem 2" "rem-codrem 3" 
"rem-codrem 4" "rem-codrem 5" "rem-codrem 6" "rem-codrem 7" "rem-codrem 8"
"rem-codrem 9" "rem-codrem 10" "rem-typapp 1" "rem-typapp 2"
"rem-typapp 3" "rem-typapp 4" "rem-typapp 5" "rem-typapp 6"
"rem-typapp 7" "rem-typapp 8" "rem-typapp 9" "rem-typapp 10"
"rem-typrem 1" "rem-typrem 2" "rem-typrem 3" "rem-typrem 4"
"rem-typrem 5" "rem-typrem 6" "rem-typrem 7" "rem-typrem 8"
"rem-typrem 9" "rem-typrem 10" "rem-px 1" "rem-px 2"
"rem-px 3" "rem-px 4" "rem-px 5" "rem-px 6" "rem-px 7" "rem-px 8" 
"rem-px 9" "rem-px 10" "rem-mont 1" "rem-mont 2" "rem-mont 3" "rem-mont 4"
"rem-mont 5" "rem-mont 6" "rem-mont 7" "rem-mont 8"
"rem-mont 9" "rem-mont 10" "htnet" "fac-ht" "fac-ttc" "top-prix" "fac-httot" "numfac"
"datfac" "datcpt" "partiel" "chrono-par" "codlig" "tenusto" "entsor 1" "entsor 2"
"mvtsto 1" "mvtsto 2" "qte-work" "ht-work" "avo-work" "datech" "magasin-2" "top-facture"
"top-facdir" "top-livre" "reliquat" "date-raptra" "numbon-reliq" "typcom-reliq" 
"chrono-reliq" "transp" "zone-trav" "pdsuni" "codlig-type" "atelier"
"ref-marche" "typbon" "code-cumul" "qte-tarif" "codech" "mt-transp" "mt-rfc" "top-rfc"
"datcre" "heucre" "opecre" "datmaj" "heumaj" "opemaj" "nivcom" "gencod"
"typaux-fac" "codaux-fac" .
 
/* extraction des ventes Agil. On les consid�re comme des factures comptant
   et on les extrait sur datcre et non datfac pour les r�cup�rer au jour le jour */
do i = 1 to num-entries( l-soc ) :
    do k = 1 to num-entries( l-fac-GP) :
    for each lignes
    where lignes.codsoc = entry(i, l-soc)
    and   lignes.motcle = "VTE"
    and   lignes.typcom = entry(k, l-fac-GP)
    and   lignes.nivcom <> "7"    
    and   lignes.datcre >= date-debut
    and   lignes.datmaj >= date-debut   
    use-index EUREA-1 no-lock :
                
     nblignes-fac[i] = nblignes-fac[i] + 1.
     export stream file2 delimiter "~011" lignes.
        
     if lignes.numbon <> numbon-prec
          then do :
          find entete 
          where entete.codsoc = lignes.codsoc
          and   entete.motcle = lignes.motcle
          and   entete.typcom = lignes.typcom
        and   entete.numbon = lignes.numbon
          no-lock no-error.
          if available entete then do : 
             nbentete-fac[i] = nbentete-fac[i] + 1.
             export stream file1 delimiter "~011" entete.
     
        /* recherche entspe correspondant pour r�cup�rer la carte de fid�lit� */
        find entspe
        where entspe.codsoc = entete.codsoc
        and   entspe.motcle = "VTE/CFI"
        and   entspe.typcom = entete.typcom
        and   entspe.numbon = entete.numbon
        and   entspe.alpha-1 <> ""
        no-lock no-error.
        if available entspe then do :
           nbentspe[i] = nbentspe[i] + 1.
           export stream file3 delimiter "~011" entspe.
        end.
     end.
     numbon-prec = entete.numbon.
     end.       
    end. /* lignes */
        end. /* l-fac-GP */
end. /* societe */   

/* extraction des autres factures, on les extrait sur le niveau et datmaj 
   et non datfac */

numbon-prec = "".

do i = 1 to num-entries( l-soc ) :

 do j = 1 to num-entries( l-fac ) :
    
        for each lignes
        where lignes.codsoc = entry(i, l-soc)
        and   lignes.motcle = "VTE"
        and   lignes.typcom = entry(j, l-fac)
        and   lignes.nivcom >= "6"        
        and   lignes.datmaj >= date-debut
        and   lignes.datmaj <> ?
        and   lignes.datfac <> ? 
          use-index EUREA-1 no-lock :
                
        nblignes-fac[i] = nblignes-fac[i] + 1.
          export stream file2 delimiter "~011" lignes.
        
          if lignes.numbon <> numbon-prec
              then do :
                  find entete 
                  where entete.codsoc = lignes.codsoc
                  and   entete.motcle = lignes.motcle
                  and   entete.typcom = lignes.typcom
                  and   entete.numbon = lignes.numbon
                  no-lock no-error.
                  if available entete then do : 
                        nbentete-fac[i] = nbentete-fac[i] + 1.
                        export stream file1 delimiter "~011" entete.
                
                /* recherche entspe correspondant pour r�cup�rer la carte de fid�lit� si renseign�e */
                        find entspe
                        where entspe.codsoc = entete.codsoc
                        and   entspe.motcle = "VTE/CFI"
                        and   entspe.typcom = entete.typcom
                        and   entspe.numbon = entete.numbon
                        and   entspe.alpha-1 <> ""
                        no-lock no-error.
                        if available entspe then do :
                                nbentspe[i] = nbentspe[i] + 1.
                                export stream file3 delimiter "~011" entspe.
                        end.        
                  end.         
                  numbon-prec = entete.numbon.
                end.
       
    end. /* lignes */
 end. /* type FAC */   
end. /* societe */   

output stream file1 close.
output stream file2 close.
output stream file3 close.

date-debut = date-debut + 1.
Message " " .
do i = 1 to num-entries( l-soc ) :
        Message "Nb entete-fac " entry(i, l-soc) " extraits                : " nbentete-fac[i].
        Message "Nb lignes-fac " entry(i, l-soc) " extraits                : " nblignes-fac[i].
        Message "Nb entspe     " entry(i, l-soc) " extraits                : " nbentspe[i].
end.

Message "Fin extraction des donn�es entete et lignes FAC pour QVW le " 
        today "a" string ( time , "hh:mm:ss" ). 
Message "Fin programme " program-name( 1 ) .
message " " .
session:system-alert-boxes = no.           