/*----------------------------------------------------------*/
/*                                             - NATHALIE - */
/*                                                          */
/*  Parametrage fournisseur DEMAT siret court               */
/*  _________________________________________               */
/*                                                          */
/* foudem-0.i                                               */
/*----------------------------------------------------------*/
 
Def {1} shared var libelle as char format "x(30)" extent 50 no-undo .
Def {1} shared var vtypaux as char format "xxx"             no-undo .
Def {1} shared var vcodaux as char format "x(10)"           no-undo .
Def {1} shared var lib-aux as char                          no-undo .
Def {1} shared var vsiret  as char format "x(15)"           no-undo .
 
Def {1} shared frame sai-fouart .
def {1} shared var auxili-soc    like auxili.codsoc.
 
Form skip( 1 )
     " " libelle[ 1 ] format "x(20)" vtypaux                 skip( 1 ) 
     " " libelle[ 2 ] format "x(20)" vcodaux auxili.adres[1] skip( 1 )
     " " libelle[ 3 ] format "x(20)" vsiret                  skip( 1 )
     with frame sai-fouart no-label row 8 centered  overlay {v6frame.i} .
