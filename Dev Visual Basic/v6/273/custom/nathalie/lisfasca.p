/* Prog : lisfasca.p extraction facture DEMAT */

{connect.i}

def stream file1.
def stream file2.

def var sel-sequence   as char              format "x(12)"            no-undo.
def var sequence       as char              format "x(5)"             no-undo.
def var sedifil1       as char              format "x(12)"            no-undo.
def var sedifil2       as char              format "x(12)"            no-undo.
def var wok    as char format "x".

def var wsoc    as char.
def var wsocdel as char.
def var aa    as int.
def buffer nex-fascan for bonens.
def buffer facbap-x   for facbap.
def var wope as char format "xxx" initial "xxx".
def var wsta as char format "x(19)" initial "0,1,2,3,4,5,7,9,S,T".
def var wnat as char format "x(7)" initial "C,F,I,P".
def var l-soc as char format "x(78)".
/*
initial "01,07,08,09,11,14,18,21,24,29,31,35,42,44,45,46,58,59,87,84".
*/
def var wmttc as char .
def var wrefus as char.
def var wbaptop as char. 
def var wbapope as char. 
def var wbapdat as date. 
def var wtoplet as char.
def var wspelib as char.
def var wlib    as char.
def var wdifope as char.
def var wopenom as char.
def var wopenom2 as char.
def var wnomfour as char.
def var auxili-soc as char.
def var wdataamm as char.
def var wtiers as char format "x(9)".
def var wcodech as char.

find tabges where
 tabges.codsoc = "" and
 tabges.etabli = ""   and
 tabges.typtab = "PRM" and
 tabges.prefix = "FASCAN"
 and tabges.codtab = "EXPORT"
no-lock no-error.
if available tabges then l-soc = libel1[2].


do with frame a: /* do saisie */

form
  "  " skip
    "Societes :" at 01                        
      skip
    l-soc at 01 no-label skip  
    "Statut 0 ou 1=Comptabilis�e ... :" at 01 wsta         no-label skip
    "       2 ou 3=Ana Saisie        :" skip
    "       4 ou 5=Valid�e,... ..... :" skip                           
    "       6 ou 7=R�gl�e .......... :" skip                           
    "            9=Refus�e ......... :" skip                           
    "       S ou T=Supprim�e ....... :"                            
    "Nature Frais,Confid,Immo,Produi :" at 01 wnat no-label skip 
      skip
    "Fichier Extraction ............ :" at 01 sedifil1     no-label
      skip
    "Fichier Anomalies ............. :" at 01 sedifil2     no-label
      skip
    "Validation .................... :" at 01 wok          no-label
      skip
      with title
     "lisfasca.p : Extraction factures DEMAT par statut" centered.

update l-soc.
display l-soc.

update wsta.
display wsta.

update wnat help "Nature C, F, I, P ou * pour tout".
display wnat.

/* determination No sequence operateur pour spoolname debut */
assign codsoc = " "
       etabli = " "
       typtab = "ope"
       prefix = "nume-spool"
       codtab = operat.
       run opeseq.p ("eud",output sequence, output sel-sequence).
/* determination No sequence operateur pour spoolname fin */

sedifil1 = "ext" + substr(sel-sequence, 4, 5) + ".csv".
display sedifil1.

/* determination No sequence operateur pour spoolname debut */
assign codsoc = " "
       etabli = " "
       typtab = "ope"
       prefix = "nume-spool"
       codtab = operat.
       run opeseq.p ("eud",output sequence, output sel-sequence).
/* determination No sequence operateur pour spoolname fin */

sedifil2 = "ano" + substr(sel-sequence, 4, 5) + ".csv".
display sedifil2.

update wok
    validate (wok = "O" or wok = "N" , "Valeurs autorisees : O ou N").

display wok.

if wok = "N" then leave.

end. /* repeat saisie*/

output stream file1 to value(sedifil1).
output stream file2 to value(sedifil2).

export stream file1 delimiter ";" 
       "Stat" "ADV" "Ope_BAP_Bonent" "Ope-Nom" "Refus�e" "Soc" "Nattrt"
       "C_Four" "Nom_Four" "Numdoc" "Docume"
       "Numfac" "Datfac" "DatEch" "Mt_ttc" 
       "Natano" "Datcre" "Heucre" "Opecre"
       "Openomcre" "AAMM_Datcre" 
       "Datmaj" "Opemaj" "Ope_Bap_bonens"
       "Ano" "Cod-Let"
       "Top-BAP" "Ope-BAP" "Dat-BAP" "Libel-Refus"
       "facbap-valide" "facbap-lib"
       "facbap-code"
       "Spe-lib-4"
       .

export stream file2 delimiter ";"
       "Lib_ano" "zone_id" "Soc" 
       "Baptop" "Bapope" "Bapdat" "Toplet" "Spelib"
       "Tiers" 
       "CODSOC"  
       "STATUT"  
       "ETABLI"  
       "DOCUME"  
       "CHRENT"  
       "DATCRE"  
       "HEUCRE"  
       "OPECRE"  
       "DATMAJ"  
       "HEUMAJ"  
       "OPEMAJ"   
       "VALIDE"  
       "CODE"    
       "LIBELLE" .

do aa = 1 to num-entries( l-soc ) :

wsoc = entry( aa, l-soc ) .

regs-app = "ELODIE" .
{ regs-cha.i }

regs-fileacc = "AUXILI" + "TIE".
{regs-rec.i}
auxili-soc = regs-soc .

/*

 if x-typtrt <> "R"  and  x-typtrt <> "V"  and  x-typtrt <> "A"  and  x-typtrt <> "N"  then  x-typtrt = "C" .

   /*  C */      assign  z-typtrt = ""   y-typtrt = "0"  ym-typtrt = "1"  i-typtrt = 1 .   /*  Comptabilis�  */
 case x-typtrt :
when  "A"  then  assign  z-typtrt = ""   y-typtrt = "2"  ym-typtrt = "3"  i-typtrt = 2 .   /*  Analytique saisie - Ecritur */
when  "V"  then  assign  z-typtrt = "3"  y-typtrt = "4"  ym-typtrt = "5"  i-typtrt = 3 .   /*  Validation - Bon � paye = O */
when  "R"  then  assign  z-typtrt = "5"  y-typtrt = "6"  ym-typtrt = "7"  i-typtrt = 4 .   /*  R�glement fait enregistr�   */
when  "N"  then  assign  z-typtrt = ""   y-typtrt = "9"  ym-typtrt = "3"  i-typtrt = 5 .   /*  Refus de signature          */
  end case .
  
*/

FOR EACH FACBAP where facbap.codsoc  = wsoc
                and   facbap.statut  <> "X"
                and   lookup(facbap.statut , wsta) <> 0  
                and   facbap.chrent  =  0
                no-lock :

if facbap.statut = "T" then wsocdel = "$$" + wsoc . 
                       else wsocdel = wsoc.

wbaptop = "?".
wbapope = "?".
wbapdat = ?.
wtoplet = "?".
wspelib = "?".
wtiers = "".
                                
FIND FASCAN where 
     fascan.codsoc = wsocdel and
     fascan.numdoc = entry( 1 , facbap.libelle , "|" )
     no-lock  no-error .
if not available fascan  then do :
        if lookup(facbap.statut, "2,3,4,5,6,7") <> 0 then do:
 
        find first mvtcpt where
             mvtcpt.codsoc = facbap.codsoc and
             mvtcpt.codetb = facbap.etabli and
             mvtcpt.docume = facbap.docume and
             mvtcpt.typaux = fascan.typaux
             no-lock no-error.
        if available mvtcpt then 
           do: 
           wbaptop = BAP-TOP.   
           wbapope = BAP-OPE.   
           wbapdat = BAP-DATE.  
           wtoplet = top-lettrage.
           wspelib = spe-lib-4.
           wtiers  = mvtcpt.typaux + trim (mvtcpt.codaux).
           end.    
        end.

        put stream file2 "Ano erreur lect fascan" ";" 
                         entry( 1 , facbap.libelle , "|" ) format "x(12)" ";"
                         wsocdel ";"
                         wbaptop ";"
                         wbapope ";"
                         wbapdat ";"
                         wtoplet ";"
                         wspelib format "x(12)" ";"
                         wtiers ";" 
                         .
        export stream file2 delimiter ";" facbap. 
        next .
end .

if lookup(fascan.nattrt , wnat) <> 0 or wnat = "*" then DO:

wtiers  = fascan.typaux + trim (fascan.codaux).

wcodech = "???".

FIND bonent where 
     bonent.codsoc = wsocdel and   
     bonent.motcle = substr(entry( 2 , facbap.libelle , "|" ), 1,3) and
     bonent.typcom = substr(entry( 2 , facbap.libelle , "|" ), 4,3) and
     bonent.numbon = substr(entry( 2 , facbap.libelle , "|" ), 7,10)
     no-lock  no-error .
if not available bonent  then do :
        if lookup(facbap.statut, "2,3,4,5,6,7") <> 0 then do:
 
        find first mvtcpt where
             mvtcpt.codsoc = facbap.codsoc and
             mvtcpt.codetb = facbap.etabli and
             mvtcpt.docume = facbap.docume and
             mvtcpt.typaux = fascan.typaux
             no-lock no-error.
        if available mvtcpt then 
           do: 
           wbaptop = BAP-TOP.   
           wbapope = BAP-OPE.   
           wbapdat = BAP-DATE.  
           wtoplet = top-lettrage.
           wspelib = spe-lib-4.
           end.    
        end.

        put stream file2 "Ano erreur lect bonent" ";" 
                         entry( 2 , facbap.libelle , "|" ) format "x(16)" ";"
                         wsocdel ";"
                         wbaptop ";"
                         wbapope ";"
                         wbapdat ";"
                         wtoplet ";"
                         wspelib format "x(12)" ";"
                         wtiers ";" 
                         .
        export stream file2 delimiter ";" facbap. 
        /*
        next .
        */
end .
else do: wcodech = bonent.codech. end.


FIND NEX-FASCAN where 
      nex-fascan.codsoc = wsocdel
  and nex-fascan.motcle = "SCN"
  and nex-fascan.typcom = ""
  and nex-fascan.numbon = fascan.numdoc
  and nex-fascan.theme  = "FASCAN-NEX"
  and nex-fascan.chrono = 0
no-lock  no-error .
if not available nex-fascan then do:  
   /*
   message "Inex BONENS" fascan.numdoc fascan.numfac . pause.
   */
   put stream file2 "Ano inex BONENS" ";" 
              entry( 2 , facbap.libelle , "|" ) format "x(16)" ";" 
                         wsocdel ";"
                         wbaptop ";"
                         wbapope ";"
                         wbapdat ";"
                         wtoplet ";"
                         wspelib format "x(12)" ";"
                         wtiers ";"
                         .
   export stream file2 delimiter ";" facbap. 
   next .
   end.
   
wmttc = string(fascan.MT-TTC * 100,  "->>>>>>9,99").

wrefus = "".
wlib = "".
wdifope = "".
find facbap-x where
     facbap-x.codsoc = facbap.codsoc and
     facbap-x.statut = "X" and   
     facbap-x.etabli = facbap.etabli and   
     facbap-x.DOCUME = facbap.DOCUME and
     facbap-x.CHRENT = facbap.CHRENT
     no-lock no-error. 
if available facbap-x then 
   do: wrefus = "Pi�ce refus�e".
       wlib   = facbap-x.libelle.
       if available bonent and bonent.codech = nex-fascan.alpha[6] 
          then wdifope = "***** " + bonent.codech + "/" + nex-fascan.alpha[6].
       end.   

wbaptop = "".
wbapope = "".
wbapdat = ?.
wtoplet = "".
wspelib = "".

if lookup(facbap.statut, "2,3,4,5,6,7") <> 0 then do:
 
find first mvtcpt where
     mvtcpt.codsoc = facbap.codsoc and
     mvtcpt.codetb = facbap.etabli and
     mvtcpt.docume = facbap.docume and
     mvtcpt.typaux = fascan.typaux
     no-lock no-error.
if available mvtcpt then 
   do: 
   wbaptop = BAP-TOP.   
   wbapope = BAP-OPE.   
   wbapdat = BAP-DATE.  
   wtoplet = top-lettrage.
   wspelib = spe-lib-4.
   end.    
   else do:
        put stream file2 "BIZARRE pas Trouve MVTCPT" ";"
              entry( 2 , facbap.libelle , "|" ) format "x(16)" ";" 
                         wsocdel ";"
                         wbaptop ";"
                         wbapope ";"
                         wbapdat ";"
                         wtoplet ";"
                         wspelib format "x(12)" ";"
                         wtiers ";" 
                         .
        export stream file2 delimiter ";" facbap.
        end.
end.

   
find tables where
     tables.codsoc = ""            and
     tables.etabli = ""            and
     tables.typtab = "BAP"         and
     tables.prefix = "CODE-BAP"    and
     tables.codtab = wcodech
     no-lock no-error.
if available tables then wopenom = wcodech + "-" + tables.libel1[1]. 
                    else wopenom = wcodech.

Find TABOOT where 
         tabOOT.codsoc = ""
     and taboot.etabli = ""
     and taboot.typtab = "OPE"
     and taboot.prefix = "OPERATEUR"
     and taboot.codtab = facbap.opecre
     no-lock no-error.
if available taboot then wopenom2 = facbap.opecre + "-" + taboot.libel1[1]. 
                    else wopenom2 = facbap.opecre.

find auxili where
     auxili.codsoc = auxili-soc    and
     auxili.typaux = fascan.typaux and
     auxili.codaux = fascan.codaux
     no-lock no-error. 
if available auxili then wnomfour = auxili.adres[1] .
                    else wnomfour = "? ? ?".

wdataamm = string(year(facbap.datcre), "9999") + string(month(facbap.datcre), "99").

export stream file1 delimiter ";" 
       facbap.statut
       nex-fascan.alpha[7] 
       wcodech
       wopenom
       wrefus
       wsoc 
       fascan.nattrt
       fascan.codaux
       wnomfour
       fascan.numdoc
       fascan.docume
       fascan.numfac
       fascan.datfac
       fascan.datech
       wmttc
       fascan.natano
       facbap.datcre
       facbap.heucre
       facbap.opecre
       wopenom2
       wdataamm
       facbap.datmaj
       facbap.opemaj
       nex-fascan.alpha[6]
       wdifope
       wtoplet
       wbaptop 
       wbapope 
       wbapdat  
       wlib
       facbap.valide
       facbap.libelle
       facbap.code
       wspelib
       .

end.   /* if lookup(fascan.nattrt , wnat) <> 0 or wnat = "*" */

end.  /* each facbap */


end.  /* do aa = 1 to num-entries( l-soc ) : */

