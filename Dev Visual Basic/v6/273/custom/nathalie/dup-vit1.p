/* ------------------------------------------------------------------------- */
/* duplication des creations / modifications articles / tarifs / gencods     */
/* de la societe 01 sur la societe 59                                        */
/* ------------------------------------------------------------------------- */
/* dup-vit1.p : programme de duplication                                     */
/* ------------------------------------------------------------------------- */
/* VLE 02022011 : suppression de la duplication de SOP TARECART              */
/* VLE 10032011 : duplication de tous les fournisseurs                       */
/* VLE 07062011 : Ajout info dans log.prov6                                  */
/* VLE 02012012 : Ne pas autoriser la modification de libell�                */
/* VLE 27092012 : Ne pas dupliquer les zones societes de artapr              */
/* VLE 27092012 : Ne pas dupliquer opemaj de {tarcli.i} VTE                      */
/* VLE 17032014 : Ajout de la famille 015 ds les familles a dupliquer        */
/* DGR 04082015 : batch perio                                                */
/* DGR 19112015 : si aticle GVSE, dup de la prov 01 -> 59                    */
/* DGR 21112016 : duplication PAN                                            */
/* CCO 08072019 : ajout {tarcli.i}                                           */
/*---------------------------------------------------------------------------*/
/*-------------------------------------------------- */
/*  Acces parametre lecture societe article ==> OK   */
/*-------------------------------------------------- */

{connect.i}
{ bat-ini.i  new }
{ sel.i      "new" }

DEFINE VARIABLE cco-datheur     AS CHARACTER   NO-UNDO.
DEFINE VARIABLE cco-fictrace    AS CHARACTER   NO-UNDO.
   
cco-datheur = STRING(YEAR(TODAY),"9999") + STRING(MONTH(TODAY),"99") + STRING(DAY(TODAY),"99") + "-" + STRING(TIME,"hh:mm:ss").
cco-datheur = REPLACE(cco-datheur, ":", "").
cco-fictrace = "/applic/li/travail/dup-vit1.log" + cco-datheur .
OUTPUT TO VALUE(cco-fictrace) .

def var periph           as char        format "x(15)"       no-undo.
def var wdate            as date        format "99/99/99"    no-undo.
def var nb-lus           as int                              no-undo.
def var nb-artaprlus     as int                              no-undo.
def var nb-artaprcre     as int                              no-undo.
def var nb-artaprmaj     as int                              no-undo.
def var nb-artbisvtecre  as int                              no-undo.
def var nb-artbisvtemaj  as int                              no-undo.
def var nb-artbisachcre  as int                              no-undo.
def var nb-artbisachmaj  as int                              no-undo.
def var nb-artbisacucre  as int                              no-undo.
def var nb-artbisacumaj  as int                              no-undo.
def var nb-artbisacecre  as int                              no-undo.
def var nb-artbisacemaj  as int                              no-undo.
def var nb-tabgesvtecre  as int                              no-undo.
def var nb-tabgesvtemaj  as int                              no-undo.
def var nb-tabgesachcre  as int                              no-undo.
def var nb-tabgesachmaj  as int                              no-undo.
def var nb-tarclivtecre  as int                              no-undo.
def var nb-tarclivtemaj  as int                              no-undo.
def var nb-tarcliachcre  as int                              no-undo.
def var nb-tarcliachmaj  as int                              no-undo.
def var nb-tarclirpdcre  as int                              no-undo.
def var nb-tarclirpdmaj  as int                              no-undo.
def var nb-articscre     as int                              no-undo.
def var nb-articsmaj     as int                              no-undo.

def var nb-codbarcre     as int                              no-undo.
def var nb-codbarmaj     as int                              no-undo.
def var nb-proglibvtecre as int                              no-undo.
def var nb-proglibvtemaj as int                              no-undo.
def var wdatmaj       like artapr.datmaj                  no-undo.
def var wheumaj       like artapr.heumaj                  no-undo.
def var wopemaj       like artapr.opemaj                  no-undo.
def var nb-opecre        as int                              no-undo.
def var nb-opemaj        as int                              no-undo.
def var heure-debut      as int .
def var wcodetar         as char                             no-undo.
def var l-fam as char init "001,002,003,004,005,006,007,008,009,010,015,022,080,081,082,083".

def buffer b-artapr for artapr.
def buffer x-artapr for artapr.

def buffer b-artbis for artbis.
def buffer b-tabges for tabges.
def buffer b-{tarcli.i} for {tarcli.i}.
def buffer b-codbar for codbar.
def buffer b-proglib for proglib.
def buffer date-tabges for tabges.
def buffer f-artapr for artapr.
def var type-trt   as char.

def buffer b-artics for artics.

def stream sortie .

def input parameter  fichier  as char format "x(12)".

run bat-lec.p ( fichier ) .

{ bat-cha.i  "type-trt"    "type-trt"   "a" }
{ bat-cha.i  "periph"      "periph"     "a" }
/* debut ajout pour batch periodique */
{ bat-cha.i "sel-applic"   "sel-applic"   "a" }
{ bat-cha.i "sel-filtre"   "sel-filtre"   "a" }
{ bat-cha.i "sel-sequence" "sel-sequence" "a" }

output stream sortie to value(periph).

/*-----------------------------------------------------*/
/* Recup. date traitement avec lock enreg. pendant trt */
/*-----------------------------------------------------*/
/*   { dup-vitdat.i "exclusive-lock" } */


{ dup-vitdat.i "no-lock" }

message "Debut programme" program-name( 1 ) " Duplication 01 ==> Vitry le " today "a" string ( time , "hh:mm:ss" ) .
message " " .
Message " DUPLICATION DONNEES MAJ DEPUIS LE  " wdate.
message " " .

heure-debut = time.
wdatmaj = today.
wheumaj = string(time,"hh:mm").
wopemaj = "VI1".

  /* tour ARTAPR */
Message "Extraction Artapr ".
for each artapr use-index date-maj
  where artapr.codsoc = "01"
  and   artapr.datmaj >= wdate
  no-lock:
  if lookup(artapr.famart, l-fam) = 0    /* on ne traite que les familles GP   */
    then next.
    nb-artaprlus = nb-artaprlus + 1.
    find b-artapr
    where b-artapr.codsoc = "59"
    and   b-artapr.articl = artapr.articl
    exclusive-lock no-error.
    if not available b-artapr then do:
      nb-artaprcre = nb-artaprcre + 1.
      {dup-vitc.i "artapr" }
/*      assign b-artapr.divers = "1"  .*/
    end.
    else do:
      nb-artaprmaj = nb-artaprmaj + 1.
      buffer-copy artapr
        except artapr.codsoc
               artapr.provenance
               artapr.code-blocage
               artapr.bloque
               artapr.code-texte
               artapr.datmaj
               artapr.heumaj
               artapr.opemaj
      to b-artapr
      assign b-artapr.codsoc = "59"
             b-artapr.datmaj = wdatmaj
             b-artapr.heumaj = wheumaj
             b-artapr.opemaj = wopemaj.
                                                                
/*      assign b-artapr.divers = "1".*/
    end. 

    
    /* ARTBIS VTE */
    find artbis
    where artbis.codsoc = artapr.codsoc
    and   artbis.motcle = "VTE"
    and   artbis.typaux = ""
    and   artbis.codaux = ""
    and   artbis.articl = artapr.articl
    no-lock no-error.
    if available artbis then do:
      find b-artbis
      where b-artbis.codsoc = "59"
      and   b-artbis.motcle = "VTE"
      and   b-artbis.typaux = ""
      and   b-artbis.codaux = ""
      and   b-artbis.articl = artapr.articl
      exclusive-lock no-error.
      if not available artbis then do :
        nb-artbisvtecre = nb-artbisvtecre + 1.
        {dup-vitc.i "artbis" }
      end.
      else do:
        nb-artbisvtemaj = nb-artbisvtemaj + 1.
        {dup-vitm.i "artbis" }
      end.
    end.
    find tabges
    where tabges.codsoc = artapr.codsoc
    and   tabges.etabli = ""
    and   tabges.typtab = "ART"
    and   tabges.prefix = "SUIT-FICHE"
    and   tabges.codtab = artapr.articl
    no-lock no-error.
    if available tabges then do:
      find b-tabges
      where b-tabges.codsoc = "59"
      and   b-tabges.etabli = ""
      and   b-tabges.typtab = "ART"
      and   b-tabges.prefix = "SUIT-FICHE"
      and   b-tabges.codtab = artapr.articl
      exclusive-lock no-error.
      if not available b-tabges then do:
        {dup-vitc.i "tabges" }
        nb-tabgesvtecre = nb-tabgesvtecre + 1.
      end.
      else do:
        {dup-vitm.i "tabges" }
        nb-tabgesvtemaj = nb-tabgesvtemaj + 1.
      end.
    end.

    find artics
    where artics.codsoc = artapr.codsoc
    and   artics.articl = artapr.articl
    and   artics.theme  = "PAN"
    and   artics.chrono = 0
    no-lock no-error.
    if available artics then do:
        find b-artics
        where b-artics.codsoc = "59"
        and   b-artics.articl = artapr.articl
        and   b-artics.theme    = "PAN"
        and   b-artics.chrono = 0
        exclusive-lock no-error.
        if not available b-artics then do:
            {dup-vitc.i "artics" }
            nb-articscre = nb-articscre + 1.
        end.
        else do:
            {dup-vitm.i "artics" }
            nb-articsmaj = nb-articsmaj + 1.
        end.
    
    
    end.
    
    /* fiche commentaire PROGLIB */
    find proglib
    where proglib.codsoc = artapr.codsoc
    and   proglib.motcle = ""
    and   proglib.etabli = ""
    and   proglib.edition = ""
    and   proglib.chrono = 0
    and   proglib.clef = "ARTI-COM" + artapr.articl
    no-lock no-error.
    if available proglib then do:
      find b-proglib
      where b-proglib.codsoc = "59"
      and   b-proglib.motcle = ""
      and   b-proglib.etabli = ""
      and   b-proglib.edition = ""
      and   b-proglib.chrono = 0
      and   b-proglib.clef = "ARTI-COM" + artapr.articl
      exclusive-lock no-error.
      if not available b-proglib then do :
        nb-proglibvtecre = nb-proglibvtecre + 1.
        {dup-vitc.i "proglib" }
      end.
      else do:
        nb-proglibvtemaj = nb-proglibvtemaj + 1.
        {dup-vitm.i "proglib" }
      end.
    end.

    /* fiche fournisseur LISADIS */
    find b-artbis
    where b-artbis.codsoc = "59"
    and   b-artbis.motcle = "ACH"
    and   b-artbis.typaux = "tie"
    and   b-artbis.codaux = "    802190"
    and   b-artbis.articl = artapr.articl
    exclusive-lock no-error.
    if not available b-artbis then do :
      find artbis
      where artbis.codsoc = "01"
      and   artbis.motcle = "ACH"
      and   artbis.typaux = "tie"
      and   artbis.codaux = "    802190"
      and   artbis.articl = artapr.articl
      no-lock no-error.
      if available artbis then do:
        nb-artbisachcre = nb-artbisachcre + 1.
        {dup-vitc.i "artbis" }

        /* DGR 19102015 : si articles GVSE on duplique la prov de 01 -> 59 */
        if trim ( artbis.articl-fou )  <> "" then do: 
            find x-artapr
            where x-artapr.codsoc = "59"
            and   x-artapr.articl = artapr.articl
            no-error.
            if available x-artapr then 
                assign  x-artapr.provenance = artapr.provenance
                        x-artapr.datmaj = wdatmaj
                        x-artapr.heumaj = wheumaj
                        x-artapr.opemaj = wopemaj.

        end.
                   
        find tabges
        where tabges.codsoc = artbis.codsoc
        and   tabges.etabli = "ACH"
        and   tabges.typtab = "ART"
        and   tabges.prefix = "SUIT-FICHE"
        and   tabges.codtab = artbis.typaux + artbis.codaux + artbis.articl
        no-lock no-error.
        if available tabges then do:
          find b-tabges
          where b-tabges.codsoc = "59"
          and   b-tabges.etabli = "ACH"
          and   b-tabges.typtab = "ART"
          and   b-tabges.prefix = "SUIT-FICHE"
          and   b-tabges.codtab = artbis.typaux + artbis.codaux + artbis.articl
          exclusive-lock no-error.
          if not available b-tabges then do:
            {dup-vitc.i "tabges" }
            nb-tabgesachcre = nb-tabgesachcre + 1.
          end.
        end.
      end.
    end.
    /* DGR 19102015 : si articles GVSE on duplique la prov de 01 -> 59 */
    else do:
        find artbis
        where artbis.codsoc = "01"
        and   artbis.motcle = "ACH"
        and   artbis.typaux = "tie"
        and   artbis.codaux = "    802190"
        and   artbis.articl = artapr.articl
        and trim ( artbis.articl-fou )  <> "" 
        no-lock no-error.
        if available artbis then do: 
            find x-artapr
            where x-artapr.codsoc = "59"
            and   x-artapr.articl = artapr.articl
            no-error.
            if available x-artapr then 
                assign  x-artapr.provenance = artapr.provenance
                        x-artapr.datmaj = wdatmaj
                        x-artapr.heumaj = wheumaj
                        x-artapr.opemaj = wopemaj.
 
        end.
    end .

    find b-artbis
    where b-artbis.codsoc = "59"
    and   b-artbis.motcle = "ACH"
    and   b-artbis.typaux = "tie"
    and   b-artbis.codaux = "    804500"
    and   b-artbis.articl = artapr.articl
    exclusive-lock no-error.
    if not available b-artbis then do:
      /* fiche fournisseur EURENA */
      find artbis
      where artbis.codsoc = "01"
      and   artbis.motcle = "ACH"
      and   artbis.typaux = "tie"
      and   artbis.codaux = "    804500"
      and   artbis.articl = artapr.articl
      no-lock no-error.
      if available artbis then do :
        nb-artbisacucre = nb-artbisacucre + 1.
        {dup-vitc.i "artbis" }
      end.
    end.

    /* fiche fournisseur ED */
    find b-artbis
    where b-artbis.codsoc = "59"
    and   b-artbis.motcle = "ACH"
    and   b-artbis.typaux = "tie"
    and   b-artbis.codaux = "    999100"
    and   b-artbis.articl = artapr.articl
    exclusive-lock no-error.
    if not available b-artbis then do:
      /* fiche fournisseur EURENA */
      find artbis
      where artbis.codsoc = "01"
      and   artbis.motcle = "ACH"
      and   artbis.typaux = "tie"
      and   artbis.codaux = "    999100"
      and   artbis.articl = artapr.articl
      no-lock no-error.
      if available artbis then do :
        nb-artbisacecre = nb-artbisacecre + 1.
        {dup-vitc.i "artbis" }
      end.
    end.
  end.   /* boucle artapr   */

  /* fiche fournisseur   */
  Message "Extraction fiche fournisseur ".
for each artbis
  where artbis.codsoc = "01"
  and   artbis.motcle = "ACH"
  and   artbis.typaux = "tie"
  and   artbis.datmaj >= wdate
  use-index date-maj no-lock :
  
    find artapr where artapr.codsoc = "00"
     and artapr.articl = artbis.articl
    no-lock no-error.
    if not available artapr or lookup(artapr.famart, l-fam) = 0    /* on ne traite que les familles GP   */
        then next.
    
    find b-artbis
    where b-artbis.codsoc = "59"
    and   b-artbis.motcle = "ACH"
    and   b-artbis.typaux = "tie"
    and   b-artbis.codaux = artbis.codaux
    and   b-artbis.articl = artbis.articl
    exclusive-lock no-error.
    if not available b-artbis then do :
      nb-artbisachcre = nb-artbisachcre + 1.
      {dup-vitc.i "artbis" }
    end.
    else do:
      nb-artbisachmaj = nb-artbisachmaj + 1.
      {dup-vitm.i "artbis" }
    end.

    find tabges
    where tabges.codsoc = artbis.codsoc
    and   tabges.etabli = "ACH"
    and   tabges.typtab = "ART"
    and   tabges.prefix = "SUIT-FICHE"
    and   tabges.codtab = artbis.typaux + artbis.codaux + artbis.articl
    no-lock no-error.
    if available tabges then do:
      find b-tabges
      where b-tabges.codsoc = "59"
      and   b-tabges.etabli = "ACH"
      and   b-tabges.typtab = "ART"
      and   b-tabges.prefix = "SUIT-FICHE"
      and   b-tabges.codtab = artbis.typaux + artbis.codaux + artbis.articl
      exclusive-lock no-error.
      if not available b-tabges then do:
        {dup-vitc.i "tabges" }
        nb-tabgesachcre = nb-tabgesachcre + 1.
      end.
      else do:
        {dup-vitm.i "tabges" }
        nb-tabgesachmaj = nb-tabgesachmaj + 1.
      end.
    end.
end.

Message "Extraction codbar ".
for each codbar use-index datmaj
  where codbar.codsoc = "01"
  and   codbar.datmaj >= wdate
  no-lock :
  /* on ne traite que les articles GP */
    find f-artapr 
    where f-artapr.codsoc = "00"
      and f-artapr.articl = codbar.articl
    no-lock no-error.  
    if not available f-artapr or lookup(f-artapr.famart, l-fam) = 0
        then next.
        
    find b-codbar
    where b-codbar.codsoc = "59"
    and   b-codbar.code-barre = codbar.code-barre
    exclusive-lock no-error.
    if not available codbar then do:
      {dup-vitc.i "codbar" }
      nb-codbarcre = nb-codbarcre + 1.
    end.
    else do:
      {dup-vitm.i "codbar" }
      nb-codbarmaj = nb-codbarmaj + 1.
    end.
end.

Message "Extraction tarifs ventes ".
for each {tarcli.i} use-index k-datmaj
  where {tarcli.i}.codsoc = "01"
  and   {tarcli.i}.motcle = "VTE"
  and   {tarcli.i}.datmaj >= wdate
  and   {tarcli.i}.typaux = ""
  and   {tarcli.i}.codaux = ""
  and   {tarcli.i}.opecre <> "DUP"
  no-lock:
  /* on ne traite que les articles GP */
    find f-artapr 
    where f-artapr.codsoc = "00"
      and f-artapr.articl = {tarcli.i}.articl
    no-lock no-error.  
    if not available f-artapr or lookup(f-artapr.famart, l-fam) = 0
        then next.
        
    wcodetar = {tarcli.i}.code-tarif.
    if wcodetar = "HT5" then wcodetar = "HTC".

    find b-{tarcli.i}
    where b-{tarcli.i}.codsoc = "59"
    and   b-{tarcli.i}.motcle = "VTE"
    and   b-{tarcli.i}.typaux = ""
    and   b-{tarcli.i}.codaux = ""
    and   b-{tarcli.i}.opecre <> "DUP"
    and   b-{tarcli.i}.articl = {tarcli.i}.articl
    and   b-{tarcli.i}.code-tarif = wcodetar
    and   b-{tarcli.i}.date-debut = {tarcli.i}.date-debut
/*
    and   b-{tarcli.i}.chrono = {tarcli.i}.chrono
*/
    exclusive-lock no-error.
    if not available b-{tarcli.i} then do:
      create b-{tarcli.i}.
      buffer-copy {tarcli.i}
        except {tarcli.i}.codsoc {tarcli.i}.code-tarif
               {tarcli.i}.datcre   {tarcli.i}.heucre  {tarcli.i}.opecre
               {tarcli.i}.datmaj  {tarcli.i}.heumaj  /* {tarcli.i}.opemaj */
      to b-{tarcli.i}
        assign b-{tarcli.i}.codsoc = "59"
               b-{tarcli.i}.code-tarif = wcodetar
               b-{tarcli.i}.datcre = wdatmaj  b-{tarcli.i}.heucre = wheumaj
               b-{tarcli.i}.opecre = wopemaj 
               b-{tarcli.i}.datmaj = wdatmaj  b-{tarcli.i}.heumaj = wheumaj
              /*  b-{tarcli.i}.opemaj = wopemaj*/ .
      release b-{tarcli.i}.
      nb-tarclivtecre = nb-tarclivtecre + 1.

    end.
    else do:
      if {tarcli.i}.code-tarif = "HTC" then next.
      assign b-{tarcli.i}.codsoc = "59"
             b-{tarcli.i}.code-tarif = wcodetar
             b-{tarcli.i}.datmaj = wdatmaj  b-{tarcli.i}.heumaj = wheumaj
             b-{tarcli.i}.opemaj = {tarcli.i}.opemaj /* wopemaj */ .
      release b-{tarcli.i}.

      nb-tarclivtemaj = nb-tarclivtemaj + 1.
    end.
end.

Message "Extraction tarifs achats ".
for each {tarcli.i} use-index k-datmaj
  where {tarcli.i}.codsoc = "01"
  and   {tarcli.i}.motcle = "ACH"
  and   {tarcli.i}.datmaj >= wdate
  and   {tarcli.i}.typaux = "TIE"
/*  and   {tarcli.i}.codaux = "    802190" */
  no-lock:
  /* on ne traite que les articles GP */
    find f-artapr 
    where f-artapr.codsoc = "00"
      and f-artapr.articl = {tarcli.i}.articl
    no-lock no-error.  
    if not available f-artapr or lookup(f-artapr.famart, l-fam) = 0
        then next.
        
    find b-{tarcli.i}
    where b-{tarcli.i}.codsoc = "59"
    and   b-{tarcli.i}.motcle = "ACH"
    and   b-{tarcli.i}.typaux = {tarcli.i}.typaux
    and   b-{tarcli.i}.codaux = {tarcli.i}.codaux
    and   b-{tarcli.i}.articl = {tarcli.i}.articl
    and   b-{tarcli.i}.code-tarif = {tarcli.i}.code-tarif
    and   b-{tarcli.i}.date-debut = {tarcli.i}.date-debut
    and   b-{tarcli.i}.chrono = {tarcli.i}.chrono
    exclusive-lock no-error.
    if not available b-{tarcli.i} then do:
      {dup-vitc.i "{tarcli.i}" }
      nb-tarcliachcre = nb-tarcliachcre + 1.
    end.
    else do:
      {dup-vitm.i "{tarcli.i}" }
      nb-tarcliachmaj = nb-tarcliachmaj + 1.
    end.
end.

/*-----------------------------------------------------------*/
/*    Duplication de {tarcli.i} RPD                              */
/*-----------------------------------------------------------*/
Message "Extraction RPD".
for each {tarcli.i} use-index k-datmaj
  where {tarcli.i}.codsoc = "01"
  and   {tarcli.i}.motcle = "VTE"
  and   {tarcli.i}.datmaj >= wdate
  and   {tarcli.i}.typaux = "TAX"
  and   {tarcli.i}.codaux = ""
  and   {tarcli.i}.code-tarif = "RPD"
  no-lock:
  /* on ne traite que les articles GP */
    find f-artapr 
    where f-artapr.codsoc = "00"
      and f-artapr.articl = {tarcli.i}.articl
    no-lock no-error.  
    if not available f-artapr or lookup(f-artapr.famart, l-fam) = 0
        then next.
        
    find b-{tarcli.i}
    where b-{tarcli.i}.codsoc = "59"
    and   b-{tarcli.i}.motcle = {tarcli.i}.motcle
    and   b-{tarcli.i}.typaux = {tarcli.i}.typaux
    and   b-{tarcli.i}.codaux = {tarcli.i}.codaux
    and   b-{tarcli.i}.articl = {tarcli.i}.articl
    and   b-{tarcli.i}.code-tarif = {tarcli.i}.code-tarif
    and   b-{tarcli.i}.date-debut = {tarcli.i}.date-debut
    and   b-{tarcli.i}.chrono = {tarcli.i}.chrono
    exclusive-lock no-error.
    if not available b-{tarcli.i} then do:
      {dup-vitc.i "{tarcli.i}" }
      nb-tarclirpdcre = nb-tarclirpdcre + 1.
    end.
    else do:
      {dup-vitm.i "{tarcli.i}" }
      nb-tarclirpdmaj = nb-tarclirpdmaj + 1.
    end.
end.

/*   VLE 02022011 
for each tabges use-index date-maj
  where tabges.codsoc = "01"
  and   tabges.etabli = ""
  and   tabges.typtab = "SOP"
  and   tabges.prefix = "TARECART"
  and   tabges.datmaj >= today
  no-lock:
    find b-tabges
    where b-tabges.codsoc = "59"
    and   b-tabges.etabli = ""
    and   b-tabges.typtab = "SOP"
    and   b-tabges.prefix = "TARECART"
    and   b-tabges.codtab = tabges.codtab
    exclusive-lock no-error.
    if not available b-tabges then do:
      {dup-vitc.i "tabges" }
      nb-opecre = nb-opecre + 1.
    end.
    else do:
      {dup-vitm.i "tabges" }
      nb-opemaj = nb-opemaj + 1.
    end.
end.
*/
/*---------------------*/
/* MAJ date extraction */
/*---------------------*/

{ dup-vitdat.i "exclusive-lock" }

/* La date debut est egale a date stockee + 1 a chaque lancement */
                                                                                        
if heure-debut < 64800                   /* Deb. av. 18 h - Fin meme j  */
   then do :
   date-tabges.dattab[1] = today - 1.   /* Pour repartir meme jour     */
   { majmoucb.i date-tabges }
end.
else                                    
   if heure-debut >= 64800 and time < 64800 /* Deb. ap. 18 h - Fin a j + 1 */
      then do :
      date-tabges.dattab[1] = today - 1.   /* Pour repartir de j + 1      */
      { majmoucb.i date-tabges }
   end.
   else 
   if heure-debut >= 64800 and time < 86400 /* Deb. ap. 18 h - Fin meme j  */
      then do :
      date-tabges.dattab[1] = today.       /* Pour repartir de j + 1      */
      { majmoucb.i date-tabges }
   end.

  disp "                       Nb CRE     Nb MAJ" no-label skip
       "                       ------     ------" no-label skip
       "ARTAPR ........ : " nb-artaprcre    no-label nb-artaprmaj    no-label skip
       "CODBAR ........ : " nb-codbarcre    no-label nb-codbarmaj    no-label skip
       "ARTBIS VTE .... : " nb-artbisvtecre no-label nb-artbisvtemaj no-label skip
       "TABGES VTE .... : " nb-tabgesvtecre no-label nb-tabgesvtemaj no-label skip
       "TARCLI VTE .... : " nb-tarclivtecre no-label nb-tarclivtemaj no-label skip
       "ARTBIS ACH lisa : " nb-artbisachcre no-label nb-artbisachmaj no-label skip
       "TABGES ACH .... : " nb-tabgesachcre no-label nb-tabgesachmaj no-label skip
       "TARCLI ACH .... : " nb-tarcliachcre no-label nb-tarcliachmaj no-label skip
       "TARCLI RPD .... : " nb-tarclirpdcre no-label nb-tarclirpdmaj no-label skip
       "ARTBIS ACH usi. : " nb-artbisacucre no-label nb-artbisacumaj no-label skip
       "ARTBIS ACH ED.. : " nb-artbisacecre no-label nb-artbisacemaj no-label skip
       "OPERATION       : " nb-opecre       no-label nb-opemaj       no-label skip
       "PROGLIB         : " nb-proglibvtecre no-label nb-proglibvtemaj no-label skip
       "ARTICS PAN .....: " nb-articscre  no-label nb-articsmaj no-label skip
.
Message "Fin duplication 01 ==> Vitry le " today "a" string ( time , "hh:mm:ss" ).
message " " .

/*
pause.
*/

RUN sel-del.p(sel-applic, sel-filtre, sel-sequence, YES).
leave. 

