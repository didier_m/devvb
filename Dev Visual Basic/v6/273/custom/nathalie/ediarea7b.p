/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! Deal/nathalie/tty/ediarea7b.p                                                               !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !tabges                                          !
!ediarea0.i            !                                     !AUXGSS                                          !
!majsto-0.i            !NEW                                  !artbis                                          !
!majmoucb.i            !artapr                               !CODBAR                                          !
!majmoucb.i            !artbis                               !artapr                                          !
!majmoucb.i            !auxgss                               !                                                !
!majmoucb.i            !codbar                               !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
!_____________________________________________________________________________________________________________!
!                                                  P R O C E D U R E S                                        !
!_____________________________________________________________________________________________________________!
!lecture-fichier       !                                                                                      !
!traitement            !                                                                                      !
!enregistrer-log       !                                                                                      !
&End
**************************************************************************************************************/

/******************************************************************************/
/* ediarea7b.p - GESTION DES ECHANGES DE DONNEES INFORMATISEES                 */
/*----------------------------------------------------------------------------*/
/* EDI EUREA - AREA                                                           */
/* EDI avec la centrale d'achat AREA : Int�gration des articles AREA          */
/* Param�tre pass� depuis le lanceur ediarea-1.p : type-trt = "ART-AREA"      */
/******************************************************************************/
/******************************************************************************/
/* NB : Merci de conserver les parties de code comment�es                     */ 
/*      Le mouchard est un outil destin� au d�veloppeur....                   */
/******************************************************************************/
{ connect.i      }
{ ediarea0.i     }
{ majsto-0.i NEW }

DEFINE VARIABLE ligne          AS CHARACTER   NO-UNDO.
DEFINE VARIABLE ano-ligne      AS CHARACTER   NO-UNDO.
DEFINE VARIABLE typaux-deal    AS CHARACTER   NO-UNDO.
DEFINE VARIABLE codaux-deal    AS CHARACTER   NO-UNDO.
DEFINE VARIABLE typaux-area    AS CHARACTER   NO-UNDO.
DEFINE VARIABLE codaux-area    AS CHARACTER   NO-UNDO.
DEFINE VARIABLE x-coredi       AS CHARACTER   NO-UNDO.
DEFINE VARIABLE x-codsoc       AS CHARACTER   NO-UNDO.
DEFINE VARIABLE x-motcle       AS CHARACTER   NO-UNDO.
DEFINE VARIABLE x-motcle2      AS CHARACTER   NO-UNDO.
DEFINE VARIABLE articl-deal    AS CHARACTER   NO-UNDO.
DEFINE VARIABLE x-fichier      AS CHARACTER   NO-UNDO.

DEFINE VARIABLE cpt-lig          AS INTEGER     NO-UNDO. /* Total lignes          */
DEFINE VARIABLE cpt-trt          AS INTEGER     NO-UNDO. /* Total lignes trait�es */
DEFINE VARIABLE cpt-trtfouinc    AS INTEGER     NO-UNDO. /* Total lignes fournisseurs inconnus */
DEFINE VARIABLE cpt-trtfouine    AS INTEGER     NO-UNDO. /* Total lignes fournisseurs inexistant */
DEFINE VARIABLE cpt-artdeal1     AS INTEGER     NO-UNDO. /* Total Articles DEAL trouv�s par ARTBIS 811000 */
DEFINE VARIABLE cpt-artdeal2     AS INTEGER     NO-UNDO. /* Total Articles DEAL trouv�s par ARTBIS 811xxx */
DEFINE VARIABLE cpt-artdeal1-i   AS INTEGER     NO-UNDO. /* Total Articles DEAL non trouv�s par ARTBIS 811000 */
DEFINE VARIABLE cpt-artdeal2-i   AS INTEGER     NO-UNDO. /* Total Articles DEAL non trouv�s par ARTBIS 811xxx */
DEFINE VARIABLE cpt-rejets       AS INTEGER     NO-UNDO. /* Total Articles DEAL non trouv�s par ARTBIS 811xxx */

DEFINE VARIABLE ccRepSource     AS CHARACTER    NO-UNDO.
DEFINE VARIABLE ccLigne         AS CHARACTER    NO-UNDO.
DEFINE VARIABLE ccNomFic        AS CHARACTER    NO-UNDO.
DEFINE VARIABLE ccFicTmp        AS CHARACTER    NO-UNDO.
DEFINE VARIABLE ccDateHeure     AS CHARACTER    NO-UNDO.

DEFINE VARIABLE z1              AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z15             AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z56             AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z70             AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z84             AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z98             AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z102            AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z113            AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z124            AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z135            AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z149            AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z151            AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z162            AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z177            AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z192            AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z196            AS CHARACTER    NO-UNDO.
DEFINE VARIABLE z198            AS CHARACTER    NO-UNDO.

DEFINE VARIABLE toperr          AS integer      NO-UNDO.
DEFINE VARIABLE toperra         AS character    NO-UNDO.

def var recartbisxxx            as recid        no-undo .
def var recartbis               as recid        no-undo .
def var ii                      as int          no-undo . 
def var aa                      as int          no-undo . 
def var ablo                    as char         no-undo .
def var nblo                    as char         no-undo .

def var wdate   as date.
def var wheu    as char.
def var wope    as char.
def var wcomm   as char.
def var optmaj  as log initial no.
def var code-barre as char.
def var gencod-ok as logical.

def temp-table t-tempo-1
    field t1coredi  as char
    field t1codaux  as char
    field t1nom     as char
    field t1gencod  as char
    index i-tempo-1 is unique primary t1coredi.

/* Table contenant les noms des fichiers � traiter */
DEFINE TEMP-TABLE ttFichier                     NO-UNDO
    FIELD fichier               AS CHARACTER
    FIELD nomfic                AS CHARACTER
    FIELD nom2                  AS CHARACTER.

/* Table contenant les rejets et le log du traitement */
DEFINE TEMP-TABLE ttRejet
    FIELD cpt           AS INTEGER
    FIELD code-enr      AS CHARACTER
    FIELD commentaire   AS CHARACTER
    FIELD ligne         AS CHARACTER.

DEFINE BUFFER bf1-ttRejet FOR ttRejet.
DEFINE BUFFER b-artbis    FOR artbis. 

DEFINE STREAM strEcriture.
DEFINE STREAM integ-fic.

IF repert-rejet = "" THEN repert-rejet = SESSION:TEMP-DIRECTORY .

OUTPUT TO ediarea7b.mouc .
MESSAGE "Debut programme " .
OUTPUT CLOSE .

/*--------------------*/
/* Valeurs par d�faut */
/*--------------------*/
ASSIGN  x-codsoc      = "45"
        x-motcle      = "ACH" 
        x-motcle2     = "VTE"
        typaux-area   = "TIE" 
        codaux-area   = "    811000" 
        x-fichier     = "produit.txt"   /* ##utile ? */
        ccNomFic      = "produit".

assign wdate = today
       wheu = string(time,"hh:mm")
       wope = "Bcc".

wcomm = "Maj Auto Produit par AREA le " + string(today,"99/99/9999")
        + " a " + wheu + " Par " + wope.
        
/*************************************************/
/* Recherche des param�trages pour le traitement */
/*************************************************/
/* Lecture du parametre EDI / ediarea7b / PARAMETRES */
FIND FIRST tabges
    WHERE  tabges.codsoc = codsoc-soc
      AND  tabges.etabli = ""
      AND  tabges.typtab = "EDI"
      AND  tabges.prefix = "ediarea7b"
      AND  tabges.codtab = "PARAMETRES"
    NO-LOCK NO-ERROR.
IF NOT AVAILABLE tabges THEN
    FIND FIRST tabges
        WHERE  tabges.codsoc = ""
          AND  tabges.etabli = ""
          AND  tabges.typtab = "EDI"
          AND  tabges.prefix = "ediarea7b"
          AND  tabges.codtab = "PARAMETRES"
        NO-LOCK NO-ERROR.

IF NOT AVAILABLE tabges THEN DO:
    /* Message d'erreur et fin du traitement. */
    ano-ligne = "Param�trage EDI / ediarea7b / PARAMETRES non trouv�. Traitement annul�" .

    RUN enregistrer-log(INPUT "REJ", INPUT cpt-lig, INPUT ligne, INPUT ano-ligne).

RETURN.
END.

ASSIGN  x-codsoc    = tabges.libel1[1]  /* soci�t� � traiter        */
        x-motcle    = tabges.libel1[2]  /* mot cl� achat            */
        x-motcle2   = tabges.libel1[3]  /* mot cl� vente            */
        typaux-area = tabges.libel1[4]  /* Typaux area              */
        codaux-area = tabges.libel1[5]. /* Codaux area              */

IF TRIM(tabges.libel1[6]) <> "" THEN
    ASSIGN  x-fichier = tabges.libel1[6]            /* Nom du fichier � integrer (produit.txt) */
            ccNomFic  = ENTRY(1, x-fichier, ".").   /* Nom du fichier sans extension (produit) */

/* Initialisation du r�pertoire � lire: celui des rejets ou celui d'arriv�e du fichier EDI) */
IF trt-rejet
    THEN ccRepSource = repert-rejet.
    ELSE ccRepSource = repert-arr.

/* Lecture des fichiers � traiter */
INPUT FROM OS-DIR(ccRepSource).

REPEAT:
    /* Retourne: "Nom du fichier" "Chemin + Nom du fichier" "Type d'objet: D = Directory, F = File" */
    /* Exemple:  produit.txt c:\arrivee\produit.txt F                                               */
    IMPORT UNFORMATTED ccLigne.
    /* Suppression des " dans la ligne */
    ccLigne = REPLACE(ccLigne, '"', "").
    /* Traitement des fichiers uniquement */
    IF TRIM(ENTRY(3, ccLigne, " ")) <> "F" THEN NEXT.
    /* Le type du fichier (=d�but du nom) doit encer par "PRODUIT" (ce nom est charg� par le param�trage) */
    IF SUBSTRING(ENTRY(1, ccLigne, " "), 1, LENGTH(ccNomFic)) <> ccNomFic THEN NEXT.
    /* Copier le fichier dans le r�pertoire de travail */
    OS-COPY VALUE(ENTRY(2, ccLigne, " ")) VALUE(repert-tra + ENTRY(1, ccLigne, " ")).
    /* Supprimer le fichier du r�pertoire de rejet */

    OS-DELETE VALUE(ENTRY(2, ccLigne, " ")).

    /* Enregistrement des noms de fichiers � traiter */
    CREATE  ttFichier.
    ASSIGN  ttFichier.fichier = repert-tra + ENTRY(1, ccLigne, " ")      /* Nom complet du fichier (chemin + nom du fichier) */
            ttfichier.nomfic  = ENTRY(1, ccLigne, " ")                  /* Nom du fichier (sans le chemin)                  */
            ttfichier.nom2    = ENTRY(1, ENTRY(1, ccLigne, " "), ".").  /* Nom du fichier (sans l'extension)                */
END.

INPUT CLOSE.

/* Si il n'y a pas de fichier � traiter ecriture du fichier log/mouchard/bilan... */
FIND FIRST ttFichier NO-LOCK NO-ERROR.
IF NOT AVAILABLE ttFichier THEN DO:
    /* Date et heure du traitement */
    ccDateHeure = STRING(YEAR(TODAY),"9999") + STRING(MONTH(TODAY),"99") + STRING(DAY(TODAY),"99") + "-" + STRING(TIME,"hh:mm:ss").
    ccDateHeure = REPLACE(ccDateHeure, ":", "").

    /* Nom du fichier log */
    ccFicTmp = repert-tra + ccNomFic + ".log" + ccDateHeure.

    OUTPUT STREAM strEcriture TO VALUE(ccFicTmp).
    PUT STREAM    strEcriture UNFORMATTED "Aucun fichier � traiter dans le repertoire " ccRepSource SKIP.
    OUTPUT STREAM strEcriture CLOSE.
    RETURN.
END.

/* Recherche des fournisseurs AREA */

FOR EACH auxapr where 
   auxapr.codsoc =  x-codsoc and
   auxapr.typaux =  typaux-area and
   auxapr.codaux >  codaux-area and
   auxapr.codaux-fac = codaux-area 
no-lock:
FIND auxgss where 
       auxgss.codsoc = auxapr.codsoc 
   and auxgss.typaux = auxapr.typaux 
   and auxgss.codaux = auxapr.codaux 
   and   auxgss.theme  = "COREDI" 
   and   auxgss.chrono = 0 
use-index primaire  no-lock  no-error . 
if not available auxgss  then next.
   
x-coredi = auxgss.alpha-cle . 

FIND T-tempo-1 where 
           t-tempo-1.t1coredi    = x-coredi
     exclusive-lock no-error .
if not available t-tempo-1 then do:
     CREATE T-tempo-1 .
     assign t-tempo-1.t1coredi    = x-coredi       
            t-tempo-1.t1codaux    = auxapr.codaux  
            t-tempo-1.t1nom       = auxapr.adres[1] 
            t-tempo-1.t1gencod    = auxapr.gencod.   
     end.       
END.
OUTPUT TO ediarea7b.mouc APPEND .
Message "export table four".
for each T-tempo-1  no-lock:
   export t-tempo-1.
END.
OUTPUT CLOSE .

/* Traitement du ou des fichiers */
FOR EACH ttFichier NO-LOCK:

    /* Les tables de travail sont purg�es */
    EMPTY TEMP-TABLE ttRejet.

    /* Controle d'existence du fichier */
    IF SEARCH (ttFichier.fichier) = ? THEN NEXT.

    /* Message d'information */
    RUN enregistrer-log(INPUT "LOG", INPUT 0, INPUT "", INPUT "Traitement du fichier: " + ccRepSource + ttfichier.nomfic).

    /* Ouverture du fichier en lecture */
    INPUT STREAM integ-fic FROM VALUE(ttFichier.fichier).
    /* Lecture du fichier � integrer et traitement des lignes */
    RUN lecture-fichier.
    /* Fermeture de la stream de lecture */
    INPUT STREAM integ-fic CLOSE.

    /* Messages d'information */
    RUN enregistrer-log(INPUT "LOG", INPUT 0, INPUT "", INPUT "Option de mise � jour                             : " + STRING(optmaj)).
    RUN enregistrer-log(INPUT "LOG", INPUT 0, INPUT "", INPUT "Nombre de lignes lues                             : " + STRING(cpt-lig)).
    RUN enregistrer-log(INPUT "LOG", INPUT 0, INPUT "", INPUT "Nombre de lignes trait�es                         : " + STRING(cpt-trt)).
    RUN enregistrer-log(INPUT "LOG", INPUT 0, INPUT "", INPUT "Nombre de lignes fournisseurs inconnus            : " + STRING(cpt-trtfouinc)).
    RUN enregistrer-log(INPUT "LOG", INPUT 0, INPUT "", INPUT "Nombre de lignes fournisseurs inexistants         : " + STRING(cpt-trtfouine)).
    RUN enregistrer-log(INPUT "LOG", INPUT 0, INPUT "", INPUT "Nombre de lignes articl-deal OK lect1 Artb 811000 : " + STRING(cpt-artdeal1)).
    RUN enregistrer-log(INPUT "LOG", INPUT 0, INPUT "", INPUT "Nbre lignes articl-deal NON OK  lect1 Artb 811000 : " + STRING(cpt-artdeal1-i)).
    RUN enregistrer-log(INPUT "LOG", INPUT 0, INPUT "", INPUT "Nombre de lignes articl-deal OK lect2 Artb 811xxx : " + STRING(cpt-artdeal2)).
    RUN enregistrer-log(INPUT "LOG", INPUT 0, INPUT "", INPUT "Nbre lignes articl-deal NON OK  lect2 Artb 811xxx : " + STRING(cpt-artdeal2-i)).
    RUN enregistrer-log(INPUT "LOG", INPUT 0, INPUT "", INPUT "Nbre lignes en rejets                             : " + STRING(cpt-rejets)).

    /* Date et heure du traitement */
    ccDateHeure = STRING(YEAR(TODAY),"9999") + STRING(MONTH(TODAY),"99") + STRING(DAY(TODAY),"99") + "-" + STRING(TIME,"hh:mm:ss").
    ccDateHeure = REPLACE(ccDateHeure, ":", "").

    /* Copier le fichier source dans le repertoire "Archives" */
    ccFicTmp = repert-arrtrt + ttFichier.nom2 + ".trt" + ccDateHeure.
    OS-COPY VALUE(ttFichier.fichier) VALUE(ccFicTmp).
    /* Supprimer le fichier du repertoire de travail */
    OS-DELETE VALUE(ttFichier.fichier).

    /* Si des rejets existent, cr�ation du fichier rejet */
    FIND FIRST ttRejet WHERE ttRejet.code-enr = "REJ" NO-LOCK NO-ERROR.
    IF AVAILABLE ttRejet THEN DO:
        /* Nom du fichier rejet */
        ccFicTmp = repert-rejet + ttFichier.nom2 + ".rej" + ccDateHeure.

        OUTPUT STREAM strEcriture TO VALUE(ccFicTmp).

        PUT STREAM strEcriture UNFORMATTED "/* Traitement fichier : " + ttFichier.fichier SKIP(2).

        FOR EACH  bf1-ttRejet
            WHERE bf1-ttRejet.code-enr = "REJ"
            NO-LOCK:

            PUT STREAM strEcriture UNFORMATTED "/* Enreg n� : " STRING(bf1-ttRejet.cpt) SKIP.
            PUT STREAM strEcriture UNFORMATTED "/* " bf1-ttRejet.commentaire SKIP.
            PUT STREAM strEcriture UNFORMATTED bf1-ttRejet.ligne SKIP.
        END.

        OUTPUT STREAM strEcriture CLOSE.
    END.

    /* Ecriture du fichier log/mouchard/bilan... */
    FIND FIRST ttRejet WHERE ttRejet.code-enr = "LOG" NO-LOCK NO-ERROR.
    IF AVAILABLE ttRejet THEN DO:
        /* Nom du fichier log */
        ccFicTmp = repert-tra + ttFichier.nom2 + ".log" + ccDateHeure.

        OUTPUT STREAM strEcriture TO VALUE(ccFicTmp).

        FOR EACH  bf1-ttRejet
            WHERE bf1-ttRejet.code-enr = "LOG"
            NO-LOCK:

            PUT STREAM strEcriture UNFORMATTED bf1-ttRejet.commentaire SKIP.
        END.

        OUTPUT STREAM strEcriture CLOSE.
    END.
END.


/*--------------------------------------------------------------------------*/
/*  P R O C E D U R E S   I N T E R N E S                                   */
/*--------------------------------------------------------------------------*/

/*********************************/
/* Lecture du fichier � int�grer */
/*********************************/
PROCEDURE lecture-fichier :
    cpt-lig = 0 .
    cpt-trt = 0 .
    cpt-trtfouinc = 0 .
    cpt-trtfouine = 0 .
    cpt-artdeal1    = 0.
    cpt-artdeal2    = 0.
    cpt-artdeal1-i  = 0.
    cpt-artdeal2-i  = 0.
    cpt-rejets      = 0.

    LECT-FIC :
    DO TRANSACTION :
      REPEAT :
          import stream integ-fic delimiter "|" z1 z15 z56 z70 z84
                 z98 z102 z113 z124 z135 z149 z151
                 z162 z177 z192 z196 z198.
          if z1 begins "/*" then next .
          RUN traitement .

      END. /* REPEAT */
    END. /* Do transaction */
END PROCEDURE . /* lecture-fichier */


/*************************************/
/* Traitement des lignes du fichiers */
/*************************************/
PROCEDURE traitement :

    DEFINE VARIABLE typaux-deal    AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE codaux-deal    AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE articl-deal    AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE articl-trav    AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE code-unite_1   AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE coef           AS DECIMAL     NO-UNDO.
    DEFINE VARIABLE rowid-trav     AS ROWID       NO-UNDO.
    DEFINE VARIABLE code-recu_1    AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE code-recu_15   AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE code-recu_56   AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE code-recu_70   AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE code-recu_84   AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE code-recu_98   AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE code-recu_149  AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE code-recu_192  AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE code-recu_196  AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE code-recu_198  AS CHARACTER   NO-UNDO.

    ASSIGN typaux-deal   = ""
           codaux-deal   = ""
           articl-deal   = ""
           code-unite_1  = "" 
           coef          = 1
           rowid-trav    = ?
           code-recu_1   = z1                               /* Code AREA du produit  */
           code-recu_15  = z15                              /* Libell� du produit    */
           code-recu_56  = z56                              /* GENCOD du produit     */
           code-recu_70  = z70                              /* Code AREA du tiers    */
           code-recu_84  = z84                              /* GENCOD du tiers       */
           code-recu_98  = z98                              /* Unit� de vente AREA   */
           code-recu_149 = z149                             /* Code TVA              */
           code-recu_192 = z192                             /* Priorit� fournisseur  1 � 99 99=inactif */
           code-recu_196 = z196                             /* C Creation M modif    */
           code-recu_198 = z198                             /* O actif N Non actif   */
           cpt-lig       = cpt-lig + 1  NO-ERROR .

/*================================================*/
/*              Recherche du codaux DEAL          */
/*    � partir du tiers � qui AREA passe commande */ 
/*================================================*/
    FIND LAST AUXGSS USE-INDEX theme 
      where  auxgss.codsoc    = x-codsoc
        and  auxgss.theme     = "COREDI"                 
        AND  auxgss.alpha-cle = code-recu_70 
        NO-LOCK NO-ERROR .

    IF NOT AVAILABLE auxgss THEN DO :
        FIND T-tempo-1 where 
             t-tempo-1.t1coredi    = code-recu_70
        no-lock no-error .
        if not available t-tempo-1 then DO:
           cpt-trtfouinc = cpt-trtfouinc + 1 .
           RETURN.     /* Si fournisseur jamais concern� */
        end.   
        cpt-trtfouine = cpt-trtfouine + 1 .
        ano-ligne = " Fournisseur AREA inexistant : " + code-recu_70  + " " + code-recu_1 + " " + code-recu_15 + " " + code-recu_84.
        RUN enregistrer-log(INPUT "REJ", INPUT cpt-lig, INPUT ligne, INPUT ano-ligne).
        RETURN.
    END.
    ELSE DO :
        ASSIGN typaux-deal = auxgss.typaux
               codaux-deal = auxgss.codaux 
               .
    END.

/* xxxxxxxxxxxxxxxxxxxxx
    /* Verification format GENCOD du Fournisseur */

     if TRIM(code-recu_84) <> "" then do: 
        code-barre = code-recu_84.
        run calgenco.p ( code-barre, output gencod-OK) .
        if not gencod-ok then do:
          cpt-rejets = cpt-rejets + 1.
          ano-ligne = " Format Gencod du Tiers Incorrect : " + code-recu_70  + " " + code-recu_1 + " " + code-recu_15 + " " + code-recu_84.
          RUN enregistrer-log(INPUT "REJ", INPUT cpt-lig, INPUT ligne, INPUT ano-ligne).
          RETURN.
        end.
     end.
xxxxxxxxxxxxxxxxxxxxx */

    /* Verification GENCOD du Fournisseur */
    FIND auxapr where  
         auxapr.codsoc = x-codsoc and
         auxapr.typaux = typaux-deal and
         auxapr.codaux = codaux-deal    
    no-lock no-error.
    if not available auxapr then do:
        cpt-rejets = cpt-rejets + 1.
        ano-ligne = " Code Tiers Inexistant : " + code-recu_70  + " " + code-recu_1 + " " + code-recu_15 + " " + code-recu_84.
        RUN enregistrer-log(INPUT "REJ", INPUT cpt-lig, INPUT ligne, INPUT ano-ligne).
        RETURN.
    end.
    ELSE DO:
        IF auxapr.gencod <> "" and auxapr.gencod <> code-recu_84 then do:
        cpt-rejets = cpt-rejets + 1.
        ano-ligne = " Gencod Tiers # Gencod Tiers AREA : Code DEAL=" + codaux-deal + " " + auxapr.gencod + " Code AreA=" + code-recu_84  + " " + code-recu_1 + " " + code-recu_15.                       
        RUN enregistrer-log(INPUT "REJ", INPUT cpt-lig, INPUT ligne, INPUT ano-ligne).
        RETURN.
        end.
    END.
    
    cpt-trt = cpt-trt + 1 .
    toperr = 0.  
    toperra = "".
                 
/*================================================*/
/*         Recherche du code article DEAL         */
/*================================================*/

/*  Sur le fournisseur AREA 811000 */

    FIND artbis 
        WHERE artbis.codsoc     = x-codsoc
          AND artbis.typaux     = typaux-area
          AND artbis.codaux     = codaux-area
          AND artbis.motcle     = x-motcle
          AND artbis.articl-fou = trim(code-recu_1) 
          AND not artbis.articl BEGINS "AR" NO-LOCK NO-ERROR .

    IF AVAILABLE artbis 
    THEN DO: 
        articl-deal = artbis.articl .
        cpt-artdeal1 = cpt-artdeal1 + 1.
        OUTPUT TO ediarea7b.mouc APPEND .
        MESSAGE "Articl-deal trouv� par Artbis " artbis.codaux "-" artbis.articl "-" artbis.articl-fou. 
        OUTPUT CLOSE .
    END.    
    ELSE DO:
       toperr = toperr + 1.  
       toperra = "1".   
       cpt-artdeal1-i = cpt-artdeal1-i + 1.
       /*
       ano-ligne = " Lien Article Fournisseur inexistant " + codaux-area + " " + code-recu_70  + " " + code-recu_1 + " " + code-recu_15 + " " + code-recu_84.
       RUN enregistrer-log(INPUT "REJ", INPUT cpt-lig, INPUT ligne, INPUT ano-ligne).
       */
    END.

/*  Sur le fournisseur livreur 811xxx */

    IF toperra = "1" THEN DO:
    FIND artbis 
        WHERE artbis.codsoc     = x-codsoc
          AND artbis.typaux     = typaux-deal
          AND artbis.codaux     = codaux-deal
          AND artbis.motcle     = x-motcle
          AND artbis.articl-fou = trim(code-recu_1) 
          AND not artbis.articl BEGINS "AR" NO-LOCK NO-ERROR .
    END. 
    ELSE DO:
        FIND artbis 
        WHERE artbis.codsoc     = x-codsoc
          AND artbis.typaux     = typaux-deal
          AND artbis.codaux     = codaux-deal
          AND artbis.motcle     = x-motcle
          AND artbis.articl     = articl-deal 
          AND not artbis.articl BEGINS "AR" NO-LOCK NO-ERROR .
    END.  
    IF AVAILABLE artbis 
    THEN DO: 
        articl-deal = artbis.articl .
        recartbisxxx = recid( artbis ).
        cpt-artdeal2 = cpt-artdeal2 + 1.
        OUTPUT TO ediarea7b.mouc APPEND .
        MESSAGE "Articl-deal trouv� par Artbis " artbis.codaux "-" artbis.articl "-" artbis.articl-fou. 
        OUTPUT CLOSE .
    END.    
    ELSE DO:  
       toperr = toperr + 1.  
       toperra = "2".   
       cpt-artdeal2-i = cpt-artdeal2-i + 1.
       /*
       ano-ligne = " Lien Article Fournisseur inexistant " + codaux-deal + " " + code-recu_70  + " " + code-recu_1 + " " + code-recu_15 + " " + code-recu_84.
       RUN enregistrer-log(INPUT "REJ", INPUT cpt-lig, INPUT ligne, INPUT ano-ligne).
       */
    END.
    if toperr = 1 then do:
       if toperra = "1" then
          ano-ligne = " Le Lien Fournisseur 811000 est inexistant! ! " + codaux-area + " " + code-recu_70  + " " + code-recu_1 + " " + code-recu_15 + " " + code-recu_84.
       if toperra = "2" then
          ano-ligne = " Le Lien Fournisseur 811xxx est inexistant! ! " + codaux-deal + " " + articl-deal + " " + code-recu_70  + " " + code-recu_1 + " " + code-recu_15 + " " + code-recu_84.
       cpt-rejets = cpt-rejets + 1.
       RUN enregistrer-log(INPUT "REJ", INPUT cpt-lig, INPUT ligne, INPUT ano-ligne).
    end.
    
    if toperr >= 1 then return.

/* Controle code barre article */
    IF LENGTH(code-recu_56) = 13 THEN DO:
     FIND codbar where 
          codbar.codsoc =  x-codsoc and
          codbar.code-barre = code-recu_56
     no-lock no-error.
     IF NOT AVAILABLE codbar then do: 
        cpt-rejets = cpt-rejets + 1.
        ano-ligne = " Code barre fourni par AREA Inexistant " + articl-deal + " " + code-recu_56 + " " + code-recu_1 + " " + code-recu_15.                       
        RUN enregistrer-log(INPUT "REJ", INPUT cpt-lig, INPUT ligne, INPUT ano-ligne).
        RETURN.
     END.
    END.
        
/* Traitement de la reference Article AREA on traite uniquement le fournisseur livreur pas le 811000 */

   recartbis = recartbisxxx.

   if code-recu_192 = "99" then do:    /* Inactif */
      Find B-ARTBIS where recid(b-artbis) = recartbis   
           no-lock no-error.  /* zzx */
           /* exclusive-lock no-error. */
      If available B-ARTBIS and not B-ARTBIS.articl-fou BEGINS "ZZ" then do:
         if optmaj = yes then do:
            B-ARTBIS.articl-fou = "ZZ" + B-ARTBIS.articl-fou.
            B-ARTBIS.datmaj = wdate.      
            B-ARTBIS.opemaj = wope. 
            B-ARTBIS.heumaj = wheu.
         end.
         OUTPUT TO ediarea7b.mouc APPEND .
         MESSAGE "Modif B-ARTBIS.articl-fou ==> ZZ " b-artbis.codaux "-" b-artbis.articl "-" b-artbis.articl-fou. 
         OUTPUT CLOSE .
      end.         
   end.
   else do:    /* Actif */
      Find B-ARTBIS where recid(b-artbis) = recartbis   
           no-lock no-error.  /* zzx */
           /* exclusive-lock no-error. */
      If available B-ARTBIS and B-ARTBIS.articl-fou BEGINS "ZZ" then do:
         if optmaj = yes then do:
            B-ARTBIS.articl-fou = SUBSTR(B-ARTBIS.articl-fou, 3).
            B-ARTBIS.datmaj = wdate.      
            B-ARTBIS.opemaj = wope. 
            B-ARTBIS.heumaj = wheu.
         end.
         OUTPUT TO ediarea7b.mouc APPEND .
         MESSAGE "Modif B-ARTBIS.articl-fou Supp ZZ " b-artbis.codaux "-" b-artbis.articl "-" b-artbis.articl-fou. 
         OUTPUT CLOSE .
      end.         
   end.
         
   find artapr where 
        artapr.codsoc = x-codsoc and         
        artapr.articl = articl-deal        
        no-lock no-error.  /* zzx */
        /* exclusive-lock no-error. */
   if available artapr then do:
      ablo = artapr.code-blocage.
      nblo = artapr.code-blocage.
   
      if code-recu_198 = "N" then do:    /* Inactif */
         if artapr.code-blocage = "" or artapr.code-blocage = "O" 
            then nblo = "I".
            else if artapr.code-blocage = "B" then nblo = "F".

         if ablo <> nblo then do:
            OUTPUT TO ediarea7b.mouc APPEND .
            MESSAGE "Modif artapr code-blocage " artapr.articl "-" ablo "-" nblo.                   
            OUTPUT CLOSE .
            if optmaj = yes then do:
               artapr.code-blocage = nblo.
               artapr.datmaj = wdate.      
               artapr.opemaj = wope. 
               artapr.heumaj = wheu.     
            end. 
         end.
      end.         /* if code-recu_198 = "N" */
      else do:
         if artapr.code-blocage = "I" 
            then nblo = "O".
            else if artapr.code-blocage = "F" then nblo = "B".

         if ablo <> nblo then do:
            OUTPUT TO ediarea7b.mouc APPEND .
            MESSAGE "Modif artapr code-blocage " artapr.articl "-" ablo "-" nblo .                   
            OUTPUT CLOSE .
            if optmaj = yes then do:
               artapr.code-blocage = nblo.
               artapr.datmaj = wdate.      
               artapr.opemaj = wope. 
               artapr.heumaj = wheu.      
            end.
         end.   
      end.

      if optmaj = yes then do:
         
         if ablo <> nblo then do:
      
         if substr(artapr.code-texte,2,1) = "" then
               assign substr(artapr.code-texte ,2,1) = "1".
   
         find proglib where proglib.motcle = ""
                                 and   proglib.codsoc = artapr.codsoc
                                 and   proglib.etabli = ""
                                 and   proglib.clef   = "ARTI-COS" + artapr.articl
                                 and   proglib.edition = ""
                                 and   proglib.chrono = 0
                                 exclusive-lock no-error.
         if available proglib then do:
             do aa = 1 to 16:
                if proglib.libelle[aa] <> "" and substr(proglib.libelle[aa],1,18) <> "Auto Arret Produit" and 
                   substr(proglib.libelle[aa],1,25) <> "Maj Auto Produit par AREA" then next.
                     OUTPUT TO ediarea7b.mouc APPEND .
                     message "MAj proglib " aa " " artapr.articl " " wcomm skip.
                     OUTPUT CLOSE .
                     assign proglib.libelle[aa] = wcomm
                           proglib.datmaj = wdate
                           proglib.heumaj = wheu
                           proglib.opemaj = wope.
                     leave.      
             end.       
         end.
         else do:
                            create proglib.
                            assign proglib.motcle = ""
                                   proglib.codsoc = artapr.codsoc
                                   proglib.etabli = ""
                                   proglib.clef   = "ARTI-COS" + artapr.articl
                                   proglib.edition = ""
                                   proglib.chrono = 0
                                   proglib.libelle[1] = wcomm
                                   proglib.datmaj = wdate
                                   proglib.heumaj = wheu
                                   proglib.opemaj = wope.
         end.

         end.   /* if ablo <> code-blocage */
      
      end.   /* optmaj = yes */

   end.  /* if available artapr */

END PROCEDURE . /* traitement */


/* Enregistrement des rejets et des messages log dans une table de travail */
PROCEDURE enregistrer-log:
DEFINE INPUT PARAMETER icType           AS CHARACTER    NO-UNDO.
DEFINE INPUT PARAMETER iiNum-Ligne      AS INTEGER      NO-UNDO.
DEFINE INPUT PARAMETER icLigne          AS CHARACTER    NO-UNDO.
DEFINE INPUT PARAMETER icAno-Ligne      AS CHARACTER    NO-UNDO.

    CREATE  ttRejet.
    ASSIGN  ttRejet.code-enr    = icType
            ttRejet.cpt         = iiNum-Ligne
            ttRejet.ligne       = icLigne
            ttRejet.commentaire = icAno-Ligne.

END PROCEDURE.

