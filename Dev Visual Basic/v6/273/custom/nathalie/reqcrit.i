def {1} shared var    wcodsoc    like bastat.codsoc                       no-undo.
def {1} shared var    wdsdem     as char    format 'x(16)'                no-undo.
def {1} shared var    wlibmag    as char    format "x(32)"                no-undo.
def {1} shared var    wsaut      as char    format 'x(16)'                no-undo.

def {1} shared var    wnocom     as char    format "x(6)"                 no-undo.
def {1} shared var    wlibcom    as char    format "x(32)"                no-undo.

def {1} shared var    topidx     as char                                  no-undo.
def {1} shared var    nnbimp     as int format '>9'    initial '01'       no-undo.
def {1} shared var    wzone as char format "x(30)".
def {1} shared var    nblus as int.
/* saut de page */
def {1} shared var    wtotlig    as integer.

/*  ------------------------------ */

def {1} shared var    wcpt       as integer format '999999'.
def {1} shared var    wcptxl     as integer format '999999'.
def {1} shared var    commande   as char.
def {1} shared var    wmagasin   as char    format 'x(03)'.
def {1} shared var    wmagcli    as char    format 'x(03)'.
def {1} shared var    wlibart1   as char    format 'x(32)'.
def {1} shared var    elibart1   as char    format 'x(32)'.
def {1} shared var    wauxilicodsoc like bastat.codsoc.

def {1} shared var    ecodsoc    like bastat.codsoc.
def {1} shared var    earticl    as char    format 'x(10)'.
def {1} shared var    emagasin   as char    format 'x(03)'.
def {1} shared var    emag2      as char    format 'x(03)'.
def {1} shared var    wtypadh    like auxapr.type-adh.

def {1} shared var    wclasse    as char format "x(15)"                   no-undo.
def {1} shared var    wcodetox   like artapr.code-toxic                   no-undo.
def {1} shared var    wva      like artapr.va                             no-undo.
def {1} shared var    wtox     like artapr.toxicite                       no-undo.
def {1} shared var    wmess    as char format "x(24)"                     no-undo.
def {1} shared var    wprix as deci decimals 3                            no-undo.
def {1} shared var    whtc  as deci decimals 3                            no-undo.
def {1} shared var    wcli  as deci decimals 3                            no-undo.
def {1} shared var    wpamp as deci decimals 3                            no-undo.

def {1} shared var    wmodliv like bastat.modliv                          no-undo.
def {1} shared var    wmodreg like bastat.modreg                          no-undo.
def {1} shared var    wdatech like bastat.datech                          no-undo.
def {1} shared var    wtypbon like bastat.typbon                          no-undo.

def {1} shared var    ecodaux    as char format 'x(10)'                   no-undo.
def {1} shared var    eqte       as deci format '>>>>>>>9.999-'           no-undo.
def {1} shared var    efacht     as deci format '>>>>>>>9.999-'           no-undo.
def {1} shared var    enumbon    as char format 'x(10)'                   no-undo.
def {1} shared var    edatbon    like bastat.datbon                       no-undo.
def {1} shared var    enumfac    like bastat.numfac                       no-undo.
def {1} shared var    edatfac    like bastat.datfac                       no-undo.
def {1} shared var    edatdep    like bastat.datdep                       no-undo.
def {1} shared var    warticl    as char format 'x(10)'                   no-undo.

def {1} shared var    efamart    as char    format 'x(03)'                no-undo.
def {1} shared var    esoufam    as char    format 'x(03)'                no-undo.
def {1} shared var    esssfam    as char    format 'x(03)'                no-undo.
def {1} shared var    wcodaux    as char    format 'x(10)'                no-undo.
def {1} shared var    wnom       as char    format 'x(32)'                no-undo.
def {1} shared var    wcdpos     as char    format 'x(05)'                no-undo.
def {1} shared var    wtlphn     as char    format 'x(15)'                no-undo.
def {1} shared var    enom       as char    format 'x(32)'                no-undo.
def {1} shared var    ecdpos     as char    format 'x(05)'                no-undo.
def {1} shared var    etlphn     as char    format 'x(15)'                no-undo.
def {1} shared var    wsocqte    as deci    format '>>>>>>>9.999-'        no-undo.
def {1} shared var    wmagqte    as deci    format '>>>>>>>9.999-'        no-undo.
def {1} shared var    wcltqte    as deci    format '>>>>>>>9.999-'        no-undo.
def {1} shared var    wcatqte    as deci    format '>>>>>>>9.999-'        no-undo.
def {1} shared var    wartqte    as deci    format '>>>>>>>9.999-'        no-undo.

def {1} shared var    wsoccaht   as deci    format '>>>>>>>9.999-'        no-undo.
def {1} shared var    wmagcaht   as deci    format '>>>>>>>9.999-'        no-undo.
def {1} shared var    wcltcaht   as deci    format '>>>>>>>9.999-'        no-undo.
def {1} shared var    wcatcaht   as deci    format '>>>>>>>9.999-'        no-undo.
def {1} shared var    wartcaht   as deci    format '>>>>>>>9.999-'        no-undo.
def {1} shared var    wcptlig    as integer                               no-undo.
def {1} shared var    wcptligd   as integer                               no-undo.
def {1} shared var    wcptpag    as integer format '>>>9'                 no-undo.
def {1} shared var    wcptpagd   as integer format '>>>9'                 no-undo.
def {1} shared var    wville     as char    format 'x(32)'                no-undo.
def {1} shared var    wauxiliadres5 as char format 'x(32)'                no-undo.
def {1} shared var    wtype-condit like artapr.type-condit                no-undo.
def {1} shared var    wusto        like artapr.usto                       no-undo.
def {1} shared var    wufac        like artbis.ufac                       no-undo.
def {1} shared var    wcoef-fac    like artbis.coef-fac                   no-undo.
def {1} shared var    wsurcondit   like artbis.surcondit                  no-undo.
def {1} shared var    wbarart as char format "x(10)"                      no-undo.
def {1} shared var    wbarcli as char format "x(20)"                      no-undo.

def {1} shared var    topdivers   as logical format "o/n"                 no-undo.
def {1} shared var    libdivers   as char    format "x(15)"               no-undo.

def {1} shared var    toplis   as logical format "o/n"                    no-undo.
def {1} shared var    liblis   as char    format "x(15)"                  no-undo.
def {1} shared var    liblis2  as char    format "x(15)"                  no-undo.
def {1} shared var    topcom   as logical format "o/n"                    no-undo.
def {1} shared var    libcom   as char    format "x(15)"                  no-undo.
def {1} shared var    libcom2  as char    format "x(15)"                  no-undo.
def {1} shared var    topeti   as logical format "o/n"                    no-undo.
def {1} shared var    libeti   as char    format "x(15)"                  no-undo.
def {1} shared var    libeti2  as char    format "x(15)"                  no-undo.
def {1} shared var    topxl    as logical format "o/n"                    no-undo.
def {1} shared var    libxl    as char    format "x(15)"                  no-undo.
def {1} shared var    libxl2   as char    format "x(15)"                  no-undo.

def stream file3.
def stream file5.
def stream file4.
def stream file1.
def stream file2.

/* dgr */

def {1} shared var barart as char                    no-undo.
def {1} shared var barcli as char                    no-undo.

def {1} shared var wtypcom as char                   no-undo.
def {1} shared var l-typcom as char                  no-undo.
def {1} shared var k as int                          no-undo.
def {1} shared var slib-court      as char         format "x(4)"                 no-undo.
def {1} shared var squidem         as char         format "x(25)"                no-undo.
def {1} shared var ddatedem        as date         format "99/99/9999"           no-undo.
def {1} shared var ddatedeb        as date         format "99/99/9999"           no-undo.
def {1} shared var ddatefin        as date         format "99/99/9999"           no-undo.
def {1} shared var sfamille        as char         format "xxx"   extent 10      no-undo.
def {1} shared var ssousfam        as char         format "xxx"   extent 10      no-undo.
def {1} shared var sssousfa        as char         format "xxx"   extent 10      no-undo.
def {1} shared var smagasin        as char         format "xxx"   extent 20      no-undo.
def {1} shared var sarticle        as char         format "x(6)" extent 25      no-undo.
def {1} shared var i               as int                                        no-undo.
def {1} shared var j               as int                                        no-undo.
def {1} shared var simpauto        as char         format 'x'                    no-undo.
def {1} shared var slibimp         as char         format "x(25)"                no-undo.
def {1} shared var sdebtrt         as char         format "x(20)"                no-undo.
def {1} shared var sfintrt         as char         format "x(20)"                no-undo.
def {1} shared var nnbpages        as int          format ">>9"                  no-undo.
def {1} shared var wcpteti         as int.
def {1} shared var wcptmag         as int.
def {1} shared var wcpteti2         as int.

/* variables pour edition */
def {1} shared var sequence     as char format "x(05)"             no-undo.
def {1} shared var wfictra      as char format "x(20)"             no-undo.
def {1} shared var snomfic      as char format "x(12)"             no-undo.
def {1} shared var snomfic2     as char format "x(12)"             no-undo.
def {1} shared var slibfic      as char format "x(12)"             no-undo.
def {1} shared var stypfic      as char format "x"                 no-undo.

def {1} shared var adres1       as char format "x(32)"             no-undo.
def {1} shared var adres2       as char format "x(32)"             no-undo.
def {1} shared var adres3       as char format "x(32)"             no-undo.
def {1} shared var adres4       as char format "x(32)"             no-undo.
def {1} shared var adres5       as char format "x(32)"             no-undo.

def {1} shared var ddatepamp        as date         format "99/99/9999"           no-undo.

def {1} shared var articl-soc       like bastat.codsoc.
def {1} shared var auxili-soc       like bastat.codsoc.
def {1} shared var type-tiers   as char format "xxx"             no-undo . 