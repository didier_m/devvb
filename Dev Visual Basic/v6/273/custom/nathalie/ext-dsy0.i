/*==========================================================================*/
/*                           E X T - D S Y 0 . I                            */
/* Extraction des donnees pour Daisy                                        */
/*--------------------------------------------------------------------------*/
/* Variables et Frames                                                      */
/*==========================================================================*/

/* Variables */
def var ind       as int                 no-undo.
def var lib-table as char format "x(76)" no-undo.

def {1} shared var libelle      as char extent 30           no-undo.
def {1} shared var wcodplf      as char format "x(10)"      no-undo.
def {1} shared var wcodfou      as char format "x(10)"      no-undo.
def {1} shared var wcodaux      as char format "x(10)"      no-undo.
def {1} shared var wcodtar      as char format "xxx"        no-undo.
def {1} shared var wlibtar      as char format "x(30)"        no-undo.
def {1} shared var wfamart      as char format "xxx"        no-undo.
def {1} shared var wsoufam      as char format "xxx"        no-undo.
def {1} shared var wsssfam      as char format "xxx"        no-undo.
def {1} shared var date-debut   as date format "99/99/9999" no-undo.
def {1} shared var date-fin     as date format "99/99/9999" no-undo.
def {1} shared var wmagasin     as char format "xxx"        no-undo.
def {1} shared var wcodsoc      as char format "xx"         no-undo.

def {1} shared var lissoc       as char                       no-undo initial "01,26,08".
def {1} shared var lisplf       as char                       no-undo initial "802190,805100".
def {1} shared var libplf       as char                       no-undo initial "LISADIS,LISADIS VEGETAL".
def {1} shared var wlibplf      as char format "x(30)"        no-undo.
def {1} shared var wlibfou      as char format "x(30)"        no-undo.
def {1} shared var wlibaux      as char format "x(30)"        no-undo.
def {1} shared var wlibfam      as char format "x(30)"        no-undo.
def {1} shared var wlibsfam     as char format "x(30)"        no-undo.
def {1} shared var wlibssfam    as char format "x(30)"        no-undo.
def {1} shared var wlibmag      as char format "x(30)"        no-undo.
def {1} shared var wlibsoc      as char format "x(30)"        no-undo.
def {1} shared var wnomenc      as log  format "O/N"          no-undo.

/* Dessin des Frames */
def {1} shared frame fr-saisie.
def {1} shared frame fr-trt.

Form libelle[01] format "x(18)" wcodsoc wlibsoc skip
     libelle[02] format "x(18)" wcodplf  wlibplf skip
     libelle[03] format "x(18)" wcodfou  wlibfou skip
     libelle[04] format "x(18)" wcodaux  wlibaux skip
     libelle[12] format "x(18)" wcodtar  skip
     libelle[05] format "x(18)" wfamart  wlibfam skip
     libelle[06] format "x(18)" wsoufam  wlibsfam skip
     libelle[07] format "x(18)" wsssfam  wlibssfam skip 
     libelle[08] format "x(18)" date-debut skip
     libelle[09] format "x(18)" date-fin skip
     libelle[10] format "x(18)" wmagasin wlibmag skip 
     libelle[11] format "x(18)" wnomenc
     with frame fr-saisie row 3 centered no-label overlay { v6frame.i }.

Form skip lib-table skip
     with frame fr-trt row 17 centered no-label overlay { v6frame.i }.