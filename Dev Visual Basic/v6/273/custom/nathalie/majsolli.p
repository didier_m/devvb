/* Programme : majsolli.p solde top-livre */

/* -------------------------------------------------------------------------- */
/* Acces parametre lecture societe article ==> OK                             */
/* -------------------------------------------------------------------------- */

{connect.i}

def var articl-soc    like artapr.codsoc.
def var xnum as char format "x(8)".
def var enum as integer format "99999999" label "N.Commande".
def var xart as char format "x(8)".
def var eart as integer label "Article".
def var wlibart as char format "x(32)" label "Libelle produit" .
def var xval as char format "x" label "Val".


repeat :
form skip
with title
"majsolli.p : Solder une ligne de commande CDF" centered.


regs-app = "NATHALIE" .
{ regs-cha.i }

{ regs-inc.i "articl" }
articl-soc = regs-soc .

display codsoc-soc.

enum = 0.
update enum.
xnum = string(enum , "99999999") + "00" .

find entete where
entete.codsoc = codsoc-soc and
entete.motcle = "ACH" and
entete.typcom = "CDF"
and numbon = xnum
no-lock no-error.
if not available entete then do:
bell. bell.
message "Commande inconnue".
undo,retry.
end.

eart = 0.
update eart help "Code article" auto-return.
xart = string(eart , "999999") .

find artapr
where artapr.codsoc = articl-soc
      and artapr.articl = xart
      no-lock no-error.
if not available artapr then do:
wlibart = "<- inconnu". display wlibart.
bell. bell.
message "code article inconnu".
undo,retry.
end.
wlibart = artapr.libart1[1].
display wlibart.

update xval
help "Valider pour Solder : O = OUI, N = NON"
validate ( xval = "O" or xval = "N" ,
" Valeurs autorises: O (OUI)  N (NON) ").

if xval = "N" then next.

for  each lignes where
 lignes.codsoc = entete.codsoc and
 lignes.motcle = entete.motcle and
 lignes.typcom = entete.typcom and
 lignes.numbon = entete.numbon and
 lignes.articl = xart          and
 lignes.top-livre <> "1"
  exclusive-lock:

message "Je solde la commande " entete.numbon
        " Article " xart artapr.libart1[1].

 lignes.top-livre = "1".
 lignes.datmaj = today . 

display entete.numbon chrono lignes.datdep
lignes.magasin lignes.articl lignes.qte[1] lignes.libart.

end.

end. /* repeat */