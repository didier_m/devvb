/* Prog : arealiv.p mise en format EXCEL livraison AREA     */

{connect.i}

def var sel-sequence   as char format "x(12)"            no-undo.
def var sequence       as char format "x(5)"             no-undo.
def var sedifile       as char format "x(48)"            no-undo.

def var wsoc   as char format "xx"          initial "45".
def var wok    as char format "x".

def stream file1.
def stream file2.

def var nb as integer .
def var entliv as char format "x(300)".
def var x-qte as char .
def var x-numbon as char.
def var x-typcom as char.
def var x-chrono as char.
def var i-chrono as int.
def var c-libart as char.
def var c-articl as char.
do with frame a: /* do saisie */

form
      "  " 
      skip
      "Spool ............... : " at 01 sedifile     no-label
      skip
      "Validation .......... : " at 01 wok          no-label
      skip
      with title
     "arealiv.p 09/2019 : Export Excel livraisons AREA du jour" centered.

/* determination No sequence operateur pour spoolname debut */
assign codsoc = " "
       etabli = " "
       typtab = "ope"
       prefix = "nume-spool"
       codtab = operat.
       run opeseq.p ("liv",output sequence, output sel-sequence).
/* determination No sequence operateur pour spoolname fin */

sedifile = sel-sequence.
substr(sedifile, 10,3) = "csv".

sedifile = "/applic/partageweb/xcspool/" + trim(operat) + "/" 
         + trim(sedifile) .
         
IF SEARCH( "/applic/erp/src/tty/nathalie/ch-param.p" ) <> ? THEN DO:
   sedifile = REPLACE (sedifile, "/applic/", "/applic/erp/").
END.

display sedifile.

update wok
  help "Confirmation Oui / Non"
    validate (wok = "O" or wok = "N" , "Valeurs autorisees : O ou N").

display wok.

if wok = "N" then leave.

input  stream file1 FROM /applic/li/arrivee/area/jmulivr.txt.
output stream file2 TO   value(sedifile).

put stream file2 UNFORMATTED 
    "Num. Cde" ";" "N.Avis" ";" "CodFour" ";" "N.BL15" ";" "Date" ";" 
    "Qte" ";" "Vehicule" ";"  "Operation" ";" "N.lig.Cde" ";"
    "CProduit" ";" "EAN" ";" "N.BL20" ";" "N.Lot" ";"
    "Typ" ";" "Num_Cde" ";" "CodArt" ";" "Libelle"
    skip. 

repeat:
import stream file1 unformatted  entliv .
nb = nb + 1.

x-qte = substr(entliv, 56,9).
x-qte = replace(x-qte, ".",",").

x-numbon    = SUBSTRING(entliv, 4, 6).           /* N� bon       */
x-numbon    = x-numbon + "00" .
x-numbon    = FILL("0", 10 - LENGTH(x-numbon)) + x-numbon.

IF SUBSTRING(entliv, 4, 1) = "8"
   THEN ASSIGN x-typcom      = "CAF".
   ELSE ASSIGN x-typcom      = "CDF".
ASSIGN  x-chrono = SUBSTRING(entliv,  84,  2).
                            
 /* Conversion du chrono */
i-chrono = INT(x-chrono) NO-ERROR.
IF ERROR-STATUS:ERROR THEN DO:
   message "PB conversion N. Ligne " x-numbon x-chrono. pause.
   next.
   end.
find lignes where 
     lignes.codsoc = "45" and
     lignes.motcle = "ACH" and
     lignes.typcom = x-typcom and
     lignes.numbon = x-numbon and
     lignes.chrono = i-chrono  
no-lock no-error.
if available lignes then do: 
                         c-articl = lignes.articl.
                         c-libart = lignes.libart.
                         end.
else do: 
     c-articl = "". 
     c-libart = "".
     end.
    

put stream file2 UNFORMATTED
    string(substr(entliv, 02,13))  ";"
    string(substr(entliv, 16,2))   ";"
    string(substr(entliv, 19,13))  ";"
    string(substr(entliv, 33,15))  ";"
    string(substr(entliv, 49,6))   ";"
    string(x-qte)   ";"
    string(substr(entliv, 66,15))  ";"
    string(substr(entliv, 82,1))   ";"
    string(substr(entliv, 84,2))   ";"
    string(substr(entliv, 87,13))  ";"
    string(substr(entliv, 101,13)) ";"
    string(substr(entliv, 115,20)) ";"
    string(substr(entliv, 136,20)) ";"
    string(x-typcom) ";" string(x-numbon) ";"
    string(c-articl) ";" string(c-libart) ";;"
    skip.

end.

end.

