

display "fermart.p debut date maj " today with frame debut.

def var waction as char.

def var wcpt    as integer format ">>>>>9".

def buffer z-tabges for tabges.
def var topmaj   as log.

def var wdate   as date.
def var wheu    as char.
def var wope    as char.

def var wnomfic1 as char format "x(26)".
def var wnomfic6 as char format "x(26)".
def var wnomfic2 as char format "x(26)".
def var wnomfic3 as char format "x(26)".
def var wnomfic4 as char format "x(26)".
def var wnomfic5 as char format "x(26)".

def buffer old-artapr for artapr.
def buffer new-artapr for artapr.
def var soc-tabges as char.
def var l-soc   as char initial "01,43,08".
def var i       as int.
def var xsoc    as char.
def var woldart as char.
def var wnewart as char.

def stream file1.
def stream file2.
def stream file3.
def stream file4.
def stream file5.

def buffer b-artbis for artbis.
def buffer b-tabges for tabges.
assign wdate = today
       wheu = string(time,"hh:mm")
       wope = "BDB" 
       topmaj = yes.

wnomfic1 = "basc-code".
wnomfic2 = wnomfic1 + "." + string(today,"999999") + ".log".
wnomfic3 = wnomfic1 + "." + string(today,"999999") + ".artapr.sav".
wnomfic4 = wnomfic1 + "." + string(today,"999999") + ".artbis.sav".
wnomfic6 = wnomfic1 + "." + string(today,"999999") + ".tabges.sav".

wnomfic5 = wnomfic1 + "." + string(today,"999999") + "codbar.sav".
output stream file5 to value(wnomfic5).

.

output stream file2 to value(wnomfic2).
output stream file3 to value(wnomfic3).
output stream file4 to value(wnomfic4).
output stream file1 to value(wnomfic6).

do i = 1 to num-entries(l-soc):

  xsoc = entry(i,l-soc).

  soc-tabges = xsoc.

  if soc-tabges = "43" then soc-tabges = "01".

  lect:
  
  for each tabges
  where tabges.codsoc = soc-tabges
  and   tabges.etabli = ""
  and   tabges.typtab = "ART"
  and   tabges.prefix = "RECODIF"
  and   index(tabges.libel3[13],xsoc ) <> 0  /* rgs passe */   
  and   index(tabges.libel3[14],xsoc ) = 0
  exclusive-lock :
    
    assign woldart = tabges.codtab 
           wnewart = tabges.libel1[1] .
         

    find old-artapr
    where old-artapr.codsoc = xsoc
    and   old-artapr.articl = woldart
    exclusive-lock no-error.
    if not available old-artapr then do:
      message "OLD ARTICLE INCONNU ==>" xsoc woldart view-as alert-box.
      next lect.
    end.

    find new-artapr
    where new-artapr.codsoc = xsoc
    and   new-artapr.articl = wnewart
    exclusive-lock no-error.
    if not available new-artapr then do:
      message "NEW ARTICLE INCONNU ==>" xsoc wnewart view-as alert-box.
      next lect.
    end.

    if new-artapr.code-blocage = "F" then 
      assign new-artapr.provenance = old-artapr.provenance
             new-artapr.code-blocage = old-artapr.code-blocage.

    if trim(new-artapr.provenance) = "" and 
       trim (old-artapr.provenance) <> ""  then 
      assign new-artapr.provenance = old-artapr.provenance.
    
    new-artapr.libart1[1] = replace (new-artapr.libart1[1],"ZZ","").
    assign new-artapr.libart1[1] = trim(new-artapr.libart1[1] )
           new-artapr.libart = substr(new-artapr.libart1[1] ,1,20).
           
    export stream file3 old-artapr.
    waction =     "JE FERME ==> " .
    export stream file2 delimiter ";" topmaj
         waction  old-artapr.codsoc old-artapr.articl
         old-artapr.libart1[1] old-artapr.provenance old-artapr.code-blocage 
         old-artapr.famart
         old-artapr.soufam
         old-artapr.sssfam
         old-artapr.famart2 
         old-artapr.soufam2 
         old-artapr.sssfam2 
         old-artapr.datcre 
         old-artapr.datmaj 
         .

    if topmaj = yes then do:
      if old-artapr.code-blocage <> "F" then 
        assign old-artapr.code-blocage = "F".

      if old-artapr.provenance <> "9" then 
        assign old-artapr.provenance = "9".
  
      if old-artapr.edi-etiq <> "" then 
        assign old-artapr.edi-etiq = "".

      if old-artapr.edit-tarif <> "1" then 
        assign old-artapr.edit-tarif = "1".

      if not (old-artapr.libart begins "ZZ" ) then 
        assign old-artapr.libart = "ZZ " + old-artapr.libart.

      assign old-artapr.datmaj = wdate  
             old-artapr.heumaj = wheu
             old-artapr.opemaj = wope
             new-artapr.datmaj = wdate  
             new-artapr.heumaj = wheu
             new-artapr.opemaj = wope.

    end. /* if topmaj = yes */

    for each codbar 
    where codbar.codsoc = old-artapr.codsoc
    and   codbar.articl = old-artapr.articl
    exclusive-lock:
      waction =     "JE REAFFECTE le GENCOD ==> " .
      export stream file2 delimiter ";" topmaj
         waction  old-artapr.codsoc old-artapr.articl
         old-artapr.libart1[1] old-artapr.provenance old-artapr.code-blocage 
         old-artapr.famart
         old-artapr.soufam
         old-artapr.sssfam
         old-artapr.famart2 
         old-artapr.soufam2 
         old-artapr.sssfam2 
         old-artapr.datcre 
         old-artapr.datmaj 
         codbar.code-barre  
         new-artapr.articl.

      export stream file5 codbar.

      if topmaj = yes then 
        assign codbar.articl = new-artapr.articl
               codbar.datmaj = wdate 
               codbar.opemaj = wope
               codbar.heumaj = wheu.
    end. /* for each codbar */
    
  
  
    find artbis
    where artbis.codsoc = old-artapr.codsoc
    and   artbis.motcle = "ACH"
    and   artbis.typaux = "TIE"
    and   artbis.codaux = "    802190"
    and   artbis.articl = old-artapr.articl 
    no-error.
    if available artbis then do:
        find b-artbis
        where b-artbis.codsoc = artbis.codsoc
        and   b-artbis.motcle = artbis.motcle
        and   b-artbis.typaux = artbis.typaux
        and   b-artbis.codaux = artbis.codaux
        and   b-artbis.articl = new-artapr.articl
        no-error.
        if not available b-artbis then do:
           create b-artbis.
           buffer-copy artbis
             except artbis.articl  
                    artbis.datcre artbis.opecre artbis.heucre
                    artbis.datmaj artbis.opemaj artbis.heumaj
           to b-artbis
             assign b-artbis.articl = new-artapr.articl
                    b-artbis.datcre = wdate
                    b-artbis.opecre = wope 
                    b-artbis.heucre = wheu
                    b-artbis.datmaj = wdate
                    b-artbis.opemaj = wope
                    b-artbis.heumaj = wheu.

        end. /* if not available b-artbis */
      
    
        waction =     "JE met le lien a blanc ==> " .
        export stream file2 delimiter ";" topmaj
           waction  xsoc old-artapr.articl 
           old-artapr.libart1[1] old-artapr.provenance old-artapr.code-blocage 
           old-artapr.famart  old-artapr.soufam  old-artapr.sssfam
           old-artapr.famart2  old-artapr.soufam2  old-artapr.sssfam2 
           old-artapr.datcre  old-artapr.datmaj 
           artbis.codaux 
           artbis.articl-fou 
           artbis.datcre 
           artbis.datmaj
           .
         

        export stream file4 artbis.

        assign artbis.articl-fou = ""
               artbis.heumaj = wheu
               artbis.datmaj = wdate 
               artbis.opemaj = wope.

    end. /* if available artbis */
    find b-tabges
    where b-tabges.codsoc = xsoc
    and   b-tabges.etabli = "ACH"
    and   b-tabges.typtab = "ART"
    and   b-tabges.prefix = "SUIT-FICHE"
    and   b-tabges.codtab = "TIE    802190" + old-artapr.articl
    no-error.
    if available b-tabges then do:
     
      find z-tabges
      where z-tabges.codsoc = xsoc
      and   z-tabges.etabli = "ACH"
      and   z-tabges.typtab = "ART"
      and   z-tabges.prefix = "SUIT-FICHE"
      and   z-tabges.codtab = "TIE    802190" + new-artapr.articl
      no-error.
      if available z-tabges then do:
        delete b-tabges.
      end.
      else do:
        
        assign b-tabges.codtab = replace(b-tabges.codtab,old-artapr.articl,new-artapr.articl)
               b-tabges.datmaj = wdate 
               b-tabges.opemaj = wope
               b-tabges.heumaj = wheu.
      end.                       
    end.  /* if available b-tabges */

    assign tabges.libel3[14] = tabges.libel3[14] + xsoc + ",".

  end. /* for each tabges */

end. /* do i = 1 to num-entries(l-soc) */
 
output stream file1 close .
output stream file2 close .
output stream file3 close .
output stream file4 close .
output stream file5 close .
