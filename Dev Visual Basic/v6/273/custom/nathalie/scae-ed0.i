/*---------------------------------------------------------------------------*/
/*                      S C A E - C - E D 0 . I                              */
/*---------------------------------------------------------------------------*/

lect = "{2}" .
if lect <> ""  and  lect <> "999"
   then  do:
        FIND sortie
        use-index chrono no-lock no-error .
   end.
   else  do:
        FIND {1} sortie 
        use-index chrono no-lock no-error.
   end.