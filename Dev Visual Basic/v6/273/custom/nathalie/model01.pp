DEFINE VARIABLE repert-mail AS CHARACTER   NO-UNDO.

FIND tabges WHERE tabges.codsoc = ""
            AND   tabges.etabli = ""
            AND   tabges.typtab = "PRM"  
            AND   tabges.prefix = "ENVOI-MAIL"  
            AND   tabges.codtab = "" 
            NO-LOCK NO-ERROR.
IF AVAILABLE tabges THEN  
   ASSIGN repert-mail = TRIM( tabges.libel1[1] + tabges.libel1[2] ) .

/* Presence nouveau repertoire erp */

IF SEARCH( "/applic/erp/src/tty/nathalie/ch-param.p" ) <> ? THEN DO:
   FIND tabges WHERE tabges.codsoc = ""
               AND   tabges.etabli = ""
               AND   tabges.typtab = "PRM"  
               AND   tabges.prefix = "ENVOI-MAIL"  
               AND   tabges.codtab = "new" 
               NO-LOCK NO-ERROR.
   IF AVAILABLE tabges THEN  
      ASSIGN repert-mail = TRIM( tabges.libel1[1] + tabges.libel1[2] ) .
END.
