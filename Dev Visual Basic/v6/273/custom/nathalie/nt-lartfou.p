/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! Deal/nathalie/cmm/nt-prgclient.p                                                            !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intercvention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 19614 30-06-10!New!bp             !mise en place d'un lanceur pour programmes maison des clients         !
!-------------------------------------------------------------------------------------------------------------!
!                             I N C L U D E S                !                    T A B L E S                 !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!d-brokmini.i          !new                                  !                                                !
!bat-ini.i             !new                                  !                                                !
!select.i              !new                                 !                                                !
!aide.i                !new                                  !                                                !
!maquette.i            !new                                  !                                                !
!bat-cha.i             !"sel-applic" "selection-applic" "a"  !                                                !
!regs-cha.i            !                                     !                                                !
!selectpt.i            !                                     !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
!_____________________________________________________________________________________________________________!
!                                                  P R O C E D U R E S                                        !
!_____________________________________________________________________________________________________________!
!TRAITEMENT            !                                                                                      !
&End
**************************************************************************************************************/
/* DGR 311016 : creation a partir de esrc/l-artfou.p                                                         */
/* DGR 211016 : ajout pan                                                                                    */
/* DGR 210317 : changement ordre de lecture suivant si on lance par fournisseur ou pas                       */
/* DGR 071217 : ajout nomenclature 2                                                                         */
/* CCO 15072019 : ajout {tarcli.i}                                                                           */

{ d-brokmini.i new }


{ bat-ini.i    new }
{ select.i     new }
{ aide.i       new }
{ maquette.i   new }  


DEFINE stream maquette.   /*  canal maquette */


DEFINE VARIABLE x-prgclient AS CHARACTER   NO-UNDO.
def var    x-alpha1         as char       no-undo . 
def var    x-alpha2         as char       no-undo .
def var    x-alpha3         as char       no-undo .
def var    x-alpha4         as char       no-undo .
def var    x-alpha5         as char       no-undo .
def var    x-alpha6         as char       no-undo .
def var    x-alpha7         as char       no-undo .
def var    x-alpha8         as char       no-undo .
def var    x-alpha9         as char       no-undo .
DEF VAR    x-alpha10        as char       no-undo .


def temp-table t-bastat like bastat.


def input parameter  fichier as char format "x(12)" .

run bat-lec.p ( fichier ) .


/*---------------------------------------------------------*/
/* Prise des selections et chargement des bornes mini-maxi */
/*---------------------------------------------------------*/


{ bat-cha.i  "sel-applic"     "selection-applic"   "a"          }
{ bat-cha.i  "sel-filtre"     "selection-filtre"   "a"          }
{ bat-cha.i  "sel-sequence"   "selection-sequence" "a"          }
{ bat-cha.i  "codsoc"         "glo-codsoc"         "a"          }
{ bat-cha.i  "etabli"         "glo-etabli"         "a"          }

{ bat-cha.i  "ma-impri"      "ma-impri"          "a"          }   
{ bat-cha.i  "ma-maquet"      "ma-maquet"          "a"          }   
{ bat-cha.i  "lib-prog"       "lib-prog"           "a"          }

{ bat-cha.i  "prgclient"     "x-prgclient"          "a"          }   

{ bat-cha.i  "alpha1"       "x-alpha1"           "a"          }   
{ bat-cha.i  "alpha2"       "x-alpha2"           "a"          }
{ bat-cha.i  "alpha3"       "x-alpha3"           "a"          }   
{ bat-cha.i  "alpha4"       "x-alpha4"           "a"          }
{ bat-cha.i  "alpha5"       "x-alpha5"           "a"          }   
{ bat-cha.i  "alpha6"       "x-alpha6"           "a"          }
{ bat-cha.i  "alpha7"       "x-alpha7"           "a"          }   
{ bat-cha.i  "alpha8"       "x-alpha8"           "a"          }
{ bat-cha.i  "alpha9"       "x-alpha9"           "a"          }   
{ bat-cha.i  "alpha10"      "x-alpha10"          "a"          }


run select-c.p.  

regs-app = "elodie".
{regs-cha.i }
regs-app = "nathalie".
{regs-cha.i}

deal-setsocreg ( "" ) .
deal-setsocreg ( glo-codsoc ) .


run sel-del.p ( selection-applic, selection-filtre, selection-sequence, yes ) .  


RUN traitement .



procedure traitement :

    def var l-soc        as char                            no-undo .
    def var wsoc         as char                            no-undo .
    def var i-soc        as int                             no-undo .
    def var topblanc     as char                            no-undo .   
    def var topferme     as char                            no-undo .
    def var top1a10      as char                            no-undo .
    def var wcodtar      as char                            no-undo .   
    
    def var l-codfam     as char                            no-undo .

    def var wmincdeMS    as dec                             no-undo .
    def var wdatste      like {tarcli.i}.date-debut             no-undo .
    

    def var articl-soc    like artapr.codsoc.

    def var nblus        as int no-undo.
    def var wgencod      as char format "x(100)"            no-undo.
    def var wartfou      as char no-undo.
    def var wlibfou      as char no-undo.
    def var wvalste      like {tarcli.i}.valeur                 no-undo.
    def var wdatcli      like {tarcli.i}.date-debut             no-undo.
    def var wvalcli      like {tarcli.i}.valeur                 no-undo.
    def var irem         as int.
    def var jrem         as int.
    def var wdatrfc      like remcli.date-debut             no-undo.
    def var wremrfc      as dec decimals 3 format ">>>>>9.999-" .


    /* variables de travail */
    def var wcode        as char format "x(6)" no-undo.
    def var wcod2        as char format "x(10)" no-undo.
    def var libaux       as char format "x(22)" no-undo.
    def var valid        as char format "x".
    def var sfamille     as char format "xxx" extent 12 no-undo.
    def var ssousfam     as char format "xxx" extent 12 no-undo.
    def var sssousfa     as char format "xxx" extent 12 no-undo.
    def var wpan         as char                        no-undo. 
    def var i            as integer.
    def var j            as integer.
 
    
    l-soc       = trim(x-alpha1)   .   
    topblanc    = trim(x-alpha2)   .   
    topferme    = trim(x-alpha3)   .    
    top1a10     = trim(x-alpha4)   .    
    wcodtar     = trim(x-alpha5)   .    
    
    if wcodtar = "C" then wcodtar = "CLI".
                     else wcodtar = "HTC".
    

    def var sel-fou as log.
    def var i-i     as int.
    /* alimentation de la liste des familles à traiter */
    if top1a10 = "O" then 
        assign l-codfam = "001,002,003,004,005,006,007,008,009,010,021,022".
    else do:    
        FOR EACH TABGES 
        where tabges.codsoc = ""
        and   tabges.etabli = ""
        and   tabges.typtab = "ART"
        and   tabges.prefix = "FAMART"
        no-lock :
            { select-z.i  "FAMART"    "tabges.codtab" }
            run select-t.p.
            if selection-ok <> ""  then  next .
            else do: 
                if l-codfam = "" then l-codfam = tabges.codtab.
                                 else l-codfam = l-codfam + "," + tabges.codtab.  
            end.
        end.
    end.

    OUTPUT STREAM maquette TO VALUE( ma-impri ) .
    export stream maquette delimiter ";"
        "Reference Fournisseur" "Code Article" "Designation" "Societe"
        "type tiers" "Code Fournisseur" "Nom Fournisseur"
        "Famille" "Sous Famille" "Sous sous Famille" "Code provenance" 
        "Code blocage" "Typologie" "PAN"
        "Gencod" "Prix modif"
        "Date dernier " + wcodtar "Valeur dernier " + wcodtar
        "Ucde" "Coef-cde" "Ufac" "Coef-fac" "Min de commande MS"
        "Date dernier STE" "Valeur dernier STE"
        "Date RFC" "Remise RFC" 
        "Fournisseur de ma plate forme = Designation 2 fiche fou"
        "Libelle Long"
        "ref fabricant(issue GVSE) "
        "code TVA"
        "Designation 1 fiche fou"
        "Famille 2" "Sous Famille 2" "Sous sous Famille 2" .
    
    
    /*  Si sélection sur le motclé 'fourniseeur' : Prise en compte des seuls clients ayant ce code article  */
    i-i = lookup( "codaux" , selfic-motcle ) .
    if i-i <> 0  then  do:
    
        do i-soc = 1 to num-entries (l-soc) :
            wsoc = entry ( i-soc , l-soc ) .
            for each auxapr use-index auxapr-1
            where auxapr.codsoc = wsoc
            and   auxapr.typaux = "tie"
            no-lock :
                { select-z.i  "typaux"   "auxapr.typaux" }
                { select-z.i  "codaux"   "auxapr.codaux" }
                run select-t.p.
                if selection-ok <> ""  then  next .
            
                wlibfou = auxapr.adres[1].
                
                for each artbis use-index artbis-2
                where artbis.codsoc = wsoc
                and   artbis.motcle = "ACH"
                and   artbis.typaux = "TIE"
                and   artbis.codaux = auxapr.codaux
                and   ( topblanc = "O" or   artbis.articl-fou <> "" )
                no-lock :

                    
    
                    /* if topblanc = "N" and artbis.articl-fou = "" then next . */
                        
                    { nt-lartfou.i }
                end .
            end.
        end.
    end.
    else do:
        do i-soc = 1 to num-entries (l-soc) :
            wsoc = entry ( i-soc , l-soc ) .
            
            
            for each artbis use-index artbis-2
            where artbis.codsoc = wsoc
            and   artbis.motcle = "ACH"
            and   artbis.typaux <> ""
            and   ( topblanc = "O" or   artbis.articl-fou <> "" )
            no-lock 
            break by artbis.codaux :
                if first-of ( artbis.codaux ) then do:
                    wlibfou = "".
                    find auxapr use-index auxapr-1
                    where auxapr.codsoc = wsoc
                    and   auxapr.typaux = artbis.typaux
                    and   auxapr.codaux = artbis.codaux
                    no-lock no-error. 
                    if available auxapr then wlibfou = auxapr.adres[1].
                                        else wlibfou = "Fournisseur Inconnu".
                                        
                end.
                    /* if topblanc = "N" and artbis.articl-fou = "" then next . */
                        
                { nt-lartfou.i }
            
            end.
        end.
    end.
    output stream maquette close.
end procedure .



