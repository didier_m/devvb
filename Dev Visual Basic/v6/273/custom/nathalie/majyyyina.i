
if not lignes.datcre <  magasi.datdeb-inv then next.  /* date Jour Inventaire */

qte-liv = 0.

case lignes.motcle :
	when "ACH"  then  sens = 1 .
	when "VTE"  then  sens = -1 .
	otherwise next .
end case .

if lignes.coef-cde < 0 	then  qte-liv = - ( lignes.qte[ 2 ] * sens ) / lignes.coef-cde .
						else  qte-liv =     lignes.qte[ 2 ] * sens   * lignes.coef-cde .

if wmaj = "OUI" then do:
	find stoinv 
	where stoinv.codsoc = lignes.codsoc
	and stoinv.date-invent = magasi.datfin-inv
	and stoinv.articl = lignes.articl
	and stoinv.evenement = "INA"
	and stoinv.magasin = lignes.magasin
	exclusive-lock no-error.
	if available stoinv then do: 
		ASSIGN	stoinv.qte-stock = stoinv.qte-stock + qte-liv /* lignes.qte[2]  */
				stoinv.datcre      = today
				stoinv.opecre      = "Bcc" 
				stoinv.datmaj      = today
				stoinv.opemaj      = "Bcc" .
	end.
	else do:
		CREATE STOINV. 
		ASSIGN  stoinv.codsoc      = lignes.codsoc
				stoinv.date-invent = magasi.datfin-inv
				stoinv.articl      = lignes.articl
				stoinv.evenement   = "INA"
				stoinv.magasin     = lignes.magasin
				stoinv.qte-stock   = stoinv.qte-stock + qte-liv /*  lignes.qte[2] */
				stoinv.datcre      = today
				stoinv.opecre      = "Bcc" 
				stoinv.datmaj      = today
				stoinv.opemaj      = "Bcc". 
	end.    
end.
nblig = nblig + 1.
display  
	nblig format ">>>" no-label column-label "nb lig" lignes.codsoc no-label  	lignes.typcom column-label "Typ"   	lignes.numbon 	lignes.nivcom column-label "N" format "x"
	lignes.datcre				lignes.datdep						lignes.nivcom	lignes.magasin 				
	lignes.opecre column-label "LIG!CRE" format "xxx" 				lignes.articl format "xxxxxx"
	lignes.qte [2] format "->>>>9"		qte-liv format "->>>>9"		
with width 500.
		