/*                              VARIABLES                                     */


def {1} shared var sequence     as Char     format "x(5)".
def {1} shared var sel-sequence as Char     format "x(12)".
def {1} shared var valid        as char     format "x".
def {1} shared var ecart        as char     format "x".
def {1} shared var annee        as integer  format "9999".
def {1} shared var mois         as integer  format "99".
def {1} shared var wdate        as date.
def {1} shared var wdateinv  as  date format "99/99/9999".
def {1} shared var wmag         as char     format "xxx".
def {1} shared var wcdm         as char     format "xxx".
def {1} shared var wpamp as dec decimals 3.
def {1} shared var vale  as dec decimals 2 format "999999999.99-".
def {1} shared var vale2 as dec decimals 2 format "999999999.99-".
