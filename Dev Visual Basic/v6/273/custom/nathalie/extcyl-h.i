/*==========================================================================*/
/*                           E X T C Y L - H . I                            */
/* Extraction des donnees pour Cylande                                      */
/*--------------------------------------------------------------------------*/
/* interface STOCKS MAGASINS                                                */
/*==========================================================================*/
/* DGR 16112018 : conversion des stocks si u2 u3                            */

find magasi 
where magasi.codsoc = magasi-soc
and   magasi.codtab = w-mag-stocks
no-lock no-error. 

assign date-invmag = ?  .
FIND MAGASS 
where magass.codsoc     = soc-trt
and   magass.codtab     = w-mag-stocks
and   magass.theme      = "INVMAG"
and   magass.chrono     = 0
and ( magass.alpha[ 1 ] = "V"  or  magass.alpha[ 1 ] = "X" )
use-index primaire  no-lock  no-error .
if available magass  then  
   date-invmag = magasi.datfin-inv .   
/* fin DGR 12082013 */        

/*  recherche des Stocks Magasins modifi�s         */
for each stocks 
where stocks.codsoc = soc-trt
and   stocks.datmaj >= date-debut 
and   stocks.evenement = ""
and   stocks.exercice = ""
and   stocks.magasin = w-mag-stocks
use-index date-maj
no-lock :
        
   find artics
   where artics.codsoc = "TCYL00"
   and   artics.theme = "CORCYART"
   and   artics.articl = stocks.articl
   and   artics.chrono = 0
   no-lock no-error.
   if not available artics then next.

   find artapr
   where artapr.codsoc = stocks.codsoc
   and   artapr.articl = stocks.articl
   no-lock no-error.
   if not available artapr then next. 
   
   assign 	codsoc-soc = ""
            typtab = "EUR"
            prefix = "CORUNCYL"
            codtab = trim(artapr.type-condit) + trim( artapr.usto) 
            xx-param = "".
    
   run ch-paray.p( output xx-param ) .
   if xx-param = ""  then wuvte = "UN" .
   else  assign  wuvte = entry	( 1,  xx-param, "|" ) .        /* correspondance unit� UR  */

      
   warticl = artics.alpha-cle.
   /* DGR 12082013  ecart inventaire */
            
   stophy = stocks.qte[2].
   if date-invmag <> ?  then do :
       FIND STOINV 
       where stoinv.codsoc      = soc-trt
       and   stoinv.magasin     = w-mag-stocks
       and   stoinv.date-invent = date-invmag
       and   stoinv.articl      = stocks.articl 
       and   stoinv.evenement   = "INA"
       no-lock  no-error .
       if available stoinv then
          stophy = stophy + ( stoinv.qte-invent - stoinv.qte-stock ) .
   end .
   if lookup ( wuvte , l-u2dec ) <> 0 then stophy = stophy * 100.
   if lookup ( wuvte , l-u3dec ) <> 0 then stophy = stophy * 1000.
   assign 
      wzone = warticl + ";"
            + string (stophy) + ";" 
            + string ( today, "99999999") + ";"
            + "0000" + ";"
            + "1".
   put stream sto unformatted
      wzone
      skip.
/*
   export stream sto delimiter ";"
      warticl stophy string ( today, "99999999") "0000" "1".
*/
   nbecr = nbecr + 1.
 end.