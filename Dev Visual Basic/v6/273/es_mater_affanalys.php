<?php
/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/elodie/php/es_mater_affanalys.php                                                       !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 21205 22-10-10!Evo!jb             !modif suite op                                                        !
!V61 20967 08-10-10!Evo!jb             !OP 16103                                                              !
!V61 17420 03-02-10!Evo!jb             !modif & bugs divers                                                   !
!V61 17275 25-01-10!Evo!jb             !colonnes suppl�mentaires                                              !
!V61 17003 18-12-09!Evo!jb             !ajout colonnes                                                        !
!V61 16440 09-11-09!Evo!jb             !r�cup�ration km                                                       !
!V61 15300 24-07-09!Evo!jb             !divers bugs                                                           !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/

    $locatError="http://".$HTTP_HOST."/errors.php";
if($PHPSESSID=="")
 {
  header("Location: ".$locatError);
  exit;
 }
else
 {
  //d�marrage de la session en cours
  session_start();

  // v�rification de l'acc�s (pr�sence du fichier de l'op�rateur dans admis)
  $FileIp = $admisPath . session_id() . "." . $ope;
  if (!file_exists($FileIp))
   {
    session_destroy();
    header("Location: ".$locatError);
    exit;
   }
 }

#fin de la s�curit�
#-----------------------------------------------------------------------------------------------

//appel de la bibliotheque outil
include($dealCommunPath);
$ses=session_name()."=".session_id();

//appel des classes utilis�es dans cette page
include(deal_commun::classesPath("do-classes_search.php"));
include(deal_commun::classesPath("do-classes_lect_bin.php"));
include(deal_commun::classesPath("do-classes_exchange.php"));

require($screenDesignPath);

$expzone_charg = explode("|",$ZONECHARG);
$zone_charg[1] = $expzone_charg[0];  //ORACLE 

//Programme serveur d'�change
$dogExchange = search::searchExe("do-dogexchange.php",1)."?".session_name()."=".session_id() ;
$dogExchangeFrameSrc = search::searchExe("do-dogexchangeiframe.php",1)."?".session_name()."=".session_id() ;
//$TabBudAff = search::searchExe("es_budaffrows.php",1)."?".session_name()."=".session_id() ;
$excel = search::searchExe("do-exportexcel.vbs",0) ; // pour export excel
$date_jour  = date("d/m/Y");
?>

<?include ($excel) ;  // pour export excel ?>

<link rel="stylesheet" href=<?=search::searchExe("dv-params.css",1)?>>
<script src="<?=search::searchExe("do-dogexchange.js",1)?>"></script> 

<SCRIPT LANGUAGE=JAVASCRIPT>

//Pour positionement de la cellule
var currow ; //Indique la ligne en cours
var curcell ; //Indique la cellule en cours
var currentrow ;

//D�finition des objets
var oDiv ; //Balise DIV du tableau
var oTable ; //Le corps du tableau
var oDivHeader ; //Balise de l'entete

//Couleur de s�lection
var cellSelectedForeColor ;
var cellSelectedBackColor
var cellRestoreForeColor ;
var cellRestoreBackColor ;

//Supprimer l'affichage du bouton droit
document.oncontextmenu = DisableRightClick;
//var ctrletb = "" ;

function Retour()
{
	
	top.window.returnValue = "" ;
	top.window.close();
}

function tableur ( tableau )
{
	ExportExcel ( document.all( "header-frame" + tableau ) ,  document.all("saisiebody-frame" + tableau ) ) ;
}

function getPOSTHTTPResponse() 
{
    retourExchange = ReadHTTPResponse () ;
	//Tester le retour et quitter s'il ya une erreur
	if ( retourExchange != "OK") return 
}

function DisableRightClick() {return false;}

function initScroll ( SelectedForeColor  , SelectedBackColor  , oObjectDiv , oObjectTable , oObjectDivHeader )
{
	//Initialiser les variables de positionner des lignes
	currow = 0 ;
	curcell = 0 ;
	//Initialiser les variables objets
	oDiv = oObjectDiv ;
	oTable = oObjectTable ;			
	oDivHeader = oObjectDivHeader
	//Intialiser les variables de couleur
	cellSelectedForeColor = SelectedForeColor ;
	cellSelectedBackColor = SelectedBackColor ; 	
}

function selectCell ( indexRow )
{
	oTable.rows(currow).className = "overTableau" ; 
}	
function restoreCell ( indexRow )
{
	oTable.rows(currow).className = "outTableau" ; 
}

//Fonction qui fait le scroll
function setScrollCell()
{
	//D�termination du scroll bas
	if ( ( oTable.rows(currow).cells(curcell).offsetTop + oTable.rows(currow).cells(curcell).offsetHeight) > ( oDiv.clientHeight + oDiv.scrollTop))  oDiv.scrollTop = ( (oTable.rows(currow).cells(curcell).offsetTop + oTable.rows(currow).cells(curcell).offsetHeight) )- oDiv.clientHeight;	
	//D�termination du scroll haut
	if ( oTable.rows(currow).cells(curcell).offsetTop< oDiv.scrollTop) oDiv.scrollTop = ( (oTable.rows(currow).cells(curcell).offsetTop + oTable.rows(currow).cells(curcell).offsetHeight) )- oDiv.clientHeight; 	
	//D�termination du scroll Left
	if ( oTable.rows(currow).cells(curcell).offsetLeft< oDiv.scrollLeft) oDiv.scrollLeft = ( (oTable.rows(currow).cells(curcell).offsetLeft + oTable.rows(currow).cells(curcell).offsetWidth) )- oDiv.clientWidth; 	
	//D�termination du scroll Right
	if ( ( oTable.rows(currow).cells(curcell).offsetLeft + oTable.rows(currow).cells(curcell).offsetWidth) > ( oDiv.clientWidth + oDiv.scrollLeft))  oDiv.scrollLeft = ( (oTable.rows(currow).cells(curcell).offsetLeft + oTable.rows(currow).cells(curcell).offsetWidth) )- oDiv.clientWidth;

	//modifier les positions du scroll de l'ent�te
	oDivHeader.scrollLeft = oDiv.scrollLeft ;
}

//Fonction qui permet de faire scroller l'ent�te en m�me tant que le corps du tableau
function setScrollFromBar ()
{
	//modifier les positions du scroll de l'ent�te
	oDivHeader.scrollLeft = oDiv.scrollLeft ;
}

//Fonction qui trappe les touches du clavier
function Tablefromkeybord()
{
	if (event.type.toUpperCase() == "KEYDOWN")
	{ 	
		//alert (cellRestoreForeColor) ;
		//restorebgcolor ( currow ) ;
		//Selon la touche
		switch (event.keyCode)
		{		
			case 27 : //Fermeture
				event.returnValue = false ; 				
				break ;
			case 8 :
				event.returnValue = false ;
				break ; 				
			case 13 :
				event.returnValue = false ;
				//curcell = event.srcElement.cellIndex  ;
				//currow = event.srcElement.parentElement.rowIndex  ;
				MyIndexRow = currow ;
				var MyRowid =  document.all ("saisiebody").rows[currow].id ;
				selectrowbyclick(MyIndexRow) ;
				break ; 
			case 37 : //Gauche
			        return ;
				event.returnValue =  "" ; //Annuler la touche saisie
				if ( ( curcell - 1) <0 ) return ;  //Tester la position	
				
				if (curcell == 1) {
				   MyIndexRow = currow ;
				   selectCell ( currow ) ;
	   			}
							
				restoreCell () ; //D�selectionner l'ancienne cellule
				//Touche Ctrl
				if ( event.ctrlKey )
					curcell = 0; //Se positionner sur la premi�re cellule
				else
					--curcell ; //D�cr�menter la cellule
				selectCell () ; //S�lectionner la nouvelle cellule
				
				setScrollCell() ; //D�terminer le scroll
				break;
				
			case 39 : //Droite
			        return ;
				event.returnValue =  "" ; //Annuler la touche saisie					
				if ( (curcell + 1 ) >= oTable.rows(currow).cells.length) return ; //Tester la position						
				restoreCell () ; //D�selectionner l'ancienne cellule
				
				if (curcell == 0 || curcell == 1) {
				   document.all ("saisiebody").rows(currow).style.backgroundColor = "WHITE" ;
				   document.all ("saisiebody").rows(currow).style.color='336699';
				   MyIndexRow = currow ;
	                        }
					
				if ( event.ctrlKey )
					curcell = oTable.rows(currow).cells.length - 1 ; //Derniere cellule de la ligne
				else
					++curcell ; //Incr�menter la cellule				
				selectCell () ; //S�lectionner la nouvelle cellule				
				setScrollCell() ; //D�terminer le scroll				
				break;
				
			case 40 : //Bas	
			    event.returnValue =  "" ; //Annuler la touche saisie
				if ( currow + 1 >= oTable.rows.length) return ; //Tester la position								
				restoreCell () ; //D�selectionner l'ancienne cellule
				
				//if (curcell == 0) {
				   restoreCell ( currow ) ;
				   MyIndexRow = currow ;
				   cellRestoreForeColor =  oTable.rows(currow).style.foregroundcolor ;				  
				   currow = currow + 1 ;
				   //S�lectionner la nouvelle
				   selectCell ( currow ) ;
				   var MyRowid =  document.all ("saisiebody").rows[currow].id ;	   			
				   return ;
				//}	
					
				if ( event.ctrlKey )
					currow = oTable.rows.length - 1; //Derni�re ligne du tableau
				else
					++currow ; //Incr�menter la ligne
				
				selectCell() ; //S�lectionner la nouvelle cellule
								
				setScrollCell() ; //D�terminer le scroll
				break ;
								
			case 38 : //Haut
				event.returnValue = "" ; //Annuler la touche saisie					
				if ( currow  == 0 )  return ; //Tester la position				
				restoreCell () ;//D�selectionner l'ancienne cellule
				
				//if (curcell == 0) {
				   restoreCell ( currow ) ;
				   MyIndexRow = currow ;
				   cellRestoreForeColor =  oTable.rows(currow).style.foregroundcolor ;
				   //cellRestoreBackColor = oTable.rows(MyIndexRow).cells(curcell).style.backgroundColor ;
				   //Faire la ligne la ligne en cours
				   currow = currow - 1 ;
		
				   //S�lectionner la nouvelle
				   selectCell ( currow ) ;
				   var MyRowid =  document.all ("saisiebody").rows[currow].id ;	   			
				   return ;
				//}	
					
				if ( event.ctrlKey )
					currow=0 ; //Premi�re ligne
				else
					--currow ; //D�cr�menter la ligne
				selectCell () ;//S�lectionner la nouvelle cellule
												
				setScrollCell() ;  //D�terminer le scroll
				break ;
				
			case 35 : //Fin
				event.returnValue = "" ; //Annuler la touche saisie										
				restoreCell () ;  //D�selectionner la ligne suivante
				currow = oTable.rows.length - 1;//D�terminer la ligne
				selectCell () ;//S�lectionner la ligne suivante
				setScrollCell() ;  //D�terminer le scroll
				break;
			case 36 : //Debut
				event.returnValue =  "" ; //Annuler la touche saisie										
				restoreCell () ;  //D�selectionner la ligne suivante
				currow = 0; //D�terminer la ligne
				selectCell () ;//S�lectionner la ligne suivante
				setScrollCell() ;  //D�terminer le scroll
				break;
			case 34: //Page down
				event.returnValue = "" ; //Annuler la touche saisie	
				restoreCell () ; //D�selectionner la ligne pr�cedent
				if ( currow + 10 < oTable.rows.length)				
					currow = currow + 10;				
				else				
					currow=oTable.rows.length -1;				
				selectCell () ;//S�lectionner la ligne suivante
				setScrollCell() ;  //D�terminer le scroll
				break;
			case 33 : //PageUp
				event.returnValue =  "" ; //Annuler la touche saisie	
				restoreCell () ;  //D�selectionner la ligne pr�cedente
				if ( currow - 10 >-1 )
					currow = currow - 10;				
				else				
					currow=0;				
				selectCell () ; //S�lectionner la ligne suivante
				setScrollCell() ;  //D�terminer le scroll				
				break; 

            case 69  :  // e    // pour export excel 
            case 101 :  // E
              if ( event.ctrlKey == true )
              {
                restorebgcolor(currentrow)
                event.returnValue =  false //Annuler la touche saisie	
                ExportExcel ( document.all("saisieheader") ,  document.all("saisiebody") )
                selectbgcolor(currentrow)
              }
            break ;  

 			
		} //Selon la touche		
	} //Si on est sur KEYDOWN
}//Fin de la fonction

function restorebgcolor ( oRow)
{
	try {
	oRow.className= "outTableau" ; } catch ( eXception) {}
}

function selectbgcolor ( oRow )
{
    try {
		CurrentRow = oRow.rowIndex ;
                 oRow.className="overTableau" ;
                 } catch ( eXception) {}
}	

function frommouseclick()
{
	//restaurer la couleur de l'ancienne cellule
	restoreCell();
	curcell = event.srcElement.cellIndex  ;
	currow = event.srcElement.parentElement.rowIndex  ;
    currentrow = event.srcElement.parentElement ;

	selectCell ( currow ) ;
}

function selectrowbyclick(numrow)
{

	//Fonction qui s�lectionne un ligne
	restoreCell();
	//statusrecord = "" ;	
	// jpl le 18/01/2005

    var oRow = event.srcElement.parentElement ; 
    currentrow = oRow ;

    var Libelle =   "&_materiel=" + oRow.materiel + 
                     "&_annee=" + oRow.annee + 
                     "&_mois=" + oRow.mois +
                     "&_date=" + oRow.date +
                     "&_rowid=" + oRow.rowid +
                     "&_typinterv=" + oRow.typinterv  +
                     "&_origine=ANA" ;

/*
    if ( window.opener.vectaff == ""  )
	{
	for ( iframe1 =0 ; iframe1<document.all("saisiebody").rows.length ; iframe1++ ) 
			{
				//R�cup�rer un pointeur vers la ligne
				aRow = document.all("saisiebody").rows[ iframe1] ;
				if ( window.opener.vectaff == "" ) window.opener.vectaff = aRow.cells[0].innerText.trim() ;
				else window.opener.vectaff += "," + aRow.cells[0].innerText.trim() ;
		        }
	 }
*/

    window.opener.getligne(Libelle) 
    // window.close()
	return ;
}

</SCRIPT>

<HTML>
<title>Analyseur garage</title>
<BODY class=dogskinbodyfont onkeydown="Tablefromkeybord()">
<!--<P style="top:1px;left:15px;font-weight:500;font-size:14pt">Liste des <?=lect_bin::getLabel("Marche","sl")?></P>-->

<!-- pour echange avec le serveur PHP via un post HTTP -->
<IFRAME ID="EXCHANGEDOG" style="visibility:hidden" onload="getPOSTHTTPResponse()" src="<?=$c_dogExchangeFrameSrc?>">
</IFRAME>

<DIV>

<?php

print "<DIV class=dogskinshapeshadowstyle style='position:absolute;width:980px;height:19px;top:4px;left:8px' ></DIV>";
print "<DIV id='saisiedivheader' class=dogskinshapestyle style='position:absolute;height:19px;left:5px;top:1px;width:980px;overflow:hidden'>";
//ent�te
print "<TABLE class=dogtableheader id='saisieheader' cellpadding='0' cellspacing='0'  style='position:absolute;left:-2;top:-2;height:19px;table-layout:fixed'>" ;
print "<TR id='tborddetheader' CLASS=dogtableheaderfont>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:70px'><CENTER>Date</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:50px'><CENTER>Mat.</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:150px'><CENTER>Libell�</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:90px'><CENTER>Fam. Mater.</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:80px'><CENTER>S/Fam Mat.</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:80px'><CENTER>Affect. ana.</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:55px'><CENTER>H/KM</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:80px'><CENTER>Date km</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:80px'><CENTER>Soc. App.</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:35px'><CENTER>Site</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:80px'><CENTER>Intervenant</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:100px'><CENTER>Type r�parat.</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:150px'><CENTER>Libell�</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:80px'><CENTER>Article</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:150px'><CENTER>Libell�</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:80px'><CENTER>Type</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:80px'><CENTER>Fam. art.</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:80px'><CENTER>S/Fam art.</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:45px'><CENTER>Qt�</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:45px'><CENTER>Prix</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:80px'><CENTER>Total</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:40px'><CENTER>Fac.</CENTER></TD>" ;
print "<TD CLASS=dogtableheadercell ISEDIT='0' STYLE='WIDTH:80px'><CENTER>N� fact.</CENTER></TD>" ;
print "<TD ISEDIT=0 class=dogtableheadercell ALIGN=CENTER style='width:45px'><CENTER>Util.<CENTER></TD>" ;
print "<TD ISEDIT=0 class=dogtableheadercell ALIGN=CENTER style='width:180px'><CENTER>Utilisateur<CENTER></TD>" ;
print "<TD ISEDIT=0 class=dogtableheadercell ALIGN=CENTER style='width:40px'><CENTER>Soc.<CENTER></TD>" ;
print "<TD ISEDIT=0 class=dogtableheadercell ALIGN=CENTER style='width:150px'><CENTER>Soci�t�<CENTER></TD>" ;
print "<TD ISEDIT=0 class=dogtableheadercell ALIGN=CENTER style='width:40px'><CENTER>Serv.<CENTER></TD>" ;
print "<TD ISEDIT=0 class=dogtableheadercell ALIGN=CENTER style='width:150px'><CENTER>Service<CENTER></TD>" ;

print "</TR>" ;
print "</TABLE>" ;
print "</DIV>" ;
				
//Imprimer le tableau de d�tail
print "<DIV class=dogskinshapeshadowstyle style='position:absolute;width:998px;height:670px;top:28px;left:8px' ></DIV>";
print "<DIV id='saisiediv' class=dogtablebodydiv style='position:absolute;top:25px;width:998px;height:670px;left:5px' onscroll='setScrollFromBar()'>"  ;
print "<TABLE onclick=frommouseclick() onmousedown=frommouseclick() ID='saisiebody' cellpadding='0' cellspacing='0'  class=dogtablebody style='position:absolute;left:-2px;table-layout:fixed' >" ; 									
print "<COL ALIGN=LEFT style='width:70px'>" ;
print "<COL ALIGN=LEFT style='width:50px'>" ;
print "<COL ALIGN=LEFT style='width:150px'>" ;
print "<COL ALIGN=LEFT style='width:90px'>" ;
print "<COL ALIGN=LEFT style='width:80px'>" ;
print "<COL ALIGN=LEFT style='width:80px'>" ;
print "<COL ALIGN=LEFT style='width:55px'>" ;
print "<COL ALIGN=LEFT style='width:80px'>" ;
print "<COL ALIGN=LEFT style='width:80px'>" ;
print "<COL ALIGN=LEFT style='width:35px'>" ;
print "<COL ALIGN=LEFT style='width:80px'>" ;
print "<COL ALIGN=LEFT style='width:100px'>" ;
print "<COL ALIGN=LEFT style='width:150px'>" ;
print "<COL ALIGN=LEFT style='width:80px'>" ;
print "<COL ALIGN=LEFT style='width:150px'>" ;
print "<COL ALIGN=LEFT style='width:80px'>" ;
print "<COL ALIGN=LEFT style='width:80px'>" ;
print "<COL ALIGN=LEFT style='width:80px'>" ;
print "<COL ALIGN=LEFT style='width:45px'>" ;
print "<COL ALIGN=LEFT style='width:45px'>" ;
print "<COL ALIGN=LEFT style='width:80px'>" ;
print "<COL ALIGN=LEFT style='width:40px'>" ;
print "<COL ALIGN=LEFT style='width:80px'>" ;
print "<COL ALIGN=LEFT style='width:45px'>" ;
print "<COL ALIGN=LEFT style='width:180px'>" ;
print "<COL ALIGN=LEFT style='width:40px'>" ;
print "<COL ALIGN=LEFT style='width:150px'>" ;
print "<COL ALIGN=LEFT style='width:40px'>" ;
print "<COL ALIGN=LEFT style='width:150px'>" ;

//Constituer le nom global du fichier
$filetoload = $GLOBALS["tempoPath"].$_listefile.".tmp" ;	

//V�rifier si on trouve le fichier ASCII sp�cifi�
if ( $_listefile != "") {

if ( file_exists ( $filetoload))
{

	if ( file_exists ( $filetoload))
    {

      $NbRow = 0 ;
      $hdl_bonlig = fopen ( $filetoload , "r") ;
			       	 
      while ( !feof($hdl_bonlig))
	  {
		
		//Lecture de la ligne
	    $Ligne = fgets($hdl_bonlig , 40000	) ;

	    //Traitement de la ligne
	    if ( $NbRow > 0 && trim($Ligne!=""))
	    {
	       //S�parer le RowId des donn�es elle-m�le
           $ExpLigne = explode ("~" , $Ligne ) ;
               //Eclater la liste des donn�es
	       $ListeData = explode ( "�" , $ExpLigne[1]);
	       
  	       //Anlyse des champs de la ligne
	       for ( $i=1; $i<sizeof($ListeData);$i++)
	       {	
				//S�parer le mot cl� de sa valeur
     			$ExpOneData = explode ( "�" , $ListeData[$i]) ;
				$ExpOneData[0] = str_replace( "-" , "" , $ExpOneData[0]) ;
				//Tester si une valeur est rens�ign�e
				// if ( trim($ExpOneData[1]) =="") $ValToAdd[$i] = "&nbsp;" ;
				// else $ValToAdd[$i] = $ExpOneData[1];	 
                $ValToAdd[$i] = $ExpOneData[1];    	   

	       } //for ( $i=0; $i<sizeof($ListeData);$i++)

		   print "<TR rowid='".$ValToAdd[17]."' materiel='".$ValToAdd[2]."' annee='".$ValToAdd[16]."' mois='".$ValToAdd[15]."' date='".$ValToAdd[1]."' typinterv='".$ValToAdd[14]."' ID='".$ExpLigne[0]."' class=dogtablerow onDblClick=selectrowbyclick() style=\"cursor:hand\"> " ;
		   print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[1]."&nbsp</TD>";  // date
		   print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[2]."&nbsp</TD>";  // code mat�riel
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[3]."&nbsp</TD>";  // libell� mat�riel
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[24]."&nbsp</TD>"; // famille mat�riel
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[27]."&nbsp</TD>"; // S/famille mat�riel
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[21]."&nbsp</TD>"; // affectation analytique
		   print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[4]."&nbsp</TD>";  // km
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[5]."&nbsp</TD>";  // date km
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[6]."&nbsp</TD>";  // soci�t� appartenance
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[19]."&nbsp</TD>"; // site
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[20]."&nbsp</TD>"; // intervenant
		   print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[7]."&nbsp</TD>";  // type r�paration
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[8]."&nbsp</TD>";  // libelle type
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[9]."&nbsp</TD>";  // article
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[10]."&nbsp</TD>"; // libelle
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[22]."&nbsp</TD>"; // type article
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[23]."&nbsp</TD>"; // famille article
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[28]."&nbsp</TD>"; // S/famille article
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[12]."&nbsp</TD>"; // qt�
		   print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[13]."&nbsp</TD>"; // pu 
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[18]."&nbsp</TD>"; // total
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[25]."&nbsp</TD>"; // facturable
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[26]."&nbsp</TD>"; // n� facture
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[29]."&nbsp</TD>"; // Utilisateur
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[30]."&nbsp</TD>"; // lib Utilisateur
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[31]."&nbsp</TD>"; // soc Utilisateur
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[32]."&nbsp</TD>"; // lib soci�t� 
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[33]."&nbsp</TD>"; // service
           print "<TD CLASS=dogtablecell ISEDIT='0'>".$ValToAdd[34]."&nbsp</TD>"; // lib service
		   print "</TR>" ;			
		} // if ( $NbRow > 0 && trim($Ligne!=""))

		$NbRow ++ ;

	  } // while ( !feof($hdl_bonlig))
	  print "</TABLE>" ;
	  print "</DIV>" ;

	  fclose ( $hdl_bonlig);
	  print "<SCRIPT>" ;
	  //Initialiser la gestion du scroll du tableau
	  print "initScroll ( 'white'  , '336699'  , document.all('saisiediv') , document.all('saisiebody') ,  document.all('saisiedivheader') );";

	  //print "document.all('saisiebody').rows.count = ".$NbRow ;
	  print "</SCRIPT>" ;
	} // if ( file_exists ( $filetoload))	
  } //if ( file_exists ( $filetoload))
} //if ( $_listefile != "")

?>

</DIV>

</BODY>

<SCRIPT LANGUAGE=JAVASCRIPT>
	//window.resizeTo ( 364 , 700 ) ;
	//window.moveTo(658, 1) ;

function afficheligne() {

	if (document.all("saisiebody").rows.length > 0)
	{
		currow = 0 ;
		curcell = 0  ;
		cellRestoreForeColor =  oTable.rows(currow).cells(1).style.foregroundcolor ;
		selectCell ( currow ) ;
        currentrow = document.all("saisiebody").rows[0] ;
	}
	
}

setTimeout("afficheligne()",500);

</SCRIPT>


</HTML>










