<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<?php
/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! Deal/nathapp/php/as_bonapp_w3c.php                                                          !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 164784 2017-06-15!Evo!Jean Baptiste C![GC] [CT78014] Gestion de saisie libre des parcelles dans la saisie de!
! 135920 2017-06-08!Bug!Willy Petit-Mai![GC] controle tiers/contrat apres modif tiers bon en attente          !
! 165512 2017-06-08!Bug!Willy Petit-Mai![GC]Correction raz parametrage zones saisie apres validation          !
! 165480 2017-06-08!Bug!pierre teyssier!Absence de cermvs theme rotation                                      !
! 163731 2017-05-22!Bug!Willy Petit-Mai![GC] Correction decompte et pb saisie date des bons apres generation +!
!        2017-05-17!Bug!Willy Petit-Mai![GC] Affichage zones ctr prod/cer avant nouvelle saisie               !
!        2017-05-16!Bug!Willy Petit-Mai!correction retour modal IE                                            !
!        2017-05-10!Bug!Willy Petit-Mai!corrections pb compilation 333                                        !
! 163351 2017-05-10!Bug!Willy Petit-Mai![GC][MNT163181] Correction caract generation +/- et CST/CET           !
!        2017-05-05!Bug!Willy Petit-Mai!pb compilation                                                        !
! 162072 2017-05-03!Bug!pierre teyssier!Anomalie sur plan de cellules et saisie des bons                      !
! 152832 2017-04-25!Evo!Petit-Maire Wil![GC] Developpement ordre saisie                                       !
! 161054 2017-04-26!Bug!Willy Petit-Mai![GC] dysfonctionnement saisie bons : changement gestion caracteriqtiqu!
! 161273 2017-04-28!Bug!pierre teyssier!Bon en attente                                                        !
! 161751 2017-04-26!Bug!Victor GAVOILLE![710] Changement du chemin vers le fichier du pont bascule pour corres!
!        2017-04-25!   !pierre teyssier!validation                                                            !
!        2017-04-24!   !pierre teyssier!Elargir la fenetre F10 sur l'adresse de livraison                     !
!        2017-04-13!Bug!pierre teyssier! sur le get-cellule                                                   !
!        2017-03-28!   !pierre teyssier!WF144598 Plan de cellule 333/734                                      !
!        2017-04-06!   !Petit-Maire Wil!Correction validation peniches                                        !
!        2017-04-04!   !Petit-Maire Wil!BUG158643 - Correction de l'affichage des caracteristiques, des mouvem!
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/

#-----------------------------------------------------------------------------------------------
#   S�curit� : v�rification que l'utilisateur a respect� toutes les �tapes de connexion

include_once("../../../../siexe/deal/emilie/php/do_connect_w3c.php");
#fin de la s�curit�
#-----------------------------------------------------------------------------------------------

//appel de la bibliotheque outil
include($dealCommunPath);
$ses=session_name()."=".session_id();

//appel des classes utilis�es dans cette page
include(deal_commun::classesPath("do-classes_search_w3c.php"));
include(deal_commun::classesPath("do-classes_lect_bin_w3c.php"));
include(deal_commun::classesPath("do-classes_exchange_w3c.php"));
include(deal_commun::classesPath("do_tableau_w3c.php"));
include(deal_commun::classesPath("do-toolsclient_w3c.php"));

require($screenDesignPath);

//acces au fichier xml de param�trage du wstk
include ( search::searchExe("dealgate_nusoap_w3c.php",0) ) ;

//Pour echange PHP
$dogExchange	= search::searchExe("do-dogexchange_w3c.php",1)."?".$ses;
$dogExchangeF10 = search::searchExe("do-dogexchangef10_w3c.php",1)."?".$ses;
$urlLectureFTP = search::searchExe("ao_lecturetaresupervisionftp_w3c.php",1)."?".$ses;
$fichierS = search::searchExe("ao_fichier_s_w3c.php",1)."?".$ses;

//opeBase
$_opebase = getOperatValue();

//Dates
$date_jour  		= date("d/m/Y");
$date_jour_en 		= date("m/d/Y");
$date_explose 		= explode("/", $date_jour);
$date_cours 		= $date_explose[0];
$mois_cours 		= $date_explose[1];
$mois_cours_1 		= intval($date_explose[1]) - 1;
$annee_cours 		= $date_explose[2];
$heure_jour 		= date("H:i:s");
$heure_explode 		= explode(":", $heure_jour);
$heure_cours 		= $heure_explode[0];
$minutes_cours 		= $heure_explode[1];
$secondes_cours 	= $heure_explode[2];

//Suivant le mois en cours, l'annn�e pr la campagne sera diff�rente
if($mois_cours >="07")
	$annee=$annee_cours;
else
	$annee=$annee_cours-1;

// entete des colonnes
$GLOBALS["strfiltreGridBons"] = "�cer-attente�20�codmvt�60�no-ticket�codauxges�adresse_1�220�codcer�libel�150�typtrp�60�libel�cer-typfac�cer-contrat�pdsnet�pdsnormes�val-etat_1�val-etat_2�val-etat_3�val-etat_4�val-etat_5";
$GLOBALS["strfiltreGridExec"] = "�ll-numlig=N� ligne�100�L�dat-min�100�C�dat-max�100�C�refer-cnt�100�L" ;//�sel= �0-----�toto= �120
$GLOBALS["strfiltreGridEclat"] = "�typauxecl=Typ �70�C�codauxgesecl=Code �100�adresse=Nom �230�C�codmvtecl=Code Mvt�100�typcal=Type Eclat�pdsNormes=Pds �118" ;

if($filtrename == "") $filtrename = $_filtrename;

// recherche param�trage de l'affichage
$fonction_rech ="DOG_FUNCTION~CHPARAX�n-parax�0�~codsoc�" . $c_codsoc . "�typtab�PRM�prefix�AFB-BONAPP�codtab�" .$filtrename ;
$_DogAction = "GET" ;
$_GetNbRow = 0 ;

//lancement de la fonction de demande
$localreponse = SendExchange (  $fonction_rech , "" ) ;

//on recupere les valeurs
$temprep=explode ( "�" , strstr($localreponse,"REPONSE�") );
$tempexp=explode( "�" ,$temprep[0] ) ;
$_reponse = $tempexp[1];
$tempexp=explode( "/" ,$_reponse ) ;


//-------------------------------------------------------------- R E C U P E R A T I O N    P A R A M   E C L A T T E M E N T   S P E
 //param�trage : ""/""/PRM/AS-BONAPP/MVT-ORI
$fonction_rech ="DOG_FUNCTION~get-parmvtori�a-tabcer�0�~";
$_DogAction = "GET" ;
$_GetNbRow = 0 ;
$localreponse = SendExchange (  $fonction_rech , "" ) ;

//on recupere les valeurs
$mvtOrirep=explode ( "�" , strstr($localreponse,"CODMVT�") );
$mvtOriexp=explode( "�" ,$mvtOrirep[0] ) ;
$_mvtOri = $mvtOriexp[1]; 
//-----------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------- R E C U P E R A T I O N    P A R A M   A F F E C T A T I O N   O R G E 
 //param�trage : ""/""/PRM/AFEC-ORGE/A
$fonction_rech ="DOG_FUNCTION~get-paramOrge�a-tabcer�0�~";
$_DogAction = "GET" ;
$_GetNbRow = 0 ;
$localreponse = SendExchange (  $fonction_rech , "" ) ;

//on recupere les valeurs
$codeCarRep=explode ( "�" , strstr($localreponse,"CODCAR�") );
$codeCarExp=explode( "�" ,$codeCarRep[0] ) ;
$_codeCar = $codeCarExp[1]; 
//-----------------------------------------------------------------------------------------------------------------------------------

if($_mvtOri != "")
	$GLOBALS["strfiltreGridEclat"] = "�typauxecl=Typ �70�codauxgesecl=Code �100�adresse=Nom �230�codmvtecl=Code Mvt�100�contrat�100�option�100�pdsNormes=Pds aux normes �118" ;
	
// attention il faut penser � faire -1 par rapport au client serveur
$_affich = $tempexp[0].$tempexp[1].$tempexp[2].$tempexp[3].$tempexp[4].$tempexp[5].$tempexp[6].$tempexp[7].$tempexp[8].$tempexp[9];


if ( $_affich != "" )
{
    // remplacer # par � et $ par �
    $_affich = str_replace( "#" , "�" , $_affich ) ;
    $_affich = str_replace( "$" , "�" , $_affich ) ;
    $GLOBALS["strfiltreGridBons"] = $_affich ;
}
//on rajoute le rowid
$GLOBALS["strfiltreGridBons"] = $GLOBALS["strfiltreGridBons"] . "�rowid�0";

//On traite les ent�tes des colonnes
doTableauVar($strfiltreGridBons, 'strenteteGridBons', 'strcolwidthGridBons', 'strcolalignGridBons', 'strcolformatGridBons', 'strcollibelGridBons');
doTableauVar($strfiltreGridEclat, 'strenteteGridEclat', 'strcolwidthGridEclat', 'strcolalignGridEclat', 'strcolformatGridEclat', 'strcollibelGridEclat');
doTableauVar($strfiltreGridExec, 'strenteteGridExec', 'strcolwidthGridExec', 'strcolalignGridExec', 'strcolformatGridExec', 'strcollibelGridExec');

//libel menu
if($_libel_menu=="")$_libel_menu="Saisie des bons";


//premier operateur base
$opebaseexplose=explode("|",$ope_base);
$opebaseexplose2=explode("*",$opebaseexplose[0]);
$opebase=$opebaseexplose2[1];

//On reccupere le parametrage de l'operateur
$fonction_rech ="DOG_FUNCTION~GET-OPERAT�n-tabges�0�~codsoc�" . $c_codsoc . "�OPERAT�" . $opebase;
$context = "�CONTEXT�ID=" . session_id() . "|OPERAT=" . GetOperatValue() . "|LANGUE=" . $lang . "|CODSOC=" . $c_codsoc . "|CODETB=" . $c_codetb . "|IP=" . getenv('REMOTE_ADDR') ;
$_DogAction = "GET" ;
$_GetNbRow = 0 ;

//lancement de la fonction de demande
$rspoperat = SendExchange (  $fonction_rech . $context , "" ) ;

//mode debug quand on le prog est dans "diexe"
$debugActif = "non";
if(strpos($_SERVER['REQUEST_URI'], 'diexe')!==false)
	$debugActif = "oui";

if(strpos($_SERVER['REQUEST_URI'], 'diexe')!==false && $ident == "230" && $opebase == "pha")
	$_clotctr="O";

?>
<link rel="stylesheet" href="<?=search::searchExe("dv-params_w3c.css",1)?>">

<script type="text/javascript" src="<?=search::searchExe("do-toolsclient_w3c.js", 1)?>"></script>
<script type="text/javascript" src="<?=search::searchExe("do-dogexchange_w3c.js", 1)?>"></script>
<script type="text/javascript" src="<?=search::searchExe("do_prototype_w3c.js", 1)?>"></script>
<script type="text/javascript" src="<?=search::searchExe("do-dogexchangeinvoke_w3c.js", 1)?>"></script>

<script type="text/javascript">
function DisableRightClick() {return false;}
document.oncontextmenu = DisableRightClick;
document.title="<?=$_libel_menu?>";

lienDogexchange = "<?=$dogExchange?>";
socConnexion = "<?=$c_codsoc?>";
prgRechercheContrat = "<?=search::searchExe("ar_contrat_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>";

//On r�cup�re en js les variables d'ent�te du tableaux GridBons
var strfiltreGridBons 			= "<?=$strfiltreGridBons?>";

var strenteteGridBons 			= "<?=$strenteteGridBons?>";
var strcolwidthGridBons 		= "<?=$strcolwidthGridBons?>";
var strcolalignGridBons 		= "<?=$strcolalignGridBons?>";
var strcolformatGridBons 		= "<?=$strcolformatGridBons?>";
var strcollibelGridBons 		= "<?=$strcollibelGridBons?>";

//On r�cup�re en js les variables d'ent�te du tableaux GridEclat
var strfiltreGridEclat 			= "<?=$strfiltreGridEclat?>";
var strenteteGridEclat 			= "<?=$strenteteGridEclat?>";
var strcolwidthGridEclat 		= "<?=$strcolwidthGridEclat?>";
var strcolalignGridEclat 		= "<?=$strcolalignGridEclat?>";
var strcolformatGridEclat 		= "<?=$strcolformatGridEclat?>";
var strcollibelGridEclat 		= "<?=$strcollibelGridEclat?>";

//On r�cup�re en js les variables d'ent�te du tableaux GridExec
var strfiltreGridExec 			= "<?=$strfiltreGridExec?>";
var strenteteGridExec 			= "<?=$strenteteGridExec?>";
var strcolwidthGridExec 		= "<?=$strcolwidthGridExec?>";
var strcolalignGridExec 		= "<?=$strcolalignGridExec?>";
var strcolformatGridExec 		= "<?=$strcolformatGridExec?>";
var strcollibelGridExec 		= "<?=$strcollibelGridExec?>";

// Pte 13/10/2016 Si Y = Multi cereales dans la cellule (734)
var ccMultiCer                  = "<?=$_multicer?>";
// Pte 12/04/2016 Si Y = Affichage bouton calendrier (333)
var ccCalend                    = "<?=$_calend?>";

//CONSTANTES pour les onglets
var POSLEFTFIRSTONGLET 			= 5;
var POSTOPONGLETINACTIF 		= 5;
var WIDTHTETEONGLET 			= 120;
var NBONGLETS 					= 5;
var CLASSONGSELEC 				= "divArrondiDeg16";
var CLASSONGNONSELEC 			= "divArrondiDeg21";

//variables globales
    RspCodcer  					= [];
var RspOperat 					= [];
var RspLiv 						= [];
var RspCellule					= [];
var RspCoderMvt					= [];
var RspCoder					= [];
    RspCodmvt					= [];
var RspDate						= [];
var RspVoe						= [];
var RspMagasi					= [];
var RspMagas2					= [];
var RspMvt						= [];
var RspContrat					= [];
var RspRotation					= [];
var RspCerMvt					= [];
var RspCermvtReciproque 		= [];
    RspContratProd				= []; //Rsp du DICT�cer-contrat-fil
var RspNumContrat				= []; //Rsp du DICT�cer-contrat
var RspLots						= [];
var RspContratEclat				= [];
var RspNumOrdre					= [];
var RspCtrOrdre                 = [];

//grids
var gridEclat 					= new AW.Grid.Extended;
var gridExec					= new AW.Grid.Extended;
var dataGridEclat 				= [];
var dataGridExec 				= [];
var dataPdsNormes 				= [];

//on recupere le type de contrat
var typcontrat					='<?=$_typctr?>';
var campagneSaisie 				= "";

var isSaisieEnCours 			= false;
var isCreation					= false;
var topnext						= true;
var topavoir					= false;
var currow 						= -1;
var currowExec					= "";
var currow2						= "";
var toppds						= "";
var soldevide 					= [];
var topF10						= false;
var topFirst					= true;
var topSuppression 				= false;
var topBascule 					= false;
var anccontrat					= "";
var isCreation3					= false;
var isSaisieEnCours3			= false;
var currow3						= "";
var IsCtrlCaract 				= false;
var IsCtrlCaract2 				= false;
var doCallBack 					= true;
var validok 					= false;
var retourRowid 				= "";
var ccCodLot    				= "";
var ccTypMvt    				= "";
var RspBons 					= [];
var topVariete 					= false; //top variete pour le F10
var typaux_aut 					= []; // contient la liste des types auxiliaires autoris�s selon le code mouvement
var depasaut 					= 0;
var codauxOrigine 				= "";
var typauxOrigine 				= "";
var poidsOrigineCST 			= 0.0;
var poidsMin 					= 0;
var poidsMax 					= 0;
var poidsCST					= 0;
var topnatcon 					= false;
var okInsert                    = true;

var pdsNormSaisie				= 0;
var dernierMvt                  = false;
var ecartArrondis               = 0;
var eecart               		= 0;

var rowidCours                  = "<?=$_rowid?>";
    codsoc                      = "";
var dateJour                    = "<?=$date_jour?>";
    typctr						= "V";
var cer_chrono                  = "";
var cer_origine                 = "";
var cer_bontrs_1                = "";
var cer_attente                 = "";
var statutBon					= "";
var annulBon					= "";
var nomAutorise					= "";
var dsd_1						= "";
var dsd_2						= "";
var heurePesee_1                = "";
var heurePesee_2                = "";
var datePesee_1					= "";
var datePesee_2					= "";

var bonIssuGenenerationPlusMoins = false;

//si _saitie saisie => ordre de saisie 333
	m_saitie 					= "<?=$_saitie?>".trim().toUpperCase();
	m_ctrprod 					= "<?=$_ctrprod?>".trim().toUpperCase();

//Variables saisie de non-conformit�s
var g_codenc 					= "";
var g_valnc 					= "";
//Champs des onglets
var zonesOnglet1 				= new Array("DICT�no-ordre", "DICT�opecre2", "DICT�codmvt", "DICT�codcer", "DICT�magas2", "DICT�typaux", "DICT�codauxges", "DICT�cer-contrat", "DICT�natcon" , "DICT�cer-contrat-fil", "DICT�e-cellule", "DICT�e-rotation",
											"DICT�typaux_chauff", "DICT�codaux_chauff", "DICT�typtrp-ctr", "DICT�transp", "DICT�transp_libel", "DICT�chauff", "DICT�immatr", "DICT�bl-interne", "DICT�cer-libel", "DICT�codcer-ori",
											"DICT�codeparc", "DICT�comonic","DICT�numlot","DICT�numpesee", "DICT�pdsbrut", "DICT�tare", "DICT�pdsnormes","DICT�cellule",
											"DICT�vide-cellule");

for(var i = 1; i < 15;i++)
{
	zonesOnglet1.push("DICT�val-etat_" + i);
	zonesOnglet1.push("DICT�val-nego_" + i);
}

zonesOnglet1 					= zonesOnglet1.concat(new Array(  "DICT�cervalid", "DICT�nettoyage", "DICT�contenant", "DICT�insect",
													"DICT�datrait", "DICT�traitm", "DICT�dosage", "DICT�protrs_1", "DICT�protrs_2", 
                                                    "DICT�protrs_3", "DICT�date-majo", "DICT�nscelle1", "DICT�nscelle2", "DICT�nscelle3"));

var zonesOnglet2 				= new Array("DICT�top-reprise", "DICT�top-mouture", "DICT�stocke", "DICT�manipule");
var zonesOnglet3Achat 			= new Array("DICT�cer-prix", "DICT�poste-cpta", "DICT�pu-transp", "DICT�datech", "DICT�modreg", "DICT�topmajps", "DICT�manu-majo", "DICT�top-complt");
var zonesOnglet3Vente 			= new Array("DICT�intracom");
var zonesOnglet3Transfert 		= [];
var zonesOnglet4 				= new Array("DICT�cod-def", "DICT�desc-def", "DICT�cause-def", "DICT�trait-def", "DICT�act-def", "DICT�fic-def", "DICT�nofic-def");
var zonesOnglet5				= new Array("");

var zonesAdresse 				= new Array("DICT�cod-adrliv", "DICT�adr-liv_1", "DICT�adr-liv_2", "DICT�adr-liv_3", "DICT�adr-liv_4", "DICT�adr-liv_5");

var zonesMulti 					= [];
for(var i = 1; i < 13; i++)
	zonesMulti.push("DICT�cellule_" + i, "DICT�pdsnet_" + i, "DICT�vide-cellule_" + i);

var zonesMotif 					= new Array("DICT�cod-def1", "DICT�desc-def1", "DICT�cause-def1", "DICT�trait-def1", "DICT�act-def1", "DICT�fic-def1", "DICT�nofic-def1", "DICT�motif",
											"DICT�typ-motif", "DICT�codcer-ret", "DICT�cellule-ret");

var zonesMotif2 				= new Array("DICT�datsai");
var zonesTicket 				= new Array("DICT�ticket");
var zonesRechbon 				= new Array("DICT�num-ctr");
var zonesDatBon 				= new Array("DICT�datbon", "DICT�heubon");
var zonesDatRec 				= new Array("dateRec", "heureRec");

//Zones recherche mouvements
var zonesRecherche 				= new Array("DICT�magasi", "DICT�cer-datsai", "DICT�campagne", "DICT�attente");
var TabFocusEntree 				= zonesRecherche.concat(zonesOnglet1, zonesOnglet2, zonesOnglet3Achat, zonesOnglet3Vente, zonesOnglet3Transfert, zonesOnglet4, zonesOnglet5,
									zonesAdresse, zonesMulti, zonesMotif, zonesMotif2, zonesTicket, zonesRechbon, zonesDatBon, zonesDatRec);

var clotureContrat 				= false;
var g_doFocus 					= true;

//Mode visu
var visu 						= false;
if("<?=$_VISU?>".toUpperCase() == "YES")
	visu = true;

var f10Contrat;
if("<?=$_f10contrat?>" != "")
	f10Contrat = true;
else
	f10Contrat = false;

//Pour le controle famille/sous famille cereale/produit cellule
var nouvelleRotation 			= false;

var qteContratSaisie 			= -1;
var rspContratsEclat 			= [];

/** ECLATEMENT **/
var bonOri 						= "<?=$_mvtOri?>"; //=> Appel param�trage(PHP)
var eclatementSPE;
if(bonOri != "")
	eclatementSPE = true;
else
	eclatementSPE = false;
var listePdsBrutEclat			= []; 	 //Liste des poids brut du grid eclatementSPE
var contratEclatOK				= true;
var modifBon					= false; //TRUE => On modif un mouvement qui a d�j� �t� g�n�r�.
var oldPdsNorme					= 0; 	 //oldPdsNorme = pdsNorme mouvement avant une possible modif de son poids (ex: via un changement de carract.)
var oldContratCer				= ""; 	 //oldContratCer = Contrat c�r�al avant modif.
var deltaQteModifBon			= 0;
var eclatOrge					= false  //Si contrat sur orge ET saisie calibrage => eclatOrge = TRUE

/** CARACTERISTIQUE AFFECTATION ORGE **/
var carOrge 					= "<?=$_codeCar?>"; //=> Appel param�trage(PHP)
var carOrgeSpe;
(carOrge != "")? 				carOrgeSpe = true : carOrgeSpe = false;		//carOrge != "" => carOrge = true
var posCalSpe					= -1;	//Position calibrage dans carract�riqtiques produit.

var cerChrono1erMvt				= "";

var saiparc						= "";

var saveCarract					= []; //Vecteur pour sauvegarder les caracteristiques avec leurs valeures / positions.
var oldCodcer					= ""; //Codcer en cours de saisie

var debug      = false;
if ("<?=$debugActif?>" == "oui") debug = true;
function trace(text)
{


	try{
	if(typeof text == "object")
	{
		console.log(text)
		// console.table(text)
	}
	else
		console.log(text);
	}
	catch(err){
	}
}

function setFieldValue(chaine,motcle,valeur)
{
	var chaineSplite = chaine.split('�');
	var trouve = false
	for(var i=0; i<chaineSplite.size(); i++){
		chaineSplite[i] = chaineSplite[i].split('�');
		if(chaineSplite[i][0].toUpperCase() == motcle.toUpperCase())
		{
			chaineSplite[i][1] = valeur;
			trouve = true;
		}
		chaineSplite[i] = chaineSplite[i].join('�');
	}
	
	if(!trouve)
		chaineSplite.push(motcle+"�"+valeur);
	
	chaineSplite = chaineSplite.join('�');
	return chaineSplite;
}

function initScreen()
{
	
	
	
    // Affichage du bouton calendrier
    if (ccCalend == "Y")
        $("btn_datheu").show();
    else
        $("btn_datheu").hide();
    
    // Si eclattement standard => cache icones ajout/suppr
	if(!eclatementSPE)
	{
		$('btn_newEclat').hide()
		$('btn_supEclat').hide()
	}
	
	//Arguments menu pour les onglets
	if("<?=$_nopresta?>".trim().toUpperCase() == "O")
		$("ong2").hide();
	if("<?=$_noinfo?>".trim().toUpperCase() == "O")
		$("ong3").hide();
	
	gestionParamDernierCharg();
	
	loadFocus();
	document.onkeydown = bodyKeyDown;
	initOnglets("divOnglets", POSTOPONGLETINACTIF, POSLEFTFIRSTONGLET, WIDTHTETEONGLET, NBONGLETS, CLASSONGSELEC, CLASSONGNONSELEC);
	
	loadOnglet(1);
	
	gridBons.init();
	gridEclat.init();
	gridExec.init();
	
	$("DICT�codmvt").value="<?=$_codmvt?>";
	
	if("<?=$_eclat?>" != "")
		$("bt10").show();

	if("<?=$_clotctr?>".trim().toUpperCase() == "O")
		clotureContrat = true;
	
	if("<?=$_rotation?>".toUpperCase() != "O")
		zonscreenget("�rotation��2");
	
	if("<?=$rspoperat?>" != "")
	{
		RspOperat = []
		RspOperat[0] = "OK"
		RspOperat[1] = Entry(2,"<?=$rspoperat?>","~")
		Rsp = RspOperat.clone();
		$("DICT�magasi").value = getValeurRsp("MAGASI");
		$("DICT�magasi_libel").value = getValeurRsp("LIBEL", "MAGASI");
	} 
	else 
	{
		var inputParameter = "CODSOC�<?=$c_codsoc?>�OPERAT�<?=$opebase?>";
		var retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , "GET-OPERAT�n-tabges" , "" , "" );
		RspOperat = Rsp.clone();
	}

	//si operateur depot le topvalidation est invisible
    if(getValeurRsp('KES-OPMAIT') == "N")
		zonscreenget("�cervalid��2");
	
	enableZone("ALL", false);
	enableZone(zonesRecherche.join("|"), true);
	
	//on charge si besoin un bon au demarrage
	if("<?=$_rowid?>"!="" && topFirst==true)
        bonrechercher();
	
	try{
	setFocus(TabFocusEntree, "FIRST");
	// console.timeEnd("Fin init screen");
	}catch(err){}
	// trace("---Fin chargement ecran");
	// try{
	// console.timeEnd("initScreen");
	// }catch(err){}
	
	if("<?=$_s?>"!=""){setInterval(function(){ fichierS(); }, 15000);}
	
}

/**
	Recherche les informations du contrat pour la dupliation
**/
function bonContrat(ctr){
	trace("--bonContrat ctr: "+ctr+" --")
	
	var contrat;
	if(ctr != undefined && ctr != "")
		contrat = ctr;
	else if($("DICT�num-ctr").value != "")
		contrat = $("DICT�num-ctr").value;
	else 
		return;
	
	var dogFct = "get-cerctr�a-cerctr"
	var requete = "CODSOC�<?=$c_codsoc?>�TYPCTR�V"+"�CER-CONTRAT�"+contrat;
	
	var retourExchange = sendExchangeInvoke("<?=$dogExchange?>",requete,dogFct,"","") 
	if(retourExchange == 'OK')
	{
		RspNumContrat = Rsp.concat();
		$('DICT�typaux').value = getValeurRsp('TYPAUX');
		$('DICT�codauxges').value = getValeurRsp('CODAUXGES');
		$('DICT�cer-contrat').value = getValeurRsp('CER-CONTRAT');
		$('DICT�codcer').value = getValeurRsp('CODCER');
		$('DICT�codmvt').value = "<?=$_mvtvte?>";
		
		dogexchange($('DICT�codmvt'),'GET');
		dogexchange($('DICT�codcer'),'GET');
		dogexchange($('DICT�codauxges'),'GET');
		
		typctr = "V";
		$('DICT�cer-contrat').defaultValue = ""
		dogexchange($('DICT�cer-contrat'),"GET");
		
		$('RECHBON').hide();
	}
	else 
	{
		alert(Rsp[1]);
		return;
	}
}

function fichierS()
{
	var pl = new SOAPClientParameters();
	var dirS = "pesee/"+$("DICT�magasi").value.trim()+"/";
	var ret = SOAPClient.invoke("<?=$fichierS?>&_dir=<?=$xcspoolPath?>"+ dirS +"&", "", pl, false);
	if(ret!=""){
		$('ctldeal').downloadAndOpenFile("<?=$xcspoolRelPath?>" + dirS + ret)
		SOAPClient.invoke("<?=$fichierS?>&_kill=<?=$xcspoolPath?>"+ dirS + ret + "&", "", pl, false);
	}
}

function setInverseFocus2 ( TabFocusEntree , strId )
{
	OkFocus = 0
	key = TabFocusEntree.indexOf(strId);
	if(key != -1)
		OkFocus = 1;
	if ( key != -1 )
	{
		for ( z = parseInt ( key ) - 1 ; z >= 0 ; z-- )
		{
			if ( OkFocus == 1 )
			{
				if ( IsExiste ( TabFocusEntree[z] ) == "0"
					|| $(TabFocusEntree[z]).style.visibility == "hidden"
					|| $(TabFocusEntree[z]).parentElement.style.visibility == "hidden"
					|| $(TabFocusEntree[z]).parentElement.parentElement.style.visibility == "hidden"
					|| $(TabFocusEntree[z]).parentElement.parentElement.parentElement.style.visibility == "hidden"
					|| $(TabFocusEntree[z]).style.display == "none"
					|| $(TabFocusEntree[z]).parentElement.style.display == "none"
					|| $(TabFocusEntree[z]).parentElement.parentElement.style.display == "none"
					|| $(TabFocusEntree[z]).parentElement.parentElement.parentElement.style.display == "none"
					|| $(TabFocusEntree[z]).type.toUpperCase() == "HIDDEN"
				) {
					strId = TabFocusEntree[z]
					OkFocus = 0
				}
				else if ( initId == "FIRSTVIDE" && $(TabFocusEntree[z]).value.trim() != "" )
				{
					strId = TabFocusEntree[z]
					OkFocus = 0
				}
			}
			//rajout test opacity
			var tbe = $(TabFocusEntree[z])
			while (tbe && tbe !== document.body && tbe !== document.documentElement) {
				if((tbe.style.opacity!="" && tbe.style.opacity!=undefined)||tbe.style.visibility=="hidden"||tbe.style.display=="none"){
					OkFocus = 0
					break;
				}
				tbe = tbe.parentNode
			}

			if ( OkFocus == 1 )
			{
				if ( $(TabFocusEntree[z]).tagName.toUpperCase() == "SELECT" )
				{
					if ( $(TabFocusEntree[z]).disabled == false )
					{
						try{$(TabFocusEntree[z]).focus()}catch(ex){}
						return true
					}
				}
				else
				{
					if ( $(TabFocusEntree[z]).type.toUpperCase() == "TEXT" )
					{
						if ( $(TabFocusEntree[z]).readOnly == false )
						{
							try{$(TabFocusEntree[z]).select()}catch(ex){}
							return true
						}
					}
					else
					{
						if ( $(TabFocusEntree[z]).disabled == false )
						{
							try{$(TabFocusEntree[z]).focus()}catch(ex){}
							return true
						}
					}
				}
			}
			if ( TabFocusEntree[z] == strId )
				OkFocus = 1
		}
	}
}

function bodyKeyDown(e)
{
    if (document.all)
    {
        keycode 	= event.keyCode;  // IE
        ctrlkey 	= event.ctrlKey;
        shiftkey 	= event.shiftKey;
        strId 		= event.srcElement.id;
    }
    else
    {
        keycode		= e.which;        // FIREFOX
        ctrlkey 	= e.ctrlKey;
        shiftkey 	= e.shiftKey;
        strId 		= e.target;
        strId 		= strId.id;
    }
    //Selon la touche
    switch ( keycode )
    {
        case 9	 : // TAB
        case 13  : // Entr�e
        case 121 : // F10
            <?=$c_returnValue?>
            if ( strId == "" ) return false;
            try
			{
                natureId	= $(strId).getAttribute("NATURE");
                obliId		= $(strId).getAttribute("OBLI");
            } catch ( eXception) {}
            if ( keycode == 9 && shiftkey )
            {
                setInverseFocus ( TabFocusEntree , strId );
                return;
            }
            if ( natureId == "SEARCH" )
            {
                trig = "GET";
                if ( keycode == 121 ) trig = "F10";
                dogexchange ( $(strId) , trig );
            }
            else if(strId.indexOf("-cell-")==-1)
				if(keycode==13 || keycode == 9)
				{
					switch(strId)
					{
						default :
							validInput($(strId), obliId);
						break;
					}
				}
            break;
        case 27 : //Echap
			if($("ECLATEMENT").style.display != "none")
				return false;
			if($("MULTI").style.display != "none")
			{
				sortieMulti();
				return false;
			}
			if(gridBons.getRowCount() > 0)
				if(isSaisieEnCours)
					razEcran(false);
				else
					echap();
			else
			{
				if(isSaisieEnCours)
					razEcran(false);
				else
					echap();
			}
            // if ( window.parent.ongActif == undefined ) window.close();
            // else window.parent.closeOng ( window.parent.ongActif );
            break;
        case 113 ://f2
			if($("divSaisieDateHeureTransfert").style.display != "none")
			{
				validerDateRec();
				return;
			}
			if($('MULTI').style.display != "none")
			{
				validMulti();
			}
			else if($("EXECUTION").style.display != "none")
			{
				validExecution();
			}
			else
			{
				bonvalider();
			}
			// else if($("").style.display == "none")
			// {
				// valid
			// }
            break;

        case 115 : //F4
            adrliv();
            break;

		case 117 : //f6
			// <?=$c_returnValue?>

			break;
        case 118 ://f7
			// newEnreg();
            break;

		case 120: //f9
			// if($("ECLATEMENT").style.display==""){
				// //inserer une ligne
				// // addemptyrow3();
				// return
			// }
			if(strId=="DICT�magasi")
			{
				$("DICT�magasi").defaultValue="";
				dogexchange($("DICT�magasi"), "GET", false)
			}
			boninserer();
			break;
    } //Selon la touche
}

var doSuite = true;
function validInput ( Obj , obli , suite)
{
	trace("--validInput - "+Obj.id+" --")
	
	if(suite != undefined)
		doSuite = suite;
	else
		doSuite = true;
    //ctrlIdValue => function de premier controle de la valeur saisie en fonction de la nature du champ (DATE,STRING,NOMBRE,CHAMP.....)
    if ( !ctrlIdValue ( Obj.id ) )
    {
    	// alert("<?=lect_bin::getLabel("valnum", "sl")?>");
    	Obj.focus();
    	return false;
    }
    switch ( Obj.id )
    {
		case "DICT�campagne" :
			campagneSaisie = $(Obj.id).value.trim();
		break;
		case "DICT�cer-datsai":
			var today = "<?=$date_jour_en?>";
			var cerdatsai = Obj.value.split("/");
			cerdatsai = cerdatsai[1] + "/" + cerdatsai[0] + "/" + cerdatsai[2];
			today = new Date(today);
			cerdatsai = new Date(cerdatsai);
			if(cerdatsai > today)
			{
				alert("La date du bon ne doit pas �tre sup�rieure � celle du jour !")
				Obj.value = "";
				Obj.defaultValue = "";
				Obj.focus();
				return false;
			}

		break;
		case "DICT�protrs_3" :
			bonvalider();
            return true;
		break;

		case "DICT�manipule" :
			loadOnglet(3);
			return true;
		break;

		case "DICT�top-complt" :
			loadOnglet(4);
			return true;
		break;

		case "DICT�nofic-def" :
			loadOnglet(5);
			return true;
		break;

        case "DICT�pdsbrut":
	    if($("DICT�pdsbrut").getAttribute("buffer2") && $("DICT�pdsbrut").getAttribute("buffer2") != "" &&$("DICT�pdsbrut").getAttribute("buffer2")!=$("DICT�pdsbrut").value){
	    	if(confirm("Voulez-vous �craser la donn�e du pont bascule")){
			dsd_1 = ""
			datePesee_1 = ""
			heurePesee_1 = ""
	    	}else{
	    		$("DICT�pdsbrut").value=$("DICT�pdsbrut").getAttribute("buffer2")
	    	}
	    }
            if (ccTypMvt != "2") {
                if(!ctrlpoids())
			    	return false;
            }
            else ctrlpoids(false,"valid") ;
	    break;

        case "DICT�tare" :
			if(Obj.value.trim() == "")
				Obj.value = "0.00";
			if($("DICT�pdsbrut").getAttribute("buffer2") && $("DICT�tare").getAttribute("buffer2") != "" &&$("DICT�tare").getAttribute("buffer2")!=$("DICT�tare").value)
			{
				if(confirm("Voulez-vous �craser la donn�e du pont bascule?"))
				{
					dsd_2 = ""
					datePesee_2 = ""
					heurePesee_2 = ""
				}
				else
				{
					$("DICT�tare").value=$("DICT�tare").getAttribute("buffer2")
				}
			}
			if (ccTypMvt != "2")
			{
				if(!ctrlpoids())
					return false;
			}
			else
				ctrlpoids(false,"valid")
			if($("DICT�cellule").value.trim() != "")
				ctrlcapacite() ;
	    break;

		case "DICT�attente" :
			bonrechercher();
		break;

		case "DICT�val-etat_1":
		case "DICT�val-etat_2":
		case "DICT�val-etat_3":
		case "DICT�val-etat_4":
		case "DICT�val-etat_5":
		case "DICT�val-etat_6":
		case "DICT�val-etat_7":
		case "DICT�val-etat_8":
		case "DICT�val-etat_9":
		case "DICT�val-etat_10":
		case "DICT�val-etat_11":
		case "DICT�val-etat_12":
		case "DICT�val-etat_13":
		case "DICT�val-etat_14":
			trace("	VALIDINPUT - " + Obj.id)		
		
			var index = parseInt(Obj.id.slice(-1));
			if(isSaisieEnCours)
			{
				var oldPds = parseFloatStr($('DICT�pdsnormes').value);
				var retourTestCaract = CtrlSaisieValCaract(Obj,index,false)
								
				if(retourTestCaract == false)
					return false;
				
				ctrlpdsnormes();

				if(getField(RspCodmvt[1], "TYPMVT") != "1")
				{
					
					$("DICT�solde-vide").value = parseFloatStr($("DICT�stock-cel").value.trim()) - parseFloatStr($("DICT�pdsnormes").value.trim());
					$("DICT�solde-vide").value = parseFloatStr($("DICT�solde-vide").value).toFixed(3)
				}
				else 
				{
					$("DICT�solde-vide").value = parseFloatStr($("DICT�stock-cel").value.trim()) + parseFloatStr($("DICT�pdsnormes").value.trim());
					$("DICT�solde-vide").value = parseFloatStr($("DICT�solde-vide").value).toFixed(3)
				}
				
				if(parseFloatStr($('DICT�pdsnormes').value) != oldPds)
					ctrlQtePdsNormes();
				
				//On sauvegarde la valeur dans la liste.
				sauvegardeCaracteristiques();
			}
			// return true;
		break;

		case "DICT�val-nego_1":
		case "DICT�val-nego_2":
		case "DICT�val-nego_3":
		case "DICT�val-nego_4":
		case "DICT�val-nego_5":
		case "DICT�val-nego_6":
		case "DICT�val-nego_7":
		case "DICT�val-nego_8":
		case "DICT�val-nego_9":
		case "DICT�val-nego_10":
		case "DICT�val-nego_11":
		case "DICT�val-nego_12":
		case "DICT�val-nego_13":
		case "DICT�val-nego_14":
			var oldPds = parseFloatStr($('DICT�pdsnormes').value)
			
			var index = parseInt(Obj.id.slice(-1));
			if(isSaisieEnCours)
				CtrlSaisieValCaract(Obj,index,true);
			
			if(parseFloatStr($('DICT�pdsnormes').value) != oldPds)
					ctrlQtePdsNormes();
							
			sauvegardeCaracteristiques();
		break;

		case "DICT�adr-liv_5":
			ctrladr();
		break;
		case "DICT�datbon" :
			var dateBon = Date.parse(Obj.value.slice(3,5) + "/" + Obj.value.slice(0,2) + "/" + Obj.value.slice(6,10));

			var today = new Date();

			if (dateBon > today)
			{
				alert("<?=lect_bin::getLabel("mess-datpiesup", "sl") . " " . $date_jour?>");
				Obj.focus();
				Obj.select();
				return false;
			}
		break;
		case "dateRec" :
			Obj.defaultValue = Obj.value;
		break;
		case "heureRec":
			if(formatHeure(Obj, true))//Format ok
			{
				Obj.defaultValue = Obj.value;
				return validerDateRec();
			}
			return false;
		break;

		case "DICT�heubon" :
			if(Obj.value.trim()==""&&$('DICT�datbon').value.trim()!="")return false
			Obj.value = Obj.value.trim();
			var regNb = new RegExp("[0-9]{2,6}", "gi");
			if(regNb.test(Obj.value))
			{
				if(Obj.value.length == 2)
					Obj.value = Obj.value + ":00:00";
				else if(Obj.value.length == 4)
					Obj.value = Obj.value.slice(0,2) + ":" + Obj.value.slice(2,4);
				else if(Obj.value.length == 6)
					Obj.value = Obj.value.slice(0,2) + ":" + Obj.value.slice(2,4) + ":" + Obj.value.slice(4,6);

			}
			if(Obj.value.length == 5 && Obj.value.charAt(2) == ":")
				Obj.value += ":00";

			var regHeu = new RegExp("^([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$", "gi");
			if(!regHeu.test(Obj.value) && Obj.value != "")
			{
				alert("<?=lect_bin::getLabel("mess-heure_1", "sl")?>");
				Obj.focus();
				Obj.select();
				return false;
			}
			else
			{
				if($("DICT�datbon").value.trim() == "<?=$date_jour?>")
				{
					var date = new Date();
					var heure_expl = Obj.value.split(":");
					var saisie = new Date(date.getFullYear(), date.getMonth(), date.getDate(), parseInt(heure_expl[0]), parseInt(heure_expl[1]) , parseInt(heure_expl[2]) );
					if( saisie > date)
					{
						alert("<?=lect_bin::getLabel("mess-heure_2", "sl")?>");
						Obj.focus();
						Obj.select();
						return false;
					}
				}
				if(!doSuite)
					return true;
				$("DICT�cellule").defaultValue = "";
				dogexchange($("DICT�cellule"), "GET", true);
				$("DATHEUBON").hide();
			}
		break;
		case "DICT�transp_libel2":
			$("DICT�dest-fin2").focus();
			return;
			break;
		case "DICT�dest-fin2":
			validationPeniche();
		
			/* $('TRANSP').style.display='none';
			$('DICT�dest-fin').value=$('DICT�dest-fin2').value;
			$('DICT�transp_libel').value=$('DICT�transp_libel2').value;
			setFocus(TabFocusEntree, 'DICT�transp_libel'); */
			return;
			break;
    }
    setFocus ( TabFocusEntree , Obj.id );
    return true;
}

function validerDateRec()
{
	if($("dateRec").value.trim() != $("dateRec").defaultValue.trim() || $("dateRec").value.trim() == "")
	{
		$("dateRec").focus();
		return false;
	}
	else if($("heureRec").value.trim() != $("heureRec").defaultValue.trim() || $("heureRec").value.trim() == "")
	{
		$("heureRec").focus();
		return false;
	}
	if(!validInput($("dateRec")))
	{
		$("dateRec").focus();
		return false;
	}
	if(!formatHeure($("heureRec"), true))
	{
		$("heureRec").focus();
		return false;
	}
	$("divSaisieDateHeureTransfert").hide();
	setFocus(TabFocusEntree, "DICT�magas2");
	return true;
}

function parseFloatStr(sString) {
	sString = (""+sString).trim()
	if(sString=="")return 0;
	try{
		return parseFloat(sString.trim());
	} catch(ex){
		return 0;
	}
}

function getColBons(champ)
{
	return getFieldCol(strfiltreGridBons, champ);
}

function getColExec(champ)
{
	return getFieldCol(strfiltreGridExec, champ);
}

function getColEclat(champ)
{
	return getFieldCol(strfiltreGridEclat, champ);
}


function getValue(motclef, jointure, master, input, keepRsp)
{
	var retour = "";
	var jointure = jointure || false;
	var master = master || "";
	var input = input || "";
	var keepRsp = keepRsp || true;
	if(input.trim() != "")
	{
		retour = setValueFromKeyword(motclef, jointure, master, input, keepRsp);

		if($(input) == null)
		{
			// if(debug)
				// trace("/!\\ - " + input + " Inexistant !");
		}
		else
		{
			try
			{
				$(input).defaultValue = $(input).value;
			}catch(err){trace("ERROR = " + err)}

		}
	}
	else
		retour = setValueFromKeyword(motclef, jointure, master, "", keepRsp).trim();
	return retour;
}

function dogexchange(myobj,trig, callback)
{	
	if(callback == undefined)
		callback = true;
	
	// trace("--- dogexchange, pour " + myobj.id + " avec le trigger " + trig + " et callback vaut " + callback);
	
	try
	{
		backupCB = doCallBack;
		
		if(callback == false)
			doCallBack = false;
		else
			doCallBack = true;
	
		//Initialiser les variables d'option des �changes
		var trigger 		= "";
		var strId 			= "";
		var filtre			= "";
		var largeurGridF10 	= "";
		var inputParameter 	= "";
		var dogFonction		= "";

		if(IsSendingExchange)
		{
			try {
				Event.stop(event);
			} catch(ex) {};
			return;
		}
		
		if ( trig != "" && trig != undefined )
		{
			trigger = trig;
			strId   = myobj.id;
			if ( $(strId).readOnly == true) return ;
		}
		else
		{
			//Tester le type de l'objet qui est � l'origine
			if (myobj.getAttribute("F10") != null)
			{
				//Affecter l'dientifiant
				strId = myobj.getAttribute("F10")
				//Indiquer le trigger
				trigger = "F10";
				//Tester s'il n'est pas en ReadOnly
				if ( $(strId).readOnly == true) return ;
			}
			else
			{
				// Ne rien faire si la zone est en lecture seule
				if ( myobj.readOnly == true ) return ;
				//Affecter l'identifiant
				strId = myobj.id ;
			}

			//D�terminer le trigger
			if ( trigger == "")
			{
				switch ( keycode)
				{
					case 9	:
					case 13	: //Touche entr�e
						trigger = "GET" ;
						break;
						
					case 121: //touche F10
						trigger = "F10" ;
						break;
				}
			}
		}
		
		strIdS = strId.split("-");
		isGrid =false;
		
		if(strIdS[0] =="tabGridEclat")
			isGrid = true;
		
		oldId 		= strId;
		oldTrigger 	= trigger;
		
		//R�initalisation des zones avant les contr�les si besoin
		// trace("strId == " + strId + ", value = " + $(strId).value + ", defaultValue : " + $(strId).defaultValue);
		
		switch (strId)
		{
			case "DICT�cellule":
				if(trigger == "GET" && $(strId).value.trim() != $(strId).defaultValue.trim() && !isGrid )
				clearZone("DICT�rotation|DICT�rot-produit|DICT�rot-produit_libel|DICT�capacite|DICT�stock-cel");
				break;
				
		}

		if($(strId).value.trim() == "" && $(strId).getAttribute("obli") == "1" && trigger == "GET" && !isGrid)
		{
			// trace("	==> value == blanc et obli == 1 et trigger = get");
			
			if($(strId +"_libel") != undefined && $(strId +"_libel") != null)
				$(strId +"_libel").value = "";
			else
				$(strId).title = "";
			
			doCallBack = backupCB;

			if(strId == 'DICT�natcon')
				$('DICT�natcon').title = "Ordre de commercialisation";

			return;
		}

		if(trigger == "GET" && $(strId).value.trim() == $(strId).defaultValue.trim() && !isGrid )
		{
			// trace("triger == GET et value == defaultValue ");
			// trace($(strId +"_libel"));
			
			if($(strId).value.trim() == "")
			{
				if($(strId +"_libel") != undefined && $(strId +"_libel") != null)
					$(strId +"_libel").value = "";
				else
					$(strId).title = "";

				switch(strId)
				{
					case "DICT�cer-contrat" :
						// trace("	==> [NUMERO CONTRAT] - contrat d�j� saisi ==> enable NATURE CONTRAT | recherche NATURE CONTRAT")
					
						enableZone("DICT�natcon" , true);
						$("DICT�natcon").value = "";
						$("DICT�natcon").defaultValue = "";
						str_val = "CODSOC�<?=$c_codsoc?>" + "�CODCER�"+$("DICT�codcer").value.trim()  + "�CODMVT�"+$("DICT�codmvt").value.trim()  + "�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�DATSAI�" + $("DICT�cer-datsai").value;
						
						if(getField(RspCodcer[1],'COLLECTE') != "")
							str_val += "�COLLECTE�" + getField(RspCodcer[1],'COLLECTE');
						else
							str_val += "�COLLECTE�O";
							
						retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", str_val, "f10-option�a-tabcer", "", "");
						
						if(retourExchange=="OK")
							getValue("CODTAB", false, "", "DICT�natcon",true);
						break;
						
					case "DICT�cer-contrat-fil":
						if($('DICT�codcer').value.trim()=="")
						{
							// trace("	==> [CONTRAT PROD] - contrat d�j� saisi ==> PAS CODCER ==> focus sur codcer")
							
							$("DICT�codcer").focus();
							return;
						}
						break;
				}

				if(!callback || !g_doFocus)
				{
					doCallBack = backupCB;
					return true;
				}
				
				setFocus(TabFocusEntree, strId);
				doCallBack = backupCB;
				return true;
			}
			else
			{
				switch(strId)
				{
					case "DICT�codauxges":
						try{$("DICT�cer-contrat").focus()}catch(ex){};
						
						if(f10Contrat && $('DICT�cer-contrat').parentElement.style.visibility!="hidden")
						{
							// trace("DOGEXCHANGE codauxges f10Contrat")
							
							Rsp = RspCodmvt.concat();
							
							inputParameter = "CODSOC�<?=$c_codsoc?>�TYPCTR�"+getValue("TYPCTR",false ,"","",true)+"�CLOTURE�0"+"�TYPAUX�"+$("DICT�typaux").value+"�CODAUXGES�"+$("DICT�codauxges").value+"�CODCER�"+$("DICT�codcer").value+"�CAMPAGNE�"+$("DICT�campagne").value+"�CODMVT�"+$("DICT�codmvt").value+"�CERGPMT�"+$("DICT�codcer-ori").value+"�PARAM�N�FILIERE�N�PROG�ASBONAPP�DATMVT�" + $("DICT�cer-datsai").value;
                            dogFonction    = "F10-cerctr�a-cerctr";
							retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");
							
                            // si il y a des contrats on ouvre la recherche
							if (retourExchange == "OK" )
								dogexchange($("DICT�cer-contrat"),"F10", false);
							return;
						}
						break;
				}
			}
			
			if(!callback || !g_doFocus)
			{
				doCallBack = backupCB;
				return true;
			}
			
			setFocus(TabFocusEntree, strId);
			doCallBack = backupCB;
			return true;
		}
		
		if(trigger == "GET" && $(strId).value.trim() == "" && !isGrid)
		{
			if($(strId +"_libel") != undefined && $(strId +"_libel") != null)
				clearZone(strId +"_libel");
			else
				$(strId).title = "";
			
			switch(strId)
			{
				case "DICT�cer-contrat" :
					enableZone("DICT�natcon" , true);
					clearZone('DICT�natcon');
					$("DICT�natcon").defaultValue = "";
					break;
			}
		}
	}//fin try
	catch(err){trace("erreur dans le dogexchange avant le switch : " + err + " / " + strId + " / " + $(strId).tagName)}

    switch (strId)
	{
        case "DICT�cellule":
            $(strId).title = "";
			$(strId).defaultValue = "";
            break;
			
		case "DICT�codcer" :
			$(strId).title = "";
			oldCodcer = $(strId).defaultValue;
			$(strId).defaultValue = "";
			break;
			
		case 'DICT�codauxges':
			$(strId).title = "";
			break;

		default:
			if(!isGrid)
			{
				$(strId).title = "";
				$(strId).defaultValue = "";
			}
			break;
	}
		
	switch (strId)
	{
		case "DICT�protrs_1" :
		case "DICT�protrs_2" :
        case "DICT�protrs_3" :
            
            if(trigger=="AUTO")
			{
				dogFonction = "f10-tabgen�a-tabgen";
				inputParameter = "CODSOC�<?=$c_codsoc?>�MOTCLE�DERNCHAR�codtab��libel�" + $(strId).value.trim();
			}
			else 
			{
				dogFonction = trigger + "-tabgen�a-tabgen";
				inputParameter = "CODSOC�<?=$c_codsoc?>�MOTCLE�DERNCHAR�codtab�" + $(strId).value.trim();
			}
			
			largeurGridF10 = 890;
			break;
			
		case "DICT�no-ordre":
			if(trigger == 'GET' && $('DICT�no-ordre').value != "" && RspNumOrdre.size() > 0)
			{
				setFocus(TabFocusEntree, strId);
				return;
			}
		
			dogFonction    = "get-ceroex�a-cerctr";
			inputParameter = "CODSOC�<?=$c_codsoc?>�TYPOEX�VOE�NUMOEX�" + $(strId).value.trim() + "�PROG-ORIGINE�AS_BONAPP�PARAME�0�CODMAG�" + $("DICT�magasi").value.trim();

			if(trigger == "F10")
			{
				//on lance une recherche des contrats
				PhpAuxges = "<?=search::searchExe("ar_numvoe_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_autoload=O&_camp="+$('DICT�campagne').value+"&_magasi="+$("DICT�magasi").value + "&_parame=0" + "&_progorigine=as_bonapp" ;
				sFeatures = "dialogHeight:700px;dialogWidth:1024px;status:no;help:no;scroll:no";
				retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
				if(retourExchange!=undefined)
				{
					//acces execution
					Rsp = retourExchange;
					lerowid = setValueFromKeyword ( "ROWID"  , false , "" , ""  , true)
					retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�ROWID�" + lerowid + "�PARAME�0�PROG-ORIGINE�AS_BONAPP�CODMAG�" + $("DICT�magasi").value.trim(), "get-ceroex�a-cerctr", "", "");
					dogexchangecallback();
					return retourExchange;
				}
			}
		break;

        case "DICT�intracom" :
            if(trigger=="F10")
			{
				dogFonction="f10-intracom�n-auxges"
                // paramFiltreSelection = "FIELD=chrono|intracom|libelle";
				inputParameter   = "�CODSOC�<?=$c_codsoc?>�TYPAUXFAC�" + $("DICT�typaux").value + "�CODAUXFAC�" + $("DICT�codauxges").value.trim() + "�ORIGINE�CONTRAT" ;
			} 
			else 
			{
                if($(strId).value.trim() == "")
					return ;
				
				dogFonction="get-intracom�n-auxges"
                inputParameter  = "�CODSOC�<?=$c_codsoc?>�TYPAUXFAC�" + $("DICT�typaux").value + "�CODAUXFAC�";
				inputParameter += $("DICT�codauxges").value.trim() + "�ORIGINE�CONTRAT�INTRACOM�" + $("DICT�intracom").value;
            }
        break ;

        case "DICT�natcon":
			Rsp=RspCodcer.clone();

			if(getValue("COLLECTE",false ,"","",true)!="" && getValue("COLLECTE",false ,"","",true).substr(0,1)=="N")
				inputParameter = "CODSOC�<?=$c_codsoc?>�COLLECTE�N"+ "�CODCER�"+$("DICT�codcer").value.trim()  + "�CODMVT�"+$("DICT�codmvt").value.trim()  + "�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�DATSAI�" + $("DICT�cer-datsai").value;
			else
				inputParameter = "CODSOC�<?=$c_codsoc?>�COLLECTE�O"+ "�CODCER�"+$("DICT�codcer").value.trim()  + "�CODMVT�"+$("DICT�codmvt").value.trim()  + "�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�DATSAI�" + $("DICT�cer-datsai").value;

			dogFonction = trigger + "-OPTION�a-tabcer";

			if(trigger == 'GET')
				inputParameter += "�CODTAB�"+$('DICT�natcon').value;

			$('DICT�natcon').title = "Ordre de commercialisation";
			break;

		case "DICT�rotation" :
			if($("DICT�codcer").value.trim() == "")
			{
				$("DICT�codcer").focus();
				return false;
			}
			
			dogFonction = trigger + "-rotation�a-rotation";
			inputParameter = "CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $("DICT�cellule").value.trim() + "�CAMPAGNE�" + $("DICT�campagne").value;
			
			if(trigger == "GET")
				inputParameter += "�ROTATION�" + $(strId).value.trim();
			break;
			
		case "DICT�rotation_1" :
		case "DICT�rotation_2" :
		case "DICT�rotation_3" :
		case "DICT�rotation_4" :
		case "DICT�rotation_5" :
		case "DICT�rotation_6" :
		case "DICT�rotation_7" :
		case "DICT�rotation_8" :
		case "DICT�rotation_9" :
		case "DICT�rotation_10" :
		case "DICT�rotation_11" :
		case "DICT�rotation_12" :
			var num = strId.split("_")[1];
			if($("DICT�codcer").value.trim() == "")
			{
				$("DICT�codcer").focus();
				return false;
			}
			
			dogFonction = trigger + "-rotation�a-rotation";
			inputParameter = "CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $("DICT�cellule_" + num).value.trim() + "�CAMPAGNE�" + $("DICT�campagne").value;
			
			if(trigger == "GET")
				inputParameter += "�ROTATION�" + $(strId).value.trim();
			break;
	
		case "DICT�cod-adrliv" :
			inputParameter = "COSOC�<?=$c_codsoc?>�TYPAUX�" + $('DICT�typaux').value + "�CODAUXGES�" + $('DICT�codauxges').value ;
			if(trigger=="F10")
			{
                largeurGridF10 = 500;
				dogFonction    = "F10-AFFADL�n-affadl";
				inputParameter = inputParameter + "�ADRESSE�" + $(strId).value;
			}
			else
			{
				dogFonction="GET-AFFADL�n-affadl";
				inputParameter = inputParameter + "�CHRONO�" + $(strId).value;
			}
			break;

		case "DICT�num-ctr":
			//337 avec le motcl� menu _mvtvte = CCV, si on saisie CCV comme code mouvement pour un nouveau bon, 
			//on peut rechercher un num�ro de contrat.
			if(trigger == "F10")
			{
				//Ouverture de l'�cran ar_contrat pour la recherche des contrats.
                PhpAuxges = "<?=search::searchExe("ar_contrat_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_autoload=O&_typctr=V&_clot=0&_codcer=&_codcerlibel=&_typaux="+$("DICT�typaux").value+"&_codauxges="+$("DICT�codauxges").value+"&_codauxgeslibel="+$("DICT�codauxges_libel").value +"&_cergpmt=&_camp="+$("DICT�campagne").value+"&_codmvt="+$("DICT�codmvt").value+"&_magasi="+$("DICT�magasi").value+"&_filiere=N&_param=N&_prog=asbonapp" + "&_datmvt=" + $("DICT�cer-datsai").value;
				sFeatures = "dialogHeight:690px;dialogWidth:975px;status:no;help:no";
				retourExchange = window.showModalDialog ( PhpAuxges , "" , sFeatures) ;
				
				$('DICT�num-ctr').value = getField(retourExchange[1],'CER-CONTRAT').trim();
				if($('DICT�num-ctr').value.trim()!="")RspNumContrat=["OK",retourExchange[1]]
				bonContrat();
				// if(!ctrlNomSaisi()){
                    // return false;
                // }
				document.activeElement.focus()
				return;
			}
			else
			{
				$('RECHBON').hide();
				bonContrat();
				// if(!ctrlNomSaisi()){
                    // return false;
                // }
				document.activeElement.focus()
				return;
			}			
		case "DICT�variete" :
			inputParameter = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�" + $("DICT�campagne").value + "�CODCER�" + $("DICT�codcer").value + "�MAGASI�" + $("DICT�magasi").value;
			dogFonction = "f10-variete�a-cereal";
			break;
		
		case "DICT�magasi" :
			$(strId).defaultValue = "";
			if(trigger=="F10")
			{
				dogFonction="f10-magasi�n-magasi"
				inputParameter = "�ACTIVI�APP�ACTIF�O�CODSOC�<?=$c_codsoc?>�MAGASI�"+$("DICT�magasi").value;
				filtre="FIELD=magasi|libel|adresse_5|telephone|codsoc-app";
			}
			else
			{
				dogFonction="get-magasi�n-magasi"
				inputParameter = "�ACTIVI�APP�ACTIF�O�CODSOC�<?=$c_codsoc?>�MAGASI�" + $(strId).value;
			}
			break;

		case "DICT�magas2" :
			if(trigger=="F10")
			{
				dogFonction="f10-magasi�n-magasi"
				inputParameter = "�ACTIVI�APP�ACTIF�O�CODSOC�<?=$c_codsoc?>�PARAME�toto�MAGASI�"+"�CODMVT�"+$("DICT�codmvt").value + "�CODCER�" + $("DICT�codcer").value + "�CAMPAGNE�" + $("DICT�campagne").value;
				filtre="FIELD=magasi|libel|adresse_5|telephone|codsoc-app";
			}
			else
			{
				dogFonction="get-magasi�n-magasi"
				inputParameter = "�ACTIVI�APP�ACTIF�O�CODSOC�<?=$c_codsoc?>�PARAME�toto�MAGASI�" + $(strId).value+"�CODMVT�"+$("DICT�codmvt").value + "�CODCER�" + $("DICT�codcer").value + "�CAMPAGNE�" + $("DICT�campagne").value;
			}

			if(trigger=="F8")
			{
				Rsp=RspCodmvt.concat()
				if(setValueFromKeyword ( "TYPTRANS"  , false , "" , ""  , true)=="3"&&setValueFromKeyword ( "TYPMVT"  , false , "" , ""  , true)=="1")
				{
					PhpAuxges = "<?=search::searchExe("ar_mvt_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_codmvt="+$("DICT�codmvt").value+"&_codcer="+$("DICT�codcer").value+"&_codcerlibel="+$("DICTLINK�codcer�libel").value+"&_magas2="+$("DICT�magas2").value+"&_magasi="+$("DICT�magasi").value+"&_magasilibel="+$("DICTLINK�magasi�libel").value;
					sFeatures = "dialogHeight:620px;dialogWidth:940px;status:no;help:no";
					retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
					if(retourExchange!=undefined&&retourExchange!=""){
						retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , "ROWID�cermvt|" +  retourExchange , "get-cermvt�a-cermvt" , "" , "" );
						// retourExchange = sendExchange ( "<?=$dogExchange?>" , "" , "ROWID�cermvt|" +  retourExchange , "get" , "" , paramExchange ,"","get-cermvt�a-cermvt") ;
						//on ramene les donn�es de l' entree
						bondupliquer();
					}
					return;
				}
				else
				{
					alert("Mouvement d'entr�e uniquement")
					return;
				}
			}
		break;
		
		case "DICT�codmvt":
			inputParameter = "�CODMVT�" + $(strId).value.trim();
			if(trigger=="F10")
			{
				dogFonction="f10-codmvt�a-tabcer";
				filtre="FIELD=codmvt|libel"
			}
			else
				dogFonction="get-codmvt�a-tabcer";
			break;

		case "DICT�cer-contrat":		
			// trace("DOGEXCHANGE NUMERO CONTRAT trigger : " + trigger)

			if(trigger == "GET")
			{
				Rsp=RspCodmvt.concat();
                // Pte 30/11/2016 Ajout du codcer pour les controles (classification...) 
				inputParameter  = "CODSOC�<?=$c_codsoc?>�CAMPAGNE�" + $("DICT�campagne").value + "�TYPCTR�A�CLOTURE�0";
                inputParameter += "�CER-CONTRAT�" + $(strId).value + "�PARAM�N�FILIERE�N�PROG�ASBONAPP" + "�CODCER�" + $("DICT�codcer").value;
				
                if (getValue("TYPTRANS", false, "", "", true) == "1")
                    inputParameter = "CODSOC�<?=$c_codsoc?>�TYPCTR�V" + "�CER-CONTRAT�" + $(strId).value;

				dogFonction = "get-cerctr�a-cerctr";
			}
			else
			{	
				// trace("	==> TRIGGER F10")
		
				if(RspContratProd.size() > 0 && RspNumContrat.size() > 0)
					return;
				
				Rsp=RspCodcer.clone();
				cergpmt=getValue("CODCER-REG",false,"","",true);
				Rsp=RspCodmvt.concat();
				
				PhpAuxges = "<?=search::searchExe("ar_contrat_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_autoload=O&_typctr=A&_clot=0&_codcer="+$("DICT�codcer").value+"&_codcerlibel="+$("DICT�codcer_libel").value+"&_typaux="+$("DICT�typaux").value+"&_codauxges="+$("DICT�codauxges").value+"&_codauxgeslibel="+$("DICT�codauxges_libel").value +"&_cergpmt="+cergpmt+"&_camp="+$("DICT�campagne").value+"&_codmvt="+$("DICT�codmvt").value+"&_magasi="+$("DICT�magasi").value+"&_filiere=N&_param=N&_prog=asbonapp" + "&_datmvt=" + $("DICT�cer-datsai").value;

				if(getValue ( "TYPTRANS"  , false , "" , ""  , true)=="1")
				{
                    PhpAuxges = "<?=search::searchExe("ar_contrat_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_autoload=O&_typctr=V&_clot=0&_codcer="+$("DICT�codcer").value+"&_codcerlibel="+$("DICT�codcer_libel").value+"&_typaux="+$("DICT�typaux").value+"&_codauxges="+$("DICT�codauxges").value+"&_codauxgeslibel="+$("DICT�codauxges_libel").value +"&_cergpmt="+cergpmt+"&_camp="+$("DICT�campagne").value+"&_codmvt="+$("DICT�codmvt").value+"&_magasi="+$("DICT�magasi").value+"&_filiere=N&_param=N&_prog=asbonapp" + "&_datmvt=" + $("DICT�cer-datsai").value;
				}

				sFeatures = "dialogHeight:690px;dialogWidth:975px;status:no;help:no";
				
				if(m_saitie.trim() == ""
				|| m_saitie.indexOf($("DICT�codmvt").value.trim().toUpperCase()) == -1
				|| m_ctrprod.trim() == ""
				|| $("DICT�cer-contrat").value.trim() != "" || $("DICT�codcer").value.trim() != "" || trigger != "GET")
				{
					if($('DICT�cer-contrat').parentElement.style.visibility!="hidden")retourModale = showModalDialog ( PhpAuxges , "" , sFeatures) ;
				}
				
				// trace("	==> retourModale : ")
				// trace(retourModale)
				
				if(retourModale == undefined)
				{
					doCallBack = backupCB;
					$(strId).focus();
					return false;
				}
				
				if(retourModale[0] =="")
				{
					doCallBack = backupCB;
					return false;
				}
				
				if(retourModale!=undefined)
				{
					oldTrigger = "GET";
					// Rsp = retourExchange;
					retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , "�ROWID�" + getValueFromDataArray(retourModale, "ROWID", false, "") , "get-cerctr�a-cerctr" , "", dogexchangecallback);
					// retourExchange = sendExchange ( "<?=$dogExchange?>" , "" ,  , "get" , "" , paramExchange ,"",) ;
				}

				doCallBack = backupCB;
				return true;
			}
			break;

		case "DICT�codcer": case "DICT�codcer-ori":
			// trace("DOGEXCHANGE CODCER trigger = " + trigger)
		
			//si on est en vente on autorise le F8
			if(oldTrigger=="F8")
			{
				Rsp=RspCodmvt.concat();
				if(getValue ( "TYPTRANS"  , false , "" , ""  , true)=="1")
				{
					//on lance une recherche des contrats
					PhpAuxges = "<?=search::searchExe("ar_numvoe_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_typctr=VCD&_camp="+$('DICT�campagne').value+"&_magasi="+$("DICT�magasi").value + "&_parame=0";
					sFeatures = "dialogHeight:620px;dialogWidth:940px;status:no;help:no";
					retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
					
					if(retourExchange!=undefined)
					{
						//acces execution
						retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�ROWID�cerctr|" + retourExchange + "�PARAME�0", "get-ceroex�a-cerctr", "", "");
						RspVoe=Rsp
						
						//on precharge les zones de commentaire
						getValue("REF-BL",false,"","DICT�ref-bl",true);
						getValue("LIB-FIL",false,"","DICT�lib-fil",true);
						getValue("DEST-FIN",false,"","DICT�dest-fin",true);
						
						if(getValue("CODMAG",false,"","",true).trim()!=$("DICT�magasi").value.trim())
							alert("Attention cet ordre d'exp�dition est rattach� sur un magasin diff�rent!");
						
						RspLiv=[];
						RspLiv[0]="OK";

						//Raz gridExec
						gridExec.setCellData([]);
						gridExec.setRowCount(0);
						gridExec.refresh();

						//on pointe sur les lignes
						for(var kk=1;kk<=getValue("LL-NUMLIG",false,"","",true).split(",").length;kk++)
						{
							i=parseFloatStr(getValue("LL-NUMLIG",false,"","",true).split(",")[kk])
							if(!isNaN(i))
							{
								RspLiv[kk] = "�ROWID�rowid|id"+i+"�CHRONO�"+i
								RspLiv[kk] += "�LL-NUMLIG�"+i
								RspLiv[kk] += "�LL-PRXTRP�"+getValue("LL-PRXTRP",false,"","",true).split(",")[kk];
								RspLiv[kk] += "�DAT-MIN�"+getValue("DAT-MIN",false,"","",true).split(",")[kk];
								RspLiv[kk] += "�DAT-MAX�"+getValue("DAT-MAX",false,"","",true).split(",")[kk];
								RspLiv[kk] += "�REFER-CNT�"+getValue("REFER-CNT",false,"","",true).split(",")[kk];
							}
						}
						//on vide la variable
						currowExec = -1;
						Rsp=RspLiv;
						
						//Cr�ation du tableau
						for(indice=1; indice<Rsp.length; indice++)
						{
							//Cr�ation d'une nouvelle ligne dans le tableau
							gridExec.loadTab();
							// {addrow2 ( "tab_header2" , "tab2_body" , unescape(indice)) ;}
						} //fin de la boucle de cration des lignes du tableau
						
						$("EXECUTION").show();
						if(parseInt(gridExec.getRowCount())==0)
						{
							$("EXECUTION").style.display="none";
							alert("Pas de lignes!");
							$("DICT�cer-contrat").focus();
							
							if($("DICT�codcer").value.trim()=="")
								$("DICT�codcer").focus();
							
							doCallBack = backupCB;
							return false;
						}

						//accesd au contrat
						Rsp = RspVoe;
						getValue("NUMOEX", false, "", "DICT�numoex",true);
						getValue ( "TRANSP"  , false , "" , "DICT�transp"  , true);
						getValue ( "LIBEL"  , true , "TRANSP" , "DICT�transp_libel"  , true);
						
						str_val = "CODSOC�<?=$c_codsoc?>�TYPCTR�V�CER-CONTRAT�"+getValue("CER-CONTRAT", false, "", "",true).trim();
						retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", str_val, "GET-CERCTR�a-cerctr", "", "");
						
						getValue ( "CODCER"  , false , "" , strId  , true);
						getValue ( "LIBEL"  , true , "CODCER" , "DICT�codcer_libel"  , true);
						
						for(i=1; i<=14; i++)
							getValue ( "VAL-ETAT-CTR_"+i  , false , "" , "DICT�val-etat-ctr_"+i  , true);
						
						currowExec=0;
						getValue ( "TYPAUX"  , false , "" , "DICT�typaux"  , true);
						getValue ( "CODAUXGES"  , false , "" , "DICT�codauxges"  , true);
						getValue ( "LIBEL"  , true , "CODAUXGES" , "DICT�codauxges_libel"  , true);
						getValue ( "CER-CONTRAT"  , false , "" , "DICT�cer-contrat"  , true);
						trigger = "GET";
					}
					else
					{
						doCallBack = backupCB;
						return false;
					}
				}
				else
				{
					alert("Vente uniquement");
					doCallBack = backupCB;
					return false;
				}
			}
			else if(trigger =="F10")
			{
				// trace("	==> TRIGGER F10")
				
				//on modifie la dogfunction
				PhpAuxges = "<?=search::searchExe("ar_cereal_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_libart="+$(strId).value+"&_campagne="+$("DICT�campagne").value+"&_magasi=" + $("DICT�magasi").value.trim()+"&_SCREEN=�libel�"+$(strId).value+"�8";
				sFeatures = "dialogHeight:450px;dialogWidth:580px;status:no;help:no";
				retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
				
				if(retourExchange!=undefined)
				{
					Rsp[0] = retourExchange[0];
					Rsp[1] = retourExchange[1];
					retourExchange = retourExchange[0];
					inputParameter = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�REGION�001�codcer�"+getField(Rsp[1],'CODCER')+ "�MAGASI�" + $("DICT�magasi").value.trim() + "�CER-DATSAI�" + $("DICT�cer-datsai").value; ;
					dogFonction="GET-cereal�a-cereal";
				}
				
				if (retourExchange != "OK")
				{
						initbufferscreen();
						$(strId).focus();
						doCallBack = backupCB;
						return false;
				}
			}
			else
			{
				// trace("	==> TRIGGER GET")
				inputParameter = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�REGION�001�codcer�"+$(strId).value.trim()+ "�MAGASI�" + $("DICT�magasi").value.trim() + "�CER-DATSAI�" + $("DICT�cer-datsai").value; ;
				dogFonction="GET-cereal�a-cereal";
			}
			break;
		
		case "DICT�typtrp-ctr" :
			$("DICT�typtrp-ctr").defaultValue=""
			inputParameter = "�TYPTRP�" + $(strId).value.trim()+"�CODMVT�"+$('DICT�codmvt').value;
			if(trigger=="F10")
				dogFonction="f10-typtrp�a-tabcer";
			else
				dogFonction="get-typtrp�a-tabcer";
			break;

		case "DICT�typaux" :
		case "DICT�typaux_chauff" :
			dogFonction = "get-typaux�d-reftables";
			if(trigger == "F10")
				dogFonction = "typaux�d-tables";
			inputParameter = "TYPAUX�" + $(strId).value;
			break;

		case "DICT�codaux_chauff" :
			if(trigger == "F10")
				{
					<?if($_rechadresse!=""){?>
					PhpAuxges = "<?=search::searchExe("nr_auxges_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_typaux="+$("DICT�typaux_chauff").value+ '&_SCREEN=�libabr� �adresse�' + $(strId).value+'&_adresse=' + $(strId).value + "&_motcle=APP";
					<?}else{?>
					PhpAuxges = "<?=search::searchExe("nr_auxges_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_typaux="+$("DICT�typaux_chauff").value+ '&_SCREEN=�libabr�' + $(strId).value + "&_motcle=APP";
					<?}?>

					sFeatures = "dialogHeight:500px;dialogWidth:900px;status:no;help:no";
					retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
					if(retourExchange!=undefined)
					{
						Rsp = retourExchange;
						retourExchange = retourExchange[0];
						dogexchangecallback();
						doCallBack = backupCB;
						return true;
					}
					else
					{
						doCallBack = backupCB;
						return false;
					}
				}
				else if(trigger == "GET")
				{
					inputParameter = "TYPAUX�" + $('DICT�typaux_chauff').value + "�CODAUXGES�" + $(strId).value + "�MOTCLE�APP";
					//on gere ici des specif pour la domiciliation
					inputParameter = inputParameter + "�TRTSPE�DOMLUC";
					dogFonction = "get-auxges�n-auxges";
				}
			break;
			
		case "DICT�codauxges" :
			// trace("DOGEXCHANGE codauxges trigger = " + trigger)
		
			if(trigger == "F10")
			{
				<?if($_rechadresse!=""){?>
				PhpAuxges = "<?=search::searchExe("nr_auxges_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_typaux="+$("DICT�typaux").value+ '&_SCREEN=�libabr� �adresse�' + $(strId).value+'&_adresse=' + $(strId).value + "&_motcle=APP";
				<?}else{?>
				PhpAuxges = "<?=search::searchExe("nr_auxges_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_typaux="+$("DICT�typaux").value+ '&_SCREEN=�libabr�' + $(strId).value + "&_motcle=APP";
				<?}?>

				sFeatures = "dialogHeight:500px;dialogWidth:900px;status:no;help:no";
				var retourDialog = showModalDialog ( PhpAuxges , "" , sFeatures) ;
				
				// trace(" ==> TRIGER F10 ==> retourDialog = " + retourDialog)
				
				if(retourDialog!=undefined)
				{
					Rsp[0] = retourDialog[0];
					Rsp[1] = retourDialog[1];
					retourExchange = retourDialog[0];
					dogexchangecallback();
					doCallBack = backupCB;
					return true;
				}
				else
				{
					doCallBack = backupCB;
					return false;
				}
			}
			else if(trigger == "GET")
			{
				// trace("	==> TRIGER GET")
				
				inputParameter = "TYPAUX�" + $('DICT�typaux').value + "�CODAUXGES�" + $(strId).value + "�MOTCLE�APP";
				//on gere ici des specif pour la domiciliation
				inputParameter = inputParameter + "�TRTSPE�DOMLUC";
				dogFonction = "get-auxges�n-auxges";
			}
			break;

		case "DICT�codcar" :
			inputParameter = "�MOTCLE�CODECAR-CER�CODTAB�" + $(strId).value.trim();
			dogFonction="GET-TABGEN�a-tabgen";
			break;

		case "DICT�cod-def" : case "DICT�cod-def1" :
			inputParameter = "�MOTCLE�CER-DEFAUT�CODTAB�" + $(strId).value.trim();
			dogFonction= trigger + "-TABGEN�a-tabgen";
			// dogFonction="F10-TABGEN�a-tabgen";
			if($(strId).value.trim() == "" && trigger == "F10")
				inputParameter = "�MOTCLE�CER-DEFAUT";
			filtre = "FIELD=codtab|libel1-1";
			break;

		case "DICT�code" :
			//on va formater le codtab correctement
			cod=$(strId).value.trim();
			//si il nest pas sur 3 caracteres on insere des espaces
			while(cod.length <3)
				cod+=" ";
			
			dogFonction="GET-TABGEN�a-tabgen";
			inputParameter = "�MOTCLE�BONREFCAR-CER�CODTAB�" +cod+" 00.00"+"�PREFIX�"+$("DICT�campagne").value.trim()+"001";
			
			//le typtab est diff�rent suivant le codecar
			switch(strIdb[1])
			{
				case "1":
					inputParameter += "�TYPTAB�HUM";
					break;
				case "2":
					inputParameter += "�TYPTAB�IMP";
					break;
				case "3":
					inputParameter += "�TYPTAB�PDS";
					break;
				case "4":
					inputParameter += "�TYPTAB�IHU";
					break;
				default:
					inputParameter += "�TYPTAB�SPE";
					break;
			}
			break;

		case "DICT�traitm" :
			if(trigger=="GET")
			{
				dogFonction = "get-traitm�a-tabcer";
				inputParameter = "TRAITM�" + $(strId).value;
			}
			else //F10
			{
				dogFonction = "f10-traitm�a-tabcer";
				inputParameter = "TRAITM�";
			}
			break;

		case "DICT�immatr" :
			if(trigger == "GET")
			{
				$(strId).defaultValue = $(strId).value;
				setFocus(TabFocusEntree, strId);
				return true;
			}
			dogFonction = "f10-immatr�n-transp";
			inputParameter = "transp�" + $("DICT�transp").value.trim();
			break;

		case "DICT�transp" :
			filtre="FIELD=TRANSP|LIBEL";
			dogFonction = trigger + "-transp�n-transp";
			inputParameter = "TRANSP�" + $(strId).value + "�ACTIF�O" +   "�typtrpctr�" + $("DICT�typtrp-ctr").value.trim();
			break;

		case "DICT�chauff" :
			dogFonction = trigger + "-chauff�n-tabges";
			inputParameter = "CHAUFF�" + $(strId).value;
			break;

		case "DICT�cer-typauxfac" :
			alert("(bnj)Il manque la fonction dog pour " + strId);
			return false;
			inputParameter = "TYPAUX�" + $(strId).value;
			break;

		case "DICT�cer-codauxfac" :
			inputParameter = "TYPAUX�" +$('DICT�typauxfac').value + "�CODAUX�" + $(strId).value ;//+ "�MOTCLE�" +$("DICT�motcle").value;
			//on gere ici des specif pour la domiciliation
			inputParameter = inputParameter + "�TRTSPE�DOMLUC";
			break;

		case "DICT�coddom" :
			paramExchange = "GETVAR|RELEASELOADFCTONSERVER|NOSCREEN";
			inputParameter = "TYPAUX-PAI�" +$("DICT�cer-typauxfac").value + "�CODAUX-PAI�" +$("DICT�cer-codauxfac").value;
			if ($("DICT�coddom").value.trim()!="") {
				inputParameter = inputParameter + "�CODDOM�" +$("DICT�coddom").value.trim() }
			break;

		case "DICT�modreg" :
			if(trigger=="GET"){
				dogFonction="get-modreg�d-reftables"
				inputParameter = "MODREG�" + $(strId).value;
			}
			else
			{
				dogFonction="modreg�d-tables";
			}
			break;

		case "DICT�cer-codech" :
			if(trigger=="F10")
				dogFonction="f10-codech�n-tabges"
			else
				dogFonction="get-codech�n-tabges"
			inputParameter = "CODECH�" + $(strId).value;
			break;

		case "DICT�cer-modliv" :
			inputParameter = "�CODTVA�" + $(strId).value.trim();
			dogFonction="codtva�d-tables";
			break;

		case "DICT�courtier" :
			inputParameter = "�MOTCLE�COURTIER-CER�CODTAB�" + $(strId).value.trim();
			if(trigger=="F10")
				dogFonction="F10-TABGEN�a-tabgen";
			else
				dogFonction="GET-TABGEN�a-tabgen";
			break;

		case "DICT�parite-cer" :
			inputParameter = "�MOTCLE�TYPTRP-CER�CODTAB�" + $(strId).value.trim();
			if(trigger=="F10")
				dogFonction="F10-TABGEN�a-tabgen";
			else
				dogFonction="GET-TABGEN�a-tabgen";
			break;

		case "DICT�assure" :
			inputParameter = "�MOTCLE�ASSURE-CER�CODTAB�" + $(strId).value.trim();
			if(trigger=="F10")
				dogFonction="F10-TABGEN�a-tabgen";
			else
				dogFonction="GET-TABGEN�a-tabgen";
			break;

		case "DICT�devise" :
			// alert("(bnj)Il manque la fonction dog pour " + strId);
			return false;
			inputParameter = "DEVISE�" + $(strId).value;
			break;

		case "DICT�codsocfac" :
			if(trigger!="F10")
				inputParameter = "CODSOC�" +  $(strId).value;
			dogFonction="AUTSOC�d-codsoc"
			break;

		case "DICT�codassur" :
			inputParameter = "�MOTCLE�ASSURE-CER�CODTAB�" + $(strId).value.trim();
			if(trigger=="F10")
				dogFonction="F10-TABGEN�a-tabgen";
			else
				dogFonction="GET-TABGEN�a-tabgen";
			break;

		case "DICT�xxx26" :
			inputParameter = "CODSOC�" +  $(strId).value;
			if(trigger=="F10")
				dogFonction="F10-AUTSOC�d-codsoc"
			else
				dogFonction="GET-AUTSOC�d-codsoc"
			break;

		case "DICT�codcer-ret" :
			inputParameter = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�REGION�001�codcer�"+$(strId).value.trim();
			dogFonction="GET-cereal�a-cereal";
			break;

		case "DICT�cellule": 
        case "DICT�cellule-ret":
            // Sans la gestion des rotations
            if ("<?=$_rotation?>".trim() == "")
            {
                // Pte 06/04/2017 melange cellule
                filtre          = "FIELD=CELLULE|LIBEL|CODCER|CODCERLIBEL|LIBEL|FAMCER|SFMCER|STOCK-CEL|TXREMPL|CAPACITE|DATE-CEL|MAGASI|TYPCEL|NUMLOT|QTE-STOCK|STOCK-ENT|STOCK-SOR|LIBCAR_1|VAL-ETAT_1|LIBCAR_2|VAL-ETAT_2|LIBCAR_3|VAL-ETAT_3|LIBCAR_4|VAL-ETAT_4|LIBCAR_5|VAL-ETAT_5|LIBCAR_6|VAL-ETAT_6|LIBCAR_7|VAL-ETAT_7|LIBCAR_8|VAL-ETAT_8|LIBCAR_9|VAL-ETAT_9|LIBCAR_10|VAL-ETAT_10|LIBCAR_11|VAL-ETAT_11|LIBCAR_12|VAL-ETAT_12|LIBCAR_13|VAL-ETAT_13|LIBCAR_14|VAL-ETAT_14";
                largeurGridF10  = 890;
				inputParameter  = "CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim()+"�CELLULE�"+$(strId).value+"�CAMPAGNE�" + $("DICT�campagne").value.trim()+"�CODCER�" + $("DICT�codcer").value.trim()+"�CODMVT�" + $("DICT�codmvt").value.trim() + "�TEST�SANS";
				
                if (trigger == "F10")
                    dogFonction = "f10-cellule�a-tabcer";
				else
					dogFonction = "get-cellule�a-tabcer";
			}
            // Avec la gestion des rotations
			else
			{
				if ($("DICT�codmvt").value.trim() == "" || $("DICT�codmvt").value.trim() != $("DICT�codmvt").defaultValue.trim())
				{
					$("DICT�codmvt").focus();
					alert("Veuillez saisir un code mouvement valide !")
					return false;
				}
                
				if ($("DICT�codcer").value.trim() == "" || $("DICT�codcer").value.trim() != $("DICT�codcer").defaultValue.trim())
				{
					$("DICT�codcer").focus();
					alert("Veuillez saisir un code c�r�ale valide !")
					return false;
				}
				
                // Affichage code cereale ou sous famille selon parametrage menu
                if (ccMultiCer == "Y")
                    filtre = "FIELD=CODTAB$Cellule|LIBEL1-1$Libel.|SFAM-PRODUIT$sous famille|LIB-SFAM$Libel.|STO-CELLULE$Stock";
                else
                    filtre = "FIELD=CODTAB$Cellule|LIBEL1-1$Libel.|STO-PRODUIT$codcer|LIB-PRODUIT$Libel.|STO-CELLULE$Stock";
                
                largeurGridF10 = 650;
                // Pte 06/04/2017 melange cellule: inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim()+"�CELLULE�"+$(strId).value+"�DATCRE�"+$("DICT�datbon").value+"�HEUCRE�"+$("DICT�heubon").value+"�CAMPAGNE�" + $("DICT�campagne").value.trim()+"�CODCER�" + $("DICT�codcer").value.trim()+"�CODMVT�" + $("DICT�codmvt").value.trim();
                inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $(strId).value
                               + "�DATCRE�"   + $("DICT�datbon").value          + "�HEUCRE�"  + $("DICT�heubon").value
                               + "�CAMPAGNE�" + $("DICT�campagne").value.trim() + "�CODCER�"  + $("DICT�codcer").value.trim()
                               + "�CODMVT�"   + $("DICT�codmvt").value.trim()   + "�CTRL-MEL�OUI";
				
                if (trigger == "F10")
					dogFonction = "f10-cellule�a-stocer";
				else
					dogFonction = "get-cellule�a-stocer";
			}
            break;
            
        case "DICT�cellule_1":
        case "DICT�cellule_2":
        case "DICT�cellule_3":
        case "DICT�cellule_4":
        case "DICT�cellule_5":
        case "DICT�cellule_6":
        case "DICT�cellule_7":
        case "DICT�cellule_8":
        case "DICT�cellule_9":
        case "DICT�cellule_10":
        case "DICT�cellule_11":
        case "DICT�cellule_12":
            // Sans la gestion des rotations
            if ("<?=$_rotation?>" == "")
            {
				filtre         = "FIELD=CELLULE|LIBEL|CODCER|FAMCER|SFMCER|STOCK-CEL|TXREMPL|CAPACITE|DATE-CEL|MAGASI|TYPCEL|NUMLOT|QTE-STOCK|STOCK-ENT|STOCK-SOR|LIBCAR_1|VAL-ETAT_1|LIBCAR_2|VAL-ETAT_2|LIBCAR_3|VAL-ETAT_3|LIBCAR_4|VAL-ETAT_4|LIBCAR_5|VAL-ETAT_5|LIBCAR_6|VAL-ETAT_6|LIBCAR_7|VAL-ETAT_7|LIBCAR_8|VAL-ETAT_8|LIBCAR_9|VAL-ETAT_9|LIBCAR_10|VAL-ETAT_10|LIBCAR_11|VAL-ETAT_11|LIBCAR_12|VAL-ETAT_12|LIBCAR_13|VAL-ETAT_13|LIBCAR_14|VAL-ETAT_14";
                largeurGridF10 = 890;
                inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $(strId).value + "�CAMPAGNE�" + $("DICT�campagne").value.trim() + "�CODCER�" + $("DICT�codcer").value.trim() + "�CODMVT�" + $("DICT�codmvt").value.trim();
                
				if (trigger == "F10")
                    dogFonction = "f10-cellule�a-tabcer";
                else
                    dogFonction = "get-cellule�a-tabcer";
            }
            // Avec la gestion des rotations
            else
            {
                // Pte 06/04/2017 melange cellule: avant
				// largeurGridF10 = 650;
				// filtre="FIELD=CODTAB|Cellule |LIBEL1-1|Libell� |STO-PRODUIT|codcer|LIB-PRODUIT|Libell� |STO-CELLULE|Stock "
				// inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim()+"�CELLULE�"+$(strId).value+"�DATCRE�"+$("DICT�datbon").value+"�HEUCRE�"+$("DICT�heubon").value+"�CAMPAGNE�" + $("DICT�campagne").value.trim()+"�CODCER�" + $("DICT�codcer").value.trim()+"�CODMVT�" + $("DICT�codmvt").value.trim();
                largeurGridF10 = 650;
                filtre         = "FIELD=CODTAB|Cellule |LIBEL1-1|Libell� |STO-PRODUIT|codcer|LIB-PRODUIT|Libell� |STO-CELLULE|Stock ";
                inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $(strId).value
                               + "�DATCRE�"   + $("DICT�datbon").value           + "�HEUCRE�"  + $("DICT�heubon").value
                               + "�CAMPAGNE�" + $("DICT�campagne").value.trim()  + "�CODCER�"  + $("DICT�codcer").value.trim()
                               + "�CODMVT�"   + $("DICT�codmvt").value.trim()    + "�CTRL-MEL�OUI";
                
                if (trigger == "F10")
                    dogFonction = "f10-cellule�a-stocer";
                else
                    dogFonction = "get-cellule�a-stocer";
            }
            break;
        
        case "DICT�e-cellule":
            // Sans la gestion des rotations
            if ("<?=$_rotation?>" == "")
            {
				largeurGridF10 = 890;
                filtre         = "FIELD=CELLULE|LIBEL|CODCER|FAMCER|SFMCER|STOCK-CEL|TXREMPL|CAPACITE|DATE-CEL|MAGASI|TYPCEL|NUMLOT|QTE-STOCK|STOCK-ENT|STOCK-SOR|LIBCAR_1|VAL-ETAT_1|LIBCAR_2|VAL-ETAT_2|LIBCAR_3|VAL-ETAT_3|LIBCAR_4|VAL-ETAT_4|LIBCAR_5|VAL-ETAT_5|LIBCAR_6|VAL-ETAT_6|LIBCAR_7|VAL-ETAT_7|LIBCAR_8|VAL-ETAT_8|LIBCAR_9|VAL-ETAT_9|LIBCAR_10|VAL-ETAT_10|LIBCAR_11|VAL-ETAT_11|LIBCAR_12|VAL-ETAT_12|LIBCAR_13|VAL-ETAT_13|LIBCAR_14|VAL-ETAT_14";
				
                if ($("DICT�magas2").parentElement.style.display == "none")
                    inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $(strId).value;
				else
                    if ($("DICT�magas2").value.trim() == "")
                        inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $(strId).value;
                    else
                        inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magas2").value.trim() + "�CELLULE�" + $(strId).value;
				
                if (trigger == "F10")
                    dogFonction = "f10-cellule�a-tabcer";
                else
                    dogFonction = "get-cellule�a-tabcer";
			}
            // Avec la gestion des rotations
            else
            {
                largeurGridF10 = 650;
                filtre         = "FIELD=CODTAB|Cellule |LIBEL1-1|Libell� |STO-PRODUIT|codcer|LIB-PRODUIT|Libell� |STO-CELLULE|Stock ";
                
                if ($("DICT�magas2").parentElement.style.display == "none")
                    inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�"+$(strId).value;
				else
                    if ($("DICT�magas2").value.trim() == "")
                        inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $(strId).value;
                    else
                        inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magas2").value.trim() + "�CELLULE�" + $(strId).value;
				
                // Pte 06/04/2017 melange cellule:
			    inputParameter += "�DATCRE�"  + $("dateRec").value              + "�HEUCRE�"  + $("heureRec").value
                               + "�CAMPAGNE�" + $("DICT�campagne").value.trim() + "�CODCER�"  + $("DICT�codcer").value.trim()
                               + "�CODMVT�"   + $("DICT�codmvt").value.trim()   + "�CTRL-MEL�OUI";
                
                if (trigger == "F10")
                    dogFonction = "f10-cellule�a-stocer";
                else
                    dogFonction = "get-cellule�a-stocer";
			}
            break;
        
		case "DICT�cergpmt": case "DICT�cerdeco": case "DICT�cerdecl": case "DICT�cerecl": case "DICT�cerech":
			dogFonction="GET-cereal�a-cereal";
			inputParameter = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�codcer�"+$(strId).value.trim();
			break;

		case "DICT�typcon":
			dogFonction="GET-TABGEN�a-tabgen";
			inputParameter = "�MOTCLE�TYPCON-CER�codtab�"+$(strId).value.trim();
			break;

		case "DICT�classact":
			dogFonction="GET-TABGEN�a-tabgen";
			inputParameter = "�MOTCLE�ACTIVE-CER�codtab�"+$(strId).value.trim();
			break;

		case "DICT�codcar" :
			inputParameter = "�MOTCLE�CODECAR-CER�CODTAB�" + $(strId).value.trim();
			dogFonction="GET-TABGEN�a-tabgen";
			break;

		case "DICT�soc":
			dogFonction = "GET-AUTSOC�d-CODSOC";
			inputParameter="�codsoc�"+ $(strId).value.trim();
			break;

		case "DICT�cer-contrat-fil":
			
            if(trigger == "GET")
			{
				Rsp=RspCodmvt.concat();
				inputParameter = "CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value+"�TYPCTR�A�CLOTURE�0"+"�CER-CONTRAT�"+$(strId).value+"�PARAM�N�FILIERE�0�PROG�ASBONAPP";
				if(getValue ( "TYPTRANS"  , false , "" , ""  , true)=="1")
				inputParameter = "CODSOC�<?=$c_codsoc?>�TYPCTR�V"+"�CER-CONTRAT�"+$(strId).value+"�FILIERE�0";
				dogFonction="get-cerctr�a-cerctr";
			}
			else
			{
				Rsp=RspCodcer.clone();
				cergpmt=getValue("CODCER-REG",false,"","",true);
				Rsp=RspCodmvt.concat();
				
				PhpAuxges = "<?=search::searchExe("ar_contrat_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_autoload=O&_typctr=A&_clot=0&_codcer="+$("DICT�codcer").value+"&_codcerlibel="+$("DICT�codcer_libel").value+"&_typaux="+$("DICT�typaux").value+"&_codauxges="+$("DICT�codauxges").value+"&_codauxgeslibel="+$("DICT�codauxges_libel").value +"&_cergpmt="+cergpmt+"&_camp="+$("DICT�campagne").value+"&_codmvt="+$("DICT�codmvt").value+"&_magasi="+$("DICT�magasi").value+"&_filiere=O&_param=N&_prog=asbonapp" + "&_datmvt=" + $("DICT�cer-datsai").value;
				
				if(getValue ( "TYPTRANS"  , false , "" , ""  , true)=="1")
					PhpAuxges = "<?=search::searchExe("ar_contrat_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_autoload=O&_typctr=V&_clot=0&_codcer="+$("DICT�codcer").value+"&_codcerlibel="+$("DICT�codcer_libel").value+"&_typaux="+$("DICT�typaux").value+"&_codauxges="+$("DICT�codauxges").value+"&_codauxgeslibel="+$("DICT�codauxges_libel").value +"&_cergpmt="+cergpmt+"&_camp="+$("DICT�campagne").value+"&_codmvt="+$("DICT�codmvt").value+"&_magasi="+$("DICT�magasi").value+"&_filiere=O&_param=N&_prog=asbonapp" + "&_datmvt=" + $("DICT�cer-datsai").value;

				sFeatures = "dialogHeight:690px;dialogWidth:975px;status:no;help:no";
				retourModale = showModalDialog ( PhpAuxges , "" , sFeatures) ;
				
				// trace("	==> [CONTRAT PROD] - retourModale : ")
				// trace(retourModale)
				
				if(retourModale == undefined)
				{
					doCallBack = backupCB;
					$(strId).focus();
					return false;
				}
				if(retourModale[0] =="")
				{
					doCallBack = backupCB;
					return false;
				}
				if(retourModale!=undefined)
				{
					oldTrigger = "GET";
					// Rsp = retourExchange;
					retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , "�ROWID�" + getValueFromDataArray(retourModale, "ROWID", false, "") , "get-cerctr�a-cerctr" , "", dogexchangecallback);
					// retourExchange = sendExchange ( "<?=$dogExchange?>" , "" ,  , "get" , "" , paramExchange ,"",) ;
				}

				doCallBack = backupCB;
				return true;
			}
			break;

		case "DICT�typie":
			inputParameter = "�typpie�" + $(strId).value.trim();
			dogFonction="typpie�d-tables";
			break;

		case "DICT�comonic" :

			inputParameter = "COMONIC�"+ $(strId).value
			dogFonction = "get-comonic�a-tabcer" ;
			if (trigger=="F10")
			{
				PhpArticl = "<?=search::searchExe("nr_comune_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_libel_menu=Recherche Commune" + "&_SCREEN=�libel�"+$(strId).value+"�8"
				sFeatures = "dialogHeight:450px;dialogWidth:580px;status:no;help:no";
				var retourModale = showModalDialog ( PhpArticl , "" , sFeatures) ;
				if(retourModale != undefined)
				{
					var reponse = retourExchange;
					reponse = reponse.split("�");
					$("DICT�comonic").value = reponse[0];
					$("DICT�comonic").defaultValue = reponse[0];
					$("comoniclibel").value = reponse[1];
					return true;
				}
				else return false;
			}
			break;

        // Pte Gestion des lots apports
        case "DICT�numlot":
            // paramExchange = "getvar|truedmd|noscreen|nodisplayerror";
            inputParameter   = "CAMPAGNE�" + $("DICT�campagne").value + "�MAGASI�" + $("DICT�magasi").value.trim() + "�CHRONO�" + $(strId).value;
            inputParameter  += "�CODCER�"  + $("DICT�codcer").value   + "�TYPMVT�" + ccTypMvt;

            dogFonction  = "get-lotapp�a-lotapp";

            if (trigger == "F10")
				dogFonction = "f10-lotapp�a-lotapp";
			break;

        // CAVAC V7 Nouvelle Gestion des parcelles
        case "DICT�codeparc":
            // Si saiparc = "1" alors le dog va lire les donn�es dans la table cercts sinon auxgss
            if ("<?=$_parcel?>" != "libre") {
                inputParameter  = "CODSOC�"    + "<?=$c_codsoc?>"                + "�CAMPAGNE�"  + $("DICT�campagne").value;
                inputParameter += "�TYPAUX�"   + $("DICT�typaux").value.trim()   + "�CODAUXGES�" + $("DICT�codauxges").value.trim();
                inputParameter += "�CODEPARC�" + $("DICT�codeparc").value.trim() + "�PRODUIT�"   + $("DICT�codcer").value;
                inputParameter += "�CONTRAT�"  + $("DICT�cer-contrat-fil").value + "�TYPCTR�A";
                filtre="FIELD=chrono|codeparc|libparc|commune|variete|topact|surface|rendement";
                
                dogFonction = "get-parcelle�n-parcel";
                
                if (trigger == "F10")
			    	dogFonction = "f10-parcelle�n-parcel";
            } // if ("<?=$_parcel?>" != "libre")
            else {setFocus ( TabFocusEntree , strId )}
            
			break;
            

		default:
			strIdS = strId.split("-");
			if(strIdS[0]=="tabGridEclat")
			{
				col = parseInt(strIdS[2]);
				row = strIdS[3];
				var valeur = "";
				// if($(gridEclat._id + "-cell-" + col + "-" + row + "-box-edit") != null)
					// valeur = $(gridEclat._id + "-cell-" + col + "-" + row + "-box-edit").value;
				// else if ( $(gridEclat._id + "-cell-" + col + "-" + row + "-box-text") != null )
					// valeur = $(gridEclat._id + "-cell-" + col + "-" + row + "-box-text").value
				// else
					valeur = gridEclat.getCellText ( col , row )
				switch(col)
				{
					case getColEclat("typauxecl") ://#TODO F10
						dogFonction = "get-typaux�d-reftables";
						inputParameter = "TYPAUX�" + valeur;
						break;
	
					case getColEclat("codauxgesecl") :
						if(trigger == "F10")
						{
							if("<?=$_rechadresse?>" != "")
								PhpAuxges = "<?=search::searchExe("nr_auxges_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_typaux="+$("DICT�typaux").value+ '&_SCREEN=�libabr� �adresse�' + valeur +'&_adresse=' + valeur + "&_motcle=APP";
							else
								PhpAuxges = "<?=search::searchExe("nr_auxges_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_typaux="+$("DICT�typaux").value+ '&_SCREEN=�libabr�' + valeur + "&_motcle=APP";

							sFeatures = "dialogHeight:500px;dialogWidth:900px;status:no;help:no";
							retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
							
							if(retourExchange!=undefined)
							{
								Rsp = retourExchange;
								retourExchange = retourExchange[0];
								dogexchangecallback();
								doCallBack = backupCB;
								return true;
							}
							else
							{
								doCallBack = backupCB;
								return false;
							}
						}
						else if(trigger == "GET")
						{
							inputParameter = "TYPAUX�" + gridEclat.getCellText(getColEclat("typauxecl"), row) + "�CODAUXGES�" + valeur + "�MOTCLE�APP�TRTSPE�DOMLUC";
							dogFonction = "get-auxges�n-auxges";
						}
						break;

					case getColEclat("codmvtecl") :
						inputParameter = "�CODMVT�" + $(strId).value.trim();
						dogFonction = trigger+"-codmvt�a-tabcer";
						
						if(trigger=="F10")
							filtre="FIELD=codmvt|libel";
						break;
						
					case getColEclat("contrat"):
						keycode = 999;
					
						if(trigger == "GET")
						{
							if(getField(RspCodmvt[1],'TYPTRANS') == "1")
								inputParameter = "CODSOC�<?=$c_codsoc?>�TYPCTR�V"+"�CER-CONTRAT�"+valeur;
							else
								inputParameter = "CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value+"�TYPCTR�A�CLOTURE�0"+"�CER-CONTRAT�"+valeur+"�PARAM�N�FILIERE�N�PROG�ASBONAPP";
							
							dogFonction = "get-cerctr�a-cerctr";
						}
						else /*F10*/
						{
							
							cergpmt = getField(RspCodcer[1],'CODCER-REG');
							
							if(getField(RspCodmvt[1],'TYPTRANS') == "1")
								PhpAuxges = "<?=search::searchExe("ar_contrat_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_autoload=O&_typctr=V&_clot=0&_codcer=" + $("DICT�codcer").value + "&_codcerlibel=" +$("DICT�codcer_libel").value+"&_typaux=" + gridEclat.getCellText(getColEclat("typauxecl"),row) + "&_codauxges=" + gridEclat.getCellText(getColEclat("codauxgesecl"),row) + "&_codauxgeslibel=" + gridEclat.getCellText(getColEclat("adresse"),row) + "&_cergpmt="+cergpmt+"&_camp="+$("DICT�campagne").value+"&_codmvt="+$("DICT�codmvt").value+"&_magasi="+$("DICT�magasi").value+"&_filiere=N&_param=N&_prog=asbonapp" + "&_datmvt=" + $("DICT�cer-datsai").value;
							else
								PhpAuxges = "<?=search::searchExe("ar_contrat_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_autoload=O&_typctr=A&_clot=0&_codcer=" + $("DICT�codcer").value + "&_codcerlibel=" + $("DICT�codcer_libel").value+"&_typaux=" + gridEclat.getCellText(getColEclat("typauxecl"),row) + "&_codauxges=" + gridEclat.getCellText(getColEclat("codauxgesecl"),row) + "&_codauxgeslibel=" + gridEclat.getCellText(getColEclat("adresse"),row) + "&_cergpmt="+cergpmt+"&_camp="+$("DICT�campagne").value+"&_codmvt="+$("DICT�codmvt").value+"&_magasi="+$("DICT�magasi").value+"&_filiere=N&_param=N&_prog=asbonapp" + "&_datmvt=" + $("DICT�cer-datsai").value;								

							if(eclatementSPE)
							{
								//R�cup�ration liste contrat utilis�s.
								var listeCTR = [];
								for(var i=0; i<gridEclat.getRowCount(); i++)
								{
									if(gridEclat.getCellData(getColEclat('contrat'),i).trim() != "")
										listeCTR.push(gridEclat.getCellData(getColEclat('contrat'),i).trim());
								}
								
								PhpAuxges += "&_listeCtrExclus=" + listeCTR.join(',');
							}
							
							sFeatures = "dialogHeight:690px;dialogWidth:975px;status:no;help:no";
							retourModale = showModalDialog ( PhpAuxges , "" , sFeatures) ;
														
							if(retourModale == undefined)
							{
								doCallBack = backupCB;
								$(strId).focus();
								return false;
							}
							
							if(retourModale[0] =="")
							{
								doCallBack = backupCB;
								return false;
							}
							
							if(retourModale!=undefined)
							{
								oldTrigger = "GET";
								// Rsp = retourExchange;
								retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , "�ROWID�" + getValueFromDataArray(retourModale, "ROWID", false, "") , "get-cerctr�a-cerctr" , "", dogexchangecallback);
								// retourExchange = sendExchange ( "<?=$dogExchange?>" , "" ,  , "get" , "" , paramExchange ,"",) ;
							}

							doCallBack = backupCB;
							return true;
						}
						break;

					case getColEclat("option"):
						if(getField(RspCodcer[1],'COLLECTE') != "" && getField(RspCodcer[1],'COLLECTE').substr(0,1) == "N")
							inputParameter = "CODSOC�<?=$c_codsoc?>�COLLECTE�N" + "�CODCER�"+$("DICT�codcer").value.trim()  + "�CODMVT�"+$("DICT�codmvt").value.trim()  + "�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�DATSAI�" + $("DICT�cer-datsai").value;
						else
							inputParameter = "CODSOC�<?=$c_codsoc?>�COLLECTE�O" + "�CODCER�"+$("DICT�codcer").value.trim()  + "�CODMVT�"+$("DICT�codmvt").value.trim()  + "�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�DATSAI�" + $("DICT�cer-datsai").value;

						dogFonction = trigger + "-OPTION�a-tabcer";

						if(trigger == 'GET')
							inputParameter += "�CODTAB�"+ gridEclat.getCellText(col,row);
						break;
				}
			}
			else
				if(debug)
				{
					alert("Zone non g�r�e dans le dogexchange : " + strId);
					doCallBack = backupCB;
					return false;
				}
			break;
	}

    // Cette fonction est charg�e d'ex�cuter les trigger d'�change DOG
    switch (trigger)
    {
        case "GET": //Touche entr�e
            //Faire le Get
            retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "", dogexchangecallback);
            break;
        case "AUTO": //Touche entr�e
            //Faire le Get
			retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "" , "" );
            break;
        case "F10": //Touche F10
            retourExchange = sendExchangeF10 ( "<?=$dogExchangeF10?>" , inputParameter , dogFonction , strId , "" , filtre, largeurGridF10 );
			switch(strId)
			{
                case "DICT�cellule":
                    if ("<?=$_rotation?>".trim() == "") break;
                    
					grid.getCellTemplate(4).setStyle("text-align", "right");
                    break;

				case "DICT�e-cellule" :
					if ("<?=$_rotation?>".trim() == "") break;
                    
					grid.getCellTemplate(4).setStyle("text-align", "right");
                    break;
			}
            break;
    } //Selon le trigger a ex�cuter
	// doCallBack = backupCB;
	return retourExchange;
} //Fin de la fonction dogexchange

function dogexchangecallback()
{
	// trace("--- dogexchangecallback, [" + Rsp[0] + "] pour " + oldId + "et doCallBack qui vaut -> " + doCallBack)
    if ( oldTrigger == "AUTO" ) return
	
	trace("		##" + oldId + " - " + $(oldId).defaultValue)
	
	
    if ( Rsp[0] == "OK" )
    {
		switch (oldId)
		{
			case "DICT�protrs_1" :
			case "DICT�protrs_2" :
			case "DICT�protrs_3" :
				$(oldId).value = getValeurRsp("CODTAB");
				$(oldId + "_libel").value  = getValeurRsp("LIBEL1-1");
                $(oldId + "_nivnet").value = getValeurRsp("LIBEL1-3");
				break;
				
			case "DICT�no-ordre":
				RspNumOrdre = Rsp.clone();
				RspNumOrdre.shift();
			
				if($("DICT�codmvt").value.trim() != "")
				{
					isSaisieEnCours = false;
					boninserer();
				}
				
				if(getField(RspNumOrdre,'CODMAG') != $("DICT�magasi").value.trim())
				{
					alert("Cet ordre d'exp�dition est pour le magasin : " + getField(RspNumOrdre,'CODMAG').trim() + " !");
					$(oldId).value = "";
					$(oldId).defaultValue = "";
					return false;
				}
				
				$(oldId).value = getField(RspNumOrdre,'NUMOEX');

				//CODMVT
				var inputParameter = "CODTAB�" + getField(RspNumOrdre,'TYPCTR') + "�MOTCLE�CODMVT-CDE";
				var dogFonction = "get-tabgen�a-tabgen";
				retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "" , "" );
				$('DICT�codmvt').value = getValeurRsp('LIBEL1-1');
				dogexchange($("DICT�codmvt"), "GET", false);

				//CODCER
				$('DICT�codcer').value = getField(RspNumOrdre,'CODCER');
				dogexchange($("DICT�codcer"), "GET", false);

				//TYPAUX
				$('DICT�typaux').value = getField(RspNumOrdre,'TYPAUX');
				
				//CODAUXGES
				$('DICT�codauxges').value = getField(RspNumOrdre,'CODAUXGES');
				
				//TRANSP
				$('DICT�transp').value = getField(RspNumOrdre,'TRANSP');

				//TYPE DE TRANSPORT
				$('DICT�typtrp-ctr').value = getField(RspNumOrdre,'TYPTRP-CTR');
				
				if($('DICT�transp').value != "")
					enableZone('DICT�transp',false);

				//CONTRAT
				$('DICT�cer-contrat').value = getField(RspNumOrdre,'CER-CONTRAT');

                // Pte 22/03/2017 Lecture des informations li�s au contrat de l'ordre d'expedition
                if ($('DICT�cer-contrat').value != "")
                {
                    dogexchange($('DICT�cer-contrat'),'GET');

                    RspCtrOrdre = Rsp.clone();
                    RspCtrOrdre.shift();

                    i = 0;
                    for(var kk = 1; kk <= 14; kk++)
                    {
                        $("DICT�libcar_" + kk).value       = getField(RspCtrOrdre, 'LIBCAR_'       + kk);
                        $("DICT�val-etat-ctr_" + kk).value = getField(RspCtrOrdre, 'VAL-ETAT-CTR_' + kk);
						
						if(modifBon)
							$("DICT�val-nego_" + kk).value     = getField(RspCtrOrdre, 'VAL-NEGO_'     + kk);
						
                        $("DICT�val-etat_" + kk).value     = getField(RspCtrOrdre, 'VAL-ETAT_'     + kk);
	                }
                }

                //MAGASIN R�CIPROQUE
				$('DICT�magas2').value = getField(RspNumOrdre,'CODMAG-REC');
				dogexchange($('DICT�magas2'),'GET');

				//ADRESSE DE LIVRAISON
				$('DICT�adr-liv_1').value = getField(RspNumOrdre,'ADR-LIV_1');
				$('DICT�adr-liv_2').value = getField(RspNumOrdre,'ADR-LIV_2');
				$('DICT�adr-liv_3').value = getField(RspNumOrdre,'ADR-LIV_3');
				$('DICT�adr-liv_4').value = getField(RspNumOrdre,'ADR-LIV_4');
				$('DICT�adr-liv_5').value = getField(RspNumOrdre,'ADR-LIV_5');

				//INDICE DE LIVRAISON
				$('DICT�indice-liv').value = getField(RspNumOrdre,'INDICE-LIV');

				//on r�cup�re les dates en format FR dans un tableau
				var sdatdeb = getField(RspNumOrdre,'DATDEB').split("/");
				var sdatmax = getField(RspNumOrdre,'DATMAX').split("/");
				var sdatsai = $("DICT�cer-datsai").value.trim().split("/");

				//On �cris les dates dans une chaine au format EN/US
				var asdatdeb = sdatdeb[1] + "/" + sdatdeb[0] + "/" + sdatdeb[2];
				var asdatmax = sdatmax[1] + "/" + sdatmax[0] + "/" + sdatmax[2];
				var asdatsai = sdatsai[1] + "/" + sdatsai[0] + "/" + sdatsai[2];

				//On cr�er les objets dates
				var ddatdeb = new Date(asdatdeb);
				var ddatmax = new Date(asdatmax);
				var ddatsai = new Date(asdatsai);

				//On compare les dates
				if(ddatsai < ddatdeb || ddatsai > ddatmax)
					alert("Attention cet ordre doit �tre exp�di� entre le " + getField(RspNumOrdre,'DATDEB') + " et le " + getField(RspNumOrdre,'DATMAX'));
				
				//Blcoage des zones
				zonscreenget("�typaux��9�codauxges��9�codcer��9�codmvt��9�cer-contrat��9�magas2��9")
				Rsp = RspCodcer.clone();
				
				setFocus(TabFocusEntree, oldId);
				return true;
				break;

            case "DICT�intracom" :
                setValueFromKeyword("INTRACOM", false, "", oldId, true);
				break ;

			case "DICT�rotation" :
				setValueFromKeyword("ROTATION", false, "", oldId, true);
				break;
				
			case "DICT�rotation_1" :
			case "DICT�rotation_2" :
			case "DICT�rotation_3" :
			case "DICT�rotation_4" :
			case "DICT�rotation_5" :
			case "DICT�rotation_6" :
			case "DICT�rotation_7" :
			case "DICT�rotation_8" :
			case "DICT�rotation_9" :
			case "DICT�rotation_10" :
			case "DICT�rotation_11" :
			case "DICT�rotation_12" :
				//var num = strId.split("_")[1];
				setValueFromKeyword("ROTATION", false, "", oldId, true);
				break;
			
			case "DICT�cod-adrliv":
				getValue( "CHRONO"  , false , "", oldId, true);
				getValue( "ADRESSE_1"  , false , "", "DICT�adr-liv_1", true);
				getValue( "ADRESSE_2"  , false , "", "DICT�adr-liv_2", true);
				getValue( "ADRESSE_3"  , false , "", "DICT�adr-liv_3", true);
				getValue( "ADRESSE_4"  , false , "", "DICT�adr-liv_4", true);
				getValue( "ADRESSE_5"  , false , "", "DICT�adr-liv_5", true);
				break;
			
			case "DICT�variete" :
				getValue("VARIETE", false, "", "DICT�variete", true);
				getValue("LIBEL", false, "", "DICT�codcer_libel", true);
				$(oldId).defaultValue = $(oldId).value;
				setFocus(TabFocusEntree, "DICT�codcer")
				return true;
				break;
			
			case "DICT�natcon" :
				getValue("CODTAB"  , false , "" , oldId  , true);
				$('DICT�natcon').title = "Ordre de commercialisation";
				
				/* if(isCreation && ($("DICT�cer-contrat-fil").value.trim() != "" || "<?=$ident?>" == "734" ))
				{
					$(oldId).defaultValue = $(oldId).value;
					setFocus(TabFocusEntree, "DICT�cer-contrat-fil");
					return true;
				} */
				
				/** ---------  O R D R E    D E   S A I S I E --------- **/
				<?if(file_exists(search::searchExe("as_ordre_saisie.js",0)))
				  {
				?>
					try
					{
						if(!modifBon)
						{
							if(zoneSuivanteSpe('NATURECONTRAT'))
								return;						
						}
					} catch(ex){trace("---zoneSuivanteSpe : " + ex)}; 
				<?}?>
				
				break;
				
			case "DICT�magasi":
				getValue("MAGASI", false ,"" , oldId ,true);
				getValue("LIBEL" , false ,"" , oldId + "_libel" ,true);
				RspMagasi = Rsp.clone();
				$("balancebrut").hide();
				$("balancetare").hide();
				$("balancebrut2").hide();
				$("balancetare2").hide();
				$("tareSupervision").hide();
				if(getValue("PT-BASCULE", false, "", "", true) == "1")
				{
					$("balancebrut").show();
					$("balancetare").show();
				}
				if(getValue("PT-BASCULE", false, "", "", true) == "2")
				{
					$("balancebrut").show();
					$("balancetare").show();
					$("balancebrut2").show();
					$("balancetare2").show();
				}
				if(getValue("BADGE", false, "", "", true) == "1")
				{
					$("tareSupervision").show();
				}
				break;

			case "DICT�typaux":
				//Gestion des typaux autoris�s selon le mouvement
				var typauxOk = false;
				for(var i = 0 ; i < typaux_aut.length && !typauxOk; i++)
					if(typaux_aut[i].toUpperCase().trim() == getValue("TYPAUX", false ,"" , "" ,true).toUpperCase().trim())
					{
						typauxOk = true;
					}
				if(!typauxOk && typaux_aut.length > 0)
				{
					alert("Type auxiliaire non autoris� !");
					$(oldId).focus();
					$(oldId).select();
					return false;
				}
				
				/** ---------  O R D R E    D E   S A I S I E --------- **/
				<?if(file_exists(search::searchExe("as_ordre_saisie.js",0)))
				{
				?>
					try
					{
						if(!modifBon)
						{
							if(zoneSuivanteSpe('TYPETIERS'))
								return;						
						}
					} catch(ex){trace("---zoneSuivanteSpe : " + ex)};
				<?}?>

			case "DICT�typaux_chauff":
				getValue("TYPAUX", false ,"" , oldId ,true);
				break;

			case "DICT�cer-contrat" :	
				// trace("CALLBACK NUMERO CONTRAT")

				RspNumContrat = Rsp.clone();
			
				RspContrat=Rsp.clone();
				if(!ctrlNomSaisi())
                {
                    return false;
                }
				
				$(oldId).value = getValeurRsp("CER-CONTRAT");
				$(oldId).defaultValue = $(oldId).value;
				
				if(topnatcon && getValeurRsp("TYPCON") != "")
				{
					setValueFromKeyword ( "TYPCON"  , false , "" , "DICT�natcon", true);
					enableZone("DICT�natcon" , false);
				}
				
				if(getValeurRsp("TYPCON") == "" && topnatcon)
				{
					enableZone("DICT�natcon" , true);
				}
				
				for(i=1; i<=14; i++)
				{
					$('DICT�val-etat-ctr_'+i).value = getValeurRsp('VAL-ETAT-CTR_'+i);
					
					if(modifBon)
						$('DICT�val-nego_'+i).value = getValeurRsp('VAL-NEGO_'+i);
				}

                //on verifie la coherence du contrat
                /* Pte 30/11 => Voir DOG Get-cerctr
				if(setValueFromKeyword("CODCER",false,"","", true).trim().toUpperCase()!=$("DICT�codcer").value.trim().toUpperCase())
					alert("Attention le code c�r�ale du contrat ne correspond pas");
				*/
				if(setValueFromKeyword("CODAUXGES",false,"","", true).trim().toUpperCase()!=$("DICT�codauxges").value.trim().toUpperCase() )
				{
					alert("Attention le code tiers du contrat ne correspond pas");
					if("<?=$_TIERS_CTR?>".trim().toUpperCase() != "FACULT")
					{
						$(oldId).value = "";
						$(oldId).defaultValue = $(oldId).value;
						return false;
					}
				}
				
				if(setValueFromKeyword("CAMPAGNE",false,"","", true).trim().toUpperCase()!=$("DICT�campagne").value.trim().toUpperCase()){
					// Pte 04/05/2017 
                    // $(oldId).value = "";
					// $(oldId).defaultValue = $(oldId).value;
					alert("Attention ce contrat est affect� sur une autre campagne")
					//return false;
				}
				
				$(oldId).defaultValue = $(oldId).value;
				Rsp = RspCoderMvt.clone();
				
				//on teste si l'oe est obligatoire donc si param montre que c'est obligatoire on affiche ds ts les cas le message
				if(setValueFromKeyword ( "TOPELEMENT_16" ,false ,"" ,""  , true) == "1")
					if($("DICT�numoex").value.trim() == "")
						alert("Veuillez saisir l'ordre d'exp�dition");

				//on regarde si l'odre d'expedition est rendu et si ds le menu on dit que rendu c'est obli on affiche le message et que l'oe est facultatif
				if(setValueFromKeyword ( "TOPELEMENT_16" ,false ,"" ,""  , true)=="2")
				{
					Rsp=RspContrat;
					if(setValueFromKeyword("TYPTRP-CTR",false,"","",true)!=""&&"<?=$_rendu?>"=="Oblig")
						if($("DICT�numoex").value.trim()=="")
							alert("Veuillez saisir l'ordre d'exp�dition");

				}
				
				Rsp=RspContrat.clone();
				
				//on regarde en modification que le contrat n'est pas cloture
				if(setValueFromKeyword("CER-VALID",false,"","",true)!="")
				{
					if(RspMvt != undefined) Rsp = Rsp.clone();
					else Rsp = undefined;

					if(setValueFromKeyword("CER-CONTRAT",false,"",oldId,true)!=$("DICT�cer-contrat").value)//&&setValueFromKeyword("CER-CONTRAT",false,"","",true)!=""){
					{
						alert("Contrat clotur�")
						return
					}
				}
				
				if(setValueFromKeyword("CLOTURE",false,"","",true)!="")
				{
					if(RspMvt != undefined) Rsp = Rsp.clone(); else Rsp = undefined;
					if(setValueFromKeyword("CER-CONTRAT",false,"",oldId,true)!=$("DICT�cer-contrat").value)//&&setValueFromKeyword("CER-CONTRAT",false,"","",true)!=""){
					{
						alert("Contrat clotur�");
						return false;
					}
				}
				
				Rsp=RspContrat;
				
				//on va verifier le code mouvement associ� au contrat
				retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , "�MOTCLE�TYPCON-CER�CODTAB�" + setValueFromKeyword("TYPCON",false,"","",true) , "GET-TABGEN�a-tabgen" , "" , "" );
				//on regarde si on a ramene qque chose
				if (retourExchange != "OK")
				{
                    setFocus(TabFocusEntree, oldId);
					return false;
				}

				RspTmp=Rsp.clone();
				Rsp=RspCodmvt.concat();

                if(setValueFromKeyword("TENU-STOCK",false,"","",true)!="1")
					tenustock = true;
				else
					tenustock = false;

                Rsp=RspContrat

                var autorichargement = setValueFromKeyword("AUTORI-CHARG",false,"","", true).trim().toUpperCase() ;

                if (autorichargement == "O" && nomAutorise != "")
					nomAutorise = "_ANNUL";
				
				setValueFromKeyword ( "CER-CONTRAT"  , false , "" , oldId  , true);

                // CAVAC V7 Lire le type du contrat pour trouver le parametrage de la zone parcelle
                typcontrat = setValueFromKeyword("TYPCON", false, "", "", true);

                inputParameter = "�MOTCLE�TYPCON-CER�CODTAB�" + typcontrat;
                dogFonction    = "GET-TABGEN�a-tabgen";
                retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "GET-TABGEN�a-tabgen", "", "");

                // Seulement si la saisie de la parcelle est obligatoire, on ecrase le param�trage du mouvement
                saiparc = setValueFromKeyword("LIBEL1-10", false, "", "", true);

                if (saiparc == "1") 
					zonscreenget("�codeparc��11")  //obligatoire
				
				var sov_docallback = doCallBack;
                if(tenustock==true)//si tenu-stock alors
				{
					//si oui on change le code mouvement
					Rsp=RspTmp;
					if(setValueFromKeyword("LIBEL1-2",false,"","",true)!="")
					{
						//et on change le code mvt
						if($("DICT�codmvt").value!=setValueFromKeyword("LIBEL1-2",false,"","",true))
						{
                            okInsert = false;   // Pour ne pas repartir en insertion de bon
							setValueFromKeyword("LIBEL1-2",false,"","DICT�codmvt",true);
							//on se refait des get
							dogexchange($('DICT�codmvt'), "GET", false);
							dogexchange($('DICT�codcer'), "GET", false);
                            okInsert = true;
						}
					}
				}
				
				oldId = "DICT�cer-contrat";
				doCallBack = sov_docallback;
				
				Rsp = RspContrat.clone();
				
				if(isCreation && $("DICT�cer-contrat-fil").value.trim() != "" && $("DICT�natcon").value.trim() != "" )
				{
					// trace("	==> creation = true | CONTRAT PROD saisi | NATURE CONTRAT saisi ==> focus tiers chauffeur")
					setFocus(TabFocusEntree, "DICT�cer-contrat-fil");
				}
				else
				{
					// trace("	==> Focus NATURE CONTRAT")
					setFocus(TabFocusEntree, oldId);
				}
				return true;
				break;
				
			case "DICT�codauxges":
				//on regarde le parametrage par rapport aux tiers a dadresse obligatoire
				RspCodaux=Rsp.clone();
				var ancienneValeur = $('DICT�codauxges').defaultValue;
				
				$(oldId).value = getField(RspCodaux[1],'CODAUXGES');
				
				// getValue("CODAUXGES" , false ,"" ,"DICT�codauxges" ,true);

				trace("	##CHECK CONTRAT");
				trace("getField(RspCodaux[1],'CODAUXGES').trim() = " + getField(RspCodaux[1],'CODAUXGES').trim());
				trace("ancienneValeur.trim() = " + ancienneValeur.trim());
				trace("$('DICT�cer-contrat').value.trim() = " + $("DICT�cer-contrat").value.trim());
				
				
				//on v�rifie la coh�rence du contrat
				if(ancienneValeur.trim() != "" && getField(RspCodaux[1],'CODAUXGES').trim() != ancienneValeur.trim() && $("DICT�cer-contrat").value.trim() != "")
				{
					alert("Attention le code tiers du contrat ne correspond pas");
					if("<?=$_TIERS_CTR?>".trim().toUpperCase() != "FACULT")
					{
						//si le tiers a changer on vire le numero de contrat
						$("DICT�cer-contrat").value="";
						$("DICT�cer-contrat").defaultValue="";
					}
					if(ancienneValeur.trim()!="")$("DICT�cer-contrat-fil").value=""
				}

				Rsp=RspCodmvt.concat();
				if(getValue("ADRAUTORI",false,"","",true)=="O")
				{
					Rsp=RspCodaux;
					if(getValue ( "SAI-ADRES"  , false , "" , ""  , true)!="1")
					{
						alert("Tiers � adresse obligatoire non autoris�");
						return;
					}
				}
				//pour les issus de la version caractere on teste si le tiers est encore actif
				Rsp=RspCodaux
				if(getValue ( "ACTIF"  , false , "" , ""  , true)=="N")
				{
					alert("Tiers inactif non autoris�");
					return;
				}

				//gestion des tiers interdits en cereales
				if(getValue("AUTO-APPORT",false,"","",true)=="2")
				{
					Rsp=RspCoderMvt;
					if(getValue("TYPCTR",false ,"","",true)=="V")
					{
						alert("Tiers interdit en ventes c�r�ales");
						return
					}
				}
				
				//si le contrat est obligatoire pour le tiers et si le contrat est obligatoire pour la cereale on fait le tiers doit au moins avoir un contrat
				Rsp=RspCoderMvt.clone();
				
				if(getValue("TOPELEMENT_10",false ,"","",true)=="1")
				{
					//on regarde si on a un contrat pour ce tiers
					Rsp=RspCodmvt.concat();
					inputParameter = "CODSOC�<?=$c_codsoc?>�TYPCTR�"+ typctr.trim() +"�CLOTURE�0"+"�TYPAUX�"+$("DICT�typaux").value+"�CODAUXGES�"+$("DICT�codauxges").value+"�CODCER�"+$("DICT�codcer").value+"�CAMPAGNE�"+$("DICT�campagne").value+"�CODMVT�"+$("DICT�codmvt").value+"�CERGPMT�"+$("DICT�codcer-ori").value+"�PROG�ASBONAPP�DATMVT�" + $("DICT�cer-datsai").value;
					dogFonction="F10-cerctr�a-cerctr";
					//Faire le Get
					retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");
					//Tester le retour et quitter s'il ya une erreur
					if (retourExchange != "OK" && $("DICT�cer-contrat").value.trim()=="")
					{
						alert("Le tiers doit avoir au moins un contrat pour ce produit");
						$(oldId).defaultValue = "";
						return;
					}
					//on charge un top
					topF10 = true
				}
				
				//si le contrat est obligatoire ou facultatif
				if(getValue("TOPELEMENT_10",false ,"","",true)!="0")
					topF10 = true
				
				if(RspMagasi==undefined)
				{
					//on fait un get sur le magasin
					dogFonction="get-magasi�n-magasi";
					inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value;
					retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");
					RspMagasi = Rsp.clone();
				}
				
				//si achat et pas de soci�t� d'appartenance blocage sauf pour nap et tap
				<? if ( file_exists ( search::searchExe("as_bonapp_dogexchangectrl_spe.inc",0) ) )
					include ( search::searchExe("as_bonapp_dogexchangectrl_spe.inc",0) ) ;
				?>
				
				Rsp = RspCodaux;
				initbufferscreen();
				$("DICT�typtrp-ctr").defaultValue="";

                // Pte 15/02/2016
                if (getValue("SOCPRI-CER", false, "", "", true) != "" && getValue("SOCPRI-CER", false, "", "", true) != "<?=$c_codsoc?>")
                    alert("Societe principale du tiers " + getValue("SOCPRI-CER",false,"","",true) + " differente de la societe de connexion");

                if("<?=$ident?>"=="677")
				{
					//on verifie les tiers autoris�s
					if(getValue("CODSOC-APP",false,"","",true)!="")
					{
						soctiers=getValue("CODSOC-APP",false,"","",true)
						if(soctiers=="0005")
							soctiers="0004";
						if(soctiers=="6001"||soctiers=="6002"||soctiers=="6003")
							soctiers="6000";
						if(RspMvt != undefined) Rsp = Rsp.clone(); else Rsp = undefined;
						if(getValue("CODSOC-BON",false,"","",true)=="0005"&&soctiers=="0004")
							soctiers="0005"
						if(getValue("CODSOC-BON",false,"","",true)=="6001"&&soctiers=="0006")
							soctiers="6001"
						if(getValue("CODSOC-BON",false,"","",true)=="6002"&&soctiers=="0006")
							soctiers="6002"
						if(getValue("CODSOC-BON",false,"","",true)=="6003"&&soctiers=="0006")
							soctiers="6003"
						if(soctiers!=getValue("CODSOC-BON",false,"","",true)&&getValue("CODSOC-BON",false,"","",true)!="")
						{
							alert("Soci�t� du tiers "+soctiers+" non autoris�e ("+getValue("CODSOC-BON",false,"","",true)+")");
							return;
						}
						if($("DICT�echantillon").getAttribute("obli")!="1"&&soctiers=="0004")
							$("DICT�echantillon").setAttribute("obli", "1");
					}
					Rsp=RspMagasi;
					//si soci�t� tiers diff de soci�t� d�pot alerte

					if(getValue("CODSOC-APP",false,"","",true)!="")
					{
						socdepot=getValue("CODSOC-APP",false,"","",true);
						Rsp=RspCodaux;

						if(socdepot!=getValue("CODSOC-APP",false,"","",true))
						{
							if(getValue("CODSOC-APP",false,"","",true)!="")
							{
								//test en dur si depot 0004 et tiers 0005 pas de message
								// avant if((socdepot!="0004" && socdepot!="0006")&& modif bp du01.08.2012
								if((socdepot!="0006")&&
							   (getValue("CODSOC-APP",false,"","",true)!="0005"&&
								getValue("CODSOC-APP",false,"","",true)!="6001"&&
								getValue("CODSOC-APP",false,"","",true)!="6002"&&
								getValue("CODSOC-APP",false,"","",true)!="6003"))
								{
									alert("Soci�t� du tiers "+getValue("CODSOC-APP",false,"","",true)+" diff�rente de la soci�t� du magasin "+socdepot);
								}

							}
						}
					}
				}//Fin sp� 677
				
				var backupDoCallback = doCallBack;
				
				if($("DICT�typtrp-ctr").value.trim()!="")
					dogexchange($("DICT�typtrp-ctr"),"GET", false);
				
				doCallBack = backupDoCallback;
				
				//on verifie si le fichier produit par rapport au tiers
				Rsp = RspCodcer.clone();
				
				artSoc = "";
				for(i=1;i<=10;i++)
					if(getValue("SOCIETE_"+i,false,"","",true)!="")
						artSoc=artSoc+getValue("SOCIETE_"+i,false,"","",true)+",";
				Rsp=RspCodaux;

				if(getValue("CODSOC-APP",false,"","",true)!=""&&artSoc!="")
				{
					if(artSoc.match(getValue("CODSOC-APP",false,"","",true))==null)
					{
						alert("Soci�t� du tiers : " + getValue("CODSOC-APP",false,"","",true) + " non autoris�e pour cet article" );
						return false;
					}
				}
				
				Rsp=RspCodaux;
				
				getValue("CODAUXGES" , false ,"" ,"DICT�codauxges" ,true);
				getValue("ADRESSE_1" , false ,"" ,"DICT�codauxges_libel" ,true);
				
				//Gestion chauffeur
				if($("DICT�codaux_chauff").value.trim() == "")
				{
					getValue("TYPAUX"    , false ,"" ,"DICT�typaux_chauff" ,true);
					$("DICT�typaux_chauff").defaultValue = $("DICT�typaux_chauff").value;
					getValue("CODAUXGES" , false ,"" ,"DICT�codaux_chauff" ,true);
					$("DICT�codaux_chauff").defaultValue = $("DICT�codaux_chauff").value;
					getValue("ADRESSE_1" , false ,"" ,"DICT�codaux_chauff_libel" ,true);
				}

				getValue("COMONIC" , false ,"" ,"DICT�comonic" ,true);
				
				//on regarde si la saisie de l adresse est obligatoire (pour cela il faut etre en achat)
				if(getValue ( "SAI-ADRES"  , false , "" , ""  , true)=="1")
				{
					Rsp=RspCodmvt.concat();

					// si on est en achat
					if(getValue ( "TYPTRANS"  , false , "" , ""  , true)=="2")
					{
						$("DICT�adr-liv_1").setAttribute("obli", "1");

						//on saisi l adresse de livraison
						ctrlReprise();

                        // Pte 12/04/2016, On affiche le pave des adresses si l'adresse n'est pas renseign�e
                        if ($("DICT�codauxges").value.trim() != "" && $("DICT�cod-adrliv").value.trim() == "") adrliv();

                        $(oldId).defaultValue = $(oldId).value;
						return true;
					}
				}
				
				//on verifie eventuellemnt les reprises
				ctrlReprise();

				Rsp=RspCodaux;
				
				getValue("ADRESSE_1" , false ,"" ,"DICT�codauxges_libel" ,true);
				getValue("CODAUXGES" , false ,"" ,"DICT�codauxges" ,true);

				/* //734CT936 saisie codaux avant produit bnj
				if(m_saitie.trim() != ""
				&& m_saitie.indexOf($("DICT�codmvt").value.trim().toUpperCase()) != -1
				&& m_ctrprod.trim() != ""
				&& $("DICT�cer-contrat").value.trim() == "" && $("DICT�codcer").value.trim() == ""
				&& $("DICT�cer-contrat").style.display != "none"
				&& $("DICT�cer-contrat").style.visibility != "hidden")
				{
					// trace("		SPECIF 734  ==>  Recherche sur CONTRAT PROD")
					inputParameter = "CODSOC�<?=$c_codsoc?>�TYPCTR�"+ typctr.trim() +"�CLOTURE�0�FILIERE�0"+"�TYPAUX�"+$("DICT�typaux").value+"�CODAUXGES�"+$("DICT�codauxges").value+"�CODCER�"+$("DICT�codcer").value+"�CAMPAGNE�"+$("DICT�campagne").value+"�CODMVT�"+$("DICT�codmvt").value+"�CERGPMT�"+$("DICT�codcer-ori").value+"�PROG�ASBONAPP�DATMVT�" + $("DICT�cer-datsai").value;
					dogFonction    = "F10-cerctr�a-cerctr";
					retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");
					
					//Pr�sence d'un contrat. => lancement �cran de recherche des contrats.
					if (retourExchange == "OK" && RspContratProd.size() == 0)
					{				
						//On lance la recherche des contrats.
						
						// trace("		Pr�sence contrat et RspContratProd = []")
						
						var PhpAuxges = "<?=search::searchExe("ar_contrat_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_autoload=O&_typctr=A&_clot=0&_typaux="+$("DICT�typaux").value+"&_codauxges="+$("DICT�codauxges").value+"&_codauxgeslibel="+$("DICT�codauxges_libel").value + "&_camp="+$("DICT�campagne").value+ "&_codmvt="+$("DICT�codmvt").value+"&_filiere=O&_param=N&_prog=asbonapp" + "&_datmvt=" + $("DICT�cer-datsai").value + "&_typcon=" + m_ctrprod;
						
						Rsp=RspCodmvt.concat();
						
						if(getValue ( "TYPTRANS"  , false , "" , ""  , true) == "1")
							PhpAuxges = "<?=search::searchExe("ar_contrat_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_autoload=O&_typctr=V&_clot=0&_typaux="+$("DICT�typaux").value+"&_codauxges="+$("DICT�codauxges").value+"&_codauxgeslibel="+$("DICT�codauxges_libel").value + "&_camp="+$("DICT�campagne").value + "&_codmvt="+$("DICT�codmvt").value+"&_filiere=O&_param=N&_prog=asbonapp" + "&_datmvt=" + $("DICT�cer-datsai").value + "&_typcon=" + m_ctrprod;
						
						var sFeatures = "dialogHeight:690px;dialogWidth:975px;status:no;help:no";
						var retourModale = showModalDialog ( PhpAuxges , "" , sFeatures) ;
						
						// trace("		retourModale : ")
						// trace(retourModale)
						
						if(retourModale == undefined)
						{
							doCallBack = backupCB;
							$("DICT�codcer").focus();
							return false;
						}
						
						if(retourModale[0] =="")
						{
							doCallBack = backupCB;
							$("DICT�codcer").focus();
							return false;
						}
						
						if(retourModale!=undefined)
						{
							RspContratProd = retourModale;
							
							$("DICT�codcer").value = getField(retourModale[1], 'CODCER');
							
							$('DICT�cer-contrat-fil').value = getField(retourModale[1], 'CER-CONTRAT');
							
							dogexchange($("DICT�codcer"), "GET", false);
							
							oldTrigger = "GET";
							
							if(m_ctrprod != "")
								oldId = "DICT�cer-contrat-fil";
							else
								oldId = "DICT�cer-contrat";
							
							retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , "�ROWID�" + getValueFromDataArray(retourModale, "ROWID", false, "") , "get-cerctr�a-cerctr" , "", dogexchangecallback);
							
							setTimeout("$('DICT�cer-contrat').focus(); ", 15);
							dogexchange($('DICT�cer-contrat'), 'F10');
							
							setTimeout('setFocus(TabFocusEntree, "DICT�cer-contrat-fil")', 50);
							return;
						}
						else
						{
							$("DICT�codcer").focus();
							return true;
						}
						
						return true;
					}
					else
					{
						// trace("		Pas contrat ou RspContratProd != '' ");
						
						$('DICT�cer-contrat-fil').focus();
						
						//on lance si param menu la recherche des contrats
						if(f10Contrat && topF10 && doCallBack && $("DICT�cer-contrat").value.trim() == ""
						&& $("DICT�cer-contrat").style.display != "none"
						&& $("DICT�cer-contrat").style.visibility != "hidden")
						{
							//337 - pour 734 - On se positionne sur la zone du contrat de production (DICT�cer-contrat-fil)
							//Si aucune zone n'a �t� renseign�, on ouvre directement l'�cran de recherche des contrats.
							
							// trace("			 Recherche NUMERO CONTRAT")
							
							 Rsp = RspCodaux;
							
							// getValue("ADRESSE_1" , false ,"" ,"DICT�codauxges_libel" ,true);
							// getValue("CODAUXGES" , false ,"" ,"DICT�codauxges" ,true);
							
							// initbufferscreen();
							// Rsp=RspCodmvt.concat();
							
							inputParameter = "CODSOC�<?=$c_codsoc?>�TYPCTR�"+getValue("TYPCTR",false ,"","",true)+"�CLOTURE�0"+"�TYPAUX�"+$("DICT�typaux").value+"�CODAUXGES�"+$("DICT�codauxges").value+"�CODCER�"+$("DICT�codcer").value+"�CAMPAGNE�"+$("DICT�campagne").value+"�CODMVT�"+$("DICT�codmvt").value+"�CERGPMT�"+$("DICT�codcer-ori").value+"�PARAM�N�FILIERE�N�PROG�ASBONAPP�DATMVT�" + $("DICT�cer-datsai").value;
							dogFonction    = "F10-cerctr�a-cerctr";
							retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");
							
							//si il y a des contrats on ouvre la recherche
							if (retourExchange == "OK" && getValue("TYPCTR",false ,"","",true)!="")
							{
								// trace("			 [NUMERO CONTRAT] - Pr�sence d'un contrat")
								
								//on charge un top
								topF10=false;
								$("DICT�cer-contrat").focus();
								
								Rsp=RspCodaux
								
								getValue("ADRESSE_1" , false ,"" ,"DICT�codauxges_libel" ,true);
								getValue("CODAUXGES" , false ,"" ,"DICT�codauxges" ,true);
								
								//Gestion chauffeur
								if($("DICT�codaux_chauff").value.trim() == "")
								{
									getValue("TYPAUX"    , false ,"" ,"DICT�typaux_chauff" ,true);
									$("DICT�typaux_chauff").defaultValue = $("DICT�typaux_chauff").value;
									getValue("CODAUXGES" , false ,"" ,"DICT�codaux_chauff" ,true);
									$("DICT�codaux_chauff").defaultValue = $("DICT�codaux_chauff").value;
									getValue("ADRESSE_1" , false ,"" ,"DICT�codaux_chauff_libel" ,true);
								}
								
								if(m_saitie.trim() == ""
								|| m_saitie.indexOf($("DICT�codmvt").value.trim().toUpperCase()) == -1
								|| m_ctrprod.trim() == ""
								|| $("DICT�cer-contrat").value.trim() != "" || $("DICT�codcer").value.trim() == "" || oldTrigger != "GET")
								{
									dogexchange($("DICT�cer-contrat"),"F10", false);
								} 
								return;
							}
							else
							{
								// trace("			 [NUMERO CONTRAT] - Aucun contrat");
								
								//Pas de contrat de production => on regarde le num�ro de contrat.
								
								// Avant de lancer l'�cran de recherche, on r�garde s'il y a des donn�es.
								var dogFct = "f10-cerctr�a-cerctr"
								var requete  = "CODSOC�<?=$c_codsoc?>�TYPCTR�"+getField(RspCodaux[1], "TYPCTR");
									requete += "�CODCER�"+$('DICT�codcer').value;
									requete += "�TYPAUX�"+$('DICT�typaux').value;
									requete += "�CODAUXGES�"+$('DICT�codauxges').value;
									requete += "�CAMPAGNE�"+$('DICT�campagne').value;
									requete += "�CODMVT�"+$('DICT�codmvt').value;
									requete += "�CLOTURE�0�CERGPMT�"+$("DICT�codcer-ori").value;
									requete += "�TYPE-TRANSP�*�MAGASI�"+$('DICT�magasi').value;
									requete += "�FILIERE�N�PARAM�N�PROG�asbonapp�DATMVT�" + $('DICT�cer-datsai').value;
								
								var retourExchange = sendExchangeInvoke("<?=$dogExchange?>",requete,dogFct,"","");
								
								if(retourExchange == "OK" && getValue("TYPCTR",false ,"","",true)!="")
								{
									// trace("			 [NUMERO CONTRAT] -Il y a contrat ==> dogexchange avec DICT�cer-contrat");
									
									$('DICT�cer-contrat').focus();
									dogexchange($('DICT�cer-contrat'), 'F10');
								
									setTimeout('if($("DICT�codcer").value == ""){$("DICT�codcer").focus()}else{$("DICT�natcon").focus()}', 50);
									return;
								}
								else
								{
									// trace("			 [NUMERO CONTRAT] -RAS ==> Focus sur nature contrat");
									
									setTimeout('if($("DICT�codcer").value == ""){$("DICT�codcer").focus()}else{$("DICT�natcon").focus()}', 50);
									return;
								}
							}
						}
						return;
					}
				} */
				
				/** ---------  O R D R E    D E   S A I S I E --------- **/
				<?if(file_exists(search::searchExe("as_ordre_saisie.js",0)))
				  {
				?>
					try
					{
						if(!modifBon)
						{
							if(zoneSuivanteSpe('CODETIERS'))
								return;						
						}
					} catch(ex){trace("---zoneSuivanteSpe : " + ex)};
				<?}?>
				
				Rsp=RspCodaux

				

				oldId = "DICT�codauxges";
				break;

			case "DICT�codaux_chauff":
				getValue("CODAUXGES" , false ,"" ,"DICT�codaux_chauff" ,true);
				getValue("ADRESSE_1" , false ,"" ,"DICT�codaux_chauff_libel" ,true);
				break;

			case "DICT�codmvt" :
                if (okInsert == true && isCreation && getField(RspCodmvt[1],"codmvt") != "" && getField(RspCodmvt[1],"codmvt") != getField(Rsp[1],"codmvt"))
                {
					var RspTemp = Rsp.concat()
					var strTemp = $("DICT�opecre2").value
					isSaisieEnCours=false
					boninserer()
					$("DICT�opecre2").value = strTemp
					Rsp = RspTemp.concat()
					setTimeout('setFocus(TabFocusEntree, "DICT�codmvt")', 150);
				}
				topnext=false
				RspCodmvt=Rsp.concat();
				
				//on verifie si le mouvement est autoris�
				if(getValue("AUTORI",false,"","",true)=="1"){
					alert("Code mouvement non autoris� !")
					return;
				}

				//Suivi depot
				if(getValue("SUIVI-DEPOT", false, "", "", true) == "0" && "<?$ident?>" == "333")
				{
					Rsp = RspOperat.clone();
					if(getValue("KES-OPMAIT", false ,"" ,"", true)=="O" || getValue("KES-OPMAIT", false ,"" ,"", true)=="")
					{
						if(!confirm("�tes-vous s�r(e) de vouloir cr�er ce mouvement au si�ge ?"))
							return false;
					}
					Rsp = RspCodmvt.concat();
				}
				
				//on autorise dans certain cas la modification du type de bon
				if("<?=$_modmvt?>"!=""&& $("DICT�codcer").value.trim()!=""){
					if(getValue("CODMVT",false,"","",true)=="")
						return;
					
					switch("<?=$_modmvt?>"){
						case "O" :
							Rsp=RspMvt;
							strtemp=getValue("CER-TYPFAC",false,"","",true)
							Rsp=RspCodmvt.concat();
							if(getValue("TYPCTR",false,"","",true)!=strtemp && strtemp!="") {
								alert("Code mouvement non autoris� !")
								return;
							}
							break;
						case "U" : //test pour les unions
							break;
					}
					
					//on relance la batterie de test pour la gestion de l'ecran
					//on s occupe ici de l affichage des zones a l ecran
					//Recherche du parametrage du couple Mvt/Codcer
					var inputParameter = "�CODSOC�<?=$c_codsoc?>�CODCER�" + $("DICT�codcer").value + "�CODMVT�" + $("DICT�codmvt").value+"�CAMPAGNE�"+$("DICT�campagne").value+"�MAGASI�"+$("DICT�magasi").value;
					retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , "PRM-CODMVT-CODCER�a-parapp", "" , "" )
					
					//on regarde si on a ramene qque chose
					if (retourExchange != "OK"){
						return;
					}
					
					//on teste si le couple mvt / cereale est autorise
					if(getValue ( "MVTCER-AUTORI" ,false ,"" ,""  , true)=="1"){
						alert("Le couple Code Mouvement/Code c�real saisi n'est pas autoris�, veuillez en saisir un autre !")
						$("DICT�codmvt").value="";
						$("DICT�codmvt").focus();
						return;
					}
					
					RspCoderMvt = Rsp.clone();
					ParamAfficheZonesCereale();
					
					//on restaure
					Rsp=RspCodmvt.concat();
					//on vide si besoin le numero de contrat
					if("<?=$_modctr?>"!="" && isCreation)
					{
						$("DICT�cer-contrat").value="";
						$("DICT�cer-contrat").defaultValue="";
					}
				}

				if(getValue ( "TYPTRANS"  , false , "" , ""  , true)=="3" && getValue ( "TYPMVT"  , false , "" , ""  , true)=="1")
					$("DICT�magas2").title="F8 pour les rapprochements";
				else
					$("DICT�magas2").title="";

				getValue ( "CODMVT"  , false , "" , oldId  , true);
				getValue ( "LIBEL"  , false , "" , oldId + "_libel"  , true);

				// Pte Gestion des lots apports
				ccTypMvt = getValue("TYPMVT", false, "", "", true);

				if(getValue("TENU-STOCK",false,"","",true)!="1")
					tenustock = true;
				else
					tenustock = false;
				
				if("<?=$_modmvt?>"=="")
					$("DICT�codcer").defaultValue="";
				//parametrage de l'obligation des chargements
				switch(getValue ("CHARG_1", false, "", "", true)) 
				{
					case "0" :
						zonscreenget("�protrs_1�"+ $("DICT�protrs_1").value +"�0")
						break;
					case "1" :
						zonscreenget("�protrs_1�"+ $("DICT�protrs_1").value +"�3")
						break;
					case "2" : case "" :
						zonscreenget("�protrs_1�"+ $("DICT�protrs_1").value +"�1")
						break;
				}
				
				switch(getValue ("CHARG_2", false, "", "", true)) {
					case "0" :
						zonscreenget("�protrs_2�"+ $("DICT�protrs_2").value +"�0")
						break;
					case "1" :
						zonscreenget("�protrs_2�"+ $("DICT�protrs_2").value +"�3")
						break;
					case "2" : case "" :
						zonscreenget("�protrs_2�"+ $("DICT�protrs_2").value +"�1")
						break;
				}
				
				switch(getValue ("CHARG_3", false, "", "", true)) {
					case "0" :
						zonscreenget("�protrs_3�"+ $("DICT�protrs_3").value +"�0")
						break;
					case "1" :
						zonscreenget("�protrs_3�"+ $("DICT�protrs_3").value +"�3")
						break;
					case "2" : case "" :
						zonscreenget("�protrs_3�"+ $("DICT�protrs_3").value +"�1")
						break;
				}
				
				// le code auxiliaire par defaut
				if(getValue ("TYPAUX", false, "", "", true)!="" && $("DICT�typaux").value.trim()=="")
					getValue ( "TYPAUX"  , false , "" , "DICT�typaux"  , true);
				
				if(doCallBack)
					$("DICT�typaux").defaultValue = "";
				
				// le type de transporteur par defaut
				if($("DICT�typtrp-ctr").value.trim()=="")
					getValue ( "TYPTRP"  , false , "" , "DICT�typtrp-ctr"  , true);
				
				typctr = getValeurRsp('TYPCTR');
				
				//on regarde si la saisie des commentaires est obligatoires
				
				if(getValue ( "SAILIB"  , false , "" , ""  , true)=="O"){
					$("DICT�codeparc").setAttribute("obli", "1");
				}
				
				//on regarde si la saisie des caracteristiques est obligatoire
				if(getValue ( "CTRL-CARAC"  , false , "" , ""  , true)=="1"){
					//on controle pas les caracteristiques
					IsCtrlCaract = false
				} 
				else 
				{
					//on controle les caracteristiques
					IsCtrlCaract = true
				}
				
				if(getValue ( "CTRL-CARAC" ,false ,"" ,""  , true)=="3")
				{
					IsCtrlCaract2 = true
				} 
				else 
				{
					IsCtrlCaract2 = false
				}

				//on ne passe pas sur les caracteristiques
				if(getValue ( "CTRL-CARAC"  , false , "" , ""  , true)=="2")
					for(kk=1;kk<=14;kk++) 
					{
						zonscreenget("�val-nego_"+kk+"��0�val-etat_"+kk+"��0�")  //gris�
						
						//pour les operateurs depots on cache les valeurs negociees
						RspTmp = Rsp.clone();
						Rsp=RspOperat.clone();
						
						if(getValue("KES-OPMAIT", false ,"" ,"", true)=="N")
							$("DICT�val-nego_"+kk).style.display="none";

						Rsp = RspTmp
						
						if($("DICT�libcar_"+kk).value=="")
							zonscreenget("�val-nego_"+kk+"��2�val-etat_"+kk+"��2�libcar_"+kk+"��2�val-etat-ctr_"+kk+"��2�")  //invisible
					}

				zonscreenget("�val-nego_4��2�val-etat_4��2�libcar_4��2�val-etat-ctr_4��2�")  //invisible

				//on regarde si on est en achat vente ou transfert
				switch(getValue ( "TYPTRANS"  , false , "" , ""  , true))
				{
					case "1": //vente
						$("ong3_p").innerHTML="Info Vente"
						$("DIVV").style.display="";
						$("DIVC").style.display="";
						$("DIVA").style.display="none";
						break;
					
					case "2": //achat
						$("ong3_p").innerHTML="Info Achat"
						$("DIVV").style.display="none";
						$("DIVC").style.display="none";
						$("DIVA").style.display="";
						break;
					
					case "3": //transfert
						$("ong3_p").innerHTML="Info Transfert"
						$("DIVV").style.display="none";
						$("DIVC").style.display="none";
						$("DIVA").style.display="none";
						break;
				}
				
				//on regarde si on doit afficher le vide cellule
				ctrlvidecellule();
				
				//si operatyeur depot pas onglet achat
				Rsp=RspOperat.clone();
				
				if(getValue("KES-OPMAIT", false ,"" ,"", true) == "N")
				{
					$("DIVV").style.display="none"
					//$("DIVC").style.display="none"
					$("DIVA").style.display="none"
					$("DIVG").style.display="none"
				}
				
				topnext=true
				Rsp=RspCodmvt.concat();

				//on regarde si on doit bloaquer la saisie du poids et des caracteristiques
				if(getValue ("MODTRF", false, "", "", true)=="N"){
					//les caracteristiques
					for(kk=1;kk<=14;kk++) {
						zonscreenget("�val-nego_"+kk+"��9�val-etat_"+kk+"��9�")  //gris�
					}
					zonscreenget("�pdsbrut��9�tare��9")
				}

				//On r�cup�re la liste des typaux autoris�s sur ce mouvement
				if(getValue("TYPAUX-AUT", false, "", "", true).trim() != "")
					typaux_aut = getValue("TYPAUX-AUT", false, "", "", true).split(",");
				else
					typaux_aut = [];

				if(getValue("ACTION",false,"","",true) == "OE")
				{
					//on ouvre la recherche des ordres d execution
					Rsp=RspCodmvt.concat();
					
					if(getValue ( "TYPTRANS"  , false , "" , ""  , true) == "1")
					{
						//si la zone numoex est deja chargee ne pas relancer la recherche
						if($("DICT�numoex").value.trim()=="" && isCreation)
						{
							initbufferscreen();
							//on lance une recherche des contrats
							// trace("		#CHARGEMENT - 2")
							
							PhpAuxges = "<?=search::searchExe("ar_numvoe_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_typctr=VCD&_camp="+$('DICT�campagne').value+"&_magasi="+$("DICT�magasi").value + "&_parame=0";
							sFeatures = "dialogHeight:620px;dialogWidth:940px;status:no;help:no";
							retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
							
							if(retourExchange!=undefined && retourExchange!="")
							{
								//acces execution
								retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , "�ROWID�cerctr|" + retourExchange + "�PARAME�0" , "get-ceroex�a-cerctr" , "" , "" );
								RspVoe=Rsp.clone();
								
								if(getValue("CODMAG",false,"","",true).trim()!=$("DICT�magasi").value.trim())
									alert("Attention cet ordre d'esp�dition est rattach� sur un magasin diff�rent!");
								
								if(getValue("CER-CONTRAT", false, "", "",true).trim()=="")
									return false;
								
								if(getValue("CLOTURE", false, "", "",true).trim()!="")
								{
									alert("Contrat clotur�!")
									return
								}
								
								//on precharge les zones de commentaire
								getValue("REF-BL",false,"","DICT�ref-bl",true);
								getValue("LIB-FIL",false,"","DICT�lib-fil",true);
								getValue("DEST-FIN",false,"","DICT�dest-fin",true);
								
								RspLiv=[];
								RspLiv[0]="OK"
								
								gridExec.setCellData([]);
								gridExec.setRowCount(0);
								gridExec.refresh();
								
								//on pointe sur les lignes
								for(var kk=0;kk<getValue("LL-NUMLIG",false,"","",true).split(",").length;kk++)
								{
									i=parseFloatStr(getValue("LL-NUMLIG",false,"","",true).split(",")[kk])
									if(!isNaN(i))
									{
										RspLiv[kk+ 1] = "�ROWID�rowid|id"+i+"�CHRONO�"+i
										RspLiv[kk+ 1] += "�LL-NUMLIG�"+i
										RspLiv[kk+ 1] += "�LL-PRXTRP�"+getValue("LL-PRXTRP",false,"","",true).split(",")[kk];
										RspLiv[kk+ 1] += "�DAT-MIN�"+getValue("DAT-MIN",false,"","",true).split(",")[kk];
										RspLiv[kk+ 1] += "�DAT-MAX�"+getValue("DAT-MAX",false,"","",true).split(",")[kk];
										RspLiv[kk+ 1] += "�REFER-CNT�"+getValue("REFER-CNT",false,"","",true).split(",")[kk];
									}
								}
								
								//on vide la variable
								currowExec = "";
								Rsp=RspLiv;
								gridExec.loadTab();
								
								$("EXECUTION").show();
								
								if(gridExec.getRowCount() == 0)
								{
									$("EXECUTION").hide();
									alert("Pas de lignes!");
									$("DICT�cer-contrat").focus()
									if($("DICT�codcer").value.trim()=="")
										$("DICT�codcer").focus()
									return false;
								}
								
								//accesd au contrat
								Rsp=RspVoe
								
								getValue("NUMOEX", false, "", "DICT�numoex",true)
								getValue("ADR-LIV_1", false, "", "DICT�adr-liv_1",true)
								getValue("ADR-LIV_2", false, "", "DICT�adr-liv_2",true)
								getValue("ADR-LIV_3", false, "", "DICT�adr-liv_3",true)
								getValue("ADR-LIV_4", false, "", "DICT�adr-liv_4",true)
								getValue("ADR-LIV_5", false, "", "DICT�adr-liv_5",true)
								getValue ( "TRANSP"  , false , "" , "DICT�transp"  , true);
								getValue ( "LIBEL"  , true , "TRANSP" , "DICT�transp_libel"  , true);
								
								//*** rajout test pour liberer libell�
								RspTmp = Rsp
								
								if(getValue("TRANSP", false, "", "",true).trim()!="")
								{
									inputParameter = "TRANSP�"+getValue("TRANSP", false, "", "",true).trim();
									retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , "get-transp�n-transp" , "" , "" );
									// retourExchange  = sendExchangeTurbo ( "<?=$dogExchange?>", "" , str_val , "get" ,"" ,"MESSAGE=acces transporteur...|GETVAR|NOSCREEN|NODISPLAYERROR|TRUEDMD", "" , "get-transp�n-transp"   ) ;
									
									if(getValue ( "TYPTRANSP", false, "", "", true)=="D")
										zonscreenget("�transp_libel��11");
									else
										zonscreenget("�transp_libel��9");
								}
								
								Rsp = RspTmp;
								
								//si on a un transporteur temporaire(valeur donn�e au niveau du menu) comme pour valfrance on autorise la modif
								zonscreenget("�transp��9�transp_libel��9");
								
								if("<?=$_trpdep?>"!=""&&$("DICT�transp").value=="<?=$_trpdep?>")
									zonscreenget("�transp��11")
								if("<?=$_trprend?>"!=""&&$("DICT�transp").value=="<?=$_trprend?>")
									zonscreenget("�transp��11")
								
								inputParameter = "CODSOC�<?=$c_codsoc?>�TYPCTR�V�CER-CONTRAT�"+getValue("CER-CONTRAT", false, "", "",true).trim();
								retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , "GET-CERCTR�a-cerctr" , "" , "" );

								RspContrat = Rsp.clone();
								
								getValue ( "CODCER"  , false , "" , "DICT�codcer"  , true);
								$("DICT�codcer").defaultValue = $("DICT�codcer").value
								getValue ( "LIBEL"  , true , "CODCER" , "DICT�codcer_libel"  , true);
								
								for(i=1; i<=14; i++)
									getValue ( "VAL-ETAT-CTR_"+i  , false , "" , "DICT�val-etat-ctr_"+i  , true);

								currow2=0;
								
								getValue ( "TYPAUX"  , false , "" , "DICT�typaux"  , true);
								getValue ( "CODAUXGES"  , false , "" , "DICT�codauxges"  , true);
								getValue ( "LIBEL"  , true , "CODAUXGES" , "DICT�codauxges_libel"  , true);
								getValue ( "CER-CONTRAT"  , false , "" , "DICT�cer-contrat"  , true);
								getValue ( "CODCER"  , false , "" , "DICT�codcer"  , true);
								
								if(topnatcon)
									setValueFromKeyword ( "TYPCON"  , false , "" , "DICT�natcon", true);
								
								$("DICT�codcer").defaultValue = $("DICT�codcer").value;
								
								initbufferscreen();
								
								$("DICT�codcer").defaultValue="";
								
								// event.keyCode = ""
								dogexchange($("DICT�codcer"), "GET", false);
								
								Rsp=RspContrat;
								
								//on va verifier le code mouvement associ� au contrat
								retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , "�MOTCLE�TYPCON-CER�CODTAB�" + getValue("TYPCON",false,"","",true) , "GET-TABGEN�a-tabgen" , "" , "" );
								// retourExchange = sendExchangeTurbo ( "<?=$dogExchange?>" , "" , "�MOTCLE�TYPCON-CER�CODTAB�" + getValue("TYPCON",false,"","",true), "get" , "" , "GETVAR|NOSCREEN|NODISPLAYERROR" ,"","GET-TABGEN�a-tabgen") ;
								//on regarde si on a ramene qque chose
								if (retourExchange != "OK")
									return false;

								RspTmp=Rsp;
								
								//si tenu-stock alors
								Rsp=RspContrat;
								
								getValue ( "CER-CONTRAT"  , false , "" , "DICT�cer-contrat" , true);
								
								if(topnatcon)
									setValueFromKeyword ( "TYPCON"  , false , "" , "DICT�natcon", true);
								
								$("DICT�cer-contrat").defaultValue=$("DICT�cer-contrat").value;
								
								if(tenustock==true)
								{
									//si oui on change le code mouvement
									Rsp=RspTmp;
									if(getValue("LIBEL1-2",false,"","",true)!=""){
										//et on change le code mvt
										getValue("LIBEL1-2",false,"","DICT�codmvt",true);
										//on se refait des get
										dogexchange($('DICT�codmvt'), "GET", false);
										$("DICT�codcer").defaultValue = "";
										dogexchange($('DICT�codcer'), "GET", false);
									}
								}
								
								trigger = "GET";
								$("DICT�codcer").defaultValue = $("DICT�codcer").value;
							}
						}
					}
				}
				
				retourRowid = "";
				if($("DICT�typtrp-ctr").value.trim()!=""){
					$("DICT�typtrp-ctr").defaultValue = "";
					dogexchange($("DICT�typtrp-ctr"),"GET", false)
					oldId="DICT�codmvt"
					$("DICT�codmvt").focus()
				}
				
				//Gestion des bons de retour (CRV)
				if(getValue("RETOUR", false, "", "", true).trim() != "" && isCreation)
				{
					PhpAuxges = "<?=search::searchExe("ar_mvt_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_retour=" + getValue("RETOUR", false, "", "", true).trim() + "&_campagne="+$('DICT�campagne').value;
					sFeatures = "dialogHeight:620px;dialogWidth:940px;status:no;help:no";
					retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
					
					if(retourExchange != "NODATA" && retourExchange != "" && retourExchange != undefined)
					{
						retourRowid = retourExchange;
						var dogFonction = "get-cermvt�a-cermvt";
						var inputParameter = "ROWID�" + retourRowid;
						retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "" , "" );
						
						RspCerMvt = Rsp.clone();
						
						// Charger les bonnes zones, faire les gets puis verrouill�s les zones qui vont bien (toutes sauf le poids) %%bnj
						var descendants = Element.descendants("contenuOng1");
						
						for(var i = descendants.length-1; i >= 0 ; i--)
							if(descendants[i].tagName.toUpperCase().trim() != "INPUT" && descendants[i].tagName.toUpperCase().trim() != "SELECT")
								descendants.splice(i, 1);
						
						// trace(descendants)
						for(var i = 0, libel = false; i < descendants.length; i++, libel = false)
						{
							if(descendants[i].id == "DICT�codmvt")
								continue;
							
							var motcle = descendants[i].id.split("�")[1];
							
							if(motcle == undefined)
							{
								// trace(descendants[i].id);
								continue;
							}
							
							if(motcle.indexOf("libel") != - 1)
							{
								libel = true;
								motcle = motcle.split("_")[0].trim();
							}

							//Gestion manuels des motcles diff�rents
							switch(motcle.toLowerCase())
							{
								case "no-ordre":
									motcle = "numoex";
									break;

								case "typaux_chauff" :
									motcle = "typaux-chauff";
									break;

								case "codaux_chauff" :
									motcle = "codaux-chauff";
									break;

								case "typtrp-ctr":
									motcle = "typtrp";
									break;

								case "numpesee" :
									motcle = "num-pesee"
									break;

								case "codauxges":
									break;

								case "cellule":
								case "rotation":
									motcle = "ne pas faire -" + motcle;
									break;
								case "codmvt":
									if(libel)
										motcle = "ne pas faire -" + motcle;
									break;
							}

							if(!libel)
							{
								if(setValueFromKeyword(motcle, false, "", "", true).trim() != "")
									setValueFromKeyword(motcle, false, "", descendants[i].id, true);
							}
								// else
									// trace("motcle non correct (ou non trouv�) : " + motcle)
							else
							{
								if(setValueFromKeyword("libel", true, motcle, "", true).trim() != "" && motcle != "CODAUXGES")
									setValueFromKeyword("libel", true, motcle, descendants[i].id, true);
								else if(setValueFromKeyword("adresse_1", true, motcle, "", true).trim() != "" && motcle == "CODAUXGES")
									setValueFromKeyword("adresse_1", true, motcle, descendants[i].id, true);								
							}
									// trace("motcle non correct (ou non trouv�) : " + motcle)
						}

						//Lancer les �changes pour les param�tres
						dogexchange($("DICT�codcer"), "GET", false);

						//Bloquer les zones
						enableZone("ALL", false, descendants);
						enableZone("DICT�cellule", true);
						
						if("<?=$ident?>" != "333")
						{
							enableZone("DICT�pdsbrut|DICT�tare", true);
							for(var i = 1; i <= 14; i++)
							{
								if($("DICT�val-etat_" + i).style.visibility != "hidden")
								{
									enableZone("DICT�val-etat_" + i, true);
									enableZone("DICT�val-nego_" + i, true);
								}
							}
						}
					}
					else
					{
						$(strId).value = "";
						$(strId + "_libel").value = "";
						$(strId).focus();
						return false;
					}
				}

				/* //734CT936 saisie codaux avant produit bnj
				if(m_saitie.indexOf($(oldId).value.trim().toUpperCase()) != -1)
				{
					$("DICT�typaux").focus();
					$("DICT�typaux").select();
					$(oldId).defaultValue = $(oldId).value;
					return true;
				} */

				
				/** ---------  O R D R E    D E   S A I S I E --------- **/
				<?if(file_exists(search::searchExe("as_ordre_saisie.js",0)))
				  {
				?>
				
					try
					{
						if(!modifBon)
						{
							if(zoneSuivanteSpe('MOUVEMENT'))
								return;						
						}
					} catch(ex){trace("---zoneSuivanteSpe : " + ex)};
				<?}?>
				
				
				//337 avec le motcl� menu mvtvte = CCV, si on saisie CCV comme code mouvement pour un nouveau bon, 
				//on peut rechercher un num�ro de contrat.
				if("<?=$_mvtvte?>".toUpperCase() == $('DICT�codmvt').value.toUpperCase())
				{
					$('RECHBON').show();
					setTimeout("$('DICT�num-ctr').focus()",200);
				}
				
			break;
			
			case "DICT�codcer" :		
				$("DICT�cer-contrat").defaultValue = ""
				
				RspCodcer=Rsp.clone();
				
				if($("DICT�cer-contrat-fil").value!="")
				{
					$("DICT�cer-contrat-fil").defaultValue="";
					dogexchange($("DICT�cer-contrat-fil"),"GET", false);
				}
				
				oldId = "DICT�codcer";
				Rsp = RspCodcer.clone();
				getValue("CODCER",false,"",oldId,true);
				getValue("CODCER-REG",false,"","DICT�codcer-ori",true);
				getValue("LIBEL",false,"",oldId + "_libel",true);
				
				if("<?=$_rotation?>" == "O")
				{
					$("DICT�cellule").defaultValue = "";
					g_doFocus = false;
					dogexchange($("DICT�cellule"), "GET", doCallBack);
					g_doFocus = true;
				}
				else
				{
					Rsp = RspCellule;
					typcel = getValue("LIBEL1-2", false, "", "", true).toUpperCase();
					$(oldId).title = $(oldId + "_libel").value.trim();
					RspCellule=Rsp.clone();

					//on verifie la cellule
					if(typcel != "V")
					{
						if(!modifBon && !ctrlCellule())
						{
							$(oldId).title = "" ;
							return;
						} 
					}
				}
				
				trace("PARAM CARACTERIQTIQUES")
				trace(modifBon)
				trace(topavoir)
								
				/*-- G E S T I O N    C A R A C T E R I S T I Q U E S --*/
				if(!modifBon && !topavoir && !bonIssuGenenerationPlusMoins)
					for(var i=1;i<=14;i++)
					{
						//On vide les zones de la caract.
						clearZone('DICT�val-etat-ctr_'+i + '|DICT�val-etat_'+i + '|DICT�val-nego_'+i);
				
						$('DICT�libcar_'+i).value = getField(RspCodcer[1],'libcar_'+i);
						$('DICT�val-nego_'+i).value = getField(RspCodcer[1],'val-nego_'+i);
						
						if(getField(RspCodcer[1],'val-etat_'+i) == "")
						{
							topChge = false;
							
							if($("DICT�libcar_"+i).value.trim() == getField(RspCodcer[1],'libcar_'+i))
								topChge=true;
							
							if(topChge==false)
								$("DICT�val-etat_"+i).value = "0";
							
							if($("DICT�val-etat_"+i).value == "")
								$("DICT�val-etat_"+i).value = "0";
						}
						else
							$('DICT�val-etat_'+i).value = getField(RspCodcer[1],'val-etat_'+i)					
						
						if(getField(RspCodcer[1],'topcar_'+i) == "")
						{
							enableZone('DICT�val-etat_'+i+'|DICT�val-nego_'+i,false);
							clearZone('DICT�val-etat_'+i+'|DICT�val-nego_'+i);
						}
						
						if(getField(RspCodcer[1],'CODCAR_'+i).trim().toUpperCase() == "IHU")
						{
							enableZone('DICT�val-etat_'+i+'|DICT�val-nego_'+i,false);
							clearZone('DICT�val-etat_'+i+'|DICT�val-nego_'+i);
						}
						
						//Interdiction de modification de caract�ristiques par un op�rateur d�p�t
						if(getField(RspCodcer[1],'MODIFCAR_'+i).trim().toUpperCase() == "N")
							enableZone('DICT�val-etat_'+i+'|DICT�val-nego_'+i,false);
						else
							enableZone('DICT�val-etat_'+i+'|DICT�val-nego_'+i,true);
						
						if($("DICT�libcar_"+i).value.trim() == "")
							$('caract_'+i).style.visibility = 'hidden';
						else if(i != 4) /*On affiche pas la 4e caracteristique*/
							$('caract_'+i).style.visibility = 'visible';
							
							
					} 
				/* -------------------------------------------------------------------------------------- */
				
				
				
				/*Si on change la c�r�ale, on reporte les valeures des caracteristiques communes entre les 2 cereales*/
				if(oldCodcer != "" && oldCodcer != $(oldId).value)
					repositionnerCaractCommunes()
				
				/*On sauvegarde la valeur des caracteristiques*/
				sauvegardeCaracteristiques();
				switch(typctr.trim().toUpperCase())
				{
					case 'A':
						if($("DICT�val-etat_2").value == "0" || $("DICT�val-etat_2").value == "")
							$('DICT�val-etat_2').value = getField(RspCodcer[1],'TAUX-IMPUR');
						
						if(getField(RspCodcer[1],'LIBACH_1').trim() != "")
							$('DICT�codcer_libel').value = getField(RspCodcer[1],'LIBACH_1').trim();
						break;
						
					case 'V':
						if(getField(RspCodcer[1],'LIBVTE_1').trim() != "")
							$('DICT�codcer_libel').value = getField(RspCodcer[1],'LIBVTE_1').trim();
						break;
				}
				//Recherche du parametrage du couple Mvt/Codcer
				retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�CODSOC�<?=$c_codsoc?>�CODCER�" + $("DICT�codcer").value + "�CODMVT�" + $("DICT�codmvt").value+"�CAMPAGNE�" + $("DICT�campagne").value+"�MAGASI�" + $("DICT�magasi").value, "PRM-CODMVT-CODCER�a-parapp", "", "");
				//on regarde si on a ramene qque chose
				if (retourExchange != "OK")
					return;

				RspCoderMvt=Rsp.clone();
				//on teste si le couple mvt / cereale est autorise
				if(getValue ( "MVTCER-AUTORI" ,false ,"" ,""  , true)=="1")
				{
					alert("Le couple Code Mouvement/Code c�real saisi n'est pas autoris�, veuillez en saisir un autre !");
					Rsp="";
					inputParameter="";
					$("DICT�codcer").value="";
					$("DICT�codcer").defaultValue="";
					$('DICT�cer-contrat-fil').value = "";
					$('DICT�cer-contrat-fil').defaultValue = "";
					$("DICT�codcer").focus();
					return;
				}
				
				ParamAfficheZonesCereale();
				ParamAfficheContratFiliere();
				
				//on regarde si on doit bloquer la saisie du poids et des caracteristiques
				RspTemp=Rsp
				Rsp=RspCodmvt.concat();
				if(getValue ("MODTRF", false, "", "", true)=="N")
				{
					//les caracteristiques
					for(kk=1;kk<=14;kk++) {
						zonscreenget("�val-nego_"+kk+"��9�val-etat_"+kk+"��9�")  //gris�
					}
					zonscreenget("�pdsbrut��9�tare��9")
				}
				
				Rsp=RspTemp
				//si la selection de la ligne d'executuion est active

				if($("EXECUTION").style.display=="")
				{
					//On selectionne la premiere ligne
					if( gridExec.getRowCount() == 0)
					{
						$("EXECUTION").hide();
						return false;
					}
					else
					{
						gridExec.selectRow(0);
						currowExec = 0;
						gridExec.focus()
						return true;
					}
				}

				if ( $("DICT�magas2").parentElement.style.visibility!="hidden" )
				{
					$("DICT�magas2").focus() ;
				}
				else
				{
					if($("DICT�typtrp-ctr").value.trim()!="")
					{
						$("DICT�typtrp-ctr").defaultValue = "";
						dogexchange($("DICT�typtrp-ctr"),"GET", false)
					}
				}
				
				Rsp=RspCodcer.clone();
				//on regarde si on est sur une cereale avec variete
				// trace(doCallBack)
				if(getValue("VARIETE",false,"","",true)=="O" && doCallBack)
				{
					topVariete = false;
					if($("DICT�variete").defaultValue!=getValue("VARIETE",false,"","",true))
					{
						topVariete = true;
						firstDog = false;
						dogexchange($('DICT�variete'), 'F10', false);
					}

				}
				else if(getValue("VARIETE",false,"","",true) != "O" && doCallBack)
				{
					$("DICT�variete").value = "";
				}
								
				oldId = "DICT�codcer";
				$("DICT�codcer").defaultValue = $("DICT�codcer").value;
				
				/* //337 - Sur 734 - Si on revient sur codcer apr�s saisie contrat prod et num contrat 
				// => On passe toutes ces zones.
				if("<?=$ident?>" == "734")
				{				
					if(RspNumContrat.size() > 1 && $('DICT�cer-contrat').value == "")
					{
						$('DICT�cer-contrat').focus();
					}
					else if(RspNumContrat.size() == 0)
					{						
						return;
						
						// Avant de lancer l'�cran de recherche, on r�garde s'il y a des donn�es.
						var dogFct = "f10-cerctr�a-cerctr"
						var requete  = "CODSOC�<?=$c_codsoc?>�TYPCTR�"+getField(RspCodaux[1], "TYPCTR");
							requete += "�CODCER�"+$('DICT�codcer').value;
							requete += "�TYPAUX�"+$('DICT�typaux').value;
							requete += "�CODAUXGES�"+$('DICT�codauxges').value;
							requete += "�CAMPAGNE�"+$('DICT�campagne').value;
							requete += "�CODMVT�"+$('DICT�codmvt').value;
							requete += "�CLOTURE�0�CERGPMT�"+$("DICT�codcer-ori").value;
							requete += "�TYPE-TRANSP�*�MAGASI�"+$('DICT�magasi').value;
							requete += "�FILIERE�N�PARAM�N�PROG�asbonapp�DATMVT�" + $('DICT�cer-datsai').value;
						
						var retourExchange = sendExchangeInvoke("<?=$dogExchange?>",requete,dogFct,"","");
						
						if(retourExchange == "OK" && getValue("TYPCTR",false ,"","",true)!="")
						{							
							$('DICT�cer-contrat').focus();
							dogexchange($('DICT�cer-contrat'), 'F10');

							setTimeout('if($("DICT�codcer").value == ""){$("DICT�codcer").focus()}else{$("DICT�natcon").focus()}', 50);
							return;
						}
						else
						{							
							setTimeout('if($("DICT�codcer").value == ""){$("DICT�codcer").focus()}else{$("DICT�natcon").focus()}', 50);
							return;
						}
					}
				} */
				
				/** ---------  O R D R E    D E   S A I S I E --------- **/
				<?if(file_exists(search::searchExe("as_ordre_saisie.js",0)))
				  {
				?>
					try
					{
						if(!modifBon && saisieCtrProduction == false || presenceCtrProduction == false)
						{
							if(zoneSuivanteSpe('CODCER'))
								return;						
						}
					} catch(ex){trace("---zoneSuivanteSpe : " + ex)}; 
				<?}?>
			
				setFocus(TabFocusEntree, oldId);
				
				if(topVariete)
					oldId = "DICT�variete";
				return true;
				break;
			
			case "DICT�typtrp-ctr":
				getValue ( "TYPTRP"  , false , "" , oldId  , true);
				getValue ( "LIBEL"  , false , "" , oldId + "_libel"  , true);

				// alert(getValue("TYPTRANSP",false,"","",true))

				if(getValue("TYPTRANSP",false,"","",true)=="2")
				{
					//si type de transport par le tiers alors bloquer code transporteur et immatriculation
					zonscreenget("�transp��9�immatr��9�transp_libel��9")
				}//si cest 1 alors obligatoire
				else
				{
					enableZone('DICT�transp',true)
					// zonscreenget("�transp��11");
				}
				
				if("<?=$ident?>" == "333")
				{
					enableZone('DICT�immatr',true);
				}
				break;

			case "DICT�coddom":
				setValueFromKeyword ( "CODDOM"  , false , "" , "DICT�coddom"  , false);
				setValueFromKeyword ( "DOM-BANQUE"  , false , "" , "DICT�coddom_libel"  , false);
				setValueFromKeyword ( "MODREG"  , false , "" , "DICT�modreg"  , false);
				setValueFromKeyword ( "LIBEL"  , true , "MODREG" , "DICT�modreg_libel"  , false);
				break;

			case "DICT�modreg":
				setValueFromKeyword("MODREG", false ,"" , oldId,true);
				setValueFromKeyword("LIBEL" , false ,"" , oldId + "_libel",true);
				break;

			case "DICT�chauff":
				setValueFromKeyword("CHAUFF", false ,"" ,"DICT�chauff" ,true);
				setValueFromKeyword("LIBEL" , false ,"" ,"DICT�chauff_libel" ,true);
				break;

			//Magasin reciproque	
			case "DICT�magas2":
				RspMagas2 = Rsp
				magasiSoc = setValueFromKeyword("CODSOC-APP",false,"","",true)
				Rsp = RspCodcer.clone();
				artSoc = ""
				
				for(i=1;i<=10;i++){
					if(setValueFromKeyword("SOCIETE_"+i,false,"","",true) != "")
						artSoc = artSoc + setValueFromKeyword("SOCIETE_"+i,false,"","",true) + ","
				}
				
				if(magasiSoc!=""&&artSoc!="")
				{
					if(artSoc.match(magasiSoc)==null) {
						//alert("Soci�t� de l'article : ("+artSoc+")" + " non autoris�e pour ce magasin " + magasiSoc )
						//return
					}
				}
				
				// Rsp=RspMagas2
				
				$('DICT�magas2').value 		 = getField(RspMagas2[1],'MAGASI');
				$('DICT�magas2_libel').value = getField(RspMagas2[1],'LIBEL');

				//on verifie que le magasin 2 et different du magasin
				if($("DICT�magasi").value.trim() == $("DICT�magas2").value.trim())
				{
					alert("Le code magasin doit �tre diff�rent du magasin d'origine")
					$("DICT�magas2").value="";
					$("DICT�magas2_libel").value="";
					$("DICT�magas2").focus();
					return false;
				}
				
				//mouvement de transfert on force le F8
				if(getField(RspMagas2[1],'TOPASS').trim() == "0")
				{
					// Rsp=RspCodmvt.concat()
					if(getField(RspCodmvt[1],'TYPTRANS').trim() == '3' && getField(RspCodmvt[1],'TYPMVT').trim() == '1')
					{
						if(cer_bontrs_1.trim() == "")
						{
							if($("DICT�no-ordre").value.trim() == "")
							{
								PhpAuxges = "<?=search::searchExe("ar_mvt_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_campagne="+$('DICT�campagne').value+"&_codmvt="+$("DICT�codmvt").value+"&_codcer="+$("DICT�codcer").value+"&_codcerlibel="+"&_magas2="+$("DICT�magas2").value+"&_magasi="+$("DICT�magasi").value+"&_magasilibel=";
								sFeatures = "dialogHeight:620px;dialogWidth:940px;status:no;help:no";
								retourExchange = showModalDialog ( PhpAuxges , "" , sFeatures) ;
								
								if(retourExchange != undefined && retourExchange.trim() != "")
								{
									retourExchange = sendExchangeInvoke ( "<?=$dogExchange?>", "ROWID�" +  retourExchange, "get-cermvt�a-cermvt", "", "" ) ;
								}
								else
								{
									$("DICT�magas2").value = "";
									$("DICT�magas2").focus();
									return;
								}
							}
							else
							{
								/*
								f10-cermvt�a-cermvt�0�~CODSOC�DE  �CAMPAGNE�2014�CER-DATSAI�25/01/2015|�LISMVT��CODMVT�CET�PARAME�S�CER-ANNULE�0�CODCER�702023�MAGASI�189�MAGAS2�101�NUMORD-EXP�MAN0064901
								*/
								var dogFonction = "f10-cermvt�a-cermvt"
								var inputParameter = "NUMORD-EXP�" + $("DICT�no-ordre").value.trim();
								retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "" , "" );
							}
							
							if(retourExchange!="NODATA")
							{
								//on ramene les donn�es de l' entree
								bondupliquer();
								$(strId).defaultValue = $(strId).value
								//on donne le focus a l'objet suivant
								//nextfocus($(strId));
							}
							else
							{
								$("DICT�magas2").value=""
								$("DICT�magas2").focus()
								return;
							}
						}
					}
				}
				else
				{
					if("<?=$ident?>".trim() == "333")
					{
						$("divSaisieDateHeureTransfert").show();
						Rsp=RspCodmvt.clone()
						if(getValeurRsp("TYPMVT") == "2") // sortie
							$("titreRec").innerHTML = "D�chargement R�ciproque";
						else
							$("titreRec").innerHTML = "Chargement R�ciproque";
						enableZone("dateRec|heureRec", true)
						$("dateRec").focus();
						$("dateRec").select();
						$(oldId).defaultValue = $(oldId).value;
						return false;
					}
				}
				break;

			case "DICT�cer-codech":
				setValueFromKeyword("CODECH", false ,"" ,"DICT�cer-codech" ,true);
				setValueFromKeyword("LIBEL" , false ,"" ,"DICT�cer-codech_libel" ,true);
				break;
				
			case "DICT�devise":
				setValueFromKeyword ( "DEVISE"  , false , "" , "DICT�devise"  , true);
				setValueFromKeyword ( "LIBEL"  , false , "" , "DICT�devise_libel"  , true);
				break;
				
			case "DICT�codsocfac":
				setValueFromKeyword ( "CODSOC"  , false , "" , "DICT�codsocfac"  , true);
				setValueFromKeyword ( "LIBSOC"  , false , "" , "DICT�codsocfac_libel"  , true);
				break;
				
			case "DICT�xxx26":
				setValueFromKeyword ( "CODASSUR"  , false , "" , "DICT�codassur"  , true);
				setValueFromKeyword ( "LIBEL"  , false , "" , "DICT�codassur_libel"  , true);
				break;
				
            case "DICT�cellule":
                if ($("DICT�codmvt").value.toUpperCase() == "CXS")
                {
                    if ($("DICT�e-cellule").value.trim() != "" && $("DICT�e-cellule").value.trim().toUpperCase() == setValueFromKeyword("CELLULE", false, "", "", true).trim().toUpperCase())
                    {
                        alert("Impossible de transf�rer d'une cellule vers elle-m�me !");
                        $(oldId).value = "";
                        return false;
                    }
                }
                
                // Sans la gestion des rotations
                if ("<?=$_rotation?>".trim() == "")
                {
                    setValueFromKeyword("CELLULE", false, "", oldId,            true);
                    setValueFromKeyword("LIBEL",   false, "", oldId + "_libel", true);
                    
                    typcel = getValue("TYPCEL", false, "", "", true).toUpperCase();
                }
                // Avec la gestion des rotations
                else
                {
                    setValueFromKeyword("CODTAB",   false, "", oldId,            true);
                    setValueFromKeyword("LIBEL1-1", false, "", oldId + "_libel", true);

                    typcel = getValue("LIBEL1-2", false, "", "", true).toUpperCase();
                }
				
                if (oldTrigger == "F10")
                {
                    dogexchange($(oldId), "GET", true);
                    return true;
                }
				
                $(oldId).title = $(oldId + "_libel").value.trim();
                RspCellule = Rsp.clone();
				
                if (doCallBack)
                {
                    // Avec la gestion des rotations
                    if("<?=$_rotation?>".toUpperCase() == "O")
					{
                        if (!recupRotationCourante())
						{
                            $(oldId).title = "";
							return false;
                        }

						// Si on a ouvert une nouvelle rotation 
						// => recherche cellule pour mettre � jour RspCellule
						if (nouvelleRotation)
						{
							nouvelleRotation = false;
							dogexchange($("DICT�cellule"), "GET", true);
						}
                        
                        // Pte 13/04/2017 melange cellule: Gestion des controles
                        // typeMes = getValeurRsp('TYPMES');
                        // mesErr  = getValeurRsp('MESSAGE');
                        typeMes = getField(RspCellule[1], "TYPMES").trim();
                        mesErr  = getField(RspCellule[1], "MESSAGE").trim();
                        
                        // Message d'erreur => annulation de la saisie / Message d'information => demande de confirmation
                        if (typeMes == "ERREUR" || ( typeMes == "ALERTE" && !confirm(mesErr)))
                        {
                            if (typeMes == "ERREUR") alert(mesErr);

							$("DICT�cellule").value       = "";
							$("DICT�cellule_libel").value = "";
							$("DICT�cellule").focus();
							return false;
						}
                    }
					
					$(oldId).defaultValue = $(oldId).value;
					
                    // On verifie la cellule
					if (typcel != "V")
                    {
                        if (!ctrlCellule())
						{
                            $(oldId).title = "" ;
                            return;
                        }

						ctrlcapacite();
					}
					else
					{
						ctrlcellulemulti();
                        $(oldId).defaultValue = $(oldId).value;
						return;
					}
										
				}
			break;

			case "DICT�cellule_1":
			case "DICT�cellule_2":
			case "DICT�cellule_3":
			case "DICT�cellule_4":
			case "DICT�cellule_5":
			case "DICT�cellule_6":
			case "DICT�cellule_7":
			case "DICT�cellule_8":
			case "DICT�cellule_9":
			case "DICT�cellule_10":
			case "DICT�cellule_11":
			case "DICT�cellule_12":
				//on ferme le pave
				var num = parseInt(oldId.split("_")[1]);
				$("DICT�pdsnet_"+num).value="";
				$("DICT�cellule_"+num+"_libel").value="";
								
				if(setValueFromKeyword("TYPCEL", false, "" , "", true)=="V")
				{
					alert("Cellule virtuelle non autoris�e");
					return	;
				}
				
                // Sans la gestion des rotations
				if ("<?=$_rotation?>" == "")
                {
					setValueFromKeyword("CELLULE",   false, "", oldId,                   true);
					setValueFromKeyword("LIBEL",     false, "", oldId + "_libel",        true);
					setValueFromKeyword("STOCK-CEL", false, "", "DICT�solde-vide_"+ num, true);
					
                    soldevide[num] = setValueFromKeyword("STOCK-CEL", false, "", "", true).trim();
					
					if (!ctrlCellule()) return;
				}
                // Avec la gestion des rotations
				else
				{
                    // Pte 13/04/2017 melange cellule:
                    // typeMes = getValeurRsp('TYPMES');
                    // mesErr  = getValeurRsp('MESSAGE');
                    typeMes = getField(RspCellule[1], "TYPMES").trim();
                    mesErr  = getField(RspCellule[1], "MESSAGE").trim();

                    
                    // Message d'erreur => annulation de la saisie / Message d'information => demande de confirmation
                    if (typeMes == "ERREUR" || ( typeMes == "ALERTE" && !confirm(mesErr)))
                    {
                        if (typeMes == "ERREUR") alert(mesErr);

                        clearZone(strId + "|" + strId + "_libel");
                        $(strId).focus();
                        /*
                        $("DICT�cellule").value       = "";
                        $("DICT�cellule_libel").value = "";
                        $("DICT�cellule").focus();
                        */
                        return false;
                    }
                    
                    setValueFromKeyword ( "CODTAB"  , false , "" , oldId  , true);
					setValueFromKeyword ( "LIBEL1-1"  , false , ""   , oldId + "_libel"  , true);

					var backRspCellule = RspCellule.clone();
					RspCellule = Rsp.clone();
					
					if(!recupRotationCourante(num))
						return false;
					
					Rsp = RspRotation.clone();
					
					setValueFromKeyword ( "STOCK", false, "", "DICT�solde-vide_"+ num, true);
					soldevide[num] = setValueFromKeyword ( "STOCK", false, "", "", true).trim();
					
					//on verifie la cellule
					if(!ctrlCellule())
						return;
					
					RspCellule = backRspCellule.clone();
				}

				$(oldId).defaultValue = $(oldId).value;
				zonscreenget("�pdsnet_"+ num +"��8");

				setFocus(TabFocusEntree, oldId);
				return;
				break;

			case "DICT�cellule-ret":
				setValueFromKeyword ( "CELLULE"  , false , "" , "DICT�cellule-ret"  , true);
				setValueFromKeyword ( "LIBEL"  , false , "" , "DICT�cellule-ret_libel"  , true);
				initbufferscreen();
				// $("bt13").focus();
				return;
				break;
				
			case "DICT�e-cellule":
				var typcel = "", cel = "";

				if("<?=$_rotation?>"!="O")
				{
					cel = setValueFromKeyword ( "CELLULE"  , false , "" , oldId  , true);
					typcel = getValue("TYPCEL", false, "", "", true).toUpperCase();
				}
				else
				{
                    // Pte 06/04/2017 melange cellule:
                    typeMes = getField(RspCellule[1], "TYPMES").trim();
                    mesErr  = getField(RspCellule[1], "MESSAGE").trim();
                    
                    // Message d'erreur => annulation de la saisie / Message d'information => demande de confirmation
                    if (typeMes == "ERREUR" || ( typeMes == "ALERTE" && !confirm(mesErr)))
                    {
                        if (typeMes == "ERREUR") alert(mesErr);

                        $("DICT�e-cellule").value       = "";
                        $("DICT�e-cellule_libel").value = "";
                        $("DICT�e-cellule").focus();
                        return false;
					}
                    
                    cel = setValueFromKeyword ( "CODTAB"  , false , "" , oldId  , true);
					typcel = getValue("LIBEL1-2", false, "", "", true).toUpperCase();
				}

				if(typcel.trim().toUpperCase() == "V")
				{
					alert("Cellule non autoris�e");
					return false;
				}
				
				//dans le cas des cxs la cellule ne peut etre elle meme
				if($("DICT�codmvt").value.toUpperCase()=="CXS")
					if($("DICT�cellule").value.trim() != "" && $("DICT�cellule").value.trim().toUpperCase() == cel.trim().toUpperCase())
					{
						alert("Impossible de transf�rer d'une cellule vers elle-m�me !");
						$(oldId).value = "";
						return false;
					}
					
				var RspECellule = Rsp.clone();
				if("<?=$_rotation?>"!="O")
				{
					setValueFromKeyword ( "CELLULE"  , false , "" , oldId  , true);
					setValueFromKeyword ( "LIBEL"  , false , ""   , oldId + "_libel"  , true);
				}
				else
				{
					setValueFromKeyword ( "CODTAB"  , false , "" , oldId  , true);
					setValueFromKeyword ( "LIBEL1-1"  , false , ""   , oldId + "_libel"  , true);
				}
				
				if(!recupRotationCouranteCelluleReciproque())
					return false;
				
				
				//Test 734 sous-familles @@337
				if("<?=$ident?>" == "734" && 
				getField(RspECellule[1],'FAM-PRODUIT').trim().toUpperCase() == getField(RspCodcer[1], "FAMCER").trim().toUpperCase() && 
				getField(RspECellule[1],'SFAM-PRODUIT').trim().toUpperCase() != getField(RspCodcer[1], "SFMCER").trim().toUpperCase())
				{
					alert("La sous-familles de la c�r�ale est diff�rente de celle de la rotation de la cellule.");
					$("DICT�e-cellule").value = "";
					$("DICT�e-cellule_libel").value = "";
					$("DICT�e-cellule").focus();
					return false;
				}
				break;

			case "DICT�transp":
				setValueFromKeyword ( "TRANSP"  , false , "" , "DICT�transp"  , true);
				setValueFromKeyword ( "LIBEL"  , false , "" , "DICT�transp_libel"  , true);
				if(setValueFromKeyword("IMMATR", false, "", "",true).trim() !="" && $("DICT�immatr").value.trim() == "")
					setValueFromKeyword("IMMATR",false,"","DICT�immatr",true);
				//si D alors le libell� est saisissable
				if(setValueFromKeyword ( "TYPTRANSP", false, "", "", true)=="D" || setValueFromKeyword ( "TYPTRANSP", false, "", "", true)=="P")
				{
					zonscreenget("�transp_libel��11");
					$('DICT�dest-fin2').value=$('DICT�dest-fin').value;
					$('DICT�transp_libel2').value="";//$('DICT�transp_libel').value;
					if(setValueFromKeyword ( "TYPTRANSP", false, "", "", true)=="P"){
						$("TRANSP").style.display="BLOCK"
						$('DICT�transp_libel2').focus()
					}else{
						$('DICT�transp_libel').focus()
					}
					$("DICT�transp").defaultValue = $("DICT�transp").value;
					return
				}
				else
				{
					zonscreenget("�transp_libel��9");
				}
				break;

			case "DICT�code":
				codeb=setValueFromKeyword ( "CODTAB"  , false , "" , ""  , true).split("");
				codeb=codeb[0]+codeb[1]+codeb[2];
				$(oldId).value=codeb;
				break ;

			case "DICT�codcar" :
				setValueFromKeyword ( "CODTAB"  , false , "" , oldId  , true);
				setValueFromKeyword ( "LIBEL1-1"  , false , "" , "DICT�libcar_"+oldIdb[1]+"_libel"  , true);
				break ;
				
			case "DICT�cod-def" :
				setValueFromKeyword ( "CODTAB"  , false , "" , oldId  , true);
				setValueFromKeyword ( "LIBEL1-1"  , false , "" , "DICT�cod-def_libel"  , true);
				break ;
				
			case "DICT�cod-def1" :
				setValueFromKeyword ( "CODTAB"  , false , "" , oldId  , true);
				setValueFromKeyword ( "LIBEL1-1"  , false , "" , "DICT�cod-def1_libel"  , true);
				break ;

			case "DICT�codcer-ret":
				setValueFromKeyword("CODCER",false,"","DICT�codcer-ret",true);
				setValueFromKeyword("LIBEL",false,"","DICT�codcer-ret_libel",true);
				break;
				
			case "DICT�codcer-ori":
				setValueFromKeyword("CODCER",false,"","DICT�codcer-ori",true);
				setValueFromKeyword("LIBEL",false,"","DICT�codcer-ori_libel",true);
				break;
				
			case "DICT�cer-formule" :
				setValueFromKeyword ( "CODTAB"  , false , "" , oldId  , true);
				setValueFromKeyword ( "LIBEL1-1"  , false , "" , "DICT�cer-formule_libel"  , true);
				// $(oldId).defaultValue = $(oldId).value;
				// nextfocus($(oldId));
				// return;
				break ;
			case "DICT�parite-cer" :
				setValueFromKeyword ( "CODTAB"  , false , "" , oldId  , true);
				setValueFromKeyword ( "LIBEL1-1"  , false , "" , "DICT�parite-cer_libel"  , true);
				break ;
				
			case "DICT�cer-modliv" :
				setValueFromKeyword ( "CODTVA"  , false , "" , oldId  , true);
				break ;
				
			case "DICT�courtier" :
				setValueFromKeyword ( "CODTAB"  , false , "" , "DICT�courtier"  , true);
				setValueFromKeyword ( "LIBEL1-1"  , false , "" , "DICT�courtier_libel"  , true);
				break ;
				
			case "DICT�immatr" :
				setValueFromKeyword ( "IMMATR"  , false , "" , "DICT�immatr"  , true);
				setValueFromKeyword ( "LIBEL"  , false , "" , "DICT�immatr_libel"  , true);
				break ;
				
			case "DICT�assure" :
				setValueFromKeyword ( "CODTAB"  , false , "" , oldId  , true);
				setValueFromKeyword ( "LIBEL1-1"  , false , "" , "DICT�assure_libel"  , true);
				break ;
				
			 case "DICT�cergpmt": case "DICT�cerdeco": case "DICT�cerdecl": case "DICT�cerecl": case "DICT�cerech":
				champ=oldId.split("�");
				setValueFromKeyword ( "CODCER"  , false , "" , oldId  , true);
				setValueFromKeyword ( "LIBEL"  , false , "" , oldId + "_libel"  , true);
				break ;
				
			case "DICT�typcon" :
				setValueFromKeyword ( "CODTAB"  , false , "" , oldId  , true);
				setValueFromKeyword ( "LIBEL1-1"  , false , "" , "DICT�typcon_libel"  , true);
				break ;
				
			case "DICT�soc" :
				setValueFromKeyword ( "CODSOC"  , false , "" , oldId  , true);
				setValueFromKeyword ( "LIBSOC"  , false , "" , "DICT�soc_"+oldIdb[1]+"_libel"  , true);
				debloqChamp('soc_',oldIdb[1]);
				break ;
				
			 case "DICT�espgnis":
				setValueFromKeyword ( "CODTAB"  , false , "" , oldId  , true);
				setValueFromKeyword ( "LIBEL1-1"  , false , "" , "DICT�espgnis_libel"  , true);
				debloqChamp('espgnis','0');
				break ;
				
			 case "DICT�vargnis":
				setValueFromKeyword ( "CODTAB"  , false , "" , oldId  , true);
				setValueFromKeyword ( "LIBEL1-1"  , false , "" , "DICT�vargnis_libel"  , true);
				// $(oldId).defaultValue = $(oldId).value;
				// nextfocus($(oldId));
				// return;
				break ;
				
			 case "DICT�typie":
				setValueFromKeyword ( "TYPPIE"  , false , "" , oldId  , true);
				setValueFromKeyword ( "LIBEL"  , false , "" , "DICT�typie_libel"  , true);
			break ;
			
            case "DICT�cer-contrat-fil":
				// trace("CALLBACK CONTRAT PROD")
			
				RspContratProd = Rsp.clone();
			
                RspContrat=Rsp.clone();

                // CAVAC V7 Lire le type du contrat pour trouver le parametrage de la zone parcelle
                typcontrat = setValueFromKeyword("TYPCON", false, "", "", true);
                setValueFromKeyword("CER-CONTRAT", false, "", oldId, true);
                getValue("CER-CONTRAT", false, "", "DICT�cer-contrat-fil", true);

                inputParameter = "�MOTCLE�TYPCON-CER�CODTAB�" + typcontrat;
                dogFonction    = "GET-TABGEN�a-tabgen";
                retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "GET-TABGEN�a-tabgen", "", "");

                // Seulement si la saisie de la parcelle est obligatoire, on ecrase le param�trage du mouvement
                saiparc = setValueFromKeyword("LIBEL1-10", false, "", "", true);

                if (saiparc == "1")
					zonscreenget("�codeparc��11")  //obligatoire

				//R�cup�ration des infos du contrat fili�re
				Rsp=RspContrat.clone();

				for(i=1; i<=14; i++)
					setValueFromKeyword ( "VAL-ETAT-CTR_"+i  , false , "" , "DICT�val-etat-ctr_"+i  , true);

				if($("DICT�codcer").value.trim() == "")
				{
					setValueFromKeyword("CODCER",false,"","DICT�codcer", true)
				}
				
				if($("DICT�codauxges").value.trim() == "")
				{
					setValueFromKeyword("CODAUXGES",false,"","DICT�codauxges", true)
				}
				
				if($("DICT�typaux").value.trim() == "")
				{
					setValueFromKeyword("TYPAUX",false,"","DICT�typaux", true)
				}

				//on verifie la coherence du contrat
                /* Pte 30/11/2016 Voir DOG get-cerctr
				if(setValueFromKeyword("CODCER",false,"","", true).trim().toUpperCase()!=$("DICT�codcer").value.trim().toUpperCase())
					alert("Attention le code c�r�ale du contrat ne correspond pas");
                */

				if(setValueFromKeyword("CODAUXGES",false,"","", true).trim().toUpperCase()!=$("DICT�codauxges").value.trim().toUpperCase())
				{
					$(oldId).value = "";
					$(oldId).defaultValue = $(oldId).value;
					alert("Attention le code tiers du contrat ne correspond pas")
					return false;
				}
				
				if(setValueFromKeyword("CAMPAGNE",false,"","", true).trim().toUpperCase()!=$("DICT�campagne").value.trim().toUpperCase()){
					$(oldId).value = "";
					$(oldId).defaultValue = $(oldId).value;
					alert("Attention ce contrat est affect� sur une autre campagne")
					return false;
				}
				
				$(oldId).defaultValue = $(oldId).value;
				/* utile pour un contrat fili�re ? @@bnj
				Rsp = RspCoderMvt;

				//on teste si l'oe est obligatoire donc si param montre que c'est obligatoire on affiche ds ts les cas le message
				if(setValueFromKeyword ( "TOPELEMENT_16" ,false ,"" ,""  , true) == "1")
					if($("DICT�numoex").value.trim() == "")
						alert("Veuillez saisir l'ordre d'exp�dition");

				//on regarde si l'odre d'expedition est rendu et si ds le menu on dit que rendu c'est obli on affiche le message et que l'oe est facultatif
				if(setValueFromKeyword ( "TOPELEMENT_16" ,false ,"" ,""  , true)=="2")
				{
					Rsp=RspContrat;
					if(setValueFromKeyword("TYPTRP-CTR",false,"","",true)!=""&&"<?=$_rendu?>"=="Oblig")
						if($("DICT�numoex").value.trim()=="")
							alert("Veuillez saisir l'ordre d'exp�dition");

				}
				*/
				
				Rsp=RspContrat;
				setValueFromKeyword ( "CER-CONTRAT"  , false , "" , oldId  , true);

				//on regarde en modification que le contrat n'est pas cloture
				if(setValueFromKeyword("CER-VALID",false,"","",true)!="")
				{
					if(RspMvt != undefined) Rsp = Rsp.clone(); else Rsp = undefined;

					if(setValueFromKeyword("CER-CONTRAT",false,"",oldId,true)!=$("DICT�cer-contrat").value)//&&setValueFromKeyword("CER-CONTRAT",false,"","",true)!=""){
					{
						alert("Contrat clotur�")
						return
					}
				}
				
				if(setValueFromKeyword("CLOTURE",false,"","",true)!="")
				{
					if(RspMvt != undefined) Rsp = Rsp.clone(); else Rsp = undefined;
					if(setValueFromKeyword("CER-CONTRAT",false,"",oldId,true)!=$("DICT�cer-contrat").value)//&&setValueFromKeyword("CER-CONTRAT",false,"","",true)!=""){
					{
						alert("Contrat clotur�");
						return false;
					}
				}
				
				Rsp=RspContrat;
				
				//on va verifier le code mouvement associ� au contrat
				retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , "�MOTCLE�TYPCON-CER�CODTAB�" + setValueFromKeyword("TYPCON",false,"","",true) , "GET-TABGEN�a-tabgen" , "" , "" );
				//on regarde si on a ramene qque chose
				if (retourExchange != "OK")
				{
					setFocus(TabFocusEntree, strId);
					return false;
				}

				RspTmp=Rsp.clone();
				Rsp=RspCodmvt.concat();

                if(setValueFromKeyword("TENU-STOCK",false,"","",true)!="1")
					tenustock = true;
				else
					tenustock = false;

                Rsp=RspContrat
				setValueFromKeyword ( "CER-CONTRAT"  , false , "" , oldId  , true);

                if(tenustock)//si tenu-stock alors
				{
					//si oui on change le code mouvement
					Rsp=RspTmp;
					if(setValueFromKeyword("LIBEL1-2",false,"","",true)!="")
					{
						//et on change le code mvt
						if($("DICT�codmvt").value!=setValueFromKeyword("LIBEL1-2",false,"","",true))
						{
							setValueFromKeyword("LIBEL1-2",false,"","DICT�codmvt",true);
							//on se refait des get
							dogexchange($('DICT�codmvt'), false);
							dogexchange($('DICT�codcer'), false);
						}
					}
				}

                if(doCallBack)
				{
					$(oldId).defaultValue = $(oldId).value;
				}
				
				if(nextZone(TabFocusEntree, $(oldId).id) == "DICT�cer-contrat" && f10Contrat)
				{
					// trace("		 F10contrat | Recherche pr�sence NUMERO CONTRAT ")
					
					inputParameter = "CODSOC�<?=$c_codsoc?>�TYPCTR�"+getValue("TYPCTR",false ,"","",true)+"�CLOTURE�0"+"�TYPAUX�"+$("DICT�typaux").value+"�CODAUXGES�"+$("DICT�codauxges").value+"�CODCER�"+$("DICT�codcer").value+"�CAMPAGNE�"+$("DICT�campagne").value+"�CODMVT�"+$("DICT�codmvt").value+"�CERGPMT�"+$("DICT�codcer-ori").value+"�PARAM�N�FILIERE�N�PROG�ASBONAPP�DATMVT�" + $("DICT�cer-datsai").value;
					dogFonction    = "F10-cerctr�a-cerctr";
					retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");
										
					//si il y a des contrats on ouvre la recherche
					if (retourExchange == "OK" && RspNumContrat.size() == 0 && $('DICT�cer-contrat').value == ""){
						// trace("		 F10contrat | PRESENCE CONTRAT => RECHERCHE NUMERO CONTRAT ")
						
						dogexchange($("DICT�cer-contrat"),"F10", false);
					}
					return;
				}
                break;

			case "DICT�traitm":
				setValueFromKeyword ( "TRAITM"  , false , "" , oldId  , true);
				setValueFromKeyword ( "LIBEL"  , false , "" , "DICT�traitm_libel"  , true);
				setValueFromKeyword ( "DOSAGE"  , false , "" , "DICT�dosage"  , true);
				break;
				
			case "DICT�comonic":
				setValueFromKeyword ( "COMONIC"  , false , "" , oldId  , true);
				setValueFromKeyword ( "LIBEL"  , false , "" , "DICT�comonic_libel"  , true);
				break;

			// Pte Gestion des lots apports
			case "DICT�numlot":
				RspLots = Rsp[1];
			
				ccCodLot = setValueFromKeyword("CODLOT", false, "", "", true);
				$("DICT�codlot").value = ccCodLot.substring(0,5);
				$("DICT�numlot").value = ccCodLot.substring(5,8);
				setValueFromKeyword ("LIB-LOT", false, "", "DICT�lib-lot", true);
				setValueFromKeyword ("CELLULE", false, "", "DICT�cellule", true);
				setValueFromKeyword ("LIB-CELLULE", false, "", "DICT�cellule_libel", true);
				break;

			// Pte Gestion des parcelles
            case "DICT�codeparc":
				setValueFromKeyword("CHRONO",   false, "", "DICT�chronoparc", true);    // Pte DDD-NAT-PARCELLE
				setValueFromKeyword("CODEPARC", false, "", "DICT�codeparc", true);
				setValueFromKeyword("LIBPARC",  false, "", "DICT�lib-parcelle", true);
            break;

			default:
				strIdS = oldId.split("-");
				if(strIdS[0]=="tabGridEclat")
				{
					col = parseInt(strIdS[2]);
					row = strIdS[3];
					switch(col)
					{
						case getColEclat("typauxecl") :
							gridEclat.setCellData(getValue("TYPAUX" , false ,"" ,"" ,true), col, row);
							break;

						case getColEclat("codauxgesecl"):
							gridEclat.setCellData(getValue("CODAUXGES" , false ,"" ,"" ,true), col, row);
							gridEclat.setCellData(getValue("ADRESSE_1" , false ,"" ,"" ,true), getColEclat("adresse"), row);
							break;
							
						case getColEclat("codmvtecl"):
							gridEclat.setCellData(getValue("CODMVT" , false ,"" ,"" ,true), col, row);
							break;	
							
						case getColEclat("contrat"):
							//On vide la cellule du Pds Normes on fait un recalcul du reste � �clater.
							gridEclat.setCellData("",getColEclat("pdsNormes"),row);
							majQteRestanteEclatement();
						
							RspContratEclat = Rsp.clone();
							RspContratEclat.shift();
							
							gridEclat.setCellData(getField(RspContratEclat[0],'CER-CONTRAT'), col, row);
							gridEclat.setCellData(getField(RspContratEclat[0],'TYPCON'), getColEclat("option"), row);
							
							//On teste si on a pas dej� saisie le contrat.
							if(eclatementSPE)
							{
								if(parseFloatStr(getValeurRsp('TON-SOLDE').trim()) == 0)
								{
									// trace("		parseFloatStr(getValeurRsp('TON-SOLDE').trim()) == 0")
									alert("Toute la quantit� de ce contrat a �t� sold�e.")
									gridEclat.setCellText("",getColEclat("contrat"),row);
									//keycode pour rester en modif dans le cellEditEnded
									keycode = 1111;
									return;
								}
								
								var trouve = false
								var ctr = getValeurRsp('CER-CONTRAT').trim().toUpperCase();
								var textCelulle = "";
								for(var i=0; i<gridEclat.getRowCount(); i++)
								{										
									textCelulle = gridEclat.getCellData(getColEclat('CONTRAT'),i).trim().toUpperCase();
																
									if(!trouve && textCelulle == ctr)
									{
										// alert("TROUVE")
										trouve = true;
										continue
									}
									
									if(trouve && textCelulle == ctr)
									{
										alert("Contrat d�j� utilis�.");
										contratEclatOK = false;
										gridEclat.setCellText("",getColEclat("contrat"),i);
										return ;
									}
								}
							}
							
							//SI qteMvt > qteContrat => poidNormes pr�charg� avec qteContrat
							//Sinon 				 => poidNormes pr�charg� avec poid restant
							if(parseFloatStr($('DICT�pdsresteini').value) > parseFloatStr(getValeurRsp('TON-SOLDE')))
							{	
								// trace("	parseFloatStr($('DICT�pdsresteini').value) > parseFloatStr(getValeurRsp('TON-SOLDE'))")
								gridEclat.setCellData(parseFloatStr(getValeurRsp('TON-SOLDE')), getColEclat("pdsNormes"), row);
								qteContratSaisie = getValeurRsp('TON-SOLDE');
							}
							else
							{
								// trace("	ELSE")
								gridEclat.setCellData(parseFloatStr($('DICT�pdsresteini').value), getColEclat("pdsNormes"), row);
							}
							
							if(eclatementSPE)
								rspContratsEclat[row] = Rsp[1];
							
							// majQteRestanteEclatement()
							break;
							
						case getColEclat("option"):
							gridEclat.setCellData(getValue("CODTAB" , false ,"" ,"" ,true), col, row);
							break;
					}
				}
				else
					if(debug)
					{
						alert("Zone non g�r�e dans le dogexchangecallback : " + oldId);
						return false;
					}
				break;
			
			case 'DICT�num-ctr':
				//337 avec le motcl� menu mvtvte = CCV, si on saisie CCV comme code mouvement pour un nouveau bon, 
				//on peut rechercher un num�ro de contrat.
				
				//=> R�cup�ration des informations du contrat.
				
				$('DICT�num-ctr').value = getValeurRsp('CER-CONTRAT');
				
				bonContrat();
				
				break;
		} //Selon le mot cl�
	}
	else if(Rsp[0] == "NODATA")
	{
		switch(oldId)
		{
			case "DICT�numlot":
				// Tester le retour et quitter s'il y a une erreur
				$("DICT�codlot").value  = "";
				$("DICT�lib-lot").value = "";
				$(oldId).value = "";
				$(oldId).defaultValue = "";
				
				if(!doCallBack)
				{
					doCallBack = true;
					return false;
				}
				
				alert(Rsp[1]);
				return;
				break;

			case "DICT�codeparc":
				// Tester le retour et quitter s'il y a une erreur
                $("DICT�chronoparc").value   = "";  // Pte DDD-NAT-PARCELLE
				$("DICT�codeparc").value     = "";
				$("DICT�lib-parcelle").value = "";
				
				if(!doCallBack)
				{
					doCallBack = true;
					return false;
				}
				
				alert(Rsp[1]);
				return;
			break;

			case "DICT�chauff":
				$(oldId).value = "";
				$(oldId).defaultValue = "";
				
				if(!doCallBack)
				{
					doCallBack = true;
					return false;
				}
				
				alert("<?=lect_bin::getLabel("error-nodata", "sl");?>");
				return false;
				break;
				
			case "DICT�cer-contrat":
				str_val = "CODSOC�<?=$c_codsoc?>" + "�CODCER�"+$("DICT�codcer").value.trim()  + "�CODMVT�"+$("DICT�codmvt").value.trim()  + "�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�DATSAI�" + $("DICT�cer-datsai").value;
				
				if(getField(RspCodcer[1],'COLLECTE') != "")
					str_val += "�COLLECTE�" + getField(RspCodcer[1],'COLLECTE');
				else
					str_val += "�COLLECTE�O";
				
				retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", str_val, "f10-option�a-tabcer", "", "");
				
				if(retourExchange=="OK")
					getValue("CODTAB", false, "", "DICT�natcon",true);
				break;
				
			default:
				$(oldId).value = "";
				$(oldId).defaultValue = "";
				$(oldId).title = "";
				
				if(!doCallBack)
				{
					doCallBack = true;
					return false;
				}
				
				if(Rsp[1] != undefined)
					alert(Rsp[1])
				
				return false;
				break;
		}
	}
	else
	{
		switch(oldId)
		{
            // Pte 30/11/2016: 734 pour le blocage sur cereale contrat/cereale bon
            case "DICT�cer-contrat":
				$(oldId).value = "";
				$(oldId).defaultValue = "";
				
				alert(Rsp[1]);
                return false;
				break;

            case "DICT�cellule":
			case "DICT�cellule_1":
			case "DICT�cellule_2":
			case "DICT�cellule_3":
			case "DICT�cellule_4":
			case "DICT�cellule_5":
			case "DICT�cellule_6":
			case "DICT�cellule_7":
			case "DICT�cellule_8":
			case "DICT�cellule_9":
			case "DICT�cellule_10":
			case "DICT�cellule_11":
			case "DICT�cellule_12":
				$(oldId).value = "";
				$(oldId).defaultValue = "";
				
				if(!doCallBack)
				{
					doCallBack = true;
					return false;
				}
				
				alert(Rsp[1]);
				break;

			default:
				$(oldId).value = "";
				$(oldId).defaultValue = "";
				return false;
				break;
		}
	}
	
	// trace(" fin callback avant maj defaultValue pour le champ : " + oldId)
	$(oldId).defaultValue = $(oldId).value;
	
	if(!doCallBack)
	{
		doCallBack = true;
		return true;
	}

	if($(oldId).isobli=="1"&&$(oldId).value.trim()=="")
		return;
	
    setFocus ( TabFocusEntree , oldId )
	return true;
}

function getValeurRsp(motcle, joint) {
	if(joint) {
		return setValueFromKeyword(motcle, true, joint, "", true).trim();
	} else {
		return setValueFromKeyword(motcle, false, "", "", true).trim();
	}
}

// Pte 06/04/2017 melange cellule: fonction comment�e
/*
function testBio(codcerOld, campOld, codcerNew, campNew, codmvt)
{
	var dogFonction = "tst-bio�a-rotation";
	var inputParameter = "CODSOC�<?=$c_codsoc?>"
				   + "�campagne-old�"  + campOld.trim()
				   + "�CODCER-old�"  + codcerOld.trim()
				   + "�campagne-new�"  + campNew.trim()
				   + "�CODCER-new�"  + codcerNew.trim()
				   + "�CODMVT�" + codmvt.trim();

	//Envoyer la demande
	var retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "" , "" );

	if(retourExchange == "NODATA" || retourExchange == "OK")
	{
		if(retourExchange == "OK")
		{
			if(getValeurRsp("TYPE-ERREUR") == "B")
				return "NOK";
		}
	}
	else
		return "NOK";
	return "OK";
}
*/

/****** GRID DES BONS *********/
var gridBons = new AW.Grid.Extended;
gridBons.init = function()
{
	var largeurGridBons = "100%";
	var hauteurGridBons = parseInt($("divGridBons").style.height.slice(0,-2)) - 31;
	createTableau("divGridBons","tabGridBons", hauteurGridBons, largeurGridBons, "gridBons");
	
	//on charge le tableau vide avec les bons libell�s et les largeurs
	var varHeaderT = strcollibelGridBons.split("�");
	var varColWidthT = strcolwidthGridBons.split("�");
	this.setId("tabGridBons");
	
	$("divGridBons").style.paddingTop = "1px";
	
	initTableau(this, varHeaderT, varColWidthT, hauteurGridBons, largeurGridBons);
	chargeGridFormat(strenteteGridBons, strcolalignGridBons, strcolformatGridBons, this, strcolwidthGridBons, strcollibelGridBons);
	// gridBons.setColumnIndices([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]);
	
	// this.setColumnCount(varHeaderT.length);
	
	this.setSelectionMode("single-row");

	this.onCurrentRowChanged  = function(row)
	{
		// trace("onCurrentRowChanged")
		try{
		// trace("--- gridBons.onCurrentRowChanged  | row -> "+ row);
		currow = parseInt(row);
		if(row < 0)
			return false;
		}
		catch(err){trace(err)}
		try{
		isSaisieEnCours = false;
		bonafficher(currow);
		}
		catch(err){trace(err)}
	}

	this.onRowDoubleClicked = function(e, row)
	{
		trace("onRowDoubleClicked")
		isSaisieEnCours = true;
		try{
			// bonafficher(row, true);
			bonModifier();
			if($("btn_ouv_grid").className == "navImg tailleImg15 upImg15")
				ouvGrid();
		}
		catch(err){trace(err)}
	}

	this.onKeyEnter = function(e)
	{
		trace("keyEnter")
		isSaisieEnCours = true;
		try{
		row = parseInt(this.getCurrentRow());

            if (row < 0) return;

		bonafficher(row, true);
		}
		catch(err){trace(err)}

	}
	this.onKeyF2 = function(e)
	{
		trace("KeyF2")
		// trace("F2 GRID")
		Event.stop(e)
	}
}

/***** FIN GRID DES BONS ******/

/**** GRID EXECUTION *********/
gridExec.init = function()
{
	try{
	var largeurGridExec = parseInt($("divGridExec").style.width.slice(0,-2)) - 15;
	var hauteurGridExec = parseInt($("divGridExec").style.height.slice(0,-2)) - 31;
	createTableau("divGridExec","tabGridExec", hauteurGridExec, largeurGridExec, "gridExec");
	//on charge le tableau vide avec les bons libell�s et les largeurs
	var varHeaderT = strcollibelGridExec.split("�");
	var varColWidthT = strcolwidthGridExec.split("�");
	this.setId("tabGridExec");
	$("divGridExec").style.paddingTop = "1px";
	initTableau(this, varHeaderT, varColWidthT, hauteurGridExec, largeurGridExec);
	// this.setColumnCount(varHeaderT.length);
	chargeGridFormat(strenteteGridExec, strcolalignGridExec.split("�"), strcolformatGridExec.split("�"), this, varColWidthT, varHeaderT);
	// this.setColumnIndices([0,1,2,3,4,5,6,7,8,9]);
	this.setSelectionMode("single-row");

	this.onCurrentRowChanged  = function(row)
	{
		trace("--- gridExec.onCurrentRowChanged  | row -> "+ row);
		currow2 = parseInt(row);

	}

	this.onRowDoubleClicked = function(e, row)
	{
		try
		{
			validExecution();
		}
		catch(err){trace(err)}
	}

	this.onKeyEnter = function(e)
	{
		Event.stop(e);
		validExecution();
	}

	this.loadTab = function()
	{
		try{

		this.setCellData([]);
		this.setRowCount(0);
		var RspSave = Rsp.clone();
		dataGridExec = [];
		// tabEnregExec = [];
		var ligne = [];
		// RspLiv.shift();
		// chargeGrid("<?=$GLOBALS['strenteteGridExec']?>", "<?=$GLOBALS['strcolalignGridExec']?>", "<?=$GLOBALS['strcolformatGridExec']?>", gridExec, "", "",RspLiv,true);
		//Cr�ation du tableau
		for(var indice=1; indice<RspSave.length; indice++)
		{
			Rsp[1] = RspSave[indice];
			// trace(Rsp[1])
			// tabEnregExec.push(Rsp[1]);
			//Cr�ation d'une nouvelle ligne dans le tableau
			ligne = [];
			ligne.push(getValue("LL-NUMLIG", false, "", "", true));


			ligne.push(getValue("DAT-MIN", false, "", "", true));


			ligne.push(getValue("DAT-MAX", false, "", "", true));

			var refer = setValueFromKeyword("REFER-CNT", false, "", "", true);
			// trace(refer)
			if(!refer)
			{
				// trace("DNAS LE IF")
				refer = "";
			}
			// trace("refer == " + refer)
			// trace(refer)
			ligne.push(refer);
			// trace("ligne")
			// trace(ligne[0]);
			// trace(ligne[1]);
			// trace(ligne[2]);
			// trace(ligne[3]);
			dataGridExec.push(ligne);
			// trace(dataGridExec)
		} //fin de la boucle de cration des lignes du tableau
		// this.setCellData([[1, "01/01/2014", "05/02/2014", ""], [2, "01/01/2014", "05/02/2014", ""], [3, "01/01/2014", "05/02/2014", ""]]);
		this.setCellData(dataGridExec);
		this.setRowCount(dataGridExec.length);
		// this.setRowCount(3);
		this.setColumnCount(4);
		this.refresh();
		}catch(err){trace(err)}
	}
	}
	catch(err){trace("Erreur dans le init gridExec -> " + err)}
}
/**** FIN GRID EXECUTION ******/


topSuppr       = false;
topSuppression = false;

function bonafficher(cur_lig, top, avoir)
{
    $("DICT�motif").setAttribute("obli", "");
	
	if(topSuppr)
	{
		topSuppr=false;
		bonsupprimer(true);
		return;
	}
	if(topSuppression)
	{
		bonsupprimer();
		topSuppression=false;
		return;
	}
	if(isSaisieEnCours && !(top))
	{
		alert("Une saisie est d�ja en cours");
		return;
	}

	$("DICT�pdsnormes").style.color = "";
	$("DICT�topmajps").value        = "";
	codauxOrigine = "";
	typauxOrigine = "";
	poidsMin = 0;
	poidsMax = 0;
	poidsCST = 0;

	//on se fait si besoin un acces magasin
	retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�ACTIVI�APP�ACTIF�O�CODSOC�<?=$c_codsoc?>�MAGASI�"+$("DICT�magasi").value, "GET-MAGASI�n-magasi", "", "");
	
	//on regarde si on a ramene qque chose
	if (retourExchange != "OK")
	{
		alert("Vous n'�tes pas autoris� � ce magasin");
		return false;
	}

    cur_lig = parseInt(cur_lig);

	if($("divOnglets").style.display == "none")
		$("divOnglets").show();

    // Pte on ne teste que la valeur du mot cl� BONMODIF qui prends d�j� en compte le type d'operateur...
    if( getField(RspBons[currow],'BONMODIF').toUpperCase() == "N")
	{
		visu = true;
		alert ("Bon non modifiable !");
	}
	else
		visu = false;
	
	try{Rsp[1] = chr10restore(Rsp[1])}catch(ex){}
	
	RspMvt = RspBons[currow];
	
	if(getField(RspMvt[1],'PLUSMOINS')=="N")
		$("bt1").style.display = "none"
	else
		$("bt1").style.display=""
	
	isCreation = false;
	
	clearZone("inputF10Fosse");
	
	Rsp[1] = RspMvt;
	
	//sauvegarde des donn�es cach�es
	poidsMin = Math.abs(parseFloatStr(getValeurRsp("POIDS-MINI", false, "", "", true)));
	
	if(isNaN(poidsMin)) 
		poidsMin = 0;
	
	poidsMax = Math.abs(parseFloatStr(getValeurRsp("POIDS-MAXI", false, "", "", true)));
	
	if(isNaN(poidsMax)) 
		poidsMax = 0;
	
	poidsCST = Math.abs(parseFloatStr(getValeurRsp("PDSNORMES", false, "", "", true)));
	
	if(isNaN(poidsCST)) 
		poidsCST = 0;
	
	// Affichage des donn�es
	rowidCours			 				= getValeurRsp('ROWID');
	codsoc				 				= getValeurRsp('CODSOC');
	
	try{$('DICT�codsoc-bon').value 		= getValeurRsp('CODSOC-BON');} catch(ex){};
	
	$('DICT�typaux').value 				= getValeurRsp('TYPAUX');
	
	try{$('DICT�typaux_libel').value 	= getValeurRsp('LIBEL','TYPAUX');}catch(ex){};
	
	$('DICT�codauxges').value 			= getValeurRsp('CODAUXGES');
	$('DICT�codauxges_libel').value 	= getValeurRsp('ADRESSE_1','CODAUXGES');
	$('DICT�magasi').value 				= getValeurRsp('MAGASI');
	$('DICT�magasi_libel').value 		= getValeurRsp('LIBEL','MAGASI');
	$('DICT�magas2').value 				= getValeurRsp('MAGAS2');
	$('DICT�magas2_libel').value 		= getValeurRsp('LIBEL','MAGAS2');
	$('DICT�campagne').value 			= getValeurRsp('CAMPAGNE');
	
	try{$('DICT�cer-periode').value 	= getValeurRsp('CER-PERIODE');} catch(ex){};
	
	$('DICT�cer-datsai').value 			= getValeurRsp('CER-DATSAI');
	$('DICT�datbon').value 				= getValeurRsp('DATORI');				
	$('DICT�heubon').value 				= getValeurRsp('HEUORI');
	cer_chrono				 			= getValeurRsp('CER-CHRONO');
	
	try{$('DICT�cer-region').value 		= getValeurRsp('CER-REGION');} catch(ex){}
	
	$('DICT�codmvt').value 				= getValeurRsp('CODMVT');
	$('DICT�codmvt_libel').value 		= getValeurRsp('LIBEL','CODMVT');
	$('DICT�codcer').value 				= getValeurRsp('CODCER');
	$('DICT�codcer_libel').value 		= getValeurRsp('LIBEL','CODCER');

	if(getValeurRsp('VARIETE') != "") 
		$('DICT�codcer_libel') 			= getValeurRsp('LIBEL','VARIETE');

	try{$('DICT�famcer').value 			= getValeurRsp('FAMCER');} catch(ex){}
	
	$('DICT�numpesee').value 			= getValeurRsp('NUM-PESEE');
	
	try{$('DICT�famcer_libel').value 	= getValeurRsp('LIBEL','FAMCER');} catch(ex){}
	try{
		$('DICT�sfmcer').value			= getValeurRsp('SFMCER');
		$('DICT�sfmcer_libel').value 	= getValeurRsp('LIBEL','SFMCER');
	} catch(ex){}
	
	$('DICT�cellule').value 			= getValeurRsp('CELLULE');
	$('DICT�rotation').value 			= getValeurRsp('ROTATION');
    $('DICT�e-cellule').value 			= getValeurRsp('E-CELLULE');
	$('DICT�typtrp-ctr').value 			= getValeurRsp('TYPTRP');
	$('DICT�typaux_chauff').value 		= getValeurRsp('TYPAUX-CHAUF');
	$('DICT�codaux_chauff').value 		= getValeurRsp('CODAUX-CHAUF');
	$('DICT�codaux_chauff_libel').value = getValeurRsp('LIBEL','CODAUX-CHAUF');
	$('DICT�transp').value 				= getValeurRsp('TRANSP');
	$('DICT�transp_libel').value 		= getValeurRsp('LIBEL','TRANSP');
	$('DICT�chauff').value 				= getValeurRsp('CHAUFF');
	$('DICT�cer-contrat').value 		= getValeurRsp('CER-CONTRAT');

	getValue("CER-CONTRAT-FIL",false,"","DICT�cer-contrat-fil",true);

	//on sauvegarde l'ancien contrat pour le renvoyer
	anccontrat = getValue("CER-CONTRAT",false,"","",true);

	getValue("VARIETE",false,"","DICT�variete",true);
	// getValue("NUMORD-EXEC",false,"","DICT�numoex",true);
	getValue("NUMORD-EXEC",false,"","DICT�no-ordre",true);
	
	getValue("INDICE-LIV",false,"","DICT�indice-liv",true);
	getValue("IMMATR",false,"","DICT�immatr",true);
	getValue("PDSBRUT",false,"","DICT�pdsbrut",true);
	getValue("PDSNET",false,"","DICT�pdsnet",true);
	$("DICT�pdsnet").value=$("DICT�pdsnet").value.trim()
	getValue("TARE",false,"","DICT�tare",true);
	getValue("PDSNORMES",false,"","DICT�pdsnormes",true);
	$("DICT�pdsnormes").value=$("DICT�pdsnormes").value.trim()
	if(getField(Rsp[1],"TOP-PDSNORMES")=="F"){
		$("DICT�pdsnormes").style.color="red"
	}else{
		$("DICT�pdsnormes").style.color=""
	}
	topForce=false;
	if("<?=$_pdsnormes?>"=="FORCE"){
		$('forcepdsnormes').style.display="block";
	}else{
		$('forcepdsnormes').style.display="none";
	}

   // Pte Gestion des lots apports
    ccCodLot = getValue("CODLOT", false, "", "", true);
    $("DICT�codlot").value = ccCodLot.substring(0,5);
    $("DICT�numlot").value = ccCodLot.substring(5,8);

	
    getValue ("LIB-LOT", false, "", "DICT�lib-lot", true);

	getValue("PDSRESTE",false,"","DICT�pdsreste",true);
	getValue("NO-TICKET",false,"","DICT�no-ticket",true);
	getValue("CER-PRIX",false,"","DICT�cer-prix",true);
	getValue("PU-TRANSP",false,"","DICT�pu-transp",true);
	getValue("CER-TYPFAC",false,"","DICT�cer-typfac",true);
	getValue("CER-TYPBON",false,"","DICT�cer-typbon",true);
	getValue("CER-TYPSTO",false,"","DICT�cer-typsto",true);
	getValue("CER-SORENT",false,"","DICT�cer-sorent",true);
	getValue("MANU-MAJO",false,"","DICT�manu-majo",true);
	getValue("CER-HUM",false,"","DICT�cer-hum",true);
	getValue("CER-IMP",false,"","DICT�cer-imp",true);
	getValue("CER-PS",false,"","DICT�cer-ps",true);
	getValue("TRAINS",false,"","DICT�trains",true);
	getValue("NETTOYAGE",false,"","DICT�nettoyage",true);
	getValue("INSECT",false,"","DICT�insect",true);
	getValue("DOSAGE",false,"","DICT�dosage",true);
	getValue("SECHAGE",false,"","DICT�sechage",true);
	getValue("COD-ADRLIV",false,"","DICT�cod-adrliv",true);
	getValue("BL-INTERNE",false,"","DICT�bl-interne",true);
    getValue("PARCELLE_1",false,"","DICT�codeparc",true);
    getValue("CHRONOPARC_1",false,"","DICT�chronoparc",true);
    getValue("LIBPARCELLE_1",false,"","DICT�lib-parcelle",true);
	getValue("CODCER-ORI",false,"","DICT�codcer-ori",true);
	getValue("CER-LIBEL",false,"","DICT�cer-libel",true);
	getValue("DATE-MAJO",false,"","DICT�date-majo",true);
	cer_bontrs_1 = getValeurRsp('CER-BONTRS-1');
	
	//mouchard
	getValue("OPECRE",false,"","DICT�opecre",true);
	getValue("OPECRE",false,"","DICT�opecre2",true);
	getValue("OPEMAJ",false,"","DICT�opemaj",true);
	getValue("DATCRE",false,"","DICT�datcre",true);
	getValue("DATMAJ",false,"","DICT�datmaj",true);
	getValue("HEUCRE",false,"","DICT�heucre",true);
	getValue("HEUMAJ",false,"","DICT�heumaj",true);
	
	//Vide cellule
	getValue("VIDCEL",false,"","DICT�vide-cellule",true);
	
	//adresse de livraison
	getValue("ADR-LIV_1",false,"","DICT�adr-liv_1",true);
	getValue("ADR-LIV_2",false,"","DICT�adr-liv_2",true);
	getValue("ADR-LIV_3",false,"","DICT�adr-liv_3",true);
	getValue("ADR-LIV_4",false,"","DICT�adr-liv_4",true);
	getValue("ADR-LIV_5",false,"","DICT�adr-liv_5",true);
	getValue("CER-VALID",false,"","DICT�cervalid",true);
	
	//Derniers chargements
	getValue("PROTRS_1",false,"","DICT�protrs_1",true);
	getValue("LIBEL",true,"PROTRS_1","DICT�protrs_1_libel",true);
    getValue("NIVNET_1",false,"","DICT�protrs_1_nivnet",true);
	getValue("PROTRS_2",false,"","DICT�protrs_2",true);
	getValue("LIBEL",true,"PROTRS_2","DICT�protrs_2_libel",true);
    getValue("NIVNET_2",false,"","DICT�protrs_2_nivnet",true);
	getValue("PROTRS_3",false,"","DICT�protrs_3",true);
	getValue("LIBEL",true,"PROTRS_3","DICT�protrs_3_libel",true);
    getValue("NIVNET_3",false,"","DICT�protrs_3_nivnet",true);
    
	getValue("DATRAIT",false,"","DICT�datrait",true);
	getValue("TRAITM",false,"","DICT�traitm",true);
	getValue("COMONIC",false,"","DICT�comonic",true);
	statutBon = getValeurRsp('STATUT');
	getValue("TOPMAJPS",false,"","DICT�topmajps",true);
	getValue("POSTE-CPT",false,"","DICT�poste-cpta",true);
	getValue("POSTE-CPT",false,"","DICT�poste-cptv",true);
	getValue("DATECH",false,"","DICT�datech",true);
	getValue("ECHANTILLON",false,"","DICT�echantillon",true);
	getValue("TOP-COMPL",false,"","DICT�top-complt",true);
	getValue("REF-BL",false,"","DICT�ref-bl",true);
	getValue("LIB-FIL",false,"","DICT�lib-fil",true);
	getValue("DEST-FIN",false,"","DICT�dest-fin",true);
	getValue("MODREG",false,"","DICT�modreg",true);
	getValue("CONTENANT",false,"","DICT�contenant",true);
	getValue("MOTIF",false,"","DICT�motif2",true);
	getValue("CER-TOPTVA",false,"","DICT�top-tva",true);
	getValue("INTRACOM",false,"","DICT�intracom",true);
	getValue("STOCKE",false,"","DICT�stocke",true);
	getValue("MANIPULE",false,"","DICT�manipule",true);
	annulBon = getValeurRsp('ANNUL-BON');

	getValue("COD-DEF",false,"","DICT�cod-def",true);
	getValue("DESC-DEF",false,"","DICT�desc-def",true);
	getValue("CAUSE-DEF",false,"","DICT�cause-def",true);
	getValue("TRAIT-DEF",false,"","DICT�trait-def",true);
	getValue("ACT-DEF",false,"","DICT�act-def",true);
	getValue("FIC-DEF",false,"","DICT�fic-def",true);
	getValue("NOFIC-DEF",false,"","DICT�nofic-def",true);
	getValue("COD-DEF",false,"","DICT�cod-def1",true);
	getValue("DESC-DEF",false,"","DICT�desc-def1",true);
	getValue("CAUSE-DEF",false,"","DICT�cause-def1",true);
	getValue("TRAIT-DEF",false,"","DICT�trait-def1",true);
	getValue("ACT-DEF",false,"","DICT�act-def1",true);
	getValue("FIC-DEF",false,"","DICT�fic-def1",true);
	getValue("NOFIC-DEF",false,"","DICT�nofic-def1",true);
	nomAutorise = getValeurRsp('NOM-AUTORI');
	dsd_1 = getValeurRsp('DSD-1');
	dsd_2 = getValeurRsp('DSD-2');
	heurePesee_1 = getValeurRsp('HEU-PESEE-1');
	heurePesee_2 = getValeurRsp('HEU-PESEE-2');
	datePesee_1 = getValeurRsp('DAT-PESEE-1');
	datePesee_2 = getValeurRsp('DAT-PESEE-2');
	getValue("NATCON",false,"","DICT�natcon",true);
	
	//N� scell�s
	getValue("NUM-SCELLE-1",false,"","DICT�nscelle1",true);
	getValue("NUM-SCELLE-2",false,"","DICT�nscelle2",true);
	getValue("NUM-SCELLE-3",false,"","DICT�nscelle3",true);
	
	i=0;
	for(var i = 1; i <= 14; i++)
	{
		$('DICT�libcar_'+i).value 		= getValeurRsp('libcar_'+i);
		$('DICT�val-etat-ctr_'+i).value = getValeurRsp('VAL-ETAT-CTR_'+i);
		$('DICT�val-nego_'+i).value 	= getValeurRsp('VAL-NEGO_'+i);
		$('DICT�val-etat_'+i).value 	= getValeurRsp('val-etat_'+i);

		if(getValeurRsp('TOPCAR_'+i) == "")
		{
			enableZone('DICT�val-nego_'+i+'|DICT�val-etat_'+i,false);
			clearZone('DICT�val-nego_'+i+'|DICT�val-etat_'+i);
		}

		if(getValeurRsp('CODCAR_'+i).trim().toUpperCase() == "IHU")
		{
			enableZone('DICT�val-nego_'+i+'|DICT�val-etat_'+i,false);
			clearZone('DICT�val-nego_'+i+'|DICT�val-etat_'+i);
		}

		RspTmp=Rsp.clone();
		//pour les op�rateurs d�p�ts on cache les valeurs n�goci�es
		Rsp=RspOperat.clone();
		if(getField(RspOperat[1],'KES-OPMAIT').trim().toUpperCase() == "N")
			$("DICT�val-nego_"+i).style.display="none";
        
		Rsp=RspTmp;
		if($("DICT�libcar_"+i).value.trim() == "")
			$('caract_'+i).style.visibility = 'hidden';
		else
			$('caract_'+i).style.visibility = 'visible';
	}
	
    initbufferscreen();

	zonscreenget("�val-nego_4��2�val-etat_4��2�libcar_4��2�val-etat-ctr_4��2�");  //invisible

	enableZone("DICT�transp_libel", false);
	
	if(nextZone(TabFocusEntree, "DICT�transp") == "DICT�dest-fin")
		zonscreenget("�chauff�dest-fin�4")
	
	//on vErifie si on a le droit de modifier
	if(getValue("STATUT",false,"","",true)!=""
	&& isSaisieEnCours == true
	&& avoir!=true
	&& getValue("STATUT",false,"","",true).trim().toUpperCase()!="ECLAT�")
	{
		alert("Bon "+ getValue("STATUT",false,"","",true) +", modification impossible");
		isSaisieEnCours = false;
	}
	
	//si le bon est �clat� seul si�ge avec alert peuvent modifier
	if(getValue("STATUT",false,"","",true).trim().toUpperCase()=="ECLAT�"&&isSaisieEnCours == true)
	{
		RspTmp = Rsp
		Rsp=RspOperat.clone();

		if(getValue("KES-OPMAIT", false ,"" ,"", true)=="N")
		{
			Rsp = RspTmp;
			alert("Bon "+ getValue("STATUT",false,"","",true) +", modification impossible");
			isSaisieEnCours = false;
		}
		else
		{
			Rsp = RspTmp;
			alert("Attention le bon est "+ getValue("STATUT",false,"","",true));
		}
		Rsp = RspTmp;
	}

	if(getValue("PDS-SORTIE",false,"","",true)!=""&&isSaisieEnCours == true)
	{
		alert("Bon d�bloqu�, modification impossible");
		isSaisieEnCours = false;
	}
	
	//on s'occupe ici de l affichage des zones � l'�cran
	//Recherche du param�trage du couple Mvt/Codcer
	var dogFct = "PRM-CODMVT-CODCER�a-parapp"
	var requete = "�CODSOC�<?=$c_codsoc?>�CODCER�" + $("DICT�codcer").value + "�CODMVT�" + $("DICT�codmvt").value+"�CAMPAGNE�"+$("DICT�campagne").value+"�MAGASI�"+$("DICT�magasi").value
	
	retourExchange = sendExchangeInvoke("<?=$dogExchange?>", requete , dogFct , "", "");
	
	//on regarde si on a ram�ne qque chose
	if (retourExchange != "OK")
		return false;
	
	RspCoderMvt = Rsp.clone();

	if("<?=$_ROTATION?>".trim().toUpperCase() == "O")
		ctrlcapacite();

	ParamAfficheZonesCereale();
	ParamAfficheContratFiliere();
	
	$("DICT�pdsbrut").setAttribute("buffer2", "");
	$("DICT�tare").setAttribute("buffer2", "");
	
	if(isSaisieEnCours)
	{
		enableZone("ALL", true);
		enableZone("ALL", false, zonesRecherche);
		
		//sur une modif de bon on bloque le code mouvement et le tiers
		//on d�bloque le bandeau
		if(rowidCours.trim()!="")
			zonscreenget("�codmvt��9�typaux��9�codauxges��9");
		
		//selon le param�trage on debloque le code mouvement
		if("<?=$_modmvt?>"!="")
			if($("DICT�codauxges").parentElement.style.display=="")
				zonscreenget("�codmvt��11�typaux��11�codauxges��11");
			else
				zonscreenget("�codmvt��11");

		//on m�morise les infos sur la c�r�ale
		retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�CODSOC�<?=$c_codsoc?>�CODCER�" + $("DICT�codcer").value + "�CAMPAGNE�"+$("DICT�campagne").value, "GET-CEREAL�a-cereal", "", "");
		
		//on regarde si on a ramene qque chose
		if (retourExchange != "OK")
			return false;

		for(kk=1;kk<=14;kk++)
		{
			if(getValue("TOPCAR_"+kk,false,"","",true)=="")
				zonscreenget("�val-nego_"+kk+"��0�val-etat_"+kk+"��0�");  //non saisissable

			if(getValue("CODCAR_"+kk,false,"","",true)=="IHU")
				zonscreenget("�val-nego_"+kk+"��0�val-etat_"+kk+"��0�");
			
			//pour les op�rateurs depots on cache les valeurs n�goci�es
			RspTmp = Rsp
			Rsp=RspOperat.clone();
			
			if(getValue("KES-OPMAIT", false ,"" ,"", true)=="N")
				$("DICT�val-nego_"+kk).style.display="none";

			Rsp = RspTmp;
			
			if($("DICT�libcar_"+kk).value==""){
				zonscreenget("�val-nego_"+kk+"��2�val-etat_"+kk+"��2�libcar_"+kk+"��2�val-etat-ctr_"+kk+"��2�");  //invisible
			}
		}
		
		zonscreenget("�val-nego_4��2�val-etat_4��2�libcar_4��2�val-etat-ctr_4��2�");  //invisible

		RspCodcer = Rsp.clone();

		//on memorise les infos sur le tiers
		retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�TRTSPE�DOMLUC�TYPAUX�" + $('DICT�typaux').value + "�CODAUXGES�" + $('DICT�codauxges').value, "GET-AUXGES�n-auxges", "", "");
		
		//on regarde si la saisie de l adresse est obligatoire
		if(getValue ( "SAI-ADRES"  , false , "" , ""  , true)=="1")
			$("DICT�adr-liv_1").setAttribute("obli", "1");

		RspAuxges=Rsp.clone();

		//on memorise les info sur le code mouvement
		retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�CODSOC�<?=$c_codsoc?>�CODMVT�" + $("DICT�codmvt").value, "GET-CODMVT�a-tabcer", "", "");
		
		//on regarde si on a ramene qque chose
		if (retourExchange != "OK")
			return false;

		RspCodmvt=Rsp.concat();
		
		//parametrage de l'obligation des chargements
		switch(getValue ("CHARG_1", false, "", "", true))
		{
			case "0" :
				zonscreenget("�protrs_1�"+ $("DICT�protrs_1").value +"�0");
				break;
			case "1" :
				zonscreenget("�protrs_1�"+ $("DICT�protrs_1").value +"�3");
				break;
			case "2" : case "" :
				zonscreenget("�protrs_1�"+ $("DICT�protrs_1").value +"�1");
				break;
		}
		
		switch(getValue ("CHARG_2", false, "", "", true))
		{
			case "0" :
				zonscreenget("�protrs_2�"+ $("DICT�protrs_2").value +"�0");
				break;
			case "1" :
				zonscreenget("�protrs_2�"+ $("DICT�protrs_2").value +"�3");
				break;
			case "2" : case "" :
				zonscreenget("�protrs_2�"+ $("DICT�protrs_2").value +"�1");
				break;
		}
		
		switch(getValue ("CHARG_3", false, "", "", true))
		{
			case "0" :
				zonscreenget("�protrs_3�"+ $("DICT�protrs_3").value +"�0");
				break;
			case "1" :
				zonscreenget("�protrs_3�"+ $("DICT�protrs_3").value +"�3");
				break;
			case "2" : case "" :
				zonscreenget("�protrs_3�"+ $("DICT�protrs_3").value +"�1");
				break;
		}
		
		//on regarde si on doit bloaquer la saisie du poids et des caracteristiques
		if(getValue ("MODTRF", false, "", "", true)=="N")
		{
			//les caracteristiques
			for(kk=1;kk<=14;kk++)
				zonscreenget("�val-nego_"+kk+"��9�val-etat_"+kk+"��9�");  //gris�

			zonscreenget("�pdsbrut��9�tare��9");
		}

		//on regarde si on doit afficher le +/-
		if(getValue ( "TYPTRANS" ,false ,"" ,""  , true)!="3")
			$("bt1").style.display="";

		IsCtrlCaract = true;
		IsCtrlCaract2 = false;
		
		switch(getValue ( "CTRL-CARAC" ,false ,"" ,""  , true))
		{
			case "1" :
				IsCtrlCaract = false;
			break;

			case "3" :
				IsCtrlCaract2 = true;
			break;

			case "2" :
				for(kk=1;kk<=14;kk++)
				{
					zonscreenget("�val-nego_"+kk+"��0�val-etat_"+kk+"��0�");  //gris�
					//pour les operateurs depots on cache les valeurs negociees
					RspTmp = Rsp.clone();
					Rsp=RspOperat.clone();
					if(getValue("KES-OPMAIT", false ,"" ,"", true)=="N")
						$("DICT�val-nego_"+kk).style.display="none";

					Rsp = RspTmp
					if($("DICT�libcar_"+kk).value=="")
						try{zonscreenget("�val-nego_"+kk+"��2�val-etat_"+kk+"��2�libcar_"+kk+"��2�val-etat-ctr_"+kk+"��2�") } catch(ex){}

			}
			break;
		}
		
		
		//Remplacer par un switch.
		IsCtrlCaract = true;  //On doit contr�ler la saisie
		IsCtrlCaract2 = false;
		
		switch(getValue ( "CTRL-CARAC" ,false ,"" ,""  , true))
		{
			case "1" :
				IsCtrlCaract = false; //On ne controle pas la saisie des caracteristiques
			break;

			case "2" :
				for(kk=1;kk<=14;kk++)
				{
					zonscreenget("�val-nego_"+kk+"��0�val-etat_"+kk+"��0�")  //gris�
					//pour les operateurs depots on cache les valeurs negociees
					RspTmp = Rsp
					Rsp=RspOperat.clone();
					if(getValue("KES-OPMAIT", false ,"" ,"", true)=="N")
						$("DICT�val-nego_"+kk).style.visibility="hidden"

					Rsp = RspTmp
					if($("DICT�libcar_"+kk,false,"","",true).value=="")
						try{zonscreenget("�val-nego_"+kk+"��2�val-etat_"+kk+"��2�libcar_"+kk+"��2�val-etat-ctr_"+kk+"��2�") } catch(ex){}

			}
			break;

			case "3" :
				IsCtrlCaract2 = true;
			break;
		}

		zonscreenget("�val-nego_4��2�val-etat_4��2�libcar_4��2�val-etat-ctr_4��2�")  //invisible
		
        //on regarde si on doit afficher le vide cellule
        ctrlvidecellule();
        
        /* ##Pte 08/06/2017 A quoi sert ce code ????? *****************************
		// On memorise les info sur la cellule
		if ($("DICT�cellule").value != "")
        {
        	// ##Pte 08/06/2017: AVANT retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value + "�CELLULE�"+$("DICT�cellule").value, "F10-CELLULE�a-tabcer", "", "");
            if ("<?=$_rotation?>".trim() == "")
            {
                retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value + "�CELLULE�"+$("DICT�cellule").value, "F10-CELLULE�a-tabcer", "", "");
            }
            else
			{
                retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value + "�CELLULE�"+$("DICT�cellule").value, "F10-CELLULE�a-stocer", "", "");
            }
        }
        
		RspCellule=Rsp.clone();
        **************************************************************************/

		//on rend les zones actives
		try{
		for ( i = 2 ; i<=6; i++)
			$ ( i + "title").disabled = false ;
		}
		catch(err){trace("//on rend les zones actives dans la fonction bonafficher()(l.1984 env.))")}

		//on refait un echange sur le code transporteur
		if($("DICT�typtrp-ctr").value.trim()!="")
		{
			var inputParameter = "�TYPTRP�" + $("DICT�typtrp-ctr").value.trim();
			var dogFonction="get-typtrp�a-tabcer";
			var retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");
			//Tester le retour et quitter s'il ya une erreur
			if (retourExchange == "OK")
			{
				if(getValue("TYPTRANSP",false,"","",true)=="2")
				{
					//si type de transport par le tiers alors bloquer code transporteur est immatriculation
					zonscreenget("�TRANSP��9�IMMATR��9�transp_libel��9");
				}
				// else
					// zonscreenget("�TRANSP��10�IMMATR��10");
			}
		}
		
		if(RspMvt != undefined) Rsp = Rsp.clone(); else Rsp = undefined;
		
		if(getValue("MODIF-TRP", false ,"" ,"", true)=="N")
			zonscreenget("�TRANSP��9�IMMATR��9�transp_libel��9");

		if(getValue ( "PLUSMOINS" ,false ,"" ,""  , true)=="N")
			$("bt1").style.display="none";

		//on bloque la cellule dans tous les cas
		if($("DICT�cellule").parentElement.style.display==""&&"<?=$ident?>"!="392")
			zonscreenget("�CELLULE��9");
		
		//gestion des zones de saisie de la non conformit�
		RspTmp = Rsp.clone();
		Rsp=RspOperat.clone();
		
		if(getValue("KES-OPMAIT", false ,"" ,"", true)=="N")
			zonscreenget("�CAUSE-DEF��9�TRAIT-DEF��9�ACT-DEF��9�FIC-DEF��9�NOFIC-DEF��9�CAUSE-DEF1��9�TRAIT-DEF1��9�ACT-DEF1��9�FIC-DEF1��9�NOFIC-DEF1��9");
		else
			zonscreenget("�CAUSE-DEF��10�TRAIT-DEF��10�ACT-DEF��10�FIC-DEF��10�NOFIC-DEF��10�CAUSE-DEF1��10�TRAIT-DEF1��10�ACT-DEF1��10�FIC-DEF1��10�NOFIC-DEF1��10");
		
		Rsp = RspTmp;
		ParamAfficheZonesCereale();
		ParamAfficheContratFiliere();
		
		if("<?=$_modifCell?>".toUpperCase().trim() == "YES")
		{
			enableZone("ALL", false);
			enableZone("DICT�cellule", true);
		}
		setFocus(TabFocusEntree, "DICT�campagne");
	}
	else
	{
		enableZone("ALL", false);
		// enableZone("ALL", true, zonesRecherche);


		//on memorise les info sur le code mouvement
		retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�CODSOC�<?=$c_codsoc?>�CODMVT�" + $("DICT�codmvt").value, "GET-CODMVT�a-tabcer", "", "");
		//on regarde si on a ramene qque chose
		if (retourExchange != "OK")
			return false;

		RspCodmvt=Rsp.concat();
		//on regarde si on est en achat vente ou transfert
		switch(getValue ( "TYPTRANS"  , false , "" , ""  , true))
		{
			case "1": //vente
				$("ong3_p").innerHTML="Info Vente";
				$("DIVV").style.display="";
				$("DIVC").style.display="";
				$("DIVA").style.display="none";
				break;
			case "2": //achat
				$("ong3_p").innerHTML="Info Achat";
				$("DIVV").style.display="none";
				$("DIVC").style.display="none";
				$("DIVA").style.display="";
				break;
			case "3": //transfert
				$("ong3_p").innerHTML="Info Transfert";
				$("DIVV").style.display="none";
				$("DIVC").style.display="none";
				$("DIVA").style.display="none";
				break;
			default:
				break;
		}
	}

	Rsp=RspCoderMvt;
	if(getValue ( "MVTCER-AUTORI" ,false ,"" ,""  , true)=="1")
	{
		alert("Le couple Code Mouvement/Code c�real saisi n'est pas autoris�, veuillez en saisir un autre !");
		$("DICT�codcer").value="";
		$("DICT�codcer").defaultValue="";
	}
	

	Rsp = RspOperat.clone();
	//si operateur depot le topvalidation est grise
	if(getValue("KES-OPMAIT", false ,"" ,"", true)=="N")
		zonscreenget("�CERVALID��9");

	//on teste si le couple mvt / cereale est autorise
	initbufferscreen();

    if ("<?=$_modmvt?>" != "")
    {
        $("DICT�codmvt").defaultValue    = "";
        $("DICT�codcer").defaultValue    = "";
        $("DICT�codauxges").defaultValue = "";
		
        // Sans la gestion des rotations
        if ("<?=$_rotation?>" == "")
        {
            inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $("DICT�cellule").value
                           + "�CAMPAGNE�" + $("DICT�campagne").value.trim() + "�CODCER�" + $("DICT�codcer").value.trim() + "�CODMVT�" + $("DICT�codmvt").value.trim();
            dogFonction    = "get-cellule�a-tabcer";
            retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");
            RspCellule     = Rsp.clone();
            
            getValue("CAPACITE", false ,"" ,"DICT�capacite", true);
        }
        // Avec la gestion des rotations
        else
        {
            // Pte 06/04/2017 melange cellule:
            inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $("DICT�cellule").value
                           + "�DATCRE�"   + $("DICT�datbon").value          + "�HEUCRE�"  + $("DICT�heubon").value
                           + "�CAMPAGNE�" + $("DICT�campagne").value.trim() + "�CODCER�"  + $("DICT�codcer").value.trim()
			               + "�CODMVT�"   + $("DICT�codmvt").value.trim()   + "�CTRL-MEL�OUI";
            dogFonction    = "get-cellule�a-stocer";
            retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");
			RspCellule     = Rsp.clone();

			getValue("NOMBRE-1", false, "", "DICT�capacite", true);
			
            inputParameter = "ROTATION�" + $("DICT�rotation").value + "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $("DICT�cellule").value.trim();
            dogFonction    = "get-rotation�a-rotation";
			retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");
			RspRotation    = Rsp.clone();

            if (retourExchange == "OK")
            {
                setValueFromKeyword("PRODUIT",  false, "",        "DICT�rot-produit",       true);
				setValueFromKeyword("LIBEL",    true,  "PRODUIT", "DICT�rot-produit_libel", true);
                //##Pte 08/06/2017, on reccupere la rotation
                setValueFromKeyword("ROTATION", false, "",        "DICT�rotation",          true);
            }
        }
    }
	
    if(top && !visu)
    {
		
        enableZone("DICT�datbon", false);

        $("DICT�codmvt").defaultValue     = "";
        $("DICT�codcer").defaultValue     = "";
        $("DICT�variete").defaultValue    = "";
        $("DICT�codauxges").defaultValue  = "";
        $("DICT�typaux").defaultValue     = "";
        $("DICT�typtrp-ctr").defaultValue = "";
        // $("DICT�cellule").defaultValue = "";

        dogexchange($("DICT�codmvt"), "GET", false);
        dogexchange($("DICT�codcer"), "GET", false);
        dogexchange($("DICT�variete"), "GET", false);
        dogexchange($("DICT�typaux"), "GET", false);
        dogexchange($("DICT�codauxges"), "GET", false);
        dogexchange($("DICT�typtrp-ctr"), "GET", false);
        dogexchange($("DICT�magas2"), "GET", false);
		
        if ("<?=$_modmvt?>" != "")
        {
            // Sans la gestion des rotations
            if ("<?=$_rotation?>" == "")
            {
                inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $("DICT�cellule").value
                               + "�CAMPAGNE�" + $("DICT�campagne").value.trim() + "�CODCER�" + $("DICT�codcer").value.trim() + "�CODMVT�" + $("DICT�codmvt").value.trim();
                dogFonction    = "get-cellule�a-tabcer";
                retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");
                RspCellule     = Rsp.clone();

                getValue("CAPACITE", false ,"" ,"DICT�capacite", true);
            }
            // Avec la gestion des rotations
            // Pte 06/04/2017 melange cellule:
            else
			{
                inputParameter = "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $("DICT�cellule").value
                               + "�DATCRE�"   + $("DICT�datbon").value          + "�HEUCRE�"  + $("DICT�heubon").value
                               + "�CAMPAGNE�" + $("DICT�campagne").value.trim() + "�CODCER�"  + $("DICT�codcer").value.trim()
                               + "�CODMVT�"   + $("DICT�codmvt").value.trim()   + "�CTRL-MEL�OUI";
                dogFonction    = "get-cellule�a-stocer";
                retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");
                RspCellule     = Rsp.clone();
				
                getValue("NOMBRE-1", false, "", "DICT�capacite", true);
				
                Rsp    = [];
                Rsp[0] = "OK";
                Rsp[1] =  RspBons[currow];
                
                getValue("ROTATION", false, "", "DICT�rotation", true);

                inputParameter = "ROTATION�" + getValue("ROTATION", false, "", "", true) + "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $("DICT�cellule").value.trim();
                dogFonction    = "get-rotation�a-rotation";
                retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");
                RspRotation    = Rsp.clone();
                
                if (retourExchange == "OK")
                {
                    setValueFromKeyword("PRODUIT",  false, "",        "DICT�rot-produit",       true);
                    setValueFromKeyword("LIBEL",    true,  "PRODUIT", "DICT�rot-produit_libel", true);
                    //##Pte 08/06/2017, on reccupere la rotation
                    setValueFromKeyword("ROTATION", false, "",        "DICT�rotation",          true);
                }
			}
		}
	}
	else
	{
		enableZone("ALL", false);
	}
	
	if(!isCreation && isSaisieEnCours)
	{
		zonscreenget("�solde-vide��2");

	}
	else if (isCreation)
	{
		zonscreenget("�solde-vide��9");
	}
	zonscreenget("�pdsnormes��9");
	$("DICT�solde-vide").value = parseFloatStr($("DICT�solde-vide").value.trim()).toFixed(3);
	
	
	try{
	if ( $ ("2title").staticheight  !=  $ ("2title").style.height )
		hidedata('2');
	}catch(err){trace("hidedata 2");}
	
	if(gridBons.getCellText(0, gridBons.getCurrentRow()) == "A" && isSaisieEnCours)
    {
    	if($("DICT�pdsbrut").value.trim() == "" || parseFloatStr($("DICT�pdsbrut").value.trim()) == 0.0)
		{
			$("DICT�pdsbrut").focus();
			$("DICT�pdsbrut").select();
		}
		else
		{
			$("DICT�tare").focus();
			$("DICT�tare").select();
		}
    }
    
    return true;
}


function boneclatement(pds)
{
	trace('--boneclatement--');
	
	if(!isSaisieEnCours || visu)
		return false;
	
	isCreation3 = false ;
	
	//on verifie que des transporteurs interdits ne sont pas saisis
	if("<?=$_trpdep?>"!="")
		if("<?=$_trpdep?>"==$("DICT�transp").value)
		{
			alert("Transporteur non autoris�");
			return;
		}

	if($("DICT�tare").value.trim() == "")
	{
		$("DICT�tare").value = "0.00";
	}
	
	if("<?=$_trprend?>"!="")
		if("<?=$_trprend?>"==$("DICT�transp").value)
		{
			alert("Transporteur non autoris�");
			return;
		}

	if (!isSaisieEnCours)
	{
		alert("Aucune saisie en cours");
		return;
	}
	
    //on verifie les zones obligatoires
    if(!topattente)
		if(!ctrlobli())
		{
			alert("Veuillez saisir les zones obligatoires");
			return;
		}
    
	//on regarde si l'odre d'expedition est obligatoire on test le topelement_16
	Rsp=RspCoderMvt
	if(setValueFromKeyword("TOPELEMENT_16",false,"","",true)=="1")
		if($("DICT�numoex").value.trim()=="")
		{
			alert("Veuillez saisir l'ordre d'exp�dition");
			return;
		}

	if($("DICT�numoex").value.trim()!=""&&$("DICT�indice-liv").value.trim()=="")
	{
		alert("Veuillez selectionner au moins une ligne de l'ordre d'exp�dition");
		return;
	}

	//si l'oe par le param est facultatif on regarde si l'odre d'expedition est rendu et si ds le menu on dit que rendu c'est obli on affiche le message
	if(setValueFromKeyword ( "TOPELEMENT_16" ,false ,"" ,""  , true)=="2")
	{
		Rsp=RspContrat;
		if(setValueFromKeyword("TYPTRP-CTR",false,"","",true)!=""&&"<?=$_rendu?>"=="Oblig")
			if($("DICT�numoex").value.trim()=="")
			{
				alert("Veuillez saisir l'ordre d'exp�dition");
				return;
			}

	}
	//si OE existant on verifie la coherence avec le tiers et la cereale
	if($("DICT�numoex").value.trim()!="")
	{
		if(RspVoe==undefined)
		{
			//on se refait un acces
			//Construction du inputParameter
			inputParameter = "CODSOC�<?=$c_codsoc?>�TYPOEX�VOE�TYPCDE�VCD�TYPCTR�V";
			inputParameter +="�NUMOEX�"+$("DICT�numoex").value.trim();
			//acces execution
			retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "get-ceroex�a-cerctr", "", "");
			RspVoe=Rsp.clone();
		}
		Rsp=RspVoe;
		//on verifie la coherence du contrat
		if(setValueFromKeyword("CODCER",false,"","", true).trim().toUpperCase()!=$("DICT�codcer").value.trim().toUpperCase())
		{
			alert("Attention le code c�r�ale du contrat ne correspond pas");
			return;
		}
		if(setValueFromKeyword("CODAUXGES",false,"","", true).trim().toUpperCase()!=$("DICT�codauxges").value.trim().toUpperCase())
		{
			alert("Attention le code tiers du contrat ne correspond pas");
			return;
		}
		if(setValueFromKeyword("CER-CONTRAT",false,"","", true).trim().toUpperCase()!=$("DICT�cer-contrat").value.trim().toUpperCase())
		{
			alert("Attention le code tiers du contrat ne correspond pas");
			return;
		}
	}
	//on verifie si on a bien valid� sur la zone tiers
	if($("DICT�codauxges").defaultValue != $("DICT�codauxges").value)
		if("<?=$ident?>"=="677")
		{
			alert("Veuillez valider la zone tiers");
			return;
		}

	//test saisie FSO sur un tiers qui n'a pas de FAP
	if(ctrlRepriseFin()==false)
	{
		return false;
	}

	//si operatyeur depot oe obligatoire
	Rsp=RspCodmvt.concat();

	//si selon le code mouvement les caracteristiques sont obligatoires
	if(setValueFromKeyword ( "CTRL-CARAC" ,false ,"" ,""  , true)=="3")
	{
		for(kk=1;kk<=14;kk++)
		{
			Rsp=RspCodmvt.concat();
			if($("DICT�libcar_"+kk).value!=""&&kk!=4)
			{
				if($("DICT�val-etat_"+kk).value.trim()==""&&$("DICT�val-etat_"+kk).readOnly!=true)
				{
					alert("les valeurs des caract�ristiques sont obligatoires");
					return;
				}
				if(parseFloatStr($("DICT�val-etat_"+kk).value)==0&&$("DICT�val-etat_"+kk).readOnly!=true)
				{
					//Recuperation des valeurs mini et maxi des caracteristiques param�tr�es
					Rsp=RspCodcer.clone(); //reponse du get codcer
					valmini=setValueFromKeyword("MINCAR_"+kk, false ,"" ,"", true);
					if(valmini=="")
						valmini=0;

					if(valmini!=0)
						valmini=parseFloatStr(valmini);

					if(valmini!=0)
					{
						alert("les valeurs des caract�ristiques sont obligatoires");
						return;
					}
				}
			}
		}
	}
	//cas des cellules virtuelles
	Rsp=RspCellule;
	if(setValueFromKeyword("TYPCEL", false, "" , "", true)=="V" && isCreation)
	{//-> afficher la multicellule
		if(!ctrlMulti())
			return false;
	}

	// cas des transilage
	if($("DICT�e-cellule").value.trim() != "" && $("DICT�magas2").value.trim()=="")
	    $("DICT�magas2").value = $("DICT�magasi").value;

	//dans le cas des transfert entree pour les depot informatise on s appuye obli sur un transfert sortie
	Rsp=RspMagas2;
	if(setValueFromKeyword("TOPASS", false, "", "", true)=="0"||RspMagas2==undefined)
	{
		Rsp=RspCodmvt.concat();
		if(setValueFromKeyword ( "TYPTRANS"  , false , "" , ""  , true)=="3"&&setValueFromKeyword ( "TYPMVT"  , false , "" , ""  , true)=="1" && cer_bontrs_1.trim()=="")
		{
			alert ("Rapprochement d'un tranfert sortie obligatoire");
			return;
		}
	}
	//dans le cas des modifications on a pas le droit de modifier certains champ
	if(rowidCours.trim()!="")
	{
		if(RspMvt!=undefined)
		{
			Rsp=RspMvt;
			topER=false;
			if(setValueFromKeyword("TYPAUX",false,"","",true).toUpperCase() != "***" && setValueFromKeyword("TYPAUX",false,"","",true).toUpperCase()!=$("DICT�typaux").value.trim().toUpperCase())
				topER=true;
			if(setValueFromKeyword("TYPAUX",false,"","",true).toUpperCase() != "***" && setValueFromKeyword("CODAUXGES",false,"","",true).toUpperCase().trim()!=$("DICT�codauxges").value.trim().toUpperCase().trim())
				topER=true
			if(setValueFromKeyword("CODMVT",false,"","",true).toUpperCase()!=$("DICT�codmvt").value.trim().toUpperCase())
				topER=true
			if(topER==true&&"<?=$_modmvt?>"=="")
			{
				alert("le code mouvement et le tiers ne peuvent �tre modifi�");
				return;
			}
		}
	}

	$('ECLATEMENT').show();
	$("DICT�pdsbrutini").value 		= $("DICT�pdsbrut").value;
	$("DICT�tareini").value 		= $("DICT�tare").value;
	
	$("DICT�pdsnetini").value = $("DICT�pdsnet").value;
		
	var qteContrat = parseFloatStr(getField(RspContrat[1],'TON-SOLDE'));

	ecartArrondis = 0;
	if(modifBon && deltaQteModifBon > 0)
	{
		if(carOrgeSpe && saisieCalibrage())
			$("DICT�pdsnormesini").value = calcOrgeCalibre('RESTE')
		else
			$("DICT�pdsnormesini").value = parseFloatStr(deltaQteModifBon).toFixed(3);
			//$("DICT�pdsnormesini").value = parseFloatStr(deltaQteModifBon - qteContrat).toFixed(3);
	}
	else
	{
		if(carOrgeSpe && saisieCalibrage())
			$("DICT�pdsnormesini").value 	= calcOrgeCalibre().toFixed(3);
		else
			$("DICT�pdsnormesini").value 	= parseFloatStr($('DICT�pdsnormes').value);
	}
	
	//var pdsContrat = parseFloatStr(getField(RspContrat[1], 'TON-SOLDE'));
	// trace("	CALCULE RESTE")
	
	// $('DICT�pdsresteini').value	= ((100000*pds)-(100000*pdsContrat))/100000;
	
	/* var total = 0;
	for(var i=0; i<gridEclat.getRowCount(); i++)
		if(gridEclat.getCellText(getColEclat("pdsNormes") , i) != "")
			total += parseFloatStr(gridEclat.getCellText(getColEclat("pdsNormes") , i));
	
	var pds = parseFloatStr($('DICT�pdsnormesini').value);
	var reste = ((100000*pds)-(100000*total))/100000;
		
	$('DICT�pdsresteini').value = reste.toFixed(3); */
	
	majQteRestanteEclatement()
	
	dataPdsNormes = [];
	if(parseInt(gridEclat.getRowCount()) == 0)
		(eclatementSPE)? gridEclat.addRowEmptySPE():gridEclat.addRowEmpty();
}

/**
	Calcul la quantit� restante � �clater.
**/
function majQteRestanteEclatement()
{
	
	
	var total = 0;
	for(var i=0; i<gridEclat.getRowCount(); i++)
		if(gridEclat.getCellText(getColEclat("pdsNormes") , i) != "")
			total += parseFloatStr(gridEclat.getCellText(getColEclat("pdsNormes") , i));
	
	var pds = parseFloatStr($('DICT�pdsnormesini').value);
	var reste = (((100000*pds)-(100000*total))/100000);
			
	$('DICT�pdsresteini').value = Math.abs(reste.toFixed(3));
}

function bonEclatValider()
{
	if(isCreation3 || isSaisieEnCours3 || parseInt($('DICT�pdsresteini').value) != 0)
	{
		alert("Veuillez terminer la saisie de l'�clatement !")
		return false;
	}
	
	//POUR ECLATTEMENT SPE => test si reste = 0
	if(eclatementSPE && parseInt($('DICT�pdsresteini').value) != 0)
	{
		alert("Veuillez terminer la saisie de l'�clatement !")
		return false;
	}
	
	if(gridEclat.getRowCount() == 0)
	{
		$('ECLATEMENT').hide();
		return false;
	}
	
	if(!eclatementSPE)
	{
		
		$("DICT�pdsnet").value 			= nombre_arrondi(parseFloatStr($("DICT�pdsresteini").value),3);
		$("DICT�pdsnet").defaultValue 	= nombre_arrondi(parseFloatStr($("DICT�pdsresteini").value),3);
		$("DICT�pdsbrut").value 		= nombre_arrondi(parseFloatStr($("DICT�pdsnet").value) + parseFloatStr($("DICT�tare").value),3);
		$("DICT�pdsbrut").defaultValue 	= nombre_arrondi(parseFloatStr($("DICT�pdsnet").value) + parseFloatStr($("DICT�tare").value),3);		
		ctrlpdsnormes();
	}
	else
	{
		
		var nbEclatSansContrat = 0;
		for(var i=0; i<gridEclat.getRowCount(); i++)
		{
			if(gridEclat.getCellData(getColEclat('contrat'),i) == '')
				nbEclatSansContrat++;
			
			if(nbEclatSansContrat > 1)
			{
				alert("Vous ne pouvez saisir qu'un mouvement sans contrat.");
				return;
			}
		}
	}
	
	$('ECLATEMENT').hide();
	bonvalider();
}

function bondupliquer()
{
	RspCermvtReciproque = Rsp.clone();
	//sauvagerde des donn�es cach�es
	poidsMin = Math.abs(parseFloatStr(getValeurRsp("POIDS-MINI", false, "", "", true)));
	if(isNaN(poidsMin)) poidsMin = 0;
	poidsMax = Math.abs(parseFloatStr(getValeurRsp("POIDS-MAXI", false, "", "", true)));
	if(isNaN(poidsMax)) poidsMax = 0;
	poidsCST = Math.abs(parseFloatStr(getValeurRsp("PDSNORMES", false, "", "", true)));
	if(isNaN(poidsCST)) poidsCST = 0;

	//Affichage des donn�es
	codsoc = getValeurRsp('CODSOC');
	setValueFromKeyword("CODSOC-BON",false,"","DICT�codsoc-bon",true);
	setValueFromKeyword("TYPAUX",false,"","DICT�typaux",true);
	setValueFromKeyword("LIBEL",true,"TYPAUX","DICTLINK�typaux�libel",true);
	setValueFromKeyword("CODAUXGES",false,"","DICT�codauxges",true);
	setValueFromKeyword("ADRESSE_1",true,"CODAUXGES","DICTLINK�codauxges�libel",true);
	setValueFromKeyword("MAGASI",false,"","DICT�magas2",true);
	setValueFromKeyword("LIBEL",true,"MAGASI","DICTLINK�magas2�libel",true);
	setValueFromKeyword("CAMPAGNE",false,"","DICT�campagne",true);
	setValueFromKeyword("CER-PERIODE",false,"","DICT�cer-periode",true);
	setValueFromKeyword("CER-REGION",false,"","DICT�cer-region",true);
	setValueFromKeyword("LIBEL",true,"CODMVT","DICTLINK�codmvt�libel",true);
	setValueFromKeyword("CODCER",false,"","DICT�codcer",true);
	setValueFromKeyword("LIBEL",true,"CODCER","DICTLINK�codcer�libel",true);
	setValueFromKeyword("FAMCER",false,"","DICT�famcer",true);
	setValueFromKeyword("LIBEL",true,"FAMCER","DICTLINK�famcer�libel",true);
	setValueFromKeyword("SFMCER",false,"","DICT�sfmcer",true);
	setValueFromKeyword("LIBEL",true,"SFMCER","DICTLINK�sfmcer�libel",true);
	setValueFromKeyword("CELLULE",false,"","DICT�e-cellule",true);
	setValueFromKeyword("TYPTRP",false,"","DICT�typtrp-ctr",true);
	setValueFromKeyword("TRANSP",false,"","DICT�transp",true);
	setValueFromKeyword("LIBEL",true,"TRANSP","DICTLINK�transp�libel",true);
	setValueFromKeyword("CHAUFF",false,"","DICT�chauff",true);
	setValueFromKeyword("CER-CONTRAT",false,"","DICT�cer-contrat",true);
	setValueFromKeyword("IMMATR",false,"","DICT�immatr",true);
	setValueFromKeyword("PDSBRUT",false,"","DICT�pdsbrut",true);
	setValueFromKeyword("PDSNET",false,"","DICT�pdsnet",true);
	setValueFromKeyword("TARE",false,"","DICT�tare",true);
	setValueFromKeyword("PDSNORMES",false,"","DICT�pdsnormes",true);
	
	ccCodLot = setValueFromKeyword("CODLOT", false, "", "", true);
	$("DICT�codlot").value = ccCodLot.substring(0,5);
	$("DICT�numlot").value = ccCodLot.substring(5,8);
	setValueFromKeyword ("LIB-LOT", false, "", "DICT�lib-lot", true);

	setValueFromKeyword("PDSRESTE",false,"","DICT�pdsreste",true);
	setValueFromKeyword("NO-TICKET",false,"","DICT�no-ticket",true);
	setValueFromKeyword("CER-PRIX",false,"","DICT�cer-prix",true);
	setValueFromKeyword("PU-TRANSP",false,"","DICT�pu-transp",true);
	setValueFromKeyword("CER-TYPFAC",false,"","DICT�cer-typfac",true);
	setValueFromKeyword("CER-TYPBON",false,"","DICT�cer-typbon",true);
	setValueFromKeyword("CER-TYPSTO",false,"","DICT�cer-typsto",true);
	setValueFromKeyword("CER-SORENT",false,"","DICT�cer-sorent",true);
	setValueFromKeyword("MANU-MAJO",false,"","DICT�manu-majo",true);
	setValueFromKeyword("CER-HUM",false,"","DICT�cer-hum",true);
	setValueFromKeyword("CER-IMP",false,"","DICT�cer-imp",true);
	setValueFromKeyword("CER-PS",false,"","DICT�cer-ps",true);
	setValueFromKeyword("TRAINS",false,"","DICT�trains",true);
	setValueFromKeyword("NETTOYAGE",false,"","DICT�nettoyage",true);
	setValueFromKeyword("TRAITON",false,"","DICT�traiton",true);
	setValueFromKeyword("INSECT",false,"","DICT�insect",true);
	setValueFromKeyword("DOSAGE",false,"","DICT�dosage",true);
	setValueFromKeyword("SECHAGE",false,"","DICT�sechage",true);
	setValueFromKeyword("COD-ADRLIV",false,"","DICT�cod-adrliv",true);
	setValueFromKeyword("BL-INTERNE",false,"","DICT�bl-interne",true);
    setValueFromKeyword("PARCELLE_1",false,"","DICT�codeparc",true);
    setValueFromKeyword("CHRONOPARC_1",false,"","DICT�chronoparc",true);    // Pte DDD-NAT-PARCELLE
    setValueFromKeyword("CODCER-ORI",false,"","DICT�codcer-ori",true);
	//Pte setValueFromKeyword("PARCELLE",false,"","DICT�parcelle",true);      //  ?????????????
	setValueFromKeyword("CER-LIBEL",false,"","DICT�cer-libel",true);
	setValueFromKeyword("DATE-MAJO",false,"","DICT�date-majo",true);
	cer_bontrs_1 = getValeurRsp('NO-TICKET');

	i=0;
	for(kk=1;kk<=14;kk++)
	{
		setValueFromKeyword("VAL-ETAT-CTR_"+kk,false,"","DICT�val-etat-ctr_"+kk,true);
		setValueFromKeyword("VAL-NEGO_"+kk,false,"","DICT�val-nego_"+kk,true);
		setValueFromKeyword("val-etat_"+kk,false,"","DICT�val-etat_"+kk,true);
		if(setValueFromKeyword("TOPCAR_"+kk,false,"","",true)==""){
			zonscreenget("�val-nego_"+kk+"��0�val-etat_"+kk+"��0�")  //non saisissable
		}

		if(setValueFromKeyword("CODCAR_"+kk,false,"","",true)=="IHU")
			zonscreenget("�val-nego_"+kk+"��0�val-etat_"+kk+"��0�")
		//pour les operateurs depots on cache les valeurs negociees
		RspTmp = Rsp
		Rsp=RspOperat.clone();
		if(setValueFromKeyword("KES-OPMAIT", false ,"" ,"", true)=="N"){
			$("DICT�val-nego_"+kk).style.visibility="hidden"
		}
		Rsp = RspTmp
		if($("DICT�libcar_"+kk,false,"","",true).value==""){
			zonscreenget("�val-nego_"+kk+"��2�val-etat_"+kk+"��2�libcar_"+kk+"��2�val-etat-ctr_"+kk+"��2�")  //invisible
		}
	}

	zonscreenget("�val-nego_4��2�val-etat_4��2�libcar_4��2�val-etat-ctr_4��2�")  //invisible
	//on bloque le code article pour eviter des probl�mes
	//on s occupe ici de l affichage des zones a l ecran
	//Recherche du parametrage du couple Mvt/Codcer
	retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�CODSOC�<?=$c_codsoc?>�CODCER�" + $("DICT�codcer").value + "�CODMVT�" + $("DICT�codmvt").value+"�CAMPAGNE�"+$("DICT�campagne").value+"�MAGASI�"+$("DICT�magasi").value + "�CER-DATSAI�" + $("DICT�cer-datsai").value, "PRM-CODMVT-CODCER�a-parapp", "", "") ;
	//on regarde si on a ramene qque chose
	if (retourExchange != "OK") {
		return;
	}
	RspCoderMvt=Rsp.clone();
	//on teste si le couple mvt / cereale est autorise
	if(setValueFromKeyword ( "MVTCER-AUTORI" ,false ,"" ,""  , true)=="1"){
		alert("Le couple Code Mouvement/Code c�real saisi n'est pas autoris�, veuillez en saisir un autre !")
		$("DICT�codcer").focus();
		return;
	}
	ParamAfficheZonesCereale()
	ParamAfficheContratFiliere()
	if(isSaisieEnCours == true) {
		//on memorise les info sur la cereale
		retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�CODSOC�<?=$c_codsoc?>�CODCER�" + $("DICT�codcer").value + "�CAMPAGNE�"+$("DICT�campagne").value + "�CER-DATSAI�" + $("DICT�cer-datsai").value,"GET-CEREAL�a-cereal","","") ;
		//on regarde si on a ramene qque chose
		if (retourExchange != "OK") {
			return;
		}
		RspCodcer=Rsp.clone();

		//on memorise les info sur le code mouvement
		retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�CODSOC�<?=$c_codsoc?>�CODMVT�" + $("DICT�codmvt").value,"GET-CODMVT�a-tabcer","","") ;
		//on regarde si on a ramene qque chose
		if (retourExchange != "OK") {
			return;
		}
		RspCodmvt=Rsp.concat();
		if(setValueFromKeyword ( "CTRL-CARAC" ,false ,"" ,""  , true)=="1")
			IsCtrlCaract = false //On ne controle pas la saisie des caracteristiques
		else
			IsCtrlCaract = true  //On doit contr�ler la saisie (valeur 2 et 3)
		//on ne passe pas sur les caracteristiques
		if(setValueFromKeyword ( "CTRL-CARAC"  , false , "" , ""  , true)=="2"){
			$("4title").disabled = true ;
			$("4data").style.visibility="hidden";
		}
		//cas du controle bloquant
		if(setValueFromKeyword ( "CTRL-CARAC" ,false ,"" ,""  , true)=="3"){
			IsCtrlCaract2 = true
		}
		//on regarde si on doit afficher le vide cellule
		ctrlvidecellule()
		//on memorise les info sur la cellule
		if($("DICT�cellule").value!=""){retourExchange = sendExchangeTurbo ( "<?=$dogExchange?>" , "" , "�CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value + "�CELLULE�"+$("DICT�cellule").value, "get" , "" , "MESSAGE=&nbsp;&nbsp;Lecture des donn�es :cellule...|GETVAR|NOSCREEN|NODISPLAYERROR" ,"","F10-CELLULE�a-tabcer") ;
			//on regarde si on a ramene qque chose
			if (retourExchange != "OK") {
				return;
			}
			RspCellule=Rsp.clone();
		}
	} else {
	}
	initbufferscreen();
	zonscreenget("�codcer��9")
}

function bonrechercher()
{
	trace("--bonrechercher--")
	
    if($("DICT�magasi").value.trim()=="" && rowidCours.trim()=="")
	{
		alert("Veuillez saisir le code magasin");
		$("DICT�magasi").focus();
		return false;
	}
	
	dogexchange($("DICT�magasi"), "GET", false);
	topDebut=false;
	
	inputParameter = "CODSOC�<?=$c_codsoc?>";
	inputParameter += "�MAGASI�" + $("DICT�magasi").value.trim();
	inputParameter += "�CAMPAGNE�" + $("DICT�campagne").value.trim();
	inputParameter += "�CER-DATSAI�" + $("DICT�cer-datsai").value.trim();
	inputParameter += "�ORDRE�<?=$_ordre?>"
	inputParameter += "�VISUANNU�<?=$_visuannu?>"
	inputParameter += "�STATUTBON�" + $("DICT�attente").value.trim();

    retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "GET-ATTENTE�a-cermvt", "", "");
	if(Rsp!="")
    {
		if(getValue("CER-DATSAI",false ,"","",true)!="")
		{
          alert("Il reste des bons en attente au : " + getValue("CER-DATSAI", false, "", "", true) + " Campagne : " + getValue("CAMPAGNE", false, "", "", true))
		}
    }

	retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "GET-CERMVT�a-cermvt", "", "");
	if(Rsp=="")
	{
		//on est ici car on s'est perdu juste avant!   (cf calcul pdsnormes?) // inutil en v7 je pense (bnj)
		inputParameter = "CODSOC�<?=$c_codsoc?>"
		inputParameter += "�MAGASI�" + $("DICT�magasi").value.trim();
		inputParameter += "�CAMPAGNE�" + $("DICT�campagne").value.trim();
		inputParameter += "�CER-DATSAI�" + $("DICT�cer-datsai").value.trim();
		inputParameter += "�ORDRE�<?=$_ordre?>";
		//on passe par nu fichier tmp tmpcontrat pour la construction du tableau en php
		// _SaveExchange = "tmpbonapp<?=session_id()?>";
		retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "GET-CERMVT�a-cermvt", "", "");
	}
	
	RspSave=Rsp.clone();
	//Tester le retour
	if (retourExchange != "OK")
	{
		//Aucun enregistrement trouv� on vide l'�cran
		gridBons.setCellData([]);
		gridBons.setRowCount(0);
		gridBons.selectRow(-1);
		gridBons.refresh();
	}
	
	Rsp = RspSave;
	if (retourExchange == "ERROR")
		alert("Une erreur est survenue " + Rsp );
	
	if(getValue("MAGASI",false,"","",true)!="")
		getValue("MAGASI",false,"","DICT�magasi",true); // ajout bp pour pouvoir tester le magasin

	// $("MULTI").style.display="none";
	// if ( $ ("1title").staticheight  !=  $ ("1title").style.height )//Si tableau cach�, le montr� ?
		// hidedata('1');
	//Cr�ation du tableau
	
	trace("topFirst == " + topFirst)
	
	if("<?=$_rowid?>"!=""&&topFirst==true)
	{
		$("DICT�attente").value = "T";
		// trace("dabns le if")
		//Charger les infos du bon (date, magasin, campagne et lancer la recherche des bons, et se positionner sur le bon ramener)
		var dogFonction = "GET-CERMVT�a-cermvt";
		var inputParameter = "ROWID�<?=$_rowid?>";
		retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");

		if(retourExchange == "OK")
		{
			getValue("CER-DATSAI", false, "", "DICT�cer-datsai", true);
			getValue("MAGASI", false, "", "DICT�magasi", true);
			getValue("CAMPAGNE", false, "", "DICT�campagne", true);
			var numbon = getValue( "no-ticket" , false , "" , "" , true);
            var codmvt = getValue( "codmvt"    , false , "" , "" , true);
            var rowid = getValue( "rowid"    , false , "" , "" , true);
            topFirst = false;
			// return false
			bonrechercher();
			// return false;
			for(var row = 0, trouve = false;!trouve  && row < gridBons.getRowCount(); row++)
			{
				if(rowid == getField(RspBons[row],"ROWID") )
				{
					gridBons.selectRow(row);
					gridBons.refresh();
					setTimeout("gridBons.focus()");
					trouve = true;
				}
			}
			return true;
		}
	}
	else
	{
		if(retourExchange != "OK")
		{
			// alert(Rsp[1]);
			return false;
		}
		
		RspBons = [];
		RspBons = Rsp.clone();
		RspBons.shift();
		
		gridBons.setRowCount(0);
		gridBons.refresh();
				
		chargeGrid("<?=$GLOBALS['strenteteGridBons']?>", "<?=$GLOBALS['strcolalignGridBons']?>", "<?=$GLOBALS['strcolformatGridBons']?>", gridBons, "", "",RspBons,true);
		gridBons.refresh();
	}

	//Raz de la variable
	Rsp = [] ;
	// for ( i = 2 ; i<=6; i++) {
		// $ ( i + "title").disabled = true ;
	// }
	enableZone("ALL", false, zonesRecherche);
	gridBons.selectRow(-1);
	setTimeout("gridBons.focus()",50);
}

//cette fonction permet d'inserer un bon
function boninserer()
{
	if("<?=$_rowid?>"!="")
	{
		alert("Non autoris�, passez par le menu");
	 	return false;
	}
	if($("DICT�campagne").value.trim()=="" || $("DICT�cer-datsai").value.trim()=="" || $("DICT�magasi").value.trim()=="")
	{
	 	alert("Veuillez saisir les donn�es de l'ent�te")
	 	return false;
	}
	if(visu)
		return false;
	if (isSaisieEnCours)
		return false;

	if(!validInput($("DICT�cer-datsai")))
		return false;

	if(!ctrlDate())
		return false;
	
	cer_bontrs_1 = "";
	
	dogexchange($("DICT�magasi"), "GET", false);
	campagneSaisie = $("DICT�campagne").value;
	clearZone("ALL", zonesOnglet1);
    $("DICT�pdsnormes").style.color = "";
	$("DICT�topmajps").value        = "";

	razEcran();
	enableZone("ALL", true);
	enableZone("DICT�transp_libel", false);
	if(nextZone(TabFocusEntree, "DICT�transp")=="DICT�dest-fin")zonscreenget("�chauff�dest-fin�4")
	var elements = Element.descendants("divOnglets");
	for(var i = 0; i < elements.length; i++)
	{
		if(elements[i].id.match("_libel"))
			elements[i].value = "";
	}
	var dogFonction = "INC-CERMVT�a-cermvt";
	var inputParameter="�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim() + "�CER-DATSAI�"+$("DICT�cer-datsai").value.trim() + "�MAGASI�"+$("DICT�magasi").value.trim();

	datsai=$("DICT�cer-datsai").value
	magasi=$("DICT�magasi").value
	camp=$("DICT�campagne").value
	magasilibel=$("DICT�magasi_libel").value
	//on lance la mise a jour
	retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");
	if (retourExchange != "OK")
	{
		alert("Probl�me sur la cr�ation d'un nouveau bon");
		return false;
	}
	
	//On r�cup�re le CER-CHRONO pour se le mettre de cot�.
	cerChrono1erMvt = getValeurRsp('CER-CHRONO');
	
	$("divSaisieDateHeureTransfert").hide();
	cer_chrono = getValeurRsp('CER-CHRONO');
	anccontrat="";
	RspMvt = undefined;
	RspVoe = undefined;
	RspMagas2 = undefined;
	RspCermvtReciproque = [];
	//on debloque certaines zones
	for(kk=1;kk<=14;kk++)
	{
		zonscreenget("�val-nego_"+kk+"��1�val-etat_"+kk+"��1�")  //gris�
		//pour les operateurs depots on cache les valeurs negociees
		RspTmp = Rsp.clone();
		Rsp=RspOperat.clone();
		if(getValue("KES-OPMAIT", false ,"" ,"", true)=="N")
			$("DICT�val-nego_"+kk).style.display="none"

		Rsp = RspTmp;
	}

	zonscreenget("�val-nego_4��2�val-etat_4��2�libcar_4��2�val-etat-ctr_4��2�")  //invisible
	zonscreenget("�codmvt��11�typaux��8�codauxges��8�cellule��1")
	// if(parseInt($("DICT�cellule").parentElement.style.top) < parseInt($("DICT�e-cellule").parentElement.style.top))
		// zonscreenget("�cellule�e-cellule�4");


	isSaisieEnCours = true;
	topDebut=false;
	isCreation=true;
	$("DICT�campagne").value=camp;
	$("DICT�cer-datsai").value=datsai;
	// $("datbon").value = datsai;
	$("DICT�magasi").value=magasi;
	$("DICT�magasi_libel").value=magasilibel;
	$("DICT�typaux").value="<?=$_typaux?>";
	$("DICT�insect").value="NP";
	$("DICT�nettoyage").value="N";
	$("DICT�contenant").value="O";
	$("DICT�vide-cellule").value="";
	$("DICT�top-complt").value="";
	$("DICT�top-reprise").value="N";
	$("DICT�top-mouture").value="N";
	$("DICT�poste-cptv").value="1";
	$("DICT�poste-cpta").value="1";
	$("DICT�typaux").setAttribute("obli", "");
	$("DICT�codauxges").setAttribute("obli", "");
	$("DICT�cer-contrat").setAttribute("obli", "");
	$("DICT�cellule").setAttribute("obli", "");
	$("DICT�typtrp-ctr").setAttribute("obli", "");
	$("DICT�transp").setAttribute("obli", "");
	$("DICT�chauff").setAttribute("obli", "");
	$("DICT�immatr").setAttribute("obli", "");
	zonscreenget("�e-cellule��2");
	IsCtrlCaract2 = false

	initbufferscreen();

	//on gere le code mouvement
	getValue("CODMVT-DEF", false ,"" ,"DICT�codmvt" ,true)
	if("<?=$_codmvt?>"!="")
		$("DICT�codmvt").value="<?=$_codmvt?>"
	$("DICT�codmvt").defaultValue = "";


	if($("btn_ouv_grid").className == "navImg tailleImg15 upImg15")
		ouvGrid();
	if($("divOnglets").style.display == "none")
	{
		$("divOnglets").show();
	}

	//on rend les zones actives
	//on bloque de le bandeau
	zonscreenget("�magasi��9�cer-datsai��9�campagne��9�attente��9");
	//si on doit saisir le pdsnormes
	zonscreenget("�pdsnormes��9");
	if("<?=$_pdsnormes?>"!="" && "<?=$_pdsnormes?>"!="FORCE"){
		zonscreenget("�pdsnormes��1�pdsnormes��3");
	}
	topForce=false;
	if("<?=$_pdsnormes?>"=="FORCE"){
		$('forcepdsnormes').style.display="block";
	}else{
		$('forcepdsnormes').style.display="none";
	}
	zonscreenget("�cer-contrat��8�typtrp-ctr��8�cer-libel��8�pdsbrut��11�tare��8�solde-vide��1");
	<?if($_saisieopecre!=''){?>
		zonscreenget("�opecre2��3");
        <?if($_saisieopecre=='F'){?>
        zonscreenget("�opecre2��1");
       <?}?>
	<?}?>
	zonscreenget("<?=$SCREEN?>");
	if("<?=$_ticket?>"!="")
	{
		$("TICKET").show();
		setTimeout('$("DICT�ticket").focus();');
	}
	else
	{
		<?if($_saisieordre!=''){?>
			setTimeout('$("DICT�no-ordre").focus();');
		<?} else {?>
			setTimeout('setFocus(TabFocusEntree, "DICT�no-ordre");');
		<?}?>
	}


    loadOnglet(1) ;
	
	rowidCours = "";
	
	return true;
}

function bonimprimer()
{
	inputParameter = "CODSOC�<?=$c_codsoc?>�TYPCTR�"+typctr.trim()+"�CER-CHRONO�"+cer_chrono.trim()+"�MAGASI�"+$("DICT�magasi").value.trim()+"�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�CER-DATSAI�"+$("DICT�cer-datsai").value.trim();
	retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , "IMP-CERMVT�a-majcermvt" , "" , "");
	//Tester le retour
	if (retourExchange != "OK")
	{
		//on part en creation de contrat
		alert("<?PHP print lect_bin::getLabel ( "mess-tabgen2_58" , "sl"); ?>");
		return ;
	}
	//On ouvre le spool
	ExpRsp = Rsp[1].split("=");
	NomSpool = ExpRsp[1];
	httpFile = "<?=$xcspoolRelPath.$ope?>/" + NomSpool;
	$("link-visu").href = httpFile;
	$("link-visu").click();
	//Raz de la variable
	Rsp = [];
}

var origine = "";
topattente = false;

function bonvalider(top)
{
	trace("--bonValider--");
	
	if(visu)
		return false
		
	//on a pas rempli le champ code depot
	if (isSaisieEnCours == false)
	{
		alert("Aucune saisie en cours");
		return false;
	}
	
	//337 - Controle de la saisie des caracteristiques.
	ctrlSaisieTousCaract()
		
	//337 - Test:
	//Si codcer renseign� dans lot : TEST si codcer mvt == codcer lot
	//Sinon TEST si famille codcer mvt == famille lot
	//test celulle mvt == cellume lot
	var codcerMvt 		= $('DICT�codcer').value;
	var famcerMvt 		= getField(RspCodcer[1],'FAMCER');
	var codcerRegMvt 	= getField(RspCodcer[1],'CODCER-REG');
	var celMvt	  		= $('DICT�cellule').value;
	var codcerLot 		= getField(RspLots,'CODCER');
	var famcerLot 		= getField(RspLots,'FAMCER');
	var celLot    		= getField(RspLots,'CELLULE');
	
	if(codcerLot != "" && (codcerLot != codcerMvt && codcerLot != codcerRegMvt))
	{
		alert("C�r�ale du mouvement diff�rente de la c�r�ale du lot.")
		return false;
	}
	else if(codcerLot == "" && famcerLot != "" && famcerLot != famcerMvt)
	{
		alert("Famille du mouvement diff�rente de la famille du lot.")
		return false;
	}
	else if(celLot != "" && celLot != celMvt)
	{
		alert("Cellule du mouvement diff�rente de la cellule du lot.")
		return false;
	}
	
	validok = false
	<?if(strtoupper(trim($_saisieopecre)) == 'O'){?>
		if($("DICT�opecre2").value.trim() == "")
		{
			alert("Saisir l'op�rateur de saisie!");
			$("DICT�opecre2").focus();
			return false;
		}
	<?}?>

    // On verifie les zones obligatoires
    if (!topattente)
    {
        if (!ctrlobli())
        {
            alert("Veuillez saisir les zones obligatoires");
            return false;
        }
    }
    
	if (origine == "test") return false;
	
	//on verifie que des transporteurs interdits ne sont pas saisis
	if("<?=$_trpdep?>"!="")
		if("<?=$_trpdep?>"==$("DICT�transp").value)
		{
			alert("Transporteur non autoris�");
			$("DICT�transp").focus();
			return false;
		}

	if("<?=$_trprend?>"!="")
		if("<?=$_trprend?>"==$("DICT�transp").value)
		{
			alert("Transporteur non autoris�");
			$("DICT�transp").focus();
			return false;
		}
		
	Rsp = RspCellule;
    
    if ("<?=$_rotation?>".toUpperCase() == "O" && !topattente)
    {
        if ($("DICT�rotation").value == "" && getValue("LIBEL1-2", false, "", "", true).toUpperCase() != "V")
		{
			alert("Il faut une rotation active !");
			$("DICT�cellule").focus();
			$("DICT�cellule").select();
			return false;
		}
    }
    
    if($("DICT�tare").value.trim()=="")
	{
		$("DICT�tare").value = "0.00";
	}
			
		
	if(!topattente && "<?=$_pdsnormes?>".trim() == "")
		if(!ctrlpoids(true))
			return false;
	
	//ajout 337
	ctrlpdsnormes();

	//on regarde si l'odre d'expedition est obligatoire on test le topelement_16
	Rsp=RspCoderMvt;
	if(getValue("TOPELEMENT_16",false,"","",true)=="1")
		if($("DICT�numoex").value.trim()=="")
		{
			alert("Veuillez saisir l'ordre d'exp�dition");
			$("DICT�numoex").focus();
			return false;
		}
		
	if($("DICT�numoex").value.trim()!="" && $("DICT�indice-liv").value.trim()=="")
	{
		alert("Veuillez selectionner au moins une ligne de l'ordre d'exp�dition")
		return false;
	}
	//si l'oe par le param est facultatif on regarde si l'odre d'expedition est rendu et si ds le menu on dit que rendu c'est obli on affiche le message
	if(getValue ( "TOPELEMENT_16" ,false ,"" ,""  , true)=="2")
	{
		Rsp=RspContrat;
		if(getValue("TYPTRP-CTR",false,"","",true)!=""&&"<?=$_rendu?>"=="Oblig")
			if($("DICT�numoex").value.trim()=="")
			{
				alert("Veuillez saisir l'ordre d'exp�dition");
				$("DICT�numoex").focus();
				return false;
			}

	}
	 //si OE existant on verifie la coherence avec le tiers et la cereale
	 if($("DICT�numoex").value.trim()!="")
	 {
		if(RspVoe==undefined)
		{
			//on se refait un acces
			//Construction du str_val
			inputParameter = "CODSOC�<?=$c_codsoc?>�TYPOEX�VOE�TYPCDE�VCD�TYPCTR�V";
			inputParameter +="�NUMOEX�"+$("DICT�numoex").value.trim();
			//acces execution
			retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "get-ceroex�a-cerctr", "", "");
			RspVoe=Rsp.clone();
		}
		
		Rsp=RspVoe;
		//on verifie la coherence du contrat
		if(getValue("CODCER",false,"","", true).trim().toUpperCase()!=$("DICT�codcer").value.trim().toUpperCase())
		{
            alert("Attention le code c�r�ale du contrat ne correspond pas");
			$("DICT�codcer").focus();
			return false;
		}
		
		if(getValue("CODAUXGES",false,"","", true).trim().toUpperCase()!=$("DICT�codauxges").value.trim().toUpperCase())
		{
			alert("Attention le code tiers du contrat ne correspond pas");
			$("DICT�codauxges").focus();
			return false;
		}
		
		if(getValue("CER-CONTRAT",false,"","", true).trim().toUpperCase()!=$("DICT�cer-contrat").value.trim().toUpperCase())
		{
			alert("Attention le code tiers du contrat ne correspond pas");
			$("DICT�cer-contrat").focus();
			return false;
		}
	 }
	
	//on verifie si on a bien valid� sur la zone tiers
	if($("DICT�codauxges").defaultValue!=$("DICT�codauxges").value)
		if("<?=$ident?>"=="677")
		{
			alert("Veuillez valider la zone tiers");
			return false;
		}
		
	//test saisie FSO sur un tiers qui n'a pas de FAP
	if(ctrlRepriseFin()==false)
		return false;
	
	//si operatyeur depot oe obligatoire
	Rsp=RspCodmvt.concat();
	//si selon le code mouvement les caracteristiques sont obligatoires
	if(getValue ( "CTRL-CARAC" ,false ,"" ,""  , true)=="3")
		for(kk=1;kk<=14;kk++)
		{
			Rsp=RspCodmvt.concat();
			if($("DICT�libcar_"+kk).value!=""&&kk!=4)
			{
				if($("DICT�val-etat_"+kk).value.trim()==""&&$("DICT�val-etat_"+kk).readOnly!=true)
				{
					alert("les valeurs des caract�ristiques sont obligatoires");
					$("DICT�val-etat_1").focus();
					return false;
				}
				
				if(parseFloatStr($("DICT�val-etat_"+kk).value)==0&&$("DICT�val-etat_"+kk).readOnly!=true)
				{
					//Recuperation des valeurs mini et maxi des caracteristiques param�tr�es
					Rsp=RspCodcer.clone(); //reponse du get codcer
					valmini=getValue("MINCAR_"+kk, false ,"" ,"", true);
					
					if(valmini=="")
						valmini=0;
					
					if(valmini!=0)
						valmini=parseFloatStr(valmini);
					
					if(valmini!=0)
					{
						alert("les valeurs des caract�ristiques sont obligatoires");
						return false;
					}
				}
			}
		}
		
	//cas des cellules virtuelles
	Rsp=RspCellule;
	if(RspCellule != undefined)
		if(getValue("TYPCEL", false, "" , "", true)=="V" && isCreation)
			if(!ctrlMulti("skipalert"))//-> afficher la multicellule
			{
				ctrlcellulemulti();
				return false;
			}

	// cas des transilage
	if($("DICT�e-cellule").value.trim() != "" && $("DICT�magas2").value.trim()=="")
	    $("DICT�magas2").value = $("DICT�magasi").value;

	//337 - (WF:131575) - [ECLATEMENT SPE] 
	//R�cup�ration qte contrat
	if(RspContrat.size() == 0)
	{
		var req = "CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value+"�TYPCTR�A�CLOTURE�0"+"�CER-CONTRAT�"+$('DICT�cer-contrat').value+"�PARAM�N�FILIERE�N�PROG�ASBONAPP";
		
		if(getValue ( "TYPTRANS"  , false , "" , ""  , true)=="1")
			req = "CODSOC�<?=$c_codsoc?>�TYPCTR�V"+"�CER-CONTRAT�"+$(strId).value;
		
		retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , req , "get-cerctr�a-cerctr" , "" , "" );
		RspContrat = Rsp.clone();
	}
	
	var qteMvt = parseFloatStr($('DICT�pdsnormes').value);
	var qteContrat;
	if(RspContrat[0] != 'OK')
		qteContrat = -1;
	else
		qteContrat = parseFloatStr(getField(RspContrat[1],'TON-SOLDE'));
	
	if(modifBon && carOrgeSpe && saisieCalibrage())
	{
		var oldPdsNormeCalibre = parseFloatStr(calcOrgeCalibre(oldPdsNorme));
		var qteMvtCalibre = parseFloatStr(calcOrgeCalibre(qteMvt));
		
		ecartArrondis = 0;
		
		// deltaQteModifBon = (((qteMvtCalibre*100000)-(oldPdsNormeCalibre*100000))/100000).toFixed(3);
		deltaQteModifBon = (((qteMvt*100000)-(oldPdsNorme*100000))/100000).toFixed(3);
		
		trace(" TEST POUR ECLATEMENT");
		trace(" deltaQteModifBon : " + deltaQteModifBon);
	}
	else if(modifBon)
		deltaQteModifBon = (((qteMvt*100000)-(oldPdsNorme*100000))/100000).toFixed(3);
	
	//Si calibre => prend pds calibre
	if(carOrgeSpe && saisieCalibrage())
	{
		ecartArrondis = 0;
		if(!modifBon)
			qteMvt = calcOrgeCalibre();
		//SINON si on modifie un mouvement qui �tait sans contrat et qu'on lui rentre un contrat
		//ALORS on calcule le calibrage du pds normes.
		else if(modifBon && oldContratCer == "" && $('DICT�cer-contrat').value != "")
			qteMvt = calcOrgeCalibre();
		else
			//SINON on calcule le calibrage de la diff�rence entre le pds normes AVANT MODIF et le pds normes APRES MODIF
			qteMvt = calcOrgeCalibre('RESTE');
	}
	
		
	//    (qteContrat = -1 ==> pas de contrat donc pas d'�clatement)
	//	  Si qte du mouvement > qte du contrat prix => Ouverture �cran �clatement du bon.
	// OU Si on est en modification d'un bon ET que le pdsNormes de d�part soit > au pdsNormes d'arriv�e (cad apr�s modif)
	// OU si on a saisi un calibrage (si carOrgeSpe = true).
	if(eclatementSPE  && (!modifBon && (qteContrat != -1 && (!saisieCalibrage() && qteMvt > qteContrat) || (carOrgeSpe && saisieCalibrage() && calcOrgeCalibre() > qteContrat))
					                || ( modifBon && (qteContrat != -1 && deltaQteModifBon > 0 && deltaQteModifBon - qteContrat > 0) 
												  || (qteContrat != -1 && deltaQteModifBon > qteContrat) /*(qteContrat != -1 && qteMvt > qteContrat) */
												  || (deltaQteModifBon > 0 && qteContrat == -1))
	))
	{
		trace(" ECLATEMENT - 1")
				
		majQteRestanteEclatement();
		
		resteSaisir = parseFloatStr($('DICT�pdsresteini').value);
		
		//OUVERTURE ECRAN ECLATEMENT
		if(($('DICT�cer-contrat').value != "" && qteMvt > qteContrat && (gridEclat.getRowCount() == 0 || gridEclat.getRowCount() > 0 && resteSaisir > 0)))
		{
			trace(" ECLATEMENT - 2")
			ecartArrondis = 0;
			boneclatement();
			return;
		}

		if(gridEclat.getRowCount() > 0)
			calculPdsBrutEclatementSPE();
	}
	//337 - (WF:131575) - [ECLATEMENT SPE] 
		
	//dans le cas des transfert entree pour les depot informatise on s appuye obli sur un transfert sortie
	Rsp=RspMagas2
	//Twist anti plantage du getValue 
	var topMagas2 = false;
	if(RspMagas2 != undefined)
	{
		if(getValeurRsp('TOPASS') == "0")
		{
			topMagas2 = true;
		}
	}
	else
		topMagas2 = true;
	


	if(topMagas2)
	{
		Rsp=RspCodmvt.concat();
				
		if(getValeurRsp('TYPTRANS') == "3"
		&& getValeurRsp('TYPMVT') == "1"
		&& cer_bontrs_1.trim()=="")
		{
			alert ("Rapprochement d'un tranfert sortie obligatoire");
			// $("DICT�cer-bontrs-1").focus();
			return false;
		}
		
		if(getField(RspCodmvt[1], "TYPTRANS") == "3" && getField(RspCodmvt[1], "TYPMVT") == "2")
		{
			//CONTROLE DES STOCKS
			var inputParameterStockSoc = "CODSOC��CODETB��TYPTAB�PRM�PREFIX�STOC-SOCK�CODTAB�ECART";
			var dogFonctionStockSoc = "get-full-tabcer�a-tabcer";
			var retourExchangeStockSoc = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameterStockSoc , dogFonctionStockSoc , "" , "" );

			if(retourExchangeStockSoc == "OK")
			{
				var qte_mvt = parseFloatStr($("DICT�pdsnormes").value.trim());
					  
				var qte_soc = 0.0;
				var ecart_permis = getValeurRsp("NOMBRE_1");
				var ecart_max = getValeurRsp("NOMBRE_2");
				
				inputParameterStockSoc = "CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CODCER�" + getField(RspCodcer[1], "CODCER-REG");
				inputParameterStockSoc += "�CAMPAGNE�" + $("DICT�campagne").value;
				
				dogFonctionStockSoc = "get-stock-societe�a-stocer";
				
				retourExchangeStockSoc = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameterStockSoc , dogFonctionStockSoc , "" , "" );
				if(retourExchangeStockSoc == "OK")
				{
					qte_soc = parseFloatStr(getValeurRsp("STOCK-SOC"));
					if(isNaN(qte_soc))
						qte_soc = 0.0;
				}
				
				if(qte_mvt - qte_soc > ecart_permis)
				{
					if(qte_mvt - qte_soc <= ecart_max)
					{
						var temp_qte = qte_mvt - qte_soc;
						if(!confirm("�cart de poids de " + temp_qte.toFixed(2) + " T, voulez-vous confirmer ?"))
							return false;
					}
					else
					{
						alert("Attention, il y a un �cart trop important entre le stock soci�t� de la c�r�ale " + $("DICT�codcer_libel").value.trim() + " et la quantit� de ce bon !");
						return false;
					}
				}
			}
		}
	
			
		if(getField(RspCodmvt[1], "TYPTRANS") == "3" && getField(RspCodmvt[1], "TYPMVT") == "1")
		{
			//CONTROLE DES ECARTS DE POIDS TRANSFERT
			var inputParameterTransfert = "CODSOC��CODETB��TYPTAB�PRM�PREFIX�TRANSFERT�CODTAB�ECART";
			
			var dogFonctionTransfert = "get-full-tabcer�a-tabcer";
			var retourExchangeTransfert = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameterTransfert , dogFonctionTransfert , "" , "" );

			if(retourExchangeTransfert == "OK")
			{
				var ecart = parseFloatStr(getValeurRsp("NOMBRE_1"));
				var poidsBonEnCours = Math.abs(parseFloatStr($("DICT�pdsnormes").value.trim()));
				var poidsReciproque = Math.abs(parseFloatStr(getField(RspCermvtReciproque[1], "PDSNORMES")));
				
				var resultat = Math.abs(poidsBonEnCours - poidsReciproque);
				
				if(resultat > ecart)
				{
					if(!confirm("�cart de poids de " + resultat.toFixed(2) + " T, voulez-vous confirmer ?"))
						return false;
				}
			}
		}
	}
	
	//dans le cas des modifications on a pas le droit de modifier certains champ
	if(rowidCours.trim()!="")
		if(RspMvt!=undefined)
		{
			Rsp=RspMvt;
			topER=false
			if(getValue("TYPAUX",false,"","",true).toUpperCase() != "***"
			&& getValue("TYPAUX",false,"","",true).toUpperCase() != $("DICT�typaux").value.trim().toUpperCase())
				topER=true;
			if(getValue("TYPAUX",false,"","",true).toUpperCase() != "***"
			&& getValue("CODAUXGES",false,"","",true).toUpperCase().trim() != $("DICT�codauxges").value.trim().toUpperCase().trim())
				topER=true;
			if(getValue("CODMVT",false,"","",true).toUpperCase()!=$("DICT�codmvt").value.trim().toUpperCase())
				topER=true;
			if(topER==true&&"<?=$_modmvt?>"=="")
			{
				alert("le code mouvement et le tiers ne peuvent �tre modifi�");
				return false;
			}
		}

	inputParameter = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�CODCER�"+$("DICT�codcer").value.trim();
	
	if(parseInt(gridEclat.getRowCount()) == 0)
		inputParameter += "�ROWID�" + rowidCours;
	
	inputParameter += "�TYPCTR�" + typctr;
	inputParameter += "�ADRESSE_1�" + $("DICT�codauxges_libel").value;
	inputParameter += "�MAGASI�" + $("DICT�magasi").value;
	inputParameter += "�CAMPAGNE�" + $("DICT�campagne").value;
	inputParameter += "�CER-DATSAI�" + $("DICT�cer-datsai").value;
	
	if(!validInput($("DICT�datbon"), "", false))
		return false;
	
	if(!validInput($("DICT�heubon"), "", false))
		return false;
	
	if($("DICT�datbon").value.trim() != "")
	{
		inputParameter += "�DATEORI�" + $("DICT�datbon").value;
		inputParameter += "�TIMEORI�" + $("DICT�heubon").value;
	}

	
	
	arr_val = [];
	if(parseInt(gridEclat.getRowCount()) != 0)
	{	
		trace("	parseInt(gridEclat.getRowCount()) != 0")
		
		//SI eclatementSPE => le bon origine envoy� avec mouvement plac� dans la param�trage.
		if(!eclatementSPE || (eclatementSPE && modifBon))
		{
			arr_val[0]  = "�CODMVT�" + $("DICT�codmvt").value;
			
			if(modifBon)
			{
				arr_val[0]  += "�CER-CONTRAT�" + $('DICT�cer-contrat').value;
				arr_val[0]  += "�ROWID�" + rowidCours;
				arr_val[0]  += "�CLE-ORIG�" + cerChrono1erMvt;
			}
		}
		else
		{
			arr_val[0]  = "�CODMVT�" + bonOri;
			arr_val[0]  += "�CER-CONTRAT�";
		}
		
		arr_val[0] += "�TYPAUX�" + $("DICT�typaux").value;
		arr_val[0] += "�CODAUXGES�" + $("DICT�codauxges").value;
		arr_val[0] += "�CODCER�" + $("DICT�codcer").value;
		
		//SI eclatementSPE ET modification d'un mouvement dont le contrat n'EST PAS clotur�
		if(eclatementSPE && ( modifBon && qteContrat > 0 && ((deltaQteModifBon > 0 && deltaQteModifBon - qteContrat > 0) || qteMvt > qteContrat)))
		{
			trace("		eclatementSPE && ( modifBon && qteContrat > 0 && (deltaQteModifBon > 0 && deltaQteModifBon - qteContrat > 0))")
		    var pdsMvt = calculPdsBrutEclatementSPE(parseFloatStr(getField(RspContrat[1],'TON-GLOBAL'))).toFixed(3);

			if(carOrgeSpe && saisieCalibrage())
			{
				arr_val[0] += "�PDSBRUT�" + calcOrgeCalibre(pdsMvt);
				arr_val[0] += "�PDSNET�" + calcOrgeCalibre(pdsMvt);
			}
			else
			{
				arr_val[0] += "�PDSBRUT�" + pdsMvt;
				arr_val[0] += "�PDSNET�" + pdsMvt;				
			}
		}
		//SI eclatementSPE ET modification d'un mouvement dont le contrat EST clotur�
		else if(eclatementSPE && ( modifBon && qteContrat == -1))
		{
			trace("		eclatementSPE && ( modifBon && qteContrat == -1)")
			var oldpdsMvt = calculPdsBrutEclatementSPE(parseFloatStr(oldPdsNorme)).toFixed(3);
			
            arr_val[0] += "�PDSBRUT�" + oldpdsMvt;
			arr_val[0] += "�PDSNET�" + oldpdsMvt;
		}
		//SI eclatementSPE ET on a le param�trage sur l'orge ET on a saisi un calibrage
		/* else if(eclatementSPE && carOrgeSpe && saisieCalibrage())
		{
			
		} */
		else
		{
			trace("		ELSE")
			arr_val[0] += "�PDSBRUT�" + $("DICT�pdsbrut").value;
			arr_val[0] += "�PDSNET�" + $("DICT�pdsnet").value;			
		}
		
		
		
		arr_val[0] += "�TARE�" + $("DICT�tare").value;
		arr_val[0] += "�CER-CHRONO�" + cer_chrono;
			
		// Pte Gestion des lots apports
		arr_val[0] += "�CODLOT�" + $("DICT�codlot").value + $("DICT�numlot").value;
		arr_val[0] += "�PDS-NORMES�" + $("DICT�pdsnormes").value;
		if(topForce||topForceValue=="D")arr_val[0] += "�TOP-PDSNORMES�"+topForceValue //forc�
		
		
		for(var i=0; i<parseInt(gridEclat.getRowCount()); i++)
		{
			arr_val[i+1] = "�CODMVT�" + gridEclat.getCellText(getColEclat("codmvtecl"), i);
			arr_val[i+1] += "�TYPAUX�" + gridEclat.getCellText(getColEclat("typauxecl"), i);
			arr_val[i+1] += "�CODAUXGES�" + gridEclat.getCellText(getColEclat("codauxgesecl"), i);
			arr_val[i+1] += "�CODCER�" + $("DICT�codcer").value;
			
			arr_val[i+1] += "�CLE-ORIG�" + cerChrono1erMvt;
			
			arr_val[i+1] +=  "�ECLATEMENT�O";
			
			//SI eclatementSPE => On ajoute le contrat associ� avec l'option correspondante
			if(eclatementSPE)
			{
				arr_val[i+1] +=  "�CER-CONTRAT�" + gridEclat.getCellText(getColEclat("contrat"), i);
				arr_val[i+1] +=  "�NATCON�" + gridEclat.getCellText(getColEclat("option"), i);				
			}
			
			// trace("typcal == " + gridEclat.getCellValue(getColEclat("typcal"), i).trim())
			
			if(gridEclat.getCellValue(getColEclat("typcal"), i).trim()!="NORMES")
			{
				if(!eclatementSPE)
				{
					arr_val[i+1] += "�PDSBRUT�" + gridEclat.getCellText(getColEclat("pdsNormes"), i);//6
					arr_val[i+1] += "�PDSNET�" + gridEclat.getCellText(getColEclat("pdsNormes"), i);//6					
				}
				else
				{
					
					arr_val[i+1] += "�PDSBRUT�" + listePdsBrutEclat[i];//6
					arr_val[i+1] += "�PDSNET�" + listePdsBrutEclat[i];//6	 
					
					/* arr_val[i+1] += "�PDSBRUT�" + calculPdsBrutEclatementSPE(listePdsBrutEclat[i]);
					arr_val[i+1] += "�PDSNET�" + calculPdsBrutEclatementSPE(listePdsBrutEclat[i]); */
				}
					
				
				if(!eclatementSPE)
					arr_val[i+1] += "�PDS-NORMES�" + dataPdsNormes[i];
				else if(eclatementSPE && carOrgeSpe && saisieCalibrage())
					arr_val[i+1] += "�PDS-NORMES�" + calculPdsNormesEclatementSPE(listePdsBrutEclat[i]);
					// arr_val[i+1] += "�PDS-NORMES�" + listePdsBrutEclat[i];
				else
					arr_val[i+1] += "�PDS-NORMES�" + gridEclat.getCellData(getColEclat('pdsNormes'),i);
			}
			else
			{
				arr_val[i+1] += "�PDSBRUT�" + dataPdsNormes[i]; //7
				arr_val[i+1] += "�PDSNET�" + dataPdsNormes[i]; //7
				
				if(!eclatementSPE)
					arr_val[i+1] += "�PDS-NORMES�" + gridEclat.getCellText(getColEclat("pdsNormes"), i);
				else
					arr_val[i+1] += "�PDS-NORMES�" + dataPdsNormes[i];
			}
			arr_val[i+1] += "�TARE�0";
			if(topForce||topForceValue=="D")arr_val[i+1] += "�TOP-PDSNORMES�"+topForceValue; //forc�
		}
		
		// if(modifBon && deltaQteModifBon > 0)
			// arr_val.splice(0,1);
		
		// trace(listePdsBrutEclat);
		
		
		
		// return;
	}
	else
	{
		
		/*------------------------------------------------------------------*/
		/*337 - A T T E N T I O N   D O G   I D E N T   T E M P O R A I R E */
		/*------------------------------------------------------------------*/
		/* Pte 28/04/2017 Le message "Report du tiers sur le ticket" qui doit etre g�r� pour CAVAC est affich� � tort tout le temps
           Ce bloc de code est donc comment�. Il devra etre corrig� pour une restitution
        if("<?=$ident?>" == "333")
		{
			if(getField(RspOperat[1],'KES-OPMAIT').trim().toUpperCase() != "N" && !isCreation && typauxOrigine != "***")
			{
				//Si    typaux != typauxOrigine 
				//OU SI typaux = typauxOrigine ET codaux != codauxOrigine
				if($("DICT�typaux").value.trim().toUpperCase() != typauxOrigine.trim().toUpperCase() 
					|| ($("DICT�typaux").value.trim().toUpperCase() ==  typauxOrigine.trim().toUpperCase() && $("DICT�codauxges").value.trim().toUpperCase() != codauxOrigine.trim().toUpperCase()))
				{
					if(confirm("Report du tiers sur le ticket ?"))
						inputParameter += "�REPORT-TIERS�O";
				}

			}			
		}
        */
		
		inputParameter += "�CER-CHRONO�" + cer_chrono;
	    inputParameter += "�CODLOT�" + $("DICT�codlot").value + $("DICT�numlot").value;
		inputParameter += "�CODMVT�" + $("DICT�codmvt").value;
		inputParameter += "�TYPAUX�" + $("DICT�typaux").value;
		inputParameter += "�CODAUXGES�" + $("DICT�codauxges").value;
		inputParameter += "�CODCER�" + $("DICT�codcer").value;		
		inputParameter += "�PDSBRUT�" + $("DICT�pdsbrut").value;
		inputParameter += "�PDSNET�" + $("DICT�pdsnet").value;
		inputParameter += "�TARE�" + $("DICT�tare").value;
		
		if(("<?=$_pdsnormes?>".trim() !="" ) || topForceValue=="D"){
			inputParameter += "�PDS-NORMES�" + $("DICT�pdsnormes").value;
			if(topForce||topForceValue=="D")inputParameter += "�TOP-PDSNORMES�"+topForceValue //forc�
		}
	}
	inputParameter += "�COMONIC�" + $("DICT�comonic").value;
	inputParameter += "�CELLULE�" + $("DICT�cellule").value;
	inputParameter += "�FOSSE�" + $("inputF10Fosse").value;
	inputParameter += "�ROTATION�" + $("DICT�rotation").value;
	inputParameter += "�TYPTRP�" + $("DICT�typtrp-ctr").value;
	inputParameter += "�TYPAUX-CHAUF�" + $("DICT�typaux_chauff").value;
	inputParameter += "�CODAUX-CHAUF�" + $("DICT�codaux_chauff").value;
	inputParameter += "�TRANSP�" + $("DICT�transp").value;
	inputParameter += "�CHAUFF�" + $("DICT�chauff").value;
	
	//SI eclatement spe => bon originine envoy� sans contrat
	if(gridEclat.getRowCount() == 0)
		inputParameter += "�CER-CONTRAT�" + $("DICT�cer-contrat").value;
	
	inputParameter += "�CER-CONTRAT-FIL�" + $("DICT�cer-contrat-fil").value;
	inputParameter += "�INDICE-LIV�" + $("DICT�indice-liv").value;
	inputParameter += "�NUMORD-EXEC�" + $("DICT�no-ordre").value;
	inputParameter += "�IMMATR�" + $("DICT�immatr").value;
	
	if($("DICT�ticket").value.trim()!="")
		inputParameter = inputParameter + "NO-TICKET�" + $("DICT�ticket").value;

	inputParameter += "�CER-PRIX�" + $("DICT�cer-prix").value;
	inputParameter += "�MTT-TRANSP�" + $("DICT�pu-transp").value;
	inputParameter += "�MANU-MAJO�" + $("DICT�manu-majo").value;
	inputParameter += "�DATE-MAJO�" + $("DICT�date-majo").value;
	inputParameter += "�DATECH�" + $("DICT�datech").value;
	inputParameter += "�NETTOYAGE�" + $("DICT�nettoyage").value;
	inputParameter += "�INSECT�" + $("DICT�insect").value;
	inputParameter += "�DOSAGE�" + $("DICT�dosage").value;
	inputParameter += "�NUM-PESEE�" + $("DICT�numpesee").value;
	inputParameter += "�COD-ADRLIV�" + $("DICT�cod-adrliv").value;
	inputParameter += "�BL-INTERNE�" + $("DICT�bl-interne").value;
	inputParameter += "�CER-BONTRS-1�" + cer_bontrs_1;
	inputParameter += "�CODCER-ORI�" + $("DICT�codcer-ori").value;
	inputParameter += "�CER-LIBEL�" + $("DICT�cer-libel").value;
	inputParameter += "�MAGAS2�" + $("DICT�magas2").value;
	inputParameter += "�Date_rec�" + $("dateRec").value;
	inputParameter += "�heure_rec�" + $("heureRec").value;
    inputParameter += "�CHRONOPARC_1�" + $("DICT�chronoparc").value;    // Pte DDD-NAT-PARCELLE
	inputParameter += "�PARCELLE_1�" + $("DICT�codeparc").value;
	inputParameter += "�CER-ATTENTE�" + cer_attente;
	inputParameter += "�VIDCEL�" + $("DICT�vide-cellule").value;
	inputParameter += "�ADR-LIV_1�" + $("DICT�adr-liv_1").value;
	inputParameter += "�ADR-LIV_2�" + $("DICT�adr-liv_2").value;
	inputParameter += "�ADR-LIV_3�" + $("DICT�adr-liv_3").value;
	inputParameter += "�ADR-LIV_4�" + $("DICT�adr-liv_4").value;
	inputParameter += "�ADR-LIV_5�" + $("DICT�adr-liv_5").value;
	inputParameter += "�E-CELLULE�" + $("DICT�e-cellule").value;
	inputParameter += "�E-ROTATION�" + $("DICT�e-rotation").value;
	inputParameter += "�LIB-TRANSP�" + $("DICT�transp_libel").value;
	inputParameter += "�CER-VALID�" + $("DICT�cervalid").value;
	inputParameter += "�PROTRS_1�" + $("DICT�protrs_1").value;
	inputParameter += "�PROTRS_2�" + $("DICT�protrs_2").value;
	inputParameter += "�PROTRS_3�" + $("DICT�protrs_3").value;
	inputParameter += "�DATRAIT�" + $("DICT�datrait").value;
	inputParameter += "�TRAITM�" + $("DICT�traitm").value;
	inputParameter += "�TOPMAJPS�" + $("DICT�topmajps").value;
	inputParameter += "�ECHANTILLON�" + $("DICT�echantillon").value;
	inputParameter += "�TOP-COMPL�" + $("DICT�top-complt").value;
	inputParameter += "�VARIETE�" + $("DICT�variete").value;
	inputParameter += "�REF-BL�" + $("DICT�ref-bl").value;
	inputParameter += "�LIB-FIL�" + $("DICT�lib-fil").value;
	inputParameter += "�DEST-FIN�" + $("DICT�dest-fin").value;
	inputParameter += "�MOTIF�" + $("DICT�motif").value;
	inputParameter += "�TYP-MOTIF�" + $("DICT�typ-motif").value;
	inputParameter += "�CER-TOPTVA�" + $("DICT�top-tva").value;
	inputParameter += "�MODREG�" + $("DICT�modreg").value;
	
	if(gridEclat.getRowCount() == 0)
		inputParameter += "�ANC-CONTRAT�" + anccontrat;
	
	inputParameter += "�CONTENANT�" + $("DICT�contenant").value;
	inputParameter += "�INTRACOM�" + $("DICT�intracom").value;
	inputParameter += "�STOCKE�" + $("DICT�stocke").value;
	inputParameter += "�MANIPULE�" + $("DICT�manipule").value;

	inputParameter += "�COD-DEF�" + $("DICT�cod-def").value;
	inputParameter += "�DESC-DEF�" + $("DICT�desc-def").value;
	inputParameter += "�CAUSE-DEF�" + $("DICT�cause-def").value;
	inputParameter += "�TRAIT-DEF�" + $("DICT�trait-def").value;
	inputParameter += "�ACT-DEF�" + $("DICT�act-def").value;
	inputParameter += "�FIC-DEF�" + $("DICT�fic-def").value;
	inputParameter += "�NOFIC-DEF�" + $("DICT�nofic-def").value;
	inputParameter += "�CER-DATMOD�" + $("DICT�datsai").value;
	inputParameter += "�NOM-AUTORI�" + nomAutorise;
	inputParameter += "�DSD-1�" + dsd_1;
	inputParameter += "�DSD-2�" + dsd_2;
	inputParameter += "�HEU-PESEE-1�" + heurePesee_1;
	inputParameter += "�HEU-PESEE-2�" + heurePesee_2;
	inputParameter += "�DAT-PESEE-1�" + datePesee_1;
	inputParameter += "�DAT-PESEE-2�" + datePesee_2;
	
	if(!eclatementSPE || (eclatementSPE && gridEclat.getRowCount() == 0))
		inputParameter += "�NATCON�" + $("DICT�natcon").value;
	
	//N� scell�
	inputParameter += "�NUM-SCELLE-1�" + $("DICT�nscelle1").value.trim();
	inputParameter += "�NUM-SCELLE-2�" + $("DICT�nscelle2").value.trim();
	inputParameter += "�NUM-SCELLE-3�" + $("DICT�nscelle3").value.trim();

	if($("DICT�opecre2").value!="")
		inputParameter += "�OPECRE�" + $("DICT�opecre2").value;

	for(i=1;i<=12;i++)
		if($("DICT�cellule_" + i).value.trim()!="")
		{
			inputParameter += "�LST-CEL_"+i+"�" + $("DICT�cellule_" + i).value;
			inputParameter += "�QTE-CEL_"+i+"�" + $("DICT�pdsnet_" + i).value;
		}
	
	if($("DIVV").style.display=="")
		inputParameter += "�POSTE-CPT�" + $("DICT�poste-cptv").value;
	
	if($("DIVA").style.display=="")
		inputParameter += "�POSTE-CPT�" + $("DICT�poste-cpta").value;
	
	for(kk=1;kk<=14;kk++)
	{
		inputParameter += "�libcar_"+kk + "�" + $("DICT�libcar_"+kk).value
		inputParameter += "�VAL-ETAT-CTR_"+kk + "�" + $("DICT�val-etat-ctr_"+kk).value;
		inputParameter += "�VAL-NEGO_"+kk + "�" + $("DICT�val-nego_"+kk).value;
		inputParameter += "�val-etat_"+kk + "�" + $("DICT�val-etat_"+kk).value;
	}
	
	try{inputParameter = chr10clear(inputParameter)}catch(ex){}
	
	//on lance la mise a jour
	$("MULTI").hide();
	if(arr_val.length!=0)
	{
		inputParameter2 = inputParameter
		str_ligne = 0
		
		trace(arr_val)
		trace(inputParameter)
		
		inputParameter = arr_val[0] + inputParameter
	}
	//On regarde si on est en retour de vente (CRV)
	if(retourRowid.trim() != "")
		inputParameter += "�RETOUR-ROWID�" + retourRowid;
	
	//Cloture contrat
	var reponse = cloturerContrat();
	if(reponse == "oui")
		inputParameter += "�CLOTCTR�O";
	else if(reponse == "")
		return false;
	
	//gestion du quest-report
	if(getField(RspCodmvt[1],"MVT-REPORT")!="" && getField(RspCodmvt[1],"SOC-REPORT")!="" && isCreation ){
		if(confirm("Voulez-vous cr�er le mouvement sur '" + getField(RspCodmvt[1],"SOC-REPORT")+"' ?")){
			inputParameter = setField(inputParameter,"CODMVT",getField(RspCodmvt[1],"MVT-REPORT"))
			inputParameter = setField(inputParameter,"CODSOC",getField(RspCodmvt[1],"SOC-REPORT"))
			inputParameter = setField(inputParameter,"CER-CHRONO","")
		}
	}

	//C O N T R O L E    D U    M O U V E M E N T    P R I N C I P A L
	retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "CTRL-CERMVT�a-ctrlcermvt", "", "");
	
	//On verifie le retour
	if (retourExchange == "OK")
	{
        if(setValueFromKeyword("REPONSE", false, "", "", true).trim().toUpperCase() == "OK")
        {
			if("<?=$ident?>" == "734")
			{
				/* contr�le des caract�risques (non-conformit� bnj) */
				var dogFonctionNC = "ctrl-nc�a-cereal";
				var inputParameterNC  = "ROWID-CEREAL�" + getField(RspCodcer[1], "ROWID");
					inputParameterNC += "�SENS-MVT�" 	+ getField(RspCodmvt[1], "TYPMVT");
					inputParameterNC += "�VALINS�" 		+ $("DICT�insect").value.trim();
					inputParameterNC += "�CODCAR�";
				
				var temp_cod = "";
				var temp_val = "";
				for(var i = 1; i <= 14; i++)
				{
					if(RspCodcer != undefined && RspCodcer.length > 1 && getField(RspCodcer[1], "CODCAR_" + i).trim() != "")
					{
						if(temp_cod.trim() != "")
							temp_cod += "|";
						
						if(temp_val.trim() != "")
							temp_val += "|";
						
						temp_cod += getField(RspCodcer[1], "CODCAR_" + i).trim();
						temp_val += $("DICT�val-etat_" + i).value.trim();
					}
				}
				
				inputParameterNC += temp_cod + "�VALCAR�" + temp_val;
				
				if(parseInt(gridEclat.getRowCount()) == 0)
					inputParameterNC += "�ROWID-CERMVT�" + rowidCours;
				
				var retourExchangeNC = sendExchangeInvoke("<?=$dogExchange?>", inputParameterNC, dogFonctionNC, "", "");

				if(retourExchangeNC == "OK" && getValeurRsp("CODE-NC").trim() != "")
				{
					PhpAuxges = "<?=search::searchExe("as_sainonconf_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_codcar=" + getValeurRsp("CODCAR") + "&_codenc="+ getValeurRsp("CODE-NC") +"&_val="+ getValeurRsp("VAL-NC") ;
					sFeatures = "dialogHeight:520px;dialogWidth:950px;status:no;help:no;scroll:no";
					var retourModale = showModalDialog ( PhpAuxges , "" , sFeatures) ;
					
					if(retourModale == undefined || retourModale == null)
						retourModale = "";
					
					g_codenc = "";
					g_valnc  = "";
					
					if(retourModale.trim() != "")
					{
						var temp = retourModale.split("�");
						g_codenc = temp[0];
						g_valnc  = temp[1];
					}
					else
					{
						alert("Il y pr�sence de non-conformit�s la saisie des actions est obligatoires.")
						return false;
					}
				}
			}
			
			Rsp = [];
			
			trace("	 V A L I D A T I O N   M O U V E M E N T   P R I N C I P A L")
			trace(inputParameter)
			trace(arr_val)
			// return;
			
			//V A L I D A T I O N   M O U V E M E N T   P R I N C I P A L
			retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "MAJ-CERMVT�a-majcermvt", "", "");
			
			if(retourExchange != 'OK')
			{
				alert(Rsp[1]);
				return;
			}
						
			var backRsp = Rsp.clone();
			
			origine = "";
    		topRep = true;
    		topNouveau = false;
    		validok = true;
    		isSaisieEnCours = false;
    		_SaveExchange = "toto<?=session_id()?>";
			
			if("<?=$ident?>" == "734")
			{
				//non confirmit�s
				//on fait tj la maj pour effacer si il n'y en a plus;
				dogFonctionNC = "maj-nc-bon�a-cereal";
				var codenc_tab	= [];
				var val_tab = [];
				
				if(g_codenc.trim() != "")
					codenc_tab = g_codenc.split("|");
				if(g_valnc.trim() != "")
					val_tab = g_valnc.split("|");
				
				var t_val = [];
				inputParameterNC = "";
				var vecteur = "";
				
				for(var i = 0; i < codenc_tab.length;i++)
				{
					if(vecteur.trim() != "")
						vecteur += "|";
					
					vecteur += codenc_tab[i];
					t_val = val_tab[i].split(",");
					vecteur += "," + t_val[0] + "," + t_val[1] + "," + t_val[2];
				}
				
				inputParameterNC  = "�CER-CHRONO�" 	+ cer_chrono ;
				inputParameterNC += "�CAMPAGNE�"  	+ $("DICT�campagne").value.trim() ;
				inputParameterNC += "�MAGASI�"    	+ $("DICT�magasi").value.trim() ;
				inputParameterNC += "�DATSAI�"    	+ $("DICT�cer-datsai").value.trim() ;
				inputParameterNC += "�CODSOC�<?=$c_codsoc?>";
				inputParameterNC += "�VECT-NC�" 	+ vecteur;
				var retourExchangeNC = sendExchangeInvoke("<?=$dogExchange?>", inputParameterNC, dogFonctionNC, "", "");
				
				if(retourExchangeNC != "OK")
				{
					alert(Rsp[1]);
					Rsp = backRsp.clone();
					return false;
				}
				
				Rsp = backRsp.clone();
			}
        }
        else
        {
            alert(setValueFromKeyword("REPONSE", false, "", "", true))
			return
		}
	}
	else
	{
        alert("L'enregistrement n'a pas �t� effectu� !")
		return false;
	}
	
	//On verifie le retour
	if (retourExchange != "OK")
	{
		// L'enregistrement de la ligne a �chou�.
		alert("<?PHP print lect_bin::getLabel ( "mess-prognat_54" , "sl"); ?>");
		isSaisieEnCours = true;
		validok = false;
		return false;
	}
	else
	{
		if(gridEclat.getRowCount() == 0)
			ouvertureSpool();
				
		
		var debut = (modifBon)? 1 : 0;
		
		//Cr�ation des cermvt de l'�clatement
		for(var i = debut; i < gridEclat.getRowCount(); i++)
		{
		
			inputParameter = arr_val[i + 1] + inputParameter2;
			requete = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim() + "�CER-DATSAI�"+$("DICT�cer-datsai").value.trim() + "�MAGASI�"+$("DICT�magasi").value.trim();
			retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", requete, "INC-CERMVT�a-cermvt", "", "") ;
			
			requete += "CER-CHRONO" + getValue("CER-CHRONO", false ,"" ,"" ,true);
			
			_SaveExchange = "toto<?=session_id()?>";
			retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>", inputParameter, "MAJ-CERMVT�a-majcermvt", "", "");
			
			if(!eclatementSPE)
				ouvertureSpool();
			else if(i == gridEclat.getRowCount()-1)
				ouvertureSpoolEclatement();
			
			// (eclatementSPE) ? ouvertureSpoolEclatement() : ouvertureSpool();
		}
		
		//on continu avec les vides cellules
		bonValiderVideCellule(); 
	}
	
	//Raz de la variable
	topattente  = false;
    cer_attente = "";
	Rsp         = [];
	topRep      = false;
    
	razEcran(true);
	
	/*On raz les variables de l'ordre de saisie spe (si fichier existant)*/
	<?if(file_exists(search::searchExe("as_ordre_saisie.js",0)))
	  {
	?>
		try
		{
			razVariablesSpe();
		} catch(ex){trace("---ordre saisie : " + ex)};
	<?}?>
}

/**
	Calcule le poid aux normes � partir du poids net.
	si qte renseign� => on calcule � partir de qte
	sinon calcule de tous les pdsNormes pour l'�clatement SPE
**/
function calculPdsBrutEclatementSPE(qte)
{
	trace("--calculPdsBrutEclatementSPE - qte : "+qte+" --");
	
	var dogFct = "pdsnorm�a-pdsnorm";
	var requeteBase  = "CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$('DICT�campagne').value;
		requeteBase += "�CODCER�"+$('DICT�codcer').value+"�CODMVT�"+$('DICT�codmvt').value;
		requeteBase += "�TYPCAL�BRUT�REGION�001";
		
	for(var i=1; i<=14; i++)
		requeteBase += "�val-etat_"+i + "�" + $("DICT�val-etat_"+i).value;				
	
	var requete = "";
	
    ecartArrondis = 0;

    if(qte == undefined || qte == "")
    	for(var i=0; i<dataGridEclat.size(); i++)
		{
			// Pte 24/01 pour la correction sur les arrondis
            if (i == dataGridEclat.size() - 1)
                dernierMvt = true;
            else
                dernierMvt = false;

            requete = requeteBase+"�PDSNET�"+dataGridEclat[i][getColEclat("pdsNormes")];
			
			retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>",requete, dogFct, "", "");
			
			//Si on a saisie un param�trage => on rajoute le calibrage � la qte du mvt (carOrgeSpe = TRUE)
			if(carOrgeSpe && saisieCalibrage())
				listePdsBrutEclat.push(calcOrgeCalibre(getValeurRsp('PDSNORMES')));
			else
				listePdsBrutEclat.push(getValeurRsp('PDSNORMES'));
		} 
	else
	{
		requete = requeteBase+"�PDSNET�"+qte;
		retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>",requete, dogFct, "", "");
		
		return parseFloatStr(getValeurRsp('PDSNORMES'));
	}
	
}

/**
	M�thode qui renvoie le poids aux normes de la qte plac� en param�tre.
	qte => quantit� � calculer.
**/
function calculPdsNormesEclatementSPE(qte)
{
	trace("--calculPdsNormesEclatementSPE - qte : "+qte+" --");
	
	var dogFct = "pdsnorm�a-pdsnorm";
	var requeteBase  = "CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$('DICT�campagne').value;
		requeteBase += "�CODCER�"+$('DICT�codcer').value+"�CODMVT�"+$('DICT�codmvt').value;
		requeteBase += "�REGION�001�PDSNET�" + qte;
		
	for(var i=1; i<=14; i++)
		requeteBase += "�val-etat_"+i + "�" + $("DICT�val-etat_"+i).value;
	
	var retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>",requeteBase, dogFct, "", "");
	
	return parseFloatStr(getValeurRsp('PDSNORMES'));
}

function cloturerContrat()
{
	//Mot clef menu inexistant -> rien � Faire
	if(!clotureContrat)
		return "erreur";
	
	//Si mot cl� menu on va regarder si le code mouvement est de nature "A", qu'un contrat est renseign� et si le contrat est non cl�tur�.
	//Test nature
	if(getField(RspCodmvt[1], "TYPCTR").trim().toUpperCase() != "A")
		return "erreur";
	//test contrat
	if($("DICT�cer-contrat").value.trim() == "")
		return "erreur";
	//Test contrat non cl�tur�
	if(getField(RspContrat[1], "CLOTURE").trim().toUpperCase() != "")
		return "erreur";
	
	var PhpAuxges = "<?=search::searchExe("ao_clotctr_confirm_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>";
	var	sFeatures = "dialogHeight:80px;dialogWidth:500px;status:no;help:no;scroll:no";
	var retourModale = showModalDialog ( PhpAuxges , "" , sFeatures) ;
	return retourModale.trim().toLowerCase()
}

function bonattente()
{
	if(visu || !isSaisieEnCours)
		return false;
	
	//mise en attente du bon
	cer_attente = "A";
	
	if($("DICT�tare").value.trim() == "")
		$("DICT�tare").value="0";
	
	if(Math.abs(parseFloatStr($("DICT�pdsnet").value))==0){
		alert("Acquisition d'un poids manquante. Mise en attente impossible.")
		return;
	}
	
	//test si on est en bon d'entr�e ou de sortie
	Rsp = RspCodmvt.clone()
	if(getValeurRsp("TYPMVT").trim() == "1")//Bon en entr�e
	{
		dogFonction = "f10-fosse�a-cellule";
		inputParameter = "CODSOC�<?=$c_codsoc?>�MAGASI�"+$("DICT�magasi").value.trim() + "�FOSSE�";
		var filtre = "FIELD=FOSSE|LIBFOSSE";
		retourExchange  = sendExchangeF10 ( "<?=$dogExchangeF10?>" , inputParameter , dogFonction , "inputF10Fosse" , callBackFosse , filtre, 630);
	}
	else//Bon en sortie
	{
		topattente = true;
		bonvalider();
		if(validok)
			alert("Bon mis en attente !",2000,"TOP","green");
			// alert("Bon mis en attente !");
	}
}

function callBackFosse()
{
	setValueFromKeyword("FOSSE", false, "", "inputF10Fosse", true);
	topattente = true;
	bonvalider();
	if(validok)
		alert("Bon mis en attente !",2000,"TOP","green");
		// alert("Bon mis en attente !");
}

function callBackEch(idZone)
{
	switch(idZone.trim())
	{
		case "inputF10Fosse" :
			// if(Rsp[0] == "OK")
			// {
				clearZone("inputF10Fosse");
				return false;
			// }
			// else
			// {
				// bonvalider();
				// if(validok)
					// alert("Bon mis en attente !");
			// }
		break;
		default:
		break;
	}
}

/* Pte a tester et a valider afin d'ajouter le rechargement du bon avant sa modification
// Pour recharger le mouvement avant une modification Voir dans la V6
function bonreload(row)
{
    var dogFonction    = "get-cermvt�a-cermvt";
	var inputParameter = "ROWID�" + getField(RspBons[row],'ROWID');
    retourExchange     = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");

	RspBons = Rsp.clone();
}
*/

function bonModifier()
{
	trace("--bonModifier--");

    if (gridBons.getRowCount() < 0 || gridBons.getCurrentRow() < 0) return false;
    if (!ctrlDate()) return false;
    if (visu /* || !isSaisieEnCours */) return false;

    var row = parseInt(gridBons.getCurrentRow());

    /* Pte a tester et a valider afin d'ajouter le rechargement du bon avant sa modification
    // bonreload(row);
    */

	isSaisieEnCours = true;
    modifBon        = true;
	
	cerChrono1erMvt = getField(RspBons[row],'CER-CHRONO');
	
	bonafficher(row, true);

	if($("btn_ouv_grid").className == "navImg tailleImg15 upImg15")
		ouvGrid();
	
	oldPdsNorme = parseFloatStr($('DICT�pdsnormes').value).toFixed(3);
	oldContratCer = $('DICT�cer-contrat').value;
}


function bonsupprimer(avoir)
{
	//si on est en saisie d'un nouveau on bloque la suppression
	if (isCreation || visu)
		return;
	//pour certaines soci�t� en consultation uniquement
	if("<?=$_consult?>".match("<?=$c_codsoc?>")!=null)
    {
		alert("En consultation uniquement")
		return;
	}
	if(!ctrlDate())
		return;
	if($("DICT�campagne").value.trim()=="" || $("DICT�codcer").value.trim()==""|| $("DICT�codmvt").value.trim()=="")
	{
		alert("Campagne/Mouvement/C�r�ale obligatoires")
		return;
	}
	//Contr�le sur l'autorisation de suppression du mouvement du bon
	RspTmp = Rsp.clone();
	inputParameter = "CODSOC�<?=$c_codsoc?>�CODMVT�" + $("DICT�codmvt").value.trim();
	dogFonction = "get-codmvt�a-tabcer";
	retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "" , "" );
	if(retourExchange != "OK")
	{
		alert("Impossible de supprimer un bon avec ce type de mouvement !")
		return false;
	}
	Rsp = RspTmp;
	
    //si le bon est eclat� seul siege avec alert peuvent modifier
	if(statutBon.trim().toUpperCase()=="ECLAT�" /* && "<?=$ident?>" == "333" */)
	{

		Rsp = RspOperat.clone();
		if(setValueFromKeyword("KES-OPMAIT", false ,"" ,"", true)=="N")
		{
			alert("Bon "+ statutBon +", suppression impossible")
			Rsp = RspTmp
			return;
			isSaisieEnCours = false
		}
		else
			alert("Attention le bon est "+ statutBon)
		Rsp = RspTmp;
	}
	
    if(avoir!=true)
	{
		if(statutBon.trim() != "" && statutBon.trim().toUpperCase() != "ECLAT�")
		{
			alert("Bon "+ statutBon.toLowerCase().trim() +", suppression impossible")
			return;
		}
	}
	
    if(annulBon.trim().toUpperCase()=="N")
	{
		alert("Suppression interdite")
		return;
	}
	//contr�le droit d�p�t/si�ge et j/j+1
	Rsp = RspOperat.clone();
	if(getField(RspCerMvt[1], "BONMODIF") == "N")
	{
		alert("Impossible de supprimer le bon.");
		return false;
	}//On pr�f�re passer par le param�tre ramener par le dog-cermvt (BONMODIF)	
		
	/* 
	
	if(getValue("KES-OPMAIT", false ,"" ,"", true)=="N")
	{
		
		rowencours = gridBons.getCurrentRow()
		Rsp = [];
		Rsp[0] = "OK"
		Rsp[1] = RspBons[rowencours];
		if(getValue("CER-DATSAI", false, "", "", true) != "<?=date("d/m/Y")?>")
		{
		   alert("Impossible de supprimer le bon.");
			return false;
		}
	}
	else
	{
		if(getValue("CER-DATSAI", false, "", "", true) == "<?=date("d/m/Y")?>")
		{
			alert("Impossible de supprimer le bon.");
			return false;
		}
	} */
	/* bej remettre de RSP */
	Rsp = RspTmp;
	// trace("topSuppression == "+ topSuppression)
	//on ouvre le pav� de gestion cereale cellule motif
	if(!topSuppression && "<?=$_nomotif?>".trim() == "")
	{
		$("MOTIF").show();
		//zonscreenget("motif��2�typ-motif��2�")
		$("DICT�codcer-ret").value=$("DICT�codcer").value
		$("DICT�codcer-ret_libel").value=$("DICT�codcer_libel").value
		enableZone(zonesMotif.toString().replace(/,/gi, "|"), true);
		$("DICT�motif").focus();
		topSuppression=true;
		return;
	}

	var inputParameter = "CODSOC�<?=$c_codsoc?>�TYPCTR�"+typctr.trim()+"�MOTIF�"+$("DICT�motif").value+"�COD-DEF�"+$("DICT�cod-def").value+"�CAUSE-DEF�"+$("DICT�cause-def").value+"�TRAIT-DEF�"+$("DICT�trait-def").value+"�ACT-DEF�"+$("DICT�act-def").value+"�FIC-DEF�"+$("DICT�fic-def").value+"�DESC-DEF�"+$("DICT�desc-def").value+"�NOFIC-DEF�"+$("DICT�nofic-def").value+"�CER-CHRONO�"+cer_chrono.trim()+"�MAGASI�"+$("DICT�magasi").value.trim()+"�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�CER-DATSAI�"+$("DICT�cer-datsai").value.trim()+"�ROWID�"+rowidCours.trim()+"�CODCER-RET�"+$("DICT�codcer-ret").value.trim()+"�CELLULE-RET�"+$("DICT�cellule-ret").value.trim();
	inputParameter += "�CER-DATMOD�" + $("DICT�datsai").value;
	inputParameter += "�DOGADMIN�3";
	// Voulez-vous vraiment supprimer l'enregistrement ?
	if(window.confirm( "<?PHP print lect_bin::getLabel ( "mess-tabgen2_59" , "sl"); ?>" ) )
	{
		retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , "MAJ-CERMVT�a-majcermvt" , "" , "" );
		
		gridBons.setRowCount(0);
		gridBons.refresh();
		
		if (retourExchange != "OK")
			return false;

		topSuppression = false;

		//on reactualise le tableau
		rowidCours = "";

		gridBons.setRowCount(0);
		gridBons.refresh();
		
		razEcran(true);
	}
}

function acceptMotif2()
{
	trace("--acceptMotif2--")
	
	if($('DICT�datsai').value.trim() != '')
	{
		$('MOTIF2').style.display='none';
		strdat=$('DICT�datsai').value;
		
		if(topavoir)
		{
			topavoir=false;
			bonIssuGenenerationPlusMoins = true;
			bonafficher(currow,true,true);
		}
		else
		{
			topSuppr=true;
			bonafficher(currow,true,true);
		}
	}
}

/**
	M�thode de la g�n�ration +/-
	- Permet d'annuler un bon en cr�ant un bon identique avec poids n�gatif
	- De "modifier" le bon en cr�ant deux bons (1 negatif et 1 positif) pour compenser le poids saisie dans le premier bon.
**/
function bonavoir()
{
	//Saisie en cours OU pas de code mouvement saisi => return
    if(isSaisieEnCours || $("DICT�codmvt").value.trim() == "") 
		return false;

    clearZone("DICT�motif");
	
	// Rsp = RspOperat.clone();
	//si operateur depot le topvalidation est grise
	if(getField(RspOperat[1],'KES-OPMAIT').trim().toUpperCase() == "N")
	{
		alert('Action non autoris�e');
		return false;
	}
	
	//pour certaines soci�t� en consultation uniquement
	if("<?=$_consult?>".match("<?=$c_codsoc?>")!=null)
	{
		alert("En consultation uniquement");
		return false;
	}
	
	if(confirm("Cliquer sur \"Ok\" pour annuler ce bon et le recr�er.\n Cliquer sur \"Annuler\" pour simplement annuler ce bon."))
	{
		if(currow == -1)
			return false;
		
		topavoir = true
		isSaisieEnCours = true;
		$("DICT�typ-motif").value = "TOUS";
		
		if(!ctrlDate())
			return false;
		
		if("<?=$_motif2?>"!="")
		{
			$("MOTIF2").show();
			zonscreenget("�datsai��11");
			$("DICT�datsai").focus();
			return true;
		}
		if("<?=$_motif?>"!="")
		{
			$("MOTIF").show();
			zonscreenget("�motif��11");
			$("DICT�motif").focus();
		}
		else
			bonafficher(currow, true, true);
		//rajout car parfois la zone se trouve bloqu�e 20131016
		// zonscreenget("�typaux��11�codauxges��11")
	}
	else
	{
		$("DICT�typ-motif").value = "NEG";
		if("<?=$_motif2?>"!="")
		{
			$("MOTIF2").show();
			zonscreenget("�datsai��11");
			// $("DICT�datsai").setAttribute("1", "OBLI");
			$("DICT�datsai").focus();
			return true;
		}
		if("<?=$_motif?>"!="")
		{
			$("MOTIF").show();
			zonscreenget("�motif��11");
			$("DICT�motif").focus();
			$("DICT�codcer-ret").value=$("DICT�codcer").value;
			$("DICT�codcer-ret_libel").value=$("DICT�codcer_libel").value;
			topSuppr = true;
			topSuppression = true;
			return false;
		}
		bonsupprimer(true);
	}
}

function bascule()
{
	if(topBascule)
	{
		$("DICT�cod-def").value		 = $("DICT�cod-def1").value;
		$("DICT�desc-def").value     = $("DICT�desc-def1").value;
		$("DICT�cause-def").value    = $("DICT�cause-def1").value;
		$("DICT�trait-def").value    = $("DICT�trait-def1").value;
		$("DICT�act-def").value      = $("DICT�act-def1").value;
		$("DICT�fic-def").value      = $("DICT�fic-def1").value;
		$("DICT�nofic-def").value    = $("DICT�nofic-def1").value;
		initbufferscreen();
		topBascule=false;
	}
}

function bonrefus()
{
	// if($('DICT�motif').value.trim()!=''||$('DICT�motif').parentElement.style.visibility=='hidden')
	// {
		// $('MOTIF').style.visibility='hidden';
		// topBascule=true;
		// bascule();
		// bonafficher(cur_lig,true,true);
	// }
	if($('DICT�motif').value.trim() != '')
	{
		$('MOTIF').hide();
		topBascule=true;
		bascule();
		bonafficher(currow,true,true);
		// if(topSuppression)
		// {
			// bonsupprimer();
			// topSuppression = false;
			// setFocus(TabFocusEntree, "FIRST");
			// return;
		// }
	}
	else
	{
		if(confirm("Vous n'avez saisi aucun motif de refus. Rester sur cette fen�tre ?"))
		{
			return false;
		}
		else
		{
			sortieMotif();
		}
	}
}

function ouvertureSpool()
{
	//On ouvre le spool
	for(kk=1;kk<=NumEntries(Rsp[1],",");kk++)
	{
		strRsp = Entry(kk,Rsp[1],",");
		ExpRsp = strRsp.split("=");
		NomSpool = ExpRsp[1];
		if(NomSpool==undefined)
			return false;
		if(NomSpool.match("turbo"))
		{
			httpFile = "<?=$xcspoolRelPath.$ope?>/" + NomSpool;
			$("link-visu").href = httpFile;
			//	if(!topattente)
					$("link-visu").click();
		}
	}
}

/**
	Fonction ouverture spool pour l'�clatement.
	On ouvre le spool du bon d'origine.
**/
function ouvertureSpoolEclatement()
{
	trace("--ouvertureSpoolEclatement--")
	trace("	cerChrono1erMvt : " + cerChrono1erMvt)
	
	if(cerChrono1erMvt == "")
		return;
	
	var dogFct = "imp-cermvt�a-majcermvt";
	var requete = "CODSOC�<?=$c_codsoc?>�CER-CHRONO�"+cerChrono1erMvt
	            +"�MAGASI�"+$('DICT�magasi').value
				+"�CAMPAGNE�"+$('DICT�campagne').value
				+"�CER-DATSAI�" + $('DICT�cer-datsai').value;
				
	var retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , requete , dogFct , "" , "" );
	
	//Tester le retour
	if (retourExchange != "OK")
	{
		//on part en creation de contrat
		alert("<?PHP print lect_bin::getLabel ( "mess-tabgen2_58" , "sl"); ?>");
		return ;
	}
	
	//On ouvre le spool
	ExpRsp = Rsp[1].split("=");
	NomSpool = ExpRsp[1];
	httpFile = "<?=$xcspoolRelPath.$ope?>/" + NomSpool;
	$("link-visu").href = httpFile;
	$("link-visu").click();
	
	//Raz de la variable
	Rsp = [];
}

function bonValiderVideCellule()
{	
	trace("--bonValiderVideCellule--")

	//--> si gestion des lots
    var i;
	
	// trace("		vide cellule : "+ $("DICT�vide-cellule").value)
	
    //-> Si vide cellule on envoye les 2 mouvements
    if($("DICT�vide-cellule").value == "O")
	{
        //-> pour la sortie
		inputParameter  = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�CODCER�"+$("DICT�codcer").value.trim();
        inputParameter += "�CODMVT�" 		+ "REPORT-S"
        inputParameter += "�CER-CHRONO�" 	+ IncCer()
        inputParameter += "�PDSBRUT�" 		+ $("DICT�solde-vide").value
		inputParameter += "�MAGASI�" 		+ $("DICT�magasi").value;
		inputParameter += "�CER-DATSAI�" 	+ $("DICT�cer-datsai").value;
		inputParameter += "�CELLULE�" 		+ $("DICT�cellule").value;

		//on lance la mise a jour
		retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "MAJ-CERMVT�a-majcermvt", "", "");

	   //-> pour l'entree
		inputParameter  = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�CODCER�"+$("DICT�codcer").value.trim();
        inputParameter += "�CODMVT�" 		+ "REPORT-E"
        inputParameter += "�CER-CHRONO�" 	+ IncCer()
        inputParameter += "�PDSBRUT�" 		+ $("DICT�solde-vide").value
		inputParameter += "�MAGASI�" 		+ $("DICT�magasi").value;
		inputParameter += "�CER-DATSAI�" 	+ $("DICT�cer-datsai").value;
		inputParameter += "�CELLULE�" 		+ $("DICT�cellule").value;

		//on lance la mise a jour
		retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "MAJ-CERMVT�a-majcermvt", "", "");
    }

	//-> cas des cellules virtuelles en creation pour les sorties
    if(RspCellule != undefined)
	{
		// trace("RspCellule ok")
    	Rsp=RspCodmvt.concat();
		
		/*trace("		getValue('TYPMVT',false,'','',true) : " + getValue("TYPMVT",false,"","",true))
		trace("		TYPCEL : " + getValue("TYPCEL",false,"","",true))
		trace("		rotation : " + "<?=$_rotation?>")
		trace("		LIBEL1-2 : " + getValue("LIBEL1-2",false,"","",true))
		trace("		isCreation : " + isCreation)
		trace("		topattente : " + topattente)*/
		
		//On regarde si le bon est en attente.
		if(gridBons.getCellText(0,currow) == 'A')
			topattente = true;
		
        if(getValue("TYPMVT",false,"","",true) == "2")
		{
            Rsp=RspCellule;
			
            if((getValue("TYPCEL",false,"","",true) == "V" || ("<?=$_rotation?>" == "O" && getValue("LIBEL1-2",false,"","",true))) && (isCreation || topattente))
			{
	            //-> pour la sortie
				inputParameter  = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�CODCER�"+$("DICT�codcer").value.trim();
	       	    inputParameter += "�CODMVT�" 			+ "VIRTUEL-E"
	       	    inputParameter += "�CER-CHRONO�" 		+ IncCer()
	       	    inputParameter += "�PDSBRUT�" 			+ $("DICT�pdsnet").value; //le01/07/09 $("DICT�solde-vide").value
				inputParameter += "�MAGASI�" 			+ $("DICT�magasi").value;
				inputParameter += "�CER-DATSAI�" 		+ $("DICT�cer-datsai").value;
				inputParameter += "�CELLULE�" 			+ $("DICT�cellule").value;
				inputParameter += "�CHRONO-ORIGINE�" 	+ cer_chrono.trim();

				//on lance la mise a jour
				retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "MAJ-CERMVT�a-majcermvt", "", "");
	          
				//-> pour les nouvelles cellules de l'�clatement (un majcermvt / cellule)
	            for(i=1;i<=12;i++)
				{
	                if($("DICT�cellule_" + i).value.trim()!="")
					{
						inputParameter  = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�CODCER�"+$("DICT�codcer").value.trim();
						inputParameter += "�CODMVT�" 			+ "VIRTUEL-S"
						inputParameter += "�CER-CHRONO�" 		+ IncCer()
						inputParameter += "�MAGASI�" 			+ $("DICT�magasi").value;
						inputParameter += "�CER-DATSAI�" 		+ $("DICT�cer-datsai").value;
						inputParameter += "�CELLULE�" 			+ $("DICT�cellule_" + i).value;
						inputParameter += "�CHRONO-ORIGINE�" 	+ cer_chrono.trim();
						
						if("<?=$_rotation?>" == "O")
							inputParameter += "�ROTATION�" + $("DICT�rotation_" + i).value.trim();
						
						inputParameter += "�PDSBRUT�" + $("DICT�pdsnet_" + i).value;
						
						if($("DICT�vide-cellule_" + i).value=="1")
							inputParameter += "�VIDCEL�O";

	                    retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "MAJ-CERMVT�a-majcermvt", "", "");
	                    
						//-> cas des vides cellule
	                    if($("DICT�vide-cellule_" + i).value=="O")
						{
							//-> pour les entr�es
							inputParameter  = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�CODCER�"+$("DICT�codcer").value.trim();
							inputParameter += "�CODMVT�" 		+ "REPORT-E"
							inputParameter += "�CER-CHRONO�" 	+ IncCer()
							inputParameter += "�MAGASI�" 		+ $("DICT�magasi").value;
							inputParameter += "�CER-DATSAI�" 	+ $("DICT�cer-datsai").value;
							inputParameter += "�CELLULE�" 		+ $("DICT�cellule_" + i).value;
							
							if("<?=$_rotation?>" == "O")
								inputParameter += "�ROTATION�" + $("DICT�rotation_" + i).value.trim();
							
							if($("DICT�vide-cellule_" + i).value=="1")
								inputParameter += "�VIDCEL�O" ;

							if(isNaN($("DICT�solde-vide_" + i).value))
								$("DICT�solde-vide_" + i).value = "0";

							inputParameter += "�PDSBRUT�" + (parseFloatStr($("DICT�solde-vide_" + i).value) - parseFloatStr($("DICT�pdsnet_" + i).value));
							
							retourExchange = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "MAJ-CERMVT�a-majcermvt", "", "");
							
							//-> pour les sorties
							inputParameter  = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�CODCER�"+$("DICT�codcer").value.trim();
							inputParameter += "�CODMVT�" 		+ "REPORT-S"
							inputParameter += "�CER-CHRONO�" 	+ IncCer()
							inputParameter += "�MAGASI�" 		+ $("DICT�magasi").value;
							inputParameter += "�CER-DATSAI�" 	+ $("DICT�cer-datsai").value;
							inputParameter += "�CELLULE�" 		+ $("DICT�cellule_" + i).value;
							
							if($("DICT�vide-cellule_" + i).value=="1")
								inputParameter += "�VIDCEL�O";

							if(isNaN($("DICT�solde-vide_" + i).value))
								$("DICT�solde-vide_" + i).value="0";

							inputParameter += "�PDSBRUT�" + (parseFloatStr($("DICT�solde-vide_" + i).value) - parseFloatStr($("DICT�pdsnet_" + i).value));
							
							//on lance la mise a jour
							retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "MAJ-CERMVT�a-majcermvt", "", "");
	                    }
	                }
	            }
            }
        }
    }

    //-> cas des cellules virtuelles en creation pour les entrees
    if(RspCellule!=undefined)
	{
		Rsp = RspCodmvt.concat();
        if(getValue("TYPMVT",false,"","",true)=="1")
		{
        	Rsp=RspCellule;
        	if((getValue("TYPCEL",false,"","",true)== "V" || ("<?=$_rotation?>" == "O" && getValue("LIBEL1-2",false,"","",true))) && (isCreation || topattente))
			{
				//-> pour la sortie
		        inputParameter  = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�CODCER�"+$("DICT�codcer").value.trim();
				inputParameter += "�CODMVT�" + "VIRTUEL-S"
				inputParameter += "�CER-CHRONO�" + IncCer()
				inputParameter += "�PDSBRUT�" + $("DICT�solde-vide").value
		        inputParameter += "�MAGASI�" + $("DICT�magasi").value;
		        inputParameter += "�CER-DATSAI�" + $("DICT�cer-datsai").value;
		        inputParameter += "�CELLULE�" + $("DICT�cellule").value;
		        inputParameter += "�PDSBRUT�" + $("DICT�pdsnet").value;
				inputParameter += "�CHRONO-ORIGINE�" + cer_chrono.trim();
				
				for(kk=1;kk<=14;kk++)
				{
					inputParameter = inputParameter + "�LIBCAR_"+kk + "�" + $("DICT�libcar_"+kk).value
					inputParameter = inputParameter + "�VAL-ETAT_"+kk + "�" + $("DICT�val-etat_"+kk).value;
				}
		       
			   //on lance la mise a jour
		        retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "MAJ-CERMVT�a-majcermvt", "", "");
		       
			   //-> pour les nouvelles cellules
		        for(i=1;i<=12;i++)
				{
		            if($("DICT�cellule_" + i).value.trim()!="")
					{
				        inputParameter  = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�CODCER�"+$("DICT�codcer").value.trim();
						inputParameter += "�CODMVT�" + "VIRTUEL-E"
						inputParameter += "�CER-CHRONO�" + IncCer()
						inputParameter += "�PDSBRUT�" + $("DICT�solde-vide").value
				        inputParameter += "�MAGASI�" + $("DICT�magasi").value;
				        inputParameter += "�CER-DATSAI�" + $("DICT�cer-datsai").value;
				        inputParameter += "�CELLULE�" + $("DICT�cellule_"+i).value;
						inputParameter += "�CHRONO-ORIGINE�" + cer_chrono.trim();
						
						if("<?=$_rotation?>" == "O")
							inputParameter += "�ROTATION�" + $("DICT�rotation_" + i).value.trim();
				        
						inputParameter += "�PDSBRUT�" + $("DICT�pdsnet_"+i).value;
						
						for(kk=1;kk<=14;kk++)
						{
							inputParameter += "�LIBCAR_"+kk + "�" + $("DICT�libcar_"+kk).value
							inputParameter += "�VAL-ETAT_"+kk + "�" + $("DICT�val-etat_"+kk).value;
						}
						
				        //on lance la mise a jour
				        retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "MAJ-CERMVT�a-majcermvt", "", "");
		            }
				}
			}
        }
    }
}

function IncCer()
{
	var inputParameter="�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim() + "�CER-DATSAI�"+$("DICT�cer-datsai").value.trim() + "�MAGASI�"+$("DICT�magasi").value.trim();
	//on lance la mise a jour
	retourExchange  = sendExchangeInvoke( "<?=$dogExchange?>", inputParameter, "INC-CERMVT�a-cermvt", "", "");
	if (retourExchange != "OK")
	{
		alert("Probl�me sur la cr�ation d'un nouveau bon !");
		return;
	}
	return getValue("CER-CHRONO", false ,"" ,"" ,true);
}

/**FIN FONCTIONS DES BONS *****/
/***** FONCTION SUR LES ONGLETS ***/

var g_classOngSelec;
var g_classOngNonSelec;
function initOnglets(idDivOnglet, _top, _left, _largeur, _nb, p_classOngSelec, p_classOngNonSelec)
{
	//On initialise les variables
	var left = _left;
	_topOnglets = _top;
	_nbOnglets = _nb;
	g_classOngSelec = p_classOngSelec;
	g_classOngNonSelec = p_classOngNonSelec;
	// var top = POSTOPONGLETINACTIF;
	// var largeur = WIDTHTETEONGLET;
	var nbOngletsAffiches = 0;

	//On r�cup�re les t�tes d'onglet
	var elements = Element.descendants(idDivOnglet);//Element.descendants(element)
	for(var i = 0; i < elements.length; i++)
	{
		if(elements[i].tagName == "DIV" && elements[i].id.indexOf("ong") == 0)
		{
			var ong = elements[i];//On r�cup�re l'onglet pour travailler avec
			ong.style.width = _largeur + "px";
			var numOnglet = ong.id.slice(3);
			// trace(numOnglet)
			$("ong" + numOnglet +"_p").style.width = (_largeur - 10)+ "px";

			if(ong.style.display == "none")//Si on d�sire cacher un onglet on le saute
			{
				continue;
			}



			if(nbOngletsAffiches == 0) // Si premier onglet
			{
				//On positionnne le top
				ong.style.top = (_top - 5) + "px";

				//On positionnne � gauche
				ong.style.left = left + "px";
			}
			else //Sinon
			{
				// On positionnne le top
				ong.style.top = _top + "px";

				//On positionnne � gauche
				ong.style.left = (left + 1) + "px";
			}
			//On incr�ment le nombre d'onglet Affich�s
			nbOngletsAffiches++;

			//On met � jour le left pour le prochain onglet
			left = (_largeur*nbOngletsAffiches) + _left;
		}
	}
}

var ongletActif;
var _topOnglets;
var _nbOnglets;
function loadOnglet(flag)
{
	//On met tous les onglets en position inactif tout en cachant les contenus
	for(var i = 1; i <= _nbOnglets; i++)
	{
		$("ong" + i).style.top = _topOnglets + "px";
		$("contenuOng" + i).style.display = "none";
		$("ong" + i).className = g_classOngNonSelec;
	}
	ongletActif = flag;
	//On met l'onglet actif au dessus des autres
	$("ong" + flag).style.top = (_topOnglets - 5) + "px";

	//On change la classe CSS
	$("ong" + flag).className = g_classOngSelec;

	//On affiche le contenu de l'onglet qui va bien
	$("contenuOng" + flag).style.display = "block";

	//Chargement des donnees selon l'onglet
	try
	{
		afterLoadOnglet(ongletActif);
	}
	catch(err){}

}

function afterLoadOnglet(ongletActif)
{
	switch(ongletActif)
	{
		case 1:
			// enableZone("ALL", true, zonesOnglet1);
			break;
		case 2:
			setFocus(zonesOnglet2, "FIRST");
			break;
		case 3:
			var RspBack = Rsp.clone();
			Rsp = RspCodmvt.concat();
			switch(getValue ( "TYPTRANS"  , false , "" , ""  , true))
			{
				case "1": //vente
					setFocus(zonesOnglet3Vente, "FIRST");
				break;
				case "2": //achat
					setFocus(zonesOnglet3Achat, "FIRST");
				break;
				case "3": //transfert
					setFocus(zonesOnglet3Transfert, "FIRST");
				break;
				default:
					setFocus(zonesOnglet3Achat, "FIRST");
				break;
			}
		break;
		case 4:
			setFocus(zonesOnglet4, "FIRST");
		break;
		case 5:
			setFocus(zonesOnglet5, "FIRST");
		break;
		case 6:
		break;
		default:
		break;
	}
}

function ouvGrid()
{
	if($("btn_ouv_grid").className == "navImg tailleImg15 upImg15")
	{
		//fermer pav�
		$("divGridBons").style.height = "22px";

		//Monter les onglets
		$("divEntetesOnglets").style.top = "97px";
		$("corps").style.top = "137px";

		//Agrandir les onglets
		$("corps").style.height = (parseInt($("corps").style.height.slice(0,-2)) + 178) + "px";
		$("corpsOnglet").style.height = (parseInt($("corpsOnglet").style.height.slice(0,-2)) + 178) + "px";

		//changer classe CSS
		$("btn_ouv_grid").className = "navImg tailleImg15 downImg15";

		//Changer tooltip
		$("btn_ouv_grid").title = "Ouvrir";
	}
	else
	{
		//Ouvrir pav�
		$("divGridBons").style.height = "200px";

		//Descendre les onglets
		$("divEntetesOnglets").style.top = "275px";
		$("corps").style.top = "315px";
		//R�tr�cir les onglets
		$("corps").style.height = (parseInt($("corps").style.height.slice(0,-2)) - 178) + "px";
		$("corpsOnglet").style.height = (parseInt($("corpsOnglet").style.height.slice(0,-2)) - 178) + "px";

		//changer classe CSS
		$("btn_ouv_grid").className = "navImg tailleImg15 upImg15";

		//Changer tooltip
		$("btn_ouv_grid").title = "Fermer";
	}
}
/**FIN FONCTION SUR LES ONGLETS ***/

/** FONCTIONS DES ADRESSES **/
function adrliv()
{
	//on affiche le pave des adresses
	if ($("DICT�codauxges").value.trim() != "")
	{
		$("ADRESSE").style.display="";
		$("DICT�cod-adrliv").focus();
	}
}

function ctrlQtePdsNormes(){
	trace("--ctrlQtePdsNormes--");
	if(eclatementSPE && gridEclat.getRowCount() > 0){
		dataGridEclat = [];
		gridEclat.setRowCount(0);
		gridEclat.refresh();
		listePdsBrutEclat = [];
	}
}

function changePdsNormes(){
	if(parseFloatStr($('DICT�pdsnormes').value) != 0){
		topForce=true;
		topForceValue="F";
		$('DICT�pdsnormes').style.color="red"
	}else{
		topForce=false;
		topForceValue="D";
		$('DICT�pdsnormes').style.color=""
		//on recalcul le pdsNormes
		ctrlpdsnormes()
	}
}

/** FONCTIONS DE CONTROLE **/
function ctrlvidecellule()
{
	// cette fonction teste si le vide cellule est autorise
	Rsp=RspCodmvt.concat();
	if(getValue("TYPMVT", false ,"" ,"", true)!="2")
		zonscreenget("�vide-cellule��2")  //invisible
	else
		zonscreenget("�vide-cellule��8")  //visible
<?if($ident=="333"){?>
	if(getField(RspCodmvt[1],"TYPMVT") == "2"){ // sortie
		zonscreenget("�vide-cellule��2")  //invisible
	}
<?}?>
}

function ctrlcapacite()
{
	//permet de renseigner les infos cellules/rotation et de contr�ler les valeurs
	if("<?=$_rotation?>".toUpperCase() == "O")//si on g�re les rotations
	{
		Rsp = RspRotation;
		getValue("STOCK", false ,"" ,"DICT�stock-cel", true);
		Rsp=RspCellule;
		getValue("NOMBRE-1", false ,"" ,"DICT�capacite", true);
	}
	else//Sinon on utilise la cellule
	{
		Rsp=RspCellule;
		getValue("STOCK-CEL", false ,"" ,"DICT�stock-cel", true);
		Rsp=RspCellule;
		getValue("CAPACITE", false ,"" ,"DICT�capacite", true);
	}

	if(isNaN($("DICT�stock-cel").value) || $("DICT�stock-cel").value.trim()=="")
		$("DICT�stock-cel").value="0.00"
	if(isNaN($("DICT�pdsnormes").value) || $("DICT�pdsnormes").value.trim()=="")
		$("DICT�pdsnormes").value="0.00";
	if(isNaN($("DICT�capacite").value)||$("DICT�capacite").value.trim()=="")
		$("DICT�capacite").value="0.00";
	//solde vide
	Rsp=RspCodmvt.concat();

	if(getValue("TYPMVT", false ,"" ,"", true)!="1")
    {
        $("DICT�solde-vide").value=parseFloatStr($("DICT�stock-cel").value.trim())-parseFloatStr($("DICT�pdsnormes").value.trim());
        $("DICT�solde-vide").value = parseFloatStr($("DICT�solde-vide").value).toFixed(3)
    }
    else {
		$("DICT�solde-vide").value=parseFloatStr($("DICT�stock-cel").value.trim())+parseFloatStr($("DICT�pdsnormes").value.trim());
        $("DICT�solde-vide").value = parseFloatStr($("DICT�solde-vide").value).toFixed(3)
		<?if($ident != "333"){?>
		if(parseFloatStr($("DICT�solde-vide").value)>parseFloatStr($("DICT�capacite").value)&&parseFloatStr($("DICT�capacite").value)!=0)
			alert("Capacit� cellule de " + $("DICT�capacite").value + " d�pass�e");
		<?}?>
	}

	//dans le cas des sorties avertir si le stock est n�gatif
	Rsp=RspCodmvt.concat();
	if("<?=$ident?>" != "333" && "<?=$ident?>"!="392"/*  && isSaisieEnCours */)
	{
		if(getValue("TYPMVT",false,"","",true)== "2")
		{
			//avertir sans bloquer
			if($("DICT�cellule").value.trim()!="")
			{
				Rsp=RspCellule;
				if(getValue("TYPCEL",false,"","",true)!= "V" && RspCellule != undefined && RspCellule != "" )
					if(parseFloatStr($("DICT�solde-vide").value)<0)
						alert("Stock de la cellule n�gatif!");
			}
		}
	}
}

/**
	V�rifier les autorisations de la cellule.
**/
function ctrlCellule()
{
	trace("--ctrlCellule--")
	
    // Avec la gestion des rotations, cette fonction n'est pas utilisee
    if ("<?=$_rotation?>" != "") 
		return true;

    // Rsp=RspCodcer;
	if(getField(RspCodcer[1],'CODCER-REG') != "")
		strCereale = getField(RspCodcer[1],'CODCER-REG');
	else
		strCereale = $("DICT�codcer").value
	
	// Rsp=RspCellule
	//-> Recuperation de la cereale, la famille et sous famille de la Cellule
	strCerCellule 	= getField(RspCellule[1],'CODCER');
	strFamille 		= getField(RspCellule[1],'FAMCER');
	strSFamille 	= getField(RspCellule[1],'SFMCER');
	
	trace("ETAT 1")
	
	// Rsp=RspCodcer
	//-> On verifie que le code Cellule ait une cereale
	if(strCerCellule != "" && "<?=$_rotation?>".toUpperCase() != "O")
	{
   		//-> Si cereale <> alors message d'erreur
		if(strCereale != strCerCellule) 
		{
			//-> Pas ok
			alert("Cellule non autoris�e c�r�ale autoris�e " + strCerCellule)
			return false;
		}
	}
		//-> Sinon, faire la comparaison sur la Sous-Fam
	else if(strSFamille != "" && "<?=$_rotation?>".toUpperCase() != "O")
	{
		trace("ETAT 2")
		//-> On verifie que les famille soient =
		if(strFamille != getField(RspCodcer[1],'FAMCER'))
		{
			//-> Pas ok
			alert("Cellule non autoris�e famille autoris�e " + strFamille)
			return false;
			//-> On verifie que les Sous familles soient =
		}
		else if(strSFamille != getField(RspCodcer[1],'SFMCER'))
		{
			//-> Pas ok
			alert("Cellule non autoris�e sous famille autoris�e " + strSFamille)
			return false;
		}
	}
		//-> On verifie que les famille soient =
	else if(strFamille != "" && "<?=$_rotation?>".toUpperCase() != "O")
	{
		trace("ETAT 3")
		if(strFamille !=  getField(RspCodcer[1],'FAMCER'))
		{
			//-> Pas ok
			alert("Cellule non autoris�e famille autoris�e " + strFamille);
			return false;
		}
	}
	
	return true
}


function ctrlContrat()
{
	//on verifie le parametrage si ope depot et mvt achat peut saisir les contrats
	inputParameter = "�MOTCLE�PARAPP-CER�CODTAB�" ;
	dogFonction="GET-TABGEN�a-tabgen";
	//Faire le Get
	retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, dogFonction, "", "");
	//on reccupere le pourcentage de depassement autorise
	try
	{
	depasaut=parseFloatStr(getValue("NOMBRE-1" ,false ,"" ,"", true));
	}catch(ex){}
	if(getValue("LIBEL2-27" ,false ,"" ,"", true)=="1")
	{
		Rsp=RspOperat.clone();
		//si on est un operateur depot
		if(getValue("KES-OPMAIT", false ,"" ,"", true)=="N")
		{
				Rsp=RspCodmvt.concat();
				// si on est en achat
				if(getValue ( "TYPTRANS"  , false , "" , ""  , true)=="2")
				{
					//on grise la zone contrat
					zonscreenget("�cer-contrat��0");
				}
		}
	}

}
//on v�rifie la date saisie
function ctrlDate()
{
	if(RspDate.length == 0)
	{
		//Recherche de la date
		retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�CODSOC�<?=$c_codsoc?>�OPERAT�<?=$opebase?>�CER-DATSAI�" + $("DICT�cer-datsai").value, "GET-TSTDATE�a-tabcer", "", "");
		//on regarde si on a ramene qque chose
		if (retourExchange != "OK")
			return false;
		RspDate=Rsp.clone();
	}
	Rsp=RspDate;
	DateMini =  getValue("DATE-MINI" ,false ,"" ,"", true);
	CampagneMaxi = getValue("CAMP-MAXI" ,false ,"" ,"", true);
	CampagneMini = getValue("CAMP-MINI" ,false ,"" ,"", true);
	if(CompareDate($("DICT�cer-datsai").value, DateMini, 2))
	{
		alert("La date de saisie doit �tre sup�rieure ou �gal � " + getValue("DATE-MINI", false, "", "", true))
		$("DICT�cer-datsai").focus();
		return false;
	}
	//on controle la campagne
	if(CampagneMaxi!="" && $("DICT�campagne").value.trim()!="")
		if(parseInt(CampagneMaxi) < parseInt($("DICT�campagne").value.trim()))
		{
			alert("Campagne non autoris�e")
			return false;
		}
	Rsp=RspOperat.clone();
	//le operateur depot peuvent pas saisir en dessous de la campagne mini
	if(getValue("KES-OPMAIT", false ,"" ,"", true)=="N")
		if(CampagneMini!="" && $("DICT�campagne").value.trim()!="")
			if(parseInt(CampagneMini) > parseInt($("DICT�campagne").value.trim()))
			{
				alert("Campagne non autoris�e.")
				return false
			}
	return true;
}

function ctrlMulti(skipalert)
{	//--> cette procedure verifie que le solde est bien a zero
	if(parseFloatStr($("CALC�solde").value)!=0)
	{
		if(!skipalert)
			alert("Le solde doit �tre �gal � 0.")
		return false
	}
	else
		return true
}

function ctrlNomSaisi(){
	Rsp = RspNumContrat.concat()
	if (setValueFromKeyword("AUTORI-CHARG", false, "", "", true) == "N")
	{
        if (nomAutorise == "_ANNUL")
			nomAutorise = "";
		
	    var saisie = prompt("Nom de la personne autorisant le chargement", nomAutorise);
		if (saisie == null || saisie == "")
		{
			$("DICT�codcer").value      = "";
			$("DICT�codcer").defaultValue      = "";
			$("DICT�cer-contrat").value = "";
			$("DICT�cer-contrat").defaultValue = "";
			$("DICT�codcer").focus();
			return false;
		}
		else
	        nomAutorise = saisie;
	}
	return true;
}

/**
	M�thode de contr�le de la saisie.
	On parcourt l'ensemble des zones de saisie OBLIGATOIRE
	pour v�rifier qu'elles ont bien �t� saisies.
**/
function ctrlobli()
{
	trace("--ctrlobli--")
	
	//R�cup�ration des toutes les balises INPUT
	var inputs = document.getElementsByTagName("INPUT");
	
	for(i=1;i<=inputs.length-1;i++)
		if(inputs[i].id != undefined)
		{
			/*
				On test : 
				SI la zone est obligatoire.
				ET SI la zone est vide OU que la valeur par d�faut de la zone a chang�e.
				ET SI la zone n'est pas en lecture seule.
				ET SI la zone n'est pas cach�e. (DISPLAY = NONE)
				ET SI la DIV dans laquelle elle est positionn�e n'est pas cach�e (DISPLAY = NONE)
				ET SI la DIV dans laquelle elle est positionn�e n'est pas cach�e (VISIBILITY = HIDDEN)
			*/
			if(inputs[i].getAttribute("OBLI")=="1"
			&& (inputs[i].value.trim() == "" || inputs[i].value.trim().toUpperCase() != inputs[i].defaultValue.trim().toUpperCase())
			&& inputs[i].readOnly != true
			&& inputs[i].style.display != "none"
			&& (inputs[i].parentElement.style.display != "none" 	   	&& inputs[i].parentElement.parentElement.style.display != "none")
			&& (inputs[i].parentElement.style.visibility != "hidden" 	&& inputs[i].parentElement.parentElement.style.visibility != "hidden") )
			{			
				if(codelang==""||codelang=="01")
				{
					try{
						if($(inputs[i].id).previous().innerHTML != "" && $(inputs[i].id).previous().innerHTML != undefined)
							alert("Zone obligatoire : " + $(inputs[i].id).previous().innerHTML);
						try{
							// Pte 12/04/2016 ouvrir la fenetre des adresses de livraisons si zone non renseign�es
                            if(inputs[i].id == 'DICT�adr-liv_1') adrliv()

							inputs[i].focus()
						} catch(ex){};
						return false;
					}catch(err){}
				}
				else
					alert("Mandatory field");
				try{inputs[i].focus()} catch(ex){};
				return false;
			}
			
			
			/*
				On test : 
				SI la valeur est renseign�
				ET SI la zone n'est pas obligatoire
				ET SI la valeur de la zone par d�faut est diff�rente de la valeur saisie
				ET SI la zone n'est pas en lecture seule
				ET SI la zone n'est pas cach�e (DISPLAY = NONE)
				ET SI la zone contient un F10
				ET SI l'�l�ment dans lequelle il est positionn� n'est pas cach� (DISPLAY = NONE)
			*/
			/* if(inputs[i].value.trim() != ""
			&& inputs[i].getAttribute("OBLI") != "1"
			&& inputs[i].defaultValue.trim() != inputs[i].value.trim()
			&& inputs[i].readOnly!=true
			&& inputs[i].style.display != "none"
			&& inputs[i].getAttribute("NATURE") == "SEARCH"
			&& (inputs[i].parentElement.style.display!="none" || inputs[i].parentElement.style.display!="none") )
			{
				trace("ZONE : " + inputs[i].id);
				trace(dogexchange(inputs[i], "GET", false))
				
				// if(!dogexchange(inputs[i], "GET", false))
				// {
					// try{inputs[i].focus()} catch(ex){trace("--ctrlobli : " + ex)};
					// return false;
				// }
			} */
		}


	return true;
}

function ctrlpoids(skipctrlcellule,orig)
{
	trace("--ctrlpoids skipctrlcellule : "+skipctrlcellule+" - orig : "+orig+" --")
	
	try{if(topattente==true)return false;}catch(ex){}
	toptare=false;
	
	//-> On contr�le que l'operation "PdsBrut - Tare" soit <= Poids autoris� selon le parametrage du couple Mvt/Codcer
	if($("DICT�tare").value.trim()==""||isNaN($("DICT�tare").value))
	{
		vtare="0.00";
		toptare=true;
	}
	else
		vtare = $("DICT�tare").value;
	
	if($("DICT�pdsbrut").value.trim()==""||isNaN($("DICT�pdsbrut").value))
		$("DICT�pdsbrut").value="0.0";
	
	//la tare ne peut etre superieure au poids
	if(vtare > Math.abs($("DICT�pdsbrut").value) && orig != "valid")
	{
		alert("La tare doit �tre inf�rieure au poids");
        if (ccTypMvt != "2") {
		    $("DICT�tare").value = "";
		    $("DICT�pdsbrut").value = "0.00";
		    $("DICT�pdsnet").value = "";
		    $("DICT�pdsnormes").value="";
        }
		$("DICT�pdsbrut").focus()
		$("DICT�pdsbrut").select()
		return false;
	}
	
	$("DICT�pdsnet").value = nombre_arrondi($("DICT�pdsbrut").value - vtare ,3);
	$("DICT�pdsnet").defaultValue = nombre_arrondi($("DICT�pdsbrut").value - vtare ,3);

	$("DICT�pdsnormes").value = $("DICT�pdsnet").value;
	$("DICT�pdsnormes").defaultValue = $("DICT�pdsnet").value;
	
	//on traite ici le controle pour les reprises
	Rsp = RspCodmvt.concat();
	if(getValue("CTR-SOLDE" ,false ,"" ,"", true)=="O"&&!toptare)
	{
		Rsp = RspStock
		if((parseFloatStr($("DICT�pdsnet").value)>parseFloatStr(getValue("SOLDE", false ,"" ,"", true)) && getValeurRsp("AUTORI-DEPASS").toUpperCase() != "O")
		 && getValue("SOLDE", false ,"" ,"", true)!=""
		 && parseFloatStr($("DICT�pdsnet").value)!=NaN )
		{
			$("DICT�pdsnet").value = "";
			$("DICT�pdsnormes").value="";
			$("DICT�tare").value = "";
			var texte = "Vous pouvez reprendre jusqu'� : " + getValue("SOLDE", false ,"" ,"", true)+ " T Maxi ";
			
			if(getValeurRsp("SOLDER") != "")
				texte += "(" + getValeurRsp("SOLDER") + " T R�el)";
			
			alert(texte)
			$("DICT�pdsbrut").value = "0.00";
			return false;
		}
		else if((parseFloatStr($("DICT�pdsnet").value)>parseFloatStr(getValue("SOLDE", false ,"" ,"", true)) && getValeurRsp("AUTORI-DEPASS").toUpperCase() == "O")
		 && getValue("SOLDE", false ,"" ,"", true)!=""
		 && parseFloatStr($("DICT�pdsnet").value)!=NaN )
		{
			var texte = "Vous pouvez reprendre jusqu'� : " + getValue("SOLDE", false ,"" ,"", true)+ " T Maxi ";
			
			if(getValeurRsp("SOLDER") != "")
				texte += "(" + getValeurRsp("SOLDER") + " T R�el)";
			
			alert(texte + "\nAttention le solde sera factur� !");
		}
	}
	
	Rsp = RspCoderMvt;
	var poidsAControler = "net"
	if(getValue("MVTCER-POIDS", false ,"" ,"", true) == "")
		poidsAControler = "brut"
	
	//-> Si le poids obtenu est > au poids autoris�, on bloque ou pas la saisie selon le parametrage
	if(poidsAControler == "net" && Math.abs(parseFloatStr($("DICT�pdsnet").value))>parseFloatStr(getValue("MVTCER-POIDS", false ,"" ,"", true))&&!toptare)
	{
		if("<?=$ident?>"=="677"&&($("DICT�magasi").value=="6117"||$("DICT�magasi").value=="6092")&&($("DICT�codmvt").value=="CET"||$("DICT�codmvt").value=="CST"))
		{

		}
		else
		{
			//Cas o� on informe simplement l'utisateur
			if(getValue("MVTCER-TYPDEP", false ,"" ,"", true)=="1")
                alert("Attention, vous avez saisi une valeur pour lequel le poids maximum autoris� "+getValue("MVTCER-POIDS", false ,"" ,"", true)+" a �t� d�pass�");
            //Cas o� on bloque la saisie
			if(getValue("MVTCER-TYPDEP", false ,"" ,"", true)=="2")
			{
                alert("Vous avez saisi une valeur pour lequel le poids maximun autoris� "+getValue("MVTCER-POIDS", false ,"" ,"", true)+" a �t� d�pass�, veuillez en ressaisir une autre !");
				$("DICT�pdsnet").value=""
				$("DICT�pdsbrut").value=""
				$("DICT�pdsnormes").value="";
				$("DICT�tare").value = ""
				$("DICT�pdsbrut").focus()
				return false;
			}
		}
	}
	
	if(poidsAControler == "brut" && Math.abs(parseFloatStr($("DICT�pdsbrut").value))>parseFloatStr(getValue("MVTCER-PDSBRUT", false ,"" ,"", true))&&!toptare)
	{
		if("<?=$ident?>"=="677"&&($("DICT�magasi").value=="6117"||$("DICT�magasi").value=="6092")&&($("DICT�codmvt").value=="CET"||$("DICT�codmvt").value=="CST"))
		{

		}
		else
		{
			//Cas o� on informe simplement l'utisateur
			if(getValue("MVTCER-TYPDEP", false ,"" ,"", true)=="1")
				alert("Attention, vous avez saisi une valeur pour lequel le poids maximum autoris� "+getValue("MVTCER-PDSBRUT", false ,"" ,"", true)+" a �t� d�pass�");
			//Cas o� on bloque la saisie
			if(getValue("MVTCER-TYPDEP", false ,"" ,"", true)=="2")
			{
				alert("Vous avez saisi une valeur pour lequel le poids maximun autoris� "+getValue("MVTCER-PDSBRUT", false ,"" ,"", true)+" a �t� d�pass�, veuillez en ressaisir une autre !");
				$("DICT�pdsnet").value=""
				$("DICT�pdsbrut").value=""
				$("DICT�pdsnormes").value="";
				$("DICT�tare").value = ""
				$("DICT�pdsbrut").focus()
				return false;
			}
		}
	}
	
	//-> Si le poids obtenu est <= au poids autoris�, et que la tare est vide on la met a zero
	try
	{
		if(Math.abs(parseFloatStr($("DICT�pdsnet").value))<=parseFloatStr(getValue("MVTCER-POIDS", false ,"" ,"", true))
		 &&$("DICT�tare").value.trim()==""
		 &&event.srcElement.id=="DICT�tare"
		 &&parseFloatStr($("DICT�pdsnet").value)!=0)
		{
			$("DICT�tare").value = "0.00"
		}
	} catch(ex){}
	
	//-> Si le poids obtenu est > au poids autoris�, et que la tare est vide et que l'on est sur la tare alert
	try{
		if(Math.abs(parseFloatStr($("DICT�pdsnet").value))>parseFloatStr(getValue("MVTCER-POIDS", false ,"" ,"", true))
		 && $("DICT�tare").value.trim()==""
		 && event.srcElement.id=="DICT�tare")
		{
			alert("Vous avez saisi une valeur pour lequel le poids maximun autoris� "+getValue("MVTCER-POIDS", false ,"" ,"", true)+" a �t� d�pass�, veuillez en ressaisir une autre !");
			$("DICT�pdsnet").value=""
			$("DICT�pdsbrut").value=""
			$("DICT�pdsnormes").value="";
			$("DICT�tare").value = ""
			$("DICT�pdsbrut").focus()
			return false;
		}
	} catch(ex){}
	
    //on regarde le maxi autoris� par lordre dexecution
	Rsp = RspVoe;
	tonglobal=0;
	
	try
	{
		if(Rsp != undefined)
			tonglobal = parseFloatStr(getValue("TON-GLOBAL", false ,"" ,"" ,true ) / getValue("NB-TOT-LIG", false ,"" ,"" ,true ))
	}
	catch(ex)
	{
		trace(ex);
	}
	
	//on tiend eventuellement compte du depassement autorise
	if(depasaut!=0)
		tonglobal = tonglobal*(100+depasaut)/100;
	
	if(parseFloatStr($("DICT�pdsnormes").value + 0)>tonglobal&&tonglobal!=0)
	{
		//pour eviter d'avoir plusieur fois le message on s'allume un top li� au pdsnorme
		if(toppds!=$("DICT�pdsnormes").value)
		{
			toppds=$("DICT�pdsnormes").value
			alert("Le maximum autoris� est de " + tonglobal);
		}
	}

	Rsp=RspCodmvt.concat();
	if(getValue ( "TYPTRANS"  , false , "" , ""  , true)=="1")
	{
		if(Rsp == undefined)
			Rsp = RspBons[gridBons.getCurrentRow()];
		if((getValue("TYPMVT", false, "", "", true).trim() == "2" && getValue("TYPCTR", false, "", "", true).trim() == "V")
		||  (getValue("TYPMVT", false, "", "", true).trim() == "1" && getValue("TYPCTR", false, "", "", true).trim() == "A"))
		{
			if($("DICT�cer-contrat").value.trim()!="")
			{
				if(RspContrat.length==0)
				{
					var inputParameter = "CODSOC�<?=$c_codsoc?>�TYPCTR�V�CER-CONTRAT�"+$("DICT�cer-contrat").value;
					retourExchange  = sendExchangeInvoke("<?=$dogExchange?>", inputParameter, "GET-CERCTR�a-cerctr", "", "");
					if(retourExchange=="OK")
						RspContrat = Rsp.clone();
				}
				if(RspContrat.length>0)
				{
					var oldPoids = 0.0;
					if(isSaisieEnCours && !isCreation)//Si on est en modif
					{
						Rsp = RspBons[gridBons.getCurrentRow()];
						oldPoids = parseFloatStr(getValeurRsp("PDSNORMES"));
					}
					Rsp = RspContrat;
					if((parseFloatStr(getValue("QTEMAX", false ,"" ,"" ,true )) + oldPoids) < parseFloatStr($("DICT�pdsnormes").value))
					{
						alert("Attention : Poids sup�rieur au solde du contrat : " +getValue("TON-SOLDE", false ,"" ,"" ,true ) + " T Pr�vu"
							  + "   (" +(parseFloatStr(getValue("QTEMAX", false ,"" ,"" ,true )) + oldPoids )+ " T Maxi avec tol�rance)" );
						return false;
					}
				}
			}
		}
    }
	
	/* contr�le poids-mini/maxi */
	if(poidsMin > 0 && poidsMax > 0 && skipctrlcellule)
	{
		var pdsnormes = Math.abs(parseFloatStr($("DICT�pdsnormes").value.trim()));

		if((pdsnormes < poidsMin || pdsnormes > poidsMax) && cer_bontrs_1.trim()!="")
		{
			if(!confirm("Ecart entre le <?PHP print lect_bin::getLabel ( "pdsnormes" , "sl"); ?> (" + parseFloatStr($("DICT�pdsnormes").value).toFixed(3) + "T) et le bon d'origine (" + poidsCST.toFixed(3) + "T).\nVoulez-vous continuer ?"))
				return false;
		}
	}
	
	if(skipctrlcellule)
		return true
	
	return true;
}

function ctrlcellulemulti()
{
	Rsp=RspCellule;
	if("<?=$_rotation?>"=="")
	{
		typcel = getValue("TYPCEL", false, "", "", true).toUpperCase();
	}
	else
	{
		typcel = getValue("LIBEL1-2", false, "", "", true).toUpperCase();
	}
	if(typcel=="V" /* && isCreation */) // isCreation ? que en cr�ation pas que en cr�ation demande de Guillaume, DDD-33-Saisie_mouvements-Cellule_virtuelle.docx
	{
		//-> afficher la multicellule
		$("MULTI").show();
		enableZone("ALL", true, zonesMulti);
		$("CALC�pdsnet").value=$("DICT�pdsnet").value
		zonscreenget("�vide-cellule��2")  //invisible
		$("DICT�cellule_1").focus();
		return false;
	}
	else
		return true
}

function ctrlPdsNet()
{
	//--> cette procedure permet de verifier le poids net
	som = 0;
	for(i=1;i<=12;i++)
	{
	    if(isNaN($("DICT�pdsnet_" + i).value) || $("DICT�pdsnet_" + i).value=="")
	        ValPds = 0
	    else
	        ValPds = parseFloatStr($("DICT�pdsnet_" + i).value)
	    som = som + ValPds;
	    if($("DICT�solde-vide_" + i).value!="")
			$("DICT�solde-vide_" + i).value=(parseFloatStr(soldevide[i])-parseFloatStr(ValPds)).toFixed(3);
	}
	som = parseFloatStr($("DICT�pdsnet").value) - som


	//-> la valeur ne peut pas etre negative
	if(som < -0.000000001) {
	    alert("Quantit� d�pass�e")
	    //return;
	}

	//-> on renseigne le solde
	$("CALC�solde").value = nombre_arrondi(som,3)
}

/**
	Calcule du poid au normes
**/
function ctrlpdsnormes()
{
	if(topForce)return
	trace("--ctrlpdsnormes--")
	
	inputParameter = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�CODCER�"+$("DICT�codcer").value.trim();
	inputParameter += "�CODMVT�" + $("DICT�codmvt").value;
	inputParameter += "�PDSNET�" + $("DICT�pdsnet").value;
	
	for(var kk=1;kk<=14;kk++)
		inputParameter += "�val-etat_"+kk + "�" + $("DICT�val-etat_"+kk).value;

	inputParameter += "�REGION�001";
	retourExchange  = sendExchangeInvoke("<?=$dogExchange?>",  inputParameter, "PDSNORM�a-pdsnorm", "", "");

	if (retourExchange == "OK")
		$("DICT�pdsnormes").value = parseFloatStr(getValue("PDSNORMES", false ,"" ,"" ,true)).toFixed(3);
}

function ctrlReprise()
{//on v�rifie la quantite de reprise si besoin
	Rsp=RspCodmvt.concat();
	if(getValue("CTR-SOLDE" ,false ,"" ,"", true)=="O")
	{
		//Recherche du stock
		retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�CODSOC�<?=$c_codsoc?>�CODCER�" + $("DICT�codcer").value + "�CODMVT�" + $("DICT�codmvt").value+"�CAMPAGNE�"+$("DICT�campagne").value+"�TYPAUX�"+$("DICT�typaux").value+"�CODAUXGES�"+$("DICT�codauxges").value+"�TYPECONSULT�5", "F10-STOCER�a-stocer", "", "");
		//on regarde si on a ramene qque chose
		if (retourExchange != "OK")
		{
			alert("Rien � reprendre!");
			return;
		}
		RspStock=Rsp.clone();

		if(getValeurRsp("SOLDER") == "0.000" && getValeurRsp("SOLDE") == "0.000")
        {
			alert("Rien � reprendre !");
			return;
        }
        else{
            var texte = "Vous pouvez reprendre jusqu'� : " + getValue("SOLDE", false ,"" ,"", true)+ " T Maxi ";
            if(getValeurRsp("SOLDER") != "")
                texte += "(" + getValeurRsp("SOLDER") + " T R�el)";
            alert(texte);
        }
	}
}

function ctrlRepriseFin()
{
//on v�rifie la quantite de reprise si besoin
	Rsp=RspCodmvt.concat();
	if(getValue("CTR-SOLDE" ,false ,"" ,"", true)=="O")
	{
		//Recherche du stock
		retourExchange = sendExchangeInvoke("<?=$dogExchange?>", "�CODSOC�<?=$c_codsoc?>�CODCER�" + $("DICT�codcer").value + "�CODMVT�" + $("DICT�codmvt").value+"�CAMPAGNE�"+$("DICT�campagne").value+"�TYPAUX�"+$("DICT�typaux").value+"�CODAUXGES�"+$("DICT�codauxges").value+"�TYPECONSULT�5", "F10-STOCER�a-stocer", "", "");
		//on regarde si on a ramene qque chose
		if (retourExchange != "OK")
		{
			alert("Rien � reprendre!");
			return false;
		}
		else
		{
			if(parseFloatStr($("DICT�pdsnormes").value)>parseFloatStr(getValue("SOLDE" ,false ,"" ,"", true)))
			{
				var texte = "Vous pouvez reprendre jusqu'� : " + getValue("SOLDE", false ,"" ,"", true)+ " Maxi";
				if(getValeurRsp("SOLDER") != "")
					texte += "(" + getValeurRsp("SOLDER") + " R�el)";

				if(getValeurRsp("AUTORI-DEPASS").toUpperCase() != "O")
				{
					alert(texte);
					return false;
				}
				else
					alert(texte + "\nAttention le solde sera factur� !");
			}
		}
	}
	return true;
}

/**
	M�thode controle saisie caract�risques
	myctrl = Objet de l'input de saisie
	index = Num�ro de l'input
	IsValNego = Si on vient de DICT�val-etat_ (FALSE) ou de DICT�val-nego_ (TRUE)
**/
function CtrlSaisieValCaract(myctrl, index, IsValNego)
{
	trace("--CtrlSaisieValCaract myctrl : "+myctrl.id+" - index : "+index+" - IsValNego :" + IsValNego+" --")
	
	//la valeur ne peut etre > a 100
	if(myctrl.value >= 1000)
	{
		alert("Saisie incorrecte, la valeur doit �tre < 1000 !");
		myctrl.value = "0.0";
		return false;
	}
	
	ind = index;

	//on regarde si on doit controler la valeur
	if(!IsCtrlCaract)
		return true;
	
	//si pas de caracteristique pas de valeur nego
	if(myctrl.value.trim()=="")
		zonscreenget("�val-etat_"+index+"��8");
	
	if(isNaN(myctrl.value))
	{
		alert("Saisie num�rique obligatoire");
		myctrl.focus();
		return false;
	}

	//Recuperation des valeurs mini et maxi des caracteristiques param�tr�es
	Rsp = RspCodcer; //reponse du get codcer
	valmini = getValue("MINCAR_"+ind, false ,"" ,"", true);
	valmaxi = getValue("MAXCAR_"+ind, false ,"" ,"", true);
	
	if(valmini == "")
		valmini = 0
	else if(valmini != 0)
		valmini = parseFloatStr(valmini)
	
	if(valmaxi == "")
		valmaxi = 0
	else if(valmaxi != 0)
		valmaxi = parseFloatStr(valmaxi)

	//on regarde si la zone est obligatoire, facultative, sans saisie
	if(!IsValNego)
	{
        switch(getValue("TOPCAR_"+ind, false ,"" ,"", true))
		{
			case "1": //Saisie + controle obligatoire	
				if(myctrl.value.trim()=="")
				{
					alert("Saisie obligatoire");
					myctrl.value=0.00;
					return false;
				}

				if(parseFloatStr(myctrl.value) < valmini || parseFloatStr(myctrl.value) > valmaxi)
				{
					
					if(valmini != 0 && valmaxi != 0)
					{
						alert("La valeur doit �tre comprise entre "+valmini+ " et "+valmaxi);
						// return false;
					}

					if(IsCtrlCaract2==true)
					{
						myctrl.value = 0.00;
						try{myctrl.focus()} catch(ex){myctrl.value = valmini}
						
						// alert("ICI - 1")
						
						// if(IsValNego)
						// {
							// setTimeout("$('DICT�val-nego_'+index).focus()");							
						// }
						
						
						// alert("ICI - 11")
						return false;
					}
				}
				break;
				
			case "2": //Saisie facultative mais si Saisie alors controle
				if(myctrl.value.trim()=="")
					return true;
				if(parseFloatStr(myctrl.value)<valmini||parseFloatStr(myctrl.value)>valmaxi)
				{
					if(valmini!=0&&valmaxi!=0)
					{
						alert("La valeur doit �tre comprise entre "+valmini+ " et "+valmaxi);
						// return false;
					}
					if(IsCtrlCaract2==true)
					{
						myctrl.value = "";
						try{myctrl.focus()} catch(ex){}
						
						// alert("ICI - 2")
						
						return false;
					}
				}
				break;
		}


		//On verifie s'il y a du d�classement � �ffectuer
	        //principe  : -> si testsai = 1 alors on est en d�classement > :
                //    dans ce cas, si saisie caracteristic > tx declassement alors on stocke le codcer declas dans codcer _
                //    -> si testsai = 2 alors on est en d�classement < :
                //    dans ce cas, si saisie caracteristic < tx declassement alors on stocke le codcer declas dans codcer
		switch(getValue("TOPDECLAS_"+ind, false ,"" ,"", true))
		{
			case "1":			
				if(parseFloatStr(myctrl.value) > parseFloatStr(getValue("TXDECLAS_"+ind, false ,"" ,"", true)))
				{
					if("<?=$_noconf?>"!="")
					{						
						cer_origine = $("DICT�codcer").value;
						getValue("LIBEL", true ,"CODCER-DECLAS" ,"DICT�codcer_libel", true);
						getValue("STOCK-DECLAS", false ,"" ,"DICT�codcer-ori", true);
						getValue("LIBEL", true ,"STOCK-DECLAS" ,"DICT�codcer-ori_libel", true);
						
						if(getValue("CODCER-DECLAS", false ,"" ,"", true)!="")
							$("DICT�codcer").value=getValue("CODCER-DECLAS", false ,"" ,"", true);
						
						$("DICT�codcer").defaultValue=$("DICT�codcer").value;
                        // R.A.Z. du contrat qui n'est plus forcement li� � la cereale de declassement
                        clearZone('DICT�cer-contrat|DICT�cer-contrat-fil');
						alert("La c�r�ale a �t� d�class�e");
					}
					else
					{
						if(confirm("Proc�der au d�classement ?  norme : "+getValue("TXDECLAS_"+ind, false ,"" ,"", true)+" c�r�ale : "+getValue("CODCER-DECLAS", false ,"" ,"", true))==true)
						{							
							cer_origine = $("DICT�codcer").value;
							getValue("LIBEL", true ,"CODCER-DECLAS" ,"DICT�codcer_libel", true);
							getValue("STOCK-DECLAS", false ,"" ,"DICT�codcer-ori", true);
							getValue("LIBEL", true ,"STOCK-DECLAS" ,"DICT�codcer-ori_libel", true);
							if(getValue("CODCER-DECLAS", false ,"" ,"", true)!="")
								$("DICT�codcer").value=getValue("CODCER-DECLAS", false ,"" ,"", true);
							$("DICT�codcer").defaultValue=$("DICT�codcer").value;
						}
						else
						{
							// alert("ICI - 3")
							return false;
						}
					}
				}
			break;
			case "2":
				if(parseFloatStr(myctrl.value)<parseFloatStr(getValue("TXDECLAS_"+ind, false ,"" ,"", true)))
				{
					if("<?=$_noconf?>"!="")
					{						
							cer_origine = $("DICT�codcer").value;
							getValue("LIBEL", true ,"CODCER-DECLAS" ,"DICT�codcer_libel", true);
							getValue("STOCK-DECLAS", false ,"" ,"DICT�codcer-ori", true);
							getValue("LIBEL", true ,"STOCK-DECLAS" ,"DICT�codcer-ori_libel", true);
							
							if(getValue("CODCER-DECLAS", false ,"" ,"", true) != "")
								$("DICT�codcer").value = getValue("CODCER-DECLAS", false ,"" ,"", true);
							 
							$("DICT�codcer").defaultValue = $("DICT�codcer").value;
                            // R.A.Z. du contrat qui n'est plus forcement li� � la cereale de declassement
                            clearZone('DICT�cer-contrat|DICT�cer-contrat-fil');
							alert("La c�r�ale a �t� d�class�e");
					}
					else
					{
						if(confirm("Proc�der au d�classement ?  norme : "+getValue("TXDECLAS_"+ind, false ,"" ,"", true)+" c�r�ale : "+getValue("CODCER-DECLAS", false ,"" ,"", true))==true)
						{
							cer_origine = $("DICT�codcer").value
							if(getValue("CODCER-DECLAS", false ,"" ,"", true)!="")
								$("DICT�codcer").value=getValue("CODCER-DECLAS", false ,"" ,"", true);
							getValue("LIBEL", true ,"CODCER-DECLAS" ,"DICT�codcer_libel", true);
							getValue("STOCK-DECLAS", false ,"" ,"DICT�codcer-ori", true);
							getValue("LIBEL", true ,"STOCK-DECLAS" ,"DICT�codcer-ori_libel", true);
							$("DICT�codcer").defaultValue=$("DICT�codcer").value;
                            // R.A.Z. du contrat qui n'est plus forcement li� � la cereale de declassement
                            $("DICT�cer-contrat").value = "";
							alert("Attention, c�r�ale d�class�e, norme: "+getValue("TXDECLAS_"+ind, false ,"" ,"", true)+" c�r�ale : "+getValue("CODCER-DECLAS", false ,"" ,"", true));
						}
						else
						{
							// alert("ICI - 4")
							return false;
						}
					}
				}
			break;
		}
	}

	if(IsValNego)
	{
		switch(getValue("TOPCAR_"+ind, false ,"" ,"", true))
		{
			case "1": //Saisie + controle obligatoire
				if(myctrl.value.trim()==""||myctrl.value.trim()=="0.00"||myctrl.value.trim()=="0.0")
					break;
				if(parseFloatStr(myctrl.value)<valmini||parseFloatStr(myctrl.value)>valmaxi)
				{
					alert("La valeur doit �tre comprise entre "+valmini+ " et "+valmaxi);
					// return false;
				}
				break;
			case "2": //Saisie facultative mais si Saisie alors controle
				if(myctrl.value.trim()==""||myctrl.value.trim()=="0.00"||myctrl.value.trim()=="0.0")
					break;
				if(parseFloatStr(myctrl.value)<valmini||parseFloatStr(myctrl.value)>valmaxi)
				{
					alert("La valeur doit �tre comprise entre "+valmini+ " et "+valmaxi);
					return false;
				}
				break;
		}
	}
		
		
		
	// if(getValue("CER-CHRONO", false ,"" ,"" ,true).trim() == "")
		return true;
}

/**
	Controle si la (famille/sous-famille) de la c�r�ale 
	est identique � la (famille/sous-famille) 
	du produit de la cellule.
**/
function ctrlSousFamille()
{
	trace("--ctrlSousFamille--")
	
	var famCer = getField(RspCodcer[1], "FAMCER").trim().toUpperCase();
	var sfmCer = getField(RspCodcer[1], "SFMCER").trim().toUpperCase();
	var famCel = "";
	var sfmCel = "";
	var produit = getField(RspCellule[1],'STO-PRODUIT').trim().toUpperCase();
	var virtuel = getField(RspCellule[1],'LIBEL1-2').trim().toUpperCase();

    if (virtuel == "V") return true;

	var ctrl = false;
	
	// GET CEREAL pour le produit
	var dogFct = "get-cereal�a-cereal"
	var requete  = "CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$('DICT�campagne').value;
		requete += "�REGION�001�codcer�"+produit;
		requete += "�MAGASI�"+$('DICT�magasi').value;
		requete += "�CER-DATSAI�"+$('DICT�cer-datsai').value;
	
	var retourExchange = sendExchangeInvoke("<?=$dogExchange?>",requete,dogFct,"","") 
	if(retourExchange == 'OK')
	{
            famCel = getValeurRsp('FAMCER');
            sfmCel = getValeurRsp('SFMCER');

            if(famCel == famCer && sfmCel == sfmCer)
                ctrl = true
        }
        else
        {
            alert(Rsp[1]);
        }
	
	return ctrl;
}

/**
	Controle effectu� � la validation : 
	On regarde si on saisie un calibrage dans les carract�ristiques.
	TRUE / FALSE
	
	si majPos = true => On met � jour la position de la carract�ristique
**/
function saisieCalibrage(majPos)
{
	// trace("--saisieCalibrage - carOrge : " +carOrge+ " --")
	
	if(!carOrgeSpe)
		return false;
	
	var isCal = false;
	posCalSpe = -1;
	var qteCal = 0;
	
	//On parcourt le Rsp du produit pour regarder les carract�riqtiques param�tr�s
	for(var i=1; i<=14; i++)
	{
		if(getField(RspCodcer[1],'CODCAR_'+i).trim().toUpperCase() == 'CAL')
		{
			isCal = true;
			posCalSpe = i;
			break;
		}
	}
	
	if(!isCal)
		return false;
	
	//On met juste � jour la position
	if(majPos != undefined && majPos == true)
		return;
	
	//On a pas de calibrage
	if(posCalSpe == -1)
		return false
	
	//On regarde si on a saisie un calibrage.
	if(parseFloatStr($('DICT�val-etat_'+posCalSpe).value) > 0)
		return true;
	else
		return false;
}

/**
	Calcule la quantit� d'Orge saisie selon le calibrage.
	(SI carOrgeSpe = TRUE)
	
	SI QTE renseign� => calcul du calibrage pour la qte.
**/
function calcOrgeCalibre(qte)
{
	trace("--calcOrgeCalibre qte : "+qte+" --")
	
	if (!carOrgeSpe) return;
	
	// On met � jour la pos de la carract�ristique
	if (posCalSpe == -1) saisieCalibrage(true);
	
	// On a pas de calibrage => retourne -1
	if (posCalSpe == -1) return -1;
	
	var qteCalibre = 0;
	var calibrage  = parseFloatStr($('DICT�val-etat_' + posCalSpe).value);
	
    if (qte == undefined || qte == "" || (qte != "" && isNaN(qte)))
	{
		// trace("	qte == undefined || qte == '' || (qte != '' && isNaN(qte))")
		
        if(qte == undefined || qte == "")
        {
			// trace("		qte == undefined || qte == ''")
			// trace("		$('DICT�pdsnormes').value : " + $('DICT�pdsnormes').value)
			// trace("		calibrage : " + calibrage)
        	qteCalibre = parseFloatStr($('DICT�pdsnormes').value) * calibrage / 100;
            eecart     = qteCalibre;
            qteCalibre = Math.round(qteCalibre * 100000) / 100000;
            qteCalibre = qteCalibre.toFixed(4)
            ecartArrondis = ecartArrondis + eecart - parseFloat(qteCalibre);
			
			// trace("		qteCalibre : " + qteCalibre)
        }
		else if(qte.toUpperCase() == 'RESTE')
        {
			// trace("		qte.toUpperCase() == 'RESTE'")
			// trace("		deltaQteModifBon : " + deltaQteModifBon)
			// trace("		calibrage : " + calibrage)
			
			// trace('		[1] qteCalibre : ' + qteCalibre)
			
        	qteCalibre = deltaQteModifBon * calibrage / 100;
			
			// trace('		[2] qteCalibre : ' + qteCalibre)
			
            eecartArrondis = qteCalibre;
            qteCalibre = Math.round(qteCalibre * 100000) / 100000;
			
			// trace('		[3] qteCalibre : ' + qteCalibre)
			
            qteCalibre = qteCalibre.toFixed(4);
			
			// trace('		[4] qteCalibre : ' + qteCalibre)
			
            ecartArrondis  = ecartArrondis + eecart - parseFloat(qteCalibre);
			
			// trace("		qteCalibre : " + qteCalibre)
        }

        if (dernierMvt == true)
        {
			// trace("		dernierMvt == true")
			// trace("		qteCalibre : " + qteCalibre)
            qteCalibre = parseFloat(qteCalibre) + ecartArrondis;
			
			// trace('		[5] qteCalibre : ' + qteCalibre)
			
            qteCalibre = qteCalibre.toFixed(4);
			
			// trace('		[6] qteCalibre : ' + qteCalibre)
			
			// trace("		APRES qteCalibre : " + qteCalibre)
        }

		trace("		RETURN qteCalibre : " + qteCalibre)
		return parseFloatStr(qteCalibre);
	}
	else
	{
		// trace("		ELSE")
		
        // Calcul calibrage pour la QTE
        qte = parseFloatStr(qte);
		
		// trace("		qte : " + qte)

        // Pte 24/01/2017 Probleme arrondis. Explication par l'exemple:
        // Le calcul de qteCalibre retourne un nombre avec 4 decimales (132.9075) celui ci est arrondis � 3 d�cimales (132.908)
        // Nous avons donc ajout� 0.0005 tonne . Cette valeur est m�moris�e dans la variable "ecart" et lors du traitement du 
        // dernier mouvement, cet ecart est enlev� (si negatif) ou ajout� (si positif)
        qteCalibre     = qte * 100 / calibrage;
		
		// trace('		[1] qteCalibre : ' + qteCalibre)
		
        eecart         = qteCalibre;
        eecartArrondis = qteCalibre;
        qteCalibre     = Math.round(qteCalibre * 100000) / 100000;
        qteCalibre     = qteCalibre.toFixed(4);
		// trace('		[2] qteCalibre : ' + qteCalibre)
        ecartArrondis  = ecartArrondis + eecart - parseFloat(qteCalibre);

        if (dernierMvt == true)
        {
            qteCalibre    = parseFloat(qteCalibre) + ecartArrondis;
            qteCalibre    = qteCalibre.toFixed(4);
			// trace('		[3] qteCalibre : ' + qteCalibre)
            ecartArrondis = 0;
        }
	}
	
	trace("RETURN qteCalibre : " + qteCalibre)
	return qteCalibre;
}

/** FIN FONCTIONS DE CONTROLE **/
/** Fonctions de validations **/
function validAdrliv()
{
	$('ADRESSE').hide();
	
	/** ---------  O R D R E    D E   S A I S I E --------- **/
	<?if(file_exists(search::searchExe("as_ordre_saisie.js",0)))
	  {
	?>
		try
		{
			if(!modifBon)
			{
				if(zoneSuivanteSpe('CODETIERS'))
					return;						
			}
		} catch(ex){trace("---zoneSuivanteSpe : " + ex)};
	<?}?>
	
	setFocus(TabFocusEntree, "DICT�codauxges");
}

function validExecution()
{
	$('EXECUTION').hide();
	ligneselectionner();
	$('DICT�cer-contrat').focus();
	$('DICT�cer-contrat').select();
}

function validMulti()
{
	if(!ctrlMulti())
		return;
	$('MULTI').hide();
	$('DICT�val-etat_1').focus();
}
/** Fin Fonctions de validations **/
function ligneselectionner()
{
	//retourne le chrono de l'echeance
	if(gridExec.getRowCount() == 0)
	{
		$("EXECUTION").hide();
		try{$("DICT�cer-contrat").focus()}catch(ex){}
		return;
	}
	if(gridExec.getCellText(1, currow2).trim() == "0" && currow2 > -1)
	{
		alert("Probl�me sur le num�ro de ligne.")
		return;
	}
	$("DICT�indice-liv").value = gridExec.getCellText(1, currow2);
	$("EXECUTION").hide();
	try{$("DICT�cer-contrat").focus()}catch(ex){}
}
/*** Fonctions d'�cran ****/
function sortieMulti()
{
	var zones = [];
	for(var i = 1 ; i < 13;i++)
	{
		zones.push("DICT�cellule_" + i);
		zones.push("DICT�cellule_" + i + "_libel");
		zones.push("DICT�rotation_" + i);
		zones.push("DICT�pdsnet_" + i);
		zones.push("DICT�solde-vide_" + i);
		zones.push("DICT�vide-cellule_" + i);
	}
	zones.push("CALC�pdsnet");
	zones.push("CALC�solde");
	zones.push("DICT�cellule");
	clearZone("ALL", zones);
	$('MULTI').hide();
	$('DICT�cellule').focus;
}

function sortieEclatement()
{
	trace("--sortieEclatement--")
	
	keycode = 999;
	// dataGridEclat = [];
	// gridEclat.setCellData(dataGridEclat);
	// gridEclat.setRowCount(0);
	// gridEclat.refresh();
	// razEclatement();
	$('ECLATEMENT').hide();
	
	if(eclatementSPE)
		keycode = 27; //Echap pour sortir de la saisie du grid
}

function razEclatement()
{
	clearZone("DICT�pdsresteini");
}

function sortieExclusion()
{
	$('EXECUTION').hide();
	$('DICT�cer-contrat').focus();
}

function sortieMotif()
{
	// if($('DICT�motif').value.trim() != '')
	// {
		$('MOTIF').hide();
		topSuppr = false;
		topSuppression = false;
	// }
}

function sortieTicket()
{
	if($('DICT�ticket').value.trim()!='')
		$('TICKET').hide();
}

function razEcran(suite)
{
	trace("--razEcran--");
	
	isSaisieEnCours = false;
	modifBon = false;
	oldPdsNorme = 0;
	visu = false;
	
	deltaQteModifBon = 0;
	qteContrat = 0;
	RspContrat = [];
	
	ecartArrondis = 0;
	dernierMvt = false;
	
	listePdsBrutEclat = [];
	
	bonIssuGenenerationPlusMoins = false;
	
	//on debloque le bandeau
	zonscreenget("�magasi��11�cer-datsai��11�campagne��11�codcer��3�magas2��2");
	
	enableZone("DICT�attente", true);
	datejour 	= $("DICT�cer-datsai").value;
	magasi 		= $("DICT�magasi").value;
	magasiLibel = $("DICT�magasi_libel").value;
	campagne 	= $("DICT�campagne").value;
	
	clearscreen();
	
	clearZone("inputF10Fosse");
	
	$("DICT�cellule").title = "";
	
	enableZone("DICT�transp_libel", false);
	
	if(nextZone(TabFocusEntree, "DICT�transp")=="DICT�dest-fin")
		zonscreenget("�chauff�dest-fin�4");
	
	$("DICT�transp_libel").setAttribute("obli", "");
	$("DICT�adr-liv_1").setAttribute("obli", "");
	
	if(campagneSaisie.trim() != "")
		$("DICT�campagne").value = campagneSaisie;
	else if(campagne.trim() != "")
		$("DICT�campagne").value = campagne.trim();
	else
		$("DICT�campagne").value = "<?=$annee?>".trim();
	
	$("DICT�cer-datsai").value 		= datejour;
	$("DICT�magasi").value 			= magasi;
	$("DICT�magasi_libel").value 	= magasiLibel;
	$("DICT�solde-vide").value 		= 0.0;
	
	RspVoe			= [];
	RspContrat		= [];
	RspCellule		= [];
	RspCodmvt		= [];
	RspCodcer		= [];
	RspContratProd	= [];
	RspNumContrat	= [];
	
	
	
	codauxOrigine 	= "";
	typauxOrigine 	= "";
	retourRowid 	= "";
	poidsMin 		= 0;
	poidsMax 		= 0;
	poidsCST 		= 0;
	
	
	/** ---------  O R D R E    D E   S A I S I E --------- **/
	<?if(file_exists(search::searchExe("as_ordre_saisie.js",0)))
	  {
	?>
		try
		{
			razVariablesSpe();
		} catch(ex){trace("---zoneSuivanteSpe : " + ex)}; 
	<?}?>
	
	/*Raz variables ordre saisie SPE
	try
	{
		razVariablesSpe();
	} catch(ex){"---ordre saisie : " + ex};*/

	$("divOnglets").hide();
	$("DATHEUBON").hide();
	
	if($("btn_ouv_grid").className == "navImg tailleImg15 downImg15")
	{
		ouvGrid();
	}

	//on r�initialise le buffer
	initbufferscreen();
	zonscreenget("�cellule��1�");
	
	if(topSwitchLot)
	{
		// zonscreenget("�cellule�numlot�4");
		topSwitchLot=false
	}
	
	dataPdsNormes = [];
	dataGridEclat = [];
	
	gridEclat.setCellData(dataGridEclat);
	gridEclat.setRowCount(dataGridEclat.length);
	gridEclat.refresh();

	if(suite)
	{
		bonrechercher();
	}
	else
	{
		gridBons.selectRow(-1);
		enableZone("ALL", false)
		zonscreenget("�magasi��11�cer-datsai��11�campagne��11�codcer��3");
		enableZone("DICT�attente", true);
		
		if(gridBons.getRowCount() > 0)
			setTimeout("gridBons.focus()")
		else
			setFocus(TabFocusEntree, "FIRST");
	}
}

function echap()
{
	enableZone("ALL", true, zonesRecherche);
	gridBons.setCellData([]);
	gridBons.setRowCount(0);
	gridBons.selectRow(-1);
	gridBons.refresh();
	$("divOnglets").hide();
	$("DATHEUBON").hide();

	setFocus(TabFocusEntree, "FIRST");
}

function clearscreen()
{
	var inputs = document.getElementsByTagName("INPUT");
	for(var i = 0; i < inputs.length;i++)
	{
		inputs[i].value = "";
		inputs[i].defaultValue = "";
	}
}
/**Fin Fonctions d'�cran **/

/** FCTS PARAMETRAGES **/

var topSwitchLot = false
/**
	Param�trage des zones de saisie � l'�cran lors de la saisie du codcer
**/
function ParamAfficheZonesCereale()
{
	trace("--ParamAfficheZonesCereale--");
	
	//	EXEMPLE PARAMETRAGE : MVT/PDT (fiche c�r�al) et TABLE MAGASINS 
	//								+-------------------------------------------------------+
	//								|					TABLE DES MAGASINS					|
	//								+-------------------+-------------------+---------------+
	//								|	MVT/PDT			|	obligatoire		|	pas saisie	|
	//----------+-------------------+-------------------+-------------------+---------------+
	//			|	pas saisie		|	pas saisie		|	pas saisie		|	pas saisie	|
	//			+-------------------+-------------------+-------------------+---------------+
	//	MVT/PDT	|	obligatoire		|	obligatoire		|	obligatoire		|	pas saisie	|
	//			+-------------------+-------------------+-------------------+---------------+
	//			|	facultatif		|	facultatif		|	obligatoire		|	pas saisie	|
	//----------+-------------------+-------------------+-------------------+---------------+
	
	//	EXEMPLE PARAMETRAGE : CODCER / CODMVT 
	//								+-------------------------------------------------------+
	//								|					CODE MOUVEMENT						|
	//								+-------------------+-------------------+---------------+
	//								|	pas saisie		|	facultatif		|	obligatoire	|
	//----------+-------------------+-------------------+-------------------+---------------+
	//			|	pas saisie		|	pas saisie		|	pas saisie		|	pas saisie	|
	//			+-------------------+-------------------+-------------------+---------------+
	//	CEREALE	|	obligatoire		|	pas saisie		|	facultatif		|	facultatif	|
	//			+-------------------+-------------------+-------------------+---------------+
	//			|	facultatif		|	pas saisie		|	facultatif		|	obligatoire	|
	//----------+-------------------+-------------------+-------------------+---------------+
	
	Rsp = RspCoderMvt;
	zonscreenget("�cellule��9�e-cellule��9");
	
	if(topSwitchLot)
	{
		topSwitchLot=false;
	}
	
	
	
	//---- T Y P E   D E   C O N T R A T -------------------------------------------------------------------
	if(getField(RspCoderMvt[1],'NATCON') == "0")
	{
		topnatcon = false
		zonscreenget("�natcon��15");  //invisible
	}
	else if(getField(RspCoderMvt[1],'NATCON') != "1")
	{
		zonscreenget("�natcon��8");  //visible
		topnatcon = true
	}
	else
	{
		zonscreenget("�natcon��11");  //obligatoire
		topnatcon = true
	}

	//---- C O D E   T I E R S -----------------------------------------------------------------------------
	if(getField(RspCoderMvt[1],'TOPELEMENT_1') == "0")
		zonscreenget("�typaux��2�codauxges��2�magas2��8");  //invisible
	else if(getField(RspCoderMvt[1],'TOPELEMENT_1') != "1")
		zonscreenget("�typaux��8�codauxges��8�magas2��15");  //visible
	else
		zonscreenget("�typaux��11�codauxges��11�magas2��15");  //obligatoire
	
	//---- T I E R S   C H A U F F E U R -------------------------------------------------------------------
	if(getField(RspCoderMvt[1],'TIERS-CHAUF') == "0")  //invisible
		zonscreenget("�typaux_chauff��15�codaux_chauff��15");
	else if(getField(RspCoderMvt[1],'TIERS-CHAUF') != "1")//visible
		zonscreenget("�typaux_chauff��8�codaux_chauff��8");
	else													//obligatoire
		zonscreenget("�typaux_chauff��11�codaux_chauff��11");
	
	//---- C O D E   C E L L U L E --------------------------------------------------------------------------
	if(getField(RspMagasi[1],'GESLOT') == "2")
		zonscreenget("�cellule��15");  //invisible
	else if(getField(RspCoderMvt[1],'TOPELEMENT_2') == "0")
		zonscreenget("�cellule��15");  //invisible
	else if(getField(RspCoderMvt[1],'TOPELEMENT_2') != "1")
		zonscreenget("�cellule��8");  //visible
	else
		zonscreenget("�cellule��11");  //obligatoire

    //---- N U M E R O   E C H A N T I L L O N --------------------------------------------------------------
	if(getField(RspCoderMvt[1],'MVTCER-ECHANTILLON') == "0" || getField(RspCoderMvt[1],'MVTCER-ECHANTILLON') == "")
		zonscreenget("�echantillon��15");  //invisible
	else if(getField(RspCoderMvt[1],'MVTCER-ECHANTILLON') != "1")
		zonscreenget("�echantillon��8");  //visible
	else
	{
		zonscreenget("�echantillon��11");  //obligatoire
	}

	//---- T Y P E   T R A N S P O R T ----------------------------------------------------------------------
	if(getField(RspCoderMvt[1],'TOPELEMENT_3') == "0")
		zonscreenget("�typtrp-ctr��15");  //invisible
	else if(getField(RspCoderMvt[1],'TOPELEMENT_3') != "1")
		zonscreenget("�typtrp-ctr��8");  //visible
	else
		zonscreenget("�typtrp-ctr��11");  //obligatoire
	
	//---- C O D E   T R A N S P O R T E U R ----------------------------------------------------------------
	if(getField(RspCoderMvt[1],'TOPELEMENT_4') == "0")
		zonscreenget("�transp��15");  //invisible
	else if(getField(RspCoderMvt[1],'TOPELEMENT_4') != "1")
		zonscreenget("�transp��8");  //visible
	else
		zonscreenget("�transp��11");  //obligatoire

	//---- C O D E   I M M A T R I C U L A T I O N ----------------------------------------------------------
	if(getField(RspCoderMvt[1],'TOPELEMENT_5') == "0")
		zonscreenget("�immatr��15");  //invisible
	else if(getField(RspCoderMvt[1],'TOPELEMENT_5') != "1")
		zonscreenget("�immatr��8");  //visible
	else
		zonscreenget("�immatr��11");  //obligatoire

	//---- M A G A S I N   R E C I P R O Q U E --------------------------------------------------------------
	if(getField(RspCoderMvt[1],'TOPELEMENT_6') == "0")
		zonscreenget("�magas2��15");  //invisible
	else if(getField(RspCoderMvt[1],'TOPELEMENT_6') != "1")
		zonscreenget("�typaux��15�codauxges��15�magas2��8");  //visible
	else
		zonscreenget("�typaux��15�codauxges��15�magas2��11");  //obligatoire

	//---- C O D E   E - C E L L U L E ----------------------------------------------------------------------
	if(getField(RspCoderMvt[1],'TOPELEMENT_7') == "0")
		zonscreenget("�e-cellule��15");  //invisible
	else if(getField(RspCoderMvt[1],'TOPELEMENT_7') != "1")
		zonscreenget("�e-cellule��8");  //visible
	else
		zonscreenget("�e-cellule��11");  //obligatoire

	//---- N U M P E S E E ----------------------------------------------------------------------------------
	if(getField(RspCoderMvt[1],'TOPELEMENT_9') == "0")
		zonscreenget("�numpesee��15");  //invisible
	else if(getField(RspCoderMvt[1],'TOPELEMENT_9') != "1")
		zonscreenget("�numpesee��8");  //visible
	else
		zonscreenget("�numpesee��11");  //obligatoire
	
	//---- N U M   B L   I N T E R N E ----------------------------------------------------------------------
	if(getField(RspCoderMvt[1],'TOPELEMENT_11') == "0")
		zonscreenget("�bl-interne��15");  //invisible
	else if(getField(RspCoderMvt[1],'TOPELEMENT_11') != "1")
		zonscreenget("�bl-interne��8");  //visible
	else
		zonscreenget("�bl-interne��11");  //obligatoire

	//---- C O D E   C O N T R A T --------------------------------------------------------------------------
	if(getField(RspCoderMvt[1],'TOPELEMENT_10') == "0")
		zonscreenget("�cer-contrat��15")  //invisible
	else if(getField(RspCoderMvt[1],'TOPELEMENT_10') != "1")
			zonscreenget("�cer-contrat��8")  //visible
		else
			zonscreenget("�cer-contrat��11")  //obligatoire
	
	//---- C O D E   C H A U F F E U R ----------------------------------------------------------------------
	if(getField(RspCoderMvt[1],'TOPELEMENT_12') == "0")
		zonscreenget("�chauff��15")  //invisible
	else if(getField(RspCoderMvt[1],'TOPELEMENT_12') != "1")
		zonscreenget("�chauff��8")  //visible
	else
		zonscreenget("�chauff��11")  //obligatoire

	
	//---- C O D E   P A R C E L L E ------------------------------------------------------------------------
	if(getField(RspCoderMvt[1],'MVTCER-SAIPAR') == "0" || getField(RspCoderMvt[1],'MVTCER-SAIPAR') == "")
		zonscreenget("�codeparc��2")  //invisible
	else if(getField(RspCoderMvt[1],'MVTCER-SAIPAR') != "1")
			zonscreenget("�codeparc��8")  //visible
		else
			zonscreenget("�codeparc��11")  //obligatoire

	//---- C E R E A L E   S T O C K ------------------------------------------------------------------------
	if(getField(RspCoderMvt[1],'MVTCER-MODCER') == "" || getField(RspCoderMvt[1],'MVTCER-MODCER') == "0")
		zonscreenget("�codcer-ori��15")  //invisible
	else
		zonscreenget("�codcer-ori��8")  //visible
	
	
	//---- C O M M U N E ------------------------------------------------------------------------------------
	if(getField(RspCoderMvt[1],'MVTCER-SAICOM') == "" || getField(RspCoderMvt[1],'MVTCER-SAICOM') == "0")
		zonscreenget("�comonic��15")  //invisible
	else if(getField(RspCoderMvt[1],'MVTCER-SAICOM') != "1")
			zonscreenget("�comonic��8")  //visible
	else
		zonscreenget("�comonic��11")  //obligatoire
	
	//---- N U M L O T - (0 = Pas de saisie, 1 = Obligatoire, 2 = Facultatif) -------------------------------
	if(getField(RspCoderMvt[1],'MVTCER-LOT-APPORT') == "0" || getField(RspCoderMvt[1],'MVTCER-LOT-APPORT') == "")
		zonscreenget("�numlot��15")  // Invisible
	else
	{
		topSwitchLot = true
		if(getField(RspCoderMvt[1],'MVTCER-LOT-APPORT') != "1")
			zonscreenget("�numlot��8");  // Visible
		else
			zonscreenget("�numlot��11"); // Obligatoire
		enableZone("DICT�codlot", false);
	}

    //---- N U M E R O   S C E L L E ------------------------------------------------------------------------
	if(getField(RspCoderMvt[1],'SCELLE') == "0" || getField(RspCoderMvt[1],'SCELLE') == "")
    	zonscreenget("�nscelle1��15�nscelle2��15�nscelle3��15");  // Invisible
	else if(getField(RspCoderMvt[1],'SCELLE') != "1")
		zonscreenget("�nscelle1��8�nscelle2��8�nscelle3��8")  // Visible
	else
		zonscreenget("�nscelle1��11�nscelle2��11�nscelle3��11")  // Obligatoire

	Rsp = RspOperat.clone();
	
	//si op�rateur d�p�t le top-validation est grise
	if(getField(RspOperat[1],'KES-OPMAIT') == "N")
		zonscreenget("�CERVALID��9");
	
	//contr�le de la zone contrat
	ctrlContrat();
}

function ParamAfficheContratFiliere()
{
try{
	if("<?=$_filiere?>"=="")
	{
		RspTemp=Rsp.clone();
		Rsp=RspCoderMvt;
		//le code tiers
		if(getValue("FILIERE",false ,"" ,"" ,true)=="1")
		{
			// trace("-> 0")
			zonscreenget("�cer-contrat-fil��8�cer-contrat-fil��11")  //visible obligatoire
			$("DICT�numoex").style.display="none";
			$("DICT�indice-liv").style.display="none";
		}
		else if(getValue("FILIERE",false ,"" ,"" ,true)=="2")
		{
			// trace("-> 2")
			zonscreenget("�cer-contrat-fil��8")  //visible
			$("DICT�numoex").style.display="none";
			$("DICT�indice-liv").style.display="none";
		}
		else
		{
			// trace("-> 0")
			zonscreenget("�cer-contrat-fil�"+"�2")  //invisible
			$("DICT�numoex").style.display="";
			$("DICT�indice-liv").style.display="";
		}
		Rsp=RspTemp;
	}
}
catch(err){trace(err)}
}

function affDatationBon()
{
	if($("DATHEUBON").style.display == "none")
	{
		$("DATHEUBON").show();
		if($("DICT�datbon").className == "dogskininputreadonly")
		{
			$("DICT�heubon").focus();
			$("DICT�heubon").select();
		}
		else
		{
			$("DICT�datbon").focus();
			$("DICT�datbon").select();
		}
	}
	else
	{
		// $("DATHEUBON").hide();
	}
}

function daterBon()
{

	if(validInput($("DICT�heubon")) && validInput($("DICT�datbon")))
		$("DATHEUBON").hide();
}

function recupRotationCourante(num)
{
	trace("--recupRotationCourante num : "+num+" --")
	
	//ON v�rifie si on a une cellule virtuelle
	if(setValueFromKeyword("LIBEL1-2", false, "", "", true).trim().toUpperCase() == "V")
	{
		clearZone("DICT�rotation|DICT�rot-produit|DICT�rot-produit_libel|DICT�capacite|DICT�stock-cel");
		return true;
	}
	
	dogFonction = "get-rotation�a-rotation";
	
	var cellule = $("DICT�cellule").value.trim();
	if(num > 0)
		cellule = $("DICT�cellule_" + num).value.trim();
	
	inputParameter = "CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + cellule + "�CAMPAGNE�" + $("DICT�campagne").value;
	inputParameter += "�CODCER�" + $("DICT�codcer").value.trim();
	inputParameter += "�CODMVT�" + $("DICT�codmvt").value.trim();
	
	if(($("DICT�datbon").value != "" && $("DICT�heubon").value != "") || "<?=$_modifCell?>".trim().toUpperCase() == "YES")
	{
		if("<?=$_modifCell?>".trim().toUpperCase() == "YES")
		{
			$("DICT�datbon").value = "<?=date("D/m/Y")?>";
			$("DICT�heubon").value = "<?=date("h:I:s")?>";
		}
		inputParameter += "�DATE�" + $("DICT�datbon").value.trim();
		inputParameter += "�HEURE�" + $("DICT�heubon").value.trim();
	}
	
	retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "" , "" );
	if(retourExchange == "NODATA")
	{
		if($("DICT�heubon").value != "")
		{
			alert("Pas de rotation � cette date et heure !");
			$("DICT�cellule").value = "";
			$("DICT�cellule").title = "";
			$("DICT�cellule").defaultValue = "";
			$("DICT�cellule").focus();
			$("DICT�cellule").select();
			return false;
		}
		else
		{
			//cr�er une rotation en suivant
			if(confirm("Aucune rotation ouverte, d�sirez-vous en ouvrir une ?"))
			{
				if(num > 0)
				{
					var dogFonction = "maj-rotation�a-rotation";
					var inputParameter  = "CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $("DICT�cellule_" + num).value.trim();
						inputParameter += "�MOTCLE�UPDATE�PRODUIT�" + $("DICT�codcer").value.trim() + "�CAMP�" + $("DICT�campagne").value.trim() + "�CONTROL�OUI";
					var retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "", "");
				}
				else
				{
					var dogFonction = "maj-rotation�a-rotation";
					var inputParameter  = "CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $("DICT�cellule").value.trim();
						inputParameter += "�MOTCLE�UPDATE�PRODUIT�" + $("DICT�codcer").value.trim() + "�CAMP�" + $("DICT�campagne").value.trim() + "�CONTROL�OUI";
					var retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "", "");
				}
				
				if(retourExchange != "OK")
					alert(Rsp[1]);
				
				if(num > 0)
				{
					$("DICT�stock-cel_" + num).value = "0.0";
					setValueFromKeyword("ROTATION", false, "", "DICT�rotation_" + num, true);
				}
				else
				{
					setValueFromKeyword("ROTATION", false, "", "DICT�rotation", true);
					setValueFromKeyword("STOCK", false, "", "DICT�stock-cel", true);
					
					if(setValueFromKeyword("PRODUIT", false, "", "", true).trim() != "")
						setValueFromKeyword("PRODUIT", false, "", "DICT�rot-produit", true);
					
					if(setValueFromKeyword("LIBEL", true, "PRODUIT", "", true).trim() != "")
						setValueFromKeyword("LIBEL", true, "PRODUIT", "DICT�rot-produit_libel", true);
				}
				
				nouvelleRotation = true;
			}
			else
			{
				$("DICT�cellule").value = "";
				$("DICT�cellule").focus();
				$("DICT�cellule").select();
				return false;
			}
		}
	}
	else if(retourExchange == "ERROR")
	{
		if($("DICT�heubon").value != "" && Rsp[1].trim() == "NOROTAPRES")
		{
			//cr�er une rotation en suivant
			if(confirm("Aucune rotation ouverte, d�sirez-vous en ouvrir une ?"))
			{
				if(num > 0)
				{
					var dogFonction = "maj-rotation�a-rotation";
					var inputParameter = "CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $("DICT�cellule_" + num).value.trim() +
										 "�MOTCLE�UPDATE�PRODUIT�" + $("DICT�codcer").value.trim() + "�CAMP�" + $("DICT�campagne").value.trim() + "�CONTROL�OUI";
					var retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "", "");
				}
				else
				{
					var dogFonction = "maj-rotation�a-rotation";
					var inputParameter = "CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magasi").value.trim() + "�CELLULE�" + $("DICT�cellule").value.trim() +
										 "�MOTCLE�UPDATE�PRODUIT�" + $("DICT�codcer").value.trim() + "�CAMP�" + $("DICT�campagne").value.trim() + "�CONTROL�OUI";
					var retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "", "");
				}
				
				if(retourExchange != "OK")
					alert(Rsp[1]);

				if(num > 0)
				{
					$("DICT�stock-cel_" + num).value = "0.0";
					setValueFromKeyword("ROTATION", false, "", "DICT�rotation_" + num, true);
				}
				else
				{
					setValueFromKeyword("ROTATION", false, "", "DICT�rotation", true);
					setValueFromKeyword("STOCK", false, "", "DICT�stock-cel", true);
					if(setValueFromKeyword("PRODUIT", false, "", "", true).trim() != "")
						setValueFromKeyword("PRODUIT", false, "", "DICT�rot-produit", true);
					if(setValueFromKeyword("LIBEL", true, "PRODUIT", "", true).trim() != "")
						setValueFromKeyword("LIBEL", true, "PRODUIT", "DICT�rot-produit_libel", true);
				}

				nouvelleRotation = true;
				
				return true;
			}
			else
			{
				$("DICT�cellule").value = "";
				$("DICT�cellule").title = "";
				$("DICT�cellule").defaultValue = "";
				$("DICT�cellule").focus();
				$("DICT�cellule").select();
				return false;
			}
		}
		else
		{
			if(Rsp[1].trim() == "ROTOUVDIFFCAMP")
				alert("Il existe une rotation ouverte sur une campagne diff�rente.\nImpossible d'affecter ce bon sur cette cellule !");
			
			if(num > 0)
			{
				$("DICT�cellule_"  + num).value = "";
				$("DICT�cellule_"  + num).defaultValue = "";
				$("DICT�rotation_" + num).value = ""
				$("DICT�rotation_" + num).defaultValue = ""
			}
			else
			{
				$("DICT�cellule").value = "";
				$("DICT�cellule").defaultValue = "";
				$("DICT�rotation").value = ""
				$("DICT�rotation").defaultValue = ""
				$("DICT�rot-produit").value = "";
				$("DICT�rot-produit_libel").value = "";
			}
			
			if(Rsp[1].trim() != "ROTOUVDIFFCAMP")
				alert(Rsp[1]);
		}
		return false
	}
	
	if(setValueFromKeyword("MESSAGE-FAM", false, "", "", true).trim() != "")
		alert(setValueFromKeyword("MESSAGE-FAM", false, "", "", true));
	
	if(setValueFromKeyword("MESSAGE-BIO", false, "", "", true).trim() != "")
		if(!confirm(setValueFromKeyword("MESSAGE-BIO", false, "", "", true)))
			return false;
	
	if(num > 0)
		setValueFromKeyword("ROTATION", false, "", "DICT�rotation_" + num, true);
	else
	{
		setValueFromKeyword("ROTATION", false, "", "DICT�rotation", true);
		setValueFromKeyword("STOCK", false, "", "DICT�stock-cel", true);
		if(setValueFromKeyword("PRODUIT", false, "", "", true).trim() != "")
			setValueFromKeyword("PRODUIT", false, "", "DICT�rot-produit", true);
		if(setValueFromKeyword("LIBEL", true, "PRODUIT", "", true).trim() != "")
			setValueFromKeyword("LIBEL", true, "PRODUIT", "DICT�rot-produit_libel", true);
	}

	RspRotation = Rsp.clone();
	return true;
}

function recupRotationCouranteCelluleReciproque()
{
	trace("--recupRotationCouranteCelluleReciproque--")
	
	//ON v�rifie si on a une cellule virtuelle
	if(setValueFromKeyword("LIBEL1-2", false, "", "", true).trim().toUpperCase() == "V")
	{
		// clearZone("DICT�rotation|DICT�rot-produit|DICT�rot-produit_libel|DICT�capacite|DICT�stock-cel");
		return true;
	}
	
	dogFonction = "get-rotation�a-rotation";
	var t_codmvt = getField(RspCodmvt[1], "MVT-REC");
	
	if(t_codmvt.trim() == "")
		t_codmvt = $("DICT�codmvt").value.trim();
	
	var cellule = $("DICT�e-cellule").value.trim();
	inputParameter = "CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magas2").value.trim() + "�CELLULE�" + cellule + "�CAMPAGNE�" + $("DICT�campagne").value;
	inputParameter += "�CODCER�" + $("DICT�codcer").value.trim();
	inputParameter += "�CODMVT�" + t_codmvt;
	
	if(($("dateRec").value != "" && $("heureRec").value != ""))
	{
		inputParameter += "�DATE�" + $("dateRec").value.trim();
		inputParameter += "�HEURE�" + $("heureRec").value.trim();
	}
	
	retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "" , "" );
	if(retourExchange == "NODATA")
	{
		if($("heureRec").value != "")
		{
			alert("Pas de rotation � cette date et heure !");
			$("DICT�e-cellule").focus();
			$("DICT�e-cellule").select();
			return false;
		}
	}
	else if(retourExchange == "ERROR")
	{
		if(Rsp[1].trim() == "NOROTAPRES")
		{
			//cr�er une rotation en suivant
			if(confirm("Aucune rotation ouverte, d�sirez-vous en ouvrir une ?"))
			{
				var dogFonction = "maj-rotation�a-rotation";
				var inputParameter = "CODSOC�<?=$c_codsoc?>�MAGASI�" + $("DICT�magas2").value.trim() + "�CELLULE�" + $("DICT�e-cellule").value.trim() +
									 "�MOTCLE�UPDATE�PRODUIT�" + $("DICT�codcer").value.trim() + "�CAMP�" + $("DICT�campagne").value.trim() + "�CONTROL�OUI";
			    inputParameter += "�DATOUV�" + $("dateRec").value;
				inputParameter += "�HEUOUV�" + $("heureRec").value;
				
				var retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "", "");
				
				if(retourExchange != "OK")
				{
					alert(Rsp[1]);
					return false;
				}
				setValueFromKeyword("ROTATION", false, "", "DICT�e-rotation", true);
				return true;
			}
			else
			{
				clearZone('DICT�e-cellule|DICT�e-cellule_libel');
				return false;
			}
		}
		else
		{
			if(Rsp[1].trim() == "ROTOUVDIFFCAMP")
			{
				alert("Il existe une rotation ouverte sur une campagne diff�rente.\nImpossible d'affecter ce bon sur cette cellule !");
				return false;
			}
			if(Rsp[1].trim() != "ROTOUVDIFFCAMP")
			{
				alert(Rsp[1]);
				return false;
			}
		}
		return false
	}
	
	setValueFromKeyword("ROTATION", false, "", "DICT�e-rotation", true);
	return true;
}
/** FIN FCTS PARAMETRAGES **/


function lecturePontBascule(obj,opt2)
{
//BRUT|NET|TARE|TICKET|DATE|HEURE|STATUT|BRUT|NETSIGNE|BRUTSIGNE|TARESIGNE
	
	if(!isSaisieEnCours)
		return false;
	
	//tetrasem
    <?php if($ident=="230"){?>
		strId = obj.id;
		
		if(((strId=="DICT�pdsbrut" && dsd_1 != "") || (strId == "DICT�tare" && dsd_2 != ""))
			&& !confirm("Attention demande d'acquisition d'un nouveau poids. Confirmer? "))
				return;
		
		if(opt2){
			var pl = new SOAPClientParameters();
			var retourInvoke = SOAPClient.invoke("<?=search::searchExe("ao_askpoids_w3c.php",1)?>?&_value=TRUE&_inifile=<?=$xcspoolPath.'/pesee/poids.txt'?>&", "", pl, false);
			//$("ctldeal").askPoids('<?echo ($_reppesee2!="")?$_reppesee2:"C:\\\\pesee2";?>');
			setTimeout("setnumpesee230(true);ctrlpdsnormes()",1000);
		}else{
			var pl = new SOAPClientParameters();
			var retourInvoke = SOAPClient.invoke("<?=search::searchExe("ao_askpoids_w3c.php",1)?>?&_value=TRUE&_inifile=<?=$xcspoolPath.'/pesee/poids.txt'?>&", "", pl, false);
			//$("ctldeal").askPoids('<?echo ($_reppesee!="")?$_reppesee:"C:\\\\pesee";?>');
			setTimeout("setnumpesee230();ctrlpdsnormes()",1000);
		}
		
		return;
	<?}?>

    <?php if($ident=="710"){?>
		// $('ctldeal').Telecharge('file:\\\\tsclient\\C\\pesee\\poids.txt','T:\\DealTempo\\poids.txt');
		$('ctldeal').Telecharge('file:\\\\tsclient\\C\\ceres\\poids.txt','T:\\DealTempo\\poids.txt');
		obj.value = parseFloatStr(Entry(1,$('ctldeal').GetPoids('T:\\DealTempo'),'|').replace(String.fromCharCode(160),''))/1000;
		setnumpesee();
		ctrlpdsnormes();
	<?php }elseif($ident=="333"){?>
	
	strId = obj.id
	
	if(((strId=="DICT�pdsbrut" && dsd_1 != "")||(strId=="DICT�tare" && dsd_2 != ""))
		&& !confirm("Attention demande d'acquisition d'un nouveau poids. Confirmer? ")){
		return;
	}
	
	if(opt2){
		$("ctldeal").askPoids('<?echo ($_reppesee2!="")?$_reppesee2:"C:\\\\pesee2";?>');
		setTimeout("setnumpesee333(true);ctrlpdsnormes()",1000);
	}else{
		$("ctldeal").askPoids('<?echo ($_reppesee!="")?$_reppesee:"C:\\\\pesee";?>');
		setTimeout("setnumpesee333();ctrlpdsnormes()",1000);
	}
	
	<?}else{?>
		obj.value = parseFloatStr(Entry(1, $("ctldeal").GetPoids('C:\\pesee'),'|'))/1000;
		obj.setAttribute("buffer2", obj.value);
		setnumpesee();
		ctrlpdsnormes();
	<?}?>
}


function lectureSupervision(obj)
{
	if(!isSaisieEnCours)
		return false;
	Rsp = RspMagasi;
	var nomFTP  = getValue("FTP-NOM", false, "", "", true);
	var userFTP = getValue("FTP-USER", false, "", "", true);
	var pwdFTP  = getValue("FTP-MDP", false, "", "", true);
	var repFTP  = getValue("FTP-REP", false, "", "", true);
	var portFTP = getValue("FTP-PORT", false, "", "", true);
	if(portFTP == "")
		portFTP = "21";
	var chronoBon = cer_chrono.trim();
	var url = "<?=$urlLectureFTP?>";
	new Ajax.Request(url, {
		method: 'post',
		postBody: "_serveur=" + nomFTP + "&_user=" + userFTP + "&_pwd=" + pwdFTP + "&_repertoire=" + repFTP + "&_port=" + portFTP + "&_chrono=" + chronoBon,
		asynchronous: false,	// false : envoie synchrone
		onSuccess: function(transport, json)
		{
			var retour = transport.responseText.trim();
			var retour_s = retour.split("|")
			if(retour_s.length > 1)
			{
				//Gestion d'erreur
				switch(retour_s[0])
				{
					case "BADSERVER" :
						alert("Impossible de se connecteur au serveur : " + retour_s[1]);
					break;

					case "BADUSER" :
						alert("Mauvais utilisateur/mot de passe");
					break;

					case "BADREPERTORY" :
						alert("R�pertoire introuvable : " + retour_s[1] + " !");
					break;

					case "BADFILE" :
						alert("Impossible de trouver le fichier suivant : " + retour_s[1] + " !");
					break;
				}
				return false;
			}
			else
			{
				retour_s = retour.split(",");
				/*if($("DICT�cellule").value.trim()==""){
					if(retour_s[0].trim() != ""){
						$("DICT�cellule").value = retour_s[0];
						if(retour_s[0].trim() != "") dogexchange($("DICT�cellule"), "GET", true);
					}
				}*/
				$("DICT�tare").value = retour_s[1];
				validInput($("DICT�tare"), "");
				//on supprime
				new Ajax.Request(url, {
					method: 'post',
					postBody: "_serveur=" + nomFTP + "&_user=" + userFTP + "&_pwd=" + pwdFTP + "&_repertoire=" + repFTP + "&_port=" + portFTP + "&_chrono=" + chronoBon + "&_kill=O",
					asynchronous: false,	// false : envoie synchrone
					onSuccess: function(transport, json)
					{
						var retour = transport.responseText.trim();
					}
				});

			}
		}
	});

}

function setnumpesee()
{
	if($("DICT�numpesee").value.trim()==""){
		$("DICT�numpesee").value = cer_chrono;
	}
}
function setnumpesee333(opt2,opt){
	if(opt2)
		strGetPoids = $('ctldeal').GetPoids('<?echo ($_reppesee2!="")?$_reppesee2:"C:\\\\pesee2";?>');
	else
		strGetPoids = $('ctldeal').GetPoids('<?echo ($_reppesee!="")?$_reppesee:"C:\\\\pesee";?>');
	if(Entry(7, strGetPoids,'|')!="I"){
		if(opt!=true)
			setTimeout("setnumpesee333(true,true);ctrlpdsnormes()",1000);
		else
			alert("Poids non valide :("+Entry(7, strGetPoids,'|')+")");
		return
	}
	$(strId).value = parseFloatStr(Entry(1, strGetPoids,'|'))/1000;
	$(strId).setAttribute("buffer2", parseFloatStr(Entry(1, strGetPoids,'|'))/1000);

	if($(strId).value.trim()==""){
		alert("Probl�me lors de la connexion!")
		return;
	}
	if(parseFloatStr($(strId).value.trim())==0){
		alert("Probl�me lors de la lecture de la pes�e!")
		return;
	}

	if(strId=="DICT�pdsbrut"){
		dsd_1 = Entry(4, strGetPoids,'|')
		datePesee_1 = Entry(5, strGetPoids,'|')
        var s = Entry(6, strGetPoids,'|') + "000000"
		heurePesee_1 = s.substr(0,2)+":"+s.substr(2,2)+":"+s.substr(4,2)
	}
	if(strId=="DICT�tare"){
		dsd_2 = Entry(4, strGetPoids,'|')
		datePesee_2 = Entry(5, strGetPoids,'|')
        var s = Entry(6, strGetPoids,'|') + "000000"
		heurePesee_2 = s.substr(0,2)+":"+s.substr(2,2)+":"+s.substr(4,2)
	}
	if($("DICT�numpesee").value.trim()=="")
		$("DICT�numpesee").value = cer_chrono;
	//alert(strId)
	validInput($(strId));

}

function setnumpesee230(opt2,opt){
	$('ctldeal').Telecharge('<?=$xcspoolRelPath?>pesee/poids.txt?'+Math.random(),'<?echo ($_reppesee!="")?$_reppesee."\\poids.txt":"C:/pesee/poids.txt";?>');
	strGetPoids = $('ctldeal').GetPoids('<?echo ($_reppesee!="")?$_reppesee:"C:\\\\pesee";?>');
	/*if(Entry(7, strGetPoids,'|')!="I"){
		if(opt!=true)
			setTimeout("setnumpesee230(true,true);ctrlpdsnormes()",1000);
		else
			alert("Poids non valide :("+Entry(7, strGetPoids,'|')+")");
		return
	}*/
	$(strId).value = parseFloatStr(Entry(1, strGetPoids,'|'))/1000;
	$(strId).setAttribute("buffer2", parseFloatStr(Entry(1, strGetPoids,'|'))/1000);

	if($(strId).value.trim()==""){
		alert("Probl�me lors de la connexion!")
		return;
	}
	if(parseFloatStr($(strId).value.trim())==0){
		alert("Probl�me lors de la lecture de la pes�e! Poids=0")
		return;
	}

	if(strId=="DICT�pdsbrut"){
		dsd_1 = Entry(4, strGetPoids,'|')
		datePesee_1 = Entry(5, strGetPoids,'|')
        var s = Entry(6, strGetPoids,'|') + "000000"
		heurePesee_1 = s.substr(0,2)+":"+s.substr(2,2)+":"+s.substr(4,2)
	}
	if(strId=="DICT�tare"){
		dsd_2 = Entry(4, strGetPoids,'|')
		datePesee_2 = Entry(5, strGetPoids,'|')
        var s = Entry(6, strGetPoids,'|') + "000000"
		heurePesee_2 = s.substr(0,2)+":"+s.substr(2,2)+":"+s.substr(4,2)
	}
	if($("DICT�numpesee").value.trim()=="")
		$("DICT�numpesee").value = cer_chrono;
	//alert(strId)
	validInput($(strId));
	var pl = new SOAPClientParameters();
	var retourInvoke = SOAPClient.invoke("<?=search::searchExe("ao_askpoids_w3c.php",1)?>?&_value=&_inifile=<?=$xcspoolPath.'/pesee/poids.txt'?>&", "", pl, false);

}

function gestionParamDernierCharg()
{
	var dogFonction = "GET-FULL-TABCER�a-tabcer";
	var inputParameter = "TYPTAB�PRM�PREFIX�SAISIE�CODTAB�CHARG";
	var	retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "" , "" );

	if(retourExchange == "OK")
	{
		if(getValeurRsp("LIBEL") == "T")
		{
			for(var i = 1; i <= 3 ;i++)
			{
				$("DICT�protrs_" + i).style.width = "80px";
				$("DICT�protrs_" + i).setAttribute("NATURE", "SEARCH");
				$("DICT�protrs_" + i + "_libel").show();
                $("DICT�protrs_" + i + "_nivnet").show();

			}
			try{
			$("DICT�protrs_1").attachEvent("onfocus", function(){initAutoComplete($('DICT�protrs_1'),'CODTAB,LIBEL1-1','CODTAB','',450,'1')})
			$("DICT�protrs_2").attachEvent("onfocus", function(){initAutoComplete($('DICT�protrs_2'),'CODTAB,LIBEL1-1','CODTAB','',450,'1')})
			$("DICT�protrs_3").attachEvent("onfocus", function(){initAutoComplete($('DICT�protrs_3'),'CODTAB,LIBEL1-1','CODTAB','',450,'1')})
			}catch(ex){
				$("DICT�protrs_1").addEventListener( "focus", function(){initAutoComplete($('DICT�protrs_1'),'CODTAB,LIBEL1-1','CODTAB','',450,'1')},false)
				$("DICT�protrs_2").addEventListener( "focus", function(){initAutoComplete($('DICT�protrs_2'),'CODTAB,LIBEL1-1','CODTAB','',450,'1')},false)
				$("DICT�protrs_3").addEventListener( "focus", function(){initAutoComplete($('DICT�protrs_3'),'CODTAB,LIBEL1-1','CODTAB','',450,'1')},false)

			}
		}
	}
}

function visuNC()
{
	if("<?=$ident?>" != "734" || rowidCours.trim() == "")
		return false;
	
	/* contr�le des caract�risques (non-conformit� bnj) */
	var dogFonctionNC = "visu-nc�a-cereal";
	var inputParameterNC = "ROWID-CERMVT�" + rowidCours;
	var retourExchangeNC = sendExchangeInvoke("<?=$dogExchange?>", inputParameterNC, dogFonctionNC, "", "");

	if(retourExchangeNC == "OK" && getValeurRsp("NONCONFS").trim() != "")
	{
		var codenc = "";
		var val = "";
		var tab = getValeurRsp("NONCONFS").trim().split("|");
		if(tab.length == 0)
			tab[0] = getValeurRsp("NONCONFS").trim();
		for(var i = 0; i < tab.length; i++)
		{
			if(codenc.trim() != "")
				codenc += "|";
			if(val.trim() != "")
				val += "|";
			if(tab[i].indexOf(",") != -1)
			{
				t_tab = tab[i].split(",");
				codenc += t_tab[0];
				val += t_tab[1] + "," + t_tab[2] + "," + t_tab[3];
			}
			else
			{
				val += ",,";
			}
			// if
		}
		PhpAuxges = "<?=search::searchExe("as_sainonconf_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_VISU=yes&_codenc="+ codenc +"&_val="+ val ;
		sFeatures = "dialogHeight:520px;dialogWidth:950px;status:no;help:no;scroll:no";
		var retourModale = showModalDialog ( PhpAuxges , "" , sFeatures) ;
		if(retourModale == undefined || retourModale == null)
			retourModale = "";
	}
}

function ajoutEclatement()
{
	trace("--ajoutEclatement--")
	
	if(isCreation3)
		return;
	
	if(parseFloatStr($('DICT�pdsresteini').value) == 0)
		return;
	
	(eclatementSPE)? gridEclat.addRowEmptySPE():gridEclat.addRowEmpty();
}

function supprEclatement()
{
	trace("--supprEclatement--")
	
	if(isSaisieEnCours3)
		return;
	
	var r = gridEclat.getCurrentRow();
	
	if(eclatementSPE && row == 0 && (!modifBon || deltaQteModifBon <= 0))
		return;
	else if(!confirm("Supprimer l'�clatement ?"))
		return;
	
	gridEclat.setCellEditable(true, getColEclat("pdsNormes"), r);
	
	dataGridEclat.splice(r,1);
	rspContratsEclat.splice(r,1);
	gridEclat.setCellData(dataGridEclat);
	gridEclat.setRowCount(dataGridEclat.size());
	gridEclat.refresh();
	
	var total = 0;
	for(var i=0; i<gridEclat.getRowCount(); i++)
		if(gridEclat.getCellText(getColEclat("pdsNormes") , i) != "")
			total += parseFloatStr(gridEclat.getCellText(getColEclat("pdsNormes") , i));

	// trace("	CALCUL RESTE SUPPRESSION")	
		
	var pds = parseFloatStr($('DICT�pdsnormesini').value);
	var reste = ((100000*pds)-(100000*total))/100000;

	$('DICT�pdsresteini').value = reste.toFixed(3);
}

/**
	Controle que la saisie est correct (le cas d'une saisie sans contrat.)
	On test que ce n'est pas la 2e fois que l'on saisi un mouvement sans contrat.
	
	(note : le test qui v�rifie que le contrat n'est pas saisi 2fois est dans le dogexchangecallback)
**/
function ctrlSaisieEclatementSPE()
{
	trace("--ctrlSaisieEclatementSPE--")
	
	var textCelulle = ""
	var trouve = false;
	for(var i=0; i<gridEclat.getRowCount(); i++)
	{	
		textCelulle = gridEclat.getCellData(getColEclat('CONTRAT'),i).trim();

		if(!trouve && textCelulle == "")
		{
			// alert("TROUVE")
			trouve = true;
			continue
		}
		
		if(trouve && textCelulle == "")
		{
			gridEclat.setCellText("",getColEclat("contrat"),i);
			keycode = 1111;
			alert("Vous ne pouvez saisir qu'un seul mouvement sans contrat.")
			return false;
		}
	}
	
	keycode = 13;
	return true;
}

topForce=false;
topForceValue="F";
function saisiepdsnormes(){
	//on debloque la zone de saisie
	if(isSaisieEnCours)zonscreenget("pdsnormes��8");
}


/**
	M�thode de sauvegarde des caracteristiques.
	dans la variable saveCarract
	["caract | ref | analyse | nego"]
**/
function sauvegardeCaracteristiques()
{
	saveCarract = [];
	for(var i=1; i<=14; i++)
	{
		if($('DICT�libcar_'+i).value != "")
			saveCarract.push($('DICT�libcar_'+i).value.trim() + '|' + $('DICT�val-etat-ctr_'+i).value + '|' + $('DICT�val-etat_'+i).value + '|' + $('DICT�val-nego_'+i).value);
	}
}

/**
	M�thode qui reporte les valeurs des caracteristiques COMMUNES entre l'ancienne c�r�ale saisie et la nouvelle.
**/
function repositionnerCaractCommunes()
{
	for(var i=1; i<=14; i++)
	{
		if(saveCarract[i] == undefined)
			continue;
		
		var carRecherche = $('DICT�libcar_'+i).value.trim();
		for(var j=0; j<14; j++)
		{
			if(saveCarract[j] == undefined)
				continue;
			
			//On a retrouve la caracteristique => on reprend les valeures saisies.
			if(saveCarract[j].split('|')[0] == carRecherche)
			{
				$('DICT�val-etat-ctr_'+i).value = saveCarract[j].split('|')[1];
				$('DICT�val-etat_'+i).value = saveCarract[j].split('|')[2];
				$('DICT�val-nego_'+i).value = saveCarract[j].split('|')[3];
				break;
			}
		}
	}
}

/**
	M�thode de validation de la saisie des p�niches
**/
function validationPeniche()
{
	if($('DICT�transp_libel2').value.trim( )== '' || $('DICT�dest-fin2').value.trim() == '')
	{
		alert('Zones obligatoires');
		return;
	}
	
	$('TRANSP').style.display 			= 'none';
	$('DICT�dest-fin').value 			= $('DICT�dest-fin2').value;
	$('DICT�transp_libel').value 		= $('DICT�transp_libel2').value;
	$('DICT�transp_libel').defaultValue	= $('DICT�transp_libel2').value;
	
	setFocus(TabFocusEntree, 'DICT�transp_libel');
}

function ctrlSaisieTousCaract()
{
	trace("--ctrlSaisieTousCaract--")
	
	var retourTest
	for(var ind=1; ind<=14; ind++)
	{
		if($('caract_'+ind).style.visibility.toUpperCase() == 'HIDDEN')
			continue;
		
		if(!$('DICT�val-etat_'+ind).readOnly)
		{
			retourTest = CtrlSaisieValCaract($('DICT�val-etat_'+ind), ind, false)			
		}
		
		if(!retourTest)
			break;
		
		if(!$('DICT�val-nego_'+ind).readOnly)
		{
			CtrlSaisieValCaract($('DICT�val-nego_'+ind), ind, true)
		}
		
		if(!retourTest)
			break;
	}
}

/**
	M�thode qui, en arrivant sur la zone du contrat, ouvre automatiquement la fen�tre de recherche de contrats
	SI aucun contrat d�j� renseign�
	SI _F10contrat = O
**/
function ouvertureRecherche(idContrat)
{
	trace("--ouvertureRecherche - " + idContrat + " --")
	
	if($(idContrat).value.trim() == "" && $(idContrat).defaultValue.trim() == "" && f10Contrat)
	{
		//m_saitie.trim() != "" && m_saitie.indexOf($("DICT�codmvt").value.trim().toUpperCase()) == -1
		if(m_saitie.trim() != "" && m_saitie.indexOf($("DICT�codmvt").value.trim().toUpperCase()) != -1)
		{
			trace("	m_saitie.trim() != '' && m_saitie.indexOf($('DICT�codmvt').value.trim().toUpperCase()) != -1");
			return;
		}
		
		var dogFct = "";
		var requete = "";
		var retourExchange;
		
		switch(idContrat)
		{
			case 'DICT�cer-contrat':
				/*TEST PRESENCE CONTRAT*/
				requete = "CODSOC�" 	+ socConnexion
						+ "�TYPCTR�"	+ typctr.trim()
						+ "�CODCER�" 	+ $('DICT�codcer').value
						+ "�TYPAUX�" 	+ $('DICT�typaux').value 
						+ "�CODAUXGES�" + $('DICT�codauxges').value
						+ "�CAMPAGNE�" 	+ $('DICT�campagne').value
						+ "�CLOTURE�0"
						+ "�CERGPMT�" 	+ $("DICT�codcer-ori").value
						+ "�TYPE-TRANSP�*"
						+ "�MAGASI�" 	+ $('DICT�magasi').value
						+ "�FILIERE�N"
						+ "�CODMVT�" 	+ $('DICT�codmvt').value
						+ "�PARAM�N�PROG�asbonapp"
						+ "�DATMVT�" 	+ $('DICT�cer-datsai').value
						+ "�CANFIND�O";
						
				dogFct = "F10-CERCTR�A-CERCTR";

				retourExchange = sendExchangeInvoke ( lienDogexchange , requete , dogFct , "" , "" );
				
				//Si pas de contrats => zone suivante
				if(retourExchange != 'OK')
				{
					trace("	pas de contrats fili�re");
					setFocus ( TabFocusEntree , idContrat );
					return;
				}

			
				Rsp=RspCodcer.clone();
				cergpmt=getValue("CODCER-REG",false,"","",true);
				Rsp=RspCodmvt.concat();
				
				PhpAuxges = "<?=search::searchExe("ar_contrat_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_autoload=O&_typctr=A&_clot=0&_codcer="+$("DICT�codcer").value+"&_codcerlibel="+$("DICT�codcer_libel").value+"&_typaux="+$("DICT�typaux").value+"&_codauxges="+$("DICT�codauxges").value+"&_codauxgeslibel="+$("DICT�codauxges_libel").value +"&_cergpmt="+cergpmt+"&_camp="+$("DICT�campagne").value+"&_codmvt="+$("DICT�codmvt").value+"&_magasi="+$("DICT�magasi").value+"&_filiere=N&_param=N&_prog=asbonapp" + "&_datmvt=" + $("DICT�cer-datsai").value;

				if(getValue ( "TYPTRANS"  , false , "" , ""  , true)=="1")
				{
					PhpAuxges = "<?=search::searchExe("ar_contrat_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_autoload=O&_typctr=V&_clot=0&_codcer="+$("DICT�codcer").value+"&_codcerlibel="+$("DICT�codcer_libel").value+"&_typaux="+$("DICT�typaux").value+"&_codauxges="+$("DICT�codauxges").value+"&_codauxgeslibel="+$("DICT�codauxges_libel").value +"&_cergpmt="+cergpmt+"&_camp="+$("DICT�campagne").value+"&_codmvt="+$("DICT�codmvt").value+"&_magasi="+$("DICT�magasi").value+"&_filiere=N&_param=N&_prog=asbonapp" + "&_datmvt=" + $("DICT�cer-datsai").value;
				}			
				
				var sFeatures = "dialogHeight:690px;dialogWidth:975px;status:no;help:no";
				var retourModale = showModalDialog ( PhpAuxges , "" , sFeatures) ;
				
				if(retourModale == undefined)
				{
					setFocus ( TabFocusEntree , idContrat );
					return;
				}
				
				var dataModal = retourModale[1]
				
				RspContrat[1] = dataModal;
									
				$('DICT�cer-contrat').value 		= getField(retourModale[1], 'CER-CONTRAT');
				$('DICT�cer-contrat').defaultValue 	= getField(retourModale[1], 'CER-CONTRAT');
				
				retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , "�ROWID�" + getValueFromDataArray(retourModale, "ROWID", false, "") , "get-cerctr�a-cerctr" , "", dogexchangecallback);
				
				setFocus ( TabFocusEntree , idContrat )
				break;
				
			case 'DICT�cer-contrat-fil':
				//TEST PRESENCE CONTRAT
				requete = "CODSOC�" 	+ socConnexion 
						+ "�TYPCTR�"	+ typctr.trim()
						+ "�CLOTURE�0�FILIERE�O"
						+ "�TYPAUX�" 	+ $('DICT�typaux').value 
						+ "�CODAUXGES�" + $('DICT�codauxges').value
						+ "�CODCER�" 	+ $('DICT�codcer').value
						+ "�CAMPAGNE�" 	+ $('DICT�campagne').value
						+ "�CODMVT�" 	+ $('DICT�codmvt').value
						+ "�CERGPMT�"	+ $("DICT�codcer-ori").value
						+ "�PROG�ASBONAPP"
						+ "�DATMVT�" 	+ $('DICT�cer-datsai').value
						+ "�CANFIND�O";
						
				dogFct = "F10-CERCTR�A-CERCTR";
				
				retourExchange = sendExchangeInvoke ( lienDogexchange , requete , dogFct , "" , "" );
				
				//Si pas de contrats => zone suivante
				if(retourExchange != 'OK')
				{
					setFocus ( TabFocusEntree , idContrat );
					return;
				}

			
				Rsp=RspCodcer.clone();
				cergpmt=getValue("CODCER-REG",false,"","",true);
				Rsp=RspCodmvt.concat();

				PhpAuxges = "<?=search::searchExe("ar_contrat_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_autoload=O&_typctr=A&_clot=0&_codcer="+$("DICT�codcer").value+"&_codcerlibel="+$("DICT�codcer_libel").value+"&_typaux="+$("DICT�typaux").value+"&_codauxges="+$("DICT�codauxges").value+"&_codauxgeslibel="+$("DICT�codauxges_libel").value +"&_cergpmt="+cergpmt+"&_camp="+$("DICT�campagne").value+"&_codmvt="+$("DICT�codmvt").value+"&_magasi="+$("DICT�magasi").value+"&_filiere=O&_param=N&_prog=asbonapp" + "&_datmvt=" + $("DICT�cer-datsai").value;

				if(getValue ( "TYPTRANS"  , false , "" , ""  , true)=="1")
					PhpAuxges = "<?=search::searchExe("ar_contrat_w3c.php",1)?>?PHPSESSID=<?=$PHPSESSID?>&_autoload=O&_typctr=V&_clot=0&_codcer="+$("DICT�codcer").value+"&_codcerlibel="+$("DICT�codcer_libel").value+"&_typaux="+$("DICT�typaux").value+"&_codauxges="+$("DICT�codauxges").value+"&_codauxgeslibel="+$("DICT�codauxges_libel").value +"&_cergpmt="+cergpmt+"&_camp="+$("DICT�campagne").value+"&_codmvt="+$("DICT�codmvt").value+"&_magasi="+$("DICT�magasi").value+"&_filiere=O&_param=N&_prog=asbonapp" + "&_datmvt=" + $("DICT�cer-datsai").value;

				var sFeatures = "dialogHeight:690px;dialogWidth:975px;status:no;help:no";
				var retourModale = showModalDialog ( PhpAuxges , "" , sFeatures) ;
				
				if(retourModale == undefined)
				{
					trace("	pas de contrats c�r�ale");
					setFocus ( TabFocusEntree , idContrat );
					return;
				}
				
				$('DICT�cer-contrat-fil').value 		= getField(retourModale[1], 'CER-CONTRAT');
				$('DICT�cer-contrat-fil').defaultValue 	= getField(retourModale[1], 'CER-CONTRAT');
				
				setFocus ( TabFocusEntree , idContrat )
				break;
		}		
	}
}

</script>

<!-- METHODES SPECIFIQUES ORDRE SAISIE -->
<script type="text/javascript" src="<?=search::searchExe("as_ordre_saisie.js", 1)?>"></script>

<html>
	<body onload="initScreen()">
	
		<div style="position:absolute;top:0px;left:0px;width:955px;width:calc(100% - 10px);height:710px;height:calc(100%);">
			
			<!-- Utilis� par dogs -->
			<INPUT ID="DICT�variete" TYPE="TEXT" style="display:none"  />
						
			<div id="titre" style="position:absolute;top:0px;left:0px;width:965px;height:20px;">
				<span class="dogskinbodyfont" style="position:absolute;top:0px;left:15px;width:500px;font-weight:bold;font-size:14px"><?=$_libel_menu?></span>
				<span class="dogskinbodyfont" style="position:absolute;top:0px;right:15px;width:500px;font-weight:bold;font-size:11px;text-align:right"><?PHP print lect_bin::getLabel ( "societe" , "sl"); ?>&nbsp:&nbsp;<?=$c_codsoc." ".$c_libsoc." - ".$c_codetb." ".$c_libetb?></span>
			</div>
			<div id="entete" class="divArrondiDeg28" style="position:absolute;top:20px;left:5px;width:945px;width:calc(100% - 10px);height:40px;">
				<div id="" class="divArrondiDeg29" style="position:absolute;top:5px;left:5px;width:935px;width:calc(100% - 10px);height:30px;">
					<!-- Code magasin -->
					<div  style="position:absolute;left:5px;top:0px">
						<p class=dogskinbodyfont style="position:absolute;top:7px;width:90px;"><?PHP print lect_bin::getLabel ( "magasi" , "sl"); ?> </P>
						<input id="DICT�magasi" NATURE=SEARCH CLASS="dogskininputobli" style="position:absolute;top:5px;left:95px;width:80px" value='<?=$magasi?>'  OBLI="1"  />
						<input class="dogskininputreadonly" readonly id="DICT�magasi_libel" value='<?=$magasilibel?>' style="left:180px;top:5px;width:230px">
					</div>

					<!--//saisie date bon //-->
					<div  style="position:absolute;left:430px;top:0px">
						<p class=dogskinbodyfont style="top:7px"><?PHP print lect_bin::getLabel ( "cer-datsai" , "sl"); ?> </p>
						<input id="DICT�cer-datsai" CLASS="dogskininputobli" NATURE=DATE  style="position:absolute;top:5px;left:75px;width:80px" value='<?=$date_jour?>' OBLI="1" />
					</div>

					<!-- Campagne -->
					<div  style="position:absolute;left:600px;top:0px">
						<p class=dogskinbodyfont style="top:7px;width:70px"><?PHP print lect_bin::getLabel ( "campagne" , "sl"); ?> </P>
						<input id="DICT�campagne" value="<?=$annee?>" maxlength=4  CLASS="dogskininputobli"  style="position:absolute;top:5px;left:80px;width:50px" OBLI="1" />
					</div>
					<!-- Attente -->
					<div  style="position:absolute;left:745px;top:0px">
						<p class=dogskinbodyfont style="top:7px;width:70px">Statut bon</P>
						<select id="DICT�attente" CLASS="dogskininput"  style="position:absolute;top:5px;left:70px;width:80px" >
							<?PHP deal_commun::charge_select("combo-attente",""); ?>
						</select>
					</div>
					<img id="btn_newBon" src="<?=$dealgateRelPath?>/site/img/transparent.png" class="navImg tailleImg30 ajoutImg30" style="position:absolute;top:0px;right:5px;cursor:pointer" onclick="boninserer()" title="Nouveau bon (F9)" >
				</div>
			</div>

			<div id="divGridBons" class="divArrondiDeg28" style="position:absolute;top:65px;left:5px;width:945px;width:calc(100% - 10px);height:200px;overflow:hidden">
				<img id="btn_ouv_grid" src="<?=$dealgateRelPath?>/site/img/transparent.png" class="navImg tailleImg15 upImg15" style="position:absolute;top:5px;left:5px;cursor:pointer" onclick="ouvGrid()" title="Fermer" />
				<span class="dogskinbodyfont" style="position:absolute;left:25px;font-weight:bold;top:5px;">Bons Saisis</span>
			</div>

			<div id="divOnglets" style="display:none">
				<!-- ENT�TE DES ONGLETS -->
				<div id="divEntetesOnglets" class="" style="position:absolute;width:945px;width:calc(100% - 10px);top:275px;left:5px;height:35px;">
					<div id="ong1" style="position:absolute;left:5px;top:0px;width:80px;height:50px;cursor:pointer" onclick="loadOnglet(1)" class="divArrondiDeg21">
						<p id="ong1_p" class="dogskinbodyfont" style="position:absolute;top:5px;left:5px;width:75px;height:20px;text-align:center">Donn�es du bon</p>
					</div>
					<div id="ong2" style="position:absolute;left:5px;top:0px;width:80px;height:50px;cursor:pointer" onclick="loadOnglet(2)" class="divArrondiDeg21">
						<p id="ong2_p" class="dogskinbodyfont" style="position:absolute;top:5px;left:5px;width:75px;height:20px;text-align:center">Prestations</p>
					</div>
					<div id="ong3" style="position:absolute;left:5px;top:0px;width:80px;height:50px;cursor:pointer" onclick="loadOnglet(3)" class="divArrondiDeg21">
						<p id="ong3_p" class="dogskinbodyfont" style="position:absolute;top:5px;left:5px;width:75px;height:20px;text-align:center">Info Achat</p>
					</div>
					<div id="ong4" style="position:absolute;left:5px;top:0px;width:80px;height:50px;cursor:pointer" onclick="loadOnglet(4)" class="divArrondiDeg21">
						<p id="ong4_p" class="dogskinbodyfont" style="position:absolute;top:5px;left:5px;width:75px;height:20px;text-align:center">Non conformit�s</p>
					</div>
					<div id="ong5" style="position:absolute;left:5px;top:0px;width:80px;height:50px;cursor:pointer" onclick="loadOnglet(5)" class="divArrondiDeg21">
						<p id="ong5_p" class="dogskinbodyfont" style="position:absolute;top:5px;left:5px;width:75px;height:20px;text-align:center">Mise � jour / motif</p>
					</div>
					<div id="divBoutons" class="" style="position:absolute;top:0px;right:5px;height:30px;width:400px">
                        <!-- Le bouton calendrier (btn_datheu) est affich� selon un param�tre menu _calend = "Y" -->
						<img id="btn_datheu"  style="position:absolute;top:0px;right:215px;cursor:pointer;width:30px;height:30px" src="<?=$dealgateRelPath?>/site/img/di-calendar-127.png" onmouseout="this.src='<?=$dealgateRelPath?>/site/img/di-calendar-127.png'" onmouseover="this.src='<?=$dealgateRelPath?>/site/img/di-calendar-127-over.png'" onclick="affDatationBon()"  title="Dater le bon" />
						<img id="btn_attente" onclick="bonattente()" style="position:absolute;top:0px;right:180px;width:30px;height:30px;cursor:pointer"   src="<?=$dealgateRelPath?>/site/img/mise-en-attente.png" onmouseout="this.src='<?=$dealgateRelPath?>/site/img/mise-en-attente.png'" onmouseover="this.src='<?=$dealgateRelPath?>/site/img/mise-en-attente-over.png'"   title="Mise en attente"   />
						<input id="inputF10Fosse" style="position:absolute;top:3px;right:30px;display:none" />
						<img id="btn_imprim"  class="navImg tailleImg30 imprimImg30" style="position:absolute;top:0px;right:145px;cursor:pointer" src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="bonimprimer()"  title="<?PHP print lect_bin::getLabel ( "mess-tabgen2_48" , "sl"); ?>" />
						<img id="btn_modifier"class="navImg tailleImg30 updImg30" style="position:absolute;top:0px;right:110px;cursor:pointer" src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="bonModifier()"  title="Modification du bon" />
						<img id="btn_valid"	  class="navImg tailleImg30 validImg30" style="position:absolute;top:0px;right:75px;cursor:pointer" src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="bonvalider()"  title="<?=lect_bin::getLabel('valid-f2','sl')?>" />
						<img id="btn_delete"  class="navImg tailleImg30 poubelleImg30" style="position:absolute;top:0px;right:40px;cursor:pointer" src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="bonsupprimer()"  title="<?PHP print lect_bin::getLabel ( "mess-tabgen2_50" , "sl"); ?>" />
						<img id="btn_quitter" class="navImg tailleImg30 annulImg30" style="position:absolute;top:0px;right:5px;cursor:pointer"  src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="razEcran(false)"	   title="<?=lect_bin::getLabel('quit-escape','sl')?>" />
					</div>
				</div>

				<div id="corps" class="divArrondiDeg28" style="position:absolute;top:315px;left:5px;width:945px;width:calc(100% - 10px);height:360px;height:calc(100% - 50px);">
					<!-- ONGLETS -->
					<div id="corpsOnglet" class="" style="position:absolute;width:935px;left:5px;top:5px;height:350px;height:calc(100% - 140px);overflow:auto">
						<!-- Donn�es du bon -->
						<div id="contenuOng1" style="display:none" >
							<div class="divArrondiDeg29" id="divDonneesDuBon" style="position:absolute;top:0px;left:0px;width:480px;height:310px">
								<DIV  style="position:absolute;left:10px;top:0px;width:200px;height:200px">
									<!-- N� Ordre -->
									<P class=dogskinbodyfont style="top:7px;width:180px"><?PHP print lect_bin::getLabel ( "cer-ordre" , "sl"); ?> </P>
									<INPUT ID="DICT�no-ordre" NATURE=SEARCH class="dogskininput"  style="top:5px;left:140px;width:70px"  maxlength="10" obli="" />
								</DIV>
								<DIV  style="position:absolute;left:250px;top:0px;display:<?if($_saisieopecre==''){?>none<?}?>">
									<P class=dogskinbodyfont style="top:7px"><?=lect_bin::getLabel("opecre","sl")?></P>
									<INPUT class="dogskininput"<?if($_saisieopecre!=''){?>obli<?}?>  ID="DICT�opecre2"  style="left:80px;top:5px;width:80px" <?if($_saisieopecre!=''){?>OBLI="1"<?}?>   >
								</DIV>
								<DIV  style="position:absolute;left:10px;top:20px;width:200px;height:200px">
									<!--Code mouvement -->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "codmvt" , "sl"); ?> </P>
									<INPUT ID="DICT�codmvt" NATURE=SEARCH class="dogskininput"  style="top:5px;left:140px;width:70px"    title="<?if($_mvtvte!=''){print 'F9 pour s\'appuyer sur un contrat';}?>"     OBLI="1"  />
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�codmvt_libel" style="left:215px;top:5px;width:240px" >
								</DIV>

								<DIV  style="position:absolute;left:10px;top:40px">
									<!--Code c�r�ale -->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "cer-codcer" , "sl"); ?> </P>
									<INPUT ID="DICT�codcer" NATURE=SEARCH class="dogskininput"  style="top:5px;left:140px;width:70px"     title="F8: recherche des ordres d'execution"  OBLI="1"   />
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�codcer_libel" style="left:215px;top:5px;width:240px" >
								</DIV>
								<DIV  style="position:absolute;left:10px;top:60px;width:200px;height:200px">
									<!--Code tiers -->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "codauxges" , "sl"); ?> </P>
									<INPUT ID="DICT�typaux" NATURE=SEARCH class="dogskininput" value="<?=$_typaux?>" style="top:5px;left:140px;width:50px"       >
									<INPUT ID="DICT�codauxges" NATURE=SEARCH class="dogskininput" style="top:5px;left:195px;width:80px"       >
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�codauxges_libel" style="left:280px;top:5px;width:175px" >
								</DIV>
								<!--//Saisie du code magasin 2//-->
									<DIV  style="position:absolute;left:10px;top:60px;visibility:hidden">
									  <P class=dogskinbodyfont style="top:5px;width:170px"><?PHP print lect_bin::getLabel ( "magas2" , "sl"); ?> </P>
									  <INPUT ID="DICT�magas2" NATURE=SEARCH class="dogskininput" title="F8 : recherche des mouvements de sorties" style="top:5px;left:140px;width:70px" value='<?=$magasi?>'       >
									  <INPUT CLASS=dogskininputreadonly readonly ID="DICT�magas2_libel" value='<?=$magas2libel?>' style="left:215px;top:5px;width:240px">
									</DIV>
								<DIV  style="position:absolute;left:10px;top:80px">
									<!-- num�ro de contrat-->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "cer-contrat" , "sl"); ?> </P>
									<INPUT ID="DICT�cer-contrat" onchange="ctrlQtePdsNormes()" onfocus="ouvertureRecherche(this.id)" NATURE=SEARCH class="dogskininput"  style="top:5px;left:140px;width:100px"        >
									<INPUT ID="DICT�numoex" style="left:245px;top:5px;width:150px" CLASS=dogskininputreadonly readonly value=""  >
									<INPUT ID="DICT�indice-liv" style="left:395px;top:5px;width:60px" CLASS=dogskininputreadonly readonly  >
								</DIV>
								<DIV  style="position:absolute;left:10px;top:80px;visibility:hidden">
									<INPUT ID="DICT�natcon" NATURE=SEARCH alt="Ordre de commercialisation" title="Ordre de commercialisation" style="left:250px;top:5px;width:50px" CLASS=dogskininput  >
								</DIV>
								<DIV  style="position:absolute;left:320px;top:80px">
									<!-- num�ro de contrat filiere-->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "cer-filiere" , "sl"); ?> </P>
									<INPUT ID="DICT�cer-contrat-fil" NATURE=SEARCH class="dogskininput" onfocus="ouvertureRecherche(this.id)" style="top:5px;left:45px;width:100px"         >
								</DIV>
								<DIV  style="position:absolute;left:10px;top:80px;width:200px;height:200px;">
									<!--Code e-cellule -->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "e-cellule" , "sl"); ?> </P>
									<INPUT ID="DICT�e-cellule" NATURE=SEARCH class="dogskininput" style="top:5px;left:140px;width:70px"      >
									<INPUT ID="DICT�e-rotation" NATURE=SEARCH class="dogskininput" style="top:5px;left:140px;width:70px;display:none"      >
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�e-cellule_libel" style="left:215px;top:5px;width:240px" >
								</DIV>

								<DIV  style="position:absolute;left:10px;top:100px;width:200px;height:200px">
									<!-- Tiers Chauffeur -->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "tierschauffeur" , "sl"); ?> </P>
									<INPUT ID="DICT�typaux_chauff" NATURE=SEARCH class="dogskininput" value="<?=$_typaux?>" style="top:5px;left:140px;width:50px"       >
									<INPUT ID="DICT�codaux_chauff" NATURE=SEARCH class="dogskininput" style="top:5px;left:195px;width:80px"       >
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�codaux_chauff_libel" style="left:280px;top:5px;width:175px" >
								</DIV>

								<DIV  style="position:absolute;left:10px;top:120px">
									<!--type transport-->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "typtrp-ctr" , "sl"); ?> </P>
									<INPUT ID="DICT�typtrp-ctr" NATURE=SEARCH class="dogskininput" style="top:5px;left:140px;width:70px"      >
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�typtrp-ctr_libel"  style="left:215px;top:5px;width:240px">
								</DIV>

								<DIV  style="position:absolute;left:10px;top:140px">
									<!-- code transporteur -->
									<P class=dogskinbodyfont style="top:5px;width:170px"><?PHP print lect_bin::getLabel ( "transp" , "sl"); ?> </P>
									<INPUT ID="DICT�transp" NATURE=SEARCH class="dogskininput" style="top:5px;left:140px;width:70px"         >
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�transp_libel" style="left:215px;top:5px;width:240px"   >
								</DIV>

								<DIV  style="position:absolute;left:10px;top:160px">
									<!-- code chauffeur -->
									<P class=dogskinbodyfont style="top:5px;width:170px"><?PHP print lect_bin::getLabel ( "chauff" , "sl"); ?> </P>
									<INPUT ID="DICT�chauff" NATURE=SEARCH class="dogskininput" style="top:5px;left:140px;width:70px"         >
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�chauff_libel"  style="left:215px;top:5px;width:240px">
								</DIV>

								<DIV  style="position:absolute;left:10px;top:180px">
									<!-- code immatr -->
									<P class=dogskinbodyfont style="top:5px"><?PHP print lect_bin::getLabel ( "immatr" , "sl"); ?> </P>
									<INPUT ID="DICT�immatr" NATURE=SEARCH class="dogskininput" style="top:5px;left:140px;width:70px"         >
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�immatr_libel"  style="left:215px;top:5px;width:240px">
								</DIV>

								<DIV  style="position:absolute;left:10px;top:200px">
									<!--reference interne-->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "bl-interne" , "sl"); ?> </P>
									<INPUT ID="DICT�bl-interne"  class="dogskininput"  style="top:5px;left:140px;width:70px"         >
								</DIV>

								<DIV  style="position:absolute;left:10px;top:220px">
									<!--commentaire-->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "comment" , "sl"); ?> </P>
									<INPUT ID="DICT�cer-libel"  class="dogskininput"  style="top:5px;left:140px;width:315px"        >
								</DIV>

								<DIV  style="position:absolute;left:10px;top:240px">
									<!--Code c�r�ale de stock-->
									<P class=dogskinbodyfont style="top:5px;width:180px">C�r�ale de stock </P>
									<INPUT ID="DICT�codcer-ori" NATURE=SEARCH class="dogskininput"  style="top:5px;left:140px;width:70px"       >
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�codcer-ori_libel" style="left:215px;top:5px;width:240px" >
								</DIV>

								<DIV  style="position:absolute;left:10px;top:260px">
									<!--parcelle Pte Ajout Code + libel + btn F10-->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "parcelle" , "sl"); ?> </P>
									<INPUT ID="DICT�codeparc" <?if($_parcel!='libre'){?>NATURE=SEARCH<?}?> class="dogskininput"  style="top:5px;left:140px;width:70px"       >
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�lib-parcelle" <?if($_parcel=='libre'){?>style="left:215px;top:5px;width:240px;visibility:hidden" <?}else{?>style="left:215px;top:5px;width:240px" <?}?>>
								</DIV>

                                <!-- Pte DDD-NAT-PARCELLE -->
								<DIV  style="position:absolute;left:10px;top:260px;visibility:hidden">
                                	<INPUT ID="DICT�chronoparc" class="dogskininput"  style="top:5px;left:220px;width:10px">
								</DIV>

								<DIV  style="position:absolute;left:10px;top:280px">
									<!-- code commune -->
									<P class=dogskinbodyfont style="top:5px"><?PHP print lect_bin::getLabel ( "comonic" , "sl"); ?> </P>
									<INPUT ID="DICT�comonic" NATURE=SEARCH class="dogskininput" style="top:5px;left:140px;width:70px"         >
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�comonic_libel"  style="left:215px;top:5px;width:240px">
								</DIV>
								<DIV  style="position:absolute;left:10px;top:280px;visibility:hidden">
									<!-- Pte Gestion des lots apports -->
									<P class=dogskinbodyfont style="top:5px;width:180px">Num�ro de lot</P>
									<INPUT ID="DICT�codlot" CLASS="dogskininputreadonly" readonly style="top:5px;left:140px;width:40px">
									<INPUT ID="DICT�numlot" NATURE=SEARCH class="dogskininput"  style="top:5px;left:185px;width:40px"      >
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�lib-lot" style="left:230px;top:5px;width:225px">
								</DIV>
								
							</div>
							
						<!--------------------------- G E S T I O N     D E S    C A R A C T E R I S T I Q U E S ------------------------------------->
						<!--            LIBELLE CARRACT       |      REFERENCE        |       ANALYSE       |        VAL NEGO                       -->
							
							<div class="divArrondiDeg29" id="divCaracteristiques" style="position:absolute;top:0px;left:510px;width:400px;height:310px">
								<DIV  style="position:absolute;left:15px;top:5px">
									<P class=dogskinbodyfont  style="font-weight:600" >Caracteristiques</P>
									<P class=dogskinbodyfont  style="left:155px;font-weight:600; width: 70px" >R�f�rence</P>
									<P class=dogskinbodyfont  style="left:235px;font-weight:600; width: 70px" >Analyses</P>
									<P class=dogskinbodyfont  style="left:315px;font-weight:600; width: 70px" >Val N�go</P>
								</DIV>

									<!-- caract�ristiques_1-->
								<DIV id="caract_1" style="position:absolute;left:5px;top:25px">
									<INPUT ID="DICT�libcar_1"  		CLASS=dogskininputreadonly 	readonly 	style="left:10px;width:140px">
									<INPUT ID="DICT�val-etat-ctr_1" CLASS=dogskininputreadonly 	readonly 	style="left:160px;width:70px">
									<INPUT ID="DICT�val-etat_1"  	class=dogskininputreadonly 	readonly 	style="left:241px;width:70px"    nature="NOMBRE">
									<INPUT ID="DICT�val-nego_1"  	class=dogskininputreadonly 	readonly 	style="left:321px;width:60px"    nature="NOMBRE">
								</DIV>
								
									<!-- caract�ristiques_2-->
								<DIV id="caract_2" style="position:absolute;left:5px;top:45px">
									<INPUT ID="DICT�libcar_2"  		CLASS=dogskininputreadonly 	readonly 	style="left:10px;width:140px">
									<INPUT ID="DICT�val-etat-ctr_2" CLASS=dogskininputreadonly 	readonly 	style="left:160px;width:70px">
									<INPUT ID="DICT�val-etat_2"  	class=dogskininputreadonly 	readonly 	style="left:241px;width:70px"    nature="NOMBRE">
									<INPUT ID="DICT�val-nego_2"  	class=dogskininputreadonly 	readonly 	style="left:321px;width:60px"    nature="NOMBRE">
								</DIV>
								
									<!-- caract�ristiques_3-->
								<DIV id="caract_3" style="position:absolute;left:5px;top:65px">
									<INPUT ID="DICT�libcar_3"  		CLASS=dogskininputreadonly 	readonly 	style="left:10px;width:140px">
									<INPUT ID="DICT�val-etat-ctr_3" CLASS=dogskininputreadonly 	readonly 	style="left:160px;width:70px">
									<INPUT ID="DICT�val-etat_3"  	class=dogskininputreadonly 	readonly 	style="left:241px;width:70px"     nature="NOMBRE">
									<INPUT ID="DICT�val-nego_3"  	class=dogskininputreadonly 	readonly 	style="left:321px;width:60px"     nature="NOMBRE">
								</DIV>
								
									<!-- caract�ristiques_4-->
								<DIV id="caract_4" style="position:absolute;left:5px;top:85px">
									<INPUT ID="DICT�libcar_4"  		CLASS=dogskininputreadonly 	readonly 	style="left:10px;width:140px">
									<INPUT ID="DICT�val-etat-ctr_4" CLASS=dogskininputreadonly 	readonly 	style="left:160px;width:70px">
									<INPUT ID="DICT�val-etat_4"  	class=dogskininputreadonly 	readonly 	style="left:241px;width:70px"     nature="NOMBRE">
									<INPUT ID="DICT�val-nego_4"  	class=dogskininputreadonly 	readonly 	style="left:321px;width:60px"     nature="NOMBRE">
								</DIV>
								
									<!-- caract�ristiques_5-->
								<DIV id="caract_5" style="position:absolute;left:5px;top:105px">
									<INPUT ID="DICT�libcar_5"  		CLASS=dogskininputreadonly 	readonly 	style="left:10px;width:140px">
									<INPUT ID="DICT�val-etat-ctr_5" CLASS=dogskininputreadonly 	readonly 	style="left:160px;width:70px">
									<INPUT ID="DICT�val-etat_5"  	class=dogskininputreadonly 	readonly 	style="left:241px;width:70px"     nature="NOMBRE">
									<INPUT ID="DICT�val-nego_5"  	class=dogskininputreadonly 	readonly 	style="left:321px;width:60px"     nature="NOMBRE">
								</DIV>
								
									<!-- caract�ristiques_6-->
								<DIV id="caract_6" style="position:absolute;left:5px;top:125px">
									<INPUT ID="DICT�libcar_6"  		CLASS=dogskininputreadonly 	readonly 	style="left:10px;width:140px">
									<INPUT ID="DICT�val-etat-ctr_6" CLASS=dogskininputreadonly 	readonly 	style="left:160px;width:70px">
									<INPUT ID="DICT�val-etat_6"  	class=dogskininputreadonly 	readonly 	style="left:241px;width:70px"     nature="NOMBRE">
									<INPUT ID="DICT�val-nego_6"  	class=dogskininputreadonly 	readonly 	style="left:321px;width:60px"     nature="NOMBRE">
								</DIV>
								
									<!-- caract�ristiques_7-->
								<DIV id="caract_7" style="position:absolute;left:5px;top:145px">
									<INPUT ID="DICT�libcar_7"  		CLASS=dogskininputreadonly 	readonly 	style="left:10px;width:140px">
									<INPUT ID="DICT�val-etat-ctr_7" CLASS=dogskininputreadonly 	readonly 	style="left:160px;width:70px">
									<INPUT ID="DICT�val-etat_7"  	class=dogskininputreadonly 	readonly 	style="left:241px;width:70px"     nature="NOMBRE">
									<INPUT ID="DICT�val-nego_7"  	class=dogskininputreadonly 	readonly 	style="left:321px;width:60px"     nature="NOMBRE">
								</DIV>
								
									<!-- caract�ristiques_8-->
								<DIV id="caract_8" style="position:absolute;left:5px;top:165px">
									<INPUT ID="DICT�libcar_8"  		CLASS=dogskininputreadonly 	readonly 	style="left:10px;width:140px">
									<INPUT ID="DICT�val-etat-ctr_8" CLASS=dogskininputreadonly 	readonly 	style="left:160px;width:70px">
									<INPUT ID="DICT�val-etat_8"  	class=dogskininputreadonly 	readonly 	style="left:241px;width:70px"     nature="NOMBRE">
									<INPUT ID="DICT�val-nego_8"  	class=dogskininputreadonly 	readonly 	style="left:321px;width:60px"     nature="NOMBRE">
								</DIV>
								
									<!-- caract�ristiques_9-->
								<DIV id="caract_9" style="position:absolute;left:5px;top:185px">
									<INPUT ID="DICT�libcar_9"  		CLASS=dogskininputreadonly 	readonly 	style="left:10px;width:140px">
									<INPUT ID="DICT�val-etat-ctr_9" CLASS=dogskininputreadonly 	readonly 	style="left:160px;width:70px">
									<INPUT ID="DICT�val-etat_9"  	class=dogskininputreadonly 	readonly 	style="left:241px;width:70px"     nature="NOMBRE">
									<INPUT ID="DICT�val-nego_9"  	class=dogskininputreadonly 	readonly 	style="left:321px;width:60px"     nature="NOMBRE">
								</DIV>
								
									<!-- caract�ristiques_10-->
								<DIV id="caract_10" style="position:absolute;left:5px;top:205px">
									<INPUT ID="DICT�libcar_10"  		CLASS=dogskininputreadonly 	readonly 	style="left:10px;width:140px">
									<INPUT ID="DICT�val-etat-ctr_10" 	CLASS=dogskininputreadonly 	readonly 	style="left:160px;width:70px">
									<INPUT ID="DICT�val-etat_10"  		class=dogskininputreadonly 	readonly 	style="left:241px;width:70px"    nature="NOMBRE">
									<INPUT ID="DICT�val-nego_10"  		class=dogskininputreadonly 	readonly 	style="left:321px;width:60px"    nature="NOMBRE">
								</DIV>
								
									<!-- caract�ristiques_11-->
								<DIV id="caract_11" style="position:absolute;left:5px;top:225px">
									<INPUT ID="DICT�libcar_11"  		CLASS=dogskininputreadonly 	readonly 	style="left:10px;width:140px">
									<INPUT ID="DICT�val-etat-ctr_11" 	CLASS=dogskininputreadonly 	readonly 	style="left:160px;width:70px">
									<INPUT ID="DICT�val-etat_11"  		class=dogskininputreadonly 	readonly 	style="left:241px;width:70px"    nature="NOMBRE">
									<INPUT ID="DICT�val-nego_11"  		class=dogskininputreadonly 	readonly 	style="left:321px;width:60px"    nature="NOMBRE">
								</DIV>
								
									<!-- caract�ristiques_12-->
								<DIV id="caract_12" style="position:absolute;left:5px;top:245px">
									<INPUT ID="DICT�libcar_12"  		CLASS=dogskininputreadonly 	readonly 	style="left:10px;width:140px">
									<INPUT ID="DICT�val-etat-ctr_12" 	CLASS=dogskininputreadonly 	readonly 	style="left:160px;width:70px">
									<INPUT ID="DICT�val-etat_12"  		class=dogskininputreadonly 	readonly 	style="left:241px;width:70px"    nature="NOMBRE">
									<INPUT ID="DICT�val-nego_12"  		class=dogskininputreadonly 	readonly 	style="left:321px;width:60px"    nature="NOMBRE">
								</DIV>
								
									<!-- caract�ristiques_13-->
								<DIV id="caract_13" style="position:absolute;left:5px;top:265px">
									<INPUT ID="DICT�libcar_13"  		CLASS=dogskininputreadonly 	readonly 	style="left:10px;width:140px">
									<INPUT ID="DICT�val-etat-ctr_13" 	CLASS=dogskininputreadonly 	readonly 	style="left:160px;width:70px">
									<INPUT ID="DICT�val-etat_13"  		class=dogskininputreadonly 	readonly 	style="left:241px;width:70px"    nature="NOMBRE">
									<INPUT ID="DICT�val-nego_13"  		class=dogskininputreadonly 	readonly 	style="left:321px;width:60px"    nature="NOMBRE">
								</DIV>
								
									<!-- caract�ristiques_14-->
								<DIV id="caract_14" style="position:absolute;left:5px;top:285px">
									<INPUT ID="DICT�libcar_14"  		CLASS=dogskininputreadonly 	readonly 	style="left:10px;width:140px">
									<INPUT ID="DICT�val-etat-ctr_14" 	CLASS=dogskininputreadonly 	readonly 	style="left:160px;width:70px">
									<INPUT ID="DICT�val-etat_14"  		class=dogskininputreadonly 	readonly 	style="left:241px;width:70px"    nature="NOMBRE">
									<INPUT ID="DICT�val-nego_14"  		class=dogskininputreadonly 	readonly 	style="left:321px;width:60px"    nature="NOMBRE">
								</DIV>
							</div>
							
							<div class="divArrondiDeg29" id="divPoidsAuxNormes" style="position:absolute;top:315px;left:0px;width:910px;height:275px">
								<DIV  style="position:absolute;left:10px;top:5px">
									<P class=dogskinbodyfont  style="top:5px;left:0px;font-weight:600" >Poids aux normes</P>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:25px">
									<!--Num Pesee-->
									<p class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "num-pesee" , "sl"); ?> </p>
									<INPUT ID="DICT�numpesee"  class="dogskininput"  style="top:5px;left:150px;width:70px"    NATURE="NOMBRE" >
								</DIV>
								<DIV  style="position:absolute;left:10px;top:45px">
									<!--Poids brut-->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "pdsbrut" , "sl"); ?> </P>
									<INPUT ID="DICT�pdsbrut" onchange="ctrlQtePdsNormes()" class="dogskininputobli"  style="top:5px;left:150px;width:70px"    nature="NOMBRE" OBLI="1"  >
									<a href="#" id="balancebrut" class=dogskinbodyfont style="position:absolute;top:5px;left:225px;text-decoration:underline;font-weight:800;font-size:8pt;cursor:hand;display:none"  title="Lecture du poids sur le pont bascule" onclick="lecturePontBascule($('DICT�pdsbrut'))">P1</a>
									<a href="#" id="balancebrut2" class=dogskinbodyfont style="position:absolute;top:5px;left:250px;text-decoration:underline;font-weight:800;font-size:8pt;cursor:hand;display:none"  title="Lecture du poids sur le pont bascule" onclick="lecturePontBascule($('DICT�pdsbrut'),true)">P2</a>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:65px">
									<!--Tare-->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "tare" , "sl"); ?> </P>
									<INPUT ID="DICT�tare" class="dogskininput" style="top:5px;left:150px;width:70px"  nature="NOMBRE"  />
									<a href="#" id="balancetare" class=dogskinbodyfont style="position:absolute;top:5px;left:225px;text-decoration:underline;font-weight:800;font-size:8pt;cursor:hand;display:none"  title="Lecture du poids sur le pont bascule" onclick="lecturePontBascule($('DICT�tare'))">P1</a>
									<a href="#" id="balancetare2" class=dogskinbodyfont style="position:absolute;top:5px;left:250px;text-decoration:underline;font-weight:800;font-size:8pt;cursor:hand;display:none"  title="Lecture du poids sur le pont bascule" onclick="lecturePontBascule($('DICT�tare'),true)">P2</a>
									<a href="#" id="tareSupervision" class=dogskinbodyfont style="position:absolute;top:5px;left:275px;text-decoration:underline;font-weight:800;font-size:8pt;cursor:hand;display:none"  title="Lecture du poids depuis Supervision" onclick="lectureSupervision($('DICT�tare'))">SV</a>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:85px">
									<!--Pdsnet-->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "pdsnet" , "sl"); ?> </P>
									<INPUT ID="DICT�pdsnet"  CLASS=dogskininputreadonly readonly style="top:5px;left:60px;width:60px;text-align:right"    nature="NOMBRE"  >
								</DIV>
								<DIV  style="position:absolute;left:137px;top:85px">
									<!--Pdsnormes-->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "pdsnormes" , "sl"); ?> </P>
									<INPUT ID="DICT�pdsnormes"  CLASS=dogskininputreadonly readonly style="top:5px;left:95px;width:60px;text-align:right" onchange="ctrlQtePdsNormes();changePdsNormes();"    nature="NOMBRE"  >
									<a href="#" id="forcepdsnormes" class=dogskinbodyfont style="position:absolute;top:5px;left:162px;text-decoration:underline;font-weight:800;font-size:8pt;cursor:hand;display:none"  title="Forcer la valeur du poids au normes" onclick="saisiepdsnormes()">F</a>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:105px">
									<P class=dogskinbodyfont  style="top:5px;left:0px;font-weight:600" >Informations cellules</P>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:125px;width:200px;height:100px">
									<!--Code cellule -->
									<P class=dogskinbodyfont style="top:5px;width:55px"><?PHP print lect_bin::getLabel ( "cellule" , "sl"); ?> </P>
									<INPUT ID="DICT�cellule" NATURE=SEARCH class="dogskininput" style="top:5px;left:60px;width:70px">
                                    <INPUT CLASS=dogskininputreadonly readonly ID="DICT�cellule_libel" style="left:215px;top:5px;width:240px;display:none">
								</DIV>
								<DIV  style="position:absolute;left:150px;top:125px;width:200px;height:100px">
									<!--Code rotation et code du produit de la rotation -->
									<P class=dogskinbodyfont style="top:5px;width:70px">Produit stock�</P>
									<INPUT ID="DICT�rotation" class="dogskininputreadonly" readonly style="top:5px;left:75px;width:70px;display:none"       />
									<INPUT ID="DICT�rot-produit" class="dogskininputreadonly" readonly style="top:5px;left:75px;width:70px"       />
								</DIV>
								<DIV  style="position:absolute;left:10px;top:145px">
									<!--Libell� du produit de la rotation-->
									<P class=dogskinbodyfont style="top:5px;width:55px">Libell� </P>
									<INPUT ID="DICT�rot-produit_libel"  CLASS=dogskininputreadonly readonly  style="top:5px;left:60px;width:225px"  >
								</DIV>
								<DIV  style="position:absolute;left:10px;top:165px">
									<!--Capacit�-->
									<P class=dogskinbodyfont style="top:5px;width:55px">Capacit� </P>
									<INPUT ID="DICT�capacite"  CLASS=dogskininputreadonly readonly  style="top:5px;left:60px;width:70px;text-align:right"    nature="NOMBRE"  >
								</DIV>
								<DIV  style="position:absolute;left:150px;top:165px">
									<!--stock cellule-->
									<P class=dogskinbodyfont style="top:5px;width:55px">Stock</P>
									<INPUT ID="DICT�stock-cel"  CLASS=dogskininputreadonly readonly  style="top:5px;left:75px;width:70px;text-align:right"    nature="NOMBRE"  >
								</DIV>
								<DIV  style="position:absolute;left:10px;top:185px">
									<!--Combo vide cellule-->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "vide-cellule" , "sl"); ?> </P>
									<SELECT id="DICT�vide-cellule" class="dogskininput" style="left:170px;top:5px;width:70px"     >
										<?PHP deal_commun::charge_select("combo-ouinon-o",""); ?>
									</SELECT>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:205px">
									<!--solde vide-->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "solde-vide" , "sl"); ?> </P>
									<INPUT ID="DICT�solde-vide"  CLASS=dogskininputreadonly readonly  style="top:5px;left:170px;width:70px;text-align:right"    nature="NOMBRE"  >
								</DIV>

								<DIV  style="position:absolute;left:310px;top:45px">
									<!-- top validation -->
									<P class=dogskinbodyfont style="top:5px;width:100px">Top validation </P>
									<INPUT ID="DICT�cervalid"  class="dogskininput"  style="top:5px;left:170px;width:70px"         >
								</DIV>
								<DIV  style="position:absolute;left:600px;top:45px;visibility:hidden">
									<!-- echantillon-->
									<P class=dogskinbodyfont style="top:5px;width:100px"><?PHP print lect_bin::getLabel ( "echantillon" , "sl"); ?> </P>
									<INPUT ID="DICT�echantillon"  CLASS=dogskininputreadonly readonly style="top:5px;left:100px;width:140px"     >
								</DIV>
								<DIV style="position:absolute;left:310px;top:65px;visibility:hidden">
									<!--Combo saisie code prix -->
									<P class=dogskinbodyfont style="top:3px"><?PHP print lect_bin::getLabel ( "cer-codeprix" , "sl"); ?> </P>
									<INPUT ID="DICT�cer-codeprix" value="<?=$_mvt?>" NATURE=SEARCH class="dogskininput"  style="top:5px;left:170px;width:80px"       >
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�cer-codeprix_libel"  style="left:255px;top:5px;width:140px" >
								</DIV>
								<DIV  style="position:absolute;left:310px;top:85px">
									<!--Combo nettoyage-->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "nettoyage" , "sl"); ?> </P>
									<SELECT id="DICT�nettoyage" class="dogskininput" style="left:170px;top:5px;width:70px"     >
										<?PHP deal_commun::charge_select("combo-ouinon-on",""); ?>
									</SELECT>
								</DIV>
								<DIV  style="position:absolute;left:600px;top:85px">
									<!--Combo contenant propre-->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "contenant" , "sl"); ?></P>
									<SELECT id="DICT�contenant" class="dogskininput" style="left:170px;top:5px;width:70px"     >
										<?PHP deal_commun::charge_select("combo-ouinon-on",""); ?>
									</SELECT>
								</DIV>
								<DIV  style="position:absolute;left:310px;top:105px">
									<!--Combo desinsectis�-->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "insect" , "sl"); ?> </P>
									<SELECT id="DICT�insect" class="dogskininput" style="left:170px;top:5px;width:70px"     >
										<?PHP deal_commun::charge_select("combo-insecte",""); ?>
									</SELECT>
								</DIV>
								<DIV  style="position:absolute;left:600px;top:105px">
									<!-- date de traitement-->
									<P class=dogskinbodyfont style="top:5px;width:150px"><?PHP print lect_bin::getLabel ( "datrait" , "sl"); ?> </P>
									<INPUT ID="DICT�datrait"  class="dogskininput"  style="top:5px;left:170px;width:70px"      nature="DATE" >
								</DIV>
								<DIV  style="position:absolute;left:310px;top:125px">
									<!-- traitement -->
									<P class=dogskinbodyfont style="top:5px"><?PHP print lect_bin::getLabel ( "traitm" , "sl"); ?> </P>
									<INPUT ID="DICT�traitm" NATURE=SEARCH class="dogskininput" style="top:5px;left:170px;width:80px" OBLI="0">
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�traitm_libel"   style="left:255px;top:5px;width:240px">
								</DIV>
								<DIV  style="position:absolute;left:310px;top:145px">
									<!-- dosage -->
									<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "dosage" , "sl"); ?> </P>
									<INPUT ID="DICT�dosage"  class="dogskininput"  style="top:5px;left:170px;width:70px"        nature="NOMBRE" >
								</DIV>
								<DIV style="position:absolute;left:310px;top:165px">
									<!-- Dernier chargement et niveau de nettoyage associ� -->
									<P class=dogskinbodyfont style="top:5px;width:180px">Dernier chargement </P>
									<INPUT ID="DICT�protrs_1"        class="dogskininput"  style="top:5px;left:170px;width:270px"/>
									<INPUT ID="DICT�protrs_1_libel"  class="dogskininputreadonly" readonly style="top:5px;left:255px;width:185px;display:none"/>
                                    <INPUT ID="DICT�protrs_1_nivnet" class="dogskininputreadonly" readonly style="top:5px;left:450px;width:80px;display:none"/>

									<INPUT ID="DICT�protrs_2"        class="dogskininput"  style="top:25px;left:170px;width:270px"/>
									<INPUT ID="DICT�protrs_2_libel"  class="dogskininputreadonly" readonly style="top:25px;left:255px;width:185px;display:none"/>
                                    <INPUT ID="DICT�protrs_2_nivnet" class="dogskininputreadonly" readonly style="top:25px;left:450px;width:80px;display:none"/>

									<INPUT ID="DICT�protrs_3"        class="dogskininput"  style="top:45px;left:170px;width:270px"/>
									<INPUT ID="DICT�protrs_3_libel"  class="dogskininputreadonly" readonly style="top:45px;left:255px;width:185px;display:none"/>
                                    <INPUT ID="DICT�protrs_3_nivnet" class="dogskininputreadonly" readonly style="top:45px;left:450px;width:80px;display:none"/>
								</DIV>
								
                                <DIV  style="position:absolute;left:10px;top:225px">
                                    <!-- Pte 01/07/2016 date de chargement (Standard mais pour CAVAC: Date de livraison) -->
                                    <!-- <P class="dogskinbodyfont" style="top:5px;width:180px;left:0px">Date de chargement</P> -->
                                    <P class="dogskinbodyfont" style="top:5px;width:180px;left:0px"><?PHP print lect_bin::getLabel("datcharg", "sl");?></P>
                                    <INPUT ID="DICT�date-majo"  class="dogskininput"  style="top:5px;left:170px;width:70px" NATURE="DATE" />
                                </DIV>

                                <DIV  style="position:absolute;left:10px;top:245px">
									<!-- N� scell� -->
									<P class="dogskinbodyfont" style="top:5px;width:180px;left:0px">N� scell� 1</P>
									<INPUT ID="DICT�nscelle1"  class="dogskininput"  style="top:5px;left:80px;width:130px" />
									<P class="dogskinbodyfont" style="top:5px;width:180px;left:270px">N� scell� 2</P>
									<INPUT ID="DICT�nscelle2"  class="dogskininput"  style="top:5px;left:350px;width:130px" />
									<P class="dogskinbodyfont" style="top:5px;width:180px;left:520px">N� scell� 3</P>
									<INPUT ID="DICT�nscelle3"  class="dogskininput"  style="top:5px;left:600px;width:130px" />
								</DIV>
							</div>
						</div>

						<!-- Prestations-->
						<div id="contenuOng2" style="display:none" >
							<DIV id="DIVPDSN" >
								<DIV  style="position:absolute;left:10px;top:20px">
									<P class=dogskinbodyfont  style="top:5px;left:0px;font-weight:600" >Elements de prestation</P>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:40px">
									<!--Combo reprise en sac-->
									<P class=dogskinbodyfont style="top:5px;width:180px">Reprise en sac </P>
									<SELECT id="DICT�top-reprise" class="dogskininput" style="left:170px;top:5px;width:70px"    >
										<?PHP deal_commun::charge_select("combo-ouinon-on",""); ?>
									</SELECT>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:60px">
									<!--Combo mouture-->
									<P class=dogskinbodyfont style="top:5px;width:180px">Reprise en mouture </P>
									<SELECT id="DICT�top-mouture" class="dogskininput" style="left:170px;top:5px;width:70px"     >
										<?PHP deal_commun::charge_select("combo-ouinon-on",""); ?>
									</SELECT>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:80px">
									<!--Combo reprise en sac-->
									<P class=dogskinbodyfont style="top:5px;width:180px">Produit stock� </P>
									<SELECT id="DICT�stocke" class="dogskininput" style="left:170px;top:5px;width:70px"     >
										<?PHP deal_commun::charge_select("combo-ouinon-o",""); ?>
									</SELECT>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:100px">
									<!--Combo mouture-->
									<P class=dogskinbodyfont style="top:5px;width:180px">Produit manipul� </P>
									<SELECT id="DICT�manipule" class="dogskininput" style="left:170px;top:5px;width:70px"     >
										<?PHP deal_commun::charge_select("combo-ouinon-o",""); ?>
									</SELECT>
								</DIV>
							</DIV>
						</div>

						<!-- Info Achat -->
						<div id="contenuOng3" style="display:none" >
							<DIV id="DIVG" style="position:absolute;top:-20px">
								<DIV  style="position:absolute;left:10px;top:40px">
									<P class=dogskinbodyfont  style="top:5px;left:0px;font-weight:600" >Elements pour valorisation</P>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:60px">
									<!--Prix du bon-->
									<P class=dogskinbodyfont style="top:5px;width:180px">Prix du bon </P>
									<INPUT ID="DICT�cer-prix"  class="dogskininput"  style="top:5px;left:150px;width:90px" onblur="javascript:ctrlIdValue(this.id);"   nature="NOMBRE"  format="3" >
								</DIV>
								<DIV  style="position:absolute;left:10px;top:80px">
									<!--Prix unitaire du transport-->
									<P class=dogskinbodyfont style="top:5px;width:180px">Prix unitaire du transport </P>
									<INPUT ID="DICT�pu-transp"  class="dogskininput"  style="top:5px;left:150px;width:90px" onblur="javascript:ctrlIdValue(this.id);"   nature="NOMBRE"   format="2" >
								</DIV>
								<DIV  style="position:absolute;left:10px;top:100px">
									<!-- date echeance-->
									<P class=dogskinbodyfont style="top:5px;width:170px"><?PHP print lect_bin::getLabel ( "datech" , "sl"); ?> </P>
									<INPUT ID="DICT�datech"  class="dogskininput"  style="top:5px;left:150px;width:90px"  onblur="javascript:ctrlIdValue(this.id);"     nature="DATE" >
								</DIV>
								<!-- Mode reglement -->
								<DIV  style="position:absolute;left:10px;top:120px">
									<P class=dogskinbodyfont style="top:5px"><?PHP print lect_bin::getLabel ( "modreg" , "sl"); ?> </P>
									<INPUT ID="DICT�modreg" NATURE=SEARCH class="dogskininput" style="top:5px;left:150px;width:110px"  >
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�modreg_libel"   style="left:265px;top:5px;width:180px">
								</DIV>
								<DIV  style="position:absolute;left:10px;top:160px">
									<P class=dogskinbodyfont  style="top:5px;left:0px;font-weight:600" >Majorations</P>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:180px">
									<!-- calcul de la majoration -->
									<P class=dogskinbodyfont style="top:5px;width:180px">Calul de la majoration </P>
									<SELECT id="DICT�topmajps" class="dogskininput" style="left:170px;top:5px;width:70px"     >
										<?PHP deal_commun::charge_select("combo-ouinon-b1",""); ?>
									</SELECT>
								</DIV>

								<DIV  style="position:absolute;left:10px;top:200px">
									 <!-- Date d�but de la majoration -->
									<P class=dogskinbodyfont style="top:5px;width:170px">Date de d�but de calcul </P>
									<INPUT ID="DICT�date-majo"  class="dogskininput"  style="top:5px;left:170px;width:70px"  onblur="javascript:ctrlIdValue(this.id);"  onchange=""   nature="DATE" >
								</DIV>

								<DIV  style="position:absolute;left:300px;top:200px">
									<!-- valeur de la majoration -->
									<P class=dogskinbodyfont style="top:5px;width:180px">Valeur de la majoration </P>
									<INPUT ID="DICT�manu-majo"  class="dogskininput"  style="top:5px;left:170px;width:70px" onblur="javascript:ctrlIdValue(this.id);"   nature="NOMBRE" format="2" >
								</DIV>

							</DIV>
							<DIV id="DIVA" style="position:absolute;top:-20px" >
								<DIV  style="position:absolute;left:10px;top:240px">
									<P class=dogskinbodyfont  style="top:5px;left:0px;font-weight:600" >Divers</P>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:260px">
									<!--Combo top compl�ment de prix-->
									<P class=dogskinbodyfont style="top:5px;width:180px">Top compl�ment de prix </P>
									<SELECT id="DICT�top-complt" class="dogskininput" style="left:170px;top:5px;width:70px"     >
										<?PHP deal_commun::charge_select("combo-ouinon-on2",""); ?>
									</SELECT>
								</DIV>
								<DIV  style="position:absolute;left:260px;top:60px">
									<!-- poste-cpt -->
									<P class=dogskinbodyfont style="top:5px;width:180px;visibility:hidden">Poste imputation </P>
									<SELECT id="DICT�poste-cpta" class="dogskininput" style="left:170px;top:5px;width:170px;visibility:hidden"     >
										<?PHP deal_commun::charge_select("combo-postecpta",""); ?>
									</SELECT>
								</DIV>

							</DIV>
							<DIV id="DIVV" style="position:absolute;top:-20px;display:none" >
								<DIV  style="position:absolute;left:10px;top:240px">
									<P class=dogskinbodyfont  style="top:5px;left:0px;font-weight:600" >Divers</P>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:260px">
									<!--Combo calcul de tva-->
									<P class=dogskinbodyfont style="top:5px;width:180px">Calcul de tva </P>
									<SELECT id="DICT�top-tva" class="dogskininput" style="left:170px;top:5px;width:70px"     >
										<OPTION VALUE="" >TVA contrat</option>
										<OPTION VALUE="0" >Oui</option>
										<OPTION VALUE="1" >Non</option>
									</SELECT>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:280px">
									<!--Combo calcul de majoration-->
									<P class=dogskinbodyfont style="top:5px;width:180px">Calcul de majoration </P>
									<SELECT id="DICT�top-majo" class="dogskininput" style="left:170px;top:5px;width:70px"     >
										<?PHP deal_commun::charge_select("combo-ouinon",""); ?>
									</SELECT>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:300px">
									<!-- R�colte pour �dition facture -->
									<P class=dogskinbodyfont style="top:5px;width:180px">R�colte pour �dition facture </P>
									<INPUT ID="DICT�fac-rec"  class="dogskininput"  style="top:5px;left:170px;width:70px" onblur="javascript:ctrlIdValue(this.id);"   nature="NOMBRE"format="2" >
								</DIV>
								<DIV  style="position:absolute;left:10px;top:320px">
									<!-- Num�ro d'identification intracommunautaire -->
									<P class=dogskinbodyfont style="top:5px;width:180px">Num�ro d'identification intracommunautaire </P>
									<INPUT ID="DICT�intracom"  class="dogskininput"  style="top:5px;left:170px;width:100px" onblur="javascript:ctrlIdValue(this.id);"   nature="SEARCH" format="2"  >
								</DIV>
								<DIV  style="position:absolute;left:260px;top:60px">
									<!-- poste-cpt -->
									<P class=dogskinbodyfont style="top:5px;width:180px">Poste imputation </P>
									<SELECT id="DICT�poste-cptv" class="dogskininput" style="left:170px;top:5px;width:170px"     >
										<?PHP deal_commun::charge_select("combo-postecptv",""); ?>
									</SELECT>
								</DIV>
							</DIV>
							<DIV id="DIVC" style="position:absolute;top:-20px;display:none">
								<DIV  style="position:absolute;left:10px;top:340px">
									<P class=dogskinbodyfont  style="top:25px;left:0px;font-weight:600" >Commentaires</P>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:380px">
									<!--R�f�rence bon de livraison-->
									<P class=dogskinbodyfont style="top:5px;width:180px">R�f�rence bon de livraison </P>
									<INPUT ID="DICT�ref-bl"  class="dogskininput"  style="top:5px;left:170px;width:270px" onblur="javascript:ctrlIdValue(this.id);"   nature="STRING" >
								</DIV>
								<DIV  style="position:absolute;left:10px;top:400px">
									<!--Libell� fili�re-->
									<P class=dogskinbodyfont style="top:5px;width:180px">Libell� fili�re </P>
									<INPUT ID="DICT�lib-fil"  class="dogskininput"  style="top:5px;left:170px;width:270px" onblur="javascript:ctrlIdValue(this.id);"   nature="STRING" >
								</DIV>
								<DIV  style="position:absolute;left:10px;top:420px">
									<!--Destination finale-->
									<P class=dogskinbodyfont style="top:5px;width:180px">Destination finale </P>
									<INPUT ID="DICT�dest-fin"  class="dogskininput"  style="top:5px;left:170px;width:270px" onblur="javascript:ctrlIdValue(this.id);"   nature="STRING" >
								</DIV>
							</DIV>
						</div>

						<!-- Non conformit�s -->
						<div id="contenuOng4" style="display:none" >
							<DIV style="position:absolute;top:-20px" >
							<!-- -->
							<?php
							if($ident == "734")
							{
								?>

								<img id="" src="<?=$dealgateRelPath;?>/site/img/transparent.png" class="navImg tailleImg30 loupeImg30" title="" style="position:absolute;top:30px;left:600px;" onclick="visuNC()" title="Non Conformit�s" />
							<?php
							}
							?>
								<DIV  style="position:absolute;left:10px;top:40px">
									<!--code d�faut-->
									<P class=dogskinbodyfont style="top:5px;width:180px">Code d�faut </P>
									<INPUT ID="DICT�cod-def"  class="dogskininput"  style="top:5px;left:170px;width:90px" onblur="javascript:ctrlIdValue(this.id);"  nature="SEARCH" format="2" >
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�cod-def_libel"   style="left:265px;top:5px;width:180px">
								</DIV>
								<DIV  style="position:absolute;left:10px;top:80px">
									<!--Description du d�faut-->
									<P class=dogskinbodyfont style="top:5px;width:180px">Description du d�faut </P>
									<TEXTAREA ID="DICT�desc-def"  class="dogskininput"  style="top:5px;left:170px;width:470px;height:60px" onblur="javascript:ctrlIdValue(this.id);"  nature="STRING" format="2" ></TEXTAREA>
								</DIV>
								<DIV  style="position:absolute;left:10px;top:160px">
									<!-- cause du d�faut-->
									<P class=dogskinbodyfont style="top:5px;width:170px">Cause du d�faut </P>
									<TEXTAREA ID="DICT�cause-def"  class="dogskininput"  style="top:5px;left:170px;width:470px;height:60px" onblur="javascript:ctrlIdValue(this.id);"   nature="STRING" ></TEXTAREA>
								</DIV>
								<!-- Traitement du probl�me -->
								<DIV  style="position:absolute;left:10px;top:240px">
									<P class=dogskinbodyfont style="top:5px">Traitement du probl�me </P>
									<TEXTAREA ID="DICT�trait-def"  class="dogskininput" style="top:5px;left:170px;width:470px;height:60px" onblur="javascript:ctrlIdValue(this.id);"  nature="STRING"  ></TEXTAREA>
								</DIV>
								<!-- Action commerciale -->
								<DIV  style="position:absolute;left:10px;top:320px">
									<P class=dogskinbodyfont style="top:5px">Action commerciale </P>
									<TEXTAREA ID="DICT�act-def"  class="dogskininput" style="top:5px;left:170px;width:470px;height:60px" onblur="javascript:ctrlIdValue(this.id);"  nature="STRING"  ></TEXTAREA>
								</DIV>
								<!-- Ouverture fiche -->
								<DIV  style="position:absolute;left:10px;top:390px">
									<P class=dogskinbodyfont style="top:5px;width:180px">Ouverture fiche </P>
									<SELECT id="DICT�fic-def" class="dogskininput" style="left:170px;top:5px;width:70px"   onchange="if($('DICT�fic-def').value=='O'){zonscreenget('�nofic-def��1')}else{zonscreenget('�nofic-def��0')} "   >
										<?PHP deal_commun::charge_select("combo-ouinon-on",""); ?>
									</SELECT>
								</DIV>
								<!-- N� de fiche -->
								<DIV  style="position:absolute;left:10px;top:410px">
									<P class=dogskinbodyfont style="top:5px;width:180px">N� de fiche </P>
									<INPUT ID="DICT�nofic-def"  class="dogskininput" style="top:5px;left:170px;width:170px" onblur="javascript:ctrlIdValue(this.id);"   nature="NOMBRE" format="2" >
								</DIV>
							</DIV>
						</div>

						<!-- Mise � jour / motif -->
						<div id="contenuOng5" style="display:none" >
							<P class=dogskinbodyfont style="top:7px;left:15px;font-weight:600" >Mise � jour / motif</P>
							<DIV class=dogskinshapestyle style="width:370px;height:75px;top:22px;left:10px;"  >
								<DIV  style="position:absolute;left:1px;top:3px">
									<P class=dogskinbodyfont style="top:7px"><?=lect_bin::getLabel("opecre","sl")?></P>
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�opecre" style="left:80px;top:5px;width:80px">
								</DIV>
								<!-- Modifi� par, ?opemaj? -->
								<DIV  style="position:absolute;left:200px;top:3px">
									<P class=dogskinbodyfont style="top:7px"><?=lect_bin::getLabel("opemaj","sl")?></P>
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�opemaj" style="left:80px;top:5px;width:80px">
								</DIV>
								<!-- Cr�� le, ?datcre? -->
								<DIV  style="position:absolute;left:1px;top:23px">
									<P class=dogskinbodyfont style="top:7px"><?=lect_bin::getLabel("datcre","sl")?></P>
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�datcre" style="left:80px;top:5px;width:80px">
								</DIV>
								<!-- Modifi� le, ?datmaj? -->
								<DIV  style="position:absolute;left:200px;top:23px">
									<P class=dogskinbodyfont style="top:7px"><?=lect_bin::getLabel("datmaj","sl")?></P>
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�datmaj" style="left:80px;top:5px;width:80px">
								</DIV>
								<!-- Cr�� �, ?heucre? -->
								<DIV  style="position:absolute;left:1px;top:43px">
									<P class=dogskinbodyfont style="top:7px"><?=lect_bin::getLabel("heucre","sl")?></P>
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�heucre" style="left:80px;top:5px;width:80px">
								</DIV>
								<!-- Modifi� �, ?heumaj? -->
								<DIV  style="position:absolute;left:200px;top:43px">
									<P class=dogskinbodyfont style="top:7px"><?=lect_bin::getLabel("heumaj","sl")?></P>
									<INPUT CLASS=dogskininputreadonly readonly ID="DICT�heumaj"  style="left:80px;top:5px;width:80px">
								</DIV>
								</DIV>
							<DIV  style="position:absolute;left:10px;top:100px">
								<!--motif-->
								<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "motif" , "sl"); ?> </P>
								<TEXTAREA ID="DICT�motif2" class=dogskininputreadonly readonly style="position:absolute;left:0px;top:20px;height:75px;width:370px;resize:" ></TEXTAREA>
							</DIV>
						</div>
					</div>

				</div>


				<div><!-- div flottantes -->
					<!--//Pav� de saisie adresse tiers //-->
					<DIV ID="ADRESSE" class="divArrondiDeg26" style="position:absolute;left:100px;top:229px;height:180px;width:630px;display:none" >
						<div class="divArrondiDeg12" style="position:absolute;top:-1px;left:-1px;width:630px;height:30px;" >
							<P class=dogskinbodyfont style="top:7px;left:35px;font-weight:600" >Tiers</P>
							<img id="btn_closeAdresse" class="navImg tailleImg20 annulImg20" style="position:absolute;top:5px;right:5px;cursor:pointer"  src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="$('ADRESSE').hide();$('DICT�vide-cellule').focus"	   title="<?=lect_bin::getLabel('fermer','sl')?>" />
						</div>
						<DIV  style="position:absolute;left:10px;top:40px">
							<!-- adresse de livraison -->
							<P class=dogskinbodyfont style="top:5px"><?PHP print lect_bin::getLabel ( "cod-adrliv" , "sl"); ?> </P>
							<INPUT ID="DICT�cod-adrliv" NATURE=SEARCH class="dogskininput" style="top:5px;left:170px;width:80px"  />
							<INPUT class="dogskininput" ID="DICT�adr-liv_1"   style="left:280px;top:5px;width:240px"     />
							<INPUT class="dogskininput" ID="DICT�adr-liv_2"   style="left:280px;top:25px;width:240px"    />
							<INPUT class="dogskininput" ID="DICT�adr-liv_3"   style="left:280px;top:45px;width:240px"    />
							<INPUT class="dogskininput" ID="DICT�adr-liv_4"   style="left:280px;top:65px;width:240px"    />
							<INPUT class="dogskininput" ID="DICT�adr-liv_5"   style="left:280px;top:85px;width:240px"    />
						</DIV>
						<DIV  style="position:absolute;top:140px;right:5px">
							<img id="bt12" class="navImg tailleImg30 validImg30" style="position:absolute;top:5px;right:0px;cursor:pointer" src="<?=$dealgateRelPath?>/site/img/transparent.png"  title="Accepter la saisie et fermer ce pav�" onclick="validAdrliv()"/>
						</DIV>
					</DIV>

					<!--//Pav� de saisie des multicellules -->
					<DIV ID="MULTI" class="divArrondiDeg26" style="position:absolute;left:103px;top:53px;height:350px;width:630px;display:none;" >
						<div class="divArrondiDeg12" style="position:absolute;top:-1px;left:-1px;width:630px;height:25px;" >
							<img id="btn_closeMulti" class="navImg tailleImg20 annulImg20" style="position:absolute;top:5px;right:5px;cursor:pointer"  src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="sortieMulti()"	title="<?=lect_bin::getLabel('fermer','sl')?>" />
						</div>
						<P class=dogskinbodyfont style="top:30px;width:180px;left:25px"><?PHP print lect_bin::getLabel ( "cellule" , "sl"); ?> </P>
						<P class=dogskinbodyfont style="top:30px;width:180px;left:375px"><?PHP print lect_bin::getLabel ( "pdsnet" , "sl"); ?> </P>
						<P class=dogskinbodyfont style="top:30px;width:180px;left:455px"><?PHP print lect_bin::getLabel ( "solde-vide" , "sl"); ?> </P>
						<P class=dogskinbodyfont style="top:30px;width:180px;left:535px"><?PHP print lect_bin::getLabel ( "vide-cellule" , "sl"); ?> </P>
						<?php
							$top = 50;
							for($i = 1; $i < 13; $i++, $top += 20)
							{
								echo '
										<DIV  style="position:absolute;left:0px;top:' . $top . 'px;width:200px;height:200px">
											<!--Code cellule ' . $i . ' -->
											<INPUT ID="DICT�cellule_' . $i . '" NATURE=SEARCH CLASS="dogskininput" style="top:5px;left:20px;width:70px" onblur="dogexchange(this,\'GET\')"/>
											<INPUT ID="DICT�rotation_' . $i . '"  CLASS="dogskininput" style="top:5px;left:95px;width:70px"   />
											<INPUT CLASS=dogskininputreadonly readonly ID="DICT�cellule_' . $i . '_libel" style="left:170px;top:5px;width:195px" />
											<INPUT ID="DICT�pdsnet_' . $i . '"  CLASS=dogskininputreadonly readonly style="top:5px;left:370px;width:70px" onblur="ctrlPdsNet()"   nature="NOMBRE" format="3" />
											<INPUT ID="DICT�solde-vide_' . $i . '"  CLASS=dogskininputreadonly readonly  style="top:5px;left:450px;width:70px" nature="NOMBRE" format="3" />
											<SELECT id="DICT�vide-cellule_' . $i . '" class="dogskininput" style="left:530px;top:5px;width:70px"  >';
								deal_commun::charge_select("combo-ouinon-o","");
								echo		'</SELECT>
										</DIV>';
							}


						?>
						<DIV  style="position:absolute;left:30px;top:300px">
							<!--Pdsnet-->
							<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "pdsnet" , "sl"); ?> </P>
							<INPUT ID="CALC�pdsnet"  CLASS=dogskininputreadonly readonly style="top:5px;left:120px;width:70px" >
						</DIV>
						<DIV  style="position:absolute;left:270px;top:300px">
							<!--Solde-->
							<P class=dogskinbodyfont style="top:5px;width:180px">Solde </P>
							<INPUT ID="CALC�solde"  CLASS=dogskininputreadonly readonly style="top:5px;left:120px;width:70px" >
						</DIV>
<!--							<a href="#" id="bt11" class=dogskinbodyfont style="position:absolute;top:5px;left:10px;text-decoration:underline;font-weight:800;font-size:10pt;cursor:hand"  title="Accepter la saisie des cellules et fermer ce pav�" onclick="validMulti()">Accepter</a>-->
							<img id="btn_validMulti" class="navImg tailleImg30 validImg30" style="position:absolute;bottom:5px;right:5px;cursor:pointer"  src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="validMulti()"	title="Accepter la saisie des cellules et fermer ce pav�" />
					</DIV>

					<!--//Pav� de selection des �ch�ances -->
					<DIV ID="EXECUTION" class="divArrondiDeg26" style="position:absolute;left:103px;height:400px;top:53px;width:650px;display:none" >
						<div class="divArrondiDeg12" style="position:absolute;top:-1px;left:-1px;width:650px;height:30px;" >
							<img id="btn_closeExecution" class="navImg tailleImg20 annulImg20" style="position:absolute;top:5px;right:5px;cursor:pointer"  src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="sortieExclusion()"	title="<?=lect_bin::getLabel('fermer','sl')?>" />
							<P class=dogskinbodyfont  style="top:10px;left:60px;font-weight:bold;width:400px;" >Choix de la ligne d'ex�cution</P>
						</div>


						<DIV ID="divGridExec" class="" style="position:absolute;left:70px;top:58px;width:520px;height:244px" >

						</DIV>

						<DIV  style="position:absolute;top:320px;left:500px">
								<a href="#" id="bt11" class=dogskinbodyfont style="position:absolute;top:5px;left:10px;text-decoration:underline;font-weight:800;font-size:10pt;cursor:hand"  title="Accepter la saisie et fermer ce pav�" onclick="validExecution()">Accepter</a>
							</DIV>

					</DIV>

					<!--//Pav� de selection des �ch�ances -->
					<DIV ID="ECLATEMENT" class="divArrondiDeg26" style="position: absolute; left: 53px; height: 400px; top: 53px; min-width: 850px; width: 60%; display:none" >
						<div class="divArrondiDeg12" style="position:absolute; top:-1px; left:-1px; width:100%; height:30px;" >
							<P class=dogskinbodyfont  style="position:absolute;top:10px;left:60px;font-weight:600" >Eclatement du bon</P>
							<img id="btn_closeEclatement" class="navImg tailleImg20 annulImg20" style="position:absolute;top:5px;right:5px;cursor:pointer"  src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="sortieEclatement()"	title="<?=lect_bin::getLabel('fermer','sl')?>" />
						</div>

						<DIV ID="divGridEclat" class="" style="position:absolute; left:20px; top:58px; width:95%; height:244px" >
							<img id="btn_newEclat" src="<?=$dealgateRelPath;?>/site/img/transparent.png" class="navImg tailleImg20 ajoutImg20" title="Ajouter un �clatement" style="position:absolute;top:2px;left:5px;" onclick="ajoutEclatement()" />
							<img id="btn_supEclat" src="<?=$dealgateRelPath;?>/site/img/transparent.png" class="navImg tailleImg20 poubelleImg20" title="Supprimer un �clatement" style="position:absolute;top:2px;left:30px;" onclick="supprEclatement()" />
						</DIV>
						
						<DIV style="position:absolute;left:20px;top:195px">
							<DIV  style="position:absolute;left:10px;top:110px">
								<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "pdsbrut" , "sl"); ?> </P>
								<INPUT ID="DICT�pdsbrutini"  CLASS=dogskininputreadonly readonly  style="top:5px;left:70px;width:65px" >
							</DIV>
							<DIV  style="position:absolute;left:160px;top:110px">
								<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "tare" , "sl"); ?> </P>
								<INPUT ID="DICT�tareini"  CLASS=dogskininputreadonly readonly  style="top:5px;left:40px;width:65px" >
							</DIV>
							<DIV  style="position:absolute;left:290px;top:110px">
								<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "pdsnet" , "sl"); ?> </P>
								<INPUT ID="DICT�pdsnetini"  CLASS=dogskininputreadonly readonly style="top:5px;left:65px;width:65px;text-align:right" >
							</DIV>
							<DIV  style="position:absolute;left:430px;top:110px">
								<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "pdsnormes" , "sl"); ?> </P>
								<INPUT ID="DICT�pdsnormesini"  CLASS=dogskininputreadonly readonly style="top:5px;left:105px;width:65px;text-align:right" >
							</DIV>
							<DIV  style="position:absolute;left:620px;top:110px">
								<P class=dogskinbodyfont style="top:5px;width:180px">Reste � affecter </P>
								<INPUT ID="DICT�pdsresteini"  CLASS=dogskininputreadonly readonly style="top:5px;left:105px;width:65px;text-align:right" >
							</DIV>
						</DIV>
						<DIV  style="position:absolute;top:320px;left:700px">
							<!--<a href="#" id="bt11" class=dogskinbodyfont style="position:absolute;top:5px;left:10px;text-decoration:underline;font-weight:800;font-size:10pt;cursor:hand"  title="Accepter la saisie et fermer ce pav�" onclick="bonEclatValider()">Accepter</a>-->
							<img id="btn_acceptEclat" class="navImg tailleImg30 validImg30" style="position:absolute;top:10px;left:65px;cursor:pointer"  src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="bonEclatValider()"	title="Accepter la saisie et fermer ce pav�" />
						</DIV>
					</DIV>

					<!--//Pav� de saisie adresse tiers //-->
					<DIV ID="MOTIF" class="divArrondiDeg26" style="position:absolute;left:113px;top:53px;height:570px;width:700px;display:none;" >
						<div class="divArrondiDeg12" style="position:absolute;top:-1px;left:-1px;width:700px;height:30px;" >
							<P id="libmotif" class=dogskinbodyfont style="top:7px;left:35px;font-weight:bold;width:500px" >Motif de la demande / tra�abilit�</P>
							<img id="btn_closeMotif" class="navImg tailleImg20 annulImg20" style="position:absolute;top:5px;right:5px;cursor:pointer"  src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="sortieMotif()"	title="<?=lect_bin::getLabel('fermer','sl')?>" />
						</div>
						<DIV  style="position:absolute;left:10px;top:40px">
							<!--code d�faut-->
							<P class=dogskinbodyfont style="top:5px;width:180px">Code d�faut </P>
							<INPUT ID="DICT�cod-def1" NATURE=SEARCH class="dogskininput"  style="top:5px;left:170px;width:80px" onblur="ctrlIdValue('DICT�cod-def1')" onkeypress="javascript:if(event.keyCode == 13)dogexchange(this)" onkeydown="javascript:if(event.keyCode == 121)dogexchange(this)"  nature="STRING" format="2" >
							<INPUT CLASS=dogskininputreadonly readonly ID="DICT�cod-def1_libel"   style="left:270px;top:5px;width:180px">
						</DIV>
						<DIV  style="position:absolute;left:10px;top:80px">
							<!--Description du d�faut-->
							<P class=dogskinbodyfont style="top:5px;width:180px">Description du d�faut </P>
							<TEXTAREA ID="DICT�desc-def1"  class="dogskininput"  style="top:5px;left:170px;width:470px;height:60px" onblur="ctrlIdValue('DICT�desc-def1')"   nature="STRING" format="2" ></TEXTAREA>
						</DIV>
						<DIV  style="position:absolute;left:10px;top:160px">
							<!-- cause du d�faut-->
							<P class=dogskinbodyfont style="top:5px;width:170px">Cause du d�faut </P>
							<TEXTAREA ID="DICT�cause-def1"  class="dogskininput"  style="top:5px;left:170px;width:470px;height:60px" onblur="javascript:if(ctrlIdValue('DICT�cause-def1'));"   nature="STRING" ></TEXTAREA>
						</DIV>
						<!-- Traitement du probl�me -->
						<DIV  style="position:absolute;left:10px;top:240px">
							<P class=dogskinbodyfont style="top:5px">Traitement du probl�me </P>
							<TEXTAREA ID="DICT�trait-def1"  class="dogskininput" style="top:5px;left:170px;width:470px;height:60px" onblur=ctrlIdValue('DICT�trait-def1')  nature="STRING"  ></TEXTAREA>
						</DIV>
						<!-- Action commerciale -->
						<DIV  style="position:absolute;left:10px;top:320px">
							<P class=dogskinbodyfont style="top:5px">Action commerciale </P>
							<TEXTAREA ID="DICT�act-def1"  class="dogskininput" style="top:5px;left:170px;width:470px;height:60px" onblur=ctrlIdValue('DICT�act-def1')  nature="STRING"  ></TEXTAREA>
						</DIV>
						<!-- Ouverture fiche -->
						<DIV  style="position:absolute;left:10px;top:400px">
							<P class=dogskinbodyfont style="top:5px;width:180px">Ouverture fiche </P>
							<SELECT id="DICT�fic-def1" class="dogskininput" style="left:170px;top:5px;width:70px"   onchange="if($('DICT�fic-def1').value=='O'){zonscreenget('�nofic-def1��1')}else{zonscreenget('�nofic-def1��0')} "   >
								<?PHP deal_commun::charge_select("combo-ouinon-on",""); ?>
							</SELECT>
						</DIV>
						<!-- N� de fiche -->
						<DIV  style="position:absolute;left:10px;top:420px">
							<P class=dogskinbodyfont style="top:5px;width:180px">N� de fiche </P>
							<INPUT ID="DICT�nofic-def1"  class="dogskininput" style="top:5px;left:170px;width:170px" onblur="ctrlIdValue('DICT�nofic-def1')"   nature="NOMBRE" format="2" >
						</DIV>
						<DIV  style="position:absolute;left:10px;top:440px">
							<!--motif-->
							<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "motif" , "sl"); ?> </P>
							<TEXTAREA ID="DICT�motif" class=dogskininput style="position:absolute;left:170px;top:5px;height:50px;width:320px;resize:none"  isobli="" onfocus="" title=""></TEXTAREA>
						</DIV>
						<DIV  style="position:absolute;left:10px;top:500px">
							<!--Combo analyse du sol-->
							<P class=dogskinbodyfont style="top:5px;width:180px">Type de motif </P>
							<SELECT id="DICT�typ-motif" class="dogskininput" style="left:170px;top:5px;width:200px"   >
								<OPTION VALUE="NEG"  >Annulation seule </option>
								<OPTION VALUE="TOUS" SELECTED >Annulation + creation </option>
							</SELECT>
						</DIV>
						<DIV  style="position:absolute;left:10px;top:520px">
							<!--Code c�r�ale -->
							<P class=dogskinbodyfont style="top:5px;width:180px">C�r�ale de retour </P>
							<INPUT ID="DICT�codcer-ret" NATURE=SEARCH  class="dogskininput"  style="top:5px;left:170px;width:80px"  onblur="ctrlIdValue('DICT�codcer-ret')" onkeypress="javascript:if(event.keyCode == 13)dogexchange(this)" onkeydown="javascript:if(event.keyCode == 121)dogexchange(this)"  nature="CHAMP" isobli="0"  >
							<INPUT CLASS=dogskininputreadonly readonly ID="DICT�codcer-ret_libel" style="left:270px;top:5px;width:220px" >
						</DIV>
						<DIV  style="position:absolute;left:10px;top:540px;width:200px;height:200px">
							<!--Code cellule -->
							<P class=dogskinbodyfont style="top:5px;width:180px"><?PHP print lect_bin::getLabel ( "cellule" , "sl"); ?> </P>
							<INPUT ID="DICT�cellule-ret" NATURE=SEARCH class="dogskininput" style="top:5px;left:170px;width:80px"  onblur="javascript:ctrlIdValue('DICT�cellule-ret');" onkeypress="javascript:if(event.keyCode == 13)dogexchange(this)" onkeydown="javascript:if(event.keyCode == 121)dogexchange(this)"  nature="CHAMP" >
							<INPUT CLASS=dogskininputreadonly readonly ID="DICT�cellule-ret_libel" style="left:270px;top:5px;width:220px" >
						</DIV>

						<DIV  style="position:absolute;top:535px;left:560px">
							<!--<a href="#" id="bt13" class=dogskinbodyfont style="position:absolute;top:5px;left:10px;text-decoration:underline;font-weight:800;font-size:10pt;cursor:hand"  title="Accepter la saisie et fermer ce pav�" onclick="if($('DICT�motif').value.trim()!=''||$('DICT�motif').parentElement.style.visibility=='hidden'){$('MOTIF').style.visibility='hidden';topBascule=true;bascule();bonafficher(cur_lig,true,true);}">Accepter</a>-->
							<img id="btn_bonrefus" class="navImg tailleImg30 validImg30" style="position:absolute;top:0px;left:10px;cursor:pointer"  src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="bonrefus()"	title="Accepter la saisie et fermer ce pav�" />
						</DIV>
					</DIV>

					<!--//Pav� de saisie adresse tiers //-->
					<DIV ID="MOTIF2" class="divArrondiDeg26" style="position:absolute;left:13px;top:53px;width:900px;height:170px;display:none" >
						<div class="divArrondiDeg12" style="position:absolute;top:-1px;left:-1px;width:900px;height:30px;" >
							<P id="libmotif" class=dogskinbodyfont style="top:7px;left:35px;font-weight:600" >Date des nouveaux bons</P>
						</div>
							<DIV  style="position:absolute;left:10px;top:40px">
								<!--date de saisie-->
								<P class=dogskinbodyfont style="top:5px;width:180px">Date des bons </P>
								<INPUT ID="DICT�datsai"  class="dogskininput"  style="top:5px;left:170px;width:70px" maxlength="10"  nature="DATE" >
							</DIV>
							<DIV  style="position:absolute;top:130px;left:760px">
								<a href="#" id="bt14" class=dogskinbodyfont style="position:absolute;top:5px;left:10px;text-decoration:underline;font-weight:800;font-size:10pt;cursor:hand"  title="Accepter la saisie et fermer ce pav�" onclick="acceptMotif2()">Accepter</a>
							</DIV>
					</DIV>

					<DIV ID="TICKET" class="divArrondiDeg26" style="position:absolute;left:103px;top:53px;height:180px;width:630px;display:none" >
						<div class="divArrondiDeg12" style="position:absolute;top:-1px;left:-1px;width:630px;height:30px;" >
							<P class=dogskinbodyfont style="top:7px;left:35px;font-weight:600" >Saisir le num�ro de ticket</P>
							<img id="btn_closeTicket" class="navImg tailleImg20 annulImg20" style="position:absolute;top:5px;right:5px;cursor:pointer"  src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="sortieTicket()"	title="<?=lect_bin::getLabel('fermer','sl')?>" />
						</div>
						<DIV  style="position:absolute;left:10px;top:60px">
							<!--N� ticket -->
							<P class=dogskinbodyfont style="top:5px;width:180px">N� de ticket </P>
							<INPUT ID="DICT�ticket"  class="dogskininput"  style="top:5px;left:170px;width:120px"  maxlength=10 onkeypress="javascript:if(event.keyCode == 13)$('btticket').focus()"  >
						</DIV>

						<DIV  style="position:absolute;top:140px;left:540px">
							<a href="#" id="btticket" class=dogskinbodyfont style="position:absolute;top:5px;left:10px;text-decoration:underline;font-weight:800;font-size:10pt;cursor:hand"  title="Accepter la saisie et fermer ce pav�" onclick="if($('DICT�ticket').value.trim()!=''){$('TICKET').style.display='none';$('DICT�codmvt').focus()}">Accepter</a>
						</DIV>
					</DIV>

					<!--//Pav� de recherche -->
					<DIV ID="RECHBON" class="divArrondiDeg26" style="position:absolute;left:285px;height:120px;top:163px;width:380px;display:none" >
							<P class=dogskinbodyfont style="left:20px;top:7px;width:210px;font-weight:600;font-size:8pt">Recherche </P>
							<img id="btn_closeTicket" class="navImg tailleImg20 annulImg20" style="position:absolute;top:5px;right:5px;cursor:pointer"  src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="$('RECHBON').hide()"	title="<?=lect_bin::getLabel('fermer','sl')?>" />

						<DIV  style="position:absolute;left:20px;top:35px">
							<P class=dogskinbodyfont style="top:7px">Num�ro de contrat </P>
							<INPUT ID="DICT�num-ctr" NATURE=SEARCH  maxlength="10" class="dogskininput"  style="top:5px;left:135px;width:80px"    >
						</DIV>
					</DIV>

					<!--//Pav� de datation du bon -->
					<DIV ID="DATHEUBON" class="divArrondiDeg26" style="position:absolute;left:285px;height:50px;top:163px;width:250px;display:none" >
						<div class="" style="position:absolute;top:0px;left:15px;" >
							<P class=dogskinbodyfont style="position:absolute;left:0px;top:7px;width:95px;font-weight:600;font-size:8pt">Date du bon</P>
							<input id="DICT�datbon" class="dogskininput" style="position:absolute;top:5px;left:100px;width:80px" NATURE="DATE" />
						</div>
						<div class="" style="position:absolute;top:20px;left:15px;" >
							<P class=dogskinbodyfont style="position:absolute;left:0px;top:7px;width:95px;font-weight:600;font-size:8pt">Heure du bon</P>
							<input id="DICT�heubon" class="dogskininput" style="position:absolute;top:5px;left:100px;width:50px" />
						</div>
						<img id="btn_valdatheu" class="navImg tailleImg30 validImg30" style="position:absolute;top:5px;right:5px;cursor:pointer"  src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="daterBon()"	title="Valider" />
					</DIV>
					<!--//Pav� libell� transporteur destination finale -->
					<DIV ID="TRANSP" class="divArrondiDeg26" style="position:absolute;left:285px;height:50px;top:163px;width:380px;display:none" >
						<div class="" style="position:absolute;top:0px;left:15px;" >
							<P class=dogskinbodyfont style="position:absolute;left:0px;top:7px;width:125px;font-weight:600;font-size:8pt">Nom du bateau</P>
							<input id="DICT�transp_libel2" class="dogskininput" style="position:absolute;top:5px;left:120px;width:180px"  />
						</div>
						<div class="" style="position:absolute;top:20px;left:15px;" >
							<P class=dogskinbodyfont style="position:absolute;left:0px;top:7px;width:95px;font-weight:600;font-size:8pt">Destination finale</P>
							<input id="DICT�dest-fin2" class="dogskininput" style="position:absolute;top:5px;left:120px;width:180px" />
						</div>
						<img id="btn_transp" class="navImg tailleImg30 validImg30" style="position:absolute;top:5px;right:5px;cursor:pointer"  src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="validationPeniche()"	title="Valider" />
					</DIV>
					<!-- Pav� de saisie de date d'un bon de transfert de sortie/d'entr�e d'un site non informatis� -->
					<div id="divSaisieDateHeureTransfert" class="divArrondiDeg26" style="position:absolute;top:230px;left:480px;width:230px;height:70px;display:none">
						<span id="titreRec" class="dogskinbodyfont" style="position:absolute;left:0px;top:5px;width:230px;text-align:center;font-weight:bold">Titre</span>
						<div class="" style="position:absolute;top:20px;left:10px;">
							<span class="dogskinbodyfont" style="position:absolute;left:5px;top:7px;width:70px;">Date</span>
							<input id="dateRec" class="dogskininputreadonly" readonly style="position:absolute;left:75px;width:80px;top:5px" NATURE="DATE" />
						</div>
						<div class="" style="position:absolute;top:40px;left:10px;">
							<span class="dogskinbodyfont" style="position:absolute;left:5px;top:7px;width:70px;">Heure</span>
							<input id="heureRec" class="dogskininputreadonly" readonly style="position:absolute;left:75px;width:80px;top:5px" />
						</div>
						<img id="btn_valdatheu" class="navImg tailleImg30 validImg30" style="position:absolute;top:25px;right:15px;cursor:pointer"  src="<?=$dealgateRelPath?>/site/img/transparent.png" onclick="validerDateRec()"	title="Valider (F2)" />
					</div>
				</div>
			</div>
			<DIV class=""  style="position:absolute;height:20px;width:945px;width:calc(100% - 20px);right:5px;bottom:10px;">
				<div id="bt9" class="divArrondiDeg31" style="position:absolute;width:110px;height:20px;cursor:pointer;top:0px;left:0px;" onclick="adrliv()">
					<p  class="dogskinbodyfont" style="position:absolute;top:3px;left:0px;text-decoration:;font-weight:bold;text-align:center;width:110px" title="Adr Liv (F4)" >Adr Liv (F4)</p>
				</div>
				<div id="bt10" class="divArrondiDeg31" style="position:absolute;width:110px;height:20px;cursor:pointer;top:0px;left:115px;display:none" onclick="boneclatement()">
					<p class="dogskinbodyfont" style="position:absolute;top:3px;left:0px;text-align:center;font-weight:bold;width:110px"  title="Eclatement des bons" >Eclatement</p>
				</div>
				
				<!-- GENERATION +/- -->
				<div id="bt1" class="divArrondiDeg31" style="position:absolute;width:110px;height:20px;cursor:pointer;top:0px;left:230px;" onclick="bonavoir()">
					<p class="dogskinbodyfont" style="position:absolute;top:3px;left:0px;text-align:center;font-weight:bold;width:110px"  title="G�n�ration +/-" >G�n�ration +/-</p>
				</div>

			</DIV>
		</div>
		<a id="link-visu"  target="_blank" style="top:5px;left:0px;display:none" ></a>
		<div style="display:none">
			<OBJECT ID="ctldeal"
				CLASSID="CLSID:532366BE-4342-47D0-8DDE-146EB1E21B34"
				CODEBASE="dealdownloadtools.ocx#version=1,0,0,0"
				style="visibility:hidden"></OBJECT>
		</div>
	</body>
</html>
<style type="text/css">
    .aw-image-selgrid_ { background:url(<?=$dealgateRelPath?>/site/img/selgrid_.png) 100% 100% ; position:relative; left: 10px;  background-repeat: no-repeat; }
    .aw-image-selgrid  { background:url(<?=$dealgateRelPath?>/site/img/selgrid.png)  100% 100% ; position:relative; left: 10px;background-repeat: no-repeat;}
</style>

<script langage=javascript>
/**** GRID ECLATEMENT *********/

/*
Fonctionnement "SPE" (si on a un codmvt d'origine d�fini dans param�trage tabcer => PRM/AS-BONAPP/MVT-ORI)

[NOUVEAU]
Eclatement un nouveau mouvement si qte saisie > qte contrat.
=> Cr�ation d'un mouvement "origine" avec codmvt param�tr�
=> cr�ation d'autant de mouvement que saisi dans le grid des �clatements.

[MODIFICATION]
Si on modifie un mouvement affili� � un contrat:
=> SI la diff�rence de poids (nouveau poids - ancien poids) est > 0 OU > qte restante au contrat
	- On �clate le reste du poids comme un �clatement classique SAUF qu'on ne g�n�re pas de mvt d'origine.

=> SINON on valide le mouvement qui met � jour la qte dans le contrat.
*/

var isSaisieEnCours3 = false;
var listeChampsSaisie = [getColEclat('typauxecl') , getColEclat('codauxgesecl') , getColEclat('codmvtecl') , getColEclat('contrat') , getColEclat('option') , getColEclat('pdsNormes')];
gridEclat.init = function()
{
	var largeurGridEclat = '98.8%';
	var hauteurGridEclat = '88%';
	createTableau("divGridEclat","tabGridEclat", hauteurGridEclat, largeurGridEclat, "gridEclat");
	
	//on charge le tableau vide avec les bons libell�s et les largeurs
	var varHeaderT = strcollibelGridEclat.split("�");
	var varColWidthT = strcolwidthGridEclat.split("�");
	
	this.setId("tabGridEclat");
	$("divGridEclat").style.paddingTop = "1px";
	
	initTableau(this, varHeaderT, varColWidthT, hauteurGridEclat, largeurGridEclat);
	
	// this.setColumnCount(varHeaderT.length);
	chargeGridFormat(strenteteGridEclat, strcolalignGridEclat, strcolformatGridEclat, this, strcolwidthGridEclat, strcollibelGridEclat);
	
	// this.setColumnIndices([0,1,2,3,4,5,6,7,8,9]);

	if(!eclatementSPE)
	{
		var combo = new AW.Formats.Combo;
		var texte = ['Poids au normes', 'Poids Net'];
		var valeurs = ['NORMES', 'NET'];
		var options = new Array(texte, valeurs);
		
		combo.setTextFormat(options);
		this.setCellFormat(combo, getColEclat("typcal"));
		this.setCellCombo(true, getColEclat("typcal"));		
	}
	
	this.setCellTemplate(new AW.Templates.F10, getColEclat("typauxecl"));
	this.setCellTemplate(new AW.Templates.F10, getColEclat("codauxgesecl"));
	this.setCellTemplate(new AW.Templates.F10, getColEclat("codmvtecl"));
	this.setCellTemplate(new AW.Templates.F10, getColEclat("contrat"));
	this.setCellTemplate(new AW.Templates.F10, getColEclat("option"));

	this.setSelectionMode("single-cell");
	this.setCellEditable(true);
	
	if(eclatementSPE)
	{
		this.setCellEditable(false, getColEclat('typauxecl')); //colonne typaux non saisissable
		this.setCellEditable(false, getColEclat('codauxgesecl')); //colonne codaux non saisissable		
	}
	
	this.setCellEditable(false, getColEclat('adresse')); //colonne libelle non saisissable

	this.onCellDoubleClicked = function(event, col, row)
	{
		trace("--onCellDoubleClicked col: "+col+" - row: "+row+" --")
		
		if(eclatementSPE && !modifBon && row == 0)
		{
			isSaisieEnCours3 = false;
			return true;
		}
		
		if(isSaisieEnCours3)
			return;
		else
		{
			//Si col n'est pas dans la liste ces colonnes �ditables => RETURN
			if(listeChampsSaisie.indexOf(col) == -1)
				return;
			else
			{
				isCreation3 = true;
				this.startCellEdit();
			}
		}
	}
	
	//A J O U T   L I G N E   S P E
	this.addRowEmptySPE = function()
	{
		trace("--gridEclat.addRowEmptySPE--")
		trace(isCreation3)
	
		if(isCreation3)
			return;
			
		var posCreation = this.getRowCount();
		
		var typaux 		= $('DICT�typaux').value;
		var codaux 		= $('DICT�codauxges').value;
		var libel 		= $('DICT�codauxges_libel').value;
		var mvt 		= $('DICT�codmvt').value;
		var contrat 	= $('DICT�cer-contrat').value;
		var option 		= $('DICT�natcon').value;
		var pdsNorme 	= "0.00";
		
		deltaQteModifBon = (((parseFloatStr($('DICT�pdsnormes').value)*100000)-(oldPdsNorme*100000))/100000).toFixed(3);
		
		qteContrat = parseFloatStr(getField(RspContrat[1],'TON-SOLDE').trim());
		
		var newRow;
		//SI c'est la premi�re ligne ET ( que l'on est pas en modif de bon OU que le pdsNormes apr�s modif <= pdsNormes avant modif )
		//ALORS on cr�� la ligne en pr�chargeant le contrat en cours.
		//		on cr�� une 2e ligne avec le m�me mvt et avec une quantit� pr�charg�e � 0.00
		if(posCreation == 0 && (!modifBon || deltaQteModifBon <= 0))
		{
			trace("	posCreation == 0 && (!modifBon || deltaQteModifBon <= 0)")
			//Premi�re ligne d'�clatement contenant le contrat prix avec qte contrat. NON MODIFIABLE
			newRow = [typaux , codaux , libel , mvt , contrat , option , getField(RspContrat[1] , 'TON-SOLDE').trim()];
			dataPdsNormes.push(parseFloatStr(getField(RspContrat[1] , 'TON-SOLDE')));
			dataGridEclat.push(newRow);
			
			newRow = [typaux , codaux , libel , mvt , '' , '' , pdsNorme];				
		}
		//SINON SI ( on modifie un bon ET  que le pdsNormes apr�s modif > pdsNormes avant modif ) OU ( on modifie un bon ET  que le pdsNormes apr�s modif - pdsNormes avant modif > � la qte du contrat)
		//ALORS on cr�� une 2e ligne avec le m�me mvt et avec une quantit� pr�charg�e � 0.00
		else if((modifBon && deltaQteModifBon > 0) || (modifBon && deltaQteModifBon > qteContrat))
		{
			trace("	(modifBon && deltaQteModifBon > 0) || (modifBon && deltaQteModifBon > qteContrat)")
			
			if(gridEclat.getRowCount() == 0)
			{
				newRow = [typaux , codaux , libel , mvt , contrat , option , getField(RspContrat[1] , 'TON-SOLDE').trim()];
				dataPdsNormes.push(parseFloatStr(getField(RspContrat[1] , 'TON-SOLDE')));
				dataGridEclat.push(newRow);
			}
			
			//Cas d'une modif pds mouvement existant. Nouveau poid > ancien poids.
			newRow = [typaux , codaux , libel , mvt , '' , '' , 0.00];
			dataPdsNormes.push(deltaQteModifBon);
			// dataGridEclat.push(newRow);
		}
		//SINON SI la quantit� saisie sur la derni�re ligne  = 0
		//ALORS on ne rajoute pas de ligne.
		else if(parseFloatStr(dataGridEclat[posCreation-1][getColEclat('pdsNormes')]) == 0)
		{
			trace("	parseFloatStr(dataGridEclat[posCreation-1][getColEclat('pdsNormes')]) == 0")
			return;
		}
		//SINON on cr�� une nouvelle ligne avec le m�me mvt et avec une quantit� pr�charg�e � 0.00
		else
		{
			trace("	ELSE")
			newRow = [typaux , codaux , libel , mvt , '' , '' , pdsNorme];
		}
		
		
		rspContratsEclat[0] = RspContrat[1];
		
		dataGridEclat.push(newRow);
		
		this.setCellData(dataGridEclat);
		this.setRowCount(dataGridEclat.length);
		this.refresh();
		
		//on rentre en modification de la cellule
		this.selectCell(getColEclat('codmvtecl'), dataGridEclat.length - 1);
		
		majQteRestanteEclatement();
		
		isCreation3 = true;
		
		setTimeout("gridEclat.focus();", 100);
		setTimeout("gridEclat.startCellEdit();", 200);
		keycode = 0;
		
		trace("fin addRowEmpty");
	}
	
	//A J O U T   L I G N E   S T A N D A R D
	this.addRowEmpty = function()
	{
		try{
		trace("---gridEclat.addRowEmpty")
		if(isCreation3)
			return;
		//on r�cup�re les champs � afficher
		exp_fieldtoget = strfiltreGridEclat.split("�");
		//Cette fonction permet d'ajouter une ligne vide
		var newRow = [];
		for(var i= 0; i < exp_fieldtoget.length; i++)
			newRow.push("");
		//on rend la ligne visible
		newRow[getColEclat("typauxecl")] = $("DICT�typaux").value;
		newRow[getColEclat("codauxgesecl")] = $("DICT�codauxges").value;
		newRow[getColEclat("adresse")] = $("DICT�codauxges_libel").value;
		newRow[getColEclat("codmvtecl")] = $("DICT�codmvt").value;
		newRow[getColEclat("typcal")] = "NORMES";
		dataGridEclat.push(newRow);
		this.setCellData(dataGridEclat);
		this.setRowCount(dataGridEclat.length);
		this.refresh();
		//on rentre en modification de la cellule
		this.selectCell(0, dataGridEclat.length - 1);
		isCreation3 = true;
		setTimeout("gridEclat.focus();", 100);
		setTimeout("gridEclat.startCellEdit();", 200);
		trace("fin addRowEmpty")
		}catch(err){trace(err)}
	}

	this.onCellEditStarting = function (text, col, row)
	{
		trace("--gridEclat.onCellEditStarting--");
		trace("	keycode : " + keycode);
		this.setCellEditable(true, getColEclat('option'));		
		isSaisieEnCours3 = true;
		keycode = 0;
	}
	
	this.onCellValidating = function (value, col, row)
	{
		trace("--gridEclat.onCellValidating col: "+col+" | row: "+row+" | value: "+value+" --")
		trace("	keycode : " + keycode);
		
		try{
			col = parseInt(col);
			row = parseInt(row);
					
			if(keycode == 999 ||keycode == 0 || keycode == 1111)
				return true;
			
			else if(keycode == 27)
				return false;
			
			strId = AW._edit + "-box-text";
			trace("strId == " + strId);
			
			switch(parseInt(col))
			{
				/** TYPAUX **/
				case getColEclat("typauxecl") :
					try{dogexchange($(strId), "GET", false)}catch(err){trace("dans le validating : " +err)};
					if(Rsp[0] != "OK") return true;
					keycode = 13;
					break;
				
				/** CODAUX **/
				case getColEclat("codauxgesecl") :
					try{dogexchange($(strId), "GET", false)}catch(err){trace("dans le validating : " +err)};
					if(Rsp[0] != "OK") return true;
					keycode = 13;
					break;
					
				/** CODMVT **/
				case getColEclat("codmvtecl") :
					try{dogexchange($(strId), "GET", false)}catch(err){trace("dans le validating : " +err)};
					if(Rsp[0] != "OK") return true;
					
					if(eclatementSPE)
					{
						//on teste si le couple mvt / cereale est autorise
						var requete = "�CODSOC�<?=$c_codsoc?>�CODCER�" + $("DICT�codcer").value + "�CODMVT�" + value+"�CAMPAGNE�"+$("DICT�campagne").value+"�MAGASI�"+$("DICT�magasi").value;
						var dogFct = "PRM-CODMVT-CODCER�a-parapp";
						var retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , requete , dogFct, "" , "" )

						if(getValue ( "MVTCER-AUTORI" ,false ,"" ,""  , true)=="1")
						{
							alert("Le couple Code Mouvement/Code c�real saisi n'est pas autoris�, veuillez en saisir un autre !")
							this.setCellData("",col,row);
							return true;
						}						
					}
					keycode = 13;
					break;
					
				/** CONTRAT **/
				case getColEclat("contrat") :
					try{
						if(value != "")
							dogexchange($(strId), "GET", false);
					} catch(err){trace("dans le validating : " +err)};
										
					if(Rsp[0] != 'OK')
						return true;
										
					if(!contratEclatOK)
					{
						alert("Contrat d�j� utilis�.");
						gridEclat.setCellData("",getColEclat("contrat"),row);
						gridEclat.selectCell(getColEclat("contrat"),row);
						gridEclat.startCellEdit();
						contratEclatOK = true
						keycode = 999;
						return true;
					}
					
					// alert(keycode)
					
					if(value == "" && eclatementSPE)
						return !ctrlSaisieEclatementSPE();
					
					// alert(keycode)
					
					//Si on r�cup�re l'option => bloque la saisie de l'option
					if(eclatementSPE && this.getCellData(getColEclat("contrat"),row) != "")
						this.setCellEditable(false, getColEclat('option'));
					
					/* //En cas de saisie d'un contrat, on bloque la saisie de la quantit�.
					if(value != "")
					{
						keycode = 27;
						this.setCellEditable(false, getColEclat("pdsNormes"), row);
						isCreation3 = false;
						majQteRestanteEclatement();
						return false;
					}
					else
						this.setCellEditable(true, getColEclat("pdsNormes"), row); */
						
					
					if(keycode != 1111)
						keycode = 13; //Pour le cellEditEnded
					break;
					
				/** OPTION **/
				case getColEclat("option") :
					try{dogexchange($(strId), "GET", false)}catch(err){trace("dans le validating : " +err)};
					if(Rsp[0] != "OK") return true;
					
					if(value == "")
						return true;
					
					//On s'assure que la cellule du poids aux normes est bien saisissable.
					this.setCellEditable(true, getColEclat("pdsNormes"), row);
										
					keycode = 13;
					break;
				
				/** POIDS NORMES **/
				case getColEclat("pdsNormes") :					
					if(parseFloatStr(value) == 0.0 || value.trim() == "")
						return true;
					
					if(eclatementSPE)
					{						
						if(this.getCellData(getColEclat('contrat'),row) == "" && this.getCellData(getColEclat('option'),row) == "")
						{
							alert("Option obligatoire.");
							this.setCellData("",col,row);
							return true;
						}
					}
					else
					{
						var inputParameter = "�CODSOC�<?=$c_codsoc?>�CAMPAGNE�"+$("DICT�campagne").value.trim()+"�CODCER�"+$("DICT�codcer").value.trim();
						inputParameter += "�CODMVT�" + this.getCellText(getColEclat("codmvtecl"), row);
						inputParameter += "�PDSNET�" + this.getCellText(getColEclat("pdsNormes"), row);
						
						if(this.getCellValue(getColEclat("typcal"), row)=="NORMES")
							inputParameter += "�TYPCAL�BRUT";
						
						for(kk=1;kk<=14;kk++)
							inputParameter += "�val-etat_"+kk + "�" + $("DICT�val-etat_"+kk).value;
						
						inputParameter += "�REGION�001";
						
						dogFonction = "pdsnorm�a-pdsnorm";
						retourExchange  = sendExchangeInvoke ( "<?=$dogExchange?>" , inputParameter , dogFonction , "" , "" );
						
						dataPdsNormes[row] = getValeurRsp('PDSNORMES');
						this.setCellData(getValeurRsp('PDSNORMES'),col,row);
					}

					
					var total = 0;
					for(var i=0; i<this.getRowCount(); i++)
						if(gridEclat.getCellText(getColEclat("pdsNormes") , i) != "")
							total += parseFloatStr(this.getCellText(getColEclat("pdsNormes") , i));
					
					total = total.toFixed(3);
					
					var pds = parseFloatStr($('DICT�pdsnormesini').value);
					var reste = ((100000*pds)-(100000*total))/100000;
					
					if(Math.abs(parseFloat(reste).toFixed(3)) < 0)
					{
						this.setCellData("",col,row);
						alert("Quantit� trop importante!")
						this.startCellEdit();
						keycode=999;
						return true;
					}
					else
					{
						// trace("	CALCULE RESTE APRES VALIDATION")
						$('DICT�pdsresteini').value = Math.abs(parseFloat(reste).toFixed(3));
					}
					
					keycode = 13;
				break;
			}
			return false;
		}catch(err){trace("ERR sur onCellValidating -> " + err)}
	}

	this.onCellEditEnded = function (text, col, row)
	{
		trace("--gridEclat.onCellEditEnded col: "+col+" | row: "+row+" | text: "+text+" --")
		trace("	keycode : " + keycode);
	
		try{
			
		//Sur eclatementSPE on ne modifie pas la 1e ligne (SAUF le cas d'une modif)
		if(eclatementSPE && row == 0 && (!modifBon || deltaQteModifBon <= 0))
			return true;
			
		col = parseInt(col);
		row = parseInt(row);	
		isSaisieEnCours3 = false;
		var pdsresteini = parseFloatStr($('DICT�pdsresteini').value);
		
		//keycode = 1111 ==> on refuse la validation et on repart en modif.
		if(keycode == 1111)
		{
			this.selectCell(getColEclat('CONTRAT'),row);
			keycode = 0;
			this.startCellEdit();
			return true;
		}
		else if(keycode == 27 || keycode == 999)
		{
			if(this.getCellData(getColEclat("pdsNormes"),row) == "")
			{
				this.setCellData(0.00,getColEclat("pdsNormes"),row)
				gridEclat.refresh();
				
				majQteRestanteEclatement();
				
				isCreation3 = false;
			}
		}
		else
		{			
			
			dataGridEclat[row][col] = text;
			trace(dataGridEclat[row][col])
			//TEST SI DERNIERE COLONNE
			if(col < strfiltreGridEclat.split("�").length-2)
			{			
				if((keycode != 13 && keycode != 39))
				{
					gridEclat.refresh();
					isCreation3 = false;
				}
				else
				{				
					switch(col)
					{
						/** OPTION **/
						case getColEclat('option'):
							//Si mouvement sans contrat => on pr�charge la qte totale restante � �clater.
							if(this.getCellData(getColEclat("contrat"),row) == "")
							{
								trace("			col == getColEclat('option') && this.getCellData(getColEclat('contrat'),row) == ''")

								if(pdsresteini > 0)
									this.setCellData(pdsresteini,getColEclat("pdsNormes"),row);
							}
							break;
						
						/** CONTRAT **/
						case getColEclat('contrat'):
							//Si qte restante < qte contrat saisi => on pr�charge la qte totale restante � �clater.
							if(pdsresteini < parseFloatStr(getField(RspContratEclat,'TON-SOLDE')))
								this.setCellData(pdsresteini,getColEclat("pdsNormes"),row);
							
							//Si on a saisi un contrat on ne peut pas modifier l'option => on passe � la col suivante.
							if(this.getCellData(getColEclat("contrat"),row) != "")
								col++;
							break;
							
						/** ADRESSE **/
						case getColEclat("adresse"):
							//On ne saisie pas l'adresse.
							col++
							break;
					}
					
					majQteRestanteEclatement();
					
					col++;
					
					this.selectCell(col, row);
					setTimeout("gridEclat.startCellEdit();gridEclat.$active=false;");
				}
			}
			else
			{
				//ajout nouvelle ligne si reste > 0
				isCreation3 = false;
				keycode = "";
				if(parseFloatStr($("DICT�pdsresteini").value.trim()) > 0.0)
					(eclatementSPE)? this.addRowEmptySPE():this.addRowEmpty();
				return false;
			}

		}
		keycode = 0;
		return false;
		}
		catch(err){trace("erreur sur le onCellEditEnded : " + err)}
	}

	this.onKeyPress = function(e)
	{
		// Event.stop(e);
	}

	this.onKeyEnter=function(e)
	{
		trace("--gridEclat.onKeyEnter--")
		trace("onKeyEnter");
		Event.stop(e);
		keycode=13;
	}
	this.onKeyRight = function(e)
	{
		trace("--gridEclat.onKeyRight--")
		Event.stop(e);
		keycode=39;
	}

	this.onKeyF2=function(e)
	{
		trace("--gridEclat.onKeyF2--")
		Event.stop(e);
		this.focus();
		// setTimeout("ligneSave1()");
		setTimeout("gridEclat.focus()");
	}

	this.onKeyF9 = function(e)
	{
		trace("--gridEclat.onKeyF9--")
		Event.stop(e);
		this.addRowEmpty();
	}

	this.onKeyDelete = function(e)
	{
		trace("--gridEclat.onKeyDelete--")

		if(!isSaisieEnCours3)
		{
			return true;
			// Event.stop(e);
			var row = parseInt(this.getCurrentRow());
			// dataGridEclat.splice(row, 1);
			// this.setCellData(dataGridEclat);
			// this.setRowCount(dataGridEclat.length);
			this.refresh();
		}
	}

	this.onKeyEscape=function(e)
	{
		trace("--gridEclat.onKeyEscape--")

		keycode=27;
		Event.stop(e);
		setTimeout("gridEclat.focus()")
		isCreation3 = false;
		isSaisieEnCours3 = false;
	}

	this.onControlDeactivated = function(e)
	{
		trace("--gridEclat.onControlDeactivated--")
		keycode = 999;
	}

	this.setCellData(dataGridEclat);
	this.setRowCount(dataGridEclat.length);
	this.refresh();
}
/**** FIN GRID ECLATEMENT ******/
</script>
