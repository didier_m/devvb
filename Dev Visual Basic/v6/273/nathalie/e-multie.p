/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/src/e-multie.p                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52 10002 23-07-04!New!cch            !Développement Etats des Stocks Articles Multipro.                     !
!-------------------------------------------------------------------------------------------------------------!
!                             I N C L U D E S                !                    T A B L E S                 !
!------------------------------------------------------------+------------------------------------------------!
!connect.i             !                                     !tables                                          !
!maquette.i            !                                     !                                                !
!e-multi0.i            !                                     !                                                !
!maq-maj1.i            !"05" "val-edi"                       !                                                !
!maq-edi.i             !"05"                                 !                                                !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E - M U L T I E . P                            */
/* Etat des Stocks Articles Multipro                                        */
/*--------------------------------------------------------------------------*/
/* Edition entete                                             CC 15/07/2004 */
/*==========================================================================*/

{ connect.i  }
{ maquette.i }
{ e-multi0.i }

assign ma-edient     = "O"
       no-page       = no-page + 1

       val-edi[ 1 ] = right-trim( codsoc-soc )
       val-edi[ 2 ] = string( no-page )
       val-edi[ 3 ] = string( date-min, "99/99/9999" )
       val-edi[ 4 ] = string( date-max, "99/99/9999" )
       .

FIND TABLES where tables.codsoc = codsoc-soc
            and   tables.etabli = codetb-etb
            and   tables.typtab = "ETA"
            and   tables.prefix = "ADRESSE"
            and   tables.codtab = ""
            no-lock no-error .

if available tables then val-edi[ 1 ] = val-edi[ 1 ] + " " + right-trim( tables.libel1[ 1 ] ) .

{ maq-maj1.i "05" "val-edi" }
{ maq-edi.i "05" }

ma-edient = "" .