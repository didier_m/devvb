/*==========================================================================*/
/*                    ***   E C R - R I S T . I   ***                       */
/*==========================================================================*/
 
def {1} shared buffer   b-auxili  for auxili .
 
def {1} shared var ecr-mt-ttc     as dec                     no-undo .
def {1} shared var ecr-majbap     as char    format "x"      no-undo .
 
def {1} shared var ecr-mt-tva     as dec     extent 10       no-undo .
def {1} shared var ecr-cpttva     as char    extent 10       no-undo .
 
def {1} shared var ecr-mt-rst     as dec     extent 20       no-undo .
def {1} shared var ecr-cptrst     as char    extent 20       no-undo .
 
def {1} shared var ecr-codaux     like mvtcpt.codaux         no-undo .
def {1} shared var ecr-typaux     like mvtcpt.typaux         no-undo .
def {1} shared var ecr-colaux     like cptgen.nocpte         no-undo .
def {1} shared var ecr-datcpt     as date                    no-undo .
def {1} shared var ecr-datpie     as date                    no-undo .
 
def {1} shared var ecr-modreg     like mvtcpt.modreg         no-undo .
def {1} shared var ecr-jourst     like mvtcpt.journal        no-undo .
def {1} shared var ecr-pierst     like mvtcpt.piece          no-undo .
def {1} shared var ecr-typrst     like mvtcpt.typie          no-undo .
 