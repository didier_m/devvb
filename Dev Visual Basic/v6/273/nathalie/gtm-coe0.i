/*============================================================================*/
/*                           G T M - C O E 0 . I                              */
/* Gestion des Coefficients Tarifs Ventes par Magasins                        */
/*----------------------------------------------------------------------------*/
/* Variables et Frame                                                         */
/*============================================================================*/

def var i           as int                      no-undo .
def var magasin     as char format "xxx"        no-undo .
def var article     as char format "x(10)"      no-undo .
def var famille     as char format "xxx"        no-undo .
def var soufam      as char format "xxx"        no-undo .
def var date-applic as date format "99/99/9999" no-undo .
def var lib-mag     as char format "x(32)"      no-undo .
def var lib-art     as char format "x(32)"      no-undo .
def var lib-fam     as char format "x(30)"      no-undo .
def var lib-sfam    as char format "x(30)"      no-undo .

def {1} shared var libelle as char              extent 30 no-undo .
def {1} shared var coef    as int  format ">>9" extent 4  no-undo .

def {1} shared frame fr-cle .
def {1} shared frame fr-coef .

form libelle[ 1 ] format "x(20)" magasin lib-mag  skip( 1 )
     libelle[ 2 ] format "x(20)" article lib-art  skip( 1 )
     libelle[ 3 ] format "x(20)" famille lib-fam  skip( 1 )
     libelle[ 4 ] format "x(20)" soufam  lib-sfam skip( 1 )
     libelle[ 5 ] format "x(20)" date-applic
     with frame fr-cle row 3 centered overlay {v6frame.i} no-label .

form libelle[ 7 ] format "x(7)" libelle[ 8 ] format "x(7)"
     libelle[ 9 ] format "x(6)" libelle[ 10 ] format "x(13)"
     skip
     space( 2 ) coef[ 1 ] space( 5 ) coef[ 2 ] space( 4 ) coef[ 3 ]
     space( 7 ) coef[ 4 ]
     with frame fr-coef row 16 centered overlay {v6frame.i} no-label
     title libelle[ 6 ] .
