/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/ext-p5ta.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 17052 05-08-08!Evo!cch            !Activation des tarifs par magasins                                    !
!V52 16786 04-04-08!Evo!cch            !Ajout extraction DIVALTO + suppression trt V2 + OET ( fait par DGR )  !
!V52 15055 12-07-06!Evo!cch            !Acc�s ARTAPR avec soci�t� de regroupement suite mise en place ED      !
!V52 13668 16-11-05!Evo!cch            !Fin d�veloppement dossier Tarifs PROMO Plateformes                    !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - P 5 T A  . I                           */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Tarifs Vente PDP V5 EN EURO                                   */
/* DGR 100308 : extraction DIVALTO                                          */
/* DGR 180708 : mise en prod tarifs par magasin                             */
/*==========================================================================*/

                                                        /* Pos.  Long. */

zone = "TARBASE "                                       /* 001 - 008 */
       + "C"                                            /* 009 - 001 */
       + string(codsoc-trait[nsoc], "xx")               /* 010 - 002 */.



if topdiva = yes then 
       zone = zone 
            + fill(" ", 6)                                   /* 012 - 003 */.
else 
  zone = zone 
       + fill(" ", 3)                                   /* 012 - 003 */
       + string(magasi.codtab, "xxx")                   /* 015 - 003 */.

zone = zone
       + fill(" ", 2)                                   /* 018 - 002 */
       + fill(" ", 8)                                   /* 020 - 008 */
       + string({tarcli.i}.articl, "x(15)")             /* 028 - 015 */
       + string({tarcli.i}.date-debut, "99999999")      /* 043 - 008 */
       .

case {tarcli.i}.valeur :                                /* 051 - 009 */
    when 0.001 then wvaleur = 0.
    otherwise       wvaleur = {tarcli.i}.valeur.
end case.

zone = zone
       + string(wvaleur * 1000, "999999999")
       .

if {tarcli.i}.date-fin <> ?                             /* 060 - 008 */
   then zone = zone + string({tarcli.i}.date-fin, "99999999").
   else zone = zone + "31122050".

zone = zone
       + fill(" ", 8)                                   /* 068 - 008 */
       .

/* DGR 180708 TARIF MAG
case string(tabges.nombre[3], "9") :                    /* 076 - 001 */
    when "1" then zone = zone + "N".
    otherwise     zone = zone + "O".
end case.
*/

case type-tarif[ lookup( right-trim( {tarcli.i}.code-tarif ), l-tarif[ 3 ] ) ] :
    when 1 then zone = zone + "N".
    otherwise   zone = zone + "O".
end case.

zone = zone
       + fill(" ", 8)                                   /* 077 - 008 */
       + fill(" ", 9)                                   /* 085 - 009 */
        .

/* modif DGR 04/03/2002 : rajout tarif cartes des saisons : CTE */
/* modif DGR 06/03/2008 : rajout tarif ahcat : STE */

case {tarcli.i}.code-tarif :                                /* 094 - 008 */
    when "PRO" then zone = zone + "PROMO"
                                + fill(" ", 3)
                                .
    when "CTE" then zone = zone + "CARTE"
                                + fill(" ", 3)
                                .
    when "STE" then zone = zone + "ACHAT"
                                + fill(" ", 3)
                                .
    otherwise       zone = zone + fill(" ", 8).
end case.


zone = zone
       + fill(" ", 6)                                   /* 102 - 006 */
       + "A"                                            /* 108 - 001 */
       + " "                                            /* 109 - 001 */
       + fill("0", 7)                                   /* 110 - 007 */
       .

if {tarcli.i}.datcre <> ?                                   /* 117 - 008 */
   then zone = zone + string({tarcli.i}.datcre, "99999999").
   else zone = zone + fill( " ", 8 ).

if {tarcli.i}.datmaj <> ?                                   /* 125 - 008 */
   then zone = zone + string({tarcli.i}.datmaj, "99999999").
   else zone = zone + fill( " ", 8 ).

zone = zone
       + fill(" ", 12)                                  /* 133 - 012 */
       + "E"
       .

/* DGR 06/03/2008 : fournisseur si TARIF ACHAT */
if {tarcli.i}.motcle = "ACH" and {tarcli.i}.codaux <> "" then do:
  overlay (zone, 135,10,"CHARACTER") = string({tarcli.i}.codaux,"x(10)").
end.