/*==========================================================================*/
/*                           E X T - P 5 A B  . I                           */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Articles Bis PDP V5                             CC 14/11/2000 */
/*==========================================================================*/

                                                        /* Pos.  Long. */

zone = "ARTTIERS"                                       /* 001 - 008 */
       + "C"                                            /* 009 - 001 */
       + string(codsoc-trait[nsoc], "xx")               /* 010 - 002 */
       + fill(" ", 6)                                   /* 012 - 006 */
       + string(artapr.articl, "x(15)")                 /* 018 - 015 */
       + "E"                                            /* 033 - 001 */
       + fill(" ", 23)                                  /* 034 - 023 */
       + fill("0", 7)                                   /* 057 - 007 */
       + fill(" ", 2)                                   /* 064 - 002 */
       .

if artbis.type-surcondit = "O"                          /* 066 - 001 */
     then zone = zone + "O".
     else if artbis.surcondit <> 1
             then zone = zone + "O".
             else zone = zone + "N".

zone = zone
       + "000"                                          /* 067 - 003 */
       + " "                                            /* 070 - 001 */
       + fill(" ", 15)                                  /* 071 - 015 */
       .

if codsoc-trait[nsoc] = "08"                            /* 086 - 010 */
then do :
    if artapr.provenance = "0"
       then zone = zone + "999000    ".
       else if artapr.provenance = "3"
       then zone = zone + "999003    ".
       else if artapr.provenance = "4"
       then zone = zone + "999004    ".
end .
else do :
    if artapr.provenance = "1"
       then zone = zone + "999001    ".
       else if artapr.provenance = "3"
       then zone = zone + "999003    ".
       else if artapr.provenance = "4"
       then zone = zone + "999004    ".
end .

zone = zone + "O".                                      /* 096 - 001 */

if artapr.datcre <> ?                                   /* 097 - 008 */
   then zone = zone + string(artapr.datcre, "99999999").
   else zone = zone + fill( " ", 8 ) .

if artapr.datmaj <> ?                                   /* 105 - 008 */
   then zone = zone + string(artapr.datmaj, "99999999").
   else zone = zone + fill( " ", 8 ).

zone = zone
       + fill(" ", 10)                                  /* 113 - 010 */
       + "T"                                            /* 123 - 001 */
       + "H"                                            /* 124 - 001 */
       + fill(" ", 33)                                  /* 125 - 033 */
       + "N"                                            /* 158 - 001 */
       + " "                                            /* 159 - 001 */
       .

FIND TABLES where tables.codsoc = ""                    /* 160 - 010 */
            and   tables.etabli = ""
            and   tables.typtab = "QTE"
            and   tables.prefix = "TYP-QUANT"
            and   tables.codtab = artbis.ucde
            no-lock no-error.

if available tables
   then zone = zone + string(artbis.ucde, "x(10)").
   else zone = zone + "UN"
                    + fill ( " ", 8 )
                    .


   zone = zone
          + "000001000"                                 /* 170 - 009 */
          + string(artbis.surcondit * 1000, "99999999") /* 179 - 008 */
          + "00001000"                                  /* 187 - 008 */
          + "0000100"                                   /* 194 - 007 */
          .
