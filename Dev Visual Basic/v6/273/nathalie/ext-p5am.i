/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/ext-p5am.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 16680 22-02-08!Evo!cch            !Ajout exrraction RPD, no AMM et top EAJ ( fait par EUREA - DGR )      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - P 5 A M  . I                           */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Articles PDP V5 : REDEVANCE POLLUTION DIFFUSE  DGR 14/09/2007 */
/*==========================================================================*/
                                                        /* Pos.  Long. */
zone = "INFO_ART"                                       /* 001 - 008 */
       + "C"                                            /* 009 - 001 */
       + string(codsoc-trait[nsoc], "xx")               /* 010 - 002 */
       + fill(" ", 6)                                   /* 012 - 006 */
       + 
       string( string(trim(tabges.libel2[18]))          /* No AMM 018 - 013*/
       + ","
       + (if tabges.nombre[16] = 0 or                   /* US/KGL 032 - 004 */
             tabges.nombre[16] = ? 
             then "1.000"          
          else string(tabges.nombre[16] , "9.999") )
       + ","
       + (if tabges.libel2[16] = "1" then "O"           /* EAJ    036 - 001*/
          else "N"),"x(20)")
       + fill(" ", 8)                                  /* 038 - 008 */
       + fill(" ", 9)                                  /* 046 - 009 */
       + string(tabges.codtab, "x(15)")                /* 055 - 015 */
       + substr(string(tabges.datcre,"99999999"),5,4)
       + substr(string(tabges.datcre,"99999999"),3,2)
       + substr(string(tabges.datcre,"99999999"),1,2)
       + substr(string(tabges.datmaj,"99999999"),5,4)
       + substr(string(tabges.datmaj,"99999999"),3,2)
       + substr(string(tabges.datmaj,"99999999"),1,2)
       + fill(" ", 9)                                   /* 086 - 009 */
       + "REDPOLD".

