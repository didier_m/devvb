/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/e-trf50.i                                                                    !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 15055 12-07-06!Evo!cch            !Acc�s ARTAPR avec soci�t� de regroupement suite mise en place ED      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                               E - T R F 5 0 . I                             */
/*             Edition Tarif Articles  ( Zones Partageables )               */
/*==========================================================================*/

def {1} shared buffer b-tarcli for {tarcli.i} .

def {1} shared var libelle     as char                 extent 99    no-undo .
def {1} shared var valeur      as char format "x(35)"  extent 19    no-undo .
def {1} shared var codsoc-bl   as char format  "xx"  initial "  "   no-undo .
def {1} shared var etabli-bl   as char format "xxx"  initial "   "  no-undo .
def {1} shared var t-pf1       as char format "xxxx" initial "pf1," no-undo .
def {1} shared var prefix-1    as char format "xxx"                 no-undo .
def {1} shared var prefix-2    as char format "x(10)"               no-undo .
def {1} shared var code-tarif  as char format "xxx"                 no-undo .
def {1} shared var lib-tarif   as char format "x(30)"               no-undo .
def {1} shared var date-tarif  as date format "99/99/9999"          no-undo .
def {1} shared var maquette    as char format "x(12)"               no-undo .
def {1} shared var i           as int                               no-undo .
def {1} shared var j           as int                               no-undo .
def {1} shared var prix        as dec                               no-undo .
def {1} shared var coef-cde    like lignes.coef-cde                 no-undo .
def {1} shared var coef-fac    like lignes.coef-fac                 no-undo .
def {1} shared var num-famart  as char format "xxx"                 no-undo .
def {1} shared var lib-famart  as char format "x(30)"               no-undo .
def {1} shared var num-soufam  as char format "xxx"                 no-undo .
def {1} shared var lib-soufam  as char format "x(30)"               no-undo .

