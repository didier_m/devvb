/*==========================================================================*/
/*                          R F C - R I 1 0 . I                             */
/*       GESTION des RFC CLIENTS   : EDITION DES RISTOURNES PAR TAUX / CATEG*/
/*       Gestion des variables communes                                     */
/*==========================================================================*/



def {1} shared var zone          as char                            no-undo .
def {1} shared var auxili-soc    as char                            no-undo .
def {1} shared var mot-cle       as char                            no-undo .
def {1} shared var type-etat     as char                            no-undo .
def {1} shared var lib-cadre     as char                            no-undo .

def {1} shared var libelle       as char                 extent 99   no-undo .
def {1} shared var valeur        as char                 extent 20   no-undo .

def {1} shared var taux-rem     as dec  format ">>9.99"             no-undo .

/* zones frames */
def {1} shared var date-deb     as date format "99/99/9999"         no-undo .
def {1} shared var date-fin     as date format "99/99/9999"         no-undo .
def {1} shared var maquette     as char format "x(12)"              no-undo .
def {1} shared var lib-maq      as char format "x(30)"              no-undo .
def {1} shared var z-choix      as char                             no-undo .

def {1} shared var periph       as char format "x(12)"              no-undo .
def {1} shared var sav-periph   as char format "x(12)"              no-undo .



/*------------------------------------------------------------------------*/

def {1} shared frame fr-saisie .

form " " skip
     libelle[ 11 ] format "x(28)"  date-deb                 skip
     libelle[ 12 ] format "x(28)"  date-fin                 skip
     libelle[ 13 ] format "x(28)"  maquette     lib-maq     skip
     libelle[ 14 ] format "x(28)"  periph                   skip(1)
     with frame fr-saisie
     row 3  centered  overlay  no-label  with title libelle[ 10 ] .