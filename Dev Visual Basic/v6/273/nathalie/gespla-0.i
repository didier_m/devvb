/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/gespla-0.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V61 56599 28-10-15!Evo!jb             !Rejet des fichiers si erreurs                                         !
!V61 56016 14-09-15!Evo!jb             !L'envoi des ARC par mail ne fonctionne pas lorsqu'il y a 2 accus�s de !
!V61 55788 24-08-15!New!jb             !Mise � jour avec les modifications apport�es par EUREA                !
!V61 43862 10-01-14!Bug!pha            !problemes variables partag�es sai-000.i                               !
!V61 37251 05-04-13!Evo!cch            !Test et Mise au point envoi ARC par mail aux magasins                 !
!V61 31728 05-07-12!Evo!cch            !Suite Fiche V61 31158 - Ajout Rupture Spool par Magasin pour envoi par!
!V61 31158 13-06-12!Evo!cch            !Ajout rupture par magasin                                             !
!V61 29713 23-03-12!Bug!cch            !Correction pble 'La saisie 3 est en dehors de la liste' - Suite Fiche !
!V61 29110 16-02-12!Evo!cch            !Ajout solde lignes non livr�es + ent�te cde - Suite Fiche V61 29087   !
!V61 24497 18-05-11!Evo!cch            !Modif. r�cup. prix et fournisseur si art. <> GVSE - Mail CCO 13/05/201!
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/* GESPLA-0.I - GESTION DES ECHANGES DE DONNEES NATHALIE / PLATEFORMES        */
/*----------------------------------------------------------------------------*/
/* Variables Communes                               08/06/2004 - SOPRES - 273 */
/******************************************************************************/

def var i              as int            no-undo .
def var fic-rac        as char           no-undo .
def var fic-pla        as char           no-undo .
def var fic-pdp        as char           no-undo .
def var fic-eli        as char           no-undo .
def var fic-ent        as char           no-undo .
def var fic-trt        as char           no-undo .
def var fic-ext        as char           no-undo .
def var fic-log        as char           no-undo .
def var fic-rej        as char           no-undo .
def var fic-ren        as char           no-undo .
def var fic-tra        as char           no-undo .
def var fic-blo        as char           no-undo .
def var fic-blo-2      as char           no-undo .
def var zone           as char           no-undo .
def var enreg          as char           no-undo .
def var lib-err        as char           no-undo .
def var x-message      as char           no-undo .
def var y-message      as char           no-undo .
def var no-enreg       as int            no-undo .
def var type-ech       as char           no-undo .
def var ean13-emet     as char           no-undo .
def var ean13-destin   as char           no-undo .
def var sav-no-interch as dec            no-undo .
def var no-interch     as dec            no-undo .
def var sav-date-ech   as date           no-undo .
def var sav-heure-ech  as char           no-undo .
def var ok-maj-interch as log            no-undo .
def var ok-ouv-rej     as log            no-undo .
def var ok-edit        as log            no-undo .
def var ok-bon         as log            no-undo .
def var ok-eli         as log            no-undo .
def var nb-lu          as int            no-undo .
def var nb-trt         as int            no-undo .
def var nb-trt-sav     as int            no-undo .
def var nb-bon         as int            no-undo .
def var nb-err         as int            no-undo .
def var nb-enreg       as int            no-undo .
def var articl-soc     as char           no-undo .
def var auxapr-soc     as char           no-undo .
def var magasi-soc     as char           no-undo .
def var codsoc-sav     as char           no-undo .
def var val-edit       as char extent 20 no-undo .
def var pos-rowid-cde  as int            no-undo .
def var l-solde-cde    as char extent 2  no-undo .

def {1} shared var libelle       as char                extent 180 no-undo .
def {1} shared var fic-rej-trt   as char format "x(50)"            no-undo .
def {1} shared var fic-spool     as char                extent 2   no-undo .
def {1} shared var trt-rejet     as log                            no-undo .
def {1} shared var trt-test      as log                            no-undo .
def {1} shared var ok-rej-bon    as log                            no-undo .
def {1} shared var l-fic-pla     as char                           no-undo .
def {1} shared var l-fic-dep     as char                           no-undo .
def {1} shared var l-soc         as char                           no-undo .
def {1} shared var l-typiece     as char                           no-undo .
def {1} shared var l-typech      as char                           no-undo .
def {1} shared var l-nature      as char                           no-undo .
def {1} shared var l-nat-contr   as char                           no-undo .
def {1} shared var l-nat-dep     as char                           no-undo .
def {1} shared var l-nat-exclus  as char                           no-undo .
def {1} shared var l-arc-dep     as char                           no-undo .
def {1} shared var l-typrej-ent  as char                           no-undo .
def {1} shared var l-typrej-lig  as char                           no-undo .
def {1} shared var l-extract     as char                           no-undo .
def {1} shared var l-bal         as char                           no-undo .
def {1} shared var unite-box     as char                           no-undo .
def {1} shared var type-trt      as char                           no-undo .
def {1} shared var date-j        as char                           no-undo .
def {1} shared var rep-travail   as char                           no-undo .
def {1} shared var rep-arriv     as char                           no-undo .
def {1} shared var rep-depart    as char                           no-undo .
def {1} shared var rep-traite    as char                           no-undo .
def {1} shared var rep-traite-d  as char                           no-undo .
def {1} shared var rep-rejet     as char                           no-undo .
def {1} shared var rep-php       as char                           no-undo .
def {1} shared var operat-maj    as char                           no-undo .
def {1} shared var operat-dep    as char                           no-undo .
def {1} shared var l-nomimp      as char                           no-undo .
def {1} shared var typaux-lis    as char                           no-undo .
def {1} shared var codaux-lis    as char                           no-undo .
def {1} shared var typaux-dis    as char                           no-undo .
def {1} shared var codaux-dis    as char                           no-undo .
def {1} shared var typaux-flo    as char                           no-undo .
def {1} shared var codaux-flo    as char                           no-undo .
def {1} shared var typaux-sod    as char                           no-undo .
def {1} shared var codaux-sod    as char                           no-undo .
def {1} shared var l-art-fr-po   as char                           no-undo .
def {1} shared var frais-pu      as dec                            no-undo .
def {1} shared var l-corresp     as char                           no-undo .
def {1} shared var date-ech      as char                           no-undo .
def {1} shared var heure-ech     as char                           no-undo .
def {1} shared var motcle-ach    as char                           no-undo .
def {1} shared var motcle-vte    as char                           no-undo .
def {1} shared var lgrel         as int                            no-undo .
def {1} shared var nb-enr-edit   as int                            no-undo .
def {1} shared var no-page       as int                            no-undo .
def {1} shared var codsoc-ed     as char                           no-undo .
def {1} shared var l-mag-mul-ed  as char                           no-undo .
def {1} shared var mag-mul-att   as char                           no-undo .
def {1} shared var code-mag      as char                           no-undo .
def {1} shared var lib-mag       as char                           no-undo .
def {1} shared var lib-soc       as char                           no-undo .
def {1} shared var l-famart      as char                           no-undo .
def {1} shared var l-sfam-exc    as char                           no-undo .
def {1} shared var rupt-sp-arc   as int                            no-undo .
def {1} shared var cmd-turbo     as char                           no-undo .
def {1} shared var l-param-turbo as char                           no-undo .

def temp-table bon
    field chrono     as int
    field type-enr   as char
    field type-rejet as char
    field enreg      as char
    index i-bon-1 is primary unique chrono
    index i-bon-2 type-enr type-rejet chrono .

def temp-table tra-lignes
    field valeur as char .

def {1} shared temp-table edition no-undo
    field codsoc      as char
    field magasin     as char
    field libmag      as char
    field ref-cdi     as char
    field chr-cdi     as int
    field date-cdi    as char
    field ref-cde     as char
    field articl      as char
    field articl-fou  as char
    field libart      as char
    field ucde        as char
    field chr-cmp-box as int
    field enreg       as char
    index i-edition-1 codsoc magasin ref-cdi chr-cdi chr-cmp-box articl .

def {1} shared temp-table fic-edit
    field nom-fic  as  char
    field magasin  as  char
    field lib-mag  as  char 
    FIELD blocage  AS  LOG    
.





