/*==========================================================================*/
/*                          B A R - C A L 1 . P                             */
/*       GESTION des BAREMES       : CALCUL DES DROITS sur RISTOURNES       */
/*                                                                          */
/*==========================================================================*/

{ connect.i  }

{ bar-cal0.i }

/*-------------------------------------------------*/
/* Definitions des Variables                       */
/*-------------------------------------------------*/

def buffer b-barstat   for barstat                      .
def buffer b-tabges    for tabges                       .
def var i              as  int                          no-undo .
def var regroup        as  char     format "x(4)"       no-undo .
def var b-regroup      as  char     format "x(4)"       no-undo .
def var b-cumul        as  char     format "x"          no-undo .
def var qte-ach        as  dec                          no-undo .
def var qte-rea        as  dec                          no-undo .
def var plafond        as  dec                          no-undo .
def var top-realise    as  char     format "x"          no-undo .
def var nb-lus         as int                           no-undo .

/*==========================================================================*/
/*                           Debut du traitement                            */
/*==========================================================================*/

/* Vidage du fichicer de calcul BARRIST */
/*--------------------------------------*/
FOR EACH BARRIST where barrist.codsoc = codsoc-soc
                       use-index primaire exclusive-lock :
    delete barrist .
END .


      /*--------------------------*/
     /* FOR EACH / BARSTAT       */
    /*--------------------------*/

FOR EACH BARSTAT where barstat.codsoc    = codsoc-soc
                   and barstat.exercice  = annee-exe
                   use-index primaire  no-lock
                   Break by (barstat.magasin  +
                             barstat.typaux    +
                             barstat.codaux    +
                             barstat.bareme ) :


    if nb-lus modulo 500 = 0
    then do :
        hide message . pause 0 .
        message fill( " " , 10 )
                "Preparation du fichier Tri ...   "
                 string( nb-lus , ">>>>9" )
                 "  "  barstat.magasin
                 "  "  barstat.codaux
                 "  "  barstat.bareme .
    end .
    nb-lus = nb-lus + 1 .

    FIND TABGES where  tabges.codsoc   = " "
                  and  tabges.etabli   = " "
                  and  tabges.typtab   = "TAR"
                  and  tabges.prefix   = "BAREME-CLI"
                  and  tabges.codtab   = barstat.bareme
                  no-lock no-error .

    if not available TABGES
    then do :
        message barstat.bareme " :  Bareme inexistant dans la table" .
        bell. bell . pause .
        hide all .
        next .
    end .

    /* test si bareme a payer  si non pas de traitement */
    if tabges.nombre[2] = 0 then next .

    Repeat while first-of ( barstat.magasin + barstat.typaux +
                            barstat.codaux  + barstat.bareme ) :

        assign qte-ach = 0
               qte-rea = 0
               .

        assign regroup = substring( tabges.libel2[ 2 ], 1, 4)
               plafond = tabges.nombre[ 1 ] .

        FOR EACH b-barstat  where b-barstat.codsoc   = codsoc-soc
                              and b-barstat.typaux   = barstat.typaux
                              and b-barstat.codaux   = barstat.codaux
                              and b-barstat.exercice = barstat.exercice
                              no-lock  :

            FIND b-tabges where b-tabges.codsoc   = " "
                           and  b-tabges.etabli   = " "
                           and  b-tabges.typtab   = "TAR"
                           and  b-tabges.prefix   = "BAREME-CLI"
                           and  b-tabges.codtab   = b-barstat.bareme
                           no-lock no-error .

            if not available TABGES
            then do :
                message b-barstat.bareme " :  Bareme inexistant dans la table" .
                bell. bell . pause .
                hide all .
                next .
            end .

            assign b-regroup = substring( b-tabges.libel2[ 2 ], 1, 4)
                   b-cumul   = substring( b-tabges.libel2[ 2 ], 5, 1) .

            if b-cumul <> "1" then next .

            if b-regroup = regroup
            then do i = 1 to 12 :
                qte-rea = qte-rea + b-barstat.qte[ i ] .
                if b-barstat.bareme = barstat.bareme
                then qte-ach = qte-ach + b-barstat.qte[ i ] .
            end .

        END . /* For Each de B-barstat */

        leave .
    END . /* IF FIRST-OF */


    if last-of ( barstat.magasin + barstat.typaux +
                 barstat.codaux  + barstat.bareme )
    then do :

        if qte-rea >= plafond
        then top-realise = "1" .
        else top-realise = " " .

        FIND BARRIST where barrist.codsoc  = barstat.codsoc
                       and barrist.magasin = barstat.magasin
                       and barrist.typaux  = barstat.typaux
                       and barrist.codaux  = barstat.codaux
                       and barrist.bareme  = barstat.bareme
                       use-index primaire exclusive-lock no-error .
        if not available barrist
        then do :
            Create BARRIST .
            assign barrist.codsoc  = barstat.codsoc
                   barrist.magasin = barstat.magasin
                   barrist.typaux  = barstat.typaux
                   barrist.codaux  = barstat.codaux
                   barrist.bareme  = barstat.bareme
                   barrist.qte-ach = 0
                   barrist.qte-rea = 0
                   barrist.top-realise = " " .
          end .

          { majmoucb.i "barrist" }

          assign barrist.qte-ach     = barrist.qte-ach + qte-ach
                 barrist.qte-rea     = barrist.qte-rea + qte-rea
                 barrist.top-realise = top-realise .

    end . /* last-of */

END .  /* FOR EACH BARSTAT  */