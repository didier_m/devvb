/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/ex-liglu.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 15441 14-11-06!Bug!cch            !Correction abscence traitement cartes fid�lit�s si commentaire d'ent�t!
!V52 14587 31-03-06!Bug!cch            !Correction probl�me affectation de cartes fid�lit�s sur des commandes !
!V52 13820 12-12-05!Evo!cch            !Ajout int�gration des adresses de livraisons sur BLV                  !
!V52 13815 09-12-05!Evo!cch            !Ajout Stockage du Mode Livraison dans les commandes de type 'COM'     !
!V52 12570 04-05-05!Evo!cch            !Int�gration des R�ceptions directes LISADIS                           !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/* R.A.Z */
assign
        libels     = ""
        mtt-pxbas  = 0
        transp     = ""
        modliv     = ""
        typai      = ""
        cregl      = ""
        tottc      = ""
        refrn      = ""
        escht      = ""
        esctv      = ""
        editf      = ""
        nuart      = ""
        cdeca      = ""
        ddliv      = ""
        dfliv      = ""
        nucde      = ""
        qtcde      = ""
        cdech      = ""
        mtht       = ""
        mtttc      = ""
        avoir      = ""
        cdtva      = ""
        dtech      = ""
        refav      = ""
        qtcnd      = ""
        qtexp      = ""
        qtfor      = ""
        qtstk      = ""
        pxbas      = ""
        pxach      = ""
        .

        assign
        cdapp   = ""
        cdatc   = ""
        borde   = ""
        caiss   = ""
        cdban   = ""
        cdmaf   = ""
        cdmof   = ""
        cdope   = ""
        cdrba   = ""
        dtbof   = ""
        dtecf   = ""
        doban   = ""
        hecre   = ""
        libop   = ""
        ligop   = ""
        cpaie   = ""
        cregf   = ""
        montt   = ""
        nomti   = ""
        nuche   = ""
        nucpt   = ""
        nufac   = ""
        numan   = ""
        nupal   = ""
        nuref   = ""
        opert   = ""
        agsnf   = ""
        soapp   = ""
        scmvt   = ""
        tiroi   = ""
        topsg   = ""
        topac   = ""
        topre   = ""
        topsf   = ""
        toptr   = ""
        ntrans  = ""
        tyech   = ""
        tygen   = ""
        ctpai   = ""
        cmreg   = ""
        nuclo   = ""
        sscai   = ""
        ref-mag = ""
        agsnu   = ""
        .

        assign
        nbeb1 = "" nbel1 = "" cahb1 = "" cato1 = ""
        nbeb2 = "" nbel2 = "" cahb2 = "" cato2 = ""
        nbeb3 = "" nbel3 = "" cahb3 = "" cato3 = ""
        .

/* Cas de Craponne  ( CCO le 28/01/2002 ) */
if substring( liglue , 53 , 2 ) = "37"
   then assign substring ( liglue , 53 , 2 ) = "05"
               substring ( liglue , 23 , 6 ) = "911   "
               substring ( liglue , 55 , 3 ) = "911"
               .

assign
        cdsoe   = substring ( liglue ,  1 ,  2  )
        cdmac   = substring ( liglue ,  4 ,  3  )
        branc   = substring ( liglue ,  7 ,  2  )
        fichi   = substring ( liglue ,  9 ,  8  )
        dtext   = substring ( liglue , 17 ,  6  )
        depoe   = substring ( liglue , 23 ,  6  )
        cdmou   = substring ( liglue , 29 ,  3  )
        nudoe   = substring ( liglue , 32 ,  7  )
        nupar   = substring ( liglue , 41 ,  5  )
        topsu   = substring ( liglue , 46 ,  1  )
        cdmop   = substring ( liglue , 47 ,  3  )
        recyc   = substring ( liglue , 50 ,  1  )

        cdsoc   = substring ( liglue , 51 ,  2  )
        filia   = substring ( liglue , 53 ,  2  )
        codsoc  = substring ( liglue , 53 ,  2  )
        depot   = substring ( liglue , 55 ,  3  )
        nubon   = substring ( liglue , 70 ,  5  )
        nulig   = substring ( liglue , 75 ,  3  )
        nutie   = substring ( liglue , 78 ,  6  )
        dtbon   = substring ( liglue , 93 ,  6  )
        ref-mag = cdmop + substr ( liglue , 68 , 7  ) + depot
        .

nupar = nupar + "00" .

if cdmop <> "BLV" then  nubon = nubon + "00" .
                  else  nubon = nubon + substring ( liglue , 68 ,  2  ) .

if not fichi begins "APP" and fichi <> "COMMENT" then car-fi-no = "" .

if fichi = "CUMCAF" then
        assign
        nbeb1  = substring ( liglue ,100 ,  7  )
        nbel1  = substring ( liglue ,107 ,  7  )
        cahb1  = substring ( liglue ,114 , 11  )
        cato1  = substring ( liglue ,125 , 11  )

        nbeb2  = substring ( liglue ,136 ,  7  )
        nbel2  = substring ( liglue ,143 ,  7  )
        cahb2  = substring ( liglue ,150 , 11  )
        cato2  = substring ( liglue ,161 , 11  )

        nbeb3  = substring ( liglue ,172 ,  7  )
        nbel3  = substring ( liglue ,179 ,  7  )
        cahb3  = substring ( liglue ,186 , 11  )
        cato3  = substring ( liglue ,197 , 11  )
        .

if fichi = "COMMANDE" then
        assign
        nuart  = substring ( liglue ,116 , 6   )
        cdeca  = substring ( liglue ,141 , 1   )
        dtech  = substring ( liglue ,165 , 6   )
        ddliv  = substring ( liglue ,171 , 6   )
        dfliv  = substring ( liglue ,177 , 6   )
        modliv = trim( substring ( liglue , 212 , 2 ) )
        nucde  = substring ( liglue ,214 , 7   )
        refrn  = substring ( liglue ,241 , 10  )
        qtcde  = substring ( liglue ,271 , 10  )
        pxbas  = substring ( liglue ,308 , 108 )
        cdech  = substring ( liglue ,499 , 1   )
        mtht   = substring ( liglue ,500 , 10  )
        mtttc  = substring ( liglue ,510 , 10  )
        agsnu  = substring ( liglue ,544 , 15  )
        devise = substring ( liglue ,559 , 1   )

        mtt-escht = 0
        mtt-esctva = 0
        .

if fichi = "MVTFINAN" then
        assign
        cdapp  = substring ( liglue , 100 ,  2 )
        cdatc  = substring ( liglue , 102 ,  6 )
        borde  = substring ( liglue , 108 ,  7 )
        caiss  = substring ( liglue , 115 ,  2 )
        avoir  = substring ( liglue , 117 ,  1 )
        cdban  = substring ( liglue , 118 ,  8 )
        cdmaf  = substring ( liglue , 126 ,  4 )
        cdmof  = substring ( liglue , 130 ,  3 )
        cdope  = substring ( liglue , 133 , 12 )
        cdrba  = substring ( liglue , 145 ,  1 )
        dtbof  = substring ( liglue , 146 ,  6 )
        dtecf  = substring ( liglue , 152 ,  6 )
        doban  = substring ( liglue , 158 , 30 )
        hecre  = substring ( liglue , 188 ,  8 )
        libop  = substring ( liglue , 196 , 30 )
        ligop  = substring ( liglue , 226 ,  1 )
        cpaie  = substring ( liglue , 227 ,  8 )
        cregf  = substring ( liglue , 235 ,  8 )
        montt  = substring ( liglue , 243 , 12 )
        nomti  = substring ( liglue , 255 , 30 )
        nuche  = substring ( liglue , 285 ,  7 )
        nucpt  = substring ( liglue , 292 ,  7 )
        nufac  = substring ( liglue , 299 , 15 )
        numan  = substring ( liglue , 314 ,  7 )
        nupal  = substring ( liglue , 321 ,  7 )
        nuref  = substring ( liglue , 328 ,  7 )
        opert  = substring ( liglue , 335 , 12 )
        agsnf  = substring ( liglue , 347 ,  7 )
        soapp  = substring ( liglue , 354 ,  2 )
        scmvt  = substring ( liglue , 356 ,  1 )
        tiroi  = substring ( liglue , 357 ,  3 )
        topsg  = substring ( liglue , 360 ,  5 )
        topac  = substring ( liglue , 365 ,  1 )
        topre  = substring ( liglue , 366 ,  1 )
        topsf  = substring ( liglue , 367 ,  1 )
        toptr  = substring ( liglue , 368 ,  1 )
        ntrans = substring ( liglue , 369 ,  4 )
        tyech  = substring ( liglue , 373 ,  1 )
        tygen  = substring ( liglue , 374 ,  1 )
        ctpai  = substring ( liglue , 375 ,  2 )
        cmreg  = substring ( liglue , 377 ,  2 )
        nuclo  = substring ( liglue , 379 ,  8 )
        sscai  = substring ( liglue , 387 ,  1 )
        agsnu  = substring ( liglue , 388 , 15 )
        devise = substring ( liglue , 418 , 1  )
        .

if fichi = "APP TETE"
then do :
    assign
        chauf         = trim( substring ( liglue , 100 ,  10 ) )
        transp        = trim( substring ( liglue , 114 ,  10 ) )
        heucre-mag    = substring ( liglue ,131 ,  8  )
        modliv        = trim( substring ( liglue , 139 , 2 ) )
        typai         = substring ( liglue ,141 ,  7  )
        cregl         = substring ( liglue ,148 ,  8  )
        tottc         = substring ( liglue ,176 , 10  )
        refrn         = substring ( liglue ,207 , 15  )
        escht         = substring ( liglue ,375 , 70  )
        esctv         = substring ( liglue ,445 , 70  )
        tyaju         = substring ( liglue ,78  ,  2  )
        editf         = substring ( liglue ,517 ,  1  )
        agsnu         = substring ( liglue ,518 , 15  )
        devise        = substring ( liglue ,533 ,  1  )
        car-fi-pts-an = substring ( liglue ,114 ,  10 )
        car-fi-no     = substring ( liglue ,534 , 13  )
        car-fi-pts    = substring ( liglue ,547 ,  6  )
        car-fi-cre    = substring ( liglue ,553 ,  1  )
        car-fi-operat = substring ( liglue ,554 ,  6  )

        mtt-escht  = 0
        mtt-esctva = 0
        .

   do a = 1 to 10 :
       mtt-escht[a]  = dec ( substring ( escht , ( a * 10 ) - 9 , 10 ) ) / 100 .
       mtt-esctva[a] = dec ( substring ( esctv , ( a * 10 ) - 9 , 10 ) ) / 100 .
   end .

end .

if fichi = "APP LIG" then
        assign
        nuatc     = substring ( liglue ,106 ,   6 )
        nuart     = substring ( liglue ,120 ,  15 )
        avoir     = substring ( liglue ,135 ,   1 )
        cdech     = substring ( liglue ,136 ,   1 )
        cdeca     = substring ( liglue ,137 ,   1 )
        cdtva     = substring ( liglue ,159 ,   1 )
        dtech     = substring ( liglue ,166 ,   6 )
        mtht      = substring ( liglue ,204 ,  11 )
        mtttc     = substring ( liglue ,215 ,  11 )
        nucde     = substring ( liglue ,240 ,   7 )
        refav     = substring ( liglue ,267 ,   7 )
        nulig-cde = substring ( liglue ,274 ,  3  )
        pxach     = substring ( liglue ,278 ,   9 )
        qtcnd     = substring ( liglue ,288 ,  10 )
        qtexp     = substring ( liglue ,298 ,  10 )
        qtfor     = substring ( liglue ,308 ,   1 )
        qtstk     = substring ( liglue ,309 ,  10 )
        pxbas     = substring ( liglue ,321 , 108 )
        solde     = substring ( liglue ,506 , 1   )
        .

do a = 1 to 12 :
   mtt-pxbas[a] = dec ( substring ( pxbas , ( a * 9 ) - 8 , 9 ) ) / 1000 .
end .

if fichi = "COMMENT "
then do :
    assign
        edit   = substring ( liglue ,103 ,   1 )
        typcmt = substring ( liglue ,104 ,   8 )
        libels = substring ( liglue ,112 , 260 )
    .

    do a = 1 to 4 :
        libel-lig[ a ] = right-trim( substring ( libels , ( a * 65 ) - 64 , 65 ) ) .
    end .

    if cdmop = "BLV" and typcmt = "ADRS" then libel-lig[ 5 ] = trim( substring ( liglue , 372 , 5 ) ) + " " +
       trim( substring ( liglue , 377 , 29 ) ) .

    if cdmop = "MES"
    then do :
        x-site   = substring ( libel-lig[ 1 ] , 1, 3 ) .
        x-destin = substring ( libel-lig[ 1 ] , 4, 3 ) .
        if     x-site <> "ELF"
           and x-site <> "USI"
           and x-site <> "DMG"
           and x-site <> "ARV"
           and x-site <> "COT"
           and x-site <> "BON"
           and x-site <> "CIV"
           and x-site <> "LOG"
        then case codsoc :
            when "08" then x-site = "CIV" .
            otherwise      x-site = "SIE" .
        end case .
        x-destin = x-destin + nubon .
    end  .
end .

if fichi = "MESSAGE" /* Messages Magasins PDP V5 */
then do :

    assign nulig  = substr ( liglue, 61, 4 )
           libels = substr ( liglue, 65, 73 )
           .

    if nulig = "0000"
       then assign x-site   = substr ( liglue, 26, 3 )
                   x-nomes  = substr ( liglue, 32, 14 )
                   x-destin = substr ( liglue, 138, 3 )
                   .

end .