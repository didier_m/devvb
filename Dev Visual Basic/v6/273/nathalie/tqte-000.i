/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/tqte-000.i                                                                     !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!        2019-09-06!Bug!RAMIN SHARIATIA![GC][273] BUG Lors compile livrable                                   !
! 290554 2019-09-05!Evo!Jean Baptiste C![GC]Intégration version 273                                           !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End                                                                                                          
**************************************************************************************************************/
/*
 
        tqte-000.i
*/
 
/*----------------------------------*/
/* variable pour acceder a messprog */
/*----------------------------------*/
def var CLE-MESSAGE             as char format "x(25)" initial "TQTE-000-01".
 
/*-----------------------------*/
/* variable pour la cle tables */
/*-----------------------------*/
 
def var base-codsoc     like tables.codsoc initial "".
def var base-etabli     like tables.etabli initial "".
def var base-typtab     like tables.typtab initial "QTE".
def var base-prefix     like tables.prefix initial "TYP-QUANT".
 
/*----------------------------------------*/
/* variable correspondant a la cle tables */
/*----------------------------------------*/
def var code-table      as char format "x(3)"  .
 
/*--------------------------------------------------------------------------*/
def var libelle-langues         like tables.libel1     .
def var libelle-code-table      like tables.libel1     .
/*--------------------------------------------------------------------------*/
 
/*----------------------*/
/* variables de boucles */
/*----------------------*/
def var a               as integer.
def var b               as integer.
/*----------------------*/
def var creation        as logical.
 
def var tit-ges-table           as char format "x(20)".
def var tit-code-table          as char format "x(20)".
def var tit-suppression         as char format "x(20)".
def var tit-libelle             as char format "x(20)".
def var tit-libelle-langues     as char format "x(20)".
def var tit-libelle-code-table  as char format "x(20)".
 
/* variable pour validation */
def var msg-valid               as char format "x(30)".
def var tit-valid               as char format "x(30)".
def var valid                   as logical format "1/0" initial "yes".
 
 
/*----------*/
/* messages */
/*----------*/
def var msg-saisie-code         as char format "x(35)".
def var msg-libelle-code        as char format "x(35)".
def var msg-suppression         as char format "x(35)".
 
def var msp-suppression         as char format "x(35)".
def var msp-inexistant          as char format "x(35)".
def var msp-bloque              as char format "x(35)".
def var msp-recherche           as char format "x(35)".
def var msp-obligatoire         as char format "x(35)".
 
/*-------------------------*/
/* variables pour le titre */
/*-------------------------*/
def var ddate           as date initial today.
def var adate           as char format "x(10)".
def var valid-sup       as logical format "1/0" .
 
def var tit-deci        as char format "X(20)".
def var tit-prix        as char format "X(20)".
def var tit-iso         as char format "X(20)".
def var tit-ach         as char format "X(20)".
def var nb-deci         as int  format "9".
def var px-unit         as dec  decimals 3 format "zzzzzz9.999".
def var norme-iso       as char format "X(15)".
def var top-achat       as int  format "9".
 
/*----------------------------------------------------------------------------*/
 
/*------------------------------------*/
/* pour frame de fond (cadre) + titre */
/*------------------------------------*/
def var vide                    as char format "x(78)".
form    vide no-label
        with frame ecr-titre with title tit-ges-table row 1 19 down  overlay {v6frame.i}.
 
/*-------------------------------*/
/* ecran de Saisie du code table */
/*-------------------------------*/
form    tit-code-table
        code-table
        with frame ecr-code-table row 3 width 40 no-label centered  overlay {v6frame.i}.
 
form    tit-deci nb-deci   skip 
        tit-prix px-unit   skip
        tit-iso  norme-iso skip
        tit-ach  top-achat
	with frame ecr-donnees    row 5    no-label centered  overlay {v6frame.i}.
 
 
/*--------------------------------------------*/
/* ecran d'Affichage des libelles des langues */
/*--------------------------------------------*/
form    libelle-langues at 2 no-label
        with frame ecr-libelle-langues no-label color message
        with title tit-libelle-langues
        row 9 col 6  overlay {v6frame.i}.
 
/*--------------------------------------------------------------------*/
/* ecran de composition des libelles des code table des 10 langues    */
/*--------------------------------------------------------------------*/
form    libelle-code-table      at 1    no-label
        with frame ecr-libelle-code-table no-labels
        with title tit-libelle-code-table
        row 9 col 39  overlay {v6frame.i} .
 
/*--------------------------------------------------*/
/* ecran de composition de la suppression de la Cle */
/*--------------------------------------------------*/
form    tit-suppression no-label
        valid-sup       no-label
        with frame ecr-suppression row 12 centered no-labels
        color message  overlay {v6frame.i}.
 
 
/* pour validation */
form    tit-valid       no-label
        valid           no-label
        with frame ecr-valid row 18 centered no-labels
        color message  overlay {v6frame.i}.
 
 
 