/*==========================================================================*/
/*                          S T - A S S 1 0 . I                             */
/*          Edition Tableau d ' Analyse des Assortiments                    */
/*          Definitions des variables et des frames                         */
/*==========================================================================*/
 
def {1} shared var libelle      as char                 extent 99   no-undo .
def {1} shared var valeur       as char                 extent 20   no-undo .
def {1} shared var mask         as char                 extent 20   no-undo .
def {1} shared var nb-lus       as int   format ">,>>>,>>9"         no-undo .
def {1} shared var no-article   as char                             no-undo .
def {1} shared var lib-article  as char                             no-undo .
def {1} shared var no-ssfam     as char                             no-undo .
def {1} shared var lib-ssfam    as char                             no-undo .
def {1} shared var no-catego    as char                             no-undo .
def {1} shared var lib-catego   as char                             no-undo .
def {1} shared var no-fam       as char                             no-undo .
def {1} shared var lib-fam      as char                             no-undo .
def {1} shared var lib-periode  as char                             no-undo .
def {1} shared var magasins     as char format "x(40)"              no-undo .
def {1} shared var familles     as char format "x(48)"              no-undo .
def {1} shared var tri-ca       as log  format "O/N"                no-undo .
def {1} shared var list-mag     as char                             no-undo .
def {1} shared var maquette     as char format "x(12)"              no-undo .
def {1} shared var periph       as char format "x(12)"              no-undo .
def {1} shared var lib-maq      as char format "x(32)"              no-undo .
def {1} shared var nom-ope      as char format "x(12)"              no-undo .
def {1} shared var fam-exclus   as char  format  "xx"               no-undo .
def {1} shared var mag-exclus   as char  format  "xx"               no-undo .
def {1} shared var mois-debcp   as char format "x(6)"               no-undo .
def {1} shared var mois-fincp   as char format "x(6)"               no-undo .
def {1} shared var mois-debex   as char format "x(6)"               no-undo .
def {1} shared var mois-deb     as int                              no-undo .
def {1} shared var anee-deb     as int                              no-undo .
def {1} shared var mois-fin     as int                              no-undo .
def {1} shared var anee-fin     as int                              no-undo .
def {1} shared var mois-dex     as int                              no-undo .
def {1} shared var anee-dex     as int                              no-undo .
def {1} shared var aa-deb       as char                             no-undo .
def {1} shared var aa-fin       as char                             no-undo .
def {1} shared var mm-deb       as int                              no-undo .
def {1} shared var mm-fin       as int                              no-undo .
def {1} shared var date-arr     as date format "99/99/9999"         no-undo .
def {1} shared var z-choix      as char                             no-undo .
 
/*------------------------------------------------------------------------*/
def {1} shared frame fr-saisie .
 
form libelle[ 11 ] format "x(24)"  mois-debcp               skip
     libelle[ 12 ] format "x(24)"  mois-fincp               skip
     libelle[ 13 ] format "x(24)"  mois-debex               skip
     libelle[ 14 ] format "x(24)"  magasins                 skip
     libelle[ 15 ] format "x(24)"  familles                 skip
     libelle[ 16 ] format "x(24)"  tri-ca                   skip
     libelle[ 17 ] format "x(24)"  maquette     lib-maq     skip
     libelle[ 18 ] format "x(24)"  periph
     with frame fr-saisie
     row 3  centered  overlay  no-label  with title libelle[ 10 ] .