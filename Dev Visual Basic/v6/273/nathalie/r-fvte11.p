/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/src/r-fvte11.p                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 302080 2019-11-18!Bug!p-o teyssier   !Synchronisation code deal et client + corrections                     !
! 292981 2019-10-24!Evo!p-o teyssier   !Reedition factures archivees                                          !
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V52 15055 12-07-06!Evo!cch            !Acc�s ARTAPR avec soci�t� de regroupement suite mise en place ED      !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                          R - F V T E 1 1 . P                             */
/*  REEDITION de FACTURE A PARTIR de BASTAT                                 */
/*==========================================================================*/
{ connect.i        }
{ r-fvte10.i "new" }
{ det-expe.i "new" }
{ saisie.i   "new" }
{ bat-ini.i  "new" }
{ select.i   "new" }
{ maquette.i "new" }
{ aide.i     "new" }

DEFINE INPUT PARAMETER fichier  AS CHARACTER FORMAT "x(12)".

DEFINE NEW SHARED STREAM maquette.
DEFINE NEW SHARED STREAM kan-tri.

DEFINE VARIABLE nb-lus          AS INTEGER          NO-UNDO.
DEFINE VARIABLE x-x             AS CHARACTER        NO-UNDO.
DEFINE VARIABLE aa              AS INTEGER          NO-UNDO.
DEFINE VARIABLE liste-typcom    AS CHARACTER        NO-UNDO.
DEFINE VARIABLE liste-typcom2   AS CHARACTER        NO-UNDO.
DEFINE VARIABLE liste-codech    AS CHARACTER        NO-UNDO.
DEFINE VARIABLE tabges-soc      AS CHARACTER        NO-UNDO.
DEFINE VARIABLE tabges-etabli   AS CHARACTER        NO-UNDO.
DEFINE VARIABLE o-param         AS CHARACTER        NO-UNDO.
DEFINE VARIABLE periph-2        AS CHARACTER        NO-UNDO.

/* Chargement du fichier aide */
aide-fichier = "r-fvte10.aid" . aide-par = "" . run aide-lec.p .

DO i = 1 TO 99:
    aide-m = "libel" + STRING(i, "999").
    { aide-lib.i }
    libelle[i] = aide-lib.
END.

/* Chargement des choix en fonction du fichier batch */
RUN bat-lec.p(fichier).

{ bat-cha.i  "sel-applic"    "selection-applic"    "a" }
{ bat-cha.i  "sel-filtre"    "selection-filtre"    "a" }
{ bat-cha.i  "sel-sequence"  "selection-sequence"  "a" }
{ bat-cha.i  "zone-charg-1"  "zone-charg[1]"       "a" }
{ bat-cha.i  "zone-charg-7"  "zone-charg[7]"       "a" }
{ bat-cha.i  "cde-motcle"    "cde-motcle"          "a" }
{ bat-cha.i  "nom-edition"   "nom-edition"         "a" }
{ bat-cha.i  "fact-mini"     "fact-mini"           "a" }
{ bat-cha.i  "fact-maxi"     "fact-maxi"           "a" }
{ bat-cha.i  "typ-tiers"     "typ-tiers"           "a" }
{ bat-cha.i  "cod-tiers"     "cod-tiers"           "a" }
{ bat-cha.i  "datfac-mini"   "datfac-mini"         "d" }
{ bat-cha.i  "datfac-maxi"   "datfac-maxi"         "d" }
{ bat-cha.i  "maquette"      "maquette"            "a" }
{ bat-cha.i  "periph"        "periph"              "a" }
{ bat-cha.i  "ma-esc"        "ma-esc"              "a" }
{ bat-cha.i  "rep-spe-spool" "rep-spe-spool"       "a" }

RUN select-c.p .

RUN sel-del.p(selection-applic, selection-filtre, selection-sequence, YES).

MESSAGE "--------------------------------------------------------------------------------------------------" SKIP(1)
        " Reedition des factures archiv�es" SKIP(1)
        " N� de facture minimum   : " fact-mini SKIP
        " N� de facture maximum   : " fact-maxi SKIP
        " Type tiers Commande     : " typ-tiers SKIP
        " Code tiers Commande     : " cod-tiers SKIP
        " Date de facture minimum : " STRING(datfac-mini) SKIP
        " Date de facture maximum : " STRING(datfac-maxi) SKIP(1)
        " Zone charge 1           : " zone-charg[1] SKIP
        " Zone charge 7           : " zone-charg[7] SKIP(1).

IF zone-charg[7] = "COMPLEO" THEN ASSIGN edi-compleo = TRUE
                                         edi-fond    = TRUE.

IF zone-charg[7] = "EDITIQ"  THEN ASSIGN edi-editiq  = TRUE
                                         edi-fond    = TRUE.

OUTPUT TO r-fvte11.mouc.
MESSAGE "zone-charg[ 7 ] : " zone-charg[7] SKIP
        "rep-spe-spool   : " rep-spe-spool SKIP
        "edi-compleo     : " edi-compleo   SKIP
        "edi-editiq      : " edi-editiq    SKIP
        "edi-fond        : " edi-fond.
OUTPUT CLOSE.

regs-app = "ELODIE".
{ regs-cha.i }

regs-app = "NATHALIE".
{ regs-cha.i }

{ regs-inc.i "magasi" }
magasi-soc = regs-soc.

{ regs-inc.i "articl" }
articl-soc = regs-soc.

/* Chargement Taux EURO / FRF, Devise Societe, Devise Inverse, Top Bascule */
FIND tabges WHERE tabges.codsoc = codsoc-soc
              AND tabges.etabli = ""
              AND tabges.typtab = "PRM"
              AND tabges.prefix = "TX-EUR-FRF"
              AND tabges.codtab = ""
            NO-LOCK NO-ERROR.
IF NOT AVAILABLE tabges THEN
    FIND tabges where tabges.codsoc = ""
                  AND tabges.etabli = ""
                  AND tabges.typtab = "PRM"
                  AND tabges.prefix = "TX-EUR-FRF"
                  AND tabges.codtab = ""
                NO-LOCK NO-ERROR.
IF AVAILABLE tabges THEN
    ASSIGN  x-devcpt   = tabges.libel1[1]
            x-deveur   = tabges.libel1[2]
            x-bascule  = tabges.libel1[3]
            tx-frf-eur = tabges.nombre[1].

/* Chargement parametrage edition et lecture maquette edition */
ASSIGN  ma-maquet = maquette          /* Maquette     */
        ma-entete = "r-fvte1e.p"      /* Gosub entete */
        ma-pied   = "r-fvte1r.p"      /* gosub report */
        ma-spool  = periph
        fic-tri   = selection-sequence
        OVERLAY(fic-tri, 1, 1) = "t".

RUN maq-lect.p.
IF ma-anom <> "" THEN DO:
    MESSAGE "--------------------------------------------------------------------------------------------------" SKIP(1)
            libelle[9] ma-maquet " - " ma-anom.
    RETURN.
END.

IF zone-charg[1] = "LI" THEN edi-info = TRUE.
                        ELSE edi-info = NO.

IF zone-charg[1] = "FOND" THEN edi-fond = TRUE.
                          ELSE edi-fond = NO.

MESSAGE " Maquette                : " maquette SKIP
        " Fichier                 : " periph SKIP
        "--------------------------------------------------------------------------------------------------" SKIP(1).

/* Ouverture spool */
IF SEARCH (periph) <> ? THEN DO:
    OUTPUT STREAM maquette TO VALUE(periph) APPEND.
    PUT STREAM maquette UNFORMATTED SKIP(1).
    
    IF edi-fond THEN PUT STREAM maquette UNFORMATTED SKIP(1).
END.
ELSE
    OUTPUT STREAM maquette TO VALUE(periph).

/* Ouverture fichier de tri */
OUTPUT STREAM kan-tri TO VALUE(fic-tri). 

IF fact-maxi = "" AND cod-tiers = "" THEN fact-maxi = FILL("z", 15).

/* FOR EACH BASTAT sur Numero de facture */
REPEAT WHILE fact-maxi <> "":
    FOR EACH  bastat
        WHERE bastat.codsoc  = codsoc-soc
          AND bastat.motcle  = cde-motcle
          AND bastat.numfac >= fact-mini
          AND bastat.numfac <= fact-maxi
        USE-INDEX numfac NO-LOCK
        BREAK BY (bastat.numfac + bastat.typcom + bastat.numbon):
        
        IF FIRST-OF(bastat.numfac + bastat.typcom + bastat.numbon)
            THEN RUN trt-ligne.
    END.
    
    LEAVE.
END.

/* FOR EACH BASTAT sur Tiers */
REPEAT WHILE cod-tiers <> "":
    FOR EACH  bastat
        WHERE bastat.codsoc  = codsoc-soc
          AND bastat.motcle  = cde-motcle
          AND bastat.typaux  = typ-tiers
          AND bastat.codaux  = cod-tiers
          AND bastat.datfac >= datfac-mini
          AND bastat.datfac <= datfac-maxi
        USE-INDEX codaux NO-LOCK
        BREAK BY (bastat.typaux + bastat.codaux + bastat.typcom + bastat.numbon):
        
        IF FIRST-OF(bastat.typaux + bastat.codaux + bastat.typcom + bastat.numbon)
            THEN RUN trt-ligne.
    END.
    
    LEAVE.
END.

OUTPUT STREAM kan-tri CLOSE.

IF nb-trt <> 0 THEN DO:
    MESSAGE libelle[61].
    
    RUN fic-tri.p(fic-tri). /* Tri du Fichier sequentiel */
END.

ASSIGN  nb-lus = nb-trt
        nb-trt = 0.

INPUT FROM VALUE(fic-tri).

{ rup-def.i NEW }
{ rup-ini.i "'120,80,10'" }

REPEAT:
    IMPORT zone.
    
    /* Rupture-test = zone qui permet de tester les ruptures */
    rupture-test = SUBSTRING(zone, 1, 120).
    
    /*-----------------------------------------------------------*/
    /* Rup-edi.i   = Module gerant les ruptures                  */
    /* r-fvte11.i  = Module contenant les editions et les cumuls */
    /*-----------------------------------------------------------*/
    { rup-edi.i  r-fvte11.i }
    
    IF nb-trt MODULO 50 = 0 THEN DO:
        MESSAGE libelle[62] STRING(nb-trt, ">>>>9") "/" STRING(nb-lus, ">>>>9").
    END.
    
    nb-trt = nb-trt + 1.
END.

INPUT CLOSE.

/* Total derniere facture */
IF ma-nblig <> 0 THEN DO:
   ok-fin = TRUE.
   { r-fvte1p.i }  /* Edition du Pied */
END.

RUN fic-sup.p(fic-tri).  /* Suppression Fichier Tri */

/* Transfert spool dans un r�pertoire sp�cifique */
/* CCO le 11/11/2019 */
IF edi-compleo = YES THEN DO:
    /* Repertoire spool specifique */
    ASSIGN  typtab = "PRM"  prefix = "E-FVTE"  codtab = "REP-SPOOL".
    RUN ch-paray.p(OUTPUT o-param).
    IF o-param <> "" THEN rep-spe-spool = TRIM(ENTRY(4, o-param, "|")).
END.

IF edi-editiq = YES THEN DO:
    /* Repertoire spool specifique */
    ASSIGN  typtab = "PRM"  prefix = "E-FVTE"  codtab = "REP-SPOOL".
    RUN ch-paray.p(OUTPUT o-param).
    IF o-param <> "" THEN rep-spe-spool = TRIM(ENTRY(5, o-param, "|")).
END.

IF rep-spe-spool <> "" THEN DO:
    IF OPSYS = "UNIX" THEN DO:
        OS-COMMAND SILENT VALUE("chmod 777 " + periph).
        
        IF OS-ERROR <> 0 THEN DO:
            IF zone-charg[9] <> "BATCH" THEN
                MESSAGE libelle[156] periph   SKIP
                        libelle[160] OS-ERROR VIEW-AS ALERT-BOX ERROR.
            ELSE
                MESSAGE libelle[156] periph   SKIP
                        libelle[160] OS-ERROR.
        END.
    END. /* IF opsys = "UNIX" THEN DO: */
    
    periph-2 = rep-spe-spool + periph.
    OS-RENAME VALUE(periph) VALUE(periph-2).
    
    OUTPUT TO r-fvte11.mouc APPEND.
    MESSAGE "periph   : " periph   SKIP
            "periph-2 : " periph-2.
    OUTPUT CLOSE.
    
    IF OS-ERROR <> 0 THEN DO:
        IF zone-charg[9] <> "BATCH" THEN DO:
            BELL. BELL.
            MESSAGE libelle[157] periph   SKIP
                    libelle[159] periph-2 SKIP
                    libelle[160] OS-ERROR VIEW-AS ALERT-BOX ERROR.
        END.
        ELSE
            MESSAGE libelle[157] periph   SKIP
                    libelle[159] periph-2 SKIP
                    libelle[160] OS-ERROR.
    END. /* IF OS-ERROR <> 0 THEN DO: */
END. /* IF rep-spe-spool <> "" THEN DO: */


/*************************** P R O C E D U R E S ******************************/

/* Traitement d'une ligne */
PROCEDURE trt-ligne:
    
    IF nb-lus MODULO 500 = 0 THEN DO:
        HIDE MESSAGE NO-PAUSE.
        MESSAGE libelle[60] bastat.numfac bastat.datfac bastat.typaux
                bastat.codaux STRING(nb-trt, ">>>>9") "/" STRING(nb-lus, ">>>>9").
    END.
    
    nb-lus = nb-lus + 1.
    
    /* Execution des selections */
    { select-z.i  "numbon"      "bastat.numbon" }
    { select-z.i  "numfac"      "bastat.numfac" }
    { select-z.i  "magasin"     "bastat.magasin" }
    { select-z.i  "datdep"      "bastat.datdep"  ",'99/99/9999'" }
    { select-z.i  "type-auxil"  "bastat.typaux-fac" }
    { select-z.i  "codaux-fac"  "bastat.codaux-fac" }
    { select-z.i  "typaux-cde"  "bastat.typaux" }
    { select-z.i  "codaux-cde"  "bastat.codaux" }
    
    RUN select-t.p.
    IF selection-ok <> "" THEN RETURN.
    
    /* Chargement de zone pour le tri des bons */
    ASSIGN  x-typaux = bastat.typaux
            x-codaux = bastat.codaux.
    
    IF bastat.typaux-fac <> "" AND bastat.codaux-fac <> "" AND
       bastat.typaux-fac + bastat.codaux-fac <> bastat.typaux + bastat.codaux
        THEN ASSIGN x-typaux = bastat.typaux-fac
                    x-codaux = bastat.codaux-fac.
    
    { l-tiers.i auxili x-typaux x-codaux }
    IF NOT AVAILABLE auxili THEN RETURN.
    
    { l-tiespe.i auxapr x-typaux x-codaux }
    IF NOT AVAILABLE auxapr THEN RETURN.
    
    zone = "".
    
    /* Les 50 premiers caracteres sont pris pour les tri d'edition */
    zone = zone + FILL(" ", 50 - LENGTH(zone)).
    
    /* Les 70 autres caracteres sont pris pour le tri ou le regroupement des bons */
    zone = zone + STRING(x-typaux, "xxx")
                + STRING(x-codaux, "x(10)")
                + STRING(bastat.typaux, "xxx")
                + STRING(bastat.codaux, "x(10)").
    
    IF bastat.modreg <> ""
        THEN x-modreg = bastat.modreg.
        ELSE x-modreg = "CHH".
    
    zone = zone + STRING(x-modreg, "xxx").
    
    IF echeance = ? AND bastat.typbon = "C"
        THEN echeance = bastat.datfac.
        ELSE echeance = bastat.datech.
    
    zone = zone + STRING(YEAR(echeance), "9999")
                + STRING(MONTH(echeance), "99")
                + STRING(DAY( echeance), "99").
    
    IF bastat.typbon = "C"
        THEN zone = zone + STRING(CAPS(bastat.typbon), "X").
        ELSE zone = zone + "T".
    
    zone = zone + STRING(bastat.numfac, "x(10)"). /* Ajout numfac pour rupture facture */
         
    zone = zone + FILL(" ", 120 - length(zone)).
    
    zone = zone + STRING(bastat.numbon, "x(10)")
                + STRING(bastat.chrono, "99999")
                + STRING(bastat.typcom, "xxx").
    
    zone = zone + FILL(" ", 200 - LENGTH(zone)).
    
    /* Rajout tri des bons sur date de livraison */
    zone = zone + STRING(YEAR(bastat.datdep), "9999")
                + STRING(MONTH(bastat.datdep), "99")
                + STRING(DAY(bastat.datdep), "99").
    
    zone = zone + STRING(RECID(bastat), "9999999999").
    zone = '"' + zone + '"'.

    PUT STREAM kan-tri UNFORMATTED zone SKIP.
    
    nb-trt = nb-trt + 1.
    
END PROCEDURE.

{ selectpt.i } /* Multi selections */

