/*==========================================================================*/
/*                          R F C - E X T C . I                             */
/*          Gestion des RFC CLIENTS - EXTRACTIONS DES RFC DE BASTAT         */
/*          Module de test des conditions requises pour Remises             */
/*          MOT-CLE = "CLA" = Remises du Controle Laitier                   */
/*==========================================================================*/


/* mettre la Variable logical TOP-VALIDE a NO                   */
/* pour que l'enreg ne soit pas traite                          */

/* Zones de test : - Famille    = artapr.famart                         */
/*                 - Categorie  = artapr.soufam                         */
/*                 - Articl     = bastat.articl                         */
/*                 - Bareme     = no-bar - affecte a la ligne article   */
/*                 - Bareme Client = auxapr.bareme-a-1                  */
/*----------------------------------------------------------------------*/


def var list-cat-c    as char                                  no-undo .
def var categ-c       as char                                  no-undo .

/*
assign list-cat-c  = "012011,032011,032012,032015" .
*/
assign list-cat-c  = "032002011,032002012,032002021,032002022,032002041" .
assign list-cat-c  = list-cat-c + ",032002042".

VALID-B :
REPEAT  :

    categ-c = artapr.famart + artapr.soufam + artapr.sssfam .

    if lookup( categ-c , list-cat-c )  = 0
    then do :
        top-valide = no .
        leave valid-b .
    end .

 Leave .

 END . /* repeat transaction */