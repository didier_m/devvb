

    /* message "trasites" auxapr.codsoc auxapr.codaux . */
if auxapr.codsoc = "08" then wgroupe = "08".
                        else wgroupe = "01".

    FIND AUXDET
        where auxdet.codsoc = auxapr.codsoc
        and   auxdet.typaux = auxapr.typaux
        and   auxdet.codaux = auxapr.codaux
        and   auxdet.motcle = "ACT"
        and   auxdet.codtab = "000"
            no-lock no-error .

    zone = "TIE  " + string ( wgroupe , "xx" )
                 + substr(auxapr.codaux , 5, 6) + "       "
                 + string ( auxili.adres[1] , "x(32)" )
                 + "     "
                 .

    if available auxdet
    then zone = zone + string ( auxdet.calf05 , "xxx" ) + "       ".
    else zone = zone + "          " .

    zone = zone + string ( substring( auxapr.onic-commune , 2 , 5 ) , "x(5)" )
                + "     "
                + string ( auxapr.magasin , "xxx" )
                + "  "
                .

    put stream trajector unformatted zone skip .

