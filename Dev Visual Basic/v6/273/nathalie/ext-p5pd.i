/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/ext-p5pd.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 16680 22-02-08!Evo!cch            !Ajout exrraction RPD, no AMM et top EAJ ( fait par EUREA - DGR )      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - P 5 P D  . I                           */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Redevance pollution diffuse                                   */
/*==========================================================================*/
                                                        /* Pos.  Long. */

zone = "TARBASE "                                       /* 001 - 008 */
       + "C"                                            /* 009 - 001 */
       + string(codsoc-trait[nsoc], "xx")               /* 010 - 002 */
       + fill(" ", 6)                                   /* 012 - 003 */

       + fill(" ", 2)                                   /* 018 - 002 */
       .

if {tarcli.i}.chrono = 3 then zone = zone + string("LOIRE-BR","x(8)").
if {tarcli.i}.chrono = 5 then zone = zone + string("RHONE-ME","x(8)").

zone = zone
       + string({tarcli.i}.articl, "x(15)")             /* 028 - 015 */
       + string({tarcli.i}.date-debut, "99999999")      /* 043 - 008 */
       .

case {tarcli.i}.valeur :                                /* 051 - 009 */
    when 0.001 then wvaleur = 0.
    otherwise       wvaleur = {tarcli.i}.valeur .
end case.

zone = zone
       + string(wvaleur * 1000, "999999999")
       .

if {tarcli.i}.date-fin <> ?                             /* 060 - 008 */
   then zone = zone + string({tarcli.i}.date-fin, "99999999").
   else zone = zone + "31122050".

zone = zone
       + fill(" ", 8)                                   /* 068 - 008 */
       + "N".                                           /* 076 - 001 */
zone = zone
       + fill(" ", 8)                                   /* 077 - 008 */
       + fill(" ", 9)                                   /* 085 - 009 */
       + "RPD"                                          /* 094 - 003 */
       . 
 zone = zone + fill(" ", 5).

zone = zone
       + fill(" ", 6)                                   /* 102 - 006 */
       + "A"                                            /* 108 - 001 */
       + " "                                            /* 109 - 001 */
       + fill("0", 7)                                   /* 110 - 007 */
       .

if {tarcli.i}.datcre <> ?                                   /* 117 - 008 */
   then zone = zone + string({tarcli.i}.datcre, "99999999").
   else zone = zone + fill( " ", 8 ).

if {tarcli.i}.datmaj <> ?                                   /* 125 - 008 */
   then zone = zone + string({tarcli.i}.datmaj, "99999999").
   else zone = zone + fill( " ", 8 ).

zone = zone
       + fill(" ", 12)                                  /* 133 - 012 */
       + "E"
       .