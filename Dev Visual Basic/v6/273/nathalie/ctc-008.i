/*=======================================================================*/
/*                             C T C - 0 0 8 . I                         */
/*       Constitution du Fichier TARPRE : Preparation Tarif Clients      */
/*=======================================================================*/
 
FIND ARTBIS where artbis.motcle = ctc-motcle
            and   artbis.codsoc = codsoc-soc
            and   artbis.articl = {1}.articl
            and   artbis.typaux = {1}.typaux
            and   artbis.codaux = {1}.codaux
            use-index artbis-1  no-lock  no-error .
if available artbis then
do :
    /*    Prix de l'unite de Stock = pxacha * coef-fac / coef-cde .  */
    if artbis.coef-fac > 0 then pxacha =     pxacha * artbis.coef-fac .
    if artbis.coef-fac < 0 then pxacha = - ( pxacha / artbis.coef-fac ) .
    if artbis.coef-cde > 0 then pxacha =     pxacha / artbis.coef-cde .
    if artbis.coef-cde < 0 then pxacha = - ( pxacha * artbis.coef-cde ) .
    pxacha = round( pxacha , 2 ) .
end .
 