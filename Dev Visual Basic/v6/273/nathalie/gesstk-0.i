/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/gesstk-0.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V61 43862 10-01-14!Bug!pha            !problemes variables partag�es sai-000.i                               !
!V61 40749 04-09-13!Bug!cch            !Correction perte imprimante initiale de l'utilisateur                 !
!V61 38303 23-05-13!Bug!cch            !Remplacement s�parateur virgule par pipe                              !
!V61 30012 06-04-12!Evo!cch            !Ajout Traitement Int�grations issues de STOCK-IT- EUREA               !
!V61 30007 06-04-12!Evo!cch            !Ajout Traitement Int�grations issues de STOCK-IT- EUREA               !
!V61 28024 19-12-11!Bug!cch            !STOCK-IT - TCS : Correction pble g�n�. de 2 fich. + msg err. 'sd-entet!
!V61 27947 14-12-11!Evo!cch            !G�n�ration enreg. ENTSPE pour identifier bons STOCK-IT au lieu de ENTE!
!V61 27785 07-12-11!Bug!cch            !Correction probl�me perte op�rateur de connexion                      !
!V61 27583 29-11-11!Evo!cch            !Ajout extr. cdes clts bulk, transferts, retours clts et fourn         !
!V61 27398 21-11-11!Evo!cch            !Ajout Extraction des Commandes achat classique vers STOCK-IT          !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/* GESSTK-0.I - GESTION DES ECHANGES DE DONNEES NATHALIE / STOCK_IT           */
/*----------------------------------------------------------------------------*/
/* Variables Communes                                11/10/2011 - EUREA - 273 */
/******************************************************************************/

def stream s-blo .
def stream s-fic .
def stream s-rej .

def var i-param-2  as char          no-undo . 
def var auxspe-soc as char          no-undo .
def var magasi-soc as char          no-undo .
def var articl-soc as char          no-undo .
def var operat-maj as char          no-undo .
def var operat-sav as char          no-undo .
def var nomimp     as char          no-undo .
def var opeimp-sav as char          no-undo .
def var fic-trt    as char          no-undo .
def var fic-ren    as char          no-undo .
def var fic-blo    as char extent 2 no-undo .
def var enreg      as char          no-undo .
def var motcle     as char          no-undo .
def var clef       as char          no-undo .
def var edition    as char          no-undo .
def var zone       as char          no-undo .
def var zone-2     as char          no-undo .
def var mess-err   as char          no-undo .
def var trt-ok     as log           no-undo .
def var zone-ok    as log           no-undo .
def var zone-ok-2  as log           no-undo .
def var ind        as int           no-undo .
def var ind-2      as int           no-undo .
def var ind-3      as int           no-undo .
def var fic-no     as int           no-undo .
def var nb-lu      as int           no-undo .
def var nb-trt     as int           no-undo .
def var val-int    as int           no-undo .
def var nb-sec-att as int           no-undo .
def var nb-err     as int           no-undo .

def {1} shared var libelle       as char extent 120                     no-undo .
def {1} shared var typtab-trt    as char                                no-undo .
def {1} shared var prefix-trt    as char                                no-undo .
def {1} shared var codtab-trt    as char                                no-undo .
def {1} shared var repert-depart as char                                no-undo .
def {1} shared var repert-arriv  as char                                no-undo .
def {1} shared var repert-traite as char                                no-undo .
def {1} shared var repert-rejet  as char                                no-undo .
def {1} shared var repert-trav   as char                                no-undo .
def {1} shared var type-appel    as char                                no-undo .
def {1} shared var type-trt      as char                                no-undo .
def {1} shared var l-type-trt    as char                                no-undo .
def {1} shared var l-soc         as char extent 3                       no-undo .
def {1} shared var l-motcle      as char                                no-undo .
def {1} shared var l-typcom      as char extent 2                       no-undo .
def {1} shared var l-type-tiers  as char                                no-undo .
def {1} shared var l-fic         as char                                no-undo .
def {1} shared var l-extens      as char                                no-undo .
def {1} shared var l-param-div   as char                                no-undo .
def {1} shared var l-mag         as char extent 3                       no-undo .
def {1} shared var l-typbon      as char                                no-undo .
def {1} shared var l-art-autori  as char extent 2                       no-undo .
def {1} shared var l-provenance  as char                                no-undo .
def {1} shared var fic-nom       as char                                no-undo .
def {1} shared var fic-rej       as char            format "x(50)"      no-undo .
def {1} shared var mouchard      as log             format "O/N"        no-undo .
def {1} shared var ext-trt       as char                                no-undo .
def {1} shared var ext-rej       as char                                no-undo .
def {1} shared var ext-blo       as char                                no-undo .
def {1} shared var motcle-ach    as char                                no-undo .
def {1} shared var motcle-vte    as char                                no-undo .
def {1} shared var motcle-stk_it as char                                no-undo .
def {1} shared var top-trt       as char extent 3                       no-undo .
def {1} shared var separ-zone    as char                                no-undo .
def {1} shared var date-j        as char                                no-undo .
def {1} shared var date-maj      as date            format "99/99/9999" no-undo .
def {1} shared var trt-rejet     as log                                 no-undo .
def {1} shared var trt-test      as log                                 no-undo .
def {1} shared var rowid-enr     as rowid                               no-undo .
def {1} shared var ind-trt       as int                                 no-undo .





