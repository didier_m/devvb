/*==========================================================================*/
/*                           C P T - V T T 1 . I                            */
/*                 Comptabilisation des Ventes ( Tiers )                    */
/*==========================================================================*/

/*-----------------------------------------------------------------------*/
/*         Zones utilisees pour le module edition des ruptures           */
/*                                                                       */
/*                   Rupture-a          : Niveau de rupture en cours     */
/*                   rupture-etat = "t" : Edition du titre de la rupture */
/*                   Rupture-etat = "r" : Edition de la rupture          */
/*-----------------------------------------------------------------------*/

run cpt-vtt2.p( rupture-etat , rupture-a ) .
