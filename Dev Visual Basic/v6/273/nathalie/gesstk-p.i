/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/gesstk-p.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V61 40749 04-09-13!Bug!cch            !Correction perte imprimante initiale de l'utilisateur                 !
!V61 38303 23-05-13!Bug!cch            !Remplacement s�parateur virgule par pipe                              !
!V61 30814 29-05-12!Evo!cch            !Ajout top inactif dans le param�trage g�n�ral STOCK                   !
!V61 30035 10-04-12!Evo!cch            !Ajouit envoi notion Article Gratuit sur Extraction des Commandes      !
!V61 30012 06-04-12!Evo!cch            !Ajout Traitement Int�grations issues de STOCK-IT- EUREA               !
!V61 30007 06-04-12!Evo!cch            !Ajout Traitement Int�grations issues de STOCK-IT- EUREA               !
!V61 28024 19-12-11!Bug!cch            !STOCK-IT - TCS : Correction pble g�n�. de 2 fich. + msg err. 'sd-entet!
!V61 27947 14-12-11!Evo!cch            !G�n�ration enreg. ENTSPE pour identifier bons STOCK-IT au lieu de ENTE!
!V61 27900 13-12-11!Evo!cch            !Ajout extraction Cdes transferts entr�es + Ordres Fabrications STK-IT !
!V61 27785 07-12-11!Bug!cch            !Correction probl�me perte op�rateur de connexion                      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/* GESSTK-P.I - GESTION DES ECHANGES DE DONNEES NATHALIE / STOCK_IT           */
/*----------------------------------------------------------------------------*/
/* Procedures communes                                11/10/2011 - EUREA - 273 */
/******************************************************************************/

/* Chargement parametres de base */
PROCEDURE CHARG-PARAM :

    assign trt-ok     = no
           typtab-trt = "STK"
           prefix-trt = "GES-STK_IT"
           codtab-trt = "PARAMETRES"
           .

    /* Acces au parametrage EDI STOCK_IT */
    assign  typtab = typtab-trt  prefix = prefix-trt  codtab = codtab-trt .
    run ch-paray.p( output o-param ) .

    if o-param <> "" and int( entry( 73, o-param, "|" ) ) = 1 then return . /* Traitements STOCK_IT inactifs */

    if nb-sec-att <> 0 and o-param <> ""
    then do :
        if int( entry( 71, o-param, "|" ) ) > 0 and int( entry( 72, o-param, "|" ) ) <> 1 then trt-ok = yes . /* Bouclage Batch Integration */
        return .
    end .

    if i-param-2 = "ARRET-BATCH" and o-param <> ""
    then do :
        assign l-type-trt = entry( 8,  o-param, "|" )                  /* Types de traitements                                    */
               type-trt   = trim( zone-charg[ 1 ] )
               trt-ok     = yes
               .
        return .
    end .

    /* Chargement aide et texte */
    aide-fichier = "gesstk-0.aid" . aide-par = "<TRAITEMENTS>" .
    run aide-lec.p .
    do ind = 1 to 120 :
        aide-m = "libel" + string( ind, "999" ) .
        { aide-lib.i }
        libelle[ ind ] = aide-lib .
    end .

    assign repert-depart     = entry( 1,  o-param, "|" )               /* Rep. export fichiers                                    */
           repert-arriv      = entry( 2,  o-param, "|" )               /* Rep. import fich.                                       */
           repert-traite     = entry( 3,  o-param, "|" )               /* Rep. sav fichiers                                       */
           repert-rejet      = entry( 4,  o-param, "|" )               /* Rep. rejets fichiers                                    */
           repert-trav       = entry( 5,  o-param, "|" )               /* Rep. de travail                                         */
           operat-maj        = entry( 6,  o-param, "|" )               /* Operateur MAJ                                           */
           nomimp            = entry( 7,  o-param, "|" )               /* Imprimante                                              */
           l-type-trt        = entry( 8,  o-param, "|" )               /* Types de traitements                                    */
           l-soc[ 1 ]        = entry( 9,  o-param, "|" )               /* Societes traitees                                       */
           l-fic             = entry( 10, o-param, "|" )               /* Radicals Noms des fichiers                              */
           l-extens          = entry( 31, o-param, "|" )               /* Extensions fichiers                                     */
           l-param-div       = entry( 32, o-param, "|" )               /* Parametres Divers                                       */
           l-mag[ 1 ]        = entry( 33, o-param, "|" )               /* Magasins traites ( gesdec-t.p )                         */
           l-typcom[ 1 ]     = entry( 34, o-param, "|" )               /* Types Pieces traitees extraction ( gesdec-t.p )         */
           l-soc[ 2 ]        = entry( 35, o-param, "|" )               /* Societes traitees extraction Tiers                      */
           l-motcle          = entry( 36, o-param, "|" )               /* Motcles traites                                         */
           l-type-tiers      = entry( 37, o-param, "|" )               /* Types de Tiers traites                                  */
           l-mag[ 2 ]        = entry( 38, o-param, "|" )               /* Magasins Bulk                                           */
           l-typcom[ 2 ]     = entry( 39, o-param, "|" )               /* Types Pieces Livraisons crees integration               */
           l-typbon          = entry( 40, o-param, "|" )               /* Types de Commandes                                      */
           l-art-autori[ 1 ] = entry( 41, o-param, "|" )               /* Articles autorises sans commande initiale               */
           l-art-autori[ 2 ] = entry( 42, o-param, "|" )               /* Articles autorises ss cde initiale ( reprise palettes ) */
           l-provenance      = entry( 43, o-param, "|" )               /* Provenances Articles                                    */
           l-mag[ 3 ]        = entry( 44, o-param, "|" )               /* Magasins pour generation Regul. / Inventaire            */
           l-soc[ 3 ]        = entry( 45, o-param, "|" )               /* Societes MAJ Articles                                   */
           l-param-div       = l-param-div + entry( 46, o-param, "|" ) /* Parametres Divers Suite                                 */
           ext-trt           = entry( 1, l-extens )
           ext-rej           = entry( 2, l-extens )
           ext-blo           = entry( 3, l-extens )
           type-trt          = trim( zone-charg[ 1 ] )
           type-appel        = zone-charg[ 5 ] 
           ind-trt           = lookup( substr( type-trt, 1, 9 ), l-type-trt )
           no-error .

    if error-status:error
    then do :

           if zone-charg[ 9 ] <> "BATCH"
           then do :
               bell . bell .
               message libelle[ 41 ] skip
                       libelle[ 42 ] skip
                       libelle[ 45 ]
                       view-as alert-box error .
           end .

           else do :
               message libelle[ 41 ] .
               message libelle[ 42 ] .
               message libelle[ 45 ] .
           end .

           return .

    end .

    if ind-trt = 0
    then do ind = 1 to num-entries( l-type-trt ) :
        if entry( ind, l-type-trt ) begins type-trt
        then do :
            ind-trt = ind .
            leave .
        end .
    end .

    if ext-trt = "" then ext-trt = ".trt" .
    if ext-rej = "" then ext-rej = ".rej" .
    if ext-blo = "" then ext-blo = ".blo" .

    if operat-maj <> "" then assign operat-sav = operat
                                    operat     = operat-maj
                                     .

    if nomimp <> "" then assign opeimp-sav = opeimp
                                opeimp     = nomimp
                                .

    if num-entries( l-param-div ) < 26 then l-param-div = "NAT,O,N,E,L,ELF,/,ACH,VTE,TRA,LIV,NLV,FAB,MAG,CLI,FOU,F,CO+,CO-,DES,INV,BLO,DBL,E,S,G" .

    if zone-charg[ 2 ] = "REJET" then trt-rejet = yes .
    if zone-charg[ 3 ] = "TEST"  then trt-test  = yes .
    if zone-charg[ 4 ] <> "" then rowid-enr = to-rowid( zone-charg[ 4 ] ) .

    assign motcle-ach    = "ACH"
           motcle-vte    = "VTE"
           motcle-stk_it = "/STK"
           top-trt[ 1 ]  = "SN"
           top-trt[ 2 ]  = "SE"
           top-trt[ 3 ]  = "SL"
           separ-zone    = "|"
           date-j        = string( today, "999999" )                  + "-" +
                           substr( string( time, "hh:mm:ss" ), 1, 2 ) +
                           substr( string( time, "hh:mm:ss" ), 4, 2 ) +
                           substr( string( time, "hh:mm:ss" ), 7, 2 )
           trt-ok        = yes
           .

END PROCEDURE . /* CHARG-PARAM */

/* Traitements d'initialisations Fichiers */
PROCEDURE TRT-INIT :

    
    assign trt-ok = no
           ind-trt = lookup( substr( type-trt, 1, 9 ), l-type-trt )
           fic-nom = entry( ind-trt, l-fic ) 
           no-error .
        
    if ind-trt = 0 or fic-nom = ""
    then do :

        if zone-charg[ 9 ] <> "BATCH"
        then do :
            bell . bell .
            message type-trt libelle[ 43 ] skip
                    libelle[ 45 ]
                    view-as alert-box error .
        end .

        else do :
            message type-trt libelle[ 43 ] .
            message libelle[ 45 ] .
        end .

        return .

    end .

    assign ind          = 0
           fic-blo[ 1 ] = repert-trav + type-trt + ext-blo
           .
    
    /* Recherche si trt deja en cours */
    REPEAT :

        if search( fic-blo[ 1 ] ) <> ? and ( ind-trt = 1 or ind-trt = 3 ) and rowid-enr <> ? /* Extraction Articles / Commandes automatique */
        then do :
            if opsys = "UNIX" then os-command silent value ( "sleep 1" ) .
            ind = ind + 1 .
            if ind = 5 then leave .
            next .
        end .

        else do :
            trt-ok = yes .
            leave .
        end .

    END . /* REPEAT : */
    
    
    if not trt-ok
    then do :

        if zone-charg[ 9 ] <> "BATCH"
        then do :
            bell . bell .
            message type-trt libelle[ 44 ] skip
                    libelle[ 45 ]
                    view-as alert-box warning .
        end .

        else do :
            message type-trt libelle[ 44 ] .
            message libelle[ 45 ] .
        end .

        return .

    end .
    
    trt-ok = no .

    /* Pose Fichier blocage */
    output stream s-blo to value( fic-blo[ 1 ] ) .
    output stream s-blo close .

    case ind-trt :

        when 1 or when 2 or when 3 or when 4  or when 8 then do : /* Extraction */

            run lect-no-fic . /* Recup. No de Fichier */

            /* Ouverture Fichier */
            /* DGR 22/08/2016 : pour ne pas transfer en fichier en cours d'ecriture 
                => ecriture dans /applic/li/travail/stockit  */
            
            fic-ren = repert-depart + fic-nom + string( fic-no, fill( "9", 6 ) ) .          
            fic-trt = repert-trav   + fic-nom + string( fic-no, fill( "9", 6 ) ) .
            
            output stream s-fic to value( fic-trt ) append . 
        
            
        end . /* when 1 */

    end case . /* case ind-trt : */
    
    trt-ok = yes .
    

END PROCEDURE . /* TRT-INIT */

/* Traitements de fin */
PROCEDURE TRT-FIN :

    case ind-trt :

        when 1 or when 2 or when 3 or when 4 or when 8 then do : /* Extraction */

            run maj-no-fic . /* MAJ No de Fichier */

            output stream s-fic close . /* Fermeture Fichier */
            
            /* DGR 22/08/2016 : pour ne pas transfer en fichier en cours d'ecriture 
            rename du fichier de /applic/li/travail/stockit vers  /applic/li/depart/stockit  */
            os-rename value( fic-trt ) value( fic-ren ) .

        end . /* when 1 or when 2 or when 3 or when 4 then do : */

    end case . /* case ind-trt : */

    /* Suppression du fichier de blocage */
    os-delete value( fic-blo[ 1 ] ) .
    if os-error <> 0
       then if zone-charg[ 9 ] <> "BATCH"
            then do :
                bell . bell .
                message libelle[ 46 ] fic-blo[ 1 ] skip
                        libelle[ 47 ] os-error
                        view-as alert-box error .
            end .
            else do :
                message libelle[ 46 ] fic-blo[ 1 ] .
                message libelle[ 47 ] os-error .
            end .

END PROCEDURE . /* TRT-FIN */

/* Lecture No Fichier */
PROCEDURE LECT-NO-FIC :
    
    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = typtab-trt
                and   tabges.prefix = prefix-trt
                and   tabges.codtab = codtab-trt
                no-lock no-error .

    if available tabges
    then do :
        fic-no = tabges.nombre[ ind-trt ] + 1 .
        if fic-no > 999999 then fic-no = 1 .
    end .

    else fic-no = 1 .

END PROCEDURE . /* LECT-NO-FIC */

 /* Maj. No de Fichier */
PROCEDURE MAJ-NO-FIC :

    if trt-test then return .

    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = typtab-trt
                and   tabges.prefix = prefix-trt
                and   tabges.codtab = codtab-trt
                exclusive-lock no-error .

    if not available tabges
    then do :

        CREATE TABGES .

        assign tabges.codsoc = ""
               tabges.etabli = ""
               tabges.typtab = typtab-trt
               tabges.prefix = prefix-trt
               tabges.codtab = codtab-trt
               .

    end .

    tabges.nombre[ ind-trt ] = fic-no .

    { majmoucb.i tabges }

    VALIDATE TABGES .
    RELEASE TABGES .

END PROCEDURE . /* MAJ-NO-FIC */






