/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/r-fvte10.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 302080 2019-11-18!Bug!p-o teyssier   !Synchronisation code deal et client + corrections                     !
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V52 15055 12-07-06!Evo!cch            !Acc�s ARTAPR avec soci�t� de regroupement suite mise en place ED      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                            R - F V T E 1 0 . I                           */
/*  REEDITION de FACTURE A PARTIR de BASTAT                                 */
/*==========================================================================*/

{ e-ediinc.i {1} }

def {1} shared var msk71  as char  init  "->>>>>>9.9" .
def {1} shared var msk72  as char  init  "->>>>>>9.99" .
def {1} shared var msk62  as char  init  "->>>>>9.99" .
def {1} shared var msk51  as char  init  "->>>>9.9" .
def {1} shared var msk52  as char  init  "->>>>9.99" .
def {1} shared var msk53  as char  init  "->>>>9.999" .
def {1} shared var msk42  as char  init  "->>>9.99" .
def {1} shared var msk33  as char  init  "->>9.999" .
def {1} shared var msk22  as char  init  "->9.99" .

def {1} shared var msk60 as char init "->>>>>9" .
def {1} shared var msk70 as char init "->>>>>>9" .
def {1} shared var msk63 as char init "->>>>9.999" .
def {1} shared var msk73 as char init "->>>>>9.999" .
def {1} shared var msk82 as char init "->>>>>>9.99" .

def {1} shared var nb-trt        as int     no-undo .
def {1} shared var ok-fin        as logical no-undo .
def {1} shared var edi-fond      as logical no-undo .
def {1} shared var edi-compleo   as logical no-undo .
def {1} shared var edi-editiq    as logical no-undo .
def {1} shared var rep-spe-spool as char    no-undo .
def {1} shared var edi-info      as logical no-undo .
def {1} shared var zone-info     as char    no-undo .

def {1} shared var libelle     as char                 extent 200   no-undo .
def {1} shared var valeur      as char                 extent 130   no-undo .
def {1} shared var rg          as int                               no-undo .

def {1} shared var cde-motcle    as char format  "xxx"              no-undo .
def {1} shared var nom-edition   as char                            no-undo .
def {1} shared var nom-sequence  as char                            no-undo .

def {1} shared var codsoc-bl   as char format  "xx"  initial "  "   no-undo .
def {1} shared var etabli-bl   as char format "xxx"  initial "   "  no-undo .
def {1} shared var t-pf1       as char format "xxxx" initial "pf1," no-undo .
def {1} shared var prefix-1    as char format "xxx"                 no-undo .
def {1} shared var prefix-2    as char format "x(10)"               no-undo .

def {1} shared var fact-mini   like bastat.numfac                   no-undo .
def {1} shared var fact-maxi   like bastat.numfac                   no-undo .
def {1} shared var typ-tiers   like bastat.typaux                   no-undo .
def {1} shared var cod-tiers   like bastat.codaux                   no-undo .
def {1} shared var datfac-mini like bastat.datfac                   no-undo .
def {1} shared var datfac-maxi like bastat.datfac                   no-undo .
def {1} shared var lib-tiers   as char format "x(32)"               no-undo .

def {1} shared var x-typaux    like bastat.typaux                   no-undo .
def {1} shared var x-codaux    like bastat.codaux                   no-undo .
def {1} shared var x-modreg    like auxapr.modreg                   no-undo .
def {1} shared var lib-modreg  as char                              no-undo .
def {1} shared var lib-prelev  as char  format "x(70)"              no-undo .
def {1} shared var x-codech    like auxapr.codech                   no-undo .
def {1} shared var x-numfac    like bastat.numfac                   no-undo .
def {1} shared var x-datcpt    as char                              no-undo .
def {1} shared var alf-datfac  as char                              no-undo .
def {1} shared var alf-datech  as char                              no-undo .
def {1} shared var x-numbon    as char format "x(16)"               no-undo .
def {1} shared var x-datfac    like bastat.datfac                   no-undo .
def {1} shared var x-type-adh  as char                              no-undo .
def {1} shared var fic-tri     as char                              no-undo .

def {1} shared var maquette    as char format "x(12)"               no-undo .

def {1} shared var num-page    as int                               no-undo .

def {1} shared var i           as int                               no-undo .
def {1} shared var j           as int                               no-undo .
def {1} shared var a-a         as int                               no-undo .

def {1} shared var periph       as char format "x(35)"              no-undo .
def {1} shared var sav-periph   as char format "x(35)"              no-undo .
def {1} shared var lib-maq      as char format "x(30)"              no-undo .
def {1} shared var z-choix      as char                             no-undo .
def {1} shared var zone         as char                             no-undo .

def {1} shared var bastat-recid as recid                            no-undo .
def {1} shared var auxapr-recid as recid                            no-undo .
def {1} shared var auxili-recid as recid                            no-undo .

def {1} shared var zone-tva   as char format "xxx"           no-undo .
def {1} shared var code-tva   as char format "xxx" extent 10 no-undo .
def {1} shared var taux-tva   as dec decimals 3    extent 10 no-undo .
def {1} shared var base-tva   as dec decimals 3    extent 10 no-undo .
def {1} shared var mtt-tva    as dec decimals 3    extent 10 no-undo .
def {1} shared var tva-lig    as dec decimals 3              no-undo .
def {1} shared var ht-lig     as dec decimals 3              no-undo .
def {1} shared var ttc-lig    as dec decimals 3              no-undo .
def {1} shared var ht-fac     as dec decimals 3              no-undo .
def {1} shared var tva-fac    as dec decimals 3              no-undo .
def {1} shared var ttc-fac    as dec decimals 3              no-undo .
def {1} shared var tx-frf-eur as dec                         no-undo .
def {1} shared var x-bascule  as char                        no-undo .
def {1} shared var x-devcpt   as char                        no-undo .
def {1} shared var x-deveur   as char                        no-undo .

def {1} shared var a-libart as char                        no-undo .
def {1} shared var a-ucde   as char format "xxx"           no-undo .
def {1} shared var a-ufac   as char format "xxx"           no-undo .
def {1} shared var r-motcle like bastat.motcle             no-undo .
def {1} shared var r-numfac like bastat.numfac             no-undo .
def {1} shared var r-typcom like bastat.typcom             no-undo .
def {1} shared var r-numbon like bastat.numbon             no-undo .

def {1} shared var tot-remfac  as dec                      no-undo .

def {1} shared var art-bio     as log                      no-undo .

def {1} shared var edi-totbon  as log                      no-undo .

def {1} shared var magasi-soc  as char                     no-undo .
def {1} shared var articl-soc  as char                     no-undo .

def {1} shared frame fr-saisie .

form libelle[ 11 ] format "x(23)"  fact-mini             skip
     libelle[ 12 ] format "x(23)"  fact-maxi             skip
     libelle[ 13 ] format "x(23)"  typ-tiers             skip
     libelle[ 14 ] format "x(23)"  cod-tiers   lib-tiers skip
     libelle[ 15 ] format "x(23)"  datfac-mini           skip
     libelle[ 16 ] format "x(23)"  datfac-maxi           skip
     libelle[ 17 ] format "x(23)"  periph
     with frame fr-saisie
     row 2  centered  overlay  no-label { v6frame.i }.
