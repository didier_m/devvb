/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/ext-agildat.i                                                              !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 61252 11-10-16!Evo!pha            !report modifs 273                                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - A G I L D A T . I                      */
/* Extraction des donnees pour AGIL                                         */
/*--------------------------------------------------------------------------*/
/* Chargement date minimum de traitement                                    */
/*==========================================================================*/

/* {1} = no-lock ou exclusive-lock */

assign typtab = "PRM"
       prefix = "EXT-AGIL"
       codtab = "DERN-TRAIT"
       .

FIND DATE-TABGES where date-tabges.codsoc = ""
                 and   date-tabges.etabli = ""
                 and   date-tabges.typtab = typtab
                 and   date-tabges.prefix = prefix
                 and   date-tabges.codtab = codtab
                 {1} no-error.

if not available date-tabges
then do :
    create date-tabges .
    assign date-tabges.codsoc    = ""
           date-tabges.etabli    = ""
           date-tabges.typtab    = typtab
           date-tabges.prefix    = prefix
           date-tabges.codtab    = codtab

           date-tabges.dattab[1] = today - 1
           .
end.

date-debut = date-tabges.dattab[1] + 1.

