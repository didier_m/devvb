/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/ext-p5ar.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 15055 12-07-06!Evo!cch            !Acc�s ARTAPR avec soci�t� de regroupement suite mise en place ED      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - P 5 A R  . I                           */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Articles PDP V5                                 CC 01/03/1999 */
/*==========================================================================*/
/* DGR 221204 : le test pour le non envoi sur PDP est fait dans ext-don1.p  */
/* DGR 110106 : rajout extraction type-client--produits phytos interdits GP */
/* DGR 130106 : rajout test sur artapr.bloque                               */
/* DGR 071106 : rajout code filiere pour EUREA DISTRI                       */

                                                        /* Pos.  Long. */

zone = "ARTICLE "                                       /* 001 - 008 */
       + "C"                                            /* 009 - 001 */
       + string(codsoc-trait[nsoc], "xx")               /* 010 - 002 */
       + fill(" ", 6)                                   /* 012 - 006 */
       + string(artapr.articl, "x(15)")                 /* 018 - 015 */
       + string(artapr.famart, "xxx")                   /* 033 - 003 */
       + string(artapr.soufam, "xxx")                   /* 036 - 003 */
       + string(artapr.libart1[mem-langue], "x(30)")    /* 039 - 030 */
       + string(artapr.libart, "x(15)")                 /* 069 - 015 */ 
       + substr(artapr.tva, 3, 1)                       /* 084 - 001 */
       .

case artapr.prx-modif :                                 /* 085 - 005 */
    when "0" then zone = zone + "09999".
    otherwise     zone = zone + string(artapr.fourche-px * 100, "99999").
end case .

zone = zone
       + fill(" ", 60)                                  /* 090 - 060 */
       + string(artapr.sssfam, "xxx")                   /* 150 - 003 */
       + fill(" ", 2)                                   /* 153 - 002 */
       .

case artapr.divers :                                    /* 155 - 001 */
    when "1" then zone = zone + "O".
    otherwise zone = zone + " ".
end case.            

zone = zone + fill(" ", 35).                            /* 156 - 035 */       

case artapr.type-condit :                               /* 191 - 001 */
    when "VR" then zone = zone + "O".
    otherwise      zone = zone + "N".
end case.

zone = zone
       + fill("0", 6).                                  /* 192 - 006 */

                                                        /* 198 - 001 */
if artapr.famart = "010" and 
   artapr.soufam = "073" and 
   artapr.code-blocage <> "F"
   then zone = zone + "C".
   else if artapr.provenance <> "9"
        then if artapr.provenance = "8"
           then if artapr.code-blocage = "" or artapr.code-blocage = "O"   
                   then zone = zone + "8".
                   else if artapr.code-blocage = "B"
                        then zone = zone + "H".
                        else zone = zone + string(artapr.code-blocage, "x").
           else if artapr.provenance = "7"
                   then if artapr.code-blocage = "" or 
                           artapr.code-blocage = "O"   
                           then zone = zone + "7".
                           else if artapr.code-blocage = "B"
                                   then zone = zone + "J".
                                   else zone = zone + 
                                        string(artapr.code-blocage, "x"). 
                   else if artapr.provenance = "M"
                           then if artapr.code-blocage = "" or 
                                   artapr.code-blocage = "O"   
                                   then zone = zone + "M".
                                   else if artapr.code-blocage = "B"
                                           then zone = zone + "N".
                                           else zone = zone + 
                                                string(artapr.code-blocage, "x"). 
                           else if artapr.provenance = "U"                     
                                   then if artapr.code-blocage = "" or 
                                           artapr.code-blocage = "O"   
                                           then zone = zone + "U".
                                           else if artapr.code-blocage = "B"
                                                   then zone = zone + "P".
                                                   else zone = zone + 
                                                        string(artapr.code-blocage, "x"). 
                                   else if artapr.code-blocage = ""
                                           then zone = zone + "O".
                                           else zone = zone + string(artapr.code-blocage, "x").
        else if artapr.code-blocage = "" or artapr.code-blocage = "O" 
           then zone = zone + "9".
           else if artapr.code-blocage = "B"
                   then zone = zone + "K".
                   else zone = zone + string(artapr.code-blocage, "x").    

    /* Ajout par CCO le 08/04/2003  */

    if substr(zone, 198, 1) = "C" and
       (artapr.provenance = "9" or artapr.code-blocage = "I" )
       then
       overlay(zone,198,1,"CHARACTER") = "D".   

case artapr.tenusto :                                   /* 199 - 001 */
    when "1" then zone = zone + "0".
    otherwise     zone = zone + "1".
end case.

/* DGR 071106 */
find tabges
where tabges.codsoc = ""
and   tabges.typtab = "ART"
and   tabges.prefix = "PROVENANCE"
and   tabges.codtab = artapr.provenance
no-lock no-error.
if available tabges then 
  zone = zone
       + string(tabges.libel2[1],"xxx").                /* 200 - 003 */
else
  zone = zone
       + fill(" ", 3).                                   /* 200 - 003 */

/* fin DGR 071106 */

zone = zone
       + string(artapr.type-client,"xxx")               /* 204 - 003 */
       + fill(" ", 4)                                   /* 206 - 003 */
       + string(codsoc-trait[nsoc], "xx")               /* 210 - 002 */
       + fill(" ", 3)                                   /* 212 - 003 */
       + "N"                                            /* 215 - 001 */
       + " "                                            /* 216 - 001 */
       + fill("0", 9)                                   /* 217 - 009 */
       + fill(" ", 8)                                   /* 226 - 008 */
       .

if artapr.datcre <> ?                                   /* 234 - 008 */
   then zone = zone + string(artapr.datcre, "99999999").
   else zone = zone + fill( " ", 8 ).

zone = zone
       + fill(" ", 5) .                                 /* 242 - 005 */

 if artapr.caract[ 1 ] <> "001" and
    artapr.caract[ 1 ] <> ""    
    then zone = zone +  string(artapr.caract[ 1 ], "x(8)"). 
    else if entry(1, artapr.toxicite) <> "" 
            then zone = zone +  string(entry(1, artapr.toxicite), "x(8)"). 
            else zone = zone + fill(" ", 8).           /* 247 - 008 */

 if artapr.code-cumul <> "0000" 
    then zone = zone +  string(artapr.code-cumul, "x(8)"). 
    else zone = zone + fill(" ", 8).                    /* 255 - 008 */
 zone = zone
       + fill(" ", 2)                                   /* 263 - 002 */
       + string(artapr.bareme, "x(30)")                 /* 265 - 030 */
       .

case artapr.negatif :                                   /* 295 - 001 */
    when "1" then zone = zone + "O".
    otherwise     zone = zone + "N".
end case.

zone = zone
       + " "                                            /* 296 - 001 */
       .

case prx-modif :                                       /* 297 - 001 */    
    when "0" then zone = zone + "O".
    otherwise     zone = zone + "N".
end case.

zone = zone
       + fill(" ", 18)                                  /* 298 - 018 */
       .

if artapr.datmaj <> ?                                   /* 316 - 008 */
   then zone = zone + string(artapr.datmaj, "99999999").
   else zone = zone + fill( " ", 8 ).

zone = zone
       + string(artapr.pds * 1000, "99999999")          /* 324 - 008 */
       + fill("0", 9)                                   /* 332 - 009 */
       + fill(" ", 2)                                   /* 341 - 002 */
       + string(artapr.volume * 1000, "9999999999")     /* 343 - 010 */
       + fill(" ", 136)                                 /* 353 - 136 */
       .

case artapr.code-blocage :                              /* 489 - 001 */
    when "F" then zone = zone + "S".
    otherwise     zone = zone + " ".
end case.

/* DGR 130106 : rajout test sur artapr.bloque */

if artapr.bloque <> 0 and artapr.bloque <> ? and
   substr(string(artapr.bloque),2,1) <> "9" 
then do:
  overlay(zone,198,1,"CHARACTER") = "F". 
end.
/* fin DGR 130106 */


zone = zone
       + " "                                            /* 490 - 001 */
       + fill(" ", 90)                                  /* 491 - 090 */
       .

case artapr.prx-modif :                                 /* 581 - 005 */
    when "0" then zone = zone + "09999".
    otherwise     zone = zone + string(artapr.fourche-px * 100, "99999").
end case .

zone = zone
       + fill(" ", 9)                                   /* 586 - 009 */
       + fill("0", 7)                                   /* 595 - 007 */
       + string(artapr.libart2[mem-langue], "x(30)")    /* 602 - 030 */
       + fill(" ", 8)                                   /* 632 - 008 */
       + string(artapr.usto, "x(10)")                   /* 640 - 010 */
       .

zone = zone
       + string(artbis.surcondit * 1000, "999999999")   /* 650 - 009 */
       + string(artbis.surcondit * 1000, "99999999")    /* 659 - 008 */
       .

if artbis.coef-cde <= 0                                 /* 667 - 008 */
   then zone = zone + string(artbis.coef-cde * -1000, "99999999").
   else zone = zone + string((1 / artbis.coef-cde) * 1000, "99999999").

if artbis.coef-fac <= 0                                 /* 675 - 007 */
   then zone = zone + string(artbis.coef-fac * -1000, "9999999").
   else zone = zone + string((1 / artbis.coef-fac) * 1000, "9999999").

if available b-artbis /* Article Achat */
then do:

    zone = zone
       + string(b-artbis.ucde, "x(10)")                 /* 682 - 010 */
       + "000001000"                                    /* 692 - 009 */ 
       + "00001000"                                     /* 701 - 008 */
       + "00001000"                                     /* 709 - 008 */
       .
    if b-artbis.coef-fac <= 0                           /* 717 - 007 */
       then zone = zone + string(b-artbis.coef-fac * -1000, "9999999").
       else zone = zone + string((1 / b-artbis.coef-fac) * 1000, "9999999").

end.

else do:
       zone = zone
       + fill(" ",10)                                   /* 682 - 010 */
       + "000001000"                                    /* 692 - 009 */
       + "00001000"                                     /* 701 - 008 */
       + "00001000"                                     /* 709 - 008 */
       + "0001000"                                      /* 717 - 007 */
       .
end.

zone = zone
       + string(artapr.usto, "x(10)")                   /* 724 - 010 */
       .

if artapr.volume <> 0                                   /* 734 - 010 */
   then zone = zone + "K/L" + fill( " ", 7 ) .
   else zone = zone + fill(" ", 10) .                            

zone = zone                                    
       +  string(artapr.volume * 1000, "9999999")      /* 744 - 007 */
       + "G".                                          /* 751 - 001 */

/* DGR 11/03/02: cas des cadeaux cartes des saisons de type "operations" */
/* rajout le 15/03/02 : 0 en position 315 */
if artapr.famart = "010" and 
   artapr.soufam = "075" then do:
  find first {tarcli.i} use-index primaire
  where {tarcli.i}.motcle     = "VTE"
  and   {tarcli.i}.codsoc     = codsoc-trait[nsoc]
  and   {tarcli.i}.typaux     = ""
  and   {tarcli.i}.codaux     = ""
  and   {tarcli.i}.articl     = artapr.articl
  and   {tarcli.i}.code-tarif = "OPE"
  no-lock no-error.
  if available {tarcli.i} then do:
    overlay(zone,198,1,"CHARACTER") = "X".   
    overlay(zone,217,9) = string({tarcli.i}.valeur * 100 , "999999999").   
    overlay(zone,307,8,"CHARACTER") = string({tarcli.i}.date-fin , "99999999").   
    overlay(zone,315,1,"CHARACTER") = "0".   
    overlay(zone,316,8,"CHARACTER") = string({tarcli.i}.date-debut, "99999999").   
  end.
end.
/* fin modif DGR 11/03/02 */