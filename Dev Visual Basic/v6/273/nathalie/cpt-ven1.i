/*==========================================================================*/
/*                           C P T - V E N 1 . I                            */
/*                       Comptabilisation des Ventes                        */
/*==========================================================================*/

/*-----------------------------------------------------------------------*/
/*         Zones utilisees pour le module edition des ruptures           */
/*                                                                       */
/*                   Rupture-a          : Niveau de rupture en cours     */
/*                   rupture-etat = "t" : Edition du titre de la rupture */
/*                   Rupture-etat = "r" : Edition de la rupture          */
/*-----------------------------------------------------------------------*/

def buffer ldoccpt for doccpt.

/*    Entete d'un Document    */

DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 1 :

    /*     Incrementation et Liberation du Compteur de Documents    */

    assign devcpt    = cpt-devcpt
           tx-devpie = 1
           tx-devdoc = 1
           devpie    = devcpt
           devdoc    = devpie
           mem-dat   = today
           codetb    = codetb-etb
           piece     = ""
           datpie    = today
           journal   = journal-vte
           typie     = typie-fac
           .

    assign execpt   = substring( zon-io ,  5 , 4 )
           percpt   = substring( zon-io ,  9 , 3 )
           joucpt   = substring( zon-io , 12 , 2 ) .

    REPEAT :
        FIND TABLES where tables.codsoc = codsoc-soc
                    and   tables.etabli = "   "
                    and   tables.typtab = "SOC"
                    and   tables.prefix = "N-DOCUMENT"
                    and   tables.codtab = ""
                    exclusive-lock  no-error .

        tables.nombre[ 1 ] = tables.nombre[ 1 ] + 1 .
        docume = string( tables.nombre[ 1 ] , "zzzzzzz9" ) .

        FIND DOCCPT where doccpt.codsoc = codsoc-soc
                    and   doccpt.codetb = codetb-etb
                    and   doccpt.docume = docume
                    no-lock  no-wait  no-error .
        if available doccpt then next .

        RELEASE TABLES .

        CREATE DOCCPT .
        do-recid = recid( doccpt ) .
        assign doccpt.codsoc = codsoc-soc
               doccpt.codetb = codetb-etb
               doccpt.docume = docume
               doccpt.execpt = execpt
               doccpt.percpt = percpt
               doccpt.joucpt = joucpt
               doccpt.datsai = today
               doccpt.journal = journal
               doccpt.operat  = operat
               doccpt.piece   = piece
               doccpt.datpie  = datpie
               doccpt.typie   = typie
               doccpt.devcpt  = devcpt
               doccpt.devpie  = devpie
               doccpt.tx-devpie = 1
               doccpt.bloc-ope  = ""
               doccpt.bloc-date = ?
               .


                /* chrono par journal */

                doccpt.codetb-chro = doccpt.codetb.
                find last ldoccpt use-index chrono-jnal
                    where ldoccpt.codsoc  = doccpt.codsoc   and
                          ldoccpt.codetb-chro
                                          = doccpt.codetb-chro and
                          ldoccpt.execpt  = doccpt.execpt   and
                          ldoccpt.journal = doccpt.journal  and
                          ldoccpt.percpt  = doccpt.percpt   and
                          ldoccpt.docume <> doccpt.docume   and
                          ldoccpt.chrono-jnal <> 0
                exclusive-lock no-error.

                if not available ldoccpt
                then doccpt.chrono-jnal = 1.
                else doccpt.chrono-jnal = ldoccpt.chrono-jnal + 1.
        leave .
    END .

    leave .

END .

/*    Entete d'une Ligne d'ecriture    */

DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 2 :

    assign mont-cpt = 0
           mont-dev = 0
           sav-zon-io = zon-io .

    leave .

END .

/*    Detail d'une Ligne de INTFAC    */

DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 3 :

    int-recid = integer( substring( zon-io , 121, 10 ) ) .
    FIND INTFAC where recid( intfac ) = int-recid exclusive-lock  no-error .
    if not available intfac then leave .

    intfac.docume = docume .

    assign mont-cpt = mont-cpt + intfac.mt-cpt
           mont-dev = mont-dev + intfac.mt-dev .

    leave .

END .

/*    Total d'une Ligne d'ecriture    */

DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 2 :

   assign libel-mv    = ""
          piece       = ""
          chrono      = ""
          cod-dom     = ""
          libel-mv    = ""
          adres       = ""
          ana-segment = ""
          echeance    = ?
          banque      = ""
          reg-dom     = ""
          dom-ech     = ""
          tire        = ""
          typaux      = substring( sav-zon-io ,  97 ,  3 )
          auxili      = substring( sav-zon-io , 100 , 10 )
          .

    if substring( sav-zon-io , 97 , 13 ) <> "" then
    do :
        assign reg-dom  = substring( sav-zon-io , 130 , 3 )
               piece    = trim( substring( sav-zon-io , 21 , 15 ) )
               codgen   = trim( substring( sav-zon-io , 36 , 10 ) )
               dom-ech  = substring( sav-zon-io , 133 , 4 )
               echeance = date( int( substring( sav-zon-io , 141 , 2 ) ) ,
                                int( substring( sav-zon-io , 143 , 2 ) ) ,
                                int( substring( sav-zon-io , 137 , 4 ) ) )
               .

        FIND AUXILI where auxili.codsoc = "01"   /*  ++++  */
                    and   auxili.typaux = typaux
                    and   auxili.codaux = auxili
                    no-lock  no-error .

        do ii = 1 to 5 while available auxili :
           adres[ ii ] = auxili.adres[ ii ] .
        end .
    end .

    if substring( sav-zon-io , 46 , 1 ) = "2" then
       assign mont-cpt = - mont-cpt
              mont-dev = - mont-dev .
    if mont-cpt < 0 then
         assign alpha-dt  = ""
                alpha-ct  = string( - mont-cpt , "ZZZZZZZZZZZ9.99-" )
                alpha-dev = string( - mont-dev , "ZZZZZZZZZZZ9.99-" ) .
    else assign alpha-dt  = string(   mont-cpt , "ZZZZZZZZZZZ9.99-" )
                alpha-ct  = ""
                alpha-dev = string(   mont-dev , "ZZZZZZZZZZZ9.99-" ) .

    segs-ana = trim( substring( sav-zon-io , 47 , 50 ) ) .
    if segs-ana <> "" then
    do :
        jj = 1 .
        do ii = 1 to nbr-seg :
           ana-segment[ ii ] = substring( segs-ana, jj, lgr-segment[ ii ] ) .
           jj = jj + lgr-segment[ ii ] .
        end .
    end .

    if mont-cpt <> 0 then
    do :
        assign chrono     = string( integer( chrono ) + 1 , "99999" )
               alpha-ini  = alpha-dev
               alpha-tini = alpha-taxe
               devini     = devpie
               tx-devini  = tx-devpie
               .
        { run.i  "s-majbas.p" }
    end .

    leave .

END .

/*    Fin d'un Document    */

DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 1 :

   leave .

END .