/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/ext-cfi0.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 16760 20-03-08!Evo!cch            !Gestion cartes Gamm Vert en soci�t� ASEC ( 08 )                       !
!V52 13592 02-11-05!Evo!cch            !Ajout g�n�ration d'un fichier contenant le nbre de courriers par magas!
!V52 13034 18-07-05!Evo!cch            !Ajout trt soci�t� 08 + ajout GENCOD op�ration sur chaque enreg.       !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - C F I 0 . I                            */
/* Extraction des Fiches Cartes Fidelite Magasin ayant droit a remise       */
/*--------------------------------------------------------------------------*/
/* Variables et Frames                                        CC 23/10/2002 */
/*==========================================================================*/

def var ind        as int  no-undo .
def var tour-soc   as int  no-undo .
def var date-tarif as date no-undo .

def {1} shared var libelle         as char extent 40                no-undo .
def {1} shared var ean13-operation as char extent 2  format "x(13)" no-undo .