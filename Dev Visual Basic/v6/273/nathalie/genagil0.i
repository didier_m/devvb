/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/genagil0.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 17758 18-09-09!New!cch            !Génération Fichier Cpta mouvements trésorerire AGIL                   !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           G E N A G I L 0 . I                            */
/* Generation fichier compta. mouvements tresorerie AGIL                    */
/*--------------------------------------------------------------------------*/
/* Variables et Frames                                                      */
/*==========================================================================*/

def var ind     as int               no-undo .
def var o-param as char              no-undo .
def var lib-maj as char format "xxx" no-undo .

def {1} shared var libelle     as char                     extent 50 no-undo .
def {1} shared var societes    as char format "x(49)"                no-undo .
def {1} shared var soc-exclues as char                               no-undo .
def {1} shared var magasins    as char format "x(39)"                no-undo .
def {1} shared var mag-exclus  as char                               no-undo .
def {1} shared var date-maxi   as date format "99/99/9999"           no-undo .
def {1} shared var maj         as char format "x"                    no-undo .
def {1} shared var repert-dep  as char format "x(50)"                no-undo .
def {1} shared var repert-trav as char                               no-undo .
def {1} shared var repert-cpta as char                               no-undo .

def {1} shared frame fr-saisie .

form libelle[ 01 ] format "x(24)" societes    skip
     libelle[ 02 ] format "x(24)" magasins    skip
     libelle[ 03 ] format "x(24)" date-maxi   skip
     libelle[ 04 ] format "x(24)" maj lib-maj skip
     libelle[ 05 ] format "x(24)" repert-dep  skip
     with frame fr-saisie
     row 3 centered overlay no-label { v6frame.i } .