/*==========================================================================*/
/*                          R F C - E X T A . I                             */
/*          Gestion des RFC CLIENTS - EXTRACTIONS DES RFC DE BASTAT         */
/*          Module de test des conditions requises pour Remises             */
/*          MOT-CLE = "RAD" = Remises Adherents                             */
/*==========================================================================*/


/* mettre la Variable logical TOP-VALIDE a NO                   */
/* pour que l'enreg ne soit pas traite                          */

/* Zones de test : - Famille    = artapr.famart                         */
/*                 - Categorie  = artapr.soufam                         */
/*                 - Articl     = bastat.articl                         */
/*                 - Bareme     = no-bar - affecte a la ligne article   */
/*                 - Bareme Client = auxapr.bareme-a-1                  */
/*----------------------------------------------------------------------*/

def var b           as int                                   no-undo .
def var list-bar    as char                                  no-undo .
def var list-art    as char                                  no-undo .
def var list-bar1   as char                                  no-undo .
def var list-bar2   as char                                  no-undo .


assign list-bar  = "E0,E1,E2,E3,E4,E5,F1,F2,F3,F4,F5,F9,G1,G2,G3,G5,
H1,K5,K6,K7,L1".

assign list-art  = "V50178,E50179,V10179,C56179,V50179" .

assign list-bar1 = "W0,W1,W2,W3,W4,W5,W6,W7,W8,W9" .

assign list-bar2 = "D5,D6,D7" .

VALID-A :
REPEAT  :

    if artapr.famart = "013"
    then do : /* 013 */
        top-valide = no  .
        leave valid-a .
    end . /* 13 */

    if artapr.famart = "012"
       and ( artapr.soufam = "002" or
             artapr.soufam = "003" or
             artapr.soufam = "004" )
    then do : /* 012 */
         top-valide = no .
         leave valid-a .
    end . /* 012 */

    if lookup( trim(bastat.articl), list-art ) <> 0
    then do b = 1 to 10  : /* articl */

       if artapr.famart = "032" and artapr.soufam = "003"
       then do b = 1 to 10  :
          if lookup( auxapr.bareme-a-1[ b ], list-bar2 ) <> 0
          then do :
             top-valide = no .
             leave valid-a .
          end .
       end .


       if artapr.famart = "032" and artapr.soufam = "004"
       then do b = 1 to 10 :
          if lookup( auxapr.bareme-a-1[ b ], list-bar ) <> 0
          then do :
             top-valide = no .
             leave valid-a .
          end .
       end .


       if artapr.famart = "032" and artapr.soufam = "006" and
         (artapr.sssfam = "011" or
          artapr.sssfam = "012" or
          artapr.sssfam = "031" or
          artapr.sssfam = "032")

       then do b = 1 to 10 :
          if lookup( auxapr.bareme-a-1[ b ], list-bar1 ) <> 0
          then do :
             top-valide = no .
             leave valid-a .
          end .
       end .
    end. /* articl */

 Leave .

 END . /* repeat transaction */