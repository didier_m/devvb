/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/rfc-exti.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  9071 14-05-04!Evo!erk            !remise a niveau des standards avec variante 273 : zones libel2 ( 2,4,5!
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                          R F C - E X T I . I                             */
/*          Gestion des RFC CLIENTS - EXTRACTIONS DES RFC DE BASTAT         */
/*          Module de test des conditions requises pour Remises             */
/*          MOT-CLE = "RIS" = Ristournes Adherents                          */
/*==========================================================================*/


/* mettre la Variable logical TOP-VALIDE a NO                   */
/* pour que l'enreg ne soit pas traite                          */
/* force le taux-rem  pour les enreg. valide                    */

/* Zones de test : - Famille    = artapr.famart                         */
/*                 - Categorie  = artapr.soufam                         */
/*                 - Articl     = bastat.articl                         */
/*                 - Bareme     = no-bar - affecte a la ligne article   */
/*                 - Bareme Client       = auxapr.bareme-a-1            */
/*                 - Code Coef Ris.Art.  = artapr.rist-adherent[3]      */
/*                 - Code Coef Ris.Fam.  = tabges.libel1[4]             */
/*                 - Code Coef Ris.SFam  = tabges.libel2[4]             */
/*----------------------------------------------------------------------*/

def buffer b-tabges   for  tabges .
def var taux-coef     as dec                                   no-undo .
def var cod-coef      as char                                  no-undo .


VALID-C :
REPEAT  :
        assign cod-coef  = " "
               taux-coef = 0.

        /* 1- Test sur le code coef de l'Article                  */
        if artapr.rist-adherent[3] <> " "
        then cod-coef = artapr.rist-adherent[3] .

        /* 2- si Code-Coef = " " Test sur le code coef. de la Sous-Famille */
        if cod-coef = " "
        then do :
            FIND TABGES where tabges.codsoc = ""
                        and   tabges.etabli = ""
                        and   tabges.typtab = "ART"
                        and   tabges.prefix = "SOUFAM" + artapr.famart
                        and   tabges.codtab = artapr.soufam
                        no-lock  no-error .
            if available tabges
            then do :
                if substring(tabges.libel3[4], 1, 2) <> "  "   /* tabges.libel2[4], 1, 2 */
                then cod-coef = substring(tabges.libel3[4], 1, 2) .  /* tabges.libel2[4], 1, 2 */
            end .
        end .

        /* 3- si Code-Coef = " " Test sur le code coef. de la Famille */
        if cod-coef = " "
        then do :
            FIND TABGES where tabges.codsoc = ""
                        and   tabges.etabli = ""
                        and   tabges.typtab = "ART"
                        and   tabges.prefix = "FAMART"
                        and   tabges.codtab = artapr.famart
                        no-lock  no-error .
            if available tabges
            then do :
                if substring(tabges.libel2[4], 1, 2) <> "  "
                then cod-coef = substring(tabges.libel2[4], 1, 2) .
            end .
        end .

        /* si Code coef <> " " alors recherche de la valeur du coef */
        if cod-coef <> " "
        then do :

           FIND B-TABGES where b-tabges.codsoc = " "
                         and   b-tabges.etabli = " "
                         and   b-tabges.typtab = "TAR"
                         and   b-tabges.prefix = "RIST-ADH"
                         and   b-tabges.codtab = cod-coef
                         no-lock  no-error .
           if available b-tabges then  taux-coef = b-tabges.nombre[1]  .
        end .
        if cod-coef <> " " then top-valide = yes .

 Leave .

 END . /* repeat transaction */