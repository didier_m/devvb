/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/cafam.i                                                                        !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End                                                                                                          
**************************************************************************************************************/

DEF {1} shared VAR l-soc AS CHAR format "x(25)"        no-undo.
def {1} shared var wtypfic as char format "x"          no-undo.
def {1} shared var wanneedeb-1 as char format "x(4)"   no-undo.
def {1} shared var wanneefin-1 as char format "x(4)"   no-undo.
def {1} shared var wmoisdeb-1 as char format "xx"      no-undo.
def {1} shared var wmoisfin-1 as char format "xx"      no-undo.

def {1} shared var wanneedeb as char format "x(4)"     no-undo.
def {1} shared var wanneefin as char format "x(4)"     no-undo.
def {1} shared var wmoisdeb  as char format "xx"       no-undo.
def {1} shared var wmoisfin  as char format "xx"       no-undo.

def {1} shared var topcar    as char format "xxx"       no-undo.


def stream edition.
def stream edition2.

DEFINE STREAM maquette.