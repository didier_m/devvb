/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/273/273/gesnom-0.p                                                                      !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!V61 38343 24-05-13!Evo!cch            !Ajout gestion Interfaces et Saisies NATHALIE - NOMEREF                !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !                                                !
!bat-ini.i             !new                                  !                                                !
!aide.i                !new                                  !                                                !
!compo.f               !new                                  !                                                !
!gesnom-0.i            !new                                  !                                                !
!v6frame.i             !                                     !                                                !
!aide-lib.i            !                                     !                                                !
!fr-cadre.i            !lib-prog                             !                                                !
!fncompo.i             !fic-trt fr-fic                       !                                                !
!fncompo.i             !validat fr-valid                     !                                                !
!bat-zon.i             !"lib-prog" "lib-prog" "c" "''" "libel!                                                !
!bat-maj.i             !gesnom-1.p                           !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*============================================================================*/
/*                           G E S N O M - 0 . P                              */
/* Gestion Echanges NATHALIE - NOMEREF                                        */
/*============================================================================*/
/* Module Saisie                                                              */
/*============================================================================*/

{ connect.i      }
{ bat-ini.i  new }
{ aide.i     new }
{ compo.f    new }
{ gesnom-0.i new }

def var validat as log format "O/N" no-undo .

form skip( 1 ) " " libelle[ 2 ] format "x(19)" fic-trt " " skip( 1 )
     with frame fr-fic row 3 centered no-label overlay { v6frame.i } .

form skip( 1 ) " " libelle[ 3 ] format "x(25)" validat " " skip( 1 )
     with frame fr-valid row 10 centered no-label overlay { v6frame.i } .

/* Chargement Parametrage General NOMEREF */
assign  typtab = "PRM"  prefix = "NOMEREF"  codtab = "PARAMETRES" .
run ch-paray.p( output o-param ) .
if o-param = "" then return . /* Pas de gestion NOMEREF */
nomeref-soc = entry( 33 , o-param , "|" ) . /* Societe Articles NOMEREF */

/* Aide et Texte */
aide-fichier = "gesnom-0.aid" . run aide-lec.p .
do ind = 1 to 50 :
  aide-m = "libel" + string( ind , "99" ) .
  { aide-lib.i }
  libelle[ ind ] = aide-lib .
end .

/* Affichage Cadre */
if zone-charg[ 2 ] = "GROUPE" then libsoc-soc = libelle[ 11 ] + nomeref-soc .

{ fr-cadre.i lib-prog }

case zone-charg[ 1 ] :

    /* Integration Articles NOMEREF */
    when "INT-ART" then do :

        display               libelle[ 2 ] with frame fr-fic .
        color display message libelle[ 2 ] with frame fr-fic .

        display               libelle[ 3 ] with frame fr-valid .
        color display message libelle[ 3 ] with frame fr-valid .

        fic-trt = entry( 2 , o-param , "|" ) + entry( 31 , o-param , "|" ) . /* Repertoire + Nom Fichier Articles */

        SAISIE-GENERALE :
        REPEAT :

            /* Saisie Repertoire + Nom Fichier Articles */
            REPEAT :

                { fncompo.i fic-trt fr-fic }

                 if keyfunction( lastkey ) = "END-ERROR" then leave .

                 if keyfunction( lastkey ) <> "RETURN" and
                    keyfunction( lastkey ) <> "CURSOR-DOWN"
                 then do :
                     bell . bell .
                     undo, retry .
                 end .

                 if search( fic-trt ) = ?
                 then do :
                     bell . bell .
                     message fic-trt libelle[ 31 ] .
                     readkey pause 2 .
                     undo, retry .
                 end .

                 leave .

            END .

            /* Validation Traitement */
            REPEAT while keyfunction( lastkey ) <> "END-ERROR" :

                { fncompo.i validat fr-valid }

                if keyfunction( lastkey ) <> "END-ERROR" and
                   keyfunction( lastkey ) <> "RETURN"    and
                   keyfunction( lastkey ) <> "CURSOR-UP"
                then do :
                    bell . bell .
                    undo, retry .
                end .

                if keyfunction( lastkey ) = "CURSOR-UP" then next SAISIE-GENERALE .

                leave .

            END .

            leave .

        END . /* SAISIE-GENERALE */

        if keyfunction( lastkey ) <> "END-ERROR" and validat
        then do :

            batch-tenu = yes . /* Indique gestion en BATCH */

           { bat-zon.i  "lib-prog"      "lib-prog"       "c"  "''"  "libelle[1]" }
           { bat-zon.i  "fic-trt"       "fic-trt"        "c"  "''"  "libelle[2]" }
           { bat-zon.i  "zone-charg-1"  "zone-charg[1]"  "c"  "''"  "''"         }

           { bat-maj.i  gesnom-1.p }

        end .

        hide frame fr-fic   no-pause .
        hide frame fr-valid no-pause .

    end . /* when "INT-ART" then do : */

    when "FICHE-ART" then run gesnom-2.p . /* Fiche Articles liens NATHALIE - NOMEREF */

end case . /* case zone-charg[ 1 ] : */

hide frame fr-titre no-pause .
hide frame fr-cadre no-pause .

