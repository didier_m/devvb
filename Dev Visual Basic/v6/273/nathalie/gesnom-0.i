/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/273/273/gesnom-0.i                                                                      !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!V61 38343 24-05-13!Evo!cch            !Ajout gestion Interfaces et Saisies NATHALIE - NOMEREF                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*============================================================================*/
/*                           G E S N O M - 0 . I                              */
/* Gestion Echanges NATHALIE - NOMEREF                                        */
/*============================================================================*/
/* Variables Communes                                                         */
/*============================================================================*/

def var o-param        as char no-undo .
def var l-separat-zone as char no-undo .
def var enreg          as char no-undo .
def var zone           as char no-undo .
def var ind            as int  no-undo .

def {1} shared var libelle        as char extent 50                no-undo .
def {1} shared var type-rech      as char                          no-undo .
def {1} shared var fic-trt        as char           format "x(50)" no-undo .
def {1} shared var fic-nom        as char                          no-undo .
def {1} shared var theme-trt      as char                          no-undo .
def {1} shared var nomeref-soc    as char                          no-undo .
def {1} shared var rep-travail    as char                          no-undo .
def {1} shared var rep-depart     as char                          no-undo .
def {1} shared var rep-dep-traite as char                          no-undo .
def {1} shared var rep-arr-traite as char                          no-undo .
def {1} shared var rep-nomeref    as char                          no-undo .
def {1} shared var serv-nomeref   as char                          no-undo .
def {1} shared var login-nomeref  as char                          no-undo .
def {1} shared var pass-nomeref   as char                          no-undo .
def {1} shared var l-param-cde    as char                          no-undo .
def {1} shared var separat-zone   as char extent 3                 no-undo .
def {1} shared var l-onu-exclus   as char                          no-undo .
def {1} shared var condit         as char                          no-undo .
def {1} shared var l-modliv       as char                          no-undo .
def {1} shared var rowid-ent      as rowid                         no-undo .

def stream s-fic .

