/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/g-trf0.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!V61 40729 04-09-13!Evo!jcc            !Acces préalable au tarif avec le code tiers                           !
!V52 16903 05-06-08!Evo!cch            !Ajout gestion des tarifs promo. par groupe de magasins multi sociétés !
!V52 13668 16-11-05!Evo!cch            !Fin développement dossier Tarifs PROMO Plateformes                    !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*------------------------------------------------------------------*/
/* Gestion Tarif Client Article                         - BACCHUS - */
/*                                                                  */
/* g-trf0.i                                                         */
/*------------------------------------------------------------------*/

/*--------------------------*/
/* Definition des Variables */
/*--------------------------*/

def {1} shared buffer b-articl for artapr .
def {1} shared buffer b-tarcli for {tarcli.i} .
def {1} shared buffer c-tarcli for {tarcli.i} .

def {1} shared var soc-auxili      like auxili.codsoc no-undo .               
def {1} shared var codsoc-bl       as char format "xx" no-undo . 
def {1} shared var etabli-bl       as char format "xxx" no-undo.
def {1} shared var type-trt        as int  no-undo.                           
def {1} shared var motcle-trt      as char format "xxx" no-undo.              
def {1} shared var titre           as char format "x(33)" no-undo.            
def {1} shared var libelle         as char extent 69 .                        
def {1} shared var articl          as char format "x(10)".                    
def {1} shared var categorie-tarif as char format "x(7)".
def {1} shared var magasin         as char format "xxx".                           
def {1} shared var groupe-mag      as char format "xxx".                           
def {1} shared var type-promo      as char no-undo .                  
def {1} shared var type-tar        as char format "x(3)" no-undo.                  
def {1} shared var date-debut      as date format "99/99/9999".                    
def {1} shared var libtar          as char format "x(15)" extent 4 no-undo.        
def {1} shared var aa              as int   no-undo.                               
def {1} shared var bb              as int   no-undo.                               
def {1} shared var tarcli-recid    as recid no-undo.                               
def {1} shared var tarcli-chrono   as int  format   ">9" .                         
def {1} shared var colonne         as char extent 3 no-undo .                      
def {1} shared var aux-recid       as recid.                                       
def {1} shared var tar-recid       as recid.                                       
def {1} shared var preced-typtar   like {tarcli.i}.typtar initial "1" .            
def {1} shared var t-pf1           as char format "xxxx"   initial "pf1," no-undo .
def {1} shared var prefix-1        as char format "x(3)"  no-undo .                
def {1} shared var prefix-2        as char format "x(10)" no-undo .                
def {1} shared var lib-erreur      as char format "x(80)" no-undo .                
def {1} shared var pas             as int no-undo .                                

def            var compo           as char format "x(10)" .
def            var lib-compo       as char format "x(25)" .
def            var lib-typtar      as char format "x(12)" .
def            var l-soc           as char                  no-undo .

/*-------------------------*/
/* Definition des Frames   */
/*-------------------------*/

Def {1} shared frame rem-cli.
Def {1} shared frame rem-art.
Def {1} shared frame rem-cat.
Def {1} shared frame rem-mag.
Def {1} shared frame rem-grpmag.
Def {1} shared frame date.
Def {1} shared frame type-tar.
Def {1} shared frame cadre-ligne.
Def {1} shared frame titre-ligne.
Def {1} shared frame tarcli-ligne.

Form    libelle[10] format "x(11)"
        typaux
        auxili
        auxili.adres[1] format "x(28)"
        with frame REM-CLI row 2 col 3 no-label overlay no-box {v6frame.i} .

Form    libelle[11] format "x(11)"
        articl
        b-articl.libart1[ mem-langue ] format "x(32)"
        with frame REM-ART row 3 col 3 no-label overlay no-box {v6frame.i} .

Form    libelle[15] format "x(15)"
        categorie-tarif
        tabges.libel1[ mem-langue ] format "x(30)"
        with frame REM-CAT row 2 col 2 no-label overlay {v6frame.i} .

Form    libelle[16] format "x(11)"
        magasin magasi.libel1[ mem-langue ] format "x(32)"
        with frame REM-MAG row 4 col 3 no-label overlay no-box {v6frame.i} .

Form    libelle[09] format "x(11)"
        groupe-mag tabges.libel1[ mem-langue ] format "x(30)"
        with frame REM-GRPMAG row 5 col 3 no-label overlay no-box {v6frame.i} .

Form    date-debut
        with frame DATE row 3 col 68 no-label overlay
        with title libelle[14] {v6frame.i} .

Form    type-tar
        with frame TYPE-TAR row 3 col 63 no-label overlay
        with title libelle[13] {v6frame.i} .

Form    colonne[1] format "x(32)"
        colonne[2] format "x(33)"
        colonne[3] format "x(9)"
        with frame TITRE-LIGNE row 6 col 3 overlay no-label no-box {v6frame.i} .

Form    " "
        with frame CADRE-LIGNE row 7 col 2 12 down width 78 overlay no-label
        {v6frame.i} .

Form    tarcli-chrono
        {tarcli.i}.code-tarif
        lib-compo
        {tarcli.i}.tarif-ref
        {tarcli.i}.typtar
        lib-typtar
        {tarcli.i}.date-fin
        {tarcli.i}.valeur format "zzzz9.999" skip
        with frame TARCLI-LIGNE
        row 8 col 3 6 down overlay no-label no-box {v6frame.i} .

