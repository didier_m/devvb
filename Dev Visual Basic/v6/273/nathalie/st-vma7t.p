/*==========================================================================*/
/*                          S T - V M A 7 T . P                             */
/*     Edition Statistique des Ventes des Magasins par Activite             */
/*     Gestion de l'edition des totaux                                      */
/*==========================================================================*/

{ connect.i  }
{ maquette.i }

{ st-vma70.i }

def var i           as int                 no-undo .
def var j           as int                 no-undo .
def var k           as int                 no-undo .
def var l           as int                 no-undo .

nb-run = nb-run + 1 .

ma-edipied = "O" .
if ind-tot = 0 then
do :
    { maq-edi.i "03" }
end .

DO I = 1 TO 4 :

    assign j = i * 10
           k = ind-tot + j
           l = k + 50 .

/*  1 = Nb Bons CPT */
    if mat-edi[ j + 1 ] = yes then
       assign valeur[ j + 1 ] = string( tot-bon[ k + 1 ] , masque-tot[ j + 1 ])
              tot-bon[ l + 1 ] = tot-bon[ l + 1 ] + tot-bon[ k + 1 ] .

/*  2 = Nb Bons TERME */
    if mat-edi[ j + 2 ] = yes then
        assign valeur[ j + 2 ] = string( tot-bon[ k + 2 ] , masque-tot[ j + 2 ])
               tot-bon[ l + 2 ] = tot-bon[ l + 2 ] + tot-bon[ k + 2 ] .

/*  3 = Panier Moyen */
    if mat-edi[ j + 3 ] = yes then
       assign valeur[ j + 3 ] = string( tot-ca[ k + 3 ] , masque-tot[ j + 3 ])
              tot-ca[ l + 3 ] = tot-ca[ l + 3 ] + tot-ca[ k + 3 ] .

/* 4 = Nb de bons Total */
    if mat-edi[ j + 4 ] = yes then
       assign valeur[ j + 4 ] = string( tot-bon[ k + 4 ] , masque-tot[ j + 4 ])
              tot-bon[ l + 4 ] = tot-bon[ l + 4 ] + tot-bon[ k + 4 ] .

    assign tot-bon[ k + 1 ] = 0
           tot-bon[ k + 2 ] = 0
           tot-bon[ k + 4 ] = 0
           tot-ca[ k + 3 ] = 0 .

END .  /*   DO   */

{ maq-maj.i  "07" 1  lib-tot }

DO I = 11 TO 49 :
   if valeur[ i ] <> "" then
   do :
       { maq-maj.i  "07" i  valeur[i] }
       valeur[ i ] = "" .
   end .
END .

{ maq-edi.i  7 }

{ maq-edi.i  4 }

ma-edipied = "" .
ma-nblig   = 0  .