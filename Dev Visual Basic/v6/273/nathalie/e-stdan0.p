/*==========================================================================*/
/*                          E-STDAN0.p .P                                   */
/*       Etat de stocks des produits dangereux                              */
/*==========================================================================*/
 
{connect.i        }
{chx-opt.f        }
{acc-tstd.i "standard" "new" }
{sel.i      "new" }
{aide.i     "new" }
{maquette.i "new" }
{bat-ini.i  "new" }
 
{e-stdan0.i "new" } 
{e-stdan0.f "new" } 
 
regs-app = "nathalie" .
{regs-cha.i}
regs-app = "elodie".
{regs-cha.i}
 
def var a               as int     format "999"                  no-undo .
def var i               as int                                   no-undo .
def var sequence        as char    format "x(5)"                 no-undo .
def var libelle         as char    format "x(30)" extent 10      no-undo .
def var choix-lib       as char                                  no-undo .
def var vide            as logical format "1/0" initial 1        no-undo .
 
aide-fichier = "e-stdan0.ais" . aide-par = "<e-stdan0>" . run aide-lec.p .
aide-fichier = "e-stdan0.aid" . aide-par = "<e-stdan0>" . run aide-lec.p .
 
do a = 1 to 100 :
    aide-m = "libel" + string(a,"999") .
    {aide-lib.i }
    mes-lib[a] = aide-lib.
end.
 
/* Initialisation des selectio & .chx */
assign  sel-applic   = "gescom"
        sel-filtre   = "*e-stdan0".                               /* .chx */
 
/* Prise Numero sequence par operateur */
run oper-seq.p ( "std" , output  sequence, output sel-sequence) .
 
assign periph  = sel-sequence .
 
{fr-cadre.i mes-lib[1] }
{ sel.f "new" }
 
 
/*=======================================================================*/
/* Debut du Traitement                                                   */
/*=======================================================================*/
 
GENERALE:
REPEAT :
 
    run e-stdanq.p .
    if keyfunction(lastkey) = "end-error" then leave generale.
 
    run sel.p .
    if keyfunction( lastkey ) = "end-error" then leave generale .
    
 
    ACCEPTATION :
    REPEAT :
        assign chx-def    = 1
               chx-milieu = 61
               chx-ombre = "O"
               choix-lib = trim( mes-lib[9]) .
 
        { chx-opt.i "3" "choix-lib" "15" "15" "17" " " "fr1" " " }
 
        if chx-trait = 1 then leave GENERALE .    /*   Validation    */
        if chx-trait = 2 then leave GENERALE .    /*   Refus         */
        if chx-trait = 3 then next  GENERALE .    /*   Modification  */
 
     END . /* Repeat acceptation */
 
END. /* generale */
 
if chx-trait = 1 and keyfunction( lastkey ) <> "end-error"
then do :
 
    /* Lecture maquette pour chargement escape */
    ma-maquet = maquette .
    run maq-lect.p .
    if ma-anom <> ""
    then do :
        message ma-anom mes-lib[ 10 ] . bell . bell . pause .
        hide all .
        return .
    end .
 
     batch-tenu = yes .
 
    { bat-zon.i  "sel-applic"    "sel-applic"    "c" "''" "''"}
    { bat-zon.i  "sel-filtre"    "sel-filtre"    "c" "''" "''"}
    { bat-zon.i  "sel-sequence"  "sel-sequence"  "c" "''" "''"}
    { bat-zon.i  "lib-prog"      "lib-prog"      "c" "''" "mes-lib[01]" }
    { bat-zon.i  "codsoc"        "codsoc-soc"    "c" "''" "mes-lib[22]" }
    { bat-zon.i  "mem-langue"    "mem-langue"    "c" "''" "''"}
    { bat-zon.i  "ma-esc"        "ma-esc"        "c" "''" "mes-lib[23]" }
    { bat-zon.i  "opt-sto"       "opt-sto"       "c" "''" "mes-lib[02]" }
    { bat-zon.i  "exercice"      "exercice"      "c" "''" "mes-lib[03]" }
    { bat-zon.i  "periode"       "periode"       "c" "''" "mes-lib[04]" }
    { bat-zon.i  "maquette"      "maquette"      "c" "''" "mes-lib[05]" }
    { bat-zon.i  "periph"        "periph"        "c" "''" "mes-lib[06]" }
 

    { bat-maj.i  "e-stdan1.p" } 
 
end .
 
else run sel-del.p( sel-applic, sel-filtre, sel-sequence, yes ) .

/* Effacement ecrans et suppression des selections */
/*-------------------------------------------------*/
{ sel-hide.i } 
hide frame fr-entete no-pause .
hide frame fr-titre    no-pause .
hide frame fr-cadre    no-pause .
