/*==========================================================================*/
/*                          S T - R E P 1 2 . P                             */
/* Statistique Vente Representant                                           */
/* Etat des Qtes & CA    / Famille / Zone                                   */
/* Lecture  du fichier de tri .                                             */
/*==========================================================================*/
 
{ connect.i  }
{ select.i   }
{ maquette.i }
{ natabsoc.i   }
 
{ st-rep10.i }
 
/*--------------------------------------------*/
/* Societe de regroupement                    */
/*--------------------------------------------*/
regs-app = "nathalie" .
{ regs-cha.i }
 
/*-------------------------------------------------*/
/* Definitions des Variables                       */
/*-------------------------------------------------*/
 
def input parameter  fic-tri      as char format "x(12)" .
def var i            as int                       no-undo .
def var am-deb       as int                       no-undo .
def var am-fin       as int                       no-undo .
def var am-debx      as int                       no-undo .
def var am-sta       as int                       no-undo .
def var list-fam     as char                      no-undo .
 
/* Preparation des familles a traiter   */
list-fam = familles .
if familles = ""
then do :
    list-fam = "" .
    FOR EACH TABGES where tabges.codsoc = ""
                    and   tabges.etabli = ""
                    and   tabges.typtab = "ART"
                    and   tabges.prefix = "FAMART"
                    no-lock :
        if familles = ""  or  lookup( tabges.codtab , familles ) = 0
        then list-fam = list-fam + "," + tabges.codtab .
    end .
    substring( list-fam , 1 , 1 ) = "" .
end .
 
/* Preparation des zone de date pour comparaison */
assign  am-deb = int(string(year(mois-deb)) + string(month(mois-deb), "99"))
        am-fin = int(string(year(mois-fin)) + string(month(mois-fin), "99"))
        am-debx = int(string(year(dat-debx )) + string(month(dat-debx), "99"))
        .
 
/*==========================================================================*/
/*                           Debut du traitements                           */
/*==========================================================================*/
/* trier par : 1-  representant
               2-  famille
               3-  zone
               4-  recid
                -----------------------------------------------------------*/
/*---------------------------*/
/* Ouverture du fichier trie */
/*---------------------------*/
input from value( fic-tri ) .
 
/*----------------------------*/
/* Definition des Ruptures    */
/*----------------------------*/
{ rup-def.i new }
 
{ rup-ini.i "'03,03,03,10'" }
 
GENERAL :
REPEAT  :
        /* lecture du fichier trier */
        import zone .
 
        /* test si famille selectionnee */
        if lookup( substring( zone,  4, 3), list-fam ) = 0 then next .
 
        /* lecture fichier STACLI   */
        recid-stacli = int( substring( zone, 10, 10 ) ) .
        FIND STACLI  where recid( stacli ) = recid-stacli  no-lock no-error .
 
        if not available stacli  then next .
 
        /* Rupture-test = zone qui permet de tester les ruptures */
        rupture-test = substring( zone , 1 , 19 ) .
 
        /*----------------------------------------------------------*/
        /* Rup-edi.i  = Module gerant les ruptures                  */
        /* st-rep12.i = module contenant les editions et les cumuls */
        /*----------------------------------------------------------*/
       { rup-edi.i  st-rep12.i }
 
    END .  /* repeat */
 
 
/* Traitement de la derniere rupture total */
/* Total zone */
do while nb-recid <> 0 :
   assign ind-tot = 00
          nb-zone = nb-zone + 1 .
                                          /* imp entete que si ligne */
   if ent-rep  then run value( ma-entete ) .
                                         /* idem ci-dessus          */
   if ent-fam
   then do :
       { maq-maj.i "02" "01" "lib-tit1"  }
       { maq-maj.i "02" "02" "no-fam"    }
       { maq-maj.i "02" "03" "lib-fam"   }
 
       { maq-edi.i "02" }
   end .
   assign ent-rep = no
          ent-fam = no .
   run st-rep1t.p .
   leave .
end .
/* Total famille */
do while nb-zone <> 0 :
    assign lib-tot = " Total ....   "  + no-fam
           ind-tot = 10
           nb-fam = nb-fam + 1.
 
    run st-rep1t.p .
    leave .
end .
 
/* Total Representant */
do while nb-fam <> 0 :
    assign lib-tot  = " Total ....   "  + no-rep
           ind-tot  = 20
           nb-rep  = nb-rep + 1 .
 
    run st-rep1t.p .
    leave .
end .
 
/* Total general */
do while nb-rep <> 0 :
    assign   no-rep   = " "
             lib-rep  = " "
             lib-tit  = " "
             lib-tot  = " Total General ......... "
             ind-tot  = 30 .
 
    run st-rep1t.p .
    leave .
end .
 
/* Fermeture du fichier de travail */
input close .  
 
 
 