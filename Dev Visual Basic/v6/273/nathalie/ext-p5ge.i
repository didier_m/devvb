/*==========================================================================*/
/*                           E X T - P 5 G E  . I                           */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Gencod Articles PDP V5                          CC 24/04/1999 */
/*==========================================================================*/

                                                        /* Pos.  Long. */

zone = "GENART  "                                       /* 001 - 008 */
       + "C"                                            /* 009 - 001 */
       + string(codsoc-trait[nsoc], "xx")               /* 010 - 002 */
       + fill( " ", 6 )                                 /* 012 - 006 */
       + string(codbar.code-barre, "x(13)")             /* 018 - 013 */
       + string(codbar.articl,     "x(15)")             /* 031 - 015 */
       + fill( " ", 10 )                                /* 046 - 010 */
       .

if codbar.datcre <> ?                                   /* 056 - 008 */
   then zone = zone + string(codbar.datcre, "99999999").
   else zone = zone + fill( " ", 8 ).

if codbar.datmaj <> ?                                   /* 064 - 008 */
   then zone = zone + string(codbar.datmaj, "99999999").
   else zone = zone + fill( " ", 8 ).

zone = zone
       + fill( " ", 3 )                                 /* 072 - 003 */
       + fill( "0", 7 )                                 /* 075 - 007 */
       .