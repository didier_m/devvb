/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/src/e-trfe1q.p                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 17786 25-09-09!Evo!cch            !Ajout prise en compte des magasins type 'AGIL'                        !
!V52 16897 03-06-08!Evo!cch            !Evolutions diverses faites par CCO ( EUREA )                          !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !tabges                                          !
!e-trfe10.i            !                                     !magasi                                          !
!aide.i                !                                     !                                                !
!compo.f               !new                                  !                                                !
!compo.i               !                                     !                                                !
!fncompo.i             !code-tarif  fr-saisie  code-tarif    !                                                !
!z-cmp.i               !code-tarif                           !                                                !
!z-rec.i               !codsoc-soc                           !                                                !
!fncompo.i             !top-complet  fr-saisie  top-complet  !                                                !
!z-cmp.i               !top-complet                          !                                                !
!fncompo.i             !date-mini  fr-saisie  date-mini      !                                                !
!z-cmp.i               !date-mini                            !                                                !
!fncompo.i             !date-maxi  fr-saisie  date-mini      !                                                !
!z-cmp.i               !date-maxi                            !                                                !
!fncompo.i             !magasins  fr-saisie magasins         !                                                !
!z-cmp.i               !magasins                             !                                                !
!fncompo.i             !periph  fr-saisie  periph            !                                                !
!z-cmp.i               !periph                               !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                          E - T R F E 1 Q . P                             */
/*                   Edition  Etiquettes  Tarif  GENCOD                     */
/* Saisie Parametres                                      ( CC 21/09/2001 ) */
/*==========================================================================*/

{ connect.i      }
{ e-trfe10.i     }
{ aide.i         }
{ compo.f    new }

def var prefix-1       as char no-undo .
def var prefix-2       as char no-undo .

/* Saisie des zones */
assign compo-vecteur = "<01>,10,20,30,40,50,60"
       compo-bloque  = no .

SAISIE :
REPEAT :

    { compo.i }

    /* 10 : Code Tarif */
    REPEAT WHILE COMPO-MOTCLE = 10 WITH FRAME FR-SAISIE :

        assign prefix-1 = "TAR"
               prefix-2 = "CODE-TARIF"
               .

        { fncompo.i  code-tarif  fr-saisie  code-tarif  }
        { z-cmp.i    code-tarif  "' '" }
        { z-rec.i    codsoc-soc  "''"       code-tarif  }

        FIND FIRST TABGES where tabges.codsoc = codsoc-soc
                          and   tabges.etabli = ""
                          and   tabges.typtab = prefix-1
                          and   tabges.prefix = prefix-2
                          and   tabges.codtab begins code-tarif
                          no-lock no-error .

        if not available tabges
        then do :
            bell . bell .
            message code-tarif libelle[ 11 ] .
            readkey pause 2 .
            undo, retry.
        end .

        assign code-tarif = caps( tabges.codtab )
               lib-tarif  = tabges.libel1[ mem-langue ]
               .
        display code-tarif lib-tarif .

        assign prefix-1 = "TAR"
               prefix-2 = "ETIQ-TARIF"
               .

        FIND TABGES where tabges.codsoc = codsoc-soc
                    and   tabges.etabli = ""
                    and   tabges.typtab = prefix-1
                    and   tabges.prefix = prefix-2
                    and   tabges.codtab = code-tarif
                    no-lock no-error .

        if not available tabges or tabges.dattab[ 1 ] = ? or
           tabges.libel1[ 1 ] = ""
        then do :
            bell . bell .
            message codsoc-soc "-" ' ' "-" prefix-1 "-" prefix-2 "-"
                    code-tarif libelle[ 12 ] .
            readkey pause 5 .
            undo, retry.
        end .

        if date-mini = ? or code-tarif <> anc-code-tarif
           then date-mini = tabges.dattab[ 1 ] + 1 .

        assign anc-code-tarif = code-tarif
               list-soc       = tabges.libel1[ 1 ]
               .

        leave .

    END .  /*  REPEAT : 10  */

    /* 20 : jeu complet */
    REPEAT WHILE COMPO-MOTCLE = 20 WITH FRAME FR-SAISIE :

        { fncompo.i  top-complet  fr-saisie  top-complet }
        { z-cmp.i    top-complet "' '" }

        IF top-complet <> "N" AND top-complet <> "O"
        THEN UNDO , RETRY .

        leave .

    END .  /*  REPEAT : 2O  */


    IF top-complet = "N"
    THEN DO:
        /* 30 : Date Minimum */
        REPEAT WHILE COMPO-MOTCLE = 30 WITH FRAME FR-SAISIE :

            { fncompo.i  date-mini  fr-saisie  date-mini }
            { z-cmp.i    date-mini "' '" }

            if date-mini = ? or date-mini > today - 1 or
               date-mini < today - int( libelle[ 53 ] )
            then do :
                bell . bell .
                message date-mini libelle[ 13 ] .
                readkey pause 2 .
                undo, retry.
            end .

            leave .

        END .  /*  REPEAT : 3O  */

        /* 40 : Date Maximum */
        REPEAT WHILE COMPO-MOTCLE = 40 WITH FRAME FR-SAISIE :

            if date-maxi = ? then date-maxi = date-mini + 6 .

            { fncompo.i  date-maxi  fr-saisie  date-mini }
            { z-cmp.i    date-maxi "' '" }

            if date-maxi < date-mini or date-maxi = ?
            then do :
                bell . bell .
                message date-maxi libelle[ 13 ] .
                readkey pause 2 .
                undo, retry.
            end .

            leave .

        END .  /*  REPEAT : 4O  */
    END. /* top-complet = "N"  */
    ELSE ASSIGN date-mini = ?
                date-maxi = ? .

    /* 50 : Magasins */
    SAI-MAG :
    REPEAT WHILE COMPO-MOTCLE = 50 WITH FRAME FR-SAISIE :

        if mag-exclus = "O" then magasins = "E:" + magasins .
        mag-exclus = "N" .

        { fncompo.i  magasins  fr-saisie magasins }
        { z-cmp.i    magasins  "' '" }

        if keyfunction( lastkey ) = "FIND"
        then do:
            assign codtab = ""
                   libel  = ""
                   .
            run v-mag-0.p .
            if codtab <> ?
               then if magasins = "" or magasins = "E:"
                       then magasins = codtab .
                       else magasins = magasins + "," + codtab .
            display magasins .
        end .

        if magasins = ""
        then do :
            mag-exclus = "" .
            leave .
        end .

        if substr( magasins, 1 , 2 ) = "E:" and substr( magasins, 3 ) = ""
        then do :
            bell . bell .
            message magasins libelle[ 14 ] .
            readkey pause 2 .
            undo, retry .
        end.

        if substring( magasins , 1 , 2 ) = "E:"
           then assign mag-exclus                 = "O"
                       substr( magasins , 1 , 2 ) = ""
                       .

        do i = 1 to num-entries( magasins ) :

           FIND MAGASI where magasi.codsoc = magasi-soc
                       and   magasi.codtab = entry( i, magasins )
                       no-lock no-error .

           if not available magasi or magasi.societe <> codsoc-soc or
              ( magasi.equipe <> "1" and magasi.equipe <> "A" ) /* Mag. PDP ou AGIL */
           then do :
               bell . bell .
               message entry( i, magasins ) libelle[ 15 ] .
               readkey pause 2 .
               undo SAI-MAG, retry SAI-MAG .
           end .

        end .

        leave .

    END .  /*  REPEAT : 50  */

    /* 60 : Fichier genere */
    REPEAT WHILE COMPO-MOTCLE = 60 WITH FRAME FR-SAISIE :
        periph = sav-periph .

        { fncompo.i  periph  fr-saisie  periph }
        { z-cmp.i    periph  "' '" }

        if periph = ""
        then do :
            bell . bell .
            message periph libelle[ 16 ] .
            readkey pause 2 .
            undo, retry.
        end .
        sav-periph = periph .

        leave .

    END .  /*  REPEAT : 60  */

END .     /*   SAISIE */