/*==========================================================================*/
/*                           E X T - P 2 D E . I                            */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Depots PDP V2                                   CC 01/03/1999 */
/*==========================================================================*/

    if magasi.nombre[30] <> 0 then return . /* Non PDP V2 */

    zone = "001       01 "                              +
           "1550000" + string ( magasi.codtab , "xxx" ) +
           string ( magasi.libel1[mem-langue] , "x(30)" )
           .
