/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/ctc-000.i                                                                    !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 14224 02-02-06!Evo!cch            !Ajout s�lection sur Fournisseur de la Plateforme si LISADIS           !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*=======================================================================*/
/*                             C T C - 0 0 0 . I                         */
/*       Constitution du Fichier TARPRE : Preparation Tarif Clients      */
/*=======================================================================*/

def var lib-trt   as char format "x(30)" no-undo .
def var lib-tarif as char format "x(30)" no-undo .

def {1} shared var zon-io           as char   format "x(51)"               no-undo .
def {1} shared var io-sav           as char   format "x(51)"               no-undo .
def {1} shared var articl           as char   format "x(10)"               no-undo .
def {1} shared var articl-codaux    as char   format "x(23)"               no-undo .

def {1} shared var mk-debut         as dec                                 no-undo .

def {1} shared var date-debut       like {tarcli.i}.date-debut             no-undo .
def {1} shared var date-fin         like {tarcli.i}.date-debut             no-undo .
def {1} shared var date-appli       like {tarcli.i}.date-debut             no-undo .
def {1} shared var dat-pxacha       like {tarcli.i}.date-debut             no-undo .

def {1} shared var tarpre-chrono    like tarpre.chrono                     no-undo .

def {1} shared var codval-cli       like {tarcli.i}.tarif-ref              no-undo .
def {1} shared var codval-rev       like {tarcli.i}.tarif-ref              no-undo .
def {1} shared var pxvold-cli       like {tarcli.i}.valeur                 no-undo .
def {1} shared var pxvold-rev       like {tarcli.i}.valeur                 no-undo .
def {1} shared var pxvnew-cli       like {tarcli.i}.valeur                 no-undo .
def {1} shared var pxvnew-rev       like {tarcli.i}.valeur                 no-undo .

def {1} shared var articl-famart    as char                                no-undo .
def {1} shared var articl-soufam    as char                                no-undo .
def {1} shared var articl-valida    as char   format "x"                   no-undo .
def {1} shared var articl-libart    as char   format "x(32)"               no-undo .

def {1} shared var catego-preced    as char   format "x(6)"                no-undo .

def {1} shared var code-tarif       like {tarcli.i}.code-tarif             no-undo . 
def {1} shared var codtar-cli       like {tarcli.i}.code-tarif             no-undo . 
def {1} shared var codtar-rev       like {tarcli.i}.code-tarif             no-undo . 

def {1} shared var ctc-motcle       like {tarcli.i}.motcle  initial "ACH"  no-undo .
def {1} shared var ctv-motcle       like {tarcli.i}.motcle  initial "VTE"  no-undo .

def {1} shared var prix-maxi        like {tarcli.i}.valeur                 no-undo .
def {1} shared var pxacha           like {tarcli.i}.valeur                 no-undo .
def {1} shared var pxbrut           like {tarcli.i}.valeur                 no-undo .
def {1} shared var remise           like {tarcli.i}.valeur                 no-undo .

def {1} shared var pxacha-old       like {tarcli.i}.valeur                 no-undo .
def {1} shared var pxacha-new       like {tarcli.i}.valeur                 no-undo .

def {1} shared var taux-tva         as dec                                 no-undo .
def {1} shared var zone-tva         as char                                no-undo .
def {1} shared var param-arondi     as char   format "x(8)"                no-undo .
def {1} shared var prix             as dec    format ">>>>>>>9.999999-"    no-undo .

def {1} shared var type-trt         as int    format "9"                   no-undo .
def {1} shared var l-tiers          as char                                no-undo .
def {1} shared var type-tiers       as char   format "xxx"                 no-undo .
def {1} shared var code-tiers       as char   format "x(10)"               no-undo .
def {1} shared var lib-tiers        as char   format "x(32)"               no-undo .

def {1} shared var code-fourn       as char   format "x(9)"                no-undo .

def {1} shared var valida           as char   format "x"                   no-undo .

def {1} shared var nb-lus           as int    format "99999"               no-undo .
def {1} shared var nb-fourni        as int    format "99999"               no-undo .

def {1} shared var ii               as int                                 no-undo .
def {1} shared var jj               as int                                 no-undo .
def {1} shared var kk               as int                                 no-undo .

def {1} shared var libelle          as char   extent 49                    no-undo .

def {1} shared var dev-gescom       as char                                no-undo .

def {1} shared frame fr-saisie .

form " " skip
     libelle[ 11 ] format "x(28)" type-trt lib-trt                skip(1)
     libelle[ 12 ] format "x(28)" type-tiers code-tiers lib-tiers skip(1)
     libelle[ 13 ] format "x(28)" code-fourn                      skip(1)
     libelle[ 14 ] format "x(28)" code-tarif lib-tarif            skip(1)
     libelle[ 15 ] format "x(28)" date-debut                      skip(1)
     libelle[ 16 ] format "x(28)" date-fin                        skip(1)
     libelle[ 17 ] format "x(28)" date-appli                      skip(1)
     libelle[ 18 ] format "x(28)" valida                          skip(1)
     with frame fr-saisie
     row 3 centered overlay no-label {v6frame.i} .

