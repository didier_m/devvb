/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/gesdec-0.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V61 55108 29-06-15!New!pte            !Ajout des Messages de debug de Mr CONSTANT                            !
!V61 42766 25-11-13!Evo!cch            !Extraction Cdes : Changement r�gle de g�n�ration dans 2 fichiers      !
!V61 42305 05-11-13!Evo!cch            !Mise au point �volution �volutions E187 et E188 - EUREA               !
!V61 41966 21-10-13!Evo!cch            !Ajout �x�cution genb-cde.p + gesdec-2.p - EUREA                       !
!V61 39472 04-07-13!Evo!cch            !Ajout gestion articles palettes perdues sans prix                     !
!V61 27397 21-11-11!Evo!cch            !Ajout majmoucb.i sur AUXAPR pour envoi sur STOCK                      !
!V61 27013 24-10-11!Evo!cch            !Ajout Int�gration Commentaires sur Adresses de Livraison              !
!V61 27003 21-10-11!Evo!cch            !Ajout Int�gration des Adresses de Livraisons                          !
!V61 25603 15-07-11!Evo!cch            !Ajout application coef. sur qt� TAF                                   !
!V61 25427 06-07-11!Evo!cch            !Ajout gestion int�gration des lignes de Soulte                        !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/* GESDEC-0.I - GESTION DES ECHANGES DE DONNEES NATHALIE / DECLIC             */
/*----------------------------------------------------------------------------*/
/* Variables Communes                               02/04/2002 - SOPRES - 273 */
/******************************************************************************/

def stream s-fic .
def stream s-fic-2 .
def stream s-rej .
def stream s-lis .
def stream s-blo .

def var i-i         as int          no-undo .
def var j-j         as int          no-undo .
def var tour-soc    as int          no-undo .
def var tour-motcle as int          no-undo .
def var tour-tiers  as int          no-undo .
def var tour-fac    as int          no-undo .
def var i-param     as char         no-undo .
def var o-param     as char         no-undo .
def var nom-fic     as char         no-undo .
def var nom-fic-2   as char         no-undo .
def var enreg       as char         no-undo .
def var zone        as char         no-undo .
def var mess-lib    as char         no-undo .
def var mess-err    as char         no-undo .
def var mess-err-2  as char         no-undo .
def var mess-tit    as char         no-undo .
def var mess-ent    as char         no-undo .
def var mess-lig    as char         no-undo .
def var nb-lu       as int          no-undo .
def var nb-trt      as int          no-undo .
def var nb-maj      as int extent 3 . /* a ne pas mettre en no-undo */
def var nb-cre      as int extent 3 .
def var nb-err      as int          no-undo .
def var nb-lig-lis  as int          no-undo .
def var val-num     as dec          no-undo .
def var codsoc-anc  as char         no-undo .
def var articl-soc  as char         no-undo .
def var auxapr-soc  as char         no-undo .
def var magasi-soc  as char         no-undo .
def var impapr-soc  as char         no-undo .
def var fin-ok      as log          no-undo .
def var edit-ok     as log          no-undo .
def var trt-ok      as log          no-undo .
def var rejet-ok    as log          no-undo .
def var pos-deb     as int          no-undo .
def var type-tiers  as char         no-undo .
def var code-tiers  as char         no-undo .
def var tiers-liv   as char         no-undo .
def var tiers-fac   as char         no-undo .
def var lib-fac     as char         no-undo .
def var ouv-cde     as log          no-undo .
def var ouv-cde-2   as log          no-undo .

def {1} shared var libelle         as char extent 200                no-undo .
def {1} shared var repert-depart   as char                           no-undo .
def {1} shared var repert-arriv    as char                           no-undo .
def {1} shared var repert-traite   as char                           no-undo .
def {1} shared var repert-rejet    as char                           no-undo .
def {1} shared var repert-liste    as char                           no-undo .
def {1} shared var repert-trav     as char                           no-undo .
def {1} shared var operat-maj      as char                           no-undo .
def {1} shared var nomimp          as char                           no-undo .
def {1} shared var ind-trt         as int                            no-undo .
def {1} shared var type-trt        as char                           no-undo .
def {1} shared var l-type-trt      as char                           no-undo .
def {1} shared var l-fic           as char                           no-undo .
def {1} shared var l-extens        as char                           no-undo .
def {1} shared var l-soc           as char                           no-undo .
def {1} shared var l-soc-appo      as char                           no-undo .
def {1} shared var l-soc-dec       as char                           no-undo .
def {1} shared var l-soc-test      as char                           no-undo .
def {1} shared var l-soc-init      as char                           no-undo .
def {1} shared var l-soc-art       as char                           no-undo .
def {1} shared var l-soc-adl       as char                           no-undo .
def {1} shared var l-tiers         as char                           no-undo .
def {1} shared var l-tiers-f-dec   as char                           no-undo .
def {1} shared var l-tiers-f-dec-2 as char                           no-undo .
def {1} shared var l-tiers-l-dec   as char                           no-undo .
def {1} shared var l-motcle        as char                           no-undo .
def {1} shared var l-typcom        as char                           no-undo .
def {1} shared var l-typcom-liv    as char                           no-undo .
def {1} shared var l-type-art      as char                           no-undo .
def {1} shared var l-condit-art    as char                           no-undo .
def {1} shared var l-mag           as char                           no-undo .
def {1} shared var l-mag-dec       as char                           no-undo .
def {1} shared var l-mag-nat       as char                           no-undo .
def {1} shared var l-modenlev      as char                           no-undo .
def {1} shared var l-modliv        as char                           no-undo .
def {1} shared var l-modtrp        as char                           no-undo .
def {1} shared var l-lieu-charg    as char                           no-undo .
def {1} shared var l-par-compta    as char                           no-undo .
def {1} shared var l-usine         as char                           no-undo .
def {1} shared var l-art-palette   as char                           no-undo .
def {1} shared var l-art-sansprix  as char                           no-undo .
def {1} shared var l-par-pfac      as char                           no-undo .
def {1} shared var l-cereale       as char extent 4                  no-undo .
def {1} shared var l-codmvt        as char extent 2                  no-undo .
def {1} shared var l-par-tarif     as char                           no-undo .
def {1} shared var l-signe         as char                           no-undo .
def {1} shared var l-typtie-stk_it as char                           no-undo .
def {1} shared var typaux-def      as char                           no-undo .
def {1} shared var nbj-ecar-date   as int                            no-undo .
def {1} shared var pcent-tol-taf   as dec                            no-undo .
def {1} shared var coef-taf        as dec                            no-undo .
def {1} shared var mag-echcer      as char                           no-undo .
def {1} shared var fic-rejet       as char            format "x(50)" no-undo .
def {1} shared var trt-rejet       as log                            no-undo .
def {1} shared var fic-trt         as char                           no-undo .
def {1} shared var fic-trt-2       as char                           no-undo .
def {1} shared var fic-ren         as char                           no-undo .
def {1} shared var fic-ren-2       as char                           no-undo .
def {1} shared var fic-rej         as char                           no-undo .
def {1} shared var fic-rej-2       as char                           no-undo .
def {1} shared var fic-lis         as char                           no-undo .
def {1} shared var fic-blo         as char extent 2                  no-undo .
def {1} shared var ext-dat         as char                           no-undo .
def {1} shared var ext-trt         as char                           no-undo .
def {1} shared var ext-rej         as char                           no-undo .
def {1} shared var ext-lis         as char                           no-undo .
def {1} shared var ext-blo         as char extent 2                  no-undo .
def {1} shared var date-j          as char                           no-undo .
def {1} shared var motcle-vte      as char                           no-undo .
def {1} shared var motcle-ach      as char                           no-undo .
def {1} shared var trt-maj         as log                            no-undo .
def {1} shared var trt-test        as log                            no-undo .
def {1} shared var l-rowent        as char                           no-undo .
def {1} shared var typtab-trt      as char                           no-undo .
def {1} shared var prefix-trt      as char                           no-undo .
def {1} shared var codtab-trt      as char                           no-undo .
def {1} shared var no-fic          as int                            no-undo .
def {1} shared var no-fic-2        as int                            no-undo .
/* Par Claude CONSTANT le 26/06/2015 */
def {1} shared var fichiermouc     as char                           no-undo .
def {1} shared var logtrace        as char                           no-undo .

