/*============================================================================*/
/*                           G T M - E T A 0 . I                              */
/* Edition des Tarifs Ventes par Magasins                                     */
/*----------------------------------------------------------------------------*/
/* Variables et Frame                                                         */
/*============================================================================*/

def var i       as int                           no-undo .
def var lib-maq as char format "x(30)"           no-undo .
def var val-edi as char                extent 20 no-undo .

def {1} shared var libelle    as char                     extent 40 no-undo .
def {1} shared var magasin    as char format "xxx"                  no-undo .
def {1} shared var famille    as char format "xxx"                  no-undo .
def {1} shared var sous-fam   as char                               no-undo .
def {1} shared var date-tarif as date format "99/99/9999"           no-undo .
def {1} shared var code-tarif as char format "xxx"                  no-undo .
def {1} shared var maquette   as char format "x(12)"                no-undo .
def {1} shared var periph     as char format "x(12)"                no-undo .
def {1} shared var lib-mag    as char format "x(32)"                no-undo .
def {1} shared var lib-fam    as char format "x(30)"                no-undo .
def {1} shared var lib-sfam   as char                               no-undo .
def {1} shared var magasi-soc as char                               no-undo .
def {1} shared var rg-edi     as int                      extent 2  no-undo .

def {1} shared frame fr-saisie .

form libelle[ 1 ] format "x(21)" magasin     lib-mag skip( 1 )
     libelle[ 2 ] format "x(21)" famille     lib-fam skip( 1 )
     libelle[ 3 ] format "x(21)" date-tarif space( 6 )
     libelle[ 6 ] format "x(17)" code-tarif          skip( 1 )
     libelle[ 4 ] format "x(21)" maquette    lib-maq skip( 1 )
     libelle[ 5 ] format "x(21)" periph
     with frame fr-saisie row 2 centered overlay {v6frame.i} no-label .
