/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/src/gesm-rex.p                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 15585 12-01-07!Bug!cch            !Correction LIGNES.DATBON non charg�e                                  !
!V52 15572 11-01-07!Evo!cch            !Forcage Dev; re�ue � "E" pour palier dev. "F" envoy�e par cde pistolet!
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !auxili                                          !
!gesmag-0.i            !                                     !entete                                          !
!majsto-0.i            !new                                  !auxapr                                          !
!x-calr.i              !new                                  !lignes                                          !
!cadr-aux.i            !codsoc-soc type-aux code-aux         !artapr                                          !
!majmoucb.i            !entete                               !artbis                                          !
!x-defpri.i            !                                     !proglib                                         !
!majmoucb.i            !lignes                               !                                                !
!gesmag-p.i            !                                     !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/* GESM-REX.P - GESTION DE L'INTEGRATION DES BONS MAGASINS                    */
/*----------------------------------------------------------------------------*/
/* Integration Cde Reappro.- REA, REM LISADIS + L.VGTAL 04/2006 - EUREA - 273 */
/******************************************************************************/

{ connect.i      }
{ gesmag-0.i     }
{ majsto-0.i new }
{ x-calr.i   new }

def var numbon-pro as char no-undo .
def var nb-decim   as int  no-undo .
def var aaa        as int  no-undo .
def var no-promo   as int  no-undo .
def var remlig     as dec  no-undo .

def buffer pro-entete for entete .

FIND LIGNES-TRAV where recid( lignes-trav ) = r-lignes-trav no-lock .

if lignes-trav.chrono = "000" and lignes-trav.ss-chrono = 0
then do :
    mess-err = libelle[ 124 ] .
    run trt-rejet .
    return .
end .

assign motcle-trt = motcle-ach
       typcom-trt = typcom-cdf
       numbon-trt = lignes-trav.magasin + lignes-trav.numbon
       niveau-trt = niveau-bon[ 1 ]
       .

assign type-aux = typaux-def
       code-aux = trim( substr( lignes-trav.enreg, 78, 6 ) )
       .

{ cadr-aux.i codsoc-soc type-aux code-aux }

run ctrl-auxapr .    
if not available auxapr then return .

run ctrl-auxili .    
if not available auxili then return .

if ( type-aux <> typaux-lis or code-aux <> codaux-lis ) and
   ( type-aux <> typaux-lisv or code-aux <> codaux-lisv )
then do :
    mess-err = type-aux + " " + code-aux + libelle[ 125 ] + typaux-lis + " " + codaux-lis + " ..." .
    run trt-rejet .
    return .
end .

zone = substr( lignes-trav.enreg, 93, 6 ) + ",0,81" . 
run ctrl-date .
if not ok-trt-bon then return .
date-bon = val-date .

zone = substr( lignes-trav.enreg, 171, 6 ) + ",1,81" . 
run ctrl-date .
if not ok-trt-bon then return .
if val-date = ? then date-dep = date-bon .
                else date-dep = val-date .

if date-dep <= date-arrete then date-dep = date-arrete + 1 . 

if date-dep < date-bon
then do :
    mess-err = string( date-dep, "99/99/9999" ) + " < " + string( date-bon, "99/99/9999" ) + " " +
               libelle[ 126 ] .
    run trt-rejet .
    return .
end .

zone = substr( lignes-trav.enreg, 177, 6 ) + ",1,81" . 
run ctrl-date .
if not ok-trt-bon then return .
if val-date = ? then date-liv = date-dep .
                else date-liv = val-date .

if date-liv <= date-arrete then date-liv = date-arrete + 1 . 

if date-liv < date-dep
then do :
    mess-err = string( date-dep, "99/99/9999" ) + " < " + string( date-bon, "99/99/9999" ) + " " +
               libelle[ 127 ] .
    run trt-rejet .
    return .
end .

mode-liv = right-trim( substr( lignes-trav.enreg, 212, 2  ) ) .
if mode-liv <> ""
then do :
    run ctrl-mode-liv .
    if not ok-trt-bon then return .
end .

/* Agrisis envoi systematiquement "F" si commande par pistolet.
   En attendant correction on prend le 1er caract�re de la devise de gestion
   ( CC 11/01/2007 )
   zone = substr( lignes-trav.enreg, 559, 1  ) + "," + auxili.devise .
*/
zone = substr( dev-gescom, 1, 1 ) + "," + auxili.devise .
run ctrl-devise .
if not ok-trt-bon then return .

run ctrl-entete .
if not ok-trt-bon then return .

CREATE ENTETE .            

assign entete.codsoc      = codsoc-soc                                            
       entete.motcle      = motcle-trt                                            
       entete.typcom      = typcom-trt                                                
       entete.numbon      = numbon-trt                                             

       entete.magasin     = lignes-trav.magasin 
       entete.datbon      = date-bon
       entete.dattar      = date-bon
       entete.datdep      = date-dep
       entete.datliv      = date-liv
       entete.devise      = auxili.devise
       entete.txdev       = 1
       entete.ref-magasin = lignes-trav.codmvt                                 + 
                            right-trim( substr( lignes-trav.enreg, 68 , 7  ) ) +
                            lignes-trav.magasin                                                   
       entete.ref-tiers   = right-trim( substr( lignes-trav.enreg, 241, 10 ) )                                                    
       entete.rec-ase     = right-trim( substr( lignes-trav.enreg, 544, 15 ) ) 
       entete.typaux      = type-aux
       entete.codaux      = code-aux
       entete.nivcom      = niveau-cde
       entete.modliv      = mode-liv
       entete.env-plat    = top-non-a
       entete.domapr      = auxapr.domapr                           
       entete.modreg      = auxapr.modreg                           
       entete.codech      = auxapr.codech
       entete.type-tva    = auxili.ttva     
       .

if lignes-trav.codmvt = typcom-rea or
   ( lignes-trav.codmvt = typcom-rem and entete.codsoc = "26" or entete.codsoc = "43" )
   then assign entete.edition     = codent-vac
               entete.datedi[ 1 ] = today
               entete.opedi[ 1 ]  = operat
               .  

case lignes-trav.codmvt :

    when typcom-rea then assign entete.typbon = typbon-std  
                                entete.codpri = codpri-ach
                                .

    when typcom-rem then do :

        assign entete.typbon = typbon-pro
               entete.codpri = codpri-pre
               .

        /* Controle et Stockage du no promo. */
        REPEAT :

            assign no-promo   = 0
                   numbon-pro = "" .
                   zone       = trim( substr( lignes-trav.enreg, 241, 10 ) )
                   .

            no-promo = int( zone ) no-error .
            if no-promo = 0
            then do :
                entete.typbon = "E" + zone .
                leave .
            end .

            numbon-pro = string( no-promo, "99999999" ) + "00" no-error .
            if numbon-pro = ""
            then do :
                entete.typbon = "E" + zone .
                leave .
            end .

            FIND PRO-ENTETE where pro-entete.codsoc = proplatef-soc
                            and   pro-entete.motcle = "VTP"
                            and   pro-entete.typcom = "PRO"
                            and   pro-entete.numbon = numbon-pro
                            no-lock no-error .

            if not available pro-entete or
               pro-entete.typaux <> entete.typaux or pro-entete.codaux <> entete.codaux
            then do :
                entete.typbon = "E" + zone .
                leave .
            end .

            entete.typbon = entete.typbon + string( no-promo ) .

            if pro-entete.datedi[ 12 ] <> ?
               then entete.dattar = pro-entete.datedi[ 12 ] .

            if pro-entete.datedi[ 14 ] <> ? and pro-entete.datedi[ 14 ] <> entete.datdep and
               pro-entete.datedi[ 14 ] >= entete.datbon
            then do :
                entete.datdep = pro-entete.datedi[ 14 ] .
                if entete.datdep <= date-arrete then entete.datdep = date-arrete + 1 .
                if entete.datliv < entete.datdep then entete.datliv = entete.datdep .
            end .

            if pro-entete.datedi[ 15 ] <> ? and pro-entete.datedi[ 15 ] <> entete.datliv and
               pro-entete.datedi[ 15 ] >= entete.datbon
            then do :
                entete.datliv = pro-entete.datedi[ 15 ] .
                if entete.datliv < entete.datdep then entete.datliv = entete.datdep .
            end .

            leave .

        END . /* REPEAT */

    end . /* when typcom-rem */

end case . /* case lignes-trav.codmvt : */

if auxapr.typaux-fac = "" or auxapr.codaux-fac = ""
   then assign entete.typaux-fac = entete.typaux               
               entete.codaux-fac = entete.codaux               
               .               
   else assign entete.typaux-fac = auxapr.typaux-fac               
               entete.codaux-fac = auxapr.codaux-fac               
               .

do ind = 1 to 5 :
    entete.adres[ ind ] = auxili.adres[ ind ] .
end .

{ majmoucb.i entete }

FOR EACH B-LIGNES-TRAV where b-lignes-trav.codsoc  = lignes-trav.codsoc
                       and   b-lignes-trav.magasin = lignes-trav.magasin
                       and   b-lignes-trav.codmvt  = lignes-trav.codmvt
                       and   b-lignes-trav.numbon  = lignes-trav.numbon
                       use-index i-lignes-1 no-lock : 

    if b-lignes-trav.chrono <> "COMMENT-ENT" /* <> Commentaire Entete */
    then do :

        zone = b-lignes-trav.chrono + ",80" . 
        run ctrl-val-int .
        if not ok-trt-bon then return .
        chrono-trt = val-int .

        FIND LIGNES where lignes.codsoc = codsoc-soc
                    and   lignes.motcle = motcle-trt
                    and   lignes.typcom = typcom-trt
                    and   lignes.numbon = numbon-trt
                    and   lignes.chrono = chrono-trt
                    exclusive-lock no-error .

    end .

    case b-lignes-trav.ss-chrono :

        when 0 then do : /* Ligne Article */

            if available lignes
            then do :
                mess-err = entete.codsoc + " " + entete.motcle + " " + entete.typcom + " " + 
                           entete.numbon + " " + string( lignes.chrono ) + " " + libelle[ 85 ] .
                run trt-rejet .
                return .
            end .

            code-art = substr( b-lignes-trav.enreg, 116, 6 ) .   
            run ctrl-artapr .
            if not available artapr then return .

            assign type-aux = entete.typaux
                   code-aux = entete.codaux
                   zone     = ""
                   .

            run ctrl-artbis-ach .            
            if not available artbis then return .

            zone = substr( b-lignes-trav.enreg, 271, 10 ) + ",0,93" . 
            run ctrl-val-dec .
            if not ok-trt-bon then return .
            quantite = val-dec .

            zone = substr( b-lignes-trav.enreg, 308, 9 ) + ",1,96" . 
            run ctrl-val-dec .
            if not ok-trt-bon then return .
            prix = val-dec / 1000 .

            CREATE LIGNES .

            assign lignes.codsoc            = entete.codsoc 
                   lignes.motcle            = entete.motcle 
                   lignes.typcom            = entete.typcom 
                   lignes.numbon            = entete.numbon 
                   lignes.chrono            = chrono-trt       

                   lignes.codlig            = codlig-normale           
                   lignes.articl            = code-art
                   lignes.codpri            = entete.codpri
                   lignes.typaux            = entete.typaux   
                   lignes.codaux            = entete.codaux           
                   lignes.nivcom            = entete.nivcom    
                   lignes.magasin           = entete.magasin
                   lignes.datbon            = entete.datbon 
                   lignes.datdep            = entete.datdep 
                   lignes.codech            = entete.codech
                   lignes.mvtsto[ 1 ]       = entete.modliv
                   lignes.mvtsto[ 2 ]       = entete.modreg
                   lignes.txdev             = entete.txdev  
                   lignes.tenusto           = artapr.tenusto
                   lignes.usto              = artapr.usto   
                   lignes.ttva              = artapr.tva    
                   lignes.pdsuni            = artapr.pds    
                   lignes.imput             = artapr.impach
                   lignes.ufac              = artbis.ufac
                   lignes.ucde              = artbis.ucde
                   lignes.coef-cde          = artbis.coef-cde
                   lignes.coef-fac          = artbis.coef-fac
                   lignes.qte[ niveau-trt ] = quantite 
                   .

            if artbis.libart1 = "" then lignes.libart = artapr.libart1[ mem-langue ] .
                                   else lignes.libart = artbis.libart1 .           

            if lignes.coef-cde > 0
               then lignes.qte[ niveau-trt ] = lignes.qte[ niveau-trt ] / lignes.coef-cde .
               else lignes.qte[ niveau-trt ] = lignes.qte[ niveau-trt ] * ( - lignes.coef-cde ) .

            if prix = 0
            then do :
                assign cde-qte  = lignes.qte[ niveau-trt ]
                       cde-rang = niveau-trt
                       .
                { x-defpri.i }
            end .
            else assign lignes.pu-brut = prix
                        lignes.pu-net  = prix
                        .

            /* Prix en attente */
            if lignes.pu-net = 0 then assign lignes.codlig-type = top-3-n
                                             lignes.codpri      = codpri-attente
                                             entete.prx-attente = top-1-n
                                             .

            if lignes.tenusto = tenusto-stock and quantite <> 0
            then do :

                assign majsto-magasin = lignes.magasin
                       majsto-articl  = lignes.articl
                       majsto-mvtsto  = mvtsto-cdf     /* + dans encours fournisseur */
                       majsto-date    = lignes.datbon
                       majsto-qte     = quantite
                       .

                run majsto-0.p .

            end .

            { majmoucb.i lignes }

            ok-trt-lig = yes .

        end . /* Ligne Article */

        otherwise do : /* Ligne Commentaire */

            if not available lignes and b-lignes-trav.chrono <> "COMMENT-ENT"
            then do :
                mess-err = entete.codsoc + " " + entete.motcle + " " + entete.typcom + " " + 
                           entete.numbon + " " + string( chrono-trt ) + " " + libelle[ 89 ] .
                run trt-rejet .
                return .
            end .            

            zone = substr( b-lignes-trav.enreg, 112, 260 ) .
            if zone = "" then next .

            if b-lignes-trav.chrono = "COMMENT-ENT"
               /* Commentaires Entete */
               then assign proglib-clef    = "COMMANDE"
                           proglib-edition = "ENT" + string( entete.typcom, "xxx" ) + entete.numbon
                           .
               /* Commentaires Ligne */
               else assign proglib-clef    = string( entete.typcom, "xxx" )   +
                                             string( entete.numbon, "x(10)" ) +
                                             string( chrono-trt, "9999" )
                           proglib-edition = "LIG-BP"
                           .

            zone = substr( b-lignes-trav.enreg, 112, 260 ) .
            if zone = "" then next .

            FIND PROGLIB where proglib.motcle  = entete.motcle
                         and   proglib.codsoc  = entete.codsoc
                         and   proglib.etabli  = ""
                         and   proglib.clef    = proglib-clef
                         and   proglib.edition = proglib-edition
                         and   proglib.chrono  = 0
                         no-lock no-error .

            if available proglib
            then do :
                mess-err = entete.codsoc + " " + entete.motcle + " " + entete.typcom + " " + 
                           entete.numbon + " " + string( chrono-trt ) + " " + libelle[ 90 ] .
                run trt-rejet .
                return .
            end .            

            CREATE PROGLIB .

            assign proglib.motcle  = entete.motcle
                   proglib.codsoc  = entete.codsoc
                   proglib.etabli  = ""
                   proglib.clef    = proglib-clef
                   proglib.edition = proglib-edition
                   proglib.chrono  = 0
                   .

            do ind = 1 to 4 :
                proglib.libelle[ ind ] = right-trim( substr( zone, ( ind * 65 ) - 64 , 65 ) ) .
            end .

        end . /* Ligne Commentaire */

    end case . /* case b-lignes-trav.ss-chrono : */

END . /* FOR EACH B-LIGNES-TRAV */

if not ok-trt-lig
then do :
    mess-err = libelle[ 103 ] .
    run trt-rejet .
    return .
end .

i-param = string( recid( entete ) ) + "," + string( niveau-trt ) + "," + typtva-exo + "," +
          entete.motcle + ",O" .
run sai-risq.p( i-param, output o-param ) . /* Calcul cumuls entete et lignes */

nb-bon-cre = nb-bon-cre + 1 .

if mouchard then message libelle[ 40 ] entete.codsoc entete.motcle entete.typcom entete.numbon entete.magasin
                 view-as alert-box information .


/**************************** P R O C E D U R E S ******************************/

{ gesmag-p.i } /* Procedures Communes */