/*==========================================================================*/
/*                           R - I N T C H B . P                            */
/* Integration des Chronos Pieces                                           */
/*==========================================================================*/

{ connect.i  }

def var lib-titre as char                  no-undo .
def var zone      as char                  no-undo .
def var x-ficchr  as char format "x(50)"   no-undo .
def var x-ok      as log  format "Oui/Non" no-undo . 
def var nb-lu     as int                   no-undo .

def stream f-chr .

FORM  "Fichier Import Chrono" x-ficchr
      "OK (O/N)             " x-ok     
      with frame FR-SAISIE
      row 2  centered  overlay {v6frame.i}  no-label with title lib-titre .

lib-titre = "INTEGRATION DES CHRONOS PIECES" .

REPEAT :
    update x-ficchr x-ok with frame fr-saisie . 
    if x-ficchr = "" or search( x-ficchr ) = ? then next .
    leave .
END .
if not x-ok then return . 

input stream f-chr from value( x-ficchr ) append .

REPEAT :

    import stream f-chr unformatted zone .
    
    if nb-lu modulo 10 = 0
    then do :
        hide message no-pause .
        message "CHRONO PIECE" nb-lu .
    end . 

    FIND TABGES where tabges.codsoc = substr( zone, 1, 4 )
                and   tabges.etabli = ""
                and   tabges.typtab = "CHR"
                and   tabges.prefix = "TYPCOM"
                and   tabges.codtab = substr( zone, 5, 3 )
                exclusive-lock no-error .
                                
    if not available tabges
    then do :
        CREATE TABGES .
        assign tabges.codsoc = right-trim( substr( zone, 1, 4 ) )
               tabges.etabli = ""
               tabges.typtab = "CHR"
               tabges.prefix = "TYPCOM"
               tabges.codtab = right-trim( substr( zone, 5, 3 ) )
               .
    end .

    tabges.nombre[ 1 ] = int( substr( zone, 8 ) ) .

    nb-lu = nb-lu + 1 .
    
END .

output stream f-chr close .

hide message no-pause .
message "CHRONO PIECE" nb-lu .
readkey .

hide frame fr-saisie no-pause .
