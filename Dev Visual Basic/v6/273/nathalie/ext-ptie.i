/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/ext-ptie.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V61 46264 23-04-14!Evo!pha            !Int�gration modifs faites par Eurea                                   !
!V61 30840 30-05-12!Evo!cch            !Suite Fiche V61 30816                                                 !
!V61 30816 29-05-12!Evo!cch            !Ajout nvelles Usines EUREA - Modif. effectuees par cco EUREA          !
!V61 19284 09-06-10!Evo!cch            !Ajout gestion soci�t� 57 VOLIREA ( EUREA )                            !
!V52 17917 07-01-10!Evo!cch            !Modif. EUREA - Nouveau code usine 70 pour la soci�t� 41               !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - P T I E . I                            */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET / PRIOS           */
/*--------------------------------------------------------------------------*/
/* Extraction Tiers PRIOS                                                   */
/* Ne redescendent que les tiers qui ont un lien dans auxdet, avec calf11   */
/* different de NE ou calf12 <> 9                                           */
/*==========================================================================*/
/* DGR 160502 : rajout extraction tiers 01 et 05 et 08 pour PRIOS           */
/*              attention 01 et 05 sont redescendues avec code societe 30   */
/* DGR 190602 : on envoie le code contentieux de DEAL                       */
/* DGR 281209 : redescente des clients EVIALIS (scodusi="70",codsoc="41" )  */
/* cco 080610 : Ajout societe 57 VOLIREA                                    */
/* DGR 270513 : recodif 08 : SOC 08 redescend soc 08                        */
/* DGR 310314 : code tiers de regroupement ( soc + auxapr.codaux-fiscal     */
/* DGR 190517 : correction extraction nbjour echeance                       */
/* DGR 070518 : Ajout numero SIRET                                          */

def var lcaract as char initial ".,;,/,\,(,),:,-,,".
def var steleph as char.
def var sfax    as char.
def var sportab as char.
def var smodges as char.
def var stransp as char.
def var i       as int.
def var scodusi as char.
def var sdepot  as char.
def var scodtie as char.
def var sclifac as char extent 10.
def var styptva as char. /* assujeti TVA */
def var scattar as char.
def var styptie as char format "xx".
def var scodech as char format "xx".
def var sconten as char format "x".
def var sonic   as char format "x(5)".
def var slib    as char format "x(24)".
def var sonic1  as char format "x(5)".
def var slib1   as char format "x(24)".
def var sonic2  as char format "x(5)".
def var slib2   as char format "x(24)".
def var sdatsup as char format "x(8)".
def var scodgrp as char.

FIND AUXDET where auxdet.codsoc = soc-prios
            and   auxdet.typaux = auxapr.typaux
            and   auxdet.codaux = auxapr.codaux
            and   auxdet.motcle = "USI"
            and   auxdet.codtab = "PRIOS"
            no-lock no-error .

if available auxdet
then do :

    if liste-transp = "" then
    FOR EACH TRANSP where transp.codsoc = ""
                    and   transp.typaux <> ""
                    and   transp.codaux <> ""
                    no-lock :

        liste-transp = liste-transp + trim(transp.codaux) + "," +
                       trim(transp.transp) + "," .

    END .

    assign smodges = string(notour,"9")
           stransp = ""
           scodusi = string(auxdet.calf11,"xx")
           .
    if scodusi = "05" then sdepot = "NO".

/* CCO 29/05/2012 si Yzeure/Billom/Lourdoueix : Alivert */
    if scodusi = "40" then sdepot = "AL".
    if scodusi = "71" then sdepot = "AL".
    if scodusi = "72" then sdepot = "AL".

/* DGR 281209 si EVIALIS : Nord */
    if scodusi = "70" then sdepot = "NO".

    /* Transporteur */
    i = lookup(trim(auxapr.codaux),liste-transp).
    if i > 0 and notour = 2 then assign smodges = "3"
                                        stransp = entry(i + 1, liste-transp)
                                        .

    /* Type de tiers */
    if num-entries(auxapr.type-tiers) > 1 
       then styptie = "".
       else styptie = substr(auxapr.type-tiers,1,2).

    /* Supprime le 19/02/2014 par cco 
    /* Categorie tarifaire dependante du type de tiers */
    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "AUX"
                and   tabges.prefix = "TYPE-TIERS"
                and   tabges.codtab = styptie
                no-lock no-error.

    if available tabges
       then scattar = substr(tabges.libel1[2],1,2).
       else scattar = "".
    Fin Supprime le 19/02/2014 par cco */

    /* Delai de reglement deduit a partir de auxapr.codech => code PRIOS
       equivalent */
    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "ECH"
                and   tabges.prefix = "CODECH"
                and   tabges.codtab = auxapr.codech
                no-lock no-error.

    /* dgr 19052017  correction recup nb jours echeance */
    /*
    if available tabges
       then scodech = substr(tabges.libel1[3],1,2). 
       else scodech = "".
    */

    if available tabges  then do:
        scodech = string ( int ( trim(substring(tabges.libel1[2],6,3))) ,"99") no-error .
        if error-status:error then scodech = "".
    end.
    else scodech = "" .
    

     
/* DGR 281209 : rajout EVIALIS */
    if smodges = "1"
    then do:
        if (codsoc-trait[nsoc] = "41" and lookup(scodusi ,"40,70") <> 0 ) or
           (codsoc-trait[nsoc] = "57" and lookup(scodusi ,"05,40") <> 0 ) or
/* ajout DGR 27/05/2013  suite recodif */
           (codsoc-trait[nsoc] = "08" )   
           then scodtie = trim(codsoc-trait[nsoc]) + trim(auxapr.codaux).
           else scodtie = soc-auxili + trim(auxapr.codaux).
        if styptie = "" then styptie = "CL".
    end.
    else do:
        scodtie = trim(auxapr.codaux).
        if smodges = "2" then styptie = "FO".
        if smodges = "3" then styptie = "T ".
    end.


    if auxapr.cpt-ferme = "1" or auxapr.blocage = "1" 
       then sdatsup =  string( year( today ), "9999" ) +
                       string( month( today ), "99" )  +
                       string( day( today ), "99" ) .
       else sdatsup = "".

    scodgrp = "".
    if auxapr.codaux-fiscal <> "" then do:
        if smodges = "1"     then do:
            if (codsoc-trait[nsoc] = "41" and lookup(scodusi ,"40,70") <> 0 ) or
               (codsoc-trait[nsoc] = "57" and lookup(scodusi ,"05,40") <> 0 ) or
               (codsoc-trait[nsoc] = "08" )   
            then scodgrp = trim(codsoc-trait[nsoc]) + trim( auxapr.codaux-fiscal).
            else scodgrp = soc-auxili + trim ( auxapr.codaux-fiscal ).
        end.
    end.

    
    zone = "01"                                     /* 001 - 002 */
           + smodges                                /* 003 - 001 */
           + string(styptie,"xx")                   /* 004 - 002 */
           + string(scodtie,"x(10)" )               /* 006 - 010 */
           .

    assign sclifac[1]  = trim(substr(auxdet.calf01,4,10))
           sclifac[2]  = trim(substr(auxdet.calf02,4,10))
           sclifac[3]  = trim(substr(auxdet.calf03,4,10))
           sclifac[4]  = trim(substr(auxdet.calf04,4,10))
           sclifac[5]  = trim(substr(auxdet.calf05,4,10))
           sclifac[6]  = trim(substr(auxdet.calf06,4,10))
           sclifac[7]  = trim(substr(auxdet.calf07,4,10))
           sclifac[8]  = trim(substr(auxdet.calf08,4,10))
           sclifac[9]  = trim(substr(auxdet.calf09,4,10))
           sclifac[10] = trim(substr(auxdet.calf10,4,10))
           .

    do i = 1 to 10 :

/* DGR 281209 : rajout EVIALIS */
        if sclifac[i] <> ""
        then do :
            if i = 7 and codsoc-trait[nsoc] = "41" and lookup(scodusi,"40,70" ) <> 0 
               then sclifac[i] = trim(codsoc-trait[nsoc]) + sclifac[i].
               else if i = 6 and codsoc-trait[nsoc] = "57" and lookup(scodusi,"05,40" ) <> 0 
                    then sclifac[i] = trim(codsoc-trait[nsoc]) + sclifac[i].
/* DGR 27052013: suite 08 recodif */
                    else if codsoc-trait[nsoc] = "08"
                         then sclifac[i] = trim(codsoc-trait[nsoc]) + sclifac[i].
                         else sclifac[i] = soc-auxili + sclifac[i].
         end .
         zone = zone + string(sclifac[i],"x(10)"). /* 016 - 100 */

    end .

    /* On enleve la civilite de l'adresse */
    libel = trim(substr(auxili.adres[1], 1, 3)).    /* 116 - 030 */
    if lookup(libel, libelle[26]) <> 0
    then do :
      if substr(auxili.adres[1], 4 , 1) = "" then
        zone = zone
               + string(substr(auxili.adres[1], 5, 28), "x(30)").
      else
        zone = zone
               + string(substr(auxili.adres[1], 4, 28), "x(30)").
    end.
    else
      zone = zone
             + string(auxili.adres[1], "x(30)").

    sonic = substr(auxapr.onic,2,5).

    zone = zone
           + string(substr(auxili.adres[2],1,30) , "x(30)") /* 146 - 030 */
           + string(substr(auxili.libabr,1,10), "x(10)")    /* 176 - 010 */
           + string(substr(auxili.adres[3],1,30),"x(30)" )  /* 186 - 030 */
                                                            /* 216 - 030 */
           + string(substr(auxili.adres[4],1,30),"x(30)" )  /* 246 - 005 */
           + string(sonic,"x(5)" )                          /* 251 - 005 */
           + string(substr(auxili.adres[5],1,5),"x(5)" ).

    /* On enleve les caracters , ; . , etc du tel, fax et portable */
    steleph = auxapr.teleph.
    do i = 1 to length(steleph):
      if lookup(substr(steleph,i,1), lcaract) > 0 then
        overlay(steleph,i) = " ".
    end.
    sfax = auxapr.fax.
    do i = 1 to length(sfax):
      if lookup(substr(sfax,i,1), lcaract) > 0 then
        overlay(sfax,i) = " ".
    end.
    sportab = auxapr.telex.
    do i = 1 to length(sportab):
      if lookup(substr(sportab,i,1), lcaract) > 0 then
        overlay(sportab,i) = " ".
    end.

    /* 0 pas de tva sinon TVA */
    styptva = substr(auxili.ctax,3,1).
    if styptva = "0" then styptva = "0".
    else styptva = "1".

    /* DGR 190602 contentieux: 0 ou blanc => 0 sinon on met code DEAL */
    if trim(auxapr.contentieux) = "" or auxapr.contentieux = "0" then
      sconten = "0".
    else
      sconten = substr(auxapr.contentieux,1,1).

    zone = zone                                                 /* 256 - 015 */
           + string(substr(trim(steleph),1,15) , "x(15)")
                                                                /* 271 - 015 */
           + string(substr(trim(sportab),1,15) , "x(15)")
                                                                /* 286 - 015 */
           + string(substr(trim(sfax),1,15) , "x(15)")
           + string(auxapr.modreg, "x(3)")                      /* 301 - 003 */
           + string(scodech, "xx")                              /* 304 - 002 */
           + string(styptva,"x")                                /* 306 - 001 */
           + string(scattar,"xx")                               /* 307 - 002 */
           + string(sconten,"x")                                /* 309 - 001 */
           + string(auxapr.magasin,"xxx")                       /* 310 - 003 */
           + string(auxapr.repres[1],"xxx")                     /* 313 - 003 */
           + string(auxapr.repres[2],"xxx")                     /* 316 - 003 */
           + string(auxapr.repres[3],"xxx")                     /* 319 - 003 */
           + string(scodusi,"xx")                               /* 322 - 002 */
           + string(sdepot,"xx")                                /* 324 - 002 */
           + string(stransp,"xxx")                              /* 326 - 003 */
           + string(sdatsup,"x(8)")                             /* 329 - 008 */
           + string ( scodgrp, "x(10)" )                        /* 337 - 010 Code Client Regroupement */       
           + string ( substr(auxili.siret,1,14), "x(14)" )      /* 347 - 014 Numero SIRET             */
           .

end. /* available auxdet */

else zone = "" .



