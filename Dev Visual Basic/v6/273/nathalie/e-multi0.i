/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/src/e-multi0.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 10643 22-09-04!Evo!cch            !Ajout option Type de tri ( Nomenclature ou Libellé ARTICLE ) + ajout c!
!V52 10002 23-07-04!New!cch            !Développement Etats des Stocks Articles Multipro.                     !
!-------------------------------------------------------------------------------------------------------------!
!                             I N C L U D E S                !                    T A B L E S                 !
!------------------------------------------------------------+------------------------------------------------!
!v6frame.i             !                                     !                                                !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E - M U L T I 0 . I                            */
/* Etat des Stocks Articles Multipro                                        */
/*--------------------------------------------------------------------------*/
/* Variables et Frames                                        CC 22/09/2003 */
/*==========================================================================*/

def var i        as int                           no-undo .
def var j        as int                           no-undo .
def var lib-edit as char format "x(30)"           no-undo .
def var lib-tri  as char format "x(45)"           no-undo .
def var lib-maq  as char format "x(30)"           no-undo .
def var val-edi  as char                extent 25 no-undo .

def {1} shared var libelle    as char extent 60           no-undo .
def {1} shared var type-edit  as int  format "9"          no-undo .
def {1} shared var type-tri   as int  format "9"          no-undo .
def {1} shared var date-min   as date format "99/99/9999" no-undo .
def {1} shared var date-max   as date format "99/99/9999" no-undo .
def {1} shared var familles   as char format "x(39)"      no-undo .
def {1} shared var maquette   as char format "x(12)"      no-undo .
def {1} shared var periph     as char format "x(12)"      no-undo .
def {1} shared var fam-exclus as char                     no-undo .
def {1} shared var no-page    as int                      no-undo .

def {1} shared frame fr-saisie .

form libelle[ 1 ] format "x(23)" type-edit lib-edit skip
     libelle[ 2 ] format "x(23)" type-tri  lib-tri  skip
     libelle[ 3 ] format "x(23)" date-min           skip
     libelle[ 4 ] format "x(23)" date-max           skip
     libelle[ 5 ] format "x(23)" familles           skip
     libelle[ 6 ] format "x(23)" maquette  lib-maq  skip
     libelle[ 7 ] format "x(23)" periph
     with frame fr-saisie row 3 centered no-label overlay { v6frame.i } .