/*--------------------------------------------------------------------*/
/* S�lection des articles � prendre en compte pour le traitement :    */
/*     - de l'�cotaxe                                                 */
/*     - des codes barres                                             */
/*     - des tarifs clients et promo                                  */
/*     - des modifications de la redevance Pollution Diffuse          */ 
/*--------------------------------------------------------------------*/
/*================================================================================*/
/*                     Modifications                                              */
/* VLE 27102009 ne plus autoriser la vente des anciens ch�ques cadeau dans Agil   */
/* VLE 22102013 traitement des nouveaux codes tva                                 */
/*================================================================================*/

find artapr 
    where artapr.codsoc = soc-groupe
    and   artapr.articl = warticl
no-lock no-error.
                        
    If not available artapr or 
        substr(artapr.articl, 7, 1) = "S" or                                        /*  articles supprimes  */
        artapr.caract[3] <> " " or                                                  /*  articles non destines � la vente  */
        lookup(artapr.articl-lie, l-article-cvo) <> 0 or                            /*  articles CVO  */
        artapr.famart2 = "P94" or                                                   /*  familles et sous-familles non traitees  */
        artapr.famart2 = "P86" or
        artapr.famart2 = "P87" or
        artapr.famart2 = "P83" or
        artapr.famart2 = "9RE" or
        artapr.soufam2 = "098" or
        (artapr.famart2 = "P85" and artapr.soufam2 = "030") or                      /* VLE 27102009 anciens ch�ques cadeau  */
        (artapr.famart2 = "9AU" and artapr.soufam2 = "099") or
        (artapr.soufam2 = "097" and artapr.sssfam2 <> "01") or
        (artapr.code-blocage = "B" or artapr.code-blocage = "F") or                 /*  articles bloques */
        artapr.type-client = "PRO" or                                               /* Produits dangereux seulement pour professionnnel */
        artapr.usto = "BX" or                                                       /* produits conditionn�s en box  */
        lookup(artapr.tva, l-tva-interdit) <> 0                                                          /* prestations de transport   */
        then next.
                        
find tabges-art 
	where tabges-art.codsoc = soc-groupe
    and   tabges-art.etabli = " "
    and   tabges-art.typtab = "ART"
    and   tabges-art.prefix = "suit-fiche"
    and   tabges-art.codtab = artapr.articl
no-lock no-error.

if available tabges-art  
   and tabges-art.libel2[16] = "0"                                     /* produit dangereux non autoris� jardin */
   then next.

/* Debut VLE 16112010  */
CD_TRAIT = "".
/* Recherche s'il existe une taxe DIA dans ce cas le produit est non vendable Agil  */
do ind-soc = 1 to num-entries( l-soc-article ) :    
    find first tarcli-tax 
		where tarcli-tax.codsoc = entry( ind-soc, l-soc-article )
        and   tarcli-tax.motcle = "VTE"
        and   tarcli-tax.typaux = "TAX"
        and   tarcli-tax.codaux = ""
        and   tarcli-tax.articl = artapr.articl                       
        and   tarcli-tax.code-tarif = "DIA"
        and   tarcli-tax.chrono = 0
    no-lock no-error.
    if not available tarcli-tax
       then next.
       else do:
		CD_TRAIT = "NS".
        leave.
     end.
end. /* boucle sur soci�t� */

if CD_TRAIT = "NS"
	then next.
	
CD_TRAIT = "".	
/* Recherche s'il existe une taxe CVO dans ce cas le produit est non vendable Agil  */
do ind-soc = 1 to num-entries( l-soc-article ) :    
    find first tarcli-tax 
		where tarcli-tax.codsoc = entry( ind-soc, l-soc-article )
            and   tarcli-tax.motcle = "VTE"
            and   tarcli-tax.typaux = "TAX"
            and   tarcli-tax.codaux = ""
            and   tarcli-tax.articl = artapr.articl                       
            and   tarcli-tax.code-tarif = "CVO"
            and   tarcli-tax.chrono = 0
    no-lock no-error.
    if not available tarcli-tax
        then next.
        else do:
			CD_TRAIT = "NS".
            leave.
    end.
end. /* boucle sur soci�t� */
if CD_TRAIT = "NS"
	then next.
/* Fin VLE 16112010  */