/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/corsto-p.i                                                                     !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End                                                                                                          
**************************************************************************************************************/
PROCEDURE CORSTOK :
    
/*  Pour les magasins � l'�tat : 'V' ou 'X' (Inventaire valid� & non termin�) : 
    Ajout de l'�cart �ventuel d'inventaire  corsto-qte   
    Utilis� pour envoie STOCK ORISON, AGIL, QLIKVIEW                       */
    
/*  Utilise MAGASS, MAGASI, TABGES,  STOINV                                */


def input parameter  corsto-soc     as char.
def input parameter  corsto-mag     as char.
def input parameter  corsto-art     as char.
def output parameter corsto-qte     as dec.
def buffer cor-magasi               for magasi.
def var corsto-eve                  as char .
def var corsto-datfininv            as date.

corsto-qte = 0.    
corsto-eve = "INV" .
        
Find MAGASS where 
          magass.codsoc     = corsto-soc     
    and   magass.theme      = "INVMAG"
    and   magass.chrono     = 0
    and   magass.codtab     = corsto-mag  
    use-index theme  no-lock no-error.
if not available MAGASS then RETURN.

if magass.alpha[ 1 ] <> "V" and magass.alpha[ 1 ] <> "X" then RETURN. 

if magass.alpha[ 1 ] = "V"  then DO:
    FIND cor-magasi where cor-magasi.codsoc = ""                    
                    and   cor-magasi.codtab = magass.codtab
                no-lock  no-error .
    if available cor-magasi  and  cor-magasi.datfin-inv <> ?  then
        assign  corsto-eve = "INA"  
                corsto-datfininv = cor-magasi.datfin-inv .
    else RETURN.
END.
ELSE DO:   /* magass.alpha[ 1 ] = "X" */ 
    FIND TABGES where tabges.codsoc = magass.codsoc
                and   tabges.etabli = ""
                and   tabges.typtab = "PAR"
                and   tabges.prefix = "PARAMETRES"
                and   tabges.codtab = "INVENT"
                no-lock  no-error .
    if not available tabges  or  tabges.dattab[ 1 ] = ?  then  return .
    Assign corsto-eve = "INV" 
           corsto-datfininv = tabges.dattab[ 1 ] .            
END.

FIND STOINV where 
          stoinv.codsoc      = corsto-soc
    and   stoinv.magasin     = magass.codtab
    and   stoinv.date-invent = corsto-datfininv 
    and   stoinv.articl      = corsto-art        
    and   stoinv.evenement   = corsto-eve                /* INA ou INV */
no-lock  no-error .
if not available stoinv  then  RETURN.
            
corsto-qte = stoinv.qte-invent - stoinv.qte-stock .

END PROCEDURE.
