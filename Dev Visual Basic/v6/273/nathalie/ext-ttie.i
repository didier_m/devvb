/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/ext-ttie.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 33686 29-10-12!Bug!cch            !Correction utilisation auxpar.onic.commune au lieu de auxapr.onic     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - T T I E . I                            */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Tiers TRAJECTOR                                 CC 23/04/1999 */
/*==========================================================================*/

if auxapr.codsoc = "08" then wgroupe = "08".
                        else wgroupe = "01".

    FIND AUXDET
        where auxdet.codsoc = auxapr.codsoc
        and   auxdet.typaux = auxapr.typaux
        and   auxdet.codaux = auxapr.codaux
        and   auxdet.motcle = "ACT"
        and   auxdet.codtab = "000"
            no-lock no-error .

    zone = "TIE  " + string ( wgroupe , "xx" )
                   + substr(auxapr.codaux , 5, 6) + "       "
                   + string ( auxili.adres[1] , "x(32)" )
                   + "     "
                   .

    if available auxdet
    then zone = zone + string ( auxdet.calf05 , "xxx" ) + "       ".
    else zone = zone + "          " .

    zone = zone + string ( substring( auxapr.onic , 2 , 5 ) , "x(5)" )
                + "     "
                + string ( auxapr.magasin , "xxx" )
                + "  "
                .




