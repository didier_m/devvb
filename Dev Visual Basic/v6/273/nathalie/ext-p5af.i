/*==========================================================================*/
/*                           E X T - P 5 A F  . I                           */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Articles par Fournisseurs PDP V5                CC 24/04/1999 */
/*==========================================================================*/

                                                        /* Pos.  Long. */

zone = "ARTTIERS"                                       /* 001 - 008 */
       + "C"                                            /* 009 - 001 */
       + string(codsoc-trait[nsoc], "xx")               /* 010 - 002 */
       + fill(" ", 6)                                   /* 012 - 006 */
       + string(artbis.articl, "x(15)")                 /* 018 - 015 */
       + "E"                                            /* 033 - 001 */
       + fill(" ", 23)                                  /* 034 - 023 */
       + fill("0", 7)                                   /* 057 - 007 */
       + fill(" ", 2)                                   /* 064 - 002 */
       .

if artbis.type-surcondit = "O"                           /* 066 - 001 */
     then zone = zone + "O".
     else if artbis.coef-cde <> 1
             then zone = zone + "O".
             else zone = zone + "N".

zone = zone
       + string(artbis.delai, "999")                    /* 067 - 003 */
       + " "                                            /* 070 - 001 */
       + string(artbis.articl-fou, "x(15)")             /* 071 - 015 */
       .

code-tiers = trim(artbis.codaux).
{ cadra-g.i "code-tiers" "6" "0" }

zone = zone
       + string(code-tiers, "x(10)").                    /* 086 - 010 */
if artbis.codaux = "    802190" or
   artbis.codaux = "    803400" or
   artbis.codaux = "    800595" or
   artbis.codaux = "    805100"
       then zone = zone + "O".
       else zone = zone + "N".                           /* 096 - 001 */


if artbis.datcre <> ?                                   /* 097 - 008 */
   then zone = zone + string(artbis.datcre, "99999999").
   else zone = zone + fill( " ", 8 ).


if artbis.datmaj <> ?                                   /* 105 - 008 */
   then zone = zone + string(artbis.datmaj, "99999999").
   else zone = zone + fill( " ", 8 ).

zone = zone
       + fill(" ", 10)                                  /* 113 - 010 */
       + "T"                                            /* 123 - 001 */
       + "H"                                            /* 124 - 001 */
       + fill(" ", 33)                                  /* 125 - 033 */
       + "N".                                           /* 158 - 001 */

/* Modif par CCO le 20/05/2003 */
/* Si pas de r�f�rence PLATE-FORME, supprimer le lien   */
if artbis.articl-fou <> ""
   then zone = zone + " ".                              /* 159 - 001 */
   else zone = zone + "S".

FIND TABLES where tables.codsoc = ""                    /* 160 - 010 */
            and   tables.etabli = ""
            and   tables.typtab = "QTE"
            and   tables.prefix = "TYP-QUANT"
            and   tables.codtab = artbis.ucde
            no-lock no-error.

if available tables
   then zone = zone + string(artbis.ucde, "x(10)").
   else zone = zone + "UN"
                    + fill ( " ", 8 )
                    .

if artbis.coef-cde <= 0                                 /* 170 - 009 */
   then zone = zone + string((1 / artbis.coef-cde) * -1000, "999999999").
   else zone = zone + string(artbis.coef-cde * 1000, "999999999").

if artbis.type-surcondit = "F"                          /* 179 - 008 */
   then do: if artbis.coef-cde = 1
               then do:
                    zone = zone + string(artbis.surcondit * 1000 , "99999999").
                    end.
               else do:
                    if artbis.coef-cde <= 0
                     then do:
                          zone = zone + string((1 / artbis.coef-cde) * -1000,
                          "99999999").
                          end.
                     else do:
                          zone = zone + string(artbis.coef-cde * 1000,
                          "99999999").
                          end.
                    end.
        end.
   else do: if artbis.coef-cde <= 0
               then do: zone = zone + string((1 / artbis.coef-cde *
                    artbis.surcondit) * -1000, "99999999").
                    end.
               else do: zone = zone + string(artbis.coef-cde * 1000 *
                    artbis.surcondit, "99999999").
                    end.
        end.
zone = zone + "00001000".                               /* 187 - 008 */

if artbis.coef-fac <= 0                                 /* 195 - 007 */
   then zone = zone + string((1 / artbis.coef-fac ) * -100, "9999999").
   else zone = zone + string(artbis.coef-fac * 100, "9999999").
