/*==========================================================================*/
/*                           E - C D M S 1 R . P                            */
/* Edition des Commandes Plateformes                                        */
/*--------------------------------------------------------------------------*/
/* Edition Report                                                           */
/*==========================================================================*/

{ connect.i  }
{ maquette.i }
{ e-cdmspr.i }

def var rg as int .

ma-edipied = "O" .

rg = 20 .

if edit-recap then rg = 45 . /* Recap. */

{ maq-edi.i rg }

ma-edipied = "" .
ma-nblig   = 0 .
