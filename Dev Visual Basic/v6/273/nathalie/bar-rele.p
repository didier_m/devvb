/*==========================================================================*/
/*                          B A R - R E L E . P                             */
/*==========================================================================*/

{ connect.i }
{ maquette.i }

{ bar-cal0.i }

def var i       as int                                              no-undo .
/*--------------------------------------------------------------------------*/
/* chargement adresse societe                                               */

Find TABLES  where tables.codsoc = codsoc-soc
               and tables.etabli = codetb-etb
               and tables.typtab = "ETA"
               and tables.prefix = "ADRESSE"
               and tables.codtab = " "
               no-lock no-error .
if not available tables
then edit[01] = "Societe Inexistante ..." .
else do i = 1 to 5 :
    edit[ i ] = tables.libel1[ i ] .
end .


assign edit[06]  = libsoc-soc
       edit[10]  = string(date-edi, "99/99/9999" )
       edit[11]  = trim(substring( zone, 38, 10) )
       edit[12]  = substring( zone, 2, 3) .

regs-fileacc = "AUXILI" + substring( zone, 35, 3 ) .
{regs-rec.i}
assign auxili-soc = regs-soc .
FIND AUXILI  where auxili.codsoc = auxili-soc
               and auxili.typaux = substring( zone, 35 , 3 )
               and auxili.codaux = substring( zone, 38, 10 )
               no-lock no-error .
if not available auxili
then edit[ 21 ] =  "Tiers Inexistant" .
else do i = 1 to 5 :
    edit[ i + 20 ] = auxili.adres[ i ] .
end .

assign  edit[ 13 ] = string( numbon , ">>>>>>9" )
        edit[ 14 ] = exe-rel .

do i = 1 to 30 :
 { maq-maj.i  01  i  edit[i]    }
 edit[i] = " " .
end .

{ maq-edi.i  01 }
{ maq-edi.i  03 }

return .