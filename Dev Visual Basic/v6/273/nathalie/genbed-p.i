/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/genbed-p.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 321667 2020-04-07!Bug!Bernard        !modification log suite test modules                                   !
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V61 56491 20-10-15!New!pte            !Synchronisation code client.                                          !
!V61 38378 27-05-13!Bug!cch            !Correction pble lock chrono bon dans TABGES (nocom.p)                 !
!V61 22030 15-12-10!Bug!cch            !Pas de rejet si Mode de Livraison non renseign� dans la cde initiale  !
!V61 21987 14-12-10!Evo!cch            !Gestion envoi Cdes mati�res 1�res vers DECLIC (EUREA)                 !
!V52 17756 18-09-09!Evo!cch            !Ajout test prov. 'N,L,X' pour gestion rupture sur type de bon si 'REI'!
!V52 17733 07-09-09!Evo!cch            !Modifications effectu�es sur site EUREA semaine 200936                !
!V52 17649 08-06-09!Evo!cch            !Ajout gestion ENTETE.TYPBON comme crit�re de rupture sur REI          !
!V52 17645 05-06-09!Evo!cch            !Ajout gestion des commandes aliments vrac et sac                      !
!V52 17339 05-12-08!Bug!cch            !Correction pble mag-trt non reinitialis� ds proced. affect-mag zone = !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/* GENBED-0.I - GENERATION DES BONS EN S.D. + E.D. ( Projet EUREA DISTRI )    */
/*----------------------------------------------------------------------------*/
/* Procedures Communes                                  09/2006 - EUREA - 273 */
/******************************************************************************/

/* Chargement Table temporaire */
PROCEDURE CHARG-TEMPO :

    def var prov-art  as char no-undo .
    def var seuil-qte as dec  no-undo .
    def var batsil    as char no-undo .
    def var sav-prov  as char no-undo .

    if zone-charg[ 3 ] <> "" then prov-art = zone-charg[ 3 ] .

    FOR EACH INI-LIGNES where ini-lignes.codsoc =  ini-entete.codsoc
                        and   ini-lignes.motcle =  ini-entete.motcle
                        and   ini-lignes.typcom =  ini-entete.typcom
                        and   ini-lignes.numbon =  ini-entete.numbon
                        and   ini-lignes.articl <> ""
                        use-index primaire no-lock :

        FIND B-INI-LIGNES where recid( b-ini-lignes ) = recid( ini-lignes )
                          exclusive-lock no-wait no-error .

        if not available b-ini-lignes
        then do :
            mess-err = string( ini-lignes.chrono ) + libelle[ 100 ] .
            run trt-rejet .
            return .
        end .

        assign codsoc-lect = articl-soc-sd
               code-art    = ini-lignes.articl
               .
        run ctrl-artapr .
        if not available artapr then next .

        if zone-charg[ 3 ] = ""
        then do :

            if artapr.provenance = ""
            then do :
                mess-err = codsoc-lect + " " + code-art + libelle[ 59 ] .
                run trt-rejet .
                next .
            end .

            if ( artapr.provenance = prov-rem or artapr.provenance = prov-sup ) and prov-art = ""
            then do :
                mess-err = string( ini-lignes.chrono ) + libelle[ 94 ] .
                run trt-rejet .
                next .
            end .

            if not ok-trt-bon then next .

            sav-prov = prov-art .

            if artapr.provenance <> prov-rem and artapr.provenance <> prov-sup /* Si Article Remise ou Supplementation on garde la provenance article precedent */
               then if artapr.provenance = prov-edx
                    then do :         
                    seuil-qte = dec( artapr.caract[ 5 ] ) no-error .
                    if seuil-qte <> 0 and abs( ini-lignes.qte[ niveau-trt ] ) < seuil-qte
                        then prov-art = prov-edl .
                        else prov-art = prov-edd .                
                end .
                else prov-art = artapr.provenance .

            /* Ajout CCO le 19/08/2016 pour savoir si ligne r�servation ou pas (CCI)                       */
            /* Ajout CCO le 17/02/2017 pour savoir si ligne r�servation ou pas (REI)                       */

            IF ini-lignes.typcom = "CCI" or ini-lignes.typcom = "REI" then do:
                FIND ligspe 
                    where ligspe.codsoc    = ini-lignes.codsoc
                    and   ligspe.motcle    = ini-lignes.motcle + "/NEX"
                    and   ligspe.typcom    = ini-lignes.typcom
                    and   ligspe.numbon    = ini-lignes.numbon
                    and   ligspe.chrono    = ini-lignes.chrono
                    and   ligspe.ss-chrono = 10
                    no-lock no-error .
                if available ligspe and ligspe.int-1 = 1 then DO:
                   prov-art = "L".
                end.
            end.

            /* Ajout CCO le 30/09/2016 pour forcer provenance N pour le type de pi�ce RET            */
            /* Ajout CCO le 08/11/2017 sauf pour les provenances L                                   */

            IF ini-lignes.typcom = "RET" and artapr.provenance <> "L" then prov-art = "N".

            /* Ajout CCO le 14/10/2015 pour CIZERON produits sac suivant la meme filiere que le Vrac */
            /* On se sert de la zone Batiment et Silo si elle contient VR et AC                      */
            /*        LIGSPE  ==> "58" "VTE/NEX" "CCI" "0000830000" 10 10 VR,AC  */

            IF ini-lignes.typcom = "CCI" and artapr.provenance = prov-eus then DO:  
                batsil = "".
                if available ligspe then DO:
                    /* On constitue dans batsil le mot VRAC : VR + AC  */
                    batsil = TRIM(entry( 1, ligspe.alpha-10 )) + 
                             TRIM(entry( 2, ligspe.alpha-10 )) no-error .
                end. 

                if batsil = "VRAC" then DO:
                    /* Si on a un article pr�c�dent, on peut tester sav-prov             */
                    /* pour faire suivre suivant la provenance du produit Vrac pr�c�dent */
                    /* Provenance F Cizeron Vrac (prov-civ) ou B Atrial Vrac (prov-euv)  */
                    /* Sinon on dit que c'est du Cizeron Vrac                            */

                    if sav-prov = prov-civ or sav-prov = prov-euv then prov-art = sav-prov .   
                                                                  else prov-art = prov-civ .
                END.

            END. 

            if prov-art = prov-blk /* Bulk : analyse mag. force ds fiche article */
            then do :
                case ini-lignes.typcom :
                    when typcom-cci then ind = 17 . /* Commande Client E.D.      */
                    when typcom-rei then ind = 20 . /* Commande de Reappro. E.D. */
                end .
                zone = "4" .
                run affect-mag .
            end .

        end . /* if zone-charg[ 3 ] = "" */

        CREATE LIGNES-TRAV .

        assign lignes-trav.provenance = prov-art
               lignes-trav.tenusto    = artapr.tenusto
               lignes-trav.magasin    = mag-trt
               lignes-trav.chrono     = ini-lignes.chrono
               lignes-trav.recid-enr  = recid( ini-lignes )
               .

       /* REI prov. N ou L : affectation du Type de Commande ( Standard / Morte saison ) */
       if ini-lignes.typcom = typcom-rei and ( prov-art = prov-edd or prov-art = prov-edl )
        then do :

            if ini-entete.typbon <> ""

            then lignes-trav.typbon = ini-entete.typbon .

            else do :

                if artapr.famart = "" or artapr.soufam = ""
                then do :
                    mess-err = codsoc-lect + " " + code-art + libelle[ 101 ] .
                    run trt-rejet .
                    next .
                end .

                FIND TABGES where tabges.codsoc = ""
                            and   tabges.etabli = ""
                            and   tabges.typtab = "ART"
                            and   tabges.prefix = "SOUFAM" + artapr.famart
                            and   tabges.codtab = artapr.soufam
                            no-lock no-error .

                if not available tabges
                then do :
                    mess-err = artapr.famart + " " + artapr.soufam + libelle[ 102 ] + codsoc-lect + " " + code-art + " ..." .
                    run trt-rejet .
                    next .
                end .

                if ( tabges.libel3[ 7 ] <> "" and tabges.libel3[ 8 ] <> "" ) or
                   ( tabges.libel3[ 9 ] <> "" and tabges.libel3[ 10 ] <> "" )
                then do :

                    zone = string( day( ini-lignes.datbon ), "99" ) + string( month( ini-lignes.datbon ), "99" ) .

                    if ( tabges.libel3[ 7 ] <> "" and tabges.libel3[ 8 ] <> "" and
                         zone >= tabges.libel3[ 7 ] and zone <= tabges.libel3[ 8 ] ) or
                       ( tabges.libel3[ 9 ] <> "" and tabges.libel3[ 10 ] <> "" and
                         zone >= tabges.libel3[ 9 ] and zone <= tabges.libel3[ 10 ] )

                       then lignes-trav.typbon = typbon-ms .

                       else lignes-trav.typbon = typbon-std .

                end .

                else lignes-trav.typbon = typbon-std .

            end . /* else do : */

        end . /* if ini-lignes.typcom = typcom-rei */

        else lignes-trav.typbon = "" .

    END . /* FOR EACH INI-LIGNES */

    if num-entries( l-mag-trt ) = 1 then mag-fab = l-mag-trt . /* Nbre mag. fab. force = 1 -> forcage du meme mag. sur tte la cde */

END PROCEDURE . /* CHARG-TEMPO */

/* Chargement no de bon */
PROCEDURE CHARG-NO-BON :

    REPEAT TRANSACTION :

        run nocom.p( rech-typcom, today, 2, output numbon-trt ) .
        numbon-trt = numbon-trt + "00" .

        if typcom-trt <> typcom-cfe-ed /* <> Commande Fabrication E.D. */
        then do :

            FIND ENTETE where entete.codsoc = codsoc-trt
                        and   entete.motcle = motcle-trt
                        and   entete.typcom = typcom-trt
                        and   entete.numbon = numbon-trt
                        no-lock no-error .

            if available entete then next .

            FIND FIRST BASTAT where bastat.codsoc = codsoc-trt
                              and   bastat.motcle = motcle-trt
                              and   bastat.typcom = typcom-trt
                              and   bastat.numbon = numbon-trt
                              use-index primaire no-lock no-error .

            if available bastat then next .

        end .
        else do : /* Commande Fabrication E.D. */

            FIND FIRST LIGNES where lignes.codsoc = codsoc-trt
                              and   lignes.motcle = motcle-ach
                              and   lignes.typcom = typcom-cfe-ed
                              and   lignes.numbon = numbon-trt
                              use-index primaire no-lock no-error .

            if available lignes then next .

            FIND FIRST LIGNES where lignes.codsoc = codsoc-trt
                              and   lignes.motcle = motcle-vte
                              and   lignes.typcom = typcom-cfs-ed
                              and   lignes.numbon = numbon-trt
                              use-index primaire no-lock no-error .

            if available lignes then next .

        end .

        leave .

    END . /* REPEAT TRANSACTION */

END PROCEDURE . /* CHARG-NO-BON */

/* Controle Tiers Gestion */
PROCEDURE CTRL-AUXAPR :

    FIND AUXAPR where auxapr.codsoc = codsoc-lect
                and   auxapr.typaux = typaux-trt
                and   auxapr.codaux = codaux-trt 
                no-lock no-error .

    if not available auxapr
    then do :
        mess-err = codsoc-lect + " " + typaux-trt + " " + codaux-trt + libelle[ 64 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-AUXAPR */

/* Controle Tiers Compta. */
PROCEDURE CTRL-AUXILI :

    regs-fileacc = "AUXILI" + typaux-trt .
    { regs-rec.i }

    FIND AUXILI where auxili.codsoc = regs-soc
                and   auxili.typaux = typaux-trt
                and   auxili.codaux = codaux-trt 
                no-lock no-error .

    if not available auxili
    then do :
        mess-err = regs-soc + " " + typaux-trt + " " + codaux-trt + libelle[ 65 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-AUXILI */

/* Controle Magasin */
PROCEDURE CTRL-MAGASI :

    case zone :

        when "1" then do :

            FIND MAGASI where magasi.codsoc = codsoc-lect
                        and   magasi.codtab = code-mag
                        no-lock no-error .

            if not available magasi
            then do :
                mess-err = code-mag + libelle[ 62 ] .
                run trt-rejet .
                return .
            end .

            if magasi.typaux-cli = "" or magasi.codaux-cli = ""
            then do :
                mess-err = code-mag + " " + libelle[ 63 ] .
                run trt-rejet .
                return .
            end .

        end .

        when "2" then if magasi.mag-log = ""
                      then do :
                          mess-err = code-mag + " " + libelle[ 88 ] .
                          run trt-rejet .
                          return .
                      end .

    end case .

END PROCEDURE . /* CTRL-MAGASI */

/* Controle Article */
PROCEDURE CTRL-ARTAPR :

    FIND ARTAPR where artapr.codsoc = codsoc-lect
                and   artapr.articl = code-art
                no-lock no-error .

    if not available artapr or code-art = ""
    then do :
        mess-err = codsoc-lect + " " + code-art + libelle[ 58 ] .
        run trt-rejet .
    end .

    if code-art = "" and available artapr then RELEASE ARTAPR .

END PROCEDURE . /* CTRL-ARTAPR */

/* Controle Article Vente */
PROCEDURE CTRL-ARTBIS-VTE :

    FIND ARTBIS where artbis.motcle = "VTE"
                and   artbis.codsoc = codsoc-lect
                and   artbis.articl = code-art
                and   artbis.typaux = ""    
                and   artbis.codaux = ""
                no-lock no-error .

    if not available artbis
    then do :
        mess-err = codsoc-lect + " " + code-art + libelle[ 66 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-ARTBIS-VTE */

/* Controle Article Achat */
PROCEDURE CTRL-ARTBIS-ACH :

    /* zone = "1" -> inexistant accepte */

    FIND ARTBIS where artbis.motcle = "ACH"
                and   artbis.codsoc = codsoc-soc
                and   artbis.articl = code-art
                and   artbis.typaux = typaux-trt    
                and   artbis.codaux = codaux-trt
                no-lock no-error .

    if not available artbis and zone <> "1"
    then do :
        mess-err = codsoc-soc + " " + code-art + libelle[ 67 ] + typaux-trt + " " + codaux-trt + " ..." .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-ARTBIS-ACH */

/* Controle Devise */
PROCEDURE CTRL-DEVISE :

    if auxili.devise <> dev-gescom
    then do :
        mess-err = auxili.codsoc + " " + auxili.typaux + " " + auxili.codaux + " " + auxili.devise + " " +
                   libelle[ 68 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-DEVISE */

/* Affectation magasin logistique */
PROCEDURE AFFECT-MAG :

    /* Bulk CCI Entete ( zone = "3" )- Eurena Vrac CCI Entete ( zone = "5" ) */
    if zone = "3" or zone = "5"
       then if code-modliv <> ""
            then do :

                FIND TABGES where tabges.codsoc = ""
                            and   tabges.etabli = ""
                            and   tabges.typtab = "APR"
                            and   tabges.prefix = "MODLIV"
                            and   tabges.codtab = code-modliv
                            no-lock no-error .

                if not available tabges
                then do :
                    mess-err = code-modliv + libelle[ 87 ] .
                    run trt-rejet .
                    return .
                end .

                if tabges.libel2[ 5 ] <> "" and ( zone = "3" or ( zone = "5" and tabges.libel2[ 6 ] <> "" ) ) /* libel2[ 6 ] = Depart ou Franco */
                then do :
                    mag-trt = tabges.libel2[ 5 ] . /* Mag. rattache */
                    return .
                end .
                else if zone = "3" then zone = "2" .
                                   else return .

            end .

            else case zone :

                when "3" then zone = "2" .

                when "5" then return .

            end case .

    /* Bulk REI ou CCI Entete sans modliv ou sans mag. bulk associe au modliv */
    if zone = "2"
    then do :

        FIND MAGASS where magass.codsoc = magasi-soc-sd
                    and   magass.codtab = code-mag
                    and   magass.theme  = "SUITE-FICHE"
                    and   magass.chrono = 0
                    no-lock no-error .

        if not available magass
        then do :
            mess-err = code-mag + libelle[ 89 ] .
            run trt-rejet .
            return .
        end .

        if magass.alpha[ 3 ] = ""
        then do :
            mess-err = code-mag + libelle[ 93 ] .
            run trt-rejet .
            return .
        end .

        mag-trt = magass.alpha[ 3 ] . /* Mag. bulk */

        return .

    end .

    /* Bulk CCI ou REI Lignes */
    if zone = "4"
    then do :

        FIND TABGES where tabges.codsoc = articl-soc-ed
                    and   tabges.etabli = ""
                    and   tabges.typtab = "ART"
                    and   tabges.prefix = "SUIT-FICHE"
                    and   tabges.codtab = code-art
                    no-lock no-error .

        if available tabges and tabges.libel2[ ind ] <> "" and num-entries( tabges.libel2[ ind ] ) = 1
        then do :
            mag-trt = right-trim( tabges.libel2[ ind ] ) . /* Mag. Bulk Article */
            if l-mag-trt = "" then l-mag-trt = mag-trt .
                              else if lookup( mag-trt, l-mag-trt ) = 0 then l-mag-trt = l-mag-trt + "," + mag-trt .
        end .

        else mag-trt = "" .

        return .

    end .

    /* ELF E.D. stockes */
    FIND TABGES where tabges.codsoc = articl-soc-ed
                and   tabges.etabli = ""
                and   tabges.typtab = "ART"
                and   tabges.prefix = "SUIT-FICHE"
                and   tabges.codtab = code-art
                no-lock no-error .

    if not available tabges or tabges.libel2[ ind ] = ""
       then return . /* Mag. Log. principal */

    case num-entries( tabges.libel2[ ind ] ) :

        when 1 then do :

            case tabges.libel2[ ind ] :

                /* Mag. Log. du mag. demandeur */
                when "LOG" then if mag-log = ""
                                then do :
                                    assign codsoc-lect = magasi-soc-sd
                                           zone        = "2"
                                           .
                                    run ctrl-magasi .
                                    if not ok-trt-bon then return .
                                    mag-trt = magasi.mag-log .
                                end .

                                else mag-trt = mag-log .

                otherwise mag-trt = tabges.libel2[ ind ] . /* Mag. Log. Article */

            end case .

        end . /* when 1 */

        otherwise do :

            FIND MAGASS where magass.codsoc = magasi-soc-sd
                        and   magass.codtab = code-mag
                        and   magass.theme  = "SUITE-FICHE"
                        and   magass.chrono = 0
                        no-lock no-error .

            if not available magass
            then do :
                mess-err = code-mag + libelle[ 89 ] .
                run trt-rejet .
                return .
            end .

            if magass.alpha[ 1 ] = ""
            then do :
                mess-err = code-mag + libelle[ 90 ] .
                run trt-rejet .
                return .
            end .

            ind-2 = 0 .
            ind-2 = int( magass.alpha[ 1 ] ) no-error . /* Region Magasin */
            if ind-2 = 0 or ind-2 = ?
            then do :
                mess-err = magass.alpha[ 1 ] + libelle[ 91 ] + code-mag + " ..." .
                run trt-rejet .
                return .
            end .

            zone = "" .
            zone = entry( ind-2, tabges.libel2[ ind ] ) no-error .
            if zone = ""
            then do :
                mess-err = string( ind-2 ) + libelle[ 92 ] + code-mag + " / " +
                           articl-soc-ed + " " + code-art + " ..." .
                run trt-rejet .
                return .
            end .  

            case zone :

                /* Mag. Log. du mag. demandeur */
                when "LOG" then if mag-log = ""
                                then do :
                                    assign codsoc-lect = magasi-soc-sd
                                           zone        = "2"
                                           .
                                    run ctrl-magasi .
                                    if not ok-trt-bon then return .
                                    mag-trt = magasi.mag-log .
                                end .

                                else mag-trt = mag-log .

                otherwise mag-trt = zone . /* Mag. Log. Article */

            end case .

        end . /* otherwise do : */

    end case . /* case num-entries( tabges.libel2[ ind ] ) : */

    mag-trt = right-trim( mag-trt ) .
    if l-mag-trt = "" then l-mag-trt = mag-trt .
                      else if lookup( mag-trt, l-mag-trt ) = 0 then l-mag-trt = l-mag-trt + "," + mag-trt .

END PROCEDURE . /* AFFECT-MAG */

/* Lecture du CAF */
PROCEDURE LECT-CAF :

    FIND LIGNES where recid( lignes ) = recid-ligne exclusive-lock .

    FIND FIRST {tarcli.i} where {tarcli.i}.motcle     = motcle-caf
                          and   {tarcli.i}.codsoc     = codsoc-ed
                          and   {tarcli.i}.typaux     = ""
                          and   {tarcli.i}.codaux     = ""
                          and   {tarcli.i}.articl     = lignes.articl
                          and   {tarcli.i}.code-tarif = codpri-caf
                          and   {tarcli.i}.valeur     > 0
                          use-index primaire no-lock no-error .

    if available {tarcli.i} 
    then do :

        val-dec = {tarcli.i}.valeur .

        if lignes.coef-cde < 0 then val-dec = - val-dec / lignes.coef-cde .
        if lignes.coef-cde > 0 then val-dec = val-dec * lignes.coef-cde .
        if lignes.coef-fac < 0 then val-dec = - val-dec * lignes.coef-fac .
        if lignes.coef-fac > 0 then val-dec = val-dec / lignes.coef-fac .

        assign lignes.codpri  = codpri-caf
               lignes.pu-brut = val-dec
               lignes.pu-net  = val-dec
               .

        if zone = "1" then assign xd-lignes.codlig-type = top-0-n
                                  xd-entete.prx-attente = top-0-n
                                  .

    end .

    else do :

        FIND ENTETE where recid( entete ) = recid-entete exclusive-lock .

        assign lignes.codpri      = codpri-attente
               lignes.codlig-type = top-3-n
               entete.prx-attente = top-1-n
               .

        if zone = "1" then assign xd-lignes.pu-brut    = 0
                                  xd-lignes.pu-net     = 0
                                  xd-lignes.rem-codrem = ""
                                  xd-lignes.rem-typrem = ""
                                  xd-lignes.rem-typapp = ""
                                  xd-lignes.rem-px     = 0
                                  xd-lignes.rem-mont   = 0
                                  .

    end .

END PROCEDURE . /* LECT-CAF */

/* Traitement des Rejets */
PROCEDURE TRT-REJET :

    if nb-err = 0
    then do :
        output stream s-fic-rej to value( fic-rej ) .
        mess-lib = chr( 27 ) + "&l1O" + "/* " + /* Mise a l'italienne */
                   libelle[ 23 ] + string( today ) + " " + string( time, "hh:mm:ss" ) .
        put stream s-fic-rej unformatted mess-lib skip( 1 ) .

        MESSAGE "rejet " mess-lib .
        mess-lib = "/* " + libelle[ 24 ] + fic-rej .
        put stream s-fic-rej unformatted mess-lib skip( 1 ) .
        MESSAGE "rejet " mess-lib .
    end .

    if not maj-bon
       then if ind = 53 then mess-lib = libelle[ ind ] .
                        else mess-lib = codsoc-soc + libelle[ ind ] .
       else mess-lib = ini-entete.codsoc + " " + ini-entete.motcle + " " + ini-entete.typcom + " " +
                       ini-entete.numbon + libelle[ 55 ] .

    put stream s-fic-rej unformatted mess-lib skip .
    put stream s-fic-rej unformatted mess-err skip( 1 ) .

    MESSAGE "rejet " mess-lib .
    MESSAGE "rejet " mess-err .

    assign nb-err     = nb-err + 1
           ok-trt-bon = no
           .

END PROCEDURE . /* TRT-REJET */

