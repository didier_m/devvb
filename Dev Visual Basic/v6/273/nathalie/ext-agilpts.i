/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/ext-agilpts.i                                                              !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 61120 04-10-16!Evo!pha            !report modifs 273                                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/


/*==========================================================================*/
/*                           E X T - A G I L P T S  . I                     */
/* Extraction des donnees pour AGIL                                         */
/*--------------------------------------------------------------------------*/
/* Extraction Des points acquis dans les LIM                                */
/*==========================================================================*/
/* VLE 02062016 ajout du code magasin dans le libelle motif de points       */
/*==========================================================================*/


auxdet-soc = "".
if substr(entspe.alpha-1, 1, 3) = "291"
	then auxdet-soc = "08".
	
	find auxdet 
	     where auxdet.codsoc = auxdet-soc
		 and auxdet.motcle = "CFI"
		 and   auxdet.typaux = ""
		 and   auxdet.codaux = ""
		 and auxdet.codtab = entspe.alpha-1
	no-lock no-error.
	
	if available auxdet then do :
	
		if auxdet.calf14 = "" 
            then 
               if auxdet.codsoc = "08"
                    then ID_CONTACT = trim(auxdet.codsoc) + substr(auxdet.codtab, 6, 8).
                    else ID_CONTACT = "01" + substr(auxdet.codtab, 6, 8).
            else ID_CONTACT= entry(2, auxdet.calf14).

        NO_CARTE = entspe.alpha-1.
		DT_POINTS = string(entspe.date-1, "99/99/9999").
		HR_POINTS = entspe.heucre.
		NB_POINTS_DEAL = entspe.int-1.
		CD_MOTIF_POINTS = "".
		LB_MOTIF_POINTS = "points LIM " + substr(entspe.numbon, 1, 8)
                                 + substr(entspe.alpha-2, 1, 3).
	
	put stream agil unformatted
           Type_Ligne_DEB Nom_Section_Deb "~011" ID_CONTACT "~011" NO_CARTE "~011" DT_POINTS "~011" 
                        HR_POINTS "~011" TY_POINTS "~011" NB_POINTS_DEAL "~011" CD_MOTIF_POINTS "~011" LB_MOTIF_POINTS "~015" skip.

    put stream agil unformatted 
            Type_Ligne_Fin Nom_Section_Fin "~015" skip. 
	end.
	

