/*==========================================================================*/
/*                          B A R - R E A 2 . I                             */
/*==========================================================================*/
/*           Zones utilise pour le module edition des ruptures              */
/*                Rupture-a               : Niveau de rupture en cours      */
/*                Rupture-nb              : Nombre de rupture existante     */
/*                Rupture-etat = "t"      : Edition du titre de la rupture  */
/*                Rupture-etat = "r"      : Edition de la rupture           */
/*--------------------------------------------------------------------------*/

DO WHILE rupture-etat = "T" and rupture-a = 1 :
    leave .
end .

/* Entete Magasin    */
/*===================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 2  :

    if substring( zone , 2 , 3 ) <> " " then do :
        hide message . pause 0 .
        message "  Traitement du Magasin ...: "
                "  " substring( zone , 2 , 3 ) .
    end .

    assign
           no-magasin = substring( zone , 2 , 3 )
           lib-magasin = " "
           nb-adh =  0
           nb-usa =  0
           tot-ach = 0
           tot-ht  = 0
           tot-tva = 0
           tot-ttc = 0
           tot-rea = 0
           .

    FIND MAGASI where magasi.codsoc = magasi-soc
                and   magasi.codtab = no-magasin
                no-lock  no-error .
    if available magasi  then  lib-magasin = magasi.libel1[ 1 ] .

    run value( ma-entete ) .

    nb-mag = nb-mag + 1 .
    leave .

END .


/*    Entete CLIENT                  */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 3 :

    assign  edit[1]  = trim(substring( zone, 38, 10) )
            edit[2]  = substring( zone,  5, 30 )
            nb-bar   = 0 .

    FIND AUXAPR where auxapr.codsoc = codsoc-soc
                  and auxapr.typaux = substring( zone, 35 , 3 )
                  and auxapr.codaux = substring( zone, 38, 10 )
                  use-index auxapr-1 no-lock no-error .
    if not available auxapr
    then do :
        message substring( zone, 35 , 3 )  "-"  substring( zone, 38, 10 )
        " : Tiers Inexistant ..." .
        bell . bell . pause . leave .
    end .

    if auxapr.type-adh = "1"
    then nb-adh = nb-adh + 1 .
    else nb-usa = nb-usa + 1 .

    leave .

END .


/*    Entete BAREMES                 */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 4 :

    assign no-bareme    = substring( zone, 48, 2)
           qte-ach      = dec( substring( zone, 62, 12) )
           qte-rea      = dec( substring( zone, 74, 12) ) .

    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "TAR"
                and   tabges.prefix = "BAREME-CLI"
                and   tabges.codtab = no-bareme
                no-lock  no-error .
    if not available tabges then leave .

    assign prix-uni = tabges.nombre[2]
           plafond  = tabges.nombre[1] .

    nb-bar = nb-bar + 1 .
    leave .

END .

/*===RUPTURES EN TOTAL=====================================================*/

/*    Total  Baremes            */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 4 :

   { maq-raz.i "04" }

   edit[3] = no-bareme .

                                         /* le pris est au quintal */
   assign mont-ttc = round(( qte-ach / 100 ) * prix-uni, 2 )
          mont-ht  = round( mont-ttc / tauxtva, 2 )
          mont-tva = mont-ttc - mont-ht .

   edit[11] = string( plafond  , ">>>>>>9.99-" ) .
   edit[12] = string( qte-ach  , ">>>>>>9.99-" ) .
   edit[13] = string( prix-uni , ">>>>9.99-"   ) .
   edit[14] = string( mont-ht  , ">>>>>>9.99-" ) .
   edit[15] = string( mont-tva , ">>>>>>9.99-" ) .
   edit[16] = string( mont-ttc , ">>>>>>9.99-" ) .
   edit[17] = string( qte-rea  , ">>>>>>9.99-" ) .

   do i = 1 to 20 :
       { maq-maj.i "04" i  edit[i] }
       edit[i] = " " .
   end .

   { maq-edi.i "04" }

   assign tot-ach = tot-ach + qte-ach
          tot-ht  = tot-ht  + mont-ht
          tot-tva = tot-tva + mont-tva
          tot-ttc = tot-ttc + mont-ttc
          tot-rea = tot-rea + qte-rea
          .

   leave .

END .

/*    Total Magasin */
/*==================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 2 :

   { maq-edi.i "07" }

   edit[1]  = "Total Depot" + " " + no-magasin + "-" + lib-magasin  .

   assign
   edit[12] = string(tot-ach  , ">>>>>>9.99-" )
   edit[14] = string(tot-ht   , ">>>>>>9.99-" )
   edit[15] = string(tot-tva  , ">>>>>>9.99-" )
   edit[16] = string(tot-ttc  , ">>>>>>9.99-" ) .
   edit[17] = string(tot-rea  , ">>>>>>9.99-" ) .

   do i = 1 to 20 :
       { maq-maj.i "05"  i  edit[i] }
       edit[i] = " " .
   end .
   { maq-edi.i "05" }

   assign edit[1] = "Nombre d'Adherents :"
          edit[2] = string( nb-adh , ">>>>9" ) .
   { maq-maj.i "06"  1  edit[1] }
   { maq-maj.i "06"  2  edit[2] }
   { maq-edi.i "06" }

   assign edit[1] = "Nombre d'Usagers   :"
          edit[2] = string( nb-usa , ">>>>9" ) .
   { maq-maj.i "06"  1  edit[1] }
   { maq-maj.i "06"  2  edit[2] }
   { maq-edi.i "06" }

   { maq-edi.i "07" }

   /* Totaux generaux */
   assign tot-gen[ 1 ] = tot-gen [ 1 ] + tot-ach
          tot-gen[ 2 ] = tot-gen [ 2 ] + tot-ht
          tot-gen[ 3 ] = tot-gen [ 3 ] + tot-tva
          tot-gen[ 4 ] = tot-gen [ 4 ] + tot-ttc
          tot-gen[ 5 ] = tot-gen [ 5 ] + tot-rea
          tot-gen[ 6 ] = tot-gen [ 6 ] + nb-adh
          tot-gen[ 7 ] = tot-gen [ 7 ] + nb-usa
          .

   leave .

END .