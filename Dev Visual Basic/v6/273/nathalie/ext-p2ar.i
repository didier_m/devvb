/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/ext-p2ar.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 15055 12-07-06!Evo!cch            !Acc�s ARTAPR avec soci�t� de regroupement suite mise en place ED      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - P 2 A R . I                            */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Articles PDP V2                                 CC 01/03/1999 */
/*==========================================================================*/

/* modif DGR 22-03-01 => nouvel enreg avant chaque ligne + quotage de zone */
    zone = '"CONCENT" "cba002" "' +
           string ( today , "99999999" ) + '" "   " "" "' +
           string ( time,"hh:mm:ss" ) + '" "C" '.
    put stream kan-pro-v2 unformatted zone skip.

    zone = '"'.
/*    zone = "".
    zone = zone + "002       01 "
                + string ( artapr.articl , "x(6)" )
                . */
    zone = zone + string ( artapr.articl , "x(6)" ).

/* fin modif DGR  22-03-01 */

    if artapr.code-blocage = "O"  then zone = zone + "O" .
    else if artapr.code-blocage = "F"  then zone = zone + "F" .
         else if artapr.code-blocage = "I"  then zone = zone + "I" .
              else if artapr.code-blocage = "B"  then zone = zone + "B" .
                   else zone = zone + "O" .

    zone = zone + string ( substring ( artapr.famart , 2 , 2 ) , "xx" )
                + string ( substring ( artapr.soufam , 2 , 2 ) , "xx" )
                + string ( artapr.stat-rayon , "x" ).
    if artapr.codsoc = "08" and artapr.libart <> ""
    then assign zone = zone +
         string ( artapr.libart , "x(30)" ).
    else zone = zone +
         string ( substring ( artapr.libart1[1] , 1 , 30 ) , "x(30)" ).

    zone = zone + string ( substring ( artapr.code-cumul , 2 , 3 ) , "xxx" ).

          if artapr.tva = "004" then zone = zone + "4" .
    else  if artapr.tva = "003" then zone = zone + "2" .
    else  if artapr.tva = "005" then zone = zone + "2" .
    else  if artapr.tva = "002" then zone = zone + "1" .
    else  if artapr.tva = "001" then zone = zone + "3" .
    else  zone = zone + "0" .
    if artapr.toxicite <> "" then zone = zone + "1" .
                             else zone = zone + "0" .
    if artapr.negatif = "1" then
                                       zone = zone + "O" .
                            else  zone = zone + "N" .
    /*                                                                  */
    /*    modif du 16.12.96 (cco) Prix modifiable                       */
    /*                                                                  */
    if artapr.prx-modif = "0" then
              zone = zone + "9999" .
        else
              zone = zone + string ( artapr.fourche-px * 100 , "9999" ) .
    /*                                                                  */
    /*    modif du 16.10.95 (gfa) acces artbis quand pls fourniss.      */
    /*                            pour un meme article                  */
    if artapr.provenance = "2" then
       find first b-artbis use-index artbis-1
                  where b-artbis.codsoc = codsoc-trait[nsoc]
                  and b-artbis.motcle = "ACH"
                  and b-artbis.articl = artapr.articl
                  and b-artbis.typaux = "TIE"
                  and b-artbis.codaux = "    802190"
                  no-lock no-error .

    if artapr.provenance = "5" then
       find first b-artbis use-index artbis-1
                  where b-artbis.codsoc = codsoc-trait[nsoc]
                  and b-artbis.motcle = "ACH"
                  and b-artbis.articl = artapr.articl
                  and b-artbis.typaux = "TIE"
                  and b-artbis.codaux = "    800595"
                  no-lock no-error .

    if artapr.provenance = "F" then
       find first b-artbis use-index artbis-1
                  where b-artbis.codsoc = codsoc-trait[nsoc]
                  and b-artbis.motcle = "ACH"
                  and b-artbis.articl = artapr.articl
                  and b-artbis.typaux = "TIE"
                  and b-artbis.codaux = "    803400"
                  no-lock no-error .

    if artapr.provenance = "V" then
       find first b-artbis use-index artbis-1
                  where b-artbis.codsoc = codsoc-trait[nsoc]
                  and b-artbis.motcle = "ACH"
                  and b-artbis.articl = artapr.articl
                  and b-artbis.typaux = "TIE"
                  and b-artbis.codaux = "    805100"
                  no-lock no-error .

    if ( artapr.provenance <> "2" ) and ( artapr.provenance <> "5" ) and
       ( artapr.provenance <> "F" ) and ( artapr.provenance <> "V" ) then
       find first b-artbis use-index artbis-1
                  where b-artbis.codsoc = codsoc-trait[nsoc]
                  and b-artbis.motcle = "ACH"
                  and b-artbis.articl = artapr.articl
                  and b-artbis.typaux = "TIE"
                  no-lock no-error .
       if available b-artbis
       then do:
           if b-artbis.articl-fou <> "" then
              zone = zone + string ( b-artbis.articl-fou , "xxxxxx" ) + "02" .
           else zone = zone + "00000002" .
       end .
       else do:
          zone = zone + "00000002" .
       end .
    if artapr.tenusto = "1" then zone = zone + "2" .
                            else zone = zone + "1" .
    zone = zone + string ( int(artapr.rist-adherent[1]) , "9" ) .
    find artbis use-index artbis-1
                where artbis.codsoc = artapr.codsoc
                  and artbis.motcle = "VTE"
                  and artbis.articl = artapr.articl
                  and artbis.typaux = ""
                  and artbis.codaux = "" no-lock no-error .
    if not available artbis
    then do : message " Article inexistant dans ARTBIS VTE " artapr.codsoc
                                                         artapr.articl .
              pause 0 .
              next .
         end .
    zone = zone + string ( artbis.ucde , "xx" )
                + string ( artapr.bareme , "x(10)" ).

    /*                                                                       */
    /*    modif du 15.03.01 (DGR) recuperation codbar.code-barre le + recent */
    /*                                                                       */
    codtab = "".
    for each codbar
    use-index article
    where codbar.codsoc = soc-articl
    and   codbar.articl = artapr.articl
    no-lock
    by codbar.datmaj descending by codbar.heumaj descending :
       codtab = codbar.code-barre.
       leave.
    end.

    /* zone = zone + string ( artapr.gencod , "x(13)" ) */

    zone = zone + string ( codtab, "x(13)" )
                + string ( substring ( artapr.articl-rempl[1],1,6), "x(6)" )
                .

    /* b-artbis <=> donn�es achats pour motcle = "ACH" */
    if available b-artbis
    then do :
        if b-artbis.coef-cde <= 0
        then zone = zone +
             string ( ( 1 / b-artbis.coef-cde) * -1000 , "9999999" ) .
        else do:
          zone = zone +
              string ( b-artbis.coef-cde * 1000 , "9999999" ). end .
        zone = zone + string ( b-artbis.surcondit * 100 , "999999" )
                   /* + string ( b-artbis.type-surcondit , "x" ) .*/
                   + "F".
        end.
    else do :
        zone = zone + string ( 1000 , "9999999" )
                    + string ( 100 , "999999" )
                    + "F".
    end.
    if available artbis
    then do :
        if artbis.coef-fac <= 0
           then zone = zone + string ( artbis.coef-fac * -100 , "999999") .
           else zone = zone + string ( ( 1 / artbis.coef-fac ) * 100 , "999999") .
        zone = zone + string ( artbis.surcondit * 100 , "999999" )
                    + "00000" .
    end.
    else do :
         zone = zone + "000100"
                     + "000000"
                     + "00000" .
    end.

    /* Traitement special blocage et provenance modif du 27/03/96 par CCO */
    if artapr.code-blocage = "I" or artapr.code-blocage = "F"
       then zone = zone + "9".
       else if artapr.provenance <> ""
               then zone = zone + string ( artapr.provenance , "x" ) .
               else zone = zone + "0" .

/* DGR 30/01/02: specifique Craponne */
    if artapr.codsoc = "01"
       then case artapr.provenance :
        when "2" or when "8" then
          overlay(zone,121,6) = "802190".
        when "3" then
          overlay(zone,121,6) = "999003".
        when "4" then
          overlay(zone,121,6) = "999004".
        when "5" then
          overlay(zone,121,6) = "800595".
        when "9" then
          overlay(zone,121,6) = "999009".
        when "F" then
          overlay(zone,121,6) = "803400".
        when "V" then
          overlay(zone,121,6) = "805100".
        otherwise
          overlay(zone,121,6) = "999001".
       end case.
/* DGR 30/01/02: fin specifique Craponne */

/* DGR  22-03-01 quotage de zone*/
    zone = zone + '" '.
/* fin ajout DGR  22-03-01 quotage de zone*/