/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/v-arf-2.i                                                                    !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 17180 25-09-08!Evo!bp             !ajout reseau blocage et provenance                                    !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*---------------------------------------------------------*/
/*     Module Recherche des articles d'un fournisseur      */
/*                   Include d'affichage                   */
/*                      v-arf-2.i                          */
/*---------------------------------------------------------*/

rec-clef = artbis.articl .

assign codsoc = ""
       codtab = rec-clef 
       libart1 = "" 
       reseau = ""
       blocage = ""
       provenance = "" 
       .

FIND ARTAPR where artapr.codsoc = articl-soc
            and   artapr.articl = codtab
            no-lock no-error .


if available artapr then 
    ASSIGN libart1 = artapr.libart1[mem-langue] 
           reseau  = substr( artapr.caract [ 6 ] , 1 , 3 )
           .

FIND ARTAPR where artapr.codsoc = "45"
            and   artapr.articl = codtab
            no-lock no-error .
if available artapr 
    then ASSIGN blocage = blocage + STRING( artapr.code-blocage  , "x" ) +   "." 
                provenance = provenance + STRING( artapr.provenance  , "x" ) +   "."
                .
    ELSE ASSIGN blocage = blocage +   " ." 
                provenance = provenance +   " ." 
                .
FIND ARTAPR where artapr.codsoc = "01"
            and   artapr.articl = codtab
            no-lock no-error .
if available artapr 
    then ASSIGN blocage = blocage + STRING( artapr.code-blocage  , "x" )  +   "."
                provenance = provenance + STRING( artapr.provenance  , "x" ) +   "."
                .
    ELSE ASSIGN blocage = blocage +   " ." 
                provenance = provenance +   " ." 
                .
FIND ARTAPR where artapr.codsoc = "08"
            and   artapr.articl = codtab
            no-lock no-error .
if available artapr 
    then ASSIGN blocage = blocage + STRING( artapr.code-blocage  , "x" ) +   " "
                provenance = provenance + STRING( artapr.provenance  , "x" )
                .

display rec-clef libart1 artbis.articl-fou reseau blocage provenance with frame fr-rech-art .
