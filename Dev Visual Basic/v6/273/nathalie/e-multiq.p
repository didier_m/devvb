/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/src/e-multiq.p                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 10702 27-09-04!Evo!cch            !Initialisation date minimum par d�faut                                !
!V52 10643 22-09-04!Evo!cch            !Ajout option Type de tri ( Nomenclature ou Libell� ARTICLE ) + ajout c!
!V52 10002 23-07-04!New!cch            !D�veloppement Etats des Stocks Articles Multipro.                     !
!-------------------------------------------------------------------------------------------------------------!
!                             I N C L U D E S                !                    T A B L E S                 !
!------------------------------------------------------------+------------------------------------------------!
!connect.i             !                                     !tabges                                          !
!aide.i                !                                     !                                                !
!e-multi0.i            !                                     !                                                !
!chx-opt.f             !                                     !                                                !
!compo.f               !"new"                                !                                                !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !tabges                                          !
!aide.i                !                                     !                                                !
!e-multi0.i            !                                     !                                                !
!chx-opt.f             !                                     !                                                !
!compo.f               !"new"                                !                                                !
!compo.i               !                                     !                                                !
!fncompo.i             !"type-edit" "fr-saisie"              !                                                !
!z-cmp.i               !"type-edit" "' '"                    !                                                !
!chx-opt.i             !"10" "libel" "9" "50" "52" "with titl!                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E - M U L T I Q . P                            */
/* Etat des Stocks Articles Multipro                                        */
/*--------------------------------------------------------------------------*/
/* Saisie des parametres                                      CC 15/07/2004 */
/*==========================================================================*/

{ connect.i        }
{ aide.i           }
{ e-multi0.i       }
{ chx-opt.f        }
{ compo.f    "new" }

def var sav-periph as char no-undo .

/* Saisie zones */
assign compo-vecteur = "<01>,10,20,30,40,50,60,70"
       compo-bloque  = no
       sav-periph    = periph
       .

SAISIE :
REPEAT :

    { compo.i }

    /* Type Edition */
    REPEAT WHILE COMPO-MOTCLE = 10 WITH FRAME FR-SAISIE :

        if type-edit = 0 then type-edit = int( entry( 1, libelle[ 31 ] ) ) .

        { fncompo.i "type-edit" "fr-saisie" }
        { z-cmp.i   "type-edit" "' '"       }

        if lookup( string( type-edit ), libelle[ 31 ] ) = 0
        then do :
            bell . bell .
            message type-edit libelle[ 22 ] .
            readkey pause 2 .
            undo, retry .
        end .

        lib-edit = entry( type-edit, libelle[ 32 ] ) .
        display lib-edit .

        case string( type-edit ) :

            when entry( 1, libelle[ 31 ] )
            then do :
                assign compo-bloque[ 30 ] = no
                       compo-bloque[ 40 ] = no
                       compo-bloque[ 50 ] = yes
                       familles           = ""
                       fam-exclus         = ""
                       .
                display familles .
            end .

            when entry( 2, libelle[ 31 ] )
            then do :
                assign compo-bloque[ 30 ] = yes
                       compo-bloque[ 40 ] = yes
                       compo-bloque[ 50 ] = no
                       date-min           = ?
                       date-max           = ?
                       .
                display date-min date-max .
            end .

        end case .

        next SAISIE .

    END . /* REPEAT : 10 */

    /* Type de Tri Article */
    REPEAT WHILE COMPO-MOTCLE = 20 WITH FRAME FR-SAISIE :

        if type-tri = 0 then type-tri = int( entry( 1, libelle[ 31 ] ) ) .

        { fncompo.i "type-tri" "fr-saisie" }
        { z-cmp.i   "type-tri" "' '"       }

        if lookup( string( type-tri ), libelle[ 31 ] ) = 0
        then do :
            bell . bell .
            message type-tri libelle[ 22 ] .
            readkey pause 2 .
            undo, retry .
        end .

        lib-tri = entry( type-tri, libelle[ 40 ] ) .
        display lib-tri .

        next SAISIE .

    END . /* REPEAT : 20 */

    /* Date Minimum */
    REPEAT WHILE COMPO-MOTCLE = 30 WITH FRAME FR-SAISIE :

        if date-min = ? then date-min = 01/01/2000 .

        { fncompo.i "date-min" "fr-saisie" }
        { z-cmp.i   "date-min" "' '"       }

        if date-min = ?
        then do :
            bell . bell .
            message date-min libelle[ 23 ] .
            readkey pause 2 .
            undo, retry .
        end .

        next SAISIE .

    END . /* REPEAT : 30 */

    /* Date Maximum */
    REPEAT WHILE COMPO-MOTCLE = 40 WITH FRAME FR-SAISIE :

        if date-max = ? then date-max = today .

        { fncompo.i "date-max" "fr-saisie" }
        { z-cmp.i   "date-max" "' '"       }

        if date-max = ? or date-max < date-min
        then do :
            bell . bell .
            message date-max libelle[ 23 ] .
            readkey pause 2 .
            undo, retry .
        end .

        next SAISIE .

    END . /* REPEAT : 40 */

    /* Familles */
    REPEAT WHILE COMPO-MOTCLE = 50 WITH FRAME FR-SAISIE :

        fam-exclus = "N" .
        if familles = "" then familles = libelle[ 36 ] .

        { fncompo.i "familles" "fr-saisie" }
        { z-cmp.i   "familles" "' '"       }

        if keyfunction( lastkey ) = "FIND"
        then do :
            assign codsoc = ""
                       etabli = ""
                       typtab = "ART"
                       prefix = "FAMART"
                       libel  = ""
                   .
                run v-tab-ss.p .
                codtab = right-trim( codtab ) .
            if codtab = ? then undo, retry .
            if lookup( codtab, familles ) <> 0
            then do :
                bell . bell .
                message codtab libelle[ 24 ] .
                readkey pause 2 .
                undo, retry .
            end .
            if familles = "" then familles = codtab .
                             else familles = familles + "," + codtab .
            next .
        end .

        if familles = ""
        then do :
            fam-exclus = "" .
            leave .
        end .

        if substr( familles, 1, 2 ) = "E:"
           then assign fam-exclus               = "O"
                       substr( familles, 1, 2 ) = ""
                       .

        if fam-exclus = "O" and familles = ""
        then do :
            bell . bell .
            message familles libelle[ 24 ] .
            readkey pause 2 .
            undo, retry .
        end .

        do i = 1 to num-entries( familles ) :

            codtab = entry( i, familles ) .

            FIND TABGES where tabges.codsoc = ""
                        and   tabges.etabli = ""
                        and   tabges.typtab = "ART"
                        and   tabges.prefix = "FAMART"
                        and   tabges.codtab = codtab
                        no-lock no-error .

            if not available tabges
            then do :
                bell . bell .
                message codtab libelle[ 25 ] .
                readkey pause 2 .
                undo, retry .
            end .

        end .

        next SAISIE .

    END . /* REPEAT : 50 */

    /* Maquette */
    SAI-MAQUETTE :
    REPEAT WHILE COMPO-MOTCLE = 60 WITH FRAME FR-SAISIE :

        libel = libelle[ 50 + type-edit ] .
        display trim( substring( libel, 16, 30 ) ) @ lib-maq .
        maquette = trim( substring( libel, 1, 12 ) ) .

        { fncompo.i "maquette" fr-saisie }
        { z-cmp.i   "maquette" "' '"     }

        if keyfunction( lastkey ) <> "FIND"
        then do :

            maquette = trim( maquette ) .
            if length( maquette ) < 7 or length( maquette ) > 12
            then do :
                bell . bell .
                message maquette libelle[ 26 ] .
                readkey pause 2 .
                undo , retry .
            end .

            do i = 51 to 60 :
                libel = libelle[ i ] .
                if maquette = substring( libel, 1, 12 )
                then do :
                    display trim( substring( libel, 16, 30 ) ) @ lib-maq .
                    next SAISIE .
                end .
            end .

            bell . bell .
            message maquette libelle[ 27 ] .
            readkey pause 2 .
            undo, retry .

            next SAISIE .

        end .

        /* Recherche maquettes */
        assign libel =  ""
               j     = 0
               .

        do i = 51 to 60 :
            if libelle[ i ] <> "" then
                assign libel = libel + "," + libelle[ i ]
                       j     = j + 1
                       .
        end .

        if j <> 10
        then do j = j to 10 :
            libel = libel + ", " .
        end .

        substr( libel, 1, 1 ) = "" .

        REPEAT :

            assign chx-def    = 1
                   chx-delete = 99
                   chx-milieu = 14
                   chx-color  = "message"
                   .

            { chx-opt.i  "10"  "libel"  "9"  "50"  "52"
                         "with title libelle[ 15 ]"
                         "ecr-maq" "color normal" }

            if keyfunction( lastkey ) = "END-ERROR"
               then undo SAI-MAQUETTE, retry SAI-MAQUETTE .
            if keyfunction( lastkey ) <> "RETURN" then undo, retry .

            leave .

        END .

        maquette = trim( substring( libelle[ chx-trait + 50 ], 1, 12 ) ) .

        display maquette .

        next SAISIE .

    END . /* REPEAT : 60 */

    /* Fichier edition */
    REPEAT WHILE COMPO-MOTCLE = 70 WITH FRAME FR-SAISIE :

        { fncompo.i "periph" "fr-saisie" }
        { z-cmp.i   "periph" "' '"       }

        periph = trim( periph ) .
        if length( periph ) < 7 or length( periph ) > 12
        then do :
            bell . bell .
            message periph libelle[ 28 ] .
            readkey pause 2 .
            periph = sav-periph .
            undo, retry.
        end .

        sav-periph = periph .

        next SAISIE .

    END . /* REPEAT : 70 */

END . /* SAISIE */