/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/st-vma70.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 13696 23-11-05!Evo!cch            !Ajout nom spécifique du spool si maquette pour MONARCH                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                          S T - V M A 7 0 . I                             */
/*     Edition Statistique des Ventes des Magasins par Activite             */
/*     Definitions des variables et des frames                              */
/*==========================================================================*/

def {1} shared var no-act        as int     format "9"               no-undo .
def {1} shared var list-act      as char                             no-undo .
def {1} shared var libelle       as char                 extent 99   no-undo .
def {1} shared var valeur        as char                 extent 49   no-undo .

def {1} shared var mat-edi       as log                  extent 49   no-undo .
def {1} shared var masque        as char                 extent 49   no-undo .
def {1} shared var masque-tot    as char                 extent 49   no-undo .

def {1} shared var tim-deb       as int                              no-undo .
def {1} shared var tim-fin       as int                              no-undo .
def {1} shared var tim-tot       as dec format "99999.999"           no-undo .

def {1} shared var tot-bon       as int                  extent 249  no-undo .
def {1} shared var tot-ca        as dec                  extent 249  no-undo .

def {1} shared var lib-tot       as char                             no-undo .
def {1} shared var ind-tot       as int                              no-undo .
def {1} shared var nba-mag       as int                              no-undo .
def {1} shared var nba-tot       as int                              no-undo .
def {1} shared var nba-lig       as int                              no-undo .

def {1} shared var lib-periode   as char                             no-undo .
def {1} shared var lib-act       as char   format "x(22)"           no-undo .
def {1} shared var lib-titre     as char                             no-undo .
def {1} shared var no-magasin    as char  format  "xxx"              no-undo .
def {1} shared var lib-magasin   as char  format  "x(30)"            no-undo .

/* variables pour la saisie */
def {1} shared var date-deb     as date format "99/99/9999"         no-undo .
def {1} shared var date-fin     as date format "99/99/9999"         no-undo .
def {1} shared var date-debex   as date format "99/99/9999"         no-undo .
def {1} shared var magasins     as char format "x(40)"              no-undo .
def {1} shared var maquette     as char format "x(12)"              no-undo .
def {1} shared var periph       as char format "x(22)"              no-undo .
def {1} shared var chemin       as char format "x(18)"              no-undo .

def {1} shared var lib-maq      as char format "x(35)"              no-undo .

def {1} shared var mag-exclus   as char  format  "xx"               no-undo .

def {1} shared var date-deb1    as date format "99/99/9999"         no-undo .
def {1} shared var date-fin1    as date format "99/99/9999"         no-undo .
def {1} shared var date-dex1    as date format "99/99/9999"         no-undo .

def {1} shared var sav-periph   as char format "x(12)"              no-undo .
def {1} shared var z-choix      as char                             no-undo .
def {1} shared var zone         as char                             no-undo .

def {1} shared var nb-lus       as int                              no-undo .
def {1} shared var nb-trt       as int                              no-undo .
def {1} shared var nb-run       as int                              no-undo .


/*------------------------------------------------------------------------*/
def {1} shared frame fr-saisie .

form libelle[ 11 ] format "x(26)"  date-deb                 skip
     libelle[ 12 ] format "x(26)"  date-fin                 skip
     libelle[ 13 ] format "x(26)"  date-debex               skip
     libelle[ 14 ] format "x(26)"  magasins                 skip
     libelle[ 15 ] format "x(26)"  maquette     lib-maq     skip
     libelle[ 16 ] format "x(26)"  periph                   skip
     libelle[ 17 ] format "x(26)"  chemin
     with frame fr-saisie
     row 3  centered  overlay  no-label  { v6frame.i } .