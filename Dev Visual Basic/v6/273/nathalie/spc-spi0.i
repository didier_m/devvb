/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/spc-spi0.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 11790 04-01-05!Evo!cch            !Suppression contr�les suppl�mentation par rapport � LOIRALIMENT       !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!                      !                                     !lignes                                          !
!                      !                                     !artapr                                          !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                     ***   S P C - S P I 0 . I   ***                      */
/*                Appele par sai-lic.p, sai-lgqt.i                          */
/*==========================================================================*/

if b-lignes.codlig = "S"  then
REPEAT :

    if b-articl.articl-usine <> "U"
    then do :
        bell . bell .
        message b-lignes.articl ": Cet Article n'est pas une Supplementation ..." .
        readkey pause 2 .
        supl-inco = "ERROR" .
        leave .
    end .

    FIND LAST LIGNES where lignes.codsoc = b-lignes.codsoc
                     and   lignes.motcle = b-lignes.motcle
                     and   lignes.typcom = b-lignes.typcom
                     and   lignes.numbon = b-lignes.numbon
                     and   lignes.chrono < b-lignes.chrono
                     and   lignes.codlig = "N"
                     use-index primaire no-lock no-error .

    if not available lignes
    then do :
        bell . bell .
        message "Pas de Ligne 'N' saisie precedemment ..." .
        readkey pause 2 .        
        supl-inco = "ERROR" .
    end .

    supl-inco = "X" .

    leave .

END .
