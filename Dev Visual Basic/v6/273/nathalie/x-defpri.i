/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/x-defpri.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 11260 08-11-04!Evo!cch            !Adaptation interfaces pour passage en V52                             !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/* Calcul des prix brut , prix net et chargement des remises */

/* Chargement du prix brut */                                         
run calprx-a.p( entete.motcle ,                                       
                entete.typaux + entete.codaux ,                       
                lignes.articl ,                                       
                entete.dattar ,                                       
                entete.codpri ,                                       
                entete.devise ,                                       
                output lignes.codpri ,                                
                output lignes.pu-brut ) .                             

assign lignes.pu-net = lignes.pu-brut                                      
       artapr-recid  = recid ( artapr )                                
       entete-recid  = recid ( entete )                                
       lignes-recid  = recid ( lignes )                                
       .                                                              

run x-calr.p .        /* Chargement des remises */                 
{ calprx-n.i lignes } /* Calcul du prix net     */       