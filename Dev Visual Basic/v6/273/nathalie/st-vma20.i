/*------------------------------------------------------------------------
    File        : st-vma20.i
    Purpose     : Edition Statistique des Ventes et Marges / Famille/Magasin
    Description : Definitions des variables et des frames  
    
    Created     : 
    Notes       : 
----------------------------------------------------------------------*/
/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/st-vma20.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 217907 2018-04-06!Bug!Jean Baptiste C![GC]Probl�mes compilation eurea : modifs programmes et base de donn�es!
!V61 26406 20-09-11!Bug!cch            !Correction probl�me totaux � z�ro                                     !
!V61 26390 19-09-11!Evo!cch            !Suite Fiche V61352 - Supprerssion question "Mois du PAMP"             !
!V61 26360 16-09-11!Evo!cch            !Ajout affichage date dernier arr�t� de stocks                         !
!V61 26332 15-09-11!Evo!cch            !Ajout �dition ann�e N - 2                                             !
!V61 19476 23-06-10!Evo!cch            !Aggrandissement zone de saisie des Magasins                           !
!V52 17461 18-02-09!Bug!cch            !Correction mauvais acc�s � la ss ss famille 2                         !
!V52 17330 03-12-08!Bug!cch            !Correction pble sur ruptures : ajout utilisation d'un stream          !
!V52 17293 06-11-08!Evo!cch            !Gesrtion nomenclature 2 article ( EUREA )                             !
!V52 15438 14-11-06!Evo!cch            !Initialisation de la variable 'AJUST' � 'NO' au lieu de 'YES'         !
!V52 14290 14-02-06!Evo!cch            !Optimisation temps de traitement                                      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                          S T - V M A 2 0 . I                             */
/*          Edition Statistique des Ventes et Marges / Famille/Magasin      */
/*          Definitions des variables et des frames                         */
/*==========================================================================*/
def {1} shared var z-libel     as char                           no-undo .
def {1} shared var z-libel2    as char                           no-undo .
def {1} shared var z-nombre1   as dec                            no-undo .
def {1} shared var z-nombre2   as dec                            no-undo .
def {1} shared var val-art     as dec     extent 6               no-undo .
def {1} shared var qte-art     as dec     extent 6               no-undo .
def {1} shared var ca-pam      as dec     extent 6               no-undo .
def {1} shared var ca-pamp     as dec     extent 6               no-undo .
def {1} shared var marge-pam   as dec     extent 6               no-undo .
def {1} shared var marge-pamp  as dec     extent 6               no-undo .
def {1} shared var pamp        as dec     extent 6               no-undo .
def {1} shared var pam         as dec     extent 6               no-undo .
def {1} shared var pamp-m      as dec                            no-undo .
def {1} shared var val-arr     as dec     extent 6               no-undo .
def {1} shared var val-pamp    as dec                            no-undo .
def {1} shared var stock-arr   as dec     extent 6               no-undo .
def {1} shared var mot-cle     as char                           no-undo .
def {1} shared var list-mag    as CHAR                           no-undo .
def {1} shared var art-rgp     like  artapr.articl               no-undo .
def {1} shared var soc-articl  as char                           no-undo .
def {1} shared var tot-val-per as dec                            no-undo .
def {1} shared var tot-val-ex  as dec                            no-undo .

DEFINE {1} shared VARIABLE mm AS INTEGER     NO-UNDO.


def var date-dern-arr          as date format "99/99/9999"       no-undo .

def {1} shared var ii          as int                            no-undo .     
def {1} shared var jj          as int                            no-undo .
def {1} shared var kk          as int                            no-undo .
def {1} shared var ll          as int                            no-undo .

def {1} shared var lg-sfa      as int                            no-undo .
def {1} shared var lg-cat      as int                            no-undo .

def {1} shared var libelle     as char                extent 99  no-undo .
def {1} shared var valeur      as char                extent 100 no-undo .

def {1} shared var mat-edi     as log                 extent 100 no-undo .
def {1} shared var mat-pmp     as log                 extent 6   no-undo .
def {1} shared var masque      as char                extent 100 no-undo .
def {1} shared var masque-tot  as char                extent 100 no-undo .

def {1} shared var statut-taux as char format "x"                no-undo .
def {1} shared var taux        as dec                 extent 6   no-undo .
def {1} shared var tot-qte     as dec                 extent 450 no-undo .
def {1} shared var tot-sto     as dec                 extent 419 no-undo .
def {1} shared var tot-val     as dec                 extent 419 no-undo .
def {1} shared var ind-tot     as int                            no-undo .
def {1} shared var lib-tot     as char                           no-undo .

def {1} shared var lib-periode as char                           no-undo .
def {1} shared var lib-catego  as char                           no-undo .
def {1} shared var lib-famille as char                           no-undo .
def {1} shared var lib-titre   as char                           no-undo .
def {1} shared var lib-tit0    as char                           no-undo .
def {1} shared var no-article  as char format "x(10)"            no-undo .
def {1} shared var no-famille  as char format "xxx"              no-undo .
def {1} shared var no-catego   as char format "xxx"              no-undo .
def {1} shared var no-magasin  as char format "xxx"              no-undo .
def {1} shared var lib-magasin as char format "x(30)"            no-undo .

def {1} shared var date-arr    as date                           no-undo .
def {1} shared var date-arrm1  as date                           no-undo .
def {1} shared var date-arrm2  as date                           no-undo .
def {1} shared var date-tar    as date                           no-undo .
def {1} shared var date-tarm1  as date                           no-undo .
def {1} shared var date-tarm2  as date                           no-undo .

def {1} shared var mois-debcp  as char format "x(6)"             no-undo .
def {1} shared var mois-fincp  as char format "x(6)"             no-undo .
def {1} shared var mois-debex  as char format "x(6)"             no-undo .
/*
def {1} shared var mois-pamp   as log  format "0/1"              no-undo .
def {1} shared var lib-pamp    as char format "x(20)"            no-undo .
*/
def {1} shared var magasins    as char format "x(49)"            no-undo .
def {1} shared var type-tri    as char format "x"                no-undo .
def {1} shared var lib-tri     as char format "x(14)"            no-undo .
def {1} shared var familles    as char format "x(40)"            no-undo .
def {1} shared var ajust       as log  format "O/N"              no-undo .
def {1} shared var lib-ajust   as char format "x(4)"             no-undo .
def {1} shared var maquette    as char format "x(12)"            no-undo .
def {1} shared var lib-maq     as char format "x(35)"            no-undo .
def {1} shared var chemin      as char format "x(18)"            no-undo .
def {1} shared var periph      as char format "x(22)"            no-undo .

def {1} shared var fam-exclus  as char format  "xx"              no-undo .
def {1} shared var mag-exclus  as char format  "xx"              no-undo .

def {1} shared var mois-deb    as int                            no-undo .
def {1} shared var anee-deb    as int                            no-undo .
def {1} shared var mois-fin    as int                            no-undo .
def {1} shared var anee-fin    as int                            no-undo .
def {1} shared var mois-dex    as int                            no-undo .
def {1} shared var anee-dex    as int                            no-undo .

def {1} shared var mois-debm1  as int                            no-undo .
def {1} shared var mois-debm2  as int                            no-undo .
def {1} shared var mois-finm1  as int                            no-undo .
def {1} shared var mois-finm2  as int                            no-undo .
def {1} shared var mois-dexm1  as int                            no-undo .
def {1} shared var mois-dexm2  as int                            no-undo .
def {1} shared var annee-min   as char                           no-undo .
def {1} shared var annee-max   as char                           no-undo .

def {1} shared var sav-periph  as char                           no-undo .
def {1} shared var z-choix     as char                           no-undo .
def {1} shared var zone        as char                           no-undo .

/* dgr 24/05/2002 : rajout 2 variables pour calcul indrot dans st-vma21.p */ 
def {1} shared var mois-deb1   as int                            no-undo .
def {1} shared var mois-fin1   as int                            no-undo .

def {1} shared var indrot      as int                            no-undo .
def {1} shared var nb-lus      as int                            no-undo .
def {1} shared var nb-trt      as int                            no-undo .
def {1} shared var nb-run      as int                            no-undo .

def {1} shared var pre-sfa     as char format "x(6)"             no-undo .
def {1} shared var pre-cat     as char format "x(5)"             no-undo .
def {1} shared var pre-fam     as char format "x(3)"             no-undo .
def {1} shared var rup-statut  as char format "x(2)"             no-undo .
def {1} shared var pre-ust     as char format "x(3)"             no-undo .

def {1} shared var nba-sfa     as int                            no-undo .
def {1} shared var nba-ust     as int                            no-undo .
def {1} shared var nba-cat     as int                            no-undo .
def {1} shared var nba-fam     as int                            no-undo .
def {1} shared var nba-tot     as int                            no-undo .

def {1} shared var mois        as int                            no-undo .
def {1} shared var exercice    as char                           no-undo .
def {1} shared var exercm1     as char                           no-undo .
def {1} shared var exercm2     as char                           no-undo .

def {1} shared var list-motcle as char                           no-undo .

def {1} shared var ind-nomenc  as char                           no-undo .
def {1} shared var prefix-ssf  as char                           no-undo .

def stream kan-tri .

def {1} shared frame fr-saisie .

form libelle[ 11 ] format "x(27)" mois-debcp space ( 13 ) libelle[ 01 ] format "x(18)" date-dern-arr skip
     libelle[ 12 ] format "x(27)" mois-fincp                                                         skip       
     libelle[ 13 ] format "x(27)" mois-debex                                                         skip
  /* libelle[ 14 ] format "x(27)" mois-pamp lib-pamp                                                 skip */
     libelle[ 15 ] format "x(27)" magasins                                                           skip
     libelle[ 09 ] format "x(27)" type-tri  lib-tri                                                  skip
     libelle[ 16 ] format "x(27)" familles                                                           skip
     libelle[ 17 ] format "x(27)" ajust lib-ajust                                                    skip
     libelle[ 18 ] format "x(27)" maquette lib-maq                                                   skip
     libelle[ 19 ] format "x(27)" periph                                                             skip
     libelle[ 10 ] format "x(27)" chemin
     with frame fr-saisie
     row 3 centered overlay no-label {v6frame.i} .


