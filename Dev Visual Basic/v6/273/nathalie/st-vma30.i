/*==========================================================================*/
/*                          S T - V M A 3 0 . I                             */
/*          Edition Statistique des Quantites Vendues / Semestre            */
/*==========================================================================*/

def {1} shared var libelle       as char                 extent 99   no-undo .
def {1} shared var valeur        as char                 extent 59   no-undo .
def {1} shared var valeur1       as char                 extent 6    no-undo .

def {1} shared var mat-edi       as log                  extent 59   no-undo .
def {1} shared var mat-pmp       as log                  extent 5    no-undo .
def {1} shared var masque        as char                 extent 59   no-undo .
def {1} shared var masque-tot    as char                 extent 59   no-undo .

def {1} shared var tot-val       as dec                  extent 310  no-undo .
def {1} shared var tot-qte       as dec                  extent 310  no-undo .
def {1} shared var tot-sto       as dec                  extent 310  no-undo .
def {1} shared var tot-q-per     as dec                  extent 310  no-undo .

def {1} shared var ind-tot       as int                              no-undo .
def {1} shared var lib-tot       as char                             no-undo .

def {1} shared var lib-periode   as char                             no-undo .
def {1} shared var lib-mois      as char format  "x(9)"              no-undo .
def {1} shared var lib-annee     as char                             no-undo .
def {1} shared var lib-famille   as char                             no-undo .
def {1} shared var lib-catego    as char                             no-undo .
def {1} shared var lib-ssf       as char                             no-undo .
def {1} shared var lib-magasin   as char                             no-undo .
def {1} shared var lib-titre     as char                             no-undo .
def {1} shared var lib-tit0      as char                             no-undo .
def {1} shared var lib-tit1      as char                             no-undo .
def {1} shared var no-famille    as char  format  "xxx"              no-undo .
def {1} shared var no-catego     as char  format  "xxx"              no-undo .
def {1} shared var no-magasin    as char  format  "xxx"              no-undo .
def {1} shared var list-mag      as char                             no-undo .

def {1} shared var date-arr      as date format "99/99/9999"         no-undo .
def {1} shared var date-arrm1    as date format "99/99/9999"         no-undo .

def {1} shared var mois-debcp   as char format "x(6)"               no-undo .
def {1} shared var mois-fincp   as char format "x(6)"               no-undo .
def {1} shared var mois-debex   as char format "x(6)"               no-undo .
def {1} shared var magasins     as char format "x(40)"              no-undo .
def {1} shared var familles     as char format "x(40)"              no-undo .
def {1} shared var maquette     as char format "x(12)"              no-undo .
def {1} shared var periph       as char format "x(12)"              no-undo .
def {1} shared var opt-edi      as char format "x"                  no-undo .

def {1} shared var lib-maq      as char format "x(30)"              no-undo .

def {1} shared var fam-exclus   as char  format  "xx"               no-undo .
def {1} shared var mag-exclus   as char  format  "xx"               no-undo .

def {1} shared var mois-deb     as int                              no-undo .
def {1} shared var anee-deb     as int                              no-undo .
def {1} shared var mois-fin     as int                              no-undo .
def {1} shared var anee-fin     as int                              no-undo .
def {1} shared var mois-dex     as int                              no-undo .
def {1} shared var anee-dex     as int                              no-undo .
def {1} shared var mois-fix     as int                              no-undo .
def {1} shared var anee-fix     as int                              no-undo .

def {1} shared var mois-debm1   as int                              no-undo .
def {1} shared var mois-finm1   as int                              no-undo .
def {1} shared var mois-dexm1   as int                              no-undo .
def {1} shared var mois-fixm1   as int                              no-undo .

def {1} shared var mois-lu      as int                              no-undo .
def {1} shared var anee-lu      as int                              no-undo .
def {1} shared var list-mois    as char                             no-undo .

def {1} shared var sav-periph   as char format "x(12)"              no-undo .
def {1} shared var z-choix      as char                             no-undo .
def {1} shared var zone         as char                             no-undo .

def {1} shared var nb-lus       as int                              no-undo .
def {1} shared var nb-trt       as int                              no-undo .
def {1} shared var nb-run       as int                              no-undo .

def {1} shared var pre-fam      as char  format "x(3)"              no-undo .
def {1} shared var pre-sfa      as char  format "x(6)"              no-undo .
def {1} shared var pre-ssf      as char  format "x(9)"              no-undo .
def {1} shared var rup-statut   as char  format "x(2)"              no-undo .

def {1} shared var nba-fam      as int                              no-undo .
def {1} shared var nba-sfa      as int                              no-undo .
def {1} shared var nba-ssf      as int                              no-undo .
def {1} shared var nba-tot      as int                              no-undo .

def {1} shared var statut-taux   as char    format "x"               no-undo .
def {1} shared var taux          as dec                  extent 5    no-undo .


/*--------------------------------------------------------------------------*/
def {1} shared frame fr-saisie .

form libelle[ 11 ] format "x(28)"  mois-debcp               skip
     libelle[ 12 ] format "x(28)"  mois-fincp               skip
     libelle[ 13 ] format "x(28)"  mois-debex               skip
     libelle[ 18 ] format "x(28)"  opt-edi                  skip
     libelle[ 14 ] format "x(28)"  magasins                 skip
     libelle[ 15 ] format "x(28)"  familles                 skip
     libelle[ 16 ] format "x(28)"  maquette     lib-maq     skip
     libelle[ 17 ] format "x(28)"  periph
     with frame fr-saisie
     row 3  centered  overlay  no-label  { v6frame.i } .