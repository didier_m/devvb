/*==========================================================================*/
/*                          C A P - E T A 0 . I                             */
/*       GESTION du CAPITAL        : EDITION DES CA & DES REMISES ADHERENTS */
/*       Gestion des variables communes                                     */
/*==========================================================================*/

def {1} shared var valeur        as char                 extent 20   no-undo .

def {1} shared var lib-cadre     as char                            no-undo .

def {1} shared var periph       as char format "x(12)"              no-undo .
def {1} shared var sav-periph   as char format "x(12)"              no-undo .
def {1} shared var nom-ope      as char format "x(12)"              no-undo .

def {1} shared var y-execpt    as char format  "x(4)"               no-undo .

/* zones maquettes */
def {1} shared var no-magasin    as char  format  "xxx"              no-undo .
def {1} shared var lib-magasin   as char  format  "x(30)"            no-undo .
def {1} shared var no-client     as char  format  "xxx"              no-undo .
def {1} shared var lib-client    as char  format  "x(30)"            no-undo .
def {1} shared var maquette      as char format "x(12)"              no-undo .

def {1} shared var z-choix      as char                             no-undo .