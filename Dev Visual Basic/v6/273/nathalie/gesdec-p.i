/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/gesdec-p.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 321667 2020-04-07!Bug!Bernard        !modification log suite test modules                                   !
! 315208 2020-02-14!Bug!p-o teyssier   !Report correction Mr Constant                                         !
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V61 55108 29-06-15!New!pte            !Ajout des Messages de debug de Mr CONSTANT                            !
!V61 47384 12-06-14!Bug!pte            !Gestion de l'incrementation du numero de fichier                      !
!V61 44082 21-01-14!Evo!jcc            !Re�criture du compteur de fichier d�s le d�part                       !
!V61 42996 03-12-13!Evo!cch            !Ajout gestion fichiers numerot�s pour Tiers DECLIC - PRIOS            !
!V61 42918 29-11-13!Evo!cch            !Ajout VALIDATE + RELEASE pour appel depuis le WEB                     !
!V61 42771 25-11-13!Evo!cch            !Suite Fiche V61 42766                                                 !
!V61 42766 25-11-13!Evo!cch            !Extraction Cdes : Changement r�gle de g�n�ration dans 2 fichiers      !
!V61 42305 05-11-13!Evo!cch            !Mise au point �volution �volutions E187 et E188 - EUREA               !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/* GESDEC-P.I - GESTION DES ECHANGES DE DONNEES NATHALIE / DECLIC             */
/*----------------------------------------------------------------------------*/
/* Procedures communes                              02/04/2002 - SOPRES - 273 */
/******************************************************************************/

/* Chargement parametres de base */
PROCEDURE CHARG-PARAM :

    assign typtab-trt = "DEC"
           prefix-trt = "GES-DECLIC"
           codtab-trt = "PARAMETRES"
           trt-ok     = no
           .

    /* Chargement aide et texte */
    if not program-name( 3 ) begins "gesdec-5"
    then do :
        aide-fichier = "gesdec-0.aid" . aide-par = "<TRAITEMENTS>" .
        run aide-lec.p .
        do i-i = 1 to 200 :
            aide-m = "libel" + string( i-i, "999" ) .
            { aide-lib.i }
            libelle[ i-i ] = aide-lib .
        end .
    end .

    /* Acces au parametrage des echanges de donnees informatisees */
    assign typtab = typtab-trt
           prefix = prefix-trt
           codtab = codtab-trt
           .
    run ch-paray.p( output o-param ) .

    if o-param = ""
    then do :
        bell . bell .
        message libelle[ 42 ] skip libelle[ 43 ] skip libelle[ 44 ]
                view-as alert-box error .
        return .
    end .

    assign repert-depart   = entry( 1,  o-param, "|" )        /* Rep. export fichiers                     */
           repert-arriv    = entry( 2,  o-param, "|" )        /* Rep. import fich.                        */
           repert-traite   = entry( 3,  o-param, "|" )        /* Rep. sav fichiers                        */
           repert-rejet    = entry( 4,  o-param, "|" )        /* Rep. rejets fichiers                     */
           repert-liste    = entry( 5,  o-param, "|" )        /* Rep. mouchards fich.                     */
           repert-trav     = entry( 6,  o-param, "|" )        /* Rep. de travail                          */
           operat-maj      = entry( 7,  o-param, "|" )        /* Operateur MAJ                            */
           nomimp          = entry( 8,  o-param, "|" )        /* Imprimante                               */
           l-type-trt      = entry( 9,  o-param, "|" )        /* Types de traitements                     */
           l-fic           = entry( 10, o-param, "|" )        /* Noms des Fichiers                        */
                          /* entry( 11, o-param, "|" )           Cpteur Extraction Mvts Apports           */
           nbj-ecar-date   = int( entry( 12, o-param, "|" ) ) /* Nb. jours ecart date                     */
           pcent-tol-taf   = int( entry( 13, o-param, "|" ) ) /* Pourcentage tol�rance ctrl solde qte TAF */
           coef-taf        = int( entry( 14, o-param, "|" ) ) /* Coefficient quantite TAF integree        */
           l-extens        = entry( 31, o-param, "|" )        /* Extensions fichiers                      */
           l-soc           = entry( 32, o-param, "|" )        /* Societes APPROS                          */
           l-tiers         = entry( 33, o-param, "|" )        /* Fournisseurs                             */
           l-motcle        = entry( 34, o-param, "|" )        /* Mots cles                                */
           l-typcom        = entry( 35, o-param, "|" )        /* Type Pieces Commandes                    */
           l-type-art      = entry( 36, o-param, "|" )        /* Types Articles                           */
           l-condit-art    = entry( 37, o-param, "|" )        /* Condit. Vte Articles                     */
           l-soc-dec       = entry( 38, o-param, "|" )        /* Societes pour DECLIC                     */
           l-tiers-l-dec   = entry( 39, o-param, "|" )        /* Tiers Livres DECLIC                      */
           l-tiers-f-dec   = entry( 40, o-param, "|" )        /* Tiers Factures DECLIC                    */
           l-mag           = entry( 41, o-param, "|" )        /* Magasins                                 */
           l-modenlev      = entry( 42, o-param, "|" )        /* Corresp. Modes Enlevement DEAL -> DECLIC */
           l-modliv        = entry( 43, o-param, "|" )        /* Modes Livraison B.L.                     */
           l-par-compta    = entry( 44, o-param, "|" )        /* Parametres Compta.                       */
           l-usine         = entry( 45, o-param, "|" )        /* Codes Usines                             */
           l-art-palette   = entry( 46, o-param, "|" )        /* Codes Articles Palette, Container, ...   */
           l-par-pfac      = entry( 47, o-param, "|" )        /* Param. Pieds Factures                    */
           l-soc-appo      = entry( 48, o-param, "|" )        /* Societes APPORTS                         */
           l-codmvt[ 1 ]   = entry( 49, o-param, "|" )        /* Codes Mvt extraction APPORTS             */
           l-soc-test      = entry( 50, o-param, "|" )        /* Societes de Test                         */
           typaux-def      = entry( 51, o-param, "|" )        /* Type Tiers par defaut                    */
           l-cereale[ 1 ]  = entry( 52, o-param, "|" )        /* Codes + Lib. Cereales                    */
           l-cereale[ 2 ]  = entry( 53, o-param, "|" )        /* Codes + Lib. Cereales                    */
           l-cereale[ 3 ]  = entry( 54, o-param, "|" )        /* Codes + Lib. Cereales                    */
           l-cereale[ 4 ]  = entry( 55, o-param, "|" )        /* Codes + Lib. Cereales                    */
           l-typcom-liv    = entry( 56, o-param, "|" )        /* Type Pieces Livraisons                   */
           l-mag-dec       = entry( 57, o-param, "|" )        /* Magasins DECLIC                          */
           l-mag-nat       = entry( 58, o-param, "|" )        /* Magasins NATHALIE                        */
           l-par-tarif     = entry( 59, o-param, "|" )        /* Parametres Tarifs                        */
           l-tiers-f-dec-2 = entry( 60, o-param, "|" )        /* Tiers Factures DECLIC suite              */
           /*              = entry( 61, o-param, "|" )           LIBRE                                    */
           l-soc-art       = entry( 62, o-param, "|" )        /* Societes APPROS Articles                 */
           l-signe         = entry( 63, o-param, "|" )        /* Signe autorisee sur valeur               */
           l-codmvt[ 2 ]   = entry( 64, o-param, "|" )        /* Codes Mvt integration APPORTS            */
           mag-echcer      = entry( 65, o-param, "|" )        /* Code Magasin echange Cereales APPORTS    */
           l-fic           = l-fic +
                             entry( 66, o-param, "|" )        /* Suite Noms des Fichiers                  */
           l-modtrp        = entry( 67, o-param, "|" )        /* Modes Transports a envoyer               */
           l-lieu-charg    = entry( 68, o-param, "|" )        /* Lieux Chargement a envoyer               */
           l-type-trt      = l-type-trt + 
                             entry( 69, o-param, "|" )        /* Suite Types de traitements               */                         
           l-soc-adl       = entry( 70, o-param, "|" )        /* Societes Adresses de Livraison           */
                          /* entry( 71, o-param, "|" )           Cpteur Extration Commandes CIZERON       */
                          /* entry( 72, o-param, "|" )           Cpteur Extraction Commandes ATRIAL       */
                          /* entry( 73, o-param, "|" )           Cpteur Extraction Tiers - ext-don1.p     */
           l-art-sansprix  = entry( 92, o-param, "|" )        /* Codes Articles sans prix                 */
           .

    if typaux-def = "" then typaux-def = "TIE" .

    if program-name( 2 ) begins "gesdec-s" or
       program-name( 2 ) begins "gesdec-t"
       then zone-charg[ 1 ] = entry( 1, l-type-trt ) .
       else if operat-maj <> "" then operat = operat-maj .

    assign motcle-vte   = "VTE"
           motcle-ach   = "ACH"
           date-j       = string( today, "999999" )                  + "-" +
                          substr( string( time, "hh:mm:ss" ), 1, 2 ) +
                          substr( string( time, "hh:mm:ss" ), 4, 2 ) +
                          substr( string( time, "hh:mm:ss" ), 7, 2 )
           type-trt     = zone-charg[ 1 ]
           ind-trt      = lookup( type-trt, l-type-trt )
           ext-dat      = entry( 1, l-extens )
           ext-trt      = entry( 3, l-extens )
           ext-rej      = entry( 4, l-extens )
           ext-lis      = entry( 5, l-extens )
           ext-blo[ 1 ] = entry( 6, l-extens )
           ext-blo[ 2 ] = entry( 7, l-extens ) no-error .

    if zone-charg[ 2 ] = "REJET" then trt-rejet = yes . /* Traitement de Rejets        */

    if zone-charg[ 3 ] = "SANSMAJ" then trt-maj = no .  /* Traitement sans mise a jour */
                                   else trt-maj = yes . /* Traitement avec mise a jour */

    if zone-charg[ 4 ] = "TEST" then trt-test  = yes .  /* Traitement Test             */

    if ind-trt = 1 then l-rowent = zone-charg[ 5 ] .    /* Liste Rowid entetes cdes a traiter - Execution depuis genb-cde avec Saisie Bon Web */ 

    if trt-test then assign l-soc-init = l-soc
                            l-soc      = l-soc-test
                            .

    if num-entries( l-type-trt ) >= 8 and ind-trt = 8
    then do :
        trt-ok = yes .
        return .
    end .

    nom-fic = entry( ind-trt, l-fic ) .
    if ind-trt = 1 then assign nom-fic-2 = entry( 2, nom-fic, ";" )
                               nom-fic   = entry( 1, nom-fic, ";" )
                               .
    if ind-trt = 3 then assign nom-fic-2 = entry( 8, l-fic )
                               fic-rej-2 = repert-rejet + nom-fic-2 + ext-rej + date-j
                               fic-ren-2 = repert-traite + nom-fic-2 + ext-trt + date-j
                               .

    assign fic-rej      = repert-rejet + nom-fic + ext-rej + date-j
           fic-ren      = repert-traite + nom-fic + ext-trt + date-j
           fic-lis      = repert-liste + nom-fic + ext-lis + date-j
           fic-blo[ 1 ] = repert-trav + nom-fic + ext-blo[ 1 ]
           fic-blo[ 2 ] = repert-trav + nom-fic + ext-blo[ 2 ]
           .

    /* Par Claude CONSTANT le 26/06/2015 */
    IF ind-trt = 1 AND trt-rejet = YES THEN DO:
        OUTPUT TO gesdec-charg.mouc APPEND.
        MESSAGE "gesdec-charg-param" trt-rejet zone-charg[ 2 ] PROGRAM-NAME( 1 ) PROGRAM-NAME( 2 ) STRING( TODAY, "99/99/9999" ) STRING( TIME, "hh:mm:ss" ).
        MESSAGE "Forcage trt-rejet � no" trt-rejet.
        OUTPUT CLOSE.
        trt-rejet = NO.
    END.

    if not trt-rejet
    then do :
        if ind-trt < 3 /* Extractions */
        then case ind-trt :
            when 1 then do :
                assign fic-trt   = repert-depart + nom-fic + string( no-fic, fill( "9", 6 ) ) + ext-dat .
                       fic-trt-2 = repert-depart + nom-fic-2 + string( no-fic-2, fill( "9", 6 ) ) + ext-dat .
            end .
            otherwise fic-trt = repert-depart + nom-fic + ext-dat .
        end case .
        else do :
            fic-trt = repert-arriv + nom-fic + ext-dat .
            if nom-fic-2 <> "" then fic-trt-2 = repert-arriv + nom-fic-2 + ext-dat .
        end .    
    end .
    else do :
        if fic-rejet <> "" and nom-fic-2 <> ""
           then fic-trt-2 = repert-rejet + nom-fic-2 + ext-rej + substr( fic-rejet, length( fic-rejet ) - 12, 13 ) .
        if fic-rejet = "" then fic-rejet = fic-rej .
        fic-trt = fic-rejet .
    end .

    if ind-trt = 10 or ind-trt = 11 /* Integration Adresses ou Commentaires Adresses Livraison */
    then do :
        assign  typtab = "STK"  prefix = "GES-STK_IT"  codtab = "PARAMETRES" .
        run ch-paray.p( output o-param ) .
        l-typtie-stk_it = entry( 37, o-param, "|" ) no-error . /* Types de Tiers envoyes sur STOCK_IT */    
    end .

    /* Par Claude CONSTANT le 26/06/2015 */
    IF logtrace = "O" THEN DO:
        OUTPUT TO VALUE(fichiermouc) APPEND.
        MESSAGE "Fin proc CHARG-PARAM" "fic-trt" fic-trt "fic-trt-2" fic-trt-2.
        OUTPUT CLOSE.
    END.

    trt-ok = yes .

END PROCEDURE . /* CHARG-PARAM */

/* Traitements d'initialisations Fichiers */
PROCEDURE TRT-INIT :

    def var tour-att as int no-undo .

    trt-ok = yes .

    /* Par Claude CONSTANT le 26/06/2015 */
    IF logtrace = "O" THEN DO:
        OUTPUT TO VALUE(fichiermouc) APPEND.
        MESSAGE "avant tour-att=1" ouv-cde ouv-cde-2 ind-trt "fic-trt" fic-trt "fic-trt-2" fic-trt-2.
        OUTPUT CLOSE.
    END.

    do tour-att = 1 to 13 while not ouv-cde and not ouv-cde-2 :

        /* Par Claude CONSTANT le 26/06/2015 */
        IF logtrace = "O" THEN DO:
            OUTPUT TO VALUE(fichiermouc) APPEND.
            MESSAGE "Dans tour-att=1 " ouv-cde ouv-cde-2 ind-trt.
            OUTPUT CLOSE.
        END.

        if tour-att = 13
        then do :
            bell . bell .
            message libelle[ 17 ] fic-trt skip libelle[ 44 ]
                    view-as alert-box warning .
            trt-ok = no .
            return .
        end .

        if search( fic-blo[ 1 ] ) <> ? or search( fic-blo[ 2 ] ) <> ?
        then do :
            bell . bell .
            hide message no-pause .
            message libelle[ 48 ] .
            message libelle[ 49 ] .
/*             os-command silent value ( "sleep 10" ) . */
            next .
        end .
        else leave .

    end .

    if ind-trt > 2 and search( fic-trt ) = ? /* Integrations */
    then do :
        bell . bell .
        message fic-trt libelle[ 41 ] view-as alert-box warning .
        trt-ok = no .
        return .
    end .

    if ind-trt > 2 and fic-trt-2 <> "" and search( fic-trt-2 ) = ?
    then do :
        bell . bell .
        message fic-trt-2 libelle[ 41 ] view-as alert-box error .
        trt-ok = no .
        return .
    end .

    /* Par Claude CONSTANT le 26/06/2015 */
    IF logtrace = "O" THEN DO:
        OUTPUT TO VALUE(fichiermouc) APPEND.
        MESSAGE "Pose fichier deblocage" ouv-cde ouv-cde-2 ind-trt.
        OUTPUT CLOSE.
    END.

    /* Pose du fichier de Blocage */
    if ( ind-trt = 1 and not ouv-cde and not ouv-cde-2 ) or ind-trt > 1
    then do :
        output stream s-blo to value( fic-blo[ 1 ] ) .
        output stream s-blo close .
    end .

    /* Par Claude CONSTANT le 26/06/2015 */
    IF logtrace = "O" THEN DO:
        OUTPUT TO VALUE(fichiermouc) APPEND.
        MESSAGE "Test case ind-trt et i-param" ind-trt "-" i-param.
        OUTPUT CLOSE.
    END.

    CASE ind-trt:
        /* Extraction Commandes Appros */
        WHEN 1 THEN DO:
            CASE i-param:
                WHEN "1" THEN DO: /* 31 - CIZERON */
                    OUTPUT STREAM s-fic TO VALUE( fic-trt ) APPEND.

                    /* Par Claude CONSTANT le 26/06/2015 */
                    IF logtrace = "O" THEN DO:
                        OUTPUT TO VALUE(fichiermouc) APPEND.
                        MESSAGE "output stream s-fic:" fic-trt.
                        OUTPUT CLOSE.
                    END.
                    ouv-cde = YES.
                END.

                WHEN "2" THEN DO: /* 27 - ATRIAL */
                    /* Par Claude CONSTANT le 26/06/2015 */
                    IF logtrace = "O" THEN DO:
                        OUTPUT TO VALUE(fichiermouc) APPEND.
                        MESSAGE "Avant output stream s-fic-2:" fic-trt-2.
                        OUTPUT CLOSE.
                    END.

                    OUTPUT STREAM s-fic-2 TO VALUE( fic-trt-2 ) APPEND.

                    /* Par Claude CONSTANT le 26/06/2015 */
                    IF logtrace = "O" THEN DO:
                        OUTPUT TO VALUE(fichiermouc) APPEND.
                        MESSAGE "output stream s-fic-2:" fic-trt-2.
                        OUTPUT CLOSE.
                    END.

                    ouv-cde-2 = YES.
                END.

                /* Par Claude CONSTANT le 26/06/2015 */
                OTHERWISE DO:
                    IF logtrace = "O" THEN DO:
                        OUTPUT TO VALUE(fichiermouc) APPEND.
                        MESSAGE "otherwise i-param:" i-param.
                        OUTPUT CLOSE.
                    END.
                END.
            END CASE.
        END.

        /* Extraction des Mouvements Apports de Cereales */
        when 2 then output stream s-fic to value( fic-trt ) append .

        /* Integrations */
        otherwise do :
            input stream s-fic from value( fic-trt ) .
            if fic-trt-2 <> "" then input stream s-fic-2 from value( fic-trt-2 ) .
        end .

    end case . /* case ind-trt : */

END PROCEDURE . /* TRT-INIT */

/* Traitements de fin */
PROCEDURE TRT-FIN :

    /* Edition Fichier Erreurs */
    if nb-err <> 0
    then do :
        output stream s-rej close .
        /* DGR 21/02/2017 : si traitement batch, ne plus editer les rejets car on les attrape directement par lrejl */
        if zone-charg[ 9 ] <> "BATCH" then  do:
            assign mess-lib = libelle[ 27 ]
                nom-fic  = fic-rej
               .
            run edition .
        end.
    end .

    if ind-trt < 3
    then do : /* Extractions  */

        /* Par Claude CONSTANT le 26/06/2015 */
        IF logtrace = "O" THEN DO:
            OUTPUT TO VALUE(fichiermouc) APPEND.
            MESSAGE "Fermeture des stream" ouv-cde "-" ouv-cde-2.
            OUTPUT CLOSE.
        END.

        if ouv-cde   then output stream s-fic close .
        if ouv-cde-2 then output stream s-fic-2 close .

        if opsys = "UNIX"
        then do :
            if search( fic-trt ) <> ?
            then do :
                zone = "chmod 777 " + fic-trt .
                os-command silent value( zone ) .
                if os-error <> 0
                then do :
                    bell . bell .
                    message libelle[ 177 ] skip
                            fic-trt        skip
                            libelle[ 47 ] os-error
                            view-as alert-box error .
                end .
            end .

            if fic-trt-2 <> "" and search( fic-trt-2 ) <> ?
            then do :
                zone = "chmod 777 " + fic-trt-2 .
                os-command silent value( zone ) .
                if os-error <> 0
                then do :
                    bell . bell .
                    message libelle[ 177 ] skip
                            fic-trt-2      skip
                            libelle[ 47 ] os-error
                            view-as alert-box error .
                end .
            end .

        end . /* if opsys = "UNIX" */
    end . /* Extractions  */

    else do : /* Integrations */

        input stream s-fic close .
        if fic-trt-2 <> "" then input stream s-fic-2 close .

        /* Edition Fichier Liste MAJ */
        if nb-trt <> 0 and ( ind-trt = 4 or ind-trt = 5 )
        then do :
            output stream s-lis close .
            assign mess-lib = libelle[ 26 ]
                   nom-fic  = fic-lis
                   .
            run edition .
        end .

        /* Rename ou Delete du fichier traite si MAJ */
        if nb-lu <> 0
        then do :

            if trt-maj and not trt-test
            then do : 
                os-rename value( fic-trt ) value( fic-ren ) .            
                if os-error <> 0                                             
                then do :                                                    
                    bell . bell .                                            
                    message libelle[ 45 ] skip                               
                            fic-trt       skip                               
                            fic-ren       skip                               
                            libelle[ 47 ] os-error                           
                            view-as alert-box error .                        
                end .                                                        
            end .

            if fic-trt-2 <> ""                                           
            then do :                                                    

                if nb-err <> 0                                           
                then do :                                                
                    if trt-maj and not trt-test
                       then os-rename value( fic-trt-2 ) value( fic-rej-2 ) .
                       else os-copy value( fic-trt-2 ) value( fic-rej-2 ) .    
                    if os-error <> 0                                     
                    then do :                                            
                        bell . bell .                                    
                        message libelle[ 117 ] skip                      
                                fic-trt-2      skip                      
                                fic-rej-2      skip                      
                                libelle[ 47 ] os-error                   
                                view-as alert-box error .                
                    end .                                                
                end .                                                    
                else if trt-maj and not trt-test
                     then do : 

                         os-rename value( fic-trt-2 ) value( fic-ren-2 ) .    
                         if os-error <> 0                                     
                         then do :                                            
                             bell . bell .                                    
                             message libelle[ 45 ] skip                       
                                     fic-trt-2     skip                       
                                     fic-ren-2     skip                       
                                     libelle[ 47 ] os-error                   
                                     view-as alert-box error .                
                         end .                                                
                     end .                                                    

            end . /* if fic-trt-2 <> "" */

        end . /* if nb-lu <> 0 */

    end . /* Integrations */

    /* Suppression du fichier de blocage */
    os-delete value( fic-blo[ 1 ] ) .
    if os-error <> 0
    then do :
        bell . bell .
        message libelle[ 46 ] skip fic-blo[ 1 ] skip libelle[ 47 ] os-error
                view-as alert-box error .
    end .

END PROCEDURE . /* TRT-FIN */

/* Controle existence des Tiers */
PROCEDURE CTRL-TIERS :

    trt-ok = yes .

    do tour-soc = 1 to num-entries( l-soc ) :

        codsoc-soc = entry( tour-soc, l-soc ) .

        if codsoc-soc = "" then next .

        regs-app = "ELODIE" .
        { regs-cha.i }

        regs-app = "NATHALIE" .
        { regs-cha.i }

        { regs-inc.i "auxspe" }
        auxapr-soc = regs-soc .

        do tour-tiers = 1 to num-entries( l-tiers ) :

            assign type-tiers = substr( entry( tour-tiers, l-tiers ), 1, 3 )
                   code-tiers = substr( entry( tour-tiers, l-tiers ), 4, 10 )
                   .

            { cadr-aux.i codsoc-soc type-tiers code-tiers }

            FIND AUXAPR where auxapr.codsoc = auxapr-soc
                        and   auxapr.typaux = type-tiers
                        and   auxapr.codaux = code-tiers
                        no-lock no-error .

            if not available auxapr
            then do :
                bell . bell .
                message type-tiers code-tiers libelle[ 64 ] codsoc-soc "..."
                        skip libelle[ 44 ]
                        view-as alert-box error .
                trt-ok = no .
            end .

            regs-fileacc = "AUXILI" + type-tiers .
            { regs-rec.i }
            FIND AUXILI where auxili.codsoc = regs-soc
                        and   auxili.typaux = type-tiers
                        and   auxili.codaux = code-tiers
                        no-lock no-error .

            if not available auxili
            then do :
                bell . bell .
                message type-tiers code-tiers libelle[ 65 ] regs-soc "..."
                        skip libelle[ 44 ]
                        view-as alert-box error .
                trt-ok = no .
            end .

        end . /* do tour-tiers = 1 to num-entries( l-tiers ) : */

    end . /* do tour-soc = 1 to num-entries( l-soc ) : */

END PROCEDURE . /* CTRL-TIERS */

/* Affectation des Tiers Liv. et Fac. Achat format DECLIC */
PROCEDURE AFFECT-TIERS-D-ACH :

    assign tiers-liv = ""
           tiers-fac = ""
           .

    FIND MAGASI where magasi.codsoc = magasi-soc
                and   magasi.codtab = entete.magasin
                no-lock no-error .

    if not available magasi or magasi.codaux-cli = ""
    then do :
        mess-err = entete.magasin .
        if not available magasi then mess-err = mess-err + libelle[ 99 ] .
                                else mess-err = mess-err + libelle[ 174 ] .
        if program-name(2) begins "ecrit-cde-dec"
        then do :
           mess-err-2 = libelle[ 176 ] + entete.codsoc + " " + entete.typcom + " " + entete.numbon . 
           run trt-rejet .
           trt-ok = no .
        end .   
        else run rej-bon . /* Rejet complet */

        return .
    end .

    case entete.codsoc :
        when entry( 1, l-soc ) /* 01 */
             then assign tiers-liv = right-trim( entete.codsoc ) + trim( magasi.codaux-cli )
                         tiers-fac = entry( 1, l-tiers-f-dec )
                         .
        /*
        when entry( 2, l-soc ) /* 05 */
             then assign tiers-liv = right-trim( entry( 1, l-soc ) ) + trim( magasi.codaux-cli )
                         tiers-fac = entry( 2, l-tiers-f-dec )
                         .
        */
        
		/* WF-315208 */
        when entry( 2, l-soc ) /* 42 */
             then assign tiers-liv = right-trim( entry( 1, l-soc ) ) + trim( magasi.codaux-cli )
                         tiers-fac = entry( 2, l-tiers-f-dec )
                         .
        
        when entry( 3, l-soc ) /* 08 */
             then assign tiers-liv = right-trim( entete.codsoc ) + trim( magasi.codaux-cli )
                         tiers-fac = entry( 3, l-tiers-f-dec )
                         .
        when entry( 4, l-soc ) /* 58 */
             then assign tiers-liv = right-trim( entry( 1, l-soc ) ) + trim( magasi.codaux-cli )
                         tiers-fac = entry( 4, l-tiers-f-dec )
                         .
        when entry( 5, l-soc ) /* 59 */
             then assign tiers-liv = right-trim( entry( 1, l-soc ) ) + trim( magasi.codaux-cli )
                         tiers-fac = entry( 1, l-tiers-f-dec-2 )
                         .
		/* WF-315208 */
        when entry( 6, l-soc ) /* 88 Grassot */
             then assign tiers-liv = right-trim( entry( 1, l-soc ) ) + trim( magasi.codaux-cli )
                         tiers-fac = entry( 3, l-tiers-f-dec-2 )
                         .
    end case .

END PROCEDURE . /* AFFECT-TIERS-D-ACH */

/* Affectation des Tiers Liv. et Fac. Vente format DECLIC */
PROCEDURE AFFECT-TIERS-D-VTE :

    assign tiers-liv = ""
           tiers-fac = ""
           .

    case entete.codsoc :
        when entry( 1, l-soc ) /* 01 */
             then assign tiers-liv = entry( 4, l-tiers-l-dec ) + trim( entete.codaux-liv )
                         tiers-fac = entry( 5, l-tiers-f-dec )
                         .
        /*
        when entry( 2, l-soc ) /* 05 */
             then assign tiers-liv = entry( 4, l-tiers-l-dec ) + trim( entete.codaux-liv )
                         tiers-fac = entry( 6, l-tiers-f-dec )
                         .
        */
		/* WF-315208 */
		when entry( 2, l-soc ) /* 42 */
             then assign tiers-liv = entry( 4, l-tiers-l-dec ) + trim( entete.codaux-liv )
                         tiers-fac = entry( 6, l-tiers-f-dec )
                         .
        when entry( 3, l-soc ) /* 08 */
             then assign tiers-liv = entry( 5, l-tiers-l-dec ) + trim( entete.codaux-liv )
                         tiers-fac = entry( 7, l-tiers-f-dec )
                         .
        when entry( 4, l-soc ) /* 58 */
             then assign tiers-liv = entry( 6, l-tiers-l-dec ) + trim( entete.codaux-liv )
                         tiers-fac = entry( 8, l-tiers-f-dec )
                         .
        when entry( 5, l-soc ) /* 59 */
             then assign tiers-liv = entry( 4, l-tiers-l-dec ) + trim( entete.codaux-liv )
                         tiers-fac = entry( 2, l-tiers-f-dec-2 )
                         .
		/* WF-315208 */
        when entry( 6, l-soc ) /* 88 Grassot */
             then assign tiers-liv = entry( 4, l-tiers-l-dec ) + trim( entete.codaux-liv )
                         tiers-fac = entry( 4, l-tiers-f-dec-2 )
                         .
    end case .

END PROCEDURE . /* AFFECT-TIERS-D-VTE */

/* Traitement d'Erreur */
PROCEDURE TRT-REJET :

    if nb-err = 0
    then do :
        output stream s-rej to value( fic-rej ) .
        mess-lib = chr( 27 ) + "&l1O" + "/* " .      /* Mise a l'italienne */
        if ind-trt < 3 /* Extractions */
           then mess-lib = mess-lib + libelle[ 15 ] .
           else mess-lib = mess-lib + libelle[ 16 ] .
        mess-lib = mess-lib + caps( trim( lib-prog ) ) + " " +
                   string( today, "99/99/9999" ) + " " +
                   string( time, "hh:mm:ss" ) .
        put stream s-rej unformatted mess-lib skip( 2 ) .
        mess-lib = "/* " + libelle[ 17 ] + fic-rej .
        put stream s-rej unformatted mess-lib skip .
        mess-lib = "/* " + libelle[ 17 ] + fic-trt .
        put stream s-rej unformatted mess-lib skip .
        if fic-trt-2 <> ""
        then do :
            mess-lib = "/* " + libelle[ 17 ] + fic-trt-2 .
            put stream s-rej unformatted mess-lib skip( 1 ) .
        end .
        else put stream s-rej unformatted skip( 1 ) .
        if tour-fac = 1 or tour-fac = 9
        then do :
            put stream s-rej unformatted skip( 1 ) .
            mess-lib = "/* " + libelle[ 138 ] .
            put stream s-rej unformatted mess-lib skip( 2 ) .
        end .
    end .

    if ( tour-fac = 2 or tour-fac = 3 ) and not rejet-ok
    then do :
        rejet-ok = yes .

        case tour-fac :
            when 2 then mess-lib = "/* " + libelle[ 139 ] .
            when 3 then mess-lib = "/* " + libelle[ 158 ] .
        end case .

        put stream s-rej unformatted skip( 2 ) .
        put stream s-rej unformatted mess-lib skip( 2 ) .
    end .

    if mess-err <> ""
    then repeat :
        if not mess-err begins libelle[ 136 ] then nb-err = nb-err + 1 .
        if ind-trt < 3 then leave . /* Extractions */
        if mess-err <> libelle[ 137 ] and mess-err <> libelle[ 141 ] and mess-err <> libelle[ 182 ] and mess-err <> libelle[ 183 ]
           then mess-err = "/* " + libelle[ 18 ] + string( nb-lu ) + " - " + mess-err .
           else mess-err = "/* " + mess-err .
        leave .
    end .
    else mess-err = "/* " + libelle[ 18 ] + trim( string( nb-lu, ">>>>9" ) ) + lib-fac .

    if mess-err-2 <> ""
    then do :
        mess-err-2 = "/* " + mess-err-2 .
        put stream s-rej unformatted mess-err-2 skip .
        mess-err-2 = "" .
    end .

    if tour-fac = 9 then enreg = "/* " + enreg .

    put stream s-rej unformatted mess-err skip .

    if ind-trt >= 3 and /* Integrations */
       not mess-err matches "*" + libelle[ 147 ] + "*" and enreg <> ""
       then put stream s-rej unformatted enreg skip .

    if tour-fac <> 0
    then do :
        if tour-fac = 9 then substr( enreg, 1, 3 ) = "" .
        run rej-fac .
    end .

END PROCEDURE . /* TRT-REJET */

/* Traitement d'ecriture dans le fichier Liste */
PROCEDURE TRT-LISTE :

    if nb-trt = 0 then output stream s-lis to value( fic-lis ) .

    if nb-lig-lis = 0 or nb-lig-lis = 60 or fin-ok
    then do :

        if nb-lig-lis = 60 or fin-ok
        then do :
            mess-lib = libelle[ 21 ] .
            put stream s-lis unformatted mess-lib skip .
            if ind-trt = 4 /* Integration Articles */
            then do :
                mess-lib = libelle[ 29 ] .
                put stream s-lis unformatted mess-lib skip .
                mess-lib = libelle[ 30 ] .
                put stream s-lis unformatted mess-lib skip .
            end .
            if not fin-ok
            then do :
                mess-lib = chr( 12 ) .
                put stream s-lis unformatted mess-lib skip .
            end .
        end .

        if fin-ok then return .

        mess-lib = fill( " ", 10 ) + mess-tit + caps( trim( lib-prog ) ) + " " +
                   string( today, "99/99/9999" ) + " " +
                   string( time, "hh:mm:ss" ) .
        put stream s-lis unformatted mess-lib skip( 2 ) .
        mess-lib = libelle[ 17 ] + fic-trt .
        put stream s-lis unformatted mess-lib skip( 1 ) .
        mess-lib = libelle[ 21 ] .
        put stream s-lis unformatted mess-lib skip .
        put stream s-lis unformatted mess-ent skip .
        mess-lib = libelle[ 21 ] .
        put stream s-lis unformatted mess-lib skip .
        nb-lig-lis = 8 .

    end .

    put stream s-lis unformatted mess-lig skip .

    nb-lig-lis = nb-lig-lis + 1 .

END PROCEDURE . /* TRT-LISTE */

/* Edition des Fichier Rejets ou Liste Mouchard */
PROCEDURE EDITION :

    def var sav-opeimp as char no-undo .

    edit-ok = yes .
    hide message no-pause .
    if zone-charg[ 9 ] <> "BATCH"
       then message color normal mess-lib nom-fic "?" view-as alert-box
                    question buttons yes-no update edit-ok .
    if edit-ok
    then do :
        sav-opeimp = opeimp .
        if opeimp = "" then opeimp = nomimp .
        run bat-imp.p( nom-fic ) .
        opeimp = sav-opeimp .
    end .

END PROCEDURE . /* EDITION */

/* Ecriture dans le fichier des Commandes DECLIC */
PROCEDURE ECRIT-CDE-DEC :

    def input parameter sup-ent as char no-undo .

    def var mot-cle     as char          no-undo .
    def var enreg-ent   as char          no-undo .
    def var lieu-charg  as char          no-undo .
    def var mode-enlev  as char          no-undo .
    def var mode-trp    as char          no-undo .
    def var comment-ent as char extent 4 no-undo .
    def var comment-lig as char extent 5 no-undo .
    def var supl-art    as char extent 5 no-undo .
    def var supl-taux   as dec  extent 5 no-undo .
    def var nb-lig      as int           no-undo .

    def buffer b-lignes for lignes .

    /* Enreg. Entete */
    enreg-ent = fill( " ", 2 )                   + /* 01 - 02 */
                string( entete.codsoc, "xx" )    + /* 03 - 02 */
                string( entete.motcle, "xxx" )   + /* 05 - 03 */
                string( entete.typcom, "xxx" )   + /* 08 - 03 */
                string( entete.numbon, "x(10)" ) + /* 11 - 10 */
                "000" .                            /* 21 - 03 */

    case entete.motcle :

        when motcle-ach
        then do :
            assign trt-ok                     = yes
                   zone                       = string( entete.typaux, "xxx" ) + entete.codaux
                   overlay( enreg-ent, 1, 2 ) = entry( lookup( zone, l-tiers ), l-soc-dec ) no-error
                   .
            if substr( enreg-ent, 1, 2 ) = ""
            then do :
                mess-err = entete.codsoc + " " + entete.typcom + " " + entete.numbon + libelle[ 194 ] .
                run trt-rejet .
                return .
            end .
            run affect-tiers-d-ach .
            if not trt-ok
            then do :
                trt-ok = yes .
                return .
            end .
        end .

        when motcle-vte
        then do :
            if lookup( entete.magasin, l-mag ) = 1 /* CIZERON sinon EURENA */
               then overlay( enreg-ent, 1, 2 ) = entry( 1, l-soc-dec ) .
               else overlay( enreg-ent, 1, 2 ) = entry( 2, l-soc-dec ) no-error .
            if substr( enreg-ent, 1, 2 ) = ""
            then do :
                mess-err = entete.codsoc + " " + entete.typcom + " " + entete.numbon + libelle[ 194 ] .
                run trt-rejet .
                return .
            end .
            run affect-tiers-d-vte . 
        end .

        otherwise do :
            mess-err = entete.codsoc + " " + entete.typcom + " " + entete.numbon + libelle[ 123 ] .
            run trt-rejet .
            return .
        end .

    end case . /* case entete.motcle : */

    /* Sup. d'un Bon -> on envoi que l'entete */
    if sup-ent = "S"
    then do :
        case substr( enreg-ent, 1, 2 ) : /* Societe DECLIC */
            when entry( 1, l-soc-dec ) /* 31 - CIZERON */
            then do :
                if not ouv-cde
                then do :
                    i-param = "1" .
                    run trt-init .
                    if not trt-ok then return .
                end .
                put stream s-fic unformatted caps( enreg-ent ) skip .
            end .
            when entry( 2, l-soc-dec ) /* 27 - ATRIAL */
            then do :
                if not ouv-cde-2
                then do :
                    i-param = "2" .
                    run trt-init .
                    if not trt-ok then return .
                end .
                put stream s-fic-2 unformatted caps( enreg-ent ) skip .
            end .
        end case .

        entete.env-plat = "DS,DS" . /* Envoye en Suppression */
        { majmoucb.i entete }
        return .
    end .

    /* Enreg. Lignes */
    if entete.modliv <> ""
    then do :

        FIND TABGES where tabges.codsoc = ""
                    and   tabges.etabli = ""
                    and   tabges.typtab = "APR"
                    and   tabges.prefix = "MODLIV"
                    and   tabges.codtab = entete.modliv
                    no-lock no-error .

        if not available tabges
        then do :
            mess-err = entete.modliv + libelle[ 185 ] + entete.codsoc + " " + entete.typcom + " " + entete.numbon + " ..." .
            run trt-rejet .
            return .
        end .

        if tabges.libel2[ 5 ] <> "" and tabges.libel2[ 6 ] <> "" /* Magasin rattache et Mode Enlevement <> "" */
        then do :

            FIND MAGASS where magass.codsoc = magasi-soc
                        and   magass.codtab = tabges.libel2[ 5 ]
                        and   magass.theme  = "SUITE-FICHE"
                        and   magass.chrono = 0
                        no-lock no-error .

            if available magass then lieu-charg = magass.alpha[ 7 ] . /* Magasin DECLIC */

            mode-enlev = entry( lookup( right-trim( tabges.libel2[ 6 ] ), entry( 1, l-modenlev, "/" ) ), entry( 2, l-modenlev, "/" ) ) no-error .

            if error-status:error
            then do :
                mess-err = tabges.libel2[ 6 ] + libelle[ 186 ] + entete.codsoc + " " + entete.typcom + " " + entete.numbon + " ..." .
                run trt-rejet .
                return .
            end .

        end .

    end . /* if entete.modliv <> "" */

    /* Commentaires d'entete */
    j-j = 1 .
    FIND PROGLIB where proglib.motcle  = entete.motcle
                 and   proglib.codsoc  = entete.codsoc
                 and   proglib.etabli  = ""
                 and   proglib.clef    = "COMMANDE"
                 and   proglib.edition = "ENT" + string( entete.typcom, "xxx" ) + entete.numbon
                 and   proglib.chrono  = 0
                 no-lock no-error .

    if available proglib
    then do i-i = 1 to 16 :
        if proglib.libelle[ i-i ] = "" then next .
        comment-ent[ j-j ] = proglib.libelle[ i-i ] .
        j-j = j-j + 1 .
        if j-j > 4 then leave .
    end .

    FIND BONENS where bonens.codsoc = entete.codsoc
                and   bonens.motcle = entete.motcle
                and   bonens.typcom = entete.typcom
                and   bonens.numbon = entete.numbon
                and   bonens.theme  = "MODTRP"
                and   bonens.chrono = 0
                no-lock no-error .

    if available bonens and lookup( right-trim( bonens.alpha[ 1 ] ), l-modtrp ) <> 0 then mode-trp = bonens.alpha[ 1 ] . /* Mode Transport */

    LECT-LIGNES :
    FOR EACH LIGNES where lignes.codsoc    = entete.codsoc
                    and   lignes.motcle    = entete.motcle
                    and   lignes.typcom    = entete.typcom
                    and   lignes.numbon    = entete.numbon
                    and   lignes.codlig    = "N"
                    and   lignes.top-livre = ""
                    use-index primaire no-lock :

        /* Elimination des lignes type Remise */
        FIND ARTAPR where artapr.codsoc = articl-soc
                    and   artapr.articl = lignes.articl
                    no-lock no-error .

        if not available artapr
        then do :
            mess-err = lignes.articl + libelle[ 152 ] + lignes.codsoc + " " +
                       lignes.typcom + " " + lignes.numbon + " " + string( lignes.chrono ) + " ..." .
            run trt-rejet .
            next .
        end .

        if artapr.articl-usine = "R" then next .

        if lieu-charg = "" and l-lieu-charg <> ""
        then do i-i = 1 to num-entries( l-lieu-charg, ";" ) :

            assign zone = ""
                   zone = entry( i-i, l-lieu-charg, ";"  ) no-error
                   .

            case entry( 1, zone, "/" ) :

                when "1" then if num-entries( zone, "/" ) >= 3
                then do :

                    if lookup( string( artapr.famart, "xxx" ) + string( artapr.soufam, "xxx" ) + string( artapr.sssfam, "xxx" ), entry( 2, zone, "/" ) ) <> 0
                    then do :
                        lieu-charg = right-trim( entry( 3, zone, "/" ) ) .
                        leave .
                    end .

                end .

                when "2" then if num-entries( zone, "/" ) >= 4
                then do :

                    if lookup( right-trim( lignes.typcom ), entry( 2, zone, "/" ) ) <> 0 and
                       lookup( right-trim( artapr.type-condit ), entry( 3, zone, "/" ) ) <> 0
                    then do :
                        lieu-charg = right-trim( entry( 4, zone, "/" ) ) .
                        leave .
                    end .

                end .

                when "3" then if num-entries( zone, "/" ) >= 3
                then do :

                    if lookup( right-trim( lignes.typcom ), entry( 2, zone, "/" ) ) <> 0
                    then do :
                        lieu-charg = right-trim( entry( 3, zone, "/" ) ) .
                        leave .
                    end .

                end .

            end case .

        end . /* do i-i = 1 to num-entries( l-lieu-charg, ";" ) : */

        /* Commentaires ligne */
        assign comment-lig = ""
               j-j         = 1
               .
        do i-i = 1 to 4 :
            if comment-ent[ i-i ] = "" then leave .
            comment-lig[ i-i ] = comment-ent[ i-i ] .
            j-j = i-i + 1 .
        end .
        if j-j > 4 then j-j = 4 .

        FIND PROGLIB where proglib.motcle  = entete.motcle
                     and   proglib.codsoc  = entete.codsoc
                     and   proglib.etabli  = ""
                     and   proglib.cle     = string( entete.typcom , "xxx" )   +
                                             string( entete.numbon , "x(10)" ) +
                                             string( lignes.chrono, "9999" )
                     and   proglib.edition = "lig-bp"
                     and   proglib.chrono  = 0
                     no-lock no-error .

        if available proglib
        then do i-i = 1 to 16 :
            if proglib.libelle[ i-i ] = "" then next .
            comment-lig[ j-j ] = proglib.libelle[ i-i ] .
            j-j = j-j + 1 .
            if j-j > 4 then leave .
        end .

        comment-lig[ 5 ] = "" .

        if available bonens and bonens.alpha[ 2 ] <> "" then comment-lig[ 5 ] = entry( 1, libelle[ 178 ] ) + trim( bonens.alpha[ 2 ] ) . /* Type Tournee */

        FIND LIGSPE where ligspe.codsoc = lignes.codsoc
                    and   ligspe.motcle = lignes.motcle + "/NEX"
                    and   ligspe.typcom = lignes.typcom
                    and   ligspe.numbon = lignes.numbon
                    and   ligspe.chrono = lignes.chrono
                    and   ligspe.ss-chrono = 10
                    no-lock no-error .

        if available ligspe
        then do :

            if entry( 1, ligspe.alpha-10 ) <> ""
            then do :
                if comment-lig[ 5 ] <> "" then comment-lig[ 5 ] = comment-lig[ 5 ] + "," .
                comment-lig[ 5 ] = comment-lig[ 5 ] + entry( 2, libelle[ 178 ] ) + trim( entry( 1, ligspe.alpha-10 ) ) . /* B�timent */
            end .

            assign zone = ""
                   zone = entry( 2, ligspe.alpha-10 ) no-error /* Silo */
                   .

            if zone <> ""
            then do :
                if comment-lig[ 5 ] <> "" then comment-lig[ 5 ] = comment-lig[ 5 ] + "," .
                comment-lig[ 5 ] = comment-lig[ 5 ] + entry( 3, libelle[ 178 ] ) + trim( zone ) .
            end .

        end .

        if comment-lig[ 5 ] <> ""
           then if j-j <= 4 then comment-lig[ j-j ] = comment-lig[ 5 ] .
                            else comment-lig[ 4 ] = comment-lig[ 5 ] + entry( 4, libelle[ 178 ] ) + comment-lig[ 4 ] .

        if lignes.codsoc = entry( 4, l-soc ) and lignes.motcle = motcle-vte /* Vente Societe 58 ( BIOAGRI ) */
        then do :

            /* Recherche Magasin d'origine */
            FIND LIGSPE where ligspe.codsoc    = lignes.codsoc
                        and   ligspe.motcle    = lignes.motcle + "/BED"
                        and   ligspe.typcom    = lignes.typcom
                        and   ligspe.numbon    = lignes.numbon
                        and   ligspe.chrono    = lignes.chrono
                        and   ligspe.ss-chrono = 0
                        no-lock no-error .

            if available ligspe
            then do :

                FIND B-LIGNES where b-lignes.codsoc = ligspe.alpha-1
                              and   b-lignes.motcle = ligspe.alpha-2
                              and   b-lignes.typcom = ligspe.alpha-3
                              and   b-lignes.numbon = ligspe.alpha-4
                              and   b-lignes.chrono = ligspe.int-1
                              no-lock no-error .

                if not available b-lignes
                then do :
                    assign mess-err   = ligspe.alpha-1 + " " + ligspe.alpha-3 + " " + ligspe.alpha-4 + " " + string( ligspe.int-1 ) + libelle[ 81 ]
                           mess-err-2 = libelle[ 179 ] + lignes.codsoc + " " + lignes.typcom + " " + lignes.numbon + " " + string( lignes.chrono )
                           .
                    run trt-rejet .
                    next .
                end .

                FIND MAGASI where magasi.codsoc = magasi-soc
                            and   magasi.codtab = b-lignes.magasin
                            no-lock no-error .

                if not available magasi or ( lookup( "REL", magasi.type-mag ) <> 0 and magasi.codaux-cli = ""  )
                then do :
                    mess-err = b-lignes.magasin .
                    if not available magasi then mess-err = mess-err + libelle[ 99 ] .
                                            else mess-err = mess-err + libelle[ 174 ] .
                    mess-err-2 = libelle[ 179 ] + lignes.codsoc + " " + lignes.typcom + " " + lignes.numbon + " " + string( lignes.chrono ) .
                end .

                if lookup( "REL", magasi.type-mag ) <> 0 /* Magasin Relais */
                   then overlay( comment-lig[ 4 ], 51, 10 ) = string( entry( 5, libelle[ 178 ] ), "xx" ) +
                                                              string( magasi.codsoc-cli, "xx" )          +
                                                              string( trim( magasi.codaux-cli ), "x(6)" ) .

                /* DGR 21/02/2017 verif si le code operateur est un magasin */ 
                if b-lignes.magasin = "DIR" then do: 
                
                    zone = trim ( b-lignes.opecre ) .
                    FIND MAGASI 
                    where magasi.codsoc = magasi-soc
                    and   magasi.codtab = b-lignes.opecre
                    no-lock no-error .
                    if available magasi then do :
                        zone = magasi.libel1[1].
                        zone = replace ( zone, "Mag Relais ", "").
                    end.
                    zone = replace ( zone, " ", "") .
                    if trim ( zone ) <> "" then 
                        overlay( comment-lig[ 4 ], 37, 14 ) = "ORI=" + string( zone , "x(10)" ) .
                
                end. 
                                                              
                                                              
            end . /* if available ligspe */

        end .  /* if lignes.codsoc = entry( 4, l-soc ) and lignes.motcle = motcle-vte */

        /* Recherche des Supplementations */
        assign supl-art  = ""
               supl-taux = 0
               i-i       = 1
               .

        FOR EACH B-LIGNES where b-lignes.codsoc = lignes.codsoc
                          and   b-lignes.motcle = lignes.motcle
                          and   b-lignes.typcom = lignes.typcom
                          and   b-lignes.numbon = lignes.numbon
                          and   b-lignes.chrono > lignes.chrono
                          use-index primaire no-lock :

            if b-lignes.codlig = "N"
            then do :

                /* Elimination des lignes type Remise */
                FIND ARTAPR where artapr.codsoc = articl-soc
                            and   artapr.articl = b-lignes.articl
                            no-lock no-error .

                if not available artapr
                then do :
                    mess-err = b-lignes.articl + libelle[ 152 ] + b-lignes.codsoc + " " +
                               b-lignes.typcom + " " + b-lignes.numbon + " " + string( b-lignes.chrono ) + " ..." .
                    run trt-rejet .
                    next LECT-LIGNES .
                end .

                if artapr.articl-usine = "R" then next .

                leave .

            end .

            if b-lignes.codlig <> "S" then next .

            if lignes.qte[ 1 ] <> 0
               then supl-taux[ i-i ] = abs( round( ( b-lignes.qte[ 1 ] / lignes.qte[ 1 ] ) * 100, 3 ) ) .

            assign supl-art[ i-i ]  = b-lignes.articl
                   i-i              = i-i + 1
                   .

            if i-i > 5 then leave .

        END . /* FOR EACH B-LIGNES */

        enreg = substr( enreg-ent, 1, 20 )     +                                     /* 001 - 20 */
                string( lignes.chrono, "999" ) .                                     /* 021 - 03 */

        if lignes.qte[ 1 ] < 0 then overlay( enreg, 7, 1 ) = "N" .

        if entete.datbon <> ?
           then enreg = enreg +
                string( year ( entete.datbon ), "9999" ) +                           /* 024 - 08 */
                string( month( entete.datbon ), "99" )   +
                string( day  ( entete.datbon ), "99" ) .
           else enreg = enreg + fill( " ", 8 ) .

        enreg = enreg                           +
                string( tiers-liv, "x(10)" )    +                                    /* 032 - 10 */
                string( tiers-fac, "x(10)" )    +                                    /* 042 - 10 */
                string( lignes.articl, "x(6)" )                                      /* 052 - 06 */
                .

        val-num = abs( ( lignes.qte[ 1 ] - lignes.qte[ 2 ] ) * 1000 ) .

        case lignes.motcle :

            /* Conversion qte en unite de stock DECLIC */
            when motcle-vte
            then do :

                assign type-tiers = ""
                       code-tiers = ""
                       .
                zone = entry( lookup( substr( enreg-ent, 1, 2 ), l-soc-dec ),
                       l-tiers ) no-error .
                if not error-status:error
                   then assign type-tiers = substr( zone, 1, 3 )
                               code-tiers = substr( zone, 4 )
                               .

                FIND ARTBIS where artbis.motcle = motcle-ach
                            and   artbis.codsoc = lignes.codsoc
                            and   artbis.articl = lignes.articl
                            and   artbis.typaux = type-tiers
                            and   artbis.codaux = code-tiers
                            no-lock no-error .

                if not available artbis
                then do :
                mess-err = type-tiers + " " + trim( code-tiers ) + " " +
                           lignes.articl + libelle[ 153 ] + lignes.codsoc +
                           " " + lignes.typcom + " " + lignes.numbon + " " +
                           string( lignes.chrono ) + " ..." .
                    run trt-rejet .
                    next .
                end .

                if artbis.coef-cde <= 0
                   then val-num = - val-num * artbis.coef-cde .
                   else val-num =   val-num / artbis.coef-cde .

                enreg = enreg                              +
                        string( val-num, "999999999" )     +                         /* 058 - 09 */
                        string( entete.mag-respon, "xxx" )                           /* 067 - 03 */
                        .
            end .

            otherwise enreg = enreg                           +
                              string( val-num, "999999999" )  +                      /* 058 - 09 */
                              string( entete.magasin, "xxx" )                        /* 067 - 03 */
                              .                                                                   

        end case .                                                                                

        if entete.datdep <> ?                                                                     
           then enreg = enreg +                                                                   
                string( year ( entete.datdep ), "9999" ) +                           /* 070 - 08 */
                string( month( entete.datdep ), "99" )   +                                        
                string( day  ( entete.datdep ), "99" ) .                                          
           else enreg = enreg + fill( " ", 8 ) .                                                  

        enreg = enreg                      +                                                      
                string( lieu-charg, "xx" ) .                                         /* 078 - 02 */

        do i-i = 1 to 4 :                                                            /* 080 - 240 */
            enreg = enreg +                                                                       
                    string( comment-lig[ i-i ], "x(60)" ) .                                       
        end .                                                                                     

        do i-i = 1 to 5 :                                                            /* 320 - 60 */
            enreg = enreg                                       +
                    string( supl-art[ i-i ], "x(6)" )           +
                    string( supl-taux[ i-i ] * 1000, "999999" ) .
        end .

        if entete.datliv <> ?
           then enreg = enreg +
                string( year ( entete.datliv ), "9999" ) +                           /* 380 - 08 */
                string( month( entete.datliv ), "99" )   +                                         
                string( day  ( entete.datliv ), "99" ) .                                           
           else enreg = enreg + fill( " ", 8 ) .                                                   

        assign enreg = enreg                      +                                                
                       string( mode-enlev, "xx" ) +                                  /* 388 - 02 */
                       fill( " ", 3 )             +                                  /* 390 - 03 */
                       string( mode-trp, "xxx" )  +                                  /* 393 - 03 */
                       fill( " ", 9 )                                                /* 396 - 09 */

               val-num = 0
               .

        if lignes.motcle = motcle-vte
        then do :
            val-num = int( lignes.atelier ) no-error .
            if val-num = 0 then val-num = int( entete.cleliv ) no-error .
        end .

        if val-num <> 0 then enreg = enreg + string( val-num, "9999" ) .             /* 405 - 04 */
                        else enreg = enreg + fill( " ", 4 ) .

        if lignes.codpri = "G" then enreg = enreg + string( lignes.codpri, "xxx" ) . /* 409 - 03 */
                               else enreg = enreg + fill( " ", 3 ) .

        enreg = caps( substr( enreg, 1, 79 ) ) + substr( enreg, 80, 240 ) + caps( substr( enreg, 320 ) ) .

        /* Par Claude CONSTANT le 26/06/2015 */
        IF logtrace = "O" THEN DO:
            OUTPUT TO VALUE(fichiermouc) APPEND.
            MESSAGE "ecrit-cde-dec_1 avant trt-init:" ouv-cde "-" ouv-cde-2 "-" SUBSTR( enreg-ent, 1, 23).
            OUTPUT CLOSE.
        END.

        case substr( enreg-ent, 1, 2 ) : /* Societe DECLIC */
            when entry( 1, l-soc-dec ) /* 31 - CIZERON */
            then do :
                if not ouv-cde
                then do :
                    i-param = "1" .
                    run trt-init .
                    if not trt-ok then return .
                end .

                /* Par Claude CONSTANT le 26/06/2015 */
                IF logtrace = "O" THEN DO:
                    OUTPUT TO VALUE(fichiermouc) APPEND.
                    MESSAGE "On va ecrire dans stream s-fic:" ouv-cde.
                    OUTPUT CLOSE.
                END.

                if nb-lig = 0 then put stream s-fic unformatted caps( enreg-ent ) skip .
                put stream s-fic unformatted enreg skip .
            end .
            when entry( 2, l-soc-dec ) /* 27 - ATRIAL */
            then do :
                if not ouv-cde-2
                then do :
                    i-param = "2" .
                    run trt-init .
                    if not trt-ok then return .
                end .

                /* Par Claude CONSTANT le 26/06/2015 */
                IF logtrace = "O" THEN DO:
                    OUTPUT TO VALUE(fichiermouc) APPEND.
                    MESSAGE "On va ecrire dans stream s-fic-2:" ouv-cde-2.
                    OUTPUT CLOSE.
                END.

                if nb-lig = 0 then put stream s-fic-2 unformatted caps( enreg-ent ) skip .
                put stream s-fic-2 unformatted enreg skip .
            end .
        end case .

        nb-lig = nb-lig + 1 .

        /* Par Claude CONSTANT le 26/06/2015 */
        IF logtrace = "O" THEN DO:
            OUTPUT TO VALUE(fichiermouc) APPEND.
            MESSAGE "ecrit-cde-dec_2 apres nb-lig+1:" ouv-cde "-" ouv-cde-2 "-" fic-trt "-" fic-trt-2 "-" nb-lig "-" fic-blo[ 1 ] "-" fic-blo[ 2 ].
            OUTPUT CLOSE.
        END.

    END . /* FOR EACH LIGNES */

    if nb-lig <> 0
    then do :
        /* Par Claude CONSTANT le 26/06/2015 */
        IF logtrace = "O" THEN DO:
            OUTPUT TO VALUE(fichiermouc) APPEND.
            MESSAGE "ecrit-cde-dec_3 apres nb-lig <>0 :" ouv-cde "-" ouv-cde-2 "-" nb-lig.
            OUTPUT CLOSE.
        END.

        assign entete.env-plat = "DE,DE" /* Envoye en Creation/Maj */
               nb-trt          =  nb-trt + 1
               .
       { majmoucb.i entete }
    end .
    else if entete.env-plat = "DN,DE"
         then do :
             case substr( enreg-ent, 1, 2 ) : /* Societe DECLIC */
                 when entry( 1, l-soc-dec ) /* 31 - CIZERON */
                 then do :
                     if not ouv-cde
                     then do :
                         i-param = "1" .
                         run trt-init .
                         if not trt-ok then return .
                     end .
                     put stream s-fic unformatted caps( enreg-ent ) skip .
                 end .
                 when entry( 2, l-soc-dec ) /* 27 - ATRIAL */
                 then do :
                     if not ouv-cde-2
                     then do :
                         i-param = "2" .
                         run trt-init .
                         if not trt-ok then return .
                     end .
                     put stream s-fic-2 unformatted caps( enreg-ent ) skip .
                 end .
             end case .

             assign entete.env-plat = "DS,DS" /* Envoye en Suppression */
                    nb-trt          =  nb-trt + 1
                     .
             { majmoucb.i entete }
         end .

END PROCEDURE . /* ECRIT-CDE-DEC */


