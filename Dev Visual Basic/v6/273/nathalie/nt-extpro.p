/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/nt-extpro.p                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 61173 06-10-16!Evo!pha            !report modifs 273                                                     !
!V61 19614 30-06-10!New!bp             !mise en place d'un lanceur pour programmes maison des clients         !
!-------------------------------------------------------------------------------------------------------------!
!                             I N C L U D E S                !                    T A B L E S                 !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!d-brokmini.i          !new                                  !entete                                          !
!bat-ini.i             !new                                  !magasi                                          !
!select.i              !new                                  !lignes                                          !
!aide.i                !new                                  !artapr                                          !
!maquette.i            !new                                  !CODBAR                                          !
!bat-cha.i             !"sel-applic" "selection-applic" "a"  !artbis                                          !
!regs-cha.i            !                                     !nat-tarcli                                      !
!tarcli.i              !                                     !stocks                                          !
!cadra-d.i             !wcodpro[i] 8 0                       !                                                !
!regs-inc.i            !"proplatef"                          !                                                !
!selectpt.i            !                                     !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
!_____________________________________________________________________________________________________________!
!                                                  P R O C E D U R E S                                        !
!_____________________________________________________________________________________________________________!
!TRAITEMENT            !                                                                                      !
&End
**************************************************************************************************************/



{ d-brokmini.i new }


{ bat-ini.i    new }
{ select.i     new }
{ aide.i       new }
{ maquette.i   new }  


DEFINE stream maquette.   /*  canal maquette */


DEFINE VARIABLE x-prgclient AS CHARACTER   NO-UNDO.
def var    x-alpha1         as char       no-undo . 
def var    x-alpha2         as char       no-undo .
def var    x-alpha3         as char       no-undo .
def var    x-alpha4         as char       no-undo .
def var    x-alpha5         as char       no-undo .
def var    x-alpha6         as char       no-undo .
def var    x-alpha7         as char       no-undo .
def var    x-alpha8         as char       no-undo .
def var    x-alpha9         as char       no-undo .
DEF VAR    x-alpha10        as char       no-undo .

def buffer b-entete for entete .
def buffer b-lignes for lignes .

def temp-table t-art
    field codsoc like artapr.codsoc
    field articl like artapr.articl
    field lib    like artapr.libart1[1]
    field pastd  as dec
    field papro  as dec 
    field pvstd  as dec 
    field pvpro  as dec
    field codfou like artbis.articl-fou
    field ean13  as char format "x(13)"
index i-art codsoc articl.


def input parameter  fichier as char format "x(12)" .

run bat-lec.p ( fichier ) .


/*---------------------------------------------------------*/
/* Prise des selections et chargement des bornes mini-maxi */
/*---------------------------------------------------------*/


{ bat-cha.i  "sel-applic"     "selection-applic"   "a"          }
{ bat-cha.i  "sel-filtre"     "selection-filtre"   "a"          }
{ bat-cha.i  "sel-sequence"   "selection-sequence" "a"          }
{ bat-cha.i  "codsoc"         "glo-codsoc"         "a"          }
{ bat-cha.i  "etabli"         "glo-etabli"         "a"          }

{ bat-cha.i  "ma-impri"      "ma-impri"          "a"          }   
{ bat-cha.i  "ma-maquet"      "ma-maquet"          "a"          }   
{ bat-cha.i  "lib-prog"       "lib-prog"           "a"          }

{ bat-cha.i  "prgclient"     "x-prgclient"          "a"          }   

{ bat-cha.i  "alpha1"       "x-alpha1"           "a"          }   
{ bat-cha.i  "alpha2"       "x-alpha2"           "a"          }
{ bat-cha.i  "alpha3"       "x-alpha3"           "a"          }   
{ bat-cha.i  "alpha4"       "x-alpha4"           "a"          }
{ bat-cha.i  "alpha5"       "x-alpha5"           "a"          }   
{ bat-cha.i  "alpha6"       "x-alpha6"           "a"          }
{ bat-cha.i  "alpha7"       "x-alpha7"           "a"          }   
{ bat-cha.i  "alpha8"       "x-alpha8"           "a"          }
{ bat-cha.i  "alpha9"       "x-alpha9"           "a"          }   
{ bat-cha.i  "alpha10"      "x-alpha10"          "a"          }


run select-c.p.  

regs-app = "elodie".
{regs-cha.i }
regs-app = "nathalie".
{regs-cha.i}

deal-setsocreg ( "" ) .
deal-setsocreg ( glo-codsoc ) .


run sel-del.p ( selection-applic, selection-filtre, selection-sequence, yes ) .  


RUN traitement .


PROCEDURE TRAITEMENT :


    def var codsoc-soc-sav as char                       no-undo .
    def var l-soc          as char                       no-undo .
    def var wsoc           as char                       no-undo .
    def var i              as int                        no-undo .
    def var j              as int                        no-undo .
    def var k              as int                        no-undo .
    def var wcodpro        as char     extent 3          no-undo .
    def var wmessage       as char                       no-undo .
    def var soc-pro        as char                       no-undo .
    def var magasi-soc     as char                       no-undo .
    def var ko             as log                        no-undo .    
    def var wtyppro        as char                       no-undo .
    def var wrefpro        as char                       no-undo .
    def var wdatsaideb     as date                       no-undo .
    def var wdatsaifin     as date                       no-undo .
    def var wdatlivdeb     as date                       no-undo .
    def var wdatlivfin     as date                       no-undo .
    def var wdatpredeb     as date                       no-undo .
    def var wdatprefin     as date                       no-undo .
    def var wdatprodeb     as date                       no-undo .
    def var wdatprofin     as date                       no-undo .

    def var wlibmag        as char                       no-undo .
    def var wdatdeba       as date                       no-undo .
    def var wdatfina       as date                       no-undo . 
    def var wdatdebtara    as date                       no-undo .
    def var wdatfintara    as date                       no-undo . 
    def var wdatdebl       as date                       no-undo .
    def var wdatfinl       as date                       no-undo . 
    def var wdatdebv       as date                       no-undo . 
    def var wdatfinv       as date                       no-undo . 
    def var wpvpro         like {tarcli.i}.valeur            no-undo .
    def var wpapro         like {tarcli.i}.valeur            no-undo .
    def var wpvstd         like {tarcli.i}.valeur            no-undo .
    def var wpastd         like {tarcli.i}.valeur            no-undo .
    def var wsto           as dec                        no-undo . 
    def var wean13         as char                       no-undo .
    def var l-codetarif    as char                       no-undo . 
    def var wcodfou        as char                       no-undo .
    def var top-val        as char                       no-undo .

    def var wdatmincde     as date                       no-undo .
    def var wdatmaxcde     as date                       no-undo .
    def var wdatmin        as date                       no-undo .
    def var wdatmax        as date                       no-undo .

    assign codsoc-soc-sav = codsoc-soc.

    assign l-soc = "01,08,59,45".


    /* verif des codes promos saisis*/  
    assign wcodpro [1] = trim ( x-alpha1 ) 
           wcodpro [2] = trim ( x-alpha2 )
           wcodpro [3] = trim ( x-alpha3 ) .


    if trim ( x-alpha4 ) <> "" then   wdatmincde = date (  trim ( x-alpha4 ) ).
    if trim ( x-alpha5 ) <> "" then   wdatmaxcde = date (  trim ( x-alpha5 ) ).


    wmessage = "".


    if wcodpro [1] = "" and wcodpro [2] = "" and wcodpro [3] = ""  then 
        wmessage = "Aucun code promo saisi .....".
    else do:
        assign ko =  yes .
        do i = 1 to 3:
            if wcodpro [i] = "" then next.
            { cadra-d.i wcodpro[i] 8 0 }
            do j = 1 to num-entries ( l-soc) :
                codsoc-soc = entry ( j , l-soc ).               
                regs-app = "NATHALIE" .
                { regs-cha.i }
                { regs-inc.i "proplatef"}       /* societe de regroupement pour vitry */ 
                soc-pro = regs-soc .
                find entete
                where entete.codsoc = soc-pro
                and   entete.motcle = "VTP"
                and   entete.typcom = "PRO"
                and   entete.numbon = wcodpro [i] + "00"
                no-lock no-error.
                if available entete then assign ko =  no.
            end.
        end.
        if ko = yes then wmessage = "aucune promo trouvée sur 01,08,59".
    end.

    if wdatmincde <> ? and wdatmaxcde <> ? and wdatmincde > wdatmaxcde then do:
        wmessage = wmessage + " " + "DATES INCOHERENTES.........." .
        wmessage = trim ( wmessage ) .
    end.

    OUTPUT STREAM maquette TO VALUE( ma-impri ) .
    export stream maquette delimiter ";" "Extraction des Promos GVSE " + string (today, "99/99/9999") + " a " + string ( time, "hh:mm") .

    export stream maquette delimiter ";" "Promos Selectionnees: " wcodpro [1]  wcodpro [2]  wcodpro [3] .
    export stream maquette delimiter ";" "Periode Commande Selectionnee: " wdatmincde "a" wdatmaxcde.

    if wdatmincde = ? and wdatmaxcde = ? then
        export stream maquette delimiter ";" "Pas de dates renseignees ===>  on prend les dates des promos" .


    if wmessage <> "" then do:
        export stream maquette delimiter ";" " **** FIN ANORMALE DU TRAITEMENT ****" .
        export stream maquette delimiter ";" wmessage .
        output close .
        codsoc-soc = codsoc-soc-sav. 
        leave .
    end.
    else do:

        message "Debut extraction PROMO GVSE le " string (today,"99/99/9999") "a" string ( time , "HH:MM:SS" ).

        export stream maquette delimiter ";" "".
        export stream maquette delimiter ";" "".

        export stream maquette delimiter ";"
            "Societe"        "Num Promo"      "Type Promo"     "Ref. Promo"
            "Date deb ACH"   "Date fin ACH"   "Date deb LIV"   "Date fin LIV"     "Date deb VTE"     "Date fin VTE"     "Date deb SAI"     "Date fin SAI"     
            "Code mag"       "Libelle Mag"    "motcle "        "typcom"         "Num bon"          "typbon" 
            "Date Cde"       "Date Liv"       "Date cre"       "Date maj"         "Validee siege"   "Validee mag "     "Envoye GVSE"      
            "art GVSE"       "Art DEAL "      "Designation"    "Gencod"           "Tarif PRE"        "Tarif STE"        "Tarif PRO"       "Tarif CLI"
            "Num lig cde"    "Qte Commandee"  "Qte cde * PRE"  "Qte cde * STE"    "stock TR".


        do i = 1 to 3:

            if wcodpro [i] = "" then next.
            { cadra-d.i wcodpro[i] 8 0 }

            do j = 1 to num-entries ( l-soc ) :


                empty temp-table t-art.

                codsoc-soc = entry ( j , l-soc ).   
                regs-app = "NATHALIE" .
                { regs-cha.i }

                { regs-inc.i "proplatef"}       /* societe de regroupement pour vitry */ 
                soc-pro = regs-soc .

                { regs-inc.i "magasi" }
                 magasi-soc = regs-soc .

                message  codsoc-soc soc-pro wcodpro [i].
                find entete
                where entete.codsoc = soc-pro
                and   entete.motcle = "VTP"
                and   entete.typcom = "PRO"
                and   entete.numbon = wcodpro [i] + "00"
                no-lock no-error.
                if available entete then do:
                    assign wtyppro = entete.typbon
                           wrefpro = entete.ref-tiers.
						   
					   
					if entete.datcre <= 09/01/2015	then
						assign 	wdatpredeb = entete.datedi [8]
								wdatprefin = entete.datedi [9]
								wdatprodeb = entete.datedi [10]
								wdatprofin = entete.datedi [11]
								wdatsaideb = entete.datedi [12]
								wdatsaifin = entete.datedi [13]
								wdatlivdeb = entete.datedi [14]
								wdatlivfin = entete.datedi [15].
			
					else 
						assign 	wdatpredeb = entete.datedi [1]
								wdatprefin = entete.datedi [2]
								wdatprodeb = entete.datedi [3]
								wdatprofin = entete.datedi [4]
								wdatsaideb = entete.datedi [5]
								wdatsaifin = entete.datedi [6]
								wdatlivdeb = entete.datedi [7]
								wdatlivfin = entete.datedi [8].

                    if wdatmincde = ? then wdatmin = wdatsaideb.
                                     else wdatmin = wdatmincde.
                    if wdatmaxcde = ? then wdatmax = wdatsaifin.
                                      else wdatmax = wdatmaxcde.

                    message soc-pro     codsoc-soc  substr (entete.typbon, 1,1 )   string ( int (substr ( entete.numbon, 2, 7 ) ), "9999999" )
							entete.typaux entete.codaux
                            wtyppro     wrefpro       wdatpredeb   wdatprefin    wdatprodeb   wdatprofin 
                            wdatsaideb  wdatsaifin    wdatlivdeb   wdatlivfin 
                            wdatmin     wdatmax.

                    /* recherche des commandes des mag */ 
                    for each b-entete
                    where b-entete.codsoc = codsoc-soc
                    and   b-entete.motcle = "ACH"                   
                    and   b-entete.typcom = "CDF"
                    and   b-entete.typaux = entete.typaux
                    and   b-entete.codaux = entete.codaux
                    and   b-entete.nivcom <= "1"
                    and   b-entete.datbon >= wdatmin
                    and   b-entete.datbon <= wdatmax
                    and   b-entete.typbon =  /* "P" */ substr (entete.typbon, 1,1 )  + string ( int (substr ( entete.numbon, 2, 7 ) ) , "9999999")
                    no-lock :

                        find magasi 
                        where magasi.codsoc  = magasi-soc
                        and   magasi.societe = codsoc-soc
                        and   magasi.codtab = b-entete.magasin
                        no-lock no-error.
                        if available magasi then wlibmag = magasi.libel1 [ 1 ].
                        else wlibmag = "".
                        rech-lignes:
                        for each b-lignes 
                        where b-lignes.codsoc = b-entete.codsoc
                        and   b-lignes.motcle = b-entete.motcle
                        and   b-lignes.typcom = b-entete.typcom
                        and   b-lignes.numbon = b-entete.numbon
                        and   b-lignes.articl <> ""
                        no-lock:

                            assign wpastd = ?  wpapro = ?   wpvstd = ?  wpvpro = ?   wcodfou = "" wean13 = "" .
                            find t-art
                            where t-art.codsoc = codsoc-soc
                            and   t-art.articl = b-lignes.articl
                            no-error.
                            if not available t-art then do:
                                find artapr
                                where artapr.codsoc = codsoc-soc
                                and   artapr.articl = b-lignes.articl
                                no-lock no-error.      
                                if not available artapr then next rech-lignes. 

                                find first codbar               
                                where  codbar.codsoc = codsoc-soc
                                and    codbar.articl = artapr.articl    
                                no-lock no-error.
                                if available codbar  then wean13 = codbar.code-barre.

                                wcodfou = "".
                                find artbis
                                where artbis.codsoc = b-entete.codsoc
                                and   artbis.motcle = "ACH"
                                and   artbis.typaux = b-entete.typaux
                                and   artbis.codaux = b-entete.codaux
                                and   artbis.articl = b-lignes.articl
                                no-lock no-error.
                                if available artbis then do:
                                    wcodfou = artbis.articl-fou.                        
                                    /* recherche tarif PRE si date  renseignée sur la promo */
                                    if wdatpredeb <> ? and wdatprefin <> ? then do:
                                        find first {tarcli.i}
                                        where {tarcli.i}.codsoc = b-entete.codsoc
                                        and   {tarcli.i}.motcle = "ACH"
                                        and   {tarcli.i}.typaux = b-entete.typaux
                                        and   {tarcli.i}.codaux = b-entete.codaux
                                        and   {tarcli.i}.code-tarif =  "PRE"
                                        and   {tarcli.i}.articl = b-lignes.articl
                                        and   ( ( {tarcli.i}.date-debut >= wdatpredeb   and   {tarcli.i}.date-fin <= wdatprefin )  or
                                                ( {tarcli.i}.date-debut <= wdatpredeb   and   {tarcli.i}.date-fin >= wdatprefin ) )
                                        no-lock no-error.
                                        if available {tarcli.i} then assign wpapro = {tarcli.i}.valeur.
                                    end. 
                                    /* recherche du tarif STE en cours sur la periode de saisie */
                                    find first {tarcli.i}
                                    where {tarcli.i}.codsoc = b-entete.codsoc
                                    and   {tarcli.i}.motcle = "ACH"
                                    and   {tarcli.i}.typaux = b-entete.typaux
                                    and   {tarcli.i}.codaux = b-entete.codaux
                                    and   {tarcli.i}.code-tarif = "STE"
                                    and   {tarcli.i}.articl = b-lignes.articl
                                    and   {tarcli.i}.date-debut <= wdatmax 
                                    no-lock no-error.
                                    if available {tarcli.i} then assign wpastd = {tarcli.i}.valeur      .
                                end.

                                /* recherche tarif PRO si date  renseignée sur la promo */
                                if wdatpredeb <> ? and wdatprefin <> ? then do:
                                    find first {tarcli.i}
                                    where {tarcli.i}.codsoc = b-entete.codsoc
                                    and   {tarcli.i}.motcle = "VTE"
                                    and   {tarcli.i}.typaux = ""
                                    and   {tarcli.i}.codaux = ""
                                    and   {tarcli.i}.articl = b-lignes.articl
                                    and   {tarcli.i}.code-tarif =  "PRO"
                                    and   ( ( {tarcli.i}.date-debut >= wdatprodeb  and   {tarcli.i}.date-fin <= wdatprofin ) or 
                                            ( {tarcli.i}.date-debut <= wdatprodeb  and   {tarcli.i}.date-fin >= wdatprofin ) )
                                    no-lock no-error .
                                    if available {tarcli.i} then 
                                        assign wpvpro = {tarcli.i}.valeur .                     
                                end.
                                find first {tarcli.i}
                                where {tarcli.i}.codsoc = b-entete.codsoc
                                and   {tarcli.i}.motcle = "VTE"
                                and   {tarcli.i}.typaux = ""
                                and   {tarcli.i}.codaux = ""
                                and   {tarcli.i}.code-tarif = "CLI"
                                and   {tarcli.i}.articl = b-lignes.articl
                                and   {tarcli.i}.date-debut <= wdatmax 
                                no-lock no-error.
                                if available {tarcli.i} then assign  wpvstd = {tarcli.i}.valeur .

                                create t-art.
                                assign t-art.codsoc = b-entete.codsoc
                                       t-art.articl = b-lignes.articl
                                       t-art.pastd  = wpastd 
                                       t-art.papro  = wpapro 
                                       t-art.pvstd  = wpvstd  
                                       t-art.pvpro  = wpvpro 
                                       t-art.codfou = wcodfou 
                                       t-art.ean13  =  wean13 .
                            end.  /* if not available t-art */
                            else 
                                assign wpastd  = t-art.pastd 
                                       wpapro  = t-art.papro 
                                       wpvstd  = t-art.pvstd  
                                       wpvpro  = t-art.pvpro 
                                       wcodfou = t-art.codfou
                                       wean13  = t-art.ean13  .

                            top-val = "NON".
                            k = lookup( "vac", b-entete.edition ).
                            if k <> 0  then 
                                assign top-val = "OUI " + string ( b-entete.datedi [ k ] ) + " -- " + string ( b-entete.opedi [ k ] ) .
                            /* recup du stock TR */ 
                            wsto = 0.
                            find stocks
                            where stocks.codsoc = b-entete.codsoc
                            and   stocks.articl = b-lignes.articl
                            and   stocks.magasin = b-entete.magasin
                            and   stocks.evenement = ""
                            and   stocks.exercice = ""
                            no-lock no-error .
                            if available stocks then assign  wsto = stocks.qte [2] .
                            export stream maquette delimiter ";"
                                codsoc-soc        substr ( entete.numbon, 4, 5 )         wtyppro           wrefpro 
                                wdatpredeb        wdatprefin       wdatlivdeb            wdatlivfin        wdatprodeb        wdatprofin   wdatsaideb        wdatsaifin 
                                b-entete.magasin  wlibmag          
                                b-entete.motcle   b-entete.typcom  b-entete.numbon       b-entete.typbon
                                b-entete.datbon   b-entete.datdep  b-entete.datcre       b-entete.datmaj   top-val                   
                                if  b-entete.zon-int-4 = 0 then "oui"   
                                                           else "non"
                                b-entete.env-plat 
                                wcodfou           b-lignes.articl  b-lignes.libart       wean13 
                                wpapro            wpastd           wpvpro                wpvstd 
                                b-lignes.chrono   b-lignes.qte[1]  
                                b-lignes.qte[1] * wpapro b-lignes.qte[1] * wpastd
                                wsto 
                                .
                        end . /* for each b-lignes .... lignes de la commandes */           
                    end.  /* for each b-entete : commandes mag */
                end.    /* if available entete .... entete de promo trouvée */ 
            end. /* do j = 1 to num-entries l-soc */ 
        end. /* do i = 1 to 3  */
    end. /* if wmessage = "" .. */

    message "Fin extraction PROMO GVSE le " string (today,"99/99/9999") "a" string ( time , "HH:MM:SS" ).

    output stream maquette close.

    assign codsoc-soc = codsoc-soc-sav .

end. /* fin de la procedure */
{ selectpt.i }
/* FIN */  


