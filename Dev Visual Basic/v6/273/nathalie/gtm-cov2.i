/*============================================================================*/
/*                           G T M - C O V 2 . I                              */
/* Gestion des Coefficients Tarifs Ventes par Magasins                        */
/*----------------------------------------------------------------------------*/
/* Affichage                                                                  */
/*============================================================================*/

assign rec-clef    = recid( tardec )
       code-mag    = substr( tardec.catego, 1, 3 )
       code-fam    = substr( tardec.catego, 4, 3 )
       code-sfam   = substr( tardec.catego, 7 )
       code-art    = tardec.articl
       date-applic = date( int( substr( tardec.groupe, 8, 2 ) ),
                           int( substr( tardec.groupe, 10, 2 ) ),
                           int( substr( tardec.groupe, 4, 4 ) ) )
       .

do i = 1 to 4 :
    coef[ i ] = int( entry( i, tardec.grille-remise ) ) no-error .
end .

display rec-clef blank code-mag code-art code-fam code-sfam date-applic
        coef[ 1 ] coef[ 2 ] coef[ 3 ] coef[ 4 ] with fram fr-rech .
