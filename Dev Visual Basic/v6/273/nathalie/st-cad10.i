/*==========================================================================*/
/*                          S T - C A D 1 0 . I                             */
/*          Edition Cadencier / Provenance et Magasin f( Typologie )        */
/*===========================================================================*/
 
def {1} shared var libelle       as char                 extent 99   no-undo .
def {1} shared var valeur        as char                 extent  9   no-undo .
def {1} shared var valent        as char                 extent 19   no-undo .
 
def {1} shared var masque        as char                 extent  9   no-undo .
 
def {1} shared var lib-per-m1    as char                             no-undo .
def {1} shared var lib-per-m2    as char                             no-undo .
def {1} shared var lib-per-a1    as char                             no-undo .
def {1} shared var lib-per-a2    as char                             no-undo .
 
def {1} shared var lib-magasin   as char                             no-undo .
def {1} shared var lib-catego    as char                             no-undo .
def {1} shared var lib-titre     as char                             no-undo .
def {1} shared var no-famille    as char  format  "xxx"              no-undo .
def {1} shared var no-catego     as char  format  "xxx"              no-undo .
 
def {1} shared var mois-debcp   as char format "x(6)"               no-undo .
def {1} shared var mois-fincp   as char format "x(6)"               no-undo .
def {1} shared var provenance   as char format "x"                  no-undo .
def {1} shared var no-magasin   as char format "xxx"                no-undo .
def {1} shared var magasins     as char format "x(40)"              no-undo .
def {1} shared var familles     as char format "x(40)"              no-undo .
def {1} shared var maquette    as char format "x(12)"               no-undo .
def {1} shared var periph       as char format "x(12)"              no-undo .
 
def {1} shared var lib-maq      as char format "x(30)"              no-undo .
def {1} shared var lib-prov     as char format "x(30)"              no-undo .
 
def {1} shared var fam-exclus   as char  format  "xx"               no-undo .
def {1} shared var mag-exclus   as char  format  "xx"               no-undo .
 
def {1} shared var mois-deb     as int                              no-undo .
def {1} shared var anee-deb     as int                              no-undo .
def {1} shared var mois-fin     as int                              no-undo .
def {1} shared var anee-fin     as int                              no-undo .
 
def {1} shared var sav-periph   as char format "x(12)"              no-undo .
def {1} shared var z-choix      as char                             no-undo .
def {1} shared var zone         as char                             no-undo .
 
def {1} shared var nb-lus       as int                              no-undo .
def {1} shared var nb-trt       as int                              no-undo .
def {1} shared var nb-run       as int                              no-undo .
 
def {1} shared var pre-sfa      as char  format "x(6)"              no-undo .
def {1} shared var nba-sfa      as int                              no-undo .
 
def {1} shared var rup-statut   as char  format "x(2)"              no-undo .
 
def {1} shared frame fr-saisie .
 
form libelle[ 11 ] format "x(28)"  mois-debcp                 skip
     libelle[ 12 ] format "x(28)"  mois-fincp                 skip
     libelle[ 13 ] format "x(28)"  provenance  " "  lib-prov  skip
     libelle[ 14 ] format "x(28)"  magasins                   skip
     libelle[ 15 ] format "x(28)"  familles                   skip
     libelle[ 16 ] format "x(28)"  maquette     lib-maq       skip
     libelle[ 17 ] format "x(28)"  periph
     with frame fr-saisie
     row 3  centered  overlay  no-label  with title libelle[ 10 ] .