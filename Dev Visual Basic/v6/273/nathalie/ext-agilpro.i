/*==========================================================================*/
/*                           E X T - A G I L P R O  . I                     */
/* Extraction des donnees pour AGIL                                         */
/*--------------------------------------------------------------------------*/
/* Extraction des proprietes modeles et des proprietes articles             */
/* VLE 29042013  mise en place de la taxe eco mobilier                      */
/*==========================================================================*/

case code-table :

        /*---------------------------------------------------*/                           
        /*  Ecriture du d�tail de section Proprietes Mod�les */
        /*---------------------------------------------------*/ 
        
        when "FA" /* Nomenclature ou fili�re d'appro */
        then do :
 
                        case notour :
        
                                when 1 
                                        then do : 
                                                ID_PRO_MODELE1 = tabges.codtab.
                                                LB_PRO_MODELE1 = tabges.libel1[mem-langue].
                                                put stream agil unformatted Type_Ligne_Det ID_PRO_MODELE1 "~011" LB_PRO_MODELE1 "~015" skip.
                                        end.
                        
                                when 2 
                                        then do :
                                                ID_PRO_MODELE2 = substr(tabges.prefix, 8, 3 ) + tabges.codtab.
                                                LB_PRO_MODELE2 = tabges.libel1[mem-langue].
                                                put stream agil unformatted Type_Ligne_Det ID_PRO_MODELE2 "~011" LB_PRO_MODELE2 "~015" skip.
                                        end.
                        
                                when 3 
                                        then do :
                                                ID_PRO_MODELE3 = substr(tabges.prefix, 5, 6 ) + tabges.codtab.
                                                LB_PRO_MODELE3 = tabges.libel1[mem-langue].
                                                put stream agil unformatted Type_Ligne_Det ID_PRO_MODELE3 "~011" LB_PRO_MODELE3 "~015" skip.
                                        end.
                                        
                        end case.
                end.
        
        when "AP" /* fili�re d'appro */
                then do :
                        ID_PRO_MODELE4 = tabges.libel2[mem-langue].
                        LB_PRO_MODELE4 = ID_PRO_MODELE4 + " - " + tabges.libel1[mem-langue].
                        put stream agil unformatted Type_Ligne_Det ID_PRO_MODELE4 "~011" LB_PRO_MODELE4 "~015" skip.
                end.

        /*---------------------------------------------------*/                           
        /*  Ecriture du d�tail de section Proprietes Articles*/
        /*---------------------------------------------------*/ 
        
        when "PA_D3E" /* Ecotaxe */
                then do :
                        ID_PRO_ARTICLE_D3E = "D3E" + caps(tabges.codtab).
                        LB_PRO_ARTICLE-D3E = "Dont Ecotaxe de " + string(Tabges.nombre[15], ">9.99") + "�/unit�".
                        put stream agil unformatted Type_Ligne_Det ID_PRO_ARTICLE_D3E "~011" LB_PRO_ARTICLE-D3E "~015" skip.
                end.
                
        when "PA_RPD" /* Redevance Pollution Diffuse */
                then do :
                        ID_PRO_ARTICLE_RPD = "RPD" + caps(tarcli-rpd.articl).
                        LB_PRO_ARTICLE-RPD = "Dont RPD de " + string((tarcli-rpd.valeur / 1000) , ">>9.999") + "� HT/unit�".
                        put stream agil unformatted Type_Ligne_Det ID_PRO_ARTICLE_RPD "~011" LB_PRO_ARTICLE-RPD "~015" skip.
                end.
				
		when "PA_MOB" /* Eco mobilier */
                then do :
                        ID_PRO_ARTICLE_MOB = "MOB" + caps(tabges.codtab).
                        LB_PRO_ARTICLE-MOB = "Dont EcoMobilier " + string(Tabges.nombre[24], ">9.99") + "�/unit�".
                        put stream agil unformatted Type_Ligne_Det ID_PRO_ARTICLE_MOB "~011" LB_PRO_ARTICLE-MOB "~015" skip.
                end.
				
        otherwise return.

end case.