/*============================================================================*/
/*                           G T M - C O V 1 . I                              */
/* Gestion des Coefficients Tarifs Ventes par Magasins                        */
/*----------------------------------------------------------------------------*/
/* Acces Table                                                                */
/*============================================================================*/

lect = "{1}" .

if lect = ""

   then FIND TARDEC where recid( tardec ) = rech-clef
                          no-lock no-error .

   else FIND {1} TARDEC where tardec.codsoc  =      codsoc-soc
                        and   tardec.catego  begins code-mag-s + code-fam-s +
                                                    code-sfam-s
                        and   tardec.articl  begins code-art-s
                        and   tardec.groupe  >=     mot-cle + date-app-s
                        use-index article no-lock no-error .

