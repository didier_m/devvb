/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/gtc-000.i                                                                      !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 290554 2019-09-05!Evo!Jean Baptiste C![GC]Int�gration version 273                                           !
! 281127 2019-06-20!Bug!Patrick        ![GC][NATHAPP]Probl�me de compilation, renpacer tarcli par {tarcli.i}  !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End                                                                                                          
**************************************************************************************************************/
/*----------------------------------------------------------------------------*/
/*                             G T C - 0 0 0 . I                              */
/*              Definition  des  Variables  et  Buffers Partages              */
/*----------------------------------------------------------------------------*/

/* CCO 15072019 : ajout {tarcli.i}                                            */
 
def {1} shared buffer b-tarpre for tarpre .
def {1} shared buffer b-articl for artapr .
 
def {1} shared var nb-lg        as int     initial 3        no-undo .
 
def {1} shared var wk-i         as int                      no-undo .
def {1} shared var wk-j         as int                      no-undo .
 
def {1} shared var libelle       as char   extent 99        no-undo .
 
def {1} shared var codtar-tb     as char   format "xxx"     no-undo .
 
def {1} shared var tarpre-affich0 as char                    no-undo .
def {1} shared var tarpre-affich1 as char                    no-undo .
def {1} shared var tarpre-affich2 as char                    no-undo .
def {1} shared var tarpre-affich3 as char                    no-undo .
def {1} shared var tarpre-chrono like tarpre.chrono          no-undo .
 
def {1} shared var date-debut       like {tarcli.i}.date-debut    no-undo .
def {1} shared var ctv-motcle       like {tarcli.i}.motcle  initial "VTE"  no-undo .
 
def {1} shared var gest-famart      as char   format "x"      no-undo .
def {1} shared var nonval-seul      as char   format "x"      no-undo .
def {1} shared var choix-trt        as char   format "x"      no-undo .
def {1} shared var statut-retour    as char   format "x"      no-undo .
 
def {1} shared var taux-marge       as dec  format "->>9.99"  no-undo .
def {1} shared var typ-coef         as char format "x"        no-undo .
def {1} shared var type-trt         as char format "x"        no-undo .
 
def {1} shared var code-tarif       like {tarcli.i}.code-tarif    no-undo .
def {1} shared var codtar-cli       like {tarcli.i}.code-tarif    no-undo .
def {1} shared var codtar-rev       like {tarcli.i}.code-tarif    no-undo .
 
def {1} shared var valida           as char   format "x"      no-undo .
def {1} shared var categorie        as char   format "x(6)"   no-undo .
def {1} shared var famart-mini      as char   format "x(3)"   no-undo .
def {1} shared var famart-maxi      as char   format "x(3)"   no-undo .
def {1} shared var soufam-mini      as char   format "x(3)"   no-undo .
def {1} shared var soufam-maxi      as char   format "x(3)"   no-undo .
def {1} shared var lib-catego       as char   format "x(30)"  no-undo .
def {1} shared var nom-soufam       as char   format "x(40)"  no-undo .
def {1} shared var nom-tarif        as char   format "x(34)"  no-undo .
def {1} shared var code-fourn       as char   format "x(9)"   no-undo .
def {1} shared var lib-choix        as char   format "x(15)"  no-undo .
def {1} shared var lib-fourn        as char   format "x(32)"  no-undo .
def {1} shared var lib-tarif        as char   format "x(30)"  no-undo .
 
def {1} shared var xx1              as char                   no-undo .
def {1} shared var xx2              as char                   no-undo .
 
def {1} shared var ii               as int                    no-undo .
def {1} shared var jj               as int                    no-undo .
