/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/gesnom-4.p                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!V61 38388 27-05-13!Bug!cch            !Correction pble Nom Client non renseigné sur les lignes NOMEREF       !
!V61 38343 24-05-13!Evo!cch            !Ajout gestion Interfaces et Saisies NATHALIE - NOMEREF                !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !entete                                          !
!gesnom-0.i            !                                     !lignes                                          !
!regs-cha.i            !                                     !ARTICS                                          !
!regs-inc.i            !"magasi"                             !transp                                          !
!                      !                                     !tabges                                          !
!                      !                                     !magasi                                          !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*============================================================================*/
/*                           G E S N O M - 4 . P                              */
/* Gestion Echanges NATHALIE - NOMEREF                                        */
/*============================================================================*/
/* Extraction Livraison                                                       */
/*============================================================================*/

{ connect.i  }
{ gesnom-0.i }

def var magasi-soc   as char no-undo .
def var fic-cde      as char no-undo .
def var ok-nomeref   as log  no-undo .
def var ouv-fic      as log  no-undo .
def var ok-copie-ftp as log  no-undo .

def buffer B-ARTICS for ARTICS .
def buffer B-ENTETE for ENTETE .

FIND ENTETE where rowid( entete ) = rowid-ent no-lock no-error .
if not available entete then return .

assign  typtab = "PRM"  prefix = "NOMEREF"  codtab = "TYPCOM" .
run ch-param.p( output o-param ) .
if o-param = "" or lookup( right-trim( entete.typcom ) , o-param ) = 0 then return .  /* Type Piece non concerne par NOMEREF */

ok-nomeref = yes .

/* Tests suivant Mode Livraison en fonction du Type Piece */
/* Ex. : l-modliv = "LIM-= ENL,TRS-<>REL"                 */
if l-modliv <> ""
then do ind = 1 to num-entries( l-modliv ) :
    zone = entry( 1 , entry( ind , l-modliv ) , "-" ) .
    if entete.typcom <> zone then next .
    zone = entry( 2 , entry( ind , l-modliv ) , "-" ) .
    case substr( zone , 1 , 2 ) :
        when "=" then if entete.modliv = substr( zone , 3 )
                      then do :
                          ok-nomeref = yes .
                          leave .
                      end .
                      else ok-nomeref = no .
        when "<>" then if entete.modliv <> substr( zone , 3 )
                       then do :
                           ok-nomeref = yes .
                           leave .
                       end .
                       else ok-nomeref = no .
    end case .
end .

if not ok-nomeref then return . /* Mode Livraison non concerne par NOMEREF */

if l-onu-exclus = "" then l-onu-exclus = ",9999" .

/* Societes Regroupement */
regs-app = "NATHALIE" .
{ regs-cha.i }

{ regs-inc.i "magasi" }
magasi-soc = regs-soc .

/* Controle presence d'au moins un article avec Code ONU NOMEREF renseigne */
ok-nomeref = no .
FOR EACH LIGNES where lignes.codsoc =  entete.codsoc
                and   lignes.motcle =  entete.motcle
                and   lignes.typcom =  entete.typcom
                and   lignes.numbon =  entete.numbon
                and   lignes.articl <> ""
                use-index primaire no-lock :

    /* Acces lien Article NATHALIE - NOMEREF */
    FIND ARTICS where artics.codsoc = nomeref-soc
                and   artics.articl = lignes.articl
                and   artics.theme  = theme-trt
                and   artics.chrono = 0
                use-index primaire no-lock no-error .

    /* Acces Article NOMEREF */
    if available artics and artics.alpha-cle <> ""
    then do :

       FIND B-ARTICS where b-artics.codsoc    = nomeref-soc
                     and   b-artics.theme     = theme-trt
                     and   b-artics.alpha-cle = artics.alpha-cle
                     and   b-artics.articl    = ""
                     use-index theme no-lock no-error .

       if available b-artics and lookup( right-trim( b-artics.alpha[ 4 ] ) , l-onu-exclus ) = 0 /* Test validite Code ONU */
       then do :
           ok-nomeref = yes . /* Generation Fichier NOMEREF */
           leave .
       end .

    end .

END . /* FOR EACH LIGNES */

if not ok-nomeref then return . /* Bon non concerne par NOMEREF */

if num-entries( l-param-cde ) < 12  then l-param-cde  = "O,cd,ftp -i -n,<<,FIN_SCRIPT,quote,user,pass,prompt,ascii,put,bye" .
if entry( 1, l-param-cde )    = "O" then ok-copie-ftp = true .
if condit                     = ""  then condit = "KG" .

REPEAT  :

    fic-trt = caps( substr( trim( entete.typcom ) , 1 , 1 ) )    +
              substr( string( year( today ) , "9999" ) , 3 , 2 ) +
              string( month( today ) , "99" )                    +
              string( day( today ) , "99" )                      +
              substr( string( time, "hh:mm:ss" ), 1, 2 )         +
              substr( string( time, "hh:mm:ss" ), 4, 2 )         +
              substr( string( time, "hh:mm:ss" ), 7, 2 )         +
              "."                                                +
              trim( entete.magasin ) .

    if search( rep-depart + fic-trt )     <> ? or
       search( rep-dep-traite + fic-trt ) <> ?
    then do :
        pause 1 no-message .
        next .
    end .

    leave .

END .

/* Generation Fichier NOMEREF */
FOR EACH LIGNES where lignes.codsoc =  entete.codsoc
                and   lignes.motcle =  entete.motcle
                and   lignes.typcom =  entete.typcom
                and   lignes.numbon =  entete.numbon
                and   lignes.articl <> ""
                use-index primaire no-lock :

    if not ouv-fic
    then do :

        output stream s-fic to value( rep-depart + fic-trt ) . /* Ouverture Fichier NOMEREF */

        /* Enregistrement Entete */
        if entete.transp <> ""
        then do :
            zone = right-trim( entete.transp ) .

            FIND TRANSP where transp.codsoc = ""
                        and   transp.transp = entete.transp
                        no-lock no-error .

            if available transp then zone = zone + separat-zone[ 2 ] + transp.libel1[ mem-langue ] .
        end .

        else if entete.modliv <> ""
             then do :
                 zone = right-trim( entete.modliv ) .

                 FIND TABGES where tabges.codsoc = ""
                             and   tabges.etabli = ""
                             and   tabges.typtab = "APR"
                             and   tabges.prefix = "MODLIV"
                             and   tabges.codtab = entete.modliv
                             no-lock no-error .

                 if available tabges then zone = zone + separat-zone[ 2 ] + tabges.libel1[ mem-langue ] .
             end .

             else zone = "" .

        assign enreg                                  = fill( separat-zone[ 1 ] , 13 ) 
               entry( 1 , enreg , separat-zone[ 1 ] ) = right-trim( entete.typcom )  + separat-zone[ 2 ] +
                                                        right-trim( entete.magasin ) + separat-zone[ 2 ] +
                                                        right-trim( entete.numbon )
               entry( 5 , enreg , separat-zone[ 1 ] ) = zone
               ouv-fic                                = yes
               .

        put stream s-fic unformatted enreg skip .

    end . /* if not ouv-fic */

    /* Enregistrement Ligne */
    enreg = fill( separat-zone[ 1 ] , 14 ) .

    /* Acces lien Article NATHALIE - NOMEREF */
    FIND ARTICS where artics.codsoc = nomeref-soc
                and   artics.articl = lignes.articl
                and   artics.theme  = theme-trt
                and   artics.chrono = 0
                use-index primaire no-lock no-error .

    if not available artics or artics.alpha-cle = "" then next .

    assign entry( 1 , enreg , separat-zone[ 1 ] ) = right-trim( artics.alpha-cle )
           zone                                   = ""
           .

    if lignes.magasin-2 <> "" /* Transfert */
    then do :
        FIND MAGASI where magasi.codsoc = magasi-soc
                    and   magasi.codtab = lignes.magasin-2
                    no-lock no-error .

        if available magasi then zone = magasi.libel1[ mem-langue ] .
    end .
    else if entete.adres[ 1 ] = "" /* Bon de Chargement */
         then do :

             FIND B-ENTETE where b-entete.codsoc = entete.codsoc
                           and   b-entete.motcle = entete.motcle
                           and   b-entete.typcom = lignes.typcom-reliq
                           and   b-entete.numbon = lignes.numbon-reliq
                           no-lock no-error .

             if available b-entete then zone = b-entete.adres[ 1 ] .

         end .

         else zone = entete.adres[ 1 ] . /* Livraison Client */

    assign zone = right-trim( substr( zone , 1 , 20 ) )                                                 + separat-zone[ 3 ] +
                  right-trim( substr( lignes.numbon , length( right-trim( lignes.numbon ) ) - 7 , 6 ) ) + separat-zone[ 2 ] +
                  string( lignes.chrono )                                                               + separat-zone[ 2 ] +
                  right-trim( lignes.articl )

           entry( 2 , enreg , separat-zone[ 1 ] ) = zone
           entry( 4 , enreg , separat-zone[ 1 ] ) = condit
           entry( 5 , enreg , separat-zone[ 1 ] ) = string( round( lignes.qte[ 2 ] * lignes.pdsuni , 3 ) )
           entry( 6 , enreg , separat-zone[ 1 ] ) = entry( 5 , enreg , separat-zone[ 1 ] )
           .

    put stream s-fic unformatted enreg skip .

END . /* FOR EACH LIGNES */

if not ouv-fic then return .

output stream s-fic close . /* Fermeture Fichier NOMEREF */

if ok-copie-ftp and serv-nomeref <> "" and login-nomeref <> "" and pass-nomeref <> "" and rep-nomeref <> ""
then do :

    /* Constitution Fichier de Commande pour copie Fichier sur Serveur NOMEREF */
    fic-cde = rep-travail + fic-trt + ".com" .
    output stream s-fic to value( fic-cde ) .

    put stream s-fic unformatted entry( 2, l-param-cde ) + " " + rep-depart skip .                                    /* Positionnement ds repertoire depart DEAL       */
    put stream s-fic unformatted entry( 3, l-param-cde ) + " " + serv-nomeref + " " + 
                                 entry( 4, l-param-cde ) + " " + entry( 5, l-param-cde )  skip .                      /* Connection sur serveur distant                 */
    put stream s-fic unformatted entry( 6, l-param-cde ) + " " + entry( 7, l-param-cde ) + " " + login-nomeref skip . /* Login serveur distant                          */
    put stream s-fic unformatted entry( 6, l-param-cde ) + " " + entry( 8, l-param-cde ) + " " + pass-nomeref skip .  /* Mot de passe serveur distant                   */
    put stream s-fic unformatted entry( 9, l-param-cde ) skip .                                                       /* Affichage off                                  */
    put stream s-fic unformatted entry( 2, l-param-cde ) + " " + rep-nomeref skip .                                   /* Positionnement ds rep. serveur distant         */
    put stream s-fic unformatted entry( 10, l-param-cde ) skip .                                                      /* Transfert Type ASCII                           */
    put stream s-fic unformatted entry( 11, l-param-cde ) + " " + fic-trt skip .                                      /* Copie fichier                                  */
    put stream s-fic unformatted entry( 12, l-param-cde ) skip .                                                      /* Deconnection ftp                               */
    put stream s-fic unformatted entry( 5, l-param-cde ) skip .                                                       /* Fin Script                                     */

    output stream s-fic close .

    /* Rendre le script excutable */
    if opsys = "UNIX"  then  os-command silent value( "chmod 777 " + fic-cde ) .

    /* Copie Fichier par Ftp sur Serveur NOMEREF */
    os-command value( fic-cde + " >> " +  rep-travail + "nomeref.log") .

    /* Suppression Fichier de Commande */
    os-delete value( fic-cde ) .

    /* Sauvegarde ou Suppression Fichier NOMEREF */
    if rep-dep-traite <> "" then os-rename value( rep-depart + fic-trt ) value( rep-dep-traite + fic-trt + ".trt" ) . /* Sauvegarde  */
                            else os-delete value( rep-depart + fic-trt ) .                                            /* Suppression */

end . /* if ok-copie-ftp and ... */


