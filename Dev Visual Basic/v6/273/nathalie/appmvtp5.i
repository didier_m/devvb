/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/appmvtp5.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 17710 10-08-09!Evo!cch            !Ajout gestion des Types de Pi�ce 'RAR'                                !
!V52 17233 14-10-08!Evo!cch            !Envoi qt� commandes tjrs en positif ( Modification CCO EUREA )        !
!V52 16606 29-01-08!Evo!cch            !Ajout gestion des chronos reliq su 4 positions                        !
!V52 16430 19-11-07!Bug!cch            !Correcrtion probl�me de d�calage si code TVA ligne non charg�e        !
!V52 16177 24-08-07!Bug!cch            !Correction probl�mes suite fiche 16032                                !
!V52 16032 03-07-07!Evo!cch            !R�ceptions des CDF S.D. faites en magasin et non plus au si�ge        !
!V52 15871 03-05-07!Evo!cch            !Ajout extraction des commandes Fournisseurs CAF Eurea Distri          !
!V52 15681 09-02-07!Bug!cch            !Correction probl�me position 56 sur 6 et 686 sur 2                    !
!V52 15669 07-02-07!Evo!cch            !Passage type de mvt diff�rent � APPMVTP5.I selon type cde d'origine   !
!V52 15668 07-02-07!Bug!cch            !Si LIV magas. r�cup�r� pass� � appmvtp5.i et plus de r�cup. ds codaux !
!V52 15622 23-01-07!New!cch            !Traitement g�n�ration des bons pour EUREA DISTRI                      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           A P P M V T P 5 . I                            */
/* Extraction des Mouvements pour les Magasins ( CDF, TRE ... ) PDP V5      */
/*--------------------------------------------------------------------------*/
/* Procedure MAJ Fichier ASCII                                CC 12/09/2000 */
/*==========================================================================*/
/* Plus de CDS pour les REA Magasins mais CDF avec DECLIC ( CC 26/09/2002 ) */

PROCEDURE APPMVTP5 :

  def input parameter recid-entete as recid .
  def input parameter i-param      as char .

  def var l-typcom    as char         no-undo .
  def var i           as int          no-undo .
  def var pos-deb     as int          no-undo .
  def var ind-qte     as int          no-undo .
  def var zone        as char         no-undo .
  def var nb-lig      as int          no-undo .
  def var ht-lig      as dec          no-undo .
  def var ttc-lig     as dec          no-undo .
  def var tva-lig     as dec          no-undo .
  def var zone-tva    as char         no-undo .
  def var taux-tva    as dec          no-undo .
  def var typtva-exo  as char         no-undo .
  def var codtva-exo  as char         no-undo .
  def var base-tva    as dec extent 7 no-undo .
  def var mht-tva     as dec extent 7 no-undo .
  def var l-param     as char         no-undo .
  def var code-mag    as char         no-undo .
  def var code-soc    as char         no-undo .
  def var codsoc-sav  as char         no-undo .
  def var top-appel   as int          no-undo .
  def var qte-env     as dec          no-undo .
  def var env-mag     as char         no-undo .
  def var l-art-fr-po as char         no-undo .
  def var code-mvt    as char         no-undo .
  def var type-mvt    as char         no-undo .

  def buffer p5-entete for entete .
  def buffer p5-magasi for magasi .
  def buffer p5-transp for transp .

  FIND P5-ENTETE where recid( p5-entete ) = recid-entete no-lock no-error .

  if not available p5-entete then return .

  assign code-mag = p5-entete.magasin
         env-mag  = "EM"
         l-typcom = "TRE,RCF,LIV,CAR,CAF,CDF,RAR"
         ind-qte  = 2
         .

  if program-name(2) begins "ext-ent1" /* Extractions des Entrees pour Magasins */
  then do :

      code-mag = entry( 3, i-param, "|" ) .

      if lookup( right-trim( p5-entete.typcom ), l-typcom ) < 4
         then top-appel = 2 .
         else assign top-appel = 3 
                     ind-qte   = 1
                     .
  end .

  FIND P5-MAGASI where p5-magasi.codsoc = ""
                 and   p5-magasi.codtab = code-mag
                 no-lock no-error .

  if not available p5-magasi then return .

  if p5-magasi.equipe <> "1"   or /* Non Informatise */
     p5-magasi.nombre[30] <> 1    /* Non PDP V5      */
     then return .

  if program-name(2) begins "ext-ent1" then code-soc = p5-magasi.societe .

  if program-name(2) begins "gespla-"  or /* Integration BL plateformes */
     program-name(4) begins "gespla-9" or /* Integration ARC LISADIS    */
     program-name(2) begins "gesedi-4" or /* Integration BL EDIFACT     */
     program-name(2) begins "int-blp1" or /* Integration BL platef. anc. version pour LISADIS ( CC 01/10/04 ) */
     program-name(2) begins "genb-liv"    /* Generation des Liv. en Soc. S.D. + E.D. */
  then do :

      /* Pas d'envoi en periode d'inventaire
         On teste les dates d'inventaire du magasin et non plus le
         parametrage inventaire ( CC 18/06/2001 )
      */
      ok-eli = no .
      if not program-name(4) begins "gespla-9"         and /* Integration ARC LISADIS */
         date-jour         >= p5-magasi.datdeb-inv - 3 and
         p5-entete.datdep  <= p5-magasi.datfin-inv     and
         p5-entete.datdep  <> ?                        and
         p5-magasi.datdeb-inv <> ?                     and
         p5-magasi.datfin-inv <> ?
         then ok-eli = yes .

      assign code-mag  = p5-entete.magasin
             code-soc  = p5-entete.codsoc
             top-appel = 1
             .

      if program-name(4) begins "gespla-9" then assign top-appel = 3 /* Integration ARC LISADIS */
                                                       ind-qte   = 1
                                                       .

  end .

  if code-mag = "039" and code-soc = "05" then code-soc = "37" .

  assign codsoc-sav = codsoc-soc
         codsoc-soc = code-soc
         typtva-exo = "002"
         codtva-exo = "000"
         typtab     = "PRM"
         prefix     = "INFO-TVA"
         codtab     = ""
         .

  run ch-parax.p( output l-param ) .
  if l-param <> ""
     then assign typtva-exo = entry( 1, l-param, "/" )
                 codtva-exo = entry( 2, l-param, "/" )
                 .
  codsoc-soc = codsoc-sav .

  l-art-fr-po = entry( 2, i-param, "|" ) no-error .

  LECT-LIGNES :
  FOR EACH LIGNES where lignes.codsoc = p5-entete.codsoc
                  and   lignes.motcle = p5-entete.motcle
                  and   lignes.typcom = p5-entete.typcom
                  and   lignes.numbon = p5-entete.numbon
                  and   lignes.qte[ ind-qte ] <> 0
                  use-index primaire exclusive-lock :

    /* On n'envoie pas les lignes de frais et port commandes CAR */
    if l-art-fr-po <> "" and lookup( right-trim( lignes.articl ), l-art-fr-po ) > 1 then next .

    do i = 1 to num-entries( lignes.zone-trav ) :
        if entry( i, lignes.zone-trav ) begins env-mag /* Envoye Magasin */
           then next LECT-LIGNES .
    end .

    assign nb-lig  = nb-lig + 1
           ht-lig  = lignes.htnet
           ttc-lig = lignes.fac-ttc
           .

    if ht-lig = 0 and lignes.qte[ ind-qte ] <> 0 and lignes.top-prix <> "1"
    then do :

        ht-lig = lignes.qte[ ind-qte ] * lignes.pu-net .
        if lignes.coef-fac < 0 then ht-lig = - ht-lig / lignes.coef-fac .
                               else ht-lig = ht-lig * lignes.coef-fac .
        ht-lig = round( ht-lig, 2 ) .

        case lignes.top-ttc :
            when "1" then do :
                assign zone-tva = lignes.ttva
                       taux-tva = 0
                       .
                { lec-taxe.i  tabges  zone-tva  p5-entete.datdep  taux-tva }
                ht-lig = round( ht-lig / ( 1 + taux-tva ), 2 ) .
            end .
        end case .

    end .

    /* Calculs des cumuls entete a envoyer */
    case p5-entete.type-tva :

        when typtva-exo then assign base-tva[ 1 ] = base-tva[ 1 ] + ht-lig /* Exo. */
                                    ttc-lig       = ht-lig
                                    .

        otherwise do :

            if ttc-lig = 0 and lignes.qte[ ind-qte ] <> 0 and lignes.top-prix <> "1"
               then case lignes.top-ttc :

                   when "1" /* Pu TTC */
                   then if lignes.coef-fac >= 0
                           then ttc-lig = round ( lignes.qte[ ind-qte ] * lignes.pu-net *
                                          lignes.coef-fac, 2 ) .
                           else ttc-lig = round ( ( lignes.qte[ ind-qte ] * lignes.pu-net )
                                          / - lignes.coef-fac, 2 ) .

                   otherwise do :

                       assign zone-tva = lignes.ttva
                              taux-tva = 0
                              .                        

                       { lec-taxe.i  tabges  zone-tva  p5-entete.datdep  taux-tva }
                       ttc-lig = round ( ht-lig * ( 1 + taux-tva ), 2 ) .

                   end .

               end case . /* case lignes.top-ttc : */

            tva-lig = ttc-lig - ht-lig .

            case lignes.ttva :

                when "001"
                then assign base-tva[ 4 ] = base-tva[ 4 ] + ht-lig   /* 5.5  */
                            mht-tva[ 4 ]  = mht-tva[ 4 ] + tva-lig .
                when "004"
                then assign base-tva[ 5 ] = base-tva[ 5 ] + ht-lig  /* 2.1  */
                            mht-tva[ 5 ]  = mht-tva[ 5 ] + tva-lig .
                when "005"
                then assign base-tva[ 3 ] = base-tva[ 3 ] + ht-lig  /* 19.6 */
                            mht-tva[ 3 ]  = mht-tva[ 3 ] + tva-lig .

            end case . /* case lignes.ttva : */

        end . /* otherwise do : */

    end . /* case p5-entete.type-tva : */

    /* Affectation enregistrement */
    case top-appel :                                         /* 0001 - 008 */
        when 3 then zone = "COMMANDE" .
        otherwise   zone = "APPMVT  " .
    end case .

    zone = zone                                            +
           fill( " ", 3 )                                  + /* 0009 - 003 */
           string( code-mag, "xxx" )                         /* 0012 - 003 */
           .

    if entry( 1, i-param, "|" ) = "REJET" and top-appel <> 3 /* 0015 - 013 */
       then zone = zone                             +
                   string( lignes.numbon, "x(10)" ) +
                   ".88" .
       else zone = zone                             +
                   string( lignes.numbon, "x(10)" ) +
                   ".99" .

    if program-name(2) begins "genb-liv" /* Generation automatique livraisons E.D. + S.D. */
       then zone = zone + entry( 4, i-param, "|" ) .         /* 0028 - 002 */
       else case lignes.typcom :
                when entry( 1, l-typcom ) then zone = zone + "01" .
                when entry( 2, l-typcom ) then zone = zone + "11" .
                when entry( 3, l-typcom ) then zone = zone + "21" .
                when entry( 4, l-typcom ) then zone = zone + "31" .
                when entry( 5, l-typcom ) then zone = zone + "31" .
                when entry( 6, l-typcom ) then zone = zone + "31" .
                when entry( 7, l-typcom ) then zone = zone + "51" .
            end case .

    zone = zone                                            +
           string( lignes.chrono, "999" )                    /* 0030 - 003 */
           .

    case top-appel :                                         /* 0033 - 002 */
        when 3 then zone = zone + "CR" .
        otherwise   zone = zone + "AP" .
    end case .


    if program-name(2) begins "genb-liv" /* Generation automatique livraisons E.D. + S.D. */
       then zone = zone + entry( 5, i-param, "|" )  .        /* 0035 - 003 */
       else case lignes.typcom :
                when entry( 1, l-typcom ) then zone = zone + "TRE" .
                when entry( 4, l-typcom ) then zone = zone + "RDC" .
                when entry( 5, l-typcom ) then zone = zone + "RDC" .
                when entry( 6, l-typcom ) then zone = zone + "RDC" .
                when entry( 7, l-typcom ) then zone = zone + "EFD" .
                otherwise                      zone = zone + "EFO" .
       end case .

    zone = zone                                            +
           fill( " ", 2 )                                  + /* 0038 - 002 */
           "0"                                             + /* 0040 - 008 */
           string(substr( lignes.numbon, 2, 7 ), "x(7)")   +
           fill( "0", 8 )                                    /* 0048 - 008 */
           .

    case lignes.typcom :                                     /* 0056 - 009 */
        when entry( 1, l-typcom ) then zone = zone         +
             string( p5-entete.magasin-2, "xxx" )          +
             fill( " ", 6 ) .
        when entry( 3, l-typcom )
             then if program-name(2) begins "genb-liv" /* Generation automatique livraisons E.D. + S.D. */
                     then zone = zone                             +
                          string( trim( lignes.codaux ), "x(6)" ) +
                          fill( " ", 3 ) .
                     else zone = zone + "056001" + fill( " ", 3 ) .
        otherwise zone = zone                              +
             string( trim( lignes.codaux ), "x(6)" )       +
             fill( " ", 3 ) .
    end case .

    if p5-entete.totht >= 0 then zone = zone + "N" .         /* 0065 - 001 */
                            else zone = zone + "A" .

    zone = zone                                            +
           " "                                             + /* 0066 - 001 */
           "D"                                               /* 0067 - 001 */
           .

    case top-appel :
        when 3 then zone = zone + fill( " ", 10 ) .          /* 0068 - 010 */
        otherwise do :
            zone = zone + fill( "0", 7 ) .                   /* 0068 - 007 */
            case lignes.typcom :                             /* 0075 - 001 */
                when entry( 1, l-typcom ) then zone = zone + " " .
                otherwise if p5-entete.ref-magasin <> ""
                             then zone = zone + "O" .
                             else zone = zone + "N" .
            end case .
        end .
    end case .

    zone = zone                                            +
           fill( " ", 5 )                                    /* 0076 / 0078 - 005 */
           .

    case top-appel :
        when 3 then if month( p5-entete.datbon ) >= 07       /* 0083 - 004 */
                       then zone = zone + string( year( p5-entete.datbon ), "9999" ) .
                       else zone = zone + string( year( p5-entete.datbon ) - 1 , "9999" ) .
        otherwise   if month( p5-entete.datdep ) >= 07       /* 0081 - 004 */
                       then zone = zone + string( year( p5-entete.datdep ), "9999" ) .
                       else zone = zone + string( year( p5-entete.datdep ) - 1 , "9999" ) .
    end case .

    zone = zone                                            +
           string( lignes.articl, "x(8)" )                 + /* 0085 / 0087 - 008 */
           fill( " ", 4 )                                    /* 0093 / 0095 - 004 */
           .

    case p5-entete.type-tva :                                /* 0097 / 0099 - 001 */
        when typtva-exo then zone = zone + string( substr( codtva-exo, 3, 1 ), "x" ) .
        otherwise            zone = zone + string( substr( lignes.ttva, 3, 1 ), "x" ) .
    end case .

    case top-appel :
        when 3 then zone = zone                            +
               string( year ( p5-entete.datbon ), "9999" ) + /* 0100 - 008 */
               string( month( p5-entete.datbon ), "99" )   +
               string( day  ( p5-entete.datbon ), "99" )   +
               fill( " ", 8 )                              + /* 0108 - 008 */
               string( year ( p5-entete.datcre ), "9999" ) + /* 0116 - 008 */
               string( month( p5-entete.datcre ), "99" )   +
               string( day  ( p5-entete.datcre ), "99" )   +
               fill( " ", 3 )                                /* 0124 - 003 */
               .
        otherwise zone = zone                              +
               string( year ( p5-entete.datdep ), "9999" ) + /* 0098 - 008 */
               string( month( p5-entete.datdep ), "99" )   +
               string( day  ( p5-entete.datdep ), "99" )   +
               fill( " ", 19 )                               /* 0106 - 019 */
               .
    end case .

    zone = zone                                            +
           string( code-soc, "xx" )                        + /* 0125 / 0127 - 002 */
           fill( " ", 2 )                                  + /* 0127 / 0129 - 002 */
           string( code-soc, "xx" )                        + /* 0129 / 0131 - 002 */
           string( lignes.libart, "x(30)" )                  /* 0131 / 0133 - 030 */
           .

    case top-appel :
        when 3 then zone = zone + 
               string( substr( lignes.typcom, 2 ), "xx" ) .  /* 0163 - 002 */
         otherwise  zone = zone + fill( " ", 2 ) .           /* 0161 - 002 */
    end case .

    if ht-lig < 0                                            /* 0163 / 0165 - 010 */
       then zone = zone                                    +
            string( ht-lig * -100, "999999999-" ) .
       else zone = zone                                    +
            string( ht-lig * 100, "999999999-" ) .

    if ttc-lig < 0                                           /* 0173 / 0175 - 010 */
       then zone = zone                                    +
            string( ttc-lig * -100, "999999999-" ) .
       else zone = zone                                    +
            string( ttc-lig * 100, "999999999-" ) .
           .

    case lignes.tenusto :                                    /* 0183 / 0185 - 001 */
        when "1" then zone = zone + "0" .
        otherwise     zone = zone + "1" .
    end case .

    zone = zone                                            +
           fill( " ", 20 )                                   /* 0184 / 186 - 020 */
           .

    case lignes.typcom :                                     /* 0204 / 0206 - 007 / 008 */
        when entry( 1, l-typcom )
             then zone = zone + string( lignes.numbon, "x(8)" ) .
        when entry( 4, l-typcom ) or when entry( 5, l-typcom ) or when entry( 6, l-typcom )
             then zone = zone + fill ( "0", 7 ) .
        otherwise zone = zone + fill ( "0", 8 ) .
    end case .

    zone = zone                                            +
           fill( " ", 15 )                                   /* 0212 / 0213 - 015 */
           .

    case p5-entete.type-tva :
        when typtva-exo then zone-tva = string( substr( codtva-exo, 3, 1 ), "x" ) .
        otherwise            zone-tva = string( substr( lignes.ttva, 3, 1 ), "x" ) .
    end case .

    case zone-tva :
        when "0" then i = 1 .
        when "1" then i = 4 .
        when "3" then i = 3 .
        when "4" then i = 5 .
        when "5" then i = 3 .
        when "6" then i = 2 .
        otherwise     i = 0 .
    end case .

    zone = zone                                            +
           string( i, "9" )                                + /* 0227 / 0228 - 001 */
           " "                                               /* 0228 / 0229 - 001 */
           .

    qte-env = lignes.qte[ ind-qte ] .
    if lignes.coef-cde < 0 then qte-env = qte-env / ( - lignes.coef-cde ) .
                           else qte-env = qte-env * lignes.coef-cde .

    case lignes.nivcom :
        when "" then assign zone = zone + string( abs( qte-env ) * 1000, "99999999-" ) /* 0230 - 009 */
                            zone = zone + fill( "0", 8 ) + " "                         /* 0239 - 009 */
                            . 
        otherwise do :                           
            zone = zone + fill( "0", 8 ) + " " .                                       /* 229 - 009  */ 
            if p5-entete.totht < 0                                                     /* 238 - 009  */
               then zone = zone + string( - qte-env * 1000, "99999999-" ) .
               else zone = zone + string( qte-env * 1000, "99999999-" ) .
        end .
    end case .

    zone = zone                                            +
           string( lignes.pu-brut * 100, "9999999999-" )   + /* 0247 / 0248 - 132 */
           fill( "0", 121 )
           .

    case lignes.typcom :
        when entry( 4, l-typcom ) or when entry( 5, l-typcom ) or when entry( 6, l-typcom )
            then  zone = zone                              +
                              "130051121000"               + /* 0380 - 144 */
                              fill( "0", 132 )             +
                              fill( " ", 34 )                /* 0524 - 034 */
                              .
        otherwise zone = zone                              +
                              "130051121000"               + /* 0379 - 144 */
                              fill( "0", 132 )             +
                              fill( " ", 29 )                /* 0523 - 029*/
                              .
    end case .

    zone = zone                                            +
           fill( "0", 8 )                                  + /* 0552 / 0558 - 009 */
           " "                                             +
           fill( " ", 5 )                                  + /* 0561 / 0567 - 005 */
           "0000100 "                                        /* 0566 / 0572 - 008 */
           .

    case lignes.typcom :
        when entry( 4, l-typcom ) or when entry( 5, l-typcom ) or when entry( 6, l-typcom )
            then zone = zone                                        +
                        fill( "0", 11 )                             + /* 0580 - 011 */
                        fill( " ", 3 )                              + /* 0591 - 003 */
                        "N"                                         + /* 0594 - 001 */
                        fill( " ", 3 )                              + /* 0595 - 011 */
                        fill( "0", 8 )                              + /* 0598 - 003 */
                        string( code-mag, "xxx" )                   + /* 0606 - 003 */
                        string( year ( p5-entete.datdep ), "9999" ) + /* 0609 - 008 */
                        string( month( p5-entete.datdep ), "99" )   +
                        string( day  ( p5-entete.datdep ), "99" )   +
                        string( year ( p5-entete.datliv ), "9999" ) + /* 0617 - 008 */
                        string( month( p5-entete.datliv ), "99" )   +
                        string( day  ( p5-entete.datliv ), "99" )   +
                        "0"                                         + /* 0625 - 004 */
                        string( code-mag, "xxx" )                   +
                        fill( " ", 32 )                             + /* 0629 - 032 */
                        "B"                                         + /* 0661 - 001 */
                        fill( " ", 4 )                              + /* 0662 - 004 */
                        "TE"                                        + /* 0666 - 002 */
                        fill( " ", 2 )                              + /* 0668 - 002 */
                        fill( "0", 10 )                               /* 0670 - 010 */
                        .
        otherwise do :

            zone = zone                                             +
                   fill( " ", 9 )                                   + /* 0574 - 009 */
                   fill( "0", 8 )                                   + /* 0583 - 009 */
                   " "
                   .

            case lignes.typcom :                                    /* 0592 - 014 */
                when entry( 1, l-typcom )
                then      zone = zone + fill( "0", 11 ) + fill( " ", 3 ) .
                when entry( 2, l-typcom )
                then      zone = zone + string( int( substr( p5-entete.ref-magasin, 4, 7 )
                                        ), "99999999" )
                                      + substr( string( lignes.chrono-reliq, "9999" ), 2, 3 )
                                      + string( code-mag, "xxx" )
                                      .
                when entry( 7, l-typcom )
                then      zone = zone + fill( " ", 14 ) .
                otherwise zone = zone + string( int( substr( p5-entete.ref-magasin, 4, 7 )
                                        ), "99999999" )
                                      + substr( string( lignes.chrono-reliq, "9999" ), 2, 3 )
                                      + string( substr( p5-entete.codaux, 8, 3 ), "xxx" )
                                      .
            end case .

            zone = zone                                             +
                   "N"                                              + /* 0606 - 001 */
                   fill( " ", 3 )                                   + /* 0607 - 003 */
                   fill( "0", 16 )                                  + /* 0610 - 017 */
                   " "                                              +
                   "0"                                              + /* 0627 - 004 */
                   string( code-mag, "xxx" )                        +
                   string( p5-entete.transp, "x(8)" )                 /* 0631 - 008 */
                   .

            FIND P5-TRANSP where p5-transp.codsoc = ""
                           and   p5-transp.transp = p5-entete.transp
                           no-lock no-error .

            if available p5-transp                                    /* 0639 - 010 */
               then zone = zone + string( p5-transp.immat, "x(10)" ) .
               else zone = zone + fill( " ", 10 ) .

            zone = zone                                             +
                   string( year ( p5-entete.datcre ), "9999" )      + /* 0649 - 008 */
                   string( month( p5-entete.datcre ), "99" )        +
                   string( day  ( p5-entete.datcre ), "99" )        +
                   fill( " ", 14 )                                  + /* 0657 - 014 */
                   fill( "0", 5 )                                   + /* 0671 - 006 */
                   " "                                              +
                   fill( " ", 5 )                                   + /* 0677 - 005 */
                   string( p5-entete.modliv, "xx" )                 + /* 0682 - 002 */
                   "TE"                                               /* 0684 - 002 */
                   .

            if lignes.nivcom > "3" then zone = zone + "TM" .          /* 0686 - 002 */
                                   else zone = zone + fill( " ", 2 ) .  

            zone = zone                                             +
                   fill( "0", 3 ) /* Chgt en fin FOR EACH */        + /* 0688 - 003 */
                   fill( "0", 18 )                                  + /* 0691 - 018 */
                   string( p5-entete.ref-tiers, "x(15)" )             /* 0709 - 015 */                   
                   .

        end . /* otherwise do : */

    end case .

    zone = zone                                                     +
           "N"                                                      + /* 0724 / 0680 - 001 */
           " "                                                        /* 0725 / 0681 - 001 */
           .

    if p5-entete.totht < 0                                            /* 0726 / 0682 - 010 */
       then zone = zone                                             +
                   string( p5-entete.totht * -100, "999999999-" ) .
       else zone = zone                                             +
                   string( p5-entete.totht * 100, "999999999-" ) .

    if p5-entete.ttc < 0                                              /* 0736 / 0692 - 010 */
       then zone = zone                                             +
                   string( p5-entete.ttc * -100, "999999999-" ) .
       else zone = zone +
                   string( p5-entete.ttc * 100, "999999999-" ) .

    zone = zone                                                     +
           fill( "0", 140 ) /* Chgt en fin FOR EACH */              + /* 0746 / 0702 - 140 */
           string( lignes.opemaj, "x(12)" )                         + /* 0886 / 0842 - 012 */
           string( p5-entete.devise, "x" )                          + /* 0898 / 0854 - 001 */
           fill( " ", 8 )                                             /* 0899 / 0855 - 008 */
           .

    case lignes.typcom :
        when entry( 4, l-typcom ) or when entry( 5, l-typcom ) or when entry( 6, l-typcom )
            then  zone = zone                                       +
                         fill( " ", 72 )                            + /* 0863 - 072 */
                         fill( "0", 95 )                            + /* 0935 - 095 */
                         " "                                        + /* 1030 - 001 */
                         fill( "0", 16 )                            + /* 1031 - 016 */
                         fill( " ", 2 )                               /* 1047 - 002 */
                         .
        otherwise zone = zone                                       +
                         fill( "0", 111 )                             /* 0907 - 111 */
                         .
    end case .

    /* Chargement table de travail */
    CREATE TRA-LIGNES .
    tra-lignes.valeur = zone .

    /* Topage de la ligne */
    if lignes.zone-trav <> "" then lignes.zone-trav = lignes.zone-trav + "," .
    lignes.zone-trav = lignes.zone-trav + env-mag +
                       string( today, "99/99/9999" ) + " " +
                       string( time, "hh:mm" ) .

    { majmoucb.i lignes }

  END . /* FOR EACH LIGNES */

  /* Inversion cumuls signe TVA */
  do i = 1 to 7 :
      if base-tva[i] < 0 then base-tva[i] = base-tva[i] * -1 .
      if mht-tva[i] < 0 then mht-tva[i] = mht-tva[i] * -1 .
  end .

  /* Ecriture Fichier ASCII */
  FOR EACH TRA-LIGNES no-lock :

      zone = tra-lignes.valeur .

      case p5-entete.typcom :
        when entry( 4, l-typcom ) or when entry( 5, l-typcom ) or when entry( 6, l-typcom ) then pos-deb = 702 .
        otherwise assign overlay( zone, 688, 3 ) = string( nb-lig, "999" )
                         pos-deb                 = 746
                         .
      end .

      do i = 1 to 14 :
          if i <= 7
             then overlay( zone, pos-deb, 10 ) =
                  string( base-tva[ i ] * 100, "999999999-" ) .
             else overlay( zone, pos-deb, 10 ) =
                  string( mht-tva[ i - 7 ] * 100, "999999999-" ) .
          pos-deb = pos-deb + 10 .
      end .

      if not ok-eli
         then put stream kan-dep-v5 unformatted zone skip .
         else put stream kan-eli-v5 unformatted zone skip .

      DELETE TRA-LIGNES .

  END . /* FOR EACH TRA-LIGNES */

END PROCEDURE .