/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/e-trf51.i                                                                    !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 15055 12-07-06!Evo!cch            !Acc�s ARTAPR avec soci�t� de regroupement suite mise en place ED      !
!V52 14816 18-05-06!Evo!cch            !Modification test sur provenance LISADIS ligne 343. Proven. "9" -> "8"!
!V52 13509 17-10-05!Evo!cch            !Ajout recherche r�ceptions dans BASTAT si inexistantes dans LIGNES    !
!V52 13505 17-10-05!Bug!cch            !Correction erreur format sur compteur + mauvaise recherche r�ceptions !
!V52 11571 06-12-04!Evo!erk            !Migration STOARR vers STOCK                                           !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                            E - T R F 5 1 . I                             */
/*                         Edition Tarif Articles                           */
/*==========================================================================*/

/*    Entete Famille Articles   */

DO WHILE RUPTURE-ETAT = "T"  AND  RUPTURE-A = 1 :

   assign num-famart = substr( zone , 1 , 3 )
          lib-famart = "" .
   FIND TABGES where tabges.codsoc = codsoc-bl   and
                     tabges.etabli = etabli-bl   and
                     tabges.typtab = "ART"       and
                     tabges.prefix = "FAMART"    and
                     tabges.codtab = num-famart
               no-lock  no-error .
   if available tabges then lib-famart = tabges.libel1[mem-langue] .
   leave .

END .

/*    Entete Sous-Famille Articles   */

DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 2 :

   assign num-soufam = substr( zone , 4 , 3 )
          lib-soufam = "" .
   FIND TABGES where tabges.codsoc = codsoc-bl   and
                     tabges.etabli = etabli-bl   and
                     tabges.typtab = "ART"       and
                     tabges.prefix = "SOUFAM" + substr( zone , 1 , 3 )  and
                     tabges.codtab = num-soufam
               no-lock  no-error .
   if available tabges then lib-soufam = tabges.libel1[mem-langue] .
   run value( ma-entete ) .
   leave .

END .

/*    Entete Sous-Sous-Famille Articles   */

DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 3 :

   valeur[ 11 ] = "   " + substr( zone , 7 , 3 ) .
   valeur[ 12 ] = "" .

   if valeur[ 11 ] = ""  then  leave .

   FIND TABGES where tabges.codsoc = codsoc-bl
               and   tabges.etabli = etabli-bl
               and   tabges.typtab = "ART"
               and   tabges.prefix = "SSFM" + substr( zone , 1 , 6 )
               and   tabges.codtab = substr( zone , 7 , 3 )
               no-lock  no-error .
   if available tabges  then  valeur[ 12 ] = trim( tabges.libel1[ mem-langue ] ) .

   do i = 11 to 12 :
      { maq-maj.i "02"  i  valeur[i] }
      valeur[i] = "" .
   end .

   { maq-edi.i "02" }

   leave .

END .

/*    Total Famille Articles   */

DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 1 :

   leave .

END .

/*    Total Sous-Famille Articles   */

DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 2 :

   run value( ma-pied ) .

   leave .

END .

/*    Total Sous-Sous-Famille Articles   */

DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 3 :

    { maq-edi.i "02" }

    leave .

END .

/*    Total Articles   */

DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 4 :

    code-article = substr( zone , 39 , 10 ) .
    repeat :
        FIND ARTAPR where artapr.codsoc = codsoc-dur1
                    and   artapr.articl = code-article
                    no-lock  no-error .
        /*
        if available artapr then leave .

        FIND ARTAPR where artapr.codsoc = codsoc-dur5
                    and   artapr.articl = code-article
                    no-lock  no-error .
        */
        leave .
    end .

    if not available artapr then leave .

    FIND FIRST {tarcli.i} where {tarcli.i}.motcle     = "VTE"           and
                                {tarcli.i}.codsoc     = codsoc-dur1     and
                                {tarcli.i}.typaux     = ""              and
                                {tarcli.i}.codaux     = ""              and
                                {tarcli.i}.articl     = artapr.articl   and
                                {tarcli.i}.code-tarif = code-tarif      and
                                {tarcli.i}.date-debut <= date-tarif
                                use-index primaire  no-lock  no-error .

    assign valeur[11] = artapr.articl
           valeur[12] = substring( artapr.libart1[ mem-langue ] , 1 , 30 )
           valeur[1]  = "!" + string( artapr.provenance , "x" ) + "!"
           valeur[2]  = string( artapr.typologie , "x" ) + "!"
           .

    if artapr.edi-etiq = "1" then valeur[3] = entry( 1, libelle[43] ) .
                             else valeur[3] = entry( 2, libelle[43] ) .
    assign valeur[3] = valeur[3] + "!" + string( artapr.code-blocage, "x" ) .
    if artapr.code-blocage = entry( 1, libelle[43] )
       then overlay( valeur[3], 3, 1 ) = "" .

    /*  Unite de Commande  */
    FIND ARTBIS where artbis.motcle = "VTE"
                and   artbis.codsoc = artapr.codsoc
                and   artbis.articl = artapr.articl
                and   artbis.typaux = ""
                and   artbis.codaux = ""
                no-lock  no-error .

    if available artbis then
       valeur[ 7 ] = string( artbis.ucde , "xx" ) + "!" .

    /*  Taux de TVA    */
    zone-tva = artapr.tva .
    { lec-taxe.i  tabges  zone-tva  today  taux-tva }
    valeur[ 8 ] = substr( string( taux-tva * 100 , "z9.99" ) , 1 , 4 ) .

    /*  Prix de LOIRAGRI ou ASEC */
    if tarif-ttc-1 = yes /* TTC */
    then do :

        valeur[ 13 ] = "" .

        if available {tarcli.i}
        then do :

            valeur[ 4 ] = string( {tarcli.i}.valeur, msq-prx[ nb-dec-ttc-1 ] ) .

            /* Acces au HT */
            FIND LAST B-TARCLI where b-tarcli.motcle     = {tarcli.i}.motcle
                               and   b-tarcli.codsoc     = {tarcli.i}.codsoc
                               and   b-tarcli.typaux     = {tarcli.i}.typaux
                               and   b-tarcli.codaux     = {tarcli.i}.codaux
                               and   b-tarcli.articl     = {tarcli.i}.articl
                               and   b-tarcli.code-tarif = libelle[ 41 ]
                               and   b-tarcli.date-debut = {tarcli.i}.date-debut
                               use-index primaire no-lock no-error .

            if available b-tarcli
               then valeur[ 13 ] = string( b-tarcli.valeur, msq-prx[ nb-dec-ht-1 ] ) .

            if valeur[ 13 ] = "" then valeur[ 9 ] = string( {tarcli.i}.valeur /
                                      ( 1 + taux-tva ), msq-prx[ nb-dec-ht-1 ] ) .

        end . /* if available {tarcli.i} */

    end . /* if tarif-ttc-1 = yes */

    else do : /* HT */

        valeur[ 4 ] = "" .

        if available {tarcli.i}
        then do :

            valeur[ 13 ] = string( {tarcli.i}.valeur, msq-prx[ nb-dec-ht-1 ] ) .

            /* Acces au TTC */
            FIND LAST B-TARCLI where b-tarcli.motcle     = {tarcli.i}.motcle
                               and   b-tarcli.codsoc     = {tarcli.i}.codsoc
                               and   b-tarcli.typaux     = {tarcli.i}.typaux
                               and   b-tarcli.codaux     = {tarcli.i}.codaux
                               and   b-tarcli.articl     = {tarcli.i}.articl
                               and   b-tarcli.code-tarif = libelle[ 40 ]
                               and   b-tarcli.date-debut = {tarcli.i}.date-debut
                               use-index primaire no-lock no-error .

            if available b-tarcli
               then valeur[ 4 ] = string( b-tarcli.valeur, msq-prx[ nb-dec-ttc-1 ] ) .

            if valeur[ 4 ] = "" then valeur[ 4 ] = string( {tarcli.i}.valeur *
                                     ( 1 + taux-tva ), msq-prx[ nb-dec-ttc-1 ] ) .

        end . /* if available {tarcli.i} */

    end . /* else do : /* HT */ */

    /* Prix de AGRI HTE LOIRE */
    if codsoc-dur5 <> ""
    then repeat :

        FIND FIRST {tarcli.i} where {tarcli.i}.motcle     = "VTE"           and
                                    {tarcli.i}.codsoc     = codsoc-dur5     and
                                    {tarcli.i}.typaux     = ""              and
                                    {tarcli.i}.codaux     = ""              and
                                    {tarcli.i}.articl     = artapr.articl   and
                                    {tarcli.i}.code-tarif = code-tarif      and
                                    {tarcli.i}.date-debut <= date-tarif
                                    use-index primaire  no-lock  no-error .

        if not available {tarcli.i} then leave .

        if tarif-ttc-2 = yes /* TTC */
        then do :

            assign valeur[ 14 ]  = string( {tarcli.i}.valeur, msq-prx[ nb-dec-ttc-1 ] )
                   valeur[ 15 ]  = ""
                   .

            /* Acces au HT */
            FIND LAST B-TARCLI where b-tarcli.motcle     = {tarcli.i}.motcle
                               and   b-tarcli.codsoc     = codsoc-dur5
                               and   b-tarcli.typaux     = {tarcli.i}.typaux
                               and   b-tarcli.codaux     = {tarcli.i}.codaux
                               and   b-tarcli.articl     = {tarcli.i}.articl
                               and   b-tarcli.code-tarif = libelle[ 41 ]
                               and   b-tarcli.date-debut = {tarcli.i}.date-debut
                               use-index primaire no-lock no-error .

            if available b-tarcli
               then valeur[ 15 ] = string( b-tarcli.valeur, msq-prx[ nb-dec-ht-2 ] ) .


            if valeur[ 15 ] = "" then valeur[ 15 ] = string( {tarcli.i}.valeur /
                                      ( 1 + taux-tva ), msq-prx[ nb-dec-ht-1 ] ) .

        end .

        else do : /* HT */

            assign valeur[ 14 ]  = ""
                   valeur[ 15 ] = string( {tarcli.i}.valeur, msq-prx[ nb-dec-ht-2 ] )
                   .

            /* Acces au TTC */
            FIND LAST B-TARCLI where b-tarcli.motcle     = {tarcli.i}.motcle
                               and   b-tarcli.codsoc     = codsoc-dur5
                               and   b-tarcli.typaux     = {tarcli.i}.typaux
                               and   b-tarcli.codaux     = {tarcli.i}.codaux
                               and   b-tarcli.articl     = {tarcli.i}.articl
                               and   b-tarcli.code-tarif = libelle[ 40 ]
                               and   b-tarcli.date-debut = {tarcli.i}.date-debut
                               use-index primaire no-lock no-error .

            if available b-tarcli
               then valeur[ 14 ] = string( b-tarcli.valeur, msq-prx[ nb-dec-ttc-2 ] ) .

            if valeur[ 14 ] = "" then valeur[ 14 ] = string( {tarcli.i}.valeur *
                                      ( 1 + taux-tva ), msq-prx[ nb-dec-ttc-2 ] ) .

        end .

        leave .

    end . /* if codsoc-dur5 <> "" */

    /* Prix Hors Taxes Revendeur Loiragri */
    FIND FIRST B-TARCLI where b-tarcli.motcle     = "VTE"
                        and   b-tarcli.codsoc     = codsoc-dur1
                        and   b-tarcli.typaux     = ""
                        and   b-tarcli.codaux     = ""
                        and   b-tarcli.articl     = artapr.articl
                        and   b-tarcli.code-tarif = libelle[ 42 ]
                        and   b-tarcli.date-debut <= date-tarif
                        use-index primaire  no-lock  no-error .

    if available b-tarcli then
       valeur[ 16 ] = string( b-tarcli.valeur , msq-prx[ nb-dec-rev ] ) .

    /*  Dernier P A M P   */
/*     FIND FIRST STOARR where stoarr.codsoc       =  codsoc-dur1      */
/*                       and   stoarr.articl       =  artapr.articl    */
/*                       and   stoarr.magasin      =  ""               */
/*                       and   stoarr.date-arrete <=  date-tarif       */
/*                       use-index primaire  no-lock  no-error .       */
/*     if available stoarr then                                        */
/*         assign valeur[ 17 ] = string( stoarr.pamp , "zzzz9.999" ) . */

    FIND LAST TABGES where tabges.codsoc = codsoc-dur1
                     and   tabges.etabli = ""
                     and   tabges.typtab = "STO"
                     and   tabges.prefix = "CALEND"
                     and   tabges.nombre[ 1 ] = 1
                     and   tabges.dattab[ 1 ] <= date-tarif
                     no-lock  no-error .

    if available tabges
    then do: 
        FIND STOCKS   where stocks.codsoc    = codsoc-dur1
                      and   stocks.articl    = artapr.articl
                      and   stocks.magasin   = ""                    
                      and   stocks.evenement = "PXACHA"
                      and   stocks.exercice  = string ( tabges.nombre [2]  , "9999")
                      no-lock no-error .

        if available stocks then valeur[ 17 ] = string( stocks.prix[ int (tabges.nombre [3]) ] , "zzzz9.999" ) . 
    end.


    /*   Code Article Fournisseur */
    assign fourni-ref = ""
           typtie-ref = "TIE"
           .
    if artapr.provenance = "2"  or
       ( artapr.provenance >= "7"  and  artapr.provenance <= "8" )
       then  fourni-ref = "    802190" . /* LISADIS  */
    if artapr.provenance = "5"
       then  fourni-ref = "    800595" . /* DISPAGRI */
    if artapr.provenance = "F"
       then  fourni-ref = "    803400" . /* FLORADIS */
    if artapr.provenance = "V"
       then  fourni-ref = "    805100" . /* LISADIS VEGETALE */

    if fourni-ref <> ""  then
    do :
        FIND ARTBIS where artbis.motcle = "ACH"
                    and   artbis.codsoc = x-codsoc
                    and   artbis.articl = artapr.articl
                    and   artbis.typaux = typtie-ref
                    and   artbis.codaux = fourni-ref
                    use-index artbis-1  no-lock  no-error .

        if not available artbis  and
           ( artapr.provenance >= "7"  and  artapr.provenance <= "9" )
           then FIND ARTBIS where artbis.motcle = "ACH"
                            and   artbis.codsoc = x-codsoc
                            and   artbis.articl = artapr.articl
                            and   artbis.typaux = typtie-ref
                            and   artbis.codaux = "    800595"
                            use-index artbis-1  no-lock  no-error .
    end .

    else FIND FIRST ARTBIS where artbis.motcle = "ACH"
                           and   artbis.codsoc = x-codsoc
                           and   artbis.articl = artapr.articl
                           use-index artbis-1  no-lock  no-error .

    if available artbis then
       valeur[ 10 ] = string( artbis.articl-fou , "x(6)" ) .

    /*   Dernier Prix d'Achat   */

    FIND LAST LIGNES where lignes.codsoc   =  x-codsoc
                     and   lignes.articl   =  artapr.articl
                     and   lignes.datdep   <= date-tarif
                     and   lignes.motcle   =  "ACH"
                     and   lignes.typcom   =  "RCF"
                     and   lignes.pu-net   >  0
                     and   lignes.qte[ 2 ] >  0
                     use-index artdat no-lock no-error .

    if available lignes
    then do :

        assign coef-cde = lignes.coef-cde
               coef-fac = lignes.coef-fac
               prix     = lignes.pu-net .
        if coef-cde = 0 then coef-cde = 1 .
        if coef-fac = 0 then coef-fac = 1 .
        /* PU Stock = PU Cde Fournisseur * Coef-fac / Coef-cde */
        if lignes.coef-cde < 0
           then prix = - ( prix * coef-cde ) .
           else prix =   ( prix / coef-cde ) .
        if lignes.coef-fac < 0
           then prix = - ( prix / coef-fac ) .
           else prix =   ( prix * coef-fac ) .
        valeur[ 18 ] = string( prix , "zzzzz9.99" ) .

    end .
    else do :

        FIND LAST BASTAT where bastat.codsoc =  x-codsoc
                         and   bastat.articl =  artapr.articl
                         and   bastat.datdep <= date-tarif
                         and   bastat.motcle =  "ACH"
                         and   bastat.typcom =  "RCF"
                         and   bastat.qte    <>  0
                         and   bastat.fac-ht >  0
                         use-index artdat no-lock no-error .

        if available bastat then valeur[ 18 ] = string( bastat.fac-ht / bastat.qte , "zzzzz9.99" ) .

    end .

    do i = 1 to 19 :
       { maq-maj.i "02"  i  valeur[i] }
       valeur[ i ] = "" .
    end .

    { maq-edi.i "02" }

    leave .

END .