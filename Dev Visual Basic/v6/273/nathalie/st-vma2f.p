/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/st-vma2f.p                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 26332 15-09-11!Evo!cch            !Ajout �dition ann�e N - 2                                             !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !                                                !
!maquette.i            !                                     !                                                !
!st-vma20.i            !                                     !                                                !
!maq-edi.i             !"03"                                 !                                                !
!maq-maj.i             !7  2  lib-tot                        !                                                !
!maq-maj1.i            !"07"                                 !                                                !
!maq-edi.i             !4                                    !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                          S T - V M A 2 F . P                             */
/*          Edition Statistique des Ventes et Marges / Famille              */
/*          Edition Total famille de 001 a 010                              */
/*==========================================================================*/

{ connect.i }
{ maquette.i }
{ st-vma20.i }

nb-run = nb-run + 1 .

ma-edipied = "O" .
if ind-tot = 0 then
do :
    { maq-edi.i "03" }
end .

DO II = 1 TO 6 :

    assign jj = ii * 10
           kk = ind-tot + jj .

/* 1 = CA Vente */
    if mat-edi[ jj + 1 ] = yes then
       valeur[ jj + 1 ] = string( tot-val[ kk + 1 ] , masque-tot[ jj + 1 ]).

/* 2 = Qte Vendue */
    if mat-edi[ jj + 2 ] = yes then
        valeur[ jj + 2 ]  = string( tot-qte[ kk + 2 ] , masque-tot[ jj + 2 ] ).

/* 3 = Rotation de stock */
    if mat-edi[ jj + 3 ] = yes  and  ( tot-val[ kk + 9 ] <> 0 )
                                and  ( tot-val[ kk + 1 ] <> 0 ) then
       valeur[ jj + 3 ] = string(indrot / ( tot-val[ kk + 1 ] / tot-val[ kk + 9 ] ) , masque-tot[ jj + 3 ] ) .

/* 3 = Marge */
    if mat-edi[ jj + 6 ] = yes then
    do :
        if statut-taux <> "1"
           then valeur[ jj + 6 ] = string(tot-val[ kk + 6 ] ,masque-tot[ jj + 6 ] ) .
           else valeur[ jj + 6 ] = string( (tot-val[ kk + 1 ] * taux[ ii ] ) / 100 , masque-tot[ jj + 6 ]).
    end .

/* 5 CA au Pamp */
    if mat-edi[ jj + 5 ] = yes
       then if statut-taux <> "1"
               then valeur[ jj + 5 ] = string( tot-val[ kk + 5 ] , masque-tot[ jj + 5 ] ) .
               else valeur [ jj + 5 ] = string( tot-val[ kk + 1 ] - tot-val[ kk + 6 ] , masque-tot[ jj + 5 ] ) .

/* 7 = Taux */
    if mat-edi[ jj + 7 ] = yes  and  tot-val[ kk + 1 ] <> 0 then
       if statut-taux <> "1"
          then valeur[ jj + 7 ] = string( ( tot-val[ kk + 6] / tot-val[ kk + 1 ]) * 100 , masque-tot[ jj + 7 ] ) .
          else valeur[ jj + 7 ] = string( ( tot-val[ kk + 6 ] / tot-val[ kk + 1 ]) * 100 , masque-tot[ jj + 7 ] ) .

/* 8 = Stock fin de mois */
    if mat-edi[ jj + 8 ] = yes then
       valeur[ jj + 8 ] = string( tot-sto[ kk + 8 ], masque-tot[ jj + 8 ]).

/* 9 = Valeur stock au PV standard */
    if mat-edi[ jj + 9 ] = yes then
       valeur[ jj + 9 ] = string( tot-val[ kk + 9 ] , masque-tot[ jj + 9 ]).

/*  40 = Qte Ajustement */
    if mat-edi[ jj + 40 ] = yes then 
       valeur[ jj + 40 ]  = string( tot-qte[ kk + 40 ], masque-tot[ jj + 40 ] ).

END .  /*   DO   */

{ maq-maj.i  7  2  lib-tot }

{ maq-maj1.i  "07"  valeur }
{ maq-edi.i   "07"  }

valeur = "" .

{ maq-edi.i  4 }

assign ma-edipied = ""
       ma-nblig   = 0 .

