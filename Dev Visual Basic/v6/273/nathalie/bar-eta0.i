/*==========================================================================*/
/*                          B A R - E T A 0 . I                             */
/*       GESTION des BAREMES       : EDITION DEs BAREMES / Quantite         */
/*       Gestion des variables communes                                     */
/*==========================================================================*/



def {1} shared var zone          as char                            no-undo .
def {1} shared var ind-tot       as int                             no-undo .
def {1} shared var recid-barstat as recid                           no-undo .
def {1} shared var auxili-soc    as char                            no-undo .

def {1} shared var tot-qte       as dec                 extent   9  no-undo .
def {1} shared var tot-gen       as dec                 extent 209  no-undo .

def {1} shared var annee-exe     as char                            no-undo .
def {1} shared var sem1-deb      as date format "99/99/9999"        no-undo .
def {1} shared var sem1-fin      as date format "99/99/9999"        no-undo .
def {1} shared var sem2-deb      as date format "99/99/9999"        no-undo .
def {1} shared var sem2-fin      as date format "99/99/9999"        no-undo .
def {1} shared var numjour       as char                            no-undo .
def {1} shared var jour-t        as int                             no-undo .
def {1} shared var mois-t        as int                             no-undo .
def {1} shared var anne-t        as int                             no-undo .
def {1} shared var date-test     as date format "99/99/9999"        no-undo .
def {1} shared var mois-lu       as int                             no-undo .

def {1} shared var lib-mois      as char format "x(7)"              no-undo .

def {1} shared var libelle       as char                 extent 99   no-undo .
def {1} shared var valeur        as char                 extent 30   no-undo .

/* zones frames */
def {1} shared var date-exe     as date format "99/99/9999"         no-undo .
def {1} shared var mois-deb     as char format "x(6)"               no-undo .
def {1} shared var mois-fin     as char format "x(6)"               no-undo .
def {1} shared var magasins     as char format "x(40)"              no-undo .
def {1} shared var familles     as char format "x(40)"              no-undo .
def {1} shared var maquette     as char format "x(12)"              no-undo .
def {1} shared var lib-maq      as char format "x(30)"              no-undo .
def {1} shared var opt-edi      as char format "x(1)"  initial "D"  no-undo .
def {1} shared var z-choix      as char                             no-undo .

def {1} shared var periph       as char format "x(12)"              no-undo .
def {1} shared var sav-periph   as char format "x(12)"              no-undo .
def {1} shared var fictri       as char format "x(12)"              no-undo .

/* zones maquettes */
def {1} shared var no-bareme     as char  format  "xxx"              no-undo .
def {1} shared var lib-bareme    as char  format  "x(30)"            no-undo .
def {1} shared var no-magasin    as char  format  "xxx"              no-undo .
def {1} shared var lib-magasin   as char  format  "x(30)"            no-undo .
def {1} shared var no-client     as char  format  "xxx"              no-undo .
def {1} shared var lib-client    as char  format  "x(30)"            no-undo .
def {1} shared var no-catego     as char  format  "xxx"              no-undo .
def {1} shared var lib-catego    as char  format  "x(30)"            no-undo .
def {1} shared var no-famille    as char  format  "xxx"              no-undo .
def {1} shared var lib-famille   as char  format  "x(30)"            no-undo .
def {1} shared var lib-titre     as char  format  "x(20)"            no-undo .
def {1} shared var lib-tit0      as char  format  "x(16)"            no-undo .
def {1} shared var lib-tit1      as char  format  "x(09)"            no-undo .
def {1} shared var lib-tit2      as char  format  "x(09)"            no-undo .
def {1} shared var lib-tot       as char  format  "x(30)"            no-undo .
def {1} shared var nb-adh        as char     no-undo .

def {1} shared var fam-exclus   as char  format  "xx"               no-undo .
def {1} shared var mag-exclus   as char  format  "xx"               no-undo .


/* zones de traitement */
def {1} shared var nb-run       as int                              no-undo .
def {1} shared var nb-lus       as int                              no-undo .
def {1} shared var nb-art       as int                              no-undo .

/*------------------------------------------------------------------------*/

def {1} shared frame fr-saisie .

form " " skip
     libelle[ 11 ] format "x(28)"  date-exe                 skip
     libelle[ 12 ] format "x(28)"  mois-deb                 skip
     libelle[ 13 ] format "x(28)"  mois-fin                 skip
     libelle[ 14 ] format "x(28)"  magasins                 skip
     libelle[ 15 ] format "x(28)"  opt-edi                  skip
     libelle[ 16 ] format "x(28)"  familles                 skip
     libelle[ 17 ] format "x(28)"  maquette     lib-maq     skip
     libelle[ 18 ] format "x(28)"  periph                   skip(1)
     with frame fr-saisie
     row 3  centered  overlay  no-label  with title libelle[ 10 ] .