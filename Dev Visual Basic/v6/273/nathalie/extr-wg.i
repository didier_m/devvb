/* EXTR-WG.I
   Extraction des tiers cr�es ou modifi�s pour WANG
   Creation de 3 enregistrements
*/
/* 1 er enregistrement */
    zone = "1" .
         if auxapr.codsoc = "01" then zone = zone + "1" .
    else if auxapr.codsoc = "05" then zone = zone + "2" .
    else if auxapr.codsoc = "06" then zone = zone + "3" .
    else next .
    zone = zone + substring ( auxapr.codaux , 5, 6 ) + "  " .
    if auxapr.type-adh = ""
    then  zone = zone + "10" .
    else  zone = zone + "40" .
    do a = 1 to 10 :
        zone = zone + string ( auxapr.bareme[a] , "xx" )  .
    end .
    zone-nom = string ( substring ( auxili.adres[1] , 1 , 32 ) , "x(32)" ) .
      if substring ( auxili.adres[1] , 1 , 4 ) = "Mr  " or
         substring ( auxili.adres[1] , 1 , 4 ) = "Mrs " or
         substring ( auxili.adres[1] , 1 , 4 ) = "Mme " or
         substring ( auxili.adres[1] , 1 , 4 ) = "Mle " or
         substring ( auxili.adres[1] , 1 , 4 ) = "Ets " or
         substring ( auxili.adres[1] , 1 , 4 ) = "Ste " or
         substring ( auxili.adres[1] , 1 , 4 ) = "Cie "
      then assign
         zone-nom = string ( substring ( auxili.adres[1] , 5 , 25 ) , "x(25)" )
                    + " " .
      if substring ( auxili.adres[1] , 1 , 3 ) = "MR "
      then
      zone-nom = string ( substring ( auxili.adres[1] , 4 , 32 ) , "x(32)" ) .
      if substring ( auxili.adres[1] , 1 , 4 ) = "MR  "
      then
      zone-nom = string ( substring ( auxili.adres[1] , 5 , 31 ) , "x(32)" ) .
    zone = zone + string ( substring( zone-nom , 1 , 25 ) , "x(25)" ) .
    /* fin de chargement de zone */
    zone = zone + fill ( " " , 80 - length ( zone ) ) .
    put stream kan-wang unformatted zone skip .
/* 2nd enregistrement */
    zone = "2" + string ( substring ( auxili.adres[2] , 1 , 25 ) , "x(25)" )
               + string ( substring ( auxili.adres[3] , 1 , 25 ) , "x(25)" ) .
    if substring ( auxili.adres[5] , 6 , 1 ) = " "
    then zone = zone +
                string ( substring ( auxili.adres[5] , 7 , 25  ) , "x(25)" ).
    else zone = zone +
                string ( substring ( auxili.adres[5] , 6 , 25  ) , "x(25)" ).
    /* fin de chargement de zone */
    zone = zone + fill ( " " , 80 - length ( zone ) ) .
    put stream kan-wang unformatted zone skip .
/* 3 eme enregistrement */
    zone = "3" .
    /* le code postal doit etre numerique */
    xxx-valeur = substring ( auxili.adres[5] , 1 , 5 ) .
    { valnum.i xxx-valeur }
    if xxx-valnum = no
    then
        zone = zone + "00000" .
    else
        zone = zone + string ( substring ( auxili.adres[5],1,5) , "x(5)" ).
    /* mode de paiement */
          if auxapr.modreg = "LCR" then zone = zone + "5" .
    else  if auxapr.modreg = "PRE" then zone = zone + "4" .
    else  if auxapr.modreg = "VIR" then zone = zone + "6" .
    else  zone = zone + "1" .
    find tabdom where tabdom.codsoc = "01"
                  and tabdom.etabli = " "
                  and tabdom.typtab = auxapr.typaux
                  and tabdom.prefix = auxapr.codaux
                  and tabdom.codtab = "0100"
                  no-lock no-error .
    if available tabdom
    then
    do :
        if tabdom.libel1[ 8 ] <> "" then
             zone = zone +
                string ( substring( tabdom.libel1[ 8 ] , 6 , 3 ) , "xxx" ) .
        else zone = zone + "030" .
    end .
    else
        zone = zone + "030" .
    aux-contentieux = auxapr.contentieux .
    if aux-contentieux = "" then aux-contentieux = "0" .
    aux-magasin = substring( auxapr.magasin , 2 , 2 ) .
    if aux-magasin = "" then aux-magasin = "04" .
    zone = zone + string ( aux-contentieux , "x" )
                + string ( aux-magasin , "xx" ) .
    if available tabdom
    then
          zone = zone + string ( tabdom.libel1[ 1 ] , "x(24)" )
                      + string ( tabdom.libel1[ 5 ] , "xxxxxxxxxxx" )
                      + string ( tabdom.libel1[ 3 ] , "xxxxx" )
                      + string ( tabdom.libel1[ 4 ] , "xxxxx" )
                      + string ( tabdom.libel1[ 6 ] , "xx" )  .
    else  zone = zone + fill ( " " , 24 )
                      + "                       " .
    /* telephone */
    zone = zone + string ( substring ( auxapr.teleph , 1, 10 ) , "x(10)" ) .
         if auxapr.codsoc = "06" then zone = zone + "3" .
    else if auxapr.codsoc = "05" then zone = zone + "2" .
    else zone = zone + "1" .
    /* fin de chargement de zone */
    zone = zone + fill ( " " , 80 - length ( zone ) ) .
    put stream kan-wang unformatted zone skip .

