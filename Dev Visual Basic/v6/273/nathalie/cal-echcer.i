/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/cal-echcer.i                                                               !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 29930 04-04-12!Bug!cch            !Suite Fiche V61 29929                                                 !
!V61 26854 14-10-11!Evo!cch            !Pour REMERE on ne prend plus les mvts avec compl�ments de prix        !
!V61 26551 27-09-11!Bug!cch            !Correction pble calcul qte soulte en int�gration livraisons DECLIC    !
!V61 25976 23-08-11!Evo!cch            !Suite Fiche V61 25931                                                 !
!V61 25919 18-08-11!Evo!cch            !Ajout possibilit� de traiter plusieurs statuts mvts apports           !
!V61 25427 06-07-11!Evo!cch            !Ajout gestion int�gration des lignes de Soulte                        !
!V61 25276 29-06-11!Evo!cch            !Ajout  pas d'affichage du tableau si soldes � z�ro                    !
!V61 25161 23-06-11!Evo!cch            !Mise en place d'une seule proc�dure pour REMERE, SOULTE et TAF        !
!V61 25060 20-06-11!Bug!cch            !Correction diverses suite tests                                       !
!V61 25011 16-06-11!Evo!cch            !Ajout gestion multi correspondances articles appros - apport ( 1 appro!
!V61 24952 15-06-11!New!cch            !Calcul quantit� solde soulte pour �change soulte apport / appros      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                          C A L - E C H C E R . I                         */
/*                                                                          */
/* CHARGEMENT DONNEES ECHANGE CEREALES ( REMERE, SOULTE, TAF )              */
/*--------------------------------------------------------------------------*/
/* Definitions communes                               06/2011 - 273 - EUREA */
/*==========================================================================*/

def var i-paramet      as char          no-undo .
def var o-paramet      as char          no-undo .
def var codsoc-sav     as char          no-undo .
def var codsoc-apr     as char          no-undo .
def var codsoc-app     as char          no-undo .
def var codsoc-cer     as char          no-undo .
def var codsoc-art     as char          no-undo .
def var codsoc-tie     as char          no-undo .
def var trt-remere     as char          no-undo .
def var trt-soulte     as char          no-undo .
def var trt-taf        as char          no-undo .
def var type-prg       as char          no-undo .
def var type-aux       as char extent 2 no-undo .
def var code-aux       as char extent 2 no-undo .
def var code-art       as char extent 2 no-undo .
def var code-cer       as char          no-undo .
def var code-camp      as char          no-undo .
def var type-fac       as char          no-undo .
def var l-type-con     as char          no-undo .
def var l-statut       as char          no-undo .
def var statut         as char          no-undo .
def var l-code-mvt     as char extent 4 no-undo .
def var l-code-mvt-2   as char          no-undo .
def var code-mvt       as char          no-undo .
def var art-remere     as char          no-undo .       
def var l-art-remere   as char extent 2 no-undo .       
def var l-art-remere-2 as char          no-undo .        
def var l-soc-remere   as char          no-undo .       
def var l-cer-remere   as char extent 2 no-undo .       
def var l-cer-remere-2 as char          no-undo .        
def var l-cer-mais     as char          no-undo .
def var l-cer-ble      as char          no-undo .
def var l-cer-orge     as char          no-undo .
def var l-cer-trit     as char          no-undo .
def var l-libech       as char          no-undo .
def var l-libtaf       as char          no-undo .
def var l-libtot       as char          no-undo .
def var top-compl-px   as char          no-undo .
def var motcle-vente   as char          no-undo .
def var motcle-taf     as char          no-undo .
def var code-trf       as char          no-undo .
def var no-contrat     as char          no-undo .
def var code-envplat   as char          no-undo .
def var rowid-cde-a    as char          no-undo .
def var date-mvt-1     as char          no-undo .
def var date-trf-1     as char          no-undo .
def var date-mvt-2     as date          no-undo .
def var date-trf-2     as date          no-undo .
def var date-deb-camp  as date          no-undo .
def var date-fin-camp  as date          no-undo .
def var date-creation  as date          no-undo .
def var ok-taf         as log           no-undo .
def var ok-qte         as log           no-undo .
def var rowid-cde      as rowid         no-undo .
def var ind            as int           no-undo .
def var type-ech       as int           no-undo .
def var tour-mvt       as int           no-undo .
def var tour-cer       as int           no-undo .
def var tour-statut    as int           no-undo .
def var tour-art       as int           no-undo .
def var taux-ech       as dec           no-undo .
def var prix-ech       as dec           no-undo .                                   
def var coef-qte       as dec           no-undo .
def var qte-lig        as dec           no-undo .
def var qte-taf        as dec  extent 4 no-undo .

def temp-table T-ECHCER no-undo
    field typech   as int
    field libech   as char
    field articl   as char
    field libart   as char
    field contrat  as char
    field typcon   as char
    field datcon   as date
    field taux-ech as dec
    field prix-ech as dec
    field qte-ent  as dec extent 2
    field qte-sor  as dec
    field qte-sol  as dec
    index i-echcer-1 is unique primary typech articl contrat .

def temp-table T-TIERS no-undo
    field typaux as char
    field codaux as char
    index i-tiers-1 is unique primary typaux codaux .








