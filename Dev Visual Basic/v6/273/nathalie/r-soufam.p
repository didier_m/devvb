/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/src/r-soufam.p                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  9071 14-05-04!Evo!erk            !remise a niveau des standards avec variante 273 : zones libel2 ( 2,4,5!
!-------------------------------------------------------------------------------------------------------------!
!                             I N C L U D E S                !                    T A B L E S                 !
!------------------------------------------------------------+------------------------------------------------!
!connect.i             !                                     !tabges                                          !
!natabsoc.i            !                                     !                                                !
!v6frame.i             !                                     !                                                !
!fr-cadre.i            !lib-titre                            !                                                !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           R - S O U F A M . P                            */
/*          Rattrapage des sous familles                                    */
/*==========================================================================*/

{ connect.i  }
{ natabsoc.i  }

/*-------------------------------------*/
/* Definition des Variables            */
/*-------------------------------------*/

def var  lib-titre    as char                     no-undo .
def var x-ok as logical  init  "no"               no-undo .

def frame FR-SAISIE .

FORM  "Ok   "  x-ok                   skip
      with frame FR-SAISIE
      row 2  centered  overlay {v6frame.i}  no-label .

/*-------------------------------------*/
/* Initialisation                      */
/*-------------------------------------*/
lib-titre = "RATTRAPAGE DES SOUS-FAMILLE ARTICLE" .

/* affichage ecran standard */
{ fr-cadre.i lib-titre }

hide frame fr-titre no-pause .
hide frame fr-cadre no-pause .
hide frame fr-saisie no-pause .


/*----------------------------------------------------------------------------*/
/*  Debut du Traitement                                                       */
/*----------------------------------------------------------------------------*/

update x-ok with frame fr-saisie .
if not x-ok  then leave .


For each TABGES where tabges.codsoc = ""        
                and   tabges.etabli = ""         
                and   tabges.typtab = "ART"      
                and   tabges.prefix begins "SOUFAM"
                exclusive-lock :

            assign  tabges.nombre[10] = tabges.nombre[1]
                    tabges.nombre[1] = 0

                    tabges.libel3[1] = tabges.libel2[1]   /* validation tarif -> */
                    tabges.libel2[1] = ""                 /* edition etiquette   */

                    tabges.libel3[2] = tabges.libel2[2]
                    tabges.libel2[2] = ""

                    tabges.libel3[2] = tabges.libel2[2]
                    tabges.libel2[2] = ""

                    tabges.libel3[4] = tabges.libel2[4]
                    tabges.libel2[4] = ""

                    tabges.libel3[5] = tabges.libel2[5]
                    tabges.libel2[5] = ""
                    .

end. /* For each Tabges */
