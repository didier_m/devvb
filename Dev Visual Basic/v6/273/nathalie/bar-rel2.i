/*==========================================================================*/
/*                          B A R - R E L 2 . I                             */
/*==========================================================================*/
/*           Zones utilise pour le module edition des ruptures              */
/*                Rupture-a               : Niveau de rupture en cours      */
/*                Rupture-nb              : Nombre de rupture existante     */
/*                Rupture-etat = "t"      : Edition du titre de la rupture  */
/*                Rupture-etat = "r"      : Edition de la rupture           */
/*--------------------------------------------------------------------------*/

/*===RUPTURES ENTETE ======================================================*/

/*    Entete ADHERENT                */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 3:

    assign tot-ht  = 0
           tot-tva = 0
           tot-ttc = 0
           nb-bar  = 0
           nb-lig  = 0
           .

    assign numbon = numbon + 1
           nb-adh = nb-adh + 1 .

    run value( ma-entete ) .
    leave .

END .

/*    Entete BAREMES                 */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 4 :

    assign no-magasin = substring( zone , 2 , 3 )
           no-bareme  = substring( zone, 48, 2 )
           qte-ach    = dec( substring( zone, 62, 12) ) .

    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "TAR"
                and   tabges.prefix = "BAREME-CLI"
                and   tabges.codtab = no-bareme
                no-lock  no-error .
    nb-bar = nb-bar + 1 .
    leave .
END .


/*===RUPTURES EN TOTAL=====================================================*/

/*    Total  Baremes            */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 4 :

   { maq-raz.i "05" }

                                             /* le prix est au quintal */
   assign mont-ttc = round( ( qte-ach / 100 ) * tabges.nombre[2] , 2)
          mont-ht  = round( mont-ttc / tauxtva , 2)
          mont-tva = mont-ttc - mont-ht .

   assign edit[11] = substring(tabges.libel2[2], 1, 4 )
          edit[12] = no-bareme
          edit[13] = string( qte-ach , ">>>>>>9.99-")
          edit[14] = string( tabges.nombre[2] , ">>>9.99-" )
          edit[15] = string( mont-ht  , ">>>>>9.99-" )
          edit[16] = string( mont-tva , ">>>>9.99-" )
          edit[17] = string( mont-ttc , ">>>>>9.99-" ) .

   do i = 1 to 20 :
       { maq-maj.i "05" i  edit[i] }
       edit[i] = " " .
   end .

   { maq-edi.i "05" }

  assign  tot-ht  = tot-ht  + mont-ht
          tot-tva = tot-tva + mont-tva
          tot-ttc = tot-ttc + mont-ttc
          nb-lig  = nb-lig  + 1 .

   leave .
END .


/*    Total  ADHERENT           */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 3 :

   ma-edipied = "O" .
   /* remplissage du cadre */
   do i = (nb-lig + 1) to 8 :
       { maq-edi.i "06" }
   end .

   { maq-raz.i "08" }

   assign edit[15] = string( tot-ht   , ">>>>>9.99-" )
          edit[16] = string( tot-tva  , ">>>>9.99-" )
          edit[17] = string( tot-ttc  , ">>>>>9.99-" ) .

   do i = 1 to 20 :
       { maq-maj.i "08" i  edit[i] }
       edit[i] = " " .
   end .

   { maq-edi.i "08" }

   edit[01] = exe-rel .
   { maq-maj.i "10"  "01"  edit[1] }
   { maq-edi.i "10" }

   ma-edipied = " " .
   ma-nblig   =  0  .

   leave .
END .