/*==========================================================================*/
/*                          S T - V M A 3 0 . P                             */
/*          Edition Statistique des Quantites Vendues / Semestre            */
/*==========================================================================*/

{ connect.i      }
{ chx-opt.f      }
{ sel.i      new }
{ aide.i     new }
{ bat-ini.i  new }
{ maquette.i new }
{ st-vma30.i new }

def var i as int no-undo .
def var sequence as char format "x(5)" .

/*----------------------------*/
/* Chargement du fichier aide */
/*----------------------------*/
aide-fichier = "st-arti.aid" . aide-par = "<st-vma30>" . run aide-lec.p .

do i = 1 to 99 :
  aide-m = "libel" + string( i , "999" ) .
  { aide-lib.i }
  libelle[ i ] = aide-lib .
end .

/*-------------------------------------*/
/* Initialisation                      */
/*-------------------------------------*/
assign  sel-applic   = "gescom"
        sel-filtre   = "*st-vma30".

/*-------------------------------------*/
/* Prise Numero sequence par operateur */
/*-------------------------------------*/
assign  codsoc = ""
        etabli = ""
        typtab = "OPE"
        prefix = "NUME-SPOOL"
        codtab = operat .

run opeseq.p ( "stv" , output  sequence, output sel-sequence ) .

assign periph = sel-sequence
       sav-periph = periph .

/* Affichage ecran standard */
{ fr-cadre.i libelle[1] }

{ sel.f "new" }

SAISIE-GENERALE :
REPEAT :

    run st-vma3q.p .
    if keyfunction( lastkey) = "end-error" then leave saisie-generale .

    /*------------------*/
    /* multi-selections */
    /*------------------*/
    run sel.p .
    if keyfunction( lastkey ) = "end-error" then leave saisie-generale .

                                  /*-------------*/
                                 /* Acceptation */
                                /*-------------*/
    REPEAT :

        assign chx-def     =  1
               chx-delete  =  99
               chx-milieu  =  64
               chx-color   =  "normal"
               z-choix     =  substring( libelle[ 5 ], 1, 7 )
               libel       =  trim( libelle[ 4 ] ) .

        { chx-opt.i  "3"  "libel"  "15"  "14"  "16"  "with title z-choix"
                     "ecr-choix"  "color message" }

        if keyfunction( lastkey ) <> "return" then next .

        if chx-trait = 1 then leave SAISIE-GENERALE .    /*   Validation    */
        if chx-trait = 2 then leave SAISIE-GENERALE .    /*   Refus         */
        if chx-trait = 3 then next  SAISIE-GENERALE .    /*   Modification  */

    END .

END .   /* SAISIE-GENERALE  */

                  /*-----------------------------------*/
                 /* Mise a jour du fichier pour batch */
                /*-----------------------------------*/

if chx-trait = 1 and keyfunction( lastkey ) <> "end-error"
then repeat :

    ma-maquet = maquette .
    run maq-lect.p .
    if ma-anom <> ""
    then do :
        message libelle[ 9 ] ma-maquet ma-anom .
        bell. bell .
        pause .
        leave .
    end .

    batch-tenu = yes . /* Gestion en batch */

    { bat-zon.i  "lib-prog"      "lib-prog"      "c" "''" "libelle[35]" }
    { bat-zon.i  "codsoc-soc"    "codsoc-soc"    "c" "''" "libelle[36]" }
    { bat-zon.i  "mois-debcp"    "mois-debcp"    "c" "''" "libelle[11]" }
    { bat-zon.i  "mois-fincp"    "mois-fincp"    "c" "''" "libelle[12]" }
    { bat-zon.i  "mois-debex"    "mois-debex"    "c" "''" "libelle[13]" }
    { bat-zon.i  "opt-edi"       "opt-edi"       "c" "''" "libelle[18]" }
    { bat-zon.i  "magasins"      "magasins"      "c" "''" "libelle[14]" }
    { bat-zon.i  "mag-exclus"    "mag-exclus"    "c" "''" "libelle[38]" }
    { bat-zon.i  "familles"      "familles"      "c" "''" "libelle[15]" }
    { bat-zon.i  "fam-exclus"    "fam-exclus"    "c" "''" "libelle[39]" }
    { bat-zon.i  "maquette"      "maquette"      "c" "''" "libelle[16]" }
    { bat-zon.i  "periph"        "periph"        "c" "''" "libelle[17]" }
    { bat-zon.i  "ma-esc"        "ma-esc"        "c" "''" "libelle[37]" }
    { bat-zon.i  "mois-deb"      "mois-deb"      "c" "''" "''"          }
    { bat-zon.i  "anee-deb"      "anee-deb"      "c" "''" "''"          }
    { bat-zon.i  "mois-fin"      "mois-fin"      "c" "''" "''"          }
    { bat-zon.i  "anee-fin"      "anee-fin"      "c" "''" "''"          }
    { bat-zon.i  "mois-dex"      "mois-dex"      "c" "''" "''"          }
    { bat-zon.i  "anee-dex"      "anee-dex"      "c" "''" "''"          }
    { bat-zon.i  "sel-applic"    "sel-applic"    "c" "''" "''"          }
    { bat-zon.i  "sel-filtre"    "sel-filtre"    "c" "''" "''"          }
    { bat-zon.i  "sel-sequence"  "sel-sequence"  "c" "''" "''"          }

    { bat-maj.i  "st-vma31.p" }

    leave .

end .

                  /*--------------------*/
                 /* Effacement ecrans  */
                /*--------------------*/

hide frame fr-saisie no-pause .
hide frame fr-titre  no-pause .
hide frame fr-cadre  no-pause .
