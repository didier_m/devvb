/*-------------------------------------------*/
/*  Chargement du compte general  Societe 01 */
/*-------------------------------------------*/
find cptgen where cptgen.codsoc = "01" and
                  cptgen.nocpte = lect-cpg
                  no-lock .
if not available cptgen
then assign lect-cpg-lib = "INEX"
            lect-cpg     = "*" .
else assign lect-cpg-lib = cptgen.libcpt[mem-langue]
            lect-cpg     = cptgen.nocpte .
lect-lib = lect-cpg-lib .