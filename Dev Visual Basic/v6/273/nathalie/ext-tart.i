/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/ext-tart.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 21802 30-11-10!Evo!jcc            !Ajout Poids facturation transport                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - T A R T . I                            */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Articles TRAJECTOR                              CC 01/03/1999 */
/*==========================================================================*/

if  codsoc-trait[nsoc] = "08" then wgroupe = "08".
                               else wgroupe = "01".
 zone = "     " + string (wgroupe , "xx")
                + string ( artapr.articl     , "x(10)" )
                + "   "
                + string ( artapr.libart1[1] , "x(32)" )
                + string ( artapr.typart     , "xx" )
                + "   AP"
                + string ( artapr.famart     , "xxx" )
                + string ( artapr.soufam     , "xxx" )
                + "                      "
                + string ( artbis.surcondit  , "999999.999" )
                + string ( artbis.ucde       , "xx" )
                + "   "
                + string ( poids-trp         , "999999.999" )
                + string ( artapr.provenance , "x" )
                + string ( artapr.articl-usine  , "x" )
                + string ( artapr.qte-lin , "99999" )
                .

