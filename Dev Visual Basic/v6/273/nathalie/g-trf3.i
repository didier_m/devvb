/*-----------------------------------------------------------*/
/* Raz lignes Scroll tarifs articles             - GESCOM -  */
/*                                                           */
/* g-trf3.i                                                  */
/*-----------------------------------------------------------*/

display " " @ tarcli-chrono
        " " @ {tarcli.i}.code-tarif
        " " @ lib-compo
        " " @ {tarcli.i}.tarif-ref
        " " @ {tarcli.i}.typtar
        " " @ lib-typtar
        " " @ {tarcli.i}.valeur
        " " @ {tarcli.i}.date-fin
        with frame tarcli-ligne .
