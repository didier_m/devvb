/*---------------------------------------------------------------*/
/* Interface de remontee des factures Fournisseurs (Version CBA) */
/* Lanceur en BATCH                                              */
/* trsfac-b.p                                                    */
/*---------------------------------------------------------------*/

{ connect.i new }

def var ind-applic   as int  no-undo.
def var trait-applic as char no-undo.
def var fic-log      as char no-undo.

/* Initialisations */
assign fic-log        = "errfac." + string ( today , "999999" ) + ".log"
       zone-charg[1] = "BATCH"
       zone-charg[2] = fic-log
       operat        = "BATCH"
       mem-langue    = 1
       trait-applic  = "21,25"
       .

output to value(fic-log) append.

display "*************************************************" format "x(75)" skip.
display "LOG INTEGRATION FACTURES FOURNISSEURS " + string(today) +
 " " + string(time,"hh:mm:ss") + " Operateur " + operat format "x(75)" skip.
display "*************************************************" format "x(75)" skip.

/* Boucle connexion sur bases */
do ind-applic = 1 to 2 :

    display " " skip.
    display "****************************" format "x(75)" skip.
    display "Connexion a l'application " + entry(ind-applic,trait-applic)
            format "x(75)" skip.
    display "****************************" format "x(75)" skip.

    run boot-cnx.p(int(entry(ind-applic,trait-applic))).

    output close.

    /* Traitement integration factures */
    run trsfac-1.p.

end. /* Boucle applications */

/*------------------*/
/* Exit de PROGRESS */
/*------------------*/
quit.