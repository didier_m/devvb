/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/st-clf10.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 13631 07-11-05!Evo!cch            !Ajout possibilité traitement en BATCH                                 !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                          S T - C L F 1 0 . I                             */
/* Statistique Vente Client                                                 */
/* Etat des CA / Depot / Type Client / Famille                              */
/*==========================================================================*/

/* Tbl des Libelles Etat & Tbl d'Edition & enreg fichier tri */
def {1} shared var libelle       as char                 extent 99   no-undo .
def {1} shared var edit          as char                 extent 49   no-undo .
def {1} shared var zone          as char                             no-undo .
def {1} shared stream maquette.                             /* canal edition*/
def {1} shared stream kan-tri .

/* zone de traitement */
def {1} shared var recid-stacli as recid                           no-undo .
def {1} shared var ent-mag      as logical                         no-undo .
def {1} shared var ent-nat      as logical                         no-undo .
def {1} shared var ent-cli      as logical                         no-undo .
def {1} shared var nb-mag       as int                             no-undo .
def {1} shared var nb-nat       as int                             no-undo .
def {1} shared var nb-cli       as int                             no-undo .
def {1} shared var nb-fam       as int                             no-undo .
def {1} shared var no-mag       as char                            no-undo .
def {1} shared var lib-mag      as char                            no-undo .
def {1} shared var no-nature    as char                            no-undo .
def {1} shared var lib-nature   as char                            no-undo .
def {1} shared var no-client    as char                            no-undo .
def {1} shared var lib-client   as char                            no-undo .
def {1} shared var no-fam       as char                            no-undo .
def {1} shared var lib-fam      as char                            no-undo .
def {1} shared var tot-ht       as dec                 extent  59  no-undo .
def {1} shared var ind-tot      as int                             no-undo .
def {1} shared var lib-tot      as char                            no-undo .
def {1} shared var lib-tot2     as char                            no-undo .
def {1} shared var lib-tot3     as char                            no-undo .
def {1} shared var lib-tit      as char                            no-undo .
def {1} shared var lib-tit1     as char                            no-undo .

def {1} shared var b-nat        as int                             no-undo .
def {1} shared var cod-nat      as char                extent  10  no-undo .
def {1} shared var nom-nat      as char                extent  10  no-undo .
def {1} shared var tot-nat      as dec                 extent  20  no-undo .


/* Definition des zones & de la frame */
def {1} shared var dat-debx     as date format "99/99/9999"         no-undo .
def {1} shared var dat-finx     as date format "99/99/9999"         no-undo .
def {1} shared var mois-deb     as date format "99/99/9999"         no-undo .
def {1} shared var mois-fin     as date format "99/99/9999"         no-undo .
def {1} shared var magasins     as char format "x(40)"              no-undo .
def {1} shared var maquette     as char format "x(12)"              no-undo .
def {1} shared var periph       as char format "x(12)"              no-undo .
def {1} shared var lib-maq      as char format "x(30)"              no-undo .


def {1} shared var z-choix      as char                             no-undo .

def {1} shared frame fr-saisie .

form libelle[ 11 ] format "x(23)" dat-debx dat-finx skip
     libelle[ 12 ] format "x(23)" mois-deb mois-fin skip
     libelle[ 13 ] format "x(23)" magasins          skip
     libelle[ 14 ] format "x(23)" maquette lib-maq  skip
     libelle[ 15 ] format "x(23)" periph
     with frame fr-saisie
     row 3  centered  overlay  no-label  with title libelle[ 10 ] { v6frame.i } .