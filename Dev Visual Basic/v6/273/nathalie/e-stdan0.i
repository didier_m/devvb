/*==========================================================================*/
/*                          E - S T D A N 0 . I                             */
/*       Edition des Articles                                               */
/*       Gestion des Variables communes & Frames                            */
/*==========================================================================*/
 
def {1} shared var mes-lib   as char   extent 100                   no-undo .
def {1} shared var lib-tri   as char  format "x(16)" extent 6       no-undo .
                                          
/* zones de maquette */
def {1} shared stream maquette .                         /* canal edition*/

def {1} shared var opt-sto      as char format "9"                  no-undo .
def {1} shared var exercice     as char format "x(4)"               no-undo .
def {1} shared var periode      as INT  FORMAT "99"                 no-undo .
def {1} shared var lib-optsto   as char format "x(30)"              no-undo .
def {1} shared var even         as char format "x(30)"              no-undo .

def {1} shared var maquette     as char format "x(12)"              no-undo .
def {1} shared var lib-maq      as char format "x(30)"              no-undo .
def {1} shared var periph       as char format "x(15)"              no-undo .
def {1} shared var edit         as char extent 100                  no-undo .
 
/*------------------------------------------------------------------------*/
/* Definition des Frames                                                  */
/*------------------------------------------------------------------------*/
def {1} shared frame fr-entete  .
 
def temp-table FICTRA field soc       LIKE artapr.codsoc
                      field mag       LIKE stocks.magasin
                      field symb      LIKE artapr.toxicite
                      FIELD etat      AS CHAR FORMAT "x(30)"
                      FIELD lib-art   LIKE artapr.libart
                      FIELD code-art  AS CHAR FORMAT "x(10)"
                      FIELD qte       AS DEC FORMAT "->>>>>>>9.999"
                      FIELD unite     LIKE artapr.usto
                      FIELD pds       LIKE artapr.pds
                      FIELD num-onu   LIKE artapr.code-toxic
                      FIELD classe    AS CHAR
                      FIELD typ       AS CHAR FORMAT "x"      
                      index primaire  is unique  primary soc mag symb etat code-art lib-art.
 
 
 
