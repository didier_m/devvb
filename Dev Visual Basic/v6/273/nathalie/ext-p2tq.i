/*==========================================================================*/
/*                           E X T - P 2 T Q  . I                           */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Tarifs quantitatifs / Remises vente PDP V2      CC 03/05/1999 */
/*==========================================================================*/

    zone = "" .
    zone = zone + "009"
                + string ( "000" , "xxx" ) + "    01 "
                + "V" + string ( remcli.specif , "xxxx" )
                + string ( year  ( remcli.date-debut ) , "9999" )
                + string ( month ( remcli.date-debut ) , "99" )
                + string ( day   ( remcli.date-debut ) , "99" ) .

    if remcli.typ-borne [ aa ] = "2"
       then zone = zone + "M" .
       else zone = zone + "Q" .

    zone = zone + "T"
                + string ( remcli.borne[ cc ] , "99999999" ) .

    if remcli.typapp [ cc ] = "2"
       then zone = zone + "F" .
       else if remcli.typapp [ cc ] = "3"
               then zone = zone + "T" .
               else zone = zone + "P" .

     zone = zone + string ( remcli.valeur[ cc ] * 1000 , "99999999" )
                 + "-" .
