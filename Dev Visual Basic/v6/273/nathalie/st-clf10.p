/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/src/st-clf10.p                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 13631 07-11-05!Evo!cch            !Ajout possibilité traitement en BATCH                                 !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !                                                !
!chx-opt.f             !                                     !                                                !
!aide.i                !new                                  !                                                !
!bat-ini.i             !new                                  !                                                !
!sel.i                 !new                                  !                                                !
!maquette.i            !new                                  !                                                !
!st-clf10.i            !new                                  !                                                !
!aide-lib.i            !                                     !                                                !
!fr-cadre.i            !libelle[1]                           !                                                !
!sel.f                 !new                                  !                                                !
!standard.i            !-chxopt libel                        !                                                !
!chx-opt.i             !"3" "libel" "15" "15" "17" " " "ecr-c!                                                !
!bat-zon.i             !"lib-prog" "lib-prog" "c" "''" "libel!                                                !
!bat-maj.i             !"st-clf11.p"                         !                                                !
!sel-hide.i            !                                     !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                          S T - C L F 1 0 . P                             */
/* Statistique Vente Client                                                 */
/* Etat des CA / Depot / Type Client / Famille                              */
/*===========================================================================*/

{ connect.i      }
{ chx-opt.f      }
{ aide.i     new }
{ bat-ini.i  new }
{ sel.i      new }
{ maquette.i new }
{ st-clf10.i new }

def var i as int no-undo .

/*----------------------------*/
/* Chargement du fichier aide */
/*----------------------------*/
aide-fichier = "st-clien.aid" . aide-par = "<st-clf10>" . run aide-lec.p .

do i = 1 to 99 :
  aide-m = "libel" + string( i , "999" ) .
  { aide-lib.i }
  libelle[ i ] = aide-lib .
end .

/*-----------------*/
/* Initialisations */
/*-----------------*/
 assign sel-applic   = "gescom"
        sel-filtre   = "*st-clf10" . /* st-clf10.chx */

/*-------------------------------------*/
/* Prise Numero sequence par operateur */
/*-------------------------------------*/
def var sequence as char format "x(5)" .

assign  codsoc = ""
        etabli = ""
        typtab = "OPE"
        prefix = "NUME-SPOOL"
        codtab = operat .

run oper-seq.p( "stc", output sequence, output sel-sequence ) .
assign periph   = sel-sequence
       lib-prog = libelle[ 1 ]
       .

{ fr-cadre.i libelle[1] }

{ sel.f new }

SAISIE-GENERALE :
REPEAT :

    /* Parametres */
    run st-clf1q.p .
    if keyfunction( lastkey) = "END-ERROR" then leave SAISIE-GENERALE .

    /* Multi Selections */
    run sel.p .
    if keyfunction( lastkey ) = "END-ERROR" then leave .

    /* Acceptation, refus, modification */
    { standard.i -chxopt libel }

    REPEAT :
        assign chx-def     =  1
               chx-milieu  =  61
               chx-ombre   =  "O"
               .

        { chx-opt.i "3" "libel" "15" "15" "17" " " "ecr-choix" }

        if keyfunction( lastkey ) <> "RETURN" then next .

        if chx-trait = 1 then leave SAISIE-GENERALE .    /*   Validation    */
        if chx-trait = 2 then leave SAISIE-GENERALE .    /*   Refus         */
        if chx-trait = 3 then next  SAISIE-GENERALE .    /*   Modification  */

    END .

END .   /* SAISIE-GENERALE  */

if chx-trait = 1 and keyfunction( lastkey ) <> "END-ERROR"
then REPEAT :

    ma-maquet = maquette .
    run maq-lect.p .
    if ma-anom <> ""
    then do :
        bell . bell .
        message libelle[ 9 ] ma-maquet ma-anom view-as alert-box error .
        leave .
    end .

    batch-tenu = yes . /* Gestion en BATCH */
    { bat-zon.i  "lib-prog"      "lib-prog"      "c"  "''"  "libelle[17]" }
    { bat-zon.i  "codsoc-soc"    "codsoc-soc"    "c"  "''"  "libelle[18]" }
    { bat-zon.i  "dat-debx"      "dat-debx"      "d"  "''"  "libelle[11]" }
    { bat-zon.i  "dat-finx"      "dat-finx"      "d"  "''"  "libelle[11]" }
    { bat-zon.i  "mois-deb"      "mois-deb"      "d"  "''"  "libelle[12]" }
    { bat-zon.i  "mois-fin"      "mois-fin"      "d"  "''"  "libelle[12]" }
    { bat-zon.i  "magasins"      "magasins"      "c"  "''"  "libelle[13]" }
    { bat-zon.i  "maquette"      "maquette"      "c"  "''"  "libelle[14]" }
    { bat-zon.i  "periph"        "periph"        "c"  "''"  "libelle[15]" }
    { bat-zon.i  "ma-esc"        "ma-esc"        "c"  "''"  "libelle[19]" }
    { bat-zon.i  "sel-applic"    "sel-applic"    "c"  "''"  "''"          }
    { bat-zon.i  "sel-filtre"    "sel-filtre"    "c"  "''"  "''"          }
    { bat-zon.i  "sel-sequence"  "sel-sequence"  "c"  "''"  "''"          }

    { bat-maj.i  "st-clf11.p" }

    leave .

END .

else run sel-del.p( sel-applic, sel-filtre, sel-sequence, yes ) .

/*-------------------------------------------------*/
/* Effacement ecrans et suppression des selections */
/*-------------------------------------------------*/
{ sel-hide.i }
hide frame fr-saisie no-pause .
hide frame fr-titre  no-pause .
hide frame fr-cadre  no-pause .




