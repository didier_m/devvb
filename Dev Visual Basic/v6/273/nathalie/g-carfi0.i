/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/g-carfi0.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 17604 30-04-09!Bug!cch            !Suite fiche 17599                                                     !
!V52 17599 28-04-09!Bug!cch            !Correction probl�me mauvaise affectation d�but no de carte            !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           G - C A R F I 0 . I                            */
/* Gestion Carte Fidelite Client                                            */
/*--------------------------------------------------------------------------*/
/* Variables et Frames                                                      */
/*==========================================================================*/

def var ind as int no-undo .

def {1} shared var libelle         as char                extent 50 no-undo .
def {1} shared var mot-cle         as char                          no-undo .
def {1} shared var type-tiers      as char format "xxx"             no-undo .
def {1} shared var code-tiers      as char format "x(10)"           no-undo .
def {1} shared var code-carte      as char format "x(13)"           no-undo .
def {1} shared var lib-tiers       as char format "x(32)"           no-undo .
def {1} shared var trt-autorise    as char                          no-undo .
def {1} shared var init-soc        as char                          no-undo .
def {1} shared var magasi-soc      as char                          no-undo .
def {1} shared var sav-compo-modif as log                           no-undo .
def {1} shared var auxspe-recid    as recid                         no-undo .

def buffer AUXSPE for AUXAPR .

def {1} shared buffer B-AUXDET for AUXDET .
def {1} shared buffer B-AUXILI for AUXILI .

form /* libelle[ 01 ] format "x(13)" type-tiers           skip
        libelle[ 02 ] format "x(13)" code-tiers lib-tiers skip
     */
     libelle[ 03 ] format "x(13)" code-carte
     with frame fr-cle
     row 2 col 2 overlay no-label { v6frame.i } .

form libelle[ 05 ] format "x(7)" b-auxdet.datcre /* skip */
     libelle[ 06 ] format "x(7)" b-auxdet.opecre skip
     libelle[ 07 ] format "x(7)" b-auxdet.datmaj /* skip */
     libelle[ 08 ] format "x(7)" b-auxdet.opemaj
     with frame fr-maj row 2 col 47 /* 60 */ overlay no-label { v6frame.i} .