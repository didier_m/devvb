/*==========================================================================*/
/*                           E X T - A G I L T A R  . I                     */
/* Extraction des donnees pour AGIL                                         */
/*--------------------------------------------------------------------------*/
/* Extraction des tarifs client et tarifs promo                             */
/*==========================================================================*/
/* DGR 21122009 : rajout ARRONDI sur PX ACHA                                */
/* VLE 10022010 : PA = PA - 0,01 pour palier au bug arrondi Agil            */
/* VLE 24032011 : modif de la date de debut application du tarif PA         */ 
/* VLE 10012012 : Gestion des soldes                                        */
/*==========================================================================*/
/*---------------------------------------------------------*/                     
/*  Ecriture du d�tail de section Px de Vte Sp�cial Central*/
/*---------------------------------------------------------*/ 

If {tarcli.i}.code-tarif = "CLI"
        then do :

        ID_ARTICLE = caps(trim({tarcli.i}.articl)).
        DT_APPLI_TARIF = {tarcli.i}.date-debut.    
        
        PX_VTE_SPE_CENTRAL = trim(replace(string(val-tarif, "->>>>9.99"), ".", ",")).
        
        put stream agil unformatted
                        Type_Ligne_Det ID_ARTICLE "~011" DT_APPLI_TARIF "~011" PX_VTE_SPE_CENTRAL "~015" skip. 

/*------------------------------------------------- -------*/                        
/*  Ecriture du d�tail de section Prix d'achat             */
/*---------------------------------------------------------*/ 

		{ ext-agilpxa.i }
        
end.
/*---------------------------------------------------------*/                     
/*  Ecriture du d�tail de section Px de Vte Sp�cial Promo  */
/*---------------------------------------------------------*/ 

If {tarcli.i}.code-tarif = "PRO"
        then do :

        ID_ARTICLE = caps(trim({tarcli.i}.articl)).
        DT_DEB_PROMO  = {tarcli.i}.date-debut.
        DT_FIN_PROMO  = {tarcli.i}.date-fin.   
        PX_VTE_SPE_PROMO = trim(replace(string(val-tarif, "->>>>9.99"), ".", ",")).
        
        put stream temp_prix unformatted
                Type_Ligne_Det ID_ARTICLE "~011" DT_DEB_PROMO "~011" DT_FIN_PROMO "~011" 
                PX_VTE_SPE_PROMO "~015" skip. 

end.

