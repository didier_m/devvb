/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/bar-eta2.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 15055 12-07-06!Evo!cch            !Acc�s ARTAPR avec soci�t� de regroupement suite mise en place ED      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                          B A R - E T A 2 . I                             */
/*==========================================================================*/
/*           Zones utilise pour le module edition des ruptures              */
/*                Rupture-a               : Niveau de rupture en cours      */
/*                Rupture-nb              : Nombre de rupture existante     */
/*                Rupture-etat = "t"      : Edition du titre de la rupture  */
/*                Rupture-etat = "r"      : Edition de la rupture           */
/*--------------------------------------------------------------------------*/

/* Entete Magasin    */
/*===================*/
DO WHILE (RUPTURE-ETAT = "T" AND RUPTURE-A = 1 ) :

    if substring( zone , 1 , 3 ) <> " " then do :
        hide message . pause 0 .
        message "  Traitement du Magasin ...: "
                "  " substring( zone , 1 , 3 ) .
    end .

    assign no-magasin  = substring( zone , 1 , 3 )
           lib-magasin = " "
           lib-tit1    = libelle[32]
           lib-tit2    = libelle[33]
           nb-art   = 0
           nb-adh-d = 0
           top-ent  = yes.

    FIND MAGASI where magasi.codsoc = magasi-soc
                and   magasi.codtab = no-magasin
                no-lock  no-error .
    if available magasi  then  lib-magasin = magasi.libel1[ 1 ] .


   assign no-bareme   = substring( zone , 4 , 2 )
          lib-bareme  = fill( " " , 30 ) .

   FIND TABGES where tabges.codsoc = ""
               and   tabges.etabli = ""
               and   tabges.typtab = "TAR"
               and   tabges.prefix = "BAREME-CLI"
               and   tabges.codtab = no-bareme
               no-lock  no-error .
   if available tabges then lib-bareme  = tabges.libel1[mem-langue] .


    run value( ma-entete ) .

    leave .

END .

/*    Entete  Bareme             */
/*===============================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 2 :

    nb-adh-b = 0 .
    hide message . pause 0 .
    message "Traitement du Magasin :  "  substring( zone , 1 , 3 )
            "Bareme : " substring( zone , 4 , 2 ).

    if not top-ent    /* si entete magasin ne vient pas d etre faite alors  */
    then do :

        assign no-bareme   = substring( zone , 4 , 2 )
               lib-bareme  = fill( " " , 30 ) .

        FIND TABGES where tabges.codsoc = ""
                    and   tabges.etabli = ""
                    and   tabges.typtab = "TAR"
                    and   tabges.prefix = "BAREME-CLI"
                    and   tabges.codtab = no-bareme
                    no-lock  no-error .
        if available tabges then lib-bareme  = tabges.libel1[mem-langue] .

        run value( ma-entete ) .

    end . /*  do top-ent */

    top-ent = no .

    leave .

END .

/*    Entete CLIENT                  */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 3 :

assign    no-client   = barstat.codaux
          lib-client  = " " .


          regs-fileacc = "AUXILI" + barstat.typaux.
          {regs-rec.i}
          assign auxili-soc = regs-soc .
          find auxili where auxili.codsoc = auxili-soc
                        and auxili.typaux = barstat.typaux
                        and auxili.codaux = barstat.codaux
                        no-lock no-error .
        if not available auxili then lib-client = "INEXISTANT".
        else lib-client = auxili.adres[ 1 ] .

   { maq-maj.i "03"  "1"  "no-client"  }
   { maq-maj.i "03"  "2"  "lib-client" }

   { maq-edi.i  03 }

   assign nb-adh-b = nb-adh-b + 1
          nb-adh-d = nb-adh-d + 1
          nb-adh-g = nb-adh-g + 1 .
   leave .

END .


/*    Entete Categorie  */
/*======================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 4 :

   assign no-catego  = substring( zone, 36, 6 )
          lib-catego = " " .
   FIND TABGES where tabges.codsoc = ""
               and   tabges.etabli = ""
               and   tabges.typtab = "ART"
               and   tabges.prefix = "SOUFAM" + substring( zone, 36, 3)
               and   tabges.codtab = substring( zone, 39, 3)
               no-lock  no-error .
   if available tabges then lib-catego = tabges.libel1[mem-langue] .

   leave .
END .


/*    Entete ARTICLE    */
/*======================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 5 :

    /* preparation de la ligne d'edition */
    assign valeur[  1 ] = trim( substring( zone , 74 , 10 ) ) /* Code Article */
           valeur[  2 ] = substring( zone , 42 , 32 )         /* Designation  */
           .
    /* recherche Unite de Facturation */
    find ARTBIS where artbis.motcle = "VTE"
                  and artbis.codsoc = articl-soc
                  and artbis.articl = valeur[ 1 ]
                  and artbis.typaux = " "
                  and artbis.codaux = " "
                  use-index artbis-1 no-lock no-error .
    if available artbis then valeur[ 3 ] = artbis.ufac .
    else valeur[ 3 ] = " " .

    leave .
END .

/*=========================================================================*/

/*    Total Recid      */
/*=====================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 7 :

    /* CUMUL des QUANTITE suivant selection */
    /*--------------------------------------*/
    /* 1 a 6 - detail des mois + total du semestre */
    if date-test <= sem1-fin
    then do :
        j = month(sem1-deb) .
        do i = 1 to 6 :
            tot-qte[ i ] = tot-qte[ i ] + barstat.qte[ j ] .
            tot-qte[ 7 ] = tot-qte[ 7 ] + barstat.qte[ j ] .
            if j + 1 > 12
            then j = 01 .
            else j = j + 1 .
            if j > m-fin then leave .
        end .
    end .
    if date-test > sem1-fin
    then do :
        j = month(sem2-deb) .
        do i = 1 to 6 :
            tot-qte[ i ] = tot-qte[ i ] + barstat.qte[ j ] .
            tot-qte[ 7 ] = tot-qte[ 7 ] + barstat.qte[ j ] .
            if j + 1 > 12
            then j = 01 .
            else j = j + 1 .
            if j > m-fin then leave .
        end .
    end .

    /* 8 - Total de l'exercice �/� a la demande */
    if date-test <= sem1-fin
    then do :
        j = month(sem1-deb) .
        do i = 1 to 6 :
            tot-qte[ 8 ] = tot-qte[8] + barstat.qte[ j ] .
            if j + 1 > 12
            then j = 01 .
            else j = j + 1 .
            if j > m-fin then leave .
        end .
    end .
    if date-test > sem1-fin
    then do :
        j = month(sem1-deb) .
        do i = 1 to 6 :
            tot-qte[ 8 ] = tot-qte[8] + barstat.qte[ j ] .
            if j + 1 > 12
            then j = 01 .
            else j = j + 1 .
        end .
        j = month(sem2-deb) .
        do i = 1 to 6 :
            tot-qte[ 8 ] = tot-qte[8] + barstat.qte[ j ] .
            if j + 1 > 12
            then j = 01 .
            else j = j + 1 .
            if j > m-fin then leave .
        end .

    end .



    /* 9 - Cumul total premier semestre */
    if date-test > sem1-fin
    then do :
        j = month(sem1-deb) .
        do i = 1 to 6 :
            tot-qte [ 9 ] = tot-qte[ 9 ] + barstat.qte[ j ] .
            if j + 1 > 12
            then j = 01    .
            else j = j + 1 .
        end .
    end .

    nb-art = nb-art + 1 .

    leave .

END .


/*    Total Articles   */
/*=====================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 6 :

   { maq-maj.i "04" "01" "valeur[ 1 ]" }
   { maq-maj.i "04" "02" "valeur[ 2 ]" }
   { maq-maj.i "04" "03" "valeur[ 3 ]" }

   do i = 1 to 9 :
       valeur[ 10 + i ] = string( tot-qte[ i ] , ">>>>>>>>-" ) .
       tot-gen[ 10 + i ] = tot-gen[ 10 + i ] + tot-qte [ i ] .
       tot-qte[i]  =  0 .
   end .

   do i = 11 to  19 :
       { maq-maj.i "04"  i  valeur[i] }
       valeur[ i ] = "" .
   end .

   { maq-edi.i "04" }

   leave .

END .


/*    Total Categorie    Articles   */
/*==================================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 4 :

   assign lib-tot = substring( libelle[ 41 ] + " " + substring(no-catego, 1,3)
                                            + ":" + substring(no-catego, 4,3)
                                            + " " + lib-catego , 1, 30 )
          ind-tot = 10 .
   run bar-etat.p .

   leave .

END .


/*    Total  Client             */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 3 :

   assign lib-tot = substring( libelle[ 41 ] + " " + trim(no-client)
                                             + "-" + lib-client, 1, 30 )
          ind-tot = 20 .
   run bar-etat.p .

   leave .

END .


/*    Total  Baremes            */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 2 :

   assign lib-tot = substring( libelle[ 41 ] + " " + no-bareme
                                             + "-" + lib-bareme , 1 , 30 )
          nb-adh  = string(nb-adh-b , ">>>>>>9" )
          ind-tot = 30 .

   run bar-etat.p .

   leave .

END .

/*    Total Magasin */
/*==================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 1 :


   assign   lib-tot  = substring( libelle[ 41 ] + " " + no-magasin
                                                + "-" + lib-magasin, 1 , 30 )
            lib-tit2 = " "
            nb-adh   = string(nb-adh-d , ">>>>>>9" )
            ind-tot  = 40 .
   run bar-etat.p .

   leave .

END .