/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/273/273/gesnom22.i                                                                      !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!V61 38343 24-05-13!Evo!cch            !Ajout gestion Interfaces et Saisies NATHALIE - NOMEREF                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*============================================================================*/
/*                           G E S N O M 2 2 . I                              */
/* Gestion Echanges NATHALIE - NOMEREF                                        */
/*============================================================================*/
/* Fiche Articles liens NATHALIE - NOMEREF - Module Rech. - Include Affichage */
/*============================================================================*/

case type-rech :

    /* Recherche Articles NOMEREF */
    when "1" then do :
        rec-clef-1 = artics.alpha-cle .
        display rec-clef-1 with frame fr-rech-1 .
    end .

    /* Recherche liens Articles NATHALIE - NOMEREF */
    when "2" then do :
        rec-clef-2 = artics.articl .
        display rec-clef-2 artics.alpha-cle with frame fr-rech-2 .
    end .

end case .

