/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/src/genagilq.p                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 17758 18-09-09!New!cch            !Génération Fichier Cpta mouvements trésorerire AGIL                   !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !tables                                          !
!genagil0.i            !                                     !magasi                                          !
!aide.i                !                                     !                                                !
!chx-opt.f             !                                     !                                                !
!compo.f               !new                                  !                                                !
!compo.i               !                                     !                                                !
!fncompo.i             !"societes" "fr-saisie"               !                                                !
!z-cmp.i               !"societes" "' '"                     !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           G E N A G I L Q . P                            */
/* Generation fichier compta. mouvements tresorerie AGIL                    */
/*--------------------------------------------------------------------------*/
/* Saisie des parametres                                                    */
/*==========================================================================*/

{ connect.i      }
{ genagil0.i     }
{ aide.i         }
{ chx-opt.f      }
{ compo.f    new }

def var i         as int  no-undo .
def var j         as int  no-undo .
def var lib-titre as char no-undo .

/* Saisie zones */
assign compo-vecteur = "<01>,10,20,30,40,50"
       compo-bloque  = no
       compo-modif   = yes
       .

SAISIE :
REPEAT :

    { compo.i }

    /* Societes */
    REPEAT WHILE COMPO-MOTCLE = 10 WITH FRAME FR-SAISIE :

        soc-exclues = "N" .

        { fncompo.i "societes" "fr-saisie" }
        { z-cmp.i   "societes" "' '"       }

        if societes = ""
        then do :
            soc-exclues = "" .
            leave .
        end .

        societes = caps( societes ) .
        display societes .

        if substr( societes, 1, 2 ) = "E:"
           then assign soc-exclues              = "O"
                       substr( societes, 1, 2 ) = ""
                       .

        if soc-exclues = "O" and societes = ""
        then do :
            bell . bell .
            message libelle[ 31 ] .
            readkey pause 2 .
            undo, retry .
        end .

        do ind = 1 to num-entries( societes ) :

            codtab = entry( ind, societes ) .

            FIND TABLES where tables.codsoc = ""
                        and   tables.etabli = ""
                        and   tables.typtab = "SOC"
                        and   tables.prefix = "SOCIETE"
                        and   tables.codtab = codtab
                        no-lock no-error .

            if not available tables
            then do :
                bell . bell .
                message codtab libelle[ 32 ] .
                readkey pause 2 .
                undo, retry .
            end .

        end .

        next SAISIE .

    END . /* Societes*/

    /* Magasins */
    REPEAT WHILE COMPO-MOTCLE = 20 WITH FRAME FR-SAISIE :

        mag-exclus = "N" .

        { fncompo.i "magasins" "fr-saisie" }
        { z-cmp.i   "magasins" "' '"       }

        if magasins = ""
        then do :
            mag-exclus = "" .
            leave .
        end .

        magasins = caps( magasins ) .
        display magasins .

        if substr( magasins, 1, 2 ) = "E:"
           then assign mag-exclus               = "O"
                       substr( magasins, 1, 2 ) = ""
                       .

        if mag-exclus = "O" and magasins = ""
        then do :
            bell . bell .
            message libelle[ 31 ] .
            readkey pause 2 .
            undo, retry .
        end .

        do ind = 1 to num-entries( magasins ) :

            codtab = entry( ind, magasins ) .

            FIND MAGASI where magasi.codsoc  = ""
                        and   magasi.codtab  = codtab
                        no-lock no-error .

            if not available magasi
            then do :
                bell . bell .
                message codtab libelle[ 33 ] .
                readkey pause 2 .
                undo, retry .
            end .

        end .

        next SAISIE .

    END . /* Magasins */

    /* Date Maxi. */
    REPEAT WHILE COMPO-MOTCLE = 30 WITH FRAME FR-SAISIE :

        if date-maxi = ? then date-maxi = today .

        { fncompo.i "date-maxi" "fr-saisie" }
        { z-cmp.i   "date-maxi" "' '"       }

        if date-maxi = ? or date-maxi > today
        then do :
            bell . bell .
            message date-maxi libelle[ 34 ] .
            readkey pause 2 .
            undo, retry .
        end .

        next SAISIE .

    END . /* Date Maxi. */

    /* MAJ ( O/N ) */
    REPEAT WHILE COMPO-MOTCLE = 40 WITH FRAME FR-SAISIE :

        if maj = "" then maj = entry( 1, libelle[ 14 ] ) . /* Non */

        { fncompo.i "maj" "fr-saisie" }
        { z-cmp.i   "maj" "' '"       }

        if lookup( maj, libelle[ 14 ] ) = 0
        then do :
            bell . bell .
            message maj libelle[ 31 ] .
            readkey pause 2 .
            undo, retry .
        end .

        assign maj = caps( maj )
               lib-maj = entry( lookup( maj, libelle[ 14 ] ), libelle[ 15 ] )
               .
        display maj lib-maj .

        next SAISIE .

    END . /* MAJ */

    /* Repertoire */
    REPEAT WHILE COMPO-MOTCLE = 50 WITH FRAME FR-SAISIE :

        if maj = entry( 2, libelle[ 14 ] ) then repert-dep = repert-cpta .
                                           else repert-dep = repert-trav .

        { fncompo.i "repert-dep" "fr-saisie" }
        { z-cmp.i   "repert-dep" "' '"       }

        repert-dep = lc( trim( repert-dep ) ) .
        display repert-dep .

        next SAISIE .

    END . /* Repertoire */

END . /* SAISIE */