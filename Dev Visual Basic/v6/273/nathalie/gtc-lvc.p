/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/gtc-lvc.p                                                                      !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 290554 2019-09-05!Evo!Jean Baptiste C![GC]Int�gration version 273                                           !
! 281127 2019-06-21!Bug!Patrick        ![GC][NATHAPP]Probl�me compilation                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*----------------------------------------------------------------------------*/
/*                             G T C - L V C . P                              */
/*----------------------------------------------------------------------------*/

{ connect.i     }
{ aide.i        }
{ gtc-000.i     }
{ gtc-000.f     }
{ compo.f   new }

form b-tarpre.codval-cli  skip
     b-tarpre.codval-rev
     with frame fr-ligval
     row( frame-row( fr-gtcscr ) + ( frame-line( fr-gtcscr ) - 1 ) * nb-lg )
     col 42  overlay  no-label  no-box  {v6frame.i} .

form b-tarpre.pxvnew-cli  format ">>>>>9.99" skip
     b-tarpre.pxvnew-rev  format ">>>>>9.99"
     with frame fr-ligprx
     row( frame-row( fr-gtcscr ) + ( frame-line( fr-gtcscr ) - 1 ) * nb-lg )
     col 60  overlay  no-label  no-box  {v6frame.i} .

form b-tarpre.datven-cli  format "99.99.99"  skip
     b-tarpre.datven-rev  format "99.99.99"
     with frame fr-ligdat
     row( frame-row( fr-gtcscr ) + ( frame-line( fr-gtcscr ) - 1 ) * nb-lg )
     col 72  overlay  no-label  no-box  {v6frame.i} .

form b-tarpre.sta-valida
     with frame fr-ligok
     row( frame-row( fr-gtcscr ) + ( frame-line( fr-gtcscr ) - 1 ) * nb-lg )
     col 7  overlay  no-label  no-box  {v6frame.i} .

def var x-x as char format "x(7)" .

display  b-tarpre.pxvnew-cli  with frame fr-ligprx .
/* display  b-tarpre.datven-cli  with frame fr-ligdat . */
display  b-tarpre.codval-cli  with frame fr-ligval .

      display         b-tarpre.sta-valida with frame fr-ligok .
color display message b-tarpre.sta-valida with frame fr-ligok .

/* compo-vecteur = "<01>,10,20,30,60,70,80,99" . */
assign compo-vecteur = "<01>,10,30,60,80,99"
       compo-bloque  = no
       .

if code-tarif <> entry( 1, libelle[ 60 ] )
   then assign compo-bloque[ 60 ] = yes
               compo-bloque[ 70 ] = yes
               compo-bloque[ 80 ] = yes .
   else do :
       display  b-tarpre.pxvnew-rev  with frame fr-ligprx .
       /* display  b-tarpre.datven-rev  with frame fr-ligdat . */
       display  b-tarpre.codval-rev  with frame fr-ligval .
   end .

SAISIE-GENERALE :
REPEAT :

  SAISIE-TARPRE :
  REPEAT :

    { compo.i "-tarpre" }

    /*     Composition Nouveau Prix de Vente CLI     */

    REPEAT while compo-motcle = 10 :

        assign compo-bloque[ 20 ] = no
               compo-bloque[ 30 ] = no .

        { fncompo.i  b-tarpre.pxvnew-cli  fr-ligprx  pxvnew-cli  "{f6.i}" }

        if ( keyfunction( lastkey ) <> "return"  and
             keyfunction( lastkey ) <> "put"     and
             keyfunction( lastkey ) <> "go"          )   and
           b-tarpre.pxvnew-cli entered    then undo , retry .

        if keyfunction( lastkey ) = "end-error" then
           undo saisie-generale, leave saisie-generale .

/*      if b-tarpre.pxvnew-cli = 0 then
        do :
            /*   Valeur non Permise    */
            message "  " b-tarpre.pxvnew-cli  libelle[ 30 ] . bell . bell .
            readkey pause 2 . undo , retry .
        end .
*/
        if keyfunction( lastkey ) = "put" then
        do :
           assign compo-bloque        = yes
                  b-tarpre.sta-valida = "V"
                  compo-touche        = "return" .
        end .

        if keyfunction( lastkey ) = "cursor-up" and
           code-tarif = entry( 1, libelle[ 60 ] ) and b-tarpre.pxvnew-rev <> 0  then
        do :
           assign b-tarpre.pxvnew-cli = b-tarpre.pxvnew-rev
                  compo-touche        = "return" .
           display b-tarpre.pxvnew-cli  with frame fr-ligprx .
        end .

        if keyfunction( lastkey ) = "go" then
        do :
           assign compo-bloque[ 20 ] = yes
                  compo-bloque[ 30 ] = yes .
           readkey pause 0 .
        end .

        if typ-coef = "X"  and  b-tarpre.pxvnew-cli <> 0  then
        do :
            taux-marge = ( ( b-tarpre.pxvnew-cli -
                         ( b-tarpre.pxacha-new * b-tarpre.pxvold-rev ) ) /
                           b-tarpre.pxvnew-cli ) * 100 .
            x-x = string( taux-marge , "->9.99" ) + "%" .
            display x-x @ b-tarpre.codval-rev  with frame fr-ligval .
        end .

        next saisie-tarpre .

    END .

    /*     Composition Date Application CLI      */

    REPEAT while compo-motcle = 20 :

        assign compo-bloque[ 30 ]  = no .

        { fncompo.i  b-tarpre.datven-cli  fr-ligdat  datven-cli  "{f6.i}" }

        if ( keyfunction( lastkey ) <> "return"  and
             keyfunction( lastkey ) <> "put"     and
             keyfunction( lastkey ) <> "go"          )   and
           b-tarpre.datven-cli entered    then undo , retry .

        if keyfunction( lastkey ) = "end-error" then
           undo saisie-generale, leave saisie-generale .

        if b-tarpre.datven-cli = ?  then
        do :
            /*   Valeur non Permise    */
            message "  " b-tarpre.datven-cli  libelle[ 30 ] . bell . bell .
            readkey pause 2 . undo , retry .
        end .

        if b-tarpre.datven-cli entered then
        do :
            FIND FIRST {tarcli.i} where {tarcli.i}.motcle     =  ctv-motcle
                              and   {tarcli.i}.codsoc     =  codsoc-soc
                              and   {tarcli.i}.typaux     =  ""
                              and   {tarcli.i}.codaux     =  ""
                              and   {tarcli.i}.articl     =  b-tarpre.articl
                              and   {tarcli.i}.code-tarif =  codtar-cli
                              and   {tarcli.i}.date-debut >= b-tarpre.datven-cli
                              use-index primaire  no-lock  no-error .
            if available {tarcli.i} then
            do :
                /*   Tarif deja entre au   */
                message libelle[ 34 ] {tarcli.i}.date-debut . bell . bell .
                readkey pause 2 .
                undo , retry.
            end .
        end .

        if keyfunction( lastkey ) = "put" then
        do :
           assign compo-bloque        = yes
                  b-tarpre.sta-valida = "V"
                  compo-touche        = "return" .
        end .

        if keyfunction( lastkey ) = "go" then
        do :
           assign compo-bloque[ 30 ] = yes .
           readkey pause 0 .
        end .

        next saisie-tarpre .

    END .

    /*     Composition : Code Valeur CLI     */

    REPEAT while compo-motcle = 30 :

        { fncompo.i  b-tarpre.codval-cli  fr-ligval  codval-cli "{f6.i}" }

        if ( keyfunction( lastkey ) <> "return"    and
             keyfunction( lastkey ) <> "put"       and
             keyfunction( lastkey ) <> "find"         )  and
           b-tarpre.codval-cli entered    then undo , retry .

        if keyfunction( lastkey ) = "end-error" then
           undo saisie-generale, leave saisie-generale .

        if keyfunction( lastkey ) = "go" then
        do : bell . undo , retry .  end .

        if keyfunction( lastkey ) = "find" then
        do :
            assign codsoc = codsoc-soc
                   etabli = ""
                   typtab = "TAR"
                   prefix = "CATEGO-TAR"
                   libel = b-tarpre.codval-cli .
            run v-tab-as.p .
            b-tarpre.codval-cli = "" .
            if codtab = "$rien$" or codtab = ? then undo , retry .
            b-tarpre.codval-cli = codtab .
            display b-tarpre.codval-cli with frame fr-ligval .
        end .

        FIND TABGES where tabges.codsoc = codsoc-soc
                    and   tabges.etabli = ""
                    and   tabges.typtab = "TAR"
                    and   tabges.prefix = "CATEGO-TAR"
                    and   tabges.codtab = b-tarpre.codval-cli
                    no-lock  no-error .
        if not available tabges then
        do :
            /*   Code Valeur Inexistant   */
            message b-tarpre.codval-cli  libelle[ 31 ] . bell . bell .
            readkey pause 2 .
            undo , retry.
        end .

        if keyfunction( lastkey ) = "put" then
        do :
           assign compo-bloque        = yes
                  b-tarpre.sta-valida = "V"
                  compo-touche        = "return" .
        end .

        next saisie-tarpre .

    END .

    /*     Composition Nouveau Prix de Vente REV     */

    REPEAT while compo-motcle = 60 :

        assign compo-bloque[ 70 ] = no
               compo-bloque[ 80 ] = no .

        { fncompo.i  b-tarpre.pxvnew-rev  fr-ligprx  pxvnew-rev  "{f6.i}" }

        if ( keyfunction( lastkey ) <> "return"  and
             keyfunction( lastkey ) <> "put"     and
             keyfunction( lastkey ) <> "go"          )   and
           b-tarpre.pxvnew-rev entered    then undo , retry .

        if keyfunction( lastkey ) = "end-error" then
           undo saisie-generale, leave saisie-generale .

/*      if b-tarpre.pxvnew-rev = 0 then
        do :
            /*   Valeur non Permise    */
            message "  " b-tarpre.pxvnew-rev  libelle[ 30 ] . bell . bell .
            readkey pause 2 .
            undo , retry .
        end .
*/
        if keyfunction( lastkey ) = "put" then
        do :
           assign compo-bloque        = yes
                  b-tarpre.sta-valida = "V"
                  compo-touche        = "return" .
        end .

        if keyfunction( lastkey ) = "go" then
        do :
           assign compo-bloque[ 70 ] = yes
                  compo-bloque[ 80 ] = yes .
           readkey pause 0 .
        end .

        next saisie-tarpre .

    END .

    /*     Composition Date Application REV      */

    REPEAT while compo-motcle = 70 :

        assign compo-bloque[ 80 ]  = no .

        { fncompo.i  b-tarpre.datven-rev  fr-ligdat  datven-rev  "{f6.i}" }

        if ( keyfunction( lastkey ) <> "return"  and
             keyfunction( lastkey ) <> "put"     and
             keyfunction( lastkey ) <> "go"          )   and
           b-tarpre.datven-rev entered    then undo , retry .

        if keyfunction( lastkey ) = "end-error" then
           undo saisie-generale, leave saisie-generale .

        if b-tarpre.datven-rev entered then
        do :
            FIND FIRST {tarcli.i} where {tarcli.i}.motcle     =  ctv-motcle
                              and   {tarcli.i}.codsoc     =  codsoc-soc
                              and   {tarcli.i}.typaux     =  ""
                              and   {tarcli.i}.codaux     =  ""
                              and   {tarcli.i}.articl     =  b-tarpre.articl
                              and   {tarcli.i}.code-tarif =  codtar-rev
                              and   {tarcli.i}.date-debut >= b-tarpre.datven-rev
                              use-index primaire  no-lock  no-error .
            if available {tarcli.i} then
            do :
                /*   Tarif deja entre au   */
                message libelle[ 34 ] {tarcli.i}.date-debut . bell . bell .
                readkey pause 2 .
                undo , retry.
            end .
        end .

        if keyfunction( lastkey ) = "put" then
        do :
           assign compo-bloque        = yes
                  b-tarpre.sta-valida = "V"
                  compo-touche        = "return" .
        end .

        if keyfunction( lastkey ) = "go" then
        do :
           assign compo-bloque[ 80 ] = yes .
           readkey pause 0 .
        end .

        next saisie-tarpre .

    END .

    /*     Composition : Code Valeur REV     */

    REPEAT while compo-motcle = 80 :

        { fncompo.i  b-tarpre.codval-rev  fr-ligval  codval-rev  "{f6.i}" }

        if ( keyfunction( lastkey ) <> "return"    and
             keyfunction( lastkey ) <> "put"       and
             keyfunction( lastkey ) <> "find"         )  and
           b-tarpre.codval-rev entered    then undo , retry .

        if keyfunction( lastkey ) = "end-error" then
           undo saisie-generale, leave saisie-generale .

        if keyfunction( lastkey ) = "go" then
        do : bell . undo , retry .  end .

        if keyfunction( lastkey ) = "find" then
        do :
            assign codsoc = codsoc-soc
                   etabli = ""
                   typtab = "TAR"
                   prefix = "CATEGO-TAR"
                   libel = b-tarpre.codval-rev .
            run v-tab-as.p .
            b-tarpre.codval-rev = "" .
            if codtab = "$rien$" then undo , retry .
            b-tarpre.codval-rev = codtab .
            display b-tarpre.codval-rev with frame fr-ligval .
        end .

        FIND TABGES where tabges.codsoc = codsoc-soc
                    and   tabges.etabli = ""
                    and   tabges.typtab = "TAR"
                    and   tabges.prefix = "CATEGO-TAR"
                    and   tabges.codtab = b-tarpre.codval-rev
                    no-lock  no-error .
        if not available tabges then
        do :
            /*   Code Valeur Inexistant   */
            message b-tarpre.codval-rev  libelle[ 31 ] . bell . bell .
            readkey pause 2 .
            undo , retry.
        end .

        if keyfunction( lastkey ) = "put" then
        do :
           assign compo-bloque        = yes
                  b-tarpre.sta-valida = "V"
                  compo-touche        = "return" .
        end .

        next saisie-tarpre .

    END .

    /*     Composition : Validation      */

    REPEAT while compo-motcle = 99 :

        { fncompo.i  b-tarpre.sta-valida  fr-ligok  sta-valida  "{f6.i}" }

        if keyfunction( lastkey ) <> "return"   and
           b-tarpre.sta-valida entered    then undo , retry .

        if keyfunction( lastkey ) = "end-error" then
           undo saisie-generale, leave saisie-generale .

        if keyfunction( lastkey ) = "go"
        then do :
           b-tarpre.sta-valida = "V" .
        end .

        if keyfunction( lastkey ) = "put"
        then do :
           assign b-tarpre.sta-valida = "V"
                  compo-touche        = "return" .
        end .

        if b-tarpre.sta-valida <> "V" and b-tarpre.sta-valida <> "A" and
           b-tarpre.sta-valida <> "R" and b-tarpre.sta-valida <> " " then
        do :
            /*   Valeur non Permise   */
            message "  " b-tarpre.sta-valida  libelle[ 30 ] . bell . bell .
            readkey pause 2 .
            undo , retry .
        end .

        b-tarpre.sta-valida = caps( b-tarpre.sta-valida ) .
        color display message b-tarpre.sta-valida  with frame fr-ligok  .
        next saisie-tarpre .

    END .


  END .   /*  SAISIE-TARPRE  */

  leave.

END.  /* SAISIE-GENERALE */

hide  frame fr-ligprx     no-pause .
clear frame fr-ligprx all no-pause .
hide  frame fr-ligval     no-pause .
clear frame fr-ligval all no-pause .
hide  frame fr-ligdat     no-pause .
clear frame fr-ligdat all no-pause .
hide  frame fr-ligok      no-pause .
clear frame fr-ligok  all no-pause .
