/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/rfc-cap0.i                                                                     !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End                                                                                                          
**************************************************************************************************************/
/*==========================================================================*/
/*                          R F C - C A P 0 . I                             */
/*       GESTION des RISTOURNES    : MAJ DU CA ET MT RISTOURNES EN CAPITAL  */
/*       Gestion des variables communes et Frames                           */
/*==========================================================================*/

def {1} shared var libelle      as char                 extent 99   no-undo .

def {1} shared var z-choix       as char                            no-undo .
def {1} shared var auxili-soc    as char                            no-undo .
def {1} shared var mot-cle       as char                            no-undo .
def {1} shared var lib-cadre     as char                            no-undo .

def {1} shared var cod-objet     as char                            no-undo .
def {1} shared var cod-activite  as char                            no-undo .

/* variables de la frame */
def {1} shared var date-debex   as date  format "99/99/9999"        no-undo .
def {1} shared var date-finex   as date  format "99/99/9999"        no-undo .

def {1} shared buffer b-tabges for tabges .

/*------------------------------------------------------------------------*/

def {1} shared frame fr-saisie .

form " " skip
     libelle[ 11 ] format "x(28)"  date-debex                      skip
     libelle[ 12 ] format "x(28)"  date-finex                      skip
     with frame fr-saisie
     row 3  centered  overlay  no-label  with title libelle[ 10 ] .