/*==========================================================================*/
/*                          R F C - E X T 0 . I                             */
/*       GESTION des R.F.C : EXTRACTIONS DES RFC DE BASTAT                  */
/*       Gestion des variables communes et Frames                           */
/*==========================================================================*/

def {1} shared var libelle    as char extent 99           no-undo .
def {1} shared var z-choix    as char                     no-undo .
def {1} shared var auxili-soc as char                     no-undo .
def {1} shared var mot-cle    as char                     no-undo .
def {1} shared var top-adh    as char                     no-undo .
def {1} shared var lib-cadre  as char                     no-undo .
def {1} shared var date-deb   as date format "99/99/9999" no-undo .
def {1} shared var date-fin   as date format "99/99/9999" no-undo .
def {1} shared var taux-rem   as dec  format ">>9.99"     no-undo .

def {1} shared frame fr-saisie .

form skip
     libelle[ 11 ] format "x(26)"  date-deb  skip(1)
     libelle[ 12 ] format "x(26)"  date-fin  skip(1)
     with frame fr-saisie
     row 3  centered  overlay  no-label  { v6frame.i } .