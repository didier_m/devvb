/*==========================================================================*/
/*                          R F C - R E L 0 . I                             */
/*       GESTION des RFC CLIENTS   : EDITION DES BONS DE REMISE             */
/*       Gestion des variables communes                                     */
/*==========================================================================*/

def {1} shared stream maquette .                         /* canal edition*/

def {1} shared buffer   b-rfcremi       for rfcremi                  .

def {1} shared var mot-cle       as char                            no-undo .
def {1} shared var lib-cadre     as char                            no-undo .

/* zones maquette */
def {1} shared var edit          as char             extent  40     no-undo .
def {1} shared var auxili-soc    as char                            no-undo .
def {1} shared var numbon        as int  format "999999"            no-undo .

/* zones frames */
def {1} shared var libelle       as char             extent  99     no-undo .
def {1} shared var date-deb     as date format "99/99/9999"         no-undo .
def {1} shared var date-fin     as date format "99/99/9999"         no-undo .
def {1} shared var date-val     as date format "99/99/9999"         no-undo .
def {1} shared var taux-rem     as dec  format ">>9.99"             no-undo .
def {1} shared var borne-mi     as dec                              no-undo .
def {1} shared var borne-m1     as dec                              no-undo .
def {1} shared var date-edi     as date format "99/99/9999"         no-undo .

def {1} shared var magasins     as char format "x(40)"              no-undo .
def {1} shared var maquette     as char format "x(12)"              no-undo .
def {1} shared var lib-maq      as char format "x(30)"              no-undo .
def {1} shared var z-choix      as char                             no-undo .

def {1} shared var periph       as char format "x(12)"              no-undo .
def {1} shared var periph1      as char format "x(12)"              no-undo .

def {1} shared var mag-exclus   as char  format  "xx"               no-undo .


/*------------------------------------------------------------------------*/

def {1} shared frame fr-saisie .

form " " skip
     libelle[ 11 ] format "x(28)"  date-deb                 skip
     libelle[ 12 ] format "x(28)"  date-fin                 skip
     libelle[ 13 ] format "x(28)"  taux-rem                 skip
     libelle[ 14 ] format "x(28)"  date-val                 skip
     libelle[ 15 ] format "x(28)"  borne-mi                 skip
     libelle[ 16 ] format "x(28)"  borne-m1                 skip

     libelle[ 17 ] format "x(28)"  magasins                 skip
     libelle[ 18 ] format "x(28)"  maquette     lib-maq     skip
     libelle[ 19 ] format "x(28)"  periph                   skip
     libelle[ 20 ] format "x(28)"  periph1                  skip
     libelle[ 21 ] format "x(28)"  date-edi                 skip(1)
     with frame fr-saisie
     row 3  centered  overlay  no-label  with title libelle[ 10 ] .