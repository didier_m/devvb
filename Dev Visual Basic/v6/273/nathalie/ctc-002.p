/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/src/ctc-002.p                                                                    !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 15055 12-07-06!Evo!cch            !Acc�s ARTAPR avec soci�t� de regroupement suite mise en place ED      !
!V52 14224 02-02-06!Evo!cch            !Ajout s�lection sur Fournisseur de la Plateforme si LISADIS           !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !artapr                                          !
!aide.i                !new                                  !nat-tarcli                                      !
!bat-ini.i             !new                                  !artbis                                          !
!ctc-000.i             !new                                  !tabges                                          !
!aide-lib.i            !                                     !TARPRE                                          !
!bat-cha.i             !"type-trt" "type-trt" "n"            !                                                !
!regs-cha.i            !                                     !                                                !
!regs-inc.i            !"articl"                             !                                                !
!tarcli.i              !                                     !                                                !
!lec-taxe.i            !tabges  zone-tva  date-fin  taux-tva !                                                !
!ctc-008.i             !"nat-tarcli  "                       !                                                !
!ctc-009.i             !"nat-tarcli  "                       !                                                !
!majmoucb.i            !"tarpre"                             !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
!_____________________________________________________________________________________________________________!
!                                                  P R O C E D U R E S                                        !
!_____________________________________________________________________________________________________________!
!ECR-FIC               !                                                                                      !
&End
**************************************************************************************************************/
/*=======================================================================*/
/*                             C T C - 0 0 2 . P                         */
/*       Constitution du Fichier TARPRE : Preparation Tarif Clients      */
/*       Module de Traitement                                            */
/*=======================================================================*/

{ connect.i     }
{ aide.i    new }
{ bat-ini.i new }
{ ctc-000.i new }

def input parameter fichier as char format "x(12)" .

def var nb-cree    as int  no-undo .
def var fic-tri1   as char no-undo .
def var fic-tri2   as char no-undo .
def var fic-tri3   as char no-undo .
def var articl-soc as char no-undo .
def var o-param    as char no-undo .

def buffer articl for artapr .

def stream kan-tri1 .
def stream kan-tri2 .
def stream kan-tri3 .

aide-fichier = "ctc-000.aid" . aide-par = "" . run aide-lec.p .
app-fichier = "gescom.sai" .

do ii = 1 to 49 :
  aide-m = "libel" + string( ii , "999" ) .
  { aide-lib.i }
  libelle[ ii ] = aide-lib .
end .

run bat-lec.p ( fichier ) .

{ bat-cha.i  "type-trt"    "type-trt"    "n" }
{ bat-cha.i  "type-tiers"  "type-tiers"  "a" }
{ bat-cha.i  "code-tiers"  "code-tiers"  "a" }
{ bat-cha.i  "lib-tiers"   "lib-tiers"   "a" }
{ bat-cha.i  "code-fourn"  "code-fourn"  "a" }
{ bat-cha.i  "code-tarif"  "code-tarif"  "a" }
{ bat-cha.i  "date-debut"  "date-debut"  "d" }
{ bat-cha.i  "date-fin"    "date-fin"    "d" }
{ bat-cha.i  "date-appli"  "date-appli"  "d" }
{ bat-cha.i  "l-tiers"     "l-tiers"     "a" }

regs-app = "NATHALIE" .
{ regs-cha.i }

{ regs-inc.i "articl" }
articl-soc = regs-soc.

assign typtab = "PRM"  prefix = "TX-EUR-FRF"  codtab = "" .
run ch-parax.p( output o-param ) .
if o-param <> ""   then dev-gescom = entry( 1, o-param, "/" ) .
if dev-gescom = "" then dev-gescom = "FRF" .

assign codtar-cli = "CLI"
       codtar-rev = "REV" .

if code-tarif = entry( 2, libelle[ 09 ] )
then assign codtar-cli = code-tarif
            codtar-rev = "" .

if codsoc-soc = "08" then codtar-rev = "" .

assign fic-tri1 = "ctc-000." + trim(codsoc-soc) + "1"
       fic-tri2 = "ctc-000." + trim(codsoc-soc) + "2"
       fic-tri3 = "ctc-000." + trim(codsoc-soc) + "3"
       .

if search( fic-tri1 ) <> ? or search( fic-tri2 ) <> ? or search( fic-tri3 ) <> ?
then do:
    bell. bell .
    message libelle[27] view-as alert-box warning .
    return .
end .

output stream kan-tri1 to value( fic-tri1 ) .

hide message no-pause .
message " Lecture des Tarifs Achats pour Generation du 1er Fichier Tri ... " .

/* Traitement par Categories */
FOR EACH ARTAPR where artapr.codsoc = articl-soc
                use-index artapr-1 no-lock
                while string( type-trt ) = entry( 1, libelle[07] ) :

    if artapr.datcre > today - 15 then next .

    FOR EACH {tarcli.i} where {tarcli.i}.motcle     =  ctc-motcle
                        and   {tarcli.i}.codsoc     =  codsoc-soc
                        and   {tarcli.i}.articl     =  artapr.articl
                        and   {tarcli.i}.code-tarif =  code-tarif
                        and ( ( {tarcli.i}.datcre   >= date-debut and
                                {tarcli.i}.datcre   <= date-fin ) or
                              ( {tarcli.i}.datmaj   >= date-debut and
                                {tarcli.i}.datmaj   <= date-fin ) )
                        and   lookup( string( {tarcli.i}.typaux, "xxx" ) +
                              string( {tarcli.i}.codaux, "x(10)" ), l-tiers ) = 0
                        and ( {tarcli.i}.date-fin = ? or {tarcli.i}.date-fin >= today )
                        use-index article no-lock :

        run ecr-fic . /* Ecriture dans fichier de preparation */

    END .

END . /* FOR EACH ARTAPR */

/* Traitement pour 1 Fournisseur */
FOR EACH {tarcli.i} where {tarcli.i}.motcle     =  ctc-motcle
                    and   {tarcli.i}.codsoc     =  codsoc-soc
                    and   {tarcli.i}.typaux     =  substr( l-tiers, 1, 3 )
                    and   {tarcli.i}.codaux     =  substr( l-tiers, 4, 10 )
                    and   {tarcli.i}.articl     <> ""
                    and   {tarcli.i}.code-tarif =  code-tarif
                    and ( ( {tarcli.i}.datcre   >= date-debut and
                            {tarcli.i}.datcre   <= date-fin ) or
                          ( {tarcli.i}.datmaj   >= date-debut and
                            {tarcli.i}.datmaj   <= date-fin ) )
                    and ( {tarcli.i}.date-fin = ? or {tarcli.i}.date-fin >= today )
                    use-index primaire no-lock
                    while type-trt >= int( entry( 2, libelle[07] ) ) :

    FIND ARTAPR where artapr.codsoc = articl-soc
                and   artapr.articl = {tarcli.i}.articl
                no-lock no-error .

    if not available artapr       then next .
    if artapr.datcre > today - 15 then next .

    if code-fourn <> "" /* On ne prend que les articles du fournisseur de la plateforme */
    then do :

        FIND ARTBIS where artbis.motcle = {tarcli.i}.motcle
                    and   artbis.codsoc = {tarcli.i}.codsoc
                    and   artbis.articl = {tarcli.i}.articl
                    and   artbis.typaux = {tarcli.i}.typaux
                    and   artbis.codaux = {tarcli.i}.codaux 
                    use-index artbis-1 no-lock no-error .

        if not available artbis or not artbis.libart2 begins code-fourn then next .

    end .

    run ecr-fic . /* Ecriture dans fichier de preparation */

END . /* FOR EACH {tarcli.i} */

output stream kan-tri1 close .

if nb-lus = 0
then do :
    run fic-sup.p( fic-tri1 ) .  /*  Suppression Fichier  */
    return .
end .

hide message no-pause .
message " Tri ... " .
run fic-tri.p( fic-tri1 ) .  /*  Tri du Fichier sequentiel  */

hide message no-pause .
message " 1 iere Relecture du Fichier Tri ... " .

/*--------------------------------------------------------------------------*/
/*             Premiere Relecture du Fichier TRI                            */
/*  Conservation par couple Article/Tiers de la Date Appli. la plus recente */
/*  ( Cas de Plusieurs MAJ dans la meme plage de dates )                    */
/*--------------------------------------------------------------------------*/

output stream kan-tri2 to value( fic-tri2 ) .

input from value( fic-tri1 ) .

assign mk-debut = 0
       io-sav   = "" .

REPEAT :

    import zon-io .

    if zon-io = "" then next .

    if mk-debut = 0 then
    do :
        assign mk-debut = 1
               articl-codaux = substring( zon-io , 1 , 23 )
               io-sav = zon-io .
        next .
    end .

    if substring( zon-io , 1 , 23 ) <> articl-codaux then
    do :
        put stream kan-tri2 unformatted '"' + io-sav + '"'  skip .
        assign articl-codaux = substring( zon-io , 1 , 23 )
               io-sav        = zon-io .

        next .
    end .

    if substring( zon-io , 24 , 8 ) > substring( io-sav , 24 , 8 ) then
       io-sav = zon-io .

END .  /*   Repeat Import   */

put stream kan-tri2 unformatted '"' + io-sav + '"'  skip .

input close .

output stream kan-tri2 close .

run fic-sup.p( fic-tri1 ) .  /*  Suppression Fichier  */

hide message no-pause .
message "2 ieme Relecture du Fichier Tri ... " .

/*--------------------------------------------------------------------------*/
/*             Deuxieme Relecture du Fichier TRI                            */
/*  Conservation par Article, du Tarif le plus eleve des Fournisseurs       */
/*  ( Cas de Plusieurs Fournisseurs MAJ dans la meme plage de dates )       */
/*--------------------------------------------------------------------------*/

output stream kan-tri3 to value( fic-tri3 ) .

input from value( fic-tri2 ) .

assign mk-debut = 0
       io-sav   = "" .

REPEAT :

    import zon-io .

    if zon-io = "" then next .

    if mk-debut = 0 then
    do :
        assign mk-debut = 1
               articl = substring( zon-io , 1 , 10 )
               io-sav = zon-io .
        next .
    end .

    if substring( zon-io , 1 , 10 ) <> articl then
    do :
        put stream kan-tri3 unformatted '"' + io-sav + '"'  skip .
        assign articl = substring( zon-io , 1 , 10 )
               io-sav = zon-io .

        next .
    end .

    if dec( substr( zon-io , 32 , 13 ) ) > dec( substr( io-sav , 32 , 13 ) )
       then io-sav = zon-io .

END .  /*   Repeat Import   */

put stream kan-tri3 unformatted '"' + io-sav + '"'  skip .

input close .

output stream kan-tri3 close .

run fic-sup.p( fic-tri2 ) .  /*  Suppression Fichier  */

hide message no-pause .
message " Generation de TARPRE ... " .

/*--------------------------------------------------------------------------*/
/*        Troisieme Relecture du Fichier TRI pour generation TARPRE         */
/*--------------------------------------------------------------------------*/

input from value( fic-tri3 ) .

REPEAT :

    import zon-io .

    if zon-io = "" then next .

    assign nb-fourni = 1
           prix-maxi = 0
           articl    = substring( zon-io ,  1 , 10 )
           typaux    = substring( zon-io , 11 ,  3 )
           auxili    = substring( zon-io , 14 , 10 ) .

    FOR EACH ARTBIS where artbis.motcle = ctc-motcle
                    and   artbis.codsoc = codsoc-soc
                    and   artbis.articl = articl
                    use-index artbis-1  no-lock :

        if artbis.typaux + artbis.codaux = typaux + auxili then next .
        nb-fourni = nb-fourni + 1 .

        /* ===================  Shunt du prix des autres fournisseurs

/*  Lecture pour chaque Article, du dernier tarif des autres Fournisseurs   */
/*  et si l'un de leur prix est plus eleve ==> pas de generation de la      */
/*  proposition de modification.                                            */

        FIND FIRST TARCLI where tarcli.motcle = artbis.motcle
                          and   tarcli.codsoc = artbis.codsoc
                          and   tarcli.typaux = artbis.typaux
                          and   tarcli.codaux = artbis.codaux
                          and   tarcli.articl = artbis.articl
                          and   tarcli.code-tarif = code-tarif
                          and   tarcli.datcre < date-debut
                          use-index primaire  no-lock  no-error .
        if not available tarcli then next .

        pxacha = tarcli.valeur .
        /*    Prix de l'unite de Stock = pxacha * coef-fac / coef-cde .  */
        if artbis.coef-fac > 0 then pxacha =     pxacha * artbis.coef-fac .
        if artbis.coef-fac < 0 then pxacha = - ( pxacha / artbis.coef-fac ) .
        if artbis.coef-cde > 0 then pxacha =     pxacha / artbis.coef-cde .
        if artbis.coef-cde < 0 then pxacha = - ( pxacha * artbis.coef-cde ) .
        pxacha = round( pxacha , 2 ) .
        { ctc-009.i  "tarcli"  }   /*  remcli   */
        if pxacha > prix-maxi then prix-maxi = pxacha .

        ===================       Fin du Shunt                         */

    END . /* FOR EACH ARTBIS */

    pxacha-new  = decimal( substring( zon-io , 32 , 13 ) ) .

    /* if prix-maxi > pxacha-new then next . */

    assign ii = integer( substring( zon-io , 24 , 4 ) )
           kk = integer( substring( zon-io , 28 , 2 ) )
           jj = integer( substring( zon-io , 30 , 2 ) )
           dat-pxacha = date( kk , jj , ii ) .

    FIND ARTICL where articl.codsoc = articl-soc
                and   articl.articl = articl
                use-index artapr-1  no-lock  no-error .

    if not available articl then next .
    if articl.valid-tarif = "2" and string( type-trt ) = entry( 1, libelle[7] )
       then next . /* Refus generation Tarif si trt par Categories */

    articl-libart = "|||" .

    case string( type-trt ) :

        when entry( 1, libelle[07] )
             then assign articl-famart = articl.famart
                         articl-soufam = articl.soufam
                         entry( 1, articl-libart, "|" ) =
                              string( articl.libart1[mem-langue], "x(32)" )
                         .

        otherwise do :

            articl-soufam = "" .

            FIND ARTBIS where artbis.motcle = ctc-motcle
                        and   artbis.codsoc = codsoc-soc
                        and   artbis.articl = articl
                        and   artbis.typaux = typaux
                        and   artbis.codaux = auxili
                        use-index artbis-1 no-lock no-error .

            if available artbis and artbis.libart1 <> ""
               then entry( 1, articl-libart, "|" ) =
                    string( artbis.libart1, "x(32)" ) .
               else entry( 1, articl-libart, "|" ) =
                    string( articl.libart1[mem-langue], "x(32)" ) .

            if string( type-trt ) <> entry( 4, libelle[07] )
            then do :

                if available artbis and artbis.libart2 <> ""
                   then assign articl-famart = substr( artbis.libart2, 1, 9 )
                               entry( 2, articl-libart, "|" ) =
                                    substr( artbis.libart2, 11, 30 )
                               .

                   else assign articl-famart = "999999999"
                               entry( 2, articl-libart, "|" ) = libelle[28]
                               .

            end .

            else assign articl-famart = string( type-tiers, "xxx" ) +
                                        string( trim( code-tiers ), "x(6)" )
                        entry( 2, articl-libart, "|" ) =
                             substr( lib-tiers, 1, 30 )
                             .

            if available artbis
               then entry( 3, articl-libart, "|" ) = artbis.articl-fou .

        end .

    end case .

    articl-valida = " " .

    if articl.valid-tarif = "1" then articl-valida = "A" .
    else do :
        FIND TABGES where tabges.codsoc = ""
                    and   tabges.etabli = ""
                    and   tabges.typtab = "ART"
                    and   tabges.prefix = "SOUFAM" + articl.famart
                    and   tabges.codtab = articl.soufam
                    no-lock  no-error .
        if available tabges then
        do :
           articl-valida = substring( tabges.libel2[1] , 1 , 1 ) .
           if articl.valid-tarif = "2" and
              string( type-trt ) = entry( 1, libelle[07] )
              then next . /* Refus generation Tarif si trt par Categories */
           if articl-valida = "1" then
                articl-valida = "A" .
           else articl-valida = " " .
        end .
    end .

    assign zone-tva = articl.tva
           taux-tva = 0 .
    { lec-taxe.i  tabges  zone-tva  date-fin  taux-tva }

    /*       Recherche Prix d'Achat Precedent    */

    pxacha-old = 0 .
    FIND FIRST {tarcli.i} where {tarcli.i}.motcle = ctc-motcle
                          and   {tarcli.i}.codsoc = codsoc-soc
                          and   {tarcli.i}.typaux = typaux
                          and   {tarcli.i}.codaux = auxili
                          and   {tarcli.i}.articl = articl
                          and   {tarcli.i}.code-tarif = code-tarif
                          and   {tarcli.i}.date-debut < dat-pxacha
                          use-index primaire  no-lock  no-error .

    if available {tarcli.i} then
    do :
        pxacha = {tarcli.i}.valeur .
        { ctc-008.i  "{tarcli.i}"  }  /*  artbis  */
        { ctc-009.i  "{tarcli.i}"  }  /*  remcli  */
        pxacha-old = pxacha .
    end .

/*--------------------------------------------------------------------------*/
/*  Si le Prix d'Achat est le meme ==> Pas de generation de Tarif de Vente  */
/*--------------------------------------------------------------------------*/

    if pxacha-new = pxacha-old and string( type-trt ) = entry( 1, libelle[07] )
       then next . /* Refus generation Tarif si trt par Categories */

    /*   Transformation des prix unitaires d'Achat ( qui a ce niveau ci    */
    /*   representent le cout de l'unite de Stock ) au cout de l'unite de  */
    /*   commande Client                                                   */

    FIND ARTBIS where artbis.motcle = ctv-motcle
                and   artbis.codsoc = articl-soc
                and   artbis.articl = articl
                and   artbis.typaux = ""
                and   artbis.codaux = ""
                use-index artbis-1 no-lock no-error .

    if available artbis then
    do :
        /*    Prix unite de Commande = Prix Unite de Stock * coef-cde .  */
        if artbis.coef-cde > 0 then
           assign pxacha-old =     pxacha-old * artbis.coef-cde
                  pxacha-new =     pxacha-new * artbis.coef-cde .
        if artbis.coef-cde < 0 then
           assign pxacha-old = - ( pxacha-old / artbis.coef-cde )
                  pxacha-new = - ( pxacha-new / artbis.coef-cde ) .
        pxacha-old = round( pxacha-old , 2 ) .
        pxacha-new = round( pxacha-new , 2 ) .
    end .

    /*      Recherche des Prix de Vente Precedents  ( CLI & REV )     */

    assign pxvold-cli = 0
           pxvold-rev = 0
           codval-cli = articl.famart + articl.soufam + " "
           codval-rev = articl.famart + "000" + " " .
    if articl.famart <= "010" then
           codval-rev = "001" + "000" + " " .

    FIND FIRST {tarcli.i} where {tarcli.i}.motcle     = ctv-motcle
                          and   {tarcli.i}.codsoc     = codsoc-soc
                          and   {tarcli.i}.typaux     = ""
                          and   {tarcli.i}.codaux     = ""
                          and   {tarcli.i}.articl     = articl
                          and   {tarcli.i}.code-tarif = codtar-cli
                          and   {tarcli.i}.date-debut < date-appli
                          use-index primaire  no-lock  no-error .

    if available {tarcli.i} then
       assign pxvold-cli = {tarcli.i}.valeur
              codval-cli = if {tarcli.i}.tarif-ref <> ""
                              then {tarcli.i}.tarif-ref
                              else codval-cli
              .

    if codtar-rev <> "" then
    do :
        FIND FIRST {tarcli.i} where {tarcli.i}.motcle     = ctv-motcle
                          and   {tarcli.i}.codsoc     = codsoc-soc
                          and   {tarcli.i}.typaux     = ""
                          and   {tarcli.i}.codaux     = ""
                          and   {tarcli.i}.articl     = articl
                          and   {tarcli.i}.code-tarif = codtar-rev
                          and   {tarcli.i}.date-debut < date-appli
                          use-index primaire  no-lock  no-error .

        if available {tarcli.i} then
           assign pxvold-rev = {tarcli.i}.valeur
                  codval-rev = if {tarcli.i}.tarif-ref <> ""
                                  then {tarcli.i}.tarif-ref
                                  else codval-rev
                  .
    end .

    /*   Lecture des Codes Valeurs ( CLI & REV ) <= Date d'Application   */
    /*   ( Plus recentes valeurs mises a Jour )                          */
    /*   Application du Taux ou du Montant au prix Nouveau prix d'Achat  */
    /*   pour obtenir les nouveaux prix de Vente .                       */

    pxvnew-cli = pxacha-new .
    FIND FIRST {tarcli.i} where {tarcli.i}.motcle     = ctv-motcle
                          and   {tarcli.i}.codsoc     = codsoc-soc
                          and   {tarcli.i}.typaux     = ""
                          and   {tarcli.i}.codaux     = codval-cli
                          and   {tarcli.i}.articl     = ""
                          and   {tarcli.i}.code-tarif = codtar-cli
                          and   {tarcli.i}.date-debut <= date-appli
                          use-index primaire  no-lock  no-error .

    if available {tarcli.i} then
    do :
        if {tarcli.i}.typtar = "2" then
           pxvnew-cli = pxvnew-cli + {tarcli.i}.valeur .
        if {tarcli.i}.typtar = "1" then
           pxvnew-cli = pxvnew-cli * ( 1 + ( {tarcli.i}.valeur / 100 ) ) .
    end .
    else assign articl-valida = "A"
                pxvnew-cli    = 0 .

    pxvnew-cli = round( pxvnew-cli * ( 1 + taux-tva ) , 2 ) .

    /* Non utilise ( CC 21/09/2001 )
    if pxvnew-cli <> 0 then
    do :
        assign param-arondi = trim(codsoc-soc) + articl.famart + articl.soufam
               prix         = pxvnew-cli .
        run c-arondi.p( input-output prix , param-arondi ) .
        pxvnew-cli = prix .
    end .
    */

    /*  Un tarif est-il deja saisi avec une date d'Application  =  ou  >  ??  */

    FIND FIRST {tarcli.i} where {tarcli.i}.motcle     = ctv-motcle
                          and   {tarcli.i}.codsoc     = codsoc-soc
                          and   {tarcli.i}.typaux     = ""
                          and   {tarcli.i}.codaux     = ""
                          and   {tarcli.i}.articl     = articl
                          and   {tarcli.i}.code-tarif = codtar-cli
                          and   {tarcli.i}.date-debut >= date-appli
                          use-index primaire  no-lock  no-error .

    if available {tarcli.i} then
       assign articl-valida = "AN"
              pxvnew-cli    = 0
              .

    pxvnew-rev = 0 .
    if codtar-rev <> "" then
    do :
        pxvnew-rev = pxacha-new .
        FIND FIRST {tarcli.i} where {tarcli.i}.motcle     = ctv-motcle
                              and   {tarcli.i}.codsoc     = codsoc-soc
                              and   {tarcli.i}.typaux     = ""
                              and   {tarcli.i}.codaux     = codval-rev
                              and   {tarcli.i}.articl     = ""
                              and   {tarcli.i}.code-tarif = codtar-rev
                              and   {tarcli.i}.date-debut <= date-appli
                              use-index primaire  no-lock  no-error .

        if available {tarcli.i} then
        do :
            if {tarcli.i}.typtar = "2" then
               pxvnew-rev = pxvnew-rev + {tarcli.i}.valeur .
            if {tarcli.i}.typtar = "1" then
               pxvnew-rev = pxvnew-rev * ( 1 + ( {tarcli.i}.valeur / 100 ) ) .
            pxvnew-rev = round( pxvnew-rev , 2 ) .
        end .
        else assign articl-valida = "A"
                    pxvnew-rev    = 0 .

    /*  Un tarif est-il deja saisi avec une date d'Application  =  ou  >  ??  */

       FIND FIRST {tarcli.i} where {tarcli.i}.motcle     = ctv-motcle
                             and   {tarcli.i}.codsoc     = codsoc-soc
                             and   {tarcli.i}.typaux     = ""
                             and   {tarcli.i}.codaux     = ""
                             and   {tarcli.i}.articl     = articl
                             and   {tarcli.i}.code-tarif = codtar-rev
                             and   {tarcli.i}.date-debut >= date-appli
                             use-index primaire  no-lock  no-error .

       if available {tarcli.i} then
          assign articl-valida = "A"
                 pxvnew-rev    = 0 .
    end .

    /* Sav du tarif CLI calcule */
    entry( 4, articl-libart, "|" ) = string( pxvnew-cli, "9999999.999-" ) .

    /* Pour tarif CLI si Lisadis/Floradis on prend le PVI ( CC 05/09/2001 ) */
    if type-trt >= int( entry( 2, libelle[07] ) )
    then do :

        case dev-gescom :
            when "FRF" then codtab = "PIF" .
            otherwise       codtab = "PIE" .
        end case .

        FIND LAST {tarcli.i} where {tarcli.i}.motcle     = ctv-motcle
                             and   {tarcli.i}.codsoc     = codsoc-soc
                             and   {tarcli.i}.typaux     = ""
                             and   {tarcli.i}.codaux     = ""
                             and   {tarcli.i}.articl     = articl
                             and   {tarcli.i}.code-tarif = codtab
                             and   {tarcli.i}.date-debut = dat-pxacha
                             use-index primaire
                             no-lock no-error .

        if available {tarcli.i} and {tarcli.i}.valeur <> 0 and articl-valida <> "AN"
        then do :

            pxvnew-cli = {tarcli.i}.valeur .

            FIND ARTBIS where artbis.motcle = ctc-motcle
                        and   artbis.codsoc = codsoc-soc
                        and   artbis.articl = articl
                        and   artbis.typaux = typaux
                        and   artbis.codaux = auxili
                        use-index artbis-1 no-lock no-error .

            if available artbis
            then do :

                /* Prix unite de Stock = pxvnew-cli * coef-fac / coef-cde */
                if artbis.coef-fac > 0
                   then pxvnew-cli =     pxvnew-cli * artbis.coef-fac .
                if artbis.coef-fac < 0
                   then pxvnew-cli = - ( pxvnew-cli / artbis.coef-fac ) .
                if artbis.coef-cde > 0
                   then pxvnew-cli =     pxvnew-cli / artbis.coef-cde .
                if artbis.coef-cde < 0
                   then pxvnew-cli = - ( pxvnew-cli * artbis.coef-cde ) .

            end .

            pxvnew-cli = round( pxvnew-cli, 2 ) .

        end . /* if available {tarcli.i} ... */

    end . /* if string( type-trt ) = entry( 2, libelle[07] ) */

    if articl-valida = "AN" then articl-valida = "A" .

    CREATE TARPRE .

    assign tarpre.codsoc     = codsoc-soc
           tarpre.code-tarif = code-tarif
           tarpre.famart     = articl-famart
           tarpre.soufam     = articl-soufam
           tarpre.libart     = articl-libart
           tarpre.articl     = articl
           tarpre.chrono     = 0
           tarpre.sta-valida = articl-valida
           tarpre.typaux     = typaux
           tarpre.codaux     = auxili
           tarpre.nb-fourni  = nb-fourni
           tarpre.pxacha-old = pxacha-old
           tarpre.dat-pxacha = dat-pxacha
           tarpre.pxacha-new = pxacha-new
           tarpre.codval-cli = codval-cli
           tarpre.pxvold-cli = pxvold-cli
           tarpre.pxvnew-cli = pxvnew-cli
           tarpre.datven-cli = date-appli
           tarpre.codval-rev = codval-rev
           tarpre.pxvold-rev = pxvold-rev
           tarpre.pxvnew-rev = pxvnew-rev
           tarpre.datven-rev = date-appli
           .

    { majmoucb.i "tarpre" }

END .  /*  REPEAT Import  */

input close .

run fic-sup.p( fic-tri3 ) .  /*  Suppression Fichier  */

hide message no-pause .
message " Numerotation du chrono de TARPRE ... " .

assign catego-preced = ""
       tarpre-chrono = 0
       .

FOR EACH TARPRE where tarpre.codsoc     = codsoc-soc
                and   tarpre.code-tarif = code-tarif
                and   tarpre.chrono     = 0
                use-index primaire exclusive-lock :

    if tarpre.famart + tarpre.soufam <> catego-preced then
       assign tarpre-chrono = 0
              catego-preced = tarpre.famart + tarpre.soufam .

    assign tarpre-chrono = tarpre-chrono + 5
           tarpre.chrono = tarpre-chrono
           nb-cree       = nb-cree + 1 .

END .

/* Pas de MAJ des dates si Fournisseur divers */
if string( type-trt ) <> entry( 4, libelle[ 07 ] )
then DO TRANSACTION :

    FIND TABGES where tabges.codsoc = codsoc-soc
                and   tabges.etabli = ""
                and   tabges.typtab = "TAR"
                and   tabges.prefix = "PREPA-TAR" + string( type-trt )
                and   tabges.codtab = code-tarif
                exclusive-lock  no-error .

    if available tabges
       then assign tabges.dattab[ 1 ] = date-debut
                   tabges.dattab[ 2 ] = date-fin
                   tabges.dattab[ 3 ] = date-appli .

END . /* TRANSACTION */

hide message no-pause .
message libelle[ 25 ] nb-cree view-as alert-box .

/*************************** P R O C E D U R E S *****************************/

/* Ecriture dans fichier de preparation */
PROCEDURE ECR-FIC :

    pxacha = {tarcli.i}.valeur .
    { ctc-008.i  "{tarcli.i}"  }   /*  artbis  */
    { ctc-009.i  "{tarcli.i}"  }   /*  remcli  */
    if pxacha <= 0 and string( type-trt ) = entry( 1, libelle[07] )
       then next . /* Refus generation Tarif si trt par Categories */

    if nb-lus modulo 100 = 0 then
    do :
        hide message . pause 0 .
        message nb-lus {tarcli.i}.articl .
    end .
    nb-lus = nb-lus + 1 .

    put stream kan-tri1 unformatted '"'                      +
               string( {tarcli.i}.articl , "x(10)" )             +
               string( {tarcli.i}.typaux , "x(3)" )              +
               string( {tarcli.i}.codaux , "x(10)" )             +
               string( year( {tarcli.i}.date-debut )  , "9999" ) +
               string( month( {tarcli.i}.date-debut ) , "99" )   +
               string( day( {tarcli.i}.date-debut )   , "99" )   +
               string( pxacha , ">>>>>>>>9.99-" )            +
               '"' skip .

END PROCEDURE .