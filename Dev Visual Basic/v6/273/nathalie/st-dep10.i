/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/st-dep10.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 13696 23-11-05!Evo!cch            !Ajout nom spécifique du spool si maquette pour MONARCH                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                          S T - D E P 1 0 . I                             */
/*          CA des DEPOTS LIVREURS pour le compte des DEPOTS COMMISSIONNES  */
/*          Definitions des variables et des frames                         */
/*==========================================================================*/

def {1} shared var libelle       as char                 extent 99   no-undo .
def {1} shared var valeur        as char                 extent 20   no-undo .
def {1} shared var mask          as char                 extent 20   no-undo .

def {1} shared var lib-periode   as char                             no-undo .

def {1} shared var magasins     as char format "x(40)"              no-undo .
def {1} shared var list-mag     as char                             no-undo .

def {1} shared var maquette     as char format "x(12)"              no-undo .
def {1} shared var lib-maq      as char format "x(35)"              no-undo .
def {1} shared var periph       as char format "x(22)"              no-undo .
def {1} shared var sav-periph   as char                             no-undo .
def {1} shared var chemin       as char format "x(18)"              no-undo .

def {1} shared var nom-ope      as char format "x(12)"              no-undo .

def {1} shared var mag-exclus   as char  format  "xx"               no-undo .

def {1} shared var mois-deb     as char format "x(6)"               no-undo .
def {1} shared var jour-deb     as date format "99/99/9999"         no-undo .
def {1} shared var jour-fin     as date format "99/99/9999"         no-undo .

def {1} shared var z-choix      as char                             no-undo .

def {1} shared var magasi-soc   as char                             no-undo .

/*------------------------------------------------------------------------*/
def {1} shared frame fr-saisie .

form libelle[ 11 ] format "x(22)"  mois-deb                 skip
     libelle[ 12 ] format "x(22)"  magasins                 skip
     libelle[ 13 ] format "x(22)"  maquette     lib-maq     skip
     libelle[ 14 ] format "x(22)"  periph                   skip
     libelle[ 15 ] format "x(22)"  chemin
     with frame fr-saisie
     row 3  centered  overlay  no-label  with title libelle[ 10 ]
     { v6frame.i } .