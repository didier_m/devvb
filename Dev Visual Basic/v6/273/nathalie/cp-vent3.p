/*=======================================================================*/
/*                        C P - V E N T 3 . P                            */
/*   Comptabilisation avec Centralisation des Lignes Comptables          */
/*-----------------------------------------------------------------------*/
/*         Zones utilisees pour le module edition des ruptures           */
/*                                                                       */
/*                   Rupture-a          : Niveau de rupture en cours     */
/*                   rupture-etat = "t" : Edition du titre de la rupture */
/*                   Rupture-etat = "r" : Edition de la rupture          */
/*-----------------------------------------------------------------------*/

/*  ++++ intfac.ttva n'existe pas chez CBA ...  */

{ connect.i  }
{ saisie.i   }
{ cp-vent0.i }
{ det-expe.i  new }

def input parameter rupt-etat as char no-undo .
def input parameter rupt-a    as int  no-undo .

{ xxx-dfbi.i }

def var segs-ana as char  no-undo .
def var jj       as int   no-undo .

def var no-contrat    as char  no-undo .

def var x-x           as char  no-undo .
def var x-y           as char  no-undo .
def var x-z           as char  no-undo .

/*    Entete d'un Document    */

DO TRANSACTION  WHILE  RUPT-ETAT = "T"  AND  RUPT-A = 1 :

    if substring( zon-io , 1 , 15 ) = fill( "9" , 15 )   or
       substring( zon-io , 1 , 15 ) = fill( "z" , 15 )  then  leave .

    assign  tot-mt-cpt = 0
            tot-mt-etb = 0
            tot-mt-ini = 0
            stat-bloc  = ""
            chrono     = "" .

    assign  execpt     = substring( zon-io ,  1 , 4 )
            percpt     = substring( zon-io ,  5 , 3 )
            joucpt     = substring( zon-io ,  8 , 2 ) .

    int-recid = integer( substring( zon-io , 108 , 10 ) ) .
    FIND INTFAC where recid( intfac ) = int-recid  no-lock  no-error .
    if available intfac  then
    do :
        assign  x-devise    = intfac.devise
                int-codsoc  = codsoc-soc
                int-codetb  = codetb-etb
                int-journal = journal-vte
                int-operat  = operat
                int-datpie  = intfac.date-facture .
        run det-expe.p .
        assign  execpt = int-execpt
                percpt = int-percpt
                joucpt = int-joucpt .
    end .

    if execpt < "1990"  or  execpt > "2099"  then  stat-bloc  = execpt .

    REPEAT :

        FIND TABLES where  tables.codsoc = codsoc-soc
                       and tables.etabli = ""
                       and tables.typtab = "PAR"
                       and tables.prefix = "EXERCICE"
                       and tables.codtab = execpt
        no-lock  no-wait  no-error.

        /*  Si exercice non Ouvert  */

        if not available tables then
        do :
            stat-bloc = "INEX" .
            leave .
        end .

        nbrper    = tables.nombre [ 1 ].   /* nombre de periodes autorisees */
        multi-dev = tables.nombre [ 2 ].   /* 0 = Multi devises
                                           1 = Pas de devises */
        tr-exe          = tables.nombre [ 5 ] .  /* Indice Solde    temps reel */
        top-cloture     = tables.libel1 [ 1 ] .  /* Exercice cloture */
        devcpt          = tables.libel1 [ 2 ] .  /* devise comptable */
        top-valid       = tables.nombre [ 6 ] .  /* 1 = validation des documents*/

        /*  Si exercice deja cloture  */

        if top-cloture = "2" then
        do :
            stat-bloc = "CLOT" .
            leave .
        end .

        /* si exercice en cours de cloture ---> aller chercher l'exercice + 1 */

        if top-cloture = "1" then
        do :
            rep-execpt = string ( ( integer ( execpt ) + 1 ), "9999" ) .
            FIND TABLES where  tables.codsoc = codsoc-soc
                        and    tables.etabli = ""
                        and    tables.typtab = "PAR"
                        and    tables.prefix = "EXERCICE"
                        and    tables.codtab = rep-execpt
                        no-lock  no-wait  no-error .
            an-rep = tables.nombre [ 4 ] . /* Indice anouveau */
            tr-rep = tables.nombre [ 5 ] . /* Indice temps reel */
        end .
        leave .

    END .

    if stat-bloc <> ""  then  leave .

    assign  bi-codsoc = codsoc-soc
            bi-codetb = codetb-etb
            bi-execpt = execpt
            bi-percpt = percpt
            bi-datpie = intfac.date-facture
            bi-devini = x-devise
            bi-mtini  = 0
            bi-motcle = "" .
    run out-mona( input  bi-codsoc , input  bi-codetb , input  bi-execpt , 
                  input  bi-percpt , input  bi-datpie , input  bi-devini , 
                  input  bi-mtini  , input  bi-motcle , output bi-mtpie  ,
                  output bi-mtcpt  , output bi-txpie  , output bi-txini  , 
                  output bi-devcpt , output bi-devpie , output bi-monnaie, 
                  output bi-retour ) .

    /*     Incrementation et Liberation du Compteur de Documents    */ 

    assign  devcpt    = bi-devcpt
            devpie    = bi-devpie
            devini    = bi-devini

            tx-devini = bi-txini
            tx-devpie = bi-txpie

            devdoc    = bi-devpie
            tx-devdoc = bi-txpie

            mem-dat   = today
            codetb    = codetb-etb
            piece     = "."
            datpie    = intfac.date-facture
            journal   = journal-vte
            typie     = typie-fac .

    if piece = ""  then  piece = "." .

    REPEAT :
        FIND TABLES where tables.codsoc = codsoc-soc
                    and   tables.etabli = ""
                    and   tables.typtab = "SOC"
                    and   tables.prefix = "N-DOCUMENT"
                    and   tables.codtab = ""
                    exclusive-lock  no-error .

        tables.nombre[ 1 ] = tables.nombre[ 1 ] + 1 .
        docume = string( tables.nombre[ 1 ] , "zzzzzzz9" ) .

        FIND DOCCPT where doccpt.codsoc = codsoc-soc
                    and   doccpt.codetb = codetb-etb
                    and   doccpt.docume = docume
                    no-lock  no-wait  no-error .
        if available doccpt then next .

        RELEASE TABLES .

        CREATE DOCCPT .
        do-recid = recid( doccpt ) .
        assign doccpt.codsoc = codsoc-soc
               doccpt.codetb = codetb-etb
               doccpt.docume = docume
               doccpt.execpt = execpt
               doccpt.percpt = percpt
               doccpt.joucpt = joucpt
               doccpt.datsai = today
               doccpt.journal = journal
               doccpt.operat  = operat
               doccpt.piece   = piece
               doccpt.datpie  = datpie
               doccpt.typie   = typie
               doccpt.devcpt  = devcpt
               doccpt.devpie  = devpie
               doccpt.tx-devpie = 1
               doccpt.bloc-ope  = ""
               doccpt.bloc-date = ? .
        leave .
    END .

    leave .

END .  /*  T 1  */

/*    Entete d'une Ligne d'ecriture    */

DO TRANSACTION  WHILE  RUPT-ETAT = "T"  AND  RUPT-A = 2 :

    if substring( zon-io , 1 , 15 ) = fill( "9" , 15 )   or
       substring( zon-io , 1 , 15 ) = fill( "z" , 15 )  then  leave .

    if stat-bloc <> ""  then  leave .

    piece-cent = substring( zon-io , 93 , 15 ) .

    int-recid = integer( substring( zon-io , 108 , 10 ) ) .
    FIND INTFAC where recid( intfac ) = int-recid  no-lock  no-error .

    assign  mont-dev   = 0
            mtax-dev   = 0
            codgen     = intfac.cpt-gen .

    FIND CPTGEN where cptgen.codsoc = soc-codgen
                and   cptgen.nocpte = codgen
                no-lock  no-error .
    if not available cptgen  then  codgen = cptgen-atent .

    x-suitax = "" .
    if available cptgen  then  x-suitax = cptgen.suitax .

    leave .

END .  /*  T 2  */

/*    Detail d'une Ligne d' INTFAC    */

DO TRANSACTION  WHILE  RUPT-ETAT = "R"  AND  RUPT-A = 3 :

    if substring( zon-io , 1 , 15 ) = fill( "9" , 15 )   or
       substring( zon-io , 1 , 15 ) = fill( "z" , 15 )  then  leave .

    if stat-bloc <> ""  then  leave .

    FIND INTFAC where recid( intfac ) = int( substring( zon-io , 108 , 10 ) )
                exclusive-lock  no-error .

    if intfac.mt-dev  <> ?  then
       mont-dev = mont-dev + intfac.mt-dev .
    if intfac.tax-dev <> ?  then
       mtax-dev = mtax-dev + intfac.tax-dev .

    assign  intfac.cpt-gen = codgen
            intfac.docume  = docume
            intfac.datcpt  = string( execpt , "x(4)" ) +
                             string( percpt , "x(3)" ) +
                             string( joucpt , "x(2)" ) .

    { majmoucb.i intfac }

    leave .

END .  /*  R 3  */

/*    Generation de la Ligne d'ecriture    */

DO TRANSACTION  WHILE  RUPT-ETAT = "R"  AND  RUPT-A = 2 :

    if stat-bloc <> ""  then  leave .

    if mont-dev = 0  then  leave .

    FIND INTFAC where recid( intfac ) = int-recid  no-lock  no-error .

    assign  libel-mv    = ""
            cod-dom     = ""
            adres       = ""
            ana-segment = ""
            banque      = ""
            tire        = ""
            alpha-taxe  = ""
            alpha1-taxe = ""
            alpha-tini  = ""
            typtaxe     = "" .

    assign reg-dom  = intfac.modreg
           piece    = right-trim( piece-cent )
           dom-ech  = intfac.numdom
           echeance = intfac.echeance
           datpie   = intfac.date-facture
           typaux   = intfac.typaux
           auxili   = intfac.codaux .

    if x-suitax = "O"  then  typtaxe = intfac.ttva .

    if intfac.ttva <> ""  and  mtax-dev <> 0  and  x-suitax = "O"
    then do :
        assign  bi-codsoc = codsoc-soc
                bi-codetb = codetb-etb
                bi-execpt = execpt
                bi-percpt = percpt
                bi-datpie = datpie
                bi-devini = x-devise
                bi-mtini  = mtax-dev
                bi-motcle = "" .
        run out-mona( input  bi-codsoc , input  bi-codetb , input  bi-execpt , 
                      input  bi-percpt , input  bi-datpie , input  bi-devini , 
                      input  bi-mtini  , input  bi-motcle , output bi-mtpie  ,
                      output bi-mtcpt  , output bi-txpie  , output bi-txini  , 
                      output bi-devcpt , output bi-devpie , output bi-monnaie, 
                      output bi-retour ) .
        assign  alpha1-taxe = string( bi-mtcpt , "ZZZZZZZZZZZ9.99-" )
                alpha-taxe  = string( bi-mtpie , "ZZZZZZZZZZZ9.99-" )
                alpha-tini  = string( mtax-dev , "ZZZZZZZZZZZ9.99-" ) .
    end .

    if auxili <> ""  then
    do :
        regs-fileacc = "AUXILI" + typaux .
        { regs-rec.i }
        FIND AUXILI where auxili.codsoc = regs-soc
                    and   auxili.typaux = typaux
                    and   auxili.codaux = auxili
                    no-lock  no-error .

        do ii = 1 to 5 while available auxili :
           adres[ ii ] = auxili.adres[ ii ] .
        end .
    end .

    assign  bi-codsoc = codsoc-soc
            bi-codetb = codetb-etb
            bi-execpt = execpt
            bi-percpt = percpt
            bi-datpie = datpie
            bi-devini = x-devise
            bi-mtini  = mont-dev
            bi-motcle = "" .
    if intfac.cpt-sens = "2"  then  bi-mtini = - bi-mtini .

    run cp-ventx .

    libel-mv = intfac.libelle .

    no-contrat = "" .
    if substring( libel-mv , 66 , 2 ) = "*$"  then
       assign  no-contrat = substring( libel-mv , 68 , 13 )
               overlay( libel-mv , 66 , 15 ) = fill( " " , 15 )
               libel-mv = trim( libel-mv ) .

    segs-ana = trim( intfac.cpt-ana ) .
    if segs-ana <> ""  then
    do :
        jj = 1 .
        do ii = 1 to nbr-seg :
           ana-segment[ ii ] = substring( segs-ana, jj, lgr-segment[ ii ] ) .
           jj = jj + lgr-segment[ ii ] .
        end .
    end .

    typie = typie-fac .
    if mt-cpt < 0  then  typie = typie-avo .

    if bi-mtcpt <> 0  or  bi-mtini <> 0  or  bi-mtpie <> 0  then
    do :
        chrono = string( integer( chrono ) + 1 , "99999" ) .
        { run.i  "s-majbas.p" }
    end .

    if no-contrat <> ""  then
    do :
        FIND MVTCPT where mvtcpt.codsoc = codsoc-soc
                    and   mvtcpt.codetb = codetb-etb
                    and   mvtcpt.docume = docume
                    and   mvtcpt.chrono = chrono
                    use-index primaire  exclusive-lock  no-error .
        if available mvtcpt  then  mvtcpt.ty-stat = no-contrat .
    end .

   leave .

END .

/*    Fin d'un Document    */

DO TRANSACTION WHILE RUPT-ETAT = "R"  AND  RUPT-A = 1 :

    if stat-bloc <> "" then leave .

    /*    Incrementation du No de Document / Journal     */

    REPEAT :

        FIND TABLES where tables.codsoc = ""
                    and   tables.etabli = ""
                    and   tables.typtab = "SPE"
                    and   tables.prefix = "DOCUMENT"
                    and   tables.codtab = "CHRONO-FIN"
                    no-lock  no-error .
        if not available tables  then  leave .

        FIND TABLES where tables.codsoc = codsoc-soc
                    and   tables.etabli = ""
                    and   tables.typtab = "ETA"
                    and   tables.prefix = "ETABLIS"
                    and   tables.codtab = codetb-etb
                    no-lock  no-error .
        if not available tables  then  leave .

        FIND DOCCPT where doccpt.codsoc = codsoc-soc
                    and   doccpt.codetb = codetb-etb
                    and   doccpt.docume = docume
                    exclusive-lock  no-error .
        if not available doccpt  then  leave.

        FIND LAST B-DOCCPT where b-doccpt.codsoc      =  doccpt.codsoc
                           and   b-doccpt.codetb-chro =
                                          string( tables.tecnic[ 2 ], "xxxx" )
                           and   b-doccpt.execpt      =  doccpt.execpt
                           and   b-doccpt.journal     =  doccpt.journal
                           and   b-doccpt.percpt      =  doccpt.percpt
                           and   b-doccpt.docume     <>  doccpt.docume
                           use-index chrono-jnal  no-lock  no-error .
        if not available b-doccpt
           then  doccpt.chrono-jnal = 1 .
           else  doccpt.chrono-jnal = b-doccpt.chrono-jnal + 1 .
        doccpt.codetb-chro = string( tables.tecnic[ 2 ], "xxxx" ) .

        release DOCCPT .

        leave .

    END .

    assign libel-mv    = ""
           piece       = "."
           cod-dom     = ""
           adres       = ""
           ana-segment = ""
           echeance    = ?
           banque      = ""
           reg-dom     = ""
           dom-ech     = ""
           tire        = ""
           typaux      = ""
           auxili      = "" .

    /*  Si Document desequilibre  */

    if tot-mt-ini <> 0  then
    do :

        codgen = cptgen-delta .

        assign  bi-codsoc = codsoc-soc
                bi-codetb = codetb-etb
                bi-execpt = execpt
                bi-percpt = percpt
                bi-datpie = datpie
                bi-devini = x-devise
                bi-mtini  = - tot-mt-ini
                bi-motcle = "" .

        run cp-ventx .

        chrono = string( integer( chrono ) + 1 , "99999" ) .
        { run.i  "s-majbas.p" }
    end .

    /*  Generation Ecriture d'ecart  */

    if tot-mt-cpt <> 0  or  tot-mt-etb <> 0  then
    do :
        codgen = cptgen-delta .

        assign  le-senlet = "D"
                alpha-ct  = ""
                alpha-dt  = string( - tot-mt-cpt , "ZZZZZZZZZZZ9.99-" )
                alpha-dev = string( - tot-mt-etb , "ZZZZZZZZZZZ9.99-" )
                alpha-ini = string(   0          , "ZZZZZZZZZZZ9.99-" ) .

        assign  typtab = "PRM"  prefix = "CP-VENT0"  codtab = "CPTE-ECART" .
        run ch-parax.p( output x-x ) .
        if x-x <> ""  then
        do :
            x-y = entry( 1 , x-x , "/" ) .
            x-z = entry( 2 , x-x , "/" ) no-error .
            if x-y <> ""  then  codgen = x-y .
            if x-z <> ""  and  tot-mt-etb > 0  then
            do :
                codgen = x-z .
                assign  le-senlet = "C"
                        alpha-dt  = ""
                        alpha-ct  = string( tot-mt-cpt , "ZZZZZZZZZZZ9.99-" )
                        alpha-dev = string( tot-mt-etb , "ZZZZZZZZZZZ9.99-" ) .
            end .            
        end .

        chrono = string( integer( chrono ) + 1 , "99999" ) .
        { run.i  "s-majbas.p" }
 
    end .

    assign  tot-mt-cpt = 0
            tot-mt-ini = 0
            tot-mt-etb = 0 .

    leave .

END .

/*      P R O C E D U R E S      */

{ out-mona.i }

{ cp-ventx.i }
