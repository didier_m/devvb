/*==========================================================================*/
/*                           E X T - A G I L T A R  . I                     */
/* Extraction des donnees pour AGIL                                         */
/*--------------------------------------------------------------------------*/
/* Extraction des tarifs client et tarifs promo                             */
/* VLE 10022010 : PA = PA - 0,01 pour palier au bug arrondi Agil            */
/* VLE 24032011  Modification de la date de debut d'application             */
/*               du prix d'achat                                            */
/* VLE 06052011 : suppression du trt des prix d'achat (cf ext-agilpxa.i)    */
/* VLE 03012012 : On ins�re les produits dans la gamme que pour le premier  */
/*                magasin de chaque soci�t�               					*/
/*                la gamme du magasin 991 contient tous les produits de     */
/*                toutes les soci�t�s pour tester toutes les OP             */ 
/*==========================================================================*/

/*---------------------------------------------------------*/                     
/*  Ecriture du d�tail de section Px de Vte Sp�cial Central*/
/*---------------------------------------------------------*/ 
If {tarcli.i}.code-tarif = "CLI"
        then do :

        ID_ARTICLE = caps(trim({tarcli.i}.articl)).
        DT_APPLI_TARIF = {tarcli.i}.date-debut.    
        
		if available tarcli-rpd 
			and code-table = "PD" 
            and {tarcli.i}.date-debut < tarcli-rpd.date-debut
                then DT_APPLI_TARIF = tarcli-rpd.date-debut.  
                
        PX_VTE_SPE_CENTRAL = trim(replace(string(val-tarif, "->>>>9.99"), ".", ",")).
        
        put stream agil unformatted
                        Type_Ligne_Det ID_ARTICLE "~011" DT_APPLI_TARIF "~011" PX_VTE_SPE_CENTRAL "~015" skip. 

end.
/*---------------------------------------------------------*/                     
/*  Ecriture du d�tail de section Px de Vte Sp�cial Promo  */
/*---------------------------------------------------------*/ 
If {tarcli.i}.code-tarif = "PRO"
        then do :

        ID_ARTICLE = caps(trim({tarcli.i}.articl)).
        DT_DEB_PROMO  = {tarcli.i}.date-debut.
        DT_FIN_PROMO  = {tarcli.i}.date-fin.   
        PX_VTE_SPE_PROMO = trim(replace(string(val-tarif, "->>>>9.99"), ".", ",")).
        
        put stream temp_tar unformatted
                Type_Ligne_Det ID_ARTICLE "~011" DT_DEB_PROMO "~011" DT_FIN_PROMO "~011" 
                PX_VTE_SPE_PROMO "~015" skip. 

end.

/*---------------------------------------------------------*/                     
/*  Ecriture du d�tail de de la gamme magasin              */
/*---------------------------------------------------------*/ 
/* VLE 03012012   */
If nb-mag[nsoc] = 1 then do :
	put stream temp_gam unformatted
                        Type_Ligne_Det ID_ARTICLE "~015" skip. 
	nbgamme = nbgamme + 1.
	put stream temp_gam_991 unformatted
                        Type_Ligne_Det ID_ARTICLE "~015" skip. 
end.