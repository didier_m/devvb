/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/ext-p2ta.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 15055 12-07-06!Evo!cch            !Acc�s ARTAPR avec soci�t� de regroupement suite mise en place ED      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - P 2 T A  . I                           */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Tarifs Vente PDP V2                             CC 03/05/1999 */
/*==========================================================================*/

/* DGR 11/12/2001 => prix franc recalcule a partir de l'EURO */
/* DGR 10/01/2002 => recuperation TVA dans TABGES au lieu de TABLES */
/* DGR 15/01/2002 => envoi prix euro ( annule 11/12/2001)  */

def var txtva    as deci decimals 3 format ">9.999" .
def var wval2    as deci decimals 2.

txtva = 1.

if {tarcli.i}.code-tarif = "HTC" then do:
  find artapr
  where artapr.codsoc = soc-articl
  and   artapr.articl = {tarcli.i}.articl
  no-lock no-error.
  if available artapr then do:
    find tabges
    where tabges.codsoc = ""
    and   tabges.etabli = ""
    and   tabges.typtab = "TAX"
    and   tabges.prefix = "TYP-TAXE"
    and   tabges.codtab  = artapr.tva
    no-lock no-error.
    if available tabges then do:
      txtva = 1 + ( tabges.nombre[1] / 100 ).
      if tabges.dattab[1] <> ? and {tarcli.i}.date-debut < tabges.dattab[1] then
        txtva = 1 + ( tabges.nombre[2] / 100 ).
    end.
  end.
end.

       zone = '"CONCENT" "cba014" "' +
              string ( today, "99999999" ) + '" "911" "" "' +
              string ( time, "hh:mm:ss" )  + '" "C" ' .

       put stream kan-pro-v2 unformatted zone skip.
               zone = '"' .

       zone = zone + string ( {tarcli.i}.articl , "xxxxxx" )
                   + string ( year  ( {tarcli.i}.date-debut ) , "9999" )
                   + string ( month ( {tarcli.i}.date-debut ) , "99" )
                   + string ( day   ( {tarcli.i}.date-debut ) , "99" ) .


/*
       /* Modif par CCO le 13/10/1998 */
       wvaleur = tarcli.valeur.
       if wvaleur = 0.001 then wvaleur = 0.
       /* fin modif */
*/

       wval2 = {tarcli.i}.valeur * txtva.
       if wval2 = 0.00 then wvaleur = 0.
       wvaleur = wval2.


       if {tarcli.i}.code-tarif = "PRO"
       then zone = zone + "P" .
       else zone = zone + "S" .
       zone = zone + "T"
                + string ( wvaleur * 1000 , "999999999" )
                + string ( wvaleur * 1000 , "999999999" ) .

       if {tarcli.i}.date-fin <> ?
       then
           zone = zone + string ( year  ( {tarcli.i}.date-fin ) , "9999" )
                       + string ( month ( {tarcli.i}.date-fin ) , "99" )
                       + string ( day   ( {tarcli.i}.date-fin ) , "99" )   .
       else
           zone = zone + "        " .

/* ajout DGR 23-03-01 : quotage zone*/
       zone = zone + '" ' .
/* fin ajout DGR 22-03-01 */