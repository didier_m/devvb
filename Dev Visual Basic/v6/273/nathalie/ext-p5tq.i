/*==========================================================================*/
/*                           E X T - P 5 T Q . I                            */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Tarifs Quantitatifs Clients PDP V5              CC 24/04/1999 */
/*==========================================================================*/

                                                        /* Pos.  Long. */

zone = "TARQTE  "                                       /* 001 - 008 */
       + "C"                                            /* 009 - 001 */
       + string(codsoc-trait[nsoc], "xx")               /* 010 - 002 */
       + fill(" ", 6)                                   /* 012 - 006 */
     /*  + string(codsoc-trait[nsoc], "xx")  */         /* 018 - 002 */
       + fill(" ", 2)                                   /* 018 - 002 */
       .

case remcli.specif :                                    /* 020 - 015 */
    when "" then zone = zone + string(remcli.articl, "x(15)").
    otherwise    zone = zone + fill(" ", 15).
                /*  zone = zone + string(remcli.specif, "x(15)").*/
end case.

zone = zone
       + string(remcli.borne[cc] * 1000, "999999999-")  /* 035 - 010 */
       + string(remcli.valeur[cc] * -1000, "999999999-") /* 045 - 010 */
       .

case remcli.typapp[cc] :                                /* 055 - 001 */
    when "1" then zone = zone + "%".
/* DGR 061201: E(uro) remplace F(ranc) */
/*    when "2" then zone = zone + "F".  */
    when "2" then zone = zone +
                (if (remcli.date-debut < 12/16/2001) or
                    ( remcli.date-debut < 12/16/2001 and
                      remcli.date-fin <> ? ) then "F"
                 else "E") .
    otherwise next.
end case.

case remcli.typ-borne[aa] :                             /* 056 - 001 */
    when ""  then zone = zone + " ".
    when "1" then zone = zone + "Q".
    when "2" then zone = zone + "M".
    when "3" then zone = zone + "P".
    otherwise next.
end case.

case remcli.specif :                                    /* 057 - 001 */
    when "" then zone = zone + "A".
    otherwise    zone = zone + "T".
end case.

zone = zone
       + "CONTLIVR"                                     /* 058 - 008 */
/*       + " "                                            /* 066 - 001 */ */
       + "O"        /* DGR 061201: remise Hors taxe */  /* 066 - 001 */
       + (if remcli.date-debut <> ? then
           string(remcli.date-debut, "99999999")
           else "        ")                             /* 067 - 008 */
       + (if remcli.date-fin <> ? then
           string(remcli.date-fin, "99999999")
           else "        ")           /* 075 - 008 */
       + fill(" ", 6)                                   /* 083 - 006 */
       + string(remcli.specif, "x(8)")                  /* 089 - 008 */
       + " "                                            /* 097 - 001 */
       + fill("0", 7)                                   /* 098 - 007 */
       + fill(" ", 8)                                   /* 105 - 008 */
       .

if remcli.datcre <> ?                                   /* 113 - 008 */
   then zone = zone + string(remcli.datcre, "99999999").
   else zone = zone + fill( " ", 8 ).

if remcli.datmaj <> ?                                   /* 121 - 008 */
   then zone = zone + string(remcli.datmaj, "99999999").
   else zone = zone + fill( " ", 8 ).

zone = zone
       + fill(" ", 2)                                   /* 129 - 002 */
       .

code-tiers = trim(remcli.codaux).
{ cadra-g.i "code-tiers" "6" "0" }

case remcli.specif :
    when "" then zone = zone + string(code-tiers, "x(10)").  /* 131 - 010 */

        otherwise    zone = zone + fill(" ",10).
        end case.

