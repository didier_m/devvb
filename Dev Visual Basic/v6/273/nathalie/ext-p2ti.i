/*==========================================================================*/
/*                           E X T - P 2 T I . I                            */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Tiers PDP V2                                    CC 24/04/1999 */
/*==========================================================================*/

    zone = "" .
    if notour = 2
    then  zone = zone + "004       01 " .
    else  zone = zone + "003       01 " .
    zone = zone + substring ( auxapr.codaux , 5, 6 ) .
    if auxapr.cpt-ferme = "1"
    then  zone = zone + "0" .
    else  zone = zone + "1" .
    if notour <> 2 /* zone non charg�e pour les fournisseurs */
    then do :
          if auxapr.cpt-ferme = "1"
                  then do:
                  zone = zone + "XX" .
                  end.
                  else do:
                  find tabges where tabges.codsoc = "  " and
                       tabges.etabli = "   "        and
                       tabges.typtab = "CLI"        and
                       tabges.prefix = "TYPE-ADH" and
                       tabges.codtab = auxapr.type-adh
                       no-lock  no-error .
                  if available tabges
                  then zone = zone + string ( tabges.libel2[1] , "xx" ) .
                  else zone = zone + "10" .
                       end.
         end .
    zone-intitule = "      " .
    zone-nom = string ( substring ( auxili.adres[1] , 1 , 32 ) , "x(32)" ) .
      if substring ( auxili.adres[1] , 1 , 4 ) = "Mr  " or
         substring ( auxili.adres[1] , 1 , 4 ) = "Mrs " or
         substring ( auxili.adres[1] , 1 , 4 ) = "Mme " or
         substring ( auxili.adres[1] , 1 , 4 ) = "Mle " or
         substring ( auxili.adres[1] , 1 , 4 ) = "Ets " or
         substring ( auxili.adres[1] , 1 , 4 ) = "Ste " or
         substring ( auxili.adres[1] , 1 , 4 ) = "Cie "
      then assign
         zone-nom = string ( substring ( auxili.adres[1] , 5 , 31 ) , "x(31)" )
                    + " "
         zone-intitule = substring ( auxili.adres[1] , 1 , 4 ) + "  " .
      if substring ( auxili.adres[1] , 1 , 3 ) = "MR "
      then assign
         zone-nom = string ( substring ( auxili.adres[1] , 4 , 32 ) , "x(32)" )
         zone-intitule = substring ( auxili.adres[1] , 1 , 3 ) + "   " .
      if substring ( auxili.adres[1] , 1 , 4 ) = "MR  "
      then assign
         zone-nom = string ( substring ( auxili.adres[1] , 5 , 31 ) , "x(32)" )
         zone-intitule = substring ( auxili.adres[1] , 1 , 4 ) + "  " .
    zone = zone + zone-nom
                + string ( substring ( auxili.adres[2] , 1 , 32 ) , "x(32)" )
                + string ( substring ( auxili.adres[3] , 1 , 32 ) , "x(32)" )
                + string ( substring ( auxili.adres[4] , 1 , 32 ) , "x(32)" ) .
    /* le code postal doit etre numerique */
    xxx-valeur = substring ( auxili.adres[5] , 1 , 5 ) .
    { valnum.i xxx-valeur }
    if xxx-valnum = no
    then
        zone = zone + "00000" .
    else
        zone = zone + string ( substring ( auxili.adres[5],1,5) , "x(5)" ).
    if substring ( auxili.adres[5] , 6 , 1 ) = " "
    then zone = zone +
                string ( substring ( auxili.adres[5] , 7 , 26  ) , "x(26)" ).
    else zone = zone +
                string ( substring ( auxili.adres[5] , 6 , 26  ) , "x(26)" ).
    if notour <> 2      /* zone non charg�e pour les fournisseurs */
    then do :
        zone = zone + string ( auxapr.contentieux , "x" ) .
        /* mode de paiement */
              if auxapr.modreg = "LCR" then zone = zone + "5" .
        else  if auxapr.modreg = "PRE" then zone = zone + "4" .
        else  if auxapr.modreg = "VIR" then zone = zone + "6" .
        else  zone = zone + "1" .
        do a = 1 to 10 :
           zone = zone + string ( auxapr.bareme[a] , "xx" )  .
        end .
    end .
    zone = zone + string ( substring ( auxapr.teleph , 1, 15 ) , "x(15)" ) .
    if notour <> 2       /* zone non charg�e pour les fournisseurs */
    then do :
        zone = zone + string ( auxapr.magasin , "xxx" ) + "   "
                    + string ( auxapr.codech , "xxx" ) + "   " .
    end .
    zone = zone + string ( zone-intitule , "x(6)" ) .
