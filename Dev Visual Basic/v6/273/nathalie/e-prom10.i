/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/e-prom10.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V61 52334 02-02-15!Evo!jb             !Modifications de la zone d'affichage des magasins                     !
!                  !   !               !Suppression du filtre sur la soci�t� d'appartenance du magasin        !
!V52 14120 23-01-06!Evo!cch            !Ajout s�lection groupe magas. + �dition lig comment. + �dition typolog!
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E - P R O M 1 0 . I                            */
/* Edition des Bons Promos Plateformes avec Historique Ventes par Article   */
/*--------------------------------------------------------------------------*/
/* Variables et Frames                                        CC 22/09/2003 */
/*==========================================================================*/

def var ind        as int                 no-undo .
def var lib-typpce as char format "x(10)" no-undo .
def var lib-grpmag as char format "x(30)" no-undo .
def var lib-maq    as char format "x(30)" no-undo .
def var val-edi    as char extent 50      no-undo .
def var no-bon     as char                no-undo .

def {1} shared var libelle    as char extent 50      no-undo .
def {1} shared var mot-cle    as char                no-undo .
def {1} shared var type-piece as char format "xxx"   no-undo .
def {1} shared var no-promo   as int  format "9999"  no-undo .
def {1} shared var no-pagin   as int  format "99"    no-undo .
def {1} shared var per-min    as char format "x(6)"  no-undo .
def {1} shared var per-max    as char format "x(6)"  no-undo .
def {1} shared var groupe-mag as char format "xxx"   no-undo .
def {1} shared var magasins   as char format "x(39)" no-undo .
def {1} shared var magasins2  as char format "x(39)" no-undo .
def {1} shared var magasins3  as char format "x(39)" no-undo .
def {1} shared var magasins4  as char format "x(39)" no-undo .
def {1} shared var maquette   as char format "x(12)" no-undo .
def {1} shared var periph     as char format "x(12)" no-undo .
def {1} shared var annee-min  as char                no-undo .
def {1} shared var annee-max  as char                no-undo .
def {1} shared var mois-min   as int                 no-undo .
def {1} shared var mois-max   as int                 no-undo .
def {1} shared var code-mag   as char                no-undo .
def {1} shared var mag-exclus as char                no-undo .
def {1} shared var no-page    as int                 no-undo .
def {1} shared var promo-soc  as char                no-undo .
def {1} shared var magasi-soc as char                no-undo .

def {1} shared buffer pro-entete for entete .

def {1} shared frame fr-saisie .

Form libelle[ 1 ] format "x(20)" type-piece lib-typpce skip ( 1 )
     libelle[ 2 ] format "x(20)" no-promo              skip ( 1 )
	 libelle[ 9 ] format "x(20)" no-pagin			   skip ( 1 ) 	
	 libelle[ 3 ] format "x(20)" per-min               skip ( 1 )
     libelle[ 4 ] format "x(20)" per-max               skip ( 1 )
     libelle[ 5 ] format "x(20)" groupe-mag lib-grpmag skip ( 1 )
     libelle[ 6 ] format "x(20)" magasins              skip 
	 libelle[ 17] format "x(20)" magasins2             skip 
	 libelle[ 18] format "x(20)" magasins3             skip 
	 libelle[ 19] format "x(20)" magasins4             skip ( 1 )
     libelle[ 7 ] format "x(20)" maquette   lib-maq    skip ( 1 )
     libelle[ 8 ] format "x(20)" periph                
	 with frame fr-saisie row 3 centered no-label overlay { v6frame.i } .

