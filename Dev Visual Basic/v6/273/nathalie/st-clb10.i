/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/st-clb10.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 13696 23-11-05!Evo!cch            !Ajout nom spécifique du spool si maquette pour MONARCH                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                          S T - C L B 1 0 . I                             */
/* Statistique Vente Client                                                 */
/* Etat des CA / Depot / Type Vente  / Bons                                 */
/*==========================================================================*/

/* Tbl des Libelles Etat & Tbl d'Edition & enreg fichier tri */
def {1} shared var libelle       as char                 extent 99   no-undo .
def {1} shared var edit          as char                 extent 49   no-undo .
def {1} shared var zone          as char                             no-undo .
def {1} shared stream maquette.                             /* canal edition*/
def {1} shared stream kan-tri .

/* zone de traitement */
def {1} shared var chrono       as int                             no-undo .
def {1} shared var int-datfac   as int                             no-undo .

def {1} shared var ent-mag      as logical                         no-undo .
def {1} shared var ent-typ      as logical                         no-undo .

def {1} shared var nb-mag       as int                             no-undo .
def {1} shared var nb-typ       as int                             no-undo .
def {1} shared var nb-dat       as int                             no-undo .
def {1} shared var nb-bon       as int                             no-undo .

def {1} shared var no-mag       as char                            no-undo .
def {1} shared var lib-mag      as char                            no-undo .
def {1} shared var no-typbon    as char                            no-undo .
def {1} shared var lib-typbon   as char                            no-undo .
def {1} shared var no-client    as char                            no-undo .
def {1} shared var lib-client   as char                            no-undo .
def {1} shared var typ-adh      as char                            no-undo .
def {1} shared var no-bon       as char                            no-undo .
def {1} shared var no-fac       as char                            no-undo .
def {1} shared var da-fac       as char                            no-undo .

def {1} shared var mont-ht      as dec                             no-undo .
def {1} shared var mont-ttc     as dec                             no-undo .
def {1} shared var mont-rab     as dec                             no-undo .
def {1} shared var mont         as dec                 extent  59  no-undo .
def {1} shared var ind-tot      as int                             no-undo .
def {1} shared var lib-tot      as char                            no-undo .
def {1} shared var lib-tit      as char                            no-undo .


/* Definition des zones & de la frame */
def {1} shared var dat-debx     as date format "99/99/9999"         no-undo .
def {1} shared var dat-finx     as date format "99/99/9999"         no-undo .
def {1} shared var mois-deb     as date format "99/99/9999"         no-undo .
def {1} shared var mois-fin     as date format "99/99/9999"         no-undo .
def {1} shared var magasins     as char format "x(40)"              no-undo .
def {1} shared var maquette     as char format "x(12)"              no-undo .
def {1} shared var lib-maq      as char format "x(35)"              no-undo .
def {1} shared var periph       as char format "x(22)"              no-undo .
def {1} shared var sav-periph   as char                             no-undo .
def {1} shared var chemin       as char format "x(18)"              no-undo .

def {1} shared var z-choix      as char                             no-undo .

def {1} shared frame fr-saisie .

form libelle[ 11 ] format "x(26)" dat-debx dat-finx skip
     libelle[ 12 ] format "x(26)" mois-deb mois-fin skip
     libelle[ 13 ] format "x(26)" magasins          skip
     libelle[ 14 ] format "x(26)" maquette lib-maq  skip
     libelle[ 15 ] format "x(26)" periph            skip
     libelle[ 16 ] format "x(26)" chemin
     with frame fr-saisie
     row 3  centered  overlay  no-label  with title libelle[ 10 ]
     { v6frame.i } .