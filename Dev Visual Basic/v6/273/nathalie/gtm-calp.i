/*============================================================================*/
/*                           G T M - C A L P . I                              */
/* Calcul des Tarifs Ventes par Magasins                                      */
/*----------------------------------------------------------------------------*/
/* Procedure de Calcul                                                        */
/*============================================================================*/

PROCEDURE GTM-CALP :

    def input  parameter i-param as char no-undo .
    def output parameter o-param as char no-undo .

    def var articl-soc     as char          no-undo .
    def var code-tarif     as char          no-undo .
    def var code-mag       as char          no-undo .
    def var code-art       as char          no-undo .
    def var code-fam       as char          no-undo .
    def var code-sfam      as char          no-undo .
    def var date-app-a     as char          no-undo .
    def var date-app-d     as date          no-undo .
    def var code-typol-art as char          no-undo .
    def var code-typol-mfs as char          no-undo .
    def var code-typol-sav as char          no-undo .
    def var l-typol        as char          no-undo .
    def var code-autor     as char          no-undo .
    def var tardec-cat     as char          no-undo .
    def var tardec-art     as char          no-undo .
    def var i              as int           no-undo .
    def var j              as int           no-undo .
    def var nb-decim       as int           no-undo .
    def var tarif-base     as dec           no-undo .
    def var coef           as int  extent 4 no-undo .
    def var tarif-mag      as dec  extent 4 no-undo .

    assign l-typol        = "C,M,L,"
           code-tarif     = entry( 1, i-param )
           code-mag       = entry( 2, i-param )
           code-art       = entry( 3, i-param )
           code-fam       = entry( 4, i-param )
           code-sfam      = entry( 5, i-param )
           code-typol-art = entry( 6, i-param )
           date-app-a     = entry( 7, i-param )  /* Format : AAAAMMJJ */
           articl-soc     = entry( 8, i-param )
           code-typol-mfs = right-trim( entry( 9, i-param ) )
           tarif-base     = dec( entry( 10, i-param ) )
           date-app-d     = date( int( substr( date-app-a, 5, 2 ) ),
                                  int( substr( date-app-a, 7, 2 ) ),
                                  int( substr( date-app-a, 1, 4 ) ) )
           no-error .

    if error-status:error or code-tarif = "" or code-mag = "" or code-art = "" or
       ( code-fam = "" and code-sfam <> "" )
       then return .

    /* Acces a ARTAPR si elements non charges */
    if code-fam = ""
    then do :

        if articl-soc = ""
        then do :
            regs-app = "NATHALIE" .
            { regs-cha.i }

            { regs-inc.i "articl" }
            articl-soc = regs-soc .
        end .

        FIND ARTAPR where artapr.codsoc = articl-soc
                    and   artapr.articl = code-art
                    no-lock no-error .

        if not available artapr or code-fam = "" then return .

        assign code-fam       = artapr.famart
               code-sfam      = artapr.soufam
               code-typol-art = artapr.typologie
               .

    end .

    /* Acces a la Typologie si Typologie non chargee */
    if code-typol-mfs = "" or code-typol-mfs = "*"
    then do :

        FIND TYPOLO where typolo.codsoc  = codsoc-soc
                    and   typolo.magasin = code-mag
                    and   typolo.catego  = string( code-fam, "xxx" ) + code-sfam
                    no-lock no-error .

        if available typolo
           then assign code-typol-sav = code-typol-mfs
                       code-typol-mfs = right-trim( typolo.codtyp )
                       .

    end .
    else if code-typol-mfs = "?" then code-typol-mfs = "" .

    /* Determination autorisation Article en Magasin
         Article          Magasin - Categorie
       +++++++++++        +++++++++++++++++++
       C    M    L          L      M      C
       |    |    |          |      |      |
       ----------------------      |      |
       |    |                      |      |
       -----------------------------      |
       |                                  |
       ------------------------------------
    */

    if code-typol-mfs = ""                                 or
       code-typol-art = ""                                 or
       lookup( right-trim( code-typol-mfs ), l-typol ) = 0 or
       lookup( right-trim( code-typol-art ), l-typol ) = 0
       or
       ( code-typol-mfs = entry( 1, l-typol ) and /* Court */
         code-typol-art <> entry( 1, l-typol )    /* Court */
       )
       or
       ( code-typol-mfs = entry( 2, l-typol ) and /* Moyen */
         code-typol-art = entry( 3, l-typol )     /* Long  */
       )
       then code-autor = "N" . /* Article non Autorise en Mag. */
       else code-autor = "O" . /* Article Autorise en Mag.     */

    /* Chargement parametre autorisation en retour */
    o-param = ",,|" + code-autor + ",," .

    if code-typol-sav <> "" then code-typol-mfs = code-typol-sav .

    /* Acces au Code Tarif */
    FIND TABGES where tabges.codsoc = codsoc-soc
                and   tabges.etabli = ""
                and   tabges.typtab = "TAR"
                and   tabges.prefix = "CODE-TARIF"
                and   tabges.codtab = code-tarif
                no-lock no-error .

    if not available tabges then return .

    nb-decim = tabges.nombre[ 4 ] .
    if nb-decim < 0 or nb-decim > 3 then nb-decim = 2 .

    /* Acces Tarif Vente de Base */
    if tarif-base = 0
    then do :

        FIND FIRST {tarcli.i} where {tarcli.i}.motcle      = "VTE"
                              and   {tarcli.i}.codsoc      = codsoc-soc
                              and   {tarcli.i}.typaux      = ""
                              and   {tarcli.i}.codaux      = ""
                              and   {tarcli.i}.articl      = code-art
                              and   {tarcli.i}.code-tarif  = code-tarif
                              and   {tarcli.i}.date-debut <= date-app-d
                              and ( {tarcli.i}.date-fin   >= date-app-d or
                                    {tarcli.i}.date-fin   = ? )
                              use-index primaire no-lock no-error .

        if not available {tarcli.i} then return .

        tarif-base = {tarcli.i}.valeur .

    end .

    if tarif-base <= 0 then return .

    /* Recherche des Coefficients de Calcul */
    do i = 1 to 6 :

        case i :

            /* Magasin + Article */
            when 1 then assign tardec-cat = code-mag
                               tardec-art = code-art
                               .

            /* Article */
            when 2 then assign tardec-cat = ""
                               tardec-art = code-art
                               .

            /* Magasin + Famille + Sous Famille */
            when 3 then assign tardec-cat = string( code-mag, "xxx" ) +
                                            string( code-fam, "xxx" ) +
                                            code-sfam
                               tardec-art = ""
                               .

            /* Famille + Sous Famille */
            when 4 then assign tardec-cat = "   " + string( code-fam, "xxx" ) +
                                            code-sfam
                               tardec-art = ""
                               .

            /* Magasin + Famille */
            when 5 then assign tardec-cat = string( code-mag, "xxx" ) + code-fam
                               tardec-art = ""
                               .

            /* Famille */
            when 6 then assign tardec-cat = "   " + code-fam
                               tardec-art = ""
                               .
        end case .

        /* Acces au Coefficient de Calcul du tarif Magasin */
        FIND LAST TARDEC where tardec.codsoc = codsoc-soc
                         and   tardec.catego = tardec-cat
                         and   tardec.articl = tardec-art
                         and   tardec.groupe <=  "GTM" + date-app-a
                         use-index article no-lock no-error .

        if not available tardec then next .

        if code-typol-mfs = "*" /* Toutes les Typologies */
        then do j = 1 to 4 :
            coef[ j ] = int( entry( j, tardec.grille-remise ) ) no-error .
            if coef[ j ] <= 0 then return .
        end .
        else do :
            coef[ 1 ] = int( entry( lookup( code-typol-mfs, l-typol ), tardec.grille-remise ) ) no-error .
            if coef[ 1 ] <= 0 then return .
        end .

        leave .

    end . /* do i = 1 to 6 : */

    /* Calcul du Tarif Magasin */
    do i = 1 to 4 :

        if coef[ i ] <> 0
           then tarif-mag[ i ] = round( tarif-base * ( coef[ i ] / 100 ), nb-decim ) .
           else tarif-mag[ i ] = tarif-base .

        if code-typol-mfs <> "*" then leave .

    end .

    /* Chargement parametres en retour */
    o-param = string( tarif-mag[ 1 ] ) + "|" +                    /* entry 1 */
              string( tarif-mag[ 2 ] ) + "|" +
              string( tarif-mag[ 3 ] ) + "|" +
              string( tarif-mag[ 4 ] ) + "," +

              string( coef[ 1 ] ) + "|" +                         /* entry 2 */
              string( coef[ 2 ] ) + "|" +
              string( coef[ 3 ] ) + "|" +
              string( coef[ 4 ] ) + "," +

              code-typol-mfs + "|" + code-autor + "," +           /* entry 3 */


              string( tarif-base ) + "|"                          /* entry 4 */
              .

    if available {tarcli.i}
    then do :

        o-param = o-param + string( {tarcli.i}.date-debut, "99/99/9999" ) + "|" .

        if {tarcli.i}.date-fin <> ?
           then o-param = o-param + string( {tarcli.i}.date-fin, "99/99/9999" ) .

    end .
    else o-param = o-param + "|" .

    o-param = o-param + "," .

    if available tardec
       then entry( 5, o-param ) = string( tardec.groupe ) + "|" +   /* entry 5 */
                                  string( tardec.catego ) + "|" +
                                  string( tardec.articl ) .

END PROCEDURE .
