/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/ext-p5et.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 16252 20-09-07!Evo!cch            !Ajout gestion de l'Ecotaxe Article ( fait par DGR )                   !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - P 5 E T  . I                           */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Articles PDP V5 : ECOTAXE                      DGR 14/09/2007 */
/*==========================================================================*/


                                                        /* Pos.  Long. */

zone = "INFO_ART"                                       /* 001 - 008 */
       + "C"                                            /* 009 - 001 */
       + string(codsoc-trait[nsoc], "xx")               /* 010 - 002 */
       + fill(" ", 6)                                   /* 012 - 006 */
       + fill(" ", 20)                                  /* 018 - 020 */
       + substr(string(today,"99999999"),5,4)
       + substr(string(today,"99999999"),3,2)
       + substr(string(today,"99999999"),1,2)
       + string(tabges.nombre[15] * 100 , "-99999999")         /* 046 - 009 */
       + string(tabges.codtab, "x(15)")                /* 055 - 015 */
       + substr(string(today,"99999999"),5,4)
       + substr(string(today,"99999999"),3,2)
       + substr(string(today,"99999999"),1,2)
       + substr(string(today,"99999999"),5,4)
       + substr(string(today,"99999999"),3,2)
       + substr(string(today,"99999999"),1,2)
       + fill(" ", 9)                                   /* 086 - 009 */
       + "ECOPAR".