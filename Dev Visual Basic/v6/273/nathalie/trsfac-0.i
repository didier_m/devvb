/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/trsfac-0.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 16461 23-11-07!Evo!cch            !Ajout intégration des factures DISPAGRI EUREA DISTRI                  !
!V52 15684 12-02-07!Evo!cch            !Ajout intégration des factures SODEVAL                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*---------------------------------------------------------------*/
/* Interface de remontee des factures Fournisseurs (Version CBA) */
/*                                                               */
/* trsfac-0.i                                                    */
/*---------------------------------------------------------------*/

Def {1} shared var verr         as log           no-undo .
Def {1} shared var verr1        as log           no-undo .
Def {1} shared var errfic       as char          no-undo .
Def {1} shared var codaux-trait as char extent 8 no-undo .
Def {1} shared var vtypaux      as char          no-undo .
Def {1} shared var naux         as int           no-undo .
Def {1} shared var ficfind      as char          no-undo .
Def {1} shared var fichier      as char          no-undo .
Def {1} shared var cod-inter    as char          no-undo .
Def {1} shared var trait-repert as char          no-undo .

Def {1} shared stream err.

/* Initialisations communes */
assign codaux-trait [1] = "    802190" /* LISADIS               */
       codaux-trait [2] = "    800595" /* DISPAGRI              */
       codaux-trait [3] = "    018133" /* COFNA                 */
       codaux-trait [4] = "    019677" /* RHONALDIS             */
       codaux-trait [5] = "    018777" /* PERRIOL               */
       codaux-trait [6] = "    805100" /* LISADIS VEGETAL       */
       codaux-trait [7] = "    801547" /* SODEVAL EUREA DISTRI  */
       codaux-trait [8] = "    800595" /* DISPAGRI EUREA DISTRI */
       .

{ top-asec.i }
if top-asec <> "O" then trait-repert = "factcba/".
                   else trait-repert = "factasec/".

if codsoc-soc = "45" then trait-repert = "" .