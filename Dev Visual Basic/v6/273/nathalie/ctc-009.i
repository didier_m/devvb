/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/ctc-009.i                                                                    !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 15055 12-07-06!Evo!cch            !Acc�s ARTAPR avec soci�t� de regroupement suite mise en place ED      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*=======================================================================*/
/*                             C T C - 0 0 9 . I                         */
/*       Constitution du Fichier TARPRE : Preparation Tarif Clients      */
/*=======================================================================*/

FIND FIRST REMCLI where remcli.motcle = {1}.motcle
                  and   remcli.codsoc = {1}.codsoc
                  and   remcli.typaux = {1}.typaux
                  and   remcli.codaux = {1}.codaux
                  and   remcli.articl = {1}.articl
                  and   remcli.specif = code-tarif
                  and   remcli.date-debut <= {1}.date-debut
                  use-index primaire  no-lock  no-error .
if available remcli then
do :
    pxbrut = pxacha .
    do ii = 1 to 10 :

       if remcli.codrem[ ii ] =  "" then next .    /*  Code Remise Informe    */
       if remcli.typrem[ ii ] <> "1" then next .   /*  Sur Facture Seulement  */
       if remcli.typ-borne[ ii ] <> "" then next . /*  Inconditionnelles Seul.*/

       FIND {n-codrem.i} where {n-codrem.i}.motcle = "ACH"
                         and   {n-codrem.i}.codsoc = codsoc-soc
                         and   {n-codrem.i}.codrem = remcli.codrem[ ii ]
                         no-lock  no-error .
       if available {n-codrem.i} then
       do :
           /*    Shunt Remises : Enlevement , Conditionnement , Magasin    */
           if {n-codrem.i}.typspe = "1"  or  {n-codrem.i}.typspe = "2"  or
              {n-codrem.i}.typspe = "3"  then next .

           /*    Shunt Remise si non incidence sur le Prix de Vente   */
           if {n-codrem.i}.incid-pv = "1"  then  next .
       end .

       jj = ( ( ii - 1 ) * 10 ) + 1 .

             /*    Pourcentage en Cascade   */

       if remcli.typapp[ jj ] = "1" then
          assign remise = round( ( pxacha * remcli.valeur[ jj ] ) / 100 , 2 )
                 pxacha = pxacha - round( remise , 2 ) .

             /*    Francs  par  Unite  */

       if remcli.typapp[ jj ] = "2" then
          pxacha = pxacha - remcli.valeur[ jj ] .

             /*    Forfait  */

       if remcli.typapp[ jj ] = "3" then next .

             /*    Pourcentage sur Prix Brut  */

       if remcli.typapp[ jj ] = "4" then 
          assign remise = round( ( pxbrut * remcli.valeur[ jj ] ) / 100 , 2 )
                 pxacha = pxacha - round( remise , 2 ) .

    end .

end .