/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/ext-agilsto.i                                                              !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 61252 11-10-16!Evo!pha            !report modifs 273                                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - A G I L S T O  . I                     */
/* Extraction des donnees pour AGIL                                         */
/*--------------------------------------------------------------------------*/
/* Extraction stocks magasin                                                */
/*==========================================================================*/

/*-------------------------------------------*/                           
/*  Ecriture du d�tail de section STOCKS     */
/*-------------------------------------------*/ 

ID_ARTICLE = caps(trim(stocks.articl)).           

/* DGR 12082013 
 ***** QTE-STOCK = trim(replace(string(stocks.qte[2], "->>>>>>>>9.99"), ".", ",")).
*/

QTE-STOCK = trim(replace(string(stophy, "->>>>>>>>9.99"), ".", ",")).

/* fin DGR 12082013 */


put stream agil unformatted
                      Type_Ligne_Det ID_ARTICLE "~011" QTE-STOCK "~015" skip. 

