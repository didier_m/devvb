
/*==========================================================================*/
/*                           E X T - P 5 T E  . I                           */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction tarif par code ecart PDP V5                     BR 14/02/2000 */
/*==========================================================================*/

def var wart as char.
  
zone = "".

wart = entry(bb,tabges.libel1[aa]).
if wart = "" then next.  
wart = string(wart,"x(15)").

if tabges.libel1[2] = "H" then do:
  do while index(wart,"-")  <> 0:
    overlay(wart,index(wart,"-"),1,"CHARACTER") = ",".
  end.
end.
zone = "TARECART"                                     /* 001 - 008 */
       + "C"                                            /* 009 - 001 */
       + string(tabges.codsoc, "xx")                    /* 010 - 002 */
       + fill(" ", 6)                                   /* 012 - 006 */
       + string(tabges.codsoc, "xx")                    /* 018 - 002 */
       + string(wart, "x(15)")                          /* 020 - 015 */
       + string(tabges.nombre[1] * 1000 , "-999999999") /* 035 - 010 */
       + string(tabges.libel1[3], "x")                  /* 045 - 001 */
       + string(tabges.libel1[2], "x")                  /* 046 - 001 */
       + "FIDELITE"                                     /* 047 - 008 */
       + string(tabges.libel1[4], "x")                  /* 055 - 001 */
       + string(tabges.dattab[1], "99999999")           /* 056 - 008 */
       + string(tabges.dattab[2], "99999999")           /* 064 - 008 */
       + fill(" ", 6)                                   /* 072 - 006 */
       + string(tabges.codtab, "xxxxxxxx")              /* 078 - 008 */
       + fill(" ", 1)                                   /* 086 - 001 */
       + fill("0", 7)                                   /* 087 - 007 */
       + fill("0", 8)                                   /* 094 - 008 */
       + string(tabges.datcre, "99999999")              /* 102 - 008 */
       + string(tabges.datmaj, "99999999")              /* 110 - 008 */
       + fill(" ", 32 )                                 /* 118 - 032 */
       .
