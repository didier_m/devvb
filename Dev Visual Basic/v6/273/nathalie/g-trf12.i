/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/g-trf12.i                                                                  !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 40729 04-09-13!Evo!jcc            !Acces préalable au tarif avec le code tiers                           !
!V52 16904 06-06-08!Evo!cch            !Suite fiche 16903 - Mise au point suite tests                         !
!V52 13668 16-11-05!Evo!cch            !Fin développement dossier Tarifs PROMO Plateformes                    !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*-----------------------------------------------------------*/
/*               g - t r f 1 2 . i                           */
/*-----------------------------------------------------------*/

if type-trt <> 5  then
do :
    typaux = type-promo .
    if magasin <> ""  then  auxili = magasin .
                      else  auxili = groupe-mag .
end .

lect = "{2}" .
if lect <> ""  and  lect <> "999"
then  do :
    FIND {tarcli.i} where {tarcli.i}.motcle     = motcle-trt
                    and   {tarcli.i}.codsoc     = codsoc-soc
                    and   {tarcli.i}.typaux     = typaux
                    and   {tarcli.i}.codaux     = auxili
                    and   {tarcli.i}.articl     = articl
                    and   {tarcli.i}.date-debut = date-debut
                    and   {tarcli.i}.chrono     = {2}
                    use-index chrono-art  no-lock  no-error .
end .
else  do :
    FIND {1} {tarcli.i} where {tarcli.i}.motcle     = motcle-trt
                        and   {tarcli.i}.codsoc     = codsoc-soc
                        and   {tarcli.i}.typaux     = typaux
                        and   {tarcli.i}.codaux     = auxili
                        and   {tarcli.i}.articl     = articl
                        and   {tarcli.i}.date-debut = date-debut
                        use-index chrono-art  no-lock  no-error .
end .

