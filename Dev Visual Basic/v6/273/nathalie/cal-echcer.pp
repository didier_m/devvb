/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/cal-echcer.pp                                                              !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 307589 2020-01-08!Evo!Patrick        !Migration V7 de la consultation des soldes agriculteurs               !
!V61 29930 04-04-12!Bug!cch            !Suite Fiche V61 29929                                                 !
!V61 29929 04-04-12!Evo!cch            !Ajout lecture cde entete.env-plat = '' pour les CCI                   !
!V61 27646 01-12-11!Bug!cch            !Correction bastat.typaux au lieu de bastat.codaux ligne 632           !
!V61 26854 14-10-11!Evo!cch            !Pour REMERE on ne prend plus les mvts avec compl�ments de prix        !
!V61 26551 27-09-11!Bug!cch            !Correction pble calcul qte soulte en int�gration livraisons DECLIC    !
!V61 25976 23-08-11!Evo!cch            !Suite Fiche V61 25931                                                 !
!V61 25931 19-08-11!Evo!cch            !Ajout Taux Incorporation TAF ( EUREA )                                !
!V61 25930 19-08-11!Evo!cch            !Qt�s TAF envoy�es dans zone qt� entr�e au lieu de qt� solde           !
!V61 25920 18-08-11!Evo!cch            !Suite Fiche V61 25919                                                 !
!V61 25919 18-08-11!Evo!cch            !Ajout possibilit� de traiter plusieurs statuts mvts apports           !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                         C A L - E C H C E R . P P                        */
/*                                                                          */
/* CHARGEMENT DONNEES ECHANGE CEREALES ( REMERE, SOULTE, TAF )              */
/*--------------------------------------------------------------------------*/
/* Procedure de chargement donnees                    06/2011 - 273 - EUREA */
/*==========================================================================*/


{ pds-norm.i new }


if mem-langue = 0 then mem-langue = 1.

/* Chargement donnees echange aliments */ 
PROCEDURE CAL-ECHCER :

    /*********************************************** INPUT PARAMETERS ************************************************/
    /* - 1- trt-remere    = Traitement REMERE - "O" = Oui                                                            */
    /* - 2- trt-soulte    = Traitement SOULTE - "O" = Oui                                                            */
    /* - 3- trt-taf       = Traitement TAF - "O" = Oui                                                               */
    /* - 4- type-prg      = Type programme appelant - "1" = Commande - "2" = Livraison - "3" = Consultation echanges */
    /* - 5- codsoc-apr    = Societe Appros                                                                           */
    /* - 6- codsoc-app    = Societe Apport                                                                           */
    /* - 7- codsoc-cer    = Societe regroupement Article Apport - Si "*" alors chgt                                  */
    /* - 8- codsoc-art    = Societe regroupement Article Appros - Si "*" alors chgt                                  */
    /* - 9- codsoc-tie    = Societe regroupement Tiers gestion - Si "*" alors chgt                                   */
    /* -10- type-aux[ 1 ] = Type Tiers Appros                                                                        */
    /* -11- code-aux[ 1 ] = Code Tiers Appros                                                                        */
    /* -12- code-art[ 1 ] = Code Article Appros - Si "*" pas d'Article                                               */
    /* -13- date-mvt      = Date Mouvement - "AAAAMMJJ" - Si "*" alors Today                                         */
    /* -14- date-trf      = Date Tarif Appros - Si "*" alors Today                                                   */
    /* -15- no-contrat    = No Contrat Apport - Si "*" alors tous                                                    */
    /* -16- rowid-cde-a   = Rowid ligne de commande si type-prg = "2"                                                */
    /*****************************************************************************************************************/

    def input parameter i-param as char no-undo .

    def var o-param as char no-undo .

    def buffer b-auxgss for auxgss .

    assign trt-remere    = entry(  1, i-param )
           trt-soulte    = entry(  2, i-param )
           trt-taf       = entry(  3, i-param )
           type-prg      = entry(  4, i-param )
           codsoc-apr    = entry(  5, i-param )   
           codsoc-app    = entry(  6, i-param )   
           codsoc-cer    = entry(  7, i-param )   
           codsoc-art    = entry(  8, i-param )   
           codsoc-tie    = entry(  9, i-param )   
           type-aux[ 1 ] = entry( 10, i-param )   
           code-aux[ 1 ] = entry( 11, i-param )   
           code-art[ 1 ] = entry( 12, i-param )   
           date-mvt-1    = entry( 13, i-param )   
           date-trf-1    = entry( 14, i-param )   
           no-contrat    = entry( 15, i-param )   
           rowid-cde-a   = entry( 16, i-param )   
           no-error .                       

    if error-status:error then return .

    /* Chargement societes regroupement */
    if codsoc-cer = "*" or codsoc-art = "*" or codsoc-tie = "*"
    then do :

        codsoc-sav = codsoc-soc .

        if codsoc-cer = "*" and ( trt-remere = "O" or trt-soulte = "O" )
        then do :
            codsoc-soc = codsoc-app .
            regs-app = "NATHALIE" .
            { regs-cha.i }

            { regs-inc.i "CEREAL" }
            codsoc-cer = regs-soc .
        end .

        if codsoc-art = "*" or codsoc-tie = "*"
        then do :

            codsoc-soc = codsoc-apr .
            regs-app = "NATHALIE" .
            { regs-cha.i }

            if codsoc-art = "*" and ( trt-remere = "O" or trt-soulte = "O" )
            then do :
                { regs-inc.i "ARTICL" }
                codsoc-art = regs-soc .
            end .

            if codsoc-tie = "*"
            then do :
                { regs-inc.i "AUXSPE" }
                codsoc-tie = regs-soc .
            end .

        end .

        if codsoc-soc <> codsoc-sav
        then do :
            codsoc-soc = codsoc-sav .
            regs-app = "NATHALIE" .
            { regs-cha.i }
        end .

    end . /* if codsoc-cer = "*" or codsoc-art = "*" or codsoc-tie = "*" */

    /* Chargement Tiers Apport echange cereales */
    if type-prg <> "3" /* <> Consultation */
    then do :

        FIND AUXGSS where auxgss.codsoc = codsoc-tie
                    and   auxgss.typaux = type-aux[ 1 ] 
                    and   auxgss.codaux = code-aux[ 1 ] 
                    and   auxgss.theme  = "REMERE"
                    and   auxgss.chrono = 0
                    use-index primaire no-lock no-error .

        if not available auxgss or auxgss.alpha[ 1 ] = "" or auxgss.alpha[ 2 ] = "" then return .

    end .

    assign type-aux[ 2 ] = auxgss.alpha[ 1 ]
           code-aux[ 2 ] = auxgss.alpha[ 2 ]
           .

    if type-prg = "2" or type-prg = "3" /* Livraison ou Consultation */
    then do :
        EMPTY TEMP-TABLE T-TIERS .
        EMPTY TEMP-TABLE T-ECHCER .
    end .

    if trt-remere = "O" or trt-soulte = "O" /* REMERE ou SOULTE */
    then do :

        /* Chargement Tiers acheteurs */
        FOR EACH B-AUXGSS where b-auxgss.codsoc     = codsoc-tie
                          and   b-auxgss.theme      = "REMERE"
                          and   b-auxgss.chrono     = 0
                          and   b-auxgss.alpha[ 1 ] = type-aux[ 2 ]
                          and   b-auxgss.alpha[ 2 ] = code-aux[ 2 ]
                          use-index theme no-lock :

            CREATE T-TIERS .

            assign t-tiers.typaux = b-auxgss.typaux
                   t-tiers.codaux = b-auxgss.codaux
                   .

        END .

        /* Chargement Campagne */
        case date-mvt-1 :

            when "*" then date-mvt-2 = today .

            otherwise do :

                assign date-mvt-2 = ?
                       date-mvt-2 = date( int( substr( date-mvt-1, 5, 2 ) ),
                                          int( substr( date-mvt-1, 7, 2 ) ),
                                          int( substr( date-mvt-1, 1, 4 ) ) )
                                          no-error .

                if date-mvt-2 = ? then date-mvt-2 = today .

            end .

        end case .

        if month( date-mvt-2 ) < 7 then assign code-camp     = string( year( date-mvt-2 ) - 1, "9999" )
                                               date-deb-camp = date( 07, 01, year( date-mvt-2 ) - 1 )
                                               date-fin-camp = date( 06, 30, year( date-mvt-2 ) )
                                               .
                                   else assign code-camp     = string( year( date-mvt-2 ), "9999" )
                                               date-deb-camp = date( 07, 01, year( date-mvt-2 ) )
                                               date-fin-camp = date( 06, 30, year( date-mvt-2 ) + 1 )
                                               .

    end . /* if trt-remere = "O" or trt-soulte = "O" */

    /* Chargement Parametres */
    assign  typtab = "PRM"  prefix = "CAL-ECHCER"  codtab = "PARAMETRES" .
    run ch-paray.p( output o-paramet ) .

    if o-paramet <> "" then assign type-fac          = entry(  1, o-paramet, "|" )
                                   l-type-con        = entry(  2, o-paramet, "|" )
                                   l-statut          = entry(  3, o-paramet, "|" )
                                   l-code-mvt[ 1 ]   = entry(  4, o-paramet, "|" )
                                   l-code-mvt[ 2 ]   = entry(  5, o-paramet, "|" )
                                   l-code-mvt[ 3 ]   = entry(  6, o-paramet, "|" )
                                   l-code-mvt[ 4 ]   = entry(  7, o-paramet, "|" )
                                   motcle-vente      = entry(  8, o-paramet, "|" )
                                   motcle-taf        = entry(  9, o-paramet, "|" )
                                   code-trf          = entry( 10, o-paramet, "|" )
                                   coef-qte          = dec( entry( 11, o-paramet, "|" ) )
                                   l-soc-remere      = entry( 31, o-paramet, "|" )
                                   l-cer-remere[ 1 ] = entry( 32, o-paramet, "|" )
                                   l-cer-remere[ 1 ] = l-cer-remere[ 1 ] + entry( 33, o-paramet, "|" )
                                   l-cer-remere[ 2 ] = entry( 34, o-paramet, "|" )
                                   l-cer-remere[ 2 ] =  l-cer-remere[ 2 ] + entry( 35, o-paramet, "|" )
                                   l-art-remere[ 1 ] = entry( 36, o-paramet, "|" )
                                   l-art-remere[ 2 ] = entry( 37, o-paramet, "|" )
                                   art-remere        = entry( 38, o-paramet, "|" )
                                   l-cer-ble         = entry( 39, o-paramet, "|" )
                                   l-cer-orge        = entry( 40, o-paramet, "|" )
                                   l-cer-trit        = entry( 41, o-paramet, "|" )
                                   l-cer-mais        = entry( 42, o-paramet, "|" )
                                   l-libech          = entry( 43, o-paramet, "|" )
                                   l-libtaf          = entry( 44, o-paramet, "|" )
                                   l-libtot          = entry( 45, o-paramet, "|" )
                                   top-compl-px      = entry( 46, o-paramet, "|" )
                                   .

    if type-fac          = ""      then type-fac          = "A" .
    if l-type-con        = ""      then l-type-con        = "SOF,SOV" .
    if l-statut          = ""      then l-statut          = ",D" .
    if l-code-mvt[ 1 ]   = ""      then l-code-mvt[ 1 ]   = "FEO/CAP" .
    if l-code-mvt[ 2 ]   = ""      then l-code-mvt[ 2 ]   = "CCI" .
    if l-code-mvt[ 3 ]   = ""      then l-code-mvt[ 3 ]   = "CDV,CDS" .
    if l-code-mvt[ 4 ]   = ""      then l-code-mvt[ 4 ]   = "LIV" .
    if motcle-vente      = ""      then motcle-vente      = "VTE" .
    if motcle-taf        = ""      then motcle-taf        = "DEC/ECA" .
    if code-trf          = ""      then code-trf          = "HTC" .
    if coef-qte          = 0       then coef-qte          = 1000 .
    if l-soc-remere      = ""      then l-soc-remere      = "01,08" .
    if l-cer-remere[ 1 ] = ""      then l-cer-remere[ 1 ] = "0021,0023,0028,0040,0041,0043,0050,0053,0058,0060,0080,0083,0088,0090,0110,0113,1021,1023,1040,1050,1053,1060,1080,1083,1110,1090" .
    if l-cer-remere[ 2 ] = ""      then l-cer-remere[ 2 ] = "0020,0021,0023,0040,0041,0043,0050,0053,0060,0080,0083,0088,0090,0110,1021,1023,1040,1043,1050,1053,1060,1080,1083,1110,1090" .
    if l-art-remere[ 1 ] = ""      then l-art-remere[ 1 ] = "723309,723310,723320" .
    if l-art-remere[ 2 ] = ""      then l-art-remere[ 2 ] = "724322,724323" .
    if art-remere        = ""      then art-remere        = "999850" .
    if l-cer-ble         = ""      then l-cer-ble         = "0023,1023,2023" .
    if l-cer-orge        = ""      then l-cer-orge        = "0053,1053,2053" . 
    if l-cer-trit        = ""      then l-cer-trit        = "0083,1083,2083" .
    if l-cer-mais        = ""      then l-cer-mais        = "0043,1043,2043" .
    if num-entries( l-libech ) < 3 then l-libech          = "REMERE,SOULTE,TAF" .
    if num-entries( l-libtaf ) < 4 then l-libtaf          = "BLE,ORGE,TRITICALE,MAIS" .
    if num-entries( l-libtot ) < 3 then l-libtot          = ",TOTAUX SOULTE,TOTAUX TAF" .
    if top-compl-px      = ""      then top-compl-px      = "N" .

    ok-qte = no .

    if trt-remere = "O" then run trt-remere . /* Traitement REMERE */

    if trt-soulte = "O" then run trt-soulte . /* Traitement SOULTE */

    if trt-taf    = "O" then run trt-taf .    /* Traitement TAF    */

    VALIDATE T-ECHCER .
    RELEASE T-ECHCER .

END PROCEDURE . /* PROCEDURE CAL-ECHCER */

/* Traitement REMERE */
PROCEDURE TRT-REMERE :

    assign ind = 0
           ind = lookup( right-trim( codsoc-app ), l-soc-remere ) no-error     
           .

    if ind = 0 or ind > 2 then return .

    assign type-ech       = 1
           l-art-remere-2 = l-art-remere[ ind ]
           l-cer-remere-2 = l-cer-remere[ ind ]
           l-code-mvt-2   = ""
           l-code-mvt-2   = entry( 2, l-code-mvt[ 1 ], "/" ) no-error
           .

    CREATE T-ECHCER .

    assign t-echcer.typech = type-ech
           t-echcer.libech = entry( type-ech, l-libech )
           t-echcer.articl = art-remere
           .

    FIND ARTAPR where artapr.codsoc = codsoc-art
                and   artapr.articl = art-remere
                no-lock no-error .

    if available artapr then t-echcer.libart = artapr.libart1[ mem-langue ] .

    /* Mouvements Apport */
    do tour-mvt = 1 to num-entries( l-code-mvt-2 ) :

        code-mvt = entry( tour-mvt, l-code-mvt-2 ) .

        do tour-cer = 1 to num-entries( l-cer-remere-2 ) :

            code-cer = entry( tour-cer, l-cer-remere-2 ) .

            { c-cereal.i "code-cer" }          

            { l-cereal.i "codsoc-cer" "code-camp" "'001'" "code-cer" "libel" }

            if not available cereal then next .

            do tour-statut = 1 to num-entries( l-statut ) :

                statut = entry( tour-statut, l-statut ) .

                FOR EACH CERMVT where cermvt.codsoc    =  codsoc-app
                                and   cermvt.campagne  =  code-camp
                                and   cermvt.typfac    =  type-fac
                                and   cermvt.statut    =  statut
                                and   cermvt.codmvt    =  code-mvt
                                and   cermvt.typaux    =  type-aux[ 2 ]
                                and   cermvt.codaux    =  code-aux[ 2 ]
                                and   cermvt.codcer    =  code-cer
                                and   cermvt.top-compl <> top-compl-px
                                use-index codmvt no-lock :

                    { calpdsno.i }

                    t-echcer.qte-ent[ 1 ] = t-echcer.qte-ent[ 1 ] + ( pds-norm * coef-qte ) . /* Qte entree */

                END .

            end . /* do tour-statut = 1 to num-entries( l-statut ) : */

        end . /* do tour-cer = 1 to num-entries( l-cer-remere-2 ) : */

    end . /* do tour-mvt = 1 to num-entries( l-code-mvt-2 ) : */

    /* Mouvements Appros */
    do tour-art = 1 to num-entries( l-art-remere-2 ) :

        code-art[ 2 ] = entry( tour-art, l-art-remere-2 ) .

        { cad-arti.i "code-art[ 2 ]" }

        /* Tiers Acheteurs */
        FOR EACH T-TIERS use-index i-tiers-1 no-lock :

            /* Commandes initiales non traitees */
            do tour-mvt = 1 to 2 :

                case tour-mvt :
                    when 1 then code-envplat = "" .
                    when 2 then code-envplat = "EDN" .
                end case .

                FOR EACH ENTETE where entete.codsoc   = codsoc-apr
                                and   entete.motcle   = motcle-vente
                                and   entete.env-plat = code-envplat
                                and   entete.typaux   = t-tiers.typaux
                                and   entete.codaux   = t-tiers.codaux
                                and   lookup( right-trim( entete.typcom ), l-code-mvt[ 2 ] ) <> 0
                                use-index entete04 no-lock :

                    FOR EACH LIGNES where lignes.codsoc = entete.codsoc
                                    and   lignes.motcle = entete.motcle
                                    and   lignes.typcom = entete.typcom
                                    and   lignes.numbon = entete.numbon
                                    and   lignes.articl = code-art[ 2 ]
                                    use-index primaire no-lock :

                        i-paramet = "L" .

                        run trt-appros-remere .

                    END . /* FOR EACH LIGNES */  

                END . /* FOR EACH ENTETE */

            end . /* do tour-mvt = 1 to 2 : */

            /* Commandes non soldees */
            FOR EACH LIGNES where lignes.codsoc    = codsoc-apr
                            and   lignes.motcle    = motcle-vente
                            and   lignes.articl    = code-art[ 2 ]
                            and   lignes.nivcom    = ""
                            and   lignes.top-livre = ""
                            and   lignes.typaux    = t-tiers.typaux
                            and   lignes.codaux    = t-tiers.codaux
                            and   lookup( right-trim( lignes.typcom ), l-code-mvt[ 3 ] ) <> 0
                            use-index encours no-lock :

                i-paramet = "L" .

                run trt-appros-remere .

            END . /* FOR EACH LIGNES */  

            /* Livraisons */
            FOR EACH LIGNES where lignes.codsoc =  codsoc-apr
                            and   lignes.articl =  code-art[ 2 ]
                            and   lignes.datdep >= date-deb-camp
                            and   lignes.datdep <= date-fin-camp
                            and   lignes.nivcom <  "7"
                            and   lignes.typaux =  t-tiers.typaux
                            and   lignes.codaux =  t-tiers.codaux
                            and   lookup( right-trim( lignes.typcom ), l-code-mvt[ 4 ] ) <> 0
                            use-index artdat no-lock :

                i-paramet = "L" .

                run trt-appros-remere .

            END . /* FOR EACH LIGNES */

            FOR EACH BASTAT where bastat.codsoc =  codsoc-apr
                            and   bastat.articl =  code-art[ 2 ]
                            and   bastat.datdep >= date-deb-camp
                            and   bastat.datdep <= date-fin-camp
                            and   bastat.typaux =  t-tiers.typaux
                            and   bastat.codaux =  t-tiers.codaux
                            and   lookup( right-trim( bastat.typcom ), l-code-mvt[ 4 ] ) <> 0
                            use-index artdat no-lock :

                i-paramet = "B" .

                run trt-appros-remere .

            END . /* FOR EACH BASTAT */

        END . /* FOR EACH T-TIERS */

    end . /* do tour-art = 1 to num-entries( l-art-remere-2 ) : */

    assign t-echcer.qte-ent[ 2 ] = t-echcer.qte-ent[ 1 ]
           t-echcer.qte-sol      = t-echcer.qte-ent[ 1 ] - t-echcer.qte-sor .
           .

    if t-echcer.qte-sol <> 0
       then ok-qte = yes .
       else if type-prg = "3" and ( t-echcer.qte-ent[ 1 ] <> 0 or t-echcer.qte-sor <> 0 )
               then ok-qte = yes .

END PROCEDURE . /* PROCEDURE TRT-REMERE */

/* Traitement SOULTE */
PROCEDURE TRT-SOULTE :

    type-ech = 2 .

    case type-prg :

        when "1" /* Commande */
        then case date-trf-1 :

            when "*" then date-trf-2 = today .

            otherwise do :

                assign date-trf-2 = ?
                       date-trf-2 = date( int( substr( date-trf-1, 5, 2 ) ),
                                          int( substr( date-trf-1, 7, 2 ) ),
                                          int( substr( date-trf-1, 1, 4 ) ) )
                                          no-error .

                if date-trf-2 = ? then date-trf-2 = today .

            end . /* otherwise do : */

        end case . /* case date-trf-1 : */

        /* Livraison */
        when "2" then assign code-art[ 2 ] = code-art[ 1 ]
                             rowid-cde     = to-rowid( rowid-cde-a )
                             .

    end case . /* case type-prg : */

    /* Mouvements Apport */
    l-code-mvt-2 = entry( 1, l-code-mvt[ 1 ], "/" ) .

    do tour-mvt = 1 to num-entries( l-code-mvt-2 ) :

        code-mvt = entry( tour-mvt, l-code-mvt-2 ) .

        case type-prg :

            /* Commande ou Consultation */
            when "1" or when "3" then do :

                assign codsoc-sav = codsoc-soc
                       codsoc-soc = codsoc-app
                       .

                /* Articles Appros Soulte */
                FOR EACH ARTICS where artics.codsoc = codsoc-art
                                and   artics.theme  = "ART-APPORT-2"
                                and   artics.chrono = 0
                                use-index theme no-lock :                

                    code-art[ 2 ] = artics.articl .

                    do tour-cer = 1 to num-entries( trim( artics.alpha-cle ) ) :

                        code-cer = entry( tour-cer, artics.alpha-cle ) .

                        { c-cereal.i "code-cer" }                

                        do tour-statut = 1 to num-entries( l-statut ) :

                            statut = entry( tour-statut, l-statut ) .

                            /* Mouvements Apport */                 
                            FOR EACH CERMVT where cermvt.codsoc   = codsoc-app
                                            and   cermvt.campagne = code-camp
                                            and   cermvt.typfac   = type-fac
                                            and   cermvt.statut   = statut
                                            and   cermvt.codmvt   = code-mvt
                                            and   cermvt.typaux   = type-aux[ 2 ]
                                            and   cermvt.codaux   = code-aux[ 2 ]
                                            and   cermvt.codcer   = code-cer
                                            use-index codmvt no-lock :

                                FIND CERCTR where cerctr.codsoc  = cermvt.codsoc
                                            and   cerctr.type    = cermvt.typfac
                                            and   cerctr.contrat = cermvt.contrat
                                            no-lock no-error .

                                if not available cerctr or lookup( right-trim( cerctr.typcon ), l-type-con ) = 0 then next . /* Que Contrats Soulte */

                                run trt-apport-soulte .

                            END . /* FOR EACH CERMVT */

                        end . /* do tour-statut = 1 to num-entries( l-statut ) : */

                    end . /* do tour-cer = 1 to num-entries( trim( artics.alpha-cle ) ) : */

                END . /* FOR EACH ARTICS */

                codsoc-soc = codsoc-sav .

            end . /* when "1" or when "3" then do : */

            /* Livraison */
            when "2" then do :

                FOR EACH CERMVT where cermvt.codsoc   = codsoc-apr
                                and   cermvt.typfac   = type-fac
                                and   cermvt.contrat  = no-contrat
                                and   cermvt.campagne = code-camp
                                and   cermvt.codmvt   = code-mvt
                                and   lookup( right-trim( cermvt.statut ), l-statut ) <> 0
                                use-index contrat no-lock :

                    run trt-apport-soulte .

                END .

            end . /* when "2" then do : */

        end case . /* case type-prg : */

    end . /* do tour-mvt = 1 to num-entries( l-code-mvt-2 ) : */

    /* Mouvements Appros */
    FOR EACH T-ECHCER where t-echcer.typech = type-ech
                      use-index i-echcer-1 exclusive-lock :

        /* Tiers Acheteurs */
        FOR EACH T-TIERS use-index i-tiers-1 no-lock :

            /* Commandes initiales non traitees */
            do tour-mvt = 1 to 2 :

                case tour-mvt :
                    when 1 then code-envplat = "" .
                    when 2 then code-envplat = "EDN" .
                end case .

                FOR EACH ENTETE where entete.codsoc   = codsoc-apr
                                and   entete.motcle   = motcle-vente
                                and   entete.env-plat = code-envplat
                                and   entete.typaux   = t-tiers.typaux
                                and   entete.codaux   = t-tiers.codaux
                                and   lookup( right-trim( entete.typcom ), l-code-mvt[ 2 ] ) <> 0
                                use-index entete04 no-lock :

                    FOR EACH LIGNES where lignes.codsoc = entete.codsoc
                                    and   lignes.motcle = entete.motcle
                                    and   lignes.typcom = entete.typcom
                                    and   lignes.numbon = entete.numbon
                                    and   lignes.articl = t-echcer.articl
                                    use-index primaire no-lock :

                        i-paramet = "L" .

                        run trt-appros-soulte .

                    END . /* FOR EACH LIGNES */  

                END . /* FOR EACH ENTETE */

            end . /* do tour-mvt = 1 to 2 : */

            /* Commandes non soldees */
            FOR EACH LIGNES where lignes.codsoc    =  codsoc-apr
                            and   lignes.motcle    =  motcle-vente
                            and   lignes.articl    =  t-echcer.articl
                            and   lignes.nivcom    =  ""
                            and   lignes.top-livre =  ""
                            and   lignes.typaux    =  t-tiers.typaux
                            and   lignes.codaux    =  t-tiers.codaux
                            and   lookup( right-trim( lignes.typcom ), l-code-mvt[ 3 ] ) <> 0
                            and   rowid( lignes )  <> rowid-cde
                            use-index encours no-lock :

                i-paramet = "L" .

                run trt-appros-soulte .

            END . /* FOR EACH LIGNES */  

            /* Livraisons */
            FOR EACH LIGNES where lignes.codsoc =  codsoc-apr
                            and   lignes.articl =  t-echcer.articl
                            and   lignes.datdep >= date-deb-camp
                            and   lignes.datdep <= date-fin-camp
                            and   lignes.nivcom <  "7"
                            and   lignes.typaux =  t-tiers.typaux
                            and   lignes.codaux =  t-tiers.codaux
                            and   lookup( right-trim( lignes.typcom ), l-code-mvt[ 4 ] ) <> 0
                            use-index artdat no-lock :

                i-paramet = "L" .

                run trt-appros-soulte .

            END . /* FOR EACH LIGNES */

            FOR EACH BASTAT where bastat.codsoc =  codsoc-apr
                            and   bastat.articl =  t-echcer.articl
                            and   bastat.datdep >= date-deb-camp
                            and   bastat.datdep <= date-fin-camp
                            and   bastat.typaux =  t-tiers.typaux
                            and   bastat.codaux =  t-tiers.codaux
                            and   lookup( right-trim( bastat.typcom ), l-code-mvt[ 4 ] ) <> 0
                            use-index artdat no-lock :

                i-paramet = "B" .

                run trt-appros-soulte .

            END . /* FOR EACH BASTAT */

        END . /* FOR EACH T-TIERS */

        assign t-echcer.qte-ent[ 2 ] = round( t-echcer.qte-ent[ 1 ] / ( t-echcer.taux-ech / 100 ), 0 ) /* Qte entree droit aliments */
               t-echcer.qte-sor      = round( t-echcer.qte-sor / ( t-echcer.taux-ech / 100 ), 0 )      /* Qte sortie droit aliments */
               t-echcer.qte-sol      = t-echcer.qte-ent[ 2 ] - t-echcer.qte-sor
               .

        if t-echcer.qte-sol <> 0 then ok-qte = yes .

    END . /* FOR EACH T-ECHCER */

END PROCEDURE . /* PROCEDURE TRT-SOULTE */

/* Traitement TAF */
PROCEDURE TRT-TAF :

    assign type-ech = 3
           qte-taf  = 0
           ok-taf   = no
           .

    FOR EACH ECHSEM where echsem.codsoc = codsoc-apr
                    and   echsem.motcle = motcle-taf
                    and   echsem.typaux = type-aux[ 2 ]
                    and   echsem.codaux = code-aux[ 2 ]
                    and   echsem.famart = ""
                    use-index primaire no-lock :

        ok-taf = yes .

        run trt-appros-taf .

    END . /* FOR EACH ECHSEM : */

    /* Cas ou l'enregistrement blanc a ete facture */
    if not ok-taf
    then do :

        date-creation = ? .

        FOR EACH ECHSEM where echsem.codsoc =  codsoc-apr
                        and   echsem.motcle =  motcle-taf
                        and   echsem.typaux =  type-aux[ 2 ]
                        and   echsem.codaux =  code-aux[ 2 ]
                        and   echsem.famart <> ""
                        use-index primaire no-lock
                        by echsem.datcre descending by echsem.famart descending :

            if date-creation = ? or echsem.datcre = date-creation
            then do :
                date-creation = echsem.datcre .
                run trt-appros-taf .
            end .

        END . /* FOR EACH ECHSEM : */

    end . /* if not ok-taf */

    /* Si Commande recup. Taux Incorporation TAF */
    if type-prg = "1"
    then do :

        { cad-arti.i "code-art[ 1 ]" }

        FIND TABGES where tabges.codsoc = codsoc-art
                    and   tabges.etabli = ""
                    and   tabges.typtab = "ART"
                    and   tabges.prefix = "SUIT-FICHE"
                    and   tabges.codtab = code-art[ 1 ]
                    no-lock no-error .

    end .

    /* Creation enreg. table temporaire */
    do ind = 1 to 4 :

        CREATE T-ECHCER .

        assign t-echcer.typech       = type-ech
               t-echcer.libech       = entry( type-ech, l-libech )
               t-echcer.articl       = string( ind )
               t-echcer.libart       = entry( ind, l-libtaf )
               t-echcer.qte-ent[ 1 ] = qte-taf[ ind ]
               .

        if type-prg = "1" and available tabges then t-echcer.taux-ech = tabges.nombre[ 19 ] . /* Taux Incorporation TAF */

        if t-echcer.qte-ent[ 1 ] <> 0 then ok-qte = yes .       

    end . /* do ind = 1 to 4 : */

END PROCEDURE . /* PROCEDURE TRT-TAF */

/* Traitement Apport SOULTE */
PROCEDURE TRT-APPORT-SOULTE :

    /* Lecture table temporaire */
    FIND T-ECHCER where t-echcer.typech  = type-ech
                  and   t-echcer.articl  = code-art[ 2 ]
                  and   t-echcer.contrat = cermvt.contrat
                  exclusive-lock no-error .

    /* Creation enreg. table temporaire */
    if not available t-echcer 
    then do :

        CREATE T-ECHCER .

        assign t-echcer.typech  = type-ech
               t-echcer.libech  = entry( type-ech, l-libech )
               t-echcer.articl  = code-art[ 2 ]
               t-echcer.contrat = cermvt.contrat

               taux-ech         = 0
               prix-ech         = 0
               .

        FIND ARTAPR where artapr.codsoc = codsoc-art
                    and   artapr.articl = code-art[ 2 ]
                    no-lock no-error .

        if available artapr then t-echcer.libart = artapr.libart1[ mem-langue ] .

        /* Recherche Taux et Prix d'echange */         
        FIND CERCTS where cercts.codsoc  = cermvt.codsoc
                    and   cercts.type    = cermvt.typfac
                    and   cercts.contrat = cermvt.contrat
                    and   cercts.theme   = "DIVERS"
                    and   cercts.chrono  = 0
                    no-lock no-error .

        if available cercts
        then do :
            taux-ech = cercts.nombre[ 1 ] .
            if type-prg = "1" and cerctr.typcon = entry( 1, l-type-con ) then prix-ech = cercts.nombre[ 2 ] . /* Type Contrat "SOF" */
        end .

        if taux-ech = 0
        then do :

            FIND CEREAS where cereas.codsoc   = codsoc-cer
                        and   cereas.campagne = cermvt.campagne
                        and   cereas.region   = cermvt.region
                        and   cereas.codcer   = cermvt.codcer
                        and   cereas.theme    = "DIVERS"
                        and   cereas.chrono   = 0
                        no-lock no-error .

            if available cereas then taux-ech = cereas.nombre[ 2 ] .

        end .                                

        if taux-ech = 0 then taux-ech = 100 .

        if type-prg = "1" and cerctr.typcon <> entry( 1, l-type-con ) /* Type Contrat <> "SOF" */
        then do :

            FIND FIRST {tarcli.i} where {tarcli.i}.motcle     =  motcle-vente
                                  and   {tarcli.i}.codsoc     =  codsoc-apr
                                  and   {tarcli.i}.typaux     =  ""
                                  and   {tarcli.i}.codaux     =  ""
                                  and   {tarcli.i}.articl     =  code-art[ 2 ]
                                  and   {tarcli.i}.code-tarif =  code-trf
                                  and   {tarcli.i}.date-debut <= date-trf-2
                                  and ( {tarcli.i}.date-fin   >= today or
                                        {tarcli.i}.date-fin   = ? )
                                  use-index primaire no-lock no-error .

            if available {tarcli.i} then prix-ech = {tarcli.i}.valeur .                

        end . /* if cerctr.typcon <> entry( 1, l-type-con ) */

        t-echcer.taux-ech = taux-ech .

        if type-prg = "1" then assign t-echcer.typcon   = caps( cerctr.typcon )
                                      t-echcer.datcon   = cerctr.datdep
                                      t-echcer.prix-ech = prix-ech
                                      .

        { l-cereal.i "codsoc-cer" "cermvt.campagne" "cermvt.region" "cermvt.codcer" "libel" }

    end . /* if not available t-echcer */

    if available cereal
    then do :
        { calpdsno.i }
        t-echcer.qte-ent[ 1 ] = t-echcer.qte-ent[ 1 ] + ( pds-norm * coef-qte ) . /* Qte entree */
    end . 

END PROCEDURE . /* PROCEDURE TRT-APPORT-SOULTE */

/* Traitement Appros REMERE */
PROCEDURE TRT-APPROS-REMERE :

    case i-paramet :

        when "L" then do :

            case lignes.nivcom :
                when "" then qte-lig = - ( lignes.qte[ 1 ] - lignes.qte[ 2 ] ) . /* Commande  */
                otherwise    qte-lig = - lignes.qte[ 2 ] .                       /* Livraison */
            end case .

            if lignes.coef-cde < 0
               then qte-lig = - ( qte-lig / lignes.coef-cde ) .
               else if lignes.coef-cde > 0
                       then qte-lig = qte-lig * lignes.coef-cde .


        end .

        when "B" then qte-lig = - bastat.qte . /* Livraison */

    end case .

    t-echcer.qte-sor = t-echcer.qte-sor + qte-lig .

END PROCEDURE . /* PROCEDURE TRT-APPROS-REMERE */

/* Traitement Appros SOULTE */
PROCEDURE TRT-APPROS-SOULTE :

    case i-paramet :

        when "L" then FIND BONLGS where bonlgs.codsoc     = lignes.codsoc
                                  and   bonlgs.motcle     = lignes.motcle
                                  and   bonlgs.typcom     = lignes.typcom
                                  and   bonlgs.numbon     = lignes.numbon
                                  and   bonlgs.chrono     = lignes.chrono
                                  and   bonlgs.theme      = "CONTRAT-APPORT"
                                  and   bonlgs.ss-chrono = 0
                                  no-lock no-error .

        when "B" then FIND BONLGS where bonlgs.codsoc     = bastat.codsoc
                                  and   bonlgs.motcle     = bastat.motcle
                                  and   bonlgs.typcom     = bastat.typcom
                                  and   bonlgs.numbon     = bastat.numbon
                                  and   bonlgs.chrono     = bastat.chrono
                                  and   bonlgs.theme      = "CONTRAT-APPORT"
                                  and   bonlgs.ss-chrono = 0
                                  no-lock no-error .

    end case .

    if not available bonlgs or bonlgs.alpha-cle <> t-echcer.contrat then return . /* Ligne non rattachee a un Contrat Soulte Apport */

    case i-paramet :

        when "L" then do :

            case lignes.nivcom :
                when "" then qte-lig = - ( lignes.qte[ 1 ] - lignes.qte[ 2 ] ) . /* Commande  */
                otherwise    qte-lig = - lignes.qte[ 2 ] .                       /* Livraison */
            end case .

            if lignes.coef-cde < 0
               then qte-lig = - ( qte-lig / lignes.coef-cde ) .
               else if lignes.coef-cde > 0
                       then qte-lig = qte-lig * lignes.coef-cde .

        end .

        when "B" then qte-lig = - bastat.qte . /* Livraison */

    end case .

    t-echcer.qte-sor = t-echcer.qte-sor + qte-lig .

END PROCEDURE . /* PROCEDURE TRT-APPROS-SOULTE */

/* Traitement Appros TAF */
PROCEDURE TRT-APPROS-TAF :

    if lookup( echsem.soufam , l-cer-ble ) <> 0
       then qte-taf[ 1 ] = qte-taf[ 1 ] + echsem.poids-apporte .
       else if lookup( echsem.soufam , l-cer-orge) <> 0
               then qte-taf[ 2 ] = qte-taf[ 2 ] + echsem.poids-apporte .
               else if lookup( echsem.soufam , l-cer-trit ) <> 0
                       then qte-taf[ 3 ] = qte-taf[ 3 ] + echsem.poids-apporte .
                       else if lookup( echsem.soufam , l-cer-mais ) <> 0                     
                               then qte-taf[ 4 ] = qte-taf[ 4 ] + echsem.poids-apporte .

END PROCEDURE . /* TRT-APPROS-TAF */






















