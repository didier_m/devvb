/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/rfc-capq.p                                                                     !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End                                                                                                          
**************************************************************************************************************/
/*==========================================================================*/
/*                          R F C - C A P Q . P                             */
/*       GESTION des RISTOURNES    : MAJ DU CA ET MT RISTOURNES EN CAPITAL  */
/*       Gestion de l'ecran                                                 */
/*==========================================================================*/
 
{ connect.i }
{ aide.i    }
{ natabsoc.i  }
{ compo.f "new" }
{ chx-opt.f }                         /* zones du module acceptation      */
 
{ rfc-cap0.i }
 
/* chargement societe commune */
regs-app = "nathalie" .
{ regs-cha.i }
 
assign date-debex = b-tabges.dattab[4]
       date-finex = b-tabges.dattab[5]
       .
 
/* Affichage des frames */
display libelle[ 11 for 2 ]  with frame fr-saisie .
 
assign compo-vecteur = "<01>,1,2"
       compo-bloque  = no .
 
/*========== DEBUT du Traitement =========================================*/
SAISIE-GENERALE :
REPEAT :
 
    SAISIE-1 :
    REPEAT :
 
        { compo.i "-1" }
 
 
        /*  1 : Date de debut d'exercice */
        REPEAT WHILE COMPO-MOTCLE = 1  WITH FRAME FR-SAISIE :
 
            { fncompo.i  date-debex  fr-saisie  date-debex   }
            if keyfunction( lastkey ) = "end-error" then leave saisie-generale .
 
            leave .
 
        END .  /*  REPEAT : 1  */
 
        /*  2 : Date de fin   d'exercice */
        REPEAT WHILE COMPO-MOTCLE = 2  WITH FRAME FR-SAISIE :
 
 
            { fncompo.i  date-finex  fr-saisie  date-finex   }
            if keyfunction( lastkey ) = "end-error" then leave saisie-generale .
 
            if date-finex < date-debex
            then do :
                message libelle[ 21 ] . bell . bell .
                pause . next .
            end .
 
            leave .
 
        END .  /*  REPEAT : 1  */
 
    END .     /*   SAISIE-1  */
 
    leave .
 
END .     /*   SAISIE-GENERALE  */  
 
 
 