/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/cafamp.i                                                                       !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End                                                                                                          
**************************************************************************************************************/
/* DGR 11122012 : formattage tel portable avec + 33                           */
/* DGR 03012013 : .csv au lieu de .operat                                     */
/* DGR 10012013 : ajout siret + num certiphyto + date                         */
/* DGR 18022013 : correctiion pb si annee deb = annee fin                     */

PROCEDURE cafam:

def var wtypo       as char                      no-undo.
def var l-typo       as char                      no-undo.
def var l-libtypo    as char                      no-undo.
    
DEF VAR ind   AS INTEGER .
DEF VAR wcap  AS dec.
DEF VAR soc   AS CHAR .
DEF VAR auxisoc   AS CHAR .

def var wlibfic as char.

def var l-repres as char .
def var l-librepres as char.

Def var topTC   as log.

def var wcert-phyto as char no-undo.
def var dcert-phyto as date no-undo.
 
def var wlibcom as char.
def var l-fam as char initial  "001,002,003,004,005,006,007,008,009,010,011,013,014,012,030,032,021,022,015,052,080".

DEF VAR cafN          AS decimal   FORMAT "->>>>>>>9.99" extent 25.
DEF VAR cafN1         AS decimal   FORMAT "->>>>>>>9.99" extent 25.
DEF VAR qteN          AS decimal   FORMAT "->>>>>>>9.99" extent 25.
DEF VAR qteN1         AS decimal   FORMAT "->>>>>>>9.99" extent 25.
def var wtotcli       as dec.

DEF VAR i              AS integer.
DEF var j              as integer.

DEF VAR wcpt           AS integer.
DEF VAR debut          AS integer.
DEF VAR fin            AS integer.
def var fichsor        as char format "x(12)".
def var fichsor2        as char format "x(12)".

def var wprefix as char.


def var wcptinc as int.
def var winsee  as char format "xxxxx".
def var wlibin  as char format "x(32)".
def var wcodpo  as char .
def var wpor    as char no-undo.
def var x       as char no-undo.

def var sav-codsoc-soc as char.

def buffer b-auxapr for auxapr.

def var toperreur as log.

toperreur = no.

if wanneedeb-1 > wanneefin-1 or 
  wanneedeb-1 + wmoisdeb-1  > wanneefin-1 + wmoisfin-1  
  then do:
  message "INCOHERENCE date ANNEE-1".
  toperreur = yes.  
end.

if wanneedeb  > wanneefin or 
  wanneedeb + wmoisdeb > wanneefin + wmoisfin
  then do:
  message "INCOHERENCE date ANNEE N".
  toperreur = yes.  
end.  

if toperreur = yes then do:
  message "SORTIE ANORMALE DU PROGRAMME".
  return.  
end.
for each tabges
where tabges.codsoc = ""
and   tabges.etabli = ""
and   tabges.typtab = "CLI"
and   tabges.prefix = "REPRES"
no-lock:
  if l-repres = "" then
    assign l-repres = trim(tabges.codtab)
           l-librepres = upper(trim(tabges.libel1[1])).
  else
    assign l-repres = l-repres + "," + trim(tabges.codtab)
           l-librepres = l-librepres + "," + upper(trim(tabges.libel1[1])).
end.

assign l-typo = ""
       l-libtypo = "".
           
for each tabges
where tabges.codsoc = ""
and   tabges.etabli = ""
and   tabges.typtab = "CLI"
and   tabges.prefix = "TYP-SEGCRM"
no-lock:
  if l-typo = "" then
    assign l-typo = trim(tabges.codtab)
           l-libtypo = upper(trim(tabges.libel1[1])).
  else
    assign l-typo = l-typo + "," + trim(tabges.codtab)
           l-libtypo = l-libtypo + "," + upper(trim(tabges.libel1[1])).
                                                          
  end.
wprefix = "/applic/partageweb/xcspool/" + trim(operat) + "/" .
if topcar = "OUI" then 
  assign wprefix = "/applic/li/" + trim(lower(operat)) + "/" .

   
  DO ind = 1 TO NUM-ENTRIES(l-soc):
    codsoc-soc = entry(ind,l-soc).
    if codsoc-soc  = "" then next.

    regs-app = "ELODIE" .
    { regs-cha.i }
        
    regs-fileacc = "AUXILI" + "TIE".
    { regs-rec.i }
        
    auxisoc = regs-soc.
    message codsoc-soc auxisoc view-as alert-box.


    /*-------------------------------*/
    /*  Initialisation des variables */
    /*-------------------------------*/
    if wtypfic = "1" then 
      fichsor = wprefix +
                "cafam-comp."  + 
                string(wanneedeb-1,"XXXX") + string(wmoisdeb-1,"XX") +  
                string(wanneefin-1,"XXXX") + string(wmoisfin-1,"XX") +  "-" +
                string(wanneedeb,"XXXX") + string(wmoisdeb,"XX") +  
                string(wanneefin,"XXXX") + string(wmoisfin,"XX") +  "." +
                string(today,"999999") + "." + trim(codsoc-soc) + ".csv".
    else if wtypfic = "2" then 
      fichsor = wprefix + 
                "cafam-red."  + 
                string(wanneedeb,"XXXX") + string(wmoisdeb,"XX") +  
                string(wanneefin,"XXXX") + string(wmoisfin,"XX") +  "." +
                string(today,"999999") + "." + trim(codsoc-soc) + ".csv".
      else 
        assign  
          fichsor = wprefix + 
                "cafam-comp."  + 
                string(wanneedeb-1,"XXXX") + string(wmoisdeb-1,"XX") +  
                string(wanneefin-1,"XXXX") + string(wmoisfin-1,"XX") +  "-" +
                string(wanneedeb,"XXXX") + string(wmoisdeb,"XX") +  
                string(wanneefin,"XXXX") + string(wmoisfin,"XX") +  "." +
                string(today,"999999") + "." + trim(codsoc-soc) + ".csv".
           
          fichsor2 = wprefix + 
                "cafam-red."  + 
                string(wanneedeb,"XXXX") + string(wmoisdeb,"XX") +  
                string(wanneefin,"XXXX") + string(wmoisfin,"XX") +  "." +
                string(today,"999999") + "." + trim(codsoc-soc) + ".csv".


    OUTPUT STREAM edition TO value(fichsor).
    if wtypfic = "3" then output stream edition2 to value(fichsor2).
    
    message "debut de traitement � : "  string(time,"hh:mm:ss") "societe " codsoc-soc.
    pause 5.

    if wtypfic = "1" then 
      export STREAM edition delimiter ";"
        "Societe" "Code" "adresse 1" "adresse 2" "adresse 3" "adresse 4"
        "adresse 5" "Commune INSEE" "Magasin" "Type adh" 
        "teleph"  "fax" "portable" "mail" 
        "ATC1" "ATC2" "ATC3" "Typologie Client"
        "lib ABR" "Mairie" 

       "Ca N Fam 1"  "Ca N Fam 2"  "Ca N Fam 3"  "Ca N Fam 4" "Ca N Fam 5" 
       "Ca N Fam 6"  "Ca N Fam 7"  "Ca N Fam 8"  "Ca N Fam 9" "Ca N Fam 10" 
       "TOTAL N GP"
       "Ca N Fam 11" "Ca N Fam 13" "Ca N Fam 14" "TOTAL CA PV N"  
       "Ca N Fam 12" "Ca N Fam 30" "Ca N Fam 32" "TOTAL CA PA N"  
       "CA N Fam 21" "CA N Fam 22" "TOTAL CA EQ N"
       "CA N Fam 015"
       "CA N Fam 52" "CA N Fam 80" 
       "TOTAL N PA+PV+EQ+MAT+PREST+COLL"
    
       "Ca N-1 Fam 1"  "Ca N-1 Fam 2"  "Ca N-1 Fam 3"  "Ca N-1 Fam 4" "Ca N-1 Fam 5" 
       "Ca N-1 Fam 6"  "Ca N-1 Fam 7"  "Ca N-1 Fam 8"  "Ca N-1 Fam 9" "Ca N-1 Fam 10" 
       "TOTAL N-1 GP"
       "Ca N-1 Fam 11" "Ca N-1 Fam 13" "Ca N-1 Fam 14" "TOTAL CA PV N-1"  
       "Ca N-1 Fam 12" "Ca N-1 Fam 30" "Ca N-1 Fam 32" "TOTAL CA PA N-1" 
       "CA N-1 Fam 21" "CA N-1 Fam 22" "TOTAL CA EQ N-1"
       "CA N-1 Fam 015"
       "CA N-1 Fam 52" "CA N-1 Fam 80" 

       "TOTAL N-1 PA+PV+EQ+MAT+PREST+COLL"

       "Qte N Fam 1"  "Qte N Fam 2"  "Qte N Fam 3"  "Qte N Fam 4" "Qte N Fam 5" 
       "Qte N Fam 6"  "Qte N Fam 7"  "Qte N Fam 8"  "Qte N Fam 9" "Qte N Fam 10" 
       "TOTAL N GP"
 
       "Qte N Fam 11" "Qte N Fam 13" "Qte N Fam 14" "TOTAL Qte PV N"  
       "Qte N Fam 12" "Qte N Fam 30" "Qte N Fam 32" "TOTAL Qte PA N"  
       "Qte N Fam 21" "Qte N Fam 22" "TOTAL Qte EQ N"
       "Qte N Fam 15" 
       "Qte N Fam 52" "Qte N Fam 80" 
       "TOTAL N PA+PV+EQ+MAT+PREST+ COLL"
 
       "Qte N-1 Fam 1"  "Qte N-1 Fam 2"  "Qte N-1 Fam 3"  "Qte N-1 Fam 4" "Qte N-1 Fam 5" 
       "Qte N-1 Fam 6"  "Qte N-1 Fam 7"  "Qte N-1 Fam 8"  "Qte N-1 Fam 9" "Qte N-1 Fam 10" 
       "TOTAL N-1 GP"
       "Qte N-1 Fam 11" "Qte N-1 Fam 13" "Qte N-1 Fam 14" "TOTAL Qte PV N-1"  
       "Qte N-1 Fam 12" "Qte N-1 Fam 30" "Qte N-1 Fam 32" "TOTAL Qte PA N-1" 
       "Qte N-1 Fam 21" "Qte N-1 Fam 22" "TOTAL Qte EQ N-1"
       "Qte N-1 Fam 15" 
       "Qte N-1 Fam 52" "Qte N-1 Fam 80" 
       "TOTAL N-1 PA+PV+EQ+MAT+PREST+ COLL"
       "CAP"
       "SIRET" "Certificat PHYTO" "Date VALIDITE".
    else if wtypfic = "2" then 
      export STREAM edition delimiter ";"
        "Societe" "Code" "libabr" "adresse 1" 
        "adresse 5" "Magasin" 
        "ATC1" "ATC2" "ATC3" "Typologie Client"

        "Ca N Fam 11" "Ca N Fam 13" "Ca N Fam 14" "TOTAL CA PV N"  
        "Ca N Fam 12" "Ca N Fam 30" "Ca N Fam 32" "TOTAL CA PA N"  
        "CA N Fam 21" "CA N Fam 22" "TOTAL CA EQ N"
        "TOTAL N PA+PV+EQ"
 
        "Qte N Fam 12" "Qte N Fam 30" "Qte N Fam 32" "TOTAL Qte PA N"  
        .

     else do: /* wtypfic = 3 */
      export STREAM edition delimiter ";"
        "Societe" "Code" "adresse 1" "adresse 2" "adresse 3" "adresse 4"
        "adresse 5" "Commune INSEE" "Magasin" "Type adh" 
        "teleph"  "fax" "portable" "mail"
        "ATC1" "ATC2" "ATC3" "Typologie Client"
        "lib ABR" "Mairie" 

       "Ca N Fam 1"  "Ca N Fam 2"  "Ca N Fam 3"  "Ca N Fam 4" "Ca N Fam 5" 
       "Ca N Fam 6"  "Ca N Fam 7"  "Ca N Fam 8"  "Ca N Fam 9" "Ca N Fam 10" 
       "TOTAL N GP"
       "Ca N Fam 11" "Ca N Fam 13" "Ca N Fam 14" "TOTAL CA PV N"  
       "Ca N Fam 12" "Ca N Fam 30" "Ca N Fam 32" "TOTAL CA PA N"  
       "CA N Fam 21" "CA N Fam 22" "TOTAL CA EQ N"
       "CA N Fam 015"
       "CA N Fam 52" "CA N Fam 80" 
       "TOTAL N PA+PV+EQ+MAT+PREST+COLL"
    
       "Ca N-1 Fam 1"  "Ca N-1 Fam 2"  "Ca N-1 Fam 3"  "Ca N-1 Fam 4" "Ca N-1 Fam 5" 
       "Ca N-1 Fam 6"  "Ca N-1 Fam 7"  "Ca N-1 Fam 8"  "Ca N-1 Fam 9" "Ca N-1 Fam 10" 
       "TOTAL N-1 GP"
       "Ca N-1 Fam 11" "Ca N-1 Fam 13" "Ca N-1 Fam 14" "TOTAL CA PV N-1"  
       "Ca N-1 Fam 12" "Ca N-1 Fam 30" "Ca N-1 Fam 32" "TOTAL CA PA N-1" 
       "CA N-1 Fam 21" "CA N-1 Fam 22" "TOTAL CA EQ N-1"
       "CA N-1 Fam 015"
       "CA N-1 Fam 52" "CA N-1 Fam 80" 

       "TOTAL N-1 PA+PV+EQ+MAT+PREST+COLL"

       "Qte N Fam 1"  "Qte N Fam 2"  "Qte N Fam 3"  "Qte N Fam 4" "Qte N Fam 5" 
       "Qte N Fam 6"  "Qte N Fam 7"  "Qte N Fam 8"  "Qte N Fam 9" "Qte N Fam 10" 
       "TOTAL N GP"
 
       "Qte N Fam 11" "Qte N Fam 13" "Qte N Fam 14" "TOTAL Qte PV N"  
       "Qte N Fam 12" "Qte N Fam 30" "Qte N Fam 32" "TOTAL Qte PA N"  
       "Qte N Fam 21" "Qte N Fam 22" "TOTAL Qte EQ N"
       "Qte N Fam 15" 
       "Qte N Fam 52" "Qte N Fam 80" 
       "TOTAL N PA+PV+EQ+MAT+PREST+ COLL"
 
       "Qte N-1 Fam 1"  "Qte N-1 Fam 2"  "Qte N-1 Fam 3"  "Qte N-1 Fam 4" "Qte N-1 Fam 5" 
       "Qte N-1 Fam 6"  "Qte N-1 Fam 7"  "Qte N-1 Fam 8"  "Qte N-1 Fam 9" "Qte N-1 Fam 10" 
       "TOTAL N-1 GP"
       "Qte N-1 Fam 11" "Qte N-1 Fam 13" "Qte N-1 Fam 14" "TOTAL Qte PV N-1"  
       "Qte N-1 Fam 12" "Qte N-1 Fam 30" "Qte N-1 Fam 32" "TOTAL Qte PA N-1" 
       "Qte N-1 Fam 21" "Qte N-1 Fam 22" "TOTAL Qte EQ N-1"
       "Qte N-1 Fam 15" 
       "Qte N-1 Fam 52" "Qte N-1 Fam 80" 
       "TOTAL N-1 PA+PV+EQ+MAT+PREST+ COLL"
       "CAP"
       "SIRET" "Certificat PHYTO" "Date VALIDITE".

      export STREAM edition2 delimiter ";"
        "Societe" "Code" "libabr" "adresse 1" 
        "adresse 5" "Magasin" 
        "ATC1" "ATC2" "ATC3" "Typologie Client"

        "Ca N Fam 11" "Ca N Fam 13" "Ca N Fam 14" "TOTAL CA PV N"  
        "Ca N Fam 12" "Ca N Fam 30" "Ca N Fam 32" "TOTAL CA PA N"  
        "CA N Fam 21" "CA N Fam 22" "TOTAL CA EQ N"
        "TOTAL N PA+PV+EQ"
 
        "Qte N Fam 12" "Qte N Fam 30" "Qte N Fam 32" "TOTAL Qte PA N"  
        .
     end.
     
    /*------------------------------------------------*/
    /* Recherche dans stacli CAHT sur annee 2001-2002 */
    /*------------------------------------------------*/
    rech-auxapr:
    for each b-auxapr
    where b-auxapr.codsoc = codsoc-soc
    /*
    
    and   b-auxapr.type-adh <> "9"  /* usager divers magasin */
    */
    no-lock:
      assign wtypo = ""
             wpor  = "".

      wtypo = (if   lookup(trim(b-auxapr.code-edition[4]), l-typo)  = 0 then "  NON RENSEIGNE"
               else trim(b-auxapr.code-edition[4]) + "-" + entry(lookup(b-auxapr.code-edition[4], l-typo),l-libtypo) ).
                                                

      if wtypfic <> "2" then do:
        assign wcert-phyto = ""
               dcert-phyto = ?.
               
        find auxgss
        where auxgss.codsoc = b-auxapr.codsoc
        and   auxgss.typaux = b-auxapr.typaux
        and   auxgss.codaux = b-auxapr.codaux
        and   auxgss.theme  = "DIVERS"
        and   auxgss.chrono = 0
        no-lock no-error.
        if available auxgss then
          assign  wcert-phyto = auxgss.alpha[8]
                  dcert-phyto = auxgss.datte[3].

      end.

      assign topTC = NO.
      do i = 1 to 3:
        if lookup(trim(repres[i]) ,",998,999") = 0 then 
          topTC = yes.
         
      end.
      wlibcom = "".
      find tabdos where
      tabdos.codsoc = " "
      and tabdos.etabli = " "
      and tabdos.typtab = "CER"
      and tabdos.prefix = "lieu-onic"
      and tabdos.codtab = substr(b-auxapr.onic,1,6)
      no-lock no-error.
      if available tabdos then wlibcom = tabdos.libel1[1].
      
      
      FIND auxili WHERE auxili.codsoc = auxisoc
      AND  auxili.typaux = b-auxapr.typaux
      AND  auxili.codaux = b-auxapr.codaux
      NO-LOCK NO-ERROR.
      if not available auxili then do: 
             /*
             message soc b-auxapr.typaux b-auxapr.codaux
             view-as alert-box.
             */
             next rech-auxapr.
      end.


      assign wpor = trim(b-auxapr.telex).
      
      do j = 1 to length(wpor):
        x = substr(wpor,j,1).
        if asc(x) < 48 or asc(x) > 57 then overlay(wpor,J,1) = " ".
      end.
    
      wpor = replace(wpor," ","").
      if wpor <> "" then do:
        wpor = string(wpor,"9999999999") no-error.      
        wpor = "+33" + substr(wpor,2,9).
      end.      
      assign cafN       = 0 cafN1      = 0  
             qteN       = 0 qteN1      = 0  
             wtotcli = 0 wcap = 0.

      if codsoc-soc = "01" then do:

        find capsoci
        where capsoci.codsoc = b-auxapr.codsoc
        and   capsoci.codetb = "001"
        and   capsoci.typaux = b-auxapr.typaux
        and   capsoci.codaux = b-auxapr.codaux
        no-lock no-error.
        if available capsoci then
          wcap =capsoci.cap-souscrit.
      end.
      

      FOR EACH stacli use-index primaire
      WHERE stacli.codsoc = codsoc-soc
      AND stacli.motcle = "VTE"
      and  stacli.typaux = b-auxapr.typaux
      and  stacli.codaux = b-auxapr.codaux
      and  lookup(stacli.famart , l-fam ) <> 0
/*
      AND stacli.annee >= "2009" 
      AND stacli.annee <= "2011" 
*/
      and ( (wtypfic = "1" or wtypfic = "3" and (stacli.annee >= wanneedeb-1 AND stacli.annee <= wanneefin ) ) or
            (wtypfic = "2" and (stacli.annee >= wanneedeb  AND stacli.annee <= wanneefin ) ) )
      no-lock:

        /* n-1 */
        if wtypfic = "1" or wtypfic = "3" then do:
        
          if stacli.annee >= wanneedeb-1 and stacli.annee <= wanneefin-1 then do :
            if  wanneedeb-1 =  wanneefin-1 then do:
              do i = int(wmoisdeb-1) to int(wmoisfin-1) :
                wtotcli = wtotcli + fac-ht[I].               
                cafN1[lookup(stacli.famart,l-fam)] = cafN1[lookup(stacli.famart,l-fam)] + fac-ht[I] .
                qteN1[lookup(stacli.famart,l-fam)] = qteN1[lookup(stacli.famart,l-fam)] + qte[I] .
              end.
            end.
            else do:
              if stacli.annee = wanneedeb-1 then do :
                do i = int(wmoisdeb-1) to 12:
                  wtotcli = wtotcli + fac-ht[I].               
                  cafN1[lookup(stacli.famart,l-fam)] = cafN1[lookup(stacli.famart,l-fam)] + fac-ht[I] .
                  qteN1[lookup(stacli.famart,l-fam)] = qteN1[lookup(stacli.famart,l-fam)] + qte[I] .
                end.
              end.
              if stacli.annee = wanneefin-1 then do :
                do i = 1 to int(wmoisfin-1) :
                  wtotcli = wtotcli + fac-ht[I].               
                  cafN1[lookup(stacli.famart,l-fam)] = cafN1[lookup(stacli.famart,l-fam)] + fac-ht[I] .
                  qteN1[lookup(stacli.famart,l-fam)] = qteN1[lookup(stacli.famart,l-fam)] + qte[I] .
                end.
              end.
            end.  
          end.
        end.
        /* n */
        if stacli.annee >= wanneedeb and stacli.annee <= wanneefin then do :
          if  wanneedeb =  wanneefin then do:
            do i = int(wmoisdeb) to int(wmoisfin) :
                wtotcli = wtotcli + fac-ht[I].               
                cafN[lookup(stacli.famart,l-fam)] = cafN[lookup(stacli.famart,l-fam)] + fac-ht[I] .
                qteN[lookup(stacli.famart,l-fam)] = qteN[lookup(stacli.famart,l-fam)] + qte[I] .
            end.
          end.
          else do:
            if stacli.annee = wanneedeb then do :
              do i = int(wmoisdeb) to 12:
                wtotcli = wtotcli + fac-ht[I].               
                cafN[lookup(stacli.famart,l-fam)] = cafN[lookup(stacli.famart,l-fam)] + fac-ht[I] .
                qteN[lookup(stacli.famart,l-fam)] = qteN[lookup(stacli.famart,l-fam)] + qte[I] .
              end.
            end.
            if stacli.annee = wanneefin then do :
              do i = 1 to int(wmoisfin) :
                wtotcli = wtotcli + fac-ht[I].               
                cafN[lookup(stacli.famart,l-fam)] = cafN[lookup(stacli.famart,l-fam)] + fac-ht[I] .
                qteN[lookup(stacli.famart,l-fam)] = qteN[lookup(stacli.famart,l-fam)] + qte[I] .
              end.
            end.
          end.
        end.
      end.

      /*----------------------------------------------------------------------*/
      /*  Creation enregistrement dans le fichie export si ca ht > 0 E        */
      /*  ou CA = 0 et au moisn UN TC <> ("",998,999") => topTC = yes         */
      /*----------------------------------------------------------------------*/

      if ( wtotcli <>  0  or
           (wtotcli = 0 and topTC = yes)) then  do:  


        if wtypfic = "1" then 
         export STREAM edition delimiter ";"
              codsoc-soc auxili.codaux auxili.adres[1] auxili.adres[2]
              auxili.adres[3] auxili.adres[4] trim(auxili.adres[5])
              trim(substr(b-auxapr.onic,1,6) + " " + wlibcom)
              b-auxapr.magasin b-auxapr.type-adh b-auxapr.teleph b-auxapr.fax 
              wpor
              b-auxapr.alpha-5 

             
             if lookup(trim(b-auxapr.repres[1]),l-repres) = 0 then ""
             else trim(b-auxapr.repres[1]) + " " + entry(lookup(b-auxapr.repres[1], l-repres),l-librepres)
             if lookup(trim(b-auxapr.repres[2]),l-repres) = 0 then ""
             else trim(b-auxapr.repres[2]) + " " + entry(lookup(b-auxapr.repres[2], l-repres),l-librepres) 
             if lookup(trim(b-auxapr.repres[3]),l-repres) = 0 then ""
                       else trim(b-auxapr.repres[3]) + " " + entry(lookup(b-auxapr.repres[3], l-repres),l-librepres) 
         
              wtypo   
              auxili.libabr
              b-auxapr.code-edition[2]
              cafN [1 for 10]
              cafN[1] + cafN[2] + cafn[3] + cafN[4] + cafN[5] + cafN[6] + cafN[7] + cafn[8] + cafN[9] + cafN[10] 
              cafN[11 for 3] 
              cafN[11] + cafN[12] +  cafN[13] 
              cafN[14 for 3] 
              cafN[14] + cafN[15] +  cafN[16] 
              cafN[17 for 2] 
              cafN[17] + cafN[18] 
              cafN[19 for 3]
              cafN[11] + cafN[12] + cafN[13] + cafN[14] + cafN[15] + cafN[16] + cafN[17] + cafN[18] + cafN[19] + 
              cafN[20] + cafN[21] 
              cafN1 [1 for 10]
              cafN1[1] + cafN1[2] + cafN1[3] + cafN1[4] + cafN1[5] + cafN1[6] + cafN1[7] + cafN1[8] + cafN1[9] + cafN1[10] 
              cafN1[11 for 3] 
              cafN1[11] + cafN1[12] +  cafN1[13] 
              cafN1[14 for 3] 
              cafN1[14] + cafN1[15] +  cafN1[16] 
              cafN1[17 for 2] 
              cafN1[17] + cafN1[18] 
              cafN1[19 for 3]
              cafN1[11] + cafN1[12] + cafN1[13] + cafN1[14] + cafN1[15] + cafN1[16] + cafN1[17] + cafN1[18] + cafN1[19] + 
              cafN1[20] + cafN1[21] 

              QteN [1 for 10]
              QteN[1] + QteN[2] + Qten[3] + QteN[4] + QteN[5] + QteN[6] + QteN[7] + Qten[8] + QteN[9] + QteN[10] 
              QteN[11 for 3] 
              QteN[11] + QteN[12] +  QteN[13] 
              QteN[14 for 3] 
              QteN[14] + QteN[15] +  QteN[16] 
              QteN[17 for 2] 
              QteN[17] + QteN[18] 
              QteN[19 for 3]
              QteN[11] + QteN[12] + QteN[13] + QteN[14] + QteN[15] + QteN[16] + QteN[17] + QteN[18] + QteN[19] + 
              QteN[20] + QteN[21] 

              QteN1 [1 for 10]
              QteN1[1] + QteN1[2] + QteN1[3] + QteN1[4] + QteN1[5] + QteN1[6] + QteN1[7] + QteN1[8] + QteN1[9] + QteN1[10] 
              QteN1[11 for 3] 
              QteN1[11] + QteN1[12] +  QteN1[13] 
              QteN1[14 for 3] 
              QteN1[14] + QteN1[15] +  QteN1[16] 
              QteN1[17 for 2] 
              QteN1[17] + QteN1[18] 
              QteN1[19 for 3]
              QteN1[11] + QteN1[12] + QteN1[13] + QteN1[14] + QteN1[15] + QteN1[16] + QteN1[17] + QteN1[18] + QteN1[19] + 
              QteN1[20] + QteN1[21] 
              wcap
              b-auxapr.siret 
              wcert-phyto
              dcert-phyto
              .

       else if wtypfic = "2" then
         export STREAM edition delimiter ";"
              codsoc-soc auxili.codaux auxili.libabr 
              auxili.adres[1] trim(auxili.adres[5])
              b-auxapr.magasin

              if lookup(trim(b-auxapr.repres[1]),l-repres) = 0 then ""
              else trim(b-auxapr.repres[1]) + " " + entry(lookup(b-auxapr.repres[1], l-repres),l-librepres)
              if lookup(trim(b-auxapr.repres[2]),l-repres) = 0 then ""
              else trim(b-auxapr.repres[2]) + " " + entry(lookup(b-auxapr.repres[2], l-repres),l-librepres) 
              if lookup(trim(b-auxapr.repres[3]),l-repres) = 0 then ""
              else trim(b-auxapr.repres[3]) + " " + entry(lookup(b-auxapr.repres[3], l-repres),l-librepres) 
              wtypo   
              cafN[11 for 3] 
              cafN[11] + cafN[12] +  cafN[13] 

              cafN[14 for 3] 
              cafN[14] + cafN[15] +  cafN[16] 

              cafN[17 for 2] 
              cafN[17] + cafN[18] 

              cafN[11] + cafN[12] + cafN[13] + cafN[14] + cafN[15] + cafN[16] + cafN[17] + cafN[18] 
              
              QteN[14 for 3] 
              QteN[14] + QteN[15] +  QteN[16]  .

         else do:
           export STREAM edition delimiter ";"
                codsoc-soc auxili.codaux auxili.adres[1] auxili.adres[2]
                auxili.adres[3] auxili.adres[4] trim(auxili.adres[5])
                trim(substr(b-auxapr.onic,1,6) + " " + wlibcom)
                b-auxapr.magasin b-auxapr.type-adh b-auxapr.teleph b-auxapr.fax 
                wpor
                b-auxapr.alpha-5 


                if lookup(trim(b-auxapr.repres[1]),l-repres) = 0 then ""
                else trim(b-auxapr.repres[1]) + " " + entry(lookup(b-auxapr.repres[1], l-repres),l-librepres)
                if lookup(trim(b-auxapr.repres[2]),l-repres) = 0 then ""
                else trim(b-auxapr.repres[2]) + " " + entry(lookup(b-auxapr.repres[2], l-repres),l-librepres) 
                if lookup(trim(b-auxapr.repres[3]),l-repres) = 0 then ""
                else trim(b-auxapr.repres[3]) + " " + entry(lookup(b-auxapr.repres[3], l-repres),l-librepres) 
                wtypo
                auxili.libabr
                b-auxapr.code-edition[2]
                cafN [1 for 10]
                cafN[1] + cafN[2] + cafn[3] + cafN[4] + cafN[5] + cafN[6] + cafN[7] + cafn[8] + cafN[9] + cafN[10] 
                cafN[11 for 3] 
                cafN[11] + cafN[12] +  cafN[13] 
                cafN[14 for 3] 
                cafN[14] + cafN[15] +  cafN[16] 
                cafN[17 for 2] 
                cafN[17] + cafN[18] 
                cafN[19 for 3]
                cafN[11] + cafN[12] + cafN[13] + cafN[14] + cafN[15] + cafN[16] + cafN[17] + cafN[18] + cafN[19] + 
                cafN[20] + cafN[21] 
                cafN1 [1 for 10]
                cafN1[1] + cafN1[2] + cafN1[3] + cafN1[4] + cafN1[5] + cafN1[6] + cafN1[7] + cafN1[8] + cafN1[9] + cafN1[10] 
                cafN1[11 for 3] 
                cafN1[11] + cafN1[12] +  cafN1[13] 
                cafN1[14 for 3] 
                cafN1[14] + cafN1[15] +  cafN1[16] 
                cafN1[17 for 2] 
                cafN1[17] + cafN1[18] 
                cafN1[19 for 3]
                cafN1[11] + cafN1[12] + cafN1[13] + cafN1[14] + cafN1[15] + cafN1[16] + cafN1[17] + cafN1[18] + cafN1[19] + 
                cafN1[20] + cafN1[21] 

                QteN [1 for 10]
                QteN[1] + QteN[2] + Qten[3] + QteN[4] + QteN[5] + QteN[6] + QteN[7] + Qten[8] + QteN[9] + QteN[10] 
                QteN[11 for 3] 
                QteN[11] + QteN[12] +  QteN[13] 
                QteN[14 for 3] 
                QteN[14] + QteN[15] +  QteN[16] 
                QteN[17 for 2] 
                QteN[17] + QteN[18] 
                QteN[19 for 3]
                QteN[11] + QteN[12] + QteN[13] + QteN[14] + QteN[15] + QteN[16] + QteN[17] + QteN[18] + QteN[19] + 
                QteN[20] + QteN[21] 

                QteN1 [1 for 10]
                QteN1[1] + QteN1[2] + QteN1[3] + QteN1[4] + QteN1[5] + QteN1[6] + QteN1[7] + QteN1[8] + QteN1[9] + QteN1[10] 
                QteN1[11 for 3] 
                QteN1[11] + QteN1[12] +  QteN1[13] 
                QteN1[14 for 3] 
                QteN1[14] + QteN1[15] +  QteN1[16] 
                QteN1[17 for 2] 
                QteN1[17] + QteN1[18] 
                QteN1[19 for 3]
                QteN1[11] + QteN1[12] + QteN1[13] + QteN1[14] + QteN1[15] + QteN1[16] + QteN1[17] + QteN1[18] + QteN1[19] + 
                QteN1[20] + QteN1[21] 
                wcap
                b-auxapr.siret 
                wcert-phyto
                dcert-phyto.

           export STREAM edition2 delimiter ";"
                codsoc-soc auxili.codaux auxili.libabr 
                auxili.adres[1] trim(auxili.adres[5])
                b-auxapr.magasin

                if lookup(trim(b-auxapr.repres[1]),l-repres) = 0 then ""
                else trim(b-auxapr.repres[1]) + " " + entry(lookup(b-auxapr.repres[1], l-repres),l-librepres)
                if lookup(trim(b-auxapr.repres[2]),l-repres) = 0 then ""
                else trim(b-auxapr.repres[2]) + " " + entry(lookup(b-auxapr.repres[2], l-repres),l-librepres) 
                if lookup(trim(b-auxapr.repres[3]),l-repres) = 0 then ""
                else trim(b-auxapr.repres[3]) + " " + entry(lookup(b-auxapr.repres[3], l-repres),l-librepres) 
                wtypo  
                cafN[11 for 3] 
                cafN[11] + cafN[12] +  cafN[13] 
 
                cafN[14 for 3] 
                cafN[14] + cafN[15] +  cafN[16] 

                cafN[17 for 2] 
                cafN[17] + cafN[18] 
  
                cafN[11] + cafN[12] + cafN[13] + cafN[14] + cafN[15] + cafN[16] + cafN[17] + cafN[18] 
              
                QteN[14 for 3] 
                QteN[14] + QteN[15] +  QteN[16]  .


         end.

      end. 
    end.

    /*--------------------------*/
    /*  RAZ des variables cumul */
    /*--------------------------*/
  
    output stream edition close.
    if wtypfic = "3" then 
      output stream edition2 close.

    message "fin de traitement : "  string(time,"hh:mm:ss")
    " societe " codsoc-soc " nb ecris = " wcpt. pause 5.
    wcpt = 0.
  END . /* do societe */


end procedure.
