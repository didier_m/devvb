/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/ext-agil0.i                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 61252 11-10-16!Evo!pha            !report modifs 273                                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - A G I L 0 . I                          */
/* Extraction des donnees pour AGIL                                         */
/*--------------------------------------------------------------------------*/
/* Variables et Frames                                                      */
/*==========================================================================*/

/* Variables */
def var ind       as int                 no-undo.
def var lib-trt   as char format "x(15)" no-undo.
def var lib-table as char format "x(76)" no-undo.

def {1} shared var libelle      as char extent 30           no-undo.
def {1} shared var type-trt     as char format "x"          no-undo.
def {1} shared var l-soc        as char format "x(32)"      no-undo.
def {1} shared var l-tables     as char format "x(45)"      no-undo.
def {1} shared var date-debut   as date format "99/99/9999" no-undo.

def {1} shared buffer date-tabges for tabges .

/* Dessin des Frames */
def {1} shared frame fr-saisie.
def {1} shared frame fr-trt.

Form libelle[01] format "x(18)" type-trt lib-trt  skip
     libelle[02] format "x(18)" l-soc             skip
     libelle[03] format "x(18)" l-tables          skip
     libelle[04] format "x(18)" date-debut
     with frame fr-saisie row 5 centered no-label overlay { v6frame.i }.

Form skip lib-table skip
     with frame fr-trt row 14 centered no-label overlay { v6frame.i }. 