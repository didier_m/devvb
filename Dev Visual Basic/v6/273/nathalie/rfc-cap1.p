/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/rfc-cap1.p                                                                     !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 281127 2019-06-20!Bug!Patrick        ![GC][NATHAPP]Probl�me de compilation, renpacer tarcli par {tarcli.i}  !
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End                                                                                                          
**************************************************************************************************************/
/* CBA-FIC 31 Suite fusion Agri Haute Loire & COPCEL. Mise a jour Capital Social <EVO LBO 20/07/04> */
/* CBA-FIC 4 Fusion du capital social. <NEW adh 04/07/00> */
/*==========================================================================*/
/*                          R F C - C A P 1 . P                             */
/*       GESTION des RISTOURNES    : MAJ DU CA ET MT RISTOURNES EN CAPITAL  */
/*       Constitution ou Maj de CAPSOLD (fichier compta)                    */
/*==========================================================================*/
/* DGR 21102013: si CA RFC remi <> alors on prend celui de rfcremi          */  
/* DGR 09102014: cumul CA codaux-fiscal                                     */  
/*==========================================================================*/

{ connect.i  }
{ natabsoc.i   }
{ rfc-cap0.i }
 
define buffer stacli for {stacli.i}.

/*-------------------------------------*/
/* Definition des Variables            */
/*-------------------------------------*/
def var  i            as int                        no-undo .
def var  j            as int                        no-undo .
def var nb-trt        as int                        no-undo .
def var  cod-etb      as char      initial "001"    no-undo .
def var  ann-st       as int                        no-undo .
def var  mt-ca        as dec format "zzzzzzzz9.99-" no-undo .
def var  mt-rist      as dec format "zzzzzzzz9.99-" no-undo .
def var  mois-debex   as int                        no-undo .
def var  mois-finex   as int                        no-undo .

/* dgr 10102014 */
def var  type-adh    as char              no-undo .
def var  type-tiers  as char              no-undo .
def var  code-tiers  as char              no-undo .
def var  code-mag    as char              no-undo .
def buffer b-auxapr   for auxapr.

def var  tot-rist     as dec format "zzzzzzzz9.99-" no-undo .
def var  mouch        as char .
def var  annee-exe    as char      format "xxxx"    no-undo .
def var  mois         as int       format 99        no-undo .
def var soc-capital   as char format "xxxx" no-undo.

def var  mt-carfc     as dec format "zzzzzzzz9.99-" no-undo .
 
/*-------------------------------------*/
/* Initialisations                     */
/*-------------------------------------*/
 
/* Societe sur laquel le capital social est gere */
If codsoc-soc = "02"
Then assign cod-objet    = "1"
            cod-activite = "10"
            soc-capital  = "01" .
Else assign cod-objet    = "2"
            cod-activite = "20"
            soc-capital = codsoc-soc .
 
/*------------------------------------------*/
/*- Suite fusion Agri Haute Loire & COPCEL -*/
/*-------------------------------------------------*/
If codsoc-soc = "34" Then Assign cod-objet    = "1" 
                                 cod-activite = "10"
                                 soc-capital  = "05".
/*-------------------------------------------------*/
 
assign mois-debex =
       int( (int(year(date-debex)) * 100 ) + int(month(date-debex)) )
       mois-finex =
       int( (int(year(date-finex)) * 100 ) + int(month(date-finex)) ) .
 
mouch = "logrist." + lc ( trim ( operat )) .
 
display  mouch with frame fr-mouch row 12 centered overlay
no-label title " Mouchard " {v6frame.i}  .
 
/*----------------------------------------------------------------------------*/
/*  Debut du Traitement                                                       */
/*----------------------------------------------------------------------------*/
 
output to value ( mouch ) .
 
GENERALE :
REPEAT :
 
    /*-------------------------------------------*/
    /* Lecture des clients  qui sont Adherent    */
    /*-------------------------------------------*/
    FOR EACH AUXAPR where auxapr.codsoc = codsoc-soc
                            use-index auxapr-1 no-lock  :
 
        assign type-tiers = auxapr.typaux
               code-tiers = auxapr.codaux
			   code-mag   = auxapr.magasin
			   type-adh   = trim( auxapr.type-adh ).

			   
		if auxapr.typaux-fiscal + auxapr.codaux-fiscal <> auxapr.typaux + auxapr.codaux and 
		    auxapr.typaux-fiscal  <> "" and auxapr.codaux-fiscal <> ""
        then do :
            FIND B-AUXAPR 
			where b-auxapr.codsoc = auxapr.codsoc
            and   b-auxapr.typaux = auxapr.typaux-fiscal
            and   b-auxapr.codaux = auxapr.codaux-fiscal
            no-lock no-error .

            if not available b-auxapr then next .

            assign type-adh   = b-auxapr.type-adh
				   type-tiers = b-auxapr.typaux
                   code-tiers = b-auxapr.codaux
                       .
            /* dgr 10072012 */
           code-mag   = b-auxapr.magasin.
        end.
        assign mt-ca   = 0
               mt-rist = 0 
               mt-carfc = 0.
 
        if nb-trt modulo 100 = 0
        then do :
            hide message . pause 0 .
            message fill( " " , 10 )  "Traitement Adherent :"
                    auxapr.typaux auxapr.codaux .
        end .
        nb-trt = nb-trt + 1 .
 
        /* 1) Calcul du CA de l'exercice */
        FOR EACH STACLI where stacli.codsoc = codsoc-soc
                          and stacli.motcle = "VTE"
						  /*
                          and stacli.typaux = auxapr.typaux
                          and stacli.codaux = auxapr.codaux
						  */
                          and stacli.typaux = type-tiers
                          and stacli.codaux = code-tiers

                          use-index primaire no-lock  :
 
 
            ann-st = int( stacli.annee ) * 100 .
 
            DO I = 1 TO 12 :
 
                if ann-st + i >= mois-debex  and  ann-st + i <= mois-finex
                then
                assign mt-ca = mt-ca + stacli.fac-ht[ i ] .
 
            END . /*  DO I = 1 TO 12   */
 
 
        End . /* For Each Stacli */

		mt-rist = 0 .
        mt-carfc = 0.
 
        /* 2) Recherche du Montant de la Ristourne  */
 
        /* Ajout boucle sur les magasins pour acces RFCREMI par cle primaire
           et non par FOR EACH sans magasin ( CC 14/10/1999 )
        */
 
        /* FOR EACH MAGASI where magasi.codsoc = magasi-soc
                        no-lock :
        */
            FIND RFCREMI where rfcremi.codsoc = codsoc-soc
                         and   rfcremi.motcle = mot-cle
                         /*
						 and   rfcremi.magasi = magasi.codtab
                         and   rfcremi.typaux = auxapr.typaux
                         and   rfcremi.codaux = auxapr.codaux
						 and   rfcremi.magasi = code-mag
						 */
                         and   rfcremi.typaux = type-tiers
                         and   rfcremi.codaux = code-tiers
                         no-lock no-error .
 
            if available rfcremi then 
               assign mt-rist = mt-rist + rfcremi.mt-rem 
                      mt-carfc = mt-carfc + rfcremi.mt-ht .
 
        /* END . */  /* FOR EACH MAGASI */

        /* DGR 21102013 
           si CA <> entre stacli et rfcremi on prend celui de rfcremi
           car pour les doublons, le CA est consolid� sur le codaux 
           principal 
		*/
        if mt-ca <> mt-carfc then assign mt-ca = mt-carfc.
		
		if type-adh <> "1"
        then do :
 
          display auxapr.codsoc auxapr.typaux auxapr.codaux mt-rist mt-ca  .
          pause 0.
 
          assign tot-rist = tot-rist + mt-rist
                 mt-rist  = 0
                 mt-ca    = 0
                 .
 
          next .
 
        end .
 
        if mt-rist < 0 then mt-rist = 0 .
 
        /*  Maj  fichier  CAPBSOLD */
        FIND CAPBSOLD where capbsold.codsoc = soc-capital
                      and capbsold.codetb   = cod-etb
                      and capbsold.execpt   = string(year(date-finex))
                      and capbsold.typaux   = type-tiers
                      and capbsold.codaux   = code-tiers
                      and capbsold.objet    = cod-objet
                      and capbsold.activite = cod-activite
                      exclusive-lock no-error .
 
        if not available CAPBSOLD
        then do :
            create CAPBSOLD .
            assign capbsold.codsoc     = soc-capital
                   capbsold.codetb     = cod-etb
                   capbsold.execpt     = string(year(date-finex))
                   capbsold.typaux     = type-tiers
                   capbsold.codaux     = code-tiers
                   capbsold.objet      = cod-objet
                   capbsold.activite   = cod-activite
                   capbsold.valid-rist = "1"
                   .
        end .
 
        assign capbsold.date-souscript= date-finex
               capbsold.zone-spe = code-mag
               capbsold.base-cap = mt-ca
               capbsold.mt-rist  = mt-rist .

		assign mt-ca = 0
			   mt-rist = 0.
    End .     /* For Each Auxapr */
 
    leave .
 
END .   /* GENERALE  */
 
/* Modif laurent PAITREAULT              */
/* Si pas de capbsold, on le cree a zero */
 
For each Capsoci where Capsoci.codsoc = soc-capital
                   and Capsoci.codetb = codetb-etb
                   no-lock :
 
        /* Recherche bases de souscripiton */
 
        FIND CAPBSOLD where capbsold.codsoc   = soc-capital
                      and capbsold.codetb   = codetb-etb
                      and capbsold.execpt   = string( year(date-finex))
                      and capbsold.typaux   = Capsoci.typaux
                      and capbsold.codaux   = Capsoci.codaux
                      and capbsold.objet    = cod-objet
                      and capbsold.activite = cod-activite
                      no-lock no-error .
 
        if not available CAPBSOLD
        then do :
            create CAPBSOLD .
            assign capbsold.codsoc   = soc-capital
                   capbsold.codetb   = codetb-etb
                   capbsold.execpt   = string(year(date-finex))
                   capbsold.typaux   = Capsoci.typaux
                   capbsold.codaux   = Capsoci.codaux
                   capbsold.objet    = cod-objet
                   capbsold.activite = cod-activite
                   capbsold.date-souscript= date-finex
                   capbsold.zone-spe = Capsoci.zone-spe
                   capbsold.valid-rist = "1"
                   .
        end .
 
End .
 
message  " " .
 
message "TOTAL RISTOURNES REJETEES :" tot-rist . readkey pause 5 .
 
output close .
 
run bat-imp.p ( mouch ) .
 
 
 
 

 
