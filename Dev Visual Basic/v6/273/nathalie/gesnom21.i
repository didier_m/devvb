/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/273/273/gesnom21.i                                                                      !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!V61 38343 24-05-13!Evo!cch            !Ajout gestion Interfaces et Saisies NATHALIE - NOMEREF                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*============================================================================*/
/*                           G E S N O M 2 1 . I                              */
/* Gestion Echanges NATHALIE - NOMEREF                                        */
/*============================================================================*/
/* Fiche Articles liens NATHALIE - NOMEREF - Module Recherche - Include Acces */
/*============================================================================*/

lect = "{1}" .

case type-rech :

    /* Recherche Articles NOMEREF */
    when "1" then case lect :

        when "" then FIND ARTICS where artics.codsoc    = nomeref-soc
                                 and   artics.theme     = theme-trt
                                 and   artics.alpha-cle = rech-clef
                                 and   artics.articl    = ""
                                 use-index theme no-lock no-error .   

        otherwise    FIND {1} ARTICS where artics.codsoc    = nomeref-soc
                                     and   artics.theme     = theme-trt
                                     and   artics.alpha-cle begins codtab
                                     and   artics.articl    = ""
                                     use-index theme no-lock no-error .   

    end case .

    /* Recherche liens Articles NATHALIE - NOMEREF */
    when "2" then case lect :

        when "" then FIND ARTICS where artics.codsoc = nomeref-soc
                                 and   artics.articl = rech-clef
                                 and   artics.theme  = theme-trt
                                 and   artics.chrono = 0
                                 use-index primaire no-lock no-error .   

        otherwise    FIND {1} ARTICS where artics.codsoc    = nomeref-soc
                                     and   artics.theme     = theme-trt
                                     and   artics.alpha-cle begins codtab
                                     and   artics.articl    > ""
                                     and   artics.chrono    = 0
                                     use-index theme no-lock no-error .   

    end case .

end case .

