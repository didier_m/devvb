/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/ext-agreo.i                                                                    !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End                                                                                                          
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - A G R E O . I                          */
/*                                                                          */
/* Extraction des exploitations et affectations TC ===> AGREO               */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/* Extraction Tiers AGREO                                                   */
/*==========================================================================*/
/* DGR 070313 : on ne prefix pas le code client avec le code soc            */
/* DGR 210313 : pas de zero dans code client                                */

/* EXPLOITATIONS */

  if lookup(auxapr.repres[1], l-tcagreo[i-agreo]) <> 0 or
     lookup(auxapr.repres[2], l-tcagreo[i-agreo]) <> 0  or
     lookup(auxapr.repres[3], l-tcagreo[i-agreo]) <> 0
  then do:

    /*
    zone = string(trim(auxapr.codsoc),"99") + substr(auxapr.codaux , 5, 6) + ";".
    zone = trim(auxapr.codaux) + ";".
    */
    zone = string(int(auxapr.codaux)) + ";".

    /* supression de la civilite */
    wlibagreo = auxili.adres[1] .

    wlibagreo = replace(wlibagreo, "MRS "," ").
    wlibagreo = replace(wlibagreo, "MR "," ").
    wlibagreo = replace(wlibagreo, "MME "," ").
    wlibagreo = replace(wlibagreo, "MLE "," ").
    wlibagreo = replace(wlibagreo, "  "," ").        

    assign zone = zone 
         + trim(string ( wlibagreo , "x(32)" ))                            + ";"
         /*
         + string(trim(auxapr.codsoc),"99") + substr(auxapr.codaux , 5, 6) + ";"  
         + trim(auxapr.codaux )                                            + ";"  
         */
         + string(int(auxapr.codaux ))                                     + ";"  
         + " "                                                             + ";"   /* forme juridique non GERE SUR DEAL */
         + "EXP"                                                           + ";" 
         + trim(auxili.siret)                                              + ";"
         + trim(auxili.adres[2])                                           + "\\n"
         + trim(auxili.adres[3])                                           + "\\n"
         + trim(auxili.adres[4])                                           + ";"
         + substr(auxili.adres[5],1,5)                                     + ";" 
         + string ( int(substr( auxapr.onic , 1 , 6) ), "99999")           + ";" .
         
   
    /* telephone */
    wlibagreo = trim(auxapr.teleph).
    wlibagreo = replace (wlibagreo," ","").
    
    assign zone = zone                                           
         + wlibagreo                                                       + ";"  .


    /* portable */
    wlibagreo = trim(auxapr.telex).
    wlibagreo = replace (wlibagreo," ","").
    
    assign zone = zone                                           
         + wlibagreo                                                       + ";"  .

    /* fax */
    wlibagreo = trim(auxapr.fax).
    wlibagreo = replace (wlibagreo," ","").
    
    assign zone = zone                                           
         + wlibagreo                                                       + ";"  
         + trim(substr(auxapr.alpha-5, 1 , 50))                            + ";"         
         + string(auxapr.datmaj,"99/99/9999")                              + ";"         
         + "31/12/2050" .
    
    put stream kan-agreo1 unformatted zone skip.


/* LIENS EXPLOITATIONS <=> TC */    

    /* envoi des TC actuels */

    woldtc = "".
    
    do i-agreo2 = 1 to 3:

      if lookup(auxapr.repres[i-agreo2],l-tcagreo[i-agreo]) <> 0 then do:

        if woldtc = "" then woldtc = auxapr.repres[i-agreo2].
        else 
          if woldtc = auxapr.repres[i-agreo2]  then next.
   
        zone = trim(auxapr.repres[i-agreo2] )                                         + ";" 
/*
             + string(trim(auxapr.codsoc),"99") + substr(auxapr.codaux , 5, 6) + ";"
             + trim(auxapr.codaux )                                            + ";"
*/
             + string(int(auxapr.codaux ))                                     + ";"
             + "01/01/2012"                                                    + ";"
             + "31/12/2050"                                                    + ";"
             + string(auxapr.datmaj,"99/99/9999")                              + ";"         
             + "" .
         
        put stream kan-agreo2 unformatted zone skip.

      end.
      
    end.   
  end.


  /* LIENS EXPLOITATIONS <=> ANCIENS TC POUR DESACTIVATION DANS AGREO */    
    
  /* envoi des eventuels changements de TC */
  find auxdet
  where auxdet.codsoc = auxapr.codsoc
  and   auxdet.typaux = auxapr.typaux
  and   auxdet.codaux = auxapr.codaux
  and   auxdet.motcle = "WEB/TIERS"
  and   auxdet.codtab = ""
  and   trim(auxdet.calf20) <> "" 
  /* no-lock  */ no-error.
  if available auxdet then do:
    
    do i-agreo2 = 1 to num-entries(auxdet.calf20):
      woldtc = entry(i-agreo2,auxdet.calf20).
      if woldtc = "" then next.
      if lookup(trim(entry(1, woldtc,"|")),",998,999") = 0  then do:

        zone = trim(entry(1, woldtc,"|"))                                      + ";" 
/*
             + string(trim(auxapr.codsoc),"99") + substr(auxapr.codaux , 5, 6) + ";"
             + trim(auxapr.codaux )                                            + ";"
*/
             + string(int(auxapr.codaux ))                                     + ";"
             + "01/01/2012"                                                    + ";"
             + entry(2, woldtc,"|")                                            + ";"
             + string(auxapr.datmaj,"99/99/9999")                              + ";"         
             + "".
        put stream kan-agreo2 unformatted zone skip.

      end.
    end.

    if exploit /* remise a blanc des anciens TC si trt exploitation */
    then do :
       
      assign auxdet.calf20 = "".
                                       
      { majmoucb.i auxdet }
    end .    
  end. /* available auxdet */
    

