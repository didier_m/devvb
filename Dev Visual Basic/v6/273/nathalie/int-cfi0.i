/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/int-cfi0.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 14569 29-03-06!Evo!cch            !Nouveau format du fichier Carte Fid�lit�                              !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           I N T - C F I 0 . I                            */
/* Integration des Fiches Cartes Fidelite Magasin                           */
/*--------------------------------------------------------------------------*/
/* Variables et Frames                                        CC 16/01/2001 */
/*==========================================================================*/

def var ind         as int                 no-undo .
def var top-valid   as log  format "1/0"   no-undo .
def var fichier-cfi as char format "x(50)" no-undo .

def {1} shared var libelle as char extent 70 no-undo .

form skip
     libelle[01] format "x(23)" top-valid
     skip
     with frame fr-saisie row 10 centered no-label overlay { v6frame.i } .

form skip
     libelle[02] format "x(15)" fichier-cfi
     skip
     with frame fr-rejet row 5 centered no-label overlay { v6frame.i } .