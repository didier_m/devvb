/*==========================================================================*/
/*                           E X T - O A R T . I                            */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Articles OET                                    CC 01/03/1999 */
/*==========================================================================*/

    ok-oet = no.

    if artapr.codsoc = "03" and
     ( artapr.typart = "AL" or
       artapr.typart = "AS" or
       artapr.typart = "MP" or
       artapr.typart = "EM" ) and
       substring (artapr.articl, 1, 1) >= "A" and
       substring (artapr.articl, 1, 1) <= "Z" and
       not blocage
    then do :

        assign zone = "ARA"
                      + string ( artapr.articl     , "x(10)" )
                      + string ( artapr.libart1[1] , "x(32)" )
                      + string ( artapr.typart     , "xx" )
                      + string ( artapr.presentation , "x" )
                      + string ( artapr.articl     , "x" )
                      + string ( substring( artapr.articl , 3 , 1 ) , "x" )
                      + string ( artapr.famart     , "xxx" )
                      + string ( artapr.soufam     , "xxx" )
                      + string ( artapr.pds * artbis.surcondit , "999999.999" )
                      + string ( artapr.type-rec   ,  "x" )
                      + string ( artapr.formule   ,  "xxx" )
                      + "0"
                      + string ( artapr.code-blocage  , "x")
                      + "F"
                      .

        ok-oet = yes.

    end.
