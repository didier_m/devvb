/*---------------------------------------------------------------*/
/* Interface de remontee des factures Fournisseurs (Version CBA) */
/* Include de controle de fin de traitement d'un fichier         */
/* trsfac0.i                                                     */
/*---------------------------------------------------------------*/

               /* Test retour traitement fichier */
               If verr1 = True
               then do:

                   Put stream err unformatted '"' + "Traitement annule fic. non integre :" + ficfind + '"' skip.
                   Put stream err unformatted '"' + " " + '"' skip.

                   /* On annule la transaction et on passe au fichier suivant */
                   Undo, next.

                End. /* verr1 = true */

                Else do:

                    /* MV fichier traite */
                    assign pos-fichier = r-index(liglue,"/")
                           commande    = "mv " + ficfind + " " + repert +
                           mv-repert + trait-repert +
                           trim(substr(liglue,pos-fichier + 1)) +
                           "." + string (today , "999999") +
                           string (time , "hh:mm")
                           .

                    message commande. readkey pause 0.
                    Unix silent value ( commande ) .

                    If verr = true
                    then do:

                        Put stream err unformatted '"' + "Erreurs sur le fichier : " + ficfind + '"' skip.
                        Put stream err unformatted '"' +
                        "Fichier quand meme integre (erreurs non bloquantes)"
                        + '"' skip.
                        Put stream err unformatted '"'
                        + "Fichier renomme en : "
                        + repert + mv-repert + trait-repert
                        + trim(substr(liglue,pos-fichier + 1))
                        + substr(commande,length(commande) - 11)
                        + '"' skip.
                        Put stream err unformatted '"' + " " + '"' skip.

                    End. /* verr = true */

                    Else do :

                        Put stream err unformatted '"' + "OK pour le fichier : " + ficfind + '"' skip.
                        Put stream err unformatted '"' + "Fichier renomme en : "
                        + repert + mv-repert + trait-repert
                        + trim(substr(liglue,pos-fichier + 1))
                        + substr(commande,length(commande) - 11)
                        + '"' skip.
                        Put stream err unformatted '"' + " " + '"' skip.

                    end. /* verr = false and verr1 = false */

               end. /* else do sur if verr1 = true */