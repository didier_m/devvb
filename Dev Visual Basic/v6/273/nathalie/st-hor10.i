/*==========================================================================*/
/*                           S T - H O R 1 0 . I                            */
/* Statistique par tranches horaires par magasin                            */
/*--------------------------------------------------------------------------*/
/* Variables et Frames                                                      */
/*==========================================================================*/

/* Variables */
def var ind                    as int                         no-undo.
def var lib-maq                as char format "x(30)"         no-undo.
def var lib-mag                as char format "x(30)"         no-undo.
def var edi                    as char extent 10              no-undo.

def {1} shared var libelle     as char extent 50              no-undo.
def {1} shared var datmin      as date format "99/99/9999"    no-undo.
def {1} shared var datmax      as date format "99/99/9999"    no-undo.
def {1} shared var semmin      as int  format ">9"            no-undo.
def {1} shared var semmax      as int  format ">9"            no-undo.
def {1} shared var magasins    as char format "x(40)"         no-undo.
def {1} shared var magasin     as char                        no-undo.
def {1} shared var maquette    as char format "x(12)"         no-undo.
def {1} shared var periph      as char format "x(12)"         no-undo.
def {1} shared var magasi-soc  as char                        no-undo.
def {1} shared var mag-exclus  as char                        no-undo.

/* Dessin des Frames */
def {1} shared frame fr-saisie.

Form libelle[01] format "x(19)" datmin                    skip
     libelle[02] format "x(19)" datmax                    skip
     libelle[03] format "x(19)" magasins                  skip
     libelle[04] format "x(19)" maquette lib-maq          skip
     libelle[05] format "x(19)" periph
     with frame fr-saisie row 3 centered no-label overlay { v6frame.i }.
