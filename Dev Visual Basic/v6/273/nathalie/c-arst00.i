/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/c-arst00.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 12740 02-06-05!Evo!cch            !Ajout possibilité de traiter un seul article                          !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           C - A R S T 0 0 . I                            */
/* Arrete de Stock                                                          */
/*--------------------------------------------------------------------------*/
/* Variables et Frames partagees                                            */
/*==========================================================================*/

def var lib-maj    as char format "x(15)" no-undo .
def var articl-soc as char                no-undo .
def var magasi-soc as char                no-undo .

def {1} shared var libelle          as char extent 30       no-undo .
def {1} shared var date-arrete      like stoarr.date-arrete no-undo .
def {1} shared var date-arrete-exe  like stoarr.date-arrete no-undo .
def {1} shared var date-arrete-mois like stoarr.date-arrete no-undo .
def {1} shared var majour           as char format "x"      no-undo .
def {1} shared var maj-prix         as char format "x"      no-undo .
def {1} shared var code-art         as char format "x(10)"  no-undo .
def {1} shared var valida           as char format "x"      no-undo .

def {1} shared frame fr-saisie .

form " " skip
     " " libelle[ 11 ] format "x(21)" date-arrete-mois                      " " skip( 1 )
     " " libelle[ 12 ] format "x(21)" date-arrete                           " " skip( 1 )
     " " libelle[ 13 ] format "x(21)" majour lib-maj                        " " skip( 1 )
     " " libelle[ 14 ] format "x(21)" code-art artapr.libart1[ mem-langue ] " " skip( 1 )
     " " libelle[ 15 ] format "x(21)" valida                                " " skip( 1 )
     with frame fr-saisie row 5 centered overlay no-label { v6frame.i } .
