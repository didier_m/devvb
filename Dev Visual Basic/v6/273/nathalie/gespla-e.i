/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/gespla-e.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V61 33946 12-11-12!Evo!cch            !Provenance '9' pour les articles GVSE type 'E' (ferm�s)-Modif. CCO    !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/* Nom : applic/273/nathalie/gespla-e.i                                     */

/* Procedure commune pour les programmes 
   - gespla-7.p
   - majlisa.p

CCO le 29/08/2012 suite nouvelles Filieres/Provenance GVSE K/6 et B/4
DGR le 22/05/2018 ajout statut  i = 2 ferme 
                        filiere T = stocke ( terroir stocke gvse )
*/   

PROCEDURE P-PROVENANCE :

/* zones entr�es test�es
   flgsta
   stocke 
   w-prove : contient 2 ou 5 pour savoir si on traite GVSE ou DISPAGRI
   wope

Valeurs de FLGSTA
   1=O ouvert
   2=F ferme
   3=I En instance de fermeture
     (article termine, la plate-forme ne commande plus)
   4=A En instance d ouverture
   5=Hors saison
   6=Probleme fabricant
   7=Promo epuisee
   8=Epuise Import
   I=F ferme idem 2 --- DGR  22/05/2018
   nouveaux codes issus DISPAGRI nouvelle informatique
   V equivalent a "4" de GVSE
   F equivalent a "2" de GVSE   

Valeurs de STOCKE
   P <==> O : pieces detachees stockees (supp)    
   R <==> N : pieces detachees non stockees (supp) 
   H <==> N : Hors catalogue                        
   nouvelles valeurs zone stocke/non stocke 
   K <==> 6 : CONTRE MARQUE
   B <==> 4 : OPERATION             
   P et R sont normalement supprim�es       
   T = filiere Terroir stocke Saint Prisest --- DGR 22/05/2018
   Rappel des valeurs sans conversion 
   O : Stocke (Aussi bien GVSE/2/ que DISPAGRI/5/)
   E : Externe
   M : Mixte
   N : Direct
   U : Dispagri Multipro
*/

x-prove = "".

if flgsta = "1" then       /* Ouvert */
   do:
   if stocke = "O" or
      stocke = "P" or 
      stocke = "T" /* DGR 22/05/2018 ajout T */
   then do:
                        if w-prove = "2" then x-prove = "2".
                                         else x-prove = "5". 
                        wope = wope + "1".
                        end.
   else if stocke = "M" then
           assign x-prove = "M" wope = wope + "2".
        else if stocke = "N" or
                stocke = "H" or
                stocke = "R" then
                assign x-prove = "8" wope = wope + "3".
             else if stocke = "E" then
                     assign x-prove = "7" wope = wope + "4".
                  else if stocke = "K" then
                          assign x-prove = "6" wope = wope + "5".
                        else if stocke = "B" then
                                assign x-prove = "4" wope = wope + "6".
                             else if stocke = "U" then
                                     assign x-prove = "U" wope = wope + "7".

   end. /* end statut 1 */
else do:
     /* if lookup(flgsta, "3,I") <> 0 then  /* En instance de fermeture DGR 22/05/2018 ajout I comme 2 , pas comme 3 */ */
     if flgsta = "3"  then  /* En instance de fermeture */
        do:
        if stocke = "O" or
           stocke = "P" or 
           stocke = "T" /* DGR 22/05/2018 ajout T */
           then do:
                        if w-prove = "2" then x-prove = "2".
                                         else x-prove = "5". 
                        wope = wope + "A".
                        end.                 
        else if stocke = "M" then
                assign x-prove = "2" wope = wope + "B".
             else if stocke = "N" or
                     stocke = "K" or
                     stocke = "H" or
                     stocke = "R" then
                     assign x-prove = "9" wope = wope + "C".
                  else if stocke = "E" then
                          assign x-prove = "7" wope = wope + "D".
                       else if stocke = "B" then
                               assign x-prove = "4" wope = wope + "E".
                            else if stocke = "U" then
                                    assign x-prove = "U" wope = wope + "F".
        end. /* end statut 3 */
     else do:
          if lookup(flgsta, "2,F,I") <> 0 then    /* Ferme */  /* DGR 22/05/2018 ajout I */
             do:
             if lookup(stocke, "O,P,M,N,H,R,K,B,U,T") <> 0 then   /* DGR 22/05/2018 ajout T */
                assign x-prove = "9" wope = wope + "I".
             else if stocke = "E" then
                     assign x-prove = "9" wope = wope + "J".
             end. /* end statut 2,F */
          else do:
               if lookup(flgsta, "4,5,6,7,8,V") <> 0 then
                  do:
                  if stocke = "O" or
                     stocke = "P" or 
                     stocke = "T" /* DGR 22/05/2018 ajout T */
                     then  do:
                                  if w-prove = "2" then x-prove = "9".
                                                   else x-prove = "5". 
                                  wope = wope + "K".
                                  end.
                  else if stocke = "M" then
                          assign x-prove = "6" wope = wope + "L".
                       else if stocke = "N" or
                               stocke = "H" or
                               stocke = "R" then
                               assign x-prove = "8" wope = wope + "M".
                            else if stocke = "E" then
                                    assign x-prove = "7" wope = wope + "N".
                                 else if stocke = "K" then
                                         assign x-prove = "6" wope = wope + "O".
                                      else if stocke = "B" then
                                              assign x-prove = "4" wope = wope + "P".
                                           else if stocke = "U" then
                                                   assign x-prove = "U" wope = wope + "Q".
                  end. /* end statut 4,5,6,7,8,V */

               end.

          end.

     end.


END PROCEDURE .

