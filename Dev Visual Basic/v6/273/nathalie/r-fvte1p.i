/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/r-fvte1p.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V61 56540 23-10-15!New!pte            !Synchronisation Code Mr CONSTANT                                      !
!V61 20975 08-10-10!Bug!cch            !Ajout di�ses 28 et 75 manquant dans le rang 15                        !
!V61 19736 06-07-10!Bug!jcc            !Affichage Infos acompte                                               !
!V52 13171 26-08-05!Bug!cch            !Ajout di�ses manquants par rapport aux modules e-fvt*                 !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*-----------------------------------------------------------------------*/
/*    R - F V T E 1 P . I    Include d'edition du Pied de Facture        */
/*      Appele par r-fvte1l.p et par r-fvte11.p ( pour la derniere )     */
/*-----------------------------------------------------------------------*/

    def var solde-pf        as dec      no-undo .

    { g-edich0.i} /* Def. variables Edition Champs parametrables */

    def var comment-soc as log no-undo .
    def var nb-tx as int no-undo .

    if art-bio /* Edition commentaire lie aux articles BIO presents */
    then REPEAT :

        assign prefix-chp  = "PARZON-ART"
               codtab-chp  = "01"
               valeur-chp  = entry( 1, libelle[ 75 ] )
               nom-edition = "FAC"
               .

        run g-edichp .


        /* Si presence de commentaires Societe ou pas de rang 33 alors on
           edite en rang 14 sinon on edite en rang 33
        */
        if lookup( "#001", ma-posi[33] ) = 0
        then do :
            run edit-comment-art . /* Edition Commentaires articles */
            leave .
        end .

        FIND PROGLIB where proglib.motcle  = "FAC-MES"
                     and   proglib.codsoc  = codsoc-soc
                     and   proglib.etabli  = ""
                     and   proglib.clef    = ""
                     and   proglib.edition = ""
                     and   proglib.chrono  = 0
                     no-lock no-error .

        if available proglib
        then do i = 1 to 4 :

            if proglib.libelle[ i ] <> ""
            then do :
                assign rg          = 18
                       comment-soc = yes
                       .
                run edit-comment-art . /* Edition Commentaires articles */
                leave .
            end . /* if proglib.libelle[ i ] <> "" */

        end . /* do i = 1 to 4 : */

        leave .

    END . /* if art-bio */

    ma-edipied = "O" .

    /* if edi-fond then run edit-comment-pied . */ /* Edition Commentaires societe */

    assign valeur = ""
           rg = 15
           .

    FIND TABLES where tables.codsoc = codsoc-soc
                and   tables.etabli = codetb-etb
                and   tables.typtab = "ETA"
                and   tables.prefix = "ADRESSE"
                and   tables.codtab = ""
                no-lock no-error .

    if available tables then valeur[117] = tables.libel1[1].

    FIND AUXAPR where auxapr.codsoc = codsoc-soc
                and   auxapr.typaux = x-typaux
                and   auxapr.codaux = x-codaux
                no-lock no-error.

    if available auxapr and ( auxapr.type-adh = "1" or auxapr.type-adh = "5" )
       then x-type-adh = entry(1, libelle[32]). /* Adherents */
       else x-type-adh = entry(2, libelle[32]). /* Autres */

    assign tva-fac = 0 ht-fac = 0 .

    do i = 1 to 10 :
        ht-fac  = ht-fac  + base-tva[i] .
        tva-fac = tva-fac + mtt-tva [i] .
    end .
    ttc-fac = ht-fac + tva-fac .

/*    i = 0 .
    do j = 1 to 10 :

        if code-tva[j] = "" then next .
        i = i + 1 .
        if i > 3 then leave .
        valeur[i + 40] = string ( code-tva[j] , "xxx" ) .
        if base-tva[j] <> 0 then
              valeur [i + 15] = string ( base-tva[j] , msk-mt ) .
        if base-tva[j] <> 0
        then do :
            if mtt-tva[j] = 0
               then valeur [i + 10] = "Exon." .
               else valeur [i + 10] = string ( ( taux-tva[j] * 100 ) , ">9.99" ) .
        end .

        if mtt-tva[j] <> 0 then
              valeur [i + 20] = string ( mtt-tva[j] , msk-mt ) .
        if base-tva[j] + mtt-tva[j] <> 0 then
              valeur [i + 55] = string ( base-tva[j] + mtt-tva[j] , msk-mt ) .

    end .
*/

nb-tx = 0 .
DO I = 1 TO 5 :

    if base-tva[ i ] = 0  then  next .

    nb-tx = nb-tx + 1 .
    if nb-tx > 3  then  leave .

    valeur[ nb-tx + 10 ] = string( ( taux-tva[ i ] * 100 ) , msk22 ) + "%"  .
    valeur[ nb-tx + 15 ] = string ( base-tva[ i ] , msk72 ) .
    valeur[ nb-tx + 20 ] = string ( mtt-tva[ i ]  , msk62 ) .
    valeur[ nb-tx + 50 ] = code-tva[ i ] .
    valeur[ nb-tx + 55 ] = string ( base-tva[ i ] + mtt-tva[ i ]  , msk72 ) .

END .  /*  DO I = 1 TO 5  */

    assign
        valeur[ 28 ] = string( ttc-fac , msk-mt )
        valeur[ 31 ] = string( ht-fac  , msk-mt )
        valeur[ 32 ] = string( tva-fac , msk-mt )
        valeur[ 35 ] = string( ttc-fac , msk-mt )
        valeur[118 ] = string( tot-remfac , msk-mt7 )
        valeur[ 06 ] = string( trim( x-codaux ) , "x(6)" )
        valeur[ 07 ] = string( trim( x-numfac ) , "x(10)" )
        valeur[ 60 ] = string( ttc-fac , msk-mt )
/*        valeur[ 09 ] = string( echeance , "99/99/9999" )
        valeur[ 10 ] = string( echeance , "99/99/9999" )
*/        valeur[ 09 ] = alf-datech
        valeur[ 10 ] = alf-datech
      valeur[120 ] = "PAIEMENT PAR " + trim( lib-modreg )
        valeur[119 ] = lib-prelev
        valeur[ 36 ] = if x-bascule = ""
                          then string( ttc-fac / tx-frf-eur, msk-mt )
                          else string( ttc-fac * tx-frf-eur, msk-mt )
        valeur[116 ] = trim(codsoc-soc) + "/" + x-type-adh + "/" +
                       trim(x-codaux)
        valeur[ 08 ] = string( x-datfac , "99/99/9999" )
        valeur[ 08 ] = alf-datfac 
        .

        if valeur[ 119 ] = "" then valeur[ 119 ] = lib-modreg .

    /* edition code magasin pour Soc 06 DESMAGRI */
    if codsoc-soc = "06"
    then do :
        valeur[ 06 ] = trim ( x-codaux ) .
        aa = 0 .
        do i = 1 to length ( x-codaux ) :
           aa = aa + int ( substring( x-codaux , i , 1 ) ) .
        end .
        valeur[ 06 ] = string ( substring(trim(x-codaux) , 1 , 3 ) , "xxx" )
                     + string ( aa , "99" )
                     + string ( substring(trim(x-codaux) , 4 , 3 ) , "xxx" ) .
    end .

    assign valeur[ 75 ] = libelle[ 38 ]
           valeur[113 ] = " A Payer "
           valeur[112 ] = " NET A PAYER"
           valeur[114 ] = "FACTURE"
           valeur[115 ] = "Facture" .

    if ttc-fac < 0 then
       assign valeur[113 ] = "AVOIR"
              valeur[112 ] = "A VOTRE CREDIT"
              valeur[114 ] = " AVOIR"
              valeur[115 ] = "Avoir" .

    solde-pf = 0 .
    FIND TABGES where tabges.codsoc = codsoc-soc
                and   tabges.etabli = ""
                and   tabges.typtab = "SAV"
                and   tabges.prefix = "SOLDE-PF"
                and   tabges.codtab = x-numfac
                no-lock  no-error .
    if available tabges  then  solde-pf = tabges.nombre[ 1 ] .

    IF SOLDE-PF <> 0  THEN
    DO :

        if solde-pf >= ttc-fac    /*  Montant facture < Solde acompte : Rien � payer  */
        then  assign  valeur[  9 ] = ""                    /*  Echeance 1         */
                      valeur[ 10 ] = ""                    /*  Echeance 2         */
                      valeur[ 26 ] = ""                    /*  Mode Reg.          */
                      valeur[ 28 ] = ""                    /*  Reste a Payer      */
                      valeur[ 29 ] = ""                    /*  Reste a Payer EUR  */
                      valeur[ 27 ] = "PAIEMENT ANTICIPE" . /*  Paiement Anticipe  */
        else  do :  /*  Montant facture > Solde acompte : Reste � payer  */
            assign  valeur[ 76 ] = "DEJA REGLE"
                    valeur[ 77 ] = string( solde-pf , msk72 )             /*  D�j� R�gl�   */
                    valeur[ 73 ] = string( ttc-fac - solde-pf , msk72 )   /*  Net � Payer  */
                    valeur[ 28 ] = string( ttc-fac - solde-pf , msk72 ) .

             if tx-frf-eur <> 0  then
                assign  valeur[ 29 ] = string( ( ttc-fac - solde-pf ) / tx-frf-eur, msk72 )
                        valeur[ 74 ] = string( ( ttc-fac - solde-pf ) / tx-frf-eur, msk72 )
                        valeur[ 78 ] = string( solde-pf / tx-frf-eur , msk72 ) .
        end .

    END .  /*  SOLDE-PF <> 0  */

    do i = 1 to 120 :
        if valeur[ i ] = "" then next .
        { maq-maj.i "15"  i  valeur[i] }
    end .

    do Ma-ligne = 1 to max-lig [ rg ] while edi-info :
        Ma-Edi [ ma-deblig [rg] + Ma-Ligne ]
          = Ma-edi [ ma-deblig [rg] + Ma-Ligne ]
          + fill ( " " , 130 - length ( Ma-edi [ ma-deblig [rg] + Ma-Ligne ] ))
          + zone-info .
    end .
    { maq-edi.i rg }

    /* if not edi-fond then run edit-comment-pied . */ /* Edition Commentaires societe */

    if not edi-info and not ok-fin
    then do :
        rg = 20 .
        { maq-edi.i rg }
    end .

    assign
        ma-edipied = ""
        ma-nblig   = 0
        code-tva   = ""
        taux-tva   = 0
        base-tva   = 0
        mtt-tva    = 0 .


/************************** P R O C E D U R E S ******************************/

/* Edition commentaire societe sur factures ou commentaire articles BIO */
PROCEDURE EDIT-COMMENT-PIED :

    if lookup( "#001", ma-posi[33] ) = 0 then return .

    FIND PROGLIB where proglib.motcle  = "FAC-MES"
                 and   proglib.codsoc  = codsoc-soc
                 and   proglib.etabli  = ""
                 and   proglib.clef    = ""
                 and   proglib.edition = ""
                 and   proglib.chrono  = 0
                 no-lock no-error .

    if available proglib
    then REPEAT :

        if art-bio and not comment-soc then leave .

        do i = 1 to 4 :
            if proglib.libelle[ i ] <> ""
            then do :
                comment-soc = yes .
                leave .
            end .
        end .

        if not comment-soc then leave .

        assign rg     = 34
               valeur = ""
               .
        do Ma-ligne = 1 to max-lig [ rg ] while edi-info :
            Ma-Edi [ ma-deblig [rg] + Ma-Ligne ]
                = Ma-edi [ ma-deblig [rg] + Ma-Ligne ]
                + fill ( " " , 130 - length ( Ma-edi [ ma-deblig [rg] + Ma-Ligne ] ) )
                + zone-info .
        end .
        { maq-edi.i rg }

        rg = 33 .
        do i = 1 to 4 while not edi-fond :
            { maq-maj.i "rg" 1 proglib.libelle[i] }
            do Ma-ligne = 1 to max-lig [ rg ] while edi-info :
                 Ma-Edi [ ma-deblig [rg] + Ma-Ligne ]
                     = Ma-edi [ ma-deblig [rg] + Ma-Ligne ]
                     + fill ( " " , 130 - length ( Ma-edi [ ma-deblig [rg] + Ma-Ligne ] ) )
                     + zone-info .
            end .
            { maq-edi.i rg }
        end .

        if edi-fond
        then do :

            do i = 1 to 4 :
                valeur[i] = proglib.libelle[i].
            end.

            { maq-maj1.i rg valeur }
            { maq-edi.i rg }

        end .

        leave .

    END . /* if available proglib */

    if art-bio and not comment-soc /* Edition commentaire lie aux articles BIO presents */
    then do :
        rg = 33 .
        run edit-comment-art . /* Edition Commentaires articles */
    end . /* if art-bio and not comment-soc */

END PROCEDURE . /* EDIT-COMMENT-PIED */

/* Edition commentaire articles BIO */
PROCEDURE EDIT-COMMENT-ART :

    valeur = "" .

    if edi-info
    then do :
        do Ma-ligne = 1 to max-lig [ rg ] :
            Ma-Edi [ ma-deblig [rg] + Ma-Ligne ]
                   = Ma-edi [ ma-deblig [rg] + Ma-Ligne ]
                   + fill ( " " , 130 - length ( Ma-edi [ ma-deblig [rg] + Ma-Ligne ] ) )
                   + zone-info .
        end .
        { maq-edi.i rg }
    end .

    do i-chp = 1 to 10 :
        if comment-edi[ i-chp ] = "" then next .
        case i-chp :
            when 1 then valeur[ i-chp ] = entry( 2 , libelle[ 75 ] ) + " " +
                                          comment-edi[ i-chp ] no-error .
            otherwise   valeur[ i-chp ] = comment-edi[ i-chp ] .
        end case .
    end .

    do i = 1 to 4 while not edi-fond :
        { maq-maj.i "rg" 1 valeur[i] }
        do Ma-ligne = 1 to max-lig [ rg ] while edi-info :
            Ma-Edi [ ma-deblig [rg] + Ma-Ligne ]
                   = Ma-edi [ ma-deblig [rg] + Ma-Ligne ]
                   + fill ( " " , 130 - length ( Ma-edi [ ma-deblig [rg] + Ma-Ligne ] ) )
                   + zone-info .
        end .
        { maq-edi.i rg }
    end .

    if edi-fond
    then do :
        { maq-maj1.i rg valeur }
        { maq-edi.i rg }
    end .

END PROCEDURE . /* EDIT-COMMENT-ART */

{ g-edichp.i } /* Edition Champs parametrables */



