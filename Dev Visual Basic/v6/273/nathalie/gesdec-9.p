/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/gesdec-9.p                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 292433 2019-10-03!Evo!Willy Petit-Mai![273][GC] Migration du lanceur de gestion de l'interface DECLIC       !
! 290554 2019-09-05!Evo!Jean Baptiste C![GC]Int�gration version 273                                           !
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V61 58302 29-02-16!Bug!pha            !Modif suite suppression du cadrage de code c�r�ale                    !
!                  !   !               !Copy en : 7.2                                                         !
!V61 39557 09-07-13!Evo!cch            !Ajout trt seulement des articles autoris�s pour �viter les rejets inut!
!V61 25426 06-07-11!Bug!cch            !Correction 'cermvt.sorent' affect� 2 fois                             !
!V61 22260 06-01-11!Evo!cch            !Suite fiche V61 22246                                                 !
!V61 22246 06-01-11!Evo!cch            !Ajout pourcentage de tol�rance sur contr�le solde quantit�s           !
!V61 22203 05-01-11!Bug!cch            !Correction mauvais chargement de la campagne                          !
!V61 21908 08-12-10!Bug!cch            !Correction divers champs non charg�s                                  !
!V61 21881 07-12-10!Bug!cch            !Correction probl�me calcul du solde entr�es - sorties                 !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !magasi                                          !
!gesdec-0.i            !                                     !auxapr                                          !
!aide.i                !                                     !auxili                                          !
!varlect.i             !                                     !tabdos                                          !
!pds-norm.i            !new                                  !cermvt                                          !
!l-parmvt.i            !                                     !cereal                                          !
!regs-cha.i            !                                     !tabcer                                          !
!regs-inc.i            !"AUXSPE"                             !                                                !
!l-cereal.i            !"cereal-soc[ i-i ]" "code-camp" "'001!                                                !
!regs-rec.i            !                                     !                                                !
!majmoucb.i            !b-cermvt                             !                                                !
!cadr-aux.i            !codsoc-soc type-tiers code-tiers     !                                                !
!calpdsno.i            !                                     !                                                !
!gesdec-p.i            !                                     !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
!_____________________________________________________________________________________________________________!
!                                                  P R O C E D U R E S                                        !
!_____________________________________________________________________________________________________________!
!CTRL-SOLDE            !                                                                                      !
&End
**************************************************************************************************************/
/******************************************************************************/
/* GESDEC-9.P - GESTION DES ECHANGES DE DONNEES NATHALIE / DECLIC             */
/*----------------------------------------------------------------------------*/
/* Integration Consommations TAF Apports             25/10/2010 - EUREA - 273 */
/******************************************************************************/
/* DGR 29/05/2018 : stockage poids aux normes                                 */

MESSAGE "-----------------------------------gesdec-9.p-----------------------------------"
VIEW-AS ALERT-BOX.

{ connect.i      }
{ gesdec-0.i     }
{ aide.i         }
{ varlect.i      }
{ pds-norm.i new }

def var l-codsoc     as char          no-undo .  
def var l-codcer     as char          no-undo .  
def var code-cer     as char          no-undo . 
def var signe        as char          no-undo . 
def var code-camp    as char          no-undo . 
def var code-periode as char          no-undo . 
def var no-ticket    as char          no-undo . 
def var auxspe-soc   as char extent 5 no-undo . 
def var cereal-soc   as char extent 5 no-undo . 
def var no-chrono    as int           no-undo . 
def var min-soc      as int           no-undo . 
def var max-soc      as int           no-undo . 
def var poids-sor    as dec           no-undo . 
def var poids-cum    as dec  extent 4 no-undo .
def var poids-cal    as dec  extent 2 no-undo .
def var poids-aff    as dec  extent 2 no-undo . 
def var date-sor     as date          no-undo . 

/* Ajout par CCO le 7/09/2016 */
def var wtabcernb21  like tabcer.nombre[ 21 ].
def var wtabcernb22  like tabcer.nombre[ 22 ].
def var wtabcerlib15 like tabcer.libel1[5].

def new shared buffer b-cermvt for cermvt .

run trt-init . /* Initialisations Fichiers */
if not trt-ok then return .

type-tiers = typaux-def .

do i-i = 1 to 4 :
    zone = entry( 1, l-cereale[ i-i ], "/" ) .
    if zone = "" then next .
    l-codcer = l-codcer + "," + zone .
end .
substr( l-codcer, 1, 1 ) = "" .

/* Verif. Code Mouvement Apport a generer */        
assign zone = ""   
       zone = entry( 2, l-codmvt[ 2 ] ) no-error .

lect-tab = zone .

{ l-parmvt.i }

if not available tabcer or zone = ""
then do :
    assign trt-ok   = no
           mess-err = zone + libelle[ 182 ]
           .
    run trt-rejet .
end .

/* Ajout par CCO le 7/09/2016 */
wtabcernb21  =    tabcer.nombre[ 21 ].
wtabcernb22  =    tabcer.nombre[ 22 ].
wtabcerlib15 =    tabcer.libel1[5].

/* Verif. Magasin */        
do tour-soc = 1 to num-entries( l-soc-appo ) :

    codsoc-soc = entry( tour-soc, l-soc-appo ) .

    regs-app = "NATHALIE" .
    { regs-cha.i }

    { regs-inc.i "AUXSPE" }
    auxspe-soc[ tour-soc ] = regs-soc .

    { regs-inc.i "CEREAL" }
    cereal-soc[ tour-soc ] = regs-soc .

    { regs-inc.i "MAGASI" }

    FIND MAGASI where magasi.codsoc = regs-soc
                and   magasi.codtab = mag-echcer
                no-lock no-error .

    if not available magasi or mag-echcer = ""
    then do :
        assign trt-ok   = no
               mess-err = mag-echcer + libelle[ 183 ] + regs-soc + " ..." .
               .
        run trt-rejet .
        leave .
    end .

end .

/* Lecture et Traitement du Fichier */
TRAITEMENT :
REPEAT while trt-ok :

    import stream s-fic unformatted enreg .

    if nb-lu modulo( 50 ) = 0
    then do :
        hide message no-pause .
        message libelle[ 31 ] nb-lu " - " libelle[ 32 ] nb-trt .
        /*readkey pause 0 .*/
    end .

    nb-lu = nb-lu + 1 .

    if ( ( enreg begins "/*" or enreg begins chr( 27 ) ) and trt-rejet ) or enreg = "" then next .

    code-cer = right-trim( substr( enreg, 33, 10 ) ) .
    if lookup( code-cer, l-codcer ) = 0 then next . /* Cereale non traitee */

    /* Controles des zones */
    assign zone      = substr( enreg, 23, 8 )
           date-sor  = date( int( substr( zone, 5, 2 ) ),
                             int( substr( zone, 7, 2 ) ),
                             int( substr( zone, 1, 4 ) ) ) no-error
           .

    if error-status:error
    then do :
        mess-err = zone + libelle[ 162 ] .
        run trt-rejet .
        next .
    end .

    assign zone      = substr( enreg, 43, 9 )
           poids-sor = dec( zone ) / 1000 no-error
           .
    if error-status:error or poids-sor = ? or poids-sor <= 0
    then do :
        mess-err = zone + libelle[ 164 ] .
        run trt-rejet .
        next .
    end .

    signe = substr( enreg, 52, 1 ) .
    if lookup( signe, l-signe ) = 0
    then do :
        mess-err = signe + libelle[ 180 ] .
        run trt-rejet .
        next .
    end .

    if signe = entry( 1, l-signe ) then poids-sor = - poids-sor .

    /* Determination Campagne et Periode */
    if month( date-sor ) >= 07 then assign code-camp    = string( year( date-sor ), "9999" )
                                           code-periode = string( month( date-sor ) - 6, "999" ) 
                                           .
                               else assign code-camp    = string( year( date-sor ) - 1, "9999" )
                                           code-periode = string( month( date-sor ) + 6, "999" ) 
                                           .

    /* Determination Societe(s) et poids pour generation mouvement consommation TAF */
    assign codsoc-soc = right-trim( substr( enreg, 13, 2 ) )
           code-tiers = right-trim( substr( enreg, 15, 8 ) )
           .

    case codsoc-soc :

        when entry( 1, l-soc-appo ) or when entry( 2, l-soc-appo ) then do : /* Societe 01 ou 03 */

            assign min-soc = 1
                   max-soc = 2
                   .

            run ctrl-solde . /* Controle Solde Entrees - Sorties suffisant */

            if mess-err <> "" then next .

            if poids-cal[ 1 ] >= poids-cal[ 2 ]
            then do :
                l-codsoc = entry( 1, l-soc-appo ) . /* Soc. 01 */
                if poids-cal[ 1 ] >= poids-sor then poids-aff[ 1 ] = poids-sor .
                                               else assign l-codsoc       = l-codsoc + "," + entry( 2, l-soc-appo ) /* + Soc. 03 */
                                                           poids-aff[ 1 ] = poids-cal[ 1 ]
                                                           poids-aff[ 2 ] = poids-sor - poids-cal[ 1 ]
                                                           .
            end .
            else do :
                l-codsoc = entry( 2, l-soc-appo ) . /* Soc. 03 */
                if poids-cal[ 2 ] >= poids-sor then poids-aff[ 1 ] = poids-sor .
                                               else assign l-codsoc       = l-codsoc + "," + entry( 1, l-soc-appo ) /* + Soc. 01 */
                                                           poids-aff[ 1 ] = poids-cal[ 2 ]
                                                           poids-aff[ 2 ] = poids-sor - poids-cal[ 2 ]
                                                           .
            end .

        end . /* when entry( 1, l-soc-appo ) or when entry( 1, l-soc-appo ) then do : */

        when entry( 3, l-soc-appo ) then do : /* Societe 08 */

            assign min-soc = 3
                   max-soc = 3
                   .

            run ctrl-solde . /* Controle Solde Entrees - Sorties suffisant */

            if mess-err <> "" then next .

            assign l-codsoc       = codsoc-soc
                   poids-aff[ 1 ] = poids-sor
                   .

        end . /* when entry( 3, l-soc-appo ) then do : */

        otherwise do :
            mess-err = substr( code-tiers, 1, 2 ) + libelle[ 63 ] .
            run trt-rejet .
            next .
        end .

    end case . /* case codsoc-soc : */

    nb-trt = nb-trt + 1 .

    if not trt-maj then next . /* Test -> Pas de MAJ */    

    /* Generation Mouvement Apport */
    GENE-MVT :
    DO TRANSACTION :

        do tour-soc = 1 to num-entries( l-codsoc ) :

            assign codsoc-soc = entry( tour-soc, l-codsoc )
                   i-i        = lookup( codsoc-soc, l-soc-appo )
                   .

            /* Verif. Cereale */
            { l-cereal.i "cereal-soc[ i-i ]" "code-camp" "'001'" "code-cer" "libel" }

            if not available cereal
            then do :
                mess-err = code-camp + " - " + code-cer + libelle[ 67 ] + cereal-soc[ i-i ] .
                run trt-rejet .
                undo GENE-MVT, leave GENE-MVT .
            end .

            /* Verif. Tiers */
            FIND AUXAPR where auxapr.codsoc = auxspe-soc[ i-i ]
                        and   auxapr.typaux = type-tiers
                        and   auxapr.codaux = code-tiers
                        no-lock no-error .

            if not available auxapr
            then do :
                mess-err = type-tiers + " " + trim( code-tiers ) + libelle[ 64 ] + auxspe-soc[ i-i ] + " ..." .
                run trt-rejet .
                undo GENE-MVT, leave GENE-MVT .
            end .

            regs-app = "ELODIE" .
            { regs-cha.i }

            regs-fileacc = "AUXILI" + type-tiers .
            { regs-rec.i }

            FIND AUXILI where auxili.codsoc = regs-soc
                        and   auxili.typaux = type-tiers
                        and   auxili.codaux = code-tiers
                        no-lock no-error .

            if not available auxili
            then do :
                mess-err = type-tiers + " " + trim( code-tiers ) + libelle[ 65 ] + regs-soc + " ..." .
                run trt-rejet .
                undo GENE-MVT, leave GENE-MVT .
            end .

            /* Recup. Chrono */
            REPEAT : 

                FIND TABDOS where tabdos.codsoc = codsoc-soc
                            and   tabdos.etabli = mag-echcer 
                            and   tabdos.typtab = "CER"
                            and   tabdos.prefix = "PLUSUN"
                            and   tabdos.codtab = code-camp
                            exclusive-lock no-error .

                if not available tabdos
                then do :

                    CREATE TABDOS .

                    assign tabdos.codsoc = codsoc-soc
                           tabdos.etabli = mag-echcer
                           tabdos.typtab = "CER"
                           tabdos.prefix = "PLUSUN"
                           tabdos.codtab = code-camp
                           .

                end .

                tabdos.nombre[ 1 ] = tabdos.nombre[ 1 ] + 1 .

                FIND CERMVT where cermvt.codsoc   = codsoc-soc
                            and   cermvt.codmag   = mag-echcer
                            and   cermvt.campagne = code-camp
                            and   cermvt.periode  = code-periode
                            and   cermvt.datsai   = date-sor
                            and   cermvt.chrono   = int( tabdos.nombre[ 1 ] )
                            no-lock no-error .

                if available cermvt then next .   

                no-chrono = int( tabdos.nombre[ 1 ] ) .

                leave .

            END . /* REPEAT : */

            /* Recup. No Ticket */
            FIND TABDOS where tabdos.codsoc = codsoc-soc
                        and   tabdos.etabli = ""
                        and   tabdos.typtab = "MAG"
                        and   tabdos.prefix = "NO-TICKET"
                        and   tabdos.codtab = mag-echcer
                        exclusive-lock no-error .

            if not available tabdos
            then do :

                CREATE TABDOS .

                assign tabdos.codsoc = codsoc-soc
                       tabdos.etabli = ""
                       tabdos.typtab = "MAG"
                       tabdos.prefix = "NO-TICKET"
                       tabdos.codtab = mag-echcer
                       .
            end .

            assign tabdos.nombre[ 1 ] = tabdos.nombre[ 1 ] + 1
                   no-ticket          = string( tabdos.nombre[ 1 ], "99999999") + "00"
                   overlay( no-ticket, 1, length( trim( mag-echcer ) ) ) = trim( mag-echcer )
                   .

            /* Creation Mouvement */
            CREATE B-CERMVT .

            assign nb-cre[ 1 ]           = nb-cre[ 1 ] + 1
                   b-cermvt.codsoc       = codsoc-soc
                   b-cermvt.codmag       = mag-echcer
                   b-cermvt.campagne     = code-camp
                   b-cermvt.periode      = code-periode 
                   b-cermvt.datsai       = date-sor
                   b-cermvt.chrono       = no-chrono
                   b-cermvt.no-ticket    = no-ticket
                   b-cermvt.codmvt       = entry( 2, l-codmvt[ 2 ] )
                   b-cermvt.typaux       = type-tiers
                   b-cermvt.codaux       = code-tiers
                   b-cermvt.semence      = cereal.semence
                   b-cermvt.cer-regroup  = cereal.cer-regroup
                   b-cermvt.codcer       = cereal.codcer
                   b-cermvt.sorent       = wtabcernb21                   
                   b-cermvt.typsto       = wtabcerlib15         
                   b-cermvt.typbon       = wtabcernb22        
                   b-cermvt.pds-brut     = poids-aff[ tour-soc ]
                   b-cermvt.pds-norme    = poids-aff[ tour-soc ]        /* DGR 29/05/2018 */
                   b-cermvt.acquit       = substr( enreg, 4, 6 )
                   b-cermvt.region       = "001"
                   b-cermvt.env-depot    = "1"
                   b-cermvt.typsto       = "FA"
                   .

            case int( wtabcernb22         ) :
                when 1 then b-cermvt.typfac = "V" .
                when 2 then b-cermvt.typfac = "A" .
            end case .

            do i-i = 1 to 14 :
                b-cermvt.cod-etat[ i-i ] = cereal.lib-etat[ i-i ] .
            end .

            { majmoucb.i b-cermvt }

            run gesdec91.p . /* MAJ Stock */

        end . /* do tour-soc = 1 to num-entries( l-codsoc ) : */

    END . /* GENE-MVT */

END . /* REPEAT IMPORT */

hide message no-pause .

run trt-fin . /* Traitements de fin sur les fichiers */

message trim( libelle[ 17 ] ) fic-trt skip
        libelle[ 31 ]  nb-lu          skip
        libelle[ 32 ]  nb-trt         skip
        libelle[ 33 ]  nb-err         skip
        libelle[ 184 ] nb-cre[ 1 ]
        view-as alert-box information .


/*************************** P R O C E D U R E S ******************************/

/* Controle Solde Entrees - Sorties suffisant */
PROCEDURE CTRL-SOLDE :

    assign j-j       = 0
           poids-cum = 0
           .

    do tour-soc = min-soc to max-soc : /* Cumuls poids campagne entrees et sorties TAF */

        codsoc-soc = entry( tour-soc, l-soc-appo ) .

       /* { c-cereal.i code-cer }   */                      /* Cadrage Code Cereale */

        regs-app = "ELODIE" .
        { regs-cha.i }

        { cadr-aux.i codsoc-soc type-tiers code-tiers } /* Cadrage Tiers */

        do i-i = 1 to 2 :

            j-j = j-j + 1 .

            FOR EACH CERMVT where cermvt.codsoc   = codsoc-soc
                            and   cermvt.campagne = code-camp
                            and   cermvt.codcer   = code-cer
                            and   cermvt.typaux   = type-tiers
                            and   cermvt.codaux   = code-tiers
                            and   cermvt.codmvt   = entry( i-i, l-codmvt[ 2 ] )
                            no-lock use-index codcer :

                { l-cereal.i "cereal-soc[ tour-soc ]" "cermvt.campagne" "cermvt.region" "cermvt.codcer" "lect-lib" }

                if not available cereal
                then do :
                    mess-err = code-camp + " - " + code-cer + libelle[ 67 ] + cereal-soc[ tour-soc ] .
                    run trt-rejet .
                    return .
                end .

                { calpdsno.i }

                poids-cum[ j-j ] = poids-cum[ j-j ] + pds-norm .

            END .

        end . /* do i-i = 1 to 2 : */

    end . /* do tour-soc = min-soc to max-soc : */

    assign poids-cal[ 1 ] = poids-cum[ 1 ] - poids-cum[ 2 ] /* Entrees - Sorties Soc. 01 et 08 */
           poids-cal[ 2 ] = poids-cum[ 3 ] - poids-cum[ 4 ] /* Entrees - Sorties Soc. 03       */
           .

    if poids-cal[ 1 ] + poids-cal[ 2 ] < poids-sor /* Solde entrees - sorties < Poids a traiter */
    then do :

        if pcent-tol-taf > 0
        then do :
            val-num = poids-sor * ( pcent-tol-taf / 100 ) .
            if poids-sor - ( poids-cal[ 1 ] + poids-cal[ 2 ] ) <= val-num
            then do :
                if poids-cal[ 2 ] = 0
                   then poids-cal[ 1 ] = poids-sor .
                   else if poids-cal[ 1 ] = 0
                           then poids-cal[ 2 ] = poids-sor .
                return . /* Qte TAF - Solde TAF <= %age Tolerance -> Pas de rejet */
            end .
        end .

        mess-err = string( poids-cal[ 1 ] + poids-cal[ 2 ] ) + " < " + string( poids-sor ) + libelle[ 181 ] .
        run trt-rejet .
        return .

    end .

    else mess-err = "" .

END PROCEDURE . /* CTRL-SOLDE */

{ gesdec-p.i } /* Procedures Communes */




