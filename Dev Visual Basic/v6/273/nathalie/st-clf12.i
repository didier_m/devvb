/*==========================================================================*/
/*                          S T - C L F 1 2 . I                             */
/* Statistique Vente Client                                                 */
/* Etat des CA / Depot / Type Client / Famille                              */
/*==========================================================================*/
/*           Zones utilise pour le module edition des ruptures              */
/*                Rupture-a               : Niveau de rupture en cours      */
/*                Rupture-nb              : Nombre de rupture existante     */
/*                Rupture-etat = "t"      : Edition du titre de la rupture  */
/*                Rupture-etat = "r"      : Edition de la rupture           */
/*--------------------------------------------------------------------------*/
 
/* Entete MAGASIN    */
/*===================*/
DO WHILE (RUPTURE-ETAT = "T" AND RUPTURE-A = 1 ) :
 
    if substring( zone , 1 , 3 ) <> " " then do :
        hide message . pause 0 .
        message "  Traitement du magasin ...: "
                "  " substring( zone , 1 , 3 ) .
    end .
 
    assign lib-tit = libelle[31]
           no-mag      = substring( zone , 1 , 3 )
           lib-mag     = " "
           nb-cli      = 0
           b-nat       = 0
           cod-nat     = " "
           nom-nat     = " "
           tot-nat     = 0
           ent-mag     = yes
           .
 
    find magasi where magasi.codsoc = magasi-soc
                  and magasi.codtab = no-mag
                      no-lock  no-error .
    if available magasi then lib-mag  = magasi.libel1[mem-langue] .
 
    leave .
 
END .
 
 
/*    Entete NATURE                  */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 2 :
 
    assign    lib-tit1    = libelle[ 32 ]
              no-nature   = substring( zone, 4, 1 )
              lib-nature  = " "
              ent-nat     = yes
              nb-cli      = 0 .
 
    case no-nature :
        when "1" then lib-nature = libelle[ 33 ] .
        when ""  then lib-nature = libelle[ 34 ] .
        when "?" then lib-nature = libelle[ 35 ] .
    end case .
     
    /* Preparation des totaux recap */
    assign  b-nat  = b-nat + 1
            cod-nat[ b-nat ] = no-nature
            nom-nat[ b-nat ] = lib-nature  .
 
    leave .
 
END .
 
/*    Entete CLIENT                  */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 3 :
 
    assign    no-client   = stacli.codaux
              lib-client  = " "
              ent-cli     = yes
              nb-fam      = 0 .
 
    regs-fileacc = "AUXILI" + stacli.typaux.
    {regs-rec.i}
    find auxili where auxili.codsoc = regs-soc
                  and auxili.typaux = stacli.typaux
                  and auxili.codaux = stacli.codaux
                  no-lock no-error .
    if not available auxili then lib-client = "INEXISTANT".
    else lib-client = auxili.adres[ 1 ] .
 
    leave .
 
END .
 
 
/*    Entete FAMILLE    */
/*======================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 4 :
 
 
    assign no-fam     = substring( zone, 33, 3 )
           lib-fam    = " " .
 
    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "ART"
                and   tabges.prefix = "FAMART"
                and   tabges.codtab = substring( zone, 33, 3)
                no-lock  no-error .
    if available tabges then lib-fam    = tabges.libel1[mem-langue] .
 
    leave .
END .
 
 
/*=========================================================================*/
 
/*    Total Recid      */
/*=====================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 5 :
 
    /* CUMUL des QUANTITE suivant date      */
    /*--------------------------------------*/
    am-sta = int( stacli.annee ) * 100 .
 
    do i = 1 to 12 :
        if am-sta + i >= am-deb and am-sta + i <= am-fin
        then assign tot-ht[ 1 ]  = tot-ht[ 1 ]  + stacli.fac-ht[ i ]
                    tot-nat[ b-nat ] = tot-nat[ b-nat ] + stacli.fac-ht[ i ] .
 
        if am-sta + i >= am-deb-1 and am-sta + i <= am-fin-1
        then assign tot-ht[ 2 ]  = tot-ht[ 2 ]  + stacli.fac-ht[ i ]
            tot-nat[ b-nat + 10 ] = tot-nat[ b-nat + 10 ] + stacli.fac-ht[ i ] .
    end .
 
    leave .
 
END .
 
                                               /* Imprime entete que si :   */
/*    Total FAMILLE      Articles   */         /*  au moins 1 ligne non nul */
/*==================================*/         /*---------------------------*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 4 :
 
   do while tot-ht[ 1 ] <> 0 or tot-ht[ 2 ] <> 0 :
       if ent-mag  then run value( ma-entete ) .
       if ent-nat
       then do :
           assign edit[ 11 ] = lib-tit1
                  edit[ 12 ] = no-nature
                  edit[ 13 ] = lib-nature .
 
           do i = 11 to 13 :
               { maq-maj.i  03  i  edit[i] }
               edit[i] = " " .
           end .
 
           { maq-edi.i  03 }
       end .
 
       ent-nat = no .
 
       assign ind-tot = 00
              nb-fam = nb-fam + 1 .
       run st-clf1t.p .
 
       ent-mag  = no .
       leave .
   end .
 
   leave .
END .
 
 
/*    Total  Client             */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 3 :
 
   do while nb-fam <> 0 :
       assign lib-tot = libelle[ 41 ]  + no-client
              ind-tot = 10
              nb-cli  = nb-cli + 1.
 
       run st-clf1t.p .
       leave .
   end .
 
   leave .
END .
 
 
/*    Total  NATURE             */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 2 :
 
   do while nb-cli <> 0 :
       assign lib-tot = libelle[ 42 ]  + lib-nature
              ind-tot = 20
              nb-nat  = nb-nat + 1.
 
       run st-clf1t.p .
       leave .
   end .
 
   leave .
END .
 
/*    Total DEPOT   */
/*==================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 1 :
 
   do while nb-nat <> 0 :
       assign   lib-tot   = libelle[ 43 ]  + no-mag
                lib-tot3  = libelle[ 45 ]  + no-mag
                ind-tot   = 30
                nb-mag    = nb-mag  + 1 .
 
       run st-clf1t.p .
       leave .
   end .
 
   leave .
END .