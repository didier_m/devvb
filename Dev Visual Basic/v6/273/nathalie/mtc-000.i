/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/mtc-000.i                                                                  !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 22760 08-02-11!Bug!cch            !Correction probl�me de suppression � tord des enreg. non valid�s      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*=======================================================================*/
/*                             M T C - 0 0 0 . I                         */
/*          Mise a Jour du Fichier TARCLI a partir de TARPRE             */
/*=======================================================================*/

def var ii                     as int                            no-undo . 
def var jj                     as int                            no-undo .      
def var xx1                    as char                           no-undo .      
def var xx2                    as char                           no-undo .      
def var lib-tarif              as char   format "x(30)"          no-undo .      
def var lib-fourn              as char   format "x(30)"          no-undo .      
def var lib-catego             as char   format "x(30)"          no-undo .      
def var lib-sup                as char   format "x(20)"          no-undo .      

def {1} shared var code-tarif  like {tarcli.i}.code-tarif        no-undo .     
def {1} shared var code-fourn  as char   format "x(9)"           no-undo .     
def {1} shared var categorie   as char   format "x(6)"           no-undo .     
def {1} shared var date-appli  as date   format "99/99/9999"     no-undo .     
def {1} shared var sup-prepa   as log    format "O/N"            no-undo .      
def {1} shared var valida      as char   format "x"              no-undo .      
def {1} shared var famart-mini as char   format "x(3)"           no-undo .      
def {1} shared var famart-maxi as char   format "x(3)"           no-undo .      
def {1} shared var soufam-mini as char   format "x(3)"           no-undo .      
def {1} shared var soufam-maxi as char   format "x(3)"           no-undo .      
def {1} shared var ctv-motcle  like {tarcli.i}.motcle init "VTE" no-undo .     
def {1} shared var codtar-cli  like {tarcli.i}.code-tarif        no-undo .      
def {1} shared var codtar-rev  like {tarcli.i}.code-tarif        no-undo .      
def {1} shared var libelle     as char   extent 49               no-undo .      

def {1} shared buffer articl for artapr .

def {1} shared frame fr-saisie .

form " " skip
     libelle[ 11 ] format "x(27)"  code-tarif lib-tarif  skip(1)
     libelle[ 12 ] format "x(27)"  code-fourn lib-fourn  skip(1)
     libelle[ 13 ] format "x(27)"  categorie  lib-catego skip(1)
     libelle[ 14 ] format "x(27)"  date-appli            skip(1)
     libelle[ 15 ] format "x(27)"  sup-prepa  lib-sup    skip(1)
     libelle[ 16 ] format "x(27)"  valida                skip(1)
     with frame fr-saisie
     row 4 centered overlay no-label {v6frame.i} .


