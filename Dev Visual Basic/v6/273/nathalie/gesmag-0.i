/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/gesmag-0.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 16706 07-03-08!Evo!cch            !Suite fiche 16592 : mise au point suite tests par DEAL                !
!V52 16653 13-02-08!Evo!cch            !Gestion de la R.P.D. venant des bons magasins                         !
!V52 16592 24-01-08!Evo!cch            !Ajout Gestion de la modification des commandes CCC et CCI             !
!V52 16478 30-11-07!Evo!cch            !Ajout gestion des bons Litiges ( LIT ) et Retour ( RET )              !
!V52 16205 05-09-07!Bug!cch            !REI et REC issus des magasins non mis en attente                      !
!V52 16162 21-08-07!Evo!cch            !Initialisation par d�faut statut EUREA DISTRI sans passage sur la zone!
!V52 16032 03-07-07!Evo!cch            !R�ceptions des CDF S.D. faites en magasin et non plus au si�ge        !
!V52 15928 04-06-07!Evo!cch            !Modifications sur site EUREA semaine 200721                           !
!V52 15900 21-05-07!Evo!cch            !Ajout gestion des receptions RAF sur commandes CAF                    !
!V52 15673 08-02-07!Evo!cch            !Ajout dans param�trage des codes mouvements � �liminer                !
!V52 15571 10-01-07!Bug!cch            !Correction nveaux pbles avec enreg. fin ( XXX ) + pble rejet si erreur!
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/* GESMAG-0.I - GESTION DE L'INTEGRATION DES BONS MAGASINS                    */
/*----------------------------------------------------------------------------*/
/* Definitions Communes                                 02/2006 - EUREA - 273 */
/******************************************************************************/

def var i-param         as char          no-undo .
def var o-param         as char          no-undo .
def var repert-arriv    as char          no-undo .    
def var repert-traite   as char          no-undo .              
def var repert-rejet    as char          no-undo .              
def var repert-trav     as char          no-undo .              
def var l-fic           as char          no-undo .                             
def var l-ext           as char          no-undo .                             
def var l-soc           as char          no-undo .                             
def var l-typenr        as char          no-undo .                             
def var l-codmvt        as char extent 3 no-undo .                            
def var l-typcom-cde    as char          no-undo .                             
def var l-typcom-liv    as char          no-undo .                             
def var l-enr-deb-fin   as char          no-undo .
def var fic-rac         as char extent 2 no-undo .    
def var fic-ren         as char          no-undo .    
def var fic-blo         as char extent 2 no-undo .    
def var ext-trt         as char          no-undo .                             
def var ext-tst         as char          no-undo .                             
def var ext-rej         as char          no-undo .                             
def var ext-blo         as char          no-undo .                             
def var ext-log         as char          no-undo .                             
def var operat-maj      as char          no-undo .                             
def var nomimp          as char          no-undo .             
def var date-j          as char          no-undo .             
def var mess-lib        as char          no-undo .    
def var mess-err        as char          no-undo .    
def var mess-err-2      as char          no-undo .    
def var enreg           as char          no-undo .    
def var zone            as char          no-undo .    
def var transp          as char          no-undo .    
def var chauffeur       as char          no-undo .    
def var mode-liv        as char          no-undo .    
def var repres          as char          no-undo .    
def var type-aux        as char          no-undo .    
def var code-aux        as char          no-undo .    
def var code-art        as char          no-undo .    
def var code-ech        as char          no-undo .    
def var motcle-trt      as char          no-undo .    
def var typcom-trt      as char          no-undo .    
def var numbon-trt      as char          no-undo .    
def var l-param-def     as char          no-undo .      
def var proglib-clef    as char          no-undo .
def var proglib-edition as char          no-undo .
def var top-solde       as char          no-undo .
def var tiers-ed        as char          no-undo .
def var l-code-tar-2    as char extent 2 no-undo .
def var code-tva        as char          no-undo .
def var ok-trt          as log           no-undo .      
def var ok-trt-lig      as log           no-undo .    
def var ok-edit         as log           no-undo .
def var maj-entete      as log           no-undo .
def var val-date        as date          no-undo .    
def var date-bon        as date          no-undo .    
def var date-dep        as date          no-undo .    
def var date-liv        as date          no-undo .    
def var date-ech        as date          no-undo .
def var ind             as int           no-undo .    
def var no-tour         as int           no-undo .    
def var nb-enr-lu       as int           no-undo .    
def var nb-enr-trt      as int           no-undo .
def var no-enreg        as int           no-undo .    
def var val-int         as int           no-undo .    
def var niveau-trt      as int           no-undo .    
def var chrono-trt      as int           no-undo .    
def var pos-code-tar    as int           no-undo .
def var lg-code-tar     as int           no-undo .
def var val-dec         as dec           no-undo .    
def var quantite        as dec           no-undo .    
def var prix            as dec           no-undo .    
def var montant         as dec           no-undo .    
def var montant-2       as dec           no-undo .    
def var taux-esco       as dec           no-undo .    
def var tx-tva          as dec           no-undo .

def {1} shared var libelle                as char  extent 170                no-undo .
def {1} shared var lib-persomix           as char                            no-undo .
def {1} shared var l-art-persomix         as char                            no-undo .
def {1} shared var l-art-fr-po            as char                            no-undo .
def {1} shared var l-art-esc              as char                            no-undo .
def {1} shared var l-art-rpd              as char                            no-undo .
def {1} shared var codrem-rfa             as char                            no-undo .
def {1} shared var l-code-tar             as char  extent 2                  no-undo .
def {1} shared var l-carte-fid            as char                            no-undo .
def {1} shared var l-top-solde            as char                            no-undo .
def {1} shared var l-top-sup              as char                            no-undo .
def {1} shared var l-top-qte-lis          as char                            no-undo .
def {1} shared var l-top-o-n              as char                            no-undo .
def {1} shared var dev-gescom             as char                            no-undo .
def {1} shared var codmvt                 as char                            no-undo .    
def {1} shared var codtva-def             as char                            no-undo .
def {1} shared var codtva-exo             as char                            no-undo .
def {1} shared var typtva-exo             as char                            no-undo .
def {1} shared var even-pam               as char                            no-undo .
def {1} shared var magasi-soc             as char                            no-undo .
def {1} shared var articl-soc             as char                            no-undo .
def {1} shared var auxspe-soc             as char                            no-undo .
def {1} shared var proplatef-soc          as char                            no-undo .
def {1} shared var typaux-def             as char                            no-undo .
def {1} shared var typbon-dir             as char                            no-undo .
def {1} shared var typbon-mts             as char                            no-undo .
def {1} shared var typbon-pro             as char                            no-undo .
def {1} shared var typbon-std             as char                            no-undo .
def {1} shared var typbon-ter             as char                            no-undo .
def {1} shared var typbon-lit             as char                            no-undo .
def {1} shared var typbon-ret             as char                            no-undo .
def {1} shared var typaux-lis             as char                            no-undo .
def {1} shared var codaux-lis             as char                            no-undo .
def {1} shared var typaux-lisv            as char                            no-undo .
def {1} shared var codaux-lisv            as char                            no-undo .
def {1} shared var typaux-ed              as char                            no-undo .
def {1} shared var codaux-ed              as char                            no-undo .
def {1} shared var codsoc-ed              as char                            no-undo .
def {1} shared var fic-log                as char                            no-undo .
def {1} shared var fic-trt                as char                            no-undo .
def {1} shared var fic-rej                as char             format "x(50)" no-undo .
def {1} shared var motcle-acp             as char                            no-undo .
def {1} shared var motcle-ach             as char                            no-undo .
def {1} shared var motcle-vte             as char                            no-undo .
def {1} shared var typcom-car             as char                            no-undo .
def {1} shared var typcom-caf             as char                            no-undo .
def {1} shared var typcom-ccc             as char                            no-undo .
def {1} shared var typcom-cci             as char                            no-undo .
def {1} shared var typcom-cdf             as char                            no-undo .
def {1} shared var typcom-cdi             as char                            no-undo .
def {1} shared var typcom-cdm             as char                            no-undo .
def {1} shared var typcom-rea             as char                            no-undo .
def {1} shared var typcom-rec             as char                            no-undo .
def {1} shared var typcom-rem             as char                            no-undo .
def {1} shared var typcom-rgs             as char                            no-undo .
def {1} shared var typcom-trs             as char                            no-undo .
def {1} shared var typcom-tre             as char                            no-undo .
def {1} shared var typcom-cfi             as char                            no-undo .
def {1} shared var typcom-trc             as char                            no-undo .
def {1} shared var typcom-rcf             as char                            no-undo .
def {1} shared var typcom-rar             as char                            no-undo .
def {1} shared var typcom-raf             as char                            no-undo .
def {1} shared var typcom-cdl             as char                            no-undo .
def {1} shared var typcom-blf             as char                            no-undo .
def {1} shared var typcom-bls             as char                            no-undo .
def {1} shared var typcom-liv             as char                            no-undo .
def {1} shared var top-0-a                as char                            no-undo .
def {1} shared var top-1-a                as char                            no-undo .
def {1} shared var top-9-a                as char                            no-undo .
def {1} shared var top-non-a              as char                            no-undo .
def {1} shared var top-trt-ed             as char                            no-undo .
def {1} shared var niveau-cde             as char                            no-undo .
def {1} shared var niveau-liv             as char                            no-undo .
def {1} shared var niveau-val             as char                            no-undo .
def {1} shared var niveau-prefac          as char                            no-undo .
def {1} shared var niveau-fac             as char                            no-undo .
def {1} shared var niveau-sta             as char                            no-undo .
def {1} shared var tenusto-n-stock        as char                            no-undo .
def {1} shared var tenusto-stock          as char                            no-undo .
def {1} shared var tenusto-valeur         as char                            no-undo .
def {1} shared var codpri-ach             as char                            no-undo .
def {1} shared var codpri-cli             as char                            no-undo .
def {1} shared var codpri-dir             as char                            no-undo .
def {1} shared var codpri-htc             as char                            no-undo .
def {1} shared var codpri-pre             as char                            no-undo .
def {1} shared var codpri-gratuit         as char                            no-undo .
def {1} shared var codpri-attente         as char                            no-undo .
def {1} shared var codlig-normale         as char                            no-undo .
def {1} shared var codlig-avoir           as char                            no-undo .
def {1} shared var codent-solde           as char                            no-undo .
def {1} shared var codent-vac             as char                            no-undo .
def {1} shared var codent-val             as char                            no-undo .
def {1} shared var mvtsto-cdc             as char                            no-undo .
def {1} shared var mvtsto-cdf             as char                            no-undo .
def {1} shared var mvtsto-rgs             as char                            no-undo .
def {1} shared var mvtsto-trs             as char                            no-undo .
def {1} shared var mvtsto-tre             as char                            no-undo .
def {1} shared var mvtsto-liv             as char                            no-undo .
def {1} shared var mvtsto-rcf             as char                            no-undo .
def {1} shared var mvtsto-rcf-enc         as char                            no-undo .
def {1} shared var mvtsto-liv-enc         as char                            no-undo .
def {1} shared var trt-rejet              as log                             no-undo .   
def {1} shared var trt-test               as log                             no-undo .
def {1} shared var trt-eclat              as log                             no-undo .
def {1} shared var trt-integr             as log                             no-undo .
def {1} shared var trt-enchain            as log                             no-undo .
def {1} shared var mouchard               as log              format "O/N"   no-undo .
def {1} shared var maj-bon                as log                             no-undo .
def {1} shared var ok-fic-log             as log                             no-undo .
def {1} shared var ok-trt-bon             as log                             no-undo .
def {1} shared var nb-bon-cre             as int                             no-undo .
def {1} shared var nb-bon-maj             as int                             no-undo .
def {1} shared var nb-err                 as int                             no-undo .
def {1} shared var top-1-n                as int                             no-undo .
def {1} shared var top-3-n                as int                             no-undo .
def {1} shared var niveau-bon             as int   extent 2                  no-undo .
def {1} shared var tx-eur-frf             as dec                             no-undo .
def {1} shared var date-arrete            as date                            no-undo .
def {1} shared var date-arrete-vte        as date                            no-undo .
def {1} shared var date-fin-inv           as date                            no-undo .
def {1} shared var r-lignes-trav          as recid                           no-undo .

def stream s-fic-trt .

def {1} shared stream s-fic-rej .
def {1} shared stream kanal .

def {1} shared temp-table lignes-trav no-undo
    field codsoc    as char
    field magasin   as char
    field codmvt    as char
    field numbon    as char
    field chrono    as char
    field ss-chrono as int
    field no-enreg  as int
    field enreg     as char
    index i-lignes-1 codsoc magasin codmvt numbon chrono ss-chrono .

def buffer b-lignes-trav for lignes-trav .