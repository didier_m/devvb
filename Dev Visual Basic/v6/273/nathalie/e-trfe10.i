/*==========================================================================*/
/*                          E - T R F E 1 0 . I                             */
/*                   Edition  Etiquettes  Tarif  GENCOD                     */
/* Variables et Frames                                    ( CC 21/09/2001 ) */
/*==========================================================================*/

def var i         as int                 no-undo .
def var lib-tarif as char format "x(30)" no-undo .

def {1} shared var libelle        as char extent 60           no-undo .
def {1} shared var t-pf1          as char initial "pf1,"      no-undo .
def {1} shared var code-tarif     as char format "xxx"        no-undo .
def {1} shared var date-mini      as date format "99/99/9999" no-undo .
def {1} shared var date-maxi      as date format "99/99/9999" no-undo .
def {1} shared var magasins       as char format "x(51)"      no-undo .
def {1} shared var mag-exclus     as char                     no-undo .
def {1} shared var periph         as char format "x(12)"      no-undo .
def {1} shared var sav-periph     as char                     no-undo .
def {1} shared var magasi-soc     as char                     no-undo .
def {1} shared var list-soc       as char                     no-undo .
def {1} shared var anc-code-tarif as char                     no-undo .
def {1} shared var top-complet    as char FORMAT "x" INIT "N" no-undo .


def {1} shared frame fr-saisie .

form libelle[ 01 ] format "x(23)" code-tarif lib-tarif skip
     libelle[ 06 ] FORMAT "X(23)" top-complet          SKIP
     libelle[ 02 ] format "x(23)" date-mini            skip
     libelle[ 03 ] format "x(23)" date-maxi            skip
     libelle[ 04 ] format "x(23)" magasins             skip
     libelle[ 05 ] format "x(23)" periph
     with frame fr-saisie
     row 3 centered overlay no-label {v6frame.i} .
