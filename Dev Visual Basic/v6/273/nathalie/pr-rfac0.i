/*=======================================================================*/
/*                           P R - R F A C 0 . I                         */
/*                       Preparation Re-Facturations                     */
/*                           Variables Partagees                         */
/*=======================================================================*/

def {1} shared buffer  b-entete  for  entete .
def {1} shared buffer  b-lignes  for  lignes .
def {1} shared buffer  b-articl  for  artapr .

def {1} shared var libelle     as char                 extent 49    no-undo .

def {1} shared var date-mini   as date format "99/99/9999"          no-undo .
def {1} shared var date-maxi   as date format "99/99/9999"          no-undo .
def {1} shared var valida      as char format "x"                   no-undo .

def {1} shared var sav-codsoc  like entete.codsoc                   no-undo .

def {1} shared var motcle      like entete.motcle                   no-undo .
def {1} shared var typcom      like entete.typcom                   no-undo .
def {1} shared var nocom       like entete.numbon                   no-undo .
def {1} shared var typox       like entete.typaux                   no-undo .
def {1} shared var codox       like entete.codaux                   no-undo .
def {1} shared var magrsp      like entete.mag-respon               no-undo .
def {1} shared var magmvt      like entete.magasin                  no-undo .
def {1} shared var ref-tiers   like entete.ref-tiers                no-undo .

def {1} shared var i-i         as int                               no-undo .
def {1} shared var a-a         as int                               no-undo .
def {1} shared var x-x         as char                              no-undo .
def {1} shared var x-y         as char                              no-undo .

def {1} shared var bareme-cli  as char                              no-undo .
def {1} shared var clitar      as char                              no-undo .
def {1} shared var specif-tar  as char                              no-undo .
def {1} shared var specif-rem  as char                              no-undo .
def {1} shared var arguments   as char                              no-undo .

def {1} shared var cde-rang    as int                               no-undo .

def {1} shared var zone-tva    as char                              no-undo .
def {1} shared var taux-tva    as dec                               no-undo .
def {1} shared var mont-lig    as dec                               no-undo .
def {1} shared var tot-ttc     as dec                               no-undo .
def {1} shared var tot-ht      as dec                               no-undo .
def {1} shared var tot-pds     as dec                               no-undo .

def {1} shared var sav-pu-brut like lignes.pu-brut                  no-undo .

def {1} shared var entete-recid    as recid                         no-undo .