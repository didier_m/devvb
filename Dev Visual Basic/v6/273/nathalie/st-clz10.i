/*==========================================================================*/
/*                          S T - C L Z 1 0 . I                             */
/* Statistique Vente Client                                                 */
/* Etat des Qtes Vendues / Zones / Client / Categorie                       */
/*==========================================================================*/
 
/* Tbl des Libelles Etat & Tbl d'Edition & enreg fichier tri */
def {1} shared var libelle       as char                 extent 99   no-undo .
def {1} shared var edit          as char                 extent 49   no-undo .
def {1} shared var mask          as char                 extent 130  no-undo .
def {1} shared var zone          as char                             no-undo .
def {1} shared stream maquette.                             /* canal edition*/
def {1} shared stream kan-tri .
 
/* zone de traitement */
def {1} shared var recid-stacli as recid                           no-undo .
def {1} shared var nb-fam       as int                             no-undo .
def {1} shared var nb-cli       as int                             no-undo .
def {1} shared var nb-zone      as int                             no-undo .
def {1} shared var nb-super     as int                             no-undo .
 
def {1} shared var no-super     as char                            no-undo .
def {1} shared var no-zone      as char                            no-undo .
def {1} shared var lib-zone     as char                            no-undo .
def {1} shared var no-client    as char                            no-undo .
def {1} shared var lib-client   as char                            no-undo .
def {1} shared var no-catego    as char                            no-undo .
def {1} shared var lib-catego   as char                            no-undo .
def {1} shared var tot-qte      as dec                 extent 700  no-undo .
def {1} shared var ind-tot      as int                             no-undo .
def {1} shared var lib-tot      as char                            no-undo .
def {1} shared var lib-tit      as char                            no-undo .
 
 
/* Definition des zones & de la frame */
def {1} shared var dat-debx     as date format "99/99/9999"         no-undo .
def {1} shared var dat-finx     as date format "99/99/9999"         no-undo .
def {1} shared var mois-deb     as date format "99/99/9999"         no-undo .
def {1} shared var mois-fin     as date format "99/99/9999"         no-undo .
def {1} shared var familles     as char format "x(40)"              no-undo .
def {1} shared var maquette     as char format "x(12)"              no-undo .
def {1} shared var periph       as char format "x(12)"              no-undo .
def {1} shared var lib-maq      as char format "x(30)"              no-undo .
 
 
def {1} shared var z-choix      as char                             no-undo .
 
def {1} shared frame fr-saisie .
 
form libelle[ 11 ] format "x(28)"  dat-debx     dat-finx    skip
     libelle[ 12 ] format "x(28)"  mois-deb     mois-fin    skip
     libelle[ 13 ] format "x(28)"  familles                 skip
     libelle[ 14 ] format "x(28)"  maquette     lib-maq     skip
     libelle[ 15 ] format "x(28)"  periph
     with frame fr-saisie
     row 3  centered  overlay  no-label  with title libelle[ 10 ] .