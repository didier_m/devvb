/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/src/coagiop0.p                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 17744 11-09-09!New!cch            !Gestion interface trésorerie AGIL                                     !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !tables                                          !
!chx-opt.f             !                                     !                                                !
!s-parsai.i            !new                                  !                                                !
!aide.i                !new                                  !                                                !
!compo.f               !new                                  !                                                !
!coagiop0.i            !new                                  !                                                !
!v6frame.i             !                                     !                                                !
!aide-lib.i            !                                     !                                                !
!fr-cadre.i            !lib-prog                             !                                                !
!compo.i               !                                     !                                                !
!fncompo.i             !"code-mvt" "fr-cle" "code-mvt"       !                                                !
!z-cmp.i               !"code-mvt" "' '"                     !                                                !
!aff-opeb.i            !b-tables                             !                                                !
!majmoucb.i            !b-tables                             !                                                !
!standard.i            !-chxopt1 std-chxbas                  !                                                !
!chx-opt.i             !"4" "std-chxbas" "13" "15" "17" " " "!                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
!_____________________________________________________________________________________________________________!
!                                                  P R O C E D U R E S                                        !
!_____________________________________________________________________________________________________________!
!RECH-CORRESP          !                                                                                      !
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           C O A G I O P 0 . P                            */
/* Correspondance Codes Mouvements AGIL / Codes Operations DEAL             */
/*--------------------------------------------------------------------------*/
/* Programme Principal                                                      */
/*==========================================================================*/

{ connect.i      }
{ chx-opt.f      }
{ s-parsai.i new }
{ aide.i     new }
{ compo.f    new }
{ coagiop0.i new }

def var ind           as int                 no-undo .
def var codsoc-trt    as char                no-undo .
def var etabli-trt    as char                no-undo .
def var typtab-trt    as char                no-undo .
def var prefix-trt    as char                no-undo .
def var codtab-trt    as char                no-undo .
def var std-chxbas    as char                no-undo .
def var code-mvt      as char format "xx"    no-undo .
def var code-groupe   as char format "xx"    no-undo .
def var code-nature   as char format "x(4)"  no-undo .
def var code-paiement as char format "xxx"   no-undo .
def var code-motif    as char format "xxx"   no-undo .

form libelle[ 01 ] format "x(16)" code-mvt      skip
     libelle[ 02 ] format "x(16)" code-groupe   skip
     libelle[ 03 ] format "x(16)" code-nature   skip
     libelle[ 04 ] format "x(16)" code-paiement skip
     libelle[ 05 ] format "x(16)" code-motif
     with frame fr-cle with title libelle[ 21 ]
     row 3 col 11 overlay no-label { v6frame.i } .

form libelle[ 11 ] format "x(7)" b-tables.datcre
     libelle[ 12 ] format "xxx"  b-tables.opecre skip
     libelle[ 13 ] format "x(7)" b-tables.datmaj
     libelle[ 14 ] format "xxx"  b-tables.opemaj
     with frame fr-maj row 3 col 43 overlay no-label { v6frame.i} .

aide-fichier = "coagiop0.ais" . aide-par = "" . run aide-lec.p .
aide-fichier = "coagiop0.aid" . aide-par = "" . run aide-lec.p .

do ind = 1 to 40 :
  aide-m = "libel" + string( ind, "99" ) .
  { aide-lib.i }
  libelle[ ind ] = aide-lib .
end .

assign libsoc-soc    = libelle[ 22 ]
       parsai-applic = "*natpro"
       parsai-prog   = "coagiop1"
       codsoc-trt    = ""
       etabli-trt    = ""
       typtab-trt    = "AGI"
       prefix-trt    = "COR-AGIOPE"
       compo-vecteur = "<01>,10,20,30,40,50"
       compo-bloque  = no
       compo-modif   = yes
       .

if zone-charg[ 1 ] <> "" then typtab-trt = trim( zone-charg[ 1 ] ) .
if zone-charg[ 2 ] <> "" then prefix-trt = trim( zone-charg[ 2 ] ) .

{ fr-cadre.i lib-prog }

      display         libelle[ 1 for 5 ] with frame fr-cle .
color display message libelle[ 1 for 5 ] with frame fr-cle .

      display         libelle[ 11 for 4 ] with frame fr-maj .
color display message libelle[ 11 for 4 ] with frame fr-maj .

parsai-type = "a" . run s-parsai.p .

SAISIE-GENERALE :
REPEAT :

    assign code-mvt      = ""
           code-groupe   = ""
           code-nature   = ""
           code-paiement = ""
           code-motif    = ""
           compo-retour  = 10
           .

    display code-mvt
            code-groupe  
            code-nature  
            code-paiement
            code-motif   
            with frame fr-cle .

    display "" @ b-tables.datcre
            "" @ b-tables.datmaj
            "" @ b-tables.opecre
            "" @ b-tables.opemaj
            with frame fr-maj .

    parsai-type = "e" . run s-parsai.p .

    SAISIE :
    REPEAT :

        { compo.i }

        /* Code Mouvement AGIL */
        REPEAT WHILE COMPO-MOTCLE = 10 WITH FRAME FR-CLE :

            { fncompo.i "code-mvt" "fr-cle" "code-mvt" }
            { z-cmp.i   "code-mvt" "' '" }

            if keyfunction( lastkey ) = "FIND"
            then do :
                libel = "." + code-mvt . /* Recherche sur codtab */
                run rech-corresp .
                if codtab = "$RIEN$" then undo, retry .
                leave SAISIE .
            end .

            if lookup( right-trim( code-mvt ), libelle[ 23 ] ) = 0
            then do :
                bell . bell .
                message code-mvt libelle[ 31 ] .
                readkey pause 2 .
                undo, retry .
            end .

            code-mvt = string( caps( code-mvt ), "xx" ) .
            display code-mvt .

            next saisie .

        END . /* Code Mouvement AGIL */

        /* Code Groupe AGIL */
        REPEAT WHILE COMPO-MOTCLE = 20 WITH FRAME FR-CLE :

            { fncompo.i "code-groupe" "fr-cle" "code-groupe" }
            { z-cmp.i   "code-groupe" "' '" }

            if keyfunction( lastkey ) = "FIND"
            then do :
                libel = "." + code-mvt + code-groupe . /* Recherche sur codtab */
                run rech-corresp .
                if codtab = "$RIEN$" then undo, retry .
                leave SAISIE .
            end .

            if lookup( right-trim( code-groupe ), libelle[ 24 ] ) = 0
            then do :
                bell . bell .
                message code-groupe libelle[ 31 ] .
                readkey pause 2 .
                undo, retry .
            end .

            code-groupe = string( caps( code-groupe ), "xx"  ) .
            display code-groupe .

            next saisie .

        END . /* Code Groupe AGIL */

        /* Code Nature AGIL */
        REPEAT WHILE COMPO-MOTCLE = 30 WITH FRAME FR-CLE :

            { fncompo.i "code-nature" "fr-cle" "code-nature" }
            { z-cmp.i   "code-nature" "' '" }

            if keyfunction( lastkey ) = "FIND"
            then do :
                libel = "." + code-mvt + code-groupe + code-nature . /* Recherche sur codtab */
                run rech-corresp .
                if codtab = "$RIEN$" then undo, retry .
                leave SAISIE .
            end .

            if lookup( right-trim( code-nature ), libelle[ 25 ] ) = 0
            then do :
                bell . bell .
                message code-nature libelle[ 31 ] .
                readkey pause 2 .
                undo, retry .
            end .

            code-nature = string( caps( code-nature ), "x(4)"  ) .
            display code-nature .

            next saisie .

        END . /* Code Nature AGIL */

        /* Code Paiement AGIL */
        REPEAT WHILE COMPO-MOTCLE = 40 WITH FRAME FR-CLE :

            { fncompo.i "code-paiement" "fr-cle" "code-paiement" }
            { z-cmp.i   "code-paiement" "' '" }

            if keyfunction( lastkey ) = "FIND"
            then do :
                libel = "." + code-mvt + code-groupe + code-nature + code-paiement . /* Recherche sur codtab */
                run rech-corresp .
                if codtab = "$RIEN$" then undo, retry .
                leave SAISIE .
            end .

            if lookup( right-trim( code-paiement ), libelle[ 26 ] ) = 0
            then do :
                bell . bell .
                message code-paiement libelle[ 31 ] .
                readkey pause 2 .
                undo, retry .
            end .

            code-paiement = string( caps( code-paiement ), "xxx"  ) .
            display code-paiement .

            next saisie .

        END . /* Code Paiement AGIL */

        /* Code Motif AGIL */
        REPEAT WHILE COMPO-MOTCLE = 50 WITH FRAME FR-CLE :

            { fncompo.i "code-motif" "fr-cle" "code-motif" }
            { z-cmp.i   "code-motif" "' '" }

            if keyfunction( lastkey ) = "FIND"
            then do :
                libel = "." + code-mvt + code-groupe + code-nature + code-paiement + code-motif . /* Recherche sur codtab */
                run rech-corresp .
                if codtab = "$RIEN$" then undo, retry .
                leave SAISIE .
            end .

            if lookup( right-trim( code-motif ), libelle[ 27 ] ) = 0
            then do :
                bell . bell .
                message code-motif libelle[ 31 ] .
                readkey pause 2 .
                undo, retry .
            end .

            code-motif = caps( right-trim( code-motif ) ) .
            display code-motif .

            next saisie .

        END . /* Code Paiement AGIL */

        if keyfunction( lastkey ) = "END-ERROR" then leave SAISIE-GENERALE .

    END . /* SAISIE */

    codtab-trt = code-mvt + code-groupe + code-nature + code-paiement + code-motif .

    FIND B-TABLES where b-tables.codsoc = codsoc-trt
                  and   b-tables.etabli = etabli-trt
                  and   b-tables.typtab = typtab-trt
                  and   b-tables.prefix = prefix-trt
                  and   b-tables.codtab = codtab-trt
                  exclusive-lock no-wait no-error .

    if locked b-tables
    then do :

        FIND B-TABLES where b-tables.codsoc = codsoc-trt
                      and   b-tables.etabli = etabli-trt
                      and   b-tables.typtab = typtab-trt
                      and   b-tables.prefix = prefix-trt
                      and   b-tables.codtab = codtab-trt
                      no-lock no-error .

        if available b-tables
        then do :
            { aff-opeb.i b-tables }
            next SAISIE-GENERALE .
        end .

    end .

    if not available b-tables
    then do :

        CREATE B-TABLES .

        {  majmoucb.i b-tables }

        assign b-tables.codsoc = codsoc-trt
               b-tables.etabli = etabli-trt
               b-tables.typtab = typtab-trt
               b-tables.prefix = prefix-trt
               b-tables.codtab = codtab-trt

               parsai-creation = yes
               .

    end .

    else parsai-creation = no .

    display b-tables.datcre b-tables.opecre b-tables.datmaj b-tables.opemaj
            with frame fr-maj.

    SAISIE-PARSAI :
    REPEAT :

        if not parsai-creation
        then do :
            parsai-type = "v". run s-parsai.p .
        end .

        parsai-type = "c". run s-parsai.p .

        if keyfunction(lastkey) = "END-ERROR"
        then do :
            bell . bell .
            hide message no-pause .
            message libelle[ 29 ] .
            readkey pause 2 .
            undo SAISIE-GENERALE, next SAISIE-GENERALE .
        end .

          /*-----------------------------------------------*/
         /* 1= Ok, 2= Refus, 3= Modif 4= Suppression      */
        /*-----------------------------------------------*/
        { standard.i -chxopt1 std-chxbas }

        REPEAT :
            assign chx-def = 1 chx-delete = 4 chx-milieu = 59 chx-ombre = "O" .
            { chx-opt.i "4" "std-chxbas" "13" "15" "17" " " "fr1" }
            if keyfunction( lastkey ) <> "RETURN" then next .
            leave .
        END .

        if chx-trait = 1 /* Acceptation */
        then do :
            { majmoucb.i b-tables }
        end .

        if chx-trait = 2 /* Refus */
        then do :
            bell . bell .
            hide message no-pause .
            message libelle[ 29 ] .
            readkey pause 2 .
            undo SAISIE-GENERALE, next SAISIE-GENERALE .
        end .

        if chx-trait = 3 /* Modification */
        then do :
            parsai-creation = no .
            next SAISIE-PARSAI .
        end .

        if chx-trait = 4 /* Suppression */
        then do :

            DELETE B-TABLES .

            bell . bell .
            hide message no-pause .
            message libelle[ 30 ] .
            readkey pause 2 .

        end .

        leave .

    END . /* SAISIE-PARSAI */

END .   /*  SAISIE-GENERALE  */

parsai-type = "h" .
run s-parsai.p .

hide frame fr-cle   no-pause .
hide frame fr-maj   no-pause .
hide frame fr-titre no-pause .
hide frame fr-cadre no-pause .


/********************** P R O C E D U R E S *************************/

/* Recherche Correspondances AGIL / DAEL */
PROCEDURE RECH-CORRESP :

    assign codsoc = codsoc-trt
                   etabli = etabli-trt
                   typtab = typtab-trt
                   prefix = prefix-trt
           .

    run coagiopr.p .

    if codtab = "$RIEN$" then return .

    assign code-mvt      = substr( codtab, 1, 2 )
           code-groupe   = substr( codtab, 3, 2 )
           code-nature   = substr( codtab, 5, 4 )
           code-paiement = substr( codtab, 9, 3 )
           code-motif    = substr( codtab, 12 )
           .

    display code-mvt     
            code-groupe  
            code-nature  
            code-paiement
            code-motif   
            with frame fr-cle .

END PROCEDURE .