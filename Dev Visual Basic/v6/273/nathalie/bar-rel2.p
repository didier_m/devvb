/*==========================================================================*/
/*                          B A R - R E L 2 . P                             */
/*       GESTION des BAREMES       : EDITION DES RELEVES DE RISTOURNE       */
/*                                                                          */
/*==========================================================================*/
 
{ connect.i   }
{ natabsoc.i    }
{ maquette.i  }
 
{ bar-cal0.i }
 
/*--------------------------------------------*/
/* Societe de regroupement                    */
/*--------------------------------------------*/
regs-app = "nathalie" .
{ regs-cha.i }
 
/*-------------------------------------------------*/
/* Definitions des Variables                       */
/*-------------------------------------------------*/
def input parameter  fichier-tri  as char format "x(12)" .
def var nb-lig      as int                                 no-undo .
def var nb-adh      as int                                 no-undo .
def var nb-bar      as int                                 no-undo .
def var qte-ach     as dec                                 no-undo .
def var mont-ttc    as dec                                 no-undo .
def var mont-ht     as dec                                 no-undo .
def var mont-tva    as dec                                 no-undo .
def var tot-ach     as dec                                 no-undo .
def var tot-ttc     as dec                                 no-undo .
def var tot-ht      as dec                                 no-undo .
def var tot-tva     as dec                                 no-undo .
def var tauxtva     as dec                                 no-undo .
 
def var i           as int                                 no-undo .
 
/*------------------------------------*/
/* Chargement des dates de l'exercice */
/*------------------------------------*/
{ exe-dat0.i }
exe-rel = substring(string(year(date-exe-deb)), 3,2) + "-" +
          substring(string(year(date-exe-fin)), 3,2).
 
/* chargement du taux de tva */
/*--------------------------*/
find TABGES where tabges.codsoc = codsoc-soc
              and tabges.etabli = " "
              and tabges.typtab = "PAR"
              and tabges.prefix = "PARAMETRES"
              and tabges.codtab = "BAREMES"
              no-lock no-error .
if not available tabges
then do :
    message "Tables des Parametres pour les Baremes Inexistante ..." .
    bell. bell. pause .
    return .
end .
tauxtva = ( tabges.nombre[1] / 100 ) + 1 .
 
 
/*==========================================================================*/
/*                           Debut du traitements                           */
/*==========================================================================*/
/* trier par : 1-  Top-realise
               2-  Magasin
               3-  Nom client / typaux / codaux
               4-  Bareme
               5-  No-enreg
                -----------------------------------------------------------*/
/*----------------------------------------*/
/* Ouverture des fichiers :  TRI et SPOOL */
/*----------------------------------------*/
input from value( fichier-tri ) .
output stream maquette to value( nom-spool ) .    /* Ouverture periph */
 
/*----------------------------*/
/* Definition des Ruptures    */
/*----------------------------*/
{ rup-def.i new }
 
{ rup-ini.i "'01,03,43,02,12'" }
 
GENERAL :
REPEAT  :
    /* lecture du fichier trier */
    import zone .
 
    /* ne prendre que les realisations        */
    if substring( zone, 01, 1 )  <> "1" then next .
 
 
    /* Rupture-test = zone qui permet de tester les ruptures */
    rupture-test = substring( zone , 1 , 61 ) .
 
    /*----------------------------------------------------------*/
    /* Rup-edi.i  = Module gerant les ruptures                  */
    /* bar-rel2.i = module contenant les editions et les cumuls */
    /*----------------------------------------------------------*/
    { rup-edi.i  bar-rel2.i }
 
END .  /* repeat */
/*-----------------------------------------*/
/* edition des totaux sur derniere rupture */
/*-----------------------------------------*/
 
if nb-bar <> 0
then do :
 
   { maq-raz.i "05" }
 
                                             /* le prix est au quintal */
   assign mont-ttc = round( ( qte-ach / 100 ) * tabges.nombre[2], 2)
          mont-ht  = round( mont-ttc / tauxtva , 2 )
          mont-tva = mont-ttc - mont-ht .
 
   assign edit[11] = substring(tabges.libel2[2], 1, 4 )
          edit[12] = no-bareme
          edit[13] = string( qte-ach , ">>>>>>9.99-")
          edit[14] = string( tabges.nombre[2] , ">>>9.99-" )
          edit[15] = string( mont-ht  , ">>>>>9.99-" )
          edit[16] = string( mont-tva , ">>>>9.99-" )
          edit[17] = string( mont-ttc , ">>>>>9.99-" ) .
 
   do i = 1 to 20 :
       { maq-maj.i "05" i  edit[i] }
       edit[i] = " " .
   end .
 
   { maq-edi.i "05" }
 
  assign  tot-ht  = tot-ht  + mont-ht
          tot-tva = tot-tva + mont-tva
          tot-ttc = tot-ttc + mont-ttc
          nb-lig  = nb-lig  + 1 .
 
   /* remplissage du cadre */
   do i = (nb-lig + 1) to 8 :
       { maq-edi.i "06" }
   end .
 
   { maq-raz.i "08" }
 
   assign edit[15] = string( tot-ht   , ">>>>>9.99-" )
          edit[16] = string( tot-tva  , ">>>>9.99-" )
          edit[17] = string( tot-ttc  , ">>>>>9.99-" ) .
 
   do i = 1 to 20 :
       { maq-maj.i "08" i  edit[i] }
       edit[i] = " " .
   end .
 
   { maq-edi.i "08" }
 
   edit[01] = exe-rel .
   { maq-maj.i "10"  "01" edit[1] }
   { maq-edi.i "10" }
end .
 
 
input close .  
 
 
 