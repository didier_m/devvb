/*==========================================================================*/
/*                           E X T - P 5 M E . I                            */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Messages Magasins PDP V5                        CC 03/05/1999 */
/*==========================================================================*/

                                                        /* Pos.  Long. */

repeat :

    code-mes = string( year ( today ), "9999" )           +
               string( month( today ), "99"   )           +
               string( day  ( today ), "99"   )           +
               substr( string( time, "hh:mm:ss" ), 1, 2 ) +
               substr( string( time, "hh:mm:ss" ), 4, 2 ) +
               substr( string( time, "hh:mm:ss" ), 7, 2 )
               .

    if code-mes = anc-code-mes then next . /* Unicite du code */
    anc-code-mes = code-mes .

    leave .

end .

/* Societes saisies */
if proglib.libelle[1] <> ""
then do notour = 1 to num-entries( proglib.libelle[1] ) :

    if entry ( notour, proglib.libelle[1] ) <> codsoc-trait[nsoc]
       then next .

    FOR EACH MAGASI where magasi.codsoc     = ""
                    and   magasi.societe    = entry( notour, proglib.libelle[1] )
                    and   magasi.equipe     = "1"
                    and   magasi.nombre[30] = 1
                    use-index magasi-1
                    no-lock :

        /* Entete */
        zone = "MESSAGE "                                   /* 001 - 008 */
               + "C"                                        /* 009 - 001 */
               + string(codsoc-trait[nsoc], "xx")           /* 010 - 002 */
               + fill(" ", 3)                               /* 012 - 003 */
               + string(magasi.codtab, "xxx")               /* 015 - 003 */
               + fill(" ", 60)                              /* 018 - 060 */
               + string(code-mes, "x(14)")                  /* 078 - 014 */
               + "0000"                                     /* 092 - 004 */
               + string(proglib.libelle[3], "x(73)")        /* 096 - 073 */
               + "##"                                       /* 096 - 014 */
               + string(proglib.opemaj, "xxx")
               + string(magasi.codtab, "xxx")
               + fill(" ", 6)
               .

        zone = caps(zone).

        put stream kan-pro-v5 unformatted zone skip.

        /* Lignes */
        do i = 3 to 16 :

            zone = "MESSAGE "                               /* 001 - 008 */
                   + "C"                                    /* 009 - 001 */
                   + string(codsoc-trait[nsoc], "xx")       /* 010 - 002 */
                   + fill(" ", 3)                           /* 012 - 003 */
                   + string(magasi.codtab, "xxx")           /* 015 - 003 */
                   + fill(" ", 60)                          /* 018 - 060 */
                   + string(code-mes, "x(14)")              /* 078 - 014 */
                   + string(i - 2, "9999" )                 /* 092 - 004 */
                   + string(proglib.libelle[i], "x(73)")    /* 096 - 073 */
                   + "##"                                   /* 096 - 014 */
                   + string(proglib.opemaj, "xxx")
                   + string(magasi.codtab, "xxx")
                   + fill(" ", 6)
                   .

            zone = caps(zone).

            put stream kan-pro-v5 unformatted zone skip.

        end. /* do i = 3 to 16 */

    END . /* FOR EACH MAGASI */

end. /* do notour = 1 to ... */

/* Magasins saisis */
if proglib.libelle[2] <> ""
then do notour = 1 to num-entries( proglib.libelle[2] ) :

    FOR EACH MAGASI where magasi.codsoc     = ""
                    and   magasi.codtab     = entry( notour, proglib.libelle[2] )
                    and   magasi.societe    = codsoc-trait[nsoc]
                    and   magasi.equipe     = "1"
                    and   magasi.nombre[30] = 1
                    use-index magasi-1
                    no-lock :

        /* Entete */
        zone = "MESSAGE "                                   /* 001 - 008 */
               + "C"                                        /* 009 - 001 */
               + string(codsoc-trait[nsoc], "xx")           /* 010 - 002 */
               + fill(" ", 3)                               /* 012 - 003 */
               + string(magasi.codtab, "xxx")               /* 015 - 003 */
               + fill(" ", 60)                              /* 018 - 060 */
               + string(code-mes, "x(14)")                  /* 078 - 014 */
               + "0000"                                     /* 092 - 004 */
               + string(proglib.libelle[3], "x(73)")        /* 096 - 073 */
               + "##"                                       /* 096 - 014 */
               + string(proglib.opemaj, "xxx")
               + string(magasi.codtab, "xxx")
               + fill(" ", 6)
               .

        zone = caps(zone).

        put stream kan-pro-v5 unformatted zone skip.

        /* Lignes */
        do i = 3 to 16 :

            zone = "MESSAGE "                               /* 001 - 008 */
                   + "C"                                    /* 009 - 001 */
                   + string(codsoc-trait[nsoc], "xx")       /* 010 - 002 */
                   + fill(" ", 3)                           /* 012 - 003 */
                   + string(magasi.codtab, "xxx")           /* 015 - 003 */
                   + fill(" ", 60)                          /* 018 - 060 */
                   + string(code-mes, "x(14)")              /* 078 - 014 */
                   + string(i - 2, "9999" )                 /* 092 - 004 */
                   + string(proglib.libelle[i], "x(73)")    /* 096 - 073 */
                   + "##"                                   /* 096 - 014 */
                   + string(proglib.opemaj, "xxx")
                   + string(magasi.codtab, "xxx")
                   + fill(" ", 6)
                   .

            zone = caps(zone).

            put stream kan-pro-v5 unformatted zone skip.

        end. /* do i = 3 to 16 */

    END . /* FOR EACH MAGASI */

end. /* do notour = 1 to ... */
