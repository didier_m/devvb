/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/fac-extr.p                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!V61 36497 01-03-13!Evo!jcc            !Lancement par par un cron                                             !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !new                                  !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/************************************************************************************************************/
/*  FAC-EXTR.P - Lanceur de l'application : NATHALIE APPROS, pour extraction des factures d'achat           */
/*----------------------------------------------------------------------------------------------------------*/

{ connect.i  new }

def var x-x         as char         no-undo .
def var fic-log     as char         no-undo .

fic-log = "/applic/li/travail/fac-extr.log" + string( today , "999999" ) .

output to value( fic-log ) append .

assign mem-langue = 1
       operat     = "SCN" .

run boot-cnx.p( 21 ) .  /*  Connexion des Bases de l'application : NATHALIE APPROS Production  */

message  "DEBUT : Dématérialisation : Extraction des factures , le"  today  "a"  string( time , "hh:mm:ss" ) .
message  "        ================================================" .
message  " " .

x-x = "DIRECT" + "|" .
run e-extfac.p( x-x ) .

message  "FIN   : Dématérialisation : Extraction des factures , le"  today  "a"  string( time , "hh:mm:ss" ) .
message  "        ================================================" .
message  " " .

output close .

quit . /* Exit de PROGRESS */



