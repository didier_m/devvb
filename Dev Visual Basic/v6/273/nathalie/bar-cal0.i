/*==========================================================================*/
/*                          B A R - C A L 0 . I                             */
/*       GESTION des BAREMES       : CALCUL DES REALISATIONS                */
/*       Gestion des variables communes                                     */
/*==========================================================================*/


def {1} shared stream kan-tri .                       /* fichier de travail */
def {1} shared stream maquette.                       /* spool edition*/

def {1} shared var zone          as char                            no-undo .
def {1} shared var edit          as char          extent  30        no-undo .

def {1} shared var auxili-soc    as char                            no-undo .
def {1} shared var libelle       as char                 extent 99  no-undo .

def {1} shared var sequence     as char format "x(5)"               no-undo .
def {1} shared var nom-spool    as char format "x(12)"              no-undo .
def {1} shared var nom-spool1   as char format "x(12)"              no-undo .
def {1} shared var nom-spool2   as char format "x(12)"              no-undo .
def {1} shared var nom-spool3   as char format "x(12)"              no-undo .
def {1} shared var lib-titre    as char                             no-undo .
def {1} shared var sav-periph   as char                             no-undo .

def {1} shared var z-choix      as char                             no-undo .

def {1} shared var fic-tri      as char                             no-undo .
def {1} shared var maquette     as char format "x(12)"              no-undo .


def {1} shared var annee-exe   as  char                             no-undo .
def {1} shared var date-fin    as char                              no-undo .
def {1} shared var no-magasin  as char   format "xxx"               no-undo .
def {1} shared var lib-magasin as char   format "x(30)"             no-undo .
def {1} shared var no-bareme   as char   format "xx"                no-undo .

def {1} shared var numbon      as int    format ">>>>>>9"           no-undo .
def {1} shared var exe-rel     as char                              no-undo .

def {1} shared var date-exe-d  as date    format "99/99/9999"        no-undo .
def {1} shared var date-exe-f  as date    format "99/99/9999"        no-undo .
def {1} shared var cal-o       as logical format "1/0" initial "0"   no-undo .
def {1} shared var rea-o       as logical format "1/0" initial "1"   no-undo .
def {1} shared var maquette1   as char    format "x(12)"             no-undo .
def {1} shared var lib-maq1    as char    format "x(30)"             no-undo .
def {1} shared var rea-n       as logical format "1/0" initial "1"   no-undo .
def {1} shared var maquette2   as char    format "x(12)"             no-undo .
def {1} shared var lib-maq2    as char    format "x(30)"             no-undo .
def {1} shared var rele        as logical format "1/0" initial "1"   no-undo .
def {1} shared var maquette3   as char    format "x(12)"             no-undo .
def {1} shared var lib-maq3    as char    format "x(30)"             no-undo .
def {1} shared var date-edi    as date    format "99/99/9999"        no-undo .
def {1} shared var cpt-rele    as logical format "1/0" initial "0"   no-undo .
def {1} shared var date-cpt    as date    format "99/99/9999"        no-undo .

def {1} shared var ma-esc1     as char                               no-undo .
def {1} shared var ma-esc2     as char                               no-undo .
def {1} shared var ma-esc3     as char                               no-undo .

/*------------------------------------------------------------------------*/

def {1} shared frame fr-saisie .

form libelle[ 11 ] format "x(30)"  date-exe-d  " - " date-exe-f skip(1)
     libelle[ 24 ] format "x(30)"  cal-o                        skip
     libelle[ 12 ] format "x(30)"  rea-o                        skip
     libelle[ 13 ] format "x(30)"  maquette1     lib-maq1       skip
     libelle[ 14 ] format "x(30)"  nom-spool1                   skip
     libelle[ 15 ] format "x(30)"  rea-n                        skip
     libelle[ 16 ] format "x(30)"  maquette2     lib-maq2       skip
     libelle[ 17 ] format "x(30)"  nom-spool2                   skip
     libelle[ 18 ] format "x(30)"  rele                         skip
     libelle[ 19 ] format "x(30)"  maquette3     lib-maq3       skip
     libelle[ 20 ] format "x(30)"  nom-spool3                   skip
     libelle[ 21 ] format "x(30)"  date-edi                     skip
     libelle[ 22 ] format "x(30)"  cpt-rele                     skip
     libelle[ 23 ] format "x(30)"  date-cpt
     with frame fr-saisie
     row 3  centered  overlay  no-label  { v6frame.i } .