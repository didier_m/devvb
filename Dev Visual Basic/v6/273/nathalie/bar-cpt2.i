/*==========================================================================*/
/*                          B A R - C P T 2 . I                             */
/*==========================================================================*/
/*           Zones utilise pour le module edition des ruptures              */
/*                Rupture-a               : Niveau de rupture en cours      */
/*                Rupture-nb              : Nombre de rupture existante     */
/*                Rupture-etat = "t"      : Edition du titre de la rupture  */
/*                Rupture-etat = "r"      : Edition de la rupture           */
/*--------------------------------------------------------------------------*/

/*===RUPTURES ENTETE ======================================================*/

/*    Entete ADHERENT                */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 3 :

    hide message . pause 0 .
    message "  Traitement du Tiers ...: "
            "  " substring( zone , 38, 10 )
            "  " substring( zone ,  5, 30 ).

    assign tot-ht  = 0
           tot-tva = 0
           tot-ttc = 0
           nb-lig  = 0
           ac      = 0
           ecr-mt-tva = 0
           ecr-cpttva = " "
           ecr-mt-rst = 0
           ecr-cptrst = " " .

    assign numbon = numbon + 1
           nb-adh = nb-adh + 1 .
                                                 /* sert pour ecr-rist.p */
    regs-fileacc = "AUXILI" + substring( zone, 35 , 3 ) .
    {regs-rec.i}
    FIND B-AUXILI  where b-auxili.codsoc = regs-soc
                     and b-auxili.typaux = substring( zone, 35 , 3 )
                     and b-auxili.codaux = substring( zone, 38, 10 )
                     no-lock no-error .
    if not available b-auxili
    then do :
        message substring( zone, 38, 10 )
                " : Tiers Inexistant en Compta ."
                " : Tiers non traite apres la pause. ".
        bell . bell . pause .
        leave .
    end .

    FIND AUXAPR where auxapr.codsoc = codsoc-soc
                  and auxapr.typaux = substring( zone, 35 , 3  )
                  and auxapr.codaux = substring( zone, 38 , 10 )
                  no-lock no-error .
    if not available auxapr
    then do :
        message substring( zone, 38, 10 )
                " : Tiers Inexistant en Gestion ."
                " : Tiers non traite apres la pause. " .
        bell . bell . pause .
        leave .
    end .

/*    if b-auxili.cpt = par-cpt[10]                           /*"411100"*/  */
    if auxapr.type-adh = "1"
    then assign ecr-colaux  = par-cpt[11]                       /*"453996"*/
                cpt-gen     = par-cpt[12]                       /*"709721"*/
                nat         = par-cpt[13]                       /*"7096"  */
                cpt-tva     = par-cpt[14]                       /*"445711"*/
               .

/*    if b-auxili.cpt = par-cpt[20]                           /*"453100"*/  */
    if auxapr.type-adh <> "1"
    then assign ecr-colaux  = par-cpt[21]                       /*"411896"*/
                cpt-gen     = par-cpt[22]                       /*"709720"*/
                nat         = par-cpt[23]                       /*"7096"  */
                cpt-tva     = par-cpt[24]                       /*"445711"*/
                .

    assign ecr-typaux  = substring( zone, 35 , 3 )
           ecr-codaux  = substring( zone, 38, 10 ) .

    leave  .
END .

/*    Entete BAREMES                 */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 4 :

    assign no-magasin = substring( zone , 2 , 3 )
           no-bareme  = substring( zone, 48, 2 )
           qte-ach    = dec( substring( zone, 62, 12) ) .

    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "TAR"
                and   tabges.prefix = "BAREME-CLI"
                and   tabges.codtab = no-bareme
                no-lock  no-error .

    assign act      = substring( tabges.libel2[2], 6, 3 )
           prix-uni = tabges.nombre[2] .
    leave .

END .

/*===RUPTURES EN TOTAL=====================================================*/

/*    Total  Baremes            */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 4 :

                                             /* le prix est au quintal */
    assign mont-ttc = round(( qte-ach / 100 ) * prix-uni, 2 )
           mont-ht  = round(mont-ttc / tauxtva , 2 )
           mont-tva = mont-ttc - mont-ht .


   assign  tot-ht  = tot-ht  + mont-ht
           tot-tva = tot-tva + mont-tva
           tot-ttc = tot-ttc + mont-ttc
           nb-lig  = nb-lig + 1
           .

   ac = ac + 1 .

   ecr-mt-rst[ac] = mont-ht .
   ecr-pierst     = string(numbon) .
   assign substring( ecr-cptrst[ ac ],  1, 10 ) = cpt-gen
          substring( ecr-cptrst[ ac ], 11,  4 ) = nat
          substring( ecr-cptrst[ ac ], 15,  3 ) = no-magasin
          substring( ecr-cptrst[ ac ], 18,  3 ) = act
          substring( ecr-cptrst[ ac ], 21,  5 ) = "     " .

   leave .

END .


/*    Total  ADHERENT           */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A =  3 :

   assign ecr-cpttva[1]  = cpt-tva
          ecr-mt-tva[1]  = tot-tva
          ecr-mt-ttc     = tot-ttc
          .

   if tot-ttc > mtmini
   then       run ecr-rist.p .

   leave .

END .