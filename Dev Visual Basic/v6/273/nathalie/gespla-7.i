/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/gespla-7.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V61 37932 10-05-13!Evo!cch            !Ajout gestion Taxe Eco mobilier - Modif. faite par EUREA              !
!V61 20849 01-10-10!Evo!cch            !Modif. faite par DGE (EUREA) : trt volume + MAJ donn�es poids, volume,!
!V52 16634 06-02-08!Evo!cch            !Modifications DGR 28/01/2008                                          !
!V52 16252 20-09-07!Evo!cch            !Ajout gestion de l'Ecotaxe Article ( fait par DGR )                   !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/* GESPLA-7.I - GESTION DES ECHANGES DE DONNEES NATHALIE / PLATEFORMES        */
/*----------------------------------------------------------------------------*/
/* Intergrations Articles et Tarifs                 08/06/2004 - SOPRES - 273 */
/******************************************************************************/

if anc-enreg = "O" /* Ancien format */
then do :

   /* L ancien format n est plus utilise */

   if tarif <> "O" /* Articles */
   then do :

       assign
        codart     = substring ( liglue ,  1 ,  6  )
        codart-fab = ""
        codart-anc = ""
        libart     = substring ( liglue ,  1 ,  6  )
        libart2    = ""
        ucde       = substring ( liglue , 41 , 30  )
        ufac       = substring ( liglue , 79 , 2 )
        flgdet     = substring ( liglue , 81 , 2 )
        flgsta     = substring ( liglue ,101 ,  1  )

        un-min-fou     = ""
        un-min-franco  = ""
        filiere-art    = ""
        faccon         = 0
        nbucol         = 0
        val-min-uvc    = 0
        val-min-fou    = 0
        val-min-franco = 0
        par-comb-uvc   = 0
        delai          = 0
        wdeee          = 0
        wamm           = ""
        wtax-ag        = 0
        wtax-ap        = 0
        wtax-lb        = 0
        wtax-rm        = 0
        wtax-rmc       = 0
        wtax-sn        = 0
        wdat-app       = ?
        weaj           = ""
        wecom          = 0  /* DGR 30/04/2013 */
        .

        faccon = round( dec ( substring ( liglue , 83 ,  5  ) ), 2 ) no-error .
        nbucol = dec ( substring ( liglue , 88 ,  4  ) ) no-error .

        if faccon = 0 then faccon = 1 .
        if nbucol = 0 then nbucol = 1 .
        if flgdet = "C" then flgdet = "O" .
                        else flgdet = "F" .
        if (flgsta = "2" or flgsta = "4")
           then flgsta = "I" .
           else flgsta = " " .

   end . /* Articles */

   else do : /* Tarifs Achats */

        assign
            typfic   = substring ( liglue ,  1 ,  1  )
            typtar   = substring ( liglue ,  1 ,  1  )
            codart   = substring ( liglue ,  2 ,  6  )
            decond   = substring ( liglue , 9 ,  8  )
            mnreve   = 0
            mvtrai   = 0
            mvfina   = 0
            pxvindic = 0
            jj       = 0
            mm       = 0
            aa       = 0
            .

        mnreve   = dec ( substring ( liglue , 18 ,  9  ) ) no-error .
        mvtrai   = dec ( substring ( liglue , 41 ,  9  ) ) no-error .
        mvfina   = dec ( substring ( liglue , 50 ,  9  ) ) no-error .
        pxvindic = dec ( substring ( liglue , 86 ,  9  ) ) no-error .

        jj = int ( substring ( decond , 7 , 2 ) ) no-error .
        mm = int ( substring ( decond , 5 , 2 ) ) no-error .
        aa = int ( substring ( decond , 1 , 4 ) ) no-error .

        date-applic = date ( mm , jj , aa ) no-error.
        if error-status:error
        then do :
            bell. bell.
            message decond ": Date debut erronee" codaux codart
            view-as alert-box warning.
            undo, next.
        end.

        /* ERIK 13/03/97 prise en compte date de fin */
        decond = substring ( liglue , 95 ,  8  ) .

        if decond <> ""
        then do :

            assign date-fin = ?
                   jj       = 0
                   mm       = 0
                   aa       = 0
                   .

            jj = int ( substring ( decond , 7 , 2 ) ) no-error .
            mm = int ( substring ( decond , 5 , 2 ) ) no-error .
            aa = int ( substring ( decond , 1 , 4 ) ) no-error .

            date-fin = date ( mm , jj , aa ) no-error.
            if error-status:error
            then do :
                bell. bell.
                message decond ": Date fin erronee" codaux codart
                view-as alert-box warning.
                undo, next.
            end.

        end .

        remise = mnreve - mvtrai .
        remrfc = mvtrai - mvfina .

        /* Prise en compte des Codes Tarifs EURO ( CC 06/07/2001 )
        if typtar = "P" then typtar = "PRO" .
                        else typtar = "   " .
        */
        run cor-code-tar .

   end . /* Tarifs Achats */

end . /* Ancien format */

else do : /* Nouveau format */

    if tarif <> "O" /* Articles */
    then do :

        assign
            codart     = substring ( liglue , 9 , 6  )
            codart-fab = substring ( liglue , 24 , 8 ) +
                         right-trim( substring ( liglue ,  62 , 7 ) )
            codart-anc = right-trim( substring ( liglue , 260 , 6 ) )
            libart     = substring ( liglue , 32 , 30 )
            stocke     = substring ( liglue , 69 , 1 )
            ucde       = substring ( liglue , 70 , 2 )
            ufac       = substring ( liglue , 72 , 2 )
            flgdet     = substring ( liglue , 92 ,  1  )
            flgsta     = substring ( liglue , 85 ,  1  )
            gencod-art = substring ( liglue , 110 , 13 )
            libart2    = substring ( liglue , 123 , 9 ) + " " +
                         substring ( liglue , 132 , 30 )

            un-min-fou    = right-trim( substring ( liglue , 211 , 3 ) )
            un-min-franco = right-trim( substring ( liglue , 219 , 3 ) )
            filiere-art   = right-trim( substring ( liglue , 225 , 1 ) )

            val-min-uvc    = 0
            val-min-fou    = 0
            val-min-franco = 0
            par-comb-uvc   = 0
            delai          = 0
            faccon         = 0
            nbucol         = 0
            pdspla         = 0
            volpla         = 0
            .

            faccon         = dec ( substring ( liglue , 74 ,  5  ) ) no-error .
            nbucol         = dec ( substring ( liglue , 79 ,  4  ) ) no-error .
            pdspla         = dec ( substring ( liglue , 98 ,  8  ) ) no-error .
            if substring ( liglue , 231,  1 ) = "L" then 
               volpla         = dec ( substring ( liglue , 232,  8  ) ) no-error .
            val-min-uvc    = dec ( substring ( liglue , 196 , 5  ) ) no-error .
            par-comb-uvc   = dec ( substring ( liglue , 201 , 5  ) ) no-error .
            val-min-fou    = dec ( substring ( liglue , 206 , 5  ) ) no-error .
            val-min-franco = dec ( substring ( liglue , 214 , 5  ) ) no-error .
            delai          = dec ( substring ( liglue , 222 , 3  ) ) no-error .

            faccon = round ( faccon , 2 ) .
            if nbucol = 0 then nbucol = 1 .
            if faccon = 0 then faccon = 1 .
            if flgdet = "C" then flgdet = "O" .
                            else flgdet = "F" .

        /* Correspondance GIE/DISPAGRI/LISADIS/FLORADIS :

                1=O ouvert
                2=F ferme
                3=I En instance de fermeture
                    (article termine, la plate-forme ne commande plus)
                4=A En instance d ouverture
                5=A Hors saison
                6=Probleme fabricant
                7=Promo epuisee
                8=Epuise Import
                nouveaux codes issus DISPAGRI nouvelle informatique
                V equivalent a "4" de LISADIS
                F equivalent a "2" de LISADIS

               codes CBA DEAL :

                O ou blanc = ouvert
                F = ferme
                I = interdit de commander a un fournisseur
                B = interdit de vente
        */
            /* enleve par CCO le 28/08/2012 
               FLGSTA reste avec sa valeur d'origine 1,2,3,4,5,6,7,8,V,F

            ***if (flgsta = "2" or flgsta = "4" or flgsta = "5"
            ***    or flgsta = "V" or flgsta = "F"
            ***    or flgsta = "6" or flgsta = "7"or flgsta = "8" )
            ***   then flgsta = "I" .
            ***   else flgsta = " " .
               Fin CCO le 28/08/2012      */

            pdspla = pdspla * 0.001.
            volpla = volpla * 0.001.

            /* DGR 14/09/2007 : rajout ECOTAXE wdeee */
            wdeee = deci( substring ( liglue , 290, 6  ) ) / 10000 no-error .           
            if error-status:error then wdeee = 0.

            /* DGR 30/04/2013 : rajout TAXE ECO-MOBILIER wecom */
            wecom = deci( substring ( liglue , 296, 4  ) ) / 100 no-error .        
            if error-status:error then wecom = 0.

            /* DGR 28/01/2008 : rajout Amm + Redevance Pollution Diffuse */
            wamm = substr(liglue, 301,  8).
            weaj     = substr(liglue, 309,  1).
            
            /* DGR 12/07/2016 : blanc = oui */
            if weaj = "O" then weaj = "1".
                else if weaj = "N" then weaj = "0".
                          else weaj = "".

            wdat-app = date (int(substr(liglue, 314,  2)) ,
                             int(substr(liglue, 316,  2)) ,
                             int(substr(liglue, 310,  4)) ) no-error.


            if error-status:error then wdat-app = ?. 

            wtax-ag  = dec(substr(liglue, 318,  9)) / 1000 no-error.
            if error-status:error then wtax-ag = 0.

            wtax-ap  = dec(substr(liglue, 327,  9)) / 1000 no-error.
            if error-status:error then wtax-ap = 0.

            wtax-lb  = dec(substr(liglue, 336,  9)) / 1000 no-error.
            if error-status:error then wtax-lb = 0.

            wtax-rm  = dec(substr(liglue, 345,  9)) / 1000 no-error.
            if error-status:error then wtax-rm = 0.

            wtax-rmc = dec(substr(liglue, 354,  9)) / 1000 no-error.
            if error-status:error then wtax-rmc = 0.

            wtax-sn  = dec(substr(liglue, 363,  9)) / 1000 no-error.
            if error-status:error then wtax-sn = 0.

            /* DGR 21/11/2016 : PAN + Assortiment */
            wpan =   trim ( substr ( liglue , 240, 5 ) ).
            wassor = trim ( substr ( liglue , 226, 5 ) ).
            
   end . /* Articles */

   else do : /* Tarifs Achats */

        assign
           typfic   = substring ( liglue ,  1 ,  1  )
           typtar   = substring ( liglue ,  1 ,  1  )
           codart   = substring ( liglue ,  10 ,  6 )
           decond   = substring ( liglue , 17 ,  8  )
           mnreve   = 0
           mvtrai   = 0
           mvfina   = 0
           pxvindic = 0
           jj       = 0
           mm       = 0
           aa       = 0
           .

        mnreve   = dec ( substring ( liglue , 29 ,  9  ) ) no-error .
        mvtrai   = dec ( substring ( liglue , 52 ,  9  ) ) no-error .
        mvfina   = dec ( substring ( liglue , 61 ,  9  ) ) no-error .
        pxvindic = dec ( substring ( liglue , 86 ,  9  ) ) no-error .

        jj = int ( substring ( decond , 7 , 2 ) ) no-error .
        mm = int ( substring ( decond , 5 , 2 ) ) no-error .
        aa = int ( substring ( decond , 1 , 4 ) ) no-error .

        date-applic = date ( mm , jj , aa ) no-error .
        if error-status:error
        then do :
            bell. bell.
            message decond ": Date debut erronee" codaux codart
            view-as alert-box warning.
            undo, next.
        end.

        /* ERIK 13/03/97 prise en compte date de fin */
        assign decond   = substring ( liglue , 95 ,  8  )
               date-fin = ?
               .

        if decond <> ""
        then do :

            assign date-fin = ?
                   jj       = 0
                   mm       = 0
                   aa       = 0
                   .

            jj = int ( substring ( decond , 7 , 2 ) ) .
            mm = int ( substring ( decond , 5 , 2 ) ) .
            aa = int ( substring ( decond , 1 , 4 ) ) .

            date-fin = date ( mm , jj , aa ) no-error.
            if error-status:error
            then do :
                bell. bell.
                message decond ": Date fin erronee" codaux codart
                view-as alert-box warning.
                undo, next.
            end .

        end .

        assign remise = mnreve - mvtrai
               remrfc = mvtrai - mvfina
               .

        /* Ajout de CCO le 24/07/2001 pour les tarifs EUROS
           maintenant on va travailler en prix net */
        /* Ajout typtar D,C,T,R ( CC 27/08/2003 ) */
        if typtar = "E" or typtar = "Q" or
           typtar = "D" or typtar = "C" or typtar = "T" or typtar = "R"
           then assign remise = 0
                       mnreve = mvtrai
                       .
        /* Fin ajout */

        /* Prise en compte des Codes Tarifs EURO ( CC 06/07/2001 )
        if typtar = "P" then typtar = "PRO" .
                        else typtar = "   " .
        */
        run cor-code-tar .

    end . /* Tarifs Achats */

end . /* Nouveau format */


