/*==========================================================================*/
/*                          B A R - C T L 2 . I                             */
/*==========================================================================*/
/*           Zones utilise pour le module edition des ruptures              */
/*                Rupture-a               : Niveau de rupture en cours      */
/*                Rupture-nb              : Nombre de rupture existante     */
/*                Rupture-etat = "t"      : Edition du titre de la rupture  */
/*                Rupture-etat = "r"      : Edition de la rupture           */
/*--------------------------------------------------------------------------*/

DO WHILE rupture-etat = "T" and rupture-a = 1 :
    leave .
end .

/* Entete Magasin    */
/*===================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 2  :

    if substring( zone , 2 , 3 ) <> " " then do :
        hide message . pause 0 .
        message "  Traitement du Magasin ...: "
                "  " substring( zone , 2 , 3 ) .
    end .

    assign
           no-magasin = substring( zone , 2 , 3 )
           lib-magasin = " "
           lib-tit = libelle[ 40 ]
           nb-cli =  0
           tot-ht  = 0
           tot-tva = 0
           tot-ttc = 0 .

    FIND MAGASI where magasi.codsoc = magasi-soc
                and   magasi.codtab = no-magasin
                no-lock  no-error .
    if available magasi
    then  lib-magasin = magasi.libel1[ 1 ] .
    else  lib-magasin = "INEXISTANT" .

    run value( ma-entete ) .

    nb-mag = nb-mag + 1 .
    leave .

END .


/*    Entete CLIENT                  */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 3 :

    assign  edit[1]  = trim(substring( zone, 38, 10) )
            edit[2]  = substring( zone,  5, 30 )
            nb-bar   = 0
            cli-ht  = 0
            cli-tva = 0
            cli-ttc = 0 .

    FIND AUXAPR where auxapr.codsoc = codsoc-soc
                  and auxapr.typaux = substring( zone, 35 , 3 )
                  and auxapr.codaux = substring( zone, 38, 10 )
                  use-index auxapr-1 no-lock no-error .
    if not available auxapr
    then do :
        message substring( zone, 35 , 3 )  "-"  substring( zone, 38, 10 )
        " : Tiers Inexistant ..." .
        bell . bell . pause . leave .
    end .

    leave .

END .


/*    Entete BAREMES                 */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 4 :

    assign no-bareme    = substring( zone, 48, 2)
           qte-ach      = dec( substring( zone, 62, 12) )
           qte-rea      = dec( substring( zone, 74, 12) ) .

    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "TAR"
                and   tabges.prefix = "BAREME-CLI"
                and   tabges.codtab = no-bareme
                no-lock  no-error .
    if not available tabges then leave .

    assign prix-uni = tabges.nombre[2]
           plafond  = tabges.nombre[1] .

    nb-bar = nb-bar + 1 .
    leave .

END .

/*===RUPTURES EN TOTAL=====================================================*/

/*    Total  Baremes            */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 4 :

                                         /* le pris est au quintal */
   assign mont-ttc = round(( qte-ach / 100 ) * prix-uni, 2 )
          mont-ht  = round( mont-ttc / tauxtva, 2 )
          mont-tva = mont-ttc - mont-ht .

   assign cli-ht  = cli-ht  + mont-ht
          cli-tva = cli-tva + mont-tva
          cli-ttc = cli-ttc + mont-ttc .

   leave .

END .

/*    Total  CLIENT                  */
/*===================================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 3 :

    if nb-bar = 0 then leave .
    if mtsup      and cli-ttc <= mtmini then leave .
    if not mtsup  and cli-ttc >  mtmini then leave .
    { maq-raz.i "04" }

    edit[14] = string( cli-ht  , ">>>>>>9.99-" ) .
    edit[15] = string( cli-tva , ">>>>>>9.99-" ) .
    edit[16] = string( cli-ttc , ">>>>>>9.99-" ) .

   do i = 1 to 20 :
       { maq-maj.i "04" i  edit[i] }
       edit[i] = " " .
   end .

   { maq-edi.i "04" }

   assign tot-ht  = tot-ht  + cli-ht
          tot-tva = tot-tva + cli-tva
          tot-ttc = tot-ttc + cli-ttc
          nb-cli = nb-cli + 1 .
    leave .

END .


/*    Total Magasin */
/*==================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 2 :

/*   if nb-cli = 0 then leave .*/
   { maq-edi.i "07" }

   edit[1]  = "Total Depot" + " " + no-magasin + "-" + lib-magasin  .

   assign
   edit[14] = string(tot-ht   , ">>>>>>9.99-" )
   edit[15] = string(tot-tva  , ">>>>>>9.99-" )
   edit[16] = string(tot-ttc  , ">>>>>>9.99-" ) .

   do i = 1 to 16 :
       { maq-maj.i "05"  i  edit[i] }
       edit[i] = " " .
   end .
   { maq-edi.i "05" }

   { maq-edi.i "07" }

   assign gen-ht  = gen-ht  + tot-ht
          gen-tva = gen-tva + tot-tva
          gen-ttc = gen-ttc + tot-ttc
          nb-mag = nb-mag + 1 .

   leave .

END .