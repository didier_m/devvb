/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/xcba-sta.pp                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 32863 25-09-12!New!jcc            !Programme d'envoi des factures vers la GED                            !
!V61 29803 28-03-12!Evo!jcc            !Ajout affectation de STADET f(PRM)                                    !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*----------------------------------------------------------------------*/
/*                              XCBA-STA.PP                             */
/*                              -----------                             */
/*    Analyse des lignes documents pour generation STADET               */
/*----------------------------------------------------------------------*/

PROCEDURE MAJ-STADET :

    def input  parameter x-codsoc        as char           no-undo .
    def input  parameter x-codetb        as char           no-undo .
    def input  parameter x-docume        as char           no-undo .
    def output parameter x-numbon        as char           no-undo .

    def var i-i             as int                        no-undo .

    def var pied-facture    as log                        no-undo .
    def var ach-motcle      as char                       no-undo .
    def var ach-typcom      as char                       no-undo .
    def var ach-numbon      as char                       no-undo .

    def var x-operat        as char                       no-undo .
    def var x-numfac        as char                       no-undo .
    def var x-codgen        as char                       no-undo .
    def var x-cptach        as char                       no-undo .
    def var x-typaux        as char                       no-undo .
    def var x-codaux        as char                       no-undo .
    def var r-mtt-ttc       as dec                        no-undo .
    def var r-mtt-tva       as dec                        no-undo .
    def var d-datsai        as date                       no-undo .
    def var d-datech        as date                       no-undo .
    def var d-datpie        as date                       no-undo .
    def var x-devise        as char                       no-undo .
    def var x-modreg        as char                       no-undo .
    def var x-datcpt        as char                       no-undo .
    def var x-libel         as char                       no-undo .
    def var x-typiec        as char                       no-undo .
    def var x-journal       as char                       no-undo .

    assign  ach-motcle   = "ACX"
            ach-typcom   = "FAX"
            pied-facture = no
            x-numbon     = "" .       

    FOR EACH MVTCPT where  mvtcpt.codsoc  = x-codsoc
                    and    mvtcpt.codetb  = x-codetb
                    and    mvtcpt.docume  = x-docume
                    use-index primaire  no-lock  :

        /*   Si ligne fournisseur   */
        if mvtcpt.typaux <> ""  and  mvtcpt.codgen begins "4"  then
        do :
            assign  pied-facture       = yes
                    x-numfac           = mvtcpt.piece
                    x-operat           = mvtcpt.operat
                    r-mtt-ttc          = r-mtt-ttc + ( mvtcpt.ct-devini - mvtcpt.dt-devini )
                    d-datsai           = mvtcpt.datsai
                    x-typaux           = mvtcpt.typaux
                    x-codaux           = mvtcpt.codaux
                    x-modreg           = mvtcpt.modreg
                    x-devise           = mvtcpt.devini
                    d-datpie           = mvtcpt.datpie
                    d-datech           = mvtcpt.echeance
                    x-codgen           = mvtcpt.codgen
                    x-libel            = right-trim( mvtcpt.libel )
                    x-journal          = mvtcpt.journal
                    x-typiec           = mvtcpt.typie
                    x-datcpt           = string( mvtcpt.execpt , "x(4)" ) + string( mvtcpt.percpt , "x(3)" ) + string( mvtcpt.joucpt , "x(2)" )
                    .
        end .  /*  ligne 401  */

        /*   Si Ligne de TVA   */
        if mvtcpt.codgen begins "44"  then  r-mtt-tva = r-mtt-tva + ( mvtcpt.dt-devini - mvtcpt.ct-devini ) .

        /*   Si Ligne de contre partie  */
        if mvtcpt.codgen begins "6"  and  x-cptach = ""  then  x-cptach = mvtcpt.codgen .

    END .

    if pied-facture = no  then  return .

    FIND STADET where stadet.codsoc      = x-codsoc
                and   stadet.motcle      = ach-motcle
                and   stadet.numfac      = "$$$"
                and   stadet.typaux-fac  = x-typaux
                and   stadet.codaux-fac  = x-codaux

                and   stadet.docume      = x-docume
                and   stadet.typcom      = ach-typcom
                and   stadet.chrono      = 0

                use-index numfac  exclusive-lock  no-error .        
    if available stadet  then  ach-numbon = stadet.numbon .

    REPEAT :
        if ach-numbon <> ""  then  leave .
        run nocom.p( ach-typcom , today , 2 , output ach-numbon ) .
        ach-numbon = ach-numbon + "00" .
        FIND FIRST STADET where stadet.codsoc = x-codsoc
                          and   stadet.motcle = ach-motcle
                          and   stadet.typcom = ach-typcom
                          and   stadet.numbon = ach-numbon
                          no-lock  no-error .
        if available stadet  then  next .

        CREATE STADET .
        assign  stadet.codsoc = x-codsoc
                stadet.motcle = ach-motcle
                stadet.typcom = ach-typcom
                stadet.numbon = ach-numbon
                stadet.chrono = 0 .

        if x-libel <> ""  then
        do :
            CREATE STADES .
            assign  stades.codsoc     = stadet.codsoc
                    stades.motcle     = stadet.motcle
                    stades.typcom     = stadet.typcom
                    stades.numbon     = stadet.numbon
                    stades.chrono     = stadet.chrono
                    stades.theme      = "FACAEN-LIB"
                    stades.ss-chrono  = 0 .
        end .            
        leave .

    END .  /*  REPEAT  */

    assign  stadet.numfac      = "$$$"
            stadet.typaux-fac  = x-typaux
            stadet.codaux-fac  = x-codaux
            stadet.devise      = x-devise
            stadet.datfac      = d-datpie
            stadet.modreg      = x-modreg
            stadet.datech      = d-datech
            stadet.datcpt      = x-datcpt
            stadet.codgen      = x-codgen

            stadet.etabli      = x-codetb
            stadet.opecre      = x-operat 
            stadet.datcre      = d-datsai
            stadet.docume      = x-docume
            stadet.articl      = x-docume

            stadet.typaux      = x-typaux
            stadet.codaux      = x-codaux
            stadet.ref-magasin = x-numfac
            stadet.modliv      = x-journal

            stadet.fac-ttc     = r-mtt-ttc
            stadet.fac-ht      = r-mtt-tva
            stadet.codadr-liv  = 1
            stadet.codpri      = x-typiec
            stadet.datdep      = d-datsai .  

/*
            stadet.ref-tiers   = right-trim( x-banque )
            stadet.numbon-cde  = right-trim( x-domech )
            stadet.ctax        = right-trim( x-regtva )
*/    

    if x-libel <> ""  and  available stades  then
    do :
        do i-i = 1 to 6 :         
           stades.alpha[ i-i ] = substring( x-libel , ( ( i-i - 1 ) * 15 ) + 1 , 15 ) .
        end .
        stades.datcre = d-datsai .
    end .

    FIND STADET where stadet.codsoc = x-codsoc  
                and   stadet.motcle = ach-motcle
                and   stadet.typcom = ach-typcom
                and   stadet.numbon = ach-numbon
                and   stadet.chrono = 1
                exclusive-lock  no-error .
    if not available stadet  then
    do :
        CREATE STADET .
        assign  stadet.codsoc = x-codsoc
                stadet.motcle = ach-motcle
                stadet.typcom = ach-typcom
                stadet.numbon = ach-numbon
                stadet.chrono = 1 .
    end .

    assign  stadet.numfac       = x-numfac
            stadet.etabli       = x-codetb
            stadet.opecre       = x-operat 
            stadet.datcre       = d-datsai
            stadet.datdep       = d-datsai
            stadet.datcpt       = x-datcpt
            stadet.devise       = x-devise
            stadet.codgen       = x-cptach
            stadet.typaux-fac   = x-typaux
            stadet.codaux-fac   = x-codaux
            stadet.typaux       = x-typaux
            stadet.codaux       = x-codaux
            stadet.motcle-cde   = "***"
            stadet.tenusto      = "0"
            stadet.datfac       = d-datpie
            stadet.datech       = d-datech
            stadet.modreg       = x-modreg
            stadet.docume       = x-docume
            stadet.qte          = 1
            stadet.fac-ht       = r-mtt-ttc - r-mtt-tva .
/*            
        assign  stadet.articl     = right-trim( fictra.articl )
                stadet.magasin    = right-trim( fictra.magasin )
                stadet.ctax       = right-trim( fictra.codtva ) .
*/                

    VALIDATE STADET . RELEASE  STADET .
    VALIDATE STADES . RELEASE  STADES .

    x-numbon = ach-numbon .

END .  /*  PROCEDURE MAJ-STADET  */


