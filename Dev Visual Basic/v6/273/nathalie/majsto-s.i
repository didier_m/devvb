/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/majsto-s.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!V61 48902 02-09-14!Bug!jcc            !Mouchard pour MaJ En cours CDF                                        !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*--------------------------------------------------------------------------*/
/*    M A J S T O - S . I  :  Module de Mise a jour Temps Reel des stocks   */
/*                            Complement Specifique de majsto-0.p           */
/*--------------------------------------------------------------------------*/

PROCEDURE MJ-MOUCH :

    def input        parameter i-par      as char     no-undo .

    def buffer B-TABGES  for  TABGES .

    def var i-i         as int              no-undo .

    def var d-d         as datetime         no-undo .
    def var x-x         as char             no-undo .
    def var x-y         as char             no-undo .
    def var x-z         as char             no-undo .
    def var x-w         as char             no-undo .

    def var x-codsoc    as char             no-undo .
    def var x-codmag    as char             no-undo .
    def var x-codart    as char             no-undo .
    def var x-codmvt    as char             no-undo .
    def var x-prog-1    as char             no-undo .
    def var x-prog-2    as char             no-undo .
    def var x-prog-3    as char             no-undo .
    def var x-prog-4    as char             no-undo .
    def var x-prog-5    as char             no-undo .
    def var x-prog-6    as char             no-undo .
    def var x-prog-7    as char             no-undo .

    def var x-prefix    as char             no-undo .
    def var x-codtab    as char             no-undo .

    FIND B-TABGES where b-tabges.codsoc = ""
                  and   b-tabges.etabli = ""
                  and   b-tabges.typtab = "PRM"
                  and   b-tabges.prefix = "MOUCHARD"
                  and   b-tabges.codtab = "ENCCDF"
                  no-lock  no-error .
    if not available b-tabges  or  b-tabges.libel1[ 1 ] <> "O"  then  return .

    do i-i = 1  to  num-entries( i-par, "|" ) :

        assign  x-w   = entry( i-i , i-par , "|" ) 
                x-y   = entry( 1 , x-w , "=" )
                x-z   = entry( 2 , x-w , "=" )  no-error .

        case x-y :

            when  "CODSOC"    then  x-codsoc = x-z .
            when  "CODMAG"    then  x-codmag = x-z .
            when  "CODART"    then  x-codart = x-z .
            when  "CODMVT"    then  x-codmvt = x-z .
            when  "PG1"       then  x-prog-1 = x-z .
            when  "PG2"       then  x-prog-2 = x-z .
            when  "PG3"       then  x-prog-3 = x-z .
            when  "PG4"       then  x-prog-4 = x-z .
            when  "PG5"       then  x-prog-5 = x-z .
            when  "PG6"       then  x-prog-6 = x-z .
            when  "PG7"       then  x-prog-7 = x-z .

        end case .

    end .  /*  i-i  */

    DO TRANSACTION :

        assign  d-d = datetime( today , mtime )
                x-x = string( d-d ) .  /*  JJ/MM/AAAA HH:MM:SS.MMM  */

        assign  x-prefix = string( x-codsoc , "x(4)" ) + string( x-codmag , "x(4)" ) + x-codart
                x-codtab = substring( x-x ,  7 ,  4 ) + substring( x-x , 4 , 2 ) + substring( x-x , 1 , 2 ) + "-" + substring( x-x , 12 , 12 ) .

        CREATE B-TABGES .
        assign  b-tabges.codsoc = ""
                b-tabges.etabli = ""
                b-tabges.typtab = "MCH-ENCCDF"
                b-tabges.prefix = x-prefix
                b-tabges.codtab = x-codtab  no-error .
        if error-status:error = yes  then
           do : pause 1 . next . end .

        assign  b-tabges.libel1[ 1 ] = x-codmvt
                b-tabges.libel1[ 2 ] = x-prog-1
                b-tabges.libel1[ 3 ] = x-prog-2
                b-tabges.libel1[ 4 ] = x-prog-3
                b-tabges.libel1[ 5 ] = x-prog-4
                b-tabges.libel1[ 6 ] = x-prog-5
                b-tabges.libel1[ 7 ] = x-prog-6
                b-tabges.libel1[ 8 ] = x-prog-7 .

        VALIDATE B-TABGES .
        RELEASE B-TABGES .

    END .  /*  DO TRANSACTION  */

END . /*  PROCEDURE MJ-MOUCH  */

