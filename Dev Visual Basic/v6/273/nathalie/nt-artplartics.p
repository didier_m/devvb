/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/nt-artplartics.p                                                           !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 50557 13-11-14!New!jb             !Passage table ARTPLA vers ARTICS                                      !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!d-brokmini.i          !new                                  !artpla                                          !
!                      !                                     !ARTICS                                          !
!                      !                                     !artbis                                          !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/* st-artplartics.p - passage des articles de la table ARTPLA vers ARTICS     */
/*----------------------------------------------------------------------------*/
{ d-brokmini.i new }

DEFINE BUFFER b-artpla FOR artpla .
DEFINE VARIABLE r-pxacha    AS DECIMAL     NO-UNDO .
DEFINE VARIABLE r-pvind     AS DECIMAL     NO-UNDO .
DEFINE VARIABLE x-codsoc    AS CHARACTER   NO-UNDO .
DEFINE VARIABLE x-ligne     AS CHARACTER   NO-UNDO .
DEFINE VARIABLE r-ecotax    AS DECIMAL     NO-UNDO .
DEFINE VARIABLE r-poids     AS DECIMAL     NO-UNDO .
DEFINE VARIABLE r-volume    AS DECIMAL     NO-UNDO .
DEFINE VARIABLE x-eajrpd    AS CHARACTER   NO-UNDO .
DEFINE VARIABLE r-rpdtax-lb AS DECIMAL     NO-UNDO .
DEFINE VARIABLE r-rpdtax-rm AS DECIMAL     NO-UNDO .
DEFINE VARIABLE ddate       AS DATE        NO-UNDO .
DEFINE VARIABLE memo-articl AS CHARACTER   NO-UNDO .
DEFINE VARIABLE heure       AS CHARACTER   NO-UNDO .
DEFINE VARIABLE datechar    AS CHARACTER   NO-UNDO .

DEFINE STREAM sortie .

heure = STRING(TIME,"hh:mm:ss") .
heure = REPLACE(heure,":","")   .
datechar = STRING(YEAR(TODAY),"9999") + STRING(MONTH(TODAY),"99") + STRING(DAY(TODAY),"99") .

heure = "" .
datechar = "" .

OUTPUT STREAM sortie TO VALUE ("plartics" + datechar + heure + ".log") . /* Log d'erreurs */

if x-codsoc  = ""  then x-codsoc = glo-codsoc .
IF x-codsoc  = ""  THEN x-codsoc = "45"       .

lecture-artpla :

/* Lecture des articles existants */
FOR EACH artpla NO-LOCK :

    IF artpla.articl-fou  = "" THEN NEXT lecture-artpla .
    IF artpla.fournisseur = "" THEN NEXT lecture-artpla .
    IF artpla.zone-fic    = "" THEN NEXT lecture-artpla .

    /* Recherche de l'existant */                                                                
    FIND FIRST ARTICS where artics.codsoc    = ""                                                  
                        AND artics.theme     = "PLA-TIE-" + TRIM( artpla.fournisseur )   
                        AND artics.alpha-cle = artpla.articl-fou
                        AND artics.chrono    = 0                                                         
                        use-index theme  no-lock  no-error .                                        


    /* article existant : �criture dans le log*/
    IF AVAILABLE artics THEN DO :
       x-ligne = "Erreur : enregistrement ARTICS existant avec codsoc = " + x-codsoc + "; alpha-cle = " + artics.alpha-cle + "; theme : " + "PLA-TIE-" + TRIM( artpla.fournisseur ) + "; chrono = 0" .
       x-ligne = x-ligne + " Cr�� le : " + STRING(artics.datcre,"99/99/9999") + " par : " + artics.opecre .
       PUT STREAM sortie UNFORMATTED x-ligne SKIP .
       NEXT lecture-artpla .
    END.

    memo-articl = "" .
    FIND FIRST artbis WHERE  artbis.codsoc = x-codsoc
                        and  artbis.motcle = "ACH"
                        AND  artbis.typaux = "TIE"
                        AND  trim(artbis.codaux)     = trim(artpla.fournisseur)
                        and  trim(artbis.articl-fou) = trim(artpla.articl-fou)
                        use-index artbis-2
                        no-lock NO-ERROR.

    IF AVAILABLE artbis THEN memo-articl = artbis.articl .
    IF trim(memo-articl) = "" THEN memo-articl = "AR/" + trim(artpla.articl-fou) . 

    /* Recherche de l'existant */                                                                
    FIND FIRST ARTICS where artics.codsoc    = ""  
                        AND artics.articl    = memo-articl
                        AND artics.theme     = "PLA-TIE-" + TRIM( artpla.fournisseur )   
                        AND artics.chrono    = 0                                                         
                        use-index primaire  no-lock  no-error .

    IF AVAILABLE artics THEN DO :
       x-ligne = "Erreur artics index primaire : enregistrement ARTICS existant avec codsoc = " + x-codsoc + "; article = " + artics.articl + "; theme : " + "PLA-TIE-" + TRIM( artpla.fournisseur ) + "; chrono = 0" + "; alpha-cle = " + artics.alpha-cle .
       x-ligne = x-ligne + " Cr�� le : " + STRING(artics.datcre,"99/99/9999") + " par : " + artics.opecre .
       PUT STREAM sortie UNFORMATTED x-ligne SKIP .
       NEXT lecture-artpla .
    END.

    /* Lecture table "suite" */
    FIND FIRST B-ARTPLA where b-artpla.type-fic     = "E"
                        and   b-artpla.fournisseur = artpla.fournisseur
                        and   b-artpla.articl-fou  = artpla.articl-fou
                        AND   b-artpla.date-applic <> ?
                        no-lock  no-error .

    ASSIGN r-pxacha = 0
           r-pvind  = 0
           .

    /* R�cup�ration des prix */
    if available b-artpla  then
    do :
        r-pxacha = dec( substring( b-artpla.zone-fic , 43 , 9 ) )  no-error .
        if error-status:error  then  r-pxacha = 0 .
        r-pvind  = dec( substring( b-artpla.zone-fic , 86 , 9 ) )  no-error .
        if error-status:error  then  r-pvind  = 0 .
    end .

    /* R�cup�ration des valeurs */
    r-ecotax = DEC(SUBSTRING(artpla.zone-fic,290,6)) / 10000  no-error .
    if error-status:error  then  r-ecotax = 0 .   

    r-poids  = DEC(SUBSTRING(artpla.zone-fic,98,8)) / 1000  no-error .
    if error-status:error  then  r-poids  = 0 .      

    r-volume = DEC(SUBSTRING(artpla.zone-fic,232,8)) / 1000  no-error .
    if error-status:error  then  r-volume = 0 .  

    r-rpdtax-lb = DEC(SUBSTRING(artpla.zone-fic,336,9)) / 1000000  no-error .
    if error-status:error  then  r-rpdtax-lb = 0 .                                    

    r-rpdtax-rm = DEC(SUBSTRING(artpla.zone-fic,354,9)) / 1000000  no-error .
    if error-status:error  then  r-rpdtax-rm = 0 .  

    ddate = ? .
    ddate = DATE(SUBSTRING(artpla.zone-fic,316,2) + "/" + SUBSTRING(artpla.zone-fic,314,2) + "/" + SUBSTRING(artpla.zone-fic,310,4)) NO-ERROR .

/*    IF AVAILABLE artbis THEN                        */
/*        ASSIGN artics.articl = artbis.articl .      */
/*    ELSE artics.articl = "AR/" + artics.alpha-cle . */



    /* Cr�ation dans la table ARTICS */
    CREATE artics .
    ASSIGN artics.codsoc     = ""
           artics.alpha-cle  = artpla.articl-fou 
           artics.articl     = memo-articl
           artics.theme      = "PLA-TIE-" + TRIM( artpla.fournisseur ) 
           artics.chrono     = 0 
           .

    ASSIGN artics.alpha[1]   = substring( artpla.zone-fic ,  32 , 30 ) 
           artics.alpha[2]   = substring( artpla.zone-fic , 123 ,  9 ) 
           artics.alpha[3]   = substring( artpla.zone-fic , 132 , 30 ) 
           artics.alpha[4]   = substring( artpla.zone-fic ,  92 ,  1 ) 
           artics.alpha[5]   = substring( artpla.zone-fic , 195 ,  1 ) 
           artics.alpha[6]   = substring( artpla.zone-fic ,  85 ,  1 ) 
           artics.alpha[7]   = substring( artpla.zone-fic ,  69 ,  1 ) 
           artics.alpha[8]   = substring( artpla.zone-fic , 309 ,  1 ) 
           artics.alpha[9]   = substring( artpla.zone-fic ,  15 ,  8 )  
           artics.alpha[10]  = substring( artpla.zone-fic ,  24 ,  8 ) 
           artics.alpha[11]  = substring( artpla.zone-fic , 182 ,  5 ) 
           artics.alpha[12]  = substring( artpla.zone-fic , 162 , 20 ) 
           artics.alpha[13]  = substring( artpla.zone-fic ,  70 ,  2 ) 
           artics.alpha[14]  = substring( artpla.zone-fic ,  72 ,  2 ) 
           artics.alpha[15]  = substring( artpla.zone-fic , 110 , 13 )
           artics.alpha[16]  = substring( artpla.zone-fic ,  86 ,  6 ) 
           artics.alpha[17]  = substring( artpla.zone-fic , 260 ,  6 )
           artics.alpha[18]  = substring( artpla.zone-fic , 226 ,  5 )
           artics.alpha[19]  = substring( artpla.zone-fic ,  62 ,  7 )
           artics.nombre[1]  = r-pxacha
           artics.nombre[2]  = r-pvind
           artics.nombre[3]  = DEC(SUBSTRING(artpla.zone-fic,74,5))
           artics.nombre[4]  = r-ecotax
           artics.nombre[5]  = DEC(SUBSTRING(artpla.zone-fic,196,5))
           artics.nombre[6]  = DEC(SUBSTRING(artpla.zone-fic,206,5))
           artics.nombre[7]  = DEC(SUBSTRING(artpla.zone-fic,201,5))
           artics.nombre[8]  = r-poids
           artics.nombre[9]  = r-rpdtax-lb
           artics.nombre[10] = r-rpdtax-rm
           artics.nombre[11] = DEC(SUBSTRING(artpla.zone-fic,222,3))
           artics.nombre[12] = DEC(SUBSTRING(artpla.zone-fic,214,5))
           artics.nombre[13] = r-volume 
           artics.nombre[14] = dec(substring( artpla.zone-fic ,  79 ,  4 ))     
           artics.nombre[15] = dec(substring( artpla.zone-fic ,  94 ,  4 )) NO-ERROR .
           IF ddate <> ? THEN artics.datte[1]   = ddate NO-ERROR .
   ASSIGN  artics.opemaj     = "TSF"
           artics.opecre     = "TSF"
           artics.datcre     = TODAY
           artics.datmaj     = TODAY
           artics.heucre     = STRING(TIME,"hh:mm:ss")
           artics.heumaj     = STRING(TIME,"hh:mm:ss")
           .

    VALIDATE artics. RELEASE artics .


    /* Recherche de l'existant */                                                                
    FIND FIRST ARTICS where artics.codsoc    = ""                                                  
                        AND artics.theme     = "PLA-TIE-" + TRIM( artpla.fournisseur )   
                        AND artics.alpha-cle = artpla.articl-fou
                        AND artics.chrono    = 1                                                         
                        use-index theme  no-lock  no-error .

    /* article existant */
    IF AVAILABLE artics THEN DO :
       x-ligne = "zzErreur : enregistrement ARTICS existant avec codsoc = " + x-codsoc + "; alpha-cle = " + artics.alpha-cle + "; theme : " + "PLA-TIE-" + TRIM( artpla.fournisseur ) + "; chrono = 1" .
       x-ligne = x-ligne + " Cr�� le : " + STRING(artics.datcre,"99/99/9999") + " par : " + artics.opecre .
       PUT STREAM sortie UNFORMATTED x-ligne SKIP .
       NEXT lecture-artpla .
    END.

    /* Recherche de l'existant */                                                                
    FIND FIRST ARTICS where artics.codsoc    = ""  
                        AND artics.articl    = memo-articl
                        AND artics.theme     = "PLA-TIE-" + TRIM( artpla.fournisseur )   
                        AND artics.chrono    = 1                                                         
                        use-index primaire  no-lock  no-error .

    IF AVAILABLE artics THEN DO :
       x-ligne = "Erreur artics index primaire : enregistrement ARTICS existant avec codsoc = " + x-codsoc + "; article = " + artics.articl + "; theme : " + "PLA-TIE-" + TRIM( artpla.fournisseur ) + "; chrono = 1" + "; alpha-cle = " + artics.alpha-cle .
       x-ligne = x-ligne + " Cr�� le : " + STRING(artics.datcre,"99/99/9999") + " par : " + artics.opecre .
       PUT STREAM sortie UNFORMATTED x-ligne SKIP .
       NEXT lecture-artpla .
    END.

    /* Cr�ation du fichier "suite" */
    CREATE artics .
    ASSIGN artics.codsoc     = ""
/*            artics.articl     = artpla.articl-fou */
           artics.articl     = memo-articl
/*            artics.theme      = "PLA-TIE-" + substring( artpla.zone-fic , 123 ,  9 ) voir ste */
           artics.theme      = "PLA-TIE-" + TRIM( artpla.fournisseur ) 
           artics.chrono     = 1 .

    ASSIGN artics.alpha-cle  = artpla.articl-fou 
           artics.alpha[1]   = substring( artpla.zone-fic , 106 ,  2 )
           artics.alpha[2]   = substring( artpla.zone-fic , 108 ,  2 )
           artics.alpha[3]   = substring( artpla.zone-fic , 187 ,  4 )
           artics.alpha[4]   = substring( artpla.zone-fic , 191 ,  4 )
           artics.alpha[5]   = substring( artpla.zone-fic , 225 ,  1 )
           artics.alpha[6]   = substring( artpla.zone-fic , 301 ,  8 )
           artics.alpha[7]   = substring( artpla.zone-fic , 211 ,  3 )
           artics.alpha[8]   = substring( artpla.zone-fic , 219 ,  3 )
           artics.opemaj     = "TSF"
           artics.opecre     = "TSF"
           artics.datcre     = TODAY
           artics.datmaj     = TODAY
           artics.heucre     = STRING(TIME,"hh:mm:ss")
           artics.heumaj     = STRING(TIME,"hh:mm:ss")
           .

    VALIDATE artics. RELEASE artics.


END. /* FOR EACH artpla NO-LOCK : */


