/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/x-defart.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 15055 12-07-06!Evo!cch            !Acc�s ARTAPR avec soci�t� de regroupement suite mise en place ED      !
!V52 11260 08-11-04!Evo!cch            !Adaptation interfaces pour passage en V52                             !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
codtab = lignes.articl .                                                                               
{ l-articl.i }                                                                                            

if not available artapr then x-message = lignes.articl + " : Article Inexistant sur Societe " +
                                         codsoc-soc + " ..." .                    

if available artapr                                                                                       
then do :                                                                                                 

    assign lignes.tenusto = artapr.tenusto                                                                
           lignes.usto    = artapr.usto                                                                   
           lignes.ttva    = artapr.tva                                                                    
           lignes.atelier = artapr.atelier                                                                
           lignes.pdsuni  = artapr.pds                                                                    
           lignes.libart  = artapr.libart1[ mem-langue ]                                                  
           lignes.imput   = artapr.impvte                                                                 
           .                                                                                              

    if lignes.motcle = "ACH" then assign codsoc-soc   = lignes.codsoc
                                         lignes.imput = artapr.impach
                                         .                                          

    { l-artbis.i artbis                                                                                   
                 lignes.motcle                                                                            
                 codsoc-soc                                                                            
                 lignes.articl                                                                            
                 lignes.typaux                                                                            
                 lignes.codaux }                                                                          

    if not available artbis                                                                               
       then case lignes.motcle :                                                                          
                when "ACH" then x-message = lignes.articl + " : Article Inexistant sur Societe / Fournisseur " +    
                                            codsoc-soc + " / " + lignes.typaux + lignes.codaux + " ..." .   
                otherwise       x-message = lignes.articl + " : Article ( ARTBIS ) Inexistant sur Societe " +
                                            codsoc-soc + " ..." .
       end case .                                                                                         

       else do :                                                                                          

           assign lignes.ufac     = artbis.ufac                                                           
                  lignes.ucde     = artbis.ucde                                                           
                  lignes.coef-cde = artbis.coef-cde                                                       
                  lignes.coef-fac = artbis.coef-fac                                                       
                  .                                                                                       

           if artbis.libart1 <> "" and lignes.motcle = "ACH" then lignes.libart = artbis.libart1 .           

       end .                                                                                              

end . /* if available artapr */                                                                           