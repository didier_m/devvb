/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/ex-depot.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 16653 13-02-08!Evo!cch            !Gestion de la R.P.D. venant des bons magasins                         !
!V52 14686 21-04-06!Bug!cch            !Suppression mouchard sauf msg no bon cr�� pour pallier erreur violatio!
!V52 13820 12-12-05!Evo!cch            !Ajout int�gration des adresses de livraisons sur BLV                  !
!V52 13355 23-09-05!Evo!cch            !Ajout r�ception automatique lignes frais et port sur r�ceptions RAR   !
!V52 12570 04-05-05!Evo!cch            !Int�gration des R�ceptions directes LISADIS                           !
!V52 11260 08-11-04!Evo!cch            !Adaptation interfaces pour passage en V52                             !
!V52  8887 04-05-04!Evo!cch            !Ajout des mouvements de type "TRS" avec provenance "U" pour DISPAGRI M!
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
def {1} shared stream kan-rej .
def {1} shared stream kan-lec .
def {1} shared stream kan-tra .
def {1} shared stream kan-log .
def {1} shared stream kan-lig .

def var a           as int .
def var cpt-tete    as int .
def var jj          as int .
def var mm          as int .
def var aa          as int .
def var motcle-init as char no-undo .
def var i-param     as char no-undo .
def var o-param     as char no-undo .

def {1} shared var opt-mouch as char format "x" .
def {1} shared var opt-mouch-sav as char no-undo .
def {1} shared var date-arrete  as date format "99/99/9999" no-undo .
def {1} shared var anc-liglue as char .
def {1} shared var liglue     as char .
def {1} shared var x-message  as char .
def {1} shared var y-message  as char .
def {1} shared var mat-liglue as char .
def {1} shared var fichier    as char .
def {1} shared var fichtra    as char .
def {1} shared var fichrej    as char .
def {1} shared var fichlog    as char .
def {1} shared var fichlig    as char .
def {1} shared var fichlig-2  as char .
def {1} shared var niveau as int format "9" .
def {1} shared var ent-motcle like entete.motcle .
def {1} shared var ent-typcom like entete.typcom .
def {1} shared var zone as char format "x(700)" .
def {1} shared var ok-maj-bon as logical no-undo .
def {1} shared var ok-ttc-bon as logical no-undo .
def {1} shared var ttc-bon as dec extent 2 .
def {1} shared var art-persomix as char.
def {1} shared var lib-persomix as char.
def {1} shared var l-carte-fid as char.
def {1} shared var l-tarif as char extent 2 .
def {1} shared var   cdsoe  as char format  "x(2)" .
def {1} shared var   cdmac  as char format  "x(3)" .
def {1} shared var   branc  as char format  "x(2)" .
def {1} shared var   fichi  as char format  "x(8)" .
def {1} shared var   dtext  as char format  "x(6)" .
def {1} shared var   depoe  as char format  "x(6)" .
def {1} shared var   cdmou  as char format  "x(3)" .
def {1} shared var   nudoe  as char format  "x(7)" .
def {1} shared var   nupar  as char format  "x(7)" .
def {1} shared var   topsu  as char format  "x(1)" .
def {1} shared var   cdmop  as char format  "x(3)" .
def {1} shared var   recyc  as char format  "x(1)" .
def {1} shared var   cdsoc  as char format  "x(2)" .
def {1} shared var   filia  as char format  "x(2)" .
def {1} shared var   depot  as char format  "x(3)" .
def {1} shared var   agsnu  as char format  "x(7)" .
def {1} shared var   nubon  as char format  "x(7)" .
def {1} shared var   nulig  as char format  "x(3)" .
def {1} shared var   nutie  as char format  "x(6)" .
def {1} shared var   ref-mag as char format  "x(10)" .
def {1} shared var   dtbon  as char format  "x(6)" .
def {1} shared var   nuart  as char format  "x(6)" .
def {1} shared var   cdeca  as char format  "x(1)" .
def {1} shared var   ddliv  as char format  "x(6)" .
def {1} shared var   dfliv  as char format  "x(6)" .
def {1} shared var   nucde  as char format  "x(7)" .
def {1} shared var   qtcde  as char format  "x(10)" .
def {1} shared var   pxach  as char format  "x(9)" no-undo .
def {1} shared var   pxbas  as char format  "x(108)" .
def {1} shared var   cdech  as char format  "x(1)" .
def {1} shared var   mtht   as char format  "x(10)" .
def {1} shared var   mtttc  as char format  "x(10)" .
def {1} shared var   transp as char format  "x(3)" .
def {1} shared var   chauf  as char format  "x(3)" .
def {1} shared var   modliv as char format  "x(2)" .
def {1} shared var   typai  as char format  "x(7)" .
def {1} shared var   cregl  as char format  "x(8)" .
def {1} shared var   tottc  as char format  "x(10)" .
def {1} shared var   refrn  as char format  "x(15)" .
def {1} shared var   escht  as char format  "x(10)" .
def {1} shared var   esctv  as char format  "x(10)" .
def {1} shared var   tyaju  as char format  "x(2)" .
def {1} shared var   editf  as char format  "x(1)" .
def {1} shared var   nuatc  as char format  "x(6)" .
def {1} shared var   avoir  as char format  "x(1)" .
def {1} shared var   cdtva  as char format  "x(1)" .
def {1} shared var   dtech  as char format  "x(6)" .
def {1} shared var   refav  as char format  "x(7)" .
def {1} shared var   qtcnd  as char format  "x(10)" .
def {1} shared var   qtexp  as char format  "x(10)" .
def {1} shared var   qtfor  as char format  "x(1)" .
def {1} shared var   qtstk  as char format  "x(10)" .
def {1} shared var   mtt-escht as dec decimals 2     extent 10 .
def {1} shared var   mtt-esctva as dec decimals 2     extent 10 .
def {1} shared var   mtt-pxbas as dec decimals 3     extent 12 .
def {1} shared var   edit      as char format  "x" .
def {1} shared var   typcmt    as char format  "x(8)" .
def {1} shared var   libels    as char format  "x(260)" .
def {1} shared var   x-site    as char format  "x(3)" .
def {1} shared var   x-destin  as char format  "x(3)" .
def {1} shared var   x-nomes   as char format  "x(14)" .
def {1} shared var   libel-lig as char format  "x(65)" extent 5 .
def {1} shared var cdapp as char format "x(2)" .
def {1} shared var cdatc as char format "x(6)" .
def {1} shared var borde as char format "x(7)" .
def {1} shared var caiss as char format "x(2)" .
def {1} shared var cdban as char format "x(8)" .
def {1} shared var cdmaf as char format "x(4)" .
def {1} shared var cdmof as char format "x(3)" .
def {1} shared var cdope as char format "x(12)" .
def {1} shared var cdrba as char format "x(1)" .
def {1} shared var dtbof as char format "x(6)" .
def {1} shared var dtecf as char format "x(6)" .
def {1} shared var doban as char format "x(30)" .
def {1} shared var hecre as char format "x(8)" .
def {1} shared var libop as char format "x(30)" .
def {1} shared var ligop as char format "x(1)" .
def {1} shared var cpaie as char format "x(8)" .
def {1} shared var cregf as char format "x(8)" .
def {1} shared var montt as char format "x(12)" .
def {1} shared var nomti as char format "x(30)" .
def {1} shared var nuche as char format "x(7)" .
def {1} shared var nucpt as char format "x(7)" .
def {1} shared var nufac as char format "x(15)" .
def {1} shared var numan as char format "x(7)" .
def {1} shared var nupal as char format "x(7)" .
def {1} shared var nuref as char format "x(7)" .
def {1} shared var opert as char format "x(12)" .
def {1} shared var agsnf as char format "x(7)" .
def {1} shared var soapp as char format "x(2)" .
def {1} shared var scmvt as char format "x(1)" .
def {1} shared var tiroi as char format "x(3)" .
def {1} shared var topsg as char format "x(5)" .
def {1} shared var topac as char format "x(1)" .
def {1} shared var topre as char format "x(1)" .
def {1} shared var topsf as char format "x(1)" .
def {1} shared var toptr as char format "x(1)" .
def {1} shared var ntrans as char format "x(4)" .
def {1} shared var tyech as char format "x(1)" .
def {1} shared var tygen as char format "x(1)" .
def {1} shared var ctpai as char format "x(2)" .
def {1} shared var cmreg as char format "x(2)" .
def {1} shared var nuclo as char format "x(8)" .
def {1} shared var sscai as char format "x(1)" .
def {1} shared var nbeb1 as char format "x(7)" .
def {1} shared var nbel1 as char format "x(7)" .
def {1} shared var cahb1 as char format "x(11)" .
def {1} shared var cato1 as char format "x(11)" .
def {1} shared var nbeb2 as char format "x(7)" .
def {1} shared var nbel2 as char format "x(7)" .
def {1} shared var cahb2 as char format "x(11)" .
def {1} shared var cato2 as char format "x(11)" .
def {1} shared var nbeb3 as char format "x(7)" .
def {1} shared var nbel3 as char format "x(7)" .
def {1} shared var cahb3 as char format "x(11)" .
def {1} shared var cato3 as char format "x(11)" .
def {1} shared var heucre-mag             as char format "x(8)".
def {1} shared var car-fi-no              as char                no-undo .
def {1} shared var car-fi-pts             as char                no-undo .
def {1} shared var car-fi-cre             as char                no-undo .
def {1} shared var car-fi-pts-an          as char                no-undo .
def {1} shared var car-fi-operat          as char                no-undo .
def {1} shared var devise                 as char                no-undo .
def {1} shared var dev-gescom             as char                no-undo .
def {1} shared var tx-eur-frf             as dec                 no-undo .
def {1} shared var codtva-def             as char                no-undo .
def {1} shared var codtva-exo             as char                no-undo .
def {1} shared var typtva-exo             as char                no-undo .
def {1} shared var y-a-articl-usine-cdf   as logical             no-undo .
def {1} shared var y-a-articl-usine-z-cdf as logical             no-undo .
def {1} shared var y-a-articl-dispagri    as logical             no-undo .
def {1} shared var cpt-ligne-cdf          as int                 no-undo .
def {1} shared var cpt-ligne              as int                 no-undo .
def {1} shared var cpt-ligne-maj          as int                 no-undo .
def {1} shared var ref-mag-ok             as log                 no-undo .
def {1} shared var nulig-cde              as char                no-undo .
def {1} shared var solde                  as char                no-undo .
def {1} shared var l-art-fr-po            as char                no-undo .
def {1} shared var l-art-rpd              as char                no-undo .