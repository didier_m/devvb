/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/st-vma2l.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 17461 18-02-09!Bug!cch            !Correction mauvais acc�s � la ss ss famille 2                         !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
if lg-sfa = 6 then FIND TABGES where tabges.codsoc = ""
                               and   tabges.etabli = ""
                               and   tabges.typtab = "ART"
                               and   tabges.prefix = "SOUFAM" + ind-nomenc + no-famille
                               and   tabges.codtab = no-catego
                               no-lock no-error .

              else FIND TABGES where tabges.codsoc = ""
                               and   tabges.etabli = ""
                               and   tabges.typtab = "ART"
                               and   tabges.prefix = prefix-ssf + no-famille + substr( no-catego , 1 , 3 )
                               and   tabges.codtab = substr( no-catego , 5 , 3 )
                               no-lock no-error .

