/*==========================================================================*/
/*                          S T - C L B 1 2 . I                             */
/* Statistique Vente Client                                                 */
/* Etat des CA / Depot / Type Vente  / Bon                                  */
/*==========================================================================*/
/*           Zones utilise pour le module edition des ruptures              */
/*                Rupture-a               : Niveau de rupture en cours      */
/*                Rupture-nb              : Nombre de rupture existante     */
/*                Rupture-etat = "t"      : Edition du titre de la rupture  */
/*                Rupture-etat = "r"      : Edition de la rupture           */
/*--------------------------------------------------------------------------*/
 
/* Entete MAGASIN    */
/*===================*/
DO WHILE (RUPTURE-ETAT = "T" AND RUPTURE-A = 1 ) :
/* message suspendu --------------------------
    if substring( zone , 1 , 3 ) <> " " then do :
        hide message . pause 0 .
        message "  Traitement du magasin ...: "
                "  " substring( zone , 1 , 3 ) .
    end .
----------------------------------------------*/
 
    assign lib-tit = libelle[31]
           no-mag      = substring( zone , 1 , 3 )
           lib-mag     = " "
           nb-typ      = 0
           ent-mag     = yes
           .
 
    find magasi where magasi.codsoc = magasi-soc
                  and magasi.codtab = no-mag
                  no-lock  no-error .
    if available magasi then lib-mag  = magasi.libel1[mem-langue] .
 
    leave .
 
END .
 
                                                     /* C =  Comptant */
/*    Entete TYPE de BON             */              /* C <> Terme    */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 2 :
 
    assign    no-typbon   = substring( zone, 4, 1 )
              lib-typbon  = " "
              ent-typ     = yes
              nb-dat      = 0 .
 
    if no-typbon = "C"  then lib-typbon = libelle[ 32 ] .
    else lib-typbon = libelle[ 33 ] .
 
    leave .
 
END .
 
/*    Entete DATE FAC                */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 3 :
 
    assign nb-bon = 0 .
 
    leave .
 
END .
 
 
/*    Entete NUMBON    */
/*======================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 4 :
 
    assign    no-client   = substring( zone , 43, 10 )
              lib-client  = " "
              .
 
    regs-fileacc = "AUXILI" + substring( zone , 40, 3 ) .
    {regs-rec.i}
    find auxili where auxili.codsoc = regs-soc
                  and auxili.typaux = substring( zone , 40, 3 )
                  and auxili.codaux = no-client
                  no-lock no-error .
    if not available auxili then lib-client = "INEXISTANT".
    else lib-client = auxili.adres[ 1 ] .
 
    find auxapr where auxapr.codsoc = codsoc-soc
                  and auxapr.typaux = substring( zone , 40, 3 )
                  and auxapr.codaux = no-client
                  no-lock no-error .
    if not available auxapr then typ-adh  = "INEX" .
    else typ-adh = auxapr.type-adh  .
 
 
    leave .
END .
 
 
/*=========================================================================*/
 
/*    Total Recid = Chrono      */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 5 :
 
    /* CUMUL des MONTANTS                   */
    /*--------------------------------------*/
 
    assign  mont-ht  = dec(substring( zone, 68, 14))
            mont-ttc = dec(substring( zone, 82, 14))
            mont-rab = dec(substring( zone, 96, 14))
            no-bon   = substring( zone, 18, 10)
            no-fac   = substring( zone, 53, 15)
            da-fac   = substring(zone, 110, 2) + " " +
                       substring(zone, 113, 2) + " " +
                       substring(zone, 118, 2)    .
 
    assign mont[1] = mont[1] + mont-ht
           mont[2] = mont[2] + mont-ttc
           mont[3] = mont[3] + mont-rab .
 
    leave .
 
END .
 
                                               /* Imprime entete que si :   */
/*    Total NUMBON                  */         /*  au moins 1 ligne non nul */
/*==================================*/         /*---------------------------*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 4 :
 
   do while mont[1] <> 0 :
       if ent-mag  or ent-typ then run value( ma-entete ) .
 
       assign ind-tot = 00
              nb-bon = nb-bon + 1 .
 
       run st-clb1t.p .
 
       assign ent-mag  = no
              ent-typ  = no .
       leave .
   end .
 
   leave .
END .
 
 
/*    Total  DATE FAC           */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 3 :
 
   do while nb-bon <> 0 :
       assign lib-tot = libelle[ 41 ]
              ind-tot = 10
              nb-dat  = nb-dat + 1.
 
       run st-clb1t.p .
       leave .
   end .
 
   leave .
END .
 
 
/*    Total  TYPE DE BON        */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 2 :
 
   do while nb-dat <> 0 :
       assign lib-tot = libelle[ 42 ]  + lib-typbon
              ind-tot = 20
              nb-typ  = nb-typ + 1.
 
       run st-clb1t.p .
       leave .
   end .
 
   leave .
END .
 
/*    Total DEPOT   */
/*==================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 1 :
 
   do while nb-typ <> 0 :
       assign   lib-tot    = libelle[ 43 ]  + no-mag
                ind-tot    = 30
                nb-mag     = nb-mag  + 1
                no-typbon  = " "
                lib-typbon = " " .
 
       run st-clb1t.p .
       leave .
   end .
 
   leave .
END .