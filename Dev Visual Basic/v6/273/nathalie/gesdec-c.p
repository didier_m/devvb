/******************************************************************************/
/* Gesdec-c.p - GESTION DES ECHANGES DE DONNEES NATHALIE / DECLIC             */
/*----------------------------------------------------------------------------*/
/* Correspondances des Tables                       02/04/2002 - SOPRES - 273 */
/******************************************************************************/

{ connect.i      }
{ aide.i     new }
{ s-parsai.i new }

def var choix as char no-undo .

choix = trim( zone-charg[ 1 ] ) .

if choix = "" then return .

assign aide-fichier = "gesdec-0.aid"
       codsoc       = ""
       libsoc-soc   = ""
       typtab       = "DEC"
       prefix       = choix
       parsai-prog  = "*"
       aide-par     = "<" + choix + ">"
       edi-mask     = ""
       .

run aide-lec.p .

case choix :

    when "COR-NOMENC" then edi-deci = 4 .
    when "COR-CONDIT" then edi-deci = 2 .
    when "COR-UNITE"  then edi-deci = 2 .

end .

{ gfic-000.i "tabges" "codsoc" "codtab" "libel1"  "tab" }
