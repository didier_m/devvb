/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/gesmag-p.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 17654 11-06-09!Bug!cch            !Suite Fiche 17652                                                     !
!V52 17652 09-06-09!Evo!cch            !Ajout pas de rejet si Article inexistant et valeur � PAS              !
!V52 16900 04-06-08!Evo!cch            !Suite fiche 16592 : Mise au point et �volutions suite tests du client !
!V52 16653 13-02-08!Evo!cch            !Gestion de la R.P.D. venant des bons magasins                         !
!V52 16592 24-01-08!Evo!cch            !Ajout Gestion de la modification des commandes CCC et CCI             !
!V52 16478 30-11-07!Evo!cch            !Ajout gestion des bons Litiges ( LIT ) et Retour ( RET )              !
!V52 16205 05-09-07!Bug!cch            !REI et REC issus des magasins non mis en attente                      !
!V52 16162 21-08-07!Evo!cch            !Initialisation par d�faut statut EUREA DISTRI sans passage sur la zone!
!V52 16136 06-08-07!Evo!cch            !Autorisation montants <= 0 + Quantit� sign�e et non plus Prix         !
!V52 16032 03-07-07!Evo!cch            !R�ceptions des CDF S.D. faites en magasin et non plus au si�ge        !
!V52 15975 18-06-07!Bug!cch            !Correction chargement Type de pi�ce CAF et RAF non fait               !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/

/******************************************************************************/
/* GESMAG-P.I - GESTION DE L'INTEGRATION DES BONS MAGASINS                    */
/*----------------------------------------------------------------------------*/
/* Procedures Communes                                  02/2006 - EUREA - 273 */
/******************************************************************************/

/* Initialisations*/
PROCEDURE CHARG-PARAM :

    assign ok-trt     = yes
           codsoc-soc = ""
           .

    /* Acces au parametrage general */
    assign  typtab = "MAG"  prefix = "GES-MAGASI"  codtab = "PARAMETRES" .
    run ch-paray.p( output o-param ) .

    if o-param = ""
    then do :
        bell . bell .
        message libelle[ 61 ] skip libelle[ 62 ] skip libelle[ 63 ]
                view-as alert-box error .
        ok-trt = no .
        return .
    end .

    assign repert-arriv    = entry( 1 , o-param, "|" ) /* Rep. import fichiers                          */ 
           repert-traite   = entry( 2 , o-param, "|" ) /* Rep. sauvegarde fichiers                      */
           repert-rejet    = entry( 3 , o-param, "|" ) /* Rep. rejets fichiers                          */
           repert-trav     = entry( 4 , o-param, "|" ) /* Rep. de travail                               */
           operat-maj      = entry( 5 , o-param, "|" ) /* Operateur MAJ                                 */
           nomimp          = entry( 6 , o-param, "|" ) /* Imprimante                                    */
           l-fic           = entry( 7 , o-param, "|" ) /* Radicals Noms Fichiers                        */
           l-ext           = entry( 8 , o-param, "|" ) /* Extensions Fichiers                           */
           l-soc           = entry( 9 , o-param, "|" ) /* Societes autorisees                           */
           l-typenr        = entry( 10, o-param, "|" ) /* Types d'enregstrements Magasins               */
           l-codmvt[ 1 ]   = entry( 31, o-param, "|" ) /* Types de Bons Magasins                        */  
           l-codmvt[ 2 ]   = entry( 32, o-param, "|" ) /* Types de Bons Magasins traites par ex-depot.p */           
           l-param-def     = entry( 33, o-param, "|" ) /* Parametres divers par defaut                  */
           l-art-persomix  = entry( 34, o-param, "|" ) /* Codes Articles Persomix                       */
           lib-persomix    = entry( 35, o-param, "|" ) /* Libelle Commentaire persomix                  */
           l-code-tar[ 1 ] = entry( 36, o-param, "|" ) /* Codes Tarifs Magasins                         */
           l-code-tar[ 2 ] = entry( 37, o-param, "|" ) /* Codes Tarifs Nathalie correspondants          */
           l-typcom-cde    = entry( 38, o-param, "|" ) /* Types de Bons Commandes Nathalie              */
           l-typcom-liv    = entry( 39, o-param, "|" ) /* Types de Bons Livraisons Nathalie             */
           l-top-solde     = entry( 40, o-param, "|" ) /* Tops Solde Commande Magasin                   */
           l-top-sup       = entry( 41, o-param, "|" ) /* Tops Suppression Magasin                      */
           l-top-qte-lis   = entry( 42, o-param, "|" ) /* Tops Envoi stocks et qtes vendues a LISADIS   */
           l-top-o-n       = entry( 43, o-param, "|" ) /* Tops Oui ou Non                               */
           l-art-esc       = entry( 44, o-param, "|" ) /* Codes Articles d'Escompte                     */
           codrem-rfa      = entry( 45, o-param, "|" ) /* Code Remise sur Facture                       */  
           tiers-ed        = entry( 46, o-param, "|" ) /* Tiers EUREA DISTRI                            */  
           l-enr-deb-fin   = entry( 47, o-param, "|" ) /* Enreg. pour Debut et Fin de Fichier           */  
           l-codmvt[ 3 ]   = entry( 48, o-param, "|" ) /* Types de Bons Magasins non traites            */           
           codsoc-ed       = entry( 49, o-param, "|" ) /* Code Soci�t� EUREA DISTRI                     */  
           l-art-rpd       = entry( 50, o-param, "|" ) /* Codes Articles Redevance Pollution Diffuse    */  
           .

    if zone-charg[ 1 ] = "REJET"      then trt-rejet   = yes .
    if zone-charg[ 2 ] = "TEST"       then trt-test    = yes .    
    if zone-charg[ 3 ] = "ECLATEMENT" then trt-eclat   = yes .    
    if zone-charg[ 3 ] = "INTEGRAT"   then trt-integr  = yes .    
    if zone-charg[ 4 ] = "ENCHAINEM"  then trt-enchain = yes .    

    if operat-maj <> "" then operat = operat-maj .
    if nomimp     <> "" then opeimp = nomimp .

    assign motcle-acp      = "ACP"
           motcle-ach      = "ACH"
           motcle-vte      = "VTE"
           niveau-bon[ 1 ] = 1
           niveau-bon[ 2 ] = 2
           niveau-cde      = ""
           niveau-liv      = "3"
           niveau-val      = "4"
           niveau-prefac   = "5"
           niveau-fac      = "6"
           niveau-sta      = "7"
           top-trt-ed      = "EDN"
           top-non-a       = "N"
           top-0-a         = "0"
           top-1-a         = "1"
           top-9-a         = "9"
           top-1-n         = 1
           top-3-n         = 3
           tenusto-n-stock = "0"
           tenusto-stock   = "1"
           tenusto-valeur  = "2"
           codpri-gratuit  = "G"
           codpri-attente  = "A"
           codlig-normale  = "N"
           codlig-avoir    = "A"
           codent-solde    = "SLL"
           codent-vac      = "VAC"
           codent-val      = "VAL"
           mvtsto-cdc      = "100"
           mvtsto-cdf      = "600"
           mvtsto-rgs      = "400"
           mvtsto-trs      = "350"
           mvtsto-tre      = "850"
           mvtsto-liv      = "200"
           mvtsto-rcf      = "750"
           mvtsto-rcf-enc  = "650"
           mvtsto-liv-enc  = "150"
           date-j          = string( today, "999999" )                  + "-" +
                             substr( string( time, "hh:mm:ss" ), 1, 2 ) +
                             substr( string( time, "hh:mm:ss" ), 4, 2 ) +
                             substr( string( time, "hh:mm:ss" ), 7, 2 )
           typaux-ed       = substr( tiers-ed, 1, 3 )
           codaux-ed       = substr( tiers-ed, 4 )
           .

    if trt-eclat
    then do :
        fic-rac[ 1 ] = entry( 1, l-fic ) .
        fic-rac[ 2 ] = entry( 2, l-fic ) no-error .
    end.
    else do :
        fic-rac[ 1 ] = entry( 2, l-fic ) no-error .
        fic-rac[ 2 ] = entry( 1, l-fic ) .
    end .

    ext-trt    = entry( 1, l-ext ) .
    ext-tst    = entry( 2, l-ext ) no-error .
    ext-rej    = entry( 3, l-ext ) no-error .
    ext-blo    = entry( 4, l-ext ) no-error .
    ext-log    = entry( 5, l-ext ) no-error .

    typaux-def = entry( 1 , l-param-def ) .

    codpri-cli = entry( 2 , l-param-def ) no-error .
    codpri-dir = entry( 3 , l-param-def ) no-error .
    codpri-htc = entry( 4 , l-param-def ) no-error .
    codpri-pre = entry( 5 , l-param-def ) no-error .
    codpri-ach = entry( 6 , l-param-def ) no-error .

    typbon-dir = entry( 7 , l-param-def ) no-error .
    typbon-mts = entry( 8 , l-param-def ) no-error .
    typbon-pro = entry( 9 , l-param-def ) no-error .
    typbon-std = entry( 10, l-param-def ) no-error .
    typbon-ter = entry( 11, l-param-def ) no-error .
    typbon-lit = entry( 12, l-param-def ) no-error .
    typbon-ret = entry( 13, l-param-def ) no-error .

    typcom-car = entry( 1,  l-typcom-cde ) .  
    typcom-cdf = entry( 2,  l-typcom-cde ) no-error .  
    typcom-cdi = entry( 3,  l-typcom-cde ) no-error .  
    typcom-cdm = entry( 4,  l-typcom-cde ) no-error .  
    typcom-ccc = entry( 5,  l-typcom-cde ) no-error .  
    typcom-rea = entry( 6,  l-typcom-cde ) no-error .  
    typcom-rec = entry( 7,  l-typcom-cde ) no-error .  
    typcom-rem = entry( 8,  l-typcom-cde ) no-error .  
    typcom-caf = entry( 9,  l-typcom-cde ) no-error .  
    typcom-cci = entry( 10, l-typcom-cde ) no-error .  

    typcom-blf = entry( 1 , l-typcom-liv ) .
    typcom-bls = entry( 2 , l-typcom-liv ) no-error .
    typcom-cfi = entry( 3 , l-typcom-liv ) no-error .
    typcom-cdl = entry( 4 , l-typcom-liv ) no-error .
    typcom-rar = entry( 5 , l-typcom-liv ) no-error .
    typcom-rcf = entry( 6 , l-typcom-liv ) no-error .
    typcom-rgs = entry( 7 , l-typcom-liv ) no-error .
    typcom-trc = entry( 8 , l-typcom-liv ) no-error .
    typcom-tre = entry( 9 , l-typcom-liv ) no-error .
    typcom-trs = entry( 10, l-typcom-liv ) no-error .
    typcom-raf = entry( 11, l-typcom-liv ) no-error .
    typcom-liv = entry( 12, l-typcom-liv ) no-error .

    if trt-eclat
    then do :
        if fic-rac[ 1 ] = "" then fic-rac[ 1 ] = "prodepv5" .
        if fic-rac[ 2 ] = "" then fic-rac[ 2 ] = "prodepv5-1" .
    end .
    else do :
        if fic-rac[ 1 ] = "" then fic-rac[ 1 ] = "prodepv5-1" .
        if fic-rac[ 2 ] = "" then fic-rac[ 2 ] = "prodepv5" .
    end .

    if ext-trt       = "" then ext-trt       = ".trt" .
    if ext-tst       = "" then ext-tst       = ".tst" .
    if ext-rej       = "" then ext-rej       = ".rej" .
    if ext-blo       = "" then ext-blo       = ".blo" .
    if ext-log       = "" then ext-log       = ".csv" .

    if typaux-def    = "" then typaux-def    = "TIE" .

    if codpri-cli    = "" then codpri-cli    = "CLI" .
    if codpri-dir    = "" then codpri-dir    = "DIR" .
    if codpri-htc    = "" then codpri-htc    = "HTC" .
    if codpri-pre    = "" then codpri-ach    = "PRE" .
    if codpri-ach    = "" then codpri-ach    = "STE" .

    if typbon-dir    = "" then typbon-dir    = "D" .
    if typbon-lit    = "" then typbon-lit    = "L" .
    if typbon-mts    = "" then typbon-mts    = "M" .
    if typbon-pro    = "" then typbon-pro    = "P" .
    if typbon-ret    = "" then typbon-ret    = "R" .
    if typbon-std    = "" then typbon-std    = "S" .
    if typbon-ter    = "" then typbon-ter    = "T" .

    if l-top-solde   = "" then l-top-solde   = " ,S,V" . /* 'S' = solde manuel - 'V' = solde automatique */
    if l-top-sup     = "" then l-top-sup     = "F,S" .
    if l-top-qte-lis = "" then l-top-qte-lis = "E,T,R" .
    if l-top-o-n     = "" then l-top-o-n     = "O,N" .
    if codrem-rfa    = "" then codrem-rfa    = "RFA" .

    if typcom-caf    = "" then typcom-caf    = "CAF" .
    if typcom-car    = "" then typcom-car    = "CAR" .
    if typcom-ccc    = "" then typcom-ccc    = "CCC" .
    if typcom-cci    = "" then typcom-cci    = "CCI" .
    if typcom-cdf    = "" then typcom-cdf    = "CDF" .
    if typcom-cdi    = "" then typcom-cdi    = "CDI" .
    if typcom-cdm    = "" then typcom-cdm    = "CDM" .
    if typcom-rea    = "" then typcom-rea    = "REA" .
    if typcom-rec    = "" then typcom-rec    = "REC" .
    if typcom-rem    = "" then typcom-rem    = "REM" .

    if typcom-blf    = "" then typcom-blf    = "BLF" .
    if typcom-bls    = "" then typcom-bls    = "BLS" .
    if typcom-cfi    = "" then typcom-cfi    = "CFI" .
    if typcom-cdl    = "" then typcom-cdl    = "CDL" .
    if typcom-liv    = "" then typcom-liv    = "LIV" .
    if typcom-raf    = "" then typcom-raf    = "RAF" .
    if typcom-rar    = "" then typcom-rar    = "RAR" .
    if typcom-rcf    = "" then typcom-rcf    = "RCF" .
    if typcom-rgs    = "" then typcom-rgs    = "RGS" .
    if typcom-trc    = "" then typcom-trc    = "TRC" .
    if typcom-tre    = "" then typcom-tre    = "TRE" .
    if typcom-trs    = "" then typcom-trs    = "TRS" .

    if typaux-ed = "" then typaux-ed = "TIE" .
    if codaux-ed = "" then codaux-ed = "    999100" .
    if codsoc-ed = "" then codsoc-ed = "45" .

    if not trt-rejet
    then do :
        fic-trt = repert-arriv + fic-rac[ 1 ] .
        if not trt-test then fic-trt = fic-trt + ext-trt .
                        else fic-trt = fic-trt + ext-tst .
    end .
    else fic-trt = fic-rej .

    assign fic-blo[ 1 ] = repert-trav   + fic-rac[ 1 ] + ext-blo
           fic-blo[ 2 ] = repert-trav   + fic-rac[ 2 ] + ext-blo
           fic-ren      = repert-traite + fic-rac[ 1 ] + ext-trt + date-j
           fic-rej      = repert-rejet  + fic-rac[ 1 ] + ext-rej + date-j
           fic-log      = repert-trav   + fic-rac[ 1 ] + "-" + date-j + ext-log
           .

    /* Chargement Typaux + Codaux des Plateformes */
    assign  typtab = "PRM"  prefix = "COD-PLATEF"  codtab = "" .
    run ch-parax.p( output o-param ) .
    if o-param <> "" then assign typaux-lis  = substr( entry( 1, o-param, "/" ), 1, 3 )
                                 codaux-lis  = substr( entry( 1, o-param, "/" ), 4 )
                                 typaux-lisv = substr( entry( 3, o-param, "/" ), 1, 3 )
                                 codaux-lisv = substr( entry( 3, o-param, "/" ), 4 )
                                 .

    if typaux-lis  = "" then typaux-lis  = "TIE" .
    if codaux-lis  = "" then codaux-lis  = "    802190" .
    if typaux-lisv = "" then typaux-lisv = "TIE" .
    if codaux-lisv = "" then codaux-lisv = "    805100" .

END PROCEDURE . /* CHARG-PARAM */

/* Chargement donnees societe */
PROCEDURE CHARG-SOCIETE :

    regs-app = "ELODIE" .
    { regs-cha.i }

    regs-app = "NATHALIE" .
            { regs-cha.i }

    { regs-inc.i "magasi" }   
    magasi-soc = regs-soc .           

    { regs-inc.i "articl" }           
    articl-soc = regs-soc .           

    { regs-inc.i "auxspe" }           
    auxspe-soc = regs-soc .           

    { regs-inc.i "proplatef" }        
    proplatef-soc = regs-soc .        

    date-arrete = today - 1 .         

    /* Arrete Global */                                                         
    FIND LAST TABGES where tabges.codsoc      = codsoc-soc
                     and   tabges.etabli      = ""                                      
                     and   tabges.typtab      = "STO"                         
                     and   tabges.prefix      = "CALEND"                                
                     and   tabges.nombre[ 1 ] = 1                                       
                     use-index primaire no-lock no-error .                                    

    if available tabges then date-arrete = tabges.dattab[ 1 ] .                         

    FIND TABGES where tabges.codsoc = codsoc-soc                                        
                and   tabges.etabli = ""                                                
                and   tabges.typtab = "STO"                                             
                and   tabges.prefix = "DAT-ARRINV"                                      
                and   tabges.codtab = ""                                                
                no-lock no-error .                                                      

    if available tabges and tabges.dattab[ 1 ] > date-arrete                            
       then date-arrete = tabges.dattab[ 1 ] .                                          

    /* Arrete Vente */                                                                  
    FIND LAST TABGES where tabges.codsoc      = codsoc-soc                              
                     and   tabges.etabli      =  ""                                     
                     and   tabges.typtab      =  "STO"                                  
                     and   tabges.prefix      =  "CALEND"                               
                     and   tabges.nombre[ 5 ] = 1                                       
                     no-lock  no-error .                                                

    if available tabges and tabges.dattab[ 1 ] > date-arrete                            
       then date-arrete-vte = tabges.dattab[ 1 ] .                                      


    /* Chargement Parametres pour traitement Cartes de Fidelite */                      
    assign  typtab = "PRM"  prefix = "CARTE-FID"  codtab = "" .                         
    run ch-parax.p( output l-carte-fid ) .                                              

    /* Chargement Articles frais et port Plateforme LISADIS */                          
    assign  typtab = "PLA"  prefix = "GES-PLATEF"  codtab = "PARAMETRES" .              
    run ch-paray.p( output o-param ) .                                                  
    l-art-fr-po  = entry( 31, o-param, "|" ) no-error .                                 

    /* Chargement devise de Gestion */                                                  
    assign  typtab = "PRM"  prefix = "TX-EUR-FRF"  codtab = "" .                        
    run ch-parax.p( output o-param ) .                                                  
    if o-param <> "" then assign dev-gescom = entry( 1 , o-param , "/" )                
                                 tx-eur-frf = dec( entry( 11 , o-param , "/" ) )        
                                 .                                                      
    if dev-gescom = "" then dev-gescom = "EUR" .                                        
    if tx-eur-frf = 0 then tx-eur-frf = 6.55957 .                                       

    /* Chargement infos TVA */                                                          
    assign  typtva-exo = "002"  codtva-exo = "000"  codtva-def = "001"                  
           typtab = "PRM"  prefix = "INFO-TVA"  codtab = "" .                           
    run ch-parax.p( output o-param ) .                                                  
    if o-param <> "" then assign typtva-exo = entry( 1 , o-param , "/" )                
                                 codtva-exo = entry( 2 , o-param , "/" )                
                                 codtva-def = entry( 3 , o-param , "/" )                
                                 .                                                      

    even-pam = "STOARR" .                                                               
    assign  typtab = "PRM"  prefix = "PX-ACH-MOY"  codtab = "EVENEMENT" .               
    run ch-param.p( output o-param ) .                                                  
    if o-param <> "" then even-pam = o-param .                                          

END PROCEDURE . /* CHARG-SOCIETE */

/* Controle valeur numerique entier */
PROCEDURE CTRL-VAL-INT :

    /* entry( 4, zone ) = "1" -> = valeur 0 acceptee */

    if num-entries( zone ) < 3 then zone = zone + ",1" .
    if num-entries( zone ) < 4 then zone = zone + ",0" .

    val-int = int( entry( 1, zone ) ) / int( entry( 3, zone ) ) no-error .
    if error-status:error or val-int = ? or val-int < 0 or ( val-int = 0 and entry( 4, zone ) <> "1" )
    then do :
        mess-err = entry( 1, zone ) + libelle[ int( entry( 2, zone ) ) ] .
        run trt-rejet .
    end .

    /* Controle valeur entiere */
    if not error-status:error and val-int <> dec( entry( 1, zone ) ) / int( entry( 3, zone ) )
    then do :
        mess-err = entry( 1, zone ) + libelle[ int( entry( 2, zone ) ) ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-VAL-INT */

/* Controle valeur numerique decimale */
PROCEDURE CTRL-VAL-DEC :

    /* entry( 2, zone ) = "1" -> = valeur 0 acceptee            */
    /* entry( 2, zone ) = "2" -> = valeur < 0 acceptee          */
    /* entry( 2, zone ) = "3" -> = valeur <= 0 acceptee         */
    /* entry( 4, zone ) = "A" -> = si - val-dec < 0 alors rejet */

    val-dec = dec( entry( 1, zone ) ) no-error .
    if error-status:error or val-dec = ?                                         or
     ( val-dec = 0 and ( entry( 2, zone ) <> "1" and entry( 2, zone ) <> "3" ) ) or
     ( val-dec < 0 and ( entry( 2, zone ) <> "2" and entry( 2, zone ) <> "3" ) )
    then do :
        mess-err = entry( 1, zone ) + libelle[ int( entry( 3, zone ) ) ] .
        run trt-rejet .
        return .
    end .

    if num-entries( zone ) < 4 then return .

    zone = entry( 4, zone ) .
    if zone = codlig-avoir
    then do :
        mess-err = string( - val-dec ) + libelle[ int( entry( 3, zone ) ) ] .
        run trt-rejet .
        return .
    end .

END PROCEDURE . /* CTRL-VAL-DEC */

/* Controle de date - date-a = AAMMJJ */
PROCEDURE CTRL-DATE :

    /* entry( 2, zone ) = "1" -> = valeur ? acceptee */

    if ( entry( 1, zone ) = "" or entry( 1, zone ) = fill( "0", 6 ) ) and entry( 2, zone ) = "1"
    then do :
        val-date = ? .
        return .
    end .

    if substr( entry( 1, zone ), 1, 2 ) <= "50" 
        then entry( 1, zone ) = "20" + entry( 1, zone ) .
        else entry( 1, zone ) = "19" + entry( 1, zone ) .

    val-date = date( int( substr( entry( 1, zone ), 5, 2 ) ),
                     int( substr( entry( 1, zone ), 7, 2 ) ),
                     int( substr( entry( 1, zone ), 1, 4 ) ) ) no-error .

    if error-status:error or ( val-date = ? and entry( 2, zone ) <> "1" )
    then do :
        mess-err = entry( 1, zone ) + libelle[ int( entry( 3, zone ) ) ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-DATE */

/* Controle Devise */
PROCEDURE CTRL-DEVISE :

    if entry( 1, zone ) <> substr( dev-gescom, 1, 1 )
    then do :
        mess-err = entry( 1, zone ) + libelle[ 100 ] .
        run trt-rejet .
        return .
    end .

    if num-entries( zone ) = 1 then return .

    if entry( 2, zone ) <> dev-gescom
    then do :
        mess-err = entry( 2, zone ) + libelle[ 101 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-DEVISE */

/* Controle Tiers Gestion */
PROCEDURE CTRL-AUXAPR :

    FIND AUXAPR where auxapr.codsoc = auxspe-soc
                and   auxapr.typaux = type-aux
                and   auxapr.codaux = code-aux 
                no-lock no-error .

    if not available auxapr and b-lignes-trav.codmvt <> "FIN"
    then do :
        mess-err = auxspe-soc + " " + type-aux + " " + code-aux + libelle[ 83 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-AUXAPR */

/* Controle Tiers Compta. */
PROCEDURE CTRL-AUXILI :

    regs-fileacc = "AUXILI" + type-aux .
    { regs-rec.i }

    FIND AUXILI where auxili.codsoc = regs-soc
                and   auxili.typaux = type-aux
                and   auxili.codaux = code-aux 
                no-lock no-error .

    if not available auxili and b-lignes-trav.codmvt <> "FIN"
    then do :
        mess-err = regs-soc + " " + type-aux + " " + code-aux + libelle[ 84 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-AUXILI */

/* Controle Chauffeur */
PROCEDURE CTRL-CHAUFFEUR :

    FIND TABGES where tabges.codsoc = ""                            
                and   tabges.etabli = ""                        
                and   tabges.typtab = "APR"                         
                and   tabges.prefix = "CHAUFFEUR"                   
                and   tabges.codtab = chauffeur                         
                no-lock no-error .                                  

    if not available tabges                                         
    then do :                                                       
        mess-err = chauffeur + " " + libelle[ 87 ] .
        run trt-rejet .
    end . 

END PROCEDURE . /* CTRL-CHAUFFEUR */

/* Controle Transporteur */
PROCEDURE CTRL-TRANSP :

    FIND TRANSP where transp.codsoc = ""
                and   transp.transp = transp
                no-lock no-error .

    if not available transp
    then do :
        mess-err = transp + " " + libelle[ 88 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-TRANSP */

/* Controle Mode de Livraison */
PROCEDURE CTRL-MODE-LIV :

    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "APR"
                and   tabges.prefix = "MODLIV"
                and   tabges.codtab = mode-liv
                no-lock no-error .

    if not available tabges
    then do :
        mess-err = mode-liv + " " + libelle[ 98 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-MODE-LIV */

/* Controle Representant */
PROCEDURE CTRL-REPRES :

    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "CLI"
                and   tabges.prefix = "REPRES"
                and   tabges.codtab = repres
                no-lock no-error .

    if not available tabges
    then do :
        mess-err = repres + " " + libelle[ 115 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-REPRES */

/* Controle Entete */
PROCEDURE CTRL-ENTETE :

    case typcom-trt :

        when typcom-ccc or when typcom-cci
        then do :

            FIND ENTETE where entete.codsoc = codsoc-soc
                        and   entete.motcle = motcle-trt
                        and   entete.typcom = typcom-trt
                        and   entete.numbon = numbon-trt
                        exclusive-lock no-error .

        end .

        when typcom-cdm
        then do :

            FIND ENTETE where entete.codsoc = codsoc-soc
                        and   entete.motcle = motcle-trt
                        and   entete.typcom = typcom-trt
                        and   entete.numbon = numbon-trt
                        exclusive-lock no-error .

            if available entete and entete.zon-int-2 = top-1-n
            then do :
                mess-err = codsoc-soc + " " + motcle-trt + " "  + typcom-trt + " " + numbon-trt + " " +
                           libelle[ 110 ] .
                run trt-rejet .
            end .

        end .

        otherwise do :

            FIND ENTETE where entete.codsoc = codsoc-soc
                        and   entete.motcle = motcle-trt
                        and   entete.typcom = typcom-trt
                        and   entete.numbon = numbon-trt
                        no-lock no-error .

            if available entete
            then do :
                mess-err = codsoc-soc + " " + motcle-trt + " "  + typcom-trt + " " + numbon-trt + " " +
                           libelle[ 86 ] .
                run trt-rejet .
            end .

        end .

    end case .

END PROCEDURE . /* CTRL-ENTETE */

/* Controle Article */
PROCEDURE CTRL-ARTAPR :

    { cad-arti.i code-art } 

    FIND ARTAPR where artapr.codsoc = articl-soc
                and   artapr.articl = code-art
                no-lock no-error .

    if not available artapr and ( b-lignes-trav.codmvt <> "VEN" or substr( b-lignes-trav.enreg, 78, 9 ) <> "PAS_SAISI" )
    then do :
        mess-err = articl-soc + " " + code-art + libelle[ 91 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-ARTAPR */

/* Controle Article Vente */
PROCEDURE CTRL-ARTBIS-VTE :

    FIND ARTBIS where artbis.motcle = "VTE"
                and   artbis.codsoc = articl-soc
                and   artbis.articl = code-art
                and   artbis.typaux = ""    
                and   artbis.codaux = ""
                no-lock no-error .

    if not available artbis
    then do :
        mess-err = articl-soc + " " + code-art + libelle[ 92 ] .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-ARTBIS-VTE */

/* Controle Article Achat */
PROCEDURE CTRL-ARTBIS-ACH :

    /* zone = "1" -> inexistant accepte */

    FIND ARTBIS where artbis.motcle = "ACH"
                and   artbis.codsoc = codsoc-soc
                and   artbis.articl = code-art
                and   artbis.typaux = type-aux    
                and   artbis.codaux = code-aux
                no-lock no-error .

    if not available artbis and zone <> "1" 
    then do :
        mess-err = codsoc-soc + " " + code-art + libelle[ 102 ] + type-aux + " " + code-aux + " ..." .
        run trt-rejet .
    end .

END PROCEDURE . /* CTRL-ARTBIS-ACH */

/* Traitement des Rejets */
PROCEDURE TRT-REJET :

    if nb-err = 0
    then do :
        output stream s-fic-rej to value( fic-rej ) .
        mess-lib = chr( 27 ) + "&l1O" + "/* " . /* Mise a l'italienne */
        if not trt-eclat then mess-lib = mess-lib + libelle[ 25 ] .
                         else mess-lib = mess-lib + libelle[ 43 ] .
        mess-lib = mess-lib + " " + string( today ) + " " + string( time, "hh:mm:ss" ) .
        put stream s-fic-rej unformatted mess-lib skip( 1 ) .
        mess-lib = "/* " + libelle[ 26 ] + " " + fic-rej .
        put stream s-fic-rej unformatted mess-lib skip .
        mess-lib = "/* " + libelle[ 30 ] + " " + fic-trt .
        put stream s-fic-rej unformatted mess-lib skip( 2 ) .
    end .

    if maj-bon then if available b-lignes-trav then no-enreg = b-lignes-trav.no-enreg .
                                               else no-enreg = lignes-trav.no-enreg .
               else no-enreg = nb-enr-lu .

    mess-lib = libelle[ 72 ] + string( no-enreg ) .
    put stream s-fic-rej unformatted mess-lib skip .
    mess-err = "/* " + mess-err .
    put stream s-fic-rej unformatted mess-err skip .

    if maj-bon
    then do :

        assign ok-trt-bon = no
               mess-lib   = "/* " + codsoc-soc + " " + lignes-trav.magasin + " " + lignes-trav.codmvt +
                            " " + lignes-trav.numbon + libelle[ 73 ]
               .

        put stream s-fic-rej unformatted mess-lib skip .

        FOR EACH B-LIGNES-TRAV where b-lignes-trav.codsoc  = lignes-trav.codsoc
                               and   b-lignes-trav.magasin = lignes-trav.magasin
                               and   b-lignes-trav.codmvt  = lignes-trav.codmvt
                               and   b-lignes-trav.numbon  = lignes-trav.numbon
                               use-index i-lignes-1 no-lock : 

            mess-lib = libelle[ 72 ] + string( b-lignes-trav.no-enreg ) .
            put stream s-fic-rej unformatted mess-lib skip .

            put stream s-fic-rej unformatted b-lignes-trav.enreg skip .        

        END .

        put stream s-fic-rej unformatted skip( 1 ) .

    end . /* if maj-bon */

    else put stream s-fic-rej unformatted enreg skip( 1 ) .

    nb-err = nb-err + 1 .

END PROCEDURE . /* TRT-REJET */