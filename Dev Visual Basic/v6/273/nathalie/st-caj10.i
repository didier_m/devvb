/*==========================================================================*/
/*                          S T - C A J 1 0 . I                             */
/* Statistique Vente Client                                                 */
/* Etat des CA Journalier par Date de Bon / Magasin Responsable             */
/*==========================================================================*/
 
/* Tbl des Libelles Etat & Tbl d'Edition & enreg fichier tri */
def {1} shared var libelle       as char                 extent 99   no-undo .
def {1} shared var edit          as char                 extent 50   no-undo .
def {1} shared var zone          as char                             no-undo .
def {1} shared stream maquette.                             /* canal edition*/
def {1} shared stream kan-tri .
 
/* zone de traitement */
def {1} shared var int-datbon   as int                             no-undo .
def {1} shared var nb-dat       as int                             no-undo .
def {1} shared var nb-mag       as int                             no-undo .
def {1} shared var nb-bon       as int                             no-undo .
 
def {1} shared var no-mag       as char                            no-undo .
def {1} shared var lib-mag      as char                            no-undo .
def {1} shared var moisan       as char                            no-undo .
def {1} shared var z-typcom     as char                            no-undo .
def {1} shared var z-numbon     as char                            no-undo .
def {1} shared var z-typbon     as char                            no-undo .
def {1} shared var z-datbon     as char                            no-undo .
 
def {1} shared var mont         as dec                extent 550   no-undo .
def {1} shared var ind-tot      as int                             no-undo .
def {1} shared var lib-tot      as char                            no-undo .
 
 
/* Definition des zones & de la frame */
def {1} shared var dat-debx     as date format "99/99/9999"         no-undo .
def {1} shared var dat-finx     as date format "99/99/9999"         no-undo .
def {1} shared var magasins     as char format "x(40)"              no-undo .
def {1} shared var maquette     as char format "x(12)"              no-undo .
def {1} shared var periph       as char format "x(12)"              no-undo .
def {1} shared var lib-maq      as char format "x(30)"              no-undo .
 
 
def {1} shared var z-choix      as char                             no-undo .
 
def {1} shared frame fr-saisie .
 
form libelle[ 11 ] format "x(28)"  dat-debx     dat-finx    skip
     libelle[ 12 ] format "x(28)"  magasins                 skip
     libelle[ 13 ] format "x(28)"  maquette     lib-maq     skip
     libelle[ 14 ] format "x(28)"  periph
     with frame fr-saisie
     row 3  centered  overlay  no-label  with title libelle[ 10 ] .