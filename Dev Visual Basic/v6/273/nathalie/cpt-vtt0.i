/*=======================================================================*/
/*                           C P T - V T T 0 . I                         */
/*    Comptabilisation des Ventes ( Tiers ) - Fichier INTFAC -->  Cpta   */
/*=======================================================================*/

def {1} shared var libelle          as char   extent 49       no-undo .

def {1} shared var typ-factur       as char   format "x"      no-undo .
def {1} shared var date-cptmax      like intfac.date-facture  no-undo .
def {1} shared var valida           as char   format "x"      no-undo .

def {1} shared var stat-bloc        as char                   no-undo .

def {1} shared var int-recid        as recid                  no-undo .

def {1} shared var soc-codgen       like cptgen.codsoc        no-undo .

def {1} shared var mont-cpt         as dec                    no-undo .
def {1} shared var mont-dev         as dec                    no-undo .

def {1} shared var tot-mt-seq       as dec                    no-undo .

def {1} shared var zon-io           as char  format "x(45)"  no-undo .

def {1} shared var ii               as int                    no-undo .

def {1} shared var nb-maj           as int    format "99999"  no-undo .
def {1} shared var nb-lus           as int    format "99999"  no-undo .

def {1} shared var lib-jouven       as char   format "x(30)"    no-undo .
def {1} shared var journal-vte      as char   format "xxx"      no-undo .
def {1} shared var typie-fac        as char   format "xxx"      no-undo .
def {1} shared var typie-avo        as char   format "xxx"      no-undo .
def {1} shared var cptgen-delta     like cptgen.nocpte          no-undo .
def {1} shared var cpt-devcpt       as char   format   "xxx"    no-undo .

/* �� */
def {1} shared var tot-mt-cpt       as dec                    no-undo .
def {1} shared var tot-mt-etb       as dec                    no-undo .
def {1} shared var tot-mt-ini       as dec                    no-undo .
def {1} shared var x-devise         as char   format "xxx"    no-undo .
