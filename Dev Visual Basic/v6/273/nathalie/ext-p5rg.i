/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/ext-p5rg.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 17854 29-10-09!Bug!cch            !Correction pble sur envoi code �ch�ance pour ASEC ( modif. EUREA )    !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - P 5 R G  . I                           */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Modes de Reglement PDP V5                       CC 24/04/1999 */
/*==========================================================================*/

                                                        /* Pos.  Long. */

zone = "TIERSRGL"                                       /* 001 - 008 */
       + "C"                                            /* 009 - 001 */
       + string(codsoc-trait[nsoc], "xx")               /* 010 - 002 */
       + fill( " ", 6 )                                 /* 012 - 006 */
       .

code-tiers = trim(auxapr.codaux).
{ cadra-g.i "code-tiers" "6" "0" }

zone = zone
       + string(code-tiers, "x(10)")                    /* 018 - 010 */
       .

case notour :                                           /* 028 - 001 */
    when 1 then zone = zone + "A".
    otherwise   zone = zone + "E".
end case.

zone = zone
       + "G"                                            /* 029 - 001 */
/* DGR 19/12/2001
       + "PT"                                           /* 030 - 008 */
*/
       + "TM"                                           /* 030 - 008 */
       + fill(" ", 6)
       + fill("0", 7)                                   /* 038 - 007 */
       .

if auxapr.datcre <> ?                                   /* 045 - 008 */
   then zone = zone + string(auxapr.datcre, "99999999").
   else zone = zone + fill( " ", 8 ).

zone = zone + "TE"                                     /* 053 - 002 */
       + fill(" ",22 )                                 /* 055 - 022 */
       .

if codsoc-trait[nsoc] = "08" and auxapr.codech = "353" /* 077 - 001 */
   then zone = zone + "1".
   else zone = zone + string(substring(trim(auxapr.codech),3,1), "x").

zone = zone
       + fill(" ",8 )                                   /* 078 - 008 */
       + string(today, "99999999")                      /* 086 - 008 */
       + fill(" ",8 )                                   /* 094 - 008 */
       + string(codsoc-trait[nsoc], "xx")               /* 102 - 002 */
       .

if auxapr.blocage = "1" or auxapr.cpt-ferme = "1"       /* 104 - 001 */
   then zone = zone + "S".
   else zone = zone + " ".

if auxapr.datmaj <> ?                                   /* 105 - 008 */
   then zone = zone + string(auxapr.datmaj, "99999999").
   else zone = zone + fill( " ", 8 ).