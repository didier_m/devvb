/*==========================================================================*/
/*                           E X T - P 5 A L  . I                           */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Articles PDP V5 : article LIE                  DGR 23/04/2004 */
/*==========================================================================*/


                                                        /* Pos.  Long. */

zone = "INFO_ART"                                       /* 001 - 008 */
       + "C"                                            /* 009 - 001 */
       + string(codsoc-trait[nsoc], "xx")               /* 010 - 002 */
       + fill(" ", 6)                                   /* 012 - 006 */
       + string(artapr.articl-lie, "x(20)")             /* 018 - 020 */
       + substr(string(today,"99999999"),5,4)
       + substr(string(today,"99999999"),3,2)
       + substr(string(today,"99999999"),1,2)
       + fill(" ", 9)                                   /* 046 - 009 */
       + string(artapr.articl, "x(15)")                 /* 055 - 015 */
       + substr(string(today,"99999999"),5,4)
       + substr(string(today,"99999999"),3,2)
       + substr(string(today,"99999999"),1,2)
       + substr(string(today,"99999999"),5,4)
       + substr(string(today,"99999999"),3,2)
       + substr(string(today,"99999999"),1,2)
       + fill(" ", 9)                                   /* 086 - 009 */
       + "CONSIGNE"
       .

