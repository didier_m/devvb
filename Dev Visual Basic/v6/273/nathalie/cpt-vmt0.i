/*=======================================================================*/
/*                           C P T - V M T 0 . I                         */
/*=======================================================================*/

def {1} shared buffer  b-entete  for  entete .

def {1} shared var libelle          as char   extent 49       no-undo .

def {1} shared var date-cptmax      like entete.datfac        no-undo .
def {1} shared var date-cptmin      like entete.datfac        no-undo .
def {1} shared var date-cpta        like entete.datfac        no-undo .
def {1} shared var valida           as char   format "x"      no-undo .

def {1} shared var ii               as int                    no-undo .

def {1} shared var nb-lus           as int    format "99999"  no-undo .

def {1} shared var x-typaux    like entete.typaux             no-undo .
def {1} shared var x-codaux    like entete.codaux             no-undo .
def {1} shared var x-datcpt    as char                        no-undo .

def {1} shared var entete-recid as recid                      no-undo .
def {1} shared var lignes-recid as recid                      no-undo .
def {1} shared var auxapr-recid as recid                      no-undo .

def {1} shared var tva-lig  as dec decimals 2                 no-undo .
def {1} shared var ht-lig   as dec decimals 2                 no-undo .

def {1} shared var zone-tva as char format "xxx"              no-undo .

def {1} shared var nom-applic     as char                     no-undo .
