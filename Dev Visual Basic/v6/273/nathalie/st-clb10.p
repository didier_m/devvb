/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/src/st-clb10.p                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 13696 23-11-05!Evo!cch            !Ajout nom spécifique du spool si maquette pour MONARCH                !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !                                                !
!chx-opt.f             !                                     !                                                !
!aide.i                !new                                  !                                                !
!maquette.i            !new                                  !                                                !
!bat-ini.i             !new                                  !                                                !
!st-clb10.i            !new                                  !                                                !
!aide-lib.i            !                                     !                                                !
!fr-cadre.i            !libelle[1]                           !                                                !
!chx-opt.i             !"3" "libel" "15" "14" "16" "with titl!                                                !
!bat-zon.i             !"codsoc-soc" "codsoc-soc" "c" "''" "l!                                                !
!bat-maj.i             !"st-clb11.p"                         !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                          S T - C L B 1 0 . P                             */
/* Statistique Vente Client                                                 */
/* Etat des CA / Depot / Type Vente  / Bon                                  */
/* Programme Lanceur                                                        */
/*==========================================================================*/

{ connect.i      } 
{ chx-opt.f      }
{ aide.i     new }
{ maquette.i new }
{ bat-ini.i  new }
{ st-clb10.i new }

def var i            as int  no-undo .
def var sequence     as char no-undo .
def var sel-sequence as char no-undo .

/*----------------------------*/
/* Chargement du fichier aide */
/*----------------------------*/
aide-fichier = "st-clien.aid" . aide-par = "<st-clb10>" . run aide-lec.p .

do i = 1 to 99 :
  aide-m = "libel" + string( i , "999" ) .
  { aide-lib.i }
  libelle[ i ] = aide-lib .
end .

/*-------------------------------------*/
/* Prise Numero sequence par operateur */
/*-------------------------------------*/
assign  codsoc = ""
        etabli = ""
        typtab = "OPE"
        prefix = "NUME-SPOOL"
        codtab = operat .

run opeseq.p ( "stc" , output  sequence, output sel-sequence) .

periph = sel-sequence .
sav-periph = periph .

/*------------------------------*/
/* Affichage  du cadre standard */
/*------------------------------*/
{ fr-cadre.i libelle[1] }

/*-------------------------------------------------------------------------*/
/* DEBUT DU TRAITEMENT                                                     */
/*-------------------------------------------------------------------------*/
SAISIE-GENERALE :
REPEAT :

    run st-clb1q.p .
    if keyfunction( lastkey) = "end-error" then leave saisie-generale .

    /* Acceptation */
    /*-------------*/
    REPEAT :
        assign chx-def     =  1
               chx-delete  =  99
               chx-milieu  =  64
               chx-color   =  "normal"
               z-choix     =  substring( libelle[ 5 ], 1, 7 )
               libel       =  trim( libelle[ 4 ] ) .

        { chx-opt.i  "3"  "libel"  "15"  "14"  "16"
                     "with title z-choix" "ecr-choix"  "color message" }

        if chx-trait = 1 then leave SAISIE-GENERALE .    /*   Validation    */
        if chx-trait = 2 then leave SAISIE-GENERALE .    /*   Refus         */
        if chx-trait = 3 then next  SAISIE-GENERALE .    /*   Modification  */

    END .

END .   /* SAISIE-GENERALE  */

/*-----------------------------------*/
/* Mise a jour du fichier pour batch */
/*-----------------------------------*/

if chx-trait = 1 and keyfunction( lastkey ) <> "end-error"
then repeat :

    ma-maquet = maquette .
    run maq-lect.p .
    if ma-anom <> ""
    then do :
        bell. bell .
        message libelle[ 9 ] ma-maquet ma-anom . 
        pause .
        leave .
    end .

    batch-tenu = yes .

    { bat-zon.i  "codsoc-soc"  "codsoc-soc"  "c"  "''"  "libelle[17]" }
    { bat-zon.i  "lib-prog"    "lib-prog"    "c"  "''"  "libelle[18]" }
    { bat-zon.i  "mois-deb"    "mois-deb"    "d"  "''"  "libelle[12]" }
    { bat-zon.i  "mois-fin"    "mois-fin"    "d"  "''"  "libelle[12]" }
    { bat-zon.i  "magasins"    "magasins"    "c"  "''"  "libelle[13]" }
    { bat-zon.i  "maquette"    "maquette"    "c"  "''"  "libelle[14]" }
    { bat-zon.i  "periph"      "periph"      "c"  "''"  "libelle[15]" }
    { bat-zon.i  "chemin"      "chemin"      "c"  "''"  "libelle[16]" }
    { bat-zon.i  "ma-esc"      "ma-esc"      "c"  "''"  "libelle[19]" }

    { bat-maj.i  "st-clb11.p" }

    leave .

end .

/*-------------------*/
/* Effacement ecrans */
/*-------------------*/

hide frame fr-saisie no-pause .
hide frame fr-titre  no-pause .
hide frame fr-cadre  no-pause .