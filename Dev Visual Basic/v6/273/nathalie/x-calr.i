/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/x-calr.i                                                                     !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 11260 08-11-04!Evo!cch            !Adaptation interfaces pour passage en V52                             !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
def {1} shared var cde-rang         as int .
def {1} shared var cde-qte          as dec .
def {1} shared var artapr-recid     as recid .
def {1} shared var lignes-recid     as recid .
def {1} shared var entete-recid     as recid .
def {1} shared var anc-entete-recid as recid .