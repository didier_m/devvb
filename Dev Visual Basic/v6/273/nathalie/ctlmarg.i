/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/ctlmarg.i                                                                      !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End                                                                                                          
**************************************************************************************************************/

def {1} shared var wsoc   as char   format "xx"                  no-undo.
def {1} shared var l-soc   as char   format "xx"                  no-undo.
def {1} shared var wfam   as char   format "xxx"                 no-undo.
def {1} shared var wan-n0 as char   format "xxxx"                no-undo.
def {1} shared var sedifil1 as char format "x(15)"               no-undo.
def {1} shared var sedifil2 as char format "x(15)"               no-undo.

def stream file1.
DEFINE STREAM maquette.
def stream file2. 
