/*==========================================================================*/
/*                          S T - R E P 1 2 . I                             */
/* Statistique Vente Representant                                           */
/* Etat des Qtes & CA    / Famille / Zone                                   */
/*==========================================================================*/
/*           Zones utilise pour le module edition des ruptures              */
/*                Rupture-a               : Niveau de rupture en cours      */
/*                Rupture-nb              : Nombre de rupture existante     */
/*                Rupture-etat = "t"      : Edition du titre de la rupture  */
/*                Rupture-etat = "r"      : Edition de la rupture           */
/*--------------------------------------------------------------------------*/
 
 
/*    Entete REPRESENTANT            */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 1 :
/* message suspendu ---------
    if substring( zone , 1 , 3 ) <> " " then do :
        hide message . pause 0 .
        message "  Traitement du Representant ...: "
                "  " substring( zone , 1 , 3 ) .
    end .
---------------------------------------------------------*/
    assign    lib-tit   = libelle[31]
              no-rep    = substring( zone , 1, 3 )
              lib-rep   = " "
              nb-fam    = 0
              ent-rep   = yes .
 
    find tabges where tabges.codsoc = "  "     and
                      tabges.etabli = "   "    and
                      tabges.typtab = "CLI"    and
                      tabges.prefix = "REPRES" and
                      tabges.codtab = no-rep
                      no-lock  no-error .
    if not available tabges
    then lib-rep = "INEXISTANT" .
    else lib-rep = tabges.libel1[1] .
 
    leave .
 
END .
 
 
/*    Entete Famille    */
/*======================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 2 :
 
    assign no-fam   = substring( zone, 4, 3 )
           lib-fam  = " "
           lib-tit1 = libelle[32]
           nb-zone  = 0
           ent-fam  = yes .
 
    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "ART"
                and   tabges.prefix = "FAMART"
                and   tabges.codtab = no-fam
                no-lock  no-error .
    if available tabges
    then lib-fam  = tabges.libel1[mem-langue] .
    else lib-fam  = "INEXISTANTE" .
 
    leave .
END .
 
 
/* Entete ZONE       */
/*===================*/
DO WHILE (RUPTURE-ETAT = "T" AND RUPTURE-A = 3 ) :
 
   assign  no-zone     = substring( zone , 7 , 3 )
           lib-zone    = " "
           nb-recid    =  0 .
 
    find tabges where tabges.codsoc = "  "     and
                      tabges.etabli = "   "    and
                      tabges.typtab = "CLI"    and
                      tabges.prefix = "SECGEO" and
                      tabges.codtab = no-zone
                      no-lock  no-error .
    if available tabges then lib-zone = tabges.libel1[mem-langue] .
 
    leave .
 
END .
 
 
/*=========================================================================*/
 
/*    Total Recid      */
/*=====================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 4 :
 
    /* CUMUL des Valeur   suivant date      */
    /*  1  =  Qte Mensuelle                 */
    /*  2  =  CA  Mensuelle                 */
    /*  3  =  Qte Annuelle                  */
    /*  4  =  CA  Annuelle                  */
    /*--------------------------------------*/
    am-sta = int( stacli.annee ) * 100 .
 
    do i = 1 to 12 :
        if am-sta + i = am-fin
        then assign tot[ 1 ] = tot[ 1 ]  + stacli.qte[ i ]
                    tot[ 2 ] = tot[ 2 ]  + stacli.fac-ht[ i ]
                    nb-recid  = nb-recid + 1 .
 
        if am-sta + i >= am-debx and am-sta + i <= am-fin
        then assign tot[ 3 ] = tot[ 3 ]  + stacli.qte[ i ]
                    tot[ 4 ] = tot[ 4 ]  + stacli.fac-ht[ i ]
                    nb-recid  = nb-recid + 1 .
    end .
    leave .
 
END .
 
/*    Total Zone                    */
/*==================================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 3 :
   do while nb-recid <> 0 :
       assign ind-tot = 00
              nb-zone = nb-zone + 1 .
                                            /* imp entete que si ligne */
       if ent-rep  then run value( ma-entete ) .
                                            /* idem ci-dessus          */
       if ent-fam
       then do :
           { maq-maj.i "02" "01" "lib-tit1"  }
           { maq-maj.i "02" "02" "no-fam"    }
           { maq-maj.i "02" "03" "lib-fam"   }
 
           { maq-edi.i "02" }
       end .
       assign ent-rep = no
              ent-fam = no .
       run st-rep1t.p .
       leave .
   end .
   leave .
END .
 
 
/*    Total  Famille            */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 2 :
 
   do while nb-zone <> 0 :
       assign lib-tot = " Total ....   "  + no-fam
              ind-tot = 10
              nb-fam = nb-fam + 1.
 
       run st-rep1t.p .
       leave .
   end .
 
   leave .
END .
 
 
/*    Total REPRESENTANT  */
/*========================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 1 :
 
   do while nb-fam <> 0 :
       assign lib-tot = " Total ....   "  + no-rep
                ind-tot  = 20
                nb-rep  = nb-rep + 1 .
 
       run st-rep1t.p .
       leave .
   end .
 
   leave .
END .