/*==========================================================================*/
/*                          S T - V M A 7 E . P                             */
/*     Edition Statistique des Ventes des Magasins par Activite             */
/*     Gestion Entete de Famille                                            */
/*==========================================================================*/

{ connect.i }
{ maquette.i }

{ st-vma70.i }

/* Definition des variables */
def var date-11     as char         no-undo .
def var date-12     as char         no-undo .
def var date-21     as char         no-undo .
def var date-22     as char         no-undo .
def var date-31     as char         no-undo .
def var date-32     as char         no-undo .

date-11 = string(date-deb    , "99/99/9999" )  .
date-12 = string(date-fin    , "99/99/9999" )  .
date-21 = string(date-debex  , "99/99/9999" )  .
date-22 = string(date-fin    , "99/99/9999" )  .
date-31 = string(date-dex1   , "99/99/9999" )  .
date-32 = string(date-fin1   , "99/99/9999" )  .

{ maq-maj.i 1  05  "libelle[31]" }
{ maq-maj.i 1  06   lib-act      }
{ maq-maj.i 1  11   date-11      }
{ maq-maj.i 1  12   date-12      }
{ maq-maj.i 1  21   date-21      }
{ maq-maj.i 1  22   date-22      }
{ maq-maj.i 1  31   date-31      }
{ maq-maj.i 1  32   date-32      }

{ maq-maj.i 1  1  lib-periode }

{ maq-edi.i 1 }

return .