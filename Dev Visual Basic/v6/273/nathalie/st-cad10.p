/*==========================================================================*/
/*                          S T - C A D 1 0 . P                             */
/*          Edition Cadencier / Provenance et Magasin f( Typologie )        */
/*===========================================================================*/
 
{ connect.i }
{ chx-opt.f }
{ natabsoc.i  }
 
{ maquette.i new }
{ aide.i     new }
{ bat-ini.i  new }
 
{ st-cad10.i  new }
 
assign batch-tenu = yes. /* Gestion en batch */
 
/* Definition des Variables */
/*--------------------------*/ 
def var sequence as char format "x(5)" .
def var i as int no-undo .
 
/* Chargement du fichier aide */
/*----------------------------*/
aide-fichier = "st-arti.aid" . aide-par = "<st-cad10>" . run aide-lec.p .
 
do i = 1 to 99 :
  aide-m = "libel" + string( i , "999" ) .
  { aide-lib.i }
  libelle[ i ] = aide-lib .
end .
 
/*-------------------------------------*/
/* Initialisation des multi-selections */
/*-------------------------------------*/
{ sel.i "new" }
assign  sel-applic   = "gescom"
        sel-filtre   = "*st-cad10". /* st-cad10.chx */
 
/*-------------------------------------*/
/* Prise Numero sequence par operateur */
/*-------------------------------------*/
assign  codsoc = ""
        etabli = ""
        typtab = "OPE"
        prefix = "NUME-SPOOL"
        codtab = operat .
 
run opeseq.p ( "cad" , output  sequence, output sel-sequence) .
 
periph = sel-sequence .
sav-periph = periph .
 
{ fr-cadre.i libelle[1] }
/*  { sel.f "new" }   */
 
SAISIE-GENERALE :
REPEAT :
 
    run st-cad1q.p .
    if keyfunction( lastkey) = "end-error" then leave saisie-generale .
 
                                  /*------------------*/
                                 /* multi-selections */
                                /*------------------*/
/*
    run sel.p .
    if keyfunction( lastkey) = "end-error" then leave saisie-generale .
*/
                                  /*-------------*/
                                 /* Acceptation */
                                /*-------------*/
    REPEAT :
        assign chx-def     =  1
               chx-delete  =  99
               chx-milieu  =  64
               chx-color   =  "normal"
               z-choix     =  substring( libelle[ 5 ], 1, 7 )
               libel       =  trim( libelle[ 4 ] ) .
 
        { chx-opt.i  "3"  "libel"  "15"  "14"  "16"  "with title z-choix"
                     "ecr-choix"  "color message" }
 
        if chx-trait = 1 then leave SAISIE-GENERALE .    /*   Validation    */
        if chx-trait = 2 then leave SAISIE-GENERALE .    /*   Refus         */
        if chx-trait = 3 then next  SAISIE-GENERALE .    /*   Modification  */
 
    END .
 
END .   /* SAISIE-GENERALE  */
 
                  /*-----------------------------------*/
                 /* Mise a jour du fichier pour batch */
                /*-----------------------------------*/
 
if chx-trait = 1 and keyfunction( lastkey ) <> "end-error" 
then do :
 
   /* lecture maquette pour chargement escape */
    ma-maquet = maquette .
    run maq-lect.p .
    if ma-anom <> ""
    then do :
        message libelle[ 9 ] ma-anom . bell. bell . pause .
        hide all .
        return .
    end .
 
        { bat-zon.i  "codsoc"        "codsoc-soc"    "c" "''" "libelle[02]"}
        { bat-zon.i  "lib-prog"      "lib-prog"      "c" "''" "libelle[01]"}
        { bat-zon.i  "sel-applic"    "sel-applic"    "c" "''" "''"}
        { bat-zon.i  "sel-filtre"    "sel-filtre"    "c" "''" "''"}
        { bat-zon.i  "sel-sequence"  "sel-sequence"  "c" "''" "''"}
        { bat-zon.i  "periph"        "periph"        "c" "''" "''"}
        { bat-zon.i  "maquette"      "maquette"      "c" "''" "libelle[16]"}
        { bat-zon.i  "ma-esc"        "ma-esc"        "c" "''" "libelle[94]"}    
        { bat-zon.i  "mois-debcp"    "mois-debcp"     "c" "''" "libelle[11]"}
        { bat-zon.i  "mois-fincp"    "mois-fincp"     "c" "''" "libelle[12]"}
        { bat-zon.i  "provenance"    "provenance"     "c" "''" "libelle[13]"}
        { bat-zon.i  "magasins"      "magasins"       "c" "''" "libelle[14]"}
        { bat-zon.i  "familles"      "familles"       "c" "''" "libelle[15]"}
 
        { bat-zon.i  "anee-deb"      "anee-deb"       "c" "''" "''"}
        { bat-zon.i  "anee-fin"      "anee-fin"       "c" "''" "''"}
        { bat-zon.i  "mois-deb"      "mois-deb"       "c" "''" "''"}
        { bat-zon.i  "mois-fin"      "mois-fin"       "c" "''" "''"}
 
        { bat-maj.i  "st-cad11.p" }
end .
 
                  /*------------------------------------------------*/
                 /* Effacement ecrans et suppression des multi-sel */
                /*------------------------------------------------*/
 
hide frame fr-saisie no-pause .
 
/*  { sel-hide.i }  */
 
hide frame fr-titre no-pause .
hide frame fr-cadre no-pause .
 
run sel-del.p ( sel-applic, sel-filtre, sel-sequence, yes ) .  
 
 
 