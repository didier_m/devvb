/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/g-trf13.i                                                                      !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 259795 2019-01-31!Bug!p-o teyssier   !Correction suite erreur compilation                                   !
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End                                                                                                          
**************************************************************************************************************/
/*-----------------------------------------------------------*/
/*               g - t r f 1 3 . i                           */
/*-----------------------------------------------------------*/
lect = "{2}".

IF lect <> "" AND lect <> "999" THEN DO:
    FIND {tarcli.i} WHERE {tarcli.i}.motcle     = motcle-trt
                      AND {tarcli.i}.codsoc     = codsoc-soc
                      AND {tarcli.i}.typaux     = " "
                      AND {tarcli.i}.codaux     = categorie-tarif
                      AND {tarcli.i}.articl     = " "
                      AND {tarcli.i}.date-debut = date-debut
                      AND {tarcli.i}.chrono     = {2}
                    USE-INDEX chrono-art NO-LOCK NO-ERROR.
END.
ELSE DO:
    FIND {1} {tarcli.i} WHERE {tarcli.i}.motcle     = motcle-trt
                          AND {tarcli.i}.codsoc     = codsoc-soc
                          AND {tarcli.i}.typaux     = " "
                          AND {tarcli.i}.codaux     = categorie-tarif
                          AND {tarcli.i}.articl     = " "
                          AND {tarcli.i}.date-debut = date-debut
                          USE-INDEX chrono-art NO-LOCK NO-ERROR.
END.
