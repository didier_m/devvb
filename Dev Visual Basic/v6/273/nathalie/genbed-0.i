/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/genbed-0.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V61 41977 21-10-13!Evo!cch            !Suite Fiche V61 41966                                                 !
!V61 41966 21-10-13!Evo!cch            !Ajout �x�cution genb-cde.p + gesdec-2.p - EUREA                       !
!V61 26855 14-10-11!Evo!cch            !Ajout ctrl existence fiche article frn pour CCI provenance EURENA ou C!
!V61 20146 11-08-10!Evo!cch            !Stockage no Camion dans BONENS et non plus dans LIGSPE                !
!V52 17870 20-11-09!Evo!cch            !Suite fiche V52 17869                                                 !
!V52 17869 20-11-09!Evo!cch            !Suite fiche V52 17840 - Nouvelles demandes                            !
!V52 17649 08-06-09!Evo!cch            !Ajout gestion ENTETE.TYPBON comme crit�re de rupture sur REI          !
!V52 17644 05-06-09!Evo!cch            !Ajout gestion des commandes aliments vrac et sac                      !
!V52 17500 16-03-09!Evo!cch            !Suite et fin fiche 17494                                              !
!V52 17404 23-01-09!Evo!cch            !Ajout g�n�ration CDC ED sur magasin interm�diaire si PCF ED           !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/* GENBED-0.I - GENERATION DES BONS EN S.D. + E.D. ( Projet EUREA DISTRI )    */
/*----------------------------------------------------------------------------*/
/* Definitions Communes                                 09/2006 - EUREA - 273 */
/******************************************************************************/

def var i-param         as char           no-undo .
def var o-param         as char           no-undo .
def var codsoc-bon      as char           no-undo .              
def var motcle-bon      as char           no-undo .              
def var typcom-bon      as char           no-undo .              
def var no-bon          as char           no-undo .              
def var nomimp          as char           no-undo .              
def var operat-maj      as char           no-undo .              
def var repert-rej      as char           no-undo .              
def var ext-rej         as char           no-undo .
def var l-soc-ini       as char           no-undo .
def var l-motcle-ini    as char           no-undo .
def var l-typcom-ini    as char           no-undo .
def var l-numbon-ini    as char           no-undo .
def var l-typcom-acp-sd as char           no-undo .
def var l-typcom-ach-sd as char           no-undo .
def var l-typcom-vte-sd as char           no-undo .
def var l-typcom-acp-ed as char           no-undo .
def var l-typcom-ach-ed as char           no-undo .
def var l-typcom-vte-ed as char           no-undo .
def var l-typcom-trt    as char           no-undo .
def var codsoc-lect     as char           no-undo .
def var codsoc-trt      as char           no-undo .
def var motcle-trt      as char           no-undo .
def var typcom-trt      as char           no-undo .
def var numbon-trt      as char           no-undo .
def var typaux-trt      as char           no-undo .
def var codaux-trt      as char           no-undo .
def var mag-log         as char           no-undo .
def var mag-fab         as char           no-undo .
def var mag-trt         as char           no-undo .
def var l-mag-trt       as char           no-undo .
def var typbon-trt      as char           no-undo .
def var rech-typcom     as char           no-undo .
def var code-art        as char           no-undo .
def var code-mag        as char           no-undo .
def var code-modliv     as char           no-undo .
def var ref-bon         as char extent 52 no-undo . 
def var mess-lib        as char           no-undo . 
def var mess-err        as char           no-undo .
def var zone            as char           no-undo .
def var ind             as int            no-undo .
def var ind-2           as int            no-undo .
def var chrono-trt      as int            no-undo .
def var ind-trt         as int            no-undo .
def var tour-bon        as int            no-undo .
def var tour-soc        as int            no-undo .  
def var tour-motcle     as int            no-undo .  
def var tour-typcom     as int            no-undo .  
def var nb-bon-lu       as int            no-undo .  
def var nb-bon-trt      as int            no-undo .
def var nb-decim        as int            no-undo .
def var aaa             as int            no-undo .
def var top-0-n         as int            no-undo .
def var remlig          as dec            no-undo .
def var val-dec         as dec            no-undo .
def var ok-trt          as log            no-undo .  
def var ok-edit         as log            no-undo .  
def var ok-pcf          as log            no-undo .  
def var ok-ach          as log            no-undo .
def var recid-entete    as recid          no-undo .           
def var recid-ligne     as recid          no-undo .           

def {1} shared var libelle         as char extent 110             no-undo .
def {1} shared var top-trt-ed      as char extent 4               no-undo .           
def {1} shared var dev-gescom      as char                        no-undo .
def {1} shared var articl-soc-ed   as char                        no-undo .               
def {1} shared var auxspe-soc-ed   as char                        no-undo .
def {1} shared var codsoc-ed       as char                        no-undo .
def {1} shared var codsoc-sd       as char                        no-undo .
def {1} shared var magasi-soc-sd   as char                        no-undo .
def {1} shared var articl-soc-sd   as char                        no-undo .               
def {1} shared var auxspe-soc-sd   as char                        no-undo .               
def {1} shared var typaux-ed       as char                        no-undo .
def {1} shared var codaux-ed       as char                        no-undo .
def {1} shared var typaux-dis      as char                        no-undo .
def {1} shared var codaux-dis      as char                        no-undo .
def {1} shared var typaux-sod      as char                        no-undo .
def {1} shared var codaux-sod      as char                        no-undo .
def {1} shared var typaux-eur      as char                        no-undo .
def {1} shared var codaux-eur      as char                        no-undo .
def {1} shared var typaux-ciz      as char                        no-undo .
def {1} shared var codaux-ciz      as char                        no-undo .
def {1} shared var l-prov-excl-ach as char                        no-undo .
def {1} shared var l-prov-excl-vte as char                        no-undo .
def {1} shared var l-typcom-sd     as char                        no-undo .
def {1} shared var l-typcom-ed     as char                        no-undo .
def {1} shared var l-cdc-sd        as char                        no-undo .
def {1} shared var l-cdalim-sd     as char                        no-undo .
def {1} shared var typcom-cci      as char                        no-undo .
def {1} shared var typcom-rei      as char                        no-undo .
def {1} shared var typcom-cdc-sd   as char                        no-undo .
def {1} shared var typcom-cdf-sd   as char                        no-undo .
def {1} shared var typcom-cds-sd   as char                        no-undo .
def {1} shared var typcom-cdv-sd   as char                        no-undo .
def {1} shared var typcom-pcf-sd   as char                        no-undo .
def {1} shared var typcom-tcs-sd   as char                        no-undo .
def {1} shared var typcom-cdc-ed   as char                        no-undo .
def {1} shared var typcom-cdr-ed   as char                        no-undo .
def {1} shared var typcom-crc-ed   as char                        no-undo .
def {1} shared var typcom-cdf-ed   as char                        no-undo .
def {1} shared var typcom-cfe-ed   as char                        no-undo .
def {1} shared var typcom-cfs-ed   as char                        no-undo .
def {1} shared var typcom-pcf-ed   as char                        no-undo .
def {1} shared var typtva-exo-sd   as char extent 10              no-undo .
def {1} shared var typtva-exo-ed   as char                        no-undo .
def {1} shared var typbon-std      as char                        no-undo .
def {1} shared var typbon-ter      as char                        no-undo .
def {1} shared var typbon-lit      as char                        no-undo .
def {1} shared var typbon-ret      as char                        no-undo .
def {1} shared var typbon-ms       as char                        no-undo .
def {1} shared var motcle-acp      as char                        no-undo .
def {1} shared var motcle-ach      as char                        no-undo .
def {1} shared var motcle-vte      as char                        no-undo .
def {1} shared var motcle-lien     as char                        no-undo .
def {1} shared var motcle-next     as char                        no-undo .
def {1} shared var mag-elf         as char                        no-undo .
def {1} shared var mag-elf-dir     as char                        no-undo .
def {1} shared var mag-elf-tmp     as char                        no-undo .
def {1} shared var mag-mul-coop    as char                        no-undo .
def {1} shared var mag-mul-asec    as char                        no-undo .
def {1} shared var mag-cofna       as char                        no-undo .
def {1} shared var mag-ali-sac     as char                        no-undo .
def {1} shared var mag-ali-vrac    as char extent 2               no-undo .
def {1} shared var fic-rej         as char                        no-undo .
def {1} shared var prov-blk        as char                        no-undo .
def {1} shared var prov-civ        as char                        no-undo .
def {1} shared var prov-cos        as char                        no-undo .
def {1} shared var prov-cov        as char                        no-undo .
def {1} shared var prov-dis        as char                        no-undo .
def {1} shared var prov-edd        as char                        no-undo .
def {1} shared var prov-edl        as char                        no-undo .
def {1} shared var prov-edx        as char                        no-undo .
def {1} shared var prov-eus        as char                        no-undo .
def {1} shared var prov-euv        as char                        no-undo .
def {1} shared var prov-mpr        as char                        no-undo .
def {1} shared var prov-mul        as char                        no-undo .
def {1} shared var prov-rem        as char                        no-undo .
def {1} shared var prov-sod        as char                        no-undo .
def {1} shared var prov-sup        as char                        no-undo .
def {1} shared var motcle-caf      as char                        no-undo .
def {1} shared var codpri-caf      as char                        no-undo .
def {1} shared var codpri-ach      as char                        no-undo .
def {1} shared var codpri-attente  as char                        no-undo .
def {1} shared var codent-vac      as char                        no-undo .
def {1} shared var tenusto-n-stock as char                        no-undo .
def {1} shared var tenusto-stock   as char                        no-undo .
def {1} shared var tenusto-valeur  as char                        no-undo .
def {1} shared var tenu-sto-sd     as char extent 50              no-undo .
def {1} shared var tombee-sto-sd   as char extent 50              no-undo .
def {1} shared var entree-sto-sd   as char extent 10              no-undo .
def {1} shared var rech-typcom-sd  as char extent 50              no-undo .
def {1} shared var tenu-sto-ed     as char extent 10              no-undo .
def {1} shared var tombee-sto-ed   as char extent 10              no-undo .
def {1} shared var entree-sto-ed   as char extent 10              no-undo .
def {1} shared var rech-typcom-ed  as char extent 10              no-undo .
def {1} shared var pas-lignes-sd   as int  extent 50              no-undo .
def {1} shared var pas-lignes-ed   as int  extent 10              no-undo .
def {1} shared var niveau-trt      as int                         no-undo .
def {1} shared var top-1-n         as int                         no-undo .
def {1} shared var top-3-n         as int                         no-undo .
def {1} shared var nb-bon-cre      as int                         no-undo .
def {1} shared var nb-bon-cre-2    as int                         no-undo .
def {1} shared var nb-err          as int                         no-undo .
def {1} shared var mouchard        as log            format "O/N" no-undo .
def {1} shared var trt-test        as log                         no-undo .
def {1} shared var maj-bon         as log                         no-undo .
def {1} shared var ok-trt-bon      as log                         no-undo .
def {1} shared var saisie-web      as log                         no-undo .
def {1} shared var date-arrete-ed  as date                        no-undo .
def {1} shared var date-arrete-sd  as date extent 10              no-undo .
def {1} shared var reservation-ed  as CHARACTER                   no-undo .

def buffer b-ini-entete for entete .
def buffer ini-lignes   for lignes .
def buffer b-ini-lignes for lignes .
def buffer ini-proglib  for proglib .

def buffer sd-lignes    for lignes .
def buffer sd-ligspe    for ligspe .
def buffer sd-proglib   for proglib .

def buffer ed-entete    for entete .
def buffer ed-lignes    for lignes .
def buffer ed-ligspe    for ligspe .
def buffer ed-proglib   for proglib .

def buffer xd-entete    for entete .
def buffer xd-lignes    for lignes .
def buffer xd-ligspe    for ligspe .
def buffer xd-proglib   for proglib .

def buffer eda-ligspe   for ligspe .
def buffer edv-ligspe   for ligspe .

def {1} shared buffer ini-entete for entete .
def {1} shared buffer sd-entete  for entete .

def {1} shared stream s-fic-rej .

def temp-table lignes-trav no-undo
    field provenance as char
    field typbon     as char
    field tenusto    as char
    field magasin    as char
    field chrono     as int
    field recid-enr  as recid
    index i-lignes-1 is primary unique provenance typbon chrono .




