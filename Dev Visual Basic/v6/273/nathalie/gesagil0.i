/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/gesagil0.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 26675 05-10-11!Evo!cch            !Ajout Statut fiche CFI � 5 (Envoy�e magasin) lors d'une modification f!
!V61 24380 11-05-11!Evo!cch            !Evolutions S73 suite nvelle version AGIL - Mail CCO du 02/05/2011     !
!V61 20856 01-10-10!Bug!cch            !Correction pble erreur no 116 + mauvais nom fichier rejet si trt rejet!
!V61 20771 27-09-10!Evo!cch            !Suite affectation mode de r�glement � 'ATT'                           !
!V52 17868 20-11-09!Evo!cch            !Remise maj STARTI LISADIS avec prm pour anc./nvelle version           !
!V52 17841 23-10-09!Bug!cch            !Suite fiche V52 17838                                                 !
!V52 17838 22-10-09!Evo!cch            !Ajout Gestion Modificartion Fiches Cartes de Fid�lit�                 !
!V52 17817 15-10-09!Evo!cch            !Ajout Gestion Cr�ation Nouvelles Cartes de Fid�lit�                   !
!V52 17814 14-10-09!Evo!cch            !Ajout MAJ table STARTI pour envoi stocks et qtes vendues � LISADIS    !
!V52 17773 23-09-09!Bug!cch            !Correction pble fichier blocage non supprim� si arr�t traitement      !
!V52 17757 18-09-09!Evo!cch            !Suite Fiche V52 17744                                                 !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/* GESAGIL0.I - GESTION DES ECHANGES AGIL - NATHALIE APPROS                   */
/*----------------------------------------------------------------------------*/
/* Definitions Communes                                 04/2009 - EUREA - 273 */
/******************************************************************************/

def var type-charg    as char          no-undo .
def var i-param       as char          no-undo .
def var o-param       as char          no-undo .
def var zone          as char          no-undo .
def var repert-arriv  as char          no-undo .
def var repert-traite as char          no-undo .
def var repert-rejet  as char          no-undo .
def var fic-rac       as char          no-undo .
def var fic-ren       as char          no-undo .
def var fic-blo       as char extent 2 no-undo .
def var ext-trt       as char          no-undo .
def var ext-rej       as char          no-undo .
def var date-j        as char          no-undo .
def var mess-err      as char          no-undo .
def var typcom-trt    as char          no-undo .
def var mag-trt       as char          no-undo .
def var caisse-trt    as char          no-undo .
def var ticket-trt    as char          no-undo .
def var date-trt      as char extent 4 no-undo .
def var heure-trt     as char          no-undo .
def var ok-trt        as log           no-undo .
def var ind           as int           no-undo .
def var hh-trt        as int           no-undo .
def var mm-trt        as int           no-undo .
def var ss-trt        as int           no-undo .
def var nb-sec-att    as int           no-undo .
def var nb-enr-lu     as int           no-undo .
def var val-int       as int           no-undo .

def {1} shared var libelle              as char                extent 140 no-undo .
def {1} shared var fic-trt              as char                           no-undo .
def {1} shared var fic-rej              as char format "x(50)"            no-undo .
def {1} shared var separat-ent          as char                           no-undo .
def {1} shared var separat-pied         as char                           no-undo .
def {1} shared var separat-chp          as int                            no-undo .
def {1} shared var separat-dec          as char                extent 2   no-undo .
def {1} shared var separat-zone         as char                extent 2   no-undo .
def {1} shared var typlig-vte           as char                           no-undo .
def {1} shared var typlig-rti           as char                           no-undo .
def {1} shared var section-vte          as char                           no-undo .
def {1} shared var section-annulvte     as char                           no-undo .
def {1} shared var section-treso        as char                           no-undo .
def {1} shared var section-annultreso   as char                           no-undo .
def {1} shared var section-paidiff      as char                           no-undo .
def {1} shared var section-annulpaidiff as char                           no-undo .
def {1} shared var section-creationcfi  as char                           no-undo .
def {1} shared var section-modifcfi     as char                           no-undo .
def {1} shared var section-pointscfi    as char                           no-undo .
def {1} shared var section-trt          as char                           no-undo .
def {1} shared var l-section            as char                           no-undo .
def {1} shared var l-sect-exclus        as char                           no-undo .
def {1} shared var l-sect-code          as char                           no-undo .
def {1} shared var motcle-vte           as char                           no-undo .
def {1} shared var motcle-agi           as char                           no-undo .
def {1} shared var motcle-tax           as char                           no-undo .
def {1} shared var motcle-cfi           as char                           no-undo .
def {1} shared var motcle-lis           as char                           no-undo .
def {1} shared var typcom-vte           as char                           no-undo .
def {1} shared var typcom-vab           as char                           no-undo .
def {1} shared var typcom-van           as char                           no-undo .
def {1} shared var nivcom-vte           as char                           no-undo .
def {1} shared var nivcom-vab           as char                           no-undo .
def {1} shared var typbon-vte           as char                           no-undo .
def {1} shared var typaux-def           as char                           no-undo .
def {1} shared var codpri-cli           as char                           no-undo .
def {1} shared var codreg-att           as char                           no-undo .
def {1} shared var codlig-normale       as char                           no-undo .
def {1} shared var codret-gav           as char                           no-undo .
def {1} shared var codmvt-sto           as char                           no-undo .
def {1} shared var codrem-rfa           as char                           no-undo .
def {1} shared var codoperat-deb        as char                           no-undo .
def {1} shared var codreg-mix           as char                           no-undo .
def {1} shared var top-false            as char                           no-undo .
def {1} shared var top-true             as char                           no-undo .
def {1} shared var val-enf-nc           as char                           no-undo .
def {1} shared var l-codpri             as char                extent 2   no-undo .
def {1} shared var l-typrem             as char                extent 2   no-undo .
def {1} shared var l-top-trt            as char                           no-undo .
def {1} shared var l-nature             as char                extent 2   no-undo .
def {1} shared var l-soc                as char                           no-undo .
def {1} shared var l-top-lisadis        as char                           no-undo .
def {1} shared var l-profession         as char                           no-undo .
def {1} shared var l-tps-deplac         as char                           no-undo .
def {1} shared var l-top-market         as char                           no-undo .
def {1} shared var l-typclt             as char                extent 2   no-undo .
def {1} shared var l-typpts             as char                           no-undo .
def {1} shared var l-civilite           as char                           no-undo .
def {1} shared var dev-gescom           as char                           no-undo .
def {1} shared var top-envoi-cfi        as char                           no-undo .
def {1} shared var top-envoye-cfi       as char                           no-undo .
def {1} shared var top-attente-cfi      as char                           no-undo .
def {1} shared var orig-cre-cfi         as char                           no-undo .
def {1} shared var l-soc-cfi            as char                extent 2   no-undo .
def {1} shared var l-debut-no-cfi       as char                           no-undo .
def {1} shared var pts-bvenue-cfi       as int                 extent 2   no-undo .
def {1} shared var ver-extvtlisadis     as int                            no-undo .
def {1} shared var nb-bon-cre           as int                            no-undo .
def {1} shared var nb-treso-cre         as int                            no-undo .
def {1} shared var nb-cfi-cre           as int                            no-undo .
def {1} shared var nb-cfi-maj           as int                            no-undo .
def {1} shared var nb-cfi-pts           as int                            no-undo .
def {1} shared var nb-err               as int                            no-undo .
def {1} shared var mouchard             as log  format "O/N"              no-undo .
def {1} shared var trt-test             as log                            no-undo .
def {1} shared var trt-rejet            as log                            no-undo .
def {1} shared var ok-mvt               as log                            no-undo .
def {1} shared var ok-typlig-rti        as log                            no-undo .
def {1} shared var ok-mess              as log                            no-undo .

def {1} shared stream s-fic-rej .

def {1} shared temp-table lignes-trav no-undo
    field no-enreg as int
    field enreg    as char .




