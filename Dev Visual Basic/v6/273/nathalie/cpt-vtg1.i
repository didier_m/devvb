/*==========================================================================*/
/*                           C P T - V T G 1 . I                            */
/*                 Comptabilisation des Ventes ( C. G. )                    */
/*==========================================================================*/

/*-----------------------------------------------------------------------*/
/*         Zones utilisees pour le module edition des ruptures           */
/*                                                                       */
/*                   Rupture-a          : Niveau de rupture en cours     */
/*                   rupture-etat = "t" : Edition du titre de la rupture */
/*                   Rupture-etat = "r" : Edition de la rupture          */
/*-----------------------------------------------------------------------*/

run cpt-vtg2.p( rupture-etat , rupture-a ) .
