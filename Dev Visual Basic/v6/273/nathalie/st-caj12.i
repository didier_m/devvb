/*==========================================================================*/
/*                          S T - C A J 1 2 . I                             */
/* Statistique Vente Client                                                 */
/* Etat des CA Journalier par Date de Bon / Magasin Responsable             */
/*==========================================================================*/
/*           Zones utilise pour le module edition des ruptures              */
/*                Rupture-a               : Niveau de rupture en cours      */
/*                Rupture-nb              : Nombre de rupture existante     */
/*                Rupture-etat = "t"      : Edition du titre de la rupture  */
/*                Rupture-etat = "r"      : Edition de la rupture           */
/*--------------------------------------------------------------------------*/
 
 
/*    Entete DATE BON                */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 1 :
 
    assign nb-dat = 0
           z-datbon = substring( zone, 40, 10)
           moisan   = substring(z-datbon, 4, 2) + "-" +
                      substring(z-datbon, 9, 2) .
 
    leave .
 
END .
 
 
/* Entete MAGASIN    */
/*===================*/
DO WHILE (RUPTURE-ETAT = "T" AND RUPTURE-A = 2 ) :
 
    hide message . pause 0 .
    message "  Traitement du magasin ...: "
            "  " moisan
            "  " substring( zone , 11, 3 ) .
 
    assign no-mag      = substring( zone , 11, 3 )
           lib-mag     = " "
           nb-bon      = 0
           .
 
    find magasi where magasi.codsoc = magasi-soc
                  and magasi.codtab = no-mag
                  no-lock  no-error .
    if available magasi then lib-mag  = magasi.libel1[mem-langue] .
                        else lib-mag  = "INEXISTANT" .
    leave .
 
END .
 
 
/*=========================================================================*/
 
/*    Total Recid               */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 3 :
 
    /* CUMUL des MONTANTS                   */
    /*--------------------------------------*/
 
 
    assign z-typcom = substring( zone, 26,  3)
           z-numbon = substring( zone, 29, 10)
           z-typbon = substring( zone, 39,  1) .
 
    /* Lecture de Lignes */
    for each lignes where lignes.codsoc = codsoc-soc
                      and lignes.motcle = "VTE"
                      and lignes.typcom = z-typcom
                      and lignes.numbon = z-numbon
                      use-index primaire no-lock .
 
        if lignes.qte[2]   = 0 then next .
        if lignes.codlig   = "T" then next .
        if lignes.qte[ 2 ] = 0   then next .
 
        if z-typbon = "C" then i = 10 .                  /* COMPTANT */
                          else i = 20 .                  /* TERME    */
 
        assign mont[ i + 1 ] = mont[ i + 1 ] + lignes.fac-ht
               mont[ i + 2 ] = mont[ i + 2 ] + lignes.fac-ttc .
 
        assign i = 30                                   /* GLOBAL */
               mont[ i + 1 ] = mont[ i + 1 ] + lignes.fac-ht
               mont[ i + 2 ] = mont[ i + 2 ] + lignes.fac-ttc.
 
    end . /* for each */
 
    nb-bon = nb-bon + 1 .
    leave .
 
END .
 
 
/*    Total DEPOT   */
/*==================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 2 :
 
   do while nb-bon <> 0 :
       assign   ind-tot    = 00
                nb-mag     = nb-mag  + 1 .
 
       run st-caj1t.p .
       leave .
   end .
 
   leave .
END .
 
 
/*    Total  DATE FAC           */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 1 :
 
   do while nb-mag <> 0 :
       assign lib-tot = libelle[ 41 ] + " " + moisan
              ind-tot = 100
              nb-dat  = nb-dat + 1.
 
       run st-caj1t.p .
       leave .
   end .
 
   leave .
END .