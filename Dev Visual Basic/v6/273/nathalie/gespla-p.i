/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/gespla-p.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 29713 23-03-12!Bug!cch            !Correction pble 'La saisie 3 est en dehors de la liste' - Suite Fiche !
!V61 29110 16-02-12!Evo!cch            !Ajout solde lignes non livr�es + ent�te cde - Suite Fiche V61 29087   !
!V52 16412 15-11-07!Bug!cch            !Correction pble sur diff. d'article si 'BL' et diff. de magasin si 'Ol!
!V52 12884 20-06-05!New!cch            !Ajout envoi des Stocks par Magasin � LISADIS                          !
!V52 12576 06-05-05!New!cch            !Ajout Extraction R�ceptions RAR issues de PRODEPOTvers LISADIS        !
!V52 12559 02-05-05!New!cch            !Int�gration mouvements ARC LISADIS                                    !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/******************************************************************************/
/* GESPLA-P.I - GESTION DES ECHANGES DE DONNEES NATHALIE / PLATEFORMES        */
/*----------------------------------------------------------------------------*/
/* Procedures Communes                              08/06/2004 - SOPRES - 273 */
/******************************************************************************/

/* Traitement des rejets */
PROCEDURE TRT-REJET :

    if not ok-ouv-rej
    then do :
        output stream kan-rej to value( fic-rej ) .
        assign ok-ouv-rej = yes 
               zone       = chr( 27 ) + "&l1O" + "/* " + /* Mise a l'italienne */
                            entry( 3, libelle[ 61 ] ) + " " + lib-prog + " " +
                            string( today ) + " " + string( time, "hh:mm:ss" )
               .
        put stream kan-rej unformatted zone skip( 1 ) .
        zone = "/* " + trim( libelle[ 3 ] ) + " " + fic-rej .
        put stream kan-rej unformatted zone skip .
        zone = "/* " + libelle[ 69 ] + " " + fic-trt .
        put stream kan-rej unformatted zone skip( 1 ) .
    end .

    if x-message <> "" or y-message <> ""
    then do :
        if no-enreg <> 0 or codsoc-soc <> ""
        then do :
            if no-enreg <> 0 then zone = libelle[ 62 ] + string( no-enreg ) .
            if codsoc-soc <> "" then if no-enreg = 0 then zone = "/* " + libelle[ 113 ] + codsoc-soc .
                                                     else zone = zone + " - " + libelle[ 113 ] + codsoc-soc .
            put stream kan-rej unformatted zone skip .
        end .
        if x-message <> ""
        then do :
            x-message = "/* " + x-message .
            put stream kan-rej unformatted x-message skip .
        end .
        if y-message <> "" then put stream kan-rej unformatted y-message skip .
    end .

    if ok-rej-bon /* Rejet bon entier */
    then do :

        FOR EACH BON use-index i-bon-1 no-lock :
            enreg = libelle[ 62 ] + string( bon.chrono ) .
            put stream kan-rej unformatted enreg skip .
            put stream kan-rej unformatted bon.enreg skip .
        END .

        put stream kan-rej unformatted " " skip .

    end .

    if pos-rowid-cde <> 0 then entry( pos-rowid-cde, l-solde-cde[ 1 ] ) = "N" . /* Pour ne pas solder lignes non livrees et entete cde ds gespla-5.p */

    if x-message <> "" then nb-err = nb-err + 1 .

    assign x-message = ""
           y-message = ""
           .

END PROCEDURE . /* TRT-REJET */

/* MAJ No Interchange */
PROCEDURE MAJ-INTERCH :

    codtab = caps( string( type-ech, "x(10)" ) + "-" +
             string( ean13-emet, "x(13)" )    + "-" +
             ean13-destin ) .

    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "EDI"
                and   tabges.prefix = "INTERCHANG"
                and   tabges.codtab = codtab
                exclusive-lock no-error .

    if not available tabges
    then do :

        CREATE TABGES .

        assign tabges.codsoc = ""
               tabges.etabli = ""
               tabges.typtab = "EDI"
               tabges.prefix = "INTERCHANG"
               tabges.codtab = codtab
               .

    end .

    if ok-maj-interch
    then do :
        assign sav-no-interch     = tabges.nombre[ 1 ]
               sav-date-ech       = tabges.dattab[ 1 ]
               sav-heure-ech      = tabges.libel1[ 1 ]
               tabges.nombre[ 1 ] = tabges.nombre[ 1 ] + 1
               no-interch         = tabges.nombre[ 1 ]
               tabges.dattab[ 1 ] = date( int( substr( date-ech, 5, 2 ) ),
                                          int( substr( date-ech, 7, 2 ) ),
                                          int( substr( date-ech, 1, 4 ) ) )
               tabges.libel1[ 1 ] = heure-ech
               .

        { majmoucb.i tabges }
    end .

    else if no-interch = tabges.nombre[ 1 ]
         then do :
             assign tabges.nombre[ 1 ] = sav-no-interch
                    tabges.dattab[ 1 ] = sav-date-ech
                    tabges.libel1[ 1 ] = sav-heure-ech
                    .

             { majmoucb.i tabges }
             if tabges.nombre[ 1 ] = 0 then DELETE TABGES .
         end .

END PROCEDURE . /* MAJ-INTERCH */

/* Lecture Correspondances E.D.I. ( DEAL -> EDI ) */
PROCEDURE LECT-COR :

    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "EDI"
                and   tabges.prefix = prefix
                and   tabges.codtab = codtab
                no-lock no-error .

    if available tabges and tabges.libel1[ mem-langue ] <> ""
       then codtab = entry( 1, right-trim( tabges.libel1[ mem-langue ] ) ) .

END PROCEDURE .

/* Lecture Correspondances E.D.I. ( EDI -> DEAL )
   ATTENTION pour les unites 1 unite EDI peut donner plusieurs unites DEAL
*/
PROCEDURE LECT-COR-2 :

    l-corresp = "" .

    FOR EACH TABGES where tabges.codsoc = ""
                    and   tabges.etabli = ""
                    and   tabges.typtab = "EDI"
                    and   tabges.prefix = prefix
                    and   lookup( right-trim( codtab ), tabges.libel1[ mem-langue ] ) <> 0
                    use-index primaire no-lock :

        if l-corresp = "" then l-corresp = right-trim( tabges.codtab ) .
                          else l-corresp = l-corresp + "," + right-trim( tabges.codtab ) .

    END .

    if l-corresp = "" then l-corresp = codtab .

END PROCEDURE .


