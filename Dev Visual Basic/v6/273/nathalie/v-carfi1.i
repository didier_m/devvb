/*==========================================================================*/
/*                           V - C A R F I 1 . I                            */
/* Recherche Carte Fidelite Client                                          */
/*--------------------------------------------------------------------------*/
/* Acces Table                                                              */
/*==========================================================================*/

lect = "{1}" .

if lect = ""
   then FIND AUXDET where auxdet.codsoc = x-codsoc
                    and   auxdet.typaux = x-typaux
                    and   auxdet.codaux = x-codaux
                    and   auxdet.motcle = x-motcle
                    and   auxdet.codtab = rech-clef
                    no-lock no-error .

   else FIND {1} AUXDET where auxdet.codsoc = x-codsoc
                        and   auxdet.typaux = x-typaux
                        and   auxdet.codaux = x-codaux
                        and   auxdet.motcle = x-motcle
                        and   auxdet.codtab begins libel
                        no-lock no-error .

if available auxdet
then do:

    FIND MAGASI where magasi.codsoc = magasi-soc
                and   magasi.codtab = auxdet.calf09
                no-lock no-error .

    if available magasi then lib-mag = magasi.libel1[ mem-langue ] .
                        else lib-mag = "" .

    if auxdet.calf01 = x-top-att then lib-aff = x-lib-att .
                                 else lib-aff = auxdet.calf03 .

end .