/*============================================================================*/
/*                           G T M - S I M 0 . I                              */
/* Simulation de Tarifs Ventes par Magasins                                   */
/*----------------------------------------------------------------------------*/
/* Variables et Frame                                                         */
/*============================================================================*/

def var i           as int                               no-undo .
def var magasin-c   as char format "xxx"                 no-undo .
def var article-c   as char format "x(10)"               no-undo .
def var famille-c   as char format "xxx"                 no-undo .
def var soufam-c    as char format "xxx"                 no-undo .
def var date-app-c  as date format "99/99/9999"          no-undo .
def var typol-c     as char format "x"                   no-undo .
def var coef-c      as int  format ">>9"                 no-undo .
def var tarif-b     as dec  format ">>>>>9.999"          no-undo .
def var date-app-b  as char format "x(10)"      extent 2 no-undo .
def var tarif-m     as dec  format ">>>>>9.999"          no-undo .
def var autor-m     as char format "xxx"                 no-undo .
def var lib-mag-c   as char format "x(32)"               no-undo .
def var lib-art-c   as char format "x(32)"               no-undo .
def var lib-fam-c   as char format "x(30)"               no-undo .
def var lib-sfam-c  as char format "x(30)"               no-undo .
def var lib-typol-c as char format "x(15)"               no-undo .

def {1} shared var libelle    as char extent 40           no-undo .
def {1} shared var i-param    as char                     no-undo .
def {1} shared var magasin    as char format "xxx"        no-undo .
def {1} shared var article    as char format "x(10)"      no-undo .
def {1} shared var date-tar-m as date format "99/99/9999" no-undo .
def {1} shared var code-tar-b as char                     no-undo .
def {1} shared var lib-mag    as char format "x(32)"      no-undo .
def {1} shared var lib-art    as char format "x(32)"      no-undo .

def {1} shared frame fr-cle .
def {1} shared frame fr-coef .
def {1} shared frame fr-tar-b .
def {1} shared frame fr-tar-m .

form libelle[ 1 ] format "x(17)" magasin    lib-mag skip
     libelle[ 2 ] format "x(17)" article    lib-art skip
     libelle[ 3 ] format "x(17)" date-tar-m
     with frame fr-cle row 2 centered overlay {v6frame.i} no-label .

form libelle[ 7 ]  format "x(20)" magasin-c  lib-mag-c   skip
     libelle[ 8 ]  format "x(20)" article-c  lib-art-c   skip
     libelle[ 9 ]  format "x(20)" famille-c  lib-fam-c   skip
     libelle[ 10 ] format "x(20)" soufam-c   lib-sfam-c  skip
     libelle[ 11 ] format "x(20)" date-app-c             skip
     libelle[ 12 ] format "x(20)" typol-c    lib-typol-c skip
     libelle[ 13 ] format "x(20)" coef-c
     with frame fr-coef row 8 centered overlay {v6frame.i} no-label
     title libelle[ 6 ] .

form libelle[ 17 ] format "x(20)" tarif-b skip
     libelle[ 18 ] format "x(20)" date-app-b[ 1 ] libelle[ 19 ] date-app-b[ 2 ]
     skip
     with frame fr-tar-b row 17 col 2 overlay {v6frame.i} no-label
     title libelle[ 16 ] .

form libelle[ 21 ] format "x(12)" tarif-m skip
     libelle[ 22 ] format "x(12)" autor-m
     with frame fr-tar-m row 17 col 55 overlay {v6frame.i} no-label
     title libelle[ 20 ] .
