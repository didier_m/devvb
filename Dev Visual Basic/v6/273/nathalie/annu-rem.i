/*===========================================================================*/
/*                            A N N U - R E M . I                            */
/* Annulation remise non significative                                       */
/* Utilise dans e-fvte1l.p, r-fvte1l.p, maj-stv1.i                           */
/*===========================================================================*/

if pu-brut - pu-net < 0.006 and pu-brut > 0.3 then pu-brut = pu-net .
