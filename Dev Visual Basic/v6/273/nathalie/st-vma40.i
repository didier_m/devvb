/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/st-vma40.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V61 36608 06-03-13!Bug!cch            !Correction pble sur calcul marge n - 2 + stock au PAMP n et n - 1     !
!V61 36427 26-02-13!Bug!cch            !Correction pble calcul marge n - 2                                    !
!V61 26406 20-09-11!Bug!cch            !Correction probl�me totaux � z�ro                                     !
!V61 26390 19-09-11!Evo!cch            !Suite Fiche V61352 - Supprerssion question "Mois du PAMP"             !
!V61 26360 16-09-11!Evo!cch            !Ajout affichage date dernier arr�t� de stocks                         !
!V61 26285 14-09-11!Evo!cch            !Suite Fiche V61 26282                                                 !
!V61 26282 14-09-11!Evo!cch            !Ajout �dition ann�e N - 2                                             !
!V61 26257 12-09-11!Bug!cch            !Correction pble di�ses sur �dition des pamps mensuels                 !
!V61 25527 11-07-11!Bug!cch            !Correction probl�me pamp � z�ro en traitement multi soci�t�s          !
!V61 23476 23-03-11!Bug!cch            !Correction diverses suite retourc test client                         !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*===========================================================================*/
/*                          S T - V M A 4 0 . I                              */
/* Edition Statistique des Ventes et Marges / Article - Multi Societes       */
/*===========================================================================*/

def var zone                    as char                            no-undo .
def var tour-mag                as int                             no-undo .
def var i                       as int                             no-undo .
def var j                       as int                             no-undo .
def var k                       as int                             no-undo .
def var l                       as int                             no-undo .
def var date-dern-arr           as date format "99/99/9999"        no-undo .

def {1} shared var libelle      as char                 extent 105 no-undo .
def {1} shared var valeur       as char                 extent 136 no-undo .

def {1} shared var mat-edi      as log                  extent 136 no-undo .
def {1} shared var mat-pmp      as log                  extent 6   no-undo .
def {1} shared var masque       as char                 extent 100 no-undo .
def {1} shared var masque-tot   as char                 extent 100 no-undo .
def {1} shared var nb-dec       as int                  extent 100 no-undo .

def {1} shared var tot-val      as dec                  extent 489 no-undo .
def {1} shared var tot-qte      as dec                  extent 520 no-undo .
def {1} shared var ind-tot      as int                             no-undo .
def {1} shared var lib-tot      as char                            no-undo .

def {1} shared var type-edit    as char                            no-undo .
def {1} shared var lib-titre    as char                            no-undo .
def {1} shared var lib-titre-2  as char                            no-undo .
def {1} shared var no-famille   as char                            no-undo .
def {1} shared var no-nomenc    as char                            no-undo .
def {1} shared var lib-famille  as char                            no-undo .
def {1} shared var lib-nomenc   as char                            no-undo .
def {1} shared var lib-ssfam    as char                            no-undo .
def {1} shared var type-fou     as char                            no-undo .
def {1} shared var no-fou       as char                            no-undo .
def {1} shared var lib-fou      as char                            no-undo .
def {1} shared var no-magasin   as char                            no-undo .
def {1} shared var lib-magasin  as char                            no-undo .
def {1} shared var lib-periode  as char                            no-undo .

def {1} shared var date-arr     as date                            no-undo .
def {1} shared var date-arrm1   as date                            no-undo .
def {1} shared var date-arrm2   as date                            no-undo .
def {1} shared var date-arr-q   as date                            no-undo .
def {1} shared var date-arrm1-q as date                            no-undo .
def {1} shared var date-arrm2-q as date                            no-undo .
def {1} shared var date-tar     as date                            no-undo .
def {1} shared var date-tarm1   as date                            no-undo .
def {1} shared var date-tarm2   as date                            no-undo .

def {1} shared var annee-arr    as char                 extent 10  no-undo .
def {1} shared var mois-arr     as int                  extent 10  no-undo .
def {1} shared var annee-arrm1  as char                 extent 10  no-undo .
def {1} shared var annee-arrm2  as char                 extent 10  no-undo .
def {1} shared var mois-arrm1   as int                  extent 10  no-undo .
def {1} shared var mois-arrm2   as int                  extent 10  no-undo .

def {1} shared var mois-debcp   as char format "x(6)"              no-undo .
def {1} shared var mois-fincp   as char format "x(6)"              no-undo .
def {1} shared var mois-debex   as char format "x(6)"              no-undo .
/*
def {1} shared var mois-pamp    as log  format "0/1"               no-undo .
def {1} shared var lib-pamp     as char format "x(20)"             no-undo .
*/
def {1} shared var type-soc     as char format "x"                 no-undo .
def {1} shared var lib-typsoc   as char format "x(30)"             no-undo .
def {1} shared var l-soc        as char format "x(19)"             no-undo .
def {1} shared var zone-var     as char format "x(47)"             no-undo .
def {1} shared var magasins     as char                            no-undo .
def {1} shared var fournisseurs as char                            no-undo .
def {1} shared var type-tri     as char format "x"                 no-undo .
def {1} shared var lib-tri      as char format "x(14)"             no-undo .
def {1} shared var familles     as char format "x(43)"             no-undo .
def {1} shared var regr-stat    as char format "x"                 no-undo .
def {1} shared var lib-regr     as char format "xxx"               no-undo .
def {1} shared var opt-glob     as log  format "G/D"   initial yes no-undo .
def {1} shared var lib-glob     as char format "x(20)"             no-undo .
def {1} shared var ajust        as log  format "O/N"               no-undo .
def {1} shared var lib-ajust    as char format "x(4)"              no-undo .
def {1} shared var maquette     as char format "x(12)"             no-undo .
def {1} shared var lib-maq      as char format "x(35)"             no-undo .
def {1} shared var periph       as char format "x(33)"             no-undo .
def {1} shared var chemin       as char format "x(18)"             no-undo .

def {1} shared var fam-exclus   as char  format  "xx"              no-undo .
def {1} shared var mag-exclus   as char  format  "xx"              no-undo .

def {1} shared var mois-deb     as int                             no-undo .
def {1} shared var anee-deb     as int                             no-undo .
def {1} shared var mois-fin     as int                             no-undo .
def {1} shared var anee-fin     as int                             no-undo .
def {1} shared var mois-dex     as int                             no-undo .
def {1} shared var anee-dex     as int                             no-undo .

def {1} shared var mois-debm1   as int                             no-undo .
def {1} shared var mois-debm2   as int                             no-undo .
def {1} shared var mois-finm1   as int                             no-undo .
def {1} shared var mois-finm2   as int                             no-undo .
def {1} shared var mois-dexm1   as int                             no-undo .
def {1} shared var mois-dexm2   as int                             no-undo .

def {1} shared var annee-min    as char                            no-undo .
def {1} shared var annee-max    as char                            no-undo .

def {1} shared var sav-periph   as char                            no-undo .
def {1} shared var z-choix      as char                            no-undo .

def {1} shared var l-mag        as char                 extent 10  no-undo .
def {1} shared var l-motcle     as char                            no-undo .

def {1} shared var ok-deb       as log                             no-undo .
def {1} shared var ok-fin       as log                             no-undo .
def {1} shared var ok-tot       as log                             no-undo .

def {1} shared var articl-soc   as char                            no-undo .
def {1} shared var magasi-soc   as char                            no-undo .
def {1} shared var auxapr-soc   as char                            no-undo .

def {1} shared var ind-nomenc   as char                            no-undo .
def {1} shared var prefix-ssf   as char                            no-undo .

def {1} shared var platef-lis   as char                            no-undo .

def {1} shared var nb-trt       as int                             no-undo .
def {1} shared var nb-cre       as int                  extent 2   no-undo .
def {1} shared var tour-soc     as int                             no-undo .
def {1} shared var tour-trt     as int                             no-undo .
def {1} shared var codsoc-min   as char                            no-undo .
def {1} shared var codsoc-max   as char                            no-undo .

def stream kan-tri .

def {1} shared temp-table T-ARTICLE
        field codsoc        as char
        field articl        as char
        field libart        as char
        field typaux        as char
        field codaux        as char
        field famart-1      as char
        field catego-1      as char
        field soufam-1      as char
        field sssfam-1      as char
        field famart-2      as char
        field soufam-2      as char
        field sssfam-2      as char
        field poids         as dec
        field volume        as dec
        field unite-hectare as dec
        index i-cle-1 is primary unique codsoc articl
        index i-cle-2 is unique codsoc typaux codaux famart-1 catego-1 soufam-1 sssfam-1 libart articl .

def {1} shared temp-table T-ARTICLE-2
        field codsoc      as char
        field articl-stat as char
        field articl-num  as char
        field tva           as char
        field qte1        as dec extent 10
        field qte2        as dec extent 10
        field qte3        as dec extent 10
        field qte4        as dec extent 10
        field qte5        as dec extent 10
        field qte6        as dec extent 10
        field stock-arr2  as dec extent 10
        field stock-arr4  as dec extent 10
        field stock-arr6  as dec extent 10
        index i-cle is primary unique codsoc articl-stat articl-num .

def {1} shared frame fr-saisie .

form libelle[ 09 ] format "x(28)" mois-debcp space ( 13 ) libelle[ 01 ] format "x(17)" date-dern-arr skip
     libelle[ 10 ] format "x(28)" mois-fincp            skip
     libelle[ 11 ] format "x(28)" mois-debex            skip
  /* libelle[ 12 ] format "x(28)" mois-pamp  lib-pamp   skip */
     libelle[ 34 ] format "x(28)" type-soc   lib-typsoc skip
     libelle[ 40 ] format "x(28)" l-soc                 skip
     libelle[ 13 ] format "x(28)" zone-var              skip
     libelle[ 06 ] format "x(28)" type-tri   lib-tri    skip
     libelle[ 14 ] format "x(28)" familles              skip
     libelle[ 07 ] format "x(28)" regr-stat  lib-regr   skip
     libelle[ 15 ] format "x(28)" opt-glob   lib-glob   skip
     libelle[ 16 ] format "x(28)" ajust      lib-ajust  skip
     libelle[ 17 ] format "x(28)" maquette   lib-maq    skip
     libelle[ 18 ] format "x(28)" periph                skip
     libelle[ 08 ] format "x(28)" chemin
     with frame fr-saisie
     row 2 centered overlay no-label { v6frame.i } .




