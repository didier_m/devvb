/*==========================================================================*/
/*                           E X T - P 2 F A . I                            */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Nomenclatures Articles PDP V2                   CC 01/03/1999 */
/*==========================================================================*/

zone = "001       01 " .

if tabges.prefix = "FAMART"
   then zone = zone + "10500000"
                    + string ( substring ( tabges.codtab , 2, 2 ) , "xx" ) .
   else zone = zone + "106000"
                    + string ( substring ( tabges.prefix , 8, 2 ) , "xx" )
                    + string ( substring ( tabges.codtab , 2, 2 ) , "xx" ) .
zone = zone + string ( tabges.libel1[mem-langue] , "x(30)" ) .
