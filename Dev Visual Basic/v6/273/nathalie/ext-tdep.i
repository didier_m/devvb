/*==========================================================================*/
/*                           E X T - T D E P . I                            */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Depots TRAJECTOR                                CC 01/03/1999 */
/*==========================================================================*/

zone = "DEP  " + string ( magasi.codtab , "x(3)" )
               + "            "
               + string ( magasi.libel1[1] , "x(32)" )
               + "               "
               + string ( magasi.adres[5] , "x(5)" )
               + "          "
               .
