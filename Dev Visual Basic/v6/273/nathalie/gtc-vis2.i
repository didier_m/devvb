/*----------------------------------------------------------------------------*/
/*                             G T C - V I S 2 . I                            */
/*                 Edition  PROGLIB : Recap / Categories                      */
/*----------------------------------------------------------------------------*/
 
rec-clef = recid( proglib ) .
 
if type-trt <> "F"  then
repeat :

    if code-fourn <> ""
    then do :
        assign prog-soufam = substring( proglib.clef, 11 )
               lib-catego  = lib-fourn
               .
        leave .
    end .
    
    FIND TABGES where tabges.codsoc = ""
    	        and   tabges.etabli = ""
	        and   tabges.typtab = "ART"
	        and   tabges.prefix = "SOUFAM" + substring( proglib.clef, 11, 3 )
	        and   tabges.codtab = substring( proglib.clef, 14, 3 )
	        no-lock  no-error .
 
    lib-catego = fill( " " , 30 ) .
    if available tabges then
       lib-catego = tabges.libel1[ mem-langue ] .
 
    prog-soufam = substring( proglib.clef, 11,  3 ) + "." +
 	          substring( proglib.clef, 14,  3 ) .

    leave .
    
end .
else prog-soufam = trim( substring( proglib.clef, 14 ,  10 ) ) .
 
assign nb-v = integer( substring( proglib.libelle[ 1 ] ,  1 , 5 ) )
       nb-r = integer( substring( proglib.libelle[ 1 ] ,  6 , 5 ) )
       nb-a = integer( substring( proglib.libelle[ 1 ] , 11 , 5 ) )
       nb-b = integer( substring( proglib.libelle[ 1 ] , 16 , 5 ) ) .
 
display rec-clef blank  prog-soufam  lib-catego  nb-b  nb-a  nb-v  nb-r
	with frame lig-rech .