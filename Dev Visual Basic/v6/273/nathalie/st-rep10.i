/*==========================================================================*/
/*                          S T - R E P 1 0 . I                             */
/* Statistique Vente Representant                                           */
/* Etat des Qtes & CA    / Famille / Zone                                   */
/*==========================================================================*/
 
/* Tbl des Libelles Etat & Tbl d'Edition & enreg fichier tri */
def {1} shared var libelle       as char                 extent 99   no-undo .
def {1} shared var edit          as char                 extent 49   no-undo .
def {1} shared var zone          as char                             no-undo .
def {1} shared stream maquette.                             /* canal edition*/
def {1} shared stream kan-tri .
 
/* zone de traitement */
def {1} shared var recid-stacli as recid                           no-undo .
def {1} shared var ent-rep      as logical                         no-undo .
def {1} shared var ent-fam      as logical                         no-undo .
def {1} shared var nb-fam       as int                             no-undo .
def {1} shared var nb-rep       as int                             no-undo .
def {1} shared var nb-zone      as int                             no-undo .
def {1} shared var nb-recid     as int                             no-undo .
def {1} shared var no-rep       as char                            no-undo .
def {1} shared var lib-rep      as char                            no-undo .
def {1} shared var no-fam       as char                            no-undo .
def {1} shared var lib-fam      as char                            no-undo .
def {1} shared var no-zone      as char                            no-undo .
def {1} shared var lib-zone     as char                            no-undo .
def {1} shared var tot          as dec                 extent  50  no-undo .
def {1} shared var msk          as char                extent   4  no-undo .
def {1} shared var ind-tot      as int                             no-undo .
def {1} shared var lib-tot      as char                            no-undo .
def {1} shared var lib-tit      as char                            no-undo .
def {1} shared var lib-tit1     as char                            no-undo .
 
 
/* Definition des zones & de la frame */
def {1} shared var dat-debx     as date format "99/99/9999"         no-undo .
def {1} shared var dat-finx     as date format "99/99/9999"         no-undo .
def {1} shared var mois-deb     as date format "99/99/9999"         no-undo .
def {1} shared var mois-fin     as date format "99/99/9999"         no-undo .
def {1} shared var familles     as char format "x(40)"              no-undo .
def {1} shared var maquette     as char format "x(12)"              no-undo .
def {1} shared var periph       as char format "x(12)"              no-undo .
def {1} shared var lib-maq      as char format "x(30)"              no-undo .
 
 
def {1} shared var z-choix      as char                             no-undo .
 
def {1} shared frame fr-saisie .
 
form libelle[ 11 ] format "x(28)"  dat-debx     dat-finx    skip
     libelle[ 12 ] format "x(28)"  mois-deb     mois-fin    skip
     libelle[ 13 ] format "x(28)"  familles                 skip
     libelle[ 14 ] format "x(28)"  maquette     lib-maq     skip
     libelle[ 15 ] format "x(28)"  periph
     with frame fr-saisie
     row 3  centered  overlay  no-label  with title libelle[ 10 ] .