/*============================================================================*/
/*                           G T M - P R O 0 . I                              */
/* Generation des Tarifs Ventes Promos par Magasins                           */
/*----------------------------------------------------------------------------*/
/* Variables et Frame                                                         */
/*============================================================================*/

def var i           as int                      no-undo .
def var grp-mag     as char format "xxx"        no-undo .
def var magasin     as char format "xxx"        no-undo .
def var promo-no    as int  format "99999"      no-undo .
def var article     as char format "x(10)"      no-undo .
def var date-deb    as date format "99/99/9999" no-undo .
def var date-fin    as date format "99/99/9999" no-undo .
def var prix-art    as dec  format ">>>>9.999"  no-undo .
def var lib-grp     as char format "x(30)"      no-undo .
def var lib-mag     as char format "x(32)"      no-undo .
def var lib-promo   as char format "x(30)"      no-undo .
def var lib-art     as char format "x(32)"      no-undo .
def var magasi-soc  as char                     no-undo .
def var promo-tarif as char                     no-undo .

def {1} shared var libelle      as char extent 30 no-undo .
def {1} shared var promo-soc    as char           no-undo .
def {1} shared var promo-motcle as char           no-undo .
def {1} shared var promo-typcom as char           no-undo .

def {1} shared frame fr-saisie .

form libelle[ 1 ] format "x(19)" grp-mag  lib-grp   skip( 1 )
     libelle[ 2 ] format "x(19)" magasin  lib-mag   skip( 1 )
     libelle[ 3 ] format "x(19)" promo-no lib-promo skip( 1 )
     libelle[ 4 ] format "x(19)" article  lib-art   skip( 1 )
     libelle[ 5 ] format "x(19)" date-deb           skip( 1 )
     libelle[ 6 ] format "x(19)" date-fin           skip( 1 )
     libelle[ 7 ] format "x(19)" prix-art
     with frame fr-saisie row 3 centered overlay {v6frame.i} no-label .
