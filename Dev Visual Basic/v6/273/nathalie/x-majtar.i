/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/x-majtar.i                                                                     !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End                                                                                                          
**************************************************************************************************************/
        assign
             remcli.codrem       = ""
             remcli.typrem       = ""
             remcli.typ-borne    = ""
             remcli.typapp       = ""
             remcli.borne        = 0
             remcli.valeur       = 0  .
             assign
                  remcli.codrem[1]    = "RFA"
                  remcli.typrem[1]    = "1"
                  remcli.typ-borne[1] = ""
                  remcli.typapp[1]    = "2"
                  remcli.valeur[1]    = remise
                  .
             assign
                  remcli.codrem[2]    = "RFC"
                  remcli.typrem[2]    = "3"
                  remcli.typ-borne[2] = ""
                  remcli.typapp[11]   = "2"
                  remcli.valeur[11]   = remrfc
                  .
             { majmoucb.i remcli }