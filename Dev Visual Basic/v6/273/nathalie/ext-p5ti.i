/*==========================================================================*/
/*                           E X T - P 5 T I . I                            */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Tiers / Depots PDP V5                           CC 24/04/1999 */
/*==========================================================================*/

                                                        /* Pos.  Long. */

case code-table :
    when "CL/FO" or when "CL" or when "FO"
    then do :

        zone = "TIERS   "                               /* 001 - 008 */
               + "C"                                    /* 009 - 001 */
               + string(codsoc-trait[nsoc], "xx")       /* 010 - 002 */
               + fill(" ", 6)                           /* 012 - 006 */
               .

        code-tiers = trim(auxapr.codaux).
        { cadra-g.i "code-tiers" "6" "0" }

        zone = zone
               + string(code-tiers, "x(10)")            /* 018 - 010 */
               .

        case notour :                                   /* 028 - 002 */
            when 1
            then do:
                if auxapr.cpt-ferme = "1"
                then zone = zone + "XX" .
                else do:
/* ajout dgr 25/01/05 */
                  if codsoc-trait[nsoc] =  "08" and 
                     auxapr.type-adh    =  ""   then do:
                    if auxapr.frequence-fac = "3" then 
                      zone = zone + "00".
                    else 
                      zone = zone + "40".
                  end.
                  else do:
/* fin ajout dgr 25/01/05 */
                    FIND TABGES where tabges.codsoc = ""
                                and   tabges.etabli = ""
                                and   tabges.typtab = "CLI"
                                and   tabges.prefix = "TYPE-ADH"
                                and   tabges.codtab = auxapr.type-adh
                                no-lock no-error.

                    if available tabges
                       then zone = zone + string ( tabges.libel2[1] , "xx" ) .
                       else zone = zone + "10" .

/* ajout dgr 25/01/05 */
                  end.
/* fin ajout dgr 25/01/05 */
                end.
            end.

            otherwise do :
/* ajout dgr 25/01/05 */
              if codsoc-trait[nsoc] =  "08" and 
                 auxapr.type-adh    =  ""   then do:
                if auxapr.frequence-fac = "3" then 
                  zone = zone + "00".
                else 
                  zone = zone + "40".
              end.
              else do:
/* fin ajout dgr 25/01/05 */

                FIND TABGES where tabges.codsoc = ""
                            and   tabges.etabli = ""
                            and   tabges.typtab = "CLI"
                            and   tabges.prefix = "TYPE-ADH"
                            and   tabges.codtab = auxapr.type-adh
                            no-lock no-error.

                if available tabges
                   then zone = zone + string ( tabges.libel2[1], "xx" ) .
                   else zone = zone + "  " .
/* ajout dgr 25/01/05 */
              end.
/* fin ajout dgr 25/01/05 */
            end.

        end case.

        libel = trim(substr(auxili.adres[1], 1, 3)).    /* 030 - 038 */

        if lookup(libel, libelle[26]) <> 0
        then do :
            zone = zone
                   + string(libel, "x(6)")
                   .
            if substr(auxili.adres[1], 4 , 1) = ""
            then zone = zone
                        + string(substr(auxili.adres[1], 5, 28), "x(32)")
                        .
            else zone = zone
                        + string(substr(auxili.adres[1], 4, 28), "x(32)")
                        .
        end.

        else zone = zone
                    + fill (" ", 6)
                    + string(auxili.adres[1], "x(32)")
                    .

        zone = zone
               + string(auxili.adres[3], "x(35)")       /* 068 - 035 */
               + string(auxili.adres[4], "x(35)")       /* 103 - 035 */
                                                        /* 138 - 025 */
               + string(substr(auxili.adres[5], 7), "x(25)" )
               + string(auxapr.teleph, "x(20)")         /* 163 - 020 */
               .

        case notour :
            when 1 then zone = zone
                   + string(auxapr.contentieux, "xx")   /* 183 - 002 */
                   + string(auxapr.magasin, "x(6)")     /* 185 - 006 */
                   .
            otherwise zone = zone + fill(" ", 8).       /* 183 - 008 */
        end case.

        zone = zone
               + fill(" ", 12)                          /* 191 - 012 */
               + string(auxili.libabr, "x(32)")         /* 203 - 032 */
               .

        case notour :                                   /* 235 - 001 */
            when 1 then zone = zone + "A".
            otherwise   zone = zone + "E".
        end case.

        zone = zone
               + string(codsoc-trait[nsoc], "xx")       /* 236 - 002 */
               .

        case notour :                                   /* 238 - 006 */
            when 1 then zone = zone + string(auxapr.repres[1], "x(6)").
            otherwise   zone = zone + fill(" ", 6).
        end case.

        zone = zone
                + fill(" ", 1)                           /* 244 - 001 */
                + string(auxili.adres[2], "x(32)")       /* 245 - 032 */
                + fill(" ", 11)                          /* 277 - 011 */
               .

        libel = "".
        case notour :
        when 1
        then do :
            do i = 1 to 10 :
                if auxapr.bareme[i] = "" then next.
                libel = libel + auxapr.bareme[i].
            end.
            /*substr(libel, 1, 1) = "".*/
        end.
        end case.

        zone = zone
               + string(libel, "x(30)")                 /* 288 - 030 */
               + string(auxapr.fax, "x(20)")            /* 318 - 020 */
               .

        case notour :                                   /* 338 - 008 */
            when 1 then zone = zone + string(substr(auxili.adres[5], 1, 2),
                                             "x(8)").
            otherwise   zone = zone + fill(" ", 8).
        end case.

        zone = zone
               + string(auxili.pays, "x(8)")            /* 346 - 008 */
                                                        /* 354 - 009 */
               + string(substr(auxili.adres[5], 1, 5),"x(9)").

        case notour :                                   /* 363 - 008 */
            when 1 then zone = zone + string(auxapr.onic-commune, "x(8)").
            otherwise   zone = zone + fill(" ", 8).
        end case.

        zone = zone
               + fill(" ", 110)                         /* 371 - 110 */
               .

        case notour :                                   /* 363 - 008 */
            when 2 then if auxili.siret <> ""
                           then libel = auxili.siret.
                           else libel = auxapr.siret.
             otherwise libel = "".
        end case.

        zone = zone
               + string(libel, "x(20)")                 /* 481 - 020 */
               + fill(" ", 49)                          /* 501 - 049 */
               + fill("0", 24)                          /* 550 - 024 */
               .

        if auxapr.datcre <> ?                           /* 574 - 008 */
           then zone = zone + string(auxapr.datcre, "99999999").
           else zone = zone + fill( " ", 8 ).

        case notour :                                   /* 582 - 003 */
            when 2 then zone = zone + string(auxapr.delai-rea, "999").
            otherwise   zone = zone + fill("0", 3).
        end case.

        zone = zone
               + fill("0", 18)                          /* 585 - 018 */
               + string(auxapr.telex, "x(20)")          /* 603 - 020 */
               + fill(" ", 1)                           /* 623 - 001 */
               + string(auxapr.cee-ident, "x(20)")      /* 624 - 020 */
               .

        if auxapr.datmaj <> ?                           /* 644 - 008 */
           then zone = zone + string(auxapr.datmaj, "99999999").
           else zone = zone + fill( " ", 8 ).

        if auxapr.blocage = "1" or auxapr.cpt-ferme = "1"
           then zone = zone + "S".                      /* 652 - 001 */
           else zone = zone + " ".

        zone = zone
               + " N"                                   /* 653 - 002 */
               + fill(" ", 2)                           /* 655 - 002 */
               + fill("0", 7)                           /* 657 - 007 */
               .

        case notour :                                   /* 664 - 001 */
            when 2 then zone = zone + " "  /*substr(auxili.ttva, 3, 1)*/.
            otherwise   zone = zone + " ".
        end case.

        zone = zone
               + substr(auxapr.assujet, 2, 1)           /* 665 - 001 */
               + fill(" ", 28)                          /* 666 - 028 */
               + substring(auxapr.onic, 2, 11)          /* 694 - 011 */
               + fill(" ", 23)                          /* 705 - 023 */
               .

    end. /* when "CL/FO" */

    when "DE"
    then do :

        zone = "TIERS   "                               /* 001 - 008 */
               + "C"                                    /* 009 - 001 */
               + string(codsoc-trait[nsoc], "xx")       /* 010 - 002 */
               + fill(" ", 6)                           /* 012 - 006 */
               .

        code-tiers = trim(magasi.codtab).
        { cadra-g.i "code-tiers" "3" "0" }

        zone = zone
               + string(code-tiers, "x(10)")            /* 018 - 010 */
               + fill(" ", 8)                           /* 028 - 008 */
                                                        /* 036 - 032 */
               + string(magasi.libel1[mem-langue], "x(32)")
               + string(magasi.adres[2], "x(35)")       /* 068 - 035 */
               + string(magasi.adres[3], "x(35)")       /* 103 - 035 */
               + fill(" ", 25)                          /* 138 - 025 */
               + string(magasi.teleph, "x(20)")         /* 163 - 020 */
               + fill(" ", 52)                          /* 183 - 052 */
               + "T"                                    /* 235 - 001 */
               + string(codsoc-trait[nsoc], "xx")       /* 236 - 002 */
               + fill(" ", 312)                         /* 238 - 312 */
               + fill("0", 24)                          /* 550 - 024 */
               + fill(" ", 83)                          /* 574 - 083 */
               + fill("0", 7)                           /* 657 - 007 */
               + fill(" ", 64)                          /* 664 - 064 */
               .

    end. /* when "DE" */

    when "CF"
    then do :

        zone = "FIDELITE"                               /* 001 - 008 */
               + "M"                                    /* 009 - 001 */
               + string(soc-carte-fid, "xx")            /* 010 - 002 */
               + fill(" ", 3)                           /* 012 - 003 */
               + string(auxdet.calf09, "xxx")           /* 015 - 003 */
               + fill(" ", 10)                          /* 018 - 010 */
               + string(auxdet.calf11, "xx")            /* 028 - 002 */
               + string(auxdet.calf02, "x(6)")          /* 030 - 006 */
               + string(auxdet.calf03, "x(32)")         /* 036 - 032 */
               + string(auxdet.calf06, "x(35)")         /* 068 - 032 */
               + string(auxdet.calf07, "x(35)")         /* 103 - 032 */
                                                        /* 138 - 025 */
               + string(substr(auxdet.calf08, 7), "x(25)")
               + fill(" ", 22)                          /* 163 - 022 */
               + string(auxdet.calf09, "x(6)")          /* 185 - 006 */
               + fill(" ", 12)                          /* 191 - 012 */
               + string(auxdet.calf03, "x(32)")         /* 203 - 032 */
               + "J"                                    /* 235 - 001 */
               + string(soc-carte-fid, "xx")            /* 236 - 002 */
               + fill(" ", 39)                          /* 238 - 039 */
               .

        /* Modif par CCO le 26/01/2001     */
        code-tiers = trim(auxdet.calf13).
        { cadra-g.i "code-tiers" "6" "0" }

        /* Modif par CCO le 24/01/2001     */
        if code-tiers = "000000" then code-tiers = "099999".

        zone = zone
               + string(code-tiers, "x(10)")            /* 277 - 010 */
               + fill(" ", 31)                          /* 287 - 031 */
               + string(auxdet.codtab, "x(20)")         /* 318 - 020 */
                                                        /* 338 - 008 */
               + string(substr(auxdet.calf08, 1, 2), "x(8)" )
               + fill(" ", 8)                           /* 346 - 008 */
                                                        /* 354 - 009 */
               + string(substr(auxdet.calf08, 1, 5), "x(9)" )
               + fill(" ", 187)                         /* 338 - 187 */
                                                        /* 550 - 012 */
               + string(auxdet.cnum05 * 100, "99999999999-")
               + string(auxdet.cnum03, "99999999999-")  /* 562 - 012 */
               .

        if auxdet.cdat04 <> ?                           /* 574 - 008 */
           then zone = zone + string(auxdet.cdat04, "99999999").
           else zone = zone + fill( " ", 8 ).

        zone = zone
               + fill(" ", 62)                          /* 582 - 062 */
               .

        if auxdet.cdat01 <> ?                           /* 644 - 008 */
           then zone = zone + string(auxdet.cdat01, "99999999").
           else zone = zone + fill( " ", 8 ).

        zone = zone
               + fill(" ", 14)                          /* 652 - 014 */
               .

        if auxdet.cdat02 <> ?                           /* 666 - 008 */
           then zone = zone + string(auxdet.cdat02, "99999999").
           else zone = zone + fill( " ", 8 ).

        if auxdet.cdat03 <> ?                           /* 674 - 008 */
           then zone = zone + string(auxdet.cdat03, "99999999").
           else zone = zone + fill( " ", 8 ).

        zone = zone
               + fill(" ", 65)                          /* 682 - 065 */
               .

    end. /* when "CF" */

    when "CS"
    then do :

        zone = "FIDELITE"                               /* 001 - 008 */
               + "M"                                    /* 009 - 001 */
               + string(soc-carte-fid, "xx")            /* 010 - 002 */
               + fill(" ", 3)                           /* 012 - 003 */
               + string(wmag         , "xxx")           /* 015 - 003 */
               + fill(" ", 10)                          /* 018 - 010 */
               + string(auxdet.calf11, "xx")            /* 028 - 002 */
               + string(auxdet.calf02, "x(6)")          /* 030 - 006 */
               + string(auxdet.calf03, "x(32)")         /* 036 - 032 */
               + string(auxdet.calf06, "x(35)")         /* 068 - 032 */
               + string(auxdet.calf07, "x(35)")         /* 103 - 032 */
                                                        /* 138 - 025 */
               + string(substr(auxdet.calf08, 7), "x(25)")
               + fill(" ", 22)                          /* 163 - 022 */
               + string(wmag         , "x(6)")          /* 185 - 006 */
               + fill(" ", 12)                          /* 191 - 012 */
               + string(auxdet.calf03, "x(32)")         /* 203 - 032 */
               + "J"                                    /* 235 - 001 */
               + string(soc-carte-fid, "xx")            /* 236 - 002 */
               + fill(" ", 39)                          /* 238 - 039 */
               .

        /* Modif par CCO le 26/01/2001     */
        code-tiers = trim(auxdet.calf13).
        { cadra-g.i "code-tiers" "6" "0" }

        /* Modif par CCO le 24/01/2001     */
        if code-tiers = "000000" then code-tiers = "099999".

        zone = zone
               + string(code-tiers, "x(10)")            /* 277 - 010 */
               + fill(" ", 31)                          /* 287 - 031 */
               + string(auxdet.codtab, "x(20)")         /* 318 - 020 */
                                                        /* 338 - 008 */
               + string(substr(auxdet.calf08, 1, 2), "x(8)" )
               + fill(" ", 8)                           /* 346 - 008 */
                                                        /* 354 - 009 */
               + string(substr(auxdet.calf08, 1, 5), "x(9)" )
               + fill(" ", 187)                         /* 338 - 187 */
                                                        /* 550 - 012 */
               + string(auxdet.cnum05 * 100, "99999999999-")
               + string(auxdet.cnum03, "99999999999-")  /* 562 - 012 */
               .

        if auxdet.cdat04 <> ?                           /* 574 - 008 */
           then zone = zone + string(auxdet.cdat04, "99999999").
           else zone = zone + fill( " ", 8 ).

        zone = zone
               + fill(" ", 62)                          /* 582 - 062 */
               .

        if auxdet.cdat01 <> ?                           /* 644 - 008 */
           then zone = zone + string(auxdet.cdat01, "99999999").
           else zone = zone + fill( " ", 8 ).

        zone = zone
               + fill(" ", 14)                          /* 652 - 014 */
               .

        if auxdet.cdat02 <> ?                           /* 666 - 008 */
           then zone = zone + string(auxdet.cdat02, "99999999").
           else zone = zone + fill( " ", 8 ).

        if auxdet.cdat03 <> ?                           /* 674 - 008 */
           then zone = zone + string(auxdet.cdat03, "99999999").
           else zone = zone + fill( " ", 8 ).

        zone = zone
               + fill(" ", 65)                          /* 682 - 065 */
               .
    end. /* when "CS" */

end case.