/*==========================================================================*/
/*                           E X T - P 2 M E . I                            */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Messages magasins PDP V2                        CC 01/03/1999 */
/*==========================================================================*/

    zone-ident = trim(proglib.clef)
                 + " " + string ( proglib.datmaj , "99/99/9999" )
                 + " " + proglib.opemaj .

    if proglib.libelle[1] <> ""
    then do :

        do a = 1 to num-entries(proglib.libelle[1]) :

            if entry ( a , proglib.libelle[1] ) <> codsoc-trait[nsoc]
               then next .

            lib-table = codsoc-trait[nsoc] + " " + proglib.clef + " " +
                        entry ( a , proglib.libelle[1] ).
            run aff-trt.

            for each magasi where magasi.codsoc     = ""
                            and   magasi.societe    = entry( a, proglib.libelle[1] )
                            and   magasi.equipe     = "1"
                            and   magasi.nombre[30] = 0
                            use-index magasi-1 no-lock :

                zone = "190" + magasi.codtab + "    " + "01" + " " + "   " +
                       string ( zone-ident , "x(47)" ) .

                zone = caps(zone).
                put stream kan-pro-v2 unformatted zone skip .

                do i = 3 to 16 :

                    zone = "190" + magasi.codtab + "    " + "01" + " " + "   " +
                           string ( proglib.libelle[i] , "x(47)" ) .

                    zone = caps(zone).
                    put stream kan-pro-v2 unformatted zone skip .

                end .

          end .

      end .

  end .

  if proglib.libelle[2] <> ""
  then do :

      do a = 1 to num-entries(proglib.libelle[2]) :

          lib-table = codsoc-trait[nsoc] + " " + proglib.clef + " " +
                      entry ( a , proglib.libelle[2] ).
          run aff-trt.

          for each magasi where magasi.codsoc     = ""
                          and   magasi.codtab     = entry( a, proglib.libelle[2] )
                          and   magasi.societe    = codsoc-trait[nsoc]
                          and   magasi.equipe     = "1"
                          and   magasi.nombre[30] = 0
                          use-index magasi-1 no-lock :

              zone = "190" + magasi.codtab + "    " + "01" + " " + "   " +
                     string ( zone-ident , "x(47)" ) .

              zone = caps(zone).
              put stream kan-pro-v2 unformatted zone skip .

              do i = 3 to 16 :

                  zone = "190" + magasi.codtab + "    " + "01" + " " + "   " +
                         string ( proglib.libelle[i] , "x(47)" ) .

                  zone = caps(zone).
                  put stream kan-pro-v2 unformatted zone skip .

              end .

          end .

      end .

  end .

