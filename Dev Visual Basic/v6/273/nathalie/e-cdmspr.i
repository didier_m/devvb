/*==========================================================================*/
/*                           E - C D M S P R . I                            */
/* Edition des Commandes Plateformes                                        */
/*--------------------------------------------------------------------------*/
/* Definition variables et frames                                           */
/*==========================================================================*/

{ e-ediinc.i {1} }

def {1} shared var libelle       as char                 extent 80    no-undo .
def {1} shared var valeur        as char                 extent 60    no-undo .
def {1} shared var date-mini     as date format "99/99/9999"          no-undo .
def {1} shared var date-maxi     as date format "99/99/9999"          no-undo .
def {1} shared var nom-edition   as char                              no-undo .
def {1} shared var periph        as char format "x(12)"               no-undo .
def {1} shared var sav-periph    as char format "x(12)"               no-undo .
def {1} shared var platef        as char format "x"                   no-undo .
def {1} shared var env-plat      as char format "x"                   no-undo .
def {1} shared var type-bon      as char format "x"                   no-undo .
def {1} shared var typaux-plat   as char                              no-undo .
def {1} shared var codaux-plat   as char                              no-undo .
def {1} shared var top-recap     as char format "x"                   no-undo .
def {1} shared var lib-platef    as char format "x(32)"               no-undo .
def {1} shared var lib-env-plat  as char format "x(15)"               no-undo .
def {1} shared var lib-typbon    as char format "x(20)"               no-undo .
def {1} shared var lib-recap     as char format "x(15)"               no-undo .
def {1} shared var edit-recap    as log                               no-undo .
def {1} shared var page-recap    as int                               no-undo .
def {1} shared var fictri-sequence as char                            no-undo .

form libelle[11] format "x(26)"  date-mini             skip
     libelle[12] format "x(26)"  date-maxi             skip
     libelle[13] format "x(26)"  platef lib-platef     skip
     libelle[14] format "x(26)"  env-plat lib-env-plat skip
     libelle[15] format "x(26)"  type-bon lib-typbon   skip
     libelle[16] format "x(26)"  top-recap lib-recap   skip
     libelle[17] format "x(26)"  periph                skip
     with frame fr-saisie
     row 3 centered overlay  no-label { v6frame.i } .
