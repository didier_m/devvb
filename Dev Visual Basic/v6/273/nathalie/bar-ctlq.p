/*==========================================================================*/
/*                          B A R - C T L Q . P                             */
/*       GESTION des BAREMES       : EAT DES MTS REMISES > MT MINI          */
/*       Gestion de l'ecran                                                 */
/*==========================================================================*/
 
{ connect.i }
{ aide.i    }
{ natabsoc.i  }
{ compo.f "new" }
{ chx-opt.f }                         /* zones du module acceptation      */
 
{ bar-ctl0.i }
 
/* chargement societe commune */
regs-app = "nathalie" .
{ regs-cha.i }
 
/* Definition des variables */
def var i   as int                                          no-undo .
def var j   as int                                          no-undo .
 
 
/* Affichage des frames */
display libelle[ 11 ]  date-exe-d date-exe-f
        libelle[ 12 ]  mtsup
        libelle[ 13 ]  maquette  lib-maq
        libelle[ 14 ]  nom-spool with frame fr-saisie .
 
assign compo-vecteur = "<01>,1,2,3,4"
       compo-bloque  = no .
 
/*========== DEBUT du Traitement =========================================*/
SAISIE-GENERALE :
REPEAT :
 
    SAISIE-1 :
    REPEAT :
 
        { compo.i "-1" }
 
        /*  1 : Top mt sup ou inf      */
        REPEAT WHILE COMPO-MOTCLE = 1  WITH FRAME FR-SAISIE :
 
            { fncompo.i  mtsup     fr-saisie  mtsup     }
            if keyfunction( lastkey ) = "end-error" then leave saisie-generale .
 
            leave .
        END .  /*  REPEAT : 1  */
 
        /*  2 : Maquette d' Edition    */
        SAI-MAQ:
        REPEAT WHILE COMPO-MOTCLE = 2 WITH FRAME FR-SAISIE :
 
            libel = libelle[ 80 ] .
            display trim( substring( libel, 19, 30 ) ) @ lib-maq .
            maquette = trim( substring( libel, 1, 12 ) ) .
            { fncompo.i  maquette  fr-saisie  maquette }
 
            if keyfunction( lastkey ) = "end-error" then leave saisie-generale .
 
            if keyfunction( lastkey ) <> "find"
            then do :
                maquette = trim( maquette ) .
                if length( maquette ) < 7 or length( maquette ) > 12
                then do :
                    message  maquette  libelle[ 54 ] . bell . bell .
                    readkey pause 2 .
                    undo , retry.
                end .
                do i = 80 to 89 :
                    libel = libelle[ i ] .
                    if maquette = substring( libel, 1, 12 )
                    then do :
                        display trim( substring( libel, 19, 30 ) ) @ lib-maq .
                        leave sai-maq .
                    end .
                end .
                message  maquette libelle[ 55 ] . bell . bell .
                readkey pause 2 .
                undo , retry.
                leave .
            end . /* <> de find */
 
            /*find*/
            assign libel =  ""
                   j = 0 .
            do i = 80 to 89 :
               if libelle[ i ] <> "" then
                  assign libel = libel + "," + libelle[ i ]
                         j = j + 1 .
            end .
            if j <> 10 then
            do j = j to 10 :
               libel = libel + ", " .
            end .
            substring( libel, 1, 1 ) = "" .
 
            assign chx-def     =  1
                   chx-delete  =  99
                   chx-milieu  =  14
                   chx-color   =  "normal"
                   z-choix     =  libelle[ 6 ] .
 
            { chx-opt.i  "10"  "libel"  "9"  "50"  "52"
                         "with title z-choix"
                         "ecr-maqu"  "color message" }
 
 
            if keyfunction( lastkey ) <> "return" then undo , retry .
 
            maquette = trim( substring( libelle[ chx-trait + 79 ], 1, 12 ) ) .
            display maquette .
            do i = 80 to 89 :
                libel = libelle[ i ] .
                if maquette = substring( libel, 1, 12 )
                then do :
                    display trim( substring( libel, 19, 30 ) ) @ lib-maq .
                    leave sai-maq .
                end .
            end .
            message  maquette  libelle[ 55 ] . bell . bell .
            readkey pause 2 .
            undo , retry .
 
 
        END .  /*  REPEAT : 2  */
 
        /*  3 : Fichier d' Edition    */
        REPEAT WHILE COMPO-MOTCLE = 3 WITH FRAME FR-SAISIE :
 
            { fncompo.i  nom-spool  fr-saisie  spool }
 
            if keyfunction( lastkey ) = "end-error" then leave saisie-generale .
 
            nom-spool = trim( nom-spool ) .
            if length(nom-spool  ) < 7 or length( nom-spool ) > 12
            then do :
                message nom-spool  libelle[ 53 ] . bell . bell .
                readkey pause 2 .
                undo , retry.
            end .
            leave .
 
        END .  /*  REPEAT : 3  */
 
 
 
    END .     /*   SAISIE-1  */
 
    leave .
 
END .     /*   SAISIE-GENERALE  */  
 
 
 