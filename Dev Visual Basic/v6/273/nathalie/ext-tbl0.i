/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/ext-tbl0.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 20163 18-08-10!Evo!cch            !Ajout des Bons de chargement - Modif. faite par EUREA                 !
!V52 15687 13-02-07!Evo!cch            !Ajout extraction des LIV EUREA DISTRI                                 !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - T B L 0 . I                            */
/* Extraction BL pour le logiciel TRAJECTOR                                 */
/*--------------------------------------------------------------------------*/
/* Variables et Frames                                        CC 17/06/1997 */
/*==========================================================================*/

/* Variables */
def var ind         as int                      no-undo.
def var libelle     as char extent 30           no-undo.
def var type-trt    as int  format "9"          no-undo.
def var magasins    as char format "x(75)"      no-undo.
def var no-camp     as char format "9999"       no-undo.
def var reenvoi     as log  format "1/0"        no-undo.
def var datmin      as date format "99/99/9999" no-undo.
def var datmax      as date format "99/99/9999" no-undo.
def var lib-trt     as char format "x(20)"      no-undo.
def var lib-reenvoi as char format "x(20)"      no-undo.
def var mag-exclus  as char                     no-undo.
def var magasi-soc  as char                     no-undo.

/* Dessin des Frames */
def {1} shared frame fr-saisie.

Form libelle[01] format "x(18)" type-trt lib-trt    skip
     libelle[02] format "x(18)"                     skip 
     magasins                                       skip
     libelle[03] format "x(18)" no-camp             skip
     libelle[04] format "x(18)" reenvoi lib-reenvoi skip
     libelle[05] format "x(18)" datmin              skip
     libelle[06] format "x(18)" datmax              skip
     with frame fr-saisie row 7 centered no-label overlay {v6frame.i}.

