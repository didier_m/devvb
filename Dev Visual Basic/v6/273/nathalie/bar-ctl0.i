/*==========================================================================*/
/*                          B A R - C T L 0 . I                             */
/*       GESTION des BAREMES       : ETAT DES MT REMISES > MT MINI          */
/*       Gestion des variables communes                                     */
/*==========================================================================*/


def {1} shared stream kan-tri .                       /* fichier de travail */
def {1} shared stream maquette.                       /* spool edition*/

def {1} shared var zone          as char                            no-undo .
def {1} shared var edit          as char          extent  30        no-undo .

def {1} shared var auxili-soc    as char                            no-undo .
def {1} shared var libelle       as char                 extent 99  no-undo .

def {1} shared var sequence     as char format "x(5)"               no-undo .
def {1} shared var nom-spool    as char format "x(12)"              no-undo .
def {1} shared var lib-titre    as char                             no-undo .
def {1} shared var sav-periph   as char                             no-undo .

def {1} shared var z-choix      as char                             no-undo .

def {1} shared var fic-tri      as char                             no-undo .


def {1} shared var no-bareme     as char  format  "xxx"             no-undo .
def {1} shared var date-fin    as char                              no-undo .
def {1} shared var no-magasin  as char   format "xxx"               no-undo .
def {1} shared var lib-magasin as char   format "x(30)"             no-undo .
def {1} shared var lib-entete  as char                              no-undo .
def {1} shared var lib-tit     as char                              no-undo .

/* variables de la frame */
def {1} shared var date-exe-d  as date    format "99/99/9999"        no-undo .
def {1} shared var date-exe-f  as date    format "99/99/9999"        no-undo .
def {1} shared var mtsup       as logical format "1/0" initial "1"   no-undo .
def {1} shared var maquette    as char    format "x(12)"             no-undo .
def {1} shared var lib-maq     as char    format "x(30)"             no-undo .

/*------------------------------------------------------------------------*/

def {1} shared frame fr-saisie .

form " " skip
     libelle[ 11 ] format "x(28)"  date-exe-d  " - " date-exe-f       skip(1)
     libelle[ 12 ] format "x(28)"  mtsup                              skip
     libelle[ 13 ] format "x(28)"  maquette      lib-maq     skip
     libelle[ 14 ] format "x(28)"  nom-spool
     with frame fr-saisie
     row 3  centered  overlay  no-label  with title libelle[ 10 ] .