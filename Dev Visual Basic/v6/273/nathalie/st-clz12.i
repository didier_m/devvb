/*==========================================================================*/
/*                          S T - C L Z 1 2 . I                             */
/* Statistique Vente Client                                                 */
/* Etat des Qtes Vendues / Zones / Client / Categorie                       */
/*==========================================================================*/
/*           Zones utilise pour le module edition des ruptures              */
/*                Rupture-a               : Niveau de rupture en cours      */
/*                Rupture-nb              : Nombre de rupture existante     */
/*                Rupture-etat = "t"      : Edition du titre de la rupture  */
/*                Rupture-etat = "r"      : Edition de la rupture           */
/*--------------------------------------------------------------------------*/
 
/* Entete SUPER ZONE       */
/*=========================*/
DO WHILE (RUPTURE-ETAT = "T" AND RUPTURE-A = 1 ) :
 
    assign no-super    = substring( zone, 1, 1 )
           nb-zone      = 0 .
 
    leave .
 
END .
 
/* Entete ZONE       */
/*===================*/
DO WHILE (RUPTURE-ETAT = "T" AND RUPTURE-A = 2 ) :
 
    if substring( zone, 1, 3 ) <> " " 
    then do :
        hide message . pause 0 .
        message "  Traitement de la Zone ...: "
                "  " substring( zone, 1, 3 ) .
    end .
 
    assign lib-tit = libelle[31]
           no-zone     = substring( zone, 1, 3 )
           lib-zone    = " "
           nb-cli      = 0 
           ma-nblig    = 0 .
 
    find tabges where tabges.codsoc = "  "     and
                      tabges.etabli = "   "    and
                      tabges.typtab = "CLI"    and
                      tabges.prefix = "SECGEO" and
                      tabges.codtab = no-zone
                      no-lock  no-error .
    if available tabges then lib-zone = tabges.libel1[mem-langue] .
 
    leave .
 
END .
 
 
/*    Entete CLIENT                  */
/*===================================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 3 :
 
    assign    no-client   = stacli.codaux
              lib-client  = " "
              nb-fam      = 0 .
 
    regs-fileacc = "AUXILI" + stacli.typaux.
    {regs-rec.i}
    find auxili where auxili.codsoc = regs-soc
                  and auxili.typaux = stacli.typaux
                  and auxili.codaux = stacli.codaux
                  no-lock no-error .
    if not available auxili then lib-client = "INEXISTANT".
    else lib-client = substring(auxili.adres[ 1 ], 1, 21) .
 
    no-client = trim(no-client) .
    { maq-maj.i "04" "01" "no-client"  }
    { maq-maj.i "04" "02" "lib-client" }
 
    leave .
 
END .
 
 
/*    Entete Categorie  */
/*======================*/
DO WHILE RUPTURE-ETAT = "T" AND RUPTURE-A = 4 :
 
    assign no-catego  = substring( zone, 32, 6 )
           lib-catego = " " .
 
    FIND TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "ART"
                and   tabges.prefix = "SOUFAM" + substring( zone, 32, 3)
                and   tabges.codtab = substring( zone, 35, 3)
                no-lock  no-error .
    if available tabges then lib-catego = tabges.libel1[mem-langue] .
 
    leave .
END .
 
 
/*=========================================================================*/
 
/*    Total Recid      */
/*=====================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 5 :
 
    /* CUMUL des QUANTITE suivant date      */
    /*--------------------------------------*/
    am-sta = int( stacli.annee ) * 100 .
 
    do i = 1 to 12 :
        if am-sta + i >= am-deb and am-sta + i <= am-fin
        then assign tot-qte[ i ]  = tot-qte[ i ]  + stacli.qte[ i ]
                    tot-qte[ 13 ] = tot-qte[ 13 ] + stacli.qte[ i ] .
    end .
 
    leave .
 
END .
 
/*    Total Categorie    Articles   */
/*==================================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 4 :
 
   do while tot-qte[ 13 ] <> 0 :
       assign ind-tot = 000
              nb-fam = nb-fam + 1 .
       run st-clz1t.p .
       leave .
   end .
 
   leave .
END .
 
 
/*    Total  Client             */
/*==============================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 3 :
 
   do while nb-fam <> 0 :
       assign lib-tot = libelle[ 41 ]  + no-client
              ind-tot = 100
              nb-cli  = nb-cli + 1.
 
       run st-clz1t.p .
       leave .
   end .
 
   leave .
END .
 
 
/*    Total ZONE    */
/*==================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 2 :
 
   do while nb-cli <> 0 :
       assign lib-tot  = libelle[ 42 ]  + no-zone
              ind-tot  = 200
              nb-zone  = nb-zone + 1 .
 
       run st-clz1t.p .
       leave .
   end .
 
   leave .
END .
 
/*    Total SUPER ZONE    */
/*========================*/
DO WHILE RUPTURE-ETAT = "R" AND RUPTURE-A = 1 :
 
   do while nb-zone <> 0 :
       assign no-zone  = " "
                   lib-zone = " "
              lib-tit  = " "
              lib-tot  = libelle[ 44 ]  + no-super
              ind-tot  = 300
              nb-super  = nb-super + 1 .
 
       run st-clz1t.p .
       leave .
   end .
 
   leave .
END .