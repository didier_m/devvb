/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/ctlmargp.i                                                                     !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des spécifiques                                     !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End                                                                                                          
**************************************************************************************************************/

PROCEDURE ctlmargproc:

def var l-an           as char.
def var wan            as char.
def var aa             as int.
def var wnomenc        as char. 
def var nbl            as int.
def var topvid         as int.

def var weven1         as char.
def var i-soc as int.
def var wcaf0 as dec decimals 2 extent 13.
def var wcaf1 as dec decimals 2 extent 13.
def var wqte0 as dec decimals 2 extent 13.
def var wqte1 as dec decimals 2 extent 13.
def var wpam0 as dec decimals 3 extent 13.
def var wpam1 as dec decimals 3 extent 13.
def var wmar0 as dec decimals 2 extent 13.
def var wmar1 as dec decimals 2 extent 13.
def var mm as int.
def var i  as int.
def var taux as dec  extent 2 no-undo .     
def var statut-taux as char format "x".
def var wan-n1 as char format "xxxx".
def var wan-np as char format "xxxx".

def var l-fam as char.
def var i-fam as int.

wan-n1 = string(int(wan-n0) - 1, "9999").
wan-np = string(int(wan-n0) + 1, "9999").

if wan-n0 >= "2011" then weven1 = "STOARR".
                    else weven1 = "PXACHA".    

output stream maquette to value(sedifil1).

output stream file2 to value(sedifil2).

/* initialisation des familles a traiter */
if wfam <> "" then l-fam = wfam.
else do:
	l-fam = "".
	for each tabges
	where tabges.codsoc = ""
	and   tabges.etabli = ""
	and   tabges.typtab = "ART"
	and   tabges.prefix = "FAMART"
	no-lock :
		{ select-z.i "famart"     "tabges.codtab" }
		run select-t.p.
		if selection-ok <> ""  then  next .
		if  l-fam = ""  then l-fam = trim ( tabges.codtab ).
						else l-fam = l-fam + ","  + trim ( tabges.codtab ).
	end.
end.
assign l-soc = wsoc.

export stream maquette delimiter ";"
	"Controle des marges pour les soc:" l-soc "exercice " wan-n0 "familles analysees:" l-fam 
	skip (1) .
	

export stream maquette delimiter ";"
"Soc" "Nomenc" "Code" "Libelle" "Poids" "Uv"
"Qte_07" "Qte_08" "Qte_09" "Qte_10" "Qte_11" "Qte_12"
"Qte_01" "Qte_02" "Qte_03" "Qte_04" "Qte_05" "Qte_06"
"Caf_07" "Caf_08" "Caf_09" "Caf_10" "Caf_11" "Caf_12"
"Caf_01" "Caf_02" "Caf_03" "Caf_04" "Caf_05" "Caf_06"
"Tot_Qte"
"Tot_Caf"
"Pam_07" "Pam_08" "Pam_09" "Pam_10" "Pam_11" "Pam_12"
"Pam_01" "Pam_02" "Pam_03" "Pam_04" "Pam_05" "Pam_06"
"Mar_07" "Mar_08" "Mar_09" "Mar_10" "Mar_11" "Mar_12"
"Mar_01" "Mar_02" "Mar_03" "Mar_04" "Mar_05" "Mar_06"
"Tot_Mar"
.

message "debut du traitement le :" string (today, "99/99/9999") " a " string (time, "hh:mm:ss").
message " ".


do i-soc = 1 to num-entries ( l-soc):

	message "debut societe :" wsoc " a " string (time, "hh:mm:ss").

	wsoc = entry ( i-soc, l-soc).
	/* lecture des  articles */
	nbl = 0.
	do i-fam = 1 to num-entries (l-fam):
	    wfam = entry (i-fam, l-fam).
		for each artapr 
		where artapr.codsoc = wsoc
		/* and   (if wfam <> "" then artapr.famart = wfam else wfam = wfam) */
		and   artapr.famart = wfam
	
		no-lock use-index artapr-4:

	
			assign  nbl = nbl + 1
					wcaf0 = 0 wcaf1 = 0 wqte0 = 0 wqte1 = 0 wpam0 = 0
					wpam1 = 0 wmar0 = 0 wmar1 = 0 statut-taux = "" taux = 0.

			/* acces sous famille pour recup statut tx marge */
			FIND TABGES 
			where tabges.codsoc = ""
			and   tabges.etabli = ""
			and   tabges.typtab = "ART"
			and   tabges.prefix = "SOUFAM" + artapr.famart
			and   tabges.codtab = artapr.soufam
			no-lock no-error .

			if available tabges then do : 
				statut-taux = substr( tabges.libel3[ 2 ], 1, 1 ) .
				do i = 1 to 2 :
					if i = 1 then taux[ i ] = tabges.nombre[ 10 ] .
					if i = 2 then taux[ i ] = tabges.nombre[ 2 ] .
				end .
			end .

			/* Acces Exercice en cours */

			l-an = wan-n0 + "," + wan-np.      /* 2011 et 2012 */ 
			do aa = 1 to num-entries( l-an ) :
				wan = entry( aa, l-an ) .
				for each starti 
				where  starti.codsoc = artapr.codsoc 
				and    starti.motcle = "VTE" 
				and    starti.articl = artapr.articl 
				and    starti.annee  = wan
				no-lock :
					find magasi 
					where magasi.codsoc = "" 
					and   magasi.codtab = starti.magasin
					no-lock no-error.
					if not available magasi or  magasi.societe <> artapr.codsoc 
					then do:
						put stream file2 "MAGASIN et SOCIETE incompatible " 
							starti.codsoc " " starti.articl " " starti.magasin " " starti.annee
							skip starti.fac-ht skip starti.qte
						skip.
					end.  

					if aa = 1 then do:        
						do mm = 7 to 12:        /* mois 7 a 12 */
							wcaf0[mm] = wcaf0[mm] + starti.fac-ht [mm].
							wqte0[mm] = wqte0[mm] + starti.qte [mm].
							wcaf0[13] = wcaf0[13] + starti.fac-ht [mm].
							wqte0[13] = wqte0[13] + starti.qte [mm].
						end.        
					end.
					else do:
						do mm = 1 to 6:      /* mois 1 a 6 */
							wcaf0[mm] = wcaf0[mm] + starti.fac-ht [mm].
							wqte0[mm] = wqte0[mm] + starti.qte [mm].
							wcaf0[13] = wcaf0[13] + starti.fac-ht [mm].
							wqte0[13] = wqte0[13] + starti.qte [mm].
						end.        
					end.
	
				end. /* for each starti */ 
			end. /* do aa ..... */

			/* Acces PAMP Exercice en cours */

			l-an = wan-n0 + "," + wan-np.      /* 2011 et 2012 */ 
			do aa = 1 to num-entries( l-an ) :
				wan = entry( aa, l-an ) .
	
				/* Pamp exercice N */ 
				FIND stocks 
				where stocks.codsoc    = artapr.codsoc
				and   stocks.magasin   = ""
				and   stocks.articl    = artapr.articl
				and   stocks.exercice  = wan
				and   stocks.evenement = "STOARR"
				no-lock  no-error .
				if available stocks  then do:
					if aa = 1 then do:        
						do mm = 7 to 12: 
							wpam0[mm] = stocks.prix[mm].
						end.
					end.
					else do:
						do mm = 1 to 6: 
							wpam0[mm] = stocks.prix[mm].
						end.
					end.   
				end. 

			end.   /* do aa = 1 */

			do mm = 1 to 12:
				if statut-taux <> "1" then 
					wmar0 [mm] = wcaf0[mm] - (wqte0[mm] * wpam0[mm]).
				else 
					wmar0 [mm] = ( taux[ 1 ] * wcaf0[mm] ) / 100.

				wmar0 [13] = wmar0[13] + wmar0[mm].
			end.

			topvid = 0.
			do mm = 1 to 12:
				if wqte0[mm] <> 0 then topvid = 1.
				if wcaf0[mm] <> 0 then topvid = 1.
				/* if wqte1[mm] <> 0 then topvid = 1.
				if wcaf1[mm] <> 0 then topvid = 1. */   
			end.	

			if topvid = 1 then do:

				wnomenc = artapr.famart2 + "." + artapr.soufam2 + "." + artapr.sssfam2. 
          
				export stream maquette delimiter ";" 
					artapr.codsoc wnomenc artapr.articl artapr.libart1[1] 
					artapr.pds artapr.usto
					wqte0[7 for 6] 
					wqte0[1 for 6] 
					wcaf0[7 for 6] 
					wcaf0[1 for 6] 
					wqte0[13] 
					wcaf0[13] 
					wpam0[7 for 6]
					wpam0[1 for 6]
					wmar0[7 for 6] 
					wmar0[1 for 6] 
					wmar0[13] .
			end.

		end. /* for each artapr */ 
	end. /* do i-fam .... */

	message "Nombre articles lus -> " nbl .
	message "fin societe :" wsoc " a " string (time, "hh:mm:ss").

end. /* do i-soc .... */

message " ".
message "fin du traitement le :" string (today, "99/99/9999") " a " string (time, "hh:mm:ss").

/*
put stream file2 "Nombre articles lus " nbl skip .
*/

output stream maquette close.
output stream file2 close.
end procedure.