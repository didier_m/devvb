/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/ext-p5fa.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 16404 13-11-07!Evo!cch            !Modification faite par EUREA le 13/11/2007 ( DGR )                    !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E X T - P 5 F A . I                            */
/* Extraction des donnees pour PRODEPOT / TRAJECTOR / OET                   */
/*--------------------------------------------------------------------------*/
/* Extraction Nomenclatures Articles PDP V5                   CC 01/03/1999 */
/* DGR 09112006 : rajout % tolerance commande                               */
/* DGR 13112007 : remise opération sur fam 001 a 010 + 022                  */
/*==========================================================================*/

                                                        /* Pos.  Long. */

zone = "HIERAR  "                                       /* 001 - 008 */
       + "C"                                            /* 009 - 001 */
       + fill(" ", 8)                                   /* 010 - 008 */
       .

case notour :                                           /* 018 - 015 */
    when 1 then zone = zone + string( tabges.codtab, "x(15)" ).
    when 2 then zone = zone + string( substr( tabges.prefix, 7, 3 ) + "," +
                                      tabges.codtab, "x(15)" ).
    when 3 then zone = zone + string( substr( tabges.prefix, 5, 3 ) + "," +
                                      substr( tabges.prefix, 8, 3 ) + "," +
                                      tabges.codtab, "x(15)" ).
end case.

zone = zone
       + string( tabges.libel1[mem-langue] , "x(30)").  /* 033 - 030 */

/* DGR 09 11 2006
case notour :                                           /* 063 - 006 */
    when 1 then zone = zone + "100100".
    otherwise   zone = zone + "000000".
end case.
*/
case notour :                                           /* 063 - 006 */
    when 1 then 
      zone = zone 
           + string(tabges.nombre[3], "999") 
           + "100" .
    otherwise   zone = zone + "000000".
end case.
/* fin dgr 0911 06 */

zone = zone
       + fill(" ", 40)                                  /* 069 - 040 */
       + "H1" + fill (" " , 6)                          /* 109 - 008 */
       + string( notour, "9" )                          /* 117 - 001 */
       + " 0000000"                                     /* 118 - 008 */
       .

/* modif DGR 13/11/07 */
if notour = 1 and 
 lookup (tabges.codtab ,"001,002,003,004,005,006,007,008,009,010,022") = 0
 then
   overlay(zone,77,1,"CHARACTER") = "N".
/* fin modif DGR 13/11/07 */

if tabges.datcre <> ?                                   /* 126 - 008 */
   then zone = zone + string(tabges.datcre, "99999999").
   else zone = zone + fill( " ", 8 ).

zone = zone
       + " ".                                           /* 134 - 001 */

if tabges.datmaj <> ?                                   /* 135 - 008 */
   then zone = zone + string(tabges.datmaj, "99999999").
   else zone = zone + fill( " ", 8 ).