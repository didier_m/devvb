/*==========================================================================*/
/*                           E X T - A G I L G E N  . I                     */
/* Extraction des donnees pour AGIL                                         */
/*--------------------------------------------------------------------------*/
/* Extraction codes barre                                                   */
/* VLE 18042011  Gestion de la section code_barre_delete                    */   
/*==========================================================================*/

/*--------------------------------------------------*/                           
/*  Ecriture du d�tail de section Code Barre Delete */
/*--------------------------------------------------*/ 

if Nom_Section_Deb = "CODE_BARRE_DELETE" then do :     
	CD_BARRE = dupsui.cle-1.
	put stream agil unformatted
                      Type_Ligne_Det "~011" CD_BARRE "~015" skip. 
end.
else do :
					  
/*-------------------------------------------*/                           
/*  Ecriture du d�tail de section Code Barre */
/*-------------------------------------------*/ 

	ID_ARTICLE = caps(trim(codbar.articl)).           
	CD_BARRE = codbar.code-barre.
	put stream agil unformatted
                      Type_Ligne_Det ID_ARTICLE "~011" CD_BARRE "~015" skip. 
end.