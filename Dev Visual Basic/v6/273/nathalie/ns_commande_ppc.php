<?php
/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! Deal/nathalie/php/ns_commande_ppc.php                                                       !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 23052 28-02-11!Evo!dmz            !retouche pour rapidit�                                                !
!V61 21221 25-10-10!Evo!dmz            !filtrage du f1 par rapport au code operateur                          !
!V61 20881 04-10-10!Evo!dmz            !bloquer le code magasin                                               !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
#-----------------------------------------------------------------------------------------------
#   S�curit� : v�rification que l'utilisateur a respect� toutes les �tapes de connexion
//V�rif de l'existence de la session
$locatError="http://".$HTTP_HOST."/errors.php";
if ( $PHPSESSID == "" )
{
    header("Location: ".$locatError);
    exit;
}
else
{
    //d�marrage de la session en cours
    session_start();

    // v�rification de l'acc�s (pr�sence du fichier de l'op�rateur dans admis)
    $FileIp = $admisPath . session_id() . "." . $ope ;
    if (!file_exists($FileIp))
    {
        session_destroy();
        header("Location: ".$locatError);
        exit;
    }
}
#fin de la s�curit�
#-----------------------------------------------------------------------------------------------

//appel de la bibliotheque outil
include($dealCommunPath);
$ses=session_name()."=".session_id();

//appel des classes utilis�es dans cette page
include(deal_commun::classesPath("do-classes_search.php"));
include(deal_commun::classesPath("do-classes_lect_bin.php"));
include(deal_commun::classesPath("do-classes_exchange.php"));

//test si on se trouve en WEBSERVICE
if ( $SystemExchange == 1 )
{
	//appel de la biblioth�que NUSOAP
	//include('nusoap.php');	
	//acces au fichier xml de param�trage du wstk
	include ( search::searchExe("dealgate_nusoap.php",0)  ) ; //$client = new soapclient( $wsdlPath ,'wsdl');
}//on se trouve en WEBSERVICE

require($screenDesignPath);

//initialisation � 0 de l'�tat de la visu
$n_etat_visu = 0 ;

//test zone charge pour voir si on se trouve sur une ligne de menu de visualisation
if ( strtoupper ( $_action ) == "VISU" )
{
    $n_etat_visu = 1 ;
	$n_gloEtatVisu = 1 ;
}	
	
//initialisation des variables globales PHP
$n_devise           = "EUR" ;
$n_typart		    = $_typart ;
$n_adrlivdef		= strtoupper ( $_adrlivdef ) ;
$n_typauxPrv		= strtoupper ( $_typauxPrv ) ;

$datJour = date("d/m/Y");

//Pour echange PHP
$dogExchange = search::searchExe("do-dogexchange.php",1)."?".session_name()."=".session_id() ;

//initialisation de la fonction � lancer
$fonction_rech ="DOG-FUNCTION~GET-MOTCLE�n-motcle�0�~MOTCLE�" . $_motcle ;
$_DogAction = "GET" ;
$_GetNbRow = 0 ;
//lancement de la fonction de demande
$response = SendExchange (  $fonction_rech , "" ) ;

//recup des valeurs de la r�ponse
$exp_response = explode ( "#�#" , $response ) ;

//test si une erreur est retourn�e
if (strtoupper($exp_response[0]) == "ERREUR")
{
	print "ERREUR : " . $nb_enreg;
	exit;
}

/*------------------------------------------------------------------------------------------------------------------
	mise � jour des variables globales des diff�rents motcle (sup,indqte,nivcom et niveau
------------------------------------------------------------------------------------------------------------------*/	
//r�cup�ration des valeurs retourn�es
$ExpResponse = explode ( "~" , $exp_response[1] ) ;
$ExpEntreeResponse = explode ( "�" , $ExpResponse[1] ) ;

//analyse des mots cl�s retourn�s
for ( $i = 0 ; $i < count ( $ExpEntreeResponse ) ; $i++ )
{
	//r�cup�ration du mot cl� et de sa valeur
	$ExpEntreeI = explode ( "�" , $ExpEntreeResponse[$i] ) ;
	
	//test si le mot cl� contient "MOTCLE-"
	if ( strtoupper ( substr ( $ExpEntreeI[0] , 0 , 7 ) ) == "MOTCLE-" )
	{
		//cr�taion du nom de la variable globales
		$NomVar = "n_" . strtolower ( str_replace ( "-" , "_" , $ExpEntreeI[0] ) ) ;
		
		//affectation de la valeur � la variable globale
		$$NomVar = $ExpEntreeI[1] ;
	}//le mot cl� contient "MOTCLE-"
}//fin d'analyse des mots cl�s
/*---------------------------------------------------------------------------------------------------------------*/	

/*----------------------------------------------------------------------------------------------------------------------
	r�cup�ration param�trage des zones de saisie (hidden, cach� ou obligatoire)
----------------------------------------------------------------------------------------------------------------------*/	

//On recupere le magasin par defaut de l'operateur
//premier operateur base
$opebaseexplose=explode("|",$ope_base);
$opebaseexplose2=explode("*",$opebaseexplose[0]);
$opebase=$opebaseexplose2[1];
$fonction_rech ="DOG_FUNCTION~GET-OPERAT�n-tabges�0�~codsoc�" . $c_codsoc2 . "�OPERAT�" . $opebase;
$context = "�CONTEXT�ID=" . session_id() . "|OPERAT=" . GetOperatValue() . "|LANGUE=" . $lang . "|CODSOC=" . $c_codsoc2 . "|CODETB=" . $c_codetb . "|IP=" . getenv('REMOTE_ADDR') ;
$_DogAction = "GET" ;
$_GetNbRow = 0 ;
//lancement de la fonction de demande
$localreponse = SendExchange (  $fonction_rech . $context , "" ) ;
//Magasin
$temprep=explode ( "�" , strstr($localreponse,"MAGASI�") );
$tempexp=explode( "�" ,$temprep[0] ) ;
$magasaut = $tempexp[1];
if($magasaut=="*") $magasaut="";

/*--------------------------------------------------------------------------------------------------------------------*/

//on se gere ici des lignes de menu en dur car la longueur est limit�e
switch($_menu1){
	case "REI":
		$_motcle="ACC";
		$_typcom="REI";
		$_provenance="L,N,X,5,U,A,C,K,R";
		$_typaux="TIE";
		$_codauxges="999100";
		$_SCREEN="�pu-brut��2�typaux��9�codauxges��9";
		break;
	case "CDF":
		$_motcle="ACC";
		$_typcom="CDF";
		$_provenance="2,M";
		$_typaux="TIE";
		$_codauxges="802190"; //a remettre ici en test
		$_natbon="S";
		$_SCREEN="�pu-brut��2�typaux��9�codauxges��9";
		break;
	case "CDI":
		$_motcle="ACC";
		$_typcom="CDI";
		$_provenance="8,M,6";
		$_typaux="TIE";
		$_qtecde="OUI";
		$_codauxges="802190";
		$_natbon="D";
		$_SCREEN="�pu-brut��2�typaux��9�codauxges��9";
		break;	
	case "RCF":
		$_SCREEN="�reftie��1";
		break;	
}

if($_SCREEN!=""){
	$SCREEN=$_SCREEN;
}
if($_codauxges!=""){
	$fonction_rech ="DOG_FUNCTION~GET-AUXGES�n-auxges�0�~codsoc�" . $c_codsoc . "�TYPAUX�" . $_typaux . "�CODAUXGES�" . $_codauxges;
	$context = "�CONTEXT�ID=" . session_id() . "|OPERAT=" . GetOperatValue() . "|LANGUE=" . $lang . "|CODSOC=" . $c_codsoc2 . "|CODETB=" . $c_codetb . "|IP=" . getenv('REMOTE_ADDR') ;
	$_DogAction = "GET" ;
	$_GetNbRow = 0 ;
	//lancement de la fonction de demande
	$localreponse = SendExchange (  $fonction_rech . $context , "" ) ;
	//Magasin
	$temprep=explode ( "�" , strstr($localreponse,"ADRESSE_1�") );
	$tempexp=explode( "�" ,$temprep[0] ) ;
	$_adresse1 = $tempexp[1];	
	//Magasin
	$temprep=explode ( "�" , strstr($localreponse,"CODAUXGES�") );
	$tempexp=explode( "�" ,$temprep[0] ) ;
	$_codauxges = $tempexp[1];	

}

//$_codauxges = "13";
?>

<link rel="stylesheet" href=<?=search::searchExe("dv-params.css",1)?>>
<script src="<?=search::searchExe("do-dogexchange.js",1)?>"></script> 
<script src="<?=search::searchExe("do-tools_date.js",1)?>"></script> 
<script src="<?=search::searchExe("do-toolsclient.js",1)?>"></script> 
<script src="<?=search::searchExe("no_ecran-consult.js",1)?>"></script>

<SCRIPT LANGUAGE=JAVASCRIPT>
function DisableRightClick() {return false;}

function Retour()
{
    window.returnValue = "" ;
    window.close();
}
//d�finition d'expressions r�guli�res utilis�es ci-dessous
var Paragraph	= new RegExp ( "�" , "gi" ) 
var Pipe	    = new RegExp ( "[|]" , "gi" ) 
var Egal	    = new RegExp ( "=" , "gi" ) 
var Arobase	    = new RegExp ( "@" , "gi" ) 
var SepAroDiese	= new RegExp ( "#@#" , "gi" ) 
var Virgule	    = new RegExp ( "," , "gi" ) 
var Sep2Point	= new RegExp ( "[:]" , "gi" ) 
var Slash	    = new RegExp ( "/" , "gi" ) 
var Soleil	    = new RegExp ( "�" , "gi" ) 
var Diese	    = new RegExp ( "#" , "gi" ) 
var Tild	    = new RegExp ( "~" , "gi" ) 
var Space	    = new RegExp ( " " , "gi" ) 

var LastLig     = 0
var PasLig      = 10
var ModeLigne   = ""
var gloremise   = ""
var codartlie   = ""
var Generique   = "N"
var CptGen      = ""
var CodTVA      = "" 
var CptTVA      = "" 
var Imputa      = "" 
var CoefFac     = "" 
var CoefCde     = "" 
var UniFac      = ""
var UniCde      = ""
var Decima      = "" 
var SuiAna      = "" 
var GesLibel    = "" 
var g_analyt    = ""
var RowidEntete = ""

//Variables pour analytique
var nbrseg	    = 0
var nbrsegEnt   = "<?=$NbSegEnt?>"
var code_ana    = new Array()   
var libel_ana   = new Array()  
var codeAnaLig  = new Array()
var libelAnaLig = new Array()
var label_ana   = new Array()
var RspLigne    = new Array()
var Rsp_ana	    = new Array()        //variable de sauvegarde du Rsp ana
var topstop	= true
//modification de la taille et de l'affichage de la fenetre
window.resizeTo( 330 , 260);
window.moveTo((screen.width-330)/2,(screen.height-260)/2);

//fonction d'�change
function dogexchange (myobj)
{
    inputParameter = ""
    paramExchange  = ""
    nomfct         = ""
    ValRowidBon    = ""
    FlagBon        = ""  
    trigger        = ""
    paramFiltreSelection = ""
    if ( dogexchange.arguments.length == 2 ) {
        if ( dogexchange.arguments[1] != "GET" )
            	ValRowidBon = dogexchange.arguments[1]
        	trigger = "GET"
    }

	//Tester le type de l'objet qui est � l'origine
	if ( myobj.F10 != undefined ) {
		//Affecter l'dientifiant
		strId = myobj.F10 ;
		//Indiquer le trigger
		trigger = "F10" ; 		
		//Tester s'il n'est pas en ReadOnly
		if ( document.all(strId).readOnly == true) return ;
	} else {
		// Ne rien faire si la zone est en lecture seule
		 if ( myobj.readOnly == true ) return ;	
		//Affecter l'identifiant
		strId = myobj.id ;
	}

	//D�terminer le trigger
	if ( trigger == "") {
		switch ( event.keyCode)
		{
			case 13: //Touche entr�e	
				trigger = "GET" 
				break;
            case 120: //touche F9
                trigger = "F9"
                break;
			case 121: //touche F10
				trigger = "F10"
				break;
			case 0: //entr�e par un lien, pas d'�v�nement on passe en GET
				trigger = "GET" 
				break;
		}
	}
	
    paramExchange = "GETVAR|NOSCREEN"
    //analyse des diff�rents mot cl�
    switch ( strId )
    {
        //cas N� de bon
        case "DICT�numbon" :
            //si rowid existant, on part en get sur le rowid
            if ( ValRowidBon != "" )
            {
                paramExchange += "|NODISPLAYERROR|KEEPZONEVALUE|TRUEDMD"
                inputParameter = ValRowidBon
                nomfct         = "GET-BONENT�n-bonent"
                FlagBon        = "modif"
            }
            else
            {
                if ( document.all(strId).value != "" )
                {
                    paramExchange += "|NODISPLAYERROR|KEEPZONEVALUE|TRUEDMD"
                    inputParameter = "CODSOC�<?=$c_codsoc?>�MOTCLE�<?=$_motcle?>�TYPBON�<?=$_typcom?>�NUMBON�" + document.all(strId).value
                    nomfct         = "GET-BONENT�n-bonent"
                    FlagBon        = "modif"
                }
                else
                {
                    paramExchange += "|TRUEDMD"
                    inputParameter = "PARAME�I�MOTCLE�<?=$_motcle?>�TYPBON�<?=$_typcom?>�CODSOC�<?=$c_codsoc?>�OPTION�DEF"
                    nomfct         = "INC-BONENT�n-bonent"
                    FlagBon        = "create" 
                }
            }
            break
        case "DICT�typaux" :
            inputParameter = "TYPAUX�" + document.all(strId).value+"�PARAME�"
            break;
        case "DICT�codauxges" :
            inputParameter = "�MOTCLE�<?=$_motcle?>�TYPAUX�" + document.all("DICT�typaux").value + "�CODAUXGES�" + document.all(strId).value + "�ORIGINE�SAIBON"+"�TYPBON�"+document.all("DICT�typbon").value
            break;
        case "DICT�natlig":
            inputParameter = "MOTCLE�<?=$_motcle?>�CODSOC�<?=$c_codsoc?>�NATLIG�" + document.all(strId).value
            break;
        case "DICT�codart":
	        if(trigger=="GET"){ 
	        	myobj.ctrl_input = "@DICT�codart@DICT�libart_1@||0|DICT�libart_1|";
	        	nomfct = "get-articl�n-articl"
	        } else myobj.ctrl_input = "@DICT�codart@DICT�libart_1@|GENERAL|0|DICT�libart_1|";
	
	        //inputParameter = "CODSOC�<?=$c_codsoc?>�MOTCLE�<?=$_motcle?>�TYPBONOLD�<?=$_typcom?>�NUMBON�"+document.all('DICT�numbon').value+"�TYPART�<?=$n_typart?>�TYPAUX�" + document.all('DICT�typaux').value + "�CODAUXGES�" + document.all('DICT�codauxges').value +  "�ANALYT�" + document.all('DICT�analyt').value +
	        //"�DEVISE�" + document.all('DICT�devise').value + "�DATTAR�" + document.all('DICT�datcde').value + "�TYPBON�<?=$_typcom?>�CODART�"+document.all(strId).value+"�MAGASI�"+document.all('DICT�magasi').value
	        inputParameter = "CODSOC�<?=$c_codsoc?>�MOTCLE�<?=$_motcle?>"+"�CODART�"+document.all(strId).value + "�TYPAUX�" + document.all('DICT�typaux').value + "�CODAUXGES�" + document.all('DICT�codauxges').value +"�MAGASI�"+document.all('DICT�magasi').value+"�CODTAR�" + document.all('DICT�codtar').value

        	paramExchange += "GETVAR|TRUEDMD|NOSCREEN"
            break
	case "DICT�typregul" :
		paramExchange = "GETVAR|NOSCREEN|TRUEDMD"
		if(trigger=="F10"){
			paramFiltreSelection="FIELD=codtab|libel1-1"
			nomfct="full-tabges�n-tabges";
			inputParameter = "�CODSOC��TYPTAB�NAT�PREFIX�TREGUL"			
		} else {
			inputParameter = "�CODSOC��TYPTAB�NAT�PREFIX�TREGUL�CODTAB�" + document.all(strId).value.trim();
			nomfct="full-tabges�n-tabges";			
		}
		break;

    }

    // Cette fonction est charg�e d'ex�cuter les trigger d'�change DOG		
    switch ( trigger)
    {
        case "GET": //Touche entr�e  
            //Faire le Get
            retourExchange  = sendExchange ( "<?=$dogExchange?>" , strId , inputParameter , "get" , "" , paramExchange ,"", nomfct ) 

            //Tester le retour et quitter s'il ya une erreur
            if ( strId == "DICT�numbon" )
            {
                if (retourExchange != "OK")
                {
                    //on vide le libell� et le suret associ�s au code
                    document.all("DICT�adresse_1").value = "" 
                    document.all("DICT�typaux").value = "" 
                    document.all("DICT�codauxges").value = "" 
                    document.all("DICT�bon-ht").value = "" 
                    document.all("DICT�devise").value = "" 
                    // Ce bons n'existe pas.
                    alert ( "<?PHP print lect_bin::getLabel ( "mess-cdeent_9" , "sl"); ?>" )
                                            //on vide le numbon et on lui remet le focus
                                            document.all("DICT�numbon").value="" 
                                            document.all("DICT�numbon").focus()
                                            window.scrollTo(0,0)
                                            posi = 1
                                            return
                }
                document.all("mode_bon").value = FlagBon
            }
            else
                if (retourExchange != "OK") return 
            break 
        case "F10": //Touche F10
            switch ( strId )
            {				
                case "DICT�codauxges" : case "DICT�codauxgesimg" : 
			dogFonction="F10-auxges�n-auxges";
			input_param ="�MOTCLE�<?=$_motcle?>�CODSOC�<?=$c_codsoc?>�TYPAUX�"+document.all("DICT�typaux").value.trim()+"�LIBABR�"+document.all("DICT�codauxges").value+"�NB-LIGNE�30"+"�TYPBON�"+document.all("DICT�typbon").value;
			retourExchange = sendExchange ( "<?=$dogExchange?>" ,    strId  , input_param , "f10" , "" , paramExchange ,"FIELD=ADRESSE_1|ADRESSE_5|CODAUXGES",dogFonction) ;
			if (retourExchange != "OK") {
				initbufferscreen();
				document.all(strId).focus();
				return ;
			 }
	                break;
                case "DICT�codart" : case "DICT�codartimg" :
			if(document.all("DICT�codart").value.trim().length<3){
				alert("Pas assez de caract�res saisis pour lancer la recherche")
				return;	
			}
			dogFonction="F10-ARTICL�n-articl";
			input_param ="�CODSOC�<?=$c_codsoc?>�LIBABR�"+document.all("DICT�codart").value.trim()+"�NB-LIGNE�20";
			retourExchange = sendExchange ( "<?=$dogExchange?>" ,    strId  , input_param , "f10" , "" , paramExchange ,"FIELD=CODART|LIBABR",dogFonction) ;
			if (retourExchange != "OK") {
				initbufferscreen();
				document.all(strId).focus();
				return ;
			 }
	                break;
                default : 
                     retourExchange = sendExchange ( "<?=$dogExchange?>" , strId , inputParameter , "f10" , "" , paramExchange,paramFiltreSelection, nomfct )
            }
            if (retourExchange != "OK") return    
            break			
    } //Selon le trigger a ex�cuter
    //analyse des diff�rents mot cl� pour maj suite retour fct
    switch ( strId )
    {
        //cas N� de bon
        case "DICT�numbon" :
            ExRsp1 = Rsp[1]
            if ( FlagBon == "modif" )
            {
                RowidEntete = getValueFromDataArray( getDataArrayFromRow(1) ,"ROWID" , false , "" ) 

                //param�tres d'�change
                paramExchange = "GETVAR|NOSCREEN|TRUEDMD" 

                //fonction � lancer
                nomFct = "CTRL-ACCES�n-autori"

                //lancement de la fonction d'autorisation
                //retourAutori = sendExchange ( "<?=$dogExchange?>" , "" , "ROWID�" + RowidEntete , "GET" , "" , paramExchange , "" , nomFct )

                //test si r�ponse incorrect pour sortir de la fonction
                //if ( retourAutori != "OK" ) return

                //r�cup�ration des droits d'acc�s � la pi�ce
                GloAutori = ""//getValueFromDataArray( getDataArrayFromRow(1) ,"ACCES" , false , "" ).toUpperCase()
            }

            Rsp[1] = ExRsp1
	    //mise � jour du N� de bon 
            setValueFromKeyword ( "numbon" , false , "" , "DICT�numbon" , true )
            if ( FlagBon == "modif" ) {
		    //on verifie que l'on est bien sur le meme magasin
		    if(document.all("DICT�magasi").value.trim()!=""){
			if(setValueFromKeyword("MAGASI",false,"","",true).trim()!=document.all("DICT�magasi").value.trim()&&document.all("DICT�typbon").value.trim()!="RAF"){
				alert("Ce bon n'appartient pas au magasin en cours")
				return;
			}
		    }	
		    document.all("DICT�bon-ht").value=setValueFromKeyword ( "bon-ht" , false , "" , "" , true ).trim()
		    setValueFromKeyword ( "devise" , false , "" , "DICT�devise" , true )
	            setValueFromKeyword ( "typaux" , false , "" , "DICT�typaux" , true )
	            setValueFromKeyword ( "codauxges" , false , "" , "DICT�codauxges" , true )
	            setValueFromKeyword ( "adresse_1" , false , "" , "DICT�adresse_1" , true )
	            setValueFromKeyword ( "adresse_2" , false , "" , "DICT�adresse_2" , true )
	            setValueFromKeyword ( "adresse_3" , false , "" , "DICT�adresse_3" , true )
	            setValueFromKeyword ( "adresse_4" , false , "" , "DICT�adresse_4" , true )
	            setValueFromKeyword ( "adresse_5" , false , "" , "DICT�adresse_5" , true )
	            setValueFromKeyword ( "reftie" , false , "" , "DICT�reftie" , true )
	            setValueFromKeyword ( "datcde" , false , "" , "DICT�datcde" , true )
	            setValueFromKeyword ( "datliv" , false , "" , "DICT�datdep" , true )
		    setValueFromKeyword ( "env-pla" , false , "" , "DICT�env-pla" , true )
		    if(setValueFromKeyword ( "natbon" , false , "" , "" , true )!="")setValueFromKeyword ( "natbon" , false , "" , "DICT�typregul" , true )
	    }
            ExRsp1 = Rsp[1]
            //r�cup�ration analytique
            charge_ana() 
            Rsp[1] = ExRsp1
            analyt = ""
            for ( i=1 ; i<=nbrseg ; i++ )
            {
                ValSeg    = getValueFromDataArray( getDataArrayFromRow(1) ,"SEGANA_"+i , false , "" )
                ValLibSeg = getValueFromDataArray( getDataArrayFromRow(1) , "LIBEL" , true , "SEGANA_"+i )
                if ( ValLibSeg == "NOMATCH" )
                    ValLibSeg = ""
                if ( ValSeg == "NOMATCH" )
                    ValSeg = ""
                if ( analyt == "" && i == 1 )
                    analyt += ValSeg 
                else
                    analyt += "." + ValSeg
                code_ana[i]     = ValSeg
                libel_ana[i]    = ValLibSeg
                codeAnaLig[i]   = ValSeg    
                libelAnaLig[i]  = ValLibSeg 
            }
            document.all("DICT�analyt").value = analyt
            document.all("DICT�analytl").value = analyt
	    //selon le type de bon
	    switch("<?=$_typcom2?>") {
	    	case "REA": case "RED": case "REI": 
			//pour les reappro on ne passe pas par l'entete
			ClearFrameSaisie ( 'SAITIERS' , true , 'NOCLEAR' )
			//on enregistre l'entete
			if ( FlagBon != "modif" ){
				BonValider();
				return;
			}
	    		break;
	    	default:
		        ClearFrameSaisie ( 'SAITIERS' , false , 'NOCLEAR' )
	    		break;
	    }
	    //le parametrage menu prend le dessus
	    zonscreenget();    
            if ( FlagBon == "modif" ) {
                ModeLigne = "create"
		RspTmp = Rsp
		inputParameter = inputParameter = "�CODSOC�<?=$c_codsoc?>�MOTCLE�"+document.all("DICT�motcle").value+"�TYPBON�"+document.all("DICT�typbon").value+"�NUMBON�"+document.all("DICT�numbon").value
		nomfct         = "GET-BONLIG�n-bonlig"
		retourExchange = sendExchange("<?=$dogExchange?>", "", inputParameter, "get", "", "NODISPLAYERROR|GETVAR|NOSCREEN|TRUEDMD|MESSAGE=Acces lignes...", "", nomfct) 
		if(retourExchange!="OK"){
			LigneAjouter()
			return	
		}
		RspLigne = Rsp;
		maxchrono()
		LigneCharger(undefined,"FIRST")
		Rsp = RspTmp
                //document.frames("RECAP").location.href = "<?=search::searchExe("ns_cdeentlignes_ppc.php",1)?>?<?=session_name()?>=<?=session_id()?>&_motcle=<?=$_motcle?>&_typcom=<?=$_typcom?>&_numbon=" + document.all("DICT�numbon").value
                //ClearFrameSaisie ( 'SAILIGNE' , false , 'NOCLEAR' )
                document.all("DICT�codart").focus()
            } else {
                	nextfocus(document.all("DICT�numbon"))//document.all("DICT�typaux").focus()
            }
            break
        case "DICT�typaux" :
           setValueFromKeyword ( "TYPAUX"  , false , "" , "DICT�typaux"  , true )
           document.all("DICT�codauxges").focus()
           break
	case "DICT�typregul" :
		setValueFromKeyword("CODTAB" , false ,"" ,"DICT�typregul" ,true);
		document.all("DICT�typregul").buffer=document.all("DICT�typregul").value;
		setValueFromKeyword("LIBEL" , false ,"" ,"DICTLINK�typregul�libel" ,true);
		nextfocus(document.all(strId));	
		break;																																					

        case "DICT�codauxges" :
		if(setValueFromKeyword ( "BLOCAGE"  , false , "" , ""  , true)=="O"){
			alert("Tiers non autoris� (bloqu�)")
			return;
		}
		if(setValueFromKeyword ( "CLOTURE"  , false , "" , ""  , true)=="O"){
			alert("Tiers non autoris� (clotur�)")
			return;
		}

		setValueFromKeyword ( "CODAUXGES" , false , "" , "DICT�codauxges" , false)
		setValueFromKeyword ( "ADRESSE_1" , false , "" , "DICT�adresse_1" , true)
		setValueFromKeyword ( "ADRESSE_2" , false , "" , "DICT�adresse_2" , true)
		setValueFromKeyword ( "ADRESSE_3" , false , "" , "DICT�adresse_3" , true)
		setValueFromKeyword ( "ADRESSE_4" , false , "" , "DICT�adresse_4" , true)
		setValueFromKeyword ( "ADRESSE_5" , false , "" , "DICT�adresse_5" , true)
		setValueFromKeyword ( "CODTAR" , false , "" , "DICT�codtar" , true)

		if(document.all("DICT�datdep").style.visibility==""&&document.all("DICT�datdep").parentElement.style.visibility=="") {
			document.all("DICT�datdep").focus()
		}
		if(document.all("DICT�reftie").style.visibility==""&&document.all("DICT�reftie").parentElement.style.visibility=="") {
			try{document.all("DICT�reftie").focus()}catch(ex){}
			//return	
			}
		if(document.all("DICT�datcde").style.visibility==""&&document.all("DICT�datcde").parentElement.style.visibility=="") {
			document.all("DICT�datcde").focus()
			//return	
			}
			break
        case "DICT�natlig":
            setValueFromKeyword ( "NATLIG" , false , "" , "DICT�natlig" , true )
            setValueFromKeyword ( "LIBEL" , false , "" , "DICTLINK�natlig�libel" , true )
            if ( document.all(strId).value == "T" )
            {
                document.all("DICT�codart").readOnly        = true
                document.all("DICT�codart").className       = "dogskininputreadonly"
                document.all("DICT�qte").readOnly           = true
                document.all("DICT�qte").className          = "dogskininputreadonly"
                document.all("DICT�pu-brut").readOnly       = true
                document.all("DICT�pu-brut").className      = "dogskininputreadonly"
                document.all("DICT�analytl").readOnly       = true
                document.all("DICT�analytl").className      = "dogskininputreadonly"
                document.all("DICT�gloremise").readOnly     = true
                document.all("DICT�gloremise").className    = "dogskininputreadonly"
                document.all("DICT�texte-lig").focus()
            }
            else
            {
                document.all("DICT�codart").readOnly        = false
                document.all("DICT�codart").className       = "dogskininput"
                document.all("DICT�qte").readOnly           = false
                document.all("DICT�qte").className          = "dogskininput"
                if('<?=$_typcom?>'!="RGS"){
	                document.all("DICT�pu-brut").readOnly       = false
	                document.all("DICT�pu-brut").className      = "dogskininput"
                }
                document.all("DICT�analytl").readOnly       = false
                document.all("DICT�analytl").className      = "dogskininput"
                document.all("DICT�gloremise").readOnly     = false
                document.all("DICT�gloremise").className    = "dogskininput"
                document.all("DICT�codart").focus()
            }
            break
        case "DICT�codart":
            //on regarde si on a pas deja une ligne avec cet article (differente de la ligne en cours!!)
            //par defaut on est en monoarticle
            if("<?=$_multi?>"!="N" && topstop == true){
	            strcodart=setValueFromKeyword2("CODART",false ,"","",true)
		    setValueFromKeyword2("TYP-SURCOND",false ,"","DICT�typ-surcondit",true)
		    setValueFromKeyword2("SURCONDIT",false ,"","DICT�surcondit",true)
		    if("<?=$_qtecde?>"!=""){
				strQte = setValueFromKeyword2("MINCDE-ART",false,"","",true)
				setValueFromKeyword2("MINCDE-ART",false,"","DICT�mincde-art",true)
				setValueFromKeyword2("PAR-COMB",false,"","DICT�par-comb",true);
		    }
	            RspTmp=Rsp
	            Rsp = new Array()
	            Rsp[0]="OK"
	            //on cherche la ligne
	            tmpRsp=RspLigne.join("|,|").split('�CODART�'+strcodart)
	            //si la ligne est trouv�e
	            if(tmpRsp.length>1){
	            	strchrono=tmpRsp[0].substring(tmpRsp[0].lastIndexOf("CHRONO"))
	            	strchrono=Entry(1,strchrono,"�")
	            	strchrono=Entry(2,strchrono,"�")
            		//trouv�! on pointe dessus
			topstop=false
			LigneCharger(strchrono,"")
            		return;
	            	
	            }
	            Rsp=RspTmp
            }
	    topstop = true
            //on controle les autorisation sur le code article
            //la provenance selon le type de bon
            if(provenance!="") {
            	if(!provenance.match(setValueFromKeyword2( "PROVENANCE", false, "", "", true))){
            		alert("Fili�re non autoris�e");
            		document.all("DICT�codart").focus()
            		return;	
            	}
            }
	    document.all("DICT�stophy2").value=setValueFromKeyword2("STOPHY",false,"","",true).trim()
	    document.all("DICT�quantite_4").value = Entry(1,setValueFromKeyword2("QUANTITE",false,"","",true),"|").trim()+ "/" + Entry(2,setValueFromKeyword2("QUANTITE",false,"","",true),"|").trim() + "/" + Entry(3,setValueFromKeyword2("QUANTITE",false,"","",true),"|").trim()
            //l'autorisation a la vente ou a l'achat
	    strblocage = setValueFromKeyword2("CODE-BLOCAGE",false,"","",true)
	    strstatbloc= setValueFromKeyword2 ( "STATBLOC"  , false , "" , ""  , true)
            if(Entry(3,strstatbloc,"|")=="1"){
            	alert("Article interdit de commande : " + setValueFromKeyword2("CODE-BLOCAGE",false ,"","",true))
            	if(setValueFromKeyword("CODART-REMPL_1",false ,"","",true)!=""){
            		//on propose l'article de substitution
            		alert("Article de substitution propos� : "+setValueFromKeyword2("CODART-REMPL_1",false ,"","",true))
            		document.all("DICT�codart").value   = setValueFromKeyword2("CODART-REMPL_1",false ,"","",true)
            		return;	
            	}
            }
	    //on regarde si l'article n'est pas ferm�
	    if(strblocage=="F"){
	    		alert("Article ferm�"); 
	    		document.all("DICT�codart").focus()
	    		return false;
	    }
	    if(document.all("DICT�motcle").value.substr(0,1)=="V"&&strblocage.toUpperCase()=="B"){
	    		alert("Article interdit en vente"); 
	    		document.all("DICT�codart").focus()
	    		return false;		
	    }
	    if(document.all("DICT�motcle").value.substr(0,1)=="A"&&strblocage.toUpperCase()=="I"){
	    		alert("Article Interdit En Achat"); 
	    		document.all("DICT�codart").focus()
	    		return false;		
	    }
	    if(document.all("DICT�typbon").value.trim()=="CCI"&&strblocage.toUpperCase()=="I"){
	    		alert("Article interdit en CCI"); 
	    		document.all("DICT�codart").focus()
	    		return false;		
	    }
	    //on regarde les autorisations par rapport au statbloc
	    if(strstatbloc!=""){
	    	if(document.all("DICT�motcle").value.substr(0,1)=="V"){
	    		//si on est en vente
	    		 if(Entry(2,strstatbloc,"|").substr(0,1)=="1"&&document.all("DICT�motcle").value=="VTC"){
	    		 	alert("Non autoris� en vente")
	    		 	document.all("DICT�codart").focus()   
	    		 	return false	
	    		 }
	    		//si on est en CCI Eurea
	    		if(document.all("DICT�typbon").value.trim()=="CCI") {
	    			 if(Entry(3,strstatbloc,"|").substr(0,1)=="1"){
	    			 	alert("Non autoris� en CCI")
	    			 	document.all("DICT�codart").focus()   
	    			 	return false
	    			 }				
	    		}
	    	} else {
	    		//si on est en achat
	    		 if(Entry(3,strstatbloc,"|").substr(0,1)=="1"&&document.all("DICT�motcle").value=="ACC"){
	    		 	alert("Non autoris� en achat")
	    		 	document.all("DICT�codart").focus()   
	    		 	return false
	    		 }
	    	}		
	    }

            codartlie = setValueFromKeyword2("CODART-LIE", false, "", "", true )
            document.all("DICT�codart").value   = setValueFromKeyword2("CODART", false, "", "", true )
            document.all("DICT�libart_1").value = setValueFromKeyword2 ("LIBART_1", false, "", "", true )
	    if('<?=$_typcom?>'!="RGS"){
            	if(setValueFromKeyword2("PU-BRUT", false, "", "", true).trim()!="")document.all("DICT�pu-brut").value  = Entry(1,setValueFromKeyword("PU-BRUT", false, "", "" , true ).trim(),"|").trim()
	    } else {
	    	//on cherche le tarif htc
	    	if(Entry(1,setValueFromKeyword2("CODTAR", false, "", "", true).toUpperCase().trim(),"|")=="HTC")
	    		document.all("DICT�pu-brut").value  = Entry(1,setValueFromKeyword("PU-BRUT", false, "", "" , true ).trim(),"|").trim()
	    	else
	    		document.all("DICT�pu-brut").value  = Entry(2,setValueFromKeyword("PU-BRUT", false, "", "" , true ).trim(),"|").trim()
	    }

            var ComArticl 					= setValueFromKeyword2 ("ARTICOM", false, "", "", true)
            var reg_com 					= new RegExp ( "CHR[(]10[)]" , "gi" )
            document.all("DICT�texte-lig").value	 	= ComArticl.replace( reg_com , "\r\n" )

            if(setValueFromKeyword ("GENERIQUE", false, "", "", true ).toUpperCase() == "O" ) Generique = "O"
            CptGen	 = setValueFromKeyword2 ("CPTGEN"  	, false , "", "", true )
            CodTVA	 = setValueFromKeyword2 ("CODTVA"  	, false , "", "", true )
            CptTVA	 = setValueFromKeyword2 ("CPTTVA"  	, false , "", "", true )
            Imputa	 = setValueFromKeyword2 ("IMPUTA"  	, false , "", "", true )
            CoefFac	 = setValueFromKeyword2 ("COEFFAC" 	, false , "", "", true )
            //CoefCde	 = setValueFromKeyword2 ("COEFCDE" 	, false , "", "", true )
            UniFac	 = setValueFromKeyword2 ("UNIFAC" 	, false , "", "", true )
            UniCde	 = setValueFromKeyword2 ("UNICDE" 	, false , "", "", true )
            //Decima	 = setValueFromKeyword2 ("DECIMA" 	, false , "", "", true )
            SuiAna	 = "N"   //getValueFromDataArray( getDataArrayFromRow(1) , "SUIANA" 	, false , "" , true )
            GesLibel = setValueFromKeyword ("GESLIBEL", false, "", "", true )

            //on s'occupe maintenant du prechargement de la quantit�
            strQte="1"
            strSurcondit=setValueFromKeyword2("SURCONDIT",false ,"","DICT�surcondit",true)
	    strTypsurcond=setValueFromKeyword2("TYP-SURCOND",false ,"","DICT�typ-surcondit",true)
	    
	    //on renseigne la decimalisation de l'article
   	    Decima	 = setValueFromKeyword2("DECIMA",false,"","DICT�decima",true);
  	    if(Entry(2,strstatbloc,"|")=="1"){
	    	strB="V:N"
	    } else {
		strB="V:O"
	    }
	    if(Entry(3,strstatbloc,"|")=="1"){
		strB=strB+" A:N"
	    } else {
		strB=strB+" A:O"
	    }
	    document.all("DICT�statbloc").value=strB
	    setValueFromKeyword2("CODE-BLOCAGE",false ,"","DICT�code-blocage",true)
            //on recupere eventuellement l'unit� d'achat
            CoefCde = setValueFromKeyword2("COEFCDE",false ,"","DICT�coefcde",true)
            if(CoefCde!="")strQte=setValueFromKeyword2("COEFCDE",false ,"","",true)
            if(strSurcondit!=""&&CoefCde!="")strQte=parseFloat(CoefCde)*parseFloat(strSurcondit)
	    //on precharge a partir d'un top menu la quantite mini de cde
	    if("<?=$_qtecde?>"!=""){
		if(setValueFromKeyword2("MINCDE-ART",false,"","",true)!=""){
			strQte = setValueFromKeyword2("MINCDE-ART",false,"","",true)
			setValueFromKeyword2("MINCDE-ART",false,"","DICT�mincde-art",true)
			setValueFromKeyword2("PAR-COMB",false,"","DICT�par-comb",true);
		}
	    } else {
		    //on precharge eventuellement l'unite du surconditionnement
	    	    if(setValueFromKeyword2("TYP-SURCOND",false,"","",true)=="F" && document.all("DICT�motcle").value.substr(0,1)=="A")
	    	    	strQte = CoefCde
	    	    else
	    	    	strQte = strSurcondit
	    }
            //on precharge la zone
            if(document.all("DICT�qte").value.trim()=="")document.all("DICT�qte").value=strQte;


            //initialisation du tableau de valeurs
            var TabGloRemise = ""
            var ValGlovalremise = ""

            //boucle de r�cup�ration des valeurs saisies
            for ( r = 1 ; r <= 5 ; r++ )
            {
                //test si le code existe
                if ( setValueFromKeyword2("REM-CODE_" + r, false, "", "", true ) != "" )
                {
                    if ( r != 1 )
                        TabGloRemise += "�"                   

                    ValGlovalremise += setValueFromKeyword2("REM-CODE_" + r, false, "", "", true )

                    //mise � jour de la variable _glovalremise
                    TabGloRemise    += "REM-CODE_" + r + "�" +     setValueFromKeyword2("REM-CODE_" + r  , false , "" , "", true ) +
                                       "�LIBEL�" +                 setValueFromKeyword("LIBEL"          , true  , "REM-CODE_" + r , "", true ) +     
                                       "�REM-TYPAPP_" + r + "�" +  setValueFromKeyword2("REM-TYPAPP_" + r, false , "" , "", true ) +
                                       "�LIBEL�" +                 setValueFromKeyword("LIBEL"          , true  , "REM-TYPAPP_" + r , "", true ) +     
                                       "�REM-VALEUR_" + r + "�" +  setValueFromKeyword2("REM-VALEUR_" + r, false , "" , "", true )
                }

                if ( r != 5 )
                    ValGlovalremise += "."                   
            }//boucle de r�cup�ration des valeurs saisies

            //mise � jour du champ glovalremise
            document.all("DICT�gloremise").value = ValGlovalremise        
            gloremise = TabGloRemise

            //test si on se trouve sur un article de type compos�
            if ( GesLibel == "1" )            {
                //enlever la lecture seule
                document.all("DICT�libart_1").readOnly = false
                //changer le style de la zone
                document.all("DICT�libart_1").className = "dogskininput"
                //mettre le focus sur la zone de libell�
                document.all("DICT�libart_1").focus()
                //quitter la fonction
                return
            } else {
                //enlever la lecture seule
                document.all("DICT�libart_1").readOnly = true
                //changer le style de la zone
                document.all("DICT�libart_1").className = "dogskininputreadonly"
            }

            document.all("DICT�qte").focus()
            break
    }
} //Fin de la fonction dogexchange

function setValueFromKeyword2(strField,aBol,strJoin,zone,aBol2){
	//fonction rapide pour la valeur de la reponse
	expTemp = Rsp[1].split(strField+"�")
	if(expTemp[1]!=undefined){
		expRep = expTemp[1].split("�")
		expTemp = expRep[0].split("�")  
		if(zone!="")document.all(zone).value = expTemp[0]
		return expTemp[0] 
	} else
		return ""
}

function maxchrono() {
	//cette fonction permet de reccuperer le chrono maxi	
	RspTemp = Rsp
	Rsp = new Array()
	Rsp[0] = "OK"
	Rsp[1] = RspLigne[RspLigne.length-1]
	LastLig = parseFloat(setValueFromKeyword2("CHRONO" , false ,"" ,"" ,true))
	if(RspLigne.length==0)LastLig=0
}

//fonction de recherche des bons par F9 (lignes) ou F10 (entetes)
function BonRechercher()
{
	//test si on se trouve sur une recherche F9
	if ( event.keyCode == 120 ) 
		PhpNumbon = "<?=search::searchExe('nr_selectionbon_ppc.php',1)?>?PHPSESSID=<?=$PHPSESSID?>&_com=O&_SCREEN=�evenement�L�0�typbon�" + document.all('DICT�typbon').value + "�motcle�" + document.all('DICT�motcle').value + "�typaux�" + document.all('DICT�typaux').value + "�codauxges�" + document.all('DICT�codauxges').value + "�magasi�" + document.all('DICT�magasi').value + "�datcdemin�" + document.all('DICT�datcde').value
	else  {	
		if(document.all('DICT�motcle').value == "ACC")
			PhpNumbon = "<?=search::searchExe('nr_selectionbon_ppc.php',1)?>?PHPSESSID=<?=$PHPSESSID?>&_com=O&_SCREEN=�evenement�E�0�typbon�" + document.all('DICT�typbon').value + "�motcle�" + document.all('DICT�motcle').value + "�typaux�" + document.all('DICT�typaux').value + "�codauxges�" + document.all('DICT�codauxges').value + "�magasi�" + document.all('DICT�magasi').value + "�datcdemin�" + document.all('DICT�datcde').value
		else
			PhpNumbon = "<?=search::searchExe('nr_selectionbon_ppc.php',1)?>?PHPSESSID=<?=$PHPSESSID?>&_com=O&_SCREEN=�evenement�E�0�typbon�" + document.all('DICT�typbon').value + "�motcle�" + document.all('DICT�motcle').value + "�typaux�" + document.all('DICT�typaux').value + "�codauxges�" + document.all('DICT�codauxges').value + "�magasi�" + document.all('DICT�magasi').value + "�datcdemin�" + document.all('DICT�datcde').value
	}
	sFeatures = "dialogHeight:358px;dialogWidth:320px;status:no;help:no";
	retourExchange = showModalDialog ( PhpNumbon , "" , sFeatures) ;
	if ( retourExchange != undefined ) {
		if ( retourExchange[0] == "" || retourExchange[0] == undefined ) return
		dogexchange (document.all("DICT�numbon"),retourExchange[0].substring(1))
	}
	else return;
} //Fonction de recherche des bons

function BonRechercherDernier()
{
	//On lance une recherche sur les bons du jour et on prend le dernier attention cependant si une autre saisie est en cours
        paramExchange = "GETVAR|NOSCREEN|NODISPLAYERROR|KEEPZONEVALUE|TRUEDMD|MESSAGE=Recherche du dernier bon saisi"
        //on recherche l'entete a partir de son rowid
        inputParameter = "�CODSOC�<?=$c_codsoc?>�MOTCLE�<?=$_motcle?>�TYPBON�<?=$_typcom?>�DATCDE�<?=$datJour?>|<?=$datJour?>�MAGASI�"+document.all("DICT�magasi").value + "�OPEIHM�<?=$ope?>" + "�TYPAUX�" + document.all("DICT�typaux").value + "�CODAUXGES�" + document.all("DICT�codauxges").value
        nomfct         = "F10-BONENT�n-bonent"
        retourExchange = sendExchange("<?=$dogExchange?>", "", inputParameter, "get", "", paramExchange, "", nomfct) 
	if ( retourExchange == "OK" ) {
		Rsp[1]=Rsp[Rsp.length-1]
		setValueFromKeyword("NUMBON" , false ,"" ,"DICT�numbon" ,true)
		dogexchange (document.all("DICT�numbon"))
	}
	else return;
} //Fonction de recherche des bons

function BonRechercherCde()
{
	//on part sur une recherche
	PhpNumbon = "<?=search::searchExe('nr_selectionbon_ppc.php',1)?>?PHPSESSID=<?=$PHPSESSID?>&_com=O&_SCREEN=�evenement�E�0�typbon�CDF�motcle�ACC�typaux�" + document.all('DICT�typaux').value + "�codauxges�" + document.all('DICT�codauxges').value + "�magasi�" + document.all('DICT�magasi').value + "�datcdemin�" + document.all('DICT�datcde').value
	sFeatures = "dialogHeight:358px;dialogWidth:320px;status:no;help:no";
	retourExchange = showModalDialog ( PhpNumbon , "" , sFeatures) ;
	if ( retourExchange != undefined ) {
		if ( retourExchange[0] == "" || retourExchange[0] == undefined ) return
                paramExchange = "GETVAR|NOSCREEN|NODISPLAYERROR|KEEPZONEVALUE|TRUEDMD"
                //on recherche l'entete a partir de son rowid
                inputParameter = retourExchange[0]
                nomfct         = "GET-BONENT�n-bonent"
                retourExchange = sendExchange("<?=$dogExchange?>", "", inputParameter, "get", "", paramExchange, "", nomfct) 

                //on recherche les lignes du bon pour les precharger
                inputParameter = inputParameter = "�CODSOC�<?=$c_codsoc?>�MOTCLE�ACC�TYPBON�CDF�NUMBON�"+setValueFromKeyword("NUMBON",false,"","",true)
                nomfct         = "GET-BONLIG�n-bonlig"
                retourExchange = sendExchange("<?=$dogExchange?>", "", inputParameter, "get", "", paramExchange, "", nomfct) 
		RspSave = Rsp;
		//on se charge les lignes
		for(indice=1; indice<RspSave.length; indice++)
		{
			Rsp=new Array()
			Rsp[0]="OK"
			Rsp[1]=RspSave[indice]	
			ValRowidLig = "Rowid|tempo"+(LastLig + PasLig)		
			//Cr�ation d'une nouvelle ligne dans le tableau
	                //document.frames("RECAP").MajTable ( "create" , ValRowidLig , LastLig + PasLig , setValueFromKeyword("CODART",false,"","",true) , setValueFromKeyword("LIBART_1",false,"","",true) , setValueFromKeyword("PU-BRUT",false,"","",true) , "" , setValueFromKeyword("PU-NET",false,"","",true) , setValueFromKeyword("UNIFAC",false,"","",true) , "0" , setValueFromKeyword("UNICDE",false,"","",true) , setValueFromKeyword("HTNET",false,"","",true) , setValueFromKeyword("COEFFAC",false,"","",true) , setValueFromKeyword("SOLLIV",false,"","",true) , setValueFromKeyword("TYPBON-CDE",false,"","",true) , setValueFromKeyword("NUMBON-CDE",false,"","",true), setValueFromKeyword("MOTCLE-CDE",false,"","",true),setValueFromKeyword("CHRONO-CDE",false,"","",true),setValueFromKeyword("NATLIG",false,"","",true))
	                LastLig  = LastLig + PasLig
		} //fin de la boucle de cration des lignes du tableau	
		//on se positionne bien le chrono
		document.all("DICT�chrono").value = LastLig  = LastLig + PasLig
		document.all("DICT�codart").focus
	} else return;
} //Fonction de recherche des bons

/*-----------------------------------------------------------------------------------------------------------------------
	fonction de chargement du param�trage analytique (Onglet TIERS)
-----------------------------------------------------------------------------------------------------------------------*/
function charge_ana()
{
    return
    paramExchange   = "GETVAR|NOSCREEN"
    nomfct          = "LABEL-ANA�d-segana"
    strbon          = "" 
	
    retourExchange  = sendExchange ( "<?=$dogExchange?>" , "" , strbon , "get" , ""  , paramExchange,"", nomfct)

    //Tester le retour et quitter s'il ya une erreur
    var tabana  = getDataArrayFromRow(1)

    nbrseg = getValueFromDataArray( tabana ,"NBR-SEG" , false , "" )
    nbrseg = parseInt( nbrseg , 10 )
    if ( nbrseg == 0 ) return

    for ( i=1 ; i<=nbrseg ; i++ )
        label_ana[i] =  getValueFromDataArray( tabana ,"LIBSEGANA_" +i , false , "" )
    Rsp_ana = Rsp
}

//ouverture de la fenetre de saisie analytique 
function setana()
{
	//r�cup�ration du param�trage de l'ent�te
	var ctrl_ana = "<?=$n_param_ana_ent?>" 
	
	//test si le param�trage est vide
	if ( ctrl_ana == ",,,,,,,,,," ) 
	{
		// Param�trage analytique de l'ent�te inexistant.
		alert ( "<?PHP print lect_bin::getLabel ( "mess-cdeent_13" , "sl"); ?>" )
		return
	}//le param�trage est vide	

	if ( nbrseg == 0 ) charge_ana() 
	//Fonction qui charge un tableau global avec les valeurs des segments ana issus du pcg
	if ( nbrseg == 0 || Rsp_ana == undefined || Rsp_ana[0] != "OK" ) return 
	
	Rsp = Rsp_ana
		
	retour = getDataArrayFromRow(1) 
	//Poser les attributs de la fen�tre
	sFeatures = "status:no;dialogWidth:700px;dialogHeight:340px;help:no" 

	//r�cup�ration exercice periode de la commande
	if ( document.all("DICT�datcde").value != "" )
	{
		ExpDatCde = document.all("DICT�datcde").value.split(Slash)
		percpt	  = ExpDatCde[1]
		execpt	  = ExpDatCde[2]
	}

	//Concatainer les variables de param 
	var paramAna = execpt + "�" + percpt + "��<?=$c_codetb?>"  

    //suppression du & dans les libell�s des segments
    var e_com = new RegExp ("&" , "gi")
    for ( l = 1 ; l < nbrseg ; l++ )
        libel_ana[l] = libel_ana[l].replace(e_com,"@et@" )

    //Afficher la fen�tre de saisie de l'analytiques																				    		
	getFromWindowAna = window.showModalDialog ( "<?=search::searchExe("ns_seganamvtcpt.php",1)."?".session_name()."=".session_id()?>&_codeana=" + code_ana + "&_libelana=" + libel_ana + "&_labelana=" + label_ana + "&_paramAna=" + paramAna + "&_ctrlana=" +  ctrl_ana,"" , "" + sFeatures )

	//cette variable si nulle on sort
	if ( getFromWindowAna != "" &&  getFromWindowAna != undefined )
	{
		var reg_an1  = new RegExp("�","gi")
		var reg_an2  = new RegExp("�","gi")
		var reg_an3 = new RegExp("@","gi")
		
		var tabana =  getFromWindowAna.split( reg_an1 ) 
        analyt = ""
		for ( i=1 ; i<=nbrseg ; i++)
		{
			if(tabana[ i-1 ]!=undefined)
			{
				FormatTabana = tabana[ i-1 ].replace(reg_an2,"@�@")
				codana = FormatTabana.split( reg_an2 ) 
			}	
			else
				codana = new Array("","","","")
			code_ana[i]     = codana[1].replace(reg_an3,"") 
			libel_ana[i]    = codana[3].replace(reg_an3,"")
            codeAnaLig[i]   = codana[1].replace(reg_an3,"") 
            libelAnaLig[i]  = codana[3].replace(reg_an3,"") 

			if(code_ana[i]!=undefined)
			{
				analyt += code_ana[i]
			}
			
			if ( i!=nbrseg ) analyt += "." 
		}	
		document.all("DICT�analyt").value = analyt 
        document.all("DICT�analytl").value = analyt

	}
} //Fin de la fonction setAna

function open_ana()
{
	//test si on est en visu seule
	if ( "<?=$n_etat_visu?>" == 1 ) return
	
	//test si le compte g�n�ral n'est suivi en analyutique
	if ( SuiAna.toUpperCase() == "N" )
	{
        // Compte g�n�ral non suivi en analytique.
		alert ( "<?PHP print lect_bin::getLabel ( "mess-cdeent_44" , "sl"); ?>" )
		
		//quitter la fonction 
		return
	}//le compte g�n�ral n'est suivi en analyutique

	if ( ModeLigne != "" )
	{
		sFeatures = "status:no;dialogWidth:700px;dialogHeight:340px;help:no"
	
		//r�cup�ration exercice periode de la commande
		if ( document.all("DICT�datcde").value != "" )
		{
			ExpDatCde = document.all("DICT�datcde").value.split(parent.Slash)
			percpt	= ExpDatCde[1]
			execpt	= ExpDatCde[2]
		}
        else
        {
            percpt = "<?=date("m")?>"
            execpt = "<?=date("Y")?>"
        }

		//Concatainer les variables de param 
        var paramAna = execpt + "�" + percpt + "�" + CptGen + "�<?=$c_codetb?>"  

		var ctrlAnaLig = "<?=$n_param_ana_lig?>" 

		//les segments
		var r_par = new RegExp ("[.]" , "gi")
		
        //suppression du & dans les libell�s des segments
        var e_com = new RegExp ("&" , "gi")
        for ( l = 1 ; l < nbrseg ; l++ )
            libelAnaLig[l] = libelAnaLig[l].replace(e_com,"@et@" )

        //Afficher la fen�tre de saisie de l'analytiques																				    		
		getFromWindowAna = window.showModalDialog ( "<?=search::searchExe("ns_seganamvtcpt.php",1)."?".session_name()."=".session_id()?>&_codeana=" + codeAnaLig + "&_libelana=" + libelAnaLig + "&_labelana=" + label_ana + "&_paramAna=" + paramAna + "&_ctrlana=" +  ctrlAnaLig,"" , "" + sFeatures );
		
		//Mettre � blanc cette variable
		if ( getFromWindowAna != "" )
		{
			var reg_an1  = new RegExp("�","gi")
			var reg_an2  = new RegExp("�","gi")
			var tabana =  getFromWindowAna.split( reg_an1 ) 
			document.all("DICT�analyt").value = ""
			for ( i=1 ; i <= tabana.length ; i++)
			{
				var codana = tabana[ i-1 ].split( reg_an2 ) 
				if ( i!=1 ) document.all("DICT�analyt").value += "." 
				if ( codana[ 1 ] != undefined )
                {
					document.all("DICT�analyt").value += codana[ 1 ]
                    codeAnaLig[i]   = codana[1] 
                    libelAnaLig[i]  = codana[3] 
                }
			}
            g_analyt = document.all("DICT�analyt").value
		}
	}
}

function addRemise()
{
	if ( "<?=$n_etat_visu?>" == 1 ) return
	
	//Poser les attributs de la fen�tre de saisie / modification de la ligne
	sFeatures = "dialogHeight:230px;dialogWidth:700px;status:no;help:no";

	//Afficher la fen�tre de saisie de l'analytique
	PhpSrc = "<?=search::searchExe("ns_cdelig_saisie_remise.php",1)."?".session_name()."=".session_id(). '&_motcle=' . $_motcle . '&_typcom=' . $_typcom ?>"
	getFromWindowLig = showModalDialog ( PhpSrc + "&_glovalremise=" + gloremise ,"" , sFeatures )	
	
	//Mettre � blanc cette variable
	if ( getFromWindowLig == undefined || getFromWindowLig == "" ) return 
	
    gloremise    = getFromWindowLig 
    glovalremise = getFromWindowLig 
    FormatGlovalremise = glovalremise.replace(parent.Soleil,"@�@")
    ExpFormatGlovalremise = FormatGlovalremise.split(parent.Soleil)
    ValDictGloremise = ""
    for ( i = 0 ; i <= ExpFormatGlovalremise.length ; i++ )
    {
        if ( /REM-CODE/.test ( ExpFormatGlovalremise[i] ) )
        {
            FormatExpI              = ExpFormatGlovalremise[i].replace(parent.Arobase,"") 
            FormatExpI              = FormatExpI.replace(parent.Paragraph,"@�@")
            ExpFormatExpI           = FormatExpI.split(parent.Paragraph)
            ValDictGloremise       += ExpFormatExpI[1].replace(parent.Arobase,"")
            if ( ExpFormatExpI[0].replace(parent.Arobase,"").toUpperCase() != "REM-CODE_5" )
                ValDictGloremise += "."         
       }        
    }
	document.all("DICT�gloremise").value = ValDictGloremise
}
/*---------------------------------------------------------------------------------------------------------------------*/

function CtrlSaiZone(obj)
{
	if ( obj.id == "DICT�reftie" ) {
			BonValider()
			return	
	}
	if ( obj.id == "DICT�qte"){
		//si on a 00 c'est une suppression
		if(obj.value=="X"){
			LigneSupprimer()
			return	
		}
		if ( !isNaN ( obj.value ) ) {
			if(parseFloat(document.all("DICT�qte").value)>100000 ){
				alert("Quantit� non autoris�e")
				document.all("DICT�qte").value = ""
				return;
			}
		}
	}
	if ( obj.id == "DICT�qte"){
		//si on a 00 c'est une annulation de saisie de ligne
		if(obj.value=="00"){
			if ( ModeLigne == "create" ){
		            ModeLigne = "create"
		            document.all("DICT�codart").value       = ""
		            document.all("DICT�libart_1").value     = ""
		            document.all("DICT�qte").value          = ""
		            document.all("DICT�pu-brut").value      = ""
		            document.all("DICT�texte-lig").value	= ""
		            Generique                               = "N"
		            CptGen	                                = ""
		            CodTVA	                                = ""
		            CptTVA	                                = ""
		            Imputa	                                = ""
		            CoefFac	                                = ""
		            CoefCde	                                = ""
		            UniFac                                  = ""
		            UniCde                                  = ""
		            Decima	                                = ""
		            SuiAna	                                = ""
		            GesLibel                                = ""
		            //mise � jour du champ glovalremise
		            document.all("DICT�gloremise").value    = ""        
		            gloremise                               = ""
		
		            analyt = ""
		            //r�cup�ration et mise � jour de l'analytique
		            for ( s = 1 ; s <= nbrseg ; s++ )
		            {
		                //charger la valeur du champ analytique de l'entete
		                analyt        += code_ana[s]
		                codeAnaLig[s]  = code_ana[s]
		                libelAnaLig[s] = libel_ana[s]
		                if ( s != nbrseg )
		                    analyt += "."
		            }
		            //maj du champs analytique
		            document.all("DICT�analytl").value          = analyt
		            document.all("DICT�chrono").value           = LastLig + PasLig
		            document.all('DICT�natlig').value           = "N"
		            document.all('DICTLINK�natlig�libel').value	= "Lignes Normales"
		            document.all("rowid").value=""
		            //enlever la lecture seule
		            document.all("DICT�libart_1").readOnly  = true
		            document.all("DICT�libart_1").className = "dogskininputreadonly"
		            document.all("DICT�chrono").readOnly  = true
		            document.all("DICT�chrono").className = "dogskininputreadonly"		            
		            if ( codartlie != "" )
		            {
		                document.all("DICT�codart").value = codartlie
		                codartlie = ""
		                dogexchange ( document.all("DICT�codart") , "GET" )
		            }
		            else
		                document.all("DICT�codart").focus()
			    return	
			} 
			if ( ModeLigne == "modif" ) {
				LigneSupprimer()
		            ModeLigne = "create"
		            document.all("DICT�codart").value       = ""
		            document.all("DICT�libart_1").value     = ""
		            document.all("DICT�qte").value          = ""
		            document.all("DICT�pu-brut").value      = ""
		            document.all("DICT�texte-lig").value	= ""
		            Generique                               = "N"
		            CptGen	                                = ""
		            CodTVA	                                = ""
		            CptTVA	                                = ""
		            Imputa	                                = ""
		            CoefFac	                                = ""
		            CoefCde	                                = ""
		            UniFac                                  = ""
		            UniCde                                  = ""
		            Decima	                                = ""
		            SuiAna	                                = ""
		            GesLibel                                = ""
		            //mise � jour du champ glovalremise
		            document.all("DICT�gloremise").value    = ""        
		            gloremise                               = ""
		
		            analyt = ""
		            //r�cup�ration et mise � jour de l'analytique
		            for ( s = 1 ; s <= nbrseg ; s++ )
		            {
		                //charger la valeur du champ analytique de l'entete
		                analyt        += code_ana[s]
		                codeAnaLig[s]  = code_ana[s]
		                libelAnaLig[s] = libel_ana[s]
		                if ( s != nbrseg )
		                    analyt += "."
		            }
		            //maj du champs analytique
		            document.all("DICT�analytl").value          = analyt
		            document.all("DICT�chrono").value           = LastLig + PasLig
		            document.all('DICT�natlig').value           = "N"
		            document.all('DICTLINK�natlig�libel').value	= "Lignes Normales"
		            document.all("rowid").value=""
		            //enlever la lecture seule
		            document.all("DICT�libart_1").readOnly  = true
		            document.all("DICT�libart_1").className = "dogskininputreadonly"
		            document.all("DICT�chrono").readOnly  = true
		            document.all("DICT�chrono").className = "dogskininputreadonly"		            
		            if ( codartlie != "" )
		            {
		                document.all("DICT�codart").value = codartlie
		                codartlie = ""
		                dogexchange ( document.all("DICT�codart") , "GET" )
		            }
		            else
		                document.all("DICT�codart").focus()
			    return	
			}
		}
		
	}
	//test que la saisie est du numl�rique
	if ( !isNaN ( obj.value ) )
	{
		if ( obj.id == "DICT�qte" && Decima != "" && Decima != "NOMATCH" && Decima != 0 && Decima != undefined ){
			obj.value = nombre_arrondi ( obj.value , Decima )
		}
		
		if ( obj.id == "DICT�pu-brut" )
	        {
	            //r�cup�ration du code tarif
	            CodTar = ""
	            //calcule du htnet
	            Qte     = parseFloat ( document.all("DICT�qte").value )
	            PuBrut  = parseFloat ( obj.value )
		    //le prix ne peut etre sup a 9999999
		    if(PuBrut>99999999){
		            document.all("DICT�pu-brut").focus()
		            document.all("DICT�pu-brut").select()
			    return			    		
		    }
	            //r�cup�ration du coef de facturation
	            CoefFacLig = 1
	            if ( CoefFac != "" )
	                CoefFacLig = parseFloat ( CoefFac )
	            HtNet   = Qte * PuBrut
	            //test si le coef facturation est < 0
	            if ( CoefFacLig < 0 ) HtNet = - ( HtNet / CoefFacLig )
	            if ( CoefFacLig > 0 ) HtNet = HtNet * CoefFacLig
	
	            //gestion remise conditionnelle
	            nomfct = "CALREM�n-calrem"
	            TypauxGesSai = document.all("DICT�typaux").value 
	            CodauxGesSai = document.all("DICT�codauxges").value
	
	            strbon = "CODSOC�<?=$c_codsoc?>�MOTCLE�<?=$_motcle?>�TYPBON�<?=$_typcom?>�NUMBON�" + document.all('DICT�numbon').value + "�CODART�" + document.all('DICT�codart').value + "�TYPAUX�" + TypauxGesSai
	            strbon += "�CODAUXGES�" + CodauxGesSai + "�DATTAR�" + document.all("DICT�datcde").value + "�DATLIV�" + document.all("DICT�datdep").value
	            strbon += "�DEVISE�" + document.all("DICT�devise").value + "�CODTAR�" + CodTar + "�QTE�" + document.all("DICT�qte").value + "�PU-BRUT�" + document.all("DICT�pu-brut").value
	            strbon += "�HTNET�" + HtNet
	
	            paramExchange = "GETVAR|NOSCREEN|TRUEDMD|NODISPLAYERROR"
	
	            //Faire le F10
	            retourExchange = sendExchange ( "<?=$dogExchange?>" , ""  , strbon , "GET" , "" , paramExchange , "" , nomfct ) 	

	            //initialisation du tableau de valeurs
	            var TabGloRemise = ""
	            var ValGlovalremise = ""

	            if ( retourExchange != "OK" ) {	
		            //boucle de r�cup�ration des valeurs saisies
		            for ( r = 1 ; r <= 5 ; r++ )
		            {
		                //test si le code existe
		                if ( getValueFromDataArray( getDataArrayFromRow(1) , "REM-CODE_" + r , false , "" ) != "NOMATCH" )
		                {
		                    if ( r != 1 )
		                        TabGloRemise += "�"                   
		
		                    ValGlovalremise += getValueFromDataArray( getDataArrayFromRow(1) , "REM-CODE_" + r  , false , "" 		  )
		
		                    //mise � jour de la variable _glovalremise
		                    TabGloRemise    += "REM-CODE_" + r + "�" +    getValueFromDataArray( getDataArrayFromRow(1) , "REM-CODE_" + r  , false , "" 		  ) +
		                                       "�LIBEL�" +                getValueFromDataArray( getDataArrayFromRow(1) , "LIBEL"          , true  , "REM-CODE_" + r  ) +     
		                                       "�REM-TYPAPP_" + r + "�" + getValueFromDataArray( getDataArrayFromRow(1) , "REM-TYPAPP_" + r, false , "" 		  ) +
		                                       "�LIBEL�" +                getValueFromDataArray( getDataArrayFromRow(1) , "LIBEL"          , true  , "REM-TYPAPP_" + r) +     
		                                       "�REM-VALEUR_" + r + "�" + getValueFromDataArray( getDataArrayFromRow(1) , "REM-VALEUR_" + r, false , "" 		  )
		                }
		
		                if ( r != 5 )
		                    ValGlovalremise += "."                   
		            }//boucle de r�cup�ration des valeurs saisies
		    }
	            //mise � jour du champ glovalremise
	            document.all("DICT�gloremise").value = ValGlovalremise        
	            gloremise = TabGloRemise
	        }        
	
	        event.returnValue = false
		//test saisie qte
	        if ( obj.id == "DICT�qte" ){
			//dans le cas des commandes fournisseur
			if("<?=$_qtecde?>"!=""){
				//le champ par-comb se substitue au champ surcondit
				//on verifie si on a une valeur de conditionnement
				if(document.all("DICT�par-comb").value.trim()!=""&&document.all("DICT�par-comb").value!="0"){
					//on verifie que l'on a bien un multiple de surcond	
					ztrav=parseFloat(document.all("DICT�qte").value)/parseFloat(document.all("DICT�par-comb").value)
					if(ztrav!=parseInt(ztrav)){
						//dans le cas ou c'est bloquant
						//if(LignesArraygetRsp(currow,"TYP-SURCOND")=="O") {
							alert("Le conditionnement doit �tre un multiple de " + document.all("DICT�par-comb").value)	
							document.all("DICT�qte").value = ""
							return false
						//} else {
						//	if(confirm("Le conditionnement doit �tre un multiple de " + LignesArraygetRsp(currow,"PAR-COMB") + ". Modifier cette quantit� ?"))return false					
						//}
					}
					if(document.all("DICT�mincde-art").value!=""){
						if(parseFloat(document.all("DICT�qte").value)<parseFloat(document.all("DICT�mincde-art").value)){
							alert("Mininum autoris� : "+document.all("DICT�mincde-art").value)
							document.all("DICT�qte").value = ""
							return false
						}
					}
				}			
			} else {
				if(document.all("DICT�surcondit").value.trim()!=""&&document.all("DICT�surcondit").value!="0"&&document.all("DICT�surcondit").value!="1"){
					//on verifie que l'on a bien un multiple de surcond	
					ztrav=parseFloat(document.all("DICT�qte").value)/parseFloat(document.all("DICT�surcondit").value)
					if(ztrav!=parseInt(ztrav)){
						//dans le cas ou c'est bloquant
						if(document.all("DICT�typ-surcondit").value=="O") {
							alert("Le conditionnement doit �tre un multiple de " + document.all("DICT�surcondit").value)	
							document.all("DICT�qte").value = ""
							return false
						} else {
							if( document.all("DICT�motcle").value.substr(0,1)=="A"){
								ztrav=parseFloat(document.all("DICT�qte").value)/parseFloat(document.all("DICT�coefcde").value)
								if(ztrav!=parseInt(ztrav)){
									alert("Le conditionnement doit �tre un multiple de " + document.all("DICT�coefcde").value)
									document.all("DICT�qte").value = ""
									return false					
								}
							} else {
								if(confirm("Le conditionnement doit �tre un multiple de " + document.all("DICT�surcondit").value + ". Modifier cette quantit� ?"))return false									
							}
						}
					}
				}				
			}

	            if(document.all("DICT�pu-brut").parentElement.style.visibility=="hidden"||document.all("DICT�pu-brut").className=="dogskininputreadonly") {
	        	document.all("LienAccept").focus()
	        	LigneValider()	            
	            } else
	            	document.all("DICT�pu-brut").focus()
		}
	        //test saisie pu-brut
	        if ( obj.id == "DICT�pu-brut" ) {
	        	document.all("LienAccept").focus()
	        	LigneValider()
	        }
	} else {
		obj.value = ""
        // Vous ne pouvez saisir que du num�rique
		alert ( "<?PHP print lect_bin::getLabel ( "mess-cdeent_43" , "sl"); ?>" )	
	}	
}

function LigneDisplay()
{
	inputParameter = inputParameter = "�CODSOC�<?=$c_codsoc?>�MOTCLE�"+document.all("DICT�motcle").value+"�TYPBON�"+document.all("DICT�typbon").value+"�NUMBON�"+document.all("DICT�numbon").value+"�CHRONO�"+document.all("DICT�chrono").value
	nomfct         = "GET-BONLIG�n-bonlig"
	retourExchange = sendExchange("<?=$dogExchange?>", "", inputParameter, "get", "", "NODISPLAYERROR|GETVAR|NOSCREEN|TRUEDMD|MESSAGE=Acces ligne : "+ document.all("DICT�chrono").value, "", nomfct) 
    	// Le chargement de la ligne a �chou�.
	if ( retourExchange != "OK" ) {
		maxchrono()
		document.all("DICT�chrono").value=LastLig+PasLig
		return
	}
	else {
		parent.ModeLigne = 'modif'
		parent.document.all("rowid").value=""
	}
	document.all("rowidligne").value="�ROWID�"+getValueFromDataArray(getDataArrayFromRow(1), "ROWID", false , "" )
    	//formatage de la zone de texte
	if ( setValueFromKeyword2("TEXTE-LIG",false ,"","",true) != "NOMATCH" )
	{
		//r�cup�ration du texte
		ValTexte = setValueFromKeyword2("TEXTE-LIG",false ,"","",true)
		
		//initialisation d'expressions r�guli�res
		var Chr10	= new RegExp ( "CHR[(]10[)]" , "g" ) 
		var BR	= new RegExp ( "<BR>" , "gi" ) 

		//test si il y a des retour chariot dans le texte
		if ( Chr10.test(ValTexte) )
			ValTexte = ValTexte.replace(Chr10,"\r")	
		
		if ( BR.test(ValTexte) )
			ValTexte = ValTexte.replace(BR,"\r")	

		parent.document.all("DICT�texte-lig").value = ValTexte
	} else 
    		parent.document.all("DICT�texte-lig").value = ""

	//si la ligne est texte, on bloque tout sauf natlig et texte
	if ( setValueFromKeyword2("NATLIG",false ,"","",true) == "T" ) {
	        parent.document.all("DICT�codart").readOnly        = true
	        parent.document.all("DICT�codart").className       = "dogskininputreadonly"
	        parent.document.all("DICT�qte").readOnly           = true
	        parent.document.all("DICT�qte").className          = "dogskininputreadonly"
	        parent.document.all("DICT�pu-brut").readOnly       = true
	        parent.document.all("DICT�pu-brut").className      = "dogskininputreadonly"
	        parent.document.all("DICT�analytl").readOnly       = true
	        parent.document.all("DICT�analytl").className      = "dogskininputreadonly"
	        parent.document.all("DICT�gloremise").readOnly     = true
	        parent.document.all("DICT�gloremise").className    = "dogskininputreadonly"
	} else {
	        parent.document.all("DICT�codart").readOnly        = false
	        parent.document.all("DICT�codart").className       = "dogskininput"
	        parent.document.all("DICT�qte").readOnly           = false
	        parent.document.all("DICT�qte").className          = "dogskininput"
		if('<?=$_typcom?>'!="RGS"){
		        parent.document.all("DICT�pu-brut").readOnly       = false
		        parent.document.all("DICT�pu-brut").className      = "dogskininput"
	        }
	        parent.document.all("DICT�analytl").readOnly       = false
	        parent.document.all("DICT�analytl").className      = "dogskininput"
	        parent.document.all("DICT�gloremise").readOnly     = false
	        parent.document.all("DICT�gloremise").className    = "dogskininput"
	        parent.document.all("DICT�chrono").readOnly        = true
	        parent.document.all("DICT�chrono").className       = "dogskininputreadonly"
	}

       	// si piece en visu on bloque tote la saisie
    	if ( parent.document.all("mode_bon").value == "visu" ) {
	        parent.document.all("DICT�codart").readOnly        = true
	        parent.document.all("DICT�codart").className      = "dogskininputreadonly"
	        parent.document.all("DICT�qte").readOnly           = true
	        parent.document.all("DICT�qte").className          = "dogskininputreadonly"
	        parent.document.all("DICT�pu-brut").readOnly       = true
	        parent.document.all("DICT�pu-brut").className      = "dogskininputreadonly"
	        parent.document.all("DICT�analytl").readOnly       = true
	        parent.document.all("DICT�analytl").className      = "dogskininputreadonly"
	        parent.document.all("DICT�gloremise").readOnly     = true
	        parent.document.all("DICT�gloremise").className    = "dogskininputreadonly"
    	}

	//initialisation du tableau de valeurs
	var TabGloRemise = ""
	var ValGlovalremise = ""
	
	//boucle de r�cup�ration des valeurs saisies
	for ( r = 1 ; r <= 5 ; r++ ) {
		//test si le code existe
		if ( setValueFromKeyword2("REM-CODE_" + r,false ,"","",true) != "NOMATCH" ){
		    if ( r != 1 )
		        TabGloRemise += "�"                   
		
		    ValGlovalremise += setValueFromKeyword2("REM-CODE_" + r,false ,"","",true)
		
		    //mise � jour de la variable _glovalremise
		    TabGloRemise    += "REM-CODE_" + r + "�" +    getValueFromDataArray( getDataArrayFromRow(1) , "REM-CODE_" + r  , false , "" , true ) +
		                       "�LIBEL�" +                getValueFromDataArray( getDataArrayFromRow(1) , "LIBEL"          , true  , "REM-CODE_" + r , true ) +     
		                       "�REM-TYPAPP_" + r + "�" + getValueFromDataArray( getDataArrayFromRow(1) , "REM-TYPAPP_" + r, false , "" , true ) +
		                       "�LIBEL�" +                getValueFromDataArray( getDataArrayFromRow(1) , "LIBEL"          , true  , "REM-TYPAPP_" + r , true ) +     
		                       "�REM-VALEUR_" + r + "�" + getValueFromDataArray( getDataArrayFromRow(1) , "REM-VALEUR_" + r, false , "" , true )
		}
		if ( r != 5 )
		    ValGlovalremise += "."                   
	}//boucle de r�cup�ration des valeurs saisies
	parent.Codart	= setValueFromKeyword2("CODART", false, "", "", true)	
	parent.CptGen	= setValueFromKeyword2("CPTGEN",false ,"","",true)
	parent.CodTVA   = setValueFromKeyword2("CODTVA",false ,"","",true)
	parent.CptTVA	= setValueFromKeyword2("CPTTVA",false ,"","",true)
	parent.Imputa	= setValueFromKeyword2("IMPUTA",false ,"","",true)
	parent.CoefFac	= setValueFromKeyword2("COEFFAC",false ,"","",true)
	parent.CoefCde	= setValueFromKeyword2("COEFCDE",false ,"","",true)
	parent.UniFac	= setValueFromKeyword2("UNIFAC",false ,"","",true)
	parent.UniCde	= setValueFromKeyword2("UNICDE",false ,"","",true)
	parent.Decima	= setValueFromKeyword2("DECIMA",false ,"","",true)
	parent.GesLibel = setValueFromKeyword2("GESLIBEL",false ,"","",true)
	parent.SuiAna   = setValueFromKeyword2("SUIANA",false ,"","",true)

    	if ( setValueFromKeyword2("GENERIQUE",false ,"","",true).toUpperCase() == "O" )
        	parent.Generique = "O"

	//mise � jour du champ glovalremise
	parent.document.all("DICT�gloremise").value         = ValGlovalremise        
	parent.gloremise                                    = TabGloRemise
	parent.document.all("DICT�chrono").value            = setValueFromKeyword2("CHRONO",false ,"","",true) 
	parent.document.all("DICT�natlig").value            = setValueFromKeyword2("NATLIG",false ,"","",true)
	parent.document.all("DICTLINK�natlig�libel").value  = setValueFromKeyword2("LIBEL",true ,"NATLIG","",true)
	parent.document.all("DICT�qte").value               = setValueFromKeyword2("QTE",false ,"","",true).trim()
	parent.document.all("DICT�pu-brut").value           = setValueFromKeyword2("PU-BRUT",false ,"","",true).trim()
	parent.document.all("DICT�codart").value            = setValueFromKeyword2("CODART",false ,"","",true)
	parent.document.all("DICT�libart_1").value          = setValueFromKeyword2("LIBART_1",false ,"","",true)
	//initialisation � vide des valeurs de la zone analytique
	analyt = ""

	//maj du champs analytique
	if ( analyt != "." ) parent.document.all("DICT�analytl").value = analyt
	
	//test si on se trouve sur un article de type compos�
	if ( parent.GesLibel == 1 ) {
		//enlever la lecture seule
		parent.document.all("DICT�libart_1").readOnly = false
		//changer le style de la zone
		parent.document.all("DICT�libart_1").className = "dogskininput"
	} else {
		//enlever la lecture seule
		parent.document.all("DICT�libart_1").readOnly = true
		//changer le style de la zone
		parent.document.all("DICT�libart_1").className = "dogskininputreadonly"
	}//on se trouve sur un article de type compos�
	
	//on se fait un acces sur le code article
	if(topstop!=true)
		parent.dogexchange(parent.document.all("DICT�codart"),"GET")
	else
		topstop=true
	window.scrollTo(0,334) 
	posi = 2      
	if ( setValueFromKeyword2("NATLIG",false ,"","",true) == "T" )
		parent.document.all("DICT�texte-lig").focus()
	else
		parent.document.all("DICT�qte").focus()
}


//maj des dates
function MajDatLoc(Obj,idFocus)
{
	//initialisation � true de VerifFormatdate
	VerifFormatDate = true
	
	//initialisation de l'obligation de saisie de la date
	Obli = 1
	CmpDate = ""
	
	VerifFormatDate = formatDate(Obj,Obli)
	//contr�le de certaines dates
	switch ( Obj.id )
	{
		//date de livraison (ong TIERS)
		case "DICT�datdep" :
	            //test si date de livraison sup�rieur ou �gal � date d'arr�t� de stock
	            if ( !CompareDate ( Obj.value , "<?=$datmin_stock?>" , 0 ) )
	            {
	                // Date inf�rieure ou �gale � derni�re date d'arr�t�.
	                CmpDate = "Saisie obligatoire"
	                break
	            }//pi�ce tenue en stock

	            //test si date de livraison sup�rieur ou �gal � date de commande
	            if ( !CompareDate ( Obj.value , document.all("DICT�datcde").value , 1 ) )
	                CmpDate = "<?PHP print lect_bin::getLabel ( "mess-cdeent_69" , "sl"); ?>" + document.all("DICT�datcde").value  
			break
	}//contr�le de certaines dates
	if(Obj.id == "DICT�datcde") {
		if ( VerifFormatDate == true && CmpDate == "" ) {
		        if(document.all("DICT�analyt").style.visibility=="hidden"&&document.all("DICT�datdep").style.visibility=="hidden") {
		        	BonValider();
		        	return;	
		        }				        	
			document.all(idFocus).focus()
			event.returnValue = "";
		}
		else if ( CmpDate.trim() != "" ) {
			alert ( CmpDate ) 
			Obj.value = ""
		}	
	}
	if(Obj.id == "DICT�datdep") {
		if ( VerifFormatDate == true && CmpDate == "" ) {
		        if((document.all("DICT�analyt").style.visibility=="hidden"&&document.all("DICT�reftie").style.visibility=="hidden")||(document.all("DICT�analyt").parentElement.style.visibility=="hidden"||document.all("DICT�reftie").parentElement.style.visibility=="hidden")) {
		        	BonValider();
		        	return;	
		        }				        	
		        if(document.all("DICT�analyt").style.visibility=="hidden"&&document.all("DICT�reftie").style.visibility=="") {
		        	document.all("DICT�reftie").focus();
		        	return;	
		        }				        	
			document.all(idFocus).focus()
			event.returnValue = "";
		}
		else if ( CmpDate.trim() != "" ) {
			alert ( CmpDate ) 
			Obj.value = ""
		}	
	}
}//fin fonction MajDatLoc

function BonValider() {
	if(!ctrlobli()){
		return;
	}
    	//si numbon saisi et valid�
	if ( document.all("mode_bon").value != "" && document.all("DICT�numbon").value != "" ) {
		//initialisation � vide des valeurs saisies
		str_val = ""
		//initialisation de la variable de saisie analytique OK
		var Point = new RegExp ( "[.]" , "gi" ) 
		OkAnalyt = true
		if ( document.all("DICT�analyt").value != "" ) {
		TempAnalyt = document.all("DICT�analyt").value.replace(Point,"@.@")
		ExpAnalyt  = TempAnalyt.split(Point)
		//boucle de test de toutes les valeurs analytiques saisies
		for ( i = 0 ; i < nbrsegEnt ; i++ ) {
			ValTestee = ""
			ValTestee = ExpAnalyt[i].replace(parent.Arobase,"")
			str_val += "SEGANA_"+(i+1)+"�"+ValTestee+"�"
			//cas traitement sp�cifique
			if ( ValTestee == "" ) {
			      OkAnalyt = false
			      break
			}
		}
		} else OkAnalyt = false
	        str_val += "CODSOC�<?=$c_codsoc?>�" 
	        str_val += "MOTCLE�<?=$_motcle?>�" 
	        str_val += "TYPBON�<?=$_typcom?>�" 
	        str_val += "ETABLI�<?=$c_codetb?>�" 
	        str_val += "MAGASI�" + document.all('DICT�magasi').value + "�" 
	        str_val += "NUMBON�" + document.all('DICT�numbon').value + "�" 
	        str_val += "TYPAUX�" + document.all('DICT�typaux').value + "�" 
	        str_val += "CODAUXGES�" + document.all('DICT�codauxges').value + "�" 
	        str_val += "ADR-LIV_1�" + document.all('DICT�adresse_1').value + "�" 
	        str_val += "ADRESSE_1�" + document.all('DICT�adresse_1').value + "�" 
	        str_val += "ADR-LIV_2�" + document.all('DICT�adresse_2').value + "�" 
	        str_val += "ADRESSE_2�" + document.all('DICT�adresse_2').value + "�" 
	        str_val += "ADR-LIV_3�" + document.all('DICT�adresse_3').value + "�" 
	        str_val += "ADRESSE_3�" + document.all('DICT�adresse_3').value + "�" 
	        str_val += "ADR-LIV_4�" + document.all('DICT�adresse_4').value + "�" 
	        str_val += "ADRESSE_4�" + document.all('DICT�adresse_4').value + "�" 
	        str_val += "ADR-LIV_5�" + document.all('DICT�adresse_5').value + "�" 
	        str_val += "ADRESSE_5�" + document.all('DICT�adresse_5').value + "�" 
	        str_val += "DATCDE�" + document.all('DICT�datcde').value + "�" 
	        str_val += "DATDEP�" + document.all('DICT�datdep').value + "�" 
	        str_val += "REFTIE�" + document.all('DICT�reftie').value + "�"         
	        str_val += "DEVISE�" + document.all('DICT�devise').value + "�" 
		str_val += "VALIDE�" + document.all('DICT�valide').value + "�" 
		str_val += "CODTAR�" + document.all('DICT�codtar').value + "�" 
	        if(document.all('DICT�typregul').value.trim()=="")
	        	str_val += "NATBON�" + document.all('DICT�natbon').value + "�" 
	        else
	        	str_val += "NATBON�" + document.all('DICT�typregul').value + "�" 
	
	        retourExchange  = sendExchange ( "<?=$dogExchange?>" ,  "" , str_val  , "get" , "" ,"GETVAR|NOSCREEN|TRUEDMD","" ,"MAJ-BONENT�n-entmaj") 
	        if ( retourExchange != "OK" ) {
			// L'enregistrement du bon a �chou�.
			alert ( "<?PHP print lect_bin::getLabel ( "mess-cdeent_62" , "sl"); ?>" )
			return
	        } else {
			//r�cuperer des zones
			document.all("DICT�bon-ht").value = getValueFromDataArray( getDataArrayFromRow(1) ,"BON-HT" , false , "" )
			document.all("mode_bon").value = "modif"
			ClearFrameSaisie ( 'SAILIGNE' , false , 'NOCLEAR' )
			if('<?=$_typcom?>'=="RGS"){
		                document.all("DICT�pu-brut").readOnly       = true
		                document.all("DICT�pu-brut").className      = "dogskininputreadonly"
                	}

			ModeLigne = "create"
			window.scrollTo(0,334)
			posi = 2       
			document.all("DICT�codart").focus()
	        }
	}
}

function BonNouveau() {
	//vider le n� de bon et mettre le focus dessus
	window.scrollTo(0,0)
	posi = 1
	document.all("DICT�numbon").value = ""
	document.all("DICT�numbon").focus() 
	document.all("DICT�bon-ht").value = ""
	ClearFrameSaisie ( "SAITIERS" , true , "" )
	ClearFrameSaisie ( "SAILIGNE" , true , "" )
	document.all("DICT�datcde").value = "<?=$datJour?>"
	InitFrame()
	LastLig = 0
	PasLig  = 10
	InitFrame()
	document.all("DICT�chrono").value            = parent.LastLig + parent.PasLig
	document.all('DICT�natlig').value            = "N"
	document.all('DICTLINK�natlig�libel').value	= "Lignes Normales"
}

function BonFin()
{
    //si numbon saisi et valid�
	if ( document.all("mode_bon").value != "" && document.all("DICT�numbon").value != "" )
	{
		//initialisation � vide des valeurs saisies
		str_val = ""

	        //initialisation de la variable de saisie analytique OK
	        str_val += "CODSOC�<?=$c_codsoc?>�" 
	        str_val += "MOTCLE�<?=$_motcle?>�" 
	        str_val += "TYPBON�<?=$_typcom?>�" 
	        str_val += "ETABLI�<?=$c_codetb?>�" 
	        str_val += "NUMBON�" + document.all('DICT�numbon').value + "�" 
	        str_val += "MAGASI�" + document.all('DICT�magasi').value + "�" 
	        str_val += "TYPAUX�" + document.all('DICT�typaux').value + "�" 
	        str_val += "CODAUXGES�" + document.all('DICT�codauxges').value + "�" 
	        str_val += "ADRESSE_1�" + document.all('DICT�adresse_1').value + "�" 
	        str_val += "DATCDE�" + document.all('DICT�datcde').value + "�" 
	        str_val += "DATDEP�" + document.all('DICT�datdep').value + "�" 
	        str_val += "DEVISE�" + document.all('DICT�devise').value + "�" 
		str_val += "VALIDE�" + document.all('DICT�valide').value + "�" 
	        retourExchange  = sendExchange ( "<?=$dogExchange?>" ,  "" , str_val  , "get" , "" ,"GETVAR|NOSCREEN|TRUEDMD","" ,"FIN-BONENT�n-entmaj") 
	        if ( retourExchange != "OK" )
	        {
	            // L'enregistrement du bon a �chou�.
	            alert ( "<?PHP print lect_bin::getLabel ( "mess-cdeent_62" , "sl"); ?>" )
	            return
	        }
	        else
	        {
			//on part en creation d'un nouveau bon
			BonNouveau()
	        }
	}
}

function BonSupprimer()
{
	//test si on se trouve sur un bon existant
	if ( RowidEntete != "" && document.all("mode_bon").value != "" )
	{
		//cr�ation des input parameter (recherche sur le ROWID)
		var inputparam = "ROWID�" + RowidEntete
		
		//confirmation demande de suppression
		if (confirm("<?PHP print lect_bin::getLabel ( "mess-cdeent_64", "sl"); ?>"))
		{
			//lancement du traitement
			retourExchange  = sendExchange ( "<?=$dogExchange?>" , ""  , inputparam , "get" , ""  ,"DOGADMIN=3" , "" , "MAJ-BONENT�n-entmaj" )
			
			//test si echange non correct
			if (retourExchange != "OK")
			{
				// La suppression du bon a �chou�.
				alert("<?PHP print lect_bin::getLabel ( "mess-cdeent_59" , "sl"); ?>")
				return
			}
			
			//vider le n� de bon et mettre le focus dessus
			document.all("DICT�numbon").value = ""
			document.all("DICT�numbon").focus() 
			window.scrollTo(0,0)
			posi = 1
            document.all("DICT�bon-ht").value = ""
            ClearFrameSaisie ( "SAITIERS" , true , "" )
            ClearFrameSaisie ( "SAILIGNE" , true , "" )
            document.all("DICT�datcde").value = "<?=$datJour?>"
            LastLig = 0
            PasLig  = 10
            //document.frames("RECAP").location.href = "<?=search::searchExe("ns_cdeentlignes_ppc.php",1)?>?<?=session_name()?>=<?=session_id()?>"
		}//confirmation demande de suppression
    }//test si on se trouve sur un bon existant
}//Fonction de suppression

//impression unitaire
function Imprimer()
{
	//test si on se trouve sur un bon existant	
	if ( RowidEntete != undefined && RowidEntete != "" && RowidEntete != "BONENT|" )
	{
		retourExchange  = sendExchange ( "<?=$dogExchange?>" , "" , "ROWID�" + RowidEntete , "get" , "" , "GETVAR|NOSCREEN|TRUEDMD" , "" , "IMP-BONENT�n-bonent" ) 

		// Erreur dans l'impression.
		if ( retourExchange != "OK" ) alert ( "<?PHP print lect_bin::getLabel ( "mess-cdeent_61" , "sl"); ?>" )
		else
		{
			ExpRsp = Rsp[1].split("=")
			NomSpool = ExpRsp[1]
			httpFile = "<?=$xcspoolRelPath.$ope?>/" + NomSpool 
			document.all("link-visu").href = httpFile 
			document.all("link-visu").click()
		}
	}//on se trouve sur un bon existant	
}//function d'impression du bon unitaire

function LigneValider()
{
	if ( ModeLigne != "" ){
		//initialisation � vide des valeurs saisies
		strLigne = "CODSOC�<?=$c_codsoc?>�MOTCLE�<?=$_motcle?>�TYPBON�<?=$_typcom?>"

		//initialisation � 1 saisie analytique OK
		SaisieAnaOK = 1

	        if ( document.all("DICT�natlig").value.toUpperCase() != "T" )
	        {	
	            //test quantit� sup�rieur � 0 pour les lignes normales
	    		if ( document.all("DICT�qte").value == 0 )
	    		{
	                // Vous ne pouvez pas saisir de quantit� �gale � 0.
	    			alert ( "<?PHP print lect_bin::getLabel ( "mess-cdeent_45" , "sl"); ?>" )
	    			document.all("DICT�qte").focus()
	    			return
	    		}	
	    				
	    		//teste si la nature de ligne est saisie
	    		if ( document.all("DICT�natlig").value.trim() == "" ) {
	    			alert ( "<?PHP print lect_bin::getLabel ( "mess-cdeent_46" , "sl"); ?>" )
	    			document.all("DICT�natlig").focus()
	    			return
	    		}
	    		if ( document.all("DICT�codart").value.trim() == "" || document.all("DICT�codart").value.trim().length >= 13 || document.all("DICT�codart").value.trim()=="NOMATCH") {
	    			alert ( "Article non coh�rent" )
	    			document.all("DICT�codart").focus()
	    			return
	    		}
	
	    		//teste si la quantit� est saisi
	    		if ( document.all("DICT�qte").value.trim() == "" )
	    		{
	                // Vous devez saisir le champs obligatoire : 
	    			alert ( "<?PHP print lect_bin::getLabel ( "mess-cdeent_46" , "sl"); ?>" )
	    			document.all("DICT�qte").focus()
	    			return
	    		}//le champs input est obligatoire
	
	    		//teste si le pu brut est saisi
	    		if ( document.all("DICT�pu-brut").value.trim() == "" && document.all("DICT�pu-brut").parentElement.style.visibility=="")
	    		{
	                // Vous devez saisir le champs obligatoire : 
	    			//alert ( "<?PHP print lect_bin::getLabel ( "mess-cdeent_46" , "sl"); ?>" )
	    			//document.all("DICT�pu-brut").focus()
	    			//return
	    		}//le champs input est obligatoire
	        }

	        strLigne += "�NUMBON�"   + document.all("DICT�numbon").value
	        strLigne += "�CHRONO�"   + document.all("DICT�chrono").value
	        strLigne += "�INDICE�00"
	        strLigne += "�QTE�"      + document.all("DICT�qte").value
	        strLigne += "�CODART�"   + document.all("DICT�codart").value
	        strLigne += "�LIBART_1�" + document.all("DICT�libart_1").value 
	        strLigne += "�UNICDE�"   + UniCde
	        strLigne += "�UNIFAC�"   + UniFac
	        strLigne += "�NATLIG�"   + document.all("DICT�natlig").value
	        strLigne += "�DEVISE�"   + document.all("DICT�devise").value
	        strLigne += "�CPTGEN�"   + CptGen
	
	        strLigne += "�"          + gloremise
	        strLigne += "�DATCDE�"   + document.all("DICT�datcde").value
	        strLigne += "�DATLIV�"   + document.all("DICT�datdep").value
	        strLigne += "�PU-BRUT�"  + nombre_arrondi(document.all("DICT�pu-brut").value,3)
	        strLigne += "�PU-NET�"   + nombre_arrondi(document.all("DICT�pu-brut").value,3)
	        if ( CoefFac == "" || Generique == "O" ) CoefFacLig = 1
	        else                                     CoefFacLig = CoefFac
	        strLigne += "�COEFFAC�" + CoefFacLig
	        if ( CoefCde == "" || Generique == "O" ) strLigne += "�COEFCDE�1"
	        else                                     strLigne += "�COEFCDE�" + CoefCde
	        strLigne += "�CALPRI�PUN"
	        var reg_com = new RegExp ( "\r\n" , "g" )
	        strLigne += "�TEXTE-LIG�" + document.all("DICT�texte-lig").value.replace(reg_com, "CHR(10)")
			strLigne += "�TYPAUX�"    + document.all("DICT�typaux").value
	        strLigne += "�CODAUXGES�" + document.all("DICT�codauxges").value	
		retourExchange = sendExchange ( "<?=$dogExchange?>" , "" , strLigne , "get" , "" ,"GETVAR|NOSCREEN|TRUEDMD","" ,"MAJ-BONLIG�n-ligmaj") 
	        //L'enregistrement de la ligne a �chou�.
	        if ( retourExchange != "OK" ) alert("<?PHP print lect_bin::getLabel ( "mess-prognat_54" , "sl"); ?>")
	        else
	        {
	            //if ( document.all("DICT�natlig").value.toUpperCase() == "N" )
	            //	document.all("DICT�bon-ht").value = getValueFromDataArray( getDataArrayFromRow(1) , "BON-HT"   , false , "" )
		    //on refait un acces aux lignes
		    RspTmp = Rsp
		    inputParameter = inputParameter = "�CODSOC�<?=$c_codsoc?>�MOTCLE�"+document.all("DICT�motcle").value+"�TYPBON�"+document.all("DICT�typbon").value+"�NUMBON�"+document.all("DICT�numbon").value
		    nomfct         = "GET-BONLIG�n-bonlig"
		    retourExchange = sendExchange("<?=$dogExchange?>", "", inputParameter, "get", "", "GETVAR|NOSCREEN|TRUEDMD|MESSAGE=Acces lignes...", "", nomfct) 
		    RspLigne = Rsp;
		    Rsp = RspTmp
		    //fin acces lignes
	
	            puNet = getValueFromDataArray( getDataArrayFromRow(1) , "PU-NET" , false , "" , true )
	            htNet = getValueFromDataArray( getDataArrayFromRow(1) , "HTNET"  , false , "" , true )
	
	            libart1 = document.all("DICT�libart_1").value ;
	            if ( document.all("DICT�texte-lig").value != "" )
	                libart1 = libart1 + " [ " + document.all("DICT�texte-lig").value.replace(  "\r\n" , " " ) + " ]" ;
	
	            if(document.all("rowid").value!="")
	            	ValRowidLig = document.all("rowid").value
	            else
	            	ValRowidLig = "ROWID�"+getValueFromDataArray( getDataArrayFromRow(1) , "ROWID"  , false , "" )
	
	            //r�cup�ration du coef de facturation
	            var remise = nombre_arrondi((document.all("DICT�pu-brut").value - puNet),2)
		    if(UniCde=="")UniCde=" "
		    if(document.all("DICT�env-pla").value.toUpperCase()=="O"){
			return;		
		    }		    
	            if ( ModeLigne == "modif" ){
	                //document.frames("RECAP").MajTable ( "modif" , ValRowidLig , document.all("DICT�chrono").value , document.all("DICT�codart").value , libart1 , document.all("DICT�pu-brut").value , remise , puNet , UniFac , document.all("DICT�qte").value , UniCde , htNet , CoefFacLig , "N" , "" , "" )
	            } else
	            {
	                //document.frames("RECAP").MajTable ( "create" , ValRowidLig , document.all("DICT�chrono").value , document.all("DICT�codart").value , libart1 , document.all("DICT�pu-brut").value , remise , puNet , UniFac , document.all("DICT�qte").value , UniCde , htNet , CoefFacLig , "N" , "" , "" )
	                LastLig  = LastLig + PasLig
	            }
	
	            ModeLigne = "create"
	            document.all("DICT�codart").value       = ""
	            document.all("DICT�libart_1").value     = ""
	            document.all("DICT�qte").value          = ""
	            document.all("DICT�pu-brut").value      = ""
	            document.all("DICT�texte-lig").value	= ""
	            Generique                               = "N"
	            CptGen	                                = ""
	            CodTVA	                                = ""
	            CptTVA	                                = ""
	            Imputa	                                = ""
	            CoefFac	                                = ""
	            CoefCde	                                = ""
	            UniFac                                  = ""
	            UniCde                                  = ""
	            Decima	                                = ""
	            SuiAna	                                = ""
	            GesLibel                                = ""
	            //mise � jour du champ glovalremise
	            document.all("DICT�gloremise").value    = ""        
	            gloremise                               = ""
	
	            analyt = ""
	            //r�cup�ration et mise � jour de l'analytique
	            for ( s = 1 ; s <= nbrseg ; s++ )
	            {
	                //charger la valeur du champ analytique de l'entete
	                analyt        += code_ana[s]
	                codeAnaLig[s]  = code_ana[s]
	                libelAnaLig[s] = libel_ana[s]
	                if ( s != nbrseg )
	                    analyt += "."
	            }
	            //maj du champs analytique
	            document.all("DICT�analytl").value          = analyt
	            document.all("DICT�chrono").value           = LastLig + PasLig
	            document.all('DICT�natlig').value           = "N"
	            document.all('DICTLINK�natlig�libel').value	= "Lignes Normales"
	            document.all("rowid").value=""
	            //enlever la lecture seule
	            document.all("DICT�libart_1").readOnly  = true
	            document.all("DICT�libart_1").className = "dogskininputreadonly"
	            document.all("DICT�chrono").readOnly  = true
	            document.all("DICT�chrono").className = "dogskininputreadonly"
	            
	            if ( codartlie != "" )
	            {
	                document.all("DICT�codart").value = codartlie
	                codartlie = ""
	                dogexchange ( document.all("DICT�codart") , "GET" )
	            }
	            else
	                document.all("DICT�codart").focus()
	            document.all("rowidligne").value = ""
	        }
    	}
}

function LigneAjouter() {
		    if(document.all("DICT�env-pla").value.toUpperCase()=="O"){
			alert("Insertion impossible (statut plateforme)")
			return;		
		    }
	            maxchrono()
	            ModeLigne = "create"
	            document.all("DICT�codart").value       = ""
	            document.all("DICT�libart_1").value     = ""
	            document.all("DICT�qte").value          = ""
	            document.all("DICT�pu-brut").value      = ""
	            document.all("DICT�texte-lig").value	= ""
	            Generique                               = "N"
	            CptGen	                                = ""
	            CodTVA	                                = ""
	            CptTVA	                                = ""
	            Imputa	                                = ""
	            CoefFac	                                = ""
	            CoefCde	                                = ""
	            UniFac                                  = ""
	            UniCde                                  = ""
	            Decima	                                = ""
	            SuiAna	                                = ""
	            GesLibel                                = ""
	            //mise � jour du champ glovalremise
	            document.all("DICT�gloremise").value    = ""        
	            gloremise                               = ""
	
	            analyt = ""
	            //r�cup�ration et mise � jour de l'analytique
	            for ( s = 1 ; s <= nbrseg ; s++ )
	            {
	                //charger la valeur du champ analytique de l'entete
	                analyt        += code_ana[s]
	                codeAnaLig[s]  = code_ana[s]
	                libelAnaLig[s] = libel_ana[s]
	                if ( s != nbrseg )
	                    analyt += "."
	            }
	            //maj du champs analytique
	            document.all("DICT�analytl").value          = analyt
	            document.all("DICT�chrono").value           = LastLig + PasLig
	            document.all('DICT�natlig').value           = "N"
	            document.all('DICTLINK�natlig�libel').value	= "Lignes Normales"
	            document.all("rowid").value=""
	            //enlever la lecture seule
	            document.all("DICT�chrono").readOnly  = true
	            document.all("DICT�chrono").className = "dogskininputreadonly"	            
		    ClearFrameSaisie ( 'SAILIGNE' , false , 'NOCLEAR' )
	            document.all("DICT�libart_1").readOnly  = true
	            document.all("DICT�libart_1").className = "dogskininputreadonly"
	            if ( codartlie != "" )
	            {
	                document.all("DICT�codart").value = codartlie
	                codartlie = ""
	                dogexchange ( document.all("DICT�codart") , "GET" )
	            }
	            else
	                document.all("DICT�codart").focus()
	            document.all("rowidligne").value = ""
	
}

function LigneSupprimer() {
	//on supprime la ligne
	LigneDelete()
}

function LigneDelete(){
	var i
	if ( "<?=$n_etat_visu?>" == 1 ) return
	if ( parent.document.all("mode_bon").value == "visu" ) return
	inputParameter = document.all("rowidligne").value
	event.returnValue = false
	if ( inputParameter == "" ) return
	// Supprimer la ligne en cours ?
	if ( confirm ( "<?PHP print lect_bin::getLabel ( "mess-cdeent_47" , "sl"); ?>" ) ) {
		retourExchange = sendExchange ( "<?=$dogExchange?>" ,  "" , inputParameter , "get" , "" , "DOGADMIN=3|TRUEDMD|GETVAR" , "" , "MAJ-BONLIG�n-ligmaj" ) 
		if ( retourExchange == "OK" ) {
		    //on refait un acces aux lignes
		    RspTmp = Rsp
		    inputParameter = inputParameter = "�CODSOC�<?=$c_codsoc?>�MOTCLE�"+document.all("DICT�motcle").value+"�TYPBON�"+document.all("DICT�typbon").value+"�NUMBON�"+document.all("DICT�numbon").value
		    nomfct         = "GET-BONLIG�n-bonlig"
		    retourExchange = sendExchange("<?=$dogExchange?>", "", inputParameter, "get", "", "GETVAR|NOSCREEN|TRUEDMD|MESSAGE=Acces lignes...", "", nomfct) 
		    RspLigne = Rsp;
		    Rsp = RspTmp
		    //fin acces lignes	
	            puNet = getValueFromDataArray( getDataArrayFromRow(1) , "PU-NET" , false , "" , true )
	            htNet = getValueFromDataArray( getDataArrayFromRow(1) , "HTNET"  , false , "" , true )
	            libart1 = document.all("DICT�libart_1").value ;
	            if ( document.all("DICT�texte-lig").value != "" )
	                libart1 = libart1 + " [ " + document.all("DICT�texte-lig").value.replace(  "\r\n" , " " ) + " ]" ;
	            if(document.all("rowid").value!="")
	            	ValRowidLig = document.all("rowid").value
	            else
	            	ValRowidLig = "ROWID�"+getValueFromDataArray( getDataArrayFromRow(1) , "ROWID"  , false , "" )
	            //r�cup�ration du coef de facturation
	            var remise = nombre_arrondi((document.all("DICT�pu-brut").value - puNet),2)
		    if(UniCde=="")UniCde=" "
	            if ( ModeLigne == "modif" ){
	                //document.frames("RECAP").MajTable ( "modif" , ValRowidLig , document.all("DICT�chrono").value , document.all("DICT�codart").value , libart1 , document.all("DICT�pu-brut").value , remise , puNet , UniFac , document.all("DICT�qte").value , UniCde , htNet , CoefFacLig , "N" , "" , "" )
	            } else
	            {
	                //document.frames("RECAP").MajTable ( "create" , ValRowidLig , document.all("DICT�chrono").value , document.all("DICT�codart").value , libart1 , document.all("DICT�pu-brut").value , remise , puNet , UniFac , document.all("DICT�qte").value , UniCde , htNet , CoefFacLig , "N" , "" , "" )
	                LastLig  = LastLig + PasLig
	            }
		    //on recupere le chrono maxiamo
		    maxchrono()	
	            ModeLigne = "create"
	            document.all("DICT�codart").value       = ""
	            document.all("DICT�libart_1").value     = ""
	            document.all("DICT�qte").value          = ""
	            document.all("DICT�pu-brut").value      = ""
	            document.all("DICT�texte-lig").value	= ""
	            Generique                               = "N"
	            CptGen	                                = ""
	            CodTVA	                                = ""
	            CptTVA	                                = ""
	            Imputa	                                = ""
	            CoefFac	                                = ""
	            CoefCde	                                = ""
	            UniFac                                  = ""
	            UniCde                                  = ""
	            Decima	                                = ""
	            SuiAna	                                = ""
	            GesLibel                                = ""
	            //mise � jour du champ glovalremise
	            document.all("DICT�gloremise").value    = ""        
	            gloremise                               = ""
	
	            analyt = ""
	            //r�cup�ration et mise � jour de l'analytique
	            for ( s = 1 ; s <= nbrseg ; s++ )
	            {
	                //charger la valeur du champ analytique de l'entete
	                analyt        += code_ana[s]
	                codeAnaLig[s]  = code_ana[s]
	                libelAnaLig[s] = libel_ana[s]
	                if ( s != nbrseg )
	                    analyt += "."
	            }
	            //maj du champs analytique
	            document.all("DICT�analytl").value          = analyt
	            document.all("DICT�chrono").value           = LastLig + PasLig
	            document.all('DICT�natlig').value           = "N"
	            document.all('DICTLINK�natlig�libel').value	= "Lignes Normales"
	            document.all("rowid").value=""
	            //enlever la lecture seule
	            document.all("DICT�libart_1").readOnly  = true
	            document.all("DICT�libart_1").className = "dogskininputreadonly"
	            document.all("DICT�chrono").readOnly  = true
	            document.all("DICT�chrono").className = "dogskininputreadonly"
	            if ( codartlie != "" )
	            {
	                document.all("DICT�codart").value = codartlie
	                codartlie = ""
	                dogexchange ( document.all("DICT�codart") , "GET" )
	            }
	            else
	                document.all("DICT�codart").focus()
	            document.all("rowidligne").value = ""			
		} else
            		alert ( "<?PHP print lect_bin::getLabel ( "mess-cdeent_42" , "sl"); ?>" )
            }
}

function LigneCharger(strChrono,position) {
     //cette fonction permet de charger une lgne a l'ecran
     topstop = false
     if(strChrono!=undefined)strChrono = strChrono.trim()
     document.all("rowidligne").value = ""
     Rsp = new Array()
     Rsp[0] = "OK"
     switch(position){
     	case "FIRST" :
     		Rsp[1] = RspLigne[1];
     		break;
     	case "LAST" :
     		Rsp[1] = RspLigne[RspLigne.length-1];
     		break;
     	case "NEXT" :	
     		for(i=1;i<RspLigne.length;i++){
     			Rsp[1] = RspLigne[i]	
     			if(setValueFromKeyword("CHRONO",false,"","",true)==strChrono){
     				if(i<RspLigne.length-1)
     					Rsp[1] = RspLigne[i+1]
     				else {
     					//Rsp[1] = RspLigne[RspLigne.length-1]
     					topstop = true
     					LigneAjouter()
     					return
     				}
     				i=9999	
     			}
     		}
     		break;	
     	case "PREV" :
     		for(i=1;i<RspLigne.length;i++){
     			Rsp[1] = RspLigne[i]	
     			if(setValueFromKeyword("CHRONO",false,"","",true)==strChrono){
     				if(i>1)
     					Rsp[1] = RspLigne[i-1]
     				else
     					Rsp[1] = RspLigne[1]
     				i=9999	
     			}
     		}     	
     		break;
     	default :
     		for(i=1;i<RspLigne.length;i++){
     			Rsp[1] = RspLigne[i]	
     			if(setValueFromKeyword("CHRONO",false,"","",true)==strChrono){
     				i=9999	
     			}
     		}
     		break;    	
     }
     //on affiche la ligne a l'ecran
     setValueFromKeyword("CHRONO",false,"","DICT�chrono",true)
     LigneDisplay()
     topstop = true     
}

//chargement de la page
function InitFrame()
{
    //on charge le tiers selon le fournisseur
    ClearFrameSaisie ( 'SAITIERS' , true , 'NOCLEAR' )
    ClearFrameSaisie ( 'SAILIGNE' , true , 'NOCLEAR' )
    if("<?=$_typaux?>"!="") document.all("DICT�typaux").value="<?=$_typaux?>"
    if("<?=$_codauxges?>"!="") document.all("DICT�codauxges").value="<?=$_codauxges?>"
    if("<?=$_codauxgeslibel?>"!="") document.all("DICT�adresse_1").value="<?=$_codauxgeslibel?>"
    provenance="<?=$_provenance?>"

    //si pas de magasin
    if("<?=$magasaut?>"==""){
	    str_val = "CODSOC�<?=$c_codsoc?>�ETABLI�<?=$c_codetb?>";
	    retourExchange  = sendExchange ( "<?=$dogExchange?>" ,  "" , str_val  , "F10" , "" ,"GETVAR|NOSCREEN|TRUEDMD","FIELD=MAGASI|LIBEL" ,"F10-MAGASI�n-magasi") 
	    if ( retourExchange == "OK" ) {
		    //r�cuperer des zones
		    document.all("DICT�magasi").value = getValueFromDataArray( getDataArrayFromRow(1) ,"MAGASI" , false , "" )
	    }    
    }

    //en livraison et reception precharge datcde et datliv a today
    if("<?=$_motcle?>"=="ACH"||"<?=$_motcle?>"=="VTE"){
    	document.all("DICT�datcde").value="<?=date("d/m/Y")?>"
    	document.all("DICT�datdep").value="<?=date("d/m/Y")?>"
    } 
    if("<?=$_motcle?>"=="ACC"){
    	document.all("DICT�datdep").value=calcDateOuvrable("<?=date("d/m/Y")?>",1)
    } 
    //pas de tiers en RGS
    if('<?=$_typcom?>'=="RGS"){
    	zonscreenget("�typaux��2�codauxges��2�typregul��1�pu-brut��0")
    	document.all("DICT�pu-brut").style.top="117px"
    	document.all("DICTAB�pu-brut").style.top="119px"
    	document.all("stk").style.visibility=""
    }
    //le parametrage menu prend le dessus
    zonscreenget();    
    //f = document.getElementById("RECAP").contentWindow;    
    //si pas de saisie du prix on cache la colonne HT
    if(document.all("DICT�pu-brut").parentElement.style.visibility=="hidden") {
    	//F.Document.All("Saisieheader").cells[9].style.width="0px"
    	document.all("stk").style.visibility=""
    } 

    LastLig = 0
    PasLig  = 10
    document.all("DICT�chrono").value            = LastLig + PasLig
    document.all('DICT�natlig').value            = "N"
    document.all('DICTLINK�natlig�libel').value	= "Lignes Normales"
    document.all("DICT�numbon").focus()
    window.scrollTo(0,0)
    posi = 1
    if("<?=$_numbon?>"!=""){
    	document.all("DICT�numbon").value="<?=$_numbon?>"
    	dogexchange(document.all("DICT�numbon"))	
    }
}

function calcDateOuvrable(date_now, nbJours){
	//retourne une date apres ajout d'un nombre de jours ouvrables
	var date_now_annee = Entry(3,date_now,"/");
	var date_now_mois = Entry(2,date_now,"/");
	var date_now_jour = Entry(1,date_now,"/");
	//**init. des compteurs**//
	var cpt_i = 0;
	var cpt_k = 0;
	//**init. des tableaux r�cup�rant les jours feries de l'annee en cours et de l'annee suivante.**//
	//var tab_1=new Array;
	//var tab_2=new Array;
	//tab_1=JoursFeries(parseInt(date_now_annee));
	//tab_2=JoursFeries(parseInt(date_now_annee)+1);
	for(cpt_i=0; cpt_i <= nbJours ; cpt_i++) {
		var date_eval = new Date();
		date_eval.setYear(parseInt(date_now_annee))
		date_eval.setMonth(parseInt(date_now_mois*1)-1)
		date_eval.setDate(parseInt(date_now_jour*1)+cpt_i)
		var day_date_eval = date_eval.getDay();
		if((day_date_eval == 6) || (day_date_eval == 0)) {
			nbJours++;
			/*for(cpt_k = 0; cpt_k <13; cpt_k++){
				if(date_eval.getMonth() == tab_1[cpt_k].getMonth() && date_eval.getFullYear() == tab_1[cpt_k].getFullYear() && date_eval.getDate() == tab_1[cpt_k].getDate()){
					nbJours--;
					break;
				}
				if(date_eval.getMonth() == tab_2[cpt_k].getMonth() && date_eval.getFullYear() == tab_2[cpt_k].getFullYear() && date_eval.getDate() == tab_2[cpt_k].getDate()){
					nbJours--;
					break;
				}
			}*/
		}
	}
	//if(nbJours==0)date_eval = new Date(date_now_annee, date_now_mois, date_now_jour);
	if(parseInt(date_eval.getMonth()+1)<10)
		strMois="0"+(date_eval.getMonth()+1)
	else
		strMois=date_eval.getMonth()+1
	if(parseInt(date_eval.getDate())<10)
		strJours="0"+date_eval.getDate()
	else
		strJours=date_eval.getDate()
	return strJours+"/"+strMois+"/"+date_eval.getFullYear();
}

function ArticleInfo() {
	//affiche les informations li�es � l'article en cours
	dogFonction="GET-ARTICL�n-articl";
	input_param ="�CODSOC�<?=$c_codsoc?>�MOTCLE�<?=$_motcle?>�TYPBON�<?=$_typcom?>�CODART�"+document.all("DICT�codart").value.trim()+"�MAGASI�"+document.all("DICT�magasi").value.trim();
	retourExchange = sendExchange ( "<?=$dogExchange?>" ,    strId  , input_param , "GET" , "" , "GETVAR|NOSCREEN|TRUEDMD" ,"",dogFonction) ;
	if (retourExchange != "OK") {
		initbufferscreen();
		document.all(strId).focus();
		return ;
	 }
	document.all("ARTICLE").style.visibility=""
 	setValueFromKeyword ( "CODART"  , false , "" , "DICT�codart2"  , true);
	setValueFromKeyword ( "LIBART_1"  , false , "" , "DICT�libart_12"  , true);
	setValueFromKeyword ( "PROVENANCE"  , false , "" , "DICT�provenance2"  , true);
	setValueFromKeyword ( "CODE-BLOCAGE"  , false , "" , "DICT�code-blocage2"  , true);
	if(Entry(2,setValueFromKeyword ( "STATBLOC"  , false , "" , ""  , true),"|")=="1"){
	 	document.all("DICT�aut12").value="N"
	} else {
		document.all("DICT�aut12").value="O"
	}
	if(Entry(3,setValueFromKeyword ( "STATBLOC"  , false , "" , ""  , true),"|")=="1"){
	 	document.all("DICT�aut22").value="N"
	} else {
	 	document.all("DICT�aut22").value="O"
	}
	//pour le tarif
	for(i=1;i<=NumEntries(setValueFromKeyword ( "CODTAR"  , false , "" , ""  , true),"|");i++) {
	 	document.all("DICT�codtar_"+i).value=Entry(i,setValueFromKeyword ( "CODTAR"  , false , "" , ""  , true),"|")
		document.all("DICT�dattar_"+i).value=Entry(i,setValueFromKeyword ( "DATTAR"  , false , "" , ""  , true),"|")
		document.all("DICT�datfin_"+i).value=Entry(i,setValueFromKeyword ( "DATFIN"  , false , "" , ""  , true),"|")
		document.all("DICT�pu-brut_"+i).value=Entry(i,setValueFromKeyword ( "PU-BRUT"  , false , "" , ""  , true),"|").trim()
	}
	//pour le stock
	document.all("DICT�stophy").value=setValueFromKeyword ( "STOPHY"  , false , "" , ""  , true).trim();
	document.all("DICT�encvte").value=setValueFromKeyword ( "ENCVTE"  , false , "" , ""  , true).trim();
	document.all("DICT�dispon").value=setValueFromKeyword ( "DISPON"  , false , "" , ""  , true).trim();
	document.all("DICT�encach").value=setValueFromKeyword ( "ENCACH"  , false , "" , ""  , true).trim();
	document.all("DICT�theori").value=setValueFromKeyword ( "THEORI"  , false , "" , ""  , true).trim();
	//pour les statistiques
	for(i=1;i<=NumEntries(setValueFromKeyword ( "MOIS"  , false , "" , ""  , true),"|");i++) {
		document.all("DICT�mois_"+i).value=Entry(i,setValueFromKeyword ( "MOIS"  , false , "" , ""  , true),"|")
		document.all("DICT�quantite_"+i).value=Entry(i,setValueFromKeyword ( "QUANTITE"  , false , "" , ""  , true),"|")
		document.all("DICT�valeur_"+i).value=Entry(i,setValueFromKeyword ( "VALEUR"  , false , "" , ""  , true),"|")
	}
	//pour les taxes
	for(i=1;i<=4;i++) {
		document.all("DICT�taxadd-code_"+i).value=Entry(i,setValueFromKeyword ( "TAXADD-CODE_"+i  , false , "" , ""  , true),"|")
		document.all("DICTLINK�taxadd-code_"+i+"�LIBEL").value=Entry(i,setValueFromKeyword ( "LIBEL", true, "TAXADD-CODE_"+i, "", true),"|")
		document.all("DICT�taxadd-pu_"+i).value=Entry(i,setValueFromKeyword ( "TAXADD-PU_"+i  , false , "" , ""  , true),"|")
		document.all("DICT�taxadd-tva_"+i).value=Entry(i,setValueFromKeyword ( "TAXADD-TVA_"+i  , false , "" , ""  , true),"|")
	} 			 
	//on se gere l'affichage des lignes
	if(document.all("DICT�codtar_1").value=="")
		document.all("div1").style.visibility="hidden"
	else
		document.all("div1").style.visibility=""
		
	if(document.all("DICT�codtar_2").value=="")
		document.all("div2").style.visibility="hidden"
	else
		document.all("div2").style.visibility=""
		
	if(document.all("DICT�codtar_3").value=="")
		document.all("div3").style.visibility="hidden"
	else
		document.all("div3").style.visibility=""
		
	if(document.all("DICT�codtar_4").value=="")
		document.all("div4").style.visibility="hidden"
	else
		document.all("div4").style.visibility=""

	if(document.all("DICT�mois_1").value=="")
		document.all("div6").style.visibility="hidden"
	else
		document.all("div6").style.visibility=""
		
	if(document.all("DICT�mois_2").value=="")
		document.all("div7").style.visibility="hidden"
	else
		document.all("div7").style.visibility=""
		
	if(document.all("DICT�mois_3").value=="")
		document.all("div8").style.visibility="hidden"
	else
		document.all("div8").style.visibility=""
		
	if(document.all("DICT�taxadd-code_1").value=="")
		document.all("div9").style.visibility="hidden"
	else
		document.all("div9").style.visibility=""
		
	if(document.all("DICT�taxadd-code_2").value=="")
		document.all("div10").style.visibility="hidden"
	else
		document.all("div10").style.visibility=""
		
	if(document.all("DICT�taxadd-code_3").value=="")
		document.all("div11").style.visibility="hidden"
	else
		document.all("div11").style.visibility=""
		
	if(document.all("DICT�taxadd-code_4").value=="")
		document.all("div12").style.visibility="hidden"
	else
		document.all("div12").style.visibility=""
		
	j=1
	for(i=1;i<=12;i++) {
		if(document.all("div"+i).style.visibility==""){
			document.all("div"+i).style.top=""+((i-j)*20+3)+"px";
		} else {
			j = j + 1;		
		}
	}

}

//gestion des touches de raccourcis
function TrapKey()
{
}

function zonscreenget(strscreen)
{
 	 //--> cette fonction permet de precharger l'ecran a partir de valeurs par defaut
	 //   du type �champ�valeur�option�champ�valeur�option
	 //    option = 0 ou '' > la zone not enable   
	 //    option = 1 > rend la zone enable
	 //    option = 2 > invisible   
	 //    option = 3 la zone est obligatoire et enable
	 //    option = 4 inversion de la position des champs sur la feuille 2 � 2
	 //    option = 5 valeurs interdites separateur ',' 
	 //    option = 6 valeurs autorisees separateur ','
	 //    option = 8 enable sans toucher au contenu
	 //    option = 9 not enable sans toucher au contenu
	 //    option = 10 enable sans toucher au contenu et sans toucher au parent
	 if(strscreen==undefined)strscreen=unescape("<?=$SCREEN?>")
	 while(strscreen.match("�")){
		strscreen=strscreen.replace("�","�");
	 }
	 while(strscreen.match("�")){
		strscreen=strscreen.replace("�","�");
	 }
	 // On boucle sur les champs
	 Temp = strscreen.split("�")
	 var i = 0 ;
	 for ( i=0 ; i<strscreen.split("�").length ; i++)
	 {	// On charge l'ecran avec les valeurs par defaut
		 while(Temp[i].match("@")){
			Temp[i]=Temp[i].replace("@","�");
		 }	 	 	
	 	
	 	if(Entry(3,Temp[i],"�")!="4" & Entry(3,Temp[i],"�")!="8" & Entry(3,Temp[i],"�")!="9" & Entry(3,Temp[i],"�")!="10")  
	 		if(document.all("DICT�" + Entry(1,Temp[i],"�"))!=undefined)document.all("DICT�" + Entry(1,Temp[i],"�")).value = Entry(2,Temp[i],"�");
	 	// On redefini si besoin des propri�t�s de la zone
	 	switch ( Entry(3,Temp[i],"�"))
		{
			case "0":  case "9"://zone bloqu�e
				document.all("DICT�" + Entry(1,Temp[i],"�")).readOnly = true;
				//document.all("DICT�" + Entry(1,Temp[i],"�")).disabled=true;
				if(document.all("DICT�" + Entry(1,Temp[i],"�")).tagName!="SELECT") 
					document.all("DICT�" + Entry(1,Temp[i],"�")).className = 'dogskininputreadonly';
				else
					document.all("DICT�" + Entry(1,Temp[i],"�")).disabled=true;
				document.all("DICT�" + Entry(1,Temp[i],"�")).className = 'dogskininputreadonly';
				break;
			case "1": case "8": //visible (DIV)
				document.all("DICT�" + Entry(1,Temp[i],"�")).parentElement.style.visibility = "";
				if(document.all("DICT�" + Entry(1,Temp[i],"�")).tagName!="SELECT") 
					document.all("DICT�" + Entry(1,Temp[i],"�")).className="dogskininput";
				else
					document.all("DICT�" + Entry(1,Temp[i],"�")).disabled=false;
				document.all("DICT�" + Entry(1,Temp[i],"�")).className="dogskininput";
				document.all("DICT�" + Entry(1,Temp[i],"�")).readOnly = false;
				break;
			case "2": //zone cach�e (DIV)
				//On remonte pour cela au div
				document.all("DICT�" + Entry(1,Temp[i],"�")).parentElement.style.visibility = "hidden";
				break;
			case "3": //zone obligatoire
				document.all("DICT�" + Entry(1,Temp[i],"�")).parentElement.style.visibility = "";
				document.all("DICT�" + Entry(1,Temp[i],"�")).className="dogskininput";
				document.all("DICT�" + Entry(1,Temp[i],"�")).readOnly = false;			
				document.all("DICT�" + Entry(1,Temp[i],"�")).isobli = "1";
				break;
			case "4": //inversion de la position des champs sur la feuille 2 � 2
				top1 = document.all("DICT�" + Entry(1,Temp[i],"�")).parentElement.style.top ;
				left1 = document.all("DICT�" + Entry(1,Temp[i],"�")).parentElement.style.left ;
				tab1 = document.all("DICT�" + Entry(1,Temp[i],"�")).tabIndex ;
				top2 = document.all("DICT�" + Entry(2,Temp[i],"�")).parentElement.style.top ;
				left2 = document.all("DICT�" + Entry(2,Temp[i],"�")).parentElement.style.left ;
				tab2 = document.all("DICT�" + Entry(2,Temp[i],"�")).tabIndex ;
				document.all("DICT�" + Entry(1,Temp[i],"�")).parentElement.style.top = top2 ;
				document.all("DICT�" + Entry(1,Temp[i],"�")).parentElement.style.left = left2 ;
				document.all("DICT�" + Entry(1,Temp[i],"�")).tabIndex = tab2 ;
				document.all("DICT�" + Entry(2,Temp[i],"�")).parentElement.style.top = top1 ;
				document.all("DICT�" + Entry(2,Temp[i],"�")).parentElement.style.left = left1 ;
				document.all("DICT�" + Entry(2,Temp[i],"�")).tabIndex = tab1 ;
				break;
			case "10": //sans le div
				if(document.all("DICT�" + Entry(1,Temp[i],"�")).tagName!="SELECT") 
					document.all("DICT�" + Entry(1,Temp[i],"�")).className="dogskininput";
				else
					document.all("DICT�" + Entry(1,Temp[i],"�")).disabled=false;
				document.all("DICT�" + Entry(1,Temp[i],"�")).className="dogskininput";
				document.all("DICT�" + Entry(1,Temp[i],"�")).readOnly = false;
				break;				
		}
	 }
	 strscreen=undefined;
} //Fonction qui charge l'ecran

function action2() {
	//on rend le pave des actions visible
	if(posi==1)document.all("EXECUTION").style.top="10px"
	if(posi==2)document.all("EXECUTION").style.top="329px"
	document.all("EXECUTION").style.visibility=""	
}
posi=1
function affichage() {
	if(posi==1){
		window.scrollTo(0,334)	
		posi = 2
	} else {
		window.scrollTo(0,0)
		posi = 1
	}
	
}

function controleFrappe(car)
{
	//f=window.event ;
	switch (car)
	{
	        //Touche echap
	        case 27:
	             	bonquitter();
        		break;
		case 112:
			document.onhelp = function() { return (false); }
			window.onhelp = function() { return (false); }
			event.returnValue = "" ;
			event.keyCode = 0;
			event.returnValue = false;
			event.cancelBubble = true;
			BonRechercherDernier()				
			break;
		//Touche F2
		case 113:
			affichage()
			break;
		
		//Touche F4
		case 115:
			if(document.all("ARTICLE").style.visibility==""){
				document.all("ARTICLE").style.visibility = "hidden"
				return	
			}
			if(document.all("EXECUTION").style.visibility==""){
				document.all("EXECUTION").style.visibility = "hidden"
				return
			}			
			bonquitter();
			break;
		//F3 Touche F3 valide le bon
		case 114:
			document.all('DICT�valide').value='O';
			BonValider();
			BonFin();
			document.all('EXECUTION').style.visibility='hidden'
			break;
		case 119:
			action2()
			break;
		case 38:
			if(event.ctrlKey == true )
				LigneCharger(document.all("DICT�chrono").value.trim(),"FIRST")
			else
				LigneCharger(document.all("DICT�chrono").value.trim(),"PREV")
			break;
		case 40:
			if(event.ctrlKey == true )
				LigneCharger(document.all("DICT�chrono").value.trim(),"LAST")
			else
				LigneCharger(document.all("DICT�chrono").value.trim(),"NEXT")
			break;
    		case 123 : case 86 : 
    			if(event.altKey == true )  ToucheAltV()
			break;			
		default :
			//Rediriger vers la gestion des touches du tableau
			//fromkeybord () ;
			break ; 					
	} //fin switch
 } // fin fonction de controle frappe

function bonquitter ()
{	
	top.window.returnValue = "" ;
	top.window.close();
} //Fin de la fonction qui quitte le bon

function loadFocusIn (){}

try{window.detachEvent ( "onunload" , glo_winOpen )}catch(ex){}
document.title="<?=$_libel_menu ?>"

</SCRIPT>

<HTML>
<BODY class=dogskinbody onload="InitFrame()" onkeydown=controleFrappe(event.keyCode)>
<form>
<input type="hidden" id="mode_bon">
<INPUT ID="DICT�valide" TYPE=HIDDEN  ctrl_input="||||1" >
<INPUT ID="DICT�magasi" TYPE=HIDDEN  value="<?=$magasaut?>" ctrl_input="||||1" >
<INPUT ID="DICT�natbon" TYPE=HIDDEN  value="<?=$_natbon?>" ctrl_input="||||1" >
<INPUT ID="DICT�adresse_2" TYPE=HIDDEN  ctrl_input="||||1" >
<INPUT ID="DICT�adresse_3" TYPE=HIDDEN  ctrl_input="||||1" >
<INPUT ID="DICT�adresse_4" TYPE=HIDDEN  ctrl_input="||||1" >
<INPUT ID="DICT�codtar" TYPE=HIDDEN  ctrl_input="||||1" >
<INPUT ID="DICT�adresse_5" TYPE=HIDDEN  ctrl_input="||||1" >
<INPUT ID="DICT�env-pla" TYPE=HIDDEN  ctrl_input="||||1" >
<INPUT ID="rowid" TYPE=HIDDEN >
<INPUT ID="rowidligne" TYPE=HIDDEN >
<DIV id='SCROLL' >
<!-- balise de l'ombre -->
<DIV class=dogskinshapeshadowstyle style="width:315px;height:75px;top:5px;left:5px" ></DIV>
<DIV id="SAIHEADER" class=dogskinshapestyle style="width:315px;height:75px;top:2px;left:2px" >
    <INPUT ID="DICT�motcle" TYPE=HIDDEN  value='<?=$_motcle?>' ctrl_input="||||1">
	<!--//Saisie du typcom //-->
    <INPUT ID="DICT�typbon" CLASS=dogskininputreadonly readonly style="top:3px;left:6px;width:40px;;font-size:13pt" value='<?=$_typcom?>' tabIndex=1>
    <!-- //Saisie du num�ro de bon// -->
    <P class=dogskinbodyfont style="left:50px;top:5px;width:300px;font-size:11pt">N� Bon </P>
    <INPUT ID="DICT�numbon" CLASS=dogskininput  style="top:3px;left:125px;width:150px;font-size:13pt" onkeypress="javascript:if(event.keyCode == 13) dogexchange(this)" onkeydown="javascript:if(event.keyCode == 120 || event.keyCode == 121 ) BonRechercher()" onfocus="this.select()" MAXLENGTH=8 tabIndex=2>
    <IMG SRC=<?=$img["nav_10"]?> F10="DICT�numbon" class=dogskinf10 style="top:7px;left:278px" onclick="javascript:BonRechercher();">
    <!-- rappel devise et total hors taxe -->
    <INPUT CLASS=dogskininputreadonly readonly ID="DICT�devise" style="top:33px;left:6px;width:55px;font-size:13pt" value="<?=$n_devise?>">	
    <INPUT CLASS=dogskininputreadonly readonly ID="DICT�bon-ht" style="top:33px;left:65px;width:95px;font-size:13pt">
</DIV>
<P class=dogskinbodyfont ID="but_scroll" style="top:35px;left:170px;width:160;text-decoration:underline;font-weight:600;font-size:13pt;cursor:hand" onclick="affichage()">Haut/Bas [F2]</P>

<!-- balise de l'ombre -->
<DIV class=dogskinshapeshadowstyle style="width:315px;height:140px;top:85px;left:5px" ></DIV>
<DIV id="SAITIERS" class=dogskinshapestyle style="width:315px;height:140px;top:82px;left:2px" >
    <!--//Saisie du typaux//-->
    <DIV>
	    <P class=dogskinbodyfont style="top:5px;left:3px;width=300px;font-size:11pt">Aux </P>
	    <INPUT ID="DICT�typaux" CLASS=dogskininput style="top:3px;left:40px;width:70px;font-size:13pt" onkeypress="javascript:if(event.keyCode == 13)dogexchange(this)" onkeydown="javascript:if(event.keyCode == 121)dogexchange(this)" onfocus="this.select()" tabIndex=3>
	    <IMG SRC=<?=$img["nav_10"]?> F10="DICT�typaux" class=dogskinf10 style="top:7px;left:112px" onclick="javascript:dogexchange (this)">
	    <!-- //Saisie du codaux// -->
	    <INPUT ID="DICT�codauxges" CLASS=dogskininput style="top:3px;left:135px;width:120px;font-size:13pt" onkeypress="javascript:if(event.keyCode == 13)dogexchange(this)" onkeydown="javascript:if(event.keyCode == 121)dogexchange(this)" onfocus="this.select()" tabIndex=4>
	    <IMG SRC=<?=$img["nav_10"]?> F10="DICT�codauxges" class=dogskinf10 style="top:7px;left:258px" onclick="javascript:dogexchange(this)">
	    <INPUT CLASS=dogskininputreadonly readonly ID="DICT�adresse_1" value="<?=$_adresse1?>" style="top:33px;left:6px;width:290px;font-size:13pt" onkeypress="javascript:if(event.keyCode == 13){}" onfocus="this.select()">
    </DIV>
    <!-- type de r�gul -->
    <DIV  style="visibility:hidden">
	<P class=dogskinbodyfont style="top:5px;top:3px;width:200px;font-size:11pt"><?PHP print lect_bin::getLabel ( "typregul" , "sl"); ?> </P>
	<INPUT ID="DICT�typregul" TYPE=TEXT CLASS=dogskininput style="top:3px;left:125px;width:100px;font-size:13pt"  ctrl_input="@@@||0||" onblur=ctrlIdValue('DICT�typregul') onkeypress="javascript:if(event.keyCode == 13)dogexchange(this)" onkeydown="javascript:if(event.keyCode == 121)dogexchange(this)" nature="CHAMP" tabIndex=5 isobli="1">
	<IMG SRC=<?=$img["nav_10"]?> F10="DICT�typregul" class=dogskinf10 style="top:7px;left:228px" onclick="javascript:dogexchange (this)">
        <INPUT CLASS=dogskininputreadonly readonly ID="DICTLINK�typregul�libel"   style="left:40px;top:33px;width:270px;font-size:13pt">
    </DIV>	
    <!-- date de commande -->
    <DIV>
	    <P class=dogskinbodyfont style="top:65px;left:3px;width=300px;font-size:11pt">DatCde </P>
	    <INPUT ID="DICT�datcde" CLASS=dogskininput style="top:62px;left:60px;width:100px;font-size:13pt" onchange="MajDatLoc(this,'DICT�datdep')" onkeypress="if ( event.keyCode == 13 ) MajDatLoc(this,'DICT�datdep')" onfocus="this.select()" value="<?=$datJour?>" tabIndex=7>
    </DIV>
    <!-- date de livraison -->
    <DIV>
	    <P class=dogskinbodyfont style="top:65px;left:160px;width=300px;font-size:11pt">DatLiv </P>
	    <INPUT ID="DICT�datdep" CLASS=dogskininput style="top:62px;left:210px;width:100px;font-size:13pt" onchange="MajDatLoc(this,'DICT�analyt')" onkeypress="if ( event.keyCode == 13 ) MajDatLoc(this,'DICT�analyt')" onfocus="this.select()" tabIndex=8>
    </DIV>
    <!-- reference du bon -->
    <DIV style="visibility:hidden">
	    <P class=dogskinbodyfont style="top:90px;left:3px;width=300px;font-size:11pt">Ref Cde </P>
	    <INPUT ID="DICT�reftie" CLASS=dogskininput style="top:91px;left:100px;width:165px;font-size:13pt" onchange="CtrlSaiZone(this)" onkeypress="if ( event.keyCode == 13 ) CtrlSaiZone(this)" onfocus="this.select()" value="" tabIndex=9>
    </DIV>
    <!-- analytique -->
    <INPUT ID="DICT�analyt" CLASS=dogskininput style="top:23px;left:610px;width:340px;visibility:hidden" onkeydown="javascript:if(event.keyCode == 121)setana()" onfocus="this.select();setana();" tabIndex=10>
</DIV>
<!-- enregistrer entete -->
<P class=dogskinbodyfont ID="but_quit" style="top:192px;left:5px;width:160;text-decoration:underline;font-weight:600;font-size:13pt;cursor:hand" onclick="bonquitter()">Quitter [F4]</P>
<P class=dogskinbodyfont ID="but_valid" style="top:192px;left:250px;width:160;text-decoration:underline;font-weight:600;font-size:13pt;cursor:hand" onclick="BonValider()">Enreg.</P>

<!-- balise de l'ombre -->
<DIV class=dogskinshapeshadowstyle style="width:315px;height:225px;top:327px;left:5px" ></DIV>
<DIV id="SAILIGNE" class=dogskinshapestyle style="width:315px;height:225px;top:324px;left:2px">
    <!-- Chrono ligne -->
    <P class=dogskinbodyfont style="top:5px;left:3px;width:300px;font-size:11pt">N� </P>
    <INPUT ID="DICT�chrono" CLASS=dogskininputreadonly readonly style="top:3px;left:40px;width:30px;font-size:13pt" onkeypress="javascript:if(event.keyCode == 13)document.all('DICT�natlig').focus()" onfocus="this.select()">
    <!-- Nature ligne -->
    <P class=dogskinbodyfont style="top:5px;left:75px;width:300px;font-size:11pt">Condt  </P>
    <INPUT ID="DICT�natlig" CLASS=dogskininput style="top:3px;left:120px;width:20px;font-size:13pt;visibility:hidden" onkeypress="javascript:if(event.keyCode == 13)dogexchange(this)" onkeydown="javascript:if(event.keyCode == 121)dogexchange(this)" onfocus="this.select()">
    <IMG SRC=<?=$img["nav_10"]?> F10="DICT�natlig" class=dogskinf10 style="top:8px;left:145px;font-size:13pt;visibility:hidden" onclick="javascript:dogexchange (this)">
    <INPUT CLASS=dogskininputreadonly readonly ID="DICTLINK�natlig�libel" style="left:170px;top:3px;width:105px;visibility:hidden">
    <!-- article -->
    <P class=dogskinbodyfont style="top:35px;left:3px;width:300px;font-size:11pt">Article </P>
    <INPUT ID="DICT�codart" CLASS=dogskininput style="top:33px;left:40px;width:100px;font-size:13pt" onkeypress="javascript:if(event.keyCode == 13)dogexchange(this)" onkeydown="javascript:if(event.keyCode == 121)dogexchange(this)"  onfocus="this.select()">
    <IMG SRC=<?=$img["nav_10"]?> F10="DICT�codart" class=dogskinf10 style="top:38px;left:145px" onclick="javascript:dogexchange (this)">
    <INPUT CLASS=dogskininputreadonly ID="DICT�libart_1" style="left:40px;top:63px;width:270px;font-size:13pt" onkeypress="javascript:if(event.keyCode==13&&GesLibel==1)document.all('DICT�qte').focus()" onfocus="this.select()">
    <INPUT CLASS=dogskininputreadonly readonly ID="DICT�surcondit" style="left:122px;top:3px;width:50px;font-size:13pt">
    <INPUT CLASS=dogskininputreadonly readonly ID="DICT�typ-surcondit" style="left:176px;top:3px;width:20px;font-size:13pt">
    <INPUT CLASS=dogskininputreadonly readonly ID="DICT�statbloc" style="left:200px;top:3px;width:85px;font-size:13pt">
    <INPUT CLASS=dogskininputreadonly readonly ID="DICT�code-blocage" style="left:290px;top:3px;width:20px;font-size:13pt">
    <INPUT CLASS=dogskininputreadonly readonly ID="DICT�decima" style="left:280px;top:33px;width:20px;font-size:13pt;visibility:hidden">
    <INPUT CLASS=dogskininputreadonly readonly ID="DICT�par-comb" style="left:290px;top:33px;width:20px;font-size:13pt">
    <INPUT CLASS=dogskininputreadonly readonly ID="DICT�coefcde" style="left:290px;top:33px;width:20px;font-size:13pt;visibility:hidden">
    <INPUT CLASS=dogskininputreadonly readonly ID="DICT�mincde-art" style="left:280px;top:3px;width:20px;visibility:hidden">
    <!-- quantit� -->
    <P class=dogskinbodyfont style="top:94px;left:3px;width:300px;font-size:11pt">Qt� </P>
    <INPUT ID="DICT�qte" CLASS=dogskininput style="top:92px;left:40px;width:80px;font-size:13pt" onchange="CtrlSaiZone(this)" onkeypress="javascript:if(event.keyCode == 13){CtrlSaiZone(this)}" onfocus="this.select()">
    <!-- prix unitaire brut -->
    <DIV>
    	<P ID="DICTAB�pu-brut" class=dogskinbodyfont style="top:94px;left:125px;width:300px;font-size:11pt"><?PHP print lect_bin::getLabel ( "pu-brut" , "cl"); ?> </P>
	<INPUT ID="DICT�pu-brut" CLASS=dogskininput style="top:92px;left:220px;width:90px;font-size:13pt" onkeypress="javascript:if(event.keyCode == 13)CtrlSaiZone(this)" onfocus="this.select()">
    </DIV>
    <!-- prix unitaire brut -->
    <DIV id="stk" style="visibility:hidden">
    	<P class=dogskinbodyfont style="top:35px;left:170px;width:300px;font-size:11pt">St </P>
	<INPUT ID="DICT�stophy2" CLASS=dogskininput style="top:33px;left:195px;width:90px;font-size:13pt" >
        <P class=dogskinbodyfont style="top:94px;left:145px;width:100px;font-size:11pt">Qv </P>
        <INPUT ID="DICT�quantite_4" TYPE=TEXT CLASS=dogskininput style="top:92px;left:175px;width:135px;font-size:13pt"   >
    </DIV>
    <!-- commentaires lignes -->
    <TEXTAREA ID="DICT�texte-lig" class=dogskininput style="position:absolute;left:660px;top:3px;height:57px;width:310px;visibility:hidden"></TEXTAREA>
    <!-- analytique -->
    <INPUT ID="DICT�analytl" CLASS=dogskininput title="<?PHP print lect_bin::getLabel ( "mess-cdeent_14" , "sl"); ?>" style="top:63px;left:110px;width:340px;visibility:hidden" onfocus="this.select();open_ana();" onkeydown="javascript:if(event.keyCode == 121)open_ana()">
    <IMG SRC=<?=$img["nav_10"]?> class=dogskinf10 style="top:62px;left:455px;visibility:hidden" onclick="javascript:open_ana()">
    <!-- remise -->
    <P class=dogskinbodyfont style="top:65px;left:500;width:300px;visibility:hidden"><?PHP print lect_bin::getLabel ( "mess-prognat_89" , "sl"); ?> </P>
    <INPUT ID="DICT�gloremise" CLASS=dogskininput title="<?PHP print lect_bin::getLabel ( "mess-cdeent_16" , "sl"); ?>" style="top:63px;left:610px;width:340px;visibility:hidden" onfocus="this.select()" onkeydown="javascript:if(event.keyCode == 121)addRemise()" onkeypress="javascript:if(event.keyCode == 13){document.all('LienAccept').focus();document.all('LienAccept').style.color = '#00FF00';}">
    <IMG SRC=<?=$img["nav_10"]?> class=dogskinf10 style="top:62px;left:955px;visibility:hidden" onclick="javascript:addRemise()">
</DIV>

<P class=dogskinbodyfont style="top:485px;left:3px;font-size:11pt">Ligne Prev/Suiv</P>
<P class=dogskinbodyfont ID="but_scrolL" style="top:485px;left:123px;width:60;font-weight:600;font-size:13pt;;cursor:hand" onclick="">[ /\ ]</P>
<P class=dogskinbodyfont ID="but_scrolL" style="top:485px;left:183px;width:60;font-weight:600;font-size:13pt;;cursor:hand" onclick="">[ \/ ]</P>

<P class=dogskinbodyfont ID="but_act" style="top:515px;left:3px;width:110;text-decoration:underline;font-weight:600;font-size:13pt;;cursor:hand" onclick="action2()">Action [F8]</P>
<P class=dogskinbodyfont ID="but_imp" style="top:515px;left:3px;width:110;text-decoration:underline;font-weight:600;font-size:13pt;;cursor:hand;visibility:hidden" onclick="Imprimer()">Impri</P>
<P class=dogskinbodyfont ID="but_suppr" style="top:515px;left:60px;width:110;text-decoration:underline;font-weight:600;font-size:13pt;cursor:hand;visibility:hidden" onclick="BonSupprimer()">Supp Bon</P>
<P class=dogskinbodyfont ID="LienAccept" style="top:515px;left:125px;width:110;text-decoration:underline;font-weight:600;font-size:13pt;cursor:hand" onclick="LigneValider()">Accept Lig</P>
<P class=dogskinbodyfont ID="LienFin" style="top:515px;left:245px;width:110;text-decoration:underline;font-weight:600;font-size:13pt;cursor:hand" onclick="BonFin()">Fin bon</P>

<A ID="link-visu"  TARGET="_blank" style="top:5px;left:0px" ></A>

<!--//Pav� des actions possibles -->	
<DIV ID="EXECUTION" style="position:absolute;left:5px;height:250px;top:235px;width:280px;visibility:hidden" onmousedown="beginDrag(this,event);" onmousemove="drag(event);" onmouseup="endDrag();">
	<DIV class=dogskinshapeshadowstyle style="position:absolute;left:13px;height:210px;top:3px;width:280px" ></DIV>
	<DIV  class=dogskinshapestyle style="position:absolute;left:10px;height:210px;top:0px;width:280px">
		<IMG SRC='<?="http://".$GLOBALS["dealgateRelPath"]."/site/skin/std/img/close.jpg" ?>' class=dogskinf10 style="top:5px;left:260px" onclick="document.all('EXECUTION').style.visibility='hidden';">
		<P class=dogskinbodyfont ID="lig_supp" style="top:25px;left:10px;text-decoration:underline;font-weight:600;font-size:13pt;;cursor:hand" onclick="LigneSupprimer();document.all('EXECUTION').style.visibility='hidden'">Suppr. ligne</P>
		<P class=dogskinbodyfont ID="lig_inse" style="top:55px;left:10px;text-decoration:underline;font-weight:600;font-size:13pt;;cursor:hand" onclick="LigneValider();document.all('EXECUTION').style.visibility='hidden'">Ins. ligne</P>
		<P class=dogskinbodyfont ID="but_newb" style="top:85px;left:10px;text-decoration:underline;font-weight:600;font-size:13pt;;cursor:hand" onclick="BonNouveau();document.all('EXECUTION').style.visibility='hidden'">Nouv. bon</P>
		<P class=dogskinbodyfont ID="but_rech" style="top:115px;left:10px;text-decoration:underline;font-weight:600;font-size:13pt;;cursor:hand" onclick="BonRechercher();document.all('EXECUTION').style.visibility='hidden'">Rappel bon</P>
		<P class=dogskinbodyfont ID="but_arti" style="top:145px;left:10px;text-decoration:underline;font-weight:600;font-size:13pt;;cursor:hand" onclick="ArticleInfo();document.all('EXECUTION').style.visibility='hidden'">Info. article</P>

		<P class=dogskinbodyfont ID="but_impr" style="top:25px;left:148px;text-decoration:underline;font-weight:600;font-size:13pt;;cursor:hand" onclick="Imprimer();document.all('EXECUTION').style.visibility='hidden'">Impr. bon</P>
		<P class=dogskinbodyfont ID="but_supp" style="top:55px;left:148px;text-decoration:underline;font-weight:600;font-size:13pt;;cursor:hand" onclick="BonSupprimer();document.all('EXECUTION').style.visibility='hidden'">Suppr. bon</P>
		<P class=dogskinbodyfont ID="but_term" style="top:85px;left:148px;text-decoration:underline;font-weight:600;font-size:13pt;;cursor:hand" onclick="BonFin();document.all('EXECUTION').style.visibility='hidden'">Fin bon</P>
		<P class=dogskinbodyfont ID="but_vali" style="top:115px;left:148px;text-decoration:underline;font-weight:600;font-size:13pt;;cursor:hand" onclick="document.all('DICT�valide').value='O';BonValider();BonFin();document.all('EXECUTION').style.visibility='hidden';">Val. bon [F3]</P>
		<P class=dogskinbodyfont ID="but_recde" style="top:145px;left:148px;text-decoration:underline;font-weight:600;font-size:13pt;;cursor:hand" onclick="BonRechercherCde();document.all('EXECUTION').style.visibility='hidden'">Model. bon</P>
		<P class=dogskinbodyfont ID="but_quit" style="top:182px;left:53px;text-decoration:underline;font-weight:600;font-size:13pt;;cursor:hand" onclick="document.all('EXECUTION').style.visibility='hidden'">Fermer ce pav� [F4]</P>
	</DIV>
</DIV>

<!--//Pav� des informations article -->	
<DIV ID="ARTICLE" style="position:absolute;left:1px;height:250px;top:233px;width:280px;visibility:hidden" onmousedown="beginDrag(this,event);" onmousemove="drag(event);" onmouseup="endDrag();">
	<DIV class=dogskinshapeshadowstyle style="position:absolute;left:0px;height:290px;top:3px;width:300px" ></DIV>
	<DIV  class=dogskinshapestyle style="position:absolute;left:3px;height:290px;top:0px;width:300px">
		<IMG SRC='<?="http://".$GLOBALS["dealgateRelPath"]."/site/skin/std/img/close.jpg" ?>' class=dogskinf10 style="top:5px;left:280px" onclick="document.all('ARTICLE').style.visibility='hidden';">
	<DIV  style="position:absolute;left:5px;top:50px">
                <DIV  style="position:absolute;left:0px;top:-40px">
			<P class=dogskinbodyfont style="top:3px"><?PHP print lect_bin::getLabel ( "codart" , "sl"); ?> </P>
			<INPUT ID="DICT�codart2" TYPE=TEXT CLASS=dogskininput  style="top:3px;left:85px;width:100px" value='<?=$codart?>' title="<?PHP print lect_bin::getLabel ( "mess-prg2nat_82" , "sl"); ?>"  ctrl_input="@DICT�codart@||0||" onkeypress="javascript:if(event.keyCode == 13)dogexchange(this)" onkeydown="javascript:if(event.keyCode == 121 || event.keyCode == 118)dogexchange(this)"  onfocus=this.select() >
			<INPUT CLASS=dogskininputreadonly readonly ID="DICT�libart_12" value='<?=$codartlibel?>' style="left:0px;top:23px;width:280px">
	     	</DIV>	
	        <!--//provenance //-->
                <DIV  style="position:absolute;left:10px;top:3px">
		      <P class=dogskinbodyfont style="top:3px">Provenan. </P>
		      <INPUT ID="DICT�provenance2" TYPE=TEXT CLASS=dogskininput  style="top:3px;left:60px;width:20px"   >
	     	</DIV>	
	        <!--//code blocage //-->
                <DIV  style="position:absolute;left:95px;top:3px">
		      <P class=dogskinbodyfont style="left:0px;top:3px">C. blocage </P>
		      <INPUT ID="DICT�code-blocage2" TYPE=TEXT CLASS=dogskininput  style="top:03px;left:65px;width:20px"   >
	     	</DIV>	
	        <!--//statut autorisation //-->
                <DIV  style="position:absolute;left:185px;top:3px">
		      <P class=dogskinbodyfont style="top:3px">Vte </P>
		      <INPUT ID="DICT�aut12" TYPE=TEXT CLASS=dogskininput  style="top:3px;left:25px;width:20px"   >
		      <P class=dogskinbodyfont style="left:50px;top:3px">Ach </P>
		      <INPUT ID="DICT�aut22" TYPE=TEXT CLASS=dogskininput  style="top:3px;left:80px;width:20px"   >
	     	</DIV>		     	
	        <!--//tarif 1//-->
                <DIV  id="div1" tops="3" style="position:absolute;left:10px;top:3px">
                      <P class=dogskinbodyfont style="top:27px">Tarif </P>
                      <P class=dogskinbodyfont style="left:50px;top:27px">Dat deb. </P>
                      <P class=dogskinbodyfont style="left:135px;top:27px">Dat fin </P>
                      <P class=dogskinbodyfont style="left:225px;top:27px">Prix B. </P>
		      <INPUT ID="DICT�codtar_1" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:0px;width:45px"   >
		      <INPUT ID="DICT�dattar_1" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:50px;width:80px"   >
		      <INPUT ID="DICT�datfin_1" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:135px;width:85px"   >
		      <INPUT ID="DICT�pu-brut_1" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:225px;width:55px"   >
	     	</DIV>		     		     	
	        <!--//tarif 2//-->
                <DIV  id="div2" style="position:relative;left:10px;top:23px">
		      <INPUT ID="DICT�codtar_2" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:0px;width:45px"   >
		      <INPUT ID="DICT�dattar_2" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:50px;width:80px"   >
		      <INPUT ID="DICT�datfin_2" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:135px;width:85px"   >
		      <INPUT ID="DICT�pu-brut_2" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:225px;width:55px"   >
	     	</DIV>		     		     	
	        <!--//tarif 3//-->
                <DIV  id="div3" style="position:relative;left:10px;top:43px">
		      <INPUT ID="DICT�codtar_3" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:0px;width:45px"   >
		      <INPUT ID="DICT�dattar_3" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:50px;width:80px"   >
		      <INPUT ID="DICT�datfin_3" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:135px;width:85px"   >
		      <INPUT ID="DICT�pu-brut_3" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:225px;width:55px"   >
	     	</DIV>		     		     	
	        <!--//tarif 4//-->
                <DIV  id="div4" style="position:relative;left:10px;top:63px">
		      <INPUT ID="DICT�codtar_4" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:0px;width:45px"   >
		      <INPUT ID="DICT�dattar_4" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:50px;width:80px"   >
		      <INPUT ID="DICT�datfin_4" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:135px;width:85px"   >
		      <INPUT ID="DICT�pu-brut_4" TYPE=TEXT CLASS=dogskininput  style="top:43px;left:225px;width:55px"   >
	     	</DIV>		     		     	
	        <!--//stock//-->
                <DIV  id="div5" style="position:relative;left:2px;top:83px">
		      <P class=dogskinbodyfont style="top:43px;left:0px">Physique </P>
		      <INPUT ID="DICT�stophy" TYPE=TEXT CLASS=dogskininput  style="top:63px;left:0px;width:54px"   >
		      <P class=dogskinbodyfont style="top:43px;left:60px">Enc Vte </P>
		      <INPUT ID="DICT�encvte" TYPE=TEXT CLASS=dogskininput  style="top:63px;left:59px;width:54px"   >
		      <P class=dogskinbodyfont style="top:43px;left:115px">Disponible </P>
		      <INPUT ID="DICT�dispon" TYPE=TEXT CLASS=dogskininput  style="top:63px;left:118px;width:54px"   >
		      <P class=dogskinbodyfont style="top:43px;left:180px">Enc Ach </P>
		      <INPUT ID="DICT�encach" TYPE=TEXT CLASS=dogskininput  style="top:63px;left:177px;width:54px"   >
		      <P class=dogskinbodyfont style="top:43px;left:240px">Theor </P>
		      <INPUT ID="DICT�theori" TYPE=TEXT CLASS=dogskininput  style="top:63px;left:236px;width:54px"   >
	     	</DIV>		     		     	
	        <!--//statistiques 1//-->
                <DIV  id="div6" style="position:relative;left:10px;top:103px">
		      <P class=dogskinbodyfont style="top:63px">Stat. </P>
		      <INPUT ID="DICT�mois_1" TYPE=TEXT CLASS=dogskininput  style="top:63px;left:50px;width:85px"   >
		      <INPUT ID="DICT�quantite_1" TYPE=TEXT CLASS=dogskininput  style="top:63px;left:140px;width:65px"   >
		      <INPUT ID="DICT�valeur_1" TYPE=TEXT CLASS=dogskininput  style="top:63px;left:210px;width:70px"   >
	     	</DIV>		     		     	
	        <!--//statistiques 2//-->
                <DIV  id="div7" style="position:relative;left:10px;top:123px">
		      <INPUT ID="DICT�mois_2" TYPE=TEXT CLASS=dogskininput  style="top:63px;left:50px;width:85px"   >
		      <INPUT ID="DICT�quantite_2" TYPE=TEXT CLASS=dogskininput  style="top:63px;left:140px;width:65px"   >
		      <INPUT ID="DICT�valeur_2" TYPE=TEXT CLASS=dogskininput  style="top:63px;left:210px;width:70px"   >
	     	</DIV>		     		     	
	        <!--//statistiques 3//-->
                <DIV  id="div8" style="position:relative;left:10px;top:137px">
		      <INPUT ID="DICT�mois_3" TYPE=TEXT CLASS=dogskininput  style="top:63px;left:50px;width:85px"   >
		      <INPUT ID="DICT�quantite_3" TYPE=TEXT CLASS=dogskininput  style="top:63px;left:140px;width:65px"   >
		      <INPUT ID="DICT�valeur_3" TYPE=TEXT CLASS=dogskininput  style="top:63px;left:210px;width:70px"   >
	     	</DIV>		     		     	
	        <!--//taxes 1//-->
                <DIV  id="div9" style="position:relative;left:10px;top:163px">
		      <P class=dogskinbodyfont style="top:65px">Taxes </P>
		      <INPUT ID="DICT�taxadd-code_1" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:0px;width:45px"   >
		      <INPUT ID="DICTLINK�taxadd-code_1�LIBEL" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:50px;width:155px"   >
		      <INPUT ID="DICT�taxadd-pu_1" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:210px;width:65px"   >
		      <INPUT ID="DICT�taxadd-tva_1" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:210px;width:70px;visibility:hidden"   >
	     	</DIV>		     		     	
	        <!--//taxes 2//-->
                <DIV  id="div10" style="position:relative;left:10px;top:183px">
		      <INPUT ID="DICT�taxadd-code_2" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:0px;width:45px"   >
		      <INPUT ID="DICTLINK�taxadd-code_2�LIBEL" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:50px;width:155px"   >
		      <INPUT ID="DICT�taxadd-pu_2" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:210px;width:65px"   >
		      <INPUT ID="DICT�taxadd-tva_2" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:210px;width:70px;visibility:hidden"   >
	     	</DIV>		     		     	
	        <!--//taxes 3//-->
                <DIV  id="div11" style="position:relative;left:10px;top:203px">
		      <INPUT ID="DICT�taxadd-code_3" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:0px;width:45px"   >
		      <INPUT ID="DICTLINK�taxadd-code_3�LIBEL" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:50px;width:155px"   >		      
		      <INPUT ID="DICT�taxadd-pu_3" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:210px;width:65px"   >
		      <INPUT ID="DICT�taxadd-tva_3" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:210px;width:70px;visibility:hidden"   >
	     	</DIV>		     		     	
	        <!--//taxes 4/-->
                <DIV  id="div12" style="position:relative;left:10px;top:223px">
		      <INPUT ID="DICT�taxadd-code_4" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:0px;width:45px"   >
		      <INPUT ID="DICTLINK�taxadd-code_4�LIBEL" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:50px;width:155px"   >
		      <INPUT ID="DICT�taxadd-pu_4" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:210px;width:65px"   >
		      <INPUT ID="DICT�taxadd-tva_4" TYPE=TEXT CLASS=dogskininput  style="top:83px;left:210px;width:70px;visibility:hidden"   >
	     	</DIV>		     		     	

	</DIV>		
</DIV>
</DIV>
</BODY>
</HTML>


