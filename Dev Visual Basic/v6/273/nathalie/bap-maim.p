/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/bap-mai1.p                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
! 222640 2018-10-08!Evo!Willy Petit-Mai![GC][273] Reprise des sp�cifiques                                     !
!V61 36481 28-02-13!Evo!jcc            !Ajout soci�t� dans objet                                              !
!V61 36426 26-02-13!Evo!jcc            !Suite                                                                 !
!V61 36210 18-02-13!Evo!jcc            !Suite ...                                                             !
!V61 36059 11-02-13!Evo!jcc            !Suite BaP                                                             !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !tabges                                          !
!nathalie/dog-get-dicta!                                     !tables                                          !
!n-genmail.pp          !                                     !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/

{ connect.i }

/*
def input parameter fichier as char format "x(12)" .
*/

def buffer B-TABGES  for  TABGES .

def stream kan-out .
def stream fic-in.

def var x-x             as char         no-undo .
def var x-y             as char         no-undo .
def var x-z             as char         no-undo .
def var x-codsoc        as char         no-undo .
def var d-datana        as date         no-undo .
def var x-datana        as char         no-undo .

def var i-to            as char         no-undo .  
def var i-from          as char         no-undo .  
def var i-copy          as char         no-undo .
def var i-objet         as char         no-undo .  
def var i-file          as char         no-undo .  
def var i-message       as char         no-undo .  

def var dbg             as log          no-undo .  
def var repert-mail     as char         no-undo .
def var dir-sep         as char         no-undo .

def var glo-codsoc      as char         no-undo .  
def var x-ident         as char         no-undo .

d-datana = today .
x-ident  = "273" . 

x-z = string( year( d-datana ) , "9999" ) + string( month( d-datana ) , "99" ) + string( day( d-datana ) , "99" ) .
FOR EACH TABGES where tabges.codsoc =  ""
                and   tabges.etabli =  ""
                and   tabges.typtab =  "BAP"
                and   tabges.prefix >  "ENVMAIL-" 
                and   tabges.prefix <= "ENVMAIL-" + x-z    /*  x-z = date d'�mission  */
                :

    x-codsoc = substring( tabges.codtab , 4 ) .
    glo-codsoc = x-codsoc .

    assign  i-from    = tabges.libel1[ 1 ]
            i-to      = substring( tabges.codtab , 1 , 3 )
            i-copy    = ""
            i-file    = ""
            i-objet   = ""
            i-message = "" .

    x-datana = "        " .
    x-y      = substring( tabges.prefix , 9 ) .
    d-datana = date( int( substring( x-y , 5 , 2 ) ) , int( substring( x-y , 7 , 2 ) ) , int( substring( x-y , 1 , 4 ) ) )  no-error .
    if error-status:error = no  then  x-datana = string( d-datana , "99/99/9999" ) .

    /*  ----  Voir N-FACMAJ-BAP.I  ----  */
    run get-dictab( input x-ident + ",DEAL" , input "LABEL" , input "BAPSIGG-NAT" , input "1" , output i-objet ) .
    if i-objet = ""  then  
       i-objet = "Signature du BAP de factures" .

    run get-dictab( input x-ident + ",DEAL" , input "MESSAGE" , input "BAPSIGG-NAT" , input "1" , output i-message ) .
    if i-message = ""  then  
       i-message = "Vous avez des bons � payer de factures, � signer en date du : #03 " + chr(10) + chr(10) +
                   "Pour la soci�t� : #01 #02" + chr(10) + chr(10) + "#99" .

    i-message = "<html><body>" + i-message + "</body></html>" .

    i-objet   = replace( i-objet   , "#01" , glo-codsoc ) .  
    i-message = replace( i-message , "#01" , glo-codsoc ) .  

    i-message = replace( i-message , "#03" , x-datana ) .  

    FIND TABLES where tables.codsoc = ""
                and   tables.etabli = ""
                and   tables.typtab = "SOC"
                and   tables.prefix = "SOCIETE"
                and   tables.codtab = glo-codsoc
                no-lock  no-error .
    if available tables  then  
       assign  i-message = replace( i-message , "#02" , tables.libel1[ 1 ] )
               i-objet   = replace( i-objet   , "#02" , tables.libel1[ 1 ] ) .  

    FIND B-TABGES where b-tabges.codsoc = glo-codsoc
                  and   b-tabges.etabli = ""
                  and   b-tabges.typtab = "PRM"  
                  and   b-tabges.prefix = "ENVOI-MAIL"  
                  and   b-tabges.codtab = "" 
                  no-lock  no-error .
    if not available b-tabges  then
       FIND B-TABGES where b-tabges.codsoc = ""
                     and   b-tabges.etabli = ""
                     and   b-tabges.typtab = "PRM"  
                     and   b-tabges.prefix = "ENVOI-MAIL"  
                     and   b-tabges.codtab = "" 
                     no-lock  no-error .

    if available b-tabges  and  b-tabges.libel1[ 6 ] <> ""  then
    do :

        /* merci de laisser les 2 blancs derri�re les = dans la ligne ci-dessous - BP le 30.08.2011 */
        x-x = b-tabges.libel1[ 6 ] + "siexe/deal/nathalie/php/ns_extsignat.php?&_paramFile=siteclient" + x-ident + "&_ope=" + i-to + "&_ident=" + x-ident +
              "&_codsoc=" + trim( glo-codsoc ) + "&_multisoc=no" .
    	x-x = replace( x-x , "=" , "&#61;" ) . 
        i-message = replace( i-message , "#99" , x-x ) .

    end .                     

    i-message = replace( i-message , "#99"  , "" ).

    run n-genmail( input i-from , input i-to , input i-copy , input i-objet , input i-file , input i-message ) .

    DELETE TABGES .

/*   leave .   */

END .  /*  EACH TABGES  */

VALIDATE TABGES . RELEASE TABGES .

{ nathalie/dog-get-dictab.pp }

{ n-genmail.pp }     /*  Pour envoi de mail ( Appel� par n-facmaj-bap.i )  */




