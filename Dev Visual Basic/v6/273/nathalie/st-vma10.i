/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/st-vma10.i                                                                 !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 26390 19-09-11!Evo!cch            !Suite Fiche V61352 - Supprerssion question "Mois du PAMP"             !
!V61 26360 16-09-11!Evo!cch            !Ajout affichage date dernier arr�t� de stocks                         !
!V61 26284 14-09-11!Evo!cch            !Changement valeurs de di�ses pour PAMP mensuel                        !
!V61 26257 12-09-11!Bug!cch            !Correction pble di�ses sur �dition des pamps mensuels                 !
!V61 26122 02-09-11!Evo!cch            !Ajout gestion du PAMP au mois � partir de 07/2011                     !
!V61 22752 08-02-11!Evo!cch            !Suite Fiche V61 22744                                                 !
!V61 22744 08-02-11!Evo!cch            !Ajout di�se pour �dition Nom Fournisseur                              !
!V61 22318 11-01-11!Evo!cch            !Ajout �dition Volume et Unites/Hectare (di�se 83, 84 rg 02)           !
!V61 21952 10-12-10!Bug!cch            !Correction pble dernier enreg. non cr�e ds tables tempo. + suite Optim!
!V61 21722 25-11-10!Bug!cch            !Correction pages entetes vides sur rupture mag. + marge fausse        !
!V61 21672 23-11-10!Evo!cch            !Passage fichier ASCII en Table temporaire - st-vma1*                  !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                          S T - V M A 1 0 . I                             */
/*          Edition Statistique des Ventes et Marges / Article              */
/*===========================================================================*/

def var date-dern-arr           as date format "99/99/9999"        no-undo .

def {1} shared var libelle      as char                 extent 99  no-undo .
def {1} shared var valeur       as char                 extent 136 no-undo .

def {1} shared var mat-edi      as log                  extent 136 no-undo .
def {1} shared var mat-pmp      as log                  extent 4   no-undo .
def {1} shared var masque       as char                 extent 80  no-undo .
def {1} shared var masque-tot   as char                 extent 80  no-undo .

def {1} shared var tot-val      as dec                  extent 350 no-undo .
def {1} shared var tot-qte      as dec                  extent 380 no-undo .
def {1} shared var ind-tot      as int                             no-undo .
def {1} shared var lib-tot      as char                            no-undo .

def {1} shared var type-edit    as char                            no-undo .
def {1} shared var lib-titre    as char                            no-undo .
def {1} shared var lib-titre-2  as char                            no-undo .
def {1} shared var no-famille   as char                            no-undo .
def {1} shared var no-nomenc    as char                            no-undo .
def {1} shared var lib-famille  as char                            no-undo .
def {1} shared var lib-nomenc   as char                            no-undo .
def {1} shared var lib-ssfam    as char                            no-undo .
def {1} shared var type-fou     as char                            no-undo .
def {1} shared var no-fou       as char                            no-undo .
def {1} shared var lib-fou      as char                            no-undo .
def {1} shared var no-magasin   as char                            no-undo .
def {1} shared var lib-magasin  as char                            no-undo .
def {1} shared var lib-periode  as char                            no-undo .

def {1} shared var date-arr     as date                            no-undo .
def {1} shared var date-arrm1   as date                            no-undo .
def {1} shared var date-arr-q   as date                            no-undo .
def {1} shared var date-arrm1-q as date                            no-undo .
def {1} shared var date-tar     as date                            no-undo .
def {1} shared var date-tarm1   as date                            no-undo .

def {1} shared var mois-debcp   as char format "x(6)"              no-undo .
def {1} shared var mois-fincp   as char format "x(6)"              no-undo .
def {1} shared var mois-debex   as char format "x(6)"              no-undo .
/*
def {1} shared var mois-pamp    as log  format "0/1"               no-undo .
def {1} shared var lib-pamp     as char format "x(20)"             no-undo .
*/
def {1} shared var zone-var     as char format "x(47)"             no-undo .
def {1} shared var magasins     as char                            no-undo .
def {1} shared var fournisseurs as char                            no-undo .
def {1} shared var type-tri     as char format "x"                 no-undo .
def {1} shared var lib-tri      as char format "x(14)"             no-undo .
def {1} shared var familles     as char format "x(43)"             no-undo .
def {1} shared var regr-stat    as char format "x"                 no-undo .
def {1} shared var lib-regr     as char format "xxx"               no-undo .
def {1} shared var opt-glob     as log  format "G/D"   initial yes no-undo .
def {1} shared var lib-glob     as char format "x(20)"             no-undo .
def {1} shared var ajust        as log  format "O/N"               no-undo .
def {1} shared var lib-ajust    as char format "x(4)"              no-undo .
def {1} shared var maquette     as char format "x(12)"             no-undo .
def {1} shared var lib-maq      as char format "x(35)"             no-undo .
def {1} shared var periph       as char format "x(33)"             no-undo .
def {1} shared var chemin       as char format "x(18)"             no-undo .

def {1} shared var fam-exclus   as char  format  "xx"              no-undo .
def {1} shared var mag-exclus   as char  format  "xx"              no-undo .

def {1} shared var mois-deb     as int                             no-undo .
def {1} shared var anee-deb     as int                             no-undo .
def {1} shared var mois-fin     as int                             no-undo .
def {1} shared var anee-fin     as int                             no-undo .
def {1} shared var mois-dex     as int                             no-undo .
def {1} shared var anee-dex     as int                             no-undo .

def {1} shared var mois-debm1   as int                             no-undo .
def {1} shared var mois-finm1   as int                             no-undo .
def {1} shared var mois-dexm1   as int                             no-undo .

def {1} shared var annee-min    as char                            no-undo .
def {1} shared var annee-max    as char                            no-undo .

def {1} shared var exercice     as char                            no-undo .
def {1} shared var exercm1      as char                            no-undo .
def {1} shared var mois         as int                             no-undo .

def {1} shared var sav-periph   as char                            no-undo .
def {1} shared var z-choix      as char                            no-undo .

def {1} shared var nb-lus       as int                             no-undo .

def {1} shared var list-mag     as char                            no-undo .
def {1} shared var list-motcle  as char                            no-undo .

def {1} shared var ok-deb       as log                             no-undo .
def {1} shared var ok-fin       as log                             no-undo .
def {1} shared var ok-tot       as log                             no-undo .

def {1} shared var articl-soc   as char                            no-undo .
def {1} shared var magasi-soc   as char                            no-undo .
def {1} shared var auxapr-soc   as char                            no-undo .

def {1} shared var ind-nomenc   as char                            no-undo .
def {1} shared var prefix-ssf   as char                            no-undo .

def {1} shared var platef-lis   as char                            no-undo .

def stream kan-tri .

def {1} shared temp-table T-ARTICLE
        field articl        as char
        field libart        as char
        field typaux        as char
        field codaux        as char
        field famart-1      as char
        field catego-1      as char
        field soufam-1      as char
        field sssfam-1      as char
        field famart-2      as char
        field soufam-2      as char
        field sssfam-2      as char
        field tva           as char
        field poids         as dec
        field volume        as dec
        field unite-hectare as dec
        index i-cle is primary unique typaux codaux famart-1 catego-1 soufam-1 sssfam-1 libart articl .

def {1} shared temp-table T-ARTICLE-2
        field articl-stat as char
        field ordre       as char
        field articl-num  as char
        index i-cle articl-stat ordre articl-num .

def {1} shared frame fr-saisie .

form libelle[ 09 ] format "x(27)" mois-debcp space ( 12 ) libelle[ 01 ] format "x(18)" date-dern-arr skip
     libelle[ 10 ] format "x(27)" mois-fincp                                                         skip
     libelle[ 11 ] format "x(27)" mois-debex                                                         skip
  /* libelle[ 12 ] format "x(27)" mois-pamp  lib-pamp                                                skip */
     libelle[ 13 ] format "x(27)" zone-var                                                           skip
     libelle[ 06 ] format "x(27)" type-tri   lib-tri                                                 skip
     libelle[ 14 ] format "x(27)" familles                                                           skip
     libelle[ 07 ] format "x(27)" regr-stat  lib-regr                                                skip
     libelle[ 15 ] format "x(27)" opt-glob   lib-glob                                                skip
     libelle[ 16 ] format "x(27)" ajust      lib-ajust                                               skip
     libelle[ 17 ] format "x(27)" maquette   lib-maq                                                 skip
     libelle[ 18 ] format "x(27)" periph                                                             skip
     libelle[ 08 ] format "x(27)" chemin
     with frame fr-saisie
     row 3 centered overlay no-label { v6frame.i } .

