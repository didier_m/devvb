/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/nathalie/273/g-trf2.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V61 40729 04-09-13!Evo!jcc            !Acces pr�alable au tarif avec le code tiers                           !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*-----------------------------------------------------------*/
/* Display Scroll tarifs articles                - GESCOM -  */
/*                                                           */
/* g-trf2.i                                                  */
/*-----------------------------------------------------------*/

assign  tarcli-chrono = {tarcli.i}.chrono
        lib-compo     = "" .
        lib-typtar    = "" .

FIND TABGES where tabges.codsoc = codsoc-soc
            and   tabges.etabli = etabli-bl
            and   tabges.typtab = "TAR"
            and   tabges.prefix = "CODE-TARIF"
            and   tabges.codtab = {tarcli.i}.code-tarif
            no-lock  no-error .
if available tabges  then  lib-compo = tabges.libel1[ mem-langue ] .

if type-trt = 2  or  type-trt = 4 then
do :
    /*  Lecture du Code Valeur pour prise du Coefficient a appliquer  */
    if {tarcli.i}.code-tarif <> "000"  and  {tarcli.i}.code-tarif <> "PRO"  and  type-trt <> 4  then
    do :
        FIND FIRST B-TARCLI where b-tarcli.motcle     = motcle-trt
                                    and   b-tarcli.codsoc     = codsoc-soc
                                    and   b-tarcli.typaux     = ""
                                    and   b-tarcli.codaux     = {tarcli.i}.tarif-ref
                                    and   b-tarcli.articl     = ""
                                    and   b-tarcli.code-tarif = {tarcli.i}.code-tarif
                                    and   b-tarcli.date-debut <= date-debut
                                    use-index primaire  no-lock  no-error .
        if available b-tarcli then
        do :
            lib-typtar = "+" .
            if b-tarcli.valeur < 0 then lib-typtar = "-" .
            lib-typtar = lib-typtar + string( b-tarcli.valeur, ">>>9.99" ) .
            if b-tarcli.typtar = "2" then lib-typtar = lib-typtar + " Eur" .
            if b-tarcli.typtar = "1" then lib-typtar = lib-typtar + " %  " .
        end .
    end .
end .

if type-trt = 2  or  type-trt = 4  or  type-trt = 5  then
do :
    display tarcli-chrono
            {tarcli.i}.code-tarif
            lib-compo
            {tarcli.i}.tarif-ref
            lib-typtar
            {tarcli.i}.valeur
            {tarcli.i}.date-fin
            with frame tarcli-ligne .
end .

if type-trt = 3 then
do :
    lib-typtar = libelle[ int( {tarcli.i}.typtar ) + 25] .
    display tarcli-chrono
            {tarcli.i}.code-tarif
            lib-compo
            {tarcli.i}.typtar
            lib-typtar
            {tarcli.i}.valeur
            with frame tarcli-ligne.
end .

