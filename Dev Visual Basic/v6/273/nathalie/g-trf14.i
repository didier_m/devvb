/*-----------------------------------------------------------*/
/*               g - t r f 1 4 . i                           */
/*-----------------------------------------------------------*/
lect = "{2}".
if lect <> "" and lect <> "999"
then do:
             Find tarcli where tarcli.motcle     = zone-charg [3]
                         and   tarcli.codsoc     = codsoc-soc
                         and   tarcli.typaux     = " "
                         and   tarcli.codaux     = " "
                         and   tarcli.articl     <> " "
                         and   tarcli.typcdt     <> " "
                         and   tarcli.date-debut = date-debut
                         and   tarcli.code-tarif = type-tar
                         and   tarcli.chrono     = {2}
                         use-index chrono-bas no-lock no-error.
end.
else do:
        Find {1} tarcli where tarcli.motcle     = zone-charg [3]
                        and   tarcli.codsoc     = codsoc-soc
                        and   tarcli.typaux     = " "
                        and   tarcli.codaux     = " "
                        and   tarcli.articl     <> " "
                        and   tarcli.typcdt     <> " "
                        and   tarcli.date-debut = date-debut
                        and   tarcli.code-tarif = type-tar
                        use-index chrono-bas no-lock no-error.
end.