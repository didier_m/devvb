/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/inc/e-cdmsp1.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 15055 12-07-06!Evo!cch            !Acc�s ARTAPR avec soci�t� de regroupement suite mise en place ED      !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           E - C D M S P 1 . I                            */
/* Edition des Commandes Plateformes                                        */
/*--------------------------------------------------------------------------*/
/* Include Creation Recap. Famille Article                                  */
/*==========================================================================*/

    assign
        qte-c[1] = lignes.qte[1]
        poids    = lignes.pdsuni * lignes.qte[1] * lignes.coef-cde
        .

    if lignes.coef-cde < 0
       then poids = - ( lignes.pdsuni * lignes.qte[1] ) / lignes.coef-cde .

    if lignes.articl <> ""
    then do :

        /* Chargement fictri pour Recap par famille */
        assign codtab     = lignes.articl
               codsoc-sav = codsoc-soc
               codsoc-soc = codsoc-art
               .

        { l-articl.i }

        codsoc-soc = codsoc-sav .

        if available artapr
           then fictri-seg-01 = artapr.famart .
           else fictri-seg-01 = "xxx" .

        fictri-seg-02 = lignes.articl .

        find fictri
             where fictri.codsoc   = codsoc-soc
             and   fictri.mot-cle  = nom-edition
             and   fictri.op-seq   = fictri-sequence
             and   fictri.seg-01   = fictri-seg-01
             and   fictri.seg-02   = fictri-seg-02
             exclusive-lock no-error .

        if not available fictri
        then do :
            create fictri .
            assign
                fictri.codsoc  = codsoc-soc
                fictri.mot-cle = nom-edition
                fictri.op-seq  = fictri-sequence
                fictri.seg-01  = fictri-seg-01
                fictri.seg-02  = fictri-seg-02 .
        end .

        assign
            fictri.nombre [1]    = fictri.nombre [1] + qte-c[1]
            fictri.nombre [2]    = fictri.nombre [2] + poids
            fictri.zone-alpha[1] = lignes.libart
            .

    end .