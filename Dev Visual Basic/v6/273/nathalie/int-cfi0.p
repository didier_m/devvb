/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/src/int-cfi0.p                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 14569 29-03-06!Evo!cch            !Nouveau format du fichier Carte Fid�lit�                              !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !                                                !
!int-cfi0.i            !"new"                                !                                                !
!bat-ini.i             !"new"                                !                                                !
!aide.i                !"new"                                !                                                !
!aide-lib.i            !                                     !                                                !
!fr-cadre.i            !lib-prog                             !                                                !
!fncompo.i             !fichier-cfi fr-rejet                 !                                                !
!fncompo.i             !top-valid fr-saisie                  !                                                !
!bat-zon.i             !"fichier-cfi" "fichier-cfi" "c" "''" !                                                !
!bat-maj.i             !"int-cfi1.p"                         !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           I N T - C F I 0 . P                            */
/* Integration des Fiches Cartes Fidelite Magasin                           */
/*--------------------------------------------------------------------------*/
/* Programme Principal                                        CC 16/01/2001 */
/*==========================================================================*/

{ connect.i        }
{ int-cfi0.i "new" }
{ bat-ini.i  "new" }
{ aide.i     "new" }

def var trt-rejet as log no-undo .

/* Chargement aide et texte */
aide-fichier = "int-cfi0.aid" . aide-par = "" . run aide-lec.p .

do ind = 1 to 33 :
    aide-m = "libel" + string( ind, "99" ) .
    { aide-lib.i }
    libelle[ ind ] = aide-lib .
end .

/* Affichage ecrans */
libsoc-soc = libelle[ 6 ] .
{ fr-cadre.i lib-prog }

display               libelle[ 1 ] with frame fr-saisie .
color display message libelle[ 1 ] with frame fr-saisie .

if zone-charg[ 1 ] = "REJET" then trt-rejet = yes .
if trt-rejet
then do :
   display               libelle[ 2 ] with frame fr-rejet .
   color display message libelle[ 2 ] with frame fr-rejet .
end .

SAISIE :
REPEAT :

    REPEAT WHILE TRT-REJET :

        if fichier-cfi = ""
           then fichier-cfi = libelle[ 33 ] + "." + string ( today, "999999" ) .

        { fncompo.i fichier-cfi fr-rejet }

        if keyfunction( lastkey ) = "END-ERROR" then leave .

        if keyfunction( lastkey ) <> "RETURN" then undo, retry .

        if search( fichier-cfi ) = ?
        then do :
            bell . bell .
            message fichier-cfi libelle[ 10 ] .
            readkey pause 2 .
            undo, retry .
        end .

        leave .

    END .

    REPEAT WHILE KEYFUNCTION( LASTKEY ) <> "END-ERROR" :

        { fncompo.i top-valid fr-saisie }

        if keyfunction( lastkey ) = "CURSOR-UP" then next SAISIE .

        if keyfunction( lastkey ) <> "END-ERROR" and
           keyfunction( lastkey ) <> "RETURN"
           then undo, retry .

        leave .

    END .

    leave .

END . /* SAISIE */

if keyfunction( lastkey ) <> "END-ERROR" and top-valid
then do :

    batch-tenu = yes . /* Gestion en BATCH */

    /* Sauvegarde parametres saisis pour batch */
    { bat-zon.i "fichier-cfi" "fichier-cfi"   "c" "''" "libelle[02]" }
    { bat-zon.i "lib-prog"    "lib-prog"      "c" "''" "libelle[05]" }
    { bat-zon.i "zone-charg2" "zone-charg[2]" "c" "''" "''"          }

    /* Execution */
    { bat-maj.i "int-cfi1.p" }

end .

if trt-rejet then hide frame fr-rejet no-pause .
hide frame fr-saisie no-pause .
hide frame fr-titre no-pause .
hide frame fr-cadre no-pause .