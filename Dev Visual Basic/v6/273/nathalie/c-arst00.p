/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 273/appros/src/c-arst00.p                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 12740 02-06-05!Evo!cch            !Ajout possibilité de traiter un seul article                          !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !                                                !
!c-arst00.i            !new                                  !                                                !
!aide.i                !new                                  !                                                !
!bat-ini.i             !new                                  !                                                !
!aide-lib.i            !                                     !                                                !
!fr-cadre.i            !"lib-prog"                           !                                                !
!bat-zon.i             !"lib-prog" "lib-prog" "c" "''" "libel!                                                !
!bat-maj.i             !"c-arst01.p"                         !                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/*==========================================================================*/
/*                           C - A R S T 0 0 . P                            */
/* Arrete de Stock                                                          */
/*--------------------------------------------------------------------------*/
/* Programme principal                                                      */
/*==========================================================================*/

{ connect.i      }
{ c-arst00.i new }
{ aide.i     new }
{ bat-ini.i  new }

def var i as int no-undo .

/* Chargement aide et texte */
aide-fichier = "c-arst00.aid" . aide-par = "" . run aide-lec.p .
do i = 1 to 30 :
  aide-m = "libel" + string( i, "99" ) .
  { aide-lib.i }
  libelle[ i ] = aide-lib .
end .

maj-prix = "O" .
if zone-charg[ 4 ] begins "N"  then  maj-prix = "N" .

/* Affichage ecrans */
{ fr-cadre.i "lib-prog" }

display libelle[ 11 for 5 ] with frame fr-saisie .
color display message libelle[ 11 for 5 ] with frame fr-saisie .

/* Saisie des Parametres */
run c-arst0q.p .

if keyfunction( lastkey ) <> "end-error" and valida = entry( 1, libelle[6] )
then do :

    batch-tenu = yes . /* Gestion en BATCH */

    /* Sauvegarde parametres saisis */
    { bat-zon.i  "lib-prog"          "lib-prog"          "c" "''" "libelle[01]" }
    { bat-zon.i  "codsoc-soc"        "codsoc-soc"        "c" "''" "libelle[02]" }
    { bat-zon.i  "date-arrete-exe"   "date-arrete-exe"   "d" "''" "libelle[10]" }
    { bat-zon.i  "date-arrete-mois"  "date-arrete-mois"  "d" "''" "libelle[11]" }
    { bat-zon.i  "date-arrete"       "date-arrete"       "d" "''" "libelle[12]" }
    { bat-zon.i  "majour"            "majour"            "c" "''" "libelle[13]" }
    { bat-zon.i  "maj-prix"          "maj-prix"          "c" "''" "''"          }
    { bat-zon.i  "code-art"          "code-art"          "c" "''" "libelle[14]" }

    /* Execution */
    { bat-maj.i "c-arst01.p" }

end.

hide frame fr-saisie no-pause .
hide frame fr-titre  no-pause .
hide frame fr-cadre  no-pause .
