VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Begin VB.UserControl ctlScanDeal 
   BackColor       =   &H00FFFFFF&
   ClientHeight    =   1605
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6375
   ScaleHeight     =   1605
   ScaleWidth      =   6375
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   2000
      Left            =   5040
      Top             =   120
   End
   Begin MSCommLib.MSComm MSComm1 
      Left            =   5760
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      CommPort        =   3
      DTREnable       =   -1  'True
      NullDiscard     =   -1  'True
      RThreshold      =   1
      RTSEnable       =   -1  'True
      SThreshold      =   1
      InputMode       =   1
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Waiting data..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   2
      Top             =   240
      Width           =   5295
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   6135
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   1200
      Width           =   6135
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "ctlScanDeal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'-> API de temporisation
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Dim sBuffer

Dim topLecture As Boolean
Dim ean128 As String
Dim SSCC As String
Dim hdlFile As Integer
Dim sMaquette As String
Dim sSpool As String
Dim sMaqGui As String
Dim sMaqCar As String

Public Function GetScan() As String
Dim strVal As String
'-> on reccupere les resultats
GetScan = SSCC + "|" + ean128
SSCC = ""
ean128 = ""

End Function

Public Function SetScan(strSSCC As String, strEan128 As String) As String
SSCC = strSSCC
ean128 = strEan128

End Function

Private Sub Timer1_Timer()
Dim strValue As String

On Error Resume Next
Timer1.Enabled = False
If topLecture Then
    topLecture = False
    Sleep 2000
    'DoEvents
    Dim Buffer As Variant
    Buffer = MSComm1.Input
    strValue = StrConv(Buffer, vbUnicode)
    If strValue <> "" Then
         LectureScan strValue
    End If
    topLecture = True
End If
Timer1.Enabled = True
End Sub


Public Function About()

About = "DMZ" & App.Major & App.Minor

End Function

Public Function InitPort(strPort As String, strConf As String) As String
    On Error GoTo GestError
    'MSComm1.PortOpen = False
    If strConf <> "" Then MSComm1.Settings = strConf
    MSComm1.CommPort = CInt(strPort)
    MSComm1.PortOpen = True
    topLecture = True
    Timer1.Enabled = True
    InitPort = "OK"
    Exit Function
GestError:
    InitPort = Err.Description
End Function

Public Function ClosePort(strPort As String, strConf As String) As String
    On Error GoTo GestError
    topLecture = False
    Timer1.Enabled = False
    MSComm1.PortOpen = False
    ClosePort = "OK"
    Exit Function
    
GestError:
    MsgBox Err.Description
End Function

Private Sub LectureScan(strValue As String)
    Dim arrValue() As String
    Dim i As Integer
    Label1.Caption = Now
    strValue = Replace(strValue, Chr$(29), "$")
    arrValue = Split(strValue, "_")
    ean128 = ""
    SSCC = ""
    For i = 0 To UBound(arrValue)
        If NumEntries(arrValue(i), ";") = 2 Then
            If Mid(Entry(1, arrValue(i), ";"), 2, 7) <> "NOREAD" Then
                ean128 = Mid(Entry(1, arrValue(i), ";"), 2)
            End If
            If Mid(Entry(2, arrValue(i), ";"), 1, 6) <> "NOREAD" Then
                SSCC = Entry(2, arrValue(i), ";")
            End If
        End If
    Next
    If ean128 = "" Then ean128 = "?"
    If SSCC = "" Then SSCC = "?"
    SSCC = Replace(SSCC, "", "")
    ean128 = Replace(ean128, "", "")
    Label2.Caption = "EAN128 : " & ean128
    Label6.Caption = "SSCC : " & SSCC
End Sub

Public Sub setSpool(sMaq As String, sSp As String, sData As String, sDataMaqGui As String, sDataMaqCar As String)
    '--> Permet la generation du spool turbo
    ' sMaq maquette graphique ayant l'extension maqgui (le fichier maq doit aussi �tre pr�sent)
    ' sSp  nom du spool sans l'extension turbo
    ' sData Donn�e de type csv avec comme carriage return \r\n
    Dim hdlFile As Integer
    Dim eData
    Dim sD
    '-> on charge les variables globales
    sMaquette = sMaq
    sSpool = sSp
    sMaqGui = sDataMaqGui
    sMaqCar = sDataMaqCar
    
    '-> Obtenir un handle de fichier
    hdlFile = FreeFile
    '-> Ouverture du fichier ascii et �criture des pages
    Open sSpool + ".csv" For Output As #hdlFile
    Print #hdlFile, "PA;MAQUETTE=" + sMaquette
    eData = Split(sData, "\r\n")
    For Each sD In eData
        Print #hdlFile, sD
    Next
    Close #hdlFile
    '-> on part dans la cr�ation du fichier spool � partir du csv
    createSpool
End Sub

Private Sub createSpool()
    Dim hdlFile As Integer
    Dim hdlFile3 As Integer
    Dim Ligne As String
    Dim expLigne
    Dim sLigne
    Dim maquette As String
    Dim sMaqTrans As String
    Dim aMaqTrans As maqTrans
    Dim Rang As String
    Dim i As Integer
    Dim tot As String
    Set Rangs = New Collection
        
    '->ouverture en lecture du fichier csv
    hdlFile = FreeFile
    Open sSpool + ".csv" For Input As #hdlFile
    '-> Boucle d'analyse du fichier
    Do While Not EOF(hdlFile)
        '-> Lecture de la ligne
        Line Input #hdlFile, Ligne
        Ligne = Ligne + ";;"
            Select Case (Mid(UCase(Ligne), 1, 2))
                Case "PA"
                    '-> on se charge les parametres du csv
                    expLigne = Split(Ligne, ";")
                    For Each sLigne In expLigne
                        Dim eL
                        eL = Split(sLigne, "=")
                        If UBound(eL) = 1 Then If UCase(eL(0)) = "MAQUETTE" Then maquette = eL(1)
                    Next
                    '-> on recherche la maquette
                    If sMaqGui <> "" Then
                        Dim eMaqGui
                        'ouverture en ecriture du fichier spool
                        hdlFile3 = FreeFile
                        Open sSpool + ".turbo" For Output As #hdlFile3
                        sMaqGui = Replace(sMaqGui, "#�##�#", Chr(13))
                        eMaqGui = Split(sMaqGui, Chr(13))
                    
                        'on ecrit l'entete
                        Print #hdlFile3, "[SPOOL]"
                        Print #hdlFile3, "[MAQ]"
                        'boucle sur le fichier maquette
                        For i = 0 To UBound(eMaqGui)
                            '-> Lecture de la ligne
                            Ligne = eMaqGui(i)
                            If Trim(Ligne) = "[MAQCHAR]" Then
                                'on ecrit maintenant un tableau special pour le print
                                Print #hdlFile3, "[TB-Tableau]"
                                Print #hdlFile3, "\LargeurTb�0"
                                Print #hdlFile3, "\OrientationTB�0"
                                Print #hdlFile3, "\Begin�Entete"
                                Print #hdlFile3, "\Largeur�30"
                                Print #hdlFile3, "\Hauteur�1"
                                Print #hdlFile3, "\AlignTop�1"
                                Print #hdlFile3, "\Top�0"
                                Print #hdlFile3, "\AlignLeft�0"
                                Print #hdlFile3, "\Left�0"
                                Print #hdlFile3, "\RgChar�"
                                Print #hdlFile3, "\Ligne�1"
                                Print #hdlFile3, "\Colonne�1"
                                Print #hdlFile3, "\Col�0.5�1;^0001;40;FAUX,FAUX,FAUX,FAUX;Arial;10;FAUX;FAUX;FAUX;0;16777215;FAUX;0@0@1;0@1"
                                Print #hdlFile3, "\Champs�0001;1"
                                Print #hdlFile3, "\End�Entete"
                                Print #hdlFile3, "\Tableau�End"
                                Print #hdlFile3, Ligne
                            Else
                                Print #hdlFile3, Ligne
                            End If
                        Next
                        Print #hdlFile3, "[/MAQ]"
                        '-> On lance la procedure d'analyse du fichier maq pour les associations diese entry
                        If sMaqCar <> "" Then
                            '-> on ouvre la maquette
                            Dim eMaqCar
                            'Print #hdlFile3, "maqcar" & sMaqCar
                            sMaqCar = Replace(sMaqCar, "#�##�#", Chr(13))
                            eMaqCar = Split(sMaqCar, Chr(13))
                            'on cree la definition
                            Set aMaqTrans = New maqTrans
                            'boucle sur le fichier maquette
                            For i = 0 To UBound(eMaqCar)
                                ' recup de la ligne du fichier
                                Ligne = eMaqCar(i)
                                If Mid(Ligne, 2, 2) = "RG" Then Rang = Mid(Ligne, 2, Len(Ligne) - 2)
                                'si on est sur un rang
                                If Rang <> "" And Mid(Ligne, 2, 2) <> "RG" And Trim(Ligne) <> "" Then
                                    aMaqTrans.SetRang Rang, Ligne
                                End If
                            Next
                            '-> on se cree le tableau special "PRINT"
                            Rang = "RG00"
                            aMaqTrans.SetRang Rang, " [TB-Tableau(BLK-Entete)][\\1]{^0001#1                              } "
                            '-> Fermer le fichier maqtrans
                        End If
                    End If
                Case "PR"
                    '-> On imprime les donn�es a partir du tableau special
                    Ligne = aMaqTrans.SetValue("RG00;" + Entry(2, Ligne, ";"))
                Case "RG"
                    '-> on se r��crit la ligne de donn�e format�e au format turbo
                    Ligne = aMaqTrans.SetValue(Ligne)
                    Print #hdlFile3, Ligne
                Case "RE"
                    '-> ligne de commentaire ne pas traiter
                    'print "->REM : ".$Ligne."<br>";
                Case "PG"
                    Print #hdlFile3, "[PAGE]" & Chr(13)
                Case Else
                    '-> Ne traiter la ligne que si on est dans un spool
            End Select 'Selon la ligne que l'on est en train de lire
    Loop

    '-> Fermer le fichier ascii
    Close #hdlFile
    '-> Fermer le fichier spool
    Close #hdlFile3
End Sub



