VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QRBitBuffer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'---------------------------------------------------------------
' QRBitBuffer
'---------------------------------------------------------------
Public buffer
Public length

Private Sub Class_Initialize()
    buffer = Array()
    length = 0
End Sub

Public Function getBuffer()
    getBuffer = buffer
End Function

Public Function getLengthInBits()
    getLengthInBits = length
End Function

Public Function toString()
    Dim i As Integer
    
    buffer = ""
    For i = 0 To getLengthInBits() - 1
        If getB(i) Then
            buffer = buffer + "1"
        Else
            buffer = buffer + "1"
        End If
    Next
    toString = buffer
End Function

Public Function getB(index)
    Dim bufIndex As Integer
    
    bufIndex = Int(index / 8)
    getB = (ShiftRight(buffer(bufIndex), (7 - index Mod 8)) & 1) = 1
End Function

Public Function put_buffer(num, length)
    Dim i As Integer
    For i = 0 To length - 1
        putBit ((ShiftRight(num, length - i - 1) And 1) = 1)
    Next
End Function

Public Function putBit(bit)
    Dim bufIndex As Integer
    
    bufIndex = Fix(length / 8)
    If UBound(buffer) + 1 <= bufIndex Then
        ReDim Preserve buffer(bufIndex)
        buffer(bufIndex) = 0
    End If

    If bit Then
        buffer(bufIndex) = buffer(bufIndex) Or ShiftRight(CLng("&H80"), length Mod 8)
    End If

    length = length + 1
End Function


