VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QRCode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public QR_PAD0 As Long
Public QR_PAD1 As Long

Public typeNumber
Public modules As QRModule
Public moduleCount As Long
Public errorCorrectLevel
Public QRUtil As QRUtil
Public QRNumber As QRNumber
Public QRAlphaNum As QRAlphaNum
Public QR8BitByte As QR8BitByte
Public QRKanji As QRKanji
Public QRRSBlock
Public QRBitBuffer As QRBitBuffer
Public QRPolynomial As QRPolynomial
Public data
Public cpt As Integer

Public Sub Class_Initialize()
    QR_PAD0 = CLng("&HEC")
    QR_PAD1 = CLng("&H11")
    Set QRUtil = New QRUtil
    Set QRNumber = New QRNumber
    Set QRAlphaNum = New QRAlphaNum
    Set QR8BitByte = New QR8BitByte
    Set QRKanji = New QRKanji
    Set QRBitBuffer = New QRBitBuffer
    Set QRPolynomial = New QRPolynomial
    typeNumber = 1
    errorCorrectLevel = QR_ERROR_CORRECT_LEVEL_H
    Set qrDataList = New Collection
End Sub

Public Function getTypeNumber()
    getTypeNumber = typeNumber
End Function

Public Function setTypeNumber(iTypeNumber)
    typeNumber = iTypeNumber
End Function

Public Function getErrorCorrectLevel()
    getErrorCorrectLevel = errorCorrectLevel
End Function

Public Function setErrorCorrectLevel(errCorrectLevel)
    errorCorrectLevel = errCorrectLevel
End Function

Public Function addData(ByVal data, Optional ByVal mode As Integer = 0)
    If mode = 0 Then
        mode = QRUtil.getMode(data)
    End If

    Select Case mode
        Case QR_MODE_NUMBER
            addDataImpl (QRNumber.QRNumber(data))
        Case QR_MODE_ALPHA_NUM
            addDataImpl (QRAlphaNum.QRAlphaNum(data))
        Case QR_MODE_8BIT_BYTE
            addDataImpl QR8BitByte.QR8BitByte(data)
        Case QR_MODE_KANJI
            addDataImpl (QRKanji.QRKanji(data))
        Case Else
        trigger_error "mode:mode", E_USER_ERROR
    End Select
End Function

Public Function clearData()
    Set qrDataList = New Collection
End Function

Public Function addDataImpl(ByVal QRData)
    If qrDataList Is Nothing Then Set qrDataList = New Collection
    qrDataList.Add QRData
End Function

Public Function getDataCount()
    getDataCount = qrDataList.count
End Function

Public Function getData(index)
    getData = qrDataList(index)
End Function

Public Function isDark(row, col) As Boolean
    isDark = False
    If CStr(modules.getValue(row, col)) <> vide Then
        If CStr(modules.getValue(row, col)) = "1" Then isDark = True
    Else
        isDark = False
    End If
End Function

Public Function getModuleCount()
    getModuleCount = moduleCount
End Function

Public Function Hex2RGB(ByVal hexNum As Long) As Integer()
    Dim r, g, b As Long
    Dim rgb(1 To 3) As Integer
    Dim l As Long
    l = hexNum
    r = l Mod &H100
    l = l / &H100
    g = l Mod &H100
    l = l / &H100
    b = l Mod &H100
    rgb(1) = r
    rgb(2) = g
    rgb(3) = b
    Hex2RGB = rgb
End Function

Public Function make()
    makeImpl False, getBestMaskPattern()
End Function

Public Function getBestMaskPattern()
    Dim minLostPoint As Long
    Dim lostPoint As Long
    Dim Pattern As Long
    Dim i As Integer
    
    minLostPoint = 0
    Pattern = 0

    For i = 0 To 7
        makeImpl True, i
        lostPoint = QRUtil.getLostPoint(Me)
        If i = 0 Or minLostPoint > lostPoint Then
            minLostPoint = lostPoint
            Pattern = i
        End If
        DoEvents
    Next
    getBestMaskPattern = Pattern
End Function

Public Function createNullArray(ByVal length As Integer) As Collection
    Dim emptyArray As Integer
    
    Dim i As Integer
    
    emptyArray = -1
    Set createNullArray = New Collection
    
    For i = 0 To length - 1
        createNullArray.Add emptyArray
    Next
End Function

Public Function createCollection(ByVal length As Integer) As Collection
    Dim i As Integer
    
    Set createCollection = New Collection
    
    For i = 0 To length - 1
        createCollection.Add New clsArray
    Next
End Function

Public Function createCollectionArray(ByVal length As Integer)
    Dim i As Integer
    
    ReDim createCollection(length)
    
End Function

Public Function makeImpl(test, maskPattern)
    Dim i As Integer
    Dim dataArray
    
    moduleCount = typeNumber * 4 + 17

    Set modules = New QRModule
    
    For i = 0 To moduleCount - 1
        '-> Euh faudrait pas affecter la valeur?
        modules.createNullArray i, moduleCount
    Next

    setupPositionProbePattern 0, 0
    setupPositionProbePattern moduleCount - 7, 0
    setupPositionProbePattern 0, moduleCount - 7

    Call setupPositionAdjustPattern
    Call setupTimingPattern

    setupTypeInfo test, maskPattern

    If typeNumber >= 7 Then
        setupTypeNumber (test)
    End If

    Set dataArray = qrDataList
    DoEvents
    Set data = createData(typeNumber, errorCorrectLevel, dataArray)
    DoEvents
    Call debugModule
    Call mapData(data, maskPattern)
End Function

Public Function mapData(ByVal data, maskPattern)
    Dim i, c As Integer
    Dim inc As Long
    Dim row As Long
    Dim col As Long
    Dim bitIndex As Long
    Dim byteIndex As Long
    Dim dark As Boolean
    Dim mask
    
    inc = -1
    row = moduleCount - 1
    bitIndex = 7
    byteIndex = 0
    Dim val As String
    For col = moduleCount - 1 To 1 Step -2
        val = ""
        If col = 6 Then col = col - 1
        Do While (True)
            DoEvents
            For c = 0 To 1
            
                DoEvents
                '-> adaptation valeur pour la collectionn depart � 1
                If modules.getValue(row, col - c) = -1 Then
                    dark = False
                    
                    If byteIndex < data.count Then
                        dark = ((ShiftRight(data(byteIndex + 1).cArray, bitIndex) And 1) = 1)
                    End If
                    
                    mask = QRUtil.getMask(maskPattern, row, col - c)
                    
                    If mask Then
                        dark = Not dark
                    End If
                    '-> adaptation valeur pour la collectionn depart � 1
                    If dark Then
                        modules.setValue ok, row, col - c
                    Else
                        modules.setValue nok, row, col - c
                    End If
                    bitIndex = bitIndex - 1

                    If bitIndex = -1 Then
                        byteIndex = byteIndex + 1
                        bitIndex = 7
                    End If
                End If
            Next

            row = row + inc

            If row < 0 Or moduleCount <= row Then
                row = row - inc
                inc = -inc
                Exit Do
            End If
        Loop
    Next
End Function

Public Function setupPositionAdjustPattern()
    Dim i, j, r, c, col, row As Integer
    Dim pos
    pos = QRUtil.getPatternPosition(typeNumber)
    For i = 0 To UBound(pos)
        For j = 0 To UBound(pos)
            row = pos(i)
            col = pos(j)

            If modules.getValue(row, col) <> vide Then
                GoTo suite
            End If

            For r = -2 To 2
                For c = -2 To 2
                    If r = -2 Or r = 2 Or c = -2 Or c = 2 Or (r = 0 And c = 0) Then
                        modules.setValue ok, row + r, col + c
                    Else
                        modules.setValue nok, row + r, col + c
                    End If
                Next
            Next
suite:
        Next
    Next
End Function

Public Function setupPositionProbePattern(row, col)
    Dim r, c As Integer
    Dim aBol As Boolean
    For r = -1 To 7
        For c = -1 To 7
            If row + r <= -1 Or moduleCount <= row + r Or col + c <= -1 Or moduleCount <= col + c Then
                GoTo suite
            End If
            If (0 <= r And r <= 6 And (c = 0 Or c = 6)) Or (0 <= c And c <= 6 And (r = 0 Or r = 6)) Or (2 <= r And r <= 4 And 2 <= c And c <= 4) Then
                modules.setValue ok, row + r, col + c
            Else
                modules.setValue nok, row + r, col + c
            End If
suite:
        Next
    Next
End Function

Public Function setupTimingPattern()
    Dim r, c As Integer
    For r = 8 To moduleCount - 7
        If modules.getValue(r, 6) <> vide Then
            GoTo suite
        End If
        If r Mod 2 = 0 Then
            modules.setValue ok, r, 6
        Else
            modules.setValue nok, r, 6
        End If
suite:
    Next

    For c = 8 To moduleCount - 7
        If modules.getValue(6, c) <> vide Then
            GoTo suite2
        End If
        If c Mod 2 = 0 Then
            modules.setValue ok, 6, c
        Else
            modules.setValue nok, 6, c
        End If
suite2:
    Next
End Function

Public Function setupTypeNumber(test)
    Dim i As Integer
    Dim bits As Byte
    Dim modB
    
    bits = QRUtil.getBCHTypeNumber(typeNumber)

    For i = 0 To 17
        modB = (Not test And (ShiftRight(bits, i) & 1) = 1)
        modules(Int(i / 3))(i Mod 3 + moduleCount - 8 - 3) = modB
    Next

    For i = 0 To 17
        modB = (Not test And (ShiftRight(bits, i) & 1) = 1)
        modules(i Mod 3 + moduleCount - 8 - 3)(Int(i / 3)) = modB
    Next
End Function

Public Function setupTypeInfo(test, maskPattern)
    Dim i As Integer
    Dim data
    Dim bits As Long
    Dim modB
    data = ShiftLeft(errorCorrectLevel, 3) Or maskPattern
    bits = QRUtil.getBCHTypeInfo(data)

    For i = 0 To 14
        If (Not test And (ShiftRight(bits, i) And 1) = 1) Then
            modB = ok
        Else
            modB = nok
        End If
        If i < 6 Then
            modules.setValue modB, i, 8
        ElseIf i < 8 Then
            modules.setValue modB, i + 1, 8
        Else
            modules.setValue modB, moduleCount - 15 + i, 8
        End If
    Next

    For i = 0 To 14
        If (Not test And (ShiftRight(bits, i) And 1) = 1) Then
            modB = ok
        Else
            modB = nok
        End If
        If i < 8 Then
            modules.setValue modB, 8, moduleCount - i - 1
        ElseIf i < 9 Then
            modules.setValue modB, 8, 15 - i - 1 + 1
        Else
            modules.setValue modB, 8, 15 - i - 1
        End If
    Next
    If Not test Then
        modules.setValue ok, moduleCount - 8, 8
    Else
        modules.setValue nok, moduleCount - 8, 8
    End If
End Function

Public Function createData(typeNumber, errorCorrectLevel, dataArray)
    Dim rsBlocks
    Dim i, totalDataCount As Integer
    Dim data
    Dim buffer  As QRBitBuffer
    
    Set QRRSBlock = New QRRSBlock
    
    Set rsBlocks = QRRSBlock.getRSBlocks(typeNumber, errorCorrectLevel)

    Set buffer = New QRBitBuffer

    For i = 1 To dataArray.count
        Set data = dataArray(i)
        buffer.put_buffer data.getMode(), 4
        buffer.put_buffer data.getLength(), data.getLengthInBits(typeNumber)
        Call data.write_buffer(buffer)
    Next

    totalDataCount = 0
    
    For i = 1 To rsBlocks.count
        totalDataCount = totalDataCount + rsBlocks(i).getDataCount()
    Next
    If buffer.getLengthInBits() > totalDataCount * 8 Then
        trigger_error "code length overflow. (" & buffer.getLengthInBits() & ">" & totalDataCount * 8 & ")", E_USER_ERROR
    End If

    ' end code.
    If buffer.getLengthInBits() + 4 <= totalDataCount * 8 Then
        Call buffer.put_buffer(0, 4)
    End If

    ' padding
    Do While (buffer.getLengthInBits() Mod 8 <> 0)
        Call buffer.putBit(False)
    Loop

    ' padding
    Do While (True)
        DoEvents
        If buffer.getLengthInBits() >= totalDataCount * 8 Then
            Exit Do
        End If
        Call buffer.put_buffer(QR_PAD0, 8)
        If buffer.getLengthInBits() >= totalDataCount * 8 Then
            Exit Do
        End If
        Call buffer.put_buffer(QR_PAD1, 8)
    Loop
    DoEvents
    Set createData = createBytes(buffer, rsBlocks)
End Function

Public Function createBytes(ByRef buffer, ByRef rsBlocks)
    Dim i, index, offset, r, totalCodeCount  As Integer
    Dim maxDcCount, maxEcCount, dcCount, ecCount As Long
    Dim dcData As Collection
    Dim ecdata As Collection
    Dim bdata
    Dim rsPoly As QRPolynomial
    Dim rawPoly
    Dim modPoly
    Dim modIndex
    Dim data
    
    offset = 0
    DoEvents
    maxDcCount = 0
    maxEcCount = 0

    Set dcData = createCollection(rsBlocks.count)
    Set ecdata = createCollection(rsBlocks.count)
    
    '-> adaptation indice rsblocks et dcdata
    For r = 1 To rsBlocks.count

        dcCount = rsBlocks(r).getDataCount
        ecCount = rsBlocks(r).getTotalCount - dcCount

        maxDcCount = Max(maxDcCount, dcCount)
        maxEcCount = Max(maxEcCount, ecCount)

        Dim curDcData() As Integer
        ReDim curDcData(dcCount - 1)
        
        dcData(r).cArray = curDcData
        For i = 0 To UBound(dcData(r).cArray)
            bdata = buffer.getBuffer()
            Dim cA As clsArray
            Set cA = dcData(r)
            cA.setData i, CInt("&Hff") And bdata(i + offset)
        Next
        offset = offset + dcCount

        Set rsPoly = QRUtil.getErrorCorrectPolynomial(ecCount)
        Set rawPoly = New QRPolynomial
        rawPoly.Polynomial dcData(r).cArray, rsPoly.getLength() - 1

        Set modPoly = rawPoly.modB(rsPoly)
        Dim curEcData() As Integer
        ReDim curEcData(rsPoly.getLength() - 1)
        
        ecdata(r).cArray = curEcData

        For i = 0 To UBound(ecdata(r).cArray) - 1
            modIndex = i + modPoly.getLength() - (UBound(ecdata(r).cArray))
            Dim eA As clsArray
            Set eA = ecdata(r)
            If modIndex >= 0 Then
                eA.setData i, modPoly.getB(modIndex)
            Else
                eA.setData i, 0
            End If
        Next
    Next

    totalCodeCount = 0
    '-> modif car on part de 1
    For i = 1 To rsBlocks.count
        totalCodeCount = totalCodeCount + rsBlocks(i).getTotalCount()
    Next

    Set data = createCollection(totalCodeCount)
    Dim eD As clsArray
    
    index = 0

    For i = 0 To maxDcCount - 1
        For r = 1 To rsBlocks.count
            If i < UBound(dcData(r).cArray) + 1 Then
                index = index + 1
                data(index).cArray = dcData(r).getData(i)
            End If
        Next
    Next

    For i = 0 To maxEcCount - 1
        For r = 1 To rsBlocks.count
            If i < UBound(ecdata(r).cArray) + 1 Then
                index = index + 1
                data(index).cArray = ecdata(r).getData(i)
            End If
        Next
    Next

    Set createBytes = data
    DoEvents
End Function

Public Static Function getMinimumQRCode(data, errorCorrectLevel)
    Dim mode
    Dim qr As QRCode
    Dim QRData As QRData
    Dim length As Long
    
    mode = QRUtil.getMode(data)

    Set qr = New QRCode
    qr.setErrorCorrectLevel (errorCorrectLevel)
    Call qr.addData(data, mode)

    QRData = qr.getData(0)
    length = QRData.getLength()

    For typeNumber = 1 To 10
        If length <= QRUtil.getMaxLength(typeNumber, mode, errorCorrectLevel) Then
            qr.setTypeNumber (typeNumber)
        End If
    Next

    Call qr.make

    getMinimumQRCode = qr
End Function

' added fg (foreground), bg (background), and bgtrans (use transparent bg) parameters
' also added some simple error checking on parameters
' updated 2015.07.27 ~ DoktorJ
Public Function createImage(Optional size = 2, Optional margin = 2, Optional fg = CLng("&H000000"), Optional bg = CLng("&HFFFFFF"), Optional bgtrans = False)
    Dim r, c As Integer
    Dim image_size As Long
    Dim image
    Dim fgrgb
    Dim bgrgb
    Dim fgc
    Dim bgc
    
    ' size/margin EC
    If Not IsNumeric(size) Then size = 2
    If Not IsNumeric(margin) Then margin = 2
    If size < 1 Then size = 1
    If margin < 0 Then margin = 0

    image_size = getModuleCount() * size + margin * 2

    image = imagecreatetruecolor(image_size, image_size)

    ' fg/bg EC
    If fg < 0 Or fg > CLng("&HFFFFFF") Then fg = CLng("&H0")
    If bg < 0 Or bg > CLng("&HFFFFFF") Then bg = CLng("&HFFFFFF")

    ' convert hexadecimal RGB to arrays for imagecolorallocate
    fgrgb = Hex2RGB(fg)
    bgrgb = Hex2RGB(bg)

    ' replace black and white with fgc and bgc
    fgc = imagecolorallocate(image, fgrgb("r"), fgrgb("g"), fgrgb("b"))
    bgc = imagecolorallocate(image, bgrgb("r"), bgrgb("g"), bgrgb("b"))
    If bgtrans Then Call imagecolortransparent(image, bgc)

    ' update white to bgc
    Call imagefilledrectangle(image, 0, 0, image_size, image_size, bgc)

    For r = 0 To getModuleCount() - 1
        For c = 0 To getModuleCount() - 1
            If isDark(r, c) Then
                ' update black to fgc
                Call imagefilledrectangle(image, margin + c * size, margin + r * size, margin + (c + 1) * size - 1, margin + (r + 1) * size - 1, fgc)
            End If
        Next
    Next

    createImage image
End Function

Private Function imagefilledrectangle()

End Function

Private Function imagecolortransparent()

End Function

Private Function imagecolorallocate()

End Function

Private Function imagecreatetruecolor()

End Function

Public Function printQR(qrPicture As PictureBox, Optional size = 2, Optional foreColor = vbBlack)
    Dim Style As String
    Dim r, c As Integer
    Dim Color As String
    
    Dim imageWidth As Long
    Dim imageHeight As Long
    Dim square As Long
    
    '-> on calcule la taille de l'image
    imageWidth = size * getModuleCount() * 10
    imageHeight = size * getModuleCount() * 10
    
    qrPicture.ScaleMode = vbPixels
    
    'qrPicture.Width = imageWidth
    'qrPicture.Height = imageHeight
    qrPicture.ScaleWidth = getModuleCount()
    qrPicture.ScaleHeight = getModuleCount()
    qrPicture.DrawWidth = 1
    qrPicture.foreColor = foreColor
    qrPicture.DrawStyle = vbSolid
    square = qrPicture.height / getModuleCount()
    
    For r = 0 To getModuleCount() - 1
        DoEvents
        For c = 0 To getModuleCount() - 1
            If isDark(r, c) Then
                qrPicture.Line (c, r)-(c + 1, r + 1), , BF
            Else
                qrPicture.Line (c, r)-(c + 1, r + 1), vbWhite, BF
            End If
        Next
    Next

End Function


Public Function debugModule()
    Dim r, c As Integer
    Dim strTemp As String
    Exit Function
    cpt = cpt + 1
    Debug.Print cpt
    For r = 0 To getModuleCount() - 1
        DoEvents
        strTemp = ""
        For c = 0 To getModuleCount() - 1
            If isDark(r, c) Then
                strTemp = strTemp + "x"
            Else
                strTemp = strTemp + "o"
            End If
        Next
        Debug.Print strTemp
    Next
End Function
