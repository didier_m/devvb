VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QRPolynomial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---------------------------------------------------------------
' QRPolynomial
'---------------------------------------------------------------

Option Explicit

Public num

Public Sub Polynomial(tNum, Optional Shift As Integer = 0)
    Dim offset As Integer
    Dim length As Integer
    Dim i As Integer
    
    offset = 0
    length = UBound(tNum) + 1
    
    Do While offset < length And tNum(offset) = 0
        offset = offset + 1
        If offset = length Then Exit Do
    Loop

    num = aQRMath.createNumArray(length - offset + Shift)
    
    '-> le -1 c'est pour le strictement
    For i = 0 To length - offset - 1
        num(i) = tNum(i + offset)
    Next
    
End Sub

'. renamed get to getB
Public Function getB(index) As Integer
    getB = num(index)
End Function

Public Function getLength()
    getLength = UBound(num)
End Function

Public Function toString()
    Dim buffer As String
    Dim i As Integer
    
    buffer = ""

    For i = 0 To getLength() - 1
        If i > 0 Then
            buffer = buffer + ","
        End If
        buffer = buffer + getB(i)
    Next

    toString = buffer
End Function

Public Function toLogString()
    Dim buffer As String
    Dim i As Integer
    
    buffer = ""

    For i = 0 To getLength() - 1
        If i > 0 Then
            buffer = buffer + ","
        End If
        buffer = buffer + aQRMath.glog(getB(i))
    Next

    toLogString = buffer
End Function

Public Function multiply(e As QRPolynomial) As QRPolynomial
    Dim i, j As Integer
    Dim tNum() As Integer
    Dim aQRPolynomial As QRPolynomial
    
    tNum = aQRMath.createNumArray(getLength() + e.getLength() - 1 - 1)

    For i = 0 To getLength() - 1
        For j = 0 To e.getLength() - 1
            tNum(i + j) = tNum(i + j) Xor aQRMath.gexp(aQRMath.glog(getB(i)) + aQRMath.glog(e.getB(j)))
         Next
    Next
    
    Set aQRPolynomial = New QRPolynomial
    aQRPolynomial.Polynomial tNum
    Set multiply = aQRPolynomial
End Function

'. renamed mod to modB
Public Function modB(e As QRPolynomial) As QRPolynomial
    Dim i As Integer
    Dim ratio As Integer
    Dim numB() As Integer
    Dim newPolynomial As QRPolynomial
    If getLength() - e.getLength() < 0 Then
        '-> on essaye de retourner le this
        Set newPolynomial = New QRPolynomial
        newPolynomial.num = num
        Set modB = newPolynomial
        Exit Function
    End If
    
    ratio = aQRMath.glog(getB(0)) - aQRMath.glog(e.getB(0))

    numB = aQRMath.createNumArray(getLength())
    For i = 0 To getLength() - 1
        numB(i) = getB(i)
    Next

    For i = 0 To e.getLength() - 1
        numB(i) = numB(i) Xor (aQRMath.gexp(aQRMath.glog(e.getB(i)) + ratio))
    Next
    
    Set newPolynomial = New QRPolynomial
    '-> rajout du shift pour diminuer le create array
    newPolynomial.Polynomial numB, -1
    Set modB = newPolynomial.modB(e)
End Function

