VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QRCode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

define("QR_PAD0", 0xEC)
define("QR_PAD1", 0x11)


    Public typeNumber
    Public modules
    Public moduleCount
    Public errorCorrectLevel
    Public qrDataList

    Public Function qrCode()
        typeNumber = 1
        errorCorrectLevel = QR_ERROR_CORRECT_LEVEL_H
        qrDataList = Array()
    End Function

    Public Function getTypeNumber()
        getTypeNumber = typeNumber
    End Function

    Public Function setTypeNumber(typeNumber)
        setTypeNumber = typeNumber
    End Function

    Public Function getErrorCorrectLevel()
        getErrorCorrectLevel = errorCorrectLevel
    End Function

    Public Function setErrorCorrectLevel(errorCorrectLevel)
        errorCorrectLevel = errorCorrectLevel
    End Function

    Public Function addData(data, mode as Integer = 0)

        If mode = 0 Then
            mode = QRUtil:: getMode (data)
        End If

        Select Case mode

        Case QR_MODE_NUMBER
            addDataImpl(new QRNumber(data) )
            

        Case QR_MODE_ALPHA_NUM
            addDataImpl(new QRAlphaNum(data) )
            

        Case QR_MODE_8BIT_BYTE
            addDataImpl(new QR8BitByte(data) )
            

        Case QR_MODE_KANJI
            addDataImpl(new QRKanji(data) )
            

default:
            trigger_error("mode:mode", E_USER_ERROR)
        End Select
    End Function

    Public Function clearData()
        qrDataList = Array()
    End Function

    Public Function addDataImpl(&qrData)
        qrDataList [] = QRData
    End Function

    Public Function getDataCount()
        return count(qrDataList)
    End Function

    Public Function getData(index)
        return qrDataList[index]
    End Function

    Public Function isDark(row, col)
        if modules[row][col] != null then
            return modules[row][col]
        Else
            return false
        End If
    End Function

    Public Function getModuleCount()
        return moduleCount
    }

    ' used for converting fg/bg colors (e.g. #0000ff = 0x0000FF)
    ' added 2015.07.27 ~ DoktorJ
    Public Function hex2rgb(hex = 0x0)
        return array(
            'r' => floor(hex / 65536),
            'g' => floor(hex / 256) % 256,
            'b' => hex % 256
        )
    End Function

    Public Function make()
        makeImpl(false, getBestMaskPattern() )
    End Function

    Public Function getBestMaskPattern()

        minLostPoint = 0
        Pattern = 0

        For i = 0 To 7

            makeImpl(true, i)

            lostPoint = QRUtil:: getLostPoint (this)

            If i = 0 Or minLostPoint > lostPoint Then
                minLostPoint = lostPoint
                Pattern = i
            End If
        Next

        getBestMaskPattern = Pattern
    End Function

    Public Function createNullArray(length)
        nullArray = Array()
        For i = 0 To length - 1
            nullArray [] = Null
        Next
        createNullArray = nullArray
    End Function

    Public Function makeImpl(test, maskPattern)

        moduleCount = typeNumber * 4 + 17

        modules = Array()
        for i = 0 i < moduleCount
            modules [] = qrCode:: createNullArray (moduleCount)
        }

        setupPositionProbePattern(0, 0)
        setupPositionProbePattern(moduleCount - 7, 0)
        setupPositionProbePattern(0, moduleCount - 7)

        setupPositionAdjustPattern()
        setupTimingPattern()

        setupTypeInfo(test, maskPattern)

        If typeNumber >= 7 Then
            setupTypeNumber (test)
        End If

        dataArray = qrDataList

        data = QRCode::createData(typeNumber, errorCorrectLevel, dataArray)

        mapData(data, maskPattern)
    End Function

    Public Function mapData(&data, maskPattern)

        inc = -1
        row = moduleCount - 1
        bitIndex = 7
        byteIndex = 0

        for col = moduleCount - 1 col > 0 col -= 2) {

            if col = 6 then col--

            while (true) {

                for c = 0 c < 2 c++) {

                    if modules[row][col - c] == null then

                        dark = False

                        If byteIndex < Count(data) Then
                            dark = ( ( (data[byteIndex] >> bitIndex) & 1) = 1)
                        }

                        mask = QRUtil::getMask(maskPattern, row, col - c)

                        If mask Then
                            dark = !dark
                        }

                        modules[row][col - c] = dark
                        bitIndex--

                        If bitIndex = -1 Then
                            byteIndex++
                            bitIndex = 7
                        }
                    End If
                Next

                row += inc

                If row < 0 Or moduleCount <= row Then
                    row -= inc
                    inc = -inc
                    
                End If
            End If
        Next
    End Function

    Public Function setupPositionAdjustPattern()

        pos = QRUtil:: getPatternPosition (typeNumber)

        for i = 0 i < count(pos) i++) {

            for j = 0 j < count(pos) j++) {

                row = pos[i]
                col = pos[j]

                if modules[row][col] != null then
                    continue
                End If

                for r = -2 r <= 2 r++) {

                    for c = -2 c <= 2 c++) {

                        if r = -2 or r = 2 or c = -2 or c = 2
                                or (r = 0 && c = 0) ) {
                            modules[row + r][col + c] = true
                        Else
                            modules[row + r][col + c] = false
                        End If
                    Next
                Next
            Next
        Next
    End Function

    Public Function setupPositionProbePattern(row, col)

        For r = -1 to r <= 7 r++) {

            for c = -1 c <= 7 c++) {

                if row + r <= -1 or moduleCount <= row + r
                        or col + c <= -1 or moduleCount <= col + c) {
                    continue
                End If

                if  (0 <= r && r <= 6 && (c = 0 or c = 6) )
                        or (0 <= c && c <= 6 && (r = 0 or r = 6) )
                        or (2 <= r && r <= 4 && 2 <= c && c <= 4) ) {
                    modules[row + r][col + c] = true
                Else
                    modules[row + r][col + c] = false
                End If
            Next
        Next
    End Function

    Public Function setupTimingPattern()

        for r = 8 r < moduleCount - 8 r++) {
            if modules[r][6] != null then
                continue
            End If
            modules[r][6] = (r % 2 = 0)
        Next

        for c = 8 c < moduleCount - 8 c++) {
            if modules[6][c] != null then
                continue
            End If
            modules[6][c] = (c % 2 = 0)
        Next
    End Function

    Public Function setupTypeNumber(test)

        bits = QRUtil:: getBCHTypeNumber (typeNumber)

        for i = 0 i < 18
            mod = (!test && ( (bits >> i) & 1) = 1)
            modules[floor(i / 3)][i % 3 + moduleCount - 8 - 3] = mod
        Next

        for i = 0 i < 18
            mod = (!test && ( (bits >> i) & 1) = 1)
            modules[i % 3 + moduleCount - 8 - 3][floor(i / 3)] = mod
        Next
    End Function

    Public Function setupTypeInfo(test, maskPattern)

        data = (errorCorrectLevel << 3) | maskPattern
        bits = QRUtil:: getBCHTypeInfo (data)

        for i = 0 i < 15

            mod = (!test && ( (bits >> i) & 1) = 1)

            If i < 6 Then
                modules[i][8] = mod
            else if i < 8 then
                modules[i + 1][8] = mod
            Else
                modules[moduleCount - 15 + i][8] = mod
            End If
        Next

        for i = 0 i < 15

            mod = (!test && ( (bits >> i) & 1) = 1)

            If i < 8 Then
                modules[8][moduleCount - i - 1] = mod
            else if i < 9 then
                modules[8][15 - i - 1 + 1] = mod
            Else
                modules[8][15 - i - 1] = mod
            End If
        Next

        modules[moduleCount - 8][8] = !test
    End Function

    Public Function createData(typeNumber, errorCorrectLevel, dataArray)

        rsBlocks = QRRSBlock::getRSBlocks(typeNumber, errorCorrectLevel)

        buffer = new QRBitBuffer()

        for i = 0 i < count(dataArray) i++) {
            data = dataArray[i]
            buffer->put(data->getMode(), 4)
            buffer->put(data->getLength(), data->getLengthInBits(typeNumber) )
            data->write(buffer)
        Next

        totalDataCount = 0
        for i = 0 i < count(rsBlocks) i++) {
            totalDataCount += rsBlocks[i]->getDataCount()
        Next

        if buffer->getLengthInBits() > totalDataCount * 8 then
            trigger_error("code length overflow. ("
                . buffer->getLengthInBits()
                . ">"
                .  totalDataCount * 8
                . ")", E_USER_ERROR)
        End If

        ' end code.
        if buffer->getLengthInBits() + 4 <= totalDataCount * 8 then
            buffer->put(0, 4)
        End If

        ' padding
        while (buffer->getLengthInBits() % 8 != 0)
            buffer->putBit(false)
        Loop

        ' padding
        While (True)

            if buffer->getLengthInBits() >= totalDataCount * 8 then
                
            End If
            buffer->put(QR_PAD0, 8)

            if buffer->getLengthInBits() >= totalDataCount * 8 then
                
            End If
            buffer->put(QR_PAD1, 8)
        Loop

        return QRCode::createBytes(buffer, rsBlocks)
    End Function

    Public Function createBytes(&buffer, &rsBlocks)

        offset = 0

        maxDcCount = 0
        maxEcCount = 0

        dcdata = qrCode:: createNullArray (Count(rsBlocks))
        ecdata = qrCode:: createNullArray (Count(rsBlocks))

        for r = 0 r < count(rsBlocks) r++) {

            dcCount = rsBlocks[r]->getDataCount()
            ecCount = rsBlocks[r]->getTotalCount() - dcCount

            maxDcCount = Max(maxDcCount, dcCount)
            maxEcCount = Max(maxEcCount, ecCount)

            dcdata [r] = qrCode:: createNullArray (dcCount)
            for i = 0 i < count(dcdata[r]) i++) {
                bdata = buffer->getBuffer()
                dcdata[r][i] = 0xff & bdata[i + offset]
            Next
            offset += dcCount

            rsPoly = QRUtil:: getErrorCorrectPolynomial (ecCount)
            rawPoly = new QRPolynomial(dcdata[r], rsPoly->getLength() - 1)

            modPoly = rawPoly->mod(rsPoly)
            ecdata[r] = QRCode::createNullArray(rsPoly->getLength() - 1)
            for i = 0 i < count(ecdata[r]) i++) {
                modIndex = i + modPoly->getLength() - count(ecdata[r])
                ecdata[r][i] = (modIndex >= 0)? modPoly->get(modIndex) : 0
            Next
        Next

        totalCodeCount = 0
        for i = 0 i < count(rsBlocks) i++) {
            totalCodeCount += rsBlocks[i]->getTotalCount()
        Next

        data = qrCode:: createNullArray (totalCodeCount)

        index = 0

        for i = 0 i < maxDcCount
            for r = 0 r < count(rsBlocks) r++) {
                if i < count(dcdata[r])  then
                    data[index++] = dcdata[r][i]
                End If
            Next
        Next

        for i = 0 i < maxEcCount
            for r = 0 r < count(rsBlocks) r++) {
                if i < count(ecdata[r])  then
                    data[index++] = ecdata[r][i]
                End If
            Next
        Next

        return data
    }

    Public Static Function getMinimumQRCode(data, errorCorrectLevel)

        mode = QRUtil:: getMode (data)

        qr = new QRCode()
        qr->setErrorCorrectLevel(errorCorrectLevel)
        qr->addData(data, mode)

        qrData = qr->getData(0)
        length = qrData->getLength()

        for typeNumber = 1 typeNumber <= 10 typeNumber++) {
            if length <= QRUtil::getMaxLength(typeNumber, mode, errorCorrectLevel)  then
                qr->setTypeNumber(typeNumber)
                
            End If
        Next

        qr->make()

        return qr
    }

    ' added fg (foreground), bg (background), and bgtrans (use transparent bg) parameters
    ' also added some simple error checking on parameters
    ' updated 2015.07.27 ~ DoktorJ
    Public Function createImage(size = 2, margin = 2, fg = 0x000000, bg = 0xFFFFFF, bgtrans = false)

        ' size/margin EC
        if !is_numeric(size)) size = 2
        if !is_numeric(margin)) margin = 2
        if size < 1) size = 1
        if margin < 0) margin = 0

        image_size = getModuleCount() * size + margin * 2

        Image = imagecreatetruecolor(image_size, image_size)

        ' fg/bg EC
        if fg < 0 or fg > 0xFFFFFF) fg = 0x0
        if bg < 0 or bg > 0xFFFFFF) bg = 0xFFFFFF

        ' convert hexadecimal RGB to arrays for imagecolorallocate
        fgrgb = hex2rgb(fg)
        bgrgb = hex2rgb(bg)

        ' replace black and white with fgc and bgc
        fgc = imagecolorallocate(image, fgrgb['r'], fgrgb['g'], fgrgb['b'])
        bgc = imagecolorallocate(image, bgrgb['r'], bgrgb['g'], bgrgb['b'])
        if bgtrans) imagecolortransparent(image, bgc)

        ' update white to bgc
        imagefilledrectangle(image, 0, 0, image_size, image_size, bgc)

        for r = 0 r < getModuleCount() r++) {
            for c = 0 c < getModuleCount() c++) {
                If isDark(r, c) Then

                    ' update black to fgc
                    imagefilledrectangle(image,
                        margin + c * size,
                        margin + r * size,
                        margin + (c + 1) * size - 1,
                        margin + (r + 1) * size - 1,
                        fgc)
                End If
            Next
        Next

        return image
    End Function

    Public Function printHTML(size = "2px")

        Style = "border-style:noneborder-collapse:collapsemargin:0pxpadding:0px"

        Print ("<table style='style'>")

        for r = 0 r < getModuleCount() r++) {

            Print ("<tr style='style'>")

            for c = 0 c < getModuleCount() c++) {
                color = isDark(r, c)? "#000000" : "#ffffff"
                Print ("<td style='stylewidth:sizeheight:sizebackground-color:color'></td>")
            Next

            Print ("</tr>")
        Next

        Print ("</table>")
    }
}

