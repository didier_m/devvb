VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QRModule"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Enum typValue
    ok = 1
    nok = 0
    vide = -1
    out = -2
End Enum

Private Module(300, 300) As typValue

Public Sub setValue(ByVal vdata As typValue, ByVal ind1 As Long, ByVal ind2 As Long)
    Module(ind1, ind2) = vdata
End Sub

Public Function getValue(ByVal ind1 As Long, ByVal ind2 As Long) As typValue
     getValue = Module(ind1, ind2)
End Function

Public Sub createNullArray(ByVal ind1 As Long, iCount As Long)
    Dim ind2 As Long
    
    For ind2 = 0 To 300
        Module(ind1, ind2) = -2
    Next
    
    For ind2 = 0 To iCount
        Module(ind1, ind2) = -1
    Next
    
End Sub
