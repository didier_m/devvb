﻿'====================================================================================================
'The Free Edition of Java to VB Converter limits conversion output to 100 lines per file.

'To purchase the Premium Edition, visit our website:
'https://www.tangiblesoftwaresolutions.com/order/order-java-to-vb.html
'====================================================================================================

Imports System
Imports System.Collections.Generic

Namespace com.d_project.qrcode


	''' <summary>
	''' QRコード.
	''' <br/>■使い方
	''' <br/>(1) 誤り訂正レベル、データ等、諸パラメータを設定します。
	''' <br/>(2) make() を呼び出してQRコードを作成します。
	''' <br/>(3) getModuleCount() と isDark() で、QRコードのデータを取得します。
	''' <br/>
	''' @author Kazuhiko Arase 
	''' </summary>
	Public Class QRCode

		Private Const PAD0 As Integer = &HEC

		Private Const PAD1 As Integer = &H11

'JAVA TO VB CONVERTER NOTE: The field typeNumber was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private typeNumber_Conflict As Integer

		Private modules()() As Boolean?

'JAVA TO VB CONVERTER NOTE: The field moduleCount was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private moduleCount_Conflict As Integer

'JAVA TO VB CONVERTER NOTE: The field errorCorrectLevel was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private errorCorrectLevel_Conflict As Integer

		Private qrDataList As IList(Of QRData)

		''' <summary>
		''' コンストラクタ
		''' <br>型番1, 誤り訂正レベルH のQRコードのインスタンスを生成します。 </summary>
		''' <seealso cref= ErrorCorrectLevel </seealso>
		Public Sub New()
			 Me.typeNumber_Conflict = 1
			Me.errorCorrectLevel_Conflict = ErrorCorrectLevel.H
			Me.qrDataList = New List(Of QRData)(1)
		End Sub

		''' <summary>
		''' 型番を取得する。 </summary>
		''' <returns> 型番 </returns>
		Public Overridable Property TypeNumber As Integer
			Get
				Return typeNumber_Conflict
			End Get
			Set(ByVal typeNumber As Integer)
				Me.typeNumber_Conflict = typeNumber
			End Set
		End Property


		''' <summary>
		''' 誤り訂正レベルを取得する。 </summary>
		''' <returns> 誤り訂正レベル </returns>
		''' <seealso cref= ErrorCorrectLevel </seealso>
		Public Overridable Property ErrorCorrectLevel As Integer
			Get
				Return errorCorrectLevel_Conflict
			End Get
			Set(ByVal errorCorrectLevel As Integer)
				Me.errorCorrectLevel_Conflict = errorCorrectLevel
			End Set
		End Property


		''' <summary>
		''' データを追加する。 </summary>
		''' <param name="data"> データ </param>
		Public Overridable Sub addData(ByVal data As String)
			addData(data, QRUtil.getMode(data))
		End Sub

		''' <summary>
		''' モードを指定してデータを追加する。 </summary>
		''' <param name="data"> データ </param>
		''' <param name="mode"> モード </param>
		''' <seealso cref= Mode </seealso>
		Public Overridable Sub addData(ByVal data As String, ByVal mode As Integer)

			Select Case mode

			Case Mode.MODE_NUMBER
				addData(New QRNumber(data))

			Case Mode.MODE_ALPHA_NUM
				addData(New QRAlphaNum(data))

			Case Mode.MODE_8BIT_BYTE
				addData(New QR8BitByte(data))

			Case Mode.MODE_KANJI
				addData(New QRKanji(data))

			Case Else
				Throw New System.ArgumentException("mode:" & mode)
			End Select
		End Sub

		''' <summary>
		''' データをクリアする。
		''' <br/>addData で追加されたデータをクリアします。
		''' </summary>
		Public Overridable Sub clearData()
			qrDataList.Clear()
		End Sub

		Protected Friend Overridable Sub addData(ByVal qrData As QRData)
			qrDataList.Add(qrData)
		End Sub

		Protected Friend Overridable ReadOnly Property DataCount As Integer
			Get
				Return qrDataList.Count
			End Get
		End Property

		Protected Friend Overridable Function getData(ByVal index As Integer) As QRData
			Return qrDataList(index)
		End Function

		''' <summary>
		''' 暗モジュールかどうかを取得する。 </summary>
		''' <param name="row"> 行 (0 ～ モジュール数 - 1) </param>
		''' <param name="col"> 列 (0 ～ モジュール数 - 1) </param>
		Public Overridable Function isDark(ByVal row As Integer, ByVal col As Integer) As Boolean
			If modules(row)(col) IsNot Nothing Then
				Return modules(row)(col).Value
			Else
				Return False
			End If
		End Function

		''' <summary>
		''' モジュール数を取得する。
		''' </summary>
		Public Overridable ReadOnly Property ModuleCount As Integer
			Get
				Return moduleCount_Conflict
			End Get
		End Property

		''' <summary>
		''' QRコードを作成する。
		''' </summary>
		Public Overridable Sub make()
			make(False, BestMaskPattern)
		End Sub

		Private ReadOnly Property BestMaskPattern As Integer
			Get
    
				Dim minLostPoint As Integer = 0
				Dim pattern As Integer = 0
    
				For i As Integer = 0 To 7
    
					make(True, i)
    
					Dim lostPoint As Integer = QRUtil.getLostPoint(Me)
    
					If i = 0 OrElse minLostPoint > lostPoint Then
						minLostPoint = lostPoint
						pattern = i
					End If
				Next i
    
				Return pattern
			End Get
		End Property

		''' 
		Private Sub make(ByVal test As Boolean, ByVal maskPattern As Integer)

			' モジュール初期化
			moduleCount_Conflict = typeNumber_Conflict * 4 + 17

'====================================================================================================
'End of the allowed output for the Free Edition of Java to VB Converter.

'To purchase the Premium Edition, visit our website:
'https://www.tangiblesoftwaresolutions.com/order/order-java-to-vb.html
'====================================================================================================
