﻿'====================================================================================================
'The Free Edition of Java to VB Converter limits conversion output to 100 lines per file.

'To purchase the Premium Edition, visit our website:
'https://www.tangiblesoftwaresolutions.com/order/order-java-to-vb.html
'====================================================================================================

Imports System

Namespace com.d_project.qrcode


	''' <summary>
	''' QRUtil
	''' @author Kazuhiko Arase 
	''' </summary>
	Friend Class QRUtil

		Private Sub New()
		End Sub

		Public Shared ReadOnly Property JISEncoding As String
			Get
				Return "SJIS"
			End Get
		End Property

		Public Shared Function getPatternPosition(ByVal typeNumber As Integer) As Integer()
			Return PATTERN_POSITION_TABLE(typeNumber - 1)
		End Function

		Private Shared ReadOnly PATTERN_POSITION_TABLE()() As Integer = {
			New Integer() {},
			New Integer() {6, 18},
			New Integer() {6, 22},
			New Integer() {6, 26},
			New Integer() {6, 30},
			New Integer() {6, 34},
			New Integer() {6, 22, 38},
			New Integer() {6, 24, 42},
			New Integer() {6, 26, 46},
			New Integer() {6, 28, 50},
			New Integer() {6, 30, 54},
			New Integer() {6, 32, 58},
			New Integer() {6, 34, 62},
			New Integer() {6, 26, 46, 66},
			New Integer() {6, 26, 48, 70},
			New Integer() {6, 26, 50, 74},
			New Integer() {6, 30, 54, 78},
			New Integer() {6, 30, 56, 82},
			New Integer() {6, 30, 58, 86},
			New Integer() {6, 34, 62, 90},
			New Integer() {6, 28, 50, 72, 94},
			New Integer() {6, 26, 50, 74, 98},
			New Integer() {6, 30, 54, 78, 102},
			New Integer() {6, 28, 54, 80, 106},
			New Integer() {6, 32, 58, 84, 110},
			New Integer() {6, 30, 58, 86, 114},
			New Integer() {6, 34, 62, 90, 118},
			New Integer() {6, 26, 50, 74, 98, 122},
			New Integer() {6, 30, 54, 78, 102, 126},
			New Integer() {6, 26, 52, 78, 104, 130},
			New Integer() {6, 30, 56, 82, 108, 134},
			New Integer() {6, 34, 60, 86, 112, 138},
			New Integer() {6, 30, 58, 86, 114, 142},
			New Integer() {6, 34, 62, 90, 118, 146},
			New Integer() {6, 30, 54, 78, 102, 126, 150},
			New Integer() {6, 24, 50, 76, 102, 128, 154},
			New Integer() {6, 28, 54, 80, 106, 132, 158},
			New Integer() {6, 32, 58, 84, 110, 136, 162},
			New Integer() {6, 26, 54, 82, 110, 138, 166},
			New Integer() {6, 30, 58, 86, 114, 142, 170}
		}

		Private Shared MAX_LENGTH()()() As Integer = {
			New Integer()() {
				New Integer() {41, 25, 17, 10},
				New Integer() {34, 20, 14, 8},
				New Integer() {27, 16, 11, 7},
				New Integer() {17, 10, 7, 4}
			},
			New Integer()() {
				New Integer() {77, 47, 32, 20},
				New Integer() {63, 38, 26, 16},
				New Integer() {48, 29, 20, 12},
				New Integer() {34, 20, 14, 8}
			},
			New Integer()() {
				New Integer() {127, 77, 53, 32},
				New Integer() {101, 61, 42, 26},
				New Integer() {77, 47, 32, 20},
				New Integer() {58, 35, 24, 15}
			},
			New Integer()() {
				New Integer() {187, 114, 78, 48},
				New Integer() {149, 90, 62, 38},
				New Integer() {111, 67, 46, 28},
				New Integer() {82, 50, 34, 21}
			},
			New Integer()() {
				New Integer() {255, 154, 106, 65},
				New Integer() {202, 122, 84, 52},
				New Integer() {144, 87, 60, 37},
				New Integer() {106, 64, 44, 27}
			},
			New Integer()() {
				New Integer() {322, 195, 134, 82},
				New Integer() {255, 154, 106, 65},
				New Integer() {178, 108, 74, 45},
				New Integer() {139, 84, 58, 36}
			},

'====================================================================================================
'End of the allowed output for the Free Edition of Java to VB Converter.

'To purchase the Premium Edition, visit our website:
'https://www.tangiblesoftwaresolutions.com/order/order-java-to-vb.html
'====================================================================================================
