﻿Imports System

Namespace com.d_project.qrcode


	''' <summary>
	''' QRKanji
	''' @author Kazuhiko Arase 
	''' </summary>
	Friend Class QRKanji
		Inherits QRData

		Public Sub New(ByVal data As String)
			MyBase.New(Mode.MODE_KANJI, data)
		End Sub

		Public Overrides Sub write(ByVal buffer As BitBuffer)

			Try

				Dim data() As SByte = Data.GetBytes(QRUtil.JISEncoding)

				Dim i As Integer = 0

				Do While i + 1 < data.Length

					Dim c As Integer = ((&Hff And data(i)) << 8) Or (&Hff And data(i + 1))

					If &H8140 <= c AndAlso c <= &H9FFC Then
						c -= &H8140
					ElseIf &HE040 <= c AndAlso c <= &HEBBF Then
						c -= &HC140
					Else
						Throw New System.ArgumentException("illegal char at " & (i + 1) & "/" & c.ToString("x"))
					End If

					c = ((CInt(CUInt(c) >> 8)) And &Hff) * &HC0 + (c And &Hff)

					buffer.put(c, 13)

					i += 2
				Loop

				If i < data.Length Then
					Throw New System.ArgumentException("illegal char at " & (i + 1))
				End If

			Catch e As UnsupportedEncodingException
				Throw New Exception(e.Message)
			End Try
		End Sub

		Public Overrides ReadOnly Property Length As Integer
			Get
				Try
					Return Data.GetBytes(QRUtil.JISEncoding).length \ 2
				Catch e As UnsupportedEncodingException
					Throw New Exception(e.Message)
				End Try
			End Get
		End Property
	End Class
End Namespace