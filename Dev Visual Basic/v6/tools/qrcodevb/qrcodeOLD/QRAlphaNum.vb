﻿Namespace com.d_project.qrcode

	''' <summary>
	''' QRAlphaNum
	''' @author Kazuhiko Arase 
	''' </summary>
	Friend Class QRAlphaNum
		Inherits QRData

		Public Sub New(ByVal data As String)
			MyBase.New(Mode.MODE_ALPHA_NUM, data)
		End Sub

		Public Overrides Sub write(ByVal buffer As BitBuffer)

			Dim c() As Char = Data.ToCharArray()

			Dim i As Integer = 0

			Do While i + 1 < c.Length
				buffer.put(getCode(c(i)) * 45 + getCode(c(i + 1)), 11)
				i += 2
			Loop

			If i < c.Length Then
				buffer.put(getCode(c(i)), 6)
			End If
		End Sub

		Public Overrides ReadOnly Property Length As Integer
			Get
				Return Data.Length
			End Get
		End Property

		Private Shared Function getCode(ByVal c As Char) As Integer

			If "0"c <= c AndAlso c <= "9"c Then
				Return AscW(c) - AscW("0"c)
			ElseIf "A"c <= c AndAlso c <= "Z"c Then
				Return AscW(c) - AscW("A"c) + 10
			Else
				Select Case c
				Case " "c
					Return 36
				Case "$"c
					Return 37
				Case "%"c
					Return 38
				Case "*"c
					Return 39
				Case "+"c
					Return 40
				Case "-"c
					Return 41
				Case "."c
					Return 42
				Case "/"c
					Return 43
				Case ":"c
					Return 44
				Case Else
					Throw New System.ArgumentException("illegal char :" & c)
				End Select
			End If

		End Function
	End Class
End Namespace