﻿Namespace com.d_project.qrcode

	''' <summary>
	''' QRMath
	''' @author Kazuhiko Arase 
	''' </summary>
	Friend Class QRMath

		Private Sub New()
		End Sub

		Private Shared ReadOnly EXP_TABLE() As Integer
		Private Shared ReadOnly LOG_TABLE() As Integer

		Shared Sub New()

			EXP_TABLE = New Integer(255){}

			For i As Integer = 0 To 7
				EXP_TABLE(i) = 1 << i
			Next i

			For i As Integer = 8 To 255
				EXP_TABLE(i) = EXP_TABLE(i - 4) Xor EXP_TABLE(i - 5) Xor EXP_TABLE(i - 6) Xor EXP_TABLE(i - 8)
			Next i

			LOG_TABLE = New Integer(255){}
			For i As Integer = 0 To 254
				LOG_TABLE(EXP_TABLE(i)) = i
			Next i
		End Sub

		Public Shared Function glog(ByVal n As Integer) As Integer

			If n < 1 Then
				Throw New ArithmeticException("log(" & n & ")")
			End If

			Return LOG_TABLE(n)
		End Function

		Public Shared Function gexp(ByVal n As Integer) As Integer

			Do While n < 0
				n += 255
			Loop

			Do While n >= 256
				n -= 255
			Loop

			Return EXP_TABLE(n)
		End Function


	End Class

End Namespace