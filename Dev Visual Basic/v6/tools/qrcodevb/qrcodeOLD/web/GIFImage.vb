﻿'====================================================================================================
'The Free Edition of Java to VB Converter limits conversion output to 100 lines per file.

'To purchase the Premium Edition, visit our website:
'https://www.tangiblesoftwaresolutions.com/order/order-java-to-vb.html
'====================================================================================================

Imports System.Collections.Generic
Imports System.IO

Namespace com.d_project.qrcode.web


	''' <summary>
	''' GIFイメージ(B/W)
	''' @author Kazuhiko Arase 
	''' </summary>
	Friend Class GIFImage

		Private width As Integer
		Private height As Integer
		Private data() As Integer

		Public Sub New(ByVal width As Integer, ByVal height As Integer)
			Me.width = width
			Me.height = height
			Me.data = New Integer((width * height) - 1){}
		End Sub

		Public Overridable Sub setPixel(ByVal x As Integer, ByVal y As Integer, ByVal pixel As Integer)
			If x < 0 OrElse width <= x Then
				Throw New System.ArgumentException()
			End If
			If y < 0 OrElse height <= y Then
				Throw New System.ArgumentException()
			End If
			data(y * width + x) = pixel
		End Sub

		Public Overridable Function getPixel(ByVal x As Integer, ByVal y As Integer) As Integer
			If x < 0 OrElse width <= x Then
				Throw New System.ArgumentException()
			End If
			If y < 0 OrElse height <= y Then
				Throw New System.ArgumentException()
			End If
			Return data(y * width + x)
		End Function

'JAVA TO VB CONVERTER WARNING: Method 'throws' clauses are not available in VB:
'ORIGINAL LINE: public void write(java.io.OutputStream out) throws java.io.IOException
		Public Overridable Sub write(ByVal out As Stream)

			'---------------------------------
			' GIF Signature

			out.WriteByte("GIF87a".GetBytes("ISO-8859-1"))

			'---------------------------------
			' Screen Descriptor

			write(out, width)
			write(out, height)

			out.WriteByte(&H80) ' 2bit
			out.WriteByte(0)
			out.WriteByte(0)

			'---------------------------------
			' Global Color Map

			' black
			out.WriteByte(&H0)
			out.WriteByte(&H0)
			out.WriteByte(&H0)

			' white
			out.WriteByte(&Hff)
			out.WriteByte(&Hff)
			out.WriteByte(&Hff)

			'---------------------------------
			' Image Descriptor

			out.WriteByte(","c)
			write(out, 0)
			write(out, 0)
			write(out, width)
			write(out, height)
			out.WriteByte(0)

			'---------------------------------
			' Local Color Map

			'---------------------------------
			' Raster Data

			Dim lzwMinCodeSize As Integer = 2
			Dim raster() As SByte = getLZWRaster(lzwMinCodeSize)

			out.WriteByte(lzwMinCodeSize)

			Dim offset As Integer = 0

			Do While raster.Length - offset > 255
				out.WriteByte(255)
				out.Write(raster, offset, 255)
				offset += 255
			Loop

			out.WriteByte(raster.Length - offset)
			out.Write(raster, offset, raster.Length - offset)
			out.WriteByte(&H0)

			'---------------------------------
			' GIF Terminator
			out.WriteByte(";"c)

		End Sub

'JAVA TO VB CONVERTER WARNING: Method 'throws' clauses are not available in VB:
'ORIGINAL LINE: private byte[] getLZWRaster(int lzwMinCodeSize) throws java.io.IOException
		Private Function getLZWRaster(ByVal lzwMinCodeSize As Integer) As SByte()

			Dim clearCode As Integer = 1 << lzwMinCodeSize
			Dim endCode As Integer = (1 << lzwMinCodeSize) + 1
			Dim bitLength As Integer = lzwMinCodeSize + 1

			' Setup LZWTable
			Dim table As New LZWTable()

			For i As Integer = 0 To clearCode - 1
				table.add((ChrW(i)).ToString())
			Next i
			table.add((ChrW(clearCode)).ToString())
			table.add((ChrW(endCode)).ToString())


			Dim byteOut As New MemoryStream()
			Dim bitOut As New BitOutputStream(byteOut)

			Try

				' clear code        
				bitOut.write(clearCode, bitLength)

				Dim dataIndex As Integer = 0
'JAVA TO VB CONVERTER WARNING: An assignment within expression was extracted from the following statement:
'ORIGINAL LINE: String s = String.valueOf((char)data[dataIndex++]);
				Dim s As String = (ChrW(data(dataIndex))).ToString()
					dataIndex += 1

				Do While dataIndex < data.Length

'JAVA TO VB CONVERTER WARNING: An assignment within expression was extracted from the following statement:
'ORIGINAL LINE: char c = (char)data[dataIndex++];
					Dim c As Char = ChrW(data(dataIndex))
						dataIndex += 1

					If table.contains(s & c) Then

						s = s & c

					Else

						bitOut.write(table.indexOf(s), bitLength)

						If table.size() < &Hfff Then

							If table.size() = (1 << bitLength) Then
								bitLength += 1
							End If

							table.add(s & c)
						End If

						s = c.ToString()
					End If
				Loop

				bitOut.write(table.indexOf(s), bitLength)

				' end code        
				bitOut.write(endCode, bitLength)

			Finally
				bitOut.close()

'====================================================================================================
'End of the allowed output for the Free Edition of Java to VB Converter.

'To purchase the Premium Edition, visit our website:
'https://www.tangiblesoftwaresolutions.com/order/order-java-to-vb.html
'====================================================================================================
