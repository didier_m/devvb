﻿'====================================================================================================
'The Free Edition of Java to VB Converter limits conversion output to 100 lines per file.

'To purchase the Premium Edition, visit our website:
'https://www.tangiblesoftwaresolutions.com/order/order-java-to-vb.html
'====================================================================================================

Imports System.IO
Imports Microsoft.VisualBasic
Imports ErrorCorrectLevel = com.d_project.qrcode.ErrorCorrectLevel
Imports QRCode = com.d_project.qrcode.QRCode

Namespace com.d_project.qrcode.web



	''' <summary>
	''' QRコードサーブレット.
	''' <br>QRコードのモノクロGIF画像を応答するサーブレットです。
	''' <br>[パラメータ]
	''' <br> o : 出力形式 (text/plain, image/gif, image/jpeg, image/png のいずれか, 省略した場合は image/gif)
	''' <br> t : 型番 (0:自動, 1～10, 省略した場合は 0)
	''' <br>	e : 誤り訂正レベル (L, Q, M, H のいずれか, 省略した場合は H)
	''' <br>	d : テキストデータ (文字列)
	''' <br>	m : マージン (0～32, 省略した場合は 0)
	''' <br>	s : セルサイズ (1～4, 省略した場合は 1)
	''' @author Kazuhiko Arase </summary>
	''' <seealso cref= com.d_project.qrcode.Mode </seealso>
	''' <seealso cref= com.d_project.qrcode.ErrorCorrectLevel </seealso>
'JAVA TO VB CONVERTER TODO TASK: Most Java annotations will not have direct .NET equivalent attributes:
'ORIGINAL LINE: @SuppressWarnings("serial") public class QRCodeServlet extends javax.servlet.http.HttpServlet
	Public Class QRCodeServlet
		Inherits HttpServlet

		Private defaultCharacterEncoding As String

		''' <summary>
		''' コンストラクタ
		''' </summary>
		Public Sub New()
		End Sub

'JAVA TO VB CONVERTER WARNING: Method 'throws' clauses are not available in VB:
'ORIGINAL LINE: public void init(javax.servlet.ServletConfig config) throws javax.servlet.ServletException
		Public Overridable Sub init(ByVal config As ServletConfig)

			MyBase.init(config)

			defaultCharacterEncoding = ServletConfig.getInitParameter("default-character-encoding")
		End Sub

'JAVA TO VB CONVERTER WARNING: Method 'throws' clauses are not available in VB:
'ORIGINAL LINE: public void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException
		Public Overridable Sub doGet(ByVal request As HttpServletRequest, ByVal response As HttpServletResponse)

			Dim data As String = getParameter(request, "d", "qrcode")

			If defaultCharacterEncoding IsNot Nothing Then
				data = New String(data.GetBytes("ISO-8859-1"), defaultCharacterEncoding)
			End If

			Dim output As String = getParameter(request, "o", "image/gif")

			Dim typeNumber As Integer = getIntParameter(request, "t", 0)
			If typeNumber < 0 OrElse 10 < typeNumber Then
				Throw New System.ArgumentException("illegal type number : " & typeNumber)
			End If

			Dim margin As Integer = getIntParameter(request, "m", 2)
			If margin < 0 OrElse 32 < margin Then
				Throw New System.ArgumentException("illegal margin : " & margin)
			End If

			Dim cellSize As Integer = getIntParameter(request, "s", 1)
			If cellSize < 1 OrElse 4 < cellSize Then
				Throw New System.ArgumentException("illegal cell size : " & cellSize)
			End If

			Dim errorCorrectLevel As Integer = parseErrorCorrectLevel(getParameter(request, "e", "H"))

			Dim qrcode As QRCode = getQRCode(data, typeNumber, errorCorrectLevel)

			If "text/plain".Equals(output) Then

				response.ContentType = "text/plain"

				Dim out As New PrintWriter(New StreamWriter(response.OutputStream, "ISO-8859-1"))

				Try
					Dim row As Integer = 0
					Do While row < qrcode.ModuleCount
						Dim col As Integer = 0
						Do While col < qrcode.ModuleCount
							out.print(If(qrcode.isDark(row, col), "1", "0"))
							col += 1
						Loop
						out.print(vbCrLf)
						row += 1
					Loop
				Finally
					out.close()
				End Try

			ElseIf "image/jpeg".Equals(output) Then

				Dim image As BufferedImage = qrcode.createImage(cellSize, margin)

				response.ContentType = "image/jpeg"

				Dim out As Stream = New BufferedOutputStream(response.OutputStream)

				Try
					ImageIO.write(image, "jpeg", out)
				Finally
					out.Close()
				End Try

			ElseIf "image/png".Equals(output) Then

				Dim image As BufferedImage = qrcode.createImage(cellSize, margin)

				response.ContentType = "image/png"

				Dim out As Stream = New BufferedOutputStream(response.OutputStream)

				Try
					ImageIO.write(image, "png", out)
				Finally
					out.Close()
				End Try

			ElseIf "image/gif".Equals(output) Then

				Dim image As GIFImage = createGIFImage(qrcode, cellSize, margin)

				response.ContentType = "image/gif"

				Dim out As Stream = New BufferedOutputStream(response.OutputStream)

				Try
					image.write(out)
				Finally
					out.Close()
				End Try

			Else
				Throw New System.ArgumentException("illegal output type : " & output)
			End If
		End Sub

		Private Shared Function getParameter(ByVal request As HttpServletRequest, ByVal name As String, ByVal defaultValue As String) As String
			Dim value As String = request.getParameter(name)
			Return If(value IsNot Nothing, value, defaultValue)
		End Function

		Private Shared Function getIntParameter(ByVal request As HttpServletRequest, ByVal name As String, ByVal defaultValue As Integer) As Integer
			Dim value As String = request.getParameter(name)
			Return If(value IsNot Nothing, Integer.Parse(value), defaultValue)
		End Function

		Private Shared Function getQRCode(ByVal text As String, ByVal typeNumber As Integer, ByVal errorCorrectLevel As Integer) As QRCode

			If typeNumber = 0 Then

				Return QRCode.getMinimumQRCode(text, errorCorrectLevel)

			Else

				Dim qr As New QRCode()
				qr.TypeNumber = typeNumber
				qr.ErrorCorrectLevel = errorCorrectLevel
				qr.addData(text)
				qr.make()

				Return qr


'====================================================================================================
'End of the allowed output for the Free Edition of Java to VB Converter.

'To purchase the Premium Edition, visit our website:
'https://www.tangiblesoftwaresolutions.com/order/order-java-to-vb.html
'====================================================================================================
