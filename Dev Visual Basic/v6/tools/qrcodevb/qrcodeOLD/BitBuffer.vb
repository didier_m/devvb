﻿Imports System
Imports System.Text

Namespace com.d_project.qrcode

	''' <summary>
	''' BitBuffer
	''' @author Kazuhiko Arase 
	''' </summary>
	Friend Class BitBuffer

'JAVA TO VB CONVERTER NOTE: The field buffer was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private buffer_Conflict() As SByte
		Private length As Integer
		Private inclements As Integer

		Public Sub New()
			inclements = 32
			buffer_Conflict = New SByte(inclements - 1){}
			length = 0
		End Sub

		Public Overridable ReadOnly Property Buffer As SByte()
			Get
				Return buffer_Conflict
			End Get
		End Property

		Public Overridable ReadOnly Property LengthInBits As Integer
			Get
				Return length
			End Get
		End Property

		Public Overrides Function ToString() As String
			Dim buffer As New StringBuilder()
			Dim i As Integer = 0
			Do While i < LengthInBits
				buffer.Append(If(get(i), "1"c, "0"c))
				i += 1
			Loop
			Return buffer.ToString()
		End Function

		Private Function get(ByVal index As Integer) As Boolean
			Return ((CInt(CUInt(buffer_Conflict(index \ 8)) >> (7 - index Mod 8))) And 1) = 1
		End Function

		Public Overridable Sub put(ByVal num As Integer, ByVal length As Integer)
			For i As Integer = 0 To length - 1
				put(((CInt(CUInt(num) >> (length - i - 1))) And 1) = 1)
			Next i
		End Sub

		Public Overridable Sub put(ByVal bit As Boolean)

			If length = buffer_Conflict.Length * 8 Then
				Dim newBuffer((buffer_Conflict.Length + inclements) - 1) As SByte
				Array.Copy(buffer_Conflict, 0, newBuffer, 0, buffer_Conflict.Length)
				buffer_Conflict = newBuffer
			End If

			If bit Then
				buffer_Conflict(length \ 8) = buffer_Conflict(length \ 8) Or (&CInt(CUInt(H80) >> (length Mod 8)))
			End If

			length += 1
		End Sub

	End Class

End Namespace