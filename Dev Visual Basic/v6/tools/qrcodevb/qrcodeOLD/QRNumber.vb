﻿Namespace com.d_project.qrcode

	''' <summary>
	''' QRNumber
	''' @author Kazuhiko Arase 
	''' </summary>
	Friend Class QRNumber
		Inherits QRData

		Public Sub New(ByVal data As String)
			MyBase.New(Mode.MODE_NUMBER, data)
		End Sub

		Public Overrides Sub write(ByVal buffer As BitBuffer)

			Dim data As String = Data

			Dim i As Integer = 0

			Do While i + 2 < data.Length
				Dim num As Integer = parseInt(data.Substring(i, 3))
				buffer.put(num, 10)
				i += 3
			Loop

			If i < data.Length Then

				If data.Length - i = 1 Then
					Dim num As Integer = parseInt(data.Substring(i, 1))
					buffer.put(num, 4)
				ElseIf data.Length - i = 2 Then
					Dim num As Integer = parseInt(data.Substring(i, 2))
					buffer.put(num, 7)
				End If

			End If
		End Sub

		Public Overrides ReadOnly Property Length As Integer
			Get
				Return Data.Length
			End Get
		End Property

		Private Shared Function parseInt(ByVal s As String) As Integer
			Dim num As Integer = 0
			For i As Integer = 0 To s.Length - 1
				num = num * 10 + parseInt(s.Chars(i))
			Next i
			Return num
		End Function

		Private Shared Function parseInt(ByVal c As Char) As Integer

			If "0"c <= c AndAlso c <= "9"c Then
				Return AscW(c) - AscW("0"c)
			End If

			Throw New System.ArgumentException("illegal char :" & c)
		End Function


	End Class
End Namespace