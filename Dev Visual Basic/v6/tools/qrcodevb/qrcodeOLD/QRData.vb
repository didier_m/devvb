﻿Namespace com.d_project.qrcode

	''' <summary>
	''' QRData
	''' @author Kazuhiko Arase 
	''' </summary>
	Friend MustInherit Class QRData

'JAVA TO VB CONVERTER NOTE: The field mode was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private ReadOnly mode_Conflict As Integer

'JAVA TO VB CONVERTER NOTE: The field data was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private ReadOnly data_Conflict As String

		Protected Friend Sub New(ByVal mode As Integer, ByVal data As String)
			Me.mode_Conflict = mode
			Me.data_Conflict = data
		End Sub

		Public Overridable ReadOnly Property Mode As Integer
			Get
				Return mode_Conflict
			End Get
		End Property

		Public Overridable ReadOnly Property Data As String
			Get
				Return data_Conflict
			End Get
		End Property

		Public MustOverride ReadOnly Property Length As Integer

		Public MustOverride Sub write(ByVal buffer As BitBuffer)


		''' <summary>
		''' 型番及びモードに対するビット長を取得する。
		''' </summary>
		Public Overridable Function getLengthInBits(ByVal type As Integer) As Integer

			If 1 <= type AndAlso type < 10 Then

				' 1 - 9

				Select Case mode_Conflict
				Case Mode.MODE_NUMBER
					Return 10
				Case Mode.MODE_ALPHA_NUM
					Return 9
				Case Mode.MODE_8BIT_BYTE
					Return 8
				Case Mode.MODE_KANJI
					Return 8
				Case Else
					Throw New System.ArgumentException("mode:" & mode_Conflict)
				End Select

			ElseIf type < 27 Then

				' 10 - 26

				Select Case mode_Conflict
				Case Mode.MODE_NUMBER
					Return 12
				Case Mode.MODE_ALPHA_NUM
					Return 11
				Case Mode.MODE_8BIT_BYTE
					Return 16
				Case Mode.MODE_KANJI
					Return 10
				Case Else
					Throw New System.ArgumentException("mode:" & mode_Conflict)
				End Select

			ElseIf type < 41 Then

				' 27 - 40

				Select Case mode_Conflict
				Case Mode.MODE_NUMBER
					Return 14
				Case Mode.MODE_ALPHA_NUM
					Return 13
				Case Mode.MODE_8BIT_BYTE
					Return 16
				Case Mode.MODE_KANJI
					Return 12
				Case Else
					Throw New System.ArgumentException("mode:" & mode_Conflict)
				End Select

			Else
				Throw New System.ArgumentException("type:" & type)
			End If
		End Function

	End Class

End Namespace