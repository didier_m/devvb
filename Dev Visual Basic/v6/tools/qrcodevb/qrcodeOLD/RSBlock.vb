﻿Imports System
Imports System.Collections.Generic

Namespace com.d_project.qrcode


	''' <summary>
	''' RSBlock
	''' @author Kazuhiko Arase 
	''' </summary>
	Friend Class RSBlock

		Private Shared ReadOnly RS_BLOCK_TABLE()() As Integer = {
			New Integer() {1, 26, 19},
			New Integer() {1, 26, 16},
			New Integer() {1, 26, 13},
			New Integer() {1, 26, 9},
			New Integer() {1, 44, 34},
			New Integer() {1, 44, 28},
			New Integer() {1, 44, 22},
			New Integer() {1, 44, 16},
			New Integer() {1, 70, 55},
			New Integer() {1, 70, 44},
			New Integer() {2, 35, 17},
			New Integer() {2, 35, 13},
			New Integer() {1, 100, 80},
			New Integer() {2, 50, 32},
			New Integer() {2, 50, 24},
			New Integer() {4, 25, 9},
			New Integer() {1, 134, 108},
			New Integer() {2, 67, 43},
			New Integer() {2, 33, 15, 2, 34, 16},
			New Integer() {2, 33, 11, 2, 34, 12},
			New Integer() {2, 86, 68},
			New Integer() {4, 43, 27},
			New Integer() {4, 43, 19},
			New Integer() {4, 43, 15},
			New Integer() {2, 98, 78},
			New Integer() {4, 49, 31},
			New Integer() {2, 32, 14, 4, 33, 15},
			New Integer() {4, 39, 13, 1, 40, 14},
			New Integer() {2, 121, 97},
			New Integer() {2, 60, 38, 2, 61, 39},
			New Integer() {4, 40, 18, 2, 41, 19},
			New Integer() {4, 40, 14, 2, 41, 15},
			New Integer() {2, 146, 116},
			New Integer() {3, 58, 36, 2, 59, 37},
			New Integer() {4, 36, 16, 4, 37, 17},
			New Integer() {4, 36, 12, 4, 37, 13},
			New Integer() {2, 86, 68, 2, 87, 69},
			New Integer() {4, 69, 43, 1, 70, 44},
			New Integer() {6, 43, 19, 2, 44, 20},
			New Integer() {6, 43, 15, 2, 44, 16}
		}

'JAVA TO VB CONVERTER NOTE: The field totalCount was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private totalCount_Conflict As Integer
'JAVA TO VB CONVERTER NOTE: The field dataCount was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private dataCount_Conflict As Integer

		Private Sub New(ByVal totalCount As Integer, ByVal dataCount As Integer)
			Me.totalCount_Conflict = totalCount
			Me.dataCount_Conflict = dataCount
		End Sub

		Public Overridable ReadOnly Property DataCount As Integer
			Get
				Return dataCount_Conflict
			End Get
		End Property

		Public Overridable ReadOnly Property TotalCount As Integer
			Get
				Return totalCount_Conflict
			End Get
		End Property

		Public Shared Function getRSBlocks(ByVal typeNumber As Integer, ByVal errorCorrectLevel As Integer) As RSBlock()

			Dim rsBlock() As Integer = getRsBlockTable(typeNumber, errorCorrectLevel)
			Dim length As Integer = rsBlock.Length \ 3


			Dim list As IList(Of RSBlock) = New List(Of RSBlock)()

			For i As Integer = 0 To length - 1

				Dim count As Integer = rsBlock(i * 3 + 0)
				Dim totalCount As Integer = rsBlock(i * 3 + 1)
				Dim dataCount As Integer = rsBlock(i * 3 + 2)

				For j As Integer = 0 To count - 1
					list.Add(New RSBlock(totalCount, dataCount))
				Next j
			Next i

			Return CType(list, List(Of RSBlock)).ToArray()
		End Function

		Private Shared Function getRsBlockTable(ByVal typeNumber As Integer, ByVal errorCorrectLevel As Integer) As Integer()

			Try

				Select Case errorCorrectLevel
				Case ErrorCorrectLevel.L
					Return RS_BLOCK_TABLE((typeNumber - 1) * 4 + 0)
				Case ErrorCorrectLevel.M
					Return RS_BLOCK_TABLE((typeNumber - 1) * 4 + 1)
				Case ErrorCorrectLevel.Q
					Return RS_BLOCK_TABLE((typeNumber - 1) * 4 + 2)
				Case ErrorCorrectLevel.H
					Return RS_BLOCK_TABLE((typeNumber - 1) * 4 + 3)
				Case Else
				End Select

			Catch e As Exception
			End Try

			Throw New System.ArgumentException("tn:" & typeNumber & "/ecl:" & errorCorrectLevel)
		End Function

	End Class
End Namespace