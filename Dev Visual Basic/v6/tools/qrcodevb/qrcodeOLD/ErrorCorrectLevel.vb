﻿Namespace com.d_project.qrcode

	''' <summary>
	''' 誤り訂正レベル.
	''' @author Kazuhiko Arase 
	''' </summary>
	Public Interface ErrorCorrectLevel

		''' <summary>
		''' 復元能力 7%.
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int L = 1;

		''' <summary>
		''' 復元能力 15%.
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int M = 0;

		''' <summary>
		''' 復元能力 25%.
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int Q = 3;

		''' <summary>
		''' 復元能力 30%.
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int H = 2;

	End Interface


End Namespace