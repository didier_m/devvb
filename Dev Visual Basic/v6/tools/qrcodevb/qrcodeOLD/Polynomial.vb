﻿Imports System
Imports System.Text

Namespace com.d_project.qrcode

	''' <summary>
	''' Polynomial
	''' @author Kazuhiko Arase 
	''' </summary>
	Friend Class Polynomial

		Private ReadOnly num() As Integer

		Public Sub New(ByVal num() As Integer)
			Me.New(num, 0)
		End Sub

		Public Sub New(ByVal num() As Integer, ByVal shift As Integer)

			Dim offset As Integer = 0

			Do While offset < num.Length AndAlso num(offset) = 0
				offset += 1
			Loop

			Me.num = New Integer((num.Length - offset + shift) - 1){}
			Array.Copy(num, offset, Me.num, 0, num.Length - offset)
		End Sub

		Public Overridable Function get(ByVal index As Integer) As Integer
			Return num(index)
		End Function

		Public Overridable ReadOnly Property Length As Integer
			Get
				Return num.Length
			End Get
		End Property

		Public Overrides Function ToString() As String

			Dim buffer As New StringBuilder()

			Dim i As Integer = 0
			Do While i < Length
				If i > 0 Then
					buffer.Append(",")
				End If
				buffer.Append(get(i))
				i += 1
			Loop

			Return buffer.ToString()
		End Function

		Public Overridable Function toLogString() As String

			Dim buffer As New StringBuilder()

			Dim i As Integer = 0
			Do While i < Length
				If i > 0 Then
					buffer.Append(",")
				End If
				buffer.Append(QRMath.glog(get(i)))

				i += 1
			Loop

			Return buffer.ToString()
		End Function

		Public Overridable Function multiply(ByVal e As Polynomial) As Polynomial

			Dim num(Length + e.Length - 2) As Integer

			Dim i As Integer = 0
			Do While i < Length
				For j As Integer = 0 To e.Length - 1
					num(i + j) = num(i + j) Xor QRMath.gexp(QRMath.glog(get(i)) + QRMath.glog(e.get(j)))
				Next j
				i += 1
			Loop

			Return New Polynomial(num)
		End Function

		Public Overridable Function [mod](ByVal e As Polynomial) As Polynomial

			If Length - e.Length < 0 Then
				Return Me
			End If

			' 最上位桁の比率
			Dim ratio As Integer = QRMath.glog(get(0)) - QRMath.glog(e.get(0))

			' コピー作成
			Dim num(Length - 1) As Integer
			Dim i As Integer = 0
			Do While i < Length
				num(i) = get(i)
				i += 1
			Loop

			' 引き算して余りを計算
			For i As Integer = 0 To e.Length - 1
				num(i) = num(i) Xor QRMath.gexp(QRMath.glog(e.get(i)) + ratio)
			Next i

			' 再帰計算
			Return (New Polynomial(num)).mod(e)
		End Function
	End Class

End Namespace