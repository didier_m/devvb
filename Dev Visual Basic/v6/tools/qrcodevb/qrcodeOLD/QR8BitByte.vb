﻿Imports System

Namespace com.d_project.qrcode


	''' <summary>
	''' QR8BitByte
	''' @author Kazuhiko Arase 
	''' </summary>
	Friend Class QR8BitByte
		Inherits QRData

		Public Sub New(ByVal data As String)
			MyBase.New(Mode.MODE_8BIT_BYTE, data)
		End Sub

		Public Overrides Sub write(ByVal buffer As BitBuffer)

			Try

				Dim data() As SByte = Data.GetBytes(QRUtil.JISEncoding)

				For i As Integer = 0 To data.Length - 1
					buffer.put(data(i), 8)
				Next i

			Catch e As UnsupportedEncodingException
				Throw New Exception(e.Message)
			End Try
		End Sub

		Public Overrides ReadOnly Property Length As Integer
			Get
				Try
					Return Data.GetBytes(QRUtil.JISEncoding).length
				Catch e As UnsupportedEncodingException
					Throw New Exception(e.Message)
				End Try
			End Get
		End Property

	End Class
End Namespace