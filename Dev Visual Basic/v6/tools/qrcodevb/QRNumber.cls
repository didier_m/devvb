VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QRNumber"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---------------------------------------------------------------
' QRNumber
'---------------------------------------------------------------
Option Explicit

Dim aQRUtil As QRUtil
Dim aQRData As QRData
Private Sub Class_Initialize()
    Set aQRUtil = New QRUtil
    Set aQRData = New QRData
End Sub
    
Public Function QRNumber(data)
    
    Call aQRData.QRData(QR_MODE_NUMBER, data)
End Function

Public Function writeB(ByVal buffer)
    Dim i As Integer
    Dim data
    Dim numB
    
    data = aQRData.getData()
    i = 0
    Do While (i + 2 < Len(data))
        numB = parseInt(Mid(data, i, 3))
        Call buffer.put(numB, 10)
        i = i + 3
    Loop

    If i < Len(data) Then
        If Len(data) - i = 1 Then
            numB = parseInt(Mid(data, i, i + 1))
            Call buffer.put(numB, 4)
        ElseIf Len(data) - i = 2 Then
            numB = parseInt(Mid(data, i, i + 2))
            Call buffer.put(numB, 7)
        End If
    End If
End Function

Public Function getLength()
    getLength = Len(aQRData.getData())
End Function

Public Static Function parseInt(s)
    Dim i As Integer
    
    num = 0
    For i = 0 To Len(s) - 1
        num = num * 10 + parseIntAt(Asc(s(i)))
    Next
    parseInt = num
End Function

Public Static Function parseIntAt(c)

    If aQRUtil.toCharCode("0") <= c And c <= aQRUtil.toCharCode("9") Then
        parseIntAt = c - aQRUtil.toCharCode("0")
    End If

    trigger_error "illegal char : c", E_USER_ERROR
End Function

