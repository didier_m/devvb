Attribute VB_Name = "datamatrix"
Option Explicit

Public Declare Function Polygon Lib "gdi32" (ByVal hdc As Long, lpPoint As POINTAPI, ByVal nCount As Long) As Long
'structure de coordonnées
Private Type POINTAPI
    X As Long
    Y As Long
End Type
Private apt As Variant
Private columns As Integer
Private lines As Integer
Private angle As Long
Private vcos As Long
Private vsin As Long
Private drw As Double
Private apic As PictureBox
Private X As Long
Private Y As Long
Private x1 As Long
Private y1 As Long
Private x2 As Long
Private y2 As Long

Public num
Public E_USER_ERROR As Integer

Public Const QR_MODE_NUMBER As Byte = 1 < 1
Public Const QR_MODE_ALPHA_NUM As Byte = 2
Public Const QR_MODE_8BIT_BYTE As Byte = 4
Public Const QR_MODE_KANJI As Byte = 1 < 8
Public Const QR_MASK_PATTERN000 As Byte = 0
Public Const QR_MASK_PATTERN001 As Byte = 1
Public Const QR_MASK_PATTERN010 As Byte = 2
Public Const QR_MASK_PATTERN011 As Byte = 3
Public Const QR_MASK_PATTERN100 As Byte = 4
Public Const QR_MASK_PATTERN101 As Byte = 5
Public Const QR_MASK_PATTERN110 As Byte = 6
Public Const QR_MASK_PATTERN111 As Byte = 7
Public Const QR_ERROR_CORRECT_LEVEL_L As Byte = 1
Public Const QR_ERROR_CORRECT_LEVEL_M As Byte = 0
Public Const QR_ERROR_CORRECT_LEVEL_Q As Byte = 3
Public Const QR_ERROR_CORRECT_LEVEL_H As Byte = 2

Public bitsInitialized As Boolean

Public Const SNOBITS As Integer = 0
Public Const SBIT0   As Integer = 1
Public Const SBIT1   As Integer = SBIT0 * 2
Public Const SBIT2   As Integer = SBIT1 * 2
Public Const SBIT3   As Integer = SBIT2 * 2
Public Const SBIT4   As Integer = SBIT3 * 2
Public Const SBIT5   As Integer = SBIT4 * 2
Public Const SBIT6   As Integer = SBIT5 * 2
Public Const SBIT7   As Integer = SBIT6 * 2
Public Const SBIT8   As Integer = SBIT7 * 2
Public Const SBIT9   As Integer = SBIT8 * 2
Public Const SBIT10  As Integer = SBIT9 * 2
Public Const SBIT11  As Integer = SBIT10 * 2
Public Const SBIT12  As Integer = SBIT11 * 2
Public Const SBIT13  As Integer = SBIT12 * 2
Public Const SBIT14  As Integer = SBIT13 * 2
Public Const SBIT15  As Integer = (Not 0) And (Not _
(SBIT0 Or SBIT1 Or SBIT2 Or SBIT3 Or SBIT4 Or SBIT5 Or SBIT6 Or SBIT7 Or _
 SBIT8 Or SBIT9 Or SBIT10 Or SBIT11 Or SBIT12 Or SBIT13 Or SBIT14))


Public Const NOBITS As Long = 0
Public Const BIT0   As Long = 1
Public Const BIT1   As Long = BIT0 * 2
Public Const BIT2   As Long = BIT1 * 2
Public Const BIT3   As Long = BIT2 * 2
Public Const BIT4   As Long = BIT3 * 2
Public Const BIT5   As Long = BIT4 * 2
Public Const BIT6   As Long = BIT5 * 2
Public Const BIT7   As Long = BIT6 * 2
Public Const BIT8   As Long = BIT7 * 2
Public Const BIT9   As Long = BIT8 * 2
Public Const BIT10  As Long = BIT9 * 2
Public Const BIT11  As Long = BIT10 * 2
Public Const BIT12  As Long = BIT11 * 2
Public Const BIT13  As Long = BIT12 * 2
Public Const BIT14  As Long = BIT13 * 2
Public Const BIT15  As Long = BIT14 * 2
Public Const BIT16  As Long = BIT15 * 2
Public Const BIT17  As Long = BIT16 * 2
Public Const BIT18  As Long = BIT17 * 2
Public Const BIT19  As Long = BIT18 * 2
Public Const BIT20  As Long = BIT19 * 2
Public Const BIT21  As Long = BIT20 * 2
Public Const BIT22  As Long = BIT21 * 2
Public Const BIT23  As Long = BIT22 * 2
Public Const BIT24  As Long = BIT23 * 2
Public Const BIT25  As Long = BIT24 * 2
Public Const BIT26  As Long = BIT25 * 2
Public Const BIT27  As Long = BIT26 * 2
Public Const BIT28  As Long = BIT27 * 2
Public Const BIT29  As Long = BIT28 * 2
Public Const BIT30  As Long = BIT29 * 2
Public Const BIT31  As Long = (Not 0) And (Not _
(BIT0 Or BIT1 Or BIT2 Or BIT3 Or BIT4 Or BIT5 Or BIT6 Or BIT7 Or _
 BIT8 Or BIT9 Or BIT10 Or BIT11 Or BIT12 Or BIT13 Or BIT14 Or BIT15 Or _
 BIT16 Or BIT17 Or BIT18 Or BIT19 Or BIT20 Or BIT21 Or BIT22 Or BIT23 Or _
 BIT24 Or BIT25 Or BIT26 Or BIT27 Or BIT28 Or BIT29 Or BIT30))
  
Private Const BITS0thru4   As Long = BIT0 Or BIT1 Or BIT2 Or BIT3 Or BIT4
Private Const BITS0thru7   As Long = BIT0 Or BIT1 Or BIT2 Or BIT3 Or BIT4 Or BIT5 Or BIT6 Or BIT7
Private Const BITS8thru15  As Long = BIT8 Or BIT9 Or BIT10 Or BIT11 Or BIT12 Or BIT13 Or BIT14 Or BIT15
Private Const BITS16thru23 As Long = BIT16 Or BIT17 Or BIT18 Or BIT19 Or BIT20 Or BIT21 Or BIT22 Or BIT23
Private Const BITS24thru31 As Long = BIT24 Or BIT25 Or BIT26 Or BIT27 Or BIT28 Or BIT29 Or BIT30 Or BIT31

Public Const BYTE0 As Long = BITS0thru7
Public Const BYTE1 As Long = BITS8thru15
Public Const BYTE2 As Long = BITS16thru23
Public Const BYTE3 As Long = BITS24thru31

Private Const strOne As String = "1"
  
Public bits(31) As Long
Public SBits(15) As Integer

Private testbitvect() As Long
Private testbitvect2() As Long


Public qrDataList As Collection
Public QR_G15 As Long
Public QR_G18 As Long
Public QR_G15_MASK As Long

Public aQRMath

Public Sub Main()
    QR_G15 = ShiftLeft(1, 10) Or ShiftLeft(1, 8) Or ShiftLeft(1, 5) Or ShiftLeft(1, 4) Or ShiftLeft(1, 2) Or ShiftLeft(1, 1) Or ShiftLeft(1, 0)
    QR_G15_MASK = ShiftLeft(1, 14) Or ShiftLeft(1, 12) Or ShiftLeft(1, 10) Or ShiftLeft(1, 4) Or ShiftLeft(1, 1)
    QR_G18 = ShiftLeft(1, 12) Or ShiftLeft(1, 11) Or ShiftLeft(1, 10) Or ShiftLeft(1, 9) Or ShiftLeft(1, 8) Or ShiftLeft(1, 5) Or ShiftLeft(1, 2) Or ShiftLeft(1, 0)
    Set aQRMath = New QRMath
    Form1.Show
End Sub

Public Function trigger_error(sString As String, typeError As Integer)

End Function

Public Function Max(ByVal i As Long, ByVal j As Long) As Long
    If i > j Then
        Max = i
    Else
        Max = j
    End If
End Function

'Function ShiftRight(ByVal lngNumber As Long, ByVal intNumBits As Integer) As Long
'    ShiftRight = lngNumber \ 2 ^ intNumBits 'note the integer division op
'
'End Function

Function ShiftLeft(ByVal lngNumber As Long, ByVal intNumBits As Integer) As Long
ShiftLeft = lngNumber * 2 ^ intNumBits

End Function

'debug.Print drawdatamatrix("123456789012",300,300)
Public Function DrawDataMatrix(pictureB As PictureBox, strValue As String, width As Integer, height As Integer) As PictureBox
'Dim apicOb As Object
Dim xx, yy, angle As Integer
        apt = getDigit(strValue, False)
        Set apic = pictureB
        angle = 0 '90
        apic.AutoSize = 1
        apic.AutoRedraw = True
        apic.ScaleMode = vbPixels
        apic.width = width * 1.5
        apic.height = height * 1.5
        apic.ScaleWidth = width
        apic.ScaleHeight = height
        apic.DrawStyle = 0
        apic.FillStyle = 0
        apic.foreColor = vbBlack
        apic.FillColor = vbBlack
        apic.DrawWidth = 1

        apic.Cls
        drw = Ceil(apic.ScaleWidth / (UBound(apt, 1)) + 1) / 10
        xx = drw * UBound(apt, 1) / 2
        yy = xx
        apic.DrawWidth = 1 'drw
        apic.DrawStyle = 1
        digitToRenderer xx, yy, angle, drw, drw, apt
        apic.Refresh
        'apic.Picture = apic.image
        'SavePicture apic.image, "test.bmp"
        'Form1.Picture1.Picture = LoadPicture("test.bmp")
        
        
        
End Function

Function Ceil(n As Double) As Long
    If Not Int(n) = n Then
        Ceil = Int(n) + 1
    Else
        Ceil = n
    End If
    
    '-> test
    Ceil = n
    
    
End Function

Private Function fn(points As Variant)
    Dim pts(3) As POINTAPI
    Dim i As Integer
    
    i = 1
    pts(0).X = points(0) * i
    pts(0).Y = points(1) * i
    pts(1).X = points(2) * i
    pts(1).Y = points(3) * i
    pts(2).X = points(4) * i
    pts(2).Y = points(5) * i
    pts(3).X = points(6) * i
    pts(3).Y = points(7) * i
    
    Polygon apic.hdc, pts(0), UBound(pts) + 1
    'apic.PSet (points(0) * drw, points(1) * drw), QBColor(0)
End Function

Private Function Deg2Rad(ByVal degrees As Single) As Single
    Deg2Rad = degrees / 57.29578
End Function

Private Function digitToRenderer(xi, yi, angle, mw, mh, digit)
    'Dim X As Integer
    'Dim Y As Integer
    Dim z As Integer
    Dim xA As Integer
    Dim xB As Integer
    Dim xC As Integer
    Dim xD As Integer
    Dim yA As Integer
    Dim yB As Integer
    Dim yC As Integer
    Dim yD As Integer
    
    lines = UBound(digit) + 1
    columns = UBound(digit, 1) + 1
    angle = Deg2Rad(-angle)
    vcos = Cos(angle)
    vsin = Sin(angle)

    rotate columns * mw / 2, lines * mh / 2, vcos, vsin, X, Y
    xi = xi - X
    yi = yi - Y
    For Y = 0 To lines - 1
        X = -1
        While (X < columns - 1)
            X = X + 1
            If digit(Y, X) = "1" Then
                z = X
                While (z + 1 < columns) And (digit(Y, z + 1) = "1")
                    z = z + 1
                Wend
                x1 = X * mw
                y1 = Y * mh
                x2 = (z + 1) * mw
                y2 = (Y + 1) * mh
                rotate x1, y1, vcos, vsin, xA, yA
                rotate x2, y1, vcos, vsin, xB, yB
                rotate x2, y2, vcos, vsin, xC, yC
                rotate x1, y2, vcos, vsin, xD, yD
'Debug.Print X & "  xAyA:(" & (xA + xi) & "," & (yA + yi) & ") xByB:(" & (xB + xi) & "," & (yB + yi) & ") xCyC:(" & (xC + xi) & "," & (yC + yi) & ") xDyD:(" & (xD + xi) & "," & (yD + yi) & ")" & Chr(13)
                fn (Array(xA + xi, yA + yi, xB + xi, yB + yi, xC + xi, yC + yi, xD + xi, yD + yi))
                X = z + 1
            End If
        Wend
    Next
    digitToRenderer = Result(xi, yi, columns, lines, mw, mh, vcos, vsin)
End Function

Private Function rotate(x1, y1, vcos, vsin, ByRef X, ByRef Y)
        X = x1 * vcos - y1 * vsin
        Y = x1 * vsin + y1 * vcos
End Function

Public Function rotateP(x1, y1, angle, X, Y)
    angle = Deg2Rad(-angle)
    vcos = Cos(angle)
    vsin = Sin(angle)
    X = x1 * vcos - y1 * vsin
    Y = x1 * vsin + y1 * vcos
End Function

Private Function Result(xi, yi, columns, lines, mw, mh, vcos, vsin)
'       rotate 0, 0, vcos, vsin, x1, y1
'        rotate columns * mw, 0, vcos, vsin, x2, y2
'        rotate columns * mw, lines * mh, vcos, vsin, X3, Y3
'        rotate 0, lines * mh, vcos, vsin, x4, y4

'        return Array(
'            'width' => $columns * $mw,
'            'height'=> $lines * $mh,
'            'p1' => array(
'                'x' => $xi + $x1,
'                'y' => $yi + $y1
'            ),
'            'p2' => array(
'                'x' => $xi + $x2,
'                'y' => $yi + $y2
'            ),
'            'p3' => array(
'                'x' => $xi + $x3,
'                'y' => $yi + $y3
'            ),
'            'p4' => array(
'                'x' => $xi + $x4,
'                'y' => $yi + $y4
'            )
'        );
End Function


