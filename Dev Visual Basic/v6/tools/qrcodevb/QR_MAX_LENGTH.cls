VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QR_MAX_LENGTH"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private qrMaxLenght()

Private Sub Class_Initialize()
    qrMaxLenght = Array( _
            Array(Array(41, 25, 17, 10), Array(34, 20, 14, 8), Array(27, 16, 11, 7), Array(17, 10, 7, 4)), _
            Array(Array(77, 47, 32, 20), Array(63, 38, 26, 16), Array(48, 29, 20, 12), Array(34, 20, 14, 8)), _
            Array(Array(127, 77, 53, 32), Array(101, 61, 42, 26), Array(77, 47, 32, 20), Array(58, 35, 24, 15)), _
            Array(Array(187, 114, 78, 48), Array(149, 90, 62, 38), Array(111, 67, 46, 28), Array(82, 50, 34, 21)), _
            Array(Array(255, 154, 106, 65), Array(202, 122, 84, 52), Array(144, 87, 60, 37), Array(106, 64, 44, 27)), _
            Array(Array(322, 195, 134, 82), Array(255, 154, 106, 65), Array(178, 108, 74, 45), Array(139, 84, 58, 36)), _
            Array(Array(370, 224, 154, 95), Array(293, 178, 122, 75), Array(207, 125, 86, 53), Array(154, 93, 64, 39)), _
            Array(Array(461, 279, 192, 118), Array(365, 221, 152, 93), Array(259, 157, 108, 66), Array(202, 122, 84, 52)), _
            Array(Array(552, 335, 230, 141), Array(432, 262, 180, 111), Array(312, 189, 130, 80), Array(235, 143, 98, 60)), _
            Array(Array(652, 395, 271, 167), Array(513, 311, 213, 131), Array(364, 221, 151, 93), Array(288, 174, 119, 74)))
End Sub

Property Get getB(ByVal x As Integer, ByVal y As Integer, ByVal z As Integer) As Integer()
    getB = qrMaxLenght(x, y, z)
End Property

