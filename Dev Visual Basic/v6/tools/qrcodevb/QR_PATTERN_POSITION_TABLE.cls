VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QR_PATTERN_POSITION_TABLE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Dim QR_PATTERN_POSITION_TABLE


Private Sub Class_Initialize()
    
QR_PATTERN_POSITION_TABLE = Array( _
        Array(), Array(6, 18), Array(6, 22), Array(6, 26), Array(6, 30), Array(6, 34), Array(6, 22, 38), Array(6, 24, 42), _
        Array(6, 26, 46), Array(6, 28, 50), Array(6, 30, 54), Array(6, 32, 58), Array(6, 34, 62), Array(6, 26, 46, 66), Array(6, 26, 48, 70), _
        Array(6, 26, 50, 74), Array(6, 30, 54, 78), Array(6, 30, 56, 82), Array(6, 30, 58, 86), Array(6, 34, 62, 90), Array(6, 28, 50, 72, 94), Array(6, 26, 50, 74, 98), _
        Array(6, 30, 54, 78, 102), Array(6, 28, 54, 80, 106), Array(6, 32, 58, 84, 110), Array(6, 30, 58, 86, 114), Array(6, 34, 62, 90, 118), Array(6, 26, 50, 74, 98, 122), Array(6, 30, 54, 78, 102, 126), _
        Array(6, 26, 52, 78, 104, 130), Array(6, 30, 56, 82, 108, 134), Array(6, 34, 60, 86, 112, 138), Array(6, 30, 58, 86, 114, 142), Array(6, 34, 62, 90, 118, 146), Array(6, 30, 54, 78, 102, 126, 150), _
        Array(6, 24, 50, 76, 102, 128, 154), Array(6, 28, 54, 80, 106, 132, 158), Array(6, 32, 58, 84, 110, 136, 162), Array(6, 26, 54, 82, 110, 138, 166), Array(6, 30, 58, 86, 114, 142, 170) _
    )
    
End Sub


Property Get getB(index)
    getB = QR_PATTERN_POSITION_TABLE(index)
End Property
