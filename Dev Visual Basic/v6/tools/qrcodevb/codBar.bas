Attribute VB_Name = "codBar"
    Dim lengthRows() As Variant
    Private lengthCols() As Variant
    Private dataCWCount() As Variant
    Dim solomonCWCount() As Variant
    Private dataRegionRows() As Variant
    Private dataRegionCols() As Variant
    Private regionRows() As Variant
    Private regionCols() As Variant
    Private interleavedBlocks() As Variant
    Private logTab() As Variant
    Private aLogTab() As Variant ' Table of aLog for the Galois field
        
    Private Function champGaloisMult(a, b) As Integer
        'MULTIPLICATION IN GALOIS FIELD GF(2^8)
        If a = 0 Or b = 0 Then
            champGaloisMult = 0
            Exit Function
        End If
        champGaloisMult = aLogTab((logTab(a) + logTab(b)) Mod 255)
    End Function
    
    Private Function champGaloisDoub(a, b) As Integer
        'THE OPERATION a * 2^b IN GALOIS FIELD GF(2^8)
        If a = 0 Then
            champGaloisDoub = 0
            Exit Function
        End If
        If b = 0 Then
            champGaloisDoub = a
            Exit Function
        End If
        champGaloisDoub = aLogTab((logTab(a) + b) Mod 255)
    End Function
    
    Private Function champGaloisSum(a, b) As Integer
        'SUM IN GALOIS FIELD GF(2^8)
        champGaloisSum = a Xor b
    End Function
    
    Private Function selectIndex(dataCodeWordsCount, rectangular) As Integer
        ' CHOOSE THE GOOD INDEX FOR TABLES
        If ((dataCodeWordsCount < 1 Or dataCodeWordsCount > 1558) And Not rectangular) Then
            selectIndex = -1
            Exit Function
        End If
        If ((dataCodeWordsCount < 1 Or dataCodeWordsCount > 49) And rectangular) Then
            selectIndex = -1
        End If
        If rectangular Then
            n = 24
        Else
            n = 0
        End If
        Do While dataCWCount(n) < dataCodeWordsCount
            n = n + 1
        Loop
        selectIndex = n
    End Function
    
    Private Function encodeDataCodeWordsASCII(text) As Variant
        Dim dataCodeWords() As Integer
        ReDim dataCodeWords(0)
        Dim n As Integer
        n = 0
        leng = Len(text)
        For i = 0 To leng - 1
            c = Asc(Mid(text, i + 1, 1))
            If c > 127 Then
                dataCodeWords(n) = 235
                c = c - 127
                n = n + 1
            Else
                If ((c >= 48 And c <= 57) And (i + 1 < leng) And InStr(1, "0123456789", Mid(text, i + 2, 1)) <> 0) Then
                    c = ((c - 48) * 10) + CInt(Mid(text, i + 2, 1))
                    c = c + 130
                    i = i + 1
                Else
                    c = c + 1
                End If
            End If
            If UBound(dataCodeWords) < n Then ReDim Preserve dataCodeWords(n)
            dataCodeWords(n) = c
            n = n + 1
        Next
        encodeDataCodeWordsASCII = dataCodeWords
    End Function
    
    Private Function addPadCW(ByRef atab, afrom, ato)
        If afrom >= ato Then Exit Function
        If UBound(atab) < ato Then ReDim Preserve atab(ato - 1)
        atab(afrom) = 129
        For i = afrom + 1 To ato - 1
            r = ((149 * (i + 1)) Mod 253) + 1
            atab(i) = (129 + r) Mod 254
        Next
    End Function
    
    Private Function calculSolFactorTable(solomonCWCount) As Variant
        ' CALCULATE THE REED SOLOMON FACTORS
        ReDim g(0 To solomonCWCount)
        Dim i As Integer
        Dim j As Integer
        For i = 0 To solomonCWCount
            g(i) = 1
        Next
        For i = 1 To solomonCWCount
            For j = i - 1 To 0 Step -1
                g(j) = champGaloisDoub(g(j), i)
                If j > 0 Then
                    g(j) = champGaloisSum(g(j), g(j - 1))
                End If
            Next
        Next
        calculSolFactorTable = g
    End Function
    
    Private Function addReedSolomonCW(nSolomonCW, coeffTab, nDataCW, ByRef dataTab, blocks)
        ' Add the Reed Solomon codewords
        Dim i As Integer
        Dim j As Integer
        Dim k As Integer
        Dim correctionCW() As Integer
        errorBlocks = nSolomonCW / blocks
        ReDim correctionCW(0 To errorBlocks - 1)
        For k = 0 To blocks - 1
            For i = 0 To errorBlocks - 1
                correctionCW(i) = 0
            Next
            For i = k To nDataCW - 1 Step blocks
                Temp = champGaloisSum(dataTab(i), correctionCW(errorBlocks - 1))
                For j = errorBlocks - 1 To 0 Step -1
                    If Temp = 0 Then
                        correctionCW(j) = 0
                    Else
                        correctionCW(j) = champGaloisMult(Temp, coeffTab(j))
                    End If
                    If j > 0 Then correctionCW(j) = champGaloisSum(correctionCW(j - 1), correctionCW(j))
                Next
            Next
            ' Renversement des blocs calcules
            j = nDataCW + k
            For i = errorBlocks - 1 To 0 Step -1
                If UBound(dataTab) < j Then ReDim Preserve dataTab(0 To j)
                dataTab(j) = correctionCW(i)
                j = j + blocks
            Next
        Next
        addReedSolomonCW = dataTab
    End Function
    
    Function ShiftRight(ByVal lngNumber As Long, ByVal intNumBits As Integer) As Long
        '--------------
        'BIT SHIFT RIGHT
        '--------------
        ShiftRight = lngNumber \ 2 ^ intNumBits 'note the integer division op
    End Function

    Private Function getBits(entier) As Integer()
        ' Transform integer to tab of bits
        Dim bits() As Integer
        Dim i As Integer
        
        ReDim bits(0 To 7)
        For i = 0 To 7
            If entier And ShiftRight(128, i) Then
                bits(i) = 1
            Else
                bits(i) = 0
            End If
        Next
        getBits = bits
    End Function
    
    Private Function anext(etape, totalRows, totalCols, codeWordsBits, ByRef datamatrix, ByRef assigned)
        ' Place codewords into the matrix
        aChr = 0 ' Place of the 8st bit from the first character to (4)(0)
        row = 4
        col = 0
        Do
            ' Check for a special case of corner
            If ((row = totalRows) And (col = 0)) Then
                patternShapeSpecial1 datamatrix, assigned, codeWordsBits(aChr), totalRows, totalCols
                aChr = aChr + 1
            ElseIf (etape < 3) And (row = totalRows - 2) And (ol = 0) And (totalCols Mod 4 <> 0) Then
                patternShapeSpecial2 datamatrix, assigned, codeWordsBits(aChr), totalRows, totalCols
                aChr = aChr + 1
            ElseIf ((row = totalRows - 2) And (col = 0) And (totalCols Mod 8 = 4)) Then
                patternShapeSpecial3 datamatrix, assigned, codeWordsBits(aChr), totalRows, totalCols
                aChr = aChr + 1
            ElseIf ((row = totalRows + 4) And (col = 2) And (totalCols Mod 8 = 0)) Then
                patternShapeSpecial4 datamatrix, assigned, codeWordsBits(aChr), totalRows, totalCols
                aChr = aChr + 1
            End If

            ' Go up and right in the datamatrix
            Do
                If ((row < totalRows) And (col >= 0) And (Not isset(assigned, row, col) Or arrayValue(assigned, row, col) <> 1)) Then
'                    Debug.Print "nextA " & row & "#" & col
                    patternShapeStandard datamatrix, assigned, codeWordsBits(aChr), row, col, totalRows, totalCols
                    aChr = aChr + 1
                End If
                row = row - 2
                col = col + 2
            Loop While ((row >= 0) And (col < totalCols))
            row = row + 1
            col = col + 3
'Debug.Print "next AB" & row & "#" & col
            ' Go down and left in the datamatrix
            Do
                If ((row >= 0) And (col < totalCols) And (Not isset(assigned, row, col) Or arrayValue(assigned, row, col) <> 1)) Then
'                    Debug.Print "nextB " & row & "#" & col
                    patternShapeStandard datamatrix, assigned, codeWordsBits(aChr), row, col, totalRows, totalCols
                    aChr = aChr + 1
                Else
'                    Debug.Print "suite " & row & "#" & col
                End If
                row = row + 2
                col = col - 2
            Loop While ((row < totalRows) And (col >= 0))
            row = row + 3
            col = col + 1
        Loop While ((row < totalRows) Or (col < totalCols))
    End Function
    
    Private Function isset(obj, Optional a, Optional b) As Boolean
        On Error GoTo fin
        Dim i As Variant
        isset = False
        i = obj(a, b)
        isset = True
        If i = -1 Then isset = False
fin:
    End Function
    
    Private Function arrayValue(obj, a, b) As Variant
        arrayValue = Null
        On Error GoTo fin
        arrayValue = obj(a, b)
fin:
    End Function
    
    Private Function patternShapeStandard(ByRef datamatrix, ByRef assigned, bits, row, col, totalRows, totalCols) ' Place bits in the matrix (standard or special case)
'Debug.Print "datamatrix, assigned, bits, row, col, totalRows, totalCols => " & "|" & "|" & "|" & "|" & row & "|" & col & "|" & totalRows & "|" & totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(0), row - 2, col - 2, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(1), row - 2, col - 1, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(2), row - 1, col - 2, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(3), row - 1, col - 1, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(4), row - 1, col, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(5), row, col - 2, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(6), row, col - 1, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(7), row, col, totalRows, totalCols
    End Function
    
    Private Function patternShapeSpecial1(ByRef datamatrix, ByRef assigned, bits, totalRows, totalCols)
        placeBitInDatamatrix datamatrix, assigned, bits(0), totalRows - 1, 0, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(1), totalRows - 1, 1, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(2), totalRows - 1, 2, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(3), 0, totalCols - 2, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(4), 0, totalCols - 1, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(5), 1, totalCols - 1, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(6), 2, totalCols - 1, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(7), 3, totalCols - 1, totalRows, totalCols
    End Function
    
    Private Function patternShapeSpecial2(ByRef datamatrix, ByRef assigned, bits, totalRows, totalCols)
        placeBitInDatamatrix datamatrix, assigned, bits(0), totalRows - 3, 0, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(1), totalRows - 2, 0, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(2), totalRows - 1, 0, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(3), 0, totalCols - 4, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(4), 0, totalCols - 3, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(5), 0, totalCols - 2, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(6), 0, totalCols - 1, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(7), 1, totalCols - 1, totalRows, totalCols
    End Function
    
    Private Function patternShapeSpecial3(ByRef datamatrix, ByRef assigned, bits, totalRows, totalCols)
        placeBitInDatamatrix datamatrix, assigned, bits(0), totalRows - 3, 0, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(1), totalRows - 2, 0, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(2), totalRows - 1, 0, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(3), 0, totalCols - 2, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(4), 0, totalCols - 1, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(5), 1, totalCols - 1, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(6), 2, totalCols - 1, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(7), 3, totalCols - 1, totalRows, totalCols
    End Function
    
    Private Function patternShapeSpecial4(ByRef datamatrix, ByRef assigned, bits, totalRows, totalCols)
        placeBitInDatamatrix datamatrix, assigned, bits(0), totalRows - 1, 0, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(1), totalRows - 1, totalCols - 1, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(2), 0, totalCols - 3, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(3), 0, totalCols - 2, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(4), 0, totalCols - 1, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(5), 1, totalCols - 3, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(6), 1, totalCols - 2, totalRows, totalCols
        placeBitInDatamatrix datamatrix, assigned, bits(7), 1, totalCols - 1, totalRows, totalCols
    End Function
    
    Private Function placeBitInDatamatrix(ByRef datamatrix, ByRef assigned, ByVal bit, ByVal row, ByVal col, totalRows, totalCols)
        'Debug.Print "" & row & "," & col & "," & totalRows & "," & totalCols
        ' Put a bit into the matrix
        If (row < 0) Then
            row = row + totalRows
            col = col + 4 - ((totalRows + 4) Mod 8)
        End If
        If (col < 0) Then
            col = col + totalCols
            row = row + 4 - ((totalCols + 4) Mod 8)
        End If
        If UBound(assigned, 1) < row Or UBound(assigned, 2) < col Then
            ReDim Preserve assigned(0 To UBound(assigned, 1), 0 To col)
            datamatrix(row, col) = bit
            assigned(row, col) = 1
        ElseIf assigned(row, col) <> 1 Then
            datamatrix(row, col) = bit
            assigned(row, col) = 1
        End If
    End Function
    
    Private Function addFinderPattern(datamatrix, rowsRegion, colsRegion, rowsRegionCW, colsRegionCW) As Integer()
        ' Add the finder pattern
        totalRowsCW = (rowsRegionCW + 2) * rowsRegion
        totalColsCW = (colsRegionCW + 2) * colsRegion

        Dim datamatrixTemp() As Integer
        ReDim datamatrixTemp(0 To totalRowsCW + 1, 0)
        Dim i As Integer

        For i = 0 To totalRowsCW - 1
            ReDim Preserve datamatrixTemp(0 To totalRowsCW + 1, 0 To totalColsCW + 1)
            datamatrixTemp(i + 1, 0) = 0
            datamatrixTemp(i + 1, totalColsCW + 1) = 0
            For j = 0 To totalColsCW - 1
                If i Mod (rowsRegionCW + 2) = 0 Then
                    If j Mod 2 = 0 Then
                        datamatrixTemp(i + 1, j + 1) = 1
                    Else
                        datamatrixTemp(i + 1, j + 1) = 0
                    End If
                ElseIf i Mod (rowsRegionCW + 2) = rowsRegionCW + 1 Then
                    datamatrixTemp(i + 1, j + 1) = 1
                ElseIf j Mod (colsRegionCW + 2) = colsRegionCW + 1 Then
                    If i Mod 2 = 0 Then
                        datamatrixTemp(i + 1, j + 1) = 0
                    Else
                        datamatrixTemp(i + 1, j + 1) = 1
                    End If
                ElseIf j Mod (colsRegionCW + 2) = 0 Then
                    datamatrixTemp(i + 1, j + 1) = 1
                Else
                    datamatrixTemp(i + 1, j + 1) = 0
                    datamatrixTemp(i + 1, j + 1) = datamatrix(i - 1 - (2 * (Fix(i / (rowsRegionCW + 2)))), j - 1 - (2 * (Fix(j / (colsRegionCW + 2))))) ' todo : parseInt => ?
                End If
            Next
        Next
        'Dim datamatrixTemp2() As Integer
        'ReDim datamatrixTemp2(0 To totalRowsCW + 1, 0 To totalColsCW + 1)
        'For i = 0 To totalRowsCW + 1
        '    For j = 0 To totalColsCW + 1
        '        datamatrixTemp2(i, j) = datamatrixTemp(i, j)
        '    Next
        'Next
        For j = 0 To totalColsCW + 1
            datamatrixTemp(totalRowsCW + 1, j) = 0
        Next
        addFinderPattern = datamatrixTemp
    End Function
    
    Public Function getDigit(text, rectangular)
        lengthRows = Array(10, 12, 14, 16, 18, 20, 22, 24, 26, 32, 36, 40, 44, 48, 52, 64, 72, 80, 88, 96, 104, 120, 132, 144, 8, 8, 12, 12, 16, 16)
        lengthCols = Array(10, 12, 14, 16, 18, 20, 22, 24, 26, 32, 36, 40, 44, 48, 52, 64, 72, 80, 88, 96, 104, 120, 132, 144, 18, 32, 26, 36, 36, 48)
        dataCWCount = Array(3, 5, 8, 12, 18, 22, 30, 36, 44, 62, 86, 114, 144, 174, 204, 280, 368, 456, 576, 696, 816, 1050, 1304, 1558, 5, 10, 16, 22, 32, 49)
        solomonCWCount = Array(5, 7, 10, 12, 14, 18, 20, 24, 28, 36, 42, 48, 56, 68, 84, 112, 144, 192, 224, 272, 336, 408, 496, 620, 7, 11, 14, 18, 24, 28)
        dataRegionRows = Array(8, 10, 12, 14, 16, 18, 20, 22, 24, 14, 16, 18, 20, 22, 24, 14, 16, 18, 20, 22, 24, 18, 20, 22, 6, 6, 10, 10, 14, 14)
        dataRegionCols = Array(8, 10, 12, 14, 16, 18, 20, 22, 24, 14, 16, 18, 20, 22, 24, 14, 16, 18, 20, 22, 24, 18, 20, 22, 16, 14, 24, 16, 16, 22)
        regionRows = Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4, 6, 6, 6, 1, 1, 1, 1, 1, 1)
        regionCols = Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4, 6, 6, 6, 1, 2, 1, 2, 2, 2)
        interleavedBlocks = Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 4, 4, 4, 4, 6, 6, 8, 8, 1, 1, 1, 1, 1, 1)
        logTab = Array(-255, 255, 1, 240, 2, 225, 241, 53, 3, _
            38, 226, 133, 242, 43, 54, 210, 4, 195, 39, 114, 227, 106, 134, 28, _
            243, 140, 44, 23, 55, 118, 211, 234, 5, 219, 196, 96, 40, 222, 115, _
            103, 228, 78, 107, 125, 135, 8, 29, 162, 244, 186, 141, 180, 45, 99, _
            24, 49, 56, 13, 119, 153, 212, 199, 235, 91, 6, 76, 220, 217, 197, _
            11, 97, 184, 41, 36, 223, 253, 116, 138, 104, 193, 229, 86, 79, 171, _
            108, 165, 126, 145, 136, 34, 9, 74, 30, 32, 163, 84, 245, 173, 187, _
            204, 142, 81, 181, 190, 46, 88, 100, 159, 25, 231, 50, 207, 57, 147, _
            14, 67, 120, 128, 154, 248, 213, 167, 200, 63, 236, 110, 92, 176, 7, _
            161, 77, 124, 221, 102, 218, 95, 198, 90, 12, 152, 98, 48, 185, 179, _
            42, 209, 37, 132, 224, 52, 254, 239, 117, 233, 139, 22, 105, 27, 194, _
            113, 230, 206, 87, 158, 80, 189, 172, 203, 109, 175, 166, 62, 127, _
            247, 146, 66, 137, 192, 35, 252, 10, 183, 75, 216, 31, 83, 33, 73, _
            164, 144, 85, 170, 246, 65, 174, 61, 188, 202, 205, 157, 143, 169, 82, _
            72, 182, 215, 191, 251, 47, 178, 89, 151, 101, 94, 160, 123, 26, 112, _
            232, 21, 51, 238, 208, 131, 58, 69, 148, 18, 15, 16, 68, 17, 121, 149, _
            129, 19, 155, 59, 249, 70, 214, 250, 168, 71, 201, 156, 64, 60, 237, _
            130, 111, 20, 93, 122, 177, 150)
        
        aLogTab = Array( _
            1, 2, 4, 8, 16, 32, 64, 128, 45, 90, _
            180, 69, 138, 57, 114, 228, 229, 231, 227, 235, 251, 219, 155, 27, 54, _
            108, 216, 157, 23, 46, 92, 184, 93, 186, 89, 178, 73, 146, 9, 18, 36, _
            72, 144, 13, 26, 52, 104, 208, 141, 55, 110, 220, 149, 7, 14, 28, 56, _
            112, 224, 237, 247, 195, 171, 123, 246, 193, 175, 115, 230, 225, 239, _
            243, 203, 187, 91, 182, 65, 130, 41, 82, 164, 101, 202, 185, 95, 190, _
            81, 162, 105, 210, 137, 63, 126, 252, 213, 135, 35, 70, 140, 53, 106, _
            212, 133, 39, 78, 156, 21, 42, 84, 168, 125, 250, 217, 159, 19, 38, 76, _
            152, 29, 58, 116, 232, 253, 215, 131, 43, 86, 172, 117, 234, 249, 223, _
            147, 11, 22, 44, 88, 176, 77, 154, 25, 50, 100, 200, 189, 87, 174, 113, _
            226, 233, 255, 211, 139, 59, 118, 236, 245, 199, 163, 107, 214, 129, _
            47, 94, 188, 85, 170, 121, 242, 201, 191, 83, 166, 97, 194, 169, 127, _
            254, 209, 143, 51, 102, 204, 181, 71, 142, 49, 98, 196, 165, 103, 206, _
            177, 79, 158, 17, 34, 68, 136, 61, 122, 244, 197, 167, 99, 198, 161, _
            111, 222, 145, 15, 30, 60, 120, 240, 205, 183, 67, 134, 33, 66, 132, _
            37, 74, 148, 5, 10, 20, 40, 80, 160, 109, 218, 153, 31, 62, 124, 248, _
            221, 151, 3, 6, 12, 24, 48, 96, 192, 173, 119, 238, 241, 207, 179, 75, _
            150, 1)
        
        
        dataCodeWords = encodeDataCodeWordsASCII(text) ' Code the text in the ASCII mode
        adataCWCount = UBound(dataCodeWords) + 1
        index = selectIndex(adataCWCount, rectangular) ' Select the index for the data tables
        totalDataCWCount = dataCWCount(index) ' Number of data CW
        asolomonCWCount = solomonCWCount(index) ' Number of Reed Solomon CW
        totalCWCount = totalDataCWCount + asolomonCWCount ' Number of CW
        rowsTotal = lengthRows(index) ' Size of symbol
        colsTotal = lengthCols(index)
        rowsRegion = regionRows(index) ' Number of region
        colsRegion = regionCols(index)
        rowsRegionCW = dataRegionRows(index)
        colsRegionCW = dataRegionCols(index)
        rowsLengthMatrice = rowsTotal - 2 * rowsRegion ' Size of matrice data
        colsLengthMatrice = colsTotal - 2 * colsRegion
        blocks = interleavedBlocks(index)  ' Number of Reed Solomon blocks
        errorBlocks = asolomonCWCount / blocks

        addPadCW dataCodeWords, adataCWCount, totalDataCWCount ' Add codewords pads

        g = calculSolFactorTable(errorBlocks) ' Calculate correction coefficients

        addReedSolomonCW asolomonCWCount, g, totalDataCWCount, dataCodeWords, blocks ' Add Reed Solomon codewords

        Dim codeWordsBits() As Variant ' Calculte bits from codewords
        ReDim codeWordsBits(0 To totalCWCount - 1)
        For i = 0 To totalCWCount - 1
            If UBound(codeWordsBits) < i Then ReDim Preserve codeWordsBits(0 To i, 7)
            codeWordsBits(i) = getBits(dataCodeWords(i))
        Next

        Dim datamatrix() As Integer
        ReDim datamatrix(0 To colsLengthMatrice - 1, 0 To colsLengthMatrice - 1)
        Dim assigned() As Integer
        ReDim assigned(0 To colsLengthMatrice - 1, 0 To colsLengthMatrice - 1)
        
        For i = 0 To colsLengthMatrice - 1
            For j = 0 To colsLengthMatrice - 1
                datamatrix(i, j) = 0
                assigned(i, j) = -1
            Next
        Next

        ' Add the bottom-right corner if needed
        If (((rowsLengthMatrice * colsLengthMatrice) Mod 8) = 4) Then
            datamatrix(rowsLengthMatrice - 2, colsLengthMatrice - 2) = 1
            datamatrix(rowsLengthMatrice - 1, colsLengthMatrice - 1) = 1
            datamatrix(rowsLengthMatrice - 1, colsLengthMatrice - 2) = 0
            datamatrix(rowsLengthMatrice - 2, colsLengthMatrice - 1) = 0
            assigned(rowsLengthMatrice - 2, colsLengthMatrice - 2) = 1
            assigned(rowsLengthMatrice - 1, colsLengthMatrice - 1) = 1
            assigned(rowsLengthMatrice - 1, colsLengthMatrice - 2) = 1
            assigned(rowsLengthMatrice - 2, colsLengthMatrice - 1) = 1
        End If

        ' Put the codewords into the matrix
        anext 0, rowsLengthMatrice, colsLengthMatrice, codeWordsBits, datamatrix, assigned

        ' Add the finder pattern
        datamatrix = addFinderPattern(datamatrix, rowsRegion, colsRegion, rowsRegionCW, colsRegionCW)
        getDigit = datamatrix

    End Function




