Attribute VB_Name = "Module1"
Option Explicit

Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal Hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Private Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Private Declare Function GetEnvironmentVariable Lib "kernel32" Alias "GetEnvironmentVariableA" (ByVal lpName As String, ByVal lpBuffer As String, ByVal nSize As Long) As Long

Public Function GetIniString(AppName As String, lpKeyName As String, IniFile As String, Error As Boolean) As String

Dim Res As Long
Dim lpBuffer As String


lpBuffer = Space$(2000)
Res = GetPrivateProfileString(AppName, lpKeyName, "NULL", lpBuffer, Len(lpBuffer), IniFile)
lpBuffer = Entry(1, lpBuffer, Chr(0))
If lpBuffer = "NULL" Then
    If Error Then
        MsgBox "Impossible de trouver la cl� : " & lpKeyName & _
               " dans la section : " & AppName & " du fichier : " & _
                IniFile, vbCritical + vbOKOnly, "Erreur de lecture"
    End If
    GetIniString = ""
Else
    lpBuffer = Mid$(lpBuffer, 1, Res)
    GetIniString = lpBuffer
End If


End Function

Public Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Variant

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Long
Dim i As Long
Dim PosAnalyse As Long

On Error GoTo GestError

PosAnalyse = 1

Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1

Exit Function

GestError:

    '-> Dans ce cas la, renvoyer 1
    NumEntries = 1
    

End Function

Public Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Long
Dim PosEnCour As Long
Dim i As Long
Dim CHarDeb As Long
Dim CharEnd As Long

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + 1 > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid$(Vecteur, PosEnCour + 1, Len(Vecteur) - PosEnCour + 1)
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid$(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + 1
Loop
        
End Function

Public Function GetVariableEnv(strVariable As String) As String

Dim Res As Long
Dim lpBuffer As String

lpBuffer = Space$(500)
Res = GetEnvironmentVariable(strVariable, lpBuffer, Len(lpBuffer))
If Res <> 0 Then
    '-> Faire un trim sur le buffer
    lpBuffer = Mid$(lpBuffer, 1, Res)
Else
    lpBuffer = ""
End If

'-> Retouner la valeur
GetVariableEnv = lpBuffer

End Function

Public Function GetFileName(MyFile As String) As String

Dim i As Integer
MyFile = Replace(MyFile, "/", "\")
i = InStrRev(MyFile, "\")
If i = 0 Then
    GetFileName = MyFile
Else
    GetFileName = Mid$(MyFile, i + 1, Len(MyFile) - i)
End If


End Function

