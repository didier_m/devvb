VERSION 5.00
Begin VB.UserControl ctlDeal 
   ClientHeight    =   1125
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5100
   ScaleHeight     =   1125
   ScaleWidth      =   5100
   Begin VB.TextBox urlFile 
      Height          =   315
      Left            =   120
      TabIndex        =   1
      Text            =   "c:\"
      Top             =   120
      Width           =   3255
   End
   Begin VB.CommandButton cmdUrl 
      Caption         =   "Get Poids"
      Height          =   345
      Left            =   3480
      TabIndex        =   0
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Deal Informatique"
      ForeColor       =   &H0000C000&
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   840
      Width           =   3495
   End
End
Attribute VB_Name = "ctlDeal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function GetPoids(Optional strPath As String) As String
Dim strVal As String
'-> on reccupere les resultats
strVal = GetIniString("POIDS", "BRUT", strPath & "\poids.txt", False)
strVal = strVal + "|" + GetIniString("POIDS", "NET", strPath & "\poids.txt", False)
strVal = strVal + "|" + GetIniString("POIDS", "TARE", strPath & "\poids.txt", False)

GetPoids = strVal

End Function

Public Function info()
MsgBox "info"
End Function

Private Sub cmdUrl_Click()
    MsgBox (GetPoids(urlFile.Text))
    
End Sub
