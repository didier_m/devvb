VERSION 5.00
Begin VB.UserControl ctlDeal 
   ClientHeight    =   1875
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7290
   ScaleHeight     =   1875
   ScaleWidth      =   7290
   Begin VB.TextBox urlFile 
      Height          =   435
      Left            =   120
      TabIndex        =   1
      Top             =   330
      Width           =   3975
   End
   Begin VB.CommandButton cmdUrl 
      Caption         =   "Ean128 & SSCC"
      Height          =   585
      Left            =   5640
      TabIndex        =   0
      Top             =   240
      Width           =   1470
   End
   Begin VB.Label Label2 
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   960
      Width           =   6975
   End
   Begin VB.Label Label1 
      Caption         =   "Deal Informatique"
      ForeColor       =   &H0000C000&
      Height          =   255
      Left            =   5640
      TabIndex        =   2
      Top             =   1560
      Width           =   1575
   End
End
Attribute VB_Name = "ctlDeal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public turboSavePath As String

Public Function GetScan(Optional strPath As String) As String
Dim strVal As String
If strPath = "" Then strPath = "C:\scan.txt"
'-> on reccupere les resultats
strVal = GetIniString("SCAN", "LECTURE", strPath, False)
strVal = strVal + "|" + GetIniString("SCAN", "EAN128", strPath, False)
strVal = strVal + "|" + GetIniString("SCAN", "SSCC", strPath, False)
SetIniString "SCAN", "EAN128", strPath, ""
SetIniString "SCAN", "SSCC", strPath, ""
SetIniString "SCAN", "LECTURE", strPath, ""
GetScan = strVal
Label2.Caption = strVal
End Function

Public Function AskPoids(Optional strPath As String) As String
Dim strVal As String
'-> on reccupere les resultats
SetIniString "POIDS", "ASK", strPath & "\poids.txt", "TRUE"

End Function

Private Sub cmdUrl_Click()
    GetScan (urlFile.Text)
End Sub
