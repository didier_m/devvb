VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "maqTrans"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'-> Liste des rangs charg�s dans le fichier
Public Value As String

Private Sub Class_Initialize()
    '-> Initialiser la collection des spools
End Sub

Public Function SetRang(Rang As String, strLigne As String) As Boolean
    On Error Resume Next
    Dim aRang As maqTrans
    strLigne = Mid(strLigne, 2, Len(strLigne) - 1)
    If isRang(Rang) Then
        Set aRang = Rangs(Rang)
        aRang.Value = aRang.Value + Chr(13) + Chr(10) + strLigne
    Else
        Set aRang = New maqTrans
        aRang.Value = strLigne
        Rangs.Add aRang, Rang
    End If
    SetRang = True
End Function


Public Function SetValue(Ligne As String) As String
    'cr�ation d'une ligne de spool
    Dim aRang As maqTrans
    Dim Rang As String
        'on va inserer toute les valeurs
        Rang = Entry(1, Ligne, ";")
        expLigne = Split(Ligne, ";")
        iSize = UBound(expLigne)
        If isRang(Rang) Then
            Set aRang = Rangs(Rang)
        Else
            SetValue = ""
            Exit Function
        End If
        expRgLig = Split(aRang.Value, Chr(13))
        fullrang = ""
        For j = 0 To UBound(expRgLig)
            expRang = Split(expRgLig(j), "#")
            strRang = expRang(0)
            For i = 0 To UBound(expRang) - 1
                expdiese = Split(expRang(i), "^")
                diese = Replace(expdiese(1), "}""", "")
                diese = Trim(diese)
                expLigne = Split(Ligne, "^" + diese + "=")
                If UBound(expLigne) = 1 Then
                    strValue = Entry(1, expLigne(1), ";")
                Else
                    strValue = ""
                End If
                strRang = strRang + strValue + Replace(Mid(expRang(i + 1), 4), "}""", "}")
            Next
            fullrang = fullrang + strRang + Chr(13)
        Next
        SetValue = fullrang
End Function






