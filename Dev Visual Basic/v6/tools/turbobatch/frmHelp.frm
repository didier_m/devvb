VERSION 5.00
Begin VB.Form frmHelp 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Commandes TurboBatch"
   ClientHeight    =   3135
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5100
   Icon            =   "frmHelp.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3135
   ScaleWidth      =   5100
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "OK"
      Height          =   375
      Left            =   1680
      TabIndex        =   6
      Top             =   2640
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "/HELP : Affiche l'aide en ligne du TurboBatch"
      Height          =   255
      Index           =   5
      Left            =   240
      TabIndex        =   5
      Top             =   2040
      Width           =   6735
   End
   Begin VB.Label Label1 
      Caption         =   "/? : Affiche le descriptif des commandes"
      Height          =   255
      Index           =   4
      Left            =   240
      TabIndex        =   4
      Top             =   1680
      Width           =   6735
   End
   Begin VB.Label Label1 
      Caption         =   "/LOG : Affiche la fen�tre des Log"
      Height          =   255
      Index           =   3
      Left            =   240
      TabIndex        =   3
      Top             =   1320
      Width           =   6735
   End
   Begin VB.Label Label1 
      Caption         =   "/STOP : Arr�te la session du TruboBatch en cours de traitement"
      Height          =   255
      Index           =   2
      Left            =   240
      TabIndex        =   2
      Top             =   960
      Width           =   6735
   End
   Begin VB.Label Label1 
      Caption         =   "/RUN : Lance TurboBatch sans afficher l'interface"
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   1
      Top             =   600
      Width           =   6735
   End
   Begin VB.Label Label1 
      Caption         =   "/PARAM : Affiche l'interface de TurboBatch"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   6735
   End
End
Attribute VB_Name = "frmHelp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Command1_Click()

Unload Me

End Sub
