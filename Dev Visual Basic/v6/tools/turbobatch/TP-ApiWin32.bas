Attribute VB_Name = "ApiWin32"
'-> S�parateur de millier : gestion des variables d'environnement
Public Const LOCALE_STHOUSAND = &HF
Public Const LOCALE_USER_DEFAULT& = &H400
Public Const LOCALE_SDECIMAL = 14
'-> Pour gestion des process m�moire
Public Const PROCESS_QUERY_INFORMATION = &H400
Public Const STILL_ACTIVE = &H103

'-> Structures pour GDI
Public Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Public Type POINTAPI
    X As Long
    y As Long
End Type

'-> Structures pour gestion du code RTF
Public Type CHARRANGE
    cpMin As Long
    cpMax As Long
End Type


Public Type FORMATRANGE
    hDC As Long
    hdcTarget As Long
    rc As RECT
    rcPage As RECT
    chrg As CHARRANGE
End Type


'-> API pour fen�tre parcourir
Public Type BROWSEINFO
    hWndOwner      As Long
    pidlRoot       As Long
    pszDisplayName As Long
    lpszTitle      As Long
    ulFlags        As Long
    lpfnCallback   As Long
    lParam         As Long
    iImage         As Long
End Type
Public Const BIF_RETURNONLYFSDIRS = 1
Public Const BIF_DONTGOBELOWDOMAIN = 2
Public Declare Function SHBrowseForFolder Lib "shell32" (lpbi As BROWSEINFO) As Long
Public Declare Function SHGetPathFromIDList Lib "shell32" (ByVal pidList As Long, ByVal lpBuffer As String) As Long
Public Declare Function lstrcat Lib "kernel32" Alias "lstrcatA" (ByVal lpString1 As String, ByVal lpString2 As String) As Long
Public Declare Function EnumWindows Lib "user32" (ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long
Public Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hWnd As Long, ByVal lpString As String, ByVal cch As Long) As Long

'-> API pour r�cup�ration des variables d'environnement
Public Declare Function GetEnvironmentVariable Lib "kernel32" Alias "GetEnvironmentVariableA" (ByVal lpName As String, ByVal lpBuffer As String, ByVal nSize As Long) As Long

'-> API de lecture des fichiers au format "*.ini"
Public Declare Function GetPrivateProfileSection& Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As Any, ByVal lpFileName As String)
Public Declare Function WritePrivateProfileSection& Lib "kernel32" Alias "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpString As String, ByVal lpFileName As String)

'-> API pour gestion d'une fen�tre
Public Declare Function GetClientRect& Lib "user32" (ByVal hWnd As Long, lpRect As RECT)
Public Declare Function GetSystemMetrics& Lib "user32" (ByVal nIndex As Long)

'-> API pour gestion des regions
Public Declare Function CreateRectRgn& Lib "gdi32" (ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long)
Public Declare Function SelectClipRgn& Lib "gdi32" (ByVal hDC As Long, ByVal hRgn As Long)
Public Declare Function GetRgnBox& Lib "gdi32" (ByVal hRgn As Long, lpRect As RECT)
Public Declare Function GetClipRgn& Lib "gdi32" (ByVal hDC As Long, ByVal hRgn As Long)

'-> Api pour Dessin
Public Declare Function CreateSolidBrush& Lib "gdi32" (ByVal crColor As Long)
Public Declare Function CreatePen& Lib "gdi32" (ByVal nPenStyle As Long, ByVal nWidth As Long, ByVal crColor As Long)
Public Declare Function DeleteObject& Lib "gdi32" (ByVal hObject As Long)
Public Declare Function GetStockObject& Lib "gdi32" (ByVal nIndex As Long)
Public Declare Function SelectObject& Lib "gdi32" (ByVal hDC As Long, ByVal hObject As Long)
Public Declare Function Rectangle& Lib "gdi32" (ByVal hDC As Long, ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long)
Public Declare Function LineTo& Lib "gdi32" (ByVal hDC As Long, ByVal X As Long, ByVal y As Long)
Public Declare Function DrawText& Lib "user32" Alias "DrawTextA" (ByVal hDC As Long, ByVal lpStr As String, ByVal nCount As Long, lpRect As RECT, ByVal wFormat As Long)
Public Declare Function FrameRect& Lib "user32" (ByVal hDC As Long, lpRect As RECT, ByVal hBrush As Long)
Public Declare Function GetDC Lib "user32" (ByVal hWnd As Long) As Long
Public Declare Function ReleaseDC Lib "user32" (ByVal hWnd As Long, ByVal hDC As Long) As Long

'-> Api pour gestion du Registre
Public Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
Public Declare Function RegOpenKey Lib "advapi32.dll" Alias "RegOpenKeyA" (ByVal hKey As Long, ByVal lpSubKey As String, phkResult As Long) As Long
Public Declare Function RegSetValueEx Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, ByVal dwType As Long, lpData As Any, ByVal cbData As Long) As Long
Public Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long

Public Const REG_SZ = 1
Public Const HKEY_CURRENT_USER = &H80000001
Public Const HKEY_LOCAL_MACHINE = &H80000002

'-> Pour envoie � la poubelle
Public Type SHFILEOPSTRUCT
   hWnd        As Long
   wFunc       As Long
   pFrom       As String
   pTo         As String
   fFlags      As Integer
   fAborted    As Boolean
   hNameMaps   As Long
   sProgress   As String
 End Type
  
Public Const FO_DELETE = &H3
Public Const FOF_ALLOWUNDO = &H40
Public Declare Function SHFileOperation Lib "shell32.dll" Alias "SHFileOperationA" (lpFileOp As SHFILEOPSTRUCT) As Long

'-> Pour ouverture du site DEAL INFORMATIQUE
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

'-> Api gestion des fichiers tempo
Public Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Public Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long

'-> Api param�trage system
Public Const LOCALE_SYSTEM_DEFAULT& = &H800
Public Declare Function GetLocaleInfo& Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long)
Public Declare Function GetSystemDirectory& Lib "kernel32" Alias "GetSystemDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long)
Public Declare Function GetWindowsDirectory Lib "kernel32" Alias "GetWindowsDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long

'-> Api pour gestion des fichiers
Public Declare Function CloseHandle& Lib "kernel32" (ByVal hObject As Long)
Public Declare Function OpenFile& Lib "kernel32" (ByVal lpFileName As String, lpReOpenBuff As OFSTRUCT, ByVal wStyle As Long)
Public Declare Function GetFileSize& Lib "kernel32" (ByVal hFile As Long, lpFileSizeHigh As Long)
Public Declare Function CopyFile& Lib "kernel32" Alias "CopyFileA" (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal bFailIfExists As Long)

'-> Api pour gestion de l'impression au format RTF
Public Declare Function SendMessage& Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any)
Public Declare Function SendMessageBis& Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Long)
Public Declare Function SetMapMode& Lib "gdi32" (ByVal hDC As Long, ByVal nMapMode As Long)
Public Declare Function GetDeviceCaps Lib "gdi32" (ByVal hDC As Long, ByVal nIndex As Long) As Long
Public Declare Function MoveToEx Lib "gdi32" (ByVal hDC As Long, ByVal X As Long, ByVal y As Long, lpPoint As POINTAPI) As Long
Public Declare Function RoundRect Lib "gdi32" (ByVal hDC As Long, ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long, ByVal X3 As Long, ByVal Y3 As Long) As Long

'-> API de temporisation
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

'-> API pour gestion des process
Public Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Public Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Long, lpExitCode As Long) As Long

'-> Pour gestion des codes RTF et SendMessage
Public Const HORZRES& = 8
Public Const VERTRES& = 10
Public Const MM_TEXT& = 1
Public Const WM_GETTEXTLENGTH& = &HE
Public Const WM_USER& = &H400
Public Const WM_SETTEXT = &HC
Public Const EM_FORMATRANGE = WM_USER + 57

'-> Pour Marges internes du p�riph�rique
Public Const PHYSICALOFFSETX& = 112
Public Const PHYSICALOFFSETY& = 113
Public Const PHYSICALWIDTH& = 110
Public Const PHYSICALHEIGHT& = 111

'-> Pour objets GDI
Public Const GRAY_BRUSH& = 2
Public Const PS_SOLID& = 0
Public Const PS_NULL& = 5

'-> Constantes pour Message de Windows
Public Const WM_PAINT& = &HF
Public Const WM_SIZE = &H5

'-> Constantes pour DrawText
Public Const DT_SINGLELINE& = &H20
Public Const DT_TOP& = &H0
Public Const DT_VCENTER& = &H4
Public Const DT_BOTTOM& = &H8
Public Const DT_LEFT& = &H0
Public Const DT_CENTER& = &H1
Public Const DT_RIGHT& = &H2
Public Const DT_WORDBREAK& = &H10
Public Const DT_CALCRECT& = &H400
Public Const DT_NOPREFIX = &H800

'-> Pour caract�ristiques d'un DC
Public Const SM_CXEDGE& = 45

'-> Pour gestion des fichiers
Public Const OFS_MAXPATHNAME& = 128
Public Const OF_EXIST& = &H4000
Public Const OF_READ& = &H0


Public Type OFSTRUCT
    cBytes As Byte
    fFixedDisk As Byte
    nErrCode As Integer
    Reserved1 As Integer
    Reserved2 As Integer
    szPathName(OFS_MAXPATHNAME) As Byte
End Type


Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function ScreenToClient Lib "user32" (ByVal hWnd As Long, lpPoint As POINTAPI) As Long

'-> API pour affichage d'une page de propri�t�s
Private Type SHELLEXECUTEINFO
             cbSize As Long
             fMask As Long
             hWnd As Long
             lpVerb As String
             lpFile As String
             lpParameters As String
             lpDirectory As String
             nShow As Long
             hInstApp As Long
             ' Optional fields
             lpIDList As Long
             lpClass As String
             hkeyClass As Long
             dwHotKey As Long
             hIcon As Long
             hProcess As Long
   End Type
   
'---> Gestion de l'envoie de mail
Private Declare Function ShellExecuteEx Lib "shell32" (lpSEI As SHELLEXECUTEINFO) As Long
Private Const SEE_MASK_INVOKEIDLIST = &HC
Private Const SW_SHOW = 5

'-> API Pour gestion du temps
Public Declare Function GetTickCount& Lib "kernel32" ()

'---> Gestion du press papier
Public Const WM_CUT = &H300
Public Const WM_PASTE = &H302
Public Const WM_COPY = &H301

Public Declare Function OleTranslateColor Lib "oleaut32.dll" (ByVal _
    lOleColor As Long, ByVal lHPalette As Long, lColorRef As Long) As Long

Public Const LF_FACESIZE = 32
Public Const GWL_STYLE = (-16)
Public Const SCF_SELECTION = &H1&

' pour recuperer les messages et text avec richtextbox
Public Const EM_SETCHARFORMAT = (WM_USER + 68)
Public Const EM_SETBKGNDCOLOR = (WM_USER + 67)
Public Const EM_GETCHARFORMAT = (WM_USER + 58)

' pour Font et BackColor
Public Const CFM_BACKCOLOR = &H4000000
Public Const CFE_AUTOBACKCOLOR = CFM_BACKCOLOR

'-> Api pour redimensionner un picturebox
Public Const ScrCopy = &HCC0020

Public Declare Function StretchBlt Lib "gdi32" (ByVal hDC As Long, ByVal X As Long, ByVal y As Long, _
                                         ByVal nWidth As Long, ByVal nHeight As Long, _
                                         ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, _
                                         ByVal nSrcWidth As Long, ByVal nSrcHeight As Long, _
                                         ByVal dwRop As Long) As Long

'-> Api pour gerer la transparence
Public Declare Function SetBkMode Lib "gdi32" (ByVal hDC As Long, ByVal nBkMode As Long) As Long

'-> Api pour trouver le programme associ�
Public Declare Function FindExecutable Lib "shell32.dll" Alias "FindExecutableA" (ByVal lpFile As String, ByVal lpDirectory As String, ByVal lpResult As String) As Long

Public Sub SaveString(hKey As Long, strPath As String, strValue As String, strData As String)

Dim hwndKey As Long

'-> Ouvertyre dans la cl� sp�cifi�e
RegOpenKey hKey, strPath, hwndKey

'-> Setting de la valeur
RegSetValueEx hwndKey, strValue, 0, REG_SZ, ByVal strData, Len(strData)

'-> fermer la cl�
RegCloseKey hwndKey

End Sub


Public Function RegQueryStringValue(ByVal hKey As Long, ByVal strValueName As String) As String
    
'---> R�cup�ration d'une valeur d'une cl� de registre
    
Dim lResult As Long
Dim lValueType As Long
Dim strBuf As String
Dim lDataBufSize As Long
        
lResult = RegQueryValueEx(hKey, strValueName, 0, lValueType, ByVal 0, lDataBufSize)
If lResult = 0 Then
    If lValueType = REG_SZ Then
        'Create a buffer
        strBuf = String(lDataBufSize, Chr$(0))
        'retrieve the key's content
        lResult = RegQueryValueEx(hKey, strValueName, 0, 0, ByVal strBuf, lDataBufSize)
        If lResult = 0 Then
            'Remove the unnecessary chr$(0)'s
            RegQueryStringValue = Left$(strBuf, InStr(1, strBuf, Chr$(0)) - 1)
        End If
    ElseIf lValueType = REG_BINARY Then
        Dim strData As Integer
        'retrieve the key's value
        lResult = RegQueryValueEx(hKey, strValueName, 0, 0, strData, lDataBufSize)
        If lResult = 0 Then
            RegQueryStringValue = strData
        End If
    End If
End If

End Function


Public Function GetRegKeyValue(ByVal root As Long, ByVal key_name As String, ByVal subkey_name As String) As String
Dim hKey As Long
Dim value As String
Dim length As Long
Dim value_type As Long
' Retourne une cle du registre.
    ' ouvriri la cl�.
    If RegOpenKeyEx(root, key_name, _
        0&, &H1, hKey) <> ERROR_SUCCESS _
    Then
        Exit Function
    End If

    ' taille de la cl�.
    If RegQueryValueEx(hKey, subkey_name, _
        0&, value_type, ByVal 0&, length) _
            <> ERROR_SUCCESS _
    Then
    End If

    ' valeur de la cl�.
    value = Space$(length)
    If RegQueryValueEx(hKey, subkey_name, _
        0&, value_type, ByVal value, length) _
            <> ERROR_SUCCESS _
    Then
    Else
        ' nettoyer
        GetRegKeyValue = Left$(value, length - 1)
    End If

    ' fermer.
    If RegCloseKey(hKey) <> ERROR_SUCCESS Then
    End If
End Function

Public Sub ShowFileProperties(ByVal ps_FileName As String)
   
         Dim lu_ShellExUDT As SHELLEXECUTEINFO
         
         With lu_ShellExUDT
             .hWnd = hWnd
             .lpVerb = "properties"
             .lpFile = ps_FileName
             .fMask = SEE_MASK_INVOKEIDLIST
             .cbSize = Len(lu_ShellExUDT)
         End With
         
         Call ShellExecuteEx(lu_ShellExUDT)
   
   End Sub
