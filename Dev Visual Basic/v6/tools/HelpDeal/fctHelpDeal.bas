Attribute VB_Name = "fctHelpDeal"
'-> Dimensions des bordures internes d'une fen�tre
Public HauteurBordure As Integer
Public LargeurBordure As Integer

'-> Variable de r�cup�ration du code langue
Public CurLangue As String

'-> Indique le code langue en cours d'utilisation
Public CurCodeLangue As Integer

'-> Indique la cl� du code progiciel � afficher
Public CurApp As String

'-> Indique le code du client en cours
Public CurClient As String

'-> Indique le mot cl� pass� en argument
Public CurKeyWord As String

'-> Indique le contexte pass� en argument
Public CurCt As String

'-> Propath du fichier TurboMaq.ini
Public IniPath As String

'-> Path de la Doc de DEAL
Public PathDocDeal As String
Public OkDocDeal As Boolean 'Indique si on a acc�s � la doc du client

'-> Path de la doc du client
Public PathDocClient As String
Public OkDocClient As Boolean 'Indique si on a acc�s � la doc du client
Public DescriptifClient As String 'Libell� associ� au client

'-> Pour affichage des docs
Public DisplayDocDeal As Boolean
Public DisplayDocClient As Boolean

'-> Pour s�lection d'un contexte unique
Public OneCtGo As Node

'-> Collection des applicatifs
Public Applicatifs As Collection
Public CliApplicatifs As Collection
Public KeyWords As Collection
Public CliKeyWords As Collection

'-> Indique que la feuille est en cours de chargement : ne pas faire de mise � jour
Public IsLoading As Boolean

'-> Pour cr�ation des nodes
Public IndexNodeCreate As Long

'-> Variable d'enregistrement d'un menu
Public Type SaveMenu
    Libelle1 As String * 70 'Libell�
    Libelle2 As String * 70 'Libell�
    Libelle3 As String * 70 'Libell�
    Libelle4 As String * 70 'Libell�
    Libelle5 As String * 70 'Libell�
    Libelle6 As String * 70 'Libell�
    Libelle7 As String * 70 'Libell�
    Libelle8 As String * 70 'Libell�
    Libelle9 As String * 70 'Libell�
    Libelle10 As String * 70 'Libell�
    FichierHTML As String * 50 'Nom du fichier HTML
    Key As String * 15 'Cl� d'acc�s au menu
    KeyParent As String * 15 'Cl� d'acc�s du niveau sup�rieur
    IsUrl As Boolean 'Indique qu'un menu est un fichier
    IsLien As Boolean 'Indique s'il y a un lien ou non
End Type

'-> Variable d'enregistrement des constantes de traduction
Public Type SaveTraduct
    idTraduction As Integer
    strTraduction As String * 50
    lngTraduction As Long
End Type

'-> Variable d'enregistrement des mots cl�s
Public Type SaveKeyWord
    KeyName As String * 50 'Nom du mot cl�
End Type

'-> Variable d'enregistrement des contextes
Public Type SaveContexte
    Libelle1 As String * 70 'Libell�
    Libelle2 As String * 70 'Libell�
    Libelle3 As String * 70 'Libell�
    Libelle4 As String * 70 'Libell�
    Libelle5 As String * 70 'Libell�
    Libelle6 As String * 70 'Libell�
    Libelle7 As String * 70 'Libell�
    Libelle8 As String * 70 'Libell�
    Libelle9 As String * 70 'Libell�
    Libelle10 As String * 70 'Libell�
    Keyword As String * 50 'Mot cl� associ�
    Index As Integer
End Type

'-> Variable d'enregistrement association Lien Fichiuer HTML
Public Type SaveLien
    KeyMenu As String * 15 'Cl� du menu associ�
    Keyword As String * 50 'Mot cl� associ�
    KeyCt As String * 10 'Cl� du contexte associ�
End Type

'-> Chaine � afficher lors de la cr�ation d'un node non traduit
Public strTraduction(1 To 6) As String
Public idTraduction(1 To 6) As Integer
Public lngTraduction(1 To 6) As Long

Public IsMouchard As Boolean
Public Mouchard As String
Public hdlMouchard As Integer

Public Sub LoadListeClient()

'-> Pour lecture du fichier TruboMaq.ini
Dim TempFileName As String
Dim Res As Long
Dim lpKey As String
Dim aApp As Applicatif
Dim CliApp As Applicatif

'-> Pour compteur
Dim i As Integer, nEntries As Integer

'-> Effectuer une copie du fichier ini dans le r�pertoire tempo
TempFileName = GetTempFileNameVB("INI")
Res = CopyFile(IniPath, TempFileName, 0)
    
'-> R�cup�ration de la liste des logiciels
lpKey = Space(500)
Res = GetPrivateProfileString("Progiciels", "App", "", lpKey, Len(lpKey), TempFileName)
If lpKey = "" Then
    '-> Impossible de trouver la cl� ou la section : Quitter
    MsgBox "Impossible de trouver la liste des progiciels DEAL INFORMATIQUE", vbCritical + vbOKOnly, "Erreur fatale"
    Unload HelpDeal
End If

Dim ToAdd As Boolean

'-> Cr�ation de la liste des progiciels
lpKey = Mid$(lpKey, 1, Res)
For i = 1 To NumEntries(lpKey, "|")
    ToAdd = True
    Set aApp = New Applicatif
    aApp.Name = Entry(i, lpKey, "|")
    '-> Indiquer que le progiciel n'est pas charg�
    aApp.IsLoaded = False
    '-> Cr�ation des identifiant par lettre
    Select Case UCase$(aApp.Name)
        Case "DEAL"
            aApp.Identifiant = "dd"
        Case "ELODIE"
            aApp.Identifiant = "ed"
        Case "SOPHIE"
            aApp.Identifiant = "sd"
        Case "LUCIE"
            aApp.Identifiant = "gd"
            ToAdd = False
        Case "NATHALIE"
            aApp.Identifiant = "gd"
            ToAdd = False
        Case "BACCHUS"
            aApp.Identifiant = "bd"
        Case "PLANNING"
            aApp.Identifiant = "pd"
        Case "KS"
            aApp.Identifiant = "kd"
        Case "JULIE"
            aApp.Identifiant = "gd"
            ToAdd = False
        Case "GESCOM"
            aApp.Identifiant = "gd"
        Case "IMMOBILISATIONS"
            aApp.Identifiant = "md"
        
    End Select
    If ToAdd Then
    
        '-> Ajouter dans la collection
        Applicatifs.Add aApp, UCase$(aApp.Name)
        aApp.IndexMenu = Applicatifs.Count
        '-> Dupliquer l'applicatif pour la gestion du client
        Set CliApp = New Applicatif
        CliApp.Identifiant = aApp.Identifiant
        CliApp.Name = aApp.Name
        CliApp.IsLoaded = False
        CliApp.Descriptif = aApp.Descriptif
        CliApplicatifs.Add CliApp, UCase$(aApp.Name)
    End If
Next

'-> R�cup�ration des propri�t�s des divers applicatifs
For i = 1 To Applicatifs.Count
    '-> R�cup�ration de la cl� Libell�
    lpKey = Space(500)
    Res = GetPrivateProfileString(Applicatifs(i).Name, "Libel", "", lpKey, Len(lpKey), TempFileName)
    If lpKey <> "" Then
        Applicatifs(i).Descriptif = Mid$(lpKey, 1, Res)
        CliApplicatifs(i).Descriptif = Mid$(lpKey, 1, Res)
    End If
    '-> Ajout dans la liste des progiciels en cours
    frmApp.ImageCombo1.ComboItems.Add , Applicatifs(i).Name, Applicatifs(i).Name & " - " & Applicatifs(i).Descriptif, "LogoDeal"
Next

End Sub


Public Function IsApp(AppName As String) As Boolean

'-> Cette fonction v�rifie si le nom pass� en argument correspond � un applicatif valide

Dim aApp As Applicatif

On Error GoTo GestError

Set aApp = Applicatifs(UCase$(Trim(AppName)))
IsApp = True

Exit Function

GestError:

    IsApp = False
    
End Function

Public Function IsCodeLangue(ByVal Code As String) As Boolean

On Error GoTo GestError

If Not IsNumeric(Code) Then GoTo GestError

If CInt(Trim(Code)) > 6 Then GoTo GestError
    
If CInt(Trim(Code)) < 1 Then GoTo GestError

IsCodeLangue = True

Exit Function

GestError:

    IsCodeLangue = False

End Function

Public Function IsClient(CurClient As String) As Boolean

'---> Cette fonction v�rifie si dans le TurboMaq.ini on trouve la section relative au client

Dim TempClient As String

'-> R�cup�ration du path de l'aide en ligne du client
TempClient = GetIniString(CurClient, "HELP", IniPath, False)
DescriptifClient = GetIniString(CurClient, "LIBEL", IniPath, False)

'-> Test du retour
If TempClient = "" Or TempClient = "NULL" Then GoTo GestError

'-> Remplacer les valeurs logiques
ReplacePathMotCles TempClient

TempClient = Replace(UCase$(TempClient), "$IDENT$", CurClient)

'-> Tester l'existence du r�pertoire
If Dir$(TempClient, vbDirectory) = "" Then GoTo GestError

'-> Mettre dans la variable du path client le repertoire
PathDocClient = TempClient
    
'-> Renvoyer une valeur de succ�s
IsClient = True

Exit Function

GestError:
    IsClient = False

End Function
Public Sub DisplayApp(NomApp As String)

Dim aApp As Applicatif
Dim aNode As Node

'---> Cette fonction charge dans le treeview de helpdeal le menu applicatif de DEAL _
s'il est disponible et celui du client


'-> Nettoyer dans un premier temps le treeview
HelpDeal.TreeView1.Nodes.Clear

'-> Afficher les pages blanches
DisplayBlanckFile HelpDeal.BrowserClient
DisplayBlanckFile HelpDeal.BrowserDeal

'-> V�rifier si on doit charger la documentation dEAL  informatique
If OkDocDeal Then
    '-> Pointer sur l'applicatif
    Set aApp = Applicatifs(NomApp)
    '-> Charger le menu en m�moire si ce n'est pas d�ja fait
    If Not aApp.IsLoaded Then LoadMenuApp NomApp, "DEAL"
    '-> Cr�er le node de la documentation DEAL
    Set aNode = HelpDeal.TreeView1.Nodes.Add(, , "DEALMENU", "Documentation Deal Informatique   " & aApp.Name & " - " & aApp.Descriptif, "CloseDoc")
    aNode.ExpandedImage = "OpenDoc"
    '-> Charger le menu de DEAL
    CreateDisplyApp aApp, "DEALMENU", ""
End If

'-> V�rifier si on doit charger la documenation client
If OkDocClient Then
    '-> Pointer sur l'applicatif
    Set aApp = CliApplicatifs(NomApp)
    '-> Charger le menu en m�moire si ce n'est pas d�ja fait
    If Not aApp.IsLoaded Then LoadMenuApp NomApp, CurClient
    '-> Cr�er le node de la documentation DEAL
    Set aNode = HelpDeal.TreeView1.Nodes.Add(, , "CLIENTMENU", "Documentation : " & CurClient & " - " & DescriptifClient & "  " & aApp.Name & " - " & aApp.Descriptif, "CloseDoc")
    aNode.ExpandedImage = "OpenDoc"
    '-> Charger le menu de DEAL
    CreateDisplyApp aApp, "CLIENTMENU", "CLI"
End If
  

End Sub
Private Sub CreateDisplyApp(aApp As Applicatif, RootNode As String, AddValue As String)

Dim i As Integer
Dim aMenu As MenuDeal
Dim aNode As Node

For Each aMenu In aApp.Menus
    '-> Cr�ation de la repr�sentation physique
    If Trim(aMenu.KeyParent) = "" Then
        Set aNode = HelpDeal.TreeView1.Nodes.Add(RootNode, 4, AddValue & Trim(aMenu.Key), aMenu.Libelle)
        aNode.Parent.Expanded = True
    Else
        Set aNode = HelpDeal.TreeView1.Nodes.Add(AddValue & Trim(aMenu.KeyParent), 4, AddValue & Trim(aMenu.Key), aMenu.Libelle)
    End If
    '-> Setting de l'image
    If aMenu.IsUrl Then
        aNode.Image = "Document"
        aNode.Tag = "DOCUMENT"
    Else
        aNode.Image = "ChapitreFerme"
        aNode.ExpandedImage = "ChapitreOuvert"
        aNode.Tag = "CHAPITRE"
    End If
    
Next 'Pour tous les menus de l'applicatif

End Sub


Public Sub LoadMenuApp(NomApp As String, Client As String)

'---> Proc�dure qui charge un applicatif
Dim NumFic As Integer
Dim i As Long
Dim NomFichier As String
Dim aSaveMenu As SaveMenu
Dim aMenu As MenuDeal
Dim nbEnreg As Long
Dim aNode As Node
Dim aNode2 As Node
Dim aApp As Applicatif
Dim lbl As String
Dim SaveKey As String

'-> Construire le nom du fichier � ouvrir
If Client = "DEAL" Then
    Set aApp = Applicatifs(NomApp)
    NomFichier = PathDocDeal & aApp.Identifiant & "-Menu.dat"
    SaveKey = ""
Else
    Set aApp = CliApplicatifs(NomApp)
    NomFichier = PathDocClient & aApp.Identifiant & "-Menu.dat"
    SaveKey = "CLI"
End If

If Dir$(NomFichier, vbNormal) <> "" Then
    '-> Ouvrir le fichier
    NumFic = FreeFile
    Open NomFichier For Random As #NumFic Len = Len(aSaveMenu)
    nbEnreg = LOF(NumFic) / Len(aSaveMenu)
    '-> Boucle d'analyse
    For i = 1 To nbEnreg
        '-> Lecture de l'enregistrement
        Get #NumFic, i, aSaveMenu
        '-> Cr�ation de l'enregistrement menu
        Set aMenu = New MenuDeal
        '-> Charger les libelles en les remplaceant par les chaine de substitution si necessaire
        '-> Fran�ais
        If Trim(aSaveMenu.Libelle1) = "" Then
            GetStrTraduct lbl, 1, aSaveMenu.Libelle1
        Else
            lbl = Trim(aSaveMenu.Libelle1)
        End If
        aMenu.SetLbByLangue 1, lbl
        '-> Anglais
        If Trim(aSaveMenu.Libelle2) = "" Then
            GetStrTraduct lbl, 2, aSaveMenu.Libelle1
        Else
            lbl = Trim(aSaveMenu.Libelle2)
        End If
        aMenu.SetLbByLangue 2, lbl
        '-> Allemand
        If Trim(aSaveMenu.Libelle3) = "" Then
            GetStrTraduct lbl, 3, aSaveMenu.Libelle1
        Else
            lbl = aSaveMenu.Libelle3
        End If
        aMenu.SetLbByLangue 3, lbl
        '-> Portugais
        If Trim(aSaveMenu.Libelle4) = "" Then
            GetStrTraduct lbl, 4, aSaveMenu.Libelle1
        Else
            lbl = aSaveMenu.Libelle4
        End If
        aMenu.SetLbByLangue 4, lbl
        '-> Espagnol
        If Trim(aSaveMenu.Libelle5) = "" Then
            GetStrTraduct lbl, 5, aSaveMenu.Libelle1
        Else
            lbl = aSaveMenu.Libelle5
        End If
        aMenu.SetLbByLangue 5, lbl
        '-> Italien
        If Trim(aSaveMenu.Libelle6) = "" Then
            GetStrTraduct lbl, 6, aSaveMenu.Libelle1
        Else
            lbl = aSaveMenu.Libelle6
        End If
        aMenu.SetLbByLangue 6, lbl
        '-> Indiquer si le menu est un fichier URL
        aMenu.IsUrl = aSaveMenu.IsUrl
        aMenu.FichierHTML = Trim(aSaveMenu.FichierHTML)
        '-> R�cup�rer les notions de cl�s
        aMenu.Key = aSaveMenu.Key
        aMenu.KeyParent = aSaveMenu.KeyParent
        '-> Ajout dans la collection
        aApp.Menus.Add aMenu, SaveKey & Trim(aSaveMenu.Key)
        '-> Indiquer que le menu de cet applicatif est charg�
        aApp.IsLoaded = True
    Next 'pour tous les enregistrements
End If

'-> Fermer le fichier
Reset

End Sub
Private Sub GetStrTraduct(lbl As String, CodeLangue As Integer, OriginalLibel As String)

Select Case idTraduction(CodeLangue)
    Case 1
        lbl = ""
    Case 2
        lbl = "<Pas de Traduction>"
    Case 3
        lbl = strTraduction(CodeLangue)
    Case 4
        lbl = OriginalLibel
End Select



End Sub

Public Sub LoadTraductFile(Path As String)

'---> Charge le fichier des constantes de traduction des menus
Dim NumFic As Integer
Dim aSaveTraduct As SaveTraduct
Dim nbEnreg As Integer, i As Integer

'-> Si Doc Client OK alors
If Dir$(Path & "Traduct.dat", vbNormal) <> "" Then
    '-> Ouvrir le fichier
    NumFic = FreeFile
    Open Path & "Traduct.dat" For Random As #NumFic Len = Len(aSaveTraduct)
    nbEnreg = LOF(NumFic) / Len(aSaveTraduct)
    For i = 1 To nbEnreg
        Get #NumFic, i, aSaveTraduct
        idTraduction(i) = aSaveTraduct.idTraduction
        strTraduction(i) = aSaveTraduct.strTraduction
        lngTraduction(i) = aSaveTraduct.lngTraduction
    Next
    '-> Fermer le fichier
    Close #NumFic
Else
    '-> Lancer l'initialisation par defaut des valeurs
    InitDefautTraduction
End If

End Sub

Public Sub InitDefautTraduction()

'-> Propri�t�s de traduction
For i = 1 To 6
    Select Case i
        Case 1
            idTraduction(i) = 2
            strTraduction(i) = "<Pas de traduction>"
        Case 2
            idTraduction(i) = 3
            strTraduction(i) = "<No translation>"
        Case 3
            idTraduction(i) = 3
            strTraduction(i) = "<Keine �bersetzung>"
        Case 4
            idTraduction(i) = 3
            strTraduction(i) = "<No traduc�o>"
        Case 5
            idTraduction(i) = 3
            strTraduction(i) = "<No traducion>"
        Case 6
            idTraduction(i) = 3
            strTraduction(i) = "<No traduzione esiste>"
    End Select
    lngTraduction(i) = &HC00000    'bleu
Next


End Sub

Public Sub DisplayAppByLangue()

'---> Affiche dans le bon codelangue les nodes du treeview1

Dim aNode As Node
Dim aMenu As Menu
Dim DealApp As Applicatif
Dim CliApp As Applicatif

'-> Ne rien faire si pas d'applicatif
If CurApp = "" Then Exit Sub

'-> Pointer sur les applicatifs respectifs
Set DealApp = Applicatifs(CurApp)
Set CliApp = CliApplicatifs(CurApp)

For Each aNode In HelpDeal.TreeView1.Nodes
    '-> Selon la nature du node premier
    If aNode.Key = "DEALMENU" Or aNode.Key = "CLIENTMENU" Then
    Else
        If Left(aNode.Key, 3) <> "CLI" Then
            aNode.Text = DealApp.Menus(aNode.Key).Libelle
        Else
            aNode.Text = CliApp.Menus(aNode.Key).Libelle
        End If
    End If
Next 'Pour tous les nodes du treeview

End Sub

Public Sub DisplayUnBlanckFile(Browser As WebBrowser)

'-> Afficher la page de recherche du client
If Dir$(PathDocClient & Format(CurCodeLangue, "00") & "\UnBlanck.htm", vbNormal) <> "" Then
    Browser.Navigate PathDocClient & Format(CurCodeLangue, "00") & "\UnBlanck.htm"
Else
    '-> Sinon celle de DEAL
    If Dir$(PathDocDeal & Format(CurCodeLangue, "00") & "\UnBlanck.htm", vbNormal) <> "" Then
        Browser.Navigate PathDocDeal & Format(CurCodeLangue, "00") & "\UnBlanck.htm"
    End If
End If

End Sub

Public Sub DisplayBlanckFile(Browser As WebBrowser)

On Error Resume Next

'-> Afficher la page de recherche du client
Browser.QueryStatusWB OLECMDID_CLOSE
If Dir$(PathDocClient & "Blanck.htm", vbNormal) <> "" Then
    Browser.Navigate2 PathDocClient & "Blanck.htm"
Else
    '-> Sinon celle de DEAL
    If Dir$(PathDocDeal & "Blanck.htm", vbNormal) <> "" Then
        Browser.Navigate2 PathDocDeal & "Blanck.htm"
    End If
End If

End Sub

Public Sub LoadKeyWordFile(IsClient As Boolean)

'---> Charger la liste des mots cl�s
Dim NumFic As Integer
Dim aSaveKey As SaveKeyWord
Dim nbEnreg As Long, i As Long
Dim aKeyWord As Keyword
Dim CollectionToAdd As Collection
Dim PathKey As String

If IsClient Then
    PathKey = PathDocClient & "Key.dat"
    Set CollectionToAdd = CliKeyWords
Else
    PathKey = PathDocDeal & "Key.dat"
    Set CollectionToAdd = KeyWords
End If

If Dir$(PathKey) <> "" Then
    '-> Ouvrir le fichier
    NumFic = FreeFile
    Open PathKey For Random As #NumFic Len = Len(aSaveKey)
    nbEnreg = LOF(NumFic) / Len(aSaveKey)
    For i = 1 To nbEnreg
        '-> Lecture de l'enregistrement
        Get #NumFic, i, aSaveKey
        '-> Cr�ation du menu
        Set aKeyWord = New Keyword
        aKeyWord.Name = Trim(aSaveKey.KeyName)
        '-> Ajout du menu dans la collection
        CollectionToAdd.Add aKeyWord, UCase$(aKeyWord.Name)
    Next 'Pour tous les mots cl�s
    '-> Fermer le fichier
    Close #NumFic
End If

End Sub


Public Sub LoadCtFile(IsClient As Boolean)

'---> Charge le fichier des contextes associ�s au mots cl�s Ct.dat

Dim NumFic As Integer
Dim aSaveCt As SaveContexte
Dim nbEnreg As Long
Dim i As Long
Dim aCt As Contexte
Dim aKeyWord As Keyword
Dim AddCt As Boolean
Dim CollectionToAdd As Collection
Dim PathKey As String
Dim lbl As String

If IsClient Then
    PathKey = PathDocClient & "Ct.dat"
    Set CollectionToAdd = CliKeyWords
Else
    PathKey = PathDocDeal & "Ct.dat"
    Set CollectionToAdd = KeyWords
End If

If Dir$(PathKey) <> "" Then
    '-> Ouverture du fichier
    NumFic = FreeFile
    Open PathKey For Random As #NumFic Len = Len(aSaveCt)
    nbEnreg = LOF(NumFic) / Len(aSaveCt)
    For i = 1 To nbEnreg
        '-> Charger le contexte
        Get #NumFic, i, aSaveCt
        '-> Il faut pointer sur le mot cl�
        Set aKeyWord = CollectionToAdd(UCase$(Trim(aSaveCt.Keyword)))
        '-> Cr�er le contexte
        Set aCt = New Contexte
        aCt.Keyword = aKeyWord.Name
        aCt.Index = aSaveCt.Index
        '-> Gestion des contextes
        If Trim(aSaveCt.Libelle1) = "" Then
            GetStrTraduct lbl, 1, aSaveCt.Libelle1
        Else
            lbl = Trim(aSaveCt.Libelle1)
        End If
        aCt.SetLbByLangue 1, lbl
        '-> Anglais
        If Trim(aSaveCt.Libelle2) = "" Then
            GetStrTraduct lbl, 2, aSaveCt.Libelle1
        Else
            lbl = Trim(aSaveCt.Libelle2)
        End If
        aCt.SetLbByLangue 2, lbl
        '-> Allemand
        If Trim(aSaveCt.Libelle3) = "" Then
            GetStrTraduct lbl, 3, aSaveCt.Libelle1
        Else
            lbl = aSaveCt.Libelle3
        End If
        aCt.SetLbByLangue 3, lbl
        '-> Portugais
        If Trim(aSaveCt.Libelle4) = "" Then
            GetStrTraduct lbl, 4, aSaveCt.Libelle1
        Else
            lbl = aSaveCt.Libelle4
        End If
        aCt.SetLbByLangue 4, lbl
        '-> Espagnol
        If Trim(aSaveCt.Libelle5) = "" Then
            GetStrTraduct lbl, 5, aSaveCt.Libelle1
        Else
            lbl = aSaveCt.Libelle5
        End If
        aCt.SetLbByLangue 5, lbl
        '-> Italien
        If Trim(aSaveCt.Libelle6) = "" Then
            GetStrTraduct lbl, 6, aSaveCt.Libelle1
        Else
            lbl = aSaveCt.Libelle6
        End If
        aCt.SetLbByLangue 6, lbl
        '-> Ajouter le contexte
        aKeyWord.Contextes.Add aCt, "CT" & aCt.Index
    Next 'Pour tous les enregistrements
End If 'Si le fichier existe

End Sub

Public Sub LoadLienApp(IsClient As Boolean)

'---> Proc�dure qui cr�er les liens propres � un applicatifs
Dim NumFic As Integer
Dim i As Long
Dim NomFichier As String
Dim aSaveLien As SaveLien
Dim aMenu As Menu
Dim nbEnreg As Long
Dim aNode As Node
Dim aCt As Contexte
Dim aKeyWord As Keyword
Dim aLien As Lien
Dim CollectionToAdd As Collection
Dim PathKey As String
Dim aApp As Applicatif

'-> Quitter si pas d'applicatif
If CurApp = "" Then Exit Sub

'-> Construire le chemin du fichier
If IsClient Then
    Set aApp = CliApplicatifs(CurApp)
    PathKey = PathDocClient & aApp.Identifiant & "-Lien.dat"
    Set CollectionToAdd = aApp.Liens
Else
    Set aApp = Applicatifs(CurApp)
    PathKey = PathDocDeal & aApp.Identifiant & "-Lien.dat"
    Set CollectionToAdd = aApp.Liens
End If

If Dir$(PathKey, vbNormal) <> "" Then
    '-> Ouvrir le fichier
    NumFic = FreeFile
    Open PathKey For Random As #NumFic Len = Len(aSaveLien)
    nbEnreg = LOF(NumFic) / Len(aSaveLien)
    '-> Boucle d'analyse
    For i = 1 To nbEnreg
        '-> Lecture de l'enregistrement
        Get #NumFic, i, aSaveLien
        '-> Ajouter le lien dans l'applicatif
        Set aLien = New Lien
        aLien.ContexteKey = Trim(UCase$(aSaveLien.KeyCt))
        aLien.KeyWordKey = Trim(UCase$(aSaveLien.Keyword))
        If IsClient Then
            If Trim(UCase$(aSaveLien.KeyMenu)) <> "" Then
                aLien.MenuKey = "CLI" & Trim(UCase$(aSaveLien.KeyMenu))
            Else
                aLien.MenuKey = ""
            End If
        Else
            aLien.MenuKey = Trim(UCase$(aSaveLien.KeyMenu))
        End If
        aApp.Liens.Add aLien, Trim(UCase$(aSaveLien.KeyMenu)) & "|" & aLien.KeyWordKey & "|" & aLien.ContexteKey
    Next 'Por tous les liens
End If 'Si on trouve le fichier

'-> Fermer les fichiers
Close #NumFic

End Sub
Public Sub SearchContexte()

Dim NbContexte As Integer
Dim aNode As Node

'-> Afficher les contextes s'il y en a
If OkDocClient Then DisplayLienCtKey True, NbContexte
If OkDocDeal Then DisplayLienCtKey False, NbContexte

'-> Quitter si c'est pas le bon contexte
If NbContexte = -999 Then Exit Sub

'-> S�lectionner directement le bon fichier si necessaire
If NbContexte = 1 Then
    frmChxCt.TreeView1_NodeClick OneCtGo
Else
    frmChxCt.Caption = CurKeyWord
    '-> Ajouter le node pour quitter
    frmChxCt.TreeView1.Nodes.Add , , "QUIT", "Quitter l'aide", "Quit"
    frmChxCt.Show
End If

End Sub

Public Sub DisplayLienCtKey(IsClient As Boolean, NbContexte As Integer)

'---> Cette fonction est charg�e d'afficher la liste des contextes associ�s � un mot cl�

Dim ColKeyWord As Collection
Dim aApp As Applicatif
Dim aLien As Lien
Dim aKeyWord As Keyword
Dim aCt As Contexte
Dim RootNode As String
Dim aNode As Node

'-> Pointer sur les bonnes collections
If IsClient Then
    '-> Ajouter le node dans la feuille de choix
    Set aNode = frmChxCt.TreeView1.Nodes.Add(, , "DOCCLIENT", "Contextes li�s au client", "Bureau")
    aNode.Expanded = True
    RootNode = "DOCCLIENT"
    Set aApp = CliApplicatifs(CurApp)
    Set ColKeyWord = CliKeyWords
Else
    '-> Ajouter le node dans la feuille de choix
    Set aNode = frmChxCt.TreeView1.Nodes.Add(, , "DOCDEAL", "Contextes li�s � Deal Informatique", "Bureau")
    aNode.Expanded = True
    RootNode = "DOCDEAL"
    Set aApp = Applicatifs(CurApp)
    Set ColKeyWord = KeyWords
End If

'-> V�rifier que le mot cl� existe
If Not IsKeyWord(ColKeyWord) Then
    HelpDeal.Visible = True
    NbContexte = -999
    Exit Sub
End If

'-> Pointer sur le mot cl�
Set aKeyWord = ColKeyWord(CurKeyWord)

'-> Afficher les contextes associ�s � ce mot cl�
For Each aLien In aApp.Liens
    If aLien.KeyWordKey = CurKeyWord Then
        '-> Pointer sur le contexte associ�
        Set aCt = aKeyWord.Contextes(aLien.ContexteKey)
        '-> Ajouter dans la liste des contextes
        Set OneCtGo = frmChxCt.TreeView1.Nodes.Add(RootNode, 4, aLien.MenuKey, aCt.Libelle & " [" & aApp.Name & " : " & aApp.Descriptif & " ]", "Document")
        NbContexte = NbContexte + 1
        '-> Afficher la bonne feuille HTML si on trouve le bon contexte
        If aLien.ContexteKey = CurCt Then
            frmChxCt.TreeView1_NodeClick OneCtGo
            NbContexte = -999
            Exit Sub
        End If
    End If
Next 'Pour tous les liens sur ce mot cl�



End Sub


Private Function IsKeyWord(ColKeyWord As Collection) As Boolean

Dim aKey As Keyword

On Error GoTo GestError

Set aKey = ColKeyWord(CurKeyWord)

IsKeyWord = True

Exit Function
    
GestError:
    IsKeyWord = False

End Function
