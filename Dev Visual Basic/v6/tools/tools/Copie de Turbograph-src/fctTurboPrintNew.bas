Attribute VB_Name = "fctTurboPrintNew"
Option Explicit

'-> Pour passage en v6
Public V6TurboIniPath As String
Public V6MqtPath As String


'---> Indique si on est en version Entreprise (0) ou profressionelle (1)
Public VersionTurbo As Integer
Public TurboGraphIniFile As String
Public Tm_PictureIniFile As String
Public TurboGraphWebFile As String
Public TurboGraphWebPrinter As String
Public TurboGraphWebDirect As String
Public TurboMaqIniFile As String
Public FichierLangue As String
Public IndexLangue As Integer
Public AskForLangue As Boolean
Public TurbosavePath As String

'---> Pour gestion des Messporg : version Internet
Public IsAutoVersion As Boolean

'---> Pour gestion des p�riph�riques de sortie
Public Mode As Integer '-> Indique le mode de consultation envoy� sur la ligne de commande
Public Sortie As Object '-> Contient le p�riph�rique de sortie
Public MargeX As Integer '-> Marge interne d'un DC axe X pour impression papier
Public MargeY As Integer '-> Marge interne d'un DC axe Y pour impression papier
Public TempPrinter As Printer '-> Objet de type printer temporaire pour test de validit� imprimante
Public SpoolSize As Long '-> Indique le nombre de page par job d'impression
Public SpoolPrint As Long '-> Indique le nombre de pages imprim�es dans le spool en cours

'---> Pour ouverture des fichiers
Public OpenPath As String 'Indique le path d�faut
Public FirstOpen As Boolean 'N'en tenir compte que la premi�re fois
Public FilePath As String

'---> Pour impression de la page des s�lections
Public Const SelectRTFKey = "%%SELECTIONRTF%%@@THIERRY"

'---> Pour dessin de la temporisation
Public TailleLue As Long
Public TailleTotale As Long

'---> Pour export Excel
Public ExcelOk As Boolean

'---> Pour export OpenOffice
Public OpenOfficeOk As Boolean

'---> Pour envoie Internet
Public OutLookOk As Boolean
Public NetParam As String
Public IsCryptedFile As Boolean

'---> Pour envoie vers NOTEPAD ou WORDPAD
Public EditorPath As String

'---> Pour echange entre les feuilles
Public strRetour As String

'---> Pour cr�ation dynamique des controles
Public IdRTf As Integer '-> Contient l'index du prochain Controle RTF qui sera charg�
Public IdBmp As Integer '-> Contient l'index du prochain controle PictureBox qui sera cr��

'---> Structure de controle de dessin d'une cellule d'un objet tableau
Public Type FormatedCell
    Ok As Boolean
    nbDec As Integer
    strFormatted As String
    Value As Double
    idNegatif As Integer
End Type

'---> Liste des collections pour gestion du multi Spool
Public Fichiers As Collection

'---> Gestion des paths
Public TempDirectory As String

'---> Pour gestion de la cr�ation des blocks de tableau
Dim sCol() As String
Dim sLigne() As String
Dim LigLue As Integer

'---> Pour gestion des envoies par messagerie
Public OrigineMail As Integer '-> Indique la provenance de l'envoie
 '-> Valeurs : 0 -> OutLook depuis Visu _
               1 -> Internet depuis visu _
               2 -> OutLook depuis commande _
               3 -> Internet depuis commande
Public ExportMail As Integer '-> Indique la nature de l'export
'-> Valeurs : 0 -> Page en Cours, 1 -> Spool en cours , Fichier en cours
Public SpoolKeyMail As String '-> Cl� du spool en cours depuis visu
Public FileKeyMail As String '-> Cl� du fihcier en cours de visu
Public PageNumMail As Integer '-> Num�ro de la page que l'on d�sire envoyer
Public SendOutLook As Boolean '-> Indique si on doit afficher ou envoyer le mail vers OutLook
Public SendLotus As Boolean '-> Indique si on doit afficher ou envoyer le mail vers Lotus
Public copyAssemb As Boolean
Public noGarde As Boolean

Public IsMouchard As Boolean

'-> Pour gestion des
Public KillSpool As Boolean
Public IsPDF As Boolean

Public PdfDestinationFile As String

'-> pour la recherche de texte
Public FindTop As Boolean
Public FindPage As Integer
Public FindPos As Integer
Public FindPos2 As Integer
Public FindText As String
Public FindScrollH As Integer
Public FindScrollV As Integer

'-> pour la gestion du debordement
Public DebordementTop As Boolean
Public AllowDebordement As Boolean

'-> pour la gestion du zoom
Public Zoom As Double

'-> pour la gestion de la navigation
Public IsNaviga As Boolean

' Format structure, pass� avec SendMessage au contr�le
Private Type FORMAT
    cbSize As Integer
    wPad1 As Integer
    dwMask As Long
    dwEffects As Long
    yHeight As Long
    yOffset As Long
    crTextColor As Long
    bCharSet As Byte
    bPitchAndFamily As Byte
    szFaceName(0 To LF_FACESIZE - 1) As Byte
    wPad2 As Integer
    wWeight As Integer
    sSpacing As Integer
    crBackColor As Long
    lLCID As Long
    dwReserved As Long
    sStyle As Integer
    wKerning As Integer
    bUnderlineType As Byte
    bAnimation As Byte
    bRevAuthor As Byte
    bReserved1 As Byte
End Type

Public Mouchard As String
Public hdlMouchard As Integer

Public Sub GetPrinterList(PrintFile As String)

'---> Cette proc�dure affiche la liste des imprimante pour la s�lection

Dim NomImp As String
Dim NbCopies As Integer
Dim LigneCommande As String


'-> Vider la variable d'�change
strRetour = ""

'-> Charger avec la liste des imprimantes
Load frmPrint

'-> Initialiser avec la fonction qui va bien
frmPrint.InitialisationGetPrint

'-> Afficher la feuille
frmPrint.Show vbModal
If strRetour = "" Then End

'-> Get du nom de l'imprimante
NomImp = Entry(1, strRetour, "|")
NbCopies = CInt(Entry(3, strRetour, "|"))

If UCase$(NomImp) = "DEALVIEW" Then
    LigneCommande = "ECRAN$" & IndexLangue & "|" & PrintFile
Else
    LigneCommande = NomImp & "~DIRECT~" & NbCopies & "|" & PrintFile
End If

'-> Lancer le turbo avec la nouvelle
Shell App.Path & "\TurboGraph.exe " & LigneCommande
End

End Sub

Private Function InitTurboVersion()

Dim strPath As String
Dim i As Integer
Dim Res As Long
Dim lpBuffer As String
Dim TempFileName As String

'->  D�tection de la version Professionelle: Pr�sence du fichier Turbo.ver et Path

On Error Resume Next

'-> Recherche du fichier Version :
If Dir$(App.Path & "\Turbo.ver") = "" Then
    '-> On ne trouve pas le fichier Ver donc on est en version Internet.
    VersionTurbo = 1 'Version internet
Else
    VersionTurbo = 0 'Version Path v6
End If

'-> Par d�faut on est en fran�ais
IndexLangue = 1

'-> On est en version Entreprise : recherche du path des fichiers Ini et des fichiers Ressources
If VersionTurbo = 1 Then 'Version internet
    TurboGraphIniFile = "" '-> Pas d'acc�s au fichier TurboGrpah.ini
    TurboGraphIniFile = App.Path & "/" & "Turbograph.ini" 'MESSPROG
    '-> Setting du fichier TM_Picture.ini
    Tm_PictureIniFile = App.Path & "\Tm_Picture.ini"
    If Dir$(Tm_PictureIniFile, vbNormal) = "" Then Tm_PictureIniFile = ""
    'TelechargeFile(TurboGraphWebFile & "/deallogo/Tm_Picture.ini" , App.Path & "\Tm_Picture.ini")
    'Tm_PictureIniFile = "" '-> Pas d'acc�s au fichier Tm_picture.ini
    TurboMaqIniFile = "" '-> Pas d'acc�s au fichier TurboMaq.ini
    FichierLangue = "" '-> Pas de fichiers Langue - > Charger en fichier Ressource
    AskForLangue = True '-> Demander le fichier Langue
    '-> on reccupere ou telecharger les images
    lpBuffer = GetIniString("SITE", "HTTP", App.Path & "\Turbograph.ini", False)
    If lpBuffer <> "NULL" Then TurboGraphWebFile = Trim(lpBuffer)
    '-> on reccupere si on a une imprimante par defaut
    lpBuffer = GetIniString("PRINTER", "DEFAULT", App.Path & "\Turbograph.ini", False)
    If lpBuffer <> "NULL" Then TurboGraphWebPrinter = Trim(lpBuffer)
    '-> on reccupere si on doit envoyer directement l'impression
    lpBuffer = GetIniString("PRINTER", "DIRECT", App.Path & "\Turbograph.ini", False)
    If lpBuffer <> "NULL" Then TurboGraphWebDirect = Trim(lpBuffer)
    '-> on regarde si on est en mode trace
    lpBuffer = GetIniString("PARAM", "TRACE", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then IsMouchard = Trim(lpBuffer)
    '-> on reccupere ou telecharger les images
    lpBuffer = GetIniString("SITE", "HTTP", App.Path & "\Turbograph.ini", False)
    If lpBuffer <> "NULL" Then TurboGraphWebFile = Trim(lpBuffer)
    '-> on reccupere le repertoire de sauvegarde par defaut
    lpBuffer = GetIniString("PARAM", "SAVE", App.Path & "\Turbograph.ini", False)
    If lpBuffer <> "NULL" Then
        TurbosavePath = Trim(lpBuffer)
    End If
    If TurbosavePath = "" Then TurbosavePath = GetSpecialfolder(CSIDL_PERSONAL) & "\mes spools\"
    Exit Function
Else 'Verdion Entreprise
    '-> Le path du r�pertoire doit �tre renseign� MESSPROG
    If V6TurboIniPath = "" Then
        If Dir$(App.Path & "\Turbograph.ini") <> "" Then
                V6TurboIniPath = App.Path
        Else
            lpBuffer = ShowOpenFolder("Recherche du r�pertoire de param�trage ", 0)
            If lpBuffer = "" Then
                MsgBox "Impossible de continuer sans le r�pertoire de param�trage", vbCritical + vbOKOnly, "Erreur"
                End
            End If
            V6TurboIniPath = lpBuffer
        End If
    End If
    If Right$(V6TurboIniPath, 1) <> "\" Then V6TurboIniPath = V6TurboIniPath & "\"
    If (GetAttr(V6TurboIniPath) And vbDirectory) <> vbDirectory Then 'MESSPROG
        MsgBox "Impossible de trouver le r�pertoire des fichiers langue." & vbCrLf & V6TurboIniPath, vbCritical + vbOKOnly, "Erreur"
        End
    End If
    TurboGraphIniFile = V6TurboIniPath & "Turbograph.ini" 'MESSPROG
    If Dir$(TurboGraphIniFile) = "" Then
        MsgBox "Impossible de trouver le fichier de param�trage : " & vbCrLf & TurboGraphIniFile, vbCritical + vbOKOnly, "Erreur"
        End
    End If
    '-> Path des fichiers langue
    lpBuffer = App.Path
    i = InStrRev(lpBuffer, "\")
    lpBuffer = Mid$(lpBuffer, 1, i) & "Mqt\"
    If (GetAttr(lpBuffer) And vbDirectory) <> vbDirectory Then 'MESSPROG
        MsgBox "Impossible de trouver le r�pertoire des fichiers langue." & vbCrLf & lpBuffer, vbCritical + vbOKOnly, "Erreur"
        End
    End If
    V6MqtPath = lpBuffer
    FichierLangue = lpBuffer
    
    '-> Setting du fichier TM_Picture.ini
    Tm_PictureIniFile = V6TurboIniPath & "Tm_Picture.ini"
    If Dir$(Tm_PictureIniFile, vbNormal) = "" Then Tm_PictureIniFile = ""
    
    '-> Setting du fichier TurboMaq.ini
    TurboMaqIniFile = V6TurboIniPath & "TurboMaq.ini"
    If Dir$(TurboMaqIniFile, vbNormal) = "" Then TurboMaqIniFile = ""
    
End If

'-> on regarde si on est en mode trace
lpBuffer = GetIniString("PARAM", "TRACE", TurboGraphIniFile, False)
If lpBuffer <> "NULL" Then IsMouchard = Trim(lpBuffer)


'-> Initialisation de la version entreprise

'-> Recherche du fichier Langue
If TurboGraphIniFile <> "" Then
    lpBuffer = GetIniString("PARAM", "LANGUE", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then
        '-> Tester si la zone est num�rique
        If IsNumeric(lpBuffer) Then
            IndexLangue = CInt(lpBuffer)
            AskForLangue = False
        Else
            '-> Il faut demander la langue de l'utilisateur
            AskForLangue = True
            IndexLangue = 0
        End If
    Else
        '-> Demander le fichier langue � charger
        AskForLangue = True
    End If
    
    '-> Taille du spool
    lpBuffer = GetIniString("PARAM", "SPOOLSIZE", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then
        If IsNumeric(lpBuffer) Then SpoolSize = CLng(lpBuffer)
    End If
        
    '-> R�pertoire d'export HTML
    lpBuffer = GetIniString("PARAM", "EXPORTHTML", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then
        PathToExport = Trim(lpBuffer)
        '-> V�rifier si on ne demande pas de travailler dans le r�pertoire de travail de Progress
        If UCase$(Trim(Entry(1, lpBuffer, "$"))) = "ENV" Then
            '-> R�cup�rer la variable d'environnement
            lpBuffer = GetVariableEnv(Entry(2, lpBuffer, "$"))
            '-> V�rifier la valeur
            If Trim(lpBuffer) <> "" Then
                If Dir$(lpBuffer, vbDirectory) <> "" Then
                    PathToExport = lpBuffer
                Else
                    PathToExport = ""
                End If
            Else
                PathToExport = ""
            End If
        Else
            If Dir$(PathToExport, vbDirectory) = "" Then PathToExport = ""
        End If
    End If
End If 'Si on a acc�s au fichier Ini

End Function

Public Function GetVariableEnv(strVariable As String) As String

Dim Res As Long
Dim lpBuffer As String

lpBuffer = Space$(500)
Res = GetEnvironmentVariable(strVariable, lpBuffer, Len(lpBuffer))
If Res <> 0 Then
    '-> Faire un trim sur le buffer
    lpBuffer = Mid$(lpBuffer, 1, Res)
Else
    lpBuffer = ""
End If

'-> Retouner la valeur
GetVariableEnv = lpBuffer

End Function



Private Sub Initialisation()

'---> Cette proc�dure est charg�e d'initialiser les diff�rentes variables n�c�ssaires au moteur d'impression

Dim Res As Long
Dim lpBuffer As String

'-> Attention, il existe 2 versions du TURBO.

'-> Version Professionelle  -> Libre
'-> Version Entreprise - > Dans architecture

'-> Initialiser les index de cr�ation des prochains objets RichTextFormat et BITMAP
IdRTf = 1
IdBmp = 1

'-> Initialiser la collection des fichiers ouverts dans le TurboGraph
Set Fichiers = New Collection

'-> R�cup�ration des informations de formatage num�rique
lpBuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, lpBuffer, Len(lpBuffer))
SepDec = Mid$(lpBuffer, 1, Res - 1)
lpBuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, lpBuffer, Len(lpBuffer))
SepMil = Mid$(lpBuffer, 1, Res - 1)

'-> R�cup�ration du r�pertoire d'ouverture par d�faut
lpBuffer = Space$(255)
Res = GetEnvironmentVariable("DEALTEMPO", lpBuffer, Len(lpBuffer))
If Res <> 0 Then
    OpenPath = Mid$(lpBuffer, 1, Res)
    If Dir$(OpenPath, vbDirectory) = "" Then OpenPath = ""
End If

'-> R�cup�ration du r�pertoire du param�trage du Turbo dans le cas ou version = 0
lpBuffer = Space$(255)
Res = GetEnvironmentVariable("TURBOINI", lpBuffer, Len(lpBuffer))
If Res <> 0 Then V6TurboIniPath = Mid$(lpBuffer, 1, Res)

'-> R�cup�rer la path du r�pertoire temporaire de windows
TempDirectory = GetTempFileNameVB("", True)

'-> Lancer l'initialisation de la gestion des versions du Turbo
Call InitTurboVersion

End Sub


Public Sub Main()

'---> Point d'entr�e du nouveau module

Dim lpBuffer As String
Dim Res As Long
Dim LigneCommande As String
Dim Param1 As String
Dim RectoVerso As Boolean
Dim DeviceName As String
Dim ModeName As String
Dim aPrinter As Printer
Dim FindPrinter As Boolean
Dim Ligne As String
Dim i As Integer, j As Integer, k As Integer, l As Integer
Dim PageDebut As Integer
Dim aPath As String
Dim Tempo As String
Dim TempFileName As String
Dim NomFichier As String
Dim aFichier As Fichier
Dim aSpool As Spool
Dim CountPage As Integer

'-> Pour argument sur la ligne de command epour IE, OutLook , HTML
Dim pFile As String
Dim pTo As String
Dim pCopies As String
Dim pFrom As String
Dim pObject As String
Dim pBody As String
Dim pBodyFile As String
Dim pFormat As String
Dim pFormatBody As String
Dim pMode As String
Dim pSpoolName As String
Dim pDir As String
Dim KeyValue As String
Dim pValue As String
Dim pRun As String
Dim pCrypt As String
Dim pPrinter As String

Dim MyCodeLangue As String


' Descriptif de la ligne de commande
'[Param1]|[Param2]

'************************************
'* Envoie de fichier sur imprimante *
'************************************

'PRINTER$DIRECT[$NBCOPIES]|Fichier
'PRINTER$BATCH[$NBCOPIES]|Fichier
'ECRAN|Fichier
'DEFAULT|FICHIER

' \\DEALNT16\HP LaserJet 4000 Maintenance$DIRECT|C:\Travail\OF02362.lgr
' \\DELL_177\EPSON Stylus COLOR 900$DIRECT|C:\Travail\OF02362.lgr
' DIRECT|C:\Travail\OF02362.lgr
' Acrobat PdfWriter~DIRECT~1|D:\Travailv51\infusio.turbo*FileName*Kill[0/1]

'*********************************************************
'* Envoie de spool sur le net, Messagerie ou export HTML *
'*********************************************************

'OUTLOOK|FILE= TO= COPIES= OBJET= BODY= BODYFILE= FORMAT= MODE= SPOOLNAME= CRYPT=
'INTERNET|FILE= TO= COPIES= OBJET= BODY= BODYFILE= FORMAT= MODE= SPOOLNAME= CRYPT=
'HTML|FILE= DIR= MODE= SPOOLNAME= RUN=

' S�parateur $
' FORMAT : 0 -> Format Turbo , 1 -> Format HTML
' CRYPT : indique si le spool doit �tre crypt� : valable que pour le format Turbo
' FLAG pour le format HTML
' MODE : 0 -> Utiliser un navigateur , 1 -> Fichier unique , 2-> Simples Pages
' SPOOLNAME : 0 -> Pad de nom de spool , 1 -> Inclure le nom du spool
' DIR : R�pertoire d'export des fichiers HTML
' Si cet argument est omis , l'argument lu sera celui du fichier Turbograph.ini, Section PARAM , Cl� ExportHTML
' RUN =  0 -> Non , 1 -> OUI

'Ligne commande Export HTML
'HTML|file=D:\travailv51\test.cot$mode=0$spoolname=1run=0

'Ligne Commande Envoie Internet format Turbo
'INTERNET|File=D:\travailv51\test.cot$to=t.cot@deal.fr$objet=Test messagerie$body=Voici le fichier deamnd�$format=0
'avec fichier joint pour le body
'INTERNET|File=D:\travailv51\test.cot$to=t.cot@deal.fr$objet=Test messagerie$bodyfile=d:\travailv51\body.txt$format=0

'Ligne Commande Envoie Internet format HTML
'INTERNET|File=D:\travailv51\test.cot$to=t.cot@deal.fr$objet=Test messagerie$body=Voici le fichier deamnd�$format=1
'avec fichier joint pour le body
'INTERNET|File=D:\travailv51\test.cot$to=t.cot@deal.fr$objet=Test messagerie$bodyfile=d:\travailv51\body.txt$format=1

'Ligne Commande Envoie Outlook format Turbo
'OUTLOOK|File=D:\travailv51\test.cot$to=thierry cot$objet=Test messagerie$body=Voici le fichier demand�$format=0
'avec fichier joint pour le body
'OUTLOOK|File=D:\travailv51\test.cot$to=thierry cot$objet=Test messagerie$bodyfile=d:\travailv51\body.txt$format=0

'Ligne Commande Envoie Outlook format HTML
'OUTLOOK|File=D:\travailv51\test.cot$to=thierry cot$objet=Test messagerie$body=Voici le fichier demand�$format=1
'avec fichier joint pour le body
'OUTLOOK|File=D:\travailv51\test.cot$to=thierry cot$objet=Test messagerie$bodyfile=d:\travailv51\body.txt$format=1

'MESSAGERIE|File=D:\spool\test.turbo$to=d.marchal@deal.fr$objet=Test messagerie$bodyfile=d:\spool\body.txt$format=1

'Ligne Commande de batch envoi Email avec pdf
'BATCH|File=D:\travail\test.text$Suppr=1

'-> On ajoute une entr�e a la ligne de commande : |RV si recto verso demand�

'-> Activer la gestion des erreurs
On Error GoTo InterceptError

Zoom = 1
curPage = 1

'-> Lancer l'initialisation
Call Initialisation

'-> Si on est en version internet
If VersionTurbo = 1 Then
    '-> Charger les ressources en locale
    LoadInternetMessProg
    Verify_Update
Else
    '-> V�rifier que l'on trouve le fichier langue
    If Dir$(FichierLangue & "\TurboPrint1-" & FORMAT(IndexLangue, "00") & ".lng", vbNormal) = "" Then
        '-> On ne trouve pas le fichier : Charger la ressource en interne
        LoadInternetMessProg
    Else
        '-> Charger le fichier messprog
        RessourcePath = FichierLangue & "\"
        If Not (CreateMessProg("TurboPrint1-" & FORMAT(IndexLangue, "00") & ".lng", True)) Then GoTo InterceptError
    End If
End If 'Selon la version


'********************************************************************************
'* ANALYSE DE LA LIGNE DE COMMANDE ET GESTION DES DIFFERENTS MODES D'IMPRESSION *
'********************************************************************************

'-> R�cup�ration de la ligne de commande
LigneCommande = Command()
LigneCommande = LTrim(LigneCommande)

Trace "Ligne de commande :" + LigneCommande

'-> Supprimer les Quotters s'il y en a
If InStr(1, LigneCommande, """") = 1 Then _
    LigneCommande = Mid$(LigneCommande, 2, Len(LigneCommande) - 1)
If InStr(1, LigneCommande, """") <> 0 Then _
LigneCommande = Mid$(LigneCommande, 1, Len(LigneCommande) - 1)

'-> Initialisation des param�tres par d�faut
Mode = 1 'Mode ECRAN
nCopies = 1 'Nombre de copies

If Trim(LigneCommande) <> "" Then

    '-> Recherche d'un "|"
    If InStr(1, LigneCommande, "|") <> 0 Then
        
        '-> R�cup�ration du nom de fichier pass� en param�tre
        NomFichier = Entry(2, LigneCommande, "|")
        
        '-> R�cup�ration des param�tres d'impression
        Param1 = Entry(1, LigneCommande, "|")
        
        '-> Recup du parametre recto verso
        If Trim(UCase$(Entry(3, LigneCommande, "|"))) = "RV" Or Trim(UCase$(Entry(3, LigneCommande, "|"))) = "RVP" Then RectoVerso = True
        
        '-> Tester si on a pass� le code op�rateur sur la ligne
        If InStr(1, Param1, "$") <> 0 Then
            '-> Si entry 1 = "ECRAN, on a pass� le code langue
            If UCase$(Trim(Entry(1, Param1, "$"))) = "ECRAN" Or UCase$(Trim(Entry(1, Param1, "$"))) = "GETPRINTERS" Then
                '-> R�cup�rer le code langue sp�cifi�
                MyCodeLangue = Trim(Entry(2, Param1, "$"))
                If IsNumeric(MyCodeLangue) Then
                    '-> V�rifier si on trouve le fichier langue associ�
                    RessourcePath = FichierLangue & "\"
                    Do While Libelles.Count <> 0
                        Libelles.Remove (1)
                    Loop
                    '-> V�rifier que l'on trouve le fichier langue
                    If Dir$(FichierLangue & "\TurboPrint1-" & FORMAT(MyCodeLangue, "00") & ".lng", vbNormal) = "" Then
                        '-> On ne trouve pas le fichier : Charger la ressource en interne
                        LoadInternetMessProg
                    Else
                        '-> Charger le fichier messprog
                        RessourcePath = FichierLangue & "\"
                        If Not (CreateMessProg("TurboPrint1-" & FORMAT(MyCodeLangue, "00") & ".lng", True)) Then GoTo InterceptError
                    End If
                    '-> Poser les bonnes valeurs
                    If UCase$(Trim(Entry(1, Param1, "$"))) = "GETPRINTERS" Then
                        Param1 = "GETPRINTERS"
                        IndexLangue = CInt(MyCodeLangue)
                    End If
                End If '-> Si on a pass� un code langue correcte
            End If 'Si on est en mode �cran
        End If
            
        '-> Cas de l'internet et du HTML
        If UCase$(Param1) = "INTERNET" Then
            Mode = 4
            GoTo suite
        ElseIf UCase$(Param1) = "OUTLOOK" Then
            Mode = 5
            GoTo suite
        ElseIf UCase$(Param1) = "HTML" Then
            Mode = 6
            GoTo suite
        ElseIf UCase$(Param1) = "LOTUS" Then
            Mode = 7
            GoTo suite
        ElseIf UCase$(Param1) = "MESSAGERIE" Then
            Mode = 8
            GoTo suite
        ElseIf UCase$(Param1) = "DEALPDF" Then
            Mode = 9
            GoTo suite
        ElseIf Entry(1, UCase$(Param1), "~") = "GETPRINTERS" Then
            GetPrinterList NomFichier
        ElseIf UCase$(Param1) = "BATCH" Then
            '-> on lance en batch les lignes de commande
            RunBatch
            '-> quitter une fois les lignes traitees
            End
        ElseIf UCase$(Entry(1, Param1, "~")) = "ACROBAT PDFWRITER" Or UCase$(Entry(1, Param1, "~")) = "ACROBAT DISTILLER" Or InStr(1, UCase$(Entry(1, Param1, "~")), "ADOBE PDF") <> 0 Or UCase$(Entry(1, Param1, "~")) = "WIN2PDF" Or UCase$(Entry(1, Param1, "~")) = "PDFCREATOR" Then
            '-> Indiquer que l'on est en mode PDF
            IsPDF = True
            '-> Tester si on a sp�cifi� le nom du fichier PDF
            If InStr(1, NomFichier, "*") = 0 Then
                '-> Composer le nom du fichier PDF
                PdfDestinationFile = NomFichier & ".pdf"
                '-> On ne supprime pas
                KillSpool = False
            Else
                '-> R�cup�rer le fichier Destination
                PdfDestinationFile = Entry(2, NomFichier, "*")
                '-> Si on doit supprimer ou non
                If Trim(Entry(3, NomFichier, "*")) = "1" Then
                    KillSpool = True
                Else
                    KillSpool = False
                End If
                '-> Recomposer le nom du fichier
                NomFichier = Entry(1, NomFichier, "*")
            End If 'Si on a specifi� le nom du fichier PDF
            '-> Modifier le Registre si necessaire

            If Trim(PdfDestinationFile) <> "" Then
                If UCase$(Entry(1, Param1, "~")) = "WIN2PDF" Then
                    SetWin2PDFWriterFileName PdfDestinationFile
                Else
                    SetPdfWriterFileName PdfDestinationFile
                End If
                If UCase$(Entry(1, Param1, "~")) = "PDFCREATOR" Then SetPdfCreatorFileName PdfDestinationFile
                If UCase$(Entry(1, Param1, "~")) = "ADOBE PDF" Then SetAdobePdfFileName PdfDestinationFile
           End If
        End If
        '-> V�rifier que le fichier existe
        If Trim(Dir$(NomFichier, vbNormal)) <> "" Then
                        
            '-> Traiter la ligne de commande pour les imprimantes
            If UCase$(Param1) <> "ECRAN" Then
                '-> Tester si on a trouv� un ~
                If InStr(1, Param1, "~") <> 0 Then
                    DeviceName = Entry(1, Param1, "~") 'Nom de l'imprimante
                    ModeName = UCase$(Entry(2, Param1, "~")) 'Mode d'impression BATCH/DIRECT
                    Trace "Param�trage avec des ~"
                    If NumEntries(Param1, "~") = 3 Then
                        '-> Le nombre de copie est sp�cifi�
                        Tempo = Entry(3, Param1, "~")
                        If IsNumeric(Tempo) Then nCopies = CInt(Tempo)
                    End If
                Else
                    DeviceName = Entry(1, Param1, "$") 'Nom de l'imprimante
                    ModeName = UCase$(Entry(2, Param1, "$")) 'Mode d'impression BATCH/DIRECT
                    Trace "Param�trage avec des $"
                    If NumEntries(Param1, "$") = 3 Then
                        '-> Le nombre de copie est sp�cifi�
                        Tempo = Entry(3, Param1, "$")
                        If IsNumeric(Tempo) Then nCopies = CInt(Tempo)
                    End If
                End If
                
                'Tester si on demande d'imprimer sur l'imprimante par d�faut
                If UCase$(Trim(DeviceName)) = "DEFAULT" Then DeviceName = Printer.DeviceName
                
                    Trace "Recherche de l'imprimante : " & DeviceName
                    Trace "Mode d'impression : " & ModeName
                
                '-> Modifier le printer pour le mettre en ad�quation avec la demande
                FindPrinter = False
                
                For Each TempPrinter In Printers
                        Trace "On analyse l'imprimante : " & TempPrinter.DeviceName
                        Trace "On cherche l'imprimante : " & DeviceName
                    If UCase$(TempPrinter.DeviceName) = UCase$(DeviceName) Then
                        FindPrinter = True
                        Set Printer = TempPrinter
                        Trace "Imprimante trouv�e : " & Printer.DeviceName
                        Exit For
                    End If
                Next 'Pour toutes les imprimantes
                
                '-> V�rifier si imprimante Ok
                If FindPrinter Then
                    '-> On a trouver le printer sp�cifi� : s�lectionner un mode d'impression
                    Select Case UCase$(ModeName)
                        Case "BATCH"
                            Mode = 3
                        Case Else 'Mode DIRECT ou inconnu
                            Mode = 2
                    End Select
                End If 'Si on a trouv� un printer
                
                '-> Tester si le nombre de copies est renseign�
                If NumEntries(Param1, "$") = 3 Then
                    '-> Le nombre de copie est sp�cifi�
                    Tempo = Entry(3, Param1, "$")
                    If IsNumeric(Tempo) Then nCopies = CInt(Tempo)
                    
                End If 'Si le nombre de copie est sp�cifi�
            End If 'Si on passe un mode <> �cran
        End If 'Si on trouve le fichier pass� en param�tre
    Else
        '-> Pas de "|" donc le paramettre est sens� �tre un fichier : v�rifier s'il existe
        If Dir$(LigneCommande, vbNormal) <> "" Then
            NomFichier = LigneCommande
        Else
            NomFichier = ""
        End If
    End If 'S'il y a un "|"
End If 'Si la ligne de commande est vide
        
'****************************************************************
'* ANALYSE DU FICHIER ASCII POUR CREATION DES DIFFERENTS OBJETS *
'****************************************************************

suite:

'-> Dans tous les cas, charger la feuille bibliotheque : elle servira � stocker les diff�rents objets
Load frmLib

'******************************
'* TRAITEMENT DE L'IMPRESSION *
'******************************

If IsMouchard Then
    Print #hdlMouchard, "Mode d'impression : " & Mode
    'Close #hdlMouchard
End If


'ATTENTION : V6 Modification pour la gestion du ZIP : on teste si le fichier est ZIPE. S'il est ZIP,
'decompression dans le fichier temporaire
FilePath = Mid(NomFichier, 1, InStrRev(Replace(NomFichier, "\", "/"), "/"))
NomFichier = GetUnzipFileName(NomFichier)

Select Case Mode

    Case 1 '-> Impression ECRAN
    
        '-> Afficher dans un premier temps l'interface de chargement
        MDIMain.Show
        '-> Charger le fichier dans l'interface
        If NomFichier <> "" Then
            '-> Bloquer la feuille MDI
            MDIMain.Enabled = False
            '-> Afficher le fichier
            DisplayFileGUI NomFichier
            '-> Debloquer la feuille MDI
            MDIMain.Enabled = True
        End If
        '-> Charger eventuellement au menu les fichiers joints
        LoadJoinFile uZipInfo2, NomFichier
            
    Case 2, 3  '-> Impression en mode direct ,  Impression en batch
    
        '-> Setting du p�riph�rique de sortie
        Set Sortie = Printer
        
        '-> Passer le printer en mode pixel
        Printer.ScaleMode = 3
                
        '-> Lancer l'analyse du spool
        Call AnalyseFileToPrint(NomFichier)
        
        '-> Pointer sur l'objet fichier
        Set aFichier = Fichiers(UCase$(NomFichier))
        
        '-> on regarde si on a des copies assembl�es
        If Trim(UCase$(Entry(4, LigneCommande, "|"))) = "A" Then copyAssemb = True
        
        '-> on regarde si on doit pas imprimer la page de garde
        If Trim(UCase$(Entry(5, LigneCommande, "|"))) = "NOGARDE" Then noGarde = True
        
        '-> Pour le nombre de copies sp�cifi�
        For j = 1 To nCopies
            curPage = j
            '-> Lancer l'impression des pages
            For Each aSpool In aFichier.Spools
                '-> Initialisation du mode de l'imprimante
                If aSpool.Maquette.Orientation = 1 Then
                    Printer.Orientation = 1
                Else
                    Printer.Orientation = 2
                End If
                
                '-> PIERROT DUPLEX selon que portrait ou paysage
                Printer.Duplex = 1
                If RectoVerso Then
                    If Printer.Orientation = 1 Then
                        Printer.Duplex = 2
                    Else
                        Printer.Duplex = 3
                    End If
                End If
                
                '-> Pour g�rer la mise en page
                If Trim(UCase$(Entry(3, LigneCommande, "|"))) = "RVP" Or Trim(UCase$(Entry(3, LigneCommande, "|"))) = "NORVP" Then
                    Dim dlg As New PropertiesDialog
                    dlg.hwnd = frmPrint.hwnd 'Me.hWnd
                    dlg.ShowPrinter
                    Set dlg = Nothing
                End If
                
                '-> Initialiser le printer
                'Trace "Relecture du registre avant printer.print : " + GetRegKeyValue(HKEY_CURRENT_USER, "SoftWare\Adobe\Acrobat Distiller\PrinterJobControl", App.Path + "\" + App.EXEName & ".exe") + " et le command$=" + Command$
                Printer.Print ""
                
                '-> cause bug microsoft on definit bien le fond transparent
                SetBkMode Sortie.hDC, 1
                
                '-> Initialiser le compteur de page imprim� dans le spool
                SpoolPrint = 0
                            
                '-> Tester s'il ya des erreurs
                If aSpool.NbError = 0 Then
                    '-> Tester si on doit imprimer la page des s�lections
                    If aSpool.IsSelectionPage Then
                        CountPage = 0
                    Else
                        CountPage = 1
                    End If
                    
                    If noGarde Then CountPage = 1
                    
                    For i = CountPage To aSpool.NbPage
                        '-> Tester s'il y a des erreurs sur la page sp�cifi�e
                        If aSpool.GetErrorPage(i) <> "" Then
                            '-> R�cup�rer la liste des erreurs
                            Tempo = aSpool.GetErrorPage(i)
                            '-> Imprrmer la liste des erreurs
                            For k = 1 To NumEntries(Tempo, Chr(0))
                                Printer.Print Entry(i, Tempo, Chr(0))
                            Next '-> Pour toutes les erreurs
                        Else
                            '-> Tester la taille du lot de page
                            If SpoolSize <> 0 Then
                                If SpoolPrint = SpoolSize Then
                                    '-> Terminer le document en cours
                                    Trace "Avant impression si spoolsize"
                                    Printer.EndDoc
                                    '-> Initialisation du mode de l'imprimante
                                    If aSpool.Maquette.Orientation = 1 Then
                                        Printer.Orientation = 1
                                    Else
                                        Printer.Orientation = 2
                                    End If
                                    
                                    '-> Initialiser le printer
                                    Printer.Print ""
                                    '-> Initialiser le compteur de page imprim� dans le spool
                                    SpoolPrint = 0
                                End If 'Si on atteint le nombre limitte de page
                            End If 'Si param�trage du spooler par lot
                            If copyAssemb Then
                                For l = 1 To nCopies
                                    '-> Imprimer la page
                                    curPage = l
                                    PrintPageSpool aSpool, i
                                    If l <> nCopies Then Printer.NewPage
                                Next
                            Else
                                '-> Imprimer la page
                                PrintPageSpool aSpool, i
                            End If
                            '-> Gerer le debordement si le top de debordement est activer on refait l'impression avec une translation
                            If DebordementTop And Not TypeOf Sortie Is PictureBox Then
                                '-> on reinitialise le top
                                i = i - 1
                            End If
                            '-> Incr�menter le compteur de page imprim�
                            SpoolPrint = SpoolPrint + 1
                        End If
                        '-> Envoyer un saut de page si pas derni�re page
                        If i <> aSpool.NbPage Then Printer.NewPage
                    Next 'Pour toutes les pages
                Else
                    '-> Imprrmer la liste des erreurs
                    For k = 1 To aSpool.NbError
                        Printer.Print aSpool.GetErrorMaq(k)
                    Next '-> Pour toutes les erreurs
                End If
                '-> Fin du document pour ce spool
                Trace "Avant l'impression"
                Printer.EndDoc
            Next
            If copyAssemb Then j = nCopies
        Next 'Pour toutes les copies
        
        '-> Si on est en mode PDF et que l'on doit supprimer la source
        If IsPDF And KillSpool Then Kill NomFichier
        '-> Fin du programme
        End
        
    Case 4, 5, 6, 7, 8 'INTERNET, OUTLOOK , HTML, MESSAGERIE
    
        '-> Initialiser le p�riph�rique de sortie
        Set Sortie = frmLib.PicObj(0)
    
        '-> Analyse des param�tres pass�s en ligne de commande
        For i = 1 To NumEntries(NomFichier, "$")
            KeyValue = UCase$(Trim(Entry(1, Entry(i, NomFichier, "$"), "=")))
            pValue = Trim(Entry(2, Entry(i, NomFichier, "$"), "="))
            
            Select Case KeyValue
                Case "FILE"
                    pFile = pValue
                Case "TO"
                    pTo = pValue
                Case "COPIES"
                    pCopies = pValue
                Case "FROM"
                    pFrom = pValue
                Case "OBJET"
                    pObject = pValue
                Case "BODY"
                    pBody = pValue
                Case "BODYFILE"
                    pBodyFile = pValue
                Case "FORMAT"
                    pFormat = pValue
                Case "FORMATBODY"
                    pFormatBody = pValue
                Case "MODE"
                    pMode = pValue
                Case "SPOOLNAME"
                    pSpoolName = pValue
                Case "DIR"
                    pDir = pValue
                Case "RUN"
                    pRun = pValue
                Case "CRYPT"
                    pRun = pValue
                Case "PRINTER"
                    pPrinter = pValue
            End Select
        Next
    
        '-> Dans tous les cas, v�rifier si le fichier pass� en argument existe
        If Dir$(pFile, vbNormal) = "" Then End
        FilePath = Mid(pFile, 1, InStrRev(Replace(pFile, "\", "/"), "/"))
        '-> Setting du nom de fichier
        FileToExport = GetFileName(pFile)
        FileToExport = GetSpoolName(FileToExport) & ".html"
        
        '-> Setting de la cl� d'acc�s au fichier format HTML
        FileKeyExport = UCase$(Trim(pFile))

        '-> Setting de la cl� d'acces au fichier export Messagerie
        FileKeyMail = FileKeyExport

        '-> En mode  Internet, Outlook
        If Mode = 4 Or Mode = 5 Then
            '-> v�rifier si destinataire saisi
            If Trim(pTo) = "" Then End
            '-> V�rifier le format par d�faut
            If Not IsNumeric(pFormat) Then
                '-> Turbo par d�faut
                pFormat = "0"
            Else
                '-> Convertir
                If CLng(pFormat) <> 1 And CLng(pFormat) <> 4 And CLng(pFormat) <> 3 Then
                    pFormat = "0" 'Turbo par def
                End If
            End If
        End If
        
        '-> Tester si crypter ou non
        If Not IsNumeric(pCrypt) Then
            IsCryptedFile = False
        Else
            If CLng(pCrypt) <> "1" Then
                IsCryptedFile = False
            Else
                IsCryptedFile = True
            End If
        End If
        
        '-> Pour format HTML : format par d�faut Fichier Unique
        If pFormat = "1" Then
            If Not IsNumeric(pMode) Then
                '-> Fichier unique
                pMode = "1"
            Else
                If CLng(pMode) < 0 Or CLng(pMode) > 2 Then
                    pMode = "1"
                End If
            End If
        Else
            If pFormat <> "4" And CLng(pFormat) <> 3 Then pFormat = "0"
        End If
        
        '-> type de navigation
        If pMode <> "" Then NavigationType = CInt(pMode)
        
        '-> Export du fichier dans sa totalit�
        ExportType = 2
        ExportMail = 2
            
        '-> Inclure le nom du spool
        If pSpoolName = "" Then
            pSpoolName = "0"
        Else
            If Not IsNumeric(pSpoolName) Then
                pSpoolName = "0"
            Else
                If CLng(pSpoolName) <> 0 And CLng(pSpoolName) <> 1 Then pSpoolName = "0"
            End If
        End If
        
        '-> Inclusion des noms des spools
        IncluseSpoolNumber = CBool(pSpoolName)
    
        '-> Run after
        If IsNumeric(pRun) Then
            If CLng(pRun) = 1 Then
                RunAfterExport = True
            Else
                RunAfterExport = False
            End If
        Else
            RunAfterExport = False
        End If
            
        '-> R�pertoire d'export si export Format HTML
        If Param1 = "HTML" Then
            If pDir = "" Then
                If PathToExport = "" Then
                    End
                Else
                    If GetAttr(PathToExport) And vbDirectory <> vbDirectory Then End
                End If
            Else
                If GetAttr(pDir) And vbDirectory <> vbDirectory Then
                    End
                Else
                    PathToExport = pDir
                End If
            End If
        
            '-> Traitement du "\" du fin de path
            If Right$(PathToExport, 1) <> "\" Then PathToExport = PathToExport & "\"
        End If
    
        '-> Pas de temporisation �cran
        IsTempo = False
        
        '-> Setting de l'origine export
        OrigineExport = 3
        OrigineMail = OrigineExport
        
        '-> Cr�ation du mod�le objet en m�moire
        AnalyseFileToPrint pFile
        
        '-> si internet on regarde la messagerie possible existante
        If Mode = 4 Then
            If IsLotus Then Mode = 7
            If IsOutLook Then Mode = 5
        End If
        
        '-> Lancement du traitement
        If Mode = 6 Then
            '-> Mode Export HTML
            CreateEditionHTML
        Else
            Select Case Mode
                Case 4  'Internet
                    '-> Setting de la variable d'export
                    NetParam = pTo & Chr(0) & pObject & Chr(0) & pBody
                    Select Case pFormat
                        Case "0"   'Format Turbo
                            SendToInternet FileKeyExport, pBodyFile
                        Case "4" 'format pdf
                            CreatePdfToMessagerie pFile, pPrinter
                            '-> on pointe sur le nouveau fichier pdf cr��
                            'FileKeyExport = FileKeyExport & ".PDF"
                            CreateHTMLToBody False, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile & Chr(0) & pFormatBody & Chr(0) & pFormat
                        Case "3" 'sans piece jointe
                            CreateHTMLToBody False, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile & Chr(0) & pFormatBody & Chr(0) & pFormat
                        Case Else 'Format HTML
                            CreateHTMLToMessagerie False, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile
                    End Select
                Case 5, 7 'Outlook, Lotus Note
                    '-> Envoyer le mail par outlook
                    If Mode = 5 Then
                        SendOutLook = True
                        SendLotus = False
                    End If
                    If Mode = 7 Then
                        SendLotus = True
                        SendOutLook = False
                    End If
                    
                    
                    
                    Select Case pFormat 'format piece jointe
                        Case "0"  'Format Turbo
                            If pFormatBody <> "1" Then
                                '-> corps rtf
                                SendToOutLook FileKeyExport, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile
                            Else
                                '-> corps HTML
                                CreateHTMLToBody True, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile & Chr(0) & pFormatBody & Chr(0) & pFormat
                            End If
                        Case "3" 'sans piece jointe
                            CreateHTMLToBody True, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile & Chr(0) & pFormatBody & Chr(0) & pFormat
                        Case "4" 'Format Pdf
                            CreatePdfToMessagerie pFile, pPrinter
                            '-> on pointe sur le nouveau fichier pdf cr��
                            'FileKeyExport = FileKeyExport & ".PDF"
                            CreateHTMLToBody True, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile & Chr(0) & pFormatBody & Chr(0) & pFormat
                        Case Else 'Format HTML
                            If pFormatBody <> "1" Then
                                '-> corps rtf
                                CreateHTMLToMessagerie True, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile
                            Else
                                '-> corps Html
                                CreateHTMLToBody True, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile & Chr(0) & pFormatBody & Chr(0) & pFormat
                            End If
                    End Select
                Case 8 'par messagerie
                    sendToSMTP pFormat, pFrom, pFile, pCopies, pTo, pObject, pBody, pBodyFile
            End Select
        End If
            
        '-> End fin du traitement
        End
    Case 9 'DEALPDF cr�ation d'un fichier PDF en natif
        Shell App.Path & "\TurboPdf.exe " & "$fileToConvert=" & NomFichier & "$SORTIE=ECRAN$embedded"

End Select

Exit Sub
 

InterceptError:


    
    If Err.Number <> 482 Then MsgBox "Erreur dans la procedure MAIN" & Chr(13) & Err.Number & " - " & Err.Description, vbCritical + vbOKOnly, "Deal Informatique"
        
    '-> Appeler la gestion des erreurs
    End

End Sub

Private Sub sendToSMTP(sFormat As String, sfrom As String, sFile As String, sCopie As String, sTo As String, sObject As String, sBody As String, sBodyFile As String)
'--> cette proc�dure va permettre d'envoyer le message par le serveur de messagerie
Dim aFichier As Fichier
Dim aSpool As Spool

'-> Lancer l'analyse du spool
Call AnalyseFileToPrint(sFile)

'-> Pointer sur l'objet fichier
Set aFichier = Fichiers(UCase$(sFile))
Set frmMail.aSpool = aFichier.Spools(1)

'-> on charge la feuille
Load frmMail

Select Case sFormat
    Case "0"
        frmMail.FormatTurbo = True
    Case "1"
        frmMail.FormatHtml = True
    Case "3"
        frmMail.FormatNothing = True
    Case "4"
        frmMail.FormatPDF = True
End Select

frmMail.Text4.Text = sfrom
frmMail.Text7.Text = sTo
frmMail.Text6.Text = sCopie
frmMail.Text5.Text = sObject
If sBodyFile <> "" Then
    frmMail.RichTextBox2.LoadFile sBodyFile
End If
frmMail.RichTextBox2.Text = frmMail.RichTextBox2.Text & Chr(13) & sBody
'-> on envoi le mail
frmMail.sendBySMTP

End Sub

Private Sub SetPdfWriterFileName(FileName As String)

'---> Cette proc�dure initialise la cl� qui va bien dans le registre
'Acrobat PdfWriter~DIRECT~1|D:\Travailv51\2001-07-06-16-55-22.turbo

'-> Setting se sa valeur
SaveString HKEY_CURRENT_USER, "SoftWare\Adobe\Acrobat PDFWriter", "PDFFileName", FileName

End Sub

'*** PIERROT : modif pour gerer le soft : WIN2PDF
Private Sub SetWin2PDFWriterFileName(FileName As String)

'---> Cette proc�dure initialise la cl� qui va bien dans le registre
'Acrobat PdfWriter~DIRECT~1|D:\Travailv51\2001-07-06-16-55-22.turbo

'-> Setting se sa valeur
SaveString HKEY_CURRENT_USER, "SoftWare\Dane Prairie Systems\Win2PDF", "PDFFileName", FileName

End Sub
'*** PIERROT : modif pour gerer le soft : WIN2PDF

Private Sub SetPdfCreatorFileName(FileName As String)

'---> Cette proc�dure initialise la cl� qui va bien dans le registre
Dim strName As String

'-> Setting se sa valeur si imprimante locale
strName = Dir$(Mid(FileName, 1, Len(FileName) - 4), vbNormal)
SaveString HKEY_CURRENT_USER, "SoftWare\PDFCreator\Program", "AutosaveFilename", strName
strName = Replace(FileName, strName + ".pdf", "", , , vbTextCompare)
SaveString HKEY_CURRENT_USER, "SoftWare\PDFCreator\Program", "AutosaveDirectory", Mid(strName, 1, Len(strName) - 1)
SaveString HKEY_CURRENT_USER, "SoftWare\PDFCreator\Program", "LastsaveDirectory", Mid(strName, 1, Len(strName) - 1)
SaveString HKEY_CURRENT_USER, "SoftWare\PDFCreator\Program", "UseAutosave", "1"
SaveString HKEY_CURRENT_USER, "SoftWare\PDFCreator\Program", "RemoveAllKnownFileExtensions", "0"

'-> Setting se sa valeur si imprimante reseau
strName = Dir$(Mid(FileName, 1, Len(FileName) - 4), vbNormal)
SaveString HKEY_LOCAL_MACHINE, "SOFTWARE\PDFCreator\Program", "AutosaveFilename", strName
strName = Replace(FileName, strName + ".pdf", "", , , vbTextCompare)
SaveString HKEY_LOCAL_MACHINE, "SoftWare\PDFCreator\Program", "AutosaveDirectory", Mid(strName, 1, Len(strName) - 1)
SaveString HKEY_LOCAL_MACHINE, "SoftWare\PDFCreator\Program", "LastsaveDirectory", Mid(strName, 1, Len(strName) - 1)
SaveString HKEY_LOCAL_MACHINE, "SoftWare\PDFCreator\Program", "UseAutosave", "1"
SaveString HKEY_LOCAL_MACHINE, "SoftWare\PDFCreator\Program", "RemoveAllKnownFileExtensions", "0"
SaveString HKEY_LOCAL_MACHINE, "SoftWare\PDFCreator\Program", "AutosaveFormat", "0"

'-> On regarde si le parametrage est dans un fichier ini
If GetRegKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\" & "{0001B4FD-9EA3-4D90-A79E-FD14BA3AB01D}", "UseINI") = "1" Then
    If GetRegKeyValue(HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Common AppData") <> "" Then
        strName = Dir$(Mid(FileName, 1, Len(FileName) - 4), vbNormal)
        SetIniString "Options", "AutosaveFilename", Replace(GetRegKeyValue(HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Common AppData") & "\PDFCreator.ini", "\\", "\"), strName
        strName = Replace(FileName, strName + ".pdf", "", , , vbTextCompare)
        SetIniString "Options", "AutosaveDirectory", Replace(GetRegKeyValue(HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Common AppData") & "\PDFCreator.ini", "\\", "\"), Mid(strName, 1, Len(strName) - 1)
        SetIniString "Options", "LastsaveDirectory", Replace(GetRegKeyValue(HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Common AppData") & "\PDFCreator.ini", "\\", "\"), Mid(strName, 1, Len(strName) - 1)
        SetIniString "Options", "UseAutosave", Replace(GetRegKeyValue(HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Common AppData") & "\PDFCreator.ini", "\\", "\"), "1"
        SetIniString "Options", "RemoveAllKnownFileExtensions", Replace(GetRegKeyValue(HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Common AppData") & "\PDFCreator.ini", "\\", "\"), "0"
        SetIniString "Options", "AutosaveFormat", Replace(GetRegKeyValue(HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Common AppData") & "\PDFCreator.ini", "\\", "\"), "0"
    End If
End If

End Sub

Private Sub SetAdobePdfFileName(FileName As String)

'---> Cette proc�dure initialise la cl� qui va bien dans le registre
Trace "Filename du setadobepdf : " + FileName

'-> Setting de la valeur
SaveString HKEY_CURRENT_USER, "SoftWare\Adobe\Acrobat Distiller\PrinterJobControl", App.Path + "\" + App.EXEName & ".exe", FileName
Trace "Relecture du registre : " + GetRegKeyValue(HKEY_CURRENT_USER, "SoftWare\Adobe\Acrobat Distiller\PrinterJobControl", App.Path + "\" + App.EXEName & ".exe")
End Sub

Private Sub GestError(ByVal ErrorCode As Integer, Optional strError As String, Optional MySpool As Spool)

'---> Cette proc�dure  effectue la gestion centralis�e des erreurs

Dim aLb As Libelle
Dim aFichier As Fichier
Dim aSpool As Spool
Dim Tempo As String
Dim aNode As Node

On Error Resume Next

'-> Pointer sur le libelle Gestion des Erreurs
Set aLb = Libelles("ERRORS")
    
'-> Si on est en mode Visu : Cr�er l'icone du fichier dans le treeview et celle de l'erreur
If Mode = 1 Then
    Select Case ErrorCode 'Les erreurs 1 et 5 ne sont pas fatales -> Trait�es dans la proc�dure d'origine
        Case 2 '2-> Impossible d'ouvrir le fichier
            '-> Cr�er un nouveau Fichier
            Set aFichier = New Fichier
            aFichier.FileName = strError
            Fichiers.add aFichier, UCase$(Trim(strError))
            '-> Cr�er un nouveau Spool
            Set aSpool = aFichier.AddSpool
            aSpool.FileName = strError
            '-> Ajouter une erreur dans le spool
            aSpool.SetErrorMaq Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", strError)
        Case 4, 6
            '-> Erreur lors de la lecture d'une ligne du fichier
            '-> Pointer sur l'objet fichier correspondant au fichier
            Set aFichier = Fichiers(UCase(Trim(strError)))
            
            '-> S'il n'y a pas de spools : il faut en cr�er un
            If aFichier.Spools.Count = 0 Then
                Set aSpool = aFichier.AddSpool
                aSpool.FileName = strError
            End If
            
            '-> Ajouter l'erreur dans le spool
            aSpool.SetErrorMaq Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", strError)
        Case 7, 8, 9, 10
            '-> Gestion des erreurs dans la proc�dure MAQLECT
            '-> Pointer sur le fichier
            Set aFichier = Fichiers(UCase$(Trim(Entry(1, strError, "|"))))
            '-> Pointer sur le spool : pr�ciser en entry 3
            Set aSpool = MySpool
            '-> Faire le setting d'une erreur
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", aSpool.FileName)
            Tempo = Replace(Tempo, "$MAQ$", aSpool.MaquetteASCII)
            Tempo = Replace(Tempo, "$INI$", Entry(3, strError, "|"))
            aSpool.SetErrorMaq Tempo
        Case 11, 12
            '-> Gestion des erreurs dans la proc�dure INITCOM
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", strError)
            Tempo = Replace(Tempo, "$TABLEAU$", strError)
            Tempo = Replace(Tempo, "$MAQ$", MySpool.MaquetteASCII)
            MySpool.SetErrorMaq Tempo
        Case 14, 15, 16
            '-> Gestion des erreurs dans la proc�dure INITCOM
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$TABLEAU$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$MAQ$", MySpool.MaquetteASCII)
            MySpool.SetErrorMaq Tempo
        Case 17
            '-> Gestion des erreurs dans la proc�dure INITFILETEMPORTF
            MySpool.SetErrorMaq aLb.GetCaption(ErrorCode)
        Case 18
            '-> Gestion des erreurs dans la proc�dure INITMAQENTETE
            MySpool.SetErrorMaq aLb.GetCaption(ErrorCode) & " " & strError
        Case 19
            '-> Gestion des erreurs dans la proc�dure CREATEBMPOBJ
            MySpool.SetErrorMaq aLb.GetCaption(ErrorCode)
        Case 20, 21, 22
            '-> Gestion des erreurs dans la proc�dure CREATEBMPOBJ
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
            If NumEntries(strError, "|") > 3 Then
                Tempo = Replace(Tempo, "$HAUTEUR$", Entry(3, strError, "|"))
                Tempo = Replace(Tempo, "$LARGEUR$", Entry(4, strError, "|"))
            Else
                Tempo = Replace(Tempo, "$FICHIER$", Entry(3, strError, "|"))
            End If
            MySpool.SetErrorMaq Tempo
        Case 23
            '-> Gestion des erreurs dans la procedure CREATECADREOBJ
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 24
            '-> Gestion des erreurs dans la proc�dure CreateRTFCadre
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", Entry(3, strError, "|"))
            Tempo = Replace(Tempo, "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 25
            '-> Gestion des erreurs dans la proc�dure CreateRTFSection
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$SECTION$", Entry(1, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 26
            '-> Gestion des erreurs dans la proc�dure CreateSectionObj
            MySpool.SetErrorMaq Replace(aLb.GetCaption(ErrorCode), "$SECTION$", strError)
        Case 27
            '-> Gestion des erreurs dans la proc�dure InitSectionEntete
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$LIG$", Entry(2, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 28, 29
            '-> Gestion des erreurs dans la proc�dure InitBmpEntete et InitCadreEntete
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$LIG$", Entry(3, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 30
            '-> Gestion des erreurs dans la procedure InitTableauEntete
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$LIG$", Entry(2, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 31
            '-> Gestion des erreurs dans la procedure InitBlockEntete
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$LIG$", Entry(3, strError, "|"))
            MySpool.SetErrorMaq Tempo
         Case 32, 33, 34
            '-> Gestion des erreurs dans la proc�dure InitCreateBlock et InitBlock (34)
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
            If ErrorCode = 33 Then Tempo = Replace(Tempo, "$LIG$", Entry(3, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 35, 36, 37, 38, 39, 40, 41
            '-> Gestion des erreurs dans la proc�dure d'affichage d'une page : MODE VISU
            Set aNode = MDIMain.TreeNaviga.Nodes(UCase$(Trim(MySpool.FileName)) & "�" & UCase$(Trim(MySpool.Key)) & "�PAGE|" & MySpool.CurrentPage)
            aNode.Image = "Warning"
            aNode.Tag = "ERROR"
                        
            '-> Faire le setting de l'erreur
            Select Case ErrorCode
                Case 35, 36
                    MySpool.SetErrorPage MySpool.CurrentPage, Replace(aLb.GetCaption(ErrorCode), "$SECTION$", strError)
                Case 37
                    MySpool.SetErrorPage MySpool.CurrentPage, Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", strError)
                Case 38
                    MySpool.SetErrorPage MySpool.CurrentPage, Replace(aLb.GetCaption(ErrorCode), "$OBJECT$", strError)
                Case 39
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
                    MySpool.SetErrorPage MySpool.CurrentPage, Tempo
                Case 40
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
                    MySpool.SetErrorPage MySpool.CurrentPage, Tempo
                Case 41
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
                    Tempo = Replace(Tempo, "$CELLULE$", Entry(3, strError, "|"))
                    MySpool.SetErrorPage MySpool.CurrentPage, Tempo
            End Select
            
            '-> Afficher la page
            MySpool.DisplayInterfaceByPage (MySpool.CurrentPage)
    End Select
Else
    '-> On est en mode impression : initialiser une page : imprimer une erreur
    
End If








End Sub


Public Sub AnalyseFileToPrint(ByVal NomFichier As String)

'---> Cette proc�dure charge le ficher � analyser, cr�er la collection des spools et les maquettes

Dim hdlFile As Integer ' handle d'ouverture de fichier
Dim Ligne As String 'lecture d'une ligne du fichier ascii
Dim aSpool As Spool 'pointeur vers un objet spool
Dim NomMaquette As String 'nom de la maquette � ouvrir pour ancienne version
Dim aFichier As Fichier 'Pointeur vers un objet fichier
Dim LectureMaquette As Boolean 'Indique que l'on est en train de lire la d�finition de la maquette
Dim LectureSelection As Boolean 'Indique que l'on est en train de lire une page de s�lection
Dim LectureRTF As Boolean 'indique que l'on est en train de lire la d�finition RTF d'une maquette
Dim hdlRtf As Integer 'handle du fichier RTF dans lequel on �crit

Dim IsSpool As Boolean 'cette variable indique que l'on est en cours de cr�ation d'un spool
Dim Page As Integer 'Compteur de page par spool
Dim FindMaqChar As Boolean 'indique de plus charger les lignes de la maquettes car on analyse la maquette caract�re
Dim Res As Long
Dim hdlFichier As Long
Dim aOf As OFSTRUCT
Dim ErrorCode As Integer
Dim i As Integer

Dim IsAccDet As Boolean  '-> Indique que l'on est dans le fichier acc�s d�tail

On Error GoTo GestError

ErrorCode = 1
'-> R�cup�rer la taille du fichier que l'on charge
hdlFichier = OpenFile(NomFichier, aOf, OF_READ)
If hdlFichier <> -1 Then
    Res = GetFileSize(hdlFichier, 0)
    TailleTotale = Res
    Res = CloseHandle(hdlFichier)
End If

'-> Initialisation de la taille lue
TailleLue = 0

ErrorCode = 2
'-> Obtenir un handle de fichier et ouvrir le spool � imprimer
hdlFile = FreeFile
Open NomFichier For Input As #hdlFile

'-> Cr�er un nouvel objet Fichier et l'ajouter dans la collection
Set aFichier = New Fichier
aFichier.FileName = NomFichier

Fichiers.add aFichier, Trim(UCase$(NomFichier))

'-> Boucle d'analyse du fichier
Do While Not EOF(hdlFile)
    ErrorCode = 4
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> Decrypter la source
    Ligne = DeCrypt(Ligne)
    '-> Analyse des diff�rents cas que l'on peut rencontrer
    If InStr(1, Ligne, "%%GUI%%") <> 0 Then
        '-> R�cup�rer le nom de la maquette
        NomMaquette = Trim(Mid$(Ligne, 8, Len(Ligne) - 7))
        '-> Cr�er un nouvel objet Spool
        Set aSpool = aFichier.AddSpool
        '-> Indiquer le num�ro du spool
        aSpool.Num_Spool = aFichier.Spools.Count
        aSpool.FileName = aFichier.FileName
        aSpool.MaquetteASCII = NomMaquette
        aSpool.AddPage
        '-> Ancienne version : charger la maquette depuis son adresse
        aFichier.IsOldVersion = True
        MaqLect aSpool, NomMaquette
        '-> Indiquer que l'on est en cr�ation d'un spool
        IsSpool = True
        '-> Premi�re page
        Page = 1
        '-> Ajouter une nouvelle page dans le spool
        'aSpool.AddPage
        '-> RAZ des variables de positionnement
        LectureMaquette = False
        LectureSelection = False
    Else 'si on n'a pas trouv� %%GUI%%
        Select Case Trim(UCase$(Ligne))
            Case "[SPOOL]"
                '-> Cr�ation d'un nouveau spool
                Set aSpool = aFichier.AddSpool
                aSpool.AddPage
                '-> Indiquer le nom du fichier maitre
                aSpool.FileName = aFichier.FileName
                '-> Indiquer le num�ro du spool
                aSpool.Num_Spool = aFichier.Spools.Count
                '-> Indiquer que l'on est en cr�ation de spool
                IsSpool = True
                '-> Indiquer que l'on n'est plus dans l'acc�s au d�tail
                IsAccDet = False
                '-> On n'est pas encore en train de lire la maquette
                LectureMaquette = False
                LectureSelection = False
                FindMaqChar = False
                '-> Init du compteur de page
                Page = 1
                '-> Init de la premi�re page
                'aSpool.AddPage
            Case "[/SPOOL]"
                '-> Indiquer que 'on n'est plus dans un spool
                IsSpool = False
            Case "[MAQ]"
                LectureMaquette = True
                LectureSelection = False
            Case "[/MAQ]"
                LectureMaquette = False
            Case "[MAQCHAR]"
                FindMaqChar = True
            Case "[GARDEOPEN]"
                LectureSelection = True
                LectureMaquette = False
                '-> Indiquer  dans le spool en cours qu'il ya une page de s�lection
                aSpool.IsSelectionPage = True
            Case "[GARDECLOSE]"
                LectureSelection = False
            Case "[PAGE]"
                '-> Incr�menter le compteur de page
                Page = Page + 1
                '-> Ajouter une nouvelle page dans le spool
                aSpool.AddPage
            Case "[DETAIL]"
                '-> On passe en mode lecture sur le spool en cours
                IsAccDet = True
            Case "[/DETAIL]"
                '-> Fin du mode lecture de l'acc�s au d�tail
                IsAccDet = False
            Case Else
                '-> Ne traiter la ligne que si on est dans un spool
                If IsSpool Then
                    '-> Selon la nature du traitement
                    If IsAccDet Then
                        '-> Ne rien faire si on est en cours de lecture acces d�tail
                        If Trim(Ligne) <> "" Then aSpool.AddLigneDetail Ligne
                    ElseIf LectureMaquette Then
                        '-> Ajouter une ligne dans la d�finition de la maquette
                        If Not FindMaqChar Then aSpool.SetMaq Ligne
                    ElseIf LectureSelection Then
                        '-> Ajouter une ligne dans la page de s�lection
                        aSpool.SetPage 0, Ligne
                    Else
                        '-> ajouter une ligne de datas dans la page en cours
                        aSpool.SetPage Page, Ligne
                    End If 'Selon la nature de la ligne
                End If 'si on est dans un spool
        End Select 'Selon la ligne que l'on est en train de lire
    End If 'si on a trouv� %%GUI%%
    
    '-> Gestion de la temprisation
    If Mode = 1 Then
        TailleLue = TailleLue + Len(Ligne) + 2
        DrawWait
    End If
Loop 'Boucle d'analyse du fichier

ErrorCode = 5
'-> Fermer le fichier ascii
Close #hdlFile

'-> Si on n' a pas trouv� de spool, en cr�er un pour l'erreur
If aFichier.Spools.Count = 0 Then
    '-> D�router sur la gestion d'erreur
    ErrorCode = 6
    GoTo GestError
End If

'-> Supprimer la derni�re page si elle est � blanc
For Each aSpool In aFichier.Spools
    For i = aSpool.NbPage To 1 Step -1
        If Trim(aSpool.GetPage(i)) = "" Then
            aSpool.NbPage = aSpool.NbPage - 1
        Else
            '-> Ne supprimer qe les pages de fin qui sont blanches
            Exit For
        End If
    Next
Next

'-> supprimer le spool si il est vide
i = 1
Do While i <= aFichier.Spools.Count
    If aFichier.Spools(i).NbPage = 0 Then
        aFichier.NbSpool = aFichier.NbSpool - 1
        aFichier.Spools.Remove i
    Else
        i = i + 1
    End If
Loop

'-> Gestion de la temporisation
If Mode = 1 Then
    MDIMain.StatusBar1.Refresh
    '-> R�initialiser les variables de lecture
    TailleLue = 0
    TailleTotale = aFichier.Spools.Count
End If

'-> G�n�rer les maquettes tous les spools
For Each aSpool In aFichier.Spools
    If Mode = 1 Then
        TailleLue = TailleLue + 1
        DrawWait
    End If
    InitCom aSpool
Next

'-> Vider la toolbar
If Mode = 1 Then
    MDIMain.StatusBar1.Refresh
    MDIMain.picSplit.Visible = IsNaviga
    MDIMain.picNaviga.Visible = IsNaviga
End If
Exit Sub

GestError:
        
'-> Fermer le fichier ascii
Close #hdlFile
        
    '-> Traitement de l'erreur
    Select Case ErrorCode
        Case 1
            '-> Pb lors de la r�cup�ration de la taille du fichier : ne rien faire
            Resume Next
        Case 2, 3, 4, 6
            '-> Erreur fatale
            Call GestError(ErrorCode, NomFichier)
        Case 5
            '-> Erreur lors de la fermture du fichier ASCII : Fermer le fichier
            Reset
            '-> Rendre la main
    End Select
                                

End Sub

Private Function OpenRTFSpool(ByVal RtfName As String) As Integer

'---> Ouverture d'un spool pour stocker le contenu RTF de la section/cadre

Dim RtfFile As String
Dim hdlRtf As Integer

'-> R�cup�ration du r�pertoire temporaire de windows
RtfFile = GetTempFileNameVB("", True)
hdlRtf = FreeFile
Open RtfFile & "\" & RtfName For Output As #hdlRtf

'-> Renvoyer son handle
OpenRTFSpool = hdlRtf

End Function

Private Sub MaqLect(ByRef aSpool As Spool, ByVal aMaq As String)

'---> Cette proc�dure charge le fichier ASCII d'une maquette dans un objet spool : compatibilite ancienne version

Dim hdlFile As Integer
Dim TmpFile As String
Dim Ligne As String
Dim ErrorCode As Integer
Dim TmpError As String

On Error GoTo GestError

'-> Setting du nom de la maquette dans l'objet spool
aSpool.MaquetteASCII = aMaq

'-> V�rifier que le fichier Maquette est bien pr�sent
ErrorCode = 7
TmpError = aSpool.FileName & "|" & aMaq
If Dir$(aMaq, vbNormal) = "" Then
    '-> Setting d'une erreur
    Call GestError(7, TmpError, aSpool)
    '-> Sortir de la proc�dure
    Exit Sub
End If

'-> Cr�er un fichier Tempo et faire une copie de la maquette
ErrorCode = 8
TmpFile = GetTempFileNameVB("MAQ")
CopyFile aMaq, TmpFile, False
    

ErrorCode = 9
'-> Ouvrir le fichier Tempo
hdlFile = FreeFile
Open TmpFile For Input As #hdlFile

'-> Transf�rer la maquette dans l'objet Spool
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    ErrorCode = 10
    Line Input #hdlFile, Ligne
    If Trim(Ligne) = "" Then GoTo NextLigne
    '-> Decrypter la maquette
    Ligne = DeCrypt(Ligne)
    '-> Quitter si on arrive � la d�finition de la maquette caract�re
    If UCase$(Trim(Ligne)) = "[MAQCHAR]" Then Exit Do
    '-> Transfert de la ligne
    aSpool.SetMaq (Ligne)
NextLigne:
Loop

ErrorCode = 11
'-> Fermer le fichier ASCII
Close #hdlFile

ErrorCode = 12
'-> Supprimer le fichier Tempo
Kill TmpFile

'-> Indiquer que la maquette est OK
aSpool.Ok = True

Exit Sub

GestError:

    '-> Traiter l'erreur selon le cas
    Select Case ErrorCode
        Case 7
            Call GestError(ErrorCode, TmpError, aSpool)
                                    
        Case 11, 12  '-> Erreurs r�cup�rables
            '-> Fermer tous les fichiers ouverts
            Reset
            '-> Continuer
            Resume Next
        Case Else
            '-> Appeler la gestion des erreurs
            Call GestError(ErrorCode, aSpool.FileName & "|" & aMaq & "|" & TmpFile, aSpool)
    End Select

End Sub

Private Sub InitCom(aSpool As Spool)

'---> Cette proc�dure initialise pour un fichier donn� la maquette associ�e

Dim Ligne As String '-> Pour lecture d'une ligne de maquette
Dim i As Long '-> Compteur de ligne
Dim SwitchLect As Integer
'-> Valeur de SwitchLect :
    '0  -> Nothing
    '1  -> Entete Maquette
    '2  -> Section PROGICIEL
    '3  -> Section de Texte : Propri�t�s
    '4  -> D�finition BMP
    '5  -> Definition d'un Cadre
    '6  -> Definition RTF d'un cadre
    '7  -> D�finition RTF d'une section
    '8  -> D�fintion d'un tableau
    '9  -> D�fintion d'un block de tableau
    '10 -> D�fintion d'une cellule de tableau
    '11 -> Ecran de s�lection

Dim LibError As String '-> Libelle si erreur
Dim TempoStr As String '-> Variable de stockage tempo

'-> D�finition du model COM
Dim aSection As Section
Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim aTb As Tableau
Dim aBlock As Block
'Dim aCell As Cellule
Dim ErrorCode As Integer
Dim TmpError As String

On Error GoTo GestError

'-> Type de ligne par d�faut
SwitchLect = 0

'-> Initialisation de la maquette
Set aSpool.Maquette = New Maquette

'-> Analyse de la maquette propre au Spool
For i = 1 To NumEntries(aSpool.GetMaq, Chr(0))
    '-> R�cup�ration de la d�finition de la ligne
    Ligne = Entry(i, aSpool.GetMaq, Chr(0))
    '-> Tester le premier caract�re de la ligne
    If Left$(Ligne, 1) = "[" Then
        '-> Analyse du type d'objet
        If UCase$(Mid$(Ligne, 2, 2)) = "NV" Then
            '-> Si on attaque le descriptif OBJECT ou la definition TRF
            If UCase$(Entry(3, Ligne, "-")) = "STD]" Then
                '-> Cr�ation du nouvel objet Section de texte
                Set aSection = New Section
                aSection.Nom = Entry(2, Ligne, "-")
                '-> Ajout dans la collection
                ErrorCode = 11
                TmpError = UCase$(Trim(aSection.Nom))
                aSpool.Maquette.Sections.add aSection, UCase$(Trim(aSection.Nom))
                '-> Ligne de d�finition d'une section de texte
                SwitchLect = 3
            ElseIf UCase$(Entry(3, Ligne, "-")) = "RTF]" Then
                '-> Initialiser pour la section en cours un fichier et l'ouvrir
                TempoStr = InitFileTempoRtf(aSpool)
                aSection.TempoRTF = CInt(Entry(1, TempoStr, "|"))
                aSection.TempoRtfString = Entry(2, TempoStr, "|")
                '-> Ligne de d�finition RTF de la section
                SwitchLect = 7
            End If
        ElseIf UCase$(Mid$(Ligne, 2, 2)) = "TB" Then
            '-> Cr�ation du nouvel objet tableau
            Set aTb = New Tableau
            aTb.Nom = Mid$(Entry(2, Ligne, "-"), 1, Len(Entry(2, Ligne, "-")) - 1)
            aTb.IdAffichage = aSpool.Maquette.AddOrdreAffichage("TB-" & UCase$(Trim(aTb.Nom)))
            '-> Ajout dans la collection
            ErrorCode = 12
            TmpError = UCase$(Trim(aTb.Nom))
            
            aSpool.Maquette.Tableaux.add aTb, UCase$(Trim(aTb.Nom))
            '-> Positionner le pointeur de lecture
            SwitchLect = 8
        Else
            If UCase$(Trim(Ligne)) = "[PROGICIEL]" Then
                '-> analyse de la cle PROGICIEL de la maquette
                SwitchLect = 2
            End If
        End If '-> Si [
    Else
    'ElseIf InStr(1, Ligne, "\") <> 0 Then
        Select Case UCase$(Trim(Ligne))
            Case "\DEFENTETE�BEGIN"
                '-> D�finition de l'entete de la maquette
                SwitchLect = 1
            Case "\DEFENTETE�END"
                '-> Envoyer dans la proc�dure d'ajout adaptation des dimensions
                InitMaqEntete aSpool.Maquette, Ligne, aSpool
                '-> Fin de la d�inition de l'entete de la maquette
                SwitchLect = 0
            Case "\BMP�END"
                '-> Lancer la cr�ation physique de l'objet BMP
                CreateBmpObj aBmp, aSpool
                '-> Fin de d�fintion d'un objet BMP : repositionner sur pointeur de section
                SwitchLect = 3
            Case "\CADRE�END"
                '-> Lancer la cr�ation physique du cadre
                CreateCadreObj aCadre, aSpool
                '-> Fin de d�finition d'un objet cadre : repositionner sur pointeur de section
                SwitchLect = 3
            Case "\CADRERTF�BEGIN"
                '-> Initialiser le fichier RTF pour lecture
                TempoStr = InitFileTempoRtf(aSpool)
                aCadre.TempoRTF = CInt(Entry(1, TempoStr, "|"))
                aCadre.TempoRtfString = Entry(2, TempoStr, "|")
                '-> D�but de d�finition RTF d'un cadre
                SwitchLect = 6
            Case "\CADRERTF�END"
                '-> Fermer le fichier tempo RTF affect�
                ErrorCode = 13
                Close #aCadre.TempoRTF
                '-> Lancer la g�n�ration physique du contenu RTF du cadre
                CreateRTFCadre aCadre, aSpool
                '-> Fin de d�fintion RTF d'un cadre : repositionner sur pointeur de section
                SwitchLect = 3
            Case "\RTF�END"
                '-> Fermer le fichier tempo RTF affect�
                ErrorCode = 13
                Close #aSection.TempoRTF
                '-> Lancer la g�n�ration physique du contenu RTF de la section de texte
                CreateRTFSection aSection, aSpool
                '-> Fin de d�finition RTF d'une section
                SwitchLect = 0
            Case "\TABLEAU�END"
                '-> Fin de d�finition d'un tableau
                SwitchLect = 0
            Case "\RANG�END"
                '-> Fin de d�finition d'une section de texte
                SwitchLect = 0
            Case Else
                Select Case SwitchLect
                    Case 1 'Analyse d'une ligne d'entete
                        InitMaqEntete aSpool.Maquette, Ligne, aSpool
                    Case 2 'Analyse de la section PROGICIEL
                        If UCase$(Trim(Entry(1, Ligne, "="))) = "\PROG" Then
                            aSpool.Maquette.Progiciel = Entry(2, Ligne, "=")
                        ElseIf UCase$(Trim(Entry(1, Ligne, "="))) = "\CLI" Then
                            aSpool.Maquette.Client = Entry(2, Ligne, "=")
                        End If
                    Case 3 'On est dans une section de texte : analyse des tag <CADRE> et <BMP>  et <TEXTE>
                        Select Case UCase$(Trim(Entry(1, Ligne, "�")))
                            Case "\BMP"
                                '-> Cr�ation d'un nouvel objet BMP
                                Set aBmp = New ImageObj
                                aBmp.Nom = Entry(2, Ligne, "�")
                                aBmp.IdOrdreAffichage = aSection.AddOrdreAffichage("BMP-" & UCase$(Trim(aBmp.Nom)))
                                aBmp.SectionName = aSection.Nom
                                '-> Ajout dans la collection des Bmps
                                ErrorCode = 14
                                TmpError = aSection.Nom & "|" & UCase$(Trim(aBmp.Nom))
                                aSection.Bmps.add aBmp, UCase$(Trim(aBmp.Nom))
                                '-> Positionnement du pointeur de lecture
                                SwitchLect = 4
                            Case "\CADRE"
                                '-> Cr�ation d'un nouvel objet cadre
                                Set aCadre = New Cadre
                                aCadre.Nom = Entry(2, Ligne, "�")
                                aCadre.IdAffichage = aSection.AddOrdreAffichage("CDR-" & UCase$(Trim(aCadre.Nom)))
                                aCadre.SectionName = aSection.Nom
                                ErrorCode = 15
                                TmpError = aSection.Nom & "|" & UCase$(Trim(aCadre.Nom))
                                aSection.Cadres.add aCadre, UCase$(Trim(aCadre.Nom))
                                '-> Positionnement du pointeur de lecture
                                SwitchLect = 5
                            Case "\TEXTE"
                                '-> Fin de d�finition d'un objet Section: Cr�ation de la repr�sentation physique
                                CreateSectionObj aSection, aSpool
                                '-> on initialise les libell�s variables
                                GetLibelVariableForSection aSpool, aSection
                            Case Else
                                '->Dans ce cas, on lit une propri�t� de la section: ajouter dans la d�fintion de la section
                                InitSectionEntete aSpool, aSection, Ligne
                        End Select 'Selon la nature de la ligne
                    Case 4
                        '-> Ligne de d�finition d'un BMP
                        InitBmpEntete aBmp, aSpool, Ligne
                    Case 5
                        '-> Ligne de d�finition d'un cadre
                        InitCadreEntete aCadre, aSpool, Ligne
                    Case 6
                        '-> Imprimer la ligne RTF du cadre dans son fichier TEMPO
                        Print #aCadre.TempoRTF, Ligne
                    Case 7
                        '-> Imprimer la ligne RTF de la section texte dans son fichier tempo
                        Print #aSection.TempoRTF, Ligne
                    Case 8
                        If UCase$(Trim(Entry(1, Ligne, "�"))) = "\BEGIN" Then
                            '-> Cr�ation d'un nouvel objet Block
                            Set aBlock = New Block
                            aBlock.Nom = Entry(2, Ligne, "�")
                            aBlock.NomTb = Trim(UCase$(aTb.Nom))
                            aBlock.AlignementLeft = 3
                            aBlock.Left = 0
                            aBlock.IdOrdreAffichage = aTb.AddOrdreAffichage("BL-" & UCase$(Trim(aBlock.Nom)))
                            '-> Ajouter le block dans la collection des tableaux
                            ErrorCode = 16
                            TmpError = aSpool.MaquetteASCII & "|" & Trim(UCase$(aTb.Nom)) & UCase$(Trim(aBlock.Nom))
                            aTb.Blocks.add aBlock, "BL-" & UCase$(Trim(aBlock.Nom))
                            '-> Initialiser les matrices des lignes et colonnes
                            Erase sCol()
                            Erase sLigne()
                            LigLue = 1
                            '-> Postionnement du pointeur de lecture sur l'analyse des bloks
                            SwitchLect = 9
                        Else
                            '-> Ajouter � la d�finition du tableau en cours
                            InitTableauEntete aTb, aSpool, Ligne
                        End If
                    Case 9
                        If UCase$(Trim(Entry(1, Ligne, "�"))) = "\END" Then
                            '-> Fin de d�finition d'un objet block : lancer les initialisations et les cr�ations
                            InitCreateBlock aBlock, aTb, aSpool
                            '-> Se repositionner sur la lecture du tableau
                            SwitchLect = 8
                        Else
                            '-> Analyse d'un ligne d'un block
                            InitBlockEntete aBlock, aTb, aSpool, Ligne
                        End If
                End Select 'Selon la nature de la lecture de la ligne
        End Select
    End If '-> Si premier caract�re est un [
Next 'Pour toutes les lignes de la maquette


'-> G�n�ration ici de la section de texte pour la page des s�lections
'-> Cr�er nouvel objet RTF
Set aSection = New Section
aSection.Largeur = aSpool.Maquette.Largeur - aSpool.Maquette.MargeLeft * 2
aSection.Hauteur = aSpool.Maquette.Hauteur - aSpool.Maquette.MargeTop * 2
aSection.AlignementLeft = 2
aSection.AlignementTop = 2
aSection.BackColor = &HFFFFFF

'-> Charger un nouvel �diteur RTF dans la biblioth�que
Load frmLib.Rtf(IdRTf)
frmLib.Rtf(IdRTf).Font.Name = "Lucida Console"
aSection.IdRTf = IdRTf

'-> Incr�menter le compteur d'objet RTF
IdRTf = IdRTf + 1

'-> Ajouter dans la collection des secstion de la maquette
aSpool.Maquette.Sections.add aSection, SelectRTFKey
                

Exit Sub

GestError:
    
    If ErrorCode = 13 Then
        Reset
        Resume Next
    Else
        Call GestError(ErrorCode, TmpError, aSpool)
    End If

End Sub
Private Function InitFileTempoRtf(ByRef aSpool As Spool) As String

'---> Cette fonction creer un fichier tempo pour lecture des RTF et renvoie :
' Handle|NomFichier

On Error GoTo GestError

Dim TempFile As String
Dim hdlFile As String

TempFile = GetTempFileNameVB("RTF")
hdlFile = FreeFile
Open TempFile For Output As #hdlFile

InitFileTempoRtf = hdlFile & "|" & TempFile

Trace "Fichier temporaire du rtf page" & TempFile

Exit Function

GestError:
    Call GestError(17, , aSpool)
    
End Function

Private Sub CreateRTFSection(ByRef aSection As Section, aSpool As Spool)

'---> Cette proc�dure charge le contenu du fichier TEmpoRTF adns l'objet affect� et le supprime
Dim ErrorCode As Integer

On Error GoTo GestError

'-> Charger le fichier RTF
frmLib.Rtf(aSection.IdRTf).LoadFile aSection.TempoRtfString
ErrorCode = -1
'-> Supprimer le fichier
Kill aSection.TempoRtfString

Exit Sub

GestError:
    
    If ErrorCode = -1 Then Resume Next
    Call GestError(25, aSection.Nom & "|" & aSection.TempoRtfString, aSpool)
    

End Sub

Private Sub CreateRTFCadre(ByRef aCadre As Cadre, aSpool As Spool)

'---> Cette proc�dure charge le contenu du fichier TEmpoRTF adns l'objet affect� et le supprime

On Error GoTo GestError

Dim ErrorCode As Integer

'-> Charger le fichier RTF
frmLib.Rtf(aCadre.IdRTf).LoadFile aCadre.TempoRtfString

'-> Supprimer le fichier
ErrorCode = -1
Kill aCadre.TempoRtfString

Exit Sub

GestError:

    If ErrorCode = -1 Then Resume Next
    Call GestError(24, aCadre.SectionName & "|" & aCadre.Nom & "|" & aCadre.TempoRtfString, aSpool)
    


End Sub


Private Sub InitMaqEntete(aMaq As Maquette, Ligne As String, aSpool As Spool)

'---> Cette proc�dure effectue le setting des propri�t�s de l'entete de la maquette

Dim DefLigne As String
Dim ValueLigne As String
Dim Tempo As Single
Dim ErrorCode As Integer

On Error GoTo GestError

'-> Tester que la ligne commence par un "\"
If InStr(1, Ligne, "\") = 0 Then Exit Sub

'-> R�cup�ration des valeurs
DefLigne = Entry(1, Ligne, "�")
ValueLigne = Entry(2, Ligne, "�")
'-> Traiter la valeur de la ligne
Select Case UCase$(DefLigne)
    Case "\NOM"
        aMaq.Nom = ValueLigne
    Case "\HAUTEUR"
        aMaq.Hauteur = CSng(Convert(ValueLigne))
    Case "\LARGEUR"
        aMaq.Largeur = CSng(Convert(ValueLigne))
    Case "\MARGETOP"
        aMaq.MargeTop = CSng(Convert(ValueLigne))
    Case "\MARGELEFT"
        aMaq.MargeLeft = CSng(Convert(ValueLigne))
    Case "\ORIENTATION"
        aMaq.Orientation = CInt(ValueLigne)
    Case "\PAGEGARDE" 'Version 2.0
        If UCase$(ValueLigne) = "VRAI" Then
            aMaq.PageGarde = True
        Else
            aMaq.PageGarde = False
        End If
    Case "\RIGHTPAGE"
        If UCase$(ValueLigne) = "VRAI" Then
            AllowDebordement = True
        Else
            AllowDebordement = False
        End If
    Case "\NAVIGATION"
        If UCase$(ValueLigne) = "VRAI" Then
            IsNaviga = True
        Else
            'IsNaviga = False
        End If
    Case "\PAGESELECTION" 'Version 2.0
        If UCase$(ValueLigne) = "VRAI" Then
            aMaq.PageSelection = True
        Else
            aMaq.PageSelection = False
        End If
    Case "\DEFENTETE"
        '-> Ajuster les dimensions
        If aMaq.Orientation <> 1 Then
            Tempo = aMaq.Largeur
            aMaq.Largeur = aMaq.Hauteur
            aMaq.Hauteur = Tempo
        End If
    Case "\NAVIGAHTML"
        '-> Setting du navigateur
        aMaq.NavigaHTML = Trim(ValueLigne)
        
End Select

Exit Sub

'-> Gestion des erreurs
GestError:

    Call GestError(18, Ligne, aSpool)
    

End Sub

Private Sub InitSectionEntete(ByRef aSpool As Spool, ByRef aSection As Section, Ligne As String)

'---> Cette proc�dure effectue le setting des propri�t�s d'une section de texte

Dim DefLigne As String
Dim ValueLigne As String

On Error GoTo GestError

If InStr(1, Ligne, "\") = 0 Then Exit Sub

 '-> R�cup�rer les valeurs
DefLigne = UCase$(Entry(1, Ligne, "�"))
ValueLigne = Entry(2, Ligne, "�")

'-> Analyse
Select Case DefLigne
    Case "\CONTOUR"
        If CInt(ValueLigne) = 0 Then
            aSection.Contour = False
        Else
            aSection.Contour = True
        End If
    Case "\HAUTEUR"
        aSection.Hauteur = CSng(Convert(ValueLigne))
    Case "\LARGEUR"
        aSection.Largeur = CSng(Convert(ValueLigne))
    Case "\ALIGNEMENTTOP"
        aSection.AlignementTop = CInt(ValueLigne)
    Case "\TOP"
        aSection.Top = CSng(Convert(ValueLigne))
    Case "\ALIGNEMENTLEFT"
        aSection.AlignementLeft = CInt(ValueLigne)
    Case "\LEFT"
        aSection.Left = CSng(Convert(ValueLigne))
    Case "\BACKCOLOR"
        aSection.BackColor = CLng(ValueLigne)
    Case "\TEXTE"
End Select

Exit Sub

GestError:
    
    Call GestError(27, aSection.Nom & "|" & Ligne, aSpool)

End Sub

Private Sub InitBmpEntete(aBmp As ImageObj, aSpool As Spool, Ligne As String)

'---> Cette proc�dure effectue le setting des propri�t�s d'un objet BMP

Dim DefLigne As String
Dim ValueLigne As String

On Error GoTo GestError

If InStr(1, Ligne, "\") = 0 Then Exit Sub

 '-> R�cup�rer les valeurs
DefLigne = UCase$(Entry(1, Ligne, "�"))
ValueLigne = Entry(2, Ligne, "�")

'-> Analyse
Select Case DefLigne
    Case "\LEFT"
        aBmp.Left = CSng(Convert(ValueLigne))
    Case "\TOP"
        aBmp.Top = CSng(Convert(ValueLigne))
    Case "\LARGEUR"
        aBmp.Largeur = CSng(Convert(ValueLigne))
    Case "\HAUTEUR"
        aBmp.Hauteur = CSng(Convert(ValueLigne))
    Case "\FICHIER"
        aBmp.Fichier = ValueLigne
    Case "\CONTOURBMP"
        If CInt(ValueLigne) = 1 Then
            aBmp.Contour = True
        Else
            aBmp.Contour = False
        End If
    Case "\VARIABLE"
        If CInt(ValueLigne) = 1 Then
            aBmp.isVariable = True
        Else
            aBmp.isVariable = False
        End If
    Case "\USEASSOCIATION"
        If CInt(ValueLigne) = 1 Then
            aBmp.UseAssociation = True
        Else
            aBmp.UseAssociation = False
        End If
    Case "\PATH"
        aBmp.Path = ValueLigne
End Select

Exit Sub

GestError:
    
    Call GestError(28, aBmp.SectionName & "|" & aBmp.Nom & "|" & Ligne, aSpool)
    

End Sub

Private Sub InitCadreEntete(ByRef aCadre As Cadre, ByRef aSpool As Spool, Ligne As String)

'---> Cette proc�dure effectue le setting des propri�t�s d'un cadre

Dim DefLigne As String
Dim ValueLigne As String

On Error GoTo GestError

If InStr(1, Ligne, "\") = 0 Then Exit Sub

 '-> R�cup�rer les valeurs
DefLigne = UCase$(Entry(1, Ligne, "�"))
ValueLigne = Entry(2, Ligne, "�")

'-> Analyse
Select Case DefLigne
    Case "\LEFT"
        aCadre.Left = CSng(Convert(ValueLigne))
    Case "\TOP"
        aCadre.Top = CSng(Convert(ValueLigne))
    Case "\DISTANCE" 'Equivalent � MargeInterne
        aCadre.MargeInterne = CSng(Convert(ValueLigne))
    Case "\HAUTEUR"
        aCadre.Hauteur = CSng(Convert(ValueLigne))
    Case "\LARGEUR"
        aCadre.Largeur = CSng(Convert(ValueLigne))
    Case "\LARGEURBORDURE"
            aCadre.LargeurTrait = CInt(ValueLigne)
    Case "\ROUNDRECT"
            aCadre.IsRoundRect = CInt(ValueLigne)
    Case "\CONTOUR"
        '-> Bordure haut
        If Entry(1, ValueLigne, ",") = "1" Then
            aCadre.Haut = True
        Else
            aCadre.Haut = False
        End If
        '-> Bordure bas
        If Entry(2, ValueLigne, ",") = "1" Then
            aCadre.Bas = True
        Else
            aCadre.Bas = False
        End If
        '-> Bordure gauche
        If Entry(3, ValueLigne, ",") = "1" Then
            aCadre.Gauche = True
        Else
            aCadre.Gauche = False
        End If
        '-> Bordure droite
        If Entry(4, ValueLigne, ",") = "1" Then
            aCadre.Droite = True
        Else
            aCadre.Droite = False
        End If
    Case "\COULEUR"
        aCadre.BackColor = CLng(ValueLigne)
End Select

Exit Sub

GestError:

    Call GestError(29, aCadre.SectionName & "|" & aCadre.Nom & "|" & Ligne, aSpool)
    

End Sub

Private Sub CreateSectionObj(ByRef aSection As Section, ByRef aSpool As Spool)

'---> Cette proc�dure cr�er les diff�rents objets pour impression de la section

On Error GoTo GestError

'-> Cr�ation d'un contenuer RTF
Load frmLib.Rtf(IdRTf)
aSection.IdRTf = IdRTf

'-> Appliquer les propri�t�s
frmLib.Rtf(IdRTf).BackColor = aSection.BackColor
If aSection.Contour Then
    frmLib.Rtf(IdRTf).BorderStyle = rtfFixedSingle
Else
    frmLib.Rtf(IdRTf).BorderStyle = rtfNoBorder
End If

'-> Incr�menter le compteur d'objets RTF
IdRTf = IdRTf + 1
Exit Sub

GestError:

    Call GestError(26, aSection.Nom, aSpool)

End Sub

Private Sub CreateCadreObj(ByRef aCadre As Cadre, ByRef aSpool As Spool)

'---> Cette proc�dure cr�er une instance d'un objet RTF pour impression du cadre

On Error GoTo GestError

'-> charger un nouvel objet du type RTF
Load frmLib.Rtf(IdRTf)
aCadre.IdRTf = IdRTf

'-> Setting des prori�t�s
frmLib.Rtf(IdRTf).BackColor = aCadre.BackColor
frmLib.Rtf(IdRTf).Visible = True

'-> Incr�menter le compteur d'objets
IdRTf = IdRTf + 1

Exit Sub

GestError:

    Call GestError(23, aCadre.SectionName & "|" & aCadre.Nom, aSpool)
    


End Sub

Private Sub CreateBmpObj(ByRef aBmp As ImageObj, ByRef aSpool As Spool)

'---> Cette proc�dure cr�er une instance d'un objet picturebox pour impression du BMP

Dim Res As Long
Dim lpBuffer As String
Dim TempoStr As String
Dim Version As String
Dim Lecteur As String
Dim i As Integer
Dim ErrorCode As Integer
Dim TmpError As String
Dim V6Root As String
Dim ImagePath As String


On Error GoTo GestError

'-> Chargement de la repr�sentation physique
ErrorCode = 19
TmpError = ""
Load frmLib.PicObj(IdBmp)

'-> Contour du Bmp
If aBmp.Contour Then
    frmLib.PicObj(IdBmp).BorderStyle = 1
Else
    frmLib.PicObj(IdBmp).BorderStyle = 0
End If

'-> Donner ses dimensions au Bmp
ErrorCode = 20
TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Largeur & "|" & aBmp.Hauteur
frmLib.PicObj(IdBmp).Height = frmLib.ScaleX(aBmp.Hauteur, 7, 1)
frmLib.PicObj(IdBmp).Width = frmLib.ScaleY(aBmp.Largeur, 7, 1)

'-> Selon la version
If VersionTurbo = 0 Then 'On est en version Pathv6

    '-> V�rifier que l'on trouve le fichier Ini
    If TurboMaqIniFile = "" Then GoTo AfterLoading
    
    '-> R�cup�rer le path Root
    lpBuffer = Space$(100)
    Res = GetPrivateProfileString("Param", "$ROOT", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
    If Res <> 0 Then V6Root = Mid$(lpBuffer, 1, Res)
    Trace "aBmp.Path : " + aBmp.Path
    Select Case UCase$(aBmp.Path)
        Case "PARAM"
            Res = GetPrivateProfileString("Param", "$MAQGUIPATH", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
            If Res = 0 Then
                aBmp.Fichier = ""
                GoTo AfterLoading
            End If
            ImagePath = Replace(Replace(UCase$(Mid$(lpBuffer, 1, Res)), "$ROOT", V6Root), "$APP", "emilie")
        Case "APP"
            Res = GetPrivateProfileString("Param", "$MAQGUIPATH", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
            If Res = 0 Then
                aBmp.Fichier = ""
                GoTo AfterLoading
            End If
            ImagePath = Replace(Replace(UCase$(Mid$(lpBuffer, 1, Res)), "$ROOT", V6Root), "$APP", aSpool.Maquette.Progiciel)
        Case "CLIENT"
            Res = GetPrivateProfileString("Param", "$IDENTPATH", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
            If Res = 0 Then
                aBmp.Fichier = ""
                GoTo AfterLoading
            End If
            ImagePath = Replace(Replace(Replace(UCase$(Mid$(lpBuffer, 1, Res)), "$ROOT", V6Root), "$APP", aSpool.Maquette.Progiciel), "$IDENT", aSpool.Maquette.Client)
    End Select
    Trace "ImagePath : " + ImagePath
    '-> V�rifier si on trouve l'image
    If Dir$(ImagePath & "\" & aBmp.Fichier) <> "" Then
        ErrorCode = 22
        TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Fichier
        frmLib.PicObj(IdBmp).Picture = LoadPicture(ImagePath & "\" & aBmp.Fichier)
'        aBmp.Fichier = lpBuffer & aBmp.Fichier PIERROT : client-serveur en VERSION V6
        aBmp.Fichier = ImagePath & "\" & aBmp.Fichier
   ' Else   PIERROT : client-serveur en VERSION V6 : ne pas changer le nom du fichier de forme ^0001 si path parametr� => MODE PARAM
   '     aBmp.Fichier = ""
    End If 'Si on trouve l'image
Else
    '-> On est en version professionnelle
    If aBmp.isVariable Then
        aBmp.IsAutosize = True
    Else
        aBmp.IsAutosize = False
        Trace "CreateBmpObj non variable aBmp.Fichier : " + aBmp.Fichier
        If Trim(aBmp.Fichier) <> "" Then
            '-> Rechercher l'image dans le sous r�pertoire Images
            lpBuffer = App.Path & "\Images\" & aBmp.Fichier
            '-> Tester que l'on trouve l'image
            If Dir$(lpBuffer, vbNormal) <> "" Then
                '-> Essayer de charger l'image
                ErrorCode = 22
                TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Fichier
                frmLib.PicObj(IdBmp).Picture = LoadPicture(lpBuffer)
                aBmp.Fichier = lpBuffer
            Else
                '-> on essaye eventuellement de telecharger le fichier
                Trace "CreateBmpObj telechargement : " + TurboGraphWebFile & "/deallogo/" & aBmp.Fichier & App.Path & "\Images\" & aBmp.Fichier
                If TelechargeFile(TurboGraphWebFile & "/deallogo/" & aBmp.Fichier, App.Path & "\Images\" & aBmp.Fichier) Then
                    '-> Rechercher l'image dans le sous r�pertoire Images
                    lpBuffer = App.Path & "\Images\" & aBmp.Fichier
                    '-> Tester que l'on trouve l'image
                    If Dir$(lpBuffer, vbNormal) <> "" Then
                        '-> Essayer de charger l'image
                        ErrorCode = 22
                        TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Fichier
                        frmLib.PicObj(IdBmp).Picture = LoadPicture(lpBuffer)
                        aBmp.Fichier = lpBuffer
                    End If
                Else
                    '-> impossible de trouver le fichier associ�
                    aBmp.Fichier = ""
                End If
            End If
        Else 'La propri�t� fichier est � blanc
            aBmp.Fichier = ""
        End If
    End If
End If 'Selon la version

Trace "CreateBmpObj chemin de l'image aBmp.Fichier : " + aBmp.Fichier

AfterLoading:

'-> Positionner l'objet
frmLib.PicObj(IdBmp).Top = frmLib.PicObj(IdBmp).ScaleY(aBmp.Top, 7, 1)
frmLib.PicObj(IdBmp).Left = frmLib.PicObj(IdBmp).ScaleX(aBmp.Left, 7, 1)
frmLib.PicObj(IdBmp).AutoSize = aBmp.IsAutosize

'-> Enregistrer l'index du picture box associ�
aBmp.IdPic = IdBmp

'-> Rendre l'objet visible
frmLib.PicObj(IdBmp).Visible = True

'-> Incr�menter
IdBmp = IdBmp + 1


Exit Sub

GestError:

    Call GestError(ErrorCode, TmpError, aSpool)
    

End Sub

Private Sub InitTableauEntete(ByRef aTb As Tableau, ByRef aSpool As Spool, Ligne As String)

'---> Cette proc�dure effectue le settin g des prorpi�t�s d'un tableau

Dim DefLigne As String
Dim ValueLigne As String

On Error GoTo GestError

If InStr(1, Ligne, "\") = 0 Then Exit Sub

 '-> R�cup�rer les valeurs
DefLigne = UCase$(Entry(1, Ligne, "�"))
ValueLigne = Entry(2, Ligne, "�")

'-> Analyse
Select Case DefLigne
    Case "\LARGEURTB"
        aTb.Largeur = CSng(Convert(ValueLigne))
    Case "\ORIENTATIONTB"
        aTb.Orientation = CInt(ValueLigne)
End Select


Exit Sub



GestError:
    Call GestError(30, aTb.Nom & "|" & Ligne, aSpool)
    


End Sub

Private Sub InitBlockEntete(ByRef aBlock As Block, aTb As Tableau, aSpool As Spool, Ligne As String)

'---> Cette proc�dure effectue le setting des propri�t�s d'un block

Dim DefLigne As String
Dim ValueLigne As String

On Error GoTo GestError

If InStr(1, Ligne, "\") = 0 Then Exit Sub

 '-> R�cup�rer les valeurs
DefLigne = UCase$(Entry(1, Ligne, "�"))
ValueLigne = Entry(2, Ligne, "�")

'-> Analyse
Select Case DefLigne
    Case "\ALIGNEMENT" 'Garder pour compatibilit� 1.00
        If CInt(ValueLigne) = 0 Then 'Marge gauche
            aBlock.AlignementLeft = 2
        ElseIf CInt(ValueLigne) = 1 Then 'Marge Droite
            aBlock.AlignementLeft = 4
        ElseIf CInt(ValueLigne) = 2 Then 'Centr�
            aBlock.AlignementLeft = 3
        ElseIf CInt(ValueLigne) = 3 Then 'Alignement Libre
            aBlock.AlignementLeft = 5 'La valeur Left est aliment�e par la valeur "\DISTANCE"
        End If
    Case "\ALIGNLEFT" 'Version 2.00 de "\Alignement"
        aBlock.AlignementLeft = CInt(ValueLigne)
    Case "\LEFT" 'Valeur de AlignLeft quand il est sp�cifi� : valeur 5
        aBlock.Left = CSng(Convert(ValueLigne))
    Case "\ALIGNTOP" 'Version  2.00 de "\AlignementBlockVertical"
        aBlock.AlignementTop = CInt(ValueLigne)
    Case "\TOP" 'Valeur de AlignTop quand il est sp�cifi� : valeur 5
        aBlock.Top = CSng(Convert(ValueLigne))
    Case "\LARGEUR"
        aBlock.Largeur = CSng(Convert(ValueLigne))
    Case "\HAUTEUR"
        aBlock.Hauteur = CSng(Convert(ValueLigne))
    Case "\LIGNE"
        aBlock.NbLigne = CInt(ValueLigne)
        ReDim sLigne(1 To CInt(ValueLigne))
    Case "\COLONNE"
        aBlock.NbCol = CInt(ValueLigne)
        ReDim sCol(1 To CInt(ValueLigne))
    Case "\EXPORTLIG"
        aBlock.ListeExcel = ValueLigne
    Case "\ACCESDET"
        aBlock.KeyAccesDet = Trim(ValueLigne)
    Case "\COL"
        sLigne(LigLue) = Ligne
        LigLue = LigLue + 1
End Select

Exit Sub

GestError:
    Call GestError(31, aTb.Nom & "|" & aBlock.Nom & "|" & Ligne, aSpool)

End Sub
Private Sub InitCreateBlock(ByRef aBlock As Block, ByRef aTb As Tableau, ByRef aSpool As Spool)

'---> Cette proc�dure cr�er les diff�rentes cellules � partir des matrices sCol et sLigne

Dim aCell As Cellule
Dim i As Integer
Dim j As Integer
Dim DefLigne As String
Dim DefCellule As String
Dim DefBordure As String
Dim ErrorCode As Integer
Dim AccesDetail As String
Dim ListeCell As String

On Error GoTo GestError

'-> Initialiser les colonnes et les lignes
aBlock.Init

'-> Gestion des exports des lignes vers Excel Attention , cette propri�t� _
ne contient que la liste des lignes non exportables vers EXCEL
If Trim(aBlock.ListeExcel) <> "" Then
    For i = 1 To NumEntries(aBlock.ListeExcel, "|")
        aBlock.SetExportExcel CInt(Entry(i, aBlock.ListeExcel, "|")), False
    Next
End If

'-> Alimenter les Matrices de ligne te de colonne et cr�ation des cellules
For i = 1 To LigLue - 1
    '-> R�cup�ration de la d�finition de la ligne
    ErrorCode = 32
    DefLigne = sLigne(i)
    '-> Setting de la hauteur de ligne
    aBlock.SetHauteurLig i, CSng(Convert(Entry(2, DefLigne, "�")))
    '-> Recup�rer QUE la liste des colonnes
    DefLigne = Entry(3, DefLigne, "�")
    For j = 1 To aBlock.NbCol
        ErrorCode = 33
        '-> Recup de la d�finition de la colonne
        DefCellule = Entry(j, DefLigne, "|")
        '-> Setting de la largeur
        aBlock.SetLargeurCol j, CSng(Convert(Entry(3, DefCellule, ";")))
        '-> Cr�ation de la cellule associ�e
        Set aCell = New Cellule
        aCell.Ligne = i
        aCell.Colonne = j
        '-> Ajout dans le tableau
        aBlock.Cellules.add aCell, "L" & i & "C" & j
        '-> setting des propri�t�s
        aCell.CellAlign = CInt(Entry(1, DefCellule, ";"))
        aCell.Contenu = Entry(2, DefCellule, ";")
        '-> Initialisation des champs
        aCell.InitChamp
        '-> Gestion des bordures
        DefBordure = Entry(4, DefCellule, ";")
        If UCase$(Entry(1, DefBordure, ",")) = "VRAI" Then
            aCell.BordureHaut = True
        Else
            aCell.BordureHaut = False
        End If
        If UCase$(Entry(2, DefBordure, ",")) = "VRAI" Then
            aCell.BordureBas = True
        Else
            aCell.BordureBas = False
        End If
        If UCase$(Entry(3, DefBordure, ",")) = "VRAI" Then
            aCell.BordureGauche = True
        Else
            aCell.BordureGauche = False
        End If
        If UCase$(Entry(4, DefBordure, ",")) = "VRAI" Then
            aCell.BordureDroite = True
        Else
            aCell.BordureDroite = False
        End If
        '-> Propri�t�s de Font
        aCell.FontName = Entry(5, DefCellule, ";")
        aCell.FontSize = CSng(Convert(Entry(6, DefCellule, ";")))
        If UCase$(Trim(Entry(7, DefCellule, ";"))) = "VRAI" Then
            aCell.FontBold = True
        Else
            aCell.FontBold = False
        End If
        If UCase$(Trim(Entry(8, DefCellule, ";"))) = "VRAI" Then
            aCell.FontItalic = True
        Else
            aCell.FontItalic = False
        End If
        If UCase$(Trim(Entry(9, DefCellule, ";"))) = "VRAI" Then
            aCell.FontUnderline = True
        Else
            aCell.FontUnderline = False
        End If
        aCell.FontColor = CLng(Entry(10, DefCellule, ";"))
        If CLng(Entry(11, DefCellule, ";")) = 999 Then
            aCell.BackColor = 16777215
            aCell.FondTransparent = True
        Else
            aCell.BackColor = CLng(Entry(11, DefCellule, ";"))
            aCell.FondTransparent = False
        End If
        '-> AutoAjust
        If UCase$(Entry(12, DefCellule, ";")) = "VRAI" Then
            aCell.AutoAjust = True
        Else
            aCell.AutoAjust = False
        End If
        '-> Format et Type
        DefBordure = Entry(13, DefCellule, ";")
        If Entry(1, DefBordure, "@") <> "" Then aCell.TypeValeur = CInt(Entry(1, DefBordure, "@"))
        aCell.Msk = Entry(2, DefBordure, "@")
        If NumEntries(DefBordure, "@") = 3 Then aCell.PrintZero = True
        
        '-> Export vers Excel Fusion
        If NumEntries(DefCellule, ";") > 13 Then
            '-> R�cup�rer le param�trage
            DefBordure = Entry(14, DefCellule, ";")
            aCell.IsFusion = CBool(Entry(1, DefBordure, "@"))
            aCell.ColFusion = CInt(Entry(2, DefBordure, "@"))
        End If
        
        '-> Impl�mnter le mod�le objet associ� � excel
        aCell.TurboToExcel
        '-> on pose les libell�s variables
        GetLibelVariableForCell aBlock, aSpool, aCell
    Next 'Pour toute les colonnes
Next 'Pour toutes les lignes

'-> Gestion de l'acc�s au d�tail
If aBlock.KeyAccesDet <> "" Then
    '-> Basculer
    AccesDetail = aBlock.KeyAccesDet
    '-> Setting des param�tres du block
    aBlock.UseAccesDet = True
    aBlock.KeyAccesDet = Entry(1, AccesDetail, "�")
    '-> Setting des cellules servant � l'acc�s au d�tail
    ListeCell = Entry(2, AccesDetail, "�")
    For i = 1 To NumEntries(ListeCell, "|")
        '-> Pointeur vers une cellule
        Set aCell = aBlock.Cellules("L" & Entry(1, Entry(i, ListeCell, "|"), "-") & "C" & Entry(2, Entry(i, ListeCell, "|"), "-"))
        '-> forcer le format
        aCell.FontColor = RGB(0, 0, 255)
        aCell.FontUnderline = True
        '-> Indiquer qu'elle sert d'acc�s au d�tail
        aCell.UseAccesDet = True
    Next 'Pour toutes les d�finitions de cellules
End If 'S'il y a l'acc�s au d�tail sur ce block

'-> Convertion des dimensions des cellules en pixels
InitBlock aBlock, aSpool


Exit Sub

GestError:
    If ErrorCode = 32 Then
        Call GestError(ErrorCode, aTb.Nom & "|" & aBlock.Nom, aSpool)
    Else
        Call GestError(ErrorCode, aTb.Nom & "|" & aBlock.Nom & "|" & DefCellule, aSpool)
    End If
    
End Sub



Public Sub InitBlock(ByVal BlockCours As Block, aSpool As Spool)

'---> Cette proc�dure initialise les matrices Lignes (), Colonnes () et Cells _
en partant des largeurs de colonnes et hauteur de lignes

Dim i As Integer
Dim j As Integer
Dim NbCol As Integer
Dim NbLig As Integer
'-> Dimensions en CM
Dim HauteurCm As Single
Dim LargeurCm As Single
'-> Convertion en Pixels pour dessin
Dim HauteurPix As Long
Dim LargeurPix As Long
Dim hPix As Long
Dim lPix As Long
'-> Matrices des lignes et colonnes
Dim Lignes() As Long
Dim Colonnes() As Long

Dim LargeurColPix As Long
Dim HauteurLigPix As Long

Dim aCell As Cellule
Dim PosX As Long
Dim PosY As Long

On Error GoTo ErrorOpen

'-> R�cup�ration des propri�t�s
NbCol = BlockCours.NbCol
NbLig = BlockCours.NbLigne
HauteurCm = BlockCours.Hauteur
LargeurCm = BlockCours.Largeur

'-> Convertion en pixels
If TypeOf Sortie Is PictureBox Then
    '-> Ecran
    LargeurPix = CLng(frmLib.ScaleX(LargeurCm, 7, 3))
    HauteurPix = CLng(frmLib.ScaleY(HauteurCm, 7, 3))
Else
    'Printer.Copies = 1
    LargeurPix = CLng(Printer.ScaleX(LargeurCm, 7, 3))
    HauteurPix = CLng(Printer.ScaleY(CDbl(HauteurCm), 7, 3))
End If

'-> Affecter la valeur au block
BlockCours.LargeurPix = LargeurPix
BlockCours.HauteurPix = HauteurPix

'-> Calcul des dimensions de base
ReDim Lignes(1 To NbLig)
ReDim Colonnes(1 To NbCol)

'-> Largeur de base des colonnes
For i = 1 To NbCol
    Colonnes(i) = Fix((BlockCours.GetLargeurCol(i) / LargeurCm) * ((LargeurPix - NbCol + 1)))
    lPix = lPix + Colonnes(i)
Next

'-> Hauteur de base des lignes
For i = 1 To NbLig
    Lignes(i) = Fix((BlockCours.GetHauteurLig(i) / HauteurCm) * (HauteurPix - (NbLig + 1)))
    hPix = hPix + Lignes(i)
Next

Dim aCumul As Double
Dim bCumul  As Long
Dim a As Double

'-> Lissage des colonnes
bCumul = 1

For i = 1 To NbCol
    '-> Largeur en pixel normale
    aCumul = aCumul + BlockCours.GetLargeurCol(i)
    LargeurColPix = CInt(Sortie.ScaleX(aCumul, 7, 3))
    bCumul = bCumul + Colonnes(i) + 1
    a = LargeurColPix - bCumul
    Colonnes(i) = Colonnes(i) + a
    bCumul = LargeurColPix
Next

'-> Lissage des lignes
aCumul = 0
bCumul = 1

For i = 1 To NbLig
    '-> Hauteur de pixel normale
    aCumul = aCumul + BlockCours.GetHauteurLig(i)
    HauteurLigPix = CInt(Sortie.ScaleY(aCumul, 7, 3))
    bCumul = bCumul + Lignes(i) + 1
    a = HauteurLigPix - bCumul
    Lignes(i) = Lignes(i) + a
    bCumul = HauteurLigPix
Next

'-> Pixel de d�part
PosY = 1
PosX = 1

For i = 1 To NbCol
    For j = 1 To NbLig
        '-> Mettre � jour les valeurs des cellules
        Set aCell = BlockCours.Cellules("L" & j & "C" & i)
        aCell.x1 = PosX
        aCell.y1 = PosY
        aCell.X2 = PosX + Colonnes(i) + 1
        aCell.Y2 = PosY + Lignes(j) + 1
        '-> Incr�menter le compteur de position
        PosY = PosY + Lignes(j) + 1
    Next 'pour toutes les lignes
    '-> initialisation des valeurs
    PosX = PosX + Colonnes(i) + 1
    PosY = 1
Next 'Pour toutes les colonnes

'-> Lib�rer le pointeur de cellule
Set aCell = Nothing

'-> Sortir de la fonction
Exit Sub

ErrorOpen:

    Call GestError(34, BlockCours.NomTb & "|" & BlockCours.Nom, aSpool)

End Sub

Public Sub DisplayFileGUI(ByVal FileName As String)

'---> Cette proc�dure charge et affiche un fichier en particulier

Dim aNode As Node
Dim aNode2 As Node
Dim aNode3 As Node
Dim aFichier As Fichier
Dim aSpool As Spool
Dim i As Integer
Dim aLb As Libelle
Dim aFrm As frmDisplaySpool
Dim IsTitrePage As Boolean
Dim IsTitreMail As Boolean

On Error Resume Next

'-> Bloquer l'interface
MDIMain.Enabled = False
Screen.MousePointer = 11

'-> Positionner le p�riph�rique d'impression sur un objet de type PictureBox
Set Sortie = frmLib.PicObj(0)

'-> Lancer le chargement et l'analyse du fichier s�lectionn�
Call AnalyseFileToPrint(FileName)

'-> Cr�er son icone dans le treeview
Set aNode = MDIMain.TreeNaviga.Nodes.add(, , UCase$(Trim(FileName)), FileName, "Close")
aNode.Tag = "FICHIER"
aNode.ExpandedImage = "Open"
aNode.Expanded = True

'-> Pointer sur la classe libell�
Set aLb = Libelles("MDIMAIN")

'-> Pointer sur le fichier
Set aFichier = Fichiers(UCase$(Trim(FileName)))

'-> Cr�er une icone par spool
For Each aSpool In aFichier.Spools
    '-> on regarde si on a specifi� un titre
    If InStr(1, aSpool.GetMaq, "[TITRE]") <> 0 Then
        IsTitrePage = True
    Else
        IsTitrePage = False
    End If
    '-> on regarde si on a specifi� un titre
    If InStr(1, aSpool.GetMaq, "[MAIL]") <> 0 Then
        IsTitreMail = True
    Else
        IsTitreMail = False
    End If
    
    Set aNode2 = MDIMain.TreeNaviga.Nodes.add(aNode.Key, 4, aNode.Key & "�" & aSpool.Key, aSpool.SpoolText, "Spool")
    aNode2.Tag = "SPOOL"
    aNode2.Expanded = True
    '-> Soit la liste des erreurs
    If aSpool.NbError = 0 Then
        '-> Afficher une page pour la liste des s�lections utilisateur
        If aSpool.IsSelectionPage Then
            Set aNode3 = MDIMain.TreeNaviga.Nodes.add(aNode2.Key, 4, aNode2.Key & "�PAGE|0", aLb.GetCaption(12) & "S�lection", "Page")
            aNode3.Tag = "PAGE"
        End If
        
        '-> Afficher une icone par page
        For i = 1 To aSpool.NbPage
            '-> Recuperer eventuellement le titre de la feuille
            If IsTitrePage Then
                Set aNode3 = MDIMain.TreeNaviga.Nodes.add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, GetTitrePage(aSpool, i), "Page")
            Else
                Set aNode3 = MDIMain.TreeNaviga.Nodes.add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, aLb.GetCaption(12) & i, "Page")
            End If
            aNode3.Tag = "PAGE"
        Next
    Else
        '-> Afficher l'icone de la page d'erreur
        Set aNode3 = MDIMain.TreeNaviga.Nodes.add(aNode2.Key, 4, aNode2.Key & "�ERROR", aLb.GetCaption(11), "Warning")
        aNode3.Tag = "ERROR"
    End If 'S'il y a des erreurs
Next 'Pour tous les spools

'-> Afficher le premier Spool dans l'interface
Set aSpool = aFichier.Spools(1)

'-> Cr�er une nouvelle instance
Set aFrm = New frmDisplaySpool
'-> Affect� le spool
Set aFrm.aSpool = aSpool
'-> Affecter la feuille au spool
Set aSpool.frmDisplay = aFrm
'-> Imprimer si necessaire
If aSpool.NbError <> 0 Then
    aSpool.DisplayInterfaceByPage (1)
    '-> Charger le fichier en erreur
    aFrm.RtfError.LoadFile aSpool.FileName
Else
    '-> Imprimer la premi�re page
    PrintPageSpool aSpool, 1
    '-> Afficher la feuille en fonction du r�sultat de l'impression
    aSpool.DisplayInterfaceByPage (1)
End If

GestError:
    '-> Bloquer l'interface
    MDIMain.Enabled = True
    Screen.MousePointer = 0


End Sub


Public Sub PrintPageSpool(ByRef aSpool As Spool, ByVal pageToPrint As Integer)

'---> Cette proc�dure imprime une page d'un spool

Dim i As Integer, j As Integer
Dim PositionX As Long
Dim PositionY As Long
Dim IsPaysage As Boolean
Dim Ligne As String
Dim TypeObjet As String
Dim NomObjet As String
Dim TypeSousObjet As String
Dim NomSousObjet As String
Dim Param As String
Dim DataFields As String
Dim FirstObj As Boolean
Dim aNode As Node
Dim ErrorCode As Integer
Dim aSection As Section
Dim x As Control
Dim MargeOld As Integer

On Error GoTo GestError

Trace "PrintPageSpool --------------------" & pageToPrint & " --------------------"

'-> Initialisation du p�riph�rique selon la nature de la sortie
If TypeOf Sortie Is PictureBox Then
    ErrorCode = 35
    '-> Conserver la valeur du zoom
    Zoom = Val(aSpool.frmDisplay.Combo1.Text) / 100
    '-> Vider les erreurs de page
    aSpool.InitErrorPage pageToPrint
    '-> Modifier la page courrante de l'objet Spool
    aSpool.CurrentPage = pageToPrint
    '-> Mode Visualisation �cran
    aSpool.InitDisplayPage
    '-> Positionner le pointeur de sortie vers le picturebox de la feuille
    Set Sortie = aSpool.frmDisplay.Page
    '-> Supprimer les labels d'acc�s au d�tail
    For Each x In aSpool.frmDisplay.Controls
        If UCase$(x.Name) = "LBLACCESDET" Then
            If x.Index <> 0 Then Unload aSpool.frmDisplay.lblAccesDet(x.Index)
        End If
    Next
    '-> Gestion de l'icone de la page s�lectionn�e
    If aSpool.IsSelectionPage Then
        j = 0
    Else
        j = 1
    End If
    For i = j To aSpool.NbPage
        '-> Tester si une page est en erreur ou non avant de modifier sa page
        If MDIMain.TreeNaviga.Nodes(UCase(Trim(aSpool.FileName)) & "�" & UCase$(Trim(aSpool.Key)) & "�PAGE|" & i).Tag <> "ERROR" Then
            If i = pageToPrint Then
                MDIMain.TreeNaviga.Nodes(UCase(Trim(aSpool.FileName)) & "�" & UCase$(Trim(aSpool.Key)) & "�PAGE|" & i).Image = "PageSelected"
            Else
                MDIMain.TreeNaviga.Nodes(UCase(Trim(aSpool.FileName)) & "�" & UCase$(Trim(aSpool.Key)) & "�PAGE|" & i).Image = "Page"
            End If
        End If
    Next
    '-> Modifier l'affichage de la page en cours
    aSpool.frmDisplay.DisplayCurrentPage
    '-> Bloquer la feuille
    aSpool.frmDisplay.Enabled = False
    '-> Gestion de la temporisation
    TailleLue = 0
    TailleTotale = NumEntries(aSpool.GetPage(pageToPrint), Chr(0))
    '-> Bloquer l'interface
    MDIMain.Enabled = False
    Screen.MousePointer = 11
Else
    '-> Mode Impression direct ou batch sur une imprimante
End If

'-> Initialiser le RTF d'impression de la page des s�lections
If pageToPrint = 0 Then
    '-> Pointer sur la section texte
    Set aSection = aSpool.Maquette.Sections(SelectRTFKey)
    '-> Vider le RTF associ�
    frmLib.Rtf(aSection.IdRTf).Text = ""
End If

'-> Sauvegarde de l'ancienne marge
If DebordementTop And Not TypeOf Sortie Is PictureBox Then
    MargeOld = MargeX - GetDeviceCaps(Sortie.hDC, PHYSICALOFFSETX)
End If

'-> R�cup�ration des marges internes du contexte de p�riph�rique
MargeX = GetDeviceCaps(Sortie.hDC, PHYSICALOFFSETX)
MargeY = GetDeviceCaps(Sortie.hDC, PHYSICALOFFSETY)

'-> Initialiser la position X , y du pointeur sur les marges du document
PositionY = -MargeY + Sortie.ScaleY(aSpool.Maquette.MargeTop, 7, 3)
PositionX = -MargeX + Sortie.ScaleX(aSpool.Maquette.MargeLeft, 7, 3)

PositionX = PositionX * Zoom
PositionY = PositionY * Zoom

'-> Gerer le debordement si le top de debordement est activer on refait l'impression avec une translation
If DebordementTop And Not TypeOf Sortie Is PictureBox Then
    MargeX = MargeX + Sortie.ScaleWidth + MargeOld
    '-> on reinitialise le top
    DebordementTop = False
End If

'-> Indiquer l'�tat du premier objet que l'on trouve
FirstObj = True

'-> Lecture des lignes de la page
For i = 1 To NumEntries(aSpool.GetPage(pageToPrint), Chr(0))
    '-> Gestion de la temporisation
    If Mode = 1 Then
        TailleLue = i
        DrawWait
    End If
    '-> R�cup�ration de la ligne en cours
    Ligne = Trim(Entry(i, aSpool.GetPage(pageToPrint), Chr(0)))
    '-> Ne rien imprimer si page � blanc
    If Trim(Ligne) = "" Then GoTo NextLigne
    
    If pageToPrint = 0 Then
        frmLib.Rtf(aSection.IdRTf).Text = frmLib.Rtf(aSection.IdRTf).Text & Chr(13) & Chr(10) & Ligne
        frmLib.Rtf(aSection.IdRTf).SelStart = 0
        frmLib.Rtf(aSection.IdRTf).SelLength = Len(frmLib.Rtf(aSection.IdRTf).Text)
        frmLib.Rtf(aSection.IdRTf).SelFontName = "Lucida Console"
    Else
        If InStr(1, Ligne, "[") = 1 Then
            '-> r�cup�ration des param�tres
            AnalyseObj Ligne, TypeObjet, NomObjet, TypeSousObjet, NomSousObjet, Param, DataFields
            '-> Selon le type d'objet
            If UCase$(TypeObjet) = "ST" Then
                If Not PrintSection(NomObjet, Param, DataFields, FirstObj, PositionX, PositionY, aSpool) Then Exit For
                FirstObj = False
            ElseIf UCase$(TypeObjet) = "TB" Then
                If Not PrintTableau(NomObjet, Param, DataFields, PositionX, PositionY, NomSousObjet, FirstObj, aSpool) Then Exit For
                FirstObj = False
            End If
        End If 'Si premier caract�re = "["
    End If 'Si on imprime la page de s�lection
NextLigne:
Next 'Pour toutes les lignes de la page

'-> Tester si le bas de page est d�pass�
If Sortie.ScaleY(PositionY, 3, 7) > aSpool.Maquette.Hauteur * Zoom Then
    Sortie.PaintPicture frmLib.pibPage.Picture, Sortie.ScaleX(0.5, 7, 3), Sortie.ScaleY(aSpool.Maquette.Hauteur, 7, 3) - Sortie.ScaleY(frmLib.pibPage.Height, 1, 3) - Sortie.ScaleY(1, 7, 3)
End If 'Si on a d�pass� le bas de page

'-> Imprimer la page de s�lection
If pageToPrint = 0 Then
    '-> imprimer la section
    PrintObjRtf 0, 0, aSection, "", 0, 0, aSpool
End If

'-> R�afficher la page si on est en mode Visu
If TypeOf Sortie Is PictureBox Then
    '-> D�bloquer la feuille
    aSpool.frmDisplay.Enabled = True
End If

'-> Gestion de la temporisation
If Mode = 1 Then
    MDIMain.StatusBar1.Refresh
    '-> Bloquer l'interface
    MDIMain.Enabled = True
    Screen.MousePointer = 0
End If

LoadJoinFile uZipInfo2, aSpool.FileName

Exit Sub
       
GestError:
    MsgBox Err.Number & " " & Err.Description
    '-> Gestion de la temporisation
    If Mode = 1 Then
        MDIMain.StatusBar1.Refresh
        '-> Bloquer l'interface
        MDIMain.Enabled = True
        Screen.MousePointer = 0
    End If

    '-> Setting d'une erreur
    Call GestError(ErrorCode, CStr(pageToPrint), aSpool)
    
    

End Sub


Private Function PrintBlock(ByRef NomSousObjet As String, ByRef Param As String, _
                        ByRef DataFields As String, ByVal PosX As Long, _
                        ByRef PosY As Long, ByRef aTb As Tableau, ByVal Ligne As Integer, ByRef aSpool As Spool) As Long

'---> Cette Proc�dure imprime un block de ligne

Dim aBlock As Block
Dim Champ As String
Dim aField() As String
Dim i As Long
Dim aCell As Cellule
Dim HauteurLigPix As Long
Dim ListeField() As String
Dim Tempo

On Error GoTo GestBlock

'-> Pointer sur le block de tableau � �diter
Set aBlock = aTb.Blocks("BL-" & UCase$(NomSousObjet))

'-> Initaliser le block
InitBlock aBlock, aSpool

'-> Calculer la modification des positions X et Y des blocks de ligne. _
Les coordonn�es de chaque cellule �tant calcul�e sur un X et Y = 0 de base

Select Case aBlock.AlignementLeft
    Case 2 '-> Marge gauche
        PosX = Sortie.ScaleX(aSpool.Maquette.MargeLeft * Zoom, 7, 3)
    Case 3 '-> Centr�
        PosX = Sortie.ScaleX((aSpool.Maquette.Largeur * Zoom - aBlock.Largeur * Zoom) / 2, 7, 3)
    Case 4 '-> Marge droite
        PosX = Sortie.ScaleX(aSpool.Maquette.Largeur * Zoom - aBlock.Largeur * Zoom - aSpool.Maquette.MargeLeft * Zoom, 7, 3)
    Case 5 '-> Sp�cifi�
        PosX = Sortie.ScaleX(aBlock.Left * Zoom, 7, 3)
End Select

'-> Tenir compte de la marge interne
PosX = (PosX - MargeX)

If aBlock.AlignementTop = 5 And Ligne <> 1 Then
Else
    Select Case aBlock.AlignementTop
        Case 1 '-> Libre
            '-> RAS le pointeur est bien positionn�
        Case 2 '-> Marge haut
            PosY = Sortie.ScaleY(aSpool.Maquette.MargeTop * Zoom, 7, 3)
        Case 3 '-> Centr�
            PosY = Sortie.ScaleY((aSpool.Maquette.Hauteur * Zoom - aBlock.Hauteur * Zoom) / 2, 7, 3)
        Case 4 '-> Marge Bas
            PosY = Sortie.ScaleY(aSpool.Maquette.Hauteur * Zoom - aBlock.Hauteur * Zoom, 7, 3)
        Case 5 '-> Sp�cifi�
            PosY = Sortie.ScaleY(aBlock.Top * Zoom, 7, 3)
    End Select

    '-> Modifier l'alignement
    If aBlock.AlignementTop <> 1 Then PosY = PosY - MargeY

End If

'-> Impression du block de ligne
For i = 1 To aBlock.NbCol
    '-> Pointer sur la cellule � dessiner
    Set aCell = aBlock.Cellules("L" & Ligne & "C" & i)
    '-> Remplacer les champs par leur valeur
    aCell.ReplaceField DataFields
    '-> Imprimer la cellule
    If Not DrawCell(aCell, PosX, PosY, aSpool, aTb.Nom, NomSousObjet) Then GoTo GestBlock
    '-> R�cup�rer la hauteur de la ligne
    HauteurLigPix = (aCell.Y2 - aCell.y1) * Zoom
Next

'-> forcer le rafraichissement
If TypeOf Sortie Is PictureBox Then Sortie.Refresh

If Ligne = aBlock.NbLigne Then PosY = PosY + 1 * Zoom
PrintBlock = PosY + HauteurLigPix

Exit Function

GestBlock:
    
    Call GestError(40, aTb.Nom & "|" & NomSousObjet, aSpool)
    PrintBlock = -9999
    
End Function

Private Function IsGoodFont(ByVal MyFont As String) As String

'---> Cette fonction d�termine si une font est valide
On Error GoTo GestError

frmLib.FontName = MyFont
IsGoodFont = MyFont

Exit Function

GestError:

    IsGoodFont = "Arial"
    

End Function


Public Function DrawCell(ByRef aCell As Cellule, ByVal PosX As Long, ByVal PosY As Long, ByRef aSpool As Spool, ByRef NomTb As String, Optional NomBlock As String) As Boolean

'---> Fonction qui dessine une cellule

Dim hdlPen As Long
Dim hdlBrush As Long
Dim hdlBordure As Long
Dim hdlFont As Long
Dim OldPen As Long
Dim oldBrush As Long
Dim oldFont As Long
Dim aPoint As POINTAPI
Dim aRect As RECT
Dim Res As Long
Dim aFt As FormatedCell
Dim xTemp
Dim PrintSigne As Boolean
Dim Couleur As Long
Dim aLbl As label
Dim aTb As Tableau
Dim aBlock As Block
Dim aCellContenuTemp As String
Dim FindTextTemp As String
Dim i As Integer

On Error GoTo GestErr

Dim x1 As Long, X2 As Long, y1 As Long, Y2 As Long

x1 = aCell.x1 * Zoom + PosX
X2 = aCell.X2 * Zoom + PosX
y1 = PosY
Y2 = (aCell.Y2 * Zoom) + PosY - (aCell.y1 * Zoom)

'-> Cr�ation des objets GDI pour dessin des cellules
hdlBrush = CreateSolidBrush(aCell.BackColor)
hdlPen = CreatePen(PS_NULL, 1, QBColor(15))
hdlBordure = CreatePen(PS_SOLID, 1, &HE0E0E0)
'-> S�lection du pinceau dans le Contexte
oldBrush = SelectObject(Sortie.hDC, hdlBrush)

'-> Dessin de la cellule
OldPen = SelectObject(Sortie.hDC, hdlPen)
If Not aCell.FondTransparent Then Res = Rectangle(Sortie.hDC, x1, y1, X2, Y2)

'-> Setting du Rect de la cellule
aRect.Left = x1
aRect.Top = y1
aRect.Right = X2 - 1
aRect.Bottom = Y2 - 1


Dim pDrawText As Long
Dim Rect2 As RECT
Dim DifL As Long
Dim DifH As Long
Dim IsCellAjust As Boolean
Dim hdlRgn As Long
Dim AlignBase(1 To 2, 3 To 6) As Long

'-> Appliquer les options de font
Sortie.FontName = IsGoodFont(aCell.FontName)
Sortie.FontSize = aCell.FontSize * Zoom
Sortie.FontBold = aCell.FontBold
Sortie.FontItalic = aCell.FontItalic
Sortie.FontUnderline = aCell.FontUnderline
Sortie.ForeColor = aCell.FontColor

'***********************************
'* Dessin du contenu de la cellule *
'***********************************
    
'-> R�cup�rer l'option de d'alignement interne
IsCellAjust = aCell.AutoAjust
    
'-> Faire une copie de Rect
Rect2.Left = aRect.Left
Rect2.Right = aRect.Right
Rect2.Top = aRect.Top
Rect2.Bottom = aRect.Bottom
    
'-> Dans un premier temps, calculer la taille necessaire pour afficher si ajustement automatique
If IsCellAjust Then _
    DrawText Sortie.hDC, aCell.ContenuCell, Len(aCell.ContenuCell), Rect2, DT_CALCRECT Or DT_WORDBREAK
   
'-> R�cup�ration des diff�rences de largeur et de hauteur
DifL = (aRect.Right - aRect.Left) - (Rect2.Right - Rect2.Left)
DifH = (aRect.Bottom - aRect.Top) - (Rect2.Bottom - Rect2.Top)
    
'-> Calcul des alignements de base
AlignBase(1, 3) = Rect2.Left + DifL / 2
AlignBase(2, 3) = Rect2.Right + DifL / 2

AlignBase(1, 4) = Rect2.Left + DifL
AlignBase(2, 4) = Rect2.Right + DifL

AlignBase(1, 5) = Rect2.Top + DifH / 2
AlignBase(2, 5) = Rect2.Bottom + DifH / 2

AlignBase(1, 6) = Rect2.Top + DifH
AlignBase(2, 6) = Rect2.Bottom + DifH
        
'-> Alignement interne dans le rectangle
Select Case aCell.CellAlign
    
    Case 1
        If IsCellAjust Then
            pDrawText = DT_LEFT
            '-> Pas necessaire de modifier les alignements
        Else
            pDrawText = DT_LEFT Or DT_TOP
        End If
        
    Case 2
        If IsCellAjust Then
            pDrawText = DT_CENTER
            Rect2.Left = AlignBase(1, 3)
            Rect2.Right = AlignBase(2, 3)
        Else
            pDrawText = DT_CENTER Or DT_TOP
        End If
    
    Case 3
        If IsCellAjust Then
            pDrawText = DT_RIGHT
            Rect2.Left = AlignBase(1, 4)
            Rect2.Right = AlignBase(2, 4)
        Else
            pDrawText = DT_RIGHT Or DT_TOP
        End If
                
    Case 4
        If IsCellAjust Then
            pDrawText = DT_LEFT
            Rect2.Top = AlignBase(1, 5)
            Rect2.Bottom = AlignBase(2, 5)
        Else
            pDrawText = DT_VCENTER Or DT_LEFT
        End If
    
    Case 5
        If IsCellAjust Then
            pDrawText = DT_CENTER
            Rect2.Top = AlignBase(1, 5)
            Rect2.Bottom = AlignBase(2, 5)
            Rect2.Left = AlignBase(1, 3)
            Rect2.Right = AlignBase(2, 3)
        Else
            pDrawText = DT_VCENTER Or DT_CENTER
        End If
    
    Case 6
        If IsCellAjust Then
            pDrawText = DT_RIGHT
            Rect2.Top = AlignBase(1, 5)
            Rect2.Bottom = AlignBase(2, 5)
            Rect2.Left = AlignBase(1, 4)
            Rect2.Right = AlignBase(2, 4)
        Else
            pDrawText = DT_VCENTER Or DT_RIGHT
        End If
    
    Case 7
        If IsCellAjust Then
            pDrawText = DT_LEFT
            Rect2.Top = AlignBase(1, 6)
            Rect2.Bottom = AlignBase(2, 6)
        Else
            pDrawText = DT_BOTTOM Or DT_LEFT
        End If
    
    Case 8
        If IsCellAjust Then
            pDrawText = DT_CENTER
            Rect2.Top = AlignBase(1, 6)
            Rect2.Bottom = AlignBase(2, 6)
            Rect2.Left = AlignBase(1, 3)
            Rect2.Right = AlignBase(2, 3)
        Else
            pDrawText = DT_BOTTOM Or DT_CENTER
        End If
    
    Case 9
        If IsCellAjust Then
            pDrawText = DT_RIGHT
            Rect2.Top = AlignBase(1, 6)
            Rect2.Bottom = AlignBase(2, 6)
            Rect2.Left = AlignBase(1, 4)
            Rect2.Right = AlignBase(2, 4)
        Else
            pDrawText = DT_BOTTOM Or DT_RIGHT
        End If
    
End Select
    
'-> on verifie si l'impression ne deborde pas
If Rect2.Right > Sortie.ScaleWidth And Not DebordementTop Then
    If AllowDebordement Then DebordementTop = True
End If
    
If IsCellAjust Then
    pDrawText = pDrawText Or DT_WORDBREAK
Else
    pDrawText = pDrawText Or DT_SINGLELINE
End If
    

'-> Gestion des formats num�rics
Select Case aCell.TypeValeur
    Case 0 '-> RAS
        PrintSigne = False
    Case 1 '-> Caract�re
        '-> Faire un susbstring
        aCell.ContenuCell = Mid$(aCell.ContenuCell, 1, CInt(aCell.Msk))
        PrintSigne = False
    Case 2 '-> Numeric
'-> V�rifier s'il y a quelquechose � formater
        If Trim(aCell.ContenuCell) = "" Then
        Else
             aFt = FormatNumTP(Trim(aCell.ContenuCell), aCell.Msk, NomBlock, "L" & aCell.Ligne & "C" & aCell.Colonne)
            '-> Rajouter Trois blancs au contenu de la cellule
            aFt.strFormatted = aFt.strFormatted & "   "
            If aFt.Ok Then
                '-> Mettre e rouge si necessaire
                If aFt.Value < 0 And aFt.idNegatif > 1 Then Sortie.ForeColor = QBColor(12)
                '-> affecter le contenu de la cellule
                aCell.ContenuCell = aFt.strFormatted
                '-> Si c'est un 0 mettre � blanc si pas imprimer
                If CDbl(aCell.ContenuCell) = 0 And Not (aCell.PrintZero) Then
                    aCell.ContenuCell = ""
                Else
                    If aFt.Value < 0 Then
                        If aFt.idNegatif = 0 Or aFt.idNegatif = 3 Then
                            aCell.ContenuCell = "- " & aCell.ContenuCell
                            PrintSigne = False
                        ElseIf aFt.idNegatif = 1 Or aFt.idNegatif = 4 Then
                            PrintSigne = True
                        End If
                    End If
                End If
            Else
                '-> Ne pas imprimer  de signe
                PrintSigne = False
            End If
        End If
End Select


'-> Faire de rect la zone de clipping en cours
hdlRgn = CreateRectRgn&(aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
SelectClipRgn Sortie.hDC, hdlRgn

'-> Postionnement du rectangle de dessin
DrawText Sortie.hDC, aCell.ContenuCell, Len(aCell.ContenuCell), Rect2, pDrawText Or DT_NOPREFIX

If PrintSigne Then DrawText Sortie.hDC, "- ", 2, Rect2, pDrawText Or DT_NOPREFIX

'-> Redessiner le texte de la cellule si on recherche du texte
If FindTop Then
    '-> On g�re le cas du num�rique avec les s�parateur des milliers
    aCellContenuTemp = Replace(aCell.ContenuCell, SepMil, "")
    '-> On fait la recherche
    If InStr(1, aCellContenuTemp, FindText, vbTextCompare) <> 0 Then
        'on compte le nombre d'occurences dans la chaine
        i = NumEntries(UCase(aCellContenuTemp), UCase(FindText)) - 1
        FindPos2 = FindPos2 + i
        '-> on regarde si c'est bien celui que l'on veut dessiner
        If FindPage = aSpool.CurrentPage And FindPos < FindPos2 Or FindPage < aSpool.CurrentPage Then
            FindPos2 = FindPos2 - i + 1
            'a cause du s�parateur millier on redefini eventuellement le texte recherch�
            FindTextTemp = GetNewFindText(FindText, aCell.ContenuCell, FindPos2 - FindPos)
            DrawFindText FindTextTemp, pDrawText Or DT_NOPREFIX, Rect2.Left, Rect2.Right, Rect2.Top, Rect2.Bottom, aCell
            FindPos = FindPos + 1
            FindPage = aSpool.CurrentPage
            FindTop = False
            '-> assigner une valeur au scroll
            FindScrollH = aRect.Left
            FindScrollV = aRect.Top
        End If
    End If
End If

'-> Faire de la fen�tre entiere la zone de clipping
SelectClipRgn Sortie.hDC, 0

'-> Dessin des bordures de la cellule
SelectObject Sortie.hDC, hdlBordure

'-> Dessin de la bordure Bas
If aCell.BordureBas Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureBas Then
Else
    DrawBordure x1 - 1, Y2 - 1, X2, Y2 - 1, Couleur
    'DrawBordure X1 - 1, Y2 - 1, X2 - 1, Y2 - 1, Couleur
End If

'-> Dessin de la bordure haut
If aCell.BordureHaut Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureHaut Then
Else
    DrawBordure x1 - 1, y1 - 1, X2, y1 - 1, Couleur
End If


'-> Dessin de la bordure gauche
If aCell.BordureGauche Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureGauche Then
Else
    DrawBordure x1 - 1, y1 - 1, x1 - 1, Y2, Couleur
End If

'-> Dessin de la bordure Droite
If aCell.BordureDroite Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureDroite Then
Else
    DrawBordure X2 - 1, y1 - 1, X2 - 1, Y2, Couleur
End If

'-> Gestion des angles des bordures
If aCell.BordureGauche Or aCell.BordureHaut Then
    '-> dessiner le coins sup�rieur gauche
    Couleur = 0
    DrawBordure x1 - 1, y1 - 1, x1 - 1, y1, Couleur
End If

If aCell.BordureDroite Or aCell.BordureHaut Then
    '-> Dessiner le coin sup�rieur droit
    Couleur = 0
    DrawBordure X2 - 1, y1 - 1, X2 - 1, y1, Couleur
End If

If aCell.BordureBas Or aCell.BordureGauche Then
    '-> Dessin du coin inf�rieur gauche
    Couleur = 0
    DrawBordure x1 - 1, Y2 - 1, x1, Y2 - 1, Couleur
End If

If aCell.BordureDroite Or aCell.BordureBas Then
    '-> Dessin du coin inf�rieur droit
    Couleur = 0
    DrawBordure X2 - 1, Y2 - 1, X2 - 1, Y2, Couleur
End If

'-> Supprimer la r�gion
DeleteObject hdlRgn
               
'-> Res�lectionner l'ancien stylo
If OldPen <> 0 Then
    SelectObject Sortie.hDC, OldPen
    DeleteObject hdlPen
End If

'-> Res�lectionner l'ancien pinceau
If oldBrush <> 0 Then
    SelectObject Sortie.hDC, oldBrush
    DeleteObject hdlBrush
End If

'-> Supprimer le stylo de dessin des bordures
DeleteObject hdlBordure

'-> Positionnement du label pour l'acces au d�tail
If aCell.UseAccesDet Then
    '-> V�rifier que l'on soit en visu
    If TypeOf Sortie Is PictureBox Then
        '-> cr�er un nouveau Label
        Load aSpool.frmDisplay.lblAccesDet(aSpool.frmDisplay.lblAccesDet.Count + 1)
        '-> Positionner l'objet
        aSpool.frmDisplay.lblAccesDet(aSpool.frmDisplay.lblAccesDet.Count).Left = x1
        aSpool.frmDisplay.lblAccesDet(aSpool.frmDisplay.lblAccesDet.Count).Width = X2 - x1
        aSpool.frmDisplay.lblAccesDet(aSpool.frmDisplay.lblAccesDet.Count).Top = y1
        aSpool.frmDisplay.lblAccesDet(aSpool.frmDisplay.lblAccesDet.Count).Height = Y2 - y1
        '-> Acc�der au tableau de premier niveau
        Set aTb = aSpool.Maquette.Tableaux(UCase$(NomTb))
        '-> Pointer sur le block de tableau � �diter
        Set aBlock = aTb.Blocks("BL-" & UCase$(NomBlock))
        '-> Positionner la cl� d'acc�s au d�tail
        aSpool.frmDisplay.lblAccesDet(aSpool.frmDisplay.lblAccesDet.Count).Tag = aCell.KeyAccesDet & Chr(0) & Entry(2, Entry(1, aBlock.KeyAccesDet, "�"), "|")
        '-> Le rendre visible
        aSpool.frmDisplay.lblAccesDet(aSpool.frmDisplay.lblAccesDet.Count).Visible = True
    End If 'Si on est en vosu ou en impression
End If 'Si on a acc�s au d�tail

'-> Renvoyer une valeur de succ�s
DrawCell = True

Exit Function

GestErr:

    Call GestError(41, NomTb & "|" & NomBlock & "|" & "L" & aCell.Ligne & "C" & aCell.Colonne, aSpool)
    

End Function

Private Sub DrawBordure(ByVal Move1, Move2, Line1, Line2, Couleur)

Dim hdlBordure As Long
Dim aPoint As POINTAPI
Dim Res As Long
Dim Old As Long

hdlBordure = CreatePen(PS_SOLID, 1 * Zoom, Couleur)
Old = SelectObject(Sortie.hDC, hdlBordure)
Res = MoveToEx(Sortie.hDC, Move1, Move2, aPoint)
LineTo Sortie.hDC, Line1, Line2
SelectObject Sortie.hDC, Old
DeleteObject hdlBordure


End Sub


Public Sub AnalyseObj(ByVal Ligne As String, TypeObjet As String, _
                       ByRef NomObjet As String, ByRef TypeSousObjet As String, _
                       ByRef NomSousObjet As String, ByRef Param As String, _
                       ByRef DataFields As String)

Dim Param1 As String
Dim Param2 As String
Dim Param3 As String
Dim Param4 As String

'---> Procedure qui r�cupre les d�finitions des objets dans une ligne
On Error Resume Next

If Ligne = "" Then Exit Sub

Param1 = Entry(1, Ligne, "]")
Param2 = Entry(2, Ligne, "]")
'Param3 = Entry(2, Ligne, "{")
'-> pb sur le caractere { present dans les valeurs
Param3 = Mid(Ligne, InStr(1, Ligne, "{") + 1)

Param1 = Mid$(Param1, 2, Len(Param1) - 2)
'-> R�cup�ration du champ de donn�es
If InStr(1, Param3, "}") <> 0 Then
    DataFields = Mid$(Param3, 1, Len(Param3) - 1)
Else
    DataFields = Param3 + " "
End If
'-> R�cup�ration des param�tres d'application
Param = Mid$(Param2, 2, Len(Param2) - 1)
'-> R�cup�ration du nom des objets
Param2 = Entry(1, Param1, "(")
Param3 = Entry(2, Param1, "(")

'-> Objet de premier niveau
TypeObjet = Entry(1, Param2, "-")
NomObjet = Entry(2, Param2, "-")

'-> Objet de second niveau
TypeSousObjet = Entry(1, Param3, "-")
NomSousObjet = Entry(2, Param3, "-")


End Sub
Public Function PrintSection(ByVal SectionName As String, ByRef Param As String, _
                         ByRef DataFields As String, ByRef FirstObj As Boolean, _
                         ByRef PositionX As Long, ByRef PositionY As Long, ByRef aSpool As Spool) As Boolean

'---> Cette proc�dure est charg�e d'imprimer une section

Dim aSection As Section
Dim i As Integer
Dim NomObjet As String
Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim DebutRangX As Long
Dim DebutRangY As Long

On Error GoTo GestError

Trace "PrintSection " & SectionName & " " & DataFields

'-> Pointer sur la section � imprimer
Set aSection = aSpool.Maquette.Sections(UCase$(SectionName))

'-> on g�re ici les libell�s variables
For i = 0 To NumEntries(aSection.LibelVariable, "�") - 1
    If InStr(1, DataFields, Entry(i * 2 + 1, aSection.LibelVariable, "�")) <> 0 Then
        If Entry(curPage, Entry(i * 2 + 2, aSection.LibelVariable, "�"), "�") <> "" Then
            DataFields = Mid(DataFields, 1, InStr(1, DataFields, Entry(i * 2 + 1, aSection.LibelVariable, "�")) + 4) + Replace(Entry(curPage, Entry(i * 2 + 2, aSection.LibelVariable, "�"), "�"), Chr(0), "") + Mid(Mid(DataFields, InStr(1, DataFields, Entry(i * 2 + 1, aSection.LibelVariable, "�")) + 4, 3200), InStr(1, Mid(DataFields, InStr(1, DataFields, Entry(i * 2 + 1, aSection.LibelVariable, "�")) + 4, 3200), "^"), 3200)
        End If
    End If
Next

'-> Imprimer la section
If Not PrintObjRtf(PositionX, PositionY, aSection, DataFields, DebutRangX, DebutRangY, aSpool) Then GoTo GestError

'-> Impression des objets associ�s dans l'ordre d'affichage
For i = 1 To aSection.nEntries - 1
    NomObjet = aSection.GetOrdreAffichage(i)
    If UCase$(Entry(1, NomObjet, "-")) = "CDR" Then
        '-> Pointer sur le cadre
        Set aCadre = aSection.Cadres(UCase$(Entry(2, NomObjet, "-")))
        '-> Imprimer l'objet
        Trace "Printsection RTF valeurs " & NomObjet & DataFields
        If Not PrintObjRtf(PositionX, PositionY, aCadre, DataFields, DebutRangX, DebutRangY, aSpool, aCadre.MargeInterne) Then GoTo GestError
        '-> Liberer le pointeur
        Set aCadre = Nothing
    ElseIf UCase$(Entry(1, NomObjet, "-")) = "BMP" Then
        Set aBmp = aSection.Bmps(UCase$(Entry(2, NomObjet, "-")))
        If Not PrintBmp(aBmp, DebutRangX, DebutRangY, DataFields, aSpool) Then GoTo GestError
    End If
Next

'-> Positionner le pointeur de position apr�s l'�dition de la section
PositionY = Sortie.ScaleY(aSection.Hauteur * Zoom, 7, 3) + PositionY 'Sortie.ScaleX(DebutRangY, 1, 3)

'-> Liberer les pointeurs
Set aSection = Nothing

PrintSection = True

Exit Function

GestError:

    Call GestError(36, aSection.Nom, aSpool)
    PrintSection = False

End Function

Private Function PrintBmp(aBmp As ImageObj, DebutRangX As Long, DebutRangY As Long, ByVal DataFields As String, ByRef aSpool As Spool) As Boolean

'---> Impression d'un BMP

Dim PosX As Long
Dim PosY As Long
Dim aPic As PictureBox
Dim Champ As String
Dim NomFichier As String
Dim aRect As RECT
Dim hdlPen As Long
Dim OldPen As Long
Dim hdlBrush As Long
Dim oldBrush As Long
Dim i As Integer

On Error GoTo GestErr


'-> Il faut modifier la position X et Y du cadre
PosX = Sortie.ScaleX(DebutRangX, 1, 3) + Sortie.ScaleX(aBmp.Left * Zoom, 7, 3)
PosY = Sortie.ScaleY(DebutRangY, 1, 3) + Sortie.ScaleY(aBmp.Top * Zoom, 7, 3)

'-> Imprimer le bmp
Set aPic = frmLib.PicObj(aBmp.IdPic)
If aBmp.isVariable Then
    '-> Venir charg� le BMP associ�
    Trace "Image variable"
    '-> R�cup�ration du nombre de champs
    For i = 2 To NumEntries(DataFields, "^")
        Champ = Entry(i, DataFields, "^")
        If UCase$(aBmp.Fichier) = "^" & UCase$(Mid$(Champ, 1, 4)) Then
            '-> R�cup�ration du nom du fichier
            NomFichier = RTrim(Mid$(Champ, 5, Len(Champ) - 1))
            '-> Test de l'utilisation de l'association
            If aBmp.UseAssociation Then
                '-> R�cup�rer le nom du fichier dans le fichier ini
                NomFichier = GetPictureAssociation(NomFichier)
            End If
            Exit For
        End If
    Next
    If Dir$(NomFichier) = "" Or Trim(NomFichier) = "" Then
        '-> en mode internet on essaye de telecharger le fichier
        If TurboGraphWebFile <> "" Then
            If TelechargeFile(TurboGraphWebFile & "/deallogo/" & GetFileName(NomFichier), App.Path & "\Images\" & GetFileName(NomFichier)) Then
                NomFichier = App.Path & "\Images\" & GetFileName(NomFichier)
            End If
        End If
    End If
    
    '-> V�rifier que le fichier existe
    Trace "Image : " + NomFichier
    If Dir$(NomFichier) = "" Or Trim(NomFichier) = "" Then
    Else
        If Zoom <> 1 Then
            aPic.AutoSize = True
            aPic.AutoRedraw = True
            aPic.Picture = LoadPicture(NomFichier)
            aPic.ScaleMode = vbPixels
            If Zoom < 1 Then
                '-> on redimensionne l'image
                StretchBlt aPic.hDC, 0, 0, aPic.Width * Zoom, aPic.Height * Zoom, aPic.hDC, 0, 0, aPic.Width, aPic.Height, ScrCopy
                '-> on ne conserve que la partie interessante
                StretchBlt Sortie.hDC, PosX, PosY, aPic.ScaleWidth * Zoom, aPic.ScaleHeight * Zoom, aPic.hDC, 0, 0, aPic.ScaleWidth * Zoom, aPic.ScaleHeight * Zoom, vbSrcCopy
            Else
                '-> on redimensionne l'image
                aPic.Width = aPic.Width * Zoom
                aPic.Height = aPic.Height * Zoom
                StretchBlt aPic.hDC, 0, 0, aPic.Width * Zoom, aPic.Height * Zoom, aPic.hDC, 0, 0, aPic.Width, aPic.Height, ScrCopy
                '-> on ne conserve que la partie interessante
                StretchBlt Sortie.hDC, PosX, PosY, aPic.ScaleWidth, aPic.ScaleHeight * Zoom, aPic.hDC, 0, 0, aPic.ScaleWidth, aPic.ScaleHeight * Zoom, vbSrcCopy
            End If
            Sortie.Refresh
        Else
            aPic.Picture = LoadPicture(NomFichier)
            Sortie.PaintPicture aPic.Picture, PosX, PosY
        End If
    End If
Else
    Trace "Image non variable : " + aBmp.Fichier
    If Trim(aBmp.Fichier) = "" Then
    Else
        If Dir$(aBmp.Fichier, vbNormal) = "" Then
        Else
            If Zoom <> 1 Then
                aPic.AutoSize = True
                aPic.AutoRedraw = True
                aPic.ScaleMode = vbPixels
                aPic.Picture = LoadPicture(aBmp.Fichier)
                If Zoom < 1 Then
                    '-> on redimensionne l'image
                    StretchBlt aPic.hDC, 0, 0, aPic.Width * Zoom, aPic.Height * Zoom, aPic.hDC, 0, 0, aPic.Width, aPic.Height, ScrCopy
                    '-> on ne conserve que la partie interessante
                    StretchBlt Sortie.hDC, PosX, PosY, aPic.ScaleWidth * Zoom, aPic.ScaleHeight * Zoom, aPic.hDC, 0, 0, aPic.ScaleWidth * Zoom, aPic.ScaleHeight * Zoom, vbSrcCopy
                Else
                    '-> on redimensionne l'image
                    aPic.Width = aPic.Width * Zoom
                    aPic.Height = aPic.Height * Zoom
                    StretchBlt aPic.hDC, 0, 0, aPic.Width * Zoom, aPic.Height * Zoom, aPic.hDC, 0, 0, aPic.Width, aPic.Height, ScrCopy
                    '-> on ne conserve que la partie interessante
                    StretchBlt Sortie.hDC, PosX, PosY, aPic.ScaleWidth, aPic.ScaleHeight * Zoom, aPic.hDC, 0, 0, aPic.ScaleWidth, aPic.ScaleHeight * Zoom, vbSrcCopy
                End If
                Sortie.Refresh
            Else
                Sortie.PaintPicture aPic.Picture, PosX, PosY
            End If
        End If
    End If
End If

'-> Dessiner la bordure si necessaire
If aBmp.Contour Then
    hdlBrush = CreateSolidBrush(0)
    aRect.Left = PosX
    aRect.Top = PosY
    aRect.Right = aRect.Left + Sortie.ScaleX(aPic.Width, 1, 3)
    aRect.Bottom = aRect.Top + Sortie.ScaleY(aPic.Height, 1, 3)
    FrameRect Sortie.hDC, aRect, hdlBrush
    DeleteObject hdlBrush
End If

Set aPic = Nothing

'-> Renvoyer une valeur de succ�s
PrintBmp = True

Exit Function

GestErr:

    Call GestError(39, aBmp.SectionName & "|" & aBmp.Nom, aSpool)
    PrintBmp = False
    

End Function

Private Function PrintObjRtf(PositionX As Long, PositionY As Long, _
                        aObject As Object, ByVal DataFields As String, _
                        DebutRangX As Long, DebutRangY As Long, ByRef aSpool As Spool, Optional MargeInterne As Single) As Boolean


'---> Cette fonction imprime un cadre ou une section ( Code RTF + Bordures )

Dim aField() As String
Dim aRtf As RichTextBox
Dim PosX As Long, PosY As Long
Dim i As Integer
Dim Champ As String
Dim RTFValue As String
Dim fr As FORMATRANGE
Dim lTextOut As Long, lTextAmt As Long, Res As Long
Dim aPoint As POINTAPI
Dim aRect As RECT
Dim hdlPen As Long
Dim OldPen As Long
Dim hdlBrush As Long
Dim oldBrush As Long
Dim NbChamp As Integer
Dim IsCadre As Boolean
Dim IsBordure As Boolean
Dim iniformat As FORMAT

On Error GoTo GestErr


'-> Mettre l'�diteur RTF aux bonnes dimensions
Set aRtf = frmLib.Rtf(aObject.IdRTf)

aRtf.Width = frmLib.ScaleX(aObject.Largeur * Zoom, 7, 1)
aRtf.Height = frmLib.ScaleY(aObject.Hauteur * Zoom, 7, 1)

'-> R�cup�ration du code RTF
RTFValue = aRtf.TextRTF

If Zoom <> 1 Then aRtf.TextRTF = ZoomRTF(aRtf.TextRTF)

'-> Cr�er une base de champs
If Trim(DataFields) = "" Then
Else
    '-> R�cup�ration du nombre de champs
    NbChamp = NumEntries(DataFields, "^")
    For i = 2 To NbChamp
        Champ = Entry(i, DataFields, "^")
        ReDim Preserve aField(1 To 2, i - 1)
        aField(1, i - 1) = "^" & Mid$(Champ, 1, 4)
        aField(2, i - 1) = RTrim(Mid$(Champ, 5, Len(Champ) - 1))
    Next
    '-> Faire un remplacement des champs par leur valeur
    For i = 1 To NbChamp - 1
        aRtf.TextRTF = Replace(aRtf.TextRTF, aField(1, i), aField(2, i))
    Next
End If

'-> Calcul des positions d'impression de l'objet selon le type d'objet � imprimer
If TypeOf aObject Is Section Then
    Select Case aObject.AlignementLeft
        Case 2 'Marge gauche
            PosX = Sortie.ScaleX(aSpool.Maquette.MargeLeft * Zoom, 7, 1)
        Case 4 'Centr�
            PosX = Sortie.ScaleX((aSpool.Maquette.Largeur * Zoom - aObject.Largeur * Zoom) / 2, 7, 1)
        Case 3 'Marge Droite
            PosX = Sortie.ScaleX(aSpool.Maquette.Largeur * Zoom - aObject.Largeur * Zoom - aSpool.Maquette.MargeLeft * Zoom, 7, 1)
        Case 5 'Sp�cifi�
            PosX = Sortie.ScaleX(aObject.Left * Zoom, 7, 1)
    End Select
    Select Case aObject.AlignementTop
        Case 1 'Libre
            PosY = Sortie.ScaleY(PositionY, 3, 1)
        Case 2 'Marge haut
            PosY = Sortie.ScaleY(aSpool.Maquette.MargeTop * Zoom, 7, 1)
        Case 4 'Centr�
            PosY = Sortie.ScaleY((aSpool.Maquette.Hauteur * Zoom - aObject.Hauteur * Zoom) / 2, 7, 1)
        Case 3 'Marge bas
            PosY = Sortie.ScaleY(aSpool.Maquette.Hauteur * Zoom - aObject.Hauteur * Zoom - aSpool.Maquette.MargeTop * Zoom, 7, 1)
        Case 5 'Sp�cifi�
            PosY = Sortie.ScaleY(aObject.Top * Zoom, 7, 1)
    End Select

    '-> Tenir compte des variations des marges internes
    PosX = PosX - Sortie.ScaleX(MargeX, 3, 1)
    If aObject.AlignementTop <> 1 Then PosY = PosY - Sortie.ScaleY(MargeY, 3, 1)
    
    '-> Setting du rect de dessin
    aRect.Left = Sortie.ScaleX(PosX, 1, 3)
    aRect.Top = Sortie.ScaleY(PosY, 1, 3)
    aRect.Right = Sortie.ScaleX(aObject.Largeur * Zoom, 7, 3) + Sortie.ScaleX(PosX, 1, 3)
    aRect.Bottom = Sortie.ScaleY(aObject.Hauteur * Zoom, 7, 3) + Sortie.ScaleY(PosY, 1, 3)

    '-> Sauvagarder la position de d�but du rang
    DebutRangX = PosX
    DebutRangY = PosY
    
ElseIf TypeOf aObject Is Cadre Then
    '-> Il faut modifier la position X et Y du cadre
    PosX = DebutRangX + Sortie.ScaleX(aObject.Left * Zoom, 7, 1)
    PosY = DebutRangY + Sortie.ScaleY(aObject.Top * Zoom, 7, 1)
    '-> Setting du rect de dessin
    aRect.Left = Sortie.ScaleX(PosX, 1, 3)
    aRect.Top = Sortie.ScaleY(PosY, 1, 3)
    aRect.Right = Sortie.ScaleX(aObject.Largeur * Zoom, 7, 3) + Sortie.ScaleX(PosX, 1, 3)
    aRect.Bottom = Sortie.ScaleY(aObject.Hauteur * Zoom, 7, 3) + Sortie.ScaleY(PosY, 1, 3)
    '-> Indiquer que c'est un cadre
    IsCadre = True
End If


'-> Imprimer le fond de l'objet
hdlBrush = CreateSolidBrush(aObject.BackColor)
hdlPen = CreatePen(PS_NULL, 1, QBColor(15))

'-> S�lection du pinceau dans le Contexte
oldBrush = SelectObject(Sortie.hDC, hdlBrush)
OldPen = SelectObject(Sortie.hDC, hdlPen)

'-> Dessin de l'objet
If IsCadre Then
    If Not aObject.IsRoundRect Then
        Res = Rectangle(Sortie.hDC, aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
    Else
        '-> Imprimer les bordures
        PrintBordure aObject, aRect
        IsBordure = True
    End If
        
Else
    Res = Rectangle(Sortie.hDC, aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
End If

'-> Redessiner le texte de la cellule si on recherche du texte
If FindTop Then
    If InStr(1, aRtf.Text, FindText, vbTextCompare) <> 0 Then
        'on compte le nombre d'occurence dans la chaine
        i = NumEntries(UCase(aRtf.Text), UCase(FindText)) - 1
        FindPos2 = FindPos2 + i
        '-> on regarde si c'est bien celui que l'on veut dessiner
        If FindPage = aSpool.CurrentPage And FindPos < FindPos2 Or FindPage < aSpool.CurrentPage Then
            FindPos2 = FindPos2 - i + 1
            '-> on pointe sur la premiere occurence du texte recherch�
            aRtf.SelStart = aRtf.find(FindText)
            '-> Dans le cas ou on a plusieurs occurences, selectionner le bon texte
            Do While FindPos2 <= FindPos
                FindPos2 = FindPos2 + 1
                aRtf.SelStart = aRtf.find(FindText, aRtf.SelStart + Len(FindText))
            Loop
            aRtf.SelLength = Len(FindText)
            '-> On met en evidence le texte recherch�
            aRtf.SelColor = vbWhite
            Call SetBackColorSel(aRtf.hwnd, vbBlack)
            FindPos = FindPos2
            FindPage = aSpool.CurrentPage
            FindTop = False
            '-> assigner une valeur au scroll
            FindScrollH = aRect.Left
            FindScrollV = aRect.Top
        End If
    End If
End If

iniformat.crBackColor = aObject.BackColor
iniformat.dwMask = CFM_BACKCOLOR
iniformat.cbSize = Len(iniformat)
SendMessage aRtf.hwnd, EM_SETCHARFORMAT, SCF_ALL, iniformat

'-> S�lectionner les anciens objets et supprimer les objets GDI non utilis�s
If OldPen <> 0 Then
    SelectObject Sortie.hDC, OldPen
    DeleteObject hdlPen
End If

If oldBrush <> 0 Then
    SelectObject Sortie.hDC, oldBrush
    DeleteObject hdlBrush
End If

'-> Initialiser la srtucture Formatrange
fr.hDC = Sortie.hDC
fr.hdcTarget = Sortie.hDC
fr.chrg.cpMin = 0
fr.chrg.cpMax = -1

'-> Intialisation du rectangle destination
fr.rc.Left = Sortie.ScaleX(MargeInterne, 7, 1) + PosX
fr.rc.Top = Sortie.ScaleX(MargeInterne, 7, 1) + PosY
fr.rc.Right = Sortie.ScaleX(aObject.Largeur * Zoom, 7, 1) + PosX - Sortie.ScaleX(MargeInterne, 7, 1)
fr.rc.Bottom = Sortie.ScaleY(aObject.Hauteur * 2 * Zoom, 7, 1) + PosY - Sortie.ScaleY(MargeInterne, 7, 1)

'-> Initialisation du rectangle de source
fr.rcPage.Left = 0
fr.rcPage.Top = 0
fr.rcPage.Right = Sortie.ScaleX(aObject.Largeur * Zoom, 7, 1)
fr.rcPage.Bottom = Sortie.ScaleY(aObject.Hauteur * Zoom, 7, 1)

'-> Faire un setting du mode de restitution
Res = SetMapMode(aRtf.hwnd, MM_TEXT)

'-> initialisation des variables de pointage de texte
lTextOut = 0
lTextAmt = SendMessage(aRtf.hwnd, WM_GETTEXTLENGTH, 0, 0)

'-> Impression du Rtf
Do While lTextOut < lTextAmt
    lTextOut = SendMessage(aRtf.hwnd, EM_FORMATRANGE, -1, fr)
    If lTextOut < lTextAmt Then
        fr.chrg.cpMin = lTextOut
        fr.chrg.cpMax = -1
    End If
Loop

'-> Lib�rer la ressource associ�e au RTF : VERSION COMPILEE
Res = SendMessage(aRtf.hwnd, EM_FORMATRANGE, -1, Null)
If Res = 0 Then
    'MsgBox "Erreur dans la lib�ration du context"
    Res = SendMessage(aRtf.hwnd, EM_FORMATRANGE, -1, vbNullString)
    'MsgBox "Apr�s Seconde tentative " & Res
End If

'-> VERSION INTERPRETEE
'Res = SendMessage(aRtf.hwnd, EM_FORMATRANGE, -1, Null)


'-> Imprimer les bordures
If Not IsBordure Then PrintBordure aObject, aRect

'-> Redonner son vrai contenu au controle RTF
aRtf.TextRTF = RTFValue
aRtf.Refresh

'-> Lib�rer le pointeur sur le controle RTF
Set aRtf = Nothing

'-> Renvoyer une valeur de suvv�s
PrintObjRtf = True

Exit Function

GestErr:

    If Err.Number = 11 Then
        PrintObjRtf = True
        Exit Function
    End If

    Call GestError(38, aObject.Nom, aSpool)
    PrintObjRtf = False
    


End Function

Private Function ZoomRTF(TextRTF As String) As String
'--> cette fonction effectue un zoom proportionnel sur un rtf
Dim i As Integer
Dim strTaille As String
Dim taille As Integer
Dim StrTabulation As String

i = 1

'-> on gere ici la taille de la police
Do While InStr(i, TextRTF, "\fs") <> 0
    '-> on reccupere la taille
    strTaille = Mid(TextRTF, InStr(i, TextRTF, "\fs") + 3, 2)
    If Val(strTaille) <> 0 Then
        taille = Mid(TextRTF, InStr(i, TextRTF, "\fs") + 3, 2)
        i = InStr(i, TextRTF, "\fs") + 1
        '-> on applique le zoom
        If Zoom < 1 Then
            taille = taille * Zoom * 0.9 'empirique mais aide bien
        Else
            taille = taille * Zoom
        End If
        '-> on modifie la taille dans le rtf
        TextRTF = Mid(TextRTF, 1, i - 2) & "\fs" & taille & Mid(TextRTF, i + 4)
    Else
        i = i + 1
    End If
Loop

'-> on gere ici les tabulations
For i = 1 To 10
    StrTabulation = StrTabulation & "\tx" & CInt(Val(710 * i * Zoom))
Next
TextRTF = Replace(TextRTF, "\pard", "\pard" + StrTabulation)

'-> on gere ici l'espace interligne avant et apres
Do While InStr(i, TextRTF, "\sb") <> 0
    '-> on reccupere la taille
    strTaille = Mid(TextRTF, InStr(i, TextRTF, "\sb") + 3, 3)
    If Val(strTaille) <> 0 Then
        taille = Mid(TextRTF, InStr(i, TextRTF, "\sb") + 3, 3)
        i = InStr(i, TextRTF, "\sb") + 1
        '-> on applique le zoom
        taille = taille * Zoom
        '-> on modifie la taille dans le rtf
        TextRTF = Mid(TextRTF, 1, i - 2) & "\sb" & taille & Mid(TextRTF, i + 5)
    Else
        i = i + 1
    End If
Loop

i = 1
Do While InStr(i, TextRTF, "\sa") <> 0
    '-> on reccupere la taille
    strTaille = Mid(TextRTF, InStr(i, TextRTF, "\sa") + 3, 3)
    If Val(strTaille) <> 0 Then
        taille = Mid(TextRTF, InStr(i, TextRTF, "\sa") + 3, 3)
        i = InStr(i, TextRTF, "\sa") + 1
        '-> on applique le zoom
        taille = taille * Zoom
        '-> on modifie la taille dans le rtf
        TextRTF = Mid(TextRTF, 1, i - 2) & "\sa" & taille & Mid(TextRTF, i + 5)
    Else
        i = i + 1
    End If
Loop

ZoomRTF = TextRTF

End Function

Private Sub PrintBordure(aObject As Object, aRect As RECT)

Dim Temp1 As Long
Dim Temp2 As Long
Dim ClipRgn As Long
Dim OldClip As Long
Dim Pix1X As Long
Dim Pix1Y As Long
Dim aPoint As POINTAPI

Dim hdlBrush As Long
Dim oldBrush As Long
Dim hdlPen As Long
Dim OldPen As Long
Dim Res As Long

On Error Resume Next

'-> Dessin du contour de l'objet si necessaire
If TypeOf aObject Is Section Then
    If aObject.Contour Then
        hdlBrush = CreateSolidBrush(0)
        oldBrush = SelectObject(Sortie.hDC, hdlBrush)
        FrameRect Sortie.hDC, aRect, hdlBrush
    End If
ElseIf TypeOf aObject Is Cadre Then


    '-> Tenir compte de la largeur du trait
    Temp2 = frmLib.ScaleX(aObject.LargeurTrait, 3, 1)
    Temp2 = Sortie.ScaleX(Temp2, 1, 3)

    Pix1X = frmLib.ScaleX(1, 3, 1)
    Pix1X = Sortie.ScaleX(Pix1X, 1, 3)

    Pix1Y = frmLib.ScaleY(1, 3, 1)
    Pix1Y = Sortie.ScaleY(Pix1Y, 1, 3)

    '-> cr�er un stylo de dessin
    hdlPen = CreatePen(PS_SOLID, Temp2, 0)
    OldPen = SelectObject(Sortie.hDC, hdlPen)
    
    '-> Cr�er le fond
    hdlBrush = CreateSolidBrush(aObject.BackColor)
    oldBrush = SelectObject(Sortie.hDC, hdlBrush)
    
    '-> S�lectionner le rectangle comme zone de clipping
    ClipRgn = CreateRectRgn(aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
    SelectClipRgn Sortie.hDC, ClipRgn

    If aObject.IsRoundRect Then
        If Temp2 / 2 <> Fix(Temp2 / 2) Then Temp2 = Temp2 + 1
        
        Pix1X = frmLib.ScaleX(50, 3, 1)
        Pix1X = Sortie.ScaleX(Pix1X, 1, 3)

        Pix1Y = frmLib.ScaleY(50, 3, 1)
        Pix1Y = Sortie.ScaleY(Pix1Y, 1, 3)
        Res = RoundRect(Sortie.hDC, aRect.Left + Temp2 / 2, aRect.Top + Temp2 / 2, aRect.Right - Temp2 / 2, aRect.Bottom - Temp2 / 2, Pix1X, Pix1Y)
    Else
    
        '-> Bordure gauche
        If aObject.Gauche Then
            '-> Dessin des bordures
            Temp1 = Fix(Temp2 / 2)
            MoveToEx Sortie.hDC, aRect.Left + Temp1, aRect.Top, aPoint
            LineTo Sortie.hDC, aRect.Left + Temp1, aRect.Bottom - 1
        End If
        
        '-> Bordure haut
        If aObject.Haut Then
            Temp1 = Fix(Temp2 / 2)
            MoveToEx Sortie.hDC, aRect.Left, aRect.Top + Temp1, aPoint
            LineTo Sortie.hDC, aRect.Right, aRect.Top + Temp1
        End If
        
        '-> Bordure Droite
        If aObject.Droite Then
            If (Temp2 / 2) = Fix(Temp2 / 2) Then
                Temp1 = Temp2 / 2
            Else
                Temp1 = (Temp2 + 1) / 2
            End If
            MoveToEx Sortie.hDC, aRect.Right - Temp1, aRect.Top, aPoint
            LineTo Sortie.hDC, aRect.Right - Temp1, aRect.Bottom
        End If
        
        '-> Bordure Bas
        If aObject.Bas Then
            If (Temp2 / 2) = Fix(Temp2 / 2) Then
                Temp1 = Temp2 / 2
            Else
                Temp1 = (Temp2 + 1) / 2
            End If
            MoveToEx Sortie.hDC, aRect.Left, aRect.Bottom - Temp1, aPoint
            LineTo Sortie.hDC, aRect.Right, aRect.Bottom - Temp1
        End If
        
    End If
    
    '-> Restituer la zone de clipping
    OldClip = CreateRectRgn(0, 0, Sortie.ScaleWidth, Sortie.ScaleHeight)
    SelectClipRgn Sortie.hDC, OldClip
    DeleteObject ClipRgn
    DeleteObject OldClip
End If
 
'-> S�lectionner les anciens objets et supprimer les objets GDI non utilis�s
If OldPen <> 0 Then
    SelectObject Sortie.hDC, OldPen
    DeleteObject hdlPen
End If

If oldBrush <> 0 Then
    SelectObject Sortie.hDC, oldBrush
    DeleteObject hdlBrush
End If


End Sub



Public Function FormatNumTP(ByVal ToFormat As String, ByVal Msk As String, Optional NomBlock As String, Optional Cell As String) As FormatedCell

'---> Cette fonction a pour but d'analyser une chaine de caract�re et de la retouner
'formatt�e en s�parateur de milier et en nombre de d�cimal

Dim strFormat As String
Dim strTempo As String
Dim strPartieEntiere As String
Dim StrPartieDecimale As String
Dim i As Integer, j As Integer
Dim nbDec As Integer
Dim strSep As String
Dim idNegatif As Integer
Dim Masque As String
Dim Tempo As String
Dim FindSepDec As Boolean

On Error GoTo GestErr

For i = Len(ToFormat) To 1 Step -1
    If IsNumeric(Mid$(ToFormat, i, 1)) Then
        Tempo = Mid$(ToFormat, i, 1) & Tempo
    Else
        '-> Gestion du s�parateur d�cimal
        If Mid$(ToFormat, i, 1) = "." Or Mid$(ToFormat, i, 1) = "," Then
            If Not FindSepDec Then
                Tempo = SepDec & Tempo
                FindSepDec = True
            End If
        Else
            Tempo = Mid$(ToFormat, i, 1) & Tempo
        End If
    End If
Next 'Pour tous les caract�res

ToFormat = Tempo

If Trim(ToFormat) = "" Then
Else
    '-> Tester si on envoie une zone num�rique
    If Not IsNumeric(ToFormat) Then
        FormatNumTP.Ok = False
        Exit Function
    End If
End If

'-> Charger le masque de la cellule
nbDec = CInt(Entry(1, Msk, "�"))
If Entry(2, Msk, "�") <> "" Then
    strSep = SepMil 'Entry(2, Msk, "�")
Else
    strSep = ""
End If
idNegatif = CInt(Entry(3, Msk, "�"))

FormatNumTP.Ok = True
FormatNumTP.nbDec = nbDec
FormatNumTP.Value = CDbl(ToFormat)
FormatNumTP.idNegatif = idNegatif

'-> Analyse si d�cimale
If nbDec = 0 Then
    '-> Arrondir
    strTempo = CStr(CDbl(ToFormat))
    Masque = "#########################################0"
Else
    Masque = "#########################################0." & String(nbDec, "0")
    strTempo = ToFormat
End If

'-> Construction d'un masque assez grand pour formatter n'importe qu'elle zone
strTempo = FORMAT(Abs(strTempo), Masque)

'-> Construction de la partie enti�re
If nbDec <> 0 Then
    strPartieEntiere = Mid$(strTempo, 1, InStr(1, strTempo, SepDec) - 1)
    StrPartieDecimale = SepDec & Mid$(strTempo, InStr(strTempo, SepDec) + 1, nbDec)
Else
    strPartieEntiere = strTempo
    StrPartieDecimale = ""
End If

j = 1
For i = Len(strPartieEntiere) To 1 Step -1
    strFormat = Mid$(strTempo, i, 1) & strFormat
    If j = 3 And i <> 1 Then
        strFormat = strSep & strFormat
        j = 1
    Else
        j = j + 1
    End If
Next

FormatNumTP.strFormatted = strFormat & StrPartieDecimale

Exit Function

GestErr:
    FormatNumTP.Ok = False
End Function

Public Function PrintTableau(ByRef NomObjet As String, ByRef Param As String, _
                         ByRef DataFields As String, ByRef PositionX As Long, _
                         ByRef PositionY As Long, ByRef NomSousObjet As String, ByRef FirstObj As Boolean, ByRef aSpool As Spool) As Boolean

Dim aTb As Tableau
Dim nLig As Integer


On Error GoTo GestError

Trace "PrintTableau " & NomObjet & DataFields

'-> Pointer sur le tableau pass� en argument
Set aTb = aSpool.Maquette.Tableaux(UCase$(NomObjet))

'-> R�cup�ration de la ligne de tableau � imprimer
nLig = CInt(Entry(2, Param, "\"))

'-> Imprimer le block de ligne
PositionY = PrintBlock(NomSousObjet, Param, DataFields, PositionX, PositionY, aTb, nLig, aSpool)

'-> Quitter si valeur d'erreur
If PositionY = -9999 Then GoTo GestError

'-> Renvoyer une valeur de succ�s
PrintTableau = True

Exit Function

GestError:
    Call GestError(37, aTb.Nom, aSpool)
    PrintTableau = False


End Function

Public Function GetPictureAssociation(ValueToSearch As String) As String

Dim Res As Long
Dim lpBuffer As String

On Error GoTo GestError

'-> reccuperer le tm_picture.ini si besoin
If VersionTurbo = 1 And Tm_PictureIniFile = "" Then
    TelechargeFile TurboGraphWebFile & "deallogo/Tm_Picture.txt", App.Path & "\Tm_Picture.ini"
    Tm_PictureIniFile = App.Path & "\Tm_Picture.ini"
    If Dir$(Tm_PictureIniFile, vbNormal) = "" Then Tm_PictureIniFile = ""
End If
'-> V�rifier que l'on ait bien acc�s au fichier TM_Picture.ini
If Tm_PictureIniFile = "" Then Exit Function

lpBuffer = Space$(5000)
Res = GetPrivateProfileString("IMAGES", ValueToSearch, "", lpBuffer, Len(lpBuffer), Tm_PictureIniFile)
If Res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Res)
    '-> Cr�er la liste des mots cl�s
    GetPathMotCles
    '-> Faire le remplacement
    ReplacePathMotCles lpBuffer
    GetPictureAssociation = lpBuffer
End If

Exit Function

GestError:
    GetPictureAssociation = ""

End Function

Public Sub CloseFichier(ByVal FileToclose As String)

'---> Cette d�charge le fichier sp�cifi�

Dim aFichier As Fichier
Dim aSpool As Spool


On Error Resume Next

'-> Pointer sur le fichier sp�cifi�
Set aFichier = Fichiers(UCase$(Trim(FileToclose)))

'-> supprimer toutes les pages actives
For Each aSpool In aFichier.Spools
    If Not aSpool.frmDisplay Is Nothing Then Unload aSpool.frmDisplay
Next

'-> Supprimer l'objet de la collection des fichiers
Fichiers.Remove (UCase$(Trim(FileToclose)))

'-> Supprimer les nodes
MDIMain.TreeNaviga.Nodes.Remove (UCase$(Trim(FileToclose)))

End Sub



Public Sub DrawWait()

'---> Proc�dure qui dessine la temporisation

Dim hdlBrush As Long
Dim hdlPen As Long
Dim oldBrush As Long
Dim OldPen As Long
Dim Res As Long
Dim hDC As Long
Dim StartPanel As Long

On Error Resume Next

'-> Get du Dc du pannel
hDC = GetDC(MDIMain.StatusBar1.hwnd)

'-> Cr�ation des objets GDI pour dessin des cellules
hdlBrush = CreateSolidBrush(&HFF8080)
hdlPen = CreatePen(PS_NULL, 1, 0)

'-> S�lection du pinceau dans le Contexte
oldBrush = SelectObject(hDC, hdlBrush)

'-> Dessin de la cellule
OldPen = SelectObject(hDC, hdlPen)

'-> Depart du dessin
StartPanel = frmLib.ScaleX(MDIMain.StatusBar1.Panels(1).Width, 1, 3) + 4

'-> Dessin du rectangle
Res = Rectangle(hDC, StartPanel, 3, StartPanel + ((TailleLue / TailleTotale) * (frmLib.ScaleX(MDIMain.StatusBar1.Panels(2).Width, 1, 3) - 2)), 17)

'-> Liberer les objets GDI
SelectObject hDC, oldBrush
SelectObject hDC, OldPen
DeleteObject hdlPen
DeleteObject hdlBrush
ReleaseDC MDIMain.StatusBar1.hwnd, hDC

End Sub
    
Private Sub LoadInternetMessProg()

Dim aLb As Libelle
Dim strLib As String

'********************
'* MESSPROG MDIMAIN *
'********************

Set aLb = New Libelle
strLib = "1=&Fichier" & Chr(0) & "2=&Ouvrir" & Chr(0) & "3=&Imprimer" & Chr(0) & _
         "4=&Quitter" & Chr(0) & "5=F&en�tre" & Chr(0) & "6=&Navigation" & Chr(0) & _
         "7=Mosa�que &horizontale" & Chr(0) & "8=Mosa�que &verticale" & Chr(0) & _
         "9=&En cascade" & Chr(0) & "10=Visionneuse DEAL INFORMATIQUE." & Chr(0) & _
         "11=Erreur sur le fichier." & Chr(0) & "12=Page N� : " & Chr(0) & "13=Fermer le fichier" & Chr(0) & _
         "14=Imprimer" & Chr(0) & "15=Propri�t�s" & Chr(0) & "16=Envoyer Vers" & Chr(0) & _
         "17=Internet" & Chr(0) & "18=Messagerie" & Chr(0) & "19=Editeur de texte" & Chr(0) & _
         "20=&R�organiser les icones" & Chr(0) & "21=A propos de la visionneuse DEAL Informatique" & Chr(0) & _
         "22=Supprimer" & Chr(0) & "23=D�finitivement" & Chr(0) & "24=Envoyer � la poubelle" & Chr(0) & "25=Le fichier existe d�ja : d�sirez-vous l'�craser ?�Confirmation" & Chr(0) & _
         "26=V�rifier les mises � jour" & Chr(0) & "27=Gestion des fichiers" & Chr(0) & "28="

'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "MDIMAIN"

'******************
'* Ac�s au d�tail *
'******************

Set aLb = New Libelle
strLib = "1=Impossible de trouver le fichier d�tail sp�cifi�D�tail" & Chr(0) & "2=Cr�ation de l'acc�s au d�tail en cours..." & _
          Chr(0) & "3=Acc�s au d�tail"

'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "ACCESDET"

'********************
'* MESSPROG FRMMAIL *
'********************
Set aLb = New Libelle
strLib = "1=Envoyer un fichier" & Chr(0) & "2=G�n�ral" & Chr(0) & "3=Envoyer la page en cours" & Chr(0) & _
         "4=Envoyer le spool en entier" & Chr(0) & "5=Crypter le fichier" & Chr(0) & "6=Internet" & Chr(0) & _
         "7=OutLook" & Chr(0) & "8=Destinataire" & Chr(0) & "9=Sujet" & Chr(0) & "10=Message" & Chr(0) & _
         "11=Envoyer" & Chr(0) & "12=Police" & Chr(0) & "13=Couleur" & Chr(0) & "14=Aligner � gauche" & Chr(0) & _
         "15=Centrer" & Chr(0) & "16=Aligner � droite" & Chr(0) & "17=Veuillez saisir le destinataire du message�Erreur" & Chr(0) & _
         "18=Envoyer le fichier en entier" & Chr(0) & "19=Options" & Chr(0) & "20=Format du fichier" & Chr(0) & _
         "21=Utiliser le format Turbo" & Chr(0) & "22=Utiliser le format HTML" & Chr(0) & "23=Navigation" & Chr(0) & _
         "24=Cr�er une page de navigation" & Chr(0) & "25=G�n�rer un seul fichier contenant toutes les pages" & Chr(0) & _
         "26=G�n�rer de simples pages" & Chr(0) & "27=Inclure le num�ro du spool dans le nom des pages" & Chr(0) & _
         "28=Int�grer le spool dans le corps du mail" & Chr(0) & "29=Utiliser le format PDF" & Chr(0) & _
         "30=Pas de pi�ce jointe" & Chr(0) & "31=Lotus Notes" & Chr(0) & "32=Copies" & Chr(0) & _
         "33=A" & Chr(0) & "34=Cc" & Chr(0) & "35=Obj" & Chr(0) & "36=Envoi multiple de courriel" & Chr(0) & "37=Destinataires" & Chr(0) & "38=Envoyer" & Chr(0) & "39=Message" & _
         Chr(0) & "40=Pi�ces jointes" & Chr(0) & "41=Format des fichiers joints" & Chr(0) & "42=Destinataires des messages" & Chr(0) & "43=Message commun � envoyer" & Chr(0) & "44=Cette fonctionnalit� n�cessite Microsoft OutLook" & Chr(0) & "45=Cette �dition n'a pas �t� configur�e pour le mailing" & Chr(0) & "46=Imprimer" & _
         Chr(0) & "47=Imprimer les destinataires s�lectionn�s" & Chr(0) & "48=Imprimer les destinataires non s�lectionn�s" & Chr(0) & "49=Nom du fichier PDF :"
         
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "FRMMAIL"
         
'*************************
'* MESSPROG FRMINTERNET  *
'*************************
         
Set aLb = New Libelle
strLib = "1=Exporter au format HTML" & Chr(0) & "2=Export :" & Chr(0) & "3=Exporter la page en cours" & Chr(0) & _
         "4=Exporter le spool en entier" & Chr(0) & "5=Options : " & Chr(0) & "6=Cr�er une page de navigation" & Chr(0) & _
         "7=Lancer � la fin du traitement" & Chr(0) & "8=Traitement : " & Chr(0) & "9=Cr�ation de la page : " & Chr(0) & _
         "10=Exporter" & Chr(0) & "11=Navigation : " & Chr(0) & "12=Utiliser une barre de navigation" & Chr(0) & "13=Utiliser une liste de pages" & Chr(0) & _
         "14=Exporter le fichier en entier" & Chr(0) & "15=Inclure le num�ro du spool dans le nom des pages" & Chr(0) & _
         "16=G�n�rer un seul fichier contenant toutes les pages" & Chr(0) & "17=G�n�rer de simples pages" & Chr(0) & _
         "18=Fichier : " & Chr(0) & "19=R�pertoire d'export" & Chr(0) & "20=Nom du fichier � g�n�rer" & Chr(0) & _
         "21=Nom de fichier incorrect" & Chr(0) & "22=Exporter les bordures"
         
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "FRMINTERNET"
                  
'*************************
'* MESSPROG FRMVISUSPOOL *
'*************************

Set aLb = New Libelle
strLib = "1=Page de s�lection" & Chr(0) & "2=Premi�re page" & Chr(0) & "3=Page pr�c�dente" & Chr(0) & _
         "4=Atteindre une page" & Chr(0) & "5=Page suivante" & Chr(0) & "6=Derni�re page" & Chr(0) & _
         "7=Imprimer" & Chr(0) & "8=Envoyer un message" & Chr(0) & "9=Exporter au format HTML" & Chr(0) & _
         "10=Aide" & Chr(0) & "11=Spool : � du fichier : " & Chr(0) & "12=Imprimer le fichier" & Chr(0) & _
         "13=Imprimer le spool" & Chr(0) & "14=Imprimer la page" & Chr(0) & "15=Exporter au format PDF" & _
         Chr(0) & "16=Rechercher" & Chr(0) & "17=Rechercher le suivant" & Chr(0) & "18=Exporter au format OpenOffice" & _
         Chr(0) & "19=Acrobat PdfWriter Distiller, Win2pdf, PdfCreator" & Chr(0) & "20=Courriel" & Chr(0) & "21=Pi�ces jointes" & Chr(0) & "22=Ajuster"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "FRMVISUSPOOL"
         
'*************************
'* MESSPROG UPDATE *
'*************************

Set aLb = New Libelle
strLib = "1=Une nouvelle mise � jour est disponible." & Chr(0) & "2=Installer la mise � jour ?" & Chr(0) & "3=Vous poss�dez d�j� la version la plus r�cente du logiciel." & Chr(0) & _
         "4=Impossible de t�l�charger la mise � jour !" & Chr(0) & "5=Impossible de v�rifier la disponibilit� des mises � jour" & Chr(0) & "6=Le programme va �tre ferm� pour pouvoir lancer l'installation." & Chr(0) & _
         "7=Voulez-vous continuer ?" & Chr(0) & "8=Impossible de r�cup�rer la mise � jour !" & Chr(0) & "9=V�rifiez que vous �tes bien connect� � Internet et r�essayez plus tard !" & Chr(0) & _
         "10=Attention" & Chr(0) & "11=Appuyez sur 'Non' pour ignorer d�finitivement les mises � jour" & Chr(0) & "Le t�l�chargement pourra prendre quelques instant..." & Chr(0) & "12=Imprimer le fichier"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "UPDATE"
                  
'**********************
'* MESSPROG STATUSBAR *
'**********************
Set aLb = New Libelle
strLib = "1=Lecture du fichier $FICHIER$ en cours ..." & Chr(0) & "2=Initialisation des maquettes ..." & Chr(0) & _
         "3=Lecture de la page en cours ..."
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "STATUSBAR"

'*******************
'* MESSPROG ERRORS *
'*******************
Set aLb = New Libelle
strLib = "1=" & Chr(0) & _
         "2=Impossible d'ouvrir le fichier $FICHIER$ pour lecture." & Chr(0) & _
         "3=Erreur lors de la cr�ation de l'objet de la classe Fichier pour le fichier $FICHIER$." & Chr(0) & _
         "4=Impossible de lire le fichier ASCII apr�s ouverture." & Chr(0) & "5=" & Chr(0) & _
         "6=Impossible de trouver un spool dans fichier sp�cifi� $FICHIER$." & Chr(0) & _
         "7=Impossible de trouver la maquette $MAQ$ du fichier $FICHIER$." & Chr(0) & _
         "8=Erreur systeme : Impossible de cr�er un fichier temporaire pour lecture de la maquette $MAQ$." & Chr(0) & _
         "9=Impossible d'ouvrir le fichier temporaire $INI$ de la maquette $MAQ$." & Chr(0) & _
         "10=Erreur lors de la lecture du fichier temporaire $INI$ de la maquette $MAQ$." & Chr(0) & _
         "11=Erreur dans la cr�ation d'un objet de type Section : $SECTION$ de la maquette $MAQ$." & _
         "12=Erreur dansla cr�ation d'un  objet de type Tableau : $TABLEAU$ de la maquette $MAQ$." & Chr(0) & "13=" & Chr(0) & _
         "14=Erreur dans la cr�ation d'un BMP : $BMP$ de la section : $SECTION$ de la maquette $MAQ$." & Chr(0) & _
         "15=Erreur dans la cr�ation d'un Cadre : $CADRE$ de la section : $SECTION$ de la maquette $MAQ$." & Chr(0) & _
         "16=Erreur dans la cr�ation d'un block : $BLOCK$ du tableau $TABLEAU$ de la maquette $MAQ$." & Chr(0) & _
         "17=Impossible de cr�er un fichier temporaire pour un objet RTF." & Chr(0) & _
         "18=Erreur dans l'initialisation de l'entete de la maquette : " & Chr(0) & _
         "19=Erreur dans l'initialisation d'un nouveau conteneur BMP." & Chr(0) & _
         "20=Erreur dans le setting des dimensions d'un objet BMP : $BMP$ de la section : $SECTION$. Hauteur : $HAUTEUR$   Largeur : $LARGEUR$." & Chr(0) & _
         "21=Erreur dans le setting du propath d'une image." & Chr(0) & _
         "22=Erreur lors du chargement du fichier : $FICHIER$ du bmp : $BMP$ de la section : $SECTION$." & Chr(0) & _
         "23=Erreur lors de la cr�ation de la repr�sentation de l'objet cadre $CADRE$ de la section $SECTION$." & Chr(0) & _
         "24=Erreur lors du chargement du fichier temporaire $FICHIER$ du cadre : $CADRE$ de la section : $SECTION$." & Chr(0) & _
         "25=Erreur lors du chargement du fichier temporaire $FICHIER$ de la section : $SECTION$." & Chr(0) & _
         "26=Erreur lors de la cr�ation de la repr�sentation physique de la section : $SECTION$." & Chr(0) & _
         "27=Erreur lors de l'initialisation de la section $SECTION$ pour la valeur de ligne : $LIG$." & Chr(0)
         
         
strLib = strLib & "28=Erreur lors de l'initialisation de l'entete de l'image : $BMP$ de la section $SECTION$ pour la valeur : $LIG$" & Chr(0) & Chr(0) & _
                  "29=Erreur lors de l'initialisation de l'entete du cadre : $CADRE$ de la section $SECTION$ pour la valeur : $LIG$" & Chr(0) & _
                  "30=Erreur lors de l'initialisation du tableau $TABLEAU$ pour la valeur : $LIG$." & Chr(0) & _
                  "31=Erreur lors de l'initialisation du block $BLOCK$ pour le tableau $TABLEAU$ pour la valeur $LIG$." & Chr(0) & _
                  "32=Erreur lors de la lecture d'une ligne de d�finition du block $BLOCK$ du tableau $TABLEAU$." & Chr(0) & _
                  "33=Erreur lors de la cr�ation des cellule du block $BLOCK$ du tableau $TABLEAU$ pour la valeur $LIG$." & Chr(0) & _
                  "34=Erreur lors de l'initialisation des dimensions des cellules du block : $BLOCK$ du tableau $TABLEAU$" & Chr(0) & _
                  "35=Erreur dans l'initialisation de la page � imprimer" & Chr(0) & _
                  "36=Erreur dans l'initialisation de l'impression de la section : $SECTION$." & Chr(0) & _
                  "37=Erreur dans l'initialisation de l'impression du tableau : $TABLEAU$." & Chr(0) & _
                  "38=Erreur dans l'impression d'un objet RTF : $OBJECT$." & Chr(0) & _
                  "39=Erreur dans l'impression de l'image : $BMP$ de la section : $SECTION$." & Chr(0) & _
                  "40=Erreur dans l'initialisation du block : $BLOCK$ du tableau $TABLEAU$" & Chr(0) & _
                  "41=Erreur dans le dessin de la cellule : $CELLULE$ du block : $BLOCK$ du tableau $TABLEAU$." & Chr(0) & _
                  "42=Erreur durant l'impression du fichier.�Erreur fatale d'impression"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "ERRORS"

'********************
'* MESSPROG MESSAGE *
'********************
Set aLb = New Libelle

strLib = "1=Quitter maintenant ?�Confirmation" & Chr(0) & "2=Fichier d�ja charg�.�Impossible de charger le fichier." & Chr(0) & _
         "3=Num�ro de page incorrect.�Erreur" & Chr(0) & "4=Impossible de se connecter � Ms OutLook.�Impossible d'envoyer un message." & Chr(0) & _
         "5=Impossible de cr�er un nouveau message dans Ms OutLook.�Impossible de cr�er un message." & Chr(0) & _
         "6=Impossible de joinde le fichier sp�cifi� : $FICHIER$.�Erreur." & Chr(0) & _
         "7=Envoyer la page en cours [Oui] ou le spool en entier [Non] ?�Question" & Chr(0) & _
         "8=Veuillez saisir le destinataire du message.�Erreur" & Chr(0) & "9=Impossible d'envoyer le message sur Internet.�Erreur" & Chr(0) & _
         "10=Impossible d'initialiser la session de messagerie.�Erreur" & Chr(0) & "11=Crypter le fichier ?�Question" & Chr(0) & "12=Saisie incorrecte.�Erreur"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "MESSAGE"

'********************
'* MESSPROG BOUTONS *
'********************
Set aLb = New Libelle

strLib = "1=&Ok" & Chr(0) & "2=&Annuler" & Chr(0) & "3=&Envoyer"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "BOUTONS"

'***************
'* FRMNETSEND  *
'***************
Set aLb = New Libelle
strLib = "1=Envoyer un e-mail" & Chr(0) & "2=Url : " & Chr(0) & "3=Message : " & Chr(0) & _
         "4=Objet : " & Chr(0) & "5=Carnet d'adresses" & Chr(0) & "6=Crypter le fichier associ� :"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "FRMNETSEND"
    
'***************
'* FRMEDITOR  *
'***************
Set aLb = New Libelle
strLib = "1=&Fichier" & Chr(0) & "2=&Ouvrir" & Chr(0) & "3=&Enregistrer" & Chr(0) & _
         "4=En&registrer sous" & Chr(0) & "5=&Imprimer" & Chr(0) & "6=&Quitter" & Chr(0) & _
         "7=&Edition" & Chr(0) & "8=Couper" & Chr(0) & "9=Copier" & Chr(0) & _
         "10=Coller" & Chr(0) & "11=Rechercher" & Chr(0) & "12=A propos de ..." & Chr(0) & _
         "13=Chercher : " & Chr(0) & "14=Forma&t" & Chr(0) & "15=&Police" & Chr(0) & _
         "16=Envoie du fichier : " & Chr(0) & "17=Envoie du spool :" & Chr(0) & "18=Envoie de la page : "
         
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "FRMEDITOR"

'***************
'* FRMFONT     *
'***************
Set aLb = New Libelle
strLib = "1=Choix d'une police" & Chr(0) & "2=Police :" & Chr(0) & "3=Style de police :" & Chr(0) & _
         "4=Taille : " & Chr(0) & "5=Normal" & Chr(0) & "6=Italique" & Chr(0) & "7=Gras" & Chr(0) & _
         "8=Gras Italique"

'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "FRMFONT"

'**************************
'* MESSPROG FRMSEARCHPAGE *
'**************************
Set aLb = New Libelle

strLib = "1=Atteindre une page" & Chr(0) & "Num�ro de la page :"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "FRMSEARCHPAGE"

'**************************
'* MESSPROG FRMSEARCHTEXT *
'**************************
Set aLb = New Libelle

strLib = "1=Rechercher" & Chr(0) & "2=Rechercher :" & Chr(0) & "3=La cha�ne recherch�e n'a pas �t� trouv�e�Erreur"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "FRMSEARCHTEXT"

'*************************
'* MESSPROG D'IMPRESSION *
'*************************
Set aLb = New Libelle
strLib = "1=S�lection d'une imprimante" & Chr(0) & "2=Nom : " & Chr(0) & "3=Entendue de l'impression : " & Chr(0) & _
        "4=Imprimer le fichier" & Chr(0) & "5=Pages" & Chr(0) & "6=de :" & Chr(0) & "7=a :" & Chr(0) & _
        "8=Page en cours" & Chr(0) & "9=Copies" & Chr(0) & "10=" & Chr(0) & "11=&OK" & Chr(0) & "12=&Annuler" & Chr(0) & _
        "13=Veuillez s�lectionner une imprimante�Erreur" & Chr(0) & "14=Veuillez indiquer le nombre de copies�Erreur" & Chr(0) & _
        "15=Veuillez indiquer la page mini � imprimer�Erreur" & Chr(0) & "16=Veuillez indiquer la page maxi � imprimer�Erreur" & Chr(0) & _
        "17=Visualisation �cran" & Chr(0) & _
        "18=Propri�t�s" & Chr(0) & _
        "19=Imprimer en Recto-Verso" & Chr(0) & "20=Spool en cours" & Chr(0) & _
        "21=Mise en page" & Chr(0) & "22=Sans la page des s�lections"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "FRMPRINTVISU"
        
'*********************
'* EXPORT VERS EXCEL *
'*********************
Set aLb = New Libelle

strLib = "1=Export vers Excel" & Chr(0) & "2=Etendue de l'export:" & Chr(0) & "3=Gestion des feuilles" & Chr(0) & _
        "4=Cr�er une feuille � chaque page" & Chr(0) & "5=Regrouper toutes les pages sur une seule feuille" & Chr(0) & _
        "6=Attention : la dur�e du traitement de l'export vers MircoSoft Excel est largement conditionn�e par les options de mise en forme des cellules." & Chr(0) & _
        "7=Maquette" & Chr(0) & "8=Propri�t�s d'export : " & Chr(0) & "9=Tableau :" & Chr(0) & _
        "10=Block :" & Chr(0) & "11=Ligne :" & Chr(0) & "12=N'exporter qu'une seule fois" & Chr(0) & _
        "13=R�f�rence" & Chr(0) & "14=Utiliser un classeur de r�f�rence" & Chr(0) & "15=Nommer la feuille" & Chr(0) & _
        "16=Nommer la plage de cellules" & Chr(0) & "17=Sp�cifier la cellule de d�part" & Chr(0) & "18=Traitement" & Chr(0) & _
        "19=Traitement en cours" & Chr(0) & "20=Exporter le format" & Chr(0) & "21=Valeur de propri�t� incorrecte" & Chr(0) & _
        "22=Erreur dans le param�trage des pages � imprimer" & Chr(0) & "23=Erreur" & Chr(0) & "24=Veuillez saisir le classeur de r�f�rence" & Chr(0) & _
        "25=Impossible de trouver le fichier" & Chr(0) & "26=Veuillez saisir le nom de l'onglet de classeur" & Chr(0) & "27=Veuillez saisir le nom de la plage de donn�es" & Chr(0) & _
        "28=Veuillez saisir la cellule de d�part" & Chr(0) & "29=Il est obligatoire de donner un nom � la feuille lorsque l'on exporte toutes les pages dans une seule feuille." & Chr(0) & _
        "30=Classeur Excel" & Chr(0) & "31=Mod�le de classeur" & Chr(0) & "32=Erreur dans l'ouverture du classeur de r�f�rence" & Chr(0) & "33=Nom de fichier incorrect" & Chr(0) & "34=Visualisation �cran" & Chr(0) & _
        "35=G�n�ral" & Chr(0) & "36=Spool en entier" & Chr(0) & "37=Descriptifs des �l�m�nts � exporter" & Chr(0) & "38=Page" & Chr(0) & _
        "39=Export : " & Chr(0) & "40=Formatage : " & Chr(0) & "41=Donn�es sans format"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "FRMEXCEL"

'*********************
'* EXPORT VERS OPENOFFICE *
'*********************
Set aLb = New Libelle

strLib = "1=Export vers OpenOffice" & Chr(0) & "2=Etendue de l'export:" & Chr(0) & "3=Gestion des feuilles" & Chr(0) & _
        "4=Cr�er une feuille � chaque page" & Chr(0) & "5=Regrouper toutes les pages sur une seule feuille" & Chr(0) & _
        "6=Attention : la dur�e du traitement de l'export vers OpenOffice est largement conditionn�e par les options de mise en forme des cellules." & Chr(0) & _
        "7=Maquette" & Chr(0) & "8=Propri�t�s d'export : " & Chr(0) & "9=Tableau :" & Chr(0) & _
        "10=Block :" & Chr(0) & "11=Ligne :" & Chr(0) & "12=N'exporter qu'une seule fois" & Chr(0) & _
        "13=R�f�rence" & Chr(0) & "14=Utiliser un classeur de r�f�rence" & Chr(0) & "15=Nommer la feuille" & Chr(0) & _
        "16=Nommer la plage de cellules" & Chr(0) & "17=Sp�cifier la cellule de d�part" & Chr(0) & "18=Traitement" & Chr(0) & _
        "19=Traitement en cours" & Chr(0) & "20=Exporter le format" & Chr(0) & "21=Valeur de propri�t� incorrecte" & Chr(0) & _
        "22=Erreur dans le param�trage des pages � imprimer" & Chr(0) & "23=Erreur" & Chr(0) & "24=Veuillez saisir le classeur de r�f�rence" & Chr(0) & _
        "25=Impossible de trouver le fichier" & Chr(0) & "26=Veuillez saisir le nom de l'onglet de classeur" & Chr(0) & "27=Veuillez saisir le nom de la plage de donn�es" & Chr(0) & _
        "28=Veuillez saisir la cellule de d�part" & Chr(0) & "29=Il est obligatoire de donner un nom � la feuille lorsque l'on exporte toutes les pages dans une seule feuille." & Chr(0) & _
        "30=Classeur OpenOffice" & Chr(0) & "31=Mod�le de classeur" & Chr(0) & "32=Erreur dans l'ouverture du classeur de r�f�rence" & Chr(0) & "33=Nom de fichier incorrect" & Chr(0) & "34=Visualisation �cran" & Chr(0) & _
        "35=G�n�ral" & Chr(0) & "36=Spool en entier" & Chr(0) & "37=Descriptifs des �l�m�nts � exporter" & Chr(0) & "38=Page" & Chr(0) & _
        "39=Export : " & Chr(0) & "40=Formatage : "
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "FRMOPENOFFICE"

'********************
'* MESSPROG FRMGESTSPOOL *
'********************
Set aLb = New Libelle
strLib = "1=Titre :" & Chr(0) & "2=Objet :" & Chr(0) & "3=Auteur :" & Chr(0) & _
         "4=Cat�gorie :" & Chr(0) & "5=Mots-cl�s :" & Chr(0) & "6=Contenant :" & Chr(0) & _
         "7=Date min/max" & Chr(0) & "8=Filtrer les fichiers" & Chr(0) & "9=R�sum�" & Chr(0) & "10=Aper�u" & Chr(0) & _
         "11=Gestion des spools" & Chr(0) & "12=Ouvrir le fichier" & Chr(0) & "13=Pr�visualiser le fichier" & Chr(0) & "14=Supprimer (les) fichier(s) [suppr]" & Chr(0) & _
         "15=Renommer le fichier [F2]" & Chr(0) & "16=Sauvegarder le fichier [Ctrl+S]" & Chr(0) & "17=Imprimer le fichier [Ctrl+P]" & Chr(0) & _
         "18=R�actualiser et r�initialiser [F5]" & Chr(0) & "19=Fusionner les spools s�l�ctionn�s" & Chr(0) & "20=Eclater le spool s�lection�" & Chr(0) & _
         "21=Afficher/Masquer le r�sum� des fichiers" & Chr(0) & "22=Utiliser le format HTML" & Chr(0) & "23=Navigation" & Chr(0) & _
         "24=Cr�er une page de navigation" & Chr(0) & "25=G�n�rer un seul fichier contenant toutes les pages" & Chr(0) & _
         "26=G�n�rer de simples pages" & Chr(0) & "27=Inclure le num�ro du spool dans le nom des pages" & Chr(0) & _
         "28=Int�grer le spool dans le corps du mail" & Chr(0) & "29=Utiliser le format PDF" & Chr(0) & _
         "30=Pas de pi�ce jointe" & Chr(0) & "31=Lotus Notes" & Chr(0) & "32=Copies" & Chr(0) & _
         "33=A" & Chr(0) & "34=Cc" & Chr(0) & "35=Obj" & Chr(0) & "36=Envoi multiple de courriel" & Chr(0) & "37=Destinataires" & Chr(0) & "38=Envoyer" & Chr(0) & "39=Message" & _
         Chr(0) & "40=Pi�ces jointes" & Chr(0) & "41=Format des fichiers joints" & Chr(0) & "42=Destinataires des messages" & Chr(0) & "43=Message commun � envoyer" & Chr(0) & "44=Cette fonctionnalit� n�cessite Microsoft OutLook" & Chr(0) & "45=Nom :"
         
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.add aLb, "FRMGESTSPOOL"

End Sub


Public Function IsOutLook() As Boolean

'---> Cette proc�dure V�rifier si outlook est install�

Dim aObj As Object

On Error GoTo GestError

Set aObj = CreateObject("Outlook.Application")

Set aObj = Nothing
IsOutLook = True


Exit Function

GestError:
    IsOutLook = False

End Function

Public Function IsLotus() As Boolean

'---> Cette proc�dure V�rifier si lotus est install�
Dim aObj As Object

On Error GoTo GestError

'-> dans le cas ou lotus n'est pas actif verifier si on doit passer le mot de passe
If IsLotus = False Then
'    If GetIniString("LOTUS", "PWD", TurboGraphIniFile, False) <> "" Then
'        '-> on ouvre la session en passant le mot de passe
'        Set aObj = CreateObject("Notes.NotesSession")
'        aObj.Initialize (GetIniString("LOTUS", "PWD", TurboGraphIniFile, False))
'        IsLotus = True
'    Else
        Set aObj = CreateObject("Notes.NotesSession")
        Set aObj = Nothing
        IsLotus = True
'    End If
End If


Exit Function

GestError:
    IsLotus = False

End Function

Public Function InitMAPISession() As Boolean

'---> Cette proc�dure initialise la session MAPI

Dim aLb As Libelle

On Error GoTo ErrorLogon

'-> Ne pas cr�er de session MAPI s'il y en a d�ja eu une
If frmLib.MAPISession1.NewSession Then
    '-> Ne pas cr�er de session
Else
    With frmLib.MAPISession1
        .DownLoadMail = False '-> Ne pas charger les e-emails � l'ouverture de la session
        .LogonUI = True '-> Indique si une bo�te de dialogue est affich�e au moment de l'ouverture d'une session
        .SignOn '-> Connecte l'utilisateur au compte indiqu� par les propri�t�s UserName et Password.
        .NewSession = True '-> Indique qu'une session MAPI est en cours
        frmLib.MAPIMessages1.SessionID = .SessionID '-> Affecter au message l'ID de session MAPI
    End With
End If

InitMAPISession = True

Exit Function

ErrorLogon:

If Err.Number = 32003 Then
    '-> Ne rien faire : l'utilisateur a cliqu� sur annuler
Else
    '-> Pointer sur la classe message
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(10), vbCritical + vbOKOnly, aLb.GetCaption(10)
End If

InitMAPISession = False

End Function

Public Sub SendToInternet(ByVal FileToSend As String, Optional pBodyFile As String)

'---> Cette proc�dure envoie un e-mail sur Internet via le ctrl MAPI

Dim aLb As Libelle
Dim hdlBody As Integer
Dim Ligne As String

On Error GoTo GestError

'-> Intialiser la session MAPI
If Not InitMAPISession Then Exit Sub

'-> V�rifier la pr�sence du fichier � linker
If Dir$(FileToSend, vbNormal) = "" Then Exit Sub

'-> Cr�er un nouveau message
With frmLib.MAPIMessages1
    If .RecipAddress = "" Then
        .MsgIndex = -1
        .AddressEditFieldCount = 2
        .Compose
        .RecipAddress = Entry(1, NetParam, Chr(0))
        .ResolveName
        .MsgSubject = Entry(2, NetParam, Chr(0))
    Else
        .ResolveName
    End If
End With
    
'-> Corp du message
If Trim(Entry(3, NetParam, Chr(0))) = "" Then
    '-> V�rifier si un fichier BODY file existe
    If pBodyFile <> "" Then
        '-> V�rifier que le fichier existe
        If Dir$(pBodyFile, vbNormal) <> "" Then
            '-> Ouverture du fichier
            hdlBody = FreeFile
            Open pBodyFile For Input As #hdlBody
            Do While Not EOF(hdlBody)
                Line Input #hdlBody, Ligne
                If frmLib.MAPIMessages1.MsgNoteText = "" Then
                    frmLib.MAPIMessages1.MsgNoteText = Ligne
                Else
                    frmLib.MAPIMessages1.MsgNoteText = frmLib.MAPIMessages1.MsgNoteText & Chr(13) + Chr(10) & Ligne
                End If
            Loop
            '-> Fermer le fichier
            Close #hdlBody
        End If 'Si fichier bodyfile existe
    End If 'Si fichier bodyfile sp�cifi�
Else
    '-> Setting du body
    frmLib.MAPIMessages1.MsgNoteText = Entry(3, NetParam, Chr(0))
End If
    
'-> on laisse la place pour inserer les pieces jointes
frmLib.MAPIMessages1.MsgNoteText = Space(20) & vbCrLf & frmLib.MAPIMessages1.MsgNoteText
    
With frmLib.MAPIMessages1
    .AttachmentIndex = frmLib.MAPIMessages1.AttachmentCount
    .AttachmentPosition = frmLib.MAPIMessages1.AttachmentCount
    .AttachmentPathName = FileToSend
    .AttachmentType = 0
End With
    
'If frmLib.MAPIMessages1.MsgNoteText <> "" Then
'    frmLib.MAPIMessages1.AttachmentPosition = Len(frmLib.MAPIMessages1.MsgNoteText) - 1
'Else
'    frmLib.MAPIMessages1.AttachmentPosition = 0
'End If
    
frmLib.MAPIMessages1.Send False
    
Exit Sub

GestError:

    '-> Ne pas afficher de message d'erreur si on vient de la ligne de commande
    If OrigineMail = 1 Then
        MsgBox "Erreur pendant l'envoir du mail :" & Chr(13) & Err.Number & "  " & Err.Description, vbCritical + vbOKOnly, "Erreur"
    End If

End Sub


Public Sub SendToOutLook(ByVal FileToSend As String, Optional pParam As String)

'---> Cette proc�dure ouvre OutLook, cr�er un nouveau message , et joint le fichier Sp�cifi�

Dim aOutLook As Object
Dim aMail As Object
Dim ErrorCode As Integer
Dim aLb As Libelle
Dim i As Integer
Dim hdlBody As Integer
Dim Ligne As String
Dim oSess As Object
Dim oDB As Object
Dim oDoc As Object
Dim oItem As Object
Dim direct As Object
Dim Var As Variant
Dim flag As Boolean
Dim richStyle As Object
Dim AttachME As Object
Dim Bodyitem As Object
Dim Mime As Object
Dim MimeChild As Object
Dim Header As Object
Dim ENC_NONE As Integer
Dim Stream As Object

'-> pParam pour envoie depuis CDE avec
' TO + chr(0) + COPIES + chr(0) + OBJET + chr(0) + BODY + chr(0) + BODYFILE
On Error GoTo GestError

'-> V�rifier que le fichier existe
If Dir$(FileToSend, vbNormal) = "" Then
    ErrorCode = 3
    GoTo GestError
End If

If IsLotus Then
    '-> on initialse la session par defaut
    Set oSess = CreateObject("Notes.NotesSession")
    Set oDB = oSess.GETDATABASE("", "")
    Call oDB.OPENMAIL
    flag = True
    If Not (oDB.ISOPEN) Then flag = oDB.Open("", "")
    '-> on verifie si c'est bien ouvert
    If Not flag Then
        MsgBox "Can't open mail file: " & oDB.SERVER & " " & oDB.FilePath
        Exit Sub
    End If
    '-> initialisation du message
    Set oDoc = oDB.CREATEDOCUMENT
    
    'oSess.setConvertMIME = False
    '-> Setting du body
    Set Bodyitem = oSess.CreateStream
    
    '-> pour les envois en copy
    Dim arrNames() As String
    ReDim arrNames(NumEntries(Replace(Entry(2, pParam, Chr(0)), ";", ","), ",") - 1)
    '-> Afficher le corps du message
    If OrigineMail = 0 Then
        '-> Setting des param�tres
        oDoc.Subject = Entry(3, pParam, Chr(0))
        oDoc.sendto = Entry(1, pParam, Chr(0))
        '-> Pour lotus le separateur est la virgule
        For i = 0 To NumEntries(Replace(Entry(2, pParam, Chr(0)), ";", ","), ",") - 1
            arrNames(i) = Entry(i + 1, Replace(Entry(2, pParam, Chr(0)), ";", ","), ",")
        Next
        oDoc.copyto = arrNames
        'oDoc.copyto = Replace(Entry(2, pParam, Chr(0)), ";", ",")
        '-> Envoie depuis la page de visu
        If frmMail.RichTextBox1.Text <> "" Then
            Bodyitem.WriteText frmMail.RichTextBox1.Text
        Else
            Bodyitem.WriteText Entry(4, pParam, Chr(0))
        End If
    Else
        '-> Setting des param�tres
        oDoc.sendto = Entry(1, pParam, Chr(0))
        '-> Pour lotus le separateur est la virgule
        For i = 0 To NumEntries(Replace(Entry(2, pParam, Chr(0)), ";", ","), ",") - 1
            arrNames(i) = Entry(i + 1, Replace(Entry(2, pParam, Chr(0)), ";", ","), ",")
        Next
        oDoc.copyto = arrNames
        'oDoc.copyto = Replace(Entry(2, pParam, Chr(0)), ";", ",")
        oDoc.Subject = Entry(3, pParam, Chr(0))
        '-> Setting du body
        If Trim(Entry(4, pParam, Chr(0))) = "" Then
            '-> V�rifier si un fichier pour body est donn�
            If Trim(Entry(5, pParam, Chr(0))) <> "" Then
                If Dir$(Trim(Entry(5, pParam, Chr(0))), vbNormal) <> "" Then
                    '-> Ouvrir le fichier body
                    hdlBody = FreeFile
                    Open Trim(Entry(5, pParam, Chr(0))) For Input As #hdlBody
                    Do While Not EOF(hdlBody)
                        '-> Lecture de la ligne
                        Line Input #hdlBody, Ligne
                        '-> Ajout dans le body du text
                        Bodyitem.WriteText Ligne
                    Loop
                    '-> Fermer le fichier
                    Close #hdlBody
                End If 'Si fichier body existe
            End If 'Si fichier body sp�cifi�
        Else
            Bodyitem.WriteText Entry(4, pParam, Chr(0))
        End If 'Si pas de body sp�cifi�
    End If 'Selon l'origine du mail
    
    '-> on met le corps du texte au bon format
    Set Mime = oDoc.CreateMIMEEntity '("Body")
    Set Header = Mime.CreateHeader("Content-Type")
    Call Header.SetHeaderVal("multipart/mixed")
    
    Set MimeChild = Mime.CreateChildEntity
    MimeChild.SetContentFromText Bodyitem, "text/html", ENC_NONE
    Bodyitem.Close
    Bodyitem.Truncate
    
    Set MimeChild = Mime.CreateChildEntity

    Set Header = MimeChild.CreateHeader("Content-Disposition")
    Call Header.setHeaderValAndParams("attachment; filename=" & GetFileName(FileToSend))

    Set Stream = oSess.CreateStream
    If Not Stream.Open(FileToSend, "binary") Then
        'Print "somefile open failed"
        Exit Sub
    End If
    If Stream.Bytes = 0 Then
        Exit Sub
    End If
    Call MimeChild.SetContentFromBytes(Stream, "application/turbo", 1730)
    Call Stream.Close
    Call Stream.Truncate
    
    '-> si il y a des fichiers joints lorsque l on vient d'un fichier zipp�
    For i = 0 To MDIMain.mnuJoin.Count - 1
        If MDIMain.mnuJoin.Item(i).Visible Then
            If Trim(MDIMain.mnuJoin.Item(i).Tag) <> "" Then AttachME.EMBEDOBJECT 1454, "", MDIMain.mnuJoin.Item(i).Tag, "Attachment"
        End If
    Next
        
    '-> mettre le mail dans la boite d'envoi
    oDoc.PostedDate = Now()
    oDoc.SAVEMESSAGEONSEND = True
     
    '-> Afficher le mail
    If SendOutLook Then
        oDoc.Send False
    Else
        oDoc.visable = True
        oDoc.Send False
    End If
        
    '-> Lib�rer les pointeurs
    Set oSess = Nothing
    Set oDB = Nothing
    Set oDoc = Nothing
    Set oItem = Nothing
    Set Mime = Nothing
    Set Bodyitem = Nothing
    Exit Sub
Else
    '-> Obtenir un pointeur vers OutLook
    ErrorCode = 1
    Set aOutLook = CreateObject("Outlook.Application")
    
    '-> Cr�er un nouveau message
    ErrorCode = 2
    Set aMail = aOutLook.createitem(0)
    
    '-> Afficher le corps du message
    If OrigineMail = 0 Then
        '-> Envoie depuis la page de visu
        If frmMail.RichTextBox1.Text <> "" Then aMail.body = frmMail.RichTextBox1.Text
        '-> Setting des param�tres
        aMail.To = Entry(1, pParam, Chr(0))
        aMail.CC = Entry(2, pParam, Chr(0))
        aMail.Subject = Entry(3, pParam, Chr(0))
        aMail.body = Entry(4, pParam, Chr(0))
    Else
        '-> Setting des param�tres
        aMail.To = Entry(1, pParam, Chr(0))
        aMail.CC = Entry(2, pParam, Chr(0))
        aMail.Subject = Entry(3, pParam, Chr(0))
        '-> Setting du body
        If Trim(Entry(4, pParam, Chr(0))) = "" Then
            '-> V�rifier si un fichier pour body est donn�
            If Trim(Entry(5, pParam, Chr(0))) <> "" Then
                If Dir$(Trim(Entry(5, pParam, Chr(0))), vbNormal) <> "" Then
                    '-> Ouvrir le fichier body
                    hdlBody = FreeFile
                    Open Trim(Entry(5, pParam, Chr(0))) For Input As #hdlBody
                    Do While Not EOF(hdlBody)
                        '-> Lecture de la ligne
                        Line Input #hdlBody, Ligne
                        '-> Ajout dans le body du text
                        If aMail.body <> "" Then
                            aMail.body = aMail.body & Chr(13) + Chr(10) & Ligne
                        Else
                            aMail.body = Ligne
                        End If
                    Loop
                    '-> Fermer le fichier
                    Close #hdlBody
                End If 'Si fichier body existe
            End If 'Si fichier body sp�cifi�
        Else
            aMail.body = Entry(4, pParam, Chr(0))
        End If 'Si pas de body sp�cifi�
    End If
    
    '-> Attacher le fichier sp�cifi�
    ErrorCode = 3
    aMail.Attachments.add FileToSend
    
    '-> si il y a des fichiers joints lorsque l on vient d'un fichier zipp�
    For i = 0 To MDIMain.mnuJoin.Count - 1
        If MDIMain.mnuJoin.Item(i).Visible Then
            If Trim(MDIMain.mnuJoin.Item(i).Tag) <> "" Then aMail.Attachments.add MDIMain.mnuJoin.Item(i).Tag
        End If
    Next
        
    '-> Afficher le mail
    ErrorCode = 4
    If Not SendOutLook Then
        aMail.Send
    Else
        aMail.Display
    End If
    
    '-> Lib�rer les pointeurs
    Set aMail = Nothing
    Set aOutLook = Nothing
    
    Exit Sub
End If

GestError:

    '-> N'afficher les messages d'erreur que si on est en visu
    If OrigineMail = 0 Then
    
        '-> Pointer sur la classe libelle
        Set aLb = Libelles("MESSAGE")
                     
        '-> Erreur lors de la connexion : envoyer un message d'erreur
        Select Case ErrorCode
            Case 1
                '-> Afficher un message d'erreur
                MsgBox aLb.GetCaption(4), vbCritical + vbOKOnly, aLb.GetToolTip(4)
            Case 2
                '-> Afficher un message d'erreur
                MsgBox aLb.GetCaption(5), vbCritical + vbOKOnly, aLb.GetToolTip(5)
                '-> Lib�rer le pointeur d'outlook
                Set aOutLook = Nothing
            Case 3
                '-> Afficher un message d'erreur
                MsgBox Replace(aLb.GetCaption(6), "$FICHIER$", FileToSend), vbCritical + vbOKOnly, aLb.GetToolTip(6)
                '-> Lib�rer le pointeur d'outlook
                Set aMail = Nothing
                Set aOutLook = Nothing
        
        End Select
    End If

End Sub


Public Sub CreateFileToSend()

'---> Cette fonction cr�er le fichier � linker pour export OutLook ou Internet
Dim aFichier As Fichier
Dim aSpool As Spool
Dim SpoolName As String

On Error GoTo GestError

'-> Pointer sur l'objet Fichier
Set aFichier = Fichiers(FileKeyMail)

'-> Pointer sur 'objet spool
If SpoolKeyMail <> "" Then Set aSpool = aFichier.Spools(SpoolKeyMail)
    
'-> Analyse selon l'origine
Select Case ExportMail

    Case 0, 1
        If ExportMail = 0 Then
            '-> Envoie de la page en cours
            SpoolName = CreateDetail(aSpool, PageNumMail)
        Else
            '-> Envoie du spool en cours
            SpoolName = CreateDetail(aSpool, -1)
        End If
        
        If OrigineMail = 0 Or OrigineMail = 2 Then
            '-> Envoie dans OutLook
            SendToOutLook SpoolName
        Else
            '-> Envoie dans Internet via MAPI
            SendToInternet SpoolName
        End If
        
    Case 2 '-> Envoie du fichier en cours
        If OrigineMail = 0 Or OrigineMail = 2 Then
            '-> Envoie du spool en entier vers OutLook
            SendToOutLook aFichier.FileName, frmMail.Text2.Text & Chr(0) & frmMail.Text3.Text & Chr(0) & frmMail.Text1.Text & Chr(0) & frmMail.RichTextBox1.Text & Chr(0)
        Else
            '-> Envoie du spool vers Internet
            SendToInternet aFichier.FileName
        End If
    
End Select

GestError:


End Sub

Public Function CreateDetail(ByRef aSpool As Spool, ByVal Page As Integer, Optional PageMin As Integer, Optional PageMax As Integer) As String

'---> Cette fonction cr�er un fichier tempo et retourne le nom du fichier � linker

Dim TempFile As String
Dim hdlFile As Integer
Dim DefMaq As String
Dim i As Integer
Dim PageMini As Integer
Dim PageMaxi As Integer

On Error GoTo GestError

'-> Obtenir un nom de fichier tempotaire
TempFile = Year(Now) & "-" & FORMAT(Month(Now), "00") & "-" & FORMAT(Day(Now), "00") & "-" & FORMAT(Hour(Now), "00") & "-" & FORMAT(Minute(Now), "00") & "-" & FORMAT(Second(Now), "00") & ".turbo"
TempFile = GetTempFileNameVB("WWW", True) & TempFile

'-> Obtenir un handle de fichier
hdlFile = FreeFile

'-> Ouverture du fichier ascii et �criture des pages
Open TempFile For Output As #hdlFile

'-> Ecriture de la balise Spool
If IsCryptedFile Then
    Print #hdlFile, Crypt("[SPOOL]")
Else
    Print #hdlFile, "[SPOOL]"
End If

'-> Il faut dans un premier temps �crire la maquette
If IsCryptedFile Then
    Print #hdlFile, Crypt("[MAQ]")
Else
    Print #hdlFile, "[MAQ]"
End If

'-> R�cup�ration de la maquette
DefMaq = aSpool.GetMaq

'-> Impression de la maquette
For i = 1 To NumEntries(DefMaq, Chr(0))
    If IsCryptedFile Then
        Print #hdlFile, Crypt(Entry(i, DefMaq, Chr(0)))
    Else
        Print #hdlFile, Entry(i, DefMaq, Chr(0))
    End If
Next 'Pour toutes les lignes de la maquette

'-> Tag de fin de maquette
If IsCryptedFile Then
    Print #hdlFile, Crypt("[/MAQ]")
Else
    Print #hdlFile, "[/MAQ]"
End If

'-> Impression du spool en entier
If Page = -1 Or Page = -2 Then
    If Page = -1 Then
        '-> Indiquer la premi�re page � imprimer
        If aSpool.IsSelectionPage = True Then
            '-> indiquer qu'il faut impimer � partir de la page 0
            PageMini = 0
        Else
            '-> Imprimer � partir de la page 1
            PageMini = 1
        End If
        
        '-> Indiquer la page maxi
        PageMaxi = aSpool.NbPage
        
    Else
        '-> Indiquer la premi�re page � imprimer
        PageMini = PageMin
        PageMaxi = PageMax
        
    End If
    '-> On imprimer toutes les pages
    For i = PageMini To PageMaxi
        '-> Imprimer la tag d'ouverture de la page de s�lection
        If i = 0 Then
            If IsCryptedFile Then
                Print #hdlFile, Crypt("[GARDEOPEN]")
            Else
                Print #hdlFile, "[GARDEOPEN]"
            End If
        End If
        
        '-> Impression de la page
        PrintPageToSpool aSpool, hdlFile, i
        
        '-> Imprimer le tag de fin de s�lection si page = 0
        If i = 0 Then
            If IsCryptedFile Then
                Print #hdlFile, Crypt("[GARDECLOSE]")
            Else
                Print #hdlFile, "[GARDECLOSE]"
            End If
        '-> Imprimer un saut de page si pas derni�re page
        ElseIf i <> aSpool.NbPage Then
            If IsCryptedFile Then
                Print #hdlFile, Crypt("[PAGE]")
            Else
                Print #hdlFile, "[PAGE]"
            End If
        End If
    Next
Else
    '-> Imprimer la tag de la page de s�lection
    If Page = 0 Then
        If IsCryptedFile Then
            Print #hdlFile, Crypt("[GARDEOPEN]")
        Else
            Print #hdlFile, "[GARDEOPEN]"
        End If
    End If

    '-> Imprimer que la page d�sir�e
    PrintPageToSpool aSpool, hdlFile, Page
    
    '-> Imprimer la tag de fermture de la page de s�lection
    If Page = 0 Then
        If IsCryptedFile Then
            Print #hdlFile, Crypt("[GARDECLOSE]")
        Else
            Print #hdlFile, "[GARDECLOSE]"
        End If
    End If
    
End If

'-> Tag de fin de spool
If IsCryptedFile Then
    Print #hdlFile, Crypt("[/SPOOL]")
Else
    Print #hdlFile, "[/SPOOL]"
End If

'-> Fermer le fichier ouvert
Close #hdlFile

'-> Renvoyer le nom du fichier
CreateDetail = TempFile

Exit Function

GestError:

    '-> Renovyer une valeur ""
    CreateDetail = ""

End Function

Private Sub PrintPageToSpool(ByRef aSpool As Spool, ByVal hdlFile As Integer, ByVal pageToPrint As Integer)

'---> Cette fonction imprime le contenu d'une page d'un spool

Dim i As Integer
    
Dim DefPage As String

'-> Recup�ration de la d�finition de la page
DefPage = aSpool.GetPage(pageToPrint)

'-> Impression de la page
For i = 1 To NumEntries(DefPage, Chr(0))
    If IsCryptedFile Then
        Print #hdlFile, Crypt(Entry(i, DefPage, Chr(0)))
    Else
        Print #hdlFile, Entry(i, DefPage, Chr(0))
    End If
Next

End Sub

Public Sub DeleteFile(FileToDelete As String, ActionFlag As Long)
 
'---> Cette proc�dure supprime un fichier

Dim SHFileOp As SHFILEOPSTRUCT
Dim Res As Long
 
'-> Ajouter un caractere de fin de string
FileToDelete = FileToDelete & Chr$(0)
 
'-> Setting de la structure
With SHFileOp
   .wFunc = FO_DELETE
   .pFrom = FileToDelete
   .fFlags = ActionFlag
End With
 
'-> suppression du fichier
Res = SHFileOperation(SHFileOp)

'-> Si on n' a pas fait annuler : virer le fichier de l'interface
If SHFileOp.fAborted = False Then CloseFichier MDIMain.TreeNaviga.SelectedItem.Key

End Sub

Public Sub DrawTempo(aPic As PictureBox)

aPic.Line (0, 0)-((TailleLue / TailleTotale) * aPic.ScaleWidth, aPic.ScaleHeight), &HC00000, BF

End Sub

Public Function IsTextInPage(ByVal aSpool As Spool, ByVal i As Integer) As Boolean
'--> on regarde si le texte recherch� est present sur la page
Dim strPage As String
Dim strTemp As String

On Error GoTo GestError

'-> on reccupere la page
strPage = aSpool.GetPage(i)

'-> premiere verification sans tenir compte des caracteres parasites
If InStr(1, strPage + aSpool.GetMaq, Replace(FindText, ",", "."), vbTextCompare) <> 0 Then IsTextInPage = True
If InStr(1, strPage + aSpool.GetMaq, Replace(FindText, ".", ","), vbTextCompare) <> 0 Then IsTextInPage = True
If InStr(1, strPage + aSpool.GetMaq, FindText, vbTextCompare) <> 0 Then IsTextInPage = True
If InStr(1, Replace(strPage, ",", ""), FindText, vbTextCompare) <> 0 Then IsTextInPage = True

GestError:

End Function

Private Function GetNewFindText(TextToFind As String, CellContenu As String, NbOccurence As Integer) As String
'--> cette fonction retravaille le texte recherch� car il peu contenir un separateur de millier
Dim i As Integer
Dim j As Integer
Dim k As Integer
Dim TempCell As String
Dim car As String
Dim find As String

'-> si pas present quitter
If InStr(1, CellContenu, SepMil) = 0 Then GoTo GestError

TextToFind = UCase(TextToFind)
CellContenu = UCase(CellContenu)

'-> on initialise le premier caractere a rechercher
j = 1
find = Mid(TextToFind, j, 1)

'-> on reconstruit la chaine � rechercher en lisant le contenu
For i = 1 To Len(CellContenu)
    car = Mid(CellContenu, i, 1)
    Select Case car
        Case find
            TempCell = TempCell + car
            '-> on regarde si on a fini
            If Replace(TempCell, SepMil, "") = TextToFind Then
                GetNewFindText = TempCell
                Exit Function
            End If
            j = j + 1
            find = Mid(TextToFind, j, 1)
        Case SepMil
            TempCell = TempCell + SepMil
        Case Else '-> c'est pas bon
            j = 1
            TempCell = ""
    End Select
Next
Exit Function
GestError:
GetNewFindText = TextToFind
End Function

Private Sub DrawFindText(TextToFind As String, pDraw, Lrect As Long, Rrect As Long, Trect As Long, Brect As Long, aCell)

'---> Cette fonction imprime le texte recherch� sous forme RTF

Dim aRtf As RichTextBox
Dim fr As FORMATRANGE
Dim lTextOut As Long, lTextAmt As Long, Res As Long
Dim aRect As RECT
Dim hdlPen As Long
Dim OldPen As Long
Dim hdlBrush As Long
Dim oldBrush As Long
Dim i As Long
Dim DifH As Integer
Dim DifW As Integer
Dim hdlRgn As Long

On Error GoTo GestErr

'-> Mettre l'�diteur RTF aux bonnes dimensions
Load frmLib.Rtf(IdRTf + 1)
Set aRtf = frmLib.Rtf(IdRTf + 1)

aRtf.Width = Sortie.ScaleX(Rrect - Lrect, 3, 1)
aRtf.Height = Sortie.ScaleX(Brect - Trect, 3, 1)
aRtf.Font.Name = aCell.FontName
aRtf.Font.Size = aCell.FontSize * Zoom
aRtf.Font.Bold = aCell.FontBold
aRtf.Font.Italic = aCell.FontItalic
aRtf.Font.Underline = aCell.FontUnderline
aRtf.BackColor = aCell.BackColor

''-> on bute eventuellement les blancs � dorite
'TextToFind = RTrim(TextToFind)

Select Case aCell.CellAlign
    Case 3, 6, 9
        '-> R�cup�ration du code RTF sans les blancs de droite
        aRtf.Text = RTrim(aCell.ContenuCell)
    Case Else
        '-> R�cup�ration du code RTF
        aRtf.Text = aCell.ContenuCell
End Select
'-> on pointe sur la premiere occurence du texte recherch�
aRtf.SelStart = aRtf.find(TextToFind)
'-> Dans le cas ou on a plusieurs occurences, selectionner le bon texte
Do While FindPos2 <= FindPos
    FindPos2 = FindPos2 + 1
    aRtf.SelStart = aRtf.find(TextToFind, aRtf.SelStart + Len(TextToFind))
Loop

'-> On met en evidence le texte recherch�
aRtf.SelLength = Len(TextToFind)
aRtf.SelColor = vbWhite
Call SetBackColorSel(aRtf.hwnd, vbBlack)

'-> Setting du rect de dessin
aRect.Left = Lrect
aRect.Top = Trect
aRect.Right = Rrect
aRect.Bottom = Brect

'-> dans le cas de l'alignement droite l'api vire les blancs de droite
Select Case aCell.CellAlign
    Case 3, 6, 9
        '-> On calcul les coordonn�es exactes du texte
        DrawText Sortie.hDC, RTrim(aCell.ContenuCell), Len(RTrim(aCell.ContenuCell)), aRect, DT_CALCRECT 'Or pDraw
        i = aRect.Right
        aRect.Left = Lrect
        aRect.Top = Trect
        aRect.Right = Rrect
        aRect.Bottom = Brect
        '-> On calcul les coordonn�es exactes du texte
        DrawText Sortie.hDC, aCell.ContenuCell, Len(aCell.ContenuCell), aRect, DT_CALCRECT 'Or pDraw
        i = aRect.Right - i
        '-> on calcul les differences de taille
        DifH = (Brect - Trect) - (aRect.Bottom - aRect.Top)
        DifW = (Rrect - Lrect) - (aRect.Right - aRect.Left)
        aRect.Top = Trect
        aRect.Bottom = Brect
    Case Else
        '-> On calcul les coordonn�es exactes du texte
        DrawText Sortie.hDC, aCell.ContenuCell, Len(aCell.ContenuCell), aRect, DT_CALCRECT Or pDraw
        '-> on calcul les differences de taille
        DifH = (Brect - Trect) - (aRect.Bottom - aRect.Top) '- 1
        DifW = (Rrect - Lrect) - (aRect.Right - aRect.Left)
End Select

Select Case aCell.CellAlign
    Case 1 'Left Top
        aRtf.SelAlignment = 0
    Case 2 'Center Top
        aRtf.SelAlignment = 2
    Case 3 'Right Top
        aRtf.SelAlignment = 1
        Rrect = Rrect - i
    Case 4 'Left Center
        aRtf.SelAlignment = 0
        Lrect = Lrect + i
        Trect = Trect + DifH  '/2'+ 1
        Rrect = Rrect + i + 100
        Brect = Brect + DifH
    Case 5 'Center Center
        aRtf.SelAlignment = 2
        Trect = Trect + DifH
        Brect = Brect + DifH
        Lrect = Lrect - 1 '- DifW - 1
        Rrect = Rrect + 1 '+ DifW + 1
    Case 6 'Right Center
        aRtf.SelAlignment = 1
        Trect = Trect + DifH / 2
        Rrect = Rrect - i
        Lrect = Lrect - i
    Case 7 'Left Bottom
        aRtf.SelAlignment = 0
        Trect = Trect + DifH
    Case 8 'Center Bottom
        aRtf.SelAlignment = 2
        Trect = Trect + DifH
    Case 9 'Right Bottom
        aRtf.SelAlignment = 1
        Rrect = Rrect - i
        Trect = Trect + DifH
End Select

'-> Imprimer le fond de l'objet
hdlBrush = CreateSolidBrush(Sortie.BackColor)
hdlPen = CreatePen(PS_NULL, 1, QBColor(15))

'-> S�lection du pinceau dans le Contexte
oldBrush = SelectObject(Sortie.hDC, hdlBrush)
OldPen = SelectObject(Sortie.hDC, hdlPen)

'-> S�lectionner les anciens objets et supprimer les objets GDI non utilis�s
If OldPen <> 0 Then
    SelectObject Sortie.hDC, OldPen
    DeleteObject hdlPen
End If

If oldBrush <> 0 Then
    SelectObject Sortie.hDC, oldBrush
    DeleteObject hdlBrush
End If

'-> Initialiser la srtucture Formatrange
fr.hDC = Sortie.hDC
fr.hdcTarget = Sortie.hDC
fr.chrg.cpMin = 0
fr.chrg.cpMax = -1

'-> Intialisation du rectangle destination
fr.rc.Left = Sortie.ScaleX(Lrect, 3, 1)  'PosX
fr.rc.Top = Sortie.ScaleY(Trect, 3, 1)  'posY
fr.rc.Right = Sortie.ScaleX(Rrect, 3, 1)  '- Lrect + PosX
fr.rc.Bottom = Sortie.ScaleY(Brect, 3, 1)  '- Trect + posY

'-> Initialisation du rectangle de source
fr.rcPage.Left = 0
fr.rcPage.Top = 0
fr.rcPage.Right = Sortie.ScaleWidth 'Rrect - Lrect + PosX
fr.rcPage.Bottom = Sortie.ScaleHeight 'Brect + Trect + posY

'-> Faire un setting du mode de restitution
'Res = SetMapMode(Sortie.hdc, MM_TEXT)

'-> initialisation des variables de pointage de texte
lTextOut = 0
lTextAmt = SendMessage(aRtf.hwnd, WM_GETTEXTLENGTH, 0, 0)

'-> Impression du Rtf
Do While lTextOut < lTextAmt
    lTextOut = SendMessage(aRtf.hwnd, EM_FORMATRANGE, True, fr)
    If lTextOut < lTextAmt Then
        fr.chrg.cpMin = lTextOut
        fr.chrg.cpMax = -1
    End If
Loop

'-> Lib�rer la ressource associ�e au RTF : VERSION COMPILEE
Res = SendMessage(aRtf.hwnd, EM_FORMATRANGE, -1, Null)
If Res = 0 Then
    'MsgBox "Erreur dans la lib�ration du context"
    Res = SendMessage(aRtf.hwnd, EM_FORMATRANGE, -1, vbNullString)
    'MsgBox "Apr�s Seconde tentative " & Res
End If

'-> Lib�rer le pointeur sur le controle RTF
Set aRtf = Nothing

GestErr:
Unload frmLib.Rtf(IdRTf + 1)
End Sub

Private Function GetBackColorSel(ByVal RichHwnd As Long) As OLE_COLOR
    Dim iniformat As FORMAT

    ' Set BackColor a masqu�
    iniformat.dwMask = CFM_BACKCOLOR
    iniformat.cbSize = Len(iniformat)
    ' Obtenez la structure du format des caract�res s�lectionner
    SendMessage RichHwnd, EM_GETCHARFORMAT, SCF_SELECTION, iniformat
    
    ' Retour le BackColour de la s�lection
    GetBackColorSel = iniformat.crBackColor
End Function

Private Function SetBackColorSel(ByVal RichHwnd As Long, ByVal NouveauFontBackColorSel As OLE_COLOR)
    Dim iniformat As FORMAT
    ' Set BackColor a masqu�
    iniformat.dwMask = CFM_BACKCOLOR
    ' Si le nouveau backcolour est mis � -1 alors nous avons mis le
    ' Backcolour RichTextbox a zero (vbwhite)
    If NouveauFontBackColorSel = -1 Then
        iniformat.dwEffects = CFE_AUTOBACKCOLOR
        iniformat.crBackColor = -1
    Else
    ' donner la nouvelle couleur � BackColour
        iniformat.crBackColor = ChangerColor(NouveauFontBackColorSel)
    End If
    ' Nous avons besoin de passer la dimension de la structure comme un
    ' partie de la structure.
    iniformat.cbSize = Len(iniformat)
    ' Envoyez le message et le nouveau format de caract�re au RichTextbox
    SendMessage RichHwnd, EM_SETCHARFORMAT, SCF_SELECTION, iniformat
End Function

Private Function ChangerColor(ByVal Couleur As OLE_COLOR, Optional Palette As Long = 0) As Long
    If OleTranslateColor(Couleur, Palette, ChangerColor) Then
        ChangerColor = -1
    End If
End Function

Public Sub CmdDeZoom(Yvalue As Integer, Xvalue As Integer)
    Dim l As Long
    l = StretchBlt(Sortie.hDC, 0, 0, Xvalue, Yvalue, Sortie.hDC, 0, 0, Sortie.ScaleWidth, Sortie.ScaleHeight, ScrCopy)
    Sortie.Refresh
End Sub

Public Function GetTitrePage(aSpool As Spool, NumPage As Integer) As String
'--> Cette fonction r�cup�re le titre d'une page
Dim ParamTitre As String
Dim Ligne As String
Dim Rg As String
Dim Dz As String
Dim i As Integer
Dim j As Integer


'-> on regarde si on a specifi� un titre
If InStr(1, aSpool.GetMaq, "[TITRE]") = 0 Then Exit Function

'-> on r�cup�re le param�trage
ParamTitre = Entry(2, aSpool.GetMaq, "[TITRE]")
ParamTitre = Entry(1, ParamTitre, "\TITRE�End")
ParamTitre = Mid(ParamTitre, 8)

'-> on parcours les titres � r�cuperer
For i = 2 To NumEntries(ParamTitre, "\")
    Rg = Entry(i, ParamTitre, "\")
    Dz = "^" + Entry(1, Entry(2, Rg, "�"), Chr(0))
    Rg = "[" + Entry(1, Rg, "�") + "]"
    '-> on recherche la ligne correspondante
    For j = 1 To NumEntries(aSpool.GetPage(NumPage), Chr(0))
        Ligne = Trim(Entry(j, aSpool.GetPage(NumPage), Chr(0)))
        If InStr(1, UCase(Ligne), UCase(Rg)) Then
            '-> on regarde si on trouve le diez
            If InStr(1, UCase(Ligne), UCase(Rg)) <> 0 And InStr(1, Ligne, Dz) Then
                '-> on est sur une bonne ligne on r�cup�re la valeur du diez
                Dz = RTrim(Mid(Entry(1, Entry(1, Entry(2, Ligne, Dz), "^"), "}"), 5))
                If InStr(1, Dz, "[\") <> 0 Then Dz = ""
                If GetTitrePage = "" Then
                    GetTitrePage = Dz
                Else
                    GetTitrePage = GetTitrePage + " - " + Dz
                End If
                '-> sortir de la boucle
                Exit For
            End If
        End If
    Next
Next

End Function

Public Function GetTitreMail(aSpool As Spool, NumPage As Integer) As String
'--> Cette fonction r�cup�re le titre d'une page
Dim ParamTitre As String
Dim Ligne As String
Dim Rg As String
Dim Dz As String
Dim i As Integer
Dim j As Integer
Dim topOK As Boolean

'-> on regarde si on a specifi� un titre
If InStr(1, aSpool.GetMaq, "[MAIL]") = 0 Then
    GetTitreMail = "[NO]"
    Exit Function
End If

'-> on r�cup�re le param�trage
ParamTitre = Entry(2, aSpool.GetMaq, "[MAIL]")
ParamTitre = Entry(1, ParamTitre, "\MAIL�End")
ParamTitre = Mid(ParamTitre, 8)

topOK = False

'-> on parcours les titres � r�cuperer
For i = 1 To NumEntries(ParamTitre, "\")
    Rg = Entry(i, ParamTitre, "\")
    Dz = "^" + Entry(1, Entry(2, Rg, "�"), Chr(0))
    Rg = "[" + Entry(1, Rg, "�") + "]"
    '-> on recherche la ligne correspondante
    For j = 1 To NumEntries(aSpool.GetPage(NumPage), Chr(0))
        Ligne = Trim(Entry(j, aSpool.GetPage(NumPage), Chr(0)))
        If InStr(1, UCase(Ligne), UCase(Rg)) Then
            '-> on regarde si on trouve le diez
            If InStr(1, UCase(Ligne), UCase(Rg)) <> 0 And InStr(1, Ligne, Dz) Then
                '-> on est sur une bonne ligne on r�cup�re la valeur du diez
                topOK = True
                Dz = RTrim(Mid(Entry(1, Entry(1, Entry(2, Ligne, Dz), "^"), "}"), 5))
                If InStr(1, Dz, "[\") <> 0 Then Dz = ""
                If GetTitreMail = "" Then
                    GetTitreMail = Dz
                Else
                    GetTitreMail = GetTitreMail + " - " + Dz
                End If
                '-> sortir de la boucle
                Exit For
            End If
        End If
    Next
Next

If topOK = False Then GetTitreMail = "[NO]"
End Function

Public Function GetLibelVariableForCell(aBlock As Block, aSpool As Spool, aCell As Cellule) As String
'--> Cette fonction r�cup�re les libell�s variables d'un spool
Dim ParamTitre As String
Dim Ligne As String
Dim Rg As String
Dim Dz As String
Dim i As Integer
Dim j As Integer


'-> on regarde si on a specifi� un titre
If InStr(1, aSpool.GetMaq, "[LIBELVARIABLE]") = 0 Then Exit Function

'-> on r�cup�re le param�trage
ParamTitre = Entry(2, aSpool.GetMaq, "[LIBELVARIABLE]")
ParamTitre = Entry(1, ParamTitre, "\LIBELVARIABLE�End")
ParamTitre = Mid(ParamTitre, 16)

'-> on parcours les titres � r�cuperer
For i = 2 To NumEntries(ParamTitre, "\")
    Rg = Entry(i, ParamTitre, "\")
    Dz = "^" + Entry(1, Entry(2, Rg, "�"), Chr(0))
    Rg = "[" + Entry(1, Rg, "�") + "]"
    If UCase("[TB-" + aBlock.NomTb + "(BLK-" + aBlock.Nom + ")]") = UCase(Rg) Then
        If aCell.LibelVariable = "" Then
            aCell.LibelVariable = "^" + Entry(2, Entry(i, ParamTitre, "\"), "�") & "�" & Entry(3, Entry(i, ParamTitre, "\"), "�")
        Else
            aCell.LibelVariable = aCell.LibelVariable + "�^" + Entry(2, Entry(i, ParamTitre, "\"), "�") & "�" & Entry(3, Entry(i, ParamTitre, "\"), "�")
        End If
    End If
Next

End Function

Public Function GetLibelVariableForSection(aSpool As Spool, aSection As Section) As String
'--> Cette fonction r�cup�re les libell�s variables d'un spool
Dim ParamTitre As String
Dim Ligne As String
Dim Rg As String
Dim Dz As String
Dim i As Integer
Dim j As Integer


'-> on regarde si on a specifi� un titre
If InStr(1, aSpool.GetMaq, "[LIBELVARIABLE]") = 0 Then Exit Function

'-> on r�cup�re le param�trage
ParamTitre = Entry(2, aSpool.GetMaq, "[LIBELVARIABLE]")
ParamTitre = Entry(1, ParamTitre, "\LIBELVARIABLE�End")
ParamTitre = Mid(ParamTitre, 16)

'-> on parcours les titres � r�cuperer
For i = 2 To NumEntries(ParamTitre, "\")
    Rg = Entry(i, ParamTitre, "\")
    Dz = "^" + Entry(1, Entry(2, Rg, "�"), Chr(0))
    Rg = "[" + Entry(1, Rg, "�") + "]"
    If UCase("[NV-" + aSection.Nom + "-STD(TXT-Section)]") = UCase(Rg) Then
        If aSection.LibelVariable = "" Then
            aSection.LibelVariable = "^" + Entry(2, Entry(i, ParamTitre, "\"), "�") & "�" & Entry(3, Entry(i, ParamTitre, "\"), "�")
        Else
            aSection.LibelVariable = aSection.LibelVariable + "�^" + Entry(2, Entry(i, ParamTitre, "\"), "�") & "�" & Entry(3, Entry(i, ParamTitre, "\"), "�")
        End If
    End If
Next

End Function

Public Function IsOpenoffice() As Boolean
'--> Cette proc�dure essaye de cr�er un lien OLE vers Openoffice pour tester que les bibli sont install�es
Dim aServiceManager As Object

On Error GoTo OfficeError

'-> Essayer de cr�er un lien OLE
Set aServiceManager = CreateObject("com.sun.star.ServiceManager")

'-> Lib�rer le pointeur
Set aServiceManager = Nothing

'-> Renvoyer une valeur de succ�s
IsOpenoffice = True

Exit Function

OfficeError:
    '-> Si erreur : pas openoffice
    IsOpenoffice = False

End Function

Public Function LoadJoinFile(JoinFich As String, FileName As String)
'--> cette procedure charge les fichiers joints au menu fenetre
Dim i As Integer
Dim Rep As String
Dim aSpool As Spool

On Error GoTo GestError

'-> on r�initialise
For i = 1 To MDIMain.mnuJoin.Count
    MDIMain.mnuJoin.Item(i - 1).Visible = False
    Fichiers(FileName).Spools(1).frmDisplay.Toolbar1.Buttons("FILEJOIN").ButtonMenus(i).Visible = False
Next
'-> on cache le separateur
MDIMain.mnusep4.Visible = False
For Each aSpool In Fichiers(FileName).Spools
    If Not aSpool.frmDisplay Is Nothing Then aSpool.frmDisplay.Toolbar1.Buttons("FILEJOIN").Visible = False
    If Not aSpool.frmDisplay Is Nothing Then aSpool.frmDisplay.Toolbar1.Buttons("FILEJOIN").Enabled = False
Next

'-> si rien quitter
If JoinFich = "" Then Exit Function

'-> si qque chose rendre le boutton des pieces jointes actif
For Each aSpool In Fichiers(FileName).Spools
    If Not aSpool.frmDisplay Is Nothing Then aSpool.frmDisplay.Toolbar1.Buttons("FILEJOIN").Visible = True
    If Not aSpool.frmDisplay Is Nothing Then aSpool.frmDisplay.Toolbar1.Buttons("FILEJOIN").Enabled = True
Next
'-> on parcourt les fichiers joints
For i = 1 To NumEntries(JoinFich, ",")
    '-> on se limite � 25
    If i > 25 Then
        MDIMain.mnuJoin.Item(i - 1).Visible = True
        MDIMain.mnuJoin.Item(i - 1).Caption = "...  "
        For Each aSpool In Fichiers(FileName).Spools
            If Not aSpool.frmDisplay Is Nothing Then
                aSpool.frmDisplay.Toolbar1.Buttons("FILEJOIN").ButtonMenus(i).Visible = True
                aSpool.frmDisplay.Toolbar1.Buttons("FILEJOIN").ButtonMenus(i).Text = "..."
            End If
        Next
        '-> quitter la boucle
        Exit For
    End If
    
    '-> on pointe sur le fichier en verifiant qu'il existe bien
    Rep = Dir(Entry(i, JoinFich, ","))
    If Rep <> "" Then
        MDIMain.mnusep4.Visible = True
        MDIMain.mnuJoin.Item(i - 1).Visible = True
        MDIMain.mnuJoin.Item(i - 1).Caption = i & "  " & Rep
        MDIMain.mnuJoin.Item(i - 1).Tag = Entry(i, JoinFich, ",")
    End If
    '-> on s'occupe ici du menu
    For Each aSpool In Fichiers(FileName).Spools
        If Not aSpool.frmDisplay Is Nothing Then
            aSpool.frmDisplay.Toolbar1.Buttons("FILEJOIN").ButtonMenus(i).Visible = True
            aSpool.frmDisplay.Toolbar1.Buttons("FILEJOIN").ButtonMenus(i).Text = i & "  " & Rep
            aSpool.frmDisplay.Toolbar1.Buttons("FILEJOIN").ButtonMenus(i).Tag = Entry(i, JoinFich, ",")
        End If
    Next
Next

GestError:

End Function

Private Sub RunBatch()
'-> cette procedure lance differents turbo a partir de lignes de commandes contenus dans un fichier
Dim BatchFile As String
Dim hdlBatch As Integer
Dim Ligne As String
Dim CurrentPIDProcess As Long
Dim FileName As String

'-> on recupere le fichier batch
BatchFile = Entry(1, Entry(2, Command$, "="), "$")

'-> Ouvrir le fichier Batch
hdlBatch = FreeFile
Open BatchFile For Input As #hdlBatch

'-> Transf�rer la maquette dans l'objet Spool
Do While Not EOF(hdlBatch)
    '-> Lecture de la ligne
    Line Input #hdlBatch, Ligne
    If Trim(Ligne) = "" Then GoTo NextLigne
    '-> On lance la ligne de commande
    CurrentPIDProcess = Shell(App.Path & "\TurboGraph.exe " & Ligne, vbNormalFocus)
    'CurrentPIDProcess = Shell("d:\compilation\TurboGraph.exe " & ligne, vbNormalFocus)
    '-> Boucler tant que le processus est actif
    Do While IsPidRunning(CurrentPIDProcess)
        '-> Lib�ration de la pile des messages
        DoEvents
        Sleep 1000
    Loop
NextLigne:
Loop

'-> Fermer le fichier Batch
Close #hdlBatch

'-> on regarde si on doit supprimer les fichiers tempo
If Entry(2, Entry(2, UCase(Command$), "$SUPPR="), "=") <> "1" Then Exit Sub
'-> on attend que tout se termine
Sleep 20000

'-> Ouvrir le fichier Batch
hdlBatch = FreeFile
Open BatchFile For Input As #hdlBatch

'-> Transf�rer la maquette dans l'objet Spool
Do While Not EOF(hdlBatch)
    '-> Lecture de la ligne
    Line Input #hdlBatch, Ligne
    If Trim(Ligne) = "" Then GoTo NextLigne
    '-> On supprime le pdf
    FileName = Entry(2, Entry(1, Entry(2, UCase(Ligne), "FILE="), "$"), "=")
    If Dir(FileName & ".pdf") <> "" Then Kill FileName & ".pdf"
    '-> On supprime le BodyFile
    FileName = Entry(2, Entry(1, Entry(2, UCase(Ligne), "BODYFILE="), "$"), "=")
    If Dir(FileName) <> "" Then Kill FileName
    '-> On supprime le File
    FileName = Entry(2, Entry(1, Entry(2, UCase(Ligne), "FILE="), "$"), "=")
    If Dir(FileName) <> "" Then Kill FileName
Loop

'-> Fermer le fichier Batch
Close #hdlBatch

'-> on supprime le mouchard
If Dir(BatchFile) <> "" Then Kill BatchFile

End Sub

Public Function TelechargeFile(strUrlName As String, strPathNameDestination As String) As Boolean
Dim errcode As Long
Dim localFileName As String
  
'-> On sp�cifie le chemin de l'image web
'url = "http://www.siteweb.com/image.jpg"
  
'On appelle la fonction api de t�l�chargement
errcode = URLDownloadToFile(0, strUrlName, strPathNameDestination, 0, 0)
  
If errcode = 0 Then
    '-> l'image a �t� rapatri�e
    TelechargeFile = True
Else
    '-> l'image n'a pas pu etre telechargee
    TelechargeFile = False
End If

End Function

Public Sub Trace(sText As String)
'--> cette procedure permet d'alimenter le fichier de debug
'-> si on est en mode trace
'on error resume next

If IsMouchard Then
    'si le fichier n'est pas ouvert l'ouvrir
    If hdlMouchard = 0 Then
        hdlMouchard = FreeFile
        Mouchard = GetTempFileNameVB("TXT")
        If FileExist(Mouchard) Then
            If FileLen(Mouchard) > 10000 Then Kill Mouchard
        End If
        Open Mouchard For Append As #hdlMouchard
        '-> on initialise le mode trace
        Print #hdlMouchard, ""
        Print #hdlMouchard, "***************************"
        Print #hdlMouchard, "Trace du " & FORMAT(Now, "dd/mm/yyyy � hh:mm:ss")
    End If
    '-> ecrire la ligne
    Print #hdlMouchard, sText
End If

End Sub

Public Function SearchForFiles(sRoot As String, sFile As String, Optional sFiltreDirectory As String) As String
    '--> cette fonction cherche des fichiers � partir d'une directorie
   Dim WFD As WIN32_FIND_DATA
   Dim hFile As Long
  
   With fp
      .sFileRoot = QualifyPath(sRoot)      'chemin de d�part
      .sFileNameExt = sFile                'fichier (* ? autoris�
      .bRecurse = 1                             'True = recherche recursive
      .bFindOrExclude = 1                       '0=inclure, 1=exclure
   End With
   
   
   hFile = FindFirstFile(sRoot & "*.*", WFD)
   If hFile <> -1 Then
      Do
        If stopSearch = True Then Exit Function
        DoEvents
        'si c'est un repertoire on boucle
         If (WFD.dwFileAttributes And vbDirectory) Then
            If Asc(WFD.cFileName) <> CLng(46) Then
                If fp.bRecurse Then
                    SearchForFiles = SearchForFiles(sRoot & TrimNull(WFD.cFileName) & vbBackslash, sFile)
                End If
            End If
         Else
           'doit etre un fichier..
            If MatchSpec(WFD.cFileName, fp.sFileNameExt) Then
                If sFiltreDirectory <> "" Then
                    If InStr(1, sRoot & TrimNull(WFD.cFileName), sFiltreDirectory) <> 0 Then
                        SearchForFiles = sRoot & TrimNull(WFD.cFileName)
                        stopSearch = True
                        Exit Do
                    End If
                Else
                    SearchForFiles = sRoot & TrimNull(WFD.cFileName)
                    stopSearch = True
                    Exit Do
                End If
            End If
         End If
      Loop While FindNextFile(hFile, WFD)
   End If
   Call FindClose(hFile)
End Function

Public Function QualifyPath(spath As String) As String
   If Right$(spath, 1) <> vbBackslash Then
      QualifyPath = spath & vbBackslash
   Else
      QualifyPath = spath
   End If
End Function

Public Function TrimNull(startstr As String) As String
   TrimNull = Left$(startstr, lstrlen(StrPtr(startstr)))
End Function

Public Function MatchSpec(sFile As String, sSpec As String) As Boolean
   MatchSpec = PathMatchSpec(StrPtr(sFile), StrPtr(sSpec)) = fp.bFindOrExclude
End Function


