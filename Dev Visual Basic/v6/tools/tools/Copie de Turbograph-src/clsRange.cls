VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsRange"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private pBorderLineStyle(7 To 10) As Variant
Private pBorderColorIndex(7 To 10) As Variant
Public FontName As String
Public FontSize As Integer
Public FontBold As Boolean
Public FontItalic As Boolean
Public FontUnderline As Boolean
Public BackColor As Long
Public VerticalAlignment As Variant
Public HorizontalAlignment As Variant
Public WrapText As Boolean


Public Sub SetBorderLineStyle(Border As Long, Value As Variant)
    pBorderLineStyle(Border) = Value
End Sub

Public Sub SetBorderColorIndex(Border As Long, Value As Variant)
    pBorderColorIndex(Border) = Value
End Sub

Public Function GetBorderLineStyle(Border As Long) As Variant
    GetBorderLineStyle = pBorderLineStyle(Border)
End Function

Public Function GetBorderColorIndex(Border As Long) As Variant
    GetBorderColorIndex = pBorderColorIndex(Border)
End Function

