VERSION 5.00
Begin VB.Form frmSearchText 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   1485
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4950
   Icon            =   "frmSearchText.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1485
   ScaleWidth      =   4950
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Height          =   1215
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4695
      Begin VB.CommandButton Command1 
         Caption         =   "Command1"
         Height          =   375
         Left            =   3600
         TabIndex        =   3
         Top             =   720
         Width           =   975
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   2040
         TabIndex        =   2
         Top             =   360
         Width           =   2535
      End
      Begin VB.Image Image1 
         Height          =   720
         Left            =   120
         Picture         =   "frmSearchText.frx":2B82
         Top             =   240
         Width           =   720
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   255
         Left            =   840
         TabIndex        =   1
         Top             =   360
         Width           =   3015
      End
   End
End
Attribute VB_Name = "frmSearchText"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()

On Error Resume Next

Dim aLb As Libelle

'-> Pointer sur la classe associ�e
Set aLb = Libelles("MESSAGE")

'-> V�rifier si saisie n'est pas vide
If Trim(Me.Text1.Text) = "" Then GoTo ErrorPage
         
'-> Renvoyer le texte � rechercher
strRetour = Me.Text1.Text
    
'-> D�charger la feuille
Unload Me
    
    
Exit Sub

ErrorPage:
    MsgBox aLb.GetCaption(12), vbCritical + vbOKOnly, aLb.GetToolTip(12)
    Me.Text1.Text = ""
    Me.Text1.SetFocus


End Sub

Private Sub Form_Activate()

'-> selectionner eventuellement le texte
Me.Text1.SelStart = 0
Me.Text1.SelLength = Len(Me.Text1.Text)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

If KeyCode = vbKeyEscape Then Unload Me

End Sub

Private Sub Form_Load()

Dim aLb As Libelle

'-> Pointer sur la classe libelles
Set aLb = Libelles("FRMSEARCHTEXT")

Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)

'-> Pointer sur la classe libelles des boutons
Set aLb = Libelles("BOUTONS")
Me.Command1.Caption = aLb.GetCaption(1)

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Command1_Click

End Sub
