VERSION 5.00
Begin VB.Form frmFont 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   1845
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7740
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1845
   ScaleWidth      =   7740
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   375
      Left            =   6720
      TabIndex        =   5
      Top             =   720
      Width           =   975
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   375
      Left            =   6720
      TabIndex        =   4
      Top             =   240
      Width           =   975
   End
   Begin VB.ListBox List3 
      Height          =   1425
      Left            =   5760
      TabIndex        =   3
      Top             =   360
      Width           =   855
   End
   Begin VB.ListBox List2 
      Height          =   1425
      Left            =   2880
      TabIndex        =   2
      Top             =   360
      Width           =   2775
   End
   Begin VB.ListBox List1 
      Height          =   1425
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   2655
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      Height          =   255
      Left            =   5760
      TabIndex        =   7
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      Height          =   255
      Left            =   2880
      TabIndex        =   6
      Top             =   120
      Width           =   2775
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   2655
   End
End
Attribute VB_Name = "frmFont"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

'---> Envoyer les nouvelles informations � l'�diteur

'-> Setting du nom de la police
frmEditor.RichTextBox1.Font.Name = Me.List1.Text

'-> Setting de la taille de la police
frmEditor.RichTextBox1.Font.Size = CInt(Me.List3.Text)

'-> Setting des attributs de la police
Select Case Me.List2.ListIndex
    Case 0
        frmEditor.RichTextBox1.Font.Bold = False
        frmEditor.RichTextBox1.Font.Italic = False
    Case 1
        frmEditor.RichTextBox1.Font.Bold = False
        frmEditor.RichTextBox1.Font.Italic = True
    Case 2
        frmEditor.RichTextBox1.Font.Bold = True
        frmEditor.RichTextBox1.Font.Italic = False
    Case 3
        frmEditor.RichTextBox1.Font.Bold = True
        frmEditor.RichTextBox1.Font.Italic = True
End Select
        
'-> Quiter
Unload Me

End Sub

Private Sub Command2_Click()

'-> Quiter
Unload Me

End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim i As Integer


'---> Afficher les messprog
Set aLb = Libelles("FRMFONT")

Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Label2.Caption = aLb.GetCaption(3)
Me.Label3.Caption = aLb.GetCaption(4)

'-> Charger les polices
For i = 1 To Screen.FontCount
    Me.List1.AddItem Screen.Fonts(i)
Next

'-> Charger les caract�risttiques
Me.List2.AddItem aLb.GetCaption(5)
Me.List2.AddItem aLb.GetCaption(6)
Me.List2.AddItem aLb.GetCaption(7)
Me.List2.AddItem aLb.GetCaption(8)

'-> Charger les tailles des polices
For i = 8 To 72 Step 2
    Me.List3.AddItem i
Next

'-> Libelles des boutons
Set aLb = Libelles("BOUTONS")
Me.Command1.Caption = aLb.GetCaption(1)
Me.Command2.Caption = aLb.GetCaption(2)


End Sub
