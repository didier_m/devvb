VERSION 5.00
Begin VB.Form frmSearchPage 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   1485
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4245
   Icon            =   "frmSearchPage.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1485
   ScaleWidth      =   4245
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Height          =   1215
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3975
      Begin VB.CommandButton Command1 
         Caption         =   "Command1"
         Height          =   375
         Left            =   2770
         TabIndex        =   3
         Top             =   760
         Width           =   1095
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   3120
         TabIndex        =   2
         Top             =   360
         Width           =   735
      End
      Begin VB.Image Image1 
         Height          =   720
         Left            =   120
         Picture         =   "frmSearchPage.frx":0ECA
         Top             =   240
         Width           =   720
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   255
         Left            =   840
         TabIndex        =   1
         Top             =   360
         Width           =   2295
      End
   End
End
Attribute VB_Name = "frmSearchPage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public NbMax As Integer
Public NbMin As Integer

Private Sub Command1_Click()

On Error Resume Next

Dim aLb As Libelle

'-> Pointer sur la classe associ�e
Set aLb = Libelles("MESSAGE")

'-> V�rifier si saisie num�rique
If Not IsNumeric(Me.Text1.Text) Then GoTo ErrorPage
    
'-> V�rifier que l'on n'est pas inf�rieur au mini
If CLng(Me.Text1.Text) < Me.NbMin Then GoTo ErrorPage
    
'-> V�rifier que pas > au nombre de page
If CLng(Me.Text1.Text) > Me.NbMax Then GoTo ErrorPage
     
'-> Renvoyer le num�ro de la page
strRetour = CLng(Me.Text1.Text)
    
'-> D�charger la feuille
Unload Me
    
    
Exit Sub

ErrorPage:
    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
    Me.Text1.Text = ""
    Me.Text1.SetFocus


End Sub

Private Sub Form_Load()

Dim aLb As Libelle

'-> Pointer sur la classe libelles
Set aLb = Libelles("FRMSEARCHPAGE")

Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)

'-> Pointer sur la classe libelles des boutons
Set aLb = Libelles("BOUTONS")
Me.Command1.Caption = aLb.GetCaption(1)

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Command1_Click

End Sub
