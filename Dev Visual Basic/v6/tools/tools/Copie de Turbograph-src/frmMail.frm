VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form frmMail 
   ClientHeight    =   8160
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11130
   Icon            =   "frmMail.frx":0000
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   8160
   ScaleWidth      =   11130
   StartUpPosition =   2  'CenterScreen
   Begin MSWinsockLib.Winsock Winsock1 
      Left            =   9120
      Top             =   840
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   6615
      Left            =   120
      TabIndex        =   3
      Top             =   1440
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   11668
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   882
      WordWrap        =   0   'False
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "frmMail.frx":2E82
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame3"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "frmMail.frx":2E9E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame5"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Tab 2"
      TabPicture(2)   =   "frmMail.frx":2EBA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame6"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Tab 3"
      TabPicture(3)   =   "frmMail.frx":2ED6
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Frame2"
      Tab(3).Control(1)=   "Check1"
      Tab(3).ControlCount=   2
      Begin VB.Frame Frame6 
         Caption         =   "Envoyer par un serveur de messagerie"
         Height          =   5535
         Left            =   -74760
         TabIndex        =   39
         Top             =   840
         Width           =   7815
         Begin VB.CommandButton Command3 
            Caption         =   "&Envoyer"
            Height          =   375
            Left            =   6240
            TabIndex        =   50
            Top             =   5040
            Width           =   1095
         End
         Begin VB.TextBox Text7 
            Height          =   285
            Left            =   1320
            TabIndex        =   41
            Top             =   960
            Width           =   4335
         End
         Begin VB.TextBox Text6 
            Height          =   285
            Left            =   1320
            TabIndex        =   42
            Top             =   1440
            Width           =   4335
         End
         Begin VB.TextBox Text5 
            Height          =   285
            Left            =   1320
            TabIndex        =   43
            Top             =   1920
            Width           =   4455
         End
         Begin VB.TextBox Text4 
            Height          =   285
            Left            =   1320
            TabIndex        =   40
            Top             =   480
            Width           =   4455
         End
         Begin RichTextLib.RichTextBox RichTextBox2 
            Height          =   3015
            Left            =   1320
            TabIndex        =   44
            Top             =   2400
            Width           =   4470
            _ExtentX        =   7885
            _ExtentY        =   5318
            _Version        =   393217
            Enabled         =   -1  'True
            TextRTF         =   $"frmMail.frx":2EF2
         End
         Begin VB.Label Label17 
            Caption         =   "Serveur de messagerie : "
            Height          =   255
            Left            =   840
            TabIndex        =   52
            Top             =   5160
            Width           =   6975
         End
         Begin VB.Label Label16 
            Caption         =   "A :"
            Height          =   255
            Left            =   720
            TabIndex        =   49
            Top             =   960
            Width           =   375
         End
         Begin VB.Label Label15 
            Caption         =   "Cc :"
            Height          =   255
            Left            =   720
            TabIndex        =   48
            Top             =   1440
            Width           =   375
         End
         Begin VB.Label Label14 
            Caption         =   "Corps :"
            Height          =   255
            Left            =   720
            TabIndex        =   47
            Top             =   2400
            Width           =   495
         End
         Begin VB.Label Label13 
            Caption         =   "Obj :"
            Height          =   255
            Left            =   720
            TabIndex        =   46
            Top             =   1920
            Width           =   495
         End
         Begin VB.Label Label12 
            Caption         =   "De :"
            Height          =   255
            Left            =   720
            TabIndex        =   45
            Top             =   480
            Width           =   495
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Envoyer par Outlook/Lotus Notes"
         Height          =   5535
         Left            =   -74760
         TabIndex        =   28
         Top             =   840
         Width           =   7815
         Begin VB.CheckBox Check3 
            Height          =   195
            Left            =   1320
            TabIndex        =   32
            Top             =   5040
            Width           =   4455
         End
         Begin VB.TextBox Text1 
            Height          =   285
            Left            =   1320
            TabIndex        =   33
            Top             =   1440
            Width           =   4335
         End
         Begin VB.TextBox Text2 
            Height          =   285
            Left            =   1320
            TabIndex        =   30
            Top             =   480
            Width           =   4335
         End
         Begin VB.TextBox Text3 
            Height          =   285
            Left            =   1320
            TabIndex        =   31
            Top             =   960
            Width           =   4335
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Envoyer"
            Height          =   375
            Left            =   6360
            TabIndex        =   29
            Top             =   4830
            Width           =   1095
         End
         Begin RichTextLib.RichTextBox RichTextBox1 
            Height          =   3015
            Left            =   1320
            TabIndex        =   34
            Top             =   1920
            Width           =   4350
            _ExtentX        =   7673
            _ExtentY        =   5318
            _Version        =   393217
            TextRTF         =   $"frmMail.frx":2F74
         End
         Begin VB.Image Image4 
            Height          =   720
            Index           =   0
            Left            =   240
            Picture         =   "frmMail.frx":2FF6
            ToolTipText     =   "Envoyer"
            Top             =   4590
            Visible         =   0   'False
            Width           =   720
         End
         Begin VB.Image Image14 
            Height          =   240
            Left            =   360
            MouseIcon       =   "frmMail.frx":4CC0
            MousePointer    =   99  'Custom
            Picture         =   "frmMail.frx":558A
            ToolTipText     =   "Destinataire"
            Top             =   480
            Width           =   240
         End
         Begin VB.Image Image15 
            Height          =   240
            Left            =   360
            MouseIcon       =   "frmMail.frx":5B14
            MousePointer    =   99  'Custom
            Picture         =   "frmMail.frx":63DE
            ToolTipText     =   "Destinataire"
            Top             =   960
            Width           =   240
         End
         Begin VB.Label Label5 
            Caption         =   "A :"
            Height          =   255
            Left            =   720
            TabIndex        =   38
            Top             =   480
            Width           =   375
         End
         Begin VB.Label Label6 
            Caption         =   "Cc :"
            Height          =   255
            Left            =   720
            TabIndex        =   37
            Top             =   960
            Width           =   375
         End
         Begin VB.Label Label7 
            Caption         =   "Obj :"
            Height          =   255
            Left            =   720
            TabIndex        =   36
            Top             =   1440
            Width           =   570
         End
         Begin VB.Label Label8 
            Caption         =   "Corps : :"
            Height          =   255
            Left            =   720
            TabIndex        =   35
            Top             =   1920
            Width           =   570
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Envoyer par votre messagerie par d�faut"
         Height          =   5535
         Left            =   240
         TabIndex        =   20
         Top             =   840
         Width           =   7815
         Begin VB.TextBox strUrl 
            Height          =   285
            Left            =   1320
            TabIndex        =   22
            Top             =   480
            Width           =   5775
         End
         Begin VB.TextBox strObjet 
            Height          =   285
            Left            =   1320
            TabIndex        =   24
            Top             =   960
            Width           =   5775
         End
         Begin VB.TextBox strMessage 
            Height          =   3135
            Left            =   1320
            MultiLine       =   -1  'True
            TabIndex        =   26
            Top             =   1440
            Width           =   5775
         End
         Begin VB.CommandButton Command1 
            Caption         =   "Envoyer"
            Height          =   375
            Left            =   5760
            TabIndex        =   21
            Top             =   4800
            Width           =   1095
         End
         Begin VB.Image Image1 
            Height          =   240
            Left            =   360
            MouseIcon       =   "frmMail.frx":6968
            MousePointer    =   99  'Custom
            Picture         =   "frmMail.frx":7232
            ToolTipText     =   "Destinataire"
            Top             =   480
            Width           =   240
         End
         Begin VB.Image Image4 
            Height          =   720
            Index           =   1
            Left            =   480
            Picture         =   "frmMail.frx":77BC
            ToolTipText     =   "Envoyer"
            Top             =   4710
            Visible         =   0   'False
            Width           =   720
         End
         Begin VB.Label Label9 
            Caption         =   "Obj :"
            Height          =   255
            Left            =   720
            TabIndex        =   27
            Top             =   960
            Width           =   570
         End
         Begin VB.Label Label10 
            Caption         =   "A :"
            Height          =   255
            Left            =   720
            TabIndex        =   25
            Top             =   480
            Width           =   375
         End
         Begin VB.Label Label11 
            Caption         =   "Corps : :"
            Height          =   255
            Left            =   720
            TabIndex        =   23
            Top             =   1440
            Width           =   570
         End
      End
      Begin VB.CheckBox Check1 
         Height          =   195
         Left            =   -74400
         TabIndex        =   19
         Top             =   1320
         Width           =   2055
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   3975
         Left            =   -74400
         TabIndex        =   5
         Top             =   1680
         Width           =   5055
         Begin VB.PictureBox PicNothing 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   2760
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   14
            Top             =   480
            Width           =   735
            Begin VB.Image Image17 
               Height          =   720
               Left            =   0
               Picture         =   "frmMail.frx":9486
               Top             =   -40
               Width           =   720
            End
         End
         Begin VB.PictureBox PicPdf 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   2760
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   13
            Top             =   1200
            Width           =   735
            Begin VB.Image Image16 
               Height          =   480
               Left            =   100
               Picture         =   "frmMail.frx":A350
               Top             =   65
               Width           =   480
            End
         End
         Begin VB.Frame Frame4 
            Enabled         =   0   'False
            Height          =   1815
            Left            =   360
            TabIndex        =   8
            Top             =   2040
            Width           =   4455
            Begin VB.CheckBox Check2 
               Height          =   255
               Left            =   240
               TabIndex        =   12
               Top             =   1440
               Value           =   1  'Checked
               Width           =   4095
            End
            Begin VB.OptionButton Option4 
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   11
               Top             =   360
               Width           =   3735
            End
            Begin VB.OptionButton Option5 
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   10
               Top             =   720
               Value           =   -1  'True
               Width           =   3975
            End
            Begin VB.OptionButton Option6 
               CausesValidation=   0   'False
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   9
               Top             =   1080
               Width           =   2415
            End
         End
         Begin VB.PictureBox PicHtml 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   360
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   7
            Top             =   1200
            Width           =   735
            Begin VB.Image Image12 
               Height          =   480
               Left            =   120
               Picture         =   "frmMail.frx":AC1A
               Top             =   80
               Width           =   480
            End
         End
         Begin VB.PictureBox picTurbo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   360
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   6
            Top             =   480
            Width           =   735
            Begin VB.Image Image3 
               Height          =   480
               Left            =   120
               Picture         =   "frmMail.frx":B4E4
               Top             =   50
               Width           =   480
            End
         End
         Begin VB.Label Label4 
            Height          =   615
            Left            =   3720
            TabIndex        =   18
            Top             =   1275
            Width           =   1215
         End
         Begin VB.Label Label3 
            Height          =   615
            Left            =   3720
            TabIndex        =   17
            Top             =   505
            Width           =   1215
         End
         Begin VB.Shape Sel 
            BorderColor     =   &H000000FF&
            BorderWidth     =   2
            Height          =   655
            Left            =   350
            Top             =   465
            Width           =   775
         End
         Begin VB.Label Label2 
            Height          =   495
            Left            =   1320
            TabIndex        =   16
            Top             =   1275
            Width           =   1335
         End
         Begin VB.Label Label1 
            Height          =   615
            Left            =   1320
            TabIndex        =   15
            Top             =   505
            Width           =   1335
         End
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1215
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8295
      Begin MSComctlLib.ImageCombo ImageCombo1 
         Height          =   570
         Left            =   5520
         TabIndex        =   51
         Top             =   360
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   1005
         _Version        =   393216
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Text            =   "ImageCombo1"
         ImageList       =   "ImageList1"
      End
      Begin VB.OptionButton Option3 
         Height          =   255
         Left            =   2760
         TabIndex        =   4
         Top             =   360
         Value           =   -1  'True
         Width           =   2415
      End
      Begin MSComDlg.CommonDialog CommonDialog1 
         Left            =   4560
         Top             =   600
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   5160
         Top             =   360
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   7
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMail.frx":BDAE
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMail.frx":C0C8
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMail.frx":C3E2
               Key             =   "LOTUS"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMail.frx":E51C
               Key             =   "OUTLOOK"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMail.frx":EDF6
               Key             =   "WWW"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMail.frx":11C88
               Key             =   "PJ"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMail.frx":12562
               Key             =   "SMTP"
            EndProperty
         EndProperty
      End
      Begin VB.OptionButton Option2 
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   720
         Width           =   2415
      End
      Begin VB.OptionButton Option1 
         Height          =   195
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   2295
      End
   End
End
Attribute VB_Name = "frmMail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-> Fichier � imprimer
Public aSpool As Spool
Public FromVisu As Boolean
Public lvSend As Integer
Public FormatHtml As Boolean
Public FormatPDF As Boolean
Public FormatNothing As Boolean
Public FormatTurbo As Boolean
Dim iStep As Integer
Dim ReponseFaite As Boolean

Private Sub Command1_Click()
Call Image4_Click(1)
End Sub

Private Sub Command2_Click()
Call Image4_Click(0)
End Sub

Private Sub Command3_Click()
    sendBySMTP
End Sub

Public Function sendBySMTP() As Boolean
'--> cette fonction nous permet d'envoyer un mail
'--> par le composant winsock
Dim i As Integer
Dim j As Long
Dim m_strEncodedFiles As String
Dim l_destinataire
Dim destinataire
Dim sPieceJointe As String
Dim CurrentPIDProcess As Long
Dim sSMTP As String
Dim FileToSend As String
Dim sAdrMail As String

On Error Resume Next

Me.MousePointer = 11
sAdrMail = Trim(Me.Text7.Text)

'-> on se connecte au serveur
Winsock1.Close
iStep = 1
If GetIniString("PARAM", "SMTP", TurboGraphIniFile, False) <> "" Then
    sSMTP = GetIniString("PARAM", "SMTP", TurboGraphIniFile, False)
Else
    MsgBox "Veuillez param�trer l'adresse de votre serveur de messagerie SMTP", vbExclamation, ""
End If
Winsock1.Connect sSMTP, 25

While Winsock1.State <> sckConnected
    DoEvents
Wend
    
ReponseFaite = False
Winsock1.SendData "HELO Serveur" & vbCrLf
While ReponseFaite = False
DoEvents
Wend

ReponseFaite = False
If Me.Text4.Text = "" Then Me.Text4.Text = "Unknown"
Winsock1.SendData "MAIL FROM: " & Replace(Trim(Me.Text4.Text), " ", "") & "" & vbCrLf
While ReponseFaite = False
DoEvents
Wend

'-> on envoie les dif�rents destinataires (s�parateur ';' dans la saisie)
iStep = 3
'-> dans le cas ou on a une copie cach�e
If Trim(Me.Text6.Text) <> "" Then
    l_destinataire = Split(Entry(3, sAdrMail & ";" & Trim(Me.Text6.Text), "~"), ";")
Else
    l_destinataire = Split(Entry(3, sAdrMail, "~"), ";")
End If

For Each destinataire In l_destinataire
    ReponseFaite = False
    Winsock1.SendData "RCPT TO: " & Trim(destinataire) & "" & vbCrLf
    While ReponseFaite = False
        DoEvents
    Wend
Next

'-> on change d'etape
iStep = 0
ReponseFaite = False
Winsock1.SendData "DATA" & vbCrLf
While ReponseFaite = False
DoEvents
Wend

'-> on s'occupe ici du fichier a envoyer
If Me.Check1.Value = 1 Then
    IsCryptedFile = True
Else
    IsCryptedFile = False
End If

'-> Cl� du spool et du fichier
SpoolKeyMail = aSpool.Key
FileKeyMail = Trim(UCase$(aSpool.FileName))
'-> Nature de l'export
If Me.Option1.Value Then '-> Export de la page
    PageNumMail = aSpool.CurrentPage
    ExportMail = 0
ElseIf Me.Option2.Value Then '-> Export du spool
    ExportMail = 1
Else '-> Export du fichier
    ExportMail = 2
End If

'-> nom du spool � traiter
SpoolName = aSpool.FileName

'-> On reccupere les pages � trater dans le spool
Select Case ExportMail
    Case 0, 1
        If ExportMail = 0 Then
            '-> Envoie de la page en cours
            SpoolName = CreateDetail(aSpool, PageNumMail)
        Else
            '-> Envoie du spool en cours
            SpoolName = CreateDetail(aSpool, -1)
        End If
End Select

FileToSend = SpoolName


ReponseFaite = False
'-> Format de la piece jointe
If FormatTurbo Then pFormat = "0"
If FormatHtml = True Then pFormat = "1"
If FormatNothing Then pFormat = "3"
If FormatPDF = True Then pFormat = "4"

'-> Attacher le fichier sp�cifi�
Select Case pFormat
    Case "0" 'turbo
        'on attache le spool turbo
        sPieceJointe = FileToSend
    Case "1" 'html
        '-> on transforme le spool en html
        '-> Inclure le num�ro de page
        IncluseSpoolNumber = CBool(Me.Check2.Value)
        '-> Positionnement de la navigationHTML
        If Me.Option4.Value Then
            NavigationType = 0 'FrameSet
        ElseIf Me.Option5.Value Then
            NavigationType = 1 'Fichier unique
        Else
            NavigationType = 2 'Pages
        End If
        PathToExport = GetTempFileNameVB("WWW", True)
        FileKeyMail = Trim(UCase$(FileToSend))
        '-> Nom du Fichier
        FileToExport = LCase$(GetSpoolName(FileKeyMail)) & ".html"
        '-> Cl� du fichiers
        FileKeyExport = FileKeyMail
        '-> Spool
        SpoolKeyMail = aSpool.Key
        SpoolKeyExport = SpoolKeyMail
        '-> Num�ro de page
        PageToExport = PageNumMail
        ExportType = 1
            
        '-> On ajoute le spool
        AnalyseFileToPrint FileToSend

        CreateEditionHTML
        '-> Attacher le fichier sp�cifi�
        If ListeHtmlFile <> "" Then
            sPieceJointe = ListeHtmlFile
        End If
        '-> Ajouter les images s'il y en a
        For i = 1 To NbImages
            sPieceJointe = sPieceJointe & "|" & ListeHTMLImages(i)
        Next
    Case "3" 'nothing
    
    Case "4" 'pdf
        '-> on g�n�re le fichier avec le turboPDF
        If FilePath = "" Then FilePath = OpenPath
        CurrentPIDProcess = Shell(App.Path & "\TurboPdf.exe " & "�fileToConvert=" & FileToSend & GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False) & "�pdfDirectory=" & Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1))
        '-> Boucler tant que le processus est actif
        Do While IsPidRunning(CurrentPIDProcess)
            '-> Lib�ration de la pile des messages
            DoEvents
            Sleep 300
        Loop
        'on attache le spool turbo
        sPieceJointe = (Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1) & "/" & GetSpoolName(FileToSend) & ".pdf")
End Select

'-> on met en forme la ou les pi�ces jointes
For i = 1 To NumEntries(sPieceJointe, "|")
    m_strEncodedFiles = m_strEncodedFiles & UUEncodeFile(Entry(i, sPieceJointe, "|")) & vbCrLf
Next i

iStep = 5
'on pr�pare l'ent�te du message
Winsock1.SendData "To:" & Entry(3, sAdrMail, "~") & vbCrLf
Winsock1.SendData "From:" & Me.Text4.Text & vbCrLf
Winsock1.SendData "Subject:" & Me.Text5.Text & vbCrLf
'ChaineMessage = ChaineMessage & "CONTENT-TYPE: TEXT/HTML;" & vbCrLf & vbCrLf

'on envoie le message et la ou les pi�ces jointes si il y en a
Dim varLines    As Variant
Dim varLine     As Variant
Dim strMessage  As String

'ajout des pi�ces jointes au texte du message
strMessage = vbCrLf & vbCrLf & Me.RichTextBox2.Text & vbCrLf & vbCrLf & m_strEncodedFiles
'on r�initialise la m�moire
m_strEncodedFiles = ""
'On coupe le fichier en plusieurs lignes (pour VB6 seulement)
varLines = Split(strMessage, vbCrLf)
'on vide la m�moire
strMessage = ""
'On envoie chaque ligne du message
For Each varLine In varLines
    Winsock1.SendData CStr(varLine) & vbLf
Next
'on envoie un point pour signaler au serveur que le msg est fini
iStep = 7
Winsock1.SendData vbCrLf & "." & vbCrLf
j = 0
While ReponseFaite = False
    j = j + 1
    If j > 10000000 Then
        ReponseFaite = True
    End If
    DoEvents
Wend

ReponseFaite = False
Winsock1.SendData "QUIT" & vbCrLf
j = 0
While ReponseFaite = False
    j = j + 1
    If j > 10000000 Then
        ReponseFaite = True
    End If
    DoEvents
Wend

Winsock1.Close

sendBySMTP = True
Me.MousePointer = 0
Unload Me
End Function

Private Sub Form_Load()

Dim aLb As Libelle
Dim PrinterFound As Boolean
Dim Printer As Printer
Dim aItem As ComboItem

'On Error Resume Next

'-> Gestion des messages
Set aLb = Libelles("FRMMAIL")

Me.Caption = aLb.GetCaption(1)
Me.Frame1.Caption = aLb.GetCaption(2)
Me.Option1.Caption = aLb.GetCaption(3)
Me.Option2.Caption = aLb.GetCaption(4)
Me.Check1.Caption = aLb.GetCaption(5)
Me.SSTab1.TabCaption(0) = "     " & aLb.GetCaption(6)
Me.SSTab1.TabCaption(1) = "     " & aLb.GetCaption(7)
Me.Image1.ToolTipText = aLb.GetCaption(8)
'Me.Image2.ToolTipText = aLb.GetCaption(9)
'Me.Image5.ToolTipText = aLb.GetCaption(10)
'Me.Image6.ToolTipText = Me.Image5.ToolTipText
Me.Image4(0).ToolTipText = aLb.GetCaption(11)
Me.Image4(1).ToolTipText = aLb.GetCaption(11)
'Me.Image7.ToolTipText = aLb.GetCaption(12)
'Me.Image8.ToolTipText = aLb.GetCaption(13)
'Me.Image9.ToolTipText = aLb.GetCaption(14)
'Me.Image10.ToolTipText = aLb.GetCaption(15)
'Me.Image11.ToolTipText = aLb.GetCaption(16)
Me.Image15.ToolTipText = aLb.GetCaption(32)
Me.Option3.Caption = aLb.GetCaption(18)
Me.SSTab1.TabCaption(3) = aLb.GetCaption(19)
Me.SSTab1.TabCaption(2) = "Serveur de messagerie"
Me.Frame2.Caption = aLb.GetCaption(20)
Me.Label1.Caption = aLb.GetCaption(21)
Me.Label2.Caption = aLb.GetCaption(22)
Me.Label3.Caption = aLb.GetCaption(30)
Me.Label4.Caption = aLb.GetCaption(29)
Me.Frame4.Caption = aLb.GetCaption(23)
Me.Option4.Caption = aLb.GetCaption(24)
Me.Option5.Caption = aLb.GetCaption(25)
Me.Option6.Caption = aLb.GetCaption(26)
Me.Check2.Caption = aLb.GetCaption(27)
Me.Check3.Caption = aLb.GetCaption(28)
Me.Label5.Caption = aLb.GetCaption(33)
Me.Label6.Caption = aLb.GetCaption(34)
Me.Label7.Caption = aLb.GetCaption(35)
If GetIniString("PARAM", "SMTP", TurboGraphIniFile, False) <> "" Then
    Me.Label17.Caption = Me.Label17.Caption & GetIniString("PARAM", "SMTP", TurboGraphIniFile, False)
Else
    Me.Label17.Caption = Me.Label17.Caption & "VEUILLEZ PARAMETRER VOTRE SERVEUR DE MESSAGERIE"
End If

'-> Charger les images
Set Me.SSTab1.TabPicture(0) = Me.ImageList1.ListImages("WWW").Picture
'-> par defaut outlook
Set Me.SSTab1.TabPicture(1) = Me.ImageList1.ListImages("OUTLOOK").Picture
Set Me.SSTab1.TabPicture(3) = Me.ImageList1.ListImages("PJ").Picture
Set Me.SSTab1.TabPicture(2) = Me.ImageList1.ListImages("SMTP").Picture
'-> lotus sinon (prend le dessus)
If IsLotus Then
    Set Me.SSTab1.TabPicture(1) = Me.ImageList1.ListImages("LOTUS").Picture
    Me.SSTab1.TabCaption(1) = "     " & aLb.GetCaption(31)
End If
'-> S�lectionner le premier onglet
Me.SSTab1.Tab = 0

Me.ImageCombo1.comboItems.add , , "Envoyer par votre messagerie par d�faut", Me.ImageList1.ListImages("WWW").Index
If Not IsLotus Then
    Me.ImageCombo1.comboItems.add , , "Envoyer en utilisant votre compte Outlook", Me.ImageList1.ListImages("OUTLOOK").Index
Else
    If IsLotus Then Me.ImageCombo1.comboItems.add , , "Envoyer en utilisant votre compte Lotus Notes", Me.ImageList1.ListImages("LOTUS").Index
End If
Me.ImageCombo1.comboItems.add , , "Envoyer en utilisant votre serveur de messagerie", Me.ImageList1.ListImages("SMTP").Index

Me.ImageCombo1.SelectedItem = Me.ImageCombo1.comboItems(2)

'-> Bloquer l'onglet OutLook si pas disponible
Me.SSTab1.TabEnabled(1) = OutLookOk Or IsLotus

''-> si on vient du menu, bloquer les options
If Not FromVisu Then
    Select Case Me.lvSend
        Case 0 'Fichier
            Me.Option1.Value = False
            Me.Option2.Value = False
            Me.Option3.Value = True
        Case 1 'Spool
            Me.Option1.Value = False
            Me.Option2.Value = True
            Me.Option3.Value = False
        Case 2 'Page
            Me.Option1.Value = True
            Me.Option2.Value = False
            Me.Option3.Value = False

    End Select
    Me.Option1.Enabled = False
    Me.Option2.Enabled = False
    Me.Option3.Enabled = False
End If

'-> on regarde si on trouve l'imprimante pdf
For Each Printer In Printers
    If Trim(UCase$(Printer.DeviceName)) = "ACROBAT PDFWRITER" Or _
       Trim(UCase$(Printer.DeviceName)) = "WIN2PDF" Or _
       Trim(UCase$(Printer.DeviceName)) = "ADOBE PDF" Or _
       Trim(UCase$(Printer.DeviceName)) = "PDFCREATOR" Or _
       Trim(UCase$(Printer.DeviceName)) = "ACROBAT DISTILLER" Then PrinterFound = True
Next

If Not PrinterFound Then
    Me.PicPdf.Visible = False
    Me.Label4.Visible = False
End If

'-> mettre l'envoi format html par defaut
PicHtml_Click
End Sub

Private Sub Form_Resize()

'-> on lance le redimensionnement
formResize

End Sub

Private Sub formResize()
'--> cette procedure permet de redimmensionner la feuille
On Error Resume Next

If Me.Height < 2000 Then
    Me.Height = 2000
    Exit Sub
End If

Me.Frame1.Left = 110
Me.Frame1.Width = Me.Width - 380
Me.SSTab1.Left = Me.Frame1.Left
Me.SSTab1.Width = Me.Frame1.Width
Me.SSTab1.Height = Me.Height - Me.SSTab1.Top - 460
Me.strMessage.Width = Me.SSTab1.Width - 2150
Me.strObjet.Width = Me.strMessage.Width
Me.strUrl.Width = Me.strMessage.Width
Me.strMessage.Height = Me.SSTab1.Height - Me.strMessage.Top - 2000
Me.Command1.Top = Me.strMessage.Top + Me.strMessage.Height + 250
Me.Command1.Left = Me.strMessage.Left + Me.strMessage.Width - Me.Command1.Width

Me.Text1.Width = Me.strMessage.Width
Me.Text2.Width = Me.strMessage.Width
Me.Text3.Width = Me.strMessage.Width
Me.RichTextBox1.Width = Me.strMessage.Width
Me.RichTextBox1.Height = Me.SSTab1.Height - Me.RichTextBox1.Top - 2000
Me.Check3.Top = Me.Command1.Top
Me.Command2.Top = Me.Command1.Top
Me.Command2.Left = Me.RichTextBox1.Left + Me.RichTextBox1.Width - Me.Command1.Width

Me.Text4.Width = Me.strMessage.Width
Me.Text5.Width = Me.strMessage.Width
Me.Text6.Width = Me.strMessage.Width
Me.Text7.Width = Me.strMessage.Width
Me.RichTextBox2.Width = Me.strMessage.Width
Me.RichTextBox2.Height = Me.SSTab1.Height - Me.RichTextBox2.Top - 2000
Me.Command3.Top = Me.Command1.Top
Me.Command3.Left = Me.RichTextBox2.Left + Me.RichTextBox2.Width - Me.Command1.Width
Me.Label17.Top = Me.Command3.Top

Me.Frame3.Width = Me.SSTab1.Width - 500
Me.Frame5.Width = Me.Frame3.Width
Me.Frame6.Width = Me.Frame5.Width

Me.Frame3.Height = Me.Command1.Top + Me.Command1.Height + 200
Me.Frame5.Height = Me.Frame3.Height
Me.Frame6.Height = Me.Frame3.Height

Me.ImageCombo1.Width = Me.strMessage.Width - 3700

End Sub

Private Sub Image1_Click()

On Error GoTo GestError

'---> Initialiser la session MAPI
Call InitMAPISession

'-> Afficher le carnet d'adresse
frmLib.MAPIMessages1.Compose
frmLib.MAPIMessages1.Show False

'-> Afficher le destinataire si saisi
If frmLib.MAPIMessages1.RecipAddress <> "" Then
    If Trim(Me.strUrl.Text) = "" Then
        Me.strUrl = frmLib.MAPIMessages1.RecipDisplayName
    Else
        Me.strUrl = Me.strUrl & "; " & frmLib.MAPIMessages1.RecipDisplayName
    End If
End If

GestError:

End Sub

Private Sub Image12_Click()
    PicHtml_Click
End Sub

Private Sub Image14_Click()
On Error GoTo GestError

'---> Initialiser la session MAPI
Call InitMAPISession

'-> Afficher le carnet d'adresse
frmLib.MAPIMessages1.Compose
frmLib.MAPIMessages1.Show False

'-> Afficher le destinataire si saisi
If frmLib.MAPIMessages1.RecipAddress <> "" Then
    If Trim(Me.Text2.Text) = "" Then
        Me.Text2.Text = frmLib.MAPIMessages1.RecipDisplayName
    Else
        Me.Text2.Text = Me.Text2.Text & "; " & frmLib.MAPIMessages1.RecipDisplayName
    End If
End If
GestError:

End Sub

Private Sub Image15_Click()
On Error GoTo GestError

'---> Initialiser la session MAPI
Call InitMAPISession

'-> Afficher le carnet d'adresse
frmLib.MAPIMessages1.Compose
frmLib.MAPIMessages1.Show False

'-> Afficher le destinataire si saisi
If frmLib.MAPIMessages1.RecipAddress <> "" Then
    If Trim(Me.Text3.Text) = "" Then
        Me.Text3.Text = frmLib.MAPIMessages1.RecipDisplayName
    Else
        Me.Text3.Text = Me.Text3.Text & "; " & frmLib.MAPIMessages1.RecipDisplayName
    End If
End If
GestError:

End Sub

Private Sub Image16_Click()
    picPdf_Click
End Sub

Private Sub Image17_Click()
    picNothing_Click
End Sub

Private Sub Image3_Click()
    picTurbo_Click
End Sub

Private Sub Image4_Click(Index As Integer)

'---> Envoyer un message vers Internet

Dim aLb As Libelle
Dim pFormat As String
Dim aPrinter As Printer
Dim PrinterName As String
Dim pFormatBody As Integer
Dim SpoolName As String

On Error Resume Next

'-> on initialise
ListeHtmlFile = ""
Set frmLib.MAPIMessages1 = Nothing
Err.Description = ""

'on met le sablier
Screen.MousePointer = 11

'-> V�rifier l'URL si on envoie sur WWW
If Index = 1 Then
    '-> V�rifier sue l'url soit saisi
    If Trim(Me.strUrl.Text) = "" Then
        '-> pointer sur la classe message
        Set aLb = Libelles("FRMMAIL")
        MsgBox aLb.GetCaption(17), vbExclamation + vbOKOnly, aLb.GetToolTip(17)
        Screen.MousePointer = 0
        Me.strUrl.SetFocus
        Exit Sub
    End If
End If

'-> Indiquer si on crypt le fichier
If Me.Check1.Value = 1 Then
    IsCryptedFile = True
Else
    IsCryptedFile = False
End If

'-> Ne mettre � jour les variables de traitement que si on vient de la visu et pas du menu
If FromVisu Then
    '-> Cl� du spool et du fichier
    SpoolKeyMail = aSpool.Key
    FileKeyMail = Trim(UCase$(aSpool.FileName))
    '-> Nature de l'export
    If Me.Option1.Value Then '-> Export de la page
        PageNumMail = aSpool.CurrentPage
        ExportMail = 0
    ElseIf Me.Option2.Value Then '-> Export du spool
        ExportMail = 1
    Else '-> Export du fichier
        ExportMail = 2
    End If
End If

'-> nom du spool � traiter
SpoolName = aSpool.FileName

'-> On reccupere les pages � trater dans le spool
Select Case ExportMail
    Case 0, 1
        If ExportMail = 0 Then
            '-> Envoie de la page en cours
            SpoolName = CreateDetail(aSpool, PageNumMail)
        Else
            '-> Envoie du spool en cours
            SpoolName = CreateDetail(aSpool, -1)
        End If
End Select

'-> Dans tous les cas, indiquer l'origine de l'envoie
If Index = 1 Then
    OrigineMail = 1
    '->NETPARAM : URL + chr(0) + OBJET + chr(0) + BODY
    NetParam = Me.strUrl.Text & Chr(0) & Me.strObjet.Text & Chr(0) & Me.strMessage.Text
Else
    OrigineMail = 0
End If
    
'-> Afficher le mail dans tous les cas si on va vers OutLook (sauf si html dans corps pour l instant
SendOutLook = True

'-> Format de la piece jointe
If FormatTurbo Then pFormat = "0"
If FormatHtml = True Then pFormat = "1"
If FormatNothing Then pFormat = "3"
If FormatPDF = True Then pFormat = "4"

'-> Format du corps
If Me.Check3.Value = "0" Then
    'format texte
    pFormatBody = "0"
Else
    pFormatBody = "1"
End If

'-> Positionner les variables d'environnement d'export HTML
If FormatHtml Then
    '-> Positionnement de la navigationHTML
    If Me.Option4.Value Then
        NavigationType = 0 'FrameSet
    ElseIf Me.Option5.Value Then
        NavigationType = 1 'Fichier unique
    Else
        NavigationType = 2 'Pages
    End If
End If

    '-> Inclure le num�ro de page
    IncluseSpoolNumber = CBool(Me.Check2.Value)
    
    '-> Envoyer le mail
    If Index = 1 Then
'        '-> Envoyer le mail par Internet
'        CreateHTMLToMessagerie False, Me.strUrl.Text & Chr(0) & Chr(0) & Me.strObjet.Text & Chr(0) & Me.strMessage.Text
        Select Case pFormat
            Case "0"   'Format Turbo
                SendToInternet SpoolName
            Case "4" 'format pdf
                '-> on pointe sur l'imprimante pdf
                For Each aPrinter In Printers
                    If Trim(UCase$(aPrinter.DeviceName)) = "ACROBAT PDFWRITER" Or _
                       Trim(UCase$(aPrinter.DeviceName)) = "WIN2PDF" Or _
                       InStr(1, Trim(UCase$(aPrinter.DeviceName)), "ADOBE PDF") <> 0 Or _
                       Trim(UCase$(aPrinter.DeviceName)) = "PDFCREATOR" Or _
                       Trim(UCase$(aPrinter.DeviceName)) = "ACROBAT DISTILLER" Then PrinterName = aPrinter.DeviceName
                Next
                '-> on cree d'abord le fichier pdf
                CreatePdfToMessagerie SpoolName, PrinterName
                '-> on pointe sur le nouveau fichier pdf cr��
                CreateHTMLToBody False, Me.strUrl.Text & Chr(0) & Chr(0) & Me.strObjet.Text & Chr(0) & Me.strMessage.Text & Chr(0) & Chr(0) & pFormatBody & Chr(0) & pFormat
            Case "3" 'sans piece jointe
                CreateHTMLToBody False, Me.strUrl.Text & Chr(0) & Chr(0) & Me.strObjet.Text & Chr(0) & Me.strMessage.Text & Chr(0) & Chr(0) & pFormatBody & Chr(0) & pFormat
            Case Else 'Format HTML
                CreateHTMLToMessagerie False, Me.strUrl.Text & Chr(0) & Chr(0) & Me.strObjet.Text & Chr(0) & Me.strMessage.Text & Chr(0)
        End Select
    Else
        '-> selon le format de la piece jointe
        Select Case pFormat 'format piece jointe
            Case "0"  'Format Turbo
                If pFormatBody <> "1" Then
                    '-> corps rtf
                    SendOutLook = False
                    SendToOutLook SpoolName, Me.Text2.Text & Chr(0) & Me.Text3.Text & Chr(0) & Me.Text1.Text & Chr(0) & Me.RichTextBox1.Text & Chr(0)
                Else
                    '-> corps HTML
                    CreateHTMLToBody True, Me.Text2.Text & Chr(0) & Me.Text3.Text & Chr(0) & Me.Text1.Text & Chr(0) & Me.RichTextBox1.Text & Chr(0) & Chr(0) & "1" & Chr(0) & pFormat
                End If
            Case "3" 'sans piece jointe
                CreateHTMLToBody True, Me.Text2.Text & Chr(0) & Me.Text3.Text & Chr(0) & Me.Text1.Text & Chr(0) & Me.RichTextBox1.Text & Chr(0) & Chr(0) & pFormatBody & Chr(0) & pFormat
            Case "4" 'Format Pdf
                '-> on pointe sur l'imprimante pdf
                For Each aPrinter In Printers
                    
                    If Trim(UCase$(aPrinter.DeviceName)) = "ACROBAT PDFWRITER" Or _
                       Trim(UCase$(aPrinter.DeviceName)) = "WIN2PDF" Or _
                       InStr(1, Trim(UCase$(aPrinter.DeviceName)), "ADOBE PDF") <> 0 Or _
                       Trim(UCase$(aPrinter.DeviceName)) = "PDFCREATOR" Or _
                       Trim(UCase$(aPrinter.DeviceName)) = "ACROBAT DISTILLER" Then PrinterName = aPrinter.DeviceName
                Next
            
                '-> on cree d'abord le fichier pdf
                CreatePdfToMessagerie SpoolName, PrinterName
                CreateHTMLToBody True, Me.Text2.Text & Chr(0) & Me.Text3.Text & Chr(0) & Me.Text1.Text & Chr(0) & Me.RichTextBox1.Text & Chr(0) & Chr(0) & pFormatBody & Chr(0) & pFormat
            Case Else 'Format HTML
                If pFormatBody <> "1" Then
                    '-> corps rtf
                    CreateHTMLToMessagerie True, Me.Text2.Text & Chr(0) & Me.Text3.Text & Chr(0) & Me.Text1.Text & Chr(0) & Me.RichTextBox1.Text & Chr(0)
                Else
                    '-> corps Html
                    CreateHTMLToBody True, Me.Text2.Text & Chr(0) & Me.Text3.Text & Chr(0) & Me.Text1.Text & Chr(0) & Me.RichTextBox1.Text & Chr(0) & Chr(0) & "1" & Chr(0) & pFormat
                End If
        End Select
    End If
    
'Else
'    '->Envoyer sur le r�seau
'    CreateFileToSend
'End If

'on met le sablier
Screen.MousePointer = 0

If Err.Description <> "" Then MsgBox Err.Description, vbExclamation

'-> D�charger la feuille
Unload Me

End Sub

Private Sub ImageCombo1_Change()

Select Case ImageCombo1.SelectedItem.Index
    Case 1 'MESSAGERIE INTERNET
        Me.SSTab1.TabEnabled(0) = True
        Me.SSTab1.TabEnabled(1) = False
        Me.SSTab1.TabEnabled(2) = False
        Me.SSTab1.Tab = 0
    Case 2 'messagerie ouytlook ou lotus
        Me.SSTab1.TabEnabled(0) = False
        Me.SSTab1.TabEnabled(1) = True
        Me.SSTab1.TabEnabled(2) = False
        Me.SSTab1.Tab = 1
    Case 3 'serveur de messagerie
        Me.SSTab1.TabEnabled(0) = False
        Me.SSTab1.TabEnabled(1) = False
        Me.SSTab1.TabEnabled(2) = True
        Me.SSTab1.Tab = 2
End Select

End Sub

Private Sub ImageCombo1_Click()

    Call ImageCombo1_Change

End Sub

Private Sub Label1_Click()
    picTurbo_Click
End Sub

Private Sub Label2_Click()
    PicHtml_Click
End Sub


Private Sub PicHtml_Click()

Me.Sel.Left = Me.PicHtml.Left - 20
Me.Sel.Top = Me.PicHtml.Top - 20
FormatHtml = True
FormatPDF = False
FormatNothing = False
FormatTurbo = False

Me.Check1.Value = 0
Me.Check1.Enabled = False

'-> Bloquer la gestion Internet
Me.Frame4.Enabled = True
Me.Option4.Enabled = True
Me.Option5.Enabled = True
Me.Option6.Enabled = True
Me.Check2.Enabled = True

End Sub

Private Sub picTurbo_Click()

Me.Sel.Left = Me.picTurbo.Left - 20
Me.Sel.Top = Me.picTurbo.Top - 20
FormatHtml = False
FormatPDF = False
FormatNothing = False
FormatTurbo = True

Me.Check1.Enabled = True

'-> Bloquer la gestion Internet
Me.Frame4.Enabled = False
Me.Option4.Enabled = False
Me.Option5.Enabled = False
Me.Option6.Enabled = False
Me.Check2.Enabled = True

End Sub

Private Sub picPdf_Click()

Me.Sel.Left = Me.PicPdf.Left - 20
Me.Sel.Top = Me.PicPdf.Top - 20
FormatPDF = True
FormatHtml = False
FormatNothing = False
FormatTurbo = False
Me.Check1.Enabled = False

'-> Bloquer la gestion Internet
Me.Frame4.Enabled = False
Me.Option4.Enabled = False
Me.Option5.Enabled = False
Me.Option6.Enabled = False
Me.Check2.Enabled = True

End Sub

Private Sub picNothing_Click()

Me.Sel.Left = Me.PicNothing.Left - 20
Me.Sel.Top = Me.PicNothing.Top - 20
FormatNothing = True
FormatPDF = False
FormatHtml = False
FormatTurbo = False
Me.Check1.Enabled = False

'-> Bloquer la gestion Internet
Me.Frame4.Enabled = False
Me.Option4.Enabled = False
Me.Option5.Enabled = False
Me.Option6.Enabled = False
Me.Check2.Enabled = False
End Sub

Private Sub Winsock1_DataArrival(ByVal bytesTotal As Long)
'--> ici sont g�r�e les r�ponses du composant winsock
Dim DonneesRecues As String
Dim strMessage As String

Winsock1.GetData DonneesRecues

'-> bon on a recu une r�ponse
ReponseFaite = True
'Exit Sub

Select Case iStep
    Case 1 '-> connexion au serveur
        If InStr(1, DonneesRecues, "220") Then
            ReponseFaite = True
            iStep = 2
        Else
            strMessage = "Impossible de se connecter au serveur SMTP" & Chr(13) & DonneesRecues
        End If
    Case 2 '-> controle de la connexion
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
        Else
            strMessage = "Connexion refus�e par le serveur SMTP" & Chr(13) & DonneesRecues
        End If
    Case 3 '-> adresse mail de l'envoyeur et du destinataire
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
            iStep = 4
        Else
            strMessage = "Adresse mail de l'envoyeur non reconnue par le serveur SMTP" & Chr(13) & DonneesRecues
        End If
    Case 4 '-> adresse mail de l'envoyeur et du destinataire
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
        Else
            strMessage = "Adresse mail du destinataire non reconnue par le serveur SMTP" & Chr(13) & DonneesRecues
        End If
    Case 5  '-> Envoi des donnees
        If InStr(1, DonneesRecues, "354 ") Then
            ReponseFaite = True
        Else
            strMessage = "Erreur lors de l'envoi de l'ent�te du mail" & Chr(13) & DonneesRecues
        End If
    Case 6
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
            iStep = 7
        Else
            strMessage = "Erreur lors de l'envoi du contenu du mail" & Chr(13) & DonneesRecues
        End If
    Case 7 '-> fermeture de la connexion
        If InStr(1, DonneesRecues, "221 ") Then ReponseFaite = True
End Select

If strMessage <> "" Then
    Me.MousePointer = 0
    MsgBox strMessage
End If

End Sub


