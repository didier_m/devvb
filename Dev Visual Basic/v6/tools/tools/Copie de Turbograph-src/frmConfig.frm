VERSION 5.00
Begin VB.Form frmConfig 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Configuration"
   ClientHeight    =   3795
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6690
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3795
   ScaleWidth      =   6690
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox Text3 
      Height          =   285
      Left            =   2280
      TabIndex        =   20
      ToolTipText     =   "exemple : smtp.mail.yahoo.fr"
      Top             =   2040
      Width           =   4335
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Afficher le fichier trace"
      Height          =   615
      Left            =   120
      TabIndex        =   19
      Top             =   3120
      Width           =   1095
   End
   Begin VB.CheckBox Check6 
      Caption         =   "Check1"
      Height          =   255
      Left            =   1200
      TabIndex        =   17
      Top             =   2760
      Width           =   255
   End
   Begin VB.CheckBox Check5 
      Caption         =   "Check1"
      Height          =   255
      Left            =   5160
      TabIndex        =   15
      Top             =   2400
      Width           =   255
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Supprimer les images temporaires"
      Height          =   615
      Left            =   1320
      TabIndex        =   14
      Top             =   3120
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CheckBox Check4 
      Caption         =   "Check1"
      Height          =   255
      Left            =   2880
      TabIndex        =   12
      Top             =   2400
      Width           =   255
   End
   Begin VB.CheckBox Check3 
      Caption         =   "Check1"
      Height          =   255
      Left            =   1200
      TabIndex        =   10
      Top             =   2400
      Width           =   255
   End
   Begin VB.CheckBox Check2 
      Caption         =   "Check1"
      Height          =   255
      Left            =   2280
      TabIndex        =   6
      Top             =   1680
      Width           =   255
   End
   Begin VB.TextBox Text2 
      Height          =   285
      Left            =   2280
      TabIndex        =   5
      Top             =   1320
      Width           =   4335
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   2280
      TabIndex        =   4
      Top             =   960
      Width           =   4335
   End
   Begin VB.CommandButton Command1 
      Height          =   615
      Left            =   6000
      Picture         =   "frmConfig.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   3120
      Width           =   615
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   255
      Left            =   2280
      TabIndex        =   1
      Top             =   600
      Width           =   255
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   2280
      TabIndex        =   0
      Top             =   240
      Width           =   4320
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "Ne pas utiliser turboPDF"
      Height          =   255
      Left            =   120
      TabIndex        =   22
      Top             =   1680
      Width           =   1935
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Serveur de messagerie SMTP"
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   2040
      Width           =   2175
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Mode trace"
      Height          =   255
      Left            =   120
      TabIndex        =   18
      Top             =   2760
      Width           =   975
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "V�rifier les mises � jour"
      Height          =   255
      Left            =   3360
      TabIndex        =   16
      Top             =   2400
      Width           =   1695
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Navigation"
      Height          =   255
      Left            =   1800
      TabIndex        =   13
      Top             =   2400
      Width           =   975
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Plein �cran"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   2400
      Width           =   975
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Options du turboPDF"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   1320
      Width           =   2175
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Site de t�l�chargement"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   960
      Width           =   2055
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Imprimante par d�faut"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   240
      Width           =   1935
   End
   Begin VB.Label label 
      BackStyle       =   0  'Transparent
      Caption         =   "Impression directe"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   1935
   End
End
Attribute VB_Name = "frmConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
'-> on enregistre dans le turbograph.ini de la version internet la config
'-> imprimante
SetIniString "PRINTER", "DEFAULT", App.Path & "\Turbograph.ini", Me.Combo1.Text
'-> impression automatique
SetIniString "PRINTER", "DIRECT", App.Path & "\Turbograph.ini", Me.Check1.Value
'-> site internet pour les images
SetIniString "SITE", "HTTP", App.Path & "\Turbograph.ini", Me.Text1.Text
'-> pour la ligne de commande pdf
SetIniString "PDF", "CMD", App.Path & "\Turbograph.ini", Me.Text2.Text
'-> dans le cas ou on a plusieurs imprimantes pdf donner la priorit� a turbopdf
SetIniString "PDF", "TURBOPDF", App.Path & "\Turbograph.ini", Me.Check2.Value
'-> pour sp�cifier si on ouvre turbograph en plein �cran
SetIniString "PARAM", "FULLSCREEN", App.Path & "\Turbograph.ini", Me.Check3.Value
'-> pour sp�cifier si on ouvre le menu navigation par d�faut
SetIniString "PARAM", "NAVIGA", App.Path & "\Turbograph.ini", Me.Check4.Value
'-> pour sp�cifier si on v�rifie les mises � jour
SetIniString "PARAM", "UPDATE", App.Path & "\Turbograph.ini", Me.Check5.Value
'-> pour v�rifier si on doit tracer le spool
SetIniString "PARAM", "TRACE", App.Path & "\Turbograph.ini", Me.Check6.Value
'-> adresse du serveur SMTP
SetIniString "PARAM", "SMTP", App.Path & "\Turbograph.ini", Me.Text3.Text

TurboGraphWebPrinter = Me.Combo1.Text
TurboGraphWebDirect = Me.Check1.Value
Unload Me
End Sub

Private Sub Command3_Click()

'-> si on a un fichier trace
If FileExist(Mouchard) Then
    'Close #hdlMouchard
    ShellExecute Me.hwnd, "Open", Mouchard, vbNullString, strBuffer, 1
End If

End Sub

Private Sub Form_Load()
Dim aPrint As Printer
Dim i As Integer

On Error Resume Next

'-> Charger la liste des imprimantes
For Each aPrint In Printers
    Me.Combo1.AddItem aPrint.DeviceName
    If aPrint.DeviceName = Printer.DeviceName Then _
        Me.Combo1.ListIndex = Me.Combo1.ListCount - 1
Next
'-> On ajoute l'imprimante ecran
Me.Combo1.AddItem "SCREEN"

'-> on pointe sur l'imprimante defaut
If TurboGraphWebPrinter <> "" Then
    For i = 1 To Me.Combo1.ListCount
        If Me.Combo1.List(i) = TurboGraphWebPrinter Then Me.Combo1.Text = TurboGraphWebPrinter
    Next
End If

'-> on regarde si l'impression est directe
If TurboGraphWebDirect = "1" Then Me.Check1.Value = 1

'-> on charge le param�trage
Me.Text1.Text = GetIniString("SITE", "HTTP", App.Path & "\Turbograph.ini", False)
Me.Text2.Text = GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False)
Me.Text3.Text = GetIniString("PARAM", "SMTP", App.Path & "\Turbograph.ini", False)
Me.Check2.Value = GetIniString("PDF", "TURBOPDF", App.Path & "\Turbograph.ini", False)
Me.Check3.Value = GetIniString("PARAM", "FULLSCREEN", App.Path & "\Turbograph.ini", False)
Me.Check4.Value = GetIniString("PARAM", "NAVIGA", App.Path & "\Turbograph.ini", False)
Me.Check5.Value = GetIniString("PARAM", "UPDATE", App.Path & "\Turbograph.ini", False)
Me.Check6.Value = GetIniString("PARAM", "TRACE", App.Path & "\Turbograph.ini", False)

End Sub


