Attribute VB_Name = "M_Function"
Option Explicit
  Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
  Private Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" (ByVal lpPrevWndFunc As Long, ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long

  Private Type POINTAPI
    X As Long
    Y As Long
  End Type
  
  Private Type MINMAXINFO
    ptReserved      As POINTAPI
    ptMaxSize       As POINTAPI
    ptMaxPosition   As POINTAPI
    ptMinTrackSize  As POINTAPI
    ptMaxTrackSize  As POINTAPI
  End Type
  
  'Const pour limiter le resize de la feuille
  Private Const GWL_WNDPROC = (-4)
  Private Const WM_GETMINMAXINFO = &H24
  
  Public Adress_Proc As Long 'Adresse de la proc�dure Windows
  
  Public MinX As Single 'Width Minimume
  Public MinY As Single 'Height Minimume
  
Public Function Hook_Process(ByVal hWnd As Long, ByVal uMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    Dim MinMax As MINMAXINFO
    
    'Intercepte le Message Windows de redimensionnement de fen�tre
    If uMsg = WM_GETMINMAXINFO Then
        Call CopyMemory(MinMax, ByVal lParam, Len(MinMax))
        MinMax.ptMinTrackSize.X = MinX \ Screen.TwipsPerPixelX
        MinMax.ptMinTrackSize.Y = MinY \ Screen.TwipsPerPixelY

        Call CopyMemory(ByVal lParam, MinMax, Len(MinMax))
        'Code de retour pour signaler � Windows que le traitement s'est correctement effectu�
        Hook_Process = 1
        Exit Function
    End If
    
    'Laisse les autres Messages � traiter � Windows
    Hook_Process = CallWindowProc(Adress_Proc, hWnd, uMsg, wParam, lParam)
End Function
