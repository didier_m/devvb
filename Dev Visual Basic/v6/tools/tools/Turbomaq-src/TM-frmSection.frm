VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmSection 
   ClientHeight    =   5040
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8970
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   NegotiateMenus  =   0   'False
   ScaleHeight     =   5040
   ScaleWidth      =   8970
   Begin MSComDlg.CommonDialog CmmColor 
      Left            =   8160
      Top             =   2880
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox BitMap 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   975
      Index           =   0
      Left            =   7440
      MouseIcon       =   "TM-frmSection.frx":0000
      MousePointer    =   99  'Custom
      ScaleHeight     =   945
      ScaleWidth      =   1065
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   1080
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.PictureBox ContainerRtf 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      Height          =   1095
      Left            =   120
      ScaleHeight     =   1095
      ScaleWidth      =   2895
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   1080
      Width           =   2895
      Begin RichTextLib.RichTextBox RangText 
         Height          =   1095
         Left            =   0
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   0
         Width           =   2895
         _ExtentX        =   5106
         _ExtentY        =   1931
         _Version        =   393217
         BackColor       =   16777215
         BorderStyle     =   0
         HideSelection   =   0   'False
         Appearance      =   0
         AutoVerbMenu    =   -1  'True
         TextRTF         =   $"TM-frmSection.frx":030A
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.PictureBox Cadre 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   1455
      Index           =   0
      Left            =   3240
      MouseIcon       =   "TM-frmSection.frx":038E
      MousePointer    =   99  'Custom
      ScaleHeight     =   97
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   174
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   1080
      Visible         =   0   'False
      Width           =   2610
      Begin RichTextLib.RichTextBox CadreTexte 
         Height          =   1215
         Index           =   0
         Left            =   120
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   120
         Width           =   2415
         _ExtentX        =   4260
         _ExtentY        =   2143
         _Version        =   393217
         BackColor       =   16777215
         BorderStyle     =   0
         ScrollBars      =   2
         MousePointer    =   3
         Appearance      =   0
         AutoVerbMenu    =   -1  'True
         TextRTF         =   $"TM-frmSection.frx":0698
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.VScrollBar Vertical 
      Height          =   1095
      Left            =   6960
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   1320
      Width           =   255
   End
   Begin VB.HScrollBar Horizontal 
      Height          =   255
      Left            =   3720
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   2520
      Width           =   1335
   End
   Begin MSComctlLib.Toolbar Toolbar2 
      Align           =   1  'Align Top
      Height          =   450
      Left            =   0
      TabIndex        =   4
      Top             =   450
      Width           =   8970
      _ExtentX        =   15822
      _ExtentY        =   794
      ButtonWidth     =   820
      ButtonHeight    =   741
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      ImageList       =   "imgBtOk"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   15
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "InsertCadre"
            ImageIndex      =   14
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "DelCadre"
            ImageIndex      =   15
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "BordureCadre"
            ImageIndex      =   13
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   11
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "1Pt"
                  Text            =   "1 pt"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "2Pt"
                  Text            =   "2 pt"
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "3Pt"
                  Text            =   "3 Pt"
               EndProperty
               BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "4Pt"
                  Text            =   "4 Pt"
               EndProperty
               BeginProperty ButtonMenu5 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "5Pt"
                  Text            =   "5 Pt"
               EndProperty
               BeginProperty ButtonMenu6 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "6Pt"
                  Text            =   "6 Pt"
               EndProperty
               BeginProperty ButtonMenu7 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "7Pt"
                  Text            =   "7 Pt"
               EndProperty
               BeginProperty ButtonMenu8 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "8Pt"
                  Text            =   "8 Pt"
               EndProperty
               BeginProperty ButtonMenu9 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "9Pt"
                  Text            =   "9 Pt"
               EndProperty
               BeginProperty ButtonMenu10 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "10Pt"
                  Text            =   "10 Pt"
               EndProperty
               BeginProperty ButtonMenu11 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Round"
                  Text            =   "Arrondis"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "RedimCadre"
            ImageIndex      =   32
            Style           =   1
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "DupCadre"
            ImageIndex      =   26
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "InsertBmp"
            ImageIndex      =   29
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "DelBmp"
            ImageIndex      =   30
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FileBmp"
            ImageIndex      =   18
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ContourBmp"
            ImageIndex      =   13
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "RedimBmp"
            ImageIndex      =   32
            Style           =   1
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Autosize"
            ImageIndex      =   24
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "DupBmp"
            ImageIndex      =   25
         EndProperty
         BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Align"
            ImageIndex      =   31
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin MSComctlLib.ImageList imgBtOk 
      Left            =   6000
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   24
      ImageHeight     =   22
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   32
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":071C
            Key             =   "Left"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":08B6
            Key             =   "Center"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":0A50
            Key             =   "Right"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":0BEA
            Key             =   "Bold"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":0D84
            Key             =   "Italic"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":0F1E
            Key             =   "Underline"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":10B8
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":1512
            Key             =   "Bas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":182C
            Key             =   "Droite"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":1B46
            Key             =   "Gauche"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":1E60
            Key             =   "Haut"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":217A
            Key             =   "Sans"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":2494
            Key             =   "Plein"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":27AE
            Key             =   "Cadre"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":2E90
            Key             =   "DelCadre"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":3572
            Key             =   "Image"
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":3FCC
            Key             =   "DelImage"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":4A26
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":4BC0
            Key             =   "BackColor"
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":4D5A
            Key             =   "FontColor"
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":4F54
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":5446
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":5640
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":5B32
            Key             =   "AutoSize"
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":5D2C
            Key             =   "DupBmp"
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":5F26
            Key             =   "DupCadre"
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":6608
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":71BA
            Key             =   "Align"
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":7D6C
            Key             =   ""
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":844E
            Key             =   ""
         EndProperty
         BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":8B30
            Key             =   ""
         EndProperty
         BeginProperty ListImage32 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSection.frx":9212
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   450
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8970
      _ExtentX        =   15822
      _ExtentY        =   794
      ButtonWidth     =   820
      ButtonHeight    =   741
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      ImageList       =   "imgBtOk"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   14
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   1
            Style           =   2
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   2
            Style           =   2
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   3
            Style           =   2
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   4
            Style           =   1
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   5
            Style           =   1
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   6
            Style           =   1
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   20
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   12
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   19
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   1
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Text            =   "Personnalis�"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin VB.ComboBox ComboSize 
         Height          =   315
         Left            =   7920
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   60
         Width           =   975
      End
      Begin VB.ComboBox ComboFont 
         Height          =   315
         Left            =   5040
         Sorted          =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   60
         Width           =   2775
      End
   End
End
Attribute VB_Name = "frmSection"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-> Pointe sur l'objet parent Section en cours
Public SectionCours As Section
'-> Indique le nom de l'objet en cours dans la section : utilis� pour les _
changements d'objet parents
Private NomLocalObject As String
'-> Cette variable pointe sur le control Rtf en cours d'utilisation dans la section
Public RtfCours As RichTextBox
'-> Cette variable indique si on est en train de dessiner dans la section
Private IsDesign As Boolean
'-> Cette varaible indique qu'un objet et en cours de redimension
Private IsRedim As Boolean
Private RedimObj
'-> Cette variable indique si on a commenc� le trac� du rectangle
Private IsClickDown As Boolean
'-> Cette variable indique l'endroit ou on � cliqu� dans le Rtf en pixels
Private StartPoint As POINTAPI
'-> Cette variable indique l'endroit ou on � cliqu� dans le Rtf en twips
Private StartPointTwips As POINTAPI
Private hdlPen As Long 'Handle du stylo pour dessiner
Private oldPen As Long 'handle de l'ancien stylo du Rtf
Private hdcRtf As Long 'HDC du RTf de dessin

Private Sub BitMap_Click(Index As Integer)

'-> Mettre au premier plan
Me.BitMap(Index).ZOrder
ObjChanged

End Sub

Private Sub BitMap_GotFocus(Index As Integer)

'-> Mettre au premier plan
Me.BitMap(Index).ZOrder
ObjChanged

End Sub

Private Sub BitMap_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'-> stocker la position de d�part
StartPointTwips.x = x
StartPointTwips.y = y

End Sub

Private Sub BitMap_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

If TypeOf ObjCours Is ImageObj Then
    If ObjCours.Locked Then Exit Sub
End If

DoEvents

If Button = vbLeftButton And Not IsRedim Then
    '-> Deplacer l'objet
    Me.BitMap(Index).Left = Me.BitMap(Index).Left + (x - StartPointTwips.x)
    Me.BitMap(Index).Top = Me.BitMap(Index).Top + (y - StartPointTwips.y)
    Exit Sub
End If

End Sub

Private Sub BitMap_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

On Error Resume Next

If Button = vbLeftButton And Not IsRedim And Not ObjCours.Locked Then
    '-> Mettre � jour les propri�t�s
    ObjCours.Left = Me.ScaleX(Me.BitMap(Index).Left, 1, 7)
    ObjCours.Top = Me.ScaleY(Me.BitMap(Index).Top, 1, 7)
    '-> Maj de la feuille de propri�t�
    If frmPropVisible Then frmProp.DisplayProperties
    '-> Deplacer les esclaves
    ObjCours.MoveSlave
    
End If

End Sub

Private Sub Cadre_Click(Index As Integer)
    ObjChanged
End Sub

Private Sub Cadre_GotFocus(Index As Integer)

'-> Mettre au premier plan
Me.Cadre(Index).ZOrder
ObjChanged

End Sub

Private Sub Cadre_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'-> stocker la position de d�part
StartPointTwips.x = x
StartPointTwips.y = y

End Sub

Private Sub Cadre_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

If TypeOf ObjCours Is Cadre Then
    If ObjCours.Locked Then Exit Sub
End If

DoEvents

If Button = vbLeftButton And Not IsRedim Then
    '-> Deplacer l'objet
    Me.Cadre(Index).Left = Me.Cadre(Index).Left + (x - StartPointTwips.x)
    Me.Cadre(Index).Top = Me.Cadre(Index).Top + (y - StartPointTwips.y)
    Exit Sub
End If

End Sub

Private Sub Cadre_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

Dim Res As Long

DoEvents

If Button = vbLeftButton And Not IsRedim Then
    '-> Mettre � jour les propri�t�s
    ObjCours.Left = Me.ScaleX(Me.Cadre(Index).Left, 1, 7)
    ObjCours.Top = Me.ScaleY(Me.Cadre(Index).Top, 1, 7)
    '-> Maj de la feuille de propri�t�
    If frmPropVisible Then frmProp.DisplayProperties
    Me.CadreTexte(Index).Visible = True
    '-> D�placer les esclaves
    ObjCours.MoveSlave
End If

    
End Sub

Private Sub Cadre_Resize(Index As Integer)
    Me.Cadre(Index).Cls
End Sub

Private Sub CadreTexte_Click(Index As Integer)

'-> Mettre le cadre au premier rang
Me.Cadre(Index).ZOrder
ObjChanged

End Sub

Private Sub CadreTexte_GotFocus(Index As Integer)
    ObjChanged
End Sub

Private Sub CadreTexte_SelChange(Index As Integer)

DisplayProperties

End Sub

Private Sub ComboFont_Click()
    
'---> Mettre � jour les propri�t�s de police de la section
RtfCours.SelFontName = Me.ComboFont.Text
    
    
End Sub

Private Sub ComboSize_Click()
     '---> Appliquer le changement de taille de la police
    RtfCours.SelFontSize = CDec(Me.ComboSize.Text)
    Me.RtfCours.SetFocus
End Sub


Private Sub ComboSize_KeyPress(KeyAscii As Integer)
    If Not LimitSaisie(KeyAscii) Then KeyAscii = 0
End Sub

Private Sub ContainerRtf_Resize()
    
'-> Donner la m�me largeur au controle Rtf
Me.RangText.Width = Me.ContainerRtf.Width
Me.RangText.Height = Me.ContainerRtf.Height

End Sub


Private Sub Form_Activate()
        
ObjChanged

End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim i As Integer
Dim aMaq As Maquette

'---> Load des messprog
Set aLb = Libelles("FRMSECTION")

Me.Caption = aLb.GetCaption(1) & SectionCours.Nom & "]"
Me.Toolbar1.Buttons(1).ToolTipText = aLb.GetCaption(2)
Me.Toolbar1.Buttons(2).ToolTipText = aLb.GetCaption(3)
Me.Toolbar1.Buttons(3).ToolTipText = aLb.GetCaption(4)
Me.Toolbar1.Buttons(5).ToolTipText = aLb.GetCaption(5)
Me.Toolbar1.Buttons(6).ToolTipText = aLb.GetCaption(6)
Me.Toolbar1.Buttons(7).ToolTipText = aLb.GetCaption(7)
Me.Toolbar1.Buttons(9).ToolTipText = aLb.GetCaption(8)
Me.Toolbar1.Buttons(11).ToolTipText = aLb.GetCaption(9)
Me.Toolbar1.Buttons(13).ToolTipText = aLb.GetCaption(10)
Me.ComboFont.ToolTipText = aLb.GetCaption(11)
Me.ComboSize.ToolTipText = aLb.GetCaption(12)
Me.Toolbar2.Buttons(1).ToolTipText = aLb.GetCaption(13)
Me.Toolbar2.Buttons(2).ToolTipText = aLb.GetCaption(14)
Me.Toolbar2.Buttons(3).ToolTipText = aLb.GetCaption(15)
Me.Toolbar2.Buttons(4).ToolTipText = aLb.GetCaption(16)
Me.Toolbar2.Buttons(5).ToolTipText = aLb.GetCaption(23)
Me.Toolbar2.Buttons(7).ToolTipText = aLb.GetCaption(17)
Me.Toolbar2.Buttons(8).ToolTipText = aLb.GetCaption(18)
Me.Toolbar2.Buttons(9).ToolTipText = aLb.GetCaption(19)
Me.Toolbar2.Buttons(10).ToolTipText = aLb.GetCaption(20)
Me.Toolbar2.Buttons(11).ToolTipText = aLb.GetCaption(21)
Me.Toolbar2.Buttons(12).ToolTipText = aLb.GetCaption(22)
Me.Toolbar2.Buttons(13).ToolTipText = aLb.GetCaption(23)
Me.Toolbar2.Buttons(15).ToolTipText = aLb.GetCaption(24)

'---> Charger les polices
For i = 1 To Screen.FontCount - 1
    Me.ComboFont.AddItem Screen.Fonts(i)
Next

'-> Ajout des tailles de police
For i = 6 To 76 Step 1
    Me.ComboSize.AddItem i
Next

'-> Dimensions du Rtf
Me.ContainerRtf.Width = Me.ScaleX(SectionCours.Largeur, 7, 1)
Me.ContainerRtf.Height = Me.ScaleY(SectionCours.Hauteur, 7, 1)

'-> Dimensions de la feuille
Me.Width = Me.ContainerRtf.Width + Me.ContainerRtf.Left * 3

'-> Largeur par d�faut
Set aMaq = Maquettes("MAQUETTE")
Me.Width = Me.ScaleX(aMaq.Largeur - aMaq.MargeLeft * 2 + 2, 7, 1)
Me.Height = Me.ScaleY(DefHauteurSection + 2, 7, 1) + Me.Toolbar1.Height + Me.Toolbar2.Height

'-> Initialiser la section
ObjChanged True

'-> Lib�rer les pointeurs
Set aMaq = Nothing
Set aLb = Nothing

End Sub
Private Sub ObjChanged(Optional Force As Boolean)

'---> Cette proc�dure a pour but pour g�rer les diff�rentes icones selon l'objet en cours _
'de s�lection

Dim ActiveName As String
Dim TypeObj As String
Dim NameObj As String
Dim aSection As Section
Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim aMaq As Maquette
Dim aNode As Node
Dim TypeObjCour As String

On Error Resume Next

'-> R�cup�ration de l'objet en cours d'affichage par la propri�t� ActiveControl dans la feuille
If Force Then
    TypeObj = "SCT"
    NameObj = SectionCours.Nom
    TypeObjCour = "TTT"
Else
    ActiveName = Me.ActiveControl.Tag
    TypeObj = Entry(1, ActiveName, "-")
    NameObj = Entry(2, ActiveName, "-")
    '-> R�cup�ration du type d'objet
    If TypeOf ObjCours Is Cadre Then
        TypeObjCour = "CDR"
    ElseIf TypeOf ObjCours Is ImageObj Then
        TypeObjCour = "BMP"
    ElseIf TypeOf ObjCours Is Tableau Then
        TypeObjCour = "TB"
    ElseIf TypeOf ObjCours Is Block Then
        TypeObjCour = "BLK"
    Else
        TypeObjCour = "SCT"
    End If
End If
'-> Faire du nouvel objet l'objet courant
Select Case UCase$(TypeObj)

    Case "SCT"
    
        '-> Quitter si on est d�ja la section en cours
        If UCase$(ObjCours.Nom) = UCase$(SectionCours.Nom) And _
           UCase$(TypeObjCour) = TypeObj Then Exit Sub
    
        '-> S�lectionner le bon Node
        Set aNode = frmNaviga.TreeNaviga.Nodes(UCase$(SectionCours.Nom))
        aNode.Selected = True
        Set aNode = Nothing
        
        '-> Gestion des redimensions
        GestDelete NameObj
        
        '-> Faire de la section l'objet en cours
        Set ObjCours = SectionCours
        NomLocalObject = SectionCours.Nom
               
        '-> S�lectionner le controlRtf par d�faut
        Set RtfCours = Me.RangText
               
        '-> Afficher les barres d'outils
        EnabledCadre False
        EnabledBmp False
        EnabledFontBar True
        Me.Toolbar2.Buttons("InsertCadre").Enabled = True
        Me.Toolbar2.Buttons("InsertBmp").Enabled = True
        Me.Toolbar2.Buttons(15).Enabled = False
                              
        '-> Boutons de redimensions � 0
        Me.Toolbar2.Buttons("RedimCadre").Value = tbrUnpressed
        Me.Toolbar2.Buttons("RedimBmp").Value = tbrUnpressed
                                      
        '-> Mettre � jour la feuille de propri�t� si elle est active
        If frmPropVisible Then frmProp.Form_Load
                               
    Case "CDR"
    
        '-> Pointer sur le cadre
        Set aCadre = SectionCours.Cadres(UCase$(NameObj))
        
        '-> V�rification si on est dans la m�me section
        If UCase$(GetSectionName(ObjCours)) = UCase$(aCadre.SectionName) Then
            '-> On n'a pas chang� de section donc v�rifier si le control actif _
            est toujours le m�me
            If UCase$(ObjCours.Nom) = UCase$(NameObj) And _
               UCase$(TypeObjCour) = TypeObj Then
                '-> L'objet n'a pas change : quitter
                Exit Sub
            End If
        Else
            '-> On a effectu� un changement de section. V�rifier si l'objet actif _
            est le m�me qu'avant le changement
            'If UCase$(NomLocalObject) = UCase$(NameObj) Then Exit Sub
        End If
    
        '-> Gestion des redimensions
        GestDelete NameObj
                
        '-> Modifier l'objet en cours
        Set ObjCours = aCadre
        NomLocalObject = aCadre.Nom
        
        '-> S�lectionner le controle Rtf par d�faut
        Set RtfCours = Me.CadreTexte(aCadre.IdAffichage)

        '-> S�lectionner le bon node dans le treeView
        Set aNode = frmNaviga.TreeNaviga.Nodes(UCase$(SectionCours.Nom) & "|" & UCase$(NameObj))
        aNode.Selected = True

        '-> gestion des barres d'outils
        EnabledCadre True
        EnabledBmp False
        EnabledFontBar True
        Me.Toolbar2.Buttons(15).Enabled = True
        Me.Toolbar2.Buttons("InsertCadre").Enabled = False
        Me.Toolbar2.Buttons("InsertBmp").Enabled = False
        
        '-> Indiquer l'objet pour la feuille de propri�t�
        If frmPropVisible Then frmProp.Form_Load
               
    Case "BMP"
    
        '-> Pointer sur le cadre
        Set aBmp = SectionCours.Bmps(UCase$(NameObj))
        
        '-> V�rification si on est dans la m�me section
        If UCase$(GetSectionName(ObjCours)) = UCase$(aBmp.SectionName) Then
            '-> On n'a pas chang� de section donc v�rifier si le control actif _
            est toujours le m�me
            If UCase$(ObjCours.Nom) = UCase$(NameObj) And _
               UCase$(TypeObjCour) = TypeObj Then
                '-> L'objet n'a pas change : quitter
                Exit Sub
            End If
        Else
            '-> On a effectu� un changement de section. V�rifiezr si l'objet actif _
            est le m�me qu'avant le changement
            If UCase$(NomLocalObject) = UCase$(NameObj) Then Exit Sub
        End If
    
        '-> Gestion des redimensions
        GestDelete NameObj
        
        '-> Modifier l'objet en cours
        Set ObjCours = aBmp
        NomLocalObject = aBmp.Nom
        
        '-> S�lectionner le bon node dans le treeView
        Set aNode = frmNaviga.TreeNaviga.Nodes(UCase$(SectionCours.Nom) & "|" & UCase$(NameObj))
        aNode.Selected = True

        '-> gestion des barres d'outils
        EnabledCadre False
        EnabledBmp True
        EnabledFontBar False
        Me.Toolbar2.Buttons("InsertCadre").Enabled = False
        Me.Toolbar2.Buttons("InsertBmp").Enabled = False
        Me.Toolbar2.Buttons(15).Enabled = True
        
        '-> Indiquer l'objet pour la feuille de propri�t�
        If frmPropVisible Then frmProp.Form_Load
        
End Select

Set aMaq = Nothing
Set aSection = Nothing
Set aCadre = Nothing
Set aBmp = Nothing
Set aNode = Nothing


End Sub

Public Sub EnabledFontBar(ByVal En As Boolean)

    Me.Toolbar1.Enabled = En
    Me.Toolbar1.Buttons(1).Enabled = En
    Me.Toolbar1.Buttons(2).Enabled = En
    Me.Toolbar1.Buttons(3).Enabled = En
    Me.Toolbar1.Buttons(5).Enabled = En
    Me.Toolbar1.Buttons(6).Enabled = En
    Me.Toolbar1.Buttons(7).Enabled = En
    Me.Toolbar1.Buttons(9).Enabled = En
    Me.Toolbar1.Buttons(11).Enabled = En
    Me.Toolbar1.Buttons(13).Enabled = En
    Me.ComboFont.Enabled = En
    Me.ComboSize.Enabled = En
    
    
End Sub

Public Sub EnabledCadre(ByVal En As Boolean)

'-> Cadre
Me.Toolbar2.Buttons(1).Enabled = False
Me.Toolbar2.Buttons(1).Value = tbrUnpressed
Me.Toolbar2.Buttons(2).Enabled = En '-> Suppression cadre
Me.Toolbar2.Buttons(3).Enabled = En '-> Bordures Cadre
Me.Toolbar2.Buttons(4).Enabled = En '-> Redimension
Me.Toolbar2.Buttons(5).Enabled = En '-> Dupliquer

End Sub

Public Sub EnabledBmp(ByVal En As Boolean)

'-> Images
Me.Toolbar2.Buttons(7).Enabled = En
Me.Toolbar2.Buttons(7).Value = tbrUnpressed
Me.Toolbar2.Buttons(8).Enabled = En
Me.Toolbar2.Buttons(9).Enabled = En
Me.Toolbar2.Buttons(10).Enabled = En
Me.Toolbar2.Buttons(11).Enabled = En
Me.Toolbar2.Buttons(12).Enabled = En
Me.Toolbar2.Buttons(13).Enabled = En

End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

If EraseFrm Then
Else
    If UnloadMode <> 4 Then Cancel = 1
    Me.Hide
End If


End Sub

Private Sub DisplayProperties()

'---> Cette proc�dure affiche les propri�t�s de l'objet en cours dans la barre _
d'outils

On Error Resume Next

If TypeOf ObjCours Is Section Or TypeOf ObjCours Is Cadre Then
    
    '-> Afficher les propri�t�s d'alignement
    If RtfCours.SelAlignment = 0 Then
        Me.Toolbar1.Buttons(1).Value = tbrPressed
    ElseIf RtfCours.SelAlignment = 1 Then
        Me.Toolbar1.Buttons(3).Value = tbrPressed
    Else
        Me.Toolbar1.Buttons(2).Value = tbrPressed
    End If
    
    '-> Propri�t�s de la police
    If RtfCours.SelBold Then
        Me.Toolbar1.Buttons(5).Value = tbrPressed
    Else
        Me.Toolbar1.Buttons(5).Value = tbrUnpressed
    End If
    If RtfCours.SelItalic Then
        Me.Toolbar1.Buttons(6).Value = tbrPressed
    Else
        Me.Toolbar1.Buttons(6).Value = tbrUnpressed
    End If
    If RtfCours.SelUnderline Then
        Me.Toolbar1.Buttons(7).Value = tbrPressed
    Else
        Me.Toolbar1.Buttons(7).Value = tbrUnpressed
    End If
    
    '-> Afficher le nom de la police et sa taille
    Me.ComboFont.Text = RtfCours.SelFontName
    Me.ComboSize.Text = RtfCours.SelFontSize

End If

End Sub

Public Sub Form_Resize()

Dim Rect1 As Rect
Dim Rect2 As Rect
Dim Rect3 As Rect
Dim Rect4 As Rect
Dim Res As Long
Dim hMask As Single
Dim vMask As Single

On Error Resume Next

'-> Essayer de toujours centr� la feuille
If Me.ContainerRtf.Width < Me.Width Then _
    Me.ContainerRtf.Left = (Me.Width - Me.ContainerRtf.Width) / 2

'-> Positionner la feuille en haut apres la seconde barre de d�filement
Me.ContainerRtf.Top = Me.Toolbar2.Top + Me.Toolbar2.Height + Me.ScaleY(1, 7, 1)

'-> R�cup�ration des coordonn�es de la feuille
Res = GetClientRect(Me.hWnd, Rect1)
Res = GetWindowRect(Me.ContainerRtf.hWnd, Rect2)
Res = GetWindowRect(Me.Horizontal.hWnd, Rect3)
Res = GetWindowRect(Me.Vertical.hWnd, Rect4)

'---> Traitement horizontal
If Me.Toolbar1.Height + Me.Toolbar2.Height + Me.Horizontal.Height < Me.ScaleY(Rect1.Bottom - (Rect3.Bottom - Rect3.Top), 3, 1) Then
    Me.Horizontal.Top = Me.ScaleY(Rect1.Bottom - (Rect3.Bottom - Rect3.Top), 3, 1)
Else
    Me.Horizontal.Top = Me.Toolbar1.Height + Me.Toolbar2.Height + Me.Horizontal.Height
End If

Me.Horizontal.Left = 0
Me.Horizontal.ZOrder
Me.Horizontal.Width = Me.ScaleX(Rect1.Right, 3, 1) - Vertical.Width

hMask = Rect1.Right - Rect2.Right + Rect2.Left - GetSystemMetrics(45) * 4
vMask = Rect1.Bottom - Rect2.Bottom + Rect2.Top - GetSystemMetrics(45) * 4 - ScaleY(Me.Toolbar1.Height + Me.Toolbar2.Height, 1, 3)

If hMask < 0 Then
    Me.ContainerRtf.Left = 120
    Me.Horizontal.Max = Me.ScaleX(hMask, 3, 1) - Me.Vertical.Width * 2
    Me.Horizontal.Min = Me.ContainerRtf.Left
    Me.Horizontal.Value = Me.ContainerRtf.Left
    Me.Horizontal.SmallChange = (Abs(Me.Horizontal.Max - Me.Horizontal.Min) / 10) + 1
    Me.Horizontal.LargeChange = Me.Horizontal.SmallChange * 5
    Me.Horizontal.Visible = True
Else
    Me.Horizontal.Visible = False
End If

Me.Vertical.Left = Me.ScaleX(Rect1.Right - (Rect4.Right - Rect4.Left), 3, 1)
Me.Vertical.Top = Me.Toolbar1.Height + Me.Toolbar2.Height
Me.Vertical.Height = Me.ScaleY(Rect1.Bottom, 3, 1) - Me.Toolbar1.Height - Me.Toolbar2.Height - Me.Horizontal.Height
Me.Vertical.ZOrder


If vMask < 0 Then
    'Me.ContainerRtf.Top = 1080
    Me.Vertical.Max = Me.ScaleY(vMask, 3, 1) - Me.Horizontal.Height * 2
    Me.Vertical.Min = Me.ContainerRtf.Top
    Me.Vertical.Value = Me.ContainerRtf.Top
    Me.Vertical.SmallChange = (Abs(Me.Vertical.Max - Me.Vertical.Min) / 10) + 1
    Me.Vertical.LargeChange = Me.Vertical.SmallChange * 5
    Me.Vertical.Visible = True
Else
    Me.Vertical.Visible = False
End If

'-> Ajuster
If Me.Horizontal.Visible = True And Not (Me.Vertical.Visible) Then Me.Horizontal.Width = Me.Horizontal.Width + Me.Vertical.Width
If Me.Vertical.Visible = True And Not (Me.Horizontal.Visible) Then Me.Vertical.Height = Me.Vertical.Height + Me.Horizontal.Height

End Sub

Private Sub Horizontal_Change()
    Me.ContainerRtf.Left = Me.Horizontal.Value
End Sub


Private Sub RangText_Click()
    ObjChanged
End Sub

Private Sub RangText_GotFocus()
    ObjChanged
End Sub

Private Sub RangText_SelChange()
    DisplayProperties
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)


Dim aPoint As POINTAPI
Dim Res As Long

Select Case Button.Index

    Case 1 'Left
        RtfCours.SelAlignment = 0
    
    Case 2 'Centre
        RtfCours.SelAlignment = 2
    
    Case 3 'Droite
        RtfCours.SelAlignment = 1
    
    Case 5 'Gras
        RtfCours.SelBold = Button.Value
        
    Case 6 'italic
        RtfCours.SelItalic = Button.Value
    
    Case 7 'SousLigne
        RtfCours.SelUnderline = Button.Value
        
    Case 9 'FontColor
    
        Res = GetCursorPos(aPoint)
        frmPalette.Top = Me.ScaleY(aPoint.y, 3, 1)
        frmPalette.Left = Me.ScaleX(aPoint.x, 3, 1)
        frmPalette.Couleur(2).Visible = False
        frmPalette.Show vbModal
        If RetourColor <> -1 Then RtfCours.SelColor = RetourColor
        
    Case 11 'Contour
    
        If Not (TypeOf ObjCours Is Section) Then Exit Sub
        ObjCours.Contour = Not (ObjCours.Contour)
        
        If ObjCours.Contour = True Then
            Me.Toolbar1.Buttons(11).Image = "Plein"
            Me.RangText.BorderStyle = rtfFixedSingle
        Else
            Me.Toolbar1.Buttons(11).Image = "Sans"
            Me.RangText.BorderStyle = rtfNoBorder
        End If
        
        If frmPropVisible Then frmProp.DisplayProperties
        Me.ContainerRtf.SetFocus

    Case 13 'BackColor
    
        Res = GetCursorPos(aPoint)
        frmPalette.Top = Me.ScaleY(aPoint.y, 3, 1)
        frmPalette.Left = Me.ScaleX(aPoint.x, 3, 1)
        frmPalette.Couleur(2).Visible = False
        frmPalette.Show vbModal
        If RetourColor <> -1 Then
            '-> Modifier la propri�t�
            ObjCours.BackColor = RetourColor
            '-> Modifier l'aspect visuel
            RtfCours.BackColor = RetourColor
            '-> Si c'est un cadre, appliquer au picturebox associ�
            If TypeOf ObjCours Is Cadre Then
                Me.Cadre(ObjCours.IdAffichage).BackColor = RetourColor
                '-> Redessiner les bordures
                ObjCours.RefreshBordures
            End If
            If frmPropVisible Then frmProp.DisplayProperties
        End If

End Select

End Sub

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)

'---> Affichage de la boite de dialogue personnalis�e

On Error GoTo Intercept

Me.CmmColor.CancelError = True
Me.CmmColor.ShowColor

'-> Modifier la propri�t�
ObjCours.BackColor = Me.CmmColor.Color
'-> Modifier l'aspect visuel
RtfCours.BackColor = Me.CmmColor.Color
'-> Si c'est un cadre, appliquer au picturebox associ�
If TypeOf ObjCours Is Cadre Then
    Me.Cadre(ObjCours.IdAffichage).BackColor = Me.CmmColor.Color
    '-> Redessiner les bordures
    ObjCours.RefreshBordures
End If
If frmPropVisible Then frmProp.DisplayProperties


Exit Sub

Intercept:

    Exit Sub


End Sub

Private Sub Toolbar2_ButtonClick(ByVal Button As MSComctlLib.Button)

Dim aPoint As POINTAPI
Dim Res As Long
Dim aLb As Libelle
Dim Rep
Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim NomCadre As String
Dim aNode As Node
Dim MasterObject As Object
Dim Tempo As String
Dim isVar As Boolean
Dim IsAsso As Boolean
Dim aField As Field
Dim FieldRef As Field
Dim aPicture As PictureBox

On Error Resume Next

Select Case Button.Index
    
    Case 1 'Insertion cadre
    
            '-> Demander le nom du cadre
            Set aLb = Libelles("MESSAGE")
            NomCadre = InputBox(aLb.GetCaption(16), aLb.GetToolTip(16), aLb.GetCaption(17) & SectionCours.nCadre)
            '-> V�rifier la valeur saisie
            If Trim(NomCadre) = "" Then Exit Sub
            '-> V�rifier la validit� du nom
            If Not IsLegalName(NomCadre) Then
                Set aLb = Libelles("MESSAGE")
                MsgBox aLb.GetCaption(5) & Chr(13) & aLb.GetCaption(6), vbCritical + vbOKOnly, aLb.GetToolTip(6)
                Exit Sub
            End If
            '-> V�rifier qu'il n'existe pas d�ja
            If SectionCours.GetCadreExist(NomCadre) Or SectionCours.GetBmpExist(NomCadre) Then
                MsgBox aLb.GetCaption(18), vbCritical + vbOKOnly, aLb.GetToolTip(18)
                Exit Sub
            End If
            
            '-> V�rifier le mot cl�
            If Not GetValidName(NomCadre) Then
                ErrorName
                Exit Sub
            End If
            
            If InStr(1, NomCadre, "-") <> 0 Then
                Set aLb = Libelles("MESSAGE")
                MsgBox aLb.GetCaption(47), vbCritical + vbOKOnly, aLb.GetToolTip(47)
                Exit Sub
            End If
            
            Set aLb = Nothing
    
            '---> Cr�ation de l'objet cadre associ�
            Set aCadre = New Cadre
            aCadre.Nom = NomCadre
            aCadre.SectionName = SectionCours.Nom
            aCadre.Left = 0
            aCadre.Top = 0
            Set aCadre.pBox = Me.Cadre(SectionCours.nCadre - 1)
            aCadre.IdAffichage = SectionCours.nCadre - 1
            
            aCadre.BackColor = 16777215
            aCadre.MargeInterne = 0.5
            aCadre.Largeur = 4
            aCadre.Hauteur = 2
            aCadre.IdOrdreAffichage = SectionCours.AddOrdreAffichage("CDR-" & NomCadre)
                
            '---> Cr�er la repr�sentation physique
            CreatePhysiqueCadre aCadre, Me, SectionCours
                
            '---> Appliquer les bordures une fois la repr�sentation physique cr�e
            aCadre.BordureBas = True
            aCadre.BordureHaut = True
            aCadre.BordureGauche = True
            aCadre.BordureDroite = True
                
                
            '-> Cr�er l'icone dans le treeview
            aCadre.AddTreeView
                
            '-> Ajouter le nouvel objet dans la collection
            SectionCours.Cadres.Add aCadre, UCase$(NomCadre)
                
            '-> Donner le focus au cadre
            Me.CadreTexte(aCadre.IdAffichage).SetFocus
            
            '-> Lib�rer les ressources
            Set aCadre = Nothing
            Set aNode = Nothing

        
    Case 2 'Suppression
    
        Set aLb = Libelles("MESSAGE")
        Res = MsgBox(aLb.GetCaption(24), vbCritical + vbYesNo, aLb.GetToolTip(24))
        If Res = vbNo Then
        Else
            '-> Supprimer la repr�sentation physique
            Unload Me.CadreTexte(ObjCours.IdAffichage)
            Unload Me.Cadre(ObjCours.IdAffichage)
            '-> Supprimer le node dans le TreeView
            ObjCours.DelTreeView
            '-> R�ajuster les matrices d'ordre d'affichage
            SectionCours.DelOrdreAffichage ObjCours.IdOrdreAffichage
            '-> Nettoyer les alignements
            ObjCours.ClearAlign
            '-> Supprimer l'objet dans la collection de la section
            SectionCours.Cadres.Remove (UCase$(ObjCours.Nom))
            Me.RangText.SetFocus
        End If
        
    Case 3 'Bordures Cadre
    
        Res = GetCursorPos(aPoint)
        frmBordure.Left = Screen.TwipsPerPixelX * aPoint.x
        frmBordure.Top = Screen.TwipsPerPixelY * aPoint.y
        frmBordure.Show vbModal
        
        If RetourBordure <> -1 Then
            Me.Cadre(ObjCours.IdAffichage).Cls
            ObjCours.IsRoundRect = False
            Me.Cadre(ObjCours.IdAffichage).BackColor = ObjCours.BackColor
            ObjCours.GetBordureByIndex RetourBordure
        End If
                    
    Case 4 'Redim Cadre
    
        If Me.Toolbar2.Buttons(4).Value = tbrPressed Then
            '-> Blocker les barres d'outils
            EnabledFontBar False
            EnabledCadre False
            EnabledBmp False
            '-> Rendre le bouton De maj Ok
            Me.Toolbar2.Buttons(4).Enabled = True
            Me.Toolbar2.Buttons(15).Enabled = False
            '-> Autoriser la redimension
            SetRedimBorder Me.Cadre(ObjCours.IdAffichage), True
            '-> Interdire la saisie dans le cadre
            Me.CadreTexte(ObjCours.IdAffichage).Enabled = False
            '-> Indiquer que l'on est en cour de redimension
            IsRedim = True
            Set RedimObj = ObjCours
            '-> Modifier l'aspect du pointeur
            Me.Cadre(ObjCours.IdAffichage).MousePointer = 0
            '-> Donner le focus � l'objet
            Me.Cadre(ObjCours.IdAffichage).SetFocus
        Else
            DeleteRedimCadre RedimObj
        End If
        
    Case 5 'Duplication Cadre
    
        '-> Demander le nom du cadre
        Set aLb = Libelles("MESSAGE")
        NomCadre = InputBox(aLb.GetCaption(16), aLb.GetToolTip(16), aLb.GetCaption(17) & SectionCours.nCadre)
        '-> V�rifier la valeur saisie
        If Trim(NomCadre) = "" Then Exit Sub
        '-> V�rifier la validit� du nom
        If Not IsLegalName(NomCadre) Then
            Set aLb = Libelles("MESSAGE")
            MsgBox aLb.GetCaption(5) & Chr(13) & aLb.GetCaption(6), vbCritical + vbOKOnly, aLb.GetToolTip(6)
            Set aLb = Nothing
            Exit Sub
        End If
        '-> V�rifier qu'il n'existe pas d�ja
        If SectionCours.GetCadreExist(NomCadre) Or SectionCours.GetBmpExist(NomCadre) Then
            MsgBox aLb.GetCaption(18), vbCritical + vbOKOnly, aLb.GetToolTip(18)
            Set aLb = Nothing
            Exit Sub
        End If
        
        '-> V�rifier le mot cl�
        If Not GetValidName(NomCadre) Then
            ErrorName
            Exit Sub
        End If
        
        If InStr(1, NomCadre, "-") <> 0 Then
            Set aLb = Libelles("MESSAGE")
            MsgBox aLb.GetCaption(47), vbCritical + vbOKOnly, aLb.GetToolTip(47)
            Exit Sub
        End If
        
        Set aLb = Nothing
        
        '---> Cr�ation de l'objet cadre associ�
        Set aCadre = New Cadre
        aCadre.Nom = NomCadre
        aCadre.SectionName = SectionCours.Nom
        aCadre.Left = Me.ScaleX(Me.Cadre(SectionCours.nCadre - 1).Left, 1, 7)
        aCadre.Top = Me.ScaleY(Me.Cadre(SectionCours.nCadre - 1).Top, 1, 7)
        Set aCadre.pBox = Me.Cadre(SectionCours.nCadre - 1)
        aCadre.IdAffichage = SectionCours.nCadre - 1
        aCadre.BackColor = ObjCours.BackColor
        aCadre.MargeInterne = ObjCours.MargeInterne
        aCadre.Largeur = ObjCours.Largeur
        aCadre.Hauteur = ObjCours.Hauteur
        aCadre.LargeurBordure = ObjCours.LargeurBordure
        aCadre.IsRoundRect = ObjCours.IsRoundRect
        aCadre.IdOrdreAffichage = SectionCours.AddOrdreAffichage("CDR-" & NomCadre)
        
        '-> Cr�ation de la repr�sentation physique
        CreatePhysiqueCadre aCadre, Me, SectionCours
        
        '-> Dupliquer le contenu du cadre
        Me.CadreTexte(aCadre.IdAffichage).TextRTF = Me.CadreTexte(ObjCours.IdAffichage).TextRTF
        
        '-> Setting des dimensions apr�s la cr�ation de la repr�sentation physique
        aCadre.BordureBas = ObjCours.BordureBas
        aCadre.BordureHaut = ObjCours.BordureHaut
        aCadre.BordureGauche = ObjCours.BordureGauche
        aCadre.BordureDroite = ObjCours.BordureDroite
        
        If aCadre.IsRoundRect Then aCadre.SetRoundRect
        
        '-> Cr�er l'icone dans le treeview
        aCadre.AddTreeView
            
        '-> Ajouter le nouvel objet dans la collection
        SectionCours.Cadres.Add aCadre, UCase$(NomCadre)
        
        '-> Dupliquer les champs attch�s au cadre
        For Each FieldRef In ObjCours.Fields
            Set aField = New Field
            aField.Name = FieldRef.Name
            aField.FormatEdit = FieldRef.FormatEdit
            aField.Present = FieldRef.Present
            aCadre.Fields.Add aField, UCase$(aField.Name)
        Next 'Pour tous les champs
        '-> Donner le focus au nouveau cadre
        Me.CadreTexte(aCadre.IdAffichage).SetFocus
        
        '-> Lib�rer les ressources
        Set aCadre = Nothing
        Set aNode = Nothing
            
    Case 7 'Insertion Bmp
    
        If IsDesign Then
            IsDesign = False
            Me.MousePointer = 0
            Me.Toolbar2.Buttons(1).Value = tbrUnpressed
        End If
            
        '-> R�cup�rer le nom de l'image
        Set aLb = Libelles("MESSAGE")
        Rep = InputBox(aLb.GetCaption(25), aLb.GetToolTip(25), aLb.GetCaption(26) & SectionCours.nBmp)
        '-> V�rifier que l'on n' a pas cliqu� sur annuler
        If Trim(Rep) = "" Then
            Set aLb = Nothing
            Exit Sub
        End If
        '-> V�rifier la validit� du nom
        If Not IsLegalName(Rep) Then
            Set aLb = Libelles("MESSAGE")
            MsgBox aLb.GetCaption(5) & Chr(13) & aLb.GetCaption(6), vbCritical + vbOKOnly, aLb.GetToolTip(6)
            Set aLb = Nothing
            Exit Sub
        End If
        '-> V�rifier qu'il n'est pas d�ja utils�
        If SectionCours.GetBmpExist(Rep) Or SectionCours.GetCadreExist(Rep) Then
            MsgBox aLb.GetCaption(28), vbCritical + vbOKOnly, aLb.GetToolTip(28)
            Set aLb = Nothing
            Exit Sub
        End If
        
        '-> V�rifier le mot cl�
        If Not GetValidName(Rep) Then
            ErrorName
            Exit Sub
        End If

        If InStr(1, Rep, "-") <> 0 Then
            Set aLb = Libelles("MESSAGE")
            MsgBox aLb.GetCaption(47), vbCritical + vbOKOnly, aLb.GetToolTip(47)
            Exit Sub
        End If

        frmLoadBmp.Show vbModal
        
        If RetourBmp <> "" Then
            '-> on charge eventuellement l'image par defaut
            ObjCours.Default = Entry(2, RetourBmp, "�")
            If Entry(4, RetourBmp, "�") = "1" Then
                ObjCours.Primary = True
            Else
                ObjCours.Primary = False
            End If
            
            If Entry(1, UCase$(RetourBmp), "�") = "VARIABLE" Then
                isVar = True
                IsAsso = False
            ElseIf Entry(1, UCase$(RetourBmp), "�") = "VAR" Then
                isVar = True
                IsAsso = True
            End If
                                
            '-> Cr�er l'objet
            Set aBmp = New ImageObj
            
            aBmp.Nom = Rep
            aBmp.Left = 0
            aBmp.Top = 0
            aBmp.Contour = True
            aBmp.Largeur = Me.ScaleX(Me.BitMap(0).Width, 1, 7)
            aBmp.Hauteur = Me.ScaleY(Me.BitMap(0).Height, 1, 7)
            aBmp.IdAffichage = SectionCours.nBmp - 1
            aBmp.IdOrdreAffichage = SectionCours.AddOrdreAffichage("BMP-" & UCase$(Rep))
            aBmp.SectionName = SectionCours.Nom
            aBmp.FormatEdit = 50
            
            '-> Ajouter l'objet dans la collection
            SectionCours.Bmps.Add aBmp, UCase$(Rep)
            
            If isVar Then
                aBmp.Fichier = Entry(3, RetourBmp, "�")
                aBmp.Path = "PARAM"
                aBmp.isVariable = True
                If IsAsso Then aBmp.UseAssociation = True
            Else
                aBmp.Fichier = Entry(1, Entry(2, RetourBmp, "|"), "�")
                aBmp.Path = Entry(1, RetourBmp, "|")
                aBmp.isVariable = False
            End If
                         
            '-> Cr�ation de la repr�sentation physique
            CreatePhysiqueBmp aBmp, Me, SectionCours
                         
            '-> Ajouter dans le treeview
            aBmp.AddTreeView
                
            '-> Liberrer les ressources
            Set aBmp = Nothing
            Set aLb = Nothing
                        
            '-> Donner le focus au bmp
            Me.BitMap(SectionCours.nBmp - 1).SetFocus
        
        End If 'Si on a s�lectionn� un bmp
        
    Case 8 'Suppression d'un bmp
    
        Set aLb = Libelles("MESSAGE")
        Res = MsgBox(aLb.GetCaption(29), vbCritical + vbYesNo, aLb.GetToolTip(29))
        If Res = vbNo Then
        Else
            '-> Supprimer la repr�sentation physique
            Unload Me.BitMap(ObjCours.IdAffichage)
            '-> Supprimer le node dans le TreeView
            ObjCours.DelTreeView
             '-> R�ajuster les matrices d'ordre d'affichage
            SectionCours.DelOrdreAffichage ObjCours.IdOrdreAffichage
            '-> Nettoyer les alignements
            ObjCours.ClearAlign
            '-> Supprimer l'objet dans la collection de la section
            SectionCours.Bmps.Remove (UCase$(ObjCours.Nom))
            Me.RangText.SetFocus
        End If
        
    Case 9 'Modification du Bmp
    
        '-> Cas d'un bmp variable
'        If ObjCours.isVariable Then
'            Set aLb = Libelles("FRMSECTION")
'            Rep = InputBox(aLb.GetCaption(25), aLb.GetToolTip(25), ObjCours.Fichier)
'            If Rep <> "" Then
'                ObjCours.Fichier = Rep
'            End If
'            '-> Mettre � jour les propri�t�s si la feuille est visible
'            If frmPropVisible Then frmProp.DisplayProperties
'            '-> Quitter
'            Exit Sub
'        End If
            
        frmLoadBmp.Show vbModal
        
        '-> Modifier la repr�sentation physique
        If RetourBmp <> "" Then
            '-> on charge eventuellement l'image par defaut
            ObjCours.Default = Entry(2, RetourBmp, "�")
            If Entry(4, RetourBmp, "�") = "1" Then
                ObjCours.Primary = True
            Else
                ObjCours.Primary = False
            End If
            '-> si on a un e image de type variable
            If Trim(Entry(3, RetourBmp, "�")) <> "" Then
                ObjCours.Fichier = Trim(Entry(3, RetourBmp, "�"))
            End If
            Select Case Entry(1, RetourBmp, "�")
                Case "VARIABLE"
                    ObjCours.isVariable = True
                    ObjCours.UseAssociation = False
                Case "VAR"
                    ObjCours.isVariable = True
                    ObjCours.UseAssociation = True
                Case Else
                    ObjCours.isVariable = False
                    ObjCours.UseAssociation = False
            End Select
            RetourBmp = Entry(1, RetourBmp, "�")
            '-> Charger la nouvelle image
            Tempo = V6SearchPath(Entry(2, RetourBmp, "|"))
            If Tempo <> "" Then
                Me.BitMap(ObjCours.IdAffichage).Picture = LoadPicture(Tempo)
                '-> Modifier le fichier
                ObjCours.Fichier = Entry(2, RetourBmp, "|")
                ObjCours.Path = Entry(1, RetourBmp, "|")
            End If
        End If
                
        '-> Modifier les dimensions
        ObjCours.Largeur = Me.ScaleX(Me.BitMap(ObjCours.IdAffichage).Width, 1, 7)
        ObjCours.Hauteur = Me.ScaleY(Me.BitMap(ObjCours.IdAffichage).Height, 1, 7)
        
        If frmPropVisible Then frmProp.DisplayProperties
    
    Case 10 'Contour du Bmp
    
        '-> Modifier la propri�t�
        ObjCours.Contour = Not ObjCours.Contour
        
        '-> Modifier la repr�sentation physique
        If ObjCours.Contour Then
            Me.BitMap(ObjCours.IdAffichage).BorderStyle = 1
        Else
            Me.BitMap(ObjCours.IdAffichage).BorderStyle = 0
        End If
            
        If frmPropVisible Then frmProp.DisplayProperties
        
    Case 11 'Redimension
    
        If Me.Toolbar2.Buttons(11).Value = tbrPressed Then
            '-> Blocker les barres d'outils
            EnabledFontBar False
            EnabledCadre False
            EnabledBmp False
            Me.Toolbar2.Buttons(15).Enabled = False
            '-> Rendre le bouton De maj Ok
            Me.Toolbar2.Buttons(11).Enabled = True
            '-> Autoriser la redimension
            SetRedimBorder Me.BitMap(ObjCours.IdAffichage), True
            '-> Pointeur de souris de redimension
            Me.BitMap(ObjCours.IdAffichage).MousePointer = 0
            '-> Indiquer que l'on est en cour de redimension
            IsRedim = True
            Set RedimObj = ObjCours
            '-> Indiquer � l'objet qu'il n'est plus en autosize
            Me.BitMap(ObjCours.IdAffichage).AutoSize = False
            '-> Donner le focus � l'objet
            Me.BitMap(ObjCours.IdAffichage).SetFocus
        Else
            DeleteRedimBmp RedimObj
        End If
        
        '-> Mettre � jour les propri�t�s si la feuille est visible
        If frmPropVisible Then frmProp.DisplayProperties
        
    Case 12 'Autosize
    
        '-> Autosize du bmp
        Me.BitMap(ObjCours.IdAffichage).AutoSize = True
        
        '-> Donner les nouvelles valeurs au bmp
        ObjCours.Largeur = Me.ScaleX(Me.BitMap(ObjCours.IdAffichage).Width, 1, 7)
        ObjCours.Hauteur = Me.ScaleY(Me.BitMap(ObjCours.IdAffichage).Height, 1, 7)
        ObjCours.Left = Me.ScaleX(Me.BitMap(ObjCours.IdAffichage).Left, 1, 7)
        ObjCours.Top = Me.ScaleY(Me.BitMap(ObjCours.IdAffichage).Top, 1, 7)
        
        '-> R�ajuster les diff�rents liens
        RefreshAlign ObjCours
        
        '-> Mettre � jour les propri�t�s si la feuille est visible
        If frmPropVisible Then frmProp.DisplayProperties
        
    Case 13 'Duplication
    
        '-> R�cup�rer le nom de l'image
        Set aLb = Libelles("MESSAGE")
        Rep = InputBox(aLb.GetCaption(25), aLb.GetToolTip(25), aLb.GetCaption(26) & SectionCours.nBmp)
        '-> V�rifier que l'on n' a pas cliqu� sur annuler
        If Trim(Rep) = "" Then
            Set aLb = Nothing
            Exit Sub
        End If
        '-> V�rifier la validit� du nom
        If Not IsLegalName(Rep) Then
            Set aLb = Libelles("MESSAGE")
            MsgBox aLb.GetCaption(5) & Chr(13) & aLb.GetCaption(6), vbCritical + vbOKOnly, aLb.GetToolTip(6)
            Set aLb = Nothing
            Exit Sub
        End If
        '-> V�rifier qu'il n'est pas d�ja utils�
        If SectionCours.GetBmpExist(Rep) Or SectionCours.GetCadreExist(Rep) Then
            MsgBox aLb.GetCaption(28), vbCritical + vbOKOnly, aLb.GetToolTip(28)
            Set aLb = Nothing
            Exit Sub
        End If
        
        '-> V�rifier le mot cl�
        If Not GetValidName(Rep) Then
            ErrorName
            Exit Sub
        End If
        
        If InStr(1, Rep, "-") <> 0 Then
            Set aLb = Libelles("MESSAGE")
            MsgBox aLb.GetCaption(47), vbCritical + vbOKOnly, aLb.GetToolTip(47)
            Exit Sub
        End If
       
        '-> Cr�er l'objet
        Set aBmp = New ImageObj
        
        aBmp.Nom = Rep
        aBmp.Fichier = ObjCours.Fichier
        aBmp.Path = ObjCours.Path
        aBmp.Left = Me.ScaleX(Me.BitMap(SectionCours.nBmp - 1).Left, 1, 7)
        aBmp.Top = Me.ScaleY(Me.BitMap(SectionCours.nBmp - 1).Top, 1, 7)
        aBmp.Contour = ObjCours.Contour
        aBmp.Largeur = ObjCours.Largeur
        aBmp.Hauteur = ObjCours.Hauteur
        aBmp.IdAffichage = SectionCours.nBmp - 1
        aBmp.IdOrdreAffichage = SectionCours.AddOrdreAffichage("BMP-" & UCase$(Rep))
        aBmp.SectionName = SectionCours.Nom
        aBmp.FormatEdit = ObjCours.FormatEdit
        aBmp.isVariable = ObjCours.isVariable
        aBmp.UseAssociation = ObjCours.UseAssociation
        
        '-> Ajouter l'objet dans la collection
        SectionCours.Bmps.Add aBmp, UCase$(Rep)
        
        '-> Cr�er la repr�sentation physique
        CreatePhysiqueBmp aBmp, Me, SectionCours
        
        '-> Ajouter dans le treeview
        aBmp.AddTreeView
            
        '-> Liberrer les ressources
        Set aBmp = Nothing
        Set aLb = Nothing
                    
        '-> Donner le focus au bmp
        Me.BitMap(SectionCours.nBmp - 1).SetFocus
        
    Case 15 'Gestion des alignements
    
        frmAlign.Show vbModal
    
End Select
    

Set aLb = Nothing
Set aCadre = Nothing
Set aNode = Nothing

End Sub

Private Sub Toolbar2_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)


Select Case ButtonMenu.Key

    Case "1Pt"
        ObjCours.LargeurBordure = 1
    Case "2Pt"
        ObjCours.LargeurBordure = 2
    Case "3Pt"
        ObjCours.LargeurBordure = 3
    Case "4Pt"
        ObjCours.LargeurBordure = 4
    Case "5Pt"
        ObjCours.LargeurBordure = 5
    Case "6Pt"
        ObjCours.LargeurBordure = 6
    Case "7Pt"
        ObjCours.LargeurBordure = 7
    Case "8Pt"
        ObjCours.LargeurBordure = 8
    Case "9Pt"
        ObjCours.LargeurBordure = 9
    Case "10Pt"
        ObjCours.LargeurBordure = 10
    Case "Round"
        ObjCours.SetRoundRect
        
End Select

ObjCours.RefreshBordures


End Sub

Private Sub Vertical_Change()
    Me.ContainerRtf.Top = Me.Vertical.Value
    Me.Toolbar1.ZOrder
    Me.Toolbar2.ZOrder
End Sub

Public Sub DeleteRedimCadre(ByVal aCadre As Cadre)

Dim aLb As Libelle

'-> D�blocker les barres d'outils
EnabledFontBar True
EnabledCadre True
EnabledBmp False
Me.Toolbar2.Buttons(15).Enabled = True
'-> Pointeur de souris normal
Me.Cadre(aCadre.IdAffichage).MousePointer = 99
Me.CadreTexte(aCadre.IdAffichage).Enabled = True
'-> supprimer la redimension
SetRedimBorder Me.Cadre(aCadre.IdAffichage), False
'-> Fin de la redimension
IsRedim = False
Me.Toolbar2.Buttons(4).Value = tbrUnpressed
'-> V�rifier que las nouvelles largeur et hauteur laiisent bien assez de place pour la saisie
If Me.ScaleX(Me.Cadre(aCadre.IdAffichage).Width, 1, 7) < aCadre.MargeInterne * 2 Or _
   Me.ScaleY(Me.Cadre(aCadre.IdAffichage).Height, 1, 7) < aCadre.MargeInterne * 2 Then
    '-> Afficher un message d'erreur
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(23), vbExclamation + vbOKOnly, aLb.GetToolTip(23)
    '-> Restaurer les anciennes valeurs
    Me.Cadre(aCadre.IdAffichage).Width = Me.ScaleX(aCadre.Largeur, 7, 1)
    Me.Cadre(aCadre.IdAffichage).Height = Me.ScaleY(aCadre.Hauteur, 7, 1)
    Me.Cadre(aCadre.IdAffichage).Left = Me.ScaleX(aCadre.Left, 7, 1)
    Me.Cadre(aCadre.IdAffichage).Top = Me.ScaleY(aCadre.Top, 7, 1)
    '-> Liberrer les ressources
    Set aLb = Nothing
Else
    '-> Donner les nouvelles valeurs au cadre
    aCadre.Largeur = Me.ScaleX(Me.Cadre(aCadre.IdAffichage).Width, 1, 7)
    aCadre.Hauteur = Me.ScaleY(Me.Cadre(aCadre.IdAffichage).Height, 1, 7)
    aCadre.Left = Me.ScaleX(Me.Cadre(aCadre.IdAffichage).Left, 1, 7)
    aCadre.Top = Me.ScaleY(Me.Cadre(aCadre.IdAffichage).Top, 1, 7)
    '-> Retailler le Rtf associ�
    Me.CadreTexte(aCadre.IdAffichage).Width = Me.ScaleX(aCadre.Largeur - (aCadre.MargeInterne * 2), 7, 3)
    Me.CadreTexte(aCadre.IdAffichage).Height = Me.ScaleY(aCadre.Hauteur - (aCadre.MargeInterne * 2), 7, 3)
    '-> Mettre � jourles propri�t�s si la feuille est visible
    If frmPropVisible Then frmProp.DisplayProperties
    '-> R�ajuster les diff�rents liens
    RefreshAlign ObjCours
End If

'-> Forcer le dessin du cadre
aCadre.BackColor = aCadre.BackColor
aCadre.RefreshBordures

Set aCadre = Nothing

End Sub

Public Sub DeleteRedimBmp(ByVal aBmp As ImageObj)

Dim aLb As Libelle

'-> D�blocker les barres d'outils
EnabledFontBar False
EnabledCadre False
EnabledBmp True
Me.Toolbar2.Buttons(7).Enabled = False
Me.Toolbar2.Buttons(15).Enabled = True

'-> Supprimer la redimension
SetRedimBorder Me.BitMap(aBmp.IdAffichage), False

'-> Pointeur de souris normal
Me.BitMap(aBmp.IdAffichage).MousePointer = 99

'-> Fin de la redimension
IsRedim = False
Me.Toolbar2.Buttons(11).Value = tbrUnpressed

'-> Donner les nouvelles valeurs au bmp
aBmp.Largeur = Me.ScaleX(Me.BitMap(aBmp.IdAffichage).Width, 1, 7)
aBmp.Hauteur = Me.ScaleY(Me.BitMap(aBmp.IdAffichage).Height, 1, 7)
aBmp.Left = Me.ScaleX(Me.BitMap(aBmp.IdAffichage).Left, 1, 7)
aBmp.Top = Me.ScaleY(Me.BitMap(aBmp.IdAffichage).Top, 1, 7)

'-> R�ajuster les diff�rents liens
RefreshAlign ObjCours
    
'-> R�afficher les propri�t�s si necessaire
If frmPropVisible Then frmProp.Form_Load


'-> Liberer les ressources
Set aLb = Nothing
Set aBmp = Nothing


End Sub


Private Function GetSectionName(ByVal obj) As String

If TypeOf obj Is Section Then
    GetSectionName = obj.Nom
ElseIf TypeOf obj Is Cadre Then
    GetSectionName = obj.SectionName
ElseIf TypeOf obj Is ImageObj Then
    GetSectionName = obj.SectionName
End If
    
End Function

Private Function GestDelete(ByVal NomObj As String)

'-> Gestion des redimensions
If IsRedim Then
    If UCase$(RedimObj.Nom) = UCase$(NomObj) Then
        '-> Ne rien faire on est sur le m�me objet
        Exit Function
    Else
        If TypeOf RedimObj Is Cadre Then
            DeleteRedimCadre RedimObj
        ElseIf TypeOf RedimObj Is ImageObj Then
            DeleteRedimBmp RedimObj
        End If
        IsRedim = False
    End If
End If


End Function
