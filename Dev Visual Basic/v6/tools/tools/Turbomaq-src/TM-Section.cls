VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Section"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'**********************************************
'*                                            *
'* Cette classe contient la d�finition d'un   *
'* Objet Section                              *
'*                                            *
'**********************************************

'---> Liste des propri�t�s
Public Nom As String
Public Contour As Boolean
Public Hauteur As Single
Public Largeur As Single
Public AlignementTop As Integer
Public Top As Single
Public AlignementLeft As Integer
Public Left As Single
Public BackColor As Long
Public IdAffichage As Integer
Public nEntries As Integer
Public LigChar As String '-> Nouvelle version pour attaque des maquettes graphiques sans maquettes caract�res
'-> RG01�10
Private pOrdreAffichage() As String

'-> Valeurs pour la propri�t� AlignementTop
    '-> Libre (1)
    '-> Marge haut (2)
    '-> Marge bas (3)
    '-> Centr� (4)
    '-> Sp�cifi� (5)
    
'-> Valeurs pour la propri�t� AlignementLeft
    '-> Marge gauche (2)
    '-> Marge droite (3)
    '-> Centr� (4)
    '-> Sp�cifi� (5)
    
'-> Pour AlignementTop et AlignementLeft , les valeurs Top et Left ne sont _
accessibles que pour la valeur Sp�cifi� (5)

'-> Cette variable indique l'index du prochain cadre � cr�er sachant que le cadre de _
r�f�rence ( index = 0 ) n'est jamais utilis�

Public nCadre As Integer
'-> Cette variable indique l'index du prochain bmp � cr�er sachant que le bmp de _
r�f�rence ( index = 0 ) n'est jamais utilis�

Public nBmp As Integer

Public Cadres As Collection
Public Bmps As Collection
Public Fields As Collection

'---> Maquette de transition
Public RangChar As String
    

Public Sub AddTreeView()

'-> Ajout dans le treeview de l'icone de la section

Dim aNode As Node

Set aNode = frmNaviga.TreeNaviga.Nodes.Add("MAQUETTE", 4, UCase$(Nom), Nom, "Section")
aNode.Tag = "SECTION"
aNode.Selected = True
Set aNode = Nothing

End Sub

Public Sub DelTreeView()

'-> Suuprime dans le treeview de l'icone de la section

frmNaviga.TreeNaviga.Nodes.Remove (UCase$(Nom))


End Sub

Private Sub Class_Initialize()
    
    '-> Index du cadre
    nCadre = 1
    '-> Index du prochain objet dans le tableau des ordres d'affichage
    nEntries = 1
    '-> Index du prochain Bmp
    nBmp = 1
    
    '-> Initaliser les collections
    Set Cadres = New Collection
    Set Bmps = New Collection
    Set Fields = New Collection
    
End Sub

Public Function GetCadreExist(ByVal NewName As String) As Boolean

'---> Cette fonction v�rifie si un nom de cadre n'est pas d�ja utilis�

Dim aCadre As Cadre

For Each aCadre In Cadres

    If UCase$(NewName) = UCase$(aCadre.Nom) Then
        GetCadreExist = True
        Exit Function
    End If
    
Next

GetCadreExist = False

End Function

Public Function GetBmpExist(ByVal NewName As String) As Boolean

'---> Cette fonction v�rifie si un nom de cadre n'est pas d�ja utilis�

Dim aBmp As ImageObj

For Each aBmp In Bmps

    If UCase$(NewName) = UCase$(aBmp.Nom) Then
        GetBmpExist = True
        Exit Function
    End If
    
Next

GetBmpExist = False

End Function
Public Function AddOrdreAffichage(ByVal Value As String) As Integer

'Value Repr�sentant :   [CDR-[NomObject]]
'                       [BMP-[NomObject]]


ReDim Preserve pOrdreAffichage(1 To nEntries)
pOrdreAffichage(nEntries) = Value
AddOrdreAffichage = nEntries
nEntries = nEntries + 1


End Function

Public Function GetOrdreAffichage(ByVal nIndex As Integer) As String

    GetOrdreAffichage = pOrdreAffichage(nIndex)

End Function

Public Sub ClearOrdreAffichage()

    Erase pOrdreAffichage()
    nEntries = 1
    
End Sub

Public Sub DelOrdreAffichage(ByVal nIndex As Integer)

'---> Cette proc�dure supprime une entr�e dans la matrice des ordres d'affichage

Dim DupOrdre() As String
Dim i As Integer
Dim j As Integer
Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim TypeObj As String
Dim NomObj As String

'-> Dans un premier temps, faire une copie du tableau des ordres d'affichage
ReDim DupOrdre(1 To nEntries)
For i = 1 To nEntries - 1
    DupOrdre(i) = pOrdreAffichage(i)
Next

'-> Ecraser la matrice
Erase pOrdreAffichage()

'-> Ne rien faire s'il n'y a plus d'entr�es
If nEntries - 1 = 1 Then
    nEntries = 1
    Exit Sub
End If

'-> La reconstituer en supprimant l'entr�e
ReDim pOrdreAffichage(1 To nEntries - 2)
j = 1
For i = 1 To nEntries - 1
    If i = nIndex Then
    Else
        '-> Ajouter l'entr�e dans la matrice
        pOrdreAffichage(j) = DupOrdre(i)
        '-> Modifier l'ordre de l'objet concern�
        TypeObj = Entry(1, pOrdreAffichage(j), "-")
        NomObj = Entry(2, pOrdreAffichage(j), "-")
        If UCase$(TypeObj) = "CDR" Then
            Set aCadre = Cadres(UCase$(NomObj))
            aCadre.IdOrdreAffichage = j
            Set aCadre = Nothing
        Else
            Set aBmp = Bmps(UCase$(NomObj))
            aBmp.IdOrdreAffichage = j
            Set aBmp = Nothing
        End If
        '-> Incr�menter le compteur d'ordre
        j = j + 1
    End If
Next

'-> Indiquer qu'il y a une entr�e en mois
nEntries = nEntries - 1

End Sub




Public Sub RefreshAlignSection()

'---> Cette fonction a pour but de r�aligner les objets qui ont pour maitre _
d'alignement la section

Dim i As Integer
Dim aCadre As Cadre
Dim aBmp As ImageObj

'---> Recherche dans les Bmps
For Each aBmp In Bmps
    If UCase$(Entry(1, aBmp.MasterAlign, "-")) = "PAGE" Then RefreshAlign aBmp
Next

'---> Recherche dans les cadres
For Each aCadre In Cadres
    If UCase$(Entry(1, aCadre.MasterAlign, "-")) = "PAGE" Then RefreshAlign aCadre
Next


End Sub

Public Sub PrintView(IntFile As Integer)


Print #IntFile, "[ST-" & UCase$(Nom) & "(TXT-SECTION)[]{}"


End Sub
