VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmMaqChar 
   Caption         =   "Form1"
   ClientHeight    =   9060
   ClientLeft      =   60
   ClientTop       =   645
   ClientWidth     =   14055
   LinkTopic       =   "Form1"
   ScaleHeight     =   12630
   ScaleWidth      =   23760
   Begin RichTextLib.RichTextBox Rtf 
      Height          =   6375
      Left            =   0
      TabIndex        =   0
      Top             =   840
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   11245
      _Version        =   393217
      Enabled         =   -1  'True
      HideSelection   =   0   'False
      ScrollBars      =   3
      RightMargin     =   20000
      AutoVerbMenu    =   -1  'True
      TextRTF         =   $"TM-frmMaqChar.frx":0000
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   450
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   23760
      _ExtentX        =   41910
      _ExtentY        =   794
      ButtonWidth     =   820
      ButtonHeight    =   741
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      ImageList       =   "imgBtOk"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   13
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Gauche"
            ImageIndex      =   1
            Style           =   2
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Centre"
            ImageIndex      =   2
            Style           =   2
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Droite"
            ImageIndex      =   3
            Style           =   2
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Gras"
            ImageIndex      =   4
            Style           =   1
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Italic"
            ImageIndex      =   5
            Style           =   1
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Sousligne"
            ImageIndex      =   6
            Style           =   1
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FontColor"
            ImageIndex      =   7
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Print"
            ImageIndex      =   8
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Save"
            ImageIndex      =   11
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin VB.ComboBox ComboFont 
         Height          =   315
         Left            =   5040
         Sorted          =   -1  'True
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   60
         Width           =   2895
      End
      Begin VB.ComboBox ComboSize 
         Height          =   315
         Left            =   8040
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   60
         Width           =   975
      End
   End
   Begin MSComctlLib.ImageList imgBtOk 
      Left            =   360
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   24
      ImageHeight     =   22
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   11
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmMaqChar.frx":0077
            Key             =   "Left"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmMaqChar.frx":0211
            Key             =   "Center"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmMaqChar.frx":03AB
            Key             =   "Right"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmMaqChar.frx":0545
            Key             =   "Bold"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmMaqChar.frx":06DF
            Key             =   "Italic"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmMaqChar.frx":0879
            Key             =   "Underline"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmMaqChar.frx":0A13
            Key             =   "FontColor"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmMaqChar.frx":0C0D
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmMaqChar.frx":0E07
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmMaqChar.frx":14E9
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmMaqChar.frx":1EC3
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
   End
   Begin VB.Menu mnuFichier 
      Caption         =   "&Fichier"
      Begin VB.Menu mnuSave 
         Caption         =   "Enregistrer"
      End
      Begin VB.Menu mnuSaveAs 
         Caption         =   "En&registrer sous"
      End
   End
End
Attribute VB_Name = "frmMaqChar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public aType As Integer '-1 pour envoyer la maquette de transition

Private Sub ComboFont_Click()
Me.Rtf.SelFontName = Me.ComboFont.Text
End Sub

Private Sub ComboSize_Click()
Me.Rtf.SelFontSize = CInt(Me.ComboSize.Text)
End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim aMaq As MaqChar
Dim i As Integer
Dim aRg As RangChar

On Error Resume Next



If MaqChars.Count <> 0 Then

    '-> Charger la maquette caract�re associ�e
    Set aMaq = MaqChars("MAQCHAR")
        
        
    If aType = -1 Then
    Else
        
        
        '-> Faire le setting de la m�me font et size pour la feuille que pour le rtf
        Me.FontName = Me.Rtf.Font.Name
        Me.FontSize = Me.Rtf.Font.Size
        
        For Each aRg In aMaq.RangChars
            If aRg.IdAffichage = 1 Or aRg.IdAffichage = aMaq.RangChars.Count Then
            Else
                Me.Rtf.Text = Me.Rtf.Text & Chr(13) + Chr(10) & """" & aRg.Nom & """"
            End If
            For i = 1 To aRg.nLignes - 1
                '-> Ajouter la ligne
                Me.Rtf.Text = Me.Rtf.Text & Chr(13) + Chr(10) & aRg.GetLigne(i)
                '-> Calculer la largeur de la ligne
                If Me.TextWidth(aRg.GetLigne(i)) > Me.Rtf.RightMargin Then Me.Rtf.RightMargin = Me.TextWidth(aRg.GetLigne(i))
            Next
        Next
    End If
End If

'---> charger les libelles
Set aLb = Libelles("FRMMAQCHAR")
Me.Caption = aLb.GetCaption(1)
Me.Toolbar1.Buttons("Gauche").ToolTipText = aLb.GetCaption(2)
Me.Toolbar1.Buttons("Centre").ToolTipText = aLb.GetCaption(3)
Me.Toolbar1.Buttons("Droite").ToolTipText = aLb.GetCaption(4)
Me.Toolbar1.Buttons("Gras").ToolTipText = aLb.GetCaption(5)
Me.Toolbar1.Buttons("Italic").ToolTipText = aLb.GetCaption(6)
Me.Toolbar1.Buttons("Sousligne").ToolTipText = aLb.GetCaption(7)
Me.Toolbar1.Buttons("FontColor").ToolTipText = aLb.GetCaption(8)
Me.Toolbar1.Buttons("Print").ToolTipText = aLb.GetCaption(9)
Me.Toolbar1.Buttons("Save").ToolTipText = aLb.GetCaption(12)
Me.ComboFont.ToolTipText = aLb.GetCaption(10)
Me.ComboSize.ToolTipText = aLb.GetCaption(11)

'---> Charger les polices
For i = 1 To Screen.FontCount - 1
    Me.ComboFont.AddItem Screen.Fonts(i)
Next

'-> Ajout des tailles de police
For i = 8 To 76 Step 2
    Me.ComboSize.AddItem i
Next

Me.ComboFont.Text = Me.Rtf.Font.Name
Me.ComboSize.Text = Me.Rtf.Font.Size

Me.Caption = Me.Caption & "  " & aMaq.Nom

'-> Chargement des messprog
Me.mnuFichier.Caption = "Fichier"
Me.mnuSave.Caption = "Enregistrer"
Me.mnuSaveAs.Caption = "Enregistrer sous"

Set aMaq = Nothing
Set aRg = Nothing
Set aLb = Nothing


End Sub

Private Sub Form_Resize()

'---> Redimensionner le Treeview avec la fen�tre
    
Dim aRect As Rect
Dim Res As Long
Dim lgBordure As Long

On Error Resume Next

'-> Zone cliente de la feuille
Res = GetClientRect(Me.Hwnd, aRect)

'-> Largeur de la bordure
lgBordure = GetSystemMetrics&(SM_CXEDGE)

'-> Modification du TreeView
Me.Rtf.Left = 10
Me.Rtf.Top = Me.Toolbar1.Height + 10
Me.Rtf.Height = Me.ScaleY(aRect.Bottom - aRect.Top - lgBordure, 3, 1) - Me.Toolbar1.Height - 20
Me.Rtf.Width = Me.ScaleX(aRect.Right - aRect.Left - lgBordure, 3, 1) - 20


End Sub

Private Sub Form_Unload(Cancel As Integer)

If aType = -1 Then IsEditMaqChar = False

End Sub

Private Sub mnuSave_Click()

'---> Demander l'enregistrement

Dim Rep  As VbMsgBoxResult
Dim PathName As String
Dim FileName As String
Dim strFileName As String

'-> On pointe sur l'applicatif en cours
aApp = V6ClientCours.Applications("APP|" & V6App).V6PathApp
    If UCase$(Trim(V6ClientCours.Code)) = "DEAL" Then
    PathName = aApp
Else
    PathName = Replace(V6IdentPath, "$IDENT", V6ClientCours.Code)
    PathName = Replace(PathName, "$APP", V6App) & "\Maqref"
End If

On Error GoTo GestError

FileName = MaqChars("MAQCHAR").Nom
'-> On determine le chemin en entier
strFileName = FileName
FileName = PathName & "\" & FileName

Kill FileName

'-> Enregistrer le fichier
Me.Rtf.SaveFile FileName, 1
MaqChars("MAQCHAR").Nom = strFileName
frmNaviga.TreeNaviga.Nodes("MAQCHAR").Text = strFileName
'-> Setting de la variable de fichier en cours
CurrentFile = FileName

'-> Changer le caption de la fen�tre
Me.Caption = CurrentFile

'-> Indiquer que cela est enregistr�
IsSaved = True

'-> Quitter la fonction
Exit Sub

GestError:

    If Err.Number = 32755 Then Exit Sub
    
    MsgBox "Impossible d'enregistrer le fichier", vbCritical + vbOKOnly, "Erreur"

End Sub

Private Sub Rtf_SelChange()
    DisplayProperties
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

Dim Res As Long
Dim aPoint As POINTAPI
Dim aLb As Libelle

On Error GoTo GestError

Select Case Button.Index

    Case 1 'Left
        Me.Rtf.SelAlignment = 0
    
    Case 2 'Centre
        Me.Rtf.SelAlignment = 2
    
    Case 3 'Droite
        Me.Rtf.SelAlignment = 1
    
    Case 5 'Gras
        Me.Rtf.SelBold = Button.Value
        
    Case 6 'italic
        Me.Rtf.SelItalic = Button.Value
    
    Case 7 'SousLigne
        Me.Rtf.SelUnderline = Button.Value
        
    Case 9 'FontColor
    
        Res = GetCursorPos(aPoint)
        frmPalette.Top = Me.ScaleY(aPoint.y, 3, 1)
        frmPalette.Left = Me.ScaleX(aPoint.x, 3, 1)
        frmPalette.Show vbModal
        If RetourColor <> -1 Then Me.Rtf.SelColor = RetourColor
        
    Case 11 'Imprimer
        
        '-> Init comme si pas de retour
        PrintOk = False
        frmPrint.Option1.Enabled = False
        frmPrint.Option2.Enabled = False
        frmPrint.Show vbModal
        '-> Teste de la valeur de retour
        If PrintOk Then
            '-> Calculer dans un premier temps l'orientation qui va bien
            If Me.ScaleX(Me.Rtf.RightMargin, 1, 7) > 21 Then
                Printer.Orientation = 2
            Else
                Printer.Orientation = 1
            End If
            For i = 1 To nCopies
                '-> Lancer l'impression
                Me.Rtf.SelPrint Printer.hDC
            Next
            '-> Fin de l'impression
            Printer.EndDoc
        End If
        
    Case 13
    
        Me.Rtf.SaveFile Me.Rtf.FileName, 1
        
End Select

Exit Sub

GestError:

    Select Case Err.Number
        Case 75
        
            Set aLb = Libelles("FRMMAQCHAR")
            MsgBox aLb.GetCaption(13), vbCritical + vbOKOnly, aLb.GetToolTip(13)
            Exit Sub
    End Select


End Sub

Private Sub DisplayProperties()

'---> Cette proc�dure affiche les propri�t�s de l'objet en cours dans la barre _
d'outils

On Error Resume Next

    
    '-> Afficher les propri�t�s d'alignement
    If Me.Rtf.SelAlignment = 0 Then
        Me.Toolbar1.Buttons(1).Value = tbrPressed
    ElseIf Me.Rtf.SelAlignment = 1 Then
        Me.Toolbar1.Buttons(3).Value = tbrPressed
    Else
        Me.Toolbar1.Buttons(2).Value = tbrPressed
    End If
    
    '-> Propri�t�s de la police
    If Me.Rtf.SelBold Then
        Me.Toolbar1.Buttons(5).Value = tbrPressed
    Else
        Me.Toolbar1.Buttons(5).Value = tbrUnpressed
    End If
    If Me.Rtf.SelItalic Then
        Me.Toolbar1.Buttons(6).Value = tbrPressed
    Else
        Me.Toolbar1.Buttons(6).Value = tbrUnpressed
    End If
    If Me.Rtf.SelUnderline Then
        Me.Toolbar1.Buttons(7).Value = tbrPressed
    Else
        Me.Toolbar1.Buttons(7).Value = tbrUnpressed
    End If
    
    '-> Afficher le nom de la police et sa taille
    Me.ComboFont.Text = Me.Rtf.SelFontName
    Me.ComboSize.Text = Me.Rtf.SelFontSize



End Sub

Private Sub mnuSaveAs_Click()

'---> Demander l'enregistrement

Dim Rep  As VbMsgBoxResult
Dim PathName As String
Dim FileName As String
Dim strFileName As String

'-> On pointe sur l'applicatif en cours
aApp = V6ClientCours.Applications("APP|" & V6App).V6PathApp
    If UCase$(Trim(V6ClientCours.Code)) = "DEAL" Then
    PathName = aApp
Else
    PathName = Replace(V6IdentPath, "$IDENT", V6ClientCours.Code)
    PathName = Replace(PathName, "$APP", V6App) & "\Maqref"
End If

On Error GoTo GestError

FileName = InputBox("Nouveau nom pour le fichier.", "cr�er une nouvelle maquette de transition", MaqChars("MAQCHAR").Nom)
If FileName = "" Then Exit Sub
'-> On determine le chemin en entier
strFileName = FileName
FileName = PathName & "\" & FileName

'-> V�rifier que le fichier n'existe pas
If Dir$(FileName, vbNormal) <> "" Then
    Rep = MsgBox("Le fichier sp�cifi� : " & Rep & " existe d�ja. Ecraser le fichier", vbExclamation + vbYesNoCancel, "Confirmation")
    If Rep = vbCancel Or Rep = vbNo Then Exit Sub
    '-> Supprimer le fichier sp�cifi�
    Kill FileName
End If

'-> Enregistrer le fichier
Me.Rtf.SaveFile FileName, 1
MaqChars("MAQCHAR").Nom = strFileName
frmNaviga.TreeNaviga.Nodes("MAQCHAR").Text = strFileName
'-> Setting de la variable de fichier en cours
CurrentFile = FileName

'-> Changer le caption de la fen�tre
Me.Caption = CurrentFile

'-> Indiquer que cela est enregistr�
IsSaved = True

'-> Quitter la fonction
Exit Sub

GestError:

    If Err.Number = 32755 Then Exit Sub
    
    MsgBox "Impossible d'enregistrer le fichier", vbCritical + vbOKOnly, "Erreur"

End Sub

