VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmFields 
   Caption         =   "Form1"
   ClientHeight    =   6885
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8880
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6885
   ScaleWidth      =   8880
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   480
      Top             =   2760
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmFields.frx":0000
            Key             =   "Lib"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmFields.frx":039A
            Key             =   "Champ"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmFields.frx":04F4
            Key             =   "CloseBook"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmFields.frx":064E
            Key             =   "OpenBook"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmFields.frx":07A8
            Key             =   "Commentaire"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmFields.frx":0BFA
            Key             =   "Rang"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmFields.frx":0D54
            Key             =   "RangChar"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmFields.frx":10EE
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView TreeFields 
      Height          =   3975
      Left            =   1320
      TabIndex        =   1
      Top             =   840
      Width           =   7575
      _ExtentX        =   13361
      _ExtentY        =   7011
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      FullRowSelect   =   -1  'True
      HotTracking     =   -1  'True
      ImageList       =   "ImageList2"
      Appearance      =   1
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   840
      Top             =   1560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   50
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmFields.frx":1540
            Key             =   "AddRdf"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmFields.frx":299A
            Key             =   "AddField"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmFields.frx":3DF4
            Key             =   "PrintRdf"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmFields.frx":524E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8880
      _ExtentX        =   15663
      _ExtentY        =   1164
      ButtonWidth     =   1508
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AddRdf"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "InsertField"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FindChar"
            ImageIndex      =   4
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmFields"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim NoMatch As String
Dim CapNoMatch As String
Dim LastFind As String

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

Select Case KeyCode

    Case 70
        If Shift And vbCtrlMask Then
            FindCharMsg
        End If
    
    Case 114
        If Trim(LastFind) <> "" Then FindChar LastFind
        
End Select


End Sub

Private Sub Form_Load()

Dim aLb As Libelle

Set aLb = Libelles("FRMFIELDS")

Me.Caption = aLb.GetCaption(1)
Me.Toolbar1.Buttons("AddRdf").ToolTipText = aLb.GetCaption(2)
Me.Toolbar1.Buttons("InsertField").ToolTipText = aLb.GetCaption(4)
Me.Toolbar1.Buttons("FindChar").ToolTipText = aLb.GetCaption(3)
NoMatch = aLb.GetCaption(5)
CapNoMatch = aLb.GetToolTip(5)

Set aLb = Nothing

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

If EraseFrm Then
Else
    If UnloadMode <> 4 Then Cancel = 1
    Me.Hide
End If

End Sub

Private Sub Form_Resize()
'---> Redimensionner le Treeview avec la fen�tre
    
Dim aRect As Rect
Dim Res As Long
Dim lgBordure As Long

On Error Resume Next

'-> Zone cliente de la feuille
Res = GetClientRect(Me.hwnd, aRect)

'-> Largeur de la bordure
lgBordure = GetSystemMetrics&(SM_CXEDGE)

'-> Modification du TreeView

Me.TreeFields.Left = 0
Me.TreeFields.Top = Me.Toolbar1.Height
Me.TreeFields.Height = Me.ScaleY(aRect.Bottom - aRect.Top - lgBordure, 3, 1) - Me.Toolbar1.Height
Me.TreeFields.Width = Me.ScaleX(aRect.Right - aRect.Left - lgBordure, 3, 1)


End Sub


Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

Dim aLb As Libelle
Dim StrToFind As String

On Error GoTo GestError

Select Case Button.Key

    Case "AddRdf"
    
        frmLoadDRF.Show vbModal

    Case "InsertField"
    
        TreeFields_DblClick
    
    Case "FindChar"
    
        FindCharMsg
    

End Select

Set aLb = Nothing

Exit Sub

GestError:

    Select Case Err.Number
    
        Case 32765
            
    End Select
    Set aLb = Nothing

End Sub

Private Sub TreeFields_DblClick()

Dim aForm As frmSection
Dim aFrm As FrmBlock
Dim aRtf As RichTextBox
Dim aTb As Tableau

On Error Resume Next

If Me.TreeFields.Nodes.Count = 0 Then Exit Sub



'-> Tester que l'on est bien sur un champ
If UCase$(Entry(1, Me.TreeFields.SelectedItem.Key, "@")) = "CHAMP" Then
    If TypeOf ObjCours Is Section Then
        Set aForm = Maquettes("MAQUETTE").frmSections(UCase$(ObjCours.Nom))
        Set aRtf = aForm.RangText
        aRtf.SelText = "^" & Entry(4, Me.TreeFields.SelectedItem.Key, "@")
        ObjCours.RangChar = UCase$(Me.TreeFields.SelectedItem.Tag)
        aForm.SetFocus
    ElseIf TypeOf ObjCours Is Cadre Then
        Set aForm = Maquettes("MAQUETTE").frmSections(UCase$(ObjCours.SectionName))
        Set aRtf = aForm.CadreTexte(ObjCours.IdAffichage)
        aRtf.SelText = "^" & Entry(4, Me.TreeFields.SelectedItem.Key, "@")
        aForm.SetFocus
    ElseIf TypeOf ObjCours Is Block Then
        If ObjCours.IsEdit Then
            Set aTb = Maquettes("MAQUETTE").Tableaux(UCase$(ObjCours.NomTb))
            Set aFrm = aTb.frmBlocks(UCase$(ObjCours.Nom))
            aFrm.BarreFormule.SelText = "^" & Entry(4, Me.TreeFields.SelectedItem.Key, "@")
            ObjCours.RangChar = UCase$(Me.TreeFields.SelectedItem.Tag)
            aFrm.BarreFormule.SetFocus
        End If
    End If
    
    Exit Sub
End If


Set aForm = Nothing
Set aFrm = Nothing
Set aRtf = Nothing
Set aTb = Nothing

End Sub


Private Sub FindChar(ByVal StrToFind As String)

'---> Cette fonction recherche dans les nodes le node associ� � une chaine de caract�re

Dim aNode As Node


For Each aNode In Me.TreeFields.Nodes
    If aNode.Index > Me.TreeFields.SelectedItem.Index Then
        If InStr(1, UCase$(aNode.Text), UCase$(StrToFind)) Then
            aNode.Selected = True
            Me.TreeFields.SetFocus
            Exit Sub
        End If
    End If
Next

'-> Indiquer que l'on n'a pas trouv� la chaine de carcat�re
MsgBox NoMatch & Chr(13) & StrToFind, vbInformation + vbOKOnly, CapNoMatch

Set aNode = Nothing

End Sub

Private Sub FindCharMsg()

    '-> Demander la chaine de caract�re � rechercher
    Set aLb = Libelles("FRMFIELDS")
    StrToFind = InputBox(aLb.GetCaption(8), aLb.GetToolTip(8), LastFind)
    If Trim(StrToFind) = "" Then Exit Sub
    LastFind = StrToFind
    FindChar StrToFind

End Sub

