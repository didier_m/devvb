Imports System

Imports System.Collections.Generic
Imports System.Text
Imports System.Security.Cryptography
Imports pkcs = System.Security.Cryptography.Pkcs
Imports x509 = System.Security.Cryptography.X509Certificates
Imports System.Convert

Public Class clsCertificat

    Private _encoding As Encoding = Encoding.Default
    Private _recipientPublicCertPath As String
    Private _signerCert As x509.X509Certificate2
    Private _recipientCert As x509.X509Certificate2

    Public Sub New()

    End Sub

    Public Property Charset()

        Get
            Return Me._encoding.WebName
        End Get

        Set(ByVal value)
            Me._encoding = Encoding.GetEncoding(value)
        End Set

    End Property

    Public Property RecipientPublicCertPath()

        Get
            Return _recipientPublicCertPath
        End Get

        Set(ByVal value)
            _recipientPublicCertPath = value
            _recipientCert = New x509.X509Certificate2(_recipientPublicCertPath)
        End Set

    End Property

    Public Sub LoadSignerCredential(ByVal signerPfxCertPath As String, ByVal signerPfxCertPassword As String)

        _signerCert = New x509.X509Certificate2(signerPfxCertPath, signerPfxCertPassword)

    End Sub

    Public Function SignAndEncrypt(ByVal cleartext As String) As String

        Dim result As String = String.Empty
        Dim messageBytes As Byte() = Me._encoding.GetBytes(cleartext)
        Dim signedBytes As Byte() = sign(messageBytes)
        Dim encryptedBytes As Byte() = Envelope(signedBytes)
        result = Base64Encode(encryptedBytes)
        Return result

    End Function

    Private Function sign(ByVal messageBytes As Byte()) As Byte()

        Dim content As pkcs.ContentInfo = New pkcs.ContentInfo(messageBytes)
        Dim signed As pkcs.SignedCms = New pkcs.SignedCms(content)
        Dim signer As pkcs.CmsSigner = New pkcs.CmsSigner(_signerCert)
        signed.ComputeSignature(signer)
        Dim signedBytes As Byte() = signed.Encode
        Return signedBytes

    End Function

    Private Function Envelope(ByVal contentBytes As Byte()) As Byte()

        Dim content As pkcs.ContentInfo = New pkcs.ContentInfo(contentBytes)
        Dim envMsg As pkcs.EnvelopedCms = New pkcs.EnvelopedCms(content)
        Dim recipient As pkcs.CmsRecipient = New pkcs.CmsRecipient(pkcs.SubjectIdentifierType.IssuerAndSerialNumber, Me._recipientCert)
        envMsg.Encrypt(recipient)
        Dim encryptedbytes As Byte() = envMsg.Encode()
        Return encryptedbytes

    End Function

    Private Function Base64Encode(ByVal encoded As Byte()) As String

        Const PKCS7_HEADER As String = "-----BEGIN PKCS7-----"
        Const PKCS7_FOOTER As String = "-----END PKCS7-----"
        Dim BASE64 As String = System.Convert.ToBase64String(encoded)
        Dim Formatted As StringBuilder = New StringBuilder
        Formatted.Append(PKCS7_HEADER)
        Formatted.Append(BASE64)
        Formatted.Append(PKCS7_FOOTER)
        Return Formatted.ToString

    End Function

End Class


