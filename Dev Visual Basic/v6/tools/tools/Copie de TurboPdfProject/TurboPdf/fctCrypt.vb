Option Strict Off
Option Explicit On
Module fctCrypt
	Const RefCrypt As String = "1,0,4,1,0,5,7,6,3,8,8,3,5,2,3,6,8,4,7,2,4,9,3,2,7,1,2,6,6,3,7,4,6,3,3,2,1,2,2,5,3,9,8,3,2," & "5,9,7,5,6,5,8,2,6,0,1,2,5,6,8,8,3,1,6,2,3,3,3,4,8,1,4,4,1,8,0,5,7,0,8,5,9,3,7,4,5,9,4,9,8," & "0,1,2,1,6,3,5,1,7,5,9,7,5,3,1,8,7,1,5,7,6,4,1,1,3,8,0,6,3,0,4,0,4,5,4,7,3,4,4,7,3,8,0,1,3," & "9,5,0,2,1,0,9,0,8,0,9,7,8,3,1,7,4,4,9,4,1,0,9,8,7,6,9,3,7,7,4,1,9,3,4,9,3,7,4,7,5,6,1,9,4," & "2,1,1,0,0,6,3,4,3,8,5,5,0,5,6,0,9,3,9,0,3,1,5,0,6,0,9,8,9,1,7,1,4,2,7,3,3,0,1,1,8,6,5,6,0," & "7,6,7,3,6,3,1,8,5,3,4,9,0,4,4,3,1,0,8,5,5,8,7,1,3,1,3,9,7,6,6,9,7,2,5,6,4,6,3,4,9,4,6,4,4," & "8,6,6,5,2,8,6,0,0,3,8,4,7,5,8,3,2,6,3,3,0,7,8,0,6,9,2,3,6,3,"
	Dim a As Short
	Dim x As Short
	Dim b As String
	Dim d As String
	Dim e As String
	Dim c() As Short
	Dim f As String
	Dim i As Short
	
	Public Function Crypt(ByVal strtoCrypt As String) As String
		
		'---> Fonction qui crypt
		
		Dim ModuloValue As Integer
		
		On Error Resume Next
		
		'-> test si zone d�ja crypt�e
		If UCase(Left(strtoCrypt, 4)) = "%KD%" Then
			Crypt = strtoCrypt
			Exit Function
		End If
		
		'-> charger les variables
        ReDim c(30000)
		d = ""
		b = strtoCrypt
		b = VB6.Format(Int((99999 * Rnd()) + 1), "00000") & strtoCrypt & VB6.Format(Int((99999 * Rnd()) + 1), "00000")
		
		For a = 1 To Len(b)
			
			
			c(a) = Asc(Mid(b, a, 1))
			ModuloValue = a - (300 * Fix(a / 300))
			If ModuloValue = 0 Then ModuloValue = 1
			
			c(a) = c(a) + Int(CDbl(Entry(ModuloValue, RefCrypt, ",")))
			
			If a > 1 Then c(a) = c(a) + Int(CDbl(Mid(VB6.Format(Asc(Mid(b, a - 1, 1)), "00000"), 3, 1)))
		Next 
		
		For a = Len(b) To 1 Step -1
			d = d & Chr(c(a))
		Next 
		
		Crypt = "%KD%" & d
		
	End Function
	
	Public Function DeCrypt(ByVal StrToDecrypt As Object) As String
		
		'---> Fonction qui d�crypte
		
		Dim ModuloValue As Integer
		
		On Error Resume Next
		
        If UCase(Left(StrToDecrypt, 4)) <> "%KD%" Then
            DeCrypt = StrToDecrypt
            Exit Function
        End If
		
		'-> charger les variables
        ReDim c(3000)
		
		x = 0
		d = ""
		e = ""
        d = Mid(StrToDecrypt, 5, Len(StrToDecrypt) - 4)
		
		For a = Len(d) To 1 Step -1
			x = x + 1
			c(x) = Asc(Mid(d, a, 1))
		Next 
		
		For a = 1 To Len(d)
			
			
			ModuloValue = a - (300 * Fix(a / 300))
			If ModuloValue = 0 Then ModuloValue = 1
			
			c(a) = c(a) - Int(CDbl(Entry(ModuloValue, RefCrypt, ",")))
			If a > 1 Then c(a) = c(a) - Int(CDbl(Mid(VB6.Format(Asc(Mid(e, a - 1, 1)), "00000"), 3, 1)))
			e = e & Chr(c(a))
		Next 
		
		e = Mid(e, 6, Len(e) - 10)
		DeCrypt = e
		
	End Function
End Module