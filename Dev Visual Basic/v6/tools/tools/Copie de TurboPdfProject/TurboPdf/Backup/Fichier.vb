Option Strict Off
Option Explicit On
Friend Class Fichier
    '---> Cette classe sert � regrouper les diiff�rents spools d'un m�me fichier ASCII

    '-> Liste des spools charg�s dans le fichier
    Public Spools As Collection

    '-> Nombre de spools dans le fichiers
    Public NbSpool As Short

    '-> Nom du fichir ASCII
    Public FileName As String

    '-> Indique si le fichier est une ancienne version
    Public IsOldVersion As Boolean

    '-> fichier d'origine
    Public FileNameZip

    Private Sub Class_Initialize_Renamed()

        '-> Initialiser la collection des spools
        Spools = New Collection

    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub

    Public Function AddSpool() As Spool

        '---> Cette fonction ajoute un spool dans la collection et renvoie un objet de type Spool

        Dim aSpool As Spool

        '-> Incr�menter le compteur de spool
        NbSpool = NbSpool + 1

        '-> initialiser une nouvelle instance d'un spool
        aSpool = New Spool

        '-> Par d�faut le spool est OK
        aSpool.Ok = True

        '-> Setting de sa cl� d'acc�s
        aSpool.Key = "SPOOL|" & NbSpool

        '-> Pour affichage
        aSpool.SpoolText = "Spool N� : " & NbSpool

        '-> Ajouter dans la collection
        Spools.Add(aSpool, aSpool.Key)

        '-> Renvoyer l'objet cr��
        AddSpool = aSpool

    End Function
End Class