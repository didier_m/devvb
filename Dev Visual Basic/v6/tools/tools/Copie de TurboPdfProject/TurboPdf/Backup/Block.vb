Option Strict Off
Option Explicit On
Public Class Block
    '**********************************************
    '*                                            *
    '* Cette classe contient la d�finition d'un   *
    '* Objet Block de ligne                       *
    '*                                            *
    '**********************************************


    '---> Propri�t�s de l'objet
    Public Nom As String
    Public NbLigne As Short
    Public NbCol As Short

    '---> Dimensions en CM
    Public Largeur As Single
    Public Hauteur As Single

    '---> Propri�t�s d'alignement
    Public AlignementTop As Short
    Public Top As Single
    Public AlignementLeft As Short
    Public Left_Renamed As Single

    '-> Valeurs pour la propri�t� AlignementTop
    '-> Libre (1)
    '-> Marge haut (2)
    '-> Centr� (3)
    '-> Marge Bas (4)
    '-> Sp�cifi� (5)

    '-> Valeurs pour la propri�t� AlignementLeft
    '-> PAS D'ALIGNEMET LIBRE EN LEFT
    '-> Marge gauche (2)
    '-> Centr� (3)
    '-> Marge droite (4)
    '-> Sp�cifi� (5)

    '-> Pour AlignementTop et AlignementLeft , les valeurs Top et Left ne sont _
    'accessibles que pour la valeur Sp�cifi� (5)

    '---> Dimensions en Pixels
    Public LargeurPix As Short
    Public HauteurPix As Short

    '---> Pour Stocker la largeur des colonnes en CM
    Private pLCol() As Single
    Private pHLig() As Single

    '---> Pour export vers Excel
    Private pExportExcel() As Boolean
    Private pExportUniqueExcel() As Boolean
    Public ListeExcel As String
    Private pIsExported() As Boolean 'Indique si on a d�ja export� cette ligne

    '---> Cellules associ�es au block
    Public Cellules As Collection

    '---> Gestion des ordres d'affichage
    Public IdOrdreAffichage As Short

    '---> Indique si un block est en cours d'�dition
    Public IsEdit As Boolean

    '---> Indique le handle du block quand il est dessin� dans le tableau
    Public hRgn As Integer

    '---> Gestion des liens
    Public MasterLink As String
    Public SlaveLink As String

    '---> Maquette de transition
    Public RangChar As String

    '---> Nom du tableau parent
    Public NomTb As String

    '---> Pour gestion de l'acc�s au d�tail
    Public UseAccesDet As Boolean '-> Indique que l'on utilise l'acc�s au d�tail
    Public KeyAccesDet As String '-> Cl� d'acc�s au d�tail



    'UPGRADE_NOTE: Class_Initializea �t� mis � niveau vers Class_Initialize_Renamed. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()

        Cellules = New Collection

    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub


    Public Sub Init()

        '---> Cette proc�dure initalise les matrices qui contiennent les diff�rentes largeurs _
        'des lignes et des colonnes

        Dim i As Short

        'UPGRADE_WARNING: La limite inf�rieure du tableau pLCol est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
        ReDim Preserve pLCol(NbCol)
        'UPGRADE_WARNING: La limite inf�rieure du tableau pHLig est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
        ReDim Preserve pHLig(NbLigne)
        'UPGRADE_WARNING: La limite inf�rieure du tableau pExportExcel est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
        ReDim Preserve pExportExcel(NbLigne)
        'UPGRADE_WARNING: La limite inf�rieure du tableau pExportUniqueExcel est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
        ReDim Preserve pExportUniqueExcel(NbLigne)
        'UPGRADE_WARNING: La limite inf�rieure du tableau pIsExported est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
        ReDim Preserve pIsExported(NbLigne)

        '-> De base on exporte toutes les lignes
        For i = 1 To NbLigne
            pExportExcel(i) = True
        Next

    End Sub

    Public Sub SetLargeurCol(ByVal IdCol As Short, ByVal LargeurCm As Single)
        pLCol(IdCol) = LargeurCm
    End Sub

    Public Function GetLargeurCol(ByVal IdCol As Short) As Single
        GetLargeurCol = pLCol(IdCol)
    End Function

    Public Sub SetHauteurLig(ByVal IdLig As Short, ByVal HauteurCm As Single)
        pHLig(IdLig) = HauteurCm
    End Sub

    Public Function GetHauteurLig(ByVal IdLig As Short) As Single
        GetHauteurLig = pHLig(IdLig)
    End Function

    Public Function GetExportExcel(ByVal IdLig As Short) As Boolean
        GetExportExcel = pExportExcel(IdLig)
    End Function

    Public Sub SetExportExcel(ByVal IdLig As Short, ByRef IsExport As Boolean)
        pExportExcel(IdLig) = IsExport
    End Sub

    Public Function GetExportUniqueExcel(ByVal IdLig As Short) As Boolean
        GetExportUniqueExcel = pExportUniqueExcel(IdLig)
    End Function

    Public Sub SetExportUniqueExcel(ByVal IdLig As Short, ByRef IsExport As Boolean)
        pExportUniqueExcel(IdLig) = IsExport
    End Sub

    Public Sub SetIsExportedLig(ByVal IdLig As Short, ByRef IsExported As Boolean)
        pIsExported(IdLig) = IsExported
    End Sub

    Public Function GetIsExportedLig(ByVal IdLig As Short) As Boolean
        GetIsExportedLig = pIsExported(IdLig)
    End Function

    Public Function IsOneExportLig() As Boolean

        '---> cette proc�dure d�termine si au moins une ligne est exportable vers excel

        Dim i As Short

        For i = 1 To NbLigne
            If pExportExcel(i) Then
                IsOneExportLig = True
                Exit Function
            End If
        Next

    End Function
End Class