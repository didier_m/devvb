Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports System.Runtime.InteropServices
Imports Microsoft.Win32

Module Mod_TurboPdf
    '-> Pour passage en v6
    Public V6TurboIniPath As String
    Public V6MqtPath As String

    '---> Indique si on est en version Entreprise (0) ou profressionelle (1)
    Public VersionTurbo As Short
    Public TurboGraphIniFile As String
    Public Tm_PictureIniFile As String
    Public TurboMaqIniFile As String
    Public FichierLangue As String
    Public IndexLangue As Short
    Public AskForLangue As Boolean

    '---> Pour gestion des Messporg : version Internet
    Public IsAutoVersion As Boolean

    '---> Pour gestion des p�riph�riques de sortie
    Public Mode As Short '-> Indique le mode de consultation envoy� sur la ligne de commande
    Public Sortie As Object '-> Contient le p�riph�rique de sortie
    Public g As Graphics
    Public MargeX As Short '-> Marge interne d'un DC axe X pour impression papier
    Public MargeY As Short '-> Marge interne d'un DC axe Y pour impression papier
    'UPGRADE_ISSUE: Printer L'objet - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
    Public TempPrinter As printer '-> Objet de type printer temporaire pour test de validit� imprimante
    Public SpoolSize As Integer '-> Indique le nombre de page par job d'impression
    Public SpoolPrint As Integer '-> Indique le nombre de pages imprim�es dans le spool en cours

    '---> Pour ouverture des fichiers
    Public OpenPath As String 'Indique le path d�faut
    Public FirstOpen As Boolean 'N'en tenir compte que la premi�re fois

    '---> Pour impression de la page des s�lections
    Public Const SelectRTFKey As String = "%%SELECTIONRTF%%@@THIERRY"

    '---> Pour dessin de la temporisation
    Public TailleLue As Double
    Public TailleTotale As Double

    Public IsCryptedFile As Boolean

    '---> Pour envoie vers NOTEPAD ou WORDPAD
    Public EditorPath As String

    '---> Pour echange entre les feuilles
    Public strRetour As String

    '---> Pour cr�ation dynamique des controles
    Public IdRTf As Short '-> Contient l'index du prochain Controle RTF qui sera charg�
    Public IdBmp As Short '-> Contient l'index du prochain controle PictureBox qui sera cr��

    '---> Structure de controle de dessin d'une cellule d'un objet tableau
    Public Structure FormatedCell
        Dim Ok As Boolean
        Dim nbDec As Short
        Dim strFormatted As String
        Dim value As Double
        Dim idNegatif As Short
    End Structure

    Public Printers As Collection

    Public Structure printer
        Dim DeviceName As String
        Dim Orientation As Integer
        Dim PaperSize As Integer
        Dim Duplex As Integer
        Dim Width As Integer
        Dim DriverName As String
        Dim Copies As Integer
        Dim port As Integer
    End Structure

    '---> Liste des collections pour gestion du multi Spool
    Public Fichiers As Collection

    '---> Gestion des paths
    Public TempDirectory As String

    '---> Pour gestion de la cr�ation des blocks de tableau
    Dim sCol() As String
    Dim sLigne() As String
    Dim LigLue As Short

    Public IsMouchard As Boolean

    '-> Pour gestion des
    Public KillSpool As Boolean
    Public IsPDF As Boolean

    Public PdfDestinationFile As String

    '-> pour la recherche de texte
    Public FindTop As Boolean
    Public FindPage As Short
    Public FindPos As Short
    Public FindPos2 As Short
    Public FindText As String
    Public FindScrollH As Short
    Public FindScrollV As Short

    '-> pour la gestion du debordement
    Public DebordementTop As Boolean
    Public AllowDebordement As Boolean

    '-> pour la gestion du zoom
    Public Zoom As Double

    '-> pour la gestion de la navigation
    Public IsNaviga As Boolean

    ' Format structure, pass� avec SendMessage au contr�le
    'UPGRADE_NOTE: FORMATa �t� mis � niveau vers FORMAT_Renamed. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Structure FORMAT_Renamed
        Dim cbSize As Short
        Dim wPad1 As Short
        Dim dwMask As Integer
        Dim dwEffects As Integer
        Dim yHeight As Integer
        Dim yOffset As Integer
        Dim crTextColor As Integer
        Dim bCharSet As Byte
        Dim bPitchAndFamily As Byte
        <VBFixedArray(LF_FACESIZE - 1)> Dim szFaceName() As Byte
        Dim wPad2 As Short
        Dim wWeight As Short
        Dim sSpacing As Short
        Dim crBackColor As Integer
        Dim lLCID As Integer
        Dim dwReserved As Integer
        Dim sStyle As Short
        Dim wKerning As Short
        Dim bUnderlineType As Byte
        Dim bAnimation As Byte
        Dim bRevAuthor As Byte
        Dim bReserved1 As Byte

        'UPGRADE_TODO: "Initialize" doit �tre appel� pour initialiser les instances de cette structure. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
        Public Sub Initialize()
            ReDim szFaceName(LF_FACESIZE - 1)
        End Sub
    End Structure

    ' API declarations
    Private Declare Auto Function GetVersionEx Lib "kernel32.dll" _
    (<Runtime.InteropServices.MarshalAs(UnmanagedType.Struct)> ByRef osinfo As OSVERSIONINFOEX) As Int32
    ' Structure definition
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)> _
    Private Structure OSVERSIONINFOEX
        Public dwOSVersionInfoSize As Int32
        Public dwMajorVersion As Int32
        Public dwMinorVersion As Int32
        Public dwBuildNumber As Int32
        Public dwPlatformId As Int32
        <VBFixedString(128), Runtime.InteropServices.MarshalAs(UnmanagedType.ByValTStr, SizeConst:=128)> _
        Public szCSDVersion As String
        Public wServicePackMajor As Int16
        Public wServicePackMinor As Int16
        Public wSuiteMask As Int16
        Public wProductType As Byte
        Public wReserved As Byte
    End Structure
    ' Useful constants
    Private Const VER_PLATFORM_WIN32s As Int32 = &H0
    Private Const VER_PLATFORM_WIN32_WINDOWS As Int32 = &H1
    Private Const VER_PLATFORM_WIN32_NT As Int32 = &H2
    Private Const VER_NT_WORKSTATION As Int32 = &H1
    Private Const VER_NT_SERVER As Int32 = &H3
    ' Terminal server in remote admin mode
    Private VER_SUITE_SINGLEUSERTS As Int32 = &H100&
    Private Const VER_SUITE_PERSONAL As Int32 = &H200&
    Public Enum WindowsVersion
        Undetermined_OS = 0
        Obsolete_OS = 1
        Windows_98 = 2
        Windows_98_SE = 3
        Windows_Me = 4
        Windows_NT4_Workstation = 5
        Windows_NT4_Server = 6
        Windows_2000_Pro = 7
        Windows_2000_Server = 8
        Windows_XP_HomeEdition = 9
        Windows_XP_Pro = 10
        Windows_Net_Server = 11
    End Enum

    Public Mouchard As String
    Public hdlMouchard As Short
    Public PdfFileName As String
    '-> pour les feuilles du projet
    Public myMDIMain As New MDIMain()

    Dim aPrinter As New vbPDF

    Private Function InitTurboVersion() As Object

        Dim strPath As String
        Dim i As Short
        Dim Res As Integer
        Dim lpBuffer As String
        Dim TempFileName As String

        '->  D�tection de la version Professionelle: Pr�sence du fichier Turbo.ver et Path

        On Error Resume Next
        InitTurboVersion = True

        '-> Recherche du fichier Version :
        'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        If Dir(My.Application.Info.DirectoryPath & "\Turbo.ver") = "" Then
            '-> On ne trouve pas le fichier Ver donc on est en version Internet.
            VersionTurbo = 1 'Version internet
        Else
            VersionTurbo = 0 'Version Path v6
        End If

        '-> Par d�faut on est en fran�ais
        IndexLangue = 1

        '-> On est en version Entreprise : recherche du path des fichiers Ini et des fichiers Ressources
        If VersionTurbo = 1 Then 'Version internet
            TurboGraphIniFile = "" '-> Pas d'acc�s au fichier TurboGrpah.ini
            Tm_PictureIniFile = "" '-> Pas d'acc�s au fichier Tm_picture.ini
            TurboMaqIniFile = "" '-> Pas d'acc�s au fichier TurboMaq.ini
            FichierLangue = "" '-> Pas de fichiers Langue - > Charger en fichier Ressource
            AskForLangue = True '-> Demander le fichier Langue
            Exit Function
        Else 'Verdion Entreprise
            '-> Le path du r�pertoire doit �tre renseign� MESSPROG
            If V6TurboIniPath = "" Then
                'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
                If Dir(My.Application.Info.DirectoryPath & "\Turbograph.ini") <> "" Then
                    V6TurboIniPath = My.Application.Info.DirectoryPath
                Else
                    lpBuffer = ShowOpenFolder("Recherche du r�pertoire de param�trage ", 0)
                    If lpBuffer = "" Then
                        MsgBox("Impossible de continuer sans le r�pertoire de param�trage", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Erreur")
                        End
                    End If
                    V6TurboIniPath = lpBuffer
                End If
            End If
            If Right(V6TurboIniPath, 1) <> "\" Then V6TurboIniPath = V6TurboIniPath & "\"
            If (GetAttr(V6TurboIniPath) And FileAttribute.Directory) <> FileAttribute.Directory Then 'MESSPROG
                MsgBox("Impossible de trouver le r�pertoire des fichiers langue." & vbCrLf & V6TurboIniPath, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Erreur")
                End
            End If
            TurboGraphIniFile = V6TurboIniPath & "Turbograph.ini" 'MESSPROG
            'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            If Dir(TurboGraphIniFile) = "" Then
                MsgBox("Impossible de trouver le fichier de param�trage : " & vbCrLf & TurboGraphIniFile, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Erreur")
                End
            End If
            '-> Path des fichiers langue
            lpBuffer = My.Application.Info.DirectoryPath
            i = InStrRev(lpBuffer, "\")
            lpBuffer = Mid(lpBuffer, 1, i) & "Mqt\"
            If (GetAttr(lpBuffer) And FileAttribute.Directory) <> FileAttribute.Directory Then 'MESSPROG
                MsgBox("Impossible de trouver le r�pertoire des fichiers langue." & vbCrLf & lpBuffer, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Erreur")
                End
            End If
            V6MqtPath = lpBuffer
            FichierLangue = lpBuffer

            '-> Setting du fichier TM_Picture.ini
            Tm_PictureIniFile = V6TurboIniPath & "Tm_Picture.ini"
            'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            If Dir(Tm_PictureIniFile, FileAttribute.Normal) = "" Then Tm_PictureIniFile = ""

            '-> Setting du fichier TurboMaq.ini
            TurboMaqIniFile = V6TurboIniPath & "TurboMaq.ini"
            'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            If Dir(TurboMaqIniFile, FileAttribute.Normal) = "" Then TurboMaqIniFile = ""

        End If

        '-> Initialisation de la version entreprise

        '-> Recherche du fichier Langue
        If TurboGraphIniFile <> "" Then
            lpBuffer = GetIniString("PARAM", "LANGUE", TurboGraphIniFile, False)
            If lpBuffer <> "NULL" Then
                '-> Tester si la zone est num�rique
                If IsNumeric(lpBuffer) Then
                    IndexLangue = CShort(lpBuffer)
                    AskForLangue = False
                Else
                    '-> Il faut demander la langue de l'utilisateur
                    AskForLangue = True
                    IndexLangue = 0
                End If
            Else
                '-> Demander le fichier langue � charger
                AskForLangue = True
            End If

            '-> Taille du spool
            lpBuffer = GetIniString("PARAM", "SPOOLSIZE", TurboGraphIniFile, False)
            If lpBuffer <> "NULL" Then
                If IsNumeric(lpBuffer) Then SpoolSize = CInt(lpBuffer)
            End If

        End If 'Si on a acc�s au fichier Ini
    End Function

    Public Function GetVariableEnv(ByRef strVariable As String) As String

        Dim Res As Integer
        Dim lpBuffer As String

        lpBuffer = Space(500)
        Res = GetEnvironmentVariable(strVariable, lpBuffer, Len(lpBuffer))
        If Res <> 0 Then
            '-> Faire un trim sur le buffer
            lpBuffer = Mid(lpBuffer, 1, Res)
        Else
            lpBuffer = ""
        End If

        '-> Retouner la valeur
        GetVariableEnv = lpBuffer

    End Function



    Private Sub Initialisation()

        '---> Cette proc�dure est charg�e d'initialiser les diff�rentes variables n�c�ssaires au moteur d'impression

        Dim Res As Integer
        Dim lpBuffer As String

        '-> Attention, il existe 2 versions du TURBO.

        '-> Version Professionelle  -> Libre
        '-> Version Entreprise - > Dans architecture

        '-> Initialiser les index de cr�ation des prochains objets RichTextFormat et BITMAP
        IdRTf = 1
        IdBmp = 1

        '-> Initialiser la collection des fichiers ouverts dans le TurboGraph
        Fichiers = New Collection

        '-> R�cup�ration des informations de formatage num�rique
        lpBuffer = Space(10)
        Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, lpBuffer, Len(lpBuffer))
        SepDec = Mid(lpBuffer, 1, Res - 1)
        lpBuffer = Space(10)
        Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, lpBuffer, Len(lpBuffer))
        SepMil = Mid(lpBuffer, 1, Res - 1)

        '-> R�cup�ration du r�pertoire d'ouverture par d�faut
        lpBuffer = Space(255)
        Res = GetEnvironmentVariable("DEALTEMPO", lpBuffer, Len(lpBuffer))
        If Res <> 0 Then
            OpenPath = Mid(lpBuffer, 1, Res)
            'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            If Dir(OpenPath, FileAttribute.Directory) = "" Then OpenPath = ""
        End If

        '-> R�cup�ration du r�pertoire du param�trage du Turbo dans le cas ou version = 0
        lpBuffer = Space(255)
        Res = GetEnvironmentVariable("TURBOINI", lpBuffer, Len(lpBuffer))
        If Res <> 0 Then V6TurboIniPath = Mid(lpBuffer, 1, Res)

        '-> R�cup�rer la path du r�pertoire temporaire de windows
        TempDirectory = GetTempFileNameVB("", True)

        '-> Lancer l'initialisation de la gestion des versions du Turbo
        Call InitTurboVersion()

    End Sub


    'UPGRADE_WARNING: L'application va se terminer � la fin de Sub Main(). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="E08DDC71-66BA-424F-A612-80AF11498FF8"'
    Public Sub Main()

        '---> Point d'entr�e du nouveau module

        Dim lpBuffer As String
        Dim Res As Integer
        Dim LigneCommande As String
        Dim Param1 As String = ""
        Dim RectoVerso As Boolean
        Dim DeviceName As String
        Dim ModeName As String
        'UPGRADE_ISSUE: Printer L'objet - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
        Dim Ligne As String = ""
        Dim j, i, k As Short
        Dim PageDebut As Short
        Dim aPath As String = ""
        Dim Tempo As String = ""
        Dim TempFileName As String = ""
        Dim NomFichier As String = ""
        Dim aFichier As Fichier
        Dim aspool As Spool
        Dim CountPage As Short

        '-> Pour argument sur la ligne de command epour IE, OutLook , HTML
        Dim pFile As String = ""
        Dim pTo As String = ""
        Dim pCopies As String = ""
        Dim pObject As String = ""
        Dim pBody As String = ""
        Dim pBodyFile As String = ""
        Dim pFormat As String = ""
        Dim pFormatBody As String = ""
        Dim pMode As String = ""
        Dim pSpoolName As String = ""
        Dim pDir As String = ""
        Dim KeyValue As String = ""
        Dim pValue As String = ""
        Dim pRun As String = ""
        Dim pCrypt As String = ""
        Dim pPrinter As String = ""

        Dim MyCodeLangue As String

        ' Descriptif de la ligne de commande
        '[Param1]|[Param2]

        '************************************
        '* Creation de fichier pdf *
        '************************************

        '-> Activer la gestion des erreurs
        On Error GoTo InterceptError

        '-> Gestion du mouchard
        IsMouchard = False
        If IsMouchard Then
            Mouchard = GetTempFileNameVB("DBG")
            hdlMouchard = FreeFile()
            FileOpen(hdlMouchard, Mouchard, OpenMode.Output)
        End If

        '-> Lancer l'initialisation
        Call Initialisation()

        '-> Si on est en version internet
        If VersionTurbo = 1 Then
            '-> Charger les ressources en locale
            LoadInternetMessProg()
        Else
            '-> V�rifier que l'on trouve le fichier langue
            'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            If Dir(FichierLangue & "\TurboPrint1-" & VB6.Format(IndexLangue, "00") & ".lng", FileAttribute.Normal) = "" Then
                '-> On ne trouve pas le fichier : Charger la ressource en interne
                LoadInternetMessProg()
            Else
                '-> Charger le fichier messprog
                RessourcePath = FichierLangue & "\"
                If Not (CreateMessProg("TurboPrint1-" & VB6.Format(IndexLangue, "00") & ".lng", True)) Then GoTo InterceptError
            End If
        End If 'Selon la version


        '********************************************************************************
        '* ANALYSE DE LA LIGNE DE COMMANDE ET GESTION DES DIFFERENTS MODES D'IMPRESSION *
        '********************************************************************************

        '-> R�cup�ration de la ligne de commande
        LigneCommande = VB.Command()
        LigneCommande = LTrim(LigneCommande)

        If IsMouchard Then PrintLine(hdlMouchard, "Ligne de commande :" & LigneCommande)

        '-> Supprimer les Quotters s'il y en a
        If InStr(1, LigneCommande, """") = 1 Then LigneCommande = Mid(LigneCommande, 2, Len(LigneCommande) - 1)
        If InStr(1, LigneCommande, """") <> 0 Then LigneCommande = Mid(LigneCommande, 1, Len(LigneCommande) - 1)

        If Trim(LigneCommande) <> "" Then
            '-> Recherche d'un "|"
            If InStr(1, LigneCommande, "|") <> 0 Then
                '-> R�cup�ration du nom de fichier pass� en param�tre
                NomFichier = Entry(2, LigneCommande, "|")
                '-> R�cup�ration des param�tres d'impression pdf
                Param1 = Entry(1, LigneCommande, "|")
            Else
                '-> Pas de "|" donc le paramettre est sens� �tre un fichier : v�rifier s'il existe
                'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
                If Dir(LigneCommande, FileAttribute.Normal) <> "" Then
                    NomFichier = LigneCommande
                Else
                    NomFichier = ""
                End If
            End If 'S'il y a un "|"
        End If 'Si la ligne de commande est vide

        If IsMouchard Then
            PrintLine(hdlMouchard, "Mode d'impression : " & Mode)
            'Close #hdlMouchard
        End If

        'ATTENTION : V6 Modification pour la gestion du ZIP : on teste si le fichier est ZIPE. S'il est ZIP,
        'decompression dans le fichier temporaire
        NomFichier = GetUnzipFileName(NomFichier)
        '->on affecte un nom au pdf
        PdfFileName = NomFichier & ".pdf"
        aPrinter.BeginDoc(PdfFileName)
        aPrinter.SetScale(vbPDF.pdfScaleMode.pdfCentimeter)
        aPrinter.SetCompression(vbPDF.pdfCompression.pdfNothing)
        '-> Lancer l'analyse du spool
        Call AnalyseFileToPrint(NomFichier)

        '-> Pointer sur l'objet fichier
        aFichier = Fichiers.Item(UCase(NomFichier))

        '-> Lancer l'impression des pages
        For Each aspool In aFichier.Spools
            '-> Initialisation du mode de l'imprimante
            '-> Initialiser le compteur de page imprim� dans le spool
            SpoolPrint = 0

            '-> Tester s'il ya des erreurs
            If aspool.NbError = 0 Then
                '-> Tester si on doit imprimer la page des s�lections
                If aspool.IsSelectionPage Then
                    CountPage = 0
                Else
                    CountPage = 1
                End If

                For i = CountPage To aspool.NbPage
                    '-> Tester s'il y a des erreurs sur la page sp�cifi�e
                    If aspool.GetErrorPage(i) <> "" Then
                        '-> R�cup�rer la liste des erreurs
                        Tempo = aspool.GetErrorPage(i)
                        '-> Imprrmer la liste des erreurs
                        For k = 1 To NumEntries(Tempo, Chr(0))
                            'Printer.Print(Entry(i, Tempo, Chr(0)))
                        Next  '-> Pour toutes les erreurs
                    Else
                        '-> Tester la taille du lot de page
                        If SpoolSize <> 0 Then
                            If SpoolPrint = SpoolSize Then
                                '-> Terminer le document en cours
                                If IsMouchard Then PrintLine(hdlMouchard, "Avant impression si spoolsize")
                                '-> Initialiser le compteur de page imprim� dans le spool
                                SpoolPrint = 0
                            End If 'Si on atteint le nombre limitte de page
                        End If 'Si param�trage du spooler par lot
                        '-> Imprimer la page
                        PrintPageSpool(aspool, i)
                        '-> Incr�menter le compteur de page imprim�
                        SpoolPrint = SpoolPrint + 1
                    End If
                    '-> Envoyer un saut de page si pas derni�re page
                    'UPGRADE_ISSUE: Printer m�thode Printer.NewPage - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
                    'If i <> aspool.NbPage Then aPrinter.NewPage()
                Next  'Pour toutes les pages
            Else
                '-> Imprrmer la liste des erreurs
                For k = 1 To aspool.NbError
                    'UPGRADE_ISSUE: Printer L'objet - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
                    'UPGRADE_ISSUE: Printer m�thode Printer.Print - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
                    'Printer.Print(aspool.GetErrorMaq(k))
                Next  '-> Pour toutes les erreurs
            End If
            '-> Fin du document pour ce spool
            If IsMouchard Then PrintLine(hdlMouchard, "Avant l'impression")
            'UPGRADE_ISSUE: Printer m�thode Printer.EndDoc - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            'Printer.EndDoc()
        Next aspool
        aPrinter.EndDoc(True, False, True)
        aPrinter.OpenDoc()
        '-> Fin du programme
        End
        Exit Sub

InterceptError:

        If Err.Number <> 482 Then MsgBox("Erreur dans la procedure MAIN" & Chr(13) & Err.Number & " - " & Err.Description, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Deal Informatique")

        '-> Appeler la gestion des erreurs
        End

    End Sub

    Private Sub GestError(ByVal ErrorCode As Short, Optional ByRef strError As String = "", Optional ByRef MySpool As Spool = Nothing)

        '---> Cette proc�dure  effectue la gestion centralis�e des erreurs

        Dim aLb As Libelle
        Dim aFichier As Fichier
        Dim aspool As Spool = Nothing
        Dim Tempo As String
        Dim aNode As System.Windows.Forms.TreeNode

        '-> Pointer sur le libelle Gestion des Erreurs
        aLb = Libelles.Item("ERRORS")

        '-> Si on est en mode Visu : Cr�er l'icone du fichier dans le treeview et celle de l'erreur
        If Mode = 1 Then
            Select Case ErrorCode 'Les erreurs 1 et 5 ne sont pas fatales -> Trait�es dans la proc�dure d'origine
                Case 2 '2-> Impossible d'ouvrir le fichier
                    '-> Cr�er un nouveau Fichier
                    aFichier = New Fichier
                    aFichier.FileName = strError
                    Fichiers.Add(aFichier, UCase(Trim(strError)))
                    '-> Cr�er un nouveau Spool
                    aspool = aFichier.AddSpool
                    aspool.FileName = strError
                    '-> Ajouter une erreur dans le spool
                    aspool.SetErrorMaq(Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", strError))
                Case 4, 6
                    '-> Erreur lors de la lecture d'une ligne du fichier
                    '-> Pointer sur l'objet fichier correspondant au fichier
                    aFichier = Fichiers.Item(UCase(Trim(strError)))

                    '-> S'il n'y a pas de spools : il faut en cr�er un
                    If aFichier.Spools.Count() = 0 Then
                        aspool = aFichier.AddSpool
                        aspool.FileName = strError
                    End If

                    '-> Ajouter l'erreur dans le spool
                    aspool.SetErrorMaq(Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", strError))
                Case 7, 8, 9, 10
                    '-> Gestion des erreurs dans la proc�dure MAQLECT
                    '-> Pointer sur le fichier
                    aFichier = Fichiers.Item(UCase(Trim(Entry(1, strError, "|"))))
                    '-> Pointer sur le spool : pr�ciser en entry 3
                    aspool = MySpool
                    '-> Faire le setting d'une erreur
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", aspool.FileName)
                    Tempo = Replace(Tempo, "$MAQ$", aspool.MaquetteASCII)
                    Tempo = Replace(Tempo, "$INI$", Entry(3, strError, "|"))
                    aspool.SetErrorMaq(Tempo)
                Case 11, 12
                    '-> Gestion des erreurs dans la proc�dure INITCOM
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", strError)
                    Tempo = Replace(Tempo, "$TABLEAU$", strError)
                    Tempo = Replace(Tempo, "$MAQ$", MySpool.MaquetteASCII)
                    MySpool.SetErrorMaq(Tempo)
                Case 14, 15, 16
                    '-> Gestion des erreurs dans la proc�dure INITCOM
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$TABLEAU$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
                    Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
                    Tempo = Replace(Tempo, "$MAQ$", MySpool.MaquetteASCII)
                    MySpool.SetErrorMaq(Tempo)
                Case 17
                    '-> Gestion des erreurs dans la proc�dure INITFILETEMPORTF
                    MySpool.SetErrorMaq(aLb.GetCaption(ErrorCode))
                Case 18
                    '-> Gestion des erreurs dans la proc�dure INITMAQENTETE
                    MySpool.SetErrorMaq(aLb.GetCaption(ErrorCode) & " " & strError)
                Case 19
                    '-> Gestion des erreurs dans la proc�dure CREATEBMPOBJ
                    MySpool.SetErrorMaq(aLb.GetCaption(ErrorCode))
                Case 20, 21, 22
                    '-> Gestion des erreurs dans la proc�dure CREATEBMPOBJ
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(strError, |). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If NumEntries(strError, "|") > 3 Then
                        Tempo = Replace(Tempo, "$HAUTEUR$", Entry(3, strError, "|"))
                        Tempo = Replace(Tempo, "$LARGEUR$", Entry(4, strError, "|"))
                    Else
                        Tempo = Replace(Tempo, "$FICHIER$", Entry(3, strError, "|"))
                    End If
                    MySpool.SetErrorMaq(Tempo)
                Case 23
                    '-> Gestion des erreurs dans la procedure CREATECADREOBJ
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
                    MySpool.SetErrorMaq(Tempo)
                Case 24
                    '-> Gestion des erreurs dans la proc�dure CreateRTFCadre
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", Entry(3, strError, "|"))
                    Tempo = Replace(Tempo, "$SECTION$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
                    MySpool.SetErrorMaq(Tempo)
                Case 25
                    '-> Gestion des erreurs dans la proc�dure CreateRTFSection
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", Entry(2, strError, "|"))
                    Tempo = Replace(Tempo, "$SECTION$", Entry(1, strError, "|"))
                    MySpool.SetErrorMaq(Tempo)
                Case 26
                    '-> Gestion des erreurs dans la proc�dure CreateSectionObj
                    MySpool.SetErrorMaq(Replace(aLb.GetCaption(ErrorCode), "$SECTION$", strError))
                Case 27
                    '-> Gestion des erreurs dans la proc�dure InitSectionEntete
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$LIG$", Entry(2, strError, "|"))
                    MySpool.SetErrorMaq(Tempo)
                Case 28, 29
                    '-> Gestion des erreurs dans la proc�dure InitBmpEntete et InitCadreEntete
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
                    Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
                    Tempo = Replace(Tempo, "$LIG$", Entry(3, strError, "|"))
                    MySpool.SetErrorMaq(Tempo)
                Case 30
                    '-> Gestion des erreurs dans la procedure InitTableauEntete
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$LIG$", Entry(2, strError, "|"))
                    MySpool.SetErrorMaq(Tempo)
                Case 31
                    '-> Gestion des erreurs dans la procedure InitBlockEntete
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
                    Tempo = Replace(Tempo, "$LIG$", Entry(3, strError, "|"))
                    MySpool.SetErrorMaq(Tempo)
                Case 32, 33, 34
                    '-> Gestion des erreurs dans la proc�dure InitCreateBlock et InitBlock (34)
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
                    If ErrorCode = 33 Then Tempo = Replace(Tempo, "$LIG$", Entry(3, strError, "|"))
                    MySpool.SetErrorMaq(Tempo)
                Case 35, 36, 37, 38, 39, 40, 41
                    '-> Gestion des erreurs dans la proc�dure d'affichage d'une page : MODE VISU
                    aNode = myMDIMain.TreeNaviga.Nodes.Item(UCase(Trim(MySpool.FileName)) & "�" & UCase(Trim(MySpool.Key)) & "�PAGE|" & MySpool.CurrentPage)
                    aNode.ImageKey = "Warning"
                    aNode.Tag = "ERROR"

                    '-> Faire le setting de l'erreur
                    Select Case ErrorCode
                        Case 35, 36
                            MySpool.SetErrorPage(MySpool.CurrentPage, Replace(aLb.GetCaption(ErrorCode), "$SECTION$", strError))
                        Case 37
                            MySpool.SetErrorPage(MySpool.CurrentPage, Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", strError))
                        Case 38
                            MySpool.SetErrorPage(MySpool.CurrentPage, Replace(aLb.GetCaption(ErrorCode), "$OBJECT$", strError))
                        Case 39
                            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
                            Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
                            MySpool.SetErrorPage(MySpool.CurrentPage, Tempo)
                        Case 40
                            Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
                            Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
                            MySpool.SetErrorPage(MySpool.CurrentPage, Tempo)
                        Case 41
                            Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
                            Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
                            Tempo = Replace(Tempo, "$CELLULE$", Entry(3, strError, "|"))
                            MySpool.SetErrorPage(MySpool.CurrentPage, Tempo)
                    End Select

                    '-> Afficher la page
                    MySpool.DisplayInterfaceByPage((MySpool.CurrentPage))
            End Select
        Else
            '-> On est en mode impression : initialiser une page : imprimer une erreur

        End If

    End Sub


    Public Sub AnalyseFileToPrint(ByVal NomFichier As String)

        '---> Cette proc�dure charge le ficher � analyser, cr�er la collection des spools et les maquettes

        Dim hdlFile As Short ' handle d'ouverture de fichier
        Dim Ligne As String 'lecture d'une ligne du fichier ascii
        Dim aspool As Spool = Nothing 'pointeur vers un objet spool
        Dim NomMaquette As String 'nom de la maquette � ouvrir pour ancienne version
        Dim aFichier As Fichier 'Pointeur vers un objet fichier
        Dim LectureMaquette As Boolean 'Indique que l'on est en train de lire la d�finition de la maquette
        Dim LectureSelection As Boolean 'Indique que l'on est en train de lire une page de s�lection
        Dim LectureRTF As Boolean 'indique que l'on est en train de lire la d�finition RTF d'une maquette
        Dim hdlRtf As Short 'handle du fichier RTF dans lequel on �crit

        Dim IsSpool As Boolean 'cette variable indique que l'on est en cours de cr�ation d'un spool
        Dim Page As Short 'Compteur de page par spool
        Dim FindMaqChar As Boolean 'indique de plus charger les lignes de la maquettes car on analyse la maquette caract�re
        Dim Res As Integer
        Dim hdlFichier As Integer
        'UPGRADE_WARNING: Il peut �tre n�cessaire d'initialiser les tableaux de la structure aOf avant de les utiliser. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim aOf As OFSTRUCT = Nothing
        Dim ErrorCode As Short
        Dim i As Short

        Dim IsAccDet As Boolean '-> Indique que l'on est dans le fichier acc�s d�tail

        On Error GoTo GestError

        ErrorCode = 1
        '-> R�cup�rer la taille du fichier que l'on charge
        'hdlFichier = OpenFile(NomFichier, aOf, OF_READ)
        'If hdlFichier <> -1 Then
        '    Res = GetFileSize(hdlFichier, 0)
        '    TailleTotale = Res
        '    Res = CloseHandle(hdlFichier)
        'End If
        TailleTotale = FileLen(NomFichier)

        '-> Initialisation de la taille lue
        TailleLue = 0

        ErrorCode = 2
        '-> Obtenir un handle de fichier et ouvrir le spool � imprimer
        hdlFile = FreeFile()
        FileOpen(hdlFile, NomFichier, OpenMode.Input)

        '-> Cr�er un nouvel objet Fichier et l'ajouter dans la collection
        aFichier = New Fichier
        aFichier.FileName = NomFichier

        Fichiers.Add(aFichier, Trim(UCase(NomFichier)))

        '-> Boucle d'analyse du fichier
        Do While Not EOF(hdlFile)
            ErrorCode = 4
            '-> Lecture de la ligne
            Ligne = LineInput(hdlFile)
            '-> Decrypter la source
            Ligne = DeCrypt(Ligne)
            '-> Analyse des diff�rents cas que l'on peut rencontrer
            If InStr(1, Ligne, "%%GUI%%") <> 0 Then
                '-> R�cup�rer le nom de la maquette
                NomMaquette = Trim(Mid(Ligne, 8, Len(Ligne) - 7))
                '-> Cr�er un nouvel objet Spool
                aspool = aFichier.AddSpool
                '-> Indiquer le num�ro du spool
                aspool.Num_Spool = aFichier.Spools.Count()
                aspool.FileName = aFichier.FileName
                aspool.MaquetteASCII = NomMaquette
                aspool.AddPage()
                '-> Ancienne version : charger la maquette depuis son adresse
                aFichier.IsOldVersion = True
                MaqLect(aspool, NomMaquette)
                '-> Indiquer que l'on est en cr�ation d'un spool
                IsSpool = True
                '-> Premi�re page
                Page = 1
                '-> Ajouter une nouvelle page dans le spool
                'aSpool.AddPage
                '-> RAZ des variables de positionnement
                LectureMaquette = False
                LectureSelection = False
            Else 'si on n'a pas trouv� %%GUI%%
                Select Case Trim(UCase(Ligne))
                    Case "[SPOOL]"
                        '-> Cr�ation d'un nouveau spool
                        aspool = aFichier.AddSpool
                        aspool.AddPage()
                        '-> Indiquer le nom du fichier maitre
                        aspool.FileName = aFichier.FileName
                        '-> Indiquer le num�ro du spool
                        aspool.Num_Spool = aFichier.Spools.Count()
                        '-> Indiquer que l'on est en cr�ation de spool
                        IsSpool = True
                        '-> Indiquer que l'on n'est plus dans l'acc�s au d�tail
                        IsAccDet = False
                        '-> On n'est pas encore en train de lire la maquette
                        LectureMaquette = False
                        LectureSelection = False
                        FindMaqChar = False
                        '-> Init du compteur de page
                        Page = 1
                        '-> Init de la premi�re page
                        'aSpool.AddPage
                    Case "[/SPOOL]"
                        '-> Indiquer que 'on n'est plus dans un spool
                        IsSpool = False
                    Case "[MAQ]"
                        LectureMaquette = True
                        LectureSelection = False
                    Case "[/MAQ]"
                        LectureMaquette = False
                    Case "[MAQCHAR]"
                        FindMaqChar = True
                    Case "[GARDEOPEN]"
                        LectureSelection = True
                        LectureMaquette = False
                        '-> Indiquer  dans le spool en cours qu'il ya une page de s�lection
                        aspool.IsSelectionPage = True
                    Case "[GARDECLOSE]"
                        LectureSelection = False
                    Case "[PAGE]"
                        '-> Incr�menter le compteur de page
                        Page = Page + 1
                        '-> Ajouter une nouvelle page dans le spool
                        aspool.AddPage()
                    Case "[DETAIL]"
                        '-> On passe en mode lecture sur le spool en cours
                        IsAccDet = True
                    Case "[/DETAIL]"
                        '-> Fin du mode lecture de l'acc�s au d�tail
                        IsAccDet = False
                    Case Else
                        '-> Ne traiter la ligne que si on est dans un spool
                        If IsSpool Then
                            '-> Selon la nature du traitement
                            If IsAccDet Then
                                '-> Ne rien faire si on est en cours de lecture acces d�tail
                                If Trim(Ligne) <> "" Then aspool.AddLigneDetail(Ligne)
                            ElseIf LectureMaquette Then
                                '-> Ajouter une ligne dans la d�finition de la maquette
                                If Not FindMaqChar Then aspool.SetMaq(Ligne)
                            ElseIf LectureSelection Then
                                '-> Ajouter une ligne dans la page de s�lection
                                aspool.SetPage(0, Ligne)
                            Else
                                '-> ajouter une ligne de datas dans la page en cours
                                aspool.SetPage(Page, Ligne)
                            End If 'Selon la nature de la ligne
                        End If 'si on est dans un spool
                End Select 'Selon la ligne que l'on est en train de lire
            End If 'si on a trouv� %%GUI%%

            '-> Gestion de la temprisation
            If Mode = 1 Then
                TailleLue = TailleLue + Len(Ligne) + 2
                DrawWait()
            End If
        Loop  'Boucle d'analyse du fichier

        ErrorCode = 5
        '-> Fermer le fichier ascii
        FileClose(hdlFile)

        '-> Si on n' a pas trouv� de spool, en cr�er un pour l'erreur
        If aFichier.Spools.Count() = 0 Then
            '-> D�router sur la gestion d'erreur
            ErrorCode = 6
            GoTo GestError
        End If

        '-> Supprimer la derni�re page si elle est � blanc
        Dim counter As Short
        For Each aspool In aFichier.Spools
            counter = aspool.NbPage
            For i = counter To 1 Step -1
                If Trim(aspool.GetPage(i)) = "" Then
                    aspool.NbPage = aspool.NbPage - 1
                Else
                    '-> Ne supprimer qe les pages de fin qui sont blanches
                    Exit For
                End If
            Next
        Next aspool

        '-> supprimer le spool si il est vide
        i = 1
        Do While i <= aFichier.Spools.Count()
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aFichier.Spools(i).NbPage. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If aFichier.Spools.Item(i).NbPage = 0 Then
                aFichier.NbSpool = aFichier.NbSpool - 1
                aFichier.Spools.Remove(i)
            Else
                i = i + 1
            End If
        Loop

        '-> Gestion de la temporisation
        If Mode = 1 Then
            myMDIMain.StatusBar1.Refresh()
            '-> R�initialiser les variables de lecture
            TailleLue = 0
            TailleTotale = aFichier.Spools.Count()
        End If

        '-> G�n�rer les maquettes tous les spools
        For Each aspool In aFichier.Spools
            If Mode = 1 Then
                TailleLue = TailleLue + 1
                DrawWait()
            End If
            InitCom(aspool)
        Next aspool

        '-> Vider la toolbar
        If Mode = 1 Then
            myMDIMain.StatusBar1.Refresh()
            myMDIMain.picSplit.Visible = IsNaviga
            myMDIMain.picNaviga.Visible = IsNaviga
        End If
        Exit Sub

GestError:


        '-> Traitement de l'erreur
        Select Case ErrorCode
            Case 1
                '-> Pb lors de la r�cup�ration de la taille du fichier : ne rien faire
                Resume Next
            Case 2, 3, 4, 6
                '-> Erreur fatale
                Call GestError(ErrorCode, NomFichier)
            Case 5
                '-> Erreur lors de la fermture du fichier ASCII : Fermer le fichier
                Reset()
                '-> Rendre la main
        End Select


    End Sub

    Private Function OpenRTFSpool(ByVal RtfName As String) As Short

        '---> Ouverture d'un spool pour stocker le contenu RTF de la section/cadre

        Dim RtfFile As String
        Dim hdlRtf As Short

        '-> R�cup�ration du r�pertoire temporaire de windows
        RtfFile = GetTempFileNameVB("", True)
        hdlRtf = FreeFile()
        FileOpen(hdlRtf, RtfFile & "\" & RtfName, OpenMode.Output)

        '-> Renvoyer son handle
        OpenRTFSpool = hdlRtf

    End Function

    Private Sub MaqLect(ByRef aspool As Spool, ByVal aMaq As String)

        '---> Cette proc�dure charge le fichier ASCII d'une maquette dans un objet spool : compatibilite ancienne version

        Dim hdlFile As Short
        Dim TmpFile As String
        Dim Ligne As String
        Dim ErrorCode As Short
        Dim TmpError As String

        On Error GoTo GestError

        '-> Setting du nom de la maquette dans l'objet spool
        aspool.MaquetteASCII = aMaq

        '-> V�rifier que le fichier Maquette est bien pr�sent
        ErrorCode = 7
        TmpError = aspool.FileName & "|" & aMaq
        'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        If Dir(aMaq, FileAttribute.Normal) = "" Then
            '-> Setting d'une erreur
            Call GestError(7, TmpError, aspool)
            '-> Sortir de la proc�dure
            Exit Sub
        End If

        '-> Cr�er un fichier Tempo et faire une copie de la maquette
        ErrorCode = 8
        TmpFile = GetTempFileNameVB("MAQ")
        CopyFile(aMaq, TmpFile, False)


        ErrorCode = 9
        '-> Ouvrir le fichier Tempo
        hdlFile = FreeFile()
        FileOpen(hdlFile, TmpFile, OpenMode.Input)

        '-> Transf�rer la maquette dans l'objet Spool
        Do While Not EOF(hdlFile)
            '-> Lecture de la ligne
            ErrorCode = 10
            Ligne = LineInput(hdlFile)
            If Trim(Ligne) = "" Then GoTo NextLigne
            '-> Decrypter la maquette
            Ligne = DeCrypt(Ligne)
            '-> Quitter si on arrive � la d�finition de la maquette caract�re
            If UCase(Trim(Ligne)) = "[MAQCHAR]" Then Exit Do
            '-> Transfert de la ligne
            aspool.SetMaq((Ligne))
NextLigne:
        Loop

        ErrorCode = 11
        '-> Fermer le fichier ASCII
        FileClose(hdlFile)

        ErrorCode = 12
        '-> Supprimer le fichier Tempo
        Kill(TmpFile)

        '-> Indiquer que la maquette est OK
        aspool.Ok = True

        Exit Sub

GestError:

        '-> Traiter l'erreur selon le cas
        Select Case ErrorCode
            Case 7
                Call GestError(ErrorCode, TmpError, aspool)

            Case 11, 12 '-> Erreurs r�cup�rables
                '-> Fermer tous les fichiers ouverts
                Reset()
                '-> Continuer
                Resume Next
            Case Else
                '-> Appeler la gestion des erreurs
                Call GestError(ErrorCode, aspool.FileName & "|" & aMaq & "|" & TmpFile, aspool)
        End Select

    End Sub

    Private Sub InitCom(ByRef aspool As Spool)

        '---> Cette proc�dure initialise pour un fichier donn� la maquette associ�e

        Dim Ligne As String '-> Pour lecture d'une ligne de maquette
        Dim i As Integer '-> Compteur de ligne
        Dim SwitchLect As Short
        '-> Valeur de SwitchLect :
        '0  -> Nothing
        '1  -> Entete Maquette
        '2  -> Section PROGICIEL
        '3  -> Section de Texte : Propri�t�s
        '4  -> D�finition BMP
        '5  -> Definition d'un Cadre
        '6  -> Definition RTF d'un cadre
        '7  -> D�finition RTF d'une section
        '8  -> D�fintion d'un tableau
        '9  -> D�fintion d'un block de tableau
        '10 -> D�fintion d'une cellule de tableau
        '11 -> Ecran de s�lection

        Dim LibError As String '-> Libelle si erreur
        Dim TempoStr As String '-> Variable de stockage tempo

        '-> D�finition du model COM
        Dim aSection As Section = Nothing
        Dim aCadre As Cadre
        Dim aBmp As ImageObj
        Dim aTb As Tableau = Nothing
        Dim aBlock As Block
        Dim aCell As Cellule
        Dim ErrorCode As Short
        Dim TmpError As String

        On Error GoTo GestError

        '-> Type de ligne par d�faut
        SwitchLect = 0

        aBlock = New Block
        aBmp = New ImageObj
        aCadre = New Cadre

        '-> Initialisation de la maquette
        aspool.Maquette_Renamed = New Maquette

        '-> Analyse de la maquette propre au Spool
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(aspool.GetMaq, Chr(0)). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        For i = 1 To NumEntries(aspool.GetMaq, Chr(0))
            '-> R�cup�ration de la d�finition de la ligne
            Ligne = Entry(i, aspool.GetMaq, Chr(0))
            '-> Tester le premier caract�re de la ligne
            If Left(Ligne, 1) = "[" Then
                '-> Analyse du type d'objet
                If UCase(Mid(Ligne, 2, 2)) = "NV" Then
                    '-> Si on attaque le descriptif OBJECT ou la definition TRF
                    If UCase(Entry(3, Ligne, "-")) = "STD]" Then
                        '-> Cr�ation du nouvel objet Section de texte
                        aSection = New Section
                        aSection.Nom = Entry(2, Ligne, "-")
                        '-> Ajout dans la collection
                        ErrorCode = 11
                        TmpError = UCase(Trim(aSection.Nom))
                        aspool.Maquette_Renamed.Sections.Add(aSection, UCase(Trim(aSection.Nom)))
                        '-> Ligne de d�finition d'une section de texte
                        SwitchLect = 3
                    ElseIf UCase(Entry(3, Ligne, "-")) = "RTF]" Then
                        '-> Initialiser pour la section en cours un fichier et l'ouvrir
                        TempoStr = InitFileTempoRtf(aspool)
                        aSection.TempoRTF = CShort(Entry(1, TempoStr, "|"))
                        aSection.TempoRtfString = Entry(2, TempoStr, "|")
                        '-> Ligne de d�finition RTF de la section
                        SwitchLect = 7
                    End If
                ElseIf UCase(Mid(Ligne, 2, 2)) = "TB" Then
                    '-> Cr�ation du nouvel objet tableau
                    aTb = New Tableau
                    aTb.Nom = Mid(Entry(2, Ligne, "-"), 1, Len(Entry(2, Ligne, "-")) - 1)
                    aTb.IdAffichage = aspool.Maquette_Renamed.AddOrdreAffichage("TB-" & UCase(Trim(aTb.Nom)))
                    '-> Ajout dans la collection
                    ErrorCode = 12
                    TmpError = UCase(Trim(aTb.Nom))

                    aspool.Maquette_Renamed.Tableaux.Add(aTb, UCase(Trim(aTb.Nom)))
                    '-> Positionner le pointeur de lecture
                    SwitchLect = 8
                Else
                    If UCase(Trim(Ligne)) = "[PROGICIEL]" Then
                        '-> analyse de la cle PROGICIEL de la maquette
                        SwitchLect = 2
                    End If
                End If '-> Si [
            Else
                'ElseIf InStr(1, Ligne, "\") <> 0 Then
                Select Case UCase(Trim(Ligne))
                    Case "\DEFENTETE�BEGIN"
                        '-> D�finition de l'entete de la maquette
                        SwitchLect = 1
                    Case "\DEFENTETE�END"
                        '-> Envoyer dans la proc�dure d'ajout adaptation des dimensions
                        InitMaqEntete((aspool.Maquette_Renamed), Ligne, aspool)
                        '-> Fin de la d�inition de l'entete de la maquette
                        SwitchLect = 0
                    Case "\BMP�END"
                        '-> Lancer la cr�ation physique de l'objet BMP
                        CreateBmpObj(aBmp, aspool)
                        '-> Fin de d�fintion d'un objet BMP : repositionner sur pointeur de section
                        SwitchLect = 3
                    Case "\CADRE�END"
                        '-> Lancer la cr�ation physique du cadre
                        CreateCadreObj(aCadre, aspool)
                        '-> Fin de d�finition d'un objet cadre : repositionner sur pointeur de section
                        SwitchLect = 3
                    Case "\CADRERTF�BEGIN"
                        '-> Initialiser le fichier RTF pour lecture
                        TempoStr = InitFileTempoRtf(aspool)
                        aCadre.TempoRTF = CShort(Entry(1, TempoStr, "|"))
                        aCadre.TempoRtfString = Entry(2, TempoStr, "|")
                        '-> D�but de d�finition RTF d'un cadre
                        SwitchLect = 6
                    Case "\CADRERTF�END"
                        '-> Fermer le fichier tempo RTF affect�
                        ErrorCode = 13
                        FileClose(aCadre.TempoRTF)
                        '-> Lancer la g�n�ration physique du contenu RTF du cadre
                        CreateRTFCadre(aCadre, aspool)
                        '-> Fin de d�fintion RTF d'un cadre : repositionner sur pointeur de section
                        SwitchLect = 3
                    Case "\RTF�END"
                        '-> Fermer le fichier tempo RTF affect�
                        ErrorCode = 13
                        FileClose(aSection.TempoRTF)
                        '-> Lancer la g�n�ration physique du contenu RTF de la section de texte
                        CreateRTFSection(aSection, aspool)
                        '-> Fin de d�finition RTF d'une section
                        SwitchLect = 0
                    Case "\TABLEAU�END"
                        '-> Fin de d�finition d'un tableau
                        SwitchLect = 0
                    Case "\RANG�END"
                        '-> Fin de d�finition d'une section de texte
                        SwitchLect = 0
                    Case Else
                        Select Case SwitchLect
                            Case 1 'Analyse d'une ligne d'entete
                                InitMaqEntete((aspool.Maquette_Renamed), Ligne, aspool)
                            Case 2 'Analyse de la section PROGICIEL
                                If UCase(Trim(Entry(1, Ligne, "="))) = "\PROG" Then
                                    aspool.Maquette_Renamed.Progiciel = Entry(2, Ligne, "=")
                                ElseIf UCase(Trim(Entry(1, Ligne, "="))) = "\CLI" Then
                                    aspool.Maquette_Renamed.Client = Entry(2, Ligne, "=")
                                End If
                            Case 3 'On est dans une section de texte : analyse des tag <CADRE> et <BMP>  et <TEXTE>
                                Select Case UCase(Trim(Entry(1, Ligne, "�")))
                                    Case "\BMP"
                                        '-> Cr�ation d'un nouvel objet BMP
                                        aBmp = New ImageObj
                                        aBmp.Nom = Entry(2, Ligne, "�")
                                        aBmp.IdOrdreAffichage = aSection.AddOrdreAffichage("BMP-" & UCase(Trim(aBmp.Nom)))
                                        aBmp.SectionName = aSection.Nom
                                        '-> Ajout dans la collection des Bmps
                                        ErrorCode = 14
                                        TmpError = aSection.Nom & "|" & UCase(Trim(aBmp.Nom))
                                        aSection.Bmps.Add(aBmp, UCase(Trim(aBmp.Nom)))
                                        '-> Positionnement du pointeur de lecture
                                        SwitchLect = 4
                                    Case "\CADRE"
                                        '-> Cr�ation d'un nouvel objet cadre
                                        aCadre = New Cadre
                                        aCadre.Nom = Entry(2, Ligne, "�")
                                        aCadre.IdAffichage = aSection.AddOrdreAffichage("CDR-" & UCase(Trim(aCadre.Nom)))
                                        aCadre.SectionName = aSection.Nom
                                        ErrorCode = 15
                                        TmpError = aSection.Nom & "|" & UCase(Trim(aCadre.Nom))
                                        aSection.Cadres.Add(aCadre, UCase(Trim(aCadre.Nom)))
                                        '-> Positionnement du pointeur de lecture
                                        SwitchLect = 5
                                    Case "\TEXTE"
                                        '-> Fin de d�finition d'un objet Section: Cr�ation de la repr�sentation physique
                                        CreateSectionObj(aSection, aspool)
                                    Case Else
                                        '->Dans ce cas, on lit une propri�t� de la section: ajouter dans la d�fintion de la section
                                        InitSectionEntete(aspool, aSection, Ligne)
                                End Select 'Selon la nature de la ligne
                            Case 4
                                '-> Ligne de d�finition d'un BMP
                                InitBmpEntete(aBmp, aspool, Ligne)
                            Case 5
                                '-> Ligne de d�finition d'un cadre
                                InitCadreEntete(aCadre, aspool, Ligne)
                            Case 6
                                '-> Imprimer la ligne RTF du cadre dans son fichier TEMPO
                                PrintLine(aCadre.TempoRTF, Ligne)
                            Case 7
                                '-> Imprimer la ligne RTF de la section texte dans son fichier tempo
                                PrintLine(aSection.TempoRTF, Ligne)
                            Case 8
                                If UCase(Trim(Entry(1, Ligne, "�"))) = "\BEGIN" Then
                                    '-> Cr�ation d'un nouvel objet Block
                                    aBlock = New Block
                                    aBlock.Nom = Entry(2, Ligne, "�")
                                    aBlock.NomTb = Trim(UCase(aTb.Nom))
                                    aBlock.AlignementLeft = 3
                                    aBlock.Left_Renamed = 0
                                    aBlock.IdOrdreAffichage = aTb.AddOrdreAffichage("BL-" & UCase(Trim(aBlock.Nom)))
                                    '-> Ajouter le block dans la collection des tableaux
                                    ErrorCode = 16
                                    TmpError = aspool.MaquetteASCII & "|" & Trim(UCase(aTb.Nom)) & UCase(Trim(aBlock.Nom))
                                    aTb.Blocks.Add(aBlock, "BL-" & UCase(Trim(aBlock.Nom)))
                                    '-> Initialiser les matrices des lignes et colonnes
                                    Erase sCol
                                    Erase sLigne
                                    LigLue = 1
                                    '-> Postionnement du pointeur de lecture sur l'analyse des bloks
                                    SwitchLect = 9
                                Else
                                    '-> Ajouter � la d�finition du tableau en cours
                                    InitTableauEntete(aTb, aspool, Ligne)
                                End If
                            Case 9
                                If UCase(Trim(Entry(1, Ligne, "�"))) = "\END" Then
                                    '-> Fin de d�finition d'un objet block : lancer les initialisations et les cr�ations
                                    InitCreateBlock(aBlock, aTb, aspool)
                                    '-> Se repositionner sur la lecture du tableau
                                    SwitchLect = 8
                                Else
                                    '-> Analyse d'un ligne d'un block
                                    InitBlockEntete(aBlock, aTb, aspool, Ligne)
                                End If
                        End Select 'Selon la nature de la lecture de la ligne
                End Select
            End If '-> Si premier caract�re est un [
        Next  'Pour toutes les lignes de la maquette


        '-> G�n�ration ici de la section de texte pour la page des s�lections
        '-> Cr�er nouvel objet RTF
        aSection = New Section
        aSection.Largeur = aspool.Maquette_Renamed.Largeur - aspool.Maquette_Renamed.MargeLeft * 2
        aSection.Hauteur = aspool.Maquette_Renamed.Hauteur - aspool.Maquette_Renamed.MargeTop * 2
        aSection.AlignementLeft = 2
        aSection.AlignementTop = 2
        aSection.BackColor = &HFFFFFF

        '-> Charger un nouvel �diteur RTF dans la biblioth�que
        frmLib.Rtf.Load(IdRTf)
        frmLib.Rtf(IdRTf).Font = VB6.FontChangeName(frmLib.Rtf(IdRTf).Font, "Lucida Console")
        aSection.IdRTf = IdRTf

        '-> Incr�menter le compteur d'objet RTF
        IdRTf = IdRTf + 1

        '-> Ajouter dans la collection des secstion de la maquette
        aspool.Maquette_Renamed.Sections.Add(aSection, SelectRTFKey)


        Exit Sub

GestError:

        If ErrorCode = 13 Then
            Reset()
            Resume Next
        Else
            Call GestError(ErrorCode, TmpError, aspool)
        End If

    End Sub
    Private Function InitFileTempoRtf(ByRef aspool As Spool) As String

        '---> Cette fonction creer un fichier tempo pour lecture des RTF et renvoie :
        ' Handle|NomFichier

        On Error GoTo GestError

        Dim TempFile As String
        Dim hdlFile As String

        TempFile = GetTempFileNameVB("RTF")
        hdlFile = CStr(FreeFile())
        FileOpen(CInt(hdlFile), TempFile, OpenMode.Output)

        InitFileTempoRtf = hdlFile & "|" & TempFile

        Exit Function

GestError:
        Call GestError(17, , aspool)

    End Function

    Private Sub CreateRTFSection(ByRef aSection As Section, ByRef aspool As Spool)

        '---> Cette proc�dure charge le contenu du fichier TEmpoRTF adns l'objet affect� et le supprime
        Dim ErrorCode As Short

        On Error GoTo GestError

        '-> Charger le fichier RTF
        frmLib.Rtf(aSection.IdRTf).LoadFile(aSection.TempoRtfString)
        ErrorCode = -1
        '-> Supprimer le fichier
        Kill(aSection.TempoRtfString)

        Exit Sub

GestError:

        If ErrorCode = -1 Then Resume Next
        Call GestError(25, aSection.Nom & "|" & aSection.TempoRtfString, aspool)


    End Sub

    Private Sub CreateRTFCadre(ByRef aCadre As Cadre, ByRef aspool As Spool)

        '---> Cette proc�dure charge le contenu du fichier TEmpoRTF adns l'objet affect� et le supprime

        On Error GoTo GestError

        Dim ErrorCode As Short

        '-> Charger le fichier RTF
        frmLib.Rtf(aCadre.IdRTf).LoadFile(aCadre.TempoRtfString)

        '-> Supprimer le fichier
        ErrorCode = -1
        Kill(aCadre.TempoRtfString)

        Exit Sub

GestError:

        If ErrorCode = -1 Then Resume Next
        Call GestError(24, aCadre.SectionName & "|" & aCadre.Nom & "|" & aCadre.TempoRtfString, aspool)



    End Sub


    Private Sub InitMaqEntete(ByRef aMaq As Maquette, ByRef Ligne As String, ByRef aspool As Spool)

        '---> Cette proc�dure effectue le setting des propri�t�s de l'entete de la maquette

        Dim DefLigne As String
        Dim ValueLigne As String
        Dim Tempo As Single
        Dim ErrorCode As Short

        On Error GoTo GestError

        '-> Tester que la ligne commence par un "\"
        If InStr(1, Ligne, "\") = 0 Then Exit Sub

        '-> R�cup�ration des valeurs
        DefLigne = Entry(1, Ligne, "�")
        ValueLigne = Entry(2, Ligne, "�")
        '-> Traiter la valeur de la ligne
        Select Case UCase(DefLigne)
            Case "\NOM"
                aMaq.Nom = ValueLigne
            Case "\HAUTEUR"
                aMaq.Hauteur = CSng(Convert(ValueLigne))
            Case "\LARGEUR"
                aMaq.Largeur = CSng(Convert(ValueLigne))
            Case "\MARGETOP"
                aMaq.MargeTop = CSng(Convert(ValueLigne))
            Case "\MARGELEFT"
                aMaq.MargeLeft = CSng(Convert(ValueLigne))
            Case "\ORIENTATION"
                aMaq.Orientation = CStr(CShort(ValueLigne))
            Case "\PAGEGARDE" 'Version 2.0
                If UCase(ValueLigne) = "VRAI" Then
                    aMaq.PageGarde = True
                Else
                    aMaq.PageGarde = False
                End If
            Case "\RIGHTPAGE"
                If UCase(ValueLigne) = "VRAI" Then
                    AllowDebordement = True
                Else
                    AllowDebordement = False
                End If
            Case "\NAVIGATION"
                If UCase(ValueLigne) = "VRAI" Then
                    IsNaviga = True
                Else
                    'IsNaviga = False
                End If
            Case "\PAGESELECTION" 'Version 2.0
                If UCase(ValueLigne) = "VRAI" Then
                    aMaq.PageSelection = True
                Else
                    aMaq.PageSelection = False
                End If
            Case "\DEFENTETE"
                '-> Ajuster les dimensions
                If CDbl(aMaq.Orientation) <> 1 Then
                    Tempo = aMaq.Largeur
                    aMaq.Largeur = aMaq.Hauteur
                    aMaq.Hauteur = Tempo
                End If
            Case "\NAVIGAHTML"
                '-> Setting du navigateur
                aMaq.NavigaHTML = Trim(ValueLigne)

        End Select

        Exit Sub

        '-> Gestion des erreurs
GestError:

        Call GestError(18, Ligne, aspool)


    End Sub

    Private Sub InitSectionEntete(ByRef aspool As Spool, ByRef aSection As Section, ByRef Ligne As String)

        '---> Cette proc�dure effectue le setting des propri�t�s d'une section de texte

        Dim DefLigne As String
        Dim ValueLigne As String

        On Error GoTo GestError

        If InStr(1, Ligne, "\") = 0 Then Exit Sub

        '-> R�cup�rer les valeurs
        DefLigne = UCase(Entry(1, Ligne, "�"))
        ValueLigne = Entry(2, Ligne, "�")

        '-> Analyse
        Select Case DefLigne
            Case "\CONTOUR"
                If CShort(ValueLigne) = 0 Then
                    aSection.Contour = False
                Else
                    aSection.Contour = True
                End If
            Case "\HAUTEUR"
                aSection.Hauteur = CSng(Convert(ValueLigne))
            Case "\LARGEUR"
                aSection.Largeur = CSng(Convert(ValueLigne))
            Case "\ALIGNEMENTTOP"
                aSection.AlignementTop = CShort(ValueLigne)
            Case "\TOP"
                aSection.Top = CSng(Convert(ValueLigne))
            Case "\ALIGNEMENTLEFT"
                aSection.AlignementLeft = CShort(ValueLigne)
            Case "\LEFT"
                aSection.Left_Renamed = CSng(Convert(ValueLigne))
            Case "\BACKCOLOR"
                aSection.BackColor = CInt(ValueLigne)
            Case "\TEXTE"
        End Select

        Exit Sub

GestError:

        Call GestError(27, aSection.Nom & "|" & Ligne, aspool)

    End Sub

    Private Sub InitBmpEntete(ByRef aBmp As ImageObj, ByRef aspool As Spool, ByRef Ligne As String)

        '---> Cette proc�dure effectue le setting des propri�t�s d'un objet BMP

        Dim DefLigne As String
        Dim ValueLigne As String

        On Error GoTo GestError

        If InStr(1, Ligne, "\") = 0 Then Exit Sub

        '-> R�cup�rer les valeurs
        DefLigne = UCase(Entry(1, Ligne, "�"))
        ValueLigne = Entry(2, Ligne, "�")

        '-> Analyse
        Select Case DefLigne
            Case "\LEFT"
                aBmp.Left_Renamed = CSng(Convert(ValueLigne))
            Case "\TOP"
                aBmp.Top = CSng(Convert(ValueLigne))
            Case "\LARGEUR"
                aBmp.Largeur = CSng(Convert(ValueLigne))
            Case "\HAUTEUR"
                aBmp.Hauteur = CSng(Convert(ValueLigne))
            Case "\FICHIER"
                aBmp.Fichier_Renamed = ValueLigne
            Case "\CONTOURBMP"
                If CShort(ValueLigne) = 1 Then
                    aBmp.Contour = True
                Else
                    aBmp.Contour = False
                End If
            Case "\VARIABLE"
                If CShort(ValueLigne) = 1 Then
                    aBmp.isVariable = True
                Else
                    aBmp.isVariable = False
                End If
            Case "\USEASSOCIATION"
                If CShort(ValueLigne) = 1 Then
                    aBmp.UseAssociation = True
                Else
                    aBmp.UseAssociation = False
                End If
            Case "\PATH"
                aBmp.Path = ValueLigne
        End Select

        Exit Sub

GestError:

        Call GestError(28, aBmp.SectionName & "|" & aBmp.Nom & "|" & Ligne, aspool)


    End Sub

    Private Sub InitCadreEntete(ByRef aCadre As Cadre, ByRef aspool As Spool, ByRef Ligne As String)

        '---> Cette proc�dure effectue le setting des propri�t�s d'un cadre

        Dim DefLigne As String
        Dim ValueLigne As String

        On Error GoTo GestError

        If InStr(1, Ligne, "\") = 0 Then Exit Sub

        '-> R�cup�rer les valeurs
        DefLigne = UCase(Entry(1, Ligne, "�"))
        ValueLigne = Entry(2, Ligne, "�")

        '-> Analyse
        Select Case DefLigne
            Case "\LEFT"
                aCadre.Left_Renamed = CSng(Convert(ValueLigne))
            Case "\TOP"
                aCadre.Top = CSng(Convert(ValueLigne))
            Case "\DISTANCE" 'Equivalent � MargeInterne
                aCadre.MargeInterne = CSng(Convert(ValueLigne))
            Case "\HAUTEUR"
                aCadre.Hauteur = CSng(Convert(ValueLigne))
            Case "\LARGEUR"
                aCadre.Largeur = CSng(Convert(ValueLigne))
            Case "\LARGEURBORDURE"
                aCadre.LargeurTrait = CShort(ValueLigne)
            Case "\ROUNDRECT"
                aCadre.IsRoundRect = CShort(ValueLigne)
            Case "\CONTOUR"
                '-> Bordure haut
                If Entry(1, ValueLigne, ",") = "1" Then
                    aCadre.Haut = True
                Else
                    aCadre.Haut = False
                End If
                '-> Bordure bas
                If Entry(2, ValueLigne, ",") = "1" Then
                    aCadre.Bas = True
                Else
                    aCadre.Bas = False
                End If
                '-> Bordure gauche
                If Entry(3, ValueLigne, ",") = "1" Then
                    aCadre.Gauche = True
                Else
                    aCadre.Gauche = False
                End If
                '-> Bordure droite
                If Entry(4, ValueLigne, ",") = "1" Then
                    aCadre.Droite = True
                Else
                    aCadre.Droite = False
                End If
            Case "\COULEUR"
                aCadre.BackColor = CInt(ValueLigne)
        End Select

        Exit Sub

GestError:

        Call GestError(29, aCadre.SectionName & "|" & aCadre.Nom & "|" & Ligne, aspool)


    End Sub

    Private Sub CreateSectionObj(ByRef aSection As Section, ByRef aspool As Spool)

        '---> Cette proc�dure cr�er les diff�rents objets pour impression de la section

        On Error GoTo GestError

        '-> Cr�ation d'un contenuer RTF
        frmLib.Rtf.Load(IdRTf)
        aSection.IdRTf = IdRTf

        '-> Appliquer les propri�t�s
        frmLib.Rtf(IdRTf).BackColor = System.Drawing.ColorTranslator.FromOle(aSection.BackColor)
        If aSection.Contour Then
            frmLib.Rtf(IdRTf).BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Else
            frmLib.Rtf(IdRTf).BorderStyle = System.Windows.Forms.BorderStyle.None
        End If

        '-> Incr�menter le compteur d'objets RTF
        IdRTf = IdRTf + 1
        Exit Sub

GestError:

        Call GestError(26, aSection.Nom, aspool)

    End Sub

    Private Sub CreateCadreObj(ByRef aCadre As Cadre, ByRef aspool As Spool)

        '---> Cette proc�dure cr�er une instance d'un objet RTF pour impression du cadre

        On Error GoTo GestError

        '-> charger un nouvel objet du type RTF
        frmLib.Rtf.Load(IdRTf)
        aCadre.IdRTf = IdRTf

        '-> Setting des prori�t�s
        frmLib.Rtf(IdRTf).BackColor = System.Drawing.ColorTranslator.FromOle(aCadre.BackColor)
        frmLib.Rtf(IdRTf).Visible = True

        '-> Incr�menter le compteur d'objets
        IdRTf = IdRTf + 1

        Exit Sub

GestError:

        Call GestError(23, aCadre.SectionName & "|" & aCadre.Nom, aspool)



    End Sub

    Private Sub CreateBmpObj(ByRef aBmp As ImageObj, ByRef aspool As Spool)

        '---> Cette proc�dure cr�er une instance d'un objet picturebox pour impression du BMP

        Dim Res As Integer
        Dim lpBuffer As String = ""
        Dim TempoStr As String = ""
        Dim Version As String = ""
        Dim Lecteur As String = ""
        Dim i As Short
        Dim ErrorCode As Short
        Dim TmpError As String = ""
        Dim V6Root As String = ""
        Dim ImagePath As String = ""


        On Error GoTo GestError

        '-> Chargement de la repr�sentation physique
        ErrorCode = 19
        TmpError = ""
        frmLib.PicObj.Load(IdBmp)

        '-> Contour du Bmp
        If aBmp.Contour Then
            frmLib.PicObj(IdBmp).BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Else
            frmLib.PicObj(IdBmp).BorderStyle = System.Windows.Forms.BorderStyle.None
        End If

        '-> Donner ses dimensions au Bmp
        ErrorCode = 20
        TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Largeur & "|" & aBmp.Hauteur
        'UPGRADE_ISSUE: Form m�thode frmLib.ScaleX - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        frmLib.PicObj(IdBmp).Height = VB6.TwipsToPixelsY(aBmp.Hauteur)
        'UPGRADE_ISSUE: Form m�thode frmLib.ScaleY - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        frmLib.PicObj(IdBmp).Width = VB6.TwipsToPixelsX(aBmp.Largeur)

        '-> Selon la version
        If VersionTurbo = 0 Then 'On est en version Pathv6

            '-> V�rifier que l'on trouve le fichier Ini
            If TurboMaqIniFile = "" Then GoTo AfterLoading

            '-> R�cup�rer le path Root
            lpBuffer = Space(100)
            Res = GetPrivateProfileString("Param", "$ROOT", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
            If Res <> 0 Then V6Root = Mid(lpBuffer, 1, Res)

            Select Case UCase(aBmp.Path)
                Case "PARAM"
                    Res = GetPrivateProfileString("Param", "$MAQGUIPATH", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
                    If Res = 0 Then
                        aBmp.Fichier_Renamed = ""
                        GoTo AfterLoading
                    End If
                    ImagePath = Replace(Replace(UCase(Mid(lpBuffer, 1, Res)), "$ROOT", V6Root), "$APP", "emilie")
                Case "APP"
                    Res = GetPrivateProfileString("Param", "$MAQGUIPATH", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
                    If Res = 0 Then
                        aBmp.Fichier_Renamed = ""
                        GoTo AfterLoading
                    End If
                    ImagePath = Replace(Replace(UCase(Mid(lpBuffer, 1, Res)), "$ROOT", V6Root), "$APP", aspool.Maquette_Renamed.Progiciel)
                Case "CLIENT"
                    Res = GetPrivateProfileString("Param", "$IDENTPATH", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
                    If Res = 0 Then
                        aBmp.Fichier_Renamed = ""
                        GoTo AfterLoading
                    End If
                    ImagePath = Replace(Replace(Replace(UCase(Mid(lpBuffer, 1, Res)), "$ROOT", V6Root), "$APP", aspool.Maquette_Renamed.Progiciel), "$IDENT", aspool.Maquette_Renamed.Client)
            End Select

            '-> V�rifier si on trouve l'image
            'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            If Dir(ImagePath & "\" & aBmp.Fichier_Renamed) <> "" Then
                ErrorCode = 22
                TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Fichier_Renamed
                frmLib.PicObj(IdBmp).Image = System.Drawing.Image.FromFile(ImagePath & "\" & aBmp.Fichier_Renamed)
                '        aBmp.Fichier = lpBuffer & aBmp.Fichier PIERROT : client-serveur en VERSION V6
                aBmp.Fichier_Renamed = ImagePath & "\" & aBmp.Fichier_Renamed
                ' Else   PIERROT : client-serveur en VERSION V6 : ne pas changer le nom du fichier de forme ^0001 si path parametr� => MODE PARAM
                '     aBmp.Fichier = ""
            End If 'Si on trouve l'image
        Else
            '-> On est en version professionnelle
            If aBmp.isVariable Then
                aBmp.IsAutosize = True
            Else
                aBmp.IsAutosize = False
                If Trim(aBmp.Fichier_Renamed) <> "" Then
                    '-> Rechercher l'image dans le sous r�pertoire Images
                    lpBuffer = My.Application.Info.DirectoryPath & "\Images\" & aBmp.Fichier_Renamed
                    '-> Tester que l'on trouve l'image
                    'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
                    If Dir(lpBuffer, FileAttribute.Normal) <> "" Then
                        '-> Essayer de charger l'image
                        ErrorCode = 22
                        TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Fichier_Renamed
                        frmLib.PicObj(IdBmp).Image = System.Drawing.Image.FromFile(lpBuffer)
                        aBmp.Fichier_Renamed = lpBuffer
                    Else
                        '-> impossible de trouver le fichier associ�
                        aBmp.Fichier_Renamed = ""
                    End If
                Else 'La propri�t� fichier est � blanc
                    aBmp.Fichier_Renamed = ""
                End If
            End If
        End If 'Selon la version

AfterLoading:

        '-> Positionner l'objet
        'UPGRADE_ISSUE: PictureBox m�thode PicObj.ScaleY - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        frmLib.PicObj(IdBmp).Top = VB6.TwipsToPixelsY(aBmp.Top)
        'UPGRADE_ISSUE: PictureBox m�thode PicObj.ScaleX - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        frmLib.PicObj(IdBmp).Left = VB6.TwipsToPixelsX(aBmp.Left_Renamed)
        'UPGRADE_WARNING: propri�t� PicObj.AutoSize de PictureBox a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
        frmLib.PicObj(IdBmp).SizeMode = aBmp.IsAutosize

        '-> Enregistrer l'index du picture box associ�
        aBmp.IdPic = IdBmp

        '-> Rendre l'objet visible
        frmLib.PicObj(IdBmp).Visible = True

        '-> Incr�menter
        IdBmp = IdBmp + 1


        Exit Sub

GestError:

        Call GestError(ErrorCode, TmpError, aspool)


    End Sub

    Private Sub InitTableauEntete(ByRef aTb As Tableau, ByRef aspool As Spool, ByRef Ligne As String)

        '---> Cette proc�dure effectue le settin g des prorpi�t�s d'un tableau

        Dim DefLigne As String
        Dim ValueLigne As String

        On Error GoTo GestError

        If InStr(1, Ligne, "\") = 0 Then Exit Sub

        '-> R�cup�rer les valeurs
        DefLigne = UCase(Entry(1, Ligne, "�"))
        ValueLigne = Entry(2, Ligne, "�")

        '-> Analyse
        Select Case DefLigne
            Case "\LARGEURTB"
                aTb.Largeur = CSng(Convert(ValueLigne))
            Case "\ORIENTATIONTB"
                aTb.Orientation = CShort(ValueLigne)
        End Select


        Exit Sub



GestError:
        Call GestError(30, aTb.Nom & "|" & Ligne, aspool)



    End Sub

    Private Sub InitBlockEntete(ByRef aBlock As Block, ByRef aTb As Tableau, ByRef aspool As Spool, ByRef Ligne As String)

        '---> Cette proc�dure effectue le setting des propri�t�s d'un block

        Dim DefLigne As String
        Dim ValueLigne As String

        On Error GoTo GestError

        If InStr(1, Ligne, "\") = 0 Then Exit Sub

        '-> R�cup�rer les valeurs
        DefLigne = UCase(Entry(1, Ligne, "�"))
        ValueLigne = Entry(2, Ligne, "�")

        '-> Analyse
        Select Case DefLigne
            Case "\ALIGNEMENT" 'Garder pour compatibilit� 1.00
                If CShort(ValueLigne) = 0 Then 'Marge gauche
                    aBlock.AlignementLeft = 2
                ElseIf CShort(ValueLigne) = 1 Then  'Marge Droite
                    aBlock.AlignementLeft = 4
                ElseIf CShort(ValueLigne) = 2 Then  'Centr�
                    aBlock.AlignementLeft = 3
                ElseIf CShort(ValueLigne) = 3 Then  'Alignement Libre
                    aBlock.AlignementLeft = 5 'La valeur Left est aliment�e par la valeur "\DISTANCE"
                End If
            Case "\ALIGNLEFT" 'Version 2.00 de "\Alignement"
                aBlock.AlignementLeft = CShort(ValueLigne)
            Case "\LEFT" 'Valeur de AlignLeft quand il est sp�cifi� : valeur 5
                aBlock.Left_Renamed = CSng(Convert(ValueLigne))
            Case "\ALIGNTOP" 'Version  2.00 de "\AlignementBlockVertical"
                aBlock.AlignementTop = CShort(ValueLigne)
            Case "\TOP" 'Valeur de AlignTop quand il est sp�cifi� : valeur 5
                aBlock.Top = CSng(Convert(ValueLigne))
            Case "\LARGEUR"
                aBlock.Largeur = CSng(Convert(ValueLigne))
            Case "\HAUTEUR"
                aBlock.Hauteur = CSng(Convert(ValueLigne))
            Case "\LIGNE"
                aBlock.NbLigne = CShort(ValueLigne)
                'UPGRADE_WARNING: La limite inf�rieure du tableau sLigne est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
                ReDim sLigne(CShort(ValueLigne))
            Case "\COLONNE"
                aBlock.NbCol = CShort(ValueLigne)
                'UPGRADE_WARNING: La limite inf�rieure du tableau sCol est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
                ReDim sCol(CShort(ValueLigne))
            Case "\EXPORTLIG"
                aBlock.ListeExcel = ValueLigne
            Case "\ACCESDET"
                aBlock.KeyAccesDet = Trim(ValueLigne)
            Case "\COL"
                sLigne(LigLue) = Ligne
                LigLue = LigLue + 1
        End Select

        Exit Sub

GestError:
        Call GestError(31, aTb.Nom & "|" & aBlock.Nom & "|" & Ligne, aspool)

    End Sub
    Private Sub InitCreateBlock(ByRef aBlock As Block, ByRef aTb As Tableau, ByRef aspool As Spool)

        '---> Cette proc�dure cr�er les diff�rentes cellules � partir des matrices sCol et sLigne

        Dim aCell As Cellule
        Dim i As Short
        Dim j As Short
        Dim DefLigne As String
        Dim DefCellule As String
        Dim DefBordure As String
        Dim ErrorCode As Short
        Dim AccesDetail As String
        Dim ListeCell As String

        On Error GoTo GestError

        '-> Initialiser les colonnes et les lignes
        aBlock.Init()

        '-> Gestion des exports des lignes vers Excel Attention , cette propri�t� _
        'ne contient que la liste des lignes non exportables vers EXCEL
        If Trim(aBlock.ListeExcel) <> "" Then
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(aBlock.ListeExcel, |). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For i = 1 To NumEntries(aBlock.ListeExcel, "|")
                aBlock.SetExportExcel(CShort(Entry(i, aBlock.ListeExcel, "|")), False)
            Next
        End If

        '-> Alimenter les Matrices de ligne te de colonne et cr�ation des cellules
        For i = 1 To LigLue - 1
            '-> R�cup�ration de la d�finition de la ligne
            ErrorCode = 32
            DefLigne = sLigne(i)
            '-> Setting de la hauteur de ligne
            aBlock.SetHauteurLig(i, CSng(Convert(Entry(2, DefLigne, "�"))))
            '-> Recup�rer QUE la liste des colonnes
            DefLigne = Entry(3, DefLigne, "�")
            For j = 1 To aBlock.NbCol
                ErrorCode = 33
                '-> Recup de la d�finition de la colonne
                DefCellule = Entry(j, DefLigne, "|")
                '-> Setting de la largeur
                aBlock.SetLargeurCol(j, CSng(Convert(Entry(3, DefCellule, ";"))))
                '-> Cr�ation de la cellule associ�e
                aCell = New Cellule
                aCell.Ligne = i
                aCell.Colonne = j
                '-> Ajout dans le tableau
                aBlock.Cellules.Add(aCell, "L" & i & "C" & j)
                '-> setting des propri�t�s
                aCell.CellAlign = CShort(Entry(1, DefCellule, ";"))
                aCell.Contenu = Entry(2, DefCellule, ";")
                '-> Initialisation des champs
                aCell.InitChamp()
                '-> Gestion des bordures
                DefBordure = Entry(4, DefCellule, ";")
                If UCase(Entry(1, DefBordure, ",")) = "VRAI" Then
                    aCell.BordureHaut = True
                Else
                    aCell.BordureHaut = False
                End If
                If UCase(Entry(2, DefBordure, ",")) = "VRAI" Then
                    aCell.BordureBas = True
                Else
                    aCell.BordureBas = False
                End If
                If UCase(Entry(3, DefBordure, ",")) = "VRAI" Then
                    aCell.BordureGauche = True
                Else
                    aCell.BordureGauche = False
                End If
                If UCase(Entry(4, DefBordure, ",")) = "VRAI" Then
                    aCell.BordureDroite = True
                Else
                    aCell.BordureDroite = False
                End If
                '-> Propri�t�s de Font
                aCell.FontName = Entry(5, DefCellule, ";")
                aCell.FontSize = CSng(Convert(Entry(6, DefCellule, ";")))
                If UCase(Trim(Entry(7, DefCellule, ";"))) = "VRAI" Then
                    aCell.FontBold = True
                Else
                    aCell.FontBold = False
                End If
                If UCase(Trim(Entry(8, DefCellule, ";"))) = "VRAI" Then
                    aCell.FontItalic = True
                Else
                    aCell.FontItalic = False
                End If
                If UCase(Trim(Entry(9, DefCellule, ";"))) = "VRAI" Then
                    aCell.FontUnderline = True
                Else
                    aCell.FontUnderline = False
                End If
                aCell.FontColor = CInt(Entry(10, DefCellule, ";"))
                If CInt(Entry(11, DefCellule, ";")) = 999 Then
                    aCell.BackColor = 16777215
                    aCell.FondTransparent = True
                Else
                    aCell.BackColor = CInt(Entry(11, DefCellule, ";"))
                    aCell.FondTransparent = False
                End If
                '-> AutoAjust
                If UCase(Entry(12, DefCellule, ";")) = "VRAI" Then
                    aCell.AutoAjust = True
                Else
                    aCell.AutoAjust = False
                End If
                '-> Format et Type
                DefBordure = Entry(13, DefCellule, ";")
                If Entry(1, DefBordure, "@") <> "" Then aCell.TypeValeur = CShort(Entry(1, DefBordure, "@"))
                aCell.Msk = Entry(2, DefBordure, "@")
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(DefBordure, ). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If NumEntries(DefBordure, "@") = 3 Then aCell.PrintZero = True

                '-> Export vers Excel Fusion
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(DefCellule, ;). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If NumEntries(DefCellule, ";") > 13 Then
                    '-> R�cup�rer le param�trage
                    DefBordure = Entry(14, DefCellule, ";")
                    aCell.IsFusion = CBool(Entry(1, DefBordure, "@"))
                    aCell.ColFusion = CShort(Entry(2, DefBordure, "@"))
                End If

                '-> Impl�mnter le mod�le objet associ� � excel
                aCell.TurboToExcel()

            Next  'Pour toute les colonnes
        Next  'Pour toutes les lignes

        '-> Gestion de l'acc�s au d�tail
        If aBlock.KeyAccesDet <> "" Then
            '-> Basculer
            AccesDetail = aBlock.KeyAccesDet
            '-> Setting des param�tres du block
            aBlock.UseAccesDet = True
            aBlock.KeyAccesDet = Entry(1, AccesDetail, "�")
            '-> Setting des cellules servant � l'acc�s au d�tail
            ListeCell = Entry(2, AccesDetail, "�")
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(ListeCell, |). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For i = 1 To NumEntries(ListeCell, "|")
                '-> Pointeur vers une cellule
                aCell = aBlock.Cellules.Item("L" & Entry(1, Entry(i, ListeCell, "|"), "-") & "C" & Entry(2, Entry(i, ListeCell, "|"), "-"))
                '-> forcer le format
                aCell.FontColor = RGB(0, 0, 255)
                aCell.FontUnderline = True
                '-> Indiquer qu'elle sert d'acc�s au d�tail
                aCell.UseAccesDet = True
            Next  'Pour toutes les d�finitions de cellules
        End If 'S'il y a l'acc�s au d�tail sur ce block

        '-> Convertion des dimensions des cellules en pixels
        InitBlock(aBlock, aspool)


        Exit Sub

GestError:
        If ErrorCode = 32 Then
            Call GestError(ErrorCode, aTb.Nom & "|" & aBlock.Nom, aspool)
        Else
            Call GestError(ErrorCode, aTb.Nom & "|" & aBlock.Nom & "|" & DefCellule, aspool)
        End If



    End Sub



    Public Sub InitBlock(ByVal BlockCours As Block, ByRef aspool As Spool)

        '---> Cette proc�dure initialise les matrices Lignes (), Colonnes () et Cells _
        'en partant des largeurs de colonnes et hauteur de lignes

        Dim i As Short
        Dim j As Short
        Dim NbCol As Short
        Dim NbLig As Short
        '-> Dimensions en CM
        Dim HauteurCm As Single
        Dim LargeurCm As Single
        '-> Convertion en Pixels pour dessin
        Dim HauteurPix As Integer
        Dim LargeurPix As Integer
        Dim hPix As Integer
        Dim lPix As Integer
        '-> Matrices des lignes et colonnes
        Dim Lignes() As Integer
        Dim Colonnes() As Integer

        Dim LargeurColPix As Integer
        Dim HauteurLigPix As Integer

        Dim aCell As Cellule
        Dim PosX As Integer
        Dim PosY As Integer

        On Error GoTo ErrorOpen

        '-> R�cup�ration des propri�t�s
        NbCol = BlockCours.NbCol
        NbLig = BlockCours.NbLigne
        HauteurCm = BlockCours.Hauteur
        LargeurCm = BlockCours.Largeur

        '-> Convertion en pixels
        LargeurPix = CInt(LargeurCm) * CDbl(37.79524)
        HauteurPix = CInt(CDbl(HauteurCm)) * CDbl(37.79524)

        '-> Affecter la valeur au block
        BlockCours.LargeurPix = LargeurPix
        BlockCours.HauteurPix = HauteurPix

        '-> Calcul des dimensions de base
        ReDim Lignes(NbLig)
        ReDim Colonnes(NbCol)

        '-> Largeur de base des colonnes
        For i = 1 To NbCol
            Colonnes(i) = Fix((BlockCours.GetLargeurCol(i) / LargeurCm) * ((LargeurPix - NbCol + 1)))
            lPix = lPix + Colonnes(i)
        Next

        '-> Hauteur de base des lignes
        For i = 1 To NbLig
            Lignes(i) = Fix((BlockCours.GetHauteurLig(i) / HauteurCm) * (HauteurPix - (NbLig + 1)))
            hPix = hPix + Lignes(i)
        Next

        Dim aCumul As Double
        Dim bCumul As Integer
        Dim a As Double

        '-> Lissage des colonnes
        bCumul = 1

        For i = 1 To NbCol
            '-> Largeur en pixel normale
            aCumul = aCumul + BlockCours.GetLargeurCol(i)
            LargeurColPix = CInt(aCumul * CDbl(37.79524))
            bCumul = bCumul + Colonnes(i) + 1
            a = LargeurColPix - bCumul
            Colonnes(i) = Colonnes(i) + a
            bCumul = LargeurColPix
        Next

        '-> Lissage des lignes
        aCumul = 0
        bCumul = 1

        For i = 1 To NbLig
            '-> Hauteur de pixel normale
            aCumul = aCumul + BlockCours.GetHauteurLig(i)
            HauteurLigPix = CShort(aCumul * CDbl(37.79524))
            bCumul = bCumul + Lignes(i) + 1
            a = HauteurLigPix - bCumul
            Lignes(i) = Lignes(i) + a
            bCumul = HauteurLigPix
        Next

        '-> Pixel de d�part
        PosY = 1
        PosX = 1

        For i = 1 To NbCol
            For j = 1 To NbLig
                '-> Mettre � jour les valeurs des cellules
                aCell = BlockCours.Cellules.Item("L" & j & "C" & i)
                aCell.X1 = PosX
                aCell.Y1 = PosY
                aCell.X2 = PosX + Colonnes(i) + 1
                aCell.Y2 = PosY + Lignes(j) + 1
                '-> Incr�menter le compteur de position
                PosY = PosY + Lignes(j) + 1
            Next  'pour toutes les lignes
            '-> initialisation des valeurs
            PosX = PosX + Colonnes(i) + 1
            PosY = 1
        Next  'Pour toutes les colonnes

        '-> Lib�rer le pointeur de cellule
        aCell = Nothing

        '-> Sortir de la fonction
        Exit Sub

ErrorOpen:

        Call GestError(34, BlockCours.NomTb & "|" & BlockCours.Nom, aspool)

    End Sub

    Public Sub DisplayFileGUI(ByVal FileName As String)
        Dim frmDisplay As Object

        '---> Cette proc�dure charge et affiche un fichier en particulier

        Dim aNode As System.Windows.Forms.TreeNode
        Dim aNode2 As System.Windows.Forms.TreeNode
        Dim aNode3 As System.Windows.Forms.TreeNode
        Dim aFichier As Fichier
        Dim aspool As Spool
        Dim i As Short
        Dim aLb As Libelle
        Dim aFrm As frmDisplaySpool
        Dim IsTitrePage As Boolean

        On Error Resume Next

        '-> Bloquer l'interface
        myMDIMain.Enabled = False
        'UPGRADE_WARNING: propri�t� Screen.MousePointer de Screen a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

        '-> Positionner le p�riph�rique d'impression sur un objet de type PictureBox
        Sortie = frmLib.PicObj(0)

        '-> Lancer le chargement et l'analyse du fichier s�lectionn�
        Call AnalyseFileToPrint(FileName)

        '-> Cr�er son icone dans le treeview
        aNode = myMDIMain.TreeNaviga.Nodes.Add(UCase(Trim(FileName)), FileName, "Close")
        aNode.Tag = "FICHIER"
        'UPGRADE_ISSUE: MSComctlLib.Node propri�t� aNode.ExpandedImage - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        aNode.SelectedImageKey = "Open"
        aNode.Expand()

        '-> Pointer sur la classe libell�
        aLb = Libelles.Item("MDIMAIN")

        '-> Pointer sur le fichier
        aFichier = Fichiers.Item(UCase(Trim(FileName)))

        '-> Cr�er une icone par spool
        For Each aspool In aFichier.Spools
            '-> on regarde si on a specifi� un titre
            If InStr(1, aspool.GetMaq, "[TITRE]") <> 0 Then
                IsTitrePage = True
            Else
                IsTitrePage = False
            End If

            aNode2 = myMDIMain.TreeNaviga.Nodes.Find(aNode.Name, True)(0).Nodes.Add(aNode.Name & "�" & aspool.Key, aspool.SpoolText, "Spool")
            aNode2.Tag = "SPOOL"
            aNode2.Expand()
            '-> Soit la liste des erreurs
            If aspool.NbError = 0 Then
                '-> Afficher une page pour la liste des s�lections utilisateur
                If aspool.IsSelectionPage Then
                    aNode3 = myMDIMain.TreeNaviga.Nodes.Add(aNode2.Name & "�PAGE|0", aLb.GetCaption(12) & "S�lection", "Page")
                    aNode3.Tag = "PAGE"
                End If

                '-> Afficher une icone par page
                For i = 1 To aspool.NbPage
                    '-> Recuperer eventuellement le titre de la feuille
                    If IsTitrePage Then
                        'aNode3 = myMDIMain.TreeNaviga.Nodes.Find(aNode2.Name, True)(0).Nodes.Add(aNode2.Name & "�PAGE|" & i, GetTitrePage(aspool, i), "Page")
                        aNode3 = myMDIMain.TreeNaviga.Nodes.Add(aNode2.Name & "�PAGE|" & i, GetTitrePage(aspool, i), "Page")
                    Else
                        aNode3 = myMDIMain.TreeNaviga.Nodes.Add(aNode2.Name & "�PAGE|" & i, aLb.GetCaption(12) & i, "Page")
                    End If
                    aNode3.Tag = "PAGE"
                Next
            Else
                '-> Afficher l'icone de la page d'erreur
                aNode3 = myMDIMain.TreeNaviga.Nodes.Find(aNode2.Name, True)(0).Nodes.Add(aNode2.Name & "�ERROR", aLb.GetCaption(11), "Warning")
                aNode3.Tag = "ERROR"
            End If 'S'il y a des erreurs
        Next aspool 'Pour tous les spools

        '-> Afficher le premier Spool dans l'interface
        aspool = aFichier.Spools.Item(1)

        '-> Cr�er une nouvelle instance
        aFrm = New frmDisplaySpool
        '-> Affect� le spool
        aFrm.aSpool = aspool
        '-> Affecter la feuille au spool
        aspool.frmDisplay = aFrm
        '-> Imprimer si necessaire
        If aspool.NbError <> 0 Then
            aspool.DisplayInterfaceByPage((1))
            '-> Charger le fichier en erreur
            aFrm.RtfError.LoadFile(aspool.FileName)
        Else
            '-> Imprimer la premi�re page
            PrintPageSpool(aspool, 1)
            '-> Afficher la feuille en fonction du r�sultat de l'impression
            aspool.DisplayInterfaceByPage((1))
        End If

GestError:
        '-> Bloquer l'interface
        myMDIMain.Enabled = True
        'UPGRADE_WARNING: propri�t� Screen.MousePointer de Screen a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default


    End Sub


    Public Sub PrintPageSpool(ByRef aspool As Spool, ByVal PageToPrint As Short)
        Dim frmDisplay As Object

        '---> Cette proc�dure imprime une page d'un spool dans un fichier pdf

        Dim i, j As Short
        Dim PositionX As Integer
        Dim PositionY As Integer
        Dim IsPaysage As Boolean
        Dim Ligne As String
        Dim TypeObjet As String = ""
        Dim NomObjet As String = ""
        Dim TypeSousObjet As String = ""
        Dim NomSousObjet As String = ""
        Dim Param As String = ""
        Dim DataFields As String = ""
        Dim FirstObj As Boolean
        Dim aNode As System.Windows.Forms.TreeNode
        Dim ErrorCode As Short
        Dim aSection As Section
        Dim x As System.Windows.Forms.Control
        Dim MargeOld As Short

        On Error GoTo GestError
        aSection = Nothing

        '-> on initialise la page
        aPrinter.BeginPage()
        aPrinter.SetMargins(aspool.Maquette_Renamed.MargeLeft, 0, aspool.Maquette_Renamed.MargeTop, 1)
        aPrinter.SetPaperSize(vbPDF.pdfPaperSize.pdfUserSize, aspool.Maquette_Renamed.Largeur, aspool.Maquette_Renamed.Hauteur)
        '-> Initialiser le RTF d'impression de la page des s�lections
        If PageToPrint = 0 Then
            '-> Pointer sur la section texte
            aSection = aspool.Maquette_Renamed.Sections.Item(SelectRTFKey)
            '-> Vider le RTF associ�
            frmLib.Rtf(aSection.IdRTf).Text = ""
        End If

        MargeX = 0
        MargeY = 0

        '-> Initialiser la position X , y du pointeur sur les marges du document
        PositionY = aspool.Maquette_Renamed.MargeTop
        PositionX = aspool.Maquette_Renamed.MargeLeft

        '-> Indiquer l'�tat du premier objet que l'on trouve
        FirstObj = True

        '-> Lecture des lignes de la page
        For i = 1 To NumEntries(aspool.GetPage(PageToPrint), Chr(0))
            '-> R�cup�ration de la ligne en cours
            Ligne = Trim(Entry(i, aspool.GetPage(PageToPrint), Chr(0)))
            '-> Ne rien imprimer si page � blanc
            If Trim(Ligne) = "" Then GoTo NextLigne

            If PageToPrint = 0 Then
                frmLib.Rtf(aSection.IdRTf).Text = frmLib.Rtf(aSection.IdRTf).Text & Chr(13) & Chr(10) & Ligne
                frmLib.Rtf(aSection.IdRTf).SelectionStart = 0
                frmLib.Rtf(aSection.IdRTf).SelectionLength = Len(frmLib.Rtf(aSection.IdRTf).Text)
                frmLib.Rtf(aSection.IdRTf).SelectionFont = VB6.FontChangeName(frmLib.Rtf(aSection.IdRTf).SelectionFont, "Lucida Console")
            Else
                If InStr(1, Ligne, "[") = 1 Then
                    '-> r�cup�ration des param�tres
                    AnalyseObj(Ligne, TypeObjet, NomObjet, TypeSousObjet, NomSousObjet, Param, DataFields)
                    '-> Selon le type d'objet
                    If UCase(TypeObjet) = "ST" Then
                        If Not PrintSection(NomObjet, Param, DataFields, FirstObj, PositionX, PositionY, aspool) Then Exit For
                        FirstObj = False
                    ElseIf UCase(TypeObjet) = "TB" Then
                        If Not PrintTableau(NomObjet, Param, DataFields, PositionX, PositionY, NomSousObjet, FirstObj, aspool) Then Exit For
                        FirstObj = False
                    End If
                End If 'Si premier caract�re = "["
            End If 'Si on imprime la page de s�lection
NextLigne:
        Next  'Pour toutes les lignes de la page

        '-> Imprimer la page de s�lection
        If PageToPrint = 0 Then
            '-> imprimer la section
            PrintObjRtf(0, 0, aSection, "", 0, 0, aspool)
        End If

        '-> on termine la page
        aPrinter.EndPage()

        Exit Sub

GestError:
        MsgBox(Err.Number & " " & Err.Description)
        '-> Setting d'une erreur
        Call GestError(ErrorCode, CStr(PageToPrint), aspool)

    End Sub


    Private Function PrintBlock(ByRef NomSousObjet As String, ByRef Param As String, ByRef DataFields As String, ByVal PosX As Integer, ByRef PosY As Integer, ByRef aTb As Tableau, ByVal Ligne As Short, ByRef aspool As Spool) As Integer

        '---> Cette Proc�dure imprime un block de ligne

        Dim aBlock As Block
        Dim Champ As String
        Dim aField() As String
        Dim i As Integer
        Dim aCell As Cellule
        Dim HauteurLigPix As Integer
        Dim ListeField() As String
        Dim Tempo As Object

        On Error GoTo GestBlock

        '-> Pointer sur le block de tableau � �diter
        aBlock = aTb.Blocks.Item("BL-" & UCase(NomSousObjet))

        '-> Initaliser le block
        InitBlock(aBlock, aspool)

        '-> Calculer la modification des positions X et Y des blocks de ligne. _
        'Les coordonn�es de chaque cellule �tant calcul�e sur un X et Y = 0 de base

        Select Case aBlock.AlignementLeft
            Case 2 '-> Marge gauche
                PosX = aspool.Maquette_Renamed.MargeLeft
            Case 3 '-> Centr�
                PosX = (aspool.Maquette_Renamed.Largeur - aBlock.Largeur) / 2
            Case 4 '-> Marge droite
                PosX = aspool.Maquette_Renamed.Largeur - aBlock.Largeur - aspool.Maquette_Renamed.MargeLeft
            Case 5 '-> Sp�cifi�
                PosX = aBlock.Left_Renamed
        End Select

        '-> Tenir compte de la marge interne
        PosX = (PosX - MargeX)

        If aBlock.AlignementTop = 5 And Ligne <> 1 Then
        Else
            Select Case aBlock.AlignementTop
                Case 1 '-> Libre
                    '-> RAS le pointeur est bien positionn�
                Case 2 '-> Marge haut
                    PosY = aspool.Maquette_Renamed.MargeTop
                Case 3 '-> Centr�
                    PosY = (aspool.Maquette_Renamed.Hauteur - aBlock.Hauteur) / 2
                Case 4 '-> Marge Bas
                    PosY = aspool.Maquette_Renamed.Hauteur - aBlock.Hauteur
                Case 5 '-> Sp�cifi�
                    PosY = aBlock.Top
            End Select

            '-> Modifier l'alignement
            If aBlock.AlignementTop <> 1 Then PosY = PosY - MargeY

        End If

        '-> Impression du block de ligne
        For i = 1 To aBlock.NbCol
            '-> Pointer sur la cellule � dessiner
            aCell = aBlock.Cellules.Item("L" & Ligne & "C" & i)
            '-> Remplacer les champs par leur valeur
            aCell.ReplaceField(DataFields)
            '-> Imprimer la cellule
            If Not DrawCell(aCell, PosX, PosY, aspool, (aTb.Nom), NomSousObjet) Then GoTo GestBlock
            '-> R�cup�rer la hauteur de la ligne
            HauteurLigPix = (aCell.Y2 - aCell.Y1)
        Next

        If Ligne = aBlock.NbLigne Then PosY = PosY + 1
        PrintBlock = PosY + HauteurLigPix

        Exit Function

GestBlock:

        Call GestError(40, aTb.Nom & "|" & NomSousObjet, aspool)
        PrintBlock = -9999



    End Function


    Private Function IsGoodFont(ByVal MyFont As String) As String

        '---> Cette fonction d�termine si une font est valide
        On Error GoTo GestError

        frmLib.Font = VB6.FontChangeName(frmLib.Font, MyFont)
        IsGoodFont = MyFont

        Exit Function

GestError:

        IsGoodFont = "Arial"


    End Function


    Public Function DrawCell(ByRef aCell As Cellule, ByVal PosX As Integer, ByVal PosY As Integer, ByRef aspool As Spool, ByRef NomTb As String, Optional ByRef NomBlock As String = "") As Boolean
        Dim frmDisplay As Object

        '---> Fonction qui dessine une cellule
        Dim aFt As FormatedCell
        Dim PrintSigne As Boolean
        Dim Couleur As Integer
        Dim Align As Integer
        Dim aTb As Tableau
        Dim aBlock As Block
        Dim FontAlias As String
        Dim i As Short
        Dim IsCellAjust As Boolean
        Dim y1, x1, X2, Y2 As Integer
        Dim acellStyle As vbPDF.pdfFontStyle

        On Error GoTo GestErr

        x1 = aCell.X1 + PosX
        X2 = aCell.X2 + PosX
        y1 = PosY
        Y2 = (aCell.Y2) + PosY - (aCell.Y1)

        If Not aCell.FondTransparent Then

        End If


        '-> Gestion des formats du texte de la cellule
        Select Case aCell.TypeValeur
            Case 0 '-> RAS
                PrintSigne = False
            Case 1 '-> Caract�re
                '-> Faire un susbstring
                aCell.ContenuCell = Mid(aCell.ContenuCell, 1, CShort(aCell.Msk))
                PrintSigne = False
            Case 2 '-> Numeric
                '-> V�rifier s'il y a quelquechose � formater
                If Trim(aCell.ContenuCell) = "" Then
                Else
                    aFt = FormatNumTP(Trim(aCell.ContenuCell), aCell.Msk, NomBlock, "L" & aCell.Ligne & "C" & aCell.Colonne)
                    '-> Rajouter Trois blancs au contenu de la cellule
                    aFt.strFormatted = aFt.strFormatted & "   "
                    If aFt.Ok Then
                        '-> Mettre e rouge si necessaire
                        If aFt.value < 0 And aFt.idNegatif > 1 Then aCell.FontColor = QBColor(12)
                        '-> affecter le contenu de la cellule
                        aCell.ContenuCell = aFt.strFormatted
                        '-> Si c'est un 0 mettre � blanc si pas imprimer
                        If CDbl(aCell.ContenuCell) = 0 And Not (aCell.PrintZero) Then
                            aCell.ContenuCell = ""
                        Else
                            If aFt.value < 0 Then
                                If aFt.idNegatif = 0 Or aFt.idNegatif = 3 Then
                                    aCell.ContenuCell = "- " & aCell.ContenuCell
                                    PrintSigne = False
                                ElseIf aFt.idNegatif = 1 Or aFt.idNegatif = 4 Then
                                    PrintSigne = True
                                End If
                            End If
                        End If
                    Else
                        '-> Ne pas imprimer  de signe
                        PrintSigne = False
                    End If
                End If
        End Select
        '-> on ajoute si besoin la font
        FontAlias = aPrinter.AddFont(GetFontFileName(aCell.FontName), "", False)
        '-> Appliquer les options de font
        acellStyle = vbPDF.pdfFontStyle.pdfNormal
        '-> si la cellule est en gras
        If aCell.FontBold Then acellStyle = acellStyle Or vbPDF.pdfFontStyle.pdfBold
        '-> si la cellule est en italique
        If aCell.FontItalic Then acellStyle = acellStyle Or vbPDF.pdfFontStyle.pdfItalic
        '-> si la cellule est soulign�e
        If aCell.FontUnderline Then acellStyle = acellStyle Or vbPDF.pdfFontStyle.pdfUnderline

        '-> on applique le style
        aPrinter.SetFont(FontAlias, 0.0!, acellStyle)

        '-> on affecte les bonnes couleurs � la cellule
        aPrinter.SetColorText("Black")
        aPrinter.SetColorFill("#" & ConvertColor(aCell.BackColor))
        aPrinter.SetColorLine("#" & ConvertColor(aCell.BackColor))

        '-> on affecte la bonne taille � la police de la cellule
        aPrinter.SetFontSize(aCell.FontSize)

        '-> on applique le style
        'aPrinter.SetFont("TimesNewRoman", 0.0!, acellStyle)
        aPrinter.SetFont(FontAlias, 0.0!, acellStyle)
        '***********************************
        '* Dessin du contenu de la cellule *
        '***********************************

        '-> R�cup�rer l'option de d'alignement interne
        IsCellAjust = aCell.AutoAjust

        '-> Alignement interne dans le rectangle
        Select Case aCell.CellAlign
            Case 1
                Align = vbPDF.pdfTextAlignment.pdfAlignLeft Or vbPDF.pdfTextAlignment.pdfAlignTop
            Case 2
                Align = vbPDF.pdfTextAlignment.pdfCenter Or vbPDF.pdfTextAlignment.pdfAlignTop
            Case 3
                Align = vbPDF.pdfTextAlignment.pdfAlignRight Or vbPDF.pdfTextAlignment.pdfAlignTop
            Case 4
                Align = vbPDF.pdfTextAlignment.pdfAlignLeft Or vbPDF.pdfTextAlignment.pdfCenter
            Case 5
                Align = vbPDF.pdfTextAlignment.pdfMiddle Or vbPDF.pdfTextAlignment.pdfCenter
            Case 6
                Align = vbPDF.pdfTextAlignment.pdfAlignRight Or vbPDF.pdfTextAlignment.pdfCenter
            Case 7
                Align = vbPDF.pdfTextAlignment.pdfAlignBottom Or vbPDF.pdfTextAlignment.pdfAlignLeft
            Case 8
                Align = vbPDF.pdfTextAlignment.pdfAlignBottom Or vbPDF.pdfTextAlignment.pdfCenter
            Case 9
                Align = vbPDF.pdfTextAlignment.pdfAlignBottom Or vbPDF.pdfTextAlignment.pdfAlignRight
        End Select

        '-> si on doit imprimer le signe
        If PrintSigne Then aCell.ContenuCell = "- " & aCell.ContenuCell

        '-> on dessine la cellule en fonction de l'alignement
        aPrinter.DrawTextBox(x1 / CDbl(37.79524), y1 / CDbl(37.79524), ((aCell.X2 - aCell.X1) / CDbl(37.79524)), (aCell.Y2 - aCell.Y1) / CDbl(37.79524), aCell.ContenuCell, 0.02!, Align, vbPDF.pdfDrawOptions.pdfFilled)

        '**** On dessine maintenant si besoin les bordures de la cellules
        aPrinter.SetLineWidth(0.02)
        '-> Dessin de la bordure Bas
        If aCell.BordureBas Then
            aPrinter.SetColorLine("Black")
            aPrinter.DrawLine((x1) / CDbl(37.79524), (Y2) / CDbl(37.79524), (X2) / CDbl(37.79524), (Y2) / CDbl(37.79524))
        End If

        '-> Dessin de la bordure haut
        If aCell.BordureHaut Then
            aPrinter.SetColorLine("Black")
            aPrinter.DrawLine((x1) / CDbl(37.79524), (y1) / CDbl(37.79524), (X2) / CDbl(37.79524), (y1) / CDbl(37.79524))
        End If

        '-> Dessin de la bordure gauche
        If aCell.BordureGauche Then
            aPrinter.SetColorLine("Black")
            aPrinter.DrawLine((x1) / CDbl(37.79524), (y1) / CDbl(37.79524), (x1) / CDbl(37.79524), (Y2) / CDbl(37.79524))
        End If

        '-> Dessin de la bordure Droite
        If aCell.BordureDroite Then
            aPrinter.SetColorLine("Black")
            aPrinter.DrawLine((X2) / CDbl(37.79524), (y1) / CDbl(37.79524), (X2) / CDbl(37.79524), (Y2) / CDbl(37.79524))
        End If

        ''-> Gestion des angles des bordures
        'If aCell.BordureGauche Or aCell.BordureHaut Then
        '    '-> dessiner le coins sup�rieur gauche
        '    Couleur = 0
        '    aPrinter.DrawLine(x1 - 1, y1 - 1, x1 - 1, y1)
        'End If

        'If aCell.BordureDroite Or aCell.BordureHaut Then
        '    '-> Dessiner le coin sup�rieur droit
        '    Couleur = 0
        '    aPrinter.DrawLine(X2 - 1, y1 - 1, X2 - 1, y1)
        'End If

        'If aCell.BordureBas Or aCell.BordureGauche Then
        '    '-> Dessin du coin inf�rieur gauche
        '    Couleur = 0
        '    aPrinter.DrawLine(x1 - 1, Y2 - 1, x1, Y2 - 1)
        'End If

        'If aCell.BordureDroite Or aCell.BordureBas Then
        '    '-> Dessin du coin inf�rieur droit
        '    Couleur = 0
        '    aPrinter.DrawLine(X2 - 1, Y2 - 1, X2 - 1, Y2)
        'End If

        '-> Renvoyer une valeur de succ�s
        DrawCell = True

        Exit Function

GestErr:

        Call GestError(41, NomTb & "|" & NomBlock & "|" & "L" & aCell.Ligne & "C" & aCell.Colonne & aCell.Contenu, aspool)

    End Function
    Private Function ConvertColor(ByVal Color As Long) As String

        '---> Cette fonction convertit une couleur exprim�e en DEC au format HTML

        Dim Red As Long
        Dim Green As Long
        Dim Blue As Long
        Dim Couleur As String

        '-> formatter la couleur � traduire
        Couleur = FixLen(Hex$(Color), "000000")
        Red = CInt("&H" & Right$(Couleur, 2))
        Green = CInt("&H" & Mid$(Couleur, 3, 2))
        Blue = CInt("&H" & Left$(Couleur, 2))

        '-> Pour internet, inverser les couleurs
        Couleur = Hex$(RevRGB(Red, Green, Blue))

        '-> Remplir avec des 0 si necessaire
        Do While Len(Couleur) <> 6
            Couleur = "0" & Couleur
        Loop

        ConvertColor = Couleur

    End Function
    Private Function RevRGB(ByVal Red As Long, ByVal Green As Long, ByVal Blue As Long) As Long
        RevRGB = CLng(Blue + (Green * 256) + (Red * 65536))
    End Function

    Private Function FixLen(ByVal sIn As String, ByVal sMask As String) As String

        If Len(sIn) < Len(sMask) Then
            FixLen = Left$(sMask, Len(sMask) - Len(sIn)) & sIn
        Else
            FixLen = Right$(sIn, Len(sMask))
        End If

    End Function

    Private Sub DrawBordure(ByVal Move1 As Object, ByRef Move2 As Object, ByRef Line1 As Object, ByRef Line2 As Object, ByRef Couleur As Object)

        Dim hdlBordure As Integer
        Dim aPoint As POINTAPI
        Dim Res As Integer
        Dim Old As Integer
        g = Sortie.CreateGraphics()
        Dim hDC As IntPtr = g.GetHdc
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Couleur. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        hdlBordure = CreatePen(PS_SOLID, 1, Couleur)
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Old = SelectObject(hDC, hdlBordure)
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Move2. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Move1. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Res = MoveToEx(hDC, Move1, Move2, aPoint)
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Line2. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Line1. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        LineTo(hDC, Line1, Line2)
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        SelectObject(hDC, Old)
        DeleteObject(hdlBordure)


    End Sub


    Public Sub AnalyseObj(ByVal Ligne As String, ByRef TypeObjet As String, ByRef NomObjet As String, ByRef TypeSousObjet As String, ByRef NomSousObjet As String, ByRef Param As String, ByRef DataFields As String)

        Dim Param1 As String
        Dim Param2 As String
        Dim Param3 As String
        Dim Param4 As String

        '---> Procedure qui r�cupre les d�finitions des objets dans une ligne
        On Error Resume Next

        If Ligne = "" Then Exit Sub

        Param1 = Entry(1, Ligne, "]")
        Param2 = Entry(2, Ligne, "]")
        'Param3 = Entry(2, Ligne, "{")
        '-> pb sur le caractere { present dans les valeurs
        Param3 = Mid(Ligne, InStr(1, Ligne, "{") + 1)

        Param1 = Mid(Param1, 2, Len(Param1) - 2)
        '-> R�cup�ration du champ de donn�es
        DataFields = Mid(Param3, 1, Len(Param3) - 1)
        '-> R�cup�ration des param�tres d'application
        Param = Mid(Param2, 2, Len(Param2) - 1)
        '-> R�cup�ration du nom des objets
        Param2 = Entry(1, Param1, "(")
        Param3 = Entry(2, Param1, "(")

        '-> Objet de premier niveau
        TypeObjet = Entry(1, Param2, "-")
        NomObjet = Entry(2, Param2, "-")

        '-> Objet de second niveau
        TypeSousObjet = Entry(1, Param3, "-")
        NomSousObjet = Entry(2, Param3, "-")


    End Sub
    Private Function PrintSection(ByVal SectionName As String, ByRef Param As String, ByRef DataFields As String, ByRef FirstObj As Boolean, ByRef PositionX As Integer, ByRef PositionY As Integer, ByRef aspool As Spool) As Boolean

        '---> Cette proc�dure est charg�e d'imprimer une section

        Dim aSection As Section
        Dim i As Short
        Dim NomObjet As String
        Dim aCadre As Cadre
        Dim aBmp As ImageObj
        Dim DebutRangX As Integer
        Dim DebutRangY As Integer

        On Error GoTo GestError



        '-> Pointer sur la section � imprimer
        aSection = aspool.Maquette_Renamed.Sections.Item(UCase(SectionName))

        '-> Imprimer la section
        If Not PrintObjRtf(PositionX, PositionY, aSection, DataFields, DebutRangX, DebutRangY, aspool) Then GoTo GestError

        '-> Impression des objets associ�s dans l'ordre d'affichage
        For i = 1 To aSection.nEntries - 1
            NomObjet = aSection.GetOrdreAffichage(i)
            If UCase(Entry(1, NomObjet, "-")) = "CDR" Then
                '-> Pointer sur le cadre
                aCadre = aSection.Cadres.Item(UCase(Entry(2, NomObjet, "-")))
                '-> Imprimer l'objet
                If Not PrintObjRtf(PositionX, PositionY, aCadre, DataFields, DebutRangX, DebutRangY, aspool, aCadre.MargeInterne) Then GoTo GestError
                '-> Liberer le pointeur
                'UPGRADE_NOTE: L'objet aCadre ne peut pas �tre d�truit tant qu'il n'est pas r�cup�r� par le garbage collector (ramasse-miettes). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                aCadre = Nothing
            ElseIf UCase(Entry(1, NomObjet, "-")) = "BMP" Then
                aBmp = aSection.Bmps.Item(UCase(Entry(2, NomObjet, "-")))
                If Not PrintBmp(aBmp, DebutRangX, DebutRangY, DataFields, aspool) Then GoTo GestError
            End If
        Next

        '-> Positionner le pointeur de position apr�s l'�dition de la section
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        PositionY = Sortie.ScaleY(aSection.Hauteur, 7, 3) + PositionY 'Sortie.ScaleX(DebutRangY, 1, 3)

        '-> Liberer les pointeurs
        'UPGRADE_NOTE: L'objet aSection ne peut pas �tre d�truit tant qu'il n'est pas r�cup�r� par le garbage collector (ramasse-miettes). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        aSection = Nothing

        PrintSection = True

        Exit Function

GestError:

        Call GestError(36, aSection.Nom, aspool)
        PrintSection = False

    End Function

    Private Function PrintBmp(ByRef aBmp As ImageObj, ByRef DebutRangX As Integer, ByRef DebutRangY As Integer, ByVal DataFields As String, ByRef aspool As Spool) As Boolean

        '---> Impression d'un BMP

        Dim PosX As Integer
        Dim PosY As Integer
        Dim aPic As System.Windows.Forms.PictureBox = Nothing
        Dim Champ As String
        Dim NomFichier As String = ""
        Dim aRect As RECT
        Dim hdlPen As Integer
        Dim OldPen As Integer
        Dim hdlBrush As Integer
        Dim oldBrush As Integer
        Dim i As Short
        Dim vbSrcCopy As Integer
        g = aPic.CreateGraphics()
        Dim hDC As IntPtr = g.GetHdc
        g.ReleaseHdc(hDC)
        g.Dispose()

        On Error GoTo GestErr


        '-> Il faut modifier la position X et Y du cadre
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        PosX = Sortie.ScaleX(DebutRangX, 1, 3) + Sortie.ScaleX(aBmp.Left_Renamed, 7, 3)
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        PosY = Sortie.ScaleY(DebutRangY, 1, 3) + Sortie.ScaleY(aBmp.Top, 7, 3)

        '-> Imprimer le bmp
        aPic = frmLib.PicObj(aBmp.IdPic)
        If aBmp.isVariable Then
            '-> Venir charg� le BMP associ�
            '-> R�cup�ration du nombre de champs
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(DataFields, ^). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For i = 2 To NumEntries(DataFields, "^")
                Champ = Entry(i, DataFields, "^")
                If UCase(aBmp.Fichier_Renamed) = "^" & UCase(Mid(Champ, 1, 4)) Then
                    '-> R�cup�ration du nom du fichier
                    NomFichier = RTrim(Mid(Champ, 5, Len(Champ) - 1))
                    '-> Test de l'utilisation de l'association
                    If aBmp.UseAssociation Then
                        '-> R�cup�rer le nom du fichier dans le fichier ini
                        NomFichier = GetPictureAssociation(NomFichier)
                    End If
                    Exit For
                End If
            Next
            '-> V�rifier que le fichier existe
            'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            If Dir(NomFichier) = "" Or Trim(NomFichier) = "" Then
            Else
                If Zoom <> 1 Then
                    aPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
                    'UPGRADE_ISSUE: PictureBox propri�t� aPic.AutoRedraw - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                    'aPic.AutoRedraw = True
                    aPic.Image = System.Drawing.Image.FromFile(NomFichier)
                    'UPGRADE_ISSUE: La constante vbPixels n'a pas �t� mise � niveau. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
                    'UPGRADE_ISSUE: PictureBox propri�t� aPic.ScaleMode - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                    'aPic.ScaleMode = vbPixels
                    If Zoom < 1 Then
                        '-> on redimensionne l'image
                        'UPGRADE_ISSUE: PictureBox propri�t� aPic.hdc - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'

                        'StretchBlt(hDC, 0, 0, VB6.PixelsToTwipsX(aPic.Width)  , VB6.PixelsToTwipsY(aPic.Height)  , aPic.hdc, 0, 0, VB6.PixelsToTwipsX(aPic.Width), VB6.PixelsToTwipsY(aPic.Height), ScrCopy)
                        '-> on ne conserve que la partie interessante
                        'UPGRADE_ISSUE: La constante vbSrcCopy n'a pas �t� mise � niveau. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
                        'UPGRADE_ISSUE: PictureBox propri�t� aPic.hdc - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        StretchBlt(Sortie.hdc, PosX, PosY, VB6.PixelsToTwipsX(aPic.ClientRectangle.Width), VB6.PixelsToTwipsY(aPic.ClientRectangle.Height), hDC, 0, 0, VB6.PixelsToTwipsX(aPic.ClientRectangle.Width), VB6.PixelsToTwipsY(aPic.ClientRectangle.Height), vbSrcCopy)
                    Else
                        '-> on redimensionne l'image
                        aPic.Width = VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(aPic.Width))
                        aPic.Height = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(aPic.Height))
                        'UPGRADE_ISSUE: PictureBox propri�t� aPic.hdc - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                        StretchBlt(hDC, 0, 0, VB6.PixelsToTwipsX(aPic.Width), VB6.PixelsToTwipsY(aPic.Height), hDC, 0, 0, VB6.PixelsToTwipsX(aPic.Width), VB6.PixelsToTwipsY(aPic.Height), ScrCopy)
                        '-> on ne conserve que la partie interessante
                        'UPGRADE_ISSUE: La constante vbSrcCopy n'a pas �t� mise � niveau. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
                        'UPGRADE_ISSUE: PictureBox propri�t� aPic.hdc - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'StretchBlt(Sortie.hdc, PosX, PosY, VB6.PixelsToTwipsX(aPic.ClientRectangle.Width), VB6.PixelsToTwipsY(aPic.ClientRectangle.Height)  , aPic.hdc, 0, 0, VB6.PixelsToTwipsX(aPic.ClientRectangle.Width), VB6.PixelsToTwipsY(aPic.ClientRectangle.Height)  , vbSrcCopy)
                    End If
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.Refresh. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Sortie.Refresh()
                Else
                    aPic.Image = System.Drawing.Image.FromFile(NomFichier)
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.PaintPicture. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Sortie.PaintPicture(aPic.Image, PosX, PosY)
                End If
            End If
        Else
            If Trim(aBmp.Fichier_Renamed) = "" Then
            Else
                'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
                If Dir(aBmp.Fichier_Renamed, FileAttribute.Normal) = "" Then
                Else
                    If Zoom <> 1 Then
                        aPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
                        'UPGRADE_ISSUE: PictureBox propri�t� aPic.AutoRedraw - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                        'aPic.AutoRedraw = True
                        'UPGRADE_ISSUE: La constante vbPixels n'a pas �t� mise � niveau. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
                        'UPGRADE_ISSUE: PictureBox propri�t� aPic.ScaleMode - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                        'aPic.ScaleMode = vbPixels
                        aPic.Image = System.Drawing.Image.FromFile(aBmp.Fichier_Renamed)
                        If Zoom < 1 Then
                            '-> on redimensionne l'image
                            'UPGRADE_ISSUE: PictureBox propri�t� aPic.hdc - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                            'StretchBlt(aPic.hdc, 0, 0, VB6.PixelsToTwipsX(aPic.Width)  , VB6.PixelsToTwipsY(aPic.Height)  , aPic.hdc, 0, 0, VB6.PixelsToTwipsX(aPic.Width), VB6.PixelsToTwipsY(aPic.Height), ScrCopy)
                            '-> on ne conserve que la partie interessante
                            'UPGRADE_ISSUE: La constante vbSrcCopy n'a pas �t� mise � niveau. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
                            'UPGRADE_ISSUE: PictureBox propri�t� aPic.hdc - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            'StretchBlt(Sortie.hdc, PosX, PosY, VB6.PixelsToTwipsX(aPic.ClientRectangle.Width)  , VB6.PixelsToTwipsY(aPic.ClientRectangle.Height)  , aPic.hdc, 0, 0, VB6.PixelsToTwipsX(aPic.ClientRectangle.Width)  , VB6.PixelsToTwipsY(aPic.ClientRectangle.Height)  , vbSrcCopy)
                        Else
                            '-> on redimensionne l'image
                            aPic.Width = VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(aPic.Width))
                            aPic.Height = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(aPic.Height))
                            'UPGRADE_ISSUE: PictureBox propri�t� aPic.hdc - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                            'StretchBlt(aPic.hdc, 0, 0, VB6.PixelsToTwipsX(aPic.Width)  , VB6.PixelsToTwipsY(aPic.Height)  , aPic.hdc, 0, 0, VB6.PixelsToTwipsX(aPic.Width), VB6.PixelsToTwipsY(aPic.Height), ScrCopy)
                            '-> on ne conserve que la partie interessante
                            'UPGRADE_ISSUE: La constante vbSrcCopy n'a pas �t� mise � niveau. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
                            'UPGRADE_ISSUE: PictureBox propri�t� aPic.hdc - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            'StretchBlt(Sortie.hdc, PosX, PosY, VB6.PixelsToTwipsX(aPic.ClientRectangle.Width), VB6.PixelsToTwipsY(aPic.ClientRectangle.Height)  , aPic.hdc, 0, 0, VB6.PixelsToTwipsX(aPic.ClientRectangle.Width), VB6.PixelsToTwipsY(aPic.ClientRectangle.Height)  , vbSrcCopy)
                        End If
                        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.Refresh. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Sortie.Refresh()
                    Else
                        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.PaintPicture. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Sortie.PaintPicture(aPic.Image, PosX, PosY)
                    End If
                End If
            End If
        End If

        '-> Dessiner la bordure si necessaire
        If aBmp.Contour Then
            hdlBrush = CreateSolidBrush(0)
            aRect.Left_Renamed = PosX
            aRect.Top = PosY
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aRect.Right_Renamed = aRect.Left_Renamed + Sortie.ScaleX(VB6.PixelsToTwipsX(aPic.Width), 1, 3)
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aRect.Bottom = aRect.Top + Sortie.ScaleY(VB6.PixelsToTwipsY(aPic.Height), 1, 3)
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            FrameRect(Sortie.hdc, aRect, hdlBrush)
            DeleteObject(hdlBrush)
        End If

        'UPGRADE_NOTE: L'objet aPic ne peut pas �tre d�truit tant qu'il n'est pas r�cup�r� par le garbage collector (ramasse-miettes). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        aPic.Image = Nothing

        '-> Renvoyer une valeur de succ�s
        PrintBmp = True

        Exit Function

GestErr:

        Call GestError(39, aBmp.SectionName & "|" & aBmp.Nom, aspool)
        PrintBmp = False


    End Function

    Private Function PrintObjRtf(ByRef PositionX As Integer, ByRef PositionY As Integer, ByRef aObject As Object, ByVal DataFields As String, ByRef DebutRangX As Integer, ByRef DebutRangY As Integer, ByRef aspool As Spool, Optional ByRef MargeInterne As Single = 0) As Boolean


        '---> Cette fonction imprime un cadre ou une section ( Code RTF + Bordures )

        Dim aField() As String
        Dim aRtf As System.Windows.Forms.RichTextBox
        Dim PosX, PosY As Integer
        Dim i As Short
        Dim Champ As String
        Dim RTFValue As String
        Dim fr As FORMATRANGE
        Dim lTextAmt, lTextOut, Res As Integer
        Dim aPoint As POINTAPI
        Dim aRect As RECT
        Dim hdlPen As Integer
        Dim OldPen As Integer
        Dim hdlBrush As Integer
        Dim oldBrush As Integer
        Dim NbChamp As Short
        Dim IsCadre As Boolean
        Dim IsBordure As Boolean

        On Error GoTo GestErr


        '-> Mettre l'�diteur RTF aux bonnes dimensions
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.IdRTf. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        aRtf = frmLib.Rtf(aObject.IdRTf)

        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Largeur. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_ISSUE: Form m�thode frmLib.ScaleX - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        aRtf.Width = VB6.TwipsToPixelsX(aObject.Largeur)
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Hauteur. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_ISSUE: Form m�thode frmLib.ScaleY - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        aRtf.Height = VB6.TwipsToPixelsY(aObject.Hauteur)

        '-> R�cup�ration du code RTF
        RTFValue = aRtf.Rtf

        'UPGRADE_WARNING: TextRTF a �t� mis � niveau vers Text et a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        If Zoom <> 1 Then aRtf.Text = ZoomRTF((aRtf.Rtf))

        '-> Cr�er une base de champs
        If Trim(DataFields) = "" Then
        Else
            '-> R�cup�ration du nombre de champs
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            NbChamp = NumEntries(DataFields, "^")
            For i = 2 To NbChamp
                Champ = Entry(i, DataFields, "^")
                'UPGRADE_WARNING: La limite inf�rieure du tableau aField est pass�e de 1,0 � 0,0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
                'ReDim Preserve aField(2, i - 1)
                'aField(1, i - 1) = "^" & Mid(Champ, 1, 4)
                'aField(2, i - 1) = RTrim(Mid(Champ, 5, Len(Champ) - 1))
            Next
            '-> Faire un remplacement des champs par leur valeur
            For i = 1 To NbChamp - 1
                'UPGRADE_WARNING: TextRTF a �t� mis � niveau vers Text et a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
                'aRtf.Text = Replace(aRtf.Rtf, aField(1, i), aField(2, i))
            Next
        End If

        '-> Calcul des positions d'impression de l'objet selon le type d'objet � imprimer
        If TypeOf aObject Is Section Then
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.AlignementLeft. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Select Case aObject.AlignementLeft
                Case 2 'Marge gauche
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    PosX = Sortie.ScaleX(aspool.Maquette_Renamed.MargeLeft, 7, 1)
                Case 4 'Centr�
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Largeur. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    PosX = Sortie.ScaleX((aspool.Maquette_Renamed.Largeur - aObject.Largeur) / 2, 7, 1)
                Case 3 'Marge Droite
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Largeur. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    PosX = Sortie.ScaleX(aspool.Maquette_Renamed.Largeur - aObject.Largeur - aspool.Maquette_Renamed.MargeLeft, 7, 1)
                Case 5 'Sp�cifi�
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Left. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    PosX = Sortie.ScaleX(aObject.Left, 7, 1)
            End Select
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.AlignementTop. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Select Case aObject.AlignementTop
                Case 1 'Libre
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    PosY = Sortie.ScaleY(PositionY, 3, 1)
                Case 2 'Marge haut
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    PosY = Sortie.ScaleY(aspool.Maquette_Renamed.MargeTop, 7, 1)
                Case 4 'Centr�
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Hauteur. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    PosY = Sortie.ScaleY((aspool.Maquette_Renamed.Hauteur - aObject.Hauteur) / 2, 7, 1)
                Case 3 'Marge bas
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Hauteur. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    PosY = Sortie.ScaleY(aspool.Maquette_Renamed.Hauteur - aObject.Hauteur - aspool.Maquette_Renamed.MargeTop, 7, 1)
                Case 5 'Sp�cifi�
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Top. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    PosY = Sortie.ScaleY(aObject.Top, 7, 1)
            End Select

            '-> Tenir compte des variations des marges internes
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            PosX = PosX - Sortie.ScaleX(MargeX, 3, 1)
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.AlignementTop. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If aObject.AlignementTop <> 1 Then PosY = PosY - Sortie.ScaleY(MargeY, 3, 1)

            '-> Setting du rect de dessin
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aRect.Left_Renamed = Sortie.ScaleX(PosX, 1, 3)
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aRect.Top = Sortie.ScaleY(PosY, 1, 3)
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Largeur. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aRect.Right_Renamed = Sortie.ScaleX(aObject.Largeur, 7, 3) + Sortie.ScaleX(PosX, 1, 3)
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Hauteur. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aRect.Bottom = Sortie.ScaleY(aObject.Hauteur, 7, 3) + Sortie.ScaleY(PosY, 1, 3)

            '-> Sauvagarder la position de d�but du rang
            DebutRangX = PosX
            DebutRangY = PosY

        ElseIf TypeOf aObject Is Cadre Then
            '-> Il faut modifier la position X et Y du cadre
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Left. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            PosX = DebutRangX + Sortie.ScaleX(aObject.Left, 7, 1)
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Top. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            PosY = DebutRangY + Sortie.ScaleY(aObject.Top, 7, 1)
            '-> Setting du rect de dessin
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aRect.Left_Renamed = Sortie.ScaleX(PosX, 1, 3)
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aRect.Top = Sortie.ScaleY(PosY, 1, 3)
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Largeur. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aRect.Right_Renamed = Sortie.ScaleX(aObject.Largeur, 7, 3) + Sortie.ScaleX(PosX, 1, 3)
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Hauteur. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aRect.Bottom = Sortie.ScaleY(aObject.Hauteur, 7, 3) + Sortie.ScaleY(PosY, 1, 3)
            '-> Indiquer que c'est un cadre
            IsCadre = True
        End If


        '-> Imprimer le fond de l'objet
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.BackColor. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        hdlBrush = CreateSolidBrush(aObject.BackColor)
        hdlPen = CreatePen(PS_NULL, 1, QBColor(15))

        '-> S�lection du pinceau dans le Contexte
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        oldBrush = SelectObject(Sortie.hdc, hdlBrush)
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        OldPen = SelectObject(Sortie.hdc, hdlPen)

        '-> Dessin de l'objet
        If IsCadre Then
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.IsRoundRect. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If Not aObject.IsRoundRect Then
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Res = Rectangle(Sortie.hdc, aRect.Left_Renamed, aRect.Top, aRect.Right_Renamed, aRect.Bottom)
            Else
                '-> Imprimer les bordures
                PrintBordure(aObject, aRect)
                IsBordure = True
            End If

        Else
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Res = Rectangle(Sortie.hdc, aRect.Left_Renamed, aRect.Top, aRect.Right_Renamed, aRect.Bottom)
        End If

        '-> Redessiner le texte de la cellule si on recherche du texte
        If FindTop Then
            If InStr(1, aRtf.Text, FindText, CompareMethod.Text) <> 0 Then
                'on compte le nombre d'occurence dans la chaine
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                i = NumEntries(UCase(aRtf.Text), UCase(FindText)) - 1
                FindPos2 = FindPos2 + i
                '-> on regarde si c'est bien celui que l'on veut dessiner
                If FindPage = aspool.CurrentPage And FindPos < FindPos2 Or FindPage < aspool.CurrentPage Then
                    FindPos2 = FindPos2 - i + 1
                    '-> on pointe sur la premiere occurence du texte recherch�
                    aRtf.SelectionStart = aRtf.Find(FindText)
                    '-> Dans le cas ou on a plusieurs occurences, selectionner le bon texte
                    Do While FindPos2 <= FindPos
                        FindPos2 = FindPos2 + 1
                        'aRtf.SelectionStart = aRtf.Find(FindText, aRtf.SelectionStart + Len(FindText))
                    Loop
                    aRtf.SelectionLength = Len(FindText)
                    '-> On met en evidence le texte recherch�
                    aRtf.SelectionColor = System.Drawing.Color.White
                    Call SetBackColorSel(aRtf.Handle.ToInt32, System.Drawing.Color.Black)
                    FindPos = FindPos2
                    FindPage = aspool.CurrentPage
                    FindTop = False
                    '-> assigner une valeur au scroll
                    FindScrollH = aRect.Left_Renamed
                    FindScrollV = aRect.Top
                End If
            End If
        End If

        '-> S�lectionner les anciens objets et supprimer les objets GDI non utilis�s
        If OldPen <> 0 Then
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            SelectObject(Sortie.hdc, OldPen)
            DeleteObject(hdlPen)
        End If

        If oldBrush <> 0 Then
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            SelectObject(Sortie.hdc, oldBrush)
            DeleteObject(hdlBrush)
        End If

        '-> Initialiser la srtucture Formatrange
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        fr.hDC = Sortie.hdc
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        fr.hdcTarget = Sortie.hdc
        fr.chrg.cpMin = 0
        fr.chrg.cpMax = -1

        '-> Intialisation du rectangle destination
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        fr.rc.Left_Renamed = Sortie.ScaleX(MargeInterne, 7, 1) + PosX
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        fr.rc.Top = Sortie.ScaleX(MargeInterne, 7, 1) + PosY
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Largeur. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        fr.rc.Right_Renamed = Sortie.ScaleX(aObject.Largeur, 7, 1) + PosX - Sortie.ScaleX(MargeInterne, 7, 1)
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Hauteur. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        fr.rc.Bottom = Sortie.ScaleY(aObject.Hauteur * 2, 7, 1) + PosY - Sortie.ScaleY(MargeInterne, 7, 1)

        '-> Initialisation du rectangle de source
        fr.rcPage.Left_Renamed = 0
        fr.rcPage.Top = 0
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Largeur. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        fr.rcPage.Right_Renamed = Sortie.ScaleX(aObject.Largeur, 7, 1)
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Hauteur. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        fr.rcPage.Bottom = Sortie.ScaleY(aObject.Hauteur, 7, 1)

        '-> Faire un setting du mode de restitution
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Res = SetMapMode(Sortie.hdc, MM_TEXT)

        '-> initialisation des variables de pointage de texte
        lTextOut = 0
        lTextAmt = ApiWin32.SendMessage(aRtf.Handle.ToInt32, WM_GETTEXTLENGTH, 0, 0)

        '-> Impression du Rtf
        Do While lTextOut < lTextAmt
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet fr. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            lTextOut = ApiWin32.SendMessage(aRtf.Handle.ToInt32, EM_FORMATRANGE, -1, fr)
            If lTextOut < lTextAmt Then
                fr.chrg.cpMin = lTextOut
                fr.chrg.cpMax = -1
            End If
        Loop

        '-> Lib�rer la ressource associ�e au RTF : VERSION COMPILEE
        'UPGRADE_WARNING: L'utilisation de Null/IsNull() a �t� d�tect�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="2EED02CB-5C0E-4DC1-AE94-4FAA3A30F51A"'
        Res = ApiWin32.SendMessage(aRtf.Handle.ToInt32, EM_FORMATRANGE, -1, 0)
        If Res = 0 Then
            'MsgBox "Erreur dans la lib�ration du context"
            Res = ApiWin32.SendMessage(aRtf.Handle.ToInt32, EM_FORMATRANGE, -1, vbNullString)
            'MsgBox "Apr�s Seconde tentative " & Res
        End If

        '-> VERSION INTERPRETEE
        'Res = SendMessage(aRtf.hwnd, EM_FORMATRANGE, -1, Null)


        '-> Imprimer les bordures
        If Not IsBordure Then PrintBordure(aObject, aRect)

        '-> Redonner son vrai contenu au controle RTF
        'UPGRADE_WARNING: TextRTF a �t� mis � niveau vers Text et a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        aRtf.Text = RTFValue
        aRtf.Refresh()

        '-> Lib�rer le pointeur sur le controle RTF
        'UPGRADE_NOTE: L'objet aRtf ne peut pas �tre d�truit tant qu'il n'est pas r�cup�r� par le garbage collector (ramasse-miettes). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        aRtf = Nothing

        '-> Renvoyer une valeur de suvv�s
        PrintObjRtf = True

        Exit Function

GestErr:

        If Err.Number = 11 Then
            PrintObjRtf = True
            Exit Function
        End If

        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Nom. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Call GestError(38, aObject.Nom, aspool)
        PrintObjRtf = False



    End Function

    Private Function ZoomRTF(ByRef TextRTF As String) As String
        '--> cette fonction effectue un zoom proportionnel sur un rtf
        Dim i As Short
        Dim strTaille As String = ""
        Dim taille As Short
        Dim StrTabulation As String = ""

        i = 1

        '-> on gere ici la taille de la police
        Do While InStr(i, TextRTF, "\fs") <> 0
            '-> on reccupere la taille
            strTaille = Mid(TextRTF, InStr(i, TextRTF, "\fs") + 3, 2)
            If Val(strTaille) <> 0 Then
                taille = CShort(Mid(TextRTF, InStr(i, TextRTF, "\fs") + 3, 2))
                i = InStr(i, TextRTF, "\fs") + 1
                '-> on applique le zoom
                If Zoom < 1 Then
                    taille = taille * 0.9 'empirique mais aide bien
                Else
                    taille = taille
                End If
                '-> on modifie la taille dans le rtf
                TextRTF = Mid(TextRTF, 1, i - 2) & "\fs" & taille & Mid(TextRTF, i + 4)
            Else
                i = i + 1
            End If
        Loop

        '-> on gere ici les tabulations
        For i = 1 To 10
            StrTabulation = StrTabulation & "\tx" & CShort(Val(CStr(710 * i)))
        Next
        TextRTF = Replace(TextRTF, "\pard", "\pard" & StrTabulation)

        '-> on gere ici l'espace interligne avant et apres
        Do While InStr(i, TextRTF, "\sb") <> 0
            '-> on reccupere la taille
            strTaille = Mid(TextRTF, InStr(i, TextRTF, "\sb") + 3, 3)
            If Val(strTaille) <> 0 Then
                taille = CShort(Mid(TextRTF, InStr(i, TextRTF, "\sb") + 3, 3))
                i = InStr(i, TextRTF, "\sb") + 1
                '-> on applique le zoom
                taille = taille
                '-> on modifie la taille dans le rtf
                TextRTF = Mid(TextRTF, 1, i - 2) & "\sb" & taille & Mid(TextRTF, i + 5)
            Else
                i = i + 1
            End If
        Loop

        i = 1
        Do While InStr(i, TextRTF, "\sa") <> 0
            '-> on reccupere la taille
            strTaille = Mid(TextRTF, InStr(i, TextRTF, "\sa") + 3, 3)
            If Val(strTaille) <> 0 Then
                taille = CShort(Mid(TextRTF, InStr(i, TextRTF, "\sa") + 3, 3))
                i = InStr(i, TextRTF, "\sa") + 1
                '-> on applique le zoom
                taille = taille
                '-> on modifie la taille dans le rtf
                TextRTF = Mid(TextRTF, 1, i - 2) & "\sa" & taille & Mid(TextRTF, i + 5)
            Else
                i = i + 1
            End If
        Loop

        ZoomRTF = TextRTF

    End Function

    Private Sub PrintBordure(ByRef aObject As Object, ByRef aRect As RECT)

        Dim Temp1 As Integer
        Dim Temp2 As Integer
        Dim ClipRgn As Integer
        Dim OldClip As Integer
        Dim Pix1X As Integer
        Dim Pix1Y As Integer
        Dim aPoint As POINTAPI

        Dim hdlBrush As Integer
        Dim oldBrush As Integer
        Dim hdlPen As Integer
        Dim OldPen As Integer
        Dim Res As Integer

        On Error Resume Next

        '-> Dessin du contour de l'objet si necessaire
        If TypeOf aObject Is Section Then
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Contour. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If aObject.Contour Then
                hdlBrush = CreateSolidBrush(0)
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oldBrush = SelectObject(Sortie.hdc, hdlBrush)
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                FrameRect(Sortie.hdc, aRect, hdlBrush)
            End If
        ElseIf TypeOf aObject Is Cadre Then


            '-> Tenir compte de la largeur du trait
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.LargeurTrait. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_ISSUE: Form m�thode frmLib.ScaleX - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
            Temp2 = aObject.LargeurTrait
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Temp2 = Sortie.ScaleX(Temp2, 1, 3)

            'UPGRADE_ISSUE: Form m�thode frmLib.ScaleX - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
            Pix1X = 1
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Pix1X = Pix1X

            'UPGRADE_ISSUE: Form m�thode frmLib.ScaleY - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
            Pix1Y = 1
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Pix1Y = Pix1Y

            '-> cr�er un stylo de dessin
            hdlPen = CreatePen(PS_SOLID, Temp2, 0)
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            OldPen = SelectObject(Sortie.hdc, hdlPen)

            '-> Cr�er le fond
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.BackColor. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            hdlBrush = CreateSolidBrush(aObject.BackColor)
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oldBrush = SelectObject(Sortie.hdc, hdlBrush)

            '-> S�lectionner le rectangle comme zone de clipping
            ClipRgn = CreateRectRgn(aRect.Left_Renamed, aRect.Top, aRect.Right_Renamed, aRect.Bottom)
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            SelectClipRgn(Sortie.hdc, ClipRgn)

            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.IsRoundRect. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If aObject.IsRoundRect Then
                If Temp2 / 2 <> Fix(Temp2 / 2) Then Temp2 = Temp2 + 1

                'UPGRADE_ISSUE: Form m�thode frmLib.ScaleX - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                Pix1X = 50
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Pix1X = Sortie.ScaleX(Pix1X, 1, 3)

                'UPGRADE_ISSUE: Form m�thode frmLib.ScaleY - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                Pix1Y = 50
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Pix1Y = Sortie.ScaleY(Pix1Y, 1, 3)
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Res = RoundRect(Sortie.hdc, aRect.Left_Renamed + Temp2 / 2, aRect.Top + Temp2 / 2, aRect.Right_Renamed - Temp2 / 2, aRect.Bottom - Temp2 / 2, Pix1X, Pix1Y)
            Else

                '-> Bordure gauche
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Gauche. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If aObject.Gauche Then
                    '-> Dessin des bordures
                    Temp1 = Fix(Temp2 / 2)
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    MoveToEx(Sortie.hdc, aRect.Left_Renamed + Temp1, aRect.Top, aPoint)
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    LineTo(Sortie.hdc, aRect.Left_Renamed + Temp1, aRect.Bottom - 1)
                End If

                '-> Bordure haut
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Haut. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If aObject.Haut Then
                    Temp1 = Fix(Temp2 / 2)
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    MoveToEx(Sortie.hdc, aRect.Left_Renamed, aRect.Top + Temp1, aPoint)
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    LineTo(Sortie.hdc, aRect.Right_Renamed, aRect.Top + Temp1)
                End If

                '-> Bordure Droite
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Droite. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If aObject.Droite Then
                    If (Temp2 / 2) = Fix(Temp2 / 2) Then
                        Temp1 = Temp2 / 2
                    Else
                        Temp1 = (Temp2 + 1) / 2
                    End If
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    MoveToEx(Sortie.hdc, aRect.Right_Renamed - Temp1, aRect.Top, aPoint)
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    LineTo(Sortie.hdc, aRect.Right_Renamed - Temp1, aRect.Bottom)
                End If

                '-> Bordure Bas
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Bas. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If aObject.Bas Then
                    If (Temp2 / 2) = Fix(Temp2 / 2) Then
                        Temp1 = Temp2 / 2
                    Else
                        Temp1 = (Temp2 + 1) / 2
                    End If
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    MoveToEx(Sortie.hdc, aRect.Left_Renamed, aRect.Bottom - Temp1, aPoint)
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    LineTo(Sortie.hdc, aRect.Right_Renamed, aRect.Bottom - Temp1)
                End If

            End If

            '-> Restituer la zone de clipping
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleHeight. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleWidth. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            OldClip = CreateRectRgn(0, 0, Sortie.ScaleWidth, Sortie.ScaleHeight)
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            SelectClipRgn(Sortie.hdc, OldClip)
            DeleteObject(ClipRgn)
            DeleteObject(OldClip)
        End If

        '-> S�lectionner les anciens objets et supprimer les objets GDI non utilis�s
        If OldPen <> 0 Then
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            SelectObject(Sortie.hdc, OldPen)
            DeleteObject(hdlPen)
        End If

        If oldBrush <> 0 Then
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            SelectObject(Sortie.hdc, oldBrush)
            DeleteObject(hdlBrush)
        End If


    End Sub



    Public Function FormatNumTP(ByVal ToFormat As String, ByVal Msk As String, Optional ByRef NomBlock As String = "", Optional ByRef Cell As String = "") As FormatedCell

        '---> Cette fonction a pour but d'analyser une chaine de caract�re et de la retouner
        'formatt�e en s�parateur de milier et en nombre de d�cimal

        Dim strFormat As String = ""
        Dim strTempo As String = ""
        Dim strPartieEntiere As String = ""
        Dim StrPartieDecimale As String = ""
        Dim i, j As Short
        Dim nbDec As Short
        Dim strSep As String = ""
        Dim idNegatif As Short
        Dim Masque As String = ""
        Dim Tempo As String = ""
        Dim FindSepDec As Boolean

        On Error GoTo GestErr
        FormatNumTP = Nothing

        For i = Len(ToFormat) To 1 Step -1
            If IsNumeric(Mid(ToFormat, i, 1)) Then
                Tempo = Mid(ToFormat, i, 1) & Tempo
            Else
                '-> Gestion du s�parateur d�cimal
                If Mid(ToFormat, i, 1) = "." Or Mid(ToFormat, i, 1) = "," Then
                    If Not FindSepDec Then
                        Tempo = SepDec & Tempo
                        FindSepDec = True
                    End If
                Else
                    Tempo = Mid(ToFormat, i, 1) & Tempo
                End If
            End If
        Next  'Pour tous les caract�res

        ToFormat = Tempo

        If Trim(ToFormat) = "" Then
        Else
            '-> Tester si on envoie une zone num�rique
            If Not IsNumeric(ToFormat) Then
                FormatNumTP.Ok = False
                Exit Function
            End If
        End If

        '-> Charger le masque de la cellule
        nbDec = CShort(Entry(1, Msk, "�"))
        If Entry(2, Msk, "�") <> "" Then
            strSep = SepMil 'Entry(2, Msk, "�")
        Else
            strSep = ""
        End If
        idNegatif = CShort(Entry(3, Msk, "�"))

        FormatNumTP.Ok = True
        FormatNumTP.nbDec = nbDec
        FormatNumTP.value = CDbl(ToFormat)
        FormatNumTP.idNegatif = idNegatif

        '-> Analyse si d�cimale
        If nbDec = 0 Then
            '-> Arrondir
            strTempo = CStr(CDbl(ToFormat))
            Masque = "#########################################0"
        Else
            Masque = "#########################################0." & New String("0", nbDec)
            strTempo = ToFormat
        End If

        '-> Construction d'un masque assez grand pour formatter n'importe qu'elle zone
        strTempo = VB6.Format(System.Math.Abs(CDbl(strTempo)), Masque)

        '-> Construction de la partie enti�re
        If nbDec <> 0 Then
            strPartieEntiere = Mid(strTempo, 1, InStr(1, strTempo, SepDec) - 1)
            StrPartieDecimale = SepDec & Mid(strTempo, InStr(strTempo, SepDec) + 1, nbDec)
        Else
            strPartieEntiere = strTempo
            StrPartieDecimale = ""
        End If

        j = 1
        For i = Len(strPartieEntiere) To 1 Step -1
            strFormat = Mid(strTempo, i, 1) & strFormat
            If j = 3 And i <> 1 Then
                strFormat = strSep & strFormat
                j = 1
            Else
                j = j + 1
            End If
        Next

        FormatNumTP.strFormatted = strFormat & StrPartieDecimale

        Exit Function

GestErr:
        FormatNumTP.Ok = False
    End Function

    Private Function PrintTableau(ByRef NomObjet As String, ByRef Param As String, ByRef DataFields As String, ByRef PositionX As Integer, ByRef PositionY As Integer, ByRef NomSousObjet As String, ByRef FirstObj As Boolean, ByRef aspool As Spool) As Boolean

        Dim aTb As Tableau
        Dim nLig As Short


        On Error GoTo GestError

        '-> Pointer sur le tableau pass� en argument
        aTb = aspool.Maquette_Renamed.Tableaux.Item(UCase(NomObjet))

        '-> R�cup�ration de la ligne de tableau � imprimer
        nLig = CShort(Entry(2, Param, "\"))

        '-> Imprimer le block de ligne
        PositionY = PrintBlock(NomSousObjet, Param, DataFields, PositionX, PositionY, aTb, nLig, aspool)

        '-> Quitter si valeur d'erreur
        If PositionY = -9999 Then GoTo GestError

        '-> Renvoyer une valeur de succ�s
        PrintTableau = True

        Exit Function

GestError:
        Call GestError(37, aTb.Nom, aspool)
        PrintTableau = False


    End Function

    Public Function GetPictureAssociation(ByRef ValueToSearch As String) As String

        Dim Res As Integer
        Dim lpBuffer As String

        On Error GoTo GestError
        GetPictureAssociation = ""

        '-> Ne traiter l'association d'image qu'en version PathV51
        If VersionTurbo = 1 Then Exit Function

        '-> V�rifier que l'on ait bien acc�s au fichier TM_Picture.ini
        If Tm_PictureIniFile = "" Then Exit Function

        lpBuffer = Space(5000)
        Res = GetPrivateProfileString("IMAGES", ValueToSearch, "", lpBuffer, Len(lpBuffer), Tm_PictureIniFile)
        If Res <> 0 Then
            lpBuffer = Mid(lpBuffer, 1, Res)
            '-> Cr�er la liste des mots cl�s
            GetPathMotCles()
            '-> Faire le remplacement
            ReplacePathMotCles(lpBuffer)
            GetPictureAssociation = lpBuffer
        End If

        Exit Function

GestError:
        GetPictureAssociation = ""

    End Function

    Public Sub CloseFichier(ByVal FileToclose As String)
        Dim frmDisplay As Object

        '---> Cette d�charge le fichier sp�cifi�

        Dim aFichier As Fichier
        Dim aspool As Spool


        On Error Resume Next

        '-> Pointer sur le fichier sp�cifi�
        aFichier = Fichiers.Item(UCase(Trim(FileToclose)))

        '-> supprimer toutes les pages actives
        For Each aspool In aFichier.Spools
            If Not aspool.frmDisplay Is Nothing Then aspool.frmDisplay.Close()
        Next aspool

        '-> Supprimer l'objet de la collection des fichiers
        Fichiers.Remove((UCase(Trim(FileToclose))))

        '-> Supprimer les nodes
        'UPGRADE_WARNING: m�thode TreeNaviga.Nodes.Remove de MSComctlLib.Nodes a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
        myMDIMain.TreeNaviga.Nodes.RemoveAt((UCase(Trim(FileToclose))))

    End Sub



    Public Sub DrawWait()

        '---> Proc�dure qui dessine la temporisation

        Dim hdlBrush As Integer
        Dim hdlPen As Integer
        Dim oldBrush As Integer
        Dim OldPen As Integer
        Dim Res As Integer
        Dim hdc As Integer
        Dim StartPanel As Integer

        On Error Resume Next

        '-> Get du Dc du pannel
        hdc = GetDC(myMDIMain.StatusBar1.Handle.ToInt32)

        '-> Cr�ation des objets GDI pour dessin des cellules
        hdlBrush = CreateSolidBrush(&HFF8080)
        hdlPen = CreatePen(PS_NULL, 1, 0)

        '-> S�lection du pinceau dans le Contexte
        oldBrush = SelectObject(hdc, hdlBrush)

        '-> Dessin de la cellule
        OldPen = SelectObject(hdc, hdlPen)

        '-> Depart du dessin
        'UPGRADE_WARNING: La limite inf�rieure de la collection MDIMain.StatusBar1.Panels est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
        'UPGRADE_ISSUE: Form m�thode frmLib.ScaleX - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        StartPanel = VB6.PixelsToTwipsX(myMDIMain.StatusBar1.Items.Item(1).Width) + 4

        '-> Dessin du rectangle
        'UPGRADE_WARNING: La limite inf�rieure de la collection MDIMain.StatusBar1.Panels est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
        'UPGRADE_ISSUE: Form m�thode frmLib.ScaleX - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        Res = Rectangle(hdc, StartPanel, 3, StartPanel + ((TailleLue / TailleTotale) * (VB6.PixelsToTwipsX(myMDIMain.StatusBar1.Items.Item(2).Width) - 2)), 17)

        '-> Liberer les objets GDI
        SelectObject(hdc, oldBrush)
        SelectObject(hdc, OldPen)
        DeleteObject(hdlPen)
        DeleteObject(hdlBrush)
        ReleaseDC(myMDIMain.StatusBar1.Handle.ToInt32, hdc)

    End Sub

    Private Sub LoadInternetMessProg()

        Dim aLb As Libelle
        Dim strLib As String

        '********************
        '* MESSPROG MDIMAIN *
        '********************

        aLb = New Libelle
        strLib = "1=&Fichier" & Chr(0) & "2=&Ouvrir" & Chr(0) & "3=&Imprimer" & Chr(0) & "4=&Quitter" & Chr(0) & "5=F&en�tre" & Chr(0) & "6=&Navigation" & Chr(0) & "7=Mosa�que &horizontale" & Chr(0) & "8=Mosa�que &verticale" & Chr(0) & "9=&En cascade" & Chr(0) & "10=Visionneuse DEAL INFORMATIQUE." & Chr(0) & "11=Erreur sur le fichier." & Chr(0) & "12=Page N� : " & Chr(0) & "13=Fermer le fichier" & Chr(0) & "14=Imprimer" & Chr(0) & "15=Propri�t�s" & Chr(0) & "16=Envoyer Vers" & Chr(0) & "17=Internet" & Chr(0) & "18=Messagerie" & Chr(0) & "19=Editeur de texte" & Chr(0) & "20=&R�organiser les icones" & Chr(0) & "21=A propos de la visionneuse DEAL Informatique" & Chr(0) & "22=Supprimer" & Chr(0) & "23=D�finitivement" & Chr(0) & "24=Envoyer � la poubelle" & Chr(0) & "25=Le fichier existe d�ja : d�sirez-vous l'�craser ?�Confirmation"

        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "MDIMAIN")

        '******************
        '* Ac�s au d�tail *
        '******************

        aLb = New Libelle
        strLib = "1=Impossible de trouver le fichier d�tail sp�cifi�D�tail" & Chr(0) & "2=Cr�ation de l'acc�s au d�tail en cours..." & Chr(0) & "3=Acc�s au d�tail"

        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "ACCESDET")

        '********************
        '* MESSPROG FRMMAIL *
        '********************
        aLb = New Libelle
        strLib = "1=Envoyer un fichier" & Chr(0) & "2=G�n�ral" & Chr(0) & "3=Envoyer la page en cours" & Chr(0) & "4=Envoyer le spool en entier" & Chr(0) & "5=Crypter le fichier" & Chr(0) & "6=Internet" & Chr(0) & "7=OutLook" & Chr(0) & "8=Destinataire" & Chr(0) & "9=Sujet" & Chr(0) & "10=Message" & Chr(0) & "11=Envoyer" & Chr(0) & "12=Police" & Chr(0) & "13=Couleur" & Chr(0) & "14=Aligner � gauche" & Chr(0) & "15=Centrer" & Chr(0) & "16=Aligner � droite" & Chr(0) & "17=Veuillez saisir le destinataire du message�Erreur" & Chr(0) & "18=Envoyer le fichier en entier" & Chr(0) & "19=Options" & Chr(0) & "20=Format du fichier" & Chr(0) & "21=Utiliser le format Turbo" & Chr(0) & "22=Utiliser le format HTML" & Chr(0) & "23=Navigation" & Chr(0) & "24=Cr�er une page de navigation" & Chr(0) & "25=G�n�rer un seul fichier contenant toutes les pages" & Chr(0) & "26=G�n�rer de simples pages" & Chr(0) & "27=Inclure le num�ro du spool dans le nom des pages" & Chr(0) & "28=Int�grer le spool dans le corps du mail" & Chr(0) & "29=Utiliser le format PDF" & Chr(0) & "30=Pas de pi�ce jointe" & Chr(0) & "31=Lotus Notes" & Chr(0) & "32=Copies" & Chr(0) & "33=A" & Chr(0) & "34=Cc" & Chr(0) & "35=Obj"
        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "FRMMAIL")

        '*************************
        '* MESSPROG FRMINTERNET  *
        '*************************

        aLb = New Libelle
        strLib = "1=Exporter au format HTML" & Chr(0) & "2=Export :" & Chr(0) & "3=Exporter la page en cours" & Chr(0) & "4=Exporter le spool en entier" & Chr(0) & "5=Options : " & Chr(0) & "6=Cr�er une page de navigation" & Chr(0) & "7=Lancer � la fin du traitement" & Chr(0) & "8=Traitement : " & Chr(0) & "9=Cr�ation de la page : " & Chr(0) & "10=Exporter" & Chr(0) & "11=Navigation : " & Chr(0) & "12=Utiliser une barre de navigation" & Chr(0) & "13=Utiliser une liste de pages" & Chr(0) & "14=Exporter le fichier en entier" & Chr(0) & "15=Inclure le num�ro du spool dans le nom des pages" & Chr(0) & "16=G�n�rer un seul fichier contenant toutes les pages" & Chr(0) & "17=G�n�rer de simples pages" & Chr(0) & "18=Fichier : " & Chr(0) & "19=R�pertoire d'export" & Chr(0) & "20=Nom du fichier � g�n�rer" & Chr(0) & "21=Nom de fichier incorrect" & Chr(0) & "22=Exporter les bordures"

        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "FRMINTERNET")

        '*************************
        '* MESSPROG FRMVISUSPOOL *
        '*************************

        aLb = New Libelle
        strLib = "1=Page de s�lection" & Chr(0) & "2=Premi�re page" & Chr(0) & "3=Page pr�c�dente" & Chr(0) & "4=Atteindre une page" & Chr(0) & "5=Page suivante" & Chr(0) & "6=Derni�re page" & Chr(0) & "7=Imprimer" & Chr(0) & "8=Envoyer un message" & Chr(0) & "9=Exporter au format HTML" & Chr(0) & "10=Aide" & Chr(0) & "11=Spool : � du fichier : " & Chr(0) & "12=Imprimer le fichier" & Chr(0) & "13=Imprimer le spool" & Chr(0) & "14=Imprimer la page" & Chr(0) & "15=Exporter au format PDF" & Chr(0) & "16=Rechercher" & Chr(0) & "17=Rechercher le suivant" & Chr(0) & "18=Exporter au format OpenOffice" & Chr(0) & "19=Acrobat PdfWriter Distiller, Win2pdf, PdfCreator"
        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "FRMVISUSPOOL")

        '**********************
        '* MESSPROG STATUSBAR *
        '**********************
        aLb = New Libelle
        strLib = "1=Lecture du fichier $FICHIER$ en cours ..." & Chr(0) & "2=Initialisation des maquettes ..." & Chr(0) & "3=Lecture de la page en cours ..."
        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "STATUSBAR")

        '*******************
        '* MESSPROG ERRORS *
        '*******************
        aLb = New Libelle
        strLib = "1=" & Chr(0) & "2=Impossible d'ouvrir le fichier $FICHIER$ pour lecture." & Chr(0) & "3=Erreur lors de la cr�ation de l'objet de la classe Fichier pour le fichier $FICHIER$." & Chr(0) & "4=Impossible de lire le fichier ASCII apr�s ouverture." & Chr(0) & "5=" & Chr(0) & "6=Impossible de trouver un spool dans fichier sp�cifi� $FICHIER$." & Chr(0) & "7=Impossible de trouver la maquette $MAQ$ du fichier $FICHIER$." & Chr(0) & "8=Erreur systeme : Impossible de cr�er un fichier temporaire pour lecture de la maquette $MAQ$." & Chr(0) & "9=Impossible d'ouvrir le fichier temporaire $INI$ de la maquette $MAQ$." & Chr(0) & "10=Erreur lors de la lecture du fichier temporaire $INI$ de la maquette $MAQ$." & Chr(0) & "11=Erreur dans la cr�ation d'un objet de type Section : $SECTION$ de la maquette $MAQ$." & "12=Erreur dansla cr�ation d'un  objet de type Tableau : $TABLEAU$ de la maquette $MAQ$." & Chr(0) & "13=" & Chr(0) & "14=Erreur dans la cr�ation d'un BMP : $BMP$ de la section : $SECTION$ de la maquette $MAQ$." & Chr(0) & "15=Erreur dans la cr�ation d'un Cadre : $CADRE$ de la section : $SECTION$ de la maquette $MAQ$." & Chr(0) & "16=Erreur dans la cr�ation d'un block : $BLOCK$ du tableau $TABLEAU$ de la maquette $MAQ$." & Chr(0) & "17=Impossible de cr�er un fichier temporaire pour un objet RTF." & Chr(0) & "18=Erreur dans l'initialisation de l'entete de la maquette : " & Chr(0) & "19=Erreur dans l'initialisation d'un nouveau conteneur BMP." & Chr(0) & "20=Erreur dans le setting des dimensions d'un objet BMP : $BMP$ de la section : $SECTION$. Hauteur : $HAUTEUR$   Largeur : $LARGEUR$." & Chr(0) & "21=Erreur dans le setting du propath d'une image." & Chr(0) & "22=Erreur lors du chargement du fichier : $FICHIER$ du bmp : $BMP$ de la section : $SECTION$." & Chr(0) & "23=Erreur lors de la cr�ation de la repr�sentation de l'objet cadre $CADRE$ de la section $SECTION$." & Chr(0) & "24=Erreur lors du chargement du fichier temporaire $FICHIER$ du cadre : $CADRE$ de la section : $SECTION$." & Chr(0) & "25=Erreur lors du chargement du fichier temporaire $FICHIER$ de la section : $SECTION$." & Chr(0) & "26=Erreur lors de la cr�ation de la repr�sentation physique de la section : $SECTION$." & Chr(0) & "27=Erreur lors de l'initialisation de la section $SECTION$ pour la valeur de ligne : $LIG$." & Chr(0)


        strLib = strLib & "28=Erreur lors de l'initialisation de l'entete de l'image : $BMP$ de la section $SECTION$ pour la valeur : $LIG$" & Chr(0) & Chr(0) & "29=Erreur lors de l'initialisation de l'entete du cadre : $CADRE$ de la section $SECTION$ pour la valeur : $LIG$" & Chr(0) & "30=Erreur lors de l'initialisation du tableau $TABLEAU$ pour la valeur : $LIG$." & Chr(0) & "31=Erreur lors de l'initialisation du block $BLOCK$ pour le tableau $TABLEAU$ pour la valeur $LIG$." & Chr(0) & "32=Erreur lors de la lecture d'une ligne de d�finition du block $BLOCK$ du tableau $TABLEAU$." & Chr(0) & "33=Erreur lors de la cr�ation des cellule du block $BLOCK$ du tableau $TABLEAU$ pour la valeur $LIG$." & Chr(0) & "34=Erreur lors de l'initialisation des dimensions des cellules du block : $BLOCK$ du tableau $TABLEAU$" & Chr(0) & "35=Erreur dans l'initialisation de la page � imprimer" & Chr(0) & "36=Erreur dans l'initialisation de l'impression de la section : $SECTION$." & Chr(0) & "37=Erreur dans l'initialisation de l'impression du tableau : $TABLEAU$." & Chr(0) & "38=Erreur dans l'impression d'un objet RTF : $OBJECT$." & Chr(0) & "39=Erreur dans l'impression de l'image : $BMP$ de la section : $SECTION$." & Chr(0) & "40=Erreur dans l'initialisation du block : $BLOCK$ du tableau $TABLEAU$" & Chr(0) & "41=Erreur dans le dessin de la cellule : $CELLULE$ du block : $BLOCK$ du tableau $TABLEAU$." & Chr(0) & "42=Erreur durant l'impression du fichier.�Erreur fatale d'impression"
        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "ERRORS")

        '********************
        '* MESSPROG MESSAGE *
        '********************
        aLb = New Libelle

        strLib = "1=Quitter maintenant ?�Confirmation" & Chr(0) & "2=Fichier d�ja charg�.�Impossible de charger le fichier." & Chr(0) & "3=Num�ro de page incorrect.�Erreur" & Chr(0) & "4=Impossible de se connecter � Ms OutLook.�Impossible d'envoyer un message." & Chr(0) & "5=Impossible de cr�er un nouveau message dans Ms OutLook.�Impossible de cr�er un message." & Chr(0) & "6=Impossible de joinde le fichier sp�cifi� : $FICHIER$.�Erreur." & Chr(0) & "7=Envoyer la page en cours [Oui] ou le spool en entier [Non] ?�Question" & Chr(0) & "8=Veuillez saisir le destinataire du message.�Erreur" & Chr(0) & "9=Impossible d'envoyer le message sur Internet.�Erreur" & Chr(0) & "10=Impossible d'initialiser la session de messagerie.�Erreur" & Chr(0) & "11=Crypter le fichier ?�Question" & Chr(0) & "12=Saisie incorrecte.�Erreur"
        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "MESSAGE")

        '********************
        '* MESSPROG BOUTONS *
        '********************
        aLb = New Libelle

        strLib = "1=&Ok" & Chr(0) & "2=&Annuler" & Chr(0) & "3=&Envoyer"
        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "BOUTONS")

        '***************
        '* FRMNETSEND  *
        '***************
        aLb = New Libelle
        strLib = "1=Envoyer un e-mail" & Chr(0) & "2=Url : " & Chr(0) & "3=Message : " & Chr(0) & "4=Objet : " & Chr(0) & "5=Carnet d'adresses" & Chr(0) & "6=Crypter le fichier associ� :"
        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "FRMNETSEND")

        '***************
        '* FRMEDITOR  *
        '***************
        aLb = New Libelle
        strLib = "1=&Fichier" & Chr(0) & "2=&Ouvrir" & Chr(0) & "3=&Enregistrer" & Chr(0) & "4=En&registrer sous" & Chr(0) & "5=&Imprimer" & Chr(0) & "6=&Quitter" & Chr(0) & "7=&Edition" & Chr(0) & "8=Couper" & Chr(0) & "9=Copier" & Chr(0) & "10=Coller" & Chr(0) & "11=Rechercher" & Chr(0) & "12=A propos de ..." & Chr(0) & "13=Chercher : " & Chr(0) & "14=Forma&t" & Chr(0) & "15=&Police" & Chr(0) & "16=Envoie du fichier : " & Chr(0) & "17=Envoie du spool :" & Chr(0) & "18=Envoie de la page : "

        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "FRMEDITOR")

        '***************
        '* FRMFONT     *
        '***************
        aLb = New Libelle
        strLib = "1=Choix d'une police" & Chr(0) & "2=Police :" & Chr(0) & "3=Style de police :" & Chr(0) & "4=Taille : " & Chr(0) & "5=Normal" & Chr(0) & "6=Italique" & Chr(0) & "7=Gras" & Chr(0) & "8=Gras Italique"

        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "FRMFONT")

        '**************************
        '* MESSPROG FRMSEARCHPAGE *
        '**************************
        aLb = New Libelle

        strLib = "1=Atteindre une page" & Chr(0) & "Num�ro de la page :"
        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "FRMSEARCHPAGE")

        '**************************
        '* MESSPROG FRMSEARCHTEXT *
        '**************************
        aLb = New Libelle

        strLib = "1=Rechercher" & Chr(0) & "2=Rechercher :" & Chr(0) & "3=La cha�ne recherch�e n'a pas �t� trouv�e�Erreur"
        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "FRMSEARCHTEXT")

        '*************************
        '* MESSPROG D'IMPRESSION *
        '*************************
        aLb = New Libelle
        strLib = "1=S�lection d'une imprimante" & Chr(0) & "2=Nom : " & Chr(0) & "3=Entendue de l'impression : " & Chr(0) & "4=Imprimer le fichier" & Chr(0) & "5=Pages" & Chr(0) & "6=de :" & Chr(0) & "7=a :" & Chr(0) & "8=Page en cours" & Chr(0) & "9=Copies" & Chr(0) & "10=" & Chr(0) & "11=&OK" & Chr(0) & "12=&Annuler" & Chr(0) & "13=Veuillez s�lectionner une imprimante�Erreur" & Chr(0) & "14=Veuillez indiquer le nombre de copies�Erreur" & Chr(0) & "15=Veuillez indiquer la page mini � imprimer�Erreur" & Chr(0) & "16=Veuillez indiquer la page maxi � imprimer�Erreur" & Chr(0) & "17=Visualisation �cran" & Chr(0) & "18=Propri�t�s" & Chr(0) & "19=Imprimer en Recto-Verso" & Chr(0) & "20=Spool en cours" & Chr(0) & "21=Mise en page"
        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "FRMPRINTVISU")

        '*********************
        '* EXPORT VERS EXCEL *
        '*********************
        aLb = New Libelle

        strLib = "1=Export vers Excel" & Chr(0) & "2=Etendue de l'export:" & Chr(0) & "3=Gestion des feuilles" & Chr(0) & "4=Cr�er une feuille � chaque page" & Chr(0) & "5=Regrouper toutes les pages sur une seule feuille" & Chr(0) & "6=Attention : la dur�e du traitement de l'export vers MircoSoft Excel est largement conditionn�e par les options de mise en forme des cellules." & Chr(0) & "7=Maquette" & Chr(0) & "8=Propri�t�s d'export : " & Chr(0) & "9=Tableau :" & Chr(0) & "10=Block :" & Chr(0) & "11=Ligne :" & Chr(0) & "12=N'exporter qu'une seule fois" & Chr(0) & "13=R�f�rence" & Chr(0) & "14=Utiliser un classeur de r�f�rence" & Chr(0) & "15=Nommer la feuille" & Chr(0) & "16=Nommer la plage de cellules" & Chr(0) & "17=Sp�cifier la cellule de d�part" & Chr(0) & "18=Traitement" & Chr(0) & "19=Traitement en cours" & Chr(0) & "20=Exporter le format" & Chr(0) & "21=Valeur de propri�t� incorrecte" & Chr(0) & "22=Erreur dans le param�trage des pages � imprimer" & Chr(0) & "23=Erreur" & Chr(0) & "24=Veuillez saisir le classeur de r�f�rence" & Chr(0) & "25=Impossible de trouver le fichier" & Chr(0) & "26=Veuillez saisir le nom de l'onglet de classeur" & Chr(0) & "27=Veuillez saisir le nom de la plage de donn�es" & Chr(0) & "28=Veuillez saisir la cellule de d�part" & Chr(0) & "29=Il est obligatoire de donner un nom � la feuille lorsque l'on exporte toutes les pages dans une seule feuille." & Chr(0) & "30=Classeur Excel" & Chr(0) & "31=Mod�le de classeur" & Chr(0) & "32=Erreur dans l'ouverture du classeur de r�f�rence" & Chr(0) & "33=Nom de fichier incorrect" & Chr(0) & "34=Visualisation �cran" & Chr(0) & "35=G�n�ral" & Chr(0) & "36=Spool en entier" & Chr(0) & "37=Descriptifs des �l�m�nts � exporter" & Chr(0) & "38=Page" & Chr(0) & "39=Export : " & Chr(0) & "40=Formatage : " & Chr(0) & "41=Donn�es sans format"
        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "FRMEXCEL")

        '*********************
        '* EXPORT VERS OPENOFFICE *
        '*********************
        aLb = New Libelle

        strLib = "1=Export vers OpenOffice" & Chr(0) & "2=Etendue de l'export:" & Chr(0) & "3=Gestion des feuilles" & Chr(0) & "4=Cr�er une feuille � chaque page" & Chr(0) & "5=Regrouper toutes les pages sur une seule feuille" & Chr(0) & "6=Attention : la dur�e du traitement de l'export vers OpenOffice est largement conditionn�e par les options de mise en forme des cellules." & Chr(0) & "7=Maquette" & Chr(0) & "8=Propri�t�s d'export : " & Chr(0) & "9=Tableau :" & Chr(0) & "10=Block :" & Chr(0) & "11=Ligne :" & Chr(0) & "12=N'exporter qu'une seule fois" & Chr(0) & "13=R�f�rence" & Chr(0) & "14=Utiliser un classeur de r�f�rence" & Chr(0) & "15=Nommer la feuille" & Chr(0) & "16=Nommer la plage de cellules" & Chr(0) & "17=Sp�cifier la cellule de d�part" & Chr(0) & "18=Traitement" & Chr(0) & "19=Traitement en cours" & Chr(0) & "20=Exporter le format" & Chr(0) & "21=Valeur de propri�t� incorrecte" & Chr(0) & "22=Erreur dans le param�trage des pages � imprimer" & Chr(0) & "23=Erreur" & Chr(0) & "24=Veuillez saisir le classeur de r�f�rence" & Chr(0) & "25=Impossible de trouver le fichier" & Chr(0) & "26=Veuillez saisir le nom de l'onglet de classeur" & Chr(0) & "27=Veuillez saisir le nom de la plage de donn�es" & Chr(0) & "28=Veuillez saisir la cellule de d�part" & Chr(0) & "29=Il est obligatoire de donner un nom � la feuille lorsque l'on exporte toutes les pages dans une seule feuille." & Chr(0) & "30=Classeur OpenOffice" & Chr(0) & "31=Mod�le de classeur" & Chr(0) & "32=Erreur dans l'ouverture du classeur de r�f�rence" & Chr(0) & "33=Nom de fichier incorrect" & Chr(0) & "34=Visualisation �cran" & Chr(0) & "35=G�n�ral" & Chr(0) & "36=Spool en entier" & Chr(0) & "37=Descriptifs des �l�m�nts � exporter" & Chr(0) & "38=Page" & Chr(0) & "39=Export : " & Chr(0) & "40=Formatage : "
        '-> Setting des messprog
        aLb.SetKeys(strLib)
        '-> Ajout dans la collection
        Libelles.Add(aLb, "FRMOPENOFFICE")

    End Sub


    Public Function IsOutLook() As Boolean

        '---> Cette proc�dure V�rifier si outlook est install�

        Dim aObj As Object

        On Error GoTo GestError

        aObj = CreateObject("Outlook.Application")

        'UPGRADE_NOTE: L'objet aObj ne peut pas �tre d�truit tant qu'il n'est pas r�cup�r� par le garbage collector (ramasse-miettes). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        aObj = Nothing
        IsOutLook = True


        Exit Function

GestError:
        IsOutLook = False

    End Function

    Public Function IsLotus() As Boolean

        '---> Cette proc�dure V�rifier si lotus est install�
        Dim aObj As Object

        On Error GoTo GestError

        '-> dans le cas ou lotus n'est pas actif verifier si on doit passer le mot de passe
        If IsLotus = False Then
            '    If GetIniString("LOTUS", "PWD", TurboGraphIniFile, False) <> "" Then
            '        '-> on ouvre la session en passant le mot de passe
            '        Set aObj = CreateObject("Notes.NotesSession")
            '        aObj.Initialize (GetIniString("LOTUS", "PWD", TurboGraphIniFile, False))
            '        IsLotus = True
            '    Else
            aObj = CreateObject("Notes.NotesSession")
            'UPGRADE_NOTE: L'objet aObj ne peut pas �tre d�truit tant qu'il n'est pas r�cup�r� par le garbage collector (ramasse-miettes). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            aObj = Nothing
            IsLotus = True
            '    End If
        End If


        Exit Function

GestError:
        IsLotus = False

    End Function

    Public Function InitMAPISession() As Boolean

    End Function

    Public Function CreateDetail(ByRef aspool As Spool, ByVal Page As Short, Optional ByRef PageMin As Short = 0, Optional ByRef PageMax As Short = 0) As String

        '---> Cette fonction cr�er un fichier tempo et retourne le nom du fichier � linker

        Dim TempFile As String
        Dim hdlFile As Short
        Dim DefMaq As String
        Dim i As Short
        Dim PageMini As Short
        Dim PageMaxi As Short

        On Error GoTo GestError

        '-> Obtenir un nom de fichier tempotaire
        TempFile = Year(Now) & "-" & VB6.Format(Month(Now), "00") & "-" & VB6.Format(VB.Day(Now), "00") & "-" & VB6.Format(Hour(Now), "00") & "-" & VB6.Format(Minute(Now), "00") & "-" & VB6.Format(Second(Now), "00") & ".turbo"
        TempFile = GetTempFileNameVB("WWW", True) & TempFile

        '-> Obtenir un handle de fichier
        hdlFile = FreeFile()

        '-> Ouverture du fichier ascii et �criture des pages
        FileOpen(hdlFile, TempFile, OpenMode.Output)

        '-> Ecriture de la balise Spool
        If IsCryptedFile Then
            PrintLine(hdlFile, Crypt("[SPOOL]"))
        Else
            PrintLine(hdlFile, "[SPOOL]")
        End If

        '-> Il faut dans un premier temps �crire la maquette
        If IsCryptedFile Then
            PrintLine(hdlFile, Crypt("[MAQ]"))
        Else
            PrintLine(hdlFile, "[MAQ]")
        End If

        '-> R�cup�ration de la maquette
        DefMaq = aspool.GetMaq

        '-> Impression de la maquette
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(DefMaq, Chr(0)). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        For i = 1 To NumEntries(DefMaq, Chr(0))
            If IsCryptedFile Then
                PrintLine(hdlFile, Crypt(Entry(i, DefMaq, Chr(0))))
            Else
                PrintLine(hdlFile, Entry(i, DefMaq, Chr(0)))
            End If
        Next  'Pour toutes les lignes de la maquette

        '-> Tag de fin de maquette
        If IsCryptedFile Then
            PrintLine(hdlFile, Crypt("[/MAQ]"))
        Else
            PrintLine(hdlFile, "[/MAQ]")
        End If

        '-> Impression du spool en entier
        If Page = -1 Or Page = -2 Then
            If Page = -1 Then
                '-> Indiquer la premi�re page � imprimer
                If aspool.IsSelectionPage = True Then
                    '-> indiquer qu'il faut impimer � partir de la page 0
                    PageMini = 0
                Else
                    '-> Imprimer � partir de la page 1
                    PageMini = 1
                End If

                '-> Indiquer la page maxi
                PageMaxi = aspool.NbPage

            Else
                '-> Indiquer la premi�re page � imprimer
                PageMini = PageMin
                PageMaxi = PageMax

            End If
            '-> On imprimer toutes les pages
            For i = PageMini To PageMaxi
                '-> Imprimer la tag d'ouverture de la page de s�lection
                If i = 0 Then
                    If IsCryptedFile Then
                        PrintLine(hdlFile, Crypt("[GARDEOPEN]"))
                    Else
                        PrintLine(hdlFile, "[GARDEOPEN]")
                    End If
                End If

                '-> Impression de la page
                PrintPageToSpool(aspool, hdlFile, i)

                '-> Imprimer le tag de fin de s�lection si page = 0
                If i = 0 Then
                    If IsCryptedFile Then
                        PrintLine(hdlFile, Crypt("[GARDECLOSE]"))
                    Else
                        PrintLine(hdlFile, "[GARDECLOSE]")
                    End If
                    '-> Imprimer un saut de page si pas derni�re page
                ElseIf i <> aspool.NbPage Then
                    If IsCryptedFile Then
                        PrintLine(hdlFile, Crypt("[PAGE]"))
                    Else
                        PrintLine(hdlFile, "[PAGE]")
                    End If
                End If
            Next
        Else
            '-> Imprimer la tag de la page de s�lection
            If Page = 0 Then
                If IsCryptedFile Then
                    PrintLine(hdlFile, Crypt("[GARDEOPEN]"))
                Else
                    PrintLine(hdlFile, "[GARDEOPEN]")
                End If
            End If

            '-> Imprimer que la page d�sir�e
            PrintPageToSpool(aspool, hdlFile, Page)

            '-> Imprimer la tag de fermture de la page de s�lection
            If Page = 0 Then
                If IsCryptedFile Then
                    PrintLine(hdlFile, Crypt("[GARDECLOSE]"))
                Else
                    PrintLine(hdlFile, "[GARDECLOSE]")
                End If
            End If

        End If

        '-> Tag de fin de spool
        If IsCryptedFile Then
            PrintLine(hdlFile, Crypt("[/SPOOL]"))
        Else
            PrintLine(hdlFile, "[/SPOOL]")
        End If

        '-> Fermer le fichier ouvert
        FileClose(hdlFile)

        '-> Renvoyer le nom du fichier
        CreateDetail = TempFile

        Exit Function

GestError:

        '-> Renovyer une valeur ""
        CreateDetail = ""

    End Function

    Private Sub PrintPageToSpool(ByRef aspool As Spool, ByVal hdlFile As Short, ByVal PageToPrint As Short)

        '---> Cette fonction imprime le contenu d'une page d'un spool

        Dim i As Short

        Dim DefPage As String

        '-> Recup�ration de la d�finition de la page
        DefPage = aspool.GetPage(PageToPrint)

        '-> Impression de la page
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(DefPage, Chr(0)). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        For i = 1 To NumEntries(DefPage, Chr(0))
            If IsCryptedFile Then
                PrintLine(hdlFile, Crypt(Entry(i, DefPage, Chr(0))))
            Else
                PrintLine(hdlFile, Entry(i, DefPage, Chr(0)))
            End If
        Next

    End Sub

    Public Sub DeleteFile(ByRef FileToDelete As String, ByRef ActionFlag As Integer)

        '---> Cette proc�dure supprime un fichier

        Dim SHFileOp As SHFILEOPSTRUCT = Nothing
        Dim Res As Integer

        '-> Ajouter un caractere de fin de string
        FileToDelete = FileToDelete & Chr(0)

        '-> Setting de la structure
        With SHFileOp
            .wFunc = FO_DELETE
            .pFrom = FileToDelete
            .fFlags = ActionFlag
        End With

        '-> suppression du fichier
        Res = SHFileOperation(SHFileOp)

        '-> Si on n' a pas fait annuler : virer le fichier de l'interface
        If SHFileOp.fAborted = False Then CloseFichier(MDIMain.TreeNaviga.SelectedNode.Name)

    End Sub

    Public Sub DrawTempo(ByRef aPic As System.Windows.Forms.PictureBox)

        'UPGRADE_ISSUE: PictureBox m�thode aPic.Line - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        Dim myPen As Pen = New Pen(Color.Black, 3)

        'aPic.CreateGraphics.DrawLine(myPen, 0, 0, CDbl((TailleLue / TailleTotale) * aPic.ClientRectangle.Width), aPic.ClientRectangle.Height)

    End Sub

    Public Function IsTextInPage(ByVal aspool As Spool, ByVal i As Short) As Boolean
        '--> on regarde si le texte recherch� est present sur la page
        Dim strPage As String
        Dim strTemp As String

        On Error GoTo GestError

        '-> on reccupere la page
        strPage = aspool.GetPage(i)

        '-> premiere verification sans tenir compte des caracteres parasites
        If InStr(1, strPage & aspool.GetMaq, Replace(FindText, ",", "."), CompareMethod.Text) <> 0 Then IsTextInPage = True
        If InStr(1, strPage & aspool.GetMaq, Replace(FindText, ".", ","), CompareMethod.Text) <> 0 Then IsTextInPage = True
        If InStr(1, strPage & aspool.GetMaq, FindText, CompareMethod.Text) <> 0 Then IsTextInPage = True
        If InStr(1, Replace(strPage, ",", ""), FindText, CompareMethod.Text) <> 0 Then IsTextInPage = True

GestError:

    End Function

    Private Function GetBackColorSel(ByVal RichHwnd As Integer) As System.Drawing.Color
        'UPGRADE_WARNING: Il peut �tre n�cessaire d'initialiser les tableaux de la structure iniformat avant de les utiliser. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim iniformat As FORMAT_Renamed = Nothing

        ' Set BackColor a masqu�
        iniformat.dwMask = CFM_BACKCOLOR
        iniformat.cbSize = Len(iniformat)
        ' Obtenez la structure du format des caract�res s�lectionner
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet iniformat. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'ApiWin32.SendMessage(RichHwnd, EM_GETCHARFORMAT, SCF_SELECTION, iniformat)

        ' Retour le BackColour de la s�lection
        GetBackColorSel = System.Drawing.ColorTranslator.FromOle(iniformat.crBackColor)
    End Function

    Private Function SetBackColorSel(ByVal RichHwnd As Integer, ByVal NouveauFontBackColorSel As System.Drawing.Color) As Object
        'UPGRADE_WARNING: Il peut �tre n�cessaire d'initialiser les tableaux de la structure iniformat avant de les utiliser. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
        Dim iniformat As FORMAT_Renamed = Nothing
        SetBackColorSel = Nothing
        ' Set BackColor a masqu�
        iniformat.dwMask = CFM_BACKCOLOR
        ' Si le nouveau backcolour est mis � -1 alors nous avons mis le
        ' Backcolour RichTextbox a zero (vbwhite)
        If System.Drawing.ColorTranslator.ToOle(NouveauFontBackColorSel) = -1 Then
            iniformat.dwEffects = CFE_AUTOBACKCOLOR
            iniformat.crBackColor = -1
        Else
            ' donner la nouvelle couleur � BackColour
            iniformat.crBackColor = ChangerColor(NouveauFontBackColorSel)
        End If
        ' Nous avons besoin de passer la dimension de la structure comme un
        ' partie de la structure.
        iniformat.cbSize = Len(iniformat)
        ' Envoyez le message et le nouveau format de caract�re au RichTextbox
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet iniformat. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'ApiWin32.SendMessage(RichHwnd, EM_SETCHARFORMAT, SCF_SELECTION, iniformat)
    End Function

    Private Function ChangerColor(ByVal Couleur As System.Drawing.Color, Optional ByRef Palette As Integer = 0) As Integer
        If OleTranslateColor(System.Drawing.ColorTranslator.ToOle(Couleur), Palette, ChangerColor) Then
            ChangerColor = -1
        End If
    End Function

    Public Function GetTitrePage(ByRef aspool As Spool, ByRef NumPage As Short) As String
        '--> Cette fonction r�cup�re le titre d'une page
        Dim ParamTitre As String
        Dim Ligne As String
        Dim Rg As String
        Dim Dz As String
        Dim i As Short
        Dim j As Short

        GetTitrePage = ""
        '-> on regarde si on a specifi� un titre
        If InStr(1, aspool.GetMaq, "[TITRE]") = 0 Then Exit Function

        '-> on r�cup�re le param�trage
        ParamTitre = Entry(2, aspool.GetMaq, "[TITRE]")
        ParamTitre = Entry(1, ParamTitre, "\TITRE�End")
        ParamTitre = Mid(ParamTitre, 8)

        '-> on parcours les titres � r�cuperer
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(ParamTitre, \). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        For i = 2 To NumEntries(ParamTitre, "\")
            Rg = Entry(i, ParamTitre, "\")
            Dz = "^" & Entry(1, Entry(2, Rg, "�"), Chr(0))
            Rg = "[" & Entry(1, Rg, "�") & "]"
            '-> on recherche la ligne correspondante
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(aspool.GetPage(NumPage), Chr(0)). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For j = 1 To NumEntries(aspool.GetPage(NumPage), Chr(0))
                Ligne = Trim(Entry(j, aspool.GetPage(NumPage), Chr(0)))
                If InStr(1, UCase(Ligne), UCase(Rg)) Then
                    '-> on regarde si on trouve le diez
                    If InStr(1, UCase(Ligne), UCase(Rg)) <> 0 And InStr(1, Ligne, Dz) Then
                        '-> on est sur une bonne ligne on r�cup�re la valeur du diez
                        Dz = RTrim(Mid(Entry(1, Entry(1, Entry(2, Ligne, Dz), "^"), "}"), 5))
                        If InStr(1, Dz, "[\") <> 0 Then Dz = ""
                        If GetTitrePage = "" Then
                            GetTitrePage = Dz
                        Else
                            GetTitrePage = GetTitrePage & " - " & Dz
                        End If
                        '-> sortir de la boucle
                        Exit For
                    End If
                End If
            Next
        Next

    End Function

    Public Function getOSVer() As WindowsVersion
        Dim osv As OSVERSIONINFOEX
        osv.dwOSVersionInfoSize = Marshal.SizeOf(osv)
        If GetVersionEx(osv) = 1 Then
            Select Case osv.dwPlatformId
                Case Is = VER_PLATFORM_WIN32s 'windows 3.x
                    Return WindowsVersion.Obsolete_OS
                Case Is = VER_PLATFORM_WIN32_WINDOWS
                    Select Case osv.dwMinorVersion
                        Case Is = 0 'win 95
                            Return WindowsVersion.Obsolete_OS
                        Case Is = 10
                            If InStr(UCase(osv.szCSDVersion), "A") > 0 Then
                                Return WindowsVersion.Windows_98_SE
                            Else
                                Return WindowsVersion.Windows_98
                            End If
                        Case Is = 90
                            Return WindowsVersion.Windows_Me
                    End Select
                Case Is = VER_PLATFORM_WIN32_NT
                    Select Case osv.dwMajorVersion
                        Case Is = 3 'win nt 3.x
                            Return WindowsVersion.Obsolete_OS
                        Case Is = 4
                            If osv.wProductType = VER_NT_WORKSTATION Then
                                Return WindowsVersion.Windows_NT4_Workstation
                            Else
                                Return WindowsVersion.Windows_NT4_Server
                            End If
                        Case Is = 5
                            Select Case osv.dwMinorVersion
                                Case Is = 0 'win 2000
                                    Select Case osv.wProductType
                                        Case Is = VER_NT_WORKSTATION
                                            Return WindowsVersion.Windows_2000_Pro
                                        Case Is = VER_NT_SERVER
                                            Return WindowsVersion.Windows_2000_Server
                                    End Select
                                Case Is = 1 'win XP
                                    If osv.wSuiteMask = VER_SUITE_PERSONAL Or osv.wSuiteMask = VER_SUITE_PERSONAL + VER_SUITE_SINGLEUSERTS Then
                                        Return WindowsVersion.Windows_XP_HomeEdition
                                    Else
                                        Return WindowsVersion.Windows_XP_Pro
                                    End If
                                Case Is = 2 '.Net server
                                    Return WindowsVersion.Windows_Net_Server
                            End Select
                    End Select
            End Select
        End If
    End Function
    Public Function RegValue(ByVal Hive As RegistryHive, ByVal Key As String, ByVal ValueName As String, Optional ByRef ErrInfo As String = "") As String
        Dim objParent As RegistryKey
        Dim objSubkey As RegistryKey
        Dim sAns As String
        Select Case Hive
            Case RegistryHive.ClassesRoot
                objParent = Registry.ClassesRoot
            Case RegistryHive.CurrentConfig
                objParent = Registry.CurrentConfig
            Case RegistryHive.CurrentUser
                objParent = Registry.CurrentUser
            Case RegistryHive.DynData
                objParent = Registry.DynData
            Case RegistryHive.LocalMachine
                objParent = Registry.LocalMachine
            Case RegistryHive.PerformanceData
                objParent = Registry.PerformanceData
            Case RegistryHive.Users
                objParent = Registry.Users

        End Select

        Try
            objSubkey = objParent.OpenSubKey(Key)
            'if can't be found, object is not initialized
            If Not objSubkey Is Nothing Then
                sAns = (objSubkey.GetValue(ValueName))
            End If

        Catch ex As Exception

            ErrInfo = ex.Message
        Finally

            'if no error but value is empty, populate errinfo
            If ErrInfo = "" And sAns = "" Then
                ErrInfo = _
                   "No value found for requested registry key"
            End If
        End Try
        Return sAns
    End Function

    Public Function GetFontFileName(ByVal FontName As String) As String
        '-> Cette fonction permet de pointer sur le fichier de la font
        Dim sAns As String
        Dim sErr As String = ""
        Dim sPath As String
        Dim mykey As String
        Dim mykey2 As String

        If (getOSVer() > 4) Then
            mykey = "SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts"
            mykey2 = "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders"
        Else
            mykey = "SOFTWARE\Microsoft\Windows\CurrentVersion\Fonts"
            mykey2 = "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\shell Folders"
        End If
        sPath = RegValue(Microsoft.Win32.RegistryHive.CurrentUser, mykey2, "Fonts", sErr) & "\"
        sAns = RegValue(Microsoft.Win32.RegistryHive.LocalMachine, mykey, FontName, sErr)

        If sAns <> "" Then
            GetFontFileName = sPath & sAns
        Else
            sAns = RegValue(Microsoft.Win32.RegistryHive.LocalMachine, mykey, FontName & " (TrueType)", sErr)
            If sAns <> "" Then
                GetFontFileName = sPath & sAns
            Else
                sAns = RegValue(Microsoft.Win32.RegistryHive.LocalMachine, mykey, FontName & " (All res)", sErr)
                If sAns <> "" Then
                    GetFontFileName = sPath & sAns
                Else
                    GetFontFileName = ""
                End If
            End If
        End If

    End Function

End Module
