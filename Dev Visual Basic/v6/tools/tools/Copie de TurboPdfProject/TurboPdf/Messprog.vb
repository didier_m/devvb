Option Strict Off
Option Explicit On
Module Messprog
	Public Libelles As New Collection
	
	'***************************************************************
	'*                                                             *
	'* Ce module contient les variables utils�es pour l'affichage  *
	'* des diff�rents libell�s                                     *
	'*                                                             *
	'***************************************************************
	
	
	Public Function LoadMessprog(ByVal FichierMessprog As String, Optional ByRef StopMess As Boolean = False) As Boolean
		Dim Res As Object
		
		'---> Fonction charg�e de loader le fichier des messprog et d'alimenter les variables
		
		'Le fichier Messprog doit se trouver dans le r�pertoire : _
		'App.path + ..\Mqt\Messprog-[Code Langue].Lng
		
		Dim lpBuffer As String 'matrice des sections
		Dim SectionName As String 'nomd 'une section
		Dim nEntries As Short 'nombre de sections
		
		Dim i, j As Short
		
		Dim lpKey As String 'matrice des cles d'une section
		Dim nKey As Short 'nombre de cle
		
		Dim aLb As Libelle
		
		On Error GoTo LoadSectionErr
		
		'-> r�cup�ration de la liste des diff�rentes sections
		lpBuffer = Space(32765)
		'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Res. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Res = GetPrivateProfileString(vbNullString, "", "", lpBuffer, Len(lpBuffer), FichierMessprog)
		'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Res. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Res = 0 Then GoTo LoadSectionErr
		
		'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Res. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lpBuffer = Mid(lpBuffer, 1, Res)
		
		'-> Charger le tableau des sections
		'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		nEntries = NumEntries(lpBuffer, Chr(0))
		For i = 1 To nEntries
			'-> R�cup�rer le nom de la section
			SectionName = Entry(i, lpBuffer, Chr(0))
			'-> Pb WIN 95 : Gestion des blancs
			If Trim(SectionName) = "" Then Exit For
			'-> Chercher les diff�rentes cl�s
			lpKey = Space(32765)
			'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Res. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Res = GetPrivateProfileSection(SectionName, lpKey, Len(lpKey), FichierMessprog)
			'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Res. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			If Res <> 0 Then lpKey = Mid(lpKey, 1, Res)
			'-> Cr�er les objets libell�s
			If lpKey = "Null" Then
			Else
				'-> Initalisation de l'objet
				aLb = New Libelle
				
				'-> Ajout dans la collection des libelle
				Libelles.Add(aLb, UCase(SectionName))
				
				'-> Cr�ation des libell�s
				aLb.SetKeys(lpKey)
				
				'-> Liberrer le pointeur
				'UPGRADE_NOTE: L'objet aLb ne peut pas �tre d�truit tant qu'il n'est pas r�cup�r� par le garbage collector (ramasse-miettes). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
				aLb = Nothing
				
			End If
			
		Next  'Pour toute les sections
		
		LoadMessprog = True
		Exit Function
		
		
		
LoadSectionErr: 
		
		If StopMess Then
		Else
			MsgBox("Erreur durant l'analyse des sections", MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "Erreur LoadMessprog")
		End If
		LoadMessprog = False
		Exit Function
		
		
	End Function
	
	Public Function CreateMessProg(ByVal FileToLoad As String, Optional ByRef StopMess As Boolean = False) As Boolean
		
		Dim NomFic As String
		Dim Res As Boolean
		Dim TempFileName As String
		
		On Error GoTo GestError
		
		'-> V�rifier si on trouve le fichier langue dans le r�pertoire du client
		NomFic = ClientPath & FileToLoad
		'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		If Trim(Dir(NomFic)) = "" Then
			'-> Ligne de reprise
Reprise: 
			'-> On n'a pas trouv� le fichier langue du client. Chercher le standard
			NomFic = RessourcePath & FileToLoad
			
			'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			If Trim(Dir(NomFic, FileAttribute.Normal)) = "" Then
				MsgBox("Impossible de trouver le fichier langue sp�cifi�.", MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "Erreur fichier")
				GoTo GestError
			End If
		End If
		
		'-> PB WIN95 : L'utilisation des API des fichiers ini pose des pb pour _
		'les r�pertoires qui sont en lecture seule; En effet , lorsqu'il lit _
		'un fichier INI, WIN95 lock le fichier en �crivant dedans : donc -> Violation _
		'des privil�ges
		
		'TempFileName = GetTempFileNameVB("LNG")
		
		'-> Effectuer une copie du fichier ini dans le r�pertoire tempo
		'Res = CopyFile(NomFic, TempFileName, 0)
		
		TempFileName = NomFic
		
		'-> Lancer le load du fichier langue
		If StopMess Then
			Res = LoadMessprog(TempFileName, True)
		Else
			Res = LoadMessprog(TempFileName)
		End If
		If Not Res Then GoTo GestError
		
		'-> Supprimer le fichier langue tempo
		'If Dir$(TempFileName) <> "" Then Kill TempFileName
		
		CreateMessProg = True
		
		
		
		Exit Function
		
GestError: 
		
		If Err.Number = 52 Then
			ClientPath = ""
			GoTo Reprise
		End If
		
		If StopMess Then
		Else
			MsgBox("Erreur dans proc�dure du Load des Messprog", MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "Erreur fatale")
		End If
		'-> Supprimer le fichier le temporaire
		'If TempFileName <> "" And Dir$(TempFileName, vbNormal) <> "" Then Kill TempFileName
		CreateMessProg = False
		
	End Function
End Module