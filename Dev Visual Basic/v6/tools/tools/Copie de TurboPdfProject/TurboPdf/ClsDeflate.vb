Imports System
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Windows.Forms

Public Class ClsDeflate
    Private ErrorCode As Integer

    Public Function SsDeflate(ByVal InBuf As Byte(), ByVal InLen As Integer, <Out()> ByRef pOutLen As Integer) As Byte()
        Return prSsDeflate(InBuf, InLen, pOutLen)
    End Function

    Public Function SsDeflate(ByVal InBuf As Char(), ByVal InLen As Integer, <Out()> ByRef pOutLen As Integer) As Char()
        Return prSsDeflate(InBuf, InLen, pOutLen)
    End Function

    Private Function prSsDeflate(ByVal InBuf As Byte(), ByVal InLen As Integer, <Out()> ByRef pOutLen As Integer) As Byte()
        Dim dfl1 As ClsDfl = Nothing
        Dim buffer1 As Byte() = Nothing
        pOutLen = 0
        dfl1 = New ClsDfl
        dfl1.oFile = Nothing
        dfl1.BufLen = (InLen / 4)
        dfl1.buf = New Byte(dfl1.BufLen - 1) {}
        dfl1.BufIndex = 0
        Me.DflInitVars(dfl1)
        dfl1.CircData = New Byte(1300 - 1) {}
        dfl1.CircNext = New UInt16(1300 - 1) {}
        Dim num1 As Byte = 120
        If Not Me.DflPutByte(dfl1, num1) Then
            Return Nothing
        End If
        If Not Me.DflPutByte(dfl1, 156) Then
            Return Nothing
        End If
        If Not Me.DflConvert(dfl1, InBuf, InLen) Then
            pOutLen = Me.ErrorCode
        Else
            If (dfl1.BufIndex >= dfl1.buf.Length) Then
                dfl1.buf = ReAlloc(dfl1.buf, (dfl1.BufIndex + 10))
            End If
            dfl1.buf(dfl1.BufIndex) = 0
            buffer1 = dfl1.buf
            pOutLen = dfl1.BufIndex
            If (Not dfl1.oFile Is Nothing) Then
                dfl1.oFile.Close()
            End If
        End If
        Return buffer1
    End Function

    Private Function prSsDeflate(ByVal InBuf As Char(), ByVal InLen As Integer, <Out()> ByRef pOutLen As Integer) As Char()
        Dim buffer1 As Byte() = New Byte((InLen + 1) - 1) {}
        Dim num1 As Integer = 0
        Do While (num1 < InLen)
            buffer1(num1) = AscW(InBuf(num1))
            num1 += 1
        Loop
        Dim buffer2 As Byte() = Me.SsDeflate(buffer1, InLen, pOutLen)
        Dim chArray1 As Char() = New Char(pOutLen - 1) {}
        Dim num2 As Integer = 0
        Do While (num2 < pOutLen)
            chArray1(num2) = ChrW(buffer2(num2))
            num2 += 1
        Loop
        Return chArray1
    End Function

    Private Function DflInitVars(ByVal dfl As ClsDfl) As Boolean
        dfl.RunLenVal(0) = 3
        dfl.RunLenBits(0) = 0
        dfl.RunLenVal(1) = 4
        dfl.RunLenBits(1) = 0
        dfl.RunLenVal(2) = 5
        dfl.RunLenBits(2) = 0
        dfl.RunLenVal(3) = 6
        dfl.RunLenBits(3) = 0
        dfl.RunLenVal(4) = 7
        dfl.RunLenBits(4) = 0
        dfl.RunLenVal(5) = 8
        dfl.RunLenBits(5) = 0
        dfl.RunLenVal(6) = 9
        dfl.RunLenBits(6) = 0
        dfl.RunLenVal(7) = 10
        dfl.RunLenBits(7) = 0
        dfl.RunLenVal(8) = 11
        dfl.RunLenBits(8) = 1
        dfl.RunLenVal(9) = 13
        dfl.RunLenBits(9) = 1
        dfl.RunLenVal(10) = 15
        dfl.RunLenBits(10) = 1
        dfl.RunLenVal(11) = 17
        dfl.RunLenBits(11) = 1
        dfl.RunLenVal(12) = 19
        dfl.RunLenBits(12) = 2
        dfl.RunLenVal(13) = 23
        dfl.RunLenBits(13) = 2
        dfl.RunLenVal(14) = 27
        dfl.RunLenBits(14) = 2
        dfl.RunLenVal(15) = 31
        dfl.RunLenBits(15) = 2
        dfl.RunLenVal(16) = 35
        dfl.RunLenBits(16) = 3
        dfl.RunLenVal(17) = 43
        dfl.RunLenBits(17) = 3
        dfl.RunLenVal(18) = 51
        dfl.RunLenBits(18) = 3
        dfl.RunLenVal(19) = 59
        dfl.RunLenBits(19) = 3
        dfl.RunLenVal(20) = 67
        dfl.RunLenBits(20) = 4
        dfl.RunLenVal(21) = 83
        dfl.RunLenBits(21) = 4
        dfl.RunLenVal(22) = 99
        dfl.RunLenBits(22) = 4
        dfl.RunLenVal(23) = 115
        dfl.RunLenBits(23) = 4
        dfl.RunLenVal(24) = 131
        dfl.RunLenBits(24) = 5
        dfl.RunLenVal(25) = 163
        dfl.RunLenBits(25) = 5
        dfl.RunLenVal(26) = 195
        dfl.RunLenBits(26) = 5
        dfl.RunLenVal(27) = 227
        dfl.RunLenBits(27) = 5
        dfl.RunLenVal(28) = 258
        dfl.RunLenBits(28) = 0
        dfl.DistVal(0) = 1
        dfl.DistBits(0) = 0
        dfl.DistVal(1) = 2
        dfl.DistBits(1) = 0
        dfl.DistVal(2) = 3
        dfl.DistBits(2) = 0
        dfl.DistVal(3) = 4
        dfl.DistBits(3) = 0
        dfl.DistVal(4) = 5
        dfl.DistBits(4) = 1
        dfl.DistVal(5) = 7
        dfl.DistBits(5) = 1
        dfl.DistVal(6) = 9
        dfl.DistBits(6) = 2
        dfl.DistVal(7) = 13
        dfl.DistBits(7) = 2
        dfl.DistVal(8) = 17
        dfl.DistBits(8) = 3
        dfl.DistVal(9) = 25
        dfl.DistBits(9) = 3
        dfl.DistVal(10) = 33
        dfl.DistBits(10) = 4
        dfl.DistVal(11) = 49
        dfl.DistBits(11) = 4
        dfl.DistVal(12) = 65
        dfl.DistBits(12) = 5
        dfl.DistVal(13) = 97
        dfl.DistBits(13) = 5
        dfl.DistVal(14) = 129
        dfl.DistBits(14) = 6
        dfl.DistVal(15) = 193
        dfl.DistBits(15) = 6
        dfl.DistVal(16) = 257
        dfl.DistBits(16) = 7
        dfl.DistVal(17) = 385
        dfl.DistBits(17) = 7
        dfl.DistVal(18) = 513
        dfl.DistBits(18) = 8
        dfl.DistVal(19) = 769
        dfl.DistBits(19) = 8
        dfl.DistVal(20) = 1025
        dfl.DistBits(20) = 9
        dfl.DistVal(21) = 1537
        dfl.DistBits(21) = 9
        dfl.DistVal(22) = 2049
        dfl.DistBits(22) = 10
        dfl.DistVal(23) = 3073
        dfl.DistBits(23) = 10
        dfl.DistVal(24) = 4097
        dfl.DistBits(24) = 11
        dfl.DistVal(25) = 6145
        dfl.DistBits(25) = 11
        dfl.DistVal(26) = 8193
        dfl.DistBits(26) = 12
        dfl.DistVal(27) = 12289
        dfl.DistBits(27) = 12
        dfl.DistVal(28) = 16385
        dfl.DistBits(28) = 13
        dfl.DistVal(29) = 24577
        dfl.DistBits(29) = 13
        dfl.CrcTable = New UInt32(256 - 1) {}
        Dim num1 As Integer = 0
        Do While (num1 < 256)
            Dim num3 As UInt32 = num1
            Dim num2 As Integer = 0
            Do While (num2 < 8)
                num3 = IIf((Not (num3 And 1) = Nothing), (3988292384 Xor (num3 >> 1)), (num3 >> 1))
                num2 += 1
            Loop
            dfl.CrcTable(num1) = num3
            num1 += 1
        Loop
        Return True
    End Function

    Private Function DflPutBits(ByVal dfl As ClsDfl, ByVal NumBits As Integer, ByVal val As UInt32) As Boolean
        Dim num1 As Byte = dfl.CurChar
        Dim num2 As Integer = dfl.DataBitPos
        If (NumBits > 0) Then
            Do While (NumBits > 0)
                Dim num3 As Integer = (8 - num2)
                If (num3 > NumBits) Then
                    num3 = NumBits
                End If
                Dim num4 As UInt32 = ((1 << (num3 And 31)) - 1)
                Dim num5 As UInt32 = (val And num4)
                num1 = ((num5 << (num2 And 31)) + num1)
                num2 = (num2 + num3)
                If (num2 = 8) Then
                    If Not Me.DflPutByte(dfl, num1) Then
                        Return False
                    End If
                    num1 = 0
                    num2 = 0
                End If
                val = (val >> (num3 And 31))
                NumBits = (NumBits - num3)
            Loop
            dfl.CurChar = num1
            dfl.DataBitPos = num2
        End If
        Return True
    End Function

    Private Function DflPutByte(ByVal dfl As ClsDfl, ByVal CurChar As Byte) As Boolean
        If (Not dfl.oFile Is Nothing) Then
            If (dfl.TextIndex >= 1000) Then
                Me.DflFlushBytes(dfl)
            End If
            dfl.text(dfl.TextIndex) = CurChar
            dfl.TextIndex += 1
        Else
            If (dfl.BufIndex >= dfl.BufLen) Then
                dfl.BufLen = ((dfl.BufLen * 5) / 4)
                dfl.buf = ReAlloc(dfl.buf, (dfl.BufLen + 1))
            End If
            dfl.buf(dfl.BufIndex) = CurChar
            dfl.BufIndex += 1
        End If
        dfl.FilePos += 1
        dfl.ChunkSize += 1
        Return True
    End Function

    Private Function DflPutBytes(ByVal dfl As ClsDfl, ByVal [text] As Byte(), ByVal len As Integer) As Boolean
        Dim num1 As Integer = 0
        Do While (num1 < len)
            If Not Me.DflPutByte(dfl, [text](num1)) Then
                Return False
            End If
            num1 += 1
        Loop
        Return True
    End Function

    Private Function DflConvert(ByVal dfl As ClsDfl, ByVal InBuf As Byte(), ByVal InLen As Integer) As Boolean
        Dim tbl1 As New ClsDflHuffTbl
        Dim tbl2 As New ClsDflHuffTbl
        dfl.Checksum = 1
        Me.DflCreateFixedHuffTable(dfl, tbl1, tbl2)
        Dim num2 As Integer = dfl.FilePos
        dfl.CurChar = 0
        dfl.DataBitPos = 0
        If Not Me.DflPutBits(dfl, 1, 1) Then
            Return False
        End If
        If Not Me.DflPutBits(dfl, 2, 1) Then
            Return False
        End If
        Dim num1 As Integer = 0
        Do While (num1 < InLen)
            If Not Me.DflWriteDataByte(dfl, InBuf(num1), tbl1, tbl2) Then
                Return False
            End If
            If Not Me.DflUpdateChecksum(dfl, InBuf(num1)) Then
                Return False
            End If
            num1 += 1
        Loop
        If ((dfl.RunLen > 0) AndAlso Not Me.DflWriteRun(dfl, tbl1, tbl2)) Then
            Return False
        End If
        If Not Me.DflWriteHuffCode(dfl, 256, tbl1) Then
            Return False
        End If
        If ((dfl.DataBitPos > 0) AndAlso Not Me.DflPutByte(dfl, dfl.CurChar)) Then
            Return False
        End If
        If Not Me.DflPutDWord(dfl, dfl.Checksum) Then
            Return False
        End If
        Me.DflFreeHuffTbl(dfl, tbl1)
        Me.DflFreeHuffTbl(dfl, tbl2)
        Return True
    End Function

    Private Function ReAlloc(ByVal OldObj As Byte(), ByVal count As Integer) As Byte()
        Dim buffer1 As Byte() = New Byte(count - 1) {}
        Dim num1 As Integer = 0
        Do While ((num1 < count) AndAlso (num1 < OldObj.Length))
            buffer1(num1) = OldObj(num1)
            num1 += 1
        Loop
        Return buffer1
    End Function

    Private Function DflCreateFixedHuffTable(ByVal dfl As ClsDfl, ByVal pHuffLit As ClsDflHuffTbl, ByVal pHuffDist As ClsDflHuffTbl) As Boolean
        Me.DflAllocHuffTbl(dfl, pHuffLit, 288)
        Me.DflAllocHuffTbl(dfl, pHuffDist, 30)
        Dim num1 As Integer = 0
        Do While (num1 < pHuffLit.count)
            pHuffLit.pSymbol(num1) = num1
            If (num1 < 144) Then
                pHuffLit.pCodeLen(num1) = 8
            ElseIf (num1 < 256) Then
                pHuffLit.pCodeLen(num1) = 9
            ElseIf (num1 < 280) Then
                pHuffLit.pCodeLen(num1) = 7
            Else
                pHuffLit.pCodeLen(num1) = 8
            End If
            num1 += 1
        Loop
        num1 = 0
        Do While (num1 < pHuffDist.count)
            pHuffDist.pSymbol(num1) = num1
            pHuffDist.pCodeLen(num1) = 5
            num1 += 1
        Loop
        Me.DflCreateHuffCode(dfl, pHuffLit)
        Me.DflCreateHuffCode(dfl, pHuffDist)
        Return True
    End Function

    Private Function DflAllocHuffTbl(ByVal dfl As ClsDfl, ByVal pHuff As ClsDflHuffTbl, ByVal count As Integer) As Boolean
        Me.DflFreeHuffTbl(dfl, pHuff)
        pHuff.count = count
        pHuff.pSymbol = New Integer((count + 1) - 1) {}
        pHuff.pCode = New Integer((count + 1) - 1) {}
        pHuff.pCodeLen = New Integer((count + 1) - 1) {}
        Return True
    End Function

    Private Function DflWriteDataByte(ByVal dfl As ClsDfl, ByVal data As Byte, ByVal pHuffLit As ClsDflHuffTbl, ByVal pHuffDist As ClsDflHuffTbl) As Boolean
        Dim buffer1 As Byte() = dfl.CircData
        Dim numArray1 As UInt16() = dfl.CircNext
        If (dfl.CircBufLen = Nothing) Then
            Me.DflWriteHuffCode(dfl, data, pHuffLit)
            buffer1(0) = data
            numArray1(0) = 65535
            dfl.FirstRun = 65535
            dfl.RunLen = 0
            dfl.CircBufLen = 1
        Else
            Dim num2 As UInt16
            If (dfl.RunLen = Nothing) Then
                Dim num3 As UInt16 = dfl.CircBufLen
                dfl.FirstRun = 65535
                num2 = 65535
                Dim num1 As UInt16 = 0
                Dim num4 As Integer = 0
                Do While (num4 < num3)
                    numArray1(num4) = 65535
                    num4 += 1
                Loop
                num1 = 0
                Do While (num1 < num3)
                    If (buffer1(num1) = data) Then
                        If (Not num2 = 65535) Then
                            numArray1(num2) = num1
                        End If
                        If (dfl.FirstRun = 65535) Then
                            dfl.FirstRun = num1
                        End If
                        num2 = num1
                    End If
                    num1 += 1
                Loop
                If (Not dfl.FirstRun = 65535) Then
                    dfl.RunLen = 1
                End If
                If (dfl.FirstRun = 65535) Then
                    Me.DflWriteHuffCode(dfl, data, pHuffLit)
                    buffer1(dfl.CircBufLen) = data
                    numArray1(dfl.CircBufLen) = 65535
                    dfl.CircBufLen += 1
                    dfl.FirstRun = 65535
                    dfl.RunLen = 0
                    If (dfl.CircBufLen > 300) Then
                        Me.DflScrollCircBuf(dfl)
                    End If
                End If
            ElseIf (dfl.RunLen = 258) Then
                If Not Me.DflWriteRun(dfl, pHuffLit, pHuffDist) Then
                    Return False
                End If
                If Not Me.DflWriteDataByte(dfl, data, pHuffLit, pHuffDist) Then
                    Return False
                End If
            Else
                Dim num5 As UInt16 = dfl.RunLen
                Dim num6 As UInt16 = 65535
                Dim num8 As UInt16 = dfl.FirstRun
                num2 = 65535
                Do While (Not num8 = 65535)
                    If (((num8 + num5) < dfl.CircBufLen) AndAlso (buffer1((num8 + num5)) = data)) Then
                        If (num2 = 65535) Then
                            num6 = num8
                        Else
                            numArray1(num2) = num8
                        End If
                        num2 = num8
                    End If
                    Dim num7 As UInt16 = num8
                    num8 = numArray1(num8)
                    numArray1(num7) = 65535
                Loop
                If (Not num6 = 65535) Then
                    dfl.RunLen += 1
                    dfl.FirstRun = num6
                Else
                    If Not Me.DflWriteRun(dfl, pHuffLit, pHuffDist) Then
                        Return False
                    End If
                    If Not Me.DflWriteDataByte(dfl, data, pHuffLit, pHuffDist) Then
                        Return False
                    End If
                End If
            End If
        End If
        Return True
    End Function

    Private Function DflUpdateChecksum(ByVal dfl As ClsDfl, ByVal CurChar As Byte) As Boolean
        Dim num1 As UInt32 = (dfl.Checksum And 65535)
        Dim num2 As UInt32 = ((dfl.Checksum >> 16) And 65535)
        num1 = ((num1 + CurChar) Mod 65521)
        num2 = ((num2 + num1) Mod 65521)
        dfl.Checksum = ((num2 << 16) + num1)
        Return True
    End Function

    Private Function DflWriteRun(ByVal dfl As ClsDfl, ByVal pHuffLit As ClsDflHuffTbl, ByVal pHuffDist As ClsDflHuffTbl) As Boolean
        Dim num2 As UInt16
        Dim buffer1 As Byte() = dfl.CircData
        Dim numArray1 As UInt16() = dfl.CircNext
        Dim numArray2 As Integer() = dfl.RunLenVal
        Dim numArray3 As Integer() = dfl.DistVal
        Dim num7 As UInt16 = dfl.FirstRun
        Dim num4 As UInt16 = dfl.RunLen
        If (num4 < 3) Then
            num2 = 0
            Do While (num2 < dfl.RunLen)
                Dim num8 As Byte = buffer1((num7 + num2))
                Me.DflWriteHuffCode(dfl, num8, pHuffLit)
                num2 += 1
            Loop
        Else
            num2 = 1
            Do While (num2 < 29)
                If (numArray2(num2) > num4) Then
                    Exit Do
                End If
                num2 += 1
            Loop
            num2 -= 1
            Dim num1 As UInt16 = (num2 + 257)
            Me.DflWriteHuffCode(dfl, num1, pHuffLit)
            Dim num3 As UInt16 = dfl.RunLenBits(num2)
            Dim num6 As UInt16 = (num4 - dfl.RunLenVal(num2))
            If ((num3 > 0) AndAlso Not Me.DflPutBits(dfl, num3, num6)) Then
                Return False
            End If
            Dim num5 As UInt16 = (dfl.CircBufLen - num7)
            num2 = 1
            Do While (num2 < 30)
                If (numArray3(num2) > num5) Then
                    Exit Do
                End If
                num2 += 1
            Loop
            num2 -= 1
            num1 = num2
            Me.DflWriteHuffCode(dfl, num1, pHuffDist)
            num3 = dfl.DistBits(num2)
            num6 = (num5 - dfl.DistVal(num2))
            If ((num3 > 0) AndAlso Not Me.DflPutBits(dfl, num3, num6)) Then
                Return False
            End If
        End If
        Dim num9 As UInt16 = dfl.CircBufLen
        num2 = 0
        Do While (num2 < num4)
            buffer1(num9) = buffer1((num7 + num2))
            num9 += 1
            num2 += 1
        Loop
        dfl.CircBufLen = num9
        dfl.RunLen = 0
        If (dfl.CircBufLen > 300) Then
            Me.DflScrollCircBuf(dfl)
        End If
        Return True
    End Function

    Private Function DflFreeHuffTbl(ByVal dfl As ClsDfl, ByVal pHuff As ClsDflHuffTbl) As Boolean
        Return True
    End Function

    Private Function DflPutDWord(ByVal dfl As ClsDfl, ByVal val As UInt32) As Boolean
        Dim buffer1 As Byte() = New Byte(4 - 1) {}
        val = Me.DflXlateDWord(val)
        buffer1(0) = (val >> 24)
        buffer1(1) = ((val >> 16) And 255)
        buffer1(2) = ((val >> 8) And 255)
        buffer1(3) = (val And 255)
        Return Me.DflPutBytes(dfl, buffer1, 4)
    End Function

    Private Function DflCreateHuffCode(ByVal dfl As ClsDfl, ByVal pHuff As ClsDflHuffTbl) As Boolean
        Dim num3 As Integer
        Dim num6 As Integer
        Dim num7 As Integer
        Dim num8 As Integer
        Dim num1 As Integer = 20
        Dim numArray1 As Integer() = New Integer(21 - 1) {}
        Dim numArray2 As Integer() = New Integer(21 - 1) {}
        Dim num2 As Integer = 0
        Do While (num2 < num1)
            numArray1(num2) = 0
            num2 += 1
        Loop
        num2 = 0
        Do While (num2 < pHuff.count)
            Dim ptr1 As IntPtr
            numArray1(ptr1 = pHuff.pCodeLen(num2)) = (numArray1(ptr1) + 1)
            num2 += 1
        Loop
        numArray1(0) = 0
        Dim num4 As Integer = 0
        num2 = 1
        Do While (num2 <= num1)
            num4 = ((num4 + numArray1((num2 - 1))) << 1)
            numArray2(num2) = num4
            num2 += 1
        Loop
        num2 = 0
        Do While (num2 < pHuff.count)
            num3 = num2
            Do While (num3 < pHuff.count)
                If (pHuff.pSymbol(num3) < pHuff.pSymbol(num2)) Then
                    num6 = pHuff.pCode(num2)
                    num7 = pHuff.pCodeLen(num2)
                    num8 = pHuff.pSymbol(num2)
                    pHuff.pCode(num2) = pHuff.pCode(num3)
                    pHuff.pCodeLen(num2) = pHuff.pCodeLen(num3)
                    pHuff.pSymbol(num2) = pHuff.pSymbol(num3)
                    pHuff.pCode(num3) = num6
                    pHuff.pCodeLen(num3) = num7
                    pHuff.pSymbol(num3) = num8
                End If
                num3 += 1
            Loop
            num2 += 1
        Loop
        num2 = 0
        Do While (num2 < pHuff.count)
            Dim num5 As Integer = pHuff.pCodeLen(num2)
            If (num5 > 0) Then
                Dim ptr2 As IntPtr
                pHuff.pCode(num2) = numArray2(num5)
                numArray2(ptr2 = num5) = (numArray2(ptr2) + 1)
            End If
            num2 += 1
        Loop
        num2 = 0
        Do While (num2 < pHuff.count)
            pHuff.pCode(num2) = Me.DflReverseBits(dfl, pHuff.pCode(num2), pHuff.pCodeLen(num2))
            num2 += 1
        Loop
        If dfl.input Then
            num2 = 0
            Do While (num2 < pHuff.count)
                num3 = num2
                Do While (num3 < pHuff.count)
                    If (pHuff.pCodeLen(num3) < pHuff.pCodeLen(num2)) Then
                        num6 = pHuff.pCode(num2)
                        num7 = pHuff.pCodeLen(num2)
                        num8 = pHuff.pSymbol(num2)
                        pHuff.pCode(num2) = pHuff.pCode(num3)
                        pHuff.pCodeLen(num2) = pHuff.pCodeLen(num3)
                        pHuff.pSymbol(num2) = pHuff.pSymbol(num3)
                        pHuff.pCode(num3) = num6
                        pHuff.pCodeLen(num3) = num7
                        pHuff.pSymbol(num3) = num8
                    End If
                    num3 += 1
                Loop
                num2 += 1
            Loop
            pHuff.FirstCode = 0
            num2 = 0
            Do While (num2 < pHuff.count)
                If (Not pHuff.pCodeLen(num2) = 0) Then
                    pHuff.FirstCode = num2
                    Exit Do
                End If
                num2 += 1
            Loop
        End If
        Return True
    End Function

    Private Function DflWriteHuffCode(ByVal dfl As ClsDfl, ByVal sym As UInt16, ByVal pHuff As ClsDflHuffTbl) As Boolean
        If Not Me.DflPutBits(dfl, pHuff.pCodeLen(sym), pHuff.pCode(sym)) Then
            Return False
        End If
        Return True
    End Function

    Private Function DflReverseBits(ByVal dfl As ClsDfl, ByVal InVal As Integer, ByVal count As Integer) As Integer
        Dim num3 As UInt32 = InVal
        Dim num4 As UInt32 = 0
        Dim num1 As Integer = 0
        Do While (num1 < count)
            Dim num2 As UInt32 = (num3 And 1)
            num3 = (num3 >> 1)
            num4 = (num4 << 1)
            num4 = (num4 Or num2)
            num1 += 1
        Loop
        Return num4
    End Function

    Private Function DflScrollCircBuf(ByVal dfl As ClsDfl) As Boolean
        Dim num1 As Integer = (dfl.CircBufLen - 150)
        If (dfl.CircBufLen > 300) Then
            Dim num2 As Integer = 0
            Dim num3 As Integer = num1
            Do While (num2 < 150)
                dfl.CircData(num2) = dfl.CircData(num3)
                dfl.CircNext(num2) = dfl.CircNext(num3)
                num2 += 1
                num3 += 1
            Loop
            dfl.CircBufLen = 150
        End If
        Return True
    End Function

    Private Function DflFlushBytes(ByVal dfl As ClsDfl) As Boolean
        If (Not dfl.oFile Is Nothing) Then
            If (dfl.TextIndex = 0) Then
                Return True
            End If
            Dim num1 As Integer = 0
            Do While (num1 < dfl.TextIndex)
                dfl.oFile.Write(dfl.text(num1))
                num1 += 1
            Loop
            dfl.TextIndex = 0
        End If
        Return True
    End Function

    Private Function DflXlateDWord(ByVal val As UInt32) As UInt32
        Dim num1 As UInt32 = (val >> 24)
        Dim num2 As UInt32 = ((val >> 16) And 255)
        Dim num3 As UInt32 = ((val >> 8) And 255)
        Dim num4 As UInt32 = (val And 255)
        Return ((((num4 << 24) + (num3 << 16)) + (num2 << 8)) + num1)
    End Function

    Private Class ClsDfl
        ' Methods
        Public Sub New()
            Me.text = New Byte(1001 - 1) {}
            Me.stack = New Byte(1001 - 1) {}
            Me.RunLenVal = New Integer(35 - 1) {}
            Me.RunLenBits = New Integer(35 - 1) {}
            Me.DistVal = New Integer(35 - 1) {}
            Me.DistBits = New Integer(35 - 1) {}
            Me.CrcTable = New UInt32(256 - 1) {}
        End Sub


        ' Fields
        Public buf As Byte()
        Public BufIndex As Integer
        Public BufLen As Integer
        Public Checksum As UInt32
        Public ChunkSize As Integer
        Public CircBufLen As UInt16
        Public CircData As Byte()
        Public CircNext As UInt16()
        Public crc As UInt32
        Public CrcTable As UInt32()
        Public CurChar As Byte
        Public CurWord As UInt16
        Public DataBitPos As Integer
        Public DataPos As Integer
        Public DataSize As UInt32
        Public DistBits As Integer()
        Public DistVal As Integer()
        Public eof As Boolean
        Public extracted As Boolean
        Public FilePos As Integer
        Public FirstRun As UInt16
        Public input As Boolean
        Public oFile As StreamWriter
        Public pData As Byte()
        Public RunLen As UInt16
        Public RunLenBits As Integer()
        Public RunLenVal As Integer()
        Public stack As Byte()
        Public StackLen As Integer
        Public [text] As Byte()
        Public TextIndex As Integer
        Public TextLen As Integer
    End Class

    Private Class ClsDflHuffTbl
        ' Fields
        Public count As Integer
        Public FirstCode As Integer
        Public pCode As Integer()
        Public pCodeLen As Integer()
        Public pSymbol As Integer()
    End Class

End Class
