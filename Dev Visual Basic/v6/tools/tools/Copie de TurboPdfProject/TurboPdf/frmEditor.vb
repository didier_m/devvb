Option Strict Off
Option Explicit On
Public Class frmEditor
    Inherits System.Windows.Forms.Form
    Public CurrentFile As String
    Public IsSaved As Boolean
    Dim FindWord As String

    Private Sub frmEditor_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        On Error Resume Next

        Dim aLb As Libelle

        '-> Par d�faut ne pas demander d'enregistrer les modifications
        IsSaved = True
        CurrentFile = ""

        '-> Chargement des messprog
        aLb = Libelles.Item("FRMEDITOR")

        Me.mnuFichier.Text = aLb.GetCaption(1)
        Me.mnuOpen.Text = aLb.GetCaption(2)
        Me.mnuSave.Text = aLb.GetCaption(3)
        Me.mnuSaveAs.Text = aLb.GetCaption(4)
        Me.mnuPrint.Text = aLb.GetCaption(5)
        Me.mnuQuit.Text = aLb.GetCaption(6)
        Me.mnuEdition.Text = aLb.GetCaption(7)
        Me.mnuCut.Text = aLb.GetCaption(8)
        Me.mnuCopy.Text = aLb.GetCaption(9)
        Me.mnuPaste.Text = aLb.GetCaption(10)
        Me.mnuFind.Text = aLb.GetCaption(11)
        Me.mnuApropos.Text = aLb.GetCaption(12)
        Me.mnuFormat.Text = aLb.GetCaption(14)
        Me.mnuPolice.Text = aLb.GetCaption(15)

    End Sub

    Private Sub frmEditor_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason

        Dim Rep As MsgBoxResult

        '-> Quitter si fichier en lecture seule
        If Me.mnuEdition.Enabled = False Then Exit Sub

        If Not IsSaved Then
            Rep = MsgBox("Enregistrer les modifications ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "Quitter")
            '-> Annuler la commande
            If Rep = MsgBoxResult.Cancel Then
                Cancel = 1
                Exit Sub
            End If

            '-> Quitter
            If Rep = MsgBoxResult.No Then Exit Sub

            '-> Lancer l'enregistrement
            If CurrentFile = "" Then
                mnuSaveAs_Click(mnuSaveAs, New System.EventArgs())
            Else
                SaveFile()
            End If
        End If

        eventArgs.Cancel = Cancel
    End Sub

    'UPGRADE_WARNING: L'�v�nement frmEditor.Resize peut se d�clencher lorsque le formulaire est initialis�. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub frmEditor_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize

        Dim aRect As RECT

        GetClientRect(Handle.ToInt32, aRect)
        Me.RichTextBox1.Left = 0
        Me.RichTextBox1.Top = 0
        Me.RichTextBox1.Width = aRect.Right_Renamed
        Me.RichTextBox1.Height = aRect.Bottom


    End Sub

    Public Sub mnuCopy_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuCopy.Click

        '-> Envoyer un message au Rtf pour gestion du press papier
        ApiWin32.SendMessage(Me.RichTextBox1.Handle.ToInt32, WM_COPY, 0, 0)

    End Sub

    Public Sub mnuCut_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuCut.Click

        ''-> Charger dans le press papier
        'Clipboard.SetText Me.RichTextBox1.SelText
        '
        ''-> Supprimer de l'�diteur
        'Me.RichTextBox1.SelText = ""

        ApiWin32.SendMessage(Me.RichTextBox1.Handle.ToInt32, WM_CUT, 0, 0)

    End Sub

    Public Sub mnuFind_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFind.Click

        Dim Rep As String
        Dim aLb As Libelle
        Dim lblString As String
        Dim TitreString As String

        On Error Resume Next

        '-> Postionner
        lblString = "Rechercher : "
        TitreString = "Recherche"

        '-> Pointer sur la classe message
        aLb = Libelles.Item("FRMEDITOR")
        lblString = aLb.GetCaption(13)
        TitreString = aLb.GetCaption(11)



        Rep = InputBox(lblString, TitreString, "")
        If Rep = "" Then Exit Sub

        '-> Stocker le mot de recherche
        FindWord = Rep

        '-> chercher
        Me.RichTextBox1.Find(Rep)


    End Sub

    Public Sub mnuOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuOpen.Click

        Dim Rep As MsgBoxResult

        On Error GoTo GestError

        '-> Tester si un fichier est ouvert
        If CurrentFile <> "" And Not IsSaved Then
            '-> Demander l'enregistrement du fichier Oui, Non , Annuler
            Rep = MsgBox("Enregistrer les modifications ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "Enregistrer")
            Select Case Rep
                Case MsgBoxResult.Yes
                    SaveFile()
                Case MsgBoxResult.No
                Case MsgBoxResult.Cancel
                    Exit Sub
            End Select
        End If

        Me.CommonDialog1Open.FileName = "*.*"
        Me.CommonDialog1Save.FileName = "*.*"
        Me.CommonDialog1Open.DefaultExt = "*.*"
        Me.CommonDialog1Save.DefaultExt = "*.*"
        'UPGRADE_WARNING: Filter a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        Me.CommonDialog1Open.Filter = "Tous les fichiers (*.*)|*.*"
        Me.CommonDialog1Save.Filter = "Tous les fichiers (*.*)|*.*"

        '-> Flags d'ouverture
        'UPGRADE_WARNING: FileOpenConstants constante FileOpenConstants.cdlOFNHideReadOnly a �t� mis � niveau vers OpenFileDialog.ShowReadOnly, qui a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
        'UPGRADE_WARNING: MSComDlg.CommonDialog propri�t� Me.CommonDialog1.Flags a �t� mis � niveau vers Me.CommonDialog1Open.CheckFileExists, qui a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
        Me.CommonDialog1Open.CheckFileExists = True
        Me.CommonDialog1Open.CheckPathExists = True
        Me.CommonDialog1Save.CheckPathExists = True
        'UPGRADE_WARNING: MSComDlg.CommonDialog propri�t� Me.CommonDialog1.Flags a �t� mis � niveau vers Me.CommonDialog1Open.ShowReadOnly, qui a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
        'UPGRADE_WARNING: FileOpenConstants constante FileOpenConstants.cdlOFNHideReadOnly a �t� mis � niveau vers OpenFileDialog.ShowReadOnly, qui a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
        Me.CommonDialog1Open.ShowReadOnly = False

        '-> Afficher la feuille
        Me.CommonDialog1Open.ShowDialog()
        Me.CommonDialog1Save.FileName = Me.CommonDialog1Open.FileName

        '-> Quitter si nom de fichier = ""
        If Me.CommonDialog1Open.FileName = "" Then Exit Sub

        '-> Afficher le fichier dans l'�diteur
        Me.RichTextBox1.LoadFile(Me.CommonDialog1Open.FileName)

        '-> Indiquer qu'on ne doit pas enregistr�
        IsSaved = True

        '-> Indiquer le nom du fichier
        CurrentFile = Me.CommonDialog1Open.FileName
        Me.Text = CurrentFile

        Exit Sub

GestError:
        If Err.Number <> 32755 Then
            MsgBox("Impossible d'ouvrir le fichier sp�cifi�", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Erreur")
        End If

    End Sub


    Private Sub SaveFile()

        '---> Cette fonction lance l'enregistrement du fichier modifi�

        On Error GoTo GestError

        Me.RichTextBox1.SaveFile(CurrentFile, Windows.Forms.RichTextBoxStreamType.PlainText)

        Exit Sub

GestError:

        MsgBox("Impossible d'enregistrer le fichier.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Erreur")

    End Sub

    Public Sub mnuPaste_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuPaste.Click

        'Dim Temp As String
        '
        ''-> Charger dans le press papier
        'Temp = Clipboard.GetText(1)
        '
        'Me.RichTextBox1.SetFocus
        '
        'Me.RichTextBox1.SelText = Temp

        ApiWin32.SendMessage(Me.RichTextBox1.Handle.ToInt32, WM_PASTE, 0, 0)

    End Sub

    Public Sub mnuPolice_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuPolice.Click

        '---> Afficher la boite de dialogue commune

        On Error GoTo GestError


        Me.CommonDialog1Font.Font = VB6.FontChangeName(Me.CommonDialog1Font.Font, Me.RichTextBox1.Font.Name)
        Me.CommonDialog1Font.Font = VB6.FontChangeBold(Me.CommonDialog1Font.Font, Me.RichTextBox1.Font.Bold)
        Me.CommonDialog1Font.Font = VB6.FontChangeItalic(Me.CommonDialog1Font.Font, Me.RichTextBox1.Font.Italic)
        Me.CommonDialog1Font.Font = VB6.FontChangeSize(Me.CommonDialog1Font.Font, Me.RichTextBox1.Font.SizeInPoints)
        'UPGRADE_ISSUE: La constante cdlCFBoth n'a pas �t� mise � niveau. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
        'UPGRADE_ISSUE: MSComDlg.CommonDialog propri�t� CommonDialog1.Flags - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        'Me.CommonDialog1.Flags = MSComDlg.FontsConstants.cdlCFBoth
        Me.CommonDialog1Font.ShowDialog()
        Me.CommonDialog1Print.PrinterSettings.MaximumPage = Me.CommonDialog1Font.MaxSize
        Me.CommonDialog1Print.PrinterSettings.MinimumPage = Me.CommonDialog1Font.MinSize

        Me.RichTextBox1.Font = VB6.FontChangeName(Me.RichTextBox1.Font, Me.CommonDialog1Font.Font.Name)
        Me.RichTextBox1.Font = VB6.FontChangeBold(Me.RichTextBox1.Font, Me.CommonDialog1Font.Font.Bold)
        Me.RichTextBox1.Font = VB6.FontChangeItalic(Me.RichTextBox1.Font, Me.CommonDialog1Font.Font.Italic)
        Me.RichTextBox1.Font = VB6.FontChangeSize(Me.RichTextBox1.Font, Me.CommonDialog1Font.Font.Size)

        Exit Sub

GestError:

    End Sub

    Public Sub mnuPrint_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuPrint.Click

        '---> Cette proc�dure envoie l'impression du contenu

        On Error GoTo GestError

        '--> Quitter si pas de fichier
        If Trim(Me.RichTextBox1.Text) = "" Then
            MsgBox("Pas de document � imprimer", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Erreur")
            Exit Sub
        End If

        '-> Afficher la boite d'impression
        'UPGRADE_ISSUE: La constante cdlPDReturnDC n'a pas �t� mise � niveau. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
        'UPGRADE_ISSUE: MSComDlg.CommonDialog propri�t� CommonDialog1.Flags - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        'Me.CommonDialog1.Flags = MSComDlg.PrinterConstants.cdlPDReturnDC
        'UPGRADE_WARNING: MSComDlg.CommonDialog propri�t� Me.CommonDialog1.Flags a �t� mis � niveau vers Me.CommonDialog1Print.AllowSelection, qui a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
        Me.CommonDialog1Print.AllowSelection = False
        Me.CommonDialog1Print.ShowDialog()
        Me.CommonDialog1Font.MaxSize = Me.CommonDialog1Print.PrinterSettings.MaximumPage
        Me.CommonDialog1Font.MinSize = Me.CommonDialog1Print.PrinterSettings.MinimumPage

        '-> Lander l'impression
        'UPGRADE_ISSUE: MSComDlg.CommonDialog propri�t� CommonDialog1.hDC - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        'UPGRADE_ISSUE: RichTextLib.RichTextBox m�thode RichTextBox1.SelPrint - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        'Me.RichTextBox1.SelPrint(Me.CommonDialog1.hDC)

        Exit Sub

GestError:
        If Err.Number = 32755 Then Exit Sub

    End Sub

    Public Sub mnuQuit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuQuit.Click

        Me.Close()

    End Sub

    Public Sub mnuSave_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuSave.Click

        If CurrentFile = "" Then
            mnuSaveAs_Click(mnuSaveAs, New System.EventArgs())
        Else
            SaveFile()
        End If

    End Sub

    Public Sub mnuSaveAs_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuSaveAs.Click

        '---> Demander l'enregistrement

        Dim Rep As MsgBoxResult

        On Error GoTo GestError

        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Me.CommonDialog1Open.FileName = Entry(NumEntries(CurrentFile, "\"), CurrentFile, "\")
        Me.CommonDialog1Save.FileName = Entry(NumEntries(CurrentFile, "\"), CurrentFile, "\")
        Me.CommonDialog1Open.DefaultExt = ""
        Me.CommonDialog1Save.DefaultExt = ""
        'UPGRADE_WARNING: Filter a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        Me.CommonDialog1Open.Filter = "Tous les fichiers (*.*)|*.*"
        Me.CommonDialog1Save.Filter = "Tous les fichiers (*.*)|*.*"

        '-> Flags d'ouverture
        'UPGRADE_WARNING: FileOpenConstants constante FileOpenConstants.cdlOFNHideReadOnly a �t� mis � niveau vers OpenFileDialog.ShowReadOnly, qui a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
        'UPGRADE_WARNING: MSComDlg.CommonDialog propri�t� Me.CommonDialog1.Flags a �t� mis � niveau vers Me.CommonDialog1Open.ShowReadOnly, qui a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
        'UPGRADE_WARNING: FileOpenConstants constante FileOpenConstants.cdlOFNHideReadOnly a �t� mis � niveau vers OpenFileDialog.ShowReadOnly, qui a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
        Me.CommonDialog1Open.ShowReadOnly = False
        Me.CommonDialog1Open.CheckPathExists = True
        Me.CommonDialog1Save.CheckPathExists = True

        '-> Afficher la boite de dialogue
        Me.CommonDialog1Save.ShowDialog()
        Me.CommonDialog1Open.FileName = Me.CommonDialog1Save.FileName

        '-> V�rifier que le fichier n'existe pas
        'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        If Dir(Me.CommonDialog1Open.FileName, FileAttribute.Normal) <> "" Then
            Rep = MsgBox("Le fichier sp�cifi� : " & Me.CommonDialog1Open.FileName & " existe d�ja. Ecraser le fichier", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNoCancel, "Confirmation")
            If Rep = MsgBoxResult.Cancel Or Rep = MsgBoxResult.No Then Exit Sub
            '-> Supprimer le fichier sp�cifi�
            Kill(Me.CommonDialog1Open.FileName)
        End If

        '-> Enregistrer le fichier
        Me.RichTextBox1.SaveFile(Me.CommonDialog1Open.FileName)

        '-> Setting de la variable de fichier en cours
        CurrentFile = Me.CommonDialog1Open.FileName

        '-> Changer le caption de la fen�tre
        Me.Text = CurrentFile

        '-> Indiquer que cela est enregistr�
        IsSaved = True

        '-> Quitter la fonction
        Exit Sub

GestError:

        If Err.Number = 32755 Then Exit Sub

        MsgBox("Impossible d'enregistrer le fichier", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Erreur")

    End Sub

    Private Sub RichTextBox1_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles RichTextBox1.TextChanged

        IsSaved = False

    End Sub

    Private Sub RichTextBox1_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles RichTextBox1.KeyDown
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000

        '-> Quitter si pas F3
        If KeyCode <> 114 Then Exit Sub

        '-> Quitter si pas de recherche
        If FindWord = "" Then Exit Sub

        '-> Lancer la recherche
        If Me.RichTextBox1.SelectionLength <> 0 Then
            Me.RichTextBox1.SelectionStart = Me.RichTextBox1.SelectionStart + Me.RichTextBox1.SelectionLength
        End If
        'Me.RichTextBox1.Find(FindWord, Me.RichTextBox1.SelectionStart)


    End Sub
End Class