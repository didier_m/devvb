Option Strict Off
Option Explicit On
Module Unzip
	
	'-- C Style argv
	Private Structure UNZIPnames
		<VBFixedArray(99)> Dim uzFiles() As String
		
		'UPGRADE_TODO: "Initialize" doit �tre appel� pour initialiser les instances de cette structure. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
		Public Sub Initialize()
			ReDim uzFiles(99)
		End Sub
	End Structure
	
	'-- Callback Large "String"
	Private Structure UNZIPCBChar
		<VBFixedArray(32800)> Dim ch() As Byte
		
		'UPGRADE_TODO: "Initialize" doit �tre appel� pour initialiser les instances de cette structure. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
		Public Sub Initialize()
			ReDim ch(32800)
		End Sub
	End Structure
	
	'-- Callback Small "String"
	Private Structure UNZIPCBCh
		<VBFixedArray(256)> Dim ch() As Byte
		
		'UPGRADE_TODO: "Initialize" doit �tre appel� pour initialiser les instances de cette structure. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
		Public Sub Initialize()
			ReDim ch(256)
		End Sub
	End Structure
	
	'-- UNZIP32.DLL DCL Structure
	Private Structure DCLIST
		Dim ExtractOnlyNewer As Integer ' 1 = Extract Only Newer/New, Else 0
		Dim SpaceToUnderscore As Integer ' 1 = Convert Space To Underscore, Else 0
		Dim PromptToOverwrite As Integer ' 1 = Prompt To Overwrite Required, Else 0
		Dim fQuiet As Integer ' 2 = No Messages, 1 = Less, 0 = All
		Dim ncflag As Integer ' 1 = Write To Stdout, Else 0
		Dim ntflag As Integer ' 1 = Test Zip File, Else 0
		Dim nvflag As Integer ' 0 = Extract, 1 = List Zip Contents
		Dim nfflag As Integer ' 1 = Extract Only Newer Over Existing, Else 0
		Dim nzflag As Integer ' 1 = Display Zip File Comment, Else 0
		Dim ndflag As Integer ' 1 = Honor Directories, Else 0
		Dim noflag As Integer ' 1 = Overwrite Files, Else 0
		Dim naflag As Integer ' 1 = Convert CR To CRLF, Else 0
		Dim nZIflag As Integer ' 1 = Zip Info Verbose, Else 0
		Dim C_flag As Integer ' 1 = Case Insensitivity, 0 = Case Sensitivity
		Dim fPrivilege As Integer ' 1 = ACL, 2 = Privileges
		Dim Zip As String ' The Zip Filename To Extract Files
		Dim ExtractDir As String ' The Extraction Directory, NULL If Extracting To Current Dir
	End Structure
	
	'-- UNZIP32.DLL Userfunctions Structure
	Private Structure USERFUNCTION
        Dim UZDLLPrnt As UZDLLPrntDelegate ' Pointer To Apps Print Function
		Dim UZDLLSND As Integer ' Pointer To Apps Sound Function
        Dim UZDLLREPLACE As UZDLLRepDelegate ' Pointer To Apps Replace Function
        Dim UZDLLPASSWORD As UZDLLPassDelegate  ' Pointer To Apps Password Function
        Dim UZDLLMESSAGE As UZReceiveDLLMessageDelegate  ' Pointer To Apps Message Function
        Dim UZDLLSERVICE As UZDLLServDelegate  ' Pointer To Apps Service Function (Not Coded!)
		Dim TotalSizeComp As Integer ' Total Size Of Zip Archive
		Dim TotalSize As Integer ' Total Size Of All Files In Archive
		Dim CompFactor As Integer ' Compression Factor
		Dim NumMembers As Integer ' Total Number Of All Files In The Archive
		Dim cchComment As Short ' Flag If Archive Has A Comment!
	End Structure
	
	'-- UNZIP32.DLL Version Structure
	Private Structure UZPVER
		Dim structlen As Integer ' Length Of The Structure Being Passed
		Dim flag As Integer ' Bit 0: is_beta  bit 1: uses_zlib
		'UPGRADE_WARNING: La taille de la cha�ne de longueur fixe doit tenir dans la m�moire tampon. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
		<VBFixedString(10),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray,SizeConst:=10)> Public beta() As Char ' e.g., "g BETA" or ""
		'UPGRADE_WARNING: La taille de la cha�ne de longueur fixe doit tenir dans la m�moire tampon. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
		'UPGRADE_NOTE: datea �t� mis � niveau vers date_Renamed. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		<VBFixedString(20),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray,SizeConst:=20)> Public date_Renamed() As Char ' e.g., "4 Sep 95" (beta) or "4 September 1995"
		'UPGRADE_WARNING: La taille de la cha�ne de longueur fixe doit tenir dans la m�moire tampon. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
		<VBFixedString(10),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray,SizeConst:=10)> Public zlib() As Char ' e.g., "1.0.5" or NULL
		'UPGRADE_NOTE: Unzipa �t� mis � niveau vers Unzip_Renamed. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		<VBFixedArray(4)> Dim Unzip_Renamed() As Byte ' Version Type Unzip
		<VBFixedArray(4)> Dim zipinfo() As Byte ' Version Type Zip Info
		Dim os2dll As Integer ' Version Type OS2 DLL
		<VBFixedArray(4)> Dim windll() As Byte ' Version Type Windows DLL
		
		'UPGRADE_TODO: "Initialize" doit �tre appel� pour initialiser les instances de cette structure. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
		Public Sub Initialize()
			'UPGRADE_WARNING: La limite inf�rieure du tableau Unzip_Renamed est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
			'UPGRADE_NOTE: Unzipa �t� mis � niveau vers Unzip_Renamed. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			ReDim Unzip_Renamed(4)
			'UPGRADE_WARNING: La limite inf�rieure du tableau zipinfo est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
			ReDim zipinfo(4)
			'UPGRADE_WARNING: La limite inf�rieure du tableau windll est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
			ReDim windll(4)
		End Sub
	End Structure
	
	'-- This Assumes UNZIP32.DLL Is In Your \Windows\System Directory!
	'UPGRADE_WARNING: La structure USERFUNCTION peut n�cessiter que des attributs de marshaling soient pass�s en tant qu'argument dans cette instruction Declare. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
	'UPGRADE_WARNING: La structure DCLIST peut n�cessiter que des attributs de marshaling soient pass�s en tant qu'argument dans cette instruction Declare. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
	'UPGRADE_WARNING: La structure UNZIPnames peut n�cessiter que des attributs de marshaling soient pass�s en tant qu'argument dans cette instruction Declare. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
	'UPGRADE_WARNING: La structure UNZIPnames peut n�cessiter que des attributs de marshaling soient pass�s en tant qu'argument dans cette instruction Declare. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
	Private Declare Function Wiz_SingleEntryUnzip Lib "unzip32.dll" (ByVal ifnc As Integer, ByRef ifnv As UNZIPnames, ByVal xfnc As Integer, ByRef xfnv As UNZIPnames, ByRef dcll As DCLIST, ByRef Userf As USERFUNCTION) As Integer

	'UPGRADE_WARNING: La structure UZPVER peut n�cessiter que des attributs de marshaling soient pass�s en tant qu'argument dans cette instruction Declare. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
	Private Declare Sub UzpVersion2 Lib "unzip32.dll" (ByRef uzpv As UZPVER)
	
	'-- Private Variables For Structure Access
	Private UZDCL As DCLIST
	Private UZUSER As USERFUNCTION
	'UPGRADE_WARNING: Il peut �tre n�cessaire d'initialiser les tableaux de la structure UZVER avant de les utiliser. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
	Private UZVER As UZPVER
	
	'-- Public Variables For Setting The
	'-- UNZIP32.DLL DCLIST Structure
	'-- These Must Be Set Before The Actual Call To VBUnZip32
	Public uExtractOnlyNewer As Short ' 1 = Extract Only Newer/New, Else 0
	Public uSpaceUnderScore As Short ' 1 = Convert Space To Underscore, Else 0
	Public uPromptOverWrite As Short ' 1 = Prompt To Overwrite Required, Else 0
	Public uQuiet As Short ' 2 = No Messages, 1 = Less, 0 = All
	Public uWriteStdOut As Short ' 1 = Write To Stdout, Else 0
	Public uTestZip As Short ' 1 = Test Zip File, Else 0
	Public uExtractList As Short ' 0 = Extract, 1 = List Contents
	Public uFreshenExisting As Short ' 1 = Update Existing by Newer, Else 0
	Public uDisplayComment As Short ' 1 = Display Zip File Comment, Else 0
	Public uHonorDirectories As Short ' 1 = Honor Directories, Else 0
	Public uOverWriteFiles As Short ' 1 = Overwrite Files, Else 0
	Public uConvertCR_CRLF As Short ' 1 = Convert CR To CRLF, Else 0
	Public uVerbose As Short ' 1 = Zip Info Verbose
	Public uCaseSensitivity As Short ' 1 = Case Insensitivity, 0 = Case Sensitivity
	Public uPrivilege As Short ' 1 = ACL, 2 = Privileges, Else 0
	Public uZipFileName As String ' The Zip File Name
	Public uExtractDir As String ' Extraction Directory, Null If Current Directory
	
	'-- Public Program Variables
	Public uZipNumber As Integer ' Zip File Number
	Public uNumberFiles As Integer ' Number Of Files
	Public uNumberXFiles As Integer ' Number Of Extracted Files
	Public uZipMessage As String ' For Zip Message
	Public uZipInfo As String ' For Zip Information
	Public uZipInfo2 As String ' For Zip Information
	'UPGRADE_WARNING: Il peut �tre n�cessaire d'initialiser les tableaux de la structure uZipNames avant de les utiliser. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
    Private uZipNames As UNZIPnames ' Names Of Files To Unzip
	'UPGRADE_WARNING: Il peut �tre n�cessaire d'initialiser les tableaux de la structure uExcludeNames avant de les utiliser. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
    Private uExcludeNames As UNZIPnames ' Names Of Zip Files To Exclude
	Public uVbSkip As Short ' For DLL Password Function
	
	
	
	'-- Puts A Function Pointer In A Structure
	'-- For Callbacks.
    Public Function FnPtr(ByVal lp As Integer) As Integer

        FnPtr = lp

    End Function
	
	'-- Callback For UNZIP32.DLL - Receive Message Function
    Private Delegate Sub UZReceiveDLLMessageDelegate(ByVal ucsize As Integer, ByVal csiz As Integer, ByVal cfactor As Short, ByVal mo As Short, ByVal dy As Short, ByVal yr As Short, ByVal hh As Short, ByVal mm As Short, ByVal c As Byte, ByRef fname As UNZIPCBCh, ByRef meth As UNZIPCBCh, ByVal crc As Integer, ByVal fCrypt As Byte)
    Private Sub UZReceiveDLLMessage(ByVal ucsize As Integer, ByVal csiz As Integer, ByVal cfactor As Short, ByVal mo As Short, ByVal dy As Short, ByVal yr As Short, ByVal hh As Short, ByVal mm As Short, ByVal c As Byte, ByRef fname As UNZIPCBCh, ByRef meth As UNZIPCBCh, ByVal crc As Integer, ByVal fCrypt As Byte)

        Dim s0 As String
        Dim xx As Integer
        Dim strout As New VB6.FixedLengthString(80)

        '-- Always Put This In Callback Routines!
        On Error Resume Next

        '------------------------------------------------
        '-- This Is Where The Received Messages Are
        '-- Printed Out And Displayed.
        '-- You Can Modify Below!
        '------------------------------------------------

        strout.Value = Space(80)

        '-- For Zip Message Printing
        If uZipNumber = 0 Then
            Mid(strout.Value, 1, 50) = "Filename:"
            Mid(strout.Value, 53, 4) = "Size"
            Mid(strout.Value, 62, 4) = "Date"
            Mid(strout.Value, 71, 4) = "Time"
            uZipMessage = strout.Value & vbNewLine
            strout.Value = Space(80)
        End If

        s0 = ""

        '-- Do Not Change This For Next!!!
        For xx = 0 To 255
            If fname.ch(xx) = 0 Then Exit For
            s0 = s0 & Chr(fname.ch(xx))
        Next

        '-- Assign Zip Information For Printing
        Mid(strout.Value, 1, 50) = Mid(s0, 1, 50)
        Mid(strout.Value, 51, 7) = Right("        " & CStr(ucsize), 7)
        Mid(strout.Value, 60, 3) = Right("0" & Trim(CStr(mo)), 2) & "/"
        Mid(strout.Value, 63, 3) = Right("0" & Trim(CStr(dy)), 2) & "/"
        Mid(strout.Value, 66, 2) = Right("0" & Trim(CStr(yr)), 2)
        Mid(strout.Value, 70, 3) = Right(Str(hh), 2) & ":"
        Mid(strout.Value, 73, 2) = Right("0" & Trim(CStr(mm)), 2)

        ' Mid(strout, 75, 2) = Right$(" " & CStr(cfactor), 2)
        ' Mid(strout, 78, 8) = Right$("        " & CStr(csiz), 8)
        ' s0 = ""
        ' For xx = 0 To 255
        '     If meth.ch(xx) = 0 Then Exit For
        '     s0 = s0 & Chr$(meth.ch(xx))
        ' Next xx

        '-- Do Not Modify Below!!!
        uZipMessage = uZipMessage & strout.Value & vbNewLine
        uZipNumber = uZipNumber + 1

    End Sub
	
	'-- Callback For UNZIP32.DLL - Print Message Function
    Private Delegate Function UZDLLPrntDelegate(ByRef fname As UNZIPCBChar, ByVal X As Integer) As Integer
    Private Function UZDLLPrnt(ByRef fname As UNZIPCBChar, ByVal X As Integer) As Integer

        Dim s0 As String
        Dim xx As Integer

        '-- Always Put This In Callback Routines!
        On Error Resume Next

        s0 = ""

        '-- Gets The UNZIP32.DLL Message For Displaying.
        For xx = 0 To X - 1
            If fname.ch(xx) = 0 Then Exit For
            s0 = s0 & Chr(fname.ch(xx))
        Next

        '-- Assign Zip Information
        If Mid(s0, 1, 1) = vbLf Then s0 = vbNewLine ' Damn UNIX :-)
        uZipInfo2 = uZipInfo2 & s0

        If uZipInfo = "" Then uZipInfo = s0

        UZDLLPrnt = 0

    End Function
	
	'-- Callback For UNZIP32.DLL - DLL Service Function
    Private Delegate Function UZDLLServDelegate(ByRef mname As UNZIPCBChar, ByVal X As Integer) As Integer
    Private Function UZDLLServ(ByRef mname As UNZIPCBChar, ByVal X As Integer) As Integer

        Dim s0 As String
        Dim xx As Integer

        '-- Always Put This In Callback Routines!
        On Error Resume Next

        s0 = ""
        '-- Get Zip32.DLL Message For processing
        For xx = 0 To X - 1
            If mname.ch(xx) = 0 Then Exit For
            s0 = s0 & Chr(mname.ch(xx))
        Next
        ' At this point, s0 contains the message passed from the DLL
        ' It is up to the developer to code something useful here :)
        UZDLLServ = 0 ' Setting this to 1 will abort the zip!

    End Function
	
    '-- Callback For UNZIP32.DLL - Password Function
    Private Delegate Function UZDLLPassDelegate(ByRef p As UNZIPCBCh, ByVal n As Integer, ByRef m As UNZIPCBCh, ByRef Name As UNZIPCBCh) As Short
    Private Function UZDLLPass(ByRef p As UNZIPCBCh, ByVal n As Integer, ByRef m As UNZIPCBCh, ByRef Name As UNZIPCBCh) As Short

        Dim prompt As String = ""
        Dim xx As Short
        Dim szpassword As String = ""

        '-- Always Put This In Callback Routines!
        On Error Resume Next

        UZDLLPass = 1

        If uVbSkip = 1 Then Exit Function

        '-- Get The Zip File Password
        szpassword = InputBox("Please Enter The Password!")

        '-- No Password So Exit The Function
        If Len(szpassword) = 0 Then
            uVbSkip = 1
            Exit Function
        End If

        '-- Zip File Password So Process It
        For xx = 0 To 255
            If m.ch(xx) = 0 Then
                Exit For
            Else
                prompt = prompt & Chr(m.ch(xx))
            End If
        Next

        For xx = 0 To n - 1
            p.ch(xx) = 0
        Next

        For xx = 0 To Len(szpassword) - 1
            p.ch(xx) = Asc(Mid(szpassword, xx + 1, 1))
        Next

        p.ch(xx) = 0 ' Put Null Terminator For C

        UZDLLPass = 0

    End Function
	
	'-- Callback For UNZIP32.DLL - Report Function To Overwrite Files.
	'-- This Function Will Display A MsgBox Asking The User
	'-- If They Would Like To Overwrite The Files.
    Private Delegate Function UZDLLRepDelegate(ByRef fname As UNZIPCBChar) As Integer
    Private Function UZDLLRep(ByRef fname As UNZIPCBChar) As Integer

        Dim s0 As String
        Dim xx As Integer

        '-- Always Put This In Callback Routines!
        On Error Resume Next

        'THIERRY : on ne pose la question on ecrase syst�matiquement

        '  UZDLLRep = 100 ' 100 = Do Not Overwrite - Keep Asking User
        '  s0 = ""
        '
        '  For xx = 0 To 255
        '    If fname.ch(xx) = 0 Then Exit For
        '    s0 = s0 & Chr$(fname.ch(xx))
        '  Next
        '
        '  '-- This Is The MsgBox Code
        '  xx = MsgBox("Overwrite " & s0 & "?", vbExclamation & vbYesNoCancel, _
        ''              "VBUnZip32 - File Already Exists!")
        '
        '  If xx = vbNo Then Exit Function
        '
        '  If xx = vbCancel Then
        '    UZDLLRep = 104       ' 104 = Overwrite None
        '    Exit Function
        '  End If

        UZDLLRep = 102 ' 102 = Overwrite, 103 = Overwrite All

    End Function
	
	'-- ASCIIZ To String Function
	Public Function szTrim(ByRef szString As String) As String
		
		Dim pos As Integer
		
		pos = InStr(szString, vbNullChar)
		
		Select Case pos
			Case Is > 1
				szTrim = Trim(Left(szString, pos - 1))
			Case 1
				szTrim = ""
			Case Else
				szTrim = Trim(szString)
		End Select
		
	End Function
	
	'-- Main UNZIP32.DLL UnZip32 Subroutine
	'-- (WARNING!) Do Not Change!
	Public Function VBUnZip32() As Integer
		
        Dim retcode As Integer

        '-- Set The UNZIP32.DLL Options
        '-- (WARNING!) Do Not Change
        UZDCL.ExtractOnlyNewer = uExtractOnlyNewer ' 1 = Extract Only Newer/New
        UZDCL.SpaceToUnderscore = uSpaceUnderScore ' 1 = Convert Space To Underscore
        UZDCL.PromptToOverwrite = uPromptOverWrite ' 1 = Prompt To Overwrite Required
        UZDCL.fQuiet = uQuiet ' 2 = No Messages 1 = Less 0 = All
        UZDCL.ncflag = uWriteStdOut ' 1 = Write To Stdout
        UZDCL.ntflag = uTestZip ' 1 = Test Zip File
        UZDCL.nvflag = uExtractList ' 0 = Extract 1 = List Contents
        UZDCL.nfflag = uFreshenExisting ' 1 = Update Existing by Newer
        UZDCL.nzflag = uDisplayComment ' 1 = Display Zip File Comment
        UZDCL.ndflag = uHonorDirectories ' 1 = Honour Directories
        UZDCL.noflag = uOverWriteFiles ' 1 = Overwrite Files
        UZDCL.naflag = uConvertCR_CRLF ' 1 = Convert CR To CRLF
        UZDCL.nZIflag = uVerbose ' 1 = Zip Info Verbose
        UZDCL.C_flag = uCaseSensitivity ' 1 = Case insensitivity, 0 = Case Sensitivity
        UZDCL.fPrivilege = uPrivilege ' 1 = ACL 2 = Priv
        UZDCL.Zip = uZipFileName ' ZIP Filename
        UZDCL.ExtractDir = uExtractDir ' Extraction Directory, NULL If Extracting
        ' To Current Directory

        '-- Set Callback Addresses
        '-- (WARNING!!!) Do Not Change
        'UPGRADE_WARNING: Ajouter un d�l�gu� pour AddressOf UZDLLPrnt Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="E9E157F7-EF0C-4016-87B7-7D7FBBC6EE08"'
        UZUSER.UZDLLPrnt = AddressOf UZDLLPrnt
        UZUSER.UZDLLSND = 0 '-- Not Supported
        'UPGRADE_WARNING: Ajouter un d�l�gu� pour AddressOf UZDLLRep Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="E9E157F7-EF0C-4016-87B7-7D7FBBC6EE08"'
        UZUSER.UZDLLREPLACE = AddressOf UZDLLRep
        'UPGRADE_WARNING: Ajouter un d�l�gu� pour AddressOf UZDLLPass Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="E9E157F7-EF0C-4016-87B7-7D7FBBC6EE08"'
        UZUSER.UZDLLPASSWORD = AddressOf UZDLLPass
        'UPGRADE_WARNING: Ajouter un d�l�gu� pour AddressOf UZReceiveDLLMessage Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="E9E157F7-EF0C-4016-87B7-7D7FBBC6EE08"'
        UZUSER.UZDLLMESSAGE = AddressOf UZReceiveDLLMessage
        'UPGRADE_WARNING: Ajouter un d�l�gu� pour AddressOf UZDLLServ Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="E9E157F7-EF0C-4016-87B7-7D7FBBC6EE08"'
        UZUSER.UZDLLSERVICE = AddressOf UZDLLServ

        '-- Set UNZIP32.DLL Version Space
        '-- (WARNING!!!) Do Not Change
        With UZVER
            .structlen = Len(UZVER)
            .beta = Space(9) & vbNullChar
            .date_Renamed = Space(19) & vbNullChar
            .zlib = Space(9) & vbNullChar
        End With

        '-- Get Version
        'Call UzpVersion2(UZVER)

        '-- Go UnZip The Files! (Do Not Change Below!!!)
        '-- This Is The Actual UnZip Routine
        retcode = Wiz_SingleEntryUnzip(uNumberFiles, uZipNames, uNumberXFiles, uExcludeNames, UZDCL, UZUSER)
        '--------------------------------------------------------------
        VBUnZip32 = retcode
		
	End Function
	
	
	Public Function GetUnzipFileName(ByRef NomFichier As String) As String
		
		Dim oZip As New CGUnzipFiles
		Dim TestZip As Boolean
		Dim MyFile As String
        Dim AllFile As String = ""
		Dim i As Short
		
        'On Error GoTo GestError
		
		'-> Tester si le fichier est zipp�
		If NomFichier <> "" Then
			'V�rifier si on trouve la librairie Unzip32.dll
			If Not FindZipDll Then GoTo GestError
			'-> Tester si on est sur une archive
			If oZip.TestArchive(NomFichier) Then
				MyFile = Trim(uZipInfo)
				MyFile = Entry(2, MyFile, ":")
				TestZip = oZip.ExtractFile(NomFichier, GetTempFileNameVB("", True))
				If TestZip Then
					If FileExist(GetTempFileNameVB("", True) & Trim(MyFile)) Then
						GetUnzipFileName = GetTempFileNameVB("", True) & Trim(MyFile)
						'-> Maintenant on reccupere tous les nom de fichiers de l archive
						'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(uZipInfo2, OK). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						For i = 1 To NumEntries(uZipInfo2, "OK") - 1
							If AllFile = "" Then
								AllFile = GetTempFileNameVB("", True) & Trim(Entry(2, Entry(i, uZipInfo2, "OK"), ":"))
							Else
								AllFile = AllFile & "," & GetTempFileNameVB("", True) & Trim(Entry(2, Entry(i, uZipInfo2, "OK"), ":"))
							End If
						Next 
						'-> si plusieur fichier on doit en avoir un avec extension turbo
						'-> On pointe sur le premier fichier turbo que l'on trouve
						'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(AllFile, ,). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						For i = 1 To NumEntries(AllFile, ",")
							If UCase(Mid(Entry(i, AllFile, ","), Len(Entry(i, AllFile, ",")) - 4)) = "TURBO" Then
								GetUnzipFileName = Entry(i, AllFile, ",")
								uZipInfo2 = DeleteEntry(AllFile, i, ",")
								Exit For
							End If
						Next 
						Exit Function
					End If
				End If
			Else
				'-> ce n'est une archive
				uZipInfo2 = ""
			End If
		End If
		
GestError: 
		GetUnzipFileName = NomFichier
		
	End Function
	
	Public Function FindZipDll() As Boolean
		
		On Error GoTo GestError
		
		Dim lpBuffer As String
		Dim Res As Integer
		
		lpBuffer = Space(255)
		Res = GetSystemDirectory(lpBuffer, Len(lpBuffer))
		If Res <> 0 Then
			lpBuffer = Mid(lpBuffer, 1, Res)
			'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			If Dir(lpBuffer & "\unzip32.dll") <> "" Then FindZipDll = True
			Exit Function
		End If
		
GestError: 
		FindZipDll = False
		
	End Function
End Module