VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmChxCt 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Choix d'un contexte de travail"
   ClientHeight    =   4485
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9120
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4485
   ScaleWidth      =   9120
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   4455
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9135
      _ExtentX        =   16113
      _ExtentY        =   7858
      _Version        =   393217
      HideSelection   =   0   'False
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3240
      Top             =   360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmChxCt.frx":0000
            Key             =   "Document"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmChxCt.frx":08DA
            Key             =   "Bureau"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmChxCt.frx":0D2C
            Key             =   "Quit"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmChxCt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)


If Node.Key = "QUIT" Then End

If Node.Key = "DOCDEAL" Or Node.Key = "DOCCLIENT" Then Exit Sub

Unload Me

HelpDeal.TreeView1.Nodes(Node.Key).Selected = True
HelpDeal.TreeView1.Nodes(Node.Key).EnsureVisible
HelpDeal.TreeView1_NodeClick HelpDeal.TreeView1.Nodes(Node.Key)
HelpDeal.Visible = True
HelpDeal.WindowState = 2

End Sub
