VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Applicatif"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Name As String
Public Descriptif As String
Public IsLoaded As Boolean
Public IndexMenu As Integer
Public Identifiant As String

Public Liens As Collection
Public Menus As Collection

Private Sub Class_Initialize()

'-> Initialiser les collections
Set Liens = New Collection
Set Menus = New Collection

End Sub
