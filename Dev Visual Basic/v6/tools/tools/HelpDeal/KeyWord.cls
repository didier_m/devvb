VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "KeyWord"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Ce module de classe sert � g�rer les mots cl�s
Public Name As String
Public Contextes As Collection 'Liste des Contextes attach�s � un mot cl�

Private Sub Class_Initialize()

'-> Initialiser la collection des liens attach�s
Set Contextes = New Collection
Index = 1

End Sub

Public Sub ClearContextes()

Do While Contextes.Count <> 0
    Contextes.Remove (1)
Loop

End Sub
