VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSearch 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Recherche"
   ClientHeight    =   6165
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6780
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6165
   ScaleWidth      =   6780
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Caption         =   "Rechercher dans : "
      Height          =   855
      Left            =   0
      TabIndex        =   4
      Top             =   720
      Width           =   6735
      Begin VB.OptionButton Option3 
         Caption         =   "Dans tous les libelles"
         Height          =   255
         Left            =   4800
         TabIndex        =   7
         Top             =   360
         Value           =   -1  'True
         Width           =   1815
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Dans les libelles des documents d'aide "
         Height          =   615
         Left            =   2880
         TabIndex        =   6
         Top             =   200
         Width           =   1815
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Dans les libelles des chapitres"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   360
         Width           =   2535
      End
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   4425
      Left            =   0
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   1680
      Width           =   6735
      _ExtentX        =   11880
      _ExtentY        =   7805
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   6240
      Picture         =   "frmSearch.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Commencer la recherche"
      Top             =   360
      Width           =   375
   End
   Begin VB.TextBox strFind 
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   360
      Width           =   6135
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2160
      Top             =   4800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSearch.frx":054E
            Key             =   "ChapitreFerme"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSearch.frx":1228
            Key             =   "Document"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Texte � rechercher : "
      Height          =   255
      Left            =   60
      TabIndex        =   2
      Top             =   120
      Width           =   1935
   End
End
Attribute VB_Name = "frmSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

Dim aNode As Node
Dim Node As Node
Dim ToAdd As Boolean
Dim DealApp As Applicatif
Dim CliApp As Applicatif
Dim aMenu As MenuDeal

'-> Vider le treeview
Me.TreeView1.Nodes.Clear

'-> Lancer la recherche
For Each aNode In HelpDeal.TreeView1.Nodes
    If InStr(1, UCase$(aNode.Text), UCase$(Trim(Me.strFind.Text))) <> 0 Then
        '-> Tester si on doit ajouter le node
        If Me.Option3.Value Then
            ToAdd = True
        Else
            If Left(aNode.Key, 3) = "CLI" Then 'Si doc client ou doc Deal informatique
                '-> Pointer sur l'applicatif client
                Set CliApp = CliApplicatifs(CurApp)
                '-> Pointer sur le menu associ�
                Set aMenu = CliApp.Menus(Node.Key)
            Else
                '-> Pointer sur l'applicatif associ�
                Set DealApp = Applicatifs(CurApp)
                '-> Pointer sur le menu associ�
                Set aMenu = DealApp.Menus(aNode.Key)
            End If
            '-> Tester si ajouter le node
            If Me.Option1.Value = True Then
                '-> N'affucher que les chapitres
                If aMenu.IsUrl Then
                    ToAdd = False
                Else
                    ToAdd = True
                End If
            Else
                '-> N'afficher que les menus
                If aMenu.IsUrl Then
                    ToAdd = True
                Else
                    ToAdd = False
                End If
            End If
        End If
        
        If ToAdd Then
                
            '-> Ajouter une node
            Set Node = Me.TreeView1.Nodes.Add(, , aNode.Key, aNode.Text)
            If aNode.Tag = "CHAPITRE" Then
                Node.Image = "ChapitreFerme"
            Else
                Node.Image = "Document"
            End If
        End If
    End If
Next 'Pour tous les nodes

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then Unload Me
End Sub

Private Sub strFind_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Command1_Click

End Sub

Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)


Unload Me
HelpDeal.TreeView1.Nodes(Node.Key).Selected = True
HelpDeal.TreeView1.Nodes(Node.Key).EnsureVisible
HelpDeal.TreeView1_NodeClick HelpDeal.TreeView1.SelectedItem
HelpDeal.TreeView1.SetFocus



End Sub
