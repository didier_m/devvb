VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MenuDeal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private pLibelle(1 To 10) As String
Public idLangueOrigine As Integer 'Index de la langue d'origine
'Public aLien As Lien
Public IsUrl As Boolean
Public KeyParent As String
Public Key As String
Public FichierHTML As String

Public Property Get Libelle() As String
    Libelle = pLibelle(CurCodeLangue)
End Property

Public Property Let Libelle(ByVal vNewValue As String)
    pLibelle(CurCodeLangue) = vNewValue
End Property

Public Function GetLbByLangue(ByVal idLangue As Integer) As String

GetLbByLangue = pLibelle(idLangue)

End Function

Public Sub SetLbByLangue(ByVal idLangue As Integer, LibValue As String)

pLibelle(idLangue) = LibValue

End Sub

