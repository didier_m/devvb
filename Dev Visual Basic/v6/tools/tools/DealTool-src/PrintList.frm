VERSION 5.00
Begin VB.Form PrintList 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Impression du document"
   ClientHeight    =   1275
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5700
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1275
   ScaleWidth      =   5700
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Imprimer"
      Height          =   975
      Left            =   4440
      Picture         =   "PrintList.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   120
      Width           =   1215
   End
   Begin VB.ListBox List1 
      Height          =   1230
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   4335
   End
End
Attribute VB_Name = "PrintList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Sel As Integer
Public ToPrint As Integer


Private Sub cmdPrint_Click()

Dim x As Printer
Dim i As Integer

On Error GoTo GestImp

'-> Afficher le sablier pour impression
Me.MousePointer = 11

'-> Faire setting de l'imprimante

For Each x In Printers

    If x.DeviceName = Me.List1.Text Then
    
        Set Printer = x
        Exit For
    
    End If
    
Next
        
'-> Impression du document
If ToPrint = -9999 Then
    frmVisu.RetourImp = "OK"
    Unload Me
    Exit Sub
End If

DealGraph.PrintForm

GestImp:

    Unload Me

End Sub


Private Sub Form_Load()

Dim x As Printer

'---> Ajouter les imprimantes dans la liste

For Each x In Printers

    Me.List1.AddItem x.DeviceName
    
    '---> S�lectionner dans la liste si c'est l'imprimante par d�faut
    
    If x.DeviceName = Printer.DeviceName Then _
                      Me.List1.Selected(Me.List1.ListCount - 1) = True
    
Next




End Sub


Private Sub Form_Unload(Cancel As Integer)
    
    If ToPrint <> -9999 Then End
    
End Sub
