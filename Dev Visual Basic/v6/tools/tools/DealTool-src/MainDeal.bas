Attribute VB_Name = "MainDeal"
'************************************
'* Descriptif des diff�rents outils *
'************************************

'---> Passage des arguments : DealTool.exe [Outils]|[Ligne de commande]


'---> DealAnim
'-> Index : 1

'---> VisuDeal
'-> Index : 2
    
'---> ImgScan
'-> Index : 3

'---> DealGraph
'-> Index : 4

'--->  Excel
'-> Index : 5 Transfert Browse dans Excel

'---> Gestion des formats de cellules pour export vesr EXCEL
'-> Index : 6

'---> Export d'un Browse vers TurboGraph
'-> Index 7

'---> Cr�ation de tableaux de Bords dans Excel : fichiers PAR
'-> Index 8

'---> Cr�ation d'un fichier WORD par fusion d'un fichier de valeur avec un Doc Word qui contient des mots cl�s
'-> index 9

'---> Visualisation d'un fichier EXCEL depuis Progress
'-> Index 10

'---> Visualisation d'un fichier au format RTF dans une fen�tre + Impression du fichier
'-> Index 11

'---> Visualisation d'un fichier au format Internet : necessite IE 5.0
'-> Index 12

'---> Visualisation d'un fichier au format PDF ACROBAT READER 4.05
'-> Index 13


'*************************************************
'* GESTION DES FICHES DEMANDES/EVOLUATION/BOGGUE *
'*************************************************
'D�marr� le 31/08/2000

'Fiche 340, DEAL, EMILIE, V51 : Suppression des fichiers Progress de data lors d'un export _
vers TBG ou EXCEL

'Fiche 377, DEAL, EMILIE, V51 : Outil de visualisation de fichier HTML

'Fiche 5, DEAL,EMILIE, TURBO : Dans le fichier param.ini pour la fusion des documents vers WORD et EXCEL, remplacement des mots cl�s
'Reference par $REFERENCE$
'Destination par $DESTINATION$


Dim LigneCommande As String
Dim IndexOutil As Integer
Dim ParamOutil As String

' API WIN 32
Private Declare Function GetClientRect& Lib "user32" (ByVal Hwnd As Long, lpRect As RECT)
Private Declare Function GetParent& Lib "user32" (ByVal Hwnd As Long)
Private Declare Function SetParent& Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long)
Private Declare Function IsWindow& Lib "user32" (ByVal Hwnd As Long)
Private Declare Function SHGetSpecialFolderLocation Lib "shell32.dll" (ByVal hWndOwner As Long, ByVal nFolder As Long, pidl As ITEMIDLIST) As Long

'Structure Rect
Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Type SHITEMID
    cb As Long
    abID As Byte
End Type

Private Type ITEMIDLIST
    mkid As SHITEMID
End Type

Public aObject As Object
Public DifX As Integer
Public DifY As Integer

Public IsMouchard As Boolean
Public Mouchard As String
Public hdlMouchard As Integer

'-> Indique si on doit utiliser une maquette ou non
Private MaqToPrint As String
Public TailleTotale As Long
Public TailleLue As Long


Sub Main()

Dim aPath As String
Dim TempFile As String
Dim hdlFile As Integer
Dim TempOutil As String
    
    '-> Cette proc�dure est charg�e de d�terminer quel outil est appel�, d'analyser _
    la ligne de commande et faire le setting de l'objet
    
'    '-> Charger la feuille de fin
'    Load frDummy
'
'    '-> Lancer le traceur
'    If Dir$(App.Path & "\Tracker.exe") = "" Then
'        MsgBoxMouchard "Impossible de trouver le fichier Trace : Fin du proframme"
'        End
'    End If
'    If InStr(1, UCase$(App.Path), "\EMILIEGUI\SRC") <> 0 Then
'        aPath = "D:\ERP\TURBO"
'    Else
'        CreatePath aPath
'    End If
'
'    Shell App.Path & "\Tracker.exe " & frDummy.hwnd & "|" & aPath & "\|3", vbNormalFocus
    
    On Error GoTo InterceptError
    
    If InStr(1, Command$, "mouchard", vbTextCompare) <> 0 Then IsMouchard = True
    If IsMouchard Then
        Mouchard = App.Path + "\" + "LogDealTool.txt"
        hdlMouchard = FreeFile
        Open Mouchard For Append As #hdlMouchard
    End If
    
    '-> R�cup�ration de la ligne de commande
    LigneCommande = Command$
    
    If IsMouchard Then Print #hdlMouchard, "Ligne de commande didier: " & Now & " " & LigneCommande
    
    If Trim(LigneCommande) = "" Then GestError "Ligne de commande incorrecte"
            
    '-> Remplacer le "�" par un "|"
    If InStr(1, LigneCommande, "�") <> 0 Then LigneCommande = Replace(LigneCommande, "�", "|")
    If InStr(1, LigneCommande, "'") <> 0 Then LigneCommande = Replace(LigneCommande, "'", "")
    '-> Initialisation de la convertion d�cimale
    InitConvertion
    
    '-> Analyse de la ligne de commande
    TempOutil = Entry(1, LigneCommande, "|")
    If Not IsNumeric(TempOutil) Then
        Select Case UCase$(TempOutil)
            Case "DEALANIM"
                IndexOutil = 1
            Case "VISUDEAL"
                IndexOutil = 2
            Case "IMGSCAN"
                IndexOutil = 3
            Case "DEALGRAPH"
                IndexOutil = 4
            Case "BRWEXCEL"
                IndexOutil = 5
            Case "LIBBRWEXCEL"
                IndexOutil = 6
            Case "BRWTURBOBGRAPH"
                IndexOutil = 7
            Case "TBEXCEL"
                IndexOutil = 8
            Case "DOCWORD"
                IndexOutil = 9
            Case "OPENEXCEL"
                IndexOutil = 10
            Case "OPENRTF"
                IndexOutil = 11
            Case "OPENHTML"
                IndexOutil = 12
            Case "OPENPDF"
                IndexOutil = 13
            Case "DOCOPENWRITER"
                IndexOutil = 16
            Case Else
                MsgBoxMouchard "Outil Deal Inconnu : " & TempOutil, vbCritical + vbOKOnly, "Erreur fatale"
                End
        End Select
    Else
        '-> Index de l'outil
        IndexOutil = CInt(Entry(1, LigneCommande, "|"))
        If IndexOutil > 100 Then
            IndexOutil = IndexOutil - 100
            'IsMouchard = True
            'Mouchard = GetTempFileNameVB("DBG")
            'hdlMouchard = FreeFile
            'Open Mouchard For Output As #hdlMouchard
        End If
    End If
    
    '-> Param�tres de l'outil
    ParamOutil = Entry(2, LigneCommande, "|")
    
    '-> Nom du fichier text � alimenter
'    If NumEntries(LigneCommande, "|") <> 3 Then _
'        GestError "Nombre de param�tre de la ligne de commande incorrecte"
'    TempFile = Entry(3, LigneCommande, "|")
'
'    '-> Ouvrir le fichier pour y �crire le handle
'    hdlFile = FreeFile
'    Open TempFile For Output As #hdlFile
'    Print #hdlFile, frDummy.hwnd
'    Close #hdlFile
        
    '-> Ex�cuter l'outil
    DealExec IndexOutil, ParamOutil
        
Exit Sub

InterceptError:

    GestError "Erreur dans le passage d'argument pour l'outil " & IndexOutil
    End

End Sub

Public Function MsgBoxMouchard(strMessage As String, Optional var, Optional var2, Optional var3, Optional var4) As Variant

If IsMouchard Then
    Print #hdlMouchard, strMessage
    MsgBoxMouchard = ""
'Else
'    MsgBoxMouchard = MsgBox(strMessage, var1, var2, var3, var4)
End If
End Function

Sub GestError(ByVal Libelle As String)

'---> Proc�dure charg�e d'envoyer un message d'erreur � l'utilisateur
If IsMouchard Then Print #hdlMouchard, "Erreur :" + Libelle

'MsgBoxMouchard Libelle, vbCritical + vbOKOnly, "Erreur"
End



End Sub

Public Sub DealExec(ByVal iOutil As Integer, ByVal pOutil As String)

'---> Proc�dure qui lance un outil

'iOutil -> Index de l'outil
'pOutil -> Param�trage de l'outil


Dim NomFic As String 'Fichier �  visualiser pour VisuDeal
Dim TypeFic As Integer 'Type de fichier � visualiser pour VisuDeal
Dim Handle As Long
Dim NomSpool As String
Dim CaptionForm As String

Select Case iOutil

    Case 1 'DealAnim
    
        '-> Test de la ligne de commande
        If NumEntries(pOutil, "@") <> 2 And NumEntries(pOutil, "@") <> 3 Then GestError "Param�tres de l'outil incorrects"
        
        DealAnim.Caption = Entry(1, pOutil, "@")
        DealAnim.Label1.Caption = Entry(2, pOutil, "@")
    
        If NumEntries(pOutil, "@") = 3 Then
            Handle = CLng(Entry(3, pOutil, "@"))
            If IsWindow(Handle) Then SetParent DealAnim.Hwnd, Handle
        End If
                
        '-> Lancement de l'animation
        DealAnim.Show
        
    Case 2 'Visudeal
    
        '-> Test de la ligne de commande
        If NumEntries(pOutil, "@") <> 2 Then GestError "Param�tres de l'outil incorrects"
        
        '-> Analyse du nom de fichier
        NomFic = Entry(1, pOutil, "@")
        If Trim(NomFic) = "" Or Dir$(NomFic) = "" Then GestError "Visudeal : Impossible de trouver le fichier sp�cifi� " & NomFic
        
        '-> Get du Type de fichier
        TypeFic = CInt(Entry(2, pOutil, "@"))
        
        
        '-> Lancement du VisuDeal
        Select Case TypeFic
            Case 1, 2
                VisuDeal.OLE.CreateLink NomFic
                Set aObject = VisuDeal.OLE
                VisuDeal.OLE.Width = VisuDeal.OLE.Width + 567
                VisuDeal.OLE.Visible = True
            Case 3
                VisuDeal.Picture1.Picture = LoadPicture(NomFic)
                VisuDeal.Picture1.Visible = True
                Set aObject = VisuDeal.Picture1
            
                '---> V�rifier si la taille de l'image est < � la taille de l'�cran.
                'Ci c'est le cas ajuster la fen�tre la fen�tre � la taille de l'image
                    
                If VisuDeal.Picture1.Width < Screen.Width And VisuDeal.Picture1.Height < Screen.Height Then
                    VisuDeal.Width = VisuDeal.Picture1.Width + 150
                    VisuDeal.Height = VisuDeal.Picture1.Height + 400
                End If
            
            Case 4
                ShellExecute 0, "OPEN", NomFic, "", "", 1
                End
            Case Else
                GestError "VisuDeal : Type de fichier incorrect " & TypeFic
        End Select
        
        '-> Ajuster la fen�tre
        AjustH
        AjustV
        If VisuDeal.DefH.Visible And VisuDeal.DefV.Visible Then
            VisuDeal.DefH.Width = VisuDeal.DefH.Width - VisuDeal.DefV.Width
            VisuDeal.DefV.Height = VisuDeal.DefV.Height - VisuDeal.DefH.Height
        End If
    
        '-> Lancement du VisuDeal
        VisuDeal.Show

    Case 3 'ImgScan
    
        'DealAnim.ImgScan1.FileType = BMP_Bitmap
        'DealAnim.ImgScan1.ScanTo = DisplayAndFile
        'DealAnim.ImgScan1.Image = pOutil
        'DealAnim.ImgScan1.StartScan
        
    Case 4 'DealGraph
    
        '-> V�rifier que le fichier existe bien
        NomSpool = Entry(1, pOutil, "@")

        '-> Libell� de la fen�tre
        CaptionForm = Entry(2, pOutil, "@")

        If Dir$(NomSpool) = "" Then GestError "Nom ou emplacement de fichier incorrect"

        '-> Lecture et cr�ation du fichier
        AnalyseSpool NomSpool

        If NumEntries(pOutil, "@") = 3 Then
            Handle = CLng(Entry(3, pOutil, "@"))
            If IsWindow(Handle) Then SetParent DealGraph.Hwnd, Handle
            DealGraph.Caption = CaptionForm
            DealGraph.Show
        Else
            DealGraph.Caption = CaptionForm
            DealGraph.Show
        End If
        
    Case 5 'Excel
                
        '-> V�rifier que le fichier d'entete existe
        If Trim(pOutil) = "" Then
            MsgBoxMouchard "Impossible de trouver le fichier ent�te :" & pOutil, vbCritical + vbOKOnly, "Erreur fatale"
            End
        End If
        
        OpenEntete pOutil
        
    Case 6 'Gestionnaire de formats Excel
    
        'EditLib.Show
        
    Case 7 'Transfert d'un browse 7|C:\travailv51\toto.txt
            
        CreateExportBrowse pOutil
        
    Case 8 'Gestion des tableaux de bords depuis un classeur de r�f�rence
    
        'GenerateTableauXls pOutil
    
    Case 9 'Gestion des documents word 9|L:\thierry\Word\toto.txt@L:\thierry\Word\Retour.txt
        MsgBoxMouchard "Gestion des documents"
        RunEnteteDocumentWord pOutil
        
    Case 10
        '-> Visualiser un fichier XLS depuis Progress
        'OpenExcel pOutil
        
    Case 11
        '-> Visualisation d'un fichier au format RTF
        VisuFile pOutil
        
    Case 12
        '-> Visualisation d'un fichier au format HTML
        'VisuFileWeb pOutil
        
    Case 13
        '-> Visualisation d'un fichier au format PDF
        VisuFilePDF pOutil
        
    Case 14
        '-> Lancement d'un applicatif Office
        ExcecApp pOutil
           
    Case 15
        '-> Trier un fichier ASCII
        'GetDirectory pOutil
           
    Case 16 'Gestion des documents write openoffice 9|L:\thierry\Word\toto.txt@L:\thierry\Word\Retour.txt
        'MsgBoxMouchard "Gestion des documents"
        RunEnteteDocumentOpenOffice pOutil
           
End Select 'Selon l'outil

End Sub

Private Sub RunEnteteDocumentWord(FichierEntete As String)

Dim hdlFile As Integer
Dim Ligne As String

'-> V�rifier si on trouve  le fichier sp�cifi�
If Trim(FichierEntete) = "" Then End
If Dir$(FichierEntete) = "" Then End

'-> Cr�er le fichier
hdlFile = FreeFile
'MsgBoxMouchard "FichierEntete : " & FichierEntete

Open FichierEntete For Input As #hdlFile

Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    If Trim(Ligne) = "" Then Exit Do
    '-> Lancer la cr�ation du document Word
    MsgBoxMouchard "RunEnteteDocumentWord Ligne : " & Ligne
    Call CreateDocumentWord(Ligne)
Loop 'Pour tout le fichier

'-> Fermer le fichier
Close #hdlFile

'-> Supprimer le fichier d'entete
Kill FichierEntete

'-> Fin du programme
End


End Sub

Private Sub RunEnteteDocumentOpenOffice(FichierEntete As String)

Dim hdlFile As Integer
Dim Ligne As String

'-> V�rifier si on trouve  le fichier sp�cifi�
If Trim(FichierEntete) = "" Then End
If Dir$(FichierEntete) = "" Then End

'-> Cr�er le fichier
hdlFile = FreeFile
'MsgBoxMouchard "FichierEntete : " & FichierEntete

Open FichierEntete For Input As #hdlFile

Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    If Trim(Ligne) = "" Then Exit Do
    '-> Lancer la cr�ation du document openoffice
    'MsgBoxMouchard "Ligne : " & Ligne
    Call CreateDocumentOpenOffice(Ligne)
Loop 'Pour tout le fichier

'-> Fermer le fichier
Close #hdlFile

'-> Supprimer le fichier d'entete
Kill FichierEntete

'-> Fin du programme
End


End Sub

Private Sub ExcecApp(AppType As String)

Dim aObject As Object

On Error GoTo EndError

Select Case UCase$(Trim(AppType))

    Case "OUTLOOK"
        Set aObject = CreateObject("Outlook.Application")
        aObject.Visible = True
    Case "EXCEL"
        Set aObject = CreateObject("Excel.Application")
        aObject.Workbooks.Add
        aObject.Visible = True
    Case "WORD"
        Set aObject = CreateObject("Word.Application")
        aObject.Application.Documents.Add
        aObject.Visible = True
    Case "IE"
        Set aObject = CreateObject("InternetExplorer.Application.1")
        aObject.Visible = True
    Case Else
        MsgBoxMouchard "Applicatif non r�f�renc� : " & Chr(13) & AppType, vbExclamation + vbOKOnly, "Erreur"
        End
    
End Select

GoTo releaseObject

EndError:
    
    '-> Message d'erreur
    MsgBoxMouchard "Erreur lors du lancement de l'applicatif : " & Chr(13) & AppType, vbCritical + vbOKOnly, "Erreur"
    
releaseObject:
    Set aObject = Nothing
    End

End Sub


Public Sub AjustH()


On Error Resume Next

Dim PartieCachee As Long
Dim aRect As RECT

GetClientRect VisuDeal.Hwnd, aRect

'---> Affichage des barres  de defil

PartieCachee = aObject.Width - VisuDeal.ScaleX(aRect.Right - aRect.Left, 3, 1)
If PartieCachee > 0 Then
    VisuDeal.DefH.Left = 0
    VisuDeal.DefH.Top = VisuDeal.ScaleY(aRect.Bottom - aRect.Top, 3, 1) - VisuDeal.DefH.Height
    VisuDeal.DefH.Width = (VisuDeal.ScaleX(aRect.Right - aRect.Left, 3, 1))
    VisuDeal.DefH.ZOrder
    VisuDeal.DefH.Min = aObject.Left
    VisuDeal.DefH.Max = aObject.Left - PartieCachee - 1000
    VisuDeal.DefH.Visible = True
Else
    VisuDeal.DefH.Visible = False
End If

End Sub
Public Sub AjustV()

Dim PartieCachee As Long
Dim aRect As RECT

On Error Resume Next

GetClientRect VisuDeal.Hwnd, aRect

'---> Affichage des barres  de defil

PartieCachee = aObject.Height - VisuDeal.ScaleY(aRect.Bottom - aRect.Top, 3, 1)
If PartieCachee > 0 Then
    VisuDeal.DefV.Left = VisuDeal.ScaleY(aRect.Right - aRect.Left, 3, 1) - VisuDeal.DefV.Width
    VisuDeal.DefV.Top = 0
    VisuDeal.DefV.Height = (VisuDeal.ScaleX(aRect.Bottom - aRect.Top, 3, 1))
    VisuDeal.DefV.ZOrder
    VisuDeal.DefV.Min = aObject.Top
    VisuDeal.DefV.Max = aObject.Top - PartieCachee - 1000
    VisuDeal.DefV.Visible = True
Else
    Form1.DefV.Visible = False
End If

End Sub

Private Sub AnalyseSpool(ByVal SpoolToAnalyse As String)

'---> Fonction pour DealGraph qui analyse le fichier

Dim NumFic As Integer
Dim Ligne As String
Dim Axe As Object
Dim DefAxe As String
Dim i As Integer
Dim Back As Long

'-> R�cup�ration du dif
DifX = DealGraph.Width - DealGraph.Graph.Width
DifY = DealGraph.Height - DealGraph.Graph.Height

'-> Ouverture du fichier pour analyse
NumFic = FreeFile
Open SpoolToAnalyse For Input As #NumFic

Do While Not EOF(NumFic)

    Line Input #NumFic, Ligne
    If Trim(Ligne) = "" Then
    Else
        '-> tester si premier caract�re <> "\"
        If Mid$(Ligne, 1, 1) <> "\" Then
        Else
            
            Select Case UCase$(Entry(1, Ligne, "�"))
            
                Case "\BACK"
                    DealGraph.BackColor = QBColor(CInt(Trim(Entry(2, Ligne, "�"))))
                        
                Case "\TITRE"
                
                    DealGraph.Graph.TitleText = Trim(Entry(2, Ligne, "�"))
                    
                Case "\NOTE"
                
                    DealGraph.Graph.FootnoteText = Trim(Entry(2, Ligne, "�"))
                                    
                Case "\GRAPH"
                
                    DealGraph.Graph.chartType = CInt(Trim(Entry(2, Ligne, "�")))
                
                Case "\SHOWLEGENDE"
                
                    If UCase$(Trim(Entry(2, Ligne, "�"))) = "OUI" Then
                        DealGraph.Graph.ShowLegend = True
                    Else
                        DealGraph.Graph.ShowLegend = False
                    End If
                
                Case "\TITREAXEX"
                
                    Set Axe = DealGraph.Graph.Plot.Axis(VtChAxisIdX, 1)
                    Axe.AxisTitle = Trim(Entry(2, Ligne, "�"))
                
                Case "\TITREAXEY"
                
                    Set Axe = DealGraph.Graph.Plot.Axis(VtChAxisIdY, 1)
                    Axe.AxisTitle = Trim(Entry(2, Ligne, "�"))
                    Set Axe = Nothing
                                
                Case "\TITREAXEY2"
                
                    Set Axe = DealGraph.Graph.Plot.Axis(VtChAxisIdY2, 1)
                    Axe.AxisTitle = Trim(Entry(2, Ligne, "�"))
                    Set Axe = Nothing
                                
                Case "\TITREAXEZ"
                
                    Set Axe = DealGraph.Graph.Plot.Axis(VtChAxisIdZ, 1)
                    Axe.AxisTitle = Trim(Entry(2, Ligne, "�"))
                    Set Axe = Nothing
                                
                Case "\ECHELLEAXEY"
                
                    DefAxe = Trim(Entry(2, Ligne, "�"))
                    Set Axe = DealGraph.Graph.Plot.Axis(VtChAxisIdY, 1)
                    
                    If UCase$(DefAxe) = "AUTO" Then
                        '-> G�n�ration de l'�chelle en automatique
                        Axe.ValueScale.Auto = True
                    Else
                        Axe.ValueScale.Minimum = CDbl(Entry(1, DefAxe, "|"))
                        Axe.ValueScale.Maximum = CDbl(Entry(2, DefAxe, "|"))
                        Axe.ValueScale.MajorDivision = CInt(Entry(3, DefAxe, "|"))
                        Axe.ValueScale.MinorDivision = CInt(Entry(4, DefAxe, "|"))
                    End If
                    
                    Set Axe = Nothing
                                                        
                Case "\ROWLABEL"
                
                    DealGraph.Graph.RowCount = NumEntries(Trim(Entry(2, Ligne, "�")), "|")
                    For i = 1 To DealGraph.Graph.RowCount
                        DealGraph.Graph.Row = i
                        DealGraph.Graph.RowLabel = Entry(i, Entry(2, Ligne, "�"), "|")
                    Next 'Pour toutes les lignes
                
                Case "\COLUMN"
                    
                    DealGraph.Graph.ColumnCount = DealGraph.Graph.ColumnCount + 1
                    DealGraph.Graph.Column = DealGraph.Graph.ColumnCount
                    DealGraph.Graph.ColumnLabel = Entry(2, Ligne, "�")
                    For i = 1 To DealGraph.Graph.RowCount
                        DealGraph.Graph.Row = i
                        DealGraph.Graph.Data = CLng(Convert(Entry(i, Entry(3, Ligne, "�"), "|")))
                    Next
                    
            End Select
            
        End If 'Si premier caract�re <> "\"
        
    End If 'Si ligne = ""

Loop 'Boucle d'analyse


End Sub

Public Sub PrintFichier(ByVal FicSpool As String)

'---> Fonction qui cr�er une maquette , un fichier de transition et envoit le tout _
� l'�diteur de maquette TurboGraph ANCIENNE VERSION

'---> Attention : toujours imprimer en bas droite : _
[Page - x] + [Jour (jj/mm/aa)] + [Heure (hh:mm)]


Dim NumFicSpool As Integer
Dim Ligne As String

Dim PathExe As String '-> %%GUI%%[Turbograph]

Dim nbCol As Integer '-> %%COL%%[NBCOL]

Dim FindEntete As Boolean
Dim DefEntete As String

Dim DefCol As String '-> Variable Tempo pour r�cup�ration de la Def d'une colonne
Dim LargeurCol() As Double
Dim ContenuCol() As String
Dim FormatCol() As Integer

Dim LargeurTableau As Single
Dim OrientationPapier As Integer
Dim HauteurDoc As Single
Dim LargeurDoc As Single
Dim HauteurPage As Single

Dim IsTitre As Boolean '-> %%TIT%%[Titre]
Dim Titre As String

Dim IsReport As Boolean '->%%REP%%[Texte du report]
Dim Report As String

Dim i As Integer, j As Integer

Dim StringRef As String '-> Variable utilis�e pour convertion de Char en CM

Dim MaxLig As Integer '-> D�termine le nombre max de ligne de d�tail � imprimer sur une feuille

Dim MaquetteFic As Integer
Dim TmpMaquette As String
Dim FichierFic As Integer
Dim TmpFichier As String

Dim TempDirectory As String

Dim Res As Long
Dim StrChamps As String
Dim LigneDetail As String
Dim LignePied As String

Dim AlignLeft As Integer

Const DefCelluleTitre = "5;CONTENU;LARGEUR;Faux,Faux,Faux,Faux;Times New Roman;10;Vrai;Faux;Faux;0;16777215;FAUX;0@0"
Const DefCelluleEntete = "5;CONTENU;LARGEUR;Vrai,Vrai,Vrai,Vrai;Times New Roman;10;Vrai;Faux;Faux;0;13158600;FAUX;0@0"
Const DefCelluleDetail = "4;CONTENU;LARGEUR;Faux,Faux,Vrai,Vrai;Times New Roman;10;Faux;Faux;Faux;0;16777215;FAUX;0@0"
Const DefCelluleBasTableau = "5;;LARGEUR;Faux,Vrai,Vrai,Vrai;Times New Roman;10;Faux;Faux;Faux;0;16777215;FAUX;0@0"
Const DefCelluleReport = "4;CONTENU;LARGEUR;Faux,Faux,Faux,Faux;Times New Roman;10;Vrai;Faux;Faux;0;16777215;FAUX;0@0"
Const DefCellulePied = "4;CONTENU;LARGEUR;Vrai,Vrai,Vrai,Vrai;Times New Roman;10;Vrai;Faux;Faux;0;16777215;FAUX;0@0"
Const DefCellulePage = "9;Edit� le ^DATE       - Page ^NUMP  - ;LARGEUR;Faux,Faux,Faux,Faux;Times New Roman;10;Faux;Faux;Faux;0;16777215;FAUX;0@0"

Dim DefCellule As String

'-> Pour gestion des param�tres r�gionnaux
Dim lpBuffer As String
Dim OldFormat As String
Dim RestaureNumericFormat As Boolean
Dim OldSepMil As String
Dim RestaureMilierFormat As Boolean


On Error GoTo GestError

'-> Modificatrion des param�tres r�gionnaux
'lpBuffer = Space$(10)
'Res = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, 14, lpBuffer, Len(lpBuffer))
'lpBuffer = Mid$(lpBuffer, 1, Res - 1)
'If lpBuffer <> "." Then
'    Res = SetLocaleInfo&(LOCALE_SYSTEM_DEFAULT, 14, ".")
'    '-> indiquer qu'il faut restaurer les param�tres en fin
'    OldFormat = lpBuffer
'    RestaureNumericFormat = True
'End If

'-> V�rification du s�parteur de milier qui doit �tre une ","
'ErrorLibelle = "Pb dans la r�cup�ration du s�parateur de miliers"
'lpBuffer = Space$(10)
'Res = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STHOUSAND, lpBuffer, Len(lpBuffer))
'lpBuffer = Mid$(lpBuffer, 1, Res - 1)
'If lpBuffer <> "," Then
'    Res = SetLocaleInfo&(LOCALE_SYSTEM_DEFAULT, LOCALE_STHOUSAND, ",")
'    '-> indiquer qu'il faut restaurer les param�tres en fin
'    OldSepMil = lpBuffer
'    RestaureMilierFormat = True
'End If

'-> Supprimer la maquette � utiliser s'il y en a une
MaqToPrint = ""

'*******************************
'* ANALYSE DU FICHIER D'ENTREE *
'*******************************

'-> V�rifier dans un premier temps que le fichier est existant
If Dir$(FicSpool) = "" Then GoTo GestError

'-> R�cup�ration d'un num�ro de fichier
NumFicSpool = FreeFile

'-> Ouverture pour lecture du fichier
Open FicSpool For Input As #NumFicSpool

'-> Lecture de toutes les lignes commenceant par %% pour cr�er le document g�n�ral
Do While Not EOF(NumFicSpool)

    '-> Lecture de la ligne
    Line Input #NumFicSpool, Ligne
    
    '-> Tester si deux premiers caract�res =  %%
    If Mid$(Ligne, 1, 2) = "%%" Then
    
        Select Case UCase$(Mid$(Ligne, 1, 7))
        
            Case "%%GUI%%"
            
                '-> R�cup�ration du path du TurboGraph
                PathExe = Mid$(Ligne, 8, Len(Ligne) - 7)
                
                '-> Tester si on trouve le path
                If Dir$(PathExe) = "" Then
                    '->Fermer le fichier
                    Close #NumFicSpool
                    '-> Quitter
                    Exit Sub
                End If
                
            Case "%%MAQ%%"
            
                '-> R�cup�ration de la maquette � utiliser pour l'impression
                MaqToPrint = Mid$(Ligne, 8, Len(Ligne) - 7)
            
            Case "%%COL%%"
            
                '-> R�cup�ration du nombre de colonne
                nbCol = CInt(Mid$(Ligne, 8, Len(Ligne) - 7))
            
            Case "%%TIT%%"
            
                '-> R�cup�ration du titre
                Titre = Mid$(Ligne, 8, Len(Ligne) - 7)
                
                '-> Tester si titre actif
                If Trim(Titre) <> "" Then IsTitre = True
                                
            Case "%%REP%%"
                
                '-> R�cup�ration du report de page
                Report = Mid$(Ligne, 8, Len(Ligne) - 7)
                
                '-> Tester si report actif
                If Trim(Report) <> "" Then IsReport = True
                                
        End Select
    
    Else
    
        '-> Il faut maintenant rechercher la ligne de d�finition d'entete
        If UCase$(Mid$(Ligne, 1, 8)) = "{ENTETE}" Then
            FindEntete = True
            Exit Do
        End If
    End If

Loop 'Boucle principale

'-> Tester ici si on a trouver l'entete du tableau
If Not FindEntete Then
    Close #NumFicSpool
    Exit Sub
End If

'-> R�cup�ration de la d�finition  de l'entete
DefEntete = Mid$(Ligne, 10, Len(Ligne) - 10)
If Trim(DefEntete) = "" Then
    Close #NumFicSpool
    GoTo GestError
End If

'-> R�cup�ration du contenu et de la largeur des colonnes d'entete
For i = 1 To nbCol
    '-> R�cup�ration de la d�finition de la colonne
    DefCol = Entry(i, DefEntete, "|")
    '-> Redim des matrices
    ReDim Preserve LargeurCol(1 To i)
    ReDim Preserve ContenuCol(1 To i)
    '-> R�cup�rartion du contenu
    strTemp = Entry(1, Entry(2, DefCol, "#"), ".")
    'strTemp = Replace(strTemp, ".", SepDeci)
    LargeurCol(i) = CSng(strTemp) + 1
    ContenuCol(i) = Entry(1, DefCol, "#")
    '-> R�cup�ration du contenu de formattage de la colonne
    
Next 'pour toutes les colonnes

'-> Calcul de la largeur du tableau : Attention : les largeurs sont exprime�s en caracteres _
il faut passer par une convertion via une police � chasse fixe

For i = 1 To nbCol
    '-> Cr�ation d'une chaine de caract�re pour r�cup�ration de la largeur en CM
    StringRef = String$(LargeurCol(i), "a")
    '-> R�cup�ration de la largeur de la colonne
    LargeurCol(i) = CDbl(VisuDeal.ScaleX(VisuDeal.TextWidth(StringRef), 1, 7))
    '-> Cumul pour largeur du tableau
    LargeurTableau = LargeurTableau + LargeurCol(i)
Next

'-> Calcul de l'orientation du tableau :
'-> Si largeur > 21 cm orientation paysage

LargeurDoc = 21
HauteurDoc = 29.7

If LargeurTableau + 1 > 21 Then
    '-> Paysage
    OrientationPapier = 0
    HauteurPage = 21
Else
    '-> Portrait
    OrientationPapier = 1
    HauteurPage = 29.7
End If

'*******************************
'* CREATION DE LA MAQUETTE     *
'*******************************

'-> Obtenir l'emplacement des fichiers temporaires de windows
TempDirectory = Space$(255)
Res = GetTempPath(255, TempDirectory)

'-> Obrenir un nom de fichier Tempo pour la maquette
TmpMaquette = Space$(255)
Res = GetTempFileName(TempDirectory, "VIS", 0, TmpMaquette)

'-> Tester les retours
If Trim(TmpMaquette) = "" Then
    Reset
    Exit Sub
End If

'-> Ouverture du fichier maquette
MaquetteFic = FreeFile
Open TmpMaquette For Output As #MaquetteFic

'-> Enregistrer l'entete de la maquette
Print #MaquetteFic, "\DefEntete�Begin"
Print #MaquetteFic, "\Hauteur�" & Replace(CStr(HauteurDoc), ",", ".")
Print #MaquetteFic, "\Largeur�" & Replace(CStr(LargeurDoc), ",", ".")
Print #MaquetteFic, "\MargeTop�1"
Print #MaquetteFic, "\MargeLeft�1"
Print #MaquetteFic, "\Orientation�" & OrientationPapier
Print #MaquetteFic, "\Suite�"
Print #MaquetteFic, "\Report�"
Print #MaquetteFic, "\Entete�"
Print #MaquetteFic, "\Pied�"
Print #MaquetteFic, "\PageGarde�FAUX"
Print #MaquetteFic, "\PageSelection�FAUX"
Print #MaquetteFic, "\DefEntete�End"

'-> Test sur l'alignelent interne du tableau
If LargeurTableau > LargeurDoc Then
    AlignLeft = 2
Else
    AlignLeft = 3
End If

'-> Enregistrement de l'entete du tableau
Print #MaquetteFic, "[TB-Visualisation]"
Print #MaquetteFic, "\OrientationTB�" & OrientationPapier

'-> Enregistrement de la ligne de titre
If IsTitre Then
    '-> Remplacer les valeurs
    DefCellule = DefCelluleTitre
    DefCellule = Replace(DefCellule, "CONTENU", Titre)
    DefCellule = Replace(DefCellule, "LARGEUR", LargeurDoc)
    '-> Enregistrer la ligne
    Print #MaquetteFic, "\Begin�Titre"
    Print #MaquetteFic, "\AlignTop�1"
    Print #MaquetteFic, "\Top�0"
    Print #MaquetteFic, "\AlignLeft�" & AlignLeft
    Print #MaquetteFic, "\Left�0"
    Print #MaquetteFic, "\Largeur�" & LargeurDoc
    Print #MaquetteFic, "\Hauteur�1"
    Print #MaquetteFic, "\Distance�0"
    Print #MaquetteFic, "\Ligne�1"
    Print #MaquetteFic, "\Colonne�1"
    Print #MaquetteFic, "\Col�1�" & DefCellule
    Print #MaquetteFic, "\Champs�"
    Print #MaquetteFic, "\End�Titre"
End If

'-> Enregistrement de la ligne d'entete
Print #MaquetteFic, "\Begin�Entete"
Print #MaquetteFic, "\AlignTop�1"
Print #MaquetteFic, "\Top�0"
Print #MaquetteFic, "\AlignLeft�" & AlignLeft
Print #MaquetteFic, "\Left�0"
Print #MaquetteFic, "\Largeur�" & Replace(CStr(LargeurTableau), ",", ".")
Print #MaquetteFic, "\Hauteur�1"
Print #MaquetteFic, "\Distance�0"
Print #MaquetteFic, "\Ligne�1"
Print #MaquetteFic, "\Colonne�" & nbCol
Print #MaquetteFic, "\Col�1�";
'-> Cr�er chaque colonne
For i = 1 To nbCol
    '-> Remplacer les valeurs
    DefCellule = DefCelluleEntete
    DefCellule = Replace(DefCellule, "CONTENU", CStr(ContenuCol(i)))
    DefCellule = Replace(DefCellule, "LARGEUR", Replace(CStr(LargeurCol(i)), ",", "."))
    '-> Ecrire la cellule
    If i <> nbCol Then
        Print #MaquetteFic, DefCellule;
        Print #MaquetteFic, "|";
    Else
        Print #MaquetteFic, DefCellule
    End If
Next 'pour toutes les colonnes
Print #MaquetteFic, "\Champs�"
Print #MaquetteFic, "\End�Entete"

'-> Enregistrement de la ligne de d�tail
Print #MaquetteFic, "\Begin�Detail"
Print #MaquetteFic, "\AlignTop�1"
Print #MaquetteFic, "\Top�0"
Print #MaquetteFic, "\AlignLeft�" & AlignLeft
Print #MaquetteFic, "\Left�0"
Print #MaquetteFic, "\Largeur�" & Replace(CStr(LargeurTableau), ",", ".")
Print #MaquetteFic, "\Hauteur�0.50"
Print #MaquetteFic, "\Distance�0"
Print #MaquetteFic, "\Ligne�1"
Print #MaquetteFic, "\Colonne�" & nbCol
Print #MaquetteFic, "\Col�0.50�";
'-> Cr�er chaque colonne et la ligne de d�tail
LigneDetail = "[TB-Visualisation(BLK-Detail)][\1]{"
For i = 1 To nbCol
    '-> Remplacer les valeurs
    DefCellule = DefCelluleDetail
    DefCellule = Replace(DefCellule, "CONTENU", "^" & Format(i, "0000"))
    DefCellule = Replace(DefCellule, "LARGEUR", Replace(CStr(LargeurCol(i)), ",", "."))
    '-> Ecrire la cellule
    If i <> nbCol Then
        Print #MaquetteFic, DefCellule;
        Print #MaquetteFic, "|";
    Else
        Print #MaquetteFic, DefCellule
    End If
    StrChamps = StrChamps & Format(i, "0000") & "; ;|"
    '-> Ligne de d�tail
    LigneDetail = LigneDetail & "^" & Format(i, "0000") & "#" & Format(i, "0000") & Space$(50)
Next 'pour toutes les colonnes
LigneDetail = LigneDetail & "}"
Print #MaquetteFic, "\Champs�" & Mid$(StrChamps, 1, Len(StrChamps) - 1)
Print #MaquetteFic, "\End�Detail"

'-> Enregistrement de la ligne de fin de tableau
Print #MaquetteFic, "\Begin�FinTableau"
Print #MaquetteFic, "\AlignTop�1"
Print #MaquetteFic, "\Top�0"
Print #MaquetteFic, "\AlignLeft�" & AlignLeft
Print #MaquetteFic, "\Left�0"
Print #MaquetteFic, "\Largeur�" & Replace(CStr(LargeurTableau), ",", ".")
Print #MaquetteFic, "\Hauteur�0.20"
Print #MaquetteFic, "\Distance�0"
Print #MaquetteFic, "\Ligne�1"
Print #MaquetteFic, "\Colonne�" & nbCol
Print #MaquetteFic, "\Col�0.20�";
'-> Cr�er chaque colonne
For i = 1 To nbCol
    '-> Remplacer les valeurs
    DefCellule = DefCelluleBasTableau
    DefCellule = Replace(DefCellule, "LARGEUR", Replace(CStr(LargeurCol(i)), ",", "."))
    '-> Ecrire la cellule
    If i <> nbCol Then
        Print #MaquetteFic, DefCellule;
        Print #MaquetteFic, "|";
    Else
        Print #MaquetteFic, DefCellule
    End If
Next 'pour toutes les colonnes
Print #MaquetteFic, "\Champs�"
Print #MaquetteFic, "\End�FinTableau"

'-> Enregistrement de la ligne de report
If IsReport Then
    '-> Remplacer les valeurs
    DefCellule = DefCelluleReport
    DefCellule = Replace(DefCellule, "CONTENU", Report)
    DefCellule = Replace(DefCellule, "LARGEUR", Replace(CStr(LargeurTableau), ",", "."))
    '-> Enregistrer la ligne
    Print #MaquetteFic, "\Begin�Report"
    Print #MaquetteFic, "\AlignTop�1"
    Print #MaquetteFic, "\Top�0"
    Print #MaquetteFic, "\AlignLeft�3"
    Print #MaquetteFic, "\Left�0"
    Print #MaquetteFic, "\Largeur�" & Replace(CStr(LargeurTableau), ",", ".")
    Print #MaquetteFic, "\Hauteur�1"
    Print #MaquetteFic, "\Distance�0"
    Print #MaquetteFic, "\Ligne�1"
    Print #MaquetteFic, "\Colonne�1"
    Print #MaquetteFic, "\Col�1�" & DefCellule
    Print #MaquetteFic, "\Champs�"
    Print #MaquetteFic, "\End�Report"
End If


'-> Enregistrement de la ligne de pied de page
Print #MaquetteFic, "\Begin�Pied"
Print #MaquetteFic, "\AlignTop�1"
Print #MaquetteFic, "\Top�0"
Print #MaquetteFic, "\AlignLeft�3"
Print #MaquetteFic, "\Left�0"
Print #MaquetteFic, "\Largeur�" & Replace(CStr(LargeurTableau), ",", ".")
Print #MaquetteFic, "\Hauteur�0.50"
Print #MaquetteFic, "\Distance�0"
Print #MaquetteFic, "\Ligne�1"
Print #MaquetteFic, "\Colonne�" & nbCol
Print #MaquetteFic, "\Col�0.50�";
'-> Cr�er chaque colonne et la ligne de d�tail
LignePied = "[TB-Visualisation(BLK-PIED)][\1]{"
StrChamps = ""
For i = 1 To nbCol
    '-> Remplacer les valeurs
    DefCellule = DefCellulePied
    DefCellule = Replace(DefCellule, "CONTENU", "^" & Format(i, "0000"))
    DefCellule = Replace(DefCellule, "LARGEUR", Replace(CStr(LargeurCol(i)), ",", "."))
    '-> Ecrire la cellule
    If i <> nbCol Then
        Print #MaquetteFic, DefCellule;
        Print #MaquetteFic, "|";
    Else
        Print #MaquetteFic, DefCellule
    End If
    StrChamps = StrChamps & Format(i, "0000") & "; ;|"
    '-> Ligne de d�tail
    LignePied = LignePied & "^" & Format(i, "0000") & "#" & Format(i, "0000") & Space$(50)
Next 'pour toutes les colonnes
LignePied = LignePied & "}"
Print #MaquetteFic, "\Champs�" & Mid$(StrChamps, 1, Len(StrChamps) - 1)
Print #MaquetteFic, "\End�Pied"


'-> Enregistrement de la ligne de pagination
Print #MaquetteFic, "\Begin�Pagination"
Print #MaquetteFic, "\AlignTop�2"
Print #MaquetteFic, "\Top�0"
Print #MaquetteFic, "\AlignLeft�3"
Print #MaquetteFic, "\Left�0"
Print #MaquetteFic, "\Largeur�" & Replace(CStr(LargeurTableau), ",", ".")
Print #MaquetteFic, "\Hauteur�0.50"
Print #MaquetteFic, "\Distance�0"
Print #MaquetteFic, "\Ligne�1"
Print #MaquetteFic, "\Colonne�1"
DefCellule = DefCellulePage
DefCellule = Replace(DefCellule, "LARGEUR", Replace(CStr(LargeurTableau), ",", "."))
Print #MaquetteFic, "\Col�0.50�" & DefCellule
Print #MaquetteFic, "\Champs�DATE;;|NUMP;;"
Print #MaquetteFic, "\End�Pagination"

'-> Enregistrer la fin du tableau
Print #MaquetteFic, "\Tableau�End"

'-> La maquette est enregistr�e, fermer le fichier
Close #MaquetteFic

CreateSpool:

'*******************************
'* CREATION DU SPOOL DE VISU   *
'*******************************

'-> D�claration des variables pour calcul des sauts de page en CM
Dim HauteurTotale As Single
Dim PosEnCours As Single
Dim Page As Integer
Dim FindPied As Boolean

'-> Obtenir un nom de fichier Tempo pour la maquette
TmpFichier = Space$(255)
Res = GetTempFileName(TempDirectory, "FIC", 0, TmpFichier)
TmpFichier = Entry(1, TmpFichier, Chr(0))

'-> Ouverture du fichier
FichierFic = FreeFile()
Open TmpFichier For Output As #FichierFic

'-> Impression de la chaine de la maquette
Print #FichierFic, "%%GUI%%" & Entry(1, TmpMaquette, Chr(0))


'-> Initalisation des variables
PosEnCours = 2 '1 de marge + 1 de l'entete
If IsTitre Then PosEnCours = PosEnCours + 1
PosTotale = HauteurPage 'HauteurDoc
Page = 1

'-> Enregistrement du fichier au nouveau format

'-> Date d'impression
Print #FichierFic, "[TB-Visualisation(BLK-PAGINATION)][\1]{^DATE" & Now & "  ^NUMP" & Page & "   }"
'Titre de l'impression
If IsTitre Then Print #FichierFic, "[TB-Visualisation(BLK-TITRE)][\1]{ }"
'Entete de l'impression
Print #FichierFic, "[TB-Visualisation(BLK-ENTETE)][\1]{ }"

Do While Not EOF(NumFicSpool)

    '-> Lecture de la ligne
    Line Input #NumFicSpool, Ligne
    
    If Trim(Ligne) = "" Then
    Else
        '-> Avant d'imprimer, tester si on � la place de l'impression
        If CInt(PosTotale - (PosEnCours + 4.7)) <= 0 Then '2.7 = 1 marge bas + 1 de Report + 0.5 de Pagination + 0.2 de fin tableau
            '-> Fermer le tableau
            Print #FichierFic, "[TB-Visualisation(BLK-FinTableau)][\1]{}"
            '-> Impression du report de page
            If IsReport Then Print #FichierFic, "[TB-Visualisation(BLK-Report)][\1]{}"
            '-> Il faut faire un saut de page
            Print #FichierFic, "[PAGE]"
            Page = Page + 1
            Print #FichierFic, "[TB-Visualisation(BLK-PAGINATION)][\1]{^DATE" & Now & "  ^NUMP" & Page & "   }"
            '-> Reimprimer l'entete
            If IsTitre Then Print #FichierFic, "[TB-Visualisation(BLK-TITRE)][\1]{ }"
            Print #FichierFic, "[TB-Visualisation(BLK-ENTETE)][\1]{ }"
            '-> Setting de la position en cours
            PosEnCours = 2 '1 de marge + 1 de l'entete
            If IsTitre Then PosEnCours = 3
        End If
        
        '-> R�cup�ration du contenu de chaque cellule
        DefCellule = Mid$(Ligne, 10, Len(Ligne) - 10)
        For i = 1 To nbCol
            DefCol = Entry(i, DefCellule, "|")
            ContenuCol(i) = Entry(1, DefCol, "#")
        Next
        
        Select Case UCase$(Trim(Entry(1, Ligne, "}")))
        
            Case "{DETAIL"
            
                '-> Remplacer les #0001 par leur valeur respective
                DefCellule = LigneDetail
                For i = 1 To nbCol
                    DefCellule = Replace(DefCellule, "#" & Format(i, "0000"), ContenuCol(i))
                Next
    
                '-> Enregistrer la ligne de d�tail
                Print #FichierFic, DefCellule
                
                '-> Mise � jour des variables
                DefCellule = ""
                PosEnCours = PosEnCours + 0.5
            
            Case "{PIED"
                '-> Impression du pied
                '-> Remplacer les #0001 par leur valeur respective
                DefCellule = LignePied
                For i = 1 To nbCol
                    DefCellule = Replace(DefCellule, "#" & Format(i, "0000"), ContenuCol(i))
                Next
                Print #FichierFic, DefCellule
                FindPied = True
                
                '-> Quitter
                Exit Do
                    
        End Select
        
    End If 'Si ligne � blanc
    
Loop

'-> Fermer le tableau si pas de pied
If Not FindPied Then Print #FichierFic, "[TB-Visualisation(BLK-FinTableau)][\1]{}"

'-> Fermer tous les fichiers ouverts
Reset

'-> Lancer la visu
If strLNG = "" Then strLNG = "01"
Shell PathExe & " ECRAN$" & strLNG & "|" & TmpFichier, vbMaximizedFocus

'-> Supprimer le fichier de donn�es envoy� de PROGRESS
If Trim(FicSpool) <> "" Then
    'If Dir$(FicSpool, vbNormal) <> "" Then Kill FicSpool
End If

GestError:

'-> Restaurer les formats numerics
'---> Fonction qui restitue les param�trages systemes si necessaire
If RestaureNumericFormat Then Res = SetLocaleInfo&(LOCALE_SYSTEM_DEFAULT, 14, OldFormat)
If RestaureMilierFormat Then Res = SetLocaleInfo&(LOCALE_SYSTEM_DEFAULT, LOCALE_STHOUSAND, OldSepMil)

'-> Terminer la session
End


End Sub

Private Sub VisuFile(FileToDisplay As String)

On Error Resume Next

frmVisu.RichTextBox1.Visible = True
frmVisu.Picture1.Visible = True
frmVisu.Web.Visible = False
If Dir$(FileToDisplay, vbNormal) <> "" Then
    frmVisu.RichTextBox1.LoadFile FileToDisplay
End If
frmVisu.Caption = FileToDisplay
frmVisu.Show

End Sub

Private Sub VisuFileWeb(FileToDisplay As String)

On Error Resume Next

frmVisu.RichTextBox1.Visible = False
frmVisu.Picture1.Visible = False
frmVisu.Web.Visible = True
If Dir$(FileToDisplay, vbNormal) <> "" Then
    frmVisu.Web.Navigate FileToDisplay
End If
frmVisu.Caption = FileToDisplay
frmVisu.Show

End Sub

Private Sub VisuFilePDF(FileToDisplay As String)

'---> Cette proc�dure permet de visualiser un fichier au format PDF

Dim Res As Long
Dim lpBuffer As String
Dim aPath As String
Dim Acrobat As String

'-> V�rifier que le fichier Pass� en argument existe bien
If FileToDisplay = "" Then
    MsgBoxMouchard "Le nom du fichier � visualiser est obligatoire", vbCritical + vbOKOnly, "Erreur"
    End
End If

If Dir$(FileToDisplay, vbNormal) = "" Then
    MsgBoxMouchard "Impossible de trouver le fichier : " & FileToDisplay, vbCritical + vbOKOnly, "Erreur"
    End
End If

ShellExecute 0, "OPEN", FileToDisplay, "", vbNullString, 1


End Sub

Public Function GetSpecialfolder(CSIDL As Long) As String
'-> pour retrouver le repertoire mes documents
    Dim r As Long
    Dim IDL As ITEMIDLIST
    'Get the special folder
    r = SHGetSpecialFolderLocation(100, CSIDL, IDL)
    If r = NOERROR Then
        'Create a buffer
        Path$ = Space$(512)
        'Get the path from the IDList
        r = SHGetPathFromIDList(ByVal IDL.mkid.cb, ByVal Path$)
        'Remove the unnecessary chr$(0)'s
        GetSpecialfolder = Left$(Path, InStr(Path, Chr$(0)) - 1)
        Exit Function
    End If
    GetSpecialfolder = ""
End Function

