VERSION 5.00
Begin VB.Form VisuDeal 
   Caption         =   "Aper�u deal Informatique"
   ClientHeight    =   7455
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9420
   Icon            =   "VisuDeal.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7455
   ScaleWidth      =   9420
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   975
      Left            =   0
      ScaleHeight     =   975
      ScaleWidth      =   6375
      TabIndex        =   3
      Top             =   0
      Visible         =   0   'False
      Width           =   6375
   End
   Begin VB.HScrollBar DefH 
      Height          =   255
      LargeChange     =   567
      Left            =   3360
      SmallChange     =   567
      TabIndex        =   2
      Top             =   5640
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.VScrollBar DefV 
      Height          =   2415
      LargeChange     =   567
      Left            =   7800
      SmallChange     =   567
      TabIndex        =   1
      Top             =   1200
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.OLE OLE 
      AutoActivate    =   0  'Manual
      AutoVerbMenu    =   0   'False
      Height          =   9855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   14385
   End
End
Attribute VB_Name = "VisuDeal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub DefH_Change()

    aObject.Left = DefH.Value

End Sub

Private Sub DefV_Change()
    aObject.Top = DefV.Value
End Sub

Private Sub Form_Resize()

On Error Resume Next

    AjustH
    AjustV
    If Me.DefH.Visible And Me.DefV.Visible Then
        Me.DefH.Width = Me.DefH.Width - Me.DefV.Width
        Me.DefV.Height = Me.DefV.Height - Me.DefH.Height
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub
