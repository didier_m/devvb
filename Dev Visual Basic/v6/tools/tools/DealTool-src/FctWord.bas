Attribute VB_Name = "FctWord"
Option Explicit

Private DataCols As Collection
Private NbRowData As Long
Private aDesktop As Object


Private Sub DrawWait()

On Error Resume Next

'---> Proc�dure qui dessine dans Accueil.picture1 le pourcentage r�alis�
'-> ( TailleLue / TailleTotale )
DoEvents
frmTempoWord.Tempo.Line (0, 0)-((TailleLue / TailleTotale) * frmTempoWord.Tempo.ScaleWidth, frmTempoWord.Tempo.ScaleHeight), &HC00000, BF

End Sub

Private Sub DrawCurrentFile(TailleLue As Long, TailleTotale As Long)

On Error Resume Next

'---> Proc�dure qui dessine dans Accueil.picture1 le pourcentage r�alis�
'-> ( TailleLue / TailleTotale )
DoEvents
frmTempoWord.Tempo2.Line (0, 0)-((TailleLue / TailleTotale) * frmTempoWord.Tempo2.ScaleWidth, frmTempoWord.Tempo2.ScaleHeight), &HC00000, BF
frmTempoWord.Label4.Caption = "Fichier : " & Format(CStr(TailleLue / TailleTotale) * 100, "00.00") & " %"
DoEvents

End Sub

Private Sub CreateObjectDataFromFile(dataFile As String)

'---> Cette proc�dure convertit un fichier De donn�es en Mod�le objet

Dim aData As srcData
Dim hdlFile As Integer
Dim Ligne As String

On Error Resume Next

'-> Vider la collection existante
If DataCols Is Nothing Then
    Set DataCols = New Collection
Else
    Do While DataCols.Count <> 0
        DataCols.Remove 1
    Loop
End If
    
'-> Raz des variables
NbRowData = 0
    
'-> Ouverture du fichier Ascii pass� en param�tres
hdlFile = FreeFile
Open dataFile For Input As #hdlFile
Do While Not EOF(hdlFile)
    '-> Lecture d'une ligne
    Line Input #hdlFile, Ligne
    '-> Ne pas traiter les lignes � blanc
    If Trim(Ligne) = "" Then GoTo NextLigne
    '-> Cr�er un nouvl objetd de donn�es
    Set aData = New srcData
    '-> Donn�es ses valeurs
    aData.KeyWord = UCase$(Trim(Entry(1, Ligne, "|")))
    aData.ValueData = Entry(2, Ligne, "|")
    '-> Ajouter dans la collection
    DataCols.Add aData, "DATA|" & aData.KeyWord
    '-> Incr�menter le compteur de ligne
    NbRowData = NbRowData + 1
NextLigne:
Loop

'-> Fermer le fichier
Close #hdlFile

End Sub

Public Sub CreateDocumentWord(ParFile As String)

'---> G�n�ration de documents Word

'-> Pour gestion des variables d'environnement
Dim lpBuffer As String
Dim Res As Long
Dim IsLoadingTempo As Boolean
Dim Rep As VbMsgBoxResult

'-> Pour gestion du fichier Ini
Dim ParamPath As String

'-> Pour lecture du fichier Par
Dim hdlFile As Integer
Dim Ligne As String

'-> Variables de param�trage du fichier PAR
Dim Ident As String
Dim Progiciel As String
Dim ListeDocument As String
Dim NbDocument As Integer
Dim SectionWord As String

'-> Pour lecture d'une section du fichier de param�trage
Dim strRef As String
Dim strDest As String
Dim strDest2 As String
Dim strData As String
Dim strPasse As String
Dim strPasseW As String
Dim strKillDataFile As String
Dim strShowAfter As String

'-> Pour r�cup�ration du param�trage du fichier Ini
Dim MsoRef As String
Dim MsoDest As String

'-> Pour gestion des variables d'environnement
Dim VarDealTempo As String
Dim VarUserName As String

'-> Pour gestion de Word
Dim WordApp As Object
Dim WordFileRef As String
Dim WordFileDest As String

'-> Pour get du contenu du fichier Word
Dim WordDocumentText As String
Dim aServiceManager As Object

'-> Variables de lecture
Dim i As Integer, j As Integer

MsgBoxMouchard "CreateDocumentWord"

On Error GoTo GestError
'-> V�rifier si on trouve le fichier
If ParFile = "" Then
    MsgBoxMouchard "Veuillez sp�cifier un fichier de param�trage", vbCritical + vbOKOnly, "Erreur fatale"
    End
Else
    If Dir$(ParFile) = "" Then
        MsgBoxMouchard "Impossible de trouver le fichier de param�trage" & Chr(13) & ParFile, vbCritical + vbOKOnly, "Erreur Fatale"
        End
    End If
End If

'-> R�cup�ration des param�tres d'entete
Ident = GetIniFileValue("GENERAL", "IDENT", ParFile)
Progiciel = GetIniFileValue("GENERAL", "PROGICIEL", ParFile)

'MsgBoxMouchard "Ident et progiciel" & Ident & Progiciel

'-> Tester l'ident
If Ident = "" Then
    MsgBoxMouchard "Veuillez renseigner l'ident", vbCritical + vbOKOnly, "Erreur Fatale"
    Exit Sub
End If

If Progiciel = "" Then
    MsgBoxMouchard "Veuillez renseigner le progiciel", vbCritical + vbOKOnly, "Erreur Fatale"
    Exit Sub
End If

'-> R�cup�rer les mots cl�s d'adressage virtuels
GetPathMotCles

'-> Construire le path du fichier PARAM.INI
'ParamPath = "$LECTEUR$\DEALPRO\$IDENT$\$VERSION$\Param.ini" PASSAGE V6
ParamPath = App.Path & "\Param.ini"
ReplacePathMotCles ParamPath

'-> Remplcer $IDENT$ par l'ident pass� en param�tre dans le *.par
ParamPath = Replace(ParamPath, "$IDENT$", Ident)

'MsgBoxMouchard "parampath : " & ParamPath

'-> R�cup�rer le param�trage du fichier ini de r�f�rence
MsoRef = GetIniString("MSO_" & Progiciel, "$REFERENCE$", ParamPath, False)
MsoDest = GetIniString("MSO_" & Progiciel, "$DESTINATION$", ParamPath, False)

'-> R�cup�rer les variables d'environnement
lpBuffer = Space$(300)
Res = GetEnvironmentVariable("DEALTEMPO", lpBuffer, Len(lpBuffer))
If Res <> 0 Then VarDealTempo = Mid$(lpBuffer, 1, Res)
lpBuffer = Space$(300)
Res = GetEnvironmentVariable("USERNAME", lpBuffer, Len(lpBuffer))
If Res <> 0 Then VarUserName = Mid$(lpBuffer, 1, Res)
    
'-> Faire un replace des mots cl�s
ReplacePathMotCles MsoRef
ReplacePathMotCles MsoDest
    
'-> Faire un replace dans les 2 paths
MsoRef = Trim(Replace(UCase$(MsoRef), "%DEALTEMPO%", VarDealTempo))
MsoRef = Trim(Replace(UCase$(MsoRef), "%USERNAME%", VarUserName))
MsoRef = Trim(Replace(UCase$(MsoRef), "$IDENT$", Ident))
MsoDest = Trim(Replace(UCase$(MsoDest), "%DEALTEMPO%", VarDealTempo))
MsoDest = Trim(Replace(UCase$(MsoDest), "%USERNAME%", VarUserName))
MsoDest = Trim(Replace(UCase$(MsoDest), "$IDENT$", Ident))

'-> Rajouter un \ si necessaire
If MsoRef <> "" And Right(MsoRef, 1) <> "\" Then MsoRef = MsoRef & "\"
If MsoDest <> "" And Right(MsoDest, 1) <> "\" Then MsoDest = MsoDest & "\"

MsgBoxMouchard "msoref:" + MsoRef + " msodest:" + MsoDest

'-> R�cup�rer la liste des documents
ListeDocument = Space(64000)
Res = GetPrivateProfileSectionNames(ListeDocument, Len(ListeDocument), ParFile)
If Res = 0 Then
    '-> Message d'erreur
    MsgBoxMouchard "Pas de document � traiter", vbExclamation + vbOKOnly, "Fin du traitement"
    End
Else
    ListeDocument = Mid$(ListeDocument, 1, Res)
End If

'-> Traitement de toutes les sections du fichier Par
NbDocument = NumEntries(ListeDocument, Chr(0)) - 1

'-> Cr�er un point d'entr�e vers Word
Set WordApp = CreateObject("Word.Application")

MsgBoxMouchard "CreateDocumentWord apres tentative creation word"

'-> Tester si cela a march�
If WordApp Is Nothing Then
    '-> Message d'erreur
    Set aServiceManager = CreateObject("com.sun.star.ServiceManager")
    If aServiceManager Is Nothing Then
        MsgBoxMouchard "Impossible de r�cup�rer un pointeur vers Mircosoft Word", vbCritical + vbOKOnly, "Erreur Fatale"
        GoTo GestError
    Else
        CreateDocumentOpenOffice ParFile
        Set aServiceManager = Nothing
        Exit Sub
    End If
End If

'-> Charger la feuille de temporisation �cran
frmTempoWord.Show
frmTempoWord.Enabled = False

'-> Indiquer que la feuille est charg�e
IsLoadingTempo = True

'-> Poser la taille max
TailleTotale = NbDocument - 2

'-> Analyse de toutes les sections � traiter
For i = 1 To NbDocument
    '-> R�cup�rer la section � trairer
    SectionWord = Trim(Entry(i, ListeDocument, Chr(0)))
    '-> Ne pas traiter la section g�n�ral
    If UCase$(Trim(SectionWord)) = "GENERAL" Then GoTo NextSection
    
    '-> Setting de la temporisation
    frmTempoWord.Label3.Caption = i - 1 & "/" & NbDocument - 1
    
    '-> Sortir de la boucle si necessaire
    If Trim(SectionWord) = "" Then Exit For
    
    '-> R�cup�rer le param�trage du document
    strRef = GetIniFileValue(SectionWord, "REFERENCE", ParFile)
    strDest = GetIniFileValue(SectionWord, "DESTINATION", ParFile)
    strDest2 = GetIniFileValue(SectionWord, "DESTINATION2", ParFile)
    strData = GetIniFileValue(SectionWord, "DATA", ParFile)
    strPasse = GetIniFileValue(SectionWord, "PASSE", ParFile)
    strPasseW = GetIniFileValue(SectionWord, "VISU", ParFile)
    strKillDataFile = UCase$(GetIniFileValue(SectionWord, "KILL", ParFile))
    strShowAfter = UCase$(GetIniFileValue(SectionWord, "SHOW", ParFile))
    
    '-> si le fifier de reference est de type open office partir sur openoffice
    If UCase(Right(strRef, 4)) = ".OTT" Or UCase(Right(strRef, 4)) = ".ODT" Then
        CreateDocumentOpenOffice ParFile
        Exit Sub
    End If
    
    '-> V�rifier si on trouve le fichier de r�f�rence
    If InStr(strRef, "/") <> 0 Or InStr(strRef, "\") <> 0 Then
        WordFileRef = strRef
    Else
        WordFileRef = MsoRef & strRef
    End If
    If Dir$(WordFileRef) = "" Then
        MsgBoxMouchard "Impossible de traiter le document : " & SectionWord & Chr(13) & "Fichier de r�f�rence introuvable" & Chr(13) & WordFileRef, vbCritical + vbCritical, "Impossible de traiter le document"
        GoTo NextSection
    End If
    
     MsgBoxMouchard "createdocument word ->wordfileref:" & WordFileRef
     
    '-> V�rifier si on trouve le fichier de donn�es sp�cifi�
    If strData = "" Then
        MsgBoxMouchard "Fichier de donn�es non renseign� pour le document : " & SectionWord, vbExclamation + vbOKOnly, "Erreur"
        GoTo NextSection
    Else
        '-> V�rifier si on trouve le fichier
        If Dir$(strData) = "" Then
            MsgBoxMouchard "Impossible de trouver le fichier de donn�es : " & strData, vbCritical + vbCritical, "Impossible de traiter le document"
            GoTo NextSection
        End If
    End If
    
    '-> Composer le fichier de destination
    '-> modif DIDIER 2/03/06
    If InStr(1, strDest, "/") <> 0 Or InStr(1, strDest, "\") <> 0 Or InStr(1, strDest, ":") <> 0 Then
        WordFileDest = strDest
    Else
        WordFileDest = MsoDest & strDest
    End If
    
    MsgBoxMouchard "createdocument word suite->wordfileref:" & WordFileRef
    
    '-> V�rifier si le fichier destination existe
    If Dir$(WordFileDest) <> "" Then
        '-> Demander confirmation avant de supprimer
        Rep = MsgBoxMouchard("Le fichier " & Chr(13) & WordFileDest & " existe d�ja. Le supprimer ?", vbExclamation + vbYesNo, "Confirmation")
        If Rep = vbNo Then GoTo NextSection
        '-> Kill du fichier
        Kill WordFileDest
    End If
    
    '-> Indiquer le nom du fichier destination
    frmTempoWord.Label2.Caption = WordFileDest
    
    '-> Lib�r�r la cpu
    frmTempoWord.Show
    DoEvents
    
    MsgBoxMouchard "createdocumentword avant createobjectdata..."
    '-> Ouvrir le fichier de donn�es et cr�er son mod�le objet
    Call CreateObjectDataFromFile(strData)
    MsgBoxMouchard "createdocumentword apres createobjectdata..." & WordFileRef
    
    '-> Ouvrir le document de r�f�rence en tant que template ou Document
    WordApp.Application.Documents.Open FileName:="" & WordFileRef & "", ReadOnly:=True
    
    MsgBoxMouchard "createdocumentword apres createobjectdata..." & WordFileRef
    MsgBoxMouchard "createdocumentword apres createobjectdata..." & Err.Description
    
    '-> Viderle presspapier
    Clipboard.Clear
        
    '-> Copy dans le presspapier
    WordApp.Application.selection.WholeStory
   
    MsgBoxMouchard "createdocumentword avant replacekeyword..."

    '-> Faitre un replace du contenu
    Call ReplaceKeyWord(WordApp)
        
    MsgBoxMouchard "createdocumentword apres replacekeyword..."
    
    '-> Enregistrer le document word
    If Trim(UCase(strPasseW)) = "OUI" Then
        WordApp.Application.ActiveDocument.SaveAs FileName:="" & WordFileDest, _
        LockComments:=False, PassWord:="", AddToRecentFiles:= _
        True, WritePassword:="" & strPasse, ReadOnlyRecommended:=True, EmbedTrueTypeFonts:= _
        False, SaveNativePictureFormat:=False, SaveFormsData:=False, SaveAsAOCELetter:=False
    Else
        If Trim(UCase(strPasseW)) = "PDF" Then
            MsgBoxMouchard "PDF..." + strDest & "*" & strDest2, vbCritical + vbOKOnly, "Erreur"
            On Error GoTo suite
            Dim topOK As Boolean
            topOK = False
            WordApp.Application.ActiveDocument.SaveAs FileName:="" & WordFileDest & ".pdf", FileFormat:=17
            If strDest2 <> "" Then
                WordApp.Application.ActiveDocument.SaveAs FileName:="" & strDest2 & ".pdf", FileFormat:=17
            End If
            topOK = True
suite:
            If topOK = False Then
                MsgBoxMouchard "no ok", vbCritical + vbOKOnly, "Erreur"
                WordApp.Application.ActiveDocument.SaveAs FileName:="" & WordFileDest, _
                LockComments:=False, PassWord:="", AddToRecentFiles:= _
                True, WritePassword:="" & strPasse, ReadOnlyRecommended:=True, EmbedTrueTypeFonts:= _
                False, SaveNativePictureFormat:=False, SaveFormsData:=False, SaveAsAOCELetter:=False
                If strDest2 <> "" Then
                    WordApp.Application.ActiveDocument.SaveAs FileName:="" & strDest2, _
                    LockComments:=False, PassWord:="", AddToRecentFiles:= _
                    True, WritePassword:="" & strPasse, ReadOnlyRecommended:=True, EmbedTrueTypeFonts:= _
                    False, SaveNativePictureFormat:=False, SaveFormsData:=False, SaveAsAOCELetter:=False
                End If
            End If
        Else
            WordApp.Application.ActiveDocument.SaveAs FileName:="" & WordFileDest, _
            LockComments:=False, PassWord:="" & strPasse, AddToRecentFiles:= _
            True, WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts:= _
            False, SaveNativePictureFormat:=False, SaveFormsData:=False, SaveAsAOCELetter:=False
            If strDest2 <> "" Then
                WordApp.Application.ActiveDocument.SaveAs FileName:="" & strDest2, _
                LockComments:=False, PassWord:="" & strPasse, AddToRecentFiles:= _
                True, WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts:= _
                False, SaveNativePictureFormat:=False, SaveFormsData:=False, SaveAsAOCELetter:=False
            End If
        End If
    End If
    '-> Fermer le document
    WordApp.Application.ActiveDocument.Close
    
    '-> Supprimer le fichier de donn�es si necessaire
    If strKillDataFile = "OUI" Then Kill strData
    
    '-> Indiquer la taille Lue
    TailleLue = TailleLue + 1
    DrawWait
    
NextSection:
Next 'Pour tous les documents � traiter

'-> Rendre word visible si necessaire
If UCase$(Trim(strShowAfter)) = "OUI" Then
    '-> Ouvrir le document de r�f�rence THIERRY
    WordApp.Application.Documents.Open FileName:="" & WordFileDest & ""
    WordApp.Visible = True
End If

'-> Supprimer le fichier PAR
Kill ParFile

GestError:
    '-> Fermer l'application Word
    If UCase$(Trim(strShowAfter)) <> "OUI" Then WordApp.Application.Quit
    '-> Lib�rer le pointeur vers Word
    Set WordApp = Nothing
    '-> Si on a charg� la feuille de temporisation , la d�charger
    If IsLoadingTempo Then Unload frmTempoWord
    


End Sub

Public Sub CreateDocumentOpenOffice(ParFile As String)

'---> G�n�ration de documents OpenOffice

'-> Pour gestion des variables d'environnement
Dim lpBuffer As String
Dim Res As Long
Dim IsLoadingTempo As Boolean
Dim Rep As VbMsgBoxResult

'-> Pour gestion du fichier Ini
Dim ParamPath As String

'-> Pour lecture du fichier Par
Dim hdlFile As Integer
Dim Ligne As String

'-> Variables de param�trage du fichier PAR
Dim Ident As String
Dim Progiciel As String
Dim ListeDocument As String
Dim NbDocument As Integer
Dim SectionOpenOffice As String

'-> Pour lecture d'une section du fichier de param�trage
Dim strRef As String
Dim strDest As String
Dim strDest2 As String
Dim strData As String
Dim strPasse As String
Dim strPasseW As String
Dim strKillDataFile As String
Dim strShowAfter As String

'-> Pour r�cup�ration du param�trage du fichier Ini
Dim MsoRef As String
Dim MsoDest As String

'-> Pour gestion des variables d'environnement
Dim VarDealTempo As String
Dim VarUserName As String

'-> Pour gestion de OpenOffice
Dim OpenOfficeApp As Object
Dim OpenOfficeFileRef As String
Dim OpenOfficeFileDest As String

'-> Pour get du contenu du fichier OpenOffice
Dim OpenOfficeDocumentText As String
Dim args()

'-> Variables de lecture
Dim i As Integer, j As Integer

Dim OpenPar(2) As Object 'Un tableau VB
Dim aServiceManager
Dim SpreadSheets
Dim objDocument
Dim oPropertyValue
Dim oWindow As Object

On Error GoTo GestError

MsgBoxMouchard "CreateDocumentOpenOffice"

'-> V�rifier si on trouve le fichier
If ParFile = "" Then
    MsgBoxMouchard "Veuillez sp�cifier un fichier de param�trage", vbCritical + vbOKOnly, "Erreur fatale"
    End
Else
    If Dir$(ParFile) = "" Then
        MsgBoxMouchard "Impossible de trouver le fichier de param�trage" & Chr(13) & ParFile, vbCritical + vbOKOnly, "Erreur Fatale"
        End
    End If
End If

'-> R�cup�ration des param�tres d'entete
Ident = GetIniFileValue("GENERAL", "IDENT", ParFile)
Progiciel = GetIniFileValue("GENERAL", "PROGICIEL", ParFile)

'MsgBoxMouchard "Ident et progiciel" & Ident & Progiciel

'-> Tester l'ident
If Ident = "" Then
    MsgBoxMouchard "Veuillez renseigner l'ident", vbCritical + vbOKOnly, "Erreur Fatale"
    Exit Sub
End If

If Progiciel = "" Then
    MsgBoxMouchard "Veuillez renseigner le progiciel", vbCritical + vbOKOnly, "Erreur Fatale"
    Exit Sub
End If

'-> R�cup�rer les mots cl�s d'adressage virtuels
GetPathMotCles

'-> Construire le path du fichier PARAM.INI
'ParamPath = "$LECTEUR$\DEALPRO\$IDENT$\$VERSION$\Param.ini" PASSAGE V6
ParamPath = App.Path & "\Param.ini"
ReplacePathMotCles ParamPath

'-> Remplcer $IDENT$ par l'ident pass� en param�tre dans le *.par
ParamPath = Replace(ParamPath, "$IDENT$", Ident)

'MsgBoxMouchard "parampath : " & ParamPath

'-> R�cup�rer le param�trage du fichier ini de r�f�rence
MsoRef = GetIniString("MSO_" & Progiciel, "$REFERENCE$", ParamPath, False)
MsoDest = GetIniString("MSO_" & Progiciel, "$DESTINATION$", ParamPath, False)

'-> R�cup�rer les variables d'environnement
lpBuffer = Space$(300)
Res = GetEnvironmentVariable("DEALTEMPO", lpBuffer, Len(lpBuffer))
If Res <> 0 Then VarDealTempo = Mid$(lpBuffer, 1, Res)
lpBuffer = Space$(300)
Res = GetEnvironmentVariable("USERNAME", lpBuffer, Len(lpBuffer))
If Res <> 0 Then VarUserName = Mid$(lpBuffer, 1, Res)
    
'-> Faire un replace des mots cl�s
ReplacePathMotCles MsoRef
ReplacePathMotCles MsoDest
    
'-> Faire un replace dans les 2 paths
MsoRef = Trim(Replace(UCase$(MsoRef), "%DEALTEMPO%", VarDealTempo))
MsoRef = Trim(Replace(UCase$(MsoRef), "%USERNAME%", VarUserName))
MsoDest = Trim(Replace(UCase$(MsoDest), "%DEALTEMPO%", VarDealTempo))
MsoDest = Trim(Replace(UCase$(MsoDest), "%USERNAME%", VarUserName))

'-> Rajouter un \ si necessaire
If MsoRef <> "" And Right(MsoRef, 1) <> "\" Then MsoRef = MsoRef & "\"
If MsoDest <> "" And Right(MsoDest, 1) <> "\" Then MsoDest = MsoDest & "\"

'-> R�cup�rer la liste des documents
ListeDocument = Space(64000)
Res = GetPrivateProfileSectionNames(ListeDocument, Len(ListeDocument), ParFile)
If Res = 0 Then
    '-> Message d'erreur
    MsgBoxMouchard "Pas de document � traiter", vbExclamation + vbOKOnly, "Fin du traitement"
    End
Else
    ListeDocument = Mid$(ListeDocument, 1, Res)
End If

'-> Traitement de toutes les sections du fichier Par
NbDocument = NumEntries(ListeDocument, Chr(0)) - 1

'-> Cr�er un point d'entr�e vers OpenOffice
'Lien vers le service manager de oppenoffice
Set aServiceManager = CreateObject("com.sun.star.ServiceManager")
Set oPropertyValue = aServiceManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue")
'Lien Vers le bureau
Set aDesktop = aServiceManager.createInstance("com.sun.star.frame.Desktop")

'-> Tester si cela a march�
If aDesktop Is Nothing Then
    '-> Message d'erreur
    MsgBoxMouchard "Impossible de r�cup�rer un pointeur vers Open Office", vbCritical + vbOKOnly, "Erreur Fatale"
    GoTo GestError
End If

'-> Charger la feuille de temporisation �cran
frmTempoWord.Show
frmTempoWord.Enabled = False

'-> Indiquer que la feuille est charg�e
IsLoadingTempo = True

'-> Poser la taille max
TailleTotale = NbDocument - 2

'-> Analyse de toutes les sections � traiter
For i = 1 To NbDocument
    '-> R�cup�rer la section � trairer
    SectionOpenOffice = Trim(Entry(i, ListeDocument, Chr(0)))
    '-> Ne pas traiter la section g�n�ral
    If UCase$(Trim(SectionOpenOffice)) = "GENERAL" Then GoTo NextSection
    
    '-> Setting de la temporisation
    frmTempoWord.Label3.Caption = i - 1 & "/" & NbDocument - 1
    
    '-> Sortir de la boucle si necessaire
    If Trim(SectionOpenOffice) = "" Then Exit For
    
    '-> R�cup�rer le param�trage du document
    strRef = GetIniFileValue(SectionOpenOffice, "REFERENCE", ParFile)
    strDest = GetIniFileValue(SectionOpenOffice, "DESTINATION", ParFile)
    strDest2 = GetIniFileValue(SectionOpenOffice, "DESTINATION2", ParFile)
    strData = GetIniFileValue(SectionOpenOffice, "DATA", ParFile)
    strPasse = GetIniFileValue(SectionOpenOffice, "PASSE", ParFile)
    strKillDataFile = UCase$(GetIniFileValue(SectionOpenOffice, "KILL", ParFile))
    strShowAfter = UCase$(GetIniFileValue(SectionOpenOffice, "SHOW", ParFile))
    
    '-> V�rifier si on trouve le fichier de r�f�rence
    OpenOfficeFileRef = MsoRef & strRef
    If Dir$(OpenOfficeFileRef) = "" Then
        MsgBoxMouchard "Impossible de traiter le document : " & SectionOpenOffice & Chr(13) & "Fichier de r�f�rence introuvable" & Chr(13) & OpenOfficeFileRef, vbCritical + vbCritical, "Impossible de traiter le document"
        GoTo NextSection
    End If
    
    '-> V�rifier si on trouve le fichier de donn�es sp�cifi�
    If strData = "" Then
        MsgBoxMouchard "Fichier de donn�es non renseign� pour le document : " & SectionOpenOffice, vbExclamation + vbOKOnly, "Erreur"
        GoTo NextSection
    Else
        '-> V�rifier si on trouve le fichier
        If Dir$(strData) = "" Then
            MsgBoxMouchard "Impossible de trouver le fichier de donn�es : " & strData, vbCritical + vbCritical, "Impossible de traiter le document"
            GoTo NextSection
        End If
    End If
    
    '-> Composer le fichier de destination
    '-> modif DIDIER 2/03/06
    If InStr(1, strDest, "/") <> 0 Or InStr(1, strDest, "\") <> 0 Or InStr(1, strDest, ":") <> 0 Then
        OpenOfficeFileDest = strDest
    Else
        OpenOfficeFileDest = MsoDest & strDest
    End If
    
    If InStr(1, strDest2, "/") <> 0 Or InStr(1, strDest2, "\") <> 0 Or InStr(1, strDest2, ":") <> 0 Then
        strDest2 = strDest2
    Else
        strDest2 = MsoDest & strDest2
    End If
    
    '-> V�rifier si le fichier destination existe
    If Dir$(OpenOfficeFileDest) <> "" Then
        '-> Demander confirmation avant de supprimer
        Rep = MsgBoxMouchard("Le fichier " & Chr(13) & OpenOfficeFileDest & " existe d�ja. Le supprimer ?", vbExclamation + vbYesNo, "Confirmation")
        If Rep = vbNo Then GoTo NextSection
        '-> Kill du fichier
        Kill OpenOfficeFileDest
    End If
    
    '-> Indiquer le nom du fichier destination
    frmTempoWord.Label2.Caption = OpenOfficeFileDest
    
    '-> Lib�r�r la cpu
    frmTempoWord.Show
    DoEvents
    
    '-> Ouvrir le fichier de donn�es et cr�er son mod�le objet
    Call CreateObjectDataFromFile(strData)
    
    '-> On appelle la fonction setOOoProp d�finie pr�c�demment pour r�cup�rer la structure
    Set OpenPar(0) = setOOoProp("ReadOnly", False)
    Set OpenPar(1) = setOOoProp("Password", "")
    Set OpenPar(2) = setOOoProp("Hidden", True)
    
    '-> Ouvrir le document de r�f�rence en tant que template ou Document
    Set OpenOfficeApp = aDesktop.loadComponentFromURL("file:///" & OpenOfficeFileRef, "_blank", 0, OpenPar)
       
    If strDest2 <> "" Then
        Set OpenOfficeApp = aDesktop.loadComponentFromURL("file:///" & strDest2, "_blank", 0, OpenPar)
    End If
       
    '-> Faitre un replace du contenu
    Call ReplaceKeyOpenOffice(OpenOfficeApp)
    Set OpenPar(1) = setOOoProp("Password", strPasse)
    
    '-> Enregistrer le document OpenOffice
    OpenOfficeApp.storeAsURL Replace("file:///" & OpenOfficeFileDest, "\", "/"), OpenPar
    
    '-> Fermer le document
    OpenOfficeApp.Close True
    
    '-> Supprimer le fichier de donn�es si necessaire
    If strKillDataFile = "OUI" Then Kill strData
    
    '-> Indiquer la taille Lue
    TailleLue = TailleLue + 1
    DrawWait
    
NextSection:
Next 'Pour tous les documents � traiter


'-> Rendre OpenOffice visible si necessaire
If UCase$(Trim(strShowAfter)) = "OUI" Then
    Set OpenPar(2) = setOOoProp("Hidden", False)
    Set OpenPar(1) = setOOoProp("Password", strPasse)
    '-> Ouvrir le document de r�f�rence
    Set OpenOfficeApp = aDesktop.loadComponentFromURL("file:///" & OpenOfficeFileDest, "_blank", 0, OpenPar)
    'OpenOfficeApp.dispose
End If

'-> Supprimer le fichier PAR
Kill ParFile

GestError:
    '-> Fermer l'application OpenOffice
    If UCase$(Trim(strShowAfter)) <> "OUI" Then OpenOfficeApp.Application.Quit
    '-> Lib�rer le pointeur vers OpenOffice
    Set OpenOfficeApp = Nothing
    '-> Si on a charg� la feuille de temporisation , la d�charger
    If IsLoadingTempo Then Unload frmTempoWord

End Sub

Function setOOoProp(cName, uValue) As Object
    
  Dim oPropertyValue As Object
  Dim oSM As Object
    
  Set oSM = CreateObject("com.sun.star.ServiceManager")
  Set oPropertyValue = oSM.Bridge_GetStruct("com.sun.star.beans.PropertyValue")
  oPropertyValue.Name = cName
  oPropertyValue.Value = uValue
      
  Set setOOoProp = oPropertyValue

End Function

Public Sub ReplaceKeyWord(WordApp As Object)

'---> Cette proc�dure remplace dans une chaine de caract�re les mots cl�s par leur valeur

Dim i As Integer, j As Integer, k As Integer, l As Integer
Dim KeyWord As String
Dim iLig As Long
Dim TempoValue As String
Dim ToPrint As String
Dim ValueToReplace As String

For l = 1 To WordApp.Application.selection.fields.Count
    '-> R�cup�rer la premi�re position
    If Not WordApp.Application.selection.fields(l).LinkFormat Is Nothing Then
        i = InStr(1, WordApp.Application.selection.fields(l).LinkFormat.SourceFullName, "<�")
        If i <> 0 Then
            '-> R�cup�rer le mot cl� dans sa totalit�
            j = InStr(i, WordApp.Application.selection.fields(l).LinkFormat.SourceFullName, ">")
            KeyWord = Mid$(WordApp.Application.selection.fields(l).LinkFormat.SourceFullName, i, j - i + 1)
            '-> R�cup�rer la valeur du mot cl�
            TempoValue = GetKeyWordValue(KeyWord)
            ValueToReplace = KeyWord
            WordApp.Application.selection.fields(l).LinkFormat.SourceFullName = Replace(WordApp.Application.selection.fields(l).LinkFormat.SourceFullName, KeyWord, TempoValue)
        End If
    End If
Next

'-> R�cup�rer la premi�re position
i = InStr(1, WordApp.Application.selection, "<�")

'-> R�cup�rer tous les mots cl�s
Do While i <> 0

    
    '-> R�cup�rer le mot cl� dans sa totalit�
    j = InStr(i, WordApp.Application.selection, ">")
    KeyWord = Mid$(WordApp.Application.selection, i, j - i + 1)
    '-> R�cup�rer la valeur du mot cl�
    TempoValue = GetKeyWordValue(KeyWord)
    ValueToReplace = KeyWord
    If Len(TempoValue) = 0 Then
        ReplaceMotCleByValue ValueToReplace, "", WordApp
    Else
        Do While Len(TempoValue) <> 0
            '-> On travaille par block de 200
            If Len(TempoValue) <= 200 Then
                ToPrint = TempoValue
                TempoValue = ""
                ReplaceMotCleByValue ValueToReplace, ToPrint, WordApp
            Else
                '-> Get de la valeur � imprimer
                ToPrint = Mid$(TempoValue, 1, 200)
                '-> Virer les 200 premiers caract�res
                TempoValue = Replace(TempoValue, ToPrint, "", , 1)
                '-> Faire le replace
                ReplaceMotCleByValue ValueToReplace, ToPrint & "<�%GWX%R�>", WordApp
                '-> Modifier le keyword
                ValueToReplace = "<�%GWX%R�>"
            End If 'Si la variable est Sup � 200
        Loop 'Pour tous les segments de text
    End If
    '-> Incr�menter le compteur
    i = InStr(i + 1, WordApp.Application.selection, "<�")
    '-> Dessin de la temporisation
    iLig = iLig + 1
    DrawCurrentFile iLig, NbRowData
Loop 'Pour tous les mots cl� r�f�renc�s

'-> Faire ensuite ici un replace g�n�ral des chr(13)
Call ReplaceMotCleByValue("$CHR(13)$", Chr(13), WordApp)

'-> on met a jour les champs
WordApp.Application.selection.fields.Update

End Sub

Public Function CreateUnoService(strServiceName) As Object
'--> cette fonction permet de creer un service uno
Dim oServiceManager As Object
    Set oServiceManager = CreateObject("com.sun.star.ServiceManager")
    Set CreateUnoService = oServiceManager.createInstance(strServiceName)
End Function

Public Sub ReplaceKeyOpenOffice(OpenOfficeApp As Object)

'---> Cette proc�dure remplace dans une chaine de caract�re les mots cl�s par leur valeur

Dim i As Integer, j As Integer, k As Integer
Dim KeyWord As String
Dim iLig As Long
Dim TempoValue As String
Dim ToPrint As String
Dim ValueToReplace As String
Dim oReplace As Object
Dim oText As Object

Set oText = OpenOfficeApp.GetText
'-> R�cup�rer la premi�re position
i = InStr(1, oText.String, "<�")

'-> R�cup�rer tous les mots cl�s
Do While i <> 0

    
    '-> R�cup�rer le mot cl� dans sa totalit�
    j = InStr(i, oText.String, ">")
    KeyWord = Mid$(oText.String, i, j - i + 1)
    '-> R�cup�rer la valeur du mot cl�
    TempoValue = GetKeyWordValue(KeyWord)
    ValueToReplace = KeyWord
    If Len(TempoValue) = 0 Then
        ReplaceMotCleByValueOpenOffice ValueToReplace, "", OpenOfficeApp
    Else
        Do While Len(TempoValue) <> 0
            '-> On travaille par block de 200
            If Len(TempoValue) <= 200 Then
                ToPrint = TempoValue
                TempoValue = ""
                ReplaceMotCleByValueOpenOffice ValueToReplace, ToPrint, OpenOfficeApp
            Else
                '-> Get de la valeur � imprimer
                ToPrint = Mid$(TempoValue, 1, 200)
                '-> Virer les 200 premiers caract�res
                TempoValue = Replace(TempoValue, ToPrint, "", , 1)
                '-> Faire le replace
                ReplaceMotCleByValueOpenOffice ValueToReplace, ToPrint & "<�%GWX%R�>", OpenOfficeApp
                '-> Modifier le keyword
                ValueToReplace = "<�%GWX%R�>"
            End If 'Si la variable est Sup � 200
        Loop 'Pour tous les segments de text
    End If
    '-> Incr�menter le compteur
    i = InStr(i + 1, oText.String, "<�")
    '-> Dessin de la temporisation
    iLig = iLig + 1
    DrawCurrentFile iLig, NbRowData
Loop 'Pour tous les mots cl� r�f�renc�s

'-> Faire ensuite ici un replace g�n�ral des chr(13)
Call ReplaceMotCleByValueOpenOffice("$CHR(13)$", Chr(13), OpenOfficeApp)

End Sub

Private Sub ReplaceMotCleByValue(KeyWord As String, ValueToReplace As String, WordApp As Object)

'-> Gestion des sauts de ligne
'ValueToReplace = Replace(ValueToReplace, Chr(13), "")
'ValueToReplace = Replace(ValueToReplace, Chr(10), "")
'ValueToReplace = Replace(ValueToReplace, "$CHR(13)$", vbCrLf)

'MsgBoxMouchard ValueToReplace

'-> Faire un replace
WordApp.Application.selection.Find.ClearFormatting
WordApp.Application.selection.Find.Replacement.ClearFormatting
With WordApp.Application.selection.Find
    .Text = KeyWord
    .Replacement.Text = ValueToReplace
End With
WordApp.Application.selection.Find.Execute Replace:=2

End Sub

Private Sub ReplaceMotCleByValueOpenOffice(KeyWord As String, ValueToReplace As String, OpenOfficeApp As Object)

Dim oSrch As Object

On Error GoTo GestError

'-> on se monte l'objet
Set oSrch = OpenOfficeApp.createReplaceDescriptor

'-> Faire un replace
oSrch.setSearchString (KeyWord)
oSrch.setReplaceString (ValueToReplace)
OpenOfficeApp.replaceAll oSrch


GestError:
'-> on vide l'objet
Set oSrch = Nothing
End Sub

Private Function GetKeyWordValue(KeyWord As String) As String

Dim aData As srcData

On Error GoTo GestError

'-> Essayer de pointer sur l'objet
Set aData = DataCols("DATA|" & UCase$(Trim(KeyWord)))

'-> Retourner sa valeur
GetKeyWordValue = aData.ValueData

'-> Lib�rer le pointeur
Set aData = Nothing

GestError:

End Function

