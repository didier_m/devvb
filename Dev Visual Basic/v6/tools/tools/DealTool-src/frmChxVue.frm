VERSION 5.00
Begin VB.Form frmChxVue 
   Caption         =   "Gestionnaire de vues"
   ClientHeight    =   5595
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10380
   Icon            =   "frmChxVue.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5595
   ScaleWidth      =   10380
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame2 
      Caption         =   "Vues existantes :"
      Height          =   5415
      Left            =   0
      TabIndex        =   8
      Top             =   120
      Width           =   3615
      Begin VB.FileListBox File1 
         Height          =   4965
         Left            =   120
         Pattern         =   "*.vis"
         TabIndex        =   0
         Top             =   240
         Width           =   3375
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Propri�t�s : "
      Height          =   5415
      Left            =   3720
      TabIndex        =   7
      Top             =   120
      Width           =   6615
      Begin VB.PictureBox DelVue 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   5160
         Picture         =   "frmChxVue.frx":030A
         ScaleHeight     =   32
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   42
         TabIndex        =   14
         ToolTipText     =   "Supprimer une vue"
         Top             =   4800
         Width           =   630
      End
      Begin VB.TextBox Nom 
         Height          =   285
         Left            =   2880
         TabIndex        =   1
         Top             =   360
         Width           =   3615
      End
      Begin VB.PictureBox SaveBt 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   5880
         Picture         =   "frmChxVue.frx":134C
         ScaleHeight     =   32
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   42
         TabIndex        =   6
         ToolTipText     =   "Enregistrer la vue"
         Top             =   4800
         Width           =   630
      End
      Begin VB.ListBox List2 
         Height          =   1860
         Left            =   120
         Style           =   1  'Checkbox
         TabIndex        =   5
         Top             =   2565
         Width           =   6375
      End
      Begin VB.ListBox List1 
         Height          =   645
         Left            =   2880
         TabIndex        =   3
         Top             =   1080
         Width           =   3615
      End
      Begin VB.TextBox Libelle 
         Height          =   285
         Left            =   2880
         TabIndex        =   4
         Top             =   1800
         Width           =   3615
      End
      Begin VB.TextBox Maquette 
         Height          =   285
         Left            =   2880
         TabIndex        =   2
         Top             =   720
         Width           =   3615
      End
      Begin VB.Label Label5 
         Caption         =   "Nom de la vue : "
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label Label4 
         Caption         =   "Colonnes associ�es : "
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   2160
         Width           =   2175
      End
      Begin VB.Label Label3 
         Caption         =   "Libell� de la maquette :"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   1800
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "Rubrique associ�e : "
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1080
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "Maquette associ�e :"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   720
         Width           =   2535
      End
   End
End
Attribute VB_Name = "frmChxVue"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---> Cette variable contient le path des vues et des maquettes graphiques
Public Rubriques As String
Private KeyRubriques() As String
Dim NbRub As Integer

Private Sub DelVue_Click()

Dim Rep

On Error GoTo ErrorKill

'-> V�rifier qu'il y a une s�lection dans le File1
If Me.File1.ListIndex = -1 Then
    MsgBoxMouchard "Veuillez s�lectionner une vue dans un premier temps", vbCritical + vbOKOnly, "Erreur"
    Me.File1.SetFocus
    Exit Sub
End If

'-> Demander la confirmation
Rep = MsgBoxMouchard("Supprimer la vue : " & Chr(13) & PathApp & Me.File1.List(Me.File1.ListIndex), vbQuestion + vbYesNo, "Confirmation de suppression")
If Rep = vbNo Then Exit Sub

'-> Supprimer le fichier en question
Kill PathApp & Me.File1.List(Me.File1.ListIndex)

'-> Faire un refresh de File1
Me.File1.Refresh

'-> Vider toutes les zones
Me.Nom.Text = ""
Me.Maquette.Text = ""
For i = 0 To Me.List1.ListCount - 1
    Me.List1.Selected(i) = False
Next

For i = 0 To Me.List2.ListCount - 1
    Me.List2.Selected(i) = False
Next

Me.Libelle.Text = ""

Exit Sub

ErrorKill:

    MsgBoxMouchard "Imossible de supprimer le fichier. Celui-ci est peut �tre en cours d'utilisation.", vbCritical + vbOKOnly, "Impossible de supprimer"
    

End Sub

Private Sub File1_Click()

Dim Rubrique As String
Dim Colonnes As String
Dim Libel As String
Dim Maquette As String
Dim i As Integer

'---> Cette proc�dure r�cup�re les param�tres de la vue s�lectionn�e
Rubrique = Trim(GetIniString("GENERAL", "RUB", PathApp & Me.File1.FileName, False))
Colonnes = Trim(GetIniString("GENERAL", "COL", PathApp & Me.File1.FileName, False))
Libel = Trim(GetIniString("GENERAL", "LIB", PathApp & Me.File1.FileName, False))
Maquette = Trim(GetIniString("GENERAL", "MAQ", PathApp & Me.File1.FileName, False))

'-> Afficher le nom de la vue
Me.Nom.Text = Me.File1.FileName

'-> Afficher le libelle
Me.Libelle.Text = Libel

'-> Afficher le nom de la maquette
Me.Maquette.Text = Maquette

'-> S�lectionner les vues de la colonne
For i = 0 To Me.List2.ListCount - 1
    Me.List2.Selected(i) = False
Next
For i = 1 To NumEntries(Colonnes, ",")
    If IsNumeric(Entry(i, Colonnes, ",")) Then
        If CInt(Entry(i, Colonnes, ",")) <= frmChxVue.List2.ListCount Then
            frmChxVue.List2.Selected(CInt(Entry(i, Colonnes, ",")) - 1) = True
        End If
    End If
Next

'-> R�cup�ration de l'index de la rubrique
NbRub = NumEntries(ListeRubrique, "|")
For i = 0 To NbRub - 1
    If UCase$(KeyRubriques(i)) = UCase$(Rubrique) Then
        Me.List1.Selected(i) = True
        Exit For
    End If
Next


End Sub

Private Sub Form_Load()

Dim i As Integer

Dim aFile As String

'-> Charger le path des fichiers vues
Me.File1.Path = PathBmp

'-> Conpter le nombre de rubriques
NbRub = NumEntries(ListeRubrique, "|")

'-> Redimensionner la matrice des rubriques
ReDim KeyRubriques(0 To NbRub - 1)

'-> Charger la liste des rubriques
For i = 1 To NbRub
    Me.List1.AddItem Entry(2, Entry(i, ListeRubrique, "|"), "@")
    KeyRubriques(i - 1) = Entry(1, Entry(i, ListeRubrique, "|"), "@")
Next

'-> Charges la liste des colonnes
For i = 1 To 200
    Me.List2.AddItem "Colonne " & i
Next

'-> Afficher la liste des vues existantes dans le r�pertoire en cours
Me.File1.Pattern = "*.vis"
Me.File1.Path = PathApp


End Sub

Private Sub Libelle_GotFocus()

SelectText Libelle

End Sub

Private Sub Maquette_GotFocus()

    SelectText Maquette

End Sub

Private Sub Nom_GotFocus()

SelectText Nom

End Sub

Private Sub SelectText(TextB As TextBox)
On Error Resume Next

TextB.SelStart = 0
TextB.SelLength = Len(TextB.Text)

End Sub

Private Sub SaveBt_Click()

Dim Rep
Dim Colonnes As String
Dim hdlVue As Integer



'-> V�rifier si les param�tres sont saisis
If Trim(Nom.Text) = "" Then
    MsgBoxMouchard "Veuillez saisir le nom de la vue.", vbCritical + vbOKOnly, "Erreur"
    Nom.SetFocus
    Exit Sub
End If

If Trim(Maquette) = "" Then
    MsgBoxMouchard "Veuillez saisir le nom de la maquette associ�e.", vbCritical + vbOKOnly, "Erreur"
    Maquette.SetFocus
    Exit Sub
End If

If Trim(Libelle) = "" Then
    MsgBoxMouchard "Veuillez saisir le libelle de la vue.", vbCritical + vbOKOnly, "Erreur"
    Libelle.SetFocus
    Exit Sub
End If

'-> V�rifier qu'une rubrque est s�lectionn�e
If Me.List1.SelCount = 0 Then
    MsgBoxMouchard "Attention : il n'y a pas de rubrique de s�lectionn�e. Ceuillez en s�lectionner au moins une.", vbCritical + vbOKOnly, "Erreur"
    Me.List1.SetFocus
    Exit Sub
End If

'-> V�rifier qu'au moins une colonne est s�lectionn�e
If Me.List2.SelCount = 0 Then
    MsgBoxMouchard "Attention : il n'y a pas de colonne de s�lectionn�e. Veuillez en s�lectionner au moins une.", vbCritical + vbOKOnly, "Erreur"
    Me.List2.SetFocus
    Exit Sub
End If

'-> V�rifier si la vue n'existe pas d�ja
If Dir$(PathApp & Nom.Text, vbNormal) <> "" Then
    Rep = MsgBoxMouchard("Ecraser la vue existante ?", vbQuestion + vbYesNo, "Confirmation")
    If Rep = vbNo Then Exit Sub
End If

Me.MousePointer = 11

'-> Enregistrer la vue
SetIniString "GENERAL", "RUB", PathApp & Nom.Text, KeyRubriques(Me.List1.ListIndex)
SetIniString "GENERAL", "LIB", PathApp & Nom.Text, Me.Libelle.Text
SetIniString "GENERAL", "MAQ", PathApp & Nom.Text, Me.Maquette.Text
For i = 0 To Me.List2.ListCount - 1
    If Me.List2.Selected(i) = True Then Colonnes = Colonnes & i + 1 & ","
Next
Colonnes = Mid$(Colonnes, 1, Len(Colonnes) - 1)
SetIniString "GENERAL", "COL", PathApp & Nom.Text, Colonnes

'-> Faire un Refresh de la liste des fichiers
Me.File1.Refresh

'-> S�lectionner la vue
For i = 0 To Me.File1.ListCount - 1
    If UCase$(Me.File1.List(i)) = UCase$(Nom.Text) Then
        Me.File1.Selected(i) = True
        Exit For
    End If
Next

Me.MousePointer = 0

End Sub
