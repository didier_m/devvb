VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmChxCol 
   Caption         =   "Sélection des colonnes"
   ClientHeight    =   6975
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9570
   LinkTopic       =   "Form1"
   ScaleHeight     =   465
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   638
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Split 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   7440
      ScaleHeight     =   495
      ScaleWidth      =   75
      TabIndex        =   2
      Top             =   5520
      Width           =   75
   End
   Begin VB.ListBox List1 
      Height          =   5910
      Index           =   0
      Left            =   5400
      Style           =   1  'Checkbox
      TabIndex        =   1
      Top             =   0
      Width           =   2535
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   6015
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   10610
      _Version        =   393217
      HideSelection   =   0   'False
      Style           =   7
      FullRowSelect   =   -1  'True
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   8400
      Top             =   6120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmChxCol.frx":0000
            Key             =   "Fonction"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmChxCol.frx":031A
            Key             =   "Doc"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmChxCol.frx":076C
            Key             =   "App"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmChxCol.frx":1446
            Key             =   "DocNull"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmChxCol.frx":2120
            Key             =   "Libel"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmChxCol.frx":243A
            Key             =   "Vue"
         EndProperty
      EndProperty
   End
   Begin VB.Image Actualiser 
      Height          =   480
      Left            =   8760
      Picture         =   "frmChxCol.frx":2754
      ToolTipText     =   "Actualiser"
      Top             =   2520
      Width           =   630
   End
   Begin VB.Image Vue_ 
      Height          =   480
      Left            =   8760
      Picture         =   "frmChxCol.frx":3796
      ToolTipText     =   "Gestion des vues"
      Top             =   3240
      Width           =   630
   End
   Begin VB.Image Vue 
      Height          =   480
      Left            =   8760
      Picture         =   "frmChxCol.frx":47D8
      ToolTipText     =   "Gestion des vues"
      Top             =   1920
      Width           =   630
   End
   Begin VB.Image Aide 
      Height          =   480
      Left            =   8760
      Picture         =   "frmChxCol.frx":581A
      ToolTipText     =   "Aide"
      Top             =   720
      Width           =   630
   End
   Begin VB.Image Ok 
      Height          =   480
      Left            =   8760
      Picture         =   "frmChxCol.frx":685C
      ToolTipText     =   "Ok"
      Top             =   120
      Width           =   630
   End
   Begin VB.Image Annuler 
      Height          =   480
      Left            =   8760
      Picture         =   "frmChxCol.frx":789E
      ToolTipText     =   "Annuler"
      Top             =   1320
      Width           =   630
   End
End
Attribute VB_Name = "frmChxCol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Actualiser_Click()
    CreateMenu
End Sub

Private Sub Annuler_Click()
    
    '-> Fermer tous les fichiers
    Reset
    '-> Quitter
    End
    
End Sub

Private Sub Form_Resize()

On Error Resume Next

Dim aRect As RECT
Dim LargeurBordure As Long
Dim HauteurBordure As Long

LargeurBordure = GetSystemMetrics(SM_CXEDGE)
HauteurBordure = GetSystemMetrics(SM_CYEDGE)

Res = GetClientRect(Me.hwnd, aRect)

'-> Repositionner le split en cas
If Me.Split.Left > aRect.Right Then Split.Left = aRect.Right / 2

'-> Positionner le spliter
Me.Split.Height = aRect.Bottom
Me.Split.Top = HauteurBordure

'-> Positionner le treeview
Me.TreeView1.Left = LargeurBordure
Me.TreeView1.Top = HauteurBordure
Me.TreeView1.Height = aRect.Bottom
Me.TreeView1.Width = Split.Left

'-> positionner la liste déroulante
Me.List1(0).Left = Split.Left + Split.Width
Me.List1(0).Top = Me.TreeView1.Top
Me.List1(0).Height = Me.TreeView1.Height
Me.List1(0).Width = aRect.Right - Me.List1(0).Left - Me.Annuler.Width - LargeurBordure * 4

'-> Positionner les boutons
Me.Annuler.Left = aRect.Right - LargeurBordure * 2 - Annuler.Width
Me.Ok.Left = Me.Annuler.Left
Me.Aide.Left = Me.Annuler.Left
Me.Vue.Left = Me.Annuler.Left
Me.Vue_.Left = Me.Vue.Left
Me.Vue_.Top = Me.Vue.Top
Me.Actualiser.Left = Me.Annuler.Left

End Sub

Private Sub Ok_Click()

'-> Vérifier que c'est bien une vue qui est selectionné&e dans le browse
If Me.TreeView1.SelectedItem.Tag <> "VUE" Then
    MsgBoxMouchard "Veuillez sélectionner une vue.", vbCritical + vbOKOnly, "Erreur"
    Me.TreeView1.SetFocus
    Exit Sub
End If

'-> Pointer sur la vue
SelectedVue = Me.TreeView1.SelectedItem.Text

'-> Décharger la feuille
Unload Me


End Sub

Private Sub Split_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)

Dim aPt As POINTAPI
Dim Res As Long

Me.Split.MousePointer = 9

If Button = vbLeftButton Then
    '-> Récupérer dans un premier temps la position de la souris
    Res = GetCursorPos(aPt)
    '-> La convertir au coordonnées clientes
    Res = ScreenToClient(Me.hwnd, aPt)
    '-> Poistionner le spilt en fonction
    If aPt.x > Me.ScaleX(2, 7, 3) And aPt.x < Me.ScaleX(Me.Width, 1, 3) - Me.ScaleX(2, 7, 3) Then Me.Split.Left = aPt.x
    Me.Split.BackColor = &H404040
    Me.Split.ZOrder
End If


End Sub

Private Sub Split_MouseUp(Button As Integer, Shift As Integer, x As Single, Y As Single)

'-> Repositionner les différents objets
If Button = vbLeftButton Then Form_Resize
Me.Split.BackColor = &H8000000F
Me.Split.ZOrder

End Sub

Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)

If Node.Tag = "VUE" Then DisplaySelCol Node.Key

End Sub

Private Sub Vue_Click()

'-> Charger le gestinnaire de vues
frmChxVue.Show vbModal

End Sub

