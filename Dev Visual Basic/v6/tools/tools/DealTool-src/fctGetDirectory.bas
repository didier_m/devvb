Attribute VB_Name = "fctGetDirectory"
Option Explicit

Const MAX_PATH = 260

Private Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
Private Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long
Private Declare Function FileTimeToSystemTime Lib "kernel32" (lpFileTime As FILETIME, lpSystemTime As SYSTEMTIME) As Long
Private Declare Function FileTimeToLocalFileTime Lib "kernel32" (lpFileTime As FILETIME, lpLocalFileTime As FILETIME) As Long

Private Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Private Type FILETIME
    dwLowDateTime As Long
    dwHighDateTime As Long
End Type

Private Type WIN32_FIND_DATA
    dwFileAttributes As Long
    ftCreationTime As FILETIME
    ftLastAccessTime As FILETIME
    ftLastWriteTime As FILETIME
    nFileSizeHigh As Long
    nFileSizeLow As Long
    dwReserved0 As Long
    dwReserved1 As Long
    cFileName As String * MAX_PATH
    cAlternate As String * 14
End Type

'-> Constante pour lecture du fichier
Private Const FILE_ATTRIBUTE_ARCHIVE = &H20 '32
Private Const FILE_ATTRIBUTE_COMPRESSED = &H800 '2048
Private Const FILE_ATTRIBUTE_DIRECTORY = &H10 '16
Private Const FILE_ATTRIBUTE_HIDDEN = &H2 '2
Private Const FILE_ATTRIBUTE_NORMAL = &H80 '128
Private Const FILE_ATTRIBUTE_READONLY = &H1 '1
Private Const FILE_ATTRIBUTE_SYSTEM = &H4 '4
Private Const FILE_ATTRIBUTE_TEMPORARY = &H100 '256

'-> Champ d'entete
Dim ScanField As String
Dim ScanFieldValue As String

Public Sub GetDirectory(pOutil As String)

'---> cette proc�dure analyse un r�pertoire et retourne une liste de fichiers

' Ligne de commande = 15|Directory +  �  matches ";"  + � RspFile + � ScanOrder + � ScanType + � ActiveEntete

'ScanOrder :

'NAME
'DATECRE
'DATEMAJ
'DATEACC
'SIZE
'HEADER[FIELD]

'ScanType

'FILE
'DIR
'BOTH

Dim aWin32 As WIN32_FIND_DATA
Dim FilA As FILETIME
Dim FilB As FileListBox
Dim SysTim As SYSTEMTIME
Dim Res As Long
Dim i As Long, j As Integer

Dim ScanDir As String
Dim ScanExtension As String
Dim ScanResult As String
Dim ScanOrder As String
Dim ScanType As String
Dim ScanEntete As Integer

Dim TempFileName As String
Dim hdlFile As Integer

Dim Attribut As Long
Dim hSearch  As Long

Dim TypeFile As String
Dim NameFile As String
Dim DateCre As String
Dim DateMaj As String
Dim DateAcc As String
Dim SizeFile As Long
Dim Extension As String
Dim TriKey As String
Dim EnteteValue As String
Dim ToAdd As Boolean

Dim IsCompressed As Integer
Dim IsArchive As Integer
Dim IsHidden As Integer
Dim IsNormal As Integer
Dim IsReadOnly As Integer
Dim IsSystem As Integer
Dim IsTemporary As Integer

Dim SortColonne As Integer
Dim X As ListItem

'-> Charger la feuille
Load frmTri

'-> R�cup�ration de la ligne de commande
ScanDir = Entry(1, pOutil, "�")
ScanExtension = Entry(2, pOutil, "�")
ScanResult = Entry(3, pOutil, "�")
ScanOrder = UCase$(Trim(Entry(4, pOutil, "�")))
ScanType = UCase$(Trim(Entry(5, pOutil, "�")))
ScanEntete = CInt(Entry(6, pOutil, "�"))

'-> Eclater le ScanOrder si HEADER
If Trim(UCase$(Entry(1, ScanOrder, "["))) = "HEADER" Then
    '-> R�cup�rer le champ
    ScanField = Trim(UCase$(Entry(2, ScanOrder, "[")))
    ScanField = Mid$(ScanField, 1, Len(ScanField) - 1)
    '-> Repositionner le ScanOrder
    ScanOrder = Entry(1, ScanOrder, "[")
End If

'-> Rajouter un "\" si necessaire
If Right$(ScanDir, 1) <> "\" Then ScanDir = ScanDir & "\"

'-> Obtenir un nom de fichier temporaire
TempFileName = GetTempFileNameVB("DIR")
hdlFile = FreeFile

'-> Ouverture du fichier temporaire
Open TempFileName For Output As #hdlFile

'-> Pour toutes les extensions
For j = 1 To NumEntries(ScanExtension, ";")

    '-> Get de l'extension
    Extension = Entry(j, ScanExtension, ";")

    '-> Analyse de la premi�re entr�e
    hSearch = FindFirstFile(ScanDir & Extension, aWin32)
    If hSearch = -1 Then GoTo EndProg
    
    '-> Lecture s�quentielle
    Do
        '-> Vider la cl�
        ScanFieldValue = ""
    
        '-> Recup�ration du nom
        NameFile = Entry(1, aWin32.cFileName, Chr(0))
        
        '-> Ne pas traiter le command.com
        If NameFile = "." Or NameFile = ".." Then GoTo NextRun
        
        '-> Incr�menter
        i = i + 1
        
        '-> Attirbut du fichier
        Attribut = aWin32.dwFileAttributes
        
        '-> Si fichier ou r�pertoire
        If Attribut And FILE_ATTRIBUTE_DIRECTORY Then
            TypeFile = "DIR"
        Else
            TypeFile = "FILE"
        End If
            
        '-> Si Archive
        If Attribut And FILE_ATTRIBUTE_ARCHIVE Then
            IsArchive = 1
        Else
            IsArchive = 1
        End If
        
        '-> Si compress�
        If Attribut And FILE_ATTRIBUTE_COMPRESSED Then
            IsCompressed = 1
        Else
            IsCompressed = 0
        End If
        
        '-> Si Hidden
        If Attribut And FILE_ATTRIBUTE_HIDDEN Then
            IsHidden = 1
        Else
            IsHidden = 0
        End If
        
        '-> Si Normal
        If Attribut And FILE_ATTRIBUTE_NORMAL Then
            IsNormal = 1
        Else
            IsNormal = 0
        End If
        
        '-> Si ReadOnly
        If Attribut And FILE_ATTRIBUTE_READONLY Then
            IsReadOnly = 1
        Else
            IsReadOnly = 0
        End If
        
        '-> Si System
        If Attribut And FILE_ATTRIBUTE_SYSTEM Then
            IsSystem = 1
        Else
            IsSystem = 0
        End If
        
        '-> Si Temporaire
        If Attribut And FILE_ATTRIBUTE_TEMPORARY Then
            IsTemporary = 1
        Else
            IsTemporary = 0
        End If
            
        '-> R�cup�ration de la date de cr�ation
        DateCre = GetTimeFromWin32(aWin32, 1)
        
        '-> R�cup�ration de la date de modification
        DateMaj = GetTimeFromWin32(aWin32, 2)
        
        '-> R�cup�ration de la date de dernier Acc�s
        DateAcc = GetTimeFromWin32(aWin32, 3)
        
        '-> R�cup�ration de la taille
        SizeFile = aWin32.nFileSizeLow
        
        '-> Ne pas ajouter de base
        ToAdd = False
        
        '-> D�terminer ce que l'on doit ajouter
        Select Case ScanType
            Case "DIR"
                If TypeFile = "DIR" Then
                    ToAdd = True
                Else
                    ToAdd = False
                End If
            Case "FILE"
                If TypeFile = "FILE" Then
                    ToAdd = True
                Else
                    ToAdd = False
                End If
            Case "BOTH"
                ToAdd = True
        End Select
                        
        '-> R�cup�rer les cl�s d'entete
        If ScanEntete = 1 And TypeFile = "FILE" Then EnteteValue = GetEnteteValue(ScanDir & NameFile)
                        
        '-> Cl� de tri
        Select Case ScanOrder
            Case "NAME"
                TriKey = NameFile
            Case "DATECRE"
                TriKey = Format(CDbl(CDate(DateCre)), "0000000000000000.000000000000000")
            Case "DATEMAJ"
                TriKey = Format(CDbl(CDate(DateMaj)), "0000000000000000.000000000000000")
            Case "DATEACC"
                TriKey = Format(CDbl(CDate(DateAcc)), "0000000000000000.000000000000000")
            Case "SIZE"
                TriKey = Format(SizeFile, "00000000000000000000.0000000000")
            Case "HEADER"
                TriKey = ScanFieldValue
        End Select
                    
        '-> Ajouter dans la collection
        If ToAdd Then
            '-> Ajouter dans le listview
            Set X = frmTri.ListView1.ListItems.Add(, , TriKey)
            X.SubItems(1) = NameFile & "|" & TypeFile & "|" & DateCre & "|" & DateMaj & "|" & DateAcc & "|" & SizeFile & "|" & IsArchive & "|" & IsCompressed & "|" & IsHidden & "|" & IsNormal & "|" & IsReadOnly & "|" & IsSystem & "|" & IsTemporary & "|" & EnteteValue
        End If
                
NextRun:
    
        '-> Analyse de l'entr�e suivante du fichier
        Res = FindNextFile(hSearch, aWin32)
        '-> Quitter si plus de fichier � analyser
        If Res = 0 Then Exit Do
    
    Loop 'Pour toutes les entr�es du r�pertoires
Next 'Pour toutes les extensions

frmTri.ListView1.Sorted = True

'-> Imprimer le listview
For Each X In frmTri.ListView1.ListItems
    Print #hdlFile, X.SubItems(1)
Next

EndProg:

'-> fermer le fichier Temporaire
Close #hdlFile

'-> Le renommer sous forme de fichier r�ponse
If Dir$(ScanResult) <> "" Then Kill ScanResult
Name TempFileName As ScanResult

'-> Fin du programme
End


End Sub


Function GetEnteteValue(Filename As String) As String

'---> Cette fonction retourne les informations d'entete du fichier

Dim ListeEntete As String
Dim DefEntete As String
Dim ReturnValue As String
Dim i As Integer, j As Integer
Dim iPos As Integer
Dim KeyHeader As String
Dim ValueHeader As String

On Error GoTo GestError

'-> R�cup�rer le contenu des cl�s de l'entete
ListeEntete = GetIniFileValue("HEADER", "", Filename, True)
If Trim(ListeEntete) = "" Then Exit Function

'-> Get du nombre de cl�
j = NumEntries(ListeEntete, Chr(0))

'-> Analyse de toutes les entetes
For i = 1 To j
    '-> Get d'une d�finition
    DefEntete = Entry(i, ListeEntete, Chr(0))
    '-> Quitter si pas de d�finition
    If Trim(DefEntete) = "" Then Exit For
    '-> ReTourner la valeur
    iPos = InStr(1, DefEntete, "=")
    '-> Get de sa cl�
    KeyHeader = Mid$(DefEntete, 1, iPos - 1)
    ValueHeader = Mid$(DefEntete, iPos + 1, Len(DefEntete) - iPos)
    '-> Concatainer la valeur de retour
    If ReturnValue = "" Then
        ReturnValue = KeyHeader & "�" & ValueHeader
    Else
        ReturnValue = ReturnValue & "�" & KeyHeader & "�" & ValueHeader
    End If
    '-> Gestion de la cl� de Tri
    If UCase$(Trim(KeyHeader)) = ScanField Then ScanFieldValue = ValueHeader
    
Next 'Pour toutes les cl�s d'entete

'-> Retourner la valeur
GetEnteteValue = ReturnValue

GestError:
    

End Function


Function GetTimeFromWin32(aWn32 As WIN32_FIND_DATA, TimeToRun As Integer) As String

Dim Fil1 As FILETIME
Dim Fil2 As FILETIME
Dim Sys1 As SYSTEMTIME
Dim strTemp As String
 
'-> Selon la date � attaquer
Select Case TimeToRun
    Case 1 'Date de cr�ation
        Fil1.dwHighDateTime = aWn32.ftCreationTime.dwHighDateTime
        Fil1.dwLowDateTime = aWn32.ftCreationTime.dwLowDateTime
    Case 2 'Date de MAJ
        Fil1.dwHighDateTime = aWn32.ftLastWriteTime.dwHighDateTime
        Fil1.dwLowDateTime = aWn32.ftLastWriteTime.dwLowDateTime
    Case 3 'Date d'acc�s
        Fil1.dwHighDateTime = aWn32.ftLastAccessTime.dwHighDateTime
        Fil1.dwLowDateTime = aWn32.ftLastAccessTime.dwLowDateTime
End Select
    
'-> Convertion au format Locale
FileTimeToLocalFileTime Fil1, Fil2

'-> Convertion au format DOS
FileTimeToSystemTime Fil2, Sys1

'-> Renvoyer la zone
strTemp = Format(Sys1.wYear, "0000") & "/" & Format(Sys1.wMonth, "00") & "/" & Format(Sys1.wDay, "00") & " " & _
          Format(Sys1.wHour, "00") & ":" & Format(Sys1.wMinute, "00") & ":" & Format(Sys1.wSecond, "00")
          
GetTimeFromWin32 = strTemp


End Function

Public Function GetIniFileValue(ByVal Section As String, ByVal Keyname As String, IniFile As String, Optional GetSection As Boolean) As String

'---> Cette proc�dure r�cup�re le contenu d'une cl� dans un fichier ini

Dim Res As Long
Dim lpbuffer As String

On Error Resume Next

'-> Init du buffer
lpbuffer = Space$(20000)

'-> Doit on r�cup�rer la section en entier
If GetSection Then
    Res = GetPrivateProfileSection&(Section, lpbuffer, Len(lpbuffer), IniFile)
Else
    Res = GetPrivateProfileString(Section, Keyname, "", lpbuffer, Len(lpbuffer), IniFile)
End If

If Res <> 0 Then
    lpbuffer = Mid$(lpbuffer, 1, Res)
Else
    lpbuffer = ""
End If

GetIniFileValue = lpbuffer


End Function




