Option Strict Off
Option Explicit On
Public Class ImageObj
    '**********************************************
    '*                                            *
    '* Cette classe contient la d�finition d'un   *
    '* Objet ImageBmp                             *
    '*                                            *
    '**********************************************


    '---> Propri�t�s de l'objet
    Public Nom As String
    Public Fichier_Renamed As String
    Public Left_Renamed As Single
    Public Top As Single
    Public Largeur As Single
    Public Hauteur As Single
    Public Contour As Boolean
    Public IdAffichage As Short
    '-> index propre � l'objet pour la gestion des ordres d'affichage
    Public IdOrdreAffichage As Short
    Public IsRedim As Boolean
    Public Path As String

    '---> Bmp Variable
    Public isVariable As Boolean
    Public UseAssociation As Boolean
    Public IsAutosize As Boolean

    '---> Nom de la section mere
    Public SectionName As String

    Public IdPic As Short
    Public sDefault As String
End Class