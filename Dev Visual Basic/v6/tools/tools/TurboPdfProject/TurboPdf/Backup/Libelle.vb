Option Strict Off
Option Explicit On
Public Class Libelle
    '***************************************************************
    '*                                                             *
    '* Cette classe sert � g�rer les lib�ll�s en multi-langue      *
    '*                                                             *
    '***************************************************************

    '-> Variable de type private qui sert � stoocker les libelles lus dans le fichier ini
    Private Captions() As String
    Private ToolTips() As String
    Public NbKey As Short 'nombre de cl�


    Public Sub SetKeys(ByVal KeysDef As String)

        '---> Procedure charg�e de cr�er les diff�rentes matrices. KeyDefs contient _
        'la liste des cl�s avec un s�parateur chr(0)

        Dim KeyDef As String 'd�finition d'une cle
        Dim KeyValue As String 'valeur de la cl�
        Dim i As Short

        '-> Nb de cle
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        NbKey = NumEntries(KeysDef, Chr(0))
        '-> Redimension des matrices
        'UPGRADE_WARNING: La limite inf�rieure du tableau Captions est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
        ReDim Captions(NbKey)
        'UPGRADE_WARNING: La limite inf�rieure du tableau ToolTips est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
        ReDim ToolTips(NbKey)

        For i = 1 To NbKey

            '-> Intialisation
            KeyDef = Entry(i, KeysDef, Chr(0))
            KeyValue = Entry(2, KeyDef, "=")

            '-> Sauvegarde du libelle
            Captions(i) = Entry(1, KeyValue, "�")

            '-> Analyse de la valeur
            If InStr(1, KeyValue, "�") <> 0 Then ToolTips(i) = Entry(2, KeyValue, "�")

        Next  'pour toutes les cl�s

    End Sub

    Public Function GetCaption(ByVal NumCaption As Short) As String

        '---> Affiche un caption selon son numero

        On Error GoTo GestError

        GetCaption = Captions(NumCaption)
        Exit Function

GestError:
        GetCaption = ""


    End Function

    Public Function GetToolTip(ByVal NumTip As Short) As String

        On Error GoTo GestError

        '---> Affiche un caption selon son numero
        GetToolTip = ToolTips(NumTip)
        Exit Function

GestError:
        GetToolTip = ""


    End Function
End Class