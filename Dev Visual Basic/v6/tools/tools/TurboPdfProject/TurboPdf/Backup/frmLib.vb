Option Strict Off
Option Explicit On

Public Class frmLib
    Inherits System.Windows.Forms.Form

    '-> Cette feuille ne sert qu'� contenir les objets de r�f�rence qui seront clon�s
#Region "Code g�n�r� par le Concepteur Windows Form "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'Cet appel est requis par le Concepteur Windows Form.
        InitializeComponent()
    End Sub
    'Form remplace la m�thode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents RtfSel As System.Windows.Forms.RichTextBox
    Public WithEvents _PicObj_0 As System.Windows.Forms.PictureBox
    Public WithEvents _Rtf_0 As System.Windows.Forms.RichTextBox
    Public WithEvents PicObj As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    Public WithEvents Rtf As Microsoft.VisualBasic.Compatibility.VB6.RichTextBoxArray
    'REMARQUE�: la proc�dure suivante est requise par le Concepteur Windows Form
    'Elle peut �tre modifi�e � l'aide du Concepteur Windows Form.
    'Ne la modifiez pas � l'aide de l'�diteur de code.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLib))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.RtfSel = New System.Windows.Forms.RichTextBox
        Me._PicObj_0 = New System.Windows.Forms.PictureBox
        Me._Rtf_0 = New System.Windows.Forms.RichTextBox
        Me.PicObj = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.Rtf = New Microsoft.VisualBasic.Compatibility.VB6.RichTextBoxArray(Me.components)
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        CType(Me._PicObj_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicObj, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Rtf, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RtfSel
        '
        Me.RtfSel.Location = New System.Drawing.Point(8, 16)
        Me.RtfSel.Name = "RtfSel"
        Me.RtfSel.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
        Me.RtfSel.Size = New System.Drawing.Size(26, 57)
        Me.RtfSel.TabIndex = 3
        Me.RtfSel.Text = ""
        '
        '_PicObj_0
        '
        Me._PicObj_0.BackColor = System.Drawing.SystemColors.Window
        Me._PicObj_0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._PicObj_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._PicObj_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.PicObj.SetIndex(Me._PicObj_0, CType(0, Short))
        Me._PicObj_0.Location = New System.Drawing.Point(159, 80)
        Me._PicObj_0.Name = "_PicObj_0"
        Me._PicObj_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PicObj_0.Size = New System.Drawing.Size(228, 65)
        Me._PicObj_0.TabIndex = 1
        Me._PicObj_0.TabStop = False
        Me._PicObj_0.Visible = False
        '
        '_Rtf_0
        '
        Me._Rtf_0.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Rtf.SetIndex(Me._Rtf_0, CType(0, Short))
        Me._Rtf_0.Location = New System.Drawing.Point(72, 8)
        Me._Rtf_0.Name = "_Rtf_0"
        Me._Rtf_0.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
        Me._Rtf_0.Size = New System.Drawing.Size(666, 153)
        Me._Rtf_0.TabIndex = 0
        Me._Rtf_0.Text = ""
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        Me.NotifyIcon1.Text = "NotifyIcon1"
        '
        'frmLib
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(750, 176)
        Me.Controls.Add(Me.RtfSel)
        Me.Controls.Add(Me._PicObj_0)
        Me.Controls.Add(Me._Rtf_0)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmLib"
        Me.Opacity = 0
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.Text = "Form1"
        CType(Me._PicObj_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicObj, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Rtf, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region


    Private Sub NotifyIcon1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles NotifyIcon1.Click
        '--> on gere ici l'arret de la generation du pdf
        If TopStopApp = "" Then
            If MsgBox("Stopper le processus en cours ?" & Chr(13) & "La g�n�ration du fichier sera arr�t�e � la page en cours", MsgBoxStyle.OkCancel, "TurboPDF") = MsgBoxResult.Ok Then
                TopStopApp = "1"
            Else
                TopStopApp = "0"
            End If
        End If
    End Sub
End Class