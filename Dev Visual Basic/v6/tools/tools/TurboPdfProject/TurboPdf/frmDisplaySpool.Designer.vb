<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDisplaySpool
#Region "Code g�n�r� par le Concepteur Windows Form "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Cet appel est requis par le Concepteur Windows Form.
		InitializeComponent()
		'Ce formulaire est un enfant MDI.
		'Ce code simule 
		' la fonctionnalit� VB6
		' de chargement et d'affichage automatique d'un
		' parent de l'enfant MDI.
        Me.MDIParent = TurboPdf.MDIMain
        TurboPdf.MDIMain.Show()
	End Sub
	'Form remplace la m�thode Dispose pour nettoyer la liste des composants.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requise par le Concepteur Windows Form
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents RtfError As System.Windows.Forms.RichTextBox
	Public WithEvents picAngle As System.Windows.Forms.PictureBox
    Public WithEvents VScroll1 As System.Windows.Forms.VScrollBar
    Public WithEvents ImageList2 As System.Windows.Forms.ImageList
	Public WithEvents TreeView1 As System.Windows.Forms.TreeView
    Public WithEvents ImageList1 As System.Windows.Forms.ImageList
	Public WithEvents lblAccesDet As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	'REMARQUE�: la proc�dure suivante est requise par le Concepteur Windows Form
	'Elle peut �tre modifi�e � l'aide du Concepteur Windows Form.
	'Ne la modifiez pas � l'aide de l'�diteur de code.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisplaySpool))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.RtfError = New System.Windows.Forms.RichTextBox
        Me.picAngle = New System.Windows.Forms.PictureBox
        Me.VScroll1 = New System.Windows.Forms.VScrollBar
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.TreeView1 = New System.Windows.Forms.TreeView
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.lblAccesDet = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(Me.components)
        CType(Me.picAngle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblAccesDet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RtfError
        '
        Me.RtfError.Location = New System.Drawing.Point(16, 240)
        Me.RtfError.Name = "RtfError"
        Me.RtfError.ReadOnly = True
        Me.RtfError.Size = New System.Drawing.Size(121, 113)
        Me.RtfError.TabIndex = 9
        Me.RtfError.Text = "RichTextBox1"
        Me.RtfError.Visible = False
        '
        'picAngle
        '
        Me.picAngle.BackColor = System.Drawing.SystemColors.Control
        Me.picAngle.Cursor = System.Windows.Forms.Cursors.Default
        Me.picAngle.ForeColor = System.Drawing.SystemColors.ControlText
        Me.picAngle.Location = New System.Drawing.Point(312, 96)
        Me.picAngle.Name = "picAngle"
        Me.picAngle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.picAngle.Size = New System.Drawing.Size(25, 25)
        Me.picAngle.TabIndex = 7
        Me.picAngle.TabStop = False
        Me.picAngle.Visible = False
        '
        'VScroll1
        '
        Me.VScroll1.Cursor = System.Windows.Forms.Cursors.Default
        Me.VScroll1.LargeChange = 1
        Me.VScroll1.Location = New System.Drawing.Point(0, 48)
        Me.VScroll1.Maximum = 32767
        Me.VScroll1.Name = "VScroll1"
        Me.VScroll1.Size = New System.Drawing.Size(17, 65)
        Me.VScroll1.TabIndex = 5
        Me.VScroll1.TabStop = True
        Me.VScroll1.Visible = False
        '
        'ImageList2
        '
        Me.ImageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList2.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList2.TransparentColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        '
        'TreeView1
        '
        Me.TreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView1.ImageIndex = 0
        Me.TreeView1.ImageList = Me.ImageList2
        Me.TreeView1.Location = New System.Drawing.Point(16, 72)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.SelectedImageIndex = 0
        Me.TreeView1.Size = New System.Drawing.Size(217, 161)
        Me.TreeView1.TabIndex = 3
        Me.TreeView1.Visible = False
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ImageList1.Images.SetKeyName(0, "Selection.ico")
        Me.ImageList1.Images.SetKeyName(1, "FirstNew.ico")
        Me.ImageList1.Images.SetKeyName(2, "LeftNew.ico")
        Me.ImageList1.Images.SetKeyName(3, "FindPageNew.ico")
        Me.ImageList1.Images.SetKeyName(4, "RightNew.ico")
        Me.ImageList1.Images.SetKeyName(5, "LastNew.ico")
        Me.ImageList1.Images.SetKeyName(6, "PrinterNew.ico")
        Me.ImageList1.Images.SetKeyName(7, "Info.ico")
        Me.ImageList1.Images.SetKeyName(8, "Mail.ico")
        Me.ImageList1.Images.SetKeyName(9, "Html.ico")
        Me.ImageList1.Images.SetKeyName(10, "Excel.ico")
        Me.ImageList1.Images.SetKeyName(11, "Search.ico")
        Me.ImageList1.Images.SetKeyName(12, "SearchNextNew.ico")
        '
        'frmDisplaySpool
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(896, 506)
        Me.Controls.Add(Me.RtfError)
        Me.Controls.Add(Me.picAngle)
        Me.Controls.Add(Me.VScroll1)
        Me.Controls.Add(Me.TreeView1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmDisplaySpool"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "Form1"
        CType(Me.picAngle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblAccesDet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region 
End Class