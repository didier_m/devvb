Option Strict Off
Option Explicit On

Friend Class MDIMain
    Inherits System.Windows.Forms.Form

    Private Sub imgClose_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles imgClose.Click

        '-> Simuler  un click sur le menu Fermeture de l'explorateur de spool
        mnuSpool_Click(mnuSpool, New System.EventArgs())

    End Sub

    Private Sub MDIMain_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        Dim aLb As Libelle

        '-> Activer la gestion des erreurs
        On Error Resume Next

        '-> Charger les libelles par langue
        aLb = Libelles.Item("MDIMAIN")

        '-> Titre de la fen�tre
        Me.Text = aLb.GetCaption(10)

        '-> Menu fichier
        Me.mnuFichier.Text = aLb.GetCaption(1)
        Me.mnuLoadFile.Text = aLb.GetCaption(2)
        Me.mnuPrint.Text = aLb.GetCaption(3)
        Me.mnuExit.Text = aLb.GetCaption(4)

        '-> Menu Fen�tre
        Me.mnuFenetre.Text = aLb.GetCaption(5)
        Me.mnuSpool.Text = aLb.GetCaption(6)
        Me.mnuMosaiqueH.Text = aLb.GetCaption(7)
        Me.mnuMosaiqueV.Text = aLb.GetCaption(8)
        Me.mnuCascade.Text = aLb.GetCaption(9)
        Me.mnuReorIcone.Text = aLb.GetCaption(20)

        '-> Menu DEAL
        Me.mnuApropos.Text = aLb.GetCaption(21)

        '-> Liberer le pointeur sur le libelle
        'UPGRADE_NOTE: L'objet aLb ne peut pas �tre d�truit tant qu'il n'est pas r�cup�r� par le garbage collector (ramasse-miettes). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        aLb = Nothing

        '-> Par d�faut, palette de navigation ferm�e
        If GetIniString("PARAM", "NAVIGA", TurboGraphIniFile, False) = "1" Then
            IsNaviga = True
        Else
            IsNaviga = False
        End If
        Me.picSplit.Visible = IsNaviga
        Me.picNaviga.Visible = IsNaviga

    End Sub


    Private Sub MDIMain_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason

        End

        eventArgs.Cancel = Cancel
    End Sub

    Public Sub mnuCascade_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuCascade.Click

        Me.LayoutMdi(System.Windows.Forms.MdiLayout.Cascade)

    End Sub

    Public Sub mnuCloseFile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuCloseFile.Click

        '-> Demande de fermeture de fichier
        CloseFichier(Me.TreeNaviga.SelectedNode.Name)

    End Sub

    Public Sub mnuDeleteFile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuDeleteFile.Click

        '-> Supprimer de mani�re d�finitive le fichier
        DeleteFile(Me.TreeNaviga.SelectedNode.Name, 0)

    End Sub

    Public Sub mnuExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuExit.Click

        Me.Close()

    End Sub

    Public Sub mnuJoin_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuJoin.Click
        Dim Index As Short = mnuJoin.GetIndex(eventSender)
        '-> Lancer si necessaire
        ShellExecute(Me.Handle.ToInt32, "Open", mnuJoin(Index).Tag, vbNullString, My.Application.Info.DirectoryPath, 1)
    End Sub

    Private Function IsLoadedFile(ByVal NomFile As String) As Boolean

        '---> Cette proc�dure v�rifie si un fichier est d�ja charg�

        Dim aFichier As Fichier

        On Error GoTo GestError

        '-> Pointer sur le fichier
        aFichier = Fichiers.Item(Trim(UCase(NomFile)))

        '-> Renvoyer une valeur de succ�s
        IsLoadedFile = True

        Exit Function

GestError:
        IsLoadedFile = False


    End Function

    Public Sub mnuMosaiqueH_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMosaiqueH.Click

        Me.LayoutMdi(System.Windows.Forms.MdiLayout.TileHorizontal)

    End Sub

    Public Sub mnuMosaiqueV_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMosaiqueV.Click

        Me.LayoutMdi(System.Windows.Forms.MdiLayout.TileVertical)

    End Sub

    Public Sub mnuNotePad_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuNotePad.Click

        Dim aFichier As Fichier
        Dim aSpool As Spool
        Dim aPage As Short
        Dim i, j As Short
        Dim aLb As Libelle
        Dim tmpPafe As String


        On Error GoTo GestError

        '-> Pointer sur le fichier
        aFichier = Fichiers.Item(Entry(1, Me.TreeNaviga.SelectedNode.Name, "�"))

        '-> Charger l'�diteur
        'UPGRADE_ISSUE: L'instruction Load n'est pas pris(e) en charge. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="B530EFF2-3132-48F8-B8BC-D88AF543D321"'
        'DEBUG
        'Load(frmEditor)

        '-> Pointer sur la classe libelle
        aLb = Libelles.Item("FRMEDITOR")

        Me.Enabled = False
        'UPGRADE_WARNING: propri�t� Screen.MousePointer de Screen a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

        '-> Envoyer le titre de l'impression
        AddTexte(aLb.GetCaption(16) & " " & aFichier.FileName & Chr(13) & Chr(10))

        '-> Gestion de la temporisation
        TailleLue = 0
        TailleTotale = 0

        '-> Envoie vers l'�diteur de texte
        Select Case NumEntries(Me.TreeNaviga.SelectedNode.Name, "�")
            Case 1 '-> Envoie du fichier en entier

                '-> Calcul du nombre de lignes � imprimer
                For i = 1 To aFichier.Spools.Count()
                    aSpool = aFichier.Spools.Item(i)
                    For j = 1 To aSpool.NbPage
                        InitTailleTotale(aSpool, j)
                    Next
                Next
                '-> Pour tous les spools du fichier
                For i = 1 To aFichier.Spools.Count()
                    '-> Pointer sur le spool
                    aSpool = aFichier.Spools.Item(i)
                    '-> Envoyer le titre de l'impression
                    AddTexte(aLb.GetCaption(17) & " " & aSpool.Key & Chr(13) & Chr(10))
                    '-> Imprimer toutes les pages
                    For j = 1 To aSpool.NbPage
                        SendPage(aSpool, j)
                        '-> Ajouter le mot cle de saut de page
                        AddTexte("[PAGE]")
                    Next
                Next
            Case 2 '-> Envoie d'un spool
                '-> Pointer sur le spool
                aSpool = aFichier.Spools.Item(Entry(2, Me.TreeNaviga.SelectedNode.Name, "�"))
                '-> Inti de la temporisation
                For i = 1 To aSpool.NbPage
                    InitTailleTotale(aSpool, i)
                Next
                '-> Imprimer toutes les pages du spool
                '-> Envoyer le titre de l'impression
                AddTexte(aLb.GetCaption(17) & " " & aSpool.Key & Chr(13) & Chr(10))
                For j = 1 To aSpool.NbPage
                    SendPage(aSpool, j)
                    '-> Ajouter le mot cle de saut de page
                    AddTexte("[PAGE]")
                Next  '-> Envoie d'une page
            Case 3
                '-> Pointer sur le spool
                aSpool = aFichier.Spools.Item(Entry(2, Me.TreeNaviga.SelectedNode.Name, "�"))
                '-> Envoyer le titre de l'impression
                AddTexte(aLb.GetCaption(17) & " " & aSpool.Key & Chr(13) & Chr(10))
                AddTexte(aLb.GetCaption(18) & " " & CShort(Entry(2, Entry(3, Me.TreeNaviga.SelectedNode.Name, "�"), "|")) & Chr(13) & Chr(10))
                '-> Init de la temporisation
                InitTailleTotale(aSpool, CShort(Entry(2, Entry(3, Me.TreeNaviga.SelectedNode.Name, "�"), "|")))
                '-> Imprimer la page sp�cifi�e
                SendPage(aSpool, CShort(Entry(2, Entry(3, Me.TreeNaviga.SelectedNode.Name, "�"), "|")))
        End Select

GestError:

        Me.Enabled = True
        'UPGRADE_WARNING: propri�t� Screen.MousePointer de Screen a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

        Me.StatusBar1.Refresh()

        '-> Afficher l'�diteur
        frmEditor.Show()


    End Sub

    Private Function InitTailleTotale(ByRef aSpool As Spool, ByRef PageToPrint As Short) As Short

        Dim tmpPage As String


        '-> V�rifier s'il y a des erreurs sur la page
        If aSpool.GetErrorPage(PageToPrint) <> "" Then
            tmpPage = aSpool.GetErrorPage(PageToPrint)
        Else
            tmpPage = aSpool.GetPage(PageToPrint)
        End If
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        TailleTotale = TailleTotale + NumEntries(tmpPage, Chr(0))

    End Function

    Private Sub SendPage(ByRef aSpool As Spool, ByVal PageToPrint As Short)

        '---> Cette proc�dure envoie une page vers l'�diteur de texte

        Dim i As Short
        Dim tmpPage As String

        '-> R�cup�rer la page � imprimer ou ses erreurs
        If aSpool.GetErrorPage(PageToPrint) <> "" Then
            tmpPage = aSpool.GetErrorPage(PageToPrint)
        Else
            tmpPage = aSpool.GetPage(PageToPrint)
        End If

        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(tmpPage, Chr(0)). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        For i = 1 To NumEntries(tmpPage, Chr(0))
            If frmEditor.RichTextBox1.Text = "" Then
                frmEditor.RichTextBox1.Text = Entry(i, tmpPage, Chr(0))
            Else
                frmEditor.RichTextBox1.Text = frmEditor.RichTextBox1.Text & Chr(13) & Chr(10) & Entry(i, tmpPage, Chr(0))
            End If
            TailleLue = TailleLue + 1
            Call DrawWait()
        Next


    End Sub

    Private Sub AddTexte(ByVal TextToAdd As String)

        '---> Cette proc�dure ajoute une ligne de texte � l'�diteur

        If frmEditor.RichTextBox1.Text = "" Then
            frmEditor.RichTextBox1.Text = TextToAdd
        Else
            frmEditor.RichTextBox1.Text = frmEditor.RichTextBox1.Text & Chr(13) & Chr(10) & TextToAdd
        End If

    End Sub


    Public Sub mnuPoubelleFile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuPoubelleFile.Click

        '-> Envoyer le fichier vers la poubelle
        DeleteFile(Me.TreeNaviga.SelectedNode.Name, FOF_ALLOWUNDO)

    End Sub

    Public Sub mnuPrintObj_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuPrintObj.Click

        '---> Cette proc�dure imprimer l'�l�ment s�lectionn�

        Dim PrinterDevice As String
        Dim FileToPrint As String
        Dim aFichier As Fichier
        Dim aSpool As Spool
        Dim NumPage As Short
        Dim NbCopies As Short
        Dim RectoVerso As String


        '-> Vider la variable de retour
        strRetour = ""

        '-> Charger la feuille de l'imprimante
        'UPGRADE_ISSUE: L'instruction Load n'est pas pris(e) en charge. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="B530EFF2-3132-48F8-B8BC-D88AF543D321"'
        'DEBUG
        'Load(frmPrint)

        '-> Bloquer les mauvaises zones
        frmPrint.Frame2.Enabled = False
        frmPrint.Option1.Checked = True
        frmPrint.Option1.Enabled = False
        frmPrint.Option2.Enabled = False
        frmPrint.Option3.Enabled = False
        frmPrint.Option4.Enabled = False

        '-> Selon le node sr lequel on a imprim�
        Select Case Me.TreeNaviga.SelectedNode.Tag
            Case "FICHIER"

                '-> Afficher la feuille
                frmPrint.ShowDialog()

                '-> Quitter si annuler
                If strRetour = "" Then Exit Sub

                '-> R�cup du nom du printer
                PrinterDevice = Entry(1, strRetour, "|")

                '-> Recup du nombre de copies
                NbCopies = CShort(Entry(3, strRetour, "|"))

                '-> recup du parampetre recto-verso
                RectoVerso = Entry(4, strRetour, "|")

                '-> Faire un run directement de l'impression du fichier sp�cifi�
                Shell(My.Application.Info.DirectoryPath & "\TurboGraph.exe " & PrinterDevice & "~DIRECT~" & NbCopies & "|" & Me.TreeNaviga.SelectedNode.Name & "|" & RectoVerso, AppWinStyle.NormalFocus)

            Case "SPOOL"
                '-> Imprimer le fichier en entier s'il n'y a qu'un seul spool dans le fichier

                '-> Afficher la feuille
                frmPrint.ShowDialog()

                '-> Quitter si annuler
                If strRetour = "" Then Exit Sub

                '-> R�cup du nom du printer
                PrinterDevice = Entry(1, strRetour, "|")

                '-> Recup du nombre de copies
                NbCopies = CShort(Entry(3, strRetour, "|"))

                '-> recup du parampetre recto-verso
                RectoVerso = Entry(4, strRetour, "|")

                '-> Pointer sur l'objet sp�cifi�
                aFichier = Fichiers.Item(Entry(1, Me.TreeNaviga.SelectedNode.Name, "�"))

                If aFichier.Spools.Count() = 1 Then
                    '-> Impression du fichier
                    Shell(My.Application.Info.DirectoryPath & "\TurboGraph.exe " & PrinterDevice & "~DIRECT~" & NbCopies & "|" & aFichier.FileName & "|" & RectoVerso, AppWinStyle.NormalFocus)
                Else
                    '-> Pointer sur le spool
                    aSpool = aFichier.Spools.Item(Entry(2, Me.TreeNaviga.SelectedNode.Name, "�"))
                    '-> Cr�er le fichier � imprimer
                    FileToPrint = CreateDetail(aSpool, -1)
                    '-> Lancer l'impression
                    Shell(My.Application.Info.DirectoryPath & "\TurboGraph.exe " & PrinterDevice & "~DIRECT~" & NbCopies & "|" & FileToPrint & "|" & RectoVerso, AppWinStyle.NormalFocus)
                End If

            Case "PAGE"

                '-> Setting de l'option 3
                frmPrint.Option3.Checked = True

                '-> Afficher la feuille
                frmPrint.ShowDialog()

                '-> Quitter si annuler
                If strRetour = "" Then Exit Sub

                '-> R�cup du nom du printer
                PrinterDevice = Entry(1, strRetour, "|")

                '-> Recup du nombre de copies
                NbCopies = CShort(Entry(3, strRetour, "|"))

                '-> recup du parampetre recto-verso
                RectoVerso = Entry(4, strRetour, "|")

                '-> Pointer sur l'objet sp�cifi�
                aFichier = Fichiers.Item(Entry(1, Me.TreeNaviga.SelectedNode.Name, "�"))
                '-> Pointer sur le spool
                aSpool = aFichier.Spools.Item(Entry(2, Me.TreeNaviga.SelectedNode.Name, "�"))
                '-> R�cup�rer le num�ro de la page � imprimer
                NumPage = CShort(Entry(2, Entry(3, Me.TreeNaviga.SelectedNode.Name, "�"), "|"))
                '-> Cr�er le fichier � imprimer
                FileToPrint = CreateDetail(aSpool, NumPage)
                '-> Lancer l'impression
                Shell(My.Application.Info.DirectoryPath & "\TurboGraph.exe " & PrinterDevice & "~DIRECT~" & NbCopies & "|" & FileToPrint & "|" & RectoVerso, AppWinStyle.NormalFocus)
        End Select


    End Sub

    Public Sub mnuProp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuProp.Click

        '---> Affiche la page de propri�t� d'un fichier

        On Error Resume Next

        ShowFileProperties(Me.TreeNaviga.SelectedNode.Name)

    End Sub

    Public Sub mnuReorIcone_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuReorIcone.Click
        Me.LayoutMdi(System.Windows.Forms.MdiLayout.ArrangeIcons)
    End Sub

    Public Sub mnuSpool_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuSpool.Click

        '-> Afficher ou ne pas afficher le volet expmloration de spool
        IsNaviga = Not IsNaviga
        Me.mnuSpool.Checked = IsNaviga
        Me.picSplit.Visible = IsNaviga
        Me.picNaviga.Visible = IsNaviga

    End Sub

    Private Sub picNaviga_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles picNaviga.Resize

        Dim aRect As RECT
        Dim Res As Integer

        On Error Resume Next

        '-> R�cup�rer la taille de la zone cliente
        Res = GetClientRect(Me.picNaviga.Handle.ToInt32, aRect)

        '-> Positionner l'entete de la fen�tre
        Me.picTitre.Left = 3
        Me.picTitre.Top = 7
        Me.picTitre.Width = aRect.Right_Renamed - 6 - imgClose.Width
        Me.imgClose.Left = aRect.Right_Renamed - imgClose.Width
        Me.imgClose.Top = Me.picTitre.Top - 2

        '-> Positionner le treeview
        Me.TreeNaviga.Left = Me.picTitre.Left
        Me.TreeNaviga.Top = Me.picTitre.Top * 2 + Me.picTitre.Height
        Me.TreeNaviga.Width = Me.picTitre.Width + Me.imgClose.Width + 6
        'UPGRADE_ISSUE: PictureBox m�thode picNaviga.ScaleY - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        'Me.TreeNaviga.Height = Me.picNaviga.ScaleY(VB6.PixelsToTwipsY(Me.picNaviga.Height), 1, 3) - 21
        'DEBUG
        Me.TreeNaviga.Height = Me.picNaviga.Height - 21
    End Sub

    Private Sub picSplit_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles picSplit.MouseMove
        Dim Button As Short = eventArgs.Button \ &H100000
        Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
        Dim x As Single = VB6.PixelsToTwipsX(eventArgs.X)
        Dim Y As Single = VB6.PixelsToTwipsY(eventArgs.Y)

        Dim apt As POINTAPI
        Dim Res As Integer

        If Button = VB6.MouseButtonConstants.LeftButton Then
            '-> R�cup�rer la position du curseur
            Res = GetCursorPos(apt)
            '-> Convertir en coordonn�es clientes
            Res = ScreenToClient(Me.Handle.ToInt32, apt)
            '-> Tester les positions mini et maxi
            'UPGRADE_ISSUE: PictureBox m�thode picNaviga.ScaleX - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
            'If apt.x > Me.picNaviga.ScaleX(2, 7, 3) And apt.x < Me.picNaviga.ScaleX(VB6.PixelsToTwipsX(Me.Width), 1, 3) - Me.picNaviga.ScaleX(2, 7, 3) Then Me.picNaviga.Width = VB6.TwipsToPixelsX(Me.picNaviga.ScaleX(apt.x, 3, 1))
            'DEBUG
            If apt.X > Me.picNaviga.Width And apt.X < Me.Width - Me.picNaviga.Width Then Me.picNaviga.Width = Me.picNaviga.Width
        End If

    End Sub

    Private Sub picTitre_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles picTitre.Resize

        '---> Dessiner la barre de titre
        Dim myPen As Pen = New Pen(Color.Black, 3)
        'UPGRADE_ISSUE: PictureBox m�thode picTitre.Line - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        Me.picTitre.CreateGraphics.DrawLine(myPen, 0, 0, Me.picTitre.Width - 1, 0)
        'UPGRADE_ISSUE: PictureBox m�thode picTitre.Line - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        Me.picTitre.CreateGraphics.DrawLine(myPen, 0, 1, Me.picTitre.Width - 1, 1)
        'UPGRADE_ISSUE: PictureBox m�thode picTitre.Line - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        Me.picTitre.CreateGraphics.DrawLine(myPen, 1, 1, Me.picTitre.Width - 1, 1)
        'UPGRADE_ISSUE: PictureBox m�thode picTitre.Line - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        Me.picTitre.CreateGraphics.DrawLine(myPen, 0, 3, Me.picTitre.Width - 1, 3)
        'UPGRADE_ISSUE: PictureBox m�thode picTitre.Line - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        Me.picTitre.CreateGraphics.DrawLine(myPen, 0, 4, Me.picTitre.Width - 1, 4)
        'UPGRADE_ISSUE: PictureBox m�thode picTitre.Line - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        Me.picTitre.CreateGraphics.DrawLine(myPen, 1, 4, Me.picTitre.Width - 1, 4)

    End Sub

    Public Sub TreeNaviga_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles TreeNaviga.DoubleClick

        '---> Traiter l'activation des spools et des pages

        Dim aFichier As Fichier
        Dim aSpool As Spool = Nothing

        '-> Quitter si pas de nodes
        If Me.TreeNaviga.SelectedNode Is Nothing Then Exit Sub

        '-> Pointer sur le fichier associ�
        aFichier = Fichiers.Item(Entry(1, Me.TreeNaviga.SelectedNode.Name, "�"))
        '-> Pointer sur le spool associ�
        If Me.TreeNaviga.SelectedNode.Tag <> "FICHIER" Then aSpool = aFichier.Spools.Item(Entry(2, Me.TreeNaviga.SelectedNode.Name, "�")) 'THIERRY

        '-> selon le node surlequel on click
        Select Case Me.TreeNaviga.SelectedNode.Tag

            Case "FICHIER"
                '-> Ne rien faire
            Case "SPOOL"
                '-> V�rifier si la visu de ce fichier est en cours
                If aSpool.frmDisplay Is Nothing Then InitFRMDISPLAY(aSpool)

                '-> V�rifier s'il y a des erreurs
                If aSpool.NbError = 0 Then
                    '-> Imprimer la page surlaquelle on a cliqu�
                    PrintPageSpool(aSpool, 1) 'CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|"))
                    '-> Afficher la page
                    aSpool.DisplayInterfaceByPage(1) 'CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|"))
                Else
                    aSpool.DisplayInterfaceByPage(1)
                End If

                '-> Afficher la page
                'UPGRADE_WARNING: m�thode frmDisplay.ZOrder de Form a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
                aSpool.frmDisplay.BringToFront()

            Case "ERROR"

                '-> V�rifier si la visu de ce fichier est en cours
                If aSpool.frmDisplay Is Nothing Then InitFRMDISPLAY(aSpool)

                '-> 2 types d'erreurs : soit fatale soit page
                If InStr(1, Me.TreeNaviga.SelectedNode.Name, "PAGE|") <> 0 Then
                    '-> C'est une erreur de page : Afficher la page
                    '-> V�rifier que l'on demande d'afficher une page diff�rente
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If CShort(Entry(NumEntries(Me.TreeNaviga.SelectedNode.Name, "|"), Me.TreeNaviga.SelectedNode.Name, "|")) <> aSpool.CurrentPage Then
                        '-> Imprimer la page
                        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        PrintPageSpool(aSpool, CShort(Entry(NumEntries(Me.TreeNaviga.SelectedNode.Name, "|"), Me.TreeNaviga.SelectedNode.Name, "|")))
                        '-> Afficher la page
                        aSpool.DisplayInterfaceByPage(aSpool.CurrentPage)
                    End If
                    Exit Sub
                Else
                    '-> Erreur fatale

                    '-> Afficher la feuille en fonction du r�sultat de l'impression
                    aSpool.DisplayInterfaceByPage((1))
                End If

                '-> Afficher la page
                'UPGRADE_WARNING: m�thode frmDisplay.ZOrder de Form a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
                aSpool.frmDisplay.BringToFront()

            Case "PAGE"
                '-> V�rifier si la visu de ce fichier est en cours
                If aSpool.frmDisplay Is Nothing Then
                    InitFRMDISPLAY(aSpool)
                    '-> Imprimer la page surlaquelle on a cliqu�
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    PrintPageSpool(aSpool, CShort(Entry(NumEntries(Me.TreeNaviga.SelectedNode.Name, "|"), Me.TreeNaviga.SelectedNode.Name, "|")))
                    '-> Afficher la page
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    aSpool.DisplayInterfaceByPage(CShort(Entry(NumEntries(Me.TreeNaviga.SelectedNode.Name, "|"), Me.TreeNaviga.SelectedNode.Name, "|")))
                Else
                    '-> V�rifier que l'on demande d'afficher une page diff�rente
                    'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If CShort(Entry(NumEntries(Me.TreeNaviga.SelectedNode.Name, "|"), Me.TreeNaviga.SelectedNode.Name, "|")) <> aSpool.CurrentPage Then
                        '-> Imprimer la page
                        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        PrintPageSpool(aSpool, CShort(Entry(NumEntries(Me.TreeNaviga.SelectedNode.Name, "|"), Me.TreeNaviga.SelectedNode.Name, "|")))
                        '-> Afficher la page
                        aSpool.DisplayInterfaceByPage(aSpool.CurrentPage)
                    End If
                End If

                '-> Mettre la feuille au premier plan
                'UPGRADE_WARNING: m�thode frmDisplay.ZOrder de Form a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
                aSpool.frmDisplay.BringToFront()

        End Select


    End Sub

    Public Sub InitFRMDISPLAY(ByRef aSpool As Spool)

        Dim aFrm As frmDisplaySpool

        '-> Cr�er une nouvelle instance
        aFrm = New frmDisplaySpool
        '-> Affect� le spool
        aFrm.aSpool = aSpool
        '-> Affecter la feuille au spool
        aSpool.frmDisplay = aFrm

    End Sub

    Private Sub TreeNaviga_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles TreeNaviga.KeyDown
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000

        Dim aSpool As Spool
        Dim aFichier As Fichier

        If Me.TreeNaviga.SelectedNode Is Nothing Then Exit Sub
        If Me.TreeNaviga.SelectedNode.Tag <> "SPOOL" Then Exit Sub

        '-> Pointer sur le fichier
        aFichier = Fichiers.Item(Entry(1, Me.TreeNaviga.SelectedNode.Name, "�"))

        '-> Pointer sur le spool
        aSpool = aFichier.Spools.Item(Entry(2, Me.TreeNaviga.SelectedNode.Name, "�"))

        If KeyCode = 112 Then
            If aSpool.MaquetteASCII = "" Then
                MsgBox("Maquette sp�cifi�e dans le fichier : " & Chr(13) & Chr(10) & aSpool.Maquette_Renamed.Nom, MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Nom de la maquette")
            Else
                MsgBox(aSpool.MaquetteASCII, MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Nom de la maquette")
            End If
        End If


    End Sub

    Private Sub TreeNaviga_MouseDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles TreeNaviga.MouseDown
        Dim Button As Short = eventArgs.Button \ &H100000
        Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
        Dim x As Single = VB6.PixelsToTwipsX(eventArgs.X)
        Dim Y As Single = VB6.PixelsToTwipsY(eventArgs.Y)

        '-> Teste sur le bouton
        If Button <> VB6.MouseButtonConstants.RightButton Then Exit Sub

        '-> S�lectionner le node
        Me.TreeNaviga.SelectedNode = Me.TreeNaviga.GetNodeAt(x, Y)

        '-> Quitter si pas de node
        If Me.TreeNaviga.SelectedNode Is Nothing Then Exit Sub

        '-> Afficher le menu selon le node
        Select Case Me.TreeNaviga.SelectedNode.Tag
            Case "FICHIER"
                Me.mnuCloseFile.Visible = True
                Me.mnuProp.Visible = True
                Me.mnuDelFile.Visible = True
            Case "SPOOL", "PAGE", "ERROR"
                Me.mnuCloseFile.Visible = False
                Me.mnuProp.Visible = False
                Me.mnuDelFile.Visible = False
        End Select

        '-> Afficher le mennu contextuel
        'UPGRADE_ISSUE: MDIForm m�thode MDIMain.PopupMenu - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        'Me.PopupMenu(Me.mnuPopup)

    End Sub
End Class