Option Strict Off
Option Explicit On
Imports System.Runtime.InteropServices
Imports Microsoft.Win32
Imports System.IO
Imports ICSharpCode.SharpZipLib.Zip
Imports Microsoft.VisualBasic.CompilerServices
Imports System.Text

Public Module fctTurboPrintNew

    '-> Pour passage en v6
    Public V6TurboIniPath As String
    Public V6MqtPath As String

    '---> Indique si on est en version Entreprise (0) ou profressionelle (1)
    Public VersionTurbo As Short
    Public TurboGraphIniFile As String
    Public Tm_PictureIniFile As String
    Public TurboMaqIniFile As String
    Public pdfSite As String
    Public imageRtf As Boolean
    Public tempImg As String
    '---> Pour gestion des Messporg : version Internet
    Public IsAutoVersion As Boolean

    '---> Pour gestion des p�riph�riques de sortie
    Public Mode As Short '-> Indique le mode de consultation envoy� sur la ligne de commande
    Public Sortie As Object '-> Contient le p�riph�rique de sortie
    Public MargeX As Short '-> Marge interne d'un DC axe X pour impression papier
    Public MargeY As Short '-> Marge interne d'un DC axe Y pour impression papier
    Public SpoolSize As Integer '-> Indique le nombre de page par job d'impression
    Public SpoolPrint As Integer '-> Indique le nombre de pages imprim�es dans le spool en cours

    '---> Pour ouverture des fichiers
    Public OpenPath As String 'Indique le path d�faut
    Public FirstOpen As Boolean 'N'en tenir compte que la premi�re fois

    '---> Pour impression de la page des s�lections
    Public Const SelectRTFKey As String = "%%SELECTIONRTF%%@@DIDIER"

    '---> Pour dessin de la temporisation
    Public IsCryptedFile As Boolean

    '---> Pour echange entre les feuilles
    Public strRetour As String

    '---> Pour cr�ation dynamique des controles
    Public IdRTf As Short '-> Contient l'index du prochain Controle RTF qui sera charg�
    Public IdBmp As Short '-> Contient l'index du prochain controle PictureBox qui sera cr��

    '---> Pour les fichiers compress�s pi�ces jointes
    Public arrFILEJOIN As String()
    Public TempRtf As New System.Windows.Forms.RichTextBox
    Public lTime As Long
    '-> fichier d'origine
    Public FileNameZip As String

    '---> Structure de controle de dessin d'une cellule d'un objet tableau
    Public Structure FormatedCell
        Dim Ok As Boolean
        Dim nbDec As Short
        Dim strFormatted As String
        Dim value As Double
        Dim idNegatif As Short
    End Structure

    '---> Liste des collections pour gestion du multi Spool
    Public Fichiers As Collection

    '---> Pour gestion de la cr�ation des blocks de tableau
    Dim sCol() As String
    Dim sLigne() As String
    Dim LigLue As Short

    Public IsMouchard As Boolean

    '-> Pour gestion des
    Public KillSpool As Boolean

    '-> pour l'affichage du bookmark
    Public TopBookmark As Boolean
    '-> pour la gestion du debordement
    Public DebordementTop As Boolean
    Public AllowDebordement As Boolean


    '-> pour la gestion de la navigation
    Public IsNaviga As Boolean

    ' API declarations
    Private Declare Auto Function GetVersionEx Lib "kernel32.dll" (<Runtime.InteropServices.MarshalAs(UnmanagedType.Struct)> ByRef osinfo As OSVERSIONINFOEX) As Int32
    ' Structure definition
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)> _
    Private Structure OSVERSIONINFOEX
        Public dwOSVersionInfoSize As Int32
        Public dwMajorVersion As Int32
        Public dwMinorVersion As Int32
        Public dwBuildNumber As Int32
        Public dwPlatformId As Int32
        <VBFixedString(128), Runtime.InteropServices.MarshalAs(UnmanagedType.ByValTStr, SizeConst:=128)> _
        Public szCSDVersion As String
        Public wServicePackMajor As Int16
        Public wServicePackMinor As Int16
        Public wSuiteMask As Int16
        Public wProductType As Byte
        Public wReserved As Byte
    End Structure
    ' Useful constants
    Private Const VER_PLATFORM_WIN32s As Int32 = &H0
    Private Const VER_PLATFORM_WIN32_WINDOWS As Int32 = &H1
    Private Const VER_PLATFORM_WIN32_NT As Int32 = &H2
    Private Const VER_NT_WORKSTATION As Int32 = &H1
    Private Const VER_NT_SERVER As Int32 = &H3
    ' Terminal server in remote admin mode
    Private VER_SUITE_SINGLEUSERTS As Int32 = &H100&
    Private Const VER_SUITE_PERSONAL As Int32 = &H200&
    Public Enum WindowsVersion
        Undetermined_OS = 0
        Obsolete_OS = 1
        Windows_98 = 2
        Windows_98_SE = 3
        Windows_Me = 4
        Windows_NT4_Workstation = 5
        Windows_NT4_Server = 6
        Windows_2000_Pro = 7
        Windows_2000_Server = 8
        Windows_XP_HomeEdition = 9
        Windows_XP_Pro = 10
        Windows_Net_Server = 11
        Windows_Vista = 12
        Windows_Seven = 13
    End Enum

    Public RTFobj As Collection
    Public RTFlignes As Collection
    Public Mouchard As String
    Public hdlMouchard As Short
    Public PdfFileName As String
    Public TopStopApp As String = ""

    Public aPrinter As New vbPDF
    Public fNotes As Collection
    Public NomFichier As String = ""
    Dim pPageDeb As Long = 999999
    Dim pPageFin As Long = 999999
    Dim pPageGarde As Boolean = False
    Dim nextIcon As System.Drawing.Icon
    Public pdfName As String = ""
    Dim fontLastName As String
    Dim fontLastDir As String
    Public strGarde As String
    Public listImage As Collection

    Private Function InitTurboVersion() As Object

        Dim strPath As String
        Dim i As Short
        Dim Res As Integer
        Dim lpBuffer As String
        Dim TempFileName As String

        '->  D�tection de la version Professionelle: Pr�sence du fichier Turbo.ver et Path

        On Error Resume Next
        InitTurboVersion = True

        '-> Recherche du fichier Version :
        If Dir(My.Application.Info.DirectoryPath & "\Turbo.ver") = "" Then
            '-> On ne trouve pas le fichier Ver donc on est en version Internet.
            VersionTurbo = 1 'Version internet
        Else
            VersionTurbo = 0 'Version Path v6
        End If

        '-> On est en version Entreprise : recherche du path des fichiers Ini et des fichiers Ressources
        If VersionTurbo = 1 Then 'Version internet
            TurboGraphIniFile = "" '-> Pas d'acc�s au fichier TurboGrpah.ini
            Tm_PictureIniFile = "" '-> Pas d'acc�s au fichier Tm_picture.ini
            If Dir(My.Application.Info.DirectoryPath & "\Tm_Picture.ini") <> "" Then
                Tm_PictureIniFile = My.Application.Info.DirectoryPath & "\Tm_Picture.ini"
            End If
            TurboMaqIniFile = "" '-> Pas d'acc�s au fichier TurboMaq.ini
            Exit Function
        Else 'Verdion Entreprise
            '-> Le path du r�pertoire doit �tre renseign� MESSPROG
            If V6TurboIniPath = "" Then
                If Dir(My.Application.Info.DirectoryPath & "\Turbograph.ini") <> "" Then
                    V6TurboIniPath = My.Application.Info.DirectoryPath
                End If
            End If
            If Right(V6TurboIniPath, 1) <> "\" Then V6TurboIniPath = V6TurboIniPath & "\"
            TurboGraphIniFile = V6TurboIniPath & "Turbograph.ini" 'MESSPROG
            If Dir(TurboGraphIniFile) = "" Then
                End
            End If
            '-> Path des fichiers langue
            lpBuffer = My.Application.Info.DirectoryPath
            i = InStrRev(lpBuffer, "\")
            lpBuffer = Mid(lpBuffer, 1, i) & "Mqt\"
            If (GetAttr(lpBuffer) And FileAttribute.Directory) <> FileAttribute.Directory Then 'MESSPROG
                End
            End If
            V6MqtPath = lpBuffer

            '-> Setting du fichier TM_Picture.ini
            Tm_PictureIniFile = V6TurboIniPath & "Tm_Picture.ini"
            If Dir(Tm_PictureIniFile, FileAttribute.Normal) = "" Then Tm_PictureIniFile = ""

            '-> Setting du fichier TurboMaq.ini
            TurboMaqIniFile = V6TurboIniPath & "TurboMaq.ini"
            If Dir(TurboMaqIniFile, FileAttribute.Normal) = "" Then TurboMaqIniFile = ""

        End If

        '-> Initialisation de la version entreprise

        '-> Recherche du fichier Langue
        If TurboGraphIniFile <> "" Then
            '-> Taille du spool
            lpBuffer = GetIniString("PARAM", "SPOOLSIZE", TurboGraphIniFile, False)
            If lpBuffer <> "NULL" Then
                If IsNumeric(lpBuffer) Then SpoolSize = CInt(lpBuffer)
            End If
        End If 'Si on a acc�s au fichier Ini
    End Function

    Public Function GetVariableEnv(ByRef strVariable As String) As String

        Dim Res As Integer
        Dim lpBuffer As String

        lpBuffer = Space(500)
        Res = GetEnvironmentVariable(strVariable, lpBuffer, Len(lpBuffer))
        If Res <> 0 Then
            '-> Faire un trim sur le buffer
            lpBuffer = Mid(lpBuffer, 1, Res)
        Else
            lpBuffer = ""
        End If

        '-> Retouner la valeur
        GetVariableEnv = lpBuffer

    End Function



    Private Sub Initialisation()

        '---> Cette proc�dure est charg�e d'initialiser les diff�rentes variables n�c�ssaires au moteur d'impression

        Dim Res As Integer
        Dim lpBuffer As String

        '-> Attention, il existe 2 versions du TURBO.

        '-> Version Professionelle  -> Libre
        '-> Version Entreprise - > Dans architecture

        '-> Initialiser les index de cr�ation des prochains objets RichTextFormat et BITMAP
        IdRTf = 1
        IdBmp = 1

        '-> Initialiser la collection des fichiers ouverts dans le TurboGraph
        Fichiers = New Collection

        '-> R�cup�ration des informations de formatage num�rique
        lpBuffer = Space(10)
        Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, lpBuffer, Len(lpBuffer))
        SepDec = Mid(lpBuffer, 1, Res - 1)
        lpBuffer = Space(10)
        Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, lpBuffer, Len(lpBuffer))
        SepMil = Mid(lpBuffer, 1, Res - 1)

        '-> R�cup�ration du r�pertoire d'ouverture par d�faut
        lpBuffer = Space(255)
        Res = GetEnvironmentVariable("DEALTEMPO", lpBuffer, Len(lpBuffer))
        If Res <> 0 Then
            OpenPath = Mid(lpBuffer, 1, Res)
            'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            If Dir(OpenPath, FileAttribute.Directory) = "" Then OpenPath = ""
        End If

        '-> R�cup�ration du r�pertoire du param�trage du Turbo dans le cas ou version = 0
        lpBuffer = Space(255)
        Res = GetEnvironmentVariable("TURBOINI", lpBuffer, Len(lpBuffer))
        If Res <> 0 Then V6TurboIniPath = Mid(lpBuffer, 1, Res)

        '-> Lancer l'initialisation de la gestion des versions du Turbo
        Call InitTurboVersion()

    End Sub
    Public LigneCommande As String


    Friend Sub Main()

        '---> Point d'entr�e du nouveau module

        Dim Param1 As String = ""
        Dim Ligne As String = ""
        Dim j, i, k As Long
        Dim aPath As String = ""
        Dim Tempo As String = ""
        Dim TempFileName As String = ""
        Dim aFichier As Fichier
        Dim aspool As Spool
        Dim CountPage As Long

        Dim pFile As String = ""
        Dim pTo As String = ""
        Dim pCopies As String = ""
        Dim pObject As String = ""
        Dim pBody As String = ""
        Dim pBodyFile As String = ""
        Dim pFormat As String = ""
        Dim pFormatBody As String = ""
        Dim pMode As String = ""
        Dim pSpoolName As String = ""
        Dim pDir As String = ""
        Dim KeyValue As String = ""
        Dim pValue As String = ""
        Dim pRun As String = ""
        Dim pCrypt As String = ""
        Dim pPrinter As String = ""
        '************************************
        '* Creation de fichier pdf *
        '************************************
        Trace("START!!!!")
        '-> Activer la gestion des erreurs
        'On Error GoTo InterceptError
        IsMouchard = True
        '-> Lancer l'initialisation
        Call Initialisation()

        '********************************************************************************
        '* ANALYSE DE LA LIGNE DE COMMANDE ET GESTION DES DIFFERENTS MODES D'IMPRESSION *
        '********************************************************************************

        '-> R�cup�ration de la ligne de commande
        LigneCommande = Command()
        LigneCommande = LTrim(LigneCommande)
        Trace(LigneCommande)
        '-> Supprimer les Quotters s'il y en a
        If InStr(1, LigneCommande, """") = 1 Then LigneCommande = Mid(LigneCommande, 2, Len(LigneCommande) - 1)
        If InStr(1, LigneCommande, """") <> 0 Then LigneCommande = Mid(LigneCommande, 1, Len(LigneCommande) - 1)

        '-> on regarde si on a pass� qu'un fichier en argument dans ce cas le convertir directement
        If NumEntries(LigneCommande, "�") = 1 And LigneCommande <> "" Then LigneCommande = "�fileToConvert=" & LigneCommande & "�sortie=ECRAN�embedded"
        If ZonChargGet("rtfToImage") = "O" Then imageRtf = True
        '-> on ouvre la fenetre de choix de fichier si aucun fichier n'a �t� pass� en param�tre
        If ZonChargGet("fileToConvert") = "" Then
            '-> ouvrir la fenetre de choix soit le zone charge, le dealtempo ou c:
            Dim openFileDialog1 As New OpenFileDialog()
            '-> on regarde si un r�pertoire par d�faut a �t� sp�cifi�
            If ZonChargGet("defaultDirectory") <> "" Then
                openFileDialog1.InitialDirectory = ZonChargGet("defaultDirectory")
            Else
                If OpenPath <> "" Then
                    openFileDialog1.InitialDirectory = OpenPath
                Else
                    openFileDialog1.InitialDirectory = "c:\"
                End If
            End If
            openFileDialog1.Filter = "fichier turbo|*.turbo|All files (*.*)|*.*"
            openFileDialog1.FilterIndex = 2
            openFileDialog1.RestoreDirectory = True

            If openFileDialog1.ShowDialog() = DialogResult.OK Then
                '-> on ajoute le fichier a la ligne de commande en mettant la sortie ecran
                LigneCommande = LigneCommande & "�fileToConvert=" & openFileDialog1.FileName & "�sortie=ECRAN"
            End If
        End If
        Trace("Retour :" + LigneCommande)
        '-> on recupere le nom du fichier a traiter
        If ZonChargGet("fileToConvert") <> "" Then
            '-> on verifie si le fichier existe
            If My.Computer.FileSystem.FileExists(ZonChargGet("fileToConvert")) Then
                NomFichier = ZonChargGet("fileToConvert")
            Else
                GoTo InterceptError
            End If
        Else
            '-> pas de fichier � traiter quitter
            End
        End If 'Si la ligne de commande est vide
        Trace("Nom fichier : " + NomFichier)
        '->on affecte un nom au pdf
        Dim pdfDirectory As String = ""
        '-> par defaut le repertoire est celui en cours
        pdfDirectory = My.Computer.FileSystem.GetFileInfo(NomFichier).DirectoryName
        '-> defaut le nom du fichier est celui du spool dont on change l'extension
        pdfName = My.Computer.FileSystem.GetFileInfo(NomFichier).Name
        pdfName = Replace(pdfName, My.Computer.FileSystem.GetFileInfo(NomFichier).Extension, "." & "pdf")
        '-> on regarde si on a sp�cifi� un nom de r�pertoire
        If UCase(ZonChargGet("pdfDirectory")) <> "" Then
            If My.Computer.FileSystem.DirectoryExists(ZonChargGet("pdfDirectory")) Then pdfDirectory = ZonChargGet("pdfDirectory")
        End If
        '-> on regarde si on a sp�cifi� un site pour le bookmark
        If UCase(ZonChargGet("pdfSite")) <> "" Then
            pdfSite = ZonChargGet("pdfSite")
        End If
        '-> on regarde si on a sp�cifi� un nom de fichier
        If UCase(ZonChargGet("pdfName")) <> "" Then pdfName = ZonChargGet("pdfName")
        '-> Et on obtient le nom du fichier pdf
        If Mid(pdfDirectory, Len(pdfDirectory)) = "/" Or Mid(pdfDirectory, Len(pdfDirectory)) = "\" Then
            PdfFileName = pdfDirectory & pdfName
        Else
            PdfFileName = pdfDirectory & "/" & pdfName
        End If
        '-> pendant la construction on a un fichier temp
        PdfFileName = PdfFileName & ".tmp"
        '-> on regarde ici si le fichier spool est comprss� en zip si oui on le d�compresse dans les fichiers tempo
        'decompression dans le fichier temporaire
        FileNameZip = NomFichier
        NomFichier = ExtractArchive(NomFichier, GetTempFileNameVB("", True))
        aPrinter.SetScale(vbPDF.pdfScaleMode.pdfCentimeter)
        '-> on regarde si on doit imprimer la page de garde
        If ZonChargGet("pdfGarde") <> "" Then
            pPageGarde = True
        End If
        '-> on regarde si on a pass� en argument des numeros de page
        If ZonChargGet("pdfMin") <> "" Then
            pPageDeb = CDbl(ZonChargGet("pdfMin"))
        End If
        If ZonChargGet("pdfMax") <> "" Then
            pPageDeb = CDbl(ZonChargGet("pdfMax"))
        End If
        '-> on dertermine la compression du pdf par defaut c'est la comprtession deflate
        If UCase(ZonChargGet("compressionType")) <> "" Then
            Select Case UCase(ZonChargGet("compressionType"))
                Case "NONE"
                    aPrinter.SetCompression(vbPDF.pdfCompression.pdfNothing)
                Case "DEFLATE"
                    aPrinter.SetCompression(vbPDF.pdfCompression.pdfDeflate)
                Case "RUNLENGTH"
                    aPrinter.SetCompression(vbPDF.pdfCompression.pdfRunLengthDecode)
                Case "LZW"
                    aPrinter.SetCompression(vbPDF.pdfCompression.pdfLZWDecode)
                Case Else
                    aPrinter.SetCompression(vbPDF.pdfCompression.pdfDeflate)
            End Select
        Else
            aPrinter.SetCompression(vbPDF.pdfCompression.pdfDeflate)
        End If

        '-> on determine si il y a des protections sur le fichier
        If UCase(ZonChargGet("userPassword")) <> "" Or UCase(ZonChargGet("ownerPassword")) <> "" Or ZonChargGet("typeProtection") <> "" Then
            '-> on parcours les differentes options
            Dim typeProtection As vbPDF.pdfProtectionMode = vbPDF.pdfProtectionMode.pdfAllowViewOnly
            For i = 1 To NumEntries(UCase(ZonChargGet("typeProtection")), ",")

                Select Case Entry(i, UCase(ZonChargGet("typeProtection")), ",")
                    Case "VIEWONLY"
                        typeProtection = typeProtection Or vbPDF.pdfProtectionMode.pdfAllowViewOnly
                    Case "PRINT"
                        typeProtection = typeProtection Or vbPDF.pdfProtectionMode.pdfAllowPrint
                    Case "MODIFY"
                        typeProtection = typeProtection Or vbPDF.pdfProtectionMode.pdfAllowModify
                    Case "COPY"
                        typeProtection = typeProtection Or vbPDF.pdfProtectionMode.pdfAllowCopy
                    Case "FORMS"
                        typeProtection = typeProtection Or vbPDF.pdfProtectionMode.pdfAllowForms
                    Case "ALL"
                        typeProtection = vbPDF.pdfProtectionMode.pdfAllowCopy Or vbPDF.pdfProtectionMode.pdfAllowForms Or vbPDF.pdfProtectionMode.pdfAllowModify Or vbPDF.pdfProtectionMode.pdfAllowPrint
                End Select
            Next
            aPrinter.SetProtection(typeProtection, ZonChargGet("ownerPassword"), ZonChargGet("userPassword"))
        End If

        '-> on s'interesse  ici aux propri�t�es du document
        If UCase(ZonChargGet("docTitle")) <> "" Then
            aPrinter.SetDocProperty(ZonChargGet("docTitle"))
        Else
            aPrinter.SetDocProperty(pdfName)
        End If
        If UCase(ZonChargGet("docSubject")) <> "" Then aPrinter.SetDocProperty(, ZonChargGet("docSubject"))
        If UCase(ZonChargGet("docAuthor")) <> "" Then aPrinter.SetDocProperty(, , ZonChargGet("docAuthor"))
        If UCase(ZonChargGet("docCreator")) <> "" Then aPrinter.SetDocProperty(, , , ZonChargGet("docCreator"))
        If UCase(ZonChargGet("docProducer")) <> "" Then aPrinter.SetDocProperty(, , , , ZonChargGet("docProducer"))
        If UCase(ZonChargGet("docKeywords")) <> "" Then aPrinter.SetDocProperty(, , , , , ZonChargGet("docKeywords"))

        '-> on s'interesse maintenant aux propri�t�es du viewer
        If UCase(ZonChargGet("viewerPreference")) <> "" Then
            '-> on parcours les differentes options
            For i = 1 To NumEntries(UCase(ZonChargGet("viewerPreference")), ",")
                Select Case Entry(i, UCase(ZonChargGet("viewerPreference")), ",")
                    Case "CENTERWINDOW"
                        aPrinter.SetViewerPreferences(vbPDF.pdfViewerPreference.pdfCenterWindow)
                    Case "FITWINDOW"
                        aPrinter.SetViewerPreferences(vbPDF.pdfViewerPreference.pdfFitWindow)
                    Case "FULLSCREEN"
                        aPrinter.SetViewerPreferences(vbPDF.pdfViewerPreference.pdfFullScreen)
                    Case "HIDEMENUBAR"
                        aPrinter.SetViewerPreferences(vbPDF.pdfViewerPreference.pdfHideMenubar)
                    Case "SINGLEPAGE"
                        aPrinter.SetViewerPreferences(vbPDF.pdfViewerPreference.pdfSinglePage)
                    Case "TWOCOLUMN"
                        aPrinter.SetViewerPreferences(vbPDF.pdfViewerPreference.pdfTwoColumnLeft)
                End Select
            Next
        End If
        aPrinter.BeginDoc(PdfFileName)

        '-> on sauvegarde l'icone
        nextIcon = frmLib.NotifyIcon1.Icon

        '-> Lancer l'analyse du spool et impression des feuilles � la vol�e nouveaut�e septembre 2011 pour g�r� les tr�s gros spools
        Trace("avant analysefiletoprint")
        Call AnalyseFileToPrint(NomFichier)
        Trace("youpi")
        '-> on regarde si on doit incorporer le spool turbo en pi�ce jointe
        If UCase(ZonChargGet("Embedded")) <> "" Then
            If arrFILEJOIN Is Nothing Then
                arrFILEJOIN = DirectCast(Utils.CopyArray(DirectCast(arrFILEJOIN, Array), New String(1) {}), String())
            Else
                arrFILEJOIN = DirectCast(Utils.CopyArray(DirectCast(arrFILEJOIN, Array), New String(arrFILEJOIN.Length) {}), String())
            End If
            arrFILEJOIN(arrFILEJOIN.Length - 1) = NomFichier
        End If
        Trace("apres embedded")
        aFichier = Fichiers.Item(UCase(NomFichier))

        If aFichier.Spools.Count = 0 Then
            aPrinter.BeginPage()
            aPrinter.EndPage()
        End If
        Trace("avant suite")
Suite:
        frmLib.NotifyIcon1.Visible = False

        '-> on regarde si on a des fichiers joint au zip en cours
        If Not arrFILEJOIN Is Nothing Then
            For i = 1 To arrFILEJOIN.Length - 1
                aPrinter.AddFileJoin(arrFILEJOIN(i))
            Next
        End If

        '-> on regarde si on donne les autorisation pour les commentaires
        If UCase(ZonChargGet("enableAnnotation")) <> "" Then aPrinter.setPermission()
        Trace("on fait quoi")
        '-> on definit si on doit afficher le pdf
        Select Case UCase(ZonChargGet("Sortie"))
            Case "ECRAN"
                aPrinter.EndDoc(False, False, True)
                aPrinter.OpenDoc()
            Case "AUTOPRINT"
                aPrinter.EndDoc(True, True, False)
            Case "AUTOPRINTCHOIX"
                aPrinter.EndDoc(True, True, True)
            Case Else
                aPrinter.EndDoc(False, False, True)
        End Select
        Trace("on touche au but")
        '-> on determine si on doit buter le fichier turbo d'origine
        If UCase(ZonChargGet("fichierTurbo")) = "KILL" Then
            My.Computer.FileSystem.DeleteFile(NomFichier)
        End If
        If UCase(ZonChargGet("fichierTurbo")) = "RENAME" Then
            My.Computer.FileSystem.RenameFile(NomFichier, NomFichier & ".tmp")
        End If
        Trace("the end")
        '-> Fin du programme
        End
InterceptError:
        If ZonChargGet("noShowSystray") = "" Then frmLib.NotifyIcon1.Visible = True
        frmLib.NotifyIcon1.ShowBalloonTip(300, "Une erreur est survenue lors de la conversion du fichier spool.", PdfFileName & ChrW(13) & Err.Description & ChrW(13) & "Fin du programme...", ToolTipIcon.Error)
        Threading.Thread.Sleep(3000)
        frmLib.NotifyIcon1.Visible = False
        End
    End Sub

    Public Function printSpool(ByVal aspool As Spool, ByVal curPage As Long, ByVal NomFichier As String, ByVal realPage As Long)
        Dim aFichier As Fichier
        'Dim aspool As Spool
        Dim CountPage As Long
        Dim i As Long
        '--> Permet de lancer l'impression d'une page donn�e du spool
        '-> Pointer sur l'objet fichier
        aFichier = Fichiers.Item(UCase(NomFichier))
        '-> Lancer l'impression des pages
        'For Each aspool In aFichier.Spools
        '-> gestion du systray
        If ZonChargGet("noShowSystray") = "" Then frmLib.NotifyIcon1.Visible = True
        If curPage = 3 Then
            frmLib.NotifyIcon1.ShowBalloonTip(10, "TurboPDF G�n�ration du fichier pdf", pdfName & ChrW(13) & "En cours...", ToolTipIcon.Info)
        End If
        '-> Initialiser le compteur de page imprim� dans le spool
        SpoolPrint = 0

        If InStr(1, aspool.GetPage(realPage), "[") = 0 And curPage <> 0 Then Exit Function

        '-> Tester s'il ya des erreurs
        If aspool.NbError = 0 Then
            '-> Tester si on doit imprimer la page des s�lections
            If aspool.IsSelectionPage Then
                CountPage = 0
            Else
                CountPage = 1
            End If

            i = curPage
            '-> on test si on doit imprimer la page
            If (i >= pPageDeb Or pPageDeb = 999999) And (i <= pPageFin Or pPageFin = 999999) Or (pPageGarde = True And i = 0) Then
                '-> Tester s'il y a des erreurs sur la page sp�cifi�e
                '-> gestion du systray
                If Len("G�n�ration du fichier : " & pdfName & Chr(13) & "page : " & i) >= 64 Then
                    frmLib.NotifyIcon1.Text = Mid(pdfName, 1, 40) & Chr(13) & "page : " & i & "/" & aspool.NbPage
                Else
                    frmLib.NotifyIcon1.Text = "G�n�ration du fichier : " & pdfName & Chr(13) & "page : " & i
                End If
                '-> Imprimer la page
                If realPage = 0 Then
                    PrintPageSpool(aspool, 0, realPage)
                Else
                    PrintPageSpool(aspool, 1, realPage)
                End If
                Trace("page reelle ")
                '-> Incr�menter le compteur de page imprim�
                SpoolPrint = SpoolPrint + 1
                '-> on g�re ici le sytray pour le stop de l'impression
                System.Windows.Forms.Application.DoEvents()
                If TopStopApp = "0" Then TopStopApp = ""
                If TopStopApp = "1" Then GoTo Suite
                '-> on g�re ici l'affichage de l'icone
                If CInt(i / 2) = CDbl(i / 2) Then
                    frmLib.NotifyIcon1.Icon = frmLib.Icon
                Else
                    frmLib.NotifyIcon1.Icon = nextIcon
                End If
                Trace("bas")
            End If
        Else
        End If
        '-> Fin du document pour ce spool
        'Next aspool
Suite:
    End Function


    Public Function ZonChargGet(ByVal StrZonCharg As String) As String

        '--> cette fonction retourne la valeur d'un zoncharge
        Dim zcharge As String
        Dim ztrav As String
        Dim i As Integer

        zcharge = LigneCommande

        For i = 2 To NumEntries(zcharge, "�")
            ztrav = UCase$(Trim(Entry(1, Entry(i, zcharge, "�"), "=")))
            Select Case ztrav
                Case UCase(StrZonCharg)
                    ZonChargGet = Trim(Entry(2, Entry(i, zcharge, "�"), "="))
                    Exit For
            End Select
        Next

    End Function


    Private Sub GestError(ByVal ErrorCode As Short, Optional ByRef strError As String = "", Optional ByRef MySpool As Spool = Nothing)

        '---> Cette proc�dure  effectue la gestion centralis�e des erreurs


    End Sub


    Public Sub AnalyseFileToPrint(ByVal NomFichier As String)

        '---> Cette proc�dure charge le ficher � analyser, cr�er la collection des spools et les maquettes
        Dim hdlFile As Short ' handle d'ouverture de fichier
        Dim Ligne As String 'lecture d'une ligne du fichier ascii
        Dim aspool As Spool = Nothing 'pointeur vers un objet spool
        Dim NomMaquette As String 'nom de la maquette � ouvrir pour ancienne version
        Dim aFichier As Fichier 'Pointeur vers un objet fichier
        Dim LectureMaquette As Boolean 'Indique que l'on est en train de lire la d�finition de la maquette
        Dim LectureSelection As Boolean 'Indique que l'on est en train de lire une page de s�lection

        Dim IsSpool As Boolean 'cette variable indique que l'on est en cours de cr�ation d'un spool
        Dim Page As Long 'Compteur de page par spool
        Dim FindMaqChar As Boolean 'indique de plus charger les lignes de la maquettes car on analyse la maquette caract�re
        Dim aOf As OFSTRUCT = Nothing
        Dim ErrorCode As Short
        Dim i As Short
        Dim lastPage As Long

        Dim IsAccDet As Boolean '-> Indique que l'on est dans le fichier acc�s d�tail
        Dim IsSelection As Boolean
        Dim strSelection As String
        Dim nbPageTot As Long
        Dim topGarde As Boolean

        'On Error GoTo GestError
        '-> on regarde si il y a des notes a charger
        Trace("av load note")
        Call loadNote(FileNameZip)
        Trace("ap load note")
        '-> Obtenir un handle de fichier et ouvrir le spool � imprimer
        hdlFile = FreeFile()


        Dim aSourceFile As New StreamReader(NomFichier, System.Text.Encoding.GetEncoding("Windows-1252")) 'Stream pour la lecture
        Ligne = aSourceFile.ReadLine()
        '-> Boucle d'analyse du fichier
        Do While Not Ligne Is Nothing
            '-> Decrypter la source
            Ligne = DeCrypt(Ligne)
            '-> Analyse des diff�rents cas que l'on peut rencontrer
            If InStr(1, Ligne, "%%GUI%%") <> 0 Then

            Else 'si on n'a pas trouv� %%GUI%%
                Select Case Trim(UCase(Ligne))
                    Case "[SPOOL]"
                        If IsSelection Then
                            IsSelection = False
                            strGarde = strSelection
                        End If
                    Case "[/MAQ]"
                        nbPageTot = nbPageTot + 1
                    Case "[PAGE]"
                        nbPageTot = nbPageTot + 1
                    Case "[GARDEOPEN]"
                        topGarde = True
                        IsSelection = True
                    Case "[GARDECLOSE]"
                        strGarde = strSelection
                        IsSelection = False
                        'nbPageTot = nbPageTot - 1     '2014/04/22 pb manque une page
                    Case Else
                        If Trim(UCase$(Mid(Ligne, 1, 7))) = "[SPOOL]" And Ligne <> "[SPOOL]" Then
                            topGarde = True
                        End If
                        If IsSelection Then
                            If strSelection = "" Then
                                strSelection = Ligne
                            Else
                                strSelection = strSelection + Chr(0) + Ligne
                            End If
                        End If
                End Select 'Selon la ligne que l'on est en train de lire
            End If 'si on a trouv� %%GUI%%
            Ligne = aSourceFile.ReadLine()
        Loop  'Boucle d'analyse du fichier
        aSourceFile.Close()

        Dim SourceFile As New StreamReader(NomFichier, System.Text.Encoding.GetEncoding("Windows-1252")) 'Stream pour la lecture
        'FileOpen(hdlFile, NomFichier, OpenMode.Input)

        '-> Cr�er un nouvel objet Fichier et l'ajouter dans la collection
        aFichier = New Fichier
        aFichier.FileName = NomFichier
        aFichier.FileNameZip = FileNameZip

        Fichiers.Add(aFichier, Trim(UCase(NomFichier)))

        Ligne = SourceFile.ReadLine()
        Trace("av boucle")
        '-> Boucle d'analyse du fichier
        Do While Not Ligne Is Nothing
            ErrorCode = 4
            'Ligne = LineInput(hdlFile)
            '-> Decrypter la source
            Ligne = DeCrypt(Ligne)
            Trace("..." + Ligne)
            '-> Analyse des diff�rents cas que l'on peut rencontrer
            If InStr(1, Ligne, "%%GUI%%") <> 0 Then
                '-> R�cup�rer le nom de la maquette
                NomMaquette = Trim(Mid(Ligne, 8, Len(Ligne) - 7))
                '-> Cr�er un nouvel objet Spool
                aspool = aFichier.AddSpool
                If strGarde <> "" Then aspool.IsSelectionPage = True
                '-> Indiquer le num�ro du spool
                aspool.Num_Spool = aFichier.Spools.Count()
                aspool.FileName = aFichier.FileName
                aspool.MaquetteASCII = NomMaquette
                aspool.NbPageR = nbPageTot - 1
                aspool.AddPage()
                '-> Ancienne version : charger la maquette depuis son adresse
                aFichier.IsOldVersion = True
                MaqLect(aspool, NomMaquette)
                '-> Indiquer que l'on est en cr�ation d'un spool
                IsSpool = True
                '-> Premi�re page
                Page = 1
                '-> Ajouter une nouvelle page dans le spool
                'aSpool.AddPage
                '-> RAZ des variables de positionnement
                LectureMaquette = False
                LectureSelection = False
            ElseIf Trim(UCase$(Mid(Ligne, 1, 7))) = "[SPOOL]" And Ligne <> "[SPOOL]" Then
                If Not aspool Is Nothing Then
                    If aspool.NbPage <> lastPage Or lastPage <> SpoolPrint Then
                        If Trim(aspool.GetPage(Page)) <> "" Then
                            If aspool.Maquette_Renamed Is Nothing Then
                                InitCom(aspool)
                            End If
                            printSpool(aspool, aspool.NbPage, NomFichier, Page)
                        End If
                    End If
                End If
                '-> Cr�ation d'un nouveau spool
                '-> Cr�ation d'un nouveau spool
                aspool = aFichier.AddSpool
                aspool.SpoolText = Mid(Ligne, 8)
                '-> on se reprend la derniere maquette s'il y a
                If aFichier.NbSpool > 1 Then
                    For i = 1 To NumEntries(aFichier.Spools(aFichier.NbSpool - 1).GetMaq, Chr(0))
                        '-> R�cup�ration de la d�finition de la ligne
                        aspool.SetMaq(Entry(i, aFichier.Spools(aFichier.NbSpool - 1).GetMaq, Chr(0)))
                    Next
                End If
                aspool.AddPage()
                '-> Indiquer le nom du fichier maitre
                aspool.FileName = aFichier.FileName
                '-> Indiquer le num�ro du spool
                aspool.Num_Spool = aFichier.Spools.Count()
                aspool.NbPageR = nbPageTot - 1
                '-> Indiquer que l'on est en cr�ation de spool
                IsSpool = True
                '-> Indiquer que l'on n'est plus dans l'acc�s au d�tail
                IsAccDet = False
                '-> On n'est pas encore en train de lire la maquette
                LectureMaquette = False
                LectureSelection = False
                FindMaqChar = False
                '-> Init du compteur de page
                Page = 1
                '-> Init de la premi�re page
                'aSpool.AddPage
            Else 'si on n'a pas trouv� %%GUI%%
                Select Case Trim(UCase(Ligne))
                    Case "[SPOOL]"
                        If Not aspool Is Nothing Then
                            If aspool.NbPage <> lastPage Or lastPage <> SpoolPrint Then
                                If Trim(aspool.GetPage(Page)) <> "" Then printSpool(aspool, aspool.NbPage, NomFichier, Page)
                            End If
                        End If
                        '-> Cr�ation d'un nouveau spool
                        aspool = aFichier.AddSpool
                        aspool.AddPage()
                        '-> Indiquer le nom du fichier maitre
                        aspool.FileName = aFichier.FileName
                        '-> Indiquer le num�ro du spool
                        aspool.Num_Spool = aFichier.Spools.Count()
                        aspool.NbPageR = nbPageTot - 1
                        '-> Indiquer que l'on est en cr�ation de spool
                        IsSpool = True
                        '-> Indiquer que l'on n'est plus dans l'acc�s au d�tail
                        IsAccDet = False
                        '-> On n'est pas encore en train de lire la maquette
                        LectureMaquette = False
                        LectureSelection = False
                        FindMaqChar = False
                        '-> Init du compteur de page
                        Page = 1
                        '-> Init de la premi�re page
                        'aSpool.AddPage
                    Case "[/SPOOL]"
                        '-> Indiquer que 'on n'est plus dans un spool
                        IsSpool = False
                    Case "[MAQ]"
                        LectureMaquette = True
                        LectureSelection = False
                    Case "[/MAQ]"
                        LectureMaquette = False
                        'on charge la maquette pour le spool
                        InitCom(aspool)
                        If topGarde And strGarde <> "" Then
                            If strGarde <> "" Then aspool.IsSelectionPage = True
                            '-> Indiquer le num�ro du spool
                            aspool.AddPage()
                            aspool.SetPage(0, strGarde)
                            printSpool(aspool, 0, NomFichier, 0)
                            aspool.IsSelectionPage = False
                            strGarde = ""
                            topGarde = False
                        End If

                    Case "[MAQCHAR]"
                        FindMaqChar = True
                    Case "[GARDEOPEN]"
                        LectureSelection = True
                        LectureMaquette = False
                        '-> Indiquer  dans le spool en cours qu'il ya une page de s�lection
                        aspool.IsSelectionPage = True
                    Case "[GARDECLOSE]"
                        LectureSelection = False
                    Case "[PAGE]"
                        '-> on imprime �ventuellement la page pr�cedente
                        If aspool.Maquette_Renamed Is Nothing Then
                            InitCom(aspool)
                        End If
                        If Trim(aspool.GetPage(Page)) <> "" Then printSpool(aspool, Page, NomFichier, Page)
                        Trace("apres print spool")
                        lastPage = Page
                        'aspool.DelPage(Page - 1)
                        '-> Incr�menter le compteur de page
                        Page = Page + 1
                        '-> Ajouter une nouvelle page dans le spool
                        aspool.AddPage()
                        If TopStopApp = "1" Then GoTo suite
                    Case "[DETAIL]"
                        '-> On passe en mode lecture sur le spool en cours
                        IsAccDet = True
                    Case "[/DETAIL]"
                        '-> Fin du mode lecture de l'acc�s au d�tail
                        IsAccDet = False
                    Case Else
                        '-> Ne traiter la ligne que si on est dans un spool
                        If IsSpool Then
                            '-> Selon la nature du traitement
                            If IsAccDet Then
                                '-> Ne rien faire si on est en cours de lecture acces d�tail
                                If Trim(Ligne) <> "" Then aspool.AddLigneDetail(Ligne)
                            ElseIf LectureMaquette Then
                                '-> Ajouter une ligne dans la d�finition de la maquette
                                If Not FindMaqChar Then aspool.SetMaq(Ligne)
                            ElseIf LectureSelection Then
                                '-> Ajouter une ligne dans la page de s�lection
                                aspool.SetPage(0, Ligne)
                            Else
                                '-> ajouter une ligne de datas dans la page en cours
                                aspool.SetPage(Page, Ligne)
                                If lastPage <> Page And Trim(Ligne) <> "" Then
                                    lastPage = Page
                                End If
                            End If 'Selon la nature de la ligne
                        End If 'si on est dans un spool
                End Select 'Selon la ligne que l'on est en train de lire
            End If 'si on a trouv� %%GUI%%
            '-> Lecture de la ligne
            Ligne = SourceFile.ReadLine()

        Loop  'Boucle d'analyse du fichier
suite:
        Trace("presquefini")
        ErrorCode = 5
        '-> Fermer le fichier ascii
        SourceFile.Close()
        If aspool.IsSelectionPage And aspool.CurrentPage = 0 And strGarde <> "" Then
            If Trim(aspool.GetPage(0)) <> "" Then
                If aspool.Maquette_Renamed Is Nothing Then
                    InitCom(aspool)
                End If
                printSpool(aspool, 0, NomFichier, 0)
            End If
        End If
        If aspool.NbPage <> lastPage Or lastPage <> SpoolPrint Then
            If Trim(aspool.GetPage(Page)) <> "" Then printSpool(aspool, aspool.NbPage, NomFichier, Page)
        End If
        '-> on regarde si il y a des notes a charger
        'Call loadNote(aFichier.FileNameZip)

        '-> Si on n' a pas trouv� de spool, en cr�er un pour l'erreur
        'If aFichier.Spools.Count() = 0 Then
        ''-> D�router sur la gestion d'erreur
        'ErrorCode = 6
        'GoTo GestError
        'End If

        '-> Supprimer la derni�re page si elle est � blanc
        'Dim counter As Short
        'For Each aspool In aFichier.Spools
        'counter = aspool.NbPage
        'For i = counter To 1 Step -1
        'If Trim(aspool.GetPage(i)) = "" Then
        'aspool.NbPage = aspool.NbPage - 1
        'Else
        '-> Ne supprimer qe les pages de fin qui sont blanches
        'Exit For
        'End If
        'Next
        'Next aspool

        '-> supprimer le spool si il est vide
        'i = 1
        'Do While i <= aFichier.Spools.Count()
        'If aFichier.Spools.Item(i).NbPage = 0 Then
        'aFichier.NbSpool = aFichier.NbSpool - 1
        'aFichier.Spools.Remove(i)
        'Else
        'i = i + 1
        'End If
        'Loop

        '-> G�n�rer les maquettes tous les spools
        'For Each aspool In aFichier.Spools
        'InitCom(aspool)
        'Next aspool

        Exit Sub

GestError:


        '-> Traitement de l'erreur
        Select Case ErrorCode
            Case 1
                '-> Pb lors de la r�cup�ration de la taille du fichier : ne rien faire
                Resume Next
            Case 2, 3, 4, 6
                '-> Erreur fatale
                Call GestError(ErrorCode, NomFichier)
            Case 5
                '-> Erreur lors de la fermture du fichier ASCII : Fermer le fichier
                Reset()
                '-> Rendre la main
        End Select


    End Sub

    Private Function OpenRTFSpool(ByVal RtfName As String) As Short

        '---> Ouverture d'un spool pour stocker le contenu RTF de la section/cadre

        Dim RtfFile As String
        Dim hdlRtf As Short

        '-> R�cup�ration du r�pertoire temporaire de windows
        RtfFile = GetTempFileNameVB("", True)
        hdlRtf = FreeFile()
        FileOpen(hdlRtf, RtfFile & "\" & RtfName, OpenMode.Output)

        '-> Renvoyer son handle
        OpenRTFSpool = hdlRtf

    End Function

    Private Sub MaqLect(ByRef aspool As Spool, ByVal aMaq As String)

        '---> Cette proc�dure charge le fichier ASCII d'une maquette dans un objet spool : compatibilite ancienne version

        Dim hdlFile As Short
        Dim TmpFile As String
        Dim Ligne As String
        Dim ErrorCode As Short
        Dim TmpError As String

        On Error GoTo GestError

        '-> Setting du nom de la maquette dans l'objet spool
        aspool.MaquetteASCII = aMaq

        '-> V�rifier que le fichier Maquette est bien pr�sent
        ErrorCode = 7
        TmpError = aspool.FileName & "|" & aMaq
        'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        If Dir(aMaq, FileAttribute.Normal) = "" Then
            '-> Setting d'une erreur
            Call GestError(7, TmpError, aspool)
            '-> Sortir de la proc�dure
            Exit Sub
        End If

        '-> Cr�er un fichier Tempo et faire une copie de la maquette
        ErrorCode = 8
        TmpFile = GetTempFileNameVB("MAQ")
        CopyFile(aMaq, TmpFile, False)


        ErrorCode = 9
        '-> Ouvrir le fichier Tempo
        hdlFile = FreeFile()
        FileOpen(hdlFile, TmpFile, OpenMode.Input)

        '-> Transf�rer la maquette dans l'objet Spool
        Do While Not EOF(hdlFile)
            '-> Lecture de la ligne
            ErrorCode = 10
            Ligne = LineInput(hdlFile)
            If Trim(Ligne) = "" Then GoTo NextLigne
            '-> Decrypter la maquette
            Ligne = DeCrypt(Ligne)
            '-> Quitter si on arrive � la d�finition de la maquette caract�re
            If UCase(Trim(Ligne)) = "[MAQCHAR]" Then Exit Do
            '-> Transfert de la ligne
            aspool.SetMaq((Ligne))
NextLigne:
        Loop

        ErrorCode = 11
        '-> Fermer le fichier ASCII
        FileClose(hdlFile)

        ErrorCode = 12
        '-> Supprimer le fichier Tempo
        Kill(TmpFile)

        '-> Indiquer que la maquette est OK
        aspool.Ok = True

        Exit Sub

GestError:

        '-> Traiter l'erreur selon le cas
        Select Case ErrorCode
            Case 7
                Call GestError(ErrorCode, TmpError, aspool)

            Case 11, 12 '-> Erreurs r�cup�rables
                '-> Fermer tous les fichiers ouverts
                Reset()
                '-> Continuer
                Resume Next
            Case Else
                '-> Appeler la gestion des erreurs
                Call GestError(ErrorCode, aspool.FileName & "|" & aMaq & "|" & TmpFile, aspool)
        End Select

    End Sub

    Private Sub InitCom(ByRef aspool As Spool)

        '---> Cette proc�dure initialise pour un fichier donn� la maquette associ�e

        Dim Ligne As String '-> Pour lecture d'une ligne de maquette
        Dim i As Long '-> Compteur de ligne
        Dim SwitchLect As Short
        '-> Valeur de SwitchLect :
        '0  -> Nothing
        '1  -> Entete Maquette
        '2  -> Section PROGICIEL
        '3  -> Section de Texte : Propri�t�s
        '4  -> D�finition BMP
        '5  -> Definition d'un Cadre
        '6  -> Definition RTF d'un cadre
        '7  -> D�finition RTF d'une section
        '8  -> D�fintion d'un tableau
        '9  -> D�fintion d'un block de tableau
        '10 -> D�fintion d'une cellule de tableau
        '11 -> Ecran de s�lection

        Dim LibError As String '-> Libelle si erreur
        Dim TempoStr As String '-> Variable de stockage tempo

        '-> D�finition du model COM
        Dim aSection As Section = Nothing
        Dim aCadre As Cadre
        Dim aBmp As ImageObj
        Dim aTb As Tableau = Nothing
        Dim aBlock As Block
        Dim aCell As Cellule
        Dim ErrorCode As Short
        Dim TmpError As String

        On Error GoTo GestError

        '-> Type de ligne par d�faut
        SwitchLect = 0

        aBlock = New Block
        aBmp = New ImageObj
        aCadre = New Cadre

        '-> Initialisation de la maquette
        aspool.Maquette_Renamed = New Maquette

        '-> Analyse de la maquette propre au Spool
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(aspool.GetMaq, Chr(0)). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        For i = 1 To NumEntries(aspool.GetMaq, Chr(0))
            '-> R�cup�ration de la d�finition de la ligne
            Ligne = Entry(i, aspool.GetMaq, Chr(0))
            '-> Tester le premier caract�re de la ligne
            If Left(Ligne, 1) = "[" Then
                '-> Analyse du type d'objet
                If UCase(Mid(Ligne, 2, 2)) = "NV" Then
                    '-> Si on attaque le descriptif OBJECT ou la definition TRF
                    If UCase(Entry(3, Ligne, "-")) = "STD]" Then
                        '-> Cr�ation du nouvel objet Section de texte
                        aSection = New Section
                        aSection.Nom = Entry(2, Ligne, "-")
                        '-> Ajout dans la collection
                        ErrorCode = 11
                        TmpError = UCase(Trim(aSection.Nom))
                        aspool.Maquette_Renamed.Sections.Add(aSection, UCase(Trim(aSection.Nom)))
                        '-> Ligne de d�finition d'une section de texte
                        SwitchLect = 3
                    ElseIf UCase(Entry(3, Ligne, "-")) = "RTF]" Then
                        '-> Initialiser pour la section en cours un fichier et l'ouvrir
                        TempoStr = InitFileTempoRtf(aspool)
                        aSection.TempoRTF = CShort(Entry(1, TempoStr, "|"))
                        aSection.TempoRtfString = Entry(2, TempoStr, "|")
                        '-> Ligne de d�finition RTF de la section
                        SwitchLect = 7
                    End If
                ElseIf UCase(Mid(Ligne, 2, 2)) = "TB" Then
                    '-> Cr�ation du nouvel objet tableau
                    aTb = New Tableau
                    aTb.Nom = Mid(Entry(2, Ligne, "-"), 1, Len(Entry(2, Ligne, "-")) - 1)
                    aTb.IdAffichage = aspool.Maquette_Renamed.AddOrdreAffichage("TB-" & UCase(Trim(aTb.Nom)))
                    '-> Ajout dans la collection
                    ErrorCode = 12
                    TmpError = UCase(Trim(aTb.Nom))

                    aspool.Maquette_Renamed.Tableaux.Add(aTb, UCase(Trim(aTb.Nom)))
                    '-> Positionner le pointeur de lecture
                    SwitchLect = 8
                Else
                    If UCase(Trim(Ligne)) = "[PROGICIEL]" Then
                        '-> analyse de la cle PROGICIEL de la maquette
                        SwitchLect = 2
                    End If
                End If '-> Si [
            Else
                'ElseIf InStr(1, Ligne, "\") <> 0 Then
                Select Case UCase(Trim(Ligne))
                    Case "\DEFENTETE�BEGIN"
                        '-> D�finition de l'entete de la maquette
                        SwitchLect = 1
                    Case "\DEFENTETE�END"
                        '-> Envoyer dans la proc�dure d'ajout adaptation des dimensions
                        InitMaqEntete((aspool.Maquette_Renamed), Ligne, aspool)
                        '-> Fin de la d�inition de l'entete de la maquette
                        SwitchLect = 0
                    Case "\BMP�END"
                        '-> Lancer la cr�ation physique de l'objet BMP
                        CreateBmpObj(aBmp, aspool)
                        '-> Fin de d�fintion d'un objet BMP : repositionner sur pointeur de section
                        SwitchLect = 3
                    Case "\CADRE�END"
                        '-> Lancer la cr�ation physique du cadre
                        CreateCadreObj(aCadre, aspool)
                        '-> Fin de d�finition d'un objet cadre : repositionner sur pointeur de section
                        SwitchLect = 3
                    Case "\CADRERTF�BEGIN"
                        '-> Initialiser le fichier RTF pour lecture
                        TempoStr = InitFileTempoRtf(aspool)
                        aCadre.TempoRTF = CShort(Entry(1, TempoStr, "|"))
                        aCadre.TempoRtfString = Entry(2, TempoStr, "|")
                        '-> D�but de d�finition RTF d'un cadre
                        SwitchLect = 6
                    Case "\CADRERTF�END"
                        '-> Fermer le fichier tempo RTF affect�
                        ErrorCode = 13
                        FileClose(aCadre.TempoRTF)
                        '-> Lancer la g�n�ration physique du contenu RTF du cadre
                        CreateRTFCadre(aCadre, aspool)
                        '-> Fin de d�fintion RTF d'un cadre : repositionner sur pointeur de section
                        SwitchLect = 3
                    Case "\RTF�END"
                        '-> Fermer le fichier tempo RTF affect�
                        ErrorCode = 13
                        FileClose(aSection.TempoRTF)
                        '-> Lancer la g�n�ration physique du contenu RTF de la section de texte
                        CreateRTFSection(aSection, aspool)
                        '-> Fin de d�finition RTF d'une section
                        SwitchLect = 0
                    Case "\TABLEAU�END"
                        '-> Fin de d�finition d'un tableau
                        SwitchLect = 0
                    Case "\RANG�END"
                        '-> Fin de d�finition d'une section de texte
                        SwitchLect = 0
                    Case Else
                        Select Case SwitchLect
                            Case 1 'Analyse d'une ligne d'entete
                                InitMaqEntete((aspool.Maquette_Renamed), Ligne, aspool)
                            Case 2 'Analyse de la section PROGICIEL
                                If UCase(Trim(Entry(1, Ligne, "="))) = "\PROG" Then
                                    aspool.Maquette_Renamed.Progiciel = Entry(2, Ligne, "=")
                                ElseIf UCase(Trim(Entry(1, Ligne, "="))) = "\CLI" Then
                                    aspool.Maquette_Renamed.Client = Entry(2, Ligne, "=")
                                End If
                            Case 3 'On est dans une section de texte : analyse des tag <CADRE> et <BMP>  et <TEXTE>
                                Select Case UCase(Trim(Entry(1, Ligne, "�")))
                                    Case "\BMP"
                                        '-> Cr�ation d'un nouvel objet BMP
                                        aBmp = New ImageObj
                                        aBmp.Nom = Entry(2, Ligne, "�")
                                        aBmp.IdOrdreAffichage = aSection.AddOrdreAffichage("BMP-" & UCase(Trim(aBmp.Nom)))
                                        aBmp.SectionName = aSection.Nom
                                        '-> Ajout dans la collection des Bmps
                                        ErrorCode = 14
                                        TmpError = aSection.Nom & "|" & UCase(Trim(aBmp.Nom))
                                        aSection.Bmps.Add(aBmp, UCase(Trim(aBmp.Nom)))
                                        '-> Positionnement du pointeur de lecture
                                        SwitchLect = 4
                                    Case "\CADRE"
                                        '-> Cr�ation d'un nouvel objet cadre
                                        aCadre = New Cadre
                                        aCadre.Nom = Entry(2, Ligne, "�")
                                        aCadre.IdAffichage = aSection.AddOrdreAffichage("CDR-" & UCase(Trim(aCadre.Nom)))
                                        aCadre.SectionName = aSection.Nom
                                        ErrorCode = 15
                                        TmpError = aSection.Nom & "|" & UCase(Trim(aCadre.Nom))
                                        aSection.Cadres.Add(aCadre, UCase(Trim(aCadre.Nom)))
                                        '-> Positionnement du pointeur de lecture
                                        SwitchLect = 5
                                    Case "\TEXTE"
                                        '-> Fin de d�finition d'un objet Section: Cr�ation de la repr�sentation physique
                                        CreateSectionObj(aSection, aspool)
                                    Case Else
                                        '->Dans ce cas, on lit une propri�t� de la section: ajouter dans la d�fintion de la section
                                        InitSectionEntete(aspool, aSection, Ligne)
                                End Select 'Selon la nature de la ligne
                            Case 4
                                '-> Ligne de d�finition d'un BMP
                                InitBmpEntete(aBmp, aspool, Ligne)
                            Case 5
                                '-> Ligne de d�finition d'un cadre
                                InitCadreEntete(aCadre, aspool, Ligne)
                            Case 6
                                '-> Imprimer la ligne RTF du cadre dans son fichier TEMPO
                                PrintLine(aCadre.TempoRTF, Ligne)
                            Case 7
                                '-> Imprimer la ligne RTF de la section texte dans son fichier tempo
                                PrintLine(aSection.TempoRTF, Ligne)
                            Case 8
                                If UCase(Trim(Entry(1, Ligne, "�"))) = "\BEGIN" Then
                                    '-> Cr�ation d'un nouvel objet Block
                                    aBlock = New Block
                                    aBlock.Nom = Entry(2, Ligne, "�")
                                    aBlock.NomTb = Trim(UCase(aTb.Nom))
                                    aBlock.AlignementLeft = 3
                                    aBlock.Left_Renamed = 0
                                    aBlock.IdOrdreAffichage = aTb.AddOrdreAffichage("BL-" & UCase(Trim(aBlock.Nom)))
                                    '-> Ajouter le block dans la collection des tableaux
                                    ErrorCode = 16
                                    TmpError = aspool.MaquetteASCII & "|" & Trim(UCase(aTb.Nom)) & UCase(Trim(aBlock.Nom))
                                    aTb.Blocks.Add(aBlock, "BL-" & UCase(Trim(aBlock.Nom)))
                                    '-> Initialiser les matrices des lignes et colonnes
                                    Erase sCol
                                    Erase sLigne
                                    LigLue = 1
                                    '-> Postionnement du pointeur de lecture sur l'analyse des bloks
                                    SwitchLect = 9
                                Else
                                    '-> Ajouter � la d�finition du tableau en cours
                                    InitTableauEntete(aTb, aspool, Ligne)
                                End If
                            Case 9
                                If UCase(Trim(Entry(1, Ligne, "�"))) = "\END" Then
                                    '-> Fin de d�finition d'un objet block : lancer les initialisations et les cr�ations
                                    InitCreateBlock(aBlock, aTb, aspool)
                                    '-> Se repositionner sur la lecture du tableau
                                    SwitchLect = 8
                                Else
                                    '-> Analyse d'un ligne d'un block
                                    InitBlockEntete(aBlock, aTb, aspool, Ligne)
                                End If
                        End Select 'Selon la nature de la lecture de la ligne
                End Select
            End If '-> Si premier caract�re est un [
        Next  'Pour toutes les lignes de la maquette


        '-> G�n�ration ici de la section de texte pour la page des s�lections
        '-> Cr�er nouvel objet RTF
        aSection = New Section
        aSection.Largeur = aspool.Maquette_Renamed.Largeur - aspool.Maquette_Renamed.MargeLeft * 2
        aSection.Hauteur = aspool.Maquette_Renamed.Hauteur - aspool.Maquette_Renamed.MargeTop * 2
        aSection.AlignementLeft = 2
        aSection.AlignementTop = 2
        aSection.BackColor = &HFFFFFF

        '-> Charger un nouvel �diteur RTF dans la biblioth�que
        frmLib.Rtf.Load(IdRTf)
        frmLib.Rtf(IdRTf).Font = VB6.FontChangeName(frmLib.Rtf(IdRTf).Font, "Lucida Console")
        aSection.IdRTf = IdRTf

        '-> Incr�menter le compteur d'objet RTF
        IdRTf = IdRTf + 1

        '-> Ajouter dans la collection des secstion de la maquette
        aspool.Maquette_Renamed.Sections.Add(aSection, SelectRTFKey)


        Exit Sub

GestError:

        If ErrorCode = 13 Then
            Reset()
            Resume Next
        Else
            Call GestError(ErrorCode, TmpError, aspool)
        End If

    End Sub
    Private Function InitFileTempoRtf(ByRef aspool As Spool) As String

        '---> Cette fonction creer un fichier tempo pour lecture des RTF et renvoie :
        ' Handle|NomFichier

        On Error GoTo GestError

        Dim TempFile As String
        Dim hdlFile As String

        TempFile = GetTempFileNameVB("RTF")
        hdlFile = CStr(FreeFile())
        FileOpen(CInt(hdlFile), TempFile, OpenMode.Output)

        InitFileTempoRtf = hdlFile & "|" & TempFile

        Exit Function

GestError:
        Call GestError(17, , aspool)

    End Function

    Private Sub CreateRTFSection(ByRef aSection As Section, ByRef aspool As Spool)

        '---> Cette proc�dure charge le contenu du fichier TEmpoRTF adns l'objet affect� et le supprime
        Dim ErrorCode As Short

        On Error GoTo GestError

        '-> Charger le fichier RTF
        frmLib.Rtf(aSection.IdRTf).LoadFile(aSection.TempoRtfString)
        ErrorCode = -1
        '-> Supprimer le fichier
        Kill(aSection.TempoRtfString)

        Exit Sub

GestError:

        If ErrorCode = -1 Then Resume Next
        Call GestError(25, aSection.Nom & "|" & aSection.TempoRtfString, aspool)


    End Sub

    Private Sub CreateRTFCadre(ByRef aCadre As Cadre, ByRef aspool As Spool)

        '---> Cette proc�dure charge le contenu du fichier TEmpoRTF adns l'objet affect� et le supprime

        On Error GoTo GestError

        Dim ErrorCode As Short

        '-> Charger le fichier RTF
        frmLib.Rtf(aCadre.IdRTf).LoadFile(aCadre.TempoRtfString)

        '-> Supprimer le fichier
        ErrorCode = -1
        Kill(aCadre.TempoRtfString)

        Exit Sub

GestError:

        If ErrorCode = -1 Then Resume Next
        Call GestError(24, aCadre.SectionName & "|" & aCadre.Nom & "|" & aCadre.TempoRtfString, aspool)



    End Sub


    Private Sub InitMaqEntete(ByRef aMaq As Maquette, ByRef Ligne As String, ByRef aspool As Spool)

        '---> Cette proc�dure effectue le setting des propri�t�s de l'entete de la maquette

        Dim DefLigne As String
        Dim ValueLigne As String
        Dim Tempo As Single
        Dim ErrorCode As Short

        On Error GoTo GestError

        '-> Tester que la ligne commence par un "\"
        If InStr(1, Ligne, "\") = 0 Then Exit Sub

        '-> R�cup�ration des valeurs
        DefLigne = Entry(1, Ligne, "�")
        ValueLigne = Entry(2, Ligne, "�")
        '-> Traiter la valeur de la ligne
        Select Case UCase(DefLigne)
            Case "\NOM"
                aMaq.Nom = ValueLigne
            Case "\HAUTEUR"
                aMaq.Hauteur = CSng(Convert(ValueLigne))
            Case "\LARGEUR"
                aMaq.Largeur = CSng(Convert(ValueLigne))
            Case "\MARGETOP"
                aMaq.MargeTop = CSng(Convert(ValueLigne))
            Case "\MARGELEFT"
                aMaq.MargeLeft = CSng(Convert(ValueLigne))
            Case "\ORIENTATION"
                aMaq.Orientation = CStr(CShort(ValueLigne))
            Case "\PAGEGARDE" 'Version 2.0
                If UCase(ValueLigne) = "VRAI" Then
                    aMaq.PageGarde = True
                Else
                    aMaq.PageGarde = False
                End If
            Case "\RIGHTPAGE"
                If UCase(ValueLigne) = "VRAI" Then
                    AllowDebordement = True
                Else
                    AllowDebordement = False
                End If
            Case "\NAVIGATION"
                If UCase(ValueLigne) = "VRAI" Then
                    IsNaviga = True
                Else
                    'IsNaviga = False
                End If
            Case "\PAGESELECTION" 'Version 2.0
                If UCase(ValueLigne) = "VRAI" Then
                    aMaq.PageSelection = True
                Else
                    aMaq.PageSelection = False
                End If
            Case "\DEFENTETE"
                '-> Ajuster les dimensions
                If CDbl(aMaq.Orientation) <> 1 Then
                    Tempo = aMaq.Largeur
                    aMaq.Largeur = aMaq.Hauteur
                    aMaq.Hauteur = Tempo
                End If
            Case "\NAVIGAHTML"
                '-> Setting du navigateur
                aMaq.NavigaHTML = Trim(ValueLigne)

        End Select

        Exit Sub

        '-> Gestion des erreurs
GestError:

        Call GestError(18, Ligne, aspool)


    End Sub

    Private Sub InitSectionEntete(ByRef aspool As Spool, ByRef aSection As Section, ByRef Ligne As String)

        '---> Cette proc�dure effectue le setting des propri�t�s d'une section de texte

        Dim DefLigne As String
        Dim ValueLigne As String

        On Error GoTo GestError

        If InStr(1, Ligne, "\") = 0 Then Exit Sub

        '-> R�cup�rer les valeurs
        DefLigne = UCase(Entry(1, Ligne, "�"))
        ValueLigne = Entry(2, Ligne, "�")

        '-> Analyse
        Select Case DefLigne
            Case "\CONTOUR"
                If CShort(ValueLigne) = 0 Then
                    aSection.Contour = False
                Else
                    aSection.Contour = True
                End If
            Case "\HAUTEUR"
                aSection.Hauteur = CSng(Convert(ValueLigne))
            Case "\LARGEUR"
                aSection.Largeur = CSng(Convert(ValueLigne))
            Case "\ALIGNEMENTTOP"
                aSection.AlignementTop = CShort(ValueLigne)
            Case "\TOP"
                aSection.Top = CSng(Convert(ValueLigne))
            Case "\ALIGNEMENTLEFT"
                aSection.AlignementLeft = CShort(ValueLigne)
            Case "\LEFT"
                aSection.Left_Renamed = CSng(Convert(ValueLigne))
            Case "\BACKCOLOR"
                aSection.BackColor = CInt(ValueLigne)
            Case "\TEXTE"
        End Select

        Exit Sub

GestError:

        Call GestError(27, aSection.Nom & "|" & Ligne, aspool)

    End Sub

    Private Sub InitBmpEntete(ByRef aBmp As ImageObj, ByRef aspool As Spool, ByRef Ligne As String)

        '---> Cette proc�dure effectue le setting des propri�t�s d'un objet BMP

        Dim DefLigne As String
        Dim ValueLigne As String

        On Error GoTo GestError

        If InStr(1, Ligne, "\") = 0 Then Exit Sub

        '-> R�cup�rer les valeurs
        DefLigne = UCase(Entry(1, Ligne, "�"))
        ValueLigne = Entry(2, Ligne, "�")

        '-> Analyse
        Select Case DefLigne
            Case "\LEFT"
                aBmp.Left_Renamed = CSng(Convert(ValueLigne))
            Case "\TOP"
                aBmp.Top = CSng(Convert(ValueLigne))
            Case "\LARGEUR"
                aBmp.Largeur = CSng(Convert(ValueLigne))
            Case "\HAUTEUR"
                aBmp.Hauteur = CSng(Convert(ValueLigne))
            Case "\FICHIER"
                aBmp.Fichier_Renamed = ValueLigne
            Case "\CONTOURBMP"
                If CShort(ValueLigne) = 1 Then
                    aBmp.Contour = True
                Else
                    aBmp.Contour = False
                End If
            Case "\DEFAULT"
                aBmp.sDefault = ValueLigne
            Case "\VARIABLE"
                If CShort(ValueLigne) = 1 Then
                    aBmp.isVariable = True
                Else
                    aBmp.isVariable = False
                End If
            Case "\USEASSOCIATION"
                If CShort(ValueLigne) = 1 Then
                    aBmp.UseAssociation = True
                Else
                    aBmp.UseAssociation = False
                End If
            Case "\PATH"
                aBmp.Path = ValueLigne
        End Select

        Exit Sub

GestError:

        Call GestError(28, aBmp.SectionName & "|" & aBmp.Nom & "|" & Ligne, aspool)


    End Sub

    Private Sub InitCadreEntete(ByRef aCadre As Cadre, ByRef aspool As Spool, ByRef Ligne As String)

        '---> Cette proc�dure effectue le setting des propri�t�s d'un cadre

        Dim DefLigne As String
        Dim ValueLigne As String

        On Error GoTo GestError

        If InStr(1, Ligne, "\") = 0 Then Exit Sub

        '-> R�cup�rer les valeurs
        DefLigne = UCase(Entry(1, Ligne, "�"))
        ValueLigne = Entry(2, Ligne, "�")

        '-> Analyse
        Select Case DefLigne
            Case "\LEFT"
                aCadre.Left_Renamed = CSng(Convert(ValueLigne))
            Case "\TOP"
                aCadre.Top = CSng(Convert(ValueLigne))
            Case "\DISTANCE" 'Equivalent � MargeInterne
                aCadre.MargeInterne = CSng(Convert(ValueLigne))
            Case "\HAUTEUR"
                aCadre.Hauteur = CSng(Convert(ValueLigne))
            Case "\LARGEUR"
                aCadre.Largeur = CSng(Convert(ValueLigne))
            Case "\LARGEURBORDURE"
                aCadre.LargeurTrait = CShort(ValueLigne)
            Case "\ROUNDRECT"
                aCadre.IsRoundRect = CShort(ValueLigne)
            Case "\CONTOUR"
                '-> Bordure haut
                If Entry(1, ValueLigne, ",") = "1" Then
                    aCadre.Haut = True
                Else
                    aCadre.Haut = False
                End If
                '-> Bordure bas
                If Entry(2, ValueLigne, ",") = "1" Then
                    aCadre.Bas = True
                Else
                    aCadre.Bas = False
                End If
                '-> Bordure gauche
                If Entry(3, ValueLigne, ",") = "1" Then
                    aCadre.Gauche = True
                Else
                    aCadre.Gauche = False
                End If
                '-> Bordure droite
                If Entry(4, ValueLigne, ",") = "1" Then
                    aCadre.Droite = True
                Else
                    aCadre.Droite = False
                End If
            Case "\COULEUR"
                aCadre.BackColor = CInt(ValueLigne)
        End Select

        Exit Sub

GestError:

        Call GestError(29, aCadre.SectionName & "|" & aCadre.Nom & "|" & Ligne, aspool)


    End Sub

    Private Sub CreateSectionObj(ByRef aSection As Section, ByRef aspool As Spool)

        '---> Cette proc�dure cr�er les diff�rents objets pour impression de la section

        On Error GoTo GestError

        '-> Cr�ation d'un contenuer RTF
        frmLib.Rtf.Load(IdRTf)
        aSection.IdRTf = IdRTf

        '-> Appliquer les propri�t�s
        frmLib.Rtf(IdRTf).BackColor = System.Drawing.ColorTranslator.FromOle(aSection.BackColor)
        If aSection.Contour Then
            frmLib.Rtf(IdRTf).BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Else
            frmLib.Rtf(IdRTf).BorderStyle = System.Windows.Forms.BorderStyle.None
        End If

        '-> Incr�menter le compteur d'objets RTF
        IdRTf = IdRTf + 1
        Exit Sub

GestError:

        Call GestError(26, aSection.Nom, aspool)

    End Sub

    Private Sub CreateCadreObj(ByRef aCadre As Cadre, ByRef aspool As Spool)

        '---> Cette proc�dure cr�er une instance d'un objet RTF pour impression du cadre

        On Error GoTo GestError

        '-> charger un nouvel objet du type RTF
        frmLib.Rtf.Load(IdRTf)
        aCadre.IdRTf = IdRTf

        '-> Setting des prori�t�s
        frmLib.Rtf(IdRTf).BackColor = System.Drawing.ColorTranslator.FromOle(aCadre.BackColor)
        frmLib.Rtf(IdRTf).Visible = True

        '-> Incr�menter le compteur d'objets
        IdRTf = IdRTf + 1

        Exit Sub

GestError:

        Call GestError(23, aCadre.SectionName & "|" & aCadre.Nom, aspool)



    End Sub

    Private Sub CreateBmpObj(ByRef aBmp As ImageObj, ByRef aspool As Spool)

        '---> Cette proc�dure cr�er une instance d'un objet picturebox pour impression du BMP

        Dim Res As Integer
        Dim lpBuffer As String = ""
        Dim TempoStr As String = ""
        Dim Version As String = ""
        Dim Lecteur As String = ""
        Dim i As Short
        Dim ErrorCode As Short
        Dim TmpError As String = ""
        Dim V6Root As String = ""
        Dim ImagePath As String = ""


        On Error GoTo GestError

        '-> Chargement de la repr�sentation physique
        'ErrorCode = 19
        'TmpError = ""
        'frmLib.PicObj.Load(IdBmp)

        '-> Contour du Bmp
        'If aBmp.Contour Then
        '    frmLib.PicObj(IdBmp).BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        'Else
        '    frmLib.PicObj(IdBmp).BorderStyle = System.Windows.Forms.BorderStyle.None
        'End If

        '-> Donner ses dimensions au Bmp
        ErrorCode = 20
        TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Largeur & "|" & aBmp.Hauteur
        ''UPGRADE_ISSUE: Form m�thode frmLib.ScaleX - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        'frmLib.PicObj(IdBmp).Height = VB6.TwipsToPixelsY(aBmp.Hauteur)
        ''UPGRADE_ISSUE: Form m�thode frmLib.ScaleY - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        'frmLib.PicObj(IdBmp).Width = VB6.TwipsToPixelsX(aBmp.Largeur)

        '-> Selon la version
        If VersionTurbo = 0 Then 'On est en version Pathv6

            '-> V�rifier que l'on trouve le fichier Ini
            If TurboMaqIniFile = "" Then GoTo AfterLoading

            '-> R�cup�rer le path Root
            lpBuffer = Space(100)
            Res = GetPrivateProfileString("Param", "$ROOT", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
            If Res <> 0 Then V6Root = Mid(lpBuffer, 1, Res)

            Select Case UCase(aBmp.Path)
                Case "PARAM"
                    Res = GetPrivateProfileString("Param", "$MAQGUIPATH", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
                    If Res = 0 Then
                        aBmp.Fichier_Renamed = ""
                        GoTo AfterLoading
                    End If
                    ImagePath = Replace(Replace(UCase(Mid(lpBuffer, 1, Res)), "$ROOT", V6Root), "$APP", "emilie")
                Case "APP"
                    Res = GetPrivateProfileString("Param", "$MAQGUIPATH", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
                    If Res = 0 Then
                        aBmp.Fichier_Renamed = ""
                        GoTo AfterLoading
                    End If
                    ImagePath = Replace(Replace(UCase(Mid(lpBuffer, 1, Res)), "$ROOT", V6Root), "$APP", aspool.Maquette_Renamed.Progiciel)
                Case "CLIENT"
                    Res = GetPrivateProfileString("Param", "$IDENTPATH", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
                    If Res = 0 Then
                        aBmp.Fichier_Renamed = ""
                        GoTo AfterLoading
                    End If
                    ImagePath = Replace(Replace(Replace(UCase(Mid(lpBuffer, 1, Res)), "$ROOT", V6Root), "$APP", aspool.Maquette_Renamed.Progiciel), "$IDENT", aspool.Maquette_Renamed.Client)
            End Select

            '-> V�rifier si on trouve l'image
            'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            If Dir(ImagePath & "\" & aBmp.Fichier_Renamed) <> "" Then
                ErrorCode = 22
                TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Fichier_Renamed
                'frmLib.PicObj(IdBmp).Image = System.Drawing.Image.FromFile(ImagePath & "\" & aBmp.Fichier_Renamed)
                '        aBmp.Fichier = lpBuffer & aBmp.Fichier PIERROT : client-serveur en VERSION V6
                aBmp.Fichier_Renamed = ImagePath & "\" & aBmp.Fichier_Renamed
                ' Else   PIERROT : client-serveur en VERSION V6 : ne pas changer le nom du fichier de forme ^0001 si path parametr� => MODE PARAM
                '     aBmp.Fichier = ""
            End If 'Si on trouve l'image
        Else
            '-> On est en version professionnelle
            If aBmp.isVariable Then
                aBmp.IsAutosize = True
            Else
                aBmp.IsAutosize = False
                If Trim(aBmp.Fichier_Renamed) <> "" Then
                    '-> Rechercher l'image dans le sous r�pertoire Images
                    lpBuffer = My.Application.Info.DirectoryPath & "\Images\" & aBmp.Fichier_Renamed
                    '-> Tester que l'on trouve l'image
                    'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
                    If Dir(lpBuffer, FileAttribute.Normal) <> "" Then
                        '-> Essayer de charger l'image
                        ErrorCode = 22
                        TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Fichier_Renamed
                        'frmLib.PicObj(IdBmp).Image = System.Drawing.Image.FromFile(lpBuffer)
                        aBmp.Fichier_Renamed = lpBuffer
                    Else
                        '-> impossible de trouver le fichier associ�
                        aBmp.Fichier_Renamed = ""
                    End If
                Else 'La propri�t� fichier est � blanc
                    aBmp.Fichier_Renamed = ""
                End If
            End If
        End If 'Selon la version

AfterLoading:

        '-> Positionner l'objet
        'UPGRADE_ISSUE: PictureBox m�thode PicObj.ScaleY - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        'frmLib.PicObj(IdBmp).Top = VB6.TwipsToPixelsY(aBmp.Top)
        ''UPGRADE_ISSUE: PictureBox m�thode PicObj.ScaleX - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        'frmLib.PicObj(IdBmp).Left = VB6.TwipsToPixelsX(aBmp.Left_Renamed)
        ''UPGRADE_WARNING: propri�t� PicObj.AutoSize de PictureBox a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
        'frmLib.PicObj(IdBmp).SizeMode = aBmp.IsAutosize

        ''-> Enregistrer l'index du picture box associ�
        'aBmp.IdPic = IdBmp

        ''-> Rendre l'objet visible
        'frmLib.PicObj(IdBmp).Visible = True

        ''-> Incr�menter
        'IdBmp = IdBmp + 1


        Exit Sub

GestError:

        Call GestError(ErrorCode, TmpError, aspool)


    End Sub

    Private Sub InitTableauEntete(ByRef aTb As Tableau, ByRef aspool As Spool, ByRef Ligne As String)

        '---> Cette proc�dure effectue le settin g des prorpi�t�s d'un tableau

        Dim DefLigne As String
        Dim ValueLigne As String

        On Error GoTo GestError

        If InStr(1, Ligne, "\") = 0 Then Exit Sub

        '-> R�cup�rer les valeurs
        DefLigne = UCase(Entry(1, Ligne, "�"))
        ValueLigne = Entry(2, Ligne, "�")

        '-> Analyse
        Select Case DefLigne
            Case "\LARGEURTB"
                aTb.Largeur = CSng(Convert(ValueLigne))
            Case "\ORIENTATIONTB"
                aTb.Orientation = CShort(ValueLigne)
        End Select


        Exit Sub



GestError:
        Call GestError(30, aTb.Nom & "|" & Ligne, aspool)



    End Sub

    Private Sub InitBlockEntete(ByRef aBlock As Block, ByRef aTb As Tableau, ByRef aspool As Spool, ByRef Ligne As String)

        '---> Cette proc�dure effectue le setting des propri�t�s d'un block

        Dim DefLigne As String
        Dim ValueLigne As String

        On Error GoTo GestError

        If InStr(1, Ligne, "\") = 0 Then Exit Sub

        '-> R�cup�rer les valeurs
        DefLigne = UCase(Entry(1, Ligne, "�"))
        ValueLigne = Entry(2, Ligne, "�")

        '-> Analyse
        Select Case DefLigne
            Case "\ALIGNEMENT" 'Garder pour compatibilit� 1.00
                If CShort(ValueLigne) = 0 Then 'Marge gauche
                    aBlock.AlignementLeft = 2
                ElseIf CShort(ValueLigne) = 1 Then  'Marge Droite
                    aBlock.AlignementLeft = 4
                ElseIf CShort(ValueLigne) = 2 Then  'Centr�
                    aBlock.AlignementLeft = 3
                ElseIf CShort(ValueLigne) = 3 Then  'Alignement Libre
                    aBlock.AlignementLeft = 5 'La valeur Left est aliment�e par la valeur "\DISTANCE"
                End If
            Case "\ALIGNLEFT" 'Version 2.00 de "\Alignement"
                aBlock.AlignementLeft = CShort(ValueLigne)
            Case "\LEFT" 'Valeur de AlignLeft quand il est sp�cifi� : valeur 5
                aBlock.Left_Renamed = CSng(Convert(ValueLigne))
            Case "\ALIGNTOP" 'Version  2.00 de "\AlignementBlockVertical"
                aBlock.AlignementTop = CShort(ValueLigne)
            Case "\TOP" 'Valeur de AlignTop quand il est sp�cifi� : valeur 5
                aBlock.Top = CSng(Convert(ValueLigne))
            Case "\LARGEUR"
                aBlock.Largeur = CSng(Convert(ValueLigne))
            Case "\HAUTEUR"
                aBlock.Hauteur = CSng(Convert(ValueLigne))
            Case "\LIGNE"
                aBlock.NbLigne = CShort(ValueLigne)
                ReDim sLigne(CShort(ValueLigne))
            Case "\COLONNE"
                aBlock.NbCol = CShort(ValueLigne)
                ReDim sCol(CShort(ValueLigne))
            Case "\EXPORTLIG"
                aBlock.ListeExcel = ValueLigne
            Case "\ACCESDET"
                aBlock.KeyAccesDet = Trim(ValueLigne)
            Case "\COL"
                sLigne(LigLue) = Ligne
                LigLue = LigLue + 1
        End Select

        Exit Sub

GestError:
        Call GestError(31, aTb.Nom & "|" & aBlock.Nom & "|" & Ligne, aspool)

    End Sub
    Private Sub InitCreateBlock(ByRef aBlock As Block, ByRef aTb As Tableau, ByRef aspool As Spool)

        '---> Cette proc�dure cr�er les diff�rentes cellules � partir des matrices sCol et sLigne

        Dim aCell As Cellule
        Dim i As Short
        Dim j As Short
        Dim DefLigne As String
        Dim DefCellule As String
        Dim DefBordure As String
        Dim ErrorCode As Short
        Dim AccesDetail As String
        Dim ListeCell As String

        On Error GoTo GestError

        '-> Initialiser les colonnes et les lignes
        aBlock.Init()

        '-> Gestion des exports des lignes vers Excel Attention , cette propri�t� _
        'ne contient que la liste des lignes non exportables vers EXCEL
        If Trim(aBlock.ListeExcel) <> "" Then
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(aBlock.ListeExcel, |). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For i = 1 To NumEntries(aBlock.ListeExcel, "|")
                aBlock.SetExportExcel(CShort(Entry(i, aBlock.ListeExcel, "|")), False)
            Next
        End If

        '-> Alimenter les Matrices de ligne te de colonne et cr�ation des cellules
        For i = 1 To LigLue - 1
            '-> R�cup�ration de la d�finition de la ligne
            ErrorCode = 32
            DefLigne = sLigne(i)
            '-> Setting de la hauteur de ligne
            aBlock.SetHauteurLig(i, CSng(Convert(Entry(2, DefLigne, "�"))))
            '-> Recup�rer QUE la liste des colonnes
            DefLigne = Entry(3, DefLigne, "�")
            For j = 1 To aBlock.NbCol
                ErrorCode = 33
                '-> Recup de la d�finition de la colonne
                DefCellule = Entry(j, DefLigne, "|")
                '-> Setting de la largeur
                aBlock.SetLargeurCol(j, CSng(Convert(Entry(3, DefCellule, ";"))))
                '-> Cr�ation de la cellule associ�e
                aCell = New Cellule
                aCell.Ligne = i
                aCell.Colonne = j
                '-> Ajout dans le tableau
                aBlock.Cellules.Add(aCell, "L" & i & "C" & j)
                '-> setting des propri�t�s
                aCell.CellAlign = CShort(Entry(1, DefCellule, ";"))
                aCell.Contenu = Entry(2, DefCellule, ";")
                '-> Initialisation des champs
                aCell.InitChamp()
                '-> Gestion des bordures
                DefBordure = Entry(4, DefCellule, ";")
                If UCase(Entry(1, DefBordure, ",")) = "VRAI" Then
                    aCell.BordureHaut = True
                Else
                    aCell.BordureHaut = False
                End If
                If UCase(Entry(2, DefBordure, ",")) = "VRAI" Then
                    aCell.BordureBas = True
                Else
                    aCell.BordureBas = False
                End If
                If UCase(Entry(3, DefBordure, ",")) = "VRAI" Then
                    aCell.BordureGauche = True
                Else
                    aCell.BordureGauche = False
                End If
                If UCase(Entry(4, DefBordure, ",")) = "VRAI" Then
                    aCell.BordureDroite = True
                Else
                    aCell.BordureDroite = False
                End If
                '-> Propri�t�s de Font
                aCell.FontName = Entry(5, DefCellule, ";")
                aCell.FontSize = CSng(Convert(Entry(6, DefCellule, ";")))
                If UCase(Trim(Entry(7, DefCellule, ";"))) = "VRAI" Then
                    aCell.FontBold = True
                Else
                    aCell.FontBold = False
                End If
                If UCase(Trim(Entry(8, DefCellule, ";"))) = "VRAI" Then
                    aCell.FontItalic = True
                Else
                    aCell.FontItalic = False
                End If
                If UCase(Trim(Entry(9, DefCellule, ";"))) = "VRAI" Then
                    aCell.FontUnderline = True
                Else
                    aCell.FontUnderline = False
                End If
                aCell.FontColor = CInt(Entry(10, DefCellule, ";"))
                If CInt(Entry(11, DefCellule, ";")) = 999 Then
                    aCell.BackColor = 16777215
                    aCell.FondTransparent = True
                Else
                    aCell.BackColor = CInt(Entry(11, DefCellule, ";"))
                    aCell.FondTransparent = False
                End If
                '-> AutoAjust
                If UCase(Entry(12, DefCellule, ";")) = "VRAI" Then
                    aCell.AutoAjust = True
                Else
                    aCell.AutoAjust = False
                End If
                '-> Format et Type
                DefBordure = Entry(13, DefCellule, ";")
                If Entry(1, DefBordure, "@") <> "" Then aCell.TypeValeur = CShort(Entry(1, DefBordure, "@"))
                aCell.Msk = Entry(2, DefBordure, "@")
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(DefBordure, ). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If NumEntries(DefBordure, "@") = 3 Then aCell.PrintZero = True

                '-> Export vers Excel Fusion
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(DefCellule, ;). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If NumEntries(DefCellule, ";") > 13 Then
                    '-> R�cup�rer le param�trage
                    DefBordure = Entry(14, DefCellule, ";")
                    aCell.IsFusion = CBool(Entry(1, DefBordure, "@"))
                    aCell.ColFusion = CShort(Entry(2, DefBordure, "@"))
                End If

                '-> Impl�mnter le mod�le objet associ� � excel
                aCell.TurboToExcel()

            Next  'Pour toute les colonnes
        Next  'Pour toutes les lignes

        '-> Gestion de l'acc�s au d�tail
        If aBlock.KeyAccesDet <> "" Then
            '-> Basculer
            AccesDetail = aBlock.KeyAccesDet
            '-> Setting des param�tres du block
            aBlock.UseAccesDet = True
            aBlock.KeyAccesDet = Entry(1, AccesDetail, "�")
            '-> Setting des cellules servant � l'acc�s au d�tail
            ListeCell = Entry(2, AccesDetail, "�")
            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(ListeCell, |). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For i = 1 To NumEntries(ListeCell, "|")
                '-> Pointeur vers une cellule
                aCell = aBlock.Cellules.Item("L" & Entry(1, Entry(i, ListeCell, "|"), "-") & "C" & Entry(2, Entry(i, ListeCell, "|"), "-"))
                '-> forcer le format
                aCell.FontColor = RGB(0, 0, 255)
                aCell.FontUnderline = True
                '-> Indiquer qu'elle sert d'acc�s au d�tail
                aCell.UseAccesDet = True
            Next  'Pour toutes les d�finitions de cellules
        End If 'S'il y a l'acc�s au d�tail sur ce block

        '-> Convertion des dimensions des cellules en pixels
        InitBlock(aBlock, aspool)


        Exit Sub

GestError:
        If ErrorCode = 32 Then
            Call GestError(ErrorCode, aTb.Nom & "|" & aBlock.Nom, aspool)
        Else
            Call GestError(ErrorCode, aTb.Nom & "|" & aBlock.Nom & "|" & DefCellule, aspool)
        End If



    End Sub



    Public Sub InitBlock(ByVal BlockCours As Block, ByRef aspool As Spool)

        '---> Cette proc�dure initialise les matrices Lignes (), Colonnes () et Cells _
        'en partant des largeurs de colonnes et hauteur de lignes

        Dim i As Short
        Dim j As Short
        Dim NbCol As Short
        Dim NbLig As Short
        '-> Dimensions en CM
        Dim HauteurCm As Single
        Dim LargeurCm As Single
        '-> Convertion en Pixels pour dessin
        Dim HauteurPix As Integer
        Dim LargeurPix As Integer
        Dim hPix As Integer
        Dim lPix As Integer
        '-> Matrices des lignes et colonnes
        Dim Lignes() As Integer
        Dim Colonnes() As Integer

        Dim LargeurColPix As Integer
        Dim HauteurLigPix As Integer

        Dim aCell As Cellule
        Dim PosX As Integer
        Dim PosY As Integer

        On Error GoTo ErrorOpen

        '-> R�cup�ration des propri�t�s
        NbCol = BlockCours.NbCol
        NbLig = BlockCours.NbLigne
        HauteurCm = BlockCours.Hauteur
        LargeurCm = BlockCours.Largeur

        '-> Convertion en pixels
        LargeurPix = CInt(LargeurCm) * CDbl(37.79524)
        HauteurPix = CInt(CDbl(HauteurCm)) * CDbl(37.79524)

        '-> Affecter la valeur au block
        BlockCours.LargeurPix = LargeurPix
        BlockCours.HauteurPix = HauteurPix

        '-> Calcul des dimensions de base
        ReDim Lignes(NbLig)
        ReDim Colonnes(NbCol)

        '-> Largeur de base des colonnes
        For i = 1 To NbCol
            Colonnes(i) = Fix((BlockCours.GetLargeurCol(i) / LargeurCm) * ((LargeurPix - NbCol + 1)))
            lPix = lPix + Colonnes(i)
        Next

        '-> Hauteur de base des lignes
        For i = 1 To NbLig
            Lignes(i) = Fix((BlockCours.GetHauteurLig(i) / HauteurCm) * (HauteurPix - (NbLig + 1)))
            hPix = hPix + Lignes(i)
        Next

        Dim aCumul As Double
        Dim bCumul As Integer
        Dim a As Double

        '-> Lissage des colonnes
        bCumul = 1

        For i = 1 To NbCol
            '-> Largeur en pixel normale
            aCumul = aCumul + BlockCours.GetLargeurCol(i)
            LargeurColPix = CInt(aCumul * CDbl(37.79524))
            bCumul = bCumul + Colonnes(i) + 1
            a = LargeurColPix - bCumul
            Colonnes(i) = Colonnes(i) + a
            bCumul = LargeurColPix
        Next

        '-> Lissage des lignes
        aCumul = 0
        bCumul = 1

        For i = 1 To NbLig
            '-> Hauteur de pixel normale
            aCumul = aCumul + BlockCours.GetHauteurLig(i)
            HauteurLigPix = CShort(aCumul * CDbl(37.79524))
            bCumul = bCumul + Lignes(i) + 1
            a = HauteurLigPix - bCumul
            Lignes(i) = Lignes(i) + a
            bCumul = HauteurLigPix
        Next

        '-> Pixel de d�part
        PosY = 1
        PosX = 1

        For i = 1 To NbCol
            For j = 1 To NbLig
                '-> Mettre � jour les valeurs des cellules
                aCell = BlockCours.Cellules.Item("L" & j & "C" & i)
                aCell.X1 = PosX
                aCell.Y1 = PosY
                aCell.X2 = PosX + Colonnes(i) + 1
                aCell.Y2 = PosY + Lignes(j) + 1
                '-> Incr�menter le compteur de position
                PosY = PosY + Lignes(j) + 1
            Next  'pour toutes les lignes
            '-> initialisation des valeurs
            PosX = PosX + Colonnes(i) + 1
            PosY = 1
        Next  'Pour toutes les colonnes

        '-> Lib�rer le pointeur de cellule
        aCell = Nothing

        '-> Sortir de la fonction
        Exit Sub

ErrorOpen:

        Call GestError(34, BlockCours.NomTb & "|" & BlockCours.Nom, aspool)

    End Sub

    Public Sub PrintPageSpool(ByRef aspool As Spool, ByVal PageToPrint As Short, ByVal realPage As Long)

        '---> Cette proc�dure imprime une page d'un spool dans un fichier pdf

        Dim i, j As Short
        Dim PositionX As Integer
        Dim PositionY As Integer
        Dim Ligne As String
        Dim TypeObjet As String = ""
        Dim NomObjet As String = ""
        Dim TypeSousObjet As String = ""
        Dim NomSousObjet As String = ""
        Dim Param As String = ""
        Dim DataFields As String = ""
        Dim FirstObj As Boolean
        Dim aSection As Section
        Dim Titre As String
        Dim isHTML As Boolean

        On Error GoTo GestError
        aSection = Nothing
        RTFobj = Nothing '-> on remet l'objet a vide
        '-> on initialise la page
        aPrinter.SetPaperSize(vbPDF.pdfPaperSize.pdfUserSize, aspool.Maquette_Renamed.Largeur, aspool.Maquette_Renamed.Hauteur)

        aPrinter.BeginPage()
        '-> on ajoute la page dans l'arborescence
        Titre = GetTitrePage(aspool, PageToPrint)
        If Titre <> "" Then
            aPrinter.AddBookmark(Titre, 1, vbPDF.pdfBookmarkType.pdfFitPage, 0.0!, 0.0!, 0.0!, 0.0!, 0.0!)
            If TopBookmark = False Then
                TopBookmark = True
                '-> on affiche dans le viewer le bookmark
                aPrinter.SetViewerPreferences(vbPDF.pdfViewerPreference.pdfShowBookmarks)
            End If
        Else
            If TopBookmark = False Then aPrinter.AddBookmark("Page - " & PageToPrint, 1, vbPDF.pdfBookmarkType.pdfFitPage, 0.0!, 0.0!, 0.0!, 0.0!, 0.0!)
        End If
        'aPrinter.SetCharSpacing(0.1)
        'aPrinter.SetWordSpacing(-0.2)
        'aPrinter.SetTextHorizontalScaling(99.0)
        '-> Initialiser le RTF d'impression de la page des s�lections
        If PageToPrint = 0 Then
            '-> Pointer sur la section texte
            aSection = aspool.Maquette_Renamed.Sections.Item(SelectRTFKey)
            '-> Vider le RTF associ�
            frmLib.Rtf(aSection.IdRTf).Text = ""
        End If

        MargeX = 0
        MargeY = 0

        '-> Initialiser la position X , y du pointeur sur les marges du document
        PositionY = (-MargeY + aspool.Maquette_Renamed.MargeTop) * CDbl(37.79524)
        PositionX = (-MargeX + aspool.Maquette_Renamed.MargeLeft) * CDbl(37.79524)

        '-> Indiquer l'�tat du premier objet que l'on trouve
        FirstObj = True

        '-> Lecture des lignes de la page
        For i = 1 To NumEntries(aspool.GetPage(PageToPrint), Chr(0))
            '-> R�cup�ration de la ligne en cours
            Ligne = Trim(Entry(i, aspool.GetPage(PageToPrint), Chr(0)))
            '-> Ne rien imprimer si page � blanc
            If Trim(Ligne) = "" Then GoTo NextLigne

            If PageToPrint = 0 Then
                frmLib.Rtf(aSection.IdRTf).Text = frmLib.Rtf(aSection.IdRTf).Text & Chr(13) & Chr(10) & Ligne
                frmLib.Rtf(aSection.IdRTf).SelectionStart = 0
                frmLib.Rtf(aSection.IdRTf).SelectionLength = Len(frmLib.Rtf(aSection.IdRTf).Text)
                frmLib.Rtf(aSection.IdRTf).SelectionFont = VB6.FontChangeName(frmLib.Rtf(aSection.IdRTf).SelectionFont, "Lucida Console")
            Else
                If InStr(1, Ligne, "[") = 1 Then
                    Trace(NomObjet)
                    '-> r�cup�ration des param�tres
                    AnalyseObj(Ligne, TypeObjet, NomObjet, TypeSousObjet, NomSousObjet, Param, DataFields)
                    '-> Selon le type d'objet
                    If UCase(TypeObjet) = "ST" Then
                        If Not PrintSection(NomObjet, Param, DataFields, FirstObj, PositionX, PositionY, aspool) Then Exit For
                        FirstObj = False
                    ElseIf UCase(TypeObjet) = "TB" Then
                        If Not PrintTableau(NomObjet, Param, DataFields, PositionX, PositionY, NomSousObjet, FirstObj, aspool) Then Exit For
                        FirstObj = False
                    End If
                End If 'Si premier caract�re = "["
            End If 'Si on imprime la page de s�lection
NextLigne:
        Next  'Pour toutes les lignes de la page
        Trace("fin boucle")

        If Not listImage Is Nothing Then
            If listImage.Count <> 0 Then
                Dim abmp As ImageObj
                For Each abmp In listImage
                    If abmp.Hauteur <> 0 Then
                        aPrinter.DrawImage(abmp.Nom, abmp.Left_Renamed, abmp.Top, , CSng(abmp.Hauteur / CDbl(37.79524)), , , True)
                    ElseIf abmp.Largeur <> 0 Then
                        aPrinter.DrawImage(abmp.Nom, abmp.Left_Renamed, abmp.Top, CSng(abmp.Largeur / CDbl(37.79524)), , , , True)
                    End If
                Next
            End If
        End If
        '-> Imprimer la page de s�lection
        If PageToPrint = 0 Then
            If isHTML = False Then
                '-> imprimer la section
                PrintObjRtf(0, 0, aSection, "", 0, 0, aspool)
            Else
                '-> on se formate la page de garde
                parseGarde(frmLib.Rtf(aSection.IdRTf).Text, aSection.IdRTf)
                '-> imprimer la section
                'PrintObjRtf(0, 0, aSection, "", 0, 0, aspool)
            End If
        End If

        '-> on indique si des pi�ces jointes sont pr�sentes
        If Not arrFILEJOIN Is Nothing And ZonChargGet("hideNote") = "" Then
            If arrFILEJOIN(0) Is Nothing Then
                Dim Text1 As String = ""
                For i = 1 To arrFILEJOIN.Length - 1
                    Text1 = Text1 & My.Computer.FileSystem.GetFileInfo(arrFILEJOIN(i)).Name & ChrW(13)
                Next
                aPrinter.AddNoteIcon(Text1, vbPDF.pdfNoteIcon.pdfNote, 0.5, 0.5, False, "Pi�ces jointes :", vbPDF.pdfNoteFlag.pdfReadOnly)
                '-> on met un top comme quoi on a deja aliment� la note
                arrFILEJOIN(0) = "0"
            End If
        End If

        '-> on affiche les notes de la page si il y en a sur la page en cours
        If Not fNotes Is Nothing Then
            Dim fNote As Note
            '-> on se parcours les notes pour voir celles qui nous interessent
            For Each fNote In fNotes
                If fNote.Tag = Fichiers.Item(UCase(aspool.FileName)).fileNameZip & "|" & aspool.Num_Spool & "|" & realPage Then
                    '-> on cree la note dans l'objet des annotations pdf
                    aPrinter.SetColorFill("#" & ConvertColor(fNote.backColor))
                    aPrinter.AddNoteIcon(fNote.Text2, vbPDF.pdfNoteIcon.pdfComment, fNote.Left / 600, (fNote.Top / 600) - 1.5, True, fNote.Text1, vbPDF.pdfNoteFlag.pdfReadOnly)
                End If
            Next
        End If

        '-> on termine la page
        aPrinter.EndPage()
        '-> on vide les variables
        aSection = Nothing
        Trace("fin page")
        Exit Sub

GestError:
        'MsgBox(Err.Number & " " & Err.Description)
        '-> Setting d'une erreur
        Call GestError(0, CStr(PageToPrint), aspool)

    End Sub

    Private Function parseGarde(ByVal strGarde As String, ByVal RtfId As String)
        '--> cette fonction va permettre de parser le texte de garde issu des batchs en quelque chose de lisible
        Dim i As Integer
        Dim expgarde
        Dim strLigne As String
        Dim strValue As String
        Dim strField As String
        Dim strLastField As String
        Dim dogIdent As String
        Dim dogLangue As String
        Dim typSel As String
        Dim strTemp As String
        Dim strGroup As String
        Dim strVue As String
        Dim strNomspool As String
        Dim topY As Integer = 6
        'aRtf = frmLib.Rtf(RtfId)
        'aRtf.Text = ""
        expgarde = Split(strGarde, Chr(10))

        For i = 1 To UBound(expgarde)
            strLigne = expgarde(i)
            strLigne = Replace(strLigne, Chr(9), "")
            strLigne = Replace(strLigne, Chr(13), "")

            Select Case UCase(Entry(1, strLigne, ">"))
                Case "<DOG-IDENT/"
                    dogIdent = Trim(Entry(2, strLigne, ">"))
                Case "<DOG-LANGUE/"
                    dogLangue = Trim(Entry(2, strLigne, ">"))
                Case "<BATDATE/"
                    strTemp = strTemp & "Cr�� le : " & Entry(2, strLigne, ">")
                Case "<DOG-OPERAT/"
                    strTemp = strTemp & "           par : " & Entry(2, strLigne, ">") & Chr(13) & Chr(10)
                Case "<LIBELVUE/" '->libell� programme
                    strVue = Entry(2, strLigne, ">")
                Case "<NOMSPOOL/"
                    strNomspool = Entry(2, strLigne, ">")
                Case "<LIBPROG/" '->libell� programme
                    If strVue <> "" Then
                        strVue = Entry(2, strLigne, ">") & "      avec la vue '" & strVue & "'"
                    Else
                        strVue = Entry(2, strLigne, ">")
                    End If
                    'aPrinter.AddFont(GetFontFileName("TIMES NEW ROMAN"), "", False)
                    'aPrinter.SetFontSize(6)
                    'aPrinter.SetColorText("Black")
                    aPrinter.SetFont("CourierNew", 6, vbPDF.pdfFontStyle.pdfNormal)
                    aPrinter.DrawText(1, 2, strVue)
                    aPrinter.SetFontSize(6)
                    aPrinter.SetColorText("Black")
                    aPrinter.DrawText(1, 2.5, Chr(13) & Chr(10) & "______________________________________________________________________________________________________")
                    aPrinter.DrawText(1, 3, strNomspool & Chr(13) & Chr(10))
                    aPrinter.DrawText(1, 3.5, strTemp & Chr(13) & Chr(10))

                    'on se commence un tableau
                    aPrinter.DrawText(1, 5, "Valeur                                                                   Ordre   Saut   Edit")
                Case "<KEYWORD/" '->champ
                    '-> on va essayer de lire le libell� du champ
                    strField = Entry(2, strLigne, ">")
                    If Entry(1, strField, "_") <> Entry(1, strLastField, "_") Then
                        strValue = "" & lectbin(strField, dogIdent, dogLangue, "")
                    Else
                        strValue = ""
                    End If
                    strLastField = strField
                    typSel = ""
                Case "<DOG-PROGICIEL/"
                    strTemp = strTemp & "Progiciel :" & Entry(2, strLigne, ">") & Chr(13) & Chr(10) & Chr(13) & Chr(10)
                Case "<DOG-CODSOC/"
                    strTemp = strTemp & Space(20) & " soci�t�/�tablissement : " & Entry(2, strLigne, ">")
                Case "<DOG-CODETB/"
                    strTemp = strTemp & "/" & Entry(2, strLigne, ">")
                Case "<TYPSEL/"
                    typSel = Entry(2, strLigne, ">")
                    Select Case typSel
                        Case "EQ"
                            strValue = Left(strValue & Space(30), 30) & " �gale � : "
                        Case "INC"
                            strValue = Left(strValue & Space(30), 30) & " compris entre : "
                        Case "DIF"
                            strValue = Left(strValue & Space(30), 30) & " diff�rent de : "
                        Case "NIN"
                            strValue = Left(strValue & Space(30), 30) & " non inclus dans : "
                    End Select
                Case "<VALMIN/"
                    If strGroup = "COMBO" Then
                        strValue = strValue & lectbin(strField, dogIdent, dogLangue, Entry(2, strLigne, ">"))
                    End If
                    strValue = strValue & Entry(2, strLigne, ">")
                Case "<GROUPE/"
                    strGroup = UCase(Entry(2, strLigne, ">"))
                Case "<VALMAX/"
                    If typSel <> "EQ" Then strValue = strValue & " / " & Entry(2, strLigne, ">")
                Case "<ORDER/"
                    If Entry(2, strLigne, ">") <> "0" Then
                        strValue = Left(Left(strValue & Space(68), 69) & Space(7) & Entry(2, strLigne, ">") & Space(50), 85)
                    Else
                        strValue = Left(Left(strValue & Space(68), 69) & Space(50), 85)
                    End If
                Case "<SAUT/"
                    strValue = Left(strValue & Space(80), 81)
                    If Entry(2, strLigne, ">") <> "0" Then
                        strValue = Left(strValue & Space(4) & "X" & Space(20), 86)
                    Else
                        strValue = Left(strValue & Space(20), 86)
                    End If
                Case "<EDITABLE/"
                    strValue = Left(strValue & Space(90), 86)
                    If Entry(2, strLigne, ">") <> "0" Then
                        strValue = Left(strValue & Space(5) & "X" & Space(7), 92)
                    Else
                        strValue = Left(strValue & Space(20), 92)
                    End If
                Case "</FIELD"
                    Dim isB As Boolean
                    aPrinter.SetXY(aPrinter.GetX, aPrinter.GetY + 0.3)
                    If isB = False Then
                        aPrinter.DrawText(1, aPrinter.GetY, strValue)
                        isB = True
                    Else
                        aPrinter.DrawText(1, aPrinter.GetY, strValue)
                        isB = False
                    End If
            End Select
        Next

    End Function

    Private Function lectbin(ByVal strField As String, ByVal strIdent As String, ByVal strLangue As String, ByVal strValue As String) As String
        '-> cette fonction va essayer de trouver sur le site la definition du bin
        lectbin = strField
    End Function

    Private Function PrintBlock(ByRef NomSousObjet As String, ByRef Param As String, ByRef DataFields As String, ByVal PosX As Long, ByRef PosY As Integer, ByRef aTb As Tableau, ByVal Ligne As Short, ByRef aspool As Spool) As Integer

        '---> Cette Proc�dure imprime un block de ligne

        Dim aBlock As Block
        Dim i As Integer
        Dim aCell As Cellule
        Dim HauteurLigPix As Integer
        Dim strTempo As String
        Dim topColor As Boolean
        Dim topBold As Boolean
        Dim topItalic As Boolean
        Dim topUnderline As Boolean

        'On Error GoTo GestBlock

        '-> Pointer sur le block de tableau � �diter
        aBlock = aTb.Blocks.Item("BL-" & UCase(NomSousObjet))

        '-> Initaliser le block
        InitBlock(aBlock, aspool)

        '-> Calculer la modification des positions X et Y des blocks de ligne. _
        'Les coordonn�es de chaque cellule �tant calcul�e sur un X et Y = 0 de base

        Select Case aBlock.AlignementLeft
            Case 2 '-> Marge gauche
                PosX = aspool.Maquette_Renamed.MargeLeft * CDbl(37.79524)
            Case 3 '-> Centr�
                PosX = (aspool.Maquette_Renamed.Largeur - aBlock.Largeur) / 2 * CDbl(37.79524)
            Case 4 '-> Marge droite
                PosX = (aspool.Maquette_Renamed.Largeur - aBlock.Largeur - aspool.Maquette_Renamed.MargeLeft) * CDbl(37.79524)
            Case 5 '-> Sp�cifi�
                PosX = aBlock.Left_Renamed * CDbl(37.79524)
        End Select

        '-> Tenir compte de la marge interne
        PosX = (PosX - MargeX)

        If aBlock.AlignementTop = 5 And Ligne <> 1 Then
        Else
            Select Case aBlock.AlignementTop
                Case 1 '-> Libre
                    '-> RAS le pointeur est bien positionn�
                Case 2 '-> Marge haut
                    PosY = aspool.Maquette_Renamed.MargeTop * CDbl(37.79524)
                Case 3 '-> Centr�
                    PosY = (aspool.Maquette_Renamed.Hauteur - aBlock.Hauteur) / 2 * CDbl(37.79524)
                Case 4 '-> Marge Bas
                    PosY = aspool.Maquette_Renamed.Hauteur - aBlock.Hauteur * CDbl(37.79524)
                Case 5 '-> Sp�cifi�
                    PosY = aBlock.Top * CDbl(37.79524)
            End Select

            '-> Modifier l'alignement
            If aBlock.AlignementTop <> 1 Then PosY = PosY - MargeY
        End If

        '-> Impression du block de ligne
        For i = 1 To aBlock.NbCol
            '-> Pointer sur la cellule � dessiner
            aCell = aBlock.Cellules.Item("L" & Ligne & "C" & i)
            '-> Remplacer les champs par leur valeur
            aCell.ReplaceField(DataFields)
            '-> on regarde si on a un lien hypertext
            If InStr(1, aCell.ContenuCell, "<url:", CompareMethod.Text) <> 0 Then
                aCell.UseHyperText = True
                aCell.KeyLink = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<url:", vbTextCompare))
                aCell.KeyLink = Mid(aCell.KeyLink, 1, InStr(1, aCell.KeyLink, ">"))
                aCell.ContenuCell = Replace(aCell.ContenuCell, aCell.KeyLink, "")
                aCell.KeyLink = Trim(Mid(aCell.KeyLink, 6, Len(aCell.KeyLink) - 6))
            End If
            '-> on regarde si on a pas une balise form
            If InStr(1, aCell.ContenuCell, "<frm:", vbTextCompare) <> 0 Then
                aCell.UseFormText = True
                aCell.KeyForm = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<frm:", vbTextCompare))
                aCell.KeyForm = Mid(aCell.KeyForm, 1, InStr(1, aCell.KeyForm, ">"))
                aCell.ContenuCell = Replace(aCell.ContenuCell, aCell.KeyForm, "")
                aCell.KeyForm = Trim(Mid(aCell.KeyForm, 6, Len(aCell.KeyForm) - 6))
                If aCell.KeyForm <> "" Then aCell.ContenuCell = aCell.KeyForm
                If aCell.ContenuCell <> "" And aCell.KeyForm = "" Then aCell.KeyForm = aCell.ContenuCell
            End If
            '-> on regarde si on doit tout passer en majusules
            If InStr(1, aCell.ContenuCell, "<upc:", vbTextCompare) <> 0 Then
                strTempo = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<upc:", vbTextCompare))
                strTempo = Mid(strTempo, 1, InStr(1, strTempo, ">"))
                aCell.ContenuCell = Replace(aCell.ContenuCell, strTempo, UCase(Mid(strTempo, 6, Len(strTempo) - 6)))
            End If
            '-> on regarde si on doit tout passer en minuscules
            If InStr(1, aCell.ContenuCell, "<upl:", vbTextCompare) <> 0 Then
                strTempo = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<upl:", vbTextCompare))
                strTempo = Mid(strTempo, 1, InStr(1, strTempo, ">"))
                aCell.ContenuCell = Replace(aCell.ContenuCell, strTempo, LCase(Mid(strTempo, 6, Len(strTempo) - 6)))
            End If
            '-> on regarde si on doit tout passer en italique
            If InStr(1, aCell.ContenuCell, "<sti:>", vbTextCompare) <> 0 Then
                aCell.ContenuCell = Replace(aCell.ContenuCell, "<sti:>", "")
                aCell.FontItalic = True
                If aCell.FontItalic = False Then topItalic = True
            End If
            '-> on regarde si on doit tout passer en gras
            If InStr(1, aCell.ContenuCell, "<stb:>", vbTextCompare) <> 0 Then
                aCell.ContenuCell = Replace(aCell.ContenuCell, "<stb:>", "")
                aCell.FontBold = True
                If aCell.FontBold = False Then topBold = True
            End If
            '-> on regarde si on doit tout passer en soulign�
            If InStr(1, aCell.ContenuCell, "<stu:>", vbTextCompare) <> 0 Then
                aCell.ContenuCell = Replace(aCell.ContenuCell, "<stu:>", "")
                aCell.FontUnderline = True
                If aCell.FontUnderline = False Then topUnderline = True
            End If
            '-> on regarde si on doit appliquer une couleur particuliere style color = stc
            If InStr(1, aCell.ContenuCell, "<stc:", vbTextCompare) <> 0 Then
                strTempo = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<stc:", vbTextCompare))
                aCell.ContenuCell = Replace(aCell.ContenuCell, strTempo, "")
                strTempo = Mid(strTempo, 1, InStr(1, strTempo, ">"))
                strTempo = Mid(strTempo, 6, Len(strTempo) - 6)
                aCell.BackColor = Val(strTempo)
                topColor = True
            End If
            '-> on regarde si on doit afficher le total des pages
            If InStr(1, aCell.ContenuCell, "<tot:>", vbTextCompare) <> 0 Then
                aCell.ContenuCell = Replace(aCell.ContenuCell, "<tot:>", aspool.NbPageR)
            End If
            '-> on regarde si on a pass� une image
            If InStr(1, aCell.ContenuCell, "<img:", vbTextCompare) <> 0 Then
                aCell.Image = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<img:", vbTextCompare))
                If InStr(1, aCell.Image, ">") <> 0 Then
                    aCell.Image = Mid(aCell.Image, 1, InStr(1, aCell.Image, ">"))
                Else
                    aCell.Image = aCell.Image & ">"
                End If
                aCell.ContenuCell = Replace(aCell.ContenuCell, aCell.Image, "")
                aCell.Image = Trim(Mid(aCell.Image, 6, Len(aCell.Image) - 6))
            End If
            '-> on regarde si on a pass� une hauteur image
            If InStr(1, aCell.ContenuCell, "<imh:", vbTextCompare) <> 0 Then
                aCell.ImageHeight = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<imh:", vbTextCompare))
                If InStr(1, aCell.ImageHeight, ">") <> 0 Then
                    aCell.ImageHeight = Mid(aCell.ImageHeight, 1, InStr(1, aCell.ImageHeight, ">"))
                Else
                    aCell.ImageHeight = aCell.ImageHeight & ">"
                End If
                aCell.ContenuCell = Replace(aCell.ContenuCell, aCell.ImageHeight, "")
                aCell.ImageHeight = Trim(Mid(aCell.ImageHeight, 6, Len(aCell.ImageHeight) - 6))
            End If
            '-> on regarde si on a pass� une largeur image
            If InStr(1, aCell.ContenuCell, "<imw:", vbTextCompare) <> 0 Then
                aCell.ImageWidth = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<imw:", vbTextCompare))
                If InStr(1, aCell.ImageWidth, ">") <> 0 Then
                    aCell.ImageWidth = Mid(aCell.ImageWidth, 1, InStr(1, aCell.ImageWidth, ">"))
                Else
                    aCell.ImageWidth = aCell.ImageWidth & ">"
                End If
                aCell.ContenuCell = Replace(aCell.ContenuCell, aCell.ImageWidth, "")
                aCell.ImageWidth = Trim(Mid(aCell.Image, 6, Len(aCell.ImageWidth) - 6))
            End If
            '-> on regarde si on doit afficher des informations de la page de garde
            If InStr(1, aCell.ContenuCell, "<gar:", vbTextCompare) <> 0 Then
                Dim strGar As String
                Dim sField As String
                Dim sKey As String
                Dim j As Integer
                For j = 1 To NumEntries(aCell.ContenuCell, "<gar:") - 1
                    strGar = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<gar:", vbTextCompare))
                    If InStr(1, strGar, ">") <> 0 Then
                        strGar = Mid(strGar, 1, InStr(1, strGar, ">"))
                    Else
                        strGar = strGar & ">"
                    End If
                    sField = Entry(1, Replace(Replace(strGar, "<gar:", ""), ">", ""), "�")
                    sKey = Entry(2, Replace(Replace(strGar, "<gar:", ""), ">", ""), "�")
                    If Entry(3, Replace(Replace(strGar, "<gar:", ""), ">", "") + "��", "�") <> "" Then
                        aCell.ContenuCell = Replace(aCell.ContenuCell, strGar, Mid(getFieldGarde(sField, sKey), Val(Entry(3, Replace(Replace(strGar, "<gar:", ""), ">", ""), "�"))))
                    Else
                        aCell.ContenuCell = Replace(aCell.ContenuCell, strGar, getFieldGarde(sField, sKey))
                    End If
                Next
            End If
            '-> on regarde si on doit afficher le total des pages
            If InStr(1, aCell.ContenuCell, "<pex:>", vbTextCompare) <> 0 Then
                aCell.ContenuCell = Replace(aCell.ContenuCell, "<pex:>", "")
            End If
            '-> Imprimer la cellule
            If Not DrawCell(aCell, PosX, PosY, aspool, (aTb.Nom), NomSousObjet) Then GoTo GestBlock
            '-> R�cup�rer la hauteur de la ligne
            HauteurLigPix = (aCell.Y2 - aCell.Y1)
        Next

        If Ligne = aBlock.NbLigne Then PosY = PosY + 1
        PrintBlock = PosY + HauteurLigPix

        '-> on vide les variables
        aBlock = Nothing
        aCell = Nothing
        Exit Function

GestBlock:
        Call GestError(40, aTb.Nom & "|" & NomSousObjet, aspool)
        PrintBlock = -9999

    End Function

    Private Function getFieldGarde(ByVal sField As String, ByVal sKey As String) As String
        '--> cette fonction va permettre de recuperer le texte de garde issu des batchs
        Dim i As Integer
        Dim expgarde
        Dim strLigne As String
        Dim strValue As String
        Dim strField As String
        Dim strLastField As String
        Dim dogIdent As String
        Dim dogLangue As String
        Dim typSel As String
        Dim strTemp As String
        Dim strGroup As String
        Dim strVue As String
        Dim strNomspool As String
        Dim topField As Boolean

        getFieldGarde = ""
        expgarde = Split(strGarde, Chr(0))

        For i = 1 To UBound(expgarde)
            strLigne = expgarde(i)
            strLigne = Replace(strLigne, Chr(9), "")
            strLigne = Replace(strLigne, Chr(13), "")
            Select Case Trim(UCase(Entry(1, sField, "=")))
                Case "KEYWORD"
                    'si on est sur la bonne section on cherche la valeur
                    If topField Then
                        If UCase(Entry(1, strLigne, ">")) = UCase("<" & sKey & "/") Then
                            getFieldGarde = Trim(Entry(2, strLigne, ">"))
                            GoTo GestError
                        End If
                    End If
                    'on regarde si on est sur la bonne section
                    If UCase(Entry(1, strLigne, ">")) = "<KEYWORD/" Then
                        If Trim(UCase(Entry(2, sField, "="))) = Trim(Entry(2, Trim(UCase(strLigne)), ">")) Then
                            topField = True
                        Else
                            topField = False
                        End If
                    End If
                Case Else
                    If UCase(Entry(1, strLigne, ">")) = "<" & Trim(UCase(sField)) & "/" Then
                        getFieldGarde = Trim(Entry(2, strLigne, ">"))
                        GoTo GestError
                    End If
            End Select

        Next
GestError:
    End Function

    Private Function IsGoodFont(ByVal MyFont As String) As String

        '---> Cette fonction d�termine si une font est valide
        On Error GoTo GestError

        frmLib.Font = VB6.FontChangeName(frmLib.Font, MyFont)
        IsGoodFont = MyFont

        Exit Function

GestError:

        IsGoodFont = "Arial"


    End Function


    Public Function DrawCell(ByRef aCell As Cellule, ByVal PosX As Integer, ByVal PosY As Integer, ByRef aspool As Spool, ByRef NomTb As String, Optional ByRef NomBlock As String = "") As Boolean

        '---> Fonction qui dessine une cellule
        Dim aFt As FormatedCell
        Dim PrintSigne As Boolean
        Dim Align As Integer
        Dim FontAlias As String
        Dim IsCellAjust As Boolean
        Dim y1, x1, X2, Y2 As Integer
        Dim acellStyle As vbPDF.pdfFontStyle

        On Error GoTo GestErr
        x1 = aCell.X1 + PosX
        X2 = aCell.X2 + PosX
        y1 = PosY
        Y2 = aCell.Y2 - aCell.Y1 + PosY
        '-> Gestion des formats du texte de la cellule
        Select Case aCell.TypeValeur
            Case 0 '-> RAS
                PrintSigne = False
            Case 1 '-> Caract�re
                '-> Faire un susbstring
                aCell.ContenuCell = Mid(aCell.ContenuCell, 1, CShort(aCell.Msk))
                PrintSigne = False
            Case 2 '-> Numeric
                '-> V�rifier s'il y a quelquechose � formater
                If Trim(aCell.ContenuCell) = "" Then
                Else
                    aFt = FormatNumTP(Trim(aCell.ContenuCell), aCell.Msk, NomBlock, "L" & aCell.Ligne & "C" & aCell.Colonne)
                    '-> Rajouter Trois blancs au contenu de la cellule
                    aFt.strFormatted = aFt.strFormatted & "   "
                    If aFt.Ok Then
                        '-> Mettre en rouge si necessaire
                        If aFt.value < 0 And aFt.idNegatif > 1 Then
                            aCell.FontColor = QBColor(12)
                        Else
                            If aCell.FontColor = QBColor(12) Then
                                aCell.FontColor = QBColor(0)
                            End If
                        End If
                        '-> affecter le contenu de la cellule
                        aCell.ContenuCell = aFt.strFormatted
                        '-> Si c'est un 0 mettre � blanc si pas imprimer
                        If CDbl(aCell.ContenuCell) = 0 And Not (aCell.PrintZero) Then
                            aCell.ContenuCell = ""
                        Else
                            If aFt.value < 0 Then
                                If aFt.idNegatif = 0 Or aFt.idNegatif = 3 Then
                                    aCell.ContenuCell = "- " & aCell.ContenuCell
                                    PrintSigne = False
                                ElseIf aFt.idNegatif = 1 Or aFt.idNegatif = 4 Then
                                    PrintSigne = True
                                End If
                            End If
                        End If
                    Else
                        '-> Ne pas imprimer  de signe
                        PrintSigne = False
                    End If
                End If
        End Select
        '-> on ajoute si besoin la font
        FontAlias = aPrinter.AddFont(GetFontFileName(aCell.FontName), "", False)
        If FontAlias = "" Then FontAlias = aCell.FontName
        '-> Appliquer les options de font
        acellStyle = vbPDF.pdfFontStyle.pdfNormal
        '-> si la cellule est en gras
        If aCell.FontBold Then acellStyle = acellStyle Or vbPDF.pdfFontStyle.pdfBold
        '-> si la cellule est en italique
        If aCell.FontItalic Then acellStyle = acellStyle Or vbPDF.pdfFontStyle.pdfItalic
        '-> si la cellule est soulign�e
        If aCell.FontUnderline Then acellStyle = acellStyle Or vbPDF.pdfFontStyle.pdfUnderline

        '-> on applique le style
        aPrinter.SetFont(FontAlias, 0.0!, acellStyle)

        '-> on affecte les bonnes couleurs � la cellule
        If aCell.FontColor = 0 Then
            aPrinter.SetColorText("Black")
        Else
            aPrinter.SetColorText("#" & ConvertColor(aCell.FontColor))
        End If
        aPrinter.SetColorFill("#" & ConvertColor(aCell.BackColor))
        aPrinter.SetColorLine("#" & ConvertColor(aCell.BackColor))

        '-> on affecte la bonne taille � la police de la cellule
        aPrinter.SetFontSize(aCell.FontSize)

        '-> on applique le style
        aPrinter.SetFont(FontAlias, 0.0!, acellStyle)
        '***********************************
        '* Dessin du contenu de la cellule *
        '***********************************

        '-> R�cup�rer l'option de d'alignement interne
        IsCellAjust = aCell.AutoAjust

        '-> Alignement interne dans le rectangle
        Select Case aCell.CellAlign
            Case 1
                Align = vbPDF.pdfTextAlignment.pdfAlignLeft Or vbPDF.pdfTextAlignment.pdfAlignTop
            Case 2
                Align = vbPDF.pdfTextAlignment.pdfCenter Or vbPDF.pdfTextAlignment.pdfAlignTop
            Case 3
                Align = vbPDF.pdfTextAlignment.pdfAlignRight Or vbPDF.pdfTextAlignment.pdfAlignTop
            Case 4
                Align = vbPDF.pdfTextAlignment.pdfAlignLeft Or vbPDF.pdfTextAlignment.pdfMiddle
            Case 5
                Align = vbPDF.pdfTextAlignment.pdfCenter Or vbPDF.pdfTextAlignment.pdfMiddle
            Case 6
                Align = vbPDF.pdfTextAlignment.pdfAlignRight Or vbPDF.pdfTextAlignment.pdfMiddle
            Case 7
                Align = vbPDF.pdfTextAlignment.pdfAlignBottom Or vbPDF.pdfTextAlignment.pdfAlignLeft
            Case 8
                Align = vbPDF.pdfTextAlignment.pdfAlignBottom Or vbPDF.pdfTextAlignment.pdfCenter
            Case 9
                Align = vbPDF.pdfTextAlignment.pdfAlignBottom Or vbPDF.pdfTextAlignment.pdfAlignRight
        End Select

        '-> on dessine la cellule en fonction de l'alignement
        '-> on tient compte de marges interne

        If Not aCell.FondTransparent Then
            aPrinter.DrawTextBox((x1) / CDbl(37.79524), (y1 / CDbl(37.79524)) + 0.013, ((aCell.X2 - aCell.X1) / CDbl(37.79524)), (aCell.Y2 - aCell.Y1) / CDbl(37.79524), aCell.ContenuCell, 0.0!, Align, vbPDF.pdfDrawOptions.pdfFilled, IsCellAjust)
        Else
            If aCell.BordureBas <> False And aCell.BordureHaut <> False Then
                aPrinter.DrawTextBox((x1) / CDbl(37.79524), (y1 / CDbl(37.79524)) + 0.013, ((aCell.X2 - aCell.X1) / CDbl(37.79524)), (aCell.Y2 - aCell.Y1) / CDbl(37.79524), aCell.ContenuCell, 0.0!, Align, vbPDF.pdfDrawOptions.pdfVisible, IsCellAjust)
            Else
                aPrinter.DrawTextBox((x1) / CDbl(37.79524), (y1 / CDbl(37.79524)) + 0.013, ((aCell.X2 - aCell.X1) / CDbl(37.79524)), (aCell.Y2 - aCell.Y1) / CDbl(37.79524), aCell.ContenuCell, 0.0!, Align, vbPDF.pdfDrawOptions.pdfInvisible, IsCellAjust)
            End If
        End If

        '-> si on doit imprimer le signe
        If PrintSigne Then
            aPrinter.DrawTextBox((x1) / CDbl(37.79524) + ((aCell.X2 - aCell.X1) / CDbl(37.79524)) - aPrinter.TextLength("- "), (y1 / CDbl(37.79524)), aPrinter.TextLength("- "), (aCell.Y2 - aCell.Y1) / CDbl(37.79524), "- ", 0.0!, Align, vbPDF.pdfDrawOptions.pdfFilled)
        End If

        '-> on regarde si on a une image sur la cellule
        If aCell.Image <> "" Then
            '-> cr�er une nouvelle image
            Dim acellimage As String

            acellimage = My.Application.Info.DirectoryPath & "/images/" & GetFileName(aCell.Image)
            If File.Exists(acellimage) Then
                'aPrinter.DrawImage(NomFichier, PosX, PosY, , , , , True)
                If aCell.ImageHeight <> "" Then
                    Dim abmp As ImageObj
                    abmp = New ImageObj
                    aCell.Image = ""
                    abmp.Left_Renamed = CSng(x1 / CDbl(37.79524))
                    abmp.Top = CSng(y1 / CDbl(37.79524))
                    'abmp.IdAffichage = aspool.frmdisplay.lblPicture.Count
                    abmp.Nom = acellimage
                    abmp.Hauteur = aCell.ImageHeight
                    If listImage Is Nothing Then listImage = New Collection
                    listImage.add(abmp)
                    'aPrinter.DrawImage(acellimage, CSng(x1 / CDbl(37.79524)), CSng(y1 / CDbl(37.79524)), , CSng(aCell.ImageHeight), , , True)
                ElseIf aCell.ImageWidth <> "" Then
                    Dim abmp As ImageObj
                    abmp = New ImageObj
                    aCell.Image = ""
                    abmp.Left_Renamed = x1
                    abmp.Top = y1
                    'abmp.IdAffichage = aspool.frmdisplay.lblPicture.Count
                    abmp.Nom = acellimage
                    abmp.Largeur = aCell.ImageWidth
                    If listImage Is Nothing Then listImage = New Collection
                    listImage.Add(abmp)
                    'aPrinter.DrawImage(acellimage, CSng(x1 / CDbl(37.79524)), CSng(y1 / CDbl(37.79524)), CSng(aCell.ImageWidth), , , , True)
                Else
                    'MODIF le 03032014
                    'aPrinter.DrawImage(acellimage, (x1) / CDbl(37.79524), y1 / CDbl(37.79524), , , , , True)
                    aPrinter.DrawImage(acellimage, CSng(x1 / CDbl(37.79524)), CSng(y1 / CDbl(37.79524)), , , , , True)
                End If
            End If
            aCell.Image = ""
        End If

        '**** On dessine maintenant si besoin les bordures de la cellules
        '-> on definit l'epaisseur de la ligne
        aPrinter.SetLineWidth(0.02)
        '-> Dessin de la bordure Bas
        If aCell.BordureBas Then
            aPrinter.SetColorLine("Black")
            aPrinter.DrawLine((x1 / CDbl(37.79524)) - 0.01, (Y2 / CDbl(37.79524)) + 0.01, (X2 / CDbl(37.79524)) + 0.01, (Y2 / CDbl(37.79524)) + 0.01)
        Else
            If aCell.BackColor <> &HFFFFFF& Then
                aPrinter.SetColorLine("#" & ConvertColor(aCell.BackColor))
                aPrinter.DrawLine((x1 / CDbl(37.79524)) - 0.01, (Y2 / CDbl(37.79524)) + 0.01, (X2 / CDbl(37.79524)) + 0.01, (Y2 / CDbl(37.79524)) + 0.01)
            End If
        End If

        '-> Dessin de la bordure haut
        If aCell.BordureHaut Then
            aPrinter.SetColorLine("Black")
            aPrinter.DrawLine((x1 / CDbl(37.79524)) - 0.01, (y1 / CDbl(37.79524)) - 0.01, (X2 / CDbl(37.79524)) + 0.01, (y1 / CDbl(37.79524)) - 0.01)
        End If

        '-> Dessin de la bordure gauche
        If aCell.BordureGauche Then
            aPrinter.SetColorLine("Black")
            aPrinter.DrawLine((x1 / CDbl(37.79524)) - 0.01, (y1 / CDbl(37.79524)) - 0.01, (x1 / CDbl(37.79524)) - 0.01, (Y2 / CDbl(37.79524)) + 0.01)
        End If

        '-> Dessin de la bordure Droite
        If aCell.BordureDroite Then
            aPrinter.SetColorLine("Black")
            aPrinter.DrawLine((X2 / CDbl(37.79524)) - 0.01, (y1 / CDbl(37.79524)) - 0.01, (X2 / CDbl(37.79524)) - 0.01, (Y2 / CDbl(37.79524)) + 0.01)
        End If

        '-> Renvoyer une valeur de succ�s
        DrawCell = True

        '-> on vide les variables
        aFt = Nothing
        acellStyle = Nothing

        '-> on regarde si on a un lien hypertext
        If aCell.UseHyperText Then
            '-> cr�er un nouveau Label
            aPrinter.AddWebLink(aCell.KeyLink, x1 / CDbl(37.79524), y1 / CDbl(37.79524), (X2 - x1) / CDbl(37.79524), (Y2 - y1) / CDbl(37.79524), 0)
        End If 'Si on est en vosu ou en impression
        '-> on regarde si on a un lien hypertext
        If aCell.UseFormText Then
            aPrinter.DrawTextBox((x1) / CDbl(37.79524), (y1 / CDbl(37.79524)) + 0.013, ((aCell.X2 - aCell.X1) / CDbl(37.79524)), (aCell.Y2 - aCell.Y1) / CDbl(37.79524), aCell.KeyForm, 0.0!, Align, vbPDF.pdfDrawOptions.pdfInvisible, IsCellAjust)
            aCell.UseFormText = False
        End If


        Exit Function

GestErr:

        Call GestError(41, NomTb & "|" & NomBlock & "|" & "L" & aCell.Ligne & "C" & aCell.Colonne & aCell.Contenu, aspool)

    End Function

    Public Function GetFileName(ByVal MyFile As String) As String

        Dim i As Integer
        MyFile = Replace(MyFile, "/", "\")
        i = InStrRev(MyFile, "\")
        If i = 0 Then
            GetFileName = MyFile
        Else
            GetFileName = Mid$(MyFile, i + 1, Len(MyFile) - i)
        End If

    End Function

    Private Function ConvertColor(ByVal Color As Long) As String

        '---> Cette fonction convertit une couleur exprim�e en DEC au format HTML

        Dim Red As Long
        Dim Green As Long
        Dim Blue As Long
        Dim Couleur As String

        '-> formatter la couleur � traduire
        Couleur = FixLen(Hex$(Color), "000000")
        Red = CInt("&H" & Right$(Couleur, 2))
        Green = CInt("&H" & Mid$(Couleur, 3, 2))
        Blue = CInt("&H" & Left$(Couleur, 2))

        '-> Pour internet, inverser les couleurs
        Couleur = Hex$(RevRGB(Red, Green, Blue))

        '-> Remplir avec des 0 si necessaire
        Do While Len(Couleur) <> 6
            Couleur = "0" & Couleur
        Loop

        ConvertColor = Couleur

    End Function
    Private Function RevRGB(ByVal Red As Long, ByVal Green As Long, ByVal Blue As Long) As Long
        RevRGB = CLng(Blue + (Green * 256) + (Red * 65536))
    End Function

    Private Function FixLen(ByVal sIn As String, ByVal sMask As String) As String

        If Len(sIn) < Len(sMask) Then
            FixLen = Left$(sMask, Len(sMask) - Len(sIn)) & sIn
        Else
            FixLen = Right$(sIn, Len(sMask))
        End If

    End Function

    Public Sub AnalyseObj(ByVal Ligne As String, ByRef TypeObjet As String, ByRef NomObjet As String, ByRef TypeSousObjet As String, ByRef NomSousObjet As String, ByRef Param As String, ByRef DataFields As String)

        Dim Param1 As String
        Dim Param2 As String
        Dim Param3 As String
        Dim Param4 As String

        '---> Procedure qui r�cupre les d�finitions des objets dans une ligne
        On Error Resume Next

        If Ligne = "" Then Exit Sub

        Param1 = Entry(1, Ligne, "]")
        Param2 = Entry(2, Ligne, "]")
        'Param3 = Entry(2, Ligne, "{")
        '-> pb sur le caractere { present dans les valeurs
        Param3 = Mid(Ligne, InStr(1, Ligne, "{") + 1)

        Param1 = Mid(Param1, 2, Len(Param1) - 2)
        '-> R�cup�ration du champ de donn�es
        If InStr(1, Param3, "}") <> 0 Then
            DataFields = Mid$(Param3, 1, Len(Param3) - 1)
        Else
            DataFields = Param3 + " "
        End If
        '-> R�cup�ration des param�tres d'application
        Param = Mid(Param2, 2, Len(Param2) - 1)
        '-> R�cup�ration du nom des objets
        Param2 = Entry(1, Param1, "(")
        Param3 = Entry(2, Param1, "(")

        '-> Objet de premier niveau
        TypeObjet = Entry(1, Param2, "-")
        NomObjet = Entry(2, Param2, "-")

        '-> Objet de second niveau
        TypeSousObjet = Entry(1, Param3, "-")
        NomSousObjet = Entry(2, Param3, "-")


    End Sub
    Private Function PrintSection(ByVal SectionName As String, ByRef Param As String, ByRef DataFields As String, ByRef FirstObj As Boolean, ByRef PositionX As Integer, ByRef PositionY As Integer, ByRef aspool As Spool) As Boolean

        '---> Cette proc�dure est charg�e d'imprimer une section

        Dim aSection As Section
        Dim i As Short
        Dim NomObjet As String
        Dim aCadre As Cadre
        Dim aBmp As ImageObj
        Dim DebutRangX As Integer
        Dim DebutRangY As Integer

        'On Error GoTo GestError
        Trace(SectionName)
        '-> Pointer sur la section � imprimer
        aSection = aspool.Maquette_Renamed.Sections.Item(Trim(UCase(SectionName)))

        '-> Imprimer la section
        If Not PrintObjRtf(PositionX, PositionY, aSection, DataFields, DebutRangX, DebutRangY, aspool) Then GoTo GestError

        '-> Impression des objets associ�s dans l'ordre d'affichage
        For i = 1 To aSection.nEntries - 1
            NomObjet = aSection.GetOrdreAffichage(i)
            If UCase(Entry(1, NomObjet, "-")) = "CDR" Then
                '-> Pointer sur le cadre
                aCadre = aSection.Cadres.Item(UCase(Entry(2, NomObjet, "-")))
                '-> Imprimer l'objet
                If Not PrintObjRtf(PositionX, PositionY, aCadre, DataFields, DebutRangX, DebutRangY, aspool, aCadre.MargeInterne) Then GoTo GestError
                '-> Liberer le pointeur
                aCadre = Nothing
            ElseIf UCase(Entry(1, NomObjet, "-")) = "BMP" Then
                aBmp = aSection.Bmps.Item(UCase(Entry(2, NomObjet, "-")))
                If Not PrintBmp(aBmp, DebutRangX, DebutRangY, DataFields, aspool) Then GoTo GestError
            End If
        Next

        '-> Positionner le pointeur de position apr�s l'�dition de la section
        PositionY = (aSection.Hauteur * CDbl(37.79524)) + PositionY
        aSection = Nothing
        aCadre = Nothing
        PrintSection = True
        Exit Function

GestError:

        Call GestError(36, aSection.Nom, aspool)
        PrintSection = False

    End Function

    Private Function PrintBmp(ByRef aBmp As ImageObj, ByRef DebutRangX As Integer, ByRef DebutRangY As Integer, ByVal DataFields As String, ByRef aspool As Spool) As Boolean

        '---> Impression d'un BMP

        Dim PosX As Double
        Dim PosY As Double
        Dim Champ As String
        Dim NomFichier As String = ""
        Dim hdlPen As Integer
        Dim OldPen As Integer
        Dim hdlBrush As Integer
        Dim oldBrush As Integer
        Dim i As Short
        Dim vbSrcCopy As Integer
        Dim strFichier As String

        On Error GoTo GestErr

        '-> Il faut modifier la position X et Y du cadre
        PosX = (DebutRangX) / CDbl(37.79524) / 1.492105 + aBmp.Left_Renamed '* CDbl(3.779524)
        PosY = (DebutRangY) / CDbl(37.79524) / CDbl(37.79524) + aBmp.Top
        Trace("printbmp")
        '-> si on a une Image prioritaire on essai de voir si on la trouve
        If aBmp.sDefault <> "" Then
            '-> si on a une image
            If InStr(1, aBmp.sDefault, "^", vbTextCompare) <> 0 Then
                '-> on est sur un dieze on recupere sa valeur
                '-> R�cup�ration du nombre de champs
                For i = 2 To NumEntries(DataFields, "^")
                    Champ = Entry(i, DataFields, "^")
                    If UCase$(aBmp.sDefault) = "^" & UCase$(Mid$(Champ, 1, 4)) Then
                        '-> R�cup�ration du nom du fichier
                        NomFichier = RTrim(Mid$(Champ, 5, Len(Champ) - 1))
                        '-> Test de l'utilisation de l'association
                        strFichier = GetPictureAssociation(NomFichier)
                        If strFichier <> "" Then
                            '-> R�cup�rer le nom du fichier dans le fichier tm_picture.ini
                            If NomFichier = strFichier Then Trace("Image non trouv�e dans le tm_picture")
                            NomFichier = strFichier
                        Else
                            If Dir$(NomFichier) = "" Then NomFichier = My.Application.Info.DirectoryPath & "\Images\" & NomFichier
                        End If
                        Exit For
                    End If
                Next
            Else
                '-> on est sur un nom de fichier
                NomFichier = Trim(aBmp.sDefault)
            End If

            '-> on teste si l'image est dans /images/
            If Dir$(NomFichier) = "" Then
                If Dir$(My.Application.Info.DirectoryPath & "\Images\" & NomFichier) <> "" Then
                    NomFichier = My.Application.Info.DirectoryPath & "\Images\" & NomFichier
                End If
            End If
            If Dir$(NomFichier) <> "" And Trim(NomFichier <> "") Then
                '-> si on a ramen� un nom de fichier on passe en image fixe pour continuer le traitement
                '-> V�rifier que le fichier existe
                '-> Imprimer le bmp
                Trace("Fic " + NomFichier)
                aPrinter.DrawImage(NomFichier, PosX, PosY, aBmp.Largeur, , , , True)
                '-> Renvoyer une valeur de succ�s
                PrintBmp = True
                Exit Function
            End If
        End If

        If aBmp.isVariable Then
            '-> Venir charg� le BMP associ�
            '-> R�cup�ration du nombre de champs
            For i = 2 To NumEntries(DataFields, "^")
                Champ = Entry(i, DataFields, "^")
                Trace("Champ:" + Champ)
                If UCase(aBmp.Fichier_Renamed) = "^" & UCase(Mid(Champ, 1, 4)) Then
                    '-> R�cup�ration du nom du fichier
                    NomFichier = RTrim(Mid(Champ, 5, Len(Champ) - 1))
                    Trace("tutu:" + NomFichier)
                    '-> Test de l'utilisation de l'association
                    If aBmp.UseAssociation Then
                        '-> R�cup�rer le nom du fichier dans le fichier ini
                        Trace("tata:" + NomFichier)
                        NomFichier = GetPictureAssociation(NomFichier)
                    End If
                    Exit For
                End If
            Next
            Trace("!!!" + NomFichier)
            If Dir$(NomFichier) = "" Then
                If Dir$(My.Application.Info.DirectoryPath & "\Images\" & NomFichier) <> "" Then
                    NomFichier = My.Application.Info.DirectoryPath & "\Images\" & NomFichier
                End If
            End If
            '-> V�rifier que le fichier existe
            If Dir(NomFichier) = "" Or Trim(NomFichier) = "" Then
            Else
                Trace("Fic 2" + NomFichier)
                If File.Exists(NomFichier) Then
                    'aPrinter.DrawImage(NomFichier, PosX, PosY, , , , , True)
                    'MODIF le 03032014
                    aPrinter.DrawImage(NomFichier, PosX, PosY, , , , , True)
                End If
            End If
        Else
            If Trim(aBmp.Fichier_Renamed) = "" Then
            Else
                If Dir$(aBmp.Fichier_Renamed, FileAttribute.Normal) = "" Then
                    If Dir$(My.Application.Info.DirectoryPath & "\Images\" & aBmp.Fichier_Renamed) <> "" Then
                        NomFichier = My.Application.Info.DirectoryPath & "\Images\" & aBmp.Fichier_Renamed
                        aPrinter.DrawImage(NomFichier, PosX, PosY, , , , , True)
                    End If
                Else
                    If File.Exists(aBmp.Fichier_Renamed) Then
                        'aPrinter.DrawImage(aBmp.Fichier_Renamed, PosX, PosY, aBmp.Largeur, aBmp.Hauteur, , , True)
                        'MODIF 04/03/2014
                        aPrinter.DrawImage(aBmp.Fichier_Renamed, PosX, PosY, , , , , True)
                        'REMODIF 16/12/2014...... ?????
                        aPrinter.DrawImage(aBmp.Fichier_Renamed, PosX, PosY, aBmp.Largeur, aBmp.Hauteur, , , True)
                    End If
                End If
            End If
        End If

        '-> Dessiner la bordure si necessaire
        If aBmp.Contour Then
            'hdlBrush = CreateSolidBrush(0)
            'aRect.Left_Renamed = PosX
            'aRect.Top = PosY
            ''UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleX. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'aRect.Right_Renamed = aRect.Left_Renamed + Sortie.ScaleX(VB6.PixelsToTwipsX(aPic.Width), 1, 3)
            ''UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.ScaleY. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'aRect.Bottom = aRect.Top + Sortie.ScaleY(VB6.PixelsToTwipsY(aPic.Height), 1, 3)
            ''UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Sortie.hdc. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'FrameRect(Sortie.hdc, aRect, hdlBrush)
            'DeleteObject(hdlBrush)
        End If

        '-> Renvoyer une valeur de succ�s
        PrintBmp = True

        Exit Function

GestErr:

        Call GestError(39, aBmp.SectionName & "|" & aBmp.Nom, aspool)
        PrintBmp = False


    End Function

    Private Function PrintObjRtf(ByRef PositionX As Integer, ByRef PositionY As Integer, ByRef aObject As Object, ByVal DataFields As String, ByRef DebutRangX As Integer, ByRef DebutRangY As Integer, ByRef aspool As Spool, Optional ByRef MargeInterne As Single = 0) As Boolean


        '---> Cette fonction imprime un cadre ou une section ( Code RTF + Bordures )
        Dim aField(2, 1) As String
        Dim aRtf As System.Windows.Forms.RichTextBox
        Dim aRtfSection As New clsRtf
        Dim PosX, PosY As Double
        Dim i As Short
        Dim Champ As String
        Dim RTFValue As String
        Dim NbChamp As Short
        Dim IsCadre As Boolean
        Dim image As Image
        Dim g As Graphics

        On Error GoTo GestErr
        'aRtf = aRtf.Rtf(CInt(aObject.idrtf))
        aRtf = frmLib.Rtf(aObject.IdRTf)
        aRtf.Width = aObject.Largeur * CDbl(37.79524)
        aRtf.Height = aObject.Hauteur * CDbl(37.79524)
        '-> R�cup�ration du code RTF
        RTFValue = aRtf.Rtf

        '-> Cr�er une base de champs
        If Trim(DataFields) <> "" Then
            '-> R�cup�ration du nombre de champs
            NbChamp = NumEntries(DataFields, "^")
            For i = 2 To NbChamp
                Champ = Entry(i, DataFields, "^")
                ReDim Preserve aField(2, i - 1)
                aField(1, i - 1) = "^" & Mid(Champ, 1, 4)
                aField(2, i - 1) = RTrim(Mid(Champ, 5, Len(Champ) - 1))
            Next
            '-> Faire un remplacement des champs par leur valeur
            For i = 1 To NbChamp - 1
                aRtf.Rtf = Replace(aRtf.Rtf, aField(1, i), aField(2, i))
            Next
        End If

        '-> Calcul des positions d'impression de l'objet selon le type d'objet � imprimer
        If TypeOf aObject Is Section Then
            Select Case aObject.AlignementLeft
                Case 2 'Marge gauche
                    PosX = aspool.Maquette_Renamed.MargeLeft * CDbl(37.79524)
                Case 4 'Centr�
                    PosX = ((aspool.Maquette_Renamed.Largeur - aObject.Largeur) / 2) * CDbl(37.79524)
                Case 3 'Marge Droite
                    PosX = (aspool.Maquette_Renamed.Largeur - aObject.Largeur - aspool.Maquette_Renamed.MargeLeft) * CDbl(37.79524)
                Case 5 'Sp�cifi�
                    PosX = aObject.Left_Renamed * CDbl(37.79524)
            End Select

            Select Case aObject.AlignementTop
                Case 1 'Libre
                    PosY = PositionY '* CDbl(37.79524)
                Case 2 'Marge haut
                    PosY = aspool.Maquette_Renamed.MargeTop * CDbl(37.79524)
                Case 4 'Centr�
                    PosY = ((aspool.Maquette_Renamed.Hauteur - aObject.Hauteur) / 2) * CDbl(37.79524)
                Case 3 'Marge bas
                    PosY = (aspool.Maquette_Renamed.Hauteur - aObject.Hauteur - aspool.Maquette_Renamed.MargeTop) * CDbl(37.79524)
                Case 5 'Sp�cifi�
                    PosY = aObject.Top * CDbl(37.79524)
            End Select

            '-> Tenir compte des variations des marges internes
            PosX = PosX - MargeX
            If aObject.AlignementTop <> 1 Then PosY = PosY - MargeY

            '-> Sauvagarder la position de d�but du rang
            DebutRangX = PosX * 1.492105
            DebutRangY = PosY * CDbl(37.79524) '* 1.492105

            If Not imageRtf Then
                '-> on lance l'analyse du rtf
                aRtfSection.SetRtf(aRtf)
            End If
            '-> on cr�e le rtf
            '-> On dessine le cadre
            aPrinter.SetColorFill("#" & ConvertColor(aObject.BackColor))
            '-> on rempli le rectangle
            If aObject.contour Then
                aPrinter.SetColorLine("Black")
                aPrinter.DrawRect(PosX / CDbl(37.79524), PosY / CDbl(37.79524), aObject.largeur, aObject.hauteur, vbPDF.pdfDrawOptions.pdfFilled)
            Else
                aPrinter.SetColorLine("#" & ConvertColor(aObject.BackColor))
                aPrinter.DrawRect(PosX / CDbl(37.79524), PosY / CDbl(37.79524), aObject.largeur, aObject.hauteur, vbPDF.pdfDrawOptions.pdfFilled)
            End If
            '-> On �crit le texte
            If imageRtf Then
                Form1.RichTextBox1.Rtf = aRtf.Rtf
                Form1.RichTextBox1.Width = aObject.Largeur * CDbl(37.79524)
                Form1.RichTextBox1.Height = aObject.Hauteur * CDbl(37.79524)
                Form1.SaveAsJpegButton()
                If aRtf.Text <> "" Then aPrinter.DrawImage(tempImg, CSng(PosX / 37.79524), CSng(PosY / 37.79524), aObject.Largeur, aObject.Hauteur, , , False)
                'Kill(tempImg)
            Else
                aPrinter.DrawTextRtf(PosX / CDbl(37.79524), PosY / CDbl(37.79524), aRtf.Name)
            End If
        ElseIf TypeOf aObject Is Cadre Then
            If Not aObject.IsRoundRect Then
                '-> Il faut modifier la position X et Y du cadre
                PosX = DebutRangX / 1.492105 + aObject.Left_renamed * CDbl(37.79524)
                PosY = DebutRangY / CDbl(37.79524) + aObject.Top * CDbl(37.79524)
                If Not imageRtf Then
                    aRtfSection.SetRtf(aRtf)
                End If
                '-> on rempli le rectangle
                If aObject.backcolor = 16777215 Then
                    aPrinter.SetColorLine("#" & ConvertColor(aObject.BackColor))
                End If
                aPrinter.SetColorFill("#" & ConvertColor(aObject.BackColor))
                aPrinter.DrawRect(PosX / CDbl(37.79524), PosY / CDbl(37.79524), aObject.largeur, aObject.hauteur, vbPDF.pdfDrawOptions.pdfFilled)

                '-> On dessine le cadre
                aPrinter.SetColorLine("Black")
                If aObject.bas Then
                    aPrinter.DrawLine((PosX) / CDbl(37.79524), (PosY + (aObject.hauteur * CDbl(37.79524))) / CDbl(37.79524), (PosX + (aObject.largeur * CDbl(37.79524))) / CDbl(37.79524), (PosY + (aObject.hauteur * CDbl(37.79524))) / CDbl(37.79524))
                End If

                If aObject.haut Then
                    aPrinter.DrawLine((PosX) / CDbl(37.79524), (PosY) / CDbl(37.79524), (PosX + (aObject.largeur * CDbl(37.79524))) / CDbl(37.79524), (PosY) / CDbl(37.79524))
                End If

                If aObject.gauche Then
                    aPrinter.DrawLine((PosX) / CDbl(37.79524), (PosY) / CDbl(37.79524), (PosX) / CDbl(37.79524), (PosY + (aObject.hauteur * CDbl(37.79524))) / CDbl(37.79524))
                End If

                If aObject.droite Then
                    aPrinter.DrawLine((PosX + (aObject.largeur * CDbl(37.79524))) / CDbl(37.79524), (PosY) / CDbl(37.79524), (PosX + (aObject.largeur * CDbl(37.79524))) / CDbl(37.79524), (PosY + (aObject.hauteur * CDbl(37.79524))) / CDbl(37.79524))
                End If
                '-> on ecrit le texte
                If Not imageRtf Then aPrinter.DrawTextRtf(PosX / CDbl(37.79524) + aObject.margeinterne * 1.492105, PosY / CDbl(37.79524) + aObject.margeinterne * 1.492105, aRtf.Name)
                If imageRtf Then
                    Form1.RichTextBox1.Rtf = aRtf.Rtf
                    Form1.RichTextBox1.Width = aObject.Largeur * CDbl(37.79524)
                    Form1.RichTextBox1.Height = aObject.Hauteur * CDbl(37.79524)
                    Form1.SaveAsJpegButton()
                    If aRtf.Text <> "" Then aPrinter.DrawImage(tempImg, CSng(PosX / 37.79524), CSng(PosY / 37.79524), aObject.Largeur, aObject.Hauteur, , , False)
                    'Kill(tempImg) 
                End If
                '-> Indiquer que c'est un cadre
                IsCadre = True
            Else
                '-> cas du rectangle arrondi
                '-> Il faut modifier la position X et Y du cadre
                PosX = DebutRangX / 1.492105 + aObject.Left_renamed * CDbl(37.79524)
                PosY = DebutRangY / CDbl(37.79524) + aObject.Top * CDbl(37.79524)
                If Not imageRtf Then
                    aRtfSection.SetRtf(aRtf)
                End If
                '-> on rempli le rectangle
                aPrinter.SetColorFill("#" & ConvertColor(aObject.BackColor))
                '-> on dessine l'arc de cercle
                aPrinter.SetColorLine("Black")
                aPrinter.DrawArc((PosX) / CDbl(37.79524) + 0.6, (PosY + (aObject.hauteur * CDbl(37.79524))) / CDbl(37.79524) - 0.6, 0.6, 90, 180, , , , vbPDF.pdfDrawOptions.pdfFilled)
                aPrinter.DrawArc((PosX) / CDbl(37.79524) + 0.6, PosY / CDbl(37.79524) + 0.6, 0.6, 180, 270, , , , vbPDF.pdfDrawOptions.pdfFilled)
                aPrinter.DrawArc((PosX + (aObject.largeur * CDbl(37.79524))) / CDbl(37.79524) - 0.6, PosY / CDbl(37.79524) + 0.6, 0.6, 270, 360, , , , vbPDF.pdfDrawOptions.pdfFilled)
                aPrinter.DrawArc((PosX + (aObject.largeur * CDbl(37.79524))) / CDbl(37.79524) - 0.6, (PosY + (aObject.hauteur * CDbl(37.79524))) / CDbl(37.79524) - 0.6, 0.6, 0, 90, , , , vbPDF.pdfDrawOptions.pdfFilled)
                aPrinter.SetColorLine("#" & ConvertColor(aObject.BackColor))
                aPrinter.DrawRect(PosX / CDbl(37.79524) + 0.6 - 0.01, PosY / CDbl(37.79524), aObject.largeur - 1.2 + 0.02, aObject.hauteur, vbPDF.pdfDrawOptions.pdfFilled)
                aPrinter.DrawRect(PosX / CDbl(37.79524), PosY / CDbl(37.79524) + 0.6 - 0.01, 0.6, aObject.hauteur - 1.2 + 0.02, vbPDF.pdfDrawOptions.pdfFilled)
                aPrinter.DrawRect((PosX + (aObject.largeur * CDbl(37.79524))) / CDbl(37.79524) - 0.6, PosY / CDbl(37.79524) + 0.6 - 0.01, 0.6, aObject.hauteur - 1.2 + 0.02, vbPDF.pdfDrawOptions.pdfFilled)
                If aObject.backcolor = 16777215 Then
                    aPrinter.SetColorLine("#" & ConvertColor(aObject.BackColor))
                End If

                '-> On dessine le cadre
                aPrinter.SetColorLine("Black")
                If aObject.bas Then
                    aPrinter.DrawLine((PosX) / CDbl(37.79524) + 0.6 - 0.025, (PosY + (aObject.hauteur * CDbl(37.79524))) / CDbl(37.79524), (PosX + (aObject.largeur * CDbl(37.79524))) / CDbl(37.79524) - 0.6 + 0.025, (PosY + (aObject.hauteur * CDbl(37.79524))) / CDbl(37.79524))
                End If

                If aObject.haut Then
                    aPrinter.DrawLine((PosX) / CDbl(37.79524) + 0.6 - 0.025, (PosY) / CDbl(37.79524), (PosX + (aObject.largeur * CDbl(37.79524))) / CDbl(37.79524) - 0.6 + 0.025, (PosY) / CDbl(37.79524))
                End If

                If aObject.gauche Then
                    aPrinter.DrawLine((PosX) / CDbl(37.79524), (PosY) / CDbl(37.79524) + 0.6 - 0.025, (PosX) / CDbl(37.79524), (PosY + (aObject.hauteur * CDbl(37.79524))) / CDbl(37.79524) - 0.6 + 0.025)
                End If

                If aObject.droite Then
                    aPrinter.DrawLine((PosX + (aObject.largeur * CDbl(37.79524))) / CDbl(37.79524), (PosY) / CDbl(37.79524) + 0.6 - 0.025, (PosX + (aObject.largeur * CDbl(37.79524))) / CDbl(37.79524), (PosY + (aObject.hauteur * CDbl(37.79524))) / CDbl(37.79524) - 0.6 + 0.025)
                End If
                '-> on ecrit le texte
                If imageRtf Then
                    Form1.RichTextBox1.Rtf = aRtf.Rtf
                    Form1.RichTextBox1.Width = aObject.Largeur * CDbl(37.79524)
                    Form1.RichTextBox1.Height = aObject.Hauteur * CDbl(37.79524)
                    Form1.SaveAsJpegButton()
                    If aRtf.Text <> "" Then aPrinter.DrawImage(tempImg, CSng(PosX / 37.79524), CSng(PosY / 37.79524), aObject.Largeur, aObject.Hauteur, , , False)
                    'Kill(tempImg)
                Else
                    aPrinter.DrawTextRtf(PosX / CDbl(37.79524) + aObject.margeinterne * 1.492105, PosY / CDbl(37.79524) + aObject.margeinterne * 1.492105, aRtf.Name)
                End If
                '-> Indiquer que c'est un cadre
                IsCadre = True
            End If
            End If

        '-> Redonner son vrai contenu au controle RTF
        aRtf.Rtf = RTFValue
        aRtf.Refresh()

            '-> Lib�rer le pointeur sur le controle RTF
        aRtf = Nothing
        aRtfSection = Nothing
        '-> Renvoyer une valeur de succes
        PrintObjRtf = True

        Exit Function

GestErr:

            If Err.Number = 11 Then
                PrintObjRtf = True
                Exit Function
            End If

            'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aObject.Nom. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Call GestError(38, aObject.Nom, aspool)
            PrintObjRtf = False

    End Function

    Public Function FormatNumTP(ByVal ToFormat As String, ByVal Msk As String, Optional ByRef NomBlock As String = "", Optional ByRef Cell As String = "") As FormatedCell

        '---> Cette fonction a pour but d'analyser une chaine de caract�re et de la retouner
        'formatt�e en s�parateur de milier et en nombre de d�cimal

        Dim strFormat As String = ""
        Dim strTempo As String = ""
        Dim strPartieEntiere As String = ""
        Dim StrPartieDecimale As String = ""
        Dim i, j As Short
        Dim nbDec As Short
        Dim strSep As String = ""
        Dim idNegatif As Short
        Dim Masque As String = ""
        Dim Tempo As String = ""
        Dim FindSepDec As Boolean

        On Error GoTo GestErr
        FormatNumTP = Nothing

        For i = Len(ToFormat) To 1 Step -1
            If IsNumeric(Mid(ToFormat, i, 1)) Then
                Tempo = Mid(ToFormat, i, 1) & Tempo
            Else
                '-> Gestion du s�parateur d�cimal
                If Mid(ToFormat, i, 1) = "." Or Mid(ToFormat, i, 1) = "," Then
                    If Not FindSepDec Then
                        Tempo = SepDec & Tempo
                        FindSepDec = True
                    End If
                Else
                    Tempo = Mid(ToFormat, i, 1) & Tempo
                End If
            End If
        Next  'Pour tous les caract�res

        ToFormat = Tempo

        If Trim(ToFormat) = "" Then
        Else
            '-> Tester si on envoie une zone num�rique
            If Not IsNumeric(ToFormat) Then
                FormatNumTP.Ok = False
                Exit Function
            End If
        End If

        '-> Charger le masque de la cellule
        nbDec = CShort(Entry(1, Msk, "�"))
        If Entry(2, Msk, "�") <> "" Then
            strSep = SepMil 'Entry(2, Msk, "�")
        Else
            strSep = ""
        End If
        idNegatif = CShort(Entry(3, Msk, "�"))

        FormatNumTP.Ok = True
        FormatNumTP.nbDec = nbDec
        FormatNumTP.value = CDbl(ToFormat)
        FormatNumTP.idNegatif = idNegatif

        '-> Analyse si d�cimale
        If nbDec = 0 Then
            '-> Arrondir
            strTempo = CStr(CDbl(ToFormat))
            Masque = "#########################################0"
        Else
            Masque = "#########################################0." & New String("0", nbDec)
            strTempo = ToFormat
        End If

        '-> Construction d'un masque assez grand pour formatter n'importe qu'elle zone
        strTempo = VB6.Format(System.Math.Abs(CDbl(strTempo)), Masque)

        '-> Construction de la partie enti�re
        If nbDec <> 0 Then
            strPartieEntiere = Mid(strTempo, 1, InStr(1, strTempo, SepDec) - 1)
            StrPartieDecimale = SepDec & Mid(strTempo, InStr(strTempo, SepDec) + 1, nbDec)
        Else
            strPartieEntiere = strTempo
            StrPartieDecimale = ""
        End If

        j = 1
        For i = Len(strPartieEntiere) To 1 Step -1
            strFormat = Mid(strTempo, i, 1) & strFormat
            If j = 3 And i <> 1 Then
                strFormat = strSep & strFormat
                j = 1
            Else
                j = j + 1
            End If
        Next

        FormatNumTP.strFormatted = strFormat & StrPartieDecimale

        Exit Function

GestErr:
        FormatNumTP.Ok = False
    End Function

    Private Function PrintTableau(ByRef NomObjet As String, ByRef Param As String, ByRef DataFields As String, ByRef PositionX As Integer, ByRef PositionY As Integer, ByRef NomSousObjet As String, ByRef FirstObj As Boolean, ByRef aspool As Spool) As Boolean

        Dim aTb As Tableau
        Dim nLig As Short

        'On Error GoTo GestError
        '-> Pointer sur le tableau pass� en argument
        aTb = aspool.Maquette_Renamed.Tableaux.Item(UCase(NomObjet))

        '-> R�cup�ration de la ligne de tableau � imprimer
        nLig = CShort(Entry(2, Param, "\"))

        '-> Imprimer le block de ligne
        PositionY = PrintBlock(NomSousObjet, Param, DataFields, PositionX, PositionY, aTb, nLig, aspool)

        '-> Quitter si valeur d'erreur
        If PositionY = -9999 Then GoTo GestError

        '-> Renvoyer une valeur de succ�s
        PrintTableau = True

        '-> on vide les variables
        aTb = Nothing
        Exit Function

GestError:
        Call GestError(37, aTb.Nom, aspool)
        PrintTableau = False


    End Function

    Public Function GetPictureAssociation(ByRef ValueToSearch As String) As String

        Dim Res As Integer
        Dim lpBuffer As String

        On Error GoTo GestError
        GetPictureAssociation = ""

        '-> Ne traiter l'association d'image qu'en version PathV51
        'If VersionTurbo = 1 Then Exit Function
        '-> V�rifier que l'on ait bien acc�s au fichier TM_Picture.ini
        If Tm_PictureIniFile = "" Then Exit Function
        Trace("Tm_PictureIniFile=" + Tm_PictureIniFile)
        Trace("ValueToSearch=" + ValueToSearch)
        lpBuffer = Space(5000)
        Res = GetPrivateProfileString("IMAGES", ValueToSearch, "", lpBuffer, Len(lpBuffer), Tm_PictureIniFile)
        If Res <> 0 Then
            lpBuffer = Mid(lpBuffer, 1, Res)
            '-> Cr�er la liste des mots cl�s
            GetPathMotCles()
            '-> Faire le remplacement
            ReplacePathMotCles(lpBuffer)
            GetPictureAssociation = lpBuffer
        End If

        Exit Function

GestError:
        GetPictureAssociation = ""

    End Function

    Public Function CreateDetail(ByRef aspool As Spool, ByVal Page As Short, Optional ByRef PageMin As Short = 0, Optional ByRef PageMax As Short = 0) As String

        '---> Cette fonction cr�er un fichier tempo et retourne le nom du fichier � linker

        Dim TempFile As String
        Dim hdlFile As Short
        Dim DefMaq As String
        Dim i As Short
        Dim PageMini As Short
        Dim PageMaxi As Short

        On Error GoTo GestError

        '-> Obtenir un nom de fichier tempotaire
        'TempFile = Year(Now) & "-" & VB6.Format(Month(Now), "00") & "-" & VB6.Format(Day(Now), "00") & "-" & VB6.Format(Hour(Now), "00") & "-" & VB6.Format(Minute(Now), "00") & "-" & VB6.Format(Second(Now), "00") & ".turbo"
        TempFile = Year(Now) & "-" & VB6.Format(Month(Now), "00") & "-" & "-" & VB6.Format(Hour(Now), "00") & "-" & VB6.Format(Minute(Now), "00") & "-" & VB6.Format(Second(Now), "00") & ".turbo"
        TempFile = GetTempFileNameVB("WWW", True) & TempFile

        '-> Obtenir un handle de fichier
        hdlFile = FreeFile()

        '-> Ouverture du fichier ascii et �criture des pages
        FileOpen(hdlFile, TempFile, OpenMode.Output)

        '-> Ecriture de la balise Spool
        If IsCryptedFile Then
            PrintLine(hdlFile, Crypt("[SPOOL]"))
        Else
            PrintLine(hdlFile, "[SPOOL]")
        End If

        '-> Il faut dans un premier temps �crire la maquette
        If IsCryptedFile Then
            PrintLine(hdlFile, Crypt("[MAQ]"))
        Else
            PrintLine(hdlFile, "[MAQ]")
        End If

        '-> R�cup�ration de la maquette
        DefMaq = aspool.GetMaq

        '-> Impression de la maquette
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(DefMaq, Chr(0)). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        For i = 1 To NumEntries(DefMaq, Chr(0))
            If IsCryptedFile Then
                PrintLine(hdlFile, Crypt(Entry(i, DefMaq, Chr(0))))
            Else
                PrintLine(hdlFile, Entry(i, DefMaq, Chr(0)))
            End If
        Next  'Pour toutes les lignes de la maquette

        '-> Tag de fin de maquette
        If IsCryptedFile Then
            PrintLine(hdlFile, Crypt("[/MAQ]"))
        Else
            PrintLine(hdlFile, "[/MAQ]")
        End If

        '-> Impression du spool en entier
        If Page = -1 Or Page = -2 Then
            If Page = -1 Then
                '-> Indiquer la premi�re page � imprimer
                If aspool.IsSelectionPage = True Then
                    '-> indiquer qu'il faut impimer � partir de la page 0
                    PageMini = 0
                Else
                    '-> Imprimer � partir de la page 1
                    PageMini = 1
                End If

                '-> Indiquer la page maxi
                PageMaxi = aspool.NbPage

            Else
                '-> Indiquer la premi�re page � imprimer
                PageMini = PageMin
                PageMaxi = PageMax

            End If
            '-> On imprimer toutes les pages
            For i = PageMini To PageMaxi
                '-> Imprimer la tag d'ouverture de la page de s�lection
                If i = 0 Then
                    If IsCryptedFile Then
                        PrintLine(hdlFile, Crypt("[GARDEOPEN]"))
                    Else
                        PrintLine(hdlFile, "[GARDEOPEN]")
                    End If
                End If

                '-> Impression de la page
                PrintPageToSpool(aspool, hdlFile, i)

                '-> Imprimer le tag de fin de s�lection si page = 0
                If i = 0 Then
                    If IsCryptedFile Then
                        PrintLine(hdlFile, Crypt("[GARDECLOSE]"))
                    Else
                        PrintLine(hdlFile, "[GARDECLOSE]")
                    End If
                    '-> Imprimer un saut de page si pas derni�re page
                ElseIf i <> aspool.NbPage Then
                    If IsCryptedFile Then
                        PrintLine(hdlFile, Crypt("[PAGE]"))
                    Else
                        PrintLine(hdlFile, "[PAGE]")
                    End If
                End If
            Next
        Else
            '-> Imprimer la tag de la page de s�lection
            If Page = 0 Then
                If IsCryptedFile Then
                    PrintLine(hdlFile, Crypt("[GARDEOPEN]"))
                Else
                    PrintLine(hdlFile, "[GARDEOPEN]")
                End If
            End If

            '-> Imprimer que la page d�sir�e
            PrintPageToSpool(aspool, hdlFile, Page)

            '-> Imprimer la tag de fermture de la page de s�lection
            If Page = 0 Then
                If IsCryptedFile Then
                    PrintLine(hdlFile, Crypt("[GARDECLOSE]"))
                Else
                    PrintLine(hdlFile, "[GARDECLOSE]")
                End If
            End If

        End If

        '-> Tag de fin de spool
        If IsCryptedFile Then
            PrintLine(hdlFile, Crypt("[/SPOOL]"))
        Else
            PrintLine(hdlFile, "[/SPOOL]")
        End If

        '-> Fermer le fichier ouvert
        FileClose(hdlFile)

        '-> Renvoyer le nom du fichier
        CreateDetail = TempFile

        Exit Function

GestError:

        '-> Renovyer une valeur ""
        CreateDetail = ""

    End Function

    Private Sub PrintPageToSpool(ByRef aspool As Spool, ByVal hdlFile As Short, ByVal PageToPrint As Short)

        '---> Cette fonction imprime le contenu d'une page d'un spool

        Dim i As Short

        Dim DefPage As String

        '-> Recup�ration de la d�finition de la page
        DefPage = aspool.GetPage(PageToPrint)

        '-> Impression de la page
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(DefPage, Chr(0)). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        For i = 1 To NumEntries(DefPage, Chr(0))
            If IsCryptedFile Then
                PrintLine(hdlFile, Crypt(Entry(i, DefPage, Chr(0))))
            Else
                PrintLine(hdlFile, Entry(i, DefPage, Chr(0)))
            End If
        Next

    End Sub

    Private Function ChangerColor(ByVal Couleur As System.Drawing.Color, Optional ByRef Palette As Integer = 0) As Integer
        If OleTranslateColor(System.Drawing.ColorTranslator.ToOle(Couleur), Palette, ChangerColor) Then
            ChangerColor = -1
        End If
    End Function

    Public Function GetTitrePage(ByRef aspool As Spool, ByRef NumPage As Short) As String
        '--> Cette fonction r�cup�re le titre d'une page
        Dim ParamTitre As String
        Dim Ligne As String
        Dim Rg As String
        Dim Dz As String
        Dim i As Short
        Dim j As Short

        GetTitrePage = ""
        '-> on regarde si on a specifi� un titre
        If InStr(1, aspool.GetMaq, "[TITRE]") = 0 Then Exit Function

        '-> on r�cup�re le param�trage
        ParamTitre = Entry(2, aspool.GetMaq, "[TITRE]")
        ParamTitre = Entry(1, ParamTitre, "\TITRE�End")
        ParamTitre = Mid(ParamTitre, 8)

        '-> on parcours les titres � r�cuperer
        For i = 2 To NumEntries(ParamTitre, "\")
            Rg = Entry(i, ParamTitre, "\")
            Dz = "^" & Entry(1, Entry(2, Rg, "�"), Chr(0))
            Rg = "[" & Entry(1, Rg, "�") & "]"
            '-> on recherche la ligne correspondante
            For j = 1 To NumEntries(aspool.GetPage(NumPage), Chr(0))
                Ligne = Trim(Entry(j, aspool.GetPage(NumPage), Chr(0)))
                If InStr(1, UCase(Ligne), UCase(Rg)) Then
                    '-> on regarde si on trouve le diez
                    If InStr(1, UCase(Ligne), UCase(Rg)) <> 0 And InStr(1, Ligne, Dz) Then
                        '-> on est sur une bonne ligne on r�cup�re la valeur du diez
                        Dz = RTrim(Mid(Entry(1, Entry(1, Entry(2, Ligne, Dz), "^"), "}"), 5))
                        If InStr(1, Dz, "[\") <> 0 Then Dz = ""
                        If GetTitrePage = "" Then
                            GetTitrePage = Dz
                        Else
                            GetTitrePage = GetTitrePage & " - " & Dz
                        End If
                        '-> sortir de la boucle
                        Exit For
                    End If
                End If
            Next
        Next

    End Function

    Public Function getOSVer() As WindowsVersion
        Dim osv As OSVERSIONINFOEX
        osv.dwOSVersionInfoSize = Marshal.SizeOf(osv)
        If GetVersionEx(osv) = 1 Then
            Select Case osv.dwPlatformId
                Case Is = VER_PLATFORM_WIN32s 'windows 3.x
                    Return WindowsVersion.Obsolete_OS
                Case Is = VER_PLATFORM_WIN32_WINDOWS
                    Select Case osv.dwMinorVersion
                        Case Is = 0 'win 95
                            Return WindowsVersion.Obsolete_OS
                        Case Is = 10
                            If InStr(UCase(osv.szCSDVersion), "A") > 0 Then
                                Return WindowsVersion.Windows_98_SE
                            Else
                                Return WindowsVersion.Windows_98
                            End If
                        Case Is = 90
                            Return WindowsVersion.Windows_Me
                    End Select
                Case Is = VER_PLATFORM_WIN32_NT
                    Select Case osv.dwMajorVersion
                        Case Is = 3 'win nt 3.x
                            Return WindowsVersion.Obsolete_OS
                        Case Is = 4
                            If osv.wProductType = VER_NT_WORKSTATION Then
                                Return WindowsVersion.Windows_NT4_Workstation
                            Else
                                Return WindowsVersion.Windows_NT4_Server
                            End If
                        Case Is = 5
                            Select Case osv.dwMinorVersion
                                Case Is = 0 'win 2000
                                    Select Case osv.wProductType
                                        Case Is = VER_NT_WORKSTATION
                                            Return WindowsVersion.Windows_2000_Pro
                                        Case Is = VER_NT_SERVER
                                            Return WindowsVersion.Windows_2000_Server
                                    End Select
                                Case Is = 1 'win XP
                                    If osv.wSuiteMask = VER_SUITE_PERSONAL Or osv.wSuiteMask = VER_SUITE_PERSONAL + VER_SUITE_SINGLEUSERTS Then
                                        Return WindowsVersion.Windows_XP_HomeEdition
                                    Else
                                        Return WindowsVersion.Windows_XP_Pro
                                    End If
                                Case Is = 2 '.Net server
                                    Return WindowsVersion.Windows_Net_Server
                            End Select
                        Case Is = 6
                            Return WindowsVersion.Windows_Vista
                        Case Is = 7
                            Return WindowsVersion.Windows_Seven
                    End Select
            End Select
        End If
    End Function
    Public Function RegValue(ByVal Hive As RegistryHive, ByVal Key As String, ByVal ValueName As String, Optional ByRef ErrInfo As String = "") As String
        Dim objParent As RegistryKey
        Dim objSubkey As RegistryKey
        Dim sAns As String
        Select Case Hive
            Case RegistryHive.ClassesRoot
                objParent = Registry.ClassesRoot
            Case RegistryHive.CurrentConfig
                objParent = Registry.CurrentConfig
            Case RegistryHive.CurrentUser
                objParent = Registry.CurrentUser
            Case RegistryHive.DynData
                objParent = Registry.DynData
            Case RegistryHive.LocalMachine
                objParent = Registry.LocalMachine
            Case RegistryHive.PerformanceData
                objParent = Registry.PerformanceData
            Case RegistryHive.Users
                objParent = Registry.Users

        End Select

        Try
            objSubkey = objParent.OpenSubKey(Key)
            'if can't be found, object is not initialized
            If Not objSubkey Is Nothing Then
                sAns = (objSubkey.GetValue(ValueName))
            End If

        Catch ex As Exception

            ErrInfo = ex.Message
        Finally

            'if no error but value is empty, populate errinfo
            If ErrInfo = "" And sAns = "" Then
                ErrInfo = _
                   "No value found for requested registry key"
            End If
        End Try
        Return sAns
    End Function

    Public Function RegValueSearch(ByVal Hive As RegistryHive, ByVal Key As String, ByVal ValueName As String, Optional ByRef ErrInfo As String = "") As String
        'cette fonction recherche une cl� approximative (contenant)
        Dim objParent As RegistryKey
        Dim objSubkey As RegistryKey
        Dim sAns As String
        Dim vals() As String
        Dim tmpstr As String
        Dim tmpkey As String

        Select Case Hive
            Case RegistryHive.ClassesRoot
                objParent = Registry.ClassesRoot
            Case RegistryHive.CurrentConfig
                objParent = Registry.CurrentConfig
            Case RegistryHive.CurrentUser
                objParent = Registry.CurrentUser
            Case RegistryHive.DynData
                objParent = Registry.DynData
            Case RegistryHive.LocalMachine
                objParent = Registry.LocalMachine
            Case RegistryHive.PerformanceData
                objParent = Registry.PerformanceData
            Case RegistryHive.Users
                objParent = Registry.Users
        End Select
        objSubkey = objParent.OpenSubKey(Key)
        'on recupere la liste des valeurs
        vals = objSubkey.GetValueNames
        For Each tmpstr In vals
            If InStr(tmpstr, ValueName, CompareMethod.Text) <> 0 Then
                sAns = objSubkey.GetValue(tmpstr) 'REcup�ration en tant qu'objet
                Return sAns
            End If
        Next

        Return sAns
    End Function

    Public Function GetFontFileName(ByVal FontName As String) As String
        '-> Cette fonction permet de pointer sur le fichier de la font
        Dim sAns As String
        Dim sErr As String = ""
        Dim sPath As String
        Dim mykey As String
        Dim mykey2 As String
        Dim key As RegistryKey
        If FontName = fontLastName Then
            Return fontLastDir
        End If
        'If FontName = "Microsoft Sans Serif" Then FontName = "Microsoft Sans Serif Regular"
        'If FontName = "Comic Sans MS" Then FontName = "Comic Sans MS Normal"
        If UCase(FontName) = UCase("Courier New") Then
            GetFontFileName = ""
            Exit Function
        End If
        If (getOSVer() > 4) Then
            mykey = "SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts"
            mykey2 = "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders"
        Else
            mykey = "SOFTWARE\Microsoft\Windows\CurrentVersion\Fonts"
            mykey2 = "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\shell Folders"
        End If
        sPath = RegValueSearch(Microsoft.Win32.RegistryHive.CurrentUser, mykey2, "Fonts", sErr) & "\"
        sAns = RegValueSearch(Microsoft.Win32.RegistryHive.LocalMachine, mykey, FontName, sErr)
        If sPath = "\" Then
            sPath = Environment.GetEnvironmentVariable("WINDIR") & "\fonts\"
        End If
        If sAns <> "" Then
            GetFontFileName = sPath & sAns
        Else
            sAns = RegValueSearch(Microsoft.Win32.RegistryHive.LocalMachine, mykey, FontName, sErr)
            If sAns <> "" Then
                GetFontFileName = sPath & sAns
            Else
                sAns = RegValueSearch(Microsoft.Win32.RegistryHive.LocalMachine, mykey, FontName, sErr)
                If sAns <> "" Then
                    GetFontFileName = sPath & sAns
                Else
                    GetFontFileName = ""
                End If
            End If
        End If
        fontLastName = FontName
        fontLastDir = GetFontFileName
        Trace("getfontname :" + FontName + "->" + GetFontFileName)
GestError:
    End Function

    Private Function ExtractArchive(ByVal zipFilename As String, ByVal ExtractDir As String) As String
        Dim Redo As Integer = 1
        Dim MyZipInputStream As ZipInputStream
        Dim MyFileStream As FileStream
        dim fileJoinCount as Integer  = 0
        MyZipInputStream = New ZipInputStream(New FileStream(zipFilename, FileMode.Open, FileAccess.Read))
        Try
            Dim MyZipEntry As ZipEntry = MyZipInputStream.GetNextEntry
            Directory.CreateDirectory(ExtractDir)
            While Not MyZipEntry Is Nothing
                If (MyZipEntry.IsDirectory) Then
                    Directory.CreateDirectory(ExtractDir & "\" & MyZipEntry.Name)
                Else
                    If Not Directory.Exists(ExtractDir & "\" & Path.GetDirectoryName(MyZipEntry.Name)) Then
                        Directory.CreateDirectory(ExtractDir & "\" & Path.GetDirectoryName(MyZipEntry.Name))
                    End If
                    MyFileStream = New FileStream(ExtractDir & "\" & MyZipEntry.Name, FileMode.OpenOrCreate, FileAccess.Write)
                    Dim count As Integer
                    Dim buffer(4096) As Byte
                    count = MyZipInputStream.Read(buffer, 0, 4096)
                    While count > 0
                        MyFileStream.Write(buffer, 0, count)
                        count = MyZipInputStream.Read(buffer, 0, 4096)
                    End While
                    MyFileStream.Close()
                End If
                '-> on sauvegarde les donn�es du fichier
                If UCase(Mid(MyZipEntry.Name, Len(MyZipEntry.Name) - 4)) = "TURBO" Then
                    ExtractArchive = ExtractDir & MyZipEntry.Name
                Else
                    '--> on essaye d'affecter le turbo
                    fileJoinCount += 1
                    arrFILEJOIN = DirectCast(Utils.CopyArray(DirectCast(arrFILEJOIN, Array), New String((fileJoinCount + 1) - 1) {}), String())
                    arrFILEJOIN(fileJoinCount) = ExtractDir & MyZipEntry.Name
                End If
                If ExtractArchive = "" Then ExtractArchive = ExtractDir & MyZipEntry.Name
                Try
                    MyZipEntry = MyZipInputStream.GetNextEntry
                Catch ex As Exception
                    MyZipEntry = Nothing
                End Try
            End While
        Catch ex As Exception
        End Try
        If Not (MyZipInputStream Is Nothing) Then MyZipInputStream.Close()
        If Not (MyFileStream Is Nothing) Then MyFileStream.Close()
        If ExtractArchive = "" Then ExtractArchive = zipFilename
    End Function

    Public Sub Trace(ByVal sText As String)
        '--> cette procedure permet d'alimenter le fichier de debug
        '-> si on est en mode trace
        On Error Resume Next
        IsMouchard = True
        If IsMouchard Then
            'si le fichier n'est pas ouvert l'ouvrir
            If hdlMouchard = 0 Then
                'If hdlMouchard = 0 And Not PdfFileName Is Nothing Then
                hdlMouchard = FreeFile()
                'Mouchard = GetTempFileNameVB("TXT")
                'If My.Computer.FileSystem.FileExists(Mouchard) Then
                'If FileLen(Mouchard) > 10000 Then Kill(Mouchard)
                'End If
                'FileOpen(hdlMouchard, My.Computer.FileSystem.GetFileInfo(PdfFileName).DirectoryName & "/trace.txt", OpenMode.Append)
                FileOpen(hdlMouchard, "C:/trace.txt", OpenMode.Append)
                '-> on initialise le mode trace
                'Print(hdlMouchard, My.Computer.FileSystem.GetFileInfo(PdfFileName).DirectoryName & "/trace.txt")
                Print(hdlMouchard, "C:/trace.txt")
                Print(hdlMouchard, "***************************")
                Print(hdlMouchard, "Trace du " & Format(Now, "dd/mm/yyyy � hh:mm:ss") & Chr(13) & Chr(10))
                'MsgBox(My.Computer.FileSystem.GetFileInfo(PdfFileName).DirectoryName & "/trace.txt")
            End If
            '-> ecrire la ligne
            If hdlMouchard <> 0 Then Print(hdlMouchard, Now.Second & Now.Millisecond & " -> " & sText & Chr(13) & Chr(10))
        End If

    End Sub

    Private Sub loadNote(ByVal strFichier As String)
        '--> cette fonction va permettre de charger la collection notes d'un fichier
        Dim aFichier As Fichier
        Dim aSpool As Spool
        Dim DSO, DSOprop
        Dim i As Integer
        On Error Resume Next

        DSO = CreateObject("DSOFile.OleDocumentProperties")
        DSO.Open(strFichier)

        '-> on pointe sur les propri�t�s
        DSOprop = DSO.SummaryProperties
        If DSOprop.Comments <> "" Then
            For i = 1 To NumEntries(DSOprop.Comments, "�")
                '-> on charge la note
                noteAdd(strFichier & Entry(i, DSOprop.Comments, "�"))
            Next
        End If
        DSO.close()
        DSOprop = Nothing
        DSO = Nothing

    End Sub

    Public Function noteAdd(ByVal strNote As String) As Note
        '--> cette fonction permet d'ajouter une note dans la collection et la retourne
        '--> strNote du type nomdufichier|numero du spool|asppol.currentpage|top|left|width|height|couleur|transparence|texteentete|texteligne
        Dim sNote() As String
        Dim fNote As Note
        '-> gestion des erreurs
        On Error Resume Next

        sNote = Split(strNote, "|")
        '-> on initialise si besoin la collection
        If fNotes Is Nothing Then
            fNotes = New Collection
        End If
        '-> on initialise une nouvelle feuille
        fNote = New Note
        fNotes.Add(fNote)
        fNote.Tag = sNote(0) & "|" & sNote(1) & "|" & sNote(2)

        If sNote(3) <> "" Then fNote.Top = Val(sNote(3))
        If sNote(4) <> "" Then fNote.Left = Val(sNote(4))
        If sNote(5) <> "" Then fNote.Width = Val(sNote(5))
        If sNote(6) <> "" Then fNote.Height = Val(sNote(6))
        If sNote(7) <> "" Then
            fNote.backColor = sNote(7)
        End If
        If sNote(8) <> "" Then
            fNote.valTransparence = sNote(8)
        End If
        If sNote(9) <> "" Then fNote.Text1 = sNote(9)
        If sNote(10) <> "" Then fNote.Text2 = sNote(10)

        '-> on retourne la feuille
        noteAdd = fNote
    End Function

End Module

