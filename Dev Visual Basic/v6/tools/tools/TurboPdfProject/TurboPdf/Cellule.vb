Option Strict Off
Option Explicit On
Public Class Cellule

    '**********************************************
    '*                                            *
    '* Cette classe contient la d�finition d'un   *
    '* Objet Cellule                              *
    '*                                            *
    '**********************************************

    '-> Variables de position
    Public X1 As Short
    Public Y1 As Short
    Public X2 As Short
    Public Y2 As Short

    '-> Coordonn�es en Ligne/Colonne
    Public Ligne As Short
    Public Colonne As Short

    '-> Couleur de fond de la cellule
    Public BackColor As Integer

    '-> Bordures stock�es en type priv� pour utilisation des Property
    Public BordureBas As Boolean
    Public BordureHaut As Boolean
    Public BordureGauche As Boolean
    Public BordureDroite As Boolean

    '-> Propri�t�s de Font
    Public FontName As String
    Public FontSize As Short
    Public FontColor As Integer
    Public FontBold As Boolean
    Public FontItalic As Boolean
    Public FontUnderline As Boolean

    Public FondTransparent As Boolean

    '-> Alignement interne : Valeurs de 1 � 9
    Public CellAlign As Short

    '-> Contenu de la cellule
    Public Contenu As String
    Public ContenuCell As String

    '-> Retour � la ligne automatique du contenu
    Public AutoAjust As Boolean

    '-> Format li� � la cellule
    Public TypeValeur As Short 'Valeur des diff�rentes formats
    'Format � appliquer avec : _
    '0 -> Pas de format _
    '1 -> Num�rique _
    '2 -> Caractere
    Public Msk As String
    ' -> pour msk si le format est numrique :
    ' 3 entr�es s�par�es par des �
    ' 1 : Nombre de d�cimal
    ' 2 : S�parateur de milier
    ' 3 : Signe n�gatif : Index de 0 � 4 ( voir fichier MessProg [frmFormatCell]
    ' -> pour msk si le format est caract�re  :
    ' 1 entr�e qui contient le nombre de caract�re � garder

    '-> Dans le cas des exports Excel
    Public IsFusion As Boolean
    Public ColFusion As Short

    '-> Pour gestion des champs en interne
    Private ListeField() As String
    Public nbField As Short
    '-> Indique si la zone est num�rique si on doit imprimer un 0
    Public PrintZero As Boolean

    '-> Pour gestion des export
    Public aRange As clsRange

    '-> Liste des cellules dans la feulle active concern�es par ce format
    Public ListeCell As String

    '---> Autoriser l'acc�s au d�tail depuis cette cellule
    Public UseAccesDet As Boolean
    Public KeyAccesDet As String

    '---> Creation de lien hypertext
    Public UseHyperText As Boolean
    Public KeyLink As String

    '---> Creation de zone modifiable
    Public UseFormText As Boolean
    Public KeyForm As String
    Public KeyFormIndex As Integer
    Public Image As String
    Public ImageHeight As String
    Public ImageWidth As String
    Public ImageIndex As Integer

    Public Function GetBordureByIndex(ByVal Index As Short) As Object

        Dim Bas As Boolean
        Dim Haut As Boolean
        Dim Gauche As Boolean
        Dim Droite As Boolean

        GetBordureByIndex = False
        Select Case Index

            Case 0

                Bas = False
                Haut = False
                Gauche = False
                Droite = False

            Case 1

                Bas = True
                Haut = True
                Gauche = True
                Droite = False

            Case 2

                Bas = True
                Haut = True
                Gauche = False
                Droite = True

            Case 3

                Bas = False
                Haut = True
                Gauche = True
                Droite = True

            Case 4

                Bas = True
                Haut = False
                Gauche = True
                Droite = True
            Case 5

                Bas = False
                Haut = False
                Gauche = True
                Droite = False

            Case 6

                Bas = False
                Haut = True
                Gauche = False
                Droite = False

            Case 7

                Bas = False
                Haut = False
                Gauche = False
                Droite = True

            Case 8

                Bas = True
                Haut = False
                Gauche = False
                Droite = False

            Case 9

                Bas = False
                Haut = False
                Gauche = True
                Droite = True

            Case 10

                Bas = True
                Haut = True
                Gauche = False
                Droite = False

            Case 11

                Bas = False
                Haut = True
                Gauche = True
                Droite = False

            Case 12

                Bas = False
                Haut = True
                Gauche = False
                Droite = True

            Case 13

                Bas = True
                Haut = False
                Gauche = True
                Droite = False

            Case 14

                Bas = True
                Haut = False
                Gauche = False
                Droite = True

            Case 15

                Bas = True
                Haut = True
                Gauche = True
                Droite = True

        End Select

        BordureBas = Bas
        BordureDroite = Droite
        BordureGauche = Gauche
        BordureHaut = Haut

    End Function


    Public Function InitChamp() As Object

        Dim i As Short
        Dim pos As Short

        pos = 1

        Do
            i = InStr(pos, Contenu, "^")
            If i = 0 Then Exit Do
            nbField = nbField + 1
            'UPGRADE_WARNING: La limite inf�rieure du tableau ListeField est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
            ReDim Preserve ListeField(nbField)
            ListeField(nbField) = Mid(Contenu, i, 5)
            pos = i + 1
        Loop
        InitChamp = True
    End Function


    Public Function GetChamp(ByVal nIndex As Short) As String


        GetChamp = ListeField(nIndex)

    End Function

    Public Sub ReplaceField(ByRef DataListe As String)

        '---> Remplacer les champs par leur valeur
        Dim ChampValue As String = ""
        Dim i As Short
        Dim j As Short
        Dim k As Short

        ContenuCell = Contenu
        If Trim(DataListe) = "" Then Exit Sub

        '-> Gestion de l'acc�s direct
        i = InStr(1, DataListe, "^ACDET")
        If i <> 0 Then
            Me.KeyAccesDet = Trim(Mid(DataListe, i + 6))
        Else
            Me.KeyAccesDet = ""
        End If

        For i = 1 To nbField
            j = InStr(1, DataListe, ListeField(i))
            If j <> 0 Then
                k = InStr(j + 1, DataListe, "^")
                If k = 0 Then k = Len(DataListe)
                '-> on regarde si ^ n'est pas une valeur si c'est une valeur il est suivi de //
                If k <> 0 Then
                    Do While Mid(DataListe, k, 3) = "^//"
                        k = InStr(k + 1, DataListe, "^")
                        If k = 0 Then
                            k = Len(DataListe)
                            Exit Do
                        End If
                    Loop
                End If
                ChampValue = RTrim(Mid(DataListe, j + 5, k - (j + 5)))
            End If
            '-> Remplacer la valuer
            ContenuCell = Replace(Replace(ContenuCell, ListeField(i), ChampValue), "^//", "^")
        Next

    End Sub

    'UPGRADE_NOTE: Class_Initializea �t� mis � niveau vers Class_Initialize_Renamed. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        '-> Cas des exports vers Excel
        Me.ColFusion = 1
        Me.IsFusion = False
    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub


    Public Sub TurboToExcel()

        '---> Appliquer le format � l'objet range en cours

        'On Error Resume Next

        '-> Initialiser l'objet Range attach� � cette cellule
        aRange = New clsRange

        '-> Gestion des bordures


        '-> Bordure du haut
        If BordureHaut Then
            aRange.SetBorderLineStyle(8, 1)
            aRange.SetBorderColorIndex(8, -4105)
        End If

        '-> Bordure du bas
        If BordureBas Then
            aRange.SetBorderLineStyle(9, 1)
            aRange.SetBorderColorIndex(9, -4105)
        End If

        '-> Bordure gauche
        If BordureGauche Then
            aRange.SetBorderLineStyle(7, 1)
            aRange.SetBorderColorIndex(7, -4105)
        End If

        '-> Bordure Droite
        If BordureDroite Then
            aRange.SetBorderLineStyle(10, 1)
            aRange.SetBorderColorIndex(10, -4105)
        End If

        '-> Font
        aRange.FontName = FontName

        '-> Taille de la police
        aRange.FontSize = FontSize

        '-> Gras
        aRange.FontBold = FontBold

        '-> Italic
        aRange.FontItalic = FontItalic

        '-> Soulign�
        aRange.FontUnderline = FontUnderline

        '-> Couleur de fond
        aRange.BackColor = BackColor

        '-> Alignement interne de la cellule
        Select Case CellAlign
            Case 1
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.VerticalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.VerticalAlignment = -4160
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.HorizontalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.HorizontalAlignment = -4131
            Case 2
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.VerticalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.VerticalAlignment = -4160
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.HorizontalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.HorizontalAlignment = -4108
            Case 3
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.VerticalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.VerticalAlignment = -4160
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.HorizontalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.HorizontalAlignment = -4152
            Case 4
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.VerticalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.VerticalAlignment = -4108
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.HorizontalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.HorizontalAlignment = -4131
            Case 5
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.VerticalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.VerticalAlignment = -4108
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.HorizontalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.HorizontalAlignment = -4108
            Case 6
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.VerticalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.VerticalAlignment = -4108
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.HorizontalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.HorizontalAlignment = -4152
            Case 7
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.VerticalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.VerticalAlignment = -4107
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.HorizontalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.HorizontalAlignment = -4131
            Case 8
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.VerticalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.VerticalAlignment = -4107
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.HorizontalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.HorizontalAlignment = -4108
            Case 9
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.VerticalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.VerticalAlignment = -4107
                'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet aRange.HorizontalAlignment. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aRange.HorizontalAlignment = -4152
        End Select

        '-> Retour � la ligne automatique
        aRange.WrapText = AutoAjust

    End Sub
End Class