Option Strict Off
Option Explicit On
Public Class frmDisplaySpool
    Inherits System.Windows.Forms.Form
    '-> Indique le Spool associ�
    Public aSpool As Spool

    '-> Pour positionnement de la page
    Public LargeurX As Single
    Public HauteurY As Single
    Public MgX As Single
    Public MgY As Single
    Dim DepartX As Single
    Dim DepartY As Single


    Private Sub frmDisplaySpool_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load


        Dim aLb As Libelle
        Dim i As Short
        Dim PrinterFound As Boolean
        'UPGRADE_ISSUE: Printer L'objet - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
        Dim Printer As printer

        On Error Resume Next

        '-> Pointer sur la classe Libelle
        aLb = Libelles.Item("FRMVISUSPOOL")



    End Sub

    Private Sub frmDisplaySpool_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        'Dim frmDisplay As Object

        Dim i As Short

        '-> Supprimer l'affectation de la feuille dans le spool
        aSpool.frmDisplay = Nothing

        If aSpool.NbError = 0 Then
            '-> Suppirmer l'icone de s�lection
            For i = 1 To aSpool.NbPage
                '-> Test attention aux pages avec des erreurs rechercher la mise � jour des icnoes
                MDIMain.TreeNaviga.Nodes.Item(UCase(Trim(aSpool.FileName)) & "�" & UCase(Trim(aSpool.Key)) & "�PAGE|" & i).ImageKey = "Page"
            Next
        End If

        aSpool.CurrentPage = 0

    End Sub

    Private Sub HScroll1_Change(ByVal newScrollValue As Integer)
        'Me.Page.Left = newScrollValue
    End Sub

    Private Sub HScroll1_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.ScrollEventArgs)
        Select Case eventArgs.Type
            Case System.Windows.Forms.ScrollEventType.EndScroll
                HScroll1_Change(eventArgs.NewValue)
        End Select
    End Sub

End Class