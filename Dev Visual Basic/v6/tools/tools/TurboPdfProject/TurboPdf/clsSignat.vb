Imports System
Imports System.Security.Cryptography
Imports System.Security.Cryptography.PKCS
Imports System.Security.Cryptography.X509Certificates
Imports System.Text

Class EnvelopedSignedCms
    Const signerName As String = "MessageSigner1"
    Const recipientName As String = "Recipient1"

    Shared Sub Main1(ByVal args As String)
        Dim origMsg As Byte()
        Console.WriteLine("System.Security.Cryptography.Pkcs " + "Sample: Encrypted, signed, decrypted, and " + "verified message")
        Const msg As String = "Here are the sales figures for the " + "upcoming quarterly report to Wall Street."
        Console.WriteLine("" & Microsoft.VisualBasic.Chr(10) & "Original message (len {0}): {1} ", msg.Length, msg)
        Dim unicode As Encoding = Encoding.Unicode
        Dim msgBytes As Byte() = unicode.GetBytes(msg)
        Console.WriteLine("" & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "------------------------------")
        Console.WriteLine(" SETUP OF CREDENTIALS ")
        Console.WriteLine("------------------------------" & Microsoft.VisualBasic.Chr(10) & "")
        Dim signerCert As X509Certificate2 = GetSignerCert()
        Dim recipientCert As X509Certificate2 = GetRecipientCert()
        Console.WriteLine("" & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "----------------------")
        Console.WriteLine(" SENDER SIDE ")
        Console.WriteLine("----------------------" & Microsoft.VisualBasic.Chr(10) & "")
        Dim encodedSignedCms As Byte() = SignMsg(msgBytes, signerCert)
        Dim encodedEnvelopedCms As Byte() = EncryptMsg(encodedSignedCms, recipientCert)
        Console.Write("" & Microsoft.VisualBasic.Chr(10) & "Message after encryption (len {0}): ", encodedEnvelopedCms.Length)
        For Each b As Byte In encodedEnvelopedCms
            Console.Write("{0:x}", b)
        Next
        Console.WriteLine()
        Console.WriteLine("" & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "------------------------")
        Console.WriteLine(" RECIPIENT SIDE ")
        Console.WriteLine("------------------------" & Microsoft.VisualBasic.Chr(10) & "")
        encodedSignedCms = DecryptMsg(encodedEnvelopedCms)
        If VerifyMsg(encodedSignedCms, origMsg) Then
            Console.WriteLine("" & Microsoft.VisualBasic.Chr(10) & "Message verified")
        Else
            Console.WriteLine("" & Microsoft.VisualBasic.Chr(10) & "Message failed to verify")
        End If
        Console.WriteLine("" & Microsoft.VisualBasic.Chr(10) & "Decrypted Authenticated Message: {0}", unicode.GetString(origMsg))
    End Sub

    Public Shared Function GetSignerCert() As X509Certificate2
        GetSignerCert = New X509Certificate2("D:\TurboPdfProject\TurboPdf\TurboPDF.pfx", "deal")
        Return GetSignerCert
        Dim storeMy As X509Store = New X509Store(StoreName.My, StoreLocation.CurrentUser)
        storeMy.Open(OpenFlags.ReadOnly)
        For Each cert As X509Certificate2 In storeMy.Certificates
            Console.WriteLine("" & Microsoft.VisualBasic.Chr(9) & "{0}", cert.SubjectName.Name)
        Next
        Dim certColl As X509Certificate2Collection = storeMy.Certificates.Find(X509FindType.FindBySubjectName, signerName, False)
        If certColl.Count = 0 Then
        End If
        storeMy.Close()
        Return certColl(0)
    End Function

    Public Shared Function GetCertificate()
        Dim storeMy As X509Store = New X509Store(StoreName.My, StoreLocation.CurrentUser)
        storeMy.Open(OpenFlags.ReadOnly)
        GetCertificate = storeMy.Certificates.Export(X509ContentType.Cert)
        storeMy.Close()
    End Function

    Public Shared Function GetRecipientCert() As X509Certificate2
        '*****************
        Dim _signerCert As X509Certificate2
        _signerCert = New X509Certificate2("D:\TurboPdfProject\TurboPdf\TurboPDF.pfx", "deal")
        Return (_signerCert)
        '*****************
        Dim storeAddressBook As X509Store = New X509Store(StoreName.AddressBook, StoreLocation.CurrentUser)
        storeAddressBook.Open(OpenFlags.ReadOnly)
        Console.WriteLine("Found certs with the following subject names in the " + "{0} store:", storeAddressBook.Name)
        For Each cert As X509Certificate2 In storeAddressBook.Certificates
            Console.WriteLine("" & Microsoft.VisualBasic.Chr(9) & "{0}", cert.SubjectName.Name)
        Next
        Dim certColl As X509Certificate2Collection = storeAddressBook.Certificates.Find(X509FindType.FindBySubjectName, recipientName, True)
        Console.WriteLine("Found {0} certificates in the {1} store with name {2}", certColl.Count, storeAddressBook.Name, recipientName)
        If certColl.Count = 0 Then
            Console.WriteLine("A suggested certificate to use for this example " + "is not in the certificate store. Select " + "an alternate certificate to use for " + "signing the message.")
        End If
        storeAddressBook.Close()
        Return certColl(0)
    End Function

    Public Shared Function SignMsg(ByVal msg As Byte(), ByVal signerCert As X509Certificate2) As Byte()
        Dim contentInfo As ContentInfo = New ContentInfo(msg)
        Dim signedCms As SignedCms = New SignedCms(contentInfo)
        Dim cmsSigner As CmsSigner = New CmsSigner(signerCert)
        Console.Write("Computing signature with signer subject " + "name {0} ... ", signerCert.SubjectName.Name)
        signedCms.ComputeSignature(cmsSigner, False)
        Console.WriteLine("Done.")
        Return signedCms.Encode()
        'Return signedCms.ContentInfo.Content
    End Function

    Public Shared Function VerifyMsg(ByVal encodedSignedCms As Byte(), ByRef origMsg As Byte()) As Boolean
        Dim signedCms As SignedCms = New SignedCms
        signedCms.Decode(encodedSignedCms)
        Try
            Console.Write("Checking signature on message ... ")
            signedCms.CheckSignature(True)
            Console.WriteLine("Done.")
        Catch e As System.Security.Cryptography.CryptographicException
            Console.WriteLine("VerifyMsg caught exception: {0}", e.Message)
            Console.WriteLine("The message may have been modified " + "in transit or storage. Authenticity of the " + "message is not guaranteed.")
            origMsg = Nothing
            Return False
        End Try
        origMsg = signedCms.ContentInfo.Content
        Return True
    End Function

    Public Shared Function EncryptMsg(ByVal msg As Byte(), ByVal recipientCert As X509Certificate2) As Byte()
        Dim contentInfo As ContentInfo = New ContentInfo(msg)
        Dim envelopedCms As EnvelopedCms = New EnvelopedCms(contentInfo)
        Dim recip1 As CmsRecipient = New CmsRecipient(SubjectIdentifierType.IssuerAndSerialNumber, recipientCert)
        Console.Write("Encrypting data for a single recipient of " + "subject name {0} ... ", recip1.Certificate.SubjectName.Name)
        envelopedCms.Encrypt(recip1)
        Console.WriteLine("Done.")
        Return envelopedCms.Encode
    End Function

    Public Shared Function DecryptMsg(ByVal encodedEnvelopedCms As Byte()) As Byte()
        Dim envelopedCms As EnvelopedCms = New EnvelopedCms
        envelopedCms.Decode(encodedEnvelopedCms)
        DisplayEnvelopedCms(envelopedCms, False)
        Console.Write("Decrypting Data ... ")
        envelopedCms.Decrypt(envelopedCms.RecipientInfos(0))
        Console.WriteLine("Done.")
        Return envelopedCms.Encode
    End Function

    Private Sub DisplaySignedCmsContent(ByVal desc As String, ByVal signedCms As SignedCms)
        Console.WriteLine(desc + " (length {0}): ", signedCms.ContentInfo.Content.Length)
        For Each b As Byte In signedCms.ContentInfo.Content
            Console.Write(b.ToString + " ")
        Next
        Console.WriteLine()
    End Sub

    Private Shared Sub DisplayEnvelopedCmsContent(ByVal desc As String, ByVal envelopedCms As EnvelopedCms)
        Console.WriteLine(desc + " (length {0}): ", envelopedCms.ContentInfo.Content.Length)
        For Each b As Byte In envelopedCms.ContentInfo.Content
            Console.Write(b.ToString + " ")
        Next
        Console.WriteLine()
    End Sub

    Private Shared Sub DisplayEnvelopedCms(ByVal e As EnvelopedCms, ByVal displayContent As Boolean)
        Console.WriteLine("" & Microsoft.VisualBasic.Chr(10) & "Enveloped PKCS #7 Message Information:")
        Console.WriteLine("" & Microsoft.VisualBasic.Chr(9) & "The number of recipients for the Enveloped PKCS #7 " + "is: {0}", e.RecipientInfos.Count)
        Dim i As Integer = 0
        While i < e.RecipientInfos.Count
            Console.WriteLine("" & Microsoft.VisualBasic.Chr(9) & "Recipient #{0} has type {1}.", i + 1, e.RecipientInfos(i).RecipientIdentifier.Type)
            System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
        End While
        If displayContent Then
            DisplayEnvelopedCmsContent("Enveloped PKCS #7 Content", e)
        End If
        Console.WriteLine()
    End Sub
End Class

