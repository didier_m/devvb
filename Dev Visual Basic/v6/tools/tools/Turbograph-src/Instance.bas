Attribute VB_Name = "InstanceModule"
Option Explicit

'API Utiliser pour r�aliser la capture des �v�nements li�s � un Hwnd
'dans notre cas celui du textbox cr��
Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" _
                (ByVal Hwnd As Long, _
                 ByVal nIndex As Long, _
                 ByVal dwNewLong As Long) As Long

'API recevant les �v�nement de la capture
Private Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" _
                (ByVal lpPrevWndFunc As Long, _
                 ByVal Hwnd As Long, _
                 ByVal Msg As Long, _
                 ByVal wParam As Long, _
                 ByVal lParam As Long) As Long

'API permettant d'envoyer un message vers un Handle, permet dans notre cas
'de lire le texte re�u dans le textbox
Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" _
                (ByVal Hwnd As Long, _
                ByVal wMsg As Long, _
                ByVal wParam As Long, _
                lParam As Any) As Long

'-----------------------------------------------------------------------------------------
'                       CONSTANTES POUR LES API
'-----------------------------------------------------------------------------------------
Private Const GWL_WNDPROC As Long = (-4)

Private Const WM_SETTEXT = &HC          'Constante d'API d�finissant l'�venement � analiser
Private Const WM_GETTEXT = &HD          'Constante d'API premettant de lire le texte
Private Const WM_GETTEXTLENGTH = &HE    'Constante d'API permettant de lire la logueur du texte pr�sent

'-----------------------------------------------------------------------------------------
'                       VARIABLES PRIVEES DU MODULE
'-----------------------------------------------------------------------------------------
Private DefWndProc      As Long
Private LinkTexthwnd    As Long
Private M_Instance      As InstanceClass


'-----------------------------------------------------------------------------------------
'                       FUNCTION ET SUB DU MODULE
'-----------------------------------------------------------------------------------------
'------------------------------------------------------------
' "Initialisation" de la capture des �v�nement du textbox
'------------------------------------------------------------
Public Function LinkCallback(ByVal TextHwnd As Long, ByVal clsInstance As InstanceClass)
  'Sauvegarde le Hwnd du textbox
    LinkTexthwnd = TextHwnd
  'Sauvegarde l'instance de la classe
    Set M_Instance = clsInstance
  'Subclass le textbox
    DefWndProc = SetWindowLong(LinkTexthwnd, GWL_WNDPROC, AddressOf WindowProc)

End Function

'------------------------------------------------------------
' Retire de la capture des �v�nement du textbox
'------------------------------------------------------------
Public Sub DetachCallback()
  'Retire le subclass du textbox
    Call SetWindowLong(LinkTexthwnd, GWL_WNDPROC, DefWndProc)
    Set M_Instance = Nothing

End Sub

'------------------------------------------------------------
' R�ception des �v�nement du textbox
'------------------------------------------------------------
Public Function WindowProc(ByVal Hwnd As Long, ByVal uMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    Dim ReturnText  As String   'Variable contenant le texte lu dans le textbox
    Dim LenText     As Long     'Variable contenant le longueur du texte � lire
    Dim LenRet      As Long     'Variable contenant la longueur du texte retourn�
  
  'Renvoie vers windows l'�v�nement
    WindowProc = CallWindowProc(DefWndProc, Hwnd, uMsg, wParam, lParam)
  'S�lection en fonction du message
    Select Case uMsg
        Case WM_SETTEXT
          'Lecture de la longueur du texte contenu dans le textbox
            LenText = SendMessage(LinkTexthwnd, WM_GETTEXTLENGTH, ByVal 0&, 0&) + 1
          'Pr�paration de la variable de lecture du texte (R�servation d'espace)
            ReturnText = Space(LenText)
          'Lecture du texte
            LenRet = SendMessage(LinkTexthwnd, WM_GETTEXT, ByVal LenText, ByVal ReturnText)
          'Retire les carract�res inutiles
            ReturnText = Left(ReturnText, LenRet)
          'Previent la classe de l'arriv�e du texte
            M_Instance.ExternCommand ReturnText
    End Select

End Function


