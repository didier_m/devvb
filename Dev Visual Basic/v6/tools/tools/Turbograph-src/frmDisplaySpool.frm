VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form frmDisplaySpool 
   BackColor       =   &H00808080&
   Caption         =   "Form1"
   ClientHeight    =   8280
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11400
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   552
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   760
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   7800
      Top             =   1200
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin RichTextLib.RichTextBox RtfError 
      Height          =   1695
      Left            =   240
      TabIndex        =   10
      Top             =   3600
      Visible         =   0   'False
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   2990
      _Version        =   393217
      Enabled         =   -1  'True
      ReadOnly        =   -1  'True
      ScrollBars      =   3
      TextRTF         =   $"frmDisplaySpool.frx":0000
   End
   Begin VB.PictureBox picAngle 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   4680
      ScaleHeight     =   25
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   25
      TabIndex        =   8
      Top             =   1440
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.PictureBox Page 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      DrawWidth       =   6
      ForeColor       =   &H80000008&
      Height          =   1695
      Left            =   4200
      ScaleHeight     =   113
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   238
      TabIndex        =   7
      Top             =   3240
      Visible         =   0   'False
      Width           =   3570
      Begin VB.TextBox lblForm 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   285
         Index           =   0
         Left            =   1440
         MousePointer    =   3  'I-Beam
         TabIndex        =   12
         Top             =   1320
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.PictureBox lblPicture 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   375
         Index           =   0
         Left            =   360
         ScaleHeight     =   375
         ScaleWidth      =   495
         TabIndex        =   14
         Top             =   240
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Label Label 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   2400
         TabIndex        =   13
         Top             =   360
         Visible         =   0   'False
         Width           =   135
      End
      Begin VB.Label lblHyperText 
         BackColor       =   &H00C0C0FF&
         BackStyle       =   0  'Transparent
         Height          =   375
         Index           =   0
         Left            =   1080
         MouseIcon       =   "frmDisplaySpool.frx":008B
         MousePointer    =   99  'Custom
         TabIndex        =   11
         Top             =   720
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.Label lblAccesDet 
         BackColor       =   &H00C0C0FF&
         BackStyle       =   0  'Transparent
         Height          =   375
         Index           =   0
         Left            =   240
         MouseIcon       =   "frmDisplaySpool.frx":0D55
         MousePointer    =   99  'Custom
         TabIndex        =   9
         Top             =   240
         Visible         =   0   'False
         Width           =   1575
      End
   End
   Begin VB.VScrollBar VScroll1 
      Height          =   975
      Left            =   0
      TabIndex        =   6
      Top             =   720
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.HScrollBar HScroll1 
      Height          =   255
      Left            =   3600
      TabIndex        =   5
      Top             =   5520
      Visible         =   0   'False
      Width           =   2655
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   5520
      Top             =   2400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":1A1F
            Key             =   "Warning"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   2415
      Left            =   240
      TabIndex        =   3
      Top             =   1080
      Visible         =   0   'False
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   4260
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "ImageList2"
      Appearance      =   1
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   570
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11400
      _ExtentX        =   20108
      _ExtentY        =   1005
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   25
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SELECTION"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FILEJOIN"
            Object.ToolTipText     =   "Fichiers joints"
            ImageIndex      =   24
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   28
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu5 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu6 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu7 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu8 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu9 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu10 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu11 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu12 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu13 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu14 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu15 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu16 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu17 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu18 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu19 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu20 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu21 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu22 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu23 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu24 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu25 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu26 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu27 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu28 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FIRST"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PREV"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "GOTO"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "NEXT"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "LAST"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PRINT"
            ImageIndex      =   7
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   3
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Key             =   "PRINTFILE"
                  Text            =   "Imprimer le fichier"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PRINTSPOOL"
                  Text            =   "Imprimer le spool"
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PRINTPAGE"
                  Text            =   "Imprimer la page"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "MESSAGERIE"
            ImageIndex      =   13
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "MAILING"
            ImageIndex      =   25
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EXPORTHTML"
            ImageIndex      =   10
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EXCEL"
            Object.ToolTipText     =   "Exporter vers Excel"
            ImageIndex      =   15
         EndProperty
         BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "OPENOFFICE"
            ImageIndex      =   21
         EndProperty
         BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PDF"
            ImageIndex      =   17
         EndProperty
         BeginProperty Button16 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "HELP"
            ImageIndex      =   8
         EndProperty
         BeginProperty Button17 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SAVEASSPOOL"
            ImageIndex      =   16
         EndProperty
         BeginProperty Button18 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "OPENDIR"
            ImageIndex      =   22
         EndProperty
         BeginProperty Button19 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "NOTES"
            ImageIndex      =   26
         EndProperty
         BeginProperty Button20 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FINDTEXT"
            ImageIndex      =   18
         EndProperty
         BeginProperty Button21 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FINDNEXT"
            ImageIndex      =   19
         EndProperty
         BeginProperty Button22 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button23 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ZOOMFIT"
            ImageIndex      =   20
         EndProperty
         BeginProperty Button24 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "ZOOM"
            ImageIndex      =   27
         EndProperty
         BeginProperty Button25 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "CURPAGE"
            ImageIndex      =   27
         EndProperty
      EndProperty
      Begin VB.ComboBox Combo1 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   11520
         TabIndex        =   4
         Text            =   "100"
         Top             =   120
         Width           =   760
      End
      Begin VB.PictureBox Picture1 
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         Height          =   360
         Left            =   11760
         ScaleHeight     =   24
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   82
         TabIndex        =   1
         Top             =   120
         Width           =   1230
         Begin VB.Label lblCountPage 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   120
            TabIndex        =   2
            Top             =   75
            Width           =   1095
         End
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   9960
      Top             =   5280
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   27
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":1B79
            Key             =   "SELECTION"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":2A53
            Key             =   "FIRST"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":332D
            Key             =   "PREV"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":3C07
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":44E1
            Key             =   "NEXT"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":4DBB
            Key             =   "LAST"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":5695
            Key             =   "PRINT"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":8527
            Key             =   "HELP"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":8E01
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":BC93
            Key             =   "MAIL"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":D96D
            Key             =   "Warning"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":DAC7
            Key             =   "MESSAGERIE"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":DDE1
            Key             =   "IMG_MAIL"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":10C73
            Key             =   "GOTO"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":11B4D
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":12A27
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":55939
            Key             =   "PDF"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":56213
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":587A5
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":5907F
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":59445
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":5975F
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":5A639
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":5AF13
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":5B7ED
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":5C0C7
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":5CFA1
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSWinsockLib.Winsock Winsock1 
      Left            =   3000
      Top             =   4320
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
End
Attribute VB_Name = "frmDisplaySpool"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-> Indique le Spool associ�
Public aSpool As Spool

'-> Pour positionnement de la page
Public LargeurX As Single
Public HauteurY As Single
Public MgX As Single
Public MgY As Single
Dim DepartX As Single
Dim DepartY As Single
Dim topToolbar As Boolean
Dim topResize As Boolean
Dim blnConnected As Boolean

Private Sub Combo1_Click()
'-> combo permettant de gerer le zoom
Dim ValZoom As Double

On Error GoTo GestError

'-> on determine la valeur du zoom
Select Case Val(Me.Combo1.Text) / 100
    Case 0
        '-> valeur erron�e remettre � 100%
        Me.Combo1.Text = "100 %"
        ValZoom = 1
    Case 0 To 0.25
        '-> la valeur minimum est 25%
        Me.Combo1.Text = " 25 %"
        ValZoom = 0.25
    Case 0.25 To 4
        Me.Combo1.Text = "" & Val(Me.Combo1.Text) & " %"
        ValZoom = Val(Me.Combo1.Text) / 100
    Case Else
        '-> valeur erron�e remettre � 100%
        Me.Combo1.Text = "100 %"
        ValZoom = 1
End Select

'-> ne rien faire si pas de modifications
If Zoom = ValZoom Then Exit Sub

'-> changer la valeur du zoom
Zoom = ValZoom
'-> Imprimer la page
PrintPageSpool aSpool, CLng(aSpool.CurrentPage)
'-> Afficher la page
aSpool.DisplayInterfaceByPage CLng(aSpool.CurrentPage)

GestError:

End Sub

Private Sub Combo1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Call Combo1_Click

End Sub

Private Sub Form_Activate()
        '-> on affiche les notes de la page
        notesDisplay aSpool
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

On Error Resume Next
Select Case KeyCode
    Case vbKeyHome 'premiere page
        Toolbar1_ButtonClick ByVal Me.Toolbar1.Buttons("FIRST")
        KeyCode = 0
    Case vbKeyPageUp 'page precedente
        Toolbar1_ButtonClick ByVal Me.Toolbar1.Buttons("PREV")
        KeyCode = 0
    Case vbKeyPageDown 'page suivante
        Toolbar1_ButtonClick ByVal Me.Toolbar1.Buttons("NEXT")
        KeyCode = 0
    Case vbKeyEnd 'derniere page
        Toolbar1_ButtonClick ByVal Me.Toolbar1.Buttons("LAST")
        KeyCode = 0
    Case vbKeyF 'rechercher
        If Shift = 2 Then
            Toolbar1_ButtonClick ByVal Me.Toolbar1.Buttons("FINDTEXT")
            KeyCode = 0
        End If
    Case vbKeyP 'fenetre impression
        If Shift = 2 Then
            Toolbar1_ButtonClick ByVal Me.Toolbar1.Buttons("PRINT")
            KeyCode = 0
        End If
    Case vbKeyS 'enregistrer sous
        If Shift = 2 Then
            Toolbar1_ButtonClick ByVal Me.Toolbar1.Buttons("SAVEASSPOOL")
            KeyCode = 0
        End If
    Case vbKeyE 'exporter sous excel
        If Shift = 2 Then
            Toolbar1_ButtonClick ByVal Me.Toolbar1.Buttons("EXCEL")
            KeyCode = 0
        End If
    Case vbKeyA 'ajuster la feuille � la taille de la fen�tre
        If Shift = 2 Then
            Toolbar1_ButtonClick ByVal Me.Toolbar1.Buttons("ZOOMFIT")
            KeyCode = 0
        End If
    Case vbKeyH 'afficher cacher la navigation
        If Shift = 2 Then
            '-> Afficher ou ne pas afficher le volet expmloration de spool
            IsNaviga = Not IsNaviga
            MDIMain.mnuSpool.Checked = IsNaviga
            MDIMain.picSplit.Visible = IsNaviga
            MDIMain.picNaviga.Visible = IsNaviga
            KeyCode = 0
        End If
    Case vbKeyF3 'rechercher le suivant
        Toolbar1_ButtonClick ByVal Me.Toolbar1.Buttons("FINDNEXT")
        KeyCode = 0
    Case vbKeyN 'pour test affichage des notes
        If Shift = 2 Then
            '--> strNote du type aspool.key|asppol.currentpage|top|left|width|height|couleur|transparence|texteentete|texteligne
            frmGetNote.Show vbModal
            noteAdd aSpool.fileName & "|" & aSpool.Num_Spool & "|" & aSpool.CurrentPage & "|1000|1000||||204|" & strRetour & "|"
            fNotes(fNotes.Count).Init Me
        End If
    Case vbKeyT 'pour test eclatement
        If Shift = 2 Then
            Set frmEclat.aSpool = Me.aSpool
            frmEclat.Show vbModal
        End If

End Select

End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim i As Integer
Dim PrinterFound As Boolean
Dim Printer As Printer

On Error Resume Next

'-> Pointer sur la classe Libelle
Set aLb = Libelles("FRMVISUSPOOL")

'-> ToolTip des boutons
Me.Toolbar1.Buttons("SELECTION").ToolTipText = aLb.GetCaption(1)
Me.Toolbar1.Buttons("FIRST").ToolTipText = aLb.GetCaption(2)
Me.Toolbar1.Buttons("PREV").ToolTipText = aLb.GetCaption(3)
Me.Toolbar1.Buttons("GOTO").ToolTipText = aLb.GetCaption(4)
Me.Toolbar1.Buttons("NEXT").ToolTipText = aLb.GetCaption(5)
Me.Toolbar1.Buttons("LAST").ToolTipText = aLb.GetCaption(6)
Me.Toolbar1.Buttons("PRINT").ToolTipText = aLb.GetCaption(7)
Me.Toolbar1.Buttons("EXPORTHTML").ToolTipText = aLb.GetCaption(9)
Me.Toolbar1.Buttons("MESSAGERIE").ToolTipText = aLb.GetCaption(8)
Me.Toolbar1.Buttons("HELP").ToolTipText = aLb.GetCaption(10)
Me.Toolbar1.Buttons("FINDTEXT").ToolTipText = aLb.GetCaption(16)
Me.Toolbar1.Buttons("FINDNEXT").ToolTipText = aLb.GetCaption(17)
Me.Toolbar1.Buttons("OPENOFFICE").ToolTipText = aLb.GetCaption(18)
Me.Toolbar1.Buttons("EXCEL").ToolTipText = aLb.GetCaption(23)
Me.Toolbar1.Buttons("SAVEASSPOOL").ToolTipText = aLb.GetCaption(24)
Me.Toolbar1.Buttons("OPENDIR").ToolTipText = aLb.GetCaption(25)
Me.Toolbar1.Buttons("ZOOMFIT").ToolTipText = aLb.GetCaption(22)
Me.Toolbar1.Buttons("FILEJOIN").ToolTipText = aLb.GetCaption(21)
Me.Toolbar1.Buttons("MAILING").ToolTipText = aLb.GetCaption(20)
Me.Toolbar1.Buttons("NOTES").ToolTipText = aLb.GetCaption(26)

'-> description des boutons
Me.Toolbar1.Buttons("SELECTION").Description = aLb.GetCaption(1)
Me.Toolbar1.Buttons("FIRST").Description = aLb.GetCaption(2)
Me.Toolbar1.Buttons("PREV").Description = aLb.GetCaption(3)
Me.Toolbar1.Buttons("GOTO").Description = aLb.GetCaption(4)
Me.Toolbar1.Buttons("NEXT").Description = aLb.GetCaption(5)
Me.Toolbar1.Buttons("LAST").Description = aLb.GetCaption(6)
Me.Toolbar1.Buttons("PRINT").Description = aLb.GetCaption(7)
Me.Toolbar1.Buttons("EXPORTHTML").Description = aLb.GetCaption(9)
Me.Toolbar1.Buttons("MESSAGERIE").Description = aLb.GetCaption(8)
Me.Toolbar1.Buttons("HELP").Description = aLb.GetCaption(10)
Me.Toolbar1.Buttons("FINDTEXT").Description = aLb.GetCaption(16)
Me.Toolbar1.Buttons("FINDNEXT").Description = aLb.GetCaption(17)
Me.Toolbar1.Buttons("OPENOFFICE").Description = aLb.GetCaption(18)
Me.Toolbar1.Buttons("EXCEL").Description = aLb.GetCaption(23)
Me.Toolbar1.Buttons("SAVEASSPOOL").Description = aLb.GetCaption(24)
Me.Toolbar1.Buttons("OPENDIR").Description = aLb.GetCaption(25)
Me.Toolbar1.Buttons("ZOOMFIT").Description = aLb.GetCaption(22)
Me.Toolbar1.Buttons("FILEJOIN").Description = aLb.GetCaption(21)
Me.Toolbar1.Buttons("MAILING").Description = aLb.GetCaption(20)
Me.Toolbar1.Buttons("NOTES").Description = aLb.GetCaption(26)

'-> Titre de la fen�tre
If Fichiers(aSpool.fileName).NbSpool <> "1" Then
    Me.Caption = aLb.GetCaption(11) & aSpool.SpoolText & "/" & Fichiers(aSpool.fileName).NbSpool & aLb.GetToolTip(11) & aSpool.fileName
Else
    Me.Caption = aLb.GetCaption(11) & aSpool.SpoolText & aLb.GetToolTip(11) & aSpool.fileName
End If

'-> Afficher les boutons des sous menus
Me.Toolbar1.Buttons("PRINT").ButtonMenus("PRINTFILE").Text = aLb.GetCaption(12)
Me.Toolbar1.Buttons("PRINT").ButtonMenus("PRINTSPOOL").Text = aLb.GetCaption(13)
Me.Toolbar1.Buttons("PRINT").ButtonMenus("PRINTPAGE").Text = aLb.GetCaption(14)
Me.Toolbar1.Buttons("PDF").ToolTipText = aLb.GetCaption(15)
Me.Toolbar1.Buttons("PDF").Description = aLb.GetCaption(15)

'-> Bloquer le bouton de la page de s�lection s'il n'y en a pas
If Not aSpool.IsSelectionPage Then Me.Toolbar1.Buttons("SELECTION").Enabled = False

'-> Masquer le bouton d'export vers Excel
Me.Toolbar1.Buttons("EXCEL").Visible = ExcelOk
Me.Toolbar1.Buttons("OPENOFFICE").Visible = OpenOfficeOk
If HideMessagerie Then
    Me.Toolbar1.Buttons("MAILING").Visible = False
    Me.Toolbar1.Buttons("MESSAGERIE").Visible = False
End If
'-> PIERROT teste si on a l'imprimante virtuelle PDF pour donner acces au bouton ou pas
'For Each Printer In Printers
'    If Trim(UCase$(Printer.DeviceName)) = "ACROBAT PDFWRITER" Or _
'       Trim(UCase$(Printer.DeviceName)) = "WIN2PDF" Or _
'       Trim(UCase$(Printer.DeviceName)) = "ADOBE PDF" Or _
'       Trim(UCase$(Printer.DeviceName)) = "PDFCREATOR" Or _
'       Trim(UCase$(Printer.DeviceName)) = "ACROBAT DISTILLER" Then PrinterFound = True
'Next

If Dir(App.Path & "\TurboPDF.exe") <> "" Then
    PrinterFound = True
End If

If Not PrinterFound Then
    Me.Toolbar1.Buttons("PDF").Enabled = False
    '-> changer le tooltip pour informer l'utilisateur des imprimantes PDF support�es
    Me.Toolbar1.Buttons("PDF").ToolTipText = aLb.GetCaption(19)
End If

'-> on charge la combo du zoom
Me.Combo1.AddItem " 25 %"
Me.Combo1.AddItem " 50 %"
Me.Combo1.AddItem " 75 %"
Me.Combo1.AddItem "100 %"
Me.Combo1.AddItem "125 %"
Me.Combo1.AddItem "150 %"
Me.Combo1.AddItem "200 %"
Me.Combo1.AddItem "300 %"
Me.Combo1.AddItem "400 %"
Me.Combo1.Text = "100 %"
Zoom = 1
'-> aligner la combo
'Me.Combo1.Left = ScaleX(Me.Toolbar1.Buttons("ZOOM").Left - Me.Toolbar1.Buttons("ZOOM").Width, 3, 1)
'Me.Picture1.Left = Me.Combo1.Left + Me.Combo1.Width + Me.Combo1.Width / 2

Call WheelHook(Me.Hwnd)

End Sub

Public Sub Form_Resize()

Dim aRect As RECT
Dim Res As Long

On Error Resume Next

'-> si on est deja en resize quitter
'If topResize Then Exit Sub
'topResize = True
'SendMessage Me.hWnd, WM_SIZE, 0, 0

'-> Recup�rer la taille de la zone client
Res = GetClientRect(Me.Hwnd, aRect)

'-> Initialiser les valeurs des barres de d�filement
Me.HScroll1.Min = MgX
Me.HScroll1.Max = -(MgX * 2 + LargeurX - aRect.Right)
Me.VScroll1.Min = MgY + Me.Toolbar1.Height
Me.VScroll1.Max = -(MgY * 2 + HauteurY - aRect.Bottom)

'-> Cas du redim en cas d'erreur
Me.TreeView1.Left = 0
Me.TreeView1.Top = 0
Me.TreeView1.Width = aRect.Right
Me.TreeView1.Height = aRect.Bottom / 8

Me.RtfError.Left = 0
Me.RtfError.Top = Me.TreeView1.Height
Me.RtfError.Width = Me.TreeView1.Width
Me.RtfError.Height = aRect.Bottom - Me.TreeView1.Top

'-> Se tirer si le tree est visible
If Me.TreeView1.Visible Then Exit Sub

'-> Tester si on doit faire apparaitre les barres de d�filement
If aRect.Right < MgX * 2 + LargeurX Then
    '-> Positionner le ScrollBar
    Me.HScroll1.Left = 0
    Me.HScroll1.Top = aRect.Bottom - Me.HScroll1.Height
    Me.HScroll1.Width = aRect.Right
    Me.HScroll1.Visible = True
    Me.HScroll1.ZOrder
Else
    Me.HScroll1.Visible = False
End If

If aRect.Bottom < MgY * 2 + HauteurY Then
    '-> Positionner le ScrollBar
    Me.VScroll1.Left = aRect.Right - Me.VScroll1.Width
    Me.VScroll1.Top = Me.Toolbar1.Height
    Me.VScroll1.Height = aRect.Bottom - Me.VScroll1.Top
    Me.VScroll1.Visible = True
    Me.VScroll1.ZOrder
Else
    Me.VScroll1.Visible = False
End If

'-> Modifier la taille des Scrolls si les 2 sont pr�sents
If Me.HScroll1.Visible And Me.VScroll1.Visible Then
    Me.HScroll1.Width = Me.HScroll1.Width - Me.VScroll1.Width
    Me.VScroll1.Height = Me.VScroll1.Height - Me.HScroll1.Height
    Me.HScroll1.Max = Me.HScroll1.Max - Me.VScroll1.Width
    Me.VScroll1.Max = Me.VScroll1.Max - Me.HScroll1.Height
    Me.picAngle.Width = Me.VScroll1.Width
    Me.picAngle.Height = Me.HScroll1.Height
    Me.picAngle.Top = Me.VScroll1.Top + Me.VScroll1.Height
    Me.picAngle.Left = Me.HScroll1.Left + Me.HScroll1.Width
    Me.picAngle.Visible = True
Else
    Me.picAngle.Visible = False
End If

'-> Mettre la barre d'outils au premier plan
Me.Toolbar1.ZOrder

Me.Combo1.Left = ScaleX(Me.Toolbar1.Buttons("ZOOM").Left, 3, 1)
Me.Combo1.Top = ScaleY(Me.Toolbar1.Buttons("ZOOM").Top + 8, 3, 1)
Me.Picture1.Left = ScaleX(Me.Toolbar1.Buttons("CURPAGE").Left - 5, 3, 1)
Me.Picture1.Top = ScaleY(Me.Toolbar1.Buttons("CURPAGE").Top + 8, 3, 1)

GestError:
'-> on est plus en resize
topResize = False
End Sub

Private Sub Form_Unload(Cancel As Integer)

Dim i As Integer

On Error Resume Next

'-> Supprimer l'affectation de la feuille dans le spool
Set aSpool.frmdisplay = Nothing

If aSpool.NbError = 0 Then
    '-> Suppirmer l'icone de s�lection
    For i = 1 To aSpool.nbPage
        '-> Test attention aux pages avec des erreurs rechercher la mise � jour des icnoes
        MDIMain.TreeNaviga.Nodes(UCase(Trim(aSpool.fileName)) & "�" & UCase$(Trim(aSpool.Key)) & "�PAGE|" & i).Image = "Page"
    Next
End If

aSpool.CurrentPage = 0

Call WheelUnHook(Me.Hwnd)

'-> on vide les eventuelles notes
'noteUnload (aSpool.FileName)

End Sub

Public Sub DisplayCurrentPage()

'---> Cette proc�dure mat � jour l'affichage

Me.lblCountPage = aSpool.CurrentPage & "/" & aSpool.nbPage
'--> on affiche la barre d'outil personnalis�e bon y a plein de bug microsoft on fait avec
'LockWindowUpdate Me.hWnd
'DoEvents
'Toolbar1.RestoreToolbar "TurboGraph", "TurboMain", "usr"
'LockWindowUpdate 0
End Sub

Private Sub HScroll1_Change()
    Me.Page.Left = Me.HScroll1.Value
End Sub

Private Sub DisplayDetail(Param As String)

'---> Cette proc�dure affiche un d�tail sp�cifi�

Dim i As Long, j As Long
Dim aLb As Libelle
Dim Condition As String
Dim AccesKey As String
Dim ToCompare As String
Dim Ligne As String
Dim ToAdd As Boolean
Dim LenAcces As Integer
Dim X As ListItem

On Error GoTo GestError

'-> Pointer sur la classe libell� affect�e
Set aLb = Libelles("ACCESDET")

'-> Ne rien faire si pas d'acc�s au d�tail
If Me.aSpool.NbLigDetail = 0 Then
    MsgBox aLb.GetCaption(1), vbExclamation + vbOKOnly, aLb.GetToolTip(1)
    Exit Sub
End If

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11
MDIMain.StatusBar1.Panels(1).Text = aLb.GetCaption(2)

'-> Eclater le param�trage
AccesKey = Entry(1, Param, Chr(0))
Condition = Entry(2, Param, Chr(0))

'-> Quitter si pas de cl� d'acc�s
If Trim(AccesKey) = "" Then GoTo GestError

'-> Charger la feuille d'acc�s au d�tail
Load frmDetail

'-> Cr�ation du browse
For i = 2 To NumEntries(aSpool.TitreAccesDetail, "|")
    '-> Cr�ation des colonnes
    frmDetail.ListView1.ColumnHeaders.add , , Entry(i, aSpool.TitreAccesDetail, "|")
Next

'-> Analyse de toutes les lignes de l'acc�s au d�tail
For i = 1 To Me.aSpool.NbLigDetail
    ToAdd = False
    '-> R�cup�rer la ligne en cours
    Ligne = Me.aSpool.GetLigDetail(i)
    '-> R�cup�rer la cl� d'acc�s au d�tail
    ToCompare = Entry(1, Ligne, "|")
    '-> Pas de test sur cl� de comparaison � blanc
    If Trim(ToCompare) = "" Then GoTo NextLig
    '-> Selon la condition � appliquer
    Select Case Condition
        Case 1 'Egal
            '-> Tester en �galit�
            If UCase$(Trim(ToCompare)) = UCase$(Trim(AccesKey)) Then ToAdd = True
        Case 2 'Inf�rieur
            '-> Tester que l'op�rateur soit num�ric
            If IsNumeric(ToCompare) Then
                If CDbl(ToCompare) < CDbl(AccesKey) Then ToAdd = True
            End If 'Si crtit�re de comparaison num
        Case 3 'Inf�rieur ou �gal
            '-> Tester que l'op�rateur soit num�ric
            If IsNumeric(ToCompare) Then
                If CDbl(ToCompare) <= CDbl(AccesKey) Then ToAdd = True
            End If 'Si crtit�re de comparaison num
        Case 4 'Supr�rieur
            '-> Tester que l'op�rateur soit num�ric
            If IsNumeric(ToCompare) Then
                If CDbl(ToCompare) > CDbl(AccesKey) Then ToAdd = True
            End If 'Si crtit�re de comparaison num
        Case 5 'Supr�rieur ou �gal
            '-> Tester que l'op�rateur soit num�ric
            If IsNumeric(ToCompare) Then
                If CDbl(ToCompare) >= CDbl(AccesKey) Then ToAdd = True
            End If 'Si crtit�re de comparaison num
        Case 6 'Commence par
            '-> Get de la longueur de la chaine de comparaison
            LenAcces = Len(AccesKey)
            '-> Comparer
            If UCase$(Mid$(Trim(ToCompare), 1, LenAcces)) = UCase$(Trim(AccesKey)) Then ToAdd = True
        Case 7 'Contient
            '-> Tester la pr�sence
            If InStr(1, UCase$(Trim(ToCompare)), UCase$(Trim(AccesKey))) <> 0 Then ToAdd = True
    End Select
    
    '-> Si on doit cr�er ou non
    If Not ToAdd Then GoTo NextLig
    
    '-> Cr�er un n ouvel Item
    Set X = frmDetail.ListView1.ListItems.add
    '-> Charger la ligne dans le browse
    For j = 2 To NumEntries(aSpool.TitreAccesDetail, "|")
        If j = 2 Then
            X.Text = Entry(j, Ligne, "|")
        Else
            X.SubItems(j - 2) = Entry(j, Ligne, "|")
        End If
    Next
NextLig:
Next

'-> Formatter le listview
FormatListView frmDetail.ListView1

'-> D�bloquer
Screen.MousePointer = 0
MDIMain.StatusBar1.Panels(1).Text = ""

'-> Afficher la feuille
frmDetail.Show vbModal

GestError:
    '-> D�bloquer l'�ran
    Me.Enabled = True
    Screen.MousePointer = 0
    MDIMain.StatusBar1.Panels(1).Text = ""

End Sub

Private Sub DisplayFile(fileName As String)
Dim lngResult As Long
Dim strBuffer As String

'si on a que le nom du fichier on le cherche dans le meme repertoire que le spool
If InStr(1, fileName, "/") = 0 And InStr(1, fileName, "\") = 0 Then
    fileName = Mid(aSpool.fileName, 1, InStrRev(Replace(aSpool.fileName, "\", "/"), "/") - 1) & "\" & fileName
End If
'-> sinon on pointe sur le lien

'--> permet d'ouvrir un lien hypertext
lngResult = ShellExecute(Me.Hwnd, "Open", fileName, vbNullString, App.Path, 1)
    
End Sub

Private Sub lblAccesDet_Click(Index As Integer)

DisplayDetail lblAccesDet(Index).Tag
End Sub

Private Sub lblForm_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)

On Error Resume Next
If KeyCode = 13 Then
    Me.VScroll1.SetFocus
    Me.lblForm(Index + 1).SetFocus
End If

End Sub

Private Sub lblForm_LostFocus(Index As Integer)
'-> enregistrer les modifications
Dim strPage As String
Dim strTemp() As String
Dim strTrav As String
Dim i As Integer

On Error Resume Next

strPage = aSpool.GetPage(aSpool.CurrentPage)
'-> on ecrit les nouvelles donn�es
strTemp = Split(strPage, "<frm:")

If UBound(strTemp) <> 0 Then
    strTrav = ""
    
    For i = 0 To Index - 2
        strTrav = strTrav & strTemp(i) & "<frm:"
    Next
    
    strTrav = strTrav & lblForm(Index).Text & ">" & Mid(strTemp(Index - 1), InStr(1, strTemp(Index - 1), ">") + 1)
    
    For i = Index To UBound(strTemp)
        If lblForm(i + 1).Visible = False Then
            strTrav = strTrav & "<frm:>" & Mid(strTemp(i), InStr(1, strTemp(i), ">") + 1)
        Else
            strTrav = strTrav & "<frm:" & strTemp(i)
            Exit For
        End If
    Next
    
    For i = i + 1 To UBound(strTemp)
        strTrav = strTrav & "<frm:" & strTemp(i)
    Next

    '-> on vide la page
    aSpool.ErasePage (aSpool.CurrentPage)
    '-> on ecrase la nouvelle page
    aSpool.SetPage aSpool.CurrentPage, strTrav

Else
    strPage = aSpool.GetMaq()
    '-> on ecrit les nouvelles donn�es
    strTemp = Split(strPage, "<frm:")
    For i = 0 To Index - 2
        strTrav = strTrav & strTemp(i) & "<frm:"
    Next
    
    strTrav = strTrav & lblForm(Index).Text & ">" & Mid(strTemp(Index - 1), InStr(1, strTemp(Index - 1), ">") + 1)
    
    For i = Index To UBound(strTemp)
        strTrav = strTrav & "<frm:" & strTemp(i)
    Next
    aSpool.SetMaqAll (strTrav)
    
End If

'-> Cr�er un fichier de d�tail
TempFile = CreateDetail(Me.aSpool, -1)
'-> Le renommer
Kill aSpool.fileName
Name TempFile As aSpool.fileName
'-> L'uploader sur le serveur
Trace "upload de " & aSpool.fileName
Call Upload(aSpool.fileName)

End Sub

Private Sub lblHyperText_Click(Index As Integer)

DisplayFile lblHyperText(Index).Tag

End Sub

Private Sub Page_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
'-> on initialises les variables de placement d'image
DepartX = X
DepartY = Y
End Sub

Private Sub Page_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
'-> on deplace eventuellement la feuille
If Button = 1 Then
    Me.Page.MousePointer = 5
    '-> on verifie pour toujours laisser une bonne partie de visible
    If Me.Page.Top + (Y - DepartY) <= Me.VScroll1.Min And Me.Page.Top + (Y - DepartY) >= Me.VScroll1.Max Then
        Me.Page.Top = Me.Page.Top + (Y - DepartY)
        Me.VScroll1.Value = Me.Page.Top
    End If
    If Me.Page.Left + (X - DepartX) <= Me.HScroll1.Min And Me.Page.Left + (X - DepartX) >= Me.HScroll1.Max Then
        Me.Page.Left = Me.Page.Left + (X - DepartX)
        Me.HScroll1.Value = Me.Page.Left
    End If
End If

End Sub

Private Sub Page_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
'-> on remet le curseur
Me.Page.MousePointer = 0
End Sub

Private Sub picAngle_Resize()

'---> Dessin des lignes de la 3D
Me.picAngle.Line (0, 0)-(Me.picAngle.Width - 1, 0), RGB(255, 255, 255)
Me.picAngle.Line (0, 0)-(0, Me.picAngle.Height - 1), RGB(255, 255, 255)

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

Dim Rep As VbMsgBoxResult
Dim aLb As Libelle
Dim MinPage As Integer
Dim DeviceName As String
Dim ChoixPage As String
Dim PageMin As Integer
Dim PageMax As Integer
Dim NbCopies As Integer
Dim RectoVerso As String
Dim Assembl As String
Dim NoGard As String
Dim FileToPrint As String
Dim Xpos As Integer
Dim Ypos As Integer
Dim i As Integer
Dim printFilesJoins As String
Dim aPrinter As Printer
Dim aSpool2 As Spool

On Error Resume Next

'-> Setting del page mini d'impression
If aSpool.IsSelectionPage Then
    MinPage = 0
Else
    MinPage = 1
End If

Select Case Button.Key

    Case "SELECTION"
        '-> Afficher la page de s�lection de cet utilisateur
        PrintPageSpool aSpool, 0
        
        '-> Afficher la page
        aSpool.DisplayInterfaceByPage 0
        
    Case "FIRST"
        '-> on gere le ca ou il y a plusieurs spool sur le fichier
        If Fichiers(aSpool.fileName).NbSpool = 1 Then
            '-> Afficher la premi�re page si elle n'est pas en cours
            If aSpool.CurrentPage <> MinPage Then
                '-> Imprimer la page
                PrintPageSpool aSpool, MinPage
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage MinPage
            End If
        Else
            Set MDIMain.TreeNaviga.SelectedItem = MDIMain.TreeNaviga.Nodes(3)
            Set MDIMain.TreeNaviga.SelectedItem = MDIMain.TreeNaviga.Nodes(UCase(aSpool.fileName) & "�SPOOL|1") '& "�PAGE|1")
            MDIMain.TreeNaviga_DblClick
        End If
    Case "PREV"
        '-> on gere le ca ou il y a plusieurs spool sur le fichier
        If Fichiers(aSpool.fileName).NbSpool = 1 Then
            '-> Afficher la page pr�c�dente si on n'est pas sur la page 1
            If aSpool.CurrentPage <> MinPage Then
                '-> Imprimer la page
                PrintPageSpool aSpool, aSpool.CurrentPage - 1
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage aSpool.CurrentPage
            End If
        Else
            '-> si on est sur la premiere page essayer d'aller sur la derniere page du spool precedent
            If aSpool.CurrentPage > 1 Then
                '-> Imprimer la page
                If aSpool.frmdisplay Is Nothing Then MDIMain.InitFRMDISPLAY aSpool
                PrintPageSpool aSpool, aSpool.CurrentPage - 1
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage aSpool.CurrentPage
            Else
                If Fichiers(aSpool.fileName).Spools(aSpool.Key).Num_Spool > 1 Then
                    If Fichiers(aSpool.fileName).Spools((Fichiers(aSpool.fileName).Spools(aSpool.Key).Num_Spool - 1)).nbPage <> 0 Then
                        Set MDIMain.TreeNaviga.SelectedItem = MDIMain.TreeNaviga.Nodes(UCase(aSpool.fileName) & "�SPOOL|" & (Fichiers(aSpool.fileName).Spools(aSpool.Key).Num_Spool - 1) & "�PAGE|" & Fichiers(aSpool.fileName).Spools((Fichiers(aSpool.fileName).Spools(aSpool.Key).Num_Spool - 1)).nbPage)
                    Else
                        Set MDIMain.TreeNaviga.SelectedItem = MDIMain.TreeNaviga.Nodes(UCase(aSpool.fileName) & "�SPOOL|" & (Fichiers(aSpool.fileName).Spools(aSpool.Key).Num_Spool - 1))
                    End If
                    MDIMain.TreeNaviga_DblClick
                End If
            End If
        End If
    Case "PRINT"
        '-> Vider la variable de retour
        strRetour = ""
        
        '-> Setting des param�trages sur le fichier et le spool en cours
        frmPrint.FichierName = aSpool.fileName
        frmPrint.SpoolKey = aSpool.Key
        frmPrint.IsSelectionPage = aSpool.IsSelectionPage
        If nCopies <> 0 Then frmPrint.Text3.Text = nCopies
        '-> afficher ou pas la zone d'impression de la globalit� des spools
        If Fichiers(UCase$(aSpool.fileName)).Spools.Count = 1 Then
            frmPrint.Option4.Visible = False
        End If
        
        '-> si on doit afficher l'option d'impression des fichiers joins
        If Fichiers(UCase$(aSpool.fileName)).filesJoins <> "" Then
            frmPrint.Check3.Visible = True
        End If
        If nRectoVerso Then frmPrint.Check1.Value = 1
        '-> Afficher le choix de l'imprimante
        Set frmPrint.aSpool = aSpool
        frmPrint.Show vbModal
    
        '-> Quitter si annuler
        If strRetour = "" Then Exit Sub
        
        '-> Traiter les choix d'impression
        DeviceName = Entry(1, strRetour, "|")
        ChoixPage = Entry(2, strRetour, "|")
        NbCopies = Entry(3, strRetour, "|")
        RectoVerso = Entry(4, strRetour, "|")
        If copyAssemb Then Assembl = Entry(5, strRetour, "|")
        If noGarde Or Fichiers(UCase$(aSpool.fileName)).Spool(1).GetPage(0) <> "" Then
            NoGard = Entry(6, strRetour, "|")
            If NoGard = "" Then noGarde = False
        End If
        If Entry(7, strRetour, "|") <> "" And NumEntries(strRetour, "|") >= 7 Then printFilesJoins = Entry(7, strRetour, "|")
        
        '-> Analyse du choix d'impression
        Select Case Entry(1, ChoixPage, "�")
            Case 1 '-> Fichier en entier
                '-> Construire le nom du fichier
                If noGarde Then
                    FileToPrint = CreateSpoolNoGarde(aSpool.fileName, "")
                Else
                    FileToPrint = aSpool.fileName
                End If
            Case 2, 5, 6 '-> Page mini � page maxi
                '-> R�cup�rer les pages � imprimer
                PageMin = CInt(Entry(2, ChoixPage, "�"))
                PageMax = CInt(Entry(3, ChoixPage, "�"))
                
                '-> V�rifier la page min
                If PageMin < 1 Then
                    '-> V�rifier si le fichier poss�de une page de s�lection
                    If aSpool.IsSelectionPage Then
                        PageMin = 0
                    Else
                        PageMin = 1
                    End If
                End If
                
                '-> V�rifier la page maxi
                If PageMax > aSpool.nbPage Then PageMax = aSpool.nbPage
                    
                '-> V�rifier que la page mini est < � la page maxi
                If PageMin > PageMax Then PageMin = PageMax
                    
                '-> Construire le nom du fichier
                If Entry(1, ChoixPage, "�") = 6 Then
                    FileToPrint = CreateSpoolDetail(Fichiers(UCase$(aSpool.fileName)), -2, PageMin, PageMax, aSpool)
                Else
                    FileToPrint = CreateSpoolDetail(Fichiers(UCase$(aSpool.fileName)), -2, PageMin, PageMax)
                End If
            Case 3  '-> Page en cours
                '-> Construire le nom du fichier
                FileToPrint = CreateDetail(Me.aSpool, aSpool.CurrentPage)
            Case 4 '-> Spool en cours
                'FileToPrint = aSpool.FileName
                FileToPrint = CreateDetail(Me.aSpool, -1)
        End Select
    
        'Debug.Print DeviceName & "~DIRECT~" & NbCopies & "|" & FileToPrint
    
        '-> Tester le cas de l'impression PDF
        
        ' *** PIERROT : tjrs pour gerer le soft WIN2PDF
        If UCase$(Trim(DeviceName)) = "WIN2PDF" Then
            '-> Lancer l'�dition
            Shell App.Path & "\TurboGraph.exe " & "WIN2PDF" & "~DIRECT~" & NbCopies & "|" & FileToPrint & "**0", vbNormalFocus
            Exit Sub
        End If
         
        If UCase$(Trim(DeviceName)) = "ACROBAT PDFWRITER" Then
            '-> Lancer l'�dition
            Shell App.Path & "\TurboGraph.exe " & "ACROBAT PDFWRITER" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso & "||" & NoGard, vbNormalFocus
            Exit Sub
        ElseIf UCase$(Trim(DeviceName)) = "ACROBAT DISTILLER" Then
            Shell App.Path & "\TurboGraph.exe " & "ACROBAT DISTILLER" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso & "||" & NoGard, vbNormalFocus
            Exit Sub
        ElseIf UCase$(Trim(DeviceName)) = "ADOBE PDF" Then
            Shell App.Path & "\TurboGraph.exe " & "ADOBE PDF" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso & "||" & NoGard, vbNormalFocus
            Exit Sub
        ElseIf UCase$(Trim(DeviceName)) = "PDFCREATOR" Then
            Shell App.Path & "\TurboGraph.exe " & "PDFCREATOR" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso & "||" & NoGard, vbNormalFocus
            Exit Sub
        ElseIf UCase$(Trim(DeviceName)) = "TURBOPDF" Then
            If GetFrameworkVersion("") <> "" Then
                If ChoixPage > 1 And TurbosavePath = "" Then
                    Shell App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & FileToPrint & GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False) & "�pdfDirectory=" & FilePath & Chr(34)
                Else
                    Shell App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & FileToPrint & GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False) & "�pdfDirectory=" & TurbosavePath & Chr(34)
                End If
            Else
                MsgBox "Please install Microsoft .net framework (2.0 minimum)"
            End If
            
            Exit Sub
        Else
            '-> Lancer l'impression
            Shell App.Path & "\TurboGraph.exe " & DeviceName & "~DIRECT~" & NbCopies & "|" & FileToPrint & "|" & RectoVerso & "|" & Assembl & "|" & NoGard & "|" & printFilesJoins, vbNormalFocus
        End If
        
        '-> si on doit imprimer les pi�ces jointes
        If printFilesJoins Then
            For Each aPrinter In Printers
                If aPrinter.DeviceName = DeviceName Then Set Printer = aPrinter
            Next
            For i = 1 To NumEntries(Fichiers(UCase$(aSpool.fileName)).filesJoins, ",")
                ShellExecute Me.Hwnd, "Print", Entry(i, Fichiers(UCase$(aSpool.fileName)).filesJoins, ","), DeviceName, App.Path, 1
            Next
        End If
    Case "GOTO"
        '-> Afficher la fen�tre de recherche des pages
        strRetour = ""
        If aSpool.IsSelectionPage Then
            frmSearchPage.NbMin = 0
        Else
            frmSearchPage.NbMin = 1
        End If
        frmSearchPage.NbMax = aSpool.nbPage
        frmSearchPage.Show vbModal
        If strRetour <> "" Then
            '-> Imprimer la page
            PrintPageSpool aSpool, CLng(strRetour)
            '-> Afficher la page
            aSpool.DisplayInterfaceByPage CLng(strRetour)
        End If
            
    Case "NEXT"
        '-> on gere le ca ou il y a plusieurs spool sur le fichier
        If Fichiers(aSpool.fileName).NbSpool = 1 Then
            '-> Afficher la page suivante si on n'est pas sur la derniere page
            If aSpool.CurrentPage <> aSpool.nbPage Then
                '-> Imprimer la page
                PrintPageSpool aSpool, aSpool.CurrentPage + 1
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage aSpool.CurrentPage
            End If
        Else
            '-> si on est sur la derniere page essayer d'aller sur la premiere page du spool suivant
            If aSpool.CurrentPage < aSpool.nbPage Then
                '-> Imprimer la page
                If aSpool.frmdisplay Is Nothing Then MDIMain.InitFRMDISPLAY aSpool
                PrintPageSpool aSpool, aSpool.CurrentPage + 1
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage aSpool.CurrentPage '+ 1
            Else
                Set aSpool2 = aSpool
                If Not aSpool.frmdisplay Is Nothing Then Unload aSpool.frmdisplay
                '-> on pointe sur le spool
                If aSpool.nbPage <> 0 Then aSpool = Fichiers(UCase$(Trim(aSpool.fileName))).Spools(Entry(2, MDIMain.TreeNaviga.SelectedItem.Key, "�"))
                If Fichiers(aSpool.fileName).Spools(aSpool.Key).Num_Spool < Fichiers(aSpool.fileName).NbSpool Then
                    'Set aSpool = aSpool2
                    Set MDIMain.TreeNaviga.SelectedItem = MDIMain.TreeNaviga.Nodes(UCase(aSpool.fileName & "�SPOOL|" & (Fichiers(aSpool.fileName).Spools(aSpool.Key).Num_Spool + 1))) '& "�PAGE|1"))
                    MDIMain.TreeNaviga_DblClick
                Else
                    Set aSpool = aSpool2
                    Set MDIMain.TreeNaviga.SelectedItem = MDIMain.TreeNaviga.Nodes(UCase(aSpool.fileName & "�SPOOL|" & (Fichiers(aSpool.fileName).Spools(aSpool.Key).Num_Spool + 1))) '& "�PAGE|1"))
                    MDIMain.TreeNaviga_DblClick
                End If
            End If
        End If
    Case "LAST"
        '-> on gere le ca ou il y a plusieurs spool sur le fichier
        If Fichiers(aSpool.fileName).NbSpool = 1 Then
            '-> Afficher la page si ce n'est pas la derni�re
            If aSpool.CurrentPage <> aSpool.nbPage Then
                '-> Imprimer la page
                PrintPageSpool aSpool, aSpool.nbPage
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage aSpool.nbPage
            End If
      Else
            Set MDIMain.TreeNaviga.SelectedItem = MDIMain.TreeNaviga.Nodes(UCase(aSpool.fileName) & "�SPOOL|" & (Fichiers(aSpool.fileName).NbSpool & "�PAGE|" & Fichiers(aSpool.fileName).Spools(Fichiers(aSpool.fileName).NbSpool).nbPage))
            MDIMain.TreeNaviga_DblClick
      End If
    Case "MESSAGERIE"
        Set frmMail.aSpool = Me.aSpool
        frmMail.FichierName = UCase$(Trim(aSpool.fileName))
        frmMail.SpoolKey = UCase$(Trim(aSpool.Key))
        iniName = "TurboMail.ini"
        frmMail.FromVisu = True
        frmMail.Show vbModal
            
    Case "MAILING"
        
        'If Not OutLookOk Then
        '    Set aLb = Libelles("FRMMAIL")
        '    MsgBox aLb.GetCaption(44), vbInformation, "Mailing"
        '    Exit Sub
        'End If

        Set frmMailList.aSpool = Me.aSpool
        frmMailList.FichierName = UCase$(Trim(aSpool.fileName))
        frmMailList.SpoolKey = UCase$(Trim(aSpool.Key))
        iniName = "TurboMail.ini"
        frmMailList.Show vbModal
        
    Case "EXPORTHTML"
        Set frmInternet.aSpool = Me.aSpool
        frmInternet.Show vbModal
        
    Case "HELP"
        ShellExecute Me.Hwnd, "Open", "http://www.deal.fr", vbNullString, App.Path, 1
    Case "EXCEL"
        strRetour = ""
        frmExcel.FichierName = UCase$(Trim(aSpool.fileName))
        frmExcel.SpoolKey = UCase$(Trim(aSpool.Key))
        frmExcel.Init
        frmExcel.Show vbModal
        
    Case "OPENOFFICE"
        strRetour = ""
        frmOpenOffice.FichierName = UCase$(Trim(aSpool.fileName))
        frmOpenOffice.SpoolKey = UCase$(Trim(aSpool.Key))
        frmOpenOffice.Init
        frmOpenOffice.Show vbModal
        
    Case "PDF"
        '-> Export du fichier en entier
        FileToPrint = CreateDetail(Me.aSpool, -1)
        'Shell App.Path & "\TurboGraph.exe " & "WIN2PDF" & "~DIRECT~1|" & FileToPrint & "**0", vbNormalFocus
        '-> on recupere l'imprimante pdf
        For i = 0 To Printers.Count - 1
            If UCase$(Printers(i).DeviceName) = "ACROBAT PDFWRITER" Or UCase$(Printers(i).DeviceName) = "ACROBAT DISTILLER" Or UCase$(Printers(i).DeviceName) = "ADOBE PDF" Or UCase$(Printers(i).DeviceName) = "WIN2PDF" Or UCase$(Printers(i).DeviceName) = "PDFCREATOR" Then
                DeviceName = Printers(i).DeviceName
            End If
        Next
        '-> on regarde si on a donn� la priorit� au turbo pdf
        If Trim(GetIniString("PDF", "TURBOPDF", App.Path & "\Turbograph.ini", False)) <> "1" Then DeviceName = "TURBOPDF"
        
        If UCase$(Trim(DeviceName)) = "WIN2PDF" Then
            '-> Lancer l'�dition
            Shell App.Path & "\TurboGraph.exe " & "WIN2PDF" & "~DIRECT~1|" & FileToPrint & "**0", vbNormalFocus
            Exit Sub
        End If

        If UCase$(Trim(DeviceName)) = "ACROBAT PDFWRITER" Then
            '-> Lancer l'�dition
            Shell App.Path & "\TurboGraph.exe " & "ACROBAT PDFWRITER" & "~DIRECT~1|" & FileToPrint & "**0", vbNormalFocus
            Exit Sub
        ElseIf UCase$(Trim(DeviceName)) = "ACROBAT DISTILLER" Then
            Shell App.Path & "\TurboGraph.exe " & "ACROBAT DISTILLER" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso, vbNormalFocus
            Exit Sub
        ElseIf UCase$(Trim(DeviceName)) = "ADOBE PDF" Then
            Shell App.Path & "\TurboGraph.exe " & "ADOBE PDF" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso, vbNormalFocus
            Exit Sub
        ElseIf UCase$(Trim(DeviceName)) = "PDFCREATOR" Then
            Shell App.Path & "\TurboGraph.exe " & "PDFCREATOR" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso, vbNormalFocus
            Exit Sub
        ElseIf Dir(App.Path & "\TurboPDF.exe") <> "" Then
            '-> on recupere le repertoire du spool d'origine
            FileToPrint = aSpool.fileName
            If Fichiers(UCase$(Trim(aSpool.fileName))).FileNameZip <> FileToPrint Then FileToPrint = Fichiers(UCase$(Trim(aSpool.fileName))).FileNameZip
            If FilePath = "" Then FilePath = OpenPath
            'If InStr(1, FileToPrint, "Content.IE5", vbTextCompare) <> 0 Then
            '    '-> on se fait une copie du fichier
            '    FileCopy FileToPrint, FileToPrint + "tmp"
            '    FileToPrint = FileToPrint + "tmp"
            'End If
            '-> en mode internet mettre dans le repertoire mes spools
'            If InStr(1, FilePath, "Content.IE5", vbTextCompare) <> 0 Or InStr(1, FilePath, "settings", vbTextCompare) <> 0 Then
'                MkDir GetSpecialfolder(CSIDL_PERSONAL) & "\Mes spools\"
'                FilePath = GetSpecialfolder(CSIDL_PERSONAL) & "\Mes spools\"
'            End If
            Trace App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & FileToPrint & GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False) & "�pdfDirectory=" & TurbosavePath & Chr(34), 1
            If GetFrameworkVersion("") <> "" Then
                Shell App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & FileToPrint & GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False) & "�pdfDirectory=" & TurbosavePath & Chr(34)
            Else
                MsgBox "Please install Microsoft .net framework (2.0 minimum)"
            End If
        End If
             
    Case "SAVEASSPOOL"
        SaveSpool
    Case "FINDTEXT"
        '-> Afficher la fen�tre de recherche du texte
        strRetour = ""
        frmSearchText.Text1 = FindText
        frmSearchText.Show vbModal
        If strRetour <> "" Then
            '-> On active le top de recherche
            FindTop = True
            FindPos2 = 0
            FindPos = 0
            FindPage = aSpool.CurrentPage
            FindText = strRetour
            Screen.MousePointer = 11
            '-> on pointe sur le spool
            Set aSpool = Fichiers(UCase$(Trim(aSpool.fileName))).Spools(Entry(2, MDIMain.TreeNaviga.SelectedItem.Key, "�"))
            '-> On recherche la page contenant le texte a afficher et la position
            i = aSpool.CurrentPage - 1
            Do While i < aSpool.nbPage
                i = i + 1
                '-> on regarde si on trouve dans le spool
                If IsTextInPage(aSpool, i) Then
                    If aSpool.frmdisplay Is Nothing Then MDIMain.InitFRMDISPLAY aSpool
                    '-> Imprimer la page en recherchant le texte
                    PrintPageSpool aSpool, CLng(i)
                End If
                '-> si on a trouve le texte quitter
                If Not FindTop Then Exit Do
                FindPos = 0
                FindPos2 = 0
                '-> on gere le ca ou il y a plusieurs spool sur le fichier
                If Fichiers(aSpool.fileName).NbSpool <> 1 Then
                    '-> si on est sur la derniere page essayer d'aller sur la premiere page du spool suivant
                    If i >= aSpool.nbPage Then
                        If Fichiers(aSpool.fileName).Spools(aSpool.Key).Num_Spool < Fichiers(aSpool.fileName).NbSpool Then
                            Set MDIMain.TreeNaviga.SelectedItem = MDIMain.TreeNaviga.Nodes(UCase(aSpool.fileName) & "�SPOOL|" & (Fichiers(aSpool.fileName).Spools(aSpool.Key).Num_Spool + 1) & "�PAGE|1")
                            Set aSpool = Fichiers(UCase$(Trim(aSpool.fileName))).Spools(Entry(2, MDIMain.TreeNaviga.SelectedItem.Key, "�"))
                            FindPage = 1
                            i = 0
                        End If
                    End If
                End If
            Loop
            Screen.MousePointer = 0
            If Not FindTop Then
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage CLng(i)
                '-> On scroll pour afficher le texte
                If Me.Page.Left + aSpool.frmdisplay.ScaleWidth - FindScrollH < 0 Then
                    aSpool.frmdisplay.HScroll1.Value = aSpool.frmdisplay.HScroll1.Max
                Else
                    aSpool.frmdisplay.HScroll1.Value = aSpool.frmdisplay.HScroll1.Min - FindScrollH
                End If
                If Me.Page.Top + aSpool.frmdisplay.ScaleHeight - FindScrollV < 0 Then
                    aSpool.frmdisplay.VScroll1.Value = aSpool.frmdisplay.VScroll1.Max
                Else
                    aSpool.frmdisplay.VScroll1.Value = aSpool.frmdisplay.VScroll1.Min - FindScrollV
                End If
                '-> on vide la feuille de la memoire
                If Fichiers(aSpool.fileName).NbSpool <> 1 Then Set aSpool.frmdisplay = Nothing
            Else
                '-> On a rien trouv�
                Set MDIMain.TreeNaviga.SelectedItem = MDIMain.TreeNaviga.Nodes(UCase(aSpool.fileName) & "�SPOOL|" & aSpool.Num_Spool & "�PAGE|" & aSpool.CurrentPage)
                aSpool.CurrentPage = 0
                MDIMain.TreeNaviga_DblClick
                FindPos = 0 'numero de l'occurence
                FindPage = 0 'on vide la variable de page de la derniere occurence
                FindTop = False 'on est plus en recherche
                '-> Pointer sur la classe libelles
                Set aLb = Libelles("FRMSEARCHTEXT")
                MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
            End If
        End If
    Case "FINDNEXT"
        If FindText = "" Then Exit Sub
        FindTop = True
        '-> dans le cas ou on est positionn� sur une autre page
        If FindPage <> aSpool.CurrentPage Then
            FindPos = 0
            FindPage = aSpool.CurrentPage
        End If
        FindPos2 = 0
        Screen.MousePointer = 11
        '-> on pointe sur le spool
        Set aSpool = Fichiers(UCase$(Trim(aSpool.fileName))).Spools(Entry(2, MDIMain.TreeNaviga.SelectedItem.Key, "�"))
        '-> On recherche la page contenant le texte a afficher et la position
        i = aSpool.CurrentPage - 1
        Do While i < aSpool.nbPage
            i = i + 1
            '-> on regarde si on trouve dans le spool
            If IsTextInPage(aSpool, i) Then
                '-> Imprimer la page en recherchant le texte
                If aSpool.frmdisplay Is Nothing Then MDIMain.InitFRMDISPLAY aSpool
                PrintPageSpool aSpool, CLng(i)
            End If
            '-> si on a trouve le texte quitter
            If Not FindTop Then Exit Do
            FindPos = 0
            FindPos2 = 0
            If i <= aSpool.nbPage Then aSpool.CurrentPage = i
            '-> on gere le ca ou il y a plusieurs spool sur le fichier
            If Fichiers(aSpool.fileName).NbSpool <> 1 Then
                '-> si on est sur la derniere page essayer d'aller sur la premiere page du spool suivant
                If aSpool.CurrentPage >= aSpool.nbPage Then
                    If Fichiers(aSpool.fileName).Spools(aSpool.Key).Num_Spool < Fichiers(aSpool.fileName).NbSpool Then
                        Set aSpool.frmdisplay = Nothing
                        Set MDIMain.TreeNaviga.SelectedItem = MDIMain.TreeNaviga.Nodes(UCase(aSpool.fileName) & "�SPOOL|" & (Fichiers(aSpool.fileName).Spools(aSpool.Key).Num_Spool + 1) & "�PAGE|1")
                        Set aSpool = Fichiers(UCase$(Trim(aSpool.fileName))).Spools("SPOOL|" & (Fichiers(aSpool.fileName).Spools(aSpool.Key).Num_Spool + 1))
                        FindPage = 1
                        i = 0
                    End If
                End If
            End If
        Loop
        Screen.MousePointer = 0
        If Not FindTop Then
            '-> Afficher la page
            aSpool.DisplayInterfaceByPage CLng(aSpool.CurrentPage)
                '-> On scroll pour afficher le texte
                If Me.Page.Left + aSpool.frmdisplay.ScaleWidth - FindScrollH < 0 Then
                    aSpool.frmdisplay.HScroll1.Value = aSpool.frmdisplay.HScroll1.Max
                Else
                    aSpool.frmdisplay.HScroll1.Value = aSpool.frmdisplay.HScroll1.Min - FindScrollH
                End If
                If Me.Page.Top + aSpool.frmdisplay.ScaleHeight - FindScrollV < 0 Then
                    aSpool.frmdisplay.VScroll1.Value = aSpool.frmdisplay.VScroll1.Max
                Else
                    aSpool.frmdisplay.VScroll1.Value = aSpool.frmdisplay.VScroll1.Min - FindScrollV
                End If
            '-> on vide la feuille de la memoire
            If Fichiers(aSpool.fileName).NbSpool <> 1 Then Set aSpool.frmdisplay = Nothing
        Else
            '-> On a rien trouv�
            Set MDIMain.TreeNaviga.SelectedItem = MDIMain.TreeNaviga.Nodes(UCase(aSpool.fileName) & "�SPOOL|" & aSpool.Num_Spool & "�PAGE|" & aSpool.CurrentPage)
            aSpool.CurrentPage = 0
            MDIMain.TreeNaviga_DblClick
            FindPos = 0
            FindPage = 0
            FindTop = False 'on est plus en recherche
            '-> Pointer sur la classe libelles
            Set aLb = Libelles("FRMSEARCHTEXT")
            MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
        End If
    Case "ZOOMFIT"
        '-> on reaffiche la feuille pour les cas ou on aurait redimensionner la feuille
        '-> Imprimer la page
        If aSpool.frmdisplay Is Nothing Then MDIMain.InitFRMDISPLAY aSpool
        '-> on reajuste la taille de l'image a celle de l'ecran
        'Ypos = Me.VScroll1.Height + Me.VScroll1.Width - 10
        Ypos = aSpool.frmdisplay.ScaleHeight - 50
        Xpos = Sortie.ScaleWidth * (Ypos / Sortie.Height)
        '-> on regarde si on passe en largeur sinon ajuster sur la largeur
        If Xpos > aSpool.frmdisplay.ScaleWidth Then
            Xpos = aSpool.frmdisplay.ScaleWidth - 25
            Ypos = Sortie.ScaleHeight * (Xpos / Sortie.Width)
        End If
        '-> on calcul la nouvelle valeur de zoom
        Me.Combo1.Text = CInt(Val(Ypos / Sortie.ScaleHeight * 100) * Zoom)
        Call Combo1_Click
    Case "OPENDIR"
        '--> ouvrir le filtre des spools
        LockWindowUpdate Me.Hwnd
        frmGestSpool.Show
        LockWindowUpdate 0
    Case "FILEJOIN"
        '-> fichiers joins
        ShellExecute Me.Hwnd, "Open", Button.ButtonMenus(1).Tag, vbNullString, App.Path, 1
    Case "NOTES"
        '--> strNote du type aspool.key|asppol.currentpage|top|left|width|height|couleur|transparence|texteentete|texteligne
        frmGetNote.Show vbModal
        noteAdd aSpool.fileName & "|" & aSpool.Num_Spool & "|" & aSpool.CurrentPage & "|" & "0" & "|0||||204|" & strRetour & "|"
        fNotes(fNotes.Count).Init Me
End Select


End Sub

Private Sub SaveSpool()

'---> Cette proc�dure enregistre le spool en cours dans un fichier


Dim Rep As VbMsgBoxResult
Dim aLb As Libelle
Dim TempFile As String

On Error GoTo GestError

'-> Bloquer la feuille
Me.Enabled = False
Screen.MousePointer = 11

'-> G�n�rer une erreur sur ANNULER
MDIMain.OpenDlg.CancelError = True
If TurbosavePath <> "" Then
    On Error Resume Next
    MDIMain.OpenDlg.InitDir = TurbosavePath
    If Dir(TurbosavePath) = "" Then MkDir TurbosavePath
End If
    
MDIMain.OpenDlg.fileName = Me.aSpool.fileName
MDIMain.OpenDlg.DefaultExt = "Turbo"
MDIMain.OpenDlg.Filter = "*.Turbo"

'-> Flags d'ouverture
MDIMain.OpenDlg.Flags = cdlOFNPathMustExist

'-> Afficher la feuille
MDIMain.OpenDlg.ShowSave

'-> quitter si pas de fichier
If MDIMain.OpenDlg.fileName = "" Then GoTo GestError

If MDIMain.OpenDlg.fileName = Me.aSpool.fileName Then GoTo GestError

'-> V�rifier que le fichier n'existe pas d�ja
If Dir$(MDIMain.OpenDlg.fileName) <> "" Then
    '-> Pointer sur la classe libell�
    Set aLb = Libelles("MDIMAIN")
    Rep = MsgBox(aLb.GetCaption(25), vbQuestion + vbYesNo, aLb.GetToolTip(25))
    If Rep = vbNo Then GoTo GestError
    '-> Supprimer le fichier
    Kill MDIMain.OpenDlg.fileName
    '-> Lib�rer le pointeur
    Set aLb = Nothing
End If

'-> Cr�er un fichier de d�tail
TempFile = CreateDetail(Me.aSpool, -1)

'-> Le renommer
Name TempFile As MDIMain.OpenDlg.fileName

GestError:

'-> D�bloquer la feuille
Me.Enabled = True
Screen.MousePointer = 0

End Sub

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
'--> on ouvre la piece jointe
ShellExecute Me.Hwnd, "Open", ButtonMenu.Tag, vbNullString, App.Path, 1

End Sub


Private Sub Toolbar1_Change()

'--> on enregistre les modifications de la barre d'outil
If topToolbar Then
    Toolbar1.SaveToolbar "TurboGraph", "TurboMain", "usr"
    topToolbar = False
End If
End Sub

Private Sub Toolbar1_DblClick()
'-> ok pour enregistrer les modifications
topToolbar = True
End Sub

Private Sub VScroll1_Change()
    Me.Page.Top = Me.VScroll1.Value
End Sub

Public Sub MouseWheel(ByVal MouseKeys As Long, ByVal Rotation As Long, ByVal Xpos As Long, ByVal Ypos As Long)
  '--> ici on va gerer les evenements sur la roulette
  Dim ctl As Control
  Dim bHandled As Boolean
  Dim bOver As Boolean
  
  For Each ctl In Controls
    ' Selon l'evenement de la souris et du control
    On Error Resume Next
    bOver = (ctl.Visible And IsOver(ctl.Hwnd, Xpos, Ypos))
    On Error GoTo 0
    
    If bOver Then
      ' c'st ok on y va
      bHandled = True
      Select Case True
      
        Case TypeOf ctl Is PictureBox
          PictureBoxScroll ctl, MouseKeys, Rotation, Xpos, Ypos
          
        Case Else
          bHandled = False

      End Select
      If bHandled Then Exit Sub
    End If
    bOver = False
  Next ctl
End Sub

Public Function Upload(ByVal strFile As String)
    Dim strHttp As String
    Dim strFileContent As String
    Dim DestUrl As URL
    
    '-> un envoi est encore en cours quitter
    If blnConnected Then Exit Function
    
    ' extract the URL using a helper function
    DestUrl = ExtractUrl(GetIniString("PARAM", "UPLOADURL", App.Path & "\Turbograph.ini", False))
    
    '-> si on a pas de site...
    If DestUrl.Host = vbNullString Then
        Trace "Invalid Host : " & GetIniString("PARAM", "UPLOADURL", App.Path & "\Turbograph.ini", False)
        Exit Function
    End If
        
    ' read the file contents as a string
    ' N.B: in HTTP everything is a string, even binary files
    strFileContent = GetFileContents(strFile)
    
    ' build the HTTP request
    strHttp = BuildFileUploadRequest(strFileContent, DestUrl, GetFileName(strFile), GetFileName(strFile), "application/octet-stream")
    
    ' assign the protocol host and port
    Winsock1.Protocol = sckTCPProtocol
    Winsock1.RemoteHost = DestUrl.Host
    
    If DestUrl.Port <> 0 Then
        Winsock1.RemotePort = DestUrl.Port
    Else
        Winsock1.RemotePort = 80
    End If
            
    ' make the connection and send the HTTP request
    Winsock1.Connect
    
    While Not blnConnected
        DoEvents
    Wend
    
    Trace "Upload : " & Now & strFile
    MDIMain.StatusBar1.Panels(1).Text = "Upload : " & Now & strFile
    Winsock1.SendData strHttp

End Function

' la reponse du serveur
Private Sub Winsock1_DataArrival(ByVal bytesTotal As Long)
    Dim strData As String
    
    Winsock1.GetData strData, vbString, bytesTotal
    Trace strData
    MDIMain.StatusBar1.Panels(1).Text = MDIMain.StatusBar1.Panels(1).Text & "  -> " & strData
    
    'MsgBox strData
    
End Sub

' il y a eu une erreur
Private Sub Winsock1_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    MsgBox Description, vbExclamation, "ERROR"
    
    Winsock1.Close
End Sub

' la connection est accept�e
Private Sub Winsock1_Connect()
    blnConnected = True
End Sub

' quand la connection est termin�e
Private Sub Winsock1_Close()
    Winsock1.Close
    blnConnected = False

End Sub

Private Function GetFileContents(ByVal strPath As String) As String
    Dim StrReturn As String
    Dim lngLength As Long
    
    lngLength = FileLen(strPath)
    StrReturn = String(lngLength, Chr(0))
    
    On Error GoTo ERR_HANDLER
    '-> Obtenir un handle de fichier et ouvrir le spool � imprimer
    hdlFile = FreeFile

    Open strPath For Binary As FreeFile
    
    Get hdlFile, , StrReturn
    
    GetFileContents = StrReturn
    
    Close #hdlFile
    
    Exit Function
    
ERR_HANDLER:
    MsgBox Err.Description, vbCritical, "ERROR"
    
    Err.Clear
End Function

