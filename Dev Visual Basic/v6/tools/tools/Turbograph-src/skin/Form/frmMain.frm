VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmMain 
   AutoRedraw      =   -1  'True
   BorderStyle     =   0  'None
   Caption         =   "Programme d'exemple d'application de Skin"
   ClientHeight    =   3975
   ClientLeft      =   150
   ClientTop       =   540
   ClientWidth     =   5835
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3975
   ScaleWidth      =   5835
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin pjt_EasySkin.comSkin comSkin1 
      Height          =   495
      Left            =   480
      TabIndex        =   0
      Top             =   600
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   873
      Text            =   "Changer de Skin"
      Fore_Color      =   -2147483640
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FontName        =   "MS Sans Serif"
      FontSize        =   8,25
   End
   Begin MSComDlg.CommonDialog Boite 
      Left            =   5160
      Top             =   360
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
 Dim Skin As New cls_Skin
 Dim frmX As Integer, frmY As Integer

Private Sub comSkin1_Click()
 On Error Resume Next
 
    Boite.Filter = "Skin (*.Ini)|*.ini"
    Boite.InitDir = App.Path
    Boite.ShowOpen
    If Err.Number <> 0 Then Exit Sub
    Skin.Open_Skin Me, Boite.FileName
    
    comSkin1.Top = Skin.Menu_PosY
    comSkin1.Left = Skin.Menu_PosX
End Sub

Private Sub Form_DblClick()
    If frmY <= Skin.HeightTitre Then If Me.WindowState = 0 Then Me.WindowState = 2 Else Me.WindowState = 0
End Sub

Private Sub Form_Load()
    Skin.Open_Skin Me, App.Path & "\skin\defaut.ini"
    Skin.Active_LimResize
    
    comSkin1.Top = Skin.Menu_PosY
    comSkin1.Left = Skin.Menu_PosX
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Skin.FormMouseDown Button, X, Y
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Skin.FormMouseMove X, Y
    frmX = X: frmY = Y
End Sub

Private Sub Form_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Skin.FormMouseUp Button, X, Y
End Sub

Private Sub Form_Resize()
    Skin.FormResize
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Skin.Desactive_LimResize
    Set Skin = Nothing
End Sub
