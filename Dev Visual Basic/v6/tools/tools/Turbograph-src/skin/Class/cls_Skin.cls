VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_Skin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'---- Declaration des APIs --------------------------------
  'API pour modifier la couleur des menus
  Private Declare Function GetSubMenu Lib "user32" (ByVal hMenu As Long, ByVal nPos As Long) As Long
  Private Declare Function DrawMenuBar Lib "user32" (ByVal hWnd As Long) As Long
  Private Declare Function GetMenu Lib "user32" (ByVal hWnd As Long) As Long
  Private Declare Function GetSystemMenu Lib "user32" (ByVal hWnd As Long, ByVal bRevert As Long) As Long
  Private Declare Function SetMenuInfo Lib "user32" (ByVal hMenu As Long, M_Info As MENUINFO) As Long
  Private Declare Function CreateSolidBrush Lib "gdi32" (ByVal crColor As Long) As Long
  Private Declare Function OleTranslateColor Lib "olepro32.dll" (ByVal OLE_COLOR As Long, ByVal HPALETTE As Long, pccolorref As Long) As Long
  
  'API pour deplacer la fen�tre
  Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
  Private Declare Sub ReleaseCapture Lib "user32" ()
  
  'API pour decouper la feuille avec une couleur et pour rendre la fenaitre transparente
  Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
  Private Declare Function SetLayeredWindowAttributes Lib "user32" (ByVal hWnd As Long, ByVal crKey As Long, ByVal bAlpha As Byte, ByVal dwFlags As Long) As Long
  Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long) As Long

  'API pour limiter le resize de la feuille
  'Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
  
  'API pour cree des menus
  Private Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
  Private Declare Function InsertMenuItem Lib "user32.dll" Alias "InsertMenuItemA" (ByVal hMenu As Long, ByVal uItem As Long, ByVal fByPosition As Long, lpmii As MENUITEMINFO) As Long
  Private Declare Function CreatePopupMenu Lib "user32" () As Long
  Private Declare Function TrackPopupMenu Lib "user32" (ByVal hMenu As Long, ByVal wFlags As enumTrackPopupMenu, ByVal X As Long, ByVal Y As Long, ByVal nReserved As Long, ByVal hWnd As Long, lprc As RECT) As Long
  Private Declare Function AppendMenu Lib "user32" Alias "AppendMenuA" (ByVal hMenu As Long, ByVal wFlags As Long, ByVal wIDNewItem As Long, ByVal lpNewItem As Any) As Long
  Private Declare Function DestroyMenu Lib "user32" (ByVal hMenu As Long) As Long

  'API pour enkever les bord des fen�tres
  Private Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
  Private Declare Function ShowWindow Lib "user32" (ByVal hWnd As Long, ByVal nCmdShow As Long) As Long
'----------------------------------------------------------

'---- Evenement des Menus ---------------------------------
  Public Event MenuClick(ByVal sItemKey As String)
'----------------------------------------------------------

'---- Declaration des Types -------------------------------
  Private Type MENUINFO
    cbSize          As Long
    fMask           As Long
    dwStyle         As Long
    cyMax           As Long
    hbrBack         As Long
    dwContextHelpID As Long
    dwMenuData      As Long
  End Type
  
  Private Type Coordoner
    X1      As String
    Y1      As String
    Width1  As String
    Height1 As String
    
    X2      As String
    Y2      As String
    Width2  As String
    Height2 As String
  End Type
  
  Private Enum enumMenuItemInfoMembers
    MIIM_STATE = &H1
    MIIM_ID = &H2
    MIIM_SUBMENU = &H4
    MIIM_CHECKMARKS = &H8
    MIIM_TYPE = &H10
    MIIM_DATA = &H20
    MIIM_STRING = &H40
    MIIM_BITMAP = &H80
    MIIM_FTYPE = &H100
  End Enum

  Public Enum enumMenuItemTypes
    MFT_STRING = &H0&
    MFT_BITMAP = &H4&
    MFT_MENUBARBREAK = &H20&
    MFT_MENUBREAK = &H40&
    MFT_OWNERDRAW = &H100&
    MFT_RADIOCHECK = &H200
    MFT_SEPARATOR = &H800&
    MFT_RIGHTORDER = &H2000
    MFT_RIGHTJUSTIFY = &H4000
  End Enum

  Public Enum enumMenuItemStates
    MFS_GRAYED = &H3
    MFS_DISABLED = MFS_GRAYED
    MFS_CHECKED = &H8&
    MFS_HILITE = &H80&
    MFS_ENABLED = &H0&
    MFS_UNCHECKED = &H0&
    MFS_UNHILITE = &H0&
    MFS_DEFAULT = &H1000
    MFS_MASK = &H108B
    MFS_HOTTRACKDRAWN = &H10000000
    MFS_CACHEDBMP = &H20000000
    MFS_BOTTOMGAPDROP = &H40000000
    MFS_TOPGAPDROP = &H80000000
    MFS_GAPDROP = &HC0000000
  End Enum

  Private Enum enumTrackPopupMenu
    TPM_CENTERALIGN = &H4
    TPM_LEFTALIGN = &H0
    TPM_RIGHTALIGN = &H8
    TPM_BOTTOMALIGN = &H20
    TPM_TOPALIGN = &H0
    TPM_VCENTERALIGN = &H10
    TPM_NONOTIFY = &H80
    TPM_RETURNCMD = &H100
    TPM_LEFTBUTTON = &H0
    TPM_RIGHTBUTTON = &H2
  End Enum

  Private Type POINTAPI
    X As Long
    Y As Long
  End Type

  Private Type MENUITEMINFO
    cbSize As Long
    fMask As enumMenuItemInfoMembers
    fType As enumMenuItemTypes
    fState As enumMenuItemStates
    wID As Long
    hSubMenu As Long
    hbmpChecked As Long
    hbmpUnchecked As Long
    dwItemData As Long
    dwTypeData As String
    cch As Long
  End Type

  Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
  End Type
'----------------------------------------------------------
 
'---- Declaration des Constantes --------------------------
  'Const pour les menus
  Private Const MIM_BACKGROUND      As Long = &H2
  Private Const MIM_APPLYTOSUBMENUS As Long = &H80000000
  
  'Const pour la zone le resize
  Private Const HTLEFT = 10
  Private Const HTRIGHT = 11
  Private Const HTTOP = 12
  Private Const HTTOPLEFT = 13
  Private Const HTTOPRIGHT = 14
  Private Const HTBOTTOM = 15
  Private Const HTBOTTOMLEFT = 16
  Private Const HTBOTTOMRIGHT = 17

  'Const pour definir la zone de resize
  Private Const MoveLeft = 5
  Private Const MoveRight = 5
  Private Const MoveTop = 5
  Private Const MoveHeight = 15
  
  'Const pour la trensparence de la fen�tre
  Private Const GWL_EXSTYLE = (-20)
  Private Const WS_EX_LAYERED = &H80000
  Private Const LWA_ALPHA = &H2
  
  'Const pour limiter le resize de la feuille
  Private Const GWL_WNDPROC = (-4)
  
  'Const pour deplacer la fen�tre
  Private Const HTCAPTION = 2
  Private Const WM_NCLBUTTONDOWN = &HA1
'----------------------------------------------------------

'---- Declaration des variables ---------------------------
  'Var pour les menu
  Private colHandles As Collection
  Private lCurItem As Long
  Private hMenu As Long
  
  'Var pour le skin
  Private Path_BMP  As String
  Private Pic       As StdPicture

  Private TailleBord  As Long
  Public HeightTitre    As Long
 
  Private Menu_Color  As Long
 
  Private Color_Trans     As Long
  Private Alpha_Trans     As Long
  Private valTransparence As Boolean
 
  Private Skin_Tab()       As Coordoner
  Private Skin_Button(6)   As String
 
  Private BMenu()     As String
  Private BMin()      As String
  Private BMax()      As String
  Private BEnd()      As String
 
  Private comMDown_Tab(3) As Coordoner
  Private comMUp_Tab(3)   As Coordoner
 
  Private vTitre(5)   As String
 
  Private MinWidth    As Single
  Private MinHeight   As Single

  Private Quitter     As Boolean
  Private Form       As Form
  
  Private ButonDown(3)  As Boolean
  
  Public Menu_PosX As Integer
  Public Menu_PosY As Integer
'----------------------------------------------------------

'---- Form Resize et form Move ----------------------------
Public Sub FormMouseDown(Button As Integer, X As Single, Y As Single)
 Dim Msg As Long
 
    If Button <> 1 Then Exit Sub
        
    If Form.WindowState = 0 Then
        If X < TailleBord And Not (Y < TailleBord Or Y > Form.ScaleHeight - TailleBord) Then Msg = HTLEFT
        If Y < TailleBord And Not (X < TailleBord Or X > Form.ScaleWidth - TailleBord) Then Msg = HTTOP
        If X > Form.ScaleWidth - TailleBord And Not (Y < TailleBord Or Y > Form.ScaleHeight - TailleBord) Then Msg = HTRIGHT
        If Y > Form.ScaleHeight - TailleBord And Not (X < TailleBord Or X > Form.ScaleWidth - TailleBord) Then Msg = HTBOTTOM
        If X < TailleBord And Y < TailleBord Then Msg = HTTOPLEFT
        If X < TailleBord And Y > Form.ScaleHeight - TailleBord Then Msg = HTBOTTOMLEFT
        If X > Form.ScaleWidth - TailleBord And Y < TailleBord Then Msg = HTTOPRIGHT
        If X > Form.ScaleWidth - TailleBord And Y > Form.ScaleHeight - TailleBord Then Msg = HTBOTTOMRIGHT
    
        Call ReleaseCapture
        Call SendMessage(Form.hWnd, WM_NCLBUTTONDOWN, Msg, 0&)
    End If

    If Y <= HeightTitre And Y > TailleBord And X > TailleBord And X < Form.Width - TailleBord Then
        If Y >= MakeValeur(BEnd(2)) And Y <= MakeValeur(BEnd(3)) And X >= MakeValeur(BEnd(0)) And X <= MakeValeur(BEnd(1)) Then
            Form.PaintPicture Pic, MakeValeur(comMDown_Tab(3).X1), MakeValeur(comMDown_Tab(3).Y1), _
                            MakeValeur(comMDown_Tab(3).Width1), MakeValeur(comMDown_Tab(3).Height1), MakeValeur(comMDown_Tab(3).X2), _
                            MakeValeur(comMDown_Tab(3).Y2), MakeValeur(comMDown_Tab(3).Width2), MakeValeur(comMDown_Tab(3).Height2)
            ButonDown(3) = True
        ElseIf Y >= MakeValeur(BMin(2)) And Y <= MakeValeur(BMin(3)) And X >= MakeValeur(BMin(0)) And X <= MakeValeur(BMin(1)) Then
            Form.PaintPicture Pic, MakeValeur(comMDown_Tab(1).X1), MakeValeur(comMDown_Tab(1).Y1), _
                            MakeValeur(comMDown_Tab(1).Width1), MakeValeur(comMDown_Tab(1).Height1), MakeValeur(comMDown_Tab(1).X2), _
                            MakeValeur(comMDown_Tab(1).Y2), MakeValeur(comMDown_Tab(1).Width2), MakeValeur(comMDown_Tab(1).Height2)
            ButonDown(1) = True
        ElseIf Y >= MakeValeur(BMax(2)) And Y <= MakeValeur(BMax(3)) And X >= MakeValeur(BMax(0)) And X <= MakeValeur(BMax(1)) Then
            Form.PaintPicture Pic, MakeValeur(comMDown_Tab(2).X1), MakeValeur(comMDown_Tab(2).Y1), _
                            MakeValeur(comMDown_Tab(2).Width1), MakeValeur(comMDown_Tab(2).Height1), MakeValeur(comMDown_Tab(2).X2), _
                            MakeValeur(comMDown_Tab(2).Y2), MakeValeur(comMDown_Tab(2).Width2), MakeValeur(comMDown_Tab(2).Height2)
            ButonDown(2) = True
        ElseIf Y >= MakeValeur(BMenu(2)) And Y <= MakeValeur(BMenu(3)) And X >= MakeValeur(BMenu(0)) And X <= MakeValeur(BMenu(1)) Then
            Form.PaintPicture Pic, MakeValeur(comMDown_Tab(0).X1), MakeValeur(comMDown_Tab(0).Y1), _
                            MakeValeur(comMDown_Tab(0).Width1), MakeValeur(comMDown_Tab(0).Height1), MakeValeur(comMDown_Tab(0).X2), _
                            MakeValeur(comMDown_Tab(0).Y2), MakeValeur(comMDown_Tab(0).Width2), MakeValeur(comMDown_Tab(0).Height2)
            ButonDown(0) = True
        Else
            Call ReleaseCapture
            Call SendMessage(Form.hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0&)
        End If
    End If
End Sub

Public Sub FormMouseMove(X As Single, Y As Single)
    If Form.WindowState <> 0 Then Exit Sub
    
    If Form.MousePointer <> 0 Then Form.MousePointer = 0
    
    If (X < TailleBord Or X > Form.ScaleWidth - TailleBord) And Not (Y < TailleBord Or Y > Form.ScaleHeight - TailleBord) Then Form.MousePointer = 9
    If (Y < TailleBord Or Y > Form.ScaleHeight - TailleBord) And Not (X < TailleBord Or X > Form.ScaleWidth - TailleBord) Then Form.MousePointer = 7
    If (X < TailleBord And Y < TailleBord) Or (X > Form.ScaleWidth - TailleBord And Y > Form.ScaleHeight - TailleBord) Then Form.MousePointer = 8
    If (X < TailleBord And Y > Form.ScaleHeight - TailleBord) Or (X > Form.ScaleWidth - TailleBord And Y < TailleBord) Then Form.MousePointer = 6

    If X > MoveLeft And Y > MoveTop And X < Form.ScaleWidth - MoveRight And Y < MoveHeight Then Form.MousePointer = 15
End Sub

Public Sub FormMouseUp(Button As Integer, X As Single, Y As Single)
    If Button <> 1 Then Exit Sub
    
    If Y >= MakeValeur(BEnd(2)) And Y <= MakeValeur(BEnd(3)) And X >= MakeValeur(BEnd(0)) And X <= MakeValeur(BEnd(1)) Or ButonDown(3) = True Then
        Form.PaintPicture Pic, MakeValeur(comMUp_Tab(3).X1), MakeValeur(comMUp_Tab(3).Y1), _
                        MakeValeur(comMUp_Tab(3).Width1), MakeValeur(comMUp_Tab(3).Height1), MakeValeur(comMUp_Tab(3).X2), _
                        MakeValeur(comMUp_Tab(3).Y2), MakeValeur(comMUp_Tab(3).Width2), MakeValeur(comMUp_Tab(3).Height2)
        If Y <= HeightTitre Then Unload Form
        ButonDown(3) = False
    ElseIf Y >= MakeValeur(BMin(2)) And Y <= MakeValeur(BMin(3)) And X >= MakeValeur(BMin(0)) And X <= MakeValeur(BMin(1)) Or ButonDown(1) = True Then
        Form.PaintPicture Pic, MakeValeur(comMUp_Tab(1).X1), MakeValeur(comMUp_Tab(1).Y1), _
                        MakeValeur(comMUp_Tab(1).Width1), MakeValeur(comMUp_Tab(1).Height1), MakeValeur(comMUp_Tab(1).X2), _
                        MakeValeur(comMUp_Tab(1).Y2), MakeValeur(comMUp_Tab(1).Width2), MakeValeur(comMUp_Tab(1).Height2)
        If Y <= HeightTitre Then Form.WindowState = 1
        ButonDown(1) = False
    ElseIf Y >= MakeValeur(BMax(2)) And Y <= MakeValeur(BMax(3)) And X >= MakeValeur(BMax(0)) And X <= MakeValeur(BMax(1)) Or ButonDown(2) = True Then
        Form.PaintPicture Pic, MakeValeur(comMUp_Tab(2).X1), MakeValeur(comMUp_Tab(2).Y1), _
                        MakeValeur(comMUp_Tab(2).Width1), MakeValeur(comMUp_Tab(2).Height1), MakeValeur(comMUp_Tab(2).X2), _
                        MakeValeur(comMUp_Tab(2).Y2), MakeValeur(comMUp_Tab(2).Width2), MakeValeur(comMUp_Tab(2).Height2)
        If Y <= HeightTitre Then If Form.WindowState = 0 Then Form.WindowState = 2 Else Form.WindowState = 0
        ButonDown(2) = False
    ElseIf Y >= MakeValeur(BMenu(2)) And Y <= MakeValeur(BMenu(3)) And X >= MakeValeur(BMenu(0)) And X <= MakeValeur(BMenu(1)) Or ButonDown(0) = True Then
        If Y <= HeightTitre Then PopupMenu Form.hWnd
        Form.PaintPicture Pic, MakeValeur(comMUp_Tab(0).X1), MakeValeur(comMUp_Tab(0).Y1), _
                        MakeValeur(comMUp_Tab(0).Width1), MakeValeur(comMUp_Tab(0).Height1), MakeValeur(comMUp_Tab(0).X2), _
                        MakeValeur(comMUp_Tab(0).Y2), MakeValeur(comMUp_Tab(0).Width2), MakeValeur(comMUp_Tab(0).Height2)
        ButonDown(0) = False
        If Quitter = True Then Unload Form
    End If
End Sub

Public Sub FormResize()
On Error Resume Next
 Dim Tmp(7) As Long, i As Long
    
    Form.Cls

    For i = 0 To UBound(Skin_Tab) 'on dessine tout se qui ne necesite pas de modification
        Tmp(0) = MakeValeur(Skin_Tab(i).X1)
        Tmp(1) = MakeValeur(Skin_Tab(i).Y1)
        Tmp(2) = MakeValeur(Skin_Tab(i).Width1)
        Tmp(3) = MakeValeur(Skin_Tab(i).Height1)
        Tmp(4) = MakeValeur(Skin_Tab(i).X2)
        Tmp(5) = MakeValeur(Skin_Tab(i).Y2)
        Tmp(6) = MakeValeur(Skin_Tab(i).Width2)
        Tmp(7) = MakeValeur(Skin_Tab(i).Height2)
        
        Form.PaintPicture Pic, Tmp(0), Tmp(1), Tmp(2), Tmp(3), Tmp(4), Tmp(5), Tmp(6), Tmp(7)
    Next i
    
    For i = 0 To UBound(comMUp_Tab) 'on dessine les boutons de la bar des titres
        Tmp(0) = MakeValeur(comMUp_Tab(i).X1)
        Tmp(1) = MakeValeur(comMUp_Tab(i).Y1)
        Tmp(2) = MakeValeur(comMUp_Tab(i).Width1)
        Tmp(3) = MakeValeur(comMUp_Tab(i).Height1)
        Tmp(4) = MakeValeur(comMUp_Tab(i).X2)
        Tmp(5) = MakeValeur(comMUp_Tab(i).Y2)
        Tmp(6) = MakeValeur(comMUp_Tab(i).Width2)
        Tmp(7) = MakeValeur(comMUp_Tab(i).Height2)
        
        Form.PaintPicture Pic, Tmp(0), Tmp(1), Tmp(2), Tmp(3), Tmp(4), Tmp(5), Tmp(6), Tmp(7)
    Next i
    
    Form.CurrentX = vTitre(1)
    Form.CurrentY = vTitre(2)
    Form.ForeColor = vTitre(0)
    Form.FontBold = vTitre(3)
    Form.FontSize = vTitre(4)
    Form.FontName = vTitre(5)
    
    If Form.TextWidth(Form.Caption) + vTitre(1) + 50 > MakeValeur(BMin(0)) Then
        For i = 0 To Len(Form.Caption) - 1
            If Form.TextWidth(Mid(Form.Caption, 1, Len(Form.Caption) - i)) < MakeValeur(BMin(0)) - vTitre(1) - 50 Then Exit For
        Next i
        Form.Print Mid(Form.Caption, 1, Len(Form.Caption) - i - 3) & "..."
    Else
        Form.Print Form.Caption
    End If
End Sub
'----------------------------------------------------------

'---- Coloration des menus --------------------------------
Private Sub Colorer_Menu(Color As Long)
    Call SetMenuColour(Form.hWnd, Color, True)
    Call SetSysMenuColour(Form.hWnd, Color)
End Sub

Private Function SetMenuColour(ByVal hWnd As Long, ByVal Color As Long, ByVal SousMenu As Boolean) As Boolean
 Dim Flags As Long, Ref As Long
 Dim M_Info As MENUINFO

    Ref = TranslateOLEtoRBG(Color)

    Flags = MIM_BACKGROUND
   
    If SousMenu Then Flags = Flags Or MIM_APPLYTOSUBMENUS

    With M_Info
        .cbSize = Len(M_Info)
        .fMask = Flags
        .hbrBack = CreateSolidBrush(Ref)
    End With

    Call SetMenuInfo(hMenu, M_Info)
    Call DrawMenuBar(hMenu)
    
    Call SetMenuInfo(GetMenu(hWnd), M_Info)
    Call DrawMenuBar(hWnd)
End Function

Private Function SetSysMenuColour(ByVal hWnd As Long, ByVal Color As Long) As Boolean
 Dim SysMenu As Long, Ref As Long
 Dim M_Info As MENUINFO

    Ref = TranslateOLEtoRBG(Color)
    SysMenu = GetSystemMenu(hWnd, False)
   
    With M_Info
        .cbSize = Len(M_Info)
        .fMask = MIM_BACKGROUND Or MIM_APPLYTOSUBMENUS
        .hbrBack = CreateSolidBrush(Ref)
    End With

    Call SetMenuInfo(SysMenu, M_Info)
    Call DrawMenuBar(SysMenu)
End Function

Private Function TranslateOLEtoRBG(ByVal dwOleColour As Long) As Long
   Call OleTranslateColor(dwOleColour, 0, TranslateOLEtoRBG)
End Function
'----------------------------------------------------------

'---- Lecture et aplication du Skin -----------------------
Public Function Open_Skin(frm As Form, Path As String) As Boolean
 Dim Ligne As String, Tmp As String
 Dim i As Integer, j As Integer, M As Integer
 Dim k As Integer, l As Integer
 
    If Dir(Path) = "" Then _
        MsgBox "Fichier de configuration du skin introuvable !", vbExclamation, "Erreur Skin.ini": Exit Function
    
    If valTransparence = True Then Call Desactive_Transparance
    
    Set Form = frm
    'Form.AutoRedraw = True
    'Form.Cls
    
    Call Hide_Bord
    
    Open Path For Input As #10
        Do While Not (EOF(1))
            Line Input #10, Ligne
            
            If Ligne <> "" And Left(Ligne, 1) <> "'" Then
                Ligne = LCase(Replace(Ligne, " ", ""))
                'Debug.Print Mid(Ligne, InStr(1, Ligne, "=") + 1)
                Select Case Left(Ligne, InStr(1, Ligne, "=") - 1)
                    Case "fichier"
                        Do
                            M = M + 1
                            If Left(Right(Path, M), 1) = "\" Then Exit Do
                        Loop
                
                        Path_BMP = Left(Path, Len(Path) - M + 1) & Mid(Ligne, InStr(1, Ligne, "=") + 1)
                        
                        If Dir(Path_BMP) = "" Then
                            MsgBox "Le Fichier Image du Skin est introuvable !", vbExclamation, _
                            "Erreur " & Mid(Ligne, InStr(1, Ligne, "=") + 1): Exit Function
                        Else
                            Set Pic = LoadPicture(Path_BMP)
                        End If
                    Case "backcolor": Form.BackColor = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                    Case "taillebord": TailleBord = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                    Case "heighttitre": HeightTitre = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                    Case "transparence"
                        Tmp = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                        
                        Alpha_Trans = 255
                        valTransparence = False
                        
                        If Left(Tmp, 4) = "true" Then
                            Tmp = Mid(Ligne, InStr(1, Ligne, ",") + 1)
                            Color_Trans = Mid(Tmp, 1, InStr(1, Tmp, ";") - 1)
                            valTransparence = True
                        End If

                        If Mid(Tmp, InStr(1, Tmp, ";") + 1, 4) = "true" Then
                            Tmp = Mid(Ligne, InStr(1, Ligne, ";") + 1)
                            If valTransparence = False Then Color_Trans = -10
                            Alpha_Trans = Mid(Tmp, InStr(1, Tmp, ",") + 1)
                            valTransparence = True
                        End If
                    Case "menucolor": Menu_Color = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                    Case "minwidth": MinWidth = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                    Case "minheight": MinHeight = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                    Case "pos"
                        ReDim Preserve Skin_Tab(i)
                            Tmp = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                            
                            With Skin_Tab(i)
                                j = InStr(1, Tmp, ",")
                                .X1 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .Y1 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .Width1 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .Height1 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .X2 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .Y2 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .Width2 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                .Height2 = Tmp
                            End With
                            
                            i = i + 1
                    Case Else
                        If Left(Ligne, 3) = "pos" Then
                            Select Case Left(Ligne, InStr(1, Ligne, "=") - 2)
                                Case "posbend": k = 3: l = 3
                                Case "posbmax": k = 2: l = 2
                                Case "posbmin": k = 1: l = 1
                                Case "posbmenu": k = 0: l = 0
                            End Select
                            
                          If Mid(Ligne, InStr(1, Ligne, "=") - 1, 1) = "1" Then
                            Tmp = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                            With comMUp_Tab(k)
                                j = InStr(1, Tmp, ",")
                                .X1 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .Y1 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .Width1 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .Height1 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .X2 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .Y2 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .Width2 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                .Height2 = Tmp
                            End With
                          Else
                            Tmp = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                            With comMDown_Tab(l)
                                j = InStr(1, Tmp, ",")
                                .X1 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .Y1 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .Width1 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .Height1 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .X2 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .Y2 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                j = InStr(1, Tmp, ",")
                                .Width2 = Mid(Tmp, 1, j - 1)
                                Tmp = Mid(Tmp, j + 1)
                                
                                .Height2 = Tmp
                            End With
                          End If
                        ElseIf Left(Ligne, 9) = "lbltitre." Then 'Pr la config du titre
                            Select Case Mid(Ligne, 10, InStr(1, Ligne, "=") - 10)
                                Case "forecolor": vTitre(0) = Mid(Ligne, InStr(1, Ligne, "=") + 1) 'ForeColor
                                Case "pos"
                                    vTitre(1) = Mid(Ligne, InStr(1, Ligne, "=") + 1, InStr(1, Ligne, ",") - 1) 'Left
                                    vTitre(2) = Mid(Ligne, InStr(1, Ligne, ",") + 1) 'Top
                                Case "font"
                                    Tmp = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                                    vTitre(3) = Mid(Tmp, 1, InStr(1, Tmp, ",") - 1) 'Metre en Gras ou pas
                                    Tmp = Mid(Ligne, InStr(1, Ligne, ",") + 1)
                                    vTitre(4) = Mid(Tmp, 1, InStr(1, Tmp, ",") - 1) 'Font size
                                    Tmp = Mid(Tmp, InStr(1, Tmp, ",") + 1)
                                    vTitre(5) = Replace(Tmp, "�", " ")              'Police
                            End Select
                        ElseIf Left(Ligne, 7) = "button." Then 'Pour la config des button
                            Select Case Mid(Ligne, 8, InStr(1, Ligne, "=") - 8)
                                Case "mdown": Skin_Button(0) = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                                Case "mup": Skin_Button(1) = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                                Case "max"
                                    Skin_Button(2) = Mid(Ligne, InStr(1, Ligne, "=") + 1, InStr(1, Ligne, ",") - InStr(1, Ligne, "=") - 1)
                                    Skin_Button(3) = Mid(Ligne, InStr(1, Ligne, ",") + 1)
                                Case "lblcolor"
                                    Skin_Button(4) = Mid(Ligne, InStr(1, Ligne, "=") + 1, InStr(1, Ligne, ",") - InStr(1, Ligne, "=") - 1)
                                    Skin_Button(5) = Mid(Ligne, InStr(1, Ligne, ",") + 1)
                                Case "lblgras": Skin_Button(6) = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                            End Select
                        ElseIf Left(Ligne, 5) = "menu." Then
                            Select Case Mid(Ligne, 6, InStr(1, Ligne, "=") - 6)
                                Case "top": Menu_PosY = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                                Case "left": Menu_PosX = Mid(Ligne, InStr(1, Ligne, "=") + 1)
                            End Select
                        Else
                            If Left(Ligne, InStr(1, Ligne, "=") - 1) = "bmenu" Then BMenu() = Split(Mid(Ligne, InStr(1, Ligne, "=") + 1), ",")
                            If Left(Ligne, InStr(1, Ligne, "=") - 1) = "bmin" Then BMin() = Split(Mid(Ligne, InStr(1, Ligne, "=") + 1), ",")
                            If Left(Ligne, InStr(1, Ligne, "=") - 1) = "bmax" Then BMax() = Split(Mid(Ligne, InStr(1, Ligne, "=") + 1), ",")
                            If Left(Ligne, InStr(1, Ligne, "=") - 1) = "bend" Then BEnd() = Split(Mid(Ligne, InStr(1, Ligne, "=") + 1), ",")
                        End If
                End Select
            End If
        Loop
    Close #10
    
    If Menu_Color <> 0 Then Call Colorer_Menu(Menu_Color) Else Call Colorer_Menu(&H80000004)
    If valTransparence = True Then Call Transparence(Alpha_Trans, Color_Trans)
    
    Call FormResize
    Call Paint_Button
    
    Open_Skin = True
End Function

Private Function Transparence(TranslucenceLevel As Long, Crk As Long) As Boolean
    Call SetWindowLong(Form.hWnd, GWL_EXSTYLE, WS_EX_LAYERED)
    If Crk >= 0 Then
        Call SetLayeredWindowAttributes(Form.hWnd, Crk, TranslucenceLevel, &H3)
    Else
        Call SetLayeredWindowAttributes(Form.hWnd, Crk, TranslucenceLevel, LWA_ALPHA)
    End If
    
    Transparence = Err.LastDllError = 0
End Function

Private Sub Desactive_Transparance()
    Call SetWindowLong(Form.hWnd, GWL_EXSTYLE, GetWindowLong(Form.hWnd, GWL_EXSTYLE) - WS_EX_LAYERED)
End Sub

Private Sub Paint_Button()
 Dim obj As Object
 
    For Each obj In Form
        If TypeOf obj Is comSkin Then
            obj.Pic1_Info Skin_Button(0)
            obj.Pic2_Info Skin_Button(1)
            
            obj.MaxWidth = Skin_Button(2)
            obj.MaxHeight = Skin_Button(3)
            
            obj.lblColorUp = CLng(Skin_Button(4))
            obj.lblColorDown = Skin_Button(5)
            obj.lblGras = Skin_Button(6)
            
            obj.Make_Skin (Path_BMP)
        End If
    Next obj
End Sub
'----------------------------------------------------------

'---- Function Divers -------------------------------------
Private Function MakeValeur(ByVal Texte As String) As String
 Dim j As Integer, i As Integer
 Dim k As Integer
 
    i = InStr(1, Texte, "me.height")
    j = InStr(1, Texte, "me.width")
       
    If i = 0 And j = 0 Then MakeValeur = Texte: Exit Function
    
    If i <> 0 Then k = k + Form.Height: Texte = Trim(Mid(Texte, i + Len("me.height")))
    If j <> 0 Then k = k + Form.Width: Texte = Trim(Mid(Texte, i + Len("me.width") + 1))
    
    Select Case Left(Texte, 1)
        Case "+": k = k + Mid(Texte, 2)
        Case "-": k = k - Mid(Texte, 2)
        Case "*": k = k * Mid(Texte, 2)
        Case "/": k = k / Mid(Texte, 2)
    End Select

    MakeValeur = k
End Function

Private Sub Hide_Bord()
 Dim dwStyle As Long, dwNewStyle(1) As Long
 
    dwStyle = GetWindowLong(Form.hWnd, (-16)) 'Lecture du Style
    dwNewStyle(0) = (dwStyle And (Not &HC00000 Or &H80000)) Or 0

    If dwStyle = dwNewStyle(0) Then Exit Sub

    'Application du nouveau Style
    Call SetWindowLong(Form.hWnd, (-16), dwNewStyle(0))
    Call SetWindowPos(Form.hWnd, 0, 0, 0, 0, 0, &H1 Or &H2 Or &H4 Or &H10 Or &H20)
    
    
    dwStyle = GetWindowLong(Form.hWnd, (-16)) 'Lecture du Style
    dwNewStyle(1) = (dwStyle And (Not &H40000)) Or 0
    
    If dwStyle = dwNewStyle(1) Then Exit Sub
    
    'Application du nouveau Style
    Call SetWindowLong(Form.hWnd, (-16), dwNewStyle(1))
    Call SetWindowPos(Form.hWnd, 0, 0, 0, 0, 0, &H1 Or &H2 Or &H4 Or &H10 Or &H20)
End Sub

'----------------------------------------------------------

'---- Limiter le Resize -----------------------------------
Public Function Active_LimResize()
 Static i As Integer
    MinX = MinWidth
    MinY = MinHeight
    
    If i = 0 Then Adress_Proc = SetWindowLong(Form.hWnd, GWL_WNDPROC, AddressOf Hook_Process)
    i = i + 1
End Function

Public Function Desactive_LimResize()
    Call SetWindowLong(Form.hWnd, GWL_WNDPROC, Adress_Proc)
End Function
'----------------------------------------------------------

'---- Creation du Menu ------------------------------------
Private Function AddItem(ByVal sKey As String, ByVal sCaption As String, Optional ByVal eType As enumMenuItemTypes = MFT_STRING, Optional ByVal eState As enumMenuItemStates = MFS_ENABLED, Optional ByVal lItemData As Long)
 Dim newMenuItem As MENUITEMINFO
 Dim lHandle As Long
 
    lCurItem = lCurItem + 1
    
    With newMenuItem
        .cbSize = Len(newMenuItem)
        .fMask = MIIM_STATE Or MIIM_ID Or MIIM_TYPE Or MIIM_DATA
        .fType = eType
        .fState = eState
        .dwItemData = lItemData
        .wID = lCurItem
        .dwTypeData = sCaption
        .cch = Len(.dwTypeData)
    End With
    
    lHandle = InsertMenuItem(hMenu, lCurItem - 1, 1, newMenuItem)
    If lHandle <> 0 Then colHandles.add sKey, "h" & lCurItem
End Function

Private Function PopupMenu(ByVal lHwnd As Long)
 Dim Pt As POINTAPI
 Dim lItem As Long
 Dim rec As RECT
    
    Call GetCursorPos(Pt)
    lItem = TrackPopupMenu(hMenu, TPM_NONOTIFY Or TPM_RETURNCMD Or TPM_LEFTALIGN, Pt.X, Pt.Y, 0, lHwnd, rec)
    
    If lItem = 0 Then Exit Function
    
    Select Case colHandles(lItem)
        Case "mRestaurer": If Form.WindowState <> 0 Then Form.WindowState = 0
        Case "mMin":  Form.WindowState = 1
        Case "mMax": If Form.WindowState = 0 Then Form.WindowState = 2

        Case "mEnd": Quitter = True
    End Select
End Function
'----------------------------------------------------------

Private Sub Class_Initialize()
    hMenu = CreatePopupMenu()
    Set colHandles = New Collection
    
    AddItem "mRestaurer", "Restaurer"
    AddItem "mMin", "R�duire"
    AddItem "mMax", "Agrandir"
    
    AddItem "", "", MFT_SEPARATOR
    AddItem "mEnd", "Fermer"
End Sub

Private Sub Class_Terminate()
    Set colHandles = Nothing
    DestroyMenu hMenu
End Sub
