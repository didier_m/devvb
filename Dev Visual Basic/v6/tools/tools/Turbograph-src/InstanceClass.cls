VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "InstanceClass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False

Option Explicit

'API pour la cr�ation dans notre cas d'un TextBox
Private Declare Function CreateWindowEx Lib "user32" Alias "CreateWindowExA" _
                (ByVal dwExStyle As Long, _
                ByVal lpClassName As String, _
                ByVal lpWindowName As String, _
                ByVal dwStyle As Long, _
                ByVal X As Long, _
                ByVal Y As Long, _
                ByVal nWidth As Long, _
                ByVal nHeight As Long, _
                ByVal hWndParent As Long, _
                ByVal hMenu As Long, _
                ByVal hInstance As Long, _
                lpParam As Any) As Long

'API pour afficher dans notre cas le TextBox cr�� par CreateWindowEx
Private Declare Function ShowWindow Lib "user32" _
                (ByVal Hwnd As Long, _
                ByVal nCmdShow As Long) As Long

'API mettre au premier plan un fen�tre
Private Declare Function SetForegroundWindow Lib "user32" _
                (ByVal Hwnd As Long) As Long

'API permettant de recherche une "fen�tre" permet dans notre cas de
'recherche si le programme est d�ja en M�moire et on r�cup�re son Hwnd
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" _
                (ByVal lpClassName As String, _
                ByVal lpWindowName As String) As Long

'API permettant d'envoyer un message vers un Handle, permet dans notre cas
'd'envoyer un texte vers le textbox de l'instance du programme trouv� en mamoire
Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" _
                (ByVal Hwnd As Long, _
                ByVal wMsg As Long, _
                ByVal wParam As Long, _
                lParam As Any) As Long

'API permettant de poster un message � l'intention d'un Hwn, permet dans notre cas
'de fermer la Previnstance de notre application
Private Declare Function PostMessage Lib "user32" Alias "PostMessageA" _
                (ByVal Hwnd As Long, _
                ByVal wMsg As Long, _
                ByVal wParam As Long, _
                lParam As Any) As Long

'-----------------------------------------------------------------------------------------
'                       CONSTANTES POUR LES API
'-----------------------------------------------------------------------------------------
Private Const WS_CHILD = &H40000000     ' Contante pour la cr�ation du textbox permet
                                        ' de rendre "enfant" le textbox de la feuille

Private Const SW_HIDE = 0               ' Constante pour l'Affichage du textbox permet
                                        ' dans notre cas d'avoir un textbox invisible

Private Const WM_SETTEXT = &HC          ' Constante utiliser avec l'API SendMessage pour
                                        ' envoyer du texte vers le textBox du programme d�ja en m�moire

Private Const WM_CLOSE = &H10           ' Constante utilis� avec l'API PostMessage pour
                                        ' fermer une application
          
'-----------------------------------------------------------------------------------------
'                       VARIABLES PRIVEES DE LA CLASSE
'-----------------------------------------------------------------------------------------
Private mFormhwnd           As Long     'Variable priv�e contenant le Hwnd de la form maitre

Private mhwndPrevInstance   As Long     'Variable priv�e contenant le Hwnd de
                                        'l'instance trouv� en m�moire du programme

Private mhwndPrevTextBox    As Long     'Variable priv�e contenant le Hwnd du
                                        'textbox de la previnstance

Private mHookTexthwnd       As Long     'Variable priv�e contenant le Hwnd du textbox cr��

Private mCommand            As String   'Variable priv�e contenant la texte qui est envoy�
                                        'au textbox de la previnstance

Private mAction             As Action   'Variable priv�e contenant l'action � faire si
                                        'une previnstance est trouv�e en m�moire

'-----------------------------------------------------------------------------------------
'                       ENUMERATION PUBLIQUES DES ACTIONS
'-----------------------------------------------------------------------------------------
Public Enum Action
    None            'Ne rien Faire
    Send_Killme     'Envoyer le texte vers la pervinstance et arr�ter cette instance
    Kill_Prev       'Arr�ter la previnstance et continuer ce programme
End Enum

'-----------------------------------------------------------------------------------------
'                       DECLARATION DE L'EVENEMENT PUBLIC
'-----------------------------------------------------------------------------------------
Public Event ReceiveOutputs(CommandOutputs As String)

'-----------------------------------------------------------------------------------------
'                       PROPRIETES DE LA CLASSE
'-----------------------------------------------------------------------------------------
'------------------------------------------------------------
' Propri�t� publique qui permet de passer ou de lire la ligne de texte
' qui est succeptible d'�tre envoyer au textbox de la previnstance
'------------------------------------------------------------
Public Property Let CommandLine(LineCommand As String)
    mCommand = LineCommand
End Property

Public Property Get CommandLine() As String
    CommandLine = mCommand
End Property

'------------------------------------------------------------
' Propri�t� publique qui permet de d�finir l'action en fonction
' du r�sultat de recherche de la prev instance
'------------------------------------------------------------
Public Property Let ActionCmd(New_Action As Action)
    mAction = New_Action
End Property

'------------------------------------------------------------
' Propri�t� publique qui permet de lire le Hwnd de la previntance
'------------------------------------------------------------
Public Property Get PrevHwnd() As Long
    PrevHwnd = mhwndPrevInstance
End Property

'------------------------------------------------------------
' Propri�t� publique qui permet de lire le Hwnd du textbox de la previntance
'------------------------------------------------------------
Public Property Get PrevTextHwnd() As Long
    PrevTextHwnd = mhwndPrevTextBox
End Property

'-----------------------------------------------------------------------------------------
'                       FUNCTION ET SUB DE LA CLASSE
'-----------------------------------------------------------------------------------------
'------------------------------------------------------------
' "Initialisation" de la classe permet de mettre en place le textbox
' et le Hook
'------------------------------------------------------------
Public Sub InitClass(ByVal Valeur As Long)
  'Sauvegarde du Hwnd de la form
    mFormhwnd = Valeur
  'Cr�ation d'un textbox qui sera utilis� pour passer les message
    mHookTexthwnd = CreateWindowEx(0, "edit", "", WS_CHILD, 0, 0, 0, 0, Valeur, 0, App.hInstance, ByVal 0&)
  'Affichage du textbox en mode cach�
    ShowWindow mHookTexthwnd, SW_HIDE
  'Mise en place du subclass pour recevoir les �v�nement du textbox que l'on vient de cr�er
    LinkCallback mHookTexthwnd, Me
End Sub

'------------------------------------------------------------
' Fin de la classe retire le hook automatiquement
'------------------------------------------------------------
Private Sub Class_Terminate()
  'Retire le subclass
    DetachCallback
End Sub

'------------------------------------------------------------
' Fin de la classe retire le hook Manuellement
'------------------------------------------------------------
Public Sub Class_Terminate98()
  'Retire le subclass
    DetachCallback
End Sub

'------------------------------------------------------------
' Appel�e par le hook lorsque le texte du textbox change
' G�n�re un �v�nement vers le programme de l'utilisateur
'------------------------------------------------------------
Friend Sub ExternCommand(Strcmd As String)
  'Re�oit du subclass le texte du subclass lorsque le texte du textbox change
  'Retransmet un �v�nement au programme principal pour l'avertir de la chose
    RaiseEvent ReceiveOutputs(Strcmd)
End Sub

'------------------------------------------------------------
' Recherche de la presence d'une previnstance
' Et r�alise les actions suivant les d�finitions
' Info :
' Ce mode de recherche de previnstance trouve aussi en m�moire
' l'instance actuelle lancer. Le test se fait sur le titre de
' l'application, c'est pour �a que l'on doit renommer cette
' instance de fa�on � ne pas "s'auto" trouver
'------------------------------------------------------------
Public Function PrevInstace() As Boolean
    Dim OldTitle    As String   'Variable de Sauvegarde du tritre
  
  'Sauvegarder le titre de l'application
    OldTitle = "Turbograph Deal Informatiqe"
  'Donner un nom Bidon � l'application
    App.Title = "Mon Instance"
    
  'Premi�re recherche
    If FindWindow("ThunderFormDC", OldTitle) <> 0 Then
      'Instance trouv�e lecture de son Hwnd
        mhwndPrevInstance = FindWindow("ThunderFormDC", OldTitle)
    End If
  'Deuxi�me recherche
    If FindWindow("ThunderRTMain", OldTitle) <> 0 Then
      'Instance trouv�e lecture de son Hwnd
        mhwndPrevInstance = FindWindow("ThunderRTMain", OldTitle)
    End If
  'Troisi�me recherche
    If FindWindow("ThunderRT5Main", OldTitle) <> 0 Then
      'Instance trouv�e lecture de son Hwnd
        mhwndPrevInstance = FindWindow("ThunderRT5Main", OldTitle)
    End If
  'Quatri�me recherche
    If FindWindow("ThunderRT6Main", OldTitle) <> 0 Then
      'Instance trouv�e lecture de son Hwnd
        mhwndPrevInstance = FindWindow("ThunderRT6Main", OldTitle)
    End If
  'Restaure son titre � l'application
  '  App.Title = OldTitle
  'Teste si la previnstance � �t� trouv�
    If mhwndPrevInstance > 0 Then
      'D�clare la pr�sence d'une instance
        PrevInstace = True
      'Previnstance trouv�e, lecture du Hwnd du textbox dans la base de registre
        mhwndPrevTextBox = GetSetting(OldTitle, "HwndInstance", "Hwnd")
      'S�lection de ce qu'on doit faire en fonction de l'action choisie
        Select Case mAction
            Case Send_Killme
              'Envoie du texte � la perv instance
                SendTexte
              'La met au premier plan
                SowPrev
              'Retire le subclass
                DetachCallback
              'Arr�t de cette instance
            Case Kill_Prev
              'Arr�t de la previnstance
                KillPrev
              'Sauvegarde du Hwnd du textbox dans la base de registre
                SaveSetting OldTitle, "HwndInstance", "Hwnd", Str$(mHookTexthwnd)
        End Select
    Else
      'Pas d'instance en cours, sauvegarde du Hwnd du textbox dans la base de registre
        SaveSetting OldTitle, "HwndInstance", "Hwnd", Str$(mHookTexthwnd)
    End If
End Function

'------------------------------------------------------------
' Fonction utiliser pour envoyer le texte vers la textBox de la prev instance
'------------------------------------------------------------
Public Function SendTexte(Optional New_Text As String = "") As Long
  'Si un texte est passer en direct on change le texte original
    If New_Text <> "" Then mCommand = New_Text
  'Si il y a bien un texte on envoie ce dernier � la previnstance
    If Len(mCommand) > 0 Then _
        SendTexte = SendMessage(mhwndPrevTextBox, WM_SETTEXT, ByVal 0&, ByVal mCommand)
End Function

'------------------------------------------------------------
' Fonction qui permer de mettre au premier plan l'intance pr�c�dente
'------------------------------------------------------------
Public Function SowPrev(Optional New_Hwnd As Long = 0) As Long
  'Si un Hwnd est pass� en direct on change le hwnd original
    If New_Hwnd <> 0 Then mhwndPrevInstance = New_Hwnd
  'Si on a bien le hwnd de la previnstance on affiche au premier plan
    If mhwndPrevInstance > 0 Then _
        SowPrev = SetForegroundWindow(mhwndPrevInstance)
End Function

'------------------------------------------------------------
' Fonction qui permer de Killer la previnstance
'------------------------------------------------------------
Public Function KillPrev(Optional New_Hwnd As Long = 0) As Long
  'Si un Hwnd est pass� en direct on change le hwnd original
    If New_Hwnd <> 0 Then mhwndPrevInstance = New_Hwnd
  'Si on a bien le hwnd de la previnstance on Kill
    If mhwndPrevInstance > 0 Then _
        KillPrev = PostMessage(mhwndPrevInstance, WM_CLOSE, 0&, 0&)
End Function


