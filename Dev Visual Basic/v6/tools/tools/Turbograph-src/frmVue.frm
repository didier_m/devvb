VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form frmVue 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   1485
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4950
   Icon            =   "frmVue.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1485
   ScaleWidth      =   4950
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Height          =   1215
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   4695
      Begin VB.CommandButton Command2 
         Caption         =   "Command2"
         Height          =   375
         Left            =   3600
         TabIndex        =   4
         Top             =   720
         Width           =   975
      End
      Begin MSComctlLib.ImageCombo Combo1 
         Height          =   330
         Left            =   1440
         TabIndex        =   0
         Top             =   360
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   582
         _Version        =   393216
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Command1"
         Height          =   375
         Left            =   2520
         TabIndex        =   3
         Top             =   720
         Width           =   975
      End
      Begin VB.Image Image1 
         Height          =   720
         Left            =   120
         Picture         =   "frmVue.frx":2B82
         Top             =   240
         Width           =   720
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   255
         Left            =   840
         TabIndex        =   2
         Top             =   360
         Width           =   3015
      End
   End
End
Attribute VB_Name = "frmVue"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public maquette As String

Private Sub Combo1_KeyUp(KeyCode As Integer, Shift As Integer)
ComboSaisieAuto Combo1, True
End Sub

Private Sub Command1_Click()

On Error Resume Next

Dim aLb As Libelle

'-> Pointer sur la classe associ�e
Set aLb = Libelles("MESSAGE")

'-> V�rifier si saisie n'est pas vide
If Trim(Me.Combo1.Text) = "" Then GoTo ErrorPage
         
'-> Renvoyer le texte � rechercher
strRetour = Me.Combo1.Text
    
'-> D�charger la feuille
Unload Me
    
    
Exit Sub

ErrorPage:
    MsgBox aLb.GetCaption(12), vbCritical + vbOKOnly, aLb.GetToolTip(12)
    Me.Combo1.Text = ""
    Me.Combo1.SetFocus


End Sub

Private Sub Command2_Click()
'--> on lance la suppression de la section
deleteSection Me.Combo1.Text
'-> on recharge la combo
LoadCombo
End Sub

Private Sub Form_Activate()

'-> selectionner eventuellement le texte
Me.Combo1.SelStart = 0
Me.Combo1.SelLength = Len(Me.Combo1.Text)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

If KeyCode = vbKeyEscape Then Unload Me

End Sub

Private Sub Form_Load()

Dim aLb As Libelle

'-> Pointer sur la classe libelles
Set aLb = Libelles("FRMVUE")

Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Command2.Caption = aLb.GetCaption(4)

'-> Pointer sur la classe libelles des boutons
Set aLb = Libelles("BOUTONS")
Me.Command1.Caption = aLb.GetCaption(1)

'-> on charge la combo
LoadCombo

End Sub

Private Sub Combo1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Command1_Click

End Sub

Private Sub LoadCombo()
'--> cette procedure permet de charger la combo
Dim Ligne As String
Dim nLigne As String

On Error GoTo GestError

'-> on vide la combo
Me.Combo1.comboItems.Clear
'-> Obtenir un handle de fichier et ouvrir le spool � imprimer
hdlFile = FreeFile
Open App.Path & "\" & iniName For Input As #hdlFile

'-> Boucle d'analyse du fichier
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    If Mid(Ligne, 1, 1) = "[" Then
        '-> on pointe ur la maquette
        Line Input #hdlFile, nLigne
        If Trim(Entry(2, nLigne, "=")) = maquette Or (maquette = "" And Trim(Entry(2, nLigne, "=")) = "Generique") Then
            Me.Combo1.comboItems.add , , LCase(Replace(Replace(Ligne, "[", ""), "]", ""))
        End If
    End If
Loop

'-> on ferme le fichier
Close #hdlFile

GestError:

End Sub

Private Sub deleteSection(sSection As String)
'--> cette procedure permet de supprimer un e vue
DeletePrivateProfileSection sSection, 0, 0, App.Path & "\" & iniName
End Sub
