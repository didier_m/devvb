VERSION 5.00
Begin VB.Form frmLoadFile 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   4080
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6870
   Icon            =   "frmLoadFile.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4080
   ScaleWidth      =   6870
   StartUpPosition =   2  'CenterScreen
   Begin VB.FileListBox File1 
      Height          =   2430
      Left            =   120
      TabIndex        =   6
      Top             =   1320
      Width           =   5775
   End
   Begin VB.PictureBox Annuler 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   6120
      Picture         =   "frmLoadFile.frx":014A
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   42
      TabIndex        =   5
      Top             =   3435
      Width           =   630
   End
   Begin VB.PictureBox Aide 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   6120
      Picture         =   "frmLoadFile.frx":118C
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   42
      TabIndex        =   4
      Top             =   2835
      Width           =   630
   End
   Begin VB.PictureBox Ok 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   6120
      Picture         =   "frmLoadFile.frx":21CE
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   42
      TabIndex        =   3
      Top             =   2280
      Width           =   630
   End
   Begin VB.TextBox Text1 
      Enabled         =   0   'False
      Height          =   285
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   5775
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Label2"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   960
      Width           =   3615
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3615
   End
End
Attribute VB_Name = "frmLoadFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Aide_Click()
    Shell App.Path & "\HelpDeal.exe DEAL|EMILIE|1|TURBO-LOADFILE", vbMaximizedFocus
End Sub

Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub File1_DblClick()
    Ok_Click
End Sub

Private Sub File1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Ok_Click
End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim lpBuffer As String
Dim Res As Long
Dim IniPath As String
Dim Extension As String

Set aLb = Libelles("FRMLOADFILE")

'-> Charger les messprogs

Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Label2.Caption = aLb.GetCaption(3)
Me.Ok.ToolTipText = aLb.GetCaption(8)
Me.Aide.ToolTipText = aLb.GetCaption(9)
Me.Annuler.ToolTipText = aLb.GetCaption(10)

'-> R�cup�ration du r�pertoire temporaire
If InStr(1, UCase$(App.Path), "\EMILIEGUI\SRC") <> 0 Then
    IniPath = "D:\ERP\TURBO"
Else
    CreatePath IniPath
End If
IniPath = IniPath & "\TurboGraph.ini"

'-> Charger le fichier
lpBuffer = Space$(500)
Res = GetPrivateProfileString("WorkDir", "Dir", "", lpBuffer, Len(lpBuffer), IniPath)
If Res = 0 Then
    '-> Impossible de trouver le r�pertoire de travail
    MsgBox aLb.GetCaption(7), vbCritical + vbOKOnly, aLb.GetToolTip(7)
    GoTo ErrorRep
End If

lpBuffer = Mid$(lpBuffer, 1, Res)

'-> Traiter le cas de la variable DEALTempo
If UCase$(lpBuffer) = "%DEALTEMPO%" Then
    '-> Il faut lire la variable d'environement DealTempo
    lpBuffer = Space$(500)
    Res = GetEnvironmentVariable("DEALTEMPO", lpBuffer, Len(lpBuffer))
    If Res = 0 Then
        MsgBox aLb.GetCaption(7), vbCritical + vbOKOnly, aLb.GetToolTip(7)
        GoTo ErrorRep
    End If
End If

'-> V�rifier que le r�pertoire existe
If Dir$(lpBuffer, vbDirectory) = "" Then
    MsgBox aLb.GetCaption(7), vbCritical + vbOKOnly, aLb.GetToolTip(7)
    GoTo ErrorRep
End If

'-> Supprimer le chr(0) li� � la fin de la requette
lpBuffer = Mid$(lpBuffer, 1, Res)

'-> Le r�pertoire existe : positionner
Me.File1.Path = lpBuffer
Me.Text1.Text = lpBuffer

'-> R�cup�rer le code op�rateur
lpBuffer = Space$(500)
Res = GetPrivateProfileString("WorkDir", "Ope", "", lpBuffer, Len(lpBuffer), IniPath)
If Res = 0 Then
    '-> Charger l'extension par d�faut
    Extension = "*.*"
End If

lpBuffer = Mid$(lpBuffer, 1, Res)
Extension = lpBuffer

Me.File1.Pattern = Extension

If Me.File1.ListCount <> 0 Then
    Me.File1.Selected(0) = True
Else
    Me.Ok.Enabled = False
End If


Exit Sub

ErrorRep:

    Me.Label1.Enabled = False
    Me.Label2.Enabled = False
    Me.File1.Enabled = False
    Me.Ok.Enabled = False
    

End Sub

Private Sub Ok_Click()

If Me.File1.FileName = "" Then Exit Sub

NomFichier = Me.File1.Path & "\" & Me.File1.List(Me.File1.ListIndex)
For i = 1 To frmVisu.Toolbar1.Buttons.Count
    frmVisu.Toolbar1.Buttons(i).Enabled = True
Next
Unload Me

InitImpression
Start

End Sub
