Attribute VB_Name = "Update"
Option Explicit

Dim TurboGraphWebSite As String

Public Sub Find_Update()
    Dim sVersion As String, iMajor As Integer, iMinor As Integer, iRevision As Integer
    Dim sTableau() As String
    Dim bUpdate As Boolean
    Dim aLb As Libelle

    '-> Pointer vers la classe des libelles
    Set aLb = Libelles("UPDATE")
    
    'On telecharge la page
    If TurboGraphWebFile = "" Then
        TurboGraphWebSite = "http://www.dealmnt.com/siexe/deal/emilie/php/ins_turbograph.txt"
    Else
        TurboGraphWebSite = TurboGraphWebFile & "/siexe/deal/emilie/php/ins_turbograph.txt"
    End If
    If DownloadPage(TurboGraphWebSite, TempDirectory & "version.tmp") Then
        'Puis on r�cup�re la version de la derni�re mise � jour
        sVersion = GetIniString(Entry(1, App.Title, " "), "Version", TempDirectory & "version.tmp", False)
        sTableau = Split(sVersion, ".")
        If UBound(sTableau) = 2 Then
            iMajor = Val(sTableau(0))
            iMinor = Val(sTableau(1))
            iRevision = Val(sTableau(2))
            'On compare la derni�re version avec celle install�e
            bUpdate = False
            If iMajor > App.Major Then
                bUpdate = True
            ElseIf iMajor = App.Major Then
                If iMinor > App.Minor Then
                    bUpdate = True
                ElseIf iMinor = App.Minor Then
                    If iRevision > App.Revision Then
                        bUpdate = True
                    End If
                End If
            End If
            'On affiche un message suivant le pr�sence d'une MAJ
            Select Case bUpdate
                Case True
                    If MsgBox(aLb.GetCaption(1) & vbCrLf & vbCrLf & "Version " & sVersion & vbCrLf & vbCrLf & aLb.GetCaption(2), vbOKCancel) = vbOK Then
                        Download_Update
                    End If
                Case False
                    MsgBox aLb.GetCaption(3), vbInformation
            End Select
        Else
            MsgBox aLb.GetCaption(4)
        End If
    Else
        MsgBox aLb.GetCaption(5), vbInformation
    End If
End Sub

Public Sub Verify_Update()
    '--> dans cette proc�dure on verifie si des mises � jour sont disponibles
    Dim sVersion As String, iMajor As Integer, iMinor As Integer, iRevision As Integer
    Dim sTableau() As String
    Dim bUpdate As Boolean
    Dim aLb As Libelle

    '-> on regarde le parametrage pour voir si on doit v�rifier les mises � jour
    Dim lpBuffer As String
    lpBuffer = GetIniString("PARAM", "UPDATE", App.Path & "\Turbograph.ini", False)
    If lpBuffer <> "NULL" Then
        If Trim(lpBuffer) = "0" Then Exit Sub
    End If

    '-> Pointer vers la classe des libelles
    Set aLb = Libelles("UPDATE")
    
    'On telecharge la page
    If TurboGraphWebFile = "" Then
        'TurboGraphWebSite = "http://www.deal.fr/menu/Ins_TurboGraph.txt"
    Else
        TurboGraphWebSite = TurboGraphWebFile & "/siexe/deal/emilie/php/Ins_TurboGraph.txt"
    End If
    If DownloadPage(TurboGraphWebSite, TempDirectory & "version.tmp") Then
        'Puis on r�cup�re la version de la derni�re mise � jour
        sVersion = GetIniString(App.Title, "Version", TempDirectory & "version.tmp", False)
        sTableau = Split(sVersion, ".")
        If UBound(sTableau) = 2 Then
            iMajor = Val(sTableau(0))
            iMinor = Val(sTableau(1))
            iRevision = Val(sTableau(2))
            'On compare la derni�re version avec celle install�e
            bUpdate = False
            If iMajor > App.Major Then
                bUpdate = True
            ElseIf iMajor = App.Major Then
                If iMinor > App.Minor Then
                    bUpdate = True
                ElseIf iMinor = App.Minor Then
                    If iRevision > App.Revision Then
                        bUpdate = True
                    End If
                End If
            End If
            'On affiche un message suivant le pr�sence d'une MAJ
            If bUpdate Then
                Select Case MsgBox(aLb.GetCaption(1) & vbCrLf & vbCrLf & "Version " & sVersion & vbCrLf & vbCrLf & aLb.GetCaption(2) & vbCrLf & aLb.GetCaption(11), vbYesNo)
                    Case vbYes
                        Download_Update
                    Case vbNo
                        '-> on ecrit dans le fichier ini comme quoi on ne veut plus verifier les updates
                        SetIniString "PARAM", "UPDATE", App.Path & "\Turbograph.ini", "0"
                End Select
            End If
        End If
    End If
End Sub

Public Sub Download_Update()
    'Pour �tre s�r que l'on est bien pass� par 'Find_Update'
    On Error GoTo GestError
    Dim aLb As Libelle

    '-> Pointer vers la classe des libelles
    Set aLb = Libelles("UPDATE")
    If TurboGraphWebFile = "" Then
        TurboGraphWebSite = "http://www.deal.fr/menu/"
    Else
        TurboGraphWebSite = TurboGraphWebFile & "/" & GetIniString(App.Title, "URL", TempDirectory & "version.tmp", False)
    End If
    If DownloadPage(TurboGraphWebSite, TempDirectory & "Ins_TurboGraph.zip") Then
        'On demande un confirmation
        If MsgBox(aLb.GetCaption(6) & vbCrLf & vbCrLf & aLb.GetCaption(7), vbQuestion Or vbYesNo, "Message") = vbNo Then
            Exit Sub
        End If
        '-> on dezip le fichier & On lance le SETUP
        Call Shell(GetUnzipFileName(TempDirectory & "Ins_TurboGraph.zip"), vbNormalFocus)
        'Et on ferme le programme
        Call Unload(MDIMain)
        End
    Else
        Call MsgBox(aLb.GetCaption(8) & vbCrLf & vbCrLf & aLb.GetCaption(9), vbExclamation Or vbOKOnly, aLb.GetCaption(10))
    End If
GestError:
End Sub

Public Function DownloadPage(ByVal URL As String, ByVal FileName As String) As Boolean
    Dim Done As Boolean, Value As Long
    On Error Resume Next
    'Si le fichier local existe d�j� on le supprime
    If Not Dir$(FileName) = vbNullString Then
        Call Kill(FileName)
    End If
    'On t�l�charge le fichier
    If TelechargeFile(URL, FileName) Then
        Done = True
    End If
    DownloadPage = Done
End Function

