VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsRange"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private pBorderLineStyle(7 To 10) As Variant
Private pBorderColorIndex(7 To 10) As Variant
Public FontName As String
Public FontSize As Integer
Public FontBold As Boolean
Public FontItalic As Boolean
Public FontUnderline As Boolean
Public BackColor As Long
Public VerticalAlignment As Variant
Public HorizontalAlignment As Variant
Public WrapText As Boolean

Public Sub SetBorderLineStyle(Border As Long, Value As Variant)
    pBorderLineStyle(Border) = Value
End Sub

Public Sub SetBorderColorIndex(Border As Long, Value As Variant)
    pBorderColorIndex(Border) = Value
End Sub

Public Function GetBorderLineStyle(Border As Long) As Variant
    GetBorderLineStyle = pBorderLineStyle(Border)
End Function

Public Function GetBorderColorIndex(Border As Long) As Variant
    GetBorderColorIndex = pBorderColorIndex(Border)
End Function

Public Function Compare(aRange As clsRange) As Boolean
Compare = False
If aRange Is Nothing Then Exit Function
If FontName <> aRange.FontName Then Exit Function
If FontSize <> aRange.FontSize Then Exit Function
If FontBold <> aRange.FontBold Then Exit Function
If FontItalic <> aRange.FontItalic Then Exit Function
If FontUnderline <> aRange.FontUnderline Then Exit Function
If BackColor <> aRange.BackColor Then Exit Function
If VerticalAlignment <> aRange.VerticalAlignment Then Exit Function
If HorizontalAlignment <> aRange.HorizontalAlignment Then Exit Function
If WrapText <> aRange.WrapText Then Exit Function

If pBorderLineStyle(7) <> aRange.GetBorderLineStyle(7) Then Exit Function
If pBorderLineStyle(8) <> aRange.GetBorderLineStyle(8) Then Exit Function
If pBorderLineStyle(9) <> aRange.GetBorderLineStyle(9) Then Exit Function
If pBorderLineStyle(10) <> aRange.GetBorderLineStyle(10) Then Exit Function

If pBorderColorIndex(7) <> aRange.GetBorderColorIndex(7) Then Exit Function
If pBorderColorIndex(8) <> aRange.GetBorderColorIndex(8) Then Exit Function
If pBorderColorIndex(9) <> aRange.GetBorderColorIndex(9) Then Exit Function
If pBorderColorIndex(10) <> aRange.GetBorderColorIndex(10) Then Exit Function

Compare = True
End Function
