VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form frmConfig 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Configuration"
   ClientHeight    =   6705
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9870
   Icon            =   "frmConfig.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6705
   ScaleWidth      =   9870
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command5 
      Height          =   615
      Left            =   9120
      Picture         =   "frmConfig.frx":08CA
      Style           =   1  'Graphical
      TabIndex        =   25
      Top             =   6000
      Width           =   615
   End
   Begin VB.CommandButton Command4 
      Height          =   615
      Left            =   7680
      Picture         =   "frmConfig.frx":1594
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   6000
      Width           =   615
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5775
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   9615
      _ExtentX        =   16960
      _ExtentY        =   10186
      _Version        =   393216
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   970
      TabCaption(0)   =   "G�n�ral"
      TabPicture(0)   =   "frmConfig.frx":3906
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label6"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label5"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label4"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label2"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Line1"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label10"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label11"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Label12"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Label13"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Label14"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Label20"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Label22"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Label59"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "Label60"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "Label62"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "Label63"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "Label64"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "Check5"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Check4"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "Check3"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "Text1"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "Command2"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "CheckExcel"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "CheckOpenOffice"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "CheckOutlook"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "Command6"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "Command8"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "Command10"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).Control(28)=   "CheckLotusNotes"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "Check8"
      Tab(0).Control(29).Enabled=   0   'False
      Tab(0).Control(30)=   "Text3"
      Tab(0).Control(30).Enabled=   0   'False
      Tab(0).Control(31)=   "Command13"
      Tab(0).Control(31).Enabled=   0   'False
      Tab(0).Control(32)=   "Text16"
      Tab(0).Control(32).Enabled=   0   'False
      Tab(0).Control(33)=   "Text17"
      Tab(0).Control(33).Enabled=   0   'False
      Tab(0).Control(34)=   "Check29"
      Tab(0).Control(34).Enabled=   0   'False
      Tab(0).Control(35)=   "Check30"
      Tab(0).Control(35).Enabled=   0   'False
      Tab(0).Control(36)=   "Check31"
      Tab(0).Control(36).Enabled=   0   'False
      Tab(0).ControlCount=   37
      TabCaption(1)   =   "Impression"
      TabPicture(1)   =   "frmConfig.frx":3922
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Check32"
      Tab(1).Control(1)=   "Check28"
      Tab(1).Control(2)=   "Combo1"
      Tab(1).Control(3)=   "Check1"
      Tab(1).Control(4)=   "Label68"
      Tab(1).Control(5)=   "Label61"
      Tab(1).Control(6)=   "label"
      Tab(1).Control(7)=   "Label1"
      Tab(1).ControlCount=   8
      TabCaption(2)   =   "PDF"
      TabPicture(2)   =   "frmConfig.frx":393E
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Check33"
      Tab(2).Control(1)=   "CheckPDFCreatorSetting"
      Tab(2).Control(2)=   "Check26"
      Tab(2).Control(3)=   "Check25"
      Tab(2).Control(4)=   "Text15"
      Tab(2).Control(5)=   "Text14"
      Tab(2).Control(6)=   "Command14"
      Tab(2).Control(7)=   "Text12"
      Tab(2).Control(8)=   "Command11"
      Tab(2).Control(9)=   "Check20"
      Tab(2).Control(10)=   "Check19"
      Tab(2).Control(11)=   "Check18"
      Tab(2).Control(12)=   "Check17"
      Tab(2).Control(13)=   "Check16"
      Tab(2).Control(14)=   "Check15"
      Tab(2).Control(15)=   "Check14"
      Tab(2).Control(16)=   "Check13"
      Tab(2).Control(17)=   "Check12"
      Tab(2).Control(18)=   "Check11"
      Tab(2).Control(19)=   "Text11"
      Tab(2).Control(20)=   "Text10"
      Tab(2).Control(21)=   "Text9"
      Tab(2).Control(22)=   "Text8"
      Tab(2).Control(23)=   "Text7"
      Tab(2).Control(24)=   "Text6"
      Tab(2).Control(25)=   "Check10"
      Tab(2).Control(26)=   "Check9"
      Tab(2).Control(27)=   "Text5"
      Tab(2).Control(28)=   "Text4"
      Tab(2).Control(29)=   "CheckPDFCreator"
      Tab(2).Control(30)=   "CheckWin2pdf"
      Tab(2).Control(31)=   "CheckPDFWriter"
      Tab(2).Control(32)=   "CheckDistiller"
      Tab(2).Control(33)=   "CheckPDF"
      Tab(2).Control(34)=   "Command7"
      Tab(2).Control(35)=   "Text2"
      Tab(2).Control(36)=   "Check2"
      Tab(2).Control(37)=   "Label69"
      Tab(2).Control(38)=   "Label67"
      Tab(2).Control(39)=   "Label57"
      Tab(2).Control(40)=   "Label56"
      Tab(2).Control(41)=   "Label55"
      Tab(2).Control(42)=   "Label54"
      Tab(2).Control(43)=   "Label47"
      Tab(2).Control(44)=   "Label46"
      Tab(2).Control(45)=   "Label45"
      Tab(2).Control(46)=   "Label44"
      Tab(2).Control(47)=   "Label43"
      Tab(2).Control(48)=   "Label42"
      Tab(2).Control(49)=   "Label41"
      Tab(2).Control(50)=   "Label40"
      Tab(2).Control(51)=   "Label39"
      Tab(2).Control(52)=   "Label38"
      Tab(2).Control(53)=   "Label37"
      Tab(2).Control(54)=   "Label36"
      Tab(2).Control(55)=   "Label35"
      Tab(2).Control(56)=   "Label34"
      Tab(2).Control(57)=   "Label33"
      Tab(2).Control(58)=   "Label32"
      Tab(2).Control(59)=   "Label31"
      Tab(2).Control(60)=   "Label30"
      Tab(2).Control(61)=   "Label29"
      Tab(2).Control(62)=   "Label25"
      Tab(2).Control(63)=   "Label24"
      Tab(2).Control(64)=   "Label23"
      Tab(2).Control(65)=   "Line2"
      Tab(2).Control(66)=   "Label15"
      Tab(2).Control(67)=   "Label16"
      Tab(2).Control(68)=   "Label17"
      Tab(2).Control(69)=   "Label18"
      Tab(2).Control(70)=   "Label19"
      Tab(2).Control(71)=   "Label3"
      Tab(2).Control(72)=   "Label9"
      Tab(2).ControlCount=   73
      TabCaption(3)   =   "Debbug"
      TabPicture(3)   =   "frmConfig.frx":395A
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Label7"
      Tab(3).Control(1)=   "LabelMouchard"
      Tab(3).Control(2)=   "Label21"
      Tab(3).Control(3)=   "Label26"
      Tab(3).Control(4)=   "Label27"
      Tab(3).Control(5)=   "Label28"
      Tab(3).Control(6)=   "Label58"
      Tab(3).Control(7)=   "Command3"
      Tab(3).Control(8)=   "Check6"
      Tab(3).Control(9)=   "RichTextBox1"
      Tab(3).Control(10)=   "Check7"
      Tab(3).Control(11)=   "Slider1"
      Tab(3).Control(12)=   "Check27"
      Tab(3).ControlCount=   13
      TabCaption(4)   =   "Courriel/HTTP"
      TabPicture(4)   =   "frmConfig.frx":3976
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Text18"
      Tab(4).Control(1)=   "Check24"
      Tab(4).Control(2)=   "Check23"
      Tab(4).Control(3)=   "Check22"
      Tab(4).Control(4)=   "Check21"
      Tab(4).Control(5)=   "Text13"
      Tab(4).Control(6)=   "Command12"
      Tab(4).Control(7)=   "Command9"
      Tab(4).Control(8)=   "SMTP"
      Tab(4).Control(9)=   "Winsock1"
      Tab(4).Control(10)=   "ImageCombo1"
      Tab(4).Control(11)=   "ImageList2"
      Tab(4).Control(12)=   "Label66"
      Tab(4).Control(13)=   "Label65"
      Tab(4).Control(14)=   "Label53"
      Tab(4).Control(15)=   "Label52"
      Tab(4).Control(16)=   "Label51"
      Tab(4).Control(17)=   "Label50"
      Tab(4).Control(18)=   "Label49"
      Tab(4).Control(19)=   "Label48"
      Tab(4).Control(20)=   "Label8"
      Tab(4).ControlCount=   21
      Begin VB.CheckBox Check33 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -71160
         TabIndex        =   150
         Top             =   2400
         Width           =   255
      End
      Begin VB.CheckBox Check32 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -72600
         TabIndex        =   148
         Top             =   2280
         Width           =   255
      End
      Begin VB.CheckBox CheckPDFCreatorSetting 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -67920
         TabIndex        =   146
         Top             =   4920
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.TextBox Text18 
         Height          =   285
         Left            =   -72120
         TabIndex        =   142
         ToolTipText     =   "exemple : smtp.mail.yahoo.fr"
         Top             =   3600
         Width           =   6015
      End
      Begin VB.CheckBox Check31 
         Caption         =   "Check1"
         Height          =   255
         Left            =   8400
         TabIndex        =   140
         Top             =   1440
         Width           =   255
      End
      Begin VB.CheckBox Check30 
         Caption         =   "Check1"
         Height          =   255
         Left            =   6960
         TabIndex        =   138
         Top             =   4200
         Width           =   255
      End
      Begin VB.CheckBox Check29 
         Caption         =   "Check1"
         Height          =   255
         Left            =   6960
         TabIndex        =   136
         Top             =   3840
         Width           =   255
      End
      Begin VB.CheckBox Check28 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -72600
         TabIndex        =   134
         Top             =   1920
         Width           =   255
      End
      Begin VB.TextBox Text17 
         Height          =   285
         Left            =   7920
         TabIndex        =   132
         Top             =   1080
         Width           =   735
      End
      Begin VB.TextBox Text16 
         Height          =   285
         Left            =   4320
         TabIndex        =   130
         Top             =   2880
         Width           =   4335
      End
      Begin VB.CheckBox Check27 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -70200
         TabIndex        =   128
         Top             =   5400
         Value           =   1  'Checked
         Width           =   255
      End
      Begin VB.CheckBox Check26 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -65820
         TabIndex        =   126
         Top             =   2400
         Width           =   255
      End
      Begin VB.CheckBox Check25 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -69600
         TabIndex        =   124
         Top             =   2400
         Width           =   255
      End
      Begin VB.TextBox Text15 
         Height          =   285
         Left            =   -72600
         TabIndex        =   122
         Top             =   3480
         Width           =   2655
      End
      Begin VB.TextBox Text14 
         Height          =   285
         Left            =   -72600
         TabIndex        =   120
         Top             =   3120
         Width           =   2655
      End
      Begin VB.CommandButton Command14 
         Caption         =   "Autoriser l'ex�cution de TurboPDF"
         Height          =   615
         Left            =   -72960
         TabIndex        =   119
         Top             =   5040
         Width           =   1695
      End
      Begin VB.CheckBox Check24 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -73680
         TabIndex        =   113
         Top             =   2400
         Width           =   255
      End
      Begin VB.CheckBox Check23 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -73680
         TabIndex        =   112
         Top             =   2760
         Width           =   255
      End
      Begin VB.CheckBox Check22 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -71520
         TabIndex        =   111
         Top             =   2760
         Width           =   255
      End
      Begin VB.CheckBox Check21 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -71520
         TabIndex        =   110
         Top             =   2400
         Width           =   255
      End
      Begin VB.CommandButton Command13 
         Caption         =   "Mettre � jour mes images "
         Height          =   615
         Left            =   2040
         TabIndex        =   109
         Top             =   4920
         Width           =   1695
      End
      Begin VB.TextBox Text13 
         Height          =   285
         Left            =   -72120
         TabIndex        =   107
         ToolTipText     =   "exemple : smtp.mail.yahoo.fr"
         Top             =   1560
         Width           =   1695
      End
      Begin VB.CommandButton Command12 
         Caption         =   "Tester l'envoi d'email"
         Height          =   615
         Left            =   -72960
         TabIndex        =   106
         Top             =   4920
         Width           =   1695
      End
      Begin VB.TextBox Text12 
         Height          =   285
         Left            =   -67680
         TabIndex        =   104
         Top             =   2040
         Width           =   2055
      End
      Begin VB.CommandButton Command11 
         Caption         =   "..."
         Height          =   255
         Left            =   -70200
         TabIndex        =   103
         Top             =   975
         Width           =   255
      End
      Begin VB.CheckBox Check20 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -67680
         TabIndex        =   101
         Top             =   3480
         Width           =   255
      End
      Begin VB.CheckBox Check19 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -69600
         TabIndex        =   99
         Top             =   3480
         Width           =   255
      End
      Begin VB.CheckBox Check18 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -67680
         TabIndex        =   97
         Top             =   3120
         Width           =   255
      End
      Begin VB.CheckBox Check17 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -69600
         TabIndex        =   95
         Top             =   3120
         Width           =   255
      End
      Begin VB.CheckBox Check16 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -67680
         TabIndex        =   93
         Top             =   2760
         Width           =   255
      End
      Begin VB.CheckBox Check15 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -69600
         TabIndex        =   91
         Top             =   2760
         Width           =   255
      End
      Begin VB.CheckBox Check14 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -72600
         TabIndex        =   89
         Top             =   2760
         Width           =   255
      End
      Begin VB.CheckBox Check13 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -74760
         TabIndex        =   87
         Top             =   2760
         Width           =   255
      End
      Begin VB.CheckBox Check12 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -72600
         TabIndex        =   85
         Top             =   2400
         Width           =   255
      End
      Begin VB.CheckBox Check11 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -74760
         TabIndex        =   83
         Top             =   2400
         Width           =   255
      End
      Begin VB.TextBox Text11 
         Height          =   285
         Left            =   -67680
         TabIndex        =   81
         Top             =   1320
         Width           =   2055
      End
      Begin VB.TextBox Text10 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   -71160
         PasswordChar    =   "*"
         TabIndex        =   80
         Top             =   2040
         Width           =   1215
      End
      Begin VB.TextBox Text9 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   -72600
         PasswordChar    =   "*"
         TabIndex        =   78
         Top             =   2040
         Width           =   1215
      End
      Begin VB.TextBox Text8 
         Height          =   285
         Left            =   -67680
         TabIndex        =   76
         Top             =   1680
         Width           =   2055
      End
      Begin VB.TextBox Text7 
         Height          =   285
         Left            =   -67680
         TabIndex        =   74
         Top             =   960
         Width           =   2055
      End
      Begin VB.TextBox Text6 
         Height          =   285
         Left            =   -72600
         TabIndex        =   72
         Top             =   960
         Width           =   2295
      End
      Begin VB.CheckBox Check10 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -70160
         TabIndex        =   70
         Top             =   1320
         Width           =   255
      End
      Begin VB.CheckBox Check9 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -72600
         TabIndex        =   68
         Top             =   1320
         Width           =   255
      End
      Begin VB.TextBox Text5 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   -71160
         PasswordChar    =   "*"
         TabIndex        =   67
         Top             =   1680
         Width           =   1215
      End
      Begin VB.TextBox Text4 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   -72600
         PasswordChar    =   "*"
         TabIndex        =   65
         Top             =   1680
         Width           =   1215
      End
      Begin MSComctlLib.Slider Slider1 
         Height          =   495
         Left            =   -70920
         TabIndex        =   61
         Top             =   1200
         Width           =   3015
         _ExtentX        =   5318
         _ExtentY        =   873
         _Version        =   393216
         Min             =   1
         Max             =   5
         SelStart        =   1
         Value           =   1
         TextPosition    =   1
      End
      Begin VB.TextBox Text3 
         Height          =   285
         Left            =   1680
         TabIndex        =   56
         Top             =   2880
         Width           =   735
      End
      Begin VB.CheckBox Check8 
         Caption         =   "Check1"
         Height          =   255
         Left            =   4320
         TabIndex        =   54
         Top             =   2520
         Width           =   255
      End
      Begin VB.CheckBox CheckLotusNotes 
         Caption         =   "Check1"
         Enabled         =   0   'False
         Height          =   255
         Left            =   3480
         TabIndex        =   52
         Top             =   3840
         Width           =   255
      End
      Begin VB.CheckBox Check7 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -74760
         TabIndex        =   50
         Top             =   5400
         Value           =   1  'Checked
         Width           =   255
      End
      Begin VB.CheckBox CheckPDFCreator 
         Caption         =   "Check1"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -67920
         TabIndex        =   44
         Top             =   4680
         Width           =   255
      End
      Begin VB.CheckBox CheckWin2pdf 
         Caption         =   "Check1"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -69840
         TabIndex        =   43
         Top             =   4680
         Width           =   255
      End
      Begin VB.CheckBox CheckPDFWriter 
         Caption         =   "Check1"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -71640
         TabIndex        =   42
         Top             =   4680
         Width           =   255
      End
      Begin VB.CheckBox CheckDistiller 
         Caption         =   "Check1"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -65880
         TabIndex        =   41
         Top             =   4680
         Width           =   255
      End
      Begin VB.CheckBox CheckPDF 
         Caption         =   "Check1"
         Enabled         =   0   'False
         Height          =   255
         Left            =   -73680
         TabIndex        =   40
         Top             =   4680
         Width           =   255
      End
      Begin VB.CommandButton Command10 
         Caption         =   "Restaurer le fichier de param�trage"
         Enabled         =   0   'False
         Height          =   615
         Left            =   7440
         TabIndex        =   39
         Top             =   4920
         Width           =   1695
      End
      Begin RichTextLib.RichTextBox RichTextBox1 
         Height          =   3495
         Left            =   -74760
         TabIndex        =   37
         Top             =   1800
         Width           =   9135
         _ExtentX        =   16113
         _ExtentY        =   6165
         _Version        =   393217
         ScrollBars      =   2
         TextRTF         =   $"frmConfig.frx":3992
      End
      Begin VB.CommandButton Command9 
         Caption         =   "Tester la connection"
         Height          =   615
         Left            =   -74760
         TabIndex        =   36
         Top             =   4920
         Width           =   1695
      End
      Begin VB.CommandButton Command8 
         Caption         =   "Sauvegarder le fichier de param�trage"
         Height          =   615
         Left            =   5640
         TabIndex        =   35
         Top             =   4920
         Width           =   1695
      End
      Begin VB.CommandButton Command7 
         Caption         =   "Tester turboPDF"
         Height          =   615
         Left            =   -74760
         TabIndex        =   34
         Top             =   5040
         Width           =   1695
      End
      Begin VB.CommandButton Command6 
         Caption         =   "V�rifier les mises � jour"
         Height          =   615
         Left            =   240
         TabIndex        =   33
         Top             =   4920
         Width           =   1695
      End
      Begin VB.CheckBox CheckOutlook 
         Caption         =   "Check1"
         Enabled         =   0   'False
         Height          =   255
         Left            =   1320
         TabIndex        =   31
         Top             =   4560
         Width           =   255
      End
      Begin VB.CheckBox CheckOpenOffice 
         Caption         =   "Check1"
         Enabled         =   0   'False
         Height          =   255
         Left            =   1320
         TabIndex        =   29
         Top             =   4200
         Width           =   255
      End
      Begin VB.CheckBox CheckExcel 
         Caption         =   "Check1"
         Enabled         =   0   'False
         Height          =   255
         Left            =   1320
         TabIndex        =   27
         Top             =   3840
         Width           =   255
      End
      Begin VB.TextBox SMTP 
         Height          =   285
         Left            =   -72120
         TabIndex        =   22
         ToolTipText     =   "exemple : smtp.mail.yahoo.fr"
         Top             =   1080
         Width           =   4335
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Supprimer les images temporaires"
         Height          =   615
         Left            =   3840
         TabIndex        =   21
         Top             =   4920
         Width           =   1695
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   4320
         TabIndex        =   19
         Top             =   2160
         Width           =   4335
      End
      Begin VB.CheckBox Check6 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -73200
         TabIndex        =   17
         Top             =   720
         Width           =   255
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Afficher le fichier trace"
         Height          =   615
         Left            =   -74760
         TabIndex        =   16
         Top             =   1080
         Width           =   1095
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   -69960
         TabIndex        =   13
         Top             =   5280
         Visible         =   0   'False
         Width           =   4335
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -65880
         TabIndex        =   12
         Top             =   4320
         Width           =   255
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Check1"
         Height          =   255
         Left            =   4320
         TabIndex        =   8
         Top             =   1080
         Width           =   255
      End
      Begin VB.CheckBox Check4 
         Caption         =   "Check1"
         Height          =   255
         Left            =   4320
         TabIndex        =   7
         Top             =   1440
         Width           =   255
      End
      Begin VB.CheckBox Check5 
         Caption         =   "Check1"
         Height          =   255
         Left            =   4320
         TabIndex        =   6
         Top             =   1800
         Width           =   255
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   -72600
         TabIndex        =   3
         Top             =   1080
         Width           =   4320
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Left            =   -72600
         TabIndex        =   2
         Top             =   1560
         Width           =   255
      End
      Begin MSWinsockLib.Winsock Winsock1 
         Left            =   -66840
         Top             =   1920
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   393216
      End
      Begin MSComctlLib.ImageCombo ImageCombo1 
         Height          =   330
         Left            =   -68880
         TabIndex        =   144
         Top             =   2400
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   582
         _Version        =   393216
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Text            =   "ImageCombo1"
         ImageList       =   "ImageList2"
      End
      Begin MSComctlLib.ImageList ImageList2 
         Left            =   -69720
         Top             =   2640
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   7
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConfig.frx":3A14
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConfig.frx":3D2E
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConfig.frx":4048
               Key             =   "LOTUS"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConfig.frx":6182
               Key             =   "OUTLOOK"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConfig.frx":6A5C
               Key             =   "WWW"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConfig.frx":98EE
               Key             =   "PJ"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmConfig.frx":A1C8
               Key             =   "SMTP"
            EndProperty
         EndProperty
      End
      Begin VB.Label Label69 
         BackStyle       =   0  'Transparent
         Caption         =   "RTF-> Img"
         Height          =   255
         Left            =   -70800
         TabIndex        =   151
         ToolTipText     =   "Les cadres de texte sont transform�s en image (plus rapide)"
         Top             =   2400
         Width           =   1215
      End
      Begin VB.Label Label68 
         BackStyle       =   0  'Transparent
         Caption         =   "Local uniquement"
         Height          =   375
         Left            =   -74760
         TabIndex        =   149
         Top             =   2280
         Width           =   2175
      End
      Begin VB.Label Label67 
         BackStyle       =   0  'Transparent
         Caption         =   "safe setting"
         Height          =   255
         Left            =   -69000
         TabIndex        =   147
         Top             =   4920
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Label Label66 
         BackStyle       =   0  'Transparent
         Caption         =   "Messagerie par d�faut"
         Height          =   255
         Left            =   -70680
         TabIndex        =   145
         Top             =   2400
         Width           =   2175
      End
      Begin VB.Label Label65 
         BackStyle       =   0  'Transparent
         Caption         =   "URL de l'upload du serveur"
         Height          =   255
         Left            =   -74760
         TabIndex        =   143
         Top             =   3600
         Width           =   2175
      End
      Begin VB.Label Label64 
         BackStyle       =   0  'Transparent
         Caption         =   "Ouvrir les spools dans la m�me instance"
         Height          =   255
         Left            =   4800
         TabIndex        =   141
         Top             =   1440
         Width           =   3495
      End
      Begin VB.Label Label63 
         BackStyle       =   0  'Transparent
         Caption         =   "Test existence mail"
         Height          =   255
         Left            =   5160
         TabIndex        =   139
         Top             =   4200
         Width           =   1575
      End
      Begin VB.Label Label62 
         BackStyle       =   0  'Transparent
         Caption         =   "Test existence tableur"
         Height          =   255
         Left            =   5160
         TabIndex        =   137
         Top             =   3840
         Width           =   1815
      End
      Begin VB.Label Label61 
         BackStyle       =   0  'Transparent
         Caption         =   "Sans la page de selection"
         Height          =   375
         Left            =   -74760
         TabIndex        =   135
         Top             =   1920
         Width           =   2175
      End
      Begin VB.Label Label60 
         BackStyle       =   0  'Transparent
         Caption         =   "Conserver les spools au maximum (en jours)"
         Height          =   255
         Left            =   4800
         TabIndex        =   133
         Top             =   1080
         Width           =   3975
      End
      Begin VB.Label Label59 
         BackStyle       =   0  'Transparent
         Caption         =   "Sauvegader dans"
         Height          =   255
         Left            =   2760
         TabIndex        =   131
         Top             =   2880
         Width           =   3975
      End
      Begin VB.Label Label58 
         BackStyle       =   0  'Transparent
         Caption         =   "Afficher au premier plan"
         Height          =   255
         Left            =   -69840
         TabIndex        =   129
         Top             =   5400
         Width           =   2895
      End
      Begin VB.Label Label57 
         BackStyle       =   0  'Transparent
         Caption         =   "Confirmation"
         Height          =   255
         Left            =   -66960
         TabIndex        =   127
         Top             =   2400
         Width           =   975
      End
      Begin VB.Label Label56 
         BackStyle       =   0  'Transparent
         Caption         =   "Imprimer � l'ouverture"
         Height          =   255
         Left            =   -69240
         TabIndex        =   125
         Top             =   2400
         Width           =   3015
      End
      Begin VB.Label Label55 
         BackStyle       =   0  'Transparent
         Caption         =   "Mots cl�s du document"
         Height          =   255
         Left            =   -74760
         TabIndex        =   123
         Top             =   3480
         Width           =   2175
      End
      Begin VB.Label Label54 
         BackStyle       =   0  'Transparent
         Caption         =   "Sujet du document"
         Height          =   255
         Left            =   -74760
         TabIndex        =   121
         Top             =   3120
         Width           =   2175
      End
      Begin VB.Label Label53 
         Caption         =   "Pi�ce jointe par d�faut"
         Height          =   255
         Left            =   -74760
         TabIndex        =   118
         Top             =   2040
         Width           =   3615
      End
      Begin VB.Label Label52 
         BackStyle       =   0  'Transparent
         Caption         =   "Turbo"
         Height          =   255
         Left            =   -74760
         TabIndex        =   117
         Top             =   2400
         Width           =   975
      End
      Begin VB.Label Label51 
         BackStyle       =   0  'Transparent
         Caption         =   "PDF"
         Height          =   255
         Left            =   -74760
         TabIndex        =   116
         Top             =   2760
         Width           =   975
      End
      Begin VB.Label Label50 
         BackStyle       =   0  'Transparent
         Caption         =   "Aucune"
         Height          =   255
         Left            =   -72600
         TabIndex        =   115
         Top             =   2760
         Width           =   975
      End
      Begin VB.Label Label49 
         BackStyle       =   0  'Transparent
         Caption         =   "Html"
         Height          =   255
         Left            =   -72600
         TabIndex        =   114
         Top             =   2400
         Width           =   975
      End
      Begin VB.Label Label48 
         BackStyle       =   0  'Transparent
         Caption         =   "D�lai d'attente � la connection (ms)"
         Height          =   255
         Left            =   -74760
         TabIndex        =   108
         Top             =   1560
         Width           =   2535
      End
      Begin VB.Label Label47 
         BackStyle       =   0  'Transparent
         Caption         =   "Libell� du site"
         Height          =   255
         Left            =   -69600
         TabIndex        =   105
         Top             =   2040
         Width           =   2175
      End
      Begin VB.Label Label46 
         BackStyle       =   0  'Transparent
         Caption         =   "Centrer la fen�tre"
         Height          =   255
         Left            =   -67320
         TabIndex        =   102
         Top             =   3480
         Width           =   1935
      End
      Begin VB.Label Label45 
         BackStyle       =   0  'Transparent
         Caption         =   "Ajuster"
         Height          =   255
         Left            =   -69240
         TabIndex        =   100
         Top             =   3480
         Width           =   1935
      End
      Begin VB.Label Label44 
         BackStyle       =   0  'Transparent
         Caption         =   "Par deux pages"
         Height          =   255
         Left            =   -67320
         TabIndex        =   98
         Top             =   3120
         Width           =   1935
      End
      Begin VB.Label Label43 
         BackStyle       =   0  'Transparent
         Caption         =   "Page � page"
         Height          =   255
         Left            =   -69240
         TabIndex        =   96
         Top             =   3120
         Width           =   1935
      End
      Begin VB.Label Label42 
         BackStyle       =   0  'Transparent
         Caption         =   "Plein �cran"
         Height          =   255
         Left            =   -67320
         TabIndex        =   94
         Top             =   2760
         Width           =   1935
      End
      Begin VB.Label Label41 
         BackStyle       =   0  'Transparent
         Caption         =   "Cacher le menu"
         Height          =   255
         Left            =   -69240
         TabIndex        =   92
         Top             =   2760
         Width           =   1935
      End
      Begin VB.Label Label40 
         BackStyle       =   0  'Transparent
         Caption         =   "Copie"
         Height          =   255
         Left            =   -72240
         TabIndex        =   90
         Top             =   2760
         Width           =   1935
      End
      Begin VB.Label Label39 
         BackStyle       =   0  'Transparent
         Caption         =   "Modification"
         Height          =   255
         Left            =   -74400
         TabIndex        =   88
         Top             =   2760
         Width           =   1935
      End
      Begin VB.Label Label38 
         BackStyle       =   0  'Transparent
         Caption         =   "Impression"
         Height          =   255
         Left            =   -72240
         TabIndex        =   86
         Top             =   2400
         Width           =   1935
      End
      Begin VB.Label Label37 
         BackStyle       =   0  'Transparent
         Caption         =   "Lecture seule"
         Height          =   255
         Left            =   -74400
         TabIndex        =   84
         Top             =   2400
         Width           =   1935
      End
      Begin VB.Label Label36 
         BackStyle       =   0  'Transparent
         Caption         =   "Titre du document"
         Height          =   255
         Left            =   -69600
         TabIndex        =   82
         Top             =   1320
         Width           =   2175
      End
      Begin VB.Label Label35 
         BackStyle       =   0  'Transparent
         Caption         =   "Mot de passe administrateur"
         Height          =   255
         Left            =   -74760
         TabIndex        =   79
         Top             =   2040
         Width           =   2175
      End
      Begin VB.Label Label34 
         BackStyle       =   0  'Transparent
         Caption         =   "Site de r�f�rence"
         Height          =   255
         Left            =   -69600
         TabIndex        =   77
         Top             =   1680
         Width           =   2175
      End
      Begin VB.Label Label33 
         BackStyle       =   0  'Transparent
         Caption         =   "Auteur du document"
         Height          =   255
         Left            =   -69600
         TabIndex        =   75
         Top             =   960
         Width           =   2175
      End
      Begin VB.Label Label32 
         BackStyle       =   0  'Transparent
         Caption         =   "R�pertoire de destination"
         Height          =   255
         Left            =   -74760
         TabIndex        =   73
         Top             =   960
         Width           =   2175
      End
      Begin VB.Label Label31 
         BackStyle       =   0  'Transparent
         Caption         =   "Afficher le pdf en sortie"
         Height          =   255
         Left            =   -72120
         TabIndex        =   71
         Top             =   1320
         Width           =   2175
      End
      Begin VB.Label Label30 
         BackStyle       =   0  'Transparent
         Caption         =   "Spool en pi�ce jointe"
         Height          =   255
         Left            =   -74760
         TabIndex        =   69
         Top             =   1320
         Width           =   2175
      End
      Begin VB.Label Label29 
         BackStyle       =   0  'Transparent
         Caption         =   "Mot de passe utilisateur"
         Height          =   255
         Left            =   -74760
         TabIndex        =   66
         Top             =   1680
         Width           =   2175
      End
      Begin VB.Label Label28 
         BackStyle       =   0  'Transparent
         Caption         =   "Total"
         Height          =   255
         Left            =   -67920
         TabIndex        =   64
         Top             =   1440
         Width           =   735
      End
      Begin VB.Label Label27 
         BackStyle       =   0  'Transparent
         Caption         =   "Minimum"
         Height          =   255
         Left            =   -71640
         TabIndex        =   63
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label Label26 
         BackStyle       =   0  'Transparent
         Caption         =   "Niveau du tra�age"
         Height          =   255
         Left            =   -70200
         TabIndex        =   62
         Top             =   960
         Width           =   2055
      End
      Begin VB.Label Label25 
         BackStyle       =   0  'Transparent
         Caption         =   "Installer le Framework .NET"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   -68280
         TabIndex        =   60
         Top             =   3840
         Visible         =   0   'False
         Width           =   2655
      End
      Begin VB.Label Label24 
         BackStyle       =   0  'Transparent
         Caption         =   "Version du framework"
         Height          =   255
         Left            =   -74760
         TabIndex        =   59
         Top             =   3960
         Width           =   4815
      End
      Begin VB.Label Label23 
         Caption         =   "Imprimantes PDF trouv�es :"
         Height          =   255
         Left            =   -74760
         TabIndex        =   58
         Top             =   4320
         Width           =   1935
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00000000&
         X1              =   -74760
         X2              =   -65640
         Y1              =   4200
         Y2              =   4200
      End
      Begin VB.Label Label22 
         BackStyle       =   0  'Transparent
         Caption         =   "Code langue"
         Height          =   255
         Left            =   240
         TabIndex        =   57
         Top             =   2880
         Width           =   3975
      End
      Begin VB.Label Label20 
         BackStyle       =   0  'Transparent
         Caption         =   "Ne pas afficher l'indicateur de d�passement"
         Height          =   255
         Left            =   240
         TabIndex        =   55
         Top             =   2520
         Width           =   3735
      End
      Begin VB.Label Label14 
         BackStyle       =   0  'Transparent
         Caption         =   "Lotus Notes"
         Height          =   255
         Left            =   2400
         TabIndex        =   53
         Top             =   3840
         Width           =   975
      End
      Begin VB.Label Label21 
         BackStyle       =   0  'Transparent
         Caption         =   "Tra�age en temps r�el"
         Height          =   255
         Left            =   -74400
         TabIndex        =   51
         Top             =   5400
         Width           =   2895
      End
      Begin VB.Label Label15 
         BackStyle       =   0  'Transparent
         Caption         =   "PDF Creator"
         Height          =   255
         Left            =   -69000
         TabIndex        =   49
         Top             =   4680
         Width           =   975
      End
      Begin VB.Label Label16 
         BackStyle       =   0  'Transparent
         Caption         =   "Win2pdf"
         Height          =   255
         Left            =   -70920
         TabIndex        =   48
         Top             =   4680
         Width           =   975
      End
      Begin VB.Label Label17 
         BackStyle       =   0  'Transparent
         Caption         =   "Acrobat pdfwriter"
         Height          =   255
         Left            =   -72960
         TabIndex        =   47
         Top             =   4680
         Width           =   1215
      End
      Begin VB.Label Label18 
         BackStyle       =   0  'Transparent
         Caption         =   "Acrobat distiller"
         Height          =   255
         Left            =   -67200
         TabIndex        =   46
         Top             =   4680
         Width           =   1335
      End
      Begin VB.Label Label19 
         BackStyle       =   0  'Transparent
         Caption         =   "Acrobat pdf"
         Height          =   255
         Left            =   -74760
         TabIndex        =   45
         Top             =   4680
         Width           =   975
      End
      Begin VB.Label LabelMouchard 
         BackStyle       =   0  'Transparent
         Caption         =   "Fichier trace :"
         Height          =   255
         Left            =   -72600
         TabIndex        =   38
         Top             =   720
         Width           =   6855
      End
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   "Outlook"
         Height          =   255
         Left            =   240
         TabIndex        =   32
         Top             =   4560
         Width           =   975
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "Open Office"
         Height          =   255
         Left            =   240
         TabIndex        =   30
         Top             =   4200
         Width           =   975
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Caption         =   "Excel"
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   3840
         Width           =   975
      End
      Begin VB.Label Label10 
         Caption         =   "Applications disponibles (utilis�es par TurboGraph)"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   3480
         Width           =   3615
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00000000&
         X1              =   240
         X2              =   9360
         Y1              =   3360
         Y2              =   3360
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Serveur de messagerie SMTP"
         Height          =   255
         Left            =   -74760
         TabIndex        =   23
         Top             =   1080
         Width           =   2175
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Site de t�l�chargement (images, logos, mises � jour...)"
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   2160
         Width           =   3975
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Mode trace actif"
         Height          =   255
         Left            =   -74760
         TabIndex        =   18
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Options du turboPDF"
         Height          =   255
         Left            =   -72120
         TabIndex        =   15
         Top             =   5280
         Visible         =   0   'False
         Width           =   2175
      End
      Begin VB.Label Label9 
         BackStyle       =   0  'Transparent
         Caption         =   "Utiliser de pr�f�rence une de mes imprimantes PDF d�j� install�e"
         Height          =   255
         Left            =   -71160
         TabIndex        =   14
         Top             =   4320
         Width           =   5055
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Au d�marrage afficher en plein �cran"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   1080
         Width           =   3735
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Afficher par d�faut la navigation"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   1440
         Width           =   3975
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Au d�marrage v�rifier la pr�sence de mises � jour"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   1800
         Width           =   3975
      End
      Begin VB.Label label 
         BackStyle       =   0  'Transparent
         Caption         =   "Impression directe"
         Height          =   375
         Left            =   -74760
         TabIndex        =   5
         Top             =   1560
         Width           =   1935
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Imprimante par d�faut"
         Height          =   255
         Left            =   -74760
         TabIndex        =   4
         Top             =   1080
         Width           =   1935
      End
   End
   Begin VB.CommandButton Command1 
      Height          =   615
      Left            =   8400
      Picture         =   "frmConfig.frx":AAA2
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   6000
      Width           =   615
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3600
      Top             =   6000
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfig.frx":B76C
            Key             =   "SELECTION"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfig.frx":C646
            Key             =   "PRINT"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfig.frx":F4D8
            Key             =   "HELP"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfig.frx":FDB2
            Key             =   "MAIL"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfig.frx":12C44
            Key             =   "SAVE"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfig.frx":55B56
            Key             =   "PDF"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfig.frx":56430
            Key             =   "DEBBUG"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ReponseFaite As Boolean
Dim toto As String
Dim iStep As Integer

Private Sub Check21_Click()

If Me.Check21.Value = 1 Then
    Me.Check22.Value = 0
    Me.Check23.Value = 0
    Me.Check24.Value = 0
End If

End Sub

Private Sub Check22_Click()

If Me.Check22.Value = 1 Then
    Me.Check21.Value = 0
    Me.Check23.Value = 0
    Me.Check24.Value = 0
End If

End Sub

Private Sub Check23_Click()

If Me.Check23.Value = 1 Then
    Me.Check21.Value = 0
    Me.Check22.Value = 0
    Me.Check24.Value = 0
End If

End Sub

Private Sub Check24_Click()

If Me.Check24.Value = 1 Then
    Me.Check21.Value = 0
    Me.Check22.Value = 0
    Me.Check23.Value = 0
End If

End Sub

Private Sub Check25_Click()
If Me.Check25.Value = 0 Then Me.Check26.Value = 0
End Sub

Private Sub Check26_Click()

If Me.Check26.Value = 1 Then Me.Check25.Value = 1

End Sub

Private Sub Check27_Click()

If Me.Check27.Value = 1 Then
    '-> mettre la feuille au premier plan
    SetWindowPos Me.Hwnd, -1, 0, 0, 0, 0, 2 Or 1
Else
    '->ne pas mettre la feuille au premier plan
    SetWindowPos Me.Hwnd, -2, 0, 0, 0, 0, 2 Or 1
End If

End Sub

Private Sub Check29_Click()

If Check29.Value = 0 Then
    Me.CheckExcel.Enabled = True
    Me.CheckOpenOffice.Enabled = True
Else
    Me.CheckExcel.Enabled = False
    Me.CheckOpenOffice.Enabled = False
End If

End Sub

Private Sub Check30_Click()
If Check30.Value = 0 Then
    Me.CheckOutlook.Enabled = True
    Me.CheckLotusNotes.Enabled = True
Else
    Me.CheckOutlook.Enabled = False
    Me.CheckLotusNotes.Enabled = False
End If

End Sub

Private Sub Check7_Click()
'-> on active ou pas le mouchard en temps reel
If Me.Check7.Value = 1 Then
    MouchardTempsReel = True
Else
    MouchardTempsReel = False
End If
End Sub

Private Sub Command1_Click()

Call Command4_Click
Unload Me
End Sub

Private Sub Command10_Click()

If FileDateTime(App.Path & "\Turbograph.old") > FileDateTime(App.Path & "\Turbograph.ini") Then
    FileCopy App.Path & "\Turbograph.old", App.Path & "\Turbograph.ini"
Else
    If MsgBox("Souhaitez vous �craser le fichier de param�trage actuel?", vbOKCancel, "Restauration") = vbOK Then
        FileCopy App.Path & "\Turbograph.old", App.Path & "\Turbograph.ini"
    End If
End If

'-> on recharge
InitTurboVersion

'-> on recharge l'�cran
Initialise

End Sub

Private Sub Command11_Click()
Dim strPath As String
'-> on lance le changement de repertoire
strPath = BrowseForFolder("Selectionnez le r�pertoire contenant les spools d'�dition", GetPath(Me.Text6.Text), Me.Text6.Text)
Me.Text6.Text = strPath
End Sub

Private Sub Command12_Click()
'-> on teste l'envoi de mail
sendBySMTP "", ""
End Sub

Private Function sendBySMTP(ByVal FileToSend As String, sAdrMail As String) As Boolean
'--> cette fonction nous permet d'envoyer un mail
'--> par le composant winsock
Dim i As Integer
Dim j As Long
Dim m_strEncodedFiles As String
Dim l_destinataire
Dim destinataire
Dim sPieceJointe As String
Dim CurrentPIDProcess As Long
Dim sSMTP As String
Dim pdfName As String

Me.MousePointer = 11
'-> on se connecte au serveur
Winsock1.Close
iStep = 1
If GetIniString("PARAM", "SMTP", TurboGraphIniFile, False) <> "" Then
    sSMTP = GetIniString("PARAM", "SMTP", TurboGraphIniFile, False)
Else
    MsgBox "Veuillez param�trer l'adresse de votre serveur de messagerie SMTP", vbExclamation, ""
End If
Winsock1.Connect Trim(sSMTP), 25

'-> on met une attente pour les antispam (en principe pas d'attente)
Sleep Val(GetIniString("PARAM", "SMTPDELAI", App.Path & "\Turbograph.ini", False))

While Winsock1.State <> sckConnected
    DoEvents
Wend
    
ReponseFaite = False
Winsock1.SendData "HELO " & Trim(sSMTP) & vbCrLf
While ReponseFaite = False
    DoEvents
Wend

Me.MousePointer = 0

ReponseFaite = False
Winsock1.SendData "MAIL FROM: <TurboGraph>" & vbCrLf
While ReponseFaite = False
DoEvents
Wend

'-> on envoie les dif�rents destinataires (s�parateur ';' dans la saisie)
iStep = 3
'-> dans le cas ou on a une copi
l_destinataire = Split(Entry(3, sAdrMail & ";" & Trim(InputBox("Saisie des destinataires avec comme s�parateur ';'", "Test")), "~"), ";")

For Each destinataire In l_destinataire
    ReponseFaite = False
    Winsock1.SendData "RCPT TO: <" & Trim(destinataire) & ">" & vbCrLf
    While ReponseFaite = False
        DoEvents
    Wend
Next

'-> on change d'etape
iStep = 4
ReponseFaite = False
Winsock1.SendData "DATA" & vbCrLf
While ReponseFaite = False
    DoEvents
Wend

ReponseFaite = False

iStep = 5
'on pr�pare l'ent�te du message
Winsock1.SendData "To:" & "Mon destinataire" & vbCrLf
Winsock1.SendData "From:Test" & vbCrLf
Winsock1.SendData "Subject:" & "Test de configuration de turboGraph" & vbCrLf
'ChaineMessage = ChaineMessage & "CONTENT-TYPE: TEXT/HTML;" & vbCrLf & vbCrLf

'on envoie le message et la ou les pi�ces jointes si il y en a
Dim varLines    As Variant
Dim varLine     As Variant
Dim strMessage  As String

'ajout des pi�ces jointes au texte du message
strMessage = vbCrLf & vbCrLf & "Corps du message, ceci est un test" & vbCrLf & vbCrLf & m_strEncodedFiles
'on r�initialise la m�moire
m_strEncodedFiles = ""
'On coupe le fichier en plusieurs lignes (pour VB6 seulement)
varLines = Split(strMessage, vbCrLf)
'on vide la m�moire
strMessage = ""
'On envoie chaque ligne du message
For Each varLine In varLines
    Winsock1.SendData CStr(varLine) & vbLf
Next
'on envoie un point pour signaler au serveur que le msg est fini
iStep = 7
Winsock1.SendData vbCrLf & "." & vbCrLf
j = 0
While ReponseFaite = False
    j = j + 1
    If j > 1000000 Then
        ReponseFaite = True
    End If
    DoEvents
Wend

ReponseFaite = False
Winsock1.SendData "QUIT" & vbCrLf
j = 0
While ReponseFaite = False
    j = j + 1
    If j > 1000000 Then
        ReponseFaite = True
    End If
    DoEvents
Wend

Winsock1.Close

'MsgBox toto

sendBySMTP = True
End Function


Private Sub Command13_Click()
'-> mettre � jour les images pr�sentes
Dim sFile As String
Dim sMAJ As String

If MsgBox("Mettre � jour les images locales? Les images trouv�es seront mises � jour", vbOKCancel, "Mise � jour") = vbCancel Then Exit Sub

sFile = Dir(App.Path & "\images\")
Do While sFile <> ""
    If InStr(1, sFile, ".jpg", vbTextCompare) <> 0 Or InStr(1, sFile, ".gif", vbTextCompare) <> 0 Or InStr(1, sFile, ".bmp", vbTextCompare) <> 0 Then
        '-> on essai de mettre � jour le fichier
        If TelechargeFile(TurboGraphWebFile & "/deallogo/" & sFile, App.Path & "\images\" & sFile) Then sMAJ = sMAJ & Chr(13) & sFile
    End If
    '-> pointer sur le fichier suivant
    sFile = Dir
Loop

'-> on met � jour le tmpicture
If TelechargeFile(TurboGraphWebFile & "/deallogo/Tm_Picture.txt", App.Path & "\Tm_Picture.ini") Then sMAJ = sMAJ & Chr(13) & "Tm_Picture.ini"

'-> si on a mis � jour des images
If sMAJ <> "" Then
    MsgBox "Les images suivantes ont �t� mises � jour." & Chr(13) & sMAJ, vbOKOnly, "Mise � jour � partir du site"
Else
    MsgBox "Aucune image mise � jour, v�rifier si besoin l'adresse du site de mise � jour", vbOKOnly, "Mise � jour � partir du site"
End If
End Sub

Private Sub Command14_Click()
'-> cette proc�dure va donner les autorisations au programme de s'executer a partir du r�seau
Dim sCMD As String
Dim hdlFile As Long

sCMD = GetRepertoireWindows() & "\Microsoft.NET\Framework\" & GetFrameworkVersion("") & "\caspol.exe -m -ag 1.2 -url " & Chr(34) & "file:" & App.Path & "\*" & Chr(34) & " FullTrust"

hdlFile = FreeFile
Open App.Path & "\autoriturbopdf.bat" For Output As #hdlFile

'-> Imprimer les balises d'ouverture du fichier
Print #hdlFile, sCMD

Close #hdlFile

'-> on lance la ligne de commande
Shell App.Path & "\autoriturbopdf.bat", vbNormalFocus

End Sub

Private Sub Command2_Click()
'-> supprimer les fichiers images
Dim sFile As String

If MsgBox("Supprimer les images locales? Les images necessaires seront de nouveau t�l�charg�es", vbOKCancel, "Suppression") = vbCancel Then Exit Sub

sFile = Dir(App.Path & "\images\")
Do While sFile <> ""
    If InStr(1, sFile, ".jpg", vbTextCompare) <> 0 Or InStr(1, sFile, ".gif", vbTextCompare) <> 0 Or InStr(1, sFile, ".bmp", vbTextCompare) <> 0 Then
        '-> on supprime le fichier
        Kill App.Path & "\images\" & sFile
    End If
    '-> pointer sur le fichier suivant
    sFile = Dir
Loop

End Sub

Private Sub Command3_Click()
Dim strBuffer As String
Dim sh As Long
'-> si on a un fichier trace
If FileExist(Mouchard) Then
    'Close #hdlMouchard
    sh = ShellExecute(Me.Hwnd, "Open", Mouchard, vbNullString, strBuffer, 1)
End If

End Sub

Private Sub Command4_Click()
Dim sPDF As String
Dim sTemp As String

If Me.Text4.Text <> Me.Text5.Text Then
    MsgBox "Confirmer le mot de passe utilisateur"
End If

If Me.Text9.Text <> Me.Text10.Text Then
    MsgBox "Confirmer le mot de passe administrateur"
End If

'-> on recree la ligne de commande pour le turbopdf
'-> le repertoire ou deposer  les pdf
If Trim(Me.Text6.Text) <> "" Then sPDF = sPDF & "�pdfDirectory=" & Trim(Me.Text6.Text)
'-> si on doit integrer le spool en piece jointe
If Me.Check9.Value = 1 Then sPDF = sPDF & "�Embedded"
'-> si il y a un mot de passe utilisateur
If Trim(Me.Text4.Text) <> "" Then sPDF = sPDF & "�userPassword=" & Me.Text4.Text
'-> si il y a un mot de passe administrateur
If Trim(Me.Text9.Text) <> "" Then sPDF = sPDF & "�ownerPassword=" & Me.Text9.Text
'-> si il y a des types de protection
If Me.Check11 Or Me.Check12 Or Me.Check13 Or Me.Check14 Then
    If Me.Check11 Then sTemp = "VIEWONLY"
    If Me.Check12 Then
        If sTemp = "" Then
            sTemp = "PRINT"
        Else
            sTemp = sTemp & ",PRINT"
        End If
    End If
    If Me.Check13 Then
        If sTemp = "" Then
            sTemp = "MODIFY"
        Else
            sTemp = sTemp & ",MODIFY"
        End If
    End If
    If Me.Check14 Then
        If sTemp = "" Then
            sTemp = "COPY"
        Else
            sTemp = sTemp & ",COPY"
        End If
    End If
    sPDF = sPDF & "�typeProtection=" & sTemp
End If
'-> si on doit mettre un auteur
If Trim(Me.Text7.Text) <> "" Then sPDF = sPDF & "�docAuthor=" & Trim(Me.Text7.Text)
'-> si on doit mettre un titre
If Trim(Me.Text11.Text) <> "" Then sPDF = sPDF & "�docTitle=" & Trim(Me.Text11.Text)
'-> si on doit mettre un sujet
If Trim(Me.Text14.Text) <> "" Then sPDF = sPDF & "�docSubject=" & Trim(Me.Text14.Text)
'-> si on doit mettre des mots cl�s
If Trim(Me.Text15.Text) <> "" Then sPDF = sPDF & "�docKeywords=" & Trim(Me.Text15.Text)
'-> si on doit mettre un site
If (Trim(Me.Text8.Text) <> "" Or Trim(Me.Text12.Text) <> "") Then sPDF = sPDF & "�pdfSite=" & Me.Text12.Text & "|" & Trim(Me.Text8.Text)
'-> si on doit afficher le pdf a la fin de la generation
If Me.Check25.Value = 1 Then
    If Me.Check26.Value = 1 Then
        sPDF = sPDF & "�Sortie=AUTOPRINTCHOIX"
    Else
        sPDF = sPDF & "�Sortie=AUTOPRINT"
    End If
Else
    If Me.Check10.Value = 1 Then sPDF = sPDF & "�Sortie=ECRAN"
End If
If Me.Check33.Value = 1 Then sPDF = sPDF & "�rtfToImage=O"
'-> si il y a des modes d'affichage
sTemp = ""
If Me.Check15 Or Me.Check16 Or Me.Check17 Or Me.Check18 Or Me.Check19 Or Me.Check20 Then
    If Me.Check15 Then sTemp = "HIDEMENUBAR"
    If Me.Check16 Then
        If sTemp = "" Then
            sTemp = "FULLSCREEN"
        Else
            sTemp = sTemp & ",FULLSCREEN"
        End If
    End If
    If Me.Check17 Then
        If sTemp = "" Then
            sTemp = "SINGLEPAGE"
        Else
            sTemp = sTemp & ",SINGLEPAGE"
        End If
    End If
    If Me.Check18 Then
        If sTemp = "" Then
            sTemp = "TWOCOLUMN"
        Else
            sTemp = sTemp & ",TWOCOLUMN"
        End If
    End If
    If Me.Check19 Then
        If sTemp = "" Then
            sTemp = "FITWINDOW"
        Else
            sTemp = sTemp & ",FITWINDOW"
        End If
    End If
    If Me.Check20 Then
        If sTemp = "" Then
            sTemp = "CENTERWINDOW "
        Else
            sTemp = sTemp & ",CENTERWINDOW "
        End If
    End If
    sPDF = sPDF & "�viewerPreference=" & sTemp
End If

'-> on enregistre dans le turbograph.ini de la version internet la config
'-> imprimante
SetIniString "PRINTER", "DEFAULT", App.Path & "\Turbograph.ini", Me.Combo1.Text
'-> impression automatique
SetIniString "PRINTER", "DIRECT", App.Path & "\Turbograph.ini", Me.Check1.Value
'-> site internet pour les images
SetIniString "SITE", "HTTP", App.Path & "\Turbograph.ini", Me.Text1.Text
'-> pour la ligne de commande pdf
SetIniString "PDF", "CMD", App.Path & "\Turbograph.ini", sPDF
'-> dans le cas ou on a plusieurs imprimantes pdf donner la priorit� a turbopdf
SetIniString "PDF", "TURBOPDF", App.Path & "\Turbograph.ini", Me.Check2.Value
'-> pour sp�cifier si on ouvre turbograph en plein �cran
SetIniString "PARAM", "FULLSCREEN", App.Path & "\Turbograph.ini", Me.Check3.Value
'-> pour sp�cifier si on ouvre turbograph en plein �cran
SetIniString "PARAM", "GARDE", App.Path & "\Turbograph.ini", Me.Check28.Value
'-> pour sp�cifier si on cherche les imprimante avec l'objet printer
SetIniString "PARAM", "PRINTEROBJET", App.Path & "\Turbograph.ini", Me.Check32.Value
'-> pour sp�cifier si on ouvre le menu navigation par d�faut
SetIniString "PARAM", "NAVIGA", App.Path & "\Turbograph.ini", Me.Check4.Value
'-> pour sp�cifier si on ouvre les spools dans la m�me session
SetIniString "PARAM", "SESSION", App.Path & "\Turbograph.ini", Me.Check31.Value
'-> pour sp�cifier si on v�rifie les mises � jour
SetIniString "PARAM", "UPDATE", App.Path & "\Turbograph.ini", Me.Check5.Value
'-> pour sp�cifier le repertoire d'enregistrement
SetIniString "PARAM", "SAVE", App.Path & "\Turbograph.ini", Me.Text16.Text
'-> pour sp�cifier le nombre de jours que l'on garde les spools si rien ils sont jamais supprim�s
SetIniString "PARAM", "KEEP", App.Path & "\Turbograph.ini", Me.Text17.Text
'-> pour v�rifier si on doit tracer le spool
SetIniString "PARAM", "TRACE", App.Path & "\Turbograph.ini", Me.Check6.Value
'-> pour v�rifier si on doit afficher l'indicateur de depassement
SetIniString "PARAM", "DEPASS", App.Path & "\Turbograph.ini", Me.Check8.Value
SetIniString "PARAM", "VERIFYTABLEUR", App.Path & "\Turbograph.ini", Me.Check29.Value

SetIniString "PARAM", "VERIFYOUTLOOK", App.Path & "\Turbograph.ini", Me.Check30.Value

If Check29.Value = 0 Then
    If Me.CheckExcel.Value = "1" Then
        SetIniString "PARAM", "ISEXCEL", App.Path & "\Turbograph.ini", "1"
    Else
        SetIniString "PARAM", "ISEXCEL", App.Path & "\Turbograph.ini", "0"
    End If
    If Me.CheckOpenOffice.Value = "1" Then
        SetIniString "PARAM", "ISOPENOFFICE", App.Path & "\Turbograph.ini", "1"
    Else
        SetIniString "PARAM", "ISOPENOFFICE", App.Path & "\Turbograph.ini", "0"
    End If
Else
    SetIniString "PARAM", "ISEXCEL", App.Path & "\Turbograph.ini", "0"
    SetIniString "PARAM", "ISOPENOFFICE", App.Path & "\Turbograph.ini", "0"
End If

If Check30.Value = 0 Then
    If Me.CheckOutlook.Value = "1" Then
        SetIniString "PARAM", "ISOUTLOOK", App.Path & "\Turbograph.ini", "1"
    Else
        SetIniString "PARAM", "ISOUTLOOK", App.Path & "\Turbograph.ini", "0"
    End If
    If Me.CheckLotusNotes.Value = "1" Then
        SetIniString "PARAM", "ISLOTUS", App.Path & "\Turbograph.ini", "1"
    Else
        SetIniString "PARAM", "ISLOTUS", App.Path & "\Turbograph.ini", "0"
    End If
Else
    SetIniString "PARAM", "ISOUTLOOK", App.Path & "\Turbograph.ini", "0"
    SetIniString "PARAM", "ISLOTUS", App.Path & "\Turbograph.ini", "0"
End If

'-> pour le code langue
SetIniString "PARAM", "LANGUE", App.Path & "\Turbograph.ini", Me.Text3.Text
'-> pour le client de messagerie par d�faut
SetIniString "PARAM", "DEFAULTCLIENT", App.Path & "\Turbograph.ini", Me.ImageCombo1.SelectedItem.Index
'-> pour le trace level
SetIniString "PARAM", "TRACELEVEL", App.Path & "\Turbograph.ini", Me.Slider1.Value
'-> pour la piece jointe par defaut
If Me.Check21.Value = 1 Then SetIniString "PARAM", "DEFAUTPJ", App.Path & "\Turbograph.ini", "HTML"
If Me.Check22.Value = 1 Then SetIniString "PARAM", "DEFAUTPJ", App.Path & "\Turbograph.ini", "NONE"
If Me.Check23.Value = 1 Then SetIniString "PARAM", "DEFAUTPJ", App.Path & "\Turbograph.ini", "PDF"
If Me.Check24.Value = 1 Then SetIniString "PARAM", "DEFAUTPJ", App.Path & "\Turbograph.ini", "TURBO"

If Me.Check6.Value = 1 Then
    IsMouchard = True
    Trace "Mouchard activ�", 1
Else
    Mouchard = ""
    IsMouchard = False
    hdlMouchard = 0
End If
'-> adresse du serveur SMTP
SetIniString "PARAM", "SMTP", App.Path & "\Turbograph.ini", Me.SMTP.Text
SetIniString "PARAM", "SMTPDELAI", App.Path & "\Turbograph.ini", Val(Me.Text13.Text)
'-> url de l'upload du serveur
SetIniString "PARAM", "UPLOADURL", App.Path & "\Turbograph.ini", Me.Text18.Text

'-> ne pas toucher au param�trage de pdfcreator
SetIniString "PARAM", "KEEPPDFCREATORSETTING", App.Path & "\Turbograph.ini", Me.CheckPDFCreatorSetting.Value

TurboGraphWebPrinter = Me.Combo1.Text
TurboGraphWebDirect = Me.Check1.Value

InitTurboVersion
Initialise
End Sub

Private Sub Command5_Click()
'-> quitter
Unload Me
End Sub

Private Sub Command6_Click()

'-> on recherche les mises � jour
Find_Update

End Sub

Private Sub Command7_Click()

'-> on lance le test du turboPDF
Shell App.Path & "\TurboPdf.exe " & GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False)
End Sub

Private Sub Command8_Click()
If Dir(App.Path & "\Turbograph.old") <> "" Then
    If MsgBox("Le fichier 'Turbograph.old' existe d�j�. L'�craser?", vbOKCancel, "Cr�ation d'un fichier de sauvegarde") = vbOK Then
        '-> on fait une copie du fichier de param�trage
        FileCopy App.Path & "\Turbograph.ini", App.Path & "\Turbograph.old"
    End If
    Me.Command10.Enabled = True
Else
    '-> on fait une copie du fichier de param�trage
    FileCopy App.Path & "\Turbograph.ini", App.Path & "\Turbograph.old"
    Me.Command10.Enabled = True
End If
End Sub

Private Sub Command9_Click()
'-> on se connecte au serveur
Dim sSMTP As String
Dim j As Long

Me.MousePointer = 11
Winsock1.Close
iStep = 1
If Trim(Me.SMTP.Text) <> "" Then
    sSMTP = Trim(Me.SMTP.Text)
Else
    MsgBox "Veuillez param�trer l'adresse de votre serveur de messagerie SMTP", vbExclamation, ""
End If
Winsock1.Connect Trim(sSMTP), 25
While Winsock1.State <> sckConnected
    DoEvents
    j = j + 1
    If j > 100000 Then
        GoTo fin
    End If
    DoEvents
Wend
    
j = 1
ReponseFaite = False
Winsock1.SendData "HELO Serveur" & vbCrLf
While ReponseFaite = False
    DoEvents
    j = j + 1
    If j > 1000000 Then
        GoTo fin
    End If
    DoEvents
Wend

fin:

Me.MousePointer = 0

If iStep = 2 Then
    MsgBox "Connexion avec succes au serveur SMTP"
Else
    MsgBox "Connexion impossible au serveur SMTP"
End If
End Sub

Private Sub Form_Load()

Initialise

End Sub

Private Sub Initialise()
Dim aPrint As Printer
Dim i As Integer
Dim DeviceName As String
Dim Version As String
Dim aLb As Libelle
Dim j As Integer

On Error Resume Next

'-> mettre la feuille au premier plan
SetWindowPos Me.Hwnd, -1, 0, 0, 0, 0, 2 Or 1

'-> Gestion des messages
Set aLb = Libelles("FRMMAIL")

'-> Charger les images
Set Me.SSTab1.TabPicture(0) = Me.ImageList1.ListImages("SELECTION").Picture
Set Me.SSTab1.TabPicture(1) = Me.ImageList1.ListImages("PRINT").Picture
Set Me.SSTab1.TabPicture(2) = Me.ImageList1.ListImages("PDF").Picture
Set Me.SSTab1.TabPicture(3) = Me.ImageList1.ListImages("DEBBUG").Picture
Set Me.SSTab1.TabPicture(4) = Me.ImageList1.ListImages("MAIL").Picture
Me.ImageCombo1.comboItems.add , , aLb.GetCaption(54), Me.ImageList2.ListImages("WWW").Index
If Not IsLotus Then
    Me.ImageCombo1.comboItems.add , , aLb.GetCaption(55), Me.ImageList2.ListImages("OUTLOOK").Index
Else
    If IsLotus Then Me.ImageCombo1.comboItems.add , , aLb.GetCaption(56), Me.ImageList2.ListImages("LOTUS").Index
End If
Me.ImageCombo1.comboItems.add , "SMTP", aLb.GetCaption(57), Me.ImageList2.ListImages("SMTP").Index
Me.ImageCombo1.SelectedItem = Me.ImageCombo1.comboItems(2)

'-> Charger la liste des imprimantes
Me.Combo1.Clear
For Each aPrint In Printers
    Me.Combo1.AddItem aPrint.DeviceName
    If aPrint.DeviceName = Printer.DeviceName Then _
        Me.Combo1.ListIndex = Me.Combo1.ListCount - 1
Next
'-> On ajoute l'imprimante ecran
Me.Combo1.AddItem "SCREEN"

'-> on pointe sur l'imprimante defaut
If TurboGraphWebPrinter <> "" Then
    For i = 1 To Me.Combo1.ListCount
        If Me.Combo1.List(i) = TurboGraphWebPrinter Then Me.Combo1.Text = TurboGraphWebPrinter
    Next
End If

'-> on regarde si l'impression est directe
If TurboGraphWebDirect = "1" Then Me.Check1.Value = 1

'-> on charge le param�trage
Me.Text1.Text = GetIniString("SITE", "HTTP", App.Path & "\Turbograph.ini", False)
Me.Text2.Text = GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False)
Me.SMTP.Text = GetIniString("PARAM", "SMTP", App.Path & "\Turbograph.ini", False)
Me.Text13.Text = Val(GetIniString("PARAM", "SMTPDELAI", App.Path & "\Turbograph.ini", False))
Me.Text18.Text = GetIniString("PARAM", "UPLOADURL", App.Path & "\Turbograph.ini", False)
Me.Text16.Text = GetIniString("PARAM", "SAVE", App.Path & "\Turbograph.ini", False)
Me.Text17.Text = GetIniString("PARAM", "KEEP", App.Path & "\Turbograph.ini", False)
Me.Check2.Value = GetIniString("PDF", "TURBOPDF", App.Path & "\Turbograph.ini", False)
Me.Check3.Value = GetIniString("PARAM", "FULLSCREEN", App.Path & "\Turbograph.ini", False)
Me.Check28.Value = GetIniString("PARAM", "GARDE", App.Path & "\Turbograph.ini", False)
Me.Check32.Value = GetIniString("PARAM", "PRINTEROBJET", App.Path & "\Turbograph.ini", False)
Me.Check4.Value = GetIniString("PARAM", "NAVIGA", App.Path & "\Turbograph.ini", False)
Me.Check31.Value = GetIniString("PARAM", "SESSION", App.Path & "\Turbograph.ini", False)
Me.Check5.Value = GetIniString("PARAM", "UPDATE", App.Path & "\Turbograph.ini", False)
Me.Check6.Value = GetIniString("PARAM", "TRACE", App.Path & "\Turbograph.ini", False)
Me.Check8.Value = GetIniString("PARAM", "DEPASS", App.Path & "\Turbograph.ini", False)
Me.Check29.Value = GetIniString("PARAM", "VERIFYTABLEUR", App.Path & "\Turbograph.ini", False)
If GetIniString("PARAM", "VERIFYTABLEUR", App.Path & "\Turbograph.ini", False) = "" Then Me.Check29.Value = "1"
Me.Check30.Value = GetIniString("PARAM", "VERIFYOUTLOOK", App.Path & "\Turbograph.ini", False)
If GetIniString("PARAM", "VERIFYOUTLOOK", App.Path & "\Turbograph.ini", False) = "" Then Me.Check30.Value = "1"
Me.Slider1.Value = GetIniString("PARAM", "TRACELEVEL", App.Path & "\Turbograph.ini", False)
If GetIniString("PARAM", "DEFAULTCLIENT", App.Path & "\Turbograph.ini", False) <> "" Then
    Me.ImageCombo1.SelectedItem = Me.ImageCombo1.comboItems(CInt(GetIniString("PARAM", "DEFAULTCLIENT", App.Path & "\Turbograph.ini", False)))
Else
    Me.ImageCombo1.SelectedItem = Me.ImageCombo1.comboItems(2)
End If

If Check29.Value = 0 Then
    CheckExcel.Enabled = True
    CheckOpenOffice.Enabled = True
    If GetIniString("PARAM", "ISEXCEL", App.Path & "\Turbograph.ini", False) = "1" Then Me.CheckExcel.Value = "1"
    If GetIniString("PARAM", "ISOPENOFFICE", App.Path & "\Turbograph.ini", False) = "1" Then Me.CheckOpenOffice.Value = "1"
Else
    CheckExcel.Enabled = False
    CheckOpenOffice.Enabled = False
End If
If Check30.Value = 0 Then
    CheckOutlook.Enabled = True
    CheckLotusNotes.Enabled = True
    If GetIniString("PARAM", "ISOUTLOOK", App.Path & "\Turbograph.ini", False) = "1" Then Me.CheckOutlook.Value = "1"
    If GetIniString("PARAM", "ISLOTUS", App.Path & "\Turbograph.ini", False) = "1" Then Me.CheckLotusNotes.Value = "1"
Else
    CheckOutlook.Enabled = False
    CheckLotusNotes.Enabled = False
End If

If Check30.Value = 0 Then
    CheckOutlook.Enabled = True
    CheckLotusNotes.Enabled = True
End If

MouchardLevel = Me.Slider1.Value
'-> on affiche le nom du fichier Mouchard
Me.LabelMouchard.Caption = "Fichier Mouchard : " & Mouchard

'-> on charge les applications trouv�es
If Me.Check29.Value = 1 Then
    If IsExcel Then Me.CheckExcel.Value = 1
    If IsOpenoffice Then Me.CheckOpenOffice = 1
End If
If Me.Check30.Value = 1 Then
    If IsOutLook Then Me.CheckOutlook = 1
    If IsLotus Then Me.CheckLotusNotes = 1
End If
'-> on regarde si les imprimantes pdf ont �t� trouv�e
'-> on recupere l'imprimante pdf
For i = 0 To Printers.Count - 1
    Select Case UCase$(Printers(i).DeviceName)
        Case "ACROBAT PDFWRITER"
            Me.CheckPDFWriter = 1
        Case "ACROBAT DISTILLER"
            Me.CheckDistiller = 1
        Case "ADOBE PDF"
            Me.CheckPDF = 1
        Case "WIN2PDF"
            Me.CheckWin2pdf = 1
        Case "PDFCREATOR"
            Me.CheckPDFCreator = 1
            Me.CheckPDFCreatorSetting.Visible = True
            Me.Label67.Visible = True
            If GetIniString("PARAM", "KEEPPDFCREATORSETTING", App.Path & "\Turbograph.ini", False) = "1" Then
                Me.CheckPDFCreatorSetting.Value = 1
            End If
    End Select
Next

'-> pour la piece jointe par defaut
Select Case GetIniString("PARAM", "DEFAUTPJ", App.Path & "\Turbograph.ini", False)
    Case "HTML"
        Me.Check21.Value = 1
    Case "PDF"
        Me.Check23.Value = 1
    Case "NONE"
        Me.Check22.Value = 1
    Case "TURBO"
        Me.Check24.Value = 1
End Select

'-> proposer de restaurer un fichier de sauvegarde
If Dir(App.Path & "\Turbograph.old") <> "" Then
    Me.Command10.Enabled = True
End If

'-> on charge le code langue
Me.Text3.Text = IndexLangue

'-> on affiche la version du framework
If GetFrameworkVersion(Version) <> "" Then
    Me.Label24.Caption = "Framework .NET " & GetFrameworkVersion(Version)
Else
    Me.Label24.Caption = "Auncune version du Framework .net install�e. TurboPDF ne peut fonctionner"
    Me.Label25.Visible = True
End If

'-> on reinitialise
Me.Check10.Value = 0
Me.Check11.Value = 0
Me.Check12.Value = 0
Me.Check13.Value = 0
Me.Check14.Value = 0
Me.Check15.Value = 0
Me.Check16.Value = 0
Me.Check17.Value = 0
Me.Check18.Value = 0
Me.Check19.Value = 0
Me.Check20.Value = 0
Me.Check25.Value = 0
Me.Check26.Value = 0
Me.Text6.Text = ""
Me.Text4.Text = ""
Me.Text9.Text = ""
Me.Text7.Text = ""
Me.Text11.Text = ""
Me.Text8.Text = ""
Me.Text12.Text = ""

'-> on charge les pdf
For i = 1 To NumEntries(Me.Text2.Text, "�")
    Select Case Entry(1, UCase(Entry(i, Me.Text2.Text, "�")), "=")
        Case "SORTIE"
            Me.Check10.Value = 1
            If Entry(2, UCase(Entry(i, Me.Text2.Text, "�")), "=") = "AUTOPRINT" Then Me.Check25.Value = 1
            If Entry(2, UCase(Entry(i, Me.Text2.Text, "�")), "=") = "AUTOPRINTCHOIX" Then
                Me.Check25.Value = 1
                Me.Check26.Value = 1
            End If
        Case UCase("Embedded")
            Me.Check9.Value = 1
        Case UCase("pdfDirectory")
            Me.Text6.Text = Entry(2, Entry(i, Me.Text2.Text, "�"), "=")
        Case UCase("userPassword")
            Me.Text4.Text = Entry(2, Entry(i, Me.Text2.Text, "�"), "=")
            Me.Text5.Text = Me.Text4.Text
        Case UCase("ownerPassword")
            Me.Text9.Text = Entry(2, Entry(i, Me.Text2.Text, "�"), "=")
            Me.Text10.Text = Me.Text9.Text
        Case UCase("typeProtection")
            If InStr(1, Entry(2, UCase(Entry(i, Me.Text2.Text, "�")), "="), "VIEWONLY") <> 0 Then Me.Check11.Value = 1
            If InStr(1, Entry(2, UCase(Entry(i, Me.Text2.Text, "�")), "="), "PRINT") <> 0 Then Me.Check12.Value = 1
            If InStr(1, Entry(2, UCase(Entry(i, Me.Text2.Text, "�")), "="), "MODIFY") <> 0 Then Me.Check13.Value = 1
            If InStr(1, Entry(2, UCase(Entry(i, Me.Text2.Text, "�")), "="), "COPY") <> 0 Then Me.Check14.Value = 1
        Case UCase("docAuthor")
            Me.Text7.Text = Entry(2, Entry(i, Me.Text2.Text, "�"), "=")
        Case UCase("rtfToImage")
            If Entry(2, UCase(Entry(i, Me.Text2.Text, "�")), "=") = "O" Then Me.Check33.Value = 1
        Case UCase("docTitle")
            Me.Text11.Text = Entry(2, Entry(i, Me.Text2.Text, "�"), "=")
        Case UCase("docSubject")
            Me.Text14.Text = Entry(2, Entry(i, Me.Text2.Text, "�"), "=")
        Case UCase("docKeywords")
            Me.Text15.Text = Entry(2, Entry(i, Me.Text2.Text, "�"), "=")
        Case UCase("pdfSite")
            Me.Text8.Text = Entry(2, Entry(2, Entry(i, Me.Text2.Text, "�"), "="), "|")
            Me.Text12.Text = Entry(1, Entry(2, Entry(i, Me.Text2.Text, "�"), "="), "|")
        Case UCase("viewerPreference")
            If InStr(1, Entry(2, UCase(Entry(i, Me.Text2.Text, "�")), "="), "HIDEMENUBAR") <> 0 Then Me.Check15.Value = 1
            If InStr(1, Entry(2, UCase(Entry(i, Me.Text2.Text, "�")), "="), "FULLSCREEN") <> 0 Then Me.Check16.Value = 1
            If InStr(1, Entry(2, UCase(Entry(i, Me.Text2.Text, "�")), "="), "GARDE") <> 0 Then Me.Check28.Value = 1
            If InStr(1, Entry(2, UCase(Entry(i, Me.Text2.Text, "�")), "="), "SINGLEPAGE") <> 0 Then Me.Check17.Value = 1
            If InStr(1, Entry(2, UCase(Entry(i, Me.Text2.Text, "�")), "="), "TWOCOLUMN") <> 0 Then Me.Check18.Value = 1
            If InStr(1, Entry(2, UCase(Entry(i, Me.Text2.Text, "�")), "="), "FITWINDOW") <> 0 Then Me.Check19.Value = 1
            If InStr(1, Entry(2, UCase(Entry(i, Me.Text2.Text, "�")), "="), "CENTERWINDOW") <> 0 Then Me.Check20.Value = 1
    End Select
Next

MouchardTempsReel = True

End Sub

Private Sub Form_Unload(Cancel As Integer)

'-> on ferme le mouchard temps r��el
MouchardTempsReel = False

End Sub

Private Sub Label25_Click()

ShellExecute MDIMain.Hwnd, "Open", "http://www.microsoft.com/downloads/details.aspx?FamilyId=0856EACB-4362-4B0D-8EDD-AAB15C5E04F5&displaylang=fr", vbNullString, "", 1

End Sub

Private Sub Slider1_Change()
'-> pour le trace level
SetIniString "PARAM", "TRACELEVEL", App.Path & "\Turbograph.ini", Me.Slider1.Value
MouchardLevel = Me.Slider1.Value
End Sub

Private Sub Winsock1_DataArrival(ByVal bytesTotal As Long)
'--> ici sont g�r�e les r�ponses du composant winsock
Dim DonneesRecues As String
Dim strMessage As String

Winsock1.GetData DonneesRecues

'-> bon on a recu une r�ponse
ReponseFaite = True
'Exit Sub

Trace DonneesRecues, 1

Select Case iStep
    Case 1 '-> connexion au serveur
        If InStr(1, DonneesRecues, "220") Then
            ReponseFaite = True
            iStep = 2
        Else
            MsgBox "Impossible de se connecter au serveur SMTP" & Chr(13) & DonneesRecues
        End If
    Case 2 '-> controle de la connexion
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
        Else
            MsgBox "Connexion refus�e par le serveur SMTP" & Chr(13) & DonneesRecues
        End If
    Case 3 '-> adresse mail de l'envoyeur et du destinataire
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
            iStep = 4
        Else
            strMessage = "Adresse mail de l'envoyeur non reconnue par le serveur SMTP" & Chr(13) & DonneesRecues
        End If
    Case 4 '-> adresse mail de l'envoyeur et du destinataire
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
        Else
            'strMessage = "Adresse mail du destinataire non reconnue par le serveur SMTP" & Chr(13) & DonneesRecues
        End If
    Case 5  '-> Envoi des donnees
        If InStr(1, DonneesRecues, "354 ") Then
            ReponseFaite = True
        Else
            strMessage = "Erreur lors de l'envoi de l'ent�te du mail" & Chr(13) & DonneesRecues
        End If
    Case 6
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
            iStep = 7
        Else
            strMessage = "Erreur lors de l'envoi du contenu du mail" & Chr(13) & DonneesRecues
        End If
    Case 7 '-> fermeture de la connexion
        If InStr(1, DonneesRecues, "221 ") Then ReponseFaite = True
End Select

If strMessage <> "" Then MsgBox strMessage


End Sub

