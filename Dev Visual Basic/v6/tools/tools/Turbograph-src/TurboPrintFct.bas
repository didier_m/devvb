Attribute VB_Name = "TurboPrintFct"
''    '-> Positionner le pointeur sur l'orientation de la maquette
''    Printer.ScaleMode = 3
''    If aMaq.Orientation = 1 Then
''        Printer.Orientation = 1
''    Else
''        Printer.Orientation = 2
''    End If
''    Printer.Print ""
''End If
''
''
'''-> R�cup�ration des marges internes du contexte de p�riph�rique
''MargeX = GetDeviceCaps(Sortie.hDC, PHYSICALOFFSETX)
''MargeY = GetDeviceCaps(Sortie.hDC, PHYSICALOFFSETY)
''
'''-> Initialiser la position X , y du pointeur sur les marges du document
''PositionY = -MargeY + Sortie.ScaleX(aMaq.MargeTop, 7, 3)
''PositionX = -MargeX + Sortie.ScaleY(aMaq.MargeLeft, 7, 3)
''
''
''Private Sub PrintTableau(ByRef NomObjet As String, ByRef Param As String, _
''                         ByRef DataFields As String, ByRef PositionX As Long, _
''                         ByRef PositionY As Long, ByRef NomSousObjet As String, ByRef FirstObj As Boolean)
''
''Dim aTb As Tableau
''Dim nLig As Integer
''
''On Error GoTo GestTableau
''
'''-> Pointer sur le tableau pass� en argument
''Set aTb = aMaq.Tableaux(UCase$(NomObjet))
''
''ErrorLibelle = "Pb dans le calcul et l'initialisation des dimensions du printer"
''
'''-> Test des l'orientation de l'imprimante
''If TypeOf Sortie Is Printer Then
''    '-> Tester si l'orientation du tableau est la m�me que l'imprimante
''    If aTb.Orientation = 0 Then
''        If Printer.Orientation <> 2 Then
''            If FirstObj Then
''                Printer.Orientation = 2
''                '-> Initialiser le printer
''                Printer.Print ""
''            End If
''        End If
''    ElseIf aTb.Orientation = 1 Then
''        If Printer.Orientation <> 1 Then
''            If FirstObj Then
''                Printer.Orientation = 1
''                '-> Initialiser le printer
''                Printer.Print ""
''            End If
''        End If
''    End If
''Else
''    If aTb.Orientation <> CInt(aMaq.Orientation) Then
''        '-> Tester si on est le premier objet
''        If FirstObj Then
''            '-> Mettre la page au format
''            frmVisu.Page.Width = frmVisu.ScaleX(aMaq.Hauteur, 7, 1)
''            frmVisu.Page.Height = frmVisu.ScaleY(aMaq.Largeur, 7, 1)
''        End If
''    End If
''End If
