VERSION 5.00
Object = "{12BB5440-C561-11CF-B762-0080C81795BC}#1.0#0"; "Indicate.ocx"
Begin VB.Form frmIndicate 
   Caption         =   "Pont � bascule - Deal Informatique"
   ClientHeight    =   2490
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   6360
   Icon            =   "frmIndicate.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   2490
   ScaleWidth      =   6360
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text6 
      Height          =   285
      Left            =   1440
      TabIndex        =   11
      Text            =   "c:\"
      Top             =   2160
      Width           =   1695
   End
   Begin VB.TextBox Text5 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5040
      TabIndex        =   10
      Top             =   840
      Width           =   1095
   End
   Begin VB.TextBox Text4 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5040
      TabIndex        =   9
      Top             =   480
      Width           =   1095
   End
   Begin VB.TextBox Text3 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5040
      TabIndex        =   8
      Top             =   120
      Width           =   1095
   End
   Begin VB.Timer Timer1 
      Interval        =   2000
      Left            =   5520
      Top             =   1680
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Enregistrer"
      Height          =   1095
      Left            =   3240
      TabIndex        =   6
      Top             =   1320
      Width           =   3015
   End
   Begin VB.TextBox Text2 
      Height          =   285
      Left            =   1440
      TabIndex        =   3
      Text            =   "\\.\com1"
      Top             =   1800
      Width           =   1695
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   1440
      TabIndex        =   2
      Text            =   "1200,e,7,1"
      Top             =   1320
      Width           =   1695
   End
   Begin VB.CommandButton Command1 
      Caption         =   "..."
      Height          =   375
      Left            =   4560
      Picture         =   "frmIndicate.frx":0BC2
      TabIndex        =   1
      Top             =   720
      Width           =   375
   End
   Begin INDICATELib.Indicate Indicate1 
      Height          =   615
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6255
      _Version        =   131075
      _ExtentX        =   11033
      _ExtentY        =   1085
      _StockProps     =   189
      Text            =   "Pont Bascule non connect�..."
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label4 
      Caption         =   "Fichier poids"
      Height          =   255
      Left            =   240
      TabIndex        =   12
      Top             =   2160
      Width           =   1095
   End
   Begin VB.Label Label3 
      Caption         =   "Waiting..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1200
      TabIndex        =   7
      Top             =   600
      Width           =   1815
   End
   Begin VB.Label Label2 
      Caption         =   "Conf. port"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   1800
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Port s�rie"
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   1320
      Width           =   1095
   End
End
Attribute VB_Name = "frmIndicate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public strPort As String
Public strMode As String
Public strFichier As String

Private Sub Command1_Click()

Me.Height = 3060

End Sub

Private Sub Command2_Click()
    Me.Indicate1.Disconnect
    End
End Sub

Private Sub Command3_Click()
'-> on enregistre le parametrage
SetIniString "CONFIG", "PORT", App.Path & "\PontBascule.ini", Me.Text1.Text
SetIniString "CONFIG", "MODE", App.Path & "\PontBascule.ini", Me.Text2.Text
SetIniString "CONFIG", "FICHIER", App.Path & "\PontBascule.ini", Me.Text6.Text

Me.Height = 1800
End Sub

Private Sub Form_Load()
Dim lpBuffer As String

'-> mettre la feuille au premier plan
SetWindowPos Me.Hwnd, -1, 0, 0, 0, 0, 2 Or 1

'-> on regarde si on a un fichier de config
If Dir$(App.Path & "\PontBascule.ini") <> "" Then
    lpBuffer = GetIniString("CONFIG", "PORT", App.Path & "\PontBascule.ini", False)
    If lpBuffer <> "NULL" Then strPort = Trim(lpBuffer)
    Me.Text1.Text = strPort
    lpBuffer = GetIniString("CONFIG", "MODE", App.Path & "\PontBascule.ini", False)
    If lpBuffer <> "NULL" Then strMode = Trim(lpBuffer)
    Me.Text2.Text = strMode
    lpBuffer = GetIniString("CONFIG", "FICHIER", App.Path & "\PontBascule.ini", False)
    If lpBuffer <> "NULL" Then strFichier = Trim(lpBuffer)
    Me.Text6.Text = strFichier
End If

Me.Height = 1800

'-> configuration du port s�rie, sous la notation classique windows (vitesse,parit�,data bits,stop)
If strPort = "" Then strPort = "1200,e,7,1"
'-> port s�rie de l'indicateur, sous la notation pr�fix�e \\.\ - exemple: �\\.\com1�
If strMode = "" Then strMode = "\\.\com1"

If Me.Indicate1.Connect(strPort, strMode, 6, 0, 1) Then

End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Me.Indicate1.Disconnect
    End
End Sub

Private Sub Timer1_Timer()

'-> on fige les donn�es
Me.Indicate1.Refresh

'-> on affiche les infos
Select Case Me.Indicate1.Status
    Case "B":
        Me.Label3.Caption = "Indicateur en brut"
    Case "Z":
        Me.Label3.Caption = "Indicateur � z�ro"
    Case "S":
        Me.Label3.Caption = "Le poids est stable"
    Case "I":
        Me.Label3.Caption = "Le poids est instable"
    Case "H":
        Me.Label3.Caption = "Hors gamme..."
    Case "D":
        Me.Label3.Caption = "D�faut de liaison"
End Select

'-> on affiche les infos
Me.Text3.Text = Me.Indicate1.PoidsBrut
Me.Text4.Text = Me.Indicate1.PoidsNet
Me.Text5.Text = Me.Indicate1.Tare

'-> on enregistre les resultats
SetIniString "POIDS", "BRUT", strFichier & "\poids.txt", Me.Text3.Text
SetIniString "POIDS", "NET", strFichier & "\poids.txt", Me.Text4.Text
SetIniString "POIDS", "TARE", strFichier & "\poids.txt", Me.Text5.Text
End Sub
