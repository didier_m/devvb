VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmLoadMqt 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   3600
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4230
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3600
   ScaleWidth      =   4230
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   1800
      Top             =   960
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.ListBox List1 
      Height          =   3570
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4215
   End
End
Attribute VB_Name = "frmLoadMqt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub List1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    
On Error GoTo EndError
    
Dim i As Integer

Dim PathName As String
Dim FileName As String
Dim aApp  As Application

'-> On pointe sur l'applicatif en cours
aApp = V6ClientCours.Applications("APP|" & V6App).V6PathApp
    
If UCase$(Trim(V6ClientCours.Code)) = "DEAL" Then
    PathName = aApp.V6PathApp
Else
    PathName = Replace(V6IdentPath, "$IDENT", V6ClientCours.Code)
    PathName = Replace(PathName, "$APP", V6App) & "\Maqref"
End If
    
If Shift Then MsgBox PathName, vbInformation + vbOKOnly, "Path"
    
If Button = vbRightButton And Shift = 1 Then

    '-> Afficher la boite de s�lection d'un fichier
    Me.CommonDialog1.CancelError = True
    Me.CommonDialog1.ShowOpen
    
    i = InStrRev(Me.CommonDialog1.FileName, "\")
    PathName = Mid$(Me.CommonDialog1.FileName, 1, i - 1)
    FileName = Entry(NumEntries(Me.CommonDialog1.FileName, "\"), Me.CommonDialog1.FileName, "\")
    
    '-> Retourner la valeur saisie
    RetourMqt = PathName & "|" & FileName
    Unload Me
    
End If
    
    
Exit Sub

EndError:
    
    
End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim FileName As String
Dim MaqRefPath As String
Dim i As Integer

'-> Pointer sur la bone classe
Set aLb = Libelles("FRMLOADMQT")
Me.Caption = aLb.GetCaption(1)
Me.List1.ToolTipText = aLb.GetCaption(2)

'-> Attention le path n'est pas le m�me selon que l'on soit sur l'ident DEAL ou sur un ident <>
If UCase$(Trim(V6ClientCours.Code)) = "DEAL" Then
    '-> Pointer sur le client en cours pour rechercher son propath
    MaqRefPath = Replace(V6MaqCharPath, "$APP", V6App)
Else
    MaqRefPath = Replace(V6IdentPath, "$IDENT", V6ClientCours.Code)
    MaqRefPath = Replace(MaqRefPath, "$APP", V6App) & "\Maqref"
End If

FileName = Dir$(MaqRefPath & "\*.maq")

Do While FileName <> ""
    If UCase$(Right$(FileName, 4)) = ".MAQ" Then Me.List1.AddItem FileName
    FileName = Dir
Loop

'-> on se positionne eventuellement sur l'existante
If MaqChars.Count <> 0 And Me.List1.ListCount <> 0 Then
    For i = 1 To Me.List1.ListCount
    If Trim(UCase(Me.List1.List(i))) = Trim(UCase(MaqChars(1).Nom)) Then Me.List1.Selected(i) = True
    Next
Else
    If Me.List1.ListCount > 0 Then Me.List1.Selected(0) = True
End If
    
End Sub

Private Sub List1_DblClick()
Dim aApp As Application
Dim MaqRefPath As String

'-> Attention le path n'est pas le m�me selon que l'on soit sur l'ident DEAL ou sur un ident <>
If UCase$(Trim(V6ClientCours.Code)) = "DEAL" Then
    '-> Pointer sur le client en cours pour rechercher son propath
Set aApp = V6ClientCours.Applications("APP|" & V6App)
    MaqRefPath = Replace(V6MaqCharPath, "$APP", V6App)
Else
    MaqRefPath = Replace(V6IdentPath, "$IDENT", V6ClientCours.Code)
    MaqRefPath = Replace(MaqRefPath, "$APP", V6App) & "\MaqRef"
End If

If Me.List1.ListCount > 0 Then
    RetourMqt = MaqRefPath & "|" & Me.List1.Text
    Unload Me
End If

End Sub
