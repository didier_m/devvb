VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAddObj 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ajouter un objet"
   ClientHeight    =   5280
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7485
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   352
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   499
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   3135
      Left            =   1440
      TabIndex        =   0
      Top             =   1200
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   5530
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      Appearance      =   1
   End
End
Attribute VB_Name = "frmAddObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-> Liste des objets d�ja affect�s � un rang
Public ListeObj As String

Private Sub Form_Load()

'---> Chargement ici de tous les objets de la maquette graphique

Dim aSection As Section
Dim aTb As Tableau
Dim aBlock As Block
Dim i As Integer
Dim aMaq As Maquette
Dim ToAdd As Boolean
Dim DefBlock As String
Dim aNode As Node

'-> Pointer sur la maquette
Set aMaq = Maquettes("MAQUETTE")

'-> Chargement des sections de texte
For Each aSection In aMaq.Sections
    '-> de base on ajoute la section de texte
    ToAdd = True
    '-> V�rifier que la section n'est pas d�ja affect�e dans ce rang
    If Me.ListeObj <> "" Then
        For i = 1 To NumEntries(Me.ListeObj, "|")
            '-> Tester que l'objet que l'on analyse est une section
            DefBlock = Entry(i, Me.ListeObj, "|")
            If Entry(1, DefBlock, "~") = "SECTION" Then
                '-> V�rifier que la section que l'on analyse n'est pas celle de la liste
                If UCase$(Entry(2, DefBlock, "~")) = UCase$(aSection.Nom) Then
                    ToAdd = False
                End If
           End If
        Next
    End If 'S'il y a des sections dans le rang char
    '-> Ajouter la section si necessaire
    If ToAdd Then
        Set aNode = Me.TreeView1.Nodes.Add(, , "SECTION|" & aSection.Nom, "Section de texte : " & aSection.Nom)
        aNode.Tag = "SECTION~" & Trim(UCase$(aSection.Nom))
    End If
Next 'Pour toutes les sections


'-> Chargement de la liste des tableaux
For Each aTb In aMaq.Tableaux
    '-> Pour tous les bloks du tableau
    For Each aBlock In aTb.Blocks
        '-> De base ajouter
        ToAdd = True
        '-> V�rifier si on doit ajouter
        If Trim(Me.ListeObj) <> "" Then
            '-> V�rifier si on trouve un block de tableau
            For i = 1 To NumEntries(Me.ListeObj, "|")
                '-> Get de la d�finition du block
                DefBlock = Entry(i, Me.ListeObj, "|")
                '-> V�rifier que l'on analyse un tableau
                If Entry(1, DefBlock, "~") = "TABLEAU" Then
                    '-> V�rifier que le nom du tableau soit celui du block
                    If UCase$(Trim(Entry(2, DefBlock, "~"))) = Trim(UCase$(aTb.Nom)) Then
                        '-> V�rifier si le nom du block est celui en cours
                        If UCase$(Trim(Entry(3, DefBlock, "~"))) = UCase$(Trim(aBlock.Nom)) Then
                            ToAdd = False
                        End If
                    End If 'Si pour le block dans la liste on a le m�me nom de tableau
                End If '-> Si on analyse un block ou une section
            Next 'Pour tous les blocks s�lectionn�s dans le rang
        End If 'S'il ya des blocks de tableaux r�f�renc�s
        '-> Ajouter le block si necessaire
        If ToAdd Then
            Set aNode = Me.TreeView1.Nodes.Add(, , "TB|" & aTb.Nom & "|" & aBlock.Nom, "Tableau : " & aTb.Nom & " -> Block : " & aBlock.Nom)
            aNode.Tag = "TABLEAU~" & Trim(UCase$(aTb.Nom)) & "~" & Trim(UCase$(aBlock.Nom))
        End If
    Next 'Pour tous les blocks dans le tableau
Next 'pour tous les tableaux dans la maquette

End Sub

Private Sub Form_Resize()

Dim Res As Long
Dim aRect As Rect

Res = GetClientRect(Me.hwnd, aRect)

Me.TreeView1.Left = 0
Me.TreeView1.Top = 0
Me.TreeView1.Width = aRect.Right
Me.TreeView1.Height = aRect.Bottom

End Sub


Private Sub TreeView1_DblClick()

'-> Quitter si pas de nodes
If Me.TreeView1.Nodes.Count = 0 Then Exit Sub

'-> Envoyer la valeur de retour
strRetour = Me.TreeView1.SelectedItem.Tag

'-> Decharger la feulle
Unload Me



End Sub
