VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmNaviga 
   ClientHeight    =   3765
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4305
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   3765
   ScaleWidth      =   4305
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   720
      Top             =   3120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmNaviga.frx":0000
            Key             =   "Maquette"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmNaviga.frx":27B2
            Key             =   "Section"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmNaviga.frx":2C04
            Key             =   "Cadre"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmNaviga.frx":38DE
            Key             =   "Bmp"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmNaviga.frx":3BF8
            Key             =   "Tableau"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmNaviga.frx":48D2
            Key             =   "Block"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmNaviga.frx":55AC
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmNaviga.frx":6286
            Key             =   "MaqChar"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView TreeNaviga 
      Height          =   2775
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   4895
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      FullRowSelect   =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
End
Attribute VB_Name = "frmNaviga"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()

Dim aLb As Libelle

'-> Cr�er l'objet de r�f�rence
Set aLb = Libelles("FRMNAVIGA")

'-> Charger les libell�s
Me.Caption = aLb.GetCaption(1)

'-> Liberer le pointeur
Set aLb = Nothing

'->Dimensions
Me.Width = 3000
Me.Height = 5000

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

If UnloadMode <> 4 Then Cancel = 1
Me.Hide


End Sub

Private Sub Form_Resize()

'---> Redimensionner le Treeview avec la fen�tre
    
Dim aRect As Rect
Dim Res As Long
Dim lgBordure As Long

On Error Resume Next

'-> Zone cliente de la feuille
Res = GetClientRect(Me.hwnd, aRect)

'-> Largeur de la bordure
lgBordure = GetSystemMetrics&(SM_CXEDGE)

'-> Modification du TreeView
Me.TreeNaviga.Left = 0
Me.TreeNaviga.Top = 0
Me.TreeNaviga.Height = Me.ScaleY(aRect.Bottom - aRect.Top - lgBordure, 3, 1)
Me.TreeNaviga.Width = Me.ScaleX(aRect.Right - aRect.Left - lgBordure, 3, 1)

End Sub

Private Sub TreeNaviga_DblClick()

Dim aNode As Node
Dim aMaq As Maquette
Dim aSection As Section
Dim aFrmSection As frmSection
Dim afrmTableur As frmTableur
Dim NomSection As String
Dim NomCadre As String
Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim NomBmp As String
Dim aTb As Tableau
Dim aBlock As Block
Dim afrmBlock As FrmBlock

'-> Ne rien fair s'il n'y a pas de node
If Me.TreeNaviga.Nodes.Count = 0 Then Exit Sub

'-> Pointer vers le node s�lectionn�
Set aNode = Me.TreeNaviga.SelectedItem

'-> Pointer vers l'objet maquette
Set aMaq = Maquettes("MAQUETTE")

Select Case aNode.Tag
    
    Case "MAQUETTE"
    
        '-> Faire de la maquette l'objet en cours
        Set ObjCours = aMaq
        
        '-> Afficher la feuille si necessaire
        If frmPropVisible Then frmProp.Form_Load
    
    Case "SECTION"
    
        Set aFrmSection = aMaq.frmSections(UCase$(aNode.Key))
        aFrmSection.Show
        aFrmSection.RangText.SetFocus
        
    Case "TABLEAU"
    
        Set afrmTableur = aMaq.frmTableurs(UCase$(aNode.Key))
        afrmTableur.Show
        afrmTableur.ZOrder
        
        
    Case "CADRE"
    
        '-> R�cup�rer le nom de la section
        NomSection = Entry(1, aNode.Key, "|")
        '-> R�cup�rer le nom du cadre
        NomCadre = Entry(2, aNode.Key, "|")
        '-> Pointer sur la section
        Set aSection = aMaq.Sections(UCase$(NomSection))
        '-> Pointer sur le cadre
        Set aCadre = aSection.Cadres(UCase$(NomCadre))
        '-> Afficher la feuille
        Set aFrmSection = aMaq.frmSections(UCase$(NomSection))
        aFrmSection.Show
        '-> Donner le focus au cadre
        aFrmSection.CadreTexte(aCadre.IdAffichage).SetFocus
        '-> Placer au premier plan
        aFrmSection.Cadre(aCadre.IdAffichage).ZOrder
        
    Case "BMP"
    
        '-> R�cup�rer le nom de la section
        NomSection = Entry(1, aNode.Key, "|")
        '-> R�cup�rer le nom du cadre
        NomBmp = Entry(2, aNode.Key, "|")
        '-> Pointer sur la section
        Set aSection = aMaq.Sections(UCase$(NomSection))
        '-> Pointer sur le cadre
        Set aBmp = aSection.Bmps(UCase$(NomBmp))
        '-> Afficher la feuille
        Set aFrmSection = aMaq.frmSections(UCase$(NomSection))
        aFrmSection.Show
        '-> Donner le focus au bmp
        aFrmSection.BitMap(aBmp.IdAffichage).SetFocus
        '-> Placer au premier plan
        aFrmSection.BitMap(aBmp.IdAffichage).ZOrder
        
        
    Case "BLOCK"
    
        '-> Pointer sur le tableau parent
        Set aTb = Maquettes("MAQUETTE").Tableaux(UCase$(Entry(2, aNode.Key, "|")))
        '-> Pointer sur le block
        Set aBlock = aTb.Blocks(Entry(1, aNode.Key, "|"))
        '-> V�rifier si le block est en cours d'�dition
        If aBlock.IsEdit Then
            '-> Afficher le block
            Set afrmBlock = aTb.frmBlocks(UCase$(aBlock.Nom))
            afrmBlock.ZOrder
        Else
            '-> Afficher le tableau
            Set afrmTableur = aMaq.frmTableurs(UCase$(Entry(2, aNode.Key, "|")))
            afrmTableur.Show
            afrmTableur.ZOrder
        End If
        
    Case "MAQCHAR"
    
        frmMaqChar.Show
        frmMaqChar.ZOrder
        
        
End Select


'-> Lib�rer les pointeurs
Set aNode = Nothing
Set aFrmSection = Nothing
Set afrmTableur = Nothing
Set afrmBlock = Nothing
Set aTb = Nothing
Set aBlock = Nothing
Set aMaq = Nothing
Set aCadre = Nothing
Set aBmp = Nothing

End Sub
