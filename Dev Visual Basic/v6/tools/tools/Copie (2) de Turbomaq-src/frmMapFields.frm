VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmMapFields 
   Caption         =   "Form1"
   ClientHeight    =   5715
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8460
   LinkTopic       =   "Form1"
   ScaleHeight     =   5715
   ScaleWidth      =   8460
   StartUpPosition =   2  'CenterScreen
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
      Height          =   4335
      Left            =   120
      TabIndex        =   1
      Top             =   720
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   7646
      _Version        =   393216
      Rows            =   0
      FixedRows       =   0
      FixedCols       =   0
      ScrollTrack     =   -1  'True
      ScrollBars      =   2
   End
   Begin MSComctlLib.ImageCombo ImageCombo1 
      Height          =   570
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   1005
      _Version        =   393216
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Locked          =   -1  'True
      ImageList       =   "ImageList1"
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   1200
      Top             =   5160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMapFields.frx":0000
            Key             =   "Section"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMapFields.frx":0452
            Key             =   "Cadre"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMapFields.frx":112C
            Key             =   "Bmp"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMapFields.frx":1446
            Key             =   "Tableau"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMapFields.frx":2120
            Key             =   "Block"
         EndProperty
      EndProperty
   End
   Begin VB.Image Ok 
      Height          =   480
      Left            =   7800
      Top             =   5160
      Width           =   630
   End
   Begin VB.Menu mnuFormat 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu MnuFormatZone 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmMapFields"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private IsLoading As Boolean

Private Sub Form_Load()

'-> Anlyser tous les objets � la recherche des champs existants
Dim aMaq As Maquette
Dim aTb As Tableau
Dim aSection As Section
Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim i As Integer
Dim aLb As Libelle


Set aLb = Libelles("FRMMAPFIELDS")
Me.Caption = aLb.GetCaption(1)

Set aLb = Libelles("BOUTONS")
Me.Ok.ToolTipText = aLb.GetCaption(3)

Me.Ok.Picture = Turbo_Maq.Ok.Picture

'-> Pointer sur la maquette
Set aMaq = Maquettes("MAQUETTE")

'-> Ajouter la liste de tous les objets de premier niveau
For Each aSection In aMaq.Sections
    Me.ImageCombo1.ComboItems.Add , "SECTION|" & UCase$(aSection.Nom) & "|" & Me.ImageCombo1.ComboItems.Count, aSection.Nom, "Section"
    Me.ImageCombo1.ComboItems(Me.ImageCombo1.ComboItems.Count).Selected = True
    DisplayFields
Next

For Each aTb In aMaq.Tableaux
    Me.ImageCombo1.ComboItems.Add , "TABLEAU|" & UCase$(aTb.Nom) & "|" & Me.ImageCombo1.ComboItems.Count, aTb.Nom, "Tableau"
    Me.ImageCombo1.ComboItems(Me.ImageCombo1.ComboItems.Count).Selected = True
    DisplayFields
Next

'IsLoading = False

If Me.ImageCombo1.ComboItems.Count <> 0 Then
    Me.ImageCombo1.ComboItems(1).Selected = True
    DisplayFields
End If
    



End Sub

Private Sub DisplayFields()

'---> Cette fonction r�cup�re tous les champs associ�s � un objet et � ses sous objets

Dim aMaq As Maquette
Dim aTb As Tableau
Dim aSection As Section
Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim aBlock As Block
Dim i As Integer
Dim aCell As Cellule
Dim SectionText As String
Dim aFrmSection As frmSection

If Me.ImageCombo1.ComboItems.Count = 0 Then Exit Sub

'-> Pointer sur la maquette
Set aMaq = Maquettes("MAQUETTE")

Me.MSFlexGrid1.Redraw = False

'-> Formater le nombre de colonne
Me.MSFlexGrid1.Rows = 0
Me.MSFlexGrid1.Cols = 4
For i = 0 To 3
    Me.MSFlexGrid1.ColWidth(i) = Me.MSFlexGrid1.Width / 4
Next

Me.MSFlexGrid1.ColWidth(3) = 0.8 * Me.MSFlexGrid1.ColWidth(3)

Select Case UCase$(Entry(1, Me.ImageCombo1.SelectedItem.Key, "|"))

    Case "TABLEAU"
        
        Set aTb = aMaq.Tableaux(Entry(2, Me.ImageCombo1.SelectedItem.Key, "|"))
        For Each aBlock In aTb.Blocks
            For Each aCell In aBlock.Cellules
                CreateMapField aCell, aBlock.Nom
            Next '-> Pour toutes les cellules
            
        Next '-> Pour tous les blocks
        '-> Tester s'il y a des cellules
        If Trim(ListeField) = "" Then
        Else
            
        
        End If 'S'il y a des champs
    
    Case "SECTION"
    
        '-> Pointer sur la section m�re
        Set aSection = aMaq.Sections(Entry(2, Me.ImageCombo1.SelectedItem.Key, "|"))
        Set aFrmSection = aMaq.frmSections(UCase$(aSection.Nom))
        
        '-> Analyser la liste des champs
        SectionText = aFrmSection.RangText.Text
        
        
        '-> Cr�ation de la liste des champs
        CreateMapField aSection, SectionText
        
        '-> Pour tous les cadres attach�s � la section
        For Each aCadre In aSection.Cadres
            SectionText = aFrmSection.CadreTexte(aCadre.IdAffichage).Text
            '-> Cr�ation de la liste des champs
            CreateMapField aCadre, SectionText
        Next 'Pour tous les cadres
        
        For Each aBmp In aSection.Bmps
            If aBmp.isVariable Then
                Me.MSFlexGrid1.Rows = Me.MSFlexGrid1.Rows + 1
                Me.MSFlexGrid1.Row = Me.MSFlexGrid1.Rows - 1
                Me.MSFlexGrid1.Col = 0
                Me.MSFlexGrid1.Text = "Bmp"
                Me.MSFlexGrid1.Col = 1
                Me.MSFlexGrid1.Text = aBmp.Nom
                Me.MSFlexGrid1.Col = 2
                Me.MSFlexGrid1.Text = aBmp.Fichier
                Me.MSFlexGrid1.Col = 3
                Me.MSFlexGrid1.Text = CStr(aBmp.FormatEdit)
            End If
        Next 'Pour tous les bp
        
        Set aFrmSection = Nothing
    
End Select

Me.MSFlexGrid1.Redraw = True

End Sub
Private Sub CreateMapField(ByVal aObject As Object, Optional TextToMap As String)


Dim ListeField As String
Dim FindField As Boolean
Dim DefField As String
Dim aField As Field
Dim FieldToCreate() As String

If TypeOf aObject Is Cellule Then
    '-> R�cup�ration de la liste des champs de l'objet
    ListeField = MapFields(aObject.Contenu)
Else
    ListeField = MapFields(TextToMap)
End If
If ListeField <> "" Then
    '-> V�rification s'il l'objet champ associ� existe
    For i = 1 To NumEntries(ListeField, "@")
        FindField = False
        '-> R�cup�ration de la d�finition du champ
        DefField = Entry(i, ListeField, "@")
        '-> Cr�ation de tous les champs existants
        For Each aField In aObject.Fields
            If UCase$(DefField) = UCase$(aField.Name) Then
                aField.Present = True
                FindField = True
                Exit For
            Else
                If InStr(1, UCase$(ListeField), UCase$(aField.Name)) = 0 Then aField.Present = False
            End If
        Next 'Pour tous les champs dans l'object
        '-> Si le champ n'a pas �t� trouv� l'ajouter
        If Not FindField Then
            Set aField = New Field
            aField.Name = DefField
            aField.Present = True
            aField.FormatEdit = 50
            aObject.Fields.Add aField, UCase$(DefField)
        End If
    Next 'Pour tous les champs
    '-> Tous les champs ont �t� test� : supprim�s ceux qui ne sont pas pr�sent
    For Each aField In aObject.Fields
        If Not aField.Present Then
            aObject.Fields.Remove aField.Name
        Else
            '-> Ajouter le champ dans la liste
            Me.MSFlexGrid1.Rows = Me.MSFlexGrid1.Rows + 1
            Me.MSFlexGrid1.Row = Me.MSFlexGrid1.Rows - 1
            Me.MSFlexGrid1.Col = 0
            If TypeOf aObject Is Cellule Then
                Me.MSFlexGrid1.Text = "Block : " & TextToMap
            ElseIf TypeOf aObject Is Section Then
                Me.MSFlexGrid1.Text = "Text"
            ElseIf TypeOf aObject Is Cadre Then
                Me.MSFlexGrid1.Text = "Cadre"
            End If
            Me.MSFlexGrid1.Col = 1
            If TypeOf aObject Is Cellule Then
                Me.MSFlexGrid1.Text = "L" & aObject.Ligne & "C" & aObject.Colonne
            Else
                Me.MSFlexGrid1.Text = aObject.Nom
            End If
            Me.MSFlexGrid1.Col = 2
            Me.MSFlexGrid1.Text = "^" & aField.Name
            Me.MSFlexGrid1.Col = 3
            Me.MSFlexGrid1.Text = CStr(aField.FormatEdit)
        End If 'Si le champ existe encore
    Next '-> Pour tous les objets champs
End If 'S'il y a des champs

End Sub

Private Sub Form_Resize()

On Error Resume Next

Dim aRect As Rect
Dim Res As Long
Dim i As Integer

Res = GetClientRect(Me.hWnd, aRect)
aRect.Top = Me.ScaleY(aRect.Top, 3, 1)
aRect.Bottom = Me.ScaleY(aRect.Bottom, 3, 1)
'---> Repositionner les diff�rents objets
Me.ImageCombo1.Width = Me.Width - Me.ImageCombo1.Left * 2.8
Me.MSFlexGrid1.Width = Me.ImageCombo1.Width
Me.MSFlexGrid1.Height = Me.Height - Me.ImageCombo1.Height - Me.MSFlexGrid1.Top - Me.Ok.Height * 2
Me.Ok.Top = Me.MSFlexGrid1.Top + Me.MSFlexGrid1.Height + ((aRect.Bottom - aRect.Top) - (Me.MSFlexGrid1.Top + Me.MSFlexGrid1.Height)) / 2
Me.Ok.Left = Me.MSFlexGrid1.Left + Me.MSFlexGrid1.Width - Me.Ok.Width
For i = 0 To 3
    Me.MSFlexGrid1.ColWidth(i) = Me.MSFlexGrid1.Width / 4
Next

Me.MSFlexGrid1.ColWidth(3) = 0.8 * Me.MSFlexGrid1.ColWidth(3)


End Sub

Private Sub ImageCombo1_Change()
    DisplayFields
End Sub

Private Sub ImageCombo1_Click()
    DisplayFields
End Sub

Private Sub MnuFormatZone_Click()

Dim Rep
Dim aLb As Libelle
Dim Min As Integer, Max As Integer, i As Integer
Dim aCell As Cellule
Dim aBlock As Block
Dim aTb As Tableau
Dim aSection As Section
Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim aField As Field
Dim aMaq As Maquette

Set aLb = Libelles("FRMMAPFIELDS")
Rep = InputBox(aLb.GetCaption(2), aLb.GetCaption(1), "")
If Rep = "" Then Exit Sub
If Not IsNumeric(Rep) Then
    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
    Exit Sub
End If

If CLng(Rep) > 500 Then
    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
    Exit Sub
End If

Me.MSFlexGrid1.Redraw = False

'-> R�cup�rer les min et max
GetMinMax Min, Max
'-> Pointer sur l'objet de premier niveau
Set aMaq = Maquettes("MAQUETTE")
Select Case UCase$(Entry(1, Me.ImageCombo1.SelectedItem.Key, "|"))
    Case "TABLEAU"
        Set aTb = aMaq.Tableaux(Entry(2, Me.ImageCombo1.SelectedItem.Key, "|"))
        For i = Min To Max
            '-> Se positionner en premi�re colonne pour pointer sur le blocK
            Me.MSFlexGrid1.Row = i
            Me.MSFlexGrid1.Col = 0
            Set aBlock = aTb.Blocks("BL-" & UCase$(Trim(Entry(2, Me.MSFlexGrid1.Text, ":"))))
            '-> Se positionner sur la cellule
            Me.MSFlexGrid1.Col = 1
            Set aCell = aBlock.Cellules(UCase$(Me.MSFlexGrid1.Text))
            '-> Se positionner sur le champ
            Me.MSFlexGrid1.Col = 2
            Set aField = aCell.Fields(Mid$(UCase$(Me.MSFlexGrid1.Text), 2, 4))
            aField.FormatEdit = Abs(CInt(Rep))
            Me.MSFlexGrid1.Col = 3
            Me.MSFlexGrid1.Text = aField.FormatEdit
            Set aField = Nothing
            Set aCell = Nothing
            Set aBlock = Nothing
        Next 'Pour toutes les cellules s�lectinn�es
    
    Case "SECTION"
        Set aSection = aMaq.Sections(Entry(2, Me.ImageCombo1.SelectedItem.Key, "|"))
        For i = Min To Max
            '-> Se positionner sur la premi�re colonne
            Me.MSFlexGrid1.Row = i
            Me.MSFlexGrid1.Col = 0
            Select Case UCase$(Me.MSFlexGrid1.Text)
                Case "TEXT"
                    '-> R�cup�rer le champ
                    Me.MSFlexGrid1.Col = 2
                    Set aField = aSection.Fields(Mid$(UCase$(Me.MSFlexGrid1.Text), 2, 4))
                Case "CADRE"
                    '-> R�cup�rer le nom du cadre
                    Me.MSFlexGrid1.Col = 1
                    Set aCadre = aSection.Cadres(UCase$(Me.MSFlexGrid1.Text))
                    '-> Pointer sur le champ
                    Me.MSFlexGrid1.Col = 2
                    Set aField = aCadre.Fields(Mid$(UCase$(Me.MSFlexGrid1.Text), 2, 4))
                Case "BMP"
                    '-> R�cup�rer le nom du bmp
                    Me.MSFlexGrid1.Col = 1
                    Set aBmp = aSection.Bmps(UCase$(Me.MSFlexGrid1.Text))
                    aBmp.FormatEdit = Abs(CInt(Rep))
            End Select
            If UCase$(Me.MSFlexGrid1.Text) <> "BMP" Then
                '-> Setting des informations
                aField.FormatEdit = Abs(CInt(Rep))
                Me.MSFlexGrid1.Col = 3
                Me.MSFlexGrid1.Text = aField.FormatEdit
            End If
        Next 'Pour toutes les lignes s�lectionn�es
                        
End Select
    
Me.MSFlexGrid1.Redraw = True

End Sub

Private Sub MSFlexGrid1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim aLb As Libelle

If Me.MSFlexGrid1.Rows = 0 Then Exit Sub

If Button = vbRightButton Then
    Set aLb = Libelles("FRMMAPFIELDS")
    Me.MnuFormatZone.Caption = aLb.GetCaption(1)
    Set aLb = Nothing
    Me.PopupMenu Me.mnuFormat
End If

End Sub


Private Sub GetMinMax(Min As Integer, Max As Integer)

If Me.MSFlexGrid1.Row > Me.MSFlexGrid1.RowSel Then
    Min = Me.MSFlexGrid1.RowSel
    Max = Me.MSFlexGrid1.Row
Else
    Max = Me.MSFlexGrid1.RowSel
    Min = Me.MSFlexGrid1.Row
End If



End Sub

Private Sub Ok_Click()
    Unload Me
End Sub
