VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmTransition 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6315
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   9870
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6315
   ScaleWidth      =   9870
   Begin VB.CommandButton Command2 
      Caption         =   ">>"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5160
      TabIndex        =   13
      Top             =   2880
      Width           =   615
   End
   Begin VB.CommandButton Command1 
      Caption         =   "<<"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5160
      TabIndex        =   12
      Top             =   2400
      Width           =   615
   End
   Begin MSComctlLib.TreeView TreeView2 
      CausesValidation=   0   'False
      Height          =   3255
      Left            =   5880
      TabIndex        =   10
      Top             =   2400
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   5741
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin MSComctlLib.TreeView TreeView1 
      CausesValidation=   0   'False
      Height          =   3255
      Left            =   120
      TabIndex        =   9
      Top             =   2400
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   5741
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   1
      FullRowSelect   =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      Height          =   1815
      Left            =   4200
      TabIndex        =   2
      Top             =   120
      Width           =   5535
      Begin VB.CheckBox Check1 
         BackColor       =   &H00C0C0C0&
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1320
         Width           =   4455
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Height          =   255
         Left            =   3000
         TabIndex        =   6
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label Label3 
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   840
         Width           =   2655
      End
      Begin VB.Label NomRG 
         Alignment       =   2  'Center
         Height          =   255
         Left            =   3000
         TabIndex        =   4
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label Label2 
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   2055
      End
   End
   Begin VB.ListBox List1 
      Height          =   1425
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   3855
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTransition.frx":0000
            Key             =   "Section"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTransition.frx":0452
            Key             =   "Tableau"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTransition.frx":112C
            Key             =   "Block"
         EndProperty
      EndProperty
   End
   Begin VB.Image Annuler 
      Height          =   480
      Left            =   7680
      Top             =   5760
      Width           =   630
   End
   Begin VB.Image Aide 
      Height          =   480
      Left            =   8400
      Top             =   5760
      Width           =   630
   End
   Begin VB.Image Ok 
      Height          =   480
      Left            =   9120
      Top             =   5760
      Width           =   630
   End
   Begin VB.Image Image2 
      Height          =   480
      Left            =   5205
      Picture         =   "TM-frmTransition.frx":1E06
      Top             =   3600
      Width           =   480
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   5205
      Picture         =   "TM-frmTransition.frx":2AD0
      Top             =   4200
      Width           =   480
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Height          =   255
      Left            =   5880
      TabIndex        =   11
      Top             =   2040
      Width           =   2775
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   2040
      Width           =   3855
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3855
   End
End
Attribute VB_Name = "frmTransition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private IsLoading As Boolean

Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub Check1_Click()

Dim aRg As RangChar
    
Set aRg = MaqChars("MAQCHAR").RangChars(UCase$(Me.List1.Text))
If Me.Check1.Value = 1 Then
    aRg.IsSautPage = True
Else
    aRg.IsSautPage = False
End If
    
Set aRg = Nothing

    
End Sub

Private Sub Command1_Click()

Dim aRg As RangChar

On Error GoTo GestError


'---> Ajouter le nouveau node dans les associations
Select Case UCase$(Entry(1, Me.TreeView2.SelectedItem.Key, "-"))

    Case "SECTION"
        '-> V�rifier s'il n'existe pas d�ja
        If Not (VerifNode("SECTION-" & UCase$(Entry(2, Me.TreeView2.SelectedItem.Key, "-")))) Then
            '->Ajouter le lien dans le treeview
            Me.TreeView1.Nodes.Add , , UCase$(Me.TreeView2.SelectedItem.Key), Me.TreeView2.SelectedItem.Text, "Section"
            '-> Pointer sur le rang
            Set aRg = MaqChars("MAQCHAR").RangChars(UCase$(Me.List1.Text))
            '-> Ajouter le lien dans l'objet RangChar
            aRg.AddLien UCase$(Me.TreeView2.SelectedItem.Key)
        End If
    Case "TB"
        '-> Ne rien faire
    Case "TABLEAU"
        '-> V�rifier s'il n'existe pas d�ja
        If Not VerifNode(UCase$(Me.TreeView2.SelectedItem.Key)) Then
            '-> Ajouter le lien dans le treeview
            Me.TreeView1.Nodes.Add , , Me.TreeView2.SelectedItem.Key, "Tableau : " & Entry(2, Me.TreeView2.SelectedItem.Key, "-") & " " & Me.TreeView2.SelectedItem.Text, "Block"
            '-> Pointer sur le rang
            Set aRg = MaqChars("MAQCHAR").RangChars(UCase$(Me.List1.Text))
            '-> Ajouter le lien dans l'objet RangChar
            aRg.AddLien UCase$(Me.TreeView2.SelectedItem.Key)
        End If
End Select

'-> S�lectionner le node
Me.TreeView1.Nodes(Me.TreeView2.SelectedItem.Key).Selected = True

GestError:

    '-> Liberer le pointeur
    Set aRg = Nothing

End Sub

Private Sub Command2_Click()

Dim aRg As RangChar
Dim i As Integer
Dim Key As String



'-> Quitter s'il n'y a pas de nodes
If Me.TreeView1.Nodes.Count = 0 Then Exit Sub

'-> Pointer sur le Rg�
Set aRg = MaqChars("MAQCHAR").RangChars(UCase$(Me.List1.Text))

'-> Supprimer le node dans la relation avec l'objet
Select Case UCase$(Entry(1, Me.TreeView1.SelectedItem.Key, "-"))
    Case "SECTION"
        aRg.DelLien Me.TreeView1.SelectedItem.Key
    Case "TABLEAU"
        aRg.DelLien UCase$(Me.TreeView1.SelectedItem.Key)
End Select

'-> Supprimer le node sur lequel on click
Me.TreeView1.Nodes.Remove Me.TreeView1.SelectedItem.Key

'-> Lib�rer le pointeur sur le rang
Set aRg = Nothing

End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim aRg As RangChar
Dim i As Integer, j As Integer
Dim NomObj As String
Dim aMaq As Maquette
Dim aTb As Tableau

Set aLb = Libelles("FRMTRANSITION")

Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Frame1.Caption = aLb.GetCaption(3)
Me.Label2.Caption = aLb.GetCaption(4)
Me.Label3.Caption = aLb.GetCaption(5)
Me.Check1.Caption = aLb.GetCaption(6)
Me.Label5.Caption = aLb.GetCaption(7)
Me.Label6.Caption = aLb.GetCaption(8)
'Me.Frame2.Caption = aLb.GetCaption(9)
Me.Label4.ToolTipText = aLb.GetCaption(10)
Me.Command1.ToolTipText = aLb.GetCaption(11)
Me.Command2.ToolTipText = aLb.GetCaption(12)
Me.Ok.ToolTipText = aLb.GetCaption(13)
Me.Image1.ToolTipText = aLb.GetCaption(16)
Me.Image2.ToolTipText = aLb.GetCaption(15)

Me.Annuler.Picture = Turbo_Maq.Annuler.Picture
Me.Aide.Picture = Turbo_Maq.Aide.Picture
Me.Ok.Picture = Turbo_Maq.Ok.Picture

Set aLb = Libelles("BOUTONS")
Me.Annuler.ToolTipText = aLb.GetCaption(1)
Me.Aide.ToolTipText = aLb.GetCaption(2)

'-> indiquer que l'on est en cours de chargement de la feuille
IsLoading = True

'-> Ajout des rangs caract�res existants
For Each aRg In MaqChars("MAQCHAR").RangChars
    '-> Ne pas rajouter les rangs de texte de d�but et de fin
    If UCase$(aRg.Nom) = "ENTETE" Or UCase$(aRg.Nom) = "FIN" Then
    Else
        Me.List1.AddItem aRg.Nom
    End If
Next

'-> indiquer la fin du chargement
IsLoading = False

'-> Afficher les propri�t�s du premier rang carcat�re
If Me.List1.ListCount <> 0 Then Me.List1.Selected(0) = True

'-> Charger tous les objets de la maquette dans l'ordre des ordres d'affichage
Set aMaq = Maquettes("MAQUETTE")
For i = 1 To aMaq.nEntries - 1
    '-> R�cup�rer l'entr�e
    NomObj = aMaq.GetOrdreAffichage(i)
    If UCase$(Entry(1, NomObj, "-")) = "SCT" Then
        Me.TreeView2.Nodes.Add , , "SECTION-" & Entry(2, NomObj, "-"), "Section de texte : " & Entry(2, NomObj, "-"), "Section"
    ElseIf UCase$(Entry(1, NomObj, "-")) = "TB" Then
        '-> Ajouter le node du tableau
        Me.TreeView2.Nodes.Add , , "TB-" & Entry(2, NomObj, "-"), "Tableau : " & Entry(2, NomObj, "-"), "Tableau"
        '-> Pointer sur le tableau
        Set aTb = aMaq.Tableaux(UCase$(Entry(2, NomObj, "-")))
        For j = 1 To aTb.nBlocks - 1
            Me.TreeView2.Nodes.Add "TB-" & Entry(2, NomObj, "-"), 4, "TABLEAU-" & Entry(2, NomObj, "-") & "-" & Entry(2, aTb.GetOrdreAffichage(j), "-"), "Block : " & Entry(2, aTb.GetOrdreAffichage(j), "-"), "Block"
        Next
    End If
Next

'-> Liberer les pointeurs
Set aMaq = Nothing
Set aRg = Nothing
Set aLb = Nothing
Set aTb = Nothing

End Sub

Private Function DisplayLiens(ByVal NomRG As String) As Boolean

'---> cette proc�dure ajoute dans le treeview associ� les objets graphques associ�s
Dim aRg As RangChar
Dim i As Integer
Dim Img As String
Dim DefLien As String
Dim Libelle As String

On Error GoTo ErrLoadAsso

'-> Effacer le treeview
Me.TreeView1.Nodes.Clear
Me.NomRG.Caption = ""
Me.Label4.Caption = ""

'-> Pointer sur le rang associ�
Set aRg = MaqChars("MAQCHAR").RangChars(UCase$(NomRG))

'-> Faire un test pour voir s'il existe des associations
If aRg.nLiens = 1 Then LoadAsso NomRG

'-> Ajouter les associations
For i = 1 To aRg.nLiens - 1
    DefLien = aRg.GetLien(i)
    Select Case UCase$(Entry(1, DefLien, "-"))
        Case "SECTION"
            Libelle = "Section de texte : " & Entry(2, DefLien, "-")
            Img = "Section"
        Case "TABLEAU"
            Libelle = "Tableau : " & Entry(2, DefLien, "-") & " Block : " & Entry(3, DefLien, "-")
            Img = "Block"
    End Select
    '-> Rajouter le lien dans le treeview
    Me.TreeView1.Nodes.Add , , DefLien, Libelle, Img
Next
'-> Afficher les informations
Me.NomRG.Caption = aRg.Nom
Me.Label4.Caption = aRg.nLignes - 1
If aRg.IsSautPage Then Me.Check1.Value = 1 Else Me.Check1.Value = 0

'-> Renvoyer une valeur de succ�s
DisplayLiens = True

'-> S�lectionner le premier node s'il y en a un
If Me.TreeView1.Nodes.Count <> 0 Then Me.TreeView1.Nodes(1).Selected = True

Set aRg = Nothing

Exit Function

ErrLoadAsso:

    '-> Renvoyer une valeur d'�chec
    DisplayLiens = False

End Function
Private Function LoadAsso(ByVal SearchRg As String) As String

Dim aSection As Section
Dim aTb As Tableau
Dim aBlock As Block
Dim aRg As RangChar

On Error Resume Next

'-> Pointer sur le rang
Set aRg = MaqChars("MAQCHAR").RangChars(UCase$(SearchRg))

'---> Fonction qui renvoie les objets li�s par la liste des champs

'-> Analyse des sections
For Each aSection In Maquettes("MAQUETTE").Sections
    If UCase$(aSection.RangChar) = UCase$(SearchRg) Then
        '-> Ajouter dans le rang l'association
        aRg.AddLien "SECTION-" & UCase$(aSection.Nom)
    End If
Next

'-> analyse des tableaux
For Each aTb In Maquettes("MAQUETTE").Tableaux
    For Each aBlock In aTb.Blocks
        If UCase$(aBlock.RangChar) = UCase$(SearchRg) Then
            '-> Ajouter le lien
            aRg.AddLien "TABLEAU-" & UCase$(aTb.Nom) & "-" & UCase$(aBlock.Nom)
        End If
    Next
Next

Set aSection = Nothing
Set aTb = Nothing
Set aBlock = Nothing
Set aRg = Nothing

End Function


Private Sub Image1_Click()

'---> Diminue l'ordre d'affichage d'un objet

Dim Matrice() As String
Dim Id As Integer
Dim i As Integer
Dim aRg As RangChar
Dim n As Integer
Dim Key As String

On Error GoTo GestError

'-> Ne rien faire si on est sur le dernier node
If Me.TreeView1.SelectedItem.Index = Me.TreeView1.Nodes.Count Then Exit Sub

'-> Dimensionner la matrice au nombre de nodes
ReDim Matrice(1 To Me.TreeView1.Nodes.Count)

'-> R�cup�ration de l'index du node � monter et de sa cl�
Key = Me.TreeView1.SelectedItem.Key
Id = Me.TreeView1.SelectedItem.Index

'-> Pointer sur l'objet rang
Set aRg = MaqChars("MAQCHAR").RangChars(UCase$(Me.List1.Text))

'-> R�cup�ration de la matrice
n = aRg.nLiens - 1
For i = aRg.nLiens - 1 To 1 Step -1
    '-> R�cup�ration de la valeur
    Matrice(i) = aRg.GetLien(i)
    '-> Supression de l'�l�ment
    aRg.DelLien (Matrice(i))
Next 'Pour tous les liens

'-> Recr�er la matrice
For i = 1 To n
    If i = Id Then
        aRg.AddLien Matrice(i + 1)
    ElseIf i = Id + 1 Then
        aRg.AddLien Matrice(i - 1)
    Else
        aRg.AddLien Matrice(i)
    End If
Next

'-> supprimer la matrice
Erase Matrice()

'-> Afficher les liens
DisplayLiens UCase$(Me.List1.Text)

'-> Liberer le pointeur
Set aRg = Nothing
        
'-> S�lectionner l'ancien �l�ment
Me.TreeView1.Nodes(Key).Selected = True

GestError:

    Exit Sub

End Sub

Private Sub Image2_Click()

'---> Augmente l'ordre d'affichage d'un objet

Dim Matrice() As String
Dim Id As Integer
Dim i As Integer
Dim aRg As RangChar
Dim n As Integer
Dim Key As String

On Error GoTo GestError

'-> Ne rien faire si on est sur le node 1
If Me.TreeView1.SelectedItem.Index = 1 Then Exit Sub

'-> Dimensionner la matrice au nombre de nodes
ReDim Matrice(1 To Me.TreeView1.Nodes.Count)

'-> R�cup�ration de l'index du node � monter et de sa cl�
Key = Me.TreeView1.SelectedItem.Key
Id = Me.TreeView1.SelectedItem.Index

'-> Pointer sur l'objet rang
Set aRg = MaqChars("MAQCHAR").RangChars(UCase$(Me.List1.Text))

'-> R�cup�ration de la matrice
n = aRg.nLiens - 1
For i = aRg.nLiens - 1 To 1 Step -1
    '-> R�cup�ration de la valeur
    Matrice(i) = aRg.GetLien(i)
    '-> Supression de l'�l�ment
    aRg.DelLien (Matrice(i))
Next 'Pour tous les liens

'-> Recr�er la matrice
For i = 1 To n
    If i = Id - 1 Then
        aRg.AddLien Matrice(i + 1)
    ElseIf i = Id Then
        aRg.AddLien Matrice(i - 1)
    Else
        aRg.AddLien Matrice(i)
    End If
Next

'-> supprimer la matrice
Erase Matrice()

'-> Afficher les liens
DisplayLiens UCase$(Me.List1.Text)

'-> Liberer le pointeur
Set aRg = Nothing
        
'-> S�lectionner l'ancien �l�ment
Me.TreeView1.Nodes(Key).Selected = True

GestError:

    Exit Sub

End Sub

Private Sub Image3_Click()
    frmMapFields.Show vbModal
End Sub

Private Sub Label4_DblClick()

Dim aLb As Libelle
Dim Rep

Set aLb = Libelles("MESSTRANSITION")
Rep = InputBox(aLb.GetCaption(6), aLb.GetToolTip(6), Me.Label4.Caption)
If Rep = "" Then Exit Sub
If Not IsNumeric(Rep) Then GoTo ErrSaisie
If CInt(Rep) < 0 Then GoTo ErrSaisie
If CInt(Rep) > 100 Then GoTo ErrSaisie

Me.Label4.Caption = Rep

Set aLb = Nothing
Exit Sub

ErrSaisie:

    Set aLb = Libelles("MESSTRANSITION")
    MsgBox aLb.GetCaption(7), vbCritical + vbOKOnly, aLb.GetToolTip(7)
    Set aLb = Nothing
    Exit Sub
    

End Sub

Private Sub List1_Click()

Dim aLb As Libelle

If IsLoading Then Exit Sub

If Not DisplayLiens(Me.List1.Text) Then
    Set aLb = Libelles("MESSTRANSITION")
    MsgBox aLb.GetCaption(5) & Chr(13) & Me.List1.Text, vbCritical + vbOKOnly, aLb.GetToolTip(5)
    Set aLb = Nothing
    Exit Sub
End If

End Sub

Private Function VerifNode(ByVal NodeKey As String) As Boolean

'---> Rechercher si la liste existe
Dim aNode As Node

For Each aNode In Me.TreeView1.Nodes
    If UCase$(aNode.Key) = UCase$(NodeKey) Then
        VerifNode = True
        Exit Function
    End If
Next

VerifNode = False

Set aNode = Nothing

End Function

Private Sub Ok_Click()
    '-> on sauvegarde la maquette de transition
    SaveTransition
End Sub

Public Sub SaveTransition(Optional bQuick As Boolean)

'---> G�n�ration de la maquette de transition
Dim MaqFic As Integer
Dim aLb As Libelle
Dim aRg As RangChar
Dim aSection As Section
Dim aTb As Tableau
Dim aBlock As Block
Dim aMaq As Maquette
Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim i As Integer, j As Integer, k As Integer, l As Integer
Dim ListeChamp As String
Dim TypeLien As String
Dim LigneToPrint As String
Dim NbLigne As Integer
Dim aFrm As Form
Dim Rep
Dim aApp As Application
Dim Res As Long
Dim lpBuffer As String
Dim aField As Field
Dim FormatEdit() As Integer
Dim CharVar As String
Dim DelBlanckLigne As Boolean
Dim MaqTransPath As String

On Error GoTo GestError

'-> Pointer sur la classe message
Set aLb = Libelles("FRMTRANSITION")

'-> R�cup�rer le nom de la maquette de transition
If Trim(MaqChars("MAQCHAR").MaqTransition) = "" Then
    '-> Afficher le nom
    ListeChamp = Entry(1, MaqChars("MAQCHAR").Nom, ".")
    MaqChars("MAQCHAR").MaqTransition = ListeChamp
Else
    ListeChamp = MaqChars("MAQCHAR").MaqTransition
End If

If Not bQuick Then
    lpBuffer = InputBox(aLb.GetCaption(18), aLb.GetCaption(19), ListeChamp)
    If Trim(lpBuffer) = "" Then Exit Sub
Else
    lpBuffer = ListeChamp
End If
ListeChamp = ""

'-> Pointer sur le client en cours pour rechercher son propath
Set aApp = V6ClientCours.Applications("APP|" & V6App)

'-> Attention le path n'est pas le m�me selon que l'on soit sur l'ident DEAL ou sur un ident <>
If UCase$(Trim(V6ClientCours.Code)) = "DEAL" Then
    MaqTransPath = aApp.V6PathApp
Else
    MaqTransPath = Replace(V6IdentPath, "$IDENT", V6ClientCours.Code)
    MaqTransPath = Replace(MaqTransPath, "$APP", V6App) & "\"
End If


'-> v�rifier si le fichier existe
If Dir$(MaqTransPath & lpBuffer & ".maq") <> "" Then
    If Not bQuick Then
        Rep = MsgBox(aLb.GetCaption(14), vbQuestion + vbYesNo, aLb.GetToolTip(14))
        If Rep = vbNo Then
            Unload Me
            Exit Sub
        End If
    End If
    '-> supprimer dans un premier temps le fichier
    Kill MaqTransPath & lpBuffer & ".maq"
End If

'-> Afficher la feuille des gestion du Lig et du Max
If Not bQuick Then
    frmLIGMAX.NomMaq = lpBuffer & ".maq"
    frmLIGMAX.PathApp = MaqTransPath & "Param.cfg"
    frmLIGMAX.Show vbModal
End If

'-> Afficher la feuille de saisie des largeurs de rang
If Not bQuick Then
    frmMapFields.Show vbModal
End If

'-> Demander si on doit supprimer les lignes � blanc
If Not bQuick Then
    Rep = MsgBox(aLb.GetCaption(20), vbQuestion + vbYesNo, aLb.GetToolTip(20))
    If Rep = vbYes Then DelBlanckLigne = True
Else
    DelBlanckLigne = True
End If

'-> Lib�rer le pointeur de messprog
Set aLb = Nothing

'-> Pointer sur la maquette
Set aMaq = Maquettes("MAQUETTE")

'-> G�n�ration de la maquette de transition
MaqFic = FreeFile
    
'-> Ouverture du fichier � g�n�r�
Open MaqTransPath & lpBuffer & ".maq" For Output As #MaqFic

'-> Enregistrer l'entete
Set aRg = MaqChars("MAQCHAR").RangChars("ENTETE")
For i = 1 To aRg.nLignes - 1
    '-> Faire ici le subsrting des LIG et MAX
    ' PIERROT 29/09/2005 : mise a jour du lig et max dans le fichier maquette
    If Mid$(Trim(UCase$(aRg.GetLigne(i))), 1, 4) = """LIG" Then
    
    'If InStr(1, UCase$(aRg.GetLigne(i)), "LIG") <> 0 Then
    
        Print #MaqFic, """LIG " & Entry(1, RetourBmp, "|") & """"
    ElseIf Mid$(Trim(UCase$(aRg.GetLigne(i))), 1, 4) = """MAX" Then
    'ElseIf InStr(1, UCase$(aRg.GetLigne(i)), "MAX") <> 0 Then
        Print #MaqFic, """MAX " & Entry(2, RetourBmp, "|") & """"
    Else
        Print #MaqFic, aRg.GetLigne(i)
    End If
Next

'-> Mot cle Graphique
Print #MaqFic, """GUI"""

'-> Enregistrer les rangs
For i = 2 To MaqChars("MAQCHAR").RangChars.Count - 1
    '-> Pointer sur le rang
    Set aRg = MaqChars("MAQCHAR").RangChars(i)
    '-> Imprimer le nom du rang
    Print #MaqFic, """" & aRg.Nom & """"
    '-> Remettre le nombre de ligen � 0
    NbLigne = 0
    '-> Pour toutes les liens
    For j = 1 To aRg.nLiens - 1
        '-> R�cup�ration du type du lien
        TypeLien = aRg.GetLien(j)
        '-> Analyse du lien
        If UCase$(Entry(1, TypeLien, "-")) = "SECTION" Then
            '-> Pointer sur l'objet
            Set aSection = aMaq.Sections(Trim(UCase$(Entry(2, TypeLien, "-"))))
            Set aFrm = aMaq.frmSections(UCase$(aSection.Nom))
            LigneToPrint = "[ST-" & Entry(2, TypeLien, "-") & "(TXT-Section)][]{"
            '-> Analyser les champs
            ListeChamp = MapFields(aFrm.RangText.Text)
            '-> Analyse des champs
            If ListeChamp = "" Then
            Else
                '-> Etablir la liste des champs
                For k = 1 To NumEntries(ListeChamp, "@")
                    If IsNumeric(Entry(k, ListeChamp, "@")) Then
                        LigneToPrint = LigneToPrint & "^" & Entry(k, ListeChamp, "@") & "#" & CInt(Entry(k, ListeChamp, "@"))
                    Else
                        LigneToPrint = LigneToPrint & "^" & Entry(k, ListeChamp, "@") & "#" & Entry(k, ListeChamp, "@")
                    End If
                    '-> Pointer sur le champ associ�
                    Set aField = aSection.Fields(UCase$(Entry(k, ListeChamp, "@")))
                    LigneToPrint = LigneToPrint & Space$(aField.FormatEdit)
                Next 'Pout tous les champs
            End If
            '-> analyse des sous objets
            For k = 1 To aSection.nEntries - 1
                TypeLien = aSection.GetOrdreAffichage(k)
                If UCase$(Entry(1, TypeLien, "-")) = "CDR" Then
                    '-> Pointer sur le cadre
                    Set aCadre = aSection.Cadres(Trim(UCase$(Entry(2, TypeLien, "-"))))
                    '-> Enregistrement du cadre s'il contient des #
                    ListeChamp = MapFields(aFrm.CadreTexte(aCadre.IdAffichage).Text)
                    If ListeChamp = "" Then
                    Else
                        '-> Etablir la liste des champs
                        For l = 1 To NumEntries(ListeChamp, "@")
                            If IsNumeric(Entry(l, ListeChamp, "@")) Then
                                LigneToPrint = LigneToPrint & "^" & Entry(l, ListeChamp, "@") & "#" & CInt(Entry(l, ListeChamp, "@"))
                            Else
                                LigneToPrint = LigneToPrint & "^" & Entry(l, ListeChamp, "@") & "#" & Entry(l, ListeChamp, "@")
                            End If
                            '-> Pointer sur le champ associ�
                            Set aField = aCadre.Fields(UCase$(Entry(l, ListeChamp, "@")))
                            LigneToPrint = LigneToPrint & Space$(aField.FormatEdit)
                        Next 'Pout tous les champs
                    End If
                    '-> Lib�rer le pointeur sur le cadre
                    Set aCadre = Nothing
                ElseIf UCase$(Entry(1, TypeLien, "-")) = "BMP" Then
                    '-> Pointer sur l'image
                    Set aBmp = aSection.Bmps(Trim(UCase$(Entry(2, TypeLien, "-"))))
                    '-> Enregistrer si le bmp est variable
                    If aBmp.isVariable Then
                        '-> Tester l'unicit� du champ dans la section en cas d'association de champs
                        If aBmp.UseAssociation Then
                            '-> Ne rien faire ici cas le champ sp�cifi� dans la propri�t� fichier est un champ existant dans la section donc d�ja renseign�
                        Else
                            If IsNumeric(Mid$(aBmp.Fichier, 2, 4)) Then
                                LigneToPrint = LigneToPrint & aBmp.Fichier & "#" & CInt(Mid$(aBmp.Fichier, 2, 4))
                            Else
                                LigneToPrint = LigneToPrint & aBmp.Fichier & "#" & Mid$(aBmp.Fichier, 2, 4)
                            End If
                            LigneToPrint = LigneToPrint & Space$(aBmp.FormatEdit)
                        End If
                    End If
                    '-> Liberer le pointeur du bmp
                    Set aBmp = Nothing
                End If
            Next 'Pour tous les objets compris dans le rang
            '-> Fermer la chaine � imprimer
            LigneToPrint = LigneToPrint & "}"
            '-> imprimer la chaine dans le fichier
            Print #MaqFic, """" & LigneToPrint & """"
            '-> Incr�menter le compteur de ligne enregistr�e
            NbLigne = NbLigne + 1
            '-> Tous les objets du rang sont imprim�s : lib�rer le pointeur sur la feuille
            Set aFrm = Nothing
        ElseIf UCase$(Entry(1, TypeLien, "-")) = "TABLEAU" Then
            '-> Pointer sur le tableau
            Set aTb = aMaq.Tableaux(UCase$(Entry(2, TypeLien, "-")))
            '-> Pointer sur le block associ�
            Set aBlock = aTb.Blocks("BL-" & Trim(UCase$(Entry(3, TypeLien, "-"))))
            '-> Imprimer chaque ligne
            For k = 1 To aBlock.NbLigne
                '-> Construire la chaine de caract�re
                
                '-> Tenir compte de la ligne variable ou non
                If aBlock.GetVarLig(k) Then
                    CharVar = "@"
                Else
                    CharVar = ""
                End If
                
                LigneToPrint = CharVar & "[TB-" & aTb.Nom & "(BLK-" & aBlock.Nom & ")][\" & k & "]{"
                ListeChamp = GetFieldByLigne(aBlock, k, FormatEdit())
                '-> Etablir la liste des champs
                If ListeChamp <> "" Then
                    For l = 1 To NumEntries(ListeChamp, "@")
                        LigneToPrint = LigneToPrint & "^" & Entry(l, ListeChamp, "@") & "#"
                        If IsNumeric(Entry(l, ListeChamp, "@")) Then
                            LigneToPrint = LigneToPrint & CInt(Entry(l, ListeChamp, "@"))
                        Else
                            LigneToPrint = LigneToPrint & Entry(l, ListeChamp, "@")
                        End If
                        LigneToPrint = LigneToPrint & Space$(FormatEdit(l))
                    Next 'Pout tous les champs
                End If
                '-> Tenir compte de l'acc�s au d�tail
                If aBlock.UseAccesDet Then
                    '-> Positionner le #d'acc�s au d�tail
                    LigneToPrint = LigneToPrint & " ^ACDET #" & Entry(1, aBlock.KeyAccesDet, "|")
                End If
                LigneToPrint = LigneToPrint & " }"
                '-> Imprimer la ligne
                Print #MaqFic, """" & LigneToPrint & """"
                NbLigne = NbLigne + 1
                '-> Supprimer la matrice
                Erase FormatEdit
            Next 'Pour toutes les lignes
            '-> Liberer le pointeur du block
            Set aBlock = Nothing
        End If
    Next 'pour tous les liens
    '-> Tous les liens sont imprim�s : compl�ter avec des blancs
    If Not DelBlanckLigne Then
        For j = NbLigne + 1 To aRg.nLignes - 1
            Print #MaqFic, """ """
        Next
    End If
    '-> Imprimer le mot cl� page si c'est un rang de saut de page
    If aRg.IsSautPage Then Print #MaqFic, """[PAGE]""" & Chr(13) + Chr(10) + """ """
        
Next 'pour tous les rangs caract�res

'-> Imprimer le rang de bas de page
Set aRg = MaqChars("MAQCHAR").RangChars("FIN")
For i = 1 To aRg.nLignes - 1
    Print #MaqFic, aRg.GetLigne(i)
Next

'-> Enregistrer le nom du fichier de la maquette de transition
MaqChars("MAQCHAR").MaqTransition = lpBuffer

GestError:

    '-> Fermer le fichier
    Close #MaqFic
    '-> Liberer tous les pointeurs
    Set aMaq = Nothing
    Set aSection = Nothing
    Set aCadre = Nothing
    Set aBmp = Nothing
    Set aTb = Nothing
    Set aBlock = Nothing
    Set aRg = Nothing
    Set aFrm = Nothing
    Set aLb = Nothing
    
    '-> D�charger la feuille
    Unload Me

    Select Case Err.Number
    
        Case 0
                        
        Case 32755
        
    End Select
    
End Sub

Private Function GetFieldByLigne(aBlock As Block, nLigne As Integer, FormatEdit() As Integer) As String

'---> Cette fonction renvoie la liste des champs donn�s pour une ligne d'un block

Dim aCell As Cellule
Dim ListeField As String
Dim CellField As String
Dim i As Integer
Dim aField As Field
Dim NbFields As Integer


For i = 1 To aBlock.NbCol
    '-> Pointer sur la cellule
    Set aCell = aBlock.Cellules("L" & nLigne & "C" & i)
    '-> Analyse des champs de la cellule
    CellField = MapFields(aCell.Contenu)
    '-> Ajouter � la liste des champs de la ligne
    If CellField <> "" Then
        '-> Pointer sur le champ
        If ListeField = "" Then
            ListeField = CellField
        Else
            ListeField = ListeField & "@" & CellField
        End If
        '-> Cr�er la matrice
        '-> Redimensionner selon le nombre de champs
        ReDim Preserve FormatEdit(1 To NbFields + NumEntries(CellField, "@"))
        For j = 1 To NumEntries(CellField, "@")
            '-> Pointer sur le champ
            Set aField = aCell.Fields(UCase$(Entry(j, CellField, "@")))
            FormatEdit(NbFields + j) = aField.FormatEdit
        Next
        NbFields = NbFields + NumEntries(CellField, "@")
    End If
Next 'Pour toutes les cellule

'-> Liberer le pointeur sur la cellule
Set aCell = Nothing

'-> Renvoyer la liste des champs
GetFieldByLigne = ListeField

End Function
