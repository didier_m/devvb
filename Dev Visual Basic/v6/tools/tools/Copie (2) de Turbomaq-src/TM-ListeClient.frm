VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmListeClient 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6180
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   8445
   ControlBox      =   0   'False
   Icon            =   "TM-ListeClient.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   412
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   563
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Appearance      =   0  'Flat
      Height          =   495
      Left            =   7620
      Picture         =   "TM-ListeClient.frx":08CA
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   5580
      Width           =   735
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   5415
      Left            =   75
      TabIndex        =   0
      Top             =   75
      Width           =   8280
      _ExtentX        =   14605
      _ExtentY        =   9551
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ColHdrIcons     =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Code"
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Libell�"
         Object.Width           =   38100
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4560
      Top             =   2640
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-ListeClient.frx":0C54
            Key             =   "CLIENT"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-ListeClient.frx":192E
            Key             =   "DEAL"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmListeClient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
'-> Fermer
Dim aLb As Libelle
On Error Resume Next
Set aLb = Libelles("FRMLISTECLIENT")
If MsgBox(aLb.GetCaption(7), vbQuestion + vbYesNo, aLb.GetToolTip(7)) = vbYes Then End

End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim IniPath As String
Dim i As Integer, j As Integer
Dim lpBuffer As String
Dim Res As Long
Dim lpKey As String
Dim ListeProgiciels() As String
Dim aClient As Client
Dim aitem As ListItem


On Error GoTo LoadSectionErr

Set aLb = Libelles("FRMLISTECLIENT")
Me.Caption = aLb.GetCaption(1)

IniPath = V6InitPath & "Turbomaq.ini"

If Not IsLoadedPath Then
    '-> R�cup�ration de la variable ROOT
    lpKey = Space$(100)
    Res = GetPrivateProfileString("Param", "$ROOT", "", lpKey, Len(lpKey), IniPath)
    V6Root = Mid$(lpKey, 1, Res)
    '-> R�cup�ration du r�pertoire des maquettes  char de r�f�rence
    lpKey = Space$(100)
    Res = GetPrivateProfileString("Param", "$MAQCHARPATH", "", lpKey, Len(lpKey), IniPath)
    V6MaqCharPath = Replace(Mid$(lpKey, 1, Res), "$ROOT", V6Root)
    If Right(V6MaqCharPath, 1) <> "\" Then V6MaqCharPath = V6MaqCharPath & "\"
    '-> R�cup�ration du r�pertoire des maquettes GUI du standard
    lpKey = Space$(100)
    Res = GetPrivateProfileString("Param", "$MAQGUIPATH", "", lpKey, Len(lpKey), IniPath)
    V6GuiCharPath = Replace(Mid$(lpKey, 1, Res), "$ROOT", V6Root)
    If Right(V6GuiCharPath, 1) <> "\" Then V6GuiCharPath = V6GuiCharPath & "\"
    '-> R�cup�ration du r�pertoire des maquettes GUI des identifiants
    lpKey = Space$(100)
    Res = GetPrivateProfileString("Param", "$IDENTPATH", "", lpKey, Len(lpKey), IniPath)
    V6IdentPath = Replace(Mid$(lpKey, 1, Res), "$ROOT", V6Root)
    '-> R�cup�ration de la liste des logiciels
    lpKey = Space(500)
    Res = GetPrivateProfileString("Progiciels", "App", "", lpKey, Len(lpKey), IniPath)
    If lpKey = "" Then
        '-> Impossible de trouver la cl� ou la section : Quitter
        MsgBox aLb.GetCaption(6), vbCritical + vbOKOnly, aLb.GetToolTip(6)
        Set aLb = Nothing
        End
    End If
    '-> Cr�ation de la liste des logiciels
    lpKey = Mid$(lpKey, 1, Res)
    ReDim Preserve ListeProgiciels(1 To NumEntries(lpKey, "|"))
    For i = 1 To UBound(ListeProgiciels)
        ListeProgiciels(i) = Entry(i, lpKey, "|")
    Next
    '-> Intialiser la collection des clients du Turbograph
    Set V6ClientList = New Collection
    '-> Initialiser le standard
    V6CreateAppRef ListeProgiciels, IniPath
    '-> Initialiser la liste des identifiants
    V6CreateIdentifiantRef IniPath, ListeProgiciels
End If 'Si on r�affiche la fen�tre ou non

'-> Afficher la liste des clients
For Each aClient In V6ClientList
    Set aitem = Me.ListView1.ListItems.Add(, "CLI|" & aClient.Code, aClient.Code, , "CLIENT")
    aitem.SubItems(1) = aClient.Libelle
Next

'-> Formatter le listview
FormatListView Me.ListView1

Exit Sub

LoadSectionErr:
    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)

End Sub

Private Sub V6CreateIdentifiantRef(FilePath As String, ListeProgiciel() As String)

'---> Cette proc�dure cr�er la liste des identifiants r�f�renc�s

Dim lpBuffer As String
Dim lpBufferAux As String
Dim Res As Integer
Dim nEntries As Integer
Dim NomSection As String
Dim i As Integer
Dim j As Integer
Dim aClient As Client
Dim aApp As Application
Dim FindApp As Boolean
Dim Temp As String

'-> R�cup�ration de toutes les sections
lpBuffer = Space$(32765)
Res = GetPrivateProfileString(vbNullString, "", "", lpBuffer, Len(lpBuffer), FilePath)
lpBuffer = Mid$(lpBuffer, 1, Res - 1)
If Res = 0 Then Exit Sub
'-> Charger le tableau des sections
nEntries = NumEntries(lpBuffer, Chr(0))

For i = 1 To nEntries
    '-> R�cup�rer le nom de la section
    NomSection = Entry(i, lpBuffer, Chr(0))
    If Trim(NomSection) = "" Then Exit For
    '-> Selon la cl� que l'on est en train de lire
    FindApp = False
    If UCase$(NomSection) <> "PARAM" And UCase$(NomSection) <> "PROGICIELS" Then
        For j = 1 To UBound(ListeProgiciel)
            If UCase$(NomSection) = UCase$(ListeProgiciel(j)) Then
                FindApp = True
                Exit For
            End If
        Next
    Else
        FindApp = True
    End If
    If Not FindApp Then
        '-> Cr�er l'ident si on a trouv�
        Set aClient = New Client
        aClient.Code = Trim(UCase$(Trim(NomSection)))
        V6ClientList.Add aClient, "CLI|" & aClient.Code
        '-> Libelle
        lpBufferAux = Space$(500)
        Res = GetPrivateProfileString(NomSection, "Libel", "", lpBufferAux, Len(lpBufferAux), FilePath)
        If Res <> 0 Then aClient.Libelle = Mid$(lpBufferAux, 1, Res)
        '-> Charger les applicatifs
        lpBufferAux = Space$(1024)
        Temp = ""
        Res = GetPrivateProfileString(NomSection, "Progiciels", "", lpBufferAux, Len(lpBufferAux), FilePath)
        If Res <> 0 Then
            lpBufferAux = Mid$(lpBufferAux, 1, Res)
            '-> V�rifier si on a une liste ou l'adresse d'un fichier
            If InStr(1, UCase$(lpBufferAux), ".INI") <> 0 Then
                Temp = Replace(lpBufferAux, "$ROOT", V6Root)
                Temp = Replace(Temp, "$IDENT", aClient.Code)
                '-> Lire la liste depuis un fichier de param�trage
                lpBufferAux = Space$(2000)
                Res = GetPrivateProfileString("LOGICAL", "$Progiciel$", "", lpBufferAux, Len(lpBufferAux), Temp)
                If Res <> 0 Then
                    Temp = Mid$(lpBufferAux, 1, Res)
                Else
                    Temp = ""
                End If
            Else
                '-> Affecter la liste
                Temp = lpBufferAux
            End If
        End If
        'Cr�er la liste des progiciels affect�s pour ce client
        If Temp <> "" Then
            For j = 1 To NumEntries(Temp, ",")
                '-> Chercher si on trouve la d�fintion de l'applicatif
                If IsAppRef(Entry(j, Temp, ",")) Then
                    Set aApp = New Application
                    aApp.Code = Trim(UCase$(Entry(j, Temp, ",")))
                    aApp.Libelle = V6ClientList("CLI|DEAL").Applications("APP|" & aApp.Code).Libelle
                    aClient.Applications.Add aApp, "APP|" & aApp.Code
                    aApp.V6PathApp = Replace(UCase$(V6IdentPath), "$IDENT", aClient.Code)
                    aApp.V6PathApp = Replace(UCase$(aApp.V6PathApp), "$APP", aApp.Code)
                    If Right$(aApp.V6PathApp, 1) <> "\" Then aApp.V6PathApp = aApp.V6PathApp & "\"
                End If
            Next 'Pour toutes les applications
        End If
    End If
Next


End Sub

Private Function IsAppRef(NomApp As String) As Boolean

'---> Fonction V6 qui v�rifie si un applicatif est r�f�renc� dans le standard
Dim aClient As Client
Dim aApp As Application

On Error GoTo GestError

Set aClient = V6ClientList("CLI|DEAL")
Set aApp = aClient.Applications("APP|" & UCase$(Trim(NomApp)))
IsAppRef = True
Exit Function
GestError:

End Function


Private Sub V6CreateAppRef(ListeProgiciel() As String, FilePath As String)

'---> Cette proc�dure cr�er le clent standard DEAL et ses applications

Dim i As Integer
Dim aClient As Client
Dim aApp As Application
Dim lpBuffer As String
Dim Res As Integer

'-> Cr�er le client standard
Set aClient = New Client
aClient.Code = "DEAL"
aClient.Libelle = "Standard Deal Informatique"
V6ClientList.Add aClient, "CLI|DEAL"

'-> Pour tous les progiciels r�f�renc�s
For i = 1 To UBound(ListeProgiciel)
    '-> R�cup�rer la section
    lpBuffer = Space$(32765)
    Res = GetPrivateProfileString(ListeProgiciel(i), vbNullString, "", lpBuffer, Len(lpBuffer), FilePath)
    If Res <> 0 Then
        '-> Cr�er l'applicatif
        Set aApp = New Application
        aApp.Code = Trim(UCase$(ListeProgiciel(i)))
        lpBuffer = Space$(1024)
        Res = GetPrivateProfileString(ListeProgiciel(i), "Libel", "", lpBuffer, Len(lpBuffer), FilePath)
        If Res <> 0 Then aApp.Libelle = Mid$(lpBuffer, 1, Res)
        lpBuffer = Space$(2000)
        Res = GetPrivateProfileString(ListeProgiciel(i), "Rubriques-" & CodeLangue, "", lpBuffer, Len(lpBuffer), FilePath)
        If Res <> 0 Then
            aApp.Rubriques = Mid$(lpBuffer, 1, Res)
            aApp.CreateRubriques
        End If
        '-> Poser le path
        aApp.V6PathApp = Replace(UCase$(V6GuiCharPath), "$APP", aApp.Code)
        If Right$(aApp.V6PathApp, 1) <> "\" Then aApp.V6PathApp = aApp.V6PathApp & "\"
        '-> Ajouter dans le client en cours
        aClient.Applications.Add aApp, "APP|" & aApp.Code
    End If 'Si on a trouv� la d�finition
Next

End Sub


Private Sub ListView1_DblClick()

'-> Rien faire si pas de client
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

If V6ClientList(Me.ListView1.SelectedItem.Key).Applications.Count = 0 Then
    MsgBox "Il n'y a pas d'applications affect�es � cet ident", vbCritical + vbOKOnly, "Erreur"
    Exit Sub
End If

'-> Charger le descriptif du client concern� et afficher le choix
DisplayClient V6ClientList(Me.ListView1.SelectedItem.Key)

End Sub

Private Sub DisplayClient(aClient As Client)

Dim aLb As Libelle
Dim aApp As Application
Dim i As Integer
Dim aTab As Object
Dim OpenType As String
Dim OpenSrc As String
Dim OpenApp As String
Dim OpenRub As String
Dim PathToAnalyse As String

'On Error GoTo GestError THIERRY

'-> Initialiser
Load FrmChxApp
Set aLb = Libelles("FRMSAMPLE")
'-> Cr�er la liste des applicatifs affect�e � ce client
For i = 1 To aClient.Applications.Count
    If i = 1 Then
        Set aTab = FrmChxApp.TabStrip1.Tabs(1)
    Else
        Set aTab = FrmChxApp.TabStrip1.Tabs.Add
    End If
    'FrmChxApp.TabStrip1.TabIndex = i
    aTab.Caption = aClient.Applications(i).Code & " " & aClient.Applications(i).Libelle
    aTab.Key = aClient.Applications(i).Code
Next 'Pour toutes les applications

'-> Afficher le contenu du premier onglet
If aClient.Code = "DEAL" Then
    FrmChxApp.IsStandard = True
Else
    FrmChxApp.IsStandard = False
End If

Set FrmChxApp.aClient = aClient
FrmChxApp.DisplayApp aClient.Applications(1).Code

'-> Afficher la feuille
strRetour = ""
FrmChxApp.Caption = aClient.Code & " - " & aClient.Libelle
FrmChxApp.Show vbModal
'-> Traiter le retour � blanc
If strRetour = "" Then
    Me.ListView1.SetFocus
    Exit Sub
End If
'-> Ex�cuter l'op�ration
OpenType = Entry(1, strRetour, "|")
OpenSrc = Entry(2, strRetour, "|")
OpenApp = Entry(3, strRetour, "|")
OpenRub = Entry(4, strRetour, "|")
strRetour = ""


'-> Poser les variables globales
Set V6ClientCours = aClient
V6Rub = OpenRub
V6App = OpenApp

'-> Charger le path
SetPath

If OpenType = "MOD" Then
    strRetour = ""
    frmChoixApp.Show vbModal
    If strRetour = "" Then Exit Sub
    V6Rub = strRetour
    strRetour = ""
    If OpenSrc = "STD" Then
        '-> Modele standard
        Unload Me
        frmNewMaq.Show
    Else
        '-> Fichier ressource
        Unload Me
        If Not OpenMaq(aClient.Applications("APP|" & V6App).V6PathApp & OpenSrc) Then Exit Sub
        Set aLb = Libelles("FRMNEWMAQ")
        Maquettes("MAQUETTE").Nom = aLb.GetCaption(3)
        frmNaviga.TreeNaviga.Nodes("MAQUETTE").Text = aLb.GetCaption(3)
    End If
ElseIf OpenType = "MAQ" Then
    '-> Ouvrir une maquette
    Unload Me
    OpenMaq aClient.Applications("APP|" & V6App).V6PathApp & OpenSrc
End If
    
Exit Sub

GestError:
    Unload FrmChxApp

End Sub

Private Sub SetPath()

On Error Resume Next

Erase V6ListePath

If V6ClientCours.Code = "DEAL" Then
    If V6App = "EMILIE" Then
        ReDim V6ListePath(1 To 1, 1 To 2)
        V6ListePath(1, 1) = V6ClientList("CLI|DEAL").Applications("APP|EMILIE").V6PathApp
        V6ListePath(1, 2) = "PARAM"
    Else
        ReDim V6ListePath(1 To 2, 1 To 2)
        V6ListePath(1, 1) = V6ClientList("CLI|DEAL").Applications("APP|" & V6App).V6PathApp
        V6ListePath(1, 2) = "APP"
        V6ListePath(2, 1) = V6ClientList("CLI|DEAL").Applications("APP|EMILIE").V6PathApp
        V6ListePath(2, 2) = "PARAM"
    End If
Else
    If V6App = "EMILIE" Then
        ReDim V6ListePath(1 To 2, 1 To 2)
        V6ListePath(1, 1) = V6ClientCours.Applications("APP|EMILIE").V6PathApp
        V6ListePath(1, 2) = "CLIENT"
        V6ListePath(2, 1) = V6ClientList("CLI|DEAL").Applications("APP|EMILIE").V6PathApp
        V6ListePath(2, 2) = "PARAM"
    Else
        ReDim V6ListePath(1 To 3, 1 To 2)
        V6ListePath(1, 1) = V6ClientCours.Applications("APP|" & V6App).V6PathApp
        V6ListePath(1, 2) = "CLIENT"
        V6ListePath(2, 1) = V6ClientList("CLI|DEAL").Applications("APP|" & V6App).V6PathApp
        V6ListePath(2, 2) = "APP"
        V6ListePath(3, 1) = V6ClientList("CLI|DEAL").Applications("APP|EMILIE").V6PathApp
        V6ListePath(3, 2) = "PARAM"
    End If
End If

End Sub


Private Sub ListView1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then ListView1_DblClick
End Sub

