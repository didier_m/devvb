VERSION 5.00
Begin VB.Form frmLoadBmp 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   6405
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10860
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6405
   ScaleWidth      =   10860
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox Check3 
      Caption         =   "Check3"
      Height          =   255
      Left            =   0
      TabIndex        =   8
      Top             =   6000
      Width           =   4455
   End
   Begin VB.CheckBox Check2 
      Caption         =   "Check2"
      Height          =   255
      Left            =   0
      TabIndex        =   7
      Top             =   5640
      Width           =   4215
   End
   Begin VB.FileListBox File1 
      Height          =   285
      Left            =   4680
      TabIndex        =   6
      Top             =   5640
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.PictureBox Ins�rer 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   10200
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   42
      TabIndex        =   5
      Top             =   5760
      Width           =   630
   End
   Begin VB.PictureBox Picture1 
      Height          =   4695
      Left            =   5760
      ScaleHeight     =   4635
      ScaleWidth      =   4995
      TabIndex        =   2
      Top             =   450
      Width           =   5055
      Begin VB.VScrollBar Vertical 
         Height          =   4360
         Left            =   4750
         TabIndex        =   4
         Top             =   0
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.HScrollBar Horizontal 
         Height          =   255
         Left            =   0
         TabIndex        =   3
         Top             =   4390
         Visible         =   0   'False
         Width           =   4755
      End
      Begin VB.Image Aper�cu 
         Height          =   1290
         Left            =   0
         Top             =   0
         Width           =   1290
      End
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   255
      Left            =   5760
      TabIndex        =   1
      Top             =   120
      Width           =   4215
   End
   Begin VB.ListBox List1 
      Height          =   4740
      Left            =   0
      TabIndex        =   0
      Top             =   480
      Width           =   5655
   End
   Begin VB.Label lblPath 
      BackStyle       =   0  'Transparent
      Height          =   255
      Left            =   0
      TabIndex        =   9
      Top             =   5280
      Width           =   10695
   End
   Begin VB.Image Annuler 
      Height          =   480
      Left            =   8760
      Top             =   5760
      Width           =   630
   End
   Begin VB.Image Aide 
      Height          =   480
      Left            =   9480
      Top             =   5760
      Width           =   630
   End
End
Attribute VB_Name = "frmLoadBmp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ListPath() As String

Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub Check1_Click()

If Me.Check1.Value = 0 Then
    Me.Aper�cu.Visible = False
    Me.Vertical.Visible = False
    Me.Horizontal.Visible = False
Else
    LoadBitmap ListPath(1, Me.List1.ListIndex) & Me.List1.Text
End If


End Sub

Private Sub Check2_Click()

If Me.Check2.Value = 0 Then Me.Check3.Value = 0

End Sub

Private Sub Check3_Click()

If Me.Check3.Value = 1 Then Me.Check2.Value = 1


End Sub

Private Sub Form_Load()

Dim aLb As Libelle

'-> Chargement des libelles
Set aLb = Libelles("FRMLOADBMP")
Me.Caption = aLb.GetCaption(1)
Me.Check1.Caption = aLb.GetCaption(2)
Me.Check2.Caption = aLb.GetCaption(3)
Me.Check3.Caption = aLb.GetCaption(4)

Set aLb = Nothing
Set aLb = Libelles("BOUTONS")
Me.Annuler.ToolTipText = aLb.GetCaption(1)
Me.Aide.ToolTipText = aLb.GetCaption(2)
Me.Ins�rer.ToolTipText = aLb.GetCaption(5)

Me.Annuler.Picture = Turbo_Maq.Annuler.Picture
Me.Aide.Picture = Turbo_Maq.Aide.Picture
Me.Ins�rer.Picture = Turbo_Maq.Ok.Picture

Set aLb = Nothing
'-> Aper�u non s�lectionn�
Me.Check1.Value = 0
If TypeOf ObjCours Is ImageObj Then
    If ObjCours.isVariable Then
        Me.Check2.Value = 1
        If ObjCours.UseAssociation Then Me.Check3.Value = 1
    Else
        Me.Check2.Value = 0
        Me.Check2.Enabled = False
        Me.Check3.Value = 0
        Me.Check3.Enabled = False
    End If
End If
'-> Affichage des icones  selon le path
DisplayAllPath

'-> s�lectionner le premier
If Me.List1.ListCount > 0 Then Me.List1.Selected(0) = True

'-> Vider la zone de retour des images
RetourBmp = ""

End Sub

Private Sub DisplayAllPath()

Dim i As Integer

For i = 1 To UBound(V6ListePath)
    Call DisplayBitmap(V6ListePath(i, 1), V6ListePath(i, 2))
Next


End Sub

Private Sub DisplayBitmap(BmpPath As String, TypePath As String)

Dim i As Integer

'-> Anlayse du path � la recherche des fichiers
Me.File1.Path = BmpPath
Me.File1.Pattern = "*.Ico;*.Wmf;*.EMF;*.Bmp;*.Jpg;*.Gif"
Me.File1.Refresh

For i = 0 To Me.File1.ListCount - 1
    '-> Ajouter le fichier dans la liste
    Me.List1.AddItem Me.File1.List(i)
    '-> Sauvegarder le path
    ReDim Preserve ListPath(1 To 2, 0 To Me.List1.ListCount - 1)
    ListPath(1, Me.List1.ListCount - 1) = BmpPath
    ListPath(2, Me.List1.ListCount - 1) = TypePath
Next

End Sub

Private Function LoadBitmap(ByVal FileToLoad As String) As Boolean

'---> Cette fonction sert � charger une image, � afficher l'aper�u et � g�rer _
donc les barres de d�filement.

Dim ManqueH As Long
Dim ManqueV As Long

'-> Charger l'image
Me.Aper�cu.Picture = LoadPicture(FileToLoad)
Me.Aper�cu.Top = 0
Me.Aper�cu.Left = 0

'-> Calcul des dimensions
ManqueH = Me.Aper�cu.Width - Me.Picture1.Width
ManqueV = Me.Aper�cu.Height - Me.Picture1.Height

If ManqueV > 0 Then
    Me.Vertical.Min = 0
    Me.Vertical.Max = -ManqueV - 500
    Me.Vertical.SmallChange = Abs(CInt(ManqueV / 10))
    Me.Vertical.LargeChange = Abs(CInt(Me.Vertical.Max) / 5)
    Me.Vertical.Visible = True
Else
    Me.Vertical.Visible = False
End If
    
If ManqueH > 0 Then
    Me.Horizontal.Min = 0
    Me.Horizontal.Max = -ManqueH - 500
    Me.Horizontal.SmallChange = Abs(CInt(ManqueV / 10))
    Me.Horizontal.LargeChange = Abs(CInt(ManqueV / 5))
    Me.Horizontal.Visible = True
Else
    Me.Horizontal.Visible = False
End If


Me.Aper�cu.Visible = True

End Function

Private Sub Horizontal_Change()
    Me.Aper�cu.Left = Me.Horizontal.Value
End Sub

Private Sub Ins�rer_Click()
    If Me.Check2.Value = 1 Then
        If Me.Check3.Value = 1 Then
            RetourBmp = "VAR"
        Else
            RetourBmp = "VARIABLE"
        End If
    Else
        RetourBmp = ListPath(2, Me.List1.ListIndex) & "|" & Me.List1.Text
    End If
    Unload Me
End Sub

Private Sub List1_Click()

On Error Resume Next

'-> Afficher l'aper�u
If Me.Check1.Value = 1 Then LoadBitmap ListPath(1, Me.List1.ListIndex) & Me.List1.Text
'-> Afficher le path
Me.lblPath = ListPath(1, Me.List1.ListIndex)
    
End Sub

Private Sub Vertical_Change()
    Me.Aper�cu.Top = Me.Vertical.Value
End Sub
