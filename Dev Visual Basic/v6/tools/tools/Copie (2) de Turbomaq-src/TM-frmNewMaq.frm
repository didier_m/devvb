VERSION 5.00
Begin VB.Form frmNewMaq 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   5100
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6360
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5100
   ScaleWidth      =   6360
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Cancel          =   -1  'True
      Height          =   375
      Left            =   6840
      TabIndex        =   18
      Top             =   2760
      Width           =   375
   End
   Begin VB.Frame Frame4 
      Height          =   1575
      Left            =   120
      TabIndex        =   17
      Top             =   3480
      Width           =   6135
      Begin VB.TextBox Description 
         Height          =   1215
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   5895
      End
   End
   Begin VB.Frame Frame3 
      Height          =   1335
      Left            =   2760
      TabIndex        =   12
      Top             =   2040
      Width           =   2655
      Begin VB.OptionButton Option2 
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   720
         Width           =   1215
      End
      Begin VB.OptionButton Option1 
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   1095
      End
      Begin VB.Image Paysage 
         Height          =   480
         Left            =   1680
         Picture         =   "TM-frmNewMaq.frx":0000
         Top             =   480
         Width           =   480
      End
      Begin VB.Image Portrait 
         Height          =   480
         Left            =   1680
         Picture         =   "TM-frmNewMaq.frx":0742
         Top             =   480
         Width           =   480
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1455
      Left            =   2760
      TabIndex        =   11
      Top             =   480
      Width           =   2655
      Begin VB.TextBox mLeft 
         Height          =   285
         Left            =   1440
         TabIndex        =   5
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox mTop 
         Height          =   285
         Left            =   1440
         TabIndex        =   4
         Top             =   360
         Width           =   615
      End
      Begin VB.Image Image2 
         Height          =   240
         Left            =   2040
         Picture         =   "TM-frmNewMaq.frx":0E84
         Top             =   840
         Width           =   240
      End
      Begin VB.Image Image1 
         Height          =   240
         Left            =   2040
         Picture         =   "TM-frmNewMaq.frx":12C6
         Top             =   240
         Width           =   240
      End
      Begin VB.Label Label5 
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label4 
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Height          =   2895
      Left            =   120
      TabIndex        =   10
      Top             =   480
      Width           =   2535
      Begin VB.TextBox Largeur 
         Enabled         =   0   'False
         Height          =   285
         Left            =   1440
         TabIndex        =   3
         Top             =   2400
         Width           =   975
      End
      Begin VB.TextBox Hauteur 
         Enabled         =   0   'False
         Height          =   285
         Left            =   1440
         TabIndex        =   2
         Top             =   2040
         Width           =   975
      End
      Begin VB.ListBox List1 
         Height          =   1620
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   2295
      End
      Begin VB.Label Label3 
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   2400
         Width           =   1335
      End
      Begin VB.Label Label2 
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   2040
         Width           =   975
      End
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   2760
      TabIndex        =   0
      Text            =   "SansNom"
      Top             =   120
      Width           =   2655
   End
   Begin VB.Image Annuler 
      Height          =   480
      Left            =   5640
      Top             =   1320
      Width           =   630
   End
   Begin VB.Image Aide 
      Height          =   480
      Left            =   5640
      Top             =   720
      Width           =   630
   End
   Begin VB.Image Ok 
      Height          =   480
      Left            =   5640
      Top             =   120
      Width           =   630
   End
   Begin VB.Label Label1 
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   2415
   End
End
Attribute VB_Name = "frmNewMaq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private DocFormatDimensions(0 To 4, 0 To 1) As Single

Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub Form_Load()

Dim aLb As Libelle

'---> charger les libell�s
Set aLb = Libelles("FRMNEWMAQ")

Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Text1.Text = aLb.GetCaption(3)
AddItemToList aLb.GetCaption(4) & "|" & aLb.GetCaption(5) & "|" & aLb.GetCaption(6) & "|" & aLb.GetCaption(7) & "|" & aLb.GetCaption(8) & "|" & aLb.GetCaption(9), Me.List1, 4
Me.Label2.Caption = aLb.GetCaption(10)
Me.Label3.Caption = aLb.GetCaption(11)
Me.Label4.Caption = aLb.GetCaption(12)
Me.Label5.Caption = aLb.GetCaption(13)
Me.Option1.Caption = aLb.GetCaption(14)
Me.Option1.Value = True
Me.Option2.Caption = aLb.GetCaption(15)
Me.Description.Text = aLb.GetCaption(16)
Me.mTop = Convert(aLb.GetCaption(17))
Me.mLeft = Convert(aLb.GetCaption(18))

Set aLb = Libelles("BOUTONS")

Me.Annuler.ToolTipText = aLb.GetCaption(1)
Me.Aide.ToolTipText = aLb.GetCaption(2)
Me.Ok.ToolTipText = aLb.GetCaption(3)

Me.Annuler.Picture = Turbo_Maq.Annuler.Picture
Me.Aide.Picture = Turbo_Maq.Aide.Picture
Me.Ok.Picture = Turbo_Maq.Ok.Picture

'-> Charger les formats des maquettes pr�d�finies

DocFormatDimensions(0, 0) = 2159
DocFormatDimensions(0, 1) = 2794
DocFormatDimensions(1, 0) = 2159
DocFormatDimensions(1, 1) = 3556
DocFormatDimensions(2, 0) = 1841
DocFormatDimensions(2, 1) = 2667
DocFormatDimensions(3, 0) = 2100
DocFormatDimensions(3, 1) = 2970
DocFormatDimensions(4, 0) = 1480
DocFormatDimensions(4, 1) = 2100

'-> Format A4 par d�faut
Me.Largeur.Text = DocFormatDimensions(3, 0) / 100
Me.Hauteur.Text = DocFormatDimensions(3, 1) / 100

Set aLb = Nothing

End Sub

Private Sub Hauteur_GotFocus()
    Me.Hauteur.SelStart = 0
    Me.Hauteur.SelLength = 65535
End Sub

Private Sub Hauteur_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Me.Largeur.SetFocus
End Sub

Private Sub Hauteur_Validate(Cancel As Boolean)

Dim aLb As Libelle
    
    If Me.Hauteur.Text = "" Then GoTo GestError
    If Not VerifValue(Me.Hauteur.Text, 1, "#0.00") Or Me.Hauteur.Text = 0 Then GoTo GestError
    
    
    Set aLb = Nothing
    
Exit Sub

GestError:
    
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
    Cancel = True
    Set aLb = Nothing

End Sub

Private Sub Largeur_GotFocus()
    Me.Largeur.SelStart = 0
    Me.Largeur.SelLength = 65535
End Sub

Private Sub Largeur_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Me.mTop.SetFocus
End Sub

Private Sub Largeur_Validate(Cancel As Boolean)
        
Dim aLb As Libelle
    
    If Me.Largeur.Text = "" Then GoTo GestError
    If Not VerifValue(Me.Largeur.Text, 1, "#0.00") Or Me.Largeur.Text = 0 Then GoTo GestError
    
    Set aLb = Nothing
    
Exit Sub

GestError:
    
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
    Cancel = True
    Set aLb = Nothing
    
End Sub

Private Sub List1_Click()

    
If Me.List1.ListIndex = 5 Then
    
    '-> Personnalis� : Rendre la saisie possible
    Me.Hauteur.Enabled = True
    Me.Largeur.Enabled = True
    Me.Hauteur.SetFocus
    
Else
    '-> Afficher les largeurs
    Me.Hauteur.Text = DocFormatDimensions(Me.List1.ListIndex, 1) / 100
    Me.Largeur.Text = DocFormatDimensions(Me.List1.ListIndex, 0) / 100
    '-> interdire la saisie
    Me.Hauteur.Enabled = False
    Me.Largeur.Enabled = False
    
End If

    

    
    


End Sub

Private Sub mLeft_GotFocus()
    Me.mLeft.SelStart = 0
    Me.mLeft.SelLength = 65535
End Sub

Private Sub mLeft_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Me.Option1.SetFocus
End Sub

Private Sub mLeft_Validate(Cancel As Boolean)
    
Dim aLb As Libelle
    
    If Me.mLeft.Text = "" Then GoTo GestError
    If Not VerifValue(Me.mLeft.Text, 1, "#0.00") Then GoTo GestError
    
    Set aLb = Nothing
    
Exit Sub

GestError:
    
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
    Cancel = True
    Set aLb = Nothing

End Sub

Private Sub mTop_GotFocus()
    Me.mTop.SelStart = 0
    Me.mTop.SelLength = 65535
End Sub

Private Sub mTop_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Me.mLeft.SetFocus
End Sub

Private Sub mTop_Validate(Cancel As Boolean)

Dim aLb As Libelle
    
    If Me.mTop.Text = "" Then GoTo GestError
    If Not VerifValue(Me.mTop.Text, 1, "#0.00") Then GoTo GestError
    
    Set aLb = Nothing
    
Exit Sub

GestError:
    
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
    Cancel = True
    Set aLb = Nothing

End Sub

Private Sub Ok_Click()

Dim aMaq As Maquette
Dim aLb As Libelle

Dim Largeur As String
Dim Hauteur As String
Dim mTop As String
Dim mLeft As String

'-> V�rifier le nom de la maquette
If Not IsLegalName(Me.Text1.Text) Then
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(5) & Chr(13) & aLb.GetCaption(6), vbCritical + vbOKOnly, aLb.GetToolTip(6)
    Me.Text1.SetFocus
    Set aLb = Nothing
    Exit Sub
End If

'-> V�rifier le mot cl�
If Not GetValidName(Me.Text1.Text) Then
    ErrorName
    Exit Sub
End If
    
'-> Convertir toutes donn�es num�riques pour les mettres au format qui va bien





'-> V�rification Largeur maquette/marge left
If Not VerifmPage(Convert(Me.Largeur.Text), Convert(Me.mLeft.Text)) Then
    Me.mLeft.SetFocus
    Exit Sub
End If
'-> V�rification Largeur maquette/marge top
If Not VerifmPage(Convert(Me.Hauteur.Text), Convert(Me.mTop.Text)) Then
    Me.mTop.SetFocus
    Exit Sub
End If

'-> Cr�er la nouvelle maquette
Set aMaq = New Maquette
Set aLb = Libelles("FRMPROP")

aMaq.Nom = Me.Text1.Text

'Test du format pr�d�fini
If Me.Option1.Value = True Then 'Option portrait
    aMaq.Hauteur = CSng(Convert(Me.Hauteur.Text))
    aMaq.Largeur = CSng(Convert(Me.Largeur.Text))
    aMaq.Orientation = aLb.GetCaption(12)
Else
    aMaq.Largeur = CSng(Convert(Me.Hauteur.Text))
    aMaq.Hauteur = CSng(Convert(Me.Largeur.Text))
    aMaq.Orientation = aLb.GetCaption(13)
End If

aMaq.MargeLeft = CSng(Convert(Me.mLeft.Text))
aMaq.MargeTop = CSng(Convert(Me.mTop.Text))
aMaq.Description = Me.Description.Text

Set aLb = Nothing

'-> Ajout dans la collection
Maquettes.Add aMaq, "MAQUETTE"

'-> Cr�ation dans le treeView
aMaq.AddTreeView

'-> D�charger la feuille
Unload Me

'-> Fermer la feuille
'Unload frmSample

'-> Faire de la maquette l'objet en cours
Set ObjCours = aMaq

'-> G�rer les menus
GestMenu

Set aMaq = Nothing
Set aLb = Nothing

End Sub

Private Sub Option1_Click()

Me.Portrait.Visible = True
Me.Paysage.Visible = False

'-> Inverser les dimensions
Tempo = Hauteur.Text

Hauteur.Text = Largeur.Text
Largeur.Text = Tempo


End Sub

Private Sub Option2_Click()

Me.Portrait.Visible = False
Me.Paysage.Visible = True

'-> Inverser les dimensions
Tempo = Hauteur.Text

Hauteur.Text = Largeur.Text
Largeur.Text = Tempo


End Sub

Private Sub Text1_GotFocus()

Me.Text1.SelStart = 0
Me.Text1.SelLength = 65535

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then _
    Ok_Click
    
End Sub



Private Sub Text1_Validate(Cancel As Boolean)
    
Dim aLb As Libelle

'-> V�rification du format
If Trim(Me.Text1.Text) = "" Then
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
    Cancel = True
    Set aLb = Nothing
End If
    
End Sub
