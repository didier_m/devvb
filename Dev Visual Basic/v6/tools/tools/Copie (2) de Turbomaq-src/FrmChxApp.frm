VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "comctl32.ocx"
Begin VB.Form FrmChxApp 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8160
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10455
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8160
   ScaleWidth      =   10455
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Appearance      =   0  'Flat
      Height          =   495
      Left            =   9360
      Picture         =   "FrmChxApp.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   7320
      Width           =   735
   End
   Begin MSComctlLib.ImageCombo ImageCombo1 
      Height          =   330
      Left            =   5280
      TabIndex        =   7
      Top             =   7320
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   582
      _Version        =   393216
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Text            =   "ImageCombo1"
      ImageList       =   "ImageList1"
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   9600
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmChxApp.frx":0CCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmChxApp.frx":19A4
            Key             =   "RUB"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmChxApp.frx":367E
            Key             =   "OPENRUB"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmChxApp.frx":5358
            Key             =   "MAQ"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmChxApp.frx":6032
            Key             =   "MOD"
         EndProperty
      EndProperty
   End
   Begin VB.TextBox Text2 
      Height          =   285
      Left            =   240
      TabIndex        =   6
      Top             =   7560
      Width           =   4815
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   285
      Index           =   3
      Left            =   240
      TabIndex        =   5
      Text            =   "Rechercher"
      Top             =   7200
      Width           =   1815
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   285
      Index           =   1
      Left            =   5760
      TabIndex        =   4
      Text            =   "Maquettes"
      Top             =   600
      Width           =   4455
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   285
      Index           =   0
      Left            =   240
      TabIndex        =   3
      Text            =   "Rubriques"
      Top             =   600
      Width           =   4455
   End
   Begin MSComctlLib.ListView ListMaq 
      Height          =   6255
      Left            =   5280
      TabIndex        =   2
      Top             =   960
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   11033
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView ListRub 
      Height          =   6135
      Left            =   240
      TabIndex        =   0
      Top             =   960
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   10821
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2540
      EndProperty
   End
   Begin ComctlLib.TabStrip TabStrip1 
      Height          =   7935
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   13996
      _Version        =   327682
      BeginProperty Tabs {0713E432-850A-101B-AFC0-4210102A8DA7} 
         NumTabs         =   1
         BeginProperty Tab1 {0713F341-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   ""
            Key             =   ""
            Object.Tag             =   ""
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmChxApp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public CodeCours As String
Public IsStandard As Boolean
Public aClient As Client

Private OpenSrc As String
Private OpenRub As String
Private SelKey As String


Public Sub DisplayApp(CodeToDisplay As String)

'---> Affiche les informations dur l'applicatif
Dim aitem As ListItem
Dim i As Integer
Dim aApp  As Application
Dim aCb As ComboItem
Dim aLb As Libelle

On Error Resume Next

Me.Enabled = False
Screen.MousePointer = 11

'-> Pointer sur le libell�
Set aLb = Libelles("FRMSAMPLE")

'-> Vider la liste des maquettes
Me.ListMaq.ListItems.Clear
Me.ListRub.ListItems.Clear
Me.ImageCombo1.ComboItems.Clear
OpenSrc = ""
OpenRub = ""

'-> Pointer sur l'app standard
Set aApp = V6ClientList("CLI|DEAL").Applications("APP|" & CodeToDisplay)
'-> Ajouter  toutes rubriques
For i = 1 To aApp.nRubriques
    Set aitem = Me.ListRub.ListItems.Add(, "RUB|" & UCase$(aApp.GetRubriquesCode(i)), aApp.GetRubriquesName(i), , "RUB")
Next 'Pour toutes les rubriques
FormatListView Me.ListRub
If Me.ListRub.ListItems.Count <> 0 Then
    Set Me.ListRub.SelectedItem = Me.ListRub.ListItems(1)
    SelKey = Me.ListRub.SelectedItem.Key
Else
    SelKey = ""
End If
'-> Ajouter le model standard
Me.ImageCombo1.ComboItems.Add , "MOD|STD", aLb.GetCaption(5), "MOD"
'-> Afficher la liste des mod�les de ce progiciel
DisplayModel aApp
If Me.ImageCombo1.ComboItems.Count <> 0 Then Set Me.ImageCombo1.SelectedItem = Me.ImageCombo1.ComboItems(1)

'-> Faire du code en cours
CodeCours = CodeToDisplay

GestError:
    Me.Enabled = True
    Screen.MousePointer = 0

End Sub

Private Sub DisplayModel(aApp As Application)

Dim aFileName As String
Dim lpKey As String
Dim Res As Long
Dim PathToAnalyse As String

On Error GoTo GestError

Screen.MousePointer = 11
Me.Enabled = False

If IsStandard Then
    PathToAnalyse = Replace(UCase$(V6GuiCharPath), "$APP", aApp.Code)
Else
    PathToAnalyse = Replace(UCase$(V6IdentPath), "$APP", aApp.Code)
    PathToAnalyse = Replace(UCase$(PathToAnalyse), "$IDENT", aClient.Code)
End If
If Right$(PathToAnalyse, 1) <> "\" Then PathToAnalyse = PathToAnalyse + "\"
Me.ListMaq.ListItems.Clear

aFileName = Dir$(PathToAnalyse & "*.drm")
Do While aFileName <> ""
    Me.ImageCombo1.ComboItems.Add , "MOD|" & aFileName, aFileName, "MOD"
    aFileName = Dir
Loop

GestError:
    Screen.MousePointer = 0
    Me.Enabled = True
    If Err.Number <> 0 Then _
        MsgBox "Erreur dans le chargement des mod�les : " & vbCrLf & Err.Number & " - " & Err.Description, vbCritical + vbOKOnly, "Erreur"


End Sub

Private Sub SearchFile(aApp As Application)

Dim aFileName As String
Dim lpKey As String
Dim Res As Long
Dim PathToAnalyse As String

On Error GoTo GestError

If Trim(Text2.Text) = "" Then Exit Sub

Screen.MousePointer = 11
Me.Enabled = False

If IsStandard Then
    PathToAnalyse = Replace(UCase$(V6GuiCharPath), "$APP", aApp.Code)
Else
    PathToAnalyse = Replace(UCase$(V6IdentPath), "$APP", aApp.Code)
    PathToAnalyse = Replace(UCase$(PathToAnalyse), "$IDENT", aClient.Code)
End If
If Right$(PathToAnalyse, 1) <> "\" Then PathToAnalyse = PathToAnalyse + "\"

aFileName = Dir$(PathToAnalyse & "*" & Text2.Text & "*.maqgui")
If aFileName = "" Then
    MsgBox "Recherche termin�e. Pas de fichier trouv�", vbInformation + vbOKOnly, "Message"
    GoTo GestError
End If

lpKey = Space(100)
Res = GetPrivateProfileString("PROGICIEL", "\RUB", "", lpKey, Len(lpKey), PathToAnalyse & aFileName)
If Res <> 0 Then
    '-> Rechercher la rubrique
    lpKey = Mid$(lpKey, 1, Res)
    If Not IsRubrique(lpKey) Then
        MsgBox "Impossible de trouver la rubrique dans le fichier" & vbCrLf & PathToAnalyse & aFileName, vbCritical + vbOKOnly, "Erreur"
        GoTo GestError
    End If
    If UCase$("RUB|" & lpKey) <> SelKey Or Me.ListMaq.ListItems.Count = 0 Then
        Set Me.ListRub.SelectedItem = Me.ListRub.ListItems("RUB|" & UCase$(Trim(lpKey)))
        ListRub_DblClick
    End If
    '-> S�lectionner la maquette
    Set Me.ListMaq.SelectedItem = Me.ListMaq.ListItems("MAQ|" & UCase$(aFileName))
    Me.ListMaq.SelectedItem.EnsureVisible
Else
    MsgBox "Impossible de trouver la rubrique dans le fichier" & vbCrLf & PathToAnalyse & aFileName, vbCritical + vbOKOnly, "Erreur"
    GoTo GestError
End If

GestError:
    Screen.MousePointer = 0
    Me.Enabled = True
    Me.ListMaq.SetFocus
    If Err.Number <> 0 Then _
        MsgBox "Erreur dans le chargement des mod�les : " & vbCrLf & Err.Number & " - " & Err.Description, vbCritical + vbOKOnly, "Erreur"


End Sub

Private Function IsRubrique(Rub As String) As Boolean

Dim aitem As ListItem
On Error GoTo GestError
Set aitem = Me.ListRub.ListItems("RUB|" & UCase$(Trim(Rub)))
IsRubrique = True
Exit Function
GestError:
IsRubrique = False

End Function

Private Sub DispatchFile(aApp As Application)

'---> Cette fonction est charg�e de trier les �ditions par rubrique

Dim aFileName As String
Dim lpKey As String
Dim Res As Long
Dim aitem As ListItem
Dim PathToAnalyse As String

On Error GoTo GestError

Screen.MousePointer = 11
Me.Enabled = False
LockWindowUpdate Me.hWnd
If IsStandard Then
    PathToAnalyse = Replace(UCase$(V6GuiCharPath), "$APP", aApp.Code)
Else
    PathToAnalyse = Replace(UCase$(V6IdentPath), "$APP", aApp.Code)
    PathToAnalyse = Replace(UCase$(PathToAnalyse), "$IDENT", aClient.Code)
End If
If Right$(PathToAnalyse, 1) <> "\" Then PathToAnalyse = PathToAnalyse + "\"
Me.ListMaq.ListItems.Clear

aFileName = Dir$(PathToAnalyse & "*.maqgui")
Do While aFileName <> ""
    lpKey = Space(100)
    Res = GetPrivateProfileString("PROGICIEL", "\RUB", "", lpKey, Len(lpKey), PathToAnalyse & aFileName)
    If Res <> 0 Then
        lpKey = Mid$(lpKey, 1, Res)
        If UCase$(Trim("RUB|" & lpKey)) = SelKey Then
            Set aitem = Me.ListMaq.ListItems.Add(, "MAQ|" & UCase$(aFileName), , , "MAQ")
            aitem.Tag = PathToAnalyse & aFileName
            aitem.Text = aFileName
            aitem.SubItems(1) = PathToAnalyse
        End If
    End If
    '-> Lire le fichier suivant
    aFileName = Dir
    DoEvents
Loop


GestError:
    LockWindowUpdate 0
    FormatListView Me.ListMaq
    Screen.MousePointer = 0
    Me.Enabled = True
    Me.ListMaq.SetFocus
    '-> Poser la cl� de la premi�re maquette s�&mectionn�e
    If Not Me.ListMaq.SelectedItem Is Nothing Then OpenSrc = Me.ListMaq.SelectedItem.Key
    If Err.Number <> 0 Then _
        MsgBox "Erreur dans le chargement des maquettes : " & vbCrLf & Err.Number & " - " & Err.Description, vbCritical + vbOKOnly, "Erreur"

End Sub


Private Sub Command1_Click()

If OpenSrc = "" Then 'MESSPROG
    MsgBox "Veuillez s�lectionner la maquette � ouvrir", vbCritical + vbOKOnly, "Erreur"
    Exit Sub
End If
strRetour = OpenSrc & "|" & CodeCours & "|" & OpenRub
Unload Me

End Sub

Private Sub ImageCombo1_Click()

If Me.ImageCombo1.SelectedItem Is Nothing Then Exit Sub
OpenSrc = Me.ImageCombo1.SelectedItem.Key
OpenRub = ""

End Sub

Private Sub ImageCombo1_LostFocus()

If Me.ImageCombo1.SelectedItem Is Nothing Then Exit Sub
OpenSrc = Me.ImageCombo1.SelectedItem.Key
OpenRub = ""

End Sub

Private Sub ListMaq_DblClick()
Command1_Click
End Sub

Private Sub ListMaq_ItemClick(ByVal Item As MSComctlLib.ListItem)

OpenSrc = Item.Key

End Sub

Private Sub ListRub_DblClick()

If Me.ListRub.SelectedItem Is Nothing Then Exit Sub

On Error Resume Next

'Repositionner l'ancienne icone
Me.ListRub.ListItems(SelKey).SmallIcon = "RUB"
SelKey = Me.ListRub.SelectedItem.Key
Me.ListRub.ListItems(SelKey).SmallIcon = "OPENRUB"
OpenRub = Entry(2, SelKey, "|")

'-> Afficher la liste des maquettes
DispatchFile aClient.Applications("APP|" & CodeCours)


End Sub

Private Sub TabStrip1_Click()

Dim i As Integer
For i = 1 To Me.TabStrip1.Tabs.Count
    If Me.TabStrip1.Tabs(i).Selected Then
        If Me.TabStrip1.Tabs(i).Key <> CodeCours Then
            DisplayApp (Me.TabStrip1.Tabs(i).Key)
            Me.ListRub.SetFocus
        End If
    End If
Next

End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    SearchFile V6ClientList("CLI|DEAL").Applications("APP|" & CodeCours)
End If

End Sub
