VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Application"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'---> D�finition d'une application
Public Code As String
Public Libelle As String
Public V6PathApp As String
Public Rubriques As String
Public nRubriques As Integer
Private pRubriques() As String

Public Sub CreateRubriques()

Dim i As Integer
Dim Def As String

'-> Fonction qui �clate les diff�rentes rubriques
nRubriques = NumEntries(Rubriques, "|")
ReDim pRubriques(1 To 2, 1 To nRubriques)

For i = 1 To nRubriques
    Def = Entry(i, Rubriques, "|")
    pRubriques(1, i) = Entry(1, Def, "@")
    pRubriques(2, i) = Entry(2, Def, "@")
Next

End Sub

Public Function GetRubriquesCode(ByVal Index As Integer) As String

GetRubriquesCode = pRubriques(1, Index)

End Function

Public Function GetRubriquesName(ByVal Index As Integer) As String

GetRubriquesName = pRubriques(2, Index)

End Function

