VERSION 5.00
Begin VB.Form frmDupBlock 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   4140
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   3855
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4140
   ScaleWidth      =   3855
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox NomBlock 
      Height          =   285
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   3615
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   3000
      Value           =   1  'Checked
      Width           =   3615
   End
   Begin VB.ListBox List1 
      Height          =   1620
      Left            =   120
      TabIndex        =   1
      Top             =   1200
      Width           =   3615
   End
   Begin VB.Image Annuler 
      Height          =   480
      Left            =   1680
      Top             =   3480
      Width           =   630
   End
   Begin VB.Image Aide 
      Height          =   480
      Left            =   2400
      Top             =   3480
      Width           =   630
   End
   Begin VB.Image Ok 
      Height          =   480
      Left            =   3120
      Top             =   3480
      Width           =   630
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   3615
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   840
      Width           =   3615
   End
End
Attribute VB_Name = "frmDupBlock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public TableauCours As Tableau
Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub Form_Load()

Dim aLb As Libelle

Set aLb = Libelles("FRMDUPBLOCK")
Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Check1.Caption = aLb.GetCaption(3)
Me.Label2.Caption = aLb.GetCaption(4)
Me.NomBlock.Text = aLb.GetCaption(5) & Me.TableauCours.nBlocks

Set aLb = Libelles("BOUTONS")
Me.Annuler.ToolTipText = aLb.GetCaption(1)
Me.Aide.ToolTipText = aLb.GetCaption(2)
Me.Ok.ToolTipText = aLb.GetCaption(3)

Me.Annuler.Picture = Turbo_Maq.Annuler.Picture
Me.Aide.Picture = Turbo_Maq.Aide.Picture
Me.Ok.Picture = Turbo_Maq.Ok.Picture

Set aLb = Nothing

End Sub

Private Sub NomBlock_GotFocus()

Me.NomBlock.SelStart = 0
Me.NomBlock.SelLength = 65535

End Sub

Private Sub NomBlock_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Ok_Click

End Sub

Private Sub Ok_Click()

Dim aLb As Libelle
Dim aBlock As Block
Dim BlockRef As Block
Dim NbCol As Integer
Dim NbLig As Integer
Dim aCell As Cellule
Dim CellRef As Cellule
Dim aForm As FrmBlock
Dim aField As Field
Dim FieldRef As Field

'---> Proc�dure qui duplique un block existant

'-> V�rifier si toutes les zones sont saisies
If Trim(Me.NomBlock.Text) = "" Then
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(34), vbCritical + vbOKOnly, aLb.GetToolTip(34)
    Me.NomBlock.SetFocus
    Set aLb = Nothing
    Exit Sub
End If
        
'-> V�rifier si le nom est l�gal
If Not IsLegalName(Me.NomBlock.Text) Then
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(5) & Chr(13) & aLb.GetCaption(6), vbCritical + vbOKOnly, aLb.GetToolTip(6)
    Me.NomBlock.SetFocus
    Set aLb = Nothing
    Exit Sub
End If

'-> V�rifier si le nom n'existe pas d�ja
If Me.TableauCours.GetBlockExist(Me.NomBlock) Then
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(12), vbCritical + vbOKOnly, aLb.GetToolTip(12)
    Me.NomBlock.SetFocus
    Set aLb = Nothing
    Exit Sub
End If

'-> Pointer sur le block de r�f�rence
Set BlockRef = Me.TableauCours.Blocks("BL-" & UCase$(Me.List1.Text))

'-> R�cup�rer les valeurs
NbCol = BlockRef.NbCol
NbLig = BlockRef.NbLigne

'-> Dupliquer le block
Set aBlock = New Block
aBlock.Nom = Me.NomBlock.Text
aBlock.Hauteur = BlockRef.Hauteur
aBlock.HauteurPix = BlockRef.HauteurPix
aBlock.IdOrdreAffichage = Me.TableauCours.AddOrdreAffichage("BL-" & Me.NomBlock.Text)
aBlock.Largeur = BlockRef.Largeur
aBlock.LargeurPix = BlockRef.LargeurPix
aBlock.NbCol = BlockRef.NbCol
aBlock.NbLigne = BlockRef.NbLigne
aBlock.NomTb = BlockRef.NomTb
aBlock.Left = BlockRef.Left
aBlock.AlignementLeft = BlockRef.AlignementLeft
aBlock.Top = BlockRef.Top
aBlock.AlignementTop = BlockRef.AlignementTop
aBlock.UseAccesDet = BlockRef.UseAccesDet
aBlock.KeyAccesDet = BlockRef.KeyAccesDet

'-> Initialiser
aBlock.Init

'-> Gestion des hauteurs de ligne variable et des exports vers Excel
For i = 1 To NbLig
    '-> Hauteru de ligne variable
    aBlock.SetVarLig i, BlockRef.GetVarLig(i)
    '-> Export des lignes dans excel
    aBlock.SetExportExcel i, BlockRef.GetExportExcel(i)
Next



For i = 1 To NbCol
    For j = 1 To NbLig
        '-> R�cup�rer les dimensions des colonnes
        If j = 1 Then aBlock.SetLargeurCol i, BlockRef.GetLargeurCol(i)
        '-> R�cuperer les dimensions des lignes
        If i = 1 Then aBlock.SetHauteurLig j, BlockRef.GetHauteurLig(j)
        '-> Pointer sur la cellule de r�f�rence
        Set CellRef = BlockRef.Cellules("L" & j & "C" & i)
        '-> Cr�er la nouvelle cellule
        Set aCell = New Cellule
        '-> dupliquer la cellule
        aCell.AutoAjust = CellRef.AutoAjust
        aCell.BackColor = CellRef.BackColor
        aCell.FondTransParent = CellRef.FondTransParent
        aCell.BordureBas = CellRef.BordureBas
        aCell.BordureDroite = CellRef.BordureDroite
        aCell.BordureGauche = CellRef.BordureGauche
        aCell.BordureHaut = CellRef.BordureHaut
        aCell.CellAlign = CellRef.CellAlign
        aCell.Colonne = CellRef.Colonne
        aCell.Contenu = CellRef.Contenu
        aCell.FontBold = CellRef.FontBold
        aCell.FontColor = CellRef.FontColor
        aCell.FontItalic = CellRef.FontItalic
        aCell.FontName = CellRef.FontName
        aCell.FontSize = CellRef.FontSize
        aCell.FontUnderline = CellRef.FontUnderline
        aCell.IsActive = CellRef.IsActive
        aCell.Ligne = CellRef.Ligne
        aCell.X1 = CellRef.X1
        aCell.X2 = CellRef.X2
        aCell.Y1 = CellRef.Y1
        aCell.Y2 = CellRef.Y2
        aCell.IsActive = False
        aCell.PrintZero = CellRef.PrintZero
        '-> Dupliquer les formats
        aCell.Msk = CellRef.Msk
        aCell.TypeValeur = CellRef.TypeValeur
        '-> Dupliquer les exports vers Excel
        aCell.IsFusion = CellRef.IsFusion
        aCell.ColFusion = CellRef.ColFusion
        '-> Gestion de l'acc�s au d�tail
        aCell.UseAccesDet = CellRef.UseAccesDet
        '-> Ajouer la nouvelle cellule dans le block
        aBlock.Cellules.Add aCell, "L" & j & "C" & i
        '-> Duppliquer les champs s'il y en a
        For Each FieldRef In CellRef.Fields
            Set aField = New Field
            aField.Name = FieldRef.Name
            aField.FormatEdit = FieldRef.FormatEdit
            aField.Present = FieldRef.Present
            aCell.Fields.Add aField, UCase$(aField.Name)
        Next 'Pour tous les champs
    Next 'Pour toutes les lignes
Next 'Pour toutes les colonnes
        
'-> Enregister le lien
If Me.Check1.Value = 1 Then
    '-> inscrire le maitre
    aBlock.MasterLink = Me.List1.Text
    '-> Inscrire l'escalve
    If BlockRef.SlaveLink = "" Then
        BlockRef.SlaveLink = Me.NomBlock.Text
    Else
        BlockRef.SlaveLink = BlockRef.SlaveLink & "|" & Me.NomBlock.Text
    End If
End If

'-> Ajouter le block dans le tableau en cours
Me.TableauCours.Blocks.Add aBlock, "BL-" & UCase$(Me.NomBlock)
'-> Ajouter l'icone dans le treeview
aBlock.AddTreeView Me.TableauCours.Nom
'-> Indiquer que le block est en cours d'�dition
aBlock.IsEdit = True
'-> Cr�er une nouvelle feuille
Set aForm = New FrmBlock
'-> Ajouter dans la collection
Me.TableauCours.frmBlocks.Add aForm, UCase$(aBlock.Nom)
'-> Initaliser le block
aForm.TableauCours = Me.TableauCours.Nom
aForm.BlockCours = aBlock.Nom

'-> Decharger la feuille
Unload Me

'-> Faire un update des blocks
aForm.UpdateBlock

'-> Afficher la nouvelle feuille
aForm.Show
        
        
Set aBlock = Nothing
Set BlockRef = Nothing
Set aForm = Nothing
Set aLb = Nothing
Set aCell = Nothing
Set CellRef = Nothing

        
End Sub
