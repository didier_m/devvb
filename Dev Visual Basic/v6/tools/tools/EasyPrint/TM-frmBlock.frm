VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Begin VB.Form frmBlock 
   Caption         =   "Form1"
   ClientHeight    =   7170
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13380
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   478
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   892
   Begin SHDocVwCtl.WebBrowser WebBrowser1 
      Height          =   975
      Left            =   9720
      TabIndex        =   15
      Top             =   4800
      Width           =   1335
      ExtentX         =   2355
      ExtentY         =   1720
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   "http:///"
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   300
      Left            =   0
      ScaleHeight     =   20
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   73
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   450
      Width           =   1095
      Begin VB.Label Adresse 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   0
         TabIndex        =   13
         Top             =   0
         Width           =   1095
      End
   End
   Begin VB.PictureBox CarretScroll 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   8040
      ScaleHeight     =   17
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   17
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   4560
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.HScrollBar Horizontal 
      Height          =   255
      Left            =   4320
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   5760
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.VScrollBar Vertical 
      Height          =   1095
      Left            =   8160
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   5280
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox EnteteColonne 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   255
      ScaleHeight     =   17
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   760
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   750
      Width           =   11400
   End
   Begin VB.PictureBox Carret 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   0
      ScaleHeight     =   17
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   17
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   750
      Width           =   255
   End
   Begin VB.PictureBox BlockOut 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   3135
      Left            =   240
      ScaleHeight     =   209
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   758
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   1005
      Width           =   11370
   End
   Begin VB.TextBox BarreFormule 
      Height          =   300
      Left            =   1080
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   450
      Width           =   14175
   End
   Begin VB.PictureBox EnteteLigne 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   3135
      Left            =   0
      ScaleHeight     =   209
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   17
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1005
      Width           =   255
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   450
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   13380
      _ExtentX        =   23601
      _ExtentY        =   794
      ButtonWidth     =   820
      ButtonHeight    =   741
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   20
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AlignCell"
            Style           =   5
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Gras"
            ImageIndex      =   1
            Style           =   1
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Italic"
            ImageIndex      =   2
            Style           =   1
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SousLigne"
            ImageIndex      =   3
            Style           =   1
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FontColor"
            ImageIndex      =   4
            Style           =   5
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Bordures"
            ImageIndex      =   15
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "BackColor"
            ImageIndex      =   5
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   1
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "COLOR"
                  Text            =   "Personnalis�"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Autoajust"
            ImageIndex      =   17
            Style           =   1
         EndProperty
         BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ALign"
            ImageIndex      =   19
         EndProperty
         BeginProperty Button16 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button17 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "DelLink"
            ImageIndex      =   20
         EndProperty
         BeginProperty Button18 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button19 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Properties"
            ImageIndex      =   21
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Largeur"
                  Text            =   "Largeur "
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Hauteur"
                  Text            =   "Hauteur"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button20 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "Dimensions"
            ImageIndex      =   22
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin VB.ComboBox ComboSize 
         Height          =   315
         Left            =   10800
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   60
         Width           =   735
      End
      Begin VB.ComboBox ComboFont 
         Height          =   315
         Left            =   9240
         Sorted          =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   60
         Width           =   1455
      End
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   7440
         ScaleHeight     =   225
         ScaleWidth      =   1725
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   100
         Width           =   1755
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   255
         Left            =   11760
         TabIndex        =   14
         Top             =   120
         Width           =   1455
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   1680
      Top             =   5040
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   24
      ImageHeight     =   22
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   22
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":0000
            Key             =   "Bold"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":019A
            Key             =   "Italic"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":0334
            Key             =   "Underline"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":04CE
            Key             =   "FontColor"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":06C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":0862
            Key             =   "Align1"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":0A5C
            Key             =   "Align2"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":0C56
            Key             =   "Align3"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":0E50
            Key             =   "Align4"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":104A
            Key             =   "Align5"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":1244
            Key             =   "Align6"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":143E
            Key             =   "Align7"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":1638
            Key             =   "Align8"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":1832
            Key             =   "Align9"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":1A2C
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":1F1E
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":2118
            Key             =   "Autoajust"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":2312
            Key             =   "AlignBlock"
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":262C
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":2826
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":2F08
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmBlock.frx":35EA
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog CmmColor 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "FrmBlock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---> Contient la variable sur le block en cours
Public BlockCours As String
Public TableauCours    As String

'---> Tableau de gestion des cellules , colonnes lignes ...
Private Cells() As Long
Private Lignes() As Long
Private Colonnes() As Long

'-> d�termine les coordon�es de la cellule active
Private ActiveCell As POINTAPI

'-> Pour Gestion des alignements interne et des couleurs
Public Retour As Long

'-> Variable pour multi s�lection des cellules
Private CellFin As POINTAPI

Private Sub FormatDefault()
Dim aBlock As Block

'-> Obtenir un pointeur surt le block
SelectBlock aBlock

'-> on pointe sur la cellule
Set aCell = aBlock.Cellules("L" & ActiveCell.y & "C" & ActiveCell.x)

'-> Propri�t�s de Font
DefFontName = aCell.FontName
DefFontSize = aCell.FontSize
DefBold = aCell.FontBold
DefItalic = aCell.FontItalic
DefUnderline = aCell.FontUnderline
DefFontColor = aCell.FontColor
            
'-> Alignement interne
DefALignCell = aCell.CellAlign
DefRetourLig = aCell.AutoAjust
                
'-> Bordures
DefBas = aCell.BordureBas
DefDroite = aCell.BordureDroite
DefGauche = aCell.BordureGauche
DefHaut = aCell.BordureHaut
        
'-> couleur de fond
DefBackcolor = aCell.BackColor
        
'-> hauteur de la ligne
DefHauteurLig = aBlock.GetHauteurLig(ActiveCell.y)
        
End Sub

Private Sub SelectBlock(aBlock As Block)

On Error Resume Next

Dim aTb As Tableau

'-> Cette proc�dure s�lectionne le tableau et le block en cours
Set aTb = Maquettes("MAQUETTE").Tableaux(UCase$(TableauCours))
Set aBlock = aTb.Blocks(UCase$("BL-" & Me.BlockCours))

Set aTb = Nothing

End Sub

Private Sub BarreFormule_KeyPress(KeyAscii As Integer)

Dim aCell As Cellule
Dim aBlock As Block

'-> Obtenir un pointeur sur le block
SelectBlock aBlock

If KeyAscii = 13 Then
    Set aCell = aBlock.Cellules("L" & ActiveCell.y & "C" & ActiveCell.x)
    aCell.Contenu = Me.BarreFormule.Text
    Me.BlockOut.SetFocus
    Me.BlockOut.Refresh
ElseIf KeyAscii = 27 Then
    Set aCell = aBlock.Cellules("L" & ActiveCell.y & "C" & ActiveCell.x)
    Me.BarreFormule.Text = aCell.Contenu
    Me.BlockOut.SetFocus
End If

Set aCell = Nothing
Set aBlock = Nothing

End Sub

Private Sub BlockOut_KeyDown(KeyCode As Integer, Shift As Integer)

Dim ColMin As Integer, ColMax As Integer, LigMin As Integer, LigMax As Integer
Dim ColTo As Integer
Dim aBlock As Block


On Error Resume Next

'-> Obtenir un pointeur sur le block courrant
SelectBlock aBlock

Select Case KeyCode

    Case 221
        Me.BarreFormule.SetFocus
        ''Me.BarreFormule.Text = "^"
        Me.BarreFormule.SelStart = 1
        SetBarreFormule

    Case 38 'Fl haut
        If Shift = 1 Then
            If CellFin.y <> 1 Then DetectCellSelected CellFin.y - 1, CellFin.x
        Else
             If ActiveCell.y <> 1 Then SelActiveCell ActiveCell.y - 1, ActiveCell.x
        End If
        GetCellVisible

    Case 39, 9  'Fl droite ou Tab
        If Shift = 1 Then
            If CellFin.x <> aBlock.NbCol Then DetectCellSelected CellFin.y, CellFin.x + 1
        Else
            If ActiveCell.x <> aBlock.NbCol Then SelActiveCell ActiveCell.y, ActiveCell.x + 1
        End If
        GetCellVisible

    Case 40 'Fl bas
        If Shift = 1 Then
            If CellFin.y <> aBlock.NbLigne Then DetectCellSelected CellFin.y + 1, CellFin.x
        Else
            If ActiveCell.y <> aBlock.NbLigne Then SelActiveCell ActiveCell.y + 1, ActiveCell.x
        End If
        GetCellVisible

    Case 37 'Fl gauche
        If Shift = 1 Then
            If CellFin.x <> 1 Then DetectCellSelected CellFin.y, CellFin.x - 1
        Else
            If ActiveCell.x <> 1 Then SelActiveCell ActiveCell.y, ActiveCell.x - 1
        End If
        GetCellVisible

    Case 35, 36  'Fin , D�but

        If KeyCode = 35 Then
            ColTo = aBlock.NbCol
        Else
            ColTo = 1
        End If

        If Shift = 1 Then
            '-> Supprimer les s�lection si necessaire
            If CellFin.x < ActiveCell.x Then
                ColMin = CellFin.x
                ColMax = ActiveCell.x
            Else
                ColMin = ActiveCell.x
                ColMax = CellFin.x
            End If
            If CellFin.y > ActiveCell.y Then
                LigMin = ActiveCell.y
                LigMax = CellFin.y
            Else
                LigMin = CellFin.y
                LigMax = ActiveCell.y
            End If
            InvertCell ColMin, ColMax, LigMin, LigMax, 0, aBlock.NbCol
            CellFin.x = ActiveCell.x
            CellFin.y = ActiveCell.y
            DetectCellSelected CellFin.y, ColTo
        Else
            SelActiveCell ActiveCell.y, ColTo
        End If
        GetCellVisible
        
    Case 46 'Suppr
    
        DelContenu ActiveCell.y, CellFin.y, ActiveCell.x, CellFin.x, aBlock

End Select


Set aBlock = Nothing

End Sub

Private Sub BlockOut_KeyPress(KeyAscii As Integer)

Dim aBlock As Block

'-> Obtenir un pointeur surt le block
SelectBlock aBlock

Select Case KeyAscii

    Case 13 'Edition de la cellule
        Me.BarreFormule.SetFocus
        Me.BarreFormule.SelStart = 0
        Me.BarreFormule.SelLength = 65535
        SetBarreFormule

    Case 27 'Echap
        '-> Ne Rien faire

    Case 8
        Me.BarreFormule.SetFocus
        Me.BarreFormule.Text = ""
        SetBarreFormule

    Case 3 'CTRL + C

        CopyCell aBlock, ActiveCell.y, ActiveCell.x

    Case 6 'CTRL + F

        FormatCell

    Case 22  'CTRL + V

        '-> Modifier pour toutes les cellules s�lectionn�es
        If CellFin.x < ActiveCell.x Then
            ColMin = CellFin.x
            ColMax = ActiveCell.x
        Else
            ColMin = ActiveCell.x
            ColMax = CellFin.x
        End If
        If CellFin.y > ActiveCell.y Then
            LigMin = ActiveCell.y
            LigMax = CellFin.y
        Else
            LigMin = CellFin.y
            LigMax = ActiveCell.y
        End If
        '-> Appliquer les modifications
        For i = ColMin To ColMax
            For j = LigMin To LigMax
                '-> Pointer sur la cellule
                Set aCell = aBlock.Cellules("L" & j & "C" & i)
                '-> Appliquer la modif
                aCell.Contenu = BufferValue
                PasteCell aBlock, j, i, Me.BlockOut
                '-> Elle n'est plus s�lectionn�e
                'Cells(6, (j - 1) * aBlock.NbCol + i) = 0
            Next
        Next
        '-> Remettre en invertion vid�o
        BarreFormule.Text = ClipBoardCell.Contenu

    Case Else

        Me.BarreFormule.SetFocus
        Me.BarreFormule.Text = Chr(KeyAscii)
        Me.BarreFormule.SelStart = 1

End Select

Me.BlockOut.Refresh
Set aBlock = Nothing

End Sub

Private Sub BlockOut_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim aPoint As POINTAPI
Dim aBlock As Block

    
'-> R�cup�ration de la cellule active
aPoint = GetCellAdress(x, y)

'-> Test securit�
If aPoint.x = 0 Or aPoint.y = 0 Then Exit Sub

'-> Afficher la feuille des formats si button vbRightButton
If Button = vbRightButton Then
    RetourMenu = -1
    Me.PopupMenu frmMenu.mnuCellule
    Select Case RetourMenu
        Case 12 'Copier
            BlockOut_KeyPress 3
        
        Case 13 'Coller
            BlockOut_KeyPress 22
        
        Case 11 'Format
            FormatCell
        Case 17 'sauvegarder le format
            FormatDefault
    End Select
Else
    '-> Faire un setting de la nouvelle cellule active
    SelActiveCell aPoint.y, aPoint.x

    '-> R�cup�rer un pointeur sur le block
    SelectBlock aBlock
    
End If


Set aBlock = Nothing

End Sub

Private Sub BlockOut_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim aCell As POINTAPI

If Button = vbLeftButton Then
    If x <= 0 Then
        x = 2
    ElseIf x >= Me.BlockOut.Width Then
        x = Me.BlockOut.Width - 2
    End If

    If y <= 0 Then
        y = 2
    ElseIf y >= Me.BlockOut.Height Then
        y = Me.BlockOut.Height - 2
    End If
    '-> Le bouton gauch est enfonc� : multi-s�lection
    aCell = GetCellAdress(x, y)
    '-> Gestion du multi-cellule
    DetectCellSelected aCell.y, aCell.x
End If


End Sub

Private Sub BlockOut_Paint()

Dim aBlock As Block
Dim aCell As Cellule
Dim i As Integer
Dim j As Integer

On Error Resume Next

'-> Pointer sur le block � �dit�
SelectBlock aBlock

'-> Dessiner le contenu des lignes et des colonnes
For i = 1 To aBlock.NbCol
    For j = 1 To aBlock.NbLigne
        DrawCell j, i, Me.BlockOut, 0, aBlock, Cells(3, (j - 1) * aBlock.NbCol + i)
        If Cells(6, (j - 1) * aBlock.NbCol + i) = 1 Then
            '-> Inverser la cellule dans le fond
            InvertRgn Me.BlockOut.hDC, Cells(3, (j - 1) * aBlock.NbCol + i)
        End If
    Next
Next

'-> Liberer le pointeur
Set aBlock = Nothing


End Sub

Private Sub BlockOut_Resize()
    AutoScroll
End Sub

Private Sub Carret_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim aBlock As Block

'-> Obtenir un pointeur sur le block
SelectBlock aBlock

'---> Repositionner le block � son emplacement d'origine
Me.BlockOut.Top = Me.EnteteColonne.Top + Me.EnteteColonne.Height
Me.Vertical.Value = Me.BlockOut.Top
Me.BlockOut.Left = Me.EnteteLigne.Left + Me.EnteteLigne.Width
Me.Horizontal.Value = Me.BlockOut.Left

'---> S�lection de l'ensemble des cellules
SelActiveCell 1, 1

'---> S�lectionner le block entier
InvertCell 1, aBlock.NbCol, 1, aBlock.NbLigne, 1, aBlock.NbCol

CellFin.x = aBlock.NbCol
CellFin.y = aBlock.NbLigne

Me.BlockOut.Refresh

If Button = vbRightButton Then
    RetourMenu = -1
    Me.PopupMenu frmMenu.mnuCarret
    Select Case RetourMenu
    
        Case 9 'Effacer le contenu du block
            DelContenu 1, aBlock.NbLigne, 1, aBlock.NbCol, aBlock
    
        Case 10 'Appliquer un format au block
            FormatCell
    
    End Select
    
End If

'-> liberer le pointeur
Set aBlock = Nothing

End Sub

Private Sub Carret_Paint()

Dim Contour As Rect 'Rectangle pour contour de la zone
Dim PosX As Integer
Dim i As Integer
Dim aBlock As Block

'-> R�cup�ration d'un pointeur sur le block en cours
SelectBlock aBlock

'-> Dimension
Contour.Left = 1
Contour.Top = 1
Contour.Right = Me.Carret.Width - 1
Contour.Bottom = Me.Carret.Height - 1

'-> Dessiner
DrawEnteteObj Me.Carret, -1, Contour

'-> Liberer le ponteur
Set aBlock = Nothing

End Sub

Private Sub ComboFont_Click()

Dim aCell As Cellule
Dim ColMin As Integer, ColMax As Integer, LigMin As Integer, LigMax As Integer
Dim i As Integer, j As Integer
Dim aBlock As Block

'-> Obtenir un pointeur
SelectBlock aBlock

'-> D�terminer le carret de s�lection
If CellFin.x < ActiveCell.x Then
    ColMin = CellFin.x
    ColMax = ActiveCell.x
Else
    ColMin = ActiveCell.x
    ColMax = CellFin.x
End If
If CellFin.y > ActiveCell.y Then
    LigMin = ActiveCell.y
    LigMax = CellFin.y
Else
    LigMin = CellFin.y
    LigMax = ActiveCell.y
End If

For i = ColMin To ColMax
    For j = LigMin To LigMax
        '-> Pointer sur la cellule
        Set aCell = aBlock.Cellules("L" & j & "C" & i)
        '-> Aplliquer la modif
        aCell.FontName = Me.ComboFont.Text
        '-> Elle n'est plus s�lectionn�e
        Cells(6, (j - 1) * aBlock.NbCol + i) = 0
    Next
Next

'-> Modifier le contenu de la barre de formule
SetBarreFormule

'-> Liberer la ressource
Set aCell = Nothing
Set aBlock = Nothing

Me.BlockOut.Refresh

End Sub

Private Sub ComboSize_Click()

Dim aCell As Cellule
Dim ColMin As Integer, ColMax As Integer, LigMin As Integer, LigMax As Integer
Dim i As Integer, j As Integer
Dim aBlock As Block

'-> Obtenir un pointeu
SelectBlock aBlock

'-> D�terminer le carret de s�lection
If CellFin.x < ActiveCell.x Then
    ColMin = CellFin.x
    ColMax = ActiveCell.x
Else
    ColMin = ActiveCell.x
    ColMax = CellFin.x
End If
If CellFin.y > ActiveCell.y Then
    LigMin = ActiveCell.y
    LigMax = CellFin.y
Else
    LigMin = CellFin.y
    LigMax = ActiveCell.y
End If

For i = ColMin To ColMax
    For j = LigMin To LigMax
        '-> Pointer sur la cellule
        Set aCell = aBlock.Cellules("L" & j & "C" & i)
        '-> Aplliquer la modif
        aCell.FontSize = CInt(Me.ComboSize.Text)
        '-> Elle n'est plus s�lectionn�e
        Cells(6, (j - 1) * aBlock.NbCol + i) = 0
    Next
Next

Me.BlockOut.Refresh

'-> Liberer la ressource
Set aCell = Nothing
Set aBlock = Nothing

End Sub

Private Sub ModifLargeur(ByVal aLargeur As Single, ByVal Cm As Boolean, ByVal aCol As Integer)

Dim LargeurCol As Single
Dim LargeurPix As Integer
Dim DifPix As Integer
Dim LinkedBlock As Block
Dim IsLinked As Boolean
Dim i As Integer
Dim aBlock As Block, aBlock2 As Block
Dim aTb As Tableau
Dim aForm As FrmBlock

'-> gestion des erreurs
On Error GoTo GestError

'-> Objtenir un pointeur sur le block courrant
SelectBlock aBlock

'-> Pointeur sur le tableau
Set aTb = Maquettes("MAQUETTE").Tableaux(UCase$(Me.TableauCours))

'-> Convertir en pixel si necessaire
If Cm Then
    '-> Ancienne Largeur
    LargeurCol = aBlock.GetLargeurCol(aCol)
    '-> Modifier la largeur globale du block en cours.
    aBlock.Largeur = aBlock.Largeur - LargeurCol + CSng(aLargeur)
    '-> Modifier la largeur de la colonne dans les propri�t�s
    aBlock.SetLargeurCol aCol, CSng(aLargeur)
    '-> Calcul de la nouvelle largeur en pixel
    LargeurPix = CInt(Me.ScaleX(aBlock.Largeur, 7, 3))
    '-> R�cup�rer la variation de pixels
    DifPix = LargeurPix - Me.BlockOut.Width
    Else
    '-> R�cup�rer la diff�rence
    DifPix = aLargeur
End If

'-> Modifier la largeur de la colonne qui correspond � la cellule active
Colonnes(aCol) = Colonnes(aCol) + DifPix
'-> Appliquer les modifications au block en cours
ApplicModifLargeur aBlock, DifPix, aCol

'-> Appliquer les modifications aux blocks li�s
'-> Appliquer les modifs pour les blocks li�s
If aBlock.SlaveLink <> "" Then
    For i = 1 To NumEntries(aBlock.SlaveLink, "|")
        Set aBlock2 = aTb.Blocks("BL-" & UCase$(Entry(i, aBlock.SlaveLink, "|")))
        If aBlock2.IsEdit Then
            Set aForm = aTb.frmBlocks(UCase$(aBlock2.Nom))
            aForm.UpdateModifLargeur aLargeur, Cm, aCol
            Set aForm = Nothing
        Else
            aBlock2.Largeur = aBlock.Largeur
            aBlock2.SetLargeurCol aCol, CSng(aLargeur)
            ApplicModifLargeur aBlock2, DifPix, aCol
        End If
        Set aBlock2 = Nothing
    Next
    Me.ZOrder
End If

'-> Liberer les pointeurs
Set aBlock = Nothing
Set aTb = Nothing
Set aForm = Nothing
Set aBlock2 = Nothing

DisplayDimensions

'-> Redonner le focus au block
Me.BlockOut.SetFocus
Exit Sub
GestError:
MsgBox "Erreur de saisie"
End Sub

Private Sub EnteteColonne_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim aPt As POINTAPI
Dim aBlock As Block
Dim aLb As Libelle
Dim Rep
Dim LargeurCol As Single
Dim i As Integer
Dim ColMin As Integer
Dim ColMax As Integer


'-> Faire un scroll du block pour qu'il se trouve en haut
Me.BlockOut.Top = Me.EnteteColonne.Top + Me.EnteteColonne.Height
Me.EnteteLigne.Top = Me.BlockOut.Top
Me.Vertical.Value = Me.BlockOut.Top

aPt = GetCellAdress(x, 2)
If aPt.x = 0 Or aPt.y = 0 Then Exit Sub

If ActiveCell.x < CellFin.x Then
    ColMin = ActiveCell.x
    ColMax = CellFin.x
Else
    ColMin = CellFin.x
    ColMax = ActiveCell.x
End If

'-> Pointer sur un block
SelectBlock aBlock

If Button = vbRightButton Then
    
    '-> Test Lien
    If aBlock.MasterLink <> "" Then
        frmMenu.mnuDelCol.Enabled = False
        frmMenu.mnuModifLargeurCol.Enabled = False
        frmMenu.mnuInsertCol.Enabled = False
    Else
        If aBlock.NbCol = 1 Then
            frmMenu.mnuDelCol.Enabled = False
        Else
            frmMenu.mnuDelCol.Enabled = True
        End If
        frmMenu.mnuModifLargeurCol.Enabled = True
        frmMenu.mnuInsertCol.Enabled = True
    End If
    
    Me.PopupMenu frmMenu.mnucolonne

    '-> Tester le retour menu
    If RetourMenu <> -1 Then
    
        Select Case RetourMenu
        
            Case 1 'Insertion colonne
                InsertCol ActiveCell.x
                            
            Case 2 'Supression colonne
                '-> Demander la confirmation
                Set aLb = Libelles("FRMMENU")
                Rep = MsgBox(aLb.GetCaption(7), vbQuestion + vbYesNo, aLb.GetToolTip(7))
                If Rep <> vbYes Then
                    Set aLb = Nothing
                    Exit Sub
                End If
                '-> V�rifier que toutes les colonnes ne sont pas s�lectionn�es
                If ColMax - ColMin = aBlock.NbCol - 1 Then
                   MsgBox aLb.GetCaption(23), vbCritical + vbOKOnly, aLb.GetToolTip(23)
                   Set aLb = Nothing
                   Exit Sub
                End If
                '-> Suppression des colonnes
                For i = ColMin To ColMax
                    DelCol ActiveCell.x
                Next
                
            Case 3 'Effacer le contenu de la colonne
                DelContenu 1, aBlock.NbLigne, ColMin, ColMax, aBlock
                
            Case 4 'Appliquer un format � la colonne
                FormatCell
                
            Case 14 'Largeur de colonnes
            
                Set aLb = Libelles("FRMBLOCK")
                LargeurCol = aBlock.GetLargeurCol(ActiveCell.x)
                Rep = InputBox(aLb.GetCaption(10), aLb.GetToolTip(10), Format(LargeurCol, "0.0000"))
            
                '-> Tester
                If Rep = "" Then Exit Sub

                '-> Tester si numeric
                If Not IsNumeric(Rep) Then
                    Set aLb = Libelles("MESSAGE")
                    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
                    Exit Sub
                End If
                
                '-> Largeur Minimum
                If CSng(Rep) < 0.1 Then
                    Set aLb = Libelles("FRMBLOCK")
                    MsgBox aLb.GetCaption(12), vbCritical + vbOKOnly, aLb.GetToolTip(12)
                    Exit Sub
                End If
                
                '-> Appliquer la modification
                If ActiveCell.x < CellFin.x Then
                    For i = ActiveCell.x To CellFin.x
                        LargeurCol = aBlock.GetLargeurCol(i)
                        If Format(CSng(Rep), "0.0000") <> Format(LargeurCol, "0.0000") Then ModifLargeur Rep, True, i
                    Next
                Else
                    For i = CellFin.x To ActiveCell.x
                        LargeurCol = aBlock.GetLargeurCol(i)
                        If Format(CSng(Rep), "0.0000") <> Format(LargeurCol, "0.0000") Then ModifLargeur Rep, True, i
                    Next
                End If

                                    
        End Select
    End If
    
Else
    '-> Changement de la cellule active
    SelActiveCell 1, aPt.x
    
End If

'-> Lib�rer le pointeur
Set aBlock = Nothing
Set aLb = Nothing

'-> Redonner le focus au block
Me.BlockOut.Refresh
Me.BlockOut.SetFocus
Me.EnteteColonne.Refresh

End Sub

Private Sub EnteteColonne_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim aPt As POINTAPI
Dim aBlock As Block

aPt = GetCellAdress(x, 2)
If aPt.x = 0 Or aPt.y = 0 Then Exit Sub

If Button = vbLeftButton Then

    '-> Pointer sur le block courant
    SelectBlock aBlock

    DetectCellSelected aBlock.NbLigne, aPt.x

    '-> Lib�rer le pointeur
    Set aBlock = Nothing
    
End If


End Sub

Private Sub EnteteColonne_Paint()

'---> Porc�dure qui dessine l'entete de la colonne sp�cifi�e selon les dimensions _
de la colonne et l'�tat de s�lection

Dim Contour As Rect 'Rectangle pour contour de la zone
Dim PosX As Integer
Dim i As Integer
Dim aBlock As Block

On Error Resume Next

'-> R�cup�ration d'un pointeur sur le block en cours
SelectBlock aBlock

'-> Faire une boucle pour dessiner toutes les colonnes
For i = 1 To aBlock.NbCol
    PosX = Cells(4, i)
    Contour.Left = PosX
    Contour.Top = 1
    Contour.Right = PosX + Colonnes(i)
    Contour.Bottom = Me.EnteteColonne.Height - 1

    DrawEnteteObj Me.EnteteColonne, i, Contour
    
Next 'Pour toutes les colonnes

'-> Lib�rer le pointeur sur le block
Set aBlock = Nothing

End Sub

Private Sub CreateBlock(ByVal PixelDepartX As Integer, ByVal PixelDepartY)
                       
'---> Cette proc�dure cr�er les cellules associ�es � un block, d�termine les diff�rentes r�gions _
et met � jour les propri�t�s globales de gestion des cellules

Dim i As Integer
Dim j As Integer
Dim NbCol As Integer
Dim NbLig As Integer
Dim aCell As Cellule

Dim PosX As Integer
Dim PosY As Integer

Dim Res As Long
Dim hWndRegion As Long

Dim aBlock As Block

'-> R�cup�rer un pointeur sur le block en cours
SelectBlock aBlock

NbCol = aBlock.NbCol
NbLig = aBlock.NbLigne

PosY = PixelDepartX
PosX = PixelDepartY

'DrawEntete 0, True, False

For i = 1 To NbCol

    For j = 1 To NbLig
    
        '-> Cr�er les diff�rentes r�gions
        hWndRegion = CreateRectRgn(PosX, PosY, PosX + Colonnes(i) + 1, PosY + Lignes(j) + 1)
        
        '-> Mettre � jour les valeurs des cellules
        Set aCell = aBlock.Cellules("L" & j & "C" & i)
        aCell.X1 = PosX
        aCell.Y1 = PosY
        aCell.X2 = PosX + Colonnes(i) + 1
        aCell.Y2 = PosY + Lignes(j) + 1
        
        '-> Ajout dans la matrice des cellules
        Cells(1, (j - 1) * NbCol + i) = i 'Stocker la colonne
        Cells(2, (j - 1) * NbCol + i) = j 'Stocker la ligne
        Cells(3, (j - 1) * NbCol + i) = hWndRegion 'Handle de la r�gion associ�
        Cells(4, (j - 1) * NbCol + i) = PosX  ' Absisse de d�part
        Cells(5, (j - 1) * NbCol + i) = PosY  'Ordonn�e de d�part
        Cells(6, (j - 1) * NbCol + i) = 0 ' Non S�lectionn�
    
        '-> Incr�menter le compteur de position
        PosY = PosY + Lignes(j) + 1
        
    Next 'pour toutes les lignes
    
    '-> initialisation des valeurs
    PosX = PosX + Colonnes(i) + 1
    PosY = PixelDepartY
    
Next 'Pour toutes les colonnes


'-> Indiquer que la cellule active est L1C1
ActiveCell.x = 1
ActiveCell.y = 1

'-> Pour multi-s�lection de cellules
CellFin.x = 1
CellFin.y = 1

'-> Indiquer son adresse
Me.Adresse.Caption = "L1C1"

'-> Afficher les options de la cellule active
Set aCell = aBlock.Cellules("L1C1")
aCell.IsActive = True

'DisplayCellProperties aCell

'-> Gestion des liens
'If Me.BlockCours.MasterLink <> "" Then
'    IsLinked = True
'Else
'    IsLinked = False
'End If
'
Set aCell = Nothing
Set aBlock = Nothing

End Sub

Public Sub Initialisation()

'---> Cette proc�dure initalise un block en calculant toutes les valeurs requises

'-> Calcul des largeurs de colonnes
InitBlock
'-> intialisation des regions associ�es
CreateBlock 1, 1

End Sub

Private Sub EnteteLigne_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    
Dim aPt As POINTAPI
Dim aBlock As Block
Dim aLb As Libelle
Dim Rep
Dim HauteurLig As Single
Dim LigMin As Integer
Dim LigMax As Integer

'On Error GoTo GestError

aPt = GetCellAdress(2, y)
If aPt.x = 0 Or aPt.y = 0 Then Exit Sub

'-> Pointer sur le block courant
SelectBlock aBlock
    
If ActiveCell.y < CellFin.y Then
    LigMin = ActiveCell.y
    LigMax = CellFin.y
Else
    LigMin = CellFin.y
    LigMax = ActiveCell.y
End If
    
If Button = vbRightButton Then
    '-> Initaliser � false la valeur de retour
    RetourMenu = -1
    '-> Test Lien
    If aBlock.MasterLink <> "" Then
        frmMenu.mnuDelLigne.Enabled = False
        frmMenu.mnuModifHauteurLig.Enabled = False
        frmMenu.mnuinsertLigne.Enabled = False
        frmMenu.mnuinsertLigneBas.Enabled = False
    Else
        If aBlock.NbLigne = 1 Then
            frmMenu.mnuDelLigne.Enabled = False
        Else
            frmMenu.mnuDelLigne.Enabled = True
        End If
        frmMenu.mnuModifHauteurLig.Enabled = True
        frmMenu.mnuinsertLigne.Enabled = True
        frmMenu.mnuinsertLigneBas.Enabled = True
    End If
        
    '-> V�rifier si la ligne est d�finie comme variable
    frmMenu.mnuVarLig.Checked = aBlock.GetVarLig(LigMin)
    frmMenu.mnuExportExcel.Checked = aBlock.GetExportExcel(LigMin)
        
    Me.PopupMenu frmMenu.mnuLigne

    '-> Tester le retour menu
    If RetourMenu <> -1 Then
    
        Select Case RetourMenu
        
            Case 5 'Insertion ligne
                InsertLig ActiveCell.y
            Case 16 'Insertion ligne en dessous
                InsertLigBas ActiveCell.y
            Case 6 'Supression ligne
                '-> Demander la confirmation
                Set aLb = Libelles("FRMMENU")
                Rep = MsgBox(aLb.GetCaption(8), vbQuestion + vbYesNo, aLb.GetToolTip(8))
                If Rep <> vbYes Then
                    Set aLb = Nothing
                    Exit Sub
                End If
                '-> V�rifier que toutes les lignes ne sont pas s�lectionn�es
                If LigMax - LigMin = aBlock.NbLigne - 1 Then
                   MsgBox aLb.GetCaption(24), vbCritical + vbOKOnly, aLb.GetToolTip(24)
                   Set aLb = Nothing
                   Exit Sub
                End If
                '-> Supression des lignes s�lectionn�es
                For i = LigMin To LigMax
                    DelLig ActiveCell.y
                Next
                
            Case 7 'Effacer le contenu de la ligne
                DelContenu LigMin, LigMax, 1, aBlock.NbCol, aBlock
            
            Case 8 'Appliquer un format � la ligne
                FormatCell
                
            Case 15
                            
                Set aLb = Libelles("FRMBLOCK")
                HauteurLig = aBlock.GetHauteurLig(ActiveCell.y)
                Rep = InputBox(aLb.GetCaption(11), aLb.GetToolTip(11), Format(HauteurLig, "0.00"))
                
                '-> Tester
                If Rep = "" Then Exit Sub
                
                '-> Tester si numeric
                If Not IsNumeric(Rep) Then
                    Set aLb = Libelles("MESSAGE")
                    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
                    Exit Sub
                End If
                
                '-> Hauteur Minimum
                If CSng(Rep) < 0.3 Then
                    Set aLb = Libelles("FRMBLOCK")
                    MsgBox aLb.GetCaption(13), vbCritical + vbOKOnly, aLb.GetToolTip(13)
                    Exit Sub
                End If
                                            
                '-> Appliquer la modification
                If ActiveCell.y < CellFin.y Then
                    For i = ActiveCell.y To CellFin.y
                        HauteurLig = aBlock.GetHauteurLig(i)
                        '-> V�rifier si la valeur a chang�e
                        If Format(CSng(Rep), "0.00") <> Format(HauteurLig, "0.00") Then ModifHauteur Rep, True, i
                    Next
                Else
                    For i = CellFin.y To ActiveCell.y
                        HauteurLig = aBlock.GetHauteurLig(i)
                        '-> V�rifier si la valeur a chang�e
                        If Format(CSng(Rep), "0.00") <> Format(HauteurLig, "0.00") Then ModifHauteur Rep, True, i
                    Next
                End If
               
                Me.Refresh
                Set aLb = Nothing
                
            Case 99
                '-> Modifier si une ligne est variable ou non
                aBlock.SetVarLig LigMin, Not (aBlock.GetVarLig(LigMin))
            Case 998
                '-> Acc�s au d�tail
                Call InitAccesDet
            Case 999
                '-> Modifier si une ligne est exportable ou non dans excel
                aBlock.SetExportExcel LigMin, Not (aBlock.GetExportExcel(LigMin))
                               
        End Select
        
    End If

Else
    '-> Changement de la cellule active
    SelActiveCell aPt.y, 1
End If

'-> Liberer le pointeur
Set aBlock = Nothing

Exit Sub

GestError:
MsgBox "Une erreur critique est survenue. "

End Sub

Private Sub InitAccesDet()

'---> Cette proc�dure initialise l'acc�s au d�tail

Dim aBlock As Block

'-> Get d'un  pointeur vers le block
SelectBlock aBlock

'-> Charger la feuille
Load frmAccDet

'-> Initialiser
Call frmAccDet.Init(aBlock)

'-> Afficher la feuille
frmAccDet.Show vbModal

End Sub

Private Sub EnteteLigne_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim aPt As POINTAPI
Dim aBlock As Block

aPt = GetCellAdress(2, y)
If aPt.x = 0 Or aPt.y = 0 Then Exit Sub

If Button = vbLeftButton Then

    '-> Pointer sur le block courant
    SelectBlock aBlock

    DetectCellSelected aPt.y, aBlock.NbCol

    '-> Lib�rer le pointeur
    Set aBlock = Nothing
    
End If

End Sub

Private Sub EnteteLigne_Paint()

Dim Contour As Rect 'Rectangle pour contour de la zone
Dim PosY As Integer
Dim i As Integer
Dim aBlock As Block

On Error Resume Next

'-> R�cup�ration d'un pointeur sur le block en cours
SelectBlock aBlock

'-> Faire une boucle pour dessiner toutes les colonnes
For i = 1 To aBlock.NbLigne
    '-> Positionner
    PosY = Cells(5, (i - 1) * aBlock.NbCol + 1)
    Contour.Left = 1
    Contour.Top = PosY
    Contour.Right = Me.EnteteLigne.Width - 1
    Contour.Bottom = PosY + Lignes(i)
    '-> Dessiner
    DrawEnteteObj Me.EnteteLigne, i, Contour
Next

'-> Liberer le pointeur
Set aBlock = Nothing


End Sub

Private Sub Form_Activate()

'-> Obtenir un pointeur
Dim aBlock As Block

SelectBlock aBlock

Set ObjCours = aBlock
'Me.BlockOut.SetFocus

'-> Pointer sur la cellule active
Set aCell = aBlock.Cellules("L" & ActiveCell.y & "C" & ActiveCell.x)
'DisplayCellProperties aCell


Set aBlock = Nothing
Set aCell = Nothing

End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim aBlock As Block

'-> Obtenir un pointeur
SelectBlock aBlock

Set aLb = Libelles("FRMBLOCK")

Me.Caption = aLb.GetCaption(1) & Me.BlockCours & "]"
Me.Toolbar1.Buttons("AlignCell").ToolTipText = aLb.GetCaption(2)
Me.Toolbar1.Buttons("Gras").ToolTipText = aLb.GetCaption(3)
Me.Toolbar1.Buttons("Italic").ToolTipText = aLb.GetCaption(4)
Me.Toolbar1.Buttons("SousLigne").ToolTipText = aLb.GetCaption(5)
Me.Toolbar1.Buttons("FontColor").ToolTipText = aLb.GetCaption(6)
Me.Toolbar1.Buttons("Bordures").ToolTipText = aLb.GetCaption(7)
Me.Toolbar1.Buttons("BackColor").ToolTipText = aLb.GetCaption(8)
Me.Toolbar1.Buttons("Autoajust").ToolTipText = aLb.GetCaption(9)
Me.Toolbar1.Buttons("ALign").ToolTipText = aLb.GetCaption(14)
If aBlock.MasterLink = "" Then
    Me.Toolbar1.Buttons("DelLink").ToolTipText = aLb.GetCaption(18)
Else
    Me.Toolbar1.Buttons("DelLink").ToolTipText = aLb.GetCaption(17) & aBlock.MasterLink
End If
Me.Toolbar1.Buttons("Properties").ToolTipText = aLb.GetCaption(21)
Me.Picture2.ToolTipText = aLb.GetCaption(22)

Set aLb = Nothing

'---> Charger les polices
For i = 1 To Screen.FontCount - 1
    Me.ComboFont.AddItem Screen.Fonts(i)
Next

'-> Ajout des tailles de police
For i = 6 To 76 Step 1
    Me.ComboSize.AddItem i
Next

'-> Liberer le pointeur
Set aBlock = Nothing

Me.EnteteColonne.Refresh
Me.EnteteLigne.Refresh

ActiveCell.x = 1
ActiveCell.y = 1
CellFin.x = 1
CellFin.y = 1

DisplayDimensions

End Sub

Private Sub DrawEnteteObj(Sortie As PictureBox, ByVal Id As Integer, Contour As Rect)


Dim hdlPen As Long
Dim hdlBrush As Long
Dim oldPen As Long
Dim oldBrush As Long
Dim aPoint As POINTAPI


'-> Cr�ation des objets GDI pour dessin des cellules
If Selected Then
Else
    hdlBrush = CreateSolidBrush(&HE0E0E0)
    hdlPen = CreatePen(PS_SOLID, 1, &HE0E0E0)
End If

'-> S�lection du pinceau dans le Contexte
oldBrush = SelectObject(Sortie.hDC, hdlBrush)

'-> Dessin de la cellule
oldPen = SelectObject(Sortie.hDC, hdlPen)

'-> Dessin du contour
Res = Rectangle(Sortie.hDC, Contour.Left, Contour.Top, Contour.Right + 1, Contour.Bottom + 1)

'-> Res�lectionner l'ancien pinceau
If oldBrush <> 0 Then
    SelectObject Sortie.hDC, oldBrush
    DeleteObject hdlBrush
End If

'-> Supprimer l'ancien Stylo
If oldPen <> 0 Then
    SelectObject Sortie.hDC, oldPen
    DeleteObject hdlPen
End If

'-> Dessin de la bordure gauche
hdlPen = CreatePen(PS_SOLID, 1, QBColor(15))
oldPen = SelectObject(Sortie.hDC, hdlPen)
Res = MoveToEx(Sortie.hDC, Contour.Left, Contour.Top, aPoint)
LineTo Sortie.hDC, Contour.Left, Contour.Bottom

'-> Dessin de la bordure haut
Res = MoveToEx(Sortie.hDC, Contour.Left, Contour.Top, aPoint)
LineTo Sortie.hDC, Contour.Right - 1, Contour.Top

'-> Suppression des GDI
If oldPen <> 0 Then SelectObject Sortie.hDC, oldPen
DeleteObject hdlPen

'-> Dessin de la bordure Droite
hdlPen = CreatePen(PS_SOLID, 1, RGB(128, 128, 128))
oldPen = SelectObject(Sortie.hDC, hdlPen)
Res = MoveToEx(Sortie.hDC, Contour.Right - 1, Contour.Top, aPoint)
LineTo Sortie.hDC, Contour.Right - 1, Contour.Bottom

'-> Dessin de la bordure Bas
Res = MoveToEx(Sortie.hDC, Contour.Left, Contour.Bottom - 1, aPoint)
LineTo Sortie.hDC, Contour.Right - 1, Contour.Bottom - 1

'-> Suppression des GDI
If oldPen <> 0 Then SelectObject Sortie.hDC, oldPen
DeleteObject hdlPen

'-> Cr�er le stylo de dessin
hdlPen = CreatePen(ps_solide, 1, QBColor(0))
oldPen = SelectObject(Sortie.hDC, hdlPen)
'-> Dessiner le num�ro
If Id <> -1 Then _
    DrawText Sortie.hDC, CStr(Id), Len(CStr(Id)), Contour, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE

'-> Res�lectionner l'ancien stylo
If oldPen <> 0 Then
    SelectObject Sortie.hDC, oldPen
    Res = DeleteObject(hdlPen)
End If

End Sub

Private Sub AutoScroll()

'---> Cette fonction sert � g�rer les scrolls dans le block

Dim Rect1 As Rect
Dim Rect2 As Rect
Dim Res As Long

On Error Resume Next

Dim LargeurManque As Integer
Dim HauteurManque As Integer
Dim LargeurBordure As Integer

'-> R�cup�ration des diff�rentes donn�es
Res = GetClientRect(Me.Hwnd, Rect1)
Rect2.Left = Me.BlockOut.Left
Rect2.Right = Me.BlockOut.Left + Me.BlockOut.Width
Rect2.Top = Me.BlockOut.Top
Rect2.Bottom = Me.BlockOut.Top + Me.BlockOut.Height

LargeurBordure = GetSystemMetrics(SM_CXEDGE)

LargeurManque = Rect1.Right - Rect2.Right - LargeurBordure
HauteurManque = Rect1.Bottom - Rect2.Bottom - LargeurBordure

'-> Positionner la barre de defilement horizontale
Me.Horizontal.Left = 0
Me.Horizontal.Top = Rect1.Bottom - Me.Horizontal.Height
Me.Horizontal.Width = (Rect1.Right - Rect1.Left)
    
'-> Positionner la barre de d�filement verticale
Me.Vertical.Left = Rect1.Right - Me.Vertical.Width
Me.Vertical.Top = Me.EnteteColonne.Top
Me.Vertical.Height = Rect1.Bottom - Me.Vertical.Top
    
'-> Ajuster si deux barres sont visibles
If LargeurManque < 0 Then HauteurManque = HauteurManque - Me.Horizontal.Height
If HauteurManque < 0 Then LargeurManque = LargeurManque - Me.Vertical.Width
    
'-> Rendre visible la barre horizontale
If LargeurManque < 0 Then
    Me.Horizontal.Min = Me.EnteteLigne.Left + Me.EnteteLigne.Width
    Me.Horizontal.Max = Me.BlockOut.Left + LargeurManque
    Me.Horizontal.Value = Me.BlockOut.Left
    Me.Horizontal.LargeChange = Abs(CInt(LargeurManque) / 5)
    Me.Horizontal.SmallChange = 5
    Me.Horizontal.Visible = True
Else
    '-> Repositionner le block � son emplacement d'origine
    Me.BlockOut.Left = Me.EnteteLigne.Left + Me.EnteteLigne.Width
    Me.EnteteColonne.Left = Me.BlockOut.Left
    Me.Horizontal.Visible = False
End If

'-> Rendre visible la barre verticale
If HauteurManque < 0 Then
    Me.Vertical.Min = Me.EnteteColonne.Top + Me.EnteteColonne.Height
    Me.Vertical.Max = Me.BlockOut.Top + HauteurManque
    Me.Vertical.Value = Me.BlockOut.Top
    Me.Vertical.LargeChange = Abs(CInt(HauteurManque) / 5)
    Me.Vertical.SmallChange = 5
    Me.Vertical.Visible = True
Else
    '-> Reposionner le block � son emplacement d'origine
    Me.BlockOut.Top = Me.EnteteColonne.Top + Me.EnteteColonne.Height
    Me.EnteteLigne.Top = Me.BlockOut.Top
    Me.Vertical.Visible = False
End If

'-> Modifier si necessaire
If LargeurManque < 0 And HauteurManque < 0 Then
    Me.Vertical.Height = Me.Vertical.Height - Me.Horizontal.Height
    Me.Horizontal.Width = Me.Horizontal.Width - Me.Vertical.Width
    Me.CarretScroll.Top = Me.Vertical.Top + Me.Vertical.Height + 1
    Me.CarretScroll.Left = Me.Horizontal.Left + Me.Horizontal.Width + 1
    Me.CarretScroll.Visible = True
    Me.CarretScroll.ZOrder
Else
    Me.CarretScroll.Visible = False
End If

End Sub

Private Sub GetCellVisible()

Dim Res As Long
Dim aRect As Rect
Dim ClipRgn As Long
Dim aBlock As Block
Dim aCell As Cellule

On Error Resume Next

'-> Pointer sur le block actif
SelectBlock aBlock

'-> Pointer sur la cellule active
Set aCell = aBlock.Cellules("L" & ActiveCell.y & "C" & ActiveCell.x)

'-> R�cup�rer la zone visible du tableur
Res = GetClipBox&(Me.BlockOut.hDC, aRect)

'-> Comparer avec les dimensions de la cellule
If aCell.X1 < aRect.Left Then _
    Me.Horizontal.Value = Me.Horizontal.Value + (aRect.Left - aCell.X1)
If aCell.X2 > aRect.Right Then
    Me.Horizontal.Value = Me.Horizontal.Value - (aCell.X2 - aRect.Right)
    If Me.Vertical.Visible Then Me.Horizontal.Value = Me.Horizontal.Value - Me.Vertical.Width
End If

If aCell.Y1 < aRect.Top Then _
    Me.Vertical.Value = Me.Vertical.Value + (aRect.Top - aCell.Y1)
If aCell.Y2 > aRect.Bottom Then
    Me.Vertical.Value = Me.Vertical.Value - (aCell.Y2 - aRect.Bottom)
    If Me.Horizontal.Visible Then Me.Vertical.Value = Me.Vertical.Value - Me.Horizontal.Height
End If

End Sub

Private Sub Form_Resize()
    AutoScroll
End Sub

Private Function GetCellAdress(ByVal x As Integer, ByVal y As Integer) As POINTAPI

'---> Cette fonction retourne l'adresse de la cellule selon les coordorn�es pass�es

Dim Res As Long
Dim i  As Integer
Dim aBlock As Block


On Error Resume Next

'-> R�cup�rer un pointeur sur le block
SelectBlock aBlock

For i = 1 To aBlock.NbCol
    For j = 1 To aBlock.NbLigne
        Res = PtInRegion&(Cells(3, (j - 1) * aBlock.NbCol + i), x, y)
        If Res <> 0 Then
            GetCellAdress.x = Cells(1, (j - 1) * aBlock.NbCol + i) 'Colonne
            GetCellAdress.y = Cells(2, (j - 1) * aBlock.NbCol + i) 'Ligne
            Exit Function
        End If
    Next
Next
            
'-> Lib�rer le pointeur sur le block
Set aBlock = Nothing

            

End Function

Private Sub SelActiveCell(ByVal NewLig As Integer, ByVal NewCol As Integer)

Dim aCell As Cellule
Dim aBlock As Block

'-> Faire de la cellule la cellule active
SelectBlock aBlock

'-> Supprimer les cellules s�lectionn�es
InvertCell 1, aBlock.NbCol, 1, aBlock.NbLigne, 0, aBlock.NbCol

'-> Pointer sur l'ancienne cellule
Set aCell = aBlock.Cellules("L" & ActiveCell.y & "C" & ActiveCell.x)
aCell.IsActive = False
Set aCell = Nothing

'-> Rendre active la nouvelle cellule
Set aCell = aBlock.Cellules("L" & NewLig & "C" & NewCol)
aCell.IsActive = True

'-> Sp�cifier la nouvelle cellule de d�part
ActiveCell.y = NewLig
ActiveCell.x = NewCol

'-> Sp�cifier la nouvelle cellule de fin
CellFin.y = NewLig
CellFin.x = NewCol

'-> Afficher les informations dans la barre de formule
Me.BarreFormule.Text = aCell.Contenu

'-> Afficher l'adresse l� ou il faut
Me.Adresse.Caption = "L" & NewLig & "C" & NewCol

'-> Afficher les infos sur la cellule
DisplayCellProperties aCell

DisplayDimensions
SetBarreFormule
'-> Info couleur
Me.Picture2.ToolTipText = "Back color:" & aCell.BackColor & "   Font color:" & aCell.FontColor

'-> Redessiner le block
Me.BlockOut.Refresh

'-> Lib�rer les pointeurs
Set aBlock = Nothing
Set aCell = Nothing

End Sub

Private Sub InvertCell(ByVal ColMin As Integer, ByVal ColMax As Integer, _
                         ByVal LigMin As Integer, ByVal LigMax As Integer, _
                         ByVal ValueState As Integer, ByVal NbCol As Integer)

Dim i As Integer
Dim j As Integer
On Error Resume Next
'-> Appliquer les modifs
For i = ColMin To ColMax
    For j = LigMin To LigMax
        If Cells(6, (j - 1) * NbCol + i) <> ValueState Then
            If j = ActiveCell.y And i = ActiveCell.x Then
            '-> Ne jamais inverser la cellule active
            Else
                InvertRgn Me.BlockOut.hDC, Cells(3, (j - 1) * NbCol + i)
                Cells(6, (j - 1) * NbCol + i) = ValueState
            End If
        End If
    Next
Next

End Sub
Public Sub DetectCellSelected(ByVal LigCours As Integer, ByVal ColCours As Integer)

'---> Proc�dure qui analyse et met en invertion vid�o les cellules s�lectionn�es

Dim VarCol As Integer
Dim VarLig As Integer
Dim i As Integer, j As Integer
Dim ColMin As Integer, ColMax As Integer, LigMin As Integer, LigMax As Integer
Dim ValueState As Integer
Dim NbCol As Integer
Dim aRef As Integer
Dim aBlock As Block

'-> Calcul de la variation
VarCol = ColCours - CellFin.x
VarLig = LigCours - CellFin.y

'-> Ne rien faire si variation � 0
If VarCol = 0 And VarLig = 0 Then Exit Sub

'-> Pond�r� la variation
If ColCours <> ActiveCell.x Then
    If ColCours < ActiveCell.x Then VarCol = VarCol * -1
Else
    If CellFin.x < ActiveCell.x Then VarCol = VarCol * -1
End If

If LigCours <> ActiveCell.y Then
    If LigCours < ActiveCell.y Then VarLig = VarLig * -1
Else
    If CellFin.y < ActiveCell.y Then VarLig = VarLig * -1
End If

'-> R�cup�ration d'un pointeur sur le block
SelectBlock aBlock

'-> R�cup�ration du nombre de colonnes
NbCol = aBlock.NbCol

'-> Liberer le pointeur
Set aBlock = Nothing

'************************
'* Gestion des colonnes *
'************************

If VarCol = 0 Then
Else
    '-> D�termination de l'�tat voulu de la variation
    If ColCours > CellFin.x Then
        ColMin = CellFin.x
        ColMax = ColCours
    Else
        ColMin = ColCours
        ColMax = CellFin.x
    End If
    
    If VarCol < 0 Then
        '-> Etat de s�lection
        ValueState = 0
        aRef = CellFin.y
        '-> Enlever 1 si on est dans le sens de ->
        If ColCours > CellFin.x Then
            ColMax = ColMax - 1
        'ElseIf ColCours = ActiveCell.X Then
        Else
            ColMin = ColMin + 1
        End If
    Else
        '-> Etat de s�lection
        ValueState = 1
        aRef = LigCours
        '-> Ajouter 1 si on est dans le sens ->
        If ColCours > CellFin.x Then
            ColMin = ColMin + 1
        Else
            ColMax = ColMax - 1
        End If
    End If
        
    '-> D�termination des lignes � appliquer
    If aRef > ActiveCell.y Then
        LigMin = ActiveCell.y
        LigMax = aRef
    Else
        LigMin = aRef
        LigMax = ActiveCell.y
    End If
    
    '-> Appliquer les modifs
    Call InvertCell(ColMin, ColMax, LigMin, LigMax, ValueState, NbCol)
    
End If
    
'************************
'* Gestion des Lignes   *
'************************

If VarLig = 0 Then
Else
    '-> D�termination de l'�tat voulu de la variation
    If LigCours > CellFin.y Then
        LigMin = CellFin.y
        LigMax = LigCours
    Else
        LigMin = LigCours
        LigMax = CellFin.y
    End If
    
    If VarLig < 0 Then
        '-> Etat de s�lection
        ValueState = 0
        aRef = CellFin.x
        If LigCours > CellFin.y Then
            LigMax = LigMax - 1
        Else
            LigMin = LigMin + 1
        End If
    Else
        '-> Etat de s�lection
        ValueState = 1
        aRef = ColCours
        If LigCours > CellFin.y Then
            LigMin = LigMin + 1
        Else
            LigMax = LigMax - 1
        End If
    End If
    
    '-> D�termination des lignes � appliquer
    If aRef > ActiveCell.x Then
        ColMin = ActiveCell.x
        ColMax = aRef
    Else
        ColMin = aRef
        ColMax = ActiveCell.x
    End If
    
    '-> Appliquer les modifs
    Call InvertCell(ColMin, ColMax, LigMin, LigMax, ValueState, NbCol)
    
End If

'-> Modifier la cellule de fin
CellFin.x = ColCours
CellFin.y = LigCours

End Sub


Private Sub ApplicModifLargeur(BlockToModifie As Block, ByVal DifPix As Integer, ByVal ColClick As Integer)

Dim i As Integer, j As Integer
Dim aCell As Cellule
Dim hdlRgn As Long
Dim IsEdit As Boolean

'-> Modifier les largeurs
BlockToModifie.LargeurPix = BlockToModifie.LargeurPix + DifPix

'-> Appliquer les variations � la matrice Cells et aux objets Cellule
For i = ColClick To BlockToModifie.NbCol
    For j = 1 To BlockToModifie.NbLigne
        '---> Modification de la matrice Cells
        Set aCell = BlockToModifie.Cellules("L" & j & "C" & i)
        
        '-> Appliquer la modif au autres colonnes
        If i <> ColClick Then
            aCell.X1 = aCell.X1 + DifPix
            If BlockToModifie.IsEdit Then Cells(4, (j - 1) * BlockToModifie.NbCol + i) = Cells(4, (j - 1) * BlockToModifie.NbCol + i) + DifPix
        End If
        aCell.X2 = aCell.X2 + DifPix
        
        If BlockToModifie.IsEdit Then
            '-> Supprimer l'ancienne r�gion
            DeleteObject (j - 1) * BlockToModifie.NbCol + i
            '-> Cr�er la nouvelle r�gion
            hdlRgn = CreateRectRgn(aCell.X1, aCell.Y1, aCell.X2, aCell.Y2)
            '-> L'affecter � la matrice cell
            Cells(3, (j - 1) * BlockToModifie.NbCol + i) = hdlRgn
        End If
    Next
Next

If BlockToModifie.IsEdit Then
    '-> Modifier les repr�sentations physiques
    Me.BlockOut.Width = BlockToModifie.LargeurPix
    Me.EnteteColonne.Width = BlockToModifie.LargeurPix
    DisplayDimensions
End If

'-> lib�rer la ressource
Set aCell = Nothing





End Sub

Private Sub ModifHauteur(ByVal aHauteur As Single, ByVal Cm As Boolean, ByVal LigClick As Integer)

Dim HauteurLig As Single
Dim HauteurPix As Integer
Dim DifPix As Integer
Dim i As Integer
Dim LinkedBlock As Block
Dim aBlock As Block, aBlock2 As Block
Dim aTb As Tableau
Dim aForm As FrmBlock

'-> gestion des erreurs
On Error GoTo GestError

'-> Pointer sur le block
SelectBlock aBlock

'-> Pointeur sur le tableau
Set aTb = Maquettes("MAQUETTE").Tableaux(UCase$(Me.TableauCours))

'-> Convertir en pixel si necessaire
If Cm Then
    '-> Ancienne hauteur
    HauteurLig = aBlock.GetHauteurLig(LigClick)
    '-> Modifier la hauteur globale du block en cours.
    aBlock.Hauteur = aBlock.Hauteur - HauteurLig + CSng(aHauteur)
    '-> Modifier la hauteur de la ligne dans les propri�t�s
    aBlock.SetHauteurLig LigClick, CSng(aHauteur)
    '-> Calcul de la nouvelle hauteur en pixel
    HauteurPix = CInt(Me.ScaleY(aBlock.Hauteur, 7, 3))
    '-> R�cup�rer la variation de pixels
    DifPix = HauteurPix - Me.BlockOut.Height
Else
    '-> R�cup�rer la diff�rence
    DifPix = aHauteur
End If

'-> Modifier la hauteur de la ligne qui correspond � la cellule active
Lignes(LigClick) = Lignes(LigClick) + DifPix

'-> Appliquer les modifications au block en cours
ApplicModifHauteur aBlock, DifPix, LigClick

If aBlock.SlaveLink <> "" Then
    For i = 1 To NumEntries(aBlock.SlaveLink, "|")
        Set aBlock2 = aTb.Blocks("BL-" & UCase$(Entry(i, aBlock.SlaveLink, "|")))
        If aBlock2.IsEdit Then
            Set aForm = aTb.frmBlocks(UCase$(aBlock2.Nom))
            aForm.UpdateModifHauteur aHauteur, Cm, LigClick
            Set aForm = Nothing
        Else
            aBlock2.Hauteur = aBlock.Hauteur
            aBlock2.SetHauteurLig LigClick, CSng(aHauteur)
            ApplicModifHauteur aBlock2, DifPix, LigClick
        End If
        Set aBlock2 = Nothing
    Next
    Me.ZOrder
End If

'-> Liberer les pointeurs
Set LinkedBlock = Nothing
Set aBlock = Nothing
Set aBlock2 = Nothing
Set aForm = Nothing
Set aTb = Nothing

DisplayDimensions

'-> Redonner le focus au block
Me.BlockOut.SetFocus

Exit Sub

GestError:

MsgBox "Erreur de saisie"

End Sub

Private Sub ApplicModifHauteur(BlockToModifie As Block, ByVal DifPix As Integer, ByVal LigClick As Integer)

Dim i As Integer, j As Integer
Dim aCell As Cellule
Dim hdlRgn As Long
Dim IsEdit As Boolean

'-> Sauvegarder la hauteur
BlockToModifie.HauteurPix = BlockToModifie.HauteurPix + DifPix

'-> Appliquer les variations � la matrice Cells et aux objets Cellule
For i = 1 To BlockToModifie.NbCol
    For j = LigClick To BlockToModifie.NbLigne
        '-> Modification de la matrice Cells
        Set aCell = BlockToModifie.Cellules("L" & j & "C" & i)
        If j <> LigClick Then
            aCell.Y1 = aCell.Y1 + DifPix
            If BlockToModifie.IsEdit Then Cells(5, (j - 1) * BlockToModifie.NbCol + i) = Cells(5, (j - 1) * BlockToModifie.NbCol + i) + DifPix
        End If
        aCell.Y2 = aCell.Y2 + DifPix
        '-> Supprimer l'ancienne r�gion associ�e
        If BlockToModifie.IsEdit Then
            DeleteObject Cells(3, (j - 1) * BlockToModifie.NbCol + i)
            '-> Cr�er la nouvelle r�gion
            hdlRgn = CreateRectRgn(aCell.X1, aCell.Y1, aCell.X2, aCell.Y2)
            '-> Modifier dans la matrice des cellules
            Cells(3, (j - 1) * BlockToModifie.NbCol + i) = hdlRgn
        End If
    Next
Next

If BlockToModifie.IsEdit Then
    '-> Modifier les repr�sentations physiques
    Me.BlockOut.Height = BlockToModifie.HauteurPix
    Me.EnteteLigne.Height = BlockToModifie.HauteurPix
    DisplayDimensions
    '-> Forcer le redessin
    Me.BlockOut.Refresh
    Me.EnteteLigne.Refresh
End If

'-> lib�rer la ressource
Set aCell = Nothing

End Sub

Private Sub InsertCol(ByVal ClickCol As Integer)

'---> Cette proc�dure insert une colonne avant la colonne active
'-> La nouvelle colonne fait 2 cm de largeur par d�faut

Dim LargeurCm As Single
Dim LargeurPix As Integer
Dim DifPix As Integer
Dim i As Integer
Dim PosCol As Integer
Dim PosLigne() As Integer
Dim NbCol As Integer, NbLig As Integer
Dim aBlock As Block, aBlock2 As Block
Dim aTb As Tableau
Dim aForm As FrmBlock

'-> Obtenir un pointeur sur le block en cours
SelectBlock aBlock

'-> obtenir un pointeur sur le tableau
Set aTb = Maquettes("MAQUETTE").Tableaux(UCase$(Me.TableauCours))

'-> R�cup�rer les valeurs
NbCol = aBlock.NbCol 'Nb de colonne en cours
NbLig = aBlock.NbLigne 'Nb de ligne en cours
PosCol = Cells(4, ClickCol) 'Pos X de la colonne
ReDim PosLigne(1 To 2, 1 To NbLig) 'Init de la matrice des lignes
For i = 1 To NbLig 'R�cup�ration de la position de chaque ligne
    PosLigne(1, i) = aBlock.Cellules("L" & i & "C1").Y1
    PosLigne(2, i) = aBlock.Cellules("L" & i & "C1").Y2
Next

'-> Calcul de la nouvelle largeur en Cm
LargeurCm = aBlock.Largeur + DefLargeurCol
'-> Calcul de la diff�rence � appliquer en pixels au block de repr�sentation
DifPix = CInt(Me.ScaleX(LargeurCm, 7, 3)) - Me.BlockOut.Width

'-> Incr�menter de 1 le nombre de colonne
NbCol = NbCol + 1

'-> Appliquer les modifs au block en cours
AddModifInsertCol aBlock, LargeurCm, NbCol, NbLig, ClickCol, DifPix, PosLigne, PosCol

'-> Appliquer le format des cellules de la colonne de droite


'-> Appliquer les modifs pour les blocks li�s
If aBlock.SlaveLink <> "" Then
    For i = 1 To NumEntries(aBlock.SlaveLink, "|")
        Set aBlock2 = aTb.Blocks("BL-" & UCase$(Entry(i, aBlock.SlaveLink, "|")))
        If aBlock2.IsEdit Then
            Set aForm = aTb.frmBlocks(UCase$(aBlock2.Nom))
            aForm.UpdateInsertCol ClickCol
            Set aForm = Nothing
        Else
            AddModifInsertCol aBlock2, LargeurCm, NbCol, NbLig, ClickCol, DifPix, PosLigne, PosCol
        End If
        Set aBlock2 = Nothing
    Next
    Me.ZOrder
End If

'-> Lib�ter le pointeur sur le block
Set aBlock = Nothing
Set aBlock2 = Nothing
Set aTb = Nothing
Set aForm = Nothing

DisplayDimensions

End Sub

Private Sub AddModifInsertCol(aBlock As Block, ByVal LargeurCm As Single, _
                                ByVal NbCol As Integer, ByVal NbLig As Integer, ByVal IdCol As Integer, _
                                ByVal DifPix As Integer, ByRef PosLigne() As Integer, _
                                ByVal PosCol As Integer)

'---> Cette fonction applique les modifications � l'objet Block lors de l'insertion _
d'une colonne dans le block

Dim i As Integer
Dim aRect As Rect, aRect1 As Rect, aRect2 As Rect
Dim Res As Long
Dim aCell As Cellule
Dim aRefCell As Cellule

'-> Largeur en cm du block de ligne
aBlock.Largeur = LargeurCm
'-> Nombre de colonne associ�e au block
aBlock.NbCol = NbCol
'-> Reconstruire la matrice interne pour ajouter une nouvelle entr�e dans les colonnes
aBlock.Init
'-> Effectuer un glissement du contenu de la matrice des colonnes du block en cours
For i = NbCol To IdCol + 1 Step -1
    aBlock.SetLargeurCol i, aBlock.GetLargeurCol(i - 1)
Next
'-> Nouvelle Largeur de la colonne active
aBlock.SetLargeurCol IdCol, DefLargeurCol

'-> Si le block est en cours d'�dition , redimensionner la matrice des colonnes de la feuille
'-> Initialiser la matrice des colonnes pour ajouter la nouvelle entr�e
If aBlock.IsEdit Then
    InitMatriceCol NbCol, True
    '-> Faire un glissement du contenu de la matrice colonnes
    For i = NbCol To IdCol + 1 Step -1
        Colonnes(i) = Colonnes(i - 1)
    Next
    '-> Setting de la matrcide des colonnes pour la colonne en cours
    Colonnes(IdCol) = DifPix - 1
    '-> Augmenter la taille des PictureBox associ�s
    Me.BlockOut.Width = BlockOut.Width + DifPix
    Me.EnteteColonne.Width = EnteteColonne.Width + DifPix
    '-> R�gion � faire d�filer
    aRect.Left = Cells(4, IdCol)
    aRect.Top = Cells(5, IdCol)
    aRect.Right = BlockOut.Width
    aRect.Bottom = Cells(5, (NbCol - 1) * NbLig) + Lignes(NbLig) + 1
    '-> Region de clipping
    Res = GetClientRect(BlockOut.Hwnd, aRect1)
    '-> Scroll du DC
    Res = ScrollDC(Me.BlockOut.hDC, DifPix, 0, aRect, aRect1, 0, aRect2)
    '-> Supprimer les objets GDI qui correspondent aux anciennes r�gions
    For i = 1 To (NbCol - 1) * NbLig
       Res = DeleteObject(Cells(3, i))
    Next
    '-> Ecraser la matrice des cellules Cells en la redimensionnant
    ReDim Cells(1 To 6, (NbCol * NbLig))
End If

'---> Effectuer un glissement des cellules de l'objet block en cours
For i = NbCol To 1 Step -1 'pour toutes les colonnes
    For j = NbLig To 1 Step -1
        If i > IdCol Then
            '-> Pointer sur l'ancienne adresse
            Set aCell = aBlock.Cellules("L" & j & "C" & i - 1)
            '-> Modifier les positions
            aCell.X1 = aCell.X1 + DifPix
            aCell.X2 = aCell.X2 + DifPix
            '-> D�caler la colonne associ�e
            aCell.Colonne = aCell.Colonne + 1
            '-> L'enregister avec la nouvelle adresse
            aBlock.Cellules.Add aCell, "L" & j & "C" & i
            Set aCell = aBlock.Cellules("L" & j & "C" & i)
            '-> Supprimer l'ancienne r�f�rence
            aBlock.Cellules.Remove ("L" & j & "C" & i - 1)
        ElseIf i = IdCol Then
            '-> Modifier les positions
            Set aCell = AddCell(j, i, True, PosCol, PosLigne(1, j), PosCol + DifPix, PosLigne(2, j))
'            '-> Gestion des bordures
            If IdCol = 1 Then
                aCell.BordureGauche = False
            Else
                aCell.BordureGauche = GetBordureGauche(aBlock, j, i)
            End If
            aCell.BordureDroite = GetBordureDroite(aBlock, j, i)
            '-> Ajouter la cellule dans la collection
            aBlock.Cellules.Add aCell, "L" & j & "C" & i
            '-> Pointer sur la cellule situ�e � droite
            Set aRefCell = aBlock.Cellules("L" & j & "C" & i + 1)
            '-> Modifier ses propri�t�s en appliquant les prorpi�t�s de la cellules situ�e � droite
            aCell.AutoAjust = aRefCell.AutoAjust
            aCell.BackColor = aRefCell.BackColor
            aCell.BordureHaut = aRefCell.BordureHaut
            aCell.BordureBas = aRefCell.BordureBas
            aCell.CellAlign = aRefCell.CellAlign
            aCell.FondTransParent = aRefCell.FondTransParent
            aCell.FontBold = aRefCell.FontBold
            aCell.FontColor = aRefCell.FontColor
            aCell.FontItalic = aRefCell.FontItalic
            aCell.FontName = aRefCell.FontName
            aCell.FontSize = aRefCell.FontSize
            aCell.FontUnderline = aRefCell.FontUnderline

            '-> Lib�rer le pointeur sur la cellule
            Set aRefCell = Nothing
            
        End If
        '-> Pointer sur la cellule
        Set aCell = aBlock.Cellules("L" & j & "C" & i)
        '-> Recr�er la matrice Cells si le block est en cours d'�dition
        Cells(1, (j - 1) * NbCol + i) = i
        Cells(2, (j - 1) * NbCol + i) = j
        Cells(3, (j - 1) * NbCol + i) = CreateRectRgn(aCell.X1, aCell.Y1, aCell.X2, aCell.Y2)
        Cells(4, (j - 1) * NbCol + i) = aCell.X1
        Cells(5, (j - 1) * NbCol + i) = aCell.Y1
        '-> Gestion de la cellule active
        If j <> ActiveCell.y And i = ActiveCell.x Then
            Cells(6, (j - 1) * NbCol + i) = 1
        Else
            Cells(6, (j - 1) * NbCol + i) = 0
        End If
    Next 'Pour toutes les lignes
Next 'pour toutes les colonnes
    
'-> Modifier la largeur
aBlock.LargeurPix = Me.BlockOut.Width

If aBlock.IsEdit Then

    '-> Pointer sur l'ancienne cellule active pour la rendre inactive
    Set aCell = aBlock.Cellules("L" & ActiveCell.y & "C" & ActiveCell.x + 1)
    aCell.IsActive = False
    Set aCell = aBlock.Cellules("L" & ActiveCell.y & "C" & ActiveCell.x)
    aCell.IsActive = True
    Set aCell = Nothing

    '-> Redonner le focus au block
    Me.BlockOut.SetFocus
    Me.BlockOut.Refresh

End If

'-> Lib�rer le pointeur sur le block
Set aCell = Nothing

End Sub

Private Sub InitBlock()

'---> Cette proc�dure initialise les matrices Lignes (), Colonnes () et Cells _
en partant des largeurs de colonnes et hauteur de lignes

Dim i As Integer
Dim j As Integer
Dim NbCol As Integer
Dim NbLig As Integer
'-> Dimensions en CM
Dim HauteurCm As Single
Dim LargeurCm As Single
'-> Convertion en Pixels pour dessin
Dim HauteurPix As Integer
Dim LargeurPix As Integer
Dim hPix As Integer
Dim lPix As Integer
Dim aBlock As Block

On Error Resume Next

'-> R�cup�ration d'un pointeur sur le block
SelectBlock aBlock

'-> R�cup�ration des propri�t�s
NbCol = aBlock.NbCol
NbLig = aBlock.NbLigne
HauteurCm = aBlock.Hauteur
LargeurCm = aBlock.Largeur

'-> Convertion en pixels
LargeurPix = CInt(Me.ScaleX(LargeurCm, 7, 3))
HauteurPix = CInt(Me.ScaleY(HauteurCm, 7, 3))

'-> Setting du PictureBox de dessin
Me.BlockOut.Width = LargeurPix
Me.BlockOut.Height = HauteurPix

Me.Height = Me.ScaleY(Me.BlockOut.Height + Me.EnteteColonne.Height + Me.BarreFormule.Height, 3, 1) + Me.ScaleY(2, 7, 1)
Me.Width = Me.ScaleX(Me.EnteteLigne.Width + Me.BlockOut.Width, 3, 1) + Me.ScaleX(1, 7, 1)

'-> Setting des dimensions
aBlock.LargeurPix = LargeurPix
aBlock.HauteurPix = HauteurPix

'-> Setting des Entete colonnes
Me.EnteteColonne.Width = LargeurPix
Me.EnteteLigne.Height = HauteurPix

'-> Calcul des dimensions de base
ReDim Lignes(1 To NbLig)
ReDim Colonnes(1 To NbCol)

'-> Largeur de base des colonnes
For i = 1 To NbCol
    Colonnes(i) = Fix((LargeurPix - (NbCol + 1)) / NbCol)
    lPix = lPix + Colonnes(i)
Next

'-> Hauteur de base des lignes
For i = 1 To NbLig
    Lignes(i) = Fix((HauteurPix - (NbLig + 1)) / NbLig)
    hPix = hPix + Lignes(i)
Next

'-> Effectuer le lissage des colonnes
For i = 1 To (LargeurPix - lPix - (NbCol + 1))
    Colonnes(i) = Colonnes(i) + 1
Next

'-> Effectuer le lissage des lignes
For i = 1 To (HauteurPix - hPix - (NbLig + 1))
    Lignes(i) = Lignes(i) + 1
Next

'-> Redimensionner la matrice des cellules
ReDim Cells(1 To 6, 1 To (NbLig * NbCol))

'-> Liberer le pointeur sur le block
Set aBlock = Nothing


End Sub
Private Sub InitMatriceCol(ByVal NbCol As Integer, ByVal ToPreserve As Boolean)

If ToPreserve Then
    ReDim Preserve Colonnes(1 To NbCol)
Else
    ReDim Colonnes(1 To NbCol)
End If
    

End Sub

Public Sub InitCells(ByVal Base1 As Integer, ByVal Base2 As Integer)

'---> Cette proc�dure permet de r�initialiser la matrice cells depuis une autre feuille

ReDim Cells(1 To Base1, 1 To Base2)

End Sub

Private Sub InsertLig(ByVal ClickLig As Integer)

Dim HauteurCm As Single
Dim HauteurPix As Integer
Dim DifPix As Integer
Dim i As Integer
Dim NbCol As Integer, NbLig As Integer
Dim PosLig As Integer
Dim PosColonne() As Integer
Dim aBlock As Block, aBlock2 As Block
Dim aForm As FrmBlock
Dim aTb As Tableau

'-> Obtenir un pointeur sur le block
SelectBlock aBlock

'-> Obtenir un pointeur sur le tableau
Set aTb = Maquettes("MAQUETTE").Tableaux(UCase$(Me.TableauCours))

'-> R�cup�ration des valeurs
NbCol = aBlock.NbCol 'Nombre de colonne
NbLig = aBlock.NbLigne 'Nombre de lignes
PosLig = Cells(5, (ClickLig - 1) * NbCol + ActiveCell.x) 'Position y de la aligne
ReDim PosColonne(1 To 2, 1 To NbCol) 'Initialisation de la matrice des colonnes
'-> R�cup�ration des positions des colonnes
For i = 1 To NbCol 'R�cup�ration de la position de chaque colonne
    PosColonne(1, i) = aBlock.Cellules("L1" & "C" & i).X1
    PosColonne(2, i) = aBlock.Cellules("L1" & "C" & i).X2
Next

'-> Nelle Hauteur Cm
HauteurCm = aBlock.Hauteur + DefHauteurLig
'-> Dif en Pix
DifPix = CInt(Me.ScaleY(HauteurCm, 7, 3)) - Me.BlockOut.Height
'-> Incr�menter le nombre de ligne
NbLig = NbLig + 1

'-> Appliquer les modifs au block en cours
AddModifInsertLig aBlock, HauteurCm, NbCol, NbLig, ClickLig, DifPix, PosColonne, PosLig

If aBlock.SlaveLink <> "" Then
    For i = 1 To NumEntries(aBlock.SlaveLink, "|")
        Set aBlock2 = aTb.Blocks("BL-" & UCase$(Entry(i, aBlock.SlaveLink, "|")))
        If aBlock2.IsEdit Then
            Set aForm = aTb.frmBlocks(UCase$(aBlock2.Nom))
            aForm.UpdateInsertLig ClickLig
            Set aForm = Nothing
        Else
            AddModifInsertLig aBlock2, HauteurCm, NbCol, NbLig, ClickLig, DifPix, PosColonne, PosLig
        End If
        Set aBlock2 = Nothing
    Next
    Me.ZOrder
End If

Set aTb = Nothing
Set aBlock2 = Nothing
Set aBlock = Nothing
Set aForm = Nothing

DisplayDimensions

End Sub

Private Sub InsertLigBas(ByVal ClickLig As Integer)

Dim HauteurCm As Single
Dim HauteurPix As Integer
Dim DifPix As Integer
Dim i As Integer
Dim NbCol As Integer, NbLig As Integer
Dim PosLig As Integer
Dim PosColonne() As Integer
Dim aBlock As Block, aBlock2 As Block
Dim aForm As FrmBlock
Dim aTb As Tableau

'-> Obtenir un pointeur sur le block
SelectBlock aBlock

'-> Obtenir un pointeur sur le tableau
Set aTb = Maquettes("MAQUETTE").Tableaux(UCase$(Me.TableauCours))

'-> R�cup�ration des valeurs
NbCol = aBlock.NbCol 'Nombre de colonne
NbLig = aBlock.NbLigne 'Nombre de lignes
PosLig = Cells(5, (ClickLig - 1) * NbCol + ActiveCell.x) 'Position y de la ligne
ReDim PosColonne(1 To 2, 1 To NbCol) 'Initialisation de la matrice des colonnes
'-> R�cup�ration des positions des colonnes
For i = 1 To NbCol 'R�cup�ration de la position de chaque colonne
    PosColonne(1, i) = aBlock.Cellules("L1" & "C" & i).X1
    PosColonne(2, i) = aBlock.Cellules("L1" & "C" & i).X2
Next

'-> Nelle Hauteur Cm
HauteurCm = aBlock.Hauteur + DefHauteurLig
'-> Dif en Pix
DifPix = CInt(Me.ScaleY(HauteurCm, 7, 3)) - Me.BlockOut.Height
'-> Incr�menter le nombre de ligne
NbLig = NbLig + 1

'-> Appliquer les modifs au block en cours
AddModifInsertLigBas aBlock, HauteurCm, NbCol, NbLig, ClickLig, DifPix, PosColonne, PosLig

If aBlock.SlaveLink <> "" Then
    For i = 1 To NumEntries(aBlock.SlaveLink, "|")
        Set aBlock2 = aTb.Blocks("BL-" & UCase$(Entry(i, aBlock.SlaveLink, "|")))
        If aBlock2.IsEdit Then
            Set aForm = aTb.frmBlocks(UCase$(aBlock2.Nom))
            aForm.UpdateInsertLigBas ClickLig
            Set aForm = Nothing
        Else
            AddModifInsertLigBas aBlock2, HauteurCm, NbCol, NbLig, ClickLig, DifPix, PosColonne, PosLig
        End If
        Set aBlock2 = Nothing
    Next
    Me.ZOrder
End If

Set aTb = Nothing
Set aBlock2 = Nothing
Set aBlock = Nothing
Set aForm = Nothing

DisplayDimensions

End Sub

Private Sub AddModifInsertLig(aBlock As Block, ByVal HauteurCm As Single, _
                               ByVal NbCol As Integer, ByVal NbLig As Integer, _
                               ByVal IdLig As Integer, ByVal DifPix As Integer, _
                               ByRef PosColonne() As Integer, ByVal PosLig As Integer)

Dim i As Integer
Dim aRect As Rect, aRect1 As Rect, aRect2 As Rect
Dim Res As Long
Dim aCell As Cellule

'-> Modifications
aBlock.Hauteur = HauteurCm
aBlock.NbLigne = NbLig
'-> Ajout d'une entr�e dans la marice des lignes de l'objet block en cours
aBlock.Init
'-> Glisser pour les valeurs pour les deux matrices
For i = NbLig To IdLig + 1 Step -1
    aBlock.SetHauteurLig i, aBlock.GetHauteurLig(i - 1)
Next
'-> Nouvelle hauteur de la ligne active
aBlock.SetHauteurLig IdLig, DefHauteurLig

'-> Si le block est en cours d'�dition, redimensionner la matrice des lignes de la _
feuille en cours
If aBlock.IsEdit Then
    '-> Matrice des lignes
    InitMatriceLig NbLig, True
    '-> Faire un glissement du contenu de la matrice des lignes
    For i = NbLig To IdLig + 1 Step -1
        Lignes(i) = Lignes(i - 1)
    Next
    '-> Setting de la matrice des lignes pour la dimensions en cours
    Lignes(IdLig) = DifPix - 1
    '-> Augmenter la taille des pictures box
    Me.BlockOut.Height = Me.BlockOut.Height + DifPix
    Me.EnteteLigne.Height = Me.EnteteLigne.Height + DifPix
    '-> R�gion � faire d�filer
    aRect.Left = Cells(4, (IdLig - 1) * NbCol + 1)
    aRect.Top = Cells(5, (IdLig - 1) * NbCol + 1)
    aRect.Right = Cells(4, (NbLig - 1) * NbCol) + Colonnes(NbCol) + 1
    aRect.Bottom = Cells(5, (NbLig - 1) * NbCol) + Lignes(NbLig - 1) + 1
    '-> R�gion de clipping
    Res = GetClientRect(Me.BlockOut.Hwnd, aRect1)
    '-> Scroll du DC
    Res = ScrollDC(Me.BlockOut.hDC, 0, DifPix, aRect, aRect1, 0, aRect2)
    '-> Ecraser la matrice des cellules ne la redimensionnant
    For i = 1 To NbCol * (NbLig - 1)
        DeleteObject Cells(3, i)
    Next
    '-> R�initialiser la matrice des objets GDI Cells
    ReDim Cells(6, (NbLig * NbCol))
End If

'-> Effectuer les glissements de l'objets block en cours
'-> Faire un glissement des cellules de l'objet Block en cours
For i = 1 To NbCol
    For j = NbLig To 1 Step -1
        If j > IdLig Then
            '-> Pointer sur la cellule pr�c�dente
            Set aCell = aBlock.Cellules("L" & j - 1 & "C" & i)
            '-> Modifier les positions
            aCell.Y1 = aCell.Y1 + DifPix
            aCell.Y2 = aCell.Y2 + DifPix
            '-> D�caler la ligne associ�e
            aCell.Ligne = aCell.Ligne + 1
            '-> Enregistrer � la nouvelle adresse
            aBlock.Cellules.Add aCell, "L" & j & "C" & i
            '-> Supprimer l'ancienne r�f�rence
            aBlock.Cellules.Remove ("L" & j - 1 & "C" & i)
        ElseIf j = IdLig Then
            '-> Modifier les positions
            Set aCell = AddCell(j, i, True, PosColonne(1, i), PosLig, PosColonne(2, i), PosLig + DifPix)
            '-> Gestion des bordures
            aCell.BordureHaut = GetBordureHaut(aBlock, j, i)
            aCell.BordureBas = GetBordureBas(aBlock, j, i)
            '-> Ajouter la cellule dans la collection
            aBlock.Cellules.Add aCell, "L" & j & "C" & i
        End If
        
        '-> Pointer sur la nouvelle adresse
        Set aCell = aBlock.Cellules("L" & j & "C" & i)
        
        '-> Recr�er la matrice Cells
        Cells(1, (j - 1) * NbCol + i) = i
        Cells(2, (j - 1) * NbCol + i) = j
        Cells(3, (j - 1) * NbCol + i) = CreateRectRgn(aCell.X1, aCell.Y1, aCell.X2, aCell.Y2)
        Cells(4, (j - 1) * NbCol + i) = aCell.X1
        Cells(5, (j - 1) * NbCol + i) = aCell.Y1
        '-> Gestion de la cellule active
        If j = ActiveCell.y And i <> ActiveCell.x Then
            Cells(6, (j - 1) * NbCol + i) = 1
        Else
            Cells(6, (j - 1) * NbCol + i) = 0
        End If
        
    Next 'Pour toutes les lignes
Next 'pour toutes les colonnes

'-> Modifier la hauteur
aBlock.HauteurPix = Me.BlockOut.Height

If aBlock.IsEdit Then
    '-> Pointer sur l'ancienne cellule active pour la rendre inactive
    Set aCell = aBlock.Cellules("L" & ActiveCell.y + 1 & "C" & ActiveCell.x)
    aCell.IsActive = False
    Set aCell = aBlock.Cellules("L" & ActiveCell.y & "C" & ActiveCell.x)
    aCell.IsActive = True
    Set aCell = Nothing

    '-> Redonner le focus au block
    Me.BlockOut.SetFocus
    Me.BlockOut.Refresh

End If

Set aCell = Nothing

End Sub

Private Sub AddModifInsertLigBas(ByVal aBlock As Block, ByVal HauteurCm As Single, _
                               ByVal NbCol As Integer, ByVal NbLig As Integer, _
                               ByVal IdLig As Integer, ByVal DifPix As Integer, _
                               ByRef PosColonne() As Integer, ByVal PosLig As Integer)

Dim i As Integer
Dim aRect As Rect, aRect1 As Rect, aRect2 As Rect
Dim Res As Long
Dim aCell As Cellule

'-> Modifications
aBlock.Hauteur = HauteurCm
aBlock.NbLigne = NbLig
'-> Ajout d'une entr�e dans la marice des lignes de l'objet block en cours
aBlock.Init
'-> Glisser pour les valeurs pour les deux matrices
For i = NbLig To IdLig + 2 Step -1
    aBlock.SetHauteurLig i, aBlock.GetHauteurLig(i - 1)
Next
'-> Nouvelle hauteur de la ligne active
aBlock.SetHauteurLig IdLig + 1, DefHauteurLig

'-> Si le block est en cours d'�dition, redimensionner la matrice des lignes de la _
feuille en cours
If aBlock.IsEdit Then
    '-> Matrice des lignes
    InitMatriceLig NbLig, True
    '-> Faire un glissement du contenu de la matrice des lignes
    For i = NbLig To IdLig + 2 Step -1
        Lignes(i) = Lignes(i - 1)
    Next
    '-> Setting de la matrice des lignes pour la dimensions en cours
    Lignes(IdLig + 1) = DifPix - 1
    '-> Augmenter la taille des pictures box
    Me.BlockOut.Height = Me.BlockOut.Height + DifPix
    Me.EnteteLigne.Height = Me.EnteteLigne.Height + DifPix
    '-> R�gion � faire d�filer
    '-> selon que l'on est sur la derniere ligne
    If IdLig + 1 = NbLig Then
        aRect.Left = Cells(4, (IdLig - 1) * NbCol + 1)
        aRect.Top = Cells(5, (IdLig - 1) * NbCol) + Lignes(NbLig - 1) + 1
        aRect.Right = Cells(4, (NbLig - 1) * NbCol) + Colonnes(NbCol) + 1
        aRect.Bottom = Cells(5, (NbLig - 1) * NbCol) + Lignes(NbLig - 1) + 1
    Else
        aRect.Left = Cells(4, (IdLig) * NbCol + 1)
        aRect.Top = Cells(5, (IdLig) * NbCol + 1)
        aRect.Right = Cells(4, (NbLig - 1) * NbCol) + Colonnes(NbCol) + 1
        aRect.Bottom = Cells(5, (NbLig - 1) * NbCol) + Lignes(NbLig - 1) + 1
    End If
    '-> R�gion de clipping
    Res = GetClientRect(Me.BlockOut.Hwnd, aRect1)
    '-> Scroll du DC
    Res = ScrollDC(Me.BlockOut.hDC, 0, DifPix, aRect, aRect1, 0, aRect2)
    '-> Ecraser la matrice des cellules en la redimensionnant
    For i = 1 To NbCol * (NbLig - 1)
        DeleteObject Cells(3, i)
    Next
    '-> R�initialiser la matrice des objets GDI Cells
    ReDim Cells(6, (NbLig * NbCol))
End If

'-> Effectuer les glissements de l'objets block en cours
'-> Faire un glissement des cellules de l'objet Block en cours
For i = 1 To NbCol
    For j = NbLig To 1 Step -1
        If j > IdLig + 1 Then
            '-> Pointer sur la cellule pr�c�dente
            Set aCell = aBlock.Cellules("L" & j - 1 & "C" & i)
            '-> Modifier les positions
            aCell.Y1 = aCell.Y1 + DifPix
            aCell.Y2 = aCell.Y2 + DifPix
            '-> D�caler la ligne associ�e
            aCell.Ligne = aCell.Ligne + 1
            '-> Enregistrer � la nouvelle adresse
            aBlock.Cellules.Add aCell, "L" & j & "C" & i
            '-> Supprimer l'ancienne r�f�rence
            aBlock.Cellules.Remove ("L" & j - 1 & "C" & i)
        ElseIf j = IdLig + 1 Then
            '-> Modifier les positions
            Set aCell = AddCell(j, i, True, PosColonne(1, i), PosLig + Lignes(IdLig), PosColonne(2, i), PosLig + Lignes(IdLig) + DifPix)
            '-> Gestion des bordures
            aCell.BordureHaut = GetBordureHaut(aBlock, j, i)
            aCell.BordureBas = GetBordureBas(aBlock, j, i)
            '-> Ajouter la cellule dans la collection
            aBlock.Cellules.Add aCell, "L" & j & "C" & i
        End If
        
        '-> Pointer sur la nouvelle adresse
        Set aCell = aBlock.Cellules("L" & j & "C" & i)
        
        '-> Recr�er la matrice Cells
        Cells(1, (j - 1) * NbCol + i) = i
        Cells(2, (j - 1) * NbCol + i) = j
        Cells(3, (j - 1) * NbCol + i) = CreateRectRgn(aCell.X1, aCell.Y1, aCell.X2, aCell.Y2)
        Cells(4, (j - 1) * NbCol + i) = aCell.X1
        Cells(5, (j - 1) * NbCol + i) = aCell.Y1
        '-> Gestion de la cellule active
        If j = ActiveCell.y + 1 And i = ActiveCell.x Then
            Cells(6, (j - 1) * NbCol + i) = 1
        Else
            Cells(6, (j - 1) * NbCol + i) = 0
        End If
        
    Next 'Pour toutes les lignes
Next 'pour toutes les colonnes

'-> Modifier la hauteur
aBlock.HauteurPix = Me.BlockOut.Height

If aBlock.IsEdit Then
    '-> Pointer sur l'ancienne cellule active pour la rendre inactive
    Set aCell = aBlock.Cellules("L" & ActiveCell.y & "C" & ActiveCell.x)
    aCell.IsActive = False
    Set aCell = aBlock.Cellules("L" & ActiveCell.y & "C" & ActiveCell.x)
    aCell.IsActive = True
    Set aCell = Nothing

    '-> Redonner le focus au block
    Me.BlockOut.SetFocus
    Me.BlockOut.Refresh
    
End If

Set aCell = Nothing

End Sub


Private Sub InitMatriceLig(ByVal NbLig As Integer, ByVal ToPreserve As Boolean)

'---> Cette proc�dure permet de reinitialiser la matrice lignes depuis une _
autre feuille

If ToPreserve Then
    ReDim Preserve Lignes(1 To NbLig)
Else
    ReDim Lignes(1 To NbLig)
End If


End Sub


Private Sub DelContenu(ByVal LigMin As Integer, ByVal LigMax As Integer, _
                       ByVal ColMin As Integer, ByVal ColMax As Integer, _
                       ByRef aBlock As Block)



Dim i As Integer, j As Integer
Dim aCell As Cellule

For i = ColMin To ColMax
    For j = LigMin To LigMax
        Set aCell = aBlock.Cellules("L" & j & "C" & i)
        aCell.Contenu = ""
        'Cells(6, (i - 1) * Me.BlockCours.NbCol + ActiveCell.X) = 0
    Next
Next

'Call InvertCell(ActiveCell.X, ActiveCell.X, 2, Me.BlockCours.NbLigne, 1, Me.BlockCours.NbCol)

Me.BlockOut.Refresh

'-> Donner le focus au block
Me.BlockOut.SetFocus

Set aCell = Nothing

End Sub

Private Sub FormatCell()

Dim aBlock As Block

'-> Obtenir un pointeur sur le block
SelectBlock aBlock

'-> Setting des propri�tes de frmFormatCell
frmFormatCell.Action = 2
Set frmFormatCell.BlockCours = aBlock
If ActiveCell.x < CellFin.x Then
    frmFormatCell.ColMin = ActiveCell.x
    frmFormatCell.ColMax = CellFin.x
Else
    frmFormatCell.ColMin = CellFin.x
    frmFormatCell.ColMax = ActiveCell.x
End If
If ActiveCell.y < CellFin.y Then
    frmFormatCell.LigMin = ActiveCell.y
    frmFormatCell.LigMax = CellFin.y
Else
    frmFormatCell.LigMin = CellFin.y
    frmFormatCell.LigMax = ActiveCell.y
End If

'-> Setting de la cellule active
Set frmFormatCell.aCell = aBlock.Cellules("L" & ActiveCell.y & "C" & ActiveCell.x)
'-> Afficher la feuille
frmFormatCell.Show vbModal

'-> Liberer le pointeur du block
Set aBlock = Nothing

'-> Redonner le focus au block
Me.BlockOut.SetFocus
Me.BlockOut.Refresh


End Sub

Private Sub DelCol(ByVal ColClick As Integer)

'---> Fonction qui supprimer la colonne
Dim Rep

Dim LargeurCm As Single
Dim LargeurPix As Integer
Dim DifPix As Integer
Dim i As Integer
Dim NbCol As Integer, NbLig As Integer
Dim aCell As Cellule
Dim PosCol As Integer
Dim PosLigne() As Integer
Dim aCol As Integer
Dim aBlock As Block, aBlock2 As Block
Dim aTb As Tableau
Dim aForm As FrmBlock

'-> Obtenir un pointeur sur le block
SelectBlock aBlock

'-> Pointeur sur le tableau
Set aTb = Maquettes("MAQUETTE").Tableaux(UCase$(Me.TableauCours))

'-> R�cup�rer les valeurs
NbCol = aBlock.NbCol 'Nombre de colonnes
NbLig = aBlock.NbLigne 'Nombre de lignes
PosCol = Cells(4, ColClick) 'PosX de la colonne
ReDim PosLigne(1 To 2, 1 To NbLig)
For i = 1 To NbLig 'R�cup�ration de la position des lignes
    PosLigne(1, i) = aBlock.Cellules("L" & i & "C1").Y1
    PosLigne(2, i) = aBlock.Cellules("L" & i & "C1").Y2
Next

'-> Largeur en Cm
LargeurCm = aBlock.Largeur - aBlock.GetLargeurCol(ColClick)
'-> Largeur en Pixels
Set aCell = aBlock.Cellules("L1" & "C" & ColClick)
DifPix = aCell.X1 - aCell.X2
Set aCell = Nothing
NbCol = NbCol - 1

'-> Appliquer les modifications au block en cours
ApplicModifDelCol aBlock, LargeurCm, NbCol, NbLig, ColClick, DifPix, PosLigne, PosCol

'-> Appliquer les modifs pour les blocks li�s
If aBlock.SlaveLink <> "" Then
    For i = 1 To NumEntries(aBlock.SlaveLink, "|")
        Set aBlock2 = aTb.Blocks("BL-" & UCase$(Entry(i, aBlock.SlaveLink, "|")))
        If aBlock2.IsEdit Then
            Set aForm = aTb.frmBlocks(UCase$(aBlock2.Nom))
            aForm.UpdateDelCol ColClick
            Set aForm = Nothing
        Else
            ApplicModifDelCol aBlock2, LargeurCm, NbCol, NbLig, ColClick, DifPix, PosLigne, PosCol
        End If
        Set aBlock2 = Nothing
    Next
    Me.ZOrder
End If

Set aTb = Nothing
Set aCell = Nothing
Set aBlock = Nothing
Set aForm = Nothing

DisplayDimensions

End Sub
Private Sub ApplicModifDelCol(aBlock As Block, ByVal LargeurCm As Single, _
                             ByVal NbCol As Integer, ByVal NbLig As Integer, ByVal IdCol As Integer, _
                             ByVal DifPix As Integer, ByRef PosLigne() As Integer, _
                             ByVal PosCol As Integer)


Dim i As Integer, j As Integer, k As Integer
Dim aRect As Rect, aRect1 As Rect, aRect2 As Rect
Dim Res As Long
Dim aCell As Cellule
Dim aCol As Integer

'-> Appliquer les modifications
aBlock.Largeur = LargeurCm
aBlock.LargeurPix = aBlock.LargeurPix + DifPix
aBlock.NbCol = NbCol

'-> Faire glisser les valeurs
For i = IdCol To NbCol
    aBlock.SetLargeurCol i, aBlock.GetLargeurCol(i + 1)
Next

'-> Redimension des matrices
aBlock.Init

If aBlock.IsEdit Then

    '-> Faire glisser les valeurs
    For i = IdCol To NbCol
        Colonnes(i) = Colonnes(i + 1)
    Next
    '-> R�gion � faire d�filer : Attention, si derni�re colonne pas de r�gion � faire d�filer
    If IdCol = NbCol + 1 Then
    Else
        aRect.Top = 0
        aRect.Left = Cells(4, IdCol + 1) - 1
        aRect.Right = Me.BlockOut.Width
        aRect.Bottom = Cells(5, (NbCol + 1) * NbLig) + Lignes(NbLig) + 1
        '-> R�cup�rer la zone cliente du block de donn�es
        Res = GetClientRect(Me.BlockOut.Hwnd, aRect1)
        aRect1.Right = aRect1.Right
        '-> Scroll du Dc
        Res = ScrollDC(Me.BlockOut.hDC, DifPix, 0, aRect, aRect1, 0, aRect2)
    End If
    '-> Diminuer la largeur des pictureBox
    Me.BlockOut.Width = Me.BlockOut.Width + DifPix
    Me.EnteteColonne.Width = Me.EnteteColonne.Width + DifPix
    '-> Ablock.init
    InitMatriceCol NbCol, True
    '-> Supprimer les diff�rentes r�gions
    For i = 1 To (NbCol + 1) * NbLig
        DeleteObject Cells(3, i)
    Next
    '-> Ecraser la matrice
    ReDim Cells(6, NbCol * NbLig)
End If

'-> Faire un glissement des cellules
For i = 1 To NbCol + 1
    For j = 1 To NbLig
        Select Case i
            Case Is <> IdCol
                '-> Tester si on doit faire glisser des cellules
                If i > IdCol Then 'Faire glisser la cellule suivante
                    '-> Pointer sur la colonne suivante
                    Set aCell = aBlock.Cellules("L" & j & "C" & i)
                    '-> Modifier les positions
                    aCell.X1 = aCell.X1 + DifPix
                    aCell.X2 = aCell.X2 + DifPix
                    '-> D�caler la colonne associ�e
                    aCell.Colonne = aCell.Colonne - 1
                    '-> Enregistrer sous la nouvelle r�f�rence Colonne = i -1
                    aBlock.Cellules.Add aCell, "L" & j & "C" & i - 1
                    '-> Supprimer l'ancienne r�f�rence colonne = i
                    aBlock.Cellules.Remove ("L" & j & "C" & i)
                    aCol = i - 1
                Else
                    aCol = i
                End If
                '-> Pointer sur la nouvelle r�f�rence
                Set aCell = aBlock.Cellules("L" & j & "C" & aCol)
                '-> Recr�er la matrice cells
                Cells(1, (j - 1) * NbCol + aCol) = aCol
                Cells(2, (j - 1) * NbCol + aCol) = j
                Cells(3, (j - 1) * NbCol + aCol) = CreateRectRgn(aCell.X1, aCell.Y1, aCell.X2, aCell.Y2)
                Cells(4, (j - 1) * NbCol + aCol) = aCell.X1
                Cells(5, (j - 1) * NbCol + aCol) = aCell.Y1
                '-> D�termination de la cellule active
                If ActiveCell.x = NbCol + 1 Then
                    k = NbCol
                Else
                    k = ActiveCell.x + 1
                End If
                If j <> 1 And i = k Then
                    Cells(6, (j - 1) * NbCol + aCol) = 1
                Else
                    If i = k Then aCell.IsActive = True
                    Cells(6, (j - 1) * NbCol + aCol) = 0
                End If
                '-> V�rifier la bordure haut
                aCell.BordureHaut = GetBordureHaut(aBlock, aLig, i)
                '-> V�rifier la bordure gauche
                aCell.BordureGauche = GetBordureGauche(aBlock, j, aCol)
            Case Is = IdCol
                '-> Supprimer les cellules
                aBlock.Cellules.Remove ("L" & j & "C" & i)
        End Select
    Next 'Pour toutes les lignes
Next 'pour toutes les colonnes

If aBlock.IsEdit Then

    '-> Pointer sur la cellule active
    If NbCol = 1 Then
        ActiveCell.x = 1
    Else
        If ActiveCell.x = NbCol + 1 Then ActiveCell.x = NbCol
    End If
    ActiveCell.y = 1
    CellFin.y = aBlock.NbLigne
    CellFin.x = ActiveCell.x
    
    '-> Redonner le focus au block
    Me.BlockOut.SetFocus
    Me.BlockOut.Refresh
    Me.EnteteColonne.Refresh
End If

'-> Liberer le pointeur
Set aCell = Nothing

End Sub


Private Sub DelLig(ByVal LigClick As Integer)

'---> Fonction qui supprime une ligne

Dim Rep
Dim aLb As Libelle

Dim HauteurCm As Single
Dim HauteurPixPix As Integer
Dim DifPix As Integer
Dim i As Integer
Dim NbCol As Integer, NbLig As Integer
Dim aCell As Cellule
Dim PosLig As Integer
Dim PosColonne() As Integer
Dim aLig As Integer
Dim aBlock As Block, aBlock2 As Block
Dim aTb As Tableau
Dim aForm As FrmBlock

'-> Obtenir un pointeur sur le block courrant
SelectBlock aBlock

'-> Obtenir un pointeur sur le tableau
Set aTb = Maquettes("MAQUETTE").Tableaux(UCase$(Me.TableauCours))

'-> R�cup�rer les valeurs
NbCol = aBlock.NbCol
NbLig = aBlock.NbLigne
PosLig = Cells(5, (LigClick - 1) * NbCol + 1)
ReDim PosColonne(1 To 2, 1 To NbCol)
For i = 1 To Col
    PosColonne(1, i) = aBlock.Cellules("L1" & "C" & i).X1
    PosColonne(2, i) = aBlock.Cellules("L" & "C" & i).X2
Next
'-> Hauteur en Cm
HauteurCm = aBlock.Hauteur - aBlock.GetHauteurLig(LigClick)
'-> Largeur en Pixels
Set aCell = aBlock.Cellules("L" & LigClick & "C1")
DifPix = aCell.Y1 - aCell.Y2
Set aCell = Nothing
NbLig = NbLig - 1

'-> Appliquer les modifications au block en cours
ApplicModifDelLig aBlock, HauteurCm, NbCol, NbLig, LigClick, DifPix, PosColonne, PosLig

If aBlock.SlaveLink <> "" Then
    For i = 1 To NumEntries(aBlock.SlaveLink, "|")
        Set aBlock2 = aTb.Blocks("BL-" & UCase$(Entry(i, aBlock.SlaveLink, "|")))
        If aBlock2.IsEdit Then
            Set aForm = aTb.frmBlocks(UCase$(aBlock2.Nom))
            aForm.UpdateDelLig LigClick
            Set aForm = Nothing
        Else
            ApplicModifDelLig aBlock2, HauteurCm, NbCol, NbLig, LigClick, DifPix, PosColonne, PosLig
        End If
        Set aBlock2 = Nothing
    Next
    Me.ZOrder
End If


Set aCell = Nothing
Set aBlock = Nothing
Set aBlock2 = Nothing
Set aTb = Nothing
Set aForm = Nothing

DisplayDimensions

End Sub

Private Sub ApplicModifDelLig(aBlock As Block, ByVal HauteurCm As Single, _
                             ByVal NbCol As Integer, ByVal NbLig As Integer, ByVal IdLig As Integer, _
                             ByVal DifPix As Integer, ByRef PosColonne() As Integer, _
                             ByVal PosLig As Integer)

Dim i As Integer, j As Integer, k As Integer
Dim aRect As Rect, aRect1 As Rect, aRect2 As Rect
Dim Res As Long
Dim aCell As Cellule
Dim aLig As Integer

'-> Appliquer les modifications
aBlock.Hauteur = HauteurCm
aBlock.NbLigne = NbLig
aBlock.HauteurPix = aBlock.HauteurPix + DifPix
'-> Faire glisser les valeurs
For i = IdLig To NbLig
    aBlock.SetHauteurLig i, aBlock.GetHauteurLig(i + 1)
Next
'-> Initialiser le block
aBlock.Init

If aBlock.IsEdit Then
    '-> Faire glissser les valeurs
    For i = IdLig To NbLig
        Lignes(i) = Lignes(i + 1)
    Next
    '-> R�gion � faire d�filer : Attention, si derni�re ligne pas de r�gion � faire d�filer
    If IdLig = NbLig + 1 Then
    Else
        aRect.Top = Cells(5, (IdLig) * NbCol + 1) - 1
        aRect.Left = 0
        aRect.Right = Me.BlockOut.Width
        aRect.Bottom = Me.BlockOut.Height
        '-> R�cup�rer la zone cliente du block de donn�es
        Res = GetClientRect(Me.BlockOut.Hwnd, aRect1)
        '-> Scroll du Dc
        Res = ScrollDC(Me.BlockOut.hDC, 0, DifPix, aRect, aRect1, 0, aRect2)
    End If
    '-> Diminuer la largeur des pictureBox
    Me.BlockOut.Height = Me.BlockOut.Height + DifPix
    Me.EnteteLigne.Height = Me.EnteteLigne.Height + DifPix
    '-> Redimension des matrices
    InitMatriceLig NbLig, True
    '-> Supprimer les diff�rentes r�gions
    For i = 1 To (NbLig + 1) * NbCol
        DeleteObject Cells(3, i)
    Next
    '-> Recr�er la matrice des cellules
    ReDim Cells(6, NbCol * NbLig)
End If

'-> Faire un glissement des cellules
For i = 1 To NbCol
    For j = 1 To NbLig + 1
        Select Case j
            Case Is < IdLig, Is > IdLig
                '-> Tester si on doit faire glisser des cellules
                If j > IdLig Then 'Faire glisser la cellule suivante
                    '-> Pointer sur la cellule d�sign�e
                    Set aCell = aBlock.Cellules("L" & j & "C" & i)
                    '-> Modifier les positions
                    aCell.Y1 = aCell.Y1 + DifPix
                    aCell.Y2 = aCell.Y2 + DifPix
                    '-> D�caler la ligne associ�e
                    aCell.Ligne = aCell.Ligne - 1
                    '-> Enregistrer sous la nouvelle r�f�rence ligne = j -1
                    aBlock.Cellules.Add aCell, "L" & j - 1 & "C" & i
                    '-> Supprimer l'ancienne r�f�rence ligne = i
                    aBlock.Cellules.Remove ("L" & j & "C" & i)
                    aLig = j - 1
                Else
                    aLig = j
                End If
                '-> Pointer sur la nouvelle r�f�rence
                Set aCell = aBlock.Cellules("L" & aLig & "C" & i)
                '-> Recr�er la matrice cells
                Cells(1, (aLig - 1) * NbCol + i) = i
                Cells(2, (aLig - 1) * NbCol + i) = aLig
                Cells(3, (aLig - 1) * NbCol + i) = CreateRectRgn(aCell.X1, aCell.Y1, aCell.X2, aCell.Y2)
                Cells(4, (aLig - 1) * NbCol + i) = aCell.X1
                Cells(5, (aLig - 1) * NbCol + i) = aCell.Y1
                '-> Gestion de la cellule active
                If ActiveCell.y = NbLig + 1 Then
                    k = NbLig
                Else
                    k = ActiveCell.y + 1
                End If
                If j = k And i <> 1 Then
                    Cells(6, (aLig - 1) * NbCol + i) = 1
                Else
                    If j = k Then aCell.IsActive = True
                    Cells(6, (aLig - 1) * NbCol + i) = 0
                End If
                '-> V�rifier la bordure haut
                aCell.BordureHaut = GetBordureHaut(aBlock, aLig, i)
            Case Is = IdLig
                '-> Supprimer les cellules
                aBlock.Cellules.Remove ("L" & j & "C" & i)
        End Select
    Next 'Pour toutes les lignes
Next 'pour toutes les colonnes

If aBlock.IsEdit Then
    '-> Pointer sur la cellule active
    If ActiveCell.y = NbLig + 1 Then ActiveCell.y = NbLig
    ActiveCell.x = 1
    CellFin.y = k
    CellFin.x = aBlock.NbCol
    
    '-> Redonner le focus au block
    Me.BlockOut.SetFocus
    Me.BlockOut.Refresh
    Me.EnteteLigne.Refresh

End If

Set aCell = Nothing

End Sub

Private Sub SetBarreFormule()

'-> Active les propri�t�s de la cellule active dans la barre de formule
Dim aCell As Cellule
Dim aBlock As Block

'-> Obtenir un pointeur sur le block
SelectBlock aBlock

Set aCell = aBlock.Cellules("L" & ActiveCell.y & "C" & ActiveCell.x)
Me.BarreFormule.FontName = aCell.FontName
Me.BarreFormule.Text = aCell.Contenu

Set aCell = Nothing
Set aBlock = Nothing

End Sub

Private Sub Form_Unload(Cancel As Integer)

Dim i As Integer, j As Integer
Dim Res As Long
Dim aBlock As Block
Dim aTb As Tableau
Dim aCell As Cellule

'-> Obtenir un pointeur sur le block
SelectBlock aBlock

'-> obtenir un pointeur sur le tableau
Set aTb = Maquettes("MAQUETTE").Tableaux(UCase$(Me.TableauCours))

'-> Indiquer que le block n'est plus �dit�
aBlock.IsEdit = False

'-> Le supprimer de la collection
aTb.frmBlocks.Remove (UCase$(aBlock.Nom))

'-> Supprimer les objets r�gions
For i = 1 To aBlock.NbCol * aBlock.NbLigne
    '-> Supprimer l'objet r�gion
    Res = DeleteObject(Cells(3, i))
Next

'-> Supprimer la r�gion associ�e au tableau
DeleteObject aBlock.hRgn

'-> Indiquer qu'il n'y plus de cellules actives
For i = 1 To aBlock.NbCol
    For j = 1 To aBlock.NbLigne
        Set aCell = aBlock.Cellules("L" & j & "C" & i)
        aCell.IsActive = False
    Next 'Pour toutes les lignes
Next 'Pour toutes les colonnes

'-> Dessiner le tableau
DrawTableau Maquettes("MAQUETTE").Tableaux(UCase$(Me.TableauCours))

'-> Supprimer la matrice des cellules
Erase Cells()

save "", True

'-> Liberer le pointeur sur le block
Set aBlock = Nothing

'-> Liberer le pointeur sur le tableau
Set aTb = Nothing

End Sub

Private Sub Horizontal_Change()
    '-> Faire d�iler le block
    Me.BlockOut.Left = Me.Horizontal.Value
    '-> Faire d�filer les entetes de colonnes
    Me.EnteteColonne.Left = Me.BlockOut.Left
    Me.BlockOut.ZOrder 1
    Me.Carret.ZOrder
    
End Sub





Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)

'---> Affichage de la boite de dialogue personnalis�e

On Error GoTo Intercept

Dim ColMin As Integer, ColMax As Integer
Dim i As Integer, j As Integer
Dim aBlock As Block

'-> Ne pas afficher la gestion des couleurs
If ButtonMenu.Key <> "COLOR" Then Exit Sub

'-> Afficher la boite de dialogue du choix des couleurs
Me.CmmColor.CancelError = True
Me.CmmColor.ShowColor

'-> Obtenir un pointeur sur le block en cours
SelectBlock aBlock

'-> D�terminer le carret de s�lection
If CellFin.x < ActiveCell.x Then
    ColMin = CellFin.x
    ColMax = ActiveCell.x
Else
    ColMin = ActiveCell.x
    ColMax = CellFin.x
End If
If CellFin.y > ActiveCell.y Then
    LigMin = ActiveCell.y
    LigMax = CellFin.y
Else
    LigMin = CellFin.y
    LigMax = ActiveCell.y
End If

'-> Appliquer la s�lection
For i = ColMin To ColMax
    For j = LigMin To LigMax
        '-> Pointer sur la cellule
        Set aCell = aBlock.Cellules("L" & j & "C" & i)
        '-> Appliquer la modif
        aCell.FondTransParent = False
        aCell.BackColor = Me.CmmColor.Color
    Next
Next

'-> Mise � jour de la fen�tre de propri�t�
DisplayCellProperties aCell
'-> Forcer le redessin du block
Me.BlockOut.Refresh



Exit Sub

Intercept:

    Exit Sub


End Sub


















Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

Dim Res As Long
Dim aPoint As POINTAPI
Dim ColMin As Integer, ColMax As Integer, LigMin As Integer, LigMax As Integer
Dim i As Integer, j As Integer
Dim aLb As Libelle
Dim Rep
Dim aBlock As Block
Dim aTb As Tableau
Dim aCell As Cellule
Dim aBlock2 As Block
Dim Tempo
Dim IsTransparent As Boolean

'-> Obtenir un pointeur sur le block en cours
SelectBlock aBlock

'-> D�terminer le carret de s�lection
If CellFin.x < ActiveCell.x Then
    ColMin = CellFin.x
    ColMax = ActiveCell.x
Else
    ColMin = ActiveCell.x
    ColMax = CellFin.x
End If
If CellFin.y > ActiveCell.y Then
    LigMin = ActiveCell.y
    LigMax = CellFin.y
Else
    LigMin = CellFin.y
    LigMax = ActiveCell.y
End If

Select Case Button.Key

    Case "AlignCell"

    Case "Gras"
            

        For i = ColMin To ColMax
            For j = LigMin To LigMax
                '-> Pointer sur la cellule
                Set aCell = aBlock.Cellules("L" & j & "C" & i)
                '-> Appliquer la modif
                If Button.Value = tbrPressed Then
                    aCell.FontBold = True
                Else
                    aCell.FontBold = False
                End If
                '-> Elle n'est plus s�lectionn�e
                Cells(6, (j - 1) * aBlock.NbCol + i) = 0
            Next
        Next

        BlockOut.Refresh

    Case "Italic"

        For i = ColMin To ColMax
            For j = LigMin To LigMax
                '-> Pointer sur la cellule
                Set aCell = aBlock.Cellules("L" & j & "C" & i)
                '-> Appliquer la modif
                If Button.Value = tbrPressed Then
                    aCell.FontItalic = True
                Else
                    aCell.FontItalic = False
                End If
                '-> Elle n'est plus s�lectionn�e
                Cells(6, (j - 1) * aBlock.NbCol + i) = 0
            Next
        Next

        BlockOut.Refresh

    Case "SousLigne"

        For i = ColMin To ColMax
            For j = LigMin To LigMax
                '-> Pointer sur la cellule
                Set aCell = aBlock.Cellules("L" & j & "C" & i)
                '-> Appliquer la modif
                If Button.Value = tbrPressed Then
                    aCell.FontUnderline = True
                Else
                    aCell.FontUnderline = False
                End If
                '-> Elle n'est plus s�lectionn�e
                Cells(6, (j - 1) * aBlock.NbCol + i) = 0
            Next
        Next

        BlockOut.Refresh

    Case "FontColor"

    Case "BackColor"
        '-> Affichage de qla palette de couleurs
        Res = GetCursorPos(aPoint)
        frmPalette.Top = Me.ScaleY(aPoint.y, 3, 1)
        frmPalette.Left = Me.ScaleX(aPoint.x, 3, 1)
        RetourColor = -1
        frmPalette.Show vbModal
                
        '-> Traitement de la couleur de reponse
        If RetourColor <> -1 Then
            For i = ColMin To ColMax
                For j = LigMin To LigMax
                    '-> Pointer sur la cellule
                    Set aCell = aBlock.Cellules("L" & j & "C" & i)
                    If RetourColor = 999 Then
                        aCell.FondTransParent = True
                        '-> Appliquer un format blanc � la s�lection
                        aCell.BackColor = 16777215
                    Else
                        '-> Appliquer la modif
                        aCell.FondTransParent = False
                        aCell.BackColor = RetourColor
                    End If
                Next
            Next
            DisplayCellProperties aCell
        End If
        Me.BlockOut.Refresh

    Case "Bordures"

        Res = GetCursorPos(aPoint)
        frmBordure.Top = Me.ScaleY(aPoint.y, 3, 1)
        frmBordure.Left = Me.ScaleX(aPoint.x, 3, 1)
        RetourBordure = -1
        frmBordure.Show vbModal
        If RetourBordure <> -1 Then
            For i = ColMin To ColMax
                For j = LigMin To LigMax
                    '-> Pointer sur la cellule
                    Set aCell = aBlock.Cellules("L" & j & "C" & i)
                    '-> Appliquer la modif
                    If RetourBordure = 99 Then
                        '-> Appliquer les bordures
                        If j = LigMin Then aCell.BordureHaut = True Else aCell.BordureHaut = False
                        If j = LigMax Then aCell.BordureBas = True Else aCell.BordureBas = False
                        If i = ColMin Then aCell.BordureGauche = True Else aCell.BordureGauche = False
                        If i = ColMax Then aCell.BordureDroite = True Else aCell.BordureDroite = False
                    Else
                        aCell.GetBordureByIndex RetourBordure
                    End If
                    '-> Modifier les cellules adjacentes
                    UpdateBordure aCell
                Next
            Next
        End If

        Me.BlockOut.Refresh
        
    Case "Autoajust"

        For i = ColMin To ColMax
            For j = LigMin To LigMax
                '-> Pointer sur la cellule
                Set aCell = aBlock.Cellules("L" & j & "C" & i)
                '-> Appliquer la modif
                If Button.Value = tbrPressed Then
                    aCell.AutoAjust = True
                Else
                    aCell.AutoAjust = False
                End If
                '-> Elle n'est plus s�lectionn�e
                Cells(6, (j - 1) * aBlock.NbCol + i) = 0
            Next
        Next
        
        Me.BlockOut.Refresh

    Case "ALign"

        Set frmAlignBlock.aBlock = aBlock
        frmAlignBlock.Show vbModal

    Case "DelLink"

        '-> Pointer sur la gestion des messprogs
        Set aLb = Libelles("FRMBLOCK")
        '-> V�rifier que le block est bien escalve d'un maitre
        If aBlock.MasterLink = "" Then
            MsgBox aLb.GetCaption(15), vbExclamation + vbOKOnly, aLb.GetToolTip(15)
            Set aLb = Nothing
            Exit Sub
        End If
        '-> Demander la confirmation
        Rep = MsgBox(aLb.GetCaption(16) & Chr(13) & aBlock.MasterLink, vbQuestion + vbYesNo, aLb.GetToolTip(16))
        If Rep = vbYes Then
            '-> Pointer sur le tableau
            Set aTb = Maquettes("MAQUETTE").Tableaux(UCase$(Me.TableauCours))
            '-> Suppression du lien chez le maitre
            Set aBlock2 = aTb.Blocks("BL-" & UCase$(aBlock.MasterLink))
            '-> R�cup�ration de l'index de l'entr�e de la matrice
            i = GetEntryIndex(aBlock2.SlaveLink, aBlock.Nom, "|")
            '-> Supression de l'entr�e
            aBlock2.SlaveLink = DeleteEntry(aBlock2.SlaveLink, i, "|")
            '-> Lib�rer le pointeur sur le block maitre
            Set aBlock2 = Nothing
            '-> indiquer que le block en cours n'est plus li�
            aBlock.MasterLink = ""
            IsLinked = False
            '-> Modifier le tootip
            Me.Toolbar1.Buttons("DelLink").ToolTipText = aLb.GetCaption(19)
        End If
        '-> Lib�rer le pointeur sur le libelle
        Set aLb = Nothing

End Select


Set aCell = Nothing
Set aLb = Nothing
Set aBlock = Nothing
Set aBlock2 = Nothing


End Sub

Private Sub Toolbar1_ButtonDropDown(ByVal Button As MSComctlLib.Button)
    
Dim aBlock As Block
Dim aCell As Cellule
Dim aPoint As POINTAPI
Dim ColMin As Integer, ColMax As Integer, LigMin As Integer, LigMax As Integer
Dim i As Integer, j As Integer

'-> Obtenir un pointeur sur le block
SelectBlock aBlock
    
'-> D�terminer le carret de s�lection
If CellFin.x < ActiveCell.x Then
    ColMin = CellFin.x
    ColMax = ActiveCell.x
Else
    ColMin = ActiveCell.x
    ColMax = CellFin.x
End If
If CellFin.y > ActiveCell.y Then
    LigMin = ActiveCell.y
    LigMax = CellFin.y
Else
    LigMin = CellFin.y
    LigMax = ActiveCell.y
End If
        
Select Case Button.Key

    Case "AlignCell"

        Res = GetCursorPos(aPoint)
        frmAlignCell.Top = Me.ScaleY(aPoint.y, 3, 1)
        frmAlignCell.Left = Me.ScaleX(aPoint.x, 3, 1)
        Set frmAlignCell.aForm = Me
        Retour = -1
        frmAlignCell.Show vbModal
        If Retour <> -1 Then
            For i = ColMin To ColMax
                For j = LigMin To LigMax
                    '-> Pointer sur la cellule
                    Set aCell = aBlock.Cellules("L" & j & "C" & i)
                    '-> Aplliquer la modif
                    aCell.CellAlign = Retour
                Next
            Next
            DisplayCellProperties aCell
        End If
        Me.BlockOut.Refresh
        
        
    Case "FontColor"
        
        Res = GetCursorPos(aPoint)
        frmPalette.Top = Me.ScaleY(aPoint.y, 3, 1)
        frmPalette.Left = Me.ScaleX(aPoint.x, 3, 1)
        frmPalette.Couleur(2).Visible = False
        RetourColor = -1
        frmPalette.Show vbModal
        If RetourColor <> -1 Then
            For i = ColMin To ColMax
                For j = LigMin To LigMax
                    '-> Pointer sur la cellule
                    Set aCell = aBlock.Cellules("L" & j & "C" & i)
                    '-> Appliquer la modif
                    aCell.FontColor = RetourColor
                Next
            Next
            DisplayCellProperties aCell
        End If
        Me.BlockOut.Refresh
        
        
End Select

'-> Lib�rer les pointeurs
Set aBlock = Nothing
Set aCell = Nothing

Me.BlockOut.SetFocus

End Sub

Private Sub Vertical_Change()
'-> Faire un scroll du block
    Me.BlockOut.Top = Me.Vertical.Value
    '-> Faire un scroll de l'entete
    Me.EnteteLigne.Top = Me.BlockOut.Top
    Me.BlockOut.ZOrder 1
    Me.Carret.ZOrder
    Me.Toolbar1.ZOrder
    Me.BarreFormule.ZOrder
    
End Sub

Private Sub DisplayCellProperties(ByVal aCell As Cellule)

'---> Met � jour la barre d'outil avec avec les options adequates _
et force le dessin de la cellule

'-> Alignement interne
Me.Toolbar1.Buttons("AlignCell").Image = Me.ImageList1.ListImages("Align" & aCell.CellAlign).Key

'-> Gras
If aCell.FontBold Then
    Me.Toolbar1.Buttons("Gras").Value = tbrPressed
Else
    Me.Toolbar1.Buttons("Gras").Value = tbrUnpressed
End If

'-> Italic
If aCell.FontItalic Then
    Me.Toolbar1.Buttons("Italic").Value = tbrPressed
Else
    Me.Toolbar1.Buttons("Italic").Value = tbrUnpressed
End If

'-> SousLigne
If aCell.FontUnderline Then
    Me.Toolbar1.Buttons("SousLigne").Value = tbrPressed
Else
    Me.Toolbar1.Buttons("SousLigne").Value = tbrUnpressed
End If

'-> Police
Me.ComboFont.Text = aCell.FontName

'-> FontSize
Me.ComboSize.Text = aCell.FontSize

'-> Contenu
Me.BarreFormule.Text = aCell.Contenu

'-> AutoAjust
If aCell.AutoAjust Then
    Me.Toolbar1.Buttons("Autoajust").Value = tbrPressed
Else
    Me.Toolbar1.Buttons("Autoajust").Value = tbrUnpressed
End If

End Sub


Private Sub DisplayDimensions()

Dim aLb As Libelle
Dim PosX As Integer
Dim PosY As Integer
Dim LibDim As String
Dim LargeurTwips As Integer
Dim HauteurTwips As Integer
Dim aBlock As Block

'-> Obtenir un pointeur sur le block
SelectBlock aBlock

Set aLb = Libelles("FRMBLOCK")

'-> Afficher les dimensions du block
Me.Toolbar1.Buttons("Properties").ButtonMenus("Largeur").Text = aLb.GetCaption(20) & "  " & aBlock.Largeur
Me.Toolbar1.Buttons("Properties").ButtonMenus("Hauteur").Text = aLb.GetCaption(19) & "  " & aBlock.Hauteur

'-> Afficher les dimensions de la cellule
LibDim = aBlock.GetLargeurCol(ActiveCell.x) & " x " & aBlock.GetHauteurLig(ActiveCell.y)
LargeurTwips = Me.Picture2.TextWidth(LibDim)
HauteurTwips = Me.Picture2.TextHeight(LibDim)
Me.Picture2.Cls
Me.Picture2.CurrentX = (Me.Picture2.Width - LargeurTwips) / 2
Me.Picture2.CurrentY = (Me.Picture2.Height - HauteurTwips) / 2
Me.Picture2.FontName = "Times New Roman"
Me.Picture2.FontSize = 10
Me.Picture2.Print LibDim
Me.Picture2.Refresh

Set aLb = Nothing

End Sub

Private Sub UpdateBordure(ByVal aCell As Cellule)

Dim aCell2 As Cellule
Dim NbLig As Integer, NbCol As Integer
Dim Ligne As Integer, Colonne As Integer
Dim aBlock As Block

'---> Cette proc�dure modifie les cellules adjacentes � la cellule en cours; Il y a 4 possibilit�s
'-> Cette proc�dure est utilis�e quand on fait le setting des bordures d'une cellule

'-> Obtenir un pointeur
SelectBlock aBlock

NbLig = aBlock.NbLigne
NbCol = aBlock.NbCol
Ligne = aCell.Ligne
Colonne = aCell.Colonne

'-> D�terminer les cellules � modifier
If Colonne <> 1 Then
    Set aCell2 = aBlock.Cellules("L" & Ligne & "C" & Colonne - 1)
    aCell2.BordureDroite = aCell.BordureGauche
End If

If Colonne <> NbCol Then
    Set aCell2 = aBlock.Cellules("L" & Ligne & "C" & Colonne + 1)
    aCell2.BordureGauche = aCell.BordureDroite
End If

If Ligne <> 1 Then
    Set aCell2 = aBlock.Cellules("L" & Ligne - 1 & "C" & Colonne)
    aCell2.BordureBas = aCell.BordureHaut
End If

If Ligne <> NbLig Then
    Set aCell2 = aBlock.Cellules("L" & Ligne + 1 & "C" & Colonne)
    aCell2.BordureHaut = aCell.BordureBas
End If

Set aCell2 = Nothing
Set aBlock = Nothing

End Sub

Public Sub UpdateInsertCol(ByVal IdCol As Integer)

'---> Applique une modification d'insertion de colonne lors d'un lien

'-> Positionner la cellule active au m�me endroit que la r�f�rence
SelActiveCell 1, IdCol

'-> Appliquer l'insertion de colonne
InsertCol IdCol


End Sub

Public Sub UpdateDelCol(ByVal IdCol As Integer)

'---> Appliquer une modification de suppression de colonne

'-> Positionner la cellule active au m�me endroit que la r�f�rence
SelActiveCell 1, IdCol

'-> Appilquer la suppression de colonne
DelCol IdCol

End Sub

Public Sub UpdateDelLig(ByVal IdLig As Integer)

'---> Appliquer une modification de suppression de ligne

'-> Positionner la cellule active au m�me endroit que la r�f�rence
SelActiveCell IdLig, 1

'-> Appilquer la suppression de ligne
DelLig IdLig


End Sub


Public Sub UpdateInsertLig(ByVal IdLig As Integer)

'---> Applique une modification d'insertion de colonne lors d'un lien

'-> Positionner la cellule active au m�me endroit que la r�f�rence
SelActiveCell IdLig, 1

'-> Appilquer l'insertion de colonne
InsertLig IdLig


End Sub

Public Sub UpdateInsertLigBas(ByVal IdLig As Integer)

'---> Applique une modification d'insertion de colonne lors d'un lien

'-> Positionner la cellule active au m�me endroit que la r�f�rence
SelActiveCell IdLig, 1

'-> Appilquer l'insertion de colonne
InsertLigBas IdLig

End Sub

Public Sub UpdateModifLargeur(ByVal aLargeur As Single, ByVal Cm As Boolean, ByVal aCol As Integer)

'---> Applique une modification de largeur de colonne lors d'un lien

'-> Positionner la cellule active au m�me endroit que la r�f�rence
SelActiveCell 1, aCol

'-> Appliquer la modification de largeur de la colonne
ModifLargeur aLargeur, Cm, aCol

Me.BlockOut.Refresh
Me.EnteteColonne.Refresh

End Sub

Public Sub UpdateModifHauteur(ByVal aHauteur As Single, ByVal Cm As Boolean, ByVal LigClick As Integer)

'---> Applique une modification de hauteur de ligne lors d'un lien

'-> Positionner la cellule active au m�me endroit que la r�f�rence
SelActiveCell LigClick, 1

'-> Appliquer la modification de hauteur de la ligne
ModifHauteur aHauteur, Cm, LigClick

Me.BlockOut.Refresh
Me.EnteteLigne.Refresh

End Sub


Public Sub UpdateBlock()

'---> Cette proc�dure r�cr�er les matrices colonnes() et lignes () en fonction _
des objets cellules

On Error Resume Next

Dim NbCol As Integer
Dim NbLig As Integer
Dim i As Integer
Dim aCell As Cellule
Dim aBlock As Block

'-> Obtenir un pointeur sur le block
SelectBlock aBlock

'-> R�cup�rer les infos
NbCol = aBlock.NbCol
NbLig = aBlock.NbLigne

'-> Recr�er les matrices
ReDim Colonnes(1 To NbCol)
ReDim Lignes(1 To NbLig)

'-> Matrice des colonnes
For i = 1 To NbCol
    Set aCell = aBlock.Cellules("L1" & "C" & i)
    Colonnes(i) = aCell.X2 - aCell.X1 - 1
Next

'-> Matrice des lignes
For i = 1 To NbLig
    Set aCell = aBlock.Cellules("L" & i & "C1")
    Lignes(i) = aCell.Y2 - aCell.Y1 - 1
Next

'-> Matrice des cellules
ReDim Cells(1 To 6, NbLig * NbCol)

'-> Setting des diff�rentes dimensions
Me.BlockOut.Width = aBlock.LargeurPix
Me.BlockOut.Height = aBlock.HauteurPix
Me.EnteteColonne.Width = aBlock.LargeurPix
Me.EnteteLigne.Height = aBlock.HauteurPix

Me.Height = Me.ScaleY(Me.BlockOut.Height + Me.EnteteColonne.Height + Me.BarreFormule.Height, 3, 1) + Me.ScaleY(2, 7, 1)
Me.Width = Me.ScaleX(Me.EnteteLigne.Width + Me.BlockOut.Width, 3, 1) + Me.ScaleX(1, 7, 1)

'-> Dessiner le Block
CreateBlock 1, 1

Set aCell = Nothing
Set aBlock = Nothing

End Sub

