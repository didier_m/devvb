VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MaqChar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'***************************************************************
'*                                                             *
'* Cette classe sert � g�rer la maquette caract�re associ�e    *
'*                                                             *
'***************************************************************

'-> Emplacement d'origine de la maquette
Public Path As String

'-> Nom de la maquette d'origine
Public Nom As String

'-> Lig de la maquette carcat�re
Public Lig As Integer

'-> Max de la maquette
Public Max As Integer

'-> Fichier de la maquette de transition
Public MaqTransition As String

'-> Collection des rangs caract�res associ�s
Public RangChars As Collection

Public Function AnalyseMaq(ByVal MaqFic As Integer, ByRef aMaqChar As MaqChar) As Boolean

'-> Proc�dure qui analyse une maquette caract�re et qui r�cup�re les divers _
rangs dans la maquette et compte le nombre de lignes

Dim i As Integer
Dim Ligne As String
Dim FindRg As Boolean
Dim FindGui As Boolean
Dim aRg As RangChar
Dim NbRang As Integer
Dim Entete As Boolean
Dim NomRang As String
Dim aLb As Libelle
Dim IsFin As Boolean

On Error GoTo EndAnalyse

'-> initaliser � false la valeur par d�faut renvoy�e
AnalyseMaq = False

'-> Initialiser le pointeur dde rang trouve
FindRg = False

'-> Initaliser le fait d'avoir trouv� l'entete
Entete = True

'-> Initialisation du nombre de rang trouv�s
NbRang = 1

'-> Le premier rang cr�� est le rang d'entete
Set aRg = New RangChar
aRg.Nom = "Entete"
aRg.IdAffichage = NbRang
NbRang = NbRang + 1
'-> Ajouter le rang dans la collection
aMaqChar.RangChars.Add aRg, (UCase$(aRg.Nom))

'-> Cr�ation de l'entete de la maquette caract�re
Do While Not EOF(MaqFic)
    '-> Lecture de la ligne
    Line Input #MaqFic, Ligne
    If Trim(Ligne) = """" Then
    Else
        If UCase$(Ligne) = """FIN""" Or InStr(1, UCase$(Ligne), "-FORMAT") <> 0 Or InStr(1, UCase$(Ligne), "-CANEDI") <> 0 Or InStr(1, UCase$(Ligne), "-CALCUL") <> 0 Then
            '-> Cr�ation d'un rang de fin de page
            If Not IsFin Then
                Set aRg = New RangChar
                aRg.Nom = "FIN"
                aRg.IdAffichage = NbRang
                NbRang = NbRang + 1
                IsFin = True
            End If
        End If
        If UCase$(Ligne) = """GUI""" Then
            Set aLb = Libelles("MESSTRANSITION")
            MsgBox aLb.GetCaption(8), vbCritical + vbOKOnly, aLb.GetToolTip(8)
            AnalyseMaq = False
            GoTo EndAnalyse
        End If
        
        
        '-> Analyse si c'est un rang
        If UCase$(Mid$(Ligne, 1, 3)) = """RG" And Not IsFin Then
            If Entete Then Entete = False
            Ligne = RTrim(Ligne)
            FindRg = True
            '-> Ajouter un objet rang dans la collection
            Set aRg = New RangChar
            aRg.IdAffichage = NbRang
            NbRang = NbRang + 1
            NomRang = UCase$(Mid$(Ligne, 2, Len(Ligne) - 2))
            aRg.Nom = NomRang
            aMaqChar.RangChars.Add aRg, (NomRang)
            aRg.nLignes = 1
        ElseIf UCase$(Mid$(Ligne, 1, 4)) = """ESC" Then
        ElseIf UCase$(Mid$(Ligne, 1, 4)) = """LIG" Then
            '-> il faut r�cup�rer le lig
            Ligne = aMaqChar.DeleteEscape(Ligne)
            aRg.AddLigne Ligne
            Ligne = Trim(Ligne)
            aMaqChar.Lig = CInt(Trim(Mid$(Ligne, 5, Len(Ligne) - 5)))
        ElseIf UCase$(Mid$(Ligne, 1, 4)) = """MAX" Then
            Ligne = aMaqChar.DeleteEscape(Ligne)
            aRg.AddLigne Ligne
            Ligne = Trim(Ligne)
            aMaqChar.Max = CInt(Trim(Mid$(Ligne, 5, Len(Ligne) - 5)))
        Else
            If Trim(Ligne) = """" Then
            Else
                If IsSautPage(Ligne) Then aRg.IsSautPage = True
                Ligne = aMaqChar.DeleteEscape(Ligne)
                aRg.AddLigne Ligne
            End If
            
        End If
    End If
Loop

'-> Ajouter le rang de fin s'il existe
If aRg.Nom = "FIN" Then
    aMaqChar.RangChars.Add aRg, "FIN"
Else
    Set aRg = New RangChar
    aRg.Nom = "FIN"
    aRg.IdAffichage = NbRang
    NbRang = NbRang + 1
    aMaqChar.RangChars.Add aRg, "FIN"
End If
'-> Tester la valeur � renvoyer
If Not FindRg Then
    Set aLb = Libelles("MESSTRANSITION")
    MsgBox aLb.GetCaption(2) & Chr(13) & aMaqChar.Path, vbCritical + vbOKOnly, aLb.GetToolTip(2)
    AnalyseMaq = False
    GoTo EndAnalyse
End If

AnalyseMaq = True

EndAnalyse:

    '-> lib�rer la m�moire
    Set aRg = Nothing
    Set aLb = Nothing
    Set RgFin = Nothing
    
    '-> Fermer la maquette
    Close #MaqFic

End Function

Private Sub Class_Initialize()

    '-> initialiser la collection des rangs
    Set RangChars = New Collection
    
End Sub

Public Function DeleteEscape(ByVal Ligne As String) As String

'-> Proc�dure qui supprime les s�quences Escapes de la ligne pass�e en argument

Dim PosDeb As Integer
Dim PosFin As Integer
Dim LigneTrav As String

'-> Sortir la Ligne = ""
 
If Trim(Ligne) = "" Then
    DeleteEscape = Ligne
    Exit Function
End If

 
'-> Analyse de la ligne
LigneTrav = Ligne
Do

    PosDeb = InStr(1, LigneTrav, "<[")
    If PosDeb = 0 Then Exit Do
    
    '-> Recherche du caractere de fin de s�quence Escape
    PosFin = InStr(PosDeb, LigneTrav, "]>")
    PosFin = PosFin + 1
    
    If PosDeb = 1 Then
        LigneTrav = Mid$(LigneTrav, PosFin, Len(LigneTrav) - PosFin)
    Else
        LigneTrav = Mid$(LigneTrav, 1, PosDeb - 1) & _
                    Mid$(LigneTrav, PosFin + 1, Len(LigneTrav) - PosFin)
    End If
    
Loop

DeleteEscape = LigneTrav

End Function

Private Function IsSautPage(Ligne As String) As Boolean

'---> Cette proc�dure renvoie TRUE si elle trouve la sequence escape d'eun saut de page _
dans une ligne de texte


If InStr(1, Ligne, "<[FF]>") <> 0 Or _
   InStr(1, Ligne, "<[ff]>") <> 0 Or _
   InStr(1, Ligne, "<[fF]>") <> 0 Or _
   InStr(1, Ligne, "<[Ff]>") <> 0 Then
        IsSautPage = True
Else
    IsSautPage = False
End If

End Function
