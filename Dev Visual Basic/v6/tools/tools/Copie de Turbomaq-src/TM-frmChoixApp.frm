VERSION 5.00
Begin VB.Form frmChoixApp 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   4275
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4440
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4275
   ScaleWidth      =   4440
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox List2 
      Height          =   2790
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   4215
   End
   Begin VB.Image Annuler 
      Height          =   480
      Left            =   2280
      Top             =   3720
      Width           =   630
   End
   Begin VB.Image Aide 
      Height          =   480
      Left            =   3000
      Top             =   3720
      Width           =   630
   End
   Begin VB.Image Ok 
      Height          =   480
      Left            =   3720
      Top             =   3720
      Width           =   630
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Label2"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Width           =   3975
   End
End
Attribute VB_Name = "frmChoixApp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim aApp As Application
Dim i As Integer

Set aLb = Libelles("FRMCHOIXAPP")

Me.Caption = aLb.GetCaption(1)
Me.Label2.Caption = aLb.GetCaption(3)

Set aLb = Libelles("BOUTONS")

Me.Annuler.ToolTipText = aLb.GetCaption(1)
Me.Aide.ToolTipText = aLb.GetCaption(2)
Me.Ok.ToolTipText = aLb.GetCaption(3)

Me.Annuler.Picture = Turbo_Maq.Annuler.Picture
Me.Aide.Picture = Turbo_Maq.Aide.Picture
Me.Ok.Picture = Turbo_Maq.Ok.Picture


Set aLb = Nothing

'-> Afficher la liste des brubriques disponibles
Set aApp = V6ClientList("CLI|DEAL").Applications("APP|" & V6App)
For i = 1 To aApp.nRubriques
    Me.List2.AddItem aApp.GetRubriquesName(i)
    If V6Rub = aApp.GetRubriquesCode(i) Then Me.List2.ListIndex = i - 1
Next

If Me.List2.ListIndex = -1 And Me.List2.ListCount <> 0 Then Me.List2.ListIndex = 0

End Sub


Private Sub List2_DblClick()
    Ok_Click
End Sub

Private Sub List2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Ok_Click
End Sub

Private Sub Ok_Click()

'---> Faire le setting des variables progiciel et rubrique
Dim aApp As Application

If Me.List2.ListIndex = -1 Then
    MsgBox "Veuillez sélectionner une rubrique ", vbCritical + vbOKOnly, "Erreur"
    Me.List2.SetFocus
    Exit Sub
End If

Set aApp = V6ClientList("CLI|DEAL").Applications("APP|" & V6App)
strRetour = aApp.GetRubriquesCode(Me.List2.ListIndex + 1)

'-> Quitter
Unload Me

End Sub
