VERSION 5.00
Begin VB.Form frmPalette 
   BorderStyle     =   0  'None
   ClientHeight    =   1530
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2100
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "TM-frmPalette.frx":0000
   ScaleHeight     =   102
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   140
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1800
      ScaleHeight     =   255
      ScaleWidth      =   255
      TabIndex        =   1
      Top             =   30
      Width           =   255
      Begin VB.Image Image1 
         Height          =   240
         Left            =   0
         Picture         =   "TM-frmPalette.frx":A5F6
         Top             =   -10
         Width           =   240
      End
   End
   Begin VB.CommandButton Command1 
      Cancel          =   -1  'True
      Caption         =   "Command1"
      Height          =   255
      Left            =   2160
      TabIndex        =   0
      Top             =   720
      Width           =   375
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   2
      Left            =   1800
      Picture         =   "TM-frmPalette.frx":A980
      Top             =   1200
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   1
      Left            =   120
      Picture         =   "TM-frmPalette.frx":AB72
      Tag             =   "C1"
      Top             =   240
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   39
      Left            =   1560
      Picture         =   "TM-frmPalette.frx":AD64
      Top             =   1200
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   37
      Left            =   1320
      Picture         =   "TM-frmPalette.frx":AF56
      Top             =   1200
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   34
      Left            =   1080
      Picture         =   "TM-frmPalette.frx":B148
      Top             =   1200
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   35
      Left            =   840
      Picture         =   "TM-frmPalette.frx":B33A
      Top             =   1200
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   36
      Left            =   600
      Picture         =   "TM-frmPalette.frx":B52C
      Top             =   1200
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   40
      Left            =   360
      Picture         =   "TM-frmPalette.frx":B71E
      Top             =   1200
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   38
      Left            =   120
      Picture         =   "TM-frmPalette.frx":B910
      Top             =   1200
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   15
      Left            =   1800
      Picture         =   "TM-frmPalette.frx":BB02
      Top             =   960
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   54
      Left            =   1560
      Picture         =   "TM-frmPalette.frx":BCF4
      Top             =   960
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   33
      Left            =   1320
      Picture         =   "TM-frmPalette.frx":BEE6
      Top             =   960
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   8
      Left            =   1080
      Picture         =   "TM-frmPalette.frx":C0D8
      Top             =   960
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   4
      Left            =   840
      Picture         =   "TM-frmPalette.frx":C2CA
      Top             =   960
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   6
      Left            =   600
      Picture         =   "TM-frmPalette.frx":C4BC
      Top             =   960
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   44
      Left            =   360
      Picture         =   "TM-frmPalette.frx":C6AE
      Top             =   960
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   7
      Left            =   120
      Picture         =   "TM-frmPalette.frx":C8A0
      Top             =   960
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   48
      Left            =   1800
      Picture         =   "TM-frmPalette.frx":CA92
      Top             =   720
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   13
      Left            =   1560
      Picture         =   "TM-frmPalette.frx":CC84
      Top             =   720
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   41
      Left            =   1320
      Picture         =   "TM-frmPalette.frx":CE76
      Top             =   720
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   42
      Left            =   1080
      Picture         =   "TM-frmPalette.frx":D068
      Top             =   720
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   50
      Left            =   840
      Picture         =   "TM-frmPalette.frx":D25A
      Top             =   720
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   43
      Left            =   600
      Picture         =   "TM-frmPalette.frx":D44C
      Top             =   720
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   45
      Left            =   360
      Picture         =   "TM-frmPalette.frx":D63E
      Top             =   720
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   3
      Left            =   120
      Picture         =   "TM-frmPalette.frx":D830
      Top             =   720
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   16
      Left            =   1800
      Picture         =   "TM-frmPalette.frx":DA22
      Top             =   480
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   47
      Left            =   1560
      Picture         =   "TM-frmPalette.frx":DC14
      Top             =   480
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   5
      Left            =   1320
      Picture         =   "TM-frmPalette.frx":DE06
      Top             =   480
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   14
      Left            =   1080
      Picture         =   "TM-frmPalette.frx":DFF8
      Top             =   480
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   10
      Left            =   840
      Picture         =   "TM-frmPalette.frx":E1EA
      Top             =   480
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   12
      Left            =   600
      Picture         =   "TM-frmPalette.frx":E3DC
      Top             =   480
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   46
      Left            =   360
      Picture         =   "TM-frmPalette.frx":E5CE
      Top             =   480
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   9
      Left            =   120
      Picture         =   "TM-frmPalette.frx":E7C0
      Top             =   480
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   56
      Left            =   1800
      Picture         =   "TM-frmPalette.frx":E9B2
      Top             =   240
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   55
      Left            =   1560
      Picture         =   "TM-frmPalette.frx":EBA4
      Top             =   240
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   11
      Left            =   1320
      Picture         =   "TM-frmPalette.frx":ED96
      Top             =   240
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   49
      Left            =   1080
      Picture         =   "TM-frmPalette.frx":EF88
      Top             =   240
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   51
      Left            =   840
      OLEDropMode     =   2  'Automatic
      Picture         =   "TM-frmPalette.frx":F17A
      Tag             =   "C51"
      Top             =   240
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   52
      Left            =   600
      Picture         =   "TM-frmPalette.frx":F36C
      Tag             =   "C52"
      Top             =   240
      Width           =   180
   End
   Begin VB.Image Couleur 
      Height          =   180
      Index           =   53
      Left            =   360
      Picture         =   "TM-frmPalette.frx":F55E
      Tag             =   "C53"
      Top             =   240
      Width           =   180
   End
End
Attribute VB_Name = "frmPalette"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Aucun_Click()

    Shape2.Visible = False
    Aucun.BorderStyle = 1

End Sub

Private Sub Command1_Click()
    RetourColor = -1
    Unload Me

End Sub

Private Sub Couleur_Click(Index As Integer)

Dim aCouleur As Long

Select Case Index
    'Ligne 1
    Case 1
        aCouleur = 0
    Case 53
        aCouleur = 13460
    Case 52
        aCouleur = 13361
    Case 51
        aCouleur = 13312
    Case 49
        aCouleur = 6501376
    Case 11
        aCouleur = 8061952
    Case 55
        aCouleur = 9712689
    Case 56
        aCouleur = 3224625
    'Ligne 2
    Case 9
        aCouleur = 26111
    Case 46
        aCouleur = 34436
    Case 12
        aCouleur = 34304
    Case 10
        aCouleur = 8685056
    Case 14
        aCouleur = 16712704
    Case 5
        aCouleur = 9725283
    Case 47
        aCouleur = 8685188
    Case 16
        aCouleur = 1279
    'Ligne 3
    Case 3
        aCouleur = 40703
    Case 45
        aCouleur = 53148
    Case 43
        aCouleur = 6526513
    Case 50
        aCouleur = 13553447
    Case 42
        aCouleur = 16739641
    Case 41
        aCouleur = 8651908
    Case 13
        aCouleur = 12435133
    Case 48
        aCouleur = 16712959
    'Ligne 4
    Case 7
        aCouleur = 51199
    Case 44
        aCouleur = 65535
    Case 6
        aCouleur = 65280
    Case 4
        aCouleur = 16776960
    Case 8
        aCouleur = 16764672
    Case 33
        aCouleur = 6501524
    Case 54
        aCouleur = 13026246
    Case 15
        aCouleur = 13541119
    'Ligne 5
    Case 38
        aCouleur = 10276863
    Case 40
        aCouleur = 10289151
    Case 36
        aCouleur = 13565902
    Case 35
        aCouleur = 16777166
    Case 34
        aCouleur = 16764828
    Case 37
        aCouleur = 16750278
    Case 39
        aCouleur = 16777215
    Case 2 'Fond Transparent
        aCouleur = 999
        

End Select
        
RetourColor = aCouleur
Unload Me

End Sub

Private Sub Image1_Click()
    RetourColor = -1
    Unload Me
End Sub
