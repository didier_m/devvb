VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "RangChar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'**********************************************
'*                                            *
'* Cette classe contient la d�finition d'un   *
'* rang d'une maquette caract�re              *
'*                                            *
'**********************************************

'-> Nom du Rang
Public Nom As String

'-> Variable de type priv�e qui contient les lignes d'un rang
Private Lignes() As String

'-> Variable qui contient le nombre de lignes + 1 du rang carcat�re
Public nLignes As Integer

'-> Variable de type priv� qui contient la liste des associations
'avec  SECTION-[NomSection] pour une section de texte _
       TABLEAU-[NomTableau]-[NomBlock]
Private Liens() As String
Public nLiens As Integer

'-> Variable qui indique l'index du rang caract�re
Public IdAffichage As Integer

'-> Variable qui indique si c'est un rang de saut de page
Public IsSautPage As Boolean

Private Sub Class_Initialize()
    nLignes = 1
    nLiens = 1
End Sub

Public Sub AddLigne(ByVal LigneToAdd As String)

'---> Fonction qui rajoute une ligne dans la matrice des lignes du rang caract�re
ReDim Preserve Lignes(1 To nLignes)
Lignes(nLignes) = LigneToAdd
nLignes = nLignes + 1

End Sub

Public Function GetLigne(ByVal IdLigne As Integer) As String

On Error Resume Next
'---> fonction qui renvoie le contenu d'une ligne en fonction de son index
GetLigne = Lignes(IdLigne)

End Function

Public Sub AddLien(ByVal LienToAdd As String)

'---> Fonction qui rajoute un lien dans la matrice des liens du rang caract�re
ReDim Preserve Liens(1 To nLiens)
Liens(nLiens) = LienToAdd
nLiens = nLiens + 1

End Sub

Public Function GetLien(ByVal IdLien As Integer) As String

'---> fonction qui renvoie le contenu d'un lien
GetLien = Liens(IdLien)

End Function

Public Function DelLien(ByVal LienToDel As String)

'---> Fonction qui supprime un lien dans la matrice

Dim Matrice() As String
Dim LienCreate As Integer

'-> Tester si on supprime le dernier �l�ment
If nLiens - 1 = 1 Then
    Erase Liens()
    nLiens = 1
    Exit Function
End If

'-> Intialiser la seconde matrice
ReDim Matrice(1 To nLiens - 2)
LienCreate = 1

For i = 1 To nLiens - 1
    If UCase$(Liens(i)) = UCase$(LienToDel) Then
    Else
        Matrice(LienCreate) = Liens(i)
        LienCreate = LienCreate + 1
    End If
Next

'-> Ecraser la matrice
ReDim Liens(1 To nLiens - 1)

'-> Basculer la matrice
For i = 1 To nLiens - 2
    Liens(i) = Matrice(i)
Next

'-> - 1 sur le nombre de lien
nLiens = nLiens - 1

Erase Matrice

End Function

