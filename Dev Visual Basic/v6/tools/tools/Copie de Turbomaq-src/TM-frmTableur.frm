VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTableur 
   Caption         =   "Form1"
   ClientHeight    =   7995
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8880
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   533
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   592
   Begin VB.HScrollBar Horizontal 
      Height          =   255
      Left            =   3840
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   6480
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.VScrollBar Vertical 
      Height          =   1095
      Left            =   7680
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   6000
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox CarretScroll 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   8760
      ScaleHeight     =   17
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   17
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   6240
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox PicTableau 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   3975
      Left            =   240
      ScaleHeight     =   265
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   457
      TabIndex        =   0
      Top             =   960
      Width           =   6855
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   9120
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   80
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTableur.frx":0000
            Key             =   "AddBlock"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTableur.frx":1FDA
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTableur.frx":3FB4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTableur.frx":5F8E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   600
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   8880
      _ExtentX        =   15663
      _ExtentY        =   1058
      ButtonWidth     =   2302
      ButtonHeight    =   1005
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AddLig"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "DelLig"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "DupLig"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "GestOrdre"
            ImageIndex      =   4
         EndProperty
      EndProperty
      Begin VB.TextBox IemBlock 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   5400
         TabIndex        =   5
         Top             =   120
         Width           =   2895
      End
   End
End
Attribute VB_Name = "frmTableur"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public TableauCours As Tableau
'-> Cette variable indique le block de ligne le plus large pour ajuster
Public LargeurTableau As Single
'-> Variable qui indique la position hautre du premier objet block
Private Const HauteurBase = 1 'Exprim�e ne CM

'-> Variable qui indique le bloc sur lequel on a cliqu�
Public NomBlock As String


Private Sub Form_Activate()

Dim aNode As Node

    Set aNode = frmNaviga.TreeNaviga.Nodes(UCase$(TableauCours.Nom))
    Set ObjCours = Me.TableauCours
    aNode.Selected = True
    Set aNode = Nothing
    
    AutoScroll
    
    If frmPropVisible Then frmProp.Form_Load

End Sub

Private Sub Form_Load()

Dim aLb As Libelle

Set aLb = Libelles("FRMTABLEUR")

Me.Caption = aLb.GetCaption(1) & TableauCours.Nom & "]"
Me.Toolbar1.Buttons("AddLig").ToolTipText = aLb.GetCaption(2)
Me.Toolbar1.Buttons("DelLig").ToolTipText = aLb.GetCaption(3)
Me.Toolbar1.Buttons("DupLig").ToolTipText = aLb.GetCaption(6)
Me.Toolbar1.Buttons("GestOrdre").ToolTipText = aLb.GetCaption(8)
Me.IemBlock.ToolTipText = aLb.GetCaption(5)
GestIco

Set aLb = Nothing

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

If EraseFrm Then
Else
    If UnloadMode <> 4 Then Cancel = 1
    Me.Hide
End If

End Sub

Private Sub Form_Resize()
    AutoScroll
End Sub

Private Sub Form_Unload(Cancel As Integer)

Me.PicTableau.Cls
Set Me.PicTableau.Picture = Nothing
Me.PicTableau.AutoRedraw = False



End Sub

Private Sub Horizontal_Change()
    Me.PicTableau.Left = Me.Horizontal.Value
    If Me.Vertical.Visible Then Me.Vertical.ZOrder
End Sub

Private Sub PicTableau_DblClick()

'-> Tester si le nom du block est renseign�
If NomBlock <> "" Then EditBlock

End Sub

Private Sub PicTableau_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

'-> D�terminer sur quel block il vient de cliquer
Dim aBlock As Block

For Each aBlock In Me.TableauCours.Blocks
    Res = PtInRegion(aBlock.hRgn, X, Y)
    If Res <> 0 Then
        NomBlock = aBlock.Nom
        Me.IemBlock.Text = NomBlock
        Exit For
    Else
        NomBlock = ""
    End If
Next


If Button = vbRightButton Then
    RetourMenu = -1
    Me.PopupMenu frmMenu.mnuTableau
    If RetourMenu = 11 Then EditBlock
End If

Set aBlock = Nothing

End Sub

Private Sub PicTableau_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

'-> D�terminer sur quel block il vient de cliquer
Dim aBlock As Block

For Each aBlock In Me.TableauCours.Blocks
    Res = PtInRegion(aBlock.hRgn, X, Y)
    If Res <> 0 Then
        Me.PicTableau.ToolTipText = aBlock.Nom
        Exit For
    End If
Next

Set aBlock = Nothing

End Sub

Private Sub PicTableau_Resize()
    AutoScroll
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

Dim aTb As Tableau
Dim aLb As Libelle
Dim Rep
Dim i As Integer
Dim aBlock As Block, BlockRef As Block


Select Case Button.Key


    Case "AddLig"
        
        Set frmNewBlock.TableauCours = Me.TableauCours
        frmNewBlock.Show vbModal
        GestIco
        Exit Sub
    
    Case "DelLig"
    
        
        '-> Ne rien faire s'il nya pas de block en cours
        If NomBlock = "" Then Exit Sub
    
        '-> Supprimer le block
        Set aBlock = Me.TableauCours.Blocks("BL-" & UCase$(NomBlock))
        If aBlock.IsEdit Then
            Set aLb = Libelles("FRMTABLEUR")
            MsgBox aLb.GetCaption(7), vbInformation + vbOKOnly, aLb.GetToolTip(7)
            Set aLb = Nothing
            Exit Sub
        End If
        
        '-> Demander la confirmation
        Set aLb = Libelles("FRMTABLEUR")
        Rep = MsgBox(aLb.GetCaption(4) & " " & NomBlock, vbQuestion + vbYesNo, aLb.GetToolTip(4))
        If Rep = vbNo Then Exit Sub
        
        '-> Supprimer l'icone
        aBlock.DelTreeView
        
        '-> Supprimer toutes les cellules
        For i = 1 To aBlock.NbCol
            For j = 1 To aBlock.NbLigne
                aBlock.Cellules.Remove ("L" & j & "C" & i)
            Next
        Next
        
        '-> Supprimer l'ordre d'affichage
        Me.TableauCours.DelOrdreAffichage aBlock.IdOrdreAffichage
        
        '-> Test des liens maitres
        If aBlock.MasterLink <> "" Then
            Set BlockRef = Me.TableauCours.Blocks("BL-" & UCase$(aBlock.MasterLink))
            BlockRef.SlaveLink = DeleteEntry(BlockRef.SlaveLink, GetEntryIndex(BlockRef.SlaveLink, aBlock.Nom, "|"), "|")
            Set BlockRef = Nothing
        End If
        
        '-> Test des liens esclaves
        If aBlock.SlaveLink <> "" Then
            For i = 1 To NumEntries(aBlock.SlaveLink, "|")
                Set BlockRef = Me.TableauCours.Blocks("BL-" & UCase$(Entry(i, aBlock.SlaveLink, "|")))
                BlockRef.MasterLink = ""
            Next
        End If

        '-> Supprimer le block dans le tableau parent
        Me.TableauCours.Blocks.Remove ("BL-" & UCase$(aBlock.Nom))
        
        '-> Redessiner le tableur
        DrawTableau Me.TableauCours
        
    Case "DupLig"
    
        '-> Setting du tableau
        Set frmDupBlock.TableauCours = Me.TableauCours
        
        '-> Cr�er la liste des blocks � dupliquer
        For Each aBlock In Me.TableauCours.Blocks
            If aBlock.MasterLink = "" Then frmDupBlock.List1.AddItem aBlock.Nom
        Next
        
        '-> S�lectionner le premier
        If frmDupBlock.List1.ListCount <> 0 Then frmDupBlock.List1.Selected(0) = True
                
        '-> Afficher la feuille
        frmDupBlock.Show vbModal
        
        Exit Sub
        
    Case "GestOrdre"
    
        If Me.TableauCours.Blocks.Count = 0 Then Exit Sub
        LoadOrdreAffichage Me.TableauCours.Blocks(1)
        frmDisplayOrder.Show vbModal
        '-> Redessiner le tableau
        DrawTableau Me.TableauCours
        
        
End Select

AutoScroll
GestIco

Set aTb = Nothing
Set aLb = Nothing
Set aBlock = Nothing
Set BlockRef = Nothing




End Sub

Public Sub AutoScroll()

'---> Cette fonction sert � g�rer les scrolls dans le block

Dim Rect1 As Rect
Dim Rect2 As Rect
Dim Res As Long

On Error Resume Next

'-> Supprimer les crolls s'il n'y a pas de block en cours
If Me.TableauCours.Blocks.Count = 0 Then
    Me.Vertical.Visible = False
    Me.Horizontal.Visible = False
    Me.CarretScroll.Visible = False
    Exit Sub
End If



Dim LargeurManque As Integer
Dim HauteurManque As Integer
Dim LargeurBordure As Integer

'-> Ne rien calculer si le tableau n'est pas visible
If Me.PicTableau.Visible = False Then
    Me.Vertical.Visible = False
    Me.Horizontal.Visible = False
    Me.CarretScroll.Visible = False
    Exit Sub
End If

Me.PicTableau.Left = Me.ScaleX(1, 7, 3)
Me.PicTableau.Top = Me.Toolbar1.Height + Me.ScaleY(1, 7, 3)

'-> R�cup�ration des diff�rentes donn�es
Res = GetClientRect(Me.hwnd, Rect1)
Res = GetWindowRect(Me.PicTableau.hwnd, Rect2)

LargeurBordure = GetSystemMetrics(SM_CXEDGE)

LargeurManque = Rect1.Right - Rect2.Right - LargeurBordure
HauteurManque = Rect1.Bottom - Rect2.Bottom - LargeurBordure

'-> Positionner la barre de defilement horizontale
Me.Horizontal.Left = 0
Me.Horizontal.Top = Rect1.Bottom - Me.Horizontal.Height
Me.Horizontal.Width = (Rect1.Right - Rect1.Left)
    
'-> Positionner la barre de d�filement verticale
Me.Vertical.Left = Rect1.Right - Me.Vertical.Width
Me.Vertical.Top = Me.Toolbar1.Top + Me.Toolbar1.Height
Me.Vertical.Height = Rect1.Bottom - Me.Vertical.Top
    
'-> Ajuster si deux barres sont visibles
If LargeurManque < 0 Then HauteurManque = HauteurManque - Me.Horizontal.Height
If HauteurManque < 0 Then LargeurManque = LargeurManque - Me.Vertical.Width
    
'-> Rendre visible la barre horizontale
If LargeurManque < 0 Then
    Me.Horizontal.Min = Me.ScaleX(1, 7, 3)
    Me.Horizontal.Max = Me.PicTableau.Left + LargeurManque
    Me.Horizontal.Value = Me.PicTableau.Left
    Me.Horizontal.LargeChange = Abs(CInt(LargeurManque) / 5)
    Me.Horizontal.SmallChange = 5
    Me.Horizontal.Visible = True
    Me.Horizontal.ZOrder
Else
    Me.Horizontal.Visible = False
End If

'-> Rendre visible la barre verticale
If HauteurManque < 0 Then
    Me.Vertical.Min = Me.ScaleY(1, 7, 3) + Me.Toolbar1.Height
    Me.Vertical.Max = Me.PicTableau.Top + HauteurManque
    Me.Vertical.Value = Me.PicTableau.Top
    Me.Vertical.LargeChange = Abs(CInt(HauteurManque) / 5)
    Me.Vertical.SmallChange = 5
    Me.Vertical.Visible = True
    Me.Vertical.ZOrder
Else
    Me.Vertical.Visible = False
End If

'-> Modifier si necessaire
If LargeurManque < 0 And HauteurManque < 0 Then
    Me.Vertical.Height = Me.Vertical.Height - Me.Horizontal.Height
    Me.Horizontal.Width = Me.Horizontal.Width - Me.Vertical.Width
    Me.CarretScroll.Top = Me.Vertical.Top + Me.Vertical.Height + 1
    Me.CarretScroll.Left = Me.Horizontal.Left + Me.Horizontal.Width + 1
    'DrawEntete -1, True, False
    Me.CarretScroll.Visible = True
    Me.CarretScroll.ZOrder
Else
    Me.CarretScroll.Visible = False
End If

End Sub


Private Sub Vertical_Change()
    Me.PicTableau.Top = Me.Vertical.Value
    Me.Vertical.ZOrder
    Me.Toolbar1.ZOrder
End Sub


Public Sub GestIco()

'-> Gere la barre d'outils
Select Case Me.TableauCours.Blocks.Count

    Case 0
        
        Me.Toolbar1.Buttons("DelLig").Enabled = False
        Me.Toolbar1.Buttons("DupLig").Enabled = False
        NomBlock = ""
        Me.PicTableau.Visible = False

    Case Else
    
        Me.Toolbar1.Buttons("DelLig").Enabled = True
        Me.Toolbar1.Buttons("DupLig").Enabled = True


End Select

Me.IemBlock.Text = NomBlock

End Sub

Private Sub EditBlock()

'-> Faire l'�dition d'un block

Dim aBlock As Block
Dim aForm As FrmBlock

Set aBlock = Me.TableauCours.Blocks("BL-" & UCase$(NomBlock))
aBlock.NomTb = Me.TableauCours.Nom

'-> Tester si le block n'est pas d�ja �dit�
If aBlock.IsEdit Then
    Set aBlock = Nothing
    Exit Sub
End If

'-> Edit� le block
Set aForm = New FrmBlock

'-> Ajouter dans la collection
Me.TableauCours.frmBlocks.Add aForm, UCase$(aBlock.Nom)
        
aForm.TableauCours = Me.TableauCours.Nom
aForm.BlockCours = aBlock.Nom

'-> Initialiser les blocks
aForm.UpdateBlock

'-> Indiqu� que le block est d�ja �dit�
aBlock.IsEdit = True

aForm.Show


Set aForm = Nothing
Set aBlock = Nothing

End Sub
