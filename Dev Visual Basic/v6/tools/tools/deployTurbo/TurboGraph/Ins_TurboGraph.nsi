; ==========================================================================
; Nom .....: Ins_Turbograph.nsi
; Auteur ..: Ezio TORLONE - JP LAMAILLOUX
; Objet ...: Installation de Turbograph avec parametrage IE6
; Ver.     Date    Auteur                Objet
; ---    --------  ------ ----------------------------------------
; 1.0    06/09/05   E.T   Version de base
; 1.0.1  08/09/05   JPLX  Ajout association .turbo a TurboGraph.exe
; 1.0.2  13/09/05   JPLX  Ajout association .turbo a TurboGraph.exe dans IE
; 1.0.3  03/01/06   JPLX  Maj TurboGraph.exe 2.5.2
; 1.0.4  26/01/06   JPLX  Maj TurboGraph.exe 2.5.4
; 1.0.5  20/02/06   JPLX  Maj TurboGraph.exe 2.5.6
; 1.0.6  05/04/06   JPLX  Maj TurboGraph.exe 2.5.9
; 1.0.7  29/05/06   JPLX  Maj TurboGraph.exe 2.6.1
; 1.0.7  01/06/06   JPLX  Maj TurboGraph.exe 2.6.1 + open tubo choix imprimante
; 1.0.8  28/07/06   JPLX  Maj TurboGraph.exe 2.6.3 telechargement images de http://monsit.fr/deallogo
; 1.0.9  03/08/06   JPLX  Maj TurboGraph.exe 2.6.4
; 1.1.1  18/12/06   JPLX  Maj TurboGraph.exe 2.6.5 Ajout PDF et turbopdf.exe
; 1.1.2  24/01/07   JPLX  Maj TurboGraph.exe 2.6.6 Ajout impression pour le web
; 1.2.0  07/02/07   JPLX  Maj TurboGraph.exe 2.7.0
; 1.2.1  14/03/07   JPLX  Maj TurboGraph.exe 2.7.3
; 1.2.2  26/03/07   JPLX  Maj TurboGraph.exe 2.7.6
; 1.2.3  11/05/07   JPLX  Maj TurboGraph.exe 2.7.8
; 1.2.4  22/05/07   JPLX  Maj TurboGraph.exe 2.7.9
; 1.2.5  25/06/07   JPLX  Maj TurboGraph.exe 2.8.0
; 1.3.0  17/10/07   JPLX  Maj TurboGraph.exe 2.9.0
; 1.3.1  08/11/07   JPLX  Maj TurboGraph.exe 2.9.1
; 1.3.2  08/11/07   JPLX  Maj TurboGraph.exe 2.9.3
; 1.3.3  10/12/07   JPLX  Maj TurboGraph.exe 2.9.4
; 1.3.4  18/12/07   JPLX  Maj TurboGraph.exe 2.9.5
; 1.3.5  17/01/08   JPLX  Maj TurboGraph.exe 2.9.7
; 1.3.6  08/02/08   JPLX  Maj TurboGraph.exe 2.9.8
; 1.3.7  10/03/08   JPLX  Maj TurboGraph.exe 2.9.9
; 1.4.0  11/03/08   JPLX  Maj TurboGraph.exe 3.0.0
; 1.4.1  13/05/08   JPLX  Maj TurboGraph.exe 3.0.2
; 1.4.2  20/05/08   JPLX  Maj TurboGraph.exe 3.0.3
; 1.4.2  26/06/08   JPLX  Maj TurboGraph.exe 3.0.4
; 1.4.3  05/08/08   JPLX  Maj TurboGraph.exe 3.0.5
; 1.4.4  15/09/08   JPLX  Maj TurboGraph.exe 3.0.8
; ==========================================================================

!define PRODUIT "turbograph"
!define DOSSIER "Turbo-print"
!define VERSION "3.0.8"
!define COMPANYNAME "Deal Informatique"
!define LEGALCOPYRIGHT "� Deal Informatique"

Name "${PRODUIT} ${VERSION}"

;!include "MUI.nsh"
!include "UMUI.nsh"


;--------------------------------
;Configuration

  ;General
  OutFile ins_${PRODUIT}.exe
  XPStyle on
  ;InstallColors 0xFFFFFF 0x3d66ab
;--------------------------------
;Interface Settings

        !define MUI_ABORTWARNING
	!define MUI_UNABORTWARNING

	!define UMUI_USE_ALTERNATE_PAGE
	!define UMUI_USE_UNALTERNATE_PAGE

;--------------------------------
;Page

  !insertmacro MUI_DEFAULT UMUI_HEADERIMAGE_BMP outils\deal-header.bmp
  !insertmacro MUI_DEFAULT UMUI_LEFTIMAGE_BMP outils\deal.bmp

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "License.txt"
;  !insertmacro MUI_PAGE_COMPONENTS  ( Selection des sections )
;  !insertmacro MUI_PAGE_DIRECTORY   ( Selection du repertoire d'installation )

   Page custom CustomPageA

  !insertmacro MUI_PAGE_INSTFILES
  
        !define MUI_FINISHPAGE_LINK "Site DEAL Informatique"
        !define MUI_FINISHPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro MUI_PAGE_FINISH

        !define UMUI_ABORTPAGE_LINK "Site DEAL Informatique"
	!define UMUI_ABORTPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro UMUI_PAGE_ABORT

  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

	!define MUI_FINISHPAGE_LINK "Site DEAL Informatique"
	!define MUI_FINISHPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro MUI_UNPAGE_FINISH

	!define UMUI_ABORTPAGE_LINK "Site DEAL Informatique"
	!define UMUI_ABORTPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro UMUI_UNPAGE_ABORT


;--------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "French"

;--------------------------------
;Reserve Files

  ;These files should be inserted before other files in the data block
  ;Keep these lines before any File command
  ;Only for solid compression (by default, solid compression is enabled for BZIP2 and LZMA)

  ReserveFile "deal.ini"
  !insertmacro MUI_RESERVEFILE_INSTALLOPTIONS

;--------------------------------
; Informations sur l'executable

VIAddVersionKey /LANG=${LANG_FRENCH} "ProductName" "${PRODUIT}"
VIAddVersionKey /LANG=${LANG_FRENCH} "Comments" "Programme de gestion des �ditions"
VIAddVersionKey /LANG=${LANG_FRENCH} "CompanyName" "${COMPANYNAME}"
VIAddVersionKey /LANG=${LANG_FRENCH} "LegalTrademarks" "${PRODUIT} est une marque d�pos�e de ${COMPANYNAME}"
VIAddVersionKey /LANG=${LANG_FRENCH} "LegalCopyright" "${LEGALCOPYRIGHT}"
VIAddVersionKey /LANG=${LANG_FRENCH} "FileDescription" "${PRODUIT} ${VERSION}"
VIAddVersionKey /LANG=${LANG_FRENCH} "FileVersion" "${VERSION}"
VIProductVersion "${VERSION}.0"

;============================================
;Fonction pour connaitre la version de windows
;============================================
Function GetWindowsVersion
  Push $0
  ReadRegStr $0 HKLM "SOFTWARE\Microsoft\Windows NT\CurrentVersion" CurrentVersion
  StrCmp $0 "" 0 lbl_winnt
  ; On est dans une arborescence de style Windows 9x ( 95,98,ME )
    StrCpy $0 '9x'
  Goto lbl_done
  lbl_winnt:
  ; On est dans une arborescence de style NT ( NT 4, NT 5, 2000, XP )
    Strcpy $0 'NT'
  lbl_done:
;  Exch $0

FunctionEnd


;======================================================
;Fonction pour connaitre Le Framework .NET est install�
;======================================================
; IsDotNETInstalled
;
; Usage:
;   Call IsDotNETInstalled
;   Pop $0
;   StrCmp $0 1 found.NETFramework no.NETFramework

Function IsDotNETInstalled
   Push $0
   Push $1
   Push $2
   Push $3
   Push $4

   ReadRegStr $4 HKEY_LOCAL_MACHINE \
     "Software\Microsoft\.NETFramework" "InstallRoot"
   # supprime le back slash de fin
   Push $4
   Exch $EXEDIR
   Exch $EXEDIR
   Pop $4
   # si le r�pertoire racine n'existe pas, .NET n'est pas install�
   IfFileExists $4 0 noDotNET

   StrCpy $0 0

   EnumStart:

     EnumRegKey $2 HKEY_LOCAL_MACHINE \
       "Software\Microsoft\.NETFramework\Policy"  $0
     IntOp $0 $0 + 1
     StrCmp $2 "" noDotNET

     StrCpy $1 0

    EnumPolicy:

       EnumRegValue $3 HKEY_LOCAL_MACHINE \
         "Software\Microsoft\.NETFramework\Policy\$2" $1
       IntOp $1 $1 + 1
        StrCmp $3 "" EnumStart
         IfFileExists "$4\$2.$3" foundDotNET EnumPolicy

   noDotNET:
     StrCpy $0 0
     Goto done

   foundDotNET:
     StrCpy $0 1

   done:
     Pop $4
     Pop $3
     Pop $2
     Pop $1
     Exch $0
 FunctionEnd



LangString TEXT_IO_TITLE ${LANG_FRENCH} "Selection du site pour les images specifiques"
LangString TEXT_IO_SUBTITLE ${LANG_FRENCH} "Ex: http://www.monsite.fr"

Function CustomPageA

    !insertmacro MUI_HEADER_TEXT "$(TEXT_IO_TITLE)" "$(TEXT_IO_SUBTITLE)"
  !insertmacro MUI_INSTALLOPTIONS_DISPLAY "deal.ini"

    ReadINIStr $0 "$PLUGINSDIR\deal.ini" "Field 2" "State"
    StrCpy $1 $0
    DetailPrint "Info=$1"
  

FunctionEnd

; *****************************
;   Repertoire d'installation
; *****************************

InstallDir $ProgramFiles\Turbo-print

; =============================
;   Installation de TurboGraph
; =============================

Section "TurboGraph" SecTurbograph
ClearErrors

  CopyFiles $INSTDIR\TurboGraph.ini $INSTDIR\TurboGraph.inisov

  SetOutPath $INSTDIR\internet
  File html\*.*

  SetOutPath $INSTDIR
  File setup\*.*

  SetOutPath $INSTDIR\images
  File images\*.*

  WriteINIStr $INSTDIR\TurboGraph.ini SITE HTTP "$1"

  ; Inscription du repertoire d'installation
  WriteRegStr HKLM "Software\Turbo-print" "" $INSTDIR

 ; Inscription des cles de desinstallation
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Turbo-print" "DisplayName" "Turbo-print Deal ${VERSION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Turbo-print" "DisplayVersion" "${VERSION}"
  ; WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Turbo-print" "InstallDate" "$0$1$2"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Turbo-print" "UninstallString" '"$INSTDIR\tb-uninst.exe"'

  ; Creation de l'executable de desinstallation
  WriteUninstaller "$INSTDIR\tb-uninst.exe"

SectionEnd


; ===============================================
;   Installation de RuntimeVB6 & ocx et autrs DLL
; ===============================================

 !include Library.nsh

 Var ALREADY_INSTALLED

 Section "-Install VB6 runtimes"

   ; Test si Turbograph est deja installe
   
   IfFileExists "$INSTDIR\TurboGraph.exe" 0 new_installation
     StrCpy $ALREADY_INSTALLED 1
   new_installation:

   ; vb6
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msvbvm60.dll" "$SYSDIR\msvbvm60.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_PROTECTED    "dll\oleaut32.dll" "$SYSDIR\oleaut32.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_PROTECTED    "dll\olepro32.dll" "$SYSDIR\olepro32.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_PROTECTED    "dll\comcat.dll"   "$SYSDIR\comcat.dll"   "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_PROTECTED    "dll\asycfilt.dll" "$SYSDIR\asycfilt.dll" "$SYSDIR"
   !insertmacro InstallLib TLB    $ALREADY_INSTALLED REBOOT_PROTECTED    "dll\stdole2.tlb"  "$SYSDIR\stdole2.tlb"  "$SYSDIR"

   ; autres
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\cmct2fr.dll"  "$SYSDIR\cmct2fr.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\cmdlgfr.dll"  "$SYSDIR\cmdlgfr.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\mscmcfr.dll"  "$SYSDIR\mscmcfr.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msmpifr.dll"  "$SYSDIR\msmpifr.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\rchtxfr.dll"  "$SYSDIR\rchtxfr.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\riched32.dll" "$SYSDIR\riched32.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\tabctfr.dll"  "$SYSDIR\tabctfr.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\unzip32.dll"  "$SYSDIR\unzip32.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\vb6fr.dll"    "$SYSDIR\vb6fr.dll"    "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\vb6stkit.dll" "$SYSDIR\vb6stkit.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msvcrt40.dll" "$SYSDIR\msvcrt40.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msvbvm50.dll" "$SYSDIR\msvbvm50.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\dsofile.dll"  "$SYSDIR\dsofile.dll" "$SYSDIR"

   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msjet35.dll"  "$SYSDIR\msjet35.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\dao350.dll"   "$SYSDIR\dao350.dll"   "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msjint35.dll" "$SYSDIR\msjint35.dll" "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\vbajet32.dll" "$SYSDIR\vbajet32.dll" "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\expsrv.dll"   "$SYSDIR\expsrv.dll"   "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msrd2x35.dll" "$SYSDIR\msrd2x35.dll" "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msrepl35.dll" "$SYSDIR\msrepl35.dll" "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\vb5db.dll"    "$SYSDIR\vb5db.dll"    "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\cmctlfr.dll"  "$SYSDIR\cmctlfr.dll"  "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msch2fr.dll"  "$SYSDIR\msch2fr.dll"  "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\flxgdfr.dll"  "$SYSDIR\flxgdfr.dll"  "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msmskfr.dll"  "$SYSDIR\msmskfr.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msexcl35.dll" "$SYSDIR\msexcl35.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msexch35.dll" "$SYSDIR\msexch35.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\mshfgfr.dll"  "$SYSDIR\mshfgfr.dll"  "$SYSDIR"
 
   ; ocx
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\comct232.ocx" "$SYSDIR\comct232.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\comdlg32.ocx" "$SYSDIR\comdlg32.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\mscomctl.ocx" "$SYSDIR\mscomctl.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\msmapi32.ocx" "$SYSDIR\msmapi32.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\richtx32.ocx" "$SYSDIR\richtx32.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\tabctl32.ocx" "$SYSDIR\tabctl32.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\dealgrid.ocx" "$SYSDIR\dealgrid.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\flash.ocx"    "$SYSDIR\flash.ocx"    "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\dealskin.ocx" "$SYSDIR\dealskin.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\dogskin.ocx"  "$SYSDIR\dogskin.ocx"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\pstimer.ocx"  "$SYSDIR\pstimer.ocx"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\mschrt20.ocx" "$SYSDIR\mschrt20.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\msflxgrd.ocx" "$SYSDIR\msflxgrd.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\msmask32.ocx" "$SYSDIR\msmask32.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\comctl32.ocx" "$SYSDIR\comctl32.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\mshflxgd.ocx" "$SYSDIR\mshflxgd.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\mscomct2.ocx" "$SYSDIR\mscomct2.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\MSWINSCK.OCX" "$SYSDIR\MSWINSCK.OCX" "$SYSDIR"

 SectionEnd


 Section "-un.Uninstall VB6 runtimes"

   !insertmacro UnInstallLib REGDLL SHARED NOREMOVE "$SYSDIR\msvbvm60.dll"
   !insertmacro UnInstallLib REGDLL SHARED NOREMOVE "$SYSDIR\oleaut32.dll"
   !insertmacro UnInstallLib REGDLL SHARED NOREMOVE "$SYSDIR\olepro32.dll"
   !insertmacro UnInstallLib REGDLL SHARED NOREMOVE "$SYSDIR\comcat.dll"
   !insertmacro UnInstallLib DLL    SHARED NOREMOVE "$SYSDIR\asycfilt.dll"
   !insertmacro UnInstallLib TLB    SHARED NOREMOVE "$SYSDIR\stdole2.tlb"

 SectionEnd
  
; ===================================================
;   Fin installation de RuntimeVB6 & ocx et autrs DLL
; ===================================================
  
 ; ==================================================================
;   Associaton extension .turbo a TurboGraph.exe et ouverture dans ie
; ===================================================================

Section "AssocTurbo" SecAssocTurbo
  
   Call GetWindowsVersion
   StrCmp $0 "9x" assoctw9x 0
   ; Windows NT,2000,XP
   DeleteRegKey HKCR turbofile
   WriteRegStr HKCR .turbo "" "turbofile"
   WriteRegStr HKCR turbofile "" ""
   WriteRegBin HKCR turbofile "Edit Flags" 0
   WriteRegBin HKCR turbofile "Browser Flags" 8
   WriteRegStr HKCR turbofile\Shell "" ""
   WriteRegStr HKCR turbofile\Shell\open "" ""
;   WriteRegStr HKCR turbofile\Shell\open\command "" '"$INSTDIR\TurboGraph.exe" "%1"'
   DeleteRegKey HKCR turbofile\Shell\open\command
   WriteRegStr HKCR turbofile\Shell\open\command "" '"$INSTDIR\TurboGraph.exe" "GETPRINTERS~SCREEN|%1%"'
   WriteRegStr HKCR Applications\TurboGraph.EXE "" ""
   WriteRegStr HKCR Applications\TurboGraph.EXE\Shell "" ""
   WriteRegStr HKCR Applications\TurboGraph.EXE\Shell\open "" ""
;   WriteRegStr HKCR Applications\TurboGraph.EXE\Shell\open\command "" '"$INSTDIR\TurboGraph.exe" "%1%"'
   WriteRegStr HKCR Applications\TurboGraph.EXE\Shell\open\command "" '"$INSTDIR\TurboGraph.exe" "GETPRINTERS~SCREEN|%1%"'
   WriteRegStr HKLM SOFTWARE\Classes\.turbo "" "turbofile"
   WriteRegStr HKLM SOFTWARE\Classes\turbofile "" ""
   WriteRegBin HKLM SOFTWARE\Classes\turbofile "Edit Flags" 0
   WriteRegBin HKLM SOFTWARE\Classes\turbofile "Browser Flags" 8
   WriteRegStr HKLM SOFTWARE\Classes\turbofile\Shell "" ""
   WriteRegStr HKLM SOFTWARE\Classes\turbofile\Shell\open "" ""
   WriteRegStr HKLM SOFTWARE\Classes\turbofile\Shell\open\command "" '"$INSTDIR\TurboGraph.exe" "GETPRINTERS~SCREEN|%1%"'
   WriteRegStr HKLM SOFTWARE\Classes\Applications\TurboGraph.EXE "" ""
   WriteRegStr HKLM SOFTWARE\Classes\Applications\TurboGraph.EXE\Shell "" ""
   WriteRegStr HKLM SOFTWARE\Classes\Applications\TurboGraph.EXE\Shell\open "" ""
   WriteRegStr HKLM SOFTWARE\Classes\Applications\TurboGraph.EXE\Shell\open\command "" '"$INSTDIR\TurboGraph.exe" "GETPRINTERS~SCREEN|%1%"'
   WriteRegStr HKCU SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.turbo "Application" "TurboGraph.exe"
   WriteRegStr HKCU SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.turbo\OpenWithList "a" "TurboGraph.exe"
   WriteRegStr HKCU SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.turbo\OpenWithList "MRUList" "a"
   goto finassoct
   assoctw9x:
   ; Windows 9x ( 95,98,ME )
   DeleteRegKey HKCR turbofile
   WriteRegStr HKCR .turbo "" "turbofile"
   WriteRegStr HKCR turbofile "" "Spools Turbo"
   WriteRegBin HKCR turbofile "Edit Flags" 0
   WriteRegStr HKCR turbofile\Shell "" ""
   WriteRegStr HKCR turbofile\Shell\open "" ""
   WriteRegStr HKCR turbofile\Shell\open\command "" '"$INSTDIR\TurboGraph.exe GETPRINTERS~SCREEN|%1"'
   WriteRegStr HKLM Software\CLASSES\.turbo "" "turbofile"
   WriteRegStr HKLM Software\CLASSES\turbofile "" "Spools Turbo"
   WriteRegBin HKLM Software\CLASSES\turbofile "Edit Flags" 0
   WriteRegStr HKLM Software\CLASSES\turbofile\Shell "" ""
   WriteRegStr HKLM Software\CLASSES\turbofile\Shell\open "" ""
   WriteRegStr HKLM Software\CLASSES\turbofile\Shell\open\command "" '"$INSTDIR\TurboGraph.exe GETPRINTERS~SCREEN|%1"'
    finassoct:

SectionEnd


; ; ============================================
; ;   D�sinstallation
; ; ============================================
; 
 Section "Uninstall"

 ;ADD YOUR OWN FILES HERE...

  Delete "$INSTDIR\Uninstall.exe"

  Delete "$INSTDIR\images\*.*"
  Delete "$INSTDIR\internet\*.*"
  RMDir "$INSTDIR\images"
  RMDir "$INSTDIR\internet"

  Delete "$INSTDIR\*.*"
  
  RMDir "$INSTDIR"

  DeleteRegKey /ifempty HKLM "Software\Turbo-print"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Turbo-print"


 SectionEnd


 Function .onInit

    Call GetWindowsVersion
    StrCmp $0 "9x" SuitInit 0

    ; Sur XP et 2000 l'utilisateur doit �tre administrateur dur la machine pour installer ce KIT
    UserInfo::GetName
    Pop $0
    UserInfo::GetAccountType
    Pop $1
    StrCmp $1 "Admin" SuitInit 0
    Messagebox MB_ICONSTOP \
    "Vous ($0) n'�tes pas administrateur de votre machine. Echec de l'installation du TurboGraph. Merci de contacter votre correspondant informatique."
    Abort

SuitInit:

   Call IsDotNETInstalled
   Pop $0
   StrCmp $0 1 found.NETFramework no.NETFramework

found.NETFramework:
  ; Messagebox MB_ICONINFORMATION
  ;  "Le Framework .NET est install� ($0) "

    Goto SuitInit1

no.NETFramework:
	MessageBox MB_ICONINFORMATION  \
        "Le Framwork .NET n'est pas install� sur votre ordinateur .$\r$\nPour b�n�ficier des derni�res modifications (Edition PDF TurboPdf)$\n$\rInstaller le Framework .NET."

SuitInit1:
   !insertmacro MUI_INSTALLOPTIONS_EXTRACT "deal.ini"
   
 FunctionEnd
