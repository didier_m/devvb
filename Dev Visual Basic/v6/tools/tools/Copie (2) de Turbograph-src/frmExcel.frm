VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form frmExcel 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Export vers Excel"
   ClientHeight    =   6255
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5745
   Icon            =   "frmExcel.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6255
   ScaleWidth      =   5745
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   4680
      Picture         =   "frmExcel.frx":0ECA
      Style           =   1  'Graphical
      TabIndex        =   44
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton Command3 
      Height          =   375
      Left            =   5160
      Picture         =   "frmExcel.frx":1454
      Style           =   1  'Graphical
      TabIndex        =   43
      Top             =   0
      Width           =   375
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   6015
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Width           =   5475
      _ExtentX        =   9657
      _ExtentY        =   10610
      _Version        =   393216
      Style           =   1
      Tabs            =   4
      Tab             =   3
      TabsPerRow      =   4
      TabHeight       =   520
      WordWrap        =   0   'False
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "frmExcel.frx":19DE
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Image1"
      Tab(0).Control(1)=   "Image3"
      Tab(0).Control(2)=   "Label1"
      Tab(0).Control(3)=   "Frame3"
      Tab(0).Control(4)=   "Frame2"
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "frmExcel.frx":19FA
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Label5"
      Tab(1).Control(1)=   "TreeView1"
      Tab(1).Control(2)=   "ImageList1"
      Tab(1).Control(3)=   "Frame1"
      Tab(1).ControlCount=   4
      TabCaption(2)   =   "Tab 2"
      TabPicture(2)   =   "frmExcel.frx":1A16
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame8"
      Tab(2).Control(1)=   "Frame6"
      Tab(2).Control(2)=   "Frame5"
      Tab(2).Control(3)=   "Frame4"
      Tab(2).ControlCount=   4
      TabCaption(3)   =   "Tab 3"
      TabPicture(3)   =   "frmExcel.frx":1A32
      Tab(3).ControlEnabled=   -1  'True
      Tab(3).Control(0)=   "Command2"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "Frame7"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).Control(2)=   "Check7"
      Tab(3).Control(2).Enabled=   0   'False
      Tab(3).Control(3)=   "Check8"
      Tab(3).Control(3).Enabled=   0   'False
      Tab(3).ControlCount=   4
      Begin VB.CheckBox Check8 
         Height          =   255
         Left            =   2160
         TabIndex        =   42
         Top             =   5400
         Value           =   1  'Checked
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.CheckBox Check7 
         Height          =   255
         Left            =   240
         TabIndex        =   39
         Top             =   5400
         Value           =   1  'Checked
         Width           =   2055
      End
      Begin VB.Frame Frame7 
         Height          =   4815
         Left            =   240
         TabIndex        =   36
         Top             =   480
         Width           =   4935
         Begin VB.PictureBox Picture1 
            AutoRedraw      =   -1  'True
            Height          =   255
            Left            =   1560
            ScaleHeight     =   195
            ScaleWidth      =   3075
            TabIndex        =   37
            Top             =   4440
            Width           =   3135
         End
         Begin MSComctlLib.TreeView TreeView2 
            Height          =   3975
            Left            =   240
            TabIndex        =   38
            Top             =   360
            Width           =   4455
            _ExtentX        =   7858
            _ExtentY        =   7011
            _Version        =   393217
            LabelEdit       =   1
            Style           =   7
            ImageList       =   "ImageList1"
            Appearance      =   1
            Enabled         =   0   'False
         End
         Begin VB.Label Label12 
            Height          =   255
            Left            =   240
            TabIndex        =   41
            Top             =   4440
            Width           =   1215
         End
      End
      Begin VB.Frame Frame8 
         Height          =   855
         Left            =   -74760
         TabIndex        =   32
         Top             =   2520
         Width           =   4935
         Begin VB.TextBox Text5 
            Enabled         =   0   'False
            Height          =   285
            Left            =   240
            TabIndex        =   34
            Text            =   "Page1"
            Top             =   360
            Width           =   3015
         End
         Begin VB.CheckBox Check4 
            Enabled         =   0   'False
            Height          =   255
            Left            =   120
            TabIndex        =   33
            Top             =   0
            Width           =   3135
         End
      End
      Begin VB.Frame Frame6 
         Height          =   855
         Left            =   -74760
         TabIndex        =   29
         Top             =   4560
         Width           =   4935
         Begin VB.TextBox Text4 
            Height          =   285
            Left            =   240
            TabIndex        =   31
            Top             =   360
            Width           =   3015
         End
         Begin VB.CheckBox Check3 
            Height          =   255
            Left            =   120
            TabIndex        =   30
            Top             =   0
            Width           =   2535
         End
      End
      Begin VB.Frame Frame5 
         Height          =   975
         Left            =   -74760
         TabIndex        =   27
         Top             =   3480
         Width           =   4935
         Begin VB.CheckBox Check2 
            Enabled         =   0   'False
            Height          =   495
            Left            =   120
            TabIndex        =   35
            Top             =   0
            Width           =   3375
         End
         Begin VB.TextBox Text3 
            Enabled         =   0   'False
            Height          =   285
            Left            =   240
            TabIndex        =   28
            Top             =   600
            Width           =   3015
         End
      End
      Begin VB.Frame Frame4 
         Height          =   1935
         Left            =   -74760
         TabIndex        =   24
         Top             =   480
         Width           =   4935
         Begin VB.CheckBox Check1 
            Height          =   255
            Left            =   120
            TabIndex        =   25
            Top             =   0
            Width           =   2655
         End
         Begin MSComDlg.CommonDialog CommonDialog1 
            Left            =   3120
            Top             =   1320
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   393216
         End
         Begin VB.Image Image2 
            Height          =   720
            Left            =   4200
            Picture         =   "frmExcel.frx":1A4E
            Top             =   1080
            Width           =   720
         End
         Begin VB.Label Label4 
            Height          =   855
            Left            =   120
            TabIndex        =   26
            Top             =   360
            Width           =   4575
         End
      End
      Begin VB.Frame Frame1 
         Enabled         =   0   'False
         Height          =   2415
         Left            =   -74760
         TabIndex        =   15
         Top             =   3480
         Width           =   4935
         Begin VB.CheckBox Check6 
            Height          =   255
            Left            =   240
            TabIndex        =   19
            Top             =   1920
            Width           =   2775
         End
         Begin VB.CheckBox Check5 
            Height          =   255
            Left            =   240
            TabIndex        =   18
            Top             =   1560
            Width           =   2775
         End
         Begin VB.Label Label11 
            Height          =   255
            Left            =   1320
            TabIndex        =   23
            Top             =   1080
            Width           =   855
         End
         Begin VB.Label Label10 
            Height          =   255
            Left            =   240
            TabIndex        =   22
            Top             =   1080
            Width           =   615
         End
         Begin VB.Label Label9 
            Height          =   255
            Left            =   1320
            TabIndex        =   21
            Top             =   720
            Width           =   3375
         End
         Begin VB.Label Label8 
            Height          =   255
            Left            =   240
            TabIndex        =   20
            Top             =   720
            Width           =   735
         End
         Begin VB.Label Label7 
            Height          =   255
            Left            =   1320
            TabIndex        =   17
            Top             =   360
            Width           =   3375
         End
         Begin VB.Label Label6 
            Height          =   255
            Left            =   240
            TabIndex        =   16
            Top             =   360
            Width           =   855
         End
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   -71400
         Top             =   720
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   3
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmExcel.frx":2918
               Key             =   "SELECTED"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmExcel.frx":2A72
               Key             =   "UNSELECTED"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmExcel.frx":2BCC
               Key             =   "TYPE"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.TreeView TreeView1 
         Height          =   2415
         Left            =   -74760
         TabIndex        =   14
         Top             =   960
         Width           =   4935
         _ExtentX        =   8705
         _ExtentY        =   4260
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin VB.CommandButton Command2 
         Height          =   375
         Left            =   4080
         TabIndex        =   0
         Top             =   5400
         Width           =   1095
      End
      Begin VB.Frame Frame2 
         Height          =   1455
         Left            =   -74760
         TabIndex        =   1
         Top             =   720
         Width           =   4275
         Begin VB.TextBox Text2 
            Height          =   285
            Left            =   2760
            TabIndex        =   6
            Top             =   720
            Width           =   495
         End
         Begin VB.TextBox Text1 
            Height          =   285
            Left            =   1800
            TabIndex        =   5
            Top             =   720
            Width           =   495
         End
         Begin VB.OptionButton Option1 
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   3
            Top             =   360
            Value           =   -1  'True
            Width           =   2295
         End
         Begin VB.OptionButton Option1 
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   4
            Top             =   720
            Width           =   1095
         End
         Begin VB.OptionButton Option1 
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   7
            Top             =   1080
            Width           =   2295
         End
         Begin VB.Label Label3 
            Height          =   255
            Left            =   2400
            TabIndex        =   12
            Top             =   720
            Width           =   255
         End
         Begin VB.Label Label2 
            Height          =   255
            Left            =   1320
            TabIndex        =   11
            Top             =   720
            Width           =   375
         End
      End
      Begin VB.Frame Frame3 
         Height          =   1455
         Left            =   -74760
         TabIndex        =   2
         Top             =   2280
         Width           =   4335
         Begin VB.OptionButton Option8 
            Height          =   195
            Index           =   0
            Left            =   240
            TabIndex        =   8
            Top             =   360
            Value           =   -1  'True
            Width           =   3735
         End
         Begin VB.OptionButton Option8 
            Height          =   435
            Index           =   1
            Left            =   240
            TabIndex        =   9
            Top             =   720
            Width           =   3975
         End
      End
      Begin VB.Label Label1 
         Height          =   735
         Left            =   -74040
         TabIndex        =   40
         Top             =   4080
         Width           =   3495
      End
      Begin VB.Image Image3 
         Height          =   240
         Left            =   -74760
         Picture         =   "frmExcel.frx":2D26
         Top             =   4080
         Width           =   240
      End
      Begin VB.Label Label5 
         Height          =   255
         Left            =   -74760
         TabIndex        =   13
         Top             =   600
         Width           =   2535
      End
      Begin VB.Image Image1 
         Appearance      =   0  'Flat
         Height          =   720
         Left            =   -70440
         Picture         =   "frmExcel.frx":2E70
         Top             =   840
         Width           =   720
      End
   End
End
Attribute VB_Name = "frmExcel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'-> Indique le fichier en cours de traitement
Public FichierName As String

'-> Indique la cl� du spool actuel
Public SpoolKey As String

'-> Variables de travail
Dim SelectSpool As Integer
Dim SelectSheet As Integer

'-> Pour export vers Excel
Dim ColDep As Integer
Dim RowDep As Integer
Dim AdresseRange As String
Dim ExcelApp As Object 'Excel.Application
Dim aFeuille As Worksheet
Dim aClasseur As Workbook
Dim pClasseurRef As Boolean
Dim pNomFeuille As Boolean
Dim pCelluleDep As Boolean
Dim pName As Boolean
Dim isCurExport As String

Dim TempFileName As String
Dim hdlFile As Integer


Private Function GetBlockPtr() As Block

Dim aTb As Tableau
Dim aBlock As Block
Dim aFichier As Fichier
Dim aSpool As Spool

'-> Pointer sur l'objet fichier
Set aFichier = Fichiers(Me.FichierName)
'-> Pointer sur le spool sp�cifi�
Set aSpool = aFichier.Spools(Me.SpoolKey)
'-> Pointer sur le tableau
Set aTb = aSpool.maquette.Tableaux(UCase$(Trim(Me.Label7.Caption)))
'-> Pointer sur le block
Set aBlock = aTb.Blocks(UCase$(Trim(Me.Label9.Caption)))

Set GetBlockPtr = aBlock

End Function

Private Sub Check5_Click()

Dim aNode As Node
Dim aBlock As Block

'-> Get d'un pointeur vers le block
Set aBlock = GetBlockPtr()

'-> Mettre � jour la propri�t�
aBlock.SetExportExcel CInt(Me.Label11.Caption), CBool(Me.Check5.Value)

'-> D�cocher la propri�t� d'export unique si on n'exporte pas
If Not CBool(Me.Check5.Value) Then
    aBlock.SetExportUniqueExcel CInt(Me.Label11.Caption), False
    '-> D�bloquer la propri�t�
    Me.Check6.Value = 0
End If

'-> Mettre � jour l'icone
Set aNode = Me.TreeView1.Nodes(Me.Label7.Caption & "�" & Me.Label9.Caption & "�" & Me.Label11.Caption)
If CBool(Me.Check5.Value) Then
    aNode.Image = "SELECTED"
Else
    aNode.Image = "UNSELECTED"
End If


End Sub

Private Sub Check6_Click()

Dim aBlock As Block

'-> Get d'un pointeur vers le blokc
Set aBlock = GetBlockPtr

'-> Modifier sa propri�t�
aBlock.SetExportUniqueExcel CInt(Me.Label11.Caption), CBool(Me.Check6.Value)


End Sub

Public Sub Init()

Dim aNode As Node
Dim aNode2 As Node
Dim aNode3 As Node
Dim aTb As Tableau
Dim aBlock As Block
Dim aFichier As Fichier
Dim aSpool As Spool
Dim i As Integer
Dim aLb As Libelle

'-> Pointer sur la gestion des messages
Set aLb = Libelles("FRMEXCEL")

'-> Par d�faut on exporte le spool en entier
SelectSpool = 1

'-> Pointer sur l'objet Fichier
Set aFichier = Fichiers(Me.FichierName)

'-> Pointer sur le spool sp�cifi�
Set aSpool = aFichier.Spools(Me.SpoolKey)

'-> Charger la liste de tous les block de donn�es dans le treeview
For Each aTb In aSpool.maquette.Tableaux
    '-> Ajouter le node du tableau
    Set aNode = Me.TreeView1.Nodes.add(, , UCase$(Trim(aTb.Nom)), aLb.GetCaption(9) & aTb.Nom, "TYPE")
    aNode.Tag = "TB"
    aNode.Expanded = True
    '-> Pour tous les blocks dans ce tableau
    For Each aBlock In aTb.Blocks
        '-> Ajouter le node du block
        Set aNode2 = Me.TreeView1.Nodes.add(aNode.Key, 4, aNode.Key & "�BL-" & aBlock.Nom, aBlock.Nom, "TYPE")
        aNode2.Tag = "BLOCK"
        aNode2.Expanded = True
        '-> Ajouter un node par ligne
        For i = 1 To aBlock.NbLigne
            Set aNode3 = Me.TreeView1.Nodes.add(aNode2.Key, 4, aNode2.Key & "�" & i, aLb.GetCaption(11) & " -> " & i)
            If aBlock.GetExportExcel(i) Then
                aNode3.Image = "SELECTED"
            Else
                aNode3.Image = "UNSELECTED"
            End If
            aNode3.Tag = "LIG"
        Next
    Next 'Pour tous les blocks du tableau
Next 'Pour tous les tableaux de la maquette

End Sub

Private Sub Check7_Click()

If Me.Check7.Value = "1" Then
    Me.Check8.Visible = False
    Me.Check8.Value = "1"
Else
    Me.Check8.Visible = True
    Me.Check8.Value = "1"
End If

End Sub

Private Sub Command1_Click()

'--> on recharge une vue enregistree
'-> on passe le nom de la maquette pour filtrer dessus
frmVue.maquette = Fichiers(Me.FichierName).Spools(Me.SpoolKey).maquette.Nom
frmVue.Show vbModal
If strRetour <> "" Then VueLoad (strRetour)

End Sub

Private Sub Command2_Click()

Dim aFichier As Fichier
Dim aSpool As Spool
Dim aLb As Libelle

'-> verifier qu'un traitement n'est pas en cours
If isCurExport = "TRUE" Then
    isCurExport = "STOP"
    Me.Enabled = False
    Me.Command2.Enabled = False
    Exit Sub
End If

'-> Pointer sur la gestion des messages
Set aLb = Libelles("FRMEXCEL")

'-> si page mini s�lectionn�e v�rifier les zones
If Me.Option1(2).Value Then
    If Trim(Me.Text1.Text) = "" Then
        MsgBox aLb.GetCaption(21), vbCritical + vbOKOnly, aLb.GetCaption(23)
        Me.Text1.SetFocus
        Exit Sub
    End If
            
    '-> si page mini s�lectionn�e v�rifier les zones
    If Trim(Me.Text2.Text) = "" Then
        MsgBox aLb.GetCaption(21), vbCritical + vbOKOnly, aLb.GetCaption(23)
        Me.Text2.SetFocus
        Exit Sub
    End If
    
    '-> Tester que borne mini <= borne maxi
    If CLng(Me.Text1.Text) > CLng(Me.Text2.Text) Then
        MsgBox aLb.GetCaption(22), vbCritical + vbOKOnly, aLb.GetCaption(23)
        Me.Text1.SetFocus
        Exit Sub
    End If
End If

'-> Initialisation des valeurs
pClasseurRef = False
pCelluleDep = False
pName = False
pNomFeuille = False

'-> Si on utilise un classeur de r�f�rence
If Me.Check1.Value = 1 Then
    '-> V�rifier que l'on ait bien saisi un nom de classeur
    If Trim(Me.Label4.Caption) = "" Then
        MsgBox aLb.GetCaption(24), vbExclamation + vbOKOnly, aLb.GetCaption(23)
        Exit Sub
    End If
    
    '-> V�rifier que le classeur sp�cifi� existe bien
    If Dir$(Me.Label4.Caption) = "" Then
        MsgBox aLb.GetCaption(25) & Me.Label4.Caption, vbExclamation + vbOKOnly, aLb.GetCaption(23)
        Exit Sub
    End If
    
    '-> On peut utiliser un classeur de r�f�rence
    pClasseurRef = True
    
End If

'-> Donner un nom � la feuille
If Me.Check4.Value = 1 Then
    '-> V�rifi� que l'on ait bien saisi le nom de la feuille
    If Trim(Me.Text5.Text) = "" Then
        MsgBox aLb.GetCaption(26), vbExclamation + vbOKOnly, aLb.GetCaption(23)
        Me.Text5.SetFocus
        Exit Sub
    End If
    
    '-> Nommer la feuille
    pNomFeuille = True
    
End If

'-> Plage de donn�es
If Me.Check2.Value = 1 Then
    '-> V�rifier que l'on ait bien saisi un nom pour l aplage de donn�es
    If Trim(Me.Text3.Text) = "" Then
        MsgBox aLb.GetCaption(27), vbExclamation + vbOKOnly, aLb.GetCaption(27)
        Me.Text3.SetFocus
        Exit Sub
    End If
    
    '-> Ok on nomme la plage de donn�es
    pName = True

End If

'-> Cellule de d�part
If Me.Check3.Value = 1 Then
    '-> V�rifier que l'on ait saisi la cellule de d�part
    If Trim(Me.Text4.Text) = "" Then
        MsgBox aLb.GetCaption(28), vbExclamation + vbOKOnly, aLb.GetCaption(23)
        Me.Text4.SetFocus
        Exit Sub
    End If
    '-> Sp�cifier la cellule de d�part
    pCelluleDep = True
End If

'-> Si on exporte tout dans une feuille, il faut donner un nom
If SelectSheet = 1 Then
    If Me.Check4.Value = 0 Then
        MsgBox aLb.GetCaption(29), vbExclamation + vbOKOnly, aLb.GetCaption(23)
        If Me.Check4.Enabled Then Me.Check4.SetFocus
        Exit Sub
    End If
End If 'Si on exporte tout dans une feuille


'-> Bloquer le bouton OK
'Me.Enabled = False
'-> ne plus bloquer pour permettre d'arreter
'Me.Command2.Enabled = False

Me.SSTab1.TabEnabled(0) = False
Me.SSTab1.TabEnabled(1) = False
Me.SSTab1.TabEnabled(2) = False
Me.TreeView2.Enabled = False
Me.Check7.Enabled = False
Me.Check8.Enabled = False

Me.Command2.Caption = "Stop"
isCurExport = "TRUE"

'-> Envoyer l'export
ExportWithFormat

'-> on a fini le travail
isCurExport = ""
Set aFichier = Nothing
Set aSpool = Nothing
Set aLb = Nothing
Set aFeuille = Nothing
Set aClasseur = Nothing
Set ExcelApp = Nothing

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Command3_Click()

'--> on lance l'enregistrement des vues
frmVue.Show vbModal
If strRetour <> "" Then VueSave (strRetour)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

Select Case KeyCode

Case vbKeyEscape
    Unload Me
End Select

End Sub

Private Sub Form_Load()

On Error Resume Next

Dim aLb As Libelle

'-> Mise � jour des libelles

Set aLb = Libelles("FRMEXCEL")
Me.Caption = aLb.GetCaption(1)
Me.SSTab1.TabCaption(0) = aLb.GetCaption(35)
Me.SSTab1.TabCaption(1) = aLb.GetCaption(7)
Me.SSTab1.TabCaption(2) = aLb.GetCaption(13)
Me.SSTab1.TabCaption(3) = aLb.GetCaption(18)
Me.Frame2.Caption = aLb.GetCaption(2)
Me.Frame3.Caption = aLb.GetCaption(3)
Me.Option8(0).Caption = aLb.GetCaption(4)
Me.Option8(1).Caption = aLb.GetCaption(5)
Me.Label1.Caption = aLb.GetCaption(6)
Me.Frame1.Caption = aLb.GetCaption(8)
Me.Label6.Caption = aLb.GetCaption(9)
Me.Label8.Caption = aLb.GetCaption(10)
Me.Label10.Caption = aLb.GetCaption(11)
Me.Check6.Caption = aLb.GetCaption(12)
Me.Check1.Caption = aLb.GetCaption(14)
Me.Check4.Caption = aLb.GetCaption(15)
Me.Check2.Caption = aLb.GetCaption(16)
Me.Check3.Caption = aLb.GetCaption(17)
Me.Frame7.Caption = aLb.GetCaption(19)
Me.Check7.Caption = aLb.GetCaption(20)
Me.Option1(1).Caption = aLb.GetCaption(36)
Me.Label5.Caption = aLb.GetCaption(37)
Me.Check5.Caption = aLb.GetCaption(1)
Me.Check8.Caption = aLb.GetCaption(41)

Set aLb = Libelles("FRMPRINTVISU")
Me.Option1(2).Caption = aLb.GetCaption(5)
Me.Option1(3).Caption = aLb.GetCaption(8)
Me.Label2.Caption = aLb.GetCaption(6)
Me.Label3.Caption = aLb.GetCaption(7)

Set aLb = Libelles("BOUTONS")
Me.Command2.Caption = aLb.GetCaption(1)

Set aLb = Libelles("FRMVUE")
'-> on met les images pour ouvrir et enregtistrer les vues
Me.Command1.ToolTipText = aLb.GetCaption(5)
Me.Command3.ToolTipText = aLb.GetCaption(6)

SelectSheet = 0

End Sub

Private Sub Image2_DblClick()

'---> Afficher la boite de dialogue ouvrir
Dim aLb As Libelle

On Error GoTo EndError

'-> Pointer sur la gestion des erreurs
Set aLb = Libelles("FRMEXCEL")

'-> Setting des param�tres de la boite de dialogue
Me.CommonDialog1.CancelError = True
Me.CommonDialog1.DialogTitle = aLb.GetCaption(14)
Me.CommonDialog1.Filter = aLb.GetCaption(30) & "( *.xls)|*.xls|" & aLb.GetCaption(31) & "( *.xlt)|*.xlt"
Me.CommonDialog1.FilterIndex = 1
Me.CommonDialog1.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly Or cdlOFNPathMustExist
Me.CommonDialog1.ShowOpen

'-> Mise � jour du label
Me.Label4.Caption = Me.CommonDialog1.fileName

'-> Cocher la case
Me.Check1.Value = 1

EndError:

End Sub

Private Sub Option1_Click(Index As Integer)
SelectSpool = Index
End Sub


Private Sub Option8_Click(Index As Integer)

'-> Indique la s�lection
SelectSheet = Index

Me.Check4.Enabled = CBool(Index)
Me.Check2.Enabled = CBool(Index)
Me.Text3.Enabled = CBool(Index)
Me.Text5.Enabled = CBool(Index)
Me.Check4.Value = Index
If Index = 0 Then
    Me.Text5.Text = ""
    Me.Text3.Text = ""
    Me.Check2.Value = Index
End If


End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)

Select Case KeyAscii
    Case 8, 48 To 57
    Case Else
        KeyAscii = 0
End Select


End Sub


Private Sub Text2_KeyPress(KeyAscii As Integer)

Select Case KeyAscii
    Case 8, 48 To 57
    Case Else
        KeyAscii = 0
End Select

End Sub

Private Sub TreeView1_DblClick()
'---> Bloquer ou d�bloquer la gestion des prori�t�s

Dim aBlock As Block
Dim aNode As MSComctlLib.Node

For Each aNode In Me.TreeView1.Nodes
    If aNode.Selected Then
        Exit For
    End If
Next

If aNode Is Nothing Then Exit Sub

'-> Afficher le nom du tableau
Me.Label7.Caption = Entry(1, aNode.Key, "�")
Me.Label9.Caption = ""
Me.Label11.Caption = ""

If aNode.Tag = "LIG" Then
    '-> D�bloquer
    Me.Frame1.Enabled = True
    '-> Afficher le nom du block
    Me.Label9.Caption = Entry(2, aNode.Key, "�")
    '-> Afficher le num�ro de la ligne
    Me.Label11.Caption = Entry(3, aNode.Key, "�")
    '-> Pointer sur le block
    Set aBlock = GetBlockPtr
    '-> On inverse la propri�t�
    Me.Check5.Value = aBlock.GetExportExcel(CInt(Me.Label11.Caption)) * -1
    If Me.Check5.Value = 1 Then
        Me.Check5.Value = 0
    Else
        Me.Check5.Value = 1
    End If
    Check5_Click
    Me.Check6.Value = aBlock.GetExportUniqueExcel(CInt(Me.Label11.Caption)) * -1
ElseIf aNode.Tag = "BLOCK" Then
    '-> Afficher le nom du block
    Me.Label9.Caption = Entry(2, aNode.Key, "�")
    '-> Bloquer la saisie
    Me.Frame1.Enabled = False
End If

End Sub

Private Sub TreeView1_KeyPress(KeyAscii As Integer)
Dim aNode As Node

'On Error Resume Next
'-> on selectionne ou deselectionne tous les elements d'un node
If KeyAscii = 13 Then
    If Me.TreeView1.SelectedItem.Tag <> "LIG" Then Exit Sub
    For Each aNode In Me.TreeView1.Nodes
        '-> on inverse la selection
        If aNode.Tag = "LIG" Then
            If aNode.Parent.Text = Me.TreeView1.SelectedItem.Parent.Text Then
                TreeView1_NodeClick aNode
                If Me.Check5.Value = 1 Then
                    Me.Check5.Value = 0
                Else
                    Me.Check5.Value = 1
                End If
                Check5_Click
            End If
        End If
    Next
End If

End Sub

Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)

'---> Bloquer ou d�bloquer la gestion des prori�t�s

Dim aBlock As Block

'-> Afficher le nom du tableau
Me.Label7.Caption = Entry(1, Node.Key, "�")
Me.Label9.Caption = ""
Me.Label11.Caption = ""

If Node.Tag = "LIG" Then
    '-> D�bloquer
    Me.Frame1.Enabled = True
    '-> Afficher le nom du block
    Me.Label9.Caption = Entry(2, Node.Key, "�")
    '-> Afficher le num�ro de la ligne
    Me.Label11.Caption = Entry(3, Node.Key, "�")
    '-> Pointer sur le block
    Set aBlock = GetBlockPtr
    '-> Afficher ses propri�t�s
    Me.Check5.Value = aBlock.GetExportExcel(CInt(Me.Label11.Caption)) * -1
    Me.Check6.Value = aBlock.GetExportUniqueExcel(CInt(Me.Label11.Caption)) * -1
ElseIf Node.Tag = "BLOCK" Then
    '-> Afficher le nom du block
    Me.Label9.Caption = Entry(2, Node.Key, "�")
    '-> Bloquer la saisie
    Me.Frame1.Enabled = False
End If

End Sub
Private Sub InitInterface(PageMin As Long, PageMax As Long)

Dim aNode As Node
Dim i As Integer
Dim aLb As Libelle

'-> Pointer sur la classe message
Set aLb = Libelles("FRMEXCEL")

For i = PageMin To PageMax
    '-> Ajouter un node
    Me.TreeView2.Nodes.add , , "PAGE" & i, aLb.GetCaption(38) & " " & i, "UNSELECTED"
Next

End Sub

Private Sub InitPageDisplay(NbLigne As Long, Page As Long)

Dim aLb As Libelle

'-> Pointer sur la classe messprog
Set aLb = Libelles("FRMEXCEL")

'-> vider le picture de temporisation
Me.Picture1.Cls

'-> Calcul de la taille total
TailleTotale = NbLigne
TailleLue = 0

'-> Modifier le libell�
Me.Label12.Caption = aLb.GetCaption(39)
DoEvents

'-> Setting de l'icone
Me.TreeView2.Nodes("PAGE" & Page).ForeColor = RGB(255, 0, 0)
Me.TreeView2.Nodes("PAGE" & Page).Bold = True
Me.TreeView2.Nodes("PAGE" & Page).EnsureVisible
DoEvents

End Sub



Private Sub ExportWithFormat()

'---> Cette proc�dure est charg�e d'exporter vers Excel

Dim PageMin As Long
Dim PageMax As Long
Dim aFichier As Fichier
Dim aSpool As Spool
Dim i As Long, j As Integer
Dim CreateSheet As Boolean
Dim Ligne As String
Dim DefPage As String
Dim NbLig As Long
Dim DelRef As String

Dim TypeObjet As String
Dim TypeSousObjet As String
Dim NomObjet As String
Dim NomSousObjet As String
Dim Param As String
Dim DataFields As String
Dim aBlock As Block
Dim aTb As Tableau
Dim SetNameFeuille As Boolean
Dim NbCol As Integer
Dim aLb As Libelle
Dim IsFormated As Boolean

On Error Resume Next

'-> Pointer sur la gestion des messages
Set aLb = Libelles("FRMEXCEL")

If IsMouchard Then Trace "Export excel :", 1

'-> Indiquer si on imprime ou non le format
IsFormated = CBool(Me.Check7.Value)

'-> Pointer sur l'objet fichier
Set aFichier = Fichiers(Me.FichierName)
'-> Pointer sur le spool sp�cifi�
Set aSpool = aFichier.Spools(Me.SpoolKey)

'-> Il faut d�terminer la plage d'impression
Select Case SelectSpool
    Case 1
        PageMin = 1
        PageMax = aSpool.NbPage
    Case 2
        PageMin = CLng(Me.Text1.Text)
        PageMax = CLng(Me.Text2.Text)
    Case 3
        PageMin = aSpool.CurrentPage
        PageMax = aSpool.CurrentPage
End Select 'Selon la plage d'export

'-> Mettre � jour le treeview avec l'import en cours
InitInterface PageMin, PageMax

'-> D�terminer si on cr�er un onglet � chaque page
CreateSheet = Me.Option8(0).Value

'-> Initialiser un pointeur vers une application Excel
'Set ExcelApp = New Excel.Application
'-> Modification pour pouvoir pointer sur la version d'excel pr�sente
Set ExcelApp = CreateObject("EXCEL.APPLICATION")

'-> Doit on cr�er un nouveau classeur ou une r�f�rence
If pClasseurRef Then
    '-> Ouvrir le classeur de r�f�rence
    Set aClasseur = OpenClasseurRef()
    '-> Tester le retour
    If aClasseur Is Nothing Then
        MsgBox aLb.GetCaption(32), vbCritical + vbOKOnly, aLb.GetCaption(23)
        Exit Sub
    End If
Else
    '-> Cr�er un nouveau classeur
    Set aClasseur = ExcelApp.Workbooks.add
End If

'-> R�cup�rer la liste des feuilles � supprimer
For Each aFeuille In aClasseur.Worksheets
    If DelRef = "" Then
        DelRef = aFeuille.Name
    Else
        DelRef = DelRef & "|" & aFeuille.Name
    End If
Next

'-> Donner son nom � la feuille par d�faut
SetNameFeuille = True

'-> V�rifier si on a donn� une r�f�rence
If pNomFeuille Then
    '-> Chercher dans toutes les feuilles si la feuille sp�cifi�e existe ou non
    For Each aFeuille In aClasseur.Worksheets
        If UCase$(Trim(aFeuille.Name)) = UCase$(Trim(Me.Text5.Text)) Then
            '-> Indiquer qu'il ne faut pas donner de nom
            SetNameFeuille = False
            '-> Quitter la boucle
            Exit For
        End If
    Next
End If

'-> Cr�er la premi�re feuille si necessaire
If SetNameFeuille Then Set aFeuille = aClasseur.Worksheets.add(, aClasseur.Worksheets(aClasseur.Worksheets.Count))

'-> Activer la feuille
aFeuille.Activate

'-> Initialiser la cellule de d�part
If pCelluleDep Then SetActiveCell

'-> Bloquer la mise � jour des questions de confirmation
ExcelApp.DisplayAlerts = False

'-> Si on est en export non formatter, initialiser l'export vers le fichier ascii
If Not IsFormated Then
    '-> Init de l'export d'impression
    InitExportPageByAscii
End If

'-> Impression des pages s�lectionn�es
For i = PageMin To PageMax
    '-> Tester si on doit nommer la feuille
    If SetNameFeuille Then
        If pNomFeuille Then
            '-> Nommer la page
            SetNameForm Trim(Me.Text5.Text)
        Else
            '-> Donner son num�ro � la page selon que classeur de reference ou pas - pierrot 15/06/2005
            If pClasseurRef Then
                aFeuille.Name = aLb.GetCaption(38) & (aClasseur.Worksheets.Count)
            Else
                aFeuille.Name = aLb.GetCaption(38) & i
            End If
        End If
    End If 'si on doit nommer la feuille
    '-> R�cup�rer la d�finition de la page
    DefPage = aSpool.GetPage(i)
    '-> r�cup�rer le nombre de ligne des la page
    NbLig = NumEntries(DefPage, Chr(0))
    InitPageDisplay NbLig, i
                
    '-> Analyse de toutes les lignes de la page
    For j = 1 To NbLig
        '-> Lecture d'une ligne
        Ligne = Entry(j, DefPage, Chr(0))
        '-> Ne traiter que les lignes tableaux
        If Mid$(Trim(UCase$(Ligne)), 1, 3) <> "[TB" Then GoTo NextLig
        '-> R�cup�rer la position de d�part
        ColDep = ExcelApp.ActiveCell.Column
        RowDep = ExcelApp.ActiveCell.Row
        AdresseRange = ExcelApp.ActiveCell.Address
        '-> Analyser la ligne
        AnalyseObj Ligne, TypeObjet, NomObjet, TypeSousObjet, NomSousObjet, Param, DataFields
        '-> Exporter la ligne
        ExportLig NomObjet, NomSousObjet, CInt(Entry(2, Param, "\")), DataFields, aFeuille, aSpool.maquette
NextLig:
        '-> Dessin de la temporisation
        TailleLue = TailleLue + 1
        DrawTempo Me.Picture1
        DoEvents
    Next 'Pour toutes les lignes � imprimer
    
    '-> Si on doit cr�er une page � chaque fois
    If CreateSheet Then
        '-> Cas ou on est en export par RTF
        If Not IsFormated Then
            '-> Remonter le fichier ascii dans Excel
            CloseExportByAscii
        Else
            '-> Formatter la page en cours
            FormatPage aFeuille, aSpool.maquette
        End If
        
         '-> Ajouter une nouvelle page
        If i <> PageMax Then
            '-> Cas de l'export sans format
            If Not IsFormated Then InitExportPageByAscii
            Set aFeuille = aClasseur.Worksheets.add(, aClasseur.Worksheets(aClasseur.Worksheets.Count))
'            '-> Donner son num�ro � la page selon que classeur de reference ou pas - pierrot 15/06/2005
'            If pClasseurRef Then
'                aFeuille.Name = aLb.GetCaption(38) & (i + (aClasseur.Worksheets.Count - 1))
'            Else
'                aFeuille.Name = aLb.GetCaption(38) & i
'            End If
        End If
        
        '-> Initialiser la cellule de d�part
        If pCelluleDep Then SetActiveCell
        
    End If 'Si on doit cr�er un onglet � chaque page
    
    '-> La page est trait�e, modifier son icone
    Me.TreeView2.Nodes("PAGE" & i).Image = "SELECTED"
    Me.TreeView2.Nodes("PAGE" & i).ForeColor = 0
    Me.TreeView2.Nodes("PAGE" & i).Bold = False
    DoEvents
    If isCurExport = "STOP" Then Exit For
Next 'Pour toutes les pages � imprimer

'-> formatter la derni�re page si ce n'est pas fait
If Not CreateSheet Then
    If IsFormated Then
        FormatPage aFeuille, aSpool.maquette
    Else
        '-> Fermer l'export en cours
         CloseExportByAscii
        '-> D�terminer le nombre maxi de colonne si on doit donner un nom � la plage de donn�es
        If pName Then
            For Each aTb In aSpool.maquette.Tableaux
                '-> Pour tous les blocks
                For Each aBlock In aTb.Blocks
                    '-> Ne g�rer les colonnes que si au moins une ligne est export�e vers Excel
                    If aBlock.IsOneExportLig Then
                        If aBlock.NbCol > NbCol Then NbCol = aBlock.NbCol
                    End If
                Next 'Pour tous les blocks
            Next 'Pour tous les tableaux
            
            '-> Donner un nom si necessaire
            If SelectSheet = 1 And pName Then SetName (NbCol)
            
        End If 'Si on doit donner un nom � la plage de donn�es
    End If
End If

'-> Supprimer les feuilles en trop dans le classeur si on n' a pas boss� avec une ref
If IsMouchard Then Trace "On supprime les feuilles inutiles", 1
If Not pClasseurRef Then
    For i = 1 To NumEntries(DelRef, "|")
        '-> Supprimer la feuille sp�cifi�e
        If aClasseur.Worksheets.Count <> 1 Then
            If Not aClasseur.Worksheets(Entry(i, DelRef, "|")) Is Nothing Then aClasseur.Worksheets(Entry(i, DelRef, "|")).Delete
        End If
    Next 'Pour toutes les feuilles � supprimer
End If 'Si on utilise un classeur de r�f�rence

If IsMouchard Then Trace "On active la feuille", 3
'-> S�lectionner la premi�re page
aClasseur.Worksheets(1).Activate

If IsMouchard Then Trace "On r�active les alertes", 3
'-> R�initialiser Excel
ExcelApp.DisplayAlerts = True

DoEvents

If IsMouchard Then Trace "On rend visible la feuille", 3
'-> Rendre excel visible
ExcelApp.Visible = True

'-> Pour tous les blocks RAZ de la variable d'export
For Each aTb In aSpool.maquette.Tableaux
    '-> Pour tous les blocks dans le tableau
    For Each aBlock In aTb.Blocks
        For i = 1 To aBlock.NbLigne
            '-> Supprimer le pointeur qui indique que la ligne est export�e
            aBlock.SetIsExportedLig i, False
        Next 'Pour toutes les lignes
    Next 'Pour tous les blocks du tableau
Next 'Pour tous les tableaux dans la maquette

If IsMouchard Then Trace "Fin de l'export", 1

ExcelError:

    '-> Lib�rer les diff�rents pointeurs
    Set aFeuille = Nothing
    Set aClasseur = Nothing
    Set ExcelApp = Nothing
    
Debug.Print Now
    
End Sub
Private Function InitExportPageByAscii()

'-> Obtenir un nom de fichier temporaire
TempFileName = GetTempFileNameVB("XLS")

'-> Obtenir un handle de fichier
hdlFile = FreeFile
Open TempFileName For Output As #hdlFile


End Function

Private Function CloseExportByAscii()

'---> Cette proc�dure exporte vers Excel le fichier ascii sp�cifie

Dim SepFic As String
Dim FieldInfoString As String
Dim i As Long
Dim LineToAnalyse As String
Dim FieldInfoArray() As Variant

If IsMouchard Then Trace "On charge les donn�es brutes", 1

'-> Fermer le fichier ASCII
Close #hdlFile

'-> Le remonter dans le RTF
frmLib.Rtf(0).LoadFile TempFileName

If Me.Check8.Value = 0 Then
    frmLib.Rtf(0).Text = Replace(frmLib.Rtf(0).Text, "|", "| ")
    frmLib.Rtf(0).Text = Replace(frmLib.Rtf(0).Text, Chr(13) & Chr(10), Chr(13) & Chr(10) & " ")
End If

'-> S�lectionner tout le text dans le RTF
frmLib.Rtf(0).SelStart = 0
frmLib.Rtf(0).SelLength = Len(frmLib.Rtf(0).Text)

'-> Vider le press papier
Clipboard.Clear

'-> Copier dans le press papier
Clipboard.SetText frmLib.Rtf(0).SelText

'-> Coller dans Excel
aFeuille.Paste

If IsMouchard Then Trace "On a coll� les donn�es brutes", 3

'-> Eclater sur toutes les cellules
SepFic = "|"
If ExcelApp.Selection.Columns.Count = 1 Then
    '-> PIERROT 27/06/2005 : ne pas traiter ni convertir les pages blanches du spool sinon erreur
    If Trim(frmLib.Rtf(0).Text) <> "" Then
        '-> PIERROT : probleme de formattage dates / on cr�e un vecteur de formats correspondant au nombre de colonnes
        '-> permet de ne plus avoir les dates en format americain
        LineToAnalyse = Entry(2, frmLib.Rtf(0).Text, Chr(13) & Chr(10))
        If IsMouchard Then Trace "On organise les donn�es", 3
        '-> on choisit la methode
        If Me.Check8.Value = 1 Then
            '-> on redimensionne le tableau
            FieldInfoArray = Array(Array(1, 2), Array(2, 2), Array(3, 2), Array(4, 2), Array(5, 2), Array(6, 2), Array(7, 2), Array(8, 2), Array(9, 2), Array(10, 2), Array(11, 2), Array(12, 2), Array(13, 2), Array(14, 2), Array(15, 2), Array(16, 2), Array(17, 2), Array(18, 2), Array(19, 2), Array(20, 2), Array(21, 2), Array(22, 2), Array(23, 2), Array(24, 2), Array(25, 2), Array(26, 2), Array(27, 2), Array(28, 2), Array(29, 2), Array(30, 2), Array(41, 2), Array(42, 2), Array(43, 2), Array(44, 2), Array(45, 2), Array(46, 2), Array(47, 2), Array(48, 2), Array(49, 2), Array(50, 2))
            'ReDim Preserve FieldInfoArray(NumEntries(LineToAnalyse, SepFic))
            '-> On envoie l'explode de la ligne
            ExcelApp.Selection.TextToColumns DataType:=xlDelimited, _
            ConsecutiveDelimiter:=False, Other:=True, OtherChar:=SepFic, FieldInfo:=FieldInfoArray
        Else
            LineToAnalyse = Entry(2, frmLib.Rtf(0).Text, Chr(13) & Chr(10))
            '-> On envoie l'explode de la ligne
            ExcelApp.Selection.TextToColumns DataType:=xlDelimited, _
            ConsecutiveDelimiter:=False, Other:=True, OtherChar:=SepFic
        End If
        '-> On parcours le fichier RTF sur separateur "saut de ligne" pour analyser
'        For i = 1 To NumEntries(LineToAnalyse, SepFic)
'            '-> Determination du data type / on ne traite que les dates
'            If IsDate(Entry(i, LineToAnalyse, SepFic)) And (Len(Entry(i, LineToAnalyse, SepFic)) = 8 Or Len(Entry(i, LineToAnalyse, SepFic)) = 10) Then
'                '-> On compose le vecteur date
'                FieldInfoArray(i - 1)(1) = xlDMYFormat
'            Else
'                '-> On compose le vecteur date
'                FieldInfoArray(i - 1)(1) = xlDMYFormat 'xlGeneralFormat
'            End If
'        Next

    End If
End If

'-> Supprimer le fichier ascii
If Dir$(TempFileName) <> "" Then Kill TempFileName

End Function

Private Function SetActiveCell() As Boolean

On Error GoTo GestError

'-> Essayer de positionner la cellule active
ExcelApp.Range(Me.Text4.Text).Select
Exit Function

GestError:
    SetActiveCell = False

End Function

Private Function OpenClasseurRef() As Workbook

Dim aW As Workbook

On Error GoTo GestError

'-> Essayer d'ouvrir le classeur sp�cifi�
Set aW = ExcelApp.Workbooks.Open(Me.Label4.Caption)
'-> Renvoyer la r�f�rence
Set OpenClasseurRef = aW

Exit Function

GestError:
        Set OpenClasseurRef = Nothing


End Function

Private Sub SetNameForm(strName As String)

On Error Resume Next

aFeuille.Name = strName

End Sub

Private Sub SetName(ByVal NbCol As Long)

On Error GoTo GestError

Dim aRange As Range

'-> S�lectionner toute la page des donn�es
Set aRange = ExcelApp.Application.Range(ExcelApp.Application.ActiveCell, ExcelApp.Application.ActiveCell.Offset(ExcelApp.Application.Selection.Areas.Item(1).Rows.Count - 1, NbCol - 1))
aRange.Name = Trim(Me.Text3.Text)

Exit Sub

GestError:



End Sub

Private Sub FormatPage(aFeuille As Worksheet, aMaq As maquette)

'---> Cette proc�dure formatte les cellules d'une feuille

Dim aTb As Tableau
Dim aBlock As Block
Dim aCell As Cellule
Dim i As Integer, j As Integer, k As Integer, l As Integer
Dim aRange As Range
Dim aCol As Range
Dim aLb As Libelle
Dim TempZone As String

'On Error GoTo GestError

'-> Pointer sur la classe libell�
Set aLb = Libelles("FRMEXCEL")

'-> vider le picture de temporisation
Me.Picture1.Cls

'-> Calcul de la taille total
TailleTotale = 0
TailleLue = 0
For Each aTb In aMaq.Tableaux
    For Each aBlock In aTb.Blocks
        For i = 1 To aBlock.NbLigne
            If aBlock.GetExportExcel(i) Then
                '-> Formatter toutes les cellules de cette ligne
                For j = 1 To aBlock.NbCol
                    TailleTotale = TailleTotale + 1
                Next
            End If
        Next
    Next
Next

'-> Modifier le libell�
Me.Label12.Caption = aLb.GetCaption(40)
DoEvents

'-> Analyse de tous les tableaux
For Each aTb In aMaq.Tableaux
    '-> Analyse de tous les blocks
    For Each aBlock In aTb.Blocks
        '-> Pour toutes les lignes
        For i = 1 To aBlock.NbLigne
            '-> Tester si on exporte la ligne lvers Excel
            If aBlock.GetExportExcel(i) Then
                '-> Formatter toutes les cellules de cette ligne
                For j = 1 To aBlock.NbCol
                    If aBlock.Cellules("L" & i & "C" & j).ListeCell <> "" Then
                        l = 0
                        TempZone = ""
                        For k = 1 To NumEntries(aBlock.Cellules("L" & i & "C" & j).ListeCell, ";")
                            '-> Incr�menter le compteur de ligne
                            l = l + 1
                            '-> Get de l'entry associ�
                            If TempZone = "" Then
                                TempZone = Entry(k, aBlock.Cellules("L" & i & "C" & j).ListeCell, ";")
                            Else
                                TempZone = TempZone & ";" & Entry(k, aBlock.Cellules("L" & i & "C" & j).ListeCell, ";")
                            End If
                            '-> Envoyer par lot de 20 le formattage
                            If l = 20 Then
                                '-> Pointer sur la zone
                                Set aRange = aFeuille.Range(TempZone)
                                '-> Formatter
                                SetFormat aBlock.Cellules("L" & i & "C" & j).aRange, aRange
                                DoEvents
                                '-> Raz des variables
                                l = 0
                                TempZone = ""
                            End If
                        Next
                        '-> Attention , si TempZone <> "" il faut formatter
                        If TempZone <> "" Then
                            '-> Pointer sur la zone
                            Set aRange = aFeuille.Range(TempZone)
                            '-> Formatter
                            SetFormat aBlock.Cellules("L" & i & "C" & j).aRange, aRange
                            DoEvents
                        End If 'S'il reste des zones � formatter
                    End If 'Sil y a des cellules � formatter
                    '-> Vider la liste des cellules
                    aBlock.Cellules("L" & i & "C" & j).ListeCell = ""
                    '-> Gestion de la temporisation
                    TailleLue = TailleLue + 1
                    DrawTempo Me.Picture1
                Next
            End If 'Si on exporte la ligne vers Excel
        Next 'Pour toutes les lignes
    Next 'Pour tous les blocks
Next 'Pour tous les tableaux

'-> Donner une largeur Automatique � toutes les colonnes
For Each aCol In ExcelApp.Columns
    aCol.EntireColumn.AutoFit
Next

Exit Sub

GestError:

End Sub

Private Sub ExportLig(Tableau As String, Block As String, ByVal Ligne As Integer, DataFields As String, aFeuille As Worksheet, maquette As maquette)

'---> Cette proc�dure exporte une ligne d'un block vers Excel

Dim aBlock As Block
Dim aCell As Cellule
Dim aRange As Range
Dim aRow As Range
Dim Decalage As Long
Dim i As Long
Dim PrintSigne As Boolean
Dim aFt As FormatedCell
Dim ContenuCell As String
Dim IsFormated As Boolean
Dim ValuePrint As String

'-> Indiquer si on exporte avec format ou non
IsFormated = CBool(Me.Check7.Value)

'-> Obtenir un pointeur vers le block
Set aBlock = maquette.Tableaux(UCase$(Trim(Tableau))).Blocks("BL-" & UCase$(Trim(Block)))

'-> V�rifier que l'on exporte la ligne sp�cifi�e
If Not aBlock.GetExportExcel(Ligne) Then Exit Sub

'-> Tester si d�ja export�e
If aBlock.GetExportUniqueExcel(Ligne) And aBlock.GetIsExportedLig(Ligne) Then Exit Sub

'-> Mettre � jour la hauteur de ligne
If IsFormated Then
    '-> On gere le pb d'une ligne trop haute pour excel DIDIER
    If frmLib.ScaleY(aBlock.GetHauteurLig(Ligne), 7, 2) <= 409 Then
        ExcelApp.Rows(RowDep).RowHeight = frmLib.ScaleY(aBlock.GetHauteurLig(Ligne), 7, 2)
    Else 'on met la valeur maximale autoris�e
        ExcelApp.Rows(RowDep).RowHeight = 409
    End If
End If
'-> Get d'un pointeur vers la cellule en cours
If IsFormated Then Set aRange = aFeuille.Range(AdresseRange)

'-> Anlyse de toutes les cellules de la ligne
For i = 1 To aBlock.NbCol
    '-> Pointer sur la cellule
    Set aCell = aBlock.Cellules("L" & Ligne & "C" & i)
    
    '-> Cas ou exporte les formats
    If IsFormated Then
    
        '-> Largeur de la colonne que sil a cellule sp�cifi�e n'est pas fusionn�e
        If Not aCell.IsFusion Then
            '-> Largeur de colonne
            aRange.ColumnWidth = frmLib.ScaleX(aBlock.GetLargeurCol(i), 7, 4)
            Decalage = Decalage + 1
        Else
            '-> S�lectionner les cellules suivantes pour la fusion
            Set aRange = ExcelApp.Range(aRange, aRange.Offset(, aCell.ColFusion))
            aRange.Merge
            Decalage = Decalage + aCell.ColFusion + 1
        End If 'S'il y a fusion
    
    End If
    
    '-> Remplacer les champs par leur valeur
    aCell.ReplaceField DataFields
        
    '-> Gestion des formats num�rics
    Select Case aCell.TypeValeur
        Case 0 '-> RAS
            If IsFormated Then aRange.NumberFormat = "@"
        Case 1 '-> Caract�re
            '-> Faire un susbstring
            aCell.ContenuCell = Mid$(aCell.ContenuCell, 1, CInt(aCell.Msk))
            If IsFormated Then aRange.NumberFormat = "@"
        Case 2 '-> Numeric
            If IsFormated Then aRange.NumberFormat = "0.00"
            '-> V�rifier s'il y a quelquechose � formater
            If Trim(aCell.ContenuCell) = "" Then
            Else
                '-> r�cup�rer le contenu de la cellule
                ContenuCell = Convert(aCell.ContenuCell)
                                                
                '-> Tester que la cellule soit num�rique
                If Not IsNumeric(ContenuCell) Then
                    If IsFormated Then aRange.NumberFormat = "@"
                Else
                    '-> Charger le masque de la cellule
                    aFt.nbDec = CInt(Entry(1, aCell.Msk, "�"))
                    aFt.idNegatif = CInt(Entry(3, aCell.Msk, "�"))
                    
                    '-> Positionner le bon format de cellule
                    If IsFormated Then aRange.NumberFormat = GetFormat(aFt.nbDec, aFt.idNegatif)
                    
                    '-> Affecter le contenu de la cellule
                    aCell.ContenuCell = CStr(CDbl(ContenuCell))
                    
                    '-> Si on doit mettre un 0
                    If CDbl(aCell.ContenuCell) = 0 And Not (aCell.PrintZero) Then aCell.ContenuCell = ""
                    
                End If
            End If
    End Select

    If IsFormated Then
        'THIERRY MODIF 27/02/2004
        '-> Mettre � jour le contenu de la cellule
        If (IsNumeric(aCell.ContenuCell) And aCell.TypeValeur) Then
            aRange.Value = CDbl(aCell.ContenuCell)
        Else
            aRange.Value = aCell.ContenuCell
        End If
        
        '-> R�cup�rer la  valaeur de la cellule
        ContenuCell = aRange.Address

        '-> Virer les $
        ContenuCell = Replace(ContenuCell, "$", "")

        '-> Ajouter dans la liste des cellules qu'il faudra formatter
        If aCell.ListeCell = "" Then
            aCell.ListeCell = ContenuCell
        Else
            aCell.ListeCell = aCell.ListeCell & ";" & ContenuCell
        End If

        '-> D�placer la cellule active d'une colonne vers la droite
        Set aRange = aRange.Offset(0, 1)
    Else
    
        '-> Mettre � jour la variable d'export
        If ValuePrint = "" Then
            ValuePrint = aCell.ContenuCell
        Else
            ValuePrint = ValuePrint & "|" & aCell.ContenuCell
        End If
    End If
Next

'-> D�placer d'une ligne vers le bas et revenir � la colonne d'origine
If IsFormated Then
    ExcelApp.Cells(RowDep, ColDep).Offset(1, 0).Select
Else
    '-> Inscrire dans le fichier la liste des donn�es � exporter
    Print #hdlFile, ValuePrint
End If

'-> Indiquer que l'on a exporter cette ligne
aBlock.SetIsExportedLig Ligne, True

End Sub

Private Function GetFormat(nbDec As Integer, IdNeg As Integer) As String

Dim strDec As String
Dim i As Integer
Dim Masque As String

If nbDec = 0 Then
    strDec = "0"
Else
    '-> Cr�er le masque
    strDec = "0." & String$(nbDec, "0")
End If
Masque = strDec

If IdNeg > 1 Then
    Masque = strDec & "_ ;[Red]-" & strDec & " "
End If

GetFormat = Masque

End Function

Private Sub SetFormat(RangeRef As clsRange, RangeDest As Range)

'---> Cette proc�dure duplique un format
Dim i As Long


'-> Bordures
For i = 7 To 10
    RangeDest.Borders(i).LineStyle = RangeRef.GetBorderLineStyle(i)
    RangeDest.Borders(i).ColorIndex = RangeRef.GetBorderColorIndex(i)   'RangeRef.borders(i).LineStyle
Next

'-> Font
With RangeDest.Font
    .Name = RangeRef.FontName
    .Size = RangeRef.FontSize
    .Bold = RangeRef.FontBold
    .Italic = RangeRef.FontItalic
    .Underline = RangeRef.FontUnderline
End With

'-> Couleur de font
RangeDest.Interior.Color = RangeRef.BackColor

'-> Alignement interne de la cellule
RangeDest.VerticalAlignment = RangeRef.VerticalAlignment
RangeDest.HorizontalAlignment = RangeRef.HorizontalAlignment


End Sub

Private Function VueSave(sName As String)
Dim aNode As Node
Dim aBlock As Block
Dim sMaquette As String

'--> cette fonction permet d'enrtegistrer une vue
SetIniString Trim(UCase(sName)), "Maquette", App.Path & "\TurboVue.ini", Fichiers(Me.FichierName).Spools(Me.SpoolKey).maquette.Nom
SetIniString Trim(UCase(sName)), "G_ETENDUE", App.Path & "\TurboVue.ini", SelectSpool & "�" & Me.Text1.Text & "�" & Me.Text2.Text
SetIniString Trim(UCase(sName)), "G_GESTION", App.Path & "\TurboVue.ini", CStr(SelectSheet)

'-> pour le tableaux
For Each aNode In Me.TreeView1.Nodes
    If sMaquette <> "" Then sMaquette = sMaquette & "�"
    If aNode.Image = "SELECTED" Then
        sMaquette = sMaquette & "1"
    Else
        sMaquette = sMaquette & "0"
    End If
    If aNode.Tag = "LIG" Then
        '-> Pointer sur le block
        Set aBlock = Fichiers(Me.FichierName).Spools(Me.SpoolKey).maquette.Tableaux(UCase$(Trim(Entry(1, aNode.Key, "�")))).Blocks(UCase$(Trim(Entry(2, aNode.Key, "�"))))
        '-> On ajoute les propri�t�s
        If aBlock.GetExportExcel(CInt(Entry(3, aNode.Key, "�"))) Then
            sMaquette = sMaquette & ",1"
        Else
            sMaquette = sMaquette & ",0"
        End If
        If aBlock.GetExportUniqueExcel(CInt(Entry(3, aNode.Key, "�"))) Then
            sMaquette = sMaquette & ",1"
        Else
            sMaquette = sMaquette & ",0"
        End If
    ElseIf aNode.Tag = "BLOCK" Then
        sMaquette = sMaquette & ",,"
    End If
Next
SetIniString Trim(UCase(sName)), "M_MAQUETTE", App.Path & "\TurboVue.ini", sMaquette

SetIniString Trim(UCase(sName)), "R_CLASSE", App.Path & "\TurboVue.ini", Me.Check1.Value
SetIniString Trim(UCase(sName)), "R_NOMMER", App.Path & "\TurboVue.ini", Me.Check4.Value & "�" & Me.Text5.Text
SetIniString Trim(UCase(sName)), "R_PLAGE", App.Path & "\TurboVue.ini", Me.Check2.Value & "�" & Me.Text3.Text
SetIniString Trim(UCase(sName)), "R_DEPART", App.Path & "\TurboVue.ini", Me.Check3.Value & "�" & Me.Text4.Text
SetIniString Trim(UCase(sName)), "T_FORMAT", App.Path & "\TurboVue.ini", Me.Check7.Value
SetIniString Trim(UCase(sName)), "T_DONNEES", App.Path & "\TurboVue.ini", Me.Check8.Value

End Function

Private Function VueLoad(sName As String)
Dim i As Integer
Dim aNode As Node
Dim aBlock As Block
Dim sMaquette As String
Dim sTrav As String

On Error Resume Next

'--> cette fonction permet de charger une vue enregistree
sTrav = GetIniString(Trim(UCase(sName)), "G_ETENDUE", App.Path & "\TurboVue.ini", False)
'--> si la vue nest pas ok quitter
If sTrav = "" Then Exit Function
Me.Text1.Text = Entry(2, sTrav, "�")
Me.Text2.Text = Entry(3, sTrav, "�")
Me.Option1(Val(Entry(1, sTrav, "�"))).Value = 1
Call Option1_Click(Val(Entry(1, sTrav, "�")))

sTrav = GetIniString(Trim(UCase(sName)), "G_GESTION", App.Path & "\TurboVue.ini", False)
Me.Option8(Val(Entry(1, sTrav, "�"))).Value = 1
Call Option8_Click(Val(sTrav))

'-> pour le tableaux
sTrav = GetIniString(Trim(UCase(sName)), "M_MAQUETTE", App.Path & "\TurboVue.ini", False)

For Each aNode In Me.TreeView1.Nodes
    i = i + 1
    If aNode.Tag = "LIG" Then
        If Entry(1, Entry(i, sTrav, "�"), ",") = "1" Then
            aNode.Image = "SELECTED"
        Else
            aNode.Image = "UNSELECTED"
        End If
        '-> Pointer sur le block
        Set aBlock = Fichiers(Me.FichierName).Spools(Me.SpoolKey).maquette.Tableaux(UCase$(Trim(Entry(1, aNode.Key, "�")))).Blocks(UCase$(Trim(Entry(2, aNode.Key, "�"))))
        '-> On ajoute les propri�t�s
        If Entry(2, Entry(i, sTrav, "�"), ",") Then
            aBlock.SetExportExcel CInt(Entry(3, aNode.Key, "�")), True
        Else
            aBlock.SetExportExcel CInt(Entry(3, aNode.Key, "�")), False
        End If
        '-> on affecte la propriete
        If Entry(3, Entry(i, sTrav, "�"), ",") Then
            aBlock.SetExportUniqueExcel CInt(Entry(3, aNode.Key, "�")), True
        Else
            aBlock.SetExportUniqueExcel CInt(Entry(3, aNode.Key, "�")), False
        End If
    ElseIf aNode.Tag = "BLOCK" Then
        
    End If
Next

sTrav = GetIniString(Trim(UCase(sName)), "R_CLASSE", App.Path & "\TurboVue.ini", False)
Me.Check1.Value = Val(sTrav)

sTrav = GetIniString(Trim(UCase(sName)), "R_NOMMER", App.Path & "\TurboVue.ini", False)
Me.Check4.Value = Val(Entry(1, sTrav, "�"))
Me.Text5.Text = Entry(2, sTrav, "�")

sTrav = GetIniString(Trim(UCase(sName)), "R_PLAGE", App.Path & "\TurboVue.ini", False)
Me.Check2.Value = Val(Entry(1, sTrav, "�"))
Me.Text3.Text = Entry(2, sTrav, "�")

sTrav = GetIniString(Trim(UCase(sName)), "R_DEPART", App.Path & "\TurboVue.ini", False)
Me.Check3.Value = Val(Entry(1, sTrav, "�"))
Me.Text4.Text = Entry(2, sTrav, "�")

sTrav = GetIniString(Trim(UCase(sName)), "T_FORMAT", App.Path & "\TurboVue.ini", False)
Me.Check7.Value = Val(sTrav)

sTrav = GetIniString(Trim(UCase(sName)), "T_DONNEES", App.Path & "\TurboVue.ini", False)
Me.Check8.Value = Val(sTrav)

End Function

