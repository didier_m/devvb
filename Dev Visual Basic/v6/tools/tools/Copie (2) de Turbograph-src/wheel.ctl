VERSION 5.00
Begin VB.UserControl WheelCtl 
   BackColor       =   &H00FFC0C0&
   ClientHeight    =   300
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   300
   InvisibleAtRuntime=   -1  'True
   ScaleHeight     =   300
   ScaleWidth      =   300
End
Attribute VB_Name = "WheelCtl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'D�claration du nouvel Event
Public Event WheelRotate(Delta As Long, Shift As Long, x As Long, y As Long)

Private Sub UserControl_Terminate()
    'Arr�t de la fonction hook d'�coute des messages de la fen�tre
    UnhookWindowsHookEx hHook
End Sub

Public Sub unHook()
    'Arr�t de la fonction hook d'�coute des messages de la fen�tre
    UnhookWindowsHookEx hHook
End Sub

Public Sub WheelMoved(Delta As Long, Shift As Long, x As Long, y As Long)
    'Event de d�placement de la roulette
    RaiseEvent WheelRotate(Delta, Shift, x, y)
End Sub
