VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.MDIForm MDIMain 
   BackColor       =   &H8000000C&
   Caption         =   "MDIForm1"
   ClientHeight    =   7560
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   12465
   Icon            =   "MDIForm1.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   2  'CenterScreen
   Begin MSComDlg.CommonDialog OpenDlg 
      Left            =   4440
      Top             =   1080
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   4
      Top             =   7305
      Width           =   12465
      _ExtentX        =   21987
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13600
            MinWidth        =   5292
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   5292
            MinWidth        =   5292
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            TextSave        =   "09:44"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picSplit 
      Align           =   3  'Align Left
      BorderStyle     =   0  'None
      Height          =   7305
      Left            =   2880
      MousePointer    =   9  'Size W E
      ScaleHeight     =   7305
      ScaleWidth      =   105
      TabIndex        =   3
      Top             =   0
      Width           =   105
   End
   Begin VB.PictureBox picNaviga 
      Align           =   3  'Align Left
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   7305
      Left            =   0
      ScaleHeight     =   487
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   192
      TabIndex        =   0
      Top             =   0
      Width           =   2880
      Begin MSComctlLib.TreeView TreeNaviga 
         Height          =   1455
         Left            =   240
         TabIndex        =   2
         Top             =   1440
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   2566
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   397
         LabelEdit       =   1
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin VB.PictureBox picTitre 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         Height          =   90
         Left            =   240
         ScaleHeight     =   6
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   128
         TabIndex        =   1
         Top             =   840
         Width           =   1920
      End
      Begin VB.Image imgClose 
         Height          =   165
         Left            =   2400
         Picture         =   "MDIForm1.frx":08CA
         Top             =   240
         Width           =   165
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4920
      Top             =   1560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":0A98
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":0BF2
            Key             =   "Close"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":0D4C
            Key             =   "Spool"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":0EA6
            Key             =   "Page"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":1000
            Key             =   "Warning"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":115A
            Key             =   "PageSelected"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   5520
      Top             =   2160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":12B4
            Key             =   "MAIL"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":1B8E
            Key             =   "MESSGAERIE"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFichier 
      Caption         =   ""
      Begin VB.Menu mnuLoadFile 
         Caption         =   ""
      End
      Begin VB.Menu mnuCloseFile2 
         Caption         =   ""
      End
      Begin VB.Menu mnuGestFile 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPrint 
         Caption         =   ""
         Visible         =   0   'False
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuExit 
         Caption         =   ""
      End
   End
   Begin VB.Menu mnuFenetre 
      Caption         =   ""
      Begin VB.Menu mnuSpool 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuMosaiqueH 
         Caption         =   ""
      End
      Begin VB.Menu mnuMosaiqueV 
         Caption         =   ""
      End
      Begin VB.Menu mnuCascade 
         Caption         =   ""
      End
      Begin VB.Menu mnuReorIcone 
         Caption         =   ""
      End
      Begin VB.Menu mnusep4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   0
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   1
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   2
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   3
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   4
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   5
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   6
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   7
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   8
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   9
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   10
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   11
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   12
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   13
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   14
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   15
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   16
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   17
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   18
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   19
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   20
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   21
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   22
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   23
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   24
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   25
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   26
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "mnuPopUp"
      Visible         =   0   'False
      Begin VB.Menu mnuCloseFile 
         Caption         =   ""
      End
      Begin VB.Menu mnuDelFile 
         Caption         =   ""
         Begin VB.Menu mnuDeleteFile 
            Caption         =   ""
         End
         Begin VB.Menu mnuPoubelleFile 
            Caption         =   ""
         End
      End
      Begin VB.Menu mnuPrintObj 
         Caption         =   ""
      End
      Begin VB.Menu mnuProp 
         Caption         =   ""
      End
      Begin VB.Menu mnuSendTo 
         Caption         =   ""
         Begin VB.Menu mnuInternet 
            Caption         =   ""
            Visible         =   0   'False
         End
         Begin VB.Menu mnuMessagerie 
            Caption         =   ""
         End
         Begin VB.Menu mnuNotePad 
            Caption         =   ""
         End
      End
   End
   Begin VB.Menu mnuDeal 
      Caption         =   "?"
      Begin VB.Menu mnuApropos 
         Caption         =   ""
      End
      Begin VB.Menu mnuConfig 
         Caption         =   ""
      End
      Begin VB.Menu mnuMaj 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "MDIMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub imgClose_Click()

'-> Simuler  un click sur le menu Fermeture de l'explorateur de spool
mnuSpool_Click

End Sub

Private Sub MDIForm_Load()

Dim aLb As Libelle

'-> Activer la gestion des erreurs
On Error Resume Next

'-> Tester si Outlook est connect�
OutLookOk = IsOutLook

'-> Tester si Excel OK
ExcelOk = IsExcel
OpenOfficeOk = IsOpenoffice

'-> Charger les libelles par langue
Set aLb = Libelles("MDIMAIN")

'-> Titre de la fen�tre
Me.Caption = aLb.GetCaption(10)

'-> Menu fichier
Me.mnuFichier.Caption = aLb.GetCaption(1)
Me.mnuLoadFile.Caption = aLb.GetCaption(2)
Me.mnuCloseFile2.Caption = aLb.GetCaption(13)
Me.mnuGestFile.Caption = aLb.GetCaption(27)
Me.mnuPrint.Caption = aLb.GetCaption(3)
Me.mnuExit.Caption = aLb.GetCaption(4)

'-> Menu Fen�tre
Me.mnuFenetre.Caption = aLb.GetCaption(5)
Me.mnuSpool.Caption = aLb.GetCaption(6)
Me.mnuMosaiqueH.Caption = aLb.GetCaption(7)
Me.mnuMosaiqueV.Caption = aLb.GetCaption(8)
Me.mnuCascade.Caption = aLb.GetCaption(9)
Me.mnuReorIcone.Caption = aLb.GetCaption(20)

'-> Menu DEAL
Me.mnuApropos.Caption = aLb.GetCaption(21)
Me.mnuMaj.Caption = aLb.GetCaption(26)

If VersionTurbo = 1 And GetIniString("PARAM", "SHOWCONFIG", TurboGraphIniFile, False) <> "0" Then
    Me.mnuMaj.Visible = True
    Me.mnuConfig.Visible = True
    Me.mnuConfig.Caption = aLb.GetCaption(29)
Else
    Me.mnuConfig.Visible = False
    Me.mnuMaj.Visible = False
End If

'-> on r�initialise
For i = 1 To MDIMain.mnuJoin.Count
    MDIMain.mnuJoin.Item(i - 1).Visible = False
    Fichiers(FileName).Spools(1).frmdisplay.Toolbar1.Buttons("FILEJOIN").ButtonMenus(i).Visible = False
Next

'-> menu POPUP
Me.mnuCloseFile.Caption = aLb.GetCaption(13)
Me.mnuPrintObj.Caption = aLb.GetCaption(14)
Me.mnuProp.Caption = aLb.GetCaption(15)
Me.mnuSendTo.Caption = aLb.GetCaption(16)
Me.mnuInternet.Caption = aLb.GetCaption(17)
Me.mnuMessagerie.Caption = aLb.GetCaption(18)
Me.mnuNotePad.Caption = aLb.GetCaption(19)
Me.mnuDelFile.Caption = aLb.GetCaption(22)
Me.mnuDeleteFile.Caption = aLb.GetCaption(23)
Me.mnuPoubelleFile.Caption = aLb.GetCaption(24)

'-> Masquer le mnue envoyer vers OutLook si pas outlook
Me.mnuMessagerie.Visible = OutLookOk

'-> Liberer le pointeur sur le libelle
Set aLb = Nothing

'-> Par d�faut, palette de navigation ferm�e
If GetIniString("PARAM", "NAVIGA", TurboGraphIniFile, False) = "1" Then
    IsNaviga = True
Else
    IsNaviga = False
End If
Me.picSplit.Visible = IsNaviga
Me.picNaviga.Visible = IsNaviga

'-> on regarde si on ouvre en plein ecran
Dim lpBuffer As String
lpBuffer = GetIniString("PARAM", "FULLSCREEN", App.Path & "\Turbograph.ini", False)
If lpBuffer <> "NULL" Then
    If Trim(lpBuffer) = "1" Then Me.WindowState = 2
End If

End Sub


Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)

End

End Sub

Private Sub mnuApropos_Click()

'---> Afficher la page Apropos de
Load AproposDe
AproposDe.Show vbModal

End Sub

Private Sub mnuCloseFile2_Click()

'-> Demande de fermeture de fichier
On Error Resume Next
CloseFichier Me.ActiveForm.aSpool.FileName

End Sub

Private Sub mnuConfig_Click()

'---> Afficher la page Apropos de
Load frmConfig
frmConfig.Show

End Sub

Private Sub mnuFichier_Click()

If Me.TreeNaviga.Nodes.Count = 0 Then
    mnuCloseFile2.Enabled = False
Else
    mnuCloseFile2.Enabled = True
End If

End Sub

Private Sub mnuGestFile_Click()

'--> ouvrir le filtre des spools
LockWindowUpdate Me.hWnd
frmGestSpool.Show
LockWindowUpdate 0

End Sub

Private Sub mnuMaj_Click()

'---> Afficher la page Apropos de
Find_Update
End Sub

Private Sub mnuCascade_Click()

Me.Arrange vbCascade

End Sub

Private Sub mnuCloseFile_Click()

'-> Demande de fermeture de fichier
CloseFichier Me.TreeNaviga.SelectedItem.Key

End Sub

Private Sub mnuDeleteFile_Click()

'-> Supprimer de mani�re d�finitive le fichier
DeleteFile Me.TreeNaviga.SelectedItem.Key, 0

End Sub

Private Sub mnuExit_Click()

Unload Me

End Sub

Private Sub mnuInternet_Click()

'-> Setting de la valeur � envoyer
strRetour = Me.TreeNaviga.SelectedItem.Key

'-> Envoyer la cr�ation du fichier
frmMail.Show vbModal

End Sub

Private Sub mnuJoin_Click(Index As Integer)
'-> Lancer si necessaire
ShellExecute MDIMain.hWnd, "Open", mnuJoin.Item(Index).Tag, vbNullString, App.Path, 1
End Sub

Private Sub mnuLoadFile_Click()

Dim lpBuffer As String
Dim Res As Long
Dim IniPath As String
Dim Extension As String
Dim ExtensionOperat As String
Dim DefFileName As String
Dim aLb As Libelle
Dim FileName As String
Dim lngResult As Long
Dim strBuffer As String

On Error GoTo GestError

'-> R�pertoire par d�faut sp�cifi� dans le registre par la cle DEALTEMPO
If Not FirstOpen Then
    '-> V�rifier si un path est sp�cifi�
    If OpenPath <> "" Then
        Me.OpenDlg.InitDir = OpenPath
        FirstOpen = True
    End If
Else
    Me.OpenDlg.InitDir = ""
End If

'-> G�n�rer une erreur sur ANNULER
Me.OpenDlg.CancelError = True

'-> R�cup�rer le code op�rateur pour l'extension par d�faut
DefFileName = "*.Turbo"
Extension = "Fichiers Turbo (*.Turbo)|*.Turbo|Tous les fichiers (*.*)|*.*"
lpBuffer = GetVariableEnv("DEALOPERAT")
If lpBuffer <> "" Then
    ExtensionOperat = "Fichiers op�rateur (" & lpBuffer & ")|" & lpBuffer
    Extension = ExtensionOperat & "|" & Extension
    DefFileName = lpBuffer
Else
    ExtensionOperat = ""
End If

Me.OpenDlg.FileName = DefFileName
Me.OpenDlg.DefaultExt = DefFileName
Me.OpenDlg.Filter = Extension

'-> Flags d'ouverture
Me.OpenDlg.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly Or _
                   cdlOFNPathMustExist

'-> Afficher la feuille
Me.OpenDlg.ShowOpen

'-> V�rifier ici que le fichier n'est pas d�ja charg� THIERRY
If IsLoadedFile(Me.OpenDlg.FileName) Then
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(2), vbExclamation + vbOKOnly, aLb.GetToolTip(2)
    Exit Sub
End If

'-> Affecter le nom
FileName = Me.OpenDlg.FileName

'-> On regarde si il y a un programme associ� different de turbo
'strBuffer = Space$(260)
'lngResult = FindExecutable(fileName, "", strBuffer)
'If strBuffer <> "" And InStr(1, strBuffer, "turbograph", vbTextCompare) = 0 And Len(Trim(strBuffer)) > 6 Then
'    '-> on lance l'application associ�e
'    strBuffer = Left(strBuffer, Len(strBuffer) - Len(Dir(strBuffer)))
'    lngResult = ShellExecute(Me.hwnd, "Open", fileName, vbNullString, strBuffer, 1)
'    Exit Sub
'End If

'-> Test surt le ZIP
FilePath = Mid(FileName, 1, InStrRev(Replace(FileName, "\", "/"), "/"))
FileName = GetUnzipFileName(FileName)

'-> Lancer le chargement du fichier
DisplayFileGUI FileName

'-> Charger eventuellement au menu les fichiers joints
LoadJoinFile uZipInfo2, FileName

GestError:

Exit Sub
    

End Sub

Private Function IsLoadedFile(ByVal NomFile As String) As Boolean

'---> Cette proc�dure v�rifie si un fichier est d�ja charg�

Dim aFichier As Fichier

On Error GoTo GestError

'-> Pointer sur le fichier
Set aFichier = Fichiers(Trim(UCase$(NomFile)))

'-> Renvoyer une valeur de succ�s
IsLoadedFile = True

Exit Function

GestError:
    IsLoadedFile = False


End Function

Private Sub mnuMessagerie_Click()

'-> Setting de la valeur � envoyer
strRetour = Me.TreeNaviga.SelectedItem.Key

If InStr(1, strRetour, "�PAGE") <> 0 Then
    '-> Envoie d'une page
    frmMail.lvSend = 2
ElseIf InStr(1, strRetour, "�SPOOL") <> 0 Then
    '-> Envoie du spool
    frmMail.lvSend = 1
Else
    '-> Envoie du fichier
    frmMail.lvSend = 0
End If

'-> Envoyer la cr�ation du fichier
frmMail.Show vbModal

End Sub

Private Sub mnuMosaiqueH_Click()

Me.Arrange vbTileHorizontal

End Sub

Private Sub mnuMosaiqueV_Click()

Me.Arrange vbTileVertical

End Sub

Private Sub mnuNotePad_Click()
    
Dim aFichier As Fichier
Dim aSpool As Spool
Dim aPage As Integer
Dim i As Integer, j As Integer
Dim aLb As Libelle
Dim tmpPafe As String


On Error GoTo GestError

'-> Pointer sur le fichier
Set aFichier = Fichiers(Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))

'-> Charger l'�diteur
Load frmEditor

'-> Pointer sur la classe libelle
Set aLb = Libelles("FRMEDITOR")

MDIMain.Enabled = False
Screen.MousePointer = 11

'-> Envoyer le titre de l'impression
AddTexte aLb.GetCaption(16) & " " & aFichier.FileName & Chr(13) & Chr(10)

'-> Gestion de la temporisation
TailleLue = 0
TailleTotale = 0

'-> Envoie vers l'�diteur de texte
Select Case NumEntries(Me.TreeNaviga.SelectedItem.Key, "�")
    Case 1 '-> Envoie du fichier en entier
                
        '-> Calcul du nombre de lignes � imprimer
        For i = 1 To aFichier.Spools.Count
            Set aSpool = aFichier.Spools(i)
            For j = 1 To aSpool.NbPage
                InitTailleTotale aSpool, j
            Next
        Next
        '-> Pour tous les spools du fichier
        For i = 1 To aFichier.Spools.Count
            '-> Pointer sur le spool
            Set aSpool = aFichier.Spools(i)
            '-> Envoyer le titre de l'impression
            AddTexte aLb.GetCaption(17) & " " & aSpool.Key & Chr(13) & Chr(10)
            '-> Imprimer toutes les pages
            For j = 1 To aSpool.NbPage
                SendPage aSpool, j
                '-> Ajouter le mot cle de saut de page
                AddTexte "[PAGE]"
            Next
        Next
    Case 2 '-> Envoie d'un spool
        '-> Pointer sur le spool
        Set aSpool = aFichier.Spools(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�"))
        '-> Inti de la temporisation
        For i = 1 To aSpool.NbPage
            InitTailleTotale aSpool, i
        Next
        '-> Imprimer toutes les pages du spool
        '-> Envoyer le titre de l'impression
        AddTexte aLb.GetCaption(17) & " " & aSpool.Key & Chr(13) & Chr(10)
        For j = 1 To aSpool.NbPage
            SendPage aSpool, j
            '-> Ajouter le mot cle de saut de page
            AddTexte "[PAGE]"
        Next '-> Envoie d'une page
    Case 3
        '-> Pointer sur le spool
        Set aSpool = aFichier.Spools(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�"))
        '-> Envoyer le titre de l'impression
        AddTexte aLb.GetCaption(17) & " " & aSpool.Key & Chr(13) & Chr(10)
        AddTexte aLb.GetCaption(18) & " " & CInt(Entry(2, Entry(3, Me.TreeNaviga.SelectedItem.Key, "�"), "|")) & Chr(13) & Chr(10)
        '-> Init de la temporisation
        InitTailleTotale aSpool, CInt(Entry(2, Entry(3, Me.TreeNaviga.SelectedItem.Key, "�"), "|"))
        '-> Imprimer la page sp�cifi�e
        SendPage aSpool, CInt(Entry(2, Entry(3, Me.TreeNaviga.SelectedItem.Key, "�"), "|"))
End Select
    
GestError:

    MDIMain.Enabled = True
    Screen.MousePointer = 0
    
    MDIMain.StatusBar1.Refresh
    
    '-> Afficher l'�diteur
    frmEditor.Show

    
End Sub

Private Function InitTailleTotale(ByRef aSpool As Spool, pageToPrint As Integer) As Integer

Dim tmpPage As String


'-> V�rifier s'il y a des erreurs sur la page
If aSpool.GetErrorPage(pageToPrint) <> "" Then
    tmpPage = aSpool.GetErrorPage(pageToPrint)
Else
    tmpPage = aSpool.GetPage(pageToPrint)
End If
TailleTotale = TailleTotale + NumEntries(tmpPage, Chr(0))

End Function

Private Sub SendPage(ByRef aSpool As Spool, ByVal pageToPrint As Integer)

'---> Cette proc�dure envoie une page vers l'�diteur de texte

Dim i As Integer
Dim tmpPage As String

'-> R�cup�rer la page � imprimer ou ses erreurs
If aSpool.GetErrorPage(pageToPrint) <> "" Then
    tmpPage = aSpool.GetErrorPage(pageToPrint)
Else
    tmpPage = aSpool.GetPage(pageToPrint)
End If

For i = 1 To NumEntries(tmpPage, Chr(0))
    If frmEditor.RichTextBox1.Text = "" Then
        frmEditor.RichTextBox1.Text = Entry(i, tmpPage, Chr(0))
    Else
        frmEditor.RichTextBox1.Text = frmEditor.RichTextBox1.Text & Chr(13) & Chr(10) & Entry(i, tmpPage, Chr(0))
    End If
    TailleLue = TailleLue + 1
    Call DrawWait
Next


End Sub

Private Sub AddTexte(ByVal TextToAdd As String)

'---> Cette proc�dure ajoute une ligne de texte � l'�diteur

If frmEditor.RichTextBox1.Text = "" Then
    frmEditor.RichTextBox1.Text = TextToAdd
Else
    frmEditor.RichTextBox1.Text = frmEditor.RichTextBox1.Text & Chr(13) & Chr(10) & TextToAdd
End If

End Sub


Private Sub mnuPoubelleFile_Click()

'-> Envoyer le fichier vers la poubelle
DeleteFile Me.TreeNaviga.SelectedItem.Key, FOF_ALLOWUNDO

End Sub

Private Sub mnuPrintObj_Click()

'---> Cette proc�dure imprimer l'�l�ment s�lectionn�

Dim PrinterDevice As String
Dim FileToPrint As String
Dim aFichier As Fichier
Dim aSpool As Spool
Dim NumPage As Integer
Dim aLb As Libelle
Dim NbCopies As Integer
Dim RectoVerso As String


'-> Vider la variable de retour
strRetour = ""

'-> Charger la feuille de l'imprimante
Load frmPrint

'-> Bloquer les mauvaises zones
frmPrint.Frame2.Enabled = False
frmPrint.Option1.Value = True
frmPrint.Option1.Enabled = False
frmPrint.Option2.Enabled = False
frmPrint.Option3.Enabled = False
frmPrint.Option4.Enabled = False

'-> Selon le node sr lequel on a imprim�
Select Case Me.TreeNaviga.SelectedItem.Tag
    Case "FICHIER"
        
        '-> Afficher la feuille
        frmPrint.Show vbModal
        
        '-> Quitter si annuler
        If strRetour = "" Then Exit Sub
        
        '-> R�cup du nom du printer
        PrinterDevice = Entry(1, strRetour, "|")
        
        '-> Recup du nombre de copies
        NbCopies = CInt(Entry(3, strRetour, "|"))
        
        '-> recup du parampetre recto-verso
        RectoVerso = Entry(4, strRetour, "|")
        
        '-> Faire un run directement de l'impression du fichier sp�cifi�
        Shell App.Path & "\TurboGraph.exe " & PrinterDevice & "~DIRECT~" & NbCopies & "|" & Me.TreeNaviga.SelectedItem.Key & "|" & RectoVerso, vbNormalFocus
        
    Case "SPOOL"
        '-> Imprimer le fichier en entier s'il n'y a qu'un seul spool dans le fichier
        
        '-> Afficher la feuille
        frmPrint.Show vbModal
        
        '-> Quitter si annuler
        If strRetour = "" Then Exit Sub
        
        '-> R�cup du nom du printer
        PrinterDevice = Entry(1, strRetour, "|")
        
        '-> Recup du nombre de copies
        NbCopies = CInt(Entry(3, strRetour, "|"))
             
        '-> recup du parampetre recto-verso
        RectoVerso = Entry(4, strRetour, "|")
             
        '-> Pointer sur l'objet sp�cifi�
        Set aFichier = Fichiers(Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))
        
        If aFichier.Spools.Count = 1 Then
            '-> Impression du fichier
            Shell App.Path & "\TurboGraph.exe " & PrinterDevice & "~DIRECT~" & NbCopies & "|" & aFichier.FileName & "|" & RectoVerso, vbNormalFocus
        Else
            '-> Pointer sur le spool
            Set aSpool = aFichier.Spools(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�"))
            '-> Cr�er le fichier � imprimer
            FileToPrint = CreateDetail(aSpool, -1)
            '-> Lancer l'impression
            Shell App.Path & "\TurboGraph.exe " & PrinterDevice & "~DIRECT~" & NbCopies & "|" & FileToPrint & "|" & RectoVerso, vbNormalFocus
        End If
                
    Case "PAGE"
    
        '-> Setting de l'option 3
        frmPrint.Option3.Value = True
    
        '-> Afficher la feuille
        frmPrint.Show vbModal
        
        '-> Quitter si annuler
        If strRetour = "" Then Exit Sub
        
        '-> R�cup du nom du printer
        PrinterDevice = Entry(1, strRetour, "|")
        
        '-> Recup du nombre de copies
        NbCopies = CInt(Entry(3, strRetour, "|"))
        
        '-> recup du parampetre recto-verso
        RectoVerso = Entry(4, strRetour, "|")
        
        '-> Pointer sur l'objet sp�cifi�
        Set aFichier = Fichiers(Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))
        '-> Pointer sur le spool
        Set aSpool = aFichier.Spools(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�"))
        '-> R�cup�rer le num�ro de la page � imprimer
        NumPage = CInt(Entry(2, Entry(3, Me.TreeNaviga.SelectedItem.Key, "�"), "|"))
        '-> Cr�er le fichier � imprimer
        FileToPrint = CreateDetail(aSpool, NumPage)
        '-> Lancer l'impression
        Shell App.Path & "\TurboGraph.exe " & PrinterDevice & "~DIRECT~" & NbCopies & "|" & FileToPrint & "|" & RectoVerso, vbNormalFocus
End Select


End Sub

Private Sub mnuProp_Click()

'---> Affiche la page de propri�t� d'un fichier

On Error Resume Next

ShowFileProperties Me.TreeNaviga.SelectedItem.Key

End Sub

Private Sub mnuReorIcone_Click()
Me.Arrange vbArrangeIcons
End Sub

Private Sub mnuSpool_Click()

'-> Afficher ou ne pas afficher le volet expmloration de spool
IsNaviga = Not IsNaviga
Me.mnuSpool.Checked = IsNaviga
Me.picSplit.Visible = IsNaviga
Me.picNaviga.Visible = IsNaviga

End Sub

Private Sub picNaviga_Resize()

Dim aRect As RECT
Dim Res As Long

On Error Resume Next

'-> R�cup�rer la taille de la zone cliente
Res = GetClientRect(Me.picNaviga.hWnd, aRect)

'-> Positionner l'entete de la fen�tre
Me.picTitre.Left = 3
Me.picTitre.Top = 7
Me.picTitre.Width = aRect.Right - 6 - imgClose.Width
Me.imgClose.Left = aRect.Right - imgClose.Width
Me.imgClose.Top = Me.picTitre.Top - 2

'-> Positionner le treeview
Me.TreeNaviga.Left = Me.picTitre.Left
Me.TreeNaviga.Top = Me.picTitre.Top * 2 + Me.picTitre.Height
Me.TreeNaviga.Width = Me.picTitre.Width + Me.imgClose.Width + 6
Me.TreeNaviga.Height = Me.picNaviga.ScaleY(Me.picNaviga.Height, 1, 3) - 21


End Sub

Private Sub picSplit_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

Dim apt As POINTAPI
Dim Res As Long

If Button = vbLeftButton Then
    '-> R�cup�rer la position du curseur
    Res = GetCursorPos(apt)
    '-> Convertir en coordonn�es clientes
    Res = ScreenToClient(Me.hWnd, apt)
    '-> Tester les positions mini et maxi
    If apt.X > Me.picNaviga.ScaleX(2, 7, 3) And apt.X < Me.picNaviga.ScaleX(Me.Width, 1, 3) - Me.picNaviga.ScaleX(2, 7, 3) Then Me.picNaviga.Width = Me.picNaviga.ScaleX(apt.X, 3, 1)
End If

End Sub

Private Sub picTitre_Resize()

'---> Dessiner la barre de titre
Me.picTitre.Line (0, 0)-(Me.picTitre.Width - 1, 0), RGB(255, 255, 255)
Me.picTitre.Line (0, 1)-(Me.picTitre.Width - 1, 1), RGB(255, 255, 255)
Me.picTitre.Line (1, 1)-(Me.picTitre.Width - 1, 1), &HE0E0E0
Me.picTitre.Line (0, 3)-(Me.picTitre.Width - 1, 3), RGB(255, 255, 255)
Me.picTitre.Line (0, 4)-(Me.picTitre.Width - 1, 4), RGB(255, 255, 255)
Me.picTitre.Line (1, 4)-(Me.picTitre.Width - 1, 4), &HE0E0E0

End Sub

Public Sub TreeNaviga_DblClick()

'---> Traiter l'activation des spools et des pages

Dim aFichier As Fichier
Dim aSpool As Spool

'-> Quitter si pas de nodes
If Me.TreeNaviga.SelectedItem Is Nothing Then Exit Sub

'-> Pointer sur le fichier associ�
Set aFichier = Fichiers(Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))
'-> Pointer sur le spool associ�
If Me.TreeNaviga.SelectedItem.Tag <> "FICHIER" Then Set aSpool = aFichier.Spools(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�")) 'THIERRY

'-> selon le node surlequel on click
Select Case Me.TreeNaviga.SelectedItem.Tag

    Case "FICHIER"
        '-> Ne rien faire
    Case "SPOOL"
        '-> V�rifier si la visu de ce fichier est en cours
        If aSpool.frmdisplay Is Nothing Then InitFRMDISPLAY aSpool
        
        '-> V�rifier s'il y a des erreurs
        If aSpool.NbError = 0 Then
            '-> Imprimer la page surlaquelle on a cliqu�
            PrintPageSpool aSpool, 1 'CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|"))
            '-> Afficher la page
            aSpool.DisplayInterfaceByPage 1 'CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|"))
        Else
            aSpool.DisplayInterfaceByPage 1
        End If
                    
        '-> Afficher la page
        aSpool.frmdisplay.ZOrder
    
    Case "ERROR"
        
        '-> V�rifier si la visu de ce fichier est en cours
        If aSpool.frmdisplay Is Nothing Then InitFRMDISPLAY aSpool
        
        '-> 2 types d'erreurs : soit fatale soit page
        If InStr(1, Me.TreeNaviga.SelectedItem.Key, "PAGE|") <> 0 Then
            '-> C'est une erreur de page : Afficher la page
            '-> V�rifier que l'on demande d'afficher une page diff�rente
            If CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|")) <> aSpool.CurrentPage Then
                '-> Imprimer la page
                PrintPageSpool aSpool, CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|"))
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage aSpool.CurrentPage
            End If
            Exit Sub
        Else
            '-> Erreur fatale
            
            '-> Afficher la feuille en fonction du r�sultat de l'impression
            aSpool.DisplayInterfaceByPage (1)
        End If
        
        '-> Afficher la page
        aSpool.frmdisplay.ZOrder
            
    Case "PAGE"
        '-> V�rifier si la visu de ce fichier est en cours
        If aSpool.frmdisplay Is Nothing Then
            InitFRMDISPLAY aSpool
            '-> Imprimer la page surlaquelle on a cliqu�
            PrintPageSpool aSpool, CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|"))
            '-> Afficher la page
            aSpool.DisplayInterfaceByPage CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|"))
        Else
            '-> V�rifier que l'on demande d'afficher une page diff�rente
            If CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|")) <> aSpool.CurrentPage Then
                '-> Imprimer la page
                PrintPageSpool aSpool, CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|"))
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage aSpool.CurrentPage
            End If
        End If
        
        '-> Mettre la feuille au premier plan
        aSpool.frmdisplay.ZOrder
        '-> on affiche les notes de la page
        DisplayNotes aSpool
        
End Select


End Sub

Public Sub InitFRMDISPLAY(ByRef aSpool As Spool)

Dim aFrm As frmDisplaySpool

'-> Cr�er une nouvelle instance
Set aFrm = New frmDisplaySpool
'-> Affect� le spool
Set aFrm.aSpool = aSpool
'-> Affecter la feuille au spool
Set aSpool.frmdisplay = aFrm

End Sub

Private Sub TreeNaviga_KeyDown(KeyCode As Integer, Shift As Integer)

Dim aSpool As Spool
Dim aFichier As Fichier

If Me.TreeNaviga.SelectedItem Is Nothing Then Exit Sub
If Me.TreeNaviga.SelectedItem.Tag <> "SPOOL" Then Exit Sub

'-> Pointer sur le fichier
Set aFichier = Fichiers(Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))

'-> Pointer sur le spool
Set aSpool = aFichier.Spools(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�"))

If KeyCode = 112 Then
    If aSpool.MaquetteASCII = "" Then
        MsgBox "Maquette sp�cifi�e dans le fichier : " & Chr(13) + Chr(10) & aSpool.maquette.Nom, vbInformation + vbOKOnly, "Nom de la maquette"
    Else
        MsgBox aSpool.MaquetteASCII, vbInformation + vbOKOnly, "Nom de la maquette"
    End If
End If


End Sub

Private Sub TreeNaviga_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

'-> Teste sur le bouton
If Button <> vbRightButton Then Exit Sub

'-> S�lectionner le node
Set Me.TreeNaviga.SelectedItem = Me.TreeNaviga.HitTest(X, Y)

'-> Quitter si pas de node
If Me.TreeNaviga.SelectedItem Is Nothing Then Exit Sub

'-> Afficher le menu selon le node
Select Case Me.TreeNaviga.SelectedItem.Tag
    Case "FICHIER"
        Me.mnuCloseFile.Visible = True
        Me.mnuProp.Visible = True
        Me.mnuDelFile.Visible = True
    Case "SPOOL", "PAGE", "ERROR"
        Me.mnuCloseFile.Visible = False
        Me.mnuProp.Visible = False
        Me.mnuDelFile.Visible = False
End Select

'-> Afficher le mennu contextuel
Me.PopupMenu Me.mnuPopup

End Sub

Public Function DisplayNotes(aSpool As Spool)
'--> cette fonction permet d'afficher les notes de la page en cours
On Error Resume Next

If fNotes Is Nothing Then Exit Function

For Each fNote In fNotes
    If fNote.Tag = aSpool.FileName & "|" & aSpool.Num_Spool & "|" & aSpool.CurrentPage Then
        fNote.Init aSpool.frmdisplay
        fNote.Visible = True
    Else
        fNote.Visible = False
    End If
Next

End Function





