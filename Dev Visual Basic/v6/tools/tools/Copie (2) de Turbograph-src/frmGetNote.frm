VERSION 5.00
Begin VB.Form frmGetNote 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   1485
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5670
   Icon            =   "frmGetNote.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1485
   ScaleWidth      =   5670
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Height          =   1215
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5415
      Begin VB.CommandButton Command1 
         Caption         =   "Command1"
         Height          =   375
         Left            =   4200
         TabIndex        =   3
         Top             =   760
         Width           =   1095
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   1680
         TabIndex        =   2
         Top             =   360
         Width           =   3615
      End
      Begin VB.Image Image1 
         Height          =   720
         Left            =   120
         Picture         =   "frmGetNote.frx":0ECA
         Top             =   240
         Width           =   720
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   255
         Left            =   840
         TabIndex        =   1
         Top             =   360
         Width           =   2295
      End
   End
End
Attribute VB_Name = "frmGetNote"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public NbMax As Integer
Public NbMin As Integer

Private Sub Command1_Click()

On Error Resume Next
     
'-> Renvoyer le num�ro de la page
strRetour = Me.Text1.Text
    
'-> D�charger la feuille
Unload Me

Exit Sub

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

If KeyCode = vbKeyEscape Then Unload Me

End Sub

Private Sub Form_Load()

Dim aLb As Libelle

On Error Resume Next

'-> Pointer sur la classe libelles
Set aLb = Libelles("FRMNOTE")

Me.Caption = aLb.GetCaption(6)
Me.Label1.Caption = aLb.GetCaption(1)

'-> Pointer sur la classe libelles des boutons
Set aLb = Libelles("BOUTONS")
Me.Command1.Caption = aLb.GetCaption(1)

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Command1_Click

End Sub
