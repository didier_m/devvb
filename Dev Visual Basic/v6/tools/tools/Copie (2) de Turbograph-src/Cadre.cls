VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cadre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'**********************************************
'*                                            *
'* Cette classe contient la d�finition d'un   *
'* Objet cadre                                *
'*                                            *
'**********************************************

'---> D�finition des propri�t�s
Public Nom As String
Public Hauteur As Single
Public Largeur As Single
Public Top As Single
Public Left As Single
Public BackColor As Long
Public IdAffichage As Integer
Public MargeInterne As Single
Public LargeurTrait As Integer
Public IsRoundRect As Boolean
Public SectionName As String

'---> Variable de stockage priv�
Public Bas As Boolean
Public Haut As Boolean
Public Gauche As Boolean
Public Droite As Boolean

Public IdRTf As Integer

'-> Pour chargement du contenu RTF
Public TempoRTF As Integer 'stocker le handle du fichier ouvert
Public TempoRtfString As String ' stocker le nom du fichier ouvert



Private Sub Class_Initialize()

    LargeurTrait = 1

End Sub
