Attribute VB_Name = "ClientSMTP"
Option Explicit

Private Declare Function SetFilePointer Lib "kernel32" _
(ByVal hFile As Long, ByVal lDistanceToMove As Long, lpDistanceToMoveHigh As Long, ByVal dwMoveMethod As Long) As Long

Private Declare Function ReadFile Lib "kernel32" _
(ByVal hFile As Long, lpBuffer As Any, ByVal nNumberOfBytesToRead As Long, lpNumberOfBytesRead As Long, ByVal lpOverlapped As Any) As Long

Private Const EOL_SIZE      As Long = 2                ' Size of vbCrLf
Private Const LINE_SIZE     As Long = 40 + EOL_SIZE    ' Size of a line

' Base64 official const
Private Const Base64        As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
Private Const Base64_EOF    As String = "="
Private Const GENERIC_READ = &H80000000
Private Const FILE_SHARE_READ = &H1
Private Const FILE_SHARE_WRITE = &H2
Private Const OPEN_EXISTING = 3
Private Const FILE_BEGIN = 0

Public Function Encode64(ByRef lpConvString As String) As String
    
    
    Dim pt          As Long
    Dim ptMax       As Long
    Dim dwBuffer    As Long
    Dim cbSize      As Long
    Dim cbBits      As Byte
    Dim cbLines     As Long
    Dim aByte       As Byte
    Dim lpBuffer    As String
    
    
    dwBuffer = 0
    cbBits = 0
    cbSize = EOL_SIZE                       ' Tips for the NewLine
    cbLines = 1
    pt = 1
    ptMax = Len(lpConvString)
    lpBuffer = String$(LINE_SIZE + ptMax * 2, 0)
    
    Do Until pt > ptMax
        
        ' Add 8 bits to the buffer
        dwBuffer = dwBuffer * 256 + Asc(Mid$(lpConvString, pt, 1))
        cbBits = cbBits + 8
        
        Do
            
            Select Case cbBits
            Case 6
                aByte = dwBuffer
                dwBuffer = 0
                cbBits = 0
            Case 8
                aByte = dwBuffer \ 4
                dwBuffer = dwBuffer And 3
                cbBits = 2
            Case 10
                aByte = dwBuffer \ 16
                dwBuffer = dwBuffer And 15
                cbBits = 4
            Case 12
                aByte = dwBuffer \ 64
                dwBuffer = dwBuffer And &H3F
                cbBits = 6
                
            Case 2 ' Only when pt = ptmax
                aByte = dwBuffer * 16
                dwBuffer = 0
                cbBits = 0
            Case 4 ' Only when pt = ptmax
                aByte = dwBuffer * 4
                dwBuffer = 0
                cbBits = 0
            
            End Select
        
            ' Add a character to the buffer
            cbSize = cbSize + 1
            Mid$(lpBuffer, cbSize, 1) = Mid$(Base64, 1 + aByte, 1)
            
            ' Add the NewLine to the buffer
            If (cbSize Mod LINE_SIZE) = 0 Then
                Mid$(lpBuffer, cbSize + 1, EOL_SIZE) = vbCrLf
                cbSize = cbSize + EOL_SIZE
                cbLines = cbLines + 1
            End If
        
        ' Loop while not done with this byte
        Loop While (cbBits = 6) Or ((pt = ptMax) And (cbBits > 0))
        
        pt = pt + 1
    
    Loop
    
    ' Add one or two bytes Base64_EOF
    Select Case (cbSize - EOL_SIZE * cbLines) Mod 3
      Case 1: '8 bit final
        Mid$(lpBuffer, cbSize + 1, 2) = Base64_EOF & Base64_EOF
        cbSize = cbSize + 2
      Case 2: '16 bit final
        Mid$(lpBuffer, cbSize + 1, 1) = Base64_EOF
        cbSize = cbSize + 1
    End Select
    
    ' Return the string and ignore the two first bytes
    Encode64 = Mid$(lpBuffer, 1 + EOL_SIZE, cbSize - EOL_SIZE)

End Function

' ---------------------------------------------------------------
Public Function hash_string(ByRef s As String) As String
    '--> fonction de hashage
    Dim h As Long
    Dim i As Long
    
    h = 0
    For i = 1 To Len(s)
        h = ((h And &HFF000000) \ &H1000000) + _
            (((h And &HFFFFFF) * 8) + Asc(Mid$(s, i, 1)))
    Next
    
    hash_string = Right$("00000000" & Hex$(h), 8)

End Function

Public Function UUEncodeFile(strFilePath As String) As String
'--> cette fonction encode en bin64 c'est pas de moi
    Dim intFile         As Integer      'file handler
    Dim intTempFile     As Integer      'temp file
    Dim lFileSize       As Long         'size of the file
    Dim strFileName     As String       'name of the file
    Dim strFileData     As String       'file data chunk
    Dim lEncodedLines   As Long         'number of encoded lines
    Dim strTempLine     As String       'temporary string
    Dim i               As Long         'loop counter
    Dim j               As Integer      'loop counter
    
    Dim strResult       As String
    '
    'Get file name
    strFileName = Mid$(strFilePath, InStrRev(strFilePath, "\") + 1)
    '
    'Insert first marker: "begin 664 ..."
    strResult = "begin 664 " + strFileName + vbLf
    '
    'Get file size
    lFileSize = FileLen(strFilePath)
    lEncodedLines = lFileSize \ 45 + 1
    '
    'Prepare buffer to retrieve data from
    'the file by 45 symbols chunks
    strFileData = Space(45)
    '
    intFile = FreeFile
    '
    Open strFilePath For Binary As intFile
        For i = 1 To lEncodedLines
            'Read file data by 45-bytes cnunks
            '
            If i = lEncodedLines Then
                'Last line of encoded data often is not
                'equal to 45, therefore we need to change
                'size of the buffer
                strFileData = Space(lFileSize Mod 45)
            End If
            'Retrieve data chunk from file to the buffer
            Get intFile, , strFileData
            'Add first symbol to encoded string that informs
            'about quantity of symbols in encoded string.
            'More often "M" symbol is used.
            strTempLine = Chr(Len(strFileData) + 32)
            '
            If i = lEncodedLines And (Len(strFileData) Mod 3) Then
                'If the last line is processed and length of
                'source data is not a number divisible by 3, add one or two
                'blankspace symbols
                strFileData = strFileData + Space(3 - (Len(strFileData) Mod 3))
            End If
            
            For j = 1 To Len(strFileData) Step 3
                'Breake each 3 (8-bits) bytes to 4 (6-bits) bytes
                '
                '1 byte
                strTempLine = strTempLine + Chr(Asc(Mid(strFileData, j, 1)) \ 4 + 32)
                '2 byte
                strTempLine = strTempLine + Chr((Asc(Mid(strFileData, j, 1)) Mod 4) * 16 _
                               + Asc(Mid(strFileData, j + 1, 1)) \ 16 + 32)
                '3 byte
                strTempLine = strTempLine + Chr((Asc(Mid(strFileData, j + 1, 1)) Mod 16) * 4 _
                               + Asc(Mid(strFileData, j + 2, 1)) \ 64 + 32)
                '4 byte
                strTempLine = strTempLine + Chr(Asc(Mid(strFileData, j + 2, 1)) Mod 64 + 32)
            Next j
            'replace " " with "`"
            strTempLine = Replace(strTempLine, " ", "`")
            'add encoded line to result buffer
            strResult = strResult + strTempLine + vbLf
            'reset line buffer
            strTempLine = ""
        Next i
    Close intFile

    'on ajoute le marqueur de fin de fichier
    strResult = strResult & "`" & vbLf + "end" + vbLf
    'on attribue la valeur � la variable
    UUEncodeFile = strResult
    
End Function

Public Function OpenFileAPI(ByRef FileName As String) As String
    Dim hOrgFile As Long
    Dim nSize As Long
    Dim Ret As Long
    
    On Error GoTo ErrorHandler
    
    'R�cup�re un Handle pour manipuler le flux m�moire du fichier
    hOrgFile = CreateFile(FileName, _
                            GENERIC_READ, _
                            FILE_SHARE_READ Or FILE_SHARE_WRITE, _
                            ByVal 0&, OPEN_EXISTING, 0, 0)

    'Taille du fichier
    nSize = GetFileSize(hOrgFile, 0)

    DoEvents

    'Initialise le pointeur sur le fichier
    SetFilePointer hOrgFile, 0, 0, FILE_BEGIN

    OpenFileAPI = Space(nSize)

    'Charge le contenu du fichier dans la variable OpenFileAPI
    ReadFile hOrgFile, ByVal OpenFileAPI, nSize, Ret, ByVal 0&
     
    DoEvents
     
    'Ferme le fichier
    CloseHandle hOrgFile
    
    Exit Function
ErrorHandler:
    OpenFileAPI = ""
    CloseHandle hOrgFile
End Function


