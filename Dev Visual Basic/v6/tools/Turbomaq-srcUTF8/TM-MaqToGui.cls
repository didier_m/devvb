VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MaqToGui"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Cette classe est utilis�e pour cr�er un squelette de maquette caract�re
' afin de plus avoir � g�n�rer ni � lier une maquette caract�re

'-> nom du rang caract�re pour squelette
Public Name As String

'-> Nombre de lignes associ�e � ce rang
Public NbLig As Integer

'-> Indique si le rang est un rang de saut de page
Public IsSaut As Boolean

'-> Liste des objets utilis�s dans ce rang
Public ListeObj As String

'-> Compl�ment sur la ligne du RG
Public Complement As String
