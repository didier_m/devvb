VERSION 5.00
Begin VB.Form frmDisplayOrder 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   2910
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   3930
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2910
   ScaleWidth      =   3930
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Down 
      Height          =   495
      Left            =   3360
      Picture         =   "TM-frmDisplayOrder.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   720
      Width           =   495
   End
   Begin VB.CommandButton Up 
      Height          =   495
      Left            =   3360
      Picture         =   "TM-frmDisplayOrder.frx":0442
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   120
      Width           =   495
   End
   Begin VB.ListBox List1 
      Height          =   2205
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3135
   End
   Begin VB.Image Annuler 
      Height          =   480
      Left            =   1200
      Top             =   2400
      Width           =   630
   End
   Begin VB.Image Aide 
      Height          =   480
      Left            =   1920
      Top             =   2400
      Width           =   630
   End
   Begin VB.Image Ok 
      Height          =   480
      Left            =   2640
      Top             =   2400
      Width           =   630
   End
End
Attribute VB_Name = "frmDisplayOrder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public ObjectSource
Public aTb As Tableau

 
Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub Down_Click()
'***********************************************************
'* Cette proc�dure augmente l'ordre d'affichage d'un objet *
'***********************************************************

Dim IdClick As Integer
Dim IdData As Integer
Dim ItemToAdd As String
Dim ToLoad As Integer
Dim Loaded As Integer

IdClick = Me.List1.ListIndex


'---> Si on est d�ja le dernier de la liste ne rien faire

If IdClick = Me.List1.ListCount - 1 Then Exit Sub

'---> R�cup�rer l'item dans la liste

ItemToAdd = Me.List1.Text
IdData = Me.List1.ItemData(Me.List1.ListIndex)

'---> Ajouter l'objet dans la liste

Me.List1.AddItem ItemToAdd, IdClick + 2
Me.List1.ItemData(IdClick + 2) = IdData

'---> Supprimer l'objet � l'ancienne position

Me.List1.RemoveItem IdClick

'---> S�lectionner l'objet

Me.List1.Selected(IdClick + 1) = True

End Sub

Private Sub Form_Activate()

    If Me.List1.ListCount = 0 Then
        Me.Ok.Enabled = False
    Else
        Me.List1.Selected(0) = True
    End If

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then Unload Me
End Sub

Private Sub Form_Load()

Dim aLb As Libelle

'---> Gestion des libell�s

Set aLb = Libelles("FRMDISPLAYORDER")

Me.Caption = aLb.GetCaption(1)
Me.Up.ToolTipText = aLb.GetCaption(2)
Me.Down.ToolTipText = aLb.GetCaption(3)

Set aLb = Libelles("BOUTONS")

Me.Annuler.ToolTipText = aLb.GetCaption(1)
Me.Aide.ToolTipText = aLb.GetCaption(2)
Me.Ok.ToolTipText = aLb.GetCaption(3)

Me.Annuler.Picture = Turbo_Maq.Annuler.Picture
Me.Aide.Picture = Turbo_Maq.Aide.Picture
Me.Ok.Picture = Turbo_Maq.Ok.Picture


Set aLb = Nothing

End Sub

Private Sub Picture1_Click()
    Unload Me
End Sub

Private Sub Picture2_Click()


Unload Me

End Sub

Private Sub Ok_Click()

Dim i As Integer
Dim aDefObj As String
Dim aSection As Section
Dim aTb As Tableau
Dim aBlock As Block
Dim aCadre As Cadre
Dim aBmp As ImageObj

    '---> Supprimer dans un premier temps la matrice en cours
    ObjectSource.ClearOrdreAffichage
    
    For i = 0 To Me.List1.ListCount - 1
        aDefObj = Me.List1.List(i)
        Select Case Trim(UCase$(Entry(1, aDefObj, ":")))
        
            Case "SECTION"
                        
                Set aSection = ObjectSource.Sections(UCase$(Trim(Entry(2, aDefObj, ":"))))
                aSection.IdAffichage = ObjectSource.AddOrdreAffichage("SCT-" & Trim(Entry(2, aDefObj, ":")))
        
            Case "TABLEAU"
            
                Set aTb = ObjectSource.Tableaux(UCase$(Trim(Entry(2, aDefObj, ":"))))
                aTb.IdAffichage = ObjectSource.AddOrdreAffichage("TB-" & Trim(Entry(2, aDefObj, ":")))
        
            Case "CADRE"
            
                Set aCadre = ObjectSource.Cadres(UCase$(Trim(Entry(2, aDefObj, ":"))))
                aCadre.IdOrdreAffichage = ObjectSource.AddOrdreAffichage("CDR-" & Trim(Entry(2, aDefObj, ":")))
            
            Case "BMP"
            
                Set aBmp = ObjectSource.Bmps(UCase$(Trim(Entry(2, aDefObj, ":"))))
                aBmp.IdOrdreAffichage = ObjectSource.AddOrdreAffichage("BMP-" & Trim(Entry(2, aDefObj, ":")))
            
            Case "BLOCK"
                
                Set aBlock = ObjectSource.Blocks("BL-" & UCase$(Trim(Entry(2, aDefObj, ":"))))
                aBlock.IdOrdreAffichage = ObjectSource.AddOrdreAffichage("BL-" & Trim(UCase$(Entry(2, aDefObj, ":"))))
            
        End Select
    Next

'-> D�charger la feuille
Unload Me

'-> Faire la mise � jour de la fen�tre des propri�t�s
If frmPropVisible Then frmProp.DisplayProperties


Set aSection = Nothing
Set aBlock = Nothing
Set aTb = Nothing
Set aCadre = Nothing
Set aBmp = Nothing

End Sub

Private Sub Up_Click()

'***********************************************************
'* Cette proc�dure diminue  l'ordre d'affichage d'un objet *
'***********************************************************

Dim IdClick As Integer
Dim ItemToAdd As String
Dim IdData As Integer


IdClick = Me.List1.ListIndex
IdData = Me.List1.ItemData(Me.List1.ListIndex)

'---> Si on est d�ja le premier de la liste ne rien faire

If IdClick = 0 Then Exit Sub

'---> R�cup�rer l'item dans la liste

ItemToAdd = Me.List1.Text

'---> Ajouter l'objet dans la liste

Me.List1.AddItem ItemToAdd, IdClick - 1
Me.List1.ItemData(IdClick - 1) = IdData

'---> Supprimer l'objet � l'ancienne position

Me.List1.RemoveItem IdClick + 1

'---> S�lectionner l'objet

Me.List1.Selected(IdClick - 1) = True


End Sub
