VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmPubli 
   Caption         =   "Form1"
   ClientHeight    =   6165
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11175
   LinkTopic       =   "Form1"
   ScaleHeight     =   411
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   745
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   9480
      ScaleHeight     =   615
      ScaleWidth      =   735
      TabIndex        =   3
      Top             =   5520
      Width           =   735
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   10320
      ScaleHeight     =   615
      ScaleWidth      =   735
      TabIndex        =   2
      Top             =   5520
      Width           =   735
   End
   Begin MSComctlLib.TreeView TreeView2 
      Height          =   3015
      Left            =   5640
      TabIndex        =   1
      Top             =   120
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   5318
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "ImageList2"
      Appearance      =   1
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   4215
      Left            =   720
      TabIndex        =   0
      Top             =   960
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   7435
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "ImageList2"
      Appearance      =   1
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   7200
      Top             =   3960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPubli.frx":0000
            Key             =   "Section"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPubli.frx":0452
            Key             =   "Tableau"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPubli.frx":112C
            Key             =   "Block"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPubli.frx":1E06
            Key             =   "Champ"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuPopup 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu mnuAdd 
         Caption         =   ""
      End
      Begin VB.Menu mnuRemove 
         Caption         =   ""
      End
      Begin VB.Menu mnuUp 
         Caption         =   ""
      End
      Begin VB.Menu mnuDown 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmPubli"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()

Dim aSection As Section
Dim aCadre As Cadre
Dim aMaq As Maquette
Dim aField As Field
Dim aNode As Node
Dim ListeFields As String
Dim aFrmSection As frmSection
Dim i As Integer, j As Integer
Dim GlobalListeField As String
Dim strTri As String
Dim strNonTri As String
Dim aNode2 As Node
Dim aNode3 As Node
Dim aTb As Tableau
Dim aBlock As Block
Dim aCell As Cellule
Dim strDef As String

Dim aLb As Libelle

On Error Resume Next

'-> Pointer sur la maquette
Set aMaq = Maquettes("MAQUETTE")

'-> Charger toutes les sections de texte
For Each aSection In aMaq.Sections
    '-> Vider les variables
    strTri = ""
    strNonTri = ""
    ListeFields = ""
    GlobalListeField = ""
    '-> Ajouter l'icone dans le treeview
    Set aNode = Me.TreeView1.Nodes.Add(, , UCase$(aSection.Nom), aSection.Nom, "Section")
    '-> Ajouter la liste des champs de la section
    Set aFrmSection = aMaq.frmSections(UCase$(aSection.Nom))
    ListeFields = MapFields(aFrmSection.RangText.Text)
    '-> Concatainer
    If ListeFields <> "" Then
        If GlobalListeField = "" Then
            GlobalListeField = ListeFields
        Else
            GlobalListeField = GlobalListeField & "@" & ListeFields
        End If
    End If
    '-> Rechercher la liste de tous les champs dans les cadres
    For Each aCadre In aSection.Cadres
        '-> R�cup�rer la liste des champs
        ListeFields = MapFields(aFrmSection.CadreTexte(aCadre.IdAffichage).Text)
        '-> Concatainer
        If ListeFields <> "" Then
            If GlobalListeField = "" Then
                GlobalListeField = ListeFields
            Else
                GlobalListeField = GlobalListeField & "@" & ListeFields
            End If
        End If
    Next 'Pour tous les cadres
    '-> Ajouter dans le treeview
    If GlobalListeField <> "" Then
        '-> Trier dans un premier temps la liste des champs
        For i = 1 To NumEntries(GlobalListeField, "@")
            If strNonTri = "" Then
                strNonTri = Entry(i, GlobalListeField, "@") & "�" & Entry(i, GlobalListeField, "@")
            Else
                strNonTri = strNonTri & "|" & Entry(i, GlobalListeField, "@") & "�" & Entry(i, GlobalListeField, "@")
            End If
        Next
        strTri = Tri(strNonTri, "|", "�")
        For i = 1 To NumEntries(strTri, "|")
            Set aNode2 = Me.TreeView1.Nodes.Add(aNode.Key, 4, aNode.Key & "|FIELD|" & Entry(i, strTri, "|"), Entry(i, strTri, "|"), "Champ")
            aNode2.Tag = "TXT|FIELD"
        Next
    End If
Next 'Pour toutes les sections


'-> Analyse de tous les tableaux
For Each aTb In aMaq.Tableaux
    '-> Ajouter l'icone dans le treeview
    Set aNode = Me.TreeView1.Nodes.Add(, , UCase$(aTb.Nom), aTb.Nom, "Tableau")
    '-> Analyse de tous les blocks
    For Each aBlock In aTb.Blocks
        '-> Vider les variables
        strTri = ""
        strNonTri = ""
        ListeFields = ""
        GlobalListeField = ""
        '-> Ajouter l'icone du block
        Set aNode2 = Me.TreeView1.Nodes.Add(aNode.Key, 4, aNode.Key & "|" & UCase$(aBlock.Nom), aBlock.Nom, "Block")
        For i = 1 To aBlock.NbLigne
            For j = 1 To aBlock.NbCol
                '-> Pointer sur la cellule
                Set aCell = aBlock.Cellules("L" & i & "C" & j)
                '-> Liste des champs
                ListeFields = MapFields(aCell.Contenu)
                '-> Ajouter dans la liste des champs
                If GlobalListeField = "" Then
                    GlobalListeField = ListeFields
                Else
                    GlobalListeField = GlobalListeField & "@" & ListeFields
                End If
            Next 'Pour toutes les colonnes
        Next 'Pour toutes les lignes
        
        '-> Trier la liste des champs
        If GlobalListeField <> "" Then
            '-> Recr�er la liste des champs
            For i = 1 To NumEntries(GlobalListeField, "@")
                If strNonTri = "" Then
                    strNonTri = Entry(i, GlobalListeField, "@") & "�" & Entry(i, GlobalListeField, "@")
                Else
                    strNonTri = strNonTri & "|" & Entry(i, GlobalListeField, "@") & "�" & Entry(i, GlobalListeField, "@")
                End If
            Next
            '-> Faire le tri
            strTri = Tri(strNonTri, "|", "�")
            '-> Afficher la liste des champs
            For i = 1 To NumEntries(strTri, "|")
                Set aNode3 = Me.TreeView1.Nodes.Add(aNode2.Key, 4, aNode2.Key & "|FIELD|" & Entry(i, strTri, "|"), Entry(i, strTri, "|"), "Champ")
                aNode3.Tag = "TAB|FIELD"
            Next
        End If
    Next 'Pour tous les bloks dans un tableau
Next 'Pour tous les tableaux

'-> Afficher les ruptures existantes
If aMaq.PubliPostage <> "" Then
    For i = 1 To NumEntries(aMaq.PubliPostage, "�")
        '-> Get de la d�finition
        strDef = Entry(i, aMaq.PubliPostage, "�")
        If Entry(1, strDef, "@") = "ST" Then
            Set aNode = Me.TreeView2.Nodes.Add(, , Entry(2, strDef, "@"), Entry(3, strDef, "@"), "Section")
            aNode.Tag = "TXT|FIELD"
        Else
            Set aNode = Me.TreeView2.Nodes.Add(, , Entry(2, strDef, "@"), Entry(3, strDef, "@"), "Tableau")
            aNode.Tag = "TAB|FIELD"
        End If
    Next
    '-> S�lectionner le premier �l�ment
    Me.TreeView2.Nodes(1).Selected = True
End If

'-> Gestion des Messprogs
Set aLb = Libelles("FRMPUBLI")
Me.Caption = aLb.GetCaption(1)
Me.mnuAdd.Caption = aLb.GetCaption(2)
Me.mnuRemove.Caption = aLb.GetCaption(3)
Me.mnuUp.Caption = aLb.GetCaption(4)
Me.mnuDown.Caption = aLb.GetCaption(5)

'-> gestion des images
Me.Picture1.Picture = Turbo_Maq.Ok.Picture
Me.Picture2.Picture = Turbo_Maq.Annuler.Picture

End Sub

Private Sub Form_Resize()

On Error Resume Next

Dim Res As Long
Dim aRect As Rect

'-> Treeview1
Res = GetClientRect(Me.hwnd, aRect)
Me.TreeView1.Left = 0
Me.TreeView1.Top = 0
Me.TreeView1.Height = aRect.Bottom
Me.TreeView1.Width = (aRect.Right) / 2 - 1

Me.TreeView2.Left = aRect.Right / 2 + 1
Me.TreeView2.Top = 0
Me.TreeView2.Height = aRect.Bottom
Me.TreeView2.Width = Me.TreeView1.Width

Me.Picture1.Left = aRect.Right - Me.Picture1.Width - 3
Me.Picture2.Left = Me.Picture1.Left - Me.Picture2.Width - 3
Me.Picture1.Top = aRect.Bottom - Me.Picture1.Height - 3
Me.Picture2.Top = Me.Picture1.Top
End Sub




Private Sub mnuAdd_Click()

Dim Lib As String
Dim aNode As Node


'-> Selon le type d'objet
If Entry(1, Me.TreeView1.SelectedItem.Tag, "|") = "TAB" Then
    '-> Get du libelle tableau et block
    Lib = Me.TreeView1.SelectedItem.Parent.Parent.Text & " - " & Me.TreeView1.SelectedItem.Parent.Text & " - " & Me.TreeView1.SelectedItem.Text
    Set aNode = Me.TreeView2.Nodes.Add(, , Me.TreeView1.SelectedItem.Key, Lib, "Tableau")
Else
    '-> Get du libel de la section
    Lib = Me.TreeView1.SelectedItem.Parent.Text & " - " & Me.TreeView1.SelectedItem.Text
    Set aNode = Me.TreeView2.Nodes.Add(, , Me.TreeView1.SelectedItem.Key, Lib, "Section")
End If

aNode.Selected = True
aNode.Tag = Me.TreeView1.SelectedItem.Tag

End Sub

Private Sub mnuDown_Click()


Dim aKey As String
Dim aText As String
Dim aImage As String
Dim aNode As Node
Dim aNode2 As Node


'-> R�cup�rer les donn�es
aKey = Me.TreeView2.SelectedItem.Key
aText = Me.TreeView2.SelectedItem.Text
aImage = Me.TreeView2.SelectedItem.Image

'-> Pointer sur le node pr�cedent
Set aNode = Me.TreeView2.SelectedItem.Next

'-> Supprimer le node en cours
Me.TreeView2.Nodes.Remove (Me.TreeView2.SelectedItem.Key)

'-> Cr�er le nouveau node
Set aNode2 = Me.TreeView2.Nodes.Add(aNode.Key, 2, aKey, aText, aImage)
aNode2.Selected = True

End Sub

Private Sub mnuRemove_Click()

'-> Supprimer le node de la rupture
Me.TreeView2.Nodes.Remove (Me.TreeView2.SelectedItem.Key)


End Sub

Private Sub mnuUp_Click()

Dim aKey As String
Dim aText As String
Dim aImage As String
Dim aNode As Node
Dim aNode2 As Node


'-> R�cup�rer les donn�es
aKey = Me.TreeView2.SelectedItem.Key
aText = Me.TreeView2.SelectedItem.Text
aImage = Me.TreeView2.SelectedItem.Image

'-> Pointer sur le node pr�cedent
Set aNode = Me.TreeView2.SelectedItem.Previous

'-> Supprimer le node en cours
Me.TreeView2.Nodes.Remove (Me.TreeView2.SelectedItem.Key)

'-> Cr�er le nouveau node
Set aNode2 = Me.TreeView2.Nodes.Add(aNode.Key, 3, aKey, aText, aImage)
aNode.Selected = True



End Sub




Private Sub Picture1_DblClick()

'---> Enregistrer les ruptures

Dim aMaq As Maquette
Dim aLb As Libelle
Dim aNode As Node
Dim StrPubli As String
Dim TypeObj As String

'-> Pointer sur la maquette
Set aMaq = Maquettes("MAQUETTE")

'-> Pointer sur la classe libelle
Set aLb = Libelles("FRMPUBLI")


'-> Enregistrer � blanc
If Me.TreeView2.Nodes.Count = 0 Then
    aMaq.PubliPostage = ""
Else
    '-> Enregister dans l'ordre
    Set aNode = Me.TreeView2.SelectedItem.Root
    Do
        '-> Cr�er la chaine d'iimpression
        If Entry(1, aNode.Tag, "|") = "TAB" Then
            TypeObj = "BLK"
        Else
            TypeObj = "ST"
        End If
        If StrPubli = "" Then
            StrPubli = TypeObj & "@" & aNode.Key & "@" & aNode.Text
        Else
            StrPubli = StrPubli & "�" & TypeObj & "@" & aNode.Key & "@" & aNode.Text
        End If
        '-> Pointer sur le node suivant
        If aNode.Next Is Nothing Then
            Exit Do
        Else
            Set aNode = aNode.Next
        End If
        '-> Quitter si plus de nodes
    Loop
End If
    
'-> Mettre � jour la propri�t� de la maquette
aMaq.PubliPostage = StrPubli

'-> Indiquer � l'utilisateur que le param�trage est mis � jour
MsgBox aLb.GetCaption(6), vbOKOnly + vbInformation, aLb.GetToolTip(6)

End Sub

Private Sub Picture2_DblClick()

Unload Me

End Sub

Private Sub TreeView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim aNode As Node

If Button <> vbRightButton Then Exit Sub
If Me.TreeView1.Nodes.Count = 0 Then Exit Sub

'-> S�lectionner le node du click
Set aNode = Me.TreeView1.HitTest(x, y)
If aNode Is Nothing Then Exit Sub
Set Me.TreeView1.SelectedItem = aNode

'-> Quitter si pas bon node
If aNode.Tag = "" Then Exit Sub

'-> Masquer les menu non autoris�s
Me.mnuAdd.Visible = True
Me.mnuUp.Visible = False
Me.mnuDown.Visible = False
Me.mnuRemove.Visible = False
If Me.TreeView2.Nodes.Count = 5 Then
    Me.mnuAdd.Enabled = False
Else
    If Not IsNode Then
        Me.mnuAdd.Enabled = True
    Else
        Me.mnuAdd.Enabled = False
    End If
End If

'-> Afficher le menu
Me.PopupMenu Me.mnuPopup

End Sub





Private Sub TreeView2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim aNode As Node

If Button <> vbRightButton Then Exit Sub
If Me.TreeView2.Nodes.Count = 0 Then Exit Sub

'-> S�lectionner le node du click
Set aNode = Me.TreeView2.HitTest(x, y)
If aNode Is Nothing Then Exit Sub
Set Me.TreeView2.SelectedItem = aNode

'-> Gestion des menus
Me.mnuRemove.Visible = True
Me.mnuUp.Visible = True
Me.mnuDown.Visible = True
Me.mnuAdd.Visible = False

'-> Tester pour lenu monter ou descendre
If Me.TreeView2.SelectedItem.Previous Is Nothing Then
    Me.mnuUp.Enabled = False
Else
    Me.mnuUp.Enabled = True
End If

If Me.TreeView2.SelectedItem.Next Is Nothing Then
    Me.mnuDown.Enabled = False
Else
    Me.mnuDown.Enabled = True
End If

'-> Afficher le menu
Me.PopupMenu Me.mnuPopup


End Sub

Private Function IsNode() As Boolean

'---> Cette fonction v�rifie qu'un node existe d�ja en tant que rupture

Dim aNode As Node

On Error GoTo GestError

Set aNode = Me.TreeView2.Nodes(Me.TreeView1.SelectedItem.Key)
IsNode = True

Exit Function

GestError:
    IsNode = False

End Function
