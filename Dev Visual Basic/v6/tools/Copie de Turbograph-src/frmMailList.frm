VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form frmMailList 
   AutoRedraw      =   -1  'True
   Caption         =   "Envoyer par mail"
   ClientHeight    =   6105
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10920
   Icon            =   "frmMailList.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   6105
   ScaleWidth      =   10920
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   9720
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMailList.frx":08CA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMailList.frx":0E64
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5895
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   8415
      _ExtentX        =   14843
      _ExtentY        =   10398
      _Version        =   393216
      Tabs            =   4
      Tab             =   1
      TabsPerRow      =   4
      TabHeight       =   882
      WordWrap        =   0   'False
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "frmMailList.frx":13FE
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame3"
      Tab(0).Control(1)=   "ImageList2"
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "frmMailList.frx":141A
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Frame2"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Tab 2"
      TabPicture(2)   =   "frmMailList.frx":1436
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Winsock1"
      Tab(2).Control(1)=   "Frame1"
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "Tab 3"
      TabPicture(3)   =   "frmMailList.frx":1452
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Frame5"
      Tab(3).ControlCount=   1
      Begin VB.Frame Frame5 
         Height          =   5055
         Left            =   -74760
         TabIndex        =   32
         Top             =   600
         Width           =   5535
         Begin VB.PictureBox Picture2 
            AutoRedraw      =   -1  'True
            Height          =   255
            Left            =   2160
            ScaleHeight     =   195
            ScaleWidth      =   3075
            TabIndex        =   37
            Top             =   4200
            Width           =   3135
         End
         Begin VB.CommandButton Command4 
            Caption         =   "Imprimer"
            Height          =   375
            Left            =   4200
            TabIndex        =   36
            Top             =   4560
            Width           =   1095
         End
         Begin VB.CheckBox Check4 
            Caption         =   "Imprimer les destinataires non s�lectionn�s"
            Height          =   255
            Left            =   240
            TabIndex        =   35
            Top             =   840
            Value           =   1  'Checked
            Width           =   4575
         End
         Begin VB.CheckBox Check3 
            Caption         =   "Imprimer les destinataires s�lectionn�s"
            Height          =   255
            Left            =   240
            TabIndex        =   33
            Top             =   360
            Width           =   4575
         End
         Begin VB.Label Label10 
            BackColor       =   &H00C0C0C0&
            BackStyle       =   0  'Transparent
            Height          =   255
            Left            =   240
            TabIndex        =   34
            Top             =   4680
            Width           =   3855
         End
      End
      Begin MSWinsockLib.Winsock Winsock1 
         Left            =   -69600
         Top             =   360
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   393216
      End
      Begin VB.Frame Frame1 
         Height          =   5055
         Left            =   -74760
         TabIndex        =   23
         Top             =   600
         Width           =   5535
         Begin VB.CheckBox Check1 
            Caption         =   "Utiliser le serveur de messagerie"
            Height          =   255
            Left            =   240
            TabIndex        =   30
            Top             =   4680
            Width           =   5295
         End
         Begin VB.CommandButton Command2 
            Height          =   375
            Left            =   4200
            TabIndex        =   26
            Top             =   4560
            Width           =   1095
         End
         Begin VB.TextBox Text2 
            Height          =   285
            Left            =   840
            TabIndex        =   24
            Top             =   3720
            Width           =   4455
         End
         Begin MSComctlLib.TreeView TreeView1 
            Height          =   3255
            Left            =   240
            TabIndex        =   27
            Top             =   360
            Width           =   5055
            _ExtentX        =   8916
            _ExtentY        =   5741
            _Version        =   393217
            LabelEdit       =   1
            Style           =   7
            Checkboxes      =   -1  'True
            ImageList       =   "ImageList1"
            Appearance      =   1
         End
         Begin VB.PictureBox Picture1 
            AutoRedraw      =   -1  'True
            Height          =   255
            Left            =   2160
            ScaleHeight     =   195
            ScaleWidth      =   3075
            TabIndex        =   25
            Top             =   4200
            Width           =   3135
         End
         Begin VB.Label Label8 
            BackColor       =   &H00C0C0C0&
            BackStyle       =   0  'Transparent
            Height          =   255
            Left            =   240
            TabIndex        =   29
            Top             =   4680
            Width           =   3855
         End
         Begin VB.Label Label6 
            Caption         =   "CC :"
            Height          =   255
            Left            =   240
            TabIndex        =   28
            Top             =   3770
            Width           =   495
         End
      End
      Begin VB.Frame Frame3 
         Height          =   5055
         Left            =   -74760
         TabIndex        =   19
         Top             =   600
         Width           =   5535
         Begin VB.TextBox Text3 
            Height          =   285
            Left            =   720
            TabIndex        =   0
            Top             =   360
            Width           =   4455
         End
         Begin VB.TextBox Text1 
            Height          =   285
            Left            =   720
            TabIndex        =   1
            Top             =   840
            Width           =   4455
         End
         Begin VB.CommandButton Command3 
            Caption         =   ">>"
            Height          =   375
            Left            =   4200
            TabIndex        =   20
            Top             =   4560
            Width           =   1095
         End
         Begin RichTextLib.RichTextBox RichTextBox1 
            Height          =   3015
            Left            =   720
            TabIndex        =   2
            Top             =   1320
            Width           =   4470
            _ExtentX        =   7885
            _ExtentY        =   5318
            _Version        =   393217
            Enabled         =   -1  'True
            TextRTF         =   $"frmMailList.frx":146E
         End
         Begin VB.Label Label9 
            Caption         =   "De :"
            Height          =   255
            Left            =   120
            TabIndex        =   31
            Top             =   360
            Width           =   495
         End
         Begin VB.Label Label7 
            Caption         =   "Obj :"
            Height          =   255
            Left            =   120
            TabIndex        =   22
            Top             =   840
            Width           =   495
         End
         Begin VB.Label Label5 
            Caption         =   "Corps :"
            Height          =   255
            Left            =   120
            TabIndex        =   21
            Top             =   1320
            Width           =   495
         End
      End
      Begin MSComctlLib.ImageList ImageList2 
         Left            =   -75000
         Top             =   120
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   5
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMailList.frx":14F0
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMailList.frx":1DCA
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMailList.frx":26A4
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMailList.frx":2F7E
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMailList.frx":3858
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin VB.Frame Frame2 
         Height          =   5055
         Left            =   240
         TabIndex        =   4
         Top             =   600
         Width           =   5535
         Begin VB.Frame Frame6 
            Height          =   615
            Left            =   360
            TabIndex        =   38
            Top             =   4320
            Width           =   4455
            Begin VB.TextBox Text4 
               Enabled         =   0   'False
               Height          =   285
               Left            =   2160
               TabIndex        =   39
               Top             =   220
               Width           =   2175
            End
            Begin VB.Label Label11 
               Caption         =   "Nom du fichier pdf"
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   40
               Top             =   220
               Width           =   1575
            End
         End
         Begin VB.CommandButton Command1 
            Caption         =   ">>"
            Height          =   375
            Left            =   4200
            TabIndex        =   18
            Top             =   4560
            Width           =   1095
         End
         Begin VB.PictureBox picTurbo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   360
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   13
            Top             =   960
            Width           =   735
            Begin VB.Image Image3 
               Height          =   480
               Left            =   120
               Picture         =   "frmMailList.frx":66EA
               Top             =   50
               Width           =   480
            End
         End
         Begin VB.PictureBox PicHtml 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   360
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   12
            Top             =   1680
            Width           =   735
            Begin VB.Image Image12 
               Height          =   480
               Left            =   120
               Picture         =   "frmMailList.frx":6FB4
               Top             =   80
               Width           =   480
            End
         End
         Begin VB.Frame Frame4 
            Enabled         =   0   'False
            Height          =   1815
            Left            =   360
            TabIndex        =   7
            Top             =   2520
            Width           =   4455
            Begin VB.OptionButton Option6 
               CausesValidation=   0   'False
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   11
               Top             =   1080
               Width           =   2415
            End
            Begin VB.OptionButton Option5 
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   10
               Top             =   720
               Value           =   -1  'True
               Width           =   3975
            End
            Begin VB.OptionButton Option4 
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   9
               Top             =   360
               Width           =   3735
            End
            Begin VB.CheckBox Check2 
               Height          =   255
               Left            =   240
               TabIndex        =   8
               Top             =   1440
               Value           =   1  'Checked
               Width           =   4095
            End
         End
         Begin VB.PictureBox PicPdf 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   2760
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   6
            Top             =   1680
            Width           =   735
            Begin VB.Image Image16 
               Height          =   480
               Left            =   105
               Picture         =   "frmMailList.frx":787E
               Top             =   65
               Width           =   480
            End
         End
         Begin VB.PictureBox PicNothing 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   2760
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   5
            Top             =   960
            Width           =   735
            Begin VB.Image Image17 
               Height          =   720
               Left            =   0
               Picture         =   "frmMailList.frx":8148
               Top             =   -40
               Width           =   720
            End
         End
         Begin VB.Label Label1 
            Height          =   615
            Left            =   1320
            TabIndex        =   17
            Top             =   990
            Width           =   1335
         End
         Begin VB.Label Label2 
            Height          =   495
            Left            =   1320
            TabIndex        =   16
            Top             =   1755
            Width           =   1335
         End
         Begin VB.Shape Sel 
            BorderColor     =   &H000000FF&
            BorderWidth     =   2
            Height          =   660
            Left            =   345
            Top             =   945
            Width           =   780
         End
         Begin VB.Label Label3 
            Height          =   615
            Left            =   3720
            TabIndex        =   15
            Top             =   990
            Width           =   1215
         End
         Begin VB.Label Label4 
            Height          =   615
            Left            =   3720
            TabIndex        =   14
            Top             =   1755
            Width           =   1215
         End
      End
   End
End
Attribute VB_Name = "frmMailList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'-> Indique le fichier en cours de traitement
Public FichierName As String
Public aSpool As Spool

Private FormatHtml As Boolean
Private FormatPDF As Boolean
Private FormatNothing As Boolean
Private FormatTurbo As Boolean
Private pFormat As String

'-> Indique la cl� du spool actuel
Public SpoolKey As String
Public strSaisie As String

Dim TempFileName As String
Dim hdlFile As Integer
Dim ReponseFaite As Boolean
Dim iStep As Integer

Dim toto As String

Public Sub Init()

Dim aNode As Node
Dim aNode2 As Node
Dim aNode3 As Node
Dim aFichier As Fichier
Dim i As Integer
Dim aLb As Libelle
Dim aSpool2 As Spool
Dim strTitre As String
Dim strPage As String
Dim topOK As Boolean

'-> Pointer sur la gestion des messages
Set aLb = Libelles("MDIMAIN")

'-> Pointer sur l'objet Fichier
Set aFichier = Fichiers(aSpool.FileName)

'-> Cr�er une icone par spool
For Each aSpool2 In aFichier.Spools
    Set aNode2 = Me.TreeView1.Nodes.add(, , aSpool2.Key, aSpool2.SpoolText)
    aNode2.Tag = "SPOOL"
    aNode2.Expanded = True
    '-> Soit la liste des erreurs
    If aSpool.NbError = 0 Then
        '-> Afficher une icone par page
        For i = 1 To aSpool.NbPage
            '-> Recuperer eventuellement le titre de la feuille
            strTitre = GetTitreMail(aSpool2, i)
            strPage = GetTitrePage(aSpool2, i)
            If strPage = "" Then strPage = aLb.GetCaption(12) & i
            '-> si on est bien sur une page
            If strTitre <> "[NO]" Then
                topOK = True
                If InStr(1, strTitre, "@") <> 0 Then
                    Set aNode3 = Me.TreeView1.Nodes.add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, strPage & " ~~ " & strTitre, 1)
                Else
                    Set aNode3 = Me.TreeView1.Nodes.add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, strPage & " ~~ " & strTitre, 2)
                End If
                aNode3.Tag = "PAGE"
            End If
        Next
    Else
        '-> Afficher l'icone de la page d'erreur
        Set aNode3 = Me.TreeView1.Nodes.add(aNode2.Key, 4, aNode2.Key & "�ERROR", aLb.GetCaption(11), "Warning")
        aNode3.Tag = "ERROR"
    End If 'S'il y a des erreurs
Next 'Pour tous les spools

'-> si le spool n'a pas �t� configur� pour le mailing
If topOK = False Then
    Set aLb = Libelles("FRMMAIL")
    MsgBox aLb.GetCaption(45), vbInformation, "Mailing"
    Unload Me
End If

'-> on envoi directement
SendOutLook = True

End Sub

Private Sub Check3_Click()
'-> on inverse l'autre case
If Me.Check3.Value = 1 Then Me.Check4.Value = 0
If Me.Check3.Value = 0 Then Me.Check4.Value = 1
End Sub

Private Sub Check4_Click()
'-> on inverse l'autre case
If Me.Check4.Value = 1 Then Me.Check3.Value = 0
If Me.Check4.Value = 0 Then Me.Check3.Value = 1
End Sub

Private Sub Command1_Click()
'-> on donne le focus � l'onglet suivant
Me.SSTab1.Tab = 2
End Sub

Private Sub sendEmail()
'---> Envoyer un message vers Internet

Dim SpoolName As String
Dim aNode As Node
Dim PageMin As Integer
Dim PageMax As Integer

On Error Resume Next

'-> on initialise
ListeHtmlFile = ""
Set frmLib.MAPIMessages1 = Nothing
Err.Description = ""
'on met le sablier
Screen.MousePointer = 11

'-> vider le picture de temporisation
Me.Picture1.Cls
TailleTotale = 0
'-> Calcul de la taille total
'-> on parcourt le treeview pour les mails � envoyer
For Each aNode In Me.TreeView1.Nodes
    '-> on on compte
    If aNode.Checked And aNode.Key <> "SPOOL" Then TailleTotale = TailleTotale + 1
Next

TailleLue = 0

'-> on parcourt le treeview pour les mails � envoyer
For Each aNode In Me.TreeView1.Nodes
    '-> on verifie que l'on est bien sur un mail � envoyer
    If aNode.Checked And aNode.Tag <> "SPOOL" Then
        '-> Ne mettre � jour les variables de traitement que si on vient de la visu et pas du menu
        '-> Cl� du spool et du fichier
        Set aSpool = Fichiers(Entry(1, aNode.Key, "�"))
        PageMin = CInt(Entry(2, Entry(2, aNode.Key, "�"), "|"))
        PageMax = CInt(Entry(2, Entry(2, aNode.Next.Key, "�"), "|")) - 1
        If PageMax < PageMin Then PageMax = aSpool.NbPage
        '-> nom du spool � traiter
        SpoolName = aSpool.FileName
        SpoolName = CreateDetail(aSpool, -2, PageMin, PageMax)
        
        '-> Format de la piece jointe
        If FormatTurbo Then pFormat = "0"
        If FormatHtml = True Then pFormat = "1"
        If FormatNothing Then pFormat = "3"
        If FormatPDF = True Then pFormat = "4"
        
        '-> on envoi le mail
        If Check1.Value = 1 Then
            If Not sendBySMTP(SpoolName, aNode.Text) Then Exit For
        Else
            If Not SendByOutLook(SpoolName, aNode.Text) Then Exit For
        End If
        '-> Dessin de la temporisation
        TailleLue = TailleLue + 1
        DrawTempo Me.Picture1
        DoEvents
    End If
Next

'on met le sablier
Screen.MousePointer = 0
Me.SetFocus
If Err.Description <> "" Then MsgBox Err.Description, vbExclamation

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Command2_Click()
'-> envoyer les mails
sendEmail
End Sub

Private Sub Command3_Click()
'-> on donne le focus � l'onglet suivant
Me.SSTab1.Tab = 1
End Sub

Private Sub Command4_Click()

'-> on imprime les selections
Call printDestinataires

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

Select Case KeyCode

Case vbKeyEscape
    'Unload Me
End Select

End Sub

Private Sub Form_Load()

'On Error Resume Next

Dim aLb As Libelle
Dim PrinterFound As Boolean
Dim Printer As Printer

'On Error Resume Next

'-> Gestion des messages
Set aLb = Libelles("FRMMAIL")

Me.Caption = aLb.GetCaption(36)
Me.SSTab1.TabCaption(0) = " " & aLb.GetCaption(39)
Me.SSTab1.TabCaption(1) = " " & aLb.GetCaption(40)
Me.SSTab1.TabCaption(2) = " " & aLb.GetCaption(37)
Me.SSTab1.TabCaption(3) = " " & aLb.GetCaption(46)

Me.Frame1.Caption = aLb.GetCaption(42)
Me.Frame2.Caption = aLb.GetCaption(41)
Me.Frame3.Caption = aLb.GetCaption(43)
Me.Frame4.Caption = aLb.GetCaption(23)
Me.Label1.Caption = aLb.GetCaption(21)
Me.Label2.Caption = aLb.GetCaption(22)
Me.Label3.Caption = aLb.GetCaption(30)
Me.Label4.Caption = aLb.GetCaption(29)
Me.Label1.Caption = aLb.GetCaption(49)
Me.Option4.Caption = aLb.GetCaption(24)
Me.Option5.Caption = aLb.GetCaption(25)
Me.Option6.Caption = aLb.GetCaption(26)
Me.Check2.Caption = aLb.GetCaption(27)
Me.Command2.Caption = aLb.GetCaption(38)
Me.Check3.Caption = aLb.GetCaption(47)
Me.Check4.Caption = aLb.GetCaption(48)
Me.Command4.Caption = aLb.GetCaption(46)
Me.Check1.Caption = Me.Check1.Caption & "  " & GetIniString("PARAM", "SMTP", TurboGraphIniFile, False)
'-> si on a param�tr� un serveur de messagerie coch� par d�faut la case
If GetIniString("PARAM", "SMTP", TurboGraphIniFile, False) <> "" Then
    Me.Check1.Value = 1
End If

'-> Charger les images
Set Me.SSTab1.TabPicture(0) = Me.ImageList2.ListImages(2).Picture
Set Me.SSTab1.TabPicture(1) = Me.ImageList2.ListImages(4).Picture
Set Me.SSTab1.TabPicture(2) = Me.ImageList2.ListImages(3).Picture
Set Me.SSTab1.TabPicture(3) = Me.ImageList2.ListImages(5).Picture

'-> S�lectionner le premier onglet
Me.SSTab1.Tab = 0

'-> par defaut on a le format turbo
FormatTurbo = True
FormatPDF = False
FormatHtml = False
FormatNothing = False

'-> initialisation
Init

End Sub

Private Sub Form_Resize()
'--> on fait un resize sur la feuille
formResize
End Sub

Private Sub formResize()
'--> on retaille la feuille
On Error Resume Next

Me.SSTab1.Height = Me.Height - 630
Me.SSTab1.Width = Me.Width - 410
Me.Frame1.Width = Me.SSTab1.Width - 450
Me.Frame2.Width = Me.Frame1.Width
Me.Frame3.Width = Me.Frame1.Width
Me.Frame5.Width = Me.Frame1.Width
Me.Frame1.Height = Me.SSTab1.Height - 820
Me.Frame2.Height = Me.Frame1.Height
Me.Frame3.Height = Me.Frame1.Height
Me.Frame5.Height = Me.Frame1.Height
Me.Text1.Width = Me.Frame1.Width - 920
Me.Text3.Width = Me.Frame1.Width - 920
Me.TreeView1.Width = Me.Frame1.Width - 450
Me.RichTextBox1.Width = Me.Text1.Width
Me.RichTextBox1.Height = Me.Frame1.Height - 2000
Me.Command3.Top = Me.RichTextBox1.Height + Me.RichTextBox1.Top + 150
Me.Command3.Left = Me.RichTextBox1.Left + Me.RichTextBox1.Width - Me.Command3.Width
Me.Command1.Left = Me.Command3.Left
Me.Command1.Top = Me.Command3.Top
Me.Command2.Left = Me.Command3.Left
Me.Command2.Top = Me.Command3.Top
Me.Command4.Left = Me.Command3.Left
Me.Command4.Top = Me.Command3.Top
Me.Text2.Width = Me.TreeView1.Width - 580
Me.TreeView1.Height = Me.SSTab1.Height - 2700
Me.Text2.Top = Me.TreeView1.Top + Me.TreeView1.Height + 150
Me.Label6.Top = Me.Text2.Top
Me.Check1.Top = Me.Text2.Top + 950
Me.Picture1.Top = Me.Text2.Top + Me.Text1.Height + 200
Me.Picture1.Left = Me.TreeView1.Left
Me.Picture1.Width = Me.TreeView1.Width
Me.Picture2.Top = Me.Text2.Top + Me.Text1.Height + 200
Me.Picture2.Left = Me.TreeView1.Left
Me.Picture2.Width = Me.TreeView1.Width

End Sub

Private Sub Image12_Click()
    PicHtml_Click
End Sub

Private Sub Image16_Click()
    picPdf_Click
End Sub

Private Sub Image17_Click()
    picNothing_Click
End Sub

Private Sub Image3_Click()
    picTurbo_Click
End Sub

Private Sub PicHtml_Click()

Me.Sel.Left = Me.PicHtml.Left - 20
Me.Sel.Top = Me.PicHtml.Top - 20
FormatHtml = True
FormatPDF = False
FormatNothing = False
FormatTurbo = False

'-> Bloquer la gestion Internet
Me.Frame6.Enabled = False
Me.Label11.Enabled = False
Me.Text4.Enabled = False
Me.Text4.Text = ""
Me.Frame4.Enabled = True
Me.Option4.Enabled = True
Me.Option5.Enabled = True
Me.Option6.Enabled = True
Me.Check2.Enabled = True

End Sub

Private Sub picTurbo_Click()

Me.Sel.Left = Me.picTurbo.Left - 20
Me.Sel.Top = Me.picTurbo.Top - 20
FormatHtml = False
FormatPDF = False
FormatNothing = False
FormatTurbo = True

'-> Bloquer la gestion Internet
Me.Frame6.Enabled = False
Me.Label11.Enabled = False
Me.Text4.Enabled = False
Me.Text4.Text = ""
Me.Frame4.Enabled = False
Me.Option4.Enabled = False
Me.Option5.Enabled = False
Me.Option6.Enabled = False
Me.Check2.Enabled = True

End Sub

Private Sub picPdf_Click()

Me.Sel.Left = Me.PicPdf.Left - 20
Me.Sel.Top = Me.PicPdf.Top - 20
FormatPDF = True
FormatHtml = False
FormatNothing = False
FormatTurbo = False

'-> Bloquer la gestion Internet
Me.Frame6.Enabled = True
Me.Label11.Enabled = True
Me.Text4.Enabled = True
Me.Text4.SetFocus
Me.Frame4.Enabled = False
Me.Option4.Enabled = False
Me.Option5.Enabled = False
Me.Option6.Enabled = False
Me.Check2.Enabled = True

End Sub

Private Sub picNothing_Click()

Me.Sel.Left = Me.PicNothing.Left - 20
Me.Sel.Top = Me.PicNothing.Top - 20
FormatNothing = True
FormatPDF = False
FormatHtml = False
FormatTurbo = False

'-> Bloquer la gestion Internet
Me.Frame6.Enabled = False
Me.Label11.Enabled = False
Me.Text4.Enabled = False
Me.Text4.Text = ""
Me.Frame4.Enabled = False
Me.Option4.Enabled = False
Me.Option5.Enabled = False
Me.Option6.Enabled = False
Me.Check2.Enabled = False
End Sub

Private Sub Text1_KeyDown(KeyCode As Integer, Shift As Integer)

If KeyCode = 13 Then Me.RichTextBox1.SetFocus

End Sub

Private Sub Text3_KeyDown(KeyCode As Integer, Shift As Integer)

If KeyCode = 13 Then Me.Text1.SetFocus

End Sub

Private Sub TreeView1_AfterLabelEdit(Cancel As Integer, NewString As String)
'-> on verifie pour info la validit� de la saisie
NewString = Entry(1, strSaisie, " ~~ ") & " ~~ " & NewString
If InStr(1, NewString, "@") <> 0 Then
    Me.TreeView1.SelectedItem.Image = 1
Else
    Me.TreeView1.SelectedItem.Image = 2
End If

End Sub

Private Sub TreeView1_DblClick()
    If Me.TreeView1.SelectedItem.Tag <> "SPOOL" Then
        '-> on se met � gauche la saisie
        strSaisie = Me.TreeView1.SelectedItem.Text
        Me.TreeView1.SelectedItem.Text = Mid(Entry(2, Me.TreeView1.SelectedItem.Text, " ~~ "), 4)
        TreeView1.StartLabelEdit
        Me.TreeView1.SelectedItem.Text = strSaisie
    End If
End Sub

Private Sub TreeView1_KeyPress(KeyAscii As Integer)
Dim aNode As Node

'On Error Resume Next
'-> on selectionne ou deselectionne tous les elements d'un node
'If KeyAscii = 13 Then
'    If Me.TreeView1.SelectedItem.Tag <> "LIG" Then Exit Sub
'    For Each aNode In Me.TreeView1.Nodes
'        '-> on inverse la selection
'        If aNode.Tag = "LIG" Then
'            If aNode.Parent.Text = Me.TreeView1.SelectedItem.Parent.Text Then
'                TreeView1_NodeClick aNode
'            End If
'        End If
'    Next
'End If

End Sub

Private Sub TreeView1_NodeCheck(ByVal Node As MSComctlLib.Node)
Dim aNode As Node

On Error Resume Next
'-> on selectionne ou deselectionne tous les elements d'un node
Set Me.TreeView1.SelectedItem = Node
If Me.TreeView1.SelectedItem.Tag <> "SPOOL" Then Exit Sub
For Each aNode In Me.TreeView1.Nodes
    '-> on inverse la selection
    If aNode.Tag = "PAGE" Then
        If aNode.Parent.Text = Me.TreeView1.SelectedItem.Text Then
            If aNode.Image = 1 Then aNode.Checked = Node.Checked
        End If
    End If
Next

End Sub

Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)

'---> Bloquer ou d�bloquer la gestion des prori�t�s

Dim aBlock As Block


End Sub

Public Function SendByOutLook(ByVal FileToSend As String, sAdrMail As String) As Boolean

'---> Cette proc�dure ouvre OutLook, cr�er un nouveau message , et joint le fichier Sp�cifi�

Dim aOutLook As Object
Dim aMail As Object
Dim ErrorCode As Integer
Dim i As Integer
Dim CurrentPIDProcess As Long

On Error GoTo GestError

'-> V�rifier que le fichier existe
If Dir$(FileToSend, vbNormal) = "" Then
    ErrorCode = 3
    GoTo GestError
End If

'-> Obtenir un pointeur vers OutLook
ErrorCode = 1
Set aOutLook = CreateObject("Outlook.Application")

'-> Cr�er un nouveau message
ErrorCode = 2
Set aMail = aOutLook.createitem(0)

'-> Afficher le corps du message
'-> Envoie depuis la page de visu
If frmMailList.RichTextBox1.Text <> "" Then aMail.body = frmMailList.RichTextBox1.Text
'-> Setting des param�tres
aMail.To = sAdrMail
aMail.CC = Me.Text2.Text 'personnes en pi�ces jointes
aMail.Subject = Me.Text1.Text 'sujet du mail

'-> Attacher le fichier sp�cifi�
ErrorCode = 3
Select Case pFormat
    Case "0" 'turbo
        'on attache le spool turbo
        aMail.Attachments.add FileToSend
    Case "1" 'html
        '-> on transforme le spool en html
        '-> Inclure le num�ro de page
        IncluseSpoolNumber = CBool(Me.Check2.Value)
        '-> Positionnement de la navigationHTML
        If Me.Option4.Value Then
            NavigationType = 0 'FrameSet
        ElseIf Me.Option5.Value Then
            NavigationType = 1 'Fichier unique
        Else
            NavigationType = 2 'Pages
        End If
        PathToExport = GetTempFileNameVB("WWW", True)
        FileKeyMail = Trim(UCase$(FileToSend))
        '-> Nom du Fichier
        FileToExport = LCase$(GetSpoolName(FileKeyMail)) & ".html"
        '-> Cl� du fichiers
        FileKeyExport = FileKeyMail
        '-> Spool
        SpoolKeyMail = aSpool.Key
        SpoolKeyExport = SpoolKeyMail
        '-> Num�ro de page
        PageToExport = PageNumMail
        ExportType = 1
            
        '-> On ajoute le spool
        AnalyseFileToPrint FileToSend

        CreateEditionHTML
        '-> Attacher le fichier sp�cifi�
        If ListeHtmlFile <> "" Then
            For i = 1 To NumEntries(ListeHtmlFile, "|")
                aMail.Attachments.add Entry(i, ListeHtmlFile, "|")
            Next
        End If
        '-> Ajouter les images s'il y en a
        For i = 1 To NbImages
            aMail.Attachments.add ListeHTMLImages(i)
        Next
    Case "3" 'nothing
    
    Case "4" 'pdf
        '-> on g�n�re le fichier avec le turboPDF
        If FilePath = "" Then FilePath = OpenPath
        CurrentPIDProcess = Shell(App.Path & "\TurboPdf.exe " & "�fileToConvert=" & FileToSend & GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False) & "�pdfDirectory=" & Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1))
        '-> Boucler tant que le processus est actif
        Do While IsPidRunning(CurrentPIDProcess)
            '-> Lib�ration de la pile des messages
            DoEvents
            Sleep 200
        Loop
        'on attache le spool turbo
        aMail.Attachments.add (Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1) & "/" & GetSpoolName(FileToSend) & ".pdf")

End Select


'-> si il y a des fichiers joints lorsque l on vient d'un fichier zipp�
For i = 0 To MDIMain.mnuJoin.Count - 1
    If MDIMain.mnuJoin.Item(i).Visible Then
        If Trim(MDIMain.mnuJoin.Item(i).Tag) <> "" Then aMail.Attachments.add MDIMain.mnuJoin.Item(i).Tag
    End If
Next
    
'-> Afficher le mail
ErrorCode = 4
'If Dir(App.Path & "\" & "ClickYes.exe") <> "" Then Shell App.Path & "\" & "ClickYes.exe", vbNormalFocus
aMail.Display
DoEvents
If Not SendOutlookMessage(aMail.Subject) Then GoTo GestError

'Me.SetFocus
SendByOutLook = True
'-> Lib�rer les pointeurs
Set aMail = Nothing
Set aOutLook = Nothing

Exit Function
GestError:

'-> Lib�rer les pointeurs
Set aMail = Nothing
Set aOutLook = Nothing

If MsgBox("Une erreur est survenue sur le(s) destinataire(s) suivant(s) " & sAdrMail & Chr(13) & "Continuer?", vbOKCancel, "Mailing") = vbOK Then
    SendByOutLook = True
End If

End Function

Private Function sendBySMTP(ByVal FileToSend As String, sAdrMail As String) As Boolean
'--> cette fonction nous permet d'envoyer un mail
'--> par le composant winsock
Dim i As Integer
Dim j As Long
Dim m_strEncodedFiles As String
Dim l_destinataire
Dim destinataire
Dim sPieceJointe As String
Dim CurrentPIDProcess As Long
Dim sSMTP As String
Dim pdfName As String

'-> on se connecte au serveur
Winsock1.Close
iStep = 1
If GetIniString("PARAM", "SMTP", TurboGraphIniFile, False) <> "" Then
    sSMTP = GetIniString("PARAM", "SMTP", TurboGraphIniFile, False)
Else
    MsgBox "Veuillez param�trer l'adresse de votre serveur de messagerie SMTP", vbExclamation, ""
End If
Winsock1.Connect Trim(sSMTP), 25
While Winsock1.State <> sckConnected
    DoEvents
Wend
    
ReponseFaite = False
Winsock1.SendData "HELO Serveur" & vbCrLf
While ReponseFaite = False
    DoEvents
Wend

ReponseFaite = False
Winsock1.SendData "MAIL FROM: " & Replace(Trim(Me.Text3.Text), " ", "") & "" & vbCrLf
While ReponseFaite = False
DoEvents
Wend

'-> on envoie les dif�rents destinataires (s�parateur ';' dans la saisie)
iStep = 3
'-> dans le cas ou on a une copi
If Trim(Me.Text2.Text) <> "" Then
    l_destinataire = Split(Entry(3, sAdrMail & ";" & Trim(Me.Text2.Text), "~"), ";")
Else
    l_destinataire = Split(Entry(3, sAdrMail, "~"), ";")
End If

For Each destinataire In l_destinataire
    ReponseFaite = False
    Winsock1.SendData "RCPT TO: " & Trim(destinataire) & "" & vbCrLf
    While ReponseFaite = False
        DoEvents
    Wend
Next

'-> on change d'etape
iStep = 4
ReponseFaite = False
Winsock1.SendData "DATA" & vbCrLf
While ReponseFaite = False
    DoEvents
Wend

ReponseFaite = False
'-> Attacher le fichier sp�cifi�
Select Case pFormat
    Case "0" 'turbo
        'on attache le spool turbo
        sPieceJointe = FileToSend
    Case "1" 'html
        '-> on transforme le spool en html
        '-> Inclure le num�ro de page
        IncluseSpoolNumber = CBool(Me.Check2.Value)
        '-> Positionnement de la navigationHTML
        If Me.Option4.Value Then
            NavigationType = 0 'FrameSet
        ElseIf Me.Option5.Value Then
            NavigationType = 1 'Fichier unique
        Else
            NavigationType = 2 'Pages
        End If
        PathToExport = GetTempFileNameVB("WWW", True)
        FileKeyMail = Trim(UCase$(FileToSend))
        '-> Nom du Fichier
        FileToExport = LCase$(GetSpoolName(FileKeyMail)) & ".html"
        '-> Cl� du fichiers
        FileKeyExport = FileKeyMail
        '-> Spool
        SpoolKeyMail = aSpool.Key
        SpoolKeyExport = SpoolKeyMail
        '-> Num�ro de page
        PageToExport = PageNumMail
        ExportType = 1
            
        '-> On ajoute le spool
        AnalyseFileToPrint FileToSend

        CreateEditionHTML
        '-> Attacher le fichier sp�cifi�
        If ListeHtmlFile <> "" Then
            sPieceJointe = ListeHtmlFile
        End If
        '-> Ajouter les images s'il y en a
        For i = 1 To NbImages
            sPieceJointe = sPieceJointe & "|" & ListeHTMLImages(i)
        Next
    Case "3" 'nothing
    
    Case "4" 'pdf
        '-> on g�n�re le fichier avec le turboPDF
        If FilePath = "" Then FilePath = OpenPath
        '-> on a specifi� eventuellement un nom pour le pdf
        If Trim(Me.Text4.Text) <> "" Then
            pdfName = "�pdfName=" & Me.Text4.Text
            pdfName = Replace(pdfName, ".pdf", "", , , vbTextCompare)
            pdfName = pdfName & "_" & GetTickCount
            pdfName = pdfName & ".pdf"
        End If
        CurrentPIDProcess = Shell(App.Path & "\TurboPdf.exe " & "�fileToConvert=" & FileToSend & GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False) & "�pdfDirectory=" & Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1) & pdfName)
        '-> Boucler tant que le processus est actif
        Do While IsPidRunning(CurrentPIDProcess)
            '-> Lib�ration de la pile des messages
            DoEvents
            Sleep 300
        Loop
        'on attache le spool turbo
        If pdfName <> "" Then
            sPieceJointe = (Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1) & "/" & Entry(2, pdfName, "="))
        Else
            sPieceJointe = (Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1) & "/" & GetSpoolName(FileToSend) & ".pdf")
        End If
        sPieceJointe = Replace(sPieceJointe, "/", "\")
End Select

'-> on met en forme la ou les pi�ces jointes
For i = 1 To NumEntries(sPieceJointe, "|")
    m_strEncodedFiles = m_strEncodedFiles & UUEncodeFile(Entry(i, sPieceJointe, "|")) & vbCrLf
Next i

iStep = 5
'on pr�pare l'ent�te du message
Winsock1.SendData "To:" & Entry(3, sAdrMail, "~") & vbCrLf
Winsock1.SendData "From:" & Me.Text3.Text & vbCrLf
Winsock1.SendData "Subject:" & Me.Text1.Text & vbCrLf
'ChaineMessage = ChaineMessage & "CONTENT-TYPE: TEXT/HTML;" & vbCrLf & vbCrLf

'on envoie le message et la ou les pi�ces jointes si il y en a
Dim varLines    As Variant
Dim varLine     As Variant
Dim strMessage  As String

'ajout des pi�ces jointes au texte du message
strMessage = vbCrLf & vbCrLf & Me.RichTextBox1.Text & vbCrLf & vbCrLf & m_strEncodedFiles
'on r�initialise la m�moire
m_strEncodedFiles = ""
'On coupe le fichier en plusieurs lignes (pour VB6 seulement)
varLines = Split(strMessage, vbCrLf)
'on vide la m�moire
strMessage = ""
'On envoie chaque ligne du message
For Each varLine In varLines
    Winsock1.SendData CStr(varLine) & vbLf
Next
'on envoie un point pour signaler au serveur que le msg est fini
iStep = 7
Winsock1.SendData vbCrLf & "." & vbCrLf
j = 0
While ReponseFaite = False
    j = j + 1
    If j > 1000000 Then
        ReponseFaite = True
    End If
    DoEvents
Wend

ReponseFaite = False
Winsock1.SendData "QUIT" & vbCrLf
j = 0
While ReponseFaite = False
    j = j + 1
    If j > 1000000 Then
        ReponseFaite = True
    End If
    DoEvents
Wend

Winsock1.Close

'MsgBox toto

sendBySMTP = True
End Function

Private Sub Winsock1_DataArrival(ByVal bytesTotal As Long)
'--> ici sont g�r�e les r�ponses du composant winsock
Dim DonneesRecues As String
Dim strMessage As String

Winsock1.GetData DonneesRecues

'-> bon on a recu une r�ponse
ReponseFaite = True
'Exit Sub

toto = toto & Chr(13) & "Reponse : " & DonneesRecues

Select Case iStep
    Case 1 '-> connexion au serveur
        If InStr(1, DonneesRecues, "220") Then
            ReponseFaite = True
            iStep = 2
        Else
            strMessage = "Impossible de se connecter au serveur SMTP" & Chr(13) & DonneesRecues
        End If
    Case 2 '-> controle de la connexion
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
        Else
            strMessage = "Connexion refus�e par le serveur SMTP" & Chr(13) & DonneesRecues
        End If
    Case 3 '-> adresse mail de l'envoyeur et du destinataire
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
            iStep = 4
        Else
            strMessage = "Adresse mail de l'envoyeur non reconnue par le serveur SMTP" & Chr(13) & DonneesRecues
        End If
    Case 4 '-> adresse mail de l'envoyeur et du destinataire
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
        Else
            'strMessage = "Adresse mail du destinataire non reconnue par le serveur SMTP" & Chr(13) & DonneesRecues
        End If
    Case 5  '-> Envoi des donnees
        If InStr(1, DonneesRecues, "354 ") Then
            ReponseFaite = True
        Else
            strMessage = "Erreur lors de l'envoi de l'ent�te du mail" & Chr(13) & DonneesRecues
        End If
    Case 6
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
            iStep = 7
        Else
            strMessage = "Erreur lors de l'envoi du contenu du mail" & Chr(13) & DonneesRecues
        End If
    Case 7 '-> fermeture de la connexion
        If InStr(1, DonneesRecues, "221 ") Then ReponseFaite = True
End Select

If strMessage <> "" Then MsgBox strMessage

End Sub

Private Sub printDestinataires()
'--> cette proc�dure va permettre d'imprimer selon les cas les destinataires selectionn�s ou pas ou tous
'---> Envoyer un message vers Internet

Dim SpoolName As String
Dim aNode As Node
Dim PageMin As Integer
Dim PageMax As Integer
Dim aToPrint As Boolean

Dim DeviceName As String
Dim ChoixPage As String
Dim NbCopies As Integer
Dim RectoVerso As String
Dim Assembl As String
Dim NoGard As String
Dim FileToPrint As String
Dim CurrentPIDProcess As Long

On Error Resume Next

'-> on initialise
ListeHtmlFile = ""
Err.Description = ""
'on met le sablier
Screen.MousePointer = 11

If Me.Check3.Value = 1 Then
    aToPrint = True
Else
    aToPrint = False
End If

'-> vider le picture de temporisation
Me.Picture2.Cls
TailleTotale = 0
'-> Calcul de la taille total
'-> on parcourt le treeview pour les mails � envoyer
For Each aNode In Me.TreeView1.Nodes
    '-> on on compte
    If (aNode.Checked And aToPrint) And aNode.Tag <> "SPOOL" Then TailleTotale = TailleTotale + 1
Next

TailleLue = 0

'-> on parcourt le treeview pour les mails � envoyer
For Each aNode In Me.TreeView1.Nodes
    '-> on verifie que l'on est bien sur un mail � envoyer
    If (aNode.Checked And aToPrint) And aNode.Tag <> "SPOOL" Then
        '-> Ne mettre � jour les variables de traitement que si on vient de la visu et pas du menu
        '-> Cl� du spool et du fichier
        Set aSpool = Fichiers(Entry(1, aNode.Key, "�"))
        PageMin = CInt(Entry(2, Entry(2, aNode.Key, "�"), "|"))
        PageMax = CInt(Entry(2, Entry(2, aNode.Next.Key, "�"), "|")) - 1
        If PageMax < PageMin Then PageMax = aSpool.NbPage
        '-> nom du spool � traiter
        SpoolName = aSpool.FileName
        SpoolName = CreateDetail(aSpool, -2, PageMin, PageMax)
        
        '-> Vider la variable de retour
        strRetour = ""
        
        '-> Setting des param�trages sur le fichier et le spool en cours
        frmPrint.FichierName = aSpool.FileName
        frmPrint.SpoolKey = aSpool.Key
        frmPrint.IsSelectionPage = aSpool.IsSelectionPage
        
        frmPrint.Option4.Visible = False
        frmPrint.Frame2.Enabled = False
        '-> Afficher le choix de l'imprimante
        Screen.MousePointer = 0
        frmPrint.Show vbModal
        Screen.MousePointer = 11
        '-> Quitter si annuler
        If strRetour = "" Then Exit Sub
        
        '-> Traiter les choix d'impression
        FileToPrint = SpoolName
        DeviceName = Entry(1, strRetour, "|")
        NbCopies = Entry(3, strRetour, "|")
        RectoVerso = Entry(4, strRetour, "|")
        If copyAssemb Then Assembl = Entry(5, strRetour, "|")
        If noGarde Then NoGard = Entry(6, strRetour, "|")
            
        '-> Lancer l'impression
        CurrentPIDProcess = Shell(App.Path & "\TurboGraph.exe " & DeviceName & "~DIRECT~" & NbCopies & "|" & FileToPrint & "|" & RectoVerso & "|" & Assembl & "|" & NoGard, vbNormalFocus)
        '-> Boucler tant que le processus est actif
        Do While IsPidRunning(CurrentPIDProcess)
            '-> Lib�ration de la pile des messages
            DoEvents
            Sleep 1000
        Loop
        
        '-> Dessin de la temporisation
        TailleLue = TailleLue + 1
        DrawTempo Me.Picture1
        DoEvents
    End If
Next

'on met le sablier
Screen.MousePointer = 0
Me.SetFocus
If Err.Description <> "" Then MsgBox Err.Description, vbExclamation

End Sub
