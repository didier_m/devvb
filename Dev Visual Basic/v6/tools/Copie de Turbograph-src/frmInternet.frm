VERSION 5.00
Begin VB.Form frmInternet 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7215
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6375
   Icon            =   "frmInternet.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7215
   ScaleWidth      =   6375
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame5 
      Height          =   2415
      Left            =   120
      TabIndex        =   11
      Top             =   3480
      Width           =   6135
      Begin VB.TextBox lblFile 
         Height          =   285
         Left            =   240
         TabIndex        =   15
         Top             =   1920
         Width           =   5415
      End
      Begin VB.Image Image1 
         Height          =   720
         Left            =   5280
         Picture         =   "frmInternet.frx":1CCA
         Top             =   600
         Width           =   720
      End
      Begin VB.Label Label4 
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   1560
         Width           =   4455
      End
      Begin VB.Label lblPath 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   615
         Left            =   240
         TabIndex        =   13
         Top             =   720
         Width           =   4935
      End
      Begin VB.Label Label2 
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   360
         Width           =   3495
      End
   End
   Begin VB.Frame Frame4 
      Height          =   1575
      Left            =   120
      TabIndex        =   9
      Top             =   1800
      Width           =   6135
      Begin VB.OptionButton Option6 
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   1080
         Width           =   4575
      End
      Begin VB.OptionButton Option5 
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   720
         Width           =   4575
      End
      Begin VB.OptionButton Option3 
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   360
         Value           =   -1  'True
         Width           =   3735
      End
   End
   Begin VB.Frame Frame3 
      Enabled         =   0   'False
      Height          =   1095
      Left            =   120
      TabIndex        =   6
      Top             =   6000
      Width           =   4215
      Begin VB.PictureBox Picture1 
         AutoRedraw      =   -1  'True
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   3675
         TabIndex        =   7
         Top             =   720
         Width           =   3735
      End
      Begin VB.Label Label1 
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   360
         Width           =   3735
      End
   End
   Begin VB.CommandButton Command1 
      Height          =   975
      Left            =   4440
      Picture         =   "frmInternet.frx":2B94
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   6120
      Width           =   1815
   End
   Begin VB.Frame Frame2 
      Height          =   1575
      Left            =   3120
      TabIndex        =   3
      Top             =   120
      Width           =   3135
      Begin VB.CheckBox Check3 
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   1200
         Value           =   1  'Checked
         Width           =   2535
      End
      Begin VB.CheckBox Check1 
         Height          =   495
         Left            =   240
         TabIndex        =   19
         Top             =   600
         Value           =   1  'Checked
         Width           =   2535
      End
      Begin VB.CheckBox Check2 
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Value           =   1  'Checked
         Width           =   2535
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1575
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2895
      Begin VB.OptionButton Option4 
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   1080
         Width           =   2175
      End
      Begin VB.OptionButton Option2 
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   720
         Value           =   -1  'True
         Width           =   2295
      End
      Begin VB.OptionButton Option1 
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   2415
      End
   End
End
Attribute VB_Name = "frmInternet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Pointeur vers l'objet Spool
Public aSpool As Spool
'-> Indique si le fichier est multi spool
Private IsMultiSpool As Boolean

Private Sub Command1_Click()

On Error GoTo GestError

Dim fileName As String
Dim ToLaunh As Boolean
Dim aFichier As Fichier
Dim SpoolToPrint As Spool
Dim NomFile As String
Dim i As Integer, j As Integer
Dim Extension As String
Dim aLb As Libelle

'-> Pointer sur la classe libell�
Set aLb = Libelles("FRMINTERNET")

'-> V�rifier que le nom de fichier saisi soit correct
If Not IsLegalName(Me.lblFile.Text) Then
    MsgBox aLb.GetCaption(21), vbExclamation + vbOKOnly, "Erreur"
    Me.lblFile.SetFocus
    Exit Sub
End If


'-> Bloquer l'�cran
Me.Enabled = False
Me.MousePointer = 11

'-> Lib�rer la frame
Me.Frame3.Enabled = True

'-> Indiquer l'origine de l'export
OrigineExport = 0

'-> Cr�ation du nom du fichier
NomFile = Trim(Me.lblFile.Text)

'-> Traitement du nom du fichier : V�rifier s'il y a un point dans le nom du fichier
i = InStrRev(NomFile, ".")
If i <> 0 Then
    If Len(NomFile) = 1 Then
        '-> Remplacer par le nom du spool
        NomFile = GetSpoolName(Me.aSpool.fileName) & ".html"
    Else
        If Left$(NomFile, 1) = "." Then
            '-> Rajouter que l'extension
            NomFile = NomFile & "html"
        Else
            '-> Rechercher l'extension
            Extension = Mid$(NomFile, i + 1, Len(NomFile) - i)
            If UCase$(Extension) = "HTML" Or UCase$(Extension) = "HTM" Then
            Else
                NomFile = Mid$(NomFile, 1, i - 1) & ".html"
            End If
        End If
    End If
Else
    '-> Pas d'extension
    NomFile = NomFile & ".html"
End If

'-> Mettre le nom du fichier en minuscule
NomFile = LCase$(NomFile)

'-> Setting du path d'export
PathToExport = Me.lblPath
If Right$(PathToExport, 1) <> "\" Then PathToExport = PathToExport & "\"

'-> Setting du nom des fichiers d'export
FileToExport = NomFile

'-> Doit on lancer � la fin du traitement
RunAfterExport = CBool(Me.Check2.Value)

'-> Doit on inclure le num�ro des spools dans le nom des fichiers
IncluseSpoolNumber = CBool(Me.Check1.Value)

'-> Setting du nom du fichier source
FileKeyExport = UCase$(Trim(aSpool.fileName))

'-> Nom du spool
SpoolKeyExport = aSpool.Key

'-> Choix de la nature de l'export
If Me.Option1.Value Then
    ExportType = 0 'Export de la page en cours
    '-> Calcul  de la limitte maximale pour dessin de la tempo
    TailleTotale = 1
ElseIf Me.Option2.Value Then
    ExportType = 1 'Export du spool en cours
    '-> Calcul  de la limitte maximale pour dessin de la tempo
    TailleTotale = aSpool.NbPage
Else
    ExportType = 2 'Export du fichier en entier
    '-> Calcul du nombre total de page � imprimer
    '-> Pointer sur le fichier
    Set aFichier = Fichiers(UCase$(Trim(aSpool.fileName)))
    TailleTotale = 0
    For Each SpoolToPrint In aFichier.Spools
        TailleTotale = TailleTotale + SpoolToPrint.NbPage
    Next
End If

'-> Choix du type de navigation
If Me.Option3.Value Then
    NavigationType = 0 'FrameSet
ElseIf Me.Option5.Value Then
    NavigationType = 1 'FichierUnique
Else
    NavigationType = 2 'Simples pages
End If

'-> Indiquer le num�ro de la page en cours
PageToExport = aSpool.CurrentPage

'-> Export des bordures
If Me.Check3.Value = 1 Then
    ExportBordure = True
Else
    ExportBordure = False
End If

'-> Lancer l'impression
CreateEditionHTML

GestError:
    '-> D�bloquer l'�cran
    Me.Enabled = True
    Me.MousePointer = 0
    '-> D�charger la page
    Unload Me


End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim IsNavigaHTML As Boolean
Dim aFichier As Fichier

On Error Resume Next

'-> Gestion des messages
Set aLb = Libelles("FRMINTERNET")
Me.Caption = aLb.GetCaption(1)
Me.Frame1.Caption = aLb.GetCaption(2)
Me.Option1.Caption = aLb.GetCaption(3)
Me.Option2.Caption = aLb.GetCaption(4)
Me.Option4.Caption = aLb.GetCaption(14)
Me.Frame2.Caption = aLb.GetCaption(5)
Me.Check1.Caption = aLb.GetCaption(15)

Me.Check2.Caption = aLb.GetCaption(7)
Me.Frame3.Caption = aLb.GetCaption(8)
Me.Command1.Caption = aLb.GetCaption(10)
Me.Frame4.Caption = aLb.GetCaption(11)
Me.Option3.Caption = aLb.GetCaption(12)
Me.Option5.Caption = aLb.GetCaption(16)
Me.Option6.Caption = aLb.GetCaption(17)
Me.Frame5.Caption = aLb.GetCaption(18)
Me.Label2.Caption = aLb.GetCaption(19)
Me.Label4.Caption = aLb.GetCaption(20)
Me.Check3.Caption = aLb.GetCaption(22)

'-> V�rifier que l'on trouve les objets de navigation
IsNavigaHTML = True

'-> FrameSet
'If Dir$(App.Path & "\internet\" & Replace(NavigaSpoolHtml, "$CHRONO$", "")) = "" Then IsNavigaHTML = False
'-> Palette de navigation
If Dir$(App.Path & "\internet\" & Replace(NavigaBarHtmlFile, "$CHRONO$", "")) = "" Then IsNavigaHTML = False

'-> Gestion des options
If Not IsNavigaHTML Then
    Me.Option3.Enabled = False
    Me.Option3.Value = False
End If

'-> Tester si on est en multi spool ou non pour le fichier
Set aFichier = Fichiers(UCase$(Trim(aSpool.fileName)))
If aFichier.Spools.Count <> 0 Then IsMultiSpool = True

'-> Charger le path www
Me.lblPath = PathToExport

'-> Charger le nom du fichier
Me.lblFile = LCase$(GetSpoolName(Me.aSpool.fileName)) & ".html"

If Me.lblFile.Text = "" Or Me.lblPath.Caption = "" Then Me.Command1.Enabled = False


End Sub


Private Sub Image1_Click()
Dim MyPath As String

MyPath = GetPathForm(Me.hwnd, False)
If MyPath <> "" Then
    If Right$(MyPath, 1) <> "\" Then MyPath = MyPath & "\"
    Me.lblPath.Caption = MyPath
    
    If Trim(Me.lblFile.Text) = "" Or Me.lblPath.Caption = "" Then
        Me.Command1.Enabled = False
    Else
        Me.Command1.Enabled = True
    End If

End If

End Sub

Private Sub lblFile_Change()

If Trim(Me.lblFile.Text) = "" Or Me.lblPath.Caption = "" Then
    Me.Command1.Enabled = False
Else
    Me.Command1.Enabled = True
End If

End Sub





