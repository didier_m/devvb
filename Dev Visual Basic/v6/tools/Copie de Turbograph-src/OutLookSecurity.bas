Attribute VB_Name = "OutLookSecurity"
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" _
        (ByVal lpClassName As String, ByVal lpWindowName As String) As Long

Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" _
        (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, _
        ByVal lParam As Long) As Long

Private Declare Function FindWindowEx Lib "user32" Alias "FindWindowExA" _
        (ByVal hWnd1 As Long, ByVal hWnd2 As Long, ByVal lpsz1 As String, _
        ByVal lpsz2 As String) As Long

Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public Declare Function EnumWindows Lib "user32" (ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long

Private Declare Function GetCursorPos Lib "user32" _
    (lpPoint As POINTAPI) As Long

Private Declare Function SetCursorPos Lib "user32" _
    (ByVal x As Long, ByVal Y As Long) As Long

Private Declare Function GetWindowRect Lib "user32" _
    (ByVal hwnd As Long, lpRect As RECT) As Long

Private Declare Sub mouse_event Lib "user32" _
    (ByVal dwFlags As Long, ByVal dx As Long, ByVal dy _
    As Long, ByVal cButtons As Long, ByVal dwExtraInfo As Long)

Private Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String, ByVal cch As Long) As Long

Private Declare Function GetParent Lib "user32" (ByVal hwnd As Long) As Long

Private Declare Function IsWindowVisible Lib "user32" (ByVal hwnd As Long) As Long

Private Declare Function GetWindowTextLength Lib "user32" Alias "GetWindowTextLengthA" (ByVal hwnd As Long) As Long

Private Declare Function GetClassName Lib "user32" _
    Alias "GetClassNameA" _
   (ByVal hwnd As Long, _
    ByVal lpClassName As String, _
    ByVal nMaxCount As Long) As Long

Private Type RECT
        Left As Long
        Top As Long
        Right As Long
        Bottom As Long
End Type

Private Type POINTAPI
    x As Long
    Y As Long
End Type


Const WM_ACTIVATE = &H6
Const MA_ACTIVATE = 1

Const BM_CLICK = &HF5
Const BM_SETCHECK = &HF1

Const MOUSEEVENTF_LEFTDOWN = &H2
Const MOUSEEVENTF_LEFTUP = &H4

Const CB_GETCOUNT = &H146
Const CB_SETCURSEL = &H14E


' Mutex Stuff
Private Declare Function CreateMutex Lib "kernel32" Alias "CreateMutexA" (lpMutexAttributes As Any, ByVal bInitialOwner As Long, ByVal lpName As String) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Private Declare Function ReleaseMutex Lib "kernel32" (ByVal hMutex As Long) As Long
Const ERROR_ALREADY_EXISTS = 183&
Dim mutex As Long
Private HdlMessage As Long
Private sMessage As String
Public TitreFenetre As String

Private Declare Function ShowWindow Lib "user32" (ByVal hwnd As Long, ByVal nCmdShow As Long) As Long

Public Sub InitKillSecurity()
    ' Setup a mutex
    If SetupMutex Then
        '-> on lance le killer
        KillSecurity 5
    End If
    '-> on ferme
    ReleaseMutex mutex
    CloseHandle mutex

End Sub


' Set the security popup to allow us to access contact info
' for the next 10 minutes. This only works if the poup
' is actually up.
Sub KillSecurity(Seconds As Integer)
    Dim hWndParent&, hwndChild&, hWndCheck&, hWndCombo&
    Dim i As Integer
    Dim comboItems As Integer
    Dim mousepos As POINTAPI
    Dim lpRect As RECT

    i = 0
    Do
        hWndParent = 0
        i = i + 1

        ' Get highest parent window handle
        hWndOutlook = FindWindow(vbNullString, "Microsoft Outlook")

        ' Get parent window handle
        hWndParent = FindWindow("#32770", "Microsoft Office Outlook")
        If hWndParent = 0 Then
            hWndParent = FindWindow("#32770", "Microsoft Outlook")
        End If

        ' If we found a handle
        If hWndParent Then
            hwndChild = 0
            hWndCombo = 0
            hWndCheck = 0

            ' Find the 'Yes' button and other things in the security box
            hwndChild = FindWindowEx(hWndParent, 0, "Button", "Oui")
            If hwndChild = 0 Then
                hwndChild = FindWindowEx(hWndParent, 0, "Button", "&Oui")
            End If

            hWndCombo = FindWindowEx(hWndParent, 0, "ComboBox", "")
            hWndCheck = FindWindowEx(hWndParent, 0, "Button", "&Non")


            ' If we found the security dialog box, let's own it
            If hwndChild And hWndCheck And hWndCombo Then

                ' Focus Outlook -- Outlook's security patch attempts to
                ' stop intruders just clicking the 'yes' which is why
                ' no one has been able to get around this yet. It only
                ' works if the window is given manual focus from the
                ' mouse, and not a system call like SetForegroundWindow.
                ' I rule.
                'Call GetCursorPos(mousepos)
                'Call GetWindowRect(hWndParent, lpRect)
                'Call SetCursorPos(lpRect.Left + 10, lpRect.Top + 10)
                'Call mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0)
                'Call Sleep(0)
                'Call mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0)
                'Call SetCursorPos(mousepos.X, mousepos.Y)

                ' Check the 'Allow access for' checkbox
                'Call SendMessage(hWndCheck, BM_SETCHECK, 1, 0)

                ' Get number of drop down items
                'comboItems = SendMessage(hWndCombo, CB_GETCOUNT, 0, 0)

                ' Set minutes to number of drop down items (0 based)
                'Call SendMessage(hWndCombo, CB_SETCURSEL, comboItems - 1, 0)

                ' Click 'Yes'
                Call SendMessage(hwndChild, WM_ACTIVATE, MA_ACTIVATE, 0)
                Call SendMessage(hwndChild, BM_CLICK, 0, 0)
            End If
        End If

        Sleep 50
    Loop While i < Seconds * 20
    
End Sub

' Create mutex
Private Function SetupMutex() As Boolean
    ' set a mutex up
    mutex = CreateMutex(ByVal 0&, 1, "Outsmartmutex")
    If Err.LastDllError = ERROR_ALREADY_EXISTS Then
        ' Clean up
        ReleaseMutex mutex
        CloseHandle mutex
        SetupMutex = False
    Else
        SetupMutex = True
    End If
End Function

Public Function SendOutlookMessage(sMessage As String) As Boolean
'--> cette procédure va permettre de simuler le click sur le bouton envoyer de outlook
Dim hWndParent, hwndChild
Dim sText As String * 255
Dim mousepos As POINTAPI
Dim lpRect As RECT
Dim iTimer As Integer

On Error GoTo GestError

hwndChild = 0

If sMessage = "" Then
    sMessage = "Message"
Else
    sMessage = sMessage
End If
'-> on se positionne sur la feuille
Do
    hWndParent = FindWindowMessage(sMessage)
    If iTimer <> 0 Then Sleep 50
    iTimer = iTimer + 50
Loop While hWndParent = 0 And iTimer < 10000

If hWndParent = 0 Then GoTo GestError
                
AppActivate TitreFenetre

hwndChild = FindWindowEx(hWndParent, 0, "MsoCommandBarDock", "MSODockTop")
hWndParent = FindWindowEx(hwndChild, 0, "MsoCommandBar", "Enveloppe")
If hWndParent = 0 Then
    hWndParent = FindWindowEx(hwndChild, 0, "MsoCommandBar", "Standard")
End If
'-> on recupere les coordonnées
Call GetCursorPos(mousepos)

'-> on click sur le bouton envoyer
iTimer = 0
Do
    Call GetWindowRect(hWndParent, lpRect)
    Call SetCursorPos(lpRect.Left + 17, lpRect.Top + 10)
    AppActivate TitreFenetre
    Call mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0)
    iTimer = iTimer + 150
    Sleep 350
    Call mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0)
Loop While (FindWindowMessage(sMessage) <> 0) And iTimer < 5000
'-> on restaure les coordonnées
Call SetCursorPos(mousepos.x, mousepos.Y)
If FindWindow(vbNullString, sMessage) <> 0 Then GoTo GestError
'-> valeur de succes
SendOutlookMessage = True
Exit Function
'-> gestion des erreurs
GestError:
SendOutlookMessage = False
'-> on restaure si besoin la souris
If mousepos.x <> 0 Then
    Call SetCursorPos(mousepos.x, mousepos.Y)
End If
End Function


Private Function EnumWindowProc(ByVal hwnd As Long, ByVal lParam As Long) As Long
   
    Dim nSize As Long
    Dim sTitle As String
    Dim sClass As String
   
    Dim sIDType As String
    Dim itmX As ListItem
    Dim nodX As Node
   
    On Error Resume Next
   
    '-> on elimine les fenetres qui ne sont pas au top level
    If GetParent(hwnd) = 0& And IsWindowVisible(hwnd) Then
        '-> on reccupere le titre de la feuille
        sTitle = GetWindowIdentification(hwnd, sIDType, sClass)
        If InStr(1, sTitle, sMessage, vbTextCompare) <> 0 Then         'And InStr(1, sClass & "         ", "Opus", vbTextCompare) <> 0 Then
            HdlMessage = hwnd
            TitreFenetre = sTitle
            EnumWindowProc = 0
            Exit Function
        End If
    End If
    EnumWindowProc = 1
      
End Function

Public Function FindWindowMessage(strMessage As String) As Long
   '-> on sauvegarde la chaine a rechercher
   On Error Resume Next
   HdlMessage = 0
   sMessage = strMessage
   '-> on recherche la fenetre
   Call EnumWindows(AddressOf EnumWindowProc, &H0)
   FindWindowMessage = HdlMessage
End Function

Private Function GetWindowIdentification(ByVal hwnd As Long, _
                                         sIDType As String, _
                                         sClass As String) As String

   Dim nSize As Long
   Dim sTitle As String

  'get the size of the string required
  'to hold the window title
   nSize = GetWindowTextLength(hwnd)
   
  'if the return is 0, there is no title
   If nSize > 0 Then
   
      sTitle = Space$(nSize + 1)
      Call GetWindowText(hwnd, sTitle, nSize + 1)
      sIDType = "title"
      
      sClass = Space$(64)
      Call GetClassName(hwnd, sClass, 64)
   
   Else
   
     'no title, so get the class name instead
      sTitle = Space$(64)
      Call GetClassName(hwnd, sTitle, 64)
      sClass = sTitle
      sIDType = "class"
   
   End If
   
   GetWindowIdentification = TrimNull(sTitle)

End Function

