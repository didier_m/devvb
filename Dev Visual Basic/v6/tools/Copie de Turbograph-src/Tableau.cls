VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Tableau"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'**********************************************
'*                                            *
'* Cette classe contient la d�finition d'un   *
'* Objet Tableau                              *
'*                                            *
'**********************************************

'---> Propri�t�s de l'objet
Public Nom As String
Public Orientation As Integer
Public Largeur As Single
Public IdAffichage As Integer
Private pOrdreAffichage() As String

'-> Cette variable indique l'index du prochain block � cr�er
Public nBlocks As Integer
Public nEntries As Integer

'---> Objets internes
Public Blocks As Collection


Private Sub Class_Initialize()
    Set Blocks = New Collection
    nBlocks = 1
    nEntries = nBlock
End Sub

Public Function GetOrdreAffichage(ByVal nIndex As Integer) As String
    GetOrdreAffichage = pOrdreAffichage(nIndex)
End Function

Public Function AddOrdreAffichage(ByVal Value As String) As Integer
    ReDim Preserve pOrdreAffichage(1 To nBlocks)
    pOrdreAffichage(nBlocks) = Value
    AddOrdreAffichage = nBlocks
    nBlocks = nBlocks + 1
    nEntries = nBlocks
End Function

Public Sub ClearOrdreAffichage()
    Erase pOrdreAffichage()
    nBlocks = 1
    nEntries = 1
End Sub
