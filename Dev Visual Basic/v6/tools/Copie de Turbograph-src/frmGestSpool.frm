VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form frmGestSpool 
   Caption         =   "Gestion des spools"
   ClientHeight    =   10845
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   14070
   Icon            =   "frmGestSpool.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   10845
   ScaleWidth      =   14070
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox Check2 
      Caption         =   "R�sum� des fichiers"
      Height          =   195
      Left            =   8160
      TabIndex        =   43
      Top             =   6240
      Visible         =   0   'False
      Width           =   3255
   End
   Begin VB.CommandButton Command4 
      Caption         =   "<"
      Height          =   375
      Left            =   11280
      TabIndex        =   41
      ToolTipText     =   "Page p�c�dente du spool"
      Top             =   5520
      Width           =   375
   End
   Begin VB.CommandButton Command1 
      Caption         =   ">"
      Height          =   375
      Left            =   11760
      TabIndex        =   40
      ToolTipText     =   "Page suivante du spool"
      Top             =   5520
      Width           =   375
   End
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   6480
      Picture         =   "frmGestSpool.frx":08CA
      ScaleHeight     =   495
      ScaleWidth      =   7935
      TabIndex        =   38
      ToolTipText     =   "Changer le r�pertoire d'analyse"
      Top             =   40
      Width           =   7935
      Begin VB.Label Label14 
         Caption         =   "Label14"
         Height          =   255
         Left            =   480
         TabIndex        =   39
         ToolTipText     =   "Changer le r�pertoire d'analyse"
         Top             =   120
         Width           =   7095
      End
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   5280
      Top             =   5520
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestSpool.frx":1194
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestSpool.frx":1A6E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestSpool.frx":4000
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestSpool.frx":48DA
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestSpool.frx":6C5C
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestSpool.frx":7536
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestSpool.frx":7E10
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestSpool.frx":4AD22
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestSpool.frx":4B5FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestSpool.frx":4BED6
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestSpool.frx":4C7B0
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestSpool.frx":4D08A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Aper�u"
      Height          =   195
      Left            =   8160
      TabIndex        =   1
      Top             =   5760
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.PictureBox Picture2 
      Height          =   4815
      Left            =   8160
      ScaleHeight     =   4755
      ScaleWidth      =   3915
      TabIndex        =   2
      Top             =   600
      Width           =   3975
      Begin VB.Image Image1 
         Height          =   5295
         Left            =   0
         MouseIcon       =   "frmGestSpool.frx":4DF64
         MousePointer    =   99  'Custom
         Stretch         =   -1  'True
         Top             =   -600
         Width           =   3975
      End
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   4815
      Left            =   120
      TabIndex        =   0
      Top             =   630
      Width           =   7935
      _ExtentX        =   13996
      _ExtentY        =   8493
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ColHdrIcons     =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   9
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Fichier"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Date de modification"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Text            =   "Taille"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Auteur"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Cat�gorie"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "R�sum�"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "Mots-cl�s"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "Objet"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "Titre"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4680
      Top             =   5520
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestSpool.frx":4E82E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestSpool.frx":54450
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog OpenDlg 
      Left            =   5880
      Top             =   5520
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3375
      Left            =   120
      TabIndex        =   3
      Top             =   7320
      Width           =   12735
      _ExtentX        =   22463
      _ExtentY        =   5953
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "Filtrer les fichiers"
      TabPicture(0)   =   "frmGestSpool.frx":549EA
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "R�sum�"
      TabPicture(1)   =   "frmGestSpool.frx":54A06
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame2 
         Caption         =   "Filtre"
         Height          =   2655
         Left            =   240
         TabIndex        =   19
         Top             =   480
         Width           =   12255
         Begin VB.TextBox Text10 
            Height          =   285
            Left            =   7320
            TabIndex        =   28
            Top             =   1800
            Width           =   4575
         End
         Begin VB.TextBox Text9 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1036
               SubFormatType   =   3
            EndProperty
            Height          =   285
            Left            =   8640
            TabIndex        =   27
            Top             =   1320
            Width           =   1215
         End
         Begin VB.TextBox Text8 
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "dd/MM/yyyy"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1036
               SubFormatType   =   0
            EndProperty
            Height          =   285
            Left            =   7320
            TabIndex        =   26
            Top             =   1320
            Width           =   1215
         End
         Begin VB.TextBox Text7 
            Height          =   285
            Left            =   7320
            TabIndex        =   25
            Top             =   840
            Width           =   4575
         End
         Begin VB.ComboBox Combo5 
            Height          =   315
            Left            =   7320
            Sorted          =   -1  'True
            TabIndex        =   24
            Text            =   "Combo1"
            Top             =   360
            Width           =   4575
         End
         Begin VB.ComboBox Combo4 
            Height          =   315
            Left            =   1320
            Sorted          =   -1  'True
            TabIndex        =   23
            Text            =   "Combo1"
            Top             =   1800
            Width           =   4575
         End
         Begin VB.ComboBox Combo3 
            Height          =   315
            Left            =   1320
            Sorted          =   -1  'True
            TabIndex        =   22
            Text            =   "Combo1"
            Top             =   1320
            Width           =   4575
         End
         Begin VB.ComboBox Combo2 
            Height          =   315
            Left            =   1320
            Sorted          =   -1  'True
            TabIndex        =   21
            Text            =   "Combo1"
            Top             =   840
            Width           =   4575
         End
         Begin VB.ComboBox Combo1 
            Height          =   315
            Left            =   1320
            Sorted          =   -1  'True
            TabIndex        =   20
            Text            =   "Combo1"
            Top             =   360
            Width           =   4575
         End
         Begin VB.Label Label15 
            Caption         =   "Nom :"
            Height          =   255
            Left            =   6240
            TabIndex        =   42
            Top             =   1800
            Width           =   2295
         End
         Begin VB.Label Label13 
            BackStyle       =   0  'Transparent
            Caption         =   "Date min/max "
            Height          =   255
            Left            =   6240
            TabIndex        =   35
            Top             =   1320
            Width           =   1095
         End
         Begin VB.Label Label12 
            Caption         =   "Contenant :"
            Height          =   255
            Left            =   6240
            TabIndex        =   34
            Top             =   840
            Width           =   2295
         End
         Begin VB.Label Label11 
            Caption         =   "Mots-cl�s :"
            Height          =   255
            Left            =   6240
            TabIndex        =   33
            Top             =   360
            Width           =   2295
         End
         Begin VB.Label Label10 
            Caption         =   "Cat�gorie :"
            Height          =   255
            Left            =   240
            TabIndex        =   32
            Top             =   1800
            Width           =   2295
         End
         Begin VB.Label Label9 
            Caption         =   "Auteur :"
            Height          =   255
            Left            =   240
            TabIndex        =   31
            Top             =   1320
            Width           =   2295
         End
         Begin VB.Label Label8 
            Caption         =   "Objet :"
            Height          =   255
            Left            =   240
            TabIndex        =   30
            Top             =   840
            Width           =   2295
         End
         Begin VB.Label Label7 
            Caption         =   "Titre :"
            Height          =   255
            Left            =   240
            TabIndex        =   29
            Top             =   360
            Width           =   2295
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "R�sum�"
         Height          =   2655
         Left            =   -74760
         TabIndex        =   4
         Top             =   480
         Width           =   12255
         Begin VB.CommandButton Command5 
            Caption         =   "Suppr"
            Height          =   495
            Left            =   6960
            TabIndex        =   44
            Top             =   1920
            Visible         =   0   'False
            Width           =   1575
         End
         Begin VB.TextBox Text1 
            Height          =   285
            Left            =   1320
            TabIndex        =   12
            Top             =   360
            Width           =   4575
         End
         Begin VB.TextBox Text2 
            Height          =   285
            Left            =   1320
            TabIndex        =   11
            Top             =   840
            Width           =   4575
         End
         Begin VB.TextBox Text3 
            Height          =   285
            Left            =   1320
            TabIndex        =   10
            Top             =   1320
            Width           =   4575
         End
         Begin VB.TextBox Text4 
            Height          =   285
            Left            =   1320
            TabIndex        =   9
            Top             =   1800
            Width           =   4575
         End
         Begin VB.TextBox Text5 
            Height          =   285
            Left            =   7320
            TabIndex        =   8
            Top             =   360
            Width           =   4575
         End
         Begin VB.TextBox Text6 
            Height          =   885
            Left            =   7320
            MultiLine       =   -1  'True
            TabIndex        =   7
            Top             =   840
            Width           =   4575
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Annuler"
            Height          =   495
            Left            =   10320
            TabIndex        =   6
            Top             =   1920
            Width           =   1575
         End
         Begin VB.CommandButton Command3 
            Caption         =   "Appliquer"
            Height          =   495
            Left            =   8640
            TabIndex        =   5
            Top             =   1920
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "Titre :"
            Height          =   255
            Left            =   240
            TabIndex        =   18
            Top             =   360
            Width           =   2295
         End
         Begin VB.Label Label2 
            Caption         =   "Objet :"
            Height          =   255
            Left            =   240
            TabIndex        =   17
            Top             =   840
            Width           =   2295
         End
         Begin VB.Label Label3 
            Caption         =   "Auteur :"
            Height          =   255
            Left            =   240
            TabIndex        =   16
            Top             =   1320
            Width           =   2295
         End
         Begin VB.Label Label4 
            Caption         =   "Cat�gorie :"
            Height          =   255
            Left            =   240
            TabIndex        =   15
            Top             =   1800
            Width           =   2295
         End
         Begin VB.Label Label5 
            Caption         =   "Mots-cl�s :"
            Height          =   255
            Left            =   6240
            TabIndex        =   14
            Top             =   360
            Width           =   2295
         End
         Begin VB.Label Label6 
            Caption         =   "R�sum� :"
            Height          =   255
            Left            =   6240
            TabIndex        =   13
            Top             =   840
            Width           =   2295
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   570
      Left            =   0
      TabIndex        =   36
      Top             =   0
      Width           =   14070
      _ExtentX        =   24818
      _ExtentY        =   1005
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Style           =   1
      ImageList       =   "ImageList2"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   10
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "VALID"
            Object.ToolTipText     =   "Ouvrir le fichier"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PREVIEW"
            Object.ToolTipText     =   "Pr�visualiser le fichier"
            ImageIndex      =   2
            Style           =   1
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SUPPR"
            Object.ToolTipText     =   "Supprimer le (les) fichier(s) [Suppr]"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "RENAME"
            Object.ToolTipText     =   "Renommer le fichier [F2]"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SAVE"
            Object.ToolTipText     =   "Sauvegarder le fichier [Ctrl+S]"
            ImageIndex      =   7
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PRINT"
            Object.ToolTipText     =   "Imprimer le fichier [Ctrl+P]"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "REFRESH"
            Object.ToolTipText     =   "R�actualiser et r�initialiser [F5]"
            ImageIndex      =   8
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FUSION"
            Object.ToolTipText     =   "Fusionner les spools s�lectionn�s"
            ImageIndex      =   9
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ECLAT"
            Object.ToolTipText     =   "Eclater le spool s�lectionn�"
            ImageIndex      =   10
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ABSTRACT"
            ImageIndex      =   12
            Style           =   1
         EndProperty
      EndProperty
      Begin VB.PictureBox Picture1 
         AutoSize        =   -1  'True
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         Height          =   360
         Left            =   11760
         ScaleHeight     =   24
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   82
         TabIndex        =   37
         Top             =   120
         Width           =   1230
      End
   End
   Begin VB.Image Image2 
      Height          =   5280
      Left            =   8040
      MousePointer    =   9  'Size W E
      Top             =   120
      Width           =   120
   End
   Begin VB.Menu mnu_popup 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu mnu_open 
         Caption         =   "Ouvrir"
      End
      Begin VB.Menu mnu_delete 
         Caption         =   "Supprimer"
      End
      Begin VB.Menu mnu_rename 
         Caption         =   "Renommer"
      End
      Begin VB.Menu mnu_preview 
         Caption         =   "Aper�u"
      End
   End
End
Attribute VB_Name = "frmGestSpool"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim spath As String
Dim DepartX As Single
Dim DepartY As Single
Dim DepartZ As Single
Dim DSO
Dim DSOprop
Dim ozip As New CGUnzipFiles
Dim countFile As Long
Dim countFileT As Long
Dim curPage As Integer
Dim aColItem As Collection
Public TopStop As Boolean

Public Sub Initialize()
'--> charge le listview a partir des spools existant
'-> gestion des erreurs
On Error Resume Next

Screen.MousePointer = 11
If TurbosavePath <> "" Then
    spath = TurbosavePath
    If Dir(TurbosavePath) = "" Then
        MkDir GetSpecialfolder(CSIDL_PERSONAL) & "\mes spools\"
        spath = GetSpecialfolder(CSIDL_PERSONAL) & "\mes spools\"
    End If
Else
    '-> on regarde ou est le spool en cours
    If Fichiers.Count <> 0 Then
        spath = Mid(Fichiers(1).fileName, 1, InStrRev(Replace(Fichiers(1).fileName, "/", "\"), "\"))
    Else
        MkDir GetSpecialfolder(CSIDL_PERSONAL) & "\mes spools\"
        spath = GetSpecialfolder(CSIDL_PERSONAL) & "\mes spools\"
    End If
End If

TurbosavePath = spath

'-> on regarde si on doit charger les propri�t�s
Me.Check2.Value = GetIniString("PARAM", "DSOFILE", TurboGraphIniFile, False)
Me.Toolbar1.Buttons("ABSTRACT").Value = Me.Check2.Value

'-> on regarde si on a le droit de changer le repertoire
If GetIniString("PARAM", "STATUT", TurboGraphIniFile, False) = "USER" Then
    Me.Picture3.Visible = False
    Me.Label14.Visible = False
End If

Set DSO = CreateObject("DSOFile.OleDocumentProperties")
'-> on lance l'affectation des propri�t�s
Call FilesSetProperties(spath, "*.*")

If Me.ListView1.ListItems.Count <> 0 Then
    Set Me.ListView1.SelectedItem = Me.ListView1.ListItems(1)
End If

Image1.Height = 0
Image1.Width = 0
DepartZ = 0.98 '2 / 3
formResize
Screen.MousePointer = 0

GestError:
End Sub

Private Sub FiltreListView(Optional NotReload As Boolean)
'-> cette proc�dure permet de filtrer les donn�es affich�es
Dim aItem As ListItem
Dim i As Integer
Dim TopSuppr As Boolean
DoEvents
LockWindowUpdate Me.hwnd
Me.MousePointer = 11
Dim j As Integer

On Error Resume Next

'-> on pose le top
TopStop = False

'-> on recharge completement le listview
If Not NotReload Then
        '-> on recharge le listview
    countFile = 0
    countFileT = aColItem.Count
    frmWait.Show
    
    Me.ListView1.ListItems.Clear
    For Each aItem In aColItem
        countFile = countFile + 1
        If Rnd < 0.05 Then
            frmWait.Label1.Caption = CInt(countFile / countFileT * 100) & "%   " & countFile & " fichiers analys�(s)      " & j & " fichiers trouv�s" & Chr(13) & "[Echap] pour quitter l'analyse"
            DoEvents
        End If
        TopSuppr = False
        '-> on regarde si on a pas deja l'entr�e
        If aItem.ListSubItems.Count > 2 Then
            If Trim(UCase(Me.Combo1.Text)) <> Trim(UCase(aItem.ListSubItems(8))) And Trim(UCase(Me.Combo1.Text)) <> "(TOUS)" Then TopSuppr = True
            If Trim(UCase(Me.Combo2.Text)) <> Trim(UCase(aItem.ListSubItems(7))) And Trim(UCase(Me.Combo2.Text)) <> "(TOUS)" Then TopSuppr = True
            If Trim(UCase(Me.Combo3.Text)) <> Trim(UCase(aItem.ListSubItems(3))) And Trim(UCase(Me.Combo3.Text)) <> "(TOUS)" Then TopSuppr = True
            If Trim(UCase(Me.Combo4.Text)) <> Trim(UCase(aItem.ListSubItems(4))) And Trim(UCase(Me.Combo4.Text)) <> "(TOUS)" Then TopSuppr = True
            If Trim(UCase(Me.Combo5.Text)) <> Trim(UCase(aItem.ListSubItems(6))) And Trim(UCase(Me.Combo5.Text)) <> "(TOUS)" Then TopSuppr = True
        End If
        '-> on regarde si on trouve la valeur recherch�e
        If Trim(Me.Text7.Text) <> "" And Not TopSuppr Then
            stopSearch = False
            If Not FileFindText(GetUnzipFileName(aItem.Key), Me.Text7.Text) Then TopSuppr = True
        End If
        '-> si on a saisi des dates
        If Trim(Me.Text8.Text + Me.Text9.Text) <> "" And Not TopSuppr Then
            If Trim(Me.Text8.Text) <> "" Then
                If FORMAT(aItem.ListSubItems(1), "yyyymmdd") < FORMAT(Me.Text8.Text, "yyyymmdd") Then TopSuppr = True
            End If
            If Trim(Me.Text9.Text) <> "" Then
                If FORMAT(aItem.ListSubItems(1), "yyyymmdd") > FORMAT(Me.Text9.Text, "yyyymmdd") Then TopSuppr = True
            End If
        End If
        '-> si on doit filtrer sur un ,om de fichier
        If Trim(Me.Text10.Text) <> "" And Not TopSuppr Then
            If InStr(1, aItem.Text, Replace(Me.Text10.Text, "*", ""), vbTextCompare) = 0 Then TopSuppr = True
        End If
        
        '-> si on a doit supprimer l'entr�e
        If Not TopSuppr Then
            '-> si on a envoy� un top d'arret
            If TopStop = True Then Exit Sub
            j = j + 1
            Me.ListView1.ListItems.add , aItem.Key, aItem.Text, aItem.Icon
            For i = 1 To Me.ListView1.ColumnHeaders.Count - 1
                Me.ListView1.ListItems(aItem.Key).SubItems(i) = aItem.ListSubItems(i)
            Next
        End If
        '-> si on a envoy� un top d'arret
        If TopStop = True Then GoTo GestError
        DoEvents
    Next
Else
    '-> on filtre selon les combo
    '-> on parcour le listview
    For i = Me.ListView1.ListItems.Count To 1 Step -1
        If i < 1 Then Exit For
        Set aItem = Me.ListView1.ListItems(i)
        TopSuppr = False
        '-> on regarde si on a pas deja l'entr�e
        If Trim(UCase(Me.Combo1.Text)) <> Trim(UCase(aItem.SubItems(8))) And Trim(UCase(Me.Combo1.Text)) <> "(TOUS)" Then TopSuppr = True
        If Trim(UCase(Me.Combo2.Text)) <> Trim(UCase(aItem.SubItems(7))) And Trim(UCase(Me.Combo2.Text)) <> "(TOUS)" Then TopSuppr = True
        If Trim(UCase(Me.Combo3.Text)) <> Trim(UCase(aItem.SubItems(3))) And Trim(UCase(Me.Combo3.Text)) <> "(TOUS)" Then TopSuppr = True
        If Trim(UCase(Me.Combo4.Text)) <> Trim(UCase(aItem.SubItems(4))) And Trim(UCase(Me.Combo4.Text)) <> "(TOUS)" Then TopSuppr = True
        If Trim(UCase(Me.Combo5.Text)) <> Trim(UCase(aItem.SubItems(6))) And Trim(UCase(Me.Combo5.Text)) <> "(TOUS)" Then TopSuppr = True
        '-> on regarde si on trouve la valeur recherch�e
        If Trim(Me.Text7.Text) <> "" Then
            If TopSuppr = False Then
                stopSearch = False
                If Not FileFindText(GetUnzipFileName(aItem.Key), Me.Text7.Text) Then TopSuppr = True
            End If
        End If
        '-> si on a saisi des dates
        If Trim(Me.Text8.Text + Me.Text9.Text) <> "" Then
            If Trim(Me.Text8.Text) <> "" Then
                If FORMAT(aItem.SubItems(1), "yyyymmdd") < FORMAT(Me.Text8.Text, "yyyymmdd") Then TopSuppr = True
            End If
            If Trim(Me.Text9.Text) <> "" Then
                If FORMAT(aItem.SubItems(1), "yyyymmdd") > FORMAT(Me.Text9.Text, "yyyymmdd") Then TopSuppr = True
            End If
        End If
        '-> si on a doit supprimer l'entr�e
        If TopSuppr Then
            'i = 0
            Me.ListView1.ListItems.Remove aItem.Key
        End If
        '-> si on a envoy� un top d'arret
        If TopStop = True Then GoTo GestError
    Next
End If

GestError:
Unload frmWait
Me.Caption = "        (" & Me.ListView1.ListItems.Count & ") fichiers trouv�s"
Me.Image1.Picture = Nothing

LockWindowUpdate 0
Me.ListView1.SetFocus
Me.MousePointer = 0
End Sub

Private Sub LoadCombo(aCol As Integer, sCombo As ComboBox)
'-> cette proc�dure permet de recharger la combo par rapport aux entr�es existantes
Dim aItem As ListItem
Dim i As Integer

'-> on vide la combo
sCombo.Clear

'-> on parcour le listview
For Each aItem In Me.ListView1.ListItems
    '-> on regarde si on a pas deja l'entr�e
    For i = 0 To sCombo.ListCount
        If UCase(sCombo.List(i)) = UCase(aItem.SubItems(aCol)) Then Exit For
    Next
    '-> si on a pas trouv� l'entr�e
    If i > sCombo.ListCount Then
        sCombo.AddItem aItem.SubItems(aCol)
    End If
Next

'-> on ajoute l'option tous
sCombo.AddItem " (tous)", 0


sCombo.Text = sCombo.List(0)

End Sub

Private Sub Check1_Click()

If Me.ListView1.SelectedItem Is Nothing Then Exit Sub
Call ListView1_ItemClick(Me.ListView1.SelectedItem)

End Sub

Private Sub Check2_Click()
'-> adresse du serveur SMTP
SetIniString "PARAM", "DSOFILE", TurboGraphIniFile, Me.Check2.Value
End Sub

Private Sub Combo1_Click()
'-> on filtre les fichiers
FiltreListView

End Sub

Private Sub Combo2_Click()
'-> on filtre les fichiers
FiltreListView

End Sub

Private Sub Combo3_Click()
'-> on filtre les fichiers
FiltreListView

End Sub

Private Sub Combo4_Click()
'-> on filtre les fichiers
FiltreListView

End Sub

Private Sub Combo5_Click()
'-> on filtre les fichiers
FiltreListView

End Sub

Private Sub FileSetProperties(strFichier As String, WFD As WIN32_FIND_DATA, hFile As Long)
'-> cette procedure permet de reaffecter aux spool leurs propri�t�s
Dim strFichierSave As String
Dim strProperties As String
Dim aItem As ListItem
Dim i As Integer
'Dim strFileName As Object
Dim ficCible As Long
Dim RetVal As Long
Dim L1, L2, L3, L4, L5, L6 As Long
Dim Ft1 As FILETIME, Ft2 As FILETIME, SysTime As SYSTEMTIME, ft3 As FILETIME

'-> gestion des erreurs
On Error Resume Next
If Rnd < 0.05 Then
    frmWait.Label1.Caption = CInt(countFile / countFileT * 100) & "%   " & countFile & " fichiers analys�(s)" & Chr(13) & "[Echap] pour quitter l'analyse des propri�t�s"
    DoEvents
End If

countFile = countFile + 1

'-> on utilise la dll dsofile.dll
strFichierSave = strFichier

'-> on s'occupe maintenant des propri�t�s
DSO.Open strFichier
'-> on pointe sur les propri�t�s
Set DSOprop = DSO.SummaryProperties
'-> si aucune version n'est affect�e
If DSOprop.Title = "" Then
    '-> on se met a gauche les infos sur les dates
    L1 = WFD.ftCreationTime.dwHighDateTime
    L2 = WFD.ftCreationTime.dwLowDateTime
    L3 = WFD.ftLastAccessTime.dwHighDateTime
    L4 = WFD.ftLastAccessTime.dwLowDateTime
    L5 = WFD.ftLastWriteTime.dwHighDateTime
    L6 = WFD.ftLastWriteTime.dwLowDateTime
    strFichier = GetUnzipFileName(strFichier)
    strProperties = GetSpoolProperties(strFichier)
    If strProperties <> "�����" Then
        If Trim(DSOprop.Author) = "" Then DSOprop.Author = Entry(6, strProperties, "�") 'CLI
        '-> si on a ramen� quelque chose
        If DSOprop.Author <> "" Then
            If DSOprop.Category = "" Then DSOprop.Category = Entry(4, strProperties, "�") 'prog
            If DSOprop.Comments = "" Then DSOprop.Comments = Entry(2, strProperties, "�") 'nom
            If DSOprop.Keywords = "" Then DSOprop.Keywords = Entry(5, strProperties, "�") 'rub
            If DSOprop.Subject = "" Then DSOprop.Subject = Entry(1, strProperties, "�")   'fieldlist
            If DSOprop.Title = "" Then DSOprop.Title = Entry(3, strProperties, "�")
            If DSOprop.company = "" Then DSOprop.company = "Deal Informatique"
        End If
    End If
    If strFichier <> strFichierSave Then DSOprop.manager = "zip" + uZipInfo2
    If DSOprop.manager = "" Then DSOprop.manager = App.Title
    If DSOprop.Title = "" Then DSOprop.Title = " "
    '-> on sauvegarde les infos
    DSO.Save
    '-> on redonne la date d'origine au fichier
    WFD.ftCreationTime.dwHighDateTime = L1
    WFD.ftCreationTime.dwLowDateTime = L2
    WFD.ftLastAccessTime.dwHighDateTime = L3
    WFD.ftLastAccessTime.dwLowDateTime = L4
    WFD.ftLastWriteTime.dwHighDateTime = L5
    WFD.ftLastWriteTime.dwLowDateTime = L6
    ficCible = CreateFile(strFichier, &H40000000, &H1 Or &H2, ByVal 0&, 3, 0, 0)
    RetVal = SetFileTime(ficCible, WFD.ftCreationTime, WFD.ftLastAccessTime, WFD.ftLastWriteTime)
    CloseHandle ficCible
End If

'-> maintenant la partie listview
'-> on regarde si le fichier est zipp�
If InStr(1, DSOprop.manager, "zip") <> 0 Then
    If DSOprop.manager = "zip" Then
        Me.ListView1.ListItems(strFichierSave).SmallIcon = 1
    Else
        Me.ListView1.ListItems(strFichierSave).SmallIcon = 2
    End If
End If

'-> on pointe sur les propri�t�s
Me.ListView1.ListItems(strFichierSave).ListSubItems.add , , DSOprop.Author
Me.ListView1.ListItems(strFichierSave).ListSubItems.add , , DSOprop.Category
Me.ListView1.ListItems(strFichierSave).ListSubItems.add , , DSOprop.Comments
Me.ListView1.ListItems(strFichierSave).ListSubItems.add , , DSOprop.Keywords
Me.ListView1.ListItems(strFichierSave).ListSubItems.add , , DSOprop.Subject
Me.ListView1.ListItems(strFichierSave).ListSubItems.add , , DSOprop.Title

GestError:

'Call CloseHandle(hFile)
Set aItem = Nothing
Set DSOprop = Nothing
DSO.Close
End Sub

Private Function FileFindText(sFile As String, sText As String) As Boolean
'--> cette proc�dure recherche du texte dans un spool
Dim hdlFile As Integer
Dim Ligne As String

hdlFile = FreeFile
'-> on ouvre le fichier en mode read
Open sFile For Input As #hdlFile
'-> on parcourt les lignes pour y rechercher des propri�t�s
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    If InStr(1, UCase(Ligne), UCase(sText), vbTextCompare) <> 0 Then
        FileFindText = True
        Exit Do
    End If
Loop

Close #hdlFile

End Function

Private Sub FileSaveProperties()
'--> cette proc�dure permet d'enregistrer les modifications des propri�t�s d'un fichier
Dim aItem As ListItem
On Error Resume Next

Set DSO = CreateObject("DSOFile.OleDocumentProperties")
DSO.Open ListView1.SelectedItem.Key
'-> on pointe sur les propri�t�s
Set DSOprop = DSO.SummaryProperties
DSOprop.Author = Me.Text3.Text
DSOprop.Category = Me.Text4.Text
DSOprop.Comments = Me.Text6.Text
DSOprop.Keywords = Me.Text5.Text
DSOprop.Subject = Me.Text2.Text
DSOprop.Title = Me.Text1.Text
DSO.Save
DSO.Close
Set DSOprop = Nothing

'-> on met a jour le listview
'-> on pointe sur le fichier
Set aItem = Me.ListView1.SelectedItem
aItem.SubItems(3) = Me.Text3.Text
aItem.SubItems(4) = Me.Text4.Text
aItem.SubItems(5) = Me.Text6.Text
aItem.SubItems(6) = Me.Text5.Text
aItem.SubItems(7) = Me.Text2.Text
aItem.SubItems(8) = Me.Text1.Text

End Sub

Private Function GetSpoolProperties(sFile As String) As String
'--> cette fonction ram�ne la propri�t� donn�e d'un fichier turbo
Dim hdlFile As Integer
Dim Ligne As String
Dim i As Integer

hdlFile = FreeFile
'-> on ouvre le fichier en mode read
Open sFile For Input As #hdlFile
'-> on parcourt les lignes pour y rechercher des propri�t�s
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
            i = i + 1
            If i > 200 Then Exit Do
            If InStr(1, Ligne, "\FieldListe�") <> 0 Then
                GetSpoolProperties = GetSpoolProperties & Replace(Entry(2, Ligne, "�"), ".drf", "", , , vbTextCompare)
                GoTo suite
            End If
            If InStr(1, Ligne, "\Nom�") <> 0 Then
                GetSpoolProperties = GetSpoolProperties & "�" & Entry(2, Ligne, "�")
                GoTo suite
            End If
            If InStr(1, Ligne, "\Description�") <> 0 Then
                GetSpoolProperties = GetSpoolProperties & "�" & Entry(2, Ligne, "�")
                GoTo suite
            End If
            If InStr(1, Ligne, "\Prog=") <> 0 Then
                GetSpoolProperties = GetSpoolProperties & "�" & Entry(2, Ligne, "=")
                GoTo suite
            End If
            If InStr(1, Ligne, "\Rub=") <> 0 Then
                GetSpoolProperties = GetSpoolProperties & "�" & Entry(2, Ligne, "=")
                GoTo suite
            End If
            If InStr(1, Ligne, "\Cli=") <> 0 Then
                GetSpoolProperties = GetSpoolProperties & "�" & Entry(2, Ligne, "=")
                Exit Do
            End If
suite:
Loop
GetSpoolProperties = GetSpoolProperties & "�����"
'-> on ferme le fichier
Close #hdlFile

End Function


Private Sub Command1_Click()
FilePreview curPage + 1
End Sub

Private Sub Command2_Click()
'--> on eaffiche les propri�t�s
Call ListView1_ItemClick(Me.ListView1.SelectedItem)

End Sub

Private Sub Command3_Click()
'-> on enregistre les propri�t�s
FileSaveProperties

End Sub

Private Sub FileChangeDirectory()
Dim strPath As String
'-> on lance le changement de repertoire
strPath = BrowseForFolder("Selectionnez le r�pertoire contenant les spools d'�dition", GetPath(spath), spath)
'-> si on a selectionne un repertoire
If strPath <> "" Then
    '-> on definit le nouveau repertoire
    spath = strPath & "\"
    '-> on lance l'affectation des propri�t�s
    Call FilesSetProperties(spath, "*.*")
    
End If

'-> on met � jour l'�cran
Me.Label14.Caption = spath

If VersionTurbo = 1 Then
    '-> on enregistre le path en version internet
    SetIniString "PARAM", "SAVE", App.Path & "\Turbograph.ini", spath
    TurbosavePath = spath
End If

End Sub

Private Sub Command4_Click()
If curPage > 1 Then FilePreview curPage - 1
End Sub

Private Sub Command5_Click()
FilesDeleteProperties spath, "*.*"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
Dim aItem As ListItem

Select Case KeyCode
    Case vbKeyF5
        '-> on recharge le filtre
        Call FilesSetProperties(spath, "*.*")
    Case vbKeyF2
        '-> renommer le fichier
        If Not Me.ListView1.SelectedItem Is Nothing Then Call ListView1.StartLabelEdit
    Case vbKeyP 'fenetre impression
        If Shift = 2 Then
            FilePrint
        End If
    Case vbKeyS 'fenetre save
        If Shift = 2 Then
            SaveSpool
        End If
    Case vbKeyEscape
        '-> on pose le top
        TopStop = True
    Case vbKeyA
        If Shift = 2 Then
            For Each aItem In Me.ListView1.ListItems
                aItem.Selected = True
            Next
        End If
End Select

End Sub

Private Sub FilePrint()
'--> cette procedure permet d'imprimer un fichier
Dim strFichier As String
Dim aFichier As Fichier
Dim aSpool As Spool
Dim MinPage As Integer
Dim DeviceName As String
Dim ChoixPage As String
Dim PageMin As Integer
Dim PageMax As Integer
Dim NbCopies As Integer
Dim RectoVerso As String
Dim Assembl As String
Dim NoGard As String
Dim FileToPrint As String

'->Vider la variable de retour
strRetour = ""

'-> Setting des param�trages sur le fichier et le spool en cours
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

strFichier = GetUnzipFileName(Me.ListView1.SelectedItem.Key)
Call AnalyseFileToPrint(strFichier)

'-> Pointer sur l'objet fichier
Set aFichier = Fichiers(UCase$(strFichier))
Set aSpool = aFichier.Spools(1)

frmPrint.FichierName = aSpool.fileName
frmPrint.SpoolKey = aSpool.Key
frmPrint.IsSelectionPage = aSpool.IsSelectionPage

'-> afficher ou pas la zone d'impression de la globalit� des spools
If Fichiers(UCase$(aSpool.fileName)).Spools.Count = 1 Then
    frmPrint.Option4.Visible = False
End If

If aSpool.NbPage = 0 And Fichiers(UCase$(aSpool.fileName)).Spools.Count = 1 Then
    MsgBox "Ce fichier ne contient pas un spool avec des pages � imprimer", vbInformation, "Imprimer un fichier"
    Exit Sub
End If

'-> Afficher le choix de l'imprimante
frmPrint.Show vbModal

'-> Quitter si annuler
If strRetour = "" Then Exit Sub

'-> Traiter les choix d'impression
DeviceName = Entry(1, strRetour, "|")
ChoixPage = Entry(2, strRetour, "|")
NbCopies = Entry(3, strRetour, "|")
RectoVerso = Entry(4, strRetour, "|")
If copyAssemb Then Assembl = Entry(5, strRetour, "|")
If noGarde Then NoGard = Entry(6, strRetour, "|")

'-> Analyse du choix d'impression
Select Case Entry(1, ChoixPage, "�")
    Case 1 '-> Fichier en entier
        '-> Construire le nom du fichier
        'FileToPrint = CreateDetail(Me.aSpool, -1)
        FileToPrint = aSpool.fileName
    Case 2 '-> Page mini � page maxi
        '-> R�cup�rer les pages � imprimer
        PageMin = CInt(Entry(2, ChoixPage, "�"))
        PageMax = CInt(Entry(3, ChoixPage, "�"))
        
        '-> V�rifier la page min
        If PageMin < 1 Then
            '-> V�rifier si le fichier poss�de une page de s�lection
            If aSpool.IsSelectionPage Then
                PageMin = 0
            Else
                PageMin = 1
            End If
        End If
        
        '-> V�rifier la page maxi
        If PageMax > aSpool.NbPage Then PageMax = aSpool.NbPage
            
        '-> V�rifier que la page mini est < � la page maxi
        If PageMin > PageMax Then PageMin = PageMax
            
        '-> Construire le nom du fichier
        FileToPrint = CreateDetail(aSpool, -2, PageMin, PageMax)
                    
    Case 3  '-> Page en cours
        '-> Construire le nom du fichier
        FileToPrint = CreateDetail(aSpool, aSpool.CurrentPage)
    Case 4 '-> Spool en cours
        'FileToPrint = aSpool.FileName
        FileToPrint = CreateDetail(aSpool, -1)
End Select

'Debug.Print DeviceName & "~DIRECT~" & NbCopies & "|" & FileToPrint

'-> Tester le cas de l'impression PDF

' *** PIERROT : tjrs pour gerer le soft WIN2PDF
If UCase$(Trim(DeviceName)) = "WIN2PDF" Then
    '-> Lancer l'�dition
    Shell App.Path & "\TurboGraph.exe " & "WIN2PDF" & "~DIRECT~" & NbCopies & "|" & FileToPrint & "**0", vbNormalFocus
    Exit Sub
End If
 
If UCase$(Trim(DeviceName)) = "ACROBAT PDFWRITER" Then
    '-> Lancer l'�dition
    Shell App.Path & "\TurboGraph.exe " & "ACROBAT PDFWRITER" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso & "||" & NoGard, vbNormalFocus
    Exit Sub
ElseIf UCase$(Trim(DeviceName)) = "ACROBAT DISTILLER" Then
    Shell App.Path & "\TurboGraph.exe " & "ACROBAT DISTILLER" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso & "||" & NoGard, vbNormalFocus
    Exit Sub
ElseIf UCase$(Trim(DeviceName)) = "ADOBE PDF" Then
    Shell App.Path & "\TurboGraph.exe " & "ADOBE PDF" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso & "||" & NoGard, vbNormalFocus
    Exit Sub
ElseIf UCase$(Trim(DeviceName)) = "PDFCREATOR" Then
    '-> on sort la sauvegarde automatique
    Shell App.Path & "\TurboGraph.exe " & "PDFCREATOR" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso & "||" & NoGard, vbNormalFocus
    Exit Sub
Else
    '-> Lancer l'impression
    Shell App.Path & "\TurboGraph.exe " & DeviceName & "~DIRECT~" & NbCopies & "|" & FileToPrint & "|" & RectoVerso & "|" & Assembl & "|" & NoGard, vbNormalFocus
End If

End Sub

Private Sub Form_Load()

'-> on charge les libell�s
InitLabel
'-> on charge la liste des fichiers
Initialize


End Sub

Private Sub Form_Resize()
'--> on lance le redessin de la feuille
formResize

End Sub

Private Sub formResize()
'--> cette procedure repositionne les objets de la feuille
On Error Resume Next
If Me.Height < 6000 Then Exit Sub
Me.SSTab1.Top = Me.Height - Me.SSTab1.Height - 550
Me.ListView1.Height = Me.SSTab1.Top - 950
Me.ListView1.Width = DepartZ * Me.Width - 250
Me.Picture3.Width = Me.ListView1.Width
Me.Label14.Width = Me.Picture3.Width - 200
If (1 - DepartZ) * Me.Width - 250 < 0 Then
    Me.Picture2.Width = 0
Else
    Me.Picture2.Width = (1 - DepartZ) * Me.Width - 250
End If
Me.Picture2.Left = Me.Width - Me.Picture2.Width - 250
Me.Picture2.Height = Me.ListView1.Height - 150
Me.Check1.Top = Me.Picture2.Top + Me.Picture2.Height + 250
Me.Check2.Top = Me.Check1.Top
Me.Check1.Left = Me.Picture2.Left + 200
Me.Image2.Left = Me.Picture2.Left - 150
Me.Image2.Height = Me.Picture2.Height
Me.Label14.Caption = spath
Me.Command1.Top = Me.Check1.Top - 100
Me.Command4.Left = Me.Check1.Left + Me.Check1.Width + 100
Me.Command4.Top = Me.Command1.Top
Me.Command1.Left = Me.Command4.Left + 480

'-> on affiche le preview
'-> selon l'orientation
If Image1.Height = 15 Then Exit Sub
Image1.Width = Picture2.Width
Image1.Height = Picture2.Height
Image1.Top = 0
If Sortie.Height / Sortie.Width < Me.Picture2.Height / Me.Picture2.Width Then
    If Sortie.Height > Sortie.Width Then
        '-> portrait
        Image1.Height = Image1.Width * Sortie.Height / Sortie.Width
    Else
        '-> paysage
        Image1.Height = Image1.Width * Sortie.Height / Sortie.Width
    End If
Else
    If Sortie.Height > Sortie.Width Then
        '-> portrait
        Image1.Width = Image1.Height * Sortie.Width / Sortie.Height
    Else
        '-> paysage
        Image1.Width = Image1.Height * Sortie.Width / Sortie.Height
    End If
End If

Image1 = Sortie.Image
'LockWindowUpdate 0
End Sub

Private Sub Form_Unload(Cancel As Integer)

Set DSO = Nothing
Set aColItem = Nothing

End Sub

Private Sub Image1_dblClick()

'-> on redimensionne l'objet
If Not Me.Image1.Stretch Then
    Me.Image1.Width = Me.Picture2.Width
    Me.Image1.Height = Me.Picture2.Height
    Me.Image1.Top = 0
    Me.Image1.Left = 0
    Me.Image1.Stretch = True
    formResize
Else
    Me.Image1.Stretch = False
End If

End Sub

Private Sub Image1_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
'-> on initialises les variables de placement d'image
DepartX = x
DepartY = Y

End Sub

Private Sub Image1_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
'-> on deplace eventuellement la feuille
If Button = 1 Then
    Me.Image1.MousePointer = 5
    '-> on verifie pour toujours laisser une bonne partie de visible
    If Me.Image1.Top + (Y - DepartY) <= 0 And Me.Image1.Top + (Y - DepartY) >= -15000 Then
        Me.Image1.Top = Me.Image1.Top + (Y - DepartY)
        'Me.VScroll1.Value = Me.Image1.Top
    End If
    If Me.Image1.Left + (x - DepartX) <= 0 And Me.Image1.Left + (x - DepartX) >= -15000 Then
        Me.Image1.Left = Me.Image1.Left + (x - DepartX)
        'Me.HScroll1.Value = Me.Image1.Left
    End If
End If

End Sub

Private Sub Image1_MouseUp(Button As Integer, Shift As Integer, x As Single, Y As Single)
    '-> on remet la souris
    Me.Image1.MousePointer = 99

End Sub

Private Sub Image2_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
'-> on initialises les variables de placement d'image
DepartX = x
DepartY = Y

End Sub

Private Sub Image2_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
'-> on deplace eventuellement l'image avec le listview et le picturebox
If Button = 1 Then
    Me.MousePointer = 9
    DepartZ = (Me.ListView1.Width + (x - DepartX)) / Me.Width
    If DepartZ < 0.2 Then Exit Sub
    If DepartZ > 0.99 Then
        DepartZ = 0.99
        Exit Sub
    End If
    formResize
End If

End Sub

Private Sub Image2_MouseUp(Button As Integer, Shift As Integer, x As Single, Y As Single)
Me.MousePointer = 0
End Sub

Private Sub Label14_Click()
'-> on change le repertoire
FileChangeDirectory
End Sub

Private Sub ListView1_AfterLabelEdit(Cancel As Integer, NewString As String)
'--> on essai de renommer le fichier
Dim strFichier As String
Dim aItem As ListItem
Dim i As Integer
Dim ozip As New CGUnzipFiles

On Error GoTo GestError

strFichier = Me.ListView1.SelectedItem.Key
'-> on verifie que le fichier n'existe pas deja
If Dir(spath & NewString) <> "" Then
    If MsgBox("Le fichier '" & NewString & "' existe d�j�" & Chr(13) & Chr(13) & "Souhaitez vous l'�craser?", vbOKCancel, "Renommer un fichier") = vbCancel Then
        '-> on annule la modification
        Cancel = True
        '-> on butte l'entr�e du listview
        Me.ListView1.ListItems.Remove NewString
        Exit Sub
    End If
End If

'-> on butte l'entr�e du listview
Me.ListView1.ListItems.Remove Me.ListView1.SelectedItem.Key
'-> on copie le fichier
FileCopy strFichier, spath & NewString
'-> on butte le fichier d'origine
Kill strFichier
'-> on recr�e l'entree avec les propri�t�s
strFichier = NewString

FileAddInListview spath & strFichier

Exit Sub
GestError:
MsgBox Err.Description, vbCritical, "Erreur"

End Sub

Private Sub ListView1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
'-> Trier sur les entetes de colonne
ColumnOrder Me.ListView1, ColumnHeader

End Sub

Private Sub ListView1_DblClick()
'-> on ouvre le fichier
FileOpen

End Sub

Private Sub ListView1_ItemClick(ByVal Item As MSComctlLib.ListItem)
Dim aFichier As Fichier
Dim aSpool As Spool
Dim DSO, DSOprop
Dim strFichier As String

On Error Resume Next

Set DSO = CreateObject("DSOFile.OleDocumentProperties")
Me.Caption = "Fichier s�l�ctionn� : " & Item.Key & "        (" & Me.ListView1.ListItems.Count & ") fichiers trouv�s"
DSO.Open Item.Key

'-> on pointe sur les propri�t�s
Set DSOprop = DSO.SummaryProperties
Me.Text3.Text = DSOprop.Author
Me.Text4.Text = DSOprop.Category
Me.Text6.Text = DSOprop.Comments
Me.Text5.Text = DSOprop.Keywords
Me.Text2.Text = DSOprop.Subject
Me.Text1.Text = DSOprop.Title
Set DSOprop = Nothing
Set DSO = Nothing

'-> si on doit pr�visualiser le spool
If Me.Check1.Value Then
    '-> Lancer l'analyse du spool
    stopSearch = False
    strFichier = GetUnzipFileName(Item.Key)
    Call AnalyseFileToPrint(strFichier)
    
    '-> Pointer sur l'objet fichier
    stopSearch = False
    Set aFichier = Fichiers(UCase$(strFichier))
    Set aSpool = aFichier.Spools(1)
    
    If aSpool.Maquette Is Nothing Then
        Me.Image1.Visible = False
        Exit Sub
    End If
    PreviewPageSpool aSpool, 1
    '-> on redonne le focus
    Me.ListView1.SetFocus
End If

'-> on redonne le focus a la feuille

End Sub

Public Sub PreviewPageSpool(ByRef aSpool As Spool, ByVal pageToPrint As Integer)

'---> Cette proc�dure imprime une page d'un spool

Dim i As Integer, j As Integer
Dim PositionX As Long
Dim PositionY As Long
Dim IsPaysage As Boolean
Dim Ligne As String
Dim TypeObjet As String
Dim NomObjet As String
Dim TypeSousObjet As String
Dim NomSousObjet As String
Dim Param As String
Dim DataFields As String
Dim FirstObj As Boolean
Dim aNode As Node
Dim ErrorCode As Integer
Dim aSection As Section
Dim x As Control
Dim MargeOld As Integer

On Error GoTo GestError

Trace "PrintPageSpool --------------------" & pageToPrint & " --------------------"
Me.Image1.Visible = True
'-> Conserver la valeur du zoom
Zoom = 1
curPage = pageToPrint

If DepartZ > 0.95 Then
    DepartZ = 0.66
    formResize
End If

'-> mettre la feuille au premier plan
SetWindowPos Me.hwnd, -1, 0, 0, 0, 0, 2 Or 1

'-> Modifier la page courrante de l'objet Spool
aSpool.CurrentPage = pageToPrint
InitFRMDISPLAY aSpool
aSpool.NbError = 0
aSpool.InitDisplayPage
'-> Positionner le pointeur de sortie vers le picturebox de la feuille
Set Sortie = aSpool.frmDisplay.Page

'-> on masque l'affichage sur la feuille principale
If Not MDIMain.ActiveForm Is Nothing Then MDIMain.ActiveForm.Visible = False

'-> on charge des infos dans le toolTip
Me.Image1.ToolTipText = "   "
Me.Image1.ToolTipText = Me.Image1.ToolTipText & "Nombre de spools : " & Fichiers(aSpool.fileName).NbSpool & "          "
Me.Image1.ToolTipText = Me.Image1.ToolTipText & "Page du spool en cours : " & Fichiers(aSpool.fileName).Spools(1).CurrentPage & "/" & Fichiers(aSpool.fileName).Spools(1).NbPage & "        "

'-> R�cup�ration des marges internes du contexte de p�riph�rique
MargeX = GetDeviceCaps(Sortie.hDC, PHYSICALOFFSETX)
MargeY = GetDeviceCaps(Sortie.hDC, PHYSICALOFFSETY)

'-> Initialiser la position X , y du pointeur sur les marges du document
PositionY = -MargeY + Sortie.ScaleY(aSpool.Maquette.MargeTop, 7, 3)
PositionX = -MargeX + Sortie.ScaleX(aSpool.Maquette.MargeLeft, 7, 3)

PositionX = PositionX * Zoom
PositionY = PositionY * Zoom

'-> Indiquer l'�tat du premier objet que l'on trouve
FirstObj = True

'-> Lecture des lignes de la page
For i = 1 To NumEntries(aSpool.GetPage(pageToPrint), Chr(0))
    '-> R�cup�ration de la ligne en cours
    Ligne = Trim(Entry(i, aSpool.GetPage(pageToPrint), Chr(0)))
    '-> Ne rien imprimer si page � blanc
    If Trim(Ligne) = "" Then GoTo NextLigne
    
    If pageToPrint = 0 Then
        frmLib.Rtf(aSection.IdRTf).Text = frmLib.Rtf(aSection.IdRTf).Text & Chr(13) & Chr(10) & Ligne
        frmLib.Rtf(aSection.IdRTf).SelStart = 0
        frmLib.Rtf(aSection.IdRTf).SelLength = Len(frmLib.Rtf(aSection.IdRTf).Text)
        frmLib.Rtf(aSection.IdRTf).SelFontName = "Lucida Console"
    Else
        If InStr(1, Ligne, "[") = 1 Then
            '-> r�cup�ration des param�tres
            AnalyseObj Ligne, TypeObjet, NomObjet, TypeSousObjet, NomSousObjet, Param, DataFields
            '-> Selon le type d'objet
            If UCase$(TypeObjet) = "ST" Then
                If Not PrintSection(NomObjet, Param, DataFields, FirstObj, PositionX, PositionY, aSpool) Then Exit For
                FirstObj = False
            ElseIf UCase$(TypeObjet) = "TB" Then
                If Not PrintTableau(NomObjet, Param, DataFields, PositionX, PositionY, NomSousObjet, FirstObj, aSpool) Then Exit For
                FirstObj = False
            End If
        End If 'Si premier caract�re = "["
    End If 'Si on imprime la page de s�lection
NextLigne:
Next 'Pour toutes les lignes de la page

'-> on affiche le preview
'-> selon l'orientation
Image1.Width = Picture2.Width
Image1.Height = Picture2.Height
Image1.Top = 0
If Sortie.Height / Sortie.Width < Me.Picture2.Height / Me.Picture2.Width Then
    If Sortie.Height > Sortie.Width Then
        '-> portrait
        Image1.Height = Image1.Width * Sortie.Height / Sortie.Width
    Else
        '-> paysage
        Image1.Height = Image1.Width * Sortie.Height / Sortie.Width
    End If
Else
    If Sortie.Height > Sortie.Width Then
        '-> portrait
        Image1.Width = Image1.Height * Sortie.Width / Sortie.Height
    Else
        '-> paysage
        Image1.Width = Image1.Height * Sortie.Width / Sortie.Height
    End If
End If
Image1 = Sortie.Image

'-> mettre la feuille au premier plan
SetWindowPos Me.hwnd, -2, 0, 0, 0, 0, 2 Or 1

Exit Sub
       
GestError:
    MsgBox Err.Number & " " & Err.Description
    
End Sub

Public Sub InitFRMDISPLAY(ByRef aSpool As Spool)

Dim aFrm As frmDisplaySpool
'-> Cr�er une nouvelle instance
Set aFrm = New frmDisplaySpool
'-> Affect� le spool
Set aFrm.aSpool = aSpool
'-> Affecter la feuille au spool
Set aSpool.frmDisplay = aFrm

End Sub

Private Sub FileDelete()
'--> cette procedure lance l'impression en fonction des parametres du zoncharg
Dim Index As Integer

On Error GoTo GestError

 '-> on supprime le fichier
If MsgBox("Supprimer les fichiers s�lectionn�s? " & Chr(13) & " ", vbOKCancel Or vbExclamation, "Supprimer des fichiers") = 1 Then
    For Index = Me.ListView1.ListItems.Count To 1 Step -1
       If Me.ListView1.ListItems(Index).Selected Then
           stopSearch = False
           Kill Me.ListView1.ListItems(Index).Key
           Me.ListView1.ListItems.Remove (Index)
       End If
    Next
End If

Me.ListView1.SetFocus
Exit Sub

GestError:
'-> gestion des erreurs
MsgBox Err.Description

End Sub

Private Sub FileOpen()
'--> cette procedure permet d'ouvrir le spool ou d'ouvrir le fichier avec l'application associ�e
'Dim lpBuffer As String
'Dim Res As Long
'Dim IniPath As String
'Dim Extension As String
'Dim ExtensionOperat As String
'Dim DefFileName As String
'Dim aLb As Libelle
Dim fileName As String
Dim lngResult As Long
Dim strBuffer As String

On Error GoTo GestError

'-> Affecter le nom
fileName = Me.ListView1.SelectedItem.Key

'-> On regarde si il y a un programme associ� different de turbo
strBuffer = Space$(260)
lngResult = FindExecutable(fileName, "", strBuffer)
If strBuffer <> "" And InStr(1, strBuffer, "turbograph", vbTextCompare) = 0 And Len(Trim(strBuffer)) > 6 Then
    '-> on lance l'application associ�e
    strBuffer = Left(strBuffer, Len(strBuffer) - Len(Dir(strBuffer)))
    lngResult = ShellExecute(Me.hwnd, "Open", fileName, vbNullString, strBuffer, 1)
    Exit Sub
End If

'-> Test surt le ZIP
FilePath = Mid(fileName, 1, InStrRev(Replace(fileName, "\", "/"), "/"))
fileName = GetUnzipFileName(fileName)

'-> Lancer le chargement du fichier
DisplayFileGUI fileName

'-> Charger eventuellement au menu les fichiers joints
LoadJoinFile uZipInfo2, fileName

'-> on redonne le focus
Me.ListView1.SetFocus
GestError:

End Sub

Public Sub ColumnOrder(aList As ListView, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
'--> cette procedure permet de trier les colonnes d'un listview
Dim aItem As ListItem
Dim aCol As ColumnHeader
Dim i As Integer
Dim j As Integer

'-> gestion des erreurs
On Error Resume Next

'-> on ajoute une colonne cachee pour le tri
aList.ColumnHeaders.add , "Cache"
'-> on la masque
aList.ColumnHeaders("Cache").Width = 0

'on vide la colonne cach�e
For Each aItem In aList.ListItems
    aItem.SubItems(aList.ColumnHeaders.Count - 1) = ""
Next
'-> si on a des ruptures et pas sur la colonne en cours on envoi les donnees dans la colonne cachee
'-> on teste l'allignement
Select Case ColumnHeader.Alignment
 Case lvwColumnRight
    '-> on met des blancs devant on est sur des chiffres
     For Each aItem In aList.ListItems
        If Not (Entry(1, aItem.Key, "|") = "Rupture") Then  'And aItem.SubItems(ColumnHeader.Index - 1) = "") Then
            aItem.SubItems(aList.ColumnHeaders.Count - 1) = Right(Space(20) & FileLen(aItem.Key), 20) 'aItem.SubItems(aList.ColumnHeaders.Count - 1) + Right(Space(20) & Str((CDbl(Replace(Replace(Replace(aItem.SubItems(ColumnHeader.Index - 1), " Bytes", ""), " MB", "000000"), " KB", "000"))) * 1000), 20)
        Else '-> on est sur une ligne de rupture
            aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + Mid("ZZZZZZZZZZZZZZZZZZ", 1, 15 - CInt(Entry(2, aItem.Key, "|")))
        End If
     Next
 Case lvwColumnLeft
        '-> on est sur une chaine on regarde si on est sur une colonne de dates
        For Each aItem In aList.ListItems
            '-> on verifie si on a que des dates!!
            If ColumnHeader.Index <> 1 Then
                If Not IsDate(aItem.SubItems(ColumnHeader.Index - 1)) Then
                     '-> on copie la chaine tel quel
                         aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + aItem.SubItems(ColumnHeader.Index - 1)
                Else
                     '-> on est sur des dates on les formate yyyymmdd
                     aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + FORMAT(aItem.SubItems(ColumnHeader.Index - 1), "yyyymmddhhmmss")
                End If
            Else '-> on est sur la premiere colonne
                If Not IsDate(aItem.Text) Then
                     '-> on copie la chaine tel quel
                     aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + aItem.Text
                Else
                     '-> on est sur des dates on les formate yyyymmdd
                     aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + FORMAT(aItem.Text, "yyyymmdd")
                End If
            End If '-> on est sur la premiere colonne ou pas
            If Entry(1, aItem.Key, "|") = "Rupture" Then 'And aItem.SubItems(ColumnHeader.Index - 1) = "") Then
                '-> on est sur une ligne de rupture
                aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + Mid("ZZZZZZZZZZZZZZZZZZ", 1, 15 - CInt(Entry(2, aItem.Key, "|")))
            End If
        Next
End Select
'-> Trier sur les entetes de colonne
aList.SortKey = aList.ColumnHeaders("Cache").Index - 1
If aList.SortOrder = lvwAscending Then
    aList.SortOrder = lvwDescending
Else
    aList.SortOrder = lvwAscending
End If
aList.Sorted = True

'-> on supprime la colonne cach�e
aList.ColumnHeaders.Remove ("Cache")

End Sub

Private Sub ListView1_KeyDown(KeyCode As Integer, Shift As Integer)

Select Case KeyCode
    Case 46 '-> suppr
        FileDelete
End Select

End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)

'-> on gere le menu popup
'-> Teste sur le bouton
If Button <> vbRightButton Then Exit Sub

'-> S�lectionner le node
Set Me.ListView1.SelectedItem = Me.ListView1.HitTest(x, Y)

'-> Quitter si pas de node
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Afficher le mennu contextuel
Me.PopupMenu Me.mnu_popup

End Sub

Private Sub mnu_delete_Click()
'-> on supprime le fichier
FileDelete

End Sub

Private Sub mnu_open_Click()
'-> on ouvre le fichier
FileOpen

End Sub

Private Sub mnu_preview_Click()
'-> on affiche l'appercu
Dim aCheck As Integer

If Me.ListView1.SelectedItem Is Nothing Then Exit Sub
LockWindowUpdate Me.hwnd
aCheck = Me.Check1.Value
Me.Check1.Value = 1
'Call ListView1_ItemClick(Me.ListView1.SelectedItem)
'-> on restaure
Me.Check1.Value = aCheck
LockWindowUpdate 0

End Sub

Private Sub mnu_rename_Click()
'-> permet de renommer un fichier
Me.ListView1.StartLabelEdit

End Sub

Private Sub Picture2_DblClick()
'-> on recadre l'image
Me.Image1.Top = 0
Me.Image1.Left = 0
End Sub

Private Sub Picture3_Click()
'-> on change le repertoire
FileChangeDirectory
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
'--> valider la zone
If KeyAscii = 13 Then NextFocus Me
End Sub

Private Sub Text10_KeyPress(KeyAscii As Integer)

'-> sur entr�e on lance la recherche
If KeyAscii = 13 Then FiltreListView

End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)
'--> valider la zone
If KeyAscii = 13 Then NextFocus Me

End Sub

Private Sub Text3_KeyPress(KeyAscii As Integer)
'--> valider la zone
If KeyAscii = 13 Then NextFocus Me

End Sub

Private Sub Text4_KeyPress(KeyAscii As Integer)
'--> valider la zone
If KeyAscii = 13 Then NextFocus Me

End Sub

Private Sub Text5_KeyPress(KeyAscii As Integer)
'--> valider la zone
If KeyAscii = 13 Then NextFocus Me

End Sub

Private Sub Text6_KeyPress(KeyAscii As Integer)
'--> valider la zone
If KeyAscii = 13 Then NextFocus Me

End Sub

Private Sub Text7_KeyPress(KeyAscii As Integer)

'-> sur entr�e on lance la recherche
If KeyAscii = 13 Then FiltreListView

End Sub

Public Sub NextFocus(aForm As Form, Optional MyControl As Control, Optional Which As String)

'--> Cette Procedure permet de passer sur la prochaine zone de libre
'    ceci en fonction du tabindex

Dim Index As Integer
Dim IndexCompare As Integer
Dim aControl As Control
Dim StrControl As String

On Error GoTo GestError

'-> on verifie que la zone en cours n'est pas obligatoire
If Not aForm.ActiveControl Is Nothing Then
    If InStr(1, aForm.ActiveControl.ToolTipText, "obligatoire", vbTextCompare) <> 0 Then
        If Trim(aForm.ActiveControl.Text) = "" Then Exit Sub
    End If
    'If TypeOf aForm.ActiveControl Is DealTextBox Then
    '    If GetValueScreen(aForm, aForm.ActiveControl) <> aForm.ActiveControl.Text Then
    '        aForm.ActiveControl.Text = GetValueScreen(aForm, aForm.ActiveControl)
    '    End If
    'End If
End If

'-> on analyse si on veut une position sp�ciale
Select Case Trim(UCase(Which))
    Case "FIRST" '-> on se met sur le premier focus de libre de la form
        '-> on essaie le tabindex = 0 sinon on envoi la sauce pour les suivant
        For Each aControl In aForm.Controls
            '-> on se positionne sur le prochain objet si c'est une zone de saisie on continue
            If TypeOf aControl Is TextBox Or TypeOf aControl Is ImageCombo Or TypeOf aControl Is ComboBox Then
                If aControl.TabIndex = 0 Then
                    If aControl.Enabled = True And aControl.Visible = True Then
                        '-> on donne le focus
                        aControl.SetFocus
                        '-> on quitte la procedure
                        Exit Sub
                    Else
                        '-> on passe sur l'objet suivant
                        Set MyControl = aControl
                        Exit For
                    End If
                End If
            End If 'on est sur une zone de saisie
        Next
        
    Case "KEEP" '-> on reste sur le controle
        If Not MyControl Is Nothing Then
            If MyControl.Visible = True And MyControl.Enabled = True Then MyControl.SetFocus
            Exit Sub
        Else
            If Not aForm.ActiveControl Is Nothing Then If aForm.ActiveControl.Enabled = True And aForm.ActiveControl.Visible = True Then aForm.ActiveControl.SetFocus
            Exit Sub
        End If
    Case "LAST" '-> on se met sur la derniere zone de libre de la form
        
End Select
'-> on recupere la valeur du prochain index
If MyControl Is Nothing Then
    Index = aForm.ActiveControl.TabIndex + 1
Else
    Index = MyControl.TabIndex + 1
End If

'-> on essaie de vider eventuellemnt le champ joint
If Not aForm.ActiveControl Is Nothing Then
    If TypeOf aForm.ActiveControl Is TextBox Then
        If Trim(aForm.ActiveControl.Text) = "" Then
            StrControl = "DICTLINK�" & Entry(2, aForm.ActiveControl.Name, "�")
            For Each aControl In aForm.Controls
                If Entry(1, aControl.Name, "�") & "�" & Entry(2, aControl.Name, "�") = StrControl Then
                    aControl.Caption = ""
                    Exit For
                End If
            Next
        End If
    End If
End If
'-> on recherche le prochain objet
Do
    '-> on charge l'index pour voir si il existe enc
        IndexCompare = Index
    '-> on parcours les objets
    For Each aControl In aForm.Controls
        '-> on se positionne sur le prochain objet si c'est une zone de saisie on continue
        If TypeOf aControl Is TextBox Or TypeOf aControl Is ImageCombo Or TypeOf aControl Is ComboBox Then
            If aControl.TabIndex = Index Then
                If aControl.Enabled = True And aControl.Visible = True Then
                    '-> si le controle est un dealtextbox vide on le charge avec un vide
                    '-> on donne le focus
                    aControl.SetFocus
                    '-> on quitte la procedure
                    Exit Sub
                Else
                    '-> on passe sur l'objet suivant
                    Index = Index + 1
                    GoTo suite
                End If
            End If
        End If 'on est sur une zone de saisie
    Next
suite:
'-> si on a pas trouve d'autres controls on quitte
Loop Until IndexCompare = Index

GestError:
End Sub

Private Sub Text8_KeyPress(KeyAscii As Integer)
'--> valider la zone
If KeyAscii = 13 Then
    If Trim(Me.Text8.Text) <> "" Then Me.Text8.Text = CtrlSaisieDate(Me.Text8.Text)
    NextFocus Me
End If
End Sub

Private Sub Text9_KeyPress(KeyAscii As Integer)
'-> sur entr�e on lance la recherche
If KeyAscii = 13 Then
    If Trim(Me.Text9.Text) <> "" Then Me.Text9.Text = CtrlSaisieDate(Me.Text9.Text)
    FiltreListView
End If
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
'--> selon le bouton
Select Case Button.Key
    Case "VALID"
        '-> on uvre le fichier
        FileOpen
    Case "SUPPR"
        '-> on suprime le/les fichiers
        FileDelete
    Case "RENAME"
        '-> permet de renommer un fichier
        Me.ListView1.StartLabelEdit
    Case "PRINT"
        FilePrint
    Case "PREVIEW"
        '-> on affiche l'appercu
        Me.Check1.Value = Button.Value
        If Me.Check1.Value = 0 Then
            DepartZ = 0.98 '2 / 3
            formResize
        Else
            Call mnu_preview_Click
        End If
    Case "SAVE"
        SaveSpool
    Case "REFRESH"
        Call FilesSetProperties(spath, "*.*")
    Case "FUSION"
        Call FileFusion
    Case "ECLAT"
        Call FileEclat
    Case "ABSTRACT"
        '-> on affiche ou pas lze resum�
        Me.Check2.Value = Button.Value
        '-> on lance un refresh
        Call FilesSetProperties(spath, "*.*")
End Select
End Sub

Public Sub SaveSpool()

'---> Cette proc�dure enregistre le spool en cours dans un fichier
Dim Rep As VbMsgBoxResult
Dim aLb As Libelle
Dim TempFile As String
Dim strTemp As String
Dim DSO, DSOprop
Dim strFichier As String
Dim strFichierSave As String
Dim strProperties As String
Dim aItem As ListItem
Dim i As Integer
Dim strFileName As Object
'Dim ficCible As Long, Ftcree As FILETIME, Ftmod As FILETIME, Ftbid As FILETIME
'Dim RetVal As Long

On Error GoTo GestError

'-> Bloquer la feuille
Me.Enabled = False
Screen.MousePointer = 11

'-> G�n�rer une erreur sur ANNULER
frmGestSpool.OpenDlg.CancelError = True
If TurbosavePath <> "" Then
    On Error Resume Next
    frmGestSpool.OpenDlg.InitDir = TurbosavePath
    If Dir(TurbosavePath) = "" Then MkDir TurbosavePath
End If
    
frmGestSpool.OpenDlg.fileName = ""
frmGestSpool.OpenDlg.DefaultExt = "Turbo"
frmGestSpool.OpenDlg.Filter = "*.Turbo"

'-> Flags d'ouverture
frmGestSpool.OpenDlg.Flags = cdlOFNPathMustExist

'-> Afficher la feuille
frmGestSpool.OpenDlg.ShowSave

If frmGestSpool.OpenDlg.fileName = "" Then GoTo GestError

'-> V�rifier que le fichier n'existe pas d�ja
If Dir$(frmGestSpool.OpenDlg.fileName) <> "" Then
    '-> Pointer sur la classe libell�
    Set aLb = Libelles("MDIMAIN")
    Rep = MsgBox(aLb.GetCaption(25), vbQuestion + vbYesNo, aLb.GetToolTip(25))
    If Rep = vbNo Then GoTo GestError
    '-> Supprimer le fichier
    Kill frmGestSpool.OpenDlg.fileName
    '-> Lib�rer le pointeur
    Set aLb = Nothing
End If

'-> Cr�er un fichier de d�tail
TempFile = Me.ListView1.SelectedItem.Key

'-> choisir le nouveau nom de fichier
strTemp = frmGestSpool.OpenDlg.fileName
If strTemp = "" Then GoTo GestError

'-> Le renommer
FileCopy TempFile, strTemp

strProperties = GetSpoolProperties(TempFile)
Set DSO = CreateObject("DSOFile.OleDocumentProperties")

DSO.Open strTemp
'-> on pointe sur les propri�t�s
Set DSOprop = DSO.SummaryProperties
'-> si aucune version n'est affect�e
DSOprop.Author = Entry(1, strProperties, "�")
DSOprop.Category = Entry(2, strProperties, "�")
DSOprop.Comments = Entry(3, strProperties, "�")
DSOprop.Keywords = Entry(4, strProperties, "�")
DSOprop.Subject = Entry(5, strProperties, "�")
DSOprop.Title = Entry(6, strProperties, "�")
DSOprop.company = "Deal Informatique"
DSOprop.manager = App.Title
'-> on sauvegarde les infos
DSO.Save
DSO.Close
Set DSOprop = Nothing
'Set DSO = Nothing

'->on recharge le listview
FiltreListView

GestError:
Set DSOprop = Nothing
DSO.Close
'Set DSO = Nothing

'-> D�bloquer la feuille
Me.Enabled = True
Screen.MousePointer = 0

End Sub

Public Function FilesAddInListview(sRoot As String, sFile As String, Optional sFiltreDirectory As String) As String
    '--> cette fonction charge le listview a partir d'un chemin root
    
 Dim WFD As WIN32_FIND_DATA
 Dim hFile As Long
 
 With fp
    .sFileRoot = QualifyPath(sRoot)      'chemin de d�part
    .sFileNameExt = sFile                'fichier (* ? autoris�
    .bRecurse = 1                             'True = recherche recursive
    .bFindOrExclude = 1                       '0=inclure, 1=exclure
 End With
  
 hFile = FindFirstFile(sRoot & "*.*", WFD)
 If hFile <> -1 Then
    Do
      'DoEvents
      'si c'est un repertoire on boucle
       If (WFD.dwFileAttributes And vbDirectory) Then
          If Asc(WFD.cFileName) <> CLng(46) Then
              If fp.bRecurse Then
                  FilesAddInListview sRoot & TrimNull(WFD.cFileName) & vbBackslash, sFile
              End If
          End If
       Else
         'doit etre un fichier..
          If MatchSpec(WFD.cFileName, fp.sFileNameExt) Then
              If sFiltreDirectory <> "" Then
                  If InStr(1, sRoot & TrimNull(WFD.cFileName), sFiltreDirectory) <> 0 Then
                      FileAddInListview (sRoot & TrimNull(WFD.cFileName))
                      'Exit Do
                  End If
              Else
                  FileAddInListview (sRoot & TrimNull(WFD.cFileName))
              End If
          End If
       End If
    Loop While FindNextFile(hFile, WFD)
 End If
Call FindClose(hFile)

'-> Formatter le ListView
LockWindowUpdate 0
'FormatListView ListView1
DoEvents
End Function

Public Function FilesSetProperties(sRoot As String, sFile As String, Optional sFiltreDirectory As String, Optional noFirst As Boolean) As String
'--> cette fonction charge le listview a partir d'un chemin root
Dim WFD As WIN32_FIND_DATA
Dim hFile As Long
Dim aItem As ListItem

'-> on gele l'ecran
LockWindowUpdate Me.ListView1.hwnd
'-> on met le sablier
Me.MousePointer = 11

'-> si c'est la premiere fois
If Not noFirst Then
    '-> on affiche le timer
    frmWait.Show
    '-> on charge le listview car c'est rapide
    Me.ListView1.ListItems.Clear
    FilesAddInListview sRoot, sFile
    countFile = 0
    countFileT = Me.ListView1.ListItems.Count
    '-> on initialise le top d'arret
    TopStop = False
End If

 With fp
    .sFileRoot = QualifyPath(sRoot)      'chemin de d�part
    .sFileNameExt = sFile                'fichier (* ? autoris�
    .bRecurse = 1                             'True = recherche recursive
    .bFindOrExclude = 1                       '0=inclure, 1=exclure
 End With
  
 hFile = FindFirstFile(sRoot & "*.*", WFD)
 If hFile <> -1 And Me.Check2.Value = "1" Then
    Do
      'si c'est un repertoire on boucle
       If (WFD.dwFileAttributes And vbDirectory) Then
          If Asc(WFD.cFileName) <> CLng(46) Then
              If fp.bRecurse Then
                  FilesSetProperties sRoot & TrimNull(WFD.cFileName) & vbBackslash, sFile, , True
              End If
          End If
       Else
         'doit etre un fichier..
          If MatchSpec(WFD.cFileName, fp.sFileNameExt) Then
              If sFiltreDirectory <> "" Then
                  If InStr(1, sRoot & TrimNull(WFD.cFileName), sFiltreDirectory) <> 0 Then
                      FileSetProperties sRoot & TrimNull(WFD.cFileName), WFD, hFile
                  End If
              Else
                  FileSetProperties sRoot & TrimNull(WFD.cFileName), WFD, hFile
              End If
          End If
       End If
       '-> quitter si on l'a demand�
       If TopStop = True Then Exit Do
    Loop While FindNextFile(hFile, WFD)
 End If
Call FindClose(hFile)
       
If Not noFirst Then
    '-> on charge les combos
    LoadCombo 8, Combo1
    LoadCombo 7, Combo2
    LoadCombo 3, Combo3
    LoadCombo 4, Combo4
    LoadCombo 6, Combo5
    '-> on recharge le listview
    FiltreListView (True)
    '-> on restore la souris
    Me.MousePointer = 0
    FormatListView Me.ListView1
    '-> tri par d�faut les dates
    'Me.ListView1.SortOrder = lvwAscending
    'ColumnOrder Me.ListView1, Me.ListView1.ColumnHeaders(2)
    '-> on masque le timer
    Unload frmWait
    
    '-> on sauvegarde ici les donn�es
    Set aColItem = New Collection
    For Each aItem In Me.ListView1.ListItems
        aColItem.add aItem, aItem.Key
    Next
    
    LockWindowUpdate 0
End If

Exit Function
GestError:
LockWindowUpdate 0

End Function

Public Function FilesSetPropertiesOld(sRoot As String, sFile As String, Optional sFiltreDirectory As String, Optional noFirst As Boolean) As String
'--> cette fonction charge le listview a partir d'un chemin root
Dim WFD As WIN32_FIND_DATA
Dim hFile As Long
Dim t1 As Long
Dim t2 As Long
Dim aItem As ListItem

'-> on gele l'ecran
LockWindowUpdate Me.hwnd
'-> on met le sablier
Me.MousePointer = 11

t1 = GetTickCount

'-> si c'est la premiere fois
If Not noFirst Then
    '-> on affiche le timer
    frmWait.Show
    Me.ListView1.ListItems.Clear
    countFile = 0
    '-> on initialise le top d'arret
    TopStop = False
End If

 With fp
    .sFileRoot = QualifyPath(sRoot)      'chemin de d�part
    .sFileNameExt = sFile                'fichier (* ? autoris�
    .bRecurse = 1                             'True = recherche recursive
    .bFindOrExclude = 1                       '0=inclure, 1=exclure
 End With
  
 hFile = FindFirstFile(sRoot & "*.*", WFD)
 If hFile <> -1 Then
    Do
      'si c'est un repertoire on boucle
       If (WFD.dwFileAttributes And vbDirectory) Then
          If Asc(WFD.cFileName) <> CLng(46) Then
              If fp.bRecurse Then
                  FilesSetProperties sRoot & TrimNull(WFD.cFileName) & vbBackslash, sFile, , True
              End If
          End If
       Else
         'doit etre un fichier..
          If MatchSpec(WFD.cFileName, fp.sFileNameExt) Then
              If sFiltreDirectory <> "" Then
                  If InStr(1, sRoot & TrimNull(WFD.cFileName), sFiltreDirectory) <> 0 Then
                      FileSetProperties sRoot & TrimNull(WFD.cFileName), WFD, hFile
                  End If
              Else
                  FileSetProperties sRoot & TrimNull(WFD.cFileName), WFD, hFile
              End If
          End If
       End If
       '-> quitter si on l'a demand�
       If TopStop = True Then Exit Do
    Loop While FindNextFile(hFile, WFD)
 End If
Call FindClose(hFile)

If Not noFirst Then
    '-> on charge les combos
    LoadCombo 8, Combo1
    LoadCombo 7, Combo2
    LoadCombo 3, Combo3
    LoadCombo 4, Combo4
    LoadCombo 6, Combo5
    '-> on recharge le listview
    FiltreListView (True)
    '-> on restore la souris
    Me.MousePointer = 0
    FormatListView Me.ListView1
    '-> on masque le timer
    Unload frmWait
    t2 = GetTickCount
    
    MsgBox Round((t2 - t1) / 1000, 2)
    '-> on sauvegarde ici les donn�es
    Set aColItem = New Collection
    For Each aItem In Me.ListView1.ListItems
        aColItem.add aItem, aItem.Key
    Next
End If


GestError:
LockWindowUpdate 0

End Function

Private Sub FileAddInListview(strFichier As String)
'--> cette proc�dure permet de charger un fichier dans le listview
Dim aItem As ListItem
Dim i As Integer

On Error Resume Next

Me.ListView1.ListItems.add , strFichier, Replace(strFichier, spath, "")

Set aItem = Me.ListView1.ListItems(strFichier)
aItem.ListSubItems.add , , FORMAT(FileDateTime(strFichier), "dd/mm/yyyy   hh:mm")
aItem.ListSubItems.add , , FormatFileSize(FileLen(strFichier), 0)

End Sub

Private Function FormatFileSize(ByVal Size As Long, FormatType As Integer) As String
'--> cette fonction permet de transformer la taille d'un fichier
Dim sRet As String
Const KB& = 1024
Const MB& = KB * KB

'FormatType = 0 Short String Format
'FormatType = 1 Long String Format
'FormatType = 2 Dual String Format

If Size < KB Then
   sRet = FORMAT(Size, "#,##0") & " Bytes"
Else
   Select Case Size \ KB
      Case Is < 10
         sRet = FORMAT(Size / KB, "0.00") & " KB"
      Case Is < 100
         sRet = FORMAT(Size / KB, "0.0") & " KB"
      Case Is < 1000
         sRet = FORMAT(Size / KB, "0") & " KB"
      Case Is < 10000
         sRet = FORMAT(Size / MB, "0.00") & " MB"
      Case Is < 100000
         sRet = FORMAT(Size / MB, "0.0") & " MB"
      Case Is < 1000000
         sRet = FORMAT(Size / MB, "0") & " MB"
      Case Is < 10000000
         sRet = FORMAT(Size / MB / KB, "0.00") & " GB"
   End Select
   
   Select Case FormatType
     Case 0 'Short
       sRet = sRet
     Case 1 'Long
       sRet = FORMAT(Size, "#,##0") & " Bytes"
     Case 2 'Dual
       sRet = sRet & " (" & FORMAT(Size, "#,##0") & " Bytes)"
   End Select
End If

FormatFileSize = sRet
End Function

Public Function CtrlSaisieDate(MyDate As String, Optional IsNoObligatoire As Boolean) As String

'--> Fonction permettant de contr�ler la saisie de la date et d'autoriser la saisie abr�g�e _
     de la date de type jjmm ou jjmmaa
Dim MyYear As String
Dim MyMonth As String
Dim ResultDate As Date

On Error GoTo GestError

'-> Si vide on va a la fin
If Trim(MyDate) = "" And IsNoObligatoire Then GoTo Compare
If Trim(MyDate) = "" And Not IsNoObligatoire Then GoTo GestError
'-> Si saisie date sous la forme jj/mm alors rajouter l'ann�e en cours
If Len(MyDate) = 5 Then
    MyYear = Year(date)
    MyDate = MyDate & "/" & MyYear
End If
If Len(MyDate) = 2 Or Len(MyDate) = 1 Then
    MyYear = Year(date)
    MyMonth = Month(date)
    MyDate = MyDate & "/" & MyMonth & "/" & MyYear
End If

'-> Si la valeur saisie est de type Date alors pas de saisie abr�g�e
If IsDate(MyDate) Then
    CtrlSaisieDate = DateValue(MyDate)
    GoTo Compare
End If

'-> Si ce n'est pas une saisie num�ric alors ressaisr
If Not IsNumeric(MyDate) Then GoTo GestError

'-> Cas de la saisie abr�g�e : On verifie que c'est un nombre � 4 ou 6 chiffres puis _
    r�cuperer l'ann�e saisie
Select Case Len(MyDate)
    Case 4      ' 4 chiffres donc ann�e = ann�e de la date du jour
        MyYear = Year(date)
        
    Case 6      ' 6 chiffres donc ann�e = ann�e compos�e :
        '-> Si >=50 alors 19XX sinon 20xx
        If Int(Mid(MyDate, 5, 2)) <= 49 Then
            MyYear = 2000 + Int(Mid(MyDate, 5, 2))
        Else
            MyYear = 1900 + Int(Mid(MyDate, 5, 2))
        End If
    Case 8 ' 8 chiffres donc ddmmyyyy date comprise entre 1900 et 2100
        MyYear = Mid(MyDate, 5, 8)
        If Val(MyYear) > 1900 And Val(MyYear) > 2100 Then MyYear = ""
    Case Else
        GoTo GestError
End Select

MyDate = Mid(MyDate, 1, 2) & "/" & Mid(MyDate, 3, 2) & "/" & MyYear

If IsDate(MyDate) Then
    CtrlSaisieDate = CDate(MyDate)
Else
    GoTo GestError
End If

Compare:

If Trim(MyDate) <> "" Then CtrlSaisieDate = DateValue(MyDate)

Exit Function

GestError:
    'MESSPROG : ressaisir
    MsgBox "Saisie incorrecte !", vbOKOnly, "Erreur de saisie..."
    strRetour = "0"
    '-> On reinitilise
    CtrlSaisieDate = ""
    
    
End Function

Private Sub FilePreview(pageToPrint)
'-> Lancer l'analyse du spool
Dim strFichier As String
Dim aFichier As Fichier
Dim aSpool As Spool

On Error Resume Next

stopSearch = False
strFichier = GetUnzipFileName(Me.ListView1.SelectedItem.Key)
Call AnalyseFileToPrint(strFichier)

'-> Pointer sur l'objet fichier
stopSearch = False
Set aFichier = Fichiers(UCase$(strFichier))
Set aSpool = aFichier.Spools(1)

If aSpool.Maquette Is Nothing Then
    Me.Image1.Visible = False
    Exit Sub
End If

If pageToPrint > aSpool.NbPage Then Exit Sub

PreviewPageSpool aSpool, pageToPrint
'-> on redonne le focus
Me.ListView1.SetFocus

End Sub

Private Sub FileFusion()
'--> cette proc�dure per met de concatener des spools
'--> cette proc�dure recherche du texte dans un spool
Dim hdlFile As Integer
Dim hdlFile2 As Integer
Dim Ligne As String
Dim sFile As String
Dim aIem As ListItem
Dim Rep As VbMsgBoxResult
Dim aLb As Libelle
Dim TempFile As String
Dim strTemp As String
Dim DSO, DSOprop
Dim strFichier As String
Dim strFichierSave As String
Dim strProperties As String
Dim aItem As ListItem
Dim i As Integer
Dim strFileName As Object

'-------------> on determine le nom du nouveau spool
On Error GoTo GestError

'-> Bloquer la feuille
Me.Enabled = False
Screen.MousePointer = 11

'-> G�n�rer une erreur sur ANNULER
frmGestSpool.OpenDlg.CancelError = True
If TurbosavePath <> "" Then
    On Error Resume Next
    frmGestSpool.OpenDlg.InitDir = TurbosavePath
    If Dir(TurbosavePath) = "" Then MkDir TurbosavePath
End If
    
frmGestSpool.OpenDlg.fileName = ""
frmGestSpool.OpenDlg.DefaultExt = "Turbo"
frmGestSpool.OpenDlg.Filter = "*.Turbo"

'-> Flags d'ouverture
frmGestSpool.OpenDlg.Flags = cdlOFNPathMustExist

'-> Afficher la feuille
frmGestSpool.OpenDlg.ShowSave

If frmGestSpool.OpenDlg.fileName = "" Then GoTo GestError

'-> V�rifier que le fichier n'existe pas d�ja
If Dir$(frmGestSpool.OpenDlg.fileName) <> "" Then
    '-> Pointer sur la classe libell�
    Set aLb = Libelles("MDIMAIN")
    Rep = MsgBox(aLb.GetCaption(25), vbQuestion + vbYesNo, aLb.GetToolTip(25))
    If Rep = vbNo Then GoTo GestError
    '-> Supprimer le fichier
    Kill frmGestSpool.OpenDlg.fileName
    '-> Lib�rer le pointeur
    Set aLb = Nothing
End If

'-> choisir le nouveau nom de fichier
strTemp = frmGestSpool.OpenDlg.fileName
If strTemp = "" Then GoTo GestError
'-> on ouvre le fichier � creer en mode write
hdlFile2 = FreeFile
Open strTemp For Output As #hdlFile2

'----------> procedure pour fusionner les documents
For Each aItem In Me.ListView1.ListItems
    If aItem.Selected Then
        hdlFile = FreeFile
        '-> on ouvre le fichier en mode read
        sFile = aItem.Key
        strFichier = GetUnzipFileName(aItem.Key)
        Open strFichier For Input As #hdlFile
        '-> on parcourt les lignes pour y rechercher des propri�t�s
        Do While Not EOF(hdlFile)
            '-> Lecture de la ligne
            Line Input #hdlFile, Ligne
            '-> on ecrit la ligne
            Print #hdlFile2, Ligne
        Loop
        '-> on ferme le fichier
        Close #hdlFile
    End If
Next
'-> on ferme le fichier
Close #hdlFile2

'-----------> procedure pour conserver les propri�t�s
strProperties = GetSpoolProperties(sFile)
Set DSO = CreateObject("DSOFile.OleDocumentProperties")

DSO.Open strTemp
'-> on pointe sur les propri�t�s
Set DSOprop = DSO.SummaryProperties
'-> si aucune version n'est affect�e
DSOprop.Author = Entry(1, strProperties, "�")
DSOprop.Category = Entry(2, strProperties, "�")
DSOprop.Comments = Entry(3, strProperties, "�")
DSOprop.Keywords = Entry(4, strProperties, "�")
DSOprop.Subject = Entry(5, strProperties, "�")
DSOprop.Title = Entry(6, strProperties, "�")
DSOprop.company = "Deal Informatique"
DSOprop.manager = App.Title
'-> on sauvegarde les infos
DSO.Save
DSO.Close
Set DSOprop = Nothing
'Set DSO = Nothing

'->on recharge le listview
FilesSetProperties spath, "*.*"


GestError:
Set DSOprop = Nothing
DSO.Close
'Set DSO = Nothing

'-> D�bloquer la feuille
Me.Enabled = True
Me.SetFocus
Screen.MousePointer = 0

End Sub

Private Sub FileEclat()
'--> cette proc�dure permet d'eclater un spool en differents spools
Dim hdlFile As Integer
Dim hdlFile2 As Integer
Dim Ligne As String
Dim sFile As String
Dim aIem As ListItem
Dim Rep As VbMsgBoxResult
Dim aLb As Libelle
Dim TempFile As String
Dim strTemp As String
Dim DSO, DSOprop
Dim strFichier As String
Dim strFichierSave As String
Dim strProperties As String
Dim aItem As ListItem
Dim i As Integer
Dim strFileName As Object

'-------------> on determine le nom du nouveau spool
On Error GoTo GestError

'-> Bloquer la feuille
Me.Enabled = False
Screen.MousePointer = 11

'-> G�n�rer une erreur sur ANNULER
frmGestSpool.OpenDlg.CancelError = True
If TurbosavePath <> "" Then
    On Error Resume Next
    frmGestSpool.OpenDlg.InitDir = TurbosavePath
    If Dir(TurbosavePath) = "" Then MkDir TurbosavePath
End If
    
frmGestSpool.OpenDlg.fileName = ""
frmGestSpool.OpenDlg.DefaultExt = "Turbo"
frmGestSpool.OpenDlg.Filter = "*.Turbo"

'-> Flags d'ouverture
frmGestSpool.OpenDlg.Flags = cdlOFNPathMustExist

'-> Afficher la feuille
frmGestSpool.OpenDlg.ShowSave

If frmGestSpool.OpenDlg.fileName = "" Then GoTo GestError

'-> V�rifier que le fichier n'existe pas d�ja
If Dir$(frmGestSpool.OpenDlg.fileName) <> "" Then
    '-> Pointer sur la classe libell�
    Set aLb = Libelles("MDIMAIN")
    Rep = MsgBox(aLb.GetCaption(25), vbQuestion + vbYesNo, aLb.GetToolTip(25))
    If Rep = vbNo Then GoTo GestError
    '-> Supprimer le fichier
    Kill frmGestSpool.OpenDlg.fileName
    '-> Lib�rer le pointeur
    Set aLb = Nothing
End If

'-> choisir le nouveau nom de fichier
strTemp = frmGestSpool.OpenDlg.fileName
If strTemp = "" Then GoTo GestError

'----------> procedure pour fusionner les documents
hdlFile = FreeFile
'-> on ouvre le fichier en mode read
strFichier = GetUnzipFileName(Me.ListView1.SelectedItem.Key)
sFile = strFichier
Open sFile For Input As #hdlFile
'-> on parcourt les lignes pour y rechercher des propri�t�s
i = 1
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    If Trim(UCase(Ligne)) = "[SPOOL]" Then
        '-> on ferme le fichier
        Close #hdlFile2
        '-> on ouvre le fichier � creer en mode write
        hdlFile2 = FreeFile
        '-> on verifie que le fichier � cr��r n'existe pas d�j�
        Do While Dir(strTemp & "_" & i) <> ""
            i = i + 1
        Loop
        Open strTemp & "_" & i For Output As #hdlFile2
        i = i + 1
    End If
    '-> on ecrit la ligne
    Print #hdlFile2, Ligne
Loop
'-> on ferme le fichier
Close #hdlFile
'-> on ferme le fichier
Close #hdlFile2

'-----------> procedure pour conserver les propri�t�s
strProperties = GetSpoolProperties(sFile)
Set DSO = CreateObject("DSOFile.OleDocumentProperties")

DSO.Open Me.ListView1.SelectedItem.Key
'-> on pointe sur les propri�t�s
Set DSOprop = DSO.SummaryProperties
'-> si aucune version n'est affect�e
DSOprop.Author = Entry(1, strProperties, "�")
DSOprop.Category = Entry(2, strProperties, "�")
DSOprop.Comments = Entry(3, strProperties, "�")
DSOprop.Keywords = Entry(4, strProperties, "�")
DSOprop.Subject = Entry(5, strProperties, "�")
DSOprop.Title = Entry(6, strProperties, "�")
DSOprop.company = "Deal Informatique"
DSOprop.manager = App.Title
'-> on sauvegarde les infos
DSO.Save
DSO.Close
Set DSOprop = Nothing
'Set DSO = Nothing

'->on recharge le listview
FilesSetProperties spath, "*.*"

GestError:
Set DSOprop = Nothing
DSO.Close
'Set DSO = Nothing

'-> D�bloquer la feuille
Me.Enabled = True
Me.SetFocus
Screen.MousePointer = 0

End Sub

Private Sub InitLabel()
'--> on charge les libell�s
Dim aLb As Libelle

On Error Resume Next

'-> Pointer sur la classe libell�
Set aLb = Libelles("FRMGESTSPOOL")

'-> Titre de la feuille
Me.Label7.Caption = aLb.GetCaption(1)
Me.Label8.Caption = aLb.GetCaption(2)
Me.Label9.Caption = aLb.GetCaption(3)
Me.Label10.Caption = aLb.GetCaption(4)
Me.Label11.Caption = aLb.GetCaption(5)
Me.Label12.Caption = aLb.GetCaption(6)
Me.Label13.Caption = aLb.GetCaption(7)
Me.Label15.Caption = aLb.GetCaption(45)
Me.SSTab1.TabCaption(0) = aLb.GetCaption(8)
Me.SSTab1.TabCaption(1) = aLb.GetCaption(9)
Me.Check1.Caption = aLb.GetCaption(10)
Me.Caption = aLb.GetCaption(11)
Me.Toolbar1.Buttons("VALID").ToolTipText = aLb.GetCaption(12)
Me.Toolbar1.Buttons("PREVIEW").ToolTipText = aLb.GetCaption(13)
Me.Toolbar1.Buttons("SUPPR").ToolTipText = aLb.GetCaption(14)
Me.Toolbar1.Buttons("RENAME").ToolTipText = aLb.GetCaption(15)
Me.Toolbar1.Buttons("SAVE").ToolTipText = aLb.GetCaption(16)
Me.Toolbar1.Buttons("PRINT").ToolTipText = aLb.GetCaption(17)
Me.Toolbar1.Buttons("REFRESH").ToolTipText = aLb.GetCaption(18)
Me.Toolbar1.Buttons("FUSION").ToolTipText = aLb.GetCaption(19)
Me.Toolbar1.Buttons("ECLAT").ToolTipText = aLb.GetCaption(20)
Me.Toolbar1.Buttons("ABSTRACT").ToolTipText = aLb.GetCaption(21)

End Sub

Private Sub FileDeleteProperties(strFichier As String, WFD As WIN32_FIND_DATA, hFile As Long)
'-> cette procedure permet de reaffecter aux spool leurs propri�t�s
Dim strFichierSave As String
Dim strProperties As String
Dim aItem As ListItem
Dim i As Integer
Dim strFileName As Object
Dim ficCible As Long, Ftcree As FILETIME, Ftmod As FILETIME, Ftbid As FILETIME
Dim RetVal As Long
Dim L1, L2, L3, L4, L5, L6 As Long
Dim Ft1 As FILETIME, Ft2 As FILETIME, SysTime As SYSTEMTIME, ft3 As FILETIME

'-> gestion des erreurs
On Error Resume Next
If Rnd < 0.05 Then
    frmWait.Label1.Caption = CInt(countFile / countFileT * 100) & "%   " & countFile & " fichiers analys�(s)" & Chr(13) & "[Echap] pour quitter l'analyse des propri�t�s"
    DoEvents
End If

countFile = countFile + 1

'-> on utilise la dll dsofile.dll
strFichierSave = strFichier

'-> on se met a gauche les infos sur les dates
L1 = WFD.ftCreationTime.dwHighDateTime
L2 = WFD.ftCreationTime.dwLowDateTime
L3 = WFD.ftLastAccessTime.dwHighDateTime
L4 = WFD.ftLastAccessTime.dwLowDateTime
L5 = WFD.ftLastWriteTime.dwHighDateTime
L6 = WFD.ftLastWriteTime.dwLowDateTime

'-> on s'occupe maintenant des propri�t�s
DSO.Open strFichier
'-> on pointe sur les propri�t�s
Set DSOprop = DSO.SummaryProperties
'-> si aucune version n'est affect�e
strFichier = GetUnzipFileName(strFichier)
'-> on vire tout
DSOprop.Author = ""
DSOprop.Category = ""
DSOprop.Comments = ""
DSOprop.Keywords = ""
DSOprop.Subject = ""
DSOprop.Title = ""
DSOprop.company = ""
DSOprop.manager = ""
DSOprop.Title = ""
'-> on sauvegarde les infos
DSO.Save
'-> on redonne la date d'origine au fichier
WFD.ftCreationTime.dwHighDateTime = L1
WFD.ftCreationTime.dwLowDateTime = L2
WFD.ftLastAccessTime.dwHighDateTime = L3
WFD.ftLastAccessTime.dwLowDateTime = L4
WFD.ftLastWriteTime.dwHighDateTime = L5
WFD.ftLastWriteTime.dwLowDateTime = L6
ficCible = CreateFile(strFichier, &H40000000, &H1 Or &H2, ByVal 0&, 3, 0, 0)
RetVal = SetFileTime(ficCible, WFD.ftCreationTime, WFD.ftLastAccessTime, WFD.ftLastWriteTime)
CloseHandle ficCible

GestError:

'Call CloseHandle(hFile)
Set DSOprop = Nothing
DSO.Close
End Sub

Public Function FilesDeleteProperties(sRoot As String, sFile As String, Optional sFiltreDirectory As String, Optional noFirst As Boolean) As String
'--> cette fonction charge le listview a partir d'un chemin root
Dim WFD As WIN32_FIND_DATA
Dim hFile As Long
Dim aItem As ListItem

'-> on gele l'ecran
LockWindowUpdate Me.ListView1.hwnd
'-> on met le sablier
Me.MousePointer = 11

'-> si c'est la premiere fois
If Not noFirst Then
    '-> on affiche le timer
    frmWait.Show
    '-> on charge le listview car c'est rapide
    Me.ListView1.ListItems.Clear
    FilesAddInListview sRoot, sFile
    countFile = 0
    countFileT = Me.ListView1.ListItems.Count
    '-> on initialise le top d'arret
    TopStop = False
End If

 With fp
    .sFileRoot = QualifyPath(sRoot)      'chemin de d�part
    .sFileNameExt = sFile                'fichier (* ? autoris�
    .bRecurse = 1                             'True = recherche recursive
    .bFindOrExclude = 1                       '0=inclure, 1=exclure
 End With
  
 hFile = FindFirstFile(sRoot & "*.*", WFD)
 If hFile <> -1 Then
    Do
      'si c'est un repertoire on boucle
       If (WFD.dwFileAttributes And vbDirectory) Then
          If Asc(WFD.cFileName) <> CLng(46) Then
              If fp.bRecurse Then
                  FilesDeleteProperties sRoot & TrimNull(WFD.cFileName) & vbBackslash, sFile, , True
              End If
          End If
       Else
         'doit etre un fichier..
          If MatchSpec(WFD.cFileName, fp.sFileNameExt) Then
              If sFiltreDirectory <> "" Then
                  If InStr(1, sRoot & TrimNull(WFD.cFileName), sFiltreDirectory) <> 0 Then
                      FileDeleteProperties sRoot & TrimNull(WFD.cFileName), WFD, hFile
                  End If
              Else
                  FileDeleteProperties sRoot & TrimNull(WFD.cFileName), WFD, hFile
              End If
          End If
       End If
       '-> quitter si on l'a demand�
       If TopStop = True Then Exit Do
    Loop While FindNextFile(hFile, WFD)
 End If
Call FindClose(hFile)

If Not noFirst Then
    '-> on charge les combos
    LoadCombo 8, Combo1
    LoadCombo 7, Combo2
    LoadCombo 3, Combo3
    LoadCombo 4, Combo4
    LoadCombo 6, Combo5
    '-> on recharge le listview
    FiltreListView (True)
    '-> on restore la souris
    Me.MousePointer = 0
    FormatListView Me.ListView1
    '-> tri par d�faut les dates
    'Me.ListView1.SortOrder = lvwAscending
    'ColumnOrder Me.ListView1, Me.ListView1.ColumnHeaders(2)
    '-> on masque le timer
    Unload frmWait
    
    '-> on sauvegarde ici les donn�es
    Set aColItem = New Collection
    For Each aItem In Me.ListView1.ListItems
        aColItem.add aItem, aItem.Key
    Next
    
    LockWindowUpdate 0
End If

Exit Function
GestError:
LockWindowUpdate 0

End Function

