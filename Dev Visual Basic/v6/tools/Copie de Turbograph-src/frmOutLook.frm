VERSION 5.00
Begin VB.Form frmOutLook 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Export vers Outlook"
   ClientHeight    =   4395
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5835
   Icon            =   "frmOutLook.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4395
   ScaleWidth      =   5835
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox Check2 
      Caption         =   "Envoyer la maquette"
      Height          =   195
      Left            =   120
      TabIndex        =   9
      Top             =   4080
      Width           =   3615
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Envoyer le fichier de donn�es"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   3840
      Width           =   4215
   End
   Begin VB.PictureBox Annuler 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   4440
      Picture         =   "frmOutLook.frx":212A
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   42
      TabIndex        =   7
      Top             =   3840
      Width           =   630
   End
   Begin VB.CommandButton Command1 
      Height          =   495
      Left            =   5160
      Picture         =   "frmOutLook.frx":316C
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Envoyer vers Outlook"
      Top             =   3840
      Width           =   615
   End
   Begin VB.TextBox Text2 
      Height          =   855
      Left            =   120
      TabIndex        =   4
      Top             =   2880
      Width           =   5655
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   120
      TabIndex        =   3
      Top             =   2040
      Width           =   5655
   End
   Begin VB.ListBox List1 
      Height          =   1035
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   5655
   End
   Begin VB.Label Label3 
      Caption         =   "Texte associ� au document : "
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   2520
      Width           =   2295
   End
   Begin VB.Label Label2 
      Caption         =   "Sujet de l'envoi : "
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1680
      Width           =   3255
   End
   Begin VB.Label Label1 
      Caption         =   "Liste des destinataires : "
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2295
   End
End
Attribute VB_Name = "frmOutLook"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Dim OutLookObj As New Outlook.Application
'Dim OutMailItem As MailItem
'Dim myAddressList As AddressList
'
'
'Private Sub Annuler_Click()
'    Unload Me
'End Sub
'
'Private Sub Command1_Click()
'
'On Error Resume Next
'
'Set olns = OutLookObj.GetNamespace("MAPI")
'Set BoiteEnvoi = olns.GetDefaultFolder(4)
'Set itm = BoiteEnvoi.Items.Add("IPM.Note.Spie")
'
''-> param�trer les propri�t�s
'With itm
'    .Body = Me.Text2.Text & Chr(13) + Chr(10) & Chr(13) + Chr(10) & "Pi�ces jointes : " & Chr(13) + Chr(10) & Chr(13) + Chr(10)
'    .Subject = Me.Text1.Text
'    .Recipients.Add (Me.List1.Text)
'End With
''-> Lier le fichier texte qui sert de donn�es
'If Me.Check1.Value = 1 Then
'    itm.Attachments.Add NomFichier, olByValue
'End If
'
''-> Lier la maquette si necessaire
'If Me.Check2.Value = 1 Then
'    itm.Attachments.Add NomMaquette, olByValue
'End If
'
''-> Afficher le message
'itm.Display
'
''-> Decharger la feuille
'Unload Me
'
'End Sub
'
'Private Sub Form_Load()
'
''---> Cr�er un lien vers OutLook
'
''-> Cr�er un lien m�moire Vers Outlook
'Set myAddressList = OutLookObj.Session.AddressLists("Liste d'adresses globale")
''-> Afficher les entr�es de la liste
'For i = 1 To myAddressList.AddressEntries.Count
'    Me.List1.AddItem myAddressList.AddressEntries.Item(i).Name
'Next
''-> S�lectionner le premier
'Me.List1.Selected(0) = True
'
'
'End Sub
