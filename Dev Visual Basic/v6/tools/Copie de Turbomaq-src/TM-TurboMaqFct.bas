Attribute VB_Name = "TurboMaqFct"
Option Explicit
'Passage en v6
Public V6InitPath  As String 'Indique le r�pertoire de param�trage o� se trouve le fichier d'initialisation
Public V6MqtPath As String ' Indique le r�pertoire des fichiers langue
Public V6Root As String 'Variable $ROOT du fichier Turbomaq.Ini
Public V6MaqCharPath As String  'Emplacement des maquettes de ref pour le standard et client
Public V6GuiCharPath As String 'Emplacement des maquettes GUI + transition + image du standard
Public V6IdentPath As String 'Emplacement du paths des maquettes par ident
Public V6ClientList As Collection 'Liste des clients r�f�renc�s
Public V6ClientCours As Client 'R�f�rence vers le client en cours
Public V6App As String '-> Nom du progiciel en cours
Public V6Rub As String '-> Rubrique associ�e au progiciel en cours
Public V6ListePath() As String '-> Liste des paths

'-> Propath g�n�ral de l'applicatif : tient compte du propath g�n�ral + propath _
applicatifs + r�pertoire BMP de l'applicatif + Repertoire du client + r�pertoire _
BMP du client
'-> Pour r�cup�ration nom du fichier Mqt
Public RetourMqt As String
'-> Pour insertion Image
Public RetourBmp As String
'-> Code langue
Public CodeLangue As String
'-> Indique si les Paths Sont charg�es
Public IsLoadedPath As Boolean
'-> Collection qui contient la maquette
Public Maquettes As New Collection
'-> Collection qui contient la maquette carcat�re associ�e
Public MaqChars As New Collection
'-> Collection qui contient le squelette d'une maquette caract�re
Public MaqToGuis As New Collection
'-> Indique si la feuille de propri�t� est visible
Public frmPropVisible As Boolean
'-> Variable globale qui contient l'objet en cours d'utilsation pour l'affichage _
de la page de propri�t�
Public ObjCours As Variant
'-> Variables utilis�es pour la gestion du s�parateur d�cimal
Public OldFormat As String
Public RestaureNumericFormat As Boolean
'-> Variable qui sert � faire la transition entre l'objet appelant et la palette de couleur
Public RetourColor As Long
'-> Varaible qui sert � faire la transition entre l'objet appelant et la palette de bordure
Public RetourBordure As Integer
'-> Dimensions par d�faut d'une section as cr�taion
Public Const DefHauteurSection = 5
'-> Variable pour l'ouverture de la maquette
Public ErrorLibelle As String
Public MaqVersion As Integer
'-> Indique que le programme supprime les feuilles
Public EraseFrm As Boolean
'-> Variable pour dessin du temporisateur
Public TailleLue As Long
Public TailleTotale As Long
'-> Variable pour la copie de maqChar
Public afrmTransition As frmMaqChar
'-> Variables pour gestion des options de tableaux
Public DefNbCol As Integer
Public DefNbLig As Integer
Public DefLargeurCol As Single
Public DefHauteurLig As Single
Public DefBold As Boolean
Public DefItalic As Boolean
Public DefUnderline As Boolean
Public DefALignCell As Integer
Public DefBas As Boolean
Public DefHaut As Boolean
Public DefGauche As Boolean
Public DefDroite As Boolean
Public DefBackcolor As Long
Public DefFontColor As Long
Public DefRetourLig As Boolean
Public DefFontName As String
Public DefFontSize As Integer
'-> Indique que la maquette de transition est en cours d'�dition
Public IsEditMaqChar As Boolean
'-> Contenu de la balise HTML
Public BaliseHTML As String
Public FileHTML As String
'-> Variable de retour pour le menu
Public RetourMenu As Integer
'-> Pour copy des cellules
Private Type CellCopy
    Contenu  As String
    BackColor As Long
    FontName As String
    FontSize As Integer
    FontColor As Long
    FontBold As Boolean
    FontItalic As Boolean
    FontUnderline As Boolean
    CellAlign As Integer
    AutoAjust As Boolean
End Type
Public ClipBoardCell As CellCopy
Public IsLoading As Boolean

'-> Var d'�change
Public strRetour As String

'-> Indique le nom de la maquette caract�re qui sert de mod�le
Public MaqRef As String

'-> Permet de stocker le compl�ment de bas de page pour les maquettes char
Public MaqBasPage As String



Public Function VerifValue(ByRef ValueToVerifie, ByVal TypeValue, ByVal FormatValue As String) As Boolean

'---> V�rifie le type de valeur saisie
' avec TypeValue    = 1 -> Single _
                    = 2 -> Long _
                    = 3 -> Texte


Select Case TypeValue
    Case 1
        If Not IsNumeric(ValueToVerifie) Then
            VerifValue = False
        Else
            '-> Formater la zone
            ValueToVerifie = Format(CSng(ValueToVerifie), FormatValue)
            VerifValue = True
        End If
    
    Case 2
        If Not IsNumeric(ValueToVerifie) Then
            VerifValue = False
        Else
            '-> Formater la zone
            ValueToVerifie = Format(CLng(ValueToVerifie), FormatValue)
            VerifValue = True
        End If
    
    Case 3
        ValueToVerifie = CStr(ValueToVerifie)
        VerifValue = True
         
End Select

End Function

Public Function VerifmPage(ByVal MaqDim As Single, ByVal mDim As Single) As Boolean
                             
'-> Verification si marge rentrent dans la feuille

Dim aLb As Libelle
                             
If MaqDim < mDim * 2 + 1 Then
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(3) & Chr(13) & aLb.GetCaption(4), vbCritical + vbOKOnly, aLb.GetToolTip(3)
    Set aLb = Nothing
    VerifmPage = False
    Exit Function
End If

VerifmPage = True

End Function

Public Sub SetRedimBorder(ByVal Ctrl As PictureBox, ByVal En As Boolean)

'---> Ajoute ou supprime la bordure de redimension d'un objet

Dim lngStyle As Long

Dim x As Long
'-> R�cup�rer l'ancien style
lngStyle = GetWindowLong(Ctrl.hwnd, GWL_STYLE)
'-> Cr�er le nouveau
If En = True Then
    lngStyle = lngStyle Or WS_THICKFRAME
Else
    lngStyle = lngStyle Xor WS_THICKFRAME
End If
'-> Appliquer le style
x = SetWindowLong(Ctrl.hwnd, GWL_STYLE, lngStyle)
x = SetWindowPos(Ctrl.hwnd, Turbo_Maq.hwnd, 0, 0, 0, 0, SWP_FLAGS)

End Sub

Public Function AlignOnObject(ByVal ObjectToAlign, _
                              ByVal NomRef As String, _
                              ByVal Alignement As Integer, _
                              ByVal DifX As Single, _
                              ByVal DifY As Single) As String

'---> Aligne un object par rapport � un autre
Dim aFrmSection As frmSection
Dim PosX As Single
Dim PosY As Single
Dim MyObject As PictureBox
Dim MyObjectRef As PictureBox
Dim aBmp As ImageObj
Dim aCadre As Cadre
Dim aLb As Libelle
Dim KeyMaster As String
Dim MyObjectRefObj As Object 'ImageObj

'-> Pointer sur la feuille
Set aFrmSection = Maquettes("MAQUETTE").frmSections(UCase$(ObjectToAlign.SectionName))

'-> Pr�parer l'objet global
If TypeOf ObjectToAlign Is Cadre Then
    Set MyObject = aFrmSection.Cadre(ObjectToAlign.IdAffichage)
ElseIf TypeOf ObjectToAlign Is ImageObj Then
    Set MyObject = aFrmSection.BitMap(ObjectToAlign.IdAffichage)
End If

'-> Pointer sur l'objet de r�f�rence
If aFrmSection.SectionCours.GetBmpExist(NomRef) Then
    '-> L'objet est un Bmp
    Set aBmp = aFrmSection.SectionCours.Bmps(UCase$(NomRef))
    Set MyObjectRef = aFrmSection.BitMap(aBmp.IdAffichage)
    KeyMaster = "BMP|" & NomRef
    Set MyObjectRefObj = aBmp
    Set aBmp = Nothing
ElseIf aFrmSection.SectionCours.GetCadreExist(NomRef) Then
    '-> L'objet est un cadre
    Set aCadre = aFrmSection.SectionCours.Cadres(UCase$(NomRef))
    Set MyObjectRef = aFrmSection.Cadre(aCadre.IdAffichage)
    KeyMaster = "CDR|" & NomRef
    Set MyObjectRefObj = aCadre
    Set aCadre = Nothing
Else
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(32), vbCritical + vbOKOnly, aLb.GetToolTip(32)
    Set aFrmSection = Nothing
    Set MyObject = Nothing
    Set MyObjectRef = Nothing
    Exit Function
End If
    
'-> Attention : PosX et PosY sont exprim�s en Twips

DifX = aFrmSection.ScaleX(DifX, 7, 1)
DifY = aFrmSection.ScaleY(DifY, 7, 1)

Select Case Alignement

    Case 1
        PosX = MyObjectRef.Left - MyObject.Width
        PosY = MyObjectRef.Top - MyObject.Height
        
    Case 2
        PosX = MyObjectRef.Left
        PosY = MyObjectRef.Top - MyObject.Height
    
    Case 3
        PosX = (MyObjectRef.Width - MyObject.Width) / 2 + MyObjectRef.Left
        PosY = MyObjectRef.Top - MyObject.Height
    
    Case 4
        PosX = MyObjectRef.Width - MyObject.Width + MyObjectRef.Left
        PosY = MyObjectRef.Top - MyObject.Height
        
    Case 5
        PosX = MyObjectRef.Width + MyObjectRef.Left
        PosY = MyObjectRef.Top - MyObject.Height
        
    Case 6
        PosX = MyObjectRef.Width + MyObjectRef.Left
        PosY = MyObjectRef.Top
        
    Case 7
        PosX = MyObjectRef.Width + MyObjectRef.Left
        PosY = (MyObjectRef.Height - MyObject.Height) / 2 + MyObjectRef.Top
        
    Case 8
        PosX = MyObjectRef.Width + MyObjectRef.Left
        PosY = (MyObjectRef.Height - MyObject.Height) + MyObjectRef.Top
        
    Case 9
        PosX = MyObjectRef.Left - MyObject.Width
        PosY = MyObjectRef.Top
    
    Case 10
        PosX = MyObjectRef.Left - MyObject.Width
        PosY = (MyObjectRef.Height - MyObject.Height) / 2 + MyObjectRef.Top
    
    Case 11
        PosX = MyObjectRef.Left - MyObject.Width
        PosY = (MyObjectRef.Height - MyObject.Height) + MyObjectRef.Top
        
    Case 12
        PosX = MyObjectRef.Left - MyObject.Width
        PosY = MyObjectRef.Height + MyObjectRef.Top
    
    Case 13
        PosX = MyObjectRef.Left
        PosY = MyObjectRef.Height + MyObjectRef.Top
        
    Case 14
        PosX = (MyObjectRef.Width - MyObject.Width) / 2 + MyObjectRef.Left
        PosY = MyObjectRef.Height + MyObjectRef.Top
        
    Case 15
        PosX = MyObjectRef.Width - MyObject.Width + MyObjectRef.Left
        PosY = MyObjectRef.Height + MyObjectRef.Top
        
    Case 16
        PosX = MyObjectRef.Width + MyObjectRef.Left
        PosY = MyObjectRef.Height + MyObjectRef.Top
        
End Select
  
'-> Tenir compte de la variation
PosX = PosX + DifX
PosY = PosY + DifY
  
'-> Appliquer la modification � l'objet physique
MyObject.Left = PosX
MyObject.Top = PosY
  
'-> Mettre � jour l'objet
ObjectToAlign.Left = aFrmSection.ScaleX(PosX, 1, 7)
ObjectToAlign.Top = aFrmSection.ScaleY(PosY, 1, 7)

'-> Mise � jour de la page de propri�t�
If frmPropVisible Then frmProp.DisplayProperties
  
'-> Renvoyer la cl� de la r�f�rence
AlignOnObject = KeyMaster

Set aFrmSection = Nothing
Set MyObject = Nothing
Set MyObjectRef = Nothing
Set aBmp = Nothing
Set aCadre = Nothing
Set aLb = Nothing
Set ObjectToAlign = Nothing

End Function


Public Sub RefreshAlign(ByVal MasterObject As Object)

'---> Cette fonction effectue les alignements

Dim aDefObj As String
Dim TypeObj As String
Dim NomObj As String
Dim aSection As Section
Dim ObjectToAlign As Object

aDefObj = Entry(1, MasterObject.MasterAlign, "-")
'-> S'il y a un aligbnement page, effectuer cet alignement avant d'aligner les esclaves
If MasterObject.MasterAlign = "" Then
    '-> Il ne faut almigner que les esclaves
    MasterObject.MoveSlave
    Exit Sub
End If
If UCase$(aDefObj) = "PAGE" Then
    '-> Refaire l'alignement sur la page
    AlignOnPage CInt(Entry(2, MasterObject.MasterAlign, "-")), MasterObject
    Set ObjectToAlign = MasterObject
Else
    TypeObj = Entry(1, aDefObj, "|")
    NomObj = Entry(2, aDefObj, "|")

    '-> Pointer sur la section m�re
    Set aSection = Maquettes("MAQUETTE").Sections(UCase$(MasterObject.SectionName))
    '-> Pointer sur l'objet maitre
    If UCase$(TypeObj) = "CDR" Then
        Set ObjectToAlign = aSection.Cadres(UCase$(NomObj))
    ElseIf UCase$(TypeObj) = "BMP" Then
        Set ObjectToAlign = aSection.Bmps(UCase$(NomObj))
    End If
End If
'-> Deplacer les esclaves
ObjectToAlign.MoveSlave

Set ObjectToAlign = Nothing
Set aSection = Nothing
Set MasterObject = Nothing

End Sub

Public Sub AlignOnPage(ByVal RetourAlign As Integer, ByVal ObjectToAlign)

'---> Gestion des alignements sur la section

'Types d'alignement : de 1 � 9 par bandeau de 3

Dim aFrmSection As frmSection
Dim PosX As Single
Dim PosY As Single
Dim MyObject



'-> Pointer sur la feuille
Set aFrmSection = Maquettes("MAQUETTE").frmSections(UCase$(ObjectToAlign.SectionName))

If TypeOf ObjectToAlign Is Cadre Then
    Set MyObject = aFrmSection.Cadre(ObjectToAlign.IdAffichage)
ElseIf TypeOf ObjectToAlign Is ImageObj Then
    Set MyObject = aFrmSection.BitMap(ObjectToAlign.IdAffichage)
End If
    
'-> Attention : PosX et PosY sont stipul�s en twips et convertis ensuite
    
Select Case RetourAlign

    Case 1
        PosX = 0
        PosY = 0
            
    Case 2
        PosX = (aFrmSection.ContainerRtf.Width - MyObject.Width) / 2
        PosY = 0
                    
    Case 3
        PosX = aFrmSection.ContainerRtf.Width - MyObject.Width
        PosY = 0
    
    Case 4
        PosX = 0
        PosY = (aFrmSection.ContainerRtf.Height - MyObject.Height) / 2
            
    Case 5
        PosX = (aFrmSection.ContainerRtf.Width - MyObject.Width) / 2
        PosY = (aFrmSection.ContainerRtf.Height - MyObject.Height) / 2
    
    Case 6
        PosX = aFrmSection.ContainerRtf.Width - MyObject.Width
        PosY = (aFrmSection.ContainerRtf.Height - MyObject.Height) / 2
    
    Case 7
        PosX = 0
        PosY = aFrmSection.ContainerRtf.Height - MyObject.Height
    
    Case 8
        PosX = (aFrmSection.ContainerRtf.Width - MyObject.Width) / 2
        PosY = aFrmSection.ContainerRtf.Height - MyObject.Height
        
    Case 9
        PosX = aFrmSection.ContainerRtf.Width - MyObject.Width
        PosY = aFrmSection.ContainerRtf.Height - MyObject.Height
        
End Select


'-> Appliquer les coordon�es � l'objet
MyObject.Left = PosX
MyObject.Top = PosY

'-> Mettre � jour l'objet
ObjectToAlign.Left = aFrmSection.ScaleX(PosX, 1, 7)
ObjectToAlign.Top = aFrmSection.ScaleY(PosY, 1, 7)

'-> Mise � jour de la page de propri�t�
If frmPropVisible Then frmProp.DisplayProperties


Set ObjectToAlign = Nothing
Set aFrmSection = Nothing
Set MyObject = Nothing

End Sub

Public Sub DrawCell(ByVal Ligne As Integer, ByVal Colonne As Integer, _
                    Sortie As PictureBox, ByVal DecalageY As Integer, _
                    aBlock As Block, ByVal Region As Long)

'---> Fonction qui dessine une cellule

Dim hdlPen As Long
Dim hdlBrush As Long
Dim hdlBordure As Long
Dim oldPen As Long
Dim oldBrush As Long
Dim aPoint As POINTAPI
Dim aRect As Rect
Dim Res As Long

Dim tm As TEXTMETRIC
Dim HauteurFont As Long
Dim LargeurFont As Long
Dim PoidFont As Long
Dim lf As LOGFONT
Dim TempByteArray() As Byte
Dim ByteArrayLimit As Long
Dim FontToUse As Long
Dim OldFont As Long

Dim X1 As Integer, X2 As Integer, Y1 As Integer, Y2 As Integer

Dim aCell As Cellule

'-> Pointer sur la cellule � dessiner
Set aCell = aBlock.Cellules("L" & Ligne & "C" & Colonne)

X1 = aCell.X1
X2 = aCell.X2
Y1 = aCell.Y1
Y2 = aCell.Y2

'-> Tenir compte du d�callage
Y1 = Y1 + DecalageY
Y2 = Y2 + DecalageY

'-> Cr�ation des objets GDI pour dessin des cellules
hdlBrush = CreateSolidBrush(aCell.BackColor)
hdlPen = CreatePen(PS_NULL, 1, QBColor(15))

'-> S�lection du pinceau dans le Contexte et du stylo dans le DC
oldBrush = SelectObject(Sortie.hDC, hdlBrush)
oldPen = SelectObject(Sortie.hDC, hdlPen)

'-> Dessin de la cellule
Res = Rectangle(Sortie.hDC, X1, Y1, X2, Y2)

'-> Setting du Rect de la cellule
aRect.Left = X1
aRect.Top = Y1
aRect.Right = X2 - 1
aRect.Bottom = Y2 - 1


hdlBordure = CreateSolidBrush(QBColor(12))

'-> Dessin du cadre de s�lection si active
If aCell.IsActive Then
    FrameRect Sortie.hDC, aRect, hdlBordure
End If

DeleteObject hdlBordure

'-> Supprimer les objets GDI  non utilis�s
If oldPen <> 0 Then
    SelectObject Sortie.hDC, oldPen
    DeleteObject hdlPen
End If

'-> Res�lectionner l'ancien pinceau
If oldBrush <> 0 Then
    SelectObject Sortie.hDC, oldBrush
    DeleteObject hdlBrush
End If

Dim pDrawText As Long
Dim Rect2 As Rect
Dim DifL As Integer
Dim DifH As Integer
Dim IsCellAjust As Boolean

Dim hdlRgn As Long
Dim AlignBase(1 To 2, 3 To 6) As Integer

'-> Appliquer les options de font
Sortie.FontName = aCell.FontName
Sortie.FontSize = aCell.FontSize
Sortie.FontBold = aCell.FontBold
Sortie.FontItalic = aCell.FontItalic
Sortie.FontUnderline = aCell.FontUnderline
Sortie.ForeColor = aCell.FontColor

'-> R�cup�ration des options de Font par API : Hauteur , Largeur , Poid
'Res = GetTextMetrics(Sortie.hDC, tm)
'PoidFont = tm.tmWeight

'-> Cr�er une police ad�quate
'lf.lfHeight = -13 'MulDiv(aCell.FontSize, GetDeviceCaps(Sortie.hDC, LOGPIXELSY), 72)
'lf.lfWidth = 5
'lf.lfWeight = 400
'lf.lfEscapement = 0
'If aCell.FontUnderline Then lf.lfUnderline = 1
'If aCell.FontItalic Then lf.lfItalic = 1
'lf.lfOutPrecision = OUT_DEFAULT_PRECIS
'lf.lfClipPrecision = OUT_DEFAULT_PRECIS
'lf.lfQuality = DEFAULT_QUALITY
'lf.lfPitchAndFamily = DEFAULT_PITCH Or FF_DONTCARE
'lf.lfCharSet = DEFAULT_CHARSET
'
''-> Convertir le nom de la police en tableau d'octet
'TempByteArray = StrConv("Times New Roman" & Chr$(0), vbFromUnicode)
'ByteArrayLimit = UBound(TempByteArray)
'For x% = 0 To ByteArrayLimit
'    lf.lfFaceName(x%) = TempByteArray(x%)
'Next
'
''-> Cr�er la police qui en d�coule
'FontToUse = CreateFontIndirect(lf)
'OldFont = SelectObject(Sortie.hDC, FontToUse)
'
'Dim rc As Rect
'
'dl& = GetClientRect(Sortie.hwnd, rc)
'    dl& = TextOut(Sortie.hDC, 1, rc.Bottom / 2, (aCell.Contenu), Len(aCell.Contenu))
'    dl& = SelectObject(Sortie.hDC, OldFont)
'    dl& = DeleteObject(FontToUse)
'Exit Sub


'***********************************
'* Dessin du contenu de la cellule *
'***********************************
    
'-> R�cup�rer l'option de d'alignement interne
IsCellAjust = aCell.AutoAjust
    
'-> Faire une copie de Rect
Rect2.Left = aRect.Left
Rect2.Right = aRect.Right
Rect2.Top = aRect.Top
Rect2.Bottom = aRect.Bottom

'-> Dans un premier temps, calculer la taille necessaire pour afficher si ajustement automatique
If IsCellAjust Then _
    DrawText Sortie.hDC, aCell.Contenu, Len(aCell.Contenu), Rect2, DT_CALCRECT Or DT_WORDBREAK
   
'-> R�cup�ration des diff�rences de largeur et de hauteur
DifL = (aRect.Right - aRect.Left) - (Rect2.Right - Rect2.Left)
DifH = (aRect.Bottom - aRect.Top) - (Rect2.Bottom - Rect2.Top)
    
'-> Calcul des alignements de base
AlignBase(1, 3) = Rect2.Left + DifL / 2
AlignBase(2, 3) = Rect2.Right + DifL / 2

AlignBase(1, 4) = Rect2.Left + DifL
AlignBase(2, 4) = Rect2.Right + DifL

AlignBase(1, 5) = Rect2.Top + DifH / 2
AlignBase(2, 5) = Rect2.Bottom + DifH / 2

AlignBase(1, 6) = Rect2.Top + DifH
AlignBase(2, 6) = Rect2.Bottom + DifH
    
'-> Alignement interne dans le rectangle
Select Case aCell.CellAlign
    
    Case 1
        If IsCellAjust Then
            pDrawText = DT_LEFT
            '-> Pas necessaire de modifier les alignements
        Else
            pDrawText = DT_LEFT Or DT_TOP
        End If
        
    Case 2
        If IsCellAjust Then
            pDrawText = DT_CENTER
            Rect2.Left = AlignBase(1, 3)
            Rect2.Right = AlignBase(2, 3)
        Else
            pDrawText = DT_CENTER Or DT_TOP
        End If
    
    Case 3
        If IsCellAjust Then
            pDrawText = DT_RIGHT
            Rect2.Left = AlignBase(1, 4)
            Rect2.Right = AlignBase(2, 4)
        Else
            pDrawText = DT_RIGHT Or DT_TOP
        End If
                
    Case 4
        If IsCellAjust Then
            pDrawText = DT_LEFT
            Rect2.Top = AlignBase(1, 5)
            Rect2.Bottom = AlignBase(2, 5)
        Else
            pDrawText = DT_VCENTER Or DT_LEFT
        End If
    
    Case 5
        If IsCellAjust Then
            pDrawText = DT_CENTER
            Rect2.Top = AlignBase(1, 5)
            Rect2.Bottom = AlignBase(2, 5)
            Rect2.Left = AlignBase(1, 3)
            Rect2.Right = AlignBase(2, 3)
        Else
            pDrawText = DT_VCENTER Or DT_CENTER
        End If
    
    Case 6
        If IsCellAjust Then
            pDrawText = DT_RIGHT
            Rect2.Top = AlignBase(1, 5)
            Rect2.Bottom = AlignBase(2, 5)
            Rect2.Left = AlignBase(1, 4)
            Rect2.Right = AlignBase(2, 4)
        Else
            pDrawText = DT_VCENTER Or DT_RIGHT
        End If
    
    Case 7
        If IsCellAjust Then
            pDrawText = DT_LEFT
            Rect2.Top = AlignBase(1, 6)
            Rect2.Bottom = AlignBase(2, 6)
        Else
            pDrawText = DT_BOTTOM Or DT_LEFT
        End If
    
    Case 8
        If IsCellAjust Then
            pDrawText = DT_CENTER
            Rect2.Top = AlignBase(1, 6)
            Rect2.Bottom = AlignBase(2, 6)
            Rect2.Left = AlignBase(1, 3)
            Rect2.Right = AlignBase(2, 3)
        Else
            pDrawText = DT_BOTTOM Or DT_CENTER
        End If
    
    Case 9
        If IsCellAjust Then
            pDrawText = DT_RIGHT
            Rect2.Top = AlignBase(1, 6)
            Rect2.Bottom = AlignBase(2, 6)
            Rect2.Left = AlignBase(1, 4)
            Rect2.Right = AlignBase(2, 4)
        Else
            pDrawText = DT_BOTTOM Or DT_RIGHT
        End If
    
End Select
    
If IsCellAjust Then
    pDrawText = pDrawText Or DT_WORDBREAK
Else
    pDrawText = pDrawText Or DT_SINGLELINE
End If
    
'-> Faire de rect la zone de clipping en cours
hdlRgn = CreateRectRgn&(aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
SelectClipRgn Sortie.hDC, hdlRgn

'-> Supprimer la r�gion associ�e
DeleteObject hdlRgn

'-> Postionnement du rectangle de dessin
DrawText Sortie.hDC, aCell.Contenu, Len(aCell.Contenu), Rect2, pDrawText

'-> Faire de la fen�tre entiere la zone de clipping
GetClientRect Sortie.hwnd, Rect2

hdlRgn = CreateRectRgn(Rect2.Left, Rect2.Top, Rect2.Right, Rect2.Bottom)
SelectClipRgn Sortie.hDC, hdlRgn

'-> Dessin des bordures de la cellule
SelectObject Sortie.hDC, hdlBordure

'-> Annuler les anciennes bordures
aRect.Right = aRect.Right + 1
aRect.Bottom = aRect.Bottom + 1
Res = GetRgnBox(Region, aRect)
aRect.Left = aRect.Left - 1
aRect.Top = aRect.Top - 1
hdlBordure = CreateSolidBrush(&HE0E0E0)
Res = FrameRect(Sortie.hDC, aRect, hdlBordure)

DeleteObject hdlBordure

hdlBordure = CreatePen(PS_SOLID, 1, QBColor(0))
Res = SelectObject(Sortie.hDC, hdlBordure)

'-> Dessin de la bordure gauche
If aCell.BordureGauche Then
    Res = MoveToEx(Sortie.hDC, X1 - 1, Y1 - 1, aPoint)
    LineTo Sortie.hDC, X1 - 1, Y2
End If

'-> Dessin de la bordure Droite
If aCell.BordureDroite Then
    Res = MoveToEx(Sortie.hDC, X2 - 1, Y1 - 1, aPoint)
    LineTo Sortie.hDC, X2 - 1, Y2
End If

'-> Dessin de la bordure Bas
If aCell.BordureBas Then
    Res = MoveToEx(Sortie.hDC, X1 - 1, Y2 - 1, aPoint)
    LineTo Sortie.hDC, X2, Y2 - 1
End If

'-> Dessin de la bordure haut
If aCell.BordureHaut Then
    Res = MoveToEx(Sortie.hDC, X1 - 1, Y1 - 1, aPoint)
    LineTo Sortie.hDC, X2, Y1 - 1
End If

'-> Supprimer la r�gion
DeleteObject hdlRgn
               
'-> Supprimer le stylo de dessin des bordures
DeleteObject hdlBordure

'-> Res�lectionner l'ancien stylo
If oldPen <> 0 Then
    SelectObject Sortie.hDC, oldPen
    DeleteObject hdlPen
End If

'-> Res�lectionner l'ancien pinceau
If oldBrush <> 0 Then
    SelectObject Sortie.hDC, oldBrush
    DeleteObject hdlBrush
End If

'-> Restituer les polices
'If OldFont <> 0 Then
'    SelectObject Sortie.hDC, OldFont
'    DeleteObject FontToUse
'End If

'-> Lib�rer le pointeur sur la cellule
Set aCell = Nothing
Set Sortie = Nothing

End Sub

Public Function OpenMaq(ByVal aMaquette As String) As Boolean

'---> Cette fonction sert � ouvrir une maquette graphique

Dim MaqFic As Integer
Dim Ligne As String
Dim Rep As Boolean
Dim ErrorValue As Integer
Dim aLb As Libelle
Dim aMaq As Maquette
Dim RtfValue As String
Dim Res As Long
Dim aOf As OFSTRUCT
Dim aRg As RangChar
Dim NomRang As String
Dim IsFormat As Boolean
Dim aApp As Application
Dim hdlFile As Integer

'On Error GoTo ErrorOpen THIERRY

'-> Bloquer la feuille
Turbo_Maq.Enabled = False

'-> R�cup�ration des libelles des messages d'erreur
Set aLb = Libelles("OPENMAQ")

'-> Afficher le temporisteur
Turbo_Maq.Picture1.Visible = True
Turbo_Maq.Label1.Caption = aLb.GetCaption(8)
DoEvents

'-> Modifier le pointeur de souris
Turbo_Maq.MousePointer = 11

'-> R�cup�ration des infos pour ProgressBar
hdlFile = OpenFile(aMaquette, aOf, OF_READ)
If hdlFile <> -1 Then
    Res = GetFileSize(hdlFile, 0)
    TailleTotale = Res
    Res = CloseHandle(hdlFile)
End If

MaqFic = FreeFile
Open aMaquette For Input As #MaqFic

Do While Not EOF(MaqFic)

    '-> Lecture de la ligne
    Line Input #MaqFic, Ligne
    
    '-> Afficher dans le temporisateur
    TailleLue = TailleLue + Len(Ligne)
    DrawWait
    

    '-> Ne pas lire les lignes ""
    If Trim(Ligne) = "" Then
    Else
        '-> Analyse du premier caract�re
        If Mid$(Ligne, 1, 1) = "\" Then
            '-> Analyse de l'entete
            If UCase$(Ligne) = "\DEFENTETE�BEGIN" Then
                ErrorLibelle = "Entete"
                If Not (Analyse_Entete(MaqFic)) Then
                    ErrorValue = 5
                    GoTo ErrorOpen
                Else
                    '-> Pointer sur la maquette
                    Set aMaq = Maquettes("MAQUETTE")
                    aMaq.Fichier = aMaquette
                End If
            End If
        ElseIf Mid$(Ligne, 1, 1) = "[" Then
            '-> Analyse des objets
            If UCase$(Mid$(Ligne, 2, 2)) = "NV" Then
                '-> Cr�ation de la section
                If UCase$(Entry(3, Ligne, "-")) = "STD]" Then
                    If Not (CreateSection(MaqFic, Entry(2, Ligne, "-"), aMaq)) Then
                        ErrorValue = 6
                        GoTo ErrorOpen
                    End If
                ElseIf UCase$(Entry(3, Ligne, "-")) = "RTF]" Then
                '-> Lecture du contenu RTf de la section
                    If Not (CreateSectionRtf(MaqFic, aMaq, Entry(2, Ligne, "-"))) Then
                        ErrorValue = 6
                        GoTo ErrorOpen
                    End If
                End If
            ElseIf UCase$(Mid$(Ligne, 2, 2)) = "TB" Then
                '-> Cr�ation du tableau
                If Not (CreateTableau(MaqFic, Mid$(Entry(2, Ligne, "-"), 1, Len(Entry(2, Ligne, "-")) - 1), aMaq)) Then
                    ErrorValue = 7
                    GoTo ErrorOpen
                End If
            ElseIf UCase$(Ligne) = "[HTML]" Then
                '-> Lecture de la balise HTML
                If Not (CreateBaliseHtml(MaqFic)) Then
                    ErrorValue = 11
                    GoTo ErrorOpen
                End If
            ElseIf UCase$(Ligne) = "[MAQCHAR]" Then
                If Not CreateMaqChar(MaqFic) Then
                    ErrorValue = 10
                    GoTo ErrorOpen
                End If
            ElseIf UCase$(Ligne) = "[MAQUETTECHAR]" Then
                '-> Pointer sur le rang d'entete
                Set aRg = MaqChars("MAQCHAR").RangChars("ENTETE")
                '-> Charger en m�moire la maquette caract�re
                Do While Not EOF(MaqFic)
                    '-> Lecture de la ligne
                    Line Input #MaqFic, Ligne
                    '-> Rechercher le lig et le max
                    If UCase$(Mid$(Ligne, 1, 4)) = """LIG" Then MaqChars("MAQCHAR").Lig = CInt(Mid$(Trim(Ligne), 5, Len(Trim(Ligne)) - 5))
                    If UCase$(Mid$(Ligne, 1, 4)) = """MAX" Then MaqChars("MAQCHAR").Max = CInt(Mid$(Trim(Ligne), 5, Len(Trim(Ligne)) - 5))
                    If Not IsFormat Then
                        If UCase$(Ligne) = """FIN""" Or InStr(1, UCase$(Ligne), "-FORMAT") <> 0 _
                                                     Or (InStr(1, UCase$(Ligne), "-CANEDI")) <> 0 _
                                                     Or (InStr(1, UCase$(Ligne), "-CALCUL")) <> 0 Then
                            '-> Pointer sur le rang de fin de page
                            Set aRg = MaqChars("MAQCHAR").RangChars("FIN")
                            IsFormat = True
                        End If
                    End If
                    '-> Analyse si c'est un rang
                    If Trim(Ligne) <> """" Then
                        If UCase$(Mid$(Ligne, 1, 3)) = """RG" And Not IsFormat Then
                            Ligne = RTrim(Ligne)
                            '-> Pointer sur le rang
                            NomRang = UCase$(Mid$(Ligne, 2, Len(Ligne) - 2))
                            Set aRg = MaqChars("MAQCHAR").RangChars(UCase$(NomRang))
                            aRg.nLignes = 1
                        Else
                            aRg.AddLigne Ligne
                        End If
                    Else
                        aRg.AddLigne Ligne
                    End If
                Loop
            End If 'Selon nature de l'objet
        End If 'Selo  premier caract�re de la ligne
    End If 'Si ligne = ""
Loop 'Boucle principale

'-> Charger les fichier ressources des champs
If aMaq.FieldListe <> "" Then
    '-> PIERROT : on chercher le DRF en specif , s'il n'existe pas on cherche en std
    If Dir$(V6ClientCours.Applications("APP|" & V6App).V6PathApp & aMaq.FieldListe) <> "" Then
        ChargeDRF V6ClientCours.Applications("APP|" & V6App).V6PathApp & aMaq.FieldListe
    Else
        '-> On charge le drf std
        ChargeDRF Replace(V6GuiCharPath, "$APP", V6App) & aMaq.FieldListe
    End If
    frmFields.Hide
End If

'-> G�rer les menus
GestMenu
GestMaqChar

'-> Fermer le handle du fichier
Close #MaqFic

'-> ne plus afficher le temporisteur
Turbo_Maq.Picture1.Visible = False

'-> Changer le pointer de la souris
Turbo_Maq.MousePointer = 0

'-> D�blocquer la feuille
Turbo_Maq.Enabled = True

Set aMaq = Nothing
Set aRg = Nothing
Set aLb = Nothing

OpenMaq = True

Exit Function
 

'-> Traitement des erreurs d'ouverture
ErrorOpen:

    '-> Afficher le message d'erreur
    MsgBox aLb.GetCaption(4) & Chr(13) & aMaquette & Chr(13) & aLb.GetCaption(ErrorValue) & Chr(13) & ErrorLibelle, vbCritical + vbOKOnly
                
    '-> Supprimer tous les objets et leur repr�sentation physique
    EraseMaquette
    
    '-> G�rer les menus
    GestMenu
    
    '-> Fermer le handle du fichier
    Close #MaqFic
    
    '-> Supprimer le temporisateur
    Turbo_Maq.Picture1.Visible = False
    
    '-> Pointeur de souris normal
    Turbo_Maq.MousePointer = 0
    
    '-> D�blocquer la feuille
    Turbo_Maq.Enabled = True
    
    Set aMaq = Nothing
    Set aRg = Nothing
    Set aLb = Nothing
    
    
End Function
Private Function CreateMaqChar(ByVal MaqFic As Integer) As Boolean

'---> Fonction qui analyse la maquette graphique pour cr�er les rangs caract�res

Dim aMaqChar As MaqChar
Dim aRg As RangChar
Dim i As Integer
Dim Ligne As String
Dim nRangs As Integer
Dim DefAssociation As String
Dim NomObjet As String
Dim TypeObjet As String
Dim DefObjet As String

On Error GoTo GestMaqChar

'-> Cr�ation de la maquette caractere
Set aMaqChar = New MaqChar

'-> Cr�ation du premier rang d'entete
Set aRg = New RangChar
aRg.Nom = "Entete"
aRg.IdAffichage = 1

'-> Ajouter ce rang l�
aMaqChar.RangChars.Add aRg, "ENTETE"

'-> initialiser le compteur de rang
nRangs = 2

'-> Analyse
Do While Not EOF(MaqFic)
    '-> lecture de la ligne
    Line Input #MaqFic, Ligne
    '-> Si ligne vide
    If Trim(Ligne) = "" Then
    Else
        '-> Premier caract�re
        If Mid$(Ligne, 1, 1) = "\" Then
            Select Case UCase$(Entry(1, Ligne, "�"))
                Case "\MAQTRANS"
                    '-> Donner son nom � la maquette
                    aMaqChar.Nom = Entry(2, Ligne, "�")
                Case "\TRANSITION"
                    '-> Nom et emplacement de la maquette de transition
                    aMaqChar.MaqTransition = Entry(2, Ligne, "�")
                Case "\RGCHAR"
                    '-> Cr�ation d'un rang
                    Set aRg = New RangChar
                    '-> Setting des prorpi�t�s
                    aRg.Nom = Entry(2, Ligne, "�")
                    aRg.IdAffichage = nRangs
                    nRangs = nRangs + 1
                    aRg.nLignes = CInt(Entry(3, Ligne, "�")) + 1
                    '-> Ajouter le rang dans la maquette
                    aMaqChar.RangChars.Add aRg, UCase$(aRg.Nom)
                    '-> R�cup�ration de l'association
                    DefAssociation = Entry(4, Ligne, "�")
                    '-> Test s'il y a une association
                    If Trim(DefAssociation) <> "" Then
                        '-> Analyse de l'ensemble des associations
                        For i = 1 To NumEntries(DefAssociation, "|")
                            '-> D�finition de l'objet
                            DefObjet = Entry(i, DefAssociation, "|")
                            '-> type de l'objet
                            TypeObjet = Entry(1, DefObjet, "@")
                            If UCase$(TypeObjet) = "RG" Then
                                '-> 'il s'agit d'un rang
                                aRg.AddLien "SECTION-" & Entry(2, DefObjet, "@")
                            Else
                                '-> Il s'agit d'un block de ligne
                                aRg.AddLien "TABLEAU-" & Entry(2, DefObjet, "@") & "-" & Entry(3, DefObjet, "@")
                            End If 'Selon la nature du parent du lien
                        Next
                    End If 'Si il y a une asso
                Case "\RGPAGE"
                    '-> R�cup�ration de la liste des sauts de page
                    DefAssociation = Entry(2, Ligne, "�")
                    '-> Cr�ation des rangs sauts de page
                    If DefAssociation <> "" Then
                        For i = 1 To NumEntries(DefAssociation, ",")
                            Set aRg = aMaqChar.RangChars(UCase$(Entry(i, DefAssociation, ",")))
                            aRg.IsSautPage = True
                        Next
                    End If
                    '-> Quitter l'analyse
                    Exit Do
            End Select 'selon le contenu
        End If 'Si commence par "\"
    End If 'Si ligne � blanc
Loop

'-> Cr�er le dernier rang
Set aRg = New RangChar
aRg.IdAffichage = nRangs
aRg.Nom = "FIN"

'-> Ajouter ce rang l�
aMaqChar.RangChars.Add aRg, "FIN"

'-> Ajouter la maquette
MaqChars.Add aMaqChar, "MAQCHAR"

'-> Ajouter dans le treeview
aMaqChar.AddTreeView

'-> Renvoyer une valeur de succ�s
CreateMaqChar = True

'-> Liberrer les pointeurs
Set aMaqChar = Nothing
Set aRg = Nothing

'-> Quitter la focntion
Exit Function


GestMaqChar:

    CreateMaqChar = False
    '-> Liberrer les pointeurs
    Set aMaqChar = Nothing
    Set aRg = Nothing


End Function

Private Function Analyse_Entete(ByVal MaqFic As Integer) As Boolean

'---> Fonction qui analyse l'entete de la maquette et renvoie False en cas de pb

'-> Liste des Top � trouver
Dim TopVersion As Boolean
Dim TopNom As Boolean
Dim TopDate As Boolean
Dim TopDescription As Boolean
Dim TopHauteur As Boolean
Dim TopLargeur As Boolean
Dim TopMargeTop As Boolean
Dim TopMargeLeft As Boolean
Dim TopOrientation As Boolean
Dim TopSuite As Boolean
Dim TopReport As Boolean
Dim TopEntete As Boolean
Dim TopPied As Boolean
Dim TopDefEntete As Boolean
Dim TopPageGarde As Boolean
Dim TopPageSelection As Boolean

Dim Ligne As String
Dim DefLigne As String
Dim ValueLigne As String
Dim aMaq As Maquette
Dim aLb As Libelle
Dim Orientation As Integer
Dim Tempo As Single

On Error GoTo ErrorEntete

'-> Pointer sur un nouvel enregistrement maquette
Set aMaq = New Maquette

Do While Not EOF(MaqFic)
    Line Input #MaqFic, Ligne
    '-> Afficher dans le temporisateur
    TailleLue = TailleLue + Len(Ligne)
    DrawWait

    If Trim(Ligne) = "" Then
    Else
        If Mid$(Ligne, 1, 1) = "\" Then
            '-> R�cup�ration des valeurs
            DefLigne = Entry(1, Ligne, "�")
            ValueLigne = Entry(2, Ligne, "�")
            '-> Traiter la valeur de la ligne
            Select Case UCase$(DefLigne)
                Case "\LIGCHAR"
                    aMaq.Lig = ValueLigne
                Case "\NOM"
                    aMaq.Nom = ValueLigne
                    TopNom = True
                Case "\VERSION"
                    MaqVersion = CInt(Convert(ValueLigne))
                    TopVersion = True
                Case "\DATE"
                    aMaq.DateCreation = CDate(ValueLigne)
                    TopDate = True
                Case "\DESCRIPTION"
                    aMaq.Description = ValueLigne
                    TopDescription = True
                Case "\HAUTEUR"
                    aMaq.Hauteur = CSng(Convert(ValueLigne))
                    TopHauteur = True
                Case "\LARGEUR"
                    aMaq.Largeur = CSng(Convert(ValueLigne))
                    TopLargeur = True
                Case "\MARGETOP"
                    aMaq.MargeTop = CSng(Convert(ValueLigne))
                    TopMargeTop = True
                Case "\MARGELEFT"
                    aMaq.MargeLeft = CSng(Convert(ValueLigne))
                    TopMargeLeft = True
                Case "\ORIENTATION"
                    Orientation = CInt(Convert(ValueLigne))
                    TopOrientation = True
                Case "\SUITE"
                    TopSuite = True
                Case "\REPORT"
                    TopReport = True
                Case "\ENTETE"
                    TopEntete = True
                Case "\PIED"
                    TopPied = True
                Case "\FIELDLISTE" 'Version 2.0
                    aMaq.FieldListe = ValueLigne
                Case "\PUBLIPOSTAGE" 'Version2.0 Gestion des ruptures de publipostage
                    aMaq.PubliPostage = ValueLigne
                Case "\PAGEGARDE" 'Version 2.0
                    TopPageGarde = False
'                    If UCase$(ValueLigne) = "VRAI" Then
'                        TopPageGarde = True
'                    Else
'                        TopPageGarde = False
'                    End If
                Case "\PAGESELECTION" 'Version 2.0
                    TopPageSelection = False
'                    If UCase$(ValueLigne) = "VRAI" Then
'                        TopPageSelection = True
'                    Else
'                        TopPageSelection = False
'                    End If
                Case "\DEFENTETE"
                    TopDefEntete = True
                    '-> Fin de la d�finition : sortir de la boucle
                    Exit Do
            End Select 'Selon le top
        End If 'Si premier caract�re = "\"
    End If 'Si ligne = ""
Loop 'Boucle principale

'-> Analyse de l'entete
If Not TopDate Or Not TopDescription Or Not TopHauteur Or Not TopLargeur Or Not TopMargeTop _
Or Not TopMargeLeft Or Not TopOrientation Or Not TopSuite Or Not TopReport Or Not TopEntete _
Or Not TopPied Or Not TopNom Or Not TopDefEntete Then

    '-> Erreur de d�finition de l'entete
    Analyse_Entete = False
    Set aMaq = Nothing
    Exit Function
    
End If

'-> Analyse de la version
If Not TopVersion Then MaqVersion = 1

'-> Pointer sur les messprog
Set aLb = Libelles("FRMPROP")

'-> Gestion de la compatibilit� avec les maquettes de la version 1.00
If MaqVersion = 1 And Orientation = 0 Then
    '-> Dans le cas de l'orientation paysage, il faut inverser la largeur et la hauteur _
    car elle sera r�ajust�e au moment de l'affichage dans la page de propri�t�
    '-> Indiquer que la maquette est en portrait car les valeurs sont invers�s si la _
    maquette est en orientation paysage
    aMaq.Orientation = aLb.GetCaption(12)
    Tempo = aMaq.Largeur
    aMaq.Largeur = aMaq.Hauteur
    aMaq.Hauteur = Tempo
End If

'-> Gestion du libell� de la maquette
If Orientation = 0 Then
    aMaq.Orientation = aLb.GetCaption(13)
Else
    aMaq.Orientation = aLb.GetCaption(12)
End If
Set aLb = Nothing

'-> Gestion des pages d'impression
aMaq.PageGarde = TopPageGarde
aMaq.PageSelection = TopPageSelection

'-> Ajouter la maquette dans la collection
Maquettes.Add aMaq, "MAQUETTE"

'-> Cr�ation dans le treeView
aMaq.AddTreeView

'-> Faire de la maquette l'objet en cours
Set ObjCours = aMaq

'-> Indiquer que la proc�dure a r�ussi
Analyse_Entete = True

Set aMaq = Nothing
Set aLb = Nothing

Exit Function

ErrorEntete:

    MsgBox "Erreur"
    Analyse_Entete = False
    Set aMaq = Nothing
    Exit Function
    Resume


End Function


Private Function CreateSection(ByVal MaqFic As Integer, ByVal NomSection As String, ByVal aMaq As Maquette) As Boolean

'---> Cette fonction est charg�e de cr�er une section

Dim aSection As New Section
Dim Ligne As String
Dim TopRang As Boolean
Dim DefLigne As String
Dim ValueLigne As String
Dim Tempo
Dim aForm As New frmSection
Dim ListeField As String
Dim CreatCreateSectioneRang As Boolean

On Error GoTo ErrorOpen

NomSection = Trim(NomSection)

'-> Setting du nom
aSection.Nom = NomSection

'-> Valeurs par d�faut pour compatibilite Version 1.00
aSection.AlignementLeft = 1
aSection.AlignementTop = 1
aSection.BackColor = 16777215
aSection.Contour = False
aSection.Largeur = aMaq.Largeur - (aMaq.MargeLeft * 2)

'-> Analyse de l'objet rang
Do While Not EOF(MaqFic)

    '-> Initialiser la variable d'erreur
    ErrorLibelle = "Section de texte : " & NomSection

    Line Input #MaqFic, Ligne
    '-> Afficher dans le temporisateur
    TailleLue = TailleLue + Len(Ligne)
    DrawWait
    
    '-> Ne traiter que les lignes <> ""
    If Trim(Ligne) = "" Then
    Else
        '-> Tester le premier caract�re pour voir sil'on a chang� d'objet
        If Mid$(Ligne, 1, 1) = "[" Then
            '-> On change d'objet : g�n�rer une erreur
            ErrorLibelle = "Impossible de trouver le marqueur de fin de section : " & aSection.Nom
            GoTo ErrorOpen
        End If
        '-> R�cup�rer les valeurs
        DefLigne = UCase$(Entry(1, Ligne, "�"))
        ValueLigne = Entry(2, Ligne, "�")
        '-> Analyse
        Select Case DefLigne
            Case "\CADRE", "\BMP"
                '-> Veut dire que l'on attaque la d�finition d'un autre objet sans avoir finie celle-la
                ErrorLibelle = "Impossible de trouver le marqueur de fin de section : " & aSection.Nom
                GoTo ErrorOpen
            Case "\LIGCHAR"
                
            Case "\CONTOUR"
                If CInt(ValueLigne) = 0 Then
                    aSection.Contour = False
                Else
                    aSection.Contour = True
                End If
            Case "\HAUTEUR"
                aSection.Hauteur = CSng(Convert(ValueLigne))
            Case "\LARGEUR"
                aSection.Largeur = CSng(Convert(ValueLigne))
            Case "\ALIGNEMENTTOP"
                aSection.AlignementTop = CInt(Convert(ValueLigne))
            Case "\TOP"
                aSection.Top = CSng(Convert(ValueLigne))
            Case "\ALIGNEMENTLEFT"
                aSection.AlignementLeft = CInt(Convert(ValueLigne))
            Case "\LEFT"
                aSection.Left = CSng(Convert(ValueLigne))
            Case "\ALIGNVERTICAL" 'Compatibilit� version 1.00
                Tempo = CInt(ValueLigne)
                Select Case Tempo
                    Case 0 'Libre
                        aSection.AlignementTop = 1
                        aSection.Top = 0
                    Case 1 'Haut de page
                        aSection.AlignementTop = 5
                        aSection.Top = 0
                    Case 2 'Bas de page
                        aSection.AlignementTop = 5
                        aSection.Top = aMaq.Hauteur - aSection.Hauteur
                    Case 3 'Marge haut
                        aSection.AlignementTop = 2
                        aSection.Top = 0
                    Case 4 'Marge Bas
                        aSection.AlignementTop = 4
                        aSection.Top = 0
                End Select
            Case "\BACKCOLOR"
                aSection.BackColor = CLng(ValueLigne)
            Case "\CHAMPS"
                ListeField = ValueLigne
            Case "\RGCHAR" 'Mapping version 2.00
                aSection.RangChar = ValueLigne
            Case "\TEXTE"
                '-> Fin du descriptif
                If UCase$(ValueLigne) = "END" Then
                    TopRang = True
                    Exit Do
                End If
        End Select
    End If 'Si ligne � blanc
Loop 'Boucle principale

'-> Test des flags
If Not TopRang Then GoTo ErrorOpen
   
'-> Cr�ation de l'objet rang
aSection.IdAffichage = aMaq.AddOrdreAffichage("SCT-" & UCase$(NomSection))
aSection.AddTreeView
aMaq.Sections.Add aSection, UCase$(NomSection)

'-> Gestion des Champs
AddField ListeField, aSection

'-> Cr�er la feuille associ�e
Set aForm.SectionCours = aSection
aForm.Tag = UCase$(NomSection)
aForm.RangText.Tag = "SCT-" & UCase$(NomSection)
aMaq.frmSections.Add aForm, UCase$(NomSection)
On Error Resume Next
aForm.Width = frmNaviga.ScaleX(aSection.Largeur + 2, 7, 1)
On Error GoTo ErrorOpen
'-> Appliquer les propri�t�s
aForm.RangText.BackColor = aSection.BackColor
If aSection.Contour Then
    aForm.RangText.BorderStyle = rtfFixedSingle
Else
    aForm.RangText.BorderStyle = rtfNoBorder
End If

'-> Pointeur de fin de rang
TopRang = False

'-> Analyse des diff�rents objets contenus dans la section
Do While Not EOF(MaqFic)

    '-> Lecture de la ligne
    Line Input #MaqFic, Ligne
    '-> Afficher dans le temporisateur
    TailleLue = TailleLue + Len(Ligne)
    DrawWait

    If Trim(Ligne) = "" Then
    Else
        If Mid$(Ligne, 1, 1) = "\" Then
            '-> R�cup�rer les valeurs
            DefLigne = UCase$(Entry(1, Ligne, "�"))
            ValueLigne = Entry(2, Ligne, "�")
            '-> Analyse
            Select Case DefLigne
                Case "\CADRE"
                    If Not (CreateCadre(MaqFic, ValueLigne, aMaq, aSection, aForm)) Then
                        CreateSection = False
                        GoTo ErrorOpen
                    End If
                Case "\BMP"
                    If Not (CreateBmp(MaqFic, ValueLigne, aMaq, aSection, aForm)) Then
                        CreatCreateSectioneRang = False
                        GoTo ErrorOpen
                    End If
                Case "\RANG"
                    '-> Fin de la cr�ation d'une rang : sortir de la boucle
                    TopRang = True
                    Exit Do
            End Select 'Selon l'analyse de la ligne
        ElseIf Mid$(Ligne, 1, 1) = "[" Then
            '-> Si on trouve un [ cela veut dire que l'on attaque la d�finition d'un autre _
            objet sans avoir termin� celui en cours
            Exit Do
        End If 'Si premier caract�re = "\"
    End If 'Si ligne = ""
Loop 'analyse des objets

'-> V�rifier que l'on a trouv� le top de fin de rang
If Not TopRang Then
    ErrorLibelle = "Impossible de trouver le marqueur de fin de rang : " & aSection.Nom
    GoTo ErrorOpen
End If

'-> Lib�rer les pointeurs
Set aSection = Nothing

'-> Retourner la valeur de succ�s
CreateSection = True

'-> Masquer la feuille
aForm.Visible = False

Set aSection = Nothing
Set aForm = Nothing
Set aMaq = Nothing

'-> Sortir
Exit Function

ErrorOpen:

    CreateSection = False
    Set aSection = Nothing
    Set aForm = Nothing
    Set aMaq = Nothing
        
End Function
Private Sub AddField(ListeField As String, aObject As Object)

Dim DefField As String
Dim aField As Field
Dim i As Integer

On Error Resume Next

'-> Gestion des champs
If ListeField <> "" Then
    For i = 1 To NumEntries(ListeField, "|")
        DefField = Entry(i, ListeField, "|")
        Set aField = New Field
        aField.Name = Entry(1, DefField, ";")
        If IsNumeric(Entry(2, DefField, ";")) Then
            aField.FormatEdit = CInt(Entry(2, DefField, ";"))
        Else
            aField.FormatEdit = 50
        End If
        aField.Present = True
        aObject.Fields.Add aField, UCase$(aField.Name)
    Next
End If

End Sub


Private Function CreateCadre(ByVal MaqFic As Integer, ByVal NomCadre As String, ByVal aMaq As Maquette, ByVal aSection As Section, ByVal aForm As frmSection) As Boolean

'---> Fonction qui cr�er un cadre

Dim Ligne As String
Dim aCadre As New Cadre
Dim DefLigne As String
Dim ValueLigne As String
Dim TopFinCadre As Boolean
Dim pHaut As Boolean, pBas As Boolean, pGauche As Boolean, pDroite As Boolean
Dim RtfValue As String
Dim ListeField As String

Dim TempFile As String
Dim hdlTemp As Integer

ErrorLibelle = "Cr�ation du cadre : " & NomCadre

'-> Setting du nom
NomCadre = Trim(NomCadre)
aCadre.Nom = NomCadre

On Error GoTo ErrorOpen

Do While Not EOF(MaqFic)

    '-> Lecture de la ligne
    Line Input #MaqFic, Ligne
    '-> Afficher dans le temporisateur
    TailleLue = TailleLue + Len(Ligne)
    DrawWait

    If Trim(Ligne) = "" Then
    Else
        If Mid$(Ligne, 1, 1) = "\" Then
            '-> R�cup�rer les valeurs
            DefLigne = UCase$(Entry(1, Ligne, "�"))
            ValueLigne = Entry(2, Ligne, "�")
            '-> Analyse
            Select Case DefLigne
                Case "\LEFT"
                    aCadre.Left = CSng(Convert(ValueLigne))
                    '-> attention : dans la version 1 les dimensions sont enregistr�es avec les marges
                    If MaqVersion = 1 Then aCadre.Left = aCadre.Left - aMaq.MargeLeft
                Case "\TOP"
                    aCadre.Top = CSng(Convert(ValueLigne))
                Case "\DISTANCE" 'Equivalent � MargeInterne
                    '-> Attention en version 1.00 le format est stock� en mm
                    If MaqVersion = 1 Then
                        aCadre.MargeInterne = CSng(Convert(ValueLigne)) / 10
                    Else
                        aCadre.MargeInterne = CSng(Convert(ValueLigne))
                    End If
                Case "\HAUTEUR"
                    aCadre.Hauteur = CSng(Convert(ValueLigne))
                Case "\LARGEUR"
                    aCadre.Largeur = CSng(Convert(ValueLigne))
                Case "\LARGEURBORDURE"
                    aCadre.LargeurBordure = CInt(Convert(ValueLigne))
                Case "\ROUNDRECT"
                    aCadre.IsRoundRect = CInt(Convert(ValueLigne))
                Case "\CONTOUR"
                    '-> Bordure haut
                    If Entry(1, ValueLigne, ",") = "1" Then
                        pHaut = True
                    Else
                        pHaut = False
                    End If
                    '-> Bordure bas
                    If Entry(2, ValueLigne, ",") = "1" Then
                        pBas = True
                    Else
                        pBas = False
                    End If
                    '-> Bordure gauche
                    If Entry(3, ValueLigne, ",") = "1" Then
                        pGauche = True
                    Else
                        pGauche = False
                    End If
                    '-> Bordure droite
                    If Entry(4, ValueLigne, ",") = "1" Then
                        pDroite = True
                    Else
                        pDroite = False
                    End If
                Case "\LOCKED" '
                    aCadre.Locked = CInt(ValueLigne)
                Case "\MASTERALIGN"
                    aCadre.MasterAlign = ValueLigne
                Case "\MASTERINDEXALIGN"
                    aCadre.MasterIndexAlign = CInt(ValueLigne)
                Case "\SLAVEALIGN"
                    aCadre.SlaveAlign = ValueLigne
                Case "\ALIGNX"
                    aCadre.AlignX = CSng(Convert(ValueLigne))
                Case "\ALIGNY"
                    aCadre.AlignY = CSng(Convert(ValueLigne))
                Case "\COULEUR"
                    aCadre.BackColor = CLng(ValueLigne)
                Case "\CHAMPS"
                    ListeField = ValueLigne
                Case "\CADRE"
                    If UCase$(ValueLigne) = "END" Then
                        TopFinCadre = True
                        Exit Do
                    End If
            End Select 'Selon la ligne
        End If 'Si commence par un \
    End If 'Si ligne = ""
Loop 'Boucle principale

'-> Tester la fin de la d�finition de l'objet cadre
If Not TopFinCadre Then
    CreateCadre = False
    Exit Function
End If

'-> Index de l'ordre d'affichage
aCadre.IdOrdreAffichage = aSection.AddOrdreAffichage("CDR-" & aCadre.Nom)

'-> Nom de la section
aCadre.SectionName = aSection.Nom

'-> Ajout dans le treeview
aCadre.AddTreeView

'-> Ajouter le nouvel objet dans la collection
aSection.Cadres.Add aCadre, UCase$(aCadre.Nom)

'-> Gestion des objets champs
AddField ListeField, aCadre

'-> Cr�er la repr�sentation physique du cadre
If Not (CreatePhysiqueCadre(aCadre, aForm, aSection)) Then
    CreateCadre = False
    Exit Function
End If

'-> Maintenant que la repr�sentation physique du cadre est faite on fait le setting des bordures
aCadre.BordureBas = pBas
aCadre.BordureHaut = pHaut
aCadre.BordureGauche = pGauche
aCadre.BordureDroite = pDroite

If aCadre.IsRoundRect Then aCadre.SetRoundRect

'-> Analyse du contenu du cadre
Do While Not EOF(MaqFic)
    '-> Lecture de la ligne
    Line Input #MaqFic, Ligne
    '-> Afficher dans le temporisateur
    TailleLue = TailleLue + Len(Ligne)
    DrawWait

    If Trim(Ligne) = "" Then
    Else
        If Mid$(Ligne, 1, 1) = "\" Then
            DefLigne = Entry(1, Ligne, "�")
            ValueLigne = Entry(2, Ligne, "�")
            If UCase$(DefLigne) = "\CADRERTF" Then
                '-> Ouverture d'un fichier Tempo pour y stocker le texte RTF
                TempFile = GetTempFileNameVB("RTF")
                hdlTemp = FreeFile
                Open TempFile For Output As #hdlTemp
                '-> Analyse jusqu'� ce que l'on trouve la cl� de fin de ligne
                Do While Not EOF(MaqFic)
                    Line Input #MaqFic, Ligne
                    '-> Afficher dans le temporisateur
                    TailleLue = TailleLue + Len(Ligne)
                    DrawWait
                                                        
                    If Trim(Ligne) = "" Then
                    Else
                        If UCase$(Ligne) = "\CADRERTF�END" Then
                            '-> Fermer le fichier tempo
                            Close #hdlTemp
                            '-> Charger son contenu dans le cadre
                            '-> Assigner le contenu au cadre
                            'aForm.CadreTexte(aCadre.IdAffichage).TextRTF = RtfValue
                            aForm.CadreTexte(aCadre.IdAffichage).LoadFile TempFile
                            '-> Supprimer le fichier tempo
                            If Dir$(TempFile, vbNormal) <> "" Then Kill TempFile
                            '-> Quitter la fonction
                            CreateCadre = True
                            Exit Function
                            Exit Do
                        Else
                            '-> Imprimer la ligne RTF dans le fichier TEMPO
                            Print #hdlTemp, Ligne
                            'RtfValue = RtfValue & Ligne
                        End If
                    End If
                Loop 'Boucle d'analyse du Rtf
            End If 'Si d�but d'analyse RTf
        End If 'Si ligne commence par un \
    End If 'Si ligne = ""
Loop 'Boucle d'analyse du cadre

'-> Si le programme passe ici, c'est qu'il y a eu un probleme
CreateCadre = False
ErrorLibelle = "Erreur sur le contenu Rtf du cadre : " & aCadre.Nom


Set aMaq = Nothing
Set aCadre = Nothing
Set aForm = Nothing
Set aSection = Nothing

Exit Function

ErrorOpen:
    
    CreateCadre = False
    ErrorLibelle = "Erreur non g�r�e"
    
    Set aMaq = Nothing
    Set aCadre = Nothing
    Set aForm = Nothing
    Set aSection = Nothing

    
End Function

Public Function CreatePhysiqueCadre(ByVal aCadre As Cadre, ByVal aForm As frmSection, ByVal aSection As Section) As Boolean

'---> Fonction qui cr�er la repr�sentation physique d'un cadre en fonction de l'objet _
cadre rattach�

On Error GoTo ErrorCreate

Dim nCadre As Integer

'-> Initialiser le compteur
nCadre = aSection.nCadre

'-> Chargement des repr�sentations physiques
Load aForm.Cadre(nCadre)
Load aForm.CadreTexte(nCadre)

'-> Changer les fen�tres parents
Set aForm.CadreTexte(nCadre).Container = aForm.Cadre(nCadre)
Set aForm.Cadre(nCadre).Container = aForm.RangText.Container

'-> Setting des propri�t�s
aForm.CadreTexte(nCadre).BackColor = aCadre.BackColor
aForm.Cadre(nCadre).BackColor = aCadre.BackColor
aForm.Cadre(nCadre).DrawWidth = aCadre.LargeurBordure

'-> On met dans la propri�t� cadre le nom de l'objet pour y acc�der
aForm.Cadre(nCadre).Tag = "CDR-" & aCadre.Nom
aForm.CadreTexte(nCadre).Tag = "CDR-" & aCadre.Nom

'-> Setting des dimensions
aForm.Cadre(nCadre).Height = aForm.ScaleY(aCadre.Hauteur, 7, 1)
aForm.Cadre(nCadre).Width = aForm.ScaleX(aCadre.Largeur, 7, 1)
aForm.Cadre(nCadre).Top = aForm.ScaleY(aCadre.Top, 7, 1)
aForm.Cadre(nCadre).Left = aForm.ScaleX(aCadre.Left, 7, 1)
aForm.CadreTexte(nCadre).Height = aForm.ScaleY((aCadre.Hauteur - aCadre.MargeInterne * 2), 7, 3)
aForm.CadreTexte(nCadre).Width = aForm.ScaleX((aCadre.Largeur - aCadre.MargeInterne * 2), 7, 3)
aForm.CadreTexte(nCadre).Top = aForm.ScaleY(aCadre.MargeInterne, 7, 3)
aForm.CadreTexte(nCadre).Left = aForm.ScaleX(aCadre.MargeInterne, 7, 3)

'-> Ajouter les ToolTips
aForm.Cadre(nCadre).ToolTipText = aCadre.Nom

'-> Rendre les objets visibles
aForm.Cadre(nCadre).Visible = True
aForm.CadreTexte(nCadre).Visible = True

'-> Placer au dessus
aForm.Cadre(nCadre).ZOrder

'-> Sauvegarder l'index de la repr�sentation dans aFrom
aCadre.IdAffichage = nCadre

'-> Sauvegarder le pictureBox associ�
Set aCadre.pBox = aForm.Cadre(nCadre)

'-> Incr�menter l'index du prochain cadre
aSection.nCadre = nCadre + 1

'-> Renvoyer une valeur de succ�s
CreatePhysiqueCadre = True

Set aCadre = Nothing
Set aForm = Nothing
Set aSection = Nothing

'-> Quitter la fonction
Exit Function

ErrorCreate:

    CreatePhysiqueCadre = False
    ErrorLibelle = "Erreur durant la cr�ation du cadre : " & aCadre.Nom
    
    Set aCadre = Nothing
    Set aForm = Nothing
    Set aSection = Nothing


End Function

Private Function CreateBmp(ByVal MaqFic As Integer, ByVal NomBmp As String, ByVal aMaq As Maquette, ByVal aSection As Section, ByVal aForm As frmSection) As Boolean

'---> Cr�ation d'un BMP
Dim Ligne As String
Dim TopFinBmp As Boolean
Dim aBmp As New ImageObj
Dim DefLigne As String
Dim ValueLigne As String
Dim FindEditFormat As Boolean

On Error GoTo ErrorOpen

'-> Initialiser la variable d'erreur
ErrorLibelle = "Erreur dans la cr�ation du Bmp : " & NomBmp

NomBmp = Trim(NomBmp)

'-> Setting du nom
aBmp.Nom = NomBmp

'-> Analyse des cl�s
Do While Not EOF(MaqFic)

    '-> Lecture de la ligne
    Line Input #MaqFic, Ligne
    '-> Afficher dans le temporisateur
    TailleLue = TailleLue + Len(Ligne)
    DrawWait

    If Trim(Ligne) = "" Then
    Else
        If Mid$(Ligne, 1, 1) = "\" Then
            '-> R�cup�rer les valeurs
            DefLigne = Entry(1, Ligne, "�")
            ValueLigne = Entry(2, Ligne, "�")
            '-> Analyse de la valeur
            Select Case UCase$(DefLigne)
                Case "\LEFT"
                    aBmp.Left = CSng(Convert(ValueLigne))
                    '-> attention : dans la version 1 les dimensions sont enregistr�es avec les marges
                    If MaqVersion = 1 Then aBmp.Left = aBmp.Left - aMaq.MargeLeft
                Case "\TOP"
                    aBmp.Top = CSng(Convert(ValueLigne))
                Case "\LARGEUR"
                    aBmp.Largeur = CSng(Convert(ValueLigne))
                Case "\HAUTEUR"
                    aBmp.Hauteur = CSng(Convert(ValueLigne))
                Case "\FICHIER"
                    aBmp.Fichier = ValueLigne
                Case "\PATH"
                    aBmp.Path = ValueLigne
                Case "\CONTOURBMP"
                    If CInt(ValueLigne) = 1 Then
                        aBmp.Contour = True
                    Else
                        aBmp.Contour = False
                    End If
                Case "\VARIABLE"
                    If MaqVersion = 1 Then
                        If UCase$(ValueLigne) = "FAUX" Then
                            aBmp.isVariable = False
                        Else
                            aBmp.isVariable = True
                        End If
                    Else
                        If CInt(ValueLigne) = 1 Then
                            aBmp.isVariable = True
                        Else
                            aBmp.isVariable = False
                        End If
                    End If
                Case "\USEASSOCIATION"
                    If CInt(ValueLigne) = 1 Then
                        aBmp.UseAssociation = True
                    Else
                        aBmp.UseAssociation = False
                    End If
                Case "\EDITFORMAT"
                    aBmp.FormatEdit = CInt(ValueLigne)
                    FindEditFormat = True
                Case "\LOCKED"
                    If CInt(ValueLigne) = 1 Then
                        aBmp.Locked = True
                    Else
                        aBmp.Locked = False
                    End If
                Case "\MASTERALIGN"
                    aBmp.MasterAlign = ValueLigne
                Case "\MASTERINDEXALIGN"
                    aBmp.MasterIndexAlign = CInt(ValueLigne)
                Case "\SLAVEALIGN"
                    aBmp.SlaveAlign = ValueLigne
                Case "\ALIGNX"
                    aBmp.AlignX = CSng(Convert(ValueLigne))
                Case "\ALIGNY"
                    aBmp.AlignY = CSng(Convert(ValueLigne))
                Case "\BMP"
                    If UCase$(ValueLigne) = "END" Then
                        TopFinBmp = True
                        Exit Do
                    End If
            End Select 'Selon l'analyse de la ligne
        End If 'Si premier caract�re = "\"
    End If 'Si ligne � blanc
Loop 'Boucle d'analyse g�n�rale

'-> Tester le top de formattage du bmp
If Not TopFinBmp Then
    ErrorLibelle = "Impossible de trouver le marqueur de fin du BMP : " & NomBmp
    CreateBmp = False
    Exit Function
End If

'-> Gestion du champ
If Not FindEditFormat Then aBmp.FormatEdit = 50

'-> Setting des infos
aBmp.IdOrdreAffichage = aSection.AddOrdreAffichage("BMP-" & UCase$(NomBmp))
aBmp.SectionName = aSection.Nom

'-> Ajout dans le treeview
aBmp.AddTreeView

'-> Ajout de l'objet dans la collection
aSection.Bmps.Add aBmp, UCase$(NomBmp)

'-> Cr�ation de la repr�sentatio physique
If Not (CreatePhysiqueBmp(aBmp, aForm, aSection)) Then
    CreateBmp = False
    Exit Function
End If

'-> Retourner une valeur de succes
CreateBmp = True

Set aMaq = Nothing
Set aForm = Nothing
Set aBmp = Nothing
Set aSection = Nothing


Exit Function

ErrorOpen:

    CreateBmp = False
    Set aMaq = Nothing
    Set aForm = Nothing
    Set aBmp = Nothing
    Set aSection = Nothing

    
End Function

Public Function CreatePhysiqueBmp(aBmp As ImageObj, aForm As frmSection, ByVal aSection As Section) As Boolean

'---> Fonction qui cr�er la repr�sentation physique d'un bmp en fonction de l'objet _
imageobj rattach�

On Error GoTo ErrorCreate

Dim nBmp As Integer
Dim BmpFile As String

'-> Initialiser le compteur
nBmp = aSection.nBmp

'-> Chargement de la repr�sentation physique
Load aForm.BitMap(nBmp)

'-> Contour du Bmp
If aBmp.Contour Then
    aForm.BitMap(nBmp).BorderStyle = 1
Else
    aForm.BitMap(nBmp).BorderStyle = 0
End If

'-> ToolTip
aForm.BitMap(nBmp).ToolTipText = aBmp.Nom

'-> Stocker le nom de l'objet dans le Bmp
aForm.BitMap(nBmp).Tag = "BMP-" & aBmp.Nom

'-> Donner ses dimensions au Bmp
aForm.BitMap(nBmp).Height = aForm.ScaleX(aBmp.Hauteur, 7, 1)
aForm.BitMap(nBmp).Width = aForm.ScaleY(aBmp.Largeur, 7, 1)
aForm.BitMap(nBmp).AutoSize = False

'-> Charger le fichier associ�
If Not aBmp.isVariable Then
    '-> Anlayse dans l'ordre des paths
    BmpFile = V6SearchPath(aBmp.Fichier)
    If BmpFile <> "" Then
        '-> Charger le fichier
        aForm.BitMap(nBmp).Picture = LoadPicture(BmpFile)
    End If
End If

'-> Positionner l'objet
aForm.BitMap(nBmp).Top = aForm.ScaleY(aBmp.Top, 7, 1)
aForm.BitMap(nBmp).Left = aForm.ScaleX(aBmp.Left, 7, 1)

'-> Changer le container
Set aForm.BitMap(nBmp).Container = aForm.RangText.Container

'-> Rendre l'objet visible
aForm.BitMap(nBmp).Visible = True

'-> Sauvegarder l'index du pictureBox associ�
aBmp.IdAffichage = nBmp

'-> Incr�menter le compteur
aSection.nBmp = nBmp + 1

'-> Placer dessus
aForm.BitMap(nBmp - 1).ZOrder

'-> Renvoyer une valeur de succ�s
CreatePhysiqueBmp = True

Set aSection = Nothing
Set aForm = Nothing

'-> Quitter la fonction
Exit Function

ErrorCreate:

    CreatePhysiqueBmp = False
    ErrorLibelle = "Erreur durant la cr�ation du bmp : " & aBmp.Nom
    
    Set aSection = Nothing
    Set aForm = Nothing
    Set aBmp = Nothing


End Function

Private Function CreateSectionRtf(ByVal MaqFic As Integer, ByVal aMaq As Maquette, ByVal NomSection As String) As Boolean

'---> Cette fonction a pour but de r�cup�rer la valeur Rtf d'une section
Dim Ligne As String
Dim TopFinRtf As Boolean
Dim aForm As frmSection
Dim ValueRtf As String
Dim TempFileName As String
Dim hdlFile As Integer

On Error GoTo ErrorRtf

'-> Setting du libelle de l'erreur
ErrorLibelle = "Erreur dans le chargement du code RTF de la section : " & NomSection

'-> Cr�er un fichier tempo
TempFileName = GetTempFileNameVB("RTF")
hdlFile = FreeFile
Open TempFileName For Output As #hdlFile
            

Do While Not EOF(MaqFic)

    '-> Initialiser la variable d'erreur
    ErrorLibelle = "analyse du code Rtf de la section : " & NomSection
    Line Input #MaqFic, Ligne
    '-> Afficher dans le temporisateur
    TailleLue = TailleLue + Len(Ligne)
    DrawWait

    '-> Ne traiter que les lignes <> ""
    If Trim(Ligne) = "" Then
    Else
        '-> Analyse
        If UCase$(Ligne) = "\RTF�END" Then
            '-> Fermer le fichier tempo
            Close #hdlFile
            '-> Pointer sur le conteneur de premier niveau
            Set aForm = aMaq.frmSections(UCase$(NomSection))
            '-> Charger le fichier tempo
            aForm.RangText.LoadFile TempFileName
            '-> Supprimer le fichier Tempo
            If Dir$(TempFileName, vbNormal) <> "" Then Kill TempFileName
            TopFinRtf = True
            Exit Do
        Else
            '-> Imprimer le contenu RTf dans le fichier TEMPO
            Print #hdlFile, Ligne
            DoEvents
        End If
    End If
Loop
        
'-> tester le formattage des sections
If Not TopFinRtf Then
    CreateSectionRtf = False
    Exit Function
End If

'-> Renvoyer une valeur de succ�s
CreateSectionRtf = True

Set aMaq = Nothing
Set aForm = Nothing

'-> Sortir
Exit Function
        
ErrorRtf:

    MsgBox Err.Number & "  " & Err.Description

    CreateSectionRtf = False
    
    Set aMaq = Nothing
    Set aForm = Nothing

    
End Function


Private Function CreateTableau(ByVal MaqFic As Integer, ByVal NomTableau As String, ByVal aMaq As Maquette) As Boolean

'---> Cette fonction est charg�e de lire et decr�er un tableau
Dim Ligne As String
Dim DefLigne As String
Dim ValueLigne As String
Dim aTb As New Tableau
Dim aLb As Libelle
Dim aForm As frmTableur
Dim TopFin As Boolean

On Error GoTo ErrorOpen

NomTableau = Trim(NomTableau)

'-> Positonner le libelle erreur
ErrorLibelle = "Cr�ation du tableau en cours : " & NomTableau

'-> Nom du tableau
aTb.Nom = NomTableau

Do While Not EOF(MaqFic)

    '-> Lecture de la ligne
    Line Input #MaqFic, Ligne
    '-> Afficher dans le temporisateur
    TailleLue = TailleLue + Len(Ligne)
    DrawWait

    If Trim(Ligne) = "" Then
    Else
        If Mid$(Ligne, 1, 1) = "\" Then
            '-> Lecture des valeurs
            DefLigne = Entry(1, Ligne, "�")
            ValueLigne = Entry(2, Ligne, "�")
            Select Case UCase$(DefLigne)
                Case "\LARGEURTB"
                    aTb.Largeur = CSng(Convert(ValueLigne))
                Case "\ORIENTATIONTB"
                   Set aLb = Libelles("FRMPROP")
                    If CInt(ValueLigne) = 1 Then 'Portrait
                        aTb.Orientation = aLb.GetCaption(12)
                    Else
                        aTb.Orientation = aLb.GetCaption(13)
                    End If
                    Set aLb = Nothing
                    '-> Quitter la boucle d'analyse
                    Exit Do
            End Select 'Selon la nature de a ligne
        '-> Tester le premier caract�re pour voir sil'on a chang� d'objet
        ElseIf Mid$(Ligne, 1, 1) = "[" Then
            '-> On change d'objet : g�n�rer une erreur
            ErrorLibelle = "Impossible de trouver le marqueur de fin de tableau : " & aTb.Nom
            GoTo ErrorOpen
        End If 'Si ligne commence par "\"
    End If 'Si ligne = ""
Loop 'Boucle d'analyse principale

'-> Cr�ation de l'objet tableau
aTb.IdAffichage = aMaq.AddOrdreAffichage("TB-" & UCase$(NomTableau))

'-> Ajout dans la collection
aMaq.Tableaux.Add aTb, UCase$(NomTableau)

'-> initalisation de la feuille
Set aForm = New frmTableur


'-> Ajouter dans la collection des feuilles
aMaq.frmTableurs.Add aForm, UCase$(NomTableau)

'-> Cr�ation de l'icone associ�e au tableau
aTb.AddTreeView

'-> Setting des variables objets du tableau en cours
Set aForm.TableauCours = aTb
Set ObjCours = aTb
aForm.Tag = UCase$(NomTableau)
aForm.Show

'-> Cr�ation des blocks rattach�s
Do While Not EOF(MaqFic)

    '-> Lecture de la ligne
    Line Input #MaqFic, Ligne
    '-> Afficher dans le temporisateur
    TailleLue = TailleLue + Len(Ligne)
    DrawWait

    If Trim(Ligne) = "" Then
    Else
        If Mid$(Ligne, 1, 1) = "\" Then
            '-> Lecture des valeurs
            DefLigne = Entry(1, Ligne, "�")
            ValueLigne = Entry(2, Ligne, "�")
            Select Case UCase$(DefLigne)
                Case "\BEGIN"
                    If Not CreateBlock(MaqFic, aTb, ValueLigne) Then
                        CreateTableau = False
                        Exit Function
                    End If
                Case "\TABLEAU"
                    If UCase$(ValueLigne) = "END" Then
                        '-> Fin du tableau : On le dessine
                        DrawTableau aTb
                        '-> Indiquer que l'on a trouv� le marqueur de fin de tableau
                        TopFin = True
                        Exit Do
                    Else
                        CreateTableau = False
                        ErrorLibelle = "Marqueur de fin de tableau invalide"
                        Exit Function
                    End If
            End Select 'Selon la ligne
        '-> Tester le premier caract�re pour voir sil'on a chang� d'objet
        ElseIf Mid$(Ligne, 1, 1) = "[" Then
            '-> On change d'objet : g�n�rer une erreur
            ErrorLibelle = "Impossible de trouver le marqueur de fin de tableau : " & aTb.Nom
            GoTo ErrorOpen
        End If 'Si premi�re ligne = "\"
    End If 'Si ligne � blanc
Loop 'Boucle d'analyse principale

'-> Test du top de fin
If Not TopFin Then
    '-> Positionner le libell� d'erreur
    ErrorLibelle = "Impossible de touver le marqueur de fin de tableau : " & aTb.Nom
    GoTo ErrorOpen
End If

'-> Gestion des Icones dans le tableau
aForm.GestIco

'-> Masquer la feuille du tableau
aForm.Visible = False

'-> Liberrer le pointeur
Set aForm = Nothing
Set aMaq = Nothing
Set aTb = Nothing
Set aLb = Nothing

'-> Renvoyer une valeur de succ�s
CreateTableau = True

Exit Function

ErrorOpen:

    CreateTableau = False
    '-> Liberrer le pointeur
    Set aForm = Nothing
    Set aMaq = Nothing
    Set aTb = Nothing
    Set aLb = Nothing

    MsgBox "OK"

End Function

Private Function CreateBlock(ByVal MaqFic As Integer, ByVal aTb As Tableau, ByVal NomBlock As String) As Boolean

'---> Fonction qui cr�� un block de ligne te qui l'ajoute au tableau en cours
Dim Ligne As String
Dim DefLigne As String
Dim ValueLigne As String
Dim aBlock As Block
Dim DefCellule As String
Dim sLigne() As String
Dim sCol() As String
Dim LigLue As Integer
Dim TopFinBlock As Boolean
Dim aForm As FrmBlock
Dim aCell As Cellule
Dim i As Integer, j As Integer, k As Integer, l As Integer
Dim DefBordure As String
Dim ListeField As String
Dim DefField1 As String
Dim DefField2 As String
Dim aField As Field
Dim ListeVar As String
Dim ListeExcel As String
Dim AccesDetail As String
Dim ListeCell As String

On Error GoTo ErrorOpen

'-> Positonner le libelle d'erreur
ErrorLibelle = "Erreur dans la cr�ation d'un block : " & NomBlock

'-> Positionner l'index de la prochaine ligne lue
LigLue = 1

NomBlock = Trim(NomBlock)

'-> Initialisation du block
Set aBlock = New Block
aBlock.Nom = NomBlock
aBlock.NomTb = aTb.Nom

'-> Valeur de ALigneLeft par d�faut
aBlock.AlignementLeft = 3
aBlock.Left = 0

'-> Analyse
Do While Not EOF(MaqFic)
    '-> Lecture de la ligne
    Line Input #MaqFic, Ligne
    '-> Afficher dans le temporisateur
    TailleLue = TailleLue + Len(Ligne)
    DrawWait

    If Trim(Ligne) = "" Then
    Else
        If Mid$(Ligne, 1, 1) = "\" Then
            '-> Lecture des param�tres
            DefLigne = Entry(1, Ligne, "�")
            ValueLigne = Entry(2, Ligne, "�")
            Select Case UCase$(DefLigne)
                Case "\BEGIN"
                    '-> Cela veut dire que l'on attaque la cr�ation d'un autre block : g�n�rer une erreur
                    ErrorLibelle = "Impossible de trouver le marqueur de fin de block : " & aBlock.Nom
                    GoTo ErrorOpen
                Case "\ALIGNEMENT" 'Garder pour compatibilit� 1.00
                    If CInt(ValueLigne) = 0 Then 'Marge gauche
                        aBlock.AlignementLeft = 2
                    ElseIf CInt(ValueLigne) = 1 Then 'Marge Droite
                        aBlock.AlignementLeft = 4
                    ElseIf CInt(ValueLigne) = 2 Then 'Centr�
                        aBlock.AlignementLeft = 3
                    ElseIf CInt(ValueLigne) = 3 Then 'Alignement Libre
                        aBlock.AlignementLeft = 5 'La valeur Left est aliment�e par la valeur "\DISTANCE"
                    End If
                Case "\ALIGNLEFT" 'Version 2.00 de "\Alignement"
                    aBlock.AlignementLeft = CInt(ValueLigne)
                Case "\LEFT" 'Valeur de AlignLeft quand il est sp�cifi� : valeur 5
                    aBlock.Left = CSng(Convert(ValueLigne))
                Case "\ALIGNEMENTBLOCKVERTICAL" 'Garder pour compatibilit� version 1.00
                    aBlock.Top = CSng(Convert(ValueLigne))
                Case "\ALIGNTOP" 'Version  2.00 de "\AlignementBlockVertical"
                    aBlock.AlignementTop = CInt(ValueLigne)
                Case "\TOP" 'Valeur de AlignTop quand il est sp�cifi� : valeur 5
                    aBlock.Top = CSng(Convert(ValueLigne))
                Case "\POSFIXE" 'Garser uniquement pour compatibilit� version 1.00
                    If CInt(ValueLigne) = 1 Then
                        '-> Positionner l'alignementTop en Sp�cifi�
                        aBlock.AlignementTop = 5
                    Else
                        aBlock.AlignementTop = 1 'Libre
                    End If
                Case "\LARGEUR"
                    aBlock.Largeur = CSng(Convert(ValueLigne))
                Case "\HAUTEUR"
                    aBlock.Hauteur = CSng(Convert(ValueLigne))
                Case "\DISTANCE" 'Garder pour compatibilit� version 1.00
                    aBlock.Left = CSng(Convert(ValueLigne))
                Case "\MASTERLINK" 'Link Version 2.00
                    aBlock.MasterLink = ValueLigne
                Case "\SLAVELINK" 'Link Version 2.00
                    aBlock.SlaveLink = ValueLigne
                Case "\RGCHAR" 'Mapping version 2.00
                    aBlock.RangChar = ValueLigne
                Case "\LIGNE"
                    aBlock.NbLigne = CInt(ValueLigne)
                    ReDim sLigne(1 To CInt(ValueLigne))
                Case "\COLONNE"
                    aBlock.NbCol = CInt(ValueLigne)
                    ReDim sCol(1 To CInt(ValueLigne))
                Case "\VARLIG"
                    ListeVar = ValueLigne
                Case "\EXPORTLIG"
                    ListeExcel = ValueLigne
                Case "\CHAMPS"
                    ListeField = ValueLigne
                Case "\COL"
                    sLigne(LigLue) = Ligne
                    LigLue = LigLue + 1
                Case "\ACCESDET"
                    AccesDetail = ValueLigne
                Case "\END"
                    TopFinBlock = True
                    Exit Do
            End Select 'Selon l'analyse de la ligne
        ElseIf Mid$(Ligne, 1, 1) = "[" Then
            '-> On change d'objet : g�n�rer une erreur
            ErrorLibelle = "Impossible de trouver le marqueur de fin de block : " & aBlock.Nom
            GoTo ErrorOpen
        End If 'Si premier char = "\"
    End If 'Si ligne = ""
Loop 'Boucle d'analyse principale

'-> Analyse du marqueur de fin de block
If Not TopFinBlock Then
    ErrorLibelle = "Impossible de trouver le marqueur de fin de block : " & NomBlock
    CreateBlock = False
    Exit Function
End If

'-> Analyse et cr�ation du block
aBlock.IdOrdreAffichage = aTb.AddOrdreAffichage("BL-" & UCase$(NomBlock))

'-> Ajout du block dans la collection
aTb.Blocks.Add aBlock, "BL-" & UCase$(NomBlock)

'-> Initialiser les colonnes et les lignes
aBlock.Init

'-> Gestion des lignes variables
If Trim(ListeVar) <> "" Then
    For i = 1 To NumEntries(ListeVar, "|")
        aBlock.SetVarLig CInt(Entry(i, ListeVar, "|")), True
    Next
End If

'-> Gestion des exports des lignes vers Excel Attention , cette propri�t� _
ne contient que la liste des lignes non exportables vers EXCEL
If Trim(ListeExcel) <> "" Then
    For i = 1 To NumEntries(ListeExcel, "|")
        aBlock.SetExportExcel CInt(Entry(i, ListeExcel, "|")), False
    Next
End If

'-> Alimenter les Matrices de ligne te de colonne et cr�ation des cellules
For i = 1 To LigLue - 1
    '-> R�cup�ration de la d�finition de la ligne
    DefLigne = sLigne(i)
    '-> Setting de la hauteur de ligne
    aBlock.SetHauteurLig i, CSng(Convert(Entry(2, DefLigne, "�")))
    '-> Recup�rer QUE la lisye des colonnes
    DefLigne = Entry(3, DefLigne, "�")
    For j = 1 To aBlock.NbCol
        '-> Recup de la d�finition de la colonne
        DefCellule = Entry(j, DefLigne, "|")
        '-> Setting de la largeur
        aBlock.SetLargeurCol j, CSng(Convert(Entry(3, DefCellule, ";")))
        '-> Cr�ation de la cellule associ�e
        Set aCell = New Cellule
        aCell.Ligne = i
        aCell.Colonne = j
        '-> Ajout dans le tableau
        aBlock.Cellules.Add aCell, "L" & i & "C" & j
        '-> setting des propri�t�s
        aCell.CellAlign = CInt(Entry(1, DefCellule, ";"))
        If MaqVersion = 1 Then aCell.CellAlign = aCell.CellAlign + 1
        aCell.Contenu = Entry(2, DefCellule, ";")
        '-> Gestion des bordures
        DefBordure = Entry(4, DefCellule, ";")
        If UCase$(Entry(1, DefBordure, ",")) = "VRAI" Then
            aCell.BordureHaut = True
        Else
            aCell.BordureHaut = False
        End If
        If UCase$(Entry(2, DefBordure, ",")) = "VRAI" Then
            aCell.BordureBas = True
        Else
            aCell.BordureBas = False
        End If
        If UCase$(Entry(3, DefBordure, ",")) = "VRAI" Then
            aCell.BordureGauche = True
        Else
            aCell.BordureGauche = False
        End If
        If UCase$(Entry(4, DefBordure, ",")) = "VRAI" Then
            aCell.BordureDroite = True
        Else
            aCell.BordureDroite = False
        End If
        '-> Propri�t�s de Font
        aCell.FontName = Entry(5, DefCellule, ";")
        aCell.FontSize = CSng(Convert(Entry(6, DefCellule, ";")))
        If UCase$(Trim(Entry(7, DefCellule, ";"))) = "VRAI" Then
            aCell.FontBold = True
        Else
            aCell.FontBold = False
        End If
        If UCase$(Trim(Entry(8, DefCellule, ";"))) = "VRAI" Then
            aCell.FontItalic = True
        Else
            aCell.FontItalic = False
        End If
        If UCase$(Trim(Entry(9, DefCellule, ";"))) = "VRAI" Then
            aCell.FontUnderline = True
        Else
            aCell.FontUnderline = False
        End If
        aCell.FontColor = CLng(Entry(10, DefCellule, ";"))
        If CLng(Entry(11, DefCellule, ";")) = 999 Then
            aCell.BackColor = 16777215
            aCell.FondTransParent = True
        Else
            aCell.BackColor = CLng(Entry(11, DefCellule, ";"))
            aCell.FondTransParent = False
        End If
        '-> Propri�t�s version 2.00
        If MaqVersion = 1 Then
        Else
            '-> Format des cellules
            
            '-> AutoAjust
            If UCase$(Entry(12, DefCellule, ";")) = "VRAI" Then
                aCell.AutoAjust = True
            Else
                aCell.AutoAjust = False
            End If
            '-> Format et Type
            DefBordure = Entry(13, DefCellule, ";")
            If Entry(1, DefBordure, "@") <> "" Then aCell.TypeValeur = CInt(Entry(1, DefBordure, "@"))
            aCell.Msk = Entry(2, DefBordure, "@")
            If NumEntries(DefBordure, "@") = 3 Then aCell.PrintZero = True
            
            '-> Export vers Excel Fusion
            If NumEntries(DefCellule, ";") > 13 Then
                '-> R�cup�rer le param�trage
                DefBordure = Entry(14, DefCellule, ";")
                aCell.IsFusion = CBool(Entry(1, DefBordure, "@"))
                aCell.ColFusion = CInt(Entry(2, DefBordure, "@"))
            End If
        End If
        '-> Gestion des champs d'�dition
        DefBordure = MapFields(aCell.Contenu)
        If DefBordure <> "" Then
            '-> Chercher le champ dans la liste des champs
            For k = 1 To NumEntries(DefBordure, "@")
                '-> Champ rechercher
                DefField1 = Entry(k, DefBordure, "@")
                For l = 1 To NumEntries(ListeField, "|")
                    DefField2 = Entry(l, ListeField, "|")
                    If UCase$(DefField1) = UCase$(Entry(1, DefField2, ";")) Then
                        '-> On a trouv� le champ : le cr�er
                        Set aField = New Field
                        aField.Name = DefField1
                        If IsNumeric(Entry(2, DefField2, ";")) Then
                            aField.FormatEdit = CInt(Entry(2, DefField2, ";"))
                        Else
                            aField.FormatEdit = 50
                        End If
                        aCell.Fields.Add aField, UCase$(aField.Name)
                        Exit For
                    End If
                Next 'Pour tous les champs de la cl� "\CHAMPS�
            Next 'Pour tous les champs contenus dans la cellule
        End If 'S'il y a des champs dans la cellule
    Next 'Pour toute les colonnes
Next 'Pour toutes les lignes

'-> Setting des fonctions d'acc�s au d�tail
If Trim(AccesDetail) <> "" Then
    '-> Setting des param�tres du block
    aBlock.UseAccesDet = True
    aBlock.KeyAccesDet = Entry(1, AccesDetail, "�")
    '-> Setting des cellules servant � l'acc�s au d�tail
    ListeCell = Entry(2, AccesDetail, "�")
    For i = 1 To NumEntries(ListeCell, "|")
        '-> Pointeur vers une cellule
        Set aCell = aBlock.Cellules("L" & Entry(1, Entry(i, ListeCell, "|"), "-") & "C" & Entry(2, Entry(i, ListeCell, "|"), "-"))
        '-> Indiquer qu'elle sert d'acc�s au d�tail
        aCell.UseAccesDet = True
    Next 'Pour toutes les d�finitions de cellules
End If 'Si acces ua d�tail de ce block

'-> Cr�ation des dimensions des cellules en pixels
If Not InitBlock(aBlock, aTb.Nom) Then
    CreateBlock = False
    Exit Function
End If

'-> Ajout de la repr�sentation physique
aBlock.AddTreeView aTb.Nom

'-> Liberer les valeurs
Set aForm = Nothing
Set aCell = Nothing
Set aBlock = Nothing
Set aTb = Nothing

'-> Renvoyer une valeur de succ�s
CreateBlock = True

Exit Function


ErrorOpen:
    
    CreateBlock = False
    '-> Liberer les valeurs
    Set aForm = Nothing
    Set aCell = Nothing
    Set aBlock = Nothing
    Set aTb = Nothing


End Function

Private Function InitBlock(ByVal BlockCours As Block, ByVal NomTableau As String) As Boolean

'---> Cette proc�dure initialise les matrices Lignes (), Colonnes () et Cells _
en partant des largeurs de colonnes et hauteur de lignes

Dim i As Integer
Dim j As Integer
Dim NbCol As Integer
Dim NbLig As Integer
'-> Dimensions en CM
Dim HauteurCm As Single
Dim LargeurCm As Single
'-> Convertion en Pixels pour dessin
Dim HauteurPix As Integer
Dim LargeurPix As Integer
Dim hPix As Integer
Dim lPix As Integer
'-> Matrices des lignes et colonnes
Dim Lignes() As Integer
Dim Colonnes() As Integer
Dim Cells() As Long

Dim aCell As Cellule
Dim PosX As Integer
Dim PosY As Integer
Dim Res As Long
Dim hWndRegion As Long

Dim aForm As frmTableur

'On Error GoTo ErrorOpen

'-> Positionner le libell� d'�rreur
ErrorLibelle = "Erreur dans la proc�dure InitBlock : " & BlockCours.Nom

'-> Pointer sur la feuille tableur
Set aForm = Maquettes("MAQUETTE").frmTableurs(UCase$(NomTableau))

'-> R�cup�ration des propri�t�s
NbCol = BlockCours.NbCol
NbLig = BlockCours.NbLigne
HauteurCm = BlockCours.Hauteur
LargeurCm = BlockCours.Largeur

'-> Convertion en pixels
LargeurPix = CInt(aForm.ScaleX(LargeurCm, 7, 3))
HauteurPix = CInt(aForm.ScaleY(HauteurCm, 7, 3))

'-> Affecter la valeur au block
BlockCours.LargeurPix = LargeurPix
BlockCours.HauteurPix = HauteurPix

'-> Calcul des dimensions de base
ReDim Lignes(1 To NbLig)
ReDim Colonnes(1 To NbCol)

'-> Largeur de base des colonnes
For i = 1 To NbCol
    Colonnes(i) = Fix((BlockCours.GetLargeurCol(i) / LargeurCm) * (LargeurPix - (NbCol + 1)))
    lPix = lPix + Colonnes(i)
Next

'-> Hauteur de base des lignes
For i = 1 To NbLig
    Lignes(i) = Fix((BlockCours.GetHauteurLig(i) / HauteurCm) * (HauteurPix - (NbLig + 1)))
    hPix = hPix + Lignes(i)
Next

'-> Effectuer le lissage des colonnes
For i = 1 To Abs((LargeurPix - lPix - (NbCol + 1)))
    If (LargeurPix - lPix - (NbCol + 1)) > 0 Then
        Colonnes(i) = Colonnes(i) + 1
    Else
        Colonnes(i) = Colonnes(i) - 1
    End If
Next

'-> Effectuer le lissage des lignes
For i = 1 To Abs((HauteurPix - hPix - (NbLig + 1)))
    If (HauteurPix - hPix - (NbLig + 1)) > 0 Then
        Lignes(i) = Lignes(i) + 1
    Else
        Lignes(i) = Lignes(i) - 1
    End If
Next

'-> Redimensionner la matrice des cellules
ReDim Cells(1 To 6, 1 To (NbLig * NbCol))

'-> Pixel de d�part
PosY = 1
PosX = 1

For i = 1 To NbCol
    For j = 1 To NbLig
        '-> Mettre � jour les valeurs des cellules
        Set aCell = BlockCours.Cellules("L" & j & "C" & i)
        aCell.X1 = PosX
        aCell.Y1 = PosY
        aCell.X2 = PosX + Colonnes(i) + 1
        aCell.Y2 = PosY + Lignes(j) + 1
        '-> Incr�menter le compteur de position
        PosY = PosY + Lignes(j) + 1
    Next 'pour toutes les lignes
    '-> initialisation des valeurs
    PosX = PosX + Colonnes(i) + 1
    PosY = 1
Next 'Pour toutes les colonnes

'-> Lib�rer le pointeur de cellule
Set aCell = Nothing

'-> Renvoyer une valeur de succ�s
InitBlock = True

'-> Sortir de la fonction
Exit Function

ErrorOpen:

    InitBlock = False
    Set aCell = Nothing


End Function

Public Sub ChargeDRF(ByVal ListeFichier As String)

'---> Cette proc�dure charge un fichier DRF dans la feuille frmSample
'-> La v�rification de l'existance du fichier se fait dans frmFields

Dim lpBuffer As String
Dim Res As Long
Dim aLb As Libelle
Dim aNode As Node
Dim TitleNode As String
Dim i As Integer, j As Integer, w As Integer
Dim NomRang As String
Dim lpKey As String
Dim KeyDef As String
Dim KeyValue As String
Dim KeyDefinition As String
Dim Fichier As String
Dim TempFileName As String
Dim RangCharValue As String

On Error GoTo GestError

For w = 1 To NumEntries(ListeFichier, "|")

    '-> R�cup�rer le nom du fichier
    Fichier = Entry(w, ListeFichier, "|")

    '-> V�rifier que le fichier existe
    If Dir$(Fichier) = "" Then
        Set aLb = Libelles("MESSAGE")
        MsgBox aLb.GetCaption(37) & Chr(13) & Fichier, vbCritical + vbOKOnly, aLb.GetToolTip(37)
        Set aLb = Nothing
        Exit Sub
    End If
    
    '-> PB WIN95 : L'utilisation des API des fichiers ini pose des pb pour _
    les r�pertoires qui sont en lecture seule; En effet , lorsqu'il lit _
    un fichier INI, WIN95 lock el fichier en �crivant dedans : donc -> Violation _
    des privil�ges
    
    TempFileName = GetTempFileNameVB("DRF")
    
    '-> Effectuer une copie du fichier ini dans le r�pertoire tempo
    Res = CopyFile(Fichier, TempFileName, 0)
    DoEvents
    
    '-> Recherche de la cle maquette dans le fichier sp�cifi�
    lpBuffer = Space$(2000)
    Res = GetPrivateProfileSection("MAQUETTE", lpBuffer, Len(lpBuffer), TempFileName)
    If Res = 0 Then
        Set aLb = Libelles("MESSAGE")
        MsgBox aLb.GetCaption(38) & Chr(13) & TempFileName, vbCritical + vbOKOnly, aLb.GetToolTip(38)
        Set aLb = Nothing
        '-> Supprimer le fichier DRF
        If Dir$(TempFileName, vbNormal) <> "" Then Kill TempFileName
        Exit Sub
    End If
    
    '->Cr�ation du node Maquette
    lpBuffer = Space$(500)
    Res = GetPrivateProfileString("MAQUETTE", "TITRE", "Erreur", lpBuffer, Len(lpBuffer), TempFileName)
    'lpBuffer = Mid$(lpBuffer, 1, Res - 1)
    If lpBuffer = "Erreur" Then
        Set aLb = Libelles("MESSAGE")
        MsgBox aLb.GetCaption(39) & Chr(13) & TempFileName, vbCritical + vbOKOnly, aLb.GetToolTip(39)
        Set aLb = Nothing
        Exit Sub
    End If
    TitleNode = Mid$(lpBuffer, 1, Res)
    Set aNode = frmFields.TreeFields.Nodes.Add(, , Fichier, TitleNode, "CloseBook")
    aNode.Selected = True
    aNode.ExpandedImage = "OpenBook"
    
    '-> Ajout de la cle fichier
    Set aLb = Libelles("FRMFIELDS")
    Set aNode = frmFields.TreeFields.Nodes.Add(Fichier, 4, "LIB-" & Fichier, aLb.GetCaption(6) & Fichier, "Lib")
    Set aLb = Nothing
    
    '-> Ajout du commentaire
    Set aLb = Libelles("FRMFIELDS")
    TitleNode = aLb.GetCaption(7)
    Set aLb = Nothing
    lpBuffer = Space$(500)
    Res = GetPrivateProfileString("MAQUETTE", "REMARQUE", "Erreur", lpBuffer, Len(lpBuffer), TempFileName)
    If Res = 0 Then
    Else
        TitleNode = TitleNode & Mid$(lpBuffer, 1, Res)
    End If
    Set aNode = frmFields.TreeFields.Nodes.Add(Fichier, 4, "R-" & Fichier, TitleNode, "Commentaire")
    Set aNode = Nothing
        
    '-> Cr�ation des nodes de l'ensemble des sections
    lpBuffer = Space$(32765)
    Res = GetPrivateProfileString(vbNullString, "", "Erreur", lpBuffer, Len(lpBuffer), TempFileName)
    lpBuffer = Mid$(lpBuffer, 1, Res - 1)
    If lpBuffer = "Erreur" Then
    Else
        For i = 1 To NumEntries(lpBuffer, Chr(0))
            NomRang = Entry(i, lpBuffer, Chr(0))
            If Trim(NomRang) = "" Then
            Else
                If UCase$(NomRang) = "MAQUETTE" Then
                Else
                    '-> Ajout de la cl� du rang
                    Set aNode = frmFields.TreeFields.Nodes.Add(Fichier, 4, Fichier & "@" & NomRang, NomRang, "Rang")
                    '-> Ajout des diff�rentes cl�s
                    lpKey = Space$(32000)
                    '-> Liste des cl�s associ�es
                    Res = GetPrivateProfileSection(NomRang, lpKey, Len(lpKey), TempFileName)
                    lpKey = Mid$(lpKey, 1, Res - 1)
                    For j = 1 To NumEntries(lpKey, Chr(0))
                        '-> R�cup�rer les valeurs
                        KeyDefinition = Entry(j, lpKey, Chr(0))
                        If Trim(KeyDefinition) = "" Then
                        Else
                            KeyDef = Entry(1, KeyDefinition, "=")
                            KeyValue = Entry(2, KeyDefinition, "=")
                            If UCase$(KeyDef) = "RANG" Then
                                '-> Ajout du node du rang caract�re associ�
                                Set aNode = frmFields.TreeFields.Nodes.Add(Fichier & "@" & NomRang, 4, Fichier & "@" & NomRang & "@" & KeyValue, KeyValue, "RangChar")
                                RangCharValue = KeyValue
                            Else
                                '-> Ajout du node du Champ
                                Set aNode = frmFields.TreeFields.Nodes.Add(Fichier & "@" & NomRang, 4, "CHAMP@" & Fichier & "@" & NomRang & "@" & KeyDef & "@" & frmFields.TreeFields.Nodes.Count, "(" & KeyDef & ") - " & KeyValue, "Champ")
                                aNode.Tag = RangCharValue
                            End If
                        End If
                    Next 'Pour toutes les cl�s
                End If
            End If
        Next 'Pour toutes les entr�es
    End If
Next 'Pour tous les fichiers

Set aLb = Nothing
Set aNode = Nothing

'-> Supprimer le fichier DRF
Kill TempFileName

Exit Sub

GestError:
    MsgBox Err.Number & "     " & Err.Description
    Set aLb = Nothing
    Set aNode = Nothing



End Sub



Public Sub EraseMaquette()

'---> Cette proc�dure supprime tous les objets de la maquette et _
vide les treeview associ�s

Dim aMaq As Maquette

On Error Resume Next

'-> Ne rien faire s'il n'y a pas de maquette en cours
If Maquettes.Count = 0 Then Exit Sub

'-> Positionner la variable de suppression
EraseFrm = True

'-> Pointer sur la maquette
Set aMaq = Maquettes("MAQUETTE")

'-> Supprimer toutes les sections
Do While aMaq.Sections.Count <> 0
    EraseSection aMaq.Sections(1).Nom, aMaq
Loop

'-> Supprimer tous les tableaux
Do While aMaq.Tableaux.Count <> 0
    EraseTableau aMaq.Tableaux(1).Nom, aMaq
Loop

'-> Supprimer la maquette caract�re
If MaqChars.Count <> 0 Then EraseMaqChar

'-> Vider la palette des propri�t�s
Unload frmProp

'-> Vider la palette des champs
Unload frmFields

'-> Supprimer la maquette
aMaq.DelTreeView
Maquettes.Remove "MAQUETTE"

'-> Supprimer le pointeur d'objet en cours
ObjCours = Empty

'-> Indiquer que la suppression est finie
EraseFrm = False

'-> Fermer la maquette de transition
If Not afrmTransition Is Nothing Then Unload afrmTransition

Set aMaq = Nothing

End Sub
Public Function EraseMaqChar() As Boolean

'---> Proc�dure qui supprime la maquette carcatere
Dim aMaq As MaqChar
Dim aRg As RangChar

'-> Pointer sur la maquette caract�re
Set aMaq = MaqChars("MAQCHAR")
'-> Suppressio de tous les rangs contenus dans la maquette
For Each aRg In aMaq.RangChars
    aMaq.RangChars.Remove UCase$(aRg.Nom)
Next
'-> Supprimer l'icone de la palette de navigation
aMaq.DelTreeView

'-> Supprimer la maquette
MaqChars.Remove "MAQCHAR"

'-> Fermer la feuille si elle est ouverte
Unload frmMaqChar
Unload frmTransition

Set aMaq = Nothing
Set aRg = Nothing

End Function

Public Function EraseSection(ByVal SectionName As String, ByVal aMaq As Maquette) As Boolean

'---> Proc�dure qui supprime une section de texte

Dim aSection As Section
Dim aFrm As frmSection
Dim aCadre As Cadre
Dim aBmp As ImageObj


'-> Pointer sur la section
Set aSection = aMaq.Sections(UCase$(SectionName))

'-> Pointer sur la feuille
Set aFrm = aMaq.frmSections(UCase$(SectionName))

'-> Suppression de tous les cadres
For Each aCadre In aSection.Cadres
    '-> Suppression de la repr�sentation physique
    Unload aFrm.CadreTexte(aCadre.IdAffichage)
    Unload aFrm.Cadre(aCadre.IdAffichage)
    '-> suppression de l'icone du treeview
    aCadre.DelTreeView
    '-> Suppression de l'objet
    aSection.Cadres.Remove UCase$(aCadre.Nom)
Next 'Pour tous les cadres
    
'-> Supprimer tous les bmps
For Each aBmp In aSection.Bmps
    '-> Suppression de la repr�sentation physique
    If aBmp.IdAffichage <> 0 Then Unload aFrm.BitMap(aBmp.IdAffichage)
    '-> suppresion de l'icone du treeview
    aBmp.DelTreeView
    '-> Suppression de l'objet
    aSection.Bmps.Remove UCase$(aBmp.Nom)
Next 'Pour tous les Bmps

'-> Supprimer la feuille associ�e
aMaq.frmSections.Remove UCase$(SectionName)
Unload aFrm

'-> Supprimer l'icone du treeview
aSection.DelTreeView
    
'-> supprimer dans les ordres d'affichage
aMaq.DelOrdreAffichage aSection.IdAffichage
    
'-> Supprimer l'objet
aMaq.Sections.Remove UCase$(aSection.Nom)

Set aMaq = Nothing
Set aFrm = Nothing
Set aCadre = Nothing
Set aBmp = Nothing
Set aSection = Nothing


End Function

Public Function EraseTableau(ByVal TableauNom As String, ByVal aMaq As Maquette) As Boolean

'---> Proc�dure qui supprime un tableu et les diff�rentes feuilles

Dim aTb As Tableau
Dim aBlock As Block
Dim aForm As frmTableur
Dim aFrm As FrmBlock

'-> Pointer sur le tableau
Set aTb = aMaq.Tableaux(UCase$(TableauNom))

'-> Pointer sur la feuille
Set aForm = aMaq.frmTableurs(UCase$(TableauNom))

'-> Suppression de tous les blocks
For Each aBlock In aTb.Blocks
    '-> si le tableau est en cours d'�dition, supprimer la feuille
    If aBlock.IsEdit Then
        Set aFrm = aTb.frmBlocks(UCase$(aBlock.Nom))
        Unload aFrm
    End If
    '-> Supprimer l'icone du treeview
    aBlock.DelTreeView
    '-> Supprimer le handle de r�gion du tableau associ� au block
    DeleteObject aBlock.hRgn
    '-> Supprimer l'objet
    aTb.Blocks.Remove UCase$("BL-" & (aBlock.Nom))
Next 'Pour tous les blocks

'-> Supprimer la feuille tableau
aMaq.frmTableurs.Remove UCase$(TableauNom)
Unload aForm

'-> Supprimer du treeview
aTb.DelTreeView

'-> Supprimer dans les ordres d'affichage
aMaq.DelOrdreAffichage aTb.IdAffichage

'-> supprimer l'objet assocoi�
aMaq.Tableaux.Remove UCase$(TableauNom)

Set aMaq = Nothing
Set aTb = Nothing
Set aBlock = Nothing
Set aForm = Nothing
Set aFrm = Nothing

End Function

Public Sub GestMenu()

'---> Cette prco�dure est charg�e de g�rer les menus

Dim aMaq As Maquette
Dim aFrm As Turbo_Maq


'-> Pointer sur la feuille des menus
Set aFrm = Turbo_Maq

If Maquettes.Count = 0 Then
    '-> Menu fermer
    aFrm.mnuClose.Enabled = False
    '-> Options d'enregistrement
    aFrm.mnuSave.Enabled = False
    '-> Menu des outils
    aFrm.mnuOutils.Enabled = False
    '-> Menu affichage
    aFrm.mnuAffichage.Enabled = False
    '-> Menu maquette de transition
    aFrm.mnuMaquetteTransition.Enabled = False
    '-> MnuPublipostage
    aFrm.mnuPubli.Enabled = False
Else
    '-> Pointer sur la maquette
    Set aMaq = Maquettes("MAQUETTE")
    '-> Menu fermer
    aFrm.mnuClose.Enabled = True
    '-> Options d'enregistrement
    aFrm.mnuSave.Enabled = True
    '-> Menu des otuils
    aFrm.mnuOutils.Enabled = True
    '-> Test des objets existants : sections
    If aMaq.Sections.Count = 0 Then
        aFrm.mnuDelSection.Enabled = False
    Else
        aFrm.mnuDelSection.Enabled = True
    End If
    '-> Test des objets existants : tableaux
    If aMaq.Tableaux.Count = 0 Then
        aFrm.mnuDeltableau.Enabled = False
    Else
        aFrm.mnuDeltableau.Enabled = True
    End If
    '-> Menu affichage
    aFrm.mnuAffichage.Enabled = True
    '-> Menu maquette de transition
    aFrm.mnuMaquetteTransition.Enabled = True
    '-> MnuPublipostage
    aFrm.mnuPubli.Enabled = True
End If

Set aMaq = Nothing
Set aFrm = Nothing

End Sub

Public Sub GestMaqChar()

If MaqChars.Count = 0 Then
    Turbo_Maq.mnuGenerateMaquette.Enabled = False
    Turbo_Maq.mnuEditMaqTransition.Enabled = False
    Turbo_Maq.mnuGenerateMaquette.Enabled = False
Else
    Turbo_Maq.mnuGenerateMaquette.Enabled = True
    Turbo_Maq.mnuEditMaqTransition.Enabled = True
    Turbo_Maq.mnuGenerateMaquette.Enabled = True
End If

End Sub

Public Sub Save(ByVal NomFichier As String, Optional Apercu As Boolean)

'---> Fonction qui enregistre une maquette
Dim MaqFic As Integer
Dim ErrorValue As Integer
Dim aLb As Libelle
Dim i As Integer, j As Integer
Dim aMaq As Maquette
Dim NomObj As String
Dim aSection As Section
Dim aTb As Tableau
Dim NomFile As String
Dim aRg As RangChar
Dim DefLien As String
Dim ListeLiens As String
Dim DefSautDePage As String

On Error GoTo ErrorSave

'-> Pointeur sur les libelles
Set aLb = Libelles("SAVEMAQ")
'-> Pointeur sur la maquette
Set aMaq = Maquettes("MAQUETTE")

'-> D�termination du nombre total d'objet � enregistrer
TailleTotale = aMaq.Sections.Count + aMaq.Tableaux.Count
For Each aSection In aMaq.Sections
    TailleTotale = TailleTotale + aSection.Cadres.Count
    TailleTotale = TailleTotale + aSection.Bmps.Count
Next
For Each aTb In aMaq.Tableaux
    TailleTotale = TailleTotale + aTb.Blocks.Count
Next
'-> Rajouter + 1 pour l'entete de la maquette
TailleTotale = TailleTotale + 1
TailleLue = 0

'-> Afficher le temporisateur
If Not Apercu Then
    Turbo_Maq.Picture1.Visible = True
    Turbo_Maq.MousePointer = 11
End If

MaqFic = FreeFile
Open NomFichier For Output As #MaqFic

'-> R�cup�rer le nom de la maquette
i = InStrRev(NomFichier, "\")
NomFile = Mid$(NomFichier, i + 1, Len(NomFichier) - i)

'-> Appliquer le nom de fichier sans l'extension
i = InStr(1, NomFile, ".")
NomFile = Mid$(NomFile, 1, i - 1)
If Not Apercu Then aMaq.Nom = NomFile

'-> Enregistrer l'entete de la maquette
If Not (SaveEntete(MaqFic, aMaq)) Then
    ErrorValue = 1
    GoTo ErrorSave
End If

'-> Dessin de la progression
TailleLue = TailleLue + 1
DrawWait

'-> Enregistrer les associations clients
Print #MaqFic, "[PROGICIEL]"
Print #MaqFic, "\Prog=" & V6App
Print #MaqFic, "\Rub=" & V6Rub
If V6ClientCours.Code = "DEAL" Then
    Print #MaqFic, "\Cli=" & V6App
Else
    Print #MaqFic, "\Cli=" & V6ClientCours.Code
End If
'-> Enregistrer la balise HTML
Print #MaqFic, "[HTML]"
Print #MaqFic, "\Fichier�" & FileHTML
Print #MaqFic, "\Rupture�" & BaliseHTML
Print #MaqFic, "\HTML�End"
'-> Enregistrer les objets dans l'ordre d'affichage
For i = 1 To aMaq.nEntries - 1
    '-> R�cup�rer l'entr�e
    NomObj = aMaq.GetOrdreAffichage(i)
    If UCase$(Entry(1, NomObj, "-")) = "SCT" Then
        '-> Pointer sur l'objet section
        Set aSection = aMaq.Sections(UCase$(Entry(2, NomObj, "-")))
        '-> Enregistrer la section
        If Not (SaveSection(MaqFic, aSection, aMaq)) Then
            ErrorLibelle = "Pb dans l'enregistrement de la section : " & aSection.Nom
            GoTo ErrorSave
        End If
        Set aSection = Nothing
    ElseIf UCase$(Entry(1, NomObj, "-")) = "TB" Then
        '-> Pointer sur l'objet tableau
        Set aTb = aMaq.Tableaux(UCase$(Entry(2, NomObj, "-")))
        If Not (SaveTableau(MaqFic, aTb, aMaq, Apercu)) Then
            ErrorLibelle = "Pb dans l'enregistrement du tableau : " & aTb.Nom
            GoTo ErrorSave
        End If
        Set aTb = Nothing
    End If
    
Next

'-> Enregistrer les associations entre les objets graphiques et les objets rangs caract�res
If MaqChars.Count <> 0 Then
    '-> Imprimer la cl�e
    Print #MaqFic, "[MAQCHAR]"
    '-> Imprimer le nom de la maquette de transition : elle a le m�me nom que la maquette d'origine
    Print #MaqFic, "\MAQTRANS�" & MaqChars("MAQCHAR").Nom
    '-> Chemin de la maquette de transition
    Print #MaqFic, "\TRANSITION�" & MaqChars("MAQCHAR").MaqTransition
    '-> Impression des associations par rang
    For i = 2 To MaqChars("MAQCHAR").RangChars.Count - 1
        '-> Pointer sur le rang
        Set aRg = MaqChars("MAQCHAR").RangChars(i)
        '-> Tester s'il afit partie de la liste des rangs de saut de page
        If aRg.IsSautPage Then
            If DefSautDePage = "" Then
                DefSautDePage = aRg.Nom
            Else
                DefSautDePage = DefSautDePage & "," & aRg.Nom
            End If
        End If
        '-> Imprimer ses liens
        For j = 1 To aRg.nLiens - 1
            '-> R�cup�rer les liens
            DefLien = aRg.GetLien(j)
            '-> Selon la nature du lien
            If UCase$(Entry(1, DefLien, "-")) = "SECTION" Then
                If ListeLiens = "" Then
                    ListeLiens = "RG@" & Entry(2, DefLien, "-")
                Else
                    ListeLiens = ListeLiens & "|RG@" & Entry(2, DefLien, "-")
                End If
            ElseIf UCase$(Entry(1, DefLien, "-")) = "TABLEAU" Then
                If ListeLiens = "" Then
                    ListeLiens = "BLOCK@" & UCase$(Entry(2, DefLien, "-")) & "@" & UCase$(Entry(3, DefLien, "-"))
                Else
                    ListeLiens = ListeLiens & "|BLOCK@" & UCase$(Entry(2, DefLien, "-")) & "@" & UCase$(Entry(3, DefLien, "-"))
                End If
            End If
        Next 'pour tous les liens
        '-> Imprimer le lien
        Print #MaqFic, "\RGCHAR�" & aRg.Nom & "�" & aRg.nLignes - 1 & "�" & ListeLiens
        '-> vider la variable
        ListeLiens = ""
        Set aRg = Nothing
    Next 'Pour tous les rangs caract�res
    '-> Imprimer les rangs sauts de page
    Print #MaqFic, "\RGPAGE�" & DefSautDePage
    '-> Il faut maintenant Enregistrer la maquette caractere associ�e
    Print #MaqFic, "[MAQUETTECHAR]"
    For Each aRg In MaqChars("MAQCHAR").RangChars
        If aRg.IdAffichage = 1 Or aRg.IdAffichage = MaqChars("MAQCHAR").RangChars.Count Then
        Else
            Print #MaqFic, """" & aRg.Nom & """"
        End If
        For j = 1 To aRg.nLignes - 1
            '-> Ajouter la ligne
            Print #MaqFic, aRg.GetLigne(j)
        Next
    Next 'pour tous les rangs
End If 'S'il y a une maquette
'-> Fermer le handle du fichier
Close #MaqFic

If Not Apercu Then

    '-> Enregistrer le nom du fichier
    aMaq.Fichier = NomFichier
    
    '-> Modifier le node de frmNaviga
    frmNaviga.TreeNaviga.Nodes("MAQUETTE").Text = NomFile

End If

'-> Supprimer le temporisateur
Turbo_Maq.Picture1.Visible = False
Turbo_Maq.MousePointer = 0

'-> liberer les pointeurs
Set aMaq = Nothing
Set aTb = Nothing
Set aLb = Nothing
Set aRg = Nothing



Exit Sub

ErrorSave:

 '-> Afficher le message d'erreur
    MsgBox aLb.GetCaption(5), vbCritical + vbOKOnly, aLb.GetToolTip(5)
                
    '-> Fermer le handle du fichier
    Close #MaqFic
    
    '-> Supprimer le temporisateur
    Turbo_Maq.Picture1.Visible = False
    Turbo_Maq.MousePointer = 0

    Set aMaq = Nothing
    Set aTb = Nothing
    Set aLb = Nothing
    Set aRg = Nothing


End Sub


Private Function SaveEntete(ByVal MaqFic As Integer, ByRef aMaq As Maquette) As Boolean

'---> Fonction qui enregistre l'entete de la maquette

Dim Orientation As Integer
Dim aLb As Libelle


On Error GoTo ErrorSave

'-> Positionner le libelle d'erreur
ErrorLibelle = "Erreur durant l'enregistrement de l'entete de la maquette "

'-> Enregistrement
Print #MaqFic, "\DefEntete�Begin"
Print #MaqFic, "\Version�2"
Print #MaqFic, "\FieldListe�" & aMaq.FieldListe
Print #MaqFic, "\Nom�" & aMaq.Nom
Print #MaqFic, "\Date�" & Now
Print #MaqFic, "\Description�" & aMaq.Description
Set aLb = Libelles("FRMPROP")
If aMaq.Orientation = aLb.GetCaption(12) Then
    '-> Portrait
    Print #MaqFic, "\Largeur�" & aMaq.Largeur
    Print #MaqFic, "\Hauteur�" & aMaq.Hauteur
    Print #MaqFic, "\Orientation�1"
Else
    '-> Paysage
    Print #MaqFic, "\Largeur�" & aMaq.Hauteur
    Print #MaqFic, "\Hauteur�" & aMaq.Largeur
    Print #MaqFic, "\Orientation�0"
End If
Print #MaqFic, "\MargeTop�" & aMaq.MargeTop
Print #MaqFic, "\MargeLeft�" & aMaq.MargeLeft
Print #MaqFic, "\Suite�"
Print #MaqFic, "\Report�"
Print #MaqFic, "\Entete�"
Print #MaqFic, "\Pied�"
If aMaq.PageGarde Then
    Print #MaqFic, "\PageGarde�VRAI"
Else
    Print #MaqFic, "\PageGarde�FAUX"
End If
If aMaq.PageSelection Then
    Print #MaqFic, "\PageSelection�VRAI"
Else
    Print #MaqFic, "\PageSelection�FAUX"
End If
Print #MaqFic, "\Publipostage�" & aMaq.PubliPostage
Print #MaqFic, "\DefEntete�End"


Set aLb = Nothing

'-> Renvoyer une valeur de succ�s
SaveEntete = True

Exit Function

ErrorSave:

    SaveEntete = False
    
End Function


Private Function SaveSection(ByVal MaqFic As Integer, ByVal aSection As Section, ByVal aMaq As Maquette) As Boolean

On Error GoTo GestSave

'---> fonction qui enregistre la d�finition d'un rang

Dim ListeFields As String
Dim aFrm As frmSection
Dim aLb As Libelle
Dim i As Integer
Dim DefObjet As String
Dim aCadre As Cadre
Dim aBmp As ImageObj

Dim hdlTemp As Integer
Dim TempFile As String
Dim Ligne As String


'-> Temporisateur
TailleLue = TailleLue + 1
DrawWait


'-> Enregistrement de la partie STD
Print #MaqFic, "[NV-" & aSection.Nom & "-STD]"
Print #MaqFic, "\Hauteur�" & aSection.Hauteur
Print #MaqFic, "\Largeur�" & aSection.Largeur
Print #MaqFic, "\RgChar�" & aSection.RangChar
Print #MaqFic, "\Contour�" & CInt(aSection.Contour)
Print #MaqFic, "\AlignementTop�" & aSection.AlignementTop
Print #MaqFic, "\Top�" & aSection.Top
Print #MaqFic, "\AlignementLeft�" & aSection.AlignementLeft
Print #MaqFic, "\Left�" & aSection.Left
Print #MaqFic, "\BackColor�" & aSection.BackColor
'-> Gestion des champs
Set aFrm = aMaq.frmSections(UCase$(aSection.Nom))
ListeFields = MapFields(aFrm.RangText.Text)
If ListeFields = "NO" Then
    '-> Il y a eu une erreur
    Set aLb = Libelles("SAVEMAQ")
    MsgBox aLb.GetCaption(3) & Chr(13) & "Section de texte : " & aSection.Nom, vbCritical + vbOKOnly, aLb.GetToolTip(3)
    Set aLb = Nothing
    '-> Erreur non fatale : on continue
Else
    If Trim(ListeFields) = "" Then
    Else
        ListeFields = FormatSaveFields(ListeFields, aSection)
    End If
End If
Print #MaqFic, "\Champs�" & ListeFields
'-> Fin de d�finition du champ
Print #MaqFic, "\Texte�End"

'---> Ecriture de la d�finition des objets internes de la secion en cours

'-> Enregistrement dand l'ordre d'affichage
For i = 1 To aSection.nEntries - 1
    '-> Description de rang
    DefObjet = aSection.GetOrdreAffichage(i)
    If UCase$(Entry(1, DefObjet, "-")) = "CDR" Then
        '-> Pointer sur le cadre
        Set aCadre = aSection.Cadres(UCase$(Entry(2, DefObjet, "-")))
        '-> Enregistrement du cadre
        If Not SaveCadre(MaqFic, aSection, aCadre) Then GoTo GestSave
        '-> Temporisateur
        TailleLue = TailleLue + 1
        DrawWait

    ElseIf UCase$(Entry(1, DefObjet, "-")) = "BMP" Then
        '-> Pointer sur l'image
        Set aBmp = aSection.Bmps(UCase$(Entry(2, DefObjet, "-")))
        '-> Enregistrement du bmp
        If Not SaveBmp(MaqFic, aSection, aBmp) Then GoTo GestSave
        '-> Temporisateur
        TailleLue = TailleLue + 1
        DrawWait
    End If
Next 'Pour tous les objets compris dans le rang

'-> Imprimer la fin du rang
Print #MaqFic, "\Rang�End"

'-> Section de texte finie d'enregistre� : enregistrer le contenu du texte Rtf
Print #MaqFic, "[NV-" & aSection.Nom; "-RTF]"

'-> Obtenir un nom de fichier temporaire
TempFile = GetTempFileNameVB("RTF")

'-> Enregisrter le contenu du text RTF dans un fichier tempo
aFrm.RangText.SaveFile TempFile

'-> Ouvrir le fichier tempo pour r�cup�rer le contenu de son texte
hdlTemp = FreeFile
Open TempFile For Input As #hdlTemp
Do While Not EOF(hdlTemp)
    '-> Lecture de la ligne
    Line Input #hdlTemp, Ligne
    '-> Impression dans la maquette
    Print #MaqFic, Ligne
Loop 'Boucle d'analyse du fichier RTF

'-> Fermer le fichier tempo
Close #hdlTemp

'-> Supprimer le fichier tempo
If Dir$(TempFile, vbNormal) <> "" Then Kill TempFile

'Print #MaqFic, aFrm.RangText.TextRTF
Print #MaqFic, "\Rtf�End"

'-> Lib�rer le pointeur sur la feuille
Set aFrm = Nothing
Set aSection = Nothing
Set aMaq = Nothing
Set aLb = Nothing
Set aCadre = Nothing
Set aBmp = Nothing

'-> Renvoyer une valeur de succ�s
SaveSection = True

'-> sortir de la fonction
Exit Function

GestSave:

    SaveSection = False
    

End Function

Public Function MapFields(ByVal strToMap As String) As String

'---> Renvoie sous forme d'entr�es s�par�es par des @ la liste des champs

Dim Pos As Integer
Dim Lect As Integer
Dim FieldsListe As String
Dim NomCHamp As String

On Error GoTo GestFields


'-> Intialiser
Pos = 1
Lect = InStr(Pos, strToMap, "^")
Pos = Lect

'-> Analyse du contenu des champs
Do While Lect <> 0
    '-> R�cup�ration du nom du champ
    NomCHamp = Mid$(strToMap, Pos + 1, 4)
    '-> Rajouter � la liste des champs
    If Trim(FieldsListe) = "" Then
        FieldsListe = NomCHamp
    Else
        FieldsListe = FieldsListe & "@" & NomCHamp
    End If
    '-> Incr�menter la position
    Pos = Lect + 1
    '-> Lecture de l'adresse du prochain champs
    Lect = InStr(Pos, strToMap, "^")
    Pos = Lect
Loop
    
'-> Renvoyer la liste des champs
MapFields = FieldsListe

'-> Quitter la fonction
Exit Function

GestFields:
    
    '-> G�n�rer une erreur
    MapFields = "NO"
        
End Function

Private Function FormatSaveFields(ByVal ListeField As String, aObject As Object) As String

On Error Resume Next

'---> Fonction qui renvoie une chaine de carcat�re format�e au format d'enregistrement des champs
Dim i As Integer
Dim fListe As String
Dim aField As Field
Dim FindField As Boolean
Dim aFormat As Integer

'-> Ne rien renvoyer si ListeField = ""
If Trim(ListeField) = "" Then
    FormatSaveFields = ""
    Exit Function
End If

'-> Analyse de la liste des champs
For i = 1 To NumEntries(ListeField, "@")
    '-> Chercher le champ pour trouver son formattage
    For Each aField In aObject.Fields
        If UCase$(Entry(i, ListeField, "@")) = UCase$(aField.Name) Then
            FindField = True
            aFormat = aField.FormatEdit
            Exit For
        Else
            FindField = False
        End If
    Next
    If Not FindField Then aFormat = 50
    If fListe = "" Then
        fListe = Entry(i, ListeField, "@") & ";" & aFormat & " ;"
    Else
        fListe = fListe & "|" & Entry(i, ListeField, "@") & ";" & aFormat & " ;"
    End If
Next

'-> Renvoyer la chaine format�e
FormatSaveFields = fListe

End Function

Private Function SaveCadre(ByVal MaqFic As Integer, ByVal aSection As Section, ByVal aCadre As Cadre) As Boolean

'---> Fonction qui sauvegarde un cadre

Dim aFrm As frmSection
Dim ListeFields As String
Dim aLb As Libelle
Dim aMaq As Maquette

Dim TempFile As String
Dim hdlTemp As Integer
Dim Ligne As String

On Error GoTo ErrorSave

'-> Enrgistrement du format du cadre
Print #MaqFic, "\Cadre�" & aCadre.Nom
Print #MaqFic, "\Left�" & aCadre.Left
Print #MaqFic, "\Top�" & aCadre.Top
Print #MaqFic, "\Distance�" & aCadre.MargeInterne
Print #MaqFic, "\Hauteur�" & aCadre.Hauteur
Print #MaqFic, "\Largeur�" & aCadre.Largeur
Print #MaqFic, "\LargeurBordure�" & aCadre.LargeurBordure
Print #MaqFic, "\RoundRect�" & CInt(aCadre.IsRoundRect)
Print #MaqFic, "\Couleur�" & aCadre.BackColor
Print #MaqFic, "\Contour�" & -CInt(aCadre.BordureHaut) & "," & -CInt(aCadre.BordureBas) & "," & -CInt(aCadre.BordureGauche) & "," & -CInt(aCadre.BordureDroite)
Print #MaqFic, "\Locked�" & CInt(aCadre.Locked)
Print #MaqFic, "\Masteralign�" & aCadre.MasterAlign
Print #MaqFic, "\MasterIndexAlign�" & aCadre.MasterIndexAlign
Print #MaqFic, "\SlaveAlign�" & aCadre.SlaveAlign
Print #MaqFic, "\AlignX�" & aCadre.AlignX
Print #MaqFic, "\AlignY�" & aCadre.AlignY
'-> Gestion des champs
Set aMaq = Maquettes("MAQUETTE")
Set aFrm = aMaq.frmSections(UCase$(aSection.Nom))
ListeFields = MapFields(aFrm.CadreTexte(aCadre.IdAffichage).Text)
If ListeFields = "NO" Then
    '-> Il y a eu une erreur
    Set aLb = Libelles("SAVEMAQ")
    MsgBox aLb.GetCaption(3) & Chr(13) & "Cadre: " & aCadre.Nom, vbCritical + vbOKOnly, aLb.GetToolTip(3)
    Set aLb = Nothing
    '-> Erreur non fatale : on continue
Else
    If Trim(ListeFields) = "" Then
    Else
        ListeFields = FormatSaveFields(ListeFields, aCadre)
    End If
End If
Print #MaqFic, "\Champs�" & ListeFields

'-> Fin de d�finition du Cadre
Print #MaqFic, "\Cadre�End"

'-> Sauvagarde du format Rtf du cadre
Print #MaqFic, "\CadreRtf�Begin"

'-> Cr�er un fichier tempo
TempFile = GetTempFileNameVB("RTF")

'-> Enregistrer le contenu du cadre dans ce fichier
aFrm.CadreTexte(aCadre.IdAffichage).SaveFile TempFile

'-> Ouvrir le fichier tempo pour r�cup�rer le code
hdlTemp = FreeFile
Open TempFile For Input As #hdlTemp
Do While Not EOF(hdlTemp)
    '-> Lecture de la ligne
    Line Input #hdlTemp, Ligne
    '-> Impression dans la maquette graphique
    Print #MaqFic, Ligne
Loop 'Boucle d'analyse du fichier tempo

'-> Fermer le fichier tempo
Close #hdlTemp

'-> Suppression du fichier tempo
If Dir$(TempFile, vbNormal) <> "" Then Kill TempFile

'Print #MaqFic, aFrm.CadreTexte(aCadre.IdAffichage).TextRTF
'-> Fermer le tag du cadre RTF
Print #MaqFic, "\CadreRtf�End"

'-> Liberer le pointeur sur la feuille
Set aFrm = Nothing
Set aSection = Nothing
Set aCadre = Nothing
Set aLb = Nothing
Set aMaq = Nothing

'-> Renvoyer une valeur de succ�s
SaveCadre = True

Exit Function

ErrorSave:

    SaveCadre = False
    '-> Liberer le pointeur sur la feuille
    Set aFrm = Nothing
    Set aSection = Nothing
    Set aCadre = Nothing
    Set aLb = Nothing
    Set aMaq = Nothing


End Function

Private Function SaveBmp(ByVal MaqFic As Integer, ByVal aSection As Section, ByVal aBmp As ImageObj) As Boolean

On Error GoTo ErrorSave

ErrorLibelle = "Erreur dans l'enregistrement du bmp : " & aBmp.Nom

'---> Proc�dure qui enregistre la description d'une image

Dim aFrm As frmSection
Dim aLb As Libelle
Dim aMaq As Maquette
Dim aBlock As Block

'-> Enregistrement
Print #MaqFic, "\Bmp�" & aBmp.Nom
Print #MaqFic, "\Left�" & aBmp.Left
Print #MaqFic, "\Top�" & aBmp.Top
Print #MaqFic, "\Largeur�" & aBmp.Largeur
Print #MaqFic, "\Hauteur�" & aBmp.Hauteur
Print #MaqFic, "\Variable�" & -CInt(aBmp.isVariable)
Print #MaqFic, "\UseAssociation�" & -CInt(aBmp.UseAssociation)
Print #MaqFic, "\EditFormat�" & aBmp.FormatEdit
Print #MaqFic, "\Fichier�" & aBmp.Fichier
Print #MaqFic, "\Path�" & aBmp.Path
Print #MaqFic, "\ContourBmp�" & -CInt(aBmp.Contour)
Print #MaqFic, "\Locked�" & -CInt(aBmp.Locked)
Print #MaqFic, "\Masteralign�" & aBmp.MasterAlign
Print #MaqFic, "\MasterIndexAlign�" & aBmp.MasterIndexAlign
Print #MaqFic, "\SlaveAlign�" & aBmp.SlaveAlign
Print #MaqFic, "\AlignX�" & aBmp.AlignX
Print #MaqFic, "\AlignY�" & aBmp.AlignY

'-> Marqueur de fin de bmp
Print #MaqFic, "\Bmp�End"

'-> Renvoyer une valeur de succ�s
SaveBmp = True

Set aSection = Nothing
Set aLb = Nothing
Set aFrm = Nothing
Set aBmp = Nothing
Set aMaq = Nothing

'-> Sortir de la fonction
Exit Function

ErrorSave:

    SaveBmp = False
    Set aSection = Nothing
    Set aLb = Nothing
    Set aFrm = Nothing
    Set aBmp = Nothing
    Set aMaq = Nothing

    

End Function

Private Function SaveTableau(ByVal MaqFic As Integer, ByVal aTb As Tableau, ByVal aMaq As Maquette, Optional Apercu As Boolean) As Boolean

On Error GoTo ErrorSave

'---> fonction qui enregistre le contenu d'un tableau
Dim i As Integer
Dim aLb As Libelle
Dim aBlock As Block

'-> D�but de l'enregistrement
Print #MaqFic, "[TB-" & aTb.Nom & "]"
Print #MaqFic, "\LargeurTb�" & aTb.Largeur
Set aLb = Libelles("FRMPROP")
If aTb.Orientation = aLb.GetCaption(12) Then
    '-> Portrait
    Print #MaqFic, "\OrientationTB�1"
Else
    Print #MaqFic, "\OrientationTB�0"
End If
Set aLb = Nothing

'-> Temporisateur
TailleLue = TailleLue + 1
DrawWait


'-> Enregistrement des blocks Associ�s
For i = 1 To aTb.nBlocks - 1
    '-> Pointer sur le block
    Set aBlock = aTb.Blocks(aTb.GetOrdreAffichage(i))
    '-> Enregistrer
    If Not SaveBlock(MaqFic, aBlock, aTb) Then GoTo ErrorSave
    '-> Temporisateur
    TailleLue = TailleLue + 1
    DrawWait
Next 'Pour tous les blocks

'-> Fin de tableau
Print #MaqFic, "\Tableau�End"

'-> Renvoyer une valeur succ�s
SaveTableau = True

Set aTb = Nothing
Set aMaq = Nothing

'-> quitter la fonction
Exit Function

ErrorSave:
    
    SaveTableau = False
    Set aTb = Nothing
    Set aMaq = Nothing



End Function
Private Function SaveBlock(ByVal MaqFic As Integer, ByVal aBlock As Block, ByVal aTb As Tableau) As Boolean

'---> Fonction qui enregistre un block de tableau

On Error GoTo ErrorSave

Dim i As Integer, j As Integer
Dim Ligne As String
Dim aCell As Cellule
Dim DefBordure As String
Dim DefCellule As String
Dim ListeCol As String
Dim ListeField As String
Dim VarLig As String


'-> positionner le libelle erreur
ErrorLibelle = "Erreur pendant l'enregistrement de l'entete du block : " & aBlock.Nom

'-> Enregistrer le d�but du block
Print #MaqFic, "\Begin�" & aBlock.Nom
Print #MaqFic, "\Largeur�" & aBlock.Largeur
Print #MaqFic, "\Hauteur�" & aBlock.Hauteur
Print #MaqFic, "\AlignTop�" & aBlock.AlignementTop
Print #MaqFic, "\Top�" & aBlock.Top
Print #MaqFic, "\AlignLeft�" & aBlock.AlignementLeft
Print #MaqFic, "\Left�" & aBlock.Left
Print #MaqFic, "\MasterLink�" & aBlock.MasterLink
Print #MaqFic, "\SlaveLink�" & aBlock.SlaveLink
Print #MaqFic, "\RgChar�" & aBlock.RangChar
Print #MaqFic, "\Ligne�" & aBlock.NbLigne
Print #MaqFic, "\Colonne�" & aBlock.NbCol

'-> Gestion des lignes � hauteur variable
For i = 1 To aBlock.NbLigne
    If aBlock.GetVarLig(i) Then
        If VarLig = "" Then
            VarLig = i
        Else
            VarLig = VarLig & "|" & i
        End If
    End If
Next

Print #MaqFic, "\Varlig�" & VarLig

'-> Gestion des lignes � export Excel
VarLig = ""
For i = 1 To aBlock.NbLigne
    If Not aBlock.GetExportExcel(i) Then
        If VarLig = "" Then
            VarLig = i
        Else
            VarLig = VarLig & "|" & i
        End If
    End If
Next

Print #MaqFic, "\ExportLig�" & VarLig

'-> Enregistrement des lignes
For i = 1 To aBlock.NbLigne
    '-> Pr�parer l'enregistrement de la ligne
    Ligne = "\Col�" & aBlock.GetHauteurLig(i) & "�"
    '-> Pour toutes les colonnes
    For j = 1 To aBlock.NbCol
        '-> Pointer sur la cellule
        Set aCell = aBlock.Cellules("L" & i & "C" & j)
        '-> Positionner le libelle d'erreur
        ErrorLibelle = "Erreur sur la cellule L" & i & "C" & j & " dans le block : " & aBlock.Nom
        DefCellule = DefCellule & aCell.CellAlign & ";" & aCell.Contenu & ";" & _
                aBlock.GetLargeurCol(j) & ";"
        '-> Enregistrement des bordures
        If Not aCell.BordureHaut Then
            DefBordure = "FAUX"
        Else
            DefBordure = "VRAI"
        End If
        If Not aCell.BordureBas Then
            DefBordure = DefBordure & ",FAUX"
        Else
            DefBordure = DefBordure & ",VRAI"
        End If
        If Not aCell.BordureGauche Then
            DefBordure = DefBordure & ",FAUX"
        Else
            DefBordure = DefBordure & ",VRAI"
        End If
        If Not aCell.BordureDroite Then
            DefBordure = DefBordure & ",FAUX"
        Else
            DefBordure = DefBordure & ",VRAI"
        End If
        DefCellule = DefCellule & DefBordure & ";" & aCell.FontName & ";" & aCell.FontSize
        If aCell.FontBold Then
            DefCellule = DefCellule & ";VRAI"
        Else
            DefCellule = DefCellule & ";FAUX"
        End If
        If aCell.FontItalic Then
            DefCellule = DefCellule & ";VRAI"
        Else
            DefCellule = DefCellule & ";FAUX"
        End If
        If aCell.FontUnderline Then
            DefCellule = DefCellule & ";VRAI"
        Else
            DefCellule = DefCellule & ";FAUX"
        End If
        '-> FontColor
        DefCellule = DefCellule & ";" & aCell.FontColor & ";"
        '-> BackColor
        If aCell.FondTransParent Then
            DefCellule = DefCellule & "999"
        Else
            DefCellule = DefCellule & aCell.BackColor
        End If
        '-> Sp�cial version 2.00
        If aCell.AutoAjust Then
            DefCellule = DefCellule & ";VRAI"
        Else
            DefCellule = DefCellule & ";FAUX"
        End If
        DefCellule = DefCellule & ";" & aCell.TypeValeur & "@" & aCell.Msk
        If aCell.PrintZero Then _
            DefCellule = DefCellule & "@1"
        
        '-> Version 2.00 Export vers Excel
        DefCellule = DefCellule & ";" & CInt(aCell.IsFusion) & "@" & aCell.ColFusion
                        
        '-> Ajouter la d�finition de la cellule � la ligne
        If Trim(ListeCol) = "" Then
            ListeCol = DefCellule
        Else
            ListeCol = ListeCol & "|" & DefCellule
        End If
        '-> Lister les champs
        DefCellule = MapFields(aCell.Contenu)
        If DefCellule <> "" Then
            DefCellule = FormatSaveFields(DefCellule, aCell)
            If Trim(ListeField) = "" Then
                ListeField = DefCellule
            Else
                ListeField = ListeField & "|" & DefCellule
            End If
        End If
        '-> Vider la variable
        DefCellule = ""
    Next 'Pour toutes les colonnes
    '-> La d�finition de la ligne est finie : -> Enregistrer
    Print #MaqFic, Ligne & ListeCol
    '-> Vides les variables
    Ligne = ""
    ListeCol = ""
Next 'Pour toutes les lignes du block

'-> Enregistrer la liste des champs
Print #MaqFic, "\Champs�" & ListeField

'-> enregistrement de l'acc�s au d�tail
ListeField = ""
DefCellule = ""
If aBlock.UseAccesDet Then
    '-> Enregistrer dans un premier temps la cl� d'acc�s au d�tail
    ListeField = "\AccesDet�" & aBlock.KeyAccesDet & "�"
    '-> Enregistrer les cellules qui donnent l'acc�s au d�tail
    For Each aCell In aBlock.Cellules
        If aCell.UseAccesDet Then
            If DefCellule = "" Then
                DefCellule = aCell.Ligne & "-" & aCell.Colonne
            Else
                DefCellule = DefCellule & "|" & aCell.Ligne & "-" & aCell.Colonne
            End If
        End If
    Next 'Pour toutes les cellules
    '-> Imprimer l'acc�s au d�tail
    Print #MaqFic, ListeField & DefCellule
Else
    Print #MaqFic, "\AccesDet�"
End If

'-> Marqueur de fin de block
Print #MaqFic, "\End�" & aBlock.Nom

'-> Renvoyer une valeur succ�s
SaveBlock = True
 
Set aBlock = Nothing
Set aTb = Nothing
Set aCell = Nothing
 
'-> Quitter la fonction
Exit Function

ErrorSave:

    SaveBlock = False
    Set aBlock = Nothing
    Set aTb = Nothing
    Set aCell = Nothing


End Function
Public Sub DrawWait()

'---> Proc�dure qui dessine dans Accueil.picture1 le pourcentage r�alis�
'-> ( TailleLue / TailleTotale )
DoEvents
Turbo_Maq.Tempo.Line (0, 0)-((TailleLue / TailleTotale) * Turbo_Maq.Picture1.ScaleWidth, Turbo_Maq.Picture1.ScaleHeight), &HC00000, BF

End Sub

Public Sub LoadOrdreAffichage(ByVal ObjectSup)

'---> Fonction qui rempli
Dim i As Integer
Dim aDefObj  As String
Dim ObjectToAnalyse As Object
Dim aMaq As Maquette



If TypeOf ObjectSup Is Section Or TypeOf ObjectSup Is Tableau Then
    Set ObjectToAnalyse = Maquettes("MAQUETTE")
ElseIf TypeOf ObjectSup Is Block Then
    Set ObjectToAnalyse = Maquettes("MAQUETTE").Tableaux(UCase$(ObjectSup.NomTb))
ElseIf TypeOf ObjectSup Is Cadre Or TypeOf ObjectSup Is ImageObj Then
    Set aMaq = Maquettes("MAQUETTE")
    Set ObjectToAnalyse = aMaq.Sections(UCase$(ObjectSup.SectionName))
End If
    

For i = 1 To ObjectToAnalyse.nEntries - 1
    aDefObj = ObjectToAnalyse.GetOrdreAffichage(i)
    If UCase$(Entry(1, aDefObj, "-")) = "SCT" Then
        frmDisplayOrder.List1.AddItem "Section : " & Entry(2, aDefObj, "-")
    ElseIf UCase$(Entry(1, aDefObj, "-")) = "TB" Then
        frmDisplayOrder.List1.AddItem "Tableau : " & Entry(2, aDefObj, "-")
    ElseIf UCase$(Entry(1, aDefObj, "-")) = "CDR" Then
        frmDisplayOrder.List1.AddItem "Cadre : " & Entry(2, aDefObj, "-")
    ElseIf UCase$(Entry(1, aDefObj, "-")) = "BL" Then
        frmDisplayOrder.List1.AddItem "Block : " & Entry(2, aDefObj, "-")
    Else
        frmDisplayOrder.List1.AddItem "Bmp : " & Entry(2, aDefObj, "-")
    End If
Next

Set frmDisplayOrder.ObjectSource = ObjectToAnalyse

Set ObjectSup = Nothing
Set ObjectToAnalyse = Nothing
Set aMaq = Nothing

End Sub


Public Function CreateBaliseHtml(MaqFic As Integer) As Boolean

On Error GoTo GestError

Dim Ligne As String

Do While Not UCase$(Ligne) = "\HTML�END"
    Line Input #MaqFic, Ligne
    If InStr(1, Ligne, "\") = 1 Then
        Select Case UCase$(Entry(1, Ligne, "�"))
            Case "\FICHIER"
                FileHTML = Entry(2, Ligne, "�")
            Case "\RUPTURE"
                BaliseHTML = Entry(2, Ligne, "�")
        End Select
    End If
Loop

CreateBaliseHtml = True

Exit Function


GestError:

    CreateBaliseHtml = False

End Function

Public Function AddCell(ByVal Ligne As Integer, ByVal Colonne As Integer, _
                        ByVal Creation As Boolean, ByVal X1 As Integer, ByVal Y1 As Integer, _
                        ByVal X2 As Integer, ByVal Y2 As Integer, Optional Ref As Cellule) As Cellule

Dim NewCell As Cellule

'-> Initialisation d'une nouvelle cellule avec les options par d�faut
Set NewCell = New Cellule

'-> Propri�t�s
NewCell.Ligne = Ligne
NewCell.Colonne = Colonne
NewCell.BackColor = DefBackcolor

'-> Positions
NewCell.X1 = X1
NewCell.X2 = X2
NewCell.Y1 = Y1
NewCell.Y2 = Y2
    
'-> Propri�t�s de Font
NewCell.Contenu = ""
NewCell.FontName = DefFontName
NewCell.FontSize = DefFontSize
NewCell.FontBold = DefBold
NewCell.FontItalic = DefItalic
NewCell.FontUnderline = DefUnderline
NewCell.FontColor = DefFontColor
            
'-> Alignement interne
NewCell.CellAlign = DefALignCell
NewCell.AutoAjust = DefRetourLig
                
'-> Bordures
NewCell.BordureBas = DefBas
NewCell.BordureDroite = DefDroite
NewCell.BordureGauche = DefGauche
NewCell.BordureHaut = DefHaut
        
'-> Format par d�faut
NewCell.TypeValeur = 0
NewCell.Msk = "0"
NewCell.PrintZero = True


'-> Renvoyer la cellule
Set AddCell = NewCell

End Function

Public Function GetBordureBas(aBlock As Block, ByVal Lig As Integer, ByVal Col As Integer) As Boolean

On Error Resume Next

'---> Cette fonction renvoie True si la bordure haut de la cellule situ�e en bas existe
Dim aCell As Cellule

If Lig <> aBlock.NbLigne Then
    Set aCell = aBlock.Cellules("L" & Lig + 1 & "C" & Col)
    GetBordureBas = aCell.BordureHaut
Else
    Set aCell = aBlock.Cellules("L" & Lig & "C" & Col)
    GetBordureBas = aCell.BordureBas
End If

Set aCell = Nothing

End Function

Public Function GetBordureDroite(aBlock As Block, ByVal Lig As Integer, ByVal Col As Integer) As Boolean

On Error Resume Next

'---> Cette fonction renvoie True si la bordure gauche de la cellule situ�e � droite existe
Dim aCell As Cellule

If Col <> aBlock.NbCol Then
    Set aCell = aBlock.Cellules("L" & Lig & "C" & Col + 1)
    GetBordureDroite = aCell.BordureGauche
Else
    Set aCell = aBlock.Cellules("L" & Lig & "C" & Col)
    GetBordureDroite = aCell.BordureDroite
End If

Set aCell = Nothing

End Function

Public Function GetBordureGauche(aBlock As Block, ByVal Lig As Integer, ByVal Col As Integer) As Boolean

On Error Resume Next

'---> Cette fonction renvoie True si la bordure droite de la cellule situ�e � gauche existe
Dim aCell As Cellule

If Col <> 1 Then
    Set aCell = aBlock.Cellules("L" & Lig & "C" & Col - 1)
    GetBordureGauche = aCell.BordureDroite
Else
    Set aCell = aBlock.Cellules("L" & Lig & "C" & Col)
    GetBordureGauche = aCell.BordureGauche
End If

Set aCell = Nothing

End Function

Public Function GetBordureHaut(aBlock As Block, ByVal Lig As Integer, ByVal Col As Integer) As Boolean

On Error Resume Next

'---> Cette fonction renvoie True si la bordure bas de la cellule situ�e au dessus existe
Dim aCell As Cellule


If Lig <> 1 Then
    Set aCell = aBlock.Cellules("L" & Lig - 1 & "C" & Col)
    GetBordureHaut = aCell.BordureBas
Else
    Set aCell = aBlock.Cellules("L" & Lig & "C" & Col)
    GetBordureHaut = aCell.BordureHaut
End If

Set aCell = Nothing

End Function

Public Sub CopyCell(aBlock As Block, ByVal Ligne As Integer, ByVal Colonne As Integer)

'-> Copie la cellule active

ClipBoardCell.Contenu = aBlock.Cellules("L" & Ligne & "C" & Colonne).Contenu
ClipBoardCell.BackColor = aBlock.Cellules("L" & Ligne & "C" & Colonne).BackColor
ClipBoardCell.FontName = aBlock.Cellules("L" & Ligne & "C" & Colonne).FontName
ClipBoardCell.FontSize = aBlock.Cellules("L" & Ligne & "C" & Colonne).FontSize
ClipBoardCell.FontColor = aBlock.Cellules("L" & Ligne & "C" & Colonne).FontColor
ClipBoardCell.FontBold = aBlock.Cellules("L" & Ligne & "C" & Colonne).FontBold
ClipBoardCell.FontItalic = aBlock.Cellules("L" & Ligne & "C" & Colonne).FontItalic
ClipBoardCell.FontUnderline = aBlock.Cellules("L" & Ligne & "C" & Colonne).FontUnderline
ClipBoardCell.CellAlign = aBlock.Cellules("L" & Ligne & "C" & Colonne).CellAlign
ClipBoardCell.AutoAjust = aBlock.Cellules("L" & Ligne & "C" & Colonne).AutoAjust

End Sub

Public Sub PasteCell(aBlock As Block, ByVal Ligne As Integer, ByVal Colonne As Integer, ByRef aPic As PictureBox)

'-> Coller la cellule

If ClipBoardCell.FontName = "" Then Exit Sub

aBlock.Cellules("L" & Ligne & "C" & Colonne).Contenu = ClipBoardCell.Contenu
aBlock.Cellules("L" & Ligne & "C" & Colonne).BackColor = ClipBoardCell.BackColor
aBlock.Cellules("L" & Ligne & "C" & Colonne).FontName = ClipBoardCell.FontName
aBlock.Cellules("L" & Ligne & "C" & Colonne).FontSize = ClipBoardCell.FontSize
aBlock.Cellules("L" & Ligne & "C" & Colonne).FontColor = ClipBoardCell.FontColor
aBlock.Cellules("L" & Ligne & "C" & Colonne).FontBold = ClipBoardCell.FontBold
aBlock.Cellules("L" & Ligne & "C" & Colonne).FontItalic = ClipBoardCell.FontItalic
aBlock.Cellules("L" & Ligne & "C" & Colonne).FontUnderline = ClipBoardCell.FontUnderline
aBlock.Cellules("L" & Ligne & "C" & Colonne).CellAlign = ClipBoardCell.CellAlign
aBlock.Cellules("L" & Ligne & "C" & Colonne).AutoAjust = ClipBoardCell.AutoAjust

aPic.Refresh

End Sub

Public Sub DrawTableau(ByVal TableauToDraw As Tableau)

'---> Cette Proc�dure dessine enti�rement un tableau selon l'ordre d'affichage des _
diff�rents blocks qui le compose

Dim i As Integer, j As Integer, k As Integer
Dim aBlock As Block
Dim DecalageY As Integer
Dim aForm As frmTableur
Dim aCell As Cellule
Dim LargeurPix As Integer
Dim HauteurPix As Integer
Dim MoveActive As Boolean
Dim hRgn As Long

'-> Ne rien faire si on ferme une feuille pendant  une proc�dure de suppression
If EraseFrm Then Exit Sub


'-> Pointer sur la feuille tableau
Set aForm = Maquettes("MAQUETTE").frmTableurs(UCase$(TableauToDraw.Nom))

'-> Effacer l'ancien tableau
aForm.PicTableau.Cls

For k = 1 To TableauToDraw.Blocks.Count
    '-> Pointer sur le block � dessiner
    Set aBlock = TableauToDraw.Blocks(UCase$(TableauToDraw.GetOrdreAffichage(k)))
    '-> S�lection du premier
    If k = 1 Then
        aForm.IemBlock.Text = aBlock.Nom
        aForm.NomBlock = aBlock.Nom
    End If
    '-> Largeur du tableau
    If aBlock.LargeurPix > LargeurPix Then
        aForm.PicTableau.Width = aBlock.LargeurPix
        LargeurPix = aBlock.LargeurPix
    End If
    '-> Hauteur du tableau
    HauteurPix = HauteurPix + aBlock.HauteurPix
    '-> Appliquer les dimensions
    aForm.PicTableau.Height = HauteurPix
    '-> Dessiner le tableau
    For i = 1 To aBlock.NbCol
        For j = 1 To aBlock.NbLigne
            Set aCell = aBlock.Cellules("L" & j & "C" & i)
            If aCell.IsActive = True Then
                aCell.IsActive = False
                MoveActive = True
            End If
            hRgn = CreateRectRgn(aCell.X1, aCell.Y1 + DecalageY, aCell.X2, aCell.Y2 + DecalageY)
            DrawCell j, i, aForm.PicTableau, DecalageY, aBlock, hRgn
            DeleteObject hRgn
            If MoveActive Then
                aCell.IsActive = True
                MoveActive = False
            End If
        Next 'Pour toutes les lignes
    Next 'Pour toutes les colonnes
    '-> Supprimer l'ancienne r�gion
    DeleteObject aBlock.hRgn
    '-> Cr�er la r�gion associ�e au block dans le tableau
    aBlock.hRgn = CreateRectRgn(0, DecalageY, aForm.PicTableau.Width, DecalageY + aBlock.HauteurPix)
    '-> Tenir compte du d�calage
    DecalageY = DecalageY + aBlock.HauteurPix
Next 'Pour tous les blocks

'-> Rendre le tableau visible
aForm.PicTableau.Visible = True

'-> Gestion des scrolls
aForm.AutoScroll

Set aBlock = Nothing
Set aCell = Nothing
Set TableauToDraw = Nothing

End Sub

Public Function V6SearchPath(ByVal NomFichier As String) As String

'-> Passage en v6 gestion des paths de recherche des images
'2 cas :
'le client est deal -> Recherche dans Emilie\gui + app\gui si app <> emilie
'le client n'esta pas deal '-> Recherche ident\app\gui +  deal\app\gui + deal\emilie\app si app <> emilie

Dim i As Integer
Dim SearchFile As String

'-> Analyse des paths
For i = 1 To UBound(V6ListePath())
    SearchFile = Dir$(V6ListePath(i, 1) & NomFichier)
    If SearchFile <> "" Then V6SearchPath = V6ListePath(i, 1) & SearchFile
Next

End Function


Public Function GetValidName(ByVal NewName As String) As Boolean

'-> Fonction qui interdit certain mot cle

GetValidName = True


Select Case UCase$(Trim(NewName))

    Case "MAQUETTE", "TABLEAU", "SECTION", "CADRE", "IMAGE", "BMP", "TB", "BLK", "CDR", "SCT", "CELLULE", "BLOCK", "MAQ"
        GetValidName = False
    
End Select


End Function


Public Sub ErrorName()

Dim aLb As Libelle

Set aLb = Libelles("MESSAGE")

MsgBox aLb.GetCaption(46), vbCritical + vbOKOnly, aLb.GetToolTip(46)


End Sub


