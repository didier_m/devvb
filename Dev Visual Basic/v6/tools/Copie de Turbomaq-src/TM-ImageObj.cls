VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ImageObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'**********************************************
'*                                            *
'* Cette classe contient la d�finition d'un   *
'* Objet ImageBmp                             *
'*                                            *
'**********************************************


'---> Propri�t�s de l'objet
Public Nom As String
Public Fichier As String
Public Left As Single
Public Top As Single
Public Largeur As Single
Public Hauteur As Single
Public Contour As Boolean
Public IdAffichage As Integer
'-> index propre � l'objet pour la gestion des ordres d'affichage
Public IdOrdreAffichage As Integer
Public IsRedim As Boolean

'---> Pour Gestion des alignements
Public Locked As Boolean 'indique si l'alignement est verrouill�
Public MasterAlign As String 'R�f�rence de l'objet d'alignement sous la forme :
                ' [OBJ]|[NOMOBJ]-[IdAlign]-[Icone]
                ' [PAGE]-[IdAlign]-[Icone]
                
Public SlaveAlign As String 'R�f�rence de l'objet qui est align� sur lui
Public MasterIndexAlign As Integer 'Index de l'alignement
Public AlignX As Single
Public AlignY As Single

'-> Indique le type de path : PARAM , CLIENT , APP
Public Path As String

'---> Bmp Variable
Public isVariable As Boolean

'---> Utiliser l'association d'image
Public UseAssociation As Boolean

'---> Nom de la section mere
Public SectionName As String

Public FormatEdit As Integer

Public Sub AddTreeView()

'---> Proc�dure charg�e d'ajouer l'icone dans la feuille de navigation

Dim aNode As Node

Set aNode = frmNaviga.TreeNaviga.Nodes.Add(UCase$(SectionName), 4, UCase$(SectionName) & "|" & UCase$(Nom), Nom, "Bmp")
aNode.Tag = "BMP"
aNode.Selected = True
Set aNode = Nothing


End Sub


Public Sub DelTreeView()

'---> Proc�dure charg�e de supprimer l'icone dans la feuille de navigation
On Error Resume Next

Dim aNode As Node

frmNaviga.TreeNaviga.Nodes.Remove UCase$(SectionName) & "|" & UCase$(Nom)
Set aNode = Nothing

End Sub

Public Sub MoveSlave()

'---> Cette proc�dure est appel�e par un objet maitre qui est d�plac�. _
Elle a pour but de d�placer son escalve s'il y en a un

If SlaveAlign = "" Then Exit Sub



Dim DefObject As String
Dim TypeObject As String
Dim NomObject As String
Dim ObjectToMove
Dim aSection As Section
Dim i As Integer

'-> Pointer sur la section
Set aSection = Maquettes("MAQUETTE").Sections(UCase$(SectionName))

For i = 1 To NumEntries(SlaveAlign, "-")

    '-> D�finitions
    DefObject = Entry(i, SlaveAlign, "-")
    TypeObject = Entry(1, DefObject, "|")
    NomObject = Entry(2, DefObject, "|")
    
    '-> Pointer sur l'objet � d�placer
    If UCase$(TypeObject) = "CDR" Then
        Set ObjectToMove = aSection.Cadres(UCase$(NomObject))
    ElseIf UCase$(TypeObject) = "BMP" Then
        Set ObjectToMove = aSection.Bmps(UCase$(NomObject))
    End If
    '-> D�placer l'esclave
    AlignOnObject ObjectToMove, Nom, ObjectToMove.MasterIndexAlign, ObjectToMove.AlignX, ObjectToMove.AlignY
    
    '-> D�placver les esclaves de l'escale
    ObjectToMove.MoveSlave
Next

'-> Lib�rer les ressources
Set ObjectToMove = Nothing
Set aSection = Nothing


End Sub

Public Sub ClearAlign()

'---> Cette fonction est appel�e pour supprimer les liens d'alignement chez les esclaves _
et chez les maitres

Dim aObject
Dim TypeObject As String
Dim NomObject As String
Dim DefObject As String
Dim aSection As Section
Dim i As Integer

Set aSection = Maquettes("MAQUETTE").Sections(UCase$(SectionName))

'-> Suppression chez le maitre.
If MasterAlign <> "" Then
    '-> V�rifier qu'il s'agit d'un lien objet et non d'un lien page
    If UCase$(Entry(1, MasterAlign, "-")) = "PAGE" Then
    Else
        '-> il faut supprimer l'entr�e dans la liste des esclaves du maitre
        DefObject = Entry(1, MasterAlign, "-")
        TypeObject = Entry(1, DefObject, "|")
        NomObject = Entry(2, DefObject, "|")
        '-> Pointer sur le maitre
        If UCase$(TypeObject) = "CDR" Then
            Set aObject = aSection.Cadres(UCase$(NomObject))
        ElseIf UCase$(TypeObject) = "BMP" Then
            Set aObject = aSection.Bmps(UCase$(NomObject))
        End If
        '-> supprimer l'entr�e dans la propri�t� SlaveAlign
        i = GetEntryIndex(aObject.SlaveAlign, "CDR|" & UCase$(Nom), "-")
        aObject.SlaveAlign = DeleteEntry(aObject.SlaveAlign, i, "-")
    End If
End If


'-> Suppression chez les esclaves
If SlaveAlign <> "" Then
    For i = 1 To NumEntries(SlaveAlign, "-")
        '-> il faut supprimer la r�f�rence au maitre chez les esclaves
        DefObject = Entry(i, SlaveAlign, "-")
        TypeObject = Entry(1, DefObject, "|")
        NomObject = Entry(2, DefObject, "|")
        '-> Pointer sur le maitre
        If UCase$(TypeObject) = "CDR" Then
            Set aObject = aSection.Cadres(UCase$(NomObject))
        ElseIf UCase$(TypeObject) = "BMP" Then
            Set aObject = aSection.Bmps(UCase$(NomObject))
        End If
        '-> Suppression du lien
        aObject.MasterAlign = ""
        aObject.MasterIndexAlign = 0
        aObject.Locked = False
        aObject.AlignX = 0
        aObject.AlignY = 0
    Next
End If
        
Set aSection = Nothing
Set aObject = Nothing





End Sub

