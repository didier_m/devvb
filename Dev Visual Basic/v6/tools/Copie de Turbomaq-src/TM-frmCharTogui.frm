VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmCharTogui 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Interface caract�re"
   ClientHeight    =   5475
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11715
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5475
   ScaleWidth      =   11715
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   "Propri�t�s de la maquette :"
      Height          =   1935
      Left            =   8880
      TabIndex        =   18
      Top             =   480
      Width           =   2775
      Begin VB.CommandButton cmdSaisieComplement 
         Caption         =   "Saisie compl�ment fin de maquette"
         Height          =   615
         Left            =   120
         TabIndex        =   22
         Top             =   1200
         Width           =   2535
      End
      Begin VB.CommandButton cmdLigMaq 
         Caption         =   "Modifier"
         Height          =   255
         Left            =   1680
         TabIndex        =   21
         Top             =   720
         Width           =   975
      End
      Begin VB.Label lblLig 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Nombre de ligne pour report (LIG) :"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   360
         Width           =   2535
      End
   End
   Begin VB.CommandButton cmdDelObj 
      Height          =   375
      Left            =   4320
      Picture         =   "TM-frmCharTogui.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   2400
      Width           =   375
   End
   Begin VB.CommandButton cmdAddObj 
      Height          =   375
      Left            =   3840
      Picture         =   "TM-frmCharTogui.frx":038A
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   2400
      Width           =   375
   End
   Begin VB.CommandButton cmdDelRG 
      Caption         =   "Supprimer"
      Height          =   255
      Left            =   1920
      TabIndex        =   12
      Top             =   2160
      Width           =   1695
   End
   Begin VB.CommandButton cmdAddRG 
      Caption         =   "Ajouter"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   2160
      Width           =   1695
   End
   Begin MSComctlLib.TreeView TreeView2 
      Height          =   2535
      Left            =   120
      TabIndex        =   10
      Top             =   2880
      Width           =   10935
      _ExtentX        =   19288
      _ExtentY        =   4471
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      Appearance      =   1
   End
   Begin VB.Frame Frame1 
      Caption         =   "Propri�t�s du rang : "
      Height          =   1935
      Left            =   3720
      TabIndex        =   2
      Top             =   480
      Width           =   5055
      Begin VB.CommandButton cmdCompleteNom 
         Caption         =   "Modifier"
         Height          =   255
         Left            =   3960
         TabIndex        =   15
         Top             =   720
         Width           =   975
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Rang de saut de page : "
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   1440
         Width           =   4695
      End
      Begin VB.CommandButton cmdLigRG 
         Caption         =   "Modifier"
         Height          =   255
         Left            =   3960
         TabIndex        =   7
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label Label7 
         Caption         =   "Compl�ment : "
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label lblComplement 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1920
         TabIndex        =   16
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label lblNbLig 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1920
         TabIndex        =   6
         Top             =   1080
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "Nombre de lignes : "
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   1080
         Width           =   1455
      End
      Begin VB.Label lblNomRang 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1920
         TabIndex        =   4
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label Label4 
         Caption         =   "Nom : "
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   360
         Width           =   735
      End
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   1575
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   2778
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      FullRowSelect   =   -1  'True
      Appearance      =   1
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   11160
      Picture         =   "TM-frmCharTogui.frx":0714
      Top             =   3480
      Width           =   480
   End
   Begin VB.Image Image2 
      Height          =   480
      Left            =   11160
      Picture         =   "TM-frmCharTogui.frx":13DE
      Top             =   2880
      Width           =   480
   End
   Begin VB.Label Label5 
      Caption         =   "Liste des objets affect�s � ce rang : "
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   2520
      Width           =   3375
   End
   Begin VB.Label Label3 
      Caption         =   "Liste des rangs :"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1575
   End
End
Attribute VB_Name = "frmCharTogui"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAddObj_Click()

Dim aRg As MaqToGui

'-> Pointer sur le rang s�lectionn�
Set aRg = MaqToGuis(Me.TreeView1.SelectedItem.Key)

'-> Charger la liste des tableaux et des sections
frmAddObj.ListeObj = aRg.ListeObj

'-> vider la variable de retour
strRetour = ""

'-> Afficher la feuille de choix
frmAddObj.Show vbModal

'-> Traiter la variable de retour
If strRetour = "" Then Exit Sub

'-> Ajouter dans la liste des objets du rang
If aRg.ListeObj = "" Then
    aRg.ListeObj = strRetour
Else
    aRg.ListeObj = aRg.ListeObj & "|" & strRetour
End If
    
'-> Ajouter dans la liste des objets affect�s � ce rang
Display_Affectation aRg

'-> S�lectionner l'objet que l'on vient d'ajouter
Me.TreeView2.Nodes(strRetour).Selected = True

'-> Gestion des boutons
GestBoutons


End Sub

Private Sub cmdAddRG_Click()

'---> Cette proc�dure ajoute un rang dans la liste des rangs caract�res

Dim aRg As MaqToGui
Dim aRep
Dim aNode As Node

'-> Demander le nom de l'imprimante
Rep = InputBox("Veuillez saisir le nouveau RG", "Ajouter un RG")

'-> Quitter si blanc
If Trim(Rep) = "" Then Exit Sub

Rep = Trim(Rep)

'-> V�rifier si le rang saisi n'existe pas d�ja
For Each aRg In MaqToGuis
    If Trim(UCase$(aRg.Name)) = Trim(UCase$(Rep)) Then
        MsgBox "Impossible de cr�er le nouveau rang : le RG sp�cifi� existe d�ja", vbCritical + vbOKOnly, "Erreur"
        Exit Sub
    End If
Next

'-> Cr�er un nouveau RG
Set aRg = New MaqToGui

'-> Propri�t�s
aRg.Name = Rep

'-> Ajout dans la collection
MaqToGuis.Add aRg, "RG|" & UCase$(Trim(Rep))

'-> Ajouter dans la liste
Set aNode = Me.TreeView1.Nodes.Add(, , "RG|" & UCase$(Trim(Rep)), aRg.Name)

'-> S�lectionner le node dans le treeview
Set Me.TreeView1.SelectedItem = aNode

'-> Simuler un click
TreeView1_Click

'-> Gestion des boutons
GestBoutons

End Sub

Private Sub cmdCompleteNom_Click()

Dim Rep As String
Dim aRg As MaqToGui

'-> Pointer sur le rang
Set aRg = MaqToGuis(Me.TreeView1.SelectedItem.Key)

'-> Demander le nouveau compl�ment
Rep = InputBox("Veuillez saisir le compl�ment sur le titre du rang (? pour remettre � blanc )", "Compl�ment", aRg.Complement)

'-> Quitter si blanc
If Trim(Rep) = "" Then Exit Sub

'-> Supprimer si ?
If Trim(Rep) = "?" Then
    aRg.Complement = ""
Else
    aRg.Complement = Rep
End If

'-> Afficher le r�sultat
Me.lblComplement = aRg.Complement

End Sub

Private Sub cmdDelObj_Click()

'---> Cette commande surpprime un objet d'une affectation

Dim aRg As MaqToGui
Dim Rep As Integer
Dim i As Integer, j As Integer
Dim DefField As String

'-> Quitter si pas de nodes
If Me.TreeView2.Nodes.Count = 0 Then Exit Sub

'-> Quitter si pas de nodes s�lectionn�
If Me.TreeView2.SelectedItem Is Nothing Then Exit Sub

'-> Demander confirmation
Rep = MsgBox("Supprimer l'objet s�lectionn� : " & Chr(13) & Me.TreeView2.SelectedItem.Text, vbQuestion + vbYesNo, "Confirmation")
If Rep = vbNo Then Exit Sub

'-> Pointer sur l'obte rang
Set aRg = MaqToGuis(Me.TreeView1.SelectedItem.Key)

'-> Supprimer l'affeactation dans la bonne liste
If Entry(1, Me.TreeView2.SelectedItem.Key, "~") = "SECTION" Then
    For i = 1 To NumEntries(aRg.ListeObj, "|")
        '-> Get de la definition d'une affectation
        DefField = Entry(i, aRg.ListeObj, "|")
        '-> Tester que l'on analyse une section de texte
        If Entry(1, DefField, "~") = "SECTION" Then
            If UCase$(Entry(2, DefField, "~")) = Entry(2, Me.TreeView2.SelectedItem.Key, "~") Then
                '-> Suppimer l'entr�e
                aRg.ListeObj = DeleteEntry(aRg.ListeObj, i, "|")
            End If
        End If 'Si on analyse une section de texte
    Next
Else
    '-> Suppression d'un block de tableau
    For i = 1 To NumEntries(aRg.ListeObj, "|")
        '-> Get de la definition d'une affectation
        DefField = Entry(i, aRg.ListeObj, "|")
        '-> Tester que l'on analyse un tableau
        If Entry(1, DefField, "~") = "TABLEAU" Then
            If Entry(2, DefField, "~") = Entry(2, Me.TreeView2.SelectedItem.Key, "~") Then
                '-> V�rifier que le nom du block de tableau est le m�me
                If Entry(3, DefField, "~") = Entry(3, Me.TreeView2.SelectedItem.Key, "~") Then
                    '-> Supprimer l'entr�e
                    aRg.ListeObj = DeleteEntry(aRg.ListeObj, i, "|")
                End If
            End If 'Si bon tableau
        End If 'Si on analyse un tableau
    Next 'Pour toutes les affectations de tableau
End If

'-> R�afficher les affectations
Display_Affectation aRg

'-> S�lectionner le premier objet s'il y en a
If Me.TreeView2.Nodes.Count <> 0 Then Me.TreeView2.Nodes(1).Selected = True

'-> Gestion des boutons
GestBoutons

End Sub

Private Sub cmdDelRG_Click()

'---> Cette prc�dure supprime un objet rang
Dim Rep As Integer
Dim aRg As MaqToGui

'-> Quitter si pas de nodes
If Me.TreeView1.Nodes.Count = 0 Then Exit Sub

'-> Quitter si pas de nade s�lectionn�
If Me.TreeView1.SelectedItem Is Nothing Then Exit Sub
 
'-> Demander confirmation
Rep = MsgBox("Supprimer le rang : " & Me.TreeView1.SelectedItem.Text, vbQuestion + vbOKOnly, "Confirmation")
If Rep = vbNo Then Exit Sub

'-> Supprimer le rang de la collection
MaqToGuis.Remove (Me.TreeView1.SelectedItem.Key)

'-> Vider le treeview 2
Me.TreeView2.Nodes.Clear

'-> Supprimer du treeview1
Me.TreeView1.Nodes.Remove (Me.TreeView1.SelectedItem.Key)

'-> S�lectionner un autre rang
If Me.TreeView1.Nodes.Count <> 0 Then
    Me.TreeView1.Nodes(1).Selected = True
    TreeView1_Click
End If

'-> Gestion des boutons
GestBoutons

End Sub

Private Sub cmdLigMaq_Click()

Dim Rep
Dim aMaq As Maquette

'-> Pointer sur la maquette
Set aMaq = Maquettes("MAQUETTE")

'-> Demander le nouveau lig
Rep = InputBox("Modification du lig", "Lig", aMaq.Lig)

'-> Quitter si annuler
If Trim(Rep) = "" Then Exit Sub

'-> V�rifier si num�rique
If Not IsNumeric(Rep) Then
    MsgBox "Valeur de propri�t� incorrecte", vbOKOnly + vbCritical, "Erreur"
    Exit Sub
End If

'-> Valeur de maxi
If CDbl(Lig) > 999 Then
    MsgBox "Valeur de propri�t� incorrecte", vbOKOnly + vbCritical, "Erreur"
    Exit Sub
End If

'-> Mettre � jour la propri�t� Lig de la maquette
aMaq.Lig = CInt(Rep)

'-> Mettre � jour le libelle
Me.lblLig = aMaq.Lig

End Sub

Private Sub cmdLigRG_Click()

Dim Rep
Dim aRg As MaqToGui

'-> Pointer sur le RG
Set aRg = MaqToGuis(Me.TreeView1.SelectedItem.Key)

'-> Demander le nouveau lig
Rep = InputBox("Modification du lig", "Lig", aRg.NbLig)

'-> Quitter si annuler
If Trim(Rep) = "" Then Exit Sub

'-> V�rifier si num�rique
If Not IsNumeric(Rep) Then
    MsgBox "Valeur de propri�t� incorrecte", vbOKOnly + vbCritical, "Erreur"
    Exit Sub
End If

'-> Valeur de maxi
If CDbl(Rep) > 999 Then
    MsgBox "Valeur de propri�t� incorrecte", vbOKOnly + vbCritical, "Erreur"
    Exit Sub
End If

'-> Mettre � jour la propri�t� Lig de l'objet
aRg.NbLig = CInt(Rep)

'-> Modifier l'affichage
Me.lblNbLig = aRg.NbLig

End Sub

Private Sub cmdSaisieComplement_Click()

frmBasMaquette.Show vbModal

End Sub

Private Sub Form_Load()

Dim aMaq As Maquette
Dim aRg As MaqToGui
Dim aNode As Node

'-> Pointer sur la maquette en cours
Set aMaq = Maquettes("MAQUETTE")

'-> Afficher le LIG de la maquette caract�re
Me.lblLig = aMaq.Lig

'-> Analyse des rangs existants pour affichage
For Each aRg In MaqToGuis
    '-> Afficher dans la liste d�roulante
    Me.TreeView1.Nodes.Add , , "RG|" & UCase$(Trim(aRg.Name)), aRg.Name
Next

'-> D�clencher la mise � jour de la liste des objets
If Me.TreeView1.Nodes.Count <> 0 Then
    '-> S�lectionner le premier node
    Me.TreeView1.Nodes(1).Selected = True
    TreeView1_Click
End If

'-> Lancer la gestion des boutons
GestBoutons

End Sub


Private Sub GestBoutons()

'---> Cette proc�dure g�re l'etat de chaque bouton

Dim EnableProp As Boolean
Dim EnableDelRg As Boolean
Dim EnableAddObj As Boolean
Dim EnableDelObj As Boolean


If Me.TreeView1.Nodes.Count = 0 Then
    '-> Prop = false
    EnableProp = False
    '-> Del rg
    EnableDelRg = False
    '-> Add obj
    EnableAddObj = False
    '-> Del obj
    EnableDelObj = False
Else
    '-> Prop
    EnableProp = True
    '-> Del rg
    EnableDelRg = True
    '-> Ajouter un objet
    EnableAddObj = True
    '-> Tester s'il y a des objets affich�s
    If Me.TreeView2.Nodes.Count = 0 Then
        EnableDelObj = False
    Else
        EnableDelObj = True
    End If
End If


'-> Mettre � jour l'interface
Me.Frame1.Enabled = EnableProp
Me.cmdDelRG.Enabled = EnableDelRg
Me.cmdAddObj.Enabled = EnableAddObj
Me.cmdDelObj.Enabled = EnableDelObj

End Sub



Private Sub Image1_Click()

Dim NextKey As String
Dim CurrentKey As String
Dim CurrentText As String
Dim aNode As Node
Dim PrecedentKey As String
Dim aRg As MaqToGui
Dim i As Integer

On Error GoTo GestError

'-> R�cup�rer la cl� du node suivant
PrecedentKey = Me.TreeView2.SelectedItem.Next.Key
CurrentKey = Me.TreeView2.SelectedItem.Key
CurrentText = Me.TreeView2.SelectedItem.Text

'-> Supprimer le node en cours
Me.TreeView2.Nodes.Remove (Me.TreeView2.SelectedItem.Key)

'-> Cr�er le node � la bonne position
Me.TreeView2.SetFocus
Set aNode = Me.TreeView2.Nodes.Add(PrecedentKey, 2, CurrentKey, CurrentText)
Set Me.TreeView2.SelectedItem = aNode
aNode.EnsureVisible

'-> Pointer sur l'objet rang
Set aRg = MaqToGuis(Me.TreeView1.SelectedItem.Key)

'-> Mettre � blanc sa matrice
aRg.ListeObj = ""

'-> Pointer sur le premier node
Set aNode = Me.TreeView2.Nodes(1).Root

'-> Ajouter dans la liste des objets
If aRg.ListeObj = "" Then
    aRg.ListeObj = aNode.Key
Else
    aRg.ListeObj = aRg.ListeObj & "|" & aNode.Key
End If

'-> Recr�er les matrices du rang
For i = 2 To Me.TreeView2.Nodes.Count
    '-> Pointer sur le node suivant
    Set aNode = aNode.Next
    If aRg.ListeObj = "" Then
        aRg.ListeObj = aNode.Key
    Else
        aRg.ListeObj = aRg.ListeObj & "|" & aNode.Key
    End If
Next

GestError:

    Exit Sub
    
End Sub

Private Sub Image2_Click()

Dim PrecedentKey As String
Dim CurrentKey As String
Dim CurrentText As String
Dim aNode As Node
Dim i As Integer
Dim aRg As MaqToGui

On Error GoTo GestError

'-> Ne pas remonter si on est le premier node

'-> R�cup�rer la cl� du node pr�c�dent
PrecedentKey = Me.TreeView2.SelectedItem.Previous.Key
CurrentKey = Me.TreeView2.SelectedItem.Key
CurrentText = Me.TreeView2.SelectedItem.Text

'-> Supprimer le node en cours
Me.TreeView2.Nodes.Remove (Me.TreeView2.SelectedItem.Key)

'-> Cr�er le node � la bonne position
Me.TreeView2.SetFocus
Set aNode = Me.TreeView2.Nodes.Add(PrecedentKey, 3, CurrentKey, CurrentText)
Set Me.TreeView2.SelectedItem = aNode
aNode.EnsureVisible

'-> Pointer sur l'objet rang
Set aRg = MaqToGuis(Me.TreeView1.SelectedItem.Key)

'-> Mettre � blanc sa matrice
aRg.ListeObj = ""

'-> Pointer sur le premier node
Set aNode = Me.TreeView2.Nodes(1).Root

'-> Ajouter dans la liste des objets
If aRg.ListeObj = "" Then
    aRg.ListeObj = aNode.Key
Else
    aRg.ListeObj = aRg.ListeObj & "|" & aNode.Key
End If

'-> Recr�er les matrices du rang
For i = 2 To Me.TreeView2.Nodes.Count
    '-> Pointer sur le node suivant
    Set aNode = aNode.Next
    If aRg.ListeObj = "" Then
        aRg.ListeObj = aNode.Key
    Else
        aRg.ListeObj = aRg.ListeObj & "|" & aNode.Key
    End If
Next

GestError:

    Exit Sub
    
End Sub

Private Sub TreeView1_Click()

Dim aRg As MaqToGui

'-> Quitter si pas de nodes
If Me.TreeView1.Nodes.Count = 0 Then Exit Sub

'-> Quitter si pas de node s�lectionn�
If Me.TreeView1.SelectedItem Is Nothing Then Exit Sub

'-> Pointer sur le Rg s�lectionn�
Set aRg = MaqToGuis(Me.TreeView1.SelectedItem.Key)

'-> Afficher ses propri�t�s
Me.lblNomRang = aRg.Name
Me.lblNbLig = aRg.NbLig
If aRg.IsSaut Then
    Me.Check1.Value = 1
Else
    Me.Check1.Value = 0
End If
Me.lblComplement = aRg.Complement

'-> Afficher la liste des s�lections de ce rang
Display_Affectation aRg

'-> S�lectionner le premier �l�ment
If Me.TreeView2.Nodes.Count <> 0 Then Me.TreeView2.Nodes(1).Selected = True

'-> Gestion des boutons
GestBoutons

End Sub


Private Sub Display_Affectation(aRg As MaqToGui)

Dim i As Integer
Dim DefBlock As String
Dim aNode As Node

'-> Vider le tree
Me.TreeView2.Nodes.Clear

'-> Quitter si rien � affecter
If Trim(aRg.ListeObj) = "" Then Exit Sub

'-> Afficher la liste des objets affect� � ce rang
For i = 1 To NumEntries(aRg.ListeObj, "|")
    '-> Get de la definition d'un block
    DefBlock = Entry(i, aRg.ListeObj, "|")
    '-> Tester si on affiche un tableau ou une section
    If Entry(1, DefBlock, "~") = "SECTION" Then
        '-> Afficher la section
        Me.TreeView2.Nodes.Add , , "SECTION~" & Entry(2, DefBlock, "~"), "Section de texte : " & Entry(2, DefBlock, "~")
    Else
        '-> Afficher le tableau
        Me.TreeView2.Nodes.Add , , "TABLEAU~" & Entry(2, DefBlock, "~") & "~" & Entry(3, DefBlock, "~"), "Tableau : " & Entry(2, DefBlock, "~") & " -> : " & Entry(3, DefBlock, "~")
    End If 'Si on affiche une section ou un tableau
Next 'Pour tous les objets affect�s � ce rang

End Sub
