Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Friend Class QRNumber
	'---------------------------------------------------------------
	' QRNumber
	'---------------------------------------------------------------

	Dim aQRUtil As QRUtil
	Dim aQRData As QRData
	Public Sub New()
		MyBase.New()
		aQRUtil = New QRUtil()
		aQRData = New QRData()
	End Sub

	Public Function QRNumber(ByVal data As Object) As Object

		aQRData.QRData(QR_MODE_NUMBER, data)
	End Function

	Public Function writeB(ByVal buffer As Object) As Object
		Dim numB As Object

        Dim data As Object = aQRData.getData()
        Dim i As Integer = 0
        Do While (i + 2 < Strings.Len(CStr(data)))
            numB = parseInt(Strings.Mid(CStr(data), i, 3))
            buffer.put(numB, 10)
            i += 3
        Loop

        If i < Strings.Len(CStr(data)) Then
            If Strings.Len(CStr(data)) - i = 1 Then
                numB = parseInt(Strings.Mid(CStr(data), i, i + 1))
                buffer.put(numB, 4)
            ElseIf Strings.Len(CStr(data)) - i = 2 Then
                numB = parseInt(Strings.Mid(CStr(data), i, i + 2))
                buffer.put(numB, 7)
            End If
        End If
    End Function

	Public Function getLength() As Object
        Return Strings.Len(CStr(aQRData.getData()))
    End Function

	Public Function parseInt(ByVal s As Object) As Object

		num = 0
        Dim tempForEndVar As Integer = Strings.Len(CStr(s)) - 1
        For i As Integer = 0 To tempForEndVar
            num = CDbl(num) * 10 + CDbl(parseIntAt(Strings.Asc(CStr(s.GetValue(i))(0))))
        Next
		Return num
	End Function

	Public Function parseIntAt(ByVal c As Object) As Object

        Dim result As Object = Nothing
        If aQRUtil.toCharCode("0") <= c And c <= aQRUtil.toCharCode("9") Then
            result = CDbl(c) - CDbl(aQRUtil.toCharCode("0"))
        End If

		trigger_error("illegal char : c", E_USER_ERROR)
		Return result
	End Function
End Class