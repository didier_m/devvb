Option Strict Off
Option Explicit On
Imports System
Imports UpgradeHelpers.Helpers
Friend Class QRMath
    '---------------------------------------------------------------
    ' QRMath
    '---------------------------------------------------------------

    Private QR_MATH_EXP_TABLE() As Integer
    Private QR_MATH_LOG_TABLE() As Integer

	Public Sub New()
		MyBase.New()

        '-> on initialise la variable QR_MATH_EXP_TABLE
        QR_MATH_EXP_TABLE = createNumArray(256)
        For i As Integer = 0 To 7
			QR_MATH_EXP_TABLE(i) = ShiftLeft(1, i)
		Next
		For i As Integer = 8 To 255
			QR_MATH_EXP_TABLE(i) = QR_MATH_EXP_TABLE(i - 4) Xor QR_MATH_EXP_TABLE(i - 5) Xor QR_MATH_EXP_TABLE(i - 6) Xor QR_MATH_EXP_TABLE(i - 8)
		Next

		'-> on initialise la variable QR_MATH_LOG_TABLE
		QR_MATH_LOG_TABLE = createNumArray(256)
		For i As Integer = 0 To 254
			QR_MATH_LOG_TABLE(CInt(QR_MATH_EXP_TABLE(i))) = i
		Next

	End Sub

	Public Function createNumArray(ByVal length As Integer) As Integer()

		Dim num_array(length) As Integer
		For i As Integer = 0 To length - 1
			num_array(i) = 0
		Next
		Return num_array
	End Function

	Public Function glog(ByVal n As Object) As Object
        If CDbl(n) < 1 Then
            trigger_error("log(n)", E_USER_ERROR)
        End If
        Return QR_MATH_LOG_TABLE(CInt(n))
    End Function

	Public Function gexp(ByRef n As Object) As Object
        Do While CDbl(n) < 0
            n = CDbl(n) + 255
        Loop
        Do While CDbl(n) >= 256
            n = CDbl(n) - 255
        Loop
        Return QR_MATH_EXP_TABLE(CInt(n))
    End Function


	'-> Fonctions pour g�rer les operations binaires

	Function ShiftRight(ByVal lngNumber As Integer, ByVal intNumBits As Integer) As Integer
		'--------------
		'BIT SHIFT RIGHT
		'--------------

		Return lngNumber \ 2 ^ intNumBits 'note the integer division op

	End Function


	Function ShiftLeft(ByVal lngNumber As Integer, ByVal intNumBits As Integer) As Integer
		'--------------
		'BIT SHIFT LEFT
		'--------------

		Return lngNumber * 2 ^ intNumBits

	End Function
End Class