﻿Imports System.ComponentModel

Imports System.Drawing

Imports System.Text

Imports System.Windows.Forms

Imports System.Runtime.InteropServices

Class Win32API

    <DllImport("KERNEL32.DLL", EntryPoint:="RtlMoveMemory",
 SetLastError:=True, CharSet:=CharSet.Auto,
 ExactSpelling:=True,
 CallingConvention:=CallingConvention.StdCall)>
    Public Shared Sub CopyArrayTo(<[In](), MarshalAs(UnmanagedType.I4)> ByVal hpvDest As Int32, <[In](), Out()> ByVal hpvSource() As Byte, ByVal cbCopy As Integer)
        ' Leave function empty - DLLImport attribute forwards calls to CopyArrayTo to
        ' RtlMoveMemory in KERNEL32.DLL.
    End Sub


End Class

Public Class Form1
    Inherits Form


    <DllImport("USER32.dll")>
    Private Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal msg As Integer, ByVal wParam As Integer, ByVal lParam As IntPtr) As Int32
    End Function
    <DllImport("gdi32.dll")>
    Public Shared Function SaveDC(ByVal hdc As Integer) As Integer
    End Function

    <DllImport("gdi32.dll")>
    Public Shared Function RestoreDC(ByVal hdc As Integer, ByVal nSavedDC As Integer) As Boolean
    End Function

    <DllImport("gdi32.dll")>
    Public Shared Function SetMapMode(ByVal hdc As Integer, ByVal fnMapMode As Integer) As Integer
    End Function

    <DllImport("gdi32.dll")>
    Public Shared Function SetWindowExtEx(ByVal hdc As Integer, ByVal nXExtent As Integer, ByVal nYExtent As Integer, ByRef size As Size) As Boolean
    End Function

    <DllImport("gdi32.dll")>
    Public Shared Function SetViewportExtEx(ByVal hdc As Integer, ByVal nXExtent As Integer, ByVal nYExtent As Integer, ByRef size As Size) As Boolean
    End Function

    Private Const WM_USER As Integer = &H400

    Private Const EM_FORMATRANGE As Integer = WM_USER + 57


    <StructLayout(LayoutKind.Sequential)>
    Private Structure RECT

        Public Left As Integer

        Public Top As Integer

        Public Right As Integer

        Public Bottom As Integer

    End Structure


    <StructLayout(LayoutKind.Sequential)>
    Private Structure CHARRANGE

        Public cpMin As Integer

        Public cpMax As Integer

    End Structure


    <StructLayout(LayoutKind.Sequential)>
    Private Structure FORMATRANGE

        Public hdc As IntPtr

        Public hdcTarget As IntPtr

        Public rc As RECT

        Public rcPage As RECT

        Public chrg As CHARRANGE

    End Structure

    Private Const inch As Double = 14.4
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox

    Private contentRectangle As Rectangle

    Public Sub New()


        InitializeComponent()
    End Sub

    <System.Runtime.InteropServices.DllImport("gdi32.dll")>
    Public Shared Function DeleteObject(hObject As IntPtr) As Boolean
    End Function

    <System.Runtime.InteropServices.DllImport("user32.dll")>
    Public Shared Function GetDC(hwnd As IntPtr) As IntPtr
    End Function

    <System.Runtime.InteropServices.DllImport("gdi32.dll")>
    Public Shared Function CreateCompatibleDC(hdc As IntPtr) As IntPtr
    End Function

    <System.Runtime.InteropServices.DllImport("user32.dll")>
    Public Shared Function ReleaseDC(hwnd As IntPtr, hdc As IntPtr) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("gdi32.dll")>
    Public Shared Function DeleteDC(hdc As IntPtr) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("gdi32.dll")>
    Public Shared Function SelectObject(hdc As IntPtr, hgdiobj As IntPtr) As IntPtr
    End Function

    <System.Runtime.InteropServices.DllImport("gdi32.dll")>
    Public Shared Function BitBlt(hdcDst As IntPtr, xDst As Integer, yDst As Integer, w As Integer, h As Integer, hdcSrc As IntPtr,
    xSrc As Integer, ySrc As Integer, rop As Integer) As Integer
    End Function
    Shared SRCCOPY As Integer = &HCC0020

    <System.Runtime.InteropServices.DllImport("gdi32.dll")>
    Private Shared Function CreateDIBSection(hdc As IntPtr, ByRef bmi As BITMAPINFO, Usage As UInteger, ByRef bits As IntPtr, hSection As IntPtr, dwOffset As UInteger) As IntPtr
    End Function
    Shared BI_RGB As UInteger = 0
    Shared DIB_RGB_COLORS As UInteger = 0
    <System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)>
    Public Structure BITMAPINFO
        Public biSize As UInteger
        Public biWidth As Integer, biHeight As Integer
        Public biPlanes As Short, biBitCount As Short
        Public biCompression As UInteger, biSizeImage As UInteger
        Public biXPelsPerMeter As Integer, biYPelsPerMeter As Integer
        Public biClrUsed As UInteger, biClrImportant As UInteger
        <System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst:=256)>
        Public cols As UInteger()
    End Structure

    Private Shared Function MAKERGB(r As Integer, g As Integer, b As Integer) As UInteger
        Return CUInt(b And 255) Or CUInt((r And 255) << 8) Or CUInt((g And 255) << 16)
    End Function

    Public Sub SaveAsJpegButton()
        tempImg = GetTempFileNameVB("img") + ".jpg"
        RichTextBox1.Height = RichTextBox1.Height * 3.125
        RichTextBox1.Width = RichTextBox1.Width * 3.125
        contentRectangle.Height = RichTextBox1.Height
        contentRectangle.Width = RichTextBox1.Width
        RtbToBitmap(RichTextBox1, contentRectangle, tempImg)
        tempImgLst = tempImgLst + "," + tempImg
    End Sub


    Public Sub RtbToBitmap(ByVal rtb As RichTextBox, ByVal rectangle As Rectangle, ByVal fileName As String)

        Dim j, r As Double
        j = 1.15
        r = 300
        Dim bmp As New Bitmap(CInt(rectangle.Width * j), CInt(rectangle.Height * j))
        'bmp.SetResolution(31, 31)
        'bmp.MakeTransparent()
        Using gr As Graphics = Graphics.FromImage(bmp)
            gr.FillRectangle(Brushes.White, 0, 0, CInt(rectangle.Width * j), CInt(rectangle.Height * j))
            Dim hDC As IntPtr = gr.GetHdc()
            Dim fmtRange As FORMATRANGE
            Dim rect As RECT
            Dim fromAPI As Integer
            Dim w, h As Integer

            w = CInt(bmp.Width * 1 / j)
            h = CInt(bmp.Height * 1 / j)

            rect.Top = 0
            rect.Left = 0
            rect.Bottom = CInt(Math.Truncate(h + (h * (bmp.HorizontalResolution / 100)) * inch))
            rect.Right = CInt(Math.Truncate(w + (w * (bmp.VerticalResolution / 100)) * inch))

            fmtRange.chrg.cpMin = 0
            fmtRange.chrg.cpMax = -1
            fmtRange.hdc = hDC
            fmtRange.hdcTarget = hDC
            fmtRange.rc = rect
            fmtRange.rcPage = rect

            Dim wParam As Integer = 1
            Dim lParam As IntPtr = Marshal.AllocCoTaskMem(Marshal.SizeOf(fmtRange))
            SetMapMode(fmtRange.hdc, 8)
            Dim s, s2 As New Size()
            SetWindowExtEx(fmtRange.hdc, w, h, s)
            SetViewportExtEx(fmtRange.hdc, w * r \ 96, h * r \ 96, s2)

            Marshal.StructureToPtr(fmtRange, lParam, False)
            fromAPI = SendMessage(rtb.Handle, EM_FORMATRANGE, wParam, lParam)
            Marshal.FreeCoTaskMem(lParam)

            fromAPI = SendMessage(rtb.Handle, EM_FORMATRANGE, wParam, New IntPtr(0))
            gr.ReleaseHdc(hDC)

        End Using
        'Dim encoderParams As System.Drawing.Imaging.EncoderParameters = New System.Drawing.Imaging.EncoderParameters
        'Dim myImageCodecInfo As Imaging.ImageCodecInfo
        'Dim myEncoder As System.Drawing.Imaging.Encoder = System.Drawing.Imaging.Encoder.Compression
        'Dim myEncoderParameter As Imaging.EncoderParameter
        'Dim myEncoderParameters As New Imaging.EncoderParameters(1)
        'myImageCodecInfo = GetEncoder(Imaging.ImageFormat.Jpeg)
        'myEncoderParameter = New Imaging.EncoderParameter(myEncoder, 50)
        'myEncoderParameters.Param(0) = myEncoderParameter
        bmp.Save(fileName, Imaging.ImageFormat.Jpeg) ', myImageCodecInfo, myEncoderParameters)
        'SaveGIFWithNewColorTable(bmp, fileName, 128, True)
        bmp.Dispose()
    End Sub


    Private Sub SaveGIFWithNewColorTable(ByVal image As Image, ByVal filename As String, ByVal nColors As Integer, ByVal fTransparent As Boolean)
        Try

            ' GIF codec supports 256 colors maximum, monochrome minimum.
            If (nColors > 256) Then
                nColors = 256
            End If

            If (nColors < 2) Then
                nColors = 2
            End If

            ' Make a new 8-BPP indexed bitmap that is the same size as the source image.
            Dim Width As Integer = image.Width
            Dim Height As Integer = image.Height

            ' Always use PixelFormat8BppIndexed because that is the color
            ' table based interface to the GIF codec.
            Dim bitmap As Bitmap = New Bitmap(Width, Height, Imaging.PixelFormat.Format8bppIndexed)

            ' Create a color palette big enough to hold the colors you want.
            Dim pal As Imaging.ColorPalette = GetColorPalette(nColors)

            ' Initialize a new color table with entries that are determined
            ' by some optimal palette-finding algorithm; for demonstration 
            ' purposes, use a grayscale.
            Dim i As Integer
            For i = 0 To nColors - 1
                Dim Alpha As Integer = 255             ' Colors are opaque
                Dim Intensity As Double = CDbl(i) * 255 / (nColors - 1) ' even distribution 

                ' The GIF encoder makes the first entry in the palette
                ' with a ZERO alpha the transparent color in the GIF.
                ' Pick the first one arbitrarily, for demonstration purposes.

                If (i = 0 And fTransparent) Then    ' Make this color index...
                    Alpha = 0                       ' Transparent
                End If

                ' Create a gray scale for demonstration purposes.
                ' Otherwise, use your favorite color reduction algorithm 
                ' and an optimum palette for that algorithm generated here.
                ' For example, a color histogram, or a median cut palette.
                pal.Entries(i) = Color.FromArgb(Alpha, Intensity, Intensity, Intensity)

            Next i

            ' Set the palette into the new Bitmap object.
            bitmap.Palette = pal


            ' Use GetPixel below to pull out the color data of
            ' image because GetPixel isn't defined on an Image; make a copy 
            ' in a Bitmap instead. Next, make a new Bitmap that is the same 
            ' size as the image that you want to export. Or, try to interpret
            ' the native pixel format of the image by using a LockBits
            ' call. Use PixelFormat32BppARGB so you can wrap a graphics  
            ' around it.
            Dim BmpCopy As Bitmap = New Bitmap(Width, Height, Imaging.PixelFormat.Format32bppPArgb)

            Dim g As Graphics
            g = Graphics.FromImage(BmpCopy)

            g.PageUnit = GraphicsUnit.Pixel

            ' Transfer the Image to the Bitmap.
            g.DrawImage(image, 0, 0, Width, Height)

            ' Force g to release its resources, namely BmpCopy.
            g.Dispose()

            ' Lock a rectangular portion of the bitmap for writing.
            Dim bitmapData As Imaging.BitmapData
            Dim rect As Rectangle = New Rectangle(0, 0, Width, Height)

            bitmapData = bitmap.LockBits(rect, Imaging.ImageLockMode.WriteOnly, Imaging.PixelFormat.Format8bppIndexed)

            ' Write to a temporary buffer, and then copy to the buffer that
            ' LockBits provides. Copy the pixels from the source image in this
            ' loop. Because you want an index, convert RGB to the appropriate
            ' palette index here.
            Dim pixels As IntPtr = bitmapData.Scan0
            Dim bits As Byte()      ' the working buffer

            ' Get the pointer to the image bits.
            Dim pBits As Int32

            If (bitmapData.Stride > 0) Then
                pBits = pixels.ToInt32()
            Else
                ' If the Stide is negative, Scan0 points to the last
                ' scanline in the buffer. To normalize the loop, obtain
                ' a pointer to the front of the buffer that is located 
                ' (Height-1) scanlines previous.
                pBits = pixels.ToInt32() + bitmapData.Stride * (Height - 1)
            End If

            Dim stride As Integer = Math.Abs(bitmapData.Stride)
            ReDim bits(Height * stride) ' Allocate the working buffer.

            Dim row As Integer
            Dim col As Integer

            For row = 0 To Height - 1
                For col = 0 To Width - 1
                    ' Map palette indices for a gray scale.
                    ' Put your favorite color reduction algorithm here.
                    ' If you use some other technique to color convert.
                    Dim pixel As Color      ' The source pixel.

                    ' The destination pixel.
                    Dim i8BppPixel As Integer = row * stride + col

                    pixel = BmpCopy.GetPixel(col, row)

                    ' Use luminance/chrominance conversion to get grayscale.
                    ' Basically, turn the image into black and white TV.
                    ' Do not calculate Cr or Cb because you 
                    ' discard the color anyway.
                    ' Y = Red * 0.299 + Green * 0.587 + Blue * 0.114

                    ' This expression should be integer math for performance;
                    ' however, because GetPixel above is the slowest part of 
                    ' this loop, the expression is left as floating point
                    ' for clarity.
                    Dim luminance As Double = (pixel.R * 0.299) +
                                        (pixel.G * 0.587) +
                                        (pixel.B * 0.114)

                    ' Gray scale is an intensity map from black to white.
                    ' Compute the index to the grayscale entry that
                    ' approximates the luminance, and then round the index.    
                    ' Also, constrain the index choices by the number of
                    ' colors to do, and then set that pixel's index to the byte
                    ' value.
                    Dim colorIndex As Double = Math.Round((luminance * (nColors - 1) / 255))

                    bits(i8BppPixel) = CByte(colorIndex)

                    ' /* end loop for col */ 
                Next col
                ' /* end loop for row */ 
            Next row

            ' Put the image bits definition into the bitmap.
            Win32API.CopyArrayTo(pBits, bits, Height * stride)

            ' To commit the changes, unlock the portion of the bitmap. 
            bitmap.UnlockBits(bitmapData)

            bitmap.Save(filename, Imaging.ImageFormat.Gif)

            ' Bitmap goes out of scope here and is also marked for
            ' garbage collection.
            ' Pal is referenced by bitmap and goes away.
            ' BmpCopy goes out of scope here and is marked for garbage
            ' collection. Force it, because it is probably quite large.
            ' The same applies for bitmap.
            BmpCopy.Dispose()
            bitmap.Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Protected Function GetColorPalette(nColors As UInteger) As Imaging.ColorPalette
        ' Assume monochrome image.
        Dim bitscolordepth As Imaging.PixelFormat = Imaging.PixelFormat.Format1bppIndexed
        Dim palette As Imaging.ColorPalette
        ' The Palette we are stealing
        Dim bitmap As Bitmap
        ' The source of the stolen palette
        ' Determine number of colors.
        If nColors > 2 Then
            bitscolordepth = Imaging.PixelFormat.Format4bppIndexed
        End If
        If nColors > 16 Then
            bitscolordepth = Imaging.PixelFormat.Format8bppIndexed
        End If

        ' Make a new Bitmap object to get its Palette.
        bitmap = New Bitmap(1, 1, bitscolordepth)

        palette = bitmap.Palette
        ' Grab the palette
        bitmap.Dispose()
        ' cleanup the source Bitmap
        Return palette
        ' Send the palette back
    End Function

    ''' <summary>  
    ''' Returns a transparent background GIF image from the specified Bitmap.  
    ''' </summary>  
    ''' <param name="bitmap">The Bitmap to make transparent.</param>  
    ''' <param name="color">The Color to make transparent.</param>  
    ''' <returns>New Bitmap containing a transparent background gif.</returns>  
    Public Shared Function MakeTransparentGif(bitmap As Bitmap, color As Color) As Bitmap
        Dim R As Byte = color.R
        Dim G As Byte = color.G
        Dim B As Byte = color.B
        Dim fin As New IO.MemoryStream()
        bitmap.Save(fin, System.Drawing.Imaging.ImageFormat.Gif)
        Dim fout As New IO.MemoryStream(CInt(fin.Length))
        Dim count As Integer = 0
        Dim buf As Byte() = New Byte(255) {}
        Dim transparentIdx As Byte = 0
        fin.Seek(0, IO.SeekOrigin.Begin)
        'header  
        count = fin.Read(buf, 0, 13)
        If (buf(0) <> 71) OrElse (buf(1) <> 73) OrElse (buf(2) <> 70) Then
            Return Nothing
        End If
        'GIF  
        fout.Write(buf, 0, 13)
        Dim i As Integer = 0
        If (buf(10) And &H80) > 0 Then
            i = If(1 << ((buf(10) And 7) + 1) = 256, 256, 0)
        End If
        While i <> 0
            fin.Read(buf, 0, 3)
            If (buf(0) = R) AndAlso (buf(1) = G) AndAlso (buf(2) = B) Then
                transparentIdx = CByte(256 - i)
            End If
            fout.Write(buf, 0, 3)
            i -= 1
        End While
        Dim gcePresent As Boolean = False
        While True
            fin.Read(buf, 0, 1)
            fout.Write(buf, 0, 1)
            If buf(0) <> &H21 Then
                Exit While
            End If
            fin.Read(buf, 0, 1)
            fout.Write(buf, 0, 1)
            gcePresent = (buf(0) = &HF9)
            While True
                fin.Read(buf, 0, 1)
                fout.Write(buf, 0, 1)
                If buf(0) = 0 Then
                    Exit While
                End If
                count = buf(0)
                If fin.Read(buf, 0, count) <> count Then
                    Return Nothing
                End If
                If gcePresent Then
                    If count = 4 Then
                        buf(0) = buf(0) Or &H1
                        buf(3) = transparentIdx
                    End If
                End If
                fout.Write(buf, 0, count)
            End While
        End While
        While count > 0
            count = fin.Read(buf, 0, 1)
            fout.Write(buf, 0, 1)
        End While
        fin.Close()
        fout.Flush()
        Return New Bitmap(fout)
    End Function


    ''' <summary>
    ''' Copies a bitmap into a 1bpp/8bpp bitmap of the same dimensions, fast
    ''' </summary>
    ''' <param name="b">original bitmap</param>
    ''' <param name="bpp">1 or 8, target bpp</param>
    ''' <returns>a 1bpp copy of the bitmap</returns>
    Private Shared Function CopyToBpp(b As System.Drawing.Bitmap, bpp As Integer) As System.Drawing.Bitmap
        If bpp <> 1 AndAlso bpp <> 8 Then
            Throw New System.ArgumentException("1 or 8", "bpp")
        End If

        ' Plan: built into Windows GDI is the ability to convert
        ' bitmaps from one format to another. Most of the time, this
        ' job is actually done by the graphics hardware accelerator card
        ' and so is extremely fast. The rest of the time, the job is done by
        ' very fast native code.
        ' We will call into this GDI functionality from C#. Our plan:
        ' (1) Convert our Bitmap into a GDI hbitmap (ie. copy unmanaged->managed)
        ' (2) Create a GDI monochrome hbitmap
        ' (3) Use GDI "BitBlt" function to copy from hbitmap into monochrome (as above)
        ' (4) Convert the monochrone hbitmap into a Bitmap (ie. copy unmanaged->managed)

        Dim w As Integer = b.Width, h As Integer = b.Height
        Dim hbm As IntPtr = b.GetHbitmap()
        ' this is step (1)
        '
        ' Step (2): create the monochrome bitmap.
        ' "BITMAPINFO" is an interop-struct which we define below.
        ' In GDI terms, it's a BITMAPHEADERINFO followed by an array of two RGBQUADs
        Dim bmi As New BITMAPINFO()
        bmi.biSize = 40
        ' the size of the BITMAPHEADERINFO struct
        bmi.biWidth = w
        bmi.biHeight = h
        bmi.biPlanes = 1
        ' "planes" are confusing. We always use just 1. Read MSDN for more info.
        bmi.biBitCount = CShort(bpp)
        ' ie. 1bpp or 8bpp
        bmi.biCompression = BI_RGB
        ' ie. the pixels in our RGBQUAD table are stored as RGBs, not palette indexes
        bmi.biSizeImage = CUInt(((w + 7) And &HFFFFFFF8UI) * h / 8)
        bmi.biXPelsPerMeter = 1000000
        ' not really important
        bmi.biYPelsPerMeter = 1000000
        ' not really important
        ' Now for the colour table.
        Dim ncols As UInteger = CUInt(1) << bpp
        ' 2 colours for 1bpp; 256 colours for 8bpp
        bmi.biClrUsed = ncols
        bmi.biClrImportant = ncols
        bmi.cols = New UInteger(255) {}
        ' The structure always has fixed size 256, even if we end up using fewer colours
        If bpp = 1 Then
            bmi.cols(0) = MAKERGB(0, 0, 0)
            bmi.cols(1) = MAKERGB(255, 255, 255)
        Else
            For i As Integer = 0 To ncols - 1
                bmi.cols(i) = MAKERGB(i, i, i)
            Next
        End If
        ' For 8bpp we've created an palette with just greyscale colours.
        ' You can set up any palette you want here. Here are some possibilities:
        ' greyscale: for (int i=0; i<256; i++) bmi.cols[i]=MAKERGB(i,i,i);
        ' rainbow: bmi.biClrUsed=216; bmi.biClrImportant=216; int[] colv=new int[6]{0,51,102,153,204,255};
        '          for (int i=0; i<216; i++) bmi.cols[i]=MAKERGB(colv[i/36],colv[(i/6)%6],colv[i%6]);
        ' optimal: a difficult topic: http://en.wikipedia.org/wiki/Color_quantization
        ' 
        ' Now create the indexed bitmap "hbm0"
        Dim bits0 As IntPtr
        ' not used for our purposes. It returns a pointer to the raw bits that make up the bitmap.
        Dim hbm0 As IntPtr = CreateDIBSection(IntPtr.Zero, bmi, DIB_RGB_COLORS, bits0, IntPtr.Zero, 0)
        '
        ' Step (3): use GDI's BitBlt function to copy from original hbitmap into monocrhome bitmap
        ' GDI programming is kind of confusing... nb. The GDI equivalent of "Graphics" is called a "DC".
        Dim sdc As IntPtr = GetDC(IntPtr.Zero)
        ' First we obtain the DC for the screen
        ' Next, create a DC for the original hbitmap
        Dim hdc As IntPtr = CreateCompatibleDC(sdc)
        SelectObject(hdc, hbm)
        ' and create a DC for the monochrome hbitmap
        Dim hdc0 As IntPtr = CreateCompatibleDC(sdc)
        SelectObject(hdc0, hbm0)
        ' Now we can do the BitBlt:
        BitBlt(hdc0, 0, 0, w, h, hdc,
        0, 0, SRCCOPY)
        ' Step (4): convert this monochrome hbitmap back into a Bitmap:
        Dim b0 As System.Drawing.Bitmap = System.Drawing.Bitmap.FromHbitmap(hbm0)
        '
        ' Finally some cleanup.
        DeleteDC(hdc)
        DeleteDC(hdc0)
        ReleaseDC(IntPtr.Zero, sdc)
        DeleteObject(hbm)
        DeleteObject(hbm0)
        '
        Return b0
    End Function

    Private Function GetEncoder(ByVal format As Imaging.ImageFormat) As Imaging.ImageCodecInfo

        Dim codecs As Imaging.ImageCodecInfo() = Imaging.ImageCodecInfo.GetImageDecoders()

        Dim codec As Imaging.ImageCodecInfo
        For Each codec In codecs
            If codec.FormatID = format.Guid Then
                Return codec
            End If
        Next codec
        Return Nothing

    End Function

    Private Sub richTextBox1_ContentsResized(ByVal sender As Object, ByVal e As ContentsResizedEventArgs)

        contentRectangle = e.NewRectangle

    End Sub

    Private Sub InitializeComponent()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox
        Me.SuspendLayout()
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Location = New System.Drawing.Point(132, 125)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(100, 96)
        Me.RichTextBox1.TabIndex = 0
        Me.RichTextBox1.Text = ""
        '
        'Form1
        '
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.RichTextBox1)
        Me.Name = "Form1"
        Me.ResumeLayout(False)

    End Sub
End Class

