Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Friend Class QRAlphaNum
	'---------------------------------------------------------------
	' QRAlphaNum
	'---------------------------------------------------------------

	Dim aQRData As QRData
	Dim aQRUtil As QRUtil

	Public Sub New()
		MyBase.New()
		aQRData = New QRData()
		aQRUtil = New QRUtil()
	End Sub

	Public Function QRAlphaNum(ByVal data As Object) As Object
		aQRData.QRData(QR_MODE_ALPHA_NUM, data)
	End Function

	'renamed write to writeB
	Public Function writeB(ByVal buffer As Object) As Object

		Dim i As Integer = 0
        Dim c() As String = aQRData.getData()

        Do While i + 1 < c.GetUpperBound(0)
            buffer.put(CDbl(getCode(Strings.Asc(c(i)(0)))) * 45 + CDbl(getCode(Strings.Asc(c(i + 1)(0)))), 11)
            i += 2
		Loop 

		If i < c.GetUpperBound(0) Then
            buffer.put(getCode(Strings.Asc(c(i)(0))), 6)
        End If
	End Function

	Public Function getLength() As Object
        Return Strings.Len(CStr(aQRData.getData()))
    End Function

	Public Function getCode(ByVal c As Object) As Object

        Dim result As Object = Nothing
        If aQRUtil.toCharCode("0") <= c And c <= aQRUtil.toCharCode("9") Then
            Return CDbl(c) - CDbl(aQRUtil.toCharCode("0"))
        ElseIf aQRUtil.toCharCode("A") <= c And c <= aQRUtil.toCharCode("Z") Then
            result = CDbl(c) - CDbl(aQRUtil.toCharCode("A")) + 10
        Else
			Select Case c
				Case aQRUtil.toCharCode(" ")
					result = 36
				Case aQRUtil.toCharCode("")
					result = 37
				Case aQRUtil.toCharCode("%")
					result = 38
				Case aQRUtil.toCharCode("*")
					result = 39
				Case aQRUtil.toCharCode("+")
					result = 40
				Case aQRUtil.toCharCode("-")
					result = 41
				Case aQRUtil.toCharCode(".")
					result = 42
				Case aQRUtil.toCharCode("/")
					result = 43
				Case aQRUtil.toCharCode(":")
					result = 44
				Case Else
					trigger_error("illegal char : c", E_USER_ERROR)
			End Select
		End If

		Return result
	End Function
End Class