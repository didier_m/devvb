Option Strict Off
Option Explicit On
Imports System
Friend Class QR_MAX_LENGTH

	Private qrMaxLenght As Array

	Public Sub New()
		MyBase.New()
        qrMaxLenght = New Object() {New Object() {New Object() {41, 25, 17, 10}, New Object() {34, 20, 14, 8}, New Object() {27, 16, 11, 7}, New Object() {17, 10, 7, 4}}, New Object() {New Object() {77, 47, 32, 20}, New Object() {63, 38, 26, 16}, New Object() {48, 29, 20, 12}, New Object() {34, 20, 14, 8}}, New Object() {New Object() {127, 77, 53, 32}, New Object() {101, 61, 42, 26}, New Object() {77, 47, 32, 20}, New Object() {58, 35, 24, 15}}, New Object() {New Object() {187, 114, 78, 48}, New Object() {149, 90, 62, 38}, New Object() {111, 67, 46, 28}, New Object() {82, 50, 34, 21}}, New Object() {New Object() {255, 154, 106, 65}, New Object() {202, 122, 84, 52}, New Object() {144, 87, 60, 37}, New Object() {106, 64, 44, 27}}, New Object() {New Object() {322, 195, 134, 82}, New Object() {255, 154, 106, 65}, New Object() {178, 108, 74, 45}, New Object() {139, 84, 58, 36}}, New Object() {New Object() {370, 224, 154, 95}, New Object() {293, 178, 122, 75}, New Object() {207, 125, 86, 53}, New Object() {154, 93, 64, 39}}, New Object() {New Object() {461, 279, 192, 118}, New Object() {365, 221, 152, 93}, New Object() {259, 157, 108, 66}, New Object() {202, 122, 84, 52}}, New Object() {New Object() {552, 335, 230, 141}, New Object() {432, 262, 180, 111}, New Object() {312, 189, 130, 80}, New Object() {235, 143, 98, 60}}, New Object() {New Object() {652, 395, 271, 167}, New Object() {513, 311, 213, 131}, New Object() {364, 221, 151, 93}, New Object() {288, 174, 119, 74}}}
    End Sub

	ReadOnly Property getB(ByVal x As Integer, ByVal y As Integer, ByVal z As Integer) As Integer()
		Get
			Return qrMaxLenght.GetValue(x, y, z)
		End Get
	End Property
End Class