Option Strict Off
Option Explicit On
Imports System
Friend Class QR_PATTERN_POSITION_TABLE

    Dim QR_PATTERN_POSITION_TABLE_Renamed As Integer()()


    Public Sub New()
        MyBase.New()

        QR_PATTERN_POSITION_TABLE_Renamed = New Integer()() {New Integer() {}, New Integer() {6, 18}, New Integer() {6, 22}, New Integer() {6, 26}, New Integer() {6, 30}, New Integer() {6, 34}, New Integer() {6, 22, 38}, New Integer() {6, 24, 42}, New Integer() {6, 26, 46}, New Integer() {6, 28, 50}, New Integer() {6, 30, 54}, New Integer() {6, 32, 58}, New Integer() {6, 34, 62}, New Integer() {6, 26, 46, 66}, New Integer() {6, 26, 48, 70}, New Integer() {6, 26, 50, 74}, New Integer() {6, 30, 54, 78}, New Integer() {6, 30, 56, 82}, New Integer() {6, 30, 58, 86}, New Integer() {6, 34, 62, 90}, New Integer() {6, 28, 50, 72, 94}, New Integer() {6, 26, 50, 74, 98}, New Integer() {6, 30, 54, 78, 102}, New Integer() {6, 28, 54, 80, 106}, New Integer() {6, 32, 58, 84, 110}, New Integer() {6, 30, 58, 86, 114}, New Integer() {6, 34, 62, 90, 118}, New Integer() {6, 26, 50, 74, 98, 122}, New Integer() {6, 30, 54, 78, 102, 126}, New Integer() {6, 26, 52, 78, 104, 130}, New Integer() {6, 30, 56, 82, 108, 134}, New Integer() {6, 34, 60, 86, 112, 138}, New Integer() {6, 30, 58, 86, 114, 142}, New Integer() {6, 34, 62, 90, 118, 146}, New Integer() {6, 30, 54, 78, 102, 126, 150}, New Integer() {6, 24, 50, 76, 102, 128, 154}, New Integer() {6, 28, 54, 80, 106, 132, 158}, New Integer() {6, 32, 58, 84, 110, 136, 162}, New Integer() {6, 26, 54, 82, 110, 138, 166}, New Integer() {6, 30, 58, 86, 114, 142, 170}}

    End Sub


    ReadOnly Property getB(ByVal index As Integer) As Integer()
        Get
            Return QR_PATTERN_POSITION_TABLE_Renamed(CInt(index))
        End Get
    End Property
End Class