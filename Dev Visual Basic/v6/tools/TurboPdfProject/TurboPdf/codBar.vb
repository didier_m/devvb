Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports UpgradeHelpers.Helpers
Module codBar
	Dim lengthRows() As Integer
	Private lengthCols() As Integer
	Private dataCWCount() As Double
	Dim solomonCWCount() As Double
	Private dataRegionRows() As Integer
	Private dataRegionCols() As Integer
	Private regionRows() As Double
	Private regionCols() As Double
	Private interleavedBlocks() As Double
	Private logTab() As Double
	Private aLogTab() As Integer ' Table of aLog for the Galois field

	Private Function champGaloisMult(ByVal a As Integer, ByVal b As Integer) As Integer
		'MULTIPLICATION IN GALOIS FIELD GF(2^8)
		If a = 0 Or b = 0 Then
			Return 0
		End If
		Return aLogTab(CInt(CInt(logTab(a) + logTab(b)) Mod 255))
	End Function

	Private Function champGaloisDoub(ByVal a As Integer, ByVal b As Integer) As Integer
		'THE OPERATION a * 2^b IN GALOIS FIELD GF(2^8)
		If a = 0 Then
			Return 0
		End If
		If b = 0 Then
			Return a
		End If
		Return aLogTab(CInt(CInt(logTab(a) + b) Mod 255))
	End Function

	Private Function champGaloisSum(ByVal a As Integer, ByVal b As Integer) As Integer
		'SUM IN GALOIS FIELD GF(2^8)
		Return a Xor b
	End Function

	Private Function selectIndex(ByVal dataCodeWordsCount As Double, ByVal rectangular As Boolean) As Integer
		Dim result As Integer = 0
		Dim n As Integer
		' CHOOSE THE GOOD INDEX FOR TABLES
		If (dataCodeWordsCount < 1 Or dataCodeWordsCount > 1558) And Not rectangular Then
			Return -1
		End If
		If (dataCodeWordsCount < 1 Or dataCodeWordsCount > 49) And rectangular Then
			result = -1
		End If
		If rectangular Then
			n = 24
		Else
			n = 0
		End If
		Do While dataCWCount(n) < dataCodeWordsCount
			n += 1
		Loop 
		Return n
	End Function

	Private Function encodeDataCodeWordsASCII(ByVal text As String) As Integer()
		Dim c As Integer
		Dim dataCodeWords() As Integer
		ReDim dataCodeWords(0)
		Dim n As Integer = 0
		Dim leng As Integer = Strings.Len(text)
		For i As Integer = 0 To leng - 1
			c = Strings.Asc(Strings.Mid(text, i + 1, 1)(0))
			If c > 127 Then
				dataCodeWords(n) = 235
				c -= 127
				n += 1
			Else
				If (c >= 48 And c <= 57) And (i + 1 < leng) And ("0123456789").IndexOf(Strings.Mid(text, i + 2, 1)) >= 0 Then
					c = ((c - 48) * 10) + CInt(Strings.Mid(text, i + 2, 1))
					c += 130
					i += 1
				Else
					c += 1
				End If
			End If
			If dataCodeWords.GetUpperBound(0) < n Then ReDim Preserve dataCodeWords(n)
			dataCodeWords(n) = c
			n += 1
		Next
        Return dataCodeWords
    End Function

	Private Function addPadCW(ByRef atab() As Integer, ByVal afrom As Double, ByVal ato As Double) As Object
		Dim r As Integer
		If afrom >= ato Then Exit Function
		If atab.GetUpperBound(0) < ato Then ReDim Preserve atab(CInt(ato - 1))
		atab(CInt(afrom)) = 129
		For i As Double = afrom + 1 To ato - 1
			r = CInt((CInt(149 * (i + 1)) Mod 253) + 1)
			atab(CInt(i)) = (129 + r) Mod 254
		Next
	End Function

	Private Function calculSolFactorTable(ByVal solomonCWCount As Integer) As Integer()
		' CALCULATE THE REED SOLOMON FACTORS
		Dim g(solomonCWCount) As Integer
		For i As Integer = 0 To solomonCWCount
			g(i) = 1
		Next
		For i As Integer = 1 To solomonCWCount
			For j As Integer = i - 1 To 0 Step -1
				g(j) = champGaloisDoub(g(j), i)
				If j > 0 Then
					g(j) = champGaloisSum(g(j), g(j - 1))
				End If
			Next
		Next
		Return g
	End Function

	Private Function addReedSolomonCW(ByVal nSolomonCW As Double, ByVal coeffTab() As Integer, ByVal nDataCW As Double, ByRef dataTab() As Integer, ByVal blocks As Double) As Integer()
		Dim Temp As Integer
		' Add the Reed Solomon codewords
		Dim j As Integer
		Dim errorBlocks As Double = nSolomonCW / blocks
		Dim correctionCW(CInt(errorBlocks - 1)) As Integer
		For k As Integer = 0 To blocks - 1
			For i As Integer = 0 To errorBlocks - 1
				correctionCW(i) = 0
			Next
			For i As Integer = k To nDataCW - 1 Step blocks
				Temp = champGaloisSum(dataTab(i), correctionCW(CInt(errorBlocks - 1)))
				For j = errorBlocks - 1 To 0 Step -1
					If Temp = 0 Then
						correctionCW(j) = 0
					Else
						correctionCW(j) = champGaloisMult(Temp, coeffTab(j))
					End If
					If j > 0 Then correctionCW(j) = champGaloisSum(correctionCW(j - 1), correctionCW(j))
				Next
			Next
			' Renversement des blocs calcules
			j = nDataCW + k
			For i As Integer = errorBlocks - 1 To 0 Step -1
				If dataTab.GetUpperBound(0) < j Then ReDim Preserve dataTab(j)
				dataTab(j) = correctionCW(i)
				j += blocks
			Next
		Next
		Return dataTab
	End Function

	Function ShiftRight(ByVal lngNumber As Integer, ByVal intNumBits As Integer) As Integer
		'--------------
		'BIT SHIFT RIGHT
		'--------------
		Return lngNumber \ 2 ^ intNumBits 'note the integer division op
	End Function

	Private Function getBits(ByVal entier As Integer) As Integer()
		' Transform integer to tab of bits

		Dim bits(7) As Integer
		For i As Integer = 0 To 7
			If entier And ShiftRight(128, i) Then
				bits(i) = 1
			Else
				bits(i) = 0
			End If
		Next
		Return bits
	End Function

	Private Function anext(ByVal etape As Integer, ByVal totalRows As Double, ByVal totalCols As Double, ByVal codeWordsBits As Array, ByVal datamatrix(, ) As Integer, ByRef assigned(, ) As Integer) As Object
		Dim ol As Integer
		' Place codewords into the matrix
		Dim aChr As Integer = 0 ' Place of the 8st bit from the first character to (4)(0)
		Dim row As Double = 4
		Dim col As Double = 0
		Do 
			' Check for a special case of corner
			If (row = totalRows) And (col = 0) Then
				patternShapeSpecial1(datamatrix, assigned, codeWordsBits.GetValue(aChr), totalRows, totalCols)
				aChr += 1
			ElseIf (etape < 3) And (row = totalRows - 2) And (ol = 0) And (CInt(totalCols) Mod 4 <> 0) Then 
				patternShapeSpecial2(datamatrix, assigned, codeWordsBits.GetValue(aChr), totalRows, totalCols)
				aChr += 1
			ElseIf ((row = totalRows - 2) And (col = 0) And (CInt(totalCols) Mod 8 = 4)) Then 
				patternShapeSpecial3(datamatrix, assigned, codeWordsBits.GetValue(aChr), totalRows, totalCols)
				aChr += 1
			ElseIf ((row = totalRows + 4) And (col = 2) And (CInt(totalCols) Mod 8 = 0)) Then 
				patternShapeSpecial4(datamatrix, assigned, codeWordsBits.GetValue(aChr), totalRows, totalCols)
				aChr += 1
			End If

			' Go up and right in the datamatrix
			Do 
				If (row < totalRows) And (col >= 0) And (Not isset(assigned, row, col) Or arrayValue(assigned, row, col) <> 1) Then
					'                    Debug.Print "nextA " & row & "#" & col
					patternShapeStandard(datamatrix, assigned, codeWordsBits.GetValue(aChr), row, col, totalRows, totalCols)
					aChr += 1
				End If
				row -= 2
				col += 2
			Loop While ((row >= 0) And (col < totalCols))
			row += 1
			col += 3
			'Debug.Print "next AB" & row & "#" & col
			' Go down and left in the datamatrix
			Do 
				If (row >= 0) And (col < totalCols) And (Not isset(assigned, row, col) Or arrayValue(assigned, row, col) <> 1) Then
					'                    Debug.Print "nextB " & row & "#" & col
					patternShapeStandard(datamatrix, assigned, codeWordsBits.GetValue(aChr), row, col, totalRows, totalCols)
					aChr += 1
				Else
					'                    Debug.Print "suite " & row & "#" & col
				End If
				row += 2
				col -= 2
			Loop While ((row < totalRows) And (col >= 0))
			row += 3
			col += 1
		Loop While ((row < totalRows) Or (col < totalCols))
	End Function

	Private Function isset(ByVal obj(, ) As Integer, Optional ByVal a As Double = 0, Optional ByVal b As Double = 0) As Boolean
		Dim result As Boolean = False
		Try
			Dim i As Integer
			result = False
			i = obj(CInt(a), CInt(b))
			result = True
			If i = -1 Then result = False

		Catch
		End Try

		Return result
	End Function

	Private Function arrayValue(ByVal obj(, ) As Integer, ByVal a As Double, ByVal b As Double) As Integer
		Dim result As Integer = 0
		Try
			result = obj(CInt(a), CInt(b))

		Catch
		End Try

		Return result
	End Function

	Private Function patternShapeStandard(ByVal datamatrix(, ) As Integer, ByRef assigned(, ) As Integer, ByVal bits() As Integer, ByVal row As Double, ByVal col As Double, ByVal totalRows As Double, ByVal totalCols As Double) As Object ' Place bits in the matrix (standard or special case)
		'Debug.Print "datamatrix, assigned, bits, row, col, totalRows, totalCols => " & "|" & "|" & "|" & "|" & row & "|" & col & "|" & totalRows & "|" & totalCols
		placeBitInDatamatrix(datamatrix, assigned, bits(0), row - 2, col - 2, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(1), row - 2, col - 1, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(2), row - 1, col - 2, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(3), row - 1, col - 1, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(4), row - 1, col, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(5), row, col - 2, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(6), row, col - 1, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(7), row, col, totalRows, totalCols)
	End Function

	Private Function patternShapeSpecial1(ByVal datamatrix(, ) As Integer, ByRef assigned(, ) As Integer, ByVal bits() As Integer, ByVal totalRows As Double, ByVal totalCols As Double) As Object
		placeBitInDatamatrix(datamatrix, assigned, bits(0), totalRows - 1, 0, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(1), totalRows - 1, 1, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(2), totalRows - 1, 2, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(3), 0, totalCols - 2, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(4), 0, totalCols - 1, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(5), 1, totalCols - 1, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(6), 2, totalCols - 1, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(7), 3, totalCols - 1, totalRows, totalCols)
	End Function

	Private Function patternShapeSpecial2(ByVal datamatrix(, ) As Integer, ByRef assigned(, ) As Integer, ByVal bits() As Integer, ByVal totalRows As Double, ByVal totalCols As Double) As Object
		placeBitInDatamatrix(datamatrix, assigned, bits(0), totalRows - 3, 0, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(1), totalRows - 2, 0, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(2), totalRows - 1, 0, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(3), 0, totalCols - 4, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(4), 0, totalCols - 3, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(5), 0, totalCols - 2, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(6), 0, totalCols - 1, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(7), 1, totalCols - 1, totalRows, totalCols)
	End Function

	Private Function patternShapeSpecial3(ByVal datamatrix(, ) As Integer, ByRef assigned(, ) As Integer, ByVal bits() As Integer, ByVal totalRows As Double, ByVal totalCols As Double) As Object
		placeBitInDatamatrix(datamatrix, assigned, bits(0), totalRows - 3, 0, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(1), totalRows - 2, 0, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(2), totalRows - 1, 0, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(3), 0, totalCols - 2, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(4), 0, totalCols - 1, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(5), 1, totalCols - 1, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(6), 2, totalCols - 1, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(7), 3, totalCols - 1, totalRows, totalCols)
	End Function

	Private Function patternShapeSpecial4(ByVal datamatrix(, ) As Integer, ByRef assigned(, ) As Integer, ByVal bits() As Integer, ByVal totalRows As Double, ByVal totalCols As Double) As Object
		placeBitInDatamatrix(datamatrix, assigned, bits(0), totalRows - 1, 0, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(1), totalRows - 1, totalCols - 1, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(2), 0, totalCols - 3, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(3), 0, totalCols - 2, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(4), 0, totalCols - 1, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(5), 1, totalCols - 3, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(6), 1, totalCols - 2, totalRows, totalCols)
		placeBitInDatamatrix(datamatrix, assigned, bits(7), 1, totalCols - 1, totalRows, totalCols)
	End Function

	Private Function placeBitInDatamatrix(ByVal datamatrix(, ) As Integer, ByRef assigned(, ) As Integer, ByVal bit As Integer, ByVal row As Double, ByVal col As Double, ByVal totalRows As Double, ByVal totalCols As Double) As Object
		'Debug.Print "" & row & "," & col & "," & totalRows & "," & totalCols
		' Put a bit into the matrix
		If row < 0 Then
			row += totalRows
			col = col + 4 - (CInt(totalRows + 4) Mod 8)
		End If
		If col < 0 Then
			col += totalCols
			row = row + 4 - (CInt(totalCols + 4) Mod 8)
		End If
		If assigned.GetUpperBound(0) < row Or assigned.GetUpperBound(1) < col Then
			ReDim Preserve assigned(assigned.GetUpperBound(0), CInt(col))
			datamatrix(CInt(row), CInt(col)) = bit
			assigned(CInt(row), CInt(col)) = 1
		ElseIf assigned(CInt(row), CInt(col)) <> 1 Then 
			datamatrix(CInt(row), CInt(col)) = bit
			assigned(CInt(row), CInt(col)) = 1
		End If
	End Function

	Private Function addFinderPattern(ByVal datamatrix(, ) As Integer, ByVal rowsRegion As Double, ByVal colsRegion As Double, ByVal rowsRegionCW As Integer, ByVal colsRegionCW As Integer) As Integer(, )
		' Add the finder pattern
		Dim totalRowsCW As Double = (rowsRegionCW + 2) * rowsRegion
		Dim totalColsCW As Double = (colsRegionCW + 2) * colsRegion

		Dim datamatrixTemp(, ) As Integer
		ReDim datamatrixTemp(CInt(totalRowsCW + 1), 0)

		For i As Integer = 0 To totalRowsCW - 1
			ReDim Preserve datamatrixTemp(CInt(totalRowsCW + 1), CInt(totalColsCW + 1))
			datamatrixTemp(i + 1, 0) = 0
			datamatrixTemp(i + 1, CInt(totalColsCW + 1)) = 0
			For j As Double = 0 To totalColsCW - 1
				If i Mod (rowsRegionCW + 2) = 0 Then
					If CInt(j) Mod 2 = 0 Then
						datamatrixTemp(i + 1, CInt(j + 1)) = 1
					Else
						datamatrixTemp(i + 1, CInt(j + 1)) = 0
					End If
				ElseIf i Mod (rowsRegionCW + 2) = rowsRegionCW + 1 Then 
					datamatrixTemp(i + 1, CInt(j + 1)) = 1
				ElseIf CInt(j) Mod (colsRegionCW + 2) = colsRegionCW + 1 Then 
					If i Mod 2 = 0 Then
						datamatrixTemp(i + 1, CInt(j + 1)) = 0
					Else
						datamatrixTemp(i + 1, CInt(j + 1)) = 1
					End If
				ElseIf CInt(j) Mod (colsRegionCW + 2) = 0 Then 
					datamatrixTemp(i + 1, CInt(j + 1)) = 1
				Else
					datamatrixTemp(i + 1, CInt(j + 1)) = 0
					datamatrixTemp(i + 1, CInt(j + 1)) = datamatrix(CInt(i - 1 - (2 * (IIf(CDbl(i / (rowsRegionCW + 2)) > 0, Math.Floor(CDbl(i / (rowsRegionCW + 2))), Math.Ceiling(CDbl(i / (rowsRegionCW + 2))))))), CInt(j - 1 - (2 * (IIf(j / (colsRegionCW + 2) > 0, Math.Floor(j / (colsRegionCW + 2)), Math.Ceiling(j / (colsRegionCW + 2))))))) ' todo : parseInt => ?
				End If
			Next
		Next
		'Dim datamatrixTemp2() As Integer
		'ReDim datamatrixTemp2(0 To totalRowsCW + 1, 0 To totalColsCW + 1)
		'For i = 0 To totalRowsCW + 1
		'    For j = 0 To totalColsCW + 1
		'        datamatrixTemp2(i, j) = datamatrixTemp(i, j)
		'    Next
		'Next
		For j As Double = 0 To totalColsCW + 1
			datamatrixTemp(CInt(totalRowsCW + 1), CInt(j)) = 0
		Next
		Return datamatrixTemp
	End Function

    Public Function getDigit(ByVal text As String, ByVal rectangular As Boolean) As Integer(,)
        lengthRows = New Integer() {10, 12, 14, 16, 18, 20, 22, 24, 26, 32, 36, 40, 44, 48, 52, 64, 72, 80, 88, 96, 104, 120, 132, 144, 8, 8, 12, 12, 16, 16}
        lengthCols = New Integer() {10, 12, 14, 16, 18, 20, 22, 24, 26, 32, 36, 40, 44, 48, 52, 64, 72, 80, 88, 96, 104, 120, 132, 144, 18, 32, 26, 36, 36, 48}
        dataCWCount = New Double() {3, 5, 8, 12, 18, 22, 30, 36, 44, 62, 86, 114, 144, 174, 204, 280, 368, 456, 576, 696, 816, 1050, 1304, 1558, 5, 10, 16, 22, 32, 49}
        solomonCWCount = New Double() {5, 7, 10, 12, 14, 18, 20, 24, 28, 36, 42, 48, 56, 68, 84, 112, 144, 192, 224, 272, 336, 408, 496, 620, 7, 11, 14, 18, 24, 28}
        dataRegionRows = New Integer() {8, 10, 12, 14, 16, 18, 20, 22, 24, 14, 16, 18, 20, 22, 24, 14, 16, 18, 20, 22, 24, 18, 20, 22, 6, 6, 10, 10, 14, 14}
        dataRegionCols = New Integer() {8, 10, 12, 14, 16, 18, 20, 22, 24, 14, 16, 18, 20, 22, 24, 14, 16, 18, 20, 22, 24, 18, 20, 22, 16, 14, 24, 16, 16, 22}
        regionRows = New Double() {1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4, 6, 6, 6, 1, 1, 1, 1, 1, 1}
        regionCols = New Double() {1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4, 6, 6, 6, 1, 2, 1, 2, 2, 2}
        interleavedBlocks = New Double() {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 4, 4, 4, 4, 6, 6, 8, 8, 1, 1, 1, 1, 1, 1}
        logTab = New Double() {-255, 255, 1, 240, 2, 225, 241, 53, 3, 38, 226, 133, 242, 43, 54, 210, 4, 195, 39, 114, 227, 106, 134, 28, 243, 140, 44, 23, 55, 118, 211, 234, 5, 219, 196, 96, 40, 222, 115, 103, 228, 78, 107, 125, 135, 8, 29, 162, 244, 186, 141, 180, 45, 99, 24, 49, 56, 13, 119, 153, 212, 199, 235, 91, 6, 76, 220, 217, 197, 11, 97, 184, 41, 36, 223, 253, 116, 138, 104, 193, 229, 86, 79, 171, 108, 165, 126, 145, 136, 34, 9, 74, 30, 32, 163, 84, 245, 173, 187, 204, 142, 81, 181, 190, 46, 88, 100, 159, 25, 231, 50, 207, 57, 147, 14, 67, 120, 128, 154, 248, 213, 167, 200, 63, 236, 110, 92, 176, 7, 161, 77, 124, 221, 102, 218, 95, 198, 90, 12, 152, 98, 48, 185, 179, 42, 209, 37, 132, 224, 52, 254, 239, 117, 233, 139, 22, 105, 27, 194, 113, 230, 206, 87, 158, 80, 189, 172, 203, 109, 175, 166, 62, 127, 247, 146, 66, 137, 192, 35, 252, 10, 183, 75, 216, 31, 83, 33, 73, 164, 144, 85, 170, 246, 65, 174, 61, 188, 202, 205, 157, 143, 169, 82, 72, 182, 215, 191, 251, 47, 178, 89, 151, 101, 94, 160, 123, 26, 112, 232, 21, 51, 238, 208, 131, 58, 69, 148, 18, 15, 16, 68, 17, 121, 149, 129, 19, 155, 59, 249, 70, 214, 250, 168, 71, 201, 156, 64, 60, 237, 130, 111, 20, 93, 122, 177, 150}
        aLogTab = New Integer() {1, 2, 4, 8, 16, 32, 64, 128, 45, 90, 180, 69, 138, 57, 114, 228, 229, 231, 227, 235, 251, 219, 155, 27, 54, 108, 216, 157, 23, 46, 92, 184, 93, 186, 89, 178, 73, 146, 9, 18, 36, 72, 144, 13, 26, 52, 104, 208, 141, 55, 110, 220, 149, 7, 14, 28, 56, 112, 224, 237, 247, 195, 171, 123, 246, 193, 175, 115, 230, 225, 239, 243, 203, 187, 91, 182, 65, 130, 41, 82, 164, 101, 202, 185, 95, 190, 81, 162, 105, 210, 137, 63, 126, 252, 213, 135, 35, 70, 140, 53, 106, 212, 133, 39, 78, 156, 21, 42, 84, 168, 125, 250, 217, 159, 19, 38, 76, 152, 29, 58, 116, 232, 253, 215, 131, 43, 86, 172, 117, 234, 249, 223, 147, 11, 22, 44, 88, 176, 77, 154, 25, 50, 100, 200, 189, 87, 174, 113, 226, 233, 255, 211, 139, 59, 118, 236, 245, 199, 163, 107, 214, 129, 47, 94, 188, 85, 170, 121, 242, 201, 191, 83, 166, 97, 194, 169, 127, 254, 209, 143, 51, 102, 204, 181, 71, 142, 49, 98, 196, 165, 103, 206, 177, 79, 158, 17, 34, 68, 136, 61, 122, 244, 197, 167, 99, 198, 161, 111, 222, 145, 15, 30, 60, 120, 240, 205, 183, 67, 134, 33, 66, 132, 37, 74, 148, 5, 10, 20, 40, 80, 160, 109, 218, 153, 31, 62, 124, 248, 221, 151, 3, 6, 12, 24, 48, 96, 192, 173, 119, 238, 241, 207, 179, 75, 150, 1}


        Dim dataCodeWords() As Integer = encodeDataCodeWordsASCII(text) ' Code the text in the ASCII mode
        Dim adataCWCount As Double = dataCodeWords.GetUpperBound(0) + 1
        Dim index As Integer = selectIndex(adataCWCount, rectangular) ' Select the index for the data tables
        Dim totalDataCWCount As Double = dataCWCount(index) ' Number of data CW
        Dim asolomonCWCount As Double = solomonCWCount(index) ' Number of Reed Solomon CW
        Dim totalCWCount As Double = totalDataCWCount + asolomonCWCount ' Number of CW
        Dim rowsTotal As Integer = lengthRows(index) ' Size of symbol
        Dim colsTotal As Integer = lengthCols(index)
        Dim rowsRegion As Double = regionRows(index) ' Number of region
        Dim colsRegion As Double = regionCols(index)
        Dim rowsRegionCW As Integer = dataRegionRows(index)
        Dim colsRegionCW As Integer = dataRegionCols(index)
        Dim rowsLengthMatrice As Double = rowsTotal - 2 * rowsRegion ' Size of matrice data
        Dim colsLengthMatrice As Double = colsTotal - 2 * colsRegion
        Dim blocks As Double = interleavedBlocks(index) ' Number of Reed Solomon blocks
        Dim errorBlocks As Integer = asolomonCWCount / blocks

        addPadCW(dataCodeWords, adataCWCount, totalDataCWCount) ' Add codewords pads

        Dim g() As Integer = calculSolFactorTable(errorBlocks) ' Calculate correction coefficients

        addReedSolomonCW(asolomonCWCount, g, totalDataCWCount, dataCodeWords, blocks) ' Add Reed Solomon codewords
        ' Calculte bits from codewords
        'Dim codeWordsBits(CInt(totalCWCount - 1)) As Object
        Dim codeWordsBits As Integer()() = New Integer(totalCWCount - 1)() {}
        For i As Double = 0 To totalCWCount - 1
            If codeWordsBits.GetUpperBound(0) < i Then codeWordsBits = RedimPreserveInteger(codeWordsBits, CInt(i), 7)
            codeWordsBits(CInt(i)) = getBits(dataCodeWords(CInt(i)))
        Next

        Dim datamatrix(,) As Integer
        ReDim datamatrix(CInt(colsLengthMatrice - 1), CInt(colsLengthMatrice - 1))
        Dim assigned(,) As Integer
        ReDim assigned(CInt(colsLengthMatrice - 1), CInt(colsLengthMatrice - 1))

        For i As Double = 0 To colsLengthMatrice - 1
            For j As Double = 0 To colsLengthMatrice - 1
                datamatrix(CInt(i), CInt(j)) = 0
                assigned(CInt(i), CInt(j)) = -1
            Next
        Next

        ' Add the bottom-right corner if needed
        If (CInt(rowsLengthMatrice * colsLengthMatrice) Mod 8) = 4 Then
            datamatrix(CInt(rowsLengthMatrice - 2), CInt(colsLengthMatrice - 2)) = 1
            datamatrix(CInt(rowsLengthMatrice - 1), CInt(colsLengthMatrice - 1)) = 1
            datamatrix(CInt(rowsLengthMatrice - 1), CInt(colsLengthMatrice - 2)) = 0
            datamatrix(CInt(rowsLengthMatrice - 2), CInt(colsLengthMatrice - 1)) = 0
            assigned(CInt(rowsLengthMatrice - 2), CInt(colsLengthMatrice - 2)) = 1
            assigned(CInt(rowsLengthMatrice - 1), CInt(colsLengthMatrice - 1)) = 1
            assigned(CInt(rowsLengthMatrice - 1), CInt(colsLengthMatrice - 2)) = 1
            assigned(CInt(rowsLengthMatrice - 2), CInt(colsLengthMatrice - 1)) = 1
        End If

        ' Put the codewords into the matrix
        anext(0, rowsLengthMatrice, colsLengthMatrice, codeWordsBits, datamatrix, assigned)

        ' Add the finder pattern
        datamatrix = addFinderPattern(datamatrix, rowsRegion, colsRegion, rowsRegionCW, colsRegionCW)
        Return datamatrix

    End Function

    Private Function RedimPreserveInteger(ByVal arrInt As Integer()(), fL As Integer, lL As Integer) As Integer()()
        Dim intBuffer()() As Integer
        Dim i As Integer
        Dim j As Integer

        ReDim intBuffer(fL)(lL)
        For i = 0 To UBound(arrInt)
            If i < UBound(arrInt) Then
                intBuffer(i) = arrInt(i)
            Else
                For j = 0 To lL
                    intBuffer(i)(j) = 0
                Next
            End If
        Next

        Return intBuffer
    End Function

End Module