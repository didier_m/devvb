﻿Namespace com.d_project.qrcode

	''' <summary>
	''' モード.
	''' @author Kazuhiko Arase 
	''' </summary>
	Public Interface Mode

		''' <summary>
		''' 数値モード
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int MODE_NUMBER = 1 << 0;

		''' <summary>
		''' 英数字モード
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int MODE_ALPHA_NUM = 1 << 1;

		''' <summary>
		''' 8ビットバイトモード
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int MODE_8BIT_BYTE = 1 << 2;

		''' <summary>
		''' 漢字モード
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int MODE_KANJI = 1 << 3;

	End Interface

End Namespace