Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Friend Class QRData
	'---------------------------------------------------------------
	' QRData
	'---------------------------------------------------------------


	Public mode As Object
	Public data As Object

	Public Function QRData(ByVal modeB As Object, ByVal dataB As Object) As Object
        mode = modeB
        data = dataB
    End Function

	Public Function getMode() As Object
		Return mode
	End Function

	Public Function getData() As Object
		Return data
	End Function

	'-> renamed write to writeB
	Public Function write_buffer(ByVal buffer As Object) As Object
		Dim data As String = ""

		Select Case mode
			Case QR_MODE_8BIT_BYTE
                data = CStr(getData())
                For i As Integer = 0 To Strings.Len(data) - 1
                    buffer.put_buffer(Strings.Asc(Strings.Mid(data, i + 1, 1)(0)), 8)
                Next
			Case Else
				trigger_error("not implemented.", E_USER_ERROR)
		End Select

	End Function

	Public Function getLengthInBits(ByVal typeB As Object) As Object
        Dim result As Object = Nothing
        If 1 <= CDbl(typeB) And CDbl(typeB) < 10 Then
			' 1 - 9
			Select Case mode
				Case QR_MODE_NUMBER
					result = 10
				Case QR_MODE_ALPHA_NUM
					result = 9
				Case QR_MODE_8BIT_BYTE, QR_MODE_KANJI
					result = 8
				Case Else
					trigger_error("mode:mode", E_USER_ERROR)
			End Select

		ElseIf CDbl(typeB) < 27 Then 
			' 10 - 26
			Select Case mode
				Case QR_MODE_NUMBER
					result = 12
				Case QR_MODE_ALPHA_NUM
					result = 11
				Case QR_MODE_8BIT_BYTE
					result = 16
				Case QR_MODE_KANJI
					result = 10
				Case Else
					trigger_error("mode:mode", E_USER_ERROR)
			End Select

		ElseIf CDbl(typeB) < 41 Then 
			' 27 - 40
			Select Case mode
				Case QR_MODE_NUMBER
					result = 14
				Case QR_MODE_ALPHA_NUM
					result = 13
				Case QR_MODE_8BIT_BYTE
					result = 16
				Case QR_MODE_KANJI
					result = 12
				Case Else
					trigger_error("mode:mode", E_USER_ERROR)
			End Select

		Else
			trigger_error("mode:mode", E_USER_ERROR)
		End If
		Return result
	End Function


	Public Function getLength() As Object
		Dim result As Object = Nothing
		Select Case mode
			Case QR_MODE_NUMBER, QR_MODE_ALPHA_NUM, QR_MODE_KANJI
			Case QR_MODE_8BIT_BYTE
                result = Strings.Len(CStr(getData()))
            Case Else
                trigger_error("getLength:" & CStr(mode), E_USER_ERROR)
        End Select
		Return result
	End Function
End Class