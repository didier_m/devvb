﻿Namespace com.d_project.qrcode

	''' <summary>
	''' マスクパターン.
	''' @author Kazuhiko Arase 
	''' </summary>
	Friend Interface MaskPattern

		''' <summary>
		''' マスクパターン000
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int PATTERN000 = 0;

		''' <summary>
		''' マスクパターン001
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int PATTERN001 = 1;

		''' <summary>
		''' マスクパターン010
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int PATTERN010 = 2;

		''' <summary>
		''' マスクパターン011
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int PATTERN011 = 3;

		''' <summary>
		''' マスクパターン100
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int PATTERN100 = 4;

		''' <summary>
		''' マスクパターン101
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int PATTERN101 = 5;

		''' <summary>
		''' マスクパターン110
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int PATTERN110 = 6;

		''' <summary>
		''' マスクパターン111
		''' </summary>
'JAVA TO VB CONVERTER TODO TASK: Interfaces cannot contain fields in .NET:
'		int PATTERN111 = 7;

	End Interface


End Namespace