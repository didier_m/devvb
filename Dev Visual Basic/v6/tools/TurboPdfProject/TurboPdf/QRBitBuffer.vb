Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports UpgradeHelpers.Helpers
Friend Class QRBitBuffer

	'---------------------------------------------------------------
	' QRBitBuffer
	'---------------------------------------------------------------
	Public buffer As Object
	Public length As Object

	Public Sub New()
		MyBase.New()
        buffer = New Object() {}
        length = 0
	End Sub

	Public Function getBuffer() As Object
		Return buffer
	End Function

	Public Function getLengthInBits() As Object
		Return length
	End Function

	Public Function toString_Renamed() As Object

		buffer = ""
        Dim tempForEndVar As Double = CDbl(getLengthInBits()) - 1
        For i As Integer = 0 To tempForEndVar
            If CBool(getB(i)) Then
                buffer = CDbl(buffer) + 1
            Else
                buffer = CDbl(buffer) + 1
            End If
		Next
		Return buffer
	End Function

	Public Function getB(ByVal index As Object) As Object

        Dim bufIndex As Integer = Math.Floor(CDbl(index) / 8)
        Return CStr(ShiftRight(CInt(buffer.GetValue(bufIndex)), 7 - CInt(index) Mod 8)) & CStr(1) = 1
    End Function

	Public Function put_buffer(ByVal num As Object, ByVal length As Object) As Object
        Dim tempForEndVar As Double = CDbl(length) - 1
        For i As Integer = 0 To tempForEndVar
            putBit((ShiftRight(CInt(num), CDbl(length) - i - 1) And 1) = 1)
        Next
	End Function

	Public Function putBit(ByVal bit As Object) As Object

        Dim bufIndex As Integer = IIf(CDbl(length) / 8 > 0, Math.Floor(CDbl(length) / 8), Math.Ceiling(CDbl(length) / 8))
        If buffer.GetUpperBound(0) + 1 <= bufIndex Then
			ReDim Preserve buffer(bufIndex)
			buffer.SetValue(0, bufIndex)
		End If

        If CBool(bit) Then
            buffer.SetValue(CInt(buffer.GetValue(bufIndex)) Or ShiftRight(CInt("&H80"), CInt(length) Mod 8), bufIndex)
        End If

        length = CDbl(length) + 1
    End Function
End Class