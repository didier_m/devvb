Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Friend Class QRUtil
	'---------------------------------------------------------------
	' QRUtil
	'---------------------------------------------------------------


	Private QRPolynomial As QRPolynomial
	Private QR_PATTERN_POSITION_TABLE(40, 7) As Integer

	Public Sub New()
		MyBase.New()


		Dim QR_MAX_LENGTH(10, 4) As Integer
		QR_MAX_LENGTH(1, 1) = 41
		QR_MAX_LENGTH(1, 2) = 25
		QR_MAX_LENGTH(1, 3) = 17
		QR_MAX_LENGTH(1, 4) = 10

		QR_MAX_LENGTH(2, 1) = 41
		QR_MAX_LENGTH(2, 2) = 25
		QR_MAX_LENGTH(2, 3) = 17
		QR_MAX_LENGTH(2, 4) = 10

		QR_MAX_LENGTH(3, 1) = 41
		QR_MAX_LENGTH(3, 2) = 25
		QR_MAX_LENGTH(3, 3) = 17
		QR_MAX_LENGTH(3, 4) = 10

		QR_MAX_LENGTH(4, 1) = 41
		QR_MAX_LENGTH(4, 2) = 25
		QR_MAX_LENGTH(4, 3) = 17
		QR_MAX_LENGTH(4, 4) = 10

		QR_MAX_LENGTH(5, 1) = 41
		QR_MAX_LENGTH(5, 2) = 25
		QR_MAX_LENGTH(5, 3) = 17
		QR_MAX_LENGTH(5, 4) = 10

		QR_MAX_LENGTH(6, 1) = 41
		QR_MAX_LENGTH(6, 2) = 25
		QR_MAX_LENGTH(6, 3) = 17
		QR_MAX_LENGTH(6, 4) = 10

		QR_MAX_LENGTH(7, 1) = 41
		QR_MAX_LENGTH(7, 2) = 25
		QR_MAX_LENGTH(7, 3) = 17
		QR_MAX_LENGTH(7, 4) = 10

		QR_MAX_LENGTH(8, 1) = 41
		QR_MAX_LENGTH(8, 2) = 25
		QR_MAX_LENGTH(8, 3) = 17
		QR_MAX_LENGTH(8, 4) = 10

		QR_MAX_LENGTH(9, 1) = 41
		QR_MAX_LENGTH(9, 2) = 25
		QR_MAX_LENGTH(9, 3) = 17
		QR_MAX_LENGTH(9, 4) = 10

		QR_MAX_LENGTH(10, 1) = 41
		QR_MAX_LENGTH(10, 2) = 25
		QR_MAX_LENGTH(10, 3) = 17
        QR_MAX_LENGTH(10, 4) = 10

        QR_PATTERN_POSITION_TABLE(1, 1) = 0
		QR_PATTERN_POSITION_TABLE(2, 1) = 6
		QR_PATTERN_POSITION_TABLE(2, 2) = 18
		QR_PATTERN_POSITION_TABLE(3, 1) = 6
		QR_PATTERN_POSITION_TABLE(3, 1) = 22



    End Sub



    Public Function getPatternPosition(ByVal typeNumber As Integer) As Integer()
        Static positionTable As QR_PATTERN_POSITION_TABLE
        positionTable = New QR_PATTERN_POSITION_TABLE()
        Return positionTable.getB(CDbl(typeNumber) - 1)
    End Function

    Public Function getMaxLength(ByVal typeNumber As Object, ByVal mode As Object, ByVal errorCorrectLevel As Object) As Object
		Static e, m, t As Integer
		Static QR_MAX_LENGTH As QR_MAX_LENGTH
        t = CDbl(typeNumber) - 1
        e = 0
		m = 0

		Select Case errorCorrectLevel
			Case QR_ERROR_CORRECT_LEVEL_L
				e = 0
			Case QR_ERROR_CORRECT_LEVEL_M
				e = 1
			Case QR_ERROR_CORRECT_LEVEL_Q
				e = 2
			Case QR_ERROR_CORRECT_LEVEL_H
				e = 3
			Case Else
				trigger_error("e:errorCorrectLevel", E_USER_ERROR)
		End Select

		Select Case mode
			Case QR_MODE_NUMBER
				m = 0
			Case QR_MODE_ALPHA_NUM
				m = 1
			Case QR_MODE_8BIT_BYTE
				m = 2
			Case QR_MODE_KANJI
				m = 3
			Case Else
				trigger_error("m:mode", E_USER_ERROR)
		End Select

		Return QR_MAX_LENGTH.getB(t, e, m)
	End Function

	Public Function getErrorCorrectPolynomial(ByVal errorCorrectLength As Object) As QRPolynomial
		Static a, aQRPolynomial As QRPolynomial

		a = New QRPolynomial()
        a.Polynomial(New Object() {1})

        Dim tempForEndVar As Double = CDbl(errorCorrectLength) - 1
        For i As Integer = 0 To tempForEndVar
			aQRPolynomial = New QRPolynomial()
            aQRPolynomial.Polynomial(New Object() {1, aQRMath.gexp(i)})
            a = a.multiply(aQRPolynomial)
		Next

		Return a
	End Function

	Public Function getMask(ByVal maskPattern As Object, ByVal i As Object, ByVal j As Object) As Object


		Dim result As Object = Nothing
		Select Case maskPattern
			Case QR_MASK_PATTERN000
                result = CInt(CDbl(i) + CDbl(j)) Mod 2 = 0
            Case QR_MASK_PATTERN001
				result = CInt(i) Mod 2 = 0
			Case QR_MASK_PATTERN010
				result = CInt(j) Mod 3 = 0
			Case QR_MASK_PATTERN011
                result = CInt(CDbl(i) + CDbl(j)) Mod 3 = 0
            Case QR_MASK_PATTERN100
                result = CInt(Math.Floor(CDbl(i) / 2) + Math.Floor(CDbl(j) / 3)) Mod 2 = 0
            Case QR_MASK_PATTERN101
                result = CInt(CDbl(i) * CDbl(j)) Mod 2 + CInt(CDbl(i) * CDbl(j)) Mod 3 = 0
            Case QR_MASK_PATTERN110
                result = CInt(CInt(CDbl(i) * CDbl(j)) Mod 2 + CInt(CDbl(i) * CDbl(j)) Mod 3) Mod 2 = 0
            Case QR_MASK_PATTERN111
                result = CInt(CInt(CDbl(i) * CDbl(j)) Mod 3 + CInt(CDbl(i) + CDbl(j)) Mod 2) Mod 2 = 0
            Case Else
				trigger_error("mask:maskPattern", E_USER_ERROR)
		End Select
		Return result
	End Function

	Public Function getLostPoint(ByVal QRCode As QRCode) As Object
		Static moduleCount As Double
		Static count, sameCount, darkCount As Integer
		Static dark As Boolean
		Static lostPoint, ratio As Double

        moduleCount = CDbl(QRCode.getModuleCount())

        lostPoint = 0


		' LEVEL1

		For row As Double = 0 To moduleCount - 1

			For col As Double = 0 To moduleCount - 1
				sameCount = 0
				dark = QRCode.isDark(row, col)

				For r As Integer = -1 To 1

					If row + r < 0 Or moduleCount <= row + r Then
						GoTo suite2
					End If

					For c As Integer = -1 To 1

						If Not (col + c < 0 Or moduleCount <= col + c) Then

							If Not (r = 0 And c = 0) Then

								If dark = QRCode.isDark(row + r, col + c) Then
									sameCount += 1
								End If
							End If
						End If

					Next
suite2:
				Next

				If sameCount > 5 Then
					lostPoint += (3 + sameCount - 5)
				End If
			Next
		Next
		' LEVEL2
		For row As Double = 0 To moduleCount - 2
			For col As Double = 0 To moduleCount - 2
				count = 0
				If QRCode.isDark(row, col) Then count += 1
				If QRCode.isDark(row + 1, col) Then count += 1
				If QRCode.isDark(row, col + 1) Then count += 1
				If QRCode.isDark(row + 1, col + 1) Then count += 1
				If count = 0 Or count = 4 Then
					lostPoint += 3
				End If
			Next
		Next

		' LEVEL3
		For row As Double = 0 To moduleCount - 1
			For col As Double = 0 To moduleCount - 7
				If QRCode.isDark(row, col) And Not QRCode.isDark(row, col + 1) And QRCode.isDark(row, col + 2) And QRCode.isDark(row, col + 3) And QRCode.isDark(row, col + 4) And Not QRCode.isDark(row, col + 5) And QRCode.isDark(row, col + 6) Then
					lostPoint += 40
				End If
			Next
		Next

		For col As Double = 0 To moduleCount - 1
			For row As Double = 0 To moduleCount - 7
				If QRCode.isDark(row, col) And Not QRCode.isDark(row + 1, col) And QRCode.isDark(row + 2, col) And QRCode.isDark(row + 3, col) And QRCode.isDark(row + 4, col) And Not QRCode.isDark(row + 5, col) And QRCode.isDark(row + 6, col) Then
					lostPoint += 40
				End If
			Next
		Next

		' LEVEL4
		darkCount = 0
		For col As Double = 0 To moduleCount - 1
			For row As Double = 0 To moduleCount - 1
				If QRCode.isDark(row, col) Then
					darkCount += 1
				End If
			Next
		Next

		ratio = Math.Abs(100 * darkCount / moduleCount / moduleCount - 50) / 5
		lostPoint += ratio * 10

		Return lostPoint
	End Function

	Public Function getMode(ByVal s As String) As Object
        Dim result As Object = Nothing
        If CBool(isAlphaNum(s)) Then
            If CBool(isNumber(s)) Then
                result = QR_MODE_NUMBER
            End If
            Return QR_MODE_ALPHA_NUM
		ElseIf CBool(isKanji(s)) Then 
			Return QR_MODE_KANJI
		Else
			Return QR_MODE_8BIT_BYTE
		End If
	End Function

	Public Function isNumber(ByVal s As Object) As Object
		Dim result As Object = Nothing
		Static c As Integer

        Dim tempForEndVar As Integer = Strings.Len(CStr(s)) - 1
        For i As Integer = 1 To tempForEndVar
            c = Strings.Asc(Strings.Mid(CStr(s), i, i + 1)(0))
            If Not (CDbl(toCharCode("0")) <= c And c <= CDbl(toCharCode("9"))) Then
                result = False
            End If
        Next
		Return True
	End Function

	Public Function isAlphaNum(ByVal s As String) As Object
		Static c As Integer

		For i As Integer = 1 To Strings.Len(s) - 1
			c = Strings.Asc(Strings.Mid(s, i, i + 1)(0))
            If Not (CDbl(toCharCode("0")) <= c And c <= CDbl(toCharCode("9"))) And Not (CDbl(toCharCode("A")) <= c And c <= CDbl(toCharCode("Z"))) And ((" %*+-./:").IndexOf(Strings.Mid(s, i, i + 1)) + 1) = 0 Then
                Return False
            End If
        Next
		Return True
	End Function

	Public Function isKanji(ByVal s As String) As Object
		Static data As String = ""
		Static i As Integer
		Static c As Integer

		data = s
		i = 0
		Do While i + 1 < Strings.Len(data)
			c = ShiftLeft(CInt("&Hff") And Strings.Asc(Strings.Mid(data, i + 1, i + 2)(0)), 8) Or (CInt(CStr(CInt("&Hff")) & CStr(Strings.Asc(Strings.Mid(data, i + 2, i + 3)(0)))))
			If Not (CInt("&H8140") <= c And c <= CInt("&H9FFC")) And Not (CInt("&HE040") <= c And c <= CInt("&HEBBF")) Then
				Return False
			End If

			i += 2
		Loop 

		If i < Strings.Len(data) Then
			Return False
		End If

		Return True
	End Function

	Public Function toCharCode(ByVal s As Object) As Object
        Return Strings.Asc(CStr(s)(0))
    End Function

	Public Function getBCHTypeInfo(ByVal data As Object) As Object
		Static d As Integer

        d = ShiftLeft(CInt(data), 10)
        Do While CDbl(getBCHDigit(d)) - CDbl(getBCHDigit(QR_G15)) >= 0
            d = d Xor ShiftLeft(QR_G15, CDbl(getBCHDigit(d)) - CDbl(getBCHDigit(QR_G15)))
        Loop
        Return (ShiftLeft(CInt(data), 10) Or d) Xor QR_G15_MASK
    End Function

	Public Function getBCHTypeNumber(ByVal data As Object) As Object
		Static d As Integer

        d = ShiftLeft(CInt(data), 12)
        Do While CDbl(getBCHDigit(d)) - CDbl(getBCHDigit(QR_G18)) >= 0
            d = d Xor ShiftLeft(QR_G18, CDbl(getBCHDigit(d)) - CDbl(getBCHDigit(QR_G18)))
        Loop
        getBCHTypeNumber(ShiftLeft(CInt(data), 12) Or d)
    End Function

	Public Function getBCHDigit(ByVal data As Object) As Object
		Static digit As Integer

		digit = 0

        Do While CDbl(data) <> 0
            digit += 1
            data = ShiftRight(CInt(data), 1)
        Loop

        Return digit
	End Function

	Function ShiftRight(ByVal lngNumber As Integer, ByVal intNumBits As Integer) As Integer
		Return lngNumber \ 2 ^ intNumBits 'note the integer division op

	End Function
End Class