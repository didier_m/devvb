Option Strict Off
Option Explicit On
'Imports VB = Microsoft.VisualBasic
Public Class frmPrint
    Inherits System.Windows.Forms.Form

    '-> Indique le fichier en cours de traitement
    Public FichierName As String
    '-> Indique la cl� du spool actuel
    Public SpoolKey As String
    Public IsGetPrinter As Boolean

    Private Const CCHDEVICENAME As Integer = 32
    Private Const CCHFORMNAME As Integer = 32

    Private Structure DevMode
        'UPGRADE_WARNING: La taille de la cha�ne de longueur fixe doit tenir dans la m�moire tampon. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
        <VBFixedString(CCHDEVICENAME), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst:=CCHDEVICENAME)> Public dmDeviceName() As Char
        Dim dmSpecVersion As Short
        Dim dmDriverVersion As Short
        Dim dmSize As Short
        Dim dmDriverExtra As Short
        Dim dmFields As Integer
        Dim dmOrientation As Short
        Dim dmPaperSize As Short
        Dim dmPaperLength As Short
        Dim dmPaperWidth As Short
        Dim dmScale As Short
        Dim dmCopies As Short
        Dim dmDefaultSource As Short
        Dim dmPrintQuality As Short
        Dim dmColor As Short
        Dim dmDuplex As Short
        Dim dmYResolution As Short
        Dim dmTTOption As Short
        Dim dmCollate As Short
        'UPGRADE_WARNING: La taille de la cha�ne de longueur fixe doit tenir dans la m�moire tampon. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
        <VBFixedString(CCHFORMNAME), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst:=CCHFORMNAME)> Public dmFormName() As Char
        Dim dmUnusedPadding As Short
        Dim dmBitsPerPel As Short
        Dim dmPelsWidth As Integer
        Dim dmPelsHeight As Integer
        Dim dmDisplayFlags As Integer
        Dim dmDisplayFrequency As Integer
    End Structure


    Private Structure PRINTER_DEFAULTS
        Dim pDatatype As String
        Dim pDevMode As DevMode
        Dim DesiredAccess As Integer
    End Structure

    Private Const DM_PROMPT As Integer = 4
    Private Const DM_IN_PROMPT As Integer = DM_PROMPT

    'UPGRADE_WARNING: La structure DevMode peut n�cessiter que des attributs de marshaling soient pass�s en tant qu'argument dans cette instruction Declare. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    'UPGRADE_WARNING: La structure DevMode peut n�cessiter que des attributs de marshaling soient pass�s en tant qu'argument dans cette instruction Declare. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    Private Declare Function DocumentProperties Lib "winspool.drv" Alias "DocumentPropertiesA" (ByVal hWnd As Integer, ByVal hPrinter As Integer, ByVal pDeviceName As String, ByRef pDevModeOutput As DevMode, ByRef pDevModeInput As DevMode, ByVal fMode As Integer) As Integer
    'UPGRADE_WARNING: La structure PRINTER_DEFAULTS peut n�cessiter que des attributs de marshaling soient pass�s en tant qu'argument dans cette instruction Declare. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    Private Declare Function OpenPrinter Lib "winspool.drv" Alias "OpenPrinterA" (ByVal pPrinterName As String, ByRef phPrinter As Integer, ByRef pDefault As PRINTER_DEFAULTS) As Integer


    'Private Sub Command1_Click()
    ''-> affichage de la page de proprietes de l'imprimante
    'Dim phPrinter As Long
    'Dim pDefault As PRINTER_DEFAULTS
    'Dim pDevModeIn As DEVMODE
    'Dim pDevModeOut As DEVMODE
    '
    ''-> On recupere un pointeur sur l'imprimante selectionn�e
    'OpenPrinter Me.Combo1.Text, phPrinter, pDefault
    '
    ''-> On affiche la page de proprietes
    'DocumentProperties Me.hwnd, phPrinter, Me.Combo1.Text, pDevModeOut, pDevModeIn, DM_IN_PROMPT
    '
    '
    'End Sub

    Public Sub InitialisationGetPrint()

        '---> Cette proc�dure initialise la liste des imprimantes

        On Error Resume Next

        Dim lpBuffer As String
        Dim Res As Integer
        Dim aLb As Libelle

        '-> Pointer sur la classe libell�
        aLb = Libelles.Item("FRMPRINTVISU")

        '-> Rajouter en entete la visualisation �cran 'MESSPROG
        Me.Combo1.Items.Insert(0, aLb.GetCaption(17))
        IsGetPrinter = True

        '-> Bloquer la nature � imprimer
        Me.Option2.Enabled = False
        Me.Option3.Enabled = False

        '-> Bloquer les zones de saisie
        Me.Text1.Enabled = False
        Me.Text2.Enabled = False
        Me.Label2.Enabled = False
        Me.Label3.Enabled = False

        '-> Chercher une variable d'environnement
        lpBuffer = Space(50)
        Res = GetEnvironmentVariable("TURBODEFAULT", lpBuffer, Len(lpBuffer))
        If Res = 0 Then
            '-> Si on doit s�lectionner l'imprimante par d�faut
            lpBuffer = GetIniString("PRINTER", "DEFAULT", TurboGraphIniFile, False)
            If UCase(Trim(lpBuffer)) = "SCREEN" Then Me.Combo1.SelectedIndex = 0
        Else
            If UCase(Trim(Mid(lpBuffer, 1, Res))) = "SCREEN" Then Me.Combo1.SelectedIndex = 0
        End If
        If Entry(2, Entry(1, Command(), "|"), "~") <> "" Then
            If UCase(Entry(2, Entry(1, Command(), "|"), "~")) = "SCREEN" Then Me.Combo1.SelectedIndex = 0
        End If
        '-> ne pas afficher le bouton de mise en page si on vient d'un dealview
        If IsGetPrinter Then Me.Command4.Visible = False

    End Sub

    Private Sub Command2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command2.Click

        '---> Renvoyer une valeur

        On Error Resume Next

        Dim aLb As Libelle

        '-> Pointer sur la classe libelle
        aLb = Libelles.Item("FRMPRINTVISU")

        '-> V�rifier q'une imprimante soit saisie
        If Me.Combo1.SelectedIndex = -1 Then
            MsgBox(aLb.GetCaption(13), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, aLb.GetToolTip(13))
            Me.Combo1.Focus()
            Exit Sub
        End If

        If Me.Option2.Checked Then
            '-> si page mini s�lectionn�e v�rifier les zones
            If Trim(Me.Text1.Text) = "" Then
                MsgBox(aLb.GetCaption(15), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, aLb.GetToolTip(15))
                Me.Text1.Focus()
                Exit Sub
            End If

            '-> si page mini s�lectionn�e v�rifier les zones
            If Trim(Me.Text2.Text) = "" Then
                MsgBox(aLb.GetCaption(16), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, aLb.GetToolTip(15))
                Me.Text2.Focus()
                Exit Sub
            End If
        End If

        '-> V�rifier le nombre de copies
        If Trim(Me.Text3.Text) = "" Then
            MsgBox(aLb.GetCaption(14), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, aLb.GetToolTip(14))
            Me.Text3.Focus()
            Exit Sub
        End If

        '-> Imprimante
        If Me.Combo1.SelectedIndex = 0 And IsGetPrinter Then
            strRetour = "DEALVIEW"
        Else
            strRetour = Me.Combo1.Text
        End If

        '-> Nature d'impression
        If Me.Option1.Checked Then
            strRetour = strRetour & "|" & 1
        ElseIf Me.Option2.Checked Then
            strRetour = strRetour & "|" & 2 & "�" & Me.Text1.Text & "�" & Me.Text2.Text
        Else
            If Me.Option3.Checked Then
                strRetour = strRetour & "|" & 3
            Else
                strRetour = strRetour & "|" & 4
            End If
        End If

        '-> nombre de copies
        strRetour = strRetour & "|" & Me.Text3.Text

        '-> Teste si impression recto verso
        '-> Teste si impression recto verso
        If Me.Check1.CheckState = 1 Then
            strRetour = strRetour & "|RV"
        Else
            strRetour = strRetour & "|NORV"
        End If

        '-> D�charger la feuille
        Me.Close()

    End Sub

    Private Sub Command3_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command3.Click
        Me.Close()
    End Sub

    Private Sub Command4_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command4.Click
        '---> Renvoyer une valeur

        On Error Resume Next

        Dim aLb As Libelle

        '-> Pointer sur la classe libelle
        aLb = Libelles.Item("FRMPRINTVISU")

        '-> V�rifier q'une imprimante soit saisie
        If Me.Combo1.SelectedIndex = -1 Then
            MsgBox(aLb.GetCaption(13), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, aLb.GetToolTip(13))
            Me.Combo1.Focus()
            Exit Sub
        End If

        If Me.Option2.Checked Then
            '-> si page mini s�lectionn�e v�rifier les zones
            If Trim(Me.Text1.Text) = "" Then
                MsgBox(aLb.GetCaption(15), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, aLb.GetToolTip(15))
                Me.Text1.Focus()
                Exit Sub
            End If

            '-> si page mini s�lectionn�e v�rifier les zones
            If Trim(Me.Text2.Text) = "" Then
                MsgBox(aLb.GetCaption(16), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, aLb.GetToolTip(15))
                Me.Text2.Focus()
                Exit Sub
            End If
        End If

        '-> V�rifier le nombre de copies
        If Trim(Me.Text3.Text) = "" Then
            MsgBox(aLb.GetCaption(14), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, aLb.GetToolTip(14))
            Me.Text3.Focus()
            Exit Sub
        End If

        '-> Imprimante
        If Me.Combo1.SelectedIndex = 0 And IsGetPrinter Then
            strRetour = "DEALVIEW"
        Else
            strRetour = Me.Combo1.Text
        End If

        '-> Nature d'impression
        If Me.Option1.Checked Then
            strRetour = strRetour & "|" & 1
        ElseIf Me.Option2.Checked Then
            strRetour = strRetour & "|" & 2 & "�" & Me.Text1.Text & "�" & Me.Text2.Text
        Else
            If Me.Option3.Checked Then
                strRetour = strRetour & "|" & 3
            Else
                strRetour = strRetour & "|" & 4
            End If
        End If

        '-> nombre de copies
        strRetour = strRetour & "|" & Me.Text3.Text

        '-> Teste si impression recto verso
        '-> Teste si impression recto verso
        If Me.Check1.CheckState = 1 Then
            strRetour = strRetour & "|RVP"
        Else
            strRetour = strRetour & "|NORVP"
        End If

        '-> D�charger la feuille
        Me.Close()

    End Sub

    Private Sub frmPrint_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)

        If KeyAscii = 13 Then Command2_Click(Command2, New System.EventArgs())

        eventArgs.KeyChar = Chr(KeyAscii)
        If KeyAscii = 0 Then
            eventArgs.Handled = True
        End If
    End Sub

    Private Sub frmPrint_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        'UPGRADE_ISSUE: Printer L'objet - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
        Dim aPrint As printer
        Dim aLb As Libelle

        On Error Resume Next

        '-> Charger la liste des imprimantes
        'UPGRADE_ISSUE: Printers L'objet - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
        For Each aPrint In Printers
            'UPGRADE_ISSUE: Printer propri�t� aPrint.DeviceName - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            Me.Combo1.Items.Add(aPrint.DeviceName)
            'UPGRADE_ISSUE: Printer propri�t� Printer.DeviceName - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            'UPGRADE_ISSUE: Printer propri�t� aPrint.DeviceName - Mise � niveau non effectu�e. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            'If aPrint.DeviceName = Printer.DeviceName Then Me.Combo1.SelectedIndex = Me.Combo1.Items.Count - 1
        Next aPrint

        '-> Pointer vers la classe des libelles
        aLb = Libelles.Item("FRMPRINTVISU")

        '-> Charger les libell�s
        Me.Text = aLb.GetCaption(1)
        Me.Label1.Text = aLb.GetCaption(2)
        Me.Frame2.Text = aLb.GetCaption(3)
        Me.Option1.Text = aLb.GetCaption(4)
        Me.Option2.Text = aLb.GetCaption(5)
        Me.Label2.Text = aLb.GetCaption(6)
        Me.Label3.Text = aLb.GetCaption(7)
        Me.Option3.Text = aLb.GetCaption(8)
        Me.Frame3.Text = aLb.GetCaption(9)
        Me.Label4.Text = aLb.GetCaption(10)
        Me.Command2.Text = aLb.GetCaption(11)
        VB6.SetCancel(Me.Command3, True)
        Me.Command3.Text = aLb.GetCaption(12)
        Me.Command1.Text = aLb.GetCaption(18)
        Me.Command4.Text = aLb.GetCaption(21)

        Me.Check1.Text = aLb.GetCaption(19)
        Me.Option4.Text = aLb.GetCaption(20)

    End Sub

    'UPGRADE_WARNING: L'�v�nement Option1.CheckedChanged peut se d�clencher lorsque le formulaire est initialis�. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub Option1_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Option1.CheckedChanged
        If eventSender.Checked Then

            Me.Text1.Enabled = False
            Me.Text2.Enabled = False
            Me.Label2.Enabled = False
            Me.Label3.Enabled = False

        End If
    End Sub

    'UPGRADE_WARNING: L'�v�nement Option2.CheckedChanged peut se d�clencher lorsque le formulaire est initialis�. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub Option2_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Option2.CheckedChanged
        If eventSender.Checked Then

            Me.Text1.Enabled = True
            Me.Text2.Enabled = True
            Me.Label2.Enabled = True
            Me.Label3.Enabled = True

        End If
    End Sub

    'UPGRADE_WARNING: L'�v�nement Option3.CheckedChanged peut se d�clencher lorsque le formulaire est initialis�. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub Option3_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Option3.CheckedChanged
        If eventSender.Checked Then

            Option1_CheckedChanged(Option1, New System.EventArgs())

        End If
    End Sub

    Private Sub Text1_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text1.KeyPress
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)

        If KeyAscii < 48 Or KeyAscii > 57 Then
            If KeyAscii <> 8 Then KeyAscii = 0
        End If

        eventArgs.KeyChar = Chr(KeyAscii)
        If KeyAscii = 0 Then
            eventArgs.Handled = True
        End If
    End Sub

    Private Sub Text2_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text2.KeyPress
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)

        If KeyAscii < 48 Or KeyAscii > 57 Then
            If KeyAscii <> 8 Then KeyAscii = 0
        End If

        eventArgs.KeyChar = Chr(KeyAscii)
        If KeyAscii = 0 Then
            eventArgs.Handled = True
        End If
    End Sub

    Private Sub Text3_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles Text3.KeyPress
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)

        If KeyAscii < 48 Or KeyAscii > 57 Then
            If KeyAscii <> 8 Then KeyAscii = 0
        End If

        eventArgs.KeyChar = Chr(KeyAscii)
        If KeyAscii = 0 Then
            eventArgs.Handled = True
        End If
    End Sub

    '   'Private Sub UpDown1_Change(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles UpDown1.Change

    '	Me.Text3.SelectionStart = 0
    '	Me.Text3.SelectionLength = Len(Me.Text3.Text)
    '	Me.Text3.Focus()

    'End Sub
End Class