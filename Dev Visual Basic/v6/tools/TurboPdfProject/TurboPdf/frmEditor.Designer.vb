<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmEditor
#Region "Code g�n�r� par le Concepteur Windows Form "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Cet appel est requis par le Concepteur Windows Form.
		InitializeComponent()
		'Ce formulaire est un enfant MDI.
		'Ce code simule 
		' la fonctionnalit� VB6
		' de chargement et d'affichage automatique d'un
		' parent de l'enfant MDI.
        Me.MDIParent = TurboPdf.MDIMain
        TurboPdf.MDIMain.Show()
	End Sub
	'Form remplace la m�thode Dispose pour nettoyer la liste des composants.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requise par le Concepteur Windows Form
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public CommonDialog1Open As System.Windows.Forms.OpenFileDialog
	Public CommonDialog1Save As System.Windows.Forms.SaveFileDialog
	Public CommonDialog1Font As System.Windows.Forms.FontDialog
	Public CommonDialog1Print As System.Windows.Forms.PrintDialog
	Public WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
	Public WithEvents mnuOpen As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents sep1 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuSave As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSaveAs As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents sep2 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents sep3 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuQuit As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFichier As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuCut As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuCopy As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuPaste As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents sep4 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuFind As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuEdition As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuPolice As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFormat As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuApropos As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuApp As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MainMenu1 As System.Windows.Forms.MenuStrip
	'REMARQUE�: la proc�dure suivante est requise par le Concepteur Windows Form
	'Elle peut �tre modifi�e � l'aide du Concepteur Windows Form.
	'Ne la modifiez pas � l'aide de l'�diteur de code.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEditor))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.CommonDialog1Open = New System.Windows.Forms.OpenFileDialog
		Me.CommonDialog1Save = New System.Windows.Forms.SaveFileDialog
		Me.CommonDialog1Font = New System.Windows.Forms.FontDialog
		Me.CommonDialog1Print = New System.Windows.Forms.PrintDialog
		Me.CommonDialog1Print.PrinterSettings = New System.Drawing.Printing.PrinterSettings
		Me.RichTextBox1 = New System.Windows.Forms.RichTextBox
		Me.MainMenu1 = New System.Windows.Forms.MenuStrip
		Me.mnuFichier = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuOpen = New System.Windows.Forms.ToolStripMenuItem
		Me.sep1 = New System.Windows.Forms.ToolStripSeparator
		Me.mnuSave = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuSaveAs = New System.Windows.Forms.ToolStripMenuItem
		Me.sep2 = New System.Windows.Forms.ToolStripSeparator
		Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
		Me.sep3 = New System.Windows.Forms.ToolStripSeparator
		Me.mnuQuit = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuEdition = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuCut = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuCopy = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuPaste = New System.Windows.Forms.ToolStripMenuItem
		Me.sep4 = New System.Windows.Forms.ToolStripSeparator
		Me.mnuFind = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuFormat = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuPolice = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuApp = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuApropos = New System.Windows.Forms.ToolStripMenuItem
		Me.MainMenu1.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.Text = "Editeur de texte"
		Me.ClientSize = New System.Drawing.Size(514, 411)
		Me.Location = New System.Drawing.Point(4, 42)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmEditor"
		Me.RichTextBox1.Size = New System.Drawing.Size(489, 225)
		Me.RichTextBox1.Location = New System.Drawing.Point(8, 8)
		Me.RichTextBox1.TabIndex = 0
		Me.RichTextBox1.Enabled = True
		Me.RichTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Both
		Me.RichTextBox1.RTF = resources.GetString("RichTextBox1.TextRTF")
		Me.RichTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.RichTextBox1.Name = "RichTextBox1"
		Me.mnuFichier.Name = "mnuFichier"
		Me.mnuFichier.Text = "&Fichier"
		Me.mnuFichier.Checked = False
		Me.mnuFichier.Enabled = True
		Me.mnuFichier.Visible = True
		Me.mnuOpen.Name = "mnuOpen"
		Me.mnuOpen.Text = "&Ouvrir"
		Me.mnuOpen.Checked = False
		Me.mnuOpen.Enabled = True
		Me.mnuOpen.Visible = True
		Me.sep1.Enabled = True
		Me.sep1.Visible = True
		Me.sep1.Name = "sep1"
		Me.mnuSave.Name = "mnuSave"
		Me.mnuSave.Text = "&Enregistrer"
		Me.mnuSave.Checked = False
		Me.mnuSave.Enabled = True
		Me.mnuSave.Visible = True
		Me.mnuSaveAs.Name = "mnuSaveAs"
		Me.mnuSaveAs.Text = "En&rgistrer sous"
		Me.mnuSaveAs.Checked = False
		Me.mnuSaveAs.Enabled = True
		Me.mnuSaveAs.Visible = True
		Me.sep2.Enabled = True
		Me.sep2.Visible = True
		Me.sep2.Name = "sep2"
		Me.mnuPrint.Name = "mnuPrint"
		Me.mnuPrint.Text = "&Imprimer"
		Me.mnuPrint.Checked = False
		Me.mnuPrint.Enabled = True
		Me.mnuPrint.Visible = True
		Me.sep3.Enabled = True
		Me.sep3.Visible = True
		Me.sep3.Name = "sep3"
		Me.mnuQuit.Name = "mnuQuit"
		Me.mnuQuit.Text = "&Quitter"
		Me.mnuQuit.Checked = False
		Me.mnuQuit.Enabled = True
		Me.mnuQuit.Visible = True
		Me.mnuEdition.Name = "mnuEdition"
		Me.mnuEdition.Text = "Edition"
		Me.mnuEdition.Checked = False
		Me.mnuEdition.Enabled = True
		Me.mnuEdition.Visible = True
		Me.mnuCut.Name = "mnuCut"
		Me.mnuCut.Text = "Couper"
		Me.mnuCut.Checked = False
		Me.mnuCut.Enabled = True
		Me.mnuCut.Visible = True
		Me.mnuCopy.Name = "mnuCopy"
		Me.mnuCopy.Text = "&Copier"
		Me.mnuCopy.Checked = False
		Me.mnuCopy.Enabled = True
		Me.mnuCopy.Visible = True
		Me.mnuPaste.Name = "mnuPaste"
		Me.mnuPaste.Text = "Coller"
		Me.mnuPaste.Checked = False
		Me.mnuPaste.Enabled = True
		Me.mnuPaste.Visible = True
		Me.sep4.Enabled = True
		Me.sep4.Visible = True
		Me.sep4.Name = "sep4"
		Me.mnuFind.Name = "mnuFind"
		Me.mnuFind.Text = "Rechercher"
		Me.mnuFind.ShortcutKeys = CType(System.Windows.Forms.Keys.Control or System.Windows.Forms.Keys.F, System.Windows.Forms.Keys)
		Me.mnuFind.Checked = False
		Me.mnuFind.Enabled = True
		Me.mnuFind.Visible = True
		Me.mnuFormat.Name = "mnuFormat"
		Me.mnuFormat.Text = "Format"
		Me.mnuFormat.Checked = False
		Me.mnuFormat.Enabled = True
		Me.mnuFormat.Visible = True
		Me.mnuPolice.Name = "mnuPolice"
		Me.mnuPolice.Text = "&Police"
		Me.mnuPolice.Checked = False
		Me.mnuPolice.Enabled = True
		Me.mnuPolice.Visible = True
		Me.mnuApp.Name = "mnuApp"
		Me.mnuApp.Text = "?"
		Me.mnuApp.Checked = False
		Me.mnuApp.Enabled = True
		Me.mnuApp.Visible = True
		Me.mnuApropos.Name = "mnuApropos"
		Me.mnuApropos.Text = "A propos de ..."
		Me.mnuApropos.Checked = False
		Me.mnuApropos.Enabled = True
		Me.mnuApropos.Visible = True
		Me.Controls.Add(RichTextBox1)
		MainMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem(){Me.mnuFichier, Me.mnuEdition, Me.mnuFormat, Me.mnuApp})
		mnuFichier.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem(){Me.mnuOpen, Me.sep1, Me.mnuSave, Me.mnuSaveAs, Me.sep2, Me.mnuPrint, Me.sep3, Me.mnuQuit})
		mnuEdition.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem(){Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.sep4, Me.mnuFind})
		mnuFormat.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem(){Me.mnuPolice})
		mnuApp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem(){Me.mnuApropos})
		Me.Controls.Add(MainMenu1)
		Me.MainMenu1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class