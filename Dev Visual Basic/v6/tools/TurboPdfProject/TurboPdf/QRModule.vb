Option Strict Off
Option Explicit On
Imports System
Friend Class QRModule

	Public Enum typValue
		ok = 1
		nok = 0
		vide = -1
		out = -2
	End Enum

    Private Module_Renamed(300, 300) As typValue

    Public Sub setValue(ByVal vdata As typValue, ByVal ind1 As Integer, ByVal ind2 As Integer)
		Module_Renamed(ind1, ind2) = vdata
	End Sub

	Public Function getValue(ByVal ind1 As Integer, ByVal ind2 As Integer) As typValue
		Return Module_Renamed(ind1, ind2)
	End Function

	Public Sub createNullArray(ByVal ind1 As Integer, ByVal iCount As Integer)

		For ind2 As Integer = 0 To 300
			Module_Renamed(ind1, ind2) = -2
		Next

		For ind2 As Integer = 0 To iCount
			Module_Renamed(ind1, ind2) = -1
		Next

	End Sub
End Class