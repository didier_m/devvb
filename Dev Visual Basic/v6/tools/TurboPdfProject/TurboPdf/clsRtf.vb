'Imports System
'Imports System.Drawing
Public Class clsRtfSection
    '--> cette classe contient la description de chaque portion de texte issue d'un RTF
    Public Text As String = ""
    Public Fnt As Drawing.Font
    Public Width As Single
    Public Height As Single
    Public WidthSection As Single
    Public HeightSection As Single 'pour g�rer les retour de ligne
    Public FntRef As String
    Public FntColor As System.Drawing.Color
    Public Ligne As Integer = 0 'numero de la ligne pour pointer sur l'objet ligne
    Public PosX As Single = 0 'position du texte sur la ligne par rapport au 1er caractere
End Class
Public Class clsRtfLigne
    '-> infos sur les lignes du rtf 
    Public Num As Integer = 1
    Public Height As Double
    Public Width As Double
    Public Align As Integer
    Public PosY As Double = 0 'posion de la ligne dans le RTF

End Class

Public Class clsRtf
    Public RTFblocks As Collection
    Public RTFsection As clsRtfSection 'contient chaque portion du rtf avec ses propri�t�s
    Public PosX As Double
    Public PosY As Double
    Public Ligne As clsRtfLigne
    Public Lignes As Collection
    Public tabs As Single = 0.7875 * 1.65 '1.4 '1.75  '2

    Public Function SetRtf(ByRef objRtf As System.Windows.Forms.RichTextBox) As Boolean
        '-> cette fonction va permettre de charger la classe des RTF
        '-> on analyse chaque caract�re pour d�terminer � quelle section ils appartiennent
        Dim i As Integer
        Dim isNew As Boolean
        Dim isNewLigne As Boolean
        Dim FontAlias As String
        Dim num1 As Single
        Dim num2 As Single = 0 'nombre de lignes
        Dim TopNew As Boolean = False
        Dim TopNewLigne As Boolean = False
        RTFsection = New clsRtfSection
        '-> pour la gestion des lignes
        '-> on ne garde que le chr(10)
        objRtf.Rtf = Strings.Replace(objRtf.Rtf, ChrW(13) & ChrW(10), ChrW(10), 1, -1, 0)
        TempRtf.Rtf = objRtf.Rtf
        TempRtf.Height = objRtf.Height '* 37.2 '40
        TempRtf.Width = objRtf.Width '* 37.2 '40
        For i = 0 To Len(objRtf.Text) - 1
            '-> mettre la valeur par defaut
            isNew = True
            isNewLigne = False
            '-> on selectionne le caractere
            objRtf.SelectionStart = i
            objRtf.SelectionLength = 1
            '-> on compare les propri�t�s du caract�re avec celles de la derni�re section
            '-> on regarde si il y a pas un retour de ligne automatique ou la pr�sence d'un retour chariot
            If Asc(objRtf.SelectedText) = 10 Then
                isNew = True
                isNewLigne = True
            Else
                If i <> 1 Then
                    If objRtf.SelectionFont.Equals(RTFsection.Fnt) And objRtf.SelectionAlignment = Ligne.Align Then
                        isNew = False
                    End If
                End If
            End If
            If TopNew Then
                TopNew = False
                isNew = True
                If TopNewLigne Then
                    isNewLigne = True
                End If
            End If
            '-> si c'est une tabulation on d�fini une nouvelle section
            If Asc(objRtf.SelectedText) = 9 Then
                isNew = True
                TopNew = True
            End If
            '-> on gere ici un eventuel saut de page
            If Asc(objRtf.SelectedText) = 9 And i > 1 Then
                If Ligne.Width > RTFsection.WidthSection Then
                    isNew = False
                    RTFsection.Text = RTFsection.Text & " "
                End If
            End If
            '-> si on est sur une nouvelle section 
            If isNew Then
                RTFsection = New clsRtfSection
                '-> on applique les nouvelles propri�t�s
                RTFsection.Fnt = objRtf.SelectionFont
                If Not isNewLigne Then RTFsection.Text = objRtf.SelectedText
                If TopNewLigne Then RTFsection.Text = objRtf.SelectedText
                RTFsection.WidthSection = objRtf.Width / CDbl(37.79524) - 0.25 'avec petite marge interne
                RTFblocks.Add(RTFsection, objRtf.Name & "|" & (RTFblocks.Count + 1))
                '-> on pointe sur la font en l'ajoutant si besoin
                FontAlias = aPrinter.AddFont(GetFontFileName(objRtf.SelectionFont.Name), "", False)
                If FontAlias = "" Then FontAlias = objRtf.SelectionFont.Name
                '-> Appliquer les options de font
                Dim acellStyle = vbPDF.pdfFontStyle.pdfNormal
                '-> si la cellule est en gras
                If objRtf.SelectionFont.Bold Then acellStyle = acellStyle Or vbPDF.pdfFontStyle.pdfBold
                '-> si la cellule est en italique
                If objRtf.SelectionFont.Italic Then acellStyle = acellStyle Or vbPDF.pdfFontStyle.pdfItalic
                '-> si la cellule est soulign�e
                If objRtf.SelectionFont.Underline Then acellStyle = acellStyle Or vbPDF.pdfFontStyle.pdfUnderline
                '-> on d�finit la couleur
                RTFsection.FntColor = objRtf.SelectionColor
                '-> on applique le style
                RTFsection.FntRef = aPrinter.SetFont(FontAlias, objRtf.SelectionFont.Size, acellStyle)
                RTFsection.FntRef = FontAlias
                '-> on determine la hauteur de la section
                RTFsection.Height = aPrinter.TextHeight() * 0.94
                '-> on defini la position du debut du texte
                If RTFblocks.Count > 1 And Not isNewLigne Then RTFsection.PosX = RTFblocks(objRtf.Name & "|" & (RTFblocks.Count - 1)).posX + RTFblocks(objRtf.Name & "|" & (RTFblocks.Count - 1)).Width

            Else
                '-> on gere ici les sauts de lignes
                '-> on regarde pour cela sur qu'elle ligne on est
                'Dim jj As Integer
                'Dim kk As Integer = 0
                'For jj = 0 To objRtf.Lines.Length - 1
                '    kk = kk + Len(objRtf.Lines(jj)) + 1
                '    If kk >= i Then Exit For
                'Next
                'Dim returnValue As Point
                'returnValue = objRtf.GetPositionFromCharIndex(i)
                If TempRtf.GetPositionFromCharIndex(i).Y <> TempRtf.GetPositionFromCharIndex(i - 1).Y Then
                    'If jj > Ligne.Num Then
                    'If Ligne.Width + aPrinter.TextLength(objRtf.SelectedText) > RTFsection.WidthSection Then
                    Dim pos As Integer
                    Dim txtL As Double
                    '-> on remonte le mot
                    pos = InStrRev(RTFsection.Text, " ")
                    If pos = 0 Then
                        'on continue la ligne
                    Else
                        txtL = aPrinter.TextLength(Mid(RTFsection.Text, pos))
                        RTFsection.Width = RTFsection.Width - txtL
                        i = i - (Len(RTFsection.Text) - pos) - 1
                        Ligne.Width = Ligne.Width - txtL
                        RTFsection.Text = Mid(RTFsection.Text, 1, pos)
                        TopNew = True
                        TopNewLigne = True
                        GoTo suite
                    End If
                End If
                RTFsection.Text = RTFsection.Text & objRtf.SelectedText
            End If
            '-> on calcule la largeur du texte en traitant le cas des tabulations
            If AscW(objRtf.SelectedText) = 9 Then
                '-> on est sur une tabulation on determine sa largeur en l'�tat actuel
                Dim num3 As Double
                num3 = (RTFsection.PosX + RTFsection.Width) Mod tabs
                If Math.Round(num3, 4) = Math.Round(tabs, 4) Then num3 = 0
                num1 = tabs - num3
            Else
                num1 = aPrinter.TextLength(objRtf.SelectedText)
            End If
            If Not isNewLigne Or TopNewLigne Then RTFsection.Width = RTFsection.Width + num1
            '-> si on est sur une nouvelle ligne
            If isNewLigne Or i = 0 Then
                Ligne = New clsRtfLigne
                num2 += 1
                Ligne.Num = num2
                Ligne.Height = RTFsection.Height
                Ligne.Width = num1
                Ligne.Align = objRtf.SelectionAlignment
                '-> nouvelle position dans le RTF
                If Ligne.Num <> 1 Then Ligne.PosY = RTFlignes(objRtf.Name & Ligne.Num - 1).posY + RTFlignes(objRtf.Name & Ligne.Num - 1).Height
                RTFlignes.Add(Ligne, objRtf.Name & Ligne.Num)
                RTFsection.Ligne = num2
                If isNewLigne And i = 0 Then
                    Ligne = New clsRtfLigne
                    num2 += 1
                    Ligne.Num = num2
                    Ligne.Height = RTFsection.Height
                    Ligne.Width = num1
                    Ligne.Align = objRtf.SelectionAlignment
                    '-> nouvelle position dans le RTF
                    If Ligne.Num <> 1 Then Ligne.PosY = RTFlignes(objRtf.Name & Ligne.Num - 1).posY + RTFlignes(objRtf.Name & Ligne.Num - 1).Height
                    RTFlignes.Add(Ligne, objRtf.Name & Ligne.Num)
                    RTFsection.Ligne = num2
                End If
            Else
                '-> on met a jour la hauteur de ligne et la largeur
                If RTFsection.Height > Ligne.Height Then Ligne.Height = RTFsection.Height
                Ligne.Width = Ligne.Width + num1
                'If objRtf.SelectionAlignment <> 0 Then Ligne.Align = objRtf.SelectionAlignment
                Ligne.Align = objRtf.SelectionAlignment
            End If
            RTFsection.HeightSection = Math.Ceiling((RTFsection.Width - RTFsection.PosX + 0.0000001) / objRtf.Width) * RTFsection.Height
            '-> faire pointer la section sur la bonne ligne
            RTFsection.Ligne = num2
            TopNewLigne = False
suite:
        Next
        '-> On lance une analyse de l'objet pour v�rifier les sauts de ligne
        'TempRtf = Nothing
        RTFsection = Nothing
        RTFobj.Add(RTFblocks, objRtf.Name)
    End Function

    Public Sub New()
        RTFobj = New Collection
        RTFblocks = New Collection
        RTFlignes = New Collection
        Ligne = New clsRtfLigne
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class

