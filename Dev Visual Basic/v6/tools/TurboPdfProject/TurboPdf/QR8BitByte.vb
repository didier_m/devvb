Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Friend Class QR8BitByte
	'---------------------------------------------------------------
	' QR8BitByte
	'---------------------------------------------------------------
	Dim aQRData As QRData


	Public Function QR8BitByte(ByVal data As Object) As QRData
		aQRData = New QRData()
		aQRData.QRData(QR_MODE_8BIT_BYTE, data)
		Return aQRData
	End Function

	'-> before write renamed in writeB
	Public Function writeB(ByVal buffer As Object) As Object
        Dim data() As String = aQRData.getData()
        For i As Integer = 0 To data.GetUpperBound(0) - 1
            buffer.put(Strings.Asc(data(i)(0)), 8)
        Next
	End Function

	Public Function getLength() As Object
        Return Strings.Len(CStr(aQRData.getData()))
    End Function
End Class