Option Strict Off
Option Explicit On
Public Class Tableau
    '**********************************************
    '*                                            *
    '* Cette classe contient la d�finition d'un   *
    '* Objet Tableau                              *
    '*                                            *
    '**********************************************

    '---> Propri�t�s de l'objet
    Public Nom As String
    Public Orientation As Short
    Public Largeur As Single
    Public IdAffichage As Short
    Private pOrdreAffichage() As String

    '-> Cette variable indique l'index du prochain block � cr�er
    Public nBlocks As Short
    Public nEntries As Short

    '---> Objets internes
    Public Blocks As Collection

    Private Sub Class_Initialize_Renamed()
        Dim nBlock As Object = Nothing
        Blocks = New Collection
        nBlocks = 1
        nEntries = nBlock
    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub

    Public Function GetOrdreAffichage(ByVal nIndex As Short) As String
        GetOrdreAffichage = pOrdreAffichage(nIndex)
    End Function

    Public Function AddOrdreAffichage(ByVal Value As String) As Short
        ReDim Preserve pOrdreAffichage(nBlocks)
        pOrdreAffichage(nBlocks) = Value
        AddOrdreAffichage = nBlocks
        nBlocks = nBlocks + 1
        nEntries = nBlocks
    End Function

    Public Sub ClearOrdreAffichage()
        Erase pOrdreAffichage
        nBlocks = 1
        nEntries = 1
    End Sub
End Class