Option Strict Off
Option Explicit On
Public Class Maquette
    '**********************************************
    '*                                            *
    '* Cette classe contient la d�finition d'un   *
    '* Objet Maquette                             *
    '*                                            *
    '**********************************************


    '---> Liste des propri�t�s
    Public Nom As String
    Public Orientation As String
    Public Hauteur As Single
    Public Largeur As Single
    Public MargeTop As Single
    Public MargeLeft As Single
    Public PageGarde As Boolean
    Public PageSelection As Boolean
    Public Progiciel As String
    Public Client As String
    Public NavigaHTML As String

    '---> Collections du model objet
    Public Sections As Collection
    Public Tableaux As Collection

    '---> Stocker les ordres d'affichage dans un tableau dynamic
    Private pOrdreAffichage() As String
    Public nEntries As Short 'var utilis�e pour le redimensionnement de la matrice des ordres d'affichage

    '---> Gestion du Mutlilangue
    Private aLb As Libelle

    'UPGRADE_NOTE: Class_Initializea �t� mis � niveau vers Class_Initialize_Renamed. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()

        '-> Initialisation des classes propri�t�s

        Sections = New Collection
        Tableaux = New Collection

        '-> index pour les ordres d'affichage
        nEntries = 1

    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub

    Public Function AddOrdreAffichage(ByVal Value As String) As Short
        'UPGRADE_WARNING: La limite inf�rieure du tableau pOrdreAffichage est pass�e de 1 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
        ReDim Preserve pOrdreAffichage(nEntries)
        pOrdreAffichage(nEntries) = Value
        AddOrdreAffichage = nEntries
        nEntries = nEntries + 1
    End Function

    Public Function GetOrdreAffichage(ByVal nIndex As Short) As String
        GetOrdreAffichage = pOrdreAffichage(nIndex)
    End Function
End Class