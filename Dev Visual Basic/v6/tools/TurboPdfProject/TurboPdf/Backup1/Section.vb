Option Strict Off
Option Explicit On
Public Class Section
    '**********************************************
    '*                                            *
    '* Cette classe contient la d�finition d'un   *
    '* Objet Section                              *
    '*                                            *
    '**********************************************

    '---> Liste des propri�t�s
    Public Nom As String
    Public Contour As Boolean
    Public Hauteur As Single
    Public Largeur As Single
    Public AlignementTop As Short
    Public Top As Single
    Public AlignementLeft As Short
    Public Left_Renamed As Single
    Public BackColor As Integer
    Public IdAffichage As Short
    Public nEntries As Short
    Private pOrdreAffichage() As String

    '-> Valeurs pour la propri�t� AlignementTop
    '-> Libre (1)
    '-> Marge haut (2)
    '-> Centr� (3)
    '-> Marge Bas (4)
    '-> Sp�cifi� (5)

    '-> Valeurs pour la propri�t� AlignementLeft
    '-> Libre (1) N'EST PLUS VALABLE
    '-> Marge gauche (2)
    '-> Centr� (3)
    '-> Marge droite (4)
    '-> Sp�cifi� (5)

    '-> Pour AlignementTop et AlignementLeft , les valeurs Top et Left ne sont _
    'accessibles que pour la valeur Sp�cifi� (5)

    Public Cadres As Collection
    Public Bmps As Collection

    '---> Maquette de transition
    Public RangChar As String

    '-> Pour chargement du contenu RTF
    Public TempoRTF As Short 'stocker le handle du fichier ouvert
    Public TempoRtfString As String ' stocker le nom du fichier ouvert

    Public IdRTf As Short

    'UPGRADE_NOTE: Class_Initializea �t� mis � niveau vers Class_Initialize_Renamed. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        Dim nBmp As Object
        Dim nCadre As Object

        '-> Index du cadre
        nCadre = 1
        '-> Index du prochain objet dans le tableau des ordres d'affichage
        nEntries = 1
        '-> Index du prochain Bmp
        nBmp = 1

        '-> Initaliser les collections
        Cadres = New Collection
        Bmps = New Collection

    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub
    Public Function AddOrdreAffichage(ByVal Value As String) As Short

        ReDim Preserve pOrdreAffichage(nEntries)
        pOrdreAffichage(nEntries) = Value
        AddOrdreAffichage = nEntries
        nEntries = nEntries + 1

    End Function

    Public Function GetOrdreAffichage(ByVal nIndex As Short) As String
        GetOrdreAffichage = pOrdreAffichage(nIndex)
    End Function
End Class