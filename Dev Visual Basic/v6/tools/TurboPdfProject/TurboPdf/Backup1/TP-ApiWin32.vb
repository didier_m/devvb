Option Strict Off
Option Explicit On
Module ApiWin32
	'-> S�parateur de millier : gestion des variables d'environnement
    Public Const LOCALE_STHOUSAND As Short = &HFS
    Public Const LOCALE_USER_DEFAULT As Integer = &H400S
    Public Const LOCALE_SDECIMAL As Short = 14

    Public Declare Function GetEnvironmentVariable Lib "kernel32" Alias "GetEnvironmentVariableA" (ByVal lpName As String, ByVal lpBuffer As String, ByVal nSize As Integer) As Integer
	
	'-> API de lecture des fichiers au format "*.ini"
	Public Declare Function GetPrivateProfileSection Lib "kernel32"  Alias "GetPrivateProfileSectionA"(ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
	Public Declare Function GetPrivateProfileString Lib "kernel32"  Alias "GetPrivateProfileStringA"(ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer

	'-> Api pour gestion du Registre
    Public Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Integer, ByVal lpValueName As String, ByVal lpReserved As Integer, ByRef lpType As Integer, ByRef lpData As Long, ByRef lpcbData As Integer) As Integer
	
    Public Const REG_SZ As Short = 1

    '-> Api gestion des fichiers tempo
    Public Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Integer, ByVal lpBuffer As String) As Integer
    Public Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Integer, ByVal lpTempFileName As String) As Integer
	
	'-> Api param�trage system
    Public Declare Function GetLocaleInfo Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Integer, ByVal LCType As Integer, ByVal lpLCData As String, ByVal cchData As Integer) As Integer
    
	'-> Api pour gestion des fichiers
    Public Declare Function CopyFile Lib "kernel32" Alias "CopyFileA" (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal bFailIfExists As Integer) As Integer
	
	'-> Api pour gestion de l'impression au format RTF
    Public Declare Function SetMapMode Lib "gdi32" (ByVal hDC As Integer, ByVal nMapMode As Integer) As Integer
	Public Declare Function GetDeviceCaps Lib "gdi32" (ByVal hDC As Integer, ByVal nIndex As Integer) As Integer
    Public Declare Function RoundRect Lib "gdi32" (ByVal hDC As Integer, ByVal X1 As Integer, ByVal Y1 As Integer, ByVal X2 As Integer, ByVal Y2 As Integer, ByVal X3 As Integer, ByVal Y3 As Integer) As Integer
	
	'-> Pour gestion des codes RTF et SendMessage
    Public Const WM_USER As Integer = &H400S

    '-> Pour gestion des fichiers
    Public Const OFS_MAXPATHNAME As Integer = 128
	
    Public Structure OFSTRUCT
        Dim cBytes As Byte
        Dim fFixedDisk As Byte
        Dim nErrCode As Short
        Dim Reserved1 As Short
        Dim Reserved2 As Short
        <VBFixedArray(OFS_MAXPATHNAME)> Dim szPathName() As Byte

        Public Sub Initialize()
            ReDim szPathName(OFS_MAXPATHNAME)
        End Sub
    End Structure
	
    Public Declare Function OleTranslateColor Lib "oleaut32.dll" (ByVal lOleColor As Integer, ByVal lHPalette As Integer, ByRef lColorRef As Integer) As Integer
	
    ' pour Font et BackColor
	Public Const CFM_BACKCOLOR As Integer = &H4000000
	Public Const CFE_AUTOBACKCOLOR As Integer = CFM_BACKCOLOR
	
    Public Function RegQueryStringValue(ByVal hKey As Integer, ByVal strValueName As String) As String
        Dim REG_BINARY As Object

        '---> R�cup�ration d'une valeur d'une cl� de registre

        Dim lResult As Integer
        Dim lValueType As Integer
        Dim strBuf As String
        Dim lDataBufSize As Integer
        REG_BINARY = Nothing
        RegQueryStringValue = ""
        lResult = RegQueryValueEx(hKey, strValueName, 0, lValueType, 0, lDataBufSize)
        Dim strData As Short
        If lResult = 0 Then
            If lValueType = REG_SZ Then
                'Create a buffer
                strBuf = New String(Chr(0), lDataBufSize)
                'retrieve the key's content
                lResult = RegQueryValueEx(hKey, strValueName, 0, 0, strBuf, lDataBufSize)
                If lResult = 0 Then
                    'Remove the unnecessary chr$(0)'s
                    RegQueryStringValue = Left(strBuf, InStr(1, strBuf, Chr(0)) - 1)
                End If
            ElseIf lValueType = REG_BINARY Then
                'retrieve the key's value
                lResult = RegQueryValueEx(hKey, strValueName, 0, 0, strData, lDataBufSize)
                If lResult = 0 Then
                    RegQueryStringValue = CStr(strData)
                End If
            End If
        End If

    End Function
	
	
    Public Function GetRegKeyValue(ByVal root As Integer, ByVal key_name As String, ByVal subkey_name As String) As String
        Dim ERROR_SUCCESS As Object
        Dim hKey As Integer
        Dim value As String
        Dim length As Integer
        Dim value_type As Integer
        ' Retourne une cle du registre.
        ' ouvriri la cl�.
        GetRegKeyValue = ""
        ERROR_SUCCESS = Nothing
        If RegOpenKeyEx(root, key_name, 0, &H1S, hKey) <> ERROR_SUCCESS Then
            Exit Function
        End If

        ' taille de la cl�.
        If RegQueryValueEx(hKey, subkey_name, 0, value_type, 0, length) <> ERROR_SUCCESS Then
        End If

        ' valeur de la cl�.
        value = Space(length)
        If RegQueryValueEx(hKey, subkey_name, 0, value_type, value, length) <> ERROR_SUCCESS Then
        Else
            ' nettoyer
            GetRegKeyValue = Left(value, length - 1)
        End If

        ' fermer.
        If RegCloseKey(hKey) <> ERROR_SUCCESS Then
        End If
    End Function
	
End Module