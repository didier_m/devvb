﻿Imports System.ComponentModel

Imports System.Drawing

Imports System.Text

Imports System.Windows.Forms

Imports System.Runtime.InteropServices


    Public Class Form1
        Inherits Form


        <DllImport("USER32.dll")> _
        Private Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal msg As Integer, ByVal wParam As Integer, ByVal lParam As IntPtr) As Int32
        End Function
    <DllImport("gdi32.dll")> _
    Public Shared Function SaveDC(ByVal hdc As Integer) As Integer
    End Function

    <DllImport("gdi32.dll")> _
    Public Shared Function RestoreDC(ByVal hdc As Integer, ByVal nSavedDC As Integer) As Boolean
    End Function

    <DllImport("gdi32.dll")> _
    Public Shared Function SetMapMode(ByVal hdc As Integer, ByVal fnMapMode As Integer) As Integer
    End Function

    <DllImport("gdi32.dll")> _
    Public Shared Function SetWindowExtEx(ByVal hdc As Integer, ByVal nXExtent As Integer, ByVal nYExtent As Integer, ByRef size As Size) As Boolean
    End Function

    <DllImport("gdi32.dll")> _
    Public Shared Function SetViewportExtEx(ByVal hdc As Integer, ByVal nXExtent As Integer, ByVal nYExtent As Integer, ByRef size As Size) As Boolean
    End Function

        Private Const WM_USER As Integer = &H400

        Private Const EM_FORMATRANGE As Integer = WM_USER + 57


        <StructLayout(LayoutKind.Sequential)> _
        Private Structure RECT

            Public Left As Integer

            Public Top As Integer

            Public Right As Integer

            Public Bottom As Integer

        End Structure


        <StructLayout(LayoutKind.Sequential)> _
        Private Structure CHARRANGE

            Public cpMin As Integer

            Public cpMax As Integer

        End Structure


        <StructLayout(LayoutKind.Sequential)> _
        Private Structure FORMATRANGE

            Public hdc As IntPtr

            Public hdcTarget As IntPtr

            Public rc As RECT

            Public rcPage As RECT

            Public chrg As CHARRANGE

        End Structure

    Private Const inch As Double = 14.4
        Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox

        Private contentRectangle As Rectangle

        Public Sub New()


            InitializeComponent()
        End Sub

    Public Sub SaveAsJpegButton()
        tempImg = GetTempFileNameVB("img") + ".jpg"
        RichTextBox1.Height = RichTextBox1.Height * 3.125
        RichTextBox1.Width = RichTextBox1.Width * 3.125
        contentRectangle.Height = RichTextBox1.Height
        contentRectangle.Width = RichTextBox1.Width
        RtbToBitmap(RichTextBox1, contentRectangle, tempImg)
        tempImgLst = tempImgLst + "," + tempImg
    End Sub


    Public Sub RtbToBitmap(ByVal rtb As RichTextBox, ByVal rectangle As Rectangle, ByVal fileName As String)

        Dim j, r As Double
        j = 1.15
        r = 300
        Dim bmp As New Bitmap(CInt(rectangle.Width * j), CInt(rectangle.Height * j))
        bmp.SetResolution(31, 31)
        bmp.MakeTransparent()
        Using gr As Graphics = Graphics.FromImage(bmp)
            gr.FillRectangle(Brushes.White, 0, 0, CInt(rectangle.Width * j), CInt(rectangle.Height * j))
        End Using 
        Using gr As Graphics = Graphics.FromImage(bmp)
            Dim hDC As IntPtr = gr.GetHdc()
            Dim fmtRange As FORMATRANGE
            Dim rect As RECT
            Dim fromAPI As Integer
            Dim w, h As Integer

            w = CInt(bmp.Width * 1 / j)
            h = CInt(bmp.Height * 1 / j)

            rect.Top = 0
            rect.Left = 0
            rect.Bottom = CInt(Math.Truncate(h + (h * (bmp.HorizontalResolution / 100)) * inch))
            rect.Right = CInt(Math.Truncate(w + (w * (bmp.VerticalResolution / 100)) * inch))

            fmtRange.chrg.cpMin = 0
            fmtRange.chrg.cpMax = -1
            fmtRange.hdc = hDC
            fmtRange.hdcTarget = hDC
            fmtRange.rc = rect
            fmtRange.rcPage = rect

            Dim wParam As Integer = 1
            Dim lParam As IntPtr = Marshal.AllocCoTaskMem(Marshal.SizeOf(fmtRange))
            SetMapMode(fmtRange.hdc, 8)
            Dim s, s2 As New Size()
            SetWindowExtEx(fmtRange.hdc, w, h, s)
            SetViewportExtEx(fmtRange.hdc, w * r \ 96, h * r \ 96, s2)

            Marshal.StructureToPtr(fmtRange, lParam, False)
            fromAPI = SendMessage(rtb.Handle, EM_FORMATRANGE, wParam, lParam)
            Marshal.FreeCoTaskMem(lParam)

            fromAPI = SendMessage(rtb.Handle, EM_FORMATRANGE, wParam, New IntPtr(0))
            gr.ReleaseHdc(hDC)

        End Using
        Dim encoderParams As System.Drawing.Imaging.EncoderParameters = New System.Drawing.Imaging.EncoderParameters
        bmp.Save(fileName, System.Drawing.Imaging.ImageFormat.Jpeg)

        bmp.Dispose()

    End Sub

        Private Sub richTextBox1_ContentsResized(ByVal sender As Object, ByVal e As ContentsResizedEventArgs)

            contentRectangle = e.NewRectangle

        End Sub

        Private Sub InitializeComponent()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox
        Me.SuspendLayout()
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Location = New System.Drawing.Point(132, 125)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(100, 96)
        Me.RichTextBox1.TabIndex = 0
        Me.RichTextBox1.Text = ""
        '
        'Form1
        '
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.RichTextBox1)
        Me.Name = "Form1"
        Me.ResumeLayout(False)

    End Sub
    End Class

