Option Strict Off
Option Explicit On
Module CommunLib
	'***************************************************************
	'*                                                             *
	'* Contient les fonctions communes � tout les projets          *
	'*                                                             *
	'***************************************************************
	
	'-> Variables pour mots cl�s de path
	Public PathLecteur As String
	Public PathVersion As String
	Public PathIdent As String
	
	'-> Variables pour la cr�ation des Path d'analyse
	Public RessourcePath As String
	'-> Indique le path ou l'on peut trouver les maquettes du client et ses samples
	Public ClientPath As String
	'-> Variable pour la gestion des imprimantes
	Public SelPrint As Short 'Indique le choix de l'utilisteur : |' 0 -> Page en cours
	' 1 -> Page Born�e
	' 2 -> Fichier
    Public PageMin As Short
	Public PageMax As Short
	Public PrintMode As Short '-> Indique le choix de l'utilisateur
	'-> 1 Page en cours
	'-> 2 Page Min:Max
	'-> 3 Fichier
	'-> Gestion des s�parateurs d�cimaux
	Public SepDec As String
	Public SepMil As String
	Private Declare Function GetLocaleInfoVB Lib "kernel32"  Alias "GetLocaleInfoA"(ByVal Locale As Integer, ByVal LCType As Integer, ByVal lpLCData As String, ByVal cchData As Integer) As Integer
	
	'-> pour la base de registre
	Public Declare Function RegOpenKeyEx Lib "advapi32.dll"  Alias "RegOpenKeyExA"(ByVal hKey As Integer, ByVal lpSubKey As String, ByVal ulOptions As Integer, ByVal samDesired As Integer, ByRef phkResult As Integer) As Integer
    Private Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Integer, ByVal lpValueName As String, ByVal lpReserved As Integer, ByRef lpType As Integer, ByRef lpData As String, ByRef lpcbData As Integer) As Integer
	Public Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Integer) As Integer
	
	Public Function InitConvertion() As Object
		
		Dim lpBuffer As String
		Dim Res As Integer
		Dim LOCALE_STHOUSAND As Integer
		Dim LOCALE_USER_DEFAULT As Integer
		Dim LOCALE_SDECIMAL As Integer
		
		'-> S�parateur de millier : gestion des variables d'environnement
		LOCALE_STHOUSAND = &HFs
		LOCALE_USER_DEFAULT = &H400s
		LOCALE_SDECIMAL = 14
		
		lpBuffer = Space(10)
		Res = GetLocaleInfoVB(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, lpBuffer, Len(lpBuffer))
		SepDec = Mid(lpBuffer, 1, Res - 1)
		lpBuffer = Space(10)
		Res = GetLocaleInfoVB(LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, lpBuffer, Len(lpBuffer))
		SepMil = Mid(lpBuffer, 1, Res - 1)
		
        InitConvertion = True
	End Function
	
	Public Function Convert(ByVal StrToAnalyse As String) As String
		
		Dim i As Short
        Dim Tempo As String = ""
		Dim FindSep As Boolean
		
		For i = Len(StrToAnalyse) To 1 Step -1
			If Mid(StrToAnalyse, i, 1) = "." Then
				If Not FindSep Then
					Tempo = SepDec & Tempo
					FindSep = True
				End If
			ElseIf Mid(StrToAnalyse, i, 1) = "," Then 
				If Not FindSep Then
					Tempo = SepDec & Tempo
					FindSep = True
				End If
			Else
				Tempo = Mid(StrToAnalyse, i, 1) & Tempo
			End If
		Next  'Pour tous les caract�res � analyser
		
		Convert = Tempo
		
	End Function
	
	Public Sub GetPathMotCles()
		Dim i As Object
		
		'---> R�cup�ration des cl�s $LECTEUR$ , $VERSION$ , $IDENT$ en fonction du proptah
		
		'-> R�cup�ration de l'index de la racine
        i = GetEntryIndex(My.Application.Info.DirectoryPath, "DEALPRO", "\")
		
		'-> R�cup�ration du nom de la version
        PathVersion = Entry(i + 2, My.Application.Info.DirectoryPath, "\")
		
		If IsMouchard Then PrintLine(hdlMouchard, "PathVersion :" & PathVersion)
		
		'-> R�cup�ration du nom de lectuer logique
        i = InStr(1, UCase(My.Application.Info.DirectoryPath), "V6")
        If i <> 0 Then PathLecteur = Mid(My.Application.Info.DirectoryPath, 1, i - 1)
		
		If IsMouchard Then PrintLine(hdlMouchard, "PathLecteur :" & PathLecteur)
		
	End Sub
	
	Public Function ReplacePathMotCles(ByRef PathToReplace As String) As Object
		
		'-> V6 : on change le path du param.ini qui est dans le repertoire de l'executable DEALTOOL
		'PathToReplace = App.Path & "\param.ini"
        ReplacePathMotCles = False

		'-> Remplacer $LECTEUR$
		If Trim(PathLecteur) <> "" Then PathToReplace = Replace(UCase(PathToReplace), "$LECTEUR$", PathLecteur)
		
		'-> Remplacer $IDENT$
		If Trim(PathIdent) <> "" Then PathToReplace = Replace(UCase(PathToReplace), "$IDENT$", PathIdent)
		
		'-> Remplacer $VERSION$
		If Trim(PathVersion) <> "" Then PathToReplace = Replace(UCase(PathToReplace), "$VERSION$", PathVersion)
		
	End Function
	
	Public Function Entry(ByVal nEntrie As Short, ByVal Vecteur As String, ByVal Separateur As String) As String
		
		'---> Fonction qui retourne une entr�e particuli�re d'un vecteur
		
		Dim NbEntries As Integer
		Dim PosEnCour As Integer
		Dim i As Integer
		Dim CHarDeb As Integer
		Dim CharEnd As Integer
		
		'-> V�rifier que l'entr�e sp�cifi�e existe bien
        NbEntries = NumEntries(Vecteur, Separateur)
		If NbEntries = 1 Or nEntrie > NbEntries Then
			Entry = Vecteur
			Exit Function
		End If
		
		'-> Tester si on cherche la derni�re entr�e
		If nEntrie = NbEntries Then
			'Recherche du dernier s�parateur
			PosEnCour = InStrRev(Vecteur, Separateur)
			If PosEnCour + 1 > Len(Vecteur) Then
				Entry = ""
				Exit Function
			Else
				Entry = Mid(Vecteur, PosEnCour + 1, Len(Vecteur) - PosEnCour + 1)
				Exit Function
			End If
		End If
		
		'-> Recherche de l'entr�e
		NbEntries = 0
		PosEnCour = 1
		i = 0
		Do 
			i = InStr(PosEnCour, Vecteur, Separateur)
			NbEntries = NbEntries + 1
			If NbEntries = nEntrie Then
				'Lecture de la position de d�but
				CHarDeb = PosEnCour
				'Recherche du s�parateur suivant
				CharEnd = i
				If CHarDeb = CharEnd Then
					Entry = ""
				Else
					Entry = Mid(Vecteur, CHarDeb, CharEnd - CHarDeb)
				End If
				
				Exit Function
			End If
			PosEnCour = i + 1
		Loop 
	End Function
	
	Public Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Object
		
		'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�
		
		Dim NbEntries As Integer
		Dim i As Integer
		Dim PosAnalyse As Integer
		
		On Error GoTo GestError
		
		PosAnalyse = 1
		
		Do 
			i = InStr(PosAnalyse, Vecteur, Separateur)
			If i <> 0 Then
				NbEntries = NbEntries + 1
				PosAnalyse = i + 1
			Else
				Exit Do
			End If
		Loop  'Analyse du vecteur
		
		'-> Renvoyer le nombre d'entr�es
        NumEntries = NbEntries + 1
		
		Exit Function
		
GestError: 
		
		'-> Dans ce cas la, renvoyer 1
        NumEntries = 1
				
	End Function
	
	Public Sub AddItemToList(ByVal Vecteur As String, ByVal aList As System.Windows.Forms.ListBox, Optional ByVal Selected As Short = 0)
		
		'---> Cette fonction  alimente une liste avec toutes les entr�es de vecteur _
		''S�parateur "|"
		
		Dim i As Short
		Dim NbEntries As Short
		
		aList.Items.Clear()
		'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		NbEntries = NumEntries(Vecteur, "|")
		For i = 1 To NbEntries
			aList.Items.Add(Entry(i, Vecteur, "|"))
		Next 
		
		'-> S�lectionner l'entr�e sp�cifi�e
		If Selected <> 0 Then aList.SetSelected(Selected - 1, True)
		
		
	End Sub
	
	Public Function AddEntryToMatrice(ByVal Matrice As String, ByVal SepMatrice As String, ByVal EntryToAdd As String) As String
		
		'---> cette fonction rajoute une entr�e dans une matrice. Elle renvoie la matrice mise � jour
		
		'-> Ajouter l'entre�e dans la matrice
		If Trim(Matrice) = "" Then
			Matrice = EntryToAdd
		Else
			Matrice = Matrice & SepMatrice & EntryToAdd
		End If
		
		AddEntryToMatrice = Matrice
		
	End Function
	
	Public Function DeleteEntry(ByVal Matrice As String, ByVal EntryToDel As Short, ByVal SepMatrice As String) As String
		
		'---> Cette fonction supprime une entr�e dans la matrice. Elle renvoie la matrice mise � jour
		Dim NbEntries As Short
		Dim i As Short
		Dim j As Short
		Dim CHarDeb As Short
		Dim CharEnd As Short
		
		'-> R�cup�rer le nombre d'entr�e
		'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet NumEntries(). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		NbEntries = NumEntries(Matrice, SepMatrice)
		
		'-> Renvoyer un chaine vide s'il n'y a qu'une seule entr�e
		If NbEntries = 1 Then
			'Sil n'y a qu'une seule entr�e, vider la matrice
			DeleteEntry = ""
			Exit Function
		ElseIf EntryToDel > NbEntries Then 
			'Si on doit supprimer une entr�e qui n'existe pas -> renvoyer la matrice d'origine
			DeleteEntry = Matrice
			Exit Function
		ElseIf EntryToDel = NbEntries Then 
			'Si on doit supprimer la derni�re entr�e, chercher le s�parateur n-1
			i = InStrRev(Matrice, SepMatrice)
			DeleteEntry = Mid(Matrice, 1, i - 1)
			Exit Function
		ElseIf EntryToDel = 1 Then 
			'-> Si on doit supprimer la premi�re entr�e, chercher le premier s�parateur
			i = InStr(1, Matrice, SepMatrice)
			DeleteEntry = Mid(Matrice, i + 1, Len(Matrice) - i)
			Exit Function
		Else
			'-> Suppression d'une entr�e
			j = 1
			i = 0
			NbEntries = 0
			Do 
				i = InStr(j, Matrice, SepMatrice)
				NbEntries = NbEntries + 1
				If NbEntries = EntryToDel - 1 Then
					'Lecture de la position de d�but
					CHarDeb = i
					CharEnd = InStr(i + 1, Matrice, SepMatrice)
					DeleteEntry = Mid(Matrice, 1, CHarDeb - 1) & Mid(Matrice, CharEnd, Len(Matrice) - CharEnd + 1)
					Exit Function
				End If
				j = i + 1
			Loop 
		End If
		
		
	End Function
	
	
	Public Function GetEntryIndex(ByVal Matrice As String, ByVal EntryToSearch As String, ByVal SepMatrice As String) As Short
		
		'---> Cette fonction retourne l'index d'une valeur dans une matrice. _
		'Attention : Cette fonction retourne la premi�re it�ration trouv�e
		
		On Error Resume Next
		
		Dim i, j As Short
		Dim NbEntries As Short
		
		'-> Recherche de la pchaine de caract�re
		i = InStr(1, UCase(Matrice), UCase(EntryToSearch))
		j = 1
		NbEntries = 1
		'-> Analyse
		Do While j < i
			j = InStr(j, Matrice, SepMatrice)
			NbEntries = NbEntries + 1
			j = j + 1
		Loop 
		
		GetEntryIndex = NbEntries
		
		
	End Function
	
	Public Function LimitSaisie(ByVal KeyAscii As Short) As Boolean
		
		'---> Fonction qui limitte la saisie � des nombres entiers
		
		Select Case KeyAscii
			Case 8, 48 To 57, 127
				LimitSaisie = True
				
			Case Else
				LimitSaisie = False
		End Select
		
	End Function
	
	Public Function IsLegalName(ByVal NewName As String, Optional ByRef Tiret As Boolean = False) As Boolean
		
		'---> Fonction qui v�rifie le contenu d'un nom pour y supprimer tous les caract�res interdits
		' Caract�res interdits : \ / : * " < > | -
		
		Dim FindBad As Boolean
		
		
		If InStr(1, NewName, "\") <> 0 Then FindBad = True
		If InStr(1, NewName, "/") <> 0 Then FindBad = True
		If InStr(1, NewName, ":") <> 0 Then FindBad = True
		If InStr(1, NewName, "*") <> 0 Then FindBad = True
		If InStr(1, NewName, """") <> 0 Then FindBad = True
		If InStr(1, NewName, "<") <> 0 Then FindBad = True
		If InStr(1, NewName, ">") <> 0 Then FindBad = True
		If InStr(1, NewName, "|") <> 0 Then FindBad = True
		If Tiret And InStr(1, NewName, "-") <> 0 Then FindBad = True
		
		IsLegalName = Not FindBad
		
		
	End Function
	
	Public Function FormatNum(ByVal ToFormat As String, ByVal Msk As String) As String
		
		'---> Cette fonction a pour but d'analyser une chaine de caract�re et de la retouner
		'formatt�e en s�parateur de milier et en nombre de d�cimal
		
        Dim strFormat As String = ""
        Dim strTempo As String = ""
        Dim strPartieEntiere As String = ""
        Dim StrPartieDecimale As String = ""
		Dim i, j As Short
		Dim nbDec As Short
        Dim strSep As String = ""
		Dim idNegatif As Short
        Dim Masque As String = ""
        Dim Tempo As String = ""
		
		'-> Modifier le contenu de la chaine pour le formatter
		For i = 1 To Len(ToFormat)
			If Not IsNumeric(Mid(ToFormat, i, 1)) And (Mid(ToFormat, i, 1)) <> SepDec Then
			Else
				Tempo = Tempo & (Mid(ToFormat, i, 1))
			End If
		Next 
		
		ToFormat = Tempo
		
		'-> Tester si on envoie une zone num�rique
		If Not IsNumeric(ToFormat) Then
			FormatNum = ""
			Exit Function
		End If
		'-> Charger le masque de la cellule
		nbDec = CShort(Entry(1, Msk, "�"))
		strSep = Entry(2, Msk, "�")
		idNegatif = CShort(Entry(3, Msk, "�"))
		
		'-> Analyse si d�cimale
		If nbDec = 0 Then
			'-> Arrondir
			strTempo = CStr(CInt(ToFormat))
			Masque = "#########################################0"
		Else
			Masque = "#########################################0." & New String("0", nbDec)
			strTempo = ToFormat
		End If
		
		'-> Construction d'un masque assez grand pour formatter n'importe qu'elle zone
		strTempo = VB6.Format(System.Math.Abs(CDbl(strTempo)), Masque)
		
		'-> Construction de la partie enti�re
		If nbDec <> 0 Then
			strPartieEntiere = Mid(strTempo, 1, InStr(1, strTempo, SepDec) - 1)
			StrPartieDecimale = SepDec & Mid(strTempo, InStr(strTempo, SepDec) + 1, nbDec)
		Else
			strPartieEntiere = strTempo
			StrPartieDecimale = ""
		End If
		
		j = 1
		For i = Len(strPartieEntiere) To 1 Step -1
			strFormat = Mid(strTempo, i, 1) & strFormat
			If j = 3 And i <> 1 Then
				strFormat = strSep & strFormat
				j = 1
			Else
				j = j + 1
			End If
		Next 
		
		FormatNum = strFormat & StrPartieDecimale
		
	End Function
	
	Public Function GetPath(ByVal InitPath As String) As String
		Dim BasePath As Object
		Dim pos As Object
		
		'---> Cette fonction renvois le path de \MQT en fonction de l'endroit d'ou _
		'est appell�e le programme ( utilis� pour faire la diff�rence en DVL et EXE)
		
		'-> La diff�rence se fait par : Si on trouve le fichier VBP : on est en DVL
		
		'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		If Dir(InitPath & "\*.vbp") = "" Then
			'-> On est en mode exe
            pos = InStrRev(InitPath, "\")
            BasePath = Mid(InitPath, 1, pos)
            RessourcePath = BasePath & "Mqt\"
		Else
			'-> On est en mode interpr�t�
			RessourcePath = InitStringPath(0) & "\"
		End If
		
		GetPath = RessourcePath
		
	End Function
	
	Public Function GetTempFileNameVB(ByVal Id As String, Optional ByRef Rep As Boolean = False) As String
		
		Dim TempPath As String
		Dim lpBuffer As String
		Dim Result As Integer
		Dim TempFileName As String
		
		'---> Fonction qui d�termine un nom de fichier tempo sous windows
		
		'-> Recherche du r�pertoire temporaire
		lpBuffer = Space(500)
		Result = GetTempPath(Len(lpBuffer), lpBuffer)
		TempPath = Mid(lpBuffer, 1, Result)
		
		'-> Si on ne demande que le r�pertoire de windows
		If Rep Then
			GetTempFileNameVB = TempPath
			Exit Function
		End If
		
		'-> Cr�ation d'un nom de fichier
		TempFileName = Space(1000)
		Result = GetTempFileName(TempPath, Id, 0, TempFileName)
		TempFileName = Entry(1, TempFileName, Chr(0))
		
		GetTempFileNameVB = TempFileName
		
	End Function
	
	Public Sub CreatePath(ByRef IniPath As String)
		
		Dim i, j As Short
		
		'-> Construire le propath � partir de l'emplacement actuel "M:\DealPro\Deal\V51\EmilieGUI\Exe" Chez DEAL
		IniPath = My.Application.Info.DirectoryPath
		For j = 1 To 2
			'-> R�cup�ration du dernier \
			i = InStrRev(IniPath, "\")
			IniPath = Mid(IniPath, 1, i - 1)
		Next 
		If Mid(IniPath, Len(IniPath), 1) <> "\" Then IniPath = IniPath & "\"
		IniPath = IniPath & "Turbo"
		
	End Sub
	
	
	
	Public Function GetAppNameByIndex(ByVal IndexApp As Short) As String

        GetAppNameByIndex = ""
		Select Case IndexApp
			
			Case 1
				GetAppNameByIndex = "Editeur de maquettes graphiques"
				
			Case 2
				GetAppNameByIndex = "Moteur d'�ditions graphiques"
				
			Case 3
				GetAppNameByIndex = "Boite � outils Deal Informatique"
				
		End Select
		
	End Function
    
    Public Function GetIniString(ByRef AppName As String, ByRef lpKeyName As String, ByRef IniFile As String, ByRef Error_Renamed As Boolean) As String

        Dim Res As Integer
        Dim lpBuffer As String


        lpBuffer = Space(2000)
        Res = GetPrivateProfileString(AppName, lpKeyName, "NULL", lpBuffer, Len(lpBuffer), IniFile)
        lpBuffer = Entry(1, lpBuffer, Chr(0))
        If lpBuffer = "NULL" Then
            If Error_Renamed Then
                MsgBox("Impossible de trouver la cl� : " & lpKeyName & " dans la section : " & AppName & " du fichier : " & IniFile, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Erreur de lecture")
            End If
            GetIniString = ""
        Else
            lpBuffer = Mid(lpBuffer, 1, Res)
            GetIniString = lpBuffer
        End If


    End Function
	
	
	Public Function InitStringPath(ByRef IsTurbo As Short) As String
		
		'---> Cette proc�dure retourne un le path de MQT  si isturbo = 0 _
		'ou le path du r�pertoire turbo = 1
		
		Dim TempPath As String
        Dim i As Short
		
		'-> R�cup�rer le path en cours
		TempPath = My.Application.Info.DirectoryPath
		
		If IsTurbo = 0 Then
			'-> Recherche du path MQT
			If InStr(1, UCase(My.Application.Info.DirectoryPath), "EMILIEGUI\SRC") <> 0 Then
				i = InStr(1, UCase(My.Application.Info.DirectoryPath), "EMILIEGUI\SRC")
				TempPath = Mid(TempPath, 1, i - 1) & "EMILIEGUI\MQT"
				InitStringPath = TempPath
			Else
				TempPath = Replace(UCase(My.Application.Info.DirectoryPath), "EMILIEGUI\EXE", "EMILIEGUI\MQT")
				InitStringPath = TempPath & "\"
			End If
			RessourcePath = TempPath
		Else
			'-> Recherche du path TURBO
			i = InStr(1, UCase(TempPath), "DEALPRO\")
			TempPath = Mid(TempPath, 1, i - 1) & "TURBO"
			InitStringPath = TempPath
			
			Exit Function	

		End If
				
	End Function
	
End Module