Imports Microsoft.VisualBasic.CompilerServices
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices
'Imports System.IO.Compression
Imports System.IO
'Imports System.Collections.Specialized
Imports System.Text
Imports System.Security.Cryptography
Imports System.Security.Cryptography.X509Certificates
'Imports System.Convert

Public Class vbPDF
    ' Methods
    Public Sub New()
        Me.mvarRC4Code = New Byte(256 - 1) {}
        Me.mvarFileNumber = 0
        Me.mvarSpecOptions = pdfSpecOptions.pdfAcrobat40
        Me.mvarEncoding = pdfEncoding.pdfWinAnsiEncoding
        Me.mvarCompression = pdfCompression.pdfNothing
        Me.mvarViewerPreference = pdfViewerPreference.pdfSinglePage
        Me.mvarDocTitle = "Spool d'�dition Deal Informatique"
        Me.mvarDocCreationDate = Me.prFormat("#d", New Object() {Now()})
        Me.mvarDocAuthor = "Deal Informatique"
        Me.mvarDocCreator = ".NET"
        Me.mvarDocProducer = "Turbopdf - " & Application.ProductVersion & ChrW(169) & " DMZ 2024"
        Me.mvarFileID = Strings.Format(DateAndTime.Now, "yyyyMMddhhmmss")
        'Me.mvarFileID = Strings.Format(DateAndTime.Now, "00000000000000")
        Me.mvarDocKeywords = "Spool d'impression"
        Me.SetScale(pdfScaleMode.pdf72PxInch, 1.0!)
        Me.SetPaperSize(pdfPaperSize.pdfA4, 0.0!, 0.0!)
        Me.mvarOrientation = pdfPageOrientation.pdfPortrait
        Me.mvarMarginL = 0.0!
        Me.mvarMarginR = 0.0!
        Me.mvarMarginT = 0.0!
        Me.mvarMarginB = 0.0!
        Me.mvarClipPage = True
        Me.mvarTotPages = 0
        Me.mvarFontCount = 0
        Me.mvarColorText = "#000000"
        Me.mvarColorFill = "#FFFFFF"
        Me.mvarColorLine = "#000000"
        Me.AddBookmark(Me.mvarDocTitle, 0, pdfBookmarkType.pdfFitPage, 0.0!, 0.0!, 0.0!, 0.0!, 0.0!)
    End Sub

    Public Sub AddBookmark(ByVal Caption As String, Optional ByVal Level As Integer = 0, Optional ByVal TypeOfBookmark As pdfBookmarkType = 0, Optional ByVal X As Single = 0.0!, Optional ByVal Y As Single = 0.0!, Optional ByVal Width As Single = 0.0!, Optional ByVal Height As Single = 0.0!, Optional ByVal Zoom As Single = 0.0!, Optional ByVal iFormat As Integer = 0, Optional ByVal strColor As String = "", Optional ByVal WebLink As String = "")
        X = SingleType.FromObject(Interaction.IIf((X = 0.0!), Me.mvarCurrX, (Me.mvarKUnit * X)))
        Y = SingleType.FromObject(Interaction.IIf((Y = 0.0!), Me.mvarCurrY, (Me.mvarPageHeight - (Me.mvarKUnit * Y))))
        Me.mvarBookmarkCount += 1
        Me.arrBOOKMARK = DirectCast(Utils.CopyArray(DirectCast(Me.arrBOOKMARK, Array), New BookmarkDescriptor((Me.mvarBookmarkCount + 1) - 1) {}), BookmarkDescriptor())
        Dim num1 As Integer = Me.mvarBookmarkCount
        '-> on determine le niveau du bookmark
        If (Me.mvarBookmarkCount > 1) Then
            If (Level = -1) Then
                Level = (Me.arrBOOKMARK((Me.mvarBookmarkCount - 1)).Level + 1)
            ElseIf (Level = 0) Then
                Level = Me.arrBOOKMARK((Me.mvarBookmarkCount - 1)).Level
            ElseIf (Level > (Me.arrBOOKMARK((Me.mvarBookmarkCount - 1)).Level + 1)) Then
                Level = (Me.arrBOOKMARK((Me.mvarBookmarkCount - 1)).Level + 1)
            End If
        Else
            Level = 0
        End If
        '-> on applique l'eventuelle mise en forme
        Me.arrBOOKMARK(num1).Format = iFormat
        '-> on applique l'eventuelle couleur
        Me.arrBOOKMARK(num1).color = strColor
        '-> on regarde si on est sur un lien web
        Me.arrBOOKMARK(num1).webLink = WebLink
        '-> on regarde comment on doit ouvrir le bookmark
        If (Level < 255) Then
            Me.arrBOOKMARK(num1).Text = Caption
            Me.arrBOOKMARK(num1).Level = Level
            Select Case TypeOfBookmark
                Case pdfBookmarkType.pdfFitPage
                    Me.arrBOOKMARK(num1).Destination = Me.prFormat("[#i 0 R /FitB]", New Object() {Me.mvarCurrentPageObj})
                    Return
                Case pdfBookmarkType.pdfFitVertical
                    Me.arrBOOKMARK(num1).Destination = Me.prFormat("[#i 0 R /FitV #s]", New Object() {Me.mvarCurrentPageObj, X})
                    Return
                Case pdfBookmarkType.pdfFitHorizontal
                    Me.arrBOOKMARK(num1).Destination = Me.prFormat("[#i 0 R /FitH #s]", New Object() {Me.mvarCurrentPageObj, Y})
                    Return
                Case pdfBookmarkType.pdfFitRect
                    Me.arrBOOKMARK(num1).Destination = Me.prFormat("[#i 0 R /FitR #s #s #s #s]", New Object() {Me.mvarCurrentPageObj, X, Y, (X + (Me.mvarKUnit * Width)), (Y - (Me.mvarKUnit * Height))})
                    Return
                Case pdfBookmarkType.pdfXYZoom
                    Me.arrBOOKMARK(num1).Destination = Me.prFormat("[#i 0 R /XYZ #s #s #s]", New Object() {Me.mvarCurrentPageObj, X, Y, (Zoom / 100.0!)})
                    Return
            End Select
        End If

    End Sub

    Public Function AddFont(ByVal TTFFile As String, Optional ByVal FontAlias As String = "", Optional ByVal Embedded As Boolean = True) As String
        Dim i As Integer
        AddFont = ""
        'Trace("addfont" + TTFFile)
        On Error Resume Next
        '-> on verifie que l'on a bien qque chose 
        If TTFFile = "" Then Exit Function
        '-> on verifie si on a pas deja ajout� la font
        If Not Me.arrFONT Is Nothing Then
            For i = 1 To Me.arrFONT.Length - 1
                If Me.arrFONT(i).FontFile = TTFFile Then Return Me.arrFONT(i).FontAlias
            Next
        End If
        Dim flag2 As Boolean
        Dim num18 As Integer
        Dim num20 As Long
        Dim num21 As Long
        Dim num22 As Long
        Dim num23 As Long
        Dim num24 As Long
        Dim num26 As Long
        Dim num27 As Long
        Dim num28 As Long
        Dim num29 As Long
        Dim num31 As Long
        Dim text3 As New String(" "c, 4)
        Dim numArray1 As Integer() = New Integer(256 - 1) {}
        Dim numArray2 As Integer() = New Integer(256 - 1) {}
        Dim num2 As Integer = FileSystem.FreeFile
        FileSystem.FileOpen(num2, TTFFile, 32, OpenAccess.Read, OpenShare.Shared, -1)
        Dim num43 As Integer = IntegerType.FromObject(prGetValue(num2, 4, 2, True))
        Dim num3 As Integer = 1
        Do While (num3 <= num43)
            FileSystem.Seek(num2, CLng((13 + ((num3 - 1) * 16))))
            FileSystem.FileGet(num2, text3, -1, False)
            Dim num25 As Long = LongType.FromObject(Me.prGetValue(num2, CLng((20 + ((num3 - 1) * 16))), 4, False))
            Dim text4 As String = Strings.Trim(text3)
            If (StringType.StrCmp(text4, "OS/2", False) = 0) Then
                num28 = num25
            ElseIf (StringType.StrCmp(text4, "head", False) = 0) Then
                num22 = num25
            ElseIf (StringType.StrCmp(text4, "hhea", False) = 0) Then
                num23 = num25
            ElseIf (StringType.StrCmp(text4, "hmtx", False) = 0) Then
                num24 = num25
            ElseIf (StringType.StrCmp(text4, "glyf", False) = 0) Then
                num21 = num25
            ElseIf (StringType.StrCmp(text4, "loca", False) = 0) Then
                num26 = num25
            ElseIf (StringType.StrCmp(text4, "post", False) = 0) Then
                num29 = num25
            ElseIf (StringType.StrCmp(text4, "name", False) = 0) Then
                num27 = num25
            ElseIf (StringType.StrCmp(text4, "cmap", False) = 0) Then
                num20 = num25
            End If
            num3 += 1
        Loop
        Dim num15 As Integer = IntegerType.FromObject(Me.prGetValue(num2, (num22 + 18), 2, True))
        Dim single2 As Single = CSng((985 / CDbl(num15))) 'on avait 1000 a l'origine
        Dim num5 As Integer = IntegerType.FromObject(LateBinding.LateGet(Nothing, GetType(Math), "Round", New Object() {ObjectType.MulObj(Me.prGetValue(num2, (num28 + 2), 2, False), single2)}, Nothing, Nothing))
        Dim num4 As Integer = IntegerType.FromObject(LateBinding.LateGet(Nothing, GetType(Math), "Round", New Object() {ObjectType.MulObj(Me.prGetValue(num2, (num28 + 74), 2, True), single2)}, Nothing, Nothing))
        Dim num7 As Integer = IntegerType.FromObject(LateBinding.LateGet(Nothing, GetType(Math), "Round", New Object() {ObjectType.MulObj(ObjectType.NegObj(Me.prGetValue(num2, (num28 + 76), 2, True)), single2)}, Nothing, Nothing))
        Dim num16 As Integer = IntegerType.FromObject(Me.prGetValue(num2, (num28 + 4), 2, True))
        Dim num14 As Integer = IntegerType.FromObject(Me.prGetValue(num2, (num28 + 62), 2, True))
        Dim flag3 As Boolean = (ObjectType.ObjTst(Me.prGetValue(num2, (num22 + 50), 2, False), 0, False) = 0)
        Dim num10 As Integer = IntegerType.FromObject(LateBinding.LateGet(Nothing, GetType(Math), "Round", New Object() {ObjectType.MulObj(Me.prGetValue(num2, (num23 + 10), 2, True), single2)}, Nothing, Nothing))
        Dim num11 As Integer = IntegerType.FromObject(Me.prGetValue(num2, num24, 2, True))
        Dim single1 As Single = SingleType.FromObject(LateBinding.LateGet(Nothing, GetType(Math), "Round", New Object() {ObjectType.AddObj(Me.prGetValue(num2, (num29 + 4), 2, False), ObjectType.DivObj(Me.prGetValue(num2, (num29 + 6), 2, False), 65535)), 2}, Nothing, Nothing))
        Dim flag1 As Boolean = (ObjectType.ObjTst(Me.prGetValue(num2, (num29 + 12), 4, True), 0, False) <> 0)
        Dim num42 As Integer = IntegerType.FromObject(ObjectType.SubObj(Me.prGetValue(num2, (num20 + 2), 2, True), 1))
        Dim num17 As Integer = 0
        Do While (num17 <= num42)
            If (ObjectType.ObjTst(Me.prGetValue(num2, ((num20 + 4) + (8 * num17)), 2, True), 3, False) = 0) Then
                flag2 = (ObjectType.ObjTst(Me.prGetValue(num2, ((num20 + 6) + (8 * num17)), 2, True), 0, False) = 0)
                Dim num19 As Long = LongType.FromObject(Me.prGetValue(num2, ((num20 + 8) + (8 * num17)), 4, True))
                If (ObjectType.ObjTst(Me.prGetValue(num2, (num20 + num19), 2, True), 4, False) = 0) Then
                    Dim num9 As Integer = IntegerType.FromObject(ObjectType.DivObj(Me.prGetValue(num2, ((num20 + num19) + 6), 2, True), 2))
                    Dim num41 As Integer = num9
                    num18 = 1
                    Do While (num18 <= num41)
                        Dim num8 As Integer = IntegerType.FromObject(ObjectType.BitAndObj(Me.prGetValue(num2, (((num20 + num19) + 14) + ((num18 - 1) * 2)), 2, True), 4095))
                        Dim num13 As Integer = IntegerType.FromObject(ObjectType.BitAndObj(Me.prGetValue(num2, (((num20 + num19) + 16) + (((num9 + num18) - 1) * 2)), 2, True), 4095))
                        Dim num6 As Integer = IntegerType.FromObject(Me.prGetValue(num2, ((((num20 + num19) + 16) + (num9 * 4)) + ((num18 - 1) * 2)), 2, False))
                        num31 = (((num19 + 16) + (num9 * 6)) + ((num18 - 1) * 2))
                        Dim num12 As Integer = IntegerType.FromObject(Me.prGetValue(num2, (num20 + num31), 2, False))
                        If (num8 >= 255) Then
                            num8 = 255
                        End If
                        Dim num40 As Integer = num8
                        num3 = num13
                        Do While (num3 <= num40)
                            If (num12 = 0) Then
                                numArray1(num3) = ((num6 + num3) Mod 65536)
                            Else
                                numArray1(num3) = IntegerType.FromObject(Me.prGetValue(num2, ((num20 + num31) + (((num3 - num13) * 2) + num12)), 2, False))
                            End If
                            num3 += 1
                        Loop
                        If (num8 >= 255) Then
                            Exit Do
                        End If
                        num18 += 1
                    Loop
                    Exit Do
                End If
            End If
            num17 += 1
        Loop
        num18 = 0
        Do While True
            Dim num30 As Long
            Dim num33 As Integer
            Dim num34 As Integer
            Dim num35 As Integer
            Dim num36 As Integer
            numArray2(num18) = IntegerType.FromObject(LateBinding.LateGet(Nothing, GetType(Math), "Round", New Object() {ObjectType.MulObj(Me.prGetValue(num2, (num24 + (numArray1(num18) * 4)), 2, True), single2)}, Nothing, Nothing))
            If flag3 Then
                num30 = LongType.FromObject(ObjectType.MulObj(2, Me.prGetValue(num2, (num26 + (numArray1(num18) * 2)), 2, True)))
            Else
                num30 = LongType.FromObject(Me.prGetValue(num2, (num26 + (numArray1(num18) * 4)), 4, True))
            End If
            If (ObjectType.ObjTst(Me.prGetValue(num2, (num21 + num30), 2, False), 0, False) >= 0) Then
                Dim num32 As Long = LongType.FromObject(Me.prGetValue(num2, ((num21 + num30) + 2), 2, False))
                If (num32 < num34) Then
                    num34 = CInt(num32)
                End If
                num32 = LongType.FromObject(Me.prGetValue(num2, ((num21 + num30) + 4), 2, False))
                If (num32 < num36) Then
                    num36 = CInt(num32)
                End If
                num32 = LongType.FromObject(Me.prGetValue(num2, ((num21 + num30) + 6), 2, False))
                If (num32 > num33) Then
                    num33 = CInt(num32)
                End If
                num32 = LongType.FromObject(Me.prGetValue(num2, ((num21 + num30) + 8), 2, False))
                If (num32 > num35) Then
                    num35 = CInt(num32)
                End If
            End If
            num18 += 1
            If (num18 > 255) Then
                Dim text2 As String
                num34 = CInt(Math.Round(Math.Round(CDbl((num34 * single2)))))
                num36 = CInt(Math.Round(Math.Round(CDbl((num36 * single2)))))
                num33 = CInt(Math.Round(Math.Round(CDbl((num33 * single2)))))
                num35 = CInt(Math.Round(Math.Round(CDbl((num35 * single2)))))
                Dim num39 As Integer = IntegerType.FromObject(ObjectType.SubObj(Me.prGetValue(num2, (num27 + 2), 2, True), 1))
                num17 = 0
                Do While (num17 <= num39)
                    If BooleanType.FromObject(ObjectType.BitAndObj(ObjectType.BitAndObj((ObjectType.ObjTst(Me.prGetValue(num2, ((num27 + 6) + (12 * num17)), 2, True), 3, False) = 0), (ObjectType.ObjTst(Me.prGetValue(num2, ((num27 + 8) + (12 * num17)), 2, True), Interaction.IIf(flag2, 0, 1), False) = 0)), (ObjectType.ObjTst(Me.prGetValue(num2, ((num27 + 12) + (12 * num17)), 2, True), 1, False) = 0))) Then
                        num31 = LongType.FromObject(Me.prGetValue(num2, (num27 + 4), 2, True))
                        Dim num38 As Integer = IntegerType.FromObject(ObjectType.SubObj(Me.prGetValue(num2, ((num27 + 14) + (12 * num17)), 2, True), 1))
                        num18 = 1
                        Do While (num18 <= num38)
                            Dim num1 As Byte
                            FileSystem.Seek(num2, LongType.FromObject(ObjectType.AddObj(ObjectType.AddObj(((num27 + 1) + num31), Me.prGetValue(num2, ((num27 + 16) + (12 * num17)), 2, True)), num18)))
                            FileSystem.FileGet(num2, num1, -1)
                            text2 = (text2 & Strings.Trim(StringType.FromChar(Strings.Chr(num1))))
                            num18 = (num18 + 2)
                        Loop
                    End If
                    num17 += 1
                Loop
                num31 = FileSystem.LOF(num2)
                If (num2 > 0) Then
                    FileSystem.FileClose(New Integer() {num2})
                End If
                Me.mvarFontCount += 1
                Me.arrFONT = DirectCast(Utils.CopyArray(DirectCast(Me.arrFONT, Array), New FontDescriptor((Me.mvarFontCount + 1) - 1) {}), FontDescriptor())
                If (StringType.StrCmp(FontAlias, "", False) = 0) Then
                    FontAlias = text2
                End If
                Dim num37 As Integer = Me.mvarFontCount
                Me.arrFONT(num37).Initialize()
                Me.arrFONT(num37).FontAlias = Strings.UCase(FontAlias)
                Me.arrFONT(num37).TrueTypeFont = True
                Me.arrFONT(num37).Embedded = Embedded
                Me.arrFONT(num37).BaseFont = text2
                Me.arrFONT(num37).Param = Me.prFormat("/Flags #i /FontBBox [#i #i #i #i] /MissingWidth #i /StemV #i /ItalicAngle #s /CapHeight #i /XHeight #i /Ascent #i /Descent #i /MaxWidth #i /AvgWidth #i", New Object() {ObjectType.AddObj(ObjectType.AddObj(Interaction.IIf((single1 <> 0.0!), 64, 0), Interaction.IIf(flag1, 1, 0)), Interaction.IIf(flag2, 4, 32)), num34, num36, num33, num35, num11, RuntimeHelpers.GetObjectValue(Interaction.IIf((num16 <= 500), 70, 120)), single1, num4, Conversion.Int((CDbl(num4) / 2)), num4, num7, num10, num5})
                Me.arrFONT(num37).Height = (num4 + Math.Abs(num7))
                Me.arrFONT(num37).BaseLine = num7
                num3 = 0
                Do
                    Me.arrFONT(num37).Widths(num3) = numArray2(num3)
                    num3 += 1
                Loop While (num3 <= 255)
                If FontAlias = "Code128" Then
                    num3 = 0
                    Do
                        Me.arrFONT(num37).Widths(num3) = 265
                        num3 += 1
                    Loop While (num3 <= 255)
                    'Me.arrFONT(num37).Widths(32) = 564
                    Me.arrFONT(num37).Widths(210) = 504
                End If
                Me.arrFONT(num37).Widths(128) = CInt(Me.arrFONT(num37).Widths(128) * 1.9)
                Me.arrFONT(num37).FontFile = TTFFile
                Me.arrFONT(num37).Length1 = num31
                Trace("addfontfin")
                Return FontAlias
            End If
        Loop
    End Function

    Public Sub AddGradient(ByVal AliasName As String, ByVal Style As pdfShadingStyle, ByVal StartColor As String, ByVal EndColor As String, ByVal XStart As Single, ByVal YStart As Single, ByVal XEnd As Single, ByVal YEnd As Single, Optional ByVal RStart As Single = 0.0!, Optional ByVal REnd As Single = 360.0!, Optional ByVal ExtendBefore As Boolean = False, Optional ByVal ExtendAfter As Boolean = False)
        Dim text1 As String
        Me.mvarShadingCount += 1
        Me.arrSHADING = DirectCast(Utils.CopyArray(DirectCast(Me.arrSHADING, Array), New ShadingDescriptor((Me.mvarShadingCount + 1) - 1) {}), ShadingDescriptor())
        Me.prParseColor(StartColor, text1)
        Me.prParseColor(EndColor, text1)
        Dim text2 As String = Me.prFormat("<</Type /Pattern /PatternType 2 /Matrix [ 1 0 0 -1 0 #s] /Shading <<#n/ColorSpace /DeviceRGB#n", New Object() {Me.mvarPageHeight})
        If (Style = pdfShadingStyle.pdfRadial) Then
            text2 = (text2 & "/ShadingType 3" & ChrW(13))
            text2 = (text2 & Me.prFormat("/Coords [#k #k #k #k #k #k]", New Object() {XStart, YStart, RStart, XEnd, YEnd, REnd}) & ChrW(13))
        Else
            text2 = (text2 & "/ShadingType 2" & ChrW(13))
            text2 = (text2 & Me.prFormat("/Coords [#k #k #k #k]", New Object() {XStart, YStart, XEnd, YEnd}) & ChrW(13))
        End If
        text2 = (text2 & Me.prFormat("/Extend [#b #b]", New Object() {ExtendBefore, ExtendAfter}) & " ")
        text2 = (text2 & Me.prFormat("/Function #n<< /FunctionType 3 /Domain [0.000 1.000] /Bounds [] /Encode [0 1 ] ", New Object(0 - 1) {}))
        text2 = (text2 & Me.prFormat("/Functions [#n<< /FunctionType 2 /Domain [0.000 1.000] /C0 [#C] /C1 [#C] /N 1 >>]#n>>#n>>", New Object() {StartColor, EndColor}))
        text2 = (text2 & ">>")
        Dim num1 As Integer = Me.mvarShadingCount
        Me.arrSHADING(num1).Name = Strings.UCase(Strings.Trim(AliasName))
        Me.arrSHADING(num1).Param = text2
        Me.arrSHADING(num1).SubType = "G"
    End Sub

    Public Sub AddNoteBox(ByVal [Text] As String, Optional ByVal X As Single = 0.0!, Optional ByVal Y As Single = 0.0!, Optional ByVal Width As Single = 0.0!, Optional ByVal Height As Single = 0.0!, Optional ByVal TextAlign As pdfTextAlignment = 1, Optional ByVal Border As pdfNoteBorder = 0, Optional ByVal BorderWidth As Integer = 1, Optional ByVal DashOn As Single = 1.0!, Optional ByVal DashOff As Single = 1.0!, Optional ByVal NoteAuthor As String = "", Optional ByVal Flag As pdfNoteFlag = 4)
        Dim text2 As String
        '--> cette procedure permet d'ajouter des notes dans le pdf
        '-> on incremente le compteur des notes
        Me.mvarNoteCount += 1
        Me.arrNOTE = DirectCast(Utils.CopyArray(DirectCast(Me.arrNOTE, Array), New NoteDescriptor((Me.mvarNoteCount + 1) - 1) {}), NoteDescriptor())
        X = SingleType.FromObject(Interaction.IIf((X = 0.0!), Me.mvarCurrX, (Me.mvarKUnit * X)))
        Y = SingleType.FromObject(Interaction.IIf((Y = 0.0!), Me.mvarCurrY, (Me.mvarPageHeight - (Me.mvarKUnit * Y))))
        If (Strings.Len(NoteAuthor) = 0) Then
            NoteAuthor = "Deal Informatique"
        End If
        Dim text1 As String = Me.prFormat("<< /Type /Annot" & ChrW(13) & "/Subtype /FreeText" & ChrW(13) & "/Rect [#s #s #s #s]", New Object() {X, Y, (X + (Me.mvarKUnit * Width)), (Y - (Me.mvarKUnit * Height))})
        text1 = text1 & ChrW(13) & "/Contents #p" & Me.prFormat(ChrW(13) & "/F #i", New Object() {Flag})
        If (StringType.StrCmp(NoteAuthor, "", False) <> 0) Then
            text1 = text1 & ChrW(13) & "/T #p"
        End If
        If (StringType.StrCmp(Strings.Left(Me.mvarColorLine, 1), "#", False) = 0) Then
            text2 = Me.prFormat("[#C] rg ", New Object() {Me.mvarColorLine})
        End If
        If (TextAlign = pdfTextAlignment.pdfAlignRight) Then
            text1 = (text1 & ChrW(13) & "/Q 2")
        ElseIf (TextAlign = pdfTextAlignment.pdfCenter) Then
            text1 = (text1 & ChrW(13) & "/Q 1")
        End If
        text2 = (text2 & Me.prFormat("#s Tf", New Object() {Me.mvarFontSize}))
        If (StringType.StrCmp(text2, "", False) <> 0) Then
            text1 = (text1 & ChrW(13) & "/DA (" & text2 & ") ")
        End If
        If (StringType.StrCmp(Strings.Left(Me.mvarColorFill, 1), "#", False) = 0) Then
            text1 = (text1 & ChrW(13) & Me.prFormat("/C [#C]", New Object() {Me.mvarColorFill}))
        End If
        text1 = (text1 & ChrW(13) & "/BS << /Type /Border /W " & StringType.FromInteger(BorderWidth))
        Select Case Border
            Case pdfNoteBorder.pdfSolid
                text1 = (text1 & "/S /S")
                Exit Select
            Case pdfNoteBorder.pdfDashed
                text1 = (text1 & "/S /D")
                text1 = String.Concat(New String() {text1, "/D [ ", Me.prToStr(DashOn, 3), " ", Me.prToStr(DashOff, 3), "]"})
                Exit Select
            Case pdfNoteBorder.pdfBeleved
                text1 = (text1 & "/S /B")
                Exit Select
            Case pdfNoteBorder.pdfInset
                text1 = (text1 & "/S /I")
                Exit Select
            Case pdfNoteBorder.pdfUnderline
                text1 = (text1 & "/S /U")
                Exit Select
        End Select
        text1 = (text1 & ">>" & ChrW(13) & ">>")
        Me.arrNOTE(Me.mvarNoteCount).Base = text1
        Me.arrNOTE(Me.mvarNoteCount).author = [NoteAuthor]
        Me.arrNOTE(Me.mvarNoteCount).Text = [Text]
    End Sub

    Public Sub AddNoteIcon(ByVal [Text] As String, Optional ByVal Icon As pdfNoteIcon = 0, Optional ByVal X As Single = 0.0!, Optional ByVal Y As Single = 0.0!, Optional ByVal Opened As Boolean = True, Optional ByVal NoteAuthor As String = "", Optional ByVal Flag As pdfNoteFlag = 4)
        '--> cette procedure permet d'ajouter un icone de notes sur la feuille
        '-> on incremente le compteur
        Me.mvarNoteCount += 1
        Me.arrNOTE = DirectCast(Utils.CopyArray(DirectCast(Me.arrNOTE, Array), New NoteDescriptor((Me.mvarNoteCount + 1) - 1) {}), NoteDescriptor())
        X = SingleType.FromObject(Interaction.IIf((X = 0.0!), Me.mvarCurrX, (Me.mvarKUnit * X)))
        Y = SingleType.FromObject(Interaction.IIf((Y = 0.0!), Me.mvarCurrY, (Me.mvarPageHeight - (Me.mvarKUnit * Y))))
        Dim text1 As String = Me.prFormat("<< /Type /Annot" & ChrW(13) & "/Subtype /Text" & ChrW(13) & "/Rect [#s #s #s #s]", New Object() {X, Y, X, Y})
        text1 = text1 & ChrW(13) & "/Contents #p" & Me.prFormat(ChrW(13) & "/F #i", New Object() {Flag})
        If (StringType.StrCmp(NoteAuthor, "", False) <> 0) Then
            text1 = text1 & ChrW(13) & "/T #p"
        End If
        If (StringType.StrCmp(Strings.Left(Me.mvarColorFill, 1), "#", False) = 0) Then
            text1 = (text1 & ChrW(13) & Me.prFormat("/C [#C]", New Object() {Me.mvarColorFill}))
        End If
        Select Case Icon
            Case pdfNoteIcon.pdfNote
                text1 = (text1 & ChrW(13) & " /Name /Note")
                Exit Select
            Case pdfNoteIcon.pdfComment
                text1 = (text1 & ChrW(13) & " /Name /Comment")
                Exit Select
            Case pdfNoteIcon.pdfKey
                text1 = (text1 & ChrW(13) & " /Name /Key")
                Exit Select
            Case pdfNoteIcon.pdfHelp
                text1 = (text1 & ChrW(13) & " /Name /Help")
                Exit Select
            Case pdfNoteIcon.pdfParagraph
                text1 = (text1 & ChrW(13) & " /Name /Paragraph")
                Exit Select
            Case pdfNoteIcon.pdfNewParagraph
                text1 = (text1 & ChrW(13) & " /Name /NewParagraph")
                Exit Select
            Case pdfNoteIcon.pdfInsert
                text1 = (text1 & ChrW(13) & " /Name /Insert")
                Exit Select
        End Select
        text1 = (text1 & ChrW(13) & Me.prFormat(" /Open #b", New Object() {Opened}) & " >>")
        'text1 = (text1 & ChrW(13) & "/T (toto) /IRT (toto) /RT R /State Unmarked /StateModel Marked")
        '-> on se charge l'objet que l'on a pas totalement trait� car on a pas la position dans le dico
        Me.arrNOTE(Me.mvarNoteCount).Base = text1
        Me.arrNOTE(Me.mvarNoteCount).Text = [Text]
        Me.arrNOTE(Me.mvarNoteCount).author = NoteAuthor
    End Sub

    Public Sub AddFileJoin(ByVal [Text] As String)
        '-> Cette proc�dure permet d'ajouter un fichier joint dans le pdf
        Me.mvarFileJoin += 1
        Me.arrFILEJOIN = DirectCast(Utils.CopyArray(DirectCast(Me.arrFILEJOIN, Array), New String((Me.mvarFileJoin + 1) - 1) {}), String())
        Me.arrFILEJOIN(Me.mvarFileJoin) = Text
    End Sub

    Public Sub AddWebLink(ByVal URL As String, Optional ByVal X As Single = 0.0!, Optional ByVal Y As Single = 0.0!, Optional ByVal Width As Single = 0.0!, Optional ByVal Height As Single = 0.0!, Optional ByVal BorderWidth As Integer = 1, Optional ByVal OnMouseDown As pdfLinkDown = 0)
        Me.mvarNoteCount += 1
        Me.arrNOTE = DirectCast(Utils.CopyArray(DirectCast(Me.arrNOTE, Array), New NoteDescriptor((Me.mvarNoteCount + 1) - 1) {}), NoteDescriptor())
        X = SingleType.FromObject(Interaction.IIf((X = 0.0!), Me.mvarCurrX, (Me.mvarKUnit * X)))
        Y = SingleType.FromObject(Interaction.IIf((Y = 0.0!), Me.mvarCurrY, (Me.mvarPageHeight - (Me.mvarKUnit * Y))))
        Dim text1 As String = Me.prFormat("<< /Type /Annot" & ChrW(13) & "/Subtype /Link" & ChrW(13) & "/Rect  [#s #s #s #s]", New Object() {X, Y, (X + (Me.mvarKUnit * Width)), (Y - (Me.mvarKUnit * Height))})
        text1 = text1 & ChrW(13) & "/A << /S /URI /URI #p >>" & Me.prFormat(ChrW(13) & "/Border [ 0 0 #s]", New Object() {BorderWidth})
        Select Case OnMouseDown
            Case pdfLinkDown.pdfNone
                text1 = (text1 & ChrW(13) & " /H /N")
                Exit Select
            Case pdfLinkDown.pdfInvert
                text1 = (text1 & ChrW(13) & " /H /I")
                Exit Select
            Case pdfLinkDown.pdfBorder
                text1 = (text1 & ChrW(13) & " /H /O")
                Exit Select
            Case pdfLinkDown.pdfPush
                text1 = (text1 & ChrW(13) & " /H /P")
                Exit Select
        End Select
        text1 = (text1 & ">>")
        Me.arrNOTE(Me.mvarNoteCount).Base = text1
        Me.arrNOTE(Me.mvarNoteCount).Text = [URL]
        Me.arrNOTE(Me.mvarNoteCount).author = ""
    End Sub

    Public Sub AxisRotation(ByVal Angle As Single)
        If ((Angle Mod 360.0!) <> 0.0!) Then
            Dim single2 As Single = CSng(-Math.Sin(((3.1415926535897931 * Angle) / 180)))
            Dim single1 As Single = CSng(Math.Cos(((3.1415926535897931 * Angle) / 180)))
            Me.prWriteObj(Me.prFormat("#s #s #s #s 0 0 cm", New Object() {single1, single2, -single2, single1}))
        End If
    End Sub

    Public Sub AxisScaling(ByVal ScaleX As Single, ByVal ScaleY As Single)
        Me.prWriteObj(Me.prFormat("#s 0 0 #s 0 0 cm", New Object() {ScaleX, ScaleY}))
    End Sub

    Public Sub AxisSkewing(ByVal AngleX As Single, ByVal AngleY As Single)
        Dim single1 As Single = CSng(Math.Tan(((3.1415926535897931 * AngleX) / 180)))
        Dim single2 As Single = CSng(Math.Tan(((3.1415926535897931 * AngleY) / 180)))
        Me.prWriteObj(Me.prFormat("1 #s #s 1 0 0 cm", New Object() {single1, single2}))
    End Sub

    Public Sub AxisTranslation(ByVal TranslateX As Single, ByVal TranslateY As Single)
        Me.prWriteObj(Me.prFormat("1 0 0 1 #k #k cm", New Object() {TranslateX, TranslateY}))
        Me.SetXY(0.0!, 0.0!)
    End Sub

    Public Sub BeginColumn(ByVal Width As Single)
        If Me.blnRowFlag Then
            Me.BeginSpace(Me.sngCellX, Me.sngCellY, 1.0!, 1.0!, 0.0!)
            If (StringType.StrCmp(Me.mvarColorFill, "", False) <> 0) Then
                Me.prWriteObj(Me.prFormat("#s #s #k #k re", New Object() {(Me.mvarLineWidth / 2.0!), (Me.mvarLineWidth / 2.0!), (Width - (Me.mvarLineWidth / Me.mvarKUnit)), (Me.sngRowHeight - (Me.mvarLineWidth / Me.mvarKUnit))}))
                Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
            End If
            If (Me.mvarLineWidth > 0.0!) Then
                Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {0, 0, Width, Me.sngRowHeight}))
                Me.prPath(pdfDrawOptions.pdfVisible)
            End If
            Me.sngCellX = (Me.sngCellX + Width)
        Else
            Me.sngColWidth = Width
            Me.prSaveState()
            Me.blnColFlag = True
        End If
    End Sub

    Public Function BeginDoc(Optional ByVal strFileName As String = "") As String
        If (Me.mvarFileNumber <> 0) Then
            FileSystem.FileClose(New Integer() {Me.mvarFileNumber})
        End If
        If (StringType.StrCmp(strFileName, "", False) = 0) Then
            strFileName = ("~" & Conversion.Hex(Conversion.Val((Strings.Format(DateAndTime.Now, "dhhmmss") & Strings.Trim(Conversion.Str(Conversion.Int((VBMath.Rnd * 10.0!))))))) & ".pdf")
        End If
        Me.mvarFileNumber = FileSystem.FreeFile
        Me.mvarFileName = strFileName
        FileSystem.FileOpen(Me.mvarFileNumber, Me.mvarFileName, 2, -1, -1, -1)
        Me.prWriteObj("%PDF-1.3")
        Me.prWriteObj(Me.prFormat("% #t rel. #t", New Object() {"TurboPDF - " & ChrW(169) & " Deal Informatique", "2.0 NET"}))
        Me.prInitObj(1)
        Me.prWriteObj(Me.prFormat("<<" & ChrW(13) & "/Title #p" & ChrW(13) & "/Author #p" & ChrW(13) & "/Creator #p" & ChrW(13) & "/Producer #p" & ChrW(13) & "/CreationDate #p" & ChrW(13) & "/Subject #p" & ChrW(13) & "/Keywords #p" & ChrW(13) & ">>", New Object() {Me.mvarDocTitle, Me.mvarDocAuthor, Me.mvarDocCreator, Me.mvarDocProducer, Me.mvarDocCreationDate, Me.mvarDocSubject, Me.mvarDocKeywords}))
        Me.prEndObj()
        Me.mvarMaxObject = 6
        Return Me.mvarFileName
    End Function

    Public Sub BeginObject(ByVal AliasName As String, Optional ByVal Options As pdfObjectType = 0)
        If Not Me.mvarInObject Then
            Me.mvarInObject = True
            Me.mvarObjectCount += 1
            Me.arrOBJECT = DirectCast(Utils.CopyArray(DirectCast(Me.arrOBJECT, Array), New ObjDescriptor((Me.mvarObjectCount + 1) - 1) {}), ObjDescriptor())
            Dim num1 As Integer = Me.mvarObjectCount
            Me.arrOBJECT(num1).Name = AliasName
            Me.arrOBJECT(num1).Options = Options
        End If
    End Sub

    Public Function BeginPage(Optional ByVal Orientation As pdfPageOrientation = 0, Optional ByVal PageLabel As pdfPageLabel = 0, Optional ByVal LabelPrefix As String = "", Optional ByVal FirstValue As Integer = -1) As Integer
        '--> on initialise la construction de la page
        Dim text1 As String
        '-> et une page de plus
        Me.mvarTotPages += 1
        Me.mvarNoteCount = 0
        Me.mvarCurrentPageObj = Me.prInitObj(-1)
        'Me.prWriteObj(Me.prFormat(StringType.FromObject(ObjectType.StrCatObj(ObjectType.StrCatObj("<< /Type /Page /Parent #i 0 R /Contents #i 0 R" & ChrW(13), Interaction.IIf((Orientation = pdfPageOrientation.pdfLandscape), "/Rotate 90" & ChrW(13), "")), "/Annots #i 0 R >>")), New Object() {4, (Me.mvarCurrentPageObj + 1), (Me.mvarCurrentPageObj + 2)}))
        Me.prWriteObj(Me.prFormat(StringType.FromObject(ObjectType.StrCatObj(ObjectType.StrCatObj("<< /Type /Page /Parent #i 0 R " & ChrW(13) & "/MediaBox [0 0 #s #s]" & ChrW(13) & "/Contents #i 0 R" & ChrW(13), Interaction.IIf((Orientation = pdfPageOrientation.pdfLandscape), "/Rotate 90" & ChrW(13), "")), "/Annots #i 0 R >>")), New Object() {4, Me.mvarPageWidth, Me.mvarPageHeight, (Me.mvarCurrentPageObj + 1), (Me.mvarCurrentPageObj + 2)}))
        Me.prEndObj()
        Me.mvarPages = (Me.mvarPages & StringType.FromInteger(Me.mvarCurrentPageObj) & " 0 R ")
        Select Case PageLabel
            Case pdfPageLabel.pdfDecimalArabic
                text1 = " /S /D"
                Exit Select
            Case pdfPageLabel.pdfUppercaseRoman
                text1 = " /S /R"
                Exit Select
            Case pdfPageLabel.pdfLowercaseRoman
                text1 = " /S /r"
                Exit Select
            Case pdfPageLabel.pdfUppercaseLetters
                text1 = " /S /A"
                Exit Select
            Case pdfPageLabel.pdfLowercaseLetters
                text1 = " /S /a"
                Exit Select
            Case pdfPageLabel.pdfNoLabel
                GoTo Label_0197
        End Select
        If (StringType.StrCmp(LabelPrefix, "", False) <> 0) Then
            text1 = (text1 & " /P (" & Me.prToPdfStr(LabelPrefix) & ")")
        End If
        If (FirstValue <> -1) Then
            text1 = (text1 & " /St " & StringType.FromInteger(FirstValue))
        End If
        If (StringType.StrCmp(text1, "", False) <> 0) Then
            Me.mvarPageLabel = String.Concat(New String() {Me.mvarPageLabel, Me.prToStr((Me.mvarTotPages - 1), 3), " << ", text1, " >> "})
        End If
Label_0197:
        Me.mvarBufferPage.Pointer = 1
        Me.mvarBufferPage.BufferLen = 0
        Me.mvarBufferPage.Buffer = String.Empty

        Me.mvarStreamBuffer = New Byte(1 - 1) {}
        Me.mvarInPage = True
        Me.prWriteObj(Me.prFormat("1 0 0 -1 #s #s cm ", New Object() {Me.mvarMarginL, (Me.mvarPageHeight - Me.mvarMarginT)}))
        If Me.mvarClipPage Then
            Me.prWriteObj(Me.prFormat("#s #s #s #s re W n", New Object() {0, 0, ((Me.mvarPageWidth - Me.mvarMarginR) - Me.mvarMarginL), ((Me.mvarPageHeight - Me.mvarMarginB) - Me.mvarMarginT)}))
        End If
        Me.SetColorFill("Black")
        Me.SetColorLine("Black")
        Me.SetColorText("Black")
        Me.prInsertObject(False)
        Me.mvarCharSpacing = 0.0!
        Me.mvarWordSpacing = 0.0!
        Me.mvarTextScaling = 100.0!
        Me.mvarCurrX = 0.0!
        Me.mvarCurrY = 0.0!
        Me.mvarLineSpacing = 1.0!
        Return Me.mvarTotPages
    End Function

    Public Sub BeginRow(ByVal Height As Single)
        If Me.blnColFlag Then
            Me.BeginSpace(Me.sngCellX, Me.sngCellY, 1.0!, 1.0!, 0.0!)
            If (StringType.StrCmp(Me.mvarColorFill, "", False) <> 0) Then
                Me.prWriteObj(Me.prFormat("#s #s #k #k re", New Object() {(Me.mvarLineWidth / 2.0!), (Me.mvarLineWidth / 2.0!), (Me.sngColWidth - (Me.mvarLineWidth / Me.mvarKUnit)), (Height - (Me.mvarLineWidth / Me.mvarKUnit))}))
                Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
            End If
            If (Me.mvarLineWidth > 0.0!) Then
                Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {0, 0, Me.sngColWidth, Height}))
                Me.prPath(pdfDrawOptions.pdfVisible)
            End If
            Me.sngCellY = (Me.sngCellY + Height)
        Else
            Me.sngRowHeight = Height
            Me.prSaveState()
            Me.blnRowFlag = True
        End If
    End Sub

    Public Sub BeginSpace(Optional ByVal NewX As Single = 0.0!, Optional ByVal NewY As Single = 0.0!, Optional ByVal ScaleX As Single = 1.0!, Optional ByVal ScaleY As Single = 1.0!, Optional ByVal Angle As Single = 0.0!)
        Me.prWriteObj("q")
        If (((NewX <> 0.0!) Or (NewY <> 0.0!)) Or ((ScaleX <> 1.0!) Or (ScaleY <> 1.0!))) Then
            Me.prWriteObj(Me.prFormat("#s 0 0 #s #k #k cm", New Object() {ScaleX, ScaleY, NewX, NewY}))
        End If
        If (Angle <> 0.0!) Then
            Me.AxisRotation(Angle)
        End If
        Me.SetXY(0.0!, 0.0!)
    End Sub

    Public Sub BeginTable(ByVal X As Single, ByVal Y As Single)
        Me.sngCellX = X
        Me.sngCellY = Y
        Me.SetXY(X, Y)
        Me.prSaveState()
        Me.blnRowFlag = False
        Me.blnColFlag = False
        Me.prWriteObj("q")
    End Sub

    Public Sub BeginTiling(ByVal AliasName As String, ByVal xMin As Single, ByVal yMin As Single, ByVal xMax As Single, ByVal yMax As Single, ByVal xStep As Single, ByVal yStep As Single, Optional ByVal Style As pdfPatternType = 0)
        If Not Me.mvarInPattern Then
            Me.mvarInPattern = True
            Me.mvarShadingCount += 1
            Me.arrSHADING = DirectCast(Utils.CopyArray(DirectCast(Me.arrSHADING, Array), New ShadingDescriptor((Me.mvarShadingCount + 1) - 1) {}), ShadingDescriptor())
            Dim num1 As Integer = Me.mvarShadingCount
            Me.arrSHADING(num1).Name = Strings.UCase(Strings.Trim(AliasName))
            Me.arrSHADING(num1).SubType = StringType.FromObject(Interaction.IIf((Style = pdfPatternType.pdfPattern), "P", "S"))
            Me.arrSHADING(num1).Param = Me.prFormat("<< /Type /Pattern /PatternType 1 /PaintType #t#n/TilingType 1#n/BBox [#k #k #k #k]#n/XStep #k /YStep #k#n/Resources #i 0 R#n/Matrix [1 0 0 1 0 0]", New Object() {RuntimeHelpers.GetObjectValue(Interaction.IIf((Style = pdfPatternType.pdfPattern), "1", "2")), xMin, yMin, xMax, yMax, xStep, yStep, 5})
            Me.arrSHADING(num1).Stream = Me.prFormat("1 0 0 -1 0 #k cm", New Object() {(yMax - yMin)})
        End If
        Me.mvarInStencil = (Style = pdfPatternType.pdfStencil)
    End Sub

    Public Sub DrawArc(ByVal X As Single, ByVal Y As Single, ByVal Ray As Single, Optional ByVal StartAngle As Single = 0.0!, Optional ByVal EndAngle As Single = 360.0!, Optional ByVal Ratio As Single = 1.0!, Optional ByVal Pie As Boolean = False, Optional ByVal Quality As Integer = 1, Optional ByVal Options As pdfDrawOptions = 0)
        Me.prWriteObj("n")
        Me.prWriteObj(Me.prFormat("#k #k m", New Object() {X, Y}))
        If ((Options And pdfDrawOptions.pdfFilled) <> pdfDrawOptions.pdfVisible) Then
            Pie = True
        End If
        Dim single5 As Single = Quality
        Dim single6 As Single = EndAngle
        Dim single1 As Single = StartAngle
        Do While FlowControl.ForNextCheckR4(single1, single6, single5)
            Dim single2 As Single = CSng((((4 * Math.Atan(1)) * single1) / 180))
            Dim single3 As Single = CSng((Ray * Math.Cos(CDbl(single2))))
            Dim single4 As Single = CSng(((Ray * Ratio) * Math.Sin(CDbl(single2))))
            If ((single1 = StartAngle) And Not Pie) Then
                Me.prWriteObj(Me.prFormat("#k #k m", New Object() {(X + single3), (Y + single4)}))
            End If
            Me.prWriteObj(Me.prFormat("#k #k l", New Object() {(X + single3), (Y + single4)}))
            single1 = (single1 + single5)
        Loop
        If Pie Then
            Me.prWriteObj(Me.prFormat("#k #k l", New Object() {X, Y}))
        End If
        Me.prPath(Options)
        Me.SetXY(X, Y)
    End Sub

    Public Function DrawBarCode(ByVal BarCodeType As pdfBarCode, ByVal Code As String, ByVal X As Single, ByVal Y As Single, Optional ByVal ShowCode As Boolean = True, Optional ByVal ZoomFactor As Single = 1.0!, Optional ByVal Height As Single = 0.0!, Optional ByVal AddCheck As Boolean = False) As Single
        Dim single1 As Single
        If ShowCode Then
            Me.prSaveState()
        End If
        Select Case BarCodeType
            Case pdfBarCode.pdfEAN13
                single1 = Me.prDrawEAN13(Code, X, Y, ShowCode, ZoomFactor, Height)
                Exit Select
            Case pdfBarCode.pdfCode39
                single1 = Me.prDrawCODE39(Code, X, Y, ShowCode, ZoomFactor, Height, AddCheck)
                Exit Select
            Case pdfBarCode.pdfInterleave25
                single1 = Me.prDrawI25(Code, X, Y, ShowCode, ZoomFactor, Height, AddCheck)
                Exit Select
            Case pdfBarCode.pdfPostNet
                single1 = Me.prDrawPOSTNET(Code, X, Y, ShowCode, ZoomFactor)
                Exit Select
            Case pdfBarCode.pdfCode128
                single1 = Me.prDrawCode128(Code, X, Y, ShowCode, ZoomFactor, Height)
                Exit Select
        End Select
        If ShowCode Then
            Me.prLoadState()
        End If
        Return single1
    End Function

    Public Sub DrawCircle(ByVal X As Single, ByVal Y As Single, ByVal Ray As Single, Optional ByVal Options As pdfDrawOptions = 0)
        Me.DrawEllipse(X, Y, Ray, Ray, Options)
    End Sub

    Public Sub DrawCurveA(ByVal X1 As Single, ByVal Y1 As Single, ByVal X2 As Single, ByVal Y2 As Single, ByVal X3 As Single, ByVal Y3 As Single, Optional ByVal Options As pdfDrawOptions = 0)
        Me.prWriteObj(Me.prFormat("#k #k #k #k #k #k c", New Object() {X1, Y1, X2, Y2, X3, Y3}))
        Me.prPath(Options)
        Me.SetXY(X3, Y3)
    End Sub

    Public Sub DrawCurveB(ByVal X1 As Single, ByVal Y1 As Single, ByVal X2 As Single, ByVal Y2 As Single, Optional ByVal Options As pdfDrawOptions = 0)
        Me.prWriteObj(Me.prFormat("#k #k #k #k v", New Object() {X1, Y1, X2, Y2}))
        Me.prPath(Options)
        Me.SetXY(X2, Y2)
    End Sub

    Public Sub DrawCurveC(ByVal X1 As Single, ByVal Y1 As Single, ByVal X2 As Single, ByVal Y2 As Single, Optional ByVal Options As pdfDrawOptions = 0)
        Me.prWriteObj(Me.prFormat("#k #k #k #k y", New Object() {X1, Y1, X2, Y2}))
        Me.prPath(Options)
        Me.SetXY(X2, Y2)
    End Sub

    Public Sub DrawEllipse(ByVal X As Single, ByVal Y As Single, ByVal XRay As Single, ByVal YRay As Single, Optional ByVal Options As pdfDrawOptions = 0)
        Dim single1 As Single = CSng(((1.3333333333333333 * (Math.Sqrt(2) - 1)) * XRay))
        Dim single2 As Single = CSng(((1.3333333333333333 * (Math.Sqrt(2) - 1)) * YRay))
        Me.prWriteObj(Me.prFormat("#k #k m", New Object() {(X + XRay), Y}))
        Me.prWriteObj(Me.prFormat("#k #k #k #k #k #k c", New Object() {(X + XRay), (Y - single2), (X + single1), (Y - YRay), X, (Y - YRay)}))
        Me.prWriteObj(Me.prFormat("#k #k #k #k #k #k c", New Object() {(X - single1), (Y - YRay), (X - XRay), (Y - single2), (X - XRay), Y}))
        Me.prWriteObj(Me.prFormat("#k #k #k #k #k #k c", New Object() {(X - XRay), (Y + single2), (X - single1), (Y + YRay), X, (Y + YRay)}))
        Me.prWriteObj(Me.prFormat("#k #k #k #k #k #k c", New Object() {(X + single1), (Y + YRay), (X + XRay), (Y + single2), (X + XRay), Y}))
        Me.prPath(Options)
        Me.SetXY(X, Y)
    End Sub

    Public Sub DrawImage(ByVal Filename As String, ByVal DestX As Single, ByVal DestY As Single, Optional ByVal ImgWidth As Single = 0.0!, Optional ByVal ImgHeight As Single = 0.0!, Optional ByVal FrameNumber As Integer = 1, Optional ByVal Angle As Single = 0.0!, Optional ByVal keepProportion As Boolean = False)
        If ((StringType.StrCmp(Strings.UCase(Strings.Right(Filename, 4)), ".EPS", False) = 0) Or (StringType.StrCmp(Strings.UCase(Strings.Right(Filename, 3)), ".AI", False) = 0)) Then
            Me.prParseEps(Filename, DestX, DestY, ImgWidth, ImgHeight)
        ElseIf (StringType.StrCmp(Strings.UCase(Strings.Right(Filename, 4)), ".WMF", False) = 0) Then
            Me.prParseWMF(Filename, DestX, DestY, ImgWidth, ImgHeight)
        Else
            Dim num3 As Integer
            Dim num2 As Integer = 1
            Dim text1 As String = Strings.UCase(Filename)
            Dim num5 As Integer = Me.mvarImageCount
            Dim num1 As Integer = 1
            Do While (num1 <= num5)
                If ((StringType.StrCmp(Me.arrIMAGE(num1).Filename, text1, False) = 0) And (Me.arrIMAGE(num1).Frame = FrameNumber)) Then
                    num3 = num1
                    Exit Do
                End If
                num1 += 1
            Loop
            If (num3 = 0) Then
                Me.mvarImageCount += 1
                Me.arrIMAGE = DirectCast(Utils.CopyArray(DirectCast(Me.arrIMAGE, Array), New ImageDescriptor((Me.mvarImageCount + 1) - 1) {}), ImageDescriptor())
                Dim num4 As Integer = Me.mvarImageCount
                Me.arrIMAGE(num4).Filename = text1
                Me.arrIMAGE(num4).Frame = FrameNumber
                num2 = 1
                Dim text2 As String = Strings.UCase(Strings.Right(Filename, 4))
                If (((StringType.StrCmp(text2, ".GIF", False) = 0) OrElse (StringType.StrCmp(text2, ".BMP", False) = 0)) OrElse (StringType.StrCmp(text2, ".JPG", False) = 0)) Then
                    Dim flag1 As Boolean = Me.prGetImageSize(Me.arrIMAGE(num4).Filename, Me.arrIMAGE(num4).Width, Me.arrIMAGE(num4).Height, Me.arrIMAGE(num4).BPP, Me.arrIMAGE(num4).Device)
                    If (StringType.StrCmp(Strings.UCase(Strings.Right(Filename, 4)), ".JPG", False) = 0) Then
                        Me.arrIMAGE(num4).Filter = "/DCTDecode "
                    End If
                End If
                num3 = Me.mvarImageCount
            End If
            Me.SetXY(DestX, DestY)
            If ((ImgWidth = 0.0!) And (ImgHeight = 0.0!)) Then
                ImgWidth = (CSng(Me.arrIMAGE(num3).Width) / Me.mvarKUnit) * 0.75
                ImgHeight = (CSng(Me.arrIMAGE(num3).Height) / Me.mvarKUnit) * 0.75
            ElseIf ((ImgWidth = 0.0!) And (ImgHeight <> 0.0!)) Then
                ImgWidth = ((ImgHeight * Me.arrIMAGE(num3).Width) / CSng(Me.arrIMAGE(num3).Height))
            ElseIf ((ImgHeight = 0.0!) And (ImgWidth <> 0.0!)) Then
                ImgHeight = ((ImgWidth * Me.arrIMAGE(num3).Height) / CSng(Me.arrIMAGE(num3).Width))
            End If
            '-> si on doit conserver les propoprtions
            If keepProportion Then
                If ImgWidth / ImgHeight <> Me.arrIMAGE(num3).Width / Me.arrIMAGE(num3).Height Then
                    ImgWidth = ImgHeight * Me.arrIMAGE(num3).Width / Me.arrIMAGE(num3).Height
                End If
            End If
            If (StringType.StrCmp(Strings.UCase(Strings.Right(Filename, 4)), ".JPG", False) = 0) Then
                Me.prWriteObj(Me.prFormat("q #k 0 0 #k #k #k cm", New Object() {ImgWidth, -ImgHeight, DestX, (DestY + ImgHeight)}))
            Else
                Me.prWriteObj(Me.prFormat("q #k 0 0 #k #k #k cm", New Object() {ImgWidth, ImgHeight, DestX, DestY}))
            End If
            Me.prWriteObj(Me.prFormat("/_IMG_#i Do Q", New Object() {num3}))
        End If
    End Sub

    Public Sub DrawLine(ByVal X1 As Single, ByVal Y1 As Single, ByVal X2 As Single, ByVal Y2 As Single, Optional ByVal Options As pdfDrawOptions = 0)
        Me.prWriteObj(Me.prFormat("#k #k m #k #k l", New Object() {X1, Y1, X2, Y2}))
        Me.prPath(Options)
        Me.SetXY(X2, Y2)
    End Sub

    Public Sub DrawLineTo(ByVal X As Single, ByVal Y As Single, Optional ByVal Options As pdfDrawOptions = 0)
        Me.prWriteObj(Me.prFormat("#k #k l", New Object() {X, Y}))
        Me.prPath(Options)
        Me.SetXY(X, Y)
    End Sub

    Public Sub DrawPolygon(ByVal X As Single, ByVal Y As Single, ByVal Ray As Single, ByVal Sides As Integer, Optional ByVal Options As pdfDrawOptions = 0)
        Ray = Math.Abs(Ray)
        Me.prWriteObj(Me.prFormat("#k #k m", New Object() {(X + Ray), Y}))
        Dim single2 As Single = CSng((360 / CDbl(Sides)))
        Dim single1 As Single = single2
        Do While (single1 < 360.0!)
            Me.prWriteObj(Me.prFormat("#k #k l", New Object() {(X + (Ray * Math.Cos((((4 * Math.Atan(1)) * single1) / 180)))), (Y + (Ray * Math.Sin((((4 * Math.Atan(1)) * single1) / 180))))}))
            single1 = (single1 + single2)
        Loop
        Me.prPath((Options Or pdfDrawOptions.pdfClosed))
        Me.SetXY((X + Ray), Y)
    End Sub

    Public Sub DrawPolyline(ByVal Points As Object(), Optional ByVal Options As pdfDrawOptions = 0)
        Dim num2 As Long
        Dim num3 As Long = Information.UBound(Points, 1)
        Dim num1 As Long = Information.LBound(Points, 1)
        Do While (num1 <= num3)
            Me.prWriteObj(Me.prFormat(StringType.FromObject(ObjectType.StrCatObj("#k #k ", Interaction.IIf((num1 = Information.LBound(Points, 1)), "m", "l"))), New Object() {SingleType.FromObject(Points(CInt(num1))), SingleType.FromObject(Points(CInt((num1 + 1))))}))
            num2 = num1
            num1 = (num1 + 2)
        Loop
        Me.prPath(Options)
        If ((Options And pdfDrawOptions.pdfClosed) <> pdfDrawOptions.pdfVisible) Then
            num2 = Information.LBound(Points, 1)
        End If
        Me.SetXY(SingleType.FromObject(Points(CInt(num2))), SingleType.FromObject(Points(CInt((num2 + 1)))))
    End Sub

    Public Sub DrawRect(ByVal X As Single, ByVal Y As Single, ByVal XDim As Single, ByVal YDim As Single, Optional ByVal Options As pdfDrawOptions = 0, Optional ByVal Ray As Single = 0.0!)
        Me.prWriteObj("n")
        If (Ray > 0.0!) Then
            If (Ray > (XDim / 2.0!)) Then
                Ray = (XDim / 2.0!)
            End If
            If (Ray > (YDim / 2.0!)) Then
                Ray = (YDim / 2.0!)
            End If
            Dim single1 As Single = CSng((0.55 * Ray))
            Me.prWriteObj(Me.prFormat("#k #k m", New Object() {(X + Ray), Y}))
            Me.prWriteObj(Me.prFormat("#k #k l", New Object() {((X + XDim) - Ray), Y}))
            Me.prWriteObj(Me.prFormat("#k #k #k #k #k #k c", New Object() {(((X + XDim) - Ray) + single1), Y, (X + XDim), ((Y + Ray) - single1), (X + XDim), (Y + Ray)}))
            Me.prWriteObj(Me.prFormat("#k #k l", New Object() {(X + XDim), ((Y + YDim) - Ray)}))
            Me.prWriteObj(Me.prFormat("#k #k #k #k #k #k c", New Object() {(X + XDim), (((Y + YDim) - Ray) + single1), (((X + XDim) - Ray) + single1), (Y + YDim), ((X + XDim) - Ray), (Y + YDim)}))
            Me.prWriteObj(Me.prFormat("#k #k l", New Object() {(X + Ray), (Y + YDim)}))
            Me.prWriteObj(Me.prFormat("#k #k #k #k #k #k c", New Object() {((X + Ray) - single1), (Y + YDim), X, (((Y + YDim) - Ray) + single1), X, ((Y + YDim) - Ray)}))
            Me.prWriteObj(Me.prFormat("#k #k l", New Object() {X, (Y + Ray)}))
            Me.prWriteObj(Me.prFormat("#k #k #k #k #k #k c", New Object() {X, ((Y + Ray) - single1), ((X + Ray) - single1), Y, (X + Ray), Y}))
        Else
            Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {X, Y, XDim, YDim}))
        End If
        Me.prPath(Options)
        Me.SetXY(X, Y)
    End Sub

    Public Sub DrawText(ByVal X As Single, ByVal Y As Single, ByVal [Text] As String, Optional ByVal Align As pdfTextAlignment = 1, Optional ByVal Rotate As Single = 0.0!)
        Align = (Align And (pdfTextAlignment.pdfCenter Or (pdfTextAlignment.pdfAlignRight Or pdfTextAlignment.pdfAlignLeft)))
        Me.prDrawTextLine(X, Y, 0.0!, [Text], 0.0!, Align, False)
    End Sub

    Public Sub DrawTextRtf(ByVal X As Single, ByVal Y As Single, ByVal RtfName As String)
        Me.prDrawTextRtf(X, Y, RtfName)
    End Sub

    Public Function DrawTextBox(ByVal X As Single, ByVal Y As Single, ByVal Width As Single, ByVal Height As Single, ByVal [Text] As String, Optional ByVal Indentation As Single = 0.0!, Optional ByVal Align As pdfTextAlignment = 9, Optional ByVal Options As pdfDrawOptions = 1, Optional ByVal autoAjust As Boolean = False) As String
        '--> cette fonction permet d'ecrire dans un rectangle
        Dim textArray1 As String()
        Dim textArray2 As String()
        Dim flag3 As Boolean
        Dim num1 As Integer
        Dim num5 As Long
        Dim text2 As String
        Dim single4 As Single

        If (StringType.StrCmp(Strings.Left([Text], 1), ChrW(0), False) = 0) Then
            [Text] = Strings.Mid([Text], 2)
            flag3 = True
        End If
        Dim single3 As Single = ((Me.mvarKUnit * Me.TextHeight) * Me.mvarLineSpacing)
        Dim single2 As Single = Me.TextLength(" ")
        If ((Align And pdfTextAlignment.pdfJustify) <> DirectCast(0, pdfTextAlignment)) Then
            Do While (Strings.InStr([Text], "  ", 0) > 0)
                [Text] = Strings.Replace([Text], "  ", " ", 1, -1, 0)
            Loop
        End If
        Do While (Strings.InStr([Text], ChrW(13) & ChrW(10) & ChrW(13) & ChrW(10), 0) > 0)
            [Text] = Strings.Replace([Text], ChrW(13) & ChrW(10) & ChrW(13) & ChrW(10), ChrW(13) & ChrW(10) & " " & ChrW(13) & ChrW(10), 1, -1, 0)
        Loop
        Dim textArray3 As String() = Strings.Split([Text], ChrW(13) & ChrW(10), -1, 0)
        Dim num2 As Integer = 0
        If (Options <> pdfDrawOptions.pdfInvisible) Then
            Me.prWriteObj(Me.prFormat("#k #k #k #k re ", New Object() {X, Y, Width, Height}))
            Me.prPath(Options)
        End If
        Do While (num2 <= Information.UBound(textArray3, 1))
            Dim flag2 As Boolean = True
            '-> on eclate les differents mots
            Dim textArray4 As String() = Strings.Split(textArray3(num2), " ", -1, 0)
            Dim num4 As Integer = Information.UBound(textArray4, 1)
            If (num4 >= 0) Then
                Dim singleArray1 As Single() = New Single(((num4 + 1) + 1) - 1) {}
                Dim num8 As Integer = num4
                num1 = 0
                '-> on se defionit les tailles de tous les mots
                Do While (num1 <= num8)
                    singleArray1((num1 + 1)) = Me.TextLength(textArray4(num1))
                    num1 += 1
                Loop
                num1 = 0
                '-> on boucle sur le nombre de mots
                Do While (num1 <= num4)
                    Dim text3 As String = textArray4(num1)
                    Dim single1 As Single = SingleType.FromObject(ObjectType.AddObj(singleArray1((num1 + 1)), Interaction.IIf((num1 = 0), Indentation, 0)))
                    Dim flag1 As Boolean = True
                    Dim num3 As Integer = 0
                    num1 += 1
                    '-> on boucle sur les mots tant que l'on ne depasse pas la largeur de ligne
                    Do While ((num1 <= num4) And flag1)
                        If (((((single1 + ((num3 + 1) * single2)) + singleArray1((num1 + 1))) <= Width) Or ((Align And pdfTextAlignment.pdfFit) = pdfTextAlignment.pdfFit)) Or ((Align And pdfTextAlignment.pdfForceFit) = pdfTextAlignment.pdfForceFit)) Then
                            text3 = (text3 & " " & textArray4(num1))
                            single1 = (single1 + singleArray1((num1 + 1)))
                            num3 += 1
                            num1 += 1
                        Else
                            '-> dans le cas ou il ne reste qu'un mot on essaye de le faire passer si y a plus de place dessous
                            If Not autoAjust Then
                                text3 = (text3 & " " & textArray4(num1))
                                single1 = (single1 + singleArray1((num1 + 1)))
                                num3 += 1
                                num1 += 1
                                flag1 = False '->14/05/2014 rajout du flag pour se limiter � l'ajout d'un seul mot
                            Else
                                flag1 = False
                            End If
                        End If
                    Loop
                    num5 = (num5 + 1)
                    textArray1 = DirectCast(Utils.CopyArray(DirectCast(textArray1, Array), New String((CInt(num5) + 1) - 1) {}), String())
                    textArray2 = DirectCast(Utils.CopyArray(DirectCast(textArray2, Array), New String((CInt(num5) + 1) - 1) {}), String())
                    textArray1(CInt(num5)) = text3
                    If (flag2 And Not flag3) Then
                        textArray2(CInt(num5)) = "F"
                    End If
                    If (num1 > num4) Then
                        textArray2(CInt(num5)) = (textArray2(CInt(num5)) & "L")
                    End If
                    flag2 = False
                    flag3 = False
                    If Not ((Height > 0.0!) And ((Me.mvarKUnit * Height) < ((num5 + 1) * single3))) Then
                        Continue Do
                    End If
                    '-> on se met un test sur les depassement limites de hauteur et qu'il ne reste qu'un mot ou un blanc
                    If autoAjust Then Continue Do
                    'If num1 = num4 And textArray4(num4) <> "" Then Continue Do

                    text2 = ""
                    If (num1 <= num4) Then
                        text2 = ChrW(0)
                    End If
                    GoTo Label_036C
Label_02E5:
                    text2 = (text2 & textArray4(num1))
                    If (num1 < num4) Then
                        text2 = (text2 & " ")
                    End If
                    num1 += 1
Label_030D:
                    If (num1 <= num4) Then
                        GoTo Label_02E5
                    End If
                    Dim num7 As Integer = Information.UBound(textArray3, 1)
                    num1 = (num2 + 1)
                    Do While (num1 <= num7)
                        If (StringType.StrCmp(text2, "", False) <> 0) Then
                            text2 = (text2 & ChrW(13) & ChrW(10))
                        End If
                        text2 = (text2 & textArray3(num1))
                        num1 += 1
                    Loop
                    num2 = (Information.UBound(textArray3, 1) + 1)
                    num1 = (num4 + 1)
Label_036C:
                    If (num2 <= Information.UBound(textArray3, 1)) Then
                        GoTo Label_030D
                    End If
                    Application.DoEvents()
                Loop
                num2 += 1
            End If
            Application.DoEvents()
        Loop
            If ((Align And pdfTextAlignment.pdfMiddle) = pdfTextAlignment.pdfMiddle) Then
                single4 = ((Me.mvarKUnit * (Y + (Height / 2.0!))) + ((single3 / 2.0!) * (2 - num5)))
            ElseIf ((Align And pdfTextAlignment.pdfAlignBottom) = pdfTextAlignment.pdfAlignBottom) Then
                single4 = ((Me.mvarKUnit * (Y + Height)) - (single3 * (num5 - 1)))
            Else
                single4 = ((Me.mvarKUnit * Y) + single3)
            End If
            single4 = (single4 + ((Me.mvarFontSize * Me.arrFONT(Me.mvarFontIndex).BaseLine) / 1000.0!))
            Dim num6 As Integer = CInt(num5)
            num1 = 1
            '-> on boucle sur les �lements de lignes pour les ecrires
            Do While (num1 <= num6)
                Me.prDrawTextLine(X, ((single4 + ((num1 - 1) * single3)) / Me.mvarKUnit), Width, textArray1(num1), SingleType.FromObject(Interaction.IIf((Strings.InStr(textArray2(num1), "F", 0) > 0), Indentation, 0)), DirectCast(IntegerType.FromObject(Interaction.IIf(((Strings.InStr(textArray2(num1), "L", 0) > 0) And ((Align And pdfTextAlignment.pdfJustify) <> DirectCast(0, pdfTextAlignment))), pdfTextAlignment.pdfAlignLeft, Align)), pdfTextAlignment), False)
                num1 += 1
            Loop
            Me.SetXY(X, Y)
            Return text2
    End Function

    Public Sub EndColumn()
        If Me.blnRowFlag Then
            Me.EndSpace()
        Else
            Me.prLoadState()
            Me.sngCellX = (Me.sngCellX + Me.sngColWidth)
            Me.sngCellY = (Me.mvarCurrY / Me.mvarKUnit)
        End If
        Me.blnColFlag = False
    End Sub

    Public Sub EndDoc(Optional ByVal Show As Boolean = False, Optional ByVal AutoPrint As Boolean = False, Optional ByVal ShowDialog As Boolean = True, Optional ByVal StartPage As Integer = 0, Optional ByVal EndPage As Integer = 0)
        Dim num1 As Integer
        Dim num2 As Integer
        Dim num5 As Integer
        Dim num10 As Integer
        Dim num12 As Integer
        Dim text2 As String
        Dim text3 As String
        Dim text4 As String
        Dim text5 As String
        Dim text6 As String
        '-> on ajoute les bookmark
        If (Me.mvarBookmarkCount > 1) Then
            num5 = Me.prPutBookMark
        End If
        '-> on ajoute les fichiers joints
        If (Me.mvarFileJoin > 0) Then
            num10 = Me.prPutFileJoin
        End If
        '-> on ajoute les fonts
        If (Me.mvarFontCount > 0) Then
            text3 = Me.prPutFonts
        End If
        '-> on ajoute les images
        If (Me.mvarImageCount > 0) Then
            Trace("ici")
            text4 = Me.prPutImages
        End If
        '-> on ajoute les objets
        If (Me.mvarObjectCount > 0) Then
            text6 = Me.prPutXobject
        End If
        If (Me.mvarShadingCount > 0) Then
            text5 = Me.prPutPattern
        End If
        If (Me.mvarAlphaStateCount > 0) Then
            Dim num9 As Integer = Me.mvarAlphaStateCount
            num1 = 1
            Do While (num1 <= num9)
                Dim num4 As Integer = Me.prInitObj(-1)
                Me.prWriteObj(Me.arrALPHASTATE(num1))
                Me.prEndObj()
                text2 = (text2 & Me.prFormat("/GSTATE_#i #i 0 R ", New Object() {num1, num4}))
                num1 += 1
            Loop
        End If
        '-> on traite la compression
        If Me.mvarEncryption Then
            num2 = Me.prInitObj(-1)
            Me.prWriteObj("<< /Filter /Standard /V 1 /R 2")
            'Me.prWriteObj(Me.prFormat("/O (#t)", New Object() {Me.prToPdfStr(Me.mvarOValue)}))
            Me.prWriteObj(Me.prFormat("/O #h", New Object() {Me.mvarOValue}))
            'Me.prWriteObj(Me.prFormat("/U (#t)", New Object() {Me.prToPdfStr(Me.mvarUValue)}))
            Me.prWriteObj(Me.prFormat("/U #h", New Object() {Me.mvarUValue}))
            Me.prWriteObj(Me.prFormat("/P #i", New Object() {Me.mvarPValue}))
            Me.prWriteObj(">>")
            Me.prEndObj()
        End If
        If AutoPrint Then
            Dim text1 As String = ("print(" & Me.prFormat("#b", New Object() {ShowDialog}))
            If (StartPage <> 0) Then
                text1 = (text1 & Me.prFormat(",nStart:#i", New Object() {(StartPage - 1)}))
            End If
            If (EndPage <> 0) Then
                text1 = (text1 & Me.prFormat(",nEnd:#i", New Object() {(EndPage - 1)}))
            End If
            text1 = (text1 & ");")
            Dim num3 As Integer = Me.prInitObj(-1)
            Me.prWriteObj(Me.prFormat("<< /Names [(EmbeddedJS) #i 0 R ] >>", New Object() {(num3 + 1)}))
            num12 = num3
            Me.prEndObj()
            Me.prInitObj(-1)
            Me.prWriteObj(Me.prFormat("<< /S /JavaScript/JS #p >>", New Object() {text1}))
            Me.prEndObj()
        End If
        Dim num7 As Integer = Me.prInitObj(-1)
        Me.prWriteObj("<< /Type /Metadata /Subtype /XML")
        Dim buffer1 As Byte() = DirectCast(Me.prStr2Mtx(Me.prCreate_XML), Byte())
        Me.prPutStream(buffer1, 0, "", 0)
        Me.prEndObj()
        Me.prInitObj(2)
        Me.prWriteObj(StringType.FromObject(ObjectType.StrCatObj(ObjectType.StrCatObj(ObjectType.StrCatObj((("<< /Type /Catalog" & ChrW(13) & "/Pages " & StringType.FromInteger(4)) & " 0 R"), Interaction.IIf((Me.mvarBookmarkCount > 1), (ChrW(13) & "/Outlines " & StringType.FromInteger(num5) & " 0 R"), "")), Interaction.IIf((StringType.StrCmp(Me.mvarPageLabel, "", False) <> 0), (ChrW(13) & "/PageLabels << /Nums [ " & Me.mvarPageLabel & " ] >>"), "")), Interaction.IIf((num7 <> 0), (ChrW(13) & "/Metadata " & StringType.FromInteger(num7) & " 0 R"), ""))))
        Select Case (Me.mvarViewerPreference And (pdfViewerPreference.pdfTwoColumnRight Or (pdfViewerPreference.pdfTwoColumnLeft Or (pdfViewerPreference.pdfOneColumn Or pdfViewerPreference.pdfSinglePage))))
            Case pdfViewerPreference.pdfOneColumn
                Me.prWriteObj("/PageLayout /OneColumn")
                GoTo Label_03CA
            Case pdfViewerPreference.pdfTwoColumnLeft
                Me.prWriteObj("/PageLayout /TwoColumnLeft")
                GoTo Label_03CA
            Case pdfViewerPreference.pdfTwoColumnRight
                Me.prWriteObj("/PageLayout /TwoColumnRight")
                Exit Select
        End Select
Label_03CA:
        '-> on s'occupe ici de poser les permissions
        'If Me.mvarPermissionAnnotation Then prPutPermissionAnnotation()
        '-> on s'occupe ici des preferences du viewer
        Select Case (Me.mvarViewerPreference And (pdfViewerPreference.pdfShowThumbs Or (pdfViewerPreference.pdfShowBookmarks Or pdfViewerPreference.pdfFullScreen)))
            Case pdfViewerPreference.pdfFullScreen
                Me.prWriteObj("/PageMode /FullScreen")
                Exit Select
            Case pdfViewerPreference.pdfShowBookmarks
                If (Me.mvarBookmarkCount > 1) Then
                    Me.prWriteObj("/PageMode /UseOutlines")
                End If
                Exit Select
            Case pdfViewerPreference.pdfShowThumbs
                Me.prWriteObj("/PageMode /UseThumbs")
                Exit Select
        End Select
        '-> on s occupe des preferences du viewer
        Dim text7 As String = "/ViewerPreferences << "
        If ((Me.mvarViewerPreference And pdfViewerPreference.pdfHideMenubar) <> DirectCast(0, pdfViewerPreference)) Then
            text7 = (text7 & "/HideMenubar true ")
        End If
        If ((Me.mvarViewerPreference And pdfViewerPreference.pdfHideToolbar) <> DirectCast(0, pdfViewerPreference)) Then
            text7 = (text7 & "/HideToolbar true ")
        End If
        If ((Me.mvarViewerPreference And pdfViewerPreference.pdfHideWindowUI) <> DirectCast(0, pdfViewerPreference)) Then
            text7 = (text7 & "/HideWindowUI true ")
        End If
        If ((Me.mvarViewerPreference And pdfViewerPreference.pdfShowTitle) <> DirectCast(0, pdfViewerPreference)) Then
            text7 = (text7 & "/DisplayDocTitle true ")
            Me.prSetReaderSpec(pdfSpecOptions.pdfAcrobat50)
        End If
        If ((Me.mvarViewerPreference And pdfViewerPreference.pdfCenterWindow) <> DirectCast(0, pdfViewerPreference)) Then
            text7 = (text7 & "/CenterWindow true ")
        End If
        If ((Me.mvarViewerPreference And pdfViewerPreference.pdfFitWindow) <> DirectCast(0, pdfViewerPreference)) Then
            text7 = (text7 & "/FitWindow true ")
        End If
        If Me.mvarPermissionAnnotation Then text7 = text7 & prPutPermissionAnnotation("")
        text7 = text7 & ">>"
        Me.prWriteObj(text7)

        '-> on s occupe si il y a des fichiers joints
        If (Me.mvarFileJoin > 0) Then
            Me.prWriteObj("/Names <<")
            Me.prWriteObj("/EmbeddedFiles <<")
            Dim text8 As String = "/Names [ "
            Dim num11 As Integer = 1
            '-> on parcourt les fichiers joints
            For num11 = 1 To Me.arrFILEJOIN.Length - 1
                text8 = text8 & Me.prFormat("#p #i 0 R ", New Object() {My.Computer.FileSystem.GetFileInfo(Me.arrFILEJOIN(num11)).Name, num10 - (Me.arrFILEJOIN.Length - 1) * 2 + num11 * 2 - 1})
            Next
            Me.prWriteObj(text8)
            Me.prWriteObj("] >>")
            '-> on regarde si il y a un autoprint
            If AutoPrint Then
                Me.prWriteObj("/JavaScript " & num12 & " 0 R")
            End If
            text7 = ">>"
            Me.prWriteObj(text7)
        Else
            '-> on regarde si il y a un autoprint
            If AutoPrint Then
                Me.prWriteObj("/Names <</JavaScript " & num12 & " 0 R>>")
            End If
        End If
        Me.prWriteObj(">>")
        Me.prEndObj()
        Me.prInitObj(3)
        Me.prWriteObj(Me.prFormat("<< /Type /Encoding /BaseEncoding /#t >>", New Object() {RuntimeHelpers.GetObjectValue(Interaction.IIf((Me.mvarEncoding = pdfEncoding.pdfWinAnsiEncoding), "WinAnsiEncoding", "MacRomanEncoding"))}))
        Me.prEndObj()
        Me.prInitObj(5)
        Me.prWriteObj(StringType.FromObject(ObjectType.StrCatObj(ObjectType.StrCatObj(ObjectType.StrCatObj(ObjectType.StrCatObj(ObjectType.StrCatObj(ObjectType.StrCatObj(ObjectType.StrCatObj("<<" & ChrW(13), Interaction.IIf((StringType.StrCmp(text3, "", False) <> 0), (("/Font <<" & ChrW(13) & text3) & ">>" & ChrW(13)), "")), "/ProcSet [/PDF /Text"), Interaction.IIf((StringType.StrCmp(text4, "", False) <> 0), " /ImageB /ImageC /ImageI", "")), " ]" & ChrW(13)), Interaction.IIf(((StringType.StrCmp(text4, "", False) <> 0) Or (StringType.StrCmp(text6, "", False) <> 0)), ObjectType.StrCatObj(ObjectType.StrCatObj(ObjectType.StrCatObj("/XObject <<" & ChrW(13), Interaction.IIf((StringType.StrCmp(text4, "", False) <> 0), (text4 & ChrW(13)), "")), text6), ">>" & ChrW(13)), "")), Interaction.IIf((StringType.StrCmp(text2, "", False) <> 0), (("/ExtGState << " & text2) & " >> " & ChrW(13)), "")), ">>")))
        Me.prEndObj()
        Me.prInitObj(6)
        Me.prWriteObj(StringType.FromObject(ObjectType.StrCatObj(ObjectType.StrCatObj(ObjectType.StrCatObj(ObjectType.StrCatObj(ObjectType.StrCatObj(ObjectType.StrCatObj(ObjectType.StrCatObj(ObjectType.StrCatObj("<<" & ChrW(13), Interaction.IIf((StringType.StrCmp(text3, "", False) <> 0), (("/Font <<" & ChrW(13) & text3) & ">>" & ChrW(13)), "")), "/ProcSet [/PDF /Text"), Interaction.IIf((StringType.StrCmp(text4, "", False) <> 0), " /ImageB /ImageC /ImageI", "")), " ]" & ChrW(13)), Interaction.IIf(((StringType.StrCmp(text4, "", False) <> 0) Or (StringType.StrCmp(text6, "", False) <> 0)), ObjectType.StrCatObj(ObjectType.StrCatObj(ObjectType.StrCatObj("/XObject <<" & ChrW(13), Interaction.IIf((StringType.StrCmp(text4, "", False) <> 0), (text4 & ChrW(13)), "")), text6), ">>" & ChrW(13)), "")), Interaction.IIf((StringType.StrCmp(text5, "", False) <> 0), (("/ColorSpace << /xxCSxx [ /Pattern /DeviceRGB ] >> " & ChrW(13) & "/Pattern <<" & ChrW(13) & text5) & ">>" & ChrW(13)), "")), Interaction.IIf((StringType.StrCmp(text2, "", False) <> 0), (("/ExtGState << " & text2) & " >> " & ChrW(13)), "")), ">>")))
        Me.prEndObj()
        Me.prInitObj(4)
        Me.prWriteObj(Me.prFormat("<<#n/Type /Pages /Count #i" & ChrW(13) & "/MediaBox [0 0 #s #s]" & ChrW(13) & "/Kids [#t] /Resources #i 0 R" & ChrW(13) & "#t>>", New Object() {Me.mvarTotPages, Me.mvarPageWidth, Me.mvarPageHeight, Me.mvarPages, 6, RuntimeHelpers.GetObjectValue(Interaction.IIf((Me.mvarOrientation = pdfPageOrientation.pdfLandscape), "/Rotate 90" & ChrW(13), ""))}))
        Me.prEndObj()
        Dim num6 As Long = (FileSystem.Seek(Me.mvarFileNumber) - 1)
        Me.prWriteObj("xref")
        Me.prWriteObj(Me.prFormat("0 #i", New Object() {(Me.mvarMaxObject + 1)}))
        Me.prWriteObj("0000000000 65535 f")
        Dim num8 As Integer = CInt(Me.mvarMaxObject)
        num1 = 1
        Do While (num1 <= num8)
            Me.prWriteObj(Me.arrXREF(num1))
            num1 += 1
        Loop
        Me.prWriteObj("trailer")
        Me.prWriteObj(Me.prFormat("<< /Size #i /Info #i 0 R /Root #i 0 R", New Object() {Me.mvarMaxObject, 1, 2}))
        If (num2 <> 0) Then
            Me.prWriteObj(Me.prFormat("/Encrypt #i 0 R", New Object() {num2}))
            Me.prWriteObj(Me.prFormat("/ID [#h#h]", New Object() {Me.mvarFileID, Me.mvarFileID}))
        End If
        Me.prWriteObj(">>")
        Me.prWriteObj("%" & Crypt(Me.mvarDocHelp))
        Me.prWriteObj(("startxref" & ChrW(13) & StringType.FromLong(num6)))
        FileSystem.PrintLine(Me.mvarFileNumber, New Object() {"%%EOF"})
        If (Me.mvarSpecOptions <> pdfSpecOptions.pdfAcrobat40) Then
            FileSystem.Seek(Me.mvarFileNumber, 1)
            Me.prWriteObj(Me.prFormat("%PDF-1.#i", New Object() {((pdfSpecOptions.pdfAcrobat60 Or pdfSpecOptions.pdfAcrobat50) + Me.mvarSpecOptions)}))
        End If
        FileSystem.FileClose(New Integer() {Me.mvarFileNumber})

        '-> si on doit signer le pdf
        If Me.mvarPermissionAnnotation Then
            'prPutByteRange()
        End If
        '-> on a fini on renomme le fichier
        If My.Computer.FileSystem.FileExists(Mid(PdfFileName, 1, Len(PdfFileName) - 4)) Then
            Try
                My.Computer.FileSystem.DeleteFile(Mid(PdfFileName, 1, Len(PdfFileName) - 4))
            Catch ex As Exception

            End Try
        End If
        Try
            My.Computer.FileSystem.RenameFile(PdfFileName, pdfName)
            Me.mvarFileName = Mid(PdfFileName, 1, Len(PdfFileName) - 4)
        Catch ex As Exception

        End Try
        Try
            Dim i As Integer
            If tempImgLst <> "" Then
                For i = 2 To NumEntries(tempImgLst, ",")
                    Kill(Entry(i, tempImgLst, ","))
                    Try
                        Kill(Entry(i, tempImgLst, ",").Replace(".jpg", ""))
                    Catch ex As Exception
                    End Try
                Next
            End If
        Catch ex As Exception

        End Try
        '-> si on doit afficher a l'ecran le pdf
        If Show Then
            'Process.Start(Me.mvarFileName)
            Interaction.Shell(("rundll32.exe url.dll,FileProtocolHandler " & Me.mvarFileName), AppWinStyle.NormalFocus, False, -1)
        End If
    End Sub

    Private Sub prPutByteRange()
        Dim fs As System.IO.FileStream
        Dim sTemp As String = ""
        Dim bTemp As Byte()
        Dim bFile As Byte()
        Dim num1 As Long
        Dim num2 As Long
        '-> on recupere la position du contents dans le fichier pdf
        Dim sFile As String = File.ReadAllText(Me.mvarFileName, System.Text.Encoding.UTF7)
        num1 = InStr(sFile, "/Contents <") + 11
        num2 = InStr(sFile, "/ByteRange") + Len("/ByteRange") + 2
        '-> on construit le byterange
        sTemp = Mid("[ 0 " & CStr(num1) & " " & CStr(num1 + 7000) & " " & CStr(FileSystem.FileLen(Me.mvarFileName)) & " ]" & Space(100), 1, 100)
        bTemp = prStr2Mtx(sTemp)
        Try
            'Ouverture du fichier et Ecriture du contenu du fichier sur la console
            fs = File.Open(Me.mvarFileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite)
            While fs.Position < fs.Length
                If fs.Position > num2 And fs.Position < num2 + 80 Then
                    fs.WriteByte(bTemp(fs.Position - num2 - 1))
                Else
                    'on passe au suivant
                    fs.ReadByte()
                End If
            End While
        Finally
            'Fermeture
            If Not IsNothing(fs) Then fs.Close()
        End Try
        '-> bon maintenant on hashe le fichier par du SHA-1
        'Dim Hashage As Byte()
        Dim code As New System.Text.UnicodeEncoding
        Dim Hashage() As Byte = code.GetBytes(File.ReadAllText(Me.mvarFileName))
        'Dim sha1hashage As New Security.Cryptography.SHA1CryptoServiceProvider
        'Hashage = sha1hashage.ComputeHash(mdp)
        '-> maintenant on fait une RSA encryption avec notre cl� priv�e pour obtenir la signature
        Dim signerCert As X509Certificate2 = EnvelopedSignedCms.GetSignerCert()
        Dim recipientCert As X509Certificate2 = EnvelopedSignedCms.GetRecipientCert()
        Dim encodedSignedCms As Byte() = EnvelopedSignedCms.SignMsg(Hashage, signerCert)
        Dim encodedEnvelopedCms As Byte() = EnvelopedSignedCms.EncryptMsg(encodedSignedCms, recipientCert)
        'Hashage = EnvelopedSignedCms.EncryptMsg(Hashage, signerCert)
        'Hashage = EnvelopedSignedCms.SignMsg(Hashage, signerCert)
        'EnvelopedSignedCms.DecryptMsg(Hashage)
        encodedSignedCms = EnvelopedSignedCms.DecryptMsg(encodedEnvelopedCms)
        'EnvelopedSignedCms.Main1("")
        'EnvelopedSignedCms.VerifyMsg(encodedEnvelopedCms, Hashage)
        bTemp = prToASCIIHex(Hashage)
        '-> on inscrit la cl�
        fs = File.Open(Me.mvarFileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite)
        While fs.Position < fs.Length
            If fs.Position > num1 + 1 And fs.Position < num1 + 1000 And fs.Position - num1 - 2 < bTemp.Length Then
                fs.WriteByte(bTemp(fs.Position - num1 - 2))
            Else
                'on passe au suivant
                fs.ReadByte()
            End If
        End While
    End Sub

    Private Function prPutPermissionAnnotation(ByVal text As String) As String
        '--> cette procedure donne les autorisations pour les modifications de commentaires
        ''Me.prWriteObj("/Perms <</UR3 <<")
        'Me.prWriteObj("/Cert (" & prToPdfStr(prMtx2Str(EnvelopedSignedCms.GetCertificate())) & ") ")
        'Me.prWriteObj("/Prop_Build <</PubSec <</R 131102 /Date (Oct 22 2006 23:30:44) /NonEFontNoWarn true >> /Filter <</R 131101 /Date (Oct 22 2006 23:36:05) /Name /Adobe.PPKLite >> /App    <</R 524288 /TrustedMode true /Name /Exchange-Pro /OS [/Win] /REx (8.0.0) >> >>")
        ''Me.prWriteObj("/M (D:20070215094336+01'00')")
        ''Me.prWriteObj("/Name (ARE Acrobat Product v8.0 P23 0002337)")
        ''Me.prWriteObj(Mid("/ByteRange [xxxxxx xxxxxx xxxxxx xxxxxx ]" & Space(100), 1, 100) & "/Contents <" & StrDup(7000, "0") & ">")
        ''Me.prWriteObj("/SubFilter /adbe.pkcs7.sha1")
        ''Me.prWriteObj("/Filter /Adobe.PPKLite")
        ''Me.prWriteObj("/Reference ")
        ''Me.prWriteObj("[<</Data 2 0 R")
        ''Me.prWriteObj("/TransformParams <</Annots [/Create /Delete /Modify /Copy /Import /Export] /V /2.2 /Type /TransformParams >>")
        ''Me.prWriteObj("/TransformMethod /UR3 /Type /SigRef >>] /Type /Sig ")
        ''Me.prWriteObj(">> >>")
        ''Me.prWriteObj("/AcroForm <<")
        ''Me.prWriteObj("/Fields [] /SigFlags 2")
        ''Me.prWriteObj(">>")
        text = text & "/Rights"
        text = text & "<</Annots[/Create/Delete/Modify/Copy/Import/Export]"
        text = text & "/Form[/Import/Export/SubmitStandalone/SpawnTemplate]"
        text = text & "/Version 1"
        text = text & "/Document[/FullSave]"
        text = text & "/Signature[/Modify]"
        '/text = text & "/RightsID[(�ht��8.��[@�ӳ�v�\)~����x�oʤ	�Y�:`�ũx��[��^���ͽ>�<�0zR)(�Цa�'��c��B�r�����6�Q�?��0���۩M�\n���\(�P*Q�z�z���\n)(�Цa�'��c��B�r�����6�Q�?��0���۩M�\n���\(�P*Q�z�z���\n)]"
        text = text & "/Msg(With Adobe Document Server for Reader Extensions, usage rights can be added to Adobe Portable Document Format \(PDF\) documents and forms.\n\nThis document shows how users of the free Adobe Reader gain access to commenting and review features. Other featu\"
        text = text & "res that are enabled include the ability to save locally, digitally sign, as well as filling out and submitting forms electronically.\n\nNote that you must have Adobe Reader 5.1 \(or higher\) installed to be able to access these additional rights. You can\"
        text = text & "download the latest version of the free Adobe Reader at http://www.adobe.com/products/acrobat/readstep2.html.)"
        text = text & "/TimeOfUbiquitization(D:20030627204336Z)"
        text = text & ">>"
        Return text

    End Function

    Public Sub EndObject()
        Me.mvarInObject = False
    End Sub

    Public Sub EndPage()
        Dim bytes As Byte() = Encoding.GetEncoding(&H4E4).GetBytes(Strings.Left(Me.mvarBufferPage.Buffer, CInt(Me.mvarBufferPage.Pointer - 1)))
        Me.prInsertObject(True)
        Me.mvarInPage = False
        Dim num2 As Integer = Me.prInitObj(-1)
        Me.prWriteObj("<<")
        Me.prPutStream(bytes, 0, "", 0)
        Me.prEndObj()
        num2 += 1
        Dim text1 As String = ""
        '-> si il y a des notes sur la page
        If (Me.mvarNoteCount > 0) Then
            Dim num3 As Integer = Me.mvarNoteCount
            Dim num1 As Integer = 1
            Do While (num1 <= num3)
                text1 = (text1 & StringType.FromInteger((num2 + num1)) & " 0 R ")
                Me.prInitObj((num2 + num1))
                Me.prWriteObj(Me.prFormat(Me.arrNOTE(num1).Base, New Object() {Me.arrNOTE(num1).Text, Me.arrNOTE(num1).author}))
                Me.prEndObj()
                num1 += 1
            Loop
        End If
        Me.prInitObj(num2)
        Me.prWriteObj(Me.prFormat("[ #t ]", New Object() {text1}))
        Me.prEndObj()
    End Sub

    Public Sub EndRow()
        If Me.blnColFlag Then
            Me.EndSpace()
        Else
            Me.prLoadState()
            Me.mvarCurrY = (Me.mvarCurrY + (Me.sngRowHeight / Me.mvarKUnit))
            Me.sngCellX = (Me.mvarCurrX / Me.mvarKUnit)
            Me.sngCellY = (Me.sngCellY + Me.sngRowHeight)
        End If
        Me.blnRowFlag = False
    End Sub

    Public Sub EndSpace()
        Me.prWriteObj("Q")
    End Sub

    Public Sub EndTable()
        Me.prWriteObj("Q")
        Me.prLoadState()
    End Sub

    Public Sub EndTiling()
        Me.mvarInPattern = False
        Me.mvarInStencil = False
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Public Function GetPage() As Integer
        Return Me.mvarTotPages
    End Function

    Public Function GetX() As Single
        Return (Me.mvarCurrX / Me.mvarKUnit)
    End Function

    Public Function GetY() As Single
        Return (Me.mvarCurrY / Me.mvarKUnit)
    End Function

    Public Sub MoveTo(ByVal X As Single, ByVal Y As Single)
        Me.prWriteObj(Me.prFormat("#k #k m", New Object() {X, Y}))
        Me.SetXY(X, Y)
    End Sub

    Public Sub OpenDoc()
        On Error Resume Next
        'Process.Start(Me.mvarFileName)
        Interaction.Shell(("rundll32.exe url.dll,FileProtocolHandler " & Me.mvarFileName), AppWinStyle.NormalFocus, False, -1)
    End Sub

    Public Sub PlayObject(ByVal Name As String)
        Me.prWriteObj(Me.prFormat("q /#t Do Q", New Object() {Name}))
    End Sub

    Private Function prDeflate(ByVal strData As Byte()) As Byte()

        '**************
        '-> on on ecrit les donn�es de strdata dans un fichier
        Dim strInputFile As String
        Dim strOutputFile As String
        strInputFile = GetTempFileNameVB("Tmp")
        strOutputFile = GetTempFileNameVB("Tmp")
        '-> on compresse les donn�es dans un fichier
        Dim inFileStream As New System.IO.FileStream(strInputFile, System.IO.FileMode.Create)
        inFileStream.Write(strData, 0, strData.Length)
        inFileStream.Flush()
        inFileStream.Close()
        Dim inFileStream2 As New System.IO.FileStream(strInputFile, System.IO.FileMode.Open)
        Dim outFileStream As New System.IO.FileStream(strOutputFile, System.IO.FileMode.Create)
        Dim zStream As New zlib.ZOutputStream(outFileStream, zlib.zlibConst.Z_BEST_COMPRESSION)
        Try
            CopyStream(inFileStream2, zStream)
        Finally
            zStream.Close()
            outFileStream.Close()
            inFileStream2.Close()
        End Try
        '-> maintenant on se lit le fichier creer
        Dim outFileStream2 As New System.IO.FileStream(strOutputFile, System.IO.FileMode.Open)
        Dim compressed_data(outFileStream2.Length - 1) As Byte
        outFileStream2.Read(compressed_data, 0, outFileStream2.Length)
        '-> on se bute les fichiers tempo
        Kill(strInputFile)
        outFileStream2.Close()
        Kill(strOutputFile)
        Return compressed_data
        '********************

    End Function

    Private Sub CopyStream(ByRef input As System.IO.Stream, ByRef output As System.IO.Stream)
        Dim num1 As Integer
        Dim buffer1 As Byte() = New Byte(32000 - 1) {}
        num1 = input.Read(buffer1, 0, 32000)
        Do While (num1 > 0)
            output.Write(buffer1, 0, num1)
            num1 = input.Read(buffer1, 0, 32000)
        Loop
        output.Flush()
        Application.DoEvents()
    End Sub


    Private Function prCreate_XML() As String
        Dim text2 As String
        Dim num1 As Long = 1000
        Do While (Strings.Len(text2) <> num1)
            num1 = Strings.Len(text2)
            text2 = (((((((("<?xpacket begin='' id='W5M0MpCehiHzreSzNTczkc9d' bytes='" & StringType.FromLong(num1) & "'?>" & ChrW(13) & "<rdf:RDF xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'" & ChrW(13) & " xmlns:iX='http://ns.adobe.com/iX/1.0/'>" & ChrW(13) & " <rdf:Description about=''" & ChrW(13) & "  xmlns='http://ns.adobe.com/pdf/1.3/'" & ChrW(13) & "  xmlns:pdf='http://ns.adobe.com/pdf/1.3/'>" & ChrW(13) & "  <pdf:Producer>TurboPDF - " & ChrW(169) & " Deal Informatique" & Application.ProductVersion & "</pdf:Producer>" & ChrW(13) & "  <pdf:Author>DMZ 2017</pdf:Author>" & ChrW(13) & "  <pdf:Creator>" & Me.mvarDocCreator & "</pdf:Creator>" & ChrW(13) & "  <pdf:ModDate>" & Me.mvarDocCreationDate) & "</pdf:ModDate>" & ChrW(13) & "  <pdf:Title>" & Me.mvarDocTitle) & "</pdf:Title>" & ChrW(13) & "  <pdf:CreationDate>" & Me.mvarDocCreationDate) & "</pdf:CreationDate>" & ChrW(13) & "  <pdf:Subject>" & Me.mvarDocSubject) & "</pdf:Subject>" & ChrW(13) & "  <pdf:Keywords>" & Me.mvarDocKeywords) & "</pdf:Keywords>" & ChrW(13) & " </rdf:Description>" & ChrW(13) & " <rdf:Description about=''" & ChrW(13) & "  xmlns='http://ns.adobe.com/xap/1.0/'" & ChrW(13) & "  xmlns:xap='http://ns.adobe.com/xap/1.0/'>" & ChrW(13) & "  <xap:Author>DMZ</xap:Author>" & ChrW(13) & "  <xap:ModifyDate>" & Me.mvarDocCreationDate) & "</xap:ModifyDate>" & ChrW(13) & "  <xap:Title>" & ChrW(13) & "   <rdf:Alt>" & ChrW(13) & "  <rdf:li xml:lang='x-default'>" & Me.mvarDocTitle) & "</rdf:li>" & ChrW(13) & "   </rdf:Alt>" & ChrW(13) & "  </xap:Title>" & ChrW(13))
            text2 = ((((((text2 & "  <xap:CreateDate>" & Me.mvarDocCreationDate) & "</xap:CreateDate>" & ChrW(13) & "  <xap:Description>" & ChrW(13) & "   <rdf:Alt>" & ChrW(13) & "  <rdf:li xml:lang='x-default'>" & Me.mvarDocSubject) & "</rdf:li>" & ChrW(13) & "   </rdf:Alt>" & ChrW(13) & "  </xap:Description>" & ChrW(13) & "  <xap:MetadataDate>" & Me.mvarDocCreationDate) & "</xap:MetadataDate>" & ChrW(13) & " </rdf:Description>" & ChrW(13) & " <rdf:Description about=''" & ChrW(13) & "  xmlns='http://purl.org/dc/elements/1.1/'" & ChrW(13) & "  xmlns:dc='http://purl.org/dc/elements/1.1/'>" & ChrW(13) & "  <dc:creator>" & Me.mvarDocAuthor & "</dc:creator>" & ChrW(13) & "  <dc:title>" & Me.mvarDocTitle) & "</dc:title>" & ChrW(13) & "  <dc:description>" & Me.mvarDocSubject) & "</dc:description>" & ChrW(13) & " </rdf:Description>" & ChrW(13) & "</rdf:RDF>" & ChrW(13) & "<?xpacket end='r'?>")
        Loop
        Return text2
    End Function

    Private Sub prCreateFontArial(ByVal Style As pdfFontStyle)
        Dim objArray1 As Object()
        Me.mvarFontCount += 1
        Me.arrFONT = DirectCast(Utils.CopyArray(DirectCast(Me.arrFONT, Array), New FontDescriptor((Me.mvarFontCount + 1) - 1) {}), FontDescriptor())
        Dim num2 As Integer = Me.mvarFontCount
        Me.arrFONT(num2).Initialize()
        Me.arrFONT(num2).TrueTypeFont = False
        Me.arrFONT(num2).BaseFont = "Arial"
        Me.arrFONT(num2).Height = 1221
        Me.arrFONT(num2).BaseLine = -221
        Select Case (Style And (pdfFontStyle.pdfItalic Or pdfFontStyle.pdfBold))
            Case pdfFontStyle.pdfNormal
                objArray1 = New Object() {272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 272, 278, 278, 355, 556, 556, 889, 667, 191, 333, 333, 389, 584, 278, 333, 278, 278, 556, 556, 556, 556, 556, 556, 556, 556, 556, 556, 278, 278, 584, 584, 584, 556, 1015, 667, 667, 722, 722, 667, 611, 778, 722, 278, 500, 667, 556, 833, 722, 778, 667, 778, 722, 667, 611, 722, 667, 944, 667, 667, 611, 278, 278, 278, 469, 556, 333, 556, 556, 500, 556, 556, 278, 556, 556, 222, 222, 500, 222, 833, 556, 556, 556, 556, 333, 500, 278, 556, 500, 722, 500, 500, 500, 334, 260, 334, 584, 750, 556, 750, 222, 556, 333, 1000, 556, 556, 333, 1000, 667, 333, 1000, 750, 611, 750, 750, 222, 222, 333, 333, 350, 556, 1000, 333, 1000, 500, 333, 944, 750, 500, 667, 278, 333, 556, 556, 556, 556, 260, 556, 333, 737, 370, 556, 584, 333, 737, 552, 400, 549, 333, 333, 333, 576, 537, 278, 333, 333, 365, 556, 834, 834, 834, 611, 667, 667, 667, 667, 667, 667, 1000, 722, 667, 667, 667, 667, 278, 278, 278, 278, 722, 722, 778, 778, 778, 778, 778, 584, 778, 722, 722, 722, 722, 667, 667, 611, 556, 556, 556, 556, 556, 556, 889, 500, 556, 556, 556, 556, 278, 278, 278, 278, 556, 556, 556, 556, 556, 556, 556, 549, 611, 556, 556, 556, 556, 500, 556, 500}
                Exit Select
            Case pdfFontStyle.pdfBold
                Me.arrFONT(Me.mvarFontCount).BaseFont = (Me.arrFONT(Me.mvarFontCount).BaseFont & ",Bold")
                objArray1 = New Object() {311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 278, 333, 474, 556, 556, 889, 722, 238, 333, 333, 389, 584, 278, 333, 278, 278, 556, 556, 556, 556, 556, 556, 556, 556, 556, 556, 333, 333, 584, 584, 584, 611, 975, 722, 722, 722, 722, 667, 611, 778, 722, 278, 556, 722, 611, 833, 722, 778, 667, 778, 722, 667, 611, 722, 667, 944, 667, 667, 611, 333, 278, 333, 584, 556, 333, 556, 611, 556, 611, 556, 333, 611, 611, 278, 278, 556, 278, 889, 611, 611, 611, 611, 389, 556, 333, 611, 556, 778, 556, 556, 500, 389, 280, 389, 584, 750, 556, 750, 278, 556, 500, 1000, 556, 556, 333, 1000, 667, 333, 1000, 750, 611, 750, 750, 278, 278, 500, 500, 350, 556, 1000, 333, 1000, 556, 333, 944, 750, 500, 667, 278, 333, 556, 556, 556, 556, 280, 556, 333, 737, 370, 556, 584, 333, 737, 552, 400, 549, 333, 333, 333, 576, 556, 278, 333, 333, 365, 556, 834, 834, 834, 611, 722, 722, 722, 722, 722, 722, 1000, 722, 667, 667, 667, 667, 278, 278, 278, 278, 722, 722, 778, 778, 778, 778, 778, 584, 778, 722, 722, 722, 722, 667, 667, 611, 556, 556, 556, 556, 556, 556, 889, 556, 556, 556, 556, 556, 278, 278, 278, 278, 611, 611, 611, 611, 611, 611, 611, 549, 611, 611, 611, 611, 611, 556, 611, 556}
                Exit Select
            Case pdfFontStyle.pdfItalic
                Me.arrFONT(Me.mvarFontCount).BaseFont = (Me.arrFONT(Me.mvarFontCount).BaseFont & ",Italic")
                objArray1 = New Object() {259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 259, 278, 278, 355, 556, 556, 889, 667, 191, 333, 333, 389, 584, 278, 333, 278, 278, 556, 556, 556, 556, 556, 556, 556, 556, 556, 556, 278, 278, 584, 584, 584, 556, 1015, 667, 667, 722, 722, 667, 611, 778, 722, 278, 500, 667, 556, 833, 722, 778, 667, 778, 722, 667, 611, 722, 667, 944, 667, 667, 611, 278, 278, 278, 469, 556, 333, 556, 556, 500, 556, 556, 278, 556, 556, 222, 222, 500, 222, 833, 556, 556, 556, 556, 333, 500, 278, 556, 500, 722, 500, 500, 500, 334, 260, 334, 584, 750, 556, 750, 222, 556, 333, 1000, 556, 556, 333, 1000, 667, 333, 1000, 750, 611, 750, 750, 222, 222, 333, 333, 350, 556, 1000, 333, 1000, 500, 333, 944, 750, 500, 667, 278, 333, 556, 556, 556, 556, 260, 556, 333, 737, 370, 556, 584, 333, 737, 552, 400, 549, 333, 333, 333, 576, 537, 278, 333, 333, 365, 556, 834, 834, 834, 611, 667, 667, 667, 667, 667, 667, 1000, 722, 667, 667, 667, 667, 278, 278, 278, 278, 722, 722, 778, 778, 778, 778, 778, 584, 778, 722, 722, 722, 722, 667, 667, 611, 556, 556, 556, 556, 556, 556, 889, 500, 556, 556, 556, 556, 278, 278, 278, 278, 556, 556, 556, 556, 556, 556, 556, 549, 611, 556, 556, 556, 556, 500, 556, 500}
                Exit Select
            Case (pdfFontStyle.pdfItalic Or pdfFontStyle.pdfBold)
                Me.arrFONT(Me.mvarFontCount).BaseFont = (Me.arrFONT(Me.mvarFontCount).BaseFont & ",BoldItalic")
                objArray1 = New Object() {311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 311, 278, 333, 474, 556, 556, 889, 722, 238, 333, 333, 389, 584, 278, 333, 278, 278, 556, 556, 556, 556, 556, 556, 556, 556, 556, 556, 333, 333, 584, 584, 584, 611, 975, 722, 722, 722, 722, 667, 611, 778, 722, 278, 556, 722, 611, 833, 722, 778, 667, 778, 722, 667, 611, 722, 667, 944, 667, 667, 611, 333, 278, 333, 584, 556, 333, 556, 611, 556, 611, 556, 333, 611, 611, 278, 278, 556, 278, 889, 611, 611, 611, 611, 389, 556, 333, 611, 556, 778, 556, 556, 500, 389, 280, 389, 584, 750, 556, 750, 278, 556, 500, 1000, 556, 556, 333, 1000, 667, 333, 1000, 750, 611, 750, 750, 278, 278, 500, 500, 350, 556, 1000, 333, 1000, 556, 333, 944, 750, 500, 667, 278, 333, 556, 556, 556, 556, 280, 556, 333, 737, 370, 556, 584, 333, 737, 552, 400, 549, 333, 333, 333, 576, 556, 278, 333, 333, 365, 556, 834, 834, 834, 611, 722, 722, 722, 722, 722, 722, 1000, 722, 667, 667, 667, 667, 278, 278, 278, 278, 722, 722, 778, 778, 778, 778, 778, 584, 778, 722, 722, 722, 722, 667, 667, 611, 556, 556, 556, 556, 556, 556, 889, 556, 556, 556, 556, 556, 278, 278, 278, 278, 611, 611, 611, 611, 611, 611, 611, 549, 611, 611, 611, 611, 611, 556, 611, 556}
                Exit Select
        End Select
        Dim num1 As Integer = 0
        Do
            Me.arrFONT(Me.mvarFontCount).Widths(num1) = IntegerType.FromObject(objArray1(num1))
            num1 += 1
        Loop While (num1 <= 255)
        Me.arrFONT(Me.mvarFontCount).FontAlias = Strings.UCase(Me.arrFONT(Me.mvarFontCount).BaseFont)
    End Sub

    Private Sub prCreateFontCourier(ByVal Style As pdfFontStyle)
        Me.mvarFontCount += 1
        Me.arrFONT = DirectCast(Utils.CopyArray(DirectCast(Me.arrFONT, Array), New FontDescriptor((Me.mvarFontCount + 1) - 1) {}), FontDescriptor())
        Dim num2 As Integer = Me.mvarFontCount
        Me.arrFONT(num2).Initialize()
        Me.arrFONT(num2).TrueTypeFont = False
        Me.arrFONT(num2).BaseFont = "CourierNew"
        Me.arrFONT(num2).Height = 1133
        Me.arrFONT(num2).BaseLine = -300
        Dim num1 As Integer = 0
        Do
            Me.arrFONT(num2).Widths(num1) = 600
            num1 += 1
        Loop While (num1 <= 255)
        Select Case (Style And (pdfFontStyle.pdfItalic Or pdfFontStyle.pdfBold))
            Case pdfFontStyle.pdfBold
                Me.arrFONT(Me.mvarFontCount).BaseFont = (Me.arrFONT(Me.mvarFontCount).BaseFont & ",Bold")
                Exit Select
            Case pdfFontStyle.pdfItalic
                Me.arrFONT(Me.mvarFontCount).BaseFont = (Me.arrFONT(Me.mvarFontCount).BaseFont & ",Italic")
                Exit Select
            Case (pdfFontStyle.pdfItalic Or pdfFontStyle.pdfBold)
                Me.arrFONT(Me.mvarFontCount).BaseFont = (Me.arrFONT(Me.mvarFontCount).BaseFont & ",BoldItalic")
                Exit Select
        End Select
        Me.arrFONT(Me.mvarFontCount).FontAlias = Strings.UCase(Me.arrFONT(Me.mvarFontCount).BaseFont)
    End Sub

    Private Sub prCreateFontSymbol(ByVal Style As pdfFontStyle)
        Dim objArray1 As Object()
        Me.mvarFontCount += 1
        Me.arrFONT = DirectCast(Utils.CopyArray(DirectCast(Me.arrFONT, Array), New FontDescriptor((Me.mvarFontCount + 1) - 1) {}), FontDescriptor())
        Dim num2 As Integer = Me.mvarFontCount
        Me.arrFONT(num2).Initialize()
        Me.arrFONT(num2).TrueTypeFont = False
        Me.arrFONT(num2).BaseFont = "Symbol"
        Me.arrFONT(num2).Height = 1225
        Me.arrFONT(num2).BaseLine = -220
        Select Case (Style And (pdfFontStyle.pdfItalic Or pdfFontStyle.pdfBold))
            Case pdfFontStyle.pdfNormal
                objArray1 = New Object() {332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 600, 600, 250, 333, 713, 500, 549, 833, 778, 439, 333, 333, 500, 549, 250, 549, 250, 278, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 278, 278, 549, 549, 549, 444, 549, 722, 667, 722, 612, 611, 763, 603, 722, 333, 631, 722, 686, 889, 722, 722, 768, 741, 556, 592, 611, 690, 439, 768, 645, 795, 611, 333, 863, 333, 658, 500, 500, 631, 549, 549, 494, 439, 521, 411, 603, 329, 603, 549, 549, 576, 521, 549, 549, 521, 549, 603, 439, 576, 713, 686, 493, 686, 494, 480, 200, 480, 549, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 620, 247, 549, 167, 713, 500, 753, 753, 753, 753, 1042, 987, 603, 987, 603, 400, 549, 411, 549, 549, 713, 494, 460, 549, 549, 549, 549, 1000, 603, 1000, 658, 823, 686, 795, 987, 768, 768, 823, 768, 768, 713, 713, 713, 713, 713, 713, 713, 768, 713, 790, 790, 890, 823, 549, 250, 713, 603, 603, 1042, 987, 603, 987, 603, 494, 329, 790, 790, 786, 713, 384, 384, 384, 384, 384, 384, 494, 494, 494, 494, 600, 329, 274, 686, 686, 686, 384, 384, 384, 384, 384, 384, 494, 494, 494, 600}
                Exit Select
            Case pdfFontStyle.pdfBold
                Me.arrFONT(Me.mvarFontCount).BaseFont = (Me.arrFONT(Me.mvarFontCount).BaseFont & ",Bold")
                objArray1 = New Object() {332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 600, 600, 250, 333, 713, 500, 549, 833, 778, 439, 333, 333, 500, 549, 250, 549, 250, 278, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 278, 278, 549, 549, 549, 444, 549, 722, 667, 722, 612, 611, 763, 603, 722, 333, 631, 722, 686, 889, 722, 722, 768, 741, 556, 592, 611, 690, 439, 768, 645, 795, 611, 333, 863, 333, 658, 500, 500, 631, 549, 549, 494, 439, 521, 411, 603, 329, 603, 549, 549, 576, 521, 549, 549, 521, 549, 603, 439, 576, 713, 686, 493, 686, 494, 480, 200, 480, 549, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 620, 247, 549, 167, 713, 500, 753, 753, 753, 753, 1042, 987, 603, 987, 603, 400, 549, 411, 549, 549, 713, 494, 460, 549, 549, 549, 549, 1000, 603, 1000, 658, 823, 686, 795, 987, 768, 768, 823, 768, 768, 713, 713, 713, 713, 713, 713, 713, 768, 713, 790, 790, 890, 823, 549, 250, 713, 603, 603, 1042, 987, 603, 987, 603, 494, 329, 790, 790, 786, 713, 384, 384, 384, 384, 384, 384, 494, 494, 494, 494, 600, 329, 274, 686, 686, 686, 384, 384, 384, 384, 384, 384, 494, 494, 494, 600}
                Exit Select
            Case pdfFontStyle.pdfItalic
                Me.arrFONT(Me.mvarFontCount).BaseFont = (Me.arrFONT(Me.mvarFontCount).BaseFont & ",Italic")
                objArray1 = New Object() {332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 600, 600, 250, 333, 713, 500, 549, 833, 778, 439, 333, 333, 500, 549, 250, 549, 250, 278, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 278, 278, 549, 549, 549, 444, 549, 722, 667, 722, 612, 611, 763, 603, 722, 333, 631, 722, 686, 889, 722, 722, 768, 741, 556, 592, 611, 690, 439, 768, 645, 795, 611, 333, 863, 333, 658, 500, 500, 631, 549, 549, 494, 439, 521, 411, 603, 329, 603, 549, 549, 576, 521, 549, 549, 521, 549, 603, 439, 576, 713, 686, 493, 686, 494, 480, 200, 480, 549, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 620, 247, 549, 167, 713, 500, 753, 753, 753, 753, 1042, 987, 603, 987, 603, 400, 549, 411, 549, 549, 713, 494, 460, 549, 549, 549, 549, 1000, 603, 1000, 658, 823, 686, 795, 987, 768, 768, 823, 768, 768, 713, 713, 713, 713, 713, 713, 713, 768, 713, 790, 790, 890, 823, 549, 250, 713, 603, 603, 1042, 987, 603, 987, 603, 494, 329, 790, 790, 786, 713, 384, 384, 384, 384, 384, 384, 494, 494, 494, 494, 600, 329, 274, 686, 686, 686, 384, 384, 384, 384, 384, 384, 494, 494, 494, 600}
                Exit Select
            Case (pdfFontStyle.pdfItalic Or pdfFontStyle.pdfBold)
                Me.arrFONT(Me.mvarFontCount).BaseFont = (Me.arrFONT(Me.mvarFontCount).BaseFont & ",BoldItalic")
                objArray1 = New Object() {332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 332, 600, 600, 250, 333, 713, 500, 549, 833, 778, 439, 333, 333, 500, 549, 250, 549, 250, 278, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 278, 278, 549, 549, 549, 444, 549, 722, 667, 722, 612, 611, 763, 603, 722, 333, 631, 722, 686, 889, 722, 722, 768, 741, 556, 592, 611, 690, 439, 768, 645, 795, 611, 333, 863, 333, 658, 500, 500, 631, 549, 549, 494, 439, 521, 411, 603, 329, 603, 549, 549, 576, 521, 549, 549, 521, 549, 603, 439, 576, 713, 686, 493, 686, 494, 480, 200, 480, 549, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 620, 247, 549, 167, 713, 500, 753, 753, 753, 753, 1042, 987, 603, 987, 603, 400, 549, 411, 549, 549, 713, 494, 460, 549, 549, 549, 549, 1000, 603, 1000, 658, 823, 686, 795, 987, 768, 768, 823, 768, 768, 713, 713, 713, 713, 713, 713, 713, 768, 713, 790, 790, 890, 823, 549, 250, 713, 603, 603, 1042, 987, 603, 987, 603, 494, 329, 790, 790, 786, 713, 384, 384, 384, 384, 384, 384, 494, 494, 494, 494, 600, 329, 274, 686, 686, 686, 384, 384, 384, 384, 384, 384, 494, 494, 494, 600}
                Exit Select
        End Select
        Dim num1 As Integer = 0
        Do
            Me.arrFONT(Me.mvarFontCount).Widths(num1) = IntegerType.FromObject(objArray1(num1))
            num1 += 1
        Loop While (num1 <= 255)
        Me.arrFONT(Me.mvarFontCount).FontAlias = Strings.UCase(Me.arrFONT(Me.mvarFontCount).BaseFont)
    End Sub

    Private Sub prCreateFontTimes(ByVal Style As pdfFontStyle)
        Dim objArray1 As Object()
        Me.mvarFontCount += 1
        Me.arrFONT = DirectCast(Utils.CopyArray(DirectCast(Me.arrFONT, Array), New FontDescriptor((Me.mvarFontCount + 1) - 1) {}), FontDescriptor())
        Dim num2 As Integer = Me.mvarFontCount
        Me.arrFONT(num2).Initialize()
        Me.arrFONT(num2).BaseFont = "TimesNewRoman"
        Me.arrFONT(num2).TrueTypeFont = False
        Me.arrFONT(num2).Height = 1107
        Me.arrFONT(num2).BaseLine = -216
        Select Case (Style And (pdfFontStyle.pdfItalic Or pdfFontStyle.pdfBold))
            Case pdfFontStyle.pdfNormal
                objArray1 = New Object() {333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 250, 333, 408, 500, 500, 833, 778, 180, 333, 333, 500, 564, 250, 333, 250, 278, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 278, 278, 564, 564, 564, 444, 921, 722, 667, 667, 722, 611, 556, 722, 722, 333, 389, 722, 611, 889, 722, 722, 556, 722, 667, 556, 611, 722, 722, 944, 722, 722, 611, 333, 278, 333, 469, 500, 333, 444, 500, 444, 500, 444, 333, 500, 500, 278, 278, 500, 278, 778, 500, 500, 500, 500, 333, 389, 278, 500, 500, 722, 500, 500, 444, 480, 200, 480, 541, 778, 500, 778, 333, 500, 444, 1000, 500, 500, 333, 1000, 556, 333, 889, 778, 611, 778, 778, 333, 333, 444, 444, 350, 500, 1000, 333, 980, 389, 333, 722, 778, 444, 722, 250, 333, 500, 500, 500, 500, 200, 500, 333, 760, 276, 500, 564, 333, 760, 500, 400, 549, 300, 300, 333, 576, 453, 250, 333, 300, 310, 500, 750, 750, 750, 444, 722, 722, 722, 722, 722, 722, 889, 667, 611, 611, 611, 611, 333, 333, 333, 333, 722, 722, 722, 722, 722, 722, 722, 564, 722, 722, 722, 722, 722, 722, 556, 500, 444, 444, 444, 444, 444, 444, 667, 444, 444, 444, 444, 444, 278, 278, 278, 278, 500, 500, 500, 500, 500, 500, 500, 549, 500, 500, 500, 500, 500, 500, 500, 500}
                Exit Select
            Case pdfFontStyle.pdfBold
                Me.arrFONT(Me.mvarFontCount).BaseFont = (Me.arrFONT(Me.mvarFontCount).BaseFont & ",Bold")
                objArray1 = New Object() {333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 250, 333, 555, 500, 500, 1000, 833, 278, 333, 333, 500, 570, 250, 333, 250, 278, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 333, 333, 570, 570, 570, 500, 930, 722, 667, 722, 722, 667, 611, 778, 778, 389, 500, 778, 667, 944, 722, 778, 611, 778, 722, 556, 667, 722, 722, 1000, 722, 722, 667, 333, 278, 333, 581, 500, 333, 500, 556, 444, 556, 444, 333, 500, 556, 278, 333, 556, 278, 833, 556, 500, 556, 556, 444, 389, 333, 556, 500, 722, 500, 500, 444, 394, 220, 394, 520, 778, 500, 778, 333, 500, 500, 1000, 500, 500, 333, 1000, 556, 333, 1000, 778, 667, 778, 778, 333, 333, 500, 500, 350, 500, 1000, 333, 1000, 389, 333, 722, 778, 444, 722, 250, 333, 500, 500, 500, 500, 220, 500, 333, 747, 300, 500, 570, 333, 747, 500, 400, 549, 300, 300, 333, 576, 540, 250, 333, 300, 330, 500, 750, 750, 750, 500, 722, 722, 722, 722, 722, 722, 1000, 722, 667, 667, 667, 667, 389, 389, 389, 389, 722, 722, 778, 778, 778, 778, 778, 570, 778, 722, 722, 722, 722, 722, 611, 556, 500, 500, 500, 500, 500, 500, 722, 444, 444, 444, 444, 444, 278, 278, 278, 278, 500, 556, 500, 500, 500, 500, 500, 549, 500, 556, 556, 556, 556, 500, 556, 500}
                Exit Select
            Case pdfFontStyle.pdfItalic
                Me.arrFONT(Me.mvarFontCount).BaseFont = (Me.arrFONT(Me.mvarFontCount).BaseFont & ",Italic")
                objArray1 = New Object() {333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 250, 333, 420, 500, 500, 833, 778, 214, 333, 333, 500, 675, 250, 333, 250, 278, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 333, 333, 675, 675, 675, 500, 920, 611, 611, 667, 722, 611, 611, 722, 722, 333, 444, 667, 556, 833, 667, 722, 611, 722, 611, 500, 556, 722, 611, 833, 611, 556, 556, 389, 278, 389, 422, 500, 333, 500, 500, 444, 500, 444, 278, 500, 500, 278, 278, 444, 278, 722, 500, 500, 500, 500, 389, 389, 278, 500, 444, 667, 444, 444, 389, 400, 275, 400, 541, 778, 500, 778, 333, 500, 556, 889, 500, 500, 333, 1000, 500, 333, 944, 778, 556, 778, 778, 333, 333, 556, 556, 350, 500, 889, 333, 980, 389, 333, 667, 778, 389, 556, 250, 389, 500, 500, 500, 500, 275, 500, 333, 760, 276, 500, 675, 333, 760, 500, 400, 549, 300, 300, 333, 576, 523, 250, 333, 300, 310, 500, 750, 750, 750, 500, 611, 611, 611, 611, 611, 611, 889, 667, 611, 611, 611, 611, 333, 333, 333, 333, 722, 667, 722, 722, 722, 722, 722, 675, 722, 722, 722, 722, 722, 556, 611, 500, 500, 500, 500, 500, 500, 500, 667, 444, 444, 444, 444, 444, 278, 278, 278, 278, 500, 500, 500, 500, 500, 500, 500, 549, 500, 500, 500, 500, 500, 444, 500, 444}
                Exit Select
            Case (pdfFontStyle.pdfItalic Or pdfFontStyle.pdfBold)
                Me.arrFONT(Me.mvarFontCount).BaseFont = (Me.arrFONT(Me.mvarFontCount).BaseFont & ",BoldItalic")
                objArray1 = New Object() {333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 333, 250, 389, 555, 500, 500, 833, 778, 278, 333, 333, 500, 570, 250, 333, 250, 278, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 333, 333, 570, 570, 570, 500, 832, 667, 667, 667, 722, 667, 667, 722, 778, 389, 500, 667, 611, 889, 722, 722, 611, 722, 667, 556, 611, 722, 667, 889, 667, 611, 611, 333, 278, 333, 570, 500, 333, 500, 500, 444, 500, 444, 333, 500, 556, 278, 278, 500, 278, 778, 556, 500, 500, 500, 389, 389, 278, 556, 444, 667, 500, 444, 389, 348, 220, 348, 570, 778, 500, 778, 333, 500, 500, 1000, 500, 500, 333, 1000, 556, 333, 944, 778, 611, 778, 778, 333, 333, 500, 500, 350, 500, 1000, 333, 1000, 389, 333, 722, 778, 389, 611, 250, 389, 500, 500, 500, 500, 220, 500, 333, 747, 266, 500, 606, 333, 747, 500, 400, 549, 300, 300, 333, 576, 500, 250, 333, 300, 300, 500, 750, 750, 750, 500, 667, 667, 667, 667, 667, 667, 944, 667, 667, 667, 667, 667, 389, 389, 389, 389, 722, 722, 722, 722, 722, 722, 722, 570, 722, 722, 722, 722, 722, 611, 611, 500, 500, 500, 500, 500, 500, 500, 722, 444, 444, 444, 444, 444, 278, 278, 278, 278, 500, 556, 500, 500, 500, 500, 500, 549, 500, 556, 556, 556, 556, 444, 500, 444}
                Exit Select
        End Select
        Dim num1 As Integer = 0
        Do
            Me.arrFONT(Me.mvarFontCount).Widths(num1) = IntegerType.FromObject(objArray1(num1))
            num1 += 1
        Loop While (num1 <= 255)
        Me.arrFONT(Me.mvarFontCount).FontAlias = Strings.UCase(Me.arrFONT(Me.mvarFontCount).BaseFont)
    End Sub

    Private Sub prCreateFontZapfDingbats()
        Me.mvarFontCount += 1
        Me.arrFONT = DirectCast(Utils.CopyArray(DirectCast(Me.arrFONT, Array), New FontDescriptor((Me.mvarFontCount + 1) - 1) {}), FontDescriptor())
        Dim num2 As Integer = Me.mvarFontCount
        Me.arrFONT(num2).Initialize()
        Me.arrFONT(num2).TrueTypeFont = False
        Me.arrFONT(num2).BaseFont = "ZapfDingbats"
        Me.arrFONT(num2).Height = 1221
        Me.arrFONT(num2).BaseLine = -221
        Dim objArray1 As Object() = New Object() {600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 278, 974, 961, 974, 980, 719, 789, 790, 791, 690, 960, 939, 549, 855, 911, 933, 911, 945, 974, 755, 846, 762, 761, 571, 677, 763, 760, 759, 754, 494, 552, 537, 577, 692, 786, 788, 788, 790, 793, 794, 816, 823, 789, 841, 823, 833, 816, 831, 923, 744, 723, 749, 790, 792, 695, 776, 768, 792, 759, 707, 708, 682, 701, 826, 815, 789, 789, 707, 687, 696, 689, 786, 787, 713, 791, 785, 791, 873, 761, 762, 762, 759, 759, 892, 892, 788, 784, 438, 138, 277, 415, 392, 392, 668, 668, 0, 390, 390, 317, 317, 276, 276, 509, 509, 410, 410, 234, 234, 334, 334, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 732, 544, 544, 910, 667, 760, 760, 776, 595, 694, 626, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 7, 88, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 788, 894, 838, 1016, 458, 748, 924, 748, 918, 927, 928, 928, 834, 873, 828, 924, 924, 917, 930, 931, 463, 883, 836, 836, 867, 867, 696, 696, 874, 0, 874, 760, 946, 771, 865, 771, 888, 967, 888, 831, 873, 927, 970, 918, 0}
        Dim num1 As Integer = 0
        Do
            Me.arrFONT(Me.mvarFontCount).Widths(num1) = IntegerType.FromObject(objArray1(num1))
            num1 += 1
        Loop While (num1 <= 255)
        Me.arrFONT(Me.mvarFontCount).FontAlias = Strings.UCase(Me.arrFONT(Me.mvarFontCount).BaseFont)
    End Sub
    Private Sub prCreateFontTrueType(ByVal Style As pdfFontStyle, ByVal strFont As String)
        '--> Cette fonction permet de d�finir un nouveau style pour une police d�j� enregistr�e
        '    Le style est celui d�fini de mani�re logicielle
        Dim NumFont As Integer = 0
        Dim num1 As Integer
        '-> on pointe sur la font truetype normale existante
        For num1 = 1 To Me.mvarFontCount
            If Me.arrFONT(num1).FontAlias = Entry(1, strFont, ",") Then
                NumFont = num1
                Exit For
            End If
        Next

        '-> on cr�e la nouvelle font en s'appuyant sur la normale
        Me.mvarFontCount += 1
        Me.arrFONT = DirectCast(Utils.CopyArray(DirectCast(Me.arrFONT, Array), New FontDescriptor((Me.mvarFontCount + 1) - 1) {}), FontDescriptor())
        Dim num2 As Integer = Me.mvarFontCount
        Me.arrFONT(num2).Initialize()
        Me.arrFONT(num2).BaseFont = Me.arrFONT(NumFont).BaseFont
        Me.arrFONT(num2).BaseLine = Me.arrFONT(NumFont).BaseLine
        Me.arrFONT(num2).Embedded = Me.arrFONT(NumFont).Embedded
        Me.arrFONT(num2).FontAlias = Me.arrFONT(NumFont).FontAlias
        Me.arrFONT(num2).FontFile = Me.arrFONT(NumFont).FontFile
        Me.arrFONT(num2).FontStyle = Style
        Me.arrFONT(num2).Height = Me.arrFONT(NumFont).Height
        Me.arrFONT(num2).Length1 = Me.arrFONT(NumFont).Length1
        Me.arrFONT(num2).Param = Me.arrFONT(NumFont).Param
        Me.arrFONT(num2).TrueTypeFont = True
        Select Case (Style And (pdfFontStyle.pdfItalic Or pdfFontStyle.pdfBold))
            Case pdfFontStyle.pdfBold
                Me.arrFONT(Me.mvarFontCount).BaseFont = (Me.arrFONT(Me.mvarFontCount).BaseFont & ",Bold")
                Exit Select
            Case pdfFontStyle.pdfItalic
                Me.arrFONT(Me.mvarFontCount).BaseFont = (Me.arrFONT(Me.mvarFontCount).BaseFont & ",Italic")
                Exit Select
            Case (pdfFontStyle.pdfItalic Or pdfFontStyle.pdfBold)
                Me.arrFONT(Me.mvarFontCount).BaseFont = (Me.arrFONT(Me.mvarFontCount).BaseFont & ",BoldItalic")
                Exit Select
        End Select
        num1 = 0
        Do
            Me.arrFONT(Me.mvarFontCount).Widths(num1) = Me.arrFONT(NumFont).Widths(num1)
            num1 += 1
        Loop While (num1 <= 255)
        Me.arrFONT(Me.mvarFontCount).Widths(128) = CInt(Me.arrFONT(Me.mvarFontCount).Widths(128) / 2)
        Me.arrFONT(Me.mvarFontCount).FontAlias = Strings.UCase(Me.arrFONT(Me.mvarFontCount).BaseFont)

    End Sub

    Private Function prDrawCode128(ByVal Code As String, ByVal X As Single, ByVal Y As Single, Optional ByVal ShowCode As Boolean = True, Optional ByVal ZoomFactor As Single = 1.0!, Optional ByVal Height As Single = 0.0!) As Single
        Dim flag1 As Boolean
        Dim single3 As Single
        Dim single4 As Single
        Dim objArray1 As Object() = New Object() {"11011001100", "11001101100", "11001100110", "10010011000", "10010001100", "10001001100", "10011001000", "10011000100", "10001100100", "11001001000", "11001000100", "11000100100", "10110011100", "10011011100", "10011001110", "10111001100", "10011101100", "10011100110", "11001110010", "11001011100", "11001001110", "11011100100", "11001110100", "11101101110", "11101001100", "11100101100", "11100100110", "11101100100", "11100110100", "11100110010", "11011011000", "11011000110", "11000110110", "10100011000", "10001011000", "10001000110", "10110001000", "10001101000", "10001100010", "11010001000", "11000101000", "11000100010", "10110111000", "10110001110", "10001101110", "10111011000", "10111000110", "10001110110", "11101110110", "11010001110", "11000101110", "11011101000", "11011100010", "11011101110", "11101011000", "11101000110", "11100010110", "11101101000", "11101100010", "11100011010", "11101111010", "11001000010", "11110001010", "10100110000", "10100001100", "10010110000", "10010000110", "10000101100", "10000100110", "10110010000", "10110000100", "10011010000", "10011000010", "10000110100", "10000110010", "11000010010", "11001010000", "11110111010", "11000010100", "10001111010", "10100111100", "10010111100", "10010011110", "10111100100", "10011110100", "10011110010", "11110100100", "11110010100", "11110010010", "11011011110", "11011110110", "11110110110", "10101111000", "10100011110", "10001011110", "10111101000", "10111100010", "11110101000", "11110100010", "10111011110", "10111101110", "11101011110", "11110101110", "11010000100", "11010010000", "11010011100", "1100011101011"}
        Dim num8 As Integer = Strings.Len(Code)
        Dim num3 As Integer = 1
        Do While (num3 <= num8)
            Dim num7 As Integer = Strings.Asc(Strings.Mid(Code, num3, 1))
            If (((num7 < 32) OrElse (num7 > 126)) AndAlso (num7 <> 198)) Then
                flag1 = True
                Exit Do
            End If
            num3 += 1
        Loop
        If Not ((Strings.Len(Code) > 0) And Not flag1) Then
            Dim single1 As Single
            Return single1
        End If
        Dim text1 As String = ""
        Dim num1 As Long = 0
        Dim num4 As Integer = 0
        Dim flag2 As Boolean = True
        num3 = 1
        Do While (num3 <= Strings.Len(Code))
            Dim num2 As Integer
            Dim num5 As Integer
            If flag2 Then
                num5 = IntegerType.FromObject(Interaction.IIf(((num3 = 1) Or ((num3 + 3) = Strings.Len(Code))), 4, 6))
                num5 -= 1
                If ((num3 + num5) <= Strings.Len(Code)) Then
                    Do While (num5 >= 0)
                        If ((Strings.Asc(Strings.Mid(Code, (num3 + num5), 1)) < 48) Or (Strings.Asc(Strings.Mid(Code, (num3 + num5), 1)) > 57)) Then
                            Exit Do
                        End If
                        num5 -= 1
                    Loop
                End If
                If (num5 < 0) Then
                    If (num3 = 1) Then
                        text1 = StringType.FromObject(objArray1(105))
                    Else
                        text1 = (text1 & StringType.FromObject(objArray1(99)))
                        num4 += 1
                        num1 = (num1 + (num4 * 99))
                    End If
                    flag2 = False
                ElseIf (num3 = 1) Then
                    text1 = StringType.FromObject(objArray1(104))
                End If
            End If
            If Not flag2 Then
                num5 = 2
                num5 -= 1
                If ((num3 + num5) <= Strings.Len(Code)) Then
                    Do While (num5 >= 0)
                        If ((Strings.Asc(Strings.Mid(Code, (num3 + num5), 1)) < 48) Or (Strings.Asc(Strings.Mid(Code, (num3 + num5), 1)) > 57)) Then
                            Exit Do
                        End If
                        num5 -= 1
                    Loop
                End If
                If (num5 < 0) Then
                    num2 = CInt(Math.Round(Conversion.Val(Strings.Mid(Code, num3, 2))))
                    text1 = (text1 & StringType.FromObject(objArray1(num2)))
                    num4 += 1
                    num1 = (num1 + (num4 * num2))
                    num3 = (num3 + 2)
                Else
                    text1 = (text1 & StringType.FromObject(objArray1(100)))
                    num4 += 1
                    num1 = (num1 + (num4 * 100))
                    flag2 = True
                End If
            End If
            If flag2 Then
                num2 = (Strings.Asc(Strings.Mid(Code, num3, 1)) - 32)
                text1 = (text1 & StringType.FromObject(objArray1(num2)))
                num4 += 1
                num1 = (num1 + (num4 * num2))
                num3 += 1
            End If
            num1 = (num1 Mod 103)
        Loop
        text1 = (text1 & StringType.FromObject(objArray1(CInt(num1))) & StringType.FromObject(objArray1(106)))
        If (ZoomFactor < 0.8) Then
            ZoomFactor = 0.8!
        End If
        If (ZoomFactor > 2) Then
            ZoomFactor = 2.0!
        End If
        Dim single2 As Single = CSng(((0.72 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        If ShowCode Then
            single3 = CSng(((4.68 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        End If
        If (Height = 0.0!) Then
            single4 = CSng(((14.4 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        Else
            single4 = (Height - single3)
        End If
        If (single4 <= 0.0!) Then
            single4 = single2
        End If
        Dim single6 As Single = (X + (single2 * 10.0!))
        Dim num6 As Integer = Strings.Len(text1)
        num3 = 1
        Do While (num3 <= num6)
            If (StringType.StrCmp(Strings.Mid(text1, num3, 1), "1", False) = 0) Then
                Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {single6, Y, single2, single4}))
                Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
            End If
            single6 = (single6 + single2)
            num3 += 1
        Loop
        Dim single5 As Single = (single6 + (single2 * 9.0!))
        If ShowCode Then
            Me.SetFont("CourierNew", CSng((10.080000000000002 * ZoomFactor)), pdfFontStyle.pdfNormal)
            Me.DrawText((X + ((single5 - X) / 2.0!)), ((Y + single4) + CSng((1.6 * single3))), Code, pdfTextAlignment.pdfCenter, 0.0!)
        End If
        Return (single5 - X)
    End Function

    Private Function prDrawCODE39(ByVal Code As String, ByVal X As Single, ByVal Y As Single, Optional ByVal ShowCode As Boolean = True, Optional ByVal ZoomFactor As Single = 1.0!, Optional ByVal Height As Single = 0.0!, Optional ByVal AddCheck As Boolean = False) As Single
        Dim single3 As Single
        Dim single4 As Single
        Dim objArray1 As Object() = New Object() {"bwbWBwBwb", "BwbWbwbwB", "bwBWbwbwB", "BwBWbwbwb", "bwbWBwbwB", "BwbWBwbwb", "bwBWBwbwb", "bwbWbwBwB", "BwbWbwBwb", "bwBWbwBwb", "BwbwbWbwB", "bwBwbWbwB", "BwBwbWbwb", "bwbwBWbwB", "BwbwBWbwb", "bwBwBWbwb", "bwbwbWBwB", "BwbwbWBwb", "bwBwbWBwb", "bwbwBWBwb", "BwbwbwbWB", "bwBwbwbWB", "BwBwbwbWb", "bwbwBwbWB", "BwbwBwbWb", "bwBwBwbWb", "bwbwbwBWB", "BwbwbwBWb", "bwBwbwBWb", "bwbwBwBWb", "BWbwbwbwB", "bWBwbwbwB", "BWBwbwbwb", "bWbwBwbwB", "BWbwBwbwb", "bWBwBwbwb", "bWbwbwBwB", "BWbwbwBwb", "bWBwbwBwb", "bWbWbWbwb", "bWbWbwbWb", "bWbwbWbWb", "bwbWbWbWb", "bWbwBwBwb"}
        If (ZoomFactor < 0.8) Then
            ZoomFactor = 0.8!
        End If
        If (ZoomFactor > 2) Then
            ZoomFactor = 2.0!
        End If
        Dim single2 As Single = CSng(((0.72 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        If ShowCode Then
            single3 = CSng(((4.68 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        End If
        If (Height = 0.0!) Then
            single4 = CSng(((14.4 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        Else
            single4 = (Height - single3)
        End If
        If (single4 <= 0.0!) Then
            single4 = single2
        End If
        Dim single6 As Single = (X + (single2 * 10.0!))
        Dim num7 As Integer = Strings.Len(Code)
        Dim num3 As Integer = 1
        Do While (num3 <= num7)
            Dim text5 As String
            Dim text4 As String = Strings.Mid(Code, num3, 1)
            Dim text6 As String = text4
            If (((((StringType.StrCmp(text6, "0", False) >= 0) AndAlso (StringType.StrCmp(text6, "9", False) <= 0)) OrElse ((StringType.StrCmp(text6, "A", False) >= 0) AndAlso (StringType.StrCmp(text6, "Z", False) <= 0))) OrElse ((StringType.StrCmp(text6, "-", False) = 0) OrElse (StringType.StrCmp(text6, ".", False) = 0))) OrElse ((((StringType.StrCmp(text6, " ", False) = 0) OrElse (StringType.StrCmp(text6, "$", False) = 0)) OrElse ((StringType.StrCmp(text6, "/", False) = 0) OrElse (StringType.StrCmp(text6, "+", False) = 0))) OrElse ((StringType.StrCmp(text6, "%", False) = 0) OrElse (StringType.StrCmp(text6, "*", False) = 0)))) Then
                text5 = text4
            ElseIf (StringType.StrCmp(text6, ChrW(0), False) = 0) Then
                text5 = "%U"
            ElseIf ((StringType.StrCmp(text6, ChrW(1), False) >= 0) AndAlso (StringType.StrCmp(text6, ChrW(26), False) <= 0)) Then
                text5 = ("$" & StringType.FromChar(Strings.Chr((Strings.Asc(text4) + 64))))
            ElseIf ((StringType.StrCmp(text6, ChrW(27), False) >= 0) AndAlso (StringType.StrCmp(text6, ChrW(31), False) <= 0)) Then
                text5 = ("%" & StringType.FromChar(Strings.Chr((Strings.Asc(text4) + 38))))
            ElseIf ((((StringType.StrCmp(text6, "!", False) >= 0) AndAlso (StringType.StrCmp(text6, ",", False) <= 0)) OrElse (StringType.StrCmp(text6, "/", False) = 0)) OrElse (StringType.StrCmp(text6, ":", False) = 0)) Then
                text5 = ("/" & StringType.FromChar(Strings.Chr((Strings.Asc(text4) + 32))))
            ElseIf ((StringType.StrCmp(text6, ";", False) >= 0) AndAlso (StringType.StrCmp(text6, "@", False) <= 0)) Then
                text5 = ("%" & StringType.FromChar(Strings.Chr((Strings.Asc(text4) + 11))))
            ElseIf ((StringType.StrCmp(text6, "[", False) >= 0) AndAlso (StringType.StrCmp(text6, "'", False) <= 0)) Then
                text5 = ("%" & StringType.FromChar(Strings.Chr((Strings.Asc(text4) - 15))))
            ElseIf ((StringType.StrCmp(text6, "a", False) >= 0) AndAlso (StringType.StrCmp(text6, "z", False) <= 0)) Then
                text5 = ("+" & Strings.UCase(text4))
            ElseIf ((StringType.StrCmp(text6, "{", False) >= 0) AndAlso (StringType.StrCmp(text6, ChrW(127), False) <= 0)) Then
                text5 = ("%" & StringType.FromChar(Strings.Chr((Strings.Asc(text4) - 43))))
            End If
            If (num3 = 1) Then
                text5 = ("*" & text5)
            End If
            Dim num4 As Integer = 1
            Do While (num4 <= Strings.Len(text5))
                Dim flag1 As Boolean
                Dim num5 As Long
                Dim num2 As Integer = (Strings.InStr("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%*", Strings.Mid(text5, num4, 1), 0) - 1)
                If (num2 <> 43) Then
                    num5 = (num5 + num2)
                End If
                Dim text3 As String = StringType.FromObject(objArray1(num2))
                If (((num3 = Strings.Len(Code)) And (num4 = Strings.Len(text5))) And Not flag1) Then
                    If AddCheck Then
                        text3 = StringType.FromObject(ObjectType.StrCatObj(text3, objArray1(CInt((num5 Mod 43)))))
                    End If
                    text3 = StringType.FromObject(ObjectType.StrCatObj(text3, objArray1(43)))
                    flag1 = True
                End If
                Dim num6 As Integer = Strings.Len(text3)
                Dim num1 As Integer = 1
                Do While (num1 <= num6)
                    Dim text2 As String = Strings.Mid(text3, num1, 1)
                    If (StringType.StrCmp(text2, "b", False) = 0) Then
                        Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {single6, Y, single2, single4}))
                        Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
                        single6 = (single6 + single2)
                    ElseIf (StringType.StrCmp(text2, "B", False) = 0) Then
                        Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {single6, Y, (single2 * 3.0!), single4}))
                        Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
                        single6 = (single6 + (single2 * 3.0!))
                    ElseIf (StringType.StrCmp(text2, "w", False) = 0) Then
                        single6 = (single6 + single2)
                    ElseIf (StringType.StrCmp(text2, "W", False) = 0) Then
                        single6 = (single6 + (single2 * 3.0!))
                    End If
                    num1 += 1
                Loop
                single6 = (single6 + single2)
                num4 += 1
            Loop
            num3 += 1
        Loop
        Dim single5 As Single = (single6 + (single2 * 9.0!))
        If ShowCode Then
            Me.SetFont("CourierNew", CSng((10.080000000000002 * ZoomFactor)), pdfFontStyle.pdfNormal)
            Me.DrawText((X + ((single5 - X) / 2.0!)), ((Y + single4) + CSng((1.6 * single3))), Code, pdfTextAlignment.pdfCenter, 0.0!)
        End If
        Return (single5 - X)
    End Function

    Private Function prDrawEAN13(ByVal Code As String, ByVal X As Single, ByVal Y As Single, Optional ByVal ShowCode As Boolean = True, Optional ByVal ZoomFactor As Single = 1.0!, Optional ByVal Height As Single = 0.0!) As Single
        Dim single3 As Single
        Dim single4 As Single
        Dim single7 As Single
        Dim text2 As String
        Dim objArray1 As Object() = New Object() {"0001101", "0011001", "0010011", "0111101", "0100011", "0110001", "0101111", "0111011", "0110111", "0001011"}
        Dim objArray2 As Object() = New Object() {"0100111", "0110011", "0011011", "0100001", "0011101", "0111001", "0000101", "0010001", "0001001", "0010111"}
        Dim objArray3 As Object() = New Object() {"AAAAAA", "AABABB", "AABBAB", "AABBBA", "ABAABB", "ABBAAB", "ABBBAA", "ABABAB", "ABABBA", "ABBABA"}
        If (ZoomFactor < 0.8) Then
            ZoomFactor = 0.8!
        End If
        If (ZoomFactor > 2) Then
            ZoomFactor = 2.0!
        End If
        Dim num2 As Integer = 0
        Dim num1 As Integer = 11
        Do
            num2 = CInt(Math.Round(CDbl(((num2 + (3 * Conversion.Val(Strings.Mid(Code, (num1 + 1), 1)))) + Conversion.Val(Strings.Mid(Code, num1, 1))))))
            num1 = (num1 + -2)
        Loop While (num1 >= 1)
        Dim num3 As Integer = CInt(Math.Round(Conversion.Val(Strings.Left(Code, 1))))
        Dim text4 As String = Strings.Mid(Code, 2, 6)
        Dim text5 As String = Strings.Mid(Code, 8, 5)
        text5 = (text5 & Strings.Trim(Conversion.Str(((10 - (num2 Mod 10)) Mod 10))))
        Dim single2 As Single = CSng(((0.93599999999999994 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        If ShowCode Then
            single3 = CSng(((4.68 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        End If
        If (Height = 0.0!) Then
            single4 = CSng(((64.8 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        Else
            single4 = (Height - single3)
        End If
        If (single4 <= 0.0!) Then
            single4 = single2
        End If
        Dim single5 As Single = (single2 * 11.0!)
        Dim single6 As Single = (single2 * 7.0!)
        If ShowCode Then
            single7 = (X + single5)
            Me.SetFont("CourierNew", CSng((10.080000000000002 * ZoomFactor)), pdfFontStyle.pdfNormal)
            Me.DrawText((single7 - (single2 * 8.0!)), ((Y + single4) + CSng((1.6 * single3))), Strings.Mid(Code, 1, 1), pdfTextAlignment.pdfAlignLeft, 0.0!)
            Me.DrawText((single7 + (single2 * 24.0!)), ((Y + single4) + CSng((1.6 * single3))), text4, pdfTextAlignment.pdfCenter, 0.0!)
            Me.DrawText((single7 + (single2 * 71.0!)), ((Y + single4) + CSng((1.6 * single3))), text5, pdfTextAlignment.pdfCenter, 0.0!)
        End If
        single7 = (X + single5)
        Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {single7, Y, single2, (single4 + single3)}))
        Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
        Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {(single7 + (2.0! * single2)), Y, single2, (single4 + single3)}))
        Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
        single7 = ((X + single5) + (single2 * 45.0!))
        Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {(single7 + single2), Y, single2, (single4 + single3)}))
        Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
        Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {(single7 + (3.0! * single2)), Y, single2, (single4 + single3)}))
        Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
        single7 = ((X + single5) + (single2 * 92.0!))
        Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {single7, Y, single2, (single4 + single3)}))
        Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
        Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {(single7 + (2.0! * single2)), Y, single2, (single4 + single3)}))
        Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
        single7 = (X + single5)
        num1 = 1
Label_0549:
        If (StringType.StrCmp(Strings.Mid(StringType.FromObject(objArray3(num3)), num1, 1), "A", False) = 0) Then
            text2 = StringType.FromObject(objArray1(CInt(Math.Round(Conversion.Val(Strings.Mid(text4, num1, 1))))))
        Else
            text2 = StringType.FromObject(objArray2(CInt(Math.Round(Conversion.Val(Strings.Mid(text4, num1, 1))))))
        End If
        Dim text3 As String = StringType.FromObject(objArray2(CInt(Math.Round(Conversion.Val(Strings.Mid(text5, num1, 1))))))
        Dim num4 As Integer = 1
        Do While True
            If (StringType.StrCmp(Strings.Mid(text2, num4, 1), "1", False) = 0) Then
                Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {(single7 + (((3 + (num4 - 1)) + ((num1 - 1) * 7)) * single2)), Y, single2, single4}))
                Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
            End If
            If (StringType.StrCmp(Strings.Mid(text3, (8 - num4), 1), "1", False) = 0) Then
                Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {(single7 + (((50 + (num4 - 1)) + ((num1 - 1) * 7)) * single2)), Y, single2, single4}))
                Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
            End If
            num4 += 1
            If (num4 > 7) Then
                num1 += 1
                If (num1 > 6) Then
                    Return ((single5 + single6) + (single2 * 95.0!))
                End If
                GoTo Label_0549
            End If
        Loop
    End Function

    Private Function prDrawI25(ByVal Code As String, ByVal X As Single, ByVal Y As Single, Optional ByVal ShowCode As Boolean = True, Optional ByVal ZoomFactor As Single = 1.0!, Optional ByVal Height As Single = 0.0!, Optional ByVal AddCheck As Boolean = False) As Single
        Dim num1 As Integer
        Dim num3 As Integer
        Dim single4 As Single
        Dim single5 As Single
        Dim text4 As String
        Dim objArray1 As Object() = New Object() {"nnwwn", "wnnnw", "nwnnw", "wwnnn", "nnwnw", "wnwnn", "nwwnn", "nnnww", "wnnwn", "nwnwn"}
        Dim text6 As String = "nnnn"
        Dim text3 As String = "wnnn"
        If (ZoomFactor < 0.8) Then
            ZoomFactor = 0.8!
        End If
        If (ZoomFactor > 2) Then
            ZoomFactor = 2.0!
        End If
        Dim single3 As Single = CSng(((0.72 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        If ShowCode Then
            single4 = CSng(((4.68 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        End If
        If (Height = 0.0!) Then
            single5 = CSng(((14.4 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        Else
            single5 = (Height - single4)
        End If
        If (single5 <= 0.0!) Then
            single5 = single3
        End If
        Dim single7 As Single = (X + (single3 * 5.0!))
        Dim text7 As String = Code
        If AddCheck Then
            Dim num4 As Long
            Dim num7 As Integer = Strings.Len(Code)
            num3 = 1
            Do While (num3 <= num7)
                Dim num2 As Integer = CInt(Math.Round(Conversion.Val(Strings.Mid(Code, ((Strings.Len(Code) - num3) + 1), 1))))
                If ((num3 Mod 2) = 0) Then
                    num4 = (num4 + num2)
                Else
                    num4 = (num4 + (3 * num2))
                End If
                num3 += 1
            Loop
            Code = (Code & StringType.FromChar(Strings.Chr(CInt((48 + ((10 - (num4 Mod 10)) Mod 10))))))
        End If
        If ((Strings.Len(Code) Mod 2) <> 0) Then
            Code = ("0" & Code)
        End If
        Dim num6 As Integer = (Strings.Len(Code) - 1)
        num3 = 1
        Do While (num3 <= num6)
            Dim text1 As String = StringType.FromObject(objArray1(CInt(Math.Round(Conversion.Val(Strings.Mid(Code, num3, 1))))))
            Dim text5 As String = StringType.FromObject(objArray1(CInt(Math.Round(Conversion.Val(Strings.Mid(Code, (num3 + 1), 1))))))
            num1 = 1
            Do
                text4 = (text4 & Strings.Mid(text1, num1, 1) & Strings.Mid(text5, num1, 1))
                num1 += 1
            Loop While (num1 <= 5)
            num3 = (num3 + 2)
        Loop
        text4 = (text6 & text4 & text3)
        Dim num5 As Integer = Strings.Len(text4)
        num1 = 1
        Do While (num1 <= num5)
            Dim single2 As Single
            Dim text2 As String = Strings.Mid(text4, num1, 1)
            If (StringType.StrCmp(text2, "w", False) = 0) Then
                single2 = (3.0! * single3)
            ElseIf (StringType.StrCmp(text2, "n", False) = 0) Then
                single2 = single3
            End If
            If ((num1 Mod 2) <> 0) Then
                Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {single7, Y, single2, single5}))
                Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
            End If
            single7 = (single7 + single2)
            num1 += 1
        Loop
        Dim single6 As Single = (single7 + (single3 * 5.0!))
        If ShowCode Then
            Me.SetFont("CourierNew", CSng((10.080000000000002 * ZoomFactor)), pdfFontStyle.pdfNormal)
            Me.DrawText((X + ((single6 - X) / 2.0!)), ((Y + single5) + CSng((1.6 * single4))), text7, pdfTextAlignment.pdfCenter, 0.0!)
        End If
        Return (single6 - X)
    End Function

    Private Function prDrawPOSTNET(ByVal Code As String, ByVal X As Single, ByVal Y As Single, Optional ByVal ShowCode As Boolean = True, Optional ByVal ZoomFactor As Single = 1.0!) As Single
        Dim num2 As Integer
        Dim num3 As Integer
        Dim single3 As Single
        Dim objArray1 As Object() = New Object() {"11000", "00011", "00101", "00110", "01001", "01010", "01100", "10001", "10010", "10100"}
        Dim text4 As String = "1"
        Dim text2 As String = "1"
        If (ZoomFactor < 0.8) Then
            ZoomFactor = 0.8!
        End If
        If (ZoomFactor > 2) Then
            ZoomFactor = 2.0!
        End If
        Dim single2 As Single = CSng(((1.44 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        Dim single4 As Single = CSng(((9 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        Dim single5 As Single = CSng(((3.6 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        Dim single7 As Single = CSng(((3.6 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        If ShowCode Then
            single3 = CSng(((4.68 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        End If
        Dim single6 As Single = CSng(((14.4 * ZoomFactor) / CDbl(Me.mvarKUnit)))
        If (single6 <= 0.0!) Then
            single6 = single2
        End If
        Dim single9 As Single = (X + (single2 * 5.0!))
        Dim text5 As String = Code
        Dim text3 As String = text4
        Dim num4 As Integer = 1
        Do
            num3 = CInt(Math.Round(Conversion.Val(Strings.Mid(Code, num4, 1))))
            num2 = (num2 + num3)
            text3 = StringType.FromObject(ObjectType.StrCatObj(text3, objArray1(num3)))
            num4 += 1
        Loop While (num4 <= 5)
        If (Strings.Len(Code) = 10) Then
            num4 = 7
            Do
                num3 = CInt(Math.Round(Conversion.Val(Strings.Mid(Code, num4, 1))))
                num2 = (num2 + num3)
                text3 = StringType.FromObject(ObjectType.StrCatObj(text3, objArray1(num3)))
                num4 += 1
            Loop While (num4 <= 10)
        End If
        num2 = (num2 Mod 10)
        If (num2 <> 0) Then
            num2 = (10 - num2)
        End If
        text3 = StringType.FromObject(ObjectType.StrCatObj(text3, objArray1(num2)))
        text3 = (text3 & text2)
        Dim num5 As Integer = Strings.Len(text3)
        Dim num1 As Integer = 1
        Do While (num1 <= num5)
            Dim text1 As String = Strings.Mid(text3, num1, 1)
            If (StringType.StrCmp(text1, "1", False) = 0) Then
                Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {single9, Y, single2, single4}))
                Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
            ElseIf (StringType.StrCmp(text1, "0", False) = 0) Then
                Me.prWriteObj(Me.prFormat("#k #k #k #k re", New Object() {single9, (Y + (single4 - single5)), single2, single5}))
                Me.prPath((pdfDrawOptions.pdfFilled Or pdfDrawOptions.pdfInvisible))
            End If
            single9 = (single9 + single7)
            num1 += 1
        Loop
        Dim single8 As Single = (single9 + (single2 * 5.0!))
        If ShowCode Then
            Me.SetFont("CourierNew", CSng((10.080000000000002 * ZoomFactor)), pdfFontStyle.pdfNormal)
            Me.DrawText((X + ((single8 - X) / 2.0!)), ((Y + single6) + single3), text5, pdfTextAlignment.pdfCenter, 0.0!)
        End If
        Return (single8 - X)
    End Function

    Private Sub prDrawTextLine(ByVal X As Single, ByVal Y As Single, ByVal xDim As Single, ByVal [Text] As String, Optional ByVal Indentation As Single = 0.0!, Optional ByVal Align As pdfTextAlignment = 1, Optional ByVal NewLine As Boolean = False)
        Dim num2 As Integer
        Dim single2 As Single
        Dim single3 As Single
        Dim single4 As Single
        Dim single5 As Single
        Dim single6 As Single
        Dim text1 As String
        Dim text2 As String
        If (StringType.StrCmp(Strings.Trim([Text]), "", False) = 0) Then
            GoTo Label_05A3
        End If
        If ((Align And pdfTextAlignment.pdfJustify) = pdfTextAlignment.pdfJustify) Then
            Do While (Strings.InStr([Text], "  ", 0) > 0)
                [Text] = Strings.Replace([Text], "  ", " ", 1, -1, 0)
            Loop
            Dim textArray1 As String() = Strings.Split([Text], " ", -1, 0)
            num2 = Information.UBound(textArray1, 1)
            Dim num3 As Integer = num2
            Dim num1 As Integer = 0
            Do While (num1 <= num3)
                single2 = (single2 + Me.TextLength(textArray1(num1)))
                num1 += 1
            Loop
            single4 = Me.TextLength(" ")
            single3 = (single2 + (single4 * num2))
        Else
            If (((Align And pdfTextAlignment.pdfFit) = pdfTextAlignment.pdfFit) Or ((Align And pdfTextAlignment.pdfForceFit) = pdfTextAlignment.pdfForceFit)) Then
                single5 = Me.mvarTextScaling
                Me.mvarTextScaling = 100.0!
            End If
            single2 = Me.TextLength([Text])
            single3 = single2
        End If
        If (Indentation <> 0.0!) Then
            single2 = (single2 + Indentation)
        End If
        Select Case (Align And (pdfTextAlignment.pdfForceFit Or (pdfTextAlignment.pdfFit Or (pdfTextAlignment.pdfJustify Or (pdfTextAlignment.pdfCenter Or (pdfTextAlignment.pdfAlignRight Or pdfTextAlignment.pdfAlignLeft))))))
            Case pdfTextAlignment.pdfJustify
                If (num2 >= 1) Then
                    Dim single1 As Single = ((xDim - single2) - (num2 * single4))
                    text1 = Me.prFormat("#k Tw", New Object() {(single1 / CSng(num2))})
                    text2 = Me.prFormat("#s Tw ", New Object() {Me.mvarWordSpacing})
                    single3 = (single3 + single1)
                End If
                GoTo Label_025D
            Case pdfTextAlignment.pdfFit, pdfTextAlignment.pdfForceFit
                Dim single7 As Single
                If (single2 > xDim) Then
                    single7 = ((100.0! * xDim) / single2)
                    text1 = Me.prFormat("#s Tz ", New Object() {single7})
                    text2 = Me.prFormat("#s Tz ", New Object() {single5})
                    GoTo Label_025D
                End If
                If ((Align And pdfTextAlignment.pdfForceFit) = pdfTextAlignment.pdfForceFit) Then
                    single7 = ((100.0! * xDim) / single2)
                    text1 = Me.prFormat("#s Tz ", New Object() {single7})
                    text2 = Me.prFormat("#s Tz ", New Object() {single5})
                End If
                GoTo Label_025D
            Case pdfTextAlignment.pdfAlignLeft
                GoTo Label_025D
            Case pdfTextAlignment.pdfAlignRight
                single6 = (xDim - single2)
                GoTo Label_025D
            Case pdfTextAlignment.pdfCenter
                single6 = ((xDim - single2) / 2.0!)
                Exit Select
        End Select
Label_025D:
        If (Indentation <> 0.0!) Then
            single6 = (single6 + Indentation)
        End If
        Dim single8 As Single = ((Me.mvarKUnit * Me.TextHeight) * Me.mvarLineSpacing)
        single6 = (Me.mvarKUnit * (X + single6))
        Dim text3 As String = Me.mvarColorFill
        Dim text4 As String = Me.mvarColorFillStencil
        Me.SetColorFill((Me.mvarColorText & ";" & Me.mvarColorTextStencil))
        Me.prWriteObj("BT")
        Me.prWriteObj(Me.prFormat("/#t #s Tf #s TL", New Object() {Me.mvarFontObj, Me.mvarFontSize, Me.mvarFontSize}))
        Me.prWriteObj(Me.prFormat("1 0 0 -1 #s #k Tm", New Object() {single6, Y}))
        If (StringType.StrCmp(text1, "", False) <> 0) Then
            Me.prWriteObj(text1)
        End If
        Me.prWriteObj(Me.prFormat("(#t) Tj", New Object() {Me.prToPdfStr([Text])}))
        If (StringType.StrCmp(text2, "", False) <> 0) Then
            Me.prWriteObj(text2)
        End If
        Me.prWriteObj("ET")
        Me.SetColorFill((text3 & ";" & text4))
        Dim flag3 As Boolean = ((Me.mvarFontStyle And pdfFontStyle.pdfUnderline) = pdfFontStyle.pdfUnderline)
        Dim flag1 As Boolean = ((Me.mvarFontStyle And pdfFontStyle.pdfOverLine) = pdfFontStyle.pdfOverLine)
        Dim flag2 As Boolean = ((Me.mvarFontStyle And pdfFontStyle.pdfStrikeOut) = pdfFontStyle.pdfStrikeOut)
        If ((flag3 Or flag1) Or flag2) Then
            text3 = Me.mvarColorLine
            text4 = Me.mvarColorLineStencil
            Me.prWriteObj("q")
            Me.SetColorLine((Me.mvarColorText & ";" & Me.mvarColorTextStencil))
            Me.prWriteObj(Me.prFormat("#s w ", New Object() {(Me.mvarFontSize / 16.0!)}))
            Me.prWriteObj(Me.prFormat("1 0 0 1 #s #k cm", New Object() {single6, Y}))
            If flag3 Then
                Me.prWriteObj(Me.prFormat("0 #s m #k #s l S", New Object() {(Me.mvarFontSize / 8.0!), single3, (Me.mvarFontSize / 8.0!)}))
            End If
            If flag1 Then
                Me.prWriteObj(Me.prFormat("0 #s m #k #s l S", New Object() {(-0.8 * Me.mvarFontSize), single3, (-0.8 * Me.mvarFontSize)}))
            End If
            If flag2 Then
                Me.prWriteObj(Me.prFormat("0 #s m #k #s l S", New Object() {(-0.3 * Me.mvarFontSize), single3, (-0.3 * Me.mvarFontSize)}))
            End If
            Me.SetColorLine((text3 & ";" & text4))
            Me.prWriteObj("Q")
        End If
Label_05A3:
        If NewLine Then
            Me.mvarCurrX = 0.0!
            Me.mvarCurrY = ((Me.mvarKUnit * Y) + single8)
        Else
            Me.mvarCurrX = (Me.mvarKUnit * (X + single3))
            Me.mvarCurrY = (Me.mvarKUnit * Y)
        End If
    End Sub

    Private Sub prDrawTextRtf(ByVal X As Single, ByVal Y As Single, ByRef RtfName As String)
        'Dim single2 As Single
        Dim single3 As Single
        'Dim single6 As Single
        Dim aSection As clsRtfSection
        Dim aLigne As clsRtfLigne
        Dim i As Integer
        Dim PosX As Double = 0
        Dim aStyle As vbPDF.pdfFontStyle
        Dim alignement As vbPDF.pdfTextAlignment

        'single3 = single2
        'Dim single8 As Single = ((Me.mvarKUnit * Me.TextHeight) * Me.mvarLineSpacing)
        'single6 = (Me.mvarKUnit * (X + single6))
        '-> on boucle sur l'objet rtf
        For i = 1 To RTFobj(RtfName).count
            PosX = 0
            aStyle = pdfFontStyle.pdfNormal
            aSection = RTFobj(RtfName)(i)
            If aSection.Text <> "" Then
                aLigne = RTFlignes(RtfName & aSection.Ligne)
                '-> si la cellule est en gras
                If aSection.Fnt.Bold Then aStyle = vbPDF.pdfFontStyle.pdfBold
                '-> si la cellule est en italique
                If aSection.Fnt.Italic Then aStyle = aStyle Or vbPDF.pdfFontStyle.pdfItalic
                '-> si la cellule est soulign�e
                If aSection.Fnt.Underline Then aStyle = aStyle Or vbPDF.pdfFontStyle.pdfUnderline
                SetFont(aSection.FntRef, aSection.Fnt.Size, aStyle)
                '-> on determine la position du texte sur la ligne
                Select Case aLigne.Align
                    Case 0 'gauche
                        alignement = pdfTextAlignment.pdfAlignLeft
                        PosX = aSection.PosX
                    Case 1 'droite
                        alignement = pdfTextAlignment.pdfAlignRight
                    Case 2 'centr�
                        alignement = pdfTextAlignment.pdfCenter
                        PosX = (-(aLigne.Width / 2 - (aSection.PosX + aSection.Width / 2))) * 2
                End Select
                aPrinter.SetColorText(System.Drawing.ColorTranslator.ToHtml(aSection.FntColor))
                'System.Drawing.ColorTranslator.FromHtml("#" & Hex$(aSection.FntColor))
                If Me.TextHeight < aLigne.Height Then
                    single3 = aLigne.Height - Me.TextHeight
                Else
                    single3 = 0
                End If
                If AscW(aSection.Text) <> 10 Then DrawTextBox(X, Y + aLigne.PosY + (aSection.Ligne * aLigne.Height / 14) + single3, aSection.WidthSection, aSection.HeightSection, aSection.Text, PosX, alignement)
                'If AscW(aSection.Text) <> 10 Then DrawTextBox(X, Y + aLigne.PosY + (aSection.Ligne * aSection.Height / 14), aSection.WidthSection, aSection.HeightSection, aSection.Text, PosX, alignement)
            End If
        Next '-> on a fini de boucler sur les objets
    End Sub

    Private Sub prEndObj()
        Me.prWriteObj("endobj")
    End Sub

    Private Function prFormat(ByVal Value As String, ByVal ParamArray Args As Object()) As String
        '--> cette procedure permet de formater une chaine selon l'argument
        '##
        '#d     pour les dates
        '#p     pour les chaines crypt�es
        '#t     pour le texte
        '# i, s pour les chiffres integer, single
        '#b     pour les bol�ans
        Do While (Strings.InStr(Value, "##", 0) > 0)
            Value = Strings.Replace(Value, "##", StringType.FromChar(Strings.Chr(255)), 1, -1, 0)
        Loop
        Do While (Strings.InStr(Value, "#n", 0) > 0)
            Value = Strings.Replace(Value, "#n", ChrW(13), 1, -1, 0)
        Loop
        Dim text4 As String = Value
        Dim num2 As Integer = 0
        num2 = Strings.InStr(text4, "#", 0)
        Dim num1 As Integer = 0
        Do While (num2 <> 0)
            Dim text2 As String
            Dim text3 As String = Strings.Mid(text4, num2, 2)
            Dim text5 As String = text3
            If (StringType.StrCmp(text5, "#i", False) = 0) Then
                text2 = StringType.FromObject(Conversion.Int(RuntimeHelpers.GetObjectValue(Args(num1))))
            ElseIf (StringType.StrCmp(text5, "#c", False) = 0) Then
                text2 = Strings.Replace(StringType.FromDouble(Math.Round(CDbl((SingleType.FromObject(Args(num1)) / 255.0!)), 3)), ",", ".", 1, -1, 0)
            ElseIf (StringType.StrCmp(text5, "#s", False) = 0) Then
                Dim objArray2 As Object() = New Object(2 - 1) {}
                Dim num3 As Integer = num1
                objArray2(0) = RuntimeHelpers.GetObjectValue(Args(num3))
                objArray2(1) = 3
                Dim objArray1 As Object() = objArray2
                Dim flagArray1 As Boolean() = New Boolean() {True, False}
                If flagArray1(0) Then
                    Args(num3) = RuntimeHelpers.GetObjectValue(objArray1(0))
                End If
                text2 = Strings.Replace(StringType.FromObject(LateBinding.LateGet(Nothing, GetType(Math), "Round", objArray1, Nothing, flagArray1)), ",", ".", 1, -1, 0)
            ElseIf (StringType.StrCmp(text5, "#k", False) = 0) Then
                text2 = Strings.Replace(StringType.FromObject(LateBinding.LateGet(Nothing, GetType(Math), "Round", New Object() {ObjectType.MulObj(Args(num1), Me.mvarKUnit), 3}, Nothing, Nothing)), ",", ".", 1, -1, 0)
            ElseIf (StringType.StrCmp(text5, "#t", False) = 0) Then
                text2 = StringType.FromObject(Args(num1))
            Else
                Dim buffer1 As Byte()
                If (StringType.StrCmp(text5, "#h", False) = 0) Then
                    text2 = StringType.FromObject(Args(num1))
                    If (StringType.StrCmp(text2, "", False) <> 0) Then
                        buffer1 = DirectCast(Me.prStr2Mtx(text2), Byte())
                        text2 = Me.prMtx2Str(DirectCast(Me.prToASCIIHex(buffer1), Byte()))
                    End If
                    text2 = ("<" & text2 & ">")
                ElseIf (StringType.StrCmp(text5, "#p", False) = 0) Then
                    text2 = StringType.FromObject(Args(num1))
                    If ((StringType.StrCmp(text2, "", False) <> 0) And Me.mvarEncryption) Then
                        buffer1 = DirectCast(Me.prStr2Mtx(text2), Byte())
                        text2 = Me.prMtx2Str(DirectCast(Me.prRC4(Me.zzFromId2Key(0), buffer1), Byte()))
                    End If
                    text2 = ("(" & Me.prToPdfStr(text2) & ")")
                ElseIf (StringType.StrCmp(text5, "#d", False) = 0) Then
                    '-> on met la donn�e au format date
                    text2 = ("D:" & Strings.Format(DateType.FromObject(Args(num1)), "yyyyMMddhhmmss"))
                    '-> si besoin on encrypte
                    If Me.mvarEncryption Then
                        buffer1 = DirectCast(Me.prStr2Mtx(text2), Byte())
                        text2 = Me.prMtx2Str(DirectCast(Me.prRC4(Me.zzFromId2Key(0), buffer1), Byte()))
                    End If
                ElseIf (StringType.StrCmp(text5, "#b", False) = 0) Then
                    text2 = StringType.FromObject(Interaction.IIf(BooleanType.FromObject(Args(num1)), "true", "false"))
                ElseIf (StringType.StrCmp(text5, "#C", False) = 0) Then
                    text2 = String.Concat(New String() {Strings.Replace(StringType.FromDouble(Math.Round((Conversion.Val(("&H" & Strings.Mid(StringType.FromObject(Args(num1)), 2, 2))) / 255), 3)), ",", ".", 1, -1, 0), " ", Strings.Replace(StringType.FromDouble(Math.Round((Conversion.Val(("&H" & Strings.Mid(StringType.FromObject(Args(num1)), 4, 2))) / 255), 3)), ",", ".", 1, -1, 0), " ", Strings.Replace(StringType.FromDouble(Math.Round((Conversion.Val(("&H" & Strings.Mid(StringType.FromObject(Args(num1)), 6, 2))) / 255), 3)), ",", ".", 1, -1, 0)})
                Else
                    text2 = text3
                    num1 -= 1
                End If
            End If
            text4 = (Strings.Left(text4, (num2 - 1)) & text2 & Strings.Mid(text4, (num2 + 2)))
            num1 += 1
            num2 = Strings.InStr((num2 + 2), text4, "#", 0)
            Application.DoEvents()
        Loop
        Do While (Strings.InStr(text4, StringType.FromChar(Strings.Chr(255)), 0) > 0)
            text4 = Strings.Replace(text4, StringType.FromChar(Strings.Chr(255)), "#", 1, -1, 0)
        Loop
        Return text4
    End Function

    Private Function prFromId2Key(ByVal ObjId As Integer) As String
        If (ObjId = 0) Then
            ObjId = Me.mvarIdObj
        End If
        Dim text1 As String = Strings.Right(("000000" & Strings.Trim(Conversion.Hex(ObjId))), 6)
        Dim textArray1 As String() = New String() {Me.mvarEncryptionKey, Conversions.ToString(Strings.Chr(CInt(Math.Round(Conversion.Val(("&H" & Strings.Mid(text1, 5, 2))))))), Conversions.ToString(Strings.Chr(CInt(Math.Round(Conversion.Val(("&H" & Strings.Mid(text1, 3, 2))))))), Conversions.ToString(Strings.Chr(CInt(Math.Round(Conversion.Val(("&H" & Strings.Mid(text1, 1, 2))))))), ChrW(0) & ChrW(0)}
        Dim text3 As String = String.Concat(textArray1)
        Return Strings.Left(Me.prMD5(text3), 10)
    End Function

    Private Function prGetImageSize(ByVal Filename As String, ByRef Width As Long, ByRef Height As Long, ByRef BitColor As Byte, ByRef Device As String) As Boolean
        Dim flag1 As Boolean
        Dim num3 As Integer = FileSystem.FreeFile
        FileSystem.FileOpen(num3, Filename, 32, OpenAccess.Read, OpenShare.Shared, -1)
        Dim text1 As String = Strings.UCase(Strings.Right(Filename, 4))
        If (StringType.StrCmp(text1, ".JPG", False) <> 0) Then
            If (StringType.StrCmp(text1, ".GIF", False) = 0) Then
                Width = LongType.FromObject(ObjectType.AddObj(Me.prGetValue(num3, 6, 1, True), ObjectType.MulObj(Me.prGetValue(num3, 7, 1, True), 256)))
                Height = LongType.FromObject(ObjectType.AddObj(Me.prGetValue(num3, 8, 1, True), ObjectType.MulObj(Me.prGetValue(num3, 9, 1, True), 256)))
                flag1 = True
            ElseIf (StringType.StrCmp(text1, ".BMP", False) = 0) Then
                Width = LongType.FromObject(ObjectType.AddObj(Me.prGetValue(num3, 18, 1, True), ObjectType.MulObj(Me.prGetValue(num3, 19, 1, True), 256)))
                Height = LongType.FromObject(ObjectType.AddObj(Me.prGetValue(num3, 22, 1, True), ObjectType.MulObj(Me.prGetValue(num3, 23, 1, True), 256)))
                flag1 = True
            End If
        Else
            Dim num5 As Long = FileSystem.LOF(num3)
            Dim num6 As Long = 0
            Do While (num6 < (num5 - 10))
                If BooleanType.FromObject(ObjectType.BitAndObj(ObjectType.BitAndObj((ObjectType.ObjTst(Me.prGetValue(num3, num6, 1, True), 255, False) = 0), (ObjectType.ObjTst(Me.prGetValue(num3, (num6 + 1), 1, True), 216, False) = 0)), (ObjectType.ObjTst(Me.prGetValue(num3, (num6 + 2), 1, True), 255, False) = 0))) Then
                    Exit Do
                End If
                num6 = (num6 + 1)
            Loop
            num6 = (num6 + 2)
            If (num6 < (num5 - 10)) Then
                Do While (1 <> 0)
                    Dim flag2 As Boolean
                    Do While (1 <> 0)
                        If BooleanType.FromObject(ObjectType.BitAndObj((ObjectType.ObjTst(Me.prGetValue(num3, num6, 1, True), 255, False) = 0), (ObjectType.ObjTst(Me.prGetValue(num3, (num6 + 1), 1, True), 255, False) <> 0))) Then
                            Exit Do
                        End If
                        num6 = (num6 + 1)
                        If (num6 >= (num5 - 10)) Then
                            Return flag2
                        End If
                    Loop
                    num6 = (num6 + 1)
                    Dim obj1 As Object = Me.prGetValue(num3, num6, 1, True)
                    If ((((ObjectType.ObjTst(obj1, 192, False) >= 0) AndAlso (ObjectType.ObjTst(obj1, 195, False) <= 0)) OrElse ((ObjectType.ObjTst(obj1, 197, False) >= 0) AndAlso (ObjectType.ObjTst(obj1, 199, False) <= 0))) OrElse (((ObjectType.ObjTst(obj1, 201, False) >= 0) AndAlso (ObjectType.ObjTst(obj1, 203, False) <= 0)) OrElse ((ObjectType.ObjTst(obj1, 205, False) >= 0) AndAlso (ObjectType.ObjTst(obj1, 207, False) <= 0)))) Then
                        Height = LongType.FromObject(ObjectType.AddObj(Me.prGetValue(num3, (num6 + 5), 1, True), ObjectType.MulObj(Me.prGetValue(num3, (num6 + 4), 1, True), 256)))
                        Width = LongType.FromObject(ObjectType.AddObj(Me.prGetValue(num3, (num6 + 7), 1, True), ObjectType.MulObj(Me.prGetValue(num3, (num6 + 6), 1, True), 256)))
                        BitColor = ByteType.FromObject(ObjectType.MulObj(8, Me.prGetValue(num3, (num6 + 8), 1, True)))
                        Select Case BitColor
                            Case 8
                                Device = "DeviceGray"
                                Exit Do
                            Case 24
                                Device = "DeviceRGB"
                                Exit Do
                            Case 32
                                Device = "DeviceCMYK"
                                Exit Do
                        End Select
                        Device = ""
                        BitColor = 8
                        Exit Do
                    End If
                    num6 = LongType.FromObject(ObjectType.AddObj(ObjectType.AddObj(num6, Me.prGetValue(num3, (num6 + 2), 1, True)), ObjectType.MulObj(Me.prGetValue(num3, (num6 + 1), 1, True), 256)))
                    If (num6 >= (num5 - 10)) Then
                        Return flag2
                    End If
                Loop
            End If
            flag1 = True
        End If
        If (num3 > 0) Then
            FileSystem.FileClose(New Integer() {num3})
        End If
        Return flag1
    End Function

    Private Function prGetValue(ByVal FileNumber As Integer, Optional ByVal Offset As Long = 0, Optional ByVal NumOfByte As Byte = 2, Optional ByVal Unsigned As Boolean = True) As Object
        Dim num1 As Byte
        Dim num2 As Byte
        Dim obj2 As Object
        FileSystem.Seek(FileNumber, (Offset + 1))
        FileSystem.FileGet(FileNumber, num1, -1)
        FileSystem.FileGet(FileNumber, num2, -1)
        Select Case NumOfByte
            Case 1
                obj2 = num1
                Exit Select
            Case 2
                obj2 = New Decimal(((num1 * 256) + num2))
                If BooleanType.FromObject(ObjectType.BitAndObj(Not Unsigned, (ObjectType.ObjTst(obj2, 32767, False) > 0))) Then
                    obj2 = ObjectType.SubObj(obj2, 65536)
                End If
                Exit Select
            Case 4
                Dim num3 As Byte
                Dim num4 As Byte
                FileSystem.FileGet(FileNumber, num3, -1)
                FileSystem.FileGet(FileNumber, num4, -1)
                obj2 = ((CDbl(New Decimal(((((num1 * 256) + num2) * 256) + num3))) * 256) + num4)
                If BooleanType.FromObject(ObjectType.BitAndObj(Not Unsigned, (ObjectType.ObjTst(obj2, 2147483647, False) > 0))) Then
                    obj2 = ObjectType.SubObj(obj2, 4294967295)
                End If
                Exit Select
        End Select
        Return RuntimeHelpers.GetObjectValue(obj2)
    End Function

    Private Function prInitObj(Optional ByVal ObjId As Integer = -1) As Integer
        If (ObjId = -1) Then
            ObjId = CInt((Me.mvarMaxObject + 1))
        End If
        If (ObjId > Me.mvarMaxObject) Then
            Me.mvarMaxObject = ObjId
        End If
        Me.arrXREF = DirectCast(Utils.CopyArray(DirectCast(Me.arrXREF, Array), New String((CInt(Me.mvarMaxObject) + 1) - 1) {}), String())
        Me.arrXREF(ObjId) = (Strings.Right(("0000000000" & StringType.FromLong((FileSystem.Seek(Me.mvarFileNumber) - 1))), 10) & " 00000 n")
        Me.prWriteObj(Me.prFormat("#i 0 obj", New Object() {ObjId}))
        Me.mvarIdObj = ObjId
        Return ObjId
    End Function

    Private Sub prInsertObject(ByVal OnFront As Boolean)
        If (Me.mvarObjectCount > 0) Then
            Dim num3 As Integer = Me.mvarObjectCount
            Dim num1 As Integer = 1
            Do While (num1 <= num3)
                Dim num2 As Integer = num1
                If (((((Me.arrOBJECT(num2).Options And pdfObjectType.pdfOnFront) <> pdfObjectType.pdfNull) And OnFront) Or (((Me.arrOBJECT(num2).Options And pdfObjectType.pdfOnFront) = pdfObjectType.pdfNull) And Not OnFront)) And ((((Me.arrOBJECT(num2).Options And pdfObjectType.pdfAllPages) = pdfObjectType.pdfAllPages) Or (((Me.arrOBJECT(num2).Options And pdfObjectType.pdfEvenPages) <> pdfObjectType.pdfNull) And ((Me.mvarTotPages Mod 2) = 0))) Or ((((Me.arrOBJECT(num2).Options And pdfObjectType.pdfOddPages) <> pdfObjectType.pdfNull) And ((Me.mvarTotPages Mod 2) <> 0)) And Not (((Me.arrOBJECT(num2).Options And pdfObjectType.pdfNotFirstPage) <> pdfObjectType.pdfNull) And (Me.mvarTotPages = 1))))) Then
                    Me.PlayObject(Me.arrOBJECT(num2).Name)
                End If
                num1 += 1
            Loop
        End If
    End Sub

    Private Sub prLoadState()
        If (Me.intGraphState > 0) Then
            Dim num1 As Integer = Me.intGraphState
            Me.mvarCurrX = Me.arrGRAPHSTATE(num1).PosX
            Me.mvarCurrY = Me.arrGRAPHSTATE(num1).PosY
            Me.mvarKUnit = Me.arrGRAPHSTATE(num1).ScaleUnit
            Me.SetColorLine((Me.arrGRAPHSTATE(num1).ColorLine & ";" & Me.arrGRAPHSTATE(num1).ColorLineStencil))
            Me.SetColorFill((Me.arrGRAPHSTATE(num1).ColorFill & ";" & Me.arrGRAPHSTATE(num1).ColorFillStencil))
            Me.SetColorText((Me.arrGRAPHSTATE(num1).ColorText & ";" & Me.arrGRAPHSTATE(num1).ColorTextStencil))
            Me.SetLineWidth((Me.arrGRAPHSTATE(num1).LineWidth / Me.mvarKUnit))
            Me.SetFont(Me.arrGRAPHSTATE(num1).FontFamily, Me.arrGRAPHSTATE(num1).FontSize, Me.arrGRAPHSTATE(num1).FontStyle)
            Me.intGraphState -= 1
            Me.arrGRAPHSTATE = DirectCast(Utils.CopyArray(DirectCast(Me.arrGRAPHSTATE, Array), New GraphicState((Me.intGraphState + 1) - 1) {}), GraphicState())
        End If
    End Sub

    Private Function prMD5(ByVal SourceText As String) As String
        'Create an encoding object to ensure the encoding standard for the source text
        'Dim Ue As New UnicodeEncoding()
        Dim s As String = ""
        Dim i As Integer
        'Retrieve a byte array based on the source text
        Dim numArray1() As Byte = Me.prStr2Mtx(SourceText)
        'Instantiate an MD5 Provider object
        Dim Md5 As New MD5CryptoServiceProvider()
        'Compute the hash value from the source
        Dim ByteHash() As Byte = Md5.ComputeHash(numArray1)
        'New MD5CryptoServiceProvider().ComputeHash(ASCIIEncoding.ASCII.GetBytes(FileContent))
        'And convert it to String format for return
        Return prMtx2Str(ByteHash)

        'Return ToBase64String(ByteHash)
    End Function

    Private Function prMD5old(ByVal strMessage As String) As String
        Dim buffer1 As Byte()
        Dim num3 As Long
        Dim buffer2 As Byte() = New Byte(64 - 1) {}
        Dim numArray1 As Long() = New Long(4 - 1) {}
        If (Strings.Len(strMessage) > 0) Then
            buffer1 = DirectCast(Me.prStr2Mtx(strMessage), Byte())
            num3 = ((Information.UBound(buffer1, 1) - Information.LBound(buffer1, 1)) + 1)
        End If
        If (num3 >= 268435455) Then
            Throw ProjectData.CreateProjectError(6)
        End If
        Dim num5 As Long = (num3 / CLng(64))
        numArray1(0) = 1732584193
        numArray1(1) = -271733879
        numArray1(2) = -1732584194
        numArray1(3) = 271733878
        Dim num2 As Long = 0
        Dim num7 As Long = (num5 - 1)
        Dim num1 As Long = 0
        Do While (num1 <= num7)
            Me.prMD5_transform(numArray1, buffer1, num2)
            num2 = (num2 + 64)
            num1 += 1
        Loop
        Dim num6 As Long = (num3 Mod CLng(64))
        num2 = (num5 * 64)
        Dim num8 As Long = (num6 - 1)
        num1 = 0
        Do While (num1 <= num8)
            buffer2(CInt(num1)) = buffer1(CInt((num2 + num1)))
            num1 += 1
        Loop
        buffer2(CInt(num6)) = 128
        If (num6 >= 56) Then
            Me.prMD5_transform(numArray1, buffer2, CLng(0))
            num1 = 0
            Do
                buffer2(CInt(num1)) = 0
                num1 += 1
            Loop While (num1 <= 63)
        End If
        Dim num4 As Long = (num3 * 8)
        buffer2(56) = CByte((num4 And 255))
        buffer2(57) = CByte(((num4 / 256) And 255))
        buffer2(58) = CByte(((num4 / CLng(65536)) And 255))
        buffer2(59) = CByte(((num4 / CLng(16777216)) And 255))
        Me.prMD5_transform(numArray1, buffer2, CLng(0))
        Dim text1 As String = ""
        num1 = 0
        Do
            Dim textArray1 As String() = New String() {text1, Conversions.ToString(Strings.Chr(CByte((numArray1(CInt(num1)) And 255)))), Conversions.ToString(Strings.Chr(CByte((((numArray1(CInt(num1)) And 65280) / CLng(256)) And 255)))), Conversions.ToString(Strings.Chr(CByte((((numArray1(CInt(num1)) And 16711680) / CLng(65536)) And 255)))), Conversions.ToString(Strings.Chr(CByte((((numArray1(CInt(num1)) And -16777216) / CLng(16777216)) And 255))))}
            text1 = String.Concat(textArray1)
            num1 += 1
        Loop While (num1 <= 3)
        Return text1
    End Function

    Private Function prMD5_Add(ByVal wordA As Long, ByVal wordB As Long) As Long
        Dim num1 As Double = Conversions.ToDouble(Operators.AddObject(Interaction.IIf((wordA < 0), (wordA + 4294967296), wordA), Interaction.IIf((wordB < 0), (wordB + 4294967296), wordB)))
        If (num1 > 4294967296) Then
            num1 = (num1 - 4294967296)
        End If
        If ((num1 < 0) Or (num1 >= 4294967296)) Then
            Throw ProjectData.CreateProjectError(6)
        End If
        If (num1 <= 2147483647) Then
            Return CLng(Math.Round(num1))
        End If
        Return CLng(Math.Round(CDbl((num1 - 4294967296))))
    End Function

    Private Function prMD5_AddRotAdd(ByVal f As Long, ByVal a As Long, ByVal b As Long, ByVal X As Long, ByVal s As Integer, ByVal ac As Long) As Long
        Dim num4 As Long = Me.prMD5_Add(a, f)
        num4 = Me.prMD5_Add(num4, X)
        num4 = Me.prMD5_Add(num4, ac)
        Dim num5 As Integer = s
        Dim num1 As Integer = 1
        Do While (num1 <= num5)
            Dim num3 As Long = ((num4 And 1073741823) * 2)
            If ((num4 And 1073741824) <> 0) Then
                num3 = (num3 Or -2147483648)
            End If
            If ((num4 And -2147483648) <> 0) Then
                num3 = (num3 Or 1)
            End If
            num4 = num3
            num1 += 1
        Loop
        Return Me.prMD5_Add(num4, b)
    End Function

    Private Sub prMD5_transform(ByVal state As Long(), ByVal buf As Byte(), ByVal Index As Long)
        Dim numArray1 As Long() = New Long(16 - 1) {}
        Dim num1 As Long = state(0)
        Dim num2 As Long = state(1)
        Dim num3 As Long = state(2)
        Dim num4 As Long = state(3)
        Dim num5 As Integer = 0
Label_001D:
        numArray1(num5) = (((((buf((CInt(Index) + 3)) And 127) * 16777216) Or (buf((CInt(Index) + 2)) * 65536)) Or (buf((CInt(Index) + 1)) * 256)) Or buf(CInt(Index)))
        If ((buf((CInt(Index) + 3)) And 128) > 0) Then
            numArray1(num5) = (numArray1(num5) Or -2147483648)
        End If
        Index = (Index + 4)
        num5 += 1
        Dim num6 As Integer = 15
        If (num5 > num6) Then
            num1 = Me.prMD5_AddRotAdd(((num2 And num3) Or (Not num2 And num4)), num1, num2, numArray1(0), 7, CLng(-680876936))
            num4 = Me.prMD5_AddRotAdd(((num1 And num2) Or (Not num1 And num3)), num4, num1, numArray1(1), 12, CLng(-389564586))
            num3 = Me.prMD5_AddRotAdd(((num4 And num1) Or (Not num4 And num2)), num3, num4, numArray1(2), 17, CLng(606105819))
            num2 = Me.prMD5_AddRotAdd(((num3 And num4) Or (Not num3 And num1)), num2, num3, numArray1(3), 22, CLng(-1044525330))
            num1 = Me.prMD5_AddRotAdd(((num2 And num3) Or (Not num2 And num4)), num1, num2, numArray1(4), 7, CLng(-176418897))
            num4 = Me.prMD5_AddRotAdd(((num1 And num2) Or (Not num1 And num3)), num4, num1, numArray1(5), 12, CLng(1200080426))
            num3 = Me.prMD5_AddRotAdd(((num4 And num1) Or (Not num4 And num2)), num3, num4, numArray1(6), 17, CLng(-1473231341))
            num2 = Me.prMD5_AddRotAdd(((num3 And num4) Or (Not num3 And num1)), num2, num3, numArray1(7), 22, CLng(-45705983))
            num1 = Me.prMD5_AddRotAdd(((num2 And num3) Or (Not num2 And num4)), num1, num2, numArray1(8), 7, CLng(1770035416))
            num4 = Me.prMD5_AddRotAdd(((num1 And num2) Or (Not num1 And num3)), num4, num1, numArray1(9), 12, CLng(-1958414417))
            num3 = Me.prMD5_AddRotAdd(((num4 And num1) Or (Not num4 And num2)), num3, num4, numArray1(10), 17, CLng(-42063))
            num2 = Me.prMD5_AddRotAdd(((num3 And num4) Or (Not num3 And num1)), num2, num3, numArray1(11), 22, CLng(-1990404162))
            num1 = Me.prMD5_AddRotAdd(((num2 And num3) Or (Not num2 And num4)), num1, num2, numArray1(12), 7, CLng(1804603682))
            num4 = Me.prMD5_AddRotAdd(((num1 And num2) Or (Not num1 And num3)), num4, num1, numArray1(13), 12, CLng(-40341101))
            num3 = Me.prMD5_AddRotAdd(((num4 And num1) Or (Not num4 And num2)), num3, num4, numArray1(14), 17, CLng(-1502002290))
            num2 = Me.prMD5_AddRotAdd(((num3 And num4) Or (Not num3 And num1)), num2, num3, numArray1(15), 22, CLng(1236535329))
            num1 = Me.prMD5_AddRotAdd(((num2 And num4) Or (num3 And Not num4)), num1, num2, numArray1(1), 5, CLng(-165796510))
            num4 = Me.prMD5_AddRotAdd(((num1 And num3) Or (num2 And Not num3)), num4, num1, numArray1(6), 9, CLng(-1069501632))
            num3 = Me.prMD5_AddRotAdd(((num4 And num2) Or (num1 And Not num2)), num3, num4, numArray1(11), 14, CLng(643717713))
            num2 = Me.prMD5_AddRotAdd(((num3 And num1) Or (num4 And Not num1)), num2, num3, numArray1(0), 20, CLng(-373897302))
            num1 = Me.prMD5_AddRotAdd(((num2 And num4) Or (num3 And Not num4)), num1, num2, numArray1(5), 5, CLng(-701558691))
            num4 = Me.prMD5_AddRotAdd(((num1 And num3) Or (num2 And Not num3)), num4, num1, numArray1(10), 9, CLng(38016083))
            num3 = Me.prMD5_AddRotAdd(((num4 And num2) Or (num1 And Not num2)), num3, num4, numArray1(15), 14, CLng(-660478335))
            num2 = Me.prMD5_AddRotAdd(((num3 And num1) Or (num4 And Not num1)), num2, num3, numArray1(4), 20, CLng(-405537848))
            num1 = Me.prMD5_AddRotAdd(((num2 And num4) Or (num3 And Not num4)), num1, num2, numArray1(9), 5, CLng(568446438))
            num4 = Me.prMD5_AddRotAdd(((num1 And num3) Or (num2 And Not num3)), num4, num1, numArray1(14), 9, CLng(-1019803690))
            num3 = Me.prMD5_AddRotAdd(((num4 And num2) Or (num1 And Not num2)), num3, num4, numArray1(3), 14, CLng(-187363961))
            num2 = Me.prMD5_AddRotAdd(((num3 And num1) Or (num4 And Not num1)), num2, num3, numArray1(8), 20, CLng(1163531501))
            num1 = Me.prMD5_AddRotAdd(((num2 And num4) Or (num3 And Not num4)), num1, num2, numArray1(13), 5, CLng(-1444681467))
            num4 = Me.prMD5_AddRotAdd(((num1 And num3) Or (num2 And Not num3)), num4, num1, numArray1(2), 9, CLng(-51403784))
            num3 = Me.prMD5_AddRotAdd(((num4 And num2) Or (num1 And Not num2)), num3, num4, numArray1(7), 14, CLng(1735328473))
            num2 = Me.prMD5_AddRotAdd(((num3 And num1) Or (num4 And Not num1)), num2, num3, numArray1(12), 20, CLng(-1926607734))
            num1 = Me.prMD5_AddRotAdd(((num2 Xor num3) Xor num4), num1, num2, numArray1(5), 4, CLng(-378558))
            num4 = Me.prMD5_AddRotAdd(((num1 Xor num2) Xor num3), num4, num1, numArray1(8), 11, CLng(-2022574463))
            num3 = Me.prMD5_AddRotAdd(((num4 Xor num1) Xor num2), num3, num4, numArray1(11), 16, CLng(1839030562))
            num2 = Me.prMD5_AddRotAdd(((num3 Xor num4) Xor num1), num2, num3, numArray1(14), 23, CLng(-35309556))
            num1 = Me.prMD5_AddRotAdd(((num2 Xor num3) Xor num4), num1, num2, numArray1(1), 4, CLng(-1530992060))
            num4 = Me.prMD5_AddRotAdd(((num1 Xor num2) Xor num3), num4, num1, numArray1(4), 11, CLng(1272893353))
            num3 = Me.prMD5_AddRotAdd(((num4 Xor num1) Xor num2), num3, num4, numArray1(7), 16, CLng(-155497632))
            num2 = Me.prMD5_AddRotAdd(((num3 Xor num4) Xor num1), num2, num3, numArray1(10), 23, CLng(-1094730640))
            num1 = Me.prMD5_AddRotAdd(((num2 Xor num3) Xor num4), num1, num2, numArray1(13), 4, CLng(681279174))
            num4 = Me.prMD5_AddRotAdd(((num1 Xor num2) Xor num3), num4, num1, numArray1(0), 11, CLng(-358537222))
            num3 = Me.prMD5_AddRotAdd(((num4 Xor num1) Xor num2), num3, num4, numArray1(3), 16, CLng(-722521979))
            num2 = Me.prMD5_AddRotAdd(((num3 Xor num4) Xor num1), num2, num3, numArray1(6), 23, CLng(76029189))
            num1 = Me.prMD5_AddRotAdd(((num2 Xor num3) Xor num4), num1, num2, numArray1(9), 4, CLng(-640364487))
            num4 = Me.prMD5_AddRotAdd(((num1 Xor num2) Xor num3), num4, num1, numArray1(12), 11, CLng(-421815835))
            num3 = Me.prMD5_AddRotAdd(((num4 Xor num1) Xor num2), num3, num4, numArray1(15), 16, CLng(530742520))
            num2 = Me.prMD5_AddRotAdd(((num3 Xor num4) Xor num1), num2, num3, numArray1(2), 23, CLng(-995338651))
            num1 = Me.prMD5_AddRotAdd((num3 Xor (num2 Or Not num4)), num1, num2, numArray1(0), 6, CLng(-198630844))
            num4 = Me.prMD5_AddRotAdd((num2 Xor (num1 Or Not num3)), num4, num1, numArray1(7), 10, CLng(1126891415))
            num3 = Me.prMD5_AddRotAdd((num1 Xor (num4 Or Not num2)), num3, num4, numArray1(14), 15, CLng(-1416354905))
            num2 = Me.prMD5_AddRotAdd((num4 Xor (num3 Or Not num1)), num2, num3, numArray1(5), 21, CLng(-57434055))
            num1 = Me.prMD5_AddRotAdd((num3 Xor (num2 Or Not num4)), num1, num2, numArray1(12), 6, CLng(1700485571))
            num4 = Me.prMD5_AddRotAdd((num2 Xor (num1 Or Not num3)), num4, num1, numArray1(3), 10, CLng(-1894986606))
            num3 = Me.prMD5_AddRotAdd((num1 Xor (num4 Or Not num2)), num3, num4, numArray1(10), 15, CLng(-1051523))
            num2 = Me.prMD5_AddRotAdd((num4 Xor (num3 Or Not num1)), num2, num3, numArray1(1), 21, CLng(-2054922799))
            num1 = Me.prMD5_AddRotAdd((num3 Xor (num2 Or Not num4)), num1, num2, numArray1(8), 6, CLng(1873313359))
            num4 = Me.prMD5_AddRotAdd((num2 Xor (num1 Or Not num3)), num4, num1, numArray1(15), 10, CLng(-30611744))
            num3 = Me.prMD5_AddRotAdd((num1 Xor (num4 Or Not num2)), num3, num4, numArray1(6), 15, CLng(-1560198380))
            num2 = Me.prMD5_AddRotAdd((num4 Xor (num3 Or Not num1)), num2, num3, numArray1(13), 21, CLng(1309151649))
            num1 = Me.prMD5_AddRotAdd((num3 Xor (num2 Or Not num4)), num1, num2, numArray1(4), 6, CLng(-145523070))
            num4 = Me.prMD5_AddRotAdd((num2 Xor (num1 Or Not num3)), num4, num1, numArray1(11), 10, CLng(-1120210379))
            num3 = Me.prMD5_AddRotAdd((num1 Xor (num4 Or Not num2)), num3, num4, numArray1(2), 15, CLng(718787259))
            num2 = Me.prMD5_AddRotAdd((num4 Xor (num3 Or Not num1)), num2, num3, numArray1(9), 21, CLng(-343485551))
            state(0) = Me.prMD5_Add(state(0), num1)
            state(1) = Me.prMD5_Add(state(1), num2)
            state(2) = Me.prMD5_Add(state(2), num3)
            state(3) = Me.prMD5_Add(state(3), num4)
        Else
            GoTo Label_001D
        End If
    End Sub

    Private Function prMtx2Str(ByVal Matrix As Byte()) As String
        Dim num2 As Long = Information.UBound(Matrix, 1)
        Dim text1 As String = Strings.Space(CInt((num2 + 1)))
        Dim num3 As Long = num2
        Dim num1 As Long = 0
        Do While (num1 <= num3)
            StringType.MidStmtStr(text1, CInt((num1 + 1)), 1, StringType.FromChar(Strings.Chr(Matrix(CInt(num1)))))
            num1 = (num1 + 1)
        Loop
        Return text1
    End Function

    Private Function prParseBMP(ByVal FileName As String, ByRef ImgBuf As Byte(), ByRef ImgColor As Byte(), ByRef ImgBPP As Byte) As Boolean
        Dim num1 As Short
        Dim num2 As Integer
        Dim num3 As Integer
        Dim flag2 As Boolean
        Dim text1 As New String(" "c, 2)
        Dim num17 As Short = CShort(FileSystem.FreeFile)
        FileSystem.FileOpen(num17, FileName, 32, OpenAccess.Read, -1, -1)
        FileSystem.FileGet(num17, text1, -1, False)
        FileSystem.FileGet(num17, num3, -1)
        FileSystem.FileGet(num17, num1, -1)
        FileSystem.FileGet(num17, num1, -1)
        FileSystem.FileGet(num17, num2, -1)
        If (StringType.StrCmp(text1, "BM", False) = 0) Then
            Dim num4 As Short
            Dim num5 As Integer
            Dim num6 As Integer
            Dim num7 As Integer
            Dim num8 As Integer
            Dim num9 As Short
            Dim num10 As Integer
            Dim num11 As Integer
            Dim num12 As Integer
            Dim num13 As Integer
            Dim num14 As Integer
            Dim num16 As Integer
            Dim array1 As Array
            FileSystem.FileGet(num17, num10, -1)
            FileSystem.FileGet(num17, num12, -1)
            FileSystem.FileGet(num17, num8, -1)
            FileSystem.FileGet(num17, num9, -1)
            FileSystem.FileGet(num17, num4, -1)
            FileSystem.FileGet(num17, num7, -1)
            FileSystem.FileGet(num17, num11, -1)
            FileSystem.FileGet(num17, num13, -1)
            FileSystem.FileGet(num17, num14, -1)
            FileSystem.FileGet(num17, num6, -1)
            FileSystem.FileGet(num17, num5, -1)
            Dim num15 As Byte = CByte(num4)
            If (num15 <= 8) Then
                Dim buffer1 As Byte() = New Byte((CInt(Math.Round(CDbl(((Math.Pow(2, CDbl(num15)) * 4) - 1)))) + 1) - 1) {}
                array1 = buffer1
                FileSystem.FileGet(num17, array1, -1, False, False)
                buffer1 = DirectCast(array1, Byte())
                ImgColor = New Byte((CInt(Math.Round(CDbl(((3 * Math.Pow(2, CDbl(num15))) - 1)))) + 1) - 1) {}
                Dim num26 As Integer = CInt(Math.Round(CDbl((Math.Pow(2, CDbl(num15)) - 1))))
                num16 = 0
                Do While (num16 <= num26)
                    ImgColor((num16 * 3)) = buffer1(((num16 * 4) + 2))
                    ImgColor(((num16 * 3) + 1)) = buffer1(((num16 * 4) + 1))
                    ImgColor(((num16 * 3) + 2)) = buffer1((num16 * 4))
                    num16 += 1
                Loop
            End If
            Dim num23 As Integer = CInt((CLng(Math.Round(CDbl(((CDbl((num12 * num15)) / 8) + 3)))) And -4))
            Dim num20 As Long = num12
            Dim num19 As Long = num8
            ImgBPP = CByte(num4)
            Dim buffer2 As Byte() = New Byte((CInt(Math.Round(CDbl(((num8 * num23) - 1.0!)))) + 1) - 1) {}
            FileSystem.Seek(num17, CLng((num2 + 1)))
            array1 = buffer2
            FileSystem.FileGet(num17, array1, -1, False, False)
            buffer2 = DirectCast(array1, Byte())
            FileSystem.FileClose(New Integer() {num17})
            ImgBuf = New Byte((Information.UBound(buffer2, 1) + 1) - 1) {}
            If (num7 = 0) Then
                Dim flag1 As Boolean
                Dim num21 As Integer = 0
                If (num15 > 8) Then
                    flag1 = ((num12 Mod 4) <> 0)
                    Dim num25 As Integer = (Information.UBound(buffer2, 1) - 1)
                    num16 = 0
                    Do While (num16 <= num25)
                        ImgBuf((3 * num21)) = buffer2((num16 + 2))
                        ImgBuf(((3 * num21) + 1)) = buffer2((num16 + 1))
                        ImgBuf(((3 * num21) + 2)) = buffer2(num16)
                        If ((((num21 + 1) Mod num12) = 0) And flag1) Then
                            num16 = (num16 + (num12 Mod 4))
                        End If
                        num21 += 1
                        num16 = (num16 + 3)
                    Loop
                ElseIf (num15 <= 8) Then
                    flag1 = (ObjectType.ObjTst(ObjectType.ModObj(num12, Interaction.IIf((num15 = 8), 4, 8)), 0, False) <> 0)
                    Dim num24 As Integer = Information.UBound(buffer2, 1)
                    Dim num18 As Integer = 0
                    On Error GoTo suite
                    Do While (num18 <= num24 And num21 < num24)
                        ImgBuf(num21) = buffer2(num18)
                        If (((CDbl((num21 + 1)) Mod Conversion.Int((((num12 + (8 / CDbl(num15))) - 1) / (8 / CDbl(num15))))) = 0) And flag1) Then
                            num18 = (num18 + (num23 - (num18 + (1 Mod num23))))
                        End If
                        num21 += 1
                        num18 += 1
                    Loop
suite:
                End If
                flag2 = True
            End If
        End If
        If (num17 > 0) Then
            FileSystem.FileClose(New Integer() {num17})
        End If
        Return flag2
    End Function

    Private Sub prParseColor(ByRef Color As String, ByRef StencilColor As String)
        Dim text1 As String
        Dim text3 As String
        If (StringType.StrCmp(Color, "", False) <> 0) Then
            If (StringType.StrCmp(Strings.Left(Color, 1), "#", False) <> 0) Then
                If (StringType.StrCmp(Strings.Left(Color, 1), "%", False) <> 0) Then
                    Dim textArray1 As String()
                    If (StringType.StrCmp(Strings.Left(Color, 1), "@", False) <> 0) Then
                        textArray1 = Strings.Split(Color, ";", 2, 0)
                        Dim text4 As String = Strings.Trim(Strings.UCase(textArray1(0)))
                        If (StringType.StrCmp(text4, "BLACK", False) = 0) Then
                            text1 = "#000000"
                        ElseIf (StringType.StrCmp(text4, "GRAY", False) = 0) Then
                            text1 = "#808080"
                        ElseIf (StringType.StrCmp(text4, "SILVER", False) = 0) Then
                            text1 = "#C0C0C0"
                        ElseIf (StringType.StrCmp(text4, "WHITE", False) = 0) Then
                            text1 = "#FFFFFF"
                        ElseIf (StringType.StrCmp(text4, "MAROON", False) = 0) Then
                            text1 = "#800000"
                        ElseIf (StringType.StrCmp(text4, "RED", False) = 0) Then
                            text1 = "#FF0000"
                        ElseIf (StringType.StrCmp(text4, "PURPLE", False) = 0) Then
                            text1 = "#800080"
                        ElseIf ((StringType.StrCmp(text4, "FUCHSIA", False) = 0) OrElse (StringType.StrCmp(text4, "MAGENTA", False) = 0)) Then
                            text1 = "#FF00FF"
                        ElseIf (StringType.StrCmp(text4, "GREEN", False) = 0) Then
                            text1 = "#008000"
                        ElseIf (StringType.StrCmp(text4, "LIME", False) = 0) Then
                            text1 = "#00FF00"
                        ElseIf (StringType.StrCmp(text4, "OLIVE", False) = 0) Then
                            text1 = "#808000"
                        ElseIf (StringType.StrCmp(text4, "YELLOW", False) = 0) Then
                            text1 = "#FFFF00"
                        ElseIf (StringType.StrCmp(text4, "NAVY", False) = 0) Then
                            text1 = "#000080"
                        ElseIf (StringType.StrCmp(text4, "BLUE", False) = 0) Then
                            text1 = "#0000FF"
                        ElseIf (StringType.StrCmp(text4, "TEAL", False) = 0) Then
                            text1 = "#008080"
                        ElseIf ((StringType.StrCmp(text4, "AQUA", False) = 0) OrElse (StringType.StrCmp(text4, "CYAN", False) = 0)) Then
                            text1 = "#00FFFF"
                        Else
                            text1 = Strings.Trim(Strings.UCase(textArray1(0)))
                            If (Information.UBound(textArray1, 1) > 0) Then
                                text3 = Strings.Trim(Strings.UCase(textArray1(1)))
                                If (StringType.StrCmp(text3, "", False) <> 0) Then
                                    Dim text2 As String
                                    Me.prParseColor(text3, text2)
                                End If
                            Else
                                text3 = ""
                            End If
                        End If
                    Else
                        Color = Strings.Replace(Color, ",", ".", 1, -1, 0)
                        textArray1 = Strings.Split(Strings.Mid(Color, 2), ";", 4, 0)
                        If (Information.UBound(textArray1, 1) = 3) Then
                            text1 = ("#" & Strings.Right(("0" & Conversion.Hex((2.55 * Math.Abs(CDbl((100 - (Conversion.Val(textArray1(0)) + Conversion.Val(textArray1(3))))))))), 2) & Strings.Right(("0" & Conversion.Hex((2.55 * Math.Abs(CDbl((100 - (Conversion.Val(textArray1(1)) + Conversion.Val(textArray1(3))))))))), 2) & Strings.Right(("0" & Conversion.Hex((2.55 * Math.Abs(CDbl((100 - (Conversion.Val(textArray1(2)) + Conversion.Val(textArray1(3))))))))), 2))
                        ElseIf (Information.UBound(textArray1, 1) = 2) Then
                            text1 = ("#" & Strings.Right(("0" & Conversion.Hex(Conversion.Val(textArray1(0)))), 2) & Strings.Right(("0" & Conversion.Hex(Conversion.Val(textArray1(1)))), 2) & Strings.Right(("0" & Conversion.Hex(Conversion.Val(textArray1(2)))), 2))
                        End If
                    End If
                Else
                    Dim num1 As Integer = CInt(Math.Round(CDbl((255 - Conversion.Int(((255 * Conversion.Val(Strings.Mid(Color, 2))) / 100))))))
                    text1 = ("#" & Strings.Right(("0" & Conversion.Hex(num1)), 2) & Strings.Right(("0" & Conversion.Hex(num1)), 2) & Strings.Right(("0" & Conversion.Hex(num1)), 2))
                End If
            Else
                text1 = Strings.Left(Color, 7)
            End If
        End If
        Color = text1
        StencilColor = text3
    End Sub

    Private Sub prParseEps(ByVal Filename As String, ByVal DestX As Single, ByVal DestY As Single, Optional ByVal Width As Single = 0.0!, Optional ByVal Height As Single = 0.0!)
        Dim num6 As Long
        Dim single2 As Single = Width
        Dim single1 As Single = Height
        Dim num5 As Long = 1
        Dim num1 As Integer = FileSystem.FreeFile
        FileSystem.FileOpen(num1, Filename, 1, -1, -1, -1)
Label_064B:
        Do While Not FileSystem.EOF(num1)
            Dim text3 As String = FileSystem.LineInput(num1)
            Do While (StringType.StrCmp(text3, "", False) <> 0)
                Dim num3 As Long
                Dim text4 As String
                Dim num2 As Long = Strings.InStr(text3, ChrW(10), 0)
                If (num2 <> 0) Then
                    text4 = Strings.Left(text3, CInt((num2 - 1)))
                    text3 = Strings.Mid(text3, CInt((num2 + 1)))
                Else
                    text4 = text3
                    text3 = ""
                End If
                Dim text2 As String = Strings.Left(text4, 1)
                If (StringType.StrCmp(text2, "%", False) = 0) Then
                    If (StringType.StrCmp(Strings.Left(text4, 14), "%%BoundingBox:", False) = 0) Then
                        Dim textArray1 As String() = Strings.Split(Strings.Trim(Strings.Mid(text4, 15)), " ", 4, 0)
                        Dim single4 As Single = CSng(Conversion.Val(textArray1(0)))
                        Dim single7 As Single = CSng(Conversion.Val(textArray1(1)))
                        Dim single3 As Single = (CSng(Conversion.Val(textArray1(2))) - single4)
                        Dim single6 As Single = (CSng(Conversion.Val(textArray1(3))) - single7)
                        If ((Width = 0.0!) And (Height = 0.0!)) Then
                            single2 = ((20.0! * single3) / (Me.mvarKUnit * 72.0!))
                            single1 = ((20.0! * single6) / (Me.mvarKUnit * 72.0!))
                        End If
                        If (Width = 0.0!) Then
                            single2 = ((Height * single3) / single6)
                        End If
                        If (Height = 0.0!) Then
                            single1 = ((Width * single6) / single3)
                        End If
                        Dim single5 As Single = ((Me.mvarKUnit * single2) / single3)
                        Dim single8 As Single = ((Me.mvarKUnit * single1) / single6)
                        Me.prWriteObj("q")
                        Me.prWriteObj(Me.prFormat("#s 0 0 #s #k #k cm", New Object() {single5, single8, DestX, DestY}))
                        Me.prWriteObj(Me.prFormat("1 0 0 -1 #s #s cm", New Object() {-single4, (single6 + single7)}))
                        num6 = num5
                    ElseIf (StringType.StrCmp(Strings.Left(text4, 10), "%%EndSetup", False) = 0) Then
                        num6 = num5
                    ElseIf (StringType.StrCmp(Strings.Left(text4, 11), "%%EndProlog", False) = 0) Then
                        num6 = num5
                    ElseIf (StringType.StrCmp(Strings.Left(text4, 13), "%%PageTrailer", False) = 0) Then
                        num3 = (num5 - 1)
                    ElseIf (StringType.StrCmp(Strings.Left(text4, 8), "showpage", False) = 0) Then
                        num3 = (num5 - 1)
                    End If
                ElseIf ((((num6 > 0) And (num5 > num6)) And (num3 = 0)) AndAlso ((((StringType.StrCmp(Strings.Trim(text4), "", False) <> 0) And (StringType.StrCmp(text2, "%", False) <> 0)) And (StringType.StrCmp(text2, "/", False) <> 0)) And (StringType.StrCmp(text2, "(", False) <> 0))) Then
                    Dim text1 As String
                    Dim num4 As Long = Strings.Len(text4)
                    If ((num4 = 1) Or ((num4 > 1) And (StringType.StrCmp(Strings.Left(Strings.Right(text4, 2), 1), " ", False) = 0))) Then
                        text1 = Strings.Right(text4, 1)
                        Dim text6 As String = text1
                        If ((((((StringType.StrCmp(text6, "m", False) = 0) OrElse (StringType.StrCmp(text6, "l", False) = 0)) OrElse (StringType.StrCmp(text6, "v", False) = 0)) OrElse ((StringType.StrCmp(text6, "y", False) = 0) OrElse (StringType.StrCmp(text6, "c", False) = 0))) OrElse (((StringType.StrCmp(text6, "k", False) = 0) OrElse (StringType.StrCmp(text6, "K", False) = 0)) OrElse ((StringType.StrCmp(text6, "g", False) = 0) OrElse (StringType.StrCmp(text6, "G", False) = 0)))) OrElse ((((StringType.StrCmp(text6, "s", False) = 0) OrElse (StringType.StrCmp(text6, "S", False) = 0)) OrElse ((StringType.StrCmp(text6, "J", False) = 0) OrElse (StringType.StrCmp(text6, "j", False) = 0))) OrElse (((StringType.StrCmp(text6, "w", False) = 0) OrElse (StringType.StrCmp(text6, "M", False) = 0)) OrElse (StringType.StrCmp(text6, "d", False) = 0)))) Then
                            Me.prWriteObj(text4)
                        ElseIf (StringType.StrCmp(text6, "L", False) = 0) Then
                            Me.prWriteObj((Strings.Left(text4, CInt((num4 - 1))) & "l"))
                        ElseIf (StringType.StrCmp(text6, "C", False) = 0) Then
                            Me.prWriteObj((Strings.Left(text4, CInt((num4 - 1))) & "c"))
                        ElseIf ((StringType.StrCmp(text6, "f", False) = 0) OrElse (StringType.StrCmp(text6, "F", False) = 0)) Then
                            Me.prWriteObj("f*")
                        ElseIf ((StringType.StrCmp(text6, "b", False) = 0) OrElse (StringType.StrCmp(text6, "B", False) = 0)) Then
                            Me.prWriteObj((text1 & "*"))
                        End If
                    ElseIf ((num4 > 2) And (StringType.StrCmp(Strings.Left(Strings.Right(text4, 3), 1), " ", False) = 0)) Then
                        text1 = Strings.Right(text4, 2)
                        Dim text5 As String = text1
                        If (StringType.StrCmp(text5, "Xa", False) = 0) Then
                            Me.prWriteObj((Strings.Left(text4, CInt((num4 - 2))) & "rg"))
                        ElseIf (StringType.StrCmp(text5, "XR", False) = 0) Then
                            Me.prWriteObj((Strings.Left(text4, CInt((num4 - 2))) & "G"))
                        End If
                    End If
                End If
                If (num3 <> 0) Then
                    GoTo Label_064B
                End If
                num5 = (num5 + 1)
            Loop
        Loop
        If (num1 > 0) Then
            FileSystem.FileClose(New Integer() {num1})
        End If
        If (num6 <> 0) Then
            Me.prWriteObj("Q")
        End If
    End Sub

    Private Function prParseGIF(ByVal FileName As String, ByRef ImgBuf As Byte(), ByRef ImgColor As Byte(), ByRef ImgBPP As Byte, Optional ByVal FrameNumber As Integer = 1) As Boolean
        Dim num2 As Byte
        Dim num4 As Byte
        Dim buffer1 As Byte()
        Dim buffer2 As Byte()
        Dim num15 As Short
        Dim num16 As Short
        Dim flag4 As Boolean
        Dim buffer3 As Byte()
        Dim array1 As Array
        Dim text4 As New String(" "c, 6)
        Dim num9 As Short = 0
        Dim num5 As Integer = FileSystem.FreeFile
        FileSystem.FileOpen(num5, FileName, 32, -1, -1, -1)
        FileSystem.FileGet(num5, text4, -1, False)
        If Not ((StringType.StrCmp(text4, "GIF87a", False) = 0) Or (StringType.StrCmp(text4, "GIF89a", False) = 0)) Then
            Return flag4
        End If
        FileSystem.FileGet(num5, num16, -1)
        FileSystem.FileGet(num5, num15, -1)
        FileSystem.FileGet(num5, num4, -1)
        Dim flag1 As Boolean = ((num4 And 128) <> 0)
        Dim num1 As Byte = CByte(((num4 And 7) + 1))
        FileSystem.FileGet(num5, num4, -1)
        FileSystem.FileGet(num5, num4, -1)
        Dim num8 As Long = num16
        Dim num7 As Long = num15
        If flag1 Then
            buffer2 = New Byte((CInt(Math.Round(CDbl(((3 * Math.Pow(2, CDbl(num1))) - 1)))) + 1) - 1) {}
            array1 = buffer2
            FileSystem.FileGet(num5, array1, -1, False, False)
            buffer2 = DirectCast(array1, Byte())
            buffer1 = buffer2
            If ((num1 > 4) And (num1 < 8)) Then
                num1 = 8
                buffer2 = DirectCast(Utils.CopyArray(DirectCast(buffer2, Array), New Byte((CInt(Math.Round(CDbl(((3 * Math.Pow(2, CDbl(num1))) - 1)))) + 1) - 1) {}), Byte())
            ElseIf ((num1 > 2) And (num1 < 4)) Then
                num1 = 4
                buffer2 = DirectCast(Utils.CopyArray(DirectCast(buffer2, Array), New Byte((CInt(Math.Round(CDbl(((3 * Math.Pow(2, CDbl(num1))) - 1)))) + 1) - 1) {}), Byte())
            End If
            ImgBPP = num1
        End If
Label_01DD:
        FileSystem.FileGet(num5, num2, -1)
        Select Case num2
            Case 33
                FileSystem.FileGet(num5, num2, -1)
                Select Case num2
                    Case 254, 249, 1, 255
                        FileSystem.FileGet(num5, num4, -1)
                        Do While (num4 <> 0)
                            buffer3 = New Byte(((num4 - 1) + 1) - 1) {}
                            array1 = buffer3
                            FileSystem.FileGet(num5, array1, -1, False, False)
                            buffer3 = DirectCast(array1, Byte())
                            FileSystem.FileGet(num5, num4, -1)
                        Loop
                        GoTo Label_0AE6
                End Select
                Exit Select
            Case 44
                Dim num10 As Short
                Dim num11 As Short
                Dim num12 As Short
                Dim num13 As Short
                Dim num23 As Byte
                FileSystem.FileGet(num5, num11, -1)
                FileSystem.FileGet(num5, num12, -1)
                FileSystem.FileGet(num5, num13, -1)
                FileSystem.FileGet(num5, num10, -1)
                FileSystem.FileGet(num5, num4, -1)
                Dim flag3 As Boolean = ((num4 And 128) <> 0)
                Dim flag2 As Boolean = ((num4 And 64) <> 0)
                num9 = CShort((num9 + 1))
                If flag3 Then
                    num1 = CByte(((num4 And 7) + 1))
                    Dim buffer4 As Byte() = New Byte((CInt(Math.Round(CDbl(((3 * Math.Pow(2, CDbl(num1))) - 1)))) + 1) - 1) {}
                    array1 = buffer4
                    FileSystem.FileGet(num5, array1, -1, False, False)
                    buffer4 = DirectCast(array1, Byte())
                    If ((num1 > 4) And (num1 < 8)) Then
                        num1 = 8
                        buffer4 = DirectCast(Utils.CopyArray(DirectCast(buffer4, Array), New Byte((CInt(Math.Round(CDbl(((3 * Math.Pow(2, CDbl(num1))) - 1)))) + 1) - 1) {}), Byte())
                    ElseIf ((num1 > 2) And (num1 < 4)) Then
                        num1 = 4
                        buffer4 = DirectCast(Utils.CopyArray(DirectCast(buffer4, Array), New Byte((CInt(Math.Round(CDbl(((3 * Math.Pow(2, CDbl(num1))) - 1)))) + 1) - 1) {}), Byte())
                    End If
                    buffer1 = buffer4
                Else
                    buffer1 = buffer2
                End If
                FileSystem.FileGet(num5, num23, -1)
                FileSystem.FileGet(num5, num4, -1)
                buffer3 = New Byte(((num4 - 1) + 1) - 1) {}
                Dim num20 As Integer = 0
                Do While (num4 <> 0)
                    Do While (num20 <= Information.UBound(buffer3, 1))
                        FileSystem.FileGet(num5, buffer3(num20), -1)
                        num20 += 1
                    Loop
                    FileSystem.FileGet(num5, num4, -1)
                    If (num4 <> 0) Then
                        buffer3 = DirectCast(Utils.CopyArray(DirectCast(buffer3, Array), New Byte(((Information.UBound(buffer3, 1) + num4) + 1) - 1) {}), Byte())
                    End If
                Loop
                If (num9 = FrameNumber) Then
                    Dim num18 As Short
                    Dim num19 As Short
                    Dim num32 As Integer
                    ImgBPP = num1
                    ImgColor = buffer1
                    Dim num21 As Long = CLng(Math.Round(CDbl(num15) * CDbl(num16)))
                    Dim num17 As Short = CShort(Math.Round(Conversion.Int((CDbl(num16) / (8 / CDbl(num1))))))
                    If ((CDbl(num16) Mod (8 / CDbl(num1))) <> 0) Then
                        num17 = CShort((num17 + 1))
                    End If
                    Dim num14 As Short = CShort(Math.Round(Conversion.Int((CDbl(num13) / (8 / CDbl(num1))))))
                    If ((CDbl(num13) Mod (8 / CDbl(num1))) <> 0) Then
                        num14 = CShort((num14 + 1))
                    End If
                    ImgBuf = New Byte((CInt(Math.Round(CDbl(((CDbl(num15) * num17) - 1.0!)))) + 1) - 1) {}
                    Dim numArray1 As Integer() = New Integer((num10 + 1) - 1) {}
                    If flag2 Then
                        num32 = 0
                        Dim num44 As Short = num10
                        num19 = 1
                        Do While (num19 <= num44)
                            num32 += 1
                            numArray1(num32) = (num19 - 1)
                            num19 = CShort((num19 + 8))
                        Loop
                        Dim num43 As Short = num10
                        num19 = 5
                        Do While (num19 <= num43)
                            num32 += 1
                            numArray1(num32) = (num19 - 1)
                            num19 = CShort((num19 + 8))
                        Loop
                        Dim num42 As Short = num10
                        num19 = 3
                        Do While (num19 <= num42)
                            num32 += 1
                            numArray1(num32) = (num19 - 1)
                            num19 = CShort((num19 + 4))
                        Loop
                        Dim num41 As Short = num10
                        num19 = 2
                        Do While (num19 <= num41)
                            num32 += 1
                            numArray1(num32) = (num19 - 1)
                            num19 = CShort((num19 + 2))
                        Loop
                    Else
                        Dim num40 As Short = num10
                        num19 = 1
                        Do While (num19 <= num40)
                            numArray1(num19) = (num19 - 1)
                            num19 = CShort((num19 + 1))
                        Loop
                    End If
                    Dim num22 As Short = CShort(Math.Round(Math.Pow(2, CDbl(num23))))
                    Dim num28 As Short = CShort((num22 + 1))
                    Dim num26 As Short = CShort((num22 + 2))
                    Dim num24 As Byte = CByte((num23 + 1))
                    Dim num29 As Short = CShort(Math.Round(Math.Pow(2, CDbl(num24))))
                    Dim num27 As Short = num26
                    Dim textArray1 As String() = New String(4096 - 1) {}
                    Dim text1 As String = ""
                    Dim num25 As Integer = 0
                    Dim num30 As Integer = 0
                    num32 = 1
                    Dim num31 As Integer = (num30 + num11)
                    Dim num33 As Integer = (numArray1(num32) + num12)
                    num20 = 0
                    Do
                        Dim text5 As String
                        If FileSystem.EOF(num5) Then
                            Information.Err.Raise(62, Nothing, Nothing, Nothing, Nothing)
                        End If
                        Do While ((Strings.Len(text1) < num24) And (num20 <= Information.UBound(buffer3, 1)))
                            num19 = buffer3(num20)
                            text5 = ""
                            Do While (num19 <> 0)
                                text5 = (StringType.FromInteger((num19 Mod 2)) & text5)
                                num19 = CShort((num19 / 2))
                            Loop
                            text1 = (Strings.Right(("00000000" & text5), 8) & text1)
                            num20 += 1
                        Loop
                        If (num20 <= Information.UBound(buffer3, 1)) Then
                            text5 = Strings.Right(text1, num24)
                            num18 = 0
                            num19 = CShort(Math.Round(Math.Pow(2, CDbl((num24 - 1)))))
                            Dim num39 As Byte = num24
                            num4 = 1
                            Do While (num4 <= num39)
                                If (StringType.StrCmp(Strings.Mid(text5, num4, 1), "1", False) = 0) Then
                                    num18 = CShort((num18 + num19))
                                End If
                                num19 = CShort(Math.Round(CDbl((CDbl(num19) / 2))))
                                num4 = CByte((num4 + 1))
                            Loop
                            text1 = Strings.Left(text1, (Strings.Len(text1) - num24))
                        Else
                            num18 = num28
                        End If
                        If (num18 = num22) Then
                            num24 = CByte((num23 + 1))
                            num29 = CShort(Math.Round(Math.Pow(2, CDbl(num24))))
                            num27 = num26
                            num25 = 0
                        ElseIf (num18 = num28) Then
                            num18 = -1
                        ElseIf (num18 >= 0) Then
                            Dim text3 As String
                            num25 += 1
                            If (num25 > 1) Then
                                Dim text2 As String
                                If (num18 < num22) Then
                                    text2 = StringType.FromChar(Strings.Chr(num18))
                                ElseIf (num18 < num27) Then
                                    text2 = Strings.Left(textArray1(num18), 1)
                                Else
                                    text2 = Strings.Left(text3, 1)
                                End If
                                If (num27 <> 4095) Then
                                    textArray1(num27) = (text3 & text2)
                                    num27 = CShort((num27 + 1))
                                    If ((num27 + 1) > num29) Then
                                        num24 = CByte((num24 + 1))
                                        num29 = CShort(Math.Round(Math.Pow(2, CDbl(num24))))
                                    End If
                                End If
                            End If
                            If (num18 < num22) Then
                                text3 = StringType.FromChar(Strings.Chr(num18))
                            ElseIf (num18 < num27) Then
                                text3 = textArray1(num18)
                            Else
                                text3 = ""
                            End If
                            Dim num38 As Integer = Strings.Len(text3)
                            Dim num6 As Integer = 1
                            Do While (num6 <= num38)
                                num31 = (num30 + num11)
                                num21 = (((num10 - 1) - num33) * num14)
                                Dim num3 As Byte = CByte(Strings.Asc(Strings.Mid(text3, num6, 1)))
                                num21 = CLng(Math.Round(CDbl((num21 + Conversion.Int((CDbl(num31) / (8 / CDbl(num1))))))))
                                Select Case num1
                                    Case 1
                                        Select Case (num31 Mod 8)
                                            Case 1
                                                GoTo Label_09AD
                                            Case 2
                                                GoTo Label_09BE
                                            Case 3
                                                GoTo Label_09CF
                                            Case 4
                                                GoTo Label_09E0
                                            Case 5
                                                GoTo Label_09F0
                                            Case 6
                                                GoTo Label_0A00
                                            Case 7
                                                GoTo Label_0A10
                                        End Select
                                        GoTo Label_0A9A
                                    Case 2
                                        Select Case (num31 Mod 4)
                                            Case 0
                                                GoTo Label_0A38
                                            Case 1
                                                GoTo Label_0A42
                                            Case 2
                                                GoTo Label_0A53
                                            Case 3
                                                GoTo Label_0A63
                                        End Select
                                        GoTo Label_0A9A
                                    Case 3, 5, 6, 7, 8
                                        GoTo Label_0A9A
                                    Case 4
                                        Select Case (num31 Mod 2)
                                            Case 0
                                                GoTo Label_0A83
                                            Case 1
                                                GoTo Label_0A8D
                                        End Select
                                        GoTo Label_0A98
                                    Case Else
                                        GoTo Label_0A9A
                                End Select
                                num3 = CByte((num3 * 128))
                                GoTo Label_0A9A
Label_09AD:
                                num3 = CByte((ImgBuf(CInt(num21)) Or (num3 * 64)))
                                GoTo Label_0A9A
Label_09BE:
                                num3 = CByte((ImgBuf(CInt(num21)) Or (num3 * 32)))
                                GoTo Label_0A9A
Label_09CF:
                                num3 = CByte((ImgBuf(CInt(num21)) Or (num3 * 16)))
                                GoTo Label_0A9A
Label_09E0:
                                num3 = CByte((ImgBuf(CInt(num21)) Or (num3 * 8)))
                                GoTo Label_0A9A
Label_09F0:
                                num3 = CByte((ImgBuf(CInt(num21)) Or (num3 * 4)))
                                GoTo Label_0A9A
Label_0A00:
                                num3 = CByte((ImgBuf(CInt(num21)) Or (num3 * 2)))
                                GoTo Label_0A9A
Label_0A10:
                                num3 = CByte((ImgBuf(CInt(num21)) Or num3))
                                GoTo Label_0A9A
Label_0A38:
                                num3 = CByte((num3 * 64))
                                GoTo Label_0A9A
Label_0A42:
                                num3 = CByte((ImgBuf(CInt(num21)) Or (num3 * 16)))
                                GoTo Label_0A9A
Label_0A53:
                                num3 = CByte((ImgBuf(CInt(num21)) Or (num3 * 4)))
                                GoTo Label_0A9A
Label_0A63:
                                num3 = CByte((ImgBuf(CInt(num21)) Or num3))
                                GoTo Label_0A9A
Label_0A83:
                                num3 = CByte((num3 * 16))
                                GoTo Label_0A98
Label_0A8D:
                                num3 = CByte((ImgBuf(CInt(num21)) Or num3))
Label_0A98:
Label_0A9A:
                                ImgBuf(CInt(num21)) = num3
                                num30 += 1
                                If ((num30 >= num13) And (num32 < num10)) Then
                                    num30 = 0
                                    num32 += 1
                                    num33 = (numArray1(num32) + num12)
                                End If
                                num6 += 1
                            Loop
                        End If
                    Loop While (num18 >= 0)
                    flag4 = True
                End If
                Exit Select
        End Select
Label_0AE6:
        If (num2 = 59) Then
            If (num5 > 0) Then
                FileSystem.FileClose(New Integer() {num5})
            End If
            Return flag4
        End If
        GoTo Label_01DD
    End Function

    Private Function prParseJPG(ByVal Filename As String, ByRef ImgBuf As Byte()) As Long
        Dim num2 As Long
        Dim num1 As Integer = FileSystem.FreeFile
        If (num1 > 0) Then
            FileSystem.FileOpen(num1, Filename, 32, 1, 3, -1)
            ImgBuf = New Byte((CInt((FileSystem.LOF(num1) - 1)) + 1) - 1) {}
            Dim array1 As Array = ImgBuf
            FileSystem.FileGet(num1, array1, -1, False, False)
            ImgBuf = DirectCast(array1, Byte())
            FileSystem.FileClose(New Integer() {num1})
        End If
        Return num2
    End Function

    Private Function prFileJoin(ByVal Filename As String) As Byte()
        '-> cette fonction retourne le contenu du fichier en byte
        On Error GoTo gestError
        Dim num1 As Integer = FileSystem.FreeFile
        FileSystem.FileOpen(num1, Filename, 32, 1, -1, -1)
        prFileJoin = New Byte((CLng((FileSystem.LOF(num1) - 1)) + 1) - 1) {}
        Dim array1 As Array = prFileJoin
        FileSystem.FileGet(num1, array1, -1, False, False)
        prFileJoin = DirectCast(array1, Byte())
        FileSystem.FileClose(New Integer() {num1})
        Exit Function
gestError:
    End Function

    Private Sub prParseWMF(ByVal Filename As String, ByVal DestX As Single, ByVal DestY As Single, Optional ByVal Width As Single = 0.0!, Optional ByVal Height As Single = 0.0!)
        Dim text1 As New String(" "c, 4)
        Dim single4 As Single = Width
        Dim single3 As Single = Height
        Dim num6 As Integer = FileSystem.FreeFile
        FileSystem.FileOpen(num6, Filename, 32, -1, -1, -1)
        Dim flag2 As Boolean = True
        Me.prWriteObj("q")
        Do While (Not FileSystem.EOF(num6) And flag2)
            FileSystem.FileGet(num6, text1, -1, False)
            Dim num15 As Long = 14
            If (StringType.StrCmp(text1, (StringType.FromChar(Strings.Chr(215)) & StringType.FromChar(Strings.Chr(205)) & StringType.FromChar(Strings.Chr(198)) & StringType.FromChar(Strings.Chr(154))), False) = 0) Then
                num15 = (num15 + 22)
            End If
            Dim buffer1 As Byte() = New Byte((CInt((num15 - 1)) + 1) - 1) {}
            Dim array1 As Array = buffer1
            FileSystem.FileGet(num6, array1, -1, False, False)
            buffer1 = DirectCast(array1, Byte())
            flag2 = True
            Dim num12 As Long = FileSystem.Seek(num6)
            Do While (Not FileSystem.EOF(num6) And flag2)
                Dim flag1 As Boolean
                Dim flag3 As Boolean
                Dim flag4 As Boolean
                Dim flag5 As Boolean
                Dim flag7 As Boolean
                Dim num1 As Byte
                Dim num2 As Byte
                Dim num3 As Byte
                Dim num4 As Byte
                Dim num5 As Long
                Dim num7 As Integer
                Dim num9 As Integer
                Dim single1 As Single
                Dim single2 As Single
                Dim num16 As Long
                Dim objArray1 As WMFObj()
                Dim text3 As String
                Dim text4 As String
                Dim single5 As Single
                Dim single6 As Single
                Dim single8 As Single
                Dim single9 As Single
                FileSystem.Seek(num6, num12)
                FileSystem.FileGet(num6, num1, -1)
                FileSystem.FileGet(num6, num2, -1)
                FileSystem.FileGet(num6, num3, -1)
                FileSystem.FileGet(num6, num4, -1)
                Dim obj1 As Object = ((((((num4 * 256) + num3) * 256) + num2) * 256) + num1)
                FileSystem.FileGet(num6, num1, -1)
                FileSystem.FileGet(num6, num2, -1)
                Dim num8 As Long = ((num2 * 256) + num1)
                If (ObjectType.ObjTst(obj1, 3, False) > 0) Then
                    buffer1 = New Byte((IntegerType.FromObject(ObjectType.SubObj(ObjectType.MulObj(2, ObjectType.SubObj(obj1, 3)), 1)) + 1) - 1) {}
                    array1 = buffer1
                    FileSystem.FileGet(num6, array1, -1, False, False)
                    buffer1 = DirectCast(array1, Byte())
                End If
                num12 = LongType.FromObject(ObjectType.AddObj(num12, ObjectType.MulObj(2, obj1)))
                If ((Not flag3 And flag7) And flag4) Then
                    If ((Width = 0.0!) And (Height = 0.0!)) Then
                        single4 = ((20.0! * single5) / (Me.mvarKUnit * 72.0!))
                        single3 = ((20.0! * single8) / (Me.mvarKUnit * 72.0!))
                    End If
                    If (Width = 0.0!) Then
                        single4 = ((Height * single5) / single8)
                    End If
                    If (Height = 0.0!) Then
                        single3 = ((Width * single8) / single5)
                    End If
                    Dim single7 As Single = ((Me.mvarKUnit * single4) / single5)
                    Dim single10 As Single = ((Me.mvarKUnit * single3) / single8)
                    text4 = Me.prFormat("#s 0 0 #s #k #k cm #n1 0 0 1 #s #s cm", New Object() {single7, single10, DestX, DestY, -single6, -single9})
                    flag7 = False
                    flag4 = False
                End If
                If ((((num8 = 301) Or (num8 = 805)) Or (num8 = 804)) Or (num8 = 1336)) Then
                    If ((StringType.StrCmp(text4, "", False) <> 0) And Not flag3) Then
                        Me.prWriteObj(text4)
                    End If
                    flag3 = True
                End If
                Dim num21 As Long = num8
                Select Case num21
                    Case 523
                        If Not flag3 Then
                            single9 = CSng((buffer1(0) + (buffer1(1) * 256)))
                            single6 = CSng((buffer1(2) + (buffer1(3) * 256)))
                            flag7 = True
                        End If
                        Continue Do
                    Case 524
                        If Not flag3 Then
                            single8 = CSng((buffer1(0) + (buffer1(1) * 256)))
                            single5 = CSng((buffer1(2) + (buffer1(3) * 256)))
                            flag4 = True
                        End If
                        Continue Do
                    Case Else
                        If ((num21 <> 764) AndAlso (num21 <> 762)) Then
                            GoTo Label_05C2
                        End If
                        flag1 = False
                        num9 = CInt((num16 + 1))
                        If (num16 > 0) Then
                            Dim num20 As Long = Information.UBound(objArray1, 1)
                            num5 = 1
                            Do While (num5 <= num20)
                                If objArray1(CInt(num5)).Empty Then
                                    num9 = CInt(num5)
                                    flag1 = True
                                    Exit Do
                                End If
                                num5 = (num5 + 1)
                            Loop
                        End If
                        Exit Do
                End Select
                If Not flag1 Then
                    objArray1 = DirectCast(Utils.CopyArray(DirectCast(objArray1, Array), New WMFObj((num9 + 1) - 1) {}), WMFObj())
                    num16 = num9
                End If
                objArray1(num9).Empty = False
                objArray1(num9).Style = (buffer1(0) + (buffer1(1) * 256))
                If (num8 = 764) Then
                    objArray1(num9).SubType = "B"
                    objArray1(num9).R = buffer1(2)
                    objArray1(num9).G = buffer1(3)
                    objArray1(num9).b = buffer1(4)
                    Continue Do
                End If
                objArray1(num9).SubType = "P"
                objArray1(num9).Width = CSng(((72 * (buffer1(2) + (buffer1(3) * 256))) / CDbl((20.0! * Me.mvarKUnit))))
                objArray1(num9).R = buffer1(6)
                objArray1(num9).G = buffer1(7)
                objArray1(num9).b = buffer1(8)
                Continue Do
Label_05C2:
                Select Case num21
                    Case 1790, 765
                        Exit Do
                    Case Else
                        GoTo Label_06E8
                End Select
                flag1 = False
                num9 = CInt((num16 + 1))
                If (num16 > 0) Then
                    Dim num19 As Long = Information.UBound(objArray1, 1)
                    num5 = 1
                    Do While (num5 <= num19)
                        If objArray1(CInt(num5)).Empty Then
                            num9 = CInt(num5)
                            flag1 = True
                            Exit Do
                        End If
                        num5 = (num5 + 1)
                    Loop
                End If
                If Not flag1 Then
                    objArray1 = DirectCast(Utils.CopyArray(DirectCast(objArray1, Array), New WMFObj((num9 + 1) - 1) {}), WMFObj())
                    num16 = num9
                End If
                objArray1(num9).Empty = False
                objArray1(num9).SubType = "D"
                Continue Do
Label_06E8:
                Select Case num21
                    Case 262
                        num7 = (buffer1(0) + (buffer1(1) * 256))
                        Continue Do
                    Case 496
                        num9 = (buffer1(0) + (buffer1(1) * 256))
                        objArray1((num9 + 1)).Empty = True
                        Continue Do
                    Case Else
                        If (num21 <> 301) Then
                            GoTo Label_093A
                        End If
                        num9 = (buffer1(0) + (buffer1(1) * 256))
                        Dim text5 As String = objArray1((num9 + 1)).SubType
                        If (StringType.StrCmp(text5, "B", False) = 0) Then
                            flag5 = False
                            If (objArray1((num9 + 1)).Style = 1) Then
                                flag5 = True
                                Continue Do
                            End If
                            Me.prWriteObj(Me.prFormat("#c #c #c rg", New Object() {objArray1((num9 + 1)).R, objArray1((num9 + 1)).G, objArray1((num9 + 1)).b}))
                            Continue Do
                        End If
                        If (StringType.StrCmp(text5, "P", False) <> 0) Then
                            Continue Do
                        End If
                        flag5 = False
                        Select Case objArray1((num9 + 1)).Style
                            Case 0
                                Me.prWriteObj("[ ] 0 d")
                                GoTo Label_0894
                            Case 1
                                Me.prWriteObj("[3 1] 0 d")
                                GoTo Label_0894
                            Case 2
                                Me.prWriteObj("[0.5 0.5] 0 d")
                                GoTo Label_0894
                            Case 3
                                Me.prWriteObj("[2 1 0.5 1] 0 d")
                                GoTo Label_0894
                            Case 4
                                Me.prWriteObj("[2 1 0.5 1 0.5 1] 0 d")
                                GoTo Label_0894
                        End Select
                        GoTo Label_0894
                End Select
                Dim flag6 As Boolean = True
Label_0894:
                If Not flag6 Then
                    Me.prWriteObj(Me.prFormat("#c #c #c RG", New Object() {objArray1((num9 + 1)).R, objArray1((num9 + 1)).G, objArray1((num9 + 1)).b}))
                    Me.prWriteObj(Me.prFormat("#s w", New Object() {objArray1((num9 + 1)).Width}))
                End If
                Continue Do
Label_093A:
                Select Case num21
                    Case 805, 804
                        num15 = CLng(Math.Round(CDbl((4 * (buffer1(0) + (buffer1(1) * 256))))))
                        num5 = (num15 - 4)
                        Do While (num5 >= 0)
                            single1 = CSng(New Decimal((buffer1(CInt((num5 + 2))) + (buffer1(CInt((num5 + 3))) * 256))))
                            single2 = CSng(New Decimal((buffer1(CInt((num5 + 4))) + (buffer1(CInt((num5 + 5))) * 256))))
                            If (num5 = (num15 - 4)) Then
                                Me.prWriteObj(Me.prFormat("#s #s m", New Object() {single1, single2}))
                            Else
                                Me.prWriteObj(Me.prFormat("#s #s l", New Object() {single1, single2}))
                            End If
                            num5 = (num5 + -4)
                        Loop
                        text3 = ""
                        Select Case num8
                            Case 805
                                text3 = "s"
                                Exit Do
                            Case 804
                                text3 = "b"
                                If flag6 Then
                                    text3 = StringType.FromObject(Interaction.IIf(flag5, "n", "f"))
                                ElseIf flag5 Then
                                    text3 = "s"
                                End If
                                If ((num7 = 1) And ((StringType.StrCmp(text3, "b", False) = 0) Or (StringType.StrCmp(text3, "f", False) = 0))) Then
                                    text3 = (text3 & "*")
                                End If
                                Exit Do
                        End Select
                        Me.prWriteObj(text3)
                        Continue Do
                End Select
                Select Case num21
                    Case 1336
                        Dim num14 As Long = CLng(Math.Round(CDbl((2 * (buffer1(0) + (buffer1(1) * 256))))))
                        Dim num11 As Long = (2 + num14)
                        Dim num17 As Long = num14
                        Dim num10 As Long = 2
                        Do While (num10 <= num17)
                            Dim num13 As Long = CLng(Math.Round(CDbl((4 * (buffer1(CInt(num10)) + (buffer1(CInt((num10 + 1))) * 256))))))
                            num5 = (num13 - 4)
                            Do While (num5 >= 0)
                                single1 = CSng(New Decimal((buffer1(CInt((num5 + num11))) + (buffer1(CInt(((num5 + 1) + num11))) * 256))))
                                single2 = CSng(New Decimal((buffer1(CInt(((num5 + 2) + num11))) + (buffer1(CInt(((num5 + 3) + num11))) * 256))))
                                If (num5 = (num13 - 4)) Then
                                    Me.prWriteObj(Me.prFormat("#s #s m", New Object() {single1, single2}))
                                Else
                                    Me.prWriteObj(Me.prFormat("#s #s l", New Object() {single1, single2}))
                                End If
                                num5 = (num5 + -4)
                            Loop
                            num11 = (num11 + num13)
                            num10 = (num10 + 2)
                        Loop
                        text3 = "b"
                        If flag6 Then
                            text3 = StringType.FromObject(Interaction.IIf(flag5, "n", "f"))
                        ElseIf flag5 Then
                            text3 = "s"
                        End If
                        If ((num7 = 1) And ((StringType.StrCmp(text3, "b", False) = 0) Or (StringType.StrCmp(text3, "f", False) = 0))) Then
                            text3 = (text3 & "*")
                        End If
                        Me.prWriteObj(text3)
                        Continue Do
                    Case 0
                        flag2 = False
                        Exit Do
                End Select
            Loop
        Loop
        If (num6 > 0) Then
            FileSystem.FileClose(New Integer() {num6})
        End If
        Me.prWriteObj("Q")
    End Sub

    Private Sub prPath(Optional ByVal Options As pdfDrawOptions = 0)
        Dim text1 As String
        Dim flag3 As Boolean = ((Options And pdfDrawOptions.pdfFilled) <> pdfDrawOptions.pdfVisible)
        Dim flag2 As Boolean = ((Options And pdfDrawOptions.pdfClosed) <> pdfDrawOptions.pdfVisible)
        Dim flag4 As Boolean = ((Options And pdfDrawOptions.pdfInvisible) = pdfDrawOptions.pdfVisible)
        Dim flag1 As Boolean = ((Options And pdfDrawOptions.pdfClipping) <> pdfDrawOptions.pdfVisible)
        If flag3 Then
            text1 = StringType.FromObject(Interaction.IIf(flag4, RuntimeHelpers.GetObjectValue(Interaction.IIf(flag2, "b*", "B*")), "f*"))
        Else
            If flag2 Then
                text1 = "h"
            End If
            If flag4 Then
                text1 = Strings.Trim((text1 & " S"))
            End If
        End If
        If flag1 Then
            text1 = ("W " & text1)
        End If
        Me.prWriteObj(text1)
    End Sub

    Private Function prPutBookMark() As Integer
        '--> cette proc�dure permet d'inserer les bookmarks
        Dim num3 As Integer
        Dim num20 As Integer = 0
        Dim numArray1 As Integer() = New Integer(256 - 1) {}
        '-> on definit la position hierarchique de notre bookmark
        If (Me.mvarBookmarkCount > 1) Then
            Dim num2 As Integer
            num3 = CInt((Me.mvarMaxObject + 1))
            Me.arrBOOKMARK(1).Parent = num3
            Me.arrBOOKMARK(1).Obj = (num3 + 1)
            numArray1(0) = 1
            Dim num11 As Integer = Me.mvarBookmarkCount
            Dim num1 As Integer = 2
            Do While (num1 <= num11)
                Dim num10 As Integer = num1
                Me.arrBOOKMARK(num10).Obj = (num3 + num1)
                numArray1(Me.arrBOOKMARK(num10).Level) = num1
                Dim num5 As Integer = (Me.arrBOOKMARK(num10).Level + 1)
                Do While (num5 <= 255)
                    numArray1(num5) = 0
                    num5 += 1
                Loop
                Me.arrBOOKMARK(num10).Parent = Me.arrBOOKMARK(numArray1((Me.arrBOOKMARK(num10).Level - 1))).Obj
                If (Me.arrBOOKMARK(numArray1((Me.arrBOOKMARK(num10).Level - 1))).First = 0) Then
                    Me.arrBOOKMARK(numArray1((Me.arrBOOKMARK(num10).Level - 1))).First = Me.arrBOOKMARK(num10).Obj
                End If
                Me.arrBOOKMARK(numArray1((Me.arrBOOKMARK(num10).Level - 1))).Last = Me.arrBOOKMARK(num10).Obj
                If (Me.arrBOOKMARK(num10).Level = 1) Then
                    num2 += 1
                End If


                If (num1 < Me.mvarBookmarkCount) Then
                    Dim num9 As Integer = Me.mvarBookmarkCount
                    Dim num4 As Integer = (num1 + 1)
                    Do While (num4 <= num9)
                        If (Me.arrBOOKMARK(num4).Level = Me.arrBOOKMARK(num10).Level) Then
                            Me.arrBOOKMARK(num10).Follow = (num3 + num4)
                            Me.arrBOOKMARK(num4).Prev = Me.arrBOOKMARK(num10).Obj
                            GoTo Label_025E
                        End If
                        If (Me.arrBOOKMARK(num4).Level < Me.arrBOOKMARK(num10).Level) Then
                            GoTo Label_025E
                        End If
                        num4 += 1
                    Loop
                End If
Label_025E:
                num1 += 1
            Loop
            '-> on initialise l'objet de reference
            Me.prInitObj(num3)
            Me.prWriteObj(Me.prFormat("<< /Count 1 /First #i 0 R /Last #i 0 R >>", New Object() {Me.arrBOOKMARK(1).Obj, Me.arrBOOKMARK(1).Obj}))
            Me.prEndObj()
            num20 = 1
            Me.prInitObj(num3 + num20)
            '-> on ecrit l'objet de lien deal
            If pdfSite <> "" Then
                Me.prWriteObj(Me.prFormat("<< /Title #p /Parent #i 0 R /Next #i 0 R /A << /S /URI /URI #p >> /F 3 /C [0 0 1]>>", New Object() {Entry(1, pdfSite, "|"), Me.arrBOOKMARK(1).Parent, Me.arrBOOKMARK(1).Obj + num20, Entry(2, pdfSite, "|")}))
            Else
                Me.prWriteObj(Me.prFormat("<< /Title #p /Parent #i 0 R /Next #i 0 R /A << /S /URI /URI #p >> /F 3 /C [0 0 1]>>", New Object() {"Deal Informatique", Me.arrBOOKMARK(1).Parent, Me.arrBOOKMARK(1).Obj + num20, "http://www.dealmnt.com"}))
            End If
            Me.arrBOOKMARK(1).Prev = num3 + num20
            Me.prEndObj()
            Dim num8 As Integer = Me.mvarBookmarkCount
            num1 = 1
            Do While (num1 <= num8)
                Dim num7 As Integer = num1
                Me.prInitObj(Me.arrBOOKMARK(num7).Obj + num20)
                Me.prWriteObj(Me.prFormat("<< /Title #p", New Object() {Me.arrBOOKMARK(num7).Text}))
                If Me.arrBOOKMARK(num7).webLink = "" Then
                    Me.prWriteObj(Me.prFormat("/Parent #i 0 R", New Object() {Me.arrBOOKMARK(num7).Parent + num20}))
                Else
                    Me.prWriteObj(Me.prFormat("/Parent #i 0 R", New Object() {Me.arrBOOKMARK(1).Parent + num20}))
                End If
                If (Me.arrBOOKMARK(num7).First > 0) Then
                    Me.prWriteObj(Me.prFormat("/First #i 0 R", New Object() {Me.arrBOOKMARK(num7).First + num20}))
                End If
                If (Me.arrBOOKMARK(num7).Last > 0) Then
                    Me.prWriteObj(Me.prFormat("/Last #i 0 R", New Object() {Me.arrBOOKMARK(num7).Last + num20}))
                End If
                If (Me.arrBOOKMARK(num7).Prev > 0) Then
                    Me.prWriteObj(Me.prFormat("/Prev #i 0 R", New Object() {Me.arrBOOKMARK(num7).Prev + num20}))
                End If
                If (Me.arrBOOKMARK(num7).Follow > 0) Then
                    Me.prWriteObj(Me.prFormat("/Next #i 0 R", New Object() {Me.arrBOOKMARK(num7).Follow + num20}))
                End If
                If ((num1 = 1) And (num2 <> 0)) Then
                    Me.prWriteObj(Me.prFormat("/Count #i", New Object() {num2}))
                End If
                If (StringType.StrCmp(Me.arrBOOKMARK(num7).Destination, "", False) <> 0) And Me.arrBOOKMARK(num7).webLink = "" Then
                    Me.prWriteObj(Me.prFormat("/Dest #t", New Object() {Me.arrBOOKMARK(num7).Destination}))
                End If
                '-> on regarde si on a specifi� une mise en forme
                If (Me.arrBOOKMARK(num7).Format <> 0) Then
                    Me.prWriteObj(Me.prFormat("/F ") & Me.arrBOOKMARK(num7).Format)
                End If
                '-> on applique eventuellemnt une couleur
                If (Me.arrBOOKMARK(num7).color <> "") Then
                    Me.prWriteObj(Me.prFormat("/C [#C]", New Object() {Me.arrBOOKMARK(num7).color}))
                End If
                '-> on regarde si on pointe sur une url
                If (Me.arrBOOKMARK(num7).webLink <> "") Then
                    Me.prWriteObj(Me.prFormat("/A << /S /URI /URI #p >>", New Object() {Me.arrBOOKMARK(num7).webLink}))
                End If
                Me.prWriteObj(">>")
                Me.prEndObj()
                num1 += 1
            Loop
        End If
            Return num3
    End Function

    Private Function prPutFileJoin() As Integer
        Dim num2 As Integer
        If (Me.mvarFileJoin > 0) Then
            Dim num1 As Integer = 1
            Dim num4 As Integer = Me.mvarFileJoin
            '-> on boucle sur les fichiers joints
            Do While (num1 <= num4)
                Dim num3 As Integer = num1
                num2 = Me.prInitObj(-1)
                '-> on d�finit que l'on est sur une pi�ce jointe
                Me.prWriteObj(Me.prFormat("<< /F#p /EF<</F #i 0 R>> /Type /Filespec>>", New Object() {My.Computer.FileSystem.GetFileInfo(Me.arrFILEJOIN(num3)).Name, num2 + 1}))
                Me.prEndObj()
                '-> on insere le fichier joint
                num2 = Me.prInitObj(-1)
                Me.prWriteObj("<</Type /EmbeddedFile ")
                '-> on insere les informations sur le fichier
                Me.prWriteObj("/Params<<")
                Me.prWriteObj("/Size " & CStr(My.Computer.FileSystem.GetFileInfo(Me.arrFILEJOIN(num3)).Length))
                Me.prWriteObj(Me.prFormat("/ModDate (#d)", New Object() {Format(My.Computer.FileSystem.GetFileInfo(Me.arrFILEJOIN(num3)).CreationTime, "#yyyy/MM/dd hh:mm:ss#")}))
                Me.prWriteObj(">>")
                Dim buffer1 As Byte() = prFileJoin(Me.arrFILEJOIN(num3))
                Me.prPutStream(buffer1, num2, "", 0)
                Me.prEndObj()
                '-> on incremente la boucle
                num1 += 1
            Loop
        End If
        Return num2
    End Function

    Private Function prPutFonts() As String
        Dim text4 As String = ""
        If (Me.mvarFontCount > 0) Then
            Dim num7 As Integer = Me.mvarFontCount
            Dim num3 As Integer = 1
            Do While (num3 <= num7)
                Dim num6 As Integer = num3
                Dim num1 As Integer = Me.prInitObj(-1)
                Dim text3 As String = ("_FNT_" & Strings.Trim(Conversion.Str(num3)))
                text4 = (String.Concat(New String() {text4, "/", text3, " ", StringType.FromInteger(num1)}) & " 0 R " & ChrW(13))
                Me.prWriteObj(Me.prFormat("<< /Type /Font /Subtype /#t /Name /#t /BaseFont /#t /FirstChar 0 /LastChar 255", New Object() {RuntimeHelpers.GetObjectValue(Interaction.IIf(Me.arrFONT(num6).TrueTypeFont, "TrueType", "Type1")), text3, Me.arrFONT(num6).BaseFont}))
                If ((StringType.StrCmp(Strings.UCase(Me.arrFONT(num6).BaseFont), "SYMBOL", False) <> 0) And (StringType.StrCmp(Strings.UCase(Me.arrFONT(num6).BaseFont), "ZAPFDINGBATS", False) <> 0)) Then
                    Me.prWriteObj(Me.prFormat("/Encoding #i 0 R", New Object() {3}))
                End If
                If Me.arrFONT(num6).TrueTypeFont Then
                    Me.prWriteObj(Me.prFormat("/FontDescriptor #i 0 R", New Object() {(num1 + 1)}))
                    Me.prWriteObj("/Widths [")
                    Dim num4 As Integer = 0
                    Do
                        Dim text2 As String
                        text2 = (text2 & Me.prFormat("#s ", New Object() {Me.arrFONT(num6).Widths(num4)}))
                        If ((((num4 + 1) Mod 16) = 0) Or (num4 = 255)) Then
                            Me.prWriteObj(text2)
                            text2 = ""
                        End If
                        num4 += 1
                    Loop While (num4 <= 255)
                    Me.prWriteObj("]")
                    Me.prWriteObj(" >>")
                    Me.prEndObj()
                    num1 = Me.prInitObj(-1)
                    Me.prWriteObj(Me.prFormat("<< /Type /FontDescriptor /FontName /#t #t ", New Object() {Me.arrFONT(num6).BaseFont, Me.arrFONT(num6).Param}))
                    If Not Me.arrFONT(num6).Embedded Then
                        Me.prWriteObj(" >>")
                        Me.prEndObj()
                    Else
                        Me.prWriteObj(Me.prFormat("/FontFile2 #i 0 R >>", New Object() {(num1 + 1)}))
                        Me.prInitObj(-1)
                        Dim num2 As Integer = FileSystem.FreeFile
                        FileSystem.FileOpen(num2, Me.arrFONT(num6).FontFile, 32, -1, -1, -1)
                        Dim num5 As Long = FileSystem.LOF(num2)
                        Dim buffer1 As Byte() = New Byte((CInt((num5 - 1)) + 1) - 1) {}
                        Dim array1 As Array = buffer1
                        FileSystem.FileGet(num2, array1, -1, False, False)
                        buffer1 = DirectCast(array1, Byte())
                        FileSystem.FileClose(New Integer() {num2})
                        Me.prWriteObj("<<")
                        Me.prPutStream(buffer1, 0, "", Me.arrFONT(num6).Length1)
                        Me.prEndObj()
                    End If
                Else
                    Me.prWriteObj(" >>")
                    Me.prEndObj()
                End If
                        num3 += 1
                    Loop
        End If
        Return text4
    End Function

    Private Function prPutImages() As String
        Dim text4 As String = ""
        If (Me.mvarImageCount > 0) Then
            Dim num4 As Integer = Me.mvarImageCount
            Dim num2 As Integer = 1
            Do While (num2 <= num4)
                Dim buffer1 As Byte()
                Dim buffer2 As Byte()
                Dim text5 As String
                Dim num3 As Integer = num2
                Dim num1 As Integer = Me.prInitObj(-1)
                Dim text3 As String = ("_IMG_" & Strings.Trim(Conversion.Str(num2)))
                Dim text6 As String = Strings.UCase(Strings.Right(Me.arrIMAGE(num3).Filename, 4))
                If (StringType.StrCmp(text6, ".BMP", False) = 0) Then
                    Me.prParseBMP(Me.arrIMAGE(num3).Filename, buffer1, buffer2, Me.arrIMAGE(num3).BPP)
                    Me.arrIMAGE(num3).Device = "DeviceRGB"
                    text5 = "B"
                ElseIf (StringType.StrCmp(text6, ".GIF", False) = 0) Then
                    Me.prParseGIF(Me.arrIMAGE(num3).Filename, buffer1, buffer2, Me.arrIMAGE(num3).BPP, Me.arrIMAGE(num3).Frame)
                    Me.arrIMAGE(num3).Device = "DeviceRGB"
                    text5 = "G"
                ElseIf (StringType.StrCmp(text6, ".JPG", False) = 0) Then
                    Me.prParseJPG(Me.arrIMAGE(num3).Filename, buffer1)
                    text5 = "J"
                End If
                Me.prWriteObj(Me.prFormat("<< /Type /XObject /Subtype /Image /Name /#t /Width #i /Height #i", New Object() {text3, Me.arrIMAGE(num3).Width, Me.arrIMAGE(num3).Height}))
                If ((Me.arrIMAGE(num3).BPP = 24) Or (StringType.StrCmp(text5, "J", False) = 0)) Then
                    Me.prWriteObj(Me.prFormat("/BitsPerComponent 8 /ColorSpace /#t ", New Object() {Me.arrIMAGE(num3).Device}))
                ElseIf (Me.arrIMAGE(num3).BPP <= 8) Then
                    Me.prWriteObj(Me.prFormat("/BitsPerComponent #i /ColorSpace [/Indexed /#t #i #i 0 R]", New Object() {Me.arrIMAGE(num3).BPP, Me.arrIMAGE(num3).Device, (Math.Pow(2, CDbl(Me.arrIMAGE(num3).BPP)) - 1), (num1 + 1)}))
                End If
                Me.prPutStream(buffer1, 0, Me.arrIMAGE(num3).Filter, 0)
                Me.prEndObj()
                text4 = (String.Concat(New String() {text4, "/", text3, " ", StringType.FromInteger(num1)}) & " 0 R " & ChrW(13))
                If ((Me.arrIMAGE(num3).BPP <= 8) And (StringType.StrCmp(text5, "J", False) <> 0)) Then
                    Me.prInitObj(-1)
                    Me.prWriteObj("<<")
                    Me.prPutStream(buffer2, 0, Me.arrIMAGE(num3).Filter, 0)
                    Me.prEndObj()
                End If
                num2 += 1
            Loop
        End If
        Return text4
    End Function

    Private Function prPutPattern() As String
        Dim text2 As String = ""
        If (Me.mvarShadingCount > 0) Then
            Dim num4 As Integer = Me.mvarShadingCount
            Dim num1 As Integer = 1
            Do While (num1 <= num4)
                Dim num3 As Integer = num1
                Dim num2 As Integer = Me.prInitObj(-1)
                Me.prWriteObj(Me.arrSHADING(num3).Param)
                If (StringType.StrCmp(Me.arrSHADING(num3).SubType, "G", False) <> 0) Then
                    Dim buffer1 As Byte() = DirectCast(Me.prStr2Mtx(Me.arrSHADING(num3).Stream), Byte())
                    Me.prPutStream(buffer1, num2, "", 0)
                End If
                Me.prEndObj()
                text2 = (text2 & Me.prFormat("/#t #i 0 R ", New Object() {Me.arrSHADING(num3).Name, num2}) & ChrW(13))
                num1 += 1
            Loop
        End If
        Return text2
    End Function

    Private Sub prPutStream(ByRef InBuf As Byte(), Optional ByVal ObjId As Integer = 0, Optional ByVal Filter As String = "", Optional ByVal FontLength As Long = 0)
        Dim text1 As String = Filter
        If (StringType.StrCmp(Filter, "", False) = 0) Then
            If ((Me.mvarCompression And pdfCompression.pdfRunLengthDecode) <> pdfCompression.pdfNothing) Then
                text1 = ("/RunLengthDecode " & text1)
                InBuf = DirectCast(Me.prToRLD(InBuf), Byte())
            End If
            If ((Me.mvarCompression And pdfCompression.pdfLZWDecode) <> pdfCompression.pdfNothing) Then
                text1 = ("/LZWDecode " & text1)
                InBuf = DirectCast(Me.prToLZW(InBuf), Byte())
            End If
        End If
        If ((Me.mvarCompression And pdfCompression.pdfASCII85Decode) <> pdfCompression.pdfNothing) Then
            text1 = ("/ASCII85Decode " & text1)
            InBuf = DirectCast(Me.prToASCII85(InBuf), Byte())
        End If
        If ((Me.mvarCompression And pdfCompression.pdfASCIIHexDecode) <> pdfCompression.pdfNothing) Then
            text1 = ("/ASCIIHexDecode " & text1)
            InBuf = DirectCast(Me.prToASCIIHex(InBuf), Byte())
        End If
        If ((Me.mvarCompression And pdfCompression.pdfDeflate) <> pdfCompression.pdfNothing) Then
            text1 = ("/FlateDecode " & text1)
            InBuf = DirectCast(Me.prDeflate(InBuf), Byte())
            'Dim aDeflate As ClsDeflate
            'aDeflate = New ClsDeflate
            'Dim inLen As Integer
            'InBuf = aDeflate.SsDeflate(InBuf, InBuf.Length, inLen)
        End If
        If Me.mvarEncryption Then
            InBuf = DirectCast(Me.prRC4(Me.zzFromId2Key(ObjId), InBuf), Byte())
        End If
        If (StringType.StrCmp(text1, "", False) <> 0) Then
            Me.prWriteObj(Me.prFormat("/Filter [#t]", New Object() {text1}))
        End If
        If (FontLength > 0) Then
        End If
        Me.prWriteObj(Me.prFormat("/Length #i >>", New Object() {(Information.UBound(InBuf, 1) + 1)}))
        Me.prWriteObj("stream")
        Dim num2 As Long = Information.UBound(InBuf, 1)
        Dim num1 As Long = 0
        'Do While (num1 <= num2)
        '    FileSystem.Print(Me.mvarFileNumber, New Object() {Strings.Chr(InBuf(CInt(num1)))})
        '    num1 = (num1 + 1)
        '    Application.DoEvents()
        'Loop
        'FileSystem.PrintLine(Me.mvarFileNumber, New Object(0 - 1) {})
        Me.prWriteObj(Strings.Left(Encoding.GetEncoding(&H4E4).GetString(InBuf), (num2 + 1)))
        Me.prWriteObj("endstream")
    End Sub

    Private Function zzFromId2Key(ByVal ObjId As Integer) As String
        If (ObjId = 0) Then
            ObjId = Me.mvarIdObj
        End If
        Dim str As String = Strings.Right(("000000" & Strings.Trim(Conversion.Hex(ObjId))), 6)
        Dim strMessage As String = String.Concat(New String() {Me.mvarEncryptionKey, Conversions.ToString(Strings.Chr(CInt(Math.Round(Conversion.Val(("&H" & Strings.Mid(str, 5, 2))))))), Conversions.ToString(Strings.Chr(CInt(Math.Round(Conversion.Val(("&H" & Strings.Mid(str, 3, 2))))))), Conversions.ToString(Strings.Chr(CInt(Math.Round(Conversion.Val(("&H" & Strings.Mid(str, 1, 2))))))), ChrW(0) & ChrW(0)})
        Return Strings.Left(Me.prMD5(strMessage), 10)
    End Function

    Private Function prPutXobject() As String
        Dim text2 As String = ""
        If (Me.mvarObjectCount > 0) Then
            Dim num4 As Integer = Me.mvarObjectCount
            Dim num1 As Integer = 1
            Do While (num1 <= num4)
                Dim num3 As Integer = num1
                Dim num2 As Integer = Me.prInitObj(-1)
                Me.prWriteObj(Me.prFormat("<< /Type /XObject /Subtype /Form /FormType 1 /Name /#t", New Object() {Me.arrOBJECT(num3).Name}))
                Me.prWriteObj(Me.prFormat("/BBox [#s #s #s #s]", New Object() {Me.mvarMarginL, Me.mvarMarginT, (Me.mvarPageWidth - Me.mvarMarginR), (Me.mvarPageHeight - Me.mvarMarginB)}))
                Me.prWriteObj("/Matrix [1 0 0 1 0 0]")
                Dim buffer1 As Byte() = DirectCast(Me.prStr2Mtx(Me.arrOBJECT(num3).Stream), Byte())
                Me.prPutStream(buffer1, num2, "", 0)
                Me.prEndObj()
                text2 = (text2 & Me.prFormat("/#t #i 0 R ", New Object() {Me.arrOBJECT(num3).Name, num2}) & ChrW(13))
                num1 += 1
            Loop
        End If
        Return text2
    End Function

    Public Function prRC4new(ByVal Password As String, ByVal Expression2 As Byte()) As Object
        'On Error Resume Next
        Dim RB(255) As Short
        Dim Y, X, Z As Integer
        Dim Key() As Byte
        Dim ByteArray() As Byte
        Dim Temp As Byte
        Dim expression As String
        expression = prMtx2Str(Expression2)
        If Len(Password) = 0 Then
            Exit Function
        End If
        If Len(expression) = 0 Then
            Exit Function
        End If
        If Len(Password) > 256 Then
            Key = System.Text.UnicodeEncoding.Unicode.GetBytes(StrConv(Strings.Left(Password, 256), 128))
        Else
            Key = prStr2Mtx(Password)
        End If
        For X = 0 To 255
            RB(X) = X
        Next X
        X = 0
        Y = 0
        Z = 0
        For X = 0 To 255
            Y = (Y + RB(X) + Key(X Mod Len(Password))) Mod 256
            Temp = RB(X)
            RB(X) = RB(Y)
            RB(Y) = Temp
        Next X
        X = 0
        Y = 0
        Z = 0
        ByteArray = prStr2Mtx(expression)
        For X = 0 To Len(expression) - 1
            Y = (Y + 1) Mod 256
            Z = (Z + RB(Y)) Mod 256
            Temp = RB(Y)
            RB(Y) = RB(Z)
            RB(Z) = Temp
            ByteArray(X) = ByteArray(X) Xor (RB((RB(Y) + RB(Z)) Mod 256))
        Next X
        prRC4new = ByteArray
    End Function


    Private Function prRC4(ByVal key As String, ByRef InBuf As Byte()) As Object
        Dim num3 As Long
        Dim num5 As Long
        Dim num6 As Integer
        Dim buffer1(255) As Short
        If (Operators.CompareString(Me.mvarRC4LastKey, key, False) <> 0) Then
            num3 = 0
            Do
                Me.mvarRC4Code(CInt(num3)) = CByte(num3)
                num3 += 1
            Loop While (num3 <= 255)
            num5 = Strings.Len(key)
            Dim buffer3 As Byte() = DirectCast(Me.prStr2Mtx(key), Byte())
            Dim num4 As Byte = 0
            num3 = 0
            Do
                num6 = Me.mvarRC4Code(CInt(num3))
                num4 = CByte((((num4 + num6) + buffer3(CInt((num3 Mod num5)))) Mod 256))
                Me.mvarRC4Code(CInt(num3)) = Me.mvarRC4Code(num4)
                Me.mvarRC4Code(num4) = CByte(num6)
                num3 += 1
            Loop While (num3 <= 255)
            Me.mvarRC4LastKey = key
        End If
        num3 = 0
        Do
            buffer1(CInt(num3)) = Me.mvarRC4Code(CInt(num3))
            num3 += 1
        Loop While (num3 <= 255)
        num5 = (Information.UBound(InBuf, 1) + 1)
        Dim num1 As Byte = 0
        Dim num2 As Byte = 0
        If (num5 = 0) Then
            Dim obj1 As Object
            Return obj1
        End If
        Dim buffer2 As Byte() = New Byte(((CInt(num5) - 1) + 1) - 1) {}
        Dim num7 As Long = (num5 - 1)
        num3 = 0
        Do While (num3 <= num7)
            num1 = CByte(((num1 + 1) Mod 256))
            num6 = buffer1(num1)
            num2 = CByte(((num2 + num6) Mod 256))
            buffer1(num1) = buffer1(num2)
            buffer1(num2) = CByte(num6)
            buffer2(CInt(num3)) = CByte((InBuf(CInt(num3)) Xor buffer1(((buffer1(num1) + buffer1(num2)) Mod 256))))
            num3 += 1
        Loop
        Return buffer2
    End Function

    Private Sub prReportError(ByVal Number As Integer, ByVal Description As String, ByVal Section As String, Optional ByVal Other As String = "")
        Dim num1 As Integer = Number
        If (num1 = 70) Then
            Description = ("Le fichier " & Other & " est d�ja" & ChrW(224) & " ouvert")
        Else
            Description = String.Concat(New String() {"Errore numero ", StringType.FromInteger(Number), " (", Description, ") "})
        End If
        Interaction.MsgBox(Description, 0, ("TurboPDF - " & ChrW(169) & " Deal Informatique : " & Section))
        ProjectData.EndApp()
    End Sub

    Private Sub prSaveState()
        Me.intGraphState += 1
        Me.arrGRAPHSTATE = DirectCast(Utils.CopyArray(DirectCast(Me.arrGRAPHSTATE, Array), New GraphicState((Me.intGraphState + 1) - 1) {}), GraphicState())
        Dim num1 As Integer = Me.intGraphState
        Me.arrGRAPHSTATE(num1).ColorFill = Me.mvarColorFill
        Me.arrGRAPHSTATE(num1).ColorFillStencil = Me.mvarColorFillStencil
        Me.arrGRAPHSTATE(num1).ColorLine = Me.mvarColorLine
        Me.arrGRAPHSTATE(num1).ColorLineStencil = Me.mvarColorLineStencil
        Me.arrGRAPHSTATE(num1).ColorText = Me.mvarColorText
        Me.arrGRAPHSTATE(num1).ColorTextStencil = Me.mvarColorTextStencil
        Me.arrGRAPHSTATE(num1).LineWidth = Me.mvarLineWidth
        Me.arrGRAPHSTATE(num1).PosX = Me.mvarCurrX
        Me.arrGRAPHSTATE(num1).PosY = Me.mvarCurrY
        Me.arrGRAPHSTATE(num1).ScaleUnit = Me.mvarKUnit
        Me.arrGRAPHSTATE(num1).FontFamily = Me.mvarFontFamily
        Me.arrGRAPHSTATE(num1).FontSize = Me.mvarFontSize
        Me.arrGRAPHSTATE(num1).FontStyle = Me.mvarFontStyle
    End Sub

    Private Sub prSetReaderSpec(ByVal NewSpec As pdfSpecOptions)
        If (Me.mvarSpecOptions < NewSpec) Then
            Me.mvarSpecOptions = NewSpec
        End If
    End Sub

    Private Function prStr2Mtx(ByVal Word As String) As Object
        Dim num2 As Long = Strings.Len(Word)
        Dim buffer1 As Byte() = New Byte((CInt((num2 - 1)) + 1) - 1) {}
        Dim num3 As Long = num2
        Dim num1 As Long = 1
        Do While (num1 <= num3)
            buffer1(CInt((num1 - 1))) = CByte(Strings.Asc(Strings.Mid(Word, CInt(num1), 1)))
            num1 = (num1 + 1)
        Loop
        Return buffer1
    End Function

    Private Function prToASCII85(ByRef InBuf As Byte()) As Object
        Dim num5 As Long = Information.UBound(InBuf, 1)
        Dim buffer1 As Byte() = New Byte((CInt(Math.Round(CDbl((Conversion.Int(((num5 + 1) * 1.3)) + 7)))) + 1) - 1) {}
        Dim num3 As Long = 0
        Dim num2 As Long = 0
        Do While (num3 <= num5)
            Dim num4 As Long = ((num5 - num3) + 1)
            Dim num6 As Double = 0
            Dim num1 As Byte = 1
            Do
                If (num1 > num4) Then
                    Exit Do
                End If
                num6 = (num6 + (InBuf(CInt(((num3 + num1) - 1))) * Math.Pow(256, CDbl((4 - num1)))))
                num1 = CByte((num1 + 1))
            Loop While (num1 <= 4)
            If ((num6 = 0) And (num4 = 4)) Then
                buffer1(CInt(num2)) = 122
                num2 = (num2 + 1)
            Else
                num1 = 1
                Do
                    Dim num7 As Long = CLng(Math.Round(Conversion.Int((num6 / Math.Pow(85, CDbl((5 - num1)))))))
                    num6 = (num6 - (num7 * Math.Pow(85, CDbl((5 - num1)))))
                    buffer1(CInt(((num2 + num1) - 1))) = CByte((33 + num7))
                    num1 = CByte((num1 + 1))
                Loop While (num1 <= 4)
                buffer1(CInt(((num2 + 5) - 1))) = CByte(Math.Round(CDbl((33 + num6))))
                num2 = LongType.FromObject(ObjectType.AddObj(num2, Interaction.IIf((num4 < 4), (num4 + 1), 5)))
            End If
            num3 = (num3 + 4)
        Loop
        buffer1(CInt(num2)) = 126
        buffer1(CInt((num2 + 1))) = 62
        Return DirectCast(Utils.CopyArray(DirectCast(buffer1, Array), New Byte((CInt((num2 + 1)) + 1) - 1) {}), Byte())
    End Function

    Private Function prToASCIIHex(ByRef InBuf As Byte()) As Object
        Dim num3 As Long = Information.UBound(InBuf, 1)
        Dim buffer1 As Byte() = New Byte((CInt(((num3 * 2) + 1)) + 1) - 1) {}
        Dim num1 As Long = 0
        Dim num4 As Long = num3
        Dim num2 As Long = 0
        Do While (num2 <= num4)
            buffer1(CInt(num1)) = CByte(Strings.Asc(Conversion.Hex((CDbl((InBuf(CInt(num2)) And 240)) / 16))))
            buffer1(CInt((num1 + 1))) = CByte(Strings.Asc(Conversion.Hex((InBuf(CInt(num2)) And 15))))
            num1 = (num1 + 2)
            num2 = (num2 + 1)
        Loop
        Return buffer1
    End Function

    Private Function prToLZW(ByRef InBuf As Byte()) As Object
        Dim num4 As Integer = CInt(Math.Round(CDbl((Math.Pow(2, CDbl(Me.mvarCompressionQuality)) - 1))))
        Dim num5 As Integer = CInt(Math.Round(Math.Pow(2, CDbl(((Me.mvarCompressionQuality - 9) + 1)))))
        Dim numArray1 As Integer() = New Integer(5022 - 1) {}
        Dim numArray2 As Integer() = New Integer(5022 - 1) {}
        Dim buffer1 As Byte() = New Byte(5022 - 1) {}
        Dim num13 As Long = Information.UBound(InBuf, 1)
        Dim buffer2 As Byte() = New Byte((CInt(Math.Round(CDbl((Conversion.Int(((num13 + 1) * 1.5)) + 1)))) + 1) - 1) {}
        Dim num7 As Integer = num4
        Dim num2 As Byte = 9
        Dim num12 As Long = 0
        Dim num15 As Long = -1
        Dim num11 As Integer = InBuf(CInt(num12))
        Do While (num12 <= num13)
            Dim num1 As Byte
            Dim num8 As Integer
            Dim text2 As String
            If (num7 = num4) Then
                num7 = 257
                Dim num6 As Integer = 0
                Do
                    numArray1(num6) = -1
                    num6 += 1
                Loop While (num6 <= 5021)
                text2 = (text2 & Strings.Right("000100000000", num2))
                num2 = 9
            End If
            If (num12 <> num13) Then
                Dim num10 As Integer
                num1 = InBuf(CInt((num12 + 1)))
                num8 = ((num1 * num5) Xor num11)
                If (num8 = 0) Then
                    num10 = 1
                Else
                    num10 = (5021 - num8)
                End If
                Do While (1 <> 0)
                    If ((numArray1(num8) = -1) OrElse ((numArray2(num8) = num11) And (buffer1(num8) = num1))) Then
                        Exit Do
                    End If
                    num8 = (num8 - num10)
                    If (num8 < 0) Then
                        num8 = (num8 + 5021)
                    End If
                    Application.DoEvents()
                Loop
            End If
            If ((numArray1(num8) <> -1) And (num12 <> num13)) Then
                num11 = numArray1(num8)
            Else
                Dim num14 As Integer
                num7 += 1
                numArray1(num8) = num7
                numArray2(num8) = num11
                buffer1(num8) = num1
                Dim num9 As Integer = num11
                Dim text1 As String = ""
                Do While (num9 <> 0)
                    text1 = (StringType.FromInteger((num9 Mod 2)) & text1)
                    num9 = (num9 / 2)
                Loop
                text2 = (text2 & Strings.Right(("000000000000" & text1), num2))
                If (num14 < num7) Then
                    num14 = num7
                End If
                If (num7 = 511) Then
                    num2 = 10
                ElseIf (num7 = 1023) Then
                    num2 = 11
                ElseIf (num7 = 2047) Then
                    num2 = 12
                End If
                num11 = num1
            End If
            num12 = (num12 + 1)
            If (num12 > num13) Then
                text2 = (text2 & Strings.Right("000100000001", num2))
            End If
            Do While (Strings.Len(text2) >= 8)
                num15 = (num15 + 1)
                buffer2(CInt(num15)) = 0
                If (StringType.StrCmp(Strings.Mid(text2, 1, 1), "1", False) = 0) Then
                    buffer2(CInt(num15)) = CByte((buffer2(CInt(num15)) + 128))
                End If
                If (StringType.StrCmp(Strings.Mid(text2, 2, 1), "1", False) = 0) Then
                    buffer2(CInt(num15)) = CByte((buffer2(CInt(num15)) + 64))
                End If
                If (StringType.StrCmp(Strings.Mid(text2, 3, 1), "1", False) = 0) Then
                    buffer2(CInt(num15)) = CByte((buffer2(CInt(num15)) + 32))
                End If
                If (StringType.StrCmp(Strings.Mid(text2, 4, 1), "1", False) = 0) Then
                    buffer2(CInt(num15)) = CByte((buffer2(CInt(num15)) + 16))
                End If
                If (StringType.StrCmp(Strings.Mid(text2, 5, 1), "1", False) = 0) Then
                    buffer2(CInt(num15)) = CByte((buffer2(CInt(num15)) + 8))
                End If
                If (StringType.StrCmp(Strings.Mid(text2, 6, 1), "1", False) = 0) Then
                    buffer2(CInt(num15)) = CByte((buffer2(CInt(num15)) + 4))
                End If
                If (StringType.StrCmp(Strings.Mid(text2, 7, 1), "1", False) = 0) Then
                    buffer2(CInt(num15)) = CByte((buffer2(CInt(num15)) + 2))
                End If
                If (StringType.StrCmp(Strings.Mid(text2, 8, 1), "1", False) = 0) Then
                    buffer2(CInt(num15)) = CByte((buffer2(CInt(num15)) + 1))
                End If
                If (Strings.Len(text2) > 8) Then
                    text2 = Strings.Mid(text2, 9)
                Else
                    text2 = ""
                End If
                Dim num3 As Byte = CByte(Strings.Len(text2))
                If (((num12 > num13) And (num3 > 0)) And (num3 < 8)) Then
                    text2 = Strings.Left((text2 & "00000000"), 8)
                End If
            Loop
        Loop
        Return DirectCast(Utils.CopyArray(DirectCast(buffer2, Array), New Byte((CInt(num15) + 1) - 1) {}), Byte())
    End Function

    Private Function prToMatrix(ByVal X As Single, ByVal Y As Single, ByVal Angle As Single) As String
        If ((Angle Mod 360.0!) <> 0.0!) Then
            Dim single3 As Single = CSng(Math.Sin(((-3.1415926535897931 * Angle) / 180)))
            Dim single1 As Single = CSng(Math.Cos(((-3.1415926535897931 * Angle) / 180)))
            Return String.Concat(New String() {Me.prToStr(single1, 3), " ", Me.prToStr(single3, 3), " ", Me.prToStr(single3, 3), " ", Me.prToStr(-single1, 3), " ", Me.prToStr(X, 3), " ", Me.prToStr(Y, 3), " Tm"})
        End If
        Return String.Concat(New String() {"1 0 0 -1 ", Me.prToStr(X, 3), " ", Me.prToStr(Y, 3), " Tm"})
    End Function

    Private Function prToPdfStr(ByVal TEMP As String) As String
        '-> cette fonction permet de traiter les caract�res sp�ciaux du format pdf
        TEMP = Strings.Replace(Strings.Replace(Strings.Replace(TEMP, "\", "\\", 1, -1, 0), "(", "\(", 1, -1, 0), ")", "\)", 1, -1, 0)
        'TEMP = Replace(TEMP, ChrW(13), "\r")
        'TEMP = Replace(TEMP, ChrW(9), "\t")
        'TEMP = Replace(TEMP, ChrW(12), "\f")
        'TEMP = Replace(TEMP, ChrW(10), "\n")
        Return TEMP
    End Function

    Private Function prToRLD(ByRef InBuf As Byte()) As Object
        Dim num3 As Long = Information.UBound(InBuf, 1)
        Dim buffer1 As Byte() = New Byte((CInt(Math.Round(Conversion.Int((1.2 * (num3 + 1))))) + 1) - 1) {}
        Dim num1 As Long = 0
        Dim num5 As Byte = 0
        Dim num2 As Long = 0
        Do While (num2 <= num3)
            num1 = (num1 + 1)
            Dim num4 As Long = num1
            If (num2 = num3) Then
                buffer1(CInt((num4 - 1))) = 0
                buffer1(CInt(num4)) = InBuf(CInt(num2))
                num1 = (num1 + 2)
                num2 = (num2 + 1)
                Continue Do
            End If
            If (InBuf(CInt(num2)) = InBuf(CInt((num2 + 1)))) Then
                num5 = 1
                Do While ((InBuf(CInt(num2)) = InBuf(CInt((num2 + 1)))) And (num5 < 128))
                    num2 = (num2 + 1)
                    num5 = CByte((num5 + 1))
                    If (num2 = num3) Then
                        Exit Do
                    End If
                Loop
                buffer1(CInt((num4 - 1))) = CByte((257 - num5))
                buffer1(CInt(num4)) = InBuf(CInt(num2))
                num1 = (num4 + 1)
                num2 = (num2 + 1)
                Continue Do
            End If
            num5 = 0
            Do While ((InBuf(CInt(num2)) <> InBuf(CInt((num2 + 1)))) And (num5 < 128))
                num1 = (num1 + 1)
                buffer1(CInt((num1 - 1))) = InBuf(CInt(num2))
                num2 = (num2 + 1)
                num5 = CByte((num5 + 1))
                If (num2 = num3) Then
                    Exit Do
                End If
            Loop
            If (num2 = num3) Then
                num5 = CByte((num5 + 1))
                num1 = (num1 + 1)
                buffer1(CInt((num1 - 1))) = InBuf(CInt(num3))
                num2 = (num2 + 1)
            End If
            buffer1(CInt((num4 - 1))) = CByte((num5 - 1))
        Loop
        buffer1(CInt(num1)) = 128
        Return DirectCast(Utils.CopyArray(DirectCast(buffer1, Array), New Byte((CInt(num1) + 1) - 1) {}), Byte())
    End Function

    Private Function prToStr(ByVal Valore As Object, Optional ByVal Dec As Integer = 3) As String
        Dim objArray1 As Object() = New Object() {RuntimeHelpers.GetObjectValue(Valore), Dec}
        Dim flagArray1 As Boolean() = New Boolean() {True, True}
        If flagArray1(1) Then
            Dec = IntegerType.FromObject(objArray1(1))
        End If
        If flagArray1(0) Then
            Valore = RuntimeHelpers.GetObjectValue(objArray1(0))
        End If
        Return Strings.Replace(StringType.FromObject(LateBinding.LateGet(Nothing, GetType(Math), "Round", objArray1, Nothing, flagArray1)), ",", ".", 1, -1, 0)
    End Function

    Private Sub prWriteObj(ByVal [Text] As String)
        '-> fonction qui ecrit le pdf!!

        '-> on en profite pour donner la main �  la cpu
        Application.DoEvents()
        If Me.mvarInPage Then
            'Dim buffer1 As Byte() = DirectCast(Me.prStr2Mtx([Text]), Byte())
            'Dim num2 As Long = Information.UBound(buffer1, 1)
            'If (num2 >= 0) Then
            '    Dim num3 As Long = Information.UBound(Me.mvarStreamBuffer, 1)
            '    If (num3 <> 0) Then
            '        Me.mvarStreamBuffer = DirectCast(Utils.CopyArray(DirectCast(Me.mvarStreamBuffer, Array), New Byte((CInt(((num3 + num2) + 2)) + 1) - 1) {}), Byte())
            '        Me.mvarStreamBuffer(CInt((num3 + 1))) = 13
            '        Me.mvarStreamBuffer(CInt((num3 + 2))) = 10
            '        num3 = (num3 + 2)
            '    Else
            '        Me.mvarStreamBuffer = New Byte((CInt(num2) + 1) - 1) {}
            '    End If
            '    Dim num4 As Integer = CInt(num2)
            '    Dim num1 As Integer = 0
            '    Do While (num1 <= num4)
            '        Me.mvarStreamBuffer((num1 + CInt(num3))) = buffer1(num1)
            '        num1 += 1
            '        Application.DoEvents()
            '    Loop
            'End If
            Dim text1 As String = ChrW(13) & [Text]
            Dim num As Integer = (Me.mvarBufferPage.Pointer + Strings.Len(text1))
            If (num > Me.mvarBufferPage.BufferLen) Then
                Me.mvarBufferPage.Buffer = (Me.mvarBufferPage.Buffer & Strings.Space(num))
                Me.mvarBufferPage.BufferLen = Strings.Len(Me.mvarBufferPage.Buffer)
            End If
            StringType.MidStmtStr(Me.mvarBufferPage.Buffer, CInt(Me.mvarBufferPage.Pointer), &H7FFFFFFF, text1)
            Me.mvarBufferPage.Pointer = num

        ElseIf Me.mvarInObject Then
            Me.arrOBJECT(Me.mvarObjectCount).Stream = (Me.arrOBJECT(Me.mvarObjectCount).Stream & ChrW(13) & [Text])
        ElseIf Me.mvarInPattern Then
            Me.arrSHADING(Me.mvarShadingCount).Stream = (Me.arrSHADING(Me.mvarShadingCount).Stream & ChrW(13) & [Text])
        Else
            FileSystem.PrintLine(Me.mvarFileNumber, New Object() {[Text]})
        End If

    End Sub

    Public Sub SetAlpha(ByVal value As Integer)
        Dim single1 As Single = CSng((CDbl(value) / 100))
        If (single1 < 0.0!) Then
            single1 = 0.0!
        End If
        If (single1 > 100.0!) Then
            single1 = 100.0!
        End If
        Me.mvarAlphaStateCount += 1
        Me.arrALPHASTATE = DirectCast(Utils.CopyArray(DirectCast(Me.arrALPHASTATE, Array), New String((Me.mvarAlphaStateCount + 1) - 1) {}), String())
        Me.arrALPHASTATE(Me.mvarAlphaStateCount) = Me.prFormat("<</Type /ExtGState /ca #s /CA #s /BM /Normal >>", New Object() {single1, single1})
        Me.prWriteObj(Me.prFormat("/GSTATE_#i gs", New Object() {Me.mvarAlphaStateCount}))
        Me.prSetReaderSpec(pdfSpecOptions.pdfAcrobat50)
    End Sub

    Public Sub SetCharSpacing(ByVal w As Single)
        Me.mvarCharSpacing = w
        Me.prWriteObj(Me.prFormat("#s Tc", New Object() {Me.mvarCharSpacing}))
    End Sub

    Public Sub setPermission()
        '-> on autorise les modifications des annotations
        Me.mvarPermissionAnnotation = True
    End Sub

    Public Sub SetColorFill(ByVal Color As String)
        If ((StringType.StrCmp(Color, "", False) <> 0) And Not Me.mvarInStencil) Then
            Dim text1 As String
            Me.prParseColor(Color, text1)
            Me.mvarColorFill = Color
            If (StringType.StrCmp(Strings.Left(Color, 1), "#", False) = 0) Then
                Me.prWriteObj(Me.prFormat("#C rg", New Object() {Color}))
            ElseIf (StringType.StrCmp(text1, "", False) <> 0) Then
                If (StringType.StrCmp(Strings.Left(text1, 1), "#", False) = 0) Then
                    Me.mvarColorFillStencil = text1
                    Me.prWriteObj(Me.prFormat("/xxCSxx cs #C /#t scn", New Object() {text1, Color}))
                End If
            Else
                Me.prWriteObj(Me.prFormat("/Pattern cs /#t scn", New Object() {Color}))
            End If
        Else
            Me.mvarColorFill = ""
        End If
    End Sub

    Public Sub SetColorLine(ByVal Color As String)
        If ((StringType.StrCmp(Color, "", False) <> 0) And Not Me.mvarInStencil) Then
            Dim text1 As String
            Me.prParseColor(Color, text1)
            Me.mvarColorLine = Color
            If (StringType.StrCmp(Strings.Left(Color, 1), "#", False) = 0) Then
                Me.prWriteObj(Me.prFormat("#C RG", New Object() {Color}))
            ElseIf (StringType.StrCmp(text1, "", False) <> 0) Then
                If (StringType.StrCmp(Strings.Left(text1, 1), "#", False) = 0) Then
                    Me.mvarColorLineStencil = text1
                    Me.prWriteObj(Me.prFormat("/xxCSxx CS #C /#t SCN", New Object() {text1, Color}))
                End If
            Else
                Me.prWriteObj(Me.prFormat("/Pattern CS /#t SCN", New Object() {Color}))
            End If
        End If
    End Sub

    Public Sub SetColorText(ByVal Color As String)
        If ((StringType.StrCmp(Color, "", False) <> 0) And Not Me.mvarInStencil) Then
            Me.prParseColor(Color, Me.mvarColorTextStencil)
            Me.mvarColorText = Color
        End If
    End Sub

    Public Sub SetCompression(ByVal CompressionMode As pdfCompression, Optional ByVal Quality As Byte = 3)
        Me.mvarCompression = CompressionMode
        If (Quality > 3) Then
            Quality = 3
        End If
        Me.mvarCompressionQuality = CByte((9 + Quality))
    End Sub

    Public Sub SetDash(Optional ByVal Pattern As String = "", Optional ByVal Phase As Single = 0.0!)
        Dim text1 As String
        Pattern = Strings.UCase(Strings.Trim(Pattern))
        If ((StringType.StrCmp(Pattern, "", False) = 0) Or (StringType.StrCmp(Pattern, "SOLID", False) = 0)) Then
            text1 = ""
            Phase = 0.0!
        ElseIf (StringType.StrCmp(Pattern, "DASH", False) = 0) Then
            text1 = Me.prFormat("#k", New Object() {0.1})
        ElseIf (StringType.StrCmp(Pattern, "DOT", False) = 0) Then
            text1 = Me.prFormat("#k", New Object() {0.05})
        ElseIf (StringType.StrCmp(Pattern, "DASHDOT", False) = 0) Then
            text1 = Me.prFormat("#k #k #k #k", New Object() {0.1, 0.05, 0.05, 0.05})
        ElseIf (StringType.StrCmp(Pattern, "DASHDOTDOT", False) = 0) Then
            text1 = Me.prFormat("#k #k #k #k #k #k", New Object() {0.1, 0.05, 0.05, 0.05, 0.05, 0.05})
        Else
            Dim textArray1 As String() = Strings.Split(Pattern, ";", -1, 0)
            Dim num2 As Integer = Information.UBound(textArray1, 1)
            Dim num1 As Integer = 0
            Do While (num1 <= num2)
                text1 = (text1 & " " & Me.prFormat("#k", New Object() {Conversion.Val(textArray1(num1))}))
                num1 += 1
                Application.DoEvents()
            Loop
        End If
        Me.prWriteObj(Me.prFormat("[#t ] #k d", New Object() {text1, Phase}))
    End Sub

    Public Sub SetDocProperty(Optional ByVal DocTitle As String = "", Optional ByVal DocSubject As String = "", Optional ByVal DocAuthor As String = "", Optional ByVal DocCreator As String = "", Optional ByVal DocProducer As String = "", Optional ByVal DocKeywords As String = "")
        '--> on affecte des propri�t�s au document
        If (StringType.StrCmp(DocTitle, "", False) <> 0) Then
            Me.mvarDocTitle = DocTitle
            Me.arrBOOKMARK(1).Text = DocTitle
        End If
        If (StringType.StrCmp(DocSubject, "", False) <> 0) Then
            Me.mvarDocSubject = DocSubject
        End If
        If (StringType.StrCmp(DocKeywords, "", False) <> 0) Then
            Me.mvarDocKeywords = DocKeywords
        End If
        If (StringType.StrCmp(DocAuthor, "", False) <> 0) Then
            Me.mvarDocAuthor = DocAuthor
        End If
        If (StringType.StrCmp(DocCreator, "", False) <> 0) Then
            Me.mvarDocCreator = DocCreator
        End If
        If (StringType.StrCmp(DocProducer, "", False) <> 0) Then
            Me.mvarDocProducer = DocProducer
        End If
    End Sub

    Public Function SetFont(ByVal FontFamily As String, Optional ByVal Size As Single = 0.0!, Optional ByVal Style As pdfFontStyle = 0)
        Dim num2 As Integer
        Me.mvarFontFamily = FontFamily
        Me.mvarFontStyle = Style
        If (Size <> 0.0!) Then
            Me.mvarFontSize = Size
        End If
        Dim text1 As String = Strings.UCase(Strings.Replace(FontFamily, " ", "", 1, -1, 0))

        If (StringType.StrCmp(text1, "TIMESNEWROMAN", False) = 0) Then
            'text1 = "TIMES"
        End If
        If (StringType.StrCmp(text1, "COURIERNEW", False) = 0) Then
            text1 = "COURIER"
        End If
        If (StringType.StrCmp(text1, "HELVETICA", False) = 0) Then
            text1 = "ARIAL"
        End If
        Dim text2 As String = ""
        Dim text3 As String = text1
        If (((StringType.StrCmp(text1, "TIMESNEWROMAN", False) = 0) Or (StringType.StrCmp(text1, "COURIER", False) = 0)) Or (StringType.StrCmp(text1, "ARIAL", False) = 0)) Then
            If ((Style And pdfFontStyle.pdfBold) = pdfFontStyle.pdfBold) Then
                text2 = "Bold"
            End If
            If ((Style And pdfFontStyle.pdfItalic) = pdfFontStyle.pdfItalic) Then
                text2 = (text2 & "Italic")
            End If
            text3 = StringType.FromObject(ObjectType.AddObj(text3, Interaction.IIf((StringType.StrCmp(text2, "", False) <> 0), ("," & text2), "")))
        End If
        Dim num3 As Integer = Me.mvarFontCount
        Dim num1 As Integer = 1
        Do While (num1 <= num3)
            If InStr(Me.arrFONT(num1).FontAlias, text3, CompareMethod.Text) <> 0 And Me.arrFONT(num1).FontStyle = Style Then
                num2 = num1
                Exit Do
            End If
            num1 += 1
            Application.DoEvents()
        Loop
        If (num2 = 0) Then
            Dim text4 As String = text1
            If (StringType.StrCmp(text4, "TIMES", False) = 0) Then
                Me.prCreateFontTimes(Style)
            ElseIf (StringType.StrCmp(text4, "COURIER", False) = 0) Then
                Me.prCreateFontCourier(Style)
            ElseIf (StringType.StrCmp(text4, "SYMBOL", False) = 0) Then
                Me.prCreateFontSymbol(Style)
            ElseIf (StringType.StrCmp(text4, "ZAPFDINGBATS", False) = 0) Then
                Me.prCreateFontZapfDingbats()
            Else
                '-> on ajoute le style � une font existante
                Me.prCreateFontTrueType(Style, text3)
                'Me.prCreateFontArial(Style)
            End If
            num2 = Me.mvarFontCount
        End If
        Me.mvarFontObj = ("_FNT_" & Strings.Trim(Conversion.Str(num2)))
        SetFont = Me.mvarFontObj
        Me.mvarFontIndex = num2
    End Function

    Public Sub SetFontSize(ByVal Size As Single)
        Me.mvarFontSize = Size
    End Sub

    Public Sub SetLayout(ByVal Layout As pdfPageOrientation)
        Me.mvarOrientation = Layout
    End Sub

    Public Sub SetLineStyle(ByVal Style As pdfLineStyle, Optional ByVal MiterLimit As Single = 0.0!)
        Dim style1 As pdfLineStyle = (Style And (pdfLineStyle.pdfSquareCap Or (pdfLineStyle.pdfRoundCap Or pdfLineStyle.pdfButtCap)))
        Dim style2 As pdfLineStyle = (Style And (pdfLineStyle.pdfBevelJoin Or (pdfLineStyle.pdfRoundJoin Or pdfLineStyle.pdfMiterJoin)))
        If (style1 >= DirectCast(0, pdfLineStyle)) Then
            Me.prWriteObj(Me.prFormat("#i J", New Object() {Conversion.Int((CDbl(style1) / 32))}))
        End If
        If (style2 >= DirectCast(0, pdfLineStyle)) Then
            Me.prWriteObj(Me.prFormat("#i j", New Object() {Conversion.Int((CDbl(style2) / 2))}))
        End If
        If (MiterLimit >= 1.0!) Then
            Me.prWriteObj(Me.prFormat("#i M", New Object() {MiterLimit}))
        End If
    End Sub

    Public Sub SetLineWidth(ByVal Width As Single)
        If (Width >= 0.0!) Then
            Me.mvarLineWidth = (Me.mvarKUnit * Width)
            Me.prWriteObj(Me.prFormat("#s w", New Object() {Me.mvarLineWidth}))
        End If
    End Sub

    Public Sub SetMargins(Optional ByVal Left As Single = 0.0!, Optional ByVal Right As Single = 0.0!, Optional ByVal Top As Single = 0.0!, Optional ByVal Bottom As Single = 0.0!, Optional ByVal ClipPage As Boolean = True)
        Me.mvarMarginL = (Me.mvarKUnit * Left)
        Me.mvarMarginR = (Me.mvarKUnit * Right)
        Me.mvarMarginT = (Me.mvarKUnit * Top)
        Me.mvarMarginB = (Me.mvarKUnit * Bottom)
        Me.mvarClipPage = ClipPage
    End Sub

    Public Sub SetPaperSize(ByVal PaperSize As pdfPaperSize, Optional ByVal UserWidth As Single = 0.0!, Optional ByVal UserHeight As Single = 0.0!)
        Select Case PaperSize
            Case pdfPaperSize.pdf8_5x12
                Me.mvarPageWidth = 612.0!
                Me.mvarPageHeight = 864.0!
                Return
            Case pdfPaperSize.pdf8_5x11
                Me.mvarPageWidth = 612.0!
                Me.mvarPageHeight = 792.0!
                Return
            Case pdfPaperSize.pdfA0
                Me.mvarPageWidth = 2384.0!
                Me.mvarPageHeight = 3371.0!
                Return
            Case pdfPaperSize.pdfA1
                Me.mvarPageWidth = 1685.0!
                Me.mvarPageHeight = 2384.0!
                Return
            Case pdfPaperSize.pdfA1R
                Me.mvarPageWidth = 2384.0!
                Me.mvarPageHeight = 1685.0!
                Return
            Case pdfPaperSize.pdfA2
                Me.mvarPageWidth = 1192.0!
                Me.mvarPageHeight = 1685.0!
                Return
            Case pdfPaperSize.pdfA2R
                Me.mvarPageWidth = 1685.0!
                Me.mvarPageHeight = 1192.0!
                Return
            Case pdfPaperSize.pdfA3
                Me.mvarPageWidth = 842.0!
                Me.mvarPageHeight = 1192.0!
                Return
            Case pdfPaperSize.pdfA3R
                Me.mvarPageWidth = 1192.0!
                Me.mvarPageHeight = 842.0!
                Return
            Case pdfPaperSize.pdfA4
                Me.mvarPageWidth = 596.0!
                Me.mvarPageHeight = 842.0!
                Return
            Case pdfPaperSize.pdfA4R
                Me.mvarPageWidth = 842.0!
                Me.mvarPageHeight = 596.0!
                Return
            Case pdfPaperSize.pdfA5
                Me.mvarPageWidth = 421.0!
                Me.mvarPageHeight = 596.0!
                Return
            Case pdfPaperSize.pdfA5R
                Me.mvarPageWidth = 596.0!
                Me.mvarPageHeight = 421.0!
                Return
            Case pdfPaperSize.pdfA6
                Me.mvarPageWidth = 298.0!
                Me.mvarPageHeight = 421.0!
                Return
            Case pdfPaperSize.pdfA6R
                Me.mvarPageWidth = 421.0!
                Me.mvarPageHeight = 298.0!
                Return
            Case pdfPaperSize.pdfA7
                Me.mvarPageWidth = 210.0!
                Me.mvarPageHeight = 298.0!
                Return
            Case pdfPaperSize.pdfA7R
                Me.mvarPageWidth = 298.0!
                Me.mvarPageHeight = 210.0!
                Return
            Case pdfPaperSize.pdfUserSize
                Me.mvarPageWidth = (Me.mvarKUnit * UserWidth)
                Me.mvarPageHeight = (Me.mvarKUnit * UserHeight)
                Exit Select
        End Select
    End Sub

    Public Sub SetProtection(ByVal Mode As pdfProtectionMode, Optional ByVal OwnerPwd As String = "", Optional ByVal UserPwd As String = "")
        Dim buffer1 As Byte() = New Byte(32 - 1) {}
        Dim text1 As String = String.Concat(New String() {"(", StringType.FromChar(Strings.Chr(191)), "N", "^", "N", "u", StringType.FromChar(Strings.Chr(138)), "A", "d", ChrW(0), "N", "V", StringType.FromChar(Strings.Chr(255)), StringType.FromChar(Strings.Chr(250)), ChrW(1), ChrW(8), ".", ".", ChrW(0), StringType.FromChar(Strings.Chr(182)), StringType.FromChar(Strings.Chr(208)), "h", ">", StringType.FromChar(Strings.Chr(128)), "/", ChrW(12), StringType.FromChar(Strings.Chr(169)), StringType.FromChar(Strings.Chr(254)), "d", "S", "i", "z"})
        Me.mvarProtection = CInt(((DirectCast(192, pdfProtectionMode) + Mode) And DirectCast(255, pdfProtectionMode)))
        If (StringType.StrCmp(OwnerPwd, "", False) = 0) Then
            OwnerPwd = UserPwd
        End If
        Me.mvarEncryption = True
        UserPwd = Strings.Left((UserPwd & text1), 32)
        OwnerPwd = Strings.Left((OwnerPwd & text1), 32)
        Me.mvarDocHelp = "%PWD O:" & OwnerPwd & "U:" & UserPwd
        Dim num1 As Byte = 0
        Do
            buffer1(num1) = CByte(Strings.Asc(Strings.Mid(UserPwd, (num1 + 1), 1)))
            num1 = CByte((num1 + 1))
        Loop While (num1 <= 31)
        Me.mvarOValue = Me.prMtx2Str(DirectCast(Me.prRC4(Strings.Left(Me.prMD5(OwnerPwd), 5), buffer1), Byte()))
        num1 = 0
        Do
            buffer1(num1) = CByte(Strings.Asc(Strings.Mid(text1, (num1 + 1), 1)))
            num1 = CByte((num1 + 1))
        Loop While (num1 <= 31)
        Me.mvarEncryptionKey = Strings.Left(Me.prMD5(String.Concat(New String() {UserPwd, Me.mvarOValue, StringType.FromChar(Strings.Chr(Me.mvarProtection)), StringType.FromChar(Strings.Chr(255)), StringType.FromChar(Strings.Chr(255)), StringType.FromChar(Strings.Chr(255)), Me.mvarFileID})), 5)
        Me.mvarUValue = Me.prMtx2Str(DirectCast(Me.prRC4(Me.mvarEncryptionKey, buffer1), Byte()))
        Me.mvarPValue = CInt(Math.Round(Conversion.Val(("&hFFFFFF" & Conversion.Hex(Me.mvarProtection)))))
    End Sub


    Public Sub SetScale(ByVal ScaleMode As pdfScaleMode, Optional ByVal UserFactor As Single = 1.0!)
        Select Case ScaleMode
            Case pdfScaleMode.pdf72PxInch
                Me.mvarKUnit = 1.0!
                Return
            Case pdfScaleMode.pdfInch
                Me.mvarKUnit = 72.0!
                Return
            Case pdfScaleMode.pdfCentimeter
                Me.mvarKUnit = 28.34646!
                Return
            Case pdfScaleMode.pdfMillimeter
                Me.mvarKUnit = 2.834646!
                Return
            Case pdfScaleMode.pdfUserScale
                Me.mvarKUnit = UserFactor
                Exit Select
        End Select
    End Sub

    Public Sub SetTextHorizontalScaling(ByVal W As Single)
        Me.mvarTextScaling = W
        Me.prWriteObj(Me.prFormat("#s Tz", New Object() {Me.mvarTextScaling}))
    End Sub

    Public Sub SetTextLineSpacing(ByVal value As Single)
        If (value >= 0.0!) Then
            Me.mvarLineSpacing = value
        End If
    End Sub

    Public Sub SetTextMode(ByVal Mode As pdfTextMode)
        Me.prWriteObj(Me.prFormat("#i Tr", New Object() {Mode}))
    End Sub

    Public Sub SetViewerPreferences(ByVal Mode As pdfViewerPreference)
        Me.mvarViewerPreference = Mode
    End Sub

    Public Sub SetWordSpacing(ByVal w As Single)
        Me.mvarWordSpacing = w
        Me.prWriteObj(Me.prFormat("#s Tw", New Object() {Me.mvarWordSpacing}))
    End Sub

    Public Sub SetXY(ByVal X As Single, ByVal Y As Single)
        Me.mvarCurrX = (Me.mvarKUnit * X)
        Me.mvarCurrY = (Me.mvarKUnit * Y)
    End Sub

    Public Function TextHeight() As Single
        Dim single1 As Single
        If (Me.mvarFontIndex > 0) Then
            single1 = (((Me.mvarFontSize * Me.arrFONT(Me.mvarFontIndex).Height) / 1000.0!) / Me.mvarKUnit)
        End If
        Return single1
    End Function

    Public Function TextLength(ByVal Phrase As String) As Single
        Dim single2 As Single = 0.0!
        Dim num3 As Integer = Strings.Len(Phrase)
        Dim single1 As Single = 0.0!
        If (Me.mvarFontIndex <= 0) Then
            Dim single3 As Single
            Return single3
        End If

        Dim num5 As Integer = Me.mvarFontIndex
        Dim num4 As Integer = num3
        Dim num2 As Integer = 1
        Do While (num2 <= num4)
            Dim num1 As Byte = CByte(Strings.Asc(Strings.Mid(Phrase, num2, 1)))
            single2 = (single2 + Me.arrFONT(num5).Widths(num1))
            If (num1 = 32) Then
                single1 = (single1 + 1.0!)
                'en essai 
                single2 = single2 - 0
            End If
            '-> test pb code barre
            If single2 = 0 Then
                single2 = 850
            End If
            num2 += 1
            Application.DoEvents()
        Loop
        Return ((((((single2 * Me.mvarFontSize) / 1000.0!) + (single1 * Me.mvarWordSpacing)) + (num3 * Me.mvarCharSpacing)) * (Me.mvarTextScaling / 100.0!)) / Me.mvarKUnit)
    End Function

    Public Function ToColor(ByVal ParamArray Args As Object()) As String
        Dim text1 As String = "#000000"
        Select Case (Information.UBound(Args, 1) + 1)
            Case 1
                Dim text2 As String = Strings.Right(("00" & Conversion.Hex(RuntimeHelpers.GetObjectValue(Args(0)))), 2)
                Return ("#" & text2 & text2 & text2)
            Case 2
                Return text1
            Case 3
                Return ("#" & Strings.Right(("00" & Conversion.Hex(RuntimeHelpers.GetObjectValue(Args(0)))), 2) & Strings.Right(("00" & Conversion.Hex(RuntimeHelpers.GetObjectValue(Args(1)))), 2) & Strings.Right(("00" & Conversion.Hex(RuntimeHelpers.GetObjectValue(Args(2)))), 2))
            Case 4
                Dim num2 As Integer = IntegerType.FromObject(Args(0))
                If (num2 > 100) Then
                    num2 = 100
                End If
                Dim num3 As Integer = IntegerType.FromObject(Args(1))
                If (num3 > 100) Then
                    num3 = 100
                End If
                Dim num4 As Integer = IntegerType.FromObject(Args(2))
                If (num4 > 100) Then
                    num4 = 100
                End If
                Dim num5 As Integer = IntegerType.FromObject(Args(3))
                If (num5 > 100) Then
                    num5 = 100
                End If
                num2 = CInt(Math.Round(CDbl(((255 * (1 - (CDbl(num5) / 100))) * (1 - (CDbl(num2) / 100))))))
                num3 = CInt(Math.Round(CDbl(((255 * (1 - (CDbl(num5) / 100))) * (1 - (CDbl(num3) / 100))))))
                num4 = CInt(Math.Round(CDbl(((255 * (1 - (CDbl(num5) / 100))) * (1 - (CDbl(num4) / 100))))))
                Return ("#" & Strings.Right(("00" & Conversion.Hex(num2)), 2) & Strings.Right(("00" & Conversion.Hex(num3)), 2) & Strings.Right(("00" & Conversion.Hex(num4)), 2))
        End Select
        Return text1
    End Function

    Public Function Version() As String
        Return "2.0 NET"
    End Function

    Public Sub WriteStream(ByVal strTemp As String)
        Me.prWriteObj(strTemp)
    End Sub

    Public Sub WriteText(ByVal [Text] As String)
        Me.prDrawTextLine((Me.mvarCurrX / Me.mvarKUnit), (Me.mvarCurrY / Me.mvarKUnit), 0.0!, [Text], 0.0!, pdfTextAlignment.pdfAlignLeft, False)
    End Sub

    Public Sub WriteTextLn(ByVal [Text] As String)
        Me.prDrawTextLine((Me.mvarCurrX / Me.mvarKUnit), (Me.mvarCurrY / Me.mvarKUnit), 0.0!, [Text], 0.0!, pdfTextAlignment.pdfAlignLeft, True)
    End Sub


    ' Fields
    Private arrALPHASTATE As String()
    Private arrBOOKMARK As BookmarkDescriptor()
    Private arrFONT As FontDescriptor()
    Private arrGRAPHSTATE As GraphicState()
    Private arrIMAGE As ImageDescriptor()
    Private arrNOTE As NoteDescriptor()
    Private arrFILEJOIN As String()
    Private arrOBJECT As ObjDescriptor()
    Private arrSHADING As ShadingDescriptor()
    Private arrXREF As String()
    Private Const AUTHOR As String = "DMZ"
    Private blnColFlag As Boolean
    Private blnRowFlag As Boolean
    Private Const CLASSVERSION As String = "5.3.3"
    Private Const CREATOR As String = "Visual Basic Deal Framework"
    Private intGraphState As Integer
    Private mvarAlphaStateCount As Integer
    Private mvarBookmarkCount As Integer
    Private mvarFileJoin As Integer
    Private mvarCharSpacing As Single
    Private mvarClipPage As Boolean
    Private mvarColorFill As String
    Private mvarColorFillStencil As String
    Private mvarColorLine As String
    Private mvarColorLineStencil As String
    Private mvarColorText As String
    Private mvarColorTextStencil As String
    Private mvarCompression As pdfCompression
    Private mvarCompressionQuality As Byte
    Private mvarCurrentPageObj As Integer
    Private mvarCurrX As Single
    Private mvarCurrY As Single
    Private mvarDocAuthor As String
    Private mvarDocCreationDate As String
    Private mvarDocCreator As String
    Private mvarDocKeywords As String
    Private mvarDocProducer As String
    Private mvarDocSubject As String
    Private mvarDocTitle As String
    Private mvarDocHelp As String
    Private mvarEncoding As pdfEncoding
    Private mvarEncryption As Boolean
    Private mvarEncryptionKey As String
    Private mvarFileID As String
    Private mvarFileName As String
    Private mvarFileNumber As Integer
    Private mvarFontCount As Integer
    Private mvarFontFamily As String
    Private mvarFontIndex As Integer
    Private mvarFontObj As String
    Private mvarFontSize As Single
    Private mvarFontStyle As pdfFontStyle
    Private mvarIdObj As Integer
    Private mvarImageCount As Integer
    Private mvarInObject As Boolean
    Private mvarInPage As Boolean
    Private mvarInPattern As Boolean
    Private mvarInStencil As Boolean
    Private mvarKUnit As Single
    Private mvarLineSpacing As Single
    Private mvarLineWidth As Single
    Private mvarMarginB As Single
    Private mvarMarginL As Single
    Private mvarMarginR As Single
    Private mvarMarginT As Single
    Private mvarMaxObject As Long
    Private mvarNoteCount As Integer
    Private mvarObjectCount As Integer
    Private mvarOrientation As pdfPageOrientation
    Private mvarOValue As String
    Private mvarPageHeight As Single
    Private mvarPageLabel As String
    Private mvarPages As String
    Private mvarPageWidth As Single
    Private mvarProtection As Integer
    Private mvarPValue As Integer
    Private mvarRC4Code As Byte()
    Private mvarRC4LastKey As String
    Private mvarShadingCount As Integer
    Private mvarSpecOptions As pdfSpecOptions
    Private mvarStreamBuffer As Byte()
    Private mvarBufferPage As BufferString
    Private mvarTextScaling As Single
    Private mvarTotPages As Integer
    Private mvarUValue As String
    Private mvarViewerPreference As pdfViewerPreference
    Private mvarPermissionAnnotation As Boolean
    Private mvarWordSpacing As Single
    Private Const OBJ_CATALOG As Integer = 2
    Private Const OBJ_ENCODING As Integer = 3
    Private Const OBJ_INFO As Integer = 1
    Private Const OBJ_PAGE As Integer = 4
    Private Const OBJ_RES4PATTERN As Integer = 5
    Private Const OBJ_RESOURCE As Integer = 6
    Private Const PRODUCER As String = "TurboPDF - " & ChrW(169) & " - Deal Informatique"
    Private sngCellX As Single
    Private sngCellY As Single
    Private sngColWidth As Single
    Private sngRowHeight As Single

    ' Nested Types
    <StructLayout(LayoutKind.Sequential)> _
    Private Structure BookmarkDescriptor
        ' Fields
        Public Destination As String
        Public First As Integer     'position
        Public Follow As Integer
        Public Last As Integer
        Public Level As Integer
        Public Obj As Integer
        Public Parent As Integer
        Public Prev As Integer
        Public [Text] As String     'texte � afficher
        Public Format As Integer    'mise en forme eventuelle 1 ou 2 italique ou gras
        Public color As String      'couleur du outline
        Public webLink As String    'pour savoir si on pointe sur une url
    End Structure

    Private Structure NoteDescriptor
        ' Fields
        Public Base As String
        Public [author] As String
        Public [Text] As String
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Private Structure FontDescriptor
        ' Methods
        Public Sub Initialize()
            Me.Widths = New Integer(256 - 1) {}
            '-> par defaut on est a normal
            Me.FontStyle = pdfFontStyle.pdfNormal
        End Sub

        ' Fields
        Public BaseFont As String
        Public BaseLine As Long
        Public Embedded As Boolean 'si on integre la police ou pas
        Public FontAlias As String
        Public FontFile As String 'ou est situ� le fichier de la font
        Public FontStyle As pdfFontStyle
        Public Height As Long
        Public Length1 As Long
        Public Param As String
        Public TrueTypeFont As Boolean
        Public Widths As Integer() 'glyphes
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Private Structure GraphicState
        ' Fields
        Public ColorFill As String
        Public ColorFillStencil As String
        Public ColorLine As String
        Public ColorLineStencil As String
        Public ColorText As String
        Public ColorTextStencil As String
        Public FontFamily As String
        Public FontSize As Single
        Public FontStyle As pdfFontStyle
        Public LineDashOff As Single
        Public LineDashOn As Single
        Public LineMiterLimit As Single
        Public LineStyle As pdfLineStyle
        Public LineWidth As Single
        Public PosX As Single
        Public PosY As Single
        Public ScaleUnit As Single
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Private Structure ImageDescriptor
        ' Fields
        Public BPP As Byte
        Public Device As String
        Public Filename As String
        Public Filter As String
        Public Frame As Integer
        Public Height As Long
        Public Width As Long
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Private Structure ObjDescriptor
        ' Fields
        Public Name As String
        Public Options As pdfObjectType
        Public Stream As String
    End Structure

    Public Enum pdfBarCode
        ' Fields
        pdfCode128 = 4
        pdfCode39 = 1
        pdfEAN13 = 0
        pdfInterleave25 = 2
        pdfPostNet = 3
    End Enum

    Public Enum pdfBookmarkType
        ' Fields
        pdfFitHorizontal = 2
        pdfFitPage = 0
        pdfFitRect = 3
        pdfFitVertical = 1
        pdfXYZoom = 4
    End Enum

    Public Enum pdfBoxBorder
        ' Fields
        pdfBorderAll = 15
        pdfBorderBottom = 8
        pdfBorderLeft = 2
        pdfBorderNone = 0
        pdfBorderRight = 4
        pdfBorderTop = 1
    End Enum

    Public Enum pdfCompression
        ' Fields
        pdfASCII85Decode = 4
        pdfASCIIHexDecode = 8
        pdfLZWDecode = 2
        pdfNothing = 0
        pdfRunLengthDecode = 1
        pdfDeflate = 16
    End Enum

    Public Enum pdfDrawOptions
        ' Fields
        pdfClipping = 128
        pdfClosed = 2
        pdfFilled = 4
        pdfInvisible = 1
        pdfVisible = 0
    End Enum

    Public Enum pdfEncoding
        ' Fields
        pdfMacRomanEncoding = 1
        pdfWinAnsiEncoding = 0
    End Enum

    Public Enum pdfFontStyle
        ' Fields
        pdfBold = 1
        pdfItalic = 2
        pdfNormal = 0
        pdfOverLine = 8
        pdfStrikeOut = 16
        pdfUnderline = 4
    End Enum

    Public Enum pdfLineStyle
        ' Fields
        pdfBevelJoin = 4
        pdfButtCap = 16
        pdfMiterJoin = 1
        pdfRoundCap = 32
        pdfRoundJoin = 2
        pdfSquareCap = 64
    End Enum

    Public Enum pdfLinkDown
        ' Fields
        pdfBorder = 2
        pdfInvert = 1
        pdfNone = 0
        pdfPush = 3
    End Enum

    Public Enum pdfNoteBorder
        ' Fields
        pdfBeleved = 2
        pdfDashed = 1
        pdfInset = 3
        pdfSolid = 0
        pdfUnderline = 4
    End Enum

    Public Enum pdfNoteFlag
        ' Fields
        pdfHidden = 2
        pdfNoRotate = 16
        pdfNoView = 32
        pdfNoZoom = 8
        pdfPrint = 4
        pdfReadOnly = 64
    End Enum

    Public Enum pdfNoteIcon
        ' Fields
        pdfComment = 1
        pdfHelp = 3
        pdfInsert = 6
        pdfKey = 2
        pdfNewParagraph = 5
        pdfNote = 0
        pdfParagraph = 4
    End Enum

    Public Enum pdfObjectType
        ' Fields
        pdfAllPages = 7
        pdfEvenPages = 2
        pdfFirstPage = 1
        pdfNotFirstPage = 8
        pdfNull = 0
        pdfOddPages = 4
        pdfOnFront = 16
    End Enum

    Public Enum pdfPageLabel
        ' Fields
        pdfDecimalArabic = 1
        pdfLowercaseLetters = 5
        pdfLowercaseRoman = 3
        pdfNoLabel = 0
        pdfUppercaseLetters = 4
        pdfUppercaseRoman = 2
    End Enum

    Public Enum pdfPageOrientation
        ' Fields
        pdfLandscape = 1
        pdfPortrait = 0
    End Enum

    Public Enum pdfPaperSize
        ' Fields
        pdf8_5x11 = 15
        pdf8_5x12 = 16
        pdfA0 = 8
        pdfA1 = 6
        pdfA1R = 7
        pdfA2 = 4
        pdfA2R = 5
        pdfA3 = 2
        pdfA3R = 3
        pdfA4 = 0
        pdfA4R = 1
        pdfA5 = 9
        pdfA5R = 10
        pdfA6 = 11
        pdfA6R = 12
        pdfA7 = 13
        pdfA7R = 14
        pdfUserSize = 255
    End Enum

    Public Enum pdfPatternType
        ' Fields
        pdfPattern = 0
        pdfStencil = 1
    End Enum

    Public Enum pdfProtectionMode
        ' Fields
        pdfAllowCopy = 16
        pdfAllowForms = 32
        pdfAllowModify = 8
        pdfAllowPrint = 4
        pdfAllowViewOnly = 0
    End Enum

    Public Enum pdfScaleMode
        ' Fields
        pdf72PxInch = 0
        pdfCentimeter = 3
        pdfInch = 1
        pdfMillimeter = 2
        pdfUserScale = 255
    End Enum

    Public Enum pdfShadingStyle
        ' Fields
        pdfLinear = 0
        pdfRadial = 1
    End Enum

    Private Enum pdfSpecOptions
        ' Fields
        pdfAcrobat40 = 0
        pdfAcrobat50 = 1
        pdfAcrobat60 = 2
    End Enum

    Public Enum pdfTextAlignment
        ' Fields
        pdfAlignBottom = 16
        pdfAlignLeft = 1
        pdfAlignRight = 2
        pdfAlignTop = 8
        pdfCenter = 4
        pdfFit = 128
        pdfForceFit = 256
        pdfJustify = 64
        pdfMiddle = 32
    End Enum

    Public Enum pdfTextMode
        ' Fields
        pdfFilled = 2
        pdfOutline = 1
        pdfPlain = 0
        pdfTextClipping = 4
        pdfTextInvisible = 3
    End Enum

    Public Enum pdfViewerPreference
        ' Fields
        pdfCenterWindow = 256
        pdfFitWindow = 512
        pdfFullScreen = 1
        pdfHideMenubar = 16
        pdfHideToolbar = 32
        pdfHideWindowUI = 64
        pdfOneColumn = 8192
        pdfShowBookmarks = 2
        pdfShowThumbs = 4
        pdfShowTitle = 128
        pdfSinglePage = 4096
        pdfTwoColumnLeft = 16384
        pdfTwoColumnRight = 32768
    End Enum

    Private Structure BufferString
        Public Buffer As String
        Public BufferLen As Long
        Public Pointer As Long
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Private Structure ShadingDescriptor
        ' Fields
        Public Name As String
        Public Param As String
        Public Stream As String
        Public SubType As String
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Private Structure WMFObj
        ' Fields
        Public b As Byte
        Public Empty As Boolean
        Public G As Byte
        Public R As Byte
        Public Style As Integer
        Public SubType As String
        Public Width As Single
    End Structure
End Class


