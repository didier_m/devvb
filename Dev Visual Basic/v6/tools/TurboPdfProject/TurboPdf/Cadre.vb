Option Strict Off
Option Explicit On
Public Class Cadre
    '**********************************************
    '*                                            *
    '* Cette classe contient la d�finition d'un   *
    '* Objet cadre                                *
    '*                                            *
    '**********************************************

    '---> D�finition des propri�t�s
    Public Nom As String
    Public Hauteur As Single
    Public Largeur As Single
    Public Top As Single
    'UPGRADE_NOTE: Lefta �t� mis � niveau vers Left_Renamed. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Public Left_Renamed As Single
    Public BackColor As Integer
    Public IdAffichage As Short
    Public MargeInterne As Single
    Public LargeurTrait As Short
    Public IsRoundRect As Boolean
    Public SectionName As String

    '---> Variable de stockage priv�
    Public Bas As Boolean
    Public Haut As Boolean
    Public Gauche As Boolean
    Public Droite As Boolean

    Public IdRTf As Short

    '-> Pour chargement du contenu RTF
    Public TempoRTF As Short 'stocker le handle du fichier ouvert
    Public TempoRtfString As String ' stocker le nom du fichier ouvert



    'UPGRADE_NOTE: Class_Initializea �t� mis � niveau vers Class_Initialize_Renamed. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()

        LargeurTrait = 1

    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub
End Class