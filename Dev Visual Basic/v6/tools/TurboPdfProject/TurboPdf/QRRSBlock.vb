Option Strict Off
Option Explicit On
Imports System
Imports System.Collections.Specialized
Imports Microsoft.VisualBasic
Imports Microsoft.VisualBasic.CompilerServices

Friend Class QRRSBlock
    '---------------------------------------------------------------
    ' QRRSBlock
    '---------------------------------------------------------------


    Dim QR_RS_BLOCK_TABLE As Integer()()
    'Dim aQRCode As QRCode
    Public totalCount As Object
    Public dataCount As Object

    Public Sub New()
        MyBase.New()

        QR_RS_BLOCK_TABLE = New Integer()() {New Integer() {1, 26, 19}, New Integer() {1, 26, 16}, New Integer() {1, 26, 13}, New Integer() {1, 26, 9}, New Integer() {1, 44, 34}, New Integer() {1, 44, 28}, New Integer() {1, 44, 22}, New Integer() {1, 44, 16}, New Integer() {1, 70, 55}, New Integer() {1, 70, 44}, New Integer() {2, 35, 17}, New Integer() {2, 35, 13}, New Integer() {1, 100, 80}, New Integer() {2, 50, 32}, New Integer() {2, 50, 24}, New Integer() {4, 25, 9}, New Integer() {1, 134, 108}, New Integer() {2, 67, 43}, New Integer() {2, 33, 15, 2, 34, 16}, New Integer() {2, 33, 11, 2, 34, 12}, New Integer() {2, 86, 68}, New Integer() {4, 43, 27}, New Integer() {4, 43, 19}, New Integer() {4, 43, 15}, New Integer() {2, 98, 78}, New Integer() {4, 49, 31}, New Integer() {2, 32, 14, 4, 33, 15}, New Integer() {4, 39, 13, 1, 40, 14}, New Integer() {2, 121, 97}, New Integer() {2, 60, 38, 2, 61, 39}, New Integer() {4, 40, 18, 2, 41, 19}, New Integer() {4, 40, 14, 2, 41, 15}, New Integer() {2, 146, 116}, New Integer() {3, 58, 36, 2, 59, 37}, New Integer() {4, 36, 16, 4, 37, 17}, New Integer() {4, 36, 12, 4, 37, 13}, New Integer() {2, 86, 68, 2, 87, 69}, New Integer() {4, 69, 43, 1, 70, 44}, New Integer() {6, 43, 19, 2, 44, 20}, New Integer() {6, 43, 15, 2, 44, 16}}

        'Set aQRCode = New QRCode
    End Sub


    Public Function QRRSBlock(ByVal tC As Object, ByVal dC As Object) As Object
        totalCount = tC
        dataCount = dC
    End Function

    Public Function getDataCount() As Object
        Return dataCount
    End Function

    Public Function getTotalCount() As Object
        Return totalCount
    End Function

    Public Function getRSBlocks(ByVal typeNumber As Integer, ByVal errorCorrectLevel As Integer) As Collection
        Static rsBlock As Integer()
        Static length As Integer
        Static List As Collection
        Static count As Integer
        Static aQRRSBlock As QRRSBlock
        rsBlock = getRsBlockTable(typeNumber, errorCorrectLevel)
        length = (rsBlock.GetUpperBound(0) + 1) / 3
        List = New Collection

        For i As Integer = 0 To length - 1
            count = rsBlock(i * 3 + 0)
            totalCount = rsBlock(i * 3 + 1)
            dataCount = rsBlock(i * 3 + 2)
            For j As Integer = 0 To count - 1
                aQRRSBlock = New QRRSBlock()
                aQRRSBlock.QRRSBlock(totalCount, dataCount)
                List.Add(aQRRSBlock)
            Next
        Next

        Return List
    End Function

    Public Function getRsBlockTable(ByVal typeNumber As Integer, ByVal errorCorrectLevel As Integer) As Integer()

        Dim result As Integer() = Nothing
        Select Case errorCorrectLevel
            Case QR_ERROR_CORRECT_LEVEL_L
                result = QR_RS_BLOCK_TABLE(CInt((CDbl(typeNumber) - 1) * 4 + 0))
            Case QR_ERROR_CORRECT_LEVEL_M
                result = QR_RS_BLOCK_TABLE(CInt((CDbl(typeNumber) - 1) * 4 + 1))
            Case QR_ERROR_CORRECT_LEVEL_Q
                result = QR_RS_BLOCK_TABLE(CInt((CDbl(typeNumber) - 1) * 4 + 2))
            Case QR_ERROR_CORRECT_LEVEL_H
                result = QR_RS_BLOCK_TABLE(CInt((CDbl(typeNumber) - 1) * 4 + 3))
            Case Else
                trigger_error("tn:typeNumber/ecl:errorCorrectLevel", E_USER_ERROR)
        End Select
        Return result
    End Function
End Class