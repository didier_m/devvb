Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Specialized
Imports System.Diagnostics
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports UpgradeHelpers.Helpers
Imports UpgradeHelpers.VB
Imports UpgradeSolution1Support.UpgradeStubs
Friend Class QRCode

	Public QR_PAD0 As Integer
	Public QR_PAD1 As Integer

	Public typeNumber As Object
	Public modules As QRModule
	Public moduleCount As Integer
	Public errorCorrectLevel As Object
	Public QRUtil As QRUtil
	Public QRNumber As QRNumber
	Public QRAlphaNum As QRAlphaNum
	Public QR8BitByte As QR8BitByte
	Public QRKanji As QRKanji
	Public QRRSBlock As Object
	Public QRBitBuffer As QRBitBuffer
	Public QRPolynomial As QRPolynomial
	Public data As Object
	Public cpt As Integer

	Public Sub New()
		MyBase.New()
		QR_PAD0 = CInt("&HEC")
		QR_PAD1 = CInt("&H11")
		QRUtil = New QRUtil()
		QRNumber = New QRNumber()
		QRAlphaNum = New QRAlphaNum()
		QR8BitByte = New QR8BitByte()
		QRKanji = New QRKanji()
		QRBitBuffer = New QRBitBuffer()
        QRPolynomial = New QRPolynomial()

        typeNumber = 1
		errorCorrectLevel = QR_ERROR_CORRECT_LEVEL_H
		qrDataList = New OrderedDictionary()
	End Sub

	Public Function getTypeNumber() As Object
		Return typeNumber
	End Function

	Public Function setTypeNumber(ByVal iTypeNumber As Object) As Object
        'UPGRADE_WARNING: (1068) iTypeNumber of type Variant is being forc ed to Scalar. More Information: https://docs.mobilize.net/vbuc/ewis#1068  
        typeNumber = iTypeNumber
	End Function

	Public Function getErrorCorrectLevel() As Object
		Return errorCorrectLevel
	End Function

	Public Function setErrorCorrectLevel(ByVal errCorrectLevel As Object) As Object
        errorCorrectLevel = errCorrectLevel
    End Function

	Public Function addData(ByVal data As Object, Optional ByVal mode As Integer = 0) As Object
		If mode = 0 Then
            mode = CInt(QRUtil.getMode(CStr(data)))
        End If

		Select Case mode
			Case QR_MODE_NUMBER
				addDataImpl(QRNumber.QRNumber(data))
			Case QR_MODE_ALPHA_NUM
				addDataImpl(QRAlphaNum.QRAlphaNum(data))
			Case QR_MODE_8BIT_BYTE
				addDataImpl(QR8BitByte.QR8BitByte(data))
			Case QR_MODE_KANJI
				addDataImpl(QRKanji.QRKanji(data))
			Case Else
				trigger_error("mode:mode", E_USER_ERROR)
		End Select
	End Function

	Public Function clearData() As Object
		qrDataList = New OrderedDictionary()
	End Function

	Public Function addDataImpl(ByVal QRData As Object) As Object
		If qrDataList Is Nothing Then qrDataList = New OrderedDictionary()
		qrDataList.Add(Guid.NewGuid().ToString(), QRData)
	End Function

	Public Function getDataCount() As Object
		Return qrDataList.Count
	End Function

	Public Function getData(ByVal index As Object) As Object
		Return qrDataList(index)
	End Function

	Public Function isDark(ByVal row As Object, ByVal col As Object) As Boolean
		Dim result As Boolean = False
        If CStr(modules.getValue(CInt(row), CInt(col))) <> QRModule.typValue.vide Then
            If CStr(modules.getValue(CInt(row), CInt(col))) = "1" Then result = True
        Else
            result = False
		End If
		Return result
	End Function

	Public Function getModuleCount() As Object
		Return moduleCount
	End Function

	Public Function Hex2RGB(ByVal hexNum As Integer) As Integer()
		Dim rgb(2) As Integer
		Dim l As Integer = hexNum
		Dim r As Integer = l Mod &H100s
		l /= &H100s
		Dim g As Integer = l Mod &H100s
		l /= &H100s
		Dim b As Integer = l Mod &H100s
		rgb(0) = r
		rgb(1) = g
		rgb(2) = b
		Return rgb
	End Function

	Public Function make() As Object
        Main2()
        makeImpl(False, getBestMaskPattern())
    End Function

	Public Function getBestMaskPattern() As Object
		Dim lostPoint As Integer

		Dim minLostPoint As Integer = 0
		Dim Pattern As Integer = 0

		For i As Integer = 0 To 7
			makeImpl(True, i)
            lostPoint = CInt(QRUtil.getLostPoint(Me))
            If i = 0 Or minLostPoint > lostPoint Then
				minLostPoint = lostPoint
				Pattern = i
			End If
			Application.DoEvents()
		Next
		Return Pattern
	End Function

	Public Function createNullArray(ByVal length As Integer) As OrderedDictionary


		Dim result As OrderedDictionary = Nothing
		Dim emptyArray As Integer = -1
		result = New OrderedDictionary()

		For i As Integer = 0 To length - 1
			result.Add(Guid.NewGuid().ToString(), emptyArray)
		Next
		Return result
	End Function

	Public Function createCollection(ByVal length As Integer) As OrderedDictionary

		Dim result As OrderedDictionary = Nothing
		result = New OrderedDictionary()

		For i As Integer = 0 To length - 1
			result.Add(Guid.NewGuid().ToString(), New clsArray())
		Next
		Return result
	End Function

	Public Function createCollectionArray(ByVal length As Integer) As Object


	End Function

	Public Function makeImpl(ByVal test As Object, ByVal maskPattern As Object) As Object
        'Dim dataArray As Object

        moduleCount = CInt(CDbl(typeNumber) * 4 + 17)

        modules = New QRModule()

		For i As Integer = 0 To moduleCount - 1
			'-> Euh faudrait pas affecter la valeur?
			modules.createNullArray(i, moduleCount)
		Next

		setupPositionProbePattern(0, 0)
		setupPositionProbePattern(moduleCount - 7, 0)
		setupPositionProbePattern(0, moduleCount - 7)

		setupPositionAdjustPattern()
		setupTimingPattern()

		setupTypeInfo(test, maskPattern)

        If CDbl(typeNumber) >= 7 Then
            setupTypeNumber(test)
        End If

        Dim dataArray As OrderedDictionary = qrDataList
		Application.DoEvents()
		data = createData(typeNumber, errorCorrectLevel, dataArray)
		Application.DoEvents()
		debugModule()
		mapData(data, maskPattern)
	End Function


    Public Function mapData(ByVal data As Object, ByVal maskPattern As Object) As Object
		Dim dark, mask As Boolean

		Dim inc As Integer = -1
		Dim row As Integer = moduleCount - 1
		Dim bitIndex As Integer = 7
		Dim byteIndex As Integer = 0
		Dim val As String = ""
		For col As Integer = moduleCount - 1 To 1 Step -2
			val = ""
			If col = 6 Then col -= 1
			Do While (True)
				Application.DoEvents()
				For c As Integer = 0 To 1

					Application.DoEvents()
					'-> adaptation valeur pour la collectionn depart � 1
					If modules.getValue(row, col - c) = -1 Then
						dark = False

                        If byteIndex < data.count Then
                            dark = ((ShiftRight(data(byteIndex).cArray(0), bitIndex) And 1) = 1)
                        End If

                        mask = CBool(QRUtil.getMask(maskPattern, row, col - c))

                        If mask Then
							dark = Not dark
						End If
						'-> adaptation valeur pour la collectionn depart � 1
						If dark Then
							modules.setValue(QRModule.typValue.ok, row, col - c)
						Else
							modules.setValue(QRModule.typValue.nok, row, col - c)
						End If
						bitIndex -= 1

						If bitIndex = -1 Then
							byteIndex += 1
							bitIndex = 7
						End If
					End If
				Next

				row += inc

				If row < 0 Or moduleCount <= row Then
					row -= inc
					inc = -inc
					Exit Do
				End If
			Loop 
		Next
	End Function

	Public Function setupPositionAdjustPattern() As Object
		Dim col As Double
		Dim row As Integer
        Dim pos() As Integer = QRUtil.getPatternPosition(typeNumber)
        For	Each pos_item As Double In pos
			For j As Integer = 0 To pos.GetUpperBound(0)
				row = pos_item
				col = pos(j)

				If Not (modules.getValue(row, CInt(col)) <> QRModule.typValue.vide) Then

					For r As Integer = -2 To 2
						For c As Integer = -2 To 2
							If r = -2 Or r = 2 Or c = -2 Or c = 2 Or (r = 0 And c = 0) Then
								modules.setValue(QRModule.typValue.ok, row + r, CInt(col + c))
							Else
								modules.setValue(QRModule.typValue.nok, row + r, CInt(col + c))
							End If
						Next
					Next
				End If

			Next
		Next pos_item
	End Function

	Public Function setupPositionProbePattern(ByVal row As Object, ByVal col As Object) As Object
		For r As Integer = -1 To 7
			For c As Integer = -1 To 7
                If Not (CDbl(row) + r <= -1 Or moduleCount <= CDbl(row) + r Or CDbl(col) + c <= -1 Or moduleCount <= CDbl(col) + c) Then
                    If (0 <= r And r <= 6 And (c = 0 Or c = 6)) Or (0 <= c And c <= 6 And (r = 0 Or r = 6)) Or (2 <= r And r <= 4 And 2 <= c And c <= 4) Then
                        modules.setValue(QRModule.typValue.ok, CInt(CDbl(row) + r), CInt(CDbl(col) + c))
                    Else
                        modules.setValue(QRModule.typValue.nok, CInt(CDbl(row) + r), CInt(CDbl(col) + c))
                    End If
                End If

            Next
		Next
	End Function

	Public Function setupTimingPattern() As Object
		For r As Double = 8 To moduleCount - 7
			If Not (modules.getValue(CInt(r), 6) <> QRModule.typValue.vide) Then
				If CInt(r) Mod 2 = 0 Then
					modules.setValue(QRModule.typValue.ok, CInt(r), 6)
				Else
					modules.setValue(QRModule.typValue.nok, CInt(r), 6)
				End If
			End If

		Next

		For c As Integer = 8 To moduleCount - 7
			If Not (modules.getValue(6, c) <> QRModule.typValue.vide) Then
				If c Mod 2 = 0 Then
					modules.setValue(QRModule.typValue.ok, 6, c)
				Else
					modules.setValue(QRModule.typValue.nok, 6, c)
				End If
			End If

		Next
	End Function

	Public Function setupTypeNumber(ByVal test As Object) As Object
		Dim modB As Boolean

        Dim bits As Byte = CByte(QRUtil.getBCHTypeNumber(typeNumber))

        For i As Integer = 0 To 17
            modB = (Not CBool(test) And CStr(ShiftRight(bits, i)) & CStr(1)) = 1

            'modules(i Mod 3 + moduleCount - 8 - 3) = modB
        Next

		For i As Integer = 0 To 17
            modB = (Not CBool(test) And CStr(ShiftRight(bits, i)) & CStr(1)) = 1

            'modules(Math.Floor(CDbl(i / 3))) = modB
        Next
	End Function

	Public Function setupTypeInfo(ByVal test As Object, ByVal maskPattern As Object) As Object
		Dim modB As QRModule.typValue
        Dim data As Integer = ShiftLeft(CInt(errorCorrectLevel), 3) Or CInt(maskPattern)
        Dim bits As Integer = CInt(QRUtil.getBCHTypeInfo(data))

        For i As Integer = 0 To 14
            If Not CBool(test) And (ShiftRight(bits, i) And 1) = 1 Then
                modB = QRModule.typValue.ok
            Else
                modB = QRModule.typValue.nok
			End If
			If i < 6 Then
				modules.setValue(modB, i, 8)
			ElseIf i < 8 Then 
				modules.setValue(modB, i + 1, 8)
			Else
				modules.setValue(modB, moduleCount - 15 + i, 8)
			End If
		Next

		For i As Integer = 0 To 14
            If Not CBool(test) And (ShiftRight(bits, i) And 1) = 1 Then
                modB = QRModule.typValue.ok
            Else
                modB = QRModule.typValue.nok
			End If
			If i < 8 Then
				modules.setValue(modB, 8, moduleCount - i - 1)
			ElseIf i < 9 Then 
				modules.setValue(modB, 8, 15 - i - 1 + 1)
			Else
				modules.setValue(modB, 8, 15 - i - 1)
			End If
		Next
        If Not CBool(test) Then
            modules.setValue(QRModule.typValue.ok, moduleCount - 8, 8)
        Else
            modules.setValue(QRModule.typValue.nok, moduleCount - 8, 8)
		End If
	End Function

	Public Function createData(ByVal typeNumber As Object, ByVal errorCorrectLevel As Object, ByVal dataArray As Object) As Object
        Dim data As Object

        QRRSBlock = New QRRSBlock()

        Dim rsBlocks As Collection = QRRSBlock.getRSBlocks(typeNumber, errorCorrectLevel)

        Dim buffer As New QRBitBuffer()

        Dim tempForEndVar As Object = dataArray.count
        For i As Integer = 0 To tempForEndVar - 1
            data = dataArray(i)
            buffer.put_buffer(data.getMode(), 4)
            buffer.put_buffer(data.getLength(), data.getLengthInBits(typeNumber))
            data.write_buffer(buffer)
        Next

        Dim totalDataCount As Integer = 0

		For i As Integer = 1 To rsBlocks.Count
            totalDataCount += rsBlocks(i).getDataCount()
        Next
        If CDbl(buffer.getLengthInBits()) > totalDataCount * 8 Then
            trigger_error("code length overflow. (" & CStr(buffer.getLengthInBits()) & ">" & CStr(totalDataCount * 8) & ")", E_USER_ERROR)
        End If

        ' end code.
        If CDbl(buffer.getLengthInBits()) + 4 <= totalDataCount * 8 Then
            buffer.put_buffer(0, 4)
        End If

        ' padding
        Do While (CInt(buffer.getLengthInBits()) Mod 8 <> 0)
			buffer.putBit(False)
		Loop 

		' padding
		Do While (True)
			Application.DoEvents()
            If CDbl(buffer.getLengthInBits()) >= totalDataCount * 8 Then
                Exit Do
            End If
            buffer.put_buffer(QR_PAD0, 8)
            If CDbl(buffer.getLengthInBits()) >= totalDataCount * 8 Then
                Exit Do
            End If
            buffer.put_buffer(QR_PAD1, 8)
		Loop 
		Application.DoEvents()
		Return createBytes(buffer, rsBlocks)
	End Function

	Public Function createBytes(ByVal buffer As Object, ByVal rsBlocks As Object) As Object
		Dim result As Object = Nothing
		Dim dcCount As Double
		Dim ecCount As Integer
		Dim bdata As Object
        Dim rsPoly, rawPoly, modPoly As New QRPolynomial
        Dim modIndex As Double
        'Dim data As Object

        Dim offset As Double = 0
		Application.DoEvents()
		Dim maxDcCount As Double = 0
		Dim maxEcCount As Double = 0

        Dim dcData As OrderedDictionary = createCollection(rsBlocks.count)
        Dim ecdata As OrderedDictionary = createCollection(rsBlocks.count)

        '-> adaptation indice rsblocks et dcdata
        Dim curDcData(CInt(dcCount - 1)) As Integer
		Dim cA As clsArray
        'Dim curEcData(CInt(CDbl(rsPoly.getLength()) - 1)) As Integer
        Dim eA As clsArray
        Dim tempForEndVar As Object = rsBlocks.count
        For r As Integer = 1 To tempForEndVar

            dcCount = rsBlocks(r).getDataCount
            ecCount = CInt(rsBlocks(r).getTotalCount - dcCount)

            maxDcCount = Max(CInt(maxDcCount), CInt(dcCount))
			maxEcCount = Max(CInt(maxEcCount), ecCount)

            ReDim curDcData(dcCount - 1)

            dcData(r - 1).cArray = curDcData
            Dim tempForEndVar2 As Integer = dcData(r - 1).cArray.GetUpperBound(0)
            For i As Double = 0 To tempForEndVar2
                bdata = buffer.getBuffer()
                cA = dcData(r - 1)
                cA.setData(i, CInt("&Hff") And bdata.GetValue(CInt(i + offset)))
            Next
			offset += dcCount

			rsPoly = QRUtil.getErrorCorrectPolynomial(ecCount)
			rawPoly = New QRPolynomial()
            rawPoly.Polynomial(dcData(r - 1).cArray, CDbl(rsPoly.getLength()) - 1)

            modPoly = rawPoly.modB(rsPoly)
            Dim curEcData(CInt(CDbl(rsPoly.getLength()) - 1)) As Integer
            ecdata(r - 1).cArray = curEcData

            Dim tempForEndVar3 As Integer = ecdata(r - 1).cArray.GetUpperBound(0) - 1
            For i As Double = 0 To tempForEndVar3
                modIndex = i + CDbl(modPoly.getLength()) - (ecdata(r - 1).cArray.GetUpperBound(0))
                eA = ecdata(r - 1)
				If modIndex >= 0 Then
					eA.setData(i, modPoly.getB(modIndex))
				Else
					eA.setData(i, 0)
				End If
			Next
		Next

		Dim totalCodeCount As Integer = 0
        '-> modif car on part de 1
        Dim tempForEndVar4 As Object = rsBlocks.count
        For i As Double = 1 To tempForEndVar4
            totalCodeCount += rsBlocks(CInt(i)).getTotalCount()
        Next

		Dim data As OrderedDictionary = createCollection(totalCodeCount)

		Dim index As Integer = 0

		For i As Double = 0 To maxDcCount - 1
            Dim tempForEndVar6 As Object = rsBlocks.count
            For r As Integer = 1 To tempForEndVar6
                If i < dcData(r - 1).cArray.GetUpperBound(0) + 1 Then
                    index += 1
                    data(index - 1).cArray = New Integer() {dcData(r - 1).cArray(i)}
                End If
            Next
		Next

		For i As Double = 0 To maxEcCount - 1
            Dim tempForEndVar8 As Object = rsBlocks.count
            For r As Integer = 1 To tempForEndVar8
                If i < ecdata(r - 1).cArray.GetUpperBound(0) + 1 Then
                    index += 1
                    data(index - 1).cArray = New Integer() {ecdata(r - 1).cArray(i)}
                End If
            Next
		Next

		result = data
		Application.DoEvents()
		Return result
	End Function

	Public Function getMinimumQRCode(ByVal data As Object, ByVal errorCorrectLevel As Object) As Object
		Static mode As Integer
		Static qr As QRCode
		Static QRData As QRData
		Static length As Integer

        mode = CInt(QRUtil.getMode(CStr(data)))

        qr = New QRCode()
		qr.setErrorCorrectLevel(errorCorrectLevel)
		qr.addData(data, mode)

        QRData = qr.getData(0)
        length = CInt(QRData.getLength())

        For typeNumber = 1 To 10
            If length <= CDbl(QRUtil.getMaxLength(typeNumber, mode, errorCorrectLevel)) Then
                qr.setTypeNumber(typeNumber)
            End If
        Next

		qr.make()

		Return qr
	End Function

	' added fg (foreground), bg (background), and bgtrans (use transparent bg) parameters
	' also added some simple error checking on parameters
	' updated 2015.07.27 ~ DoktorJ
	Public Function createImage(ByRef size As Object, ByRef margin As Object, ByRef fg As Object, ByRef bg As Object, ByVal bgtrans As Object) As Object

		' size/margin EC
		If Not Information.IsNumeric(size) Then size = 2
		If Not Information.IsNumeric(margin) Then margin = 2
        If CDbl(size) < 1 Then size = 1
        If CDbl(margin) < 0 Then margin = 0

        Dim image_size As Integer = CInt(CDbl(getModuleCount()) * CDbl(size) + CDbl(margin) * 2)

        Dim image As Integer = CInt(imagecreatetruecolor(image_size, image_size))

        ' fg/bg EC
        If CDbl(fg) < 0 Or CDbl(fg) > CInt("&HFFFFFF") Then fg = CInt("&H0")
        If CDbl(bg) < 0 Or CDbl(bg) > CInt("&HFFFFFF") Then bg = CInt("&HFFFFFF")

        ' convert hexadecimal RGB to arrays for imagecolorallocate
        Dim fgrgb() As Integer = Hex2RGB(CInt(fg))
        Dim bgrgb() As Integer = Hex2RGB(CInt(bg))

        ' replace black and white with fgc and bgc
        Dim fgc As Byte = CByte(imagecolorallocate(image, fgrgb(CInt("r") - 1), fgrgb(CInt("g") - 1), fgrgb(CInt("b") - 1)))
		Dim bgc As Byte = CByte(imagecolorallocate(image, bgrgb(CInt("r") - 1), bgrgb(CInt("g") - 1), bgrgb(CInt("b") - 1)))
        If CBool(bgtrans) Then imagecolortransparent.GetValue(image, bgc)

        ' update white to bgc
        imagefilledrectangle.GetValue(image, 0, 0, image_size, image_size, bgc)

        Dim tempForEndVar As Double = CDbl(getModuleCount()) - 1
        For r As Double = 0 To tempForEndVar
            Dim tempForEndVar2 As Double = CDbl(getModuleCount()) - 1
            For c As Integer = 0 To tempForEndVar2
				If isDark(r, c) Then
                    ' update black to fgc
                    imagefilledrectangle.GetValue(image, CInt(CDbl(margin) + c * CDbl(size)), CInt(CDbl(margin) + r * CDbl(size)), CInt(CDbl(margin) + (c + 1) * CDbl(size) - 1), CInt(CDbl(margin) + (r + 1) * CDbl(size) - 1), fgc)
                End If
			Next
		Next

		createImage(image)
	End Function

	Public Function createImage(ByRef size As Object, ByRef margin As Object, ByRef fg As Object, ByRef bg As Object) As Object
		Return createImage(size, margin, fg, bg, False)
	End Function

	Public Function createImage(ByRef size As Object, ByRef margin As Object, ByRef fg As Object) As Object
		Dim tempRefParam As Object = CInt("&HFFFFFF")
		Return createImage(size, margin, fg, tempRefParam, False)
	End Function

	Public Function createImage(ByRef size As Object, ByRef margin As Object) As Object
		Dim tempRefParam2 As Object = CInt("&H000000")
		Dim tempRefParam3 As Object = CInt("&HFFFFFF")
		Return createImage(size, margin, tempRefParam2, tempRefParam3, False)
	End Function

	Public Function createImage(ByRef size As Object) As Object
		Dim tempRefParam4 As Object = 2
		Dim tempRefParam5 As Object = CInt("&H000000")
		Dim tempRefParam6 As Object = CInt("&HFFFFFF")
		Return createImage(size, tempRefParam4, tempRefParam5, tempRefParam6, False)
	End Function

	Public Function createImage() As Object
		Dim tempRefParam7 As Object = 2
		Dim tempRefParam8 As Object = 2
		Dim tempRefParam9 As Object = CInt("&H000000")
		Dim tempRefParam10 As Object = CInt("&HFFFFFF")
		Return createImage(tempRefParam7, tempRefParam8, tempRefParam9, tempRefParam10, False)
	End Function

	Private Function imagefilledrectangle() As Object

	End Function

	Private Function imagecolortransparent() As Object

	End Function

	Private Function imagecolorallocate() As Byte(, , , )

	End Function

	Private Function imagecreatetruecolor() As Integer(, )

	End Function

    Public Function printQR(Optional size = 2, Optional foreColor = vbBack) As Bitmap
        Dim b As Bitmap = New Bitmap(CInt(getModuleCount()), CInt(getModuleCount()))
        Dim r, c As Int16
        Dim e As System.Windows.Forms.PaintEventArgs
        Dim blackPen As New Pen(Color.Black)
        Dim g = System.Drawing.Graphics.FromImage(b)
        'g.DrawRectangle(blackPen, New Rectangle(0, 0, 200, 200))

        For r = 0 To getModuleCount() - 1
            For c = 0 To getModuleCount() - 1
                If isDark(r, c) Then
                    'g.FillRectangle(Brushes.Black, New Rectangle(0, 0, 200, 200))
                    g.FillRectangle(Brushes.Black, c, r, c + 1, r + 1)
                Else
                    g.FillRectangle(Brushes.White, c, r, c + 1, r + 1)
                End If
            Next
        Next

        g.dispose()
        Return b
        'b.Save("d:\bitmap1.bmp")
    End Function

    Public Function printQROLD(ByVal qrPicture As PictureBox, ByVal size As Object, ByVal foreColor As Object) As Object


        '      '-> on calcule la taille de l'image
        '      Dim imageWidth As Integer = CInt(CDbl(size) * CDbl(getModuleCount()) * 10)
        '      Dim imageHeight As Integer = CInt(CDbl(size) * CDbl(getModuleCount()) * 10)

        '      qrPicture.setScaleMode(PrinterHelper.ScaleModeConstants.VbPixels)

        '      'qrPicture.Width = imageWidth
        '      'qrPicture.Height = imageHeight
        '      qrPicture.setScaleWidth(CSng(getModuleCount()))
        '      qrPicture.setScaleHeight(CSng(getModuleCount()))
        '      qrPicture.setDrawWidth(1)
        '      qrPicture.setforeColor(ColorTranslator.FromOle(CInt(foreColor)))
        '      qrPicture.setDrawStyle(UpgradeSolution1Support.UpgradeStubs.VBRUN_DrawStyleConstants.getvbSolid())
        '      Dim square As Integer = CInt(qrPicture.Height * 15 / CDbl(getModuleCount()))

        '      Dim tempForEndVar As Double = CDbl(getModuleCount()) - 1
        '      For r As Double = 0 To tempForEndVar
        '	Application.DoEvents()
        '          Dim tempForEndVar2 As Double = CDbl(getModuleCount()) - 1
        '          For c As Integer = 0 To tempForEndVar2
        '		If isDark(r, c) Then
        '			Using g As Graphics = qrPicture.CreateGraphics()
        '				g.FillRectangle(New SolidBrush(System.Drawing.Color.Black), c, r, c + 1, r + 1)
        '			End Using
        '		Else
        '			Using g As Graphics = qrPicture.CreateGraphics()
        '				g.FillRectangle(New SolidBrush(System.Drawing.Color.White), c, r, c + 1, r + 1)
        '			End Using
        '		End If
        '	Next
        'Next

    End Function

    Public Function printQR(ByVal size As Object) As Object
        Return printQR(size, ColorTranslator.ToOle(Color.Black))
    End Function

    Public Function printQR() As Object
        Return printQR(2, ColorTranslator.ToOle(Color.Black))
    End Function


    Public Function debugModule() As Object
		Dim strTemp As New StringBuilder()
		Exit Function
		cpt += 1
		Debug.WriteLine(CStr(cpt))
        Dim tempForEndVar As Double = CDbl(getModuleCount()) - 1
        For r As Double = 0 To tempForEndVar
			Application.DoEvents()
			strTemp = New StringBuilder("")
            Dim tempForEndVar2 As Double = CDbl(getModuleCount()) - 1
            For c As Integer = 0 To tempForEndVar2
				If isDark(r, c) Then
					strTemp.Append("x")
				Else
					strTemp.Append("o")
				End If
			Next
			Debug.WriteLine(strTemp.ToString())
		Next
	End Function
End Class