Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Friend Class QRKanji
	'---------------------------------------------------------------
	' QRKanji
	'---------------------------------------------------------------

	Public aQRData As QRData

	Public Function QRKanji(ByVal data As Object) As Object
		aQRData.QRData(QR_MODE_KANJI, data)
	End Function

	'renamed write to writeB
	Public Function writeB(ByVal buffer As Object) As Object
		Dim c As Integer

        Dim data() As String = aQRData.getData()
        Dim i As Integer = 0
		Do While i + 1 < data.GetUpperBound(0)
			c = ShiftLeft(CInt(CStr(CInt("&Hff")) & CStr(Strings.Asc(data(i)(0)))), 8) Or (CInt(CStr(CInt("&Hff")) & CStr(Strings.Asc(data(i + 1)(0)))))
			If CInt("&H8140") <= c And c <= CInt("&H9FFC") Then
				c -= CInt("&H8140")
			ElseIf CInt("&HE040") <= c And c <= CInt("&HEBBF") Then 
				c -= CInt("&HC140")
			Else
				trigger_error("illegal char at " & (CStr(i + 1)) & "/c", E_USER_ERROR)
			End If
			c = (CDbl(CStr(ShiftRight(c, 8)) & CStr(CInt("&Hff")))) * CInt("&HC0") + (CDbl(CStr(c) & CStr(CInt("&Hff"))))
            buffer.putB(c, 13)
            i += 2
		Loop 

		If i < data.GetUpperBound(0) Then
			trigger_error("illegal char at " & (CStr(i + 1)), E_USER_ERROR)
		End If
	End Function

	Public Function getLength() As Object
		Return Math.Floor(CDbl(aQRData.getData().GetUpperBound(0) / 2))
	End Function
End Class