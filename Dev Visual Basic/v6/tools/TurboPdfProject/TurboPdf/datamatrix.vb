Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Specialized
Imports System.Drawing
Imports System.Runtime.InteropServices
Imports System.Windows.Forms
Imports UpgradeHelpers.Helpers
Imports UpgradeHelpers.VB
Imports UpgradeSolution1Support.UpgradeStubs
Module datamatrix

    'Public Declare Function Polygon Lib "gdi32" (ByVal hdc As Integer, ByRef lpPoint As UpgradeSolution1Support.UnsafeNative.Structures.POINTAPI, ByVal nCount As Integer) As Integer
    'structure de coordonnées
    Private apt(, ) As Integer
	Private columns As Integer
	Private lines As Integer
	Private angle As Integer
	Private vcos As Integer
	Private vsin As Integer
	Private drw As Double
	Private apic As PictureBox
	Private X As Integer
	Private Y As Integer
	Private x1 As Integer
	Private y1 As Integer
	Private x2 As Integer
	Private y2 As Integer

	Public num As Object
	Public E_USER_ERROR As Integer

	ReadOnly Public QR_MODE_NUMBER As Byte = 1 < 1
	Public Const QR_MODE_ALPHA_NUM As Byte = 2
	Public Const QR_MODE_8BIT_BYTE As Byte = 4
	ReadOnly Public QR_MODE_KANJI As Byte = 1 < 8
	Public Const QR_MASK_PATTERN000 As Byte = 0
	Public Const QR_MASK_PATTERN001 As Byte = 1
	Public Const QR_MASK_PATTERN010 As Byte = 2
	Public Const QR_MASK_PATTERN011 As Byte = 3
	Public Const QR_MASK_PATTERN100 As Byte = 4
	Public Const QR_MASK_PATTERN101 As Byte = 5
	Public Const QR_MASK_PATTERN110 As Byte = 6
	Public Const QR_MASK_PATTERN111 As Byte = 7
	Public Const QR_ERROR_CORRECT_LEVEL_L As Byte = 1
	Public Const QR_ERROR_CORRECT_LEVEL_M As Byte = 0
	Public Const QR_ERROR_CORRECT_LEVEL_Q As Byte = 3
	Public Const QR_ERROR_CORRECT_LEVEL_H As Byte = 2

	Public bitsInitialized As Boolean

	Public Const SNOBITS As Integer = 0
	Public Const SBIT0 As Integer = 1
	Public Const SBIT1 As Integer = SBIT0 * 2
	Public Const SBIT2 As Integer = SBIT1 * 2
	Public Const SBIT3 As Integer = SBIT2 * 2
	Public Const SBIT4 As Integer = SBIT3 * 2
	Public Const SBIT5 As Integer = SBIT4 * 2
	Public Const SBIT6 As Integer = SBIT5 * 2
	Public Const SBIT7 As Integer = SBIT6 * 2
	Public Const SBIT8 As Integer = SBIT7 * 2
	Public Const SBIT9 As Integer = SBIT8 * 2
	Public Const SBIT10 As Integer = SBIT9 * 2
	Public Const SBIT11 As Integer = SBIT10 * 2
	Public Const SBIT12 As Integer = SBIT11 * 2
	Public Const SBIT13 As Integer = SBIT12 * 2
	Public Const SBIT14 As Integer = SBIT13 * 2
	ReadOnly Public SBIT15 As Integer = (Not 0) And (Not (SBIT0 Or SBIT1 Or SBIT2 Or SBIT3 Or SBIT4 Or SBIT5 Or SBIT6 Or SBIT7 Or SBIT8 Or SBIT9 Or SBIT10 Or SBIT11 Or SBIT12 Or SBIT13 Or SBIT14))


	Public Const NOBITS As Integer = 0
	Public Const BIT0 As Integer = 1
	Public Const BIT1 As Integer = BIT0 * 2
	Public Const BIT2 As Integer = BIT1 * 2
	Public Const BIT3 As Integer = BIT2 * 2
	Public Const BIT4 As Integer = BIT3 * 2
	Public Const BIT5 As Integer = BIT4 * 2
	Public Const BIT6 As Integer = BIT5 * 2
	Public Const BIT7 As Integer = BIT6 * 2
	Public Const BIT8 As Integer = BIT7 * 2
	Public Const BIT9 As Integer = BIT8 * 2
	Public Const BIT10 As Integer = BIT9 * 2
	Public Const BIT11 As Integer = BIT10 * 2
	Public Const BIT12 As Integer = BIT11 * 2
	Public Const BIT13 As Integer = BIT12 * 2
	Public Const BIT14 As Integer = BIT13 * 2
	Public Const BIT15 As Integer = BIT14 * 2
	Public Const BIT16 As Integer = BIT15 * 2
	Public Const BIT17 As Integer = BIT16 * 2
	Public Const BIT18 As Integer = BIT17 * 2
	Public Const BIT19 As Integer = BIT18 * 2
	Public Const BIT20 As Integer = BIT19 * 2
	Public Const BIT21 As Integer = BIT20 * 2
	Public Const BIT22 As Integer = BIT21 * 2
	Public Const BIT23 As Integer = BIT22 * 2
	Public Const BIT24 As Integer = BIT23 * 2
	Public Const BIT25 As Integer = BIT24 * 2
	Public Const BIT26 As Integer = BIT25 * 2
	Public Const BIT27 As Integer = BIT26 * 2
	Public Const BIT28 As Integer = BIT27 * 2
	Public Const BIT29 As Integer = BIT28 * 2
	Public Const BIT30 As Integer = BIT29 * 2
	ReadOnly Public BIT31 As Integer = (Not 0) And (Not (BIT0 Or BIT1 Or BIT2 Or BIT3 Or BIT4 Or BIT5 Or BIT6 Or BIT7 Or BIT8 Or BIT9 Or BIT10 Or BIT11 Or BIT12 Or BIT13 Or BIT14 Or BIT15 Or BIT16 Or BIT17 Or BIT18 Or BIT19 Or BIT20 Or BIT21 Or BIT22 Or BIT23 Or BIT24 Or BIT25 Or BIT26 Or BIT27 Or BIT28 Or BIT29 Or BIT30))

	ReadOnly Private BITS0thru4 As Integer = BIT0 Or BIT1 Or BIT2 Or BIT3 Or BIT4
	ReadOnly Private BITS0thru7 As Integer = BIT0 Or BIT1 Or BIT2 Or BIT3 Or BIT4 Or BIT5 Or BIT6 Or BIT7
	ReadOnly Private BITS8thru15 As Integer = BIT8 Or BIT9 Or BIT10 Or BIT11 Or BIT12 Or BIT13 Or BIT14 Or BIT15
	ReadOnly Private BITS16thru23 As Integer = BIT16 Or BIT17 Or BIT18 Or BIT19 Or BIT20 Or BIT21 Or BIT22 Or BIT23
	ReadOnly Private BITS24thru31 As Integer = BIT24 Or BIT25 Or BIT26 Or BIT27 Or BIT28 Or BIT29 Or BIT30 Or BIT31

	ReadOnly Public BYTE0 As Integer = BITS0thru7
	ReadOnly Public BYTE1 As Integer = BITS8thru15
	ReadOnly Public BYTE2 As Integer = BITS16thru23
	ReadOnly Public BYTE3 As Integer = BITS24thru31

	Private Const strOne As String = "1"

	Public bits(31) As Integer
	Public SBits(15) As Integer

	Private testbitvect() As Integer
	Private testbitvect2() As Integer


	Public qrDataList As OrderedDictionary
	Public QR_G15 As Integer
	Public QR_G18 As Integer
	Public QR_G15_MASK As Integer

	Public aQRMath As QRMath

    Public Structure POINTAPI
        Public x As Integer
        Public y As Integer
    End Structure

    <STAThread>
    Public Sub Main2()
        'Application.EnableVisualStyles()
        'Application.SetCompatibleTextRenderingDefault(False)
        QR_G15 = ShiftLeft(1, 10) Or ShiftLeft(1, 8) Or ShiftLeft(1, 5) Or ShiftLeft(1, 4) Or ShiftLeft(1, 2) Or ShiftLeft(1, 1) Or ShiftLeft(1, 0)
        QR_G15_MASK = ShiftLeft(1, 14) Or ShiftLeft(1, 12) Or ShiftLeft(1, 10) Or ShiftLeft(1, 4) Or ShiftLeft(1, 1)
        QR_G18 = ShiftLeft(1, 12) Or ShiftLeft(1, 11) Or ShiftLeft(1, 10) Or ShiftLeft(1, 9) Or ShiftLeft(1, 8) Or ShiftLeft(1, 5) Or ShiftLeft(1, 2) Or ShiftLeft(1, 0)
        aQRMath = New QRMath()
        'Application.Run(Form1.DefInstance)
    End Sub

    Public Function trigger_error(ByVal sString As String, ByVal typeError As Integer) As Object

	End Function

	Public Function Max(ByVal i As Integer, ByVal j As Integer) As Integer
		If i > j Then
			Return i
		Else
			Return j
		End If
	End Function

	'Function ShiftRight(ByVal lngNumber As Long, ByVal intNumBits As Integer) As Long
	'    ShiftRight = lngNumber \ 2 ^ intNumBits 'note the integer division op
	'
	'End Function

	Function ShiftLeft(ByVal lngNumber As Integer, ByVal intNumBits As Integer) As Integer
		Return lngNumber * 2 ^ intNumBits

	End Function

    'debug.Print drawdatamatrix("123456789012",300,300)
    Public Function DrawDataMatrix(ByVal strValue As String, ByVal width As Integer, ByVal height As Integer) As Bitmap
        apic = New PictureBox
        apic.BackColor = Color.FromArgb(255, 255, 0)
        apt = getDigit(strValue, False)
        'apic = pictureB
        Dim angle As Integer = 0 '90
        'apic.SizeMode = IIf(1, PictureBoxSizeMode.AutoSize, PictureBoxSizeMode.Normal)
        'apic.setAutoRedraw(True)
        'apic.setScaleMode(PrinterHelper.ScaleModeConstants.VbPixels)
        apic.Width = width * 1.5 / 15
        apic.Height = height * 1.5 / 15
        'apic.setScaleWidth(width)
        'apic.setScaleHeight(height)
        'apic.setDrawStyle(UpgradeSolution1Support.UpgradeStubs.VBRUN_DrawStyleConstants.getvbSolid())
        'apic.setFillStyle(UpgradeSolution1Support.UpgradeStubs.VBRUN_FillStyleConstants.getvbFSSolid())
        'apic.setforeColor(Color.Black)
        'apic.setFillColor(Color.Black)
        'apic.setDrawWidth(1)

        'apic.Cls()
        drw = Ceil(apic.ClientRectangle.Width * 29.79 / (apt.GetUpperBound(0)) + 1) / 10
        Dim xx As Double = drw * apt.GetUpperBound(0) / 2
        Dim yy As Double = xx
        'apic.setDrawWidth(1) 'drw
        'apic.setDrawStyle(UpgradeSolution1Support.UpgradeStubs.VBRUN_DrawStyleConstants.getvbDash())
        Return digitToRenderer(xx, yy, angle, drw, drw, apt)
        'apic.Refresh()
        'apic.Picture = apic.image
        'SavePicture apic.image, "test.bmp"
        'Form1.Picture1.Picture = LoadPicture("test.bmp")



    End Function

    Function Ceil(ByVal n As Double) As Integer
		Dim result2 As Integer = 0
		If Math.Floor(n) <> n Then
			result2 = CInt(Math.Floor(n) + 1)
		Else
			result2 = CInt(n)
		End If

		'-> test
		Return CInt(n)


	End Function

	Private Function fn(ByVal points() As Double) As Object

        Dim pts(3) As POINTAPI


        Dim i As Integer = 1
		pts(0).X = CInt(points(0) * i)
		pts(0).Y = CInt(points(1) * i)
		pts(1).X = CInt(points(2) * i)
		pts(1).Y = CInt(points(3) * i)
		pts(2).X = CInt(points(4) * i)
		pts(2).Y = CInt(points(5) * i)
		pts(3).X = CInt(points(6) * i)
		pts(3).Y = CInt(points(7) * i)

		Using g As Graphics = apic.CreateGraphics()
            'gdi32.Polygon(g.GetHdc().ToInt32(), pts(0), pts.GetUpperBound(0) + 1)
            'g.ReleaseHdc()
        End Using
		'apic.PSet (points(0) * drw, points(1) * drw), QBColor(0)
	End Function

	Private Function Deg2Rad(ByVal degrees As Single) As Single
		Return degrees / 57.29578
	End Function

    Private Function digitToRenderer(ByRef xi As Double, ByRef yi As Double, ByRef angle As Double, ByVal mw As Double, ByVal mh As Double, ByVal digit(,) As Integer) As Bitmap
        'Dim X As Integer
        'Dim Y As Integer
        Dim z, xA, xB, xC, xD, yA, yB, yC, yD As Integer
        Dim r, c As Int16
        Dim e As System.Windows.Forms.PaintEventArgs
        'g.DrawRectangle(blackPen, New Rectangle(0, 0, 200, 200))

        lines = digit.GetUpperBound(0) + 1
        columns = digit.GetUpperBound(0) + 1
        angle = Deg2Rad(-angle)
        vcos = CInt(Math.Cos(angle))
        vsin = CInt(Math.Sin(angle))

        Dim b As Bitmap = New Bitmap(lines * 100, columns * 100)
        Dim blackPen As New Pen(Color.Black)
        Dim g = System.Drawing.Graphics.FromImage(b)
        g.FillRectangle(Brushes.White, 0, 0, lines * 100, columns * 100)
        rotate(columns * mw / 2, lines * mh / 2, vcos, vsin, X, Y)
        xi -= X
        yi -= Y
        For Y = 0 To lines - 1
            X = -1
            While (X < columns - 1)
                X += 1
                If digit(Y, X) = "1" Then
                    z = X
                    While (z + 1 < columns) And (digit(Y, z + 1) = "1")
                        z += 1
                    End While
                    x1 = CInt(X * mw)
                    y1 = CInt(Y * mh)
                    x2 = CInt((z + 1) * mw)
                    y2 = CInt((Y + 1) * mh)
                    rotate(x1, y1, vcos, vsin, xA, yA)
                    rotate(x2, y1, vcos, vsin, xB, yB)
                    rotate(x2, y2, vcos, vsin, xC, yC)
                    rotate(x1, y2, vcos, vsin, xD, yD)
                    'Debug.Print X & "  xAyA:(" & (xA + xi) & "," & (yA + yi) & ") xByB:(" & (xB + xi) & "," & (yB + yi) & ") xCyC:(" & (xC + xi) & "," & (yC + yi) & ") xDyD:(" & (xD + xi) & "," & (yD + yi) & ")" & Chr(13)
                    'fn({xA + xi, yA + yi, xB + xi, yB + yi, xC + xi, yC + yi, xD + xi, yD + yi})
                    g.FillRectangle(Brushes.Black, CSng(xA + xi), CSng(yA + yi), CSng(xC + xi) - CSng(xA + xi), CSng(yC + yi) - CSng(yA + yi))
                    X = z + 1
                End If
            End While
        Next

        g.dispose()
        'b.Save("d:\bitmap1.bmp")

        Return b
    End Function

    Private Function rotate(ByVal x1 As Double, ByVal y1 As Double, ByVal vcos As Double, ByVal vsin As Double, ByRef X As Double, ByRef Y As Double) As Object
		X = x1 * vcos - y1 * vsin
		Y = x1 * vsin + y1 * vcos
	End Function

	Public Function rotateP(ByVal x1 As Double, ByVal y1 As Double, ByRef angle As Double, ByRef X As Double, ByRef Y As Double) As Object
		angle = Deg2Rad(-angle)
		vcos = CInt(Math.Cos(angle))
		vsin = CInt(Math.Sin(angle))
		X = x1 * vcos - y1 * vsin
		Y = x1 * vsin + y1 * vcos
	End Function

	Private Function Result(ByVal xi As Double, ByVal yi As Double, ByVal columns As Double, ByVal lines As Double, ByVal mw As Double, ByVal mh As Double, ByVal vcos As Double, ByVal vsin As Double) As Object
		'       rotate 0, 0, vcos, vsin, x1, y1
		'        rotate columns * mw, 0, vcos, vsin, x2, y2
		'        rotate columns * mw, lines * mh, vcos, vsin, X3, Y3
		'        rotate 0, lines * mh, vcos, vsin, x4, y4

		'        return Array(
		'            'width' => $columns * $mw,
		'            'height'=> $lines * $mh,
		'            'p1' => array(
		'                'x' => $xi + $x1,
		'                'y' => $yi + $y1
		'            ),
		'            'p2' => array(
		'                'x' => $xi + $x2,
		'                'y' => $yi + $y2
		'            ),
		'            'p3' => array(
		'                'x' => $xi + $x3,
		'                'y' => $yi + $y3
		'            ),
		'            'p4' => array(
		'                'x' => $xi + $x4,
		'                'y' => $yi + $y4
		'            )
		'        );
	End Function
End Module