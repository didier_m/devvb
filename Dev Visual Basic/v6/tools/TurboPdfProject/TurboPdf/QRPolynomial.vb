Option Strict Off
Option Explicit On
Imports System
Imports System.Text
Friend Class QRPolynomial
	'---------------------------------------------------------------
	' QRPolynomial
	'---------------------------------------------------------------


	Public num As Object

    Public Sub New()
        'MessageBox.Show("", "", MessageBoxButtons.OK)

    End Sub

    Public Sub Polynomial(ByVal tNum As Object, Optional ByVal Shift As Integer = 0)

        Dim offset As Integer = 0
        Dim length As Integer = tNum.GetUpperBound(0) + 1

        Do While offset < length And CDbl(tNum(offset)) = 0
            offset += 1
            If offset = length Then Exit Do
        Loop

        num = aQRMath.createNumArray(length - offset + Shift)

        '-> le -1 c'est pour le strictement
        For i As Integer = 0 To length - offset - 1
            num.SetValue(tNum.GetValue(i + offset), i)
        Next

    End Sub

    '. renamed get to getB
    Public Function getB(ByVal index As Object) As Integer
        Return CInt(num.GetValue(CInt(index)))
    End Function

	Public Function getLength() As Object
		Return num.GetUpperBound(0)
	End Function

	Public Function toString_Renamed() As Object

		Dim buffer As New StringBuilder()

        Dim tempForEndVar As Double = CDbl(getLength()) - 1
        For i As Integer = 0 To tempForEndVar
			If i > 0 Then
				buffer.Append(",")
			End If
			buffer = New StringBuilder(CStr(CDbl(buffer.ToString()) + getB(i)))
		Next

		Return buffer.ToString()
	End Function

	Public Function toLogString() As Object

        Dim buffer As String = ""

        Dim tempForEndVar As Double = CDbl(getLength()) - 1
        For i As Integer = 0 To tempForEndVar
			If i > 0 Then
                buffer = buffer + ","
            End If
            buffer = buffer.ToString() + aQRMath.glog(getB(i))
        Next

		Return buffer.ToString()
	End Function

	Public Function multiply(ByVal e As QRPolynomial) As QRPolynomial

        Dim tNum() As Integer = aQRMath.createNumArray(CDbl(getLength()) + CDbl(e.getLength()) - 1 - 1)

        Dim tempForEndVar As Double = CDbl(getLength()) - 1
        For i As Double = 0 To tempForEndVar
            Dim tempForEndVar2 As Double = CDbl(e.getLength()) - 1
            For j As Integer = 0 To tempForEndVar2
                tNum(CInt(i + j)) = tNum(CInt(i + j)) Xor aQRMath.gexp(CDbl(aQRMath.glog(getB(i))) + CDbl(aQRMath.glog(e.getB(j))))
            Next
		Next

		Dim aQRPolynomial As New QRPolynomial()
		aQRPolynomial.Polynomial(tNum)
		Return aQRPolynomial
	End Function

	'. renamed mod to modB
	Public Function modB(ByVal e As QRPolynomial) As QRPolynomial
		Dim newPolynomial As QRPolynomial
        If CDbl(getLength()) - CDbl(e.getLength()) < 0 Then
            '-> on essaye de retourner le this
            newPolynomial = New QRPolynomial()
            newPolynomial.num = num
            Return newPolynomial
        End If

        Dim ratio As Integer = CDbl(aQRMath.glog(getB(0))) - CDbl(aQRMath.glog(e.getB(0)))

        Dim numB() As Integer = aQRMath.createNumArray(CInt(getLength()))
        Dim tempForEndVar As Double = CDbl(getLength()) - 1
        For i As Integer = 0 To tempForEndVar
			numB(i) = getB(i)
		Next

        Dim tempForEndVar2 As Double = CDbl(e.getLength()) - 1
        For i As Integer = 0 To tempForEndVar2
            numB(i) = numB(i) Xor (aQRMath.gexp(CDbl(aQRMath.glog(e.getB(i))) + ratio))
        Next

		newPolynomial = New QRPolynomial()
		'-> rajout du shift pour diminuer le create array
		newPolynomial.Polynomial(numB, -1)
		Return newPolynomial.modB(e)
	End Function
End Class