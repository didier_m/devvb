VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cellule"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'**********************************************
'*                                            *
'* Cette classe contient la d�finition d'un   *
'* Objet Cellule                              *
'*                                            *
'**********************************************

'-> Variables de position
Public x1 As Long
Public y1 As Integer
Public x2 As Long
Public y2 As Integer

'-> Coordonn�es en Ligne/Colonne
Public Ligne As Integer
Public Colonne As Integer

'-> Couleur de fond de la cellule
Public BackColor As Long

'-> Bordures stock�es en type priv� pour utilisation des Property
Public BordureBas As Boolean
Public BordureHaut As Boolean
Public BordureGauche As Boolean
Public BordureDroite As Boolean

'-> Propri�t�s de Font
Public FontName As String
Public FontSize As Integer
Public FontColor As Long
Public FontBold As Boolean
Public FontItalic As Boolean
Public FontUnderline As Boolean

Public FondTransparent As Boolean

'-> Alignement interne : Valeurs de 1 � 9
Public CellAlign As Integer

'-> Contenu de la cellule
Public Contenu As String
Public ContenuCell As String

'-> Retour � la ligne automatique du contenu
Public AutoAjust As Boolean

'-> Format li� � la cellule
Public TypeValeur As Integer 'Valeur des diff�rentes formats
 'Format � appliquer avec : _
    0 -> Pas de format _
    1 -> Num�rique _
    2 -> Caractere
Public Msk As String
' -> pour msk si le format est numrique :
' 3 entr�es s�par�es par des �
' 1 : Nombre de d�cimal
' 2 : S�parateur de milier
' 3 : Signe n�gatif : Index de 0 � 4 ( voir fichier MessProg [frmFormatCell]
' -> pour msk si le format est caract�re  :
' 1 entr�e qui contient le nombre de caract�re � garder

'-> Dans le cas des exports Excel
Public IsFusion As Boolean
Public ColFusion As Integer

'-> Pour gestion des champs en interne
Private ListeField() As String
Public nbField As Integer
'-> Indique si la zone est num�rique si on doit imprimer un 0
Public PrintZero As Boolean

'-> Pour gestion des export
Public aRange As clsRange

'-> Liste des cellules dans la feulle active concern�es par ce format
Public ListeCell As String

'---> Autoriser l'acc�s au d�tail depuis cette cellule
Public UseAccesDet As Boolean
Public KeyAccesDet As String
Public LibelVariable As String

'---> Creation de lien hypertext
Public UseHyperText As Boolean
Public KeyLink As String

'---> Creation de zone modifiable
Public UseFormText As Integer
Public KeyForm As String
Public KeyFormIndex As Integer
Public Image As String
Public ImageHeight As String
Public ImageWidth As String

Public ImageIndex As Integer
Public Fichier As String
Public CXL As String

Public Function GetBordureByIndex(ByVal Index As Integer)

Dim Bas As Boolean
Dim Haut As Boolean
Dim Gauche As Boolean
Dim Droite As Boolean


Select Case Index

    Case 0
    
        Bas = False
        Haut = False
        Gauche = False
        Droite = False
                
    Case 1
    
        Bas = True
        Haut = True
        Gauche = True
        Droite = False
        
    Case 2
    
        Bas = True
        Haut = True
        Gauche = False
        Droite = True
            
    Case 3
    
        Bas = False
        Haut = True
        Gauche = True
        Droite = True
        
    Case 4
    
        Bas = True
        Haut = False
        Gauche = True
        Droite = True
    Case 5
    
        Bas = False
        Haut = False
        Gauche = True
        Droite = False
    
    Case 6
    
        Bas = False
        Haut = True
        Gauche = False
        Droite = False
    
    Case 7
    
        Bas = False
        Haut = False
        Gauche = False
        Droite = True
    
    Case 8
    
        Bas = True
        Haut = False
        Gauche = False
        Droite = False
    
    Case 9
    
        Bas = False
        Haut = False
        Gauche = True
        Droite = True
    
    Case 10
    
        Bas = True
        Haut = True
        Gauche = False
        Droite = False
    
    Case 11
    
        Bas = False
        Haut = True
        Gauche = True
        Droite = False
    
    Case 12
    
        Bas = False
        Haut = True
        Gauche = False
        Droite = True
    
    Case 13
    
        Bas = True
        Haut = False
        Gauche = True
        Droite = False
    
    Case 14
    
        Bas = True
        Haut = False
        Gauche = False
        Droite = True
    
    Case 15
    
        Bas = True
        Haut = True
        Gauche = True
        Droite = True
        
End Select

BordureBas = Bas
BordureDroite = Droite
BordureGauche = Gauche
BordureHaut = Haut
    
End Function


Public Function InitChamp()

Dim i As Integer
Dim pos As Integer

pos = 1

Do
    i = InStr(pos, Contenu, "^")
    If i = 0 Then Exit Do
    nbField = nbField + 1
    ReDim Preserve ListeField(1 To nbField)
    ListeField(nbField) = Mid$(Contenu, i, 5)
    pos = i + 1
Loop

End Function


Public Function GetChamp(ByVal nIndex As Integer) As String


GetChamp = ListeField(nIndex)

End Function

Public Sub ReplaceField(ByRef DataListe As String, Optional ean As Boolean)

'---> Remplacer les champs par leur valeur
Dim ChampValue As String
Dim i As Integer
Dim j As Integer
Dim k As Integer

ContenuCell = Contenu
If Trim(DataListe) = "" Then Exit Sub

'-> Gestion de l'acc�s direct
i = InStr(1, DataListe, "^ACDET")
If i <> 0 Then
    Me.KeyAccesDet = Trim(Mid$(DataListe, i + 6))
Else
    Me.KeyAccesDet = ""
End If

For i = 1 To nbField
    j = InStr(1, DataListe, ListeField(i))
    If j <> 0 Then
        k = InStr(j + 1, DataListe, "^")
        If k = 0 Then k = Len(DataListe)
        '-> on regarde si ^ n'est pas une valeur si c'est une valeur il est suivi de //
        If k <> 0 Then
            Do While Mid(DataListe, k, 3) = "^//"
                k = InStr(k + 1, DataListe, "^")
                If k = 0 Then
                    k = Len(DataListe)
                    Exit Do
                End If
            Loop
        End If
        ChampValue = RTrim(Mid$(DataListe, j + 5, k - (j + 5)))
        '-> on regarde si on a a faire avec un libell� variable
        If InStr(1, LibelVariable, ListeField(i)) <> 0 Then 'And TypeOf Sortie Is PictureBox Then
            ChampValue = Entry(1, Mid(LibelVariable, InStr(1, LibelVariable, ListeField(i)) + 6), "�")
            ChampValue = Replace(Entry(curPage, ChampValue, "�"), Chr(0), "")
            '-> si on a rien on met les valeurs du spool
            If ChampValue = "" Then ChampValue = RTrim(Mid$(DataListe, j + 5, k - (j + 5)))
        End If
    End If
    '-> Remplacer la valuer
    ContenuCell = Replace(Replace(ContenuCell, ListeField(i), ChampValue), "^//", "^")
    ContenuCell = Replace(Replace(ContenuCell, ListeField(i), ChampValue), "dotcoma//", ";")
    If InStr(1, ContenuCell, "&#") <> 0 And Not ean Then
        Dim oDOMDoc As Object
        Set oDOMDoc = CreateObject("Msxml2.DOMDocument.6.0")
        oDOMDoc.loadXML ("<HTML>" + ContenuCell + "</HTML>")
        ContenuCell = oDOMDoc.text
    End If
Next
If isUTF8(ContenuCell) And Not ean Then
    ContenuCell = AToUTF8(ContenuCell)
End If

End Sub

Private Sub Class_Initialize()
    '-> Cas des exports vers Excel
    Me.ColFusion = 1
    Me.IsFusion = False
End Sub


Public Sub TurboToExcel()

'---> Appliquer le format � l'objet range en cours

'On Error Resume Next

'-> Initialiser l'objet Range attach� � cette cellule
Set aRange = New clsRange

'-> Gestion des bordures


'-> Bordure du haut
If BordureHaut Then
    aRange.SetBorderLineStyle 8, 1
    aRange.SetBorderColorIndex 8, -4105
End If

'-> Bordure du bas
If BordureBas Then
    aRange.SetBorderLineStyle 9, 1
    aRange.SetBorderColorIndex 9, -4105
End If

'-> Bordure gauche
If BordureGauche Then
    aRange.SetBorderLineStyle 7, 1
    aRange.SetBorderColorIndex 7, -4105
End If

'-> Bordure Droite
If BordureDroite Then
    aRange.SetBorderLineStyle 10, 1
    aRange.SetBorderColorIndex 10, -4105
End If

'-> Font
aRange.FontName = FontName

'-> Taille de la police
aRange.FontSize = FontSize

'-> Gras
aRange.FontBold = FontBold

'-> Italic
aRange.FontItalic = FontItalic

'-> Soulign�
aRange.FontUnderline = FontUnderline

'-> Couleur de fond
aRange.BackColor = BackColor

'-> Alignement interne de la cellule
Select Case CellAlign
    Case 1
        aRange.VerticalAlignment = -4160
        aRange.HorizontalAlignment = -4131
    Case 2
        aRange.VerticalAlignment = -4160
        aRange.HorizontalAlignment = -4108
    Case 3
        aRange.VerticalAlignment = -4160
        aRange.HorizontalAlignment = -4152
    Case 4
        aRange.VerticalAlignment = -4108
        aRange.HorizontalAlignment = -4131
    Case 5
        aRange.VerticalAlignment = -4108
        aRange.HorizontalAlignment = -4108
    Case 6
        aRange.VerticalAlignment = -4108
        aRange.HorizontalAlignment = -4152
    Case 7
        aRange.VerticalAlignment = -4107
        aRange.HorizontalAlignment = -4131
    Case 8
        aRange.VerticalAlignment = -4107
        aRange.HorizontalAlignment = -4108
    Case 9
        aRange.VerticalAlignment = -4107
        aRange.HorizontalAlignment = -4152
End Select
    
'-> Retour � la ligne automatique
aRange.WrapText = AutoAjust

End Sub


