VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Block"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'**********************************************
'*                                            *
'* Cette classe contient la d�finition d'un   *
'* Objet Block de ligne                       *
'*                                            *
'**********************************************


'---> Propri�t�s de l'objet
Public Nom As String
Public NbLigne As Integer
Public NbCol As Integer

'---> Dimensions en CM
Public Largeur As Single
Public Hauteur As Single

'---> Propri�t�s d'alignement
Public AlignementTop As Integer
Public Top As Single
Public AlignementLeft As Integer
Public Left As Single

'-> Valeurs pour la propri�t� AlignementTop
    '-> Libre (1)
    '-> Marge haut (2)
    '-> Centr� (3)
    '-> Marge Bas (4)
    '-> Sp�cifi� (5)
    
'-> Valeurs pour la propri�t� AlignementLeft
    '-> PAS D'ALIGNEMET LIBRE EN LEFT
    '-> Marge gauche (2)
    '-> Centr� (3)
    '-> Marge droite (4)
    '-> Sp�cifi� (5)
    
'-> Pour AlignementTop et AlignementLeft , les valeurs Top et Left ne sont _
accessibles que pour la valeur Sp�cifi� (5)

'---> Dimensions en Pixels
Public LargeurPix As Single
Public HauteurPix As Integer

'---> Pour Stocker la largeur des colonnes en CM
Private pLCol() As Single
Private pHLig() As Single

'---> Pour export vers Excel
Private pExportExcel() As Boolean
Private pExportUniqueExcel() As Boolean
Public ListeExcel As String
Private pIsExported() As Boolean  'Indique si on a d�ja export� cette ligne

'---> Cellules associ�es au block
Public Cellules As Collection

'---> Gestion des ordres d'affichage
Public IdOrdreAffichage As Integer

'---> Indique si un block est en cours d'�dition
Public IsEdit As Boolean

'---> Indique le handle du block quand il est dessin� dans le tableau
Public hRgn As Long

'---> Gestion des liens
Public MasterLink As String
Public SlaveLink As String

'---> Maquette de transition
Public RangChar As String

'---> Nom du tableau parent
Public NomTb As String

'---> Pour gestion de l'acc�s au d�tail
Public UseAccesDet As Boolean '-> Indique que l'on utilise l'acc�s au d�tail
Public KeyAccesDet As String '-> Cl� d'acc�s au d�tail



Private Sub Class_Initialize()

    Set Cellules = New Collection

End Sub


Public Sub Init()

'---> Cette proc�dure initalise les matrices qui contiennent les diff�rentes largeurs _
des lignes et des colonnes

Dim i As Integer

ReDim Preserve pLCol(1 To NbCol)
ReDim Preserve pHLig(1 To NbLigne)
ReDim Preserve pExportExcel(1 To NbLigne)
ReDim Preserve pExportUniqueExcel(1 To NbLigne)
ReDim Preserve pIsExported(1 To NbLigne)

'-> De base on exporte toutes les lignes
For i = 1 To NbLigne
    pExportExcel(i) = True
Next

End Sub

Public Sub SetLargeurCol(ByVal IdCol As Integer, ByVal LargeurCm As Single)
    pLCol(IdCol) = LargeurCm
End Sub

Public Function GetLargeurCol(ByVal IdCol As Integer) As Single
    GetLargeurCol = pLCol(IdCol)
End Function

Public Sub SetHauteurLig(ByVal IdLig As Integer, ByVal HauteurCm As Single)
    pHLig(IdLig) = HauteurCm
End Sub

Public Function GetHauteurLig(ByVal IdLig As Integer) As Single
    GetHauteurLig = pHLig(IdLig)
End Function

Public Function GetExportExcel(ByVal IdLig As Integer) As Boolean
    GetExportExcel = pExportExcel(IdLig)
End Function

Public Sub SetExportExcel(ByVal IdLig As Integer, IsExport As Boolean)
    pExportExcel(IdLig) = IsExport
End Sub

Public Function GetExportUniqueExcel(ByVal IdLig As Integer) As Boolean
    GetExportUniqueExcel = pExportUniqueExcel(IdLig)
End Function

Public Sub SetExportUniqueExcel(ByVal IdLig As Integer, IsExport As Boolean)
    pExportUniqueExcel(IdLig) = IsExport
End Sub

Public Sub SetIsExportedLig(ByVal IdLig As Integer, IsExported As Boolean)
    pIsExported(IdLig) = IsExported
End Sub

Public Function GetIsExportedLig(ByVal IdLig As Integer) As Boolean
    GetIsExportedLig = pIsExported(IdLig)
End Function

Public Function IsOneExportLig() As Boolean

'---> cette proc�dure d�termine si au moins une ligne est exportable vers excel

Dim i As Integer

For i = 1 To NbLigne
    If pExportExcel(i) Then
        IsOneExportLig = True
        Exit Function
    End If
Next

End Function
