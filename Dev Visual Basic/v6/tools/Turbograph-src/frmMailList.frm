VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "mscomctl.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.Ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TabCtl32.Ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.ocx"
Begin VB.Form frmMailList 
   AutoRedraw      =   -1  'True
   Caption         =   "Envoyer par mail"
   ClientHeight    =   7500
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13080
   Icon            =   "frmMailList.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   7500
   ScaleWidth      =   13080
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   9720
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMailList.frx":08CA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMailList.frx":1FB4
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5895
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   8415
      _ExtentX        =   14843
      _ExtentY        =   10398
      _Version        =   393216
      Tabs            =   4
      Tab             =   1
      TabsPerRow      =   4
      TabHeight       =   882
      WordWrap        =   0   'False
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "frmMailList.frx":3E9E
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "ImageList2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame3"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "frmMailList.frx":3EBA
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Frame2"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Tab 2"
      TabPicture(2)   =   "frmMailList.frx":3ED6
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame1"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "Winsock1"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "Tab 3"
      TabPicture(3)   =   "frmMailList.frx":3EF2
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Frame5"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).ControlCount=   1
      Begin VB.Frame Frame5 
         Height          =   5055
         Left            =   -74760
         TabIndex        =   32
         Top             =   600
         Width           =   5535
         Begin VB.PictureBox Picture2 
            AutoRedraw      =   -1  'True
            Height          =   255
            Left            =   2160
            ScaleHeight     =   195
            ScaleWidth      =   3075
            TabIndex        =   37
            Top             =   4200
            Width           =   3135
         End
         Begin VB.CommandButton Command4 
            Caption         =   "Imprimer"
            Height          =   375
            Left            =   4200
            TabIndex        =   36
            Top             =   4560
            Width           =   1095
         End
         Begin VB.CheckBox Check4 
            Caption         =   "Imprimer les destinataires non s�lectionn�s"
            Height          =   255
            Left            =   240
            TabIndex        =   35
            Top             =   840
            Value           =   1  'Checked
            Width           =   4575
         End
         Begin VB.CheckBox Check3 
            Caption         =   "Imprimer les destinataires s�lectionn�s"
            Height          =   255
            Left            =   240
            TabIndex        =   33
            Top             =   360
            Width           =   4575
         End
         Begin VB.Label Label10 
            BackColor       =   &H00C0C0C0&
            BackStyle       =   0  'Transparent
            Height          =   255
            Left            =   240
            TabIndex        =   34
            Top             =   4680
            Width           =   3855
         End
      End
      Begin MSWinsockLib.Winsock Winsock1 
         Left            =   -69600
         Top             =   360
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   393216
      End
      Begin VB.Frame Frame1 
         Height          =   5055
         Left            =   -74760
         TabIndex        =   23
         Top             =   600
         Width           =   5535
         Begin VB.ComboBox Combo1 
            Height          =   315
            Left            =   4800
            TabIndex        =   44
            Top             =   4640
            Width           =   2755
         End
         Begin VB.CheckBox Check1 
            Caption         =   "Utiliser le serveur de messagerie"
            Height          =   255
            Left            =   2880
            TabIndex        =   30
            Top             =   1200
            Visible         =   0   'False
            Width           =   5295
         End
         Begin VB.CommandButton Command2 
            Height          =   375
            Left            =   4200
            TabIndex        =   26
            Top             =   4560
            Width           =   1095
         End
         Begin VB.TextBox Text2 
            Height          =   285
            Left            =   840
            TabIndex        =   24
            Top             =   3720
            Width           =   4455
         End
         Begin MSComctlLib.TreeView TreeView1 
            Height          =   3255
            Left            =   240
            TabIndex        =   27
            Top             =   360
            Width           =   5055
            _ExtentX        =   8916
            _ExtentY        =   5741
            _Version        =   393217
            LabelEdit       =   1
            Style           =   7
            Checkboxes      =   -1  'True
            ImageList       =   "ImageList1"
            Appearance      =   1
         End
         Begin VB.PictureBox Picture1 
            AutoRedraw      =   -1  'True
            Height          =   255
            Left            =   4680
            ScaleHeight     =   195
            ScaleWidth      =   555
            TabIndex        =   25
            Top             =   4200
            Width           =   615
         End
         Begin MSComctlLib.ImageCombo ImageCombo1 
            Height          =   570
            Left            =   240
            TabIndex        =   45
            Top             =   4200
            Width           =   3780
            _ExtentX        =   6668
            _ExtentY        =   1005
            _Version        =   393216
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Text            =   "ImageCombo1"
            ImageList       =   "ImageList3"
         End
         Begin MSComctlLib.ImageList ImageList3 
            Left            =   3720
            Top             =   4200
            _ExtentX        =   1005
            _ExtentY        =   1005
            BackColor       =   -2147483643
            ImageWidth      =   32
            ImageHeight     =   32
            MaskColor       =   12632256
            _Version        =   393216
            BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
               NumListImages   =   7
               BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmMailList.frx":3F0E
                  Key             =   ""
               EndProperty
               BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmMailList.frx":4228
                  Key             =   ""
               EndProperty
               BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmMailList.frx":4542
                  Key             =   "LOTUS"
               EndProperty
               BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmMailList.frx":667C
                  Key             =   "OUTLOOK"
               EndProperty
               BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmMailList.frx":6F56
                  Key             =   "WWW"
               EndProperty
               BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmMailList.frx":9DE8
                  Key             =   "PJ"
               EndProperty
               BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmMailList.frx":A6C2
                  Key             =   "SMTP"
               EndProperty
            EndProperty
         End
         Begin VB.Label Label12 
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   360
            TabIndex        =   41
            Top             =   4200
            Width           =   1695
         End
         Begin VB.Label Label8 
            BackColor       =   &H00C0C0C0&
            BackStyle       =   0  'Transparent
            Height          =   255
            Left            =   240
            TabIndex        =   29
            Top             =   4680
            Width           =   3855
         End
         Begin VB.Label Label6 
            Caption         =   "CC :"
            Height          =   255
            Left            =   240
            TabIndex        =   28
            Top             =   3770
            Width           =   495
         End
      End
      Begin VB.Frame Frame3 
         Height          =   5055
         Left            =   -74760
         TabIndex        =   19
         Top             =   600
         Width           =   5535
         Begin VB.CommandButton Command6 
            Height          =   375
            Left            =   720
            Picture         =   "frmMailList.frx":DE8C
            Style           =   1  'Graphical
            TabIndex        =   43
            Top             =   4560
            Width           =   495
         End
         Begin VB.CommandButton Command5 
            Height          =   375
            Left            =   1200
            Picture         =   "frmMailList.frx":E416
            Style           =   1  'Graphical
            TabIndex        =   42
            Top             =   4560
            Width           =   375
         End
         Begin VB.TextBox Text3 
            Height          =   285
            Left            =   720
            TabIndex        =   0
            Top             =   360
            Width           =   4455
         End
         Begin VB.TextBox Text1 
            Height          =   285
            Left            =   720
            TabIndex        =   1
            Top             =   840
            Width           =   4455
         End
         Begin VB.CommandButton Command3 
            Caption         =   ">>"
            Height          =   375
            Left            =   4200
            TabIndex        =   20
            Top             =   4560
            Width           =   1095
         End
         Begin RichTextLib.RichTextBox RichTextBox1 
            Height          =   3015
            Left            =   720
            TabIndex        =   2
            Top             =   1320
            Width           =   4470
            _ExtentX        =   7885
            _ExtentY        =   5318
            _Version        =   393217
            Enabled         =   -1  'True
            TextRTF         =   $"frmMailList.frx":F078
         End
         Begin VB.Label Label9 
            Caption         =   "De :"
            Height          =   255
            Left            =   120
            TabIndex        =   31
            Top             =   360
            Width           =   495
         End
         Begin VB.Label Label7 
            Caption         =   "Obj :"
            Height          =   255
            Left            =   120
            TabIndex        =   22
            Top             =   840
            Width           =   495
         End
         Begin VB.Label Label5 
            Caption         =   "Corps :"
            Height          =   255
            Left            =   120
            TabIndex        =   21
            Top             =   1320
            Width           =   495
         End
      End
      Begin MSComctlLib.ImageList ImageList2 
         Left            =   -75000
         Top             =   120
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   5
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMailList.frx":F0FA
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMailList.frx":F9D4
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMailList.frx":102AE
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMailList.frx":10B88
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMailList.frx":11462
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin VB.Frame Frame2 
         Height          =   5055
         Left            =   240
         TabIndex        =   4
         Top             =   600
         Width           =   8055
         Begin VB.TextBox Text5 
            Height          =   285
            Left            =   2400
            TabIndex        =   46
            Top             =   360
            Width           =   5295
         End
         Begin VB.Frame Frame6 
            Height          =   615
            Left            =   360
            TabIndex        =   38
            Top             =   4320
            Width           =   4455
            Begin VB.TextBox Text4 
               Enabled         =   0   'False
               Height          =   285
               Left            =   2160
               TabIndex        =   39
               Top             =   220
               Width           =   2175
            End
            Begin VB.Label Label11 
               Caption         =   "Nom du fichier pdf"
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   40
               Top             =   220
               Width           =   1575
            End
         End
         Begin VB.CommandButton Command1 
            Caption         =   ">>"
            Height          =   375
            Left            =   4200
            TabIndex        =   18
            Top             =   4560
            Width           =   1095
         End
         Begin VB.PictureBox picTurbo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   360
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   13
            Top             =   960
            Width           =   735
            Begin VB.Image Image3 
               Height          =   510
               Left            =   150
               Picture         =   "frmMailList.frx":142F4
               Top             =   45
               Width           =   420
            End
         End
         Begin VB.PictureBox PicHtml 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   360
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   12
            Top             =   1680
            Width           =   735
            Begin VB.Image Image12 
               Height          =   480
               Left            =   120
               Picture         =   "frmMailList.frx":1473E
               Top             =   80
               Width           =   480
            End
         End
         Begin VB.Frame Frame4 
            Enabled         =   0   'False
            Height          =   1815
            Left            =   360
            TabIndex        =   7
            Top             =   2520
            Width           =   4455
            Begin VB.OptionButton Option6 
               CausesValidation=   0   'False
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   11
               Top             =   1080
               Width           =   2415
            End
            Begin VB.OptionButton Option5 
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   10
               Top             =   720
               Value           =   -1  'True
               Width           =   3975
            End
            Begin VB.OptionButton Option4 
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   9
               Top             =   360
               Width           =   3735
            End
            Begin VB.CheckBox Check2 
               Height          =   255
               Left            =   240
               TabIndex        =   8
               Top             =   1440
               Value           =   1  'Checked
               Width           =   4095
            End
         End
         Begin VB.PictureBox PicPdf 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   2760
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   6
            Top             =   1680
            Width           =   735
            Begin VB.Image Image16 
               Height          =   480
               Left            =   105
               Picture         =   "frmMailList.frx":17EF8
               Top             =   65
               Width           =   480
            End
         End
         Begin VB.PictureBox PicNothing 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   2760
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   5
            Top             =   960
            Width           =   735
            Begin VB.Image Image17 
               Height          =   720
               Left            =   0
               Picture         =   "frmMailList.frx":1B311
               Top             =   -40
               Width           =   720
            End
         End
         Begin VB.Label Label13 
            Caption         =   "Rupture"
            Enabled         =   0   'False
            Height          =   255
            Left            =   480
            TabIndex        =   47
            Top             =   360
            Width           =   1575
         End
         Begin VB.Label Label1 
            Height          =   615
            Left            =   1320
            TabIndex        =   17
            Top             =   990
            Width           =   1335
         End
         Begin VB.Label Label2 
            Height          =   495
            Left            =   1320
            TabIndex        =   16
            Top             =   1755
            Width           =   1335
         End
         Begin VB.Shape Sel 
            BorderColor     =   &H000000FF&
            BorderWidth     =   2
            Height          =   660
            Left            =   345
            Top             =   945
            Width           =   780
         End
         Begin VB.Label Label3 
            Height          =   615
            Left            =   3720
            TabIndex        =   15
            Top             =   990
            Width           =   1215
         End
         Begin VB.Label Label4 
            Height          =   615
            Left            =   3720
            TabIndex        =   14
            Top             =   1755
            Width           =   1215
         End
      End
   End
   Begin MSWinsockLib.Winsock Winsock2 
      Left            =   9840
      Top             =   3360
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
End
Attribute VB_Name = "frmMailList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'-> Indique le fichier en cours de traitement
Public FichierName As String
Public aSpool As Spool

Public FormatHtml As Boolean
Public FormatPDF As Boolean
Public FormatNothing As Boolean
Public FormatTurbo As Boolean
Private pFormat As String

'-> Indique la cl� du spool actuel
Public SpoolKey As String
Public strSaisie As String

Dim TempFileName As String
Dim hdlFile As Integer
Dim ReponseFaite As Boolean
Dim iStep As Integer

Dim toto As String
Dim password As String
Dim strTemp As String
Dim blnConnected As Boolean

Public Sub Init()

Dim aNode As Node
Dim aNode2 As Node
Dim aNode3 As Node
Dim aFichier As Fichier
Dim i As Long
Dim j As Long
Dim aLb As Libelle
Dim aSpool2 As Spool
Dim strTitre As String
Dim strLastTitre As String
Dim strFirstTitre As String
Dim strPage As String
Dim topOK As Boolean
Dim strFichier As String

On Error Resume Next

If Me.Combo1.ListIndex = 3 Then
    Regroup
    Exit Sub
End If

'-> Pointer sur la gestion des messages
Set aLb = Libelles("MDIMAIN")
strLastTitre = ""
j = 0

Me.TreeView1.Nodes.Clear

For Each aFichier In Fichiers
        j = j + 1
        strFichier = GetUnzipFileName(aFichier.fileName)
        '-> Pointer sur l'objet fichier
        Set aFichier = Fichiers(UCase$(strFichier))
        Set aSpool = aFichier.Spools(1)

        '-> Cr�er une icone par spool
        For Each aSpool2 In aFichier.Spools
            Set aNode2 = Me.TreeView1.Nodes.Add(, , aSpool2.Key & "-" & j, aSpool2.SpoolText)
            aNode2.Tag = "SPOOL"
            aNode2.Expanded = True
            '-> Soit la liste des erreurs
            If aSpool2.NbError = 0 Then
                '-> Afficher une icone par page
                For i = 1 To aSpool2.nbPage
                    '-> Recuperer eventuellement le titre de la feuille
                    strTitre = GetMailValue(aSpool2, Trim(Me.Text5.Text), i)
                    If strFirstTitre = "" Then strFirstTitre = strTitre
                    strPage = GetTitrePage(aSpool2, i)
                    If strPage = "" Then strPage = aLb.GetCaption(12) & i
                    If Me.Combo1.ListIndex = 1 Then strLastTitre = strLastTitre & i
                    '-> si on est bien sur une page
                    If strTitre <> "[NO]" And strTitre <> strLastTitre Then
                        strLastTitre = strTitre
                        If Trim(strLastTitre) = "" Then strLastTitre = "wxw"
                        topOK = True
                        strTitre = strTitre & "�����"
                        If InStr(1, strTitre, "@") <> 0 And InStr(1, strTitre, " ") = 0 Then
                            Set aNode3 = Me.TreeView1.Nodes.Add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, strPage & " ~~ " & Entry(1, strTitre, "�"), 1)
                        Else
                            Set aNode3 = Me.TreeView1.Nodes.Add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, strPage & " ~~ " & Entry(1, strTitre, "�"), 2)
                        End If
                        aNode3.Tag = "PAGE"
                    End If
                Next
            Else
                '-> Afficher l'icone de la page d'erreur
                Set aNode3 = Me.TreeView1.Nodes.Add(aNode2.Key, 4, aNode2.Key & "�ERROR", aLb.GetCaption(11), "Warning")
                aNode3.Tag = "ERROR"
            End If 'S'il y a des erreurs
        Next 'Pour tous les spools
Next

'-> si le spool n'a pas �t� configur� pour le mailing
If topOK = False Then
    Set aLb = Libelles("FRMMAIL")
    MsgBox aLb.GetCaption(45), vbInformation, "Mailing"
    'Unload Me
    Exit Sub
Else
    '-> OBJ:
    strTitre = strFirstTitre
    Me.Text1.Text = Entry(2, strTitre + "�����", "�")
    '-> Corps:
    strFichier = Entry(3, strTitre & "�����", "�")
    If InStr(1, strTitre, "<fic:", vbTextCompare) <> 0 Then
        strFichier = Mid(strTitre, InStr(1, strTitre, "<fic:", vbTextCompare))
        strFichier = Mid(strFichier, 1, InStr(1, strFichier, ">"))
        strFichier = Trim(Mid(strFichier, 6, Len(strFichier) - 6))
        Dim TempFile As String
        TempFile = GetTempFileNameVB("RTF")
        If TelechargeFile(TurboGraphWebFile & "/" & strFichier, TempFile) Then
             Me.RichTextBox1.LoadFile (TempFile)
        End If
    Else
        '-> Corps:
        Me.RichTextBox1.Text = Entry(3, strTitre + "�����", "�")
    End If
    '-> De:
    Me.Text3.Text = Entry(4, strTitre + "�����", "�")
    '-> CC:
    Me.Text2.Text = Entry(5, strTitre + "�����", "�")
End If

'-> on ferme le composant
Winsock1.Close

'-> on envoi directement
SendOutLook = True

End Sub

Public Sub Regroup()

Dim aNode As Node
Dim aNode2 As Node
Dim aNode3 As Node
Dim aFichier As Fichier
Dim i As Long
Dim aLb As Libelle
Dim aSpool2 As Spool
Dim strTitre As String
Dim strLastTitre As String
Dim strPage As String
Dim topOK As Boolean

On Error Resume Next

Me.TreeView1.Nodes.Clear

'-> Pointer sur la gestion des messages
Set aLb = Libelles("MDIMAIN")
strLastTitre = ""
'-> Pointer sur l'objet Fichier
Set aFichier = Fichiers(aSpool.fileName)

If Me.Combo1.ListIndex <> 3 Then
    '-> Cr�er une icone par spool
    For Each aSpool2 In aFichier.Spools
        Set aNode2 = Me.TreeView1.Nodes.Add(, , aSpool2.Key, aSpool2.SpoolText)
        aNode2.Tag = "SPOOL"
        aNode2.Expanded = True
        strLastTitre = ""
        '-> Soit la liste des erreurs
        If aSpool.NbError = 0 Then
            '-> Afficher une icone par page
            For i = 1 To aSpool.nbPage
                '-> Recuperer eventuellement le titre de la feuille
                strTitre = GetTitreMail(aSpool2, i)
                strPage = GetTitrePage(aSpool2, i)
                If strPage = "" Then strPage = aLb.GetCaption(12) & i
                If Me.Combo1.ListIndex = 0 Then strLastTitre = strLastTitre & i
                If Me.Combo1.ListIndex = 2 Then strTitre = strPage
                '-> si on est bien sur une page
                If strTitre <> "[NO]" And strTitre <> strLastTitre Then
                    strLastTitre = strTitre
                    If Trim(strLastTitre) = "" Then strLastTitre = "wxw"
                    'If Me.Combo1.ListIndex = 2 Then strLastTitre = strPage
                    strTitre = GetTitreMail(aSpool2, i)
                    topOK = True
                    strTitre = strTitre & "�����"
                    If InStr(1, strTitre, "@") <> 0 And InStr(1, strTitre, " ") = 0 Then
                        Set aNode3 = Me.TreeView1.Nodes.Add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, strPage & " ~~ " & Entry(1, strTitre, "�"), 1)
                    Else
                        Set aNode3 = Me.TreeView1.Nodes.Add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, strPage & " ~~ " & Entry(1, strTitre, "�"), 2)
                    End If
                    aNode3.Tag = "PAGE"
                End If
            Next
        Else
            '-> Afficher l'icone de la page d'erreur
            Set aNode3 = Me.TreeView1.Nodes.Add(aNode2.Key, 4, aNode2.Key & "�ERROR", aLb.GetCaption(11), "Warning")
            aNode3.Tag = "ERROR"
        End If 'S'il y a des erreurs
    Next 'Pour tous les spools
Else
    For Each aFichier In Fichiers
        '-> Cr�er une icone par spool
        Set aNode2 = Me.TreeView1.Nodes.Add(, , aFichier.fileName, aFichier.fileName & " ~~ ", 1)
        aNode2.Tag = "FICHIER"
        '-> on va chercher le mail
        For Each aSpool2 In aFichier.Spools
            strLastTitre = ""
            '-> Soit la liste des erreurs
            If aSpool.NbError = 0 Then
                '-> Afficher une icone par page
                For i = 1 To aSpool.nbPage
                    '-> Recuperer eventuellement le titre de la feuille
                    strTitre = GetTitreMail(aSpool2, i)
                    strPage = GetTitrePage(aSpool2, i)
                    If strPage = "" Then strPage = aLb.GetCaption(12) & i
                    '-> si on est bien sur une page
                    If strTitre <> "[NO]" And strTitre <> strLastTitre Then
                        strLastTitre = strTitre
                        If Trim(strLastTitre) = "" Then strLastTitre = "wxw"
                        'If Me.Combo1.ListIndex = 2 Then strLastTitre = strPage
                        strTitre = GetTitreMail(aSpool2, i)
                        topOK = True
                        strTitre = strTitre & "�����"
                        If InStr(1, strTitre, "@") <> 0 And InStr(1, strTitre, " ") = 0 Then
                            aNode2.Text = GetFileName(aFichier.fileName) & " ~~ " & Entry(1, strTitre, "�")
                            i = aSpool.nbPage
                        End If
                    End If
                Next
            End If 'S'il y a des erreurs
        Next 'Pour tous les spools
    Next
End If

End Sub

Private Sub Check3_Click()
'-> on inverse l'autre case
If Me.Check3.Value = 1 Then Me.Check4.Value = 0
If Me.Check3.Value = 0 Then Me.Check4.Value = 1
End Sub

Private Sub Check4_Click()
'-> on inverse l'autre case
If Me.Check4.Value = 1 Then Me.Check3.Value = 0
If Me.Check4.Value = 0 Then Me.Check3.Value = 1
End Sub


Private Sub Combo1_Change()
    Init
End Sub

Private Sub Combo1_Click()
    Init
End Sub

Private Sub Combo1_KeyDown(KeyCode As Integer, Shift As Integer)
    Regroup
End Sub

Private Sub Command1_Click()
'-> on donne le focus � l'onglet suivant
Me.SSTab1.Tab = 2
End Sub

Public Sub sendEmail()
'---> Envoyer un message vers Internet

Dim SpoolName As String
Dim aNode As Node
Dim PageMin As Integer
Dim PageMax As Integer
Dim strMe As String
Dim aFichier As Fichier

On Error Resume Next

'-> on initialise
strMe = Me.Caption
ListeHtmlFile = ""
Set frmLib.MAPIMessages1 = Nothing
Err.Description = ""
'on met le sablier
Screen.MousePointer = 11

'-> vider le picture de temporisation
Me.Picture1.Cls
TailleTotale = 0
'-> Calcul de la taille total
'-> on parcourt le treeview pour les mails � envoyer
For Each aNode In Me.TreeView1.Nodes
    '-> on on compte
    If aNode.Checked And aNode.Key <> "SPOOL" Then TailleTotale = TailleTotale + 1
Next

TailleLue = 0
password = ""
'-> on parcourt le treeview pour les mails � envoyer
For Each aNode In Me.TreeView1.Nodes
    '-> on verifie que l'on est bien sur un mail � envoyer
    If aNode.Checked And aNode.Tag <> "SPOOL" Then
        '-> Ne mettre � jour les variables de traitement que si on vient de la visu et pas du menu
        PageMax = 0
        '-> Cl� du spool et du fichier
        If aNode.Tag = "FICHIER" Then
            Set aFichier = Fichiers(aNode.Key)
            SpoolName = aNode.Key
        Else
            Set aFichier = Fichiers(aSpool.fileName)
            Set aSpool = aFichier.Spools(Entry(1, Entry(1, aNode.Key, "�"), "-"))
            PageMin = CInt(Entry(2, Entry(2, aNode.Key, "�"), "|"))
            PageMax = CInt(Entry(2, Entry(2, aNode.Next.Key, "�"), "|")) - 1
            If PageMax < PageMin Then PageMax = aSpool.nbPage
            '-> nom du spool � traiter
            SpoolName = aSpool.fileName
            SpoolName = CreateDetail(aSpool, -2, PageMin, PageMax)
        End If
        
        
        '-> Format de la piece jointe
        If FormatTurbo Then pFormat = "0"
        If FormatHtml = True Then pFormat = "1"
        If FormatNothing Then pFormat = "3"
        If FormatPDF = True Then pFormat = "4"
        
        '-> on envoi le mail
        Dim mesErr As String
        If Me.ImageCombo1.SelectedItem.Index = 3 Then
            If Not sendBySMTP(SpoolName, aNode.Text) Then
                'If Not (MsgBox("Erreur d'envoi. Continuer?", vbYesNo, "") = vbYes) Then
                '    Exit For
                'End If
                DoEvents
                mesErr = " Erreur d'envoi sur " & TailleLue
            Else
                mesErr = ""
            End If
        Else
            If Me.ImageCombo1.SelectedItem.Index = 2 Then
                If Not SendByOutLook(SpoolName, aNode.Text) Then Exit For
            Else
                If Not SendByClient(SpoolName, aNode.Text) Then Exit For
            End If
        End If
        '-> Dessin de la temporisation
        TailleLue = TailleLue + 1
        DrawTempo Me.Picture1
        Me.Caption = strMe & "       " & TailleLue & "/" & TailleTotale & mesErr
        Me.Refresh
        DoEvents
    End If
Next

MDIMain.StatusBar1.Panels(1).Text = "Envoi avec Succes"

'on met le sablier
Screen.MousePointer = 0
Me.SetFocus
If Err.Description <> "" Then MsgBox Err.Description, vbExclamation

'-> D�charger la feuille
Unload Me
 If mode = 10 Then
    MDIMain.Caption = "**FIN**"
    MDIMain.TerminateApp "**FIN**"
    End
End If

End Sub

Private Sub Command2_Click()
'-> envoyer les mails
sendEmail
End Sub

Private Sub Command3_Click()
'-> on donne le focus � l'onglet suivant
Me.SSTab1.Tab = 1
End Sub

Private Sub Command4_Click()

'-> on imprime les selections
Call printDestinataires

End Sub

Private Sub Command5_Click()

'--> on lance l'enregistrement des vues
iniName = "TurboMail.ini"
frmVue.Show vbModal
If strRetour <> "" Then VueSave (strRetour)

End Sub

Private Sub Command6_Click()

'--> on recharge une vue enregistree
'-> on passe le nom de la maquette pour filtrer dessus
frmVue.maquette = Fichiers(Me.FichierName).Spools(Me.SpoolKey).maquette.Nom
iniName = "TurboMail.ini"
frmVue.Show vbModal
If strRetour <> "" Then VueLoad (strRetour)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

Select Case KeyCode

Case vbKeyEscape
    'Unload Me
End Select

End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim PrinterFound As Boolean
Dim Printer As Printer

On Error Resume Next

'-> Gestion des messages
Set aLb = Libelles("FRMMAIL")

Me.Caption = aLb.GetCaption(36)
Me.SSTab1.TabCaption(0) = " " & aLb.GetCaption(39)
Me.SSTab1.TabCaption(1) = " " & aLb.GetCaption(40)
Me.SSTab1.TabCaption(2) = " " & aLb.GetCaption(37)
Me.SSTab1.TabCaption(3) = " " & aLb.GetCaption(46)

Me.Frame1.Caption = aLb.GetCaption(42)
Me.Frame2.Caption = aLb.GetCaption(41)
Me.Frame3.Caption = aLb.GetCaption(43)
Me.Frame4.Caption = aLb.GetCaption(23)
Me.Label1.Caption = aLb.GetCaption(21)
Me.Label2.Caption = aLb.GetCaption(22)
Me.Label3.Caption = aLb.GetCaption(30)
Me.Label4.Caption = aLb.GetCaption(29)
Me.Label9.Caption = aLb.GetCaption(51)
Me.Label7.Caption = aLb.GetCaption(52)
Me.Label5.Caption = aLb.GetCaption(53)
Me.Label11.Caption = aLb.GetCaption(49)
Me.Option4.Caption = aLb.GetCaption(24)
Me.Option5.Caption = aLb.GetCaption(25)
Me.Option6.Caption = aLb.GetCaption(26)
Me.Check1.Caption = aLb.GetCaption(50)
Me.Check2.Caption = aLb.GetCaption(27)
Me.Command2.Caption = aLb.GetCaption(38)
Me.Check3.Caption = aLb.GetCaption(47)
Me.Check4.Caption = aLb.GetCaption(48)
Me.Command4.Caption = aLb.GetCaption(46)
Me.Check1.Caption = Me.Check1.Caption & "  " & GetIniString("PARAM", "SMTP", TurboGraphIniFile, False)
'Me.Check5.Caption = aLb.GetCaption(63)
Me.Combo1.AddItem aLb.GetCaption(64), 0
Me.Combo1.AddItem aLb.GetCaption(63), 1
Me.Combo1.AddItem aLb.GetCaption(65), 2
Me.Combo1.AddItem aLb.GetCaption(66), 3

If Fichiers.Count > 1 Then
    Me.Combo1.ListIndex = 3
Else
    Me.Combo1.ListIndex = 0
End If
Me.ImageCombo1.comboItems.Add , , aLb.GetCaption(54)
If Not IsLotus Then
    Me.ImageCombo1.comboItems.Add , , aLb.GetCaption(55)
Else
    If IsLotus Then Me.ImageCombo1.comboItems.Add , , aLb.GetCaption(56)
End If
Me.ImageCombo1.comboItems.Add , "SMTP", aLb.GetCaption(57)
Me.ImageCombo1.SelectedItem = Me.ImageCombo1.comboItems(2)

'-> si on a param�tr� un serveur de messagerie coch� par d�faut la case
If GetIniString("PARAM", "SMTP", TurboGraphIniFile, False) <> "" Then
    Me.Check1.Value = 1
    Me.ImageCombo1.SelectedItem = Me.ImageCombo1.comboItems(3)
End If

'-> Charger les images
'Set Me.SSTab1.TabPicture(0) = Me.ImageList2.ListImages(2).Picture
'Set Me.SSTab1.TabPicture(1) = Me.ImageList2.ListImages(4).Picture
'Set Me.SSTab1.TabPicture(2) = Me.ImageList2.ListImages(3).Picture
'Set Me.SSTab1.TabPicture(3) = Me.ImageList2.ListImages(5).Picture

'-> S�lectionner le premier onglet
Me.SSTab1.Tab = 0

'-> par defaut on a le format turbo
FormatTurbo = True
FormatPDF = False
FormatHtml = False
FormatNothing = False

'-> pour la piece jointe par defaut
Select Case GetIniString("PARAM", "DEFAUTPJ", App.Path & "\Turbograph.ini", False)
    Case "HTML"
        Call PicHtml_Click
    Case "TURBO"
        Call picTurbo_Click
    Case "NONE"
        Call picNothing_Click
    Case "PDF"
        Call picPdf_Click
End Select

Me.Text5.Text = GetMail(aSpool)

'-> initialisation au dessus dans le change de la zone
'Init

End Sub

Private Sub Form_Resize()
'--> on fait un resize sur la feuille
FormResize
End Sub

Private Sub FormResize()
'--> on retaille la feuille
On Error Resume Next

Me.SSTab1.height = Me.height - 750
Me.SSTab1.width = Me.width - 410
Me.Frame1.width = Me.SSTab1.width - 450
Me.Frame2.width = Me.Frame1.width
Me.Frame3.width = Me.Frame1.width
Me.Frame5.width = Me.Frame1.width
Me.Frame1.height = Me.SSTab1.height - 820
Me.Frame2.height = Me.Frame1.height
Me.Frame3.height = Me.Frame1.height
Me.Frame5.height = Me.Frame1.height
Me.Text1.width = Me.Frame1.width - 920
Me.Text3.width = Me.Frame1.width - 920
Me.TreeView1.width = Me.Frame1.width - 450
Me.RichTextBox1.width = Me.Text1.width
Me.RichTextBox1.height = Me.Frame1.height - 2000
Me.Command3.Top = Me.RichTextBox1.height + Me.RichTextBox1.Top + 150
Me.Command3.Left = Me.RichTextBox1.Left + Me.RichTextBox1.width - Me.Command3.width
Me.Command1.Left = Me.Command3.Left
Me.Command1.Top = Me.Command3.Top
Me.Command2.Left = Me.Command3.Left
Me.Command2.Top = Me.Command3.Top
Me.Command4.Left = Me.Command3.Left
Me.Command4.Top = Me.Command3.Top
Me.Command6.Top = Me.Command3.Top
Me.Command5.Top = Me.Command3.Top
Me.Text2.width = Me.TreeView1.width - 580
Me.TreeView1.height = Me.SSTab1.height - 2700
Me.Text2.Top = Me.TreeView1.Top + Me.TreeView1.height + 150
Me.Label6.Top = Me.Text2.Top
Me.Check1.Top = Me.Text2.Top + 950
Me.Combo1.Top = Me.Text2.Top + 930
Me.Combo1.Left = Me.TreeView1.Left + 4000
Me.Picture1.Top = Me.Text2.Top + Me.Text1.height + 200
Me.Label12.Top = Me.Picture1.Top
Me.Label12.width = 2400
Me.Picture1.Left = Me.TreeView1.Left + 4000
Me.Picture1.width = Me.TreeView1.width - 4000
Me.Picture2.Top = Me.Text2.Top + Me.Text1.height + 200
Me.Picture2.Left = Me.TreeView1.Left
Me.Picture2.width = Me.TreeView1.width
Me.ImageCombo1.Top = Me.RichTextBox1.height + Me.RichTextBox1.Top + 0

End Sub

Private Sub Image12_Click()
    PicHtml_Click
End Sub

Private Sub Image16_Click()
    picPdf_Click
End Sub

Private Sub Image17_Click()
    picNothing_Click
End Sub

Private Sub Image3_Click()
    picTurbo_Click
End Sub

Private Sub PicHtml_Click()

Me.Sel.Left = Me.PicHtml.Left - 20
Me.Sel.Top = Me.PicHtml.Top - 20
FormatHtml = True
FormatPDF = False
FormatNothing = False
FormatTurbo = False

'-> Bloquer la gestion Internet
Me.Frame6.Enabled = False
Me.Label11.Enabled = False
Me.Text4.Enabled = False
Me.Text4.Text = ""
Me.Frame4.Enabled = True
Me.Option4.Enabled = True
Me.Option5.Enabled = True
Me.Option6.Enabled = True
Me.Check2.Enabled = True

End Sub

Private Sub picTurbo_Click()

Me.Sel.Left = Me.picTurbo.Left - 20
Me.Sel.Top = Me.picTurbo.Top - 20
FormatHtml = False
FormatPDF = False
FormatNothing = False
FormatTurbo = True

'-> Bloquer la gestion Internet
Me.Frame6.Enabled = False
Me.Label11.Enabled = False
Me.Text4.Enabled = False
Me.Text4.Text = ""
Me.Frame4.Enabled = False
Me.Option4.Enabled = False
Me.Option5.Enabled = False
Me.Option6.Enabled = False
Me.Check2.Enabled = True

End Sub

Private Sub picPdf_Click()

Me.Sel.Left = Me.PicPdf.Left - 20
Me.Sel.Top = Me.PicPdf.Top - 20
FormatPDF = True
FormatHtml = False
FormatNothing = False
FormatTurbo = False

'-> Bloquer la gestion Internet
Me.Frame6.Enabled = True
Me.Label11.Enabled = True
Me.Text4.Enabled = True
'Me.Text4.SetFocus
Me.Frame4.Enabled = False
Me.Option4.Enabled = False
Me.Option5.Enabled = False
Me.Option6.Enabled = False
Me.Check2.Enabled = True

End Sub

Private Sub picNothing_Click()

Me.Sel.Left = Me.PicNothing.Left - 20
Me.Sel.Top = Me.PicNothing.Top - 20
FormatNothing = True
FormatPDF = False
FormatHtml = False
FormatTurbo = False

'-> Bloquer la gestion Internet
Me.Frame6.Enabled = False
Me.Label11.Enabled = False
Me.Text4.Enabled = False
Me.Text4.Text = ""
Me.Frame4.Enabled = False
Me.Option4.Enabled = False
Me.Option5.Enabled = False
Me.Option6.Enabled = False
Me.Check2.Enabled = False
End Sub

Private Sub Text1_KeyDown(KeyCode As Integer, Shift As Integer)

If KeyCode = 13 Then Me.RichTextBox1.SetFocus

End Sub

Private Sub Text3_KeyDown(KeyCode As Integer, Shift As Integer)

If KeyCode = 13 Then Me.Text1.SetFocus

End Sub

Private Sub Text5_Change()
    On Error Resume Next
    '-> on relance le chargement
    Me.Init
    '-> on sauvegarde
    SetIniString Trim(UCase("default")), Trim(UCase(GetMaqName(aSpool))), App.Path & "\TurboMail.ini", Me.Text5.Text

End Sub

Private Sub TreeView1_AfterLabelEdit(Cancel As Integer, NewString As String)
'-> on verifie pour info la validit� de la saisie
NewString = Entry(1, strSaisie, " ~~ ") & " ~~ " & NewString
If InStr(1, NewString, "@") <> 0 Then
    Me.TreeView1.SelectedItem.Image = 1
Else
    Me.TreeView1.SelectedItem.Image = 2
End If

End Sub

Private Sub TreeView1_DblClick()
    If Me.TreeView1.SelectedItem.Tag <> "SPOOL" Then
        '-> on se met � gauche la saisie
        strSaisie = Me.TreeView1.SelectedItem.Text
        Me.TreeView1.SelectedItem.Text = Mid(Entry(2, Me.TreeView1.SelectedItem.Text, " ~~ "), 4)
        TreeView1.StartLabelEdit
        Me.TreeView1.SelectedItem.Text = strSaisie
    End If
End Sub

Private Sub TreeView1_KeyDown(KeyCode As Integer, Shift As Integer)
Dim aNode As Node

On Error Resume Next
'-> on selectionne ou deselectionne tous les elements d'un node
If KeyCode = vbKeyA And Shift = 2 Then
    For Each aNode In Me.TreeView1.Nodes
        '-> on inverse la selection
        If aNode.Tag = "SPOOL" Then
            aNode.Checked = True
            TreeView1_NodeCheck aNode
        End If
    Next
End If


End Sub

Private Sub TreeView1_KeyPress(KeyAscii As Integer)
Dim aNode As Node

'On Error Resume Next
'-> on selectionne ou deselectionne tous les elements d'un node
'If KeyAscii = 13 Then
'    If Me.TreeView1.SelectedItem.Tag <> "LIG" Then Exit Sub
'    For Each aNode In Me.TreeView1.Nodes
'        '-> on inverse la selection
'        If aNode.Tag = "LIG" Then
'            If aNode.Parent.Text = Me.TreeView1.SelectedItem.Parent.Text Then
'                TreeView1_NodeClick aNode
'            End If
'        End If
'    Next
'End If

End Sub

Private Sub TreeView1_NodeCheck(ByVal Node As MSComctlLib.Node)
Dim aNode As Node

On Error Resume Next
'-> on selectionne ou deselectionne tous les elements d'un node
Set Me.TreeView1.SelectedItem = Node
If Me.TreeView1.SelectedItem.Tag <> "SPOOL" Then Exit Sub
For Each aNode In Me.TreeView1.Nodes
    '-> on inverse la selection
    If aNode.Tag = "PAGE" Then
        If aNode.Parent.Text = Me.TreeView1.SelectedItem.Text Then
            If aNode.Image = 1 Then aNode.Checked = Node.Checked
        End If
    End If
Next

End Sub

Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)

'---> Bloquer ou d�bloquer la gestion des prori�t�s

Dim aBlock As Block


End Sub

Public Function SendByOutLook(ByVal FileToSend As String, sAdrMail As String) As Boolean

'---> Cette proc�dure ouvre OutLook, cr�er un nouveau message , et joint le fichier Sp�cifi�

Dim aOutLook As Object
Dim aMail As Object
Dim ErrorCode As Integer
Dim i As Integer
Dim CurrentPIDProcess As Long
Dim sNow As String

On Error GoTo GestError

'-> V�rifier que le fichier existe
If Dir$(FileToSend, vbNormal) = "" Then
    ErrorCode = 3
    GoTo GestError
End If

sNow = GetTickCount

'-> on regarde si il n'y a pas une feuille outlook qui generee
If Not TestOutlookMessage("-- " & Me.Text1.Text & " --(" & sNow & ")") Then
    SendByOutLook = False
    Exit Function
End If

'-> Obtenir un pointeur vers OutLook
ErrorCode = 1
Set aOutLook = StartOutlook()

'-> Cr�er un nouveau message
ErrorCode = 2
Set aMail = aOutLook.createitem(0)

'-> Afficher le corps du message
'-> Envoie depuis la page de visu
If frmMailList.RichTextBox1.Text <> "" Then aMail.body = frmMailList.RichTextBox1.Text
'-> Setting des param�tres
aMail.To = sAdrMail
aMail.CC = Me.Text2.Text 'personnes en pi�ces jointes
aMail.Subject = "-- " & Me.Text1.Text & " --(" & sNow & ")" 'sujet du mail

'-> Attacher le fichier sp�cifi�
ErrorCode = 3
Select Case pFormat
    Case "0" 'turbo
        'on attache le spool turbo
        aMail.Attachments.Add FileToSend
    Case "1" 'html
        '-> on transforme le spool en html
        '-> Inclure le num�ro de page
        IncluseSpoolNumber = CBool(Me.Check2.Value)
        '-> Positionnement de la navigationHTML
        If Me.Option4.Value Then
            NavigationType = 0 'FrameSet
        ElseIf Me.Option5.Value Then
            NavigationType = 1 'Fichier unique
        Else
            NavigationType = 2 'Pages
        End If
        PathToExport = GetTempFileNameVB("WWW", True)
        FileKeyMail = Trim(UCase$(FileToSend))
        '-> Nom du Fichier
        FileToExport = LCase$(GetSpoolName(FileKeyMail)) & ".html"
        '-> Cl� du fichiers
        FileKeyExport = FileKeyMail
        '-> Spool
        SpoolKeyMail = aSpool.Key
        SpoolKeyExport = SpoolKeyMail
        '-> Num�ro de page
        PageToExport = PageNumMail
        ExportType = 1
            
        '-> On ajoute le spool
        AnalyseFileToPrint FileToSend

        CreateEditionHTML
        '-> Attacher le fichier sp�cifi�
        If ListeHtmlFile <> "" Then
            For i = 1 To NumEntries(ListeHtmlFile, "|")
                aMail.Attachments.Add Entry(i, ListeHtmlFile, "|")
            Next
        End If
        '-> Ajouter les images s'il y en a
        For i = 1 To NbImages
            aMail.Attachments.Add ListeHTMLImages(i)
        Next
    Case "3" 'nothing
    
    Case "4" 'pdf
        '-> on g�n�re le fichier avec le turboPDF
        If FilePath = "" Then FilePath = OpenPath
        Dim aSpool2 As Spool
        AnalyseFileToPrint FileToSend
        Set aSpool2 = Fichiers.Item(Trim(FileToSend)).Spools(1)
        If GetTitrePdf(aSpool, 1) <> "[NO]" Then
            Dim pdfDir As String
            Dim pdfName As String
            pdfName = GetFileName(GetTitrePdf(aSpool2, 1))
            pdfDir = Replace(GetTitrePdf(aSpool2, 1), pdfName, "")
            Trace "sendByOutlook 1:" & App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & FileToSend & "�" & Replace(Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare), "�pdfDirectory=", "�NoPdfDirectory=", , , vbTextCompare) & "�pdfDirectory=" & pdfDir & "�pdfName=" & pdfName & Chr(34)
            CurrentPIDProcess = Shell(App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & FileToSend & "�" & Replace(Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare), "�pdfDirectory=", "�NoPdfDirectory=", , , vbTextCompare) & "�pdfDirectory=" & pdfDir & "�pdfName=" & pdfName & Chr(34))
        Else
            CurrentPIDProcess = Shell(App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & FileToSend & "�" & Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare) & "�pdfDirectory=" & Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1) & Chr(34))
            Trace "sendByOutlook 2:" & App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & FileToSend & "�" & Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare) & "�pdfDirectory=" & Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1) & Chr(34)
        End If
        '-> Boucler tant que le processus est actif
        Do While IsPidRunning(CurrentPIDProcess)
            '-> Lib�ration de la pile des messages
            DoEvents
            Sleep 200
        Loop
        'on attache le spool turbo
        If GetTitrePdf(aSpool2, 1) <> "[NO]" Then
            aMail.Attachments.Add (GetTitrePdf(aSpool2, 1))
        Else
            aMail.Attachments.Add (Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1) & "/" & GetSpoolName(FileToSend) & ".pdf")
        End If
End Select


'-> si il y a des fichiers joints lorsque l on vient d'un fichier zipp�
For i = 0 To MDIMain.mnuJoin.Count - 1
    If MDIMain.mnuJoin.Item(i).Visible Then
        If Trim(MDIMain.mnuJoin.Item(i).Tag) <> "" Then aMail.Attachments.Add MDIMain.mnuJoin.Item(i).Tag
    End If
Next
    
'-> Afficher le mail
ErrorCode = 4
'If Dir(App.Path & "\" & "ClickYes.exe") <> "" Then Shell App.Path & "\" & "ClickYes.exe", vbNormalFocus
aMail.Send

'aMail.Display
DoEvents
'If Not SendOutlookMessage(aMail.Subject) Then GoTo GestError

'Me.SetFocus
SendByOutLook = True
'-> Lib�rer les pointeurs
Set aMail = Nothing
Set aOutLook = Nothing

Exit Function
GestError:

'-> Lib�rer les pointeurs
Set aMail = Nothing
Set aOutLook = Nothing

If MsgBox("Une erreur est survenue sur le(s) destinataire(s) suivant(s) " & sAdrMail & Chr(13) & "Continuer?", vbOKCancel, "Mailing") = vbOK Then
    SendByOutLook = True
End If

End Function

Public Function SendByClient(ByVal FileToSend As String, sAdrMail As String) As Boolean

'---> Cette proc�dure ouvre OutLook, cr�er un nouveau message , et joint le fichier Sp�cifi�

Dim aOutLook As Object
Dim aMail As Object
Dim ErrorCode As Integer
Dim i As Integer
Dim CurrentPIDProcess As Long
Dim sNow As String

On Error GoTo GestError

'-> V�rifier que le fichier existe
If Dir$(FileToSend, vbNormal) = "" Then
    ErrorCode = 3
    GoTo GestError
End If

sNow = GetTickCount
ListeHtmlFile = ""

'-> Intialiser la session MAPI
If Not InitMAPISession Then Exit Function

'-> V�rifier la pr�sence du fichier � linker
If Dir$(FileToSend, vbNormal) = "" Then Exit Function

'-> Cr�er un nouveau message
With frmLib.MAPIMessages1
        .MsgIndex = -1
        .AddressEditFieldCount = 2
        .Compose
        .RecipIndex = 0
        .RecipAddress = Trim(Mid(Entry(2, sAdrMail, "~~"), 2))
        .ResolveName
        If Me.Text2.Text <> "" Then
            .RecipIndex = 1
            .RecipAddress = Me.Text2.Text
            .ResolveName
        End If
        .MsgSubject = Me.Text1.Text  'sujet du mail
End With
        
'-> Setting du body
frmLib.MAPIMessages1.MsgNoteText = frmMailList.RichTextBox1.Text
    
'-> on laisse la place pour inserer les pieces jointes
frmLib.MAPIMessages1.MsgNoteText = Space(20) & vbCrLf & frmLib.MAPIMessages1.MsgNoteText
        
'-> Attacher le fichier sp�cifi�
ErrorCode = 3
Select Case pFormat
    Case "0" 'turbo
        'on attache le spool turbo
        With frmLib.MAPIMessages1
            .AttachmentIndex = frmLib.MAPIMessages1.AttachmentCount
            .AttachmentPosition = frmLib.MAPIMessages1.AttachmentCount
            .AttachmentPathName = FileToSend
            .AttachmentType = 0
        End With
    Case "1" 'html
        '-> on transforme le spool en html
        '-> Inclure le num�ro de page
        IncluseSpoolNumber = CBool(Me.Check2.Value)
        '-> Positionnement de la navigationHTML
        If Me.Option4.Value Then
            NavigationType = 0 'FrameSet
        ElseIf Me.Option5.Value Then
            NavigationType = 1 'Fichier unique
        Else
            NavigationType = 2 'Pages
        End If
        PathToExport = GetTempFileNameVB("WWW", True)
        FileKeyMail = Trim(UCase$(FileToSend))
        '-> Nom du Fichier
        FileToExport = LCase$(GetSpoolName(FileKeyMail)) & ".html"
        '-> Cl� du fichiers
        FileKeyExport = FileKeyMail
        '-> Spool
        SpoolKeyMail = aSpool.Key
        SpoolKeyExport = SpoolKeyMail
        '-> Num�ro de page
        PageToExport = PageNumMail
        ExportType = 1
            
        '-> On ajoute le spool
        AnalyseFileToPrint FileToSend

        CreateEditionHTML
        '-> Attacher le fichier sp�cifi�
        If ListeHtmlFile <> "" Then
            For i = 1 To NumEntries(ListeHtmlFile, "|")
                With frmLib.MAPIMessages1
                    .AttachmentIndex = frmLib.MAPIMessages1.AttachmentCount
                    .AttachmentPosition = frmLib.MAPIMessages1.AttachmentCount
                    .AttachmentPathName = Entry(i, ListeHtmlFile, "|")
                    .AttachmentType = 0
                End With
            Next
        End If
        '-> Ajouter les images s'il y en a
        For i = 1 To NbImages
            With frmLib.MAPIMessages1
                .AttachmentIndex = frmLib.MAPIMessages1.AttachmentCount
                .AttachmentPosition = frmLib.MAPIMessages1.AttachmentCount
                .AttachmentPathName = ListeHTMLImages(i)
                .AttachmentType = 0
            End With
        Next
    Case "3" 'nothing
    
    Case "4" 'pdf
        '-> on g�n�re le fichier avec le turboPDF
        If FilePath = "" Then FilePath = OpenPath
        Dim aSpool2 As Spool
        AnalyseFileToPrint FileToSend
        Set aSpool2 = Fichiers.Item(Trim(FileToSend)).Spools(1)
        If GetTitrePdf(aSpool, 1) <> "[NO]" Then
            Dim pdfDir As String
            Dim pdfName As String
            pdfName = GetFileName(GetTitrePdf(aSpool2, 1))
            pdfDir = Replace(GetTitrePdf(aSpool2, 1), pdfName, "")
            Trace App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & FileToSend & Replace(Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare), "�pdfDirectory=", "�NoPdfDirectory=", , , vbTextCompare) & "�pdfDirectory=" & pdfDir & "�pdfName=" & pdfName & Chr(34)
            CurrentPIDProcess = Shell(App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & FileToSend & Replace(Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare), "�pdfDirectory=", "�NoPdfDirectory=", , , vbTextCompare) & "�pdfDirectory=" & pdfDir & "�pdfName=" & pdfName & Chr(34))
        Else
            CurrentPIDProcess = Shell(App.Path & "\TurboPdf.exe " & "�fileToConvert=" & FileToSend & Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare) & "�pdfDirectory=" & Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1))
        End If
        '-> Boucler tant que le processus est actif
        Do While IsPidRunning(CurrentPIDProcess)
            '-> Lib�ration de la pile des messages
            DoEvents
            Sleep 200
        Loop
        'on attache le spool turbo
        If GetTitrePdf(aSpool2, 1) <> "[NO]" Then
            With frmLib.MAPIMessages1
                .AttachmentIndex = frmLib.MAPIMessages1.AttachmentCount
                .AttachmentPosition = frmLib.MAPIMessages1.AttachmentCount
                .AttachmentPathName = GetTitrePdf(aSpool2, 1)
                .AttachmentType = 0
            End With
        Else
            With frmLib.MAPIMessages1
                .AttachmentIndex = frmLib.MAPIMessages1.AttachmentCount
                .AttachmentPosition = frmLib.MAPIMessages1.AttachmentCount
                .AttachmentPathName = (Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1) & "/" & GetSpoolName(FileToSend) & ".pdf")
                .AttachmentType = 0
            End With
        End If
End Select


'-> si il y a des fichiers joints lorsque l on vient d'un fichier zipp�
For i = 0 To MDIMain.mnuJoin.Count - 1
    If MDIMain.mnuJoin.Item(i).Visible Then
        If Trim(MDIMain.mnuJoin.Item(i).Tag) <> "" Then
            With frmLib.MAPIMessages1
                .AttachmentIndex = frmLib.MAPIMessages1.AttachmentCount
                .AttachmentPosition = frmLib.MAPIMessages1.AttachmentCount
                .AttachmentPathName = MDIMain.mnuJoin.Item(i).Tag
                .AttachmentType = 0
            End With
        End If
    End If
Next

'-> Afficher le mail
ErrorCode = 4
    
frmLib.MAPIMessages1.Send False

DoEvents

SendByClient = True

Exit Function
GestError:


If MsgBox("Une erreur est survenue sur le(s) destinataire(s) suivant(s) " & sAdrMail & Chr(13) & "Continuer?", vbOKCancel, "Mailing") = vbOK Then
    SendByClient = True
End If

End Function

Private Function sendBySMTP(ByVal FileToSend As String, sAdrMail As String) As Boolean
'--> cette fonction nous permet d'envoyer un mail
'--> par le composant winsock
Dim i As Integer
Dim j As Long
Dim m_strEncodedFiles As String
Dim l_destinataire
Dim destinataire
Dim sPieceJointe As String
Dim CurrentPIDProcess As Long
Dim sSMTP As String
Dim pdfName As String
Dim pdffilePath As String
Dim pdffileName As String

On Error Resume Next

'-> on se connecte au serveur
Label12.Caption = "Initialize"
DoEvents

If GetIniString("SITE", "PHPMAIL", TurboGraphIniFile, False) <> "" Then
    sendBySMTP = sendByPHPMail(FileToSend, sAdrMail)
    DoEvents
    Exit Function
End If

If CInt(val(GetIniString("PARAM", "SMTPPORT", TurboGraphIniFile, False))) = 0 Or CInt(val(GetIniString("PARAM", "SMTPPORT", TurboGraphIniFile, False))) = 25 Then

Else
    sendBySMTP = sendBySMTP_POWERSHELL(FileToSend, sAdrMail)
    Exit Function
End If

If GetIniString("PARAM", "MAILMESSAGECMDLET", TurboGraphIniFile, False) = "1" Then
    sendBySMTP = sendBySMTP_POWERSHELL(FileToSend, sAdrMail)
    Exit Function
End If

If Winsock1.State <> sckConnected Then

    Winsock1.Close
    
    Me.Label12.Caption = "DisConnect..."
    iStep = 1
    If GetIniString("PARAM", "SMTP", TurboGraphIniFile, False) <> "" Then
        sSMTP = GetIniString("PARAM", "SMTP", TurboGraphIniFile, False)
    Else
        MsgBox "Veuillez param�trer l'adresse de votre serveur de messagerie SMTP", vbExclamation, ""
    End If
    ReponseFaite = False
    Winsock1.Connect Trim(sSMTP), 25
    
    '-> on met une attente pour les antispam (en principe pas d'attente)
    Sleep val(GetIniString("PARAM", "SMTPDELAI", App.Path & "\Turbograph.ini", False))
    Me.Label12.Caption = "Connect..."
    While ReponseFaite = False
        DoEvents
    Wend
    
    While Winsock1.State <> sckConnected
        DoEvents
    Wend
    
    Me.Label12.Caption = "Mail to : " & Entry(2, sAdrMail, "~~")
    iStep = 2
    ReponseFaite = False
    Winsock1.SendData "HELO " & Trim(sSMTP) & vbCrLf
    While ReponseFaite = False
        DoEvents
    Wend

End If
iStep = 3
ReponseFaite = False
Winsock1.SendData "MAIL FROM: <" & Replace(Trim(Me.Text3.Text), " ", "") & ">" & vbCrLf
While ReponseFaite = False
    DoEvents
Wend

'-> on envoie les dif�rents destinataires (s�parateur ';' dans la saisie)
iStep = 4
'-> dans le cas ou on a une copi
If Trim(Me.Text2.Text) <> "" Then
    l_destinataire = Split(Entry(3, sAdrMail & ";" & Trim(Me.Text2.Text), "~"), ";")
Else
    l_destinataire = Split(Entry(3, sAdrMail, "~"), ";")
End If

For Each destinataire In l_destinataire
    ReponseFaite = False
    Winsock1.SendData "RCPT TO: <" & Trim(destinataire) & ">" & vbCrLf
    While ReponseFaite = False
        DoEvents
    Wend
Next

'-> on change d'etape
iStep = 5
ReponseFaite = False
Winsock1.SendData "DATA" & vbCrLf
While ReponseFaite = False
    DoEvents
Wend

ReponseFaite = False
'-> Attacher le fichier sp�cifi�
Select Case pFormat
    Case "0" 'turbo
        'on attache le spool turbo
        sPieceJointe = FileToSend
    Case "1" 'html
        '-> on transforme le spool en html
        '-> Inclure le num�ro de page
        IncluseSpoolNumber = CBool(Me.Check2.Value)
        '-> Positionnement de la navigationHTML
        If Me.Option4.Value Then
            NavigationType = 0 'FrameSet
        ElseIf Me.Option5.Value Then
            NavigationType = 1 'Fichier unique
        Else
            NavigationType = 2 'Pages
        End If
        PathToExport = GetTempFileNameVB("WWW", True)
        FileKeyMail = Trim(UCase$(FileToSend))
        '-> Nom du Fichier
        FileToExport = LCase$(GetSpoolName(FileKeyMail)) & ".html"
        '-> Cl� du fichiers
        FileKeyExport = FileKeyMail
        '-> Spool
        SpoolKeyMail = aSpool.Key
        SpoolKeyExport = SpoolKeyMail
        '-> Num�ro de page
        PageToExport = PageNumMail
        ExportType = 1
            
        '-> On ajoute le spool
        AnalyseFileToPrint FileToSend

        CreateEditionHTML
        '-> Attacher le fichier sp�cifi�
        If ListeHtmlFile <> "" Then
            sPieceJointe = ListeHtmlFile
        End If
        '-> Ajouter les images s'il y en a
        For i = 1 To NbImages
            sPieceJointe = sPieceJointe & "|" & ListeHTMLImages(i)
        Next
    Case "3" 'nothing
    
    Case "4" 'pdf
        '-> on g�n�re le fichier avec le turboPDF
        If FilePath = "" Then FilePath = OpenPath
        '-> on a specifi� eventuellement un nom pour le pdf
        Dim aSpool2 As Spool
        AnalyseFileToPrint FileToSend
        Set aSpool2 = Fichiers.Item(Trim(FileToSend)).Spools(1)
        '-> par defaut
        pdfName = GetSpoolName(FileToSend) & GetTickCount & ".pdf"
        If GetTitrePdf(aSpool, 1) <> "[NO]" Then
            pdfName = GetTitrePdf(aSpool2, 1)
        End If
        If Trim(Me.Text4.Text) <> "" Then
            pdfName = "�pdfName=" & Me.Text4.Text
            pdfName = Replace(pdfName, ".pdf", "", , , vbTextCompare)
            pdfName = pdfName & GetTickCount
            pdfName = pdfName & ".pdf"
        End If
        Trace "Avant le TurboPDF ligne de commande ci-dessous"
        Label12.Caption = "Create PDF..."
        If GetTitrePdf(aSpool2, 1) <> "[NO]" Then
                Dim pdfDir As String
                pdfName = GetFileName(GetTitrePdf(aSpool2, 1))
                pdfDir = Replace(GetTitrePdf(aSpool2, 1), pdfName, "")
                If pdfDir = "" Then pdfDir = OpenPath
                pdffilePath = pdfDir
                pdffileName = GetFileName(Replace(pdfName, "�pdfName=", ""))
        Else
            If TurbosavePath <> "" Then
                If FilePath = "" Then FilePath = Mid(OpenPath, 1, InStrRev(Replace(OpenPath, "\", "/"), "/") - 1)
                Trace App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & FileToSend & Replace(Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare), "�pdfDirectory=", "�NoPdfDirectory=", , , vbTextCompare) & "�pdfDirectory=" & TurbosavePath & pdfName & Chr(34)
                pdffilePath = FilePath
                pdffileName = GetFileName(Replace(pdfName, "�pdfName=", ""))
                CurrentPIDProcess = Shell(Chr(34) & App.Path & "\TurboPdf.exe" & Chr(34) & " " & Chr(34) & "�fileToConvert=" & FileToSend & Replace(Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare), "�pdfDirectory=", "�NoPdfDirectory=", , , vbTextCompare) & "�pdfDirectory=" & pdffilePath & "�pdfName=" & pdffileName & Chr(34))
                '-> Boucler tant que le processus est actif
                Do While IsPidRunning(CurrentPIDProcess)
                    '-> Lib�ration de la pile des messages
                    DoEvents
                    Sleep 300
                Loop
                pdffilePath = TurbosavePath
            Else
                pdffileName = GetFileName(Replace(pdfName, "�pdfName=", ""))
                pdffilePath = Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1)
            End If
        End If
        
        Trace App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & FileToSend & Replace(Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare), "�pdfDirectory=", "�NoPdfDirectory=", , , vbTextCompare) & "�pdfDirectory=" & pdffilePath & "�pdfName=" & pdffileName & Chr(34)
        CurrentPIDProcess = Shell(Chr(34) & App.Path & "\TurboPdf.exe" & Chr(34) & " " & Chr(34) & "�fileToConvert=" & FileToSend & Replace(Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare), "�pdfDirectory=", "�NoPdfDirectory=", , , vbTextCompare) & "�pdfDirectory=" & pdffilePath & "�pdfName=" & pdffileName & Chr(34))
        '-> Boucler tant que le processus est actif
        Do While IsPidRunning(CurrentPIDProcess)
            '-> Lib�ration de la pile des messages
            DoEvents
            Sleep 300
        Loop
        Trace "Apr�s le turbopdf"
        'on attache le spool turbo
        sPieceJointe = Mid(pdffilePath, 1, InStrRev(Replace(pdffilePath, "\", "/"), "/") - 1) & "/" & pdffileName
        sPieceJointe = Replace(sPieceJointe, "/", "\")
        
End Select

'-> on met en forme la ou les pi�ces jointes
'For i = 1 To NumEntries(sPieceJointe, "|")
'    m_strEncodedFiles = m_strEncodedFiles & UUEncodeFile(Entry(i, sPieceJointe, "|")) & vbCrLf
'Next i
'-> on regarde si on a des pieces jointes
If Fichiers(aSpool.fileName).filesJoins <> "" Then
    If sPieceJointe <> "" Then
        sPieceJointe = sPieceJointe & "|" & Replace(Fichiers(aSpool.fileName).filesJoins, ",", "|")
    Else
        sPieceJointe = Replace(Fichiers(aSpool.fileName).filesJoins, ",", "|")
    End If
End If

iStep = 5
'on pr�pare l'ent�te du message
Winsock1.SendData "To:" & Entry(3, sAdrMail, "~") & vbCrLf
DoEvents
Winsock1.SendData "From:" & Me.Text3.Text & vbCrLf
DoEvents
Winsock1.SendData "Subject:" & Me.Text1.Text & vbCrLf
DoEvents

'on envoie le message et la ou les pi�ces jointes si il y en a

Label12.Caption = "Send Mail..."
' D�finit le mail comme comportant plusieurs objets
Winsock1.SendData "MIME-Version: 1.0" & vbCrLf
DoEvents
Winsock1.SendData "Content-Type: multipart/mixed; "
DoEvents
Winsock1.SendData "boundary=" & Chr(34) & "NextPart" & Chr(34) & vbCrLf & vbCrLf
DoEvents

' Partie texte plein
Winsock1.SendData "--NextPart" & vbCrLf
DoEvents
Winsock1.SendData "Content-Type: text/plain; charset=" & Chr(34) & "ISO-8859-1" & Chr(34) & vbCrLf
DoEvents
Winsock1.SendData "Content-Transfer-Encoding: 8bit" & vbCrLf & vbCrLf
' le corps du message
Winsock1.SendData Me.RichTextBox1.Text & vbCrLf

For i = 1 To NumEntries(sPieceJointe, "|")
    ' d�finition et envoi du fichier joint
    Winsock1.SendData "--NextPart" & vbCrLf
    Winsock1.SendData "Content-Type:application/octet-stream;name=" & Chr(34) & GetFileName(Entry(i, sPieceJointe, "|")) & Chr(34) & vbCrLf
    Winsock1.SendData "Content-Transfer-Encoding: base64" & vbCrLf
    Winsock1.SendData "Content-Disposition: attachment; filename=" & Chr(34) & GetFileName(Entry(i, sPieceJointe, "|")) & Chr(34) & vbCrLf & vbCrLf
'    Winsock1.SendData "Content-Length: " & Len(Encode64(OpenFileAPI(Entry(i, sPieceJointe, "|")))) & vbCrLf
    ' Envoi le fichier cod� en base64
    Winsock1.SendData Encode64(OpenFileAPI(Entry(i, sPieceJointe, "|"))) & vbCrLf '& vbCrLf
Next
    ' Ferme les corps multiples
    Winsock1.SendData "--NextPart" & "--" & vbCrLf

'on envoie un point pour signaler au serveur que le msg est fini
iStep = 7
DoEvents
ReponseFaite = False
Winsock1.SendData vbCrLf & "." & vbCrLf '& vbCrLf & "QUIT" & vbCrLf

While ReponseFaite = False
    DoEvents
Wend

Label12.Caption = "End..."

sendBySMTP = True
End Function

Private Function sendBySMTP_POWERSHELL(ByVal FileToSend As String, sAdrMail As String) As Boolean
'--> cette fonction nous permet d'envoyer un mail
'--> par le composant winsock
Dim i As Integer
Dim j As Long
Dim m_strEncodedFiles As String
Dim l_destinataire
Dim destinataire
Dim sPieceJointe As String
Dim CurrentPIDProcess As Long
Dim sSMTP As String
Dim sPort As Integer

'On Error Resume Next

Me.MousePointer = 11

'-> on se connecte au serveur
'Winsock1.Close
DoEvents
Sleep val(GetIniString("PARAM", "SMTPDELAI", App.Path & "\Turbograph.ini", False))

iStep = 1
If GetIniString("PARAM", "SMTP", TurboGraphIniFile, False) <> "" Then
    sSMTP = GetIniString("PARAM", "SMTP", TurboGraphIniFile, False)
Else
    MsgBox "Veuillez param�trer l'adresse de votre serveur de messagerie SMTP", vbExclamation, ""
End If

sPort = CInt(val(GetIniString("PARAM", "SMTPPORT", TurboGraphIniFile, False)))

If Me.Text4.Text = "" Then Me.Text4.Text = "Unknown"
'Winsock1.SendData "MAIL FROM: <" & Replace(Trim(Me.Text4.Text), " ", "") & ">" & vbCrLf

'-> on envoie les dif�rents destinataires (s�parateur ';' dans la saisie)
iStep = 3
'-> dans le cas ou on a une copie cach�e
If Trim(Me.Text4.Text) <> "" Then
    l_destinataire = Split(sAdrMail & ";" & Trim(Me.Text4.Text), ";")
Else
    l_destinataire = Split(sAdrMail, ";")
End If

'-> on s'occupe ici du fichier a envoyer
If Me.Check1.Value = 1 Then
    IsCryptedFile = True
Else
    IsCryptedFile = False
End If

ReponseFaite = False
'-> Format de la piece jointe
If FormatTurbo Then pFormat = "0"
If FormatHtml = True Then pFormat = "1"
If FormatNothing Then pFormat = "3"
If FormatPDF = True Then pFormat = "4"

'-> Attacher le fichier sp�cifi�
Select Case pFormat
    Case "0" 'turbo
        'on attache le spool turbo
        sPieceJointe = FileToSend
    Case "1" 'html
        '-> on transforme le spool en html
        '-> Inclure le num�ro de page
        IncluseSpoolNumber = CBool(Me.Check2.Value)
        '-> Positionnement de la navigationHTML
        If Me.Option4.Value Then
            NavigationType = 0 'FrameSet
        ElseIf Me.Option5.Value Then
            NavigationType = 1 'Fichier unique
        Else
            NavigationType = 2 'Pages
        End If
        PathToExport = GetTempFileNameVB("WWW", True)
        FileKeyMail = Trim(UCase$(FileToSend))
        '-> Nom du Fichier
        FileToExport = LCase$(GetSpoolName(FileKeyMail)) & ".html"
        '-> Cl� du fichiers
        FileKeyExport = FileKeyMail
        '-> Spool
        SpoolKeyMail = aSpool.Key
        SpoolKeyExport = SpoolKeyMail
        '-> Num�ro de page
        PageToExport = PageNumMail
        ExportType = 1
            
        '-> On ajoute le spool
        FileToSend = AnalyseFileToPrint(FileToSend)

        CreateEditionHTML
        '-> Attacher le fichier sp�cifi�
        If ListeHtmlFile <> "" Then
            sPieceJointe = ListeHtmlFile
        End If
        '-> Ajouter les images s'il y en a
        For i = 1 To NbImages
            sPieceJointe = sPieceJointe & "|" & ListeHTMLImages(i)
        Next
    Case "3" 'nothing
    
    Case "4" 'pdf
        '-> on g�n�re le fichier avec le turboPDF
        If FilePath = "" Then FilePath = OpenPath
        CurrentPIDProcess = Shell(Chr(34) & App.Path & "\TurboPdf.exe" & Chr(34) & " " & Chr(34) & "�fileToConvert=" & FileToSend & Replace(Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare), "�pdfDirectory=", "�pdfDirectoryNo=", , , vbTextCompare) & "�pdfDirectory=" & Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1) & Chr(34))
        '-> Boucler tant que le processus est actif
        Do While IsPidRunning(CurrentPIDProcess)
            '-> Lib�ration de la pile des messages
            DoEvents
            Sleep 300
        Loop
        'on attache le spool turbo
        sPieceJointe = (Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1) & "/" & GetSpoolName(FileToSend) & ".pdf")
End Select

'-> on regarde si on a des pieces jointes
If Fichiers(aSpool.fileName).filesJoins <> "" Then
    sPieceJointe = sPieceJointe & "|" & Replace(Fichiers(aSpool.fileName).filesJoins, ",", "|")
End If

Dim TempFile As String
Dim hdlFile As Long
DoEvents
TempFile = "SCRIPT-" & Year(Now) & "-" & FORMAT(Month(Now), "00") & "-" & FORMAT(Day(Now), "00") & "-" & FORMAT(Hour(Now), "00") & "-" & FORMAT(Minute(Now), "00") & "-" & FORMAT(Second(Now), "00") & Right(CStr(GetTickCount), 2) & ".ps1"
TempFile = GetTempFileNameVB("WWW", True) & TempFile

'-> Obtenir un handle de fichier
hdlFile = FreeFile

'-> Ouverture du fichier ascii et �criture des pages
Open TempFile For Output As #hdlFile

Print #hdlFile, "$KeyFile = '" & GetTempFileNameVB("WWW", True) & "AES.key'"
Print #hdlFile, "$Key = New-Object Byte[] 16   # You can use 16, 24, or 32 for AES"
Print #hdlFile, "[Security.Cryptography.RNGCryptoServiceProvider]::Create().GetBytes($Key)"
Print #hdlFile, "$Key | out-file $KeyFile"
Print #hdlFile, ""
Print #hdlFile, "$PasswordFile = '" & GetTempFileNameVB("WWW", True) & "Password.txt'"
Print #hdlFile, "$KeyFile = '" & GetTempFileNameVB("WWW", True) & "AES.key'"
Print #hdlFile, "$Key = Get-Content $KeyFile"

'-> on demande le mot de passe
Dim sReturnVal As String

If GetIniString("PARAM", "SMTPCREDENTIALS", TurboGraphIniFile, False) <> "1" Then
    If sPort <> 0 Then
        If password = "" Then
            sReturnVal = InputBoxEx("Enter Password", "Secure Box", "password", , , , , vbBlack, vbWhite, "Courier New", 10, "*")
            password = sReturnVal
        Else
            sReturnVal = password
        End If
    End If
End If

Print #hdlFile, "$Password = '" & sReturnVal & "' | ConvertTo-SecureString -AsPlainText -Force"
Print #hdlFile, "$Password | ConvertFrom-SecureString -key $Key | Out-File $PasswordFile"
Print #hdlFile, ""
Print #hdlFile, "$SMTPServer = '" & sSMTP & "'"
Print #hdlFile, "$SMTPPort = '" & sPort & "'"
If Me.Text3.Text = "" Then Me.Text3.Text = "Unknown"
Print #hdlFile, "$Username = '" & Trim(Me.Text3.Text) & "'"
Print #hdlFile, "$PasswordFile = '" & GetTempFileNameVB("WWW", True) & "Password.txt'"
Print #hdlFile, "$KeyFile = '" & GetTempFileNameVB("WWW", True) & "AES.key'"
Print #hdlFile, "$key = Get-Content $KeyFile"
Print #hdlFile, "$Password = (Get-Content $PasswordFile | ConvertTo-SecureString -Key $key)"
Print #hdlFile, ""
If Me.Text2.Text <> "" Then Print #hdlFile, "$cc = '" & Me.Text2.Text & "'"
Print #hdlFile, "$subject = '" & Replace(Me.Text1.Text, "'", "''") & "'"
Print #hdlFile, "$body = @'"
Print #hdlFile, Replace(Me.RichTextBox1.Text, "'", "''")
Print #hdlFile, "'@"
Print #hdlFile, ""
Print #hdlFile, "$message = New-Object System.Net.Mail.MailMessage"
Print #hdlFile, "$message.subject = $subject"
Print #hdlFile, "$message.body = $body"
For i = 1 To NumEntries(sAdrMail, ";")
    Print #hdlFile, "$to = '" & Entry(i, Entry(3, sAdrMail, "~"), ";") & "'"
    Print #hdlFile, "$message.to.add($to)"
Next i
If Me.Text2.Text <> "" Then Print #hdlFile, "$message.cc.add($cc)"
Print #hdlFile, "$message.from = $username"
For i = 1 To NumEntries(sPieceJointe, "|")
    If Trim(Entry(i, sPieceJointe, "|")) <> "" Then
        Print #hdlFile, "$attachment = '" & Entry(i, sPieceJointe, "|") & "'"
        Print #hdlFile, "$message.Attachments.Add( $attachment )"
    End If
Next i
Print #hdlFile, ""
Print #hdlFile, "$smtp = New-Object System.Net.Mail.SmtpClient($SMTPServer, $SMTPPort);"
If GetIniString("PARAM", "SMTPSSL", TurboGraphIniFile, False) = "1" Then
    Print #hdlFile, "$smtp.EnableSSL = $true"
Else
    Print #hdlFile, "$smtp.EnableSSL = $false"
End If
If GetIniString("PARAM", "SMTPCREDENTIALS", TurboGraphIniFile, False) = "1" Then
    Print #hdlFile, "$smtp.UseDefaultCredentials = $true"
Else
    Print #hdlFile, "$smtp.Credentials = New-Object System.Net.NetworkCredential($Username, $Password);"
End If
If GetIniString("PARAM", "IGNORESSLTRUST", TurboGraphIniFile, False) = "1" Then Print #hdlFile, "[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { return $true }"
Print #hdlFile, "Start-Transcript -path '" & GetTempFileNameVB("WWW", True) & "Log.txt'"
Print #hdlFile, "$smtp.send($message)"
Print #hdlFile, "Stop-Transcript"
Print #hdlFile, "Remove-Item '" & GetTempFileNameVB("WWW", True) & "Password.txt'"
Print #hdlFile, "Remove-Item '" & GetTempFileNameVB("WWW", True) & "AES.key'"

Close #hdlFile

'-> Obtenir un handle de fichier
hdlFile = FreeFile
Dim sTemp As String
sTemp = GetTempFileNameVB("WWW", True) & "cmd" & Rnd & ".bat"

'-> Ouverture du fichier ascii et �criture des pages
Open sTemp For Output As #hdlFile

Print #hdlFile, "powershell.exe -ExecutionPolicy unrestricted -file " & TempFile
Close #hdlFile
        
CurrentPIDProcess = Shell(sTemp, vbHide)
'-> Boucler tant que le processus est actif
DoEvents
i = 0
Do While IsPidRunning(CurrentPIDProcess)
    '-> Lib�ration de la pile des messages
    i = i + 1
    DoEvents
    If i > 60 Then Exit Do
    Sleep 300
Loop
DoEvents
Trace "Allways isRunning ? : " & IsPidRunning(CurrentPIDProcess)
DoEvents
'Sleep val(GetIniString("PARAM", "SMTPDELAI", App.Path & "\Turbograph.ini", False))
'DoEvents

'-> on analyse l'envoi
hdlFile = FreeFile
Open GetTempFileNameVB("WWW", True) & "Log.txt" For Input As #hdlFile
'-> Boucle d'analyse du fichier
Dim strTemp As String
Dim topF As Boolean
Dim Ligne As String
topF = False
i = 0
Do While Not EOF(hdlFile) And i < 100
    '-> Lecture de la ligne
    i = i + 1
    Line Input #hdlFile, Ligne
    '-> Decrypter la source
    If "+ ~~~~~~~~~~~~~~~~~~~~" = Ligne And strTemp = "" Then
        topF = True
    End If
    If "+ ~~~~~~~~~~~~~~~~~~~~" = Ligne And strTemp <> "" Then
        topF = False
    End If
    Ligne = UTF8ToA(Ligne)
    Trace Ligne
    If topF And InStr(1, Ligne, "ps1", vbTextCompare) = 0 And InStr(1, Ligne, "$message") = 0 Then strTemp = strTemp & Ligne & Chr(13)
Loop
Close #hdlFile
Kill GetTempFileNameVB("WWW", True) & "Log.txt"
Kill sTemp
Kill TempFile
Err.Description = ""
sendBySMTP_POWERSHELL = True
Me.MousePointer = 0
If strTemp <> "" Then
    MsgBox strTemp, vbCritical, "Erreur d'envoi de mail"
    sendBySMTP_POWERSHELL = False
    Err.Clear
End If
'Unload Me
End Function

Private Function sendByPHPMail(ByVal FileToSend As String, sAdrMail As String) As Boolean
'--> cette fonction nous permet d'envoyer un mail
'--> par le composant winsock
Dim i As Integer
Dim j As Long
Dim m_strEncodedFiles As String
Dim l_destinataire
Dim destinataire
Dim sPieceJointe As String
Dim CurrentPIDProcess As Long
Dim sSMTP As String
Dim sPort As Integer

'On Error Resume Next

Me.MousePointer = 11

'-> on se connecte au serveur
'Winsock1.Close
DoEvents

For i = 1 To NumEntries(sAdrMail, ";")
    If i <> 1 Then l_destinataire = l_destinataire & ";"
    l_destinataire = l_destinataire & Entry(i, Entry(3, sAdrMail, "~"), ";")
Next i
sAdrMail = l_destinataire
'-> si destinataire en CC
If Me.Text2.Text <> "" Then
    sAdrMail = sAdrMail & "|" & Me.Text2.Text
End If

'-> on s'occupe ici du fichier a envoyer
If Me.Check1.Value = 1 Then
    IsCryptedFile = True
Else
    IsCryptedFile = False
End If

'-> Format de la piece jointe
If FormatTurbo Then pFormat = "0"
If FormatHtml = True Then pFormat = "1"
If FormatNothing Then pFormat = "3"
If FormatPDF = True Then pFormat = "4"

'-> Attacher le fichier sp�cifi�
Select Case pFormat
    Case "0" 'turbo
        'on attache le spool turbo
        sPieceJointe = FileToSend
    Case "1" 'html
        '-> on transforme le spool en html
        '-> Inclure le num�ro de page
        IncluseSpoolNumber = CBool(Me.Check2.Value)
        '-> Positionnement de la navigationHTML
        If Me.Option4.Value Then
            NavigationType = 0 'FrameSet
        ElseIf Me.Option5.Value Then
            NavigationType = 1 'Fichier unique
        Else
            NavigationType = 2 'Pages
        End If
        PathToExport = GetTempFileNameVB("WWW", True)
        FileKeyMail = Trim(UCase$(FileToSend))
        '-> Nom du Fichier
        FileToExport = LCase$(GetSpoolName(FileKeyMail)) & ".html"
        '-> Cl� du fichiers
        FileKeyExport = FileKeyMail
        '-> Spool
        SpoolKeyMail = aSpool.Key
        SpoolKeyExport = SpoolKeyMail
        '-> Num�ro de page
        PageToExport = PageNumMail
        ExportType = 1
            
        '-> On ajoute le spool
        FileToSend = AnalyseFileToPrint(FileToSend)

        CreateEditionHTML
        '-> Attacher le fichier sp�cifi�
        If ListeHtmlFile <> "" Then
            sPieceJointe = ListeHtmlFile
        End If
        '-> Ajouter les images s'il y en a
        For i = 1 To NbImages
            sPieceJointe = sPieceJointe & "|" & ListeHTMLImages(i)
        Next
    Case "3" 'nothing
    
    Case "4" 'pdf
        '-> on g�n�re le fichier avec le turboPDF
        If FilePath = "" Then FilePath = OpenPath
        CurrentPIDProcess = Shell(Chr(34) & App.Path & "\TurboPdf.exe" & Chr(34) & " " & Chr(34) & "�fileToConvert=" & FileToSend & Replace(Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare), "�pdfDirectory=", "�pdfDirectoryNo=", , , vbTextCompare) & "�pdfDirectory=" & Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1) & Chr(34))
        '-> Boucler tant que le processus est actif
        Do While IsPidRunning(CurrentPIDProcess)
            '-> Lib�ration de la pile des messages
            DoEvents
            Sleep 300
        Loop
        'on attache le spool turbo
        sPieceJointe = (Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1) & "/" & GetSpoolName(FileToSend) & ".pdf")
End Select

'-> on regarde si on a des pieces jointes
If Fichiers(aSpool.fileName).filesJoins <> "" Then
    sPieceJointe = sPieceJointe & ";" & Replace(Fichiers(aSpool.fileName).filesJoins, ",", ";")
End If

Dim TempFile As String
Dim hdlFile As Long
DoEvents

TempFile = "TurbographMail-" & Year(Now) & "-" & FORMAT(Month(Now), "00") & "-" & FORMAT(Day(Now), "00") & "-" & FORMAT(Hour(Now), "00") & "-" & FORMAT(Minute(Now), "00") & "-" & FORMAT(Second(Now), "00") & Right(CStr(GetTickCount), 2) & ".mail"
TempFile = GetTempFileNameVB("WWW", True) & TempFile

'-> Obtenir un handle de fichier
hdlFile = FreeFile
'-> Ouverture du fichier ascii et �criture des pages
Open TempFile For Output As #hdlFile
'-> Ligne 1 : From (mail adress)
If Me.Text3.Text = "" Then Me.Text3.Text = "Unknown"
Print #hdlFile, Trim(Me.Text3.Text)
'-> Ligne 2 : To (List Mail adress to;|cc;|cci; )
Print #hdlFile, sAdrMail
'-> Ligne 3 : Pieces jointes
Print #hdlFile, GetFileName(sPieceJointe)
'-> Ligne 4 : Subject
Print #hdlFile, Me.Text1.Text
'-> Ligne 5 : Body
Print #hdlFile, Me.RichTextBox1.Text
Close #hdlFile

For i = 1 To NumEntries(sPieceJointe, ";")
    If Trim(Entry(i, sPieceJointe, ";")) <> "" Then
        UploadMail (Entry(i, sPieceJointe, ";"))
        DoEvents
        Sleep 200
        DoEvents
    End If
Next i
'-> on upload le fichier mail (en dernier)
UploadMail (TempFile)

DoEvents
Sleep 100
DoEvents

sendByPHPMail = True
Me.MousePointer = 0
If strTemp <> "" Then
    MsgBox strTemp, vbCritical, "Erreur d'envoi de mail"
    sendByPHPMail = False
    Err.Clear
End If
'Unload Me
End Function

Private Sub Winsock1_DataArrival(ByVal bytesTotal As Long)
'--> ici sont g�r�e les r�ponses du composant winsock
Dim DonneesRecues As String
Dim strMessage As String

On Error GoTo GestError

Winsock1.getData DonneesRecues

'Exit Sub

toto = toto & Chr(13) & "Reponse : " & DonneesRecues

Select Case iStep
    Case 1 '-> connexion au serveur
        If InStr(1, DonneesRecues, "220") Then
            ReponseFaite = True
        Else
            strMessage = "Impossible de se connecter au serveur SMTP" & Chr(13) & DonneesRecues
        End If
    Case 2 '-> controle de la connexion
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
        Else
            strMessage = "Connexion refus�e par le serveur SMTP" & Chr(13) & DonneesRecues
        End If
    Case 3 '-> adresse mail de l'envoyeur et du destinataire
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
            'iStep = 4
        Else
            strMessage = "Adresse mail de l'envoyeur non reconnue par le serveur SMTP" & Chr(13) & DonneesRecues
            ReponseFaite = True
        End If
    Case 4 '-> adresse mail de l'envoyeur et du destinataire
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
        Else
            strMessage = "Adresse mail du destinataire non reconnue par le serveur SMTP" & Chr(13) & DonneesRecues
            ReponseFaite = True
        End If
    Case 5  '-> Envoi des donnees
        If InStr(1, DonneesRecues, "354 ") Then
            ReponseFaite = True
        Else
            strMessage = "Erreur lors de l'envoi de l'ent�te du mail" & Chr(13) & DonneesRecues
        End If
    Case 6
        If InStr(1, DonneesRecues, "250") Then
            ReponseFaite = True
            iStep = 7
        Else
            strMessage = "Erreur lors de l'envoi du contenu du mail" & Chr(13) & DonneesRecues
        End If
    Case 7 '-> fermeture de la connexion
        If InStr(1, DonneesRecues, "250") Then ReponseFaite = True
End Select

If strMessage <> "" Then MsgBox strMessage

Trace "-> " & DonneesRecues, 4

'-> bon on a recu une r�ponse
ReponseFaite = True
Exit Sub
GestError:
If Err.Number = 505 Then

End If
End Sub

Private Sub printDestinataires()
'--> cette proc�dure va permettre d'imprimer selon les cas les destinataires selectionn�s ou pas ou tous
'---> Envoyer un message vers Internet

Dim SpoolName As String
Dim aNode As Node
Dim PageMin As Integer
Dim PageMax As Integer
Dim aToPrint As Boolean

Dim DeviceName As String
Dim ChoixPage As String
Dim NbCopies As Integer
Dim RectoVerso As String
Dim Assembl As String
Dim NoGard As String
Dim FileToPrint As String
Dim CurrentPIDProcess As Long
Dim aFichier As Fichier

'On Error GoTo GestError

'-> on initialise
ListeHtmlFile = ""
Err.Description = ""
'on met le sablier
Screen.MousePointer = 11

If Me.Check3.Value = 1 Then
    aToPrint = True
Else
    aToPrint = False
End If

'-> vider le picture de temporisation
Me.Picture2.Cls
TailleTotale = 0
'-> Calcul de la taille total
'-> on parcourt le treeview pour les mails � envoyer
For Each aNode In Me.TreeView1.Nodes
    '-> on on compte
    If (aNode.Checked = aToPrint) And aNode.Tag <> "SPOOL" Then TailleTotale = TailleTotale + 1
Next

'-> Vider la variable de retour
strRetour = ""

'-> Setting des param�trages sur le fichier et le spool en cours
frmPrint.FichierName = aSpool.fileName
frmPrint.SpoolKey = aSpool.Key
frmPrint.IsSelectionPage = aSpool.IsSelectionPage

frmPrint.Option4.Visible = False
frmPrint.Frame2.Enabled = False
'-> Afficher le choix de l'imprimante
Screen.MousePointer = 0
frmPrint.Show vbModal
Screen.MousePointer = 11
'-> Quitter si annuler
If strRetour = "" Then Exit Sub

TailleLue = 0

'-> on parcourt le treeview pour les mails � envoyer
For Each aNode In Me.TreeView1.Nodes
    '-> on verifie que l'on est bien sur un mail � envoyer
    If (aNode.Checked = aToPrint) And aNode.Tag <> "SPOOL" Then
        '-> Ne mettre � jour les variables de traitement que si on vient de la visu et pas du menu
        '-> Cl� du spool et du fichier
        Set aFichier = Fichiers(aSpool.fileName)
        On Error Resume Next
        Set aSpool = aFichier.Spools(Entry(1, aNode.Key, "�"))
        PageMin = CInt(Entry(2, Entry(2, aNode.Key, "�"), "|"))
        PageMax = CInt(Entry(2, Entry(2, aNode.Next.Key, "�"), "|")) - 1
        If PageMax < PageMin Then PageMax = aSpool.nbPage
        '-> nom du spool � traiter
        On Error GoTo GestError
        SpoolName = aSpool.fileName
        SpoolName = CreateDetail(aSpool, -2, PageMin, PageMax)
                
        '-> Traiter les choix d'impression
        FileToPrint = SpoolName
        DeviceName = Entry(1, strRetour, "|")
        NbCopies = Entry(3, strRetour, "|")
        RectoVerso = Entry(4, strRetour, "|")
        If copyAssemb Then Assembl = Entry(5, strRetour, "|")
        If noGarde Then NoGard = Entry(6, strRetour, "|")
            
        '-> Lancer l'impression
        CurrentPIDProcess = Shell(App.Path & "\TurboGraph.exe " & Chr(34) & DeviceName & "~DIRECT~" & NbCopies & "|" & FileToPrint & "|" & RectoVerso & "|" & Assembl & "|" & NoGard & Chr(34), vbNormalFocus)
        Dim aSpool2 As Spool
        AnalyseFileToPrint FileToPrint
        Set aSpool2 = Fichiers.Item(Trim(FileToPrint)).Spools(1)
        If GetTitrePdf(aSpool2, 1) <> "[NO]" Then
            Dim pdfDir As String
            Dim pdfName As String
            pdfName = GetFileName(GetTitrePdf(aSpool2, 1))
            pdfDir = Replace(GetTitrePdf(aSpool2, 1), pdfName, "")
            CurrentPIDProcess = Shell(App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & FileToPrint & Replace(Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare), "�pdfDirectory=", "�NoPdfDirectory=", , , vbTextCompare) & "�pdfDirectory=" & pdfDir & "�pdfName=" & pdfName & Chr(34))
        End If
        
        '-> Boucler tant que le processus est actif
        Do While IsPidRunning(CurrentPIDProcess)
            '-> Lib�ration de la pile des messages
            DoEvents
            Sleep 1000
        Loop
        
        '-> Dessin de la temporisation
        TailleLue = TailleLue + 1
        DrawTempo Me.Picture2
        DoEvents
    End If
Next

GestError:
'on met le sablier
Screen.MousePointer = 0
Me.SetFocus
If Err.Description <> "" Then MsgBox Err.Description, vbExclamation

End Sub

Private Function VueSave(sName As String)
Dim aNode As Node
Dim aBlock As Block
Dim sMaquette As String

'--> cette fonction permet d'enrtegistrer une vue
If Fichiers(Me.FichierName).Spools(Me.SpoolKey).maquette.Nom = "" Then
    SetIniString Trim(UCase(sName)), "Maquette", App.Path & "\TurboMail.ini", "Generique"
Else
    SetIniString Trim(UCase(sName)), "Maquette", App.Path & "\TurboMail.ini", Fichiers(Me.FichierName).Spools(Me.SpoolKey).maquette.Nom
End If
SetIniString Trim(UCase(sName)), "ENVOYEUR", App.Path & "\TurboMail.ini", Me.Text3.Text
SetIniString Trim(UCase(sName)), "OBJET", App.Path & "\TurboMail.ini", Me.Text1.Text
SetIniString Trim(UCase(sName)), "CORPS", App.Path & "\TurboMail.ini", Replace(Me.RichTextBox1.TextRTF, Chr(13) & Chr(10), "chr(13)")

End Function

Private Function VueLoad(sName As String)
Dim i As Integer
Dim aNode As Node
Dim aBlock As Block
Dim sMaquette As String
Dim sTrav As String

On Error Resume Next

'--> cette fonction permet de charger une vue enregistree
sTrav = GetIniString(Trim(UCase(sName)), "ENVOYEUR", App.Path & "\TurboMail.ini", False)
Me.Text3.Text = sTrav
sTrav = GetIniString(Trim(UCase(sName)), "OBJET", App.Path & "\TurboMail.ini", False)
Me.Text1.Text = sTrav
sTrav = GetIniEntry(Trim(UCase(sName)), "CORPS", App.Path & "\TurboMail.ini", False)
Me.RichTextBox1.TextRTF = Replace(sTrav, "chr(13)", Chr(13) & Chr(10))

End Function

Private Function GetMailValue(aSpool As Spool, strRupture As String, NumPage As Long) As String
'--> Cette fonction r�cup�re le titre d'une page
Dim ParamTitre As String
Dim Ligne As String
Dim Rg As String
Dim Dz As String
Dim i As Integer
Dim j As Integer
Dim topOK As Boolean
Dim topFIND As Boolean

'-> on regarde si on a specifi� un titre
'If InStr(1, aSpool.GetMaq, "[MAIL]") = 0 Then
'    GetRuptureValue = "[NO]"
'    Exit Function
'End If

'-> on r�cup�re le param�trage
ParamTitre = strRupture

topOK = False
topFIND = False

'-> on parcours les titres � r�cuperer
For i = 1 To NumEntries(ParamTitre, "\")
    Rg = Entry(i, ParamTitre, "\")
    Dz = "^" + Entry(1, Entry(2, Rg, "�"), Chr(0))
    Rg = "[" + Entry(1, Rg, "�") + "]"
    '-> on recherche la ligne correspondante
    For j = 1 To NumEntries(aSpool.GetPage(NumPage), Chr(0))
        Ligne = Trim(Entry(j, aSpool.GetPage(NumPage), Chr(0)))
        If InStr(1, UCase(Ligne), UCase(Rg)) Then
            '-> on regarde si on trouve le diez
            If InStr(1, UCase(Ligne), UCase(Rg)) <> 0 And InStr(1, Ligne, Dz) Then
                '-> on est sur une bonne ligne on r�cup�re la valeur du diez
                topOK = True
                topFIND = True
                Dz = RTrim(Mid(Entry(1, Entry(1, Entry(2, Ligne, Dz), "^"), "}"), 5))
                If InStr(1, Dz, "[\") <> 0 Then Dz = ""
                If Dz = "***" Then topOK = False
                If GetMailValue = "" Then
                    GetMailValue = Dz
                Else
                    GetMailValue = GetMailValue + "�" + Dz
                End If
                '-> sortir de la boucle
                Exit For
            End If
        End If
    Next
    If topFIND = True And Dz = "" And i = 1 Then GetMailValue = " "
    topFIND = False
Next
If topOK = False Then GetMailValue = "[NO]"
If Entry(1, GetMailValue, "�") = "***" Then GetMailValue = "[NO]"
End Function

Public Function UploadMail(ByVal strFile As String) As Boolean
    Dim strHttp As String
    Dim strFileContent As String
    Dim DestUrl As URL
    
    '-> un envoi est encore en cours quitter
    Winsock2.Close
    Winsock1.Close
    
    
    ' extract the URL using a helper function
    DestUrl = ExtractUrl(GetIniString("SITE", "PHPMAIL", App.Path & "\Turbograph.ini", False) & "/siexe/deal/emilie/php/do_turbomaildump_w3c.php")
    
    '-> si on a pas de site...
    If DestUrl.Host = vbNullString Then
        Trace "Invalid Host for mail"
        Exit Function
    End If
        
    ' read the file contents as a string
    ' N.B: in HTTP everything is a string, even binary files
    strFileContent = GetFileContents(strFile)
    
    ' build the HTTP request
    strHttp = BuildFileUploadRequest(strFileContent, DestUrl, GetFileName(strFile), GetFileName(strFile), "application/octet-stream")
    
    ' assign the protocol host and port
    Winsock2.Protocol = sckTCPProtocol
    Winsock2.RemoteHost = DestUrl.Host
    
    If DestUrl.Port <> 0 Then
        Winsock2.RemotePort = DestUrl.Port
    Else
        Winsock2.RemotePort = 80
    End If
            
    ' make the connection and send the HTTP request
    Winsock2.Connect
    
    While Winsock2.State <> sckConnected
        DoEvents
    Wend

    
    While Not blnConnected
        DoEvents
    Wend
    
    Trace "Upload : " & Now & strFile
    MDIMain.StatusBar1.Panels(1).Text = "Upload : " & Now & "  " & strFile
    Winsock2.SendData strHttp
    UploadMail = True
End Function

Private Function GetFileContents(ByVal strPath As String) As String
    Dim StrReturn As String
    Dim lngLength As Long
    
    lngLength = FileLen(strPath)
    StrReturn = String(lngLength, Chr(0))
    
    On Error GoTo ERR_HANDLER
    '-> Obtenir un handle de fichier et ouvrir le spool � imprimer
    hdlFile = FreeFile

    Open strPath For Binary As FreeFile
    
    Get hdlFile, , StrReturn
    
    GetFileContents = StrReturn
    
    Close #hdlFile
    
    Exit Function
    
ERR_HANDLER:
    MsgBox Err.Description, vbCritical, "ERROR"
    
    Err.Clear
End Function

' la connection est accept�e
Private Sub Winsock2_Connect()
    blnConnected = True
End Sub

' quand la connection est termin�e
Private Sub Winsock2_Close()
    Winsock2.Close
    blnConnected = False

End Sub

Private Sub Winsock2_DataArrival(ByVal bytesTotal As Long)
'--> ici sont g�r�e les r�ponses du composant winsock
Dim DonneesRecues As String
Dim strMessage As String

Winsock2.getData DonneesRecues

'-> bon on a recu une r�ponse
If InStr(1, DonneesRecues, "200 OK") Then ReponseFaite = True
Me.MousePointer = 0

Trace DonneesRecues
MDIMain.StatusBar1.Panels(1).Text = DonneesRecues
End Sub



