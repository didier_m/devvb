Attribute VB_Name = "fctExcel"
'***********************************************************
'* Ce module est utilis� pour exporter un spool vers Excel *
'***********************************************************

Option Explicit

'-> DOM Excel
'Dim ExcelApp As Object
'Dim aWorkBook As Object
'Dim aSheet As Object
'Dim aRange As Object

Public Function IsExcel() As Boolean

'---> Cette proc�dure essaye de cr�er un lien OLE vers Excel pour tester que les _
bibli sont install�es

Dim aExcel As Object

On Error GoTo ExcelError

'-> Essayer de cr�er un lien OLE
Set aExcel = StartExcel()

'-> Lib�rer le pointeur
Set aExcel = Nothing

'-> Renvoyer une valeur de succ�s
IsExcel = True

Exit Function

ExcelError:
    '-> Si erreur : pas Excel
    IsExcel = False

End Function

