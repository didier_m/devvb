VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGUnzipFiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'-> Extraction de l'atchive sp�cifi�e dans le r�pertoire sp�cifi�
Public Function ExtractFile(NomArchive As String, ExtractDirectory As String) As Boolean

Dim RetTestZip As Long

On Error GoTo GestError

'->Initialisation des variables globales
uZipInfo = ""
uZipNumber = 0
uPromptOverWrite = 0
uOverWriteFiles = 1
If Not TurboOverWrite Then uOverWriteFiles = 0
uDisplayComment = 0
uExtractList = 0
uHonorDirectories = 0
uZipNames.uzFiles(0) = vbNullString
uNumberFiles = 0
uExcludeNames.uzFiles(0) = vbNullString
uNumberXFiles = 0
'-> On sp�cifie le nom du fichier
uZipFileName = NomArchive
uExtractDir = ExtractDirectory
uTestZip = 0
'-> Lancer le test pour v�rifier si le fichier est zip�
RetTestZip = VBUnZip32
If RetTestZip = 0 Then ExtractFile = True

Exit Function

GestError:

ExtractFile = False



End Function

'-> Test si l'archive est OK et zipp�e
Public Function TestArchive(NomFichier As String) As Boolean

Dim RetTestZip As Long

On Error GoTo GestError

'-> PAs d'archive � blanc
If Trim(NomFichier) = "" Then GoTo GestError
If FileLen(NomFichier) = 0 Then GoTo GestError

'->Initialisation des variables globales
uZipInfo = ""
uZipNumber = 0
uPromptOverWrite = 0
uOverWriteFiles = 1
uDisplayComment = 0
uExtractList = 1
uHonorDirectories = 0
uZipNames.uzFiles(0) = vbNullString
uNumberFiles = 0
uExcludeNames.uzFiles(0) = vbNullString
uNumberXFiles = 0
'-> On sp�cifie le nom du fichier
uZipFileName = NomFichier
uExtractDir = ""
uTestZip = 1
'-> Lancer le test pour v�rifier si le fichier est zip�
RetTestZip = VBUnZip32
If RetTestZip = 0 Then TestArchive = True

Exit Function

GestError:

TestArchive = False


End Function

