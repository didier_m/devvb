VERSION 5.00
Begin VB.Form frmListen 
   Caption         =   "Form1"
   ClientHeight    =   3315
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4560
   LinkTopic       =   "Form1"
   ScaleHeight     =   3315
   ScaleWidth      =   4560
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdListen 
      Caption         =   "Listen"
      Height          =   975
      Left            =   120
      TabIndex        =   4
      ToolTipText     =   "Listen for others to connect you (SERVER)"
      Top             =   120
      Width           =   1695
   End
   Begin VB.TextBox txtOut 
      Height          =   285
      Left            =   120
      TabIndex        =   3
      Top             =   1200
      Width           =   3615
   End
   Begin VB.TextBox txtin 
      Height          =   975
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   1920
      Width           =   3615
   End
   Begin VB.TextBox txtNick 
      Height          =   285
      Left            =   120
      TabIndex        =   1
      Top             =   1560
      Width           =   3615
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Stop"
      Height          =   975
      Left            =   1920
      TabIndex        =   0
      Top             =   120
      Width           =   1575
   End
   Begin VB.Timer tmrUpdate 
      Interval        =   20000
      Left            =   4080
      Top             =   0
   End
   Begin VB.Label lblState 
      AutoSize        =   -1  'True
      Caption         =   "State :"
      Height          =   195
      Left            =   120
      TabIndex        =   5
      Top             =   3000
      Width           =   3585
   End
End
Attribute VB_Name = "frmListen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Public bListen As Boolean
Public sListen As String
Public aListen As Integer

Private Declare Function GetIpAddrTable_API Lib "IpHlpApi" Alias "GetIpAddrTable" (pIPAddrTable As Any, pdwSize As Long, ByVal bOrder As Long) As Long

' Returns an array with the local IP addresses (as strings).
' Author: Christian d'Heureuse, www.source-code.biz
Public Function GetIpAddrTable()
   Dim Buf(0 To 511) As Byte
   Dim BufSize As Long: BufSize = UBound(Buf) + 1
   Dim rc As Long
   rc = GetIpAddrTable_API(Buf(0), BufSize, 1)
   If rc <> 0 Then Err.Raise vbObjectError, , "GetIpAddrTable failed with return value " & rc
   Dim NrOfEntries As Integer: NrOfEntries = Buf(1) * 256 + Buf(0)
   If NrOfEntries = 0 Then GetIpAddrTable = Array(): Exit Function
   ReDim IpAddrs(0 To NrOfEntries - 1) As String
   Dim i As Integer
   For i = 0 To NrOfEntries - 1
      Dim j As Integer, s As String: s = ""
      For j = 0 To 3: s = s & IIf(j > 0, ".", "") & Buf(4 + i * 24 + j): Next
      IpAddrs(i) = s
      Next
   GetIpAddrTable = IpAddrs
End Function

' Test program for GetIpAddrTable.
Public Function myIPs() As String
   Dim IpAddrs
   IpAddrs = GetIpAddrTable
   Dim i As Integer
   For i = LBound(IpAddrs) To UBound(IpAddrs)
      If myIPs = "" Then
        myIPs = IpAddrs(i)
      Else
        myIPs = myIPs & "," & IpAddrs(i)
      End If
   Next
End Function

Sub UpdateState() ' sub for controling sck.state
    'On Error Resume Next

    ' print the state of sck
    Select Case Sck.State
        Case 0
            MDIMain.StatusBar1.Panels(2).Text = "Closed"
        Case 1
            MDIMain.StatusBar1.Panels(2).Text = "Open"
        Case 2
            MDIMain.StatusBar1.Panels(2).Text = "Listening"
        Case 3
            MDIMain.StatusBar1.Panels(2).Text = "ConnectionPending"
        Case 4
            MDIMain.StatusBar1.Panels(2).Text = "ResolvingHost"
        Case 5
            MDIMain.StatusBar1.Panels(2).Text = "HostResolved"
        Case 6
            MDIMain.StatusBar1.Panels(2).Text = "Connecting"
        Case 7
            MDIMain.StatusBar1.Panels(2).Text = "Connected"
        Case 8
            MDIMain.StatusBar1.Panels(2).Text = "Closing"
        Case 9
            MDIMain.StatusBar1.Panels(2).Text = "Error"
            Sck.Close
            Sck.LocalPort = 6001
            Sck.Listen ' listen for others to connect you
            startListen
    End Select
End Sub
Private Sub cmdListen_Click()
    ' set localport for listening ( you can change it whatever you want )
    On Error Resume Next
    If Err.Description <> "" Then
        Trace "Av" & Err.Description
        Err.Clear
    End If
    bListen = True
    Sck.Close
    Sck.LocalPort = 6001
    Sck.Listen ' listen for others to connect you
    UpdateState
    startListen
    If Err.Description <> "" Then
        Trace "Ap" & Err.Description
        Err.Clear
    End If
End Sub

Private Sub Command1_Click()
    bListen = False
    Sck.Close
End Sub

Private Sub Form_Load()
    '-> lecture du parametrage
    sListen = GetIniString("PARAM", "LISTEN", App.Path & "\Turbograph.ini", False)
    sListen = DeCrypt(sListen)
    sListen = Replace(sListen, "::", "�")
    sListen = Entry(1, sListen, "�")
    Call cmdListen_Click
End Sub

Private Sub http_Connect()
Debug.Print "connected"
End Sub

Private Sub http_DataArrival(ByVal bytesTotal As Long)
    Debug.Print "DatatArrival"
    Dim strResponse As String
    If bytesTotal <> 0 Then
        http.getData strResponse, vbString, bytesTotal
        Trace strResponse
        Debug.Print strResponse
    End If
End Sub

Private Sub http_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    MsgBox Description
End Sub

Private Sub http_SendComplete()
    Debug.Print "SendComplete"
End Sub

Private Sub Sck_Close()
'    If bListen Then
'     Sck.Close
'     Sck.Listen
'     bListen = False
'    End If
End Sub

Private Sub sck_ConnectionRequest(ByVal requestID As Long)
'    If Sck.State <> sckClosed Then Sck.Close ' if not closed the connection close it
'    Sck.Accept requestID ' accept the requestid
End Sub

Private Sub Sck_DataArrival(ByVal bytesTotal As Long)
'    Dim IncomeData As String
'    Sck.getData IncomeData ' get data
'    traiteListen (IncomeData)
'    Sck.Close
'    Sck.Listen
End Sub

Private Sub tmrUpdate_Timer()
'UpdateState ' update the state of Sck every 100 miliseconds
getFiles  ' on lance une requete manuelle de demande
End Sub

Public Sub traiteListen(files As String, Optional force As Boolean)
    Dim fileName As String
    Dim NomFichier As String
    strTemp = Split(files, ",")
    Trace "Litener receive :" & files
    
    Dim tListen As Integer
    tListen = aListen
    If force Then tListen = 2
    For j = 0 To UBound(strTemp)
        forceScreen = True
        fileName = strTemp(j)
        If Not force Then MDIMain.m_fMessage.ShowMessage fileName, 5000
        
        If tListen < 2 Then Exit Sub
        
        '-> Affecter le nom
        If Not TelechargeFile(sListen & "/" & fileName, GetMyDocumentsPathVB() & fileName) Then
            MsgBox "Error getting " & sListen & fileName, vbExclamation
            Exit Sub
        End If
        
        fileName = GetMyDocumentsPathVB() & fileName
        
        If tListen = 2 Or force Then
         '-> V�rifier ici que le fichier n'est pas d�ja charg�
         If MDIMain.IsLoadedFile(fileName) Then
             Set aLb = Libelles("MESSAGE")
             'MsgBox aLb.GetCaption(2), vbExclamation + vbOKOnly, aLb.GetToolTip(2)
             GoTo suite
         End If
         nPrinter = "SCREEN"
         mode = 1
         Trace "On affiche le spool du watcher"
         '-> On regarde si il y a un programme associ� different de turbo
         'strBuffer = Space$(1024)
         'lngResult = FindExecutable(FileName, "D:\demoturbo\", strBuffer)
         If LCase(Mid(fileName, InStrRev(fileName, "."))) <> ".turbo" Then
             '-> on lance l'application associ�e
             strBuffer = "" 'Left(strBuffer, Len(strBuffer) - Len(Dir(strBuffer)))
             lngResult = ShellExecute(Me.hwnd, "open", fileName, vbNullString, vbNullString, 1)
             GoTo suite
         End If
         '-> Test surt le ZIP
         FilePath = Mid(fileName, 1, InStrRev(Replace(fileName, "\", "/"), "/"))
         NomFichier = GetUnzipFileName(fileName)
         
                 'dans le cas des packages
                 If InStrRev(NomFichier, ".zip", , vbTextCompare) = Len(NomFichier) - 3 Then
                     NomFichier = uZipInfo2
                     For i = 1 To NumEntries(NomFichier, ",")
                         '-> Test sur le ZIP
                         fileName = GetUnzipFileName(Entry(i, NomFichier, ","))
                         '-> Lancer le chargement du fichier
                         DisplayFileGUI fileName
                         
                         '-> Charger eventuellement au menu les fichiers joints
                         LoadJoinFile uZipInfo2, fileName
                     Next
                     NomFichier = ""
                     GoTo suite
                 End If
         '-> Lancer le chargement du fichier
         DisplayFileGUI NomFichier
         
         '-> Charger eventuellement au menu les fichiers joints
         LoadJoinFile uZipInfo2, NomFichier
         forceScreen = False
        End If
        
        If tListen = 3 Then
            Shell App.Path & "\TurboGraph.exe " & TurboGraphWebPrinter & "~DIRECT~1" & "|" & fileName
        End If
suite:
    Next

End Sub

Public Sub startListen()
    Dim Rep As String
    
    On Error GoTo GestError
    Trace "start Listen"
    SetIniString "PARAM", "WATCHER", App.Path & "\Turbograph.ini", CStr(aListen)
    If TurboGraphWebFile = "" Then
        'MsgBox "Param�trage absent SITE", vbCritical
        Exit Sub
    End If
    
    Dim pListen As String
    pListen = GetIniString("PARAM", "LISTEN", App.Path & "\Turbograph.ini", False)
    If pListen = "" Then
        Exit Sub
        Rep = InputBox("Ins�rer votre cl�", "Cl� d'authentification absente", "")
        If Rep = "" Then Exit Sub
        If Rep = DeCrypt(Rep) Then
            MsgBox "Bad key.", vbCritical
            Exit Sub
        End If
        SetIniString "PARAM", "LISTEN", App.Path & "\Turbograph.ini", Rep
        pListen = Rep
    End If
    
    pListen = DeCrypt(pListen)
    pListen = Replace(pListen, "::", "�")
    pListen = Entry(2, pListen, "�")
        
'    Trace TurboGraphWebFile & "/siexe/deal/emilie/php/watcher_w3c.php?&client=" & myIPs & "&path=" & pListen
'
'    Set myMSXML = CreateObject("MSXML2.XMLHTTP.6.0")
'    myMSXML.Open "POST", TurboGraphWebFile & "/siexe/deal/emilie/php/watcher_w3c.php?&client=" & myIPs & "&path=" & pListen, True
'    myMSXML.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
'    myMSXML.Send "&client=" & myIPs & "&path=" & pListen
'
'    Trace "watcher launched!"
    Exit Sub
GestError:
    MsgBox "Error : " & Err.Description, vbCritical
End Sub

Public Sub getFiles()
    On Error Resume Next
    Dim lFiles As String
    '--> on va demander au serveur si on a des nouveaux spools
    Dim pListen As String
    Dim myMSXML As Object
    pListen = GetIniString("PARAM", "LISTEN", App.Path & "\Turbograph.ini", False)
    pListen = DeCrypt(pListen)
    pListen = Replace(pListen, "::", "�")
    pListen = Entry(2, pListen, "�")
    
    If pListen = "" Then Exit Sub
    MDIMain.StatusBar1.Panels(2).Text = "Reading..."
    Set myMSXML = CreateObject("MSXML2.XMLHTTP.6.0")
    DoEvents
    myMSXML.Open "GET", TurboGraphWebFile & "/siexe/deal/emilie/php/watcher_w3c.php?&files=" & pListen & "&tick=" & CStr(GetTickCount), True    'false pour sync
    Trace TurboGraphWebFile & "/siexe/deal/emilie/php/watcher_w3c.php?&files=" & pListen & "&tick=" & CStr(GetTickCount)
    myMSXML.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
    myMSXML.Send
    
    Sleep 2000
    MDIMain.StatusBar1.Panels(2).Text = ""
    DoEvents
    lFiles = ""
    lFiles = myMSXML.responseText
    MDIMain.StatusBar1.Panels(2).Text = lFiles
    Trace lFiles
    '--> on provoque l'ouverture/impression des fichiers
    If lFiles <> "" Then traiteListen (lFiles)
    
End Sub

' url encodes a string
Function URLEncode(ByVal str As String) As String
        Dim intLen As Integer
        Dim x As Integer
        Dim curChar As Long
                Dim newStr As String
                intLen = Len(str)
        newStr = ""
                        For x = 1 To intLen
            curChar = Asc(Mid$(str, x, 1))
            
            If (curChar < 48 Or curChar > 57) And _
                (curChar < 65 Or curChar > 90) And _
                (curChar < 97 Or curChar > 122) Then
                                newStr = newStr & "%" & Hex(curChar)
            Else
                newStr = newStr & Chr(curChar)
            End If
        Next x
        
        URLEncode = newStr
End Function

Public Sub filePrint(strFichier As String)
        Dim aSpool As Spool
        Dim CurrentPIDProcess As Long
        
        Call AnalyseFileToPrint(strFichier)
        
        '-> Pointer sur l'objet fichier
        Set aFichier = Fichiers(UCase$(strFichier))
        Set aSpool = aFichier.Spools(1)
        
        frmPrint.FichierName = aSpool.fileName
        frmPrint.SpoolKey = aSpool.Key
        frmPrint.IsSelectionPage = aSpool.IsSelectionPage
        
        '-> afficher ou pas la zone d'impression de la globalit� des spools
        If Fichiers(UCase$(aSpool.fileName)).Spools.Count = 1 Then
            frmPrint.Option4.Visible = False
        End If
        
        If aSpool.nbPage = 0 And Fichiers(UCase$(aSpool.fileName)).Spools.Count = 1 Then
            MsgBox "Ce fichier ne contient pas un spool avec des pages � imprimer", vbInformation, "Imprimer un fichier"
            Exit Sub
        End If
                
        '-> Analyse du choix d'impression
        Select Case Entry(1, ChoixPage, "�")
            Case 1 '-> Fichier en entier
                '-> Construire le nom du fichier
                'FileToPrint = CreateDetail(Me.aSpool, -1)
                FileToPrint = aItem.Key 'aSpool.FileName
            Case 2 '-> Page mini � page maxi
                '-> R�cup�rer les pages � imprimer
                PageMin = CInt(Entry(2, ChoixPage, "�"))
                PageMax = CInt(Entry(3, ChoixPage, "�"))
                
                '-> V�rifier la page min
                If PageMin < 1 Then
                    '-> V�rifier si le fichier poss�de une page de s�lection
                    If aSpool.IsSelectionPage Then
                        PageMin = 0
                    Else
                        PageMin = 1
                    End If
                End If
                
                '-> V�rifier la page maxi
                If PageMax > aSpool.nbPage Then PageMax = aSpool.nbPage
                    
                '-> V�rifier que la page mini est < � la page maxi
                If PageMin > PageMax Then PageMin = PageMax
                    
                '-> Construire le nom du fichier
                FileToPrint = CreateDetail(aSpool, -2, PageMin, PageMax)
                            
            Case 3  '-> Page en cours
                '-> Construire le nom du fichier
                FileToPrint = CreateDetail(aSpool, aSpool.CurrentPage)
            Case 4 '-> Spool en cours
                'FileToPrint = aSpool.FileName
                FileToPrint = CreateDetail(aSpool, -1)
        End Select
            
        '-> Lancer l'impression
        CurrentPIDProcess = Shell(App.Path & "\TurboGraph.exe " & DeviceName & "~DIRECT~" & NbCopies & "|" & FileToPrint & "|" & RectoVerso & "|" & Assembl & "|" & NoGard & "|" & printFilesJoins, vbNormalFocus)
        
        Do While IsPidRunning(CurrentPIDProcess)
            '-> Lib�ration de la pile des messages
            DoEvents
            Sleep 500
        Loop

End Sub
