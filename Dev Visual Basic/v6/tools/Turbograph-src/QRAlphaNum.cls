VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QRAlphaNum"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---------------------------------------------------------------
' QRAlphaNum
'---------------------------------------------------------------
Option Explicit

Dim aQRData As QRData
Dim aQRUtil As QRUtil

Private Sub Class_Initialize()
    Set aQRData = New QRData
    Set aQRUtil = New QRUtil
End Sub

Public Function QRAlphaNum(data)
    aQRData.QRData QR_MODE_ALPHA_NUM, data
End Function

'renamed write to writeB
Public Function writeB(buffer)
    Dim i As Integer
    Dim c() As String
    
    i = 0
    c = aQRData.getData()

    Do While i + 1 < UBound(c)
        buffer.put getCode(Asc(c(i))) * 45 + getCode(Asc(c(i + 1))), 11
        i = i + 2
    Loop

    If i < UBound(c) Then
        buffer.put getCode(Asc(c(i))), 6
    End If
End Function

Public Function getLength()
    getLength = Len(aQRData.getData())
End Function

Public Static Function getCode(c)

    If aQRUtil.toCharCode("0") <= c And c <= aQRUtil.toCharCode("9") Then
        getCode = c - aQRUtil.toCharCode("0")
        Exit Function
    ElseIf aQRUtil.toCharCode("A") <= c And c <= aQRUtil.toCharCode("Z") Then
        getCode = c - aQRUtil.toCharCode("A") + 10
    Else
        Select Case c
            Case aQRUtil.toCharCode(" ")
                getCode = 36
            Case aQRUtil.toCharCode("")
                getCode = 37
            Case aQRUtil.toCharCode("%")
                getCode = 38
            Case aQRUtil.toCharCode("*")
                getCode = 39
            Case aQRUtil.toCharCode("+")
                getCode = 40
            Case aQRUtil.toCharCode("-")
                getCode = 41
            Case aQRUtil.toCharCode(".")
                getCode = 42
            Case aQRUtil.toCharCode("/")
                getCode = 43
            Case aQRUtil.toCharCode(":")
                getCode = 44
            Case Else
                trigger_error "illegal char : c", E_USER_ERROR
        End Select
    End If

End Function


