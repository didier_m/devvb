VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmEditor 
   Caption         =   "Editeur de texte"
   ClientHeight    =   6165
   ClientLeft      =   60
   ClientTop       =   630
   ClientWidth     =   7710
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   411
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   514
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   1680
      Top             =   4200
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
   End
   Begin RichTextLib.RichTextBox RichTextBox1 
      Height          =   3375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7335
      _ExtentX        =   12938
      _ExtentY        =   5953
      _Version        =   393217
      ScrollBars      =   3
      AutoVerbMenu    =   -1  'True
      TextRTF         =   $"frmEditor.frx":0000
   End
   Begin VB.Menu mnuFichier 
      Caption         =   "&Fichier"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Ouvrir"
      End
      Begin VB.Menu sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Enregistrer"
      End
      Begin VB.Menu mnuSaveAs 
         Caption         =   "En&rgistrer sous"
      End
      Begin VB.Menu sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPrint 
         Caption         =   "&Imprimer"
      End
      Begin VB.Menu sep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuQuit 
         Caption         =   "&Quitter"
      End
   End
   Begin VB.Menu mnuEdition 
      Caption         =   "Edition"
      Begin VB.Menu mnuCut 
         Caption         =   "Couper"
      End
      Begin VB.Menu mnuCopy 
         Caption         =   "&Copier"
      End
      Begin VB.Menu mnuPaste 
         Caption         =   "Coller"
      End
      Begin VB.Menu sep4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFind 
         Caption         =   "Rechercher"
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mnuFormat 
      Caption         =   "Format"
      Begin VB.Menu mnuPolice 
         Caption         =   "&Police"
      End
   End
   Begin VB.Menu mnuApp 
      Caption         =   "?"
      Begin VB.Menu mnuApropos 
         Caption         =   "A propos de ..."
      End
   End
End
Attribute VB_Name = "frmEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public CurrentFile As String
Public IsSaved As Boolean
Dim FindWord As String

Private Sub Form_Load()

On Error Resume Next

Dim aLb As Libelle

'-> Par d�faut ne pas demander d'enregistrer les modifications
IsSaved = True
CurrentFile = ""

'-> Chargement des messprog
Set aLb = Libelles("FRMEDITOR")

Me.mnuFichier.Caption = aLb.GetCaption(1)
Me.mnuOpen.Caption = aLb.GetCaption(2)
Me.mnuSave.Caption = aLb.GetCaption(3)
Me.mnuSaveAs.Caption = aLb.GetCaption(4)
Me.mnuPrint.Caption = aLb.GetCaption(5)
Me.mnuQuit.Caption = aLb.GetCaption(6)
Me.mnuEdition.Caption = aLb.GetCaption(7)
Me.mnuCut.Caption = aLb.GetCaption(8)
Me.mnuCopy.Caption = aLb.GetCaption(9)
Me.mnuPaste.Caption = aLb.GetCaption(10)
Me.mnuFind.Caption = aLb.GetCaption(11)
Me.mnuApropos.Caption = aLb.GetCaption(12)
Me.mnuFormat.Caption = aLb.GetCaption(14)
Me.mnuPolice.Caption = aLb.GetCaption(15)

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

Dim Rep As VbMsgBoxResult

'-> Quitter si fichier en lecture seule
If Me.mnuEdition.Enabled = False Then Exit Sub

If Not IsSaved Then
    Rep = MsgBox("Enregistrer les modifications ?", vbQuestion + vbYesNoCancel, "Quitter")
    '-> Annuler la commande
    If Rep = vbCancel Then
        Cancel = 1
        Exit Sub
    End If
    
    '-> Quitter
    If Rep = vbNo Then Exit Sub
    
    '-> Lancer l'enregistrement
    If CurrentFile = "" Then
        mnuSaveAs_Click
    Else
        SaveFile
    End If
End If

End Sub

Private Sub Form_Resize()

Dim aRect As RECT

GetClientRect Hwnd, aRect
Me.RichTextBox1.Left = 0
Me.RichTextBox1.Top = 0
Me.RichTextBox1.width = aRect.Right
Me.RichTextBox1.height = aRect.Bottom


End Sub

Private Sub mnuApropos_Click()
    AproposDe.Show vbModal
End Sub

Private Sub mnuCopy_Click()

'-> Envoyer un message au Rtf pour gestion du press papier
SendMessage Me.RichTextBox1.Hwnd, WM_COPY, 0, 0

End Sub

Private Sub mnuCut_Click()

''-> Charger dans le press papier
'Clipboard.SetText Me.RichTextBox1.SelText
'
''-> Supprimer de l'�diteur
'Me.RichTextBox1.SelText = ""

SendMessage Me.RichTextBox1.Hwnd, WM_CUT, 0, 0

End Sub

Private Sub mnuFind_Click()

Dim Rep As String
Dim aLb As Libelle
Dim lblString As String
Dim TitreString As String

On Error Resume Next

'-> Postionner
lblString = "Rechercher : "
TitreString = "Recherche"

'-> Pointer sur la classe message
Set aLb = Libelles("FRMEDITOR")
lblString = aLb.GetCaption(13)
TitreString = aLb.GetCaption(11)



Rep = InputBox(lblString, TitreString, "")
If Rep = "" Then Exit Sub

'-> Stocker le mot de recherche
FindWord = Rep

'-> chercher
Me.RichTextBox1.find Rep


End Sub

Private Sub mnuOpen_Click()

Dim Rep As VbMsgBoxResult

On Error GoTo GestError

'-> Tester si un fichier est ouvert
If CurrentFile <> "" And Not IsSaved Then
    '-> Demander l'enregistrement du fichier Oui, Non , Annuler
    Rep = MsgBox("Enregistrer les modifications ?", vbQuestion + vbYesNoCancel, "Enregistrer")
    Select Case Rep
        Case vbYes
            SaveFile
        Case vbNo
        Case vbCancel
            Exit Sub
    End Select
End If

Me.CommonDialog1.FileName = "*.*"
Me.CommonDialog1.DefaultExt = "*.*"
Me.CommonDialog1.Filter = "Tous les fichiers (*.*)|*.*"

'-> Flags d'ouverture
Me.CommonDialog1.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly Or _
                   cdlOFNPathMustExist

'-> Afficher la feuille
Me.CommonDialog1.ShowOpen

'-> Quitter si nom de fichier = ""
If Me.CommonDialog1.FileName = "" Then Exit Sub

'-> Afficher le fichier dans l'�diteur
Me.RichTextBox1.LoadFile Me.CommonDialog1.FileName

'-> Indiquer qu'on ne doit pas enregistr�
IsSaved = True

'-> Indiquer le nom du fichier
CurrentFile = Me.CommonDialog1.FileName
Me.Caption = CurrentFile

Exit Sub

GestError:
    If Err.Number <> 32755 Then
        MsgBox "Impossible d'ouvrir le fichier sp�cifi�", vbCritical + vbOKOnly, "Erreur"
    End If
    
End Sub


Private Sub SaveFile()

'---> Cette fonction lance l'enregistrement du fichier modifi�

On Error GoTo GestError

Me.RichTextBox1.SaveFile CurrentFile, 1

Exit Sub

GestError:

    MsgBox "Impossible d'enregistrer le fichier.", vbCritical + vbOKOnly, "Erreur"

End Sub

Public Sub setText(strValue As String)
    '-> on ajoute du text
    Me.RichTextBox1.text = Me.RichTextBox1.text & strValue
End Sub

Private Sub mnuPaste_Click()

'Dim Temp As String
'
''-> Charger dans le press papier
'Temp = Clipboard.GetText(1)
'
'Me.RichTextBox1.SetFocus
'
'Me.RichTextBox1.SelText = Temp

SendMessage Me.RichTextBox1.Hwnd, WM_PASTE, 0, 0

End Sub

Private Sub mnuPolice_Click()

'---> Afficher la boite de dialogue commune

On Error GoTo GestError


Me.CommonDialog1.FontName = Me.RichTextBox1.Font.Name
Me.CommonDialog1.FontBold = Me.RichTextBox1.Font.Bold
Me.CommonDialog1.FontItalic = Me.RichTextBox1.Font.Italic
Me.CommonDialog1.FontSize = Me.RichTextBox1.Font.size
Me.CommonDialog1.Flags = cdlCFBoth
Me.CommonDialog1.ShowFont

Me.RichTextBox1.Font.Name = Me.CommonDialog1.FontName
Me.RichTextBox1.Font.Bold = Me.CommonDialog1.FontBold
Me.RichTextBox1.Font.Italic = Me.CommonDialog1.FontItalic
Me.RichTextBox1.Font.size = Me.CommonDialog1.FontSize

Exit Sub

GestError:

End Sub

Private Sub mnuPrint_Click()

'---> Cette proc�dure envoie l'impression du contenu

On Error GoTo GestError

'--> Quitter si pas de fichier
If Trim(Me.RichTextBox1.text) = "" Then
    MsgBox "Pas de document � imprimer", vbCritical + vbOKOnly, "Erreur"
    Exit Sub
End If

'-> Afficher la boite d'impression
Me.CommonDialog1.Flags = cdlPDReturnDC Or cdlPDNoSelection
Me.CommonDialog1.ShowPrinter

'-> Lander l'impression
Me.RichTextBox1.SelPrint Me.CommonDialog1.hdc

Exit Sub

GestError:
    If Err.Number = 32755 Then Exit Sub

End Sub

Private Sub mnuQuit_Click()

Unload Me

End Sub

Private Sub mnuSave_Click()

If CurrentFile = "" Then
    mnuSaveAs_Click
Else
    SaveFile
End If

End Sub

Private Sub mnuSaveAs_Click()

'---> Demander l'enregistrement

Dim Rep  As VbMsgBoxResult

On Error GoTo GestError

Me.CommonDialog1.FileName = Entry(NumEntries(CurrentFile, "\"), CurrentFile, "\")
Me.CommonDialog1.DefaultExt = ""
Me.CommonDialog1.Filter = "Tous les fichiers (*.*)|*.*"

'-> Flags d'ouverture
Me.CommonDialog1.Flags = cdlOFNHideReadOnly Or cdlOFNPathMustExist

'-> Afficher la boite de dialogue
Me.CommonDialog1.ShowSave

'-> V�rifier que le fichier n'existe pas
If Dir$(Me.CommonDialog1.FileName, vbNormal) <> "" Then
    Rep = MsgBox("Le fichier sp�cifi� : " & Me.CommonDialog1.FileName & " existe d�ja. Ecraser le fichier", vbExclamation + vbYesNoCancel, "Confirmation")
    If Rep = vbCancel Or Rep = vbNo Then Exit Sub
    '-> Supprimer le fichier sp�cifi�
    Kill Me.CommonDialog1.FileName
End If

'-> Enregistrer le fichier
Me.RichTextBox1.SaveFile Me.CommonDialog1.FileName

'-> Setting de la variable de fichier en cours
CurrentFile = Me.CommonDialog1.FileName

'-> Changer le caption de la fen�tre
Me.Caption = CurrentFile

'-> Indiquer que cela est enregistr�
IsSaved = True

'-> Quitter la fonction
Exit Sub

GestError:

    If Err.Number = 32755 Then Exit Sub
    
    MsgBox "Impossible d'enregistrer le fichier", vbCritical + vbOKOnly, "Erreur"

End Sub

Private Sub RichTextBox1_Change()

IsSaved = False

End Sub

Private Sub RichTextBox1_KeyDown(KeyCode As Integer, Shift As Integer)

'-> Quitter si pas F3
If KeyCode <> 114 Then Exit Sub

'-> Quitter si pas de recherche
If FindWord = "" Then Exit Sub

'-> Lancer la recherche
If Me.RichTextBox1.SelLength <> 0 Then
    Me.RichTextBox1.SelStart = Me.RichTextBox1.SelStart + Me.RichTextBox1.SelLength
End If
Me.RichTextBox1.find FindWord, Me.RichTextBox1.SelStart


End Sub
