VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "mscomctl.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form frmOpenOffice 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Export vers Excel"
   ClientHeight    =   6270
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5730
   Icon            =   "frmOpenOffice.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6270
   ScaleWidth      =   5730
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command3 
      Height          =   375
      Left            =   5160
      Picture         =   "frmOpenOffice.frx":372D
      Style           =   1  'Graphical
      TabIndex        =   43
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   4680
      Picture         =   "frmOpenOffice.frx":438F
      Style           =   1  'Graphical
      TabIndex        =   42
      Top             =   0
      Width           =   375
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   6015
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   5480
      _ExtentX        =   9657
      _ExtentY        =   10610
      _Version        =   393216
      Style           =   1
      Tabs            =   4
      Tab             =   2
      TabsPerRow      =   4
      TabHeight       =   520
      WordWrap        =   0   'False
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "frmOpenOffice.frx":4919
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame3"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Image3"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Image1"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "frmOpenOffice.frx":4935
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).Control(1)=   "ImageList1"
      Tab(1).Control(2)=   "TreeView1"
      Tab(1).Control(3)=   "Label5"
      Tab(1).ControlCount=   4
      TabCaption(2)   =   "Tab 2"
      TabPicture(2)   =   "frmOpenOffice.frx":4951
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "Frame4"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "Frame5"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "Frame6"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "Frame8"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).ControlCount=   4
      TabCaption(3)   =   "Tab 3"
      TabPicture(3)   =   "frmOpenOffice.frx":496D
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Command2"
      Tab(3).Control(1)=   "Frame7"
      Tab(3).Control(2)=   "Check7"
      Tab(3).ControlCount=   3
      Begin VB.CheckBox Check7 
         Height          =   255
         Left            =   -74760
         TabIndex        =   39
         Top             =   5400
         Value           =   1  'Checked
         Width           =   2055
      End
      Begin VB.Frame Frame7 
         Height          =   4815
         Left            =   -74760
         TabIndex        =   36
         Top             =   480
         Width           =   4935
         Begin VB.PictureBox Picture1 
            AutoRedraw      =   -1  'True
            Height          =   255
            Left            =   1560
            ScaleHeight     =   195
            ScaleWidth      =   3075
            TabIndex        =   37
            Top             =   4440
            Width           =   3135
         End
         Begin MSComctlLib.TreeView TreeView2 
            Height          =   3975
            Left            =   240
            TabIndex        =   38
            Top             =   360
            Width           =   4455
            _ExtentX        =   7858
            _ExtentY        =   7011
            _Version        =   393217
            LabelEdit       =   1
            Style           =   7
            ImageList       =   "ImageList1"
            Appearance      =   1
            Enabled         =   0   'False
         End
         Begin VB.Label Label12 
            Height          =   255
            Left            =   240
            TabIndex        =   41
            Top             =   4440
            Width           =   1215
         End
      End
      Begin VB.Frame Frame8 
         Height          =   855
         Left            =   240
         TabIndex        =   32
         Top             =   2520
         Width           =   4935
         Begin VB.TextBox Text5 
            Enabled         =   0   'False
            Height          =   285
            Left            =   240
            TabIndex        =   34
            Top             =   360
            Width           =   3015
         End
         Begin VB.CheckBox Check4 
            Enabled         =   0   'False
            Height          =   255
            Left            =   120
            TabIndex        =   33
            Top             =   0
            Width           =   3015
         End
      End
      Begin VB.Frame Frame6 
         Height          =   855
         Left            =   240
         TabIndex        =   29
         Top             =   4560
         Width           =   4935
         Begin VB.TextBox Text4 
            Height          =   285
            Left            =   240
            TabIndex        =   31
            Top             =   360
            Width           =   3015
         End
         Begin VB.CheckBox Check3 
            Height          =   255
            Left            =   120
            TabIndex        =   30
            Top             =   0
            Width           =   2535
         End
      End
      Begin VB.Frame Frame5 
         Height          =   975
         Left            =   240
         TabIndex        =   27
         Top             =   3480
         Width           =   4935
         Begin VB.CheckBox Check2 
            Enabled         =   0   'False
            Height          =   495
            Left            =   120
            TabIndex        =   35
            Top             =   0
            Width           =   3375
         End
         Begin VB.TextBox Text3 
            Enabled         =   0   'False
            Height          =   285
            Left            =   240
            TabIndex        =   28
            Top             =   600
            Width           =   3015
         End
      End
      Begin VB.Frame Frame4 
         Height          =   1935
         Left            =   240
         TabIndex        =   24
         Top             =   480
         Width           =   4935
         Begin VB.CheckBox Check1 
            Height          =   255
            Left            =   120
            TabIndex        =   25
            Top             =   0
            Width           =   2655
         End
         Begin MSComDlg.CommonDialog CommonDialog1 
            Left            =   3120
            Top             =   1320
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   393216
         End
         Begin VB.Image Image2 
            Height          =   480
            Left            =   4320
            Picture         =   "frmOpenOffice.frx":4989
            Top             =   1320
            Width           =   480
         End
         Begin VB.Label Label4 
            Height          =   855
            Left            =   120
            TabIndex        =   26
            Top             =   360
            Width           =   4575
         End
      End
      Begin VB.Frame Frame1 
         Enabled         =   0   'False
         Height          =   2415
         Left            =   -74760
         TabIndex        =   15
         Top             =   3480
         Width           =   4935
         Begin VB.CheckBox Check6 
            Height          =   255
            Left            =   240
            TabIndex        =   19
            Top             =   1920
            Width           =   2775
         End
         Begin VB.CheckBox Check5 
            Height          =   255
            Left            =   240
            TabIndex        =   18
            Top             =   1560
            Width           =   2775
         End
         Begin VB.Label Label11 
            Height          =   255
            Left            =   1320
            TabIndex        =   23
            Top             =   1080
            Width           =   855
         End
         Begin VB.Label Label10 
            Height          =   255
            Left            =   240
            TabIndex        =   22
            Top             =   1080
            Width           =   615
         End
         Begin VB.Label Label9 
            Height          =   255
            Left            =   1320
            TabIndex        =   21
            Top             =   720
            Width           =   3375
         End
         Begin VB.Label Label8 
            Height          =   255
            Left            =   240
            TabIndex        =   20
            Top             =   720
            Width           =   735
         End
         Begin VB.Label Label7 
            Height          =   255
            Left            =   1320
            TabIndex        =   17
            Top             =   360
            Width           =   3375
         End
         Begin VB.Label Label6 
            Height          =   255
            Left            =   240
            TabIndex        =   16
            Top             =   360
            Width           =   855
         End
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   -71400
         Top             =   720
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   3
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpenOffice.frx":6D5B
               Key             =   "SELECTED"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpenOffice.frx":6EB5
               Key             =   "UNSELECTED"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpenOffice.frx":700F
               Key             =   "TYPE"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.TreeView TreeView1 
         Height          =   2415
         Left            =   -74760
         TabIndex        =   14
         Top             =   960
         Width           =   4935
         _ExtentX        =   8705
         _ExtentY        =   4260
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin VB.CommandButton Command2 
         Height          =   375
         Left            =   -70920
         TabIndex        =   12
         Top             =   5400
         Width           =   1095
      End
      Begin VB.Frame Frame2 
         Height          =   1455
         Left            =   -74760
         TabIndex        =   0
         Top             =   720
         Width           =   4275
         Begin VB.TextBox Text2 
            Height          =   285
            Left            =   2760
            TabIndex        =   5
            Top             =   720
            Width           =   495
         End
         Begin VB.TextBox Text1 
            Height          =   285
            Left            =   1800
            TabIndex        =   4
            Top             =   720
            Width           =   495
         End
         Begin VB.OptionButton Option1 
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   2
            Top             =   360
            Value           =   -1  'True
            Width           =   2295
         End
         Begin VB.OptionButton Option1 
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   3
            Top             =   720
            Width           =   1095
         End
         Begin VB.OptionButton Option1 
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   6
            Top             =   1080
            Width           =   2295
         End
         Begin VB.Label Label3 
            Height          =   255
            Left            =   2400
            TabIndex        =   11
            Top             =   720
            Width           =   255
         End
         Begin VB.Label Label2 
            Height          =   255
            Left            =   1320
            TabIndex        =   10
            Top             =   720
            Width           =   375
         End
      End
      Begin VB.Frame Frame3 
         Height          =   1455
         Left            =   -74760
         TabIndex        =   1
         Top             =   2280
         Width           =   4335
         Begin VB.OptionButton Option8 
            Height          =   195
            Index           =   0
            Left            =   240
            TabIndex        =   7
            Top             =   360
            Value           =   -1  'True
            Width           =   3735
         End
         Begin VB.OptionButton Option8 
            Height          =   435
            Index           =   1
            Left            =   240
            TabIndex        =   8
            Top             =   720
            Width           =   3975
         End
      End
      Begin VB.Label Label1 
         Height          =   735
         Left            =   -74040
         TabIndex        =   40
         Top             =   4080
         Width           =   3495
      End
      Begin VB.Image Image3 
         Height          =   480
         Left            =   -74760
         Picture         =   "frmOpenOffice.frx":7169
         Top             =   4080
         Width           =   480
      End
      Begin VB.Label Label5 
         Height          =   255
         Left            =   -74760
         TabIndex        =   13
         Top             =   600
         Width           =   2535
      End
      Begin VB.Image Image1 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   -70320
         Picture         =   "frmOpenOffice.frx":BECB
         Top             =   960
         Width           =   480
      End
   End
End
Attribute VB_Name = "frmOpenOffice"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'-> Indique le fichier en cours de traitement
Public FichierName As String

'-> Indique la cl� du spool actuel
Public SpoolKey As String

'-> Variables de travail
Dim SelectSpool As Integer
Dim SelectSheet As Integer

'-> Pour export vers Excel
Dim ColDep As Integer
Dim RowDep As Integer
Dim AdresseRange As String
Dim pClasseurRef As Boolean
Dim pNomFeuille As Boolean
Dim pCelluleDep As Boolean
Dim pName As Boolean
Dim aSpreadSheet As Object
Dim aDesktop As Object
Dim XPos As Integer
Dim YPos As Integer

Dim TempFileName As String
Dim hdlFile As Integer


Private Function GetBlockPtr() As Block

Dim aTb As Tableau
Dim aBlock As Block
Dim aFichier As Fichier
Dim aSpool As Spool

'-> Pointer sur l'objet fichier
Set aFichier = Fichiers(Me.FichierName)
'-> Pointer sur le spool sp�cifi�
Set aSpool = aFichier.Spools(Me.SpoolKey)
'-> Pointer sur le tableau
Set aTb = aSpool.maquette.Tableaux(UCase$(Trim(Me.Label7.Caption)))
'-> Pointer sur le block
Set aBlock = aTb.Blocks(UCase$(Trim(Me.Label9.Caption)))

Set GetBlockPtr = aBlock

End Function

Private Sub Check5_Click()

Dim aNode As Node
Dim aBlock As Block

'-> Get d'un pointeur vers le block
Set aBlock = GetBlockPtr()

'-> Mettre � jour la propri�t�
aBlock.SetExportExcel CInt(Me.Label11.Caption), CBool(Me.Check5.Value)

'-> D�cocher la propri�t� d'export unique si on n'exporte pas
If Not CBool(Me.Check5.Value) Then
    aBlock.SetExportUniqueExcel CInt(Me.Label11.Caption), False
    '-> D�bloquer la propri�t�
    Me.Check6.Value = 0
End If

'-> Mettre � jour l'icone
Set aNode = Me.TreeView1.Nodes(Me.Label7.Caption & "�" & Me.Label9.Caption & "�" & Me.Label11.Caption)
If CBool(Me.Check5.Value) Then
    aNode.image = "SELECTED"
Else
    aNode.image = "UNSELECTED"
End If


End Sub

Private Sub Check6_Click()

Dim aBlock As Block

'-> Get d'un pointeur vers le blokc
Set aBlock = GetBlockPtr

'-> Modifier sa propri�t�
aBlock.SetExportUniqueExcel CInt(Me.Label11.Caption), CBool(Me.Check6.Value)


End Sub

Public Sub Init()

Dim aNode As Node
Dim aNode2 As Node
Dim aNode3 As Node
Dim aTb As Tableau
Dim aBlock As Block
Dim aFichier As Fichier
Dim aSpool As Spool
Dim i As Integer
Dim aLb As Libelle

'-> Pointer sur la gestion des messages
Set aLb = Libelles("FRMOPENOFFICE")

'-> Par d�faut on exporte le spool en entier
SelectSpool = 1

'-> Pointer sur l'objet Fichier
Set aFichier = Fichiers(Me.FichierName)

'-> Pointer sur le spool sp�cifi�
Set aSpool = aFichier.Spools(Me.SpoolKey)

'-> Charger la liste de tous les block de donn�es dans le treeview
For Each aTb In aSpool.maquette.Tableaux
    '-> Ajouter le node du tableau
    Set aNode = Me.TreeView1.Nodes.add(, , UCase$(Trim(aTb.Nom)), aLb.GetCaption(9) & aTb.Nom, "TYPE")
    aNode.Tag = "TB"
    aNode.Expanded = True
    '-> Pour tous les blocks dans ce tableau
    For Each aBlock In aTb.Blocks
        '-> Ajouter le node du block
        Set aNode2 = Me.TreeView1.Nodes.add(aNode.Key, 4, aNode.Key & "�BL-" & aBlock.Nom, aBlock.Nom, "TYPE")
        aNode2.Tag = "BLOCK"
        aNode2.Expanded = True
        '-> Ajouter un node par ligne
        For i = 1 To aBlock.NbLigne
            Set aNode3 = Me.TreeView1.Nodes.add(aNode2.Key, 4, aNode2.Key & "�" & i, aLb.GetCaption(11) & " -> " & i)
            If aBlock.GetExportExcel(i) Then
                aNode3.image = "SELECTED"
            Else
                aNode3.image = "UNSELECTED"
            End If
            aNode3.Tag = "LIG"
        Next
    Next 'Pour tous les blocks du tableau
Next 'Pour tous les tableaux de la maquette

End Sub

Private Sub Command1_Click()

'--> on recharge une vue enregistree
frmVue.maquette = Fichiers(Me.FichierName).Spools(Me.SpoolKey).maquette.Nom
iniName = "TurboVue.ini"
frmVue.Show vbModal
If strRetour <> "" Then VueLoad (strRetour)

End Sub

Private Sub Command2_Click()

Dim aFichier As Fichier
Dim aSpool As Spool
Dim aLb As Libelle

'-> Pointer sur la gestion des messages
Set aLb = Libelles("FRMOPENOFFICE")

'-> si page mini s�lectionn�e v�rifier les zones
If Me.Option1(2).Value Then
    If Trim(Me.Text1.text) = "" Then
        MsgBox aLb.GetCaption(21), vbCritical + vbOKOnly, aLb.GetCaption(23)
        Me.Text1.SetFocus
        Exit Sub
    End If
            
    '-> si page mini s�lectionn�e v�rifier les zones
    If Trim(Me.Text2.text) = "" Then
        MsgBox aLb.GetCaption(21), vbCritical + vbOKOnly, aLb.GetCaption(23)
        Me.Text2.SetFocus
        Exit Sub
    End If
    
    '-> Tester que borne mini <= borne maxi
    If CLng(Me.Text1.text) > CLng(Me.Text2.text) Then
        MsgBox aLb.GetCaption(22), vbCritical + vbOKOnly, aLb.GetCaption(23)
        Me.Text1.SetFocus
        Exit Sub
    End If
End If

'-> Initialisation des valeurs
pClasseurRef = False
pCelluleDep = False
pName = False
pNomFeuille = False
ColDep = 0
RowDep = 0
XPos = 0
YPos = 0

'-> Si on utilise un classeur de r�f�rence
If Me.Check1.Value = 1 Then
    '-> V�rifier que l'on ait bien saisi un nom de classeur
    If Trim(Me.Label4.Caption) = "" Then
        MsgBox aLb.GetCaption(24), vbExclamation + vbOKOnly, aLb.GetCaption(23)
        Exit Sub
    End If
    
    '-> V�rifier que le classeur sp�cifi� existe bien
    If Dir$(Me.Label4.Caption) = "" Then
        MsgBox aLb.GetCaption(25) & Me.Label4.Caption, vbExclamation + vbOKOnly, aLb.GetCaption(23)
        Exit Sub
    End If
    
    '-> On peut utiliser un classeur de r�f�rence
    pClasseurRef = True
    
End If

'-> Donner un nom � la feuille
If Me.Check4.Value = 1 Then
    '-> V�rifi� que l'on ait bien saisi le nom de la feuille
    If Trim(Me.Text5.text) = "" Then
        MsgBox aLb.GetCaption(26), vbExclamation + vbOKOnly, aLb.GetCaption(23)
        Me.Text5.SetFocus
        Exit Sub
    End If
    
    '-> Nommer la feuille
    pNomFeuille = True
    
End If

'-> Plage de donn�es
If Me.Check2.Value = 1 Then
    '-> V�rifier que l'on ait bien saisi un nom pour l aplage de donn�es
    If Trim(Me.Text3.text) = "" Then
        MsgBox aLb.GetCaption(27), vbExclamation + vbOKOnly, aLb.GetCaption(27)
        Me.Text3.SetFocus
        Exit Sub
    End If
    
    '-> Ok on nomme la plage de donn�es
    pName = True

End If

'-> Cellule de d�part
If Me.Check3.Value = 1 Then
    '-> V�rifier que l'on ait saisi la cellule de d�part
    If Trim(Me.Text4.text) = "" Then
        MsgBox aLb.GetCaption(28), vbExclamation + vbOKOnly, aLb.GetCaption(23)
        Me.Text4.SetFocus
        Exit Sub
    End If
    '-> Sp�cifier la cellule de d�part
    pCelluleDep = True
End If

'-> Si on exporte tout dans une feuille, il faut donner un nom
If SelectSheet = 1 Then
    If Me.Check4.Value = 0 Then
        MsgBox aLb.GetCaption(29), vbExclamation + vbOKOnly, aLb.GetCaption(23)
        If Me.Check4.Enabled Then Me.Check4.SetFocus
        Exit Sub
    End If
End If 'Si on exporte tout dans une feuille


'-> Bloquer le bouton OK
Me.Enabled = False
Me.Command2.Enabled = False

'-> Envoyer l'export
ExportWithFormat

'-> decharger les objets
Set aSpreadSheet = Nothing
Set aDesktop = Nothing
'-> D�charger la feuille
Unload Me

End Sub

Private Sub Command3_Click()

'--> on lance l'enregistrement des vues
iniName = "TurboVue.ini"
frmVue.Show vbModal
If strRetour <> "" Then VueSave (strRetour)
End Sub

Private Sub Form_Load()

On Error Resume Next

Dim aLb As Libelle

'-> Mise � jour des libelles

Set aLb = Libelles("FRMOPENOFFICE")
Me.Caption = aLb.GetCaption(1)
Me.SSTab1.TabCaption(0) = aLb.GetCaption(35)
Me.SSTab1.TabCaption(1) = aLb.GetCaption(7)
Me.SSTab1.TabCaption(2) = aLb.GetCaption(13)
Me.SSTab1.TabCaption(3) = aLb.GetCaption(18)
Me.Frame2.Caption = aLb.GetCaption(2)
Me.Frame3.Caption = aLb.GetCaption(3)
Me.Option8(0).Caption = aLb.GetCaption(4)
Me.Option8(1).Caption = aLb.GetCaption(5)
Me.Label1.Caption = aLb.GetCaption(6)
Me.Frame1.Caption = aLb.GetCaption(8)
Me.Label6.Caption = aLb.GetCaption(9)
Me.Label8.Caption = aLb.GetCaption(10)
Me.Label10.Caption = aLb.GetCaption(11)
Me.Check6.Caption = aLb.GetCaption(12)
Me.Check1.Caption = aLb.GetCaption(14)
Me.Check4.Caption = aLb.GetCaption(15)
Me.Check2.Caption = aLb.GetCaption(16)
Me.Check3.Caption = aLb.GetCaption(17)
Me.Frame7.Caption = aLb.GetCaption(19)
Me.Check7.Caption = aLb.GetCaption(20)
Me.Option1(1).Caption = aLb.GetCaption(36)
Me.Label5.Caption = aLb.GetCaption(37)
Me.Check5.Caption = aLb.GetCaption(1)

Set aLb = Libelles("FRMPRINTVISU")
Me.Option1(2).Caption = aLb.GetCaption(5)
Me.Option1(3).Caption = aLb.GetCaption(8)
Me.Label2.Caption = aLb.GetCaption(6)
Me.Label3.Caption = aLb.GetCaption(7)

Set aLb = Libelles("BOUTONS")
Me.Command2.Caption = aLb.GetCaption(1)



End Sub

Private Sub Image2_DblClick()

'---> Afficher la boite de dialogue ouvrir
Dim aLb As Libelle

On Error GoTo EndError

'-> Pointer sur la gestion des erreurs
Set aLb = Libelles("FRMEXCEL")

'-> Setting des param�tres de la boite de dialogue
Me.CommonDialog1.CancelError = True
Me.CommonDialog1.DialogTitle = aLb.GetCaption(14)
Me.CommonDialog1.Filter = aLb.GetCaption(30) & "( *.ods)|*.ods|" & aLb.GetCaption(31) & "( *.odt)|*.odt"
Me.CommonDialog1.FilterIndex = 1
Me.CommonDialog1.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly Or cdlOFNPathMustExist
Me.CommonDialog1.ShowOpen

'-> Mise � jour du label
Me.Label4.Caption = Me.CommonDialog1.FileName

'-> Cocher la case
Me.Check1.Value = 1

EndError:

End Sub

Private Sub Option1_Click(index As Integer)
SelectSpool = index
End Sub


Private Sub Option8_Click(index As Integer)

'-> Indique la s�lection
SelectSheet = index

Me.Check4.Enabled = CBool(index)
Me.Check2.Enabled = CBool(index)
Me.Text3.Enabled = CBool(index)
Me.Text5.Enabled = CBool(index)
Me.Check4.Value = index
If index = 0 Then
    Me.Text5.text = ""
    Me.Text3.text = ""
    Me.Check2.Value = index
End If


End Sub



Private Sub Text1_KeyPress(KeyAscii As Integer)

Select Case KeyAscii
    Case 8, 48 To 57
    Case Else
        KeyAscii = 0
End Select


End Sub


Private Sub Text2_KeyPress(KeyAscii As Integer)

Select Case KeyAscii
    Case 8, 48 To 57
    Case Else
        KeyAscii = 0
End Select

End Sub

Private Sub TreeView1_DblClick()
'---> Bloquer ou d�bloquer la gestion des prori�t�s

Dim aBlock As Block
Dim aNode As MSComctlLib.Node

For Each aNode In Me.TreeView1.Nodes
    If aNode.Selected Then
        Exit For
    End If
Next

If aNode Is Nothing Then Exit Sub

'-> Afficher le nom du tableau
Me.Label7.Caption = Entry(1, aNode.Key, "�")
Me.Label9.Caption = ""
Me.Label11.Caption = ""

If aNode.Tag = "LIG" Then
    '-> D�bloquer
    Me.Frame1.Enabled = True
    '-> Afficher le nom du block
    Me.Label9.Caption = Entry(2, aNode.Key, "�")
    '-> Afficher le num�ro de la ligne
    Me.Label11.Caption = Entry(3, aNode.Key, "�")
    '-> Pointer sur le block
    Set aBlock = GetBlockPtr
    '-> On inverse la propri�t�
    Me.Check5.Value = aBlock.GetExportExcel(CInt(Me.Label11.Caption)) * -1
    If Me.Check5.Value = 1 Then
        Me.Check5.Value = 0
    Else
        Me.Check5.Value = 1
    End If
    Check5_Click
    Me.Check6.Value = aBlock.GetExportUniqueExcel(CInt(Me.Label11.Caption)) * -1
ElseIf aNode.Tag = "BLOCK" Then
    '-> Afficher le nom du block
    Me.Label9.Caption = Entry(2, aNode.Key, "�")
    '-> Bloquer la saisie
    Me.Frame1.Enabled = False
End If

End Sub

Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)

'---> Bloquer ou d�bloquer la gestion des prori�t�s

Dim aBlock As Block

'-> Afficher le nom du tableau
Me.Label7.Caption = Entry(1, Node.Key, "�")
Me.Label9.Caption = ""
Me.Label11.Caption = ""

If Node.Tag = "LIG" Then
    '-> D�bloquer
    Me.Frame1.Enabled = True
    '-> Afficher le nom du block
    Me.Label9.Caption = Entry(2, Node.Key, "�")
    '-> Afficher le num�ro de la ligne
    Me.Label11.Caption = Entry(3, Node.Key, "�")
    '-> Pointer sur le block
    Set aBlock = GetBlockPtr
    '-> Afficher ses propri�t�s
    Me.Check5.Value = aBlock.GetExportExcel(CInt(Me.Label11.Caption)) * -1
    Me.Check6.Value = aBlock.GetExportUniqueExcel(CInt(Me.Label11.Caption)) * -1
ElseIf Node.Tag = "BLOCK" Then
    '-> Afficher le nom du block
    Me.Label9.Caption = Entry(2, Node.Key, "�")
    '-> Bloquer la saisie
    Me.Frame1.Enabled = False
End If

End Sub
Private Sub InitInterface(PageMin As Long, PageMax As Long)

Dim aNode As Node
Dim i As Integer
Dim aLb As Libelle

'-> Pointer sur la classe message
Set aLb = Libelles("FRMOPENOFFICE")

For i = PageMin To PageMax
    '-> Ajouter un node
    Me.TreeView2.Nodes.add , , "PAGE" & i, aLb.GetCaption(38) & " " & i, "UNSELECTED"
Next

End Sub

Private Sub InitPageDisplay(NbLigne As Long, Page As Long)

Dim aLb As Libelle

'-> Pointer sur la classe messprog
Set aLb = Libelles("FRMOPENOFFICE")

'-> vider le picture de temporisation
Me.Picture1.Cls

'-> Calcul de la taille total
TailleTotale = NbLigne
TailleLue = 0

'-> Modifier le libell�
Me.Label12.Caption = aLb.GetCaption(39)
DoEvents

'-> Setting de l'icone
Me.TreeView2.Nodes("PAGE" & Page).ForeColor = rgb(255, 0, 0)
Me.TreeView2.Nodes("PAGE" & Page).Bold = True
Me.TreeView2.Nodes("PAGE" & Page).EnsureVisible
DoEvents

End Sub



Private Sub ExportWithFormat()

'---> Cette proc�dure est charg�e d'exporter vers Excel

Dim PageMin As Long
Dim PageMax As Long
Dim aFichier As Fichier
Dim aSpool As Spool
Dim i As Long, j As Integer
Dim CreateSheet As Boolean
Dim Ligne As String
Dim DefPage As String
Dim NbLig As Long
Dim DelRef As String
Dim TypeObjet As String
Dim TypeSousObjet As String
Dim NomObjet As String
Dim NomSousObjet As String
Dim Param As String
Dim DataFields As String
Dim aBlock As Block
Dim aTb As Tableau
Dim SetNameFeuille As Boolean
Dim NbCol As Integer
Dim aLb As Libelle
Dim IsFormated As Boolean
Dim aServiceManager
Dim SpreadSheets
Dim objDocument
Dim oPropertyValue
Dim oWindow As Object

'-> Pointer sur la gestion des messages
Set aLb = Libelles("FRMOPENOFFICE")

'-> Indiquer si on imprime ou non le format
IsFormated = CBool(Me.Check7.Value)

'-> Pointer sur l'objet fichier
Set aFichier = Fichiers(Me.FichierName)
'-> Pointer sur le spool sp�cifi�
Set aSpool = aFichier.Spools(Me.SpoolKey)

'-> Il faut d�terminer la plage d'impression
Select Case SelectSpool
    Case 1
        PageMin = 1
        PageMax = aSpool.nbPage
    Case 2
        PageMin = CLng(Me.Text1.text)
        PageMax = CLng(Me.Text2.text)
    Case 3
        PageMin = aSpool.CurrentPage
        PageMax = aSpool.CurrentPage
End Select 'Selon la plage d'export

'-> Mettre � jour le treeview avec l'import en cours
InitInterface PageMin, PageMax

'-> D�terminer si on cr�er un onglet � chaque page
CreateSheet = Me.Option8(0).Value

'Lien vers le service manager de oppenoffice
Set aServiceManager = CreateObject("com.sun.star.ServiceManager")
Set oPropertyValue = aServiceManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue")
'Lien Vers le bureau
Set aDesktop = aServiceManager.createInstance("com.sun.star.frame.Desktop")

'-> Doit on cr�er un nouveau classeur ou une r�f�rence
If pClasseurRef Then
    '-> Ouvrir le classeur de r�f�rence
    Set objDocument = OpenClasseurRef()
    '-> Tester le retour
    If objDocument Is Nothing Then
        MsgBox aLb.GetCaption(32), vbCritical + vbOKOnly, aLb.GetCaption(23)
        Exit Sub
    End If
    'R�cup�rer la collection des feuille
    Set SpreadSheets = objDocument.getSheets()
    '-> Tester si on doit pointer sur une feuille
    If pNomFeuille Then
        '-> pointer sur la feuille
        For i = 0 To SpreadSheets.count - 1
            Set aSpreadSheet = SpreadSheets.getByindex(i) '(Trim(Me.Text5.Text))
            If aSpreadSheet.Name = Trim(Me.Text5.text) Then Exit For
        Next
    Else
        Set aSpreadSheet = SpreadSheets.getByindex(0)
        '-> donner un nom a la feuille
        If pClasseurRef Then
            aSpreadSheet.Name = aLb.GetCaption(38) & (SpreadSheets.count)
        Else
            aSpreadSheet.Name = aLb.GetCaption(38) & i
        End If
    End If
Else
    '-> Cr�er un nouveau classeur
    'Cr�er une nouvelle instance du tableau
    Dim args()
    Set objDocument = aDesktop.LoadComponentFromURL("private:factory/scalc", "_blank", 0, args)
    Set oWindow = objDocument.CurrentController.Frame.ContainerWindow
    oWindow.Visible = False
    
    'R�cup�rer la collection des feuille
    Set SpreadSheets = objDocument.getSheets()
    '-> si on passe pas par un classeur de reference
    If Not pClasseurRef Then
        '-> on supprime les feuilles par defaut
        For i = SpreadSheets.count - 1 To 1 Step -1
            SpreadSheets.removeByName SpreadSheets.getByindex(i).Name
        Next
    End If
    'R�cup�rer le premi�re feuille
    Set aSpreadSheet = SpreadSheets.getByindex(0)
    '-> Donner son nom � la feuille par d�faut
    SetNameFeuille = True
End If

'-> Initialiser la cellule de d�part
If pCelluleDep Then SetActiveCell

'-> Si on est en export non formatter, initialiser l'export vers le fichier ascii
If Not IsFormated Then
    '-> Init de l'export d'impression
    InitExportPageByAscii
End If

'-> Impression des pages s�lectionn�es
For i = PageMin To PageMax
    '-> Tester si on doit nommer la feuille
    If SetNameFeuille Then
        If pNomFeuille Then
            '-> Nommer la page
            aSpreadSheet.Name = Trim(Me.Text5.text)
        Else
            '-> Donner son num�ro � la page selon que classeur de reference ou pas - pierrot 15/06/2005
            If pClasseurRef Then
                aSpreadSheet.Name = aLb.GetCaption(38) & (SpreadSheets.count)
            Else
                aSpreadSheet.Name = aLb.GetCaption(38) & i
            End If
        End If
    End If 'si on doit nommer la feuille
    '-> R�cup�rer la d�finition de la page
    DefPage = aSpool.GetPage(i)
    '-> r�cup�rer le nombre de ligne des la page
    NbLig = NumEntries(DefPage, Chr(0))
    InitPageDisplay NbLig, i
    
    If Not pNomFeuille Then YPos = RowDep
    '-> Analyse de toutes les lignes de la page
    For j = 1 To NbLig
        '-> Lecture d'une ligne
        Ligne = Entry(j, DefPage, Chr(0))
        '-> Ne traiter que les lignes tableaux
        If Mid$(Trim(UCase$(Ligne)), 1, 3) <> "[TB" Then GoTo NextLig
        '-> Analyser la ligne
        AnalyseObj Ligne, TypeObjet, NomObjet, TypeSousObjet, NomSousObjet, Param, DataFields
        '-> initialiser les positions
        XPos = ColDep
        '-> Exporter la ligne
        ExportLig NomObjet, NomSousObjet, CInt(Entry(2, Param, "\")), DataFields, aSpreadSheet, aSpool.maquette
        YPos = YPos + 1
NextLig:
        '-> Dessin de la temporisation
        TailleLue = TailleLue + 1
        DrawTempo Me.Picture1
        DoEvents
    Next 'Pour toutes les lignes � imprimer

    '-> Si on doit cr�er une page � chaque fois
    If CreateSheet Then
        '-> Cas ou on est en export par RTF
        If Not IsFormated Then
            '-> Remonter le fichier ascii dans Excel
            CloseExportByAscii
        Else
            '-> Formatter la page en cours
            FormatPage aSpreadSheet, aSpool.maquette
        End If

         '-> Ajouter une nouvelle page
        If i <> PageMax Then
            '-> Cas de l'export sans format
            If Not IsFormated Then InitExportPageByAscii
            '-> on insere une nouvelle feuille
            SpreadSheets.insertNewByName aLb.GetCaption(38) & (SpreadSheets.count + 1), SpreadSheets.count
            Set aSpreadSheet = SpreadSheets.getByindex(SpreadSheets.count - 1)
            '-> Donner son num�ro � la page selon que classeur de reference ou pas - pierrot 15/06/2005
'            If pClasseurRef Then
'                aFeuille.Name = aLb.GetCaption(38) & (i + (aClasseur.Worksheets.Count - 1))
'            Else
'                aFeuille.Name = aLb.GetCaption(38) & i
'            End If
        End If

        '-> Initialiser la cellule de d�part
        If pCelluleDep Then SetActiveCell

    End If 'Si on doit cr�er un onglet � chaque page

    '-> La page est trait�e, modifier son icone
    Me.TreeView2.Nodes("PAGE" & i).image = "SELECTED"
    Me.TreeView2.Nodes("PAGE" & i).ForeColor = 0
    Me.TreeView2.Nodes("PAGE" & i).Bold = False
    DoEvents
Next 'Pour toutes les pages � imprimer
    
objDocument.CurrentController.Frame.ContainerWindow.Visible = True
End Sub
Private Function InitExportPageByAscii()

'-> Obtenir un nom de fichier temporaire
TempFileName = GetTempFileNameVB("ODS")

'-> Obtenir un handle de fichier
hdlFile = FreeFile
Open TempFileName For Output As #hdlFile


End Function

Private Function CloseExportByAscii()

'---> Cette proc�dure exporte vers Excel le fichier ascii sp�cifie

Dim SepFic As String
Dim FieldInfoString As String
Dim i As Long
Dim LineToAnalyse As String
Dim FieldInfoArray() As Variant

'-> Fermer le fichier ASCII
Close #hdlFile

'-> Le remonter dans le RTF
frmLib.Rtf(0).LoadFile TempFileName

'-> S�lectionner tout le text dans le RTF
frmLib.Rtf(0).SelStart = 0
frmLib.Rtf(0).SelLength = Len(frmLib.Rtf(0).text)

'-> Vider le press papier
Clipboard.Clear

'-> Copier dans le press papier
Clipboard.SetText frmLib.Rtf(0).SelText

'-> Coller dans Excel
'aSpreadSheet.Paste

'-> Eclater sur toutes les cellules
SepFic = "|"
'If ExcelApp.Selection.Columns.Count = 1 Then
'    '-> PIERROT 27/06/2005 : ne pas traiter ni convertir les pages blanches du spool sinon erreur
'    If Trim(frmLib.Rtf(0).Text) <> "" Then
'        '-> PIERROT : probleme de formattage dates / on cr�e un vecteur de formats correspondant au nombre de colonnes
'        '-> permet de ne plus avoir les dates en format americain
'
'        LineToAnalyse = Entry(2, frmLib.Rtf(0).Text, Chr(13) & Chr(10))
'
'        '-> on redimensionne le tableau
'        FieldInfoArray = Array(Array(1, 2), Array(2, 2), Array(3, 2), Array(4, 2), Array(5, 2), Array(6, 2), Array(7, 2), Array(8, 2), Array(9, 2), Array(10, 2), Array(11, 2), Array(12, 2), Array(13, 2), Array(14, 2), Array(15, 2), Array(16, 2), Array(17, 2), Array(18, 2), Array(19, 2), Array(20, 2), Array(21, 2), Array(22, 2), Array(23, 2), Array(24, 2), Array(25, 2), Array(26, 2), Array(27, 2), Array(28, 2), Array(29, 2), Array(30, 2), Array(41, 2), Array(42, 2), Array(43, 2), Array(44, 2), Array(45, 2), Array(46, 2), Array(47, 2), Array(48, 2), Array(49, 2), Array(50, 2))
'        'ReDim Preserve FieldInfoArray(NumEntries(LineToAnalyse, SepFic))
'
'        '-> On parcours le fichier RTF sur separateur "saut de ligne" pour analyser
''        For i = 1 To NumEntries(LineToAnalyse, SepFic)
''            '-> Determination du data type / on ne traite que les dates
''            If IsDate(Entry(i, LineToAnalyse, SepFic)) And (Len(Entry(i, LineToAnalyse, SepFic)) = 8 Or Len(Entry(i, LineToAnalyse, SepFic)) = 10) Then
''                '-> On compose le vecteur date
''                FieldInfoArray(i - 1)(1) = xlDMYFormat
''            Else
''                '-> On compose le vecteur date
''                FieldInfoArray(i - 1)(1) = xlDMYFormat 'xlGeneralFormat
''            End If
''        Next
'
'        '-> On envoie l'explode de la ligne
'        ExcelApp.Selection.TextToColumns DataType:=xlDelimited, _
'        ConsecutiveDelimiter:=False, Other:=True, OtherChar:=SepFic, fieldinfo:=FieldInfoArray
'
'    End If
'End If
'-> Supprimer le fichier ascii
If Dir$(TempFileName) <> "" Then Kill TempFileName

End Function

Private Function SetActiveCell() As Boolean
Dim aCellule As Object
Dim aRange As Object
On Error GoTo GestError

'-> Essayer de positionner la cellule active
Set aRange = aSpreadSheet.getCellRangeByName(Me.Text4.text)
'Set aCellule = aRange.getCellByPosition(0, 0)
ColDep = aRange.CellAddress.Column
RowDep = aRange.CellAddress.row
Exit Function

GestError:
    SetActiveCell = False

End Function

Public Function MakePropertyValue(cName, uValue) As Object
Dim oStruct, oServiceManager As Object
'-> permet de donner une propri�t�
Set oServiceManager = CreateObject("com.sun.star.ServiceManager")
Set oStruct = oServiceManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue")
oStruct.Name = cName
oStruct.Value = uValue
Set MakePropertyValue = oStruct
End Function

Private Function OpenClasseurRef() As Object

Dim aW As Object
Dim AdresseDoc As String
Dim OpenParam()

On Error GoTo GestError
'Set OpenParam(0) = MakePropertyValue("Hidden", True)
'Set OpenParam(1) = MakePropertyValue("ReadOnly", False)

AdresseDoc = Me.Label4.Caption
'-> Essayer d'ouvrir le classeur sp�cifi�
Set aW = aDesktop.LoadComponentFromURL(ConvertToUrl(AdresseDoc), "_blank", 0, OpenParam)
'-> Renvoyer la r�f�rence
Set OpenClasseurRef = aW

'-> on masque le classeur
aW.CurrentController.Frame.ContainerWindow.Visible = False
Exit Function

GestError:
        Set OpenClasseurRef = Nothing

End Function

Private Function ConvertToUrl(strFile) As String
    '-> fonction pour transformer le chemin du fichier
    strFile = Replace(strFile, "\", "/")
    strFile = Replace(strFile, ":", "|")
    strFile = Replace(strFile, " ", "%20")
    strFile = "file:///" + strFile
    ConvertToUrl = strFile
End Function

Private Sub FormatPage(aFeuille, aMaq As maquette)

'---> Cette proc�dure formatte les cellules d'une feuille

Dim aTb As Tableau
Dim aBlock As Block
Dim aCell As Cellule
Dim i As Integer, j As Integer, k As Integer, l As Integer, m As Integer
Dim aRange As Object
Dim aCol As Object
Dim aLb As Libelle
Dim TempZone As String

'On Error GoTo GestError

'-> Pointer sur la classe libell�
Set aLb = Libelles("FRMOPENOFFICE")

'-> vider le picture de temporisation
Me.Picture1.Cls

'-> Calcul de la taille total
TailleTotale = 0
TailleLue = 0
For Each aTb In aMaq.Tableaux
    For Each aBlock In aTb.Blocks
        For i = 1 To aBlock.NbLigne
            If aBlock.GetExportExcel(i) Then
                '-> Formatter toutes les cellules de cette ligne
                For j = 1 To aBlock.NbCol
                    TailleTotale = TailleTotale + 1
                Next
            End If
        Next
    Next
Next

'-> Modifier le libell�
Me.Label12.Caption = aLb.GetCaption(40)
DoEvents

'-> Analyse de tous les tableaux
For Each aTb In aMaq.Tableaux
    '-> Analyse de tous les blocks
    For Each aBlock In aTb.Blocks
        '-> Pour toutes les lignes
        For i = 1 To aBlock.NbLigne
            '-> Tester si on exporte la ligne lvers Excel
            If aBlock.GetExportExcel(i) Then
                '-> Formatter toutes les cellules de cette ligne
                For j = 1 To aBlock.NbCol
                    If aBlock.Cellules("L" & i & "C" & j).ListeCell <> "" Then
                        l = 0
                        TempZone = ""
                        For k = 1 To NumEntries(aBlock.Cellules("L" & i & "C" & j).ListeCell, ";")
                            '-> Incr�menter le compteur de ligne
                            l = l + 1
                            '-> Get de l'entry associ�
                            If TempZone = "" Then
                                TempZone = Entry(k, aBlock.Cellules("L" & i & "C" & j).ListeCell, ";")
                            Else
                                TempZone = TempZone & ";" & Entry(k, aBlock.Cellules("L" & i & "C" & j).ListeCell, ";")
                            End If
                        Next
                        '-> il faut formatter
                        If TempZone <> "" Then
                            For m = 1 To NumEntries(TempZone, ";")
                                '-> Pointer sur la zone
                                Set aRange = aFeuille.getCellByPosition(CInt(Entry(1, Entry(m, TempZone, ";"), ",")), CInt(Entry(2, Entry(m, TempZone, ";"), ",")))
                                '-> Formatter
                                SetFormat aBlock.Cellules("L" & i & "C" & j).aRange, aRange
                                DoEvents
                            Next
                        End If 'S'il reste des zones � formatter
                    End If 'Sil y a des cellules � formatter
                    '-> Vider la liste des cellules
                    aBlock.Cellules("L" & i & "C" & j).ListeCell = ""
                    '-> Gestion de la temporisation
                    TailleLue = TailleLue + 1
                    DrawTempo Me.Picture1
                Next
            End If 'Si on exporte la ligne vers Excel
        Next 'Pour toutes les lignes
    Next 'Pour tous les blocks
Next 'Pour tous les tableaux

'-> Donner une largeur Automatique � toutes les colonnes
For i = 0 To aFeuille.columns.count - 1
    Set aCol = aFeuille.columns(i)
    aCol.OptimalWidth = True
Next

GestError:
'-> on vide
Set aRange = Nothing
Set aTb = Nothing
Set aBlock = Nothing
Set aCell = Nothing
Set aRange = Nothing
Set aCol = Nothing
Set aLb = Nothing

End Sub

Private Sub ExportLig(Tableau As String, Block As String, ByVal Ligne As Integer, DataFields As String, aFeuille, maquette As maquette)

'---> Cette proc�dure exporte une ligne d'un block vers Excel

Dim aBlock As Block
Dim aCell As Cellule
Dim aRange
Dim aRow As Object
Dim Decalage As Long
Dim i As Long
Dim j As Integer
Dim k As Integer
Dim PrintSigne As Boolean
Dim aFt As FormatedCell
Dim ContenuCell As String
Dim IsFormated As Boolean
Dim ValuePrint As String
Dim aColumn As Object
Dim aLigne As Object

'-> Indiquer si on exporte avec format ou non
IsFormated = CBool(Me.Check7.Value)

'-> Obtenir un pointeur vers le block
Set aBlock = maquette.Tableaux(UCase$(Trim(Tableau))).Blocks("BL-" & UCase$(Trim(Block)))

'-> V�rifier que l'on exporte la ligne sp�cifi�e
If Not aBlock.GetExportExcel(Ligne) Then Exit Sub

'-> Tester si d�ja export�e
If aBlock.GetExportUniqueExcel(Ligne) And aBlock.GetIsExportedLig(Ligne) Then Exit Sub

'-> Mettre � jour la hauteur de ligne
If IsFormated Then
        Set aLigne = aFeuille.getCellByPosition(XPos, YPos).Rows
        aLigne.height = aBlock.GetHauteurLig(Ligne) * 1000
End If
'-> Get d'un pointeur vers la cellule en cours
If IsFormated Then Set aRange = aFeuille.getCellByPosition(XPos, YPos)

'-> Anlyse de toutes les cellules de la ligne
For i = 1 To aBlock.NbCol
    '-> Pointer sur la cellule
    Set aCell = aBlock.Cellules("L" & Ligne & "C" & i)
    
    '-> Cas ou exporte les formats
    If IsFormated Then
    
        '-> Largeur de la colonne que sil a cellule sp�cifi�e n'est pas fusionn�e
        If Not aCell.IsFusion Then
            Set aColumn = aSpreadSheet.getCellByPosition(XPos, YPos).columns
            '-> Largeur de colonne
            aColumn.width = aBlock.GetLargeurCol(i) * 1000 'frmLib.ScaleX(aBlock.GetLargeurCol(i), 7, 1)
            Decalage = Decalage + 1
        Else
            '-> S�lectionner les cellules suivantes pour la fusion
            Set aRange = aFeuille.getCellRangeByPosition(Decalage, YPos, Decalage + aCell.ColFusion, YPos)  'ExcelApp.Range(aRange, aRange.Offset(, aCell.ColFusion))
            aRange.Merge (True)
            Decalage = Decalage + aCell.ColFusion + 1
        End If 'S'il y a fusion
    
    End If
    
    '-> Remplacer les champs par leur valeur
    aCell.ReplaceField DataFields
        
    '-> Gestion des formats num�rics
    Select Case aCell.TypeValeur
        Case 0 '-> RAS
            'If IsFormated Then aRange.CellContentType = "@"
        Case 1 '-> Caract�re
            '-> Faire un susbstring
            aCell.ContenuCell = Mid$(aCell.ContenuCell, 1, CInt(aCell.Msk))
            'If IsFormated Then aRange.NumberFormat = "@"
        Case 2 '-> Numeric
            'If IsFormated Then aRange.NumberFormat = "0.00"
            '-> V�rifier s'il y a quelquechose � formater
            If Trim(aCell.ContenuCell) = "" Then
            Else
                '-> r�cup�rer le contenu de la cellule
                ContenuCell = Convert(aCell.ContenuCell)
                                                
                '-> Tester que la cellule soit num�rique
                If Not IsNumeric(ContenuCell) Then
                    'If IsFormated Then aRange.NumberFormat = "@"
                Else
                    '-> Charger le masque de la cellule
                    aFt.nbDec = CInt(Entry(1, aCell.Msk, "�"))
                    aFt.idNegatif = CInt(Entry(3, aCell.Msk, "�"))
                    
                    '-> Positionner le bon format de cellule
                    'If IsFormated Then aRange.NumberFormat = GetFormat(aFt.nbDec, aFt.idNegatif)
                    
                    '-> Affecter le contenu de la cellule
                    aCell.ContenuCell = CStr(CDbl(ContenuCell))
                    
                    '-> Si on doit mettre un 0
                    If CDbl(aCell.ContenuCell) = 0 And Not (aCell.PrintZero) Then aCell.ContenuCell = ""
                    
                End If
            End If
    End Select

    If IsFormated Then
        'THIERRY MODIF 27/02/2004
        '-> Mettre � jour le contenu de la cellule
        If IsFormated Then Set aRange = aFeuille.getCellByPosition(XPos, YPos)
        If (IsNumeric(aCell.ContenuCell) And aCell.TypeValeur) Then
            aRange.setValue (aCell.ContenuCell)
        Else
            aRange.setString (aCell.ContenuCell)
        End If
        
        '-> R�cup�rer la  valeur de la cellule
        j = aRange.getcelladdress.Column
        k = aRange.getcelladdress.row
        
        '-> Virer les $
        ContenuCell = j & "," & k
        '-> Ajouter dans la liste des cellules qu'il faudra formatter
        If aCell.ListeCell = "" Then
            aCell.ListeCell = ContenuCell
        Else
            If InStr(1, aCell.ListeCell, ContenuCell) = 0 Then aCell.ListeCell = aCell.ListeCell & ";" & ContenuCell
        End If

        '-> D�placer la cellule active d'une colonne vers la droite
        Set aRange = aSpreadSheet.getCellByPosition(XPos, YPos + 1)
        XPos = XPos + 1
        '-> Lib�rer
        Set aRange = Nothing
    Else
        '-> on ecrit la valeur sur la feuille
        Set aRange = aSpreadSheet.getCellByPosition(XPos, YPos)
            Call SetRangeValueForOpenOffice(aRange, aCell.ContenuCell)
            XPos = XPos + 1
            '-> Lib�rer
            Set aRange = Nothing
    End If
    If aCell.IsFusion Then XPos = XPos + aCell.ColFusion
Next

'-> D�placer d'une ligne vers le bas et revenir � la colonne d'origine
If IsFormated Then
    'Ypos = Ypos + 1
Else
    '-> Inscrire dans le fichier la liste des donn�es � exporter
    Print #hdlFile, ValuePrint
End If

'-> Indiquer que l'on a exporter cette ligne
aBlock.SetIsExportedLig Ligne, True

End Sub

Sub SetRangeValueForOpenOffice(aRange, valueToSet)
'-> Proc�dure qui pose le contenu d'une cellule

    On Error Resume Next

    '-> poser en num�rique ou en texte selon la valeur
    If IsNumeric(valueToSet) Then
        aRange.setValue (valueToSet)
    Else
        aRange.setString (valueToSet)
    End If

End Sub
Private Function GetFormat(nbDec As Integer, IdNeg As Integer) As String

Dim strDec As String
Dim i As Integer
Dim Masque As String

If nbDec = 0 Then
    strDec = "0"
Else
    '-> Cr�er le masque
    strDec = "0." & String$(nbDec, "0")
End If
Masque = strDec

If IdNeg > 1 Then
    Masque = strDec & "_ ;[Red]-" & strDec & " "
End If

GetFormat = Masque

End Function

Private Function RGBConverter(Color As Long) As Long
'--> cette procedure converti les couleur en RGB
Dim RGB1 As Integer
Dim RGB2 As Integer
Dim RGB3 As Integer
Dim cl As Long
If Color <> 16777215 Then
    Color = Color
End If
If Color < 256 Then 'Ne poss�de que du rouge
RGB1 = Color
RGB2 = 0
RGB3 = 0
ElseIf Color < 65536 Then 'Rouge + Vert
RGB1 = Color Mod 256
RGB2 = Color \ 256
RGB3 = 0
Else 'Rouge + Vert + Bleu
RGB3 = Color \ 65536
cl = Color Mod 65536
RGB1 = cl Mod 256
RGB2 = cl \ 256
End If

'--> on retourne le resultat
RGBConverter = rgb(RGB1, RGB2, RGB3)

End Function

Private Sub SetFormat(RangeRef As clsRange, RangeDest As Object)

'---> Cette proc�dure duplique un format
Dim i As Long
Dim aBorder As Object

'-> Bordures
For i = 7 To 10
    Select Case i
        Case 7
            'Bordure Gauche
            If Not IsEmpty(RangeRef.GetBorderColorIndex(i)) Then
                Set aBorder = RangeDest.getPropertyValue("LeftBorder")
                aBorder.Color = rgb(0, 0, 0)
                aBorder.InnerLineWidth = 0
                aBorder.OuterLineWidth = 10
                aBorder.LineDistance = 10
                Call RangeDest.setPropertyValue("LeftBorder", aBorder)
            Else
'                Set aBorder = RangeDest.getPropertyValue("LeftBorder")
'                aBorder.Color = RGB(255, 255, 255)
'                aBorder.InnerLineWidth = 0
'                aBorder.OuterLineWidth = 10
'                aBorder.LineDistance = 10
'                Call RangeDest.setPropertyValue("LeftBorder", aBorder)
            End If
        Case 10
            'Bordure Droite
            If Not IsEmpty(RangeRef.GetBorderColorIndex(i)) Then
                Set aBorder = RangeDest.getPropertyValue("RightBorder")
                aBorder.Color = rgb(0, 0, 0)
                aBorder.InnerLineWidth = 0
                aBorder.OuterLineWidth = 10
                aBorder.LineDistance = 10
                Call RangeDest.setPropertyValue("RightBorder", aBorder)
            Else
'                Set aBorder = RangeDest.getPropertyValue("RightBorder")
'                aBorder.Color = RGB(255, 255, 255)
'                aBorder.InnerLineWidth = 0
'                aBorder.OuterLineWidth = 10
'                aBorder.LineDistance = 10
'                Call RangeDest.setPropertyValue("RightBorder", aBorder)
            End If
        Case 8
            'Bordure Haut
            If Not IsEmpty(RangeRef.GetBorderColorIndex(i)) Then
                Set aBorder = RangeDest.getPropertyValue("TopBorder")
                aBorder.Color = rgb(0, 0, 0)
                aBorder.InnerLineWidth = 0
                aBorder.OuterLineWidth = 10
                aBorder.LineDistance = 10
                Call RangeDest.setPropertyValue("TopBorder", aBorder)
            Else
'                Set aBorder = RangeDest.getPropertyValue("TopBorder")
'                aBorder.Color = RGB(255, 255, 255)
'                aBorder.InnerLineWidth = 0
'                aBorder.OuterLineWidth = 10
'                aBorder.LineDistance = 10
'                Call RangeDest.setPropertyValue("TopBorder", aBorder)
            End If
        Case 9
            'Bordure Bas
            If Not IsEmpty(RangeRef.GetBorderColorIndex(i)) Then
                Set aBorder = RangeDest.getPropertyValue("BottomBorder")
                aBorder.Color = rgb(0, 0, 0)
                aBorder.InnerLineWidth = 0
                aBorder.OuterLineWidth = 10
                aBorder.LineDistance = 10
                Call RangeDest.setPropertyValue("BottomBorder", aBorder)
            Else
'                Set aBorder = RangeDest.getPropertyValue("BottomBorder")
'                aBorder.Color = RGB(255, 255, 255)
'                aBorder.InnerLineWidth = 0
'                aBorder.OuterLineWidth = 10
'                aBorder.LineDistance = 10
'                Call RangeDest.setPropertyValue("BottomBorder", aBorder)
            End If
    End Select
Next

'-> Font
RangeDest.CharFontName = RangeRef.FontName
RangeDest.CharHeight = RangeRef.FontSize
'-> le gras
If RangeRef.FontBold Then
    RangeDest.CharWeight = 150
Else
    RangeDest.CharWeight = 100
End If
'-> l'italique
If RangeRef.FontItalic Then
    RangeDest.CharPosture = 2
Else
    RangeDest.CharPosture = 0
End If
'-> le soulign�
If RangeRef.FontUnderline Then
    RangeDest.CharUnderline = 1
Else
    RangeDest.CharUnderline = 0
End If
'-> Couleur de fond
RangeDest.CellBackColor = RGBConverter(RangeRef.BackColor)

'-> Alignement interne de la cellule
Select Case RangeRef.VerticalAlignment
    Case -4160
        RangeDest.VertJustify = 1
    Case -4108
        RangeDest.VertJustify = 2
    Case -4107
        RangeDest.VertJustify = 3
End Select

Select Case RangeRef.HorizontalAlignment
    Case -4131
        RangeDest.HoriJustify = 1
    Case -4108
        RangeDest.HoriJustify = 2
    Case -4152
        RangeDest.HoriJustify = 3
End Select

Set aBorder = Nothing

End Sub

Private Function VueSave(sName As String)
Dim aNode As Node
Dim aBlock As Block
Dim sMaquette As String

'--> cette fonction permet d'enrtegistrer une vue
SetIniString Trim(UCase(sName)), "G_ETENDUE", App.Path & "\TurboVue.ini", SelectSpool & "�" & Me.Text1.text & "�" & Me.Text2.text
SetIniString Trim(UCase(sName)), "G_GESTION", App.Path & "\TurboVue.ini", CStr(SelectSheet)

'-> pour le tableaux
For Each aNode In Me.TreeView1.Nodes
    If sMaquette <> "" Then sMaquette = sMaquette & "�"
    If aNode.image = "SELECTED" Then
        sMaquette = sMaquette & "1"
    Else
        sMaquette = sMaquette & "0"
    End If
    If aNode.Tag = "LIG" Then
        '-> Pointer sur le block
        Set aBlock = Fichiers(Me.FichierName).Spools(Me.SpoolKey).maquette.Tableaux(UCase$(Trim(Entry(1, aNode.Key, "�")))).Blocks(UCase$(Trim(Entry(2, aNode.Key, "�"))))
        '-> On ajoute les propri�t�s
        If aBlock.GetExportExcel(CInt(Entry(3, aNode.Key, "�"))) Then
            sMaquette = sMaquette & ",1"
        Else
            sMaquette = sMaquette & ",0"
        End If
        If aBlock.GetExportUniqueExcel(CInt(Entry(3, aNode.Key, "�"))) Then
            sMaquette = sMaquette & ",1"
        Else
            sMaquette = sMaquette & ",0"
        End If
    ElseIf aNode.Tag = "BLOCK" Then
        sMaquette = sMaquette & ",,"
    End If
Next
SetIniString Trim(UCase(sName)), "M_MAQUETTE", App.Path & "\TurboVue.ini", sMaquette

SetIniString Trim(UCase(sName)), "R_CLASSE", App.Path & "\TurboVue.ini", Me.Check1.Value
SetIniString Trim(UCase(sName)), "R_NOMMER", App.Path & "\TurboVue.ini", Me.Check4.Value & "�" & Me.Text5.text
SetIniString Trim(UCase(sName)), "R_PLAGE", App.Path & "\TurboVue.ini", Me.Check2.Value & "�" & Me.Text3.text
SetIniString Trim(UCase(sName)), "R_DEPART", App.Path & "\TurboVue.ini", Me.Check3.Value & "�" & Me.Text4.text
SetIniString Trim(UCase(sName)), "T_FORMAT", App.Path & "\TurboVue.ini", Me.Check7.Value

End Function

Private Function VueLoad(sName As String)
Dim i As Integer
Dim aNode As Node
Dim aBlock As Block
Dim sMaquette As String
Dim sTrav As String

On Error Resume Next

'--> cette fonction permet de charger une vue enregistree
sTrav = GetIniString(Trim(UCase(sName)), "G_ETENDUE", App.Path & "\TurboVue.ini", False)
'--> si la vue nest pas ok quitter
If sTrav = "" Then Exit Function
Me.Text1.text = Entry(2, sTrav, "�")
Me.Text2.text = Entry(3, sTrav, "�")
Me.Option1(val(Entry(1, sTrav, "�"))).Value = 1
Call Option1_Click(val(Entry(1, sTrav, "�")))

sTrav = GetIniString(Trim(UCase(sName)), "G_GESTION", App.Path & "\TurboVue.ini", False)
Me.Option8(val(Entry(1, sTrav, "�"))).Value = 1
Call Option8_Click(val(sTrav))

'-> pour le tableaux
sTrav = GetIniString(Trim(UCase(sName)), "M_MAQUETTE", App.Path & "\TurboVue.ini", False)

For Each aNode In Me.TreeView1.Nodes
    i = i + 1
    If aNode.Tag = "LIG" Then
        If Entry(1, Entry(i, sTrav, "�"), ",") = "1" Then
            aNode.image = "SELECTED"
        Else
            aNode.image = "UNSELECTED"
        End If
        '-> Pointer sur le block
        Set aBlock = Fichiers(Me.FichierName).Spools(Me.SpoolKey).maquette.Tableaux(UCase$(Trim(Entry(1, aNode.Key, "�")))).Blocks(UCase$(Trim(Entry(2, aNode.Key, "�"))))
        '-> On ajoute les propri�t�s
        If Entry(2, Entry(i, sTrav, "�"), ",") Then
            aBlock.SetExportExcel CInt(Entry(3, aNode.Key, "�")), True
        Else
            aBlock.SetExportExcel CInt(Entry(3, aNode.Key, "�")), False
        End If
        '-> on affecte la propriete
        If Entry(3, Entry(i, sTrav, "�"), ",") Then
            aBlock.SetExportUniqueExcel CInt(Entry(3, aNode.Key, "�")), True
        Else
            aBlock.SetExportUniqueExcel CInt(Entry(3, aNode.Key, "�")), False
        End If
    ElseIf aNode.Tag = "BLOCK" Then
        
    End If
Next

sTrav = GetIniString(Trim(UCase(sName)), "R_CLASSE", App.Path & "\TurboVue.ini", False)
Me.Check1.Value = val(sTrav)

sTrav = GetIniString(Trim(UCase(sName)), "R_NOMMER", App.Path & "\TurboVue.ini", False)
Me.Check4.Value = val(Entry(1, sTrav, "�"))
Me.Text5.text = Entry(2, sTrav, "�")

sTrav = GetIniString(Trim(UCase(sName)), "R_PLAGE", App.Path & "\TurboVue.ini", False)
Me.Check2.Value = val(Entry(1, sTrav, "�"))
Me.Text3.text = Entry(2, sTrav, "�")

sTrav = GetIniString(Trim(UCase(sName)), "R_DEPART", App.Path & "\TurboVue.ini", False)
Me.Check3.Value = val(Entry(1, sTrav, "�"))
Me.Text4.text = Entry(2, sTrav, "�")

sTrav = GetIniString(Trim(UCase(sName)), "T_FORMAT", App.Path & "\TurboVue.ini", False)
Me.Check7.Value = val(sTrav)

'sTrav = GetIniString(Trim(UCase(sName)), "T_DONNEES", App.Path & "\TurboVue.ini", False)
'Me.Check8.Value = Val(sTrav)

End Function


