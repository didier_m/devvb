VERSION 5.00
Begin VB.Form frmMessage 
   BackColor       =   &H80000003&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   1080
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3405
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1080
   ScaleWidth      =   3405
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Left            =   1560
      Top             =   600
   End
   Begin VB.Image Image1 
      Height          =   360
      Left            =   40
      Picture         =   "frmMessage.frx":0000
      Stretch         =   -1  'True
      Top             =   40
      Width           =   360
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H00808000&
      BorderWidth     =   3
      FillColor       =   &H000000FF&
      Height          =   1075
      Left            =   0
      Top             =   0
      Width           =   3390
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   570
      TabIndex        =   0
      Top             =   480
      Width           =   645
   End
End
Attribute VB_Name = "frmMessage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function SystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" ( _
                ByVal uAction As Long, _
                ByVal uParam As Long, _
                lpvParam As Any, _
                ByVal fuWinIni As Long) As Long

Private Declare Function SetWindowPos Lib "user32" ( _
                ByVal Hwnd As Long, _
                ByVal hWndInsertAfter As Long, _
                ByVal X As Long, _
                ByVal Y As Long, _
                ByVal cx As Long, _
                ByVal cy As Long, _
                ByVal wFlags As Long) As Long

Private Const SPI_GETWORKAREA As Long = 48

Private Const HWND_TOPMOST = -1
Private Const SWP_NOMOVE = &H2
Private Const SWP_NOSIZE = &H1
Private Const SWP_NOACTIVATE = &H10

Private Const iGap As Long = 10 ' Distance from side of screen

Public Event TextClick()

Private deskRECT As RECT

Private Type RECT
    lLeft As Long
    lTop As Long
    lRight As Long
    lBottom As Long
End Type

Private Sub Form_Load()
    SetWindowPos Me.Hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE Or SWP_NOACTIVATE
End Sub

Public Sub ShowMessage(ByVal sText As String, Optional ByVal lTimeOut As Long = 20000)
    Dim n&
    
    If Timer1.Enabled Then HideMessage
    
    ' Set Text
    With Label1
        .Caption = sText
        AlterText False
        .Move (Me.ScaleWidth - .width) / 2, _
              (Me.ScaleHeight - .height) / 2
    End With
    
    ' Scroll Up
    Call SystemParametersInfo(SPI_GETWORKAREA, 0&, deskRECT, 0&)
    For n = 0 To Me.height Step 15
        With deskRECT
            Me.Move (.lRight - iGap) * Screen.TwipsPerPixelX - Me.width, _
                    .lBottom * Screen.TwipsPerPixelY - n, _
                    Me.width, n
            If Not Me.Visible Then Me.Visible = True
            DoEvents
        End With
    Next n
    
    ' Set & Start Timer
    With Timer1
        .Interval = lTimeOut
        .Enabled = True
    End With
End Sub

Private Sub HideMessage()
    Dim n&, lHeight&
    
    Timer1.Enabled = False
    lHeight = Me.height
    
    ' Scroll Down
    For n = lHeight To 0 Step -15
        With deskRECT
            Me.Move (.lRight - iGap) * Screen.TwipsPerPixelX - Me.width, _
                    .lBottom * Screen.TwipsPerPixelY - n, _
                    Me.width, n
            DoEvents
        End With
    Next n
    
    Me.Visible = False
    Me.height = lHeight
End Sub

Private Sub AlterText(ByVal IsOver As Boolean)
    With Label1
        .FontUnderline = IsOver
        .ForeColor = IIf(IsOver, vbGrayed, vbBlack)
    End With
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    AlterText False
End Sub

Private Sub Label1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    AlterText True
End Sub

Private Sub Timer1_Timer()
    HideMessage
End Sub

Private Sub Label1_Click()
    HideMessage
    RaiseEvent TextClick
End Sub


