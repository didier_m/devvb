VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QRUtil"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---------------------------------------------------------------
' QRUtil
'---------------------------------------------------------------
Option Explicit


Private QRPolynomial As QRPolynomial
Private QR_PATTERN_POSITION_TABLE(40, 7) As Integer

Private Sub Class_Initialize()
    
    
    Dim QR_MAX_LENGTH(10, 4) As Integer
    QR_MAX_LENGTH(1, 1) = 41
    QR_MAX_LENGTH(1, 2) = 25
    QR_MAX_LENGTH(1, 3) = 17
    QR_MAX_LENGTH(1, 4) = 10
    
    QR_MAX_LENGTH(2, 1) = 41
    QR_MAX_LENGTH(2, 2) = 25
    QR_MAX_LENGTH(2, 3) = 17
    QR_MAX_LENGTH(2, 4) = 10
    
    QR_MAX_LENGTH(3, 1) = 41
    QR_MAX_LENGTH(3, 2) = 25
    QR_MAX_LENGTH(3, 3) = 17
    QR_MAX_LENGTH(3, 4) = 10

    QR_MAX_LENGTH(4, 1) = 41
    QR_MAX_LENGTH(4, 2) = 25
    QR_MAX_LENGTH(4, 3) = 17
    QR_MAX_LENGTH(4, 4) = 10

    QR_MAX_LENGTH(5, 1) = 41
    QR_MAX_LENGTH(5, 2) = 25
    QR_MAX_LENGTH(5, 3) = 17
    QR_MAX_LENGTH(5, 4) = 10

    QR_MAX_LENGTH(6, 1) = 41
    QR_MAX_LENGTH(6, 2) = 25
    QR_MAX_LENGTH(6, 3) = 17
    QR_MAX_LENGTH(6, 4) = 10

    QR_MAX_LENGTH(7, 1) = 41
    QR_MAX_LENGTH(7, 2) = 25
    QR_MAX_LENGTH(7, 3) = 17
    QR_MAX_LENGTH(7, 4) = 10

    QR_MAX_LENGTH(8, 1) = 41
    QR_MAX_LENGTH(8, 2) = 25
    QR_MAX_LENGTH(8, 3) = 17
    QR_MAX_LENGTH(8, 4) = 10

    QR_MAX_LENGTH(9, 1) = 41
    QR_MAX_LENGTH(9, 2) = 25
    QR_MAX_LENGTH(9, 3) = 17
    QR_MAX_LENGTH(9, 4) = 10

    QR_MAX_LENGTH(10, 1) = 41
    QR_MAX_LENGTH(10, 2) = 25
    QR_MAX_LENGTH(10, 3) = 17
    QR_MAX_LENGTH(10, 4) = 10

'        array( array(41,  25,  17,  10),  array(34,  20,  14,  8),   array(27,  16,  11,  7),  array(17,  10,  7,   4) ),
'        array( array(77,  47,  32,  20),  array(63,  38,  26,  16),  array(48,  29,  20,  12), array(34,  20,  14,  8) ),
'        array( array(127, 77,  53,  32),  array(101, 61,  42,  26),  array(77,  47,  32,  20), array(58,  35,  24,  15) ),
'        array( array(187, 114, 78,  48),  array(149, 90,  62,  38),  array(111, 67,  46,  28), array(82,  50,  34,  21) ),
'        array( array(255, 154, 106, 65),  array(202, 122, 84,  52),  array(144, 87,  60,  37), array(106, 64,  44,  27) ),
'        array( array(322, 195, 134, 82),  array(255, 154, 106, 65),  array(178, 108, 74,  45), array(139, 84,  58,  36) ),
'        array( array(370, 224, 154, 95),  array(293, 178, 122, 75),  array(207, 125, 86,  53), array(154, 93,  64,  39) ),
'        array( array(461, 279, 192, 118), array(365, 221, 152, 93),  array(259, 157, 108, 66), array(202, 122, 84,  52) ),
'        array( array(552, 335, 230, 141), array(432, 262, 180, 111), array(312, 189, 130, 80), array(235, 143, 98,  60) ),
'        array( array(652, 395, 271, 167), array(513, 311, 213, 131), array(364, 221, 151, 93), array(288, 174, 119, 74) )
    

    QR_PATTERN_POSITION_TABLE(1, 1) = 0
    QR_PATTERN_POSITION_TABLE(2, 1) = 6
    QR_PATTERN_POSITION_TABLE(2, 2) = 18
    QR_PATTERN_POSITION_TABLE(3, 1) = 6
    QR_PATTERN_POSITION_TABLE(3, 1) = 22
    
    
'        array(),
'        array(6, 18),
'        array(6, 22),
'        array(6, 26),
'        array(6, 30),
'        array(6, 34),
'        array(6, 22, 38),
'        array(6, 24, 42),
'        array(6, 26, 46),
'        array(6, 28, 50),
'        array(6, 30, 54),
'        array(6, 32, 58),
'        array(6, 34, 62),
'        array(6, 26, 46, 66),
'        array(6, 26, 48, 70),
'        array(6, 26, 50, 74),
'        array(6, 30, 54, 78),
'        array(6, 30, 56, 82),
'        array(6, 30, 58, 86),
'        array(6, 34, 62, 90),
'        array(6, 28, 50, 72, 94),
'        array(6, 26, 50, 74, 98),
'        array(6, 30, 54, 78, 102),
'        array(6, 28, 54, 80, 106),
'        array(6, 32, 58, 84, 110),
'        array(6, 30, 58, 86, 114),
'        array(6, 34, 62, 90, 118),
'        array(6, 26, 50, 74, 98, 122),
'        array(6, 30, 54, 78, 102, 126),
'        array(6, 26, 52, 78, 104, 130),
'        array(6, 30, 56, 82, 108, 134),
'        array(6, 34, 60, 86, 112, 138),
'        array(6, 30, 58, 86, 114, 142),
'        array(6, 34, 62, 90, 118, 146),
'        array(6, 30, 54, 78, 102, 126, 150),
'        array(6, 24, 50, 76, 102, 128, 154),
'        array(6, 28, 54, 80, 106, 132, 158),
'        array(6, 32, 58, 84, 110, 136, 162),
'        array(6, 26, 54, 82, 110, 138, 166),
'        array(6, 30, 58, 86, 114, 142, 170)

End Sub



Public Static Function getPatternPosition(typeNumber)
    Dim positionTable
    Set positionTable = New QR_PATTERN_POSITION_TABLE
    getPatternPosition = positionTable.getB(typeNumber - 1)
End Function

Public Static Function getMaxLength(typeNumber, mode, errorCorrectLevel)
    Dim e, m, t As Integer
    Dim QR_MAX_LENGTH As QR_MAX_LENGTH
    t = typeNumber - 1
    e = 0
    m = 0

    Select Case errorCorrectLevel
        Case QR_ERROR_CORRECT_LEVEL_L
            e = 0
        Case QR_ERROR_CORRECT_LEVEL_M
            e = 1
        Case QR_ERROR_CORRECT_LEVEL_Q
            e = 2
        Case QR_ERROR_CORRECT_LEVEL_H
            e = 3
        Case Else
            trigger_error "e:errorCorrectLevel", E_USER_ERROR
    End Select

    Select Case mode
        Case QR_MODE_NUMBER
            m = 0
        Case QR_MODE_ALPHA_NUM
            m = 1
        Case QR_MODE_8BIT_BYTE
            m = 2
        Case QR_MODE_KANJI
            m = 3
        Case Else
            trigger_error "m:mode", E_USER_ERROR
    End Select

    getMaxLength = QR_MAX_LENGTH.getB(t, e, m)
End Function

Public Static Function getErrorCorrectPolynomial(errorCorrectLength) As QRPolynomial
    Dim i  As Integer
    Dim QRPolynomial
    Dim a As QRPolynomial
    Dim aQRPolynomial As QRPolynomial
    
    Set a = New QRPolynomial
    a.Polynomial Array(1)

    For i = 0 To errorCorrectLength - 1
        Set aQRPolynomial = New QRPolynomial
        aQRPolynomial.Polynomial Array(1, aQRMath.gexp(i))
        Set a = a.multiply(aQRPolynomial)
    Next

    Set getErrorCorrectPolynomial = a
End Function

Public Static Function getMask(maskPattern, i, j)

    Select Case maskPattern

    Case QR_MASK_PATTERN000
        getMask = (i + j) Mod 2 = 0
    Case QR_MASK_PATTERN001
        getMask = i Mod 2 = 0
    Case QR_MASK_PATTERN010
        getMask = j Mod 3 = 0
    Case QR_MASK_PATTERN011
        getMask = (i + j) Mod 3 = 0
    Case QR_MASK_PATTERN100
        getMask = (Int(i / 2) + Int(j / 3)) Mod 2 = 0
    Case QR_MASK_PATTERN101
        getMask = (i * j) Mod 2 + (i * j) Mod 3 = 0
    Case QR_MASK_PATTERN110
        getMask = ((i * j) Mod 2 + (i * j) Mod 3) Mod 2 = 0
    Case QR_MASK_PATTERN111
        getMask = ((i * j) Mod 3 + (i + j) Mod 2) Mod 2 = 0
    Case Else
        trigger_error "mask:maskPattern", E_USER_ERROR
    End Select
End Function

Public Static Function getLostPoint(QRCode As QRCode)
    Dim moduleCount
    Dim col, c, row, r, sameCount, count, darkCount  As Long
    Dim dark
    Dim ratio, lostPoint As Variant
    
    moduleCount = QRCode.getModuleCount()

    lostPoint = 0


    ' LEVEL1
Dim strTemp As String

    For row = 0 To moduleCount - 1

        For col = 0 To moduleCount - 1
            sameCount = 0
            dark = QRCode.isDark(row, col)

            For r = -1 To 1

                If row + r < 0 Or moduleCount <= row + r Then
                    GoTo suite2
                End If

                For c = -1 To 1

                    If col + c < 0 Or moduleCount <= col + c Then
                        GoTo suite1
                    End If

                    If r = 0 And c = 0 Then
                        GoTo suite1
                    End If

                    If dark = QRCode.isDark(row + r, col + c) Then
                        sameCount = sameCount + 1
                    End If
suite1:
                Next
suite2:
            Next

            If sameCount > 5 Then
                lostPoint = lostPoint + (3 + sameCount - 5)
            End If
        Next
    Next
    ' LEVEL2
    For row = 0 To moduleCount - 2
        For col = 0 To moduleCount - 2
            count = 0
            If QRCode.isDark(row, col) Then count = count + 1
            If QRCode.isDark(row + 1, col) Then count = count + 1
            If QRCode.isDark(row, col + 1) Then count = count + 1
            If QRCode.isDark(row + 1, col + 1) Then count = count + 1
            If count = 0 Or count = 4 Then
                lostPoint = lostPoint + 3
            End If
        Next
    Next

    ' LEVEL3
    For row = 0 To moduleCount - 1
        For col = 0 To moduleCount - 7
            If QRCode.isDark(row, col) And Not QRCode.isDark(row, col + 1) And QRCode.isDark(row, col + 2) And QRCode.isDark(row, col + 3) And QRCode.isDark(row, col + 4) And Not QRCode.isDark(row, col + 5) And QRCode.isDark(row, col + 6) Then
                lostPoint = lostPoint + 40
            End If
        Next
    Next

    For col = 0 To moduleCount - 1
        For row = 0 To moduleCount - 7
            If QRCode.isDark(row, col) And Not QRCode.isDark(row + 1, col) And QRCode.isDark(row + 2, col) And QRCode.isDark(row + 3, col) And QRCode.isDark(row + 4, col) And Not QRCode.isDark(row + 5, col) And QRCode.isDark(row + 6, col) Then
                lostPoint = lostPoint + 40
            End If
        Next
    Next

    ' LEVEL4
    darkCount = 0
    For col = 0 To moduleCount - 1
        For row = 0 To moduleCount - 1
            If QRCode.isDark(row, col) Then
                darkCount = darkCount + 1
            End If
        Next
    Next

    ratio = Abs(100 * darkCount / moduleCount / moduleCount - 50) / 5
    lostPoint = lostPoint + ratio * 10

    getLostPoint = lostPoint
End Function

Public Static Function getMode(ByVal s As String)
    If isAlphaNum(s) Then
        If isNumber(s) Then
            getMode = QR_MODE_NUMBER
        End If
        getMode = QR_MODE_ALPHA_NUM
    ElseIf isKanji(s) Then
        getMode = QR_MODE_KANJI
    Else
        getMode = QR_MODE_8BIT_BYTE
    End If
End Function

Public Static Function isNumber(s)
    Dim i As Integer
    Dim c As Integer
    
    For i = 1 To Len(s) - 1
        c = Asc(Mid(s, i, i + 1))
        If Not (toCharCode("0") <= c And c <= toCharCode("9")) Then
            isNumber = False
        End If
    Next
    isNumber = True
End Function

Public Static Function isAlphaNum(s As String)
    Dim i As Integer
    Dim c As Integer
    
    For i = 1 To Len(s) - 1
        c = Asc(Mid(s, i, i + 1))
        If Not (toCharCode("0") <= c And c <= toCharCode("9")) And Not (toCharCode("A") <= c And c <= toCharCode("Z")) And InStr(" %*+-./:", Mid(s, i, i + 1)) = 0 Then
            isAlphaNum = False
            Exit Function
        End If
    Next
    isAlphaNum = True
End Function

Public Static Function isKanji(ByVal s As String)
    Dim data As String
    Dim i As Integer
    Dim c
    
    data = s
    i = 0
    Do While i + 1 < Len(data)
        c = ShiftLeft((CLng("&Hff") And Asc(Mid(data, i + 1, i + 2))), 8) Or (CLng("&Hff") & Asc(Mid(data, i + 2, i + 3)))
        If Not (CLng("&H8140") <= c And c <= CLng("&H9FFC")) And Not (CLng("&HE040") <= c And c <= CLng("&HEBBF")) Then
            isKanji = False
            Exit Function
        End If

        i = i + 2
    Loop

    If i < Len(data) Then
        isKanji = False
        Exit Function
    End If

    isKanji = True
End Function

Public Static Function toCharCode(s)
    toCharCode = Asc(s)
End Function

Public Static Function getBCHTypeInfo(data)
    Dim d
    
    d = ShiftLeft(data, 10)
    Do While getBCHDigit(d) - getBCHDigit(QR_G15) >= 0
        d = d Xor ShiftLeft(QR_G15, (getBCHDigit(d) - getBCHDigit(QR_G15)))
    Loop
    getBCHTypeInfo = (ShiftLeft(data, 10) Or d) Xor QR_G15_MASK
End Function

Public Static Function getBCHTypeNumber(data)
    Dim d
    
    d = ShiftLeft(data, 12)
    Do While getBCHDigit(d) - getBCHDigit(QR_G18) >= 0
        d = d Xor ShiftLeft(QR_G18, (getBCHDigit(d) - getBCHDigit(QR_G18)))
    Loop
    getBCHTypeNumber ShiftLeft(data, 12) Or d
End Function

Public Static Function getBCHDigit(ByVal data)
    Dim digit As Integer
    
    digit = 0

    Do While data <> 0
        digit = digit + 1
        data = ShiftRight(data, 1)
    Loop

    getBCHDigit = digit
End Function

Function ShiftRight(ByVal lngNumber As Long, ByVal intNumBits As Integer) As Long
    ShiftRight = lngNumber \ 2 ^ intNumBits 'note the integer division op

End Function

