VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Fichier"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Cette classe sert � regrouper les diiff�rents spools d'un m�me fichier ASCII

'-> Liste des spools charg�s dans le fichier
Public Spools As Collection

'-> Nombre de spools dans le fichiers
Public NbSpool As Integer

'-> Nom du fichir ASCII
Public FileName As String

'-> Liste des pieces jointes
Public filesJoins As String

'-> fichier d'origine
Public FileNameZip

'-> Indique si le fichier est une ancienne version
Public IsOldVersion As Boolean

Private Sub Class_Initialize()

'-> Initialiser la collection des spools
Set Spools = New Collection

End Sub

Public Function AddSpool() As Spool

'---> Cette fonction ajoute un spool dans la collection et renvoie un objet de type Spool

Dim aSpool As Spool

'-> Incr�menter le compteur de spool
NbSpool = NbSpool + 1

'-> initialiser une nouvelle instance d'un spool
Set aSpool = New Spool

'-> Par d�faut le spool est OK
aSpool.Ok = True

'-> Setting de sa cl� d'acc�s
aSpool.Key = "SPOOL|" & NbSpool

'-> Pour affichage
aSpool.SpoolText = "Spool N� : " & NbSpool

'-> Ajouter dans la collection
Spools.add aSpool, aSpool.Key

'-> Renvoyer l'objet cr��
Set AddSpool = aSpool

End Function

Public Function isSpool(strKey As String) As Boolean

isSpool = False
Dim aSpool As Spool
On Error GoTo GestError

Set aSpool = Spools(strKey)
isSpool = True
GestError:

End Function


