Attribute VB_Name = "fctTurboPrintNew"
Option Explicit

'-> Pour passage en v6
Public V6TurboIniPath As String
Public V6MqtPath As String


'---> Indique si on est en version Entreprise (0) ou profressionelle (1)
Public VersionTurbo As Integer
Public TurboGraphIniFile As String
Public Tm_PictureIniFile As String
Public TurboGraphWebFile As String
Public TurboGraphWebPrinter As String
Public TurboGraphWebDirect As String
Public TurboMaqIniFile As String
Public FichierLangue As String
Public IndexLangue As Integer
Public AskForLangue As Boolean
Public TurbosavePath As String
Public TurboOverWrite As Boolean
Public FileNameZip As String
Public wVersion As Long
'---> Pour gestion des Messporg : version Internet
Public IsAutoVersion As Boolean

'---> Pour gestion des p�riph�riques de sortie
Public mode As Integer '-> Indique le mode de consultation envoy� sur la ligne de commande
Public forceScreen As Boolean
Public Sortie As Object '-> Contient le p�riph�rique de sortie
Public MargeX As Integer '-> Marge interne d'un DC axe X pour impression papier
Public MargeY As Integer '-> Marge interne d'un DC axe Y pour impression papier
Public TempPrinter As Printer '-> Objet de type printer temporaire pour test de validit� imprimante
Public SpoolSize As Long '-> Indique le nombre de page par job d'impression
Public SpoolPrint As Long '-> Indique le nombre de pages imprim�es dans le spool en cours

'---> Pour ouverture des fichiers
Public OpenPath As String 'Indique le path d�faut
Public FirstOpen As Boolean 'N'en tenir compte que la premi�re fois
Public FilePath As String

'---> Pour impression de la page des s�lections
Public Const SelectRTFKey = "%%SELECTIONRTF%%@@THIERRY"

'---> Pour dessin de la temporisation
Public TailleLue As Long
Public TailleTotale As Long

'---> Pour export Excel
Public ExcelOk As Boolean

'---> Pour export OpenOffice
Public OpenOfficeOk As Boolean

'-> pour verifier si on on verifie la presence des tableurs
Public TestTableur As Boolean
Public TestOutlook As Boolean
Public HideMessagerie As Boolean

'---> Pour envoie Internet
Public OutLookOk As Boolean
Public NetParam As String
Public IsCryptedFile As Boolean

'---> Pour envoie vers NOTEPAD ou WORDPAD
Public EditorPath As String

'---> Pour echange entre les feuilles
Public strRetour As String

'---> Pour cr�ation dynamique des controles
Public IdRTf As Integer '-> Contient l'index du prochain Controle RTF qui sera charg�
Public IdBmp As Integer '-> Contient l'index du prochain controle PictureBox qui sera cr��

'---> Structure de controle de dessin d'une cellule d'un objet tableau
Public Type FormatedCell
    Ok As Boolean
    nbDec As Integer
    strFormatted As String
    Value As Double
    idNegatif As Integer
End Type

Private Type vArray
    width As Integer
    height As Integer
    p1() As Integer
    p2() As Integer
    p3() As Integer
    p4() As Integer
End Type

'-> pour la conversion unix2dos
Public Enum ConvertType
    dostounix = 0
    unixtodos = 1
End Enum

'---> Liste des collections pour gestion du multi Spool
Public Fichiers As Collection

'---> Gestion des paths
Public TempDirectory As String

'---> Pour gestion de la cr�ation des blocks de tableau
Dim sCol() As String
Dim sLigne() As String
Dim LigLue As Integer

'---> Pour gestion des envoies par messagerie
Public OrigineMail As Integer '-> Indique la provenance de l'envoie
 '-> Valeurs : 0 -> OutLook depuis Visu _
               1 -> Internet depuis visu _
               2 -> OutLook depuis commande _
               3 -> Internet depuis commande
Public ExportMail As Integer '-> Indique la nature de l'export
'-> Valeurs : 0 -> Page en Cours, 1 -> Spool en cours , Fichier en cours
Public SpoolKeyMail As String '-> Cl� du spool en cours depuis visu
Public FileKeyMail As String '-> Cl� du fihcier en cours de visu
Public PageNumMail As Integer '-> Num�ro de la page que l'on d�sire envoyer
Public SendOutLook As Boolean '-> Indique si on doit afficher ou envoyer le mail vers OutLook
Public SendLotus As Boolean '-> Indique si on doit afficher ou envoyer le mail vers Lotus
Public copyAssemb As Boolean
Public noGarde As Boolean
Public isGarde As Boolean
Public printFilesJoins As Boolean
Public noDepass As Boolean

Public IsMouchard As Boolean
Public MouchardTempsReel As Boolean
Public MouchardLevel As Integer

'-> Pour gestion des
Public KillSpool As Boolean
Public IsPDF As Boolean

Public PdfDestinationFile As String

'-> pour la recherche de texte
Public FindTop As Boolean
Public FindPage As Integer
Public FindPos As Integer
Public FindPos2 As Integer
Public FindText As String
Public FindScrollH As Integer
Public FindScrollV As Integer

'Propri�t� de la portion affich�e
Public GetProp As Boolean
Public strProp As String
Public vProp As Integer
Public hProp As Integer
Public curLigProp As String
Public tempLigProp As String

'-> pour la gestion du debordement
Public DebordementTop As Boolean
Public AllowDebordement As Boolean

'-> pour la gestion du zoom
Public Zoom As Double

'-> pour la gestion de la navigation
Public IsNaviga As Boolean

Public topDirect As Boolean

' Format structure, pass� avec SendMessage au contr�le
Private Type FORMAT
    cbSize As Integer
    wPad1 As Integer
    dwMask As Long
    dwEffects As Long
    yHeight As Long
    yOffset As Long
    crTextColor As Long
    bCharSet As Byte
    bPitchAndFamily As Byte
    szFaceName(0 To LF_FACESIZE - 1) As Byte
    wPad2 As Integer
    wWeight As Integer
    sSpacing As Integer
    crBackColor As Long
    lLCID As Long
    dwReserved As Long
    sStyle As Integer
    wKerning As Integer
    bUnderlineType As Byte
    bAnimation As Byte
    bRevAuthor As Byte
    bReserved1 As Byte
End Type

Public Type URL
    Scheme As String
    Host As String
    Port As Long
    URI As String
    Query As String
End Type
    
Public fNotes As Collection
Public fNote As Form

Public Mouchard As String
Public hdlMouchard As Integer

Public listprinter
Public topForce As Boolean
Public topCXL As Boolean
Public iniName As String
Public strGarde As String
Public listImage As Collection
Public Const CP_UTF8 = 65001
Public Declare Function MultiByteToWideChar Lib "kernel32" (ByVal CodePage As Long, ByVal dwFlags As Long, ByVal lpMultiByteStr As String, ByVal cchMultiByte As Long, ByVal lpWideCharStr As Long, ByVal cchWideChar As Long) As Long
Public Declare Function WideCharToMultiByte Lib "kernel32" (ByVal CodePage As Long, ByVal dwFlags As Long, ByVal lpWideCharStr As Long, ByVal cchWideChar As Long, ByVal lpMultiByteStr As String, ByVal cchMultiByte As Long, ByVal lpDefaultChar As Long, ByVal lpUsedDefaultChar As Long) As Long
'version de windows
Private Type OSVERSIONINFO
        dwOSVersionInfoSize As Long
        dwMajorVersion As Long
        dwMinorVersion As Long
        dwBuildNumber As Long
        dwPlatformId As Long
        szCSDVersion As String * 128      '  Maintenance string for PSS usage
End Type
'renvoie la version de windows
Private Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (lpVersionInformation As OSVERSIONINFO) As Long

Const GTL_USECRLF = 1
Const GTL_PRECISE = 2
Const GTL_NUMCHARS = 8
Const CP_UNICODE = 1200
Const GT_USECRLF = 1
Private Type GETTEXTEX
    cb As Long
    Flags As Long
    CodePage As Integer
    lpDefaultChar As String
    lpUsedDefChar As Boolean
End Type

Public Const EM_GETTEXTEX = (WM_USER + 94)
Public Const EM_GETTEXTLENGTHEX = (WM_USER + 95)

Private Type GETTEXTLENGTHEX
    Flags As Long          ' /* flags (see GTL_XXX defines)              */
    CodePage As Long       ' /* code page for translation (CP_ACP for default,
                            '1200 for Unicode                         */
End Type

Private Declare Function SendMessageWLng Lib "user32" Alias "SendMessageW" ( _
    ByVal Hwnd As Long, _
    ByVal wMsg As Long, _
    ByVal wParam As Long, _
    ByVal lParam As Long) As Long
    
Public Enum rgnFillStyle    'FillStyle Enum
    Transparent = 0
    HorizontalLine = 1
    VerticalLine = 2
    UpwardDiagonal = 3
    DownwardDiogonal = 4
    Cross = 5
    DiagonalCross = 6
    Solid = 7
    FillBitmap = 8
End Enum

Public Type COORD   'The co-ordinates in Long
    X As Long
    Y As Long
End Type

Public Declare Function CreatePatternBrush Lib "gdi32" (ByVal hBitmap As Long) As Long

Public Type LOGBRUSH    'Type for the API 'CreateBrushIndirect'
        lbStyle As Long
        lbColor As Long
        lbHatch As Long
End Type

Const ALTERNATE = 1 ' ALTERNATE and WINDING are
Const WINDING = 2 ' constants for FillMode.

Declare Function CreateBrushIndirect Lib "gdi32" (lpLogBrush As LOGBRUSH) As Long
Private Declare Function CreatePolygonRgn Lib "gdi32" (lpPoint As Any, ByVal nCount As Long, ByVal nPolyFillMode As Long) As Long
Private Declare Function FillRgn Lib "gdi32" (ByVal hdc As Long, ByVal hRgn As Long, ByVal hBrush As Long) As Long

Public isCode128 As Boolean
    
'Private lines As Integer
'Private vcos As Long
'Private vsin As Long

Private Const GENERIC_READ = &H80000000
Private Const GENERIC_WRITE = &H40000000
Private Const FILE_SHARE_READ = &H1
Private Const FILE_SHARE_WRITE = &H2
Private Const OPEN_EXISTING = 3

Private Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Private Declare Function FileTimeToLocalFileTime Lib "kernel32" (lpFileTime As FILETIME, lpLocalFileTime As FILETIME) As Long
Private Declare Function LocalFileTimeToFileTime Lib "kernel32" (lpLocalFileTime As FILETIME, lpFileTime As FILETIME) As Long
Private Declare Function FileTimeToSystemTime Lib "kernel32" (lpFileTime As FILETIME, lpSystemTime As SYSTEMTIME) As Long
Private Declare Function SystemTimeToFileTime Lib "kernel32" (lpSystemTime As SYSTEMTIME, lpFileTime As FILETIME) As Long

Public Declare Function Polygon Lib "gdi32" (ByVal hdc As Long, lpPoint As POINTAPI, ByVal nCount As Long) As Long

Public Declare Function MakeSureDirectoryPathExists Lib "imagehlp.dll" (ByVal lpPath As String) As Long
Public Declare Function GetShortPathName Lib "kernel32.dll" Alias "GetShortPathNameA" (ByVal lpszLongPath As String, ByVal lpszShortPath As String, ByVal cchBuffer As Integer) As Integer

'structure de coordonn�es
Private Type POINTAPI
    X As Long
    Y As Long
End Type
Private apt As Variant
Private columns As Integer
Private lines As Integer
Private angle As Long
Private vcos As Long
Private vsin As Long
Private drw As Double
Private apic As PictureBox
Private X As Long
Private Y As Long
Private x1 As Long
Private y1 As Long
Private x2 As Long
Private y2 As Long

Public num
Public E_USER_ERROR As Integer

Public Const QR_MODE_NUMBER As Byte = 1 < 1
Public Const QR_MODE_ALPHA_NUM As Byte = 2
Public Const QR_MODE_8BIT_BYTE As Byte = 4
Public Const QR_MODE_KANJI As Byte = 1 < 8
Public Const QR_MASK_PATTERN000 As Byte = 0
Public Const QR_MASK_PATTERN001 As Byte = 1
Public Const QR_MASK_PATTERN010 As Byte = 2
Public Const QR_MASK_PATTERN011 As Byte = 3
Public Const QR_MASK_PATTERN100 As Byte = 4
Public Const QR_MASK_PATTERN101 As Byte = 5
Public Const QR_MASK_PATTERN110 As Byte = 6
Public Const QR_MASK_PATTERN111 As Byte = 7
Public Const QR_ERROR_CORRECT_LEVEL_L As Byte = 1
Public Const QR_ERROR_CORRECT_LEVEL_M As Byte = 0
Public Const QR_ERROR_CORRECT_LEVEL_Q As Byte = 3
Public Const QR_ERROR_CORRECT_LEVEL_H As Byte = 2

Public bitsInitialized As Boolean

Public Const SNOBITS As Integer = 0
Public Const SBIT0   As Integer = 1
Public Const SBIT1   As Integer = SBIT0 * 2
Public Const SBIT2   As Integer = SBIT1 * 2
Public Const SBIT3   As Integer = SBIT2 * 2
Public Const SBIT4   As Integer = SBIT3 * 2
Public Const SBIT5   As Integer = SBIT4 * 2
Public Const SBIT6   As Integer = SBIT5 * 2
Public Const SBIT7   As Integer = SBIT6 * 2
Public Const SBIT8   As Integer = SBIT7 * 2
Public Const SBIT9   As Integer = SBIT8 * 2
Public Const SBIT10  As Integer = SBIT9 * 2
Public Const SBIT11  As Integer = SBIT10 * 2
Public Const SBIT12  As Integer = SBIT11 * 2
Public Const SBIT13  As Integer = SBIT12 * 2
Public Const SBIT14  As Integer = SBIT13 * 2
Public Const SBIT15  As Integer = (Not 0) And (Not _
(SBIT0 Or SBIT1 Or SBIT2 Or SBIT3 Or SBIT4 Or SBIT5 Or SBIT6 Or SBIT7 Or _
 SBIT8 Or SBIT9 Or SBIT10 Or SBIT11 Or SBIT12 Or SBIT13 Or SBIT14))


Public Const NOBITS As Long = 0
Public Const BIT0   As Long = 1
Public Const BIT1   As Long = BIT0 * 2
Public Const BIT2   As Long = BIT1 * 2
Public Const BIT3   As Long = BIT2 * 2
Public Const BIT4   As Long = BIT3 * 2
Public Const BIT5   As Long = BIT4 * 2
Public Const BIT6   As Long = BIT5 * 2
Public Const BIT7   As Long = BIT6 * 2
Public Const BIT8   As Long = BIT7 * 2
Public Const BIT9   As Long = BIT8 * 2
Public Const BIT10  As Long = BIT9 * 2
Public Const BIT11  As Long = BIT10 * 2
Public Const BIT12  As Long = BIT11 * 2
Public Const BIT13  As Long = BIT12 * 2
Public Const BIT14  As Long = BIT13 * 2
Public Const BIT15  As Long = BIT14 * 2
Public Const BIT16  As Long = BIT15 * 2
Public Const BIT17  As Long = BIT16 * 2
Public Const BIT18  As Long = BIT17 * 2
Public Const BIT19  As Long = BIT18 * 2
Public Const BIT20  As Long = BIT19 * 2
Public Const BIT21  As Long = BIT20 * 2
Public Const BIT22  As Long = BIT21 * 2
Public Const BIT23  As Long = BIT22 * 2
Public Const BIT24  As Long = BIT23 * 2
Public Const BIT25  As Long = BIT24 * 2
Public Const BIT26  As Long = BIT25 * 2
Public Const BIT27  As Long = BIT26 * 2
Public Const BIT28  As Long = BIT27 * 2
Public Const BIT29  As Long = BIT28 * 2
Public Const BIT30  As Long = BIT29 * 2
Public Const BIT31  As Long = (Not 0) And (Not _
(BIT0 Or BIT1 Or BIT2 Or BIT3 Or BIT4 Or BIT5 Or BIT6 Or BIT7 Or _
 BIT8 Or BIT9 Or BIT10 Or BIT11 Or BIT12 Or BIT13 Or BIT14 Or BIT15 Or _
 BIT16 Or BIT17 Or BIT18 Or BIT19 Or BIT20 Or BIT21 Or BIT22 Or BIT23 Or _
 BIT24 Or BIT25 Or BIT26 Or BIT27 Or BIT28 Or BIT29 Or BIT30))
  
Private Const BITS0thru4   As Long = BIT0 Or BIT1 Or BIT2 Or BIT3 Or BIT4
Private Const BITS0thru7   As Long = BIT0 Or BIT1 Or BIT2 Or BIT3 Or BIT4 Or BIT5 Or BIT6 Or BIT7
Private Const BITS8thru15  As Long = BIT8 Or BIT9 Or BIT10 Or BIT11 Or BIT12 Or BIT13 Or BIT14 Or BIT15
Private Const BITS16thru23 As Long = BIT16 Or BIT17 Or BIT18 Or BIT19 Or BIT20 Or BIT21 Or BIT22 Or BIT23
Private Const BITS24thru31 As Long = BIT24 Or BIT25 Or BIT26 Or BIT27 Or BIT28 Or BIT29 Or BIT30 Or BIT31

Public Const BYTE0 As Long = BITS0thru7
Public Const BYTE1 As Long = BITS8thru15
Public Const BYTE2 As Long = BITS16thru23
Public Const BYTE3 As Long = BITS24thru31

Private Const strOne As String = "1"
  
Public bits(31) As Long
Public SBits(15) As Integer

Private testbitvect() As Long
Private testbitvect2() As Long


Public qrDataList As Collection
Public QR_G15 As Long
Public QR_G18 As Long
Public QR_G15_MASK As Long

Public aQRMath
Public lockToolbar As Boolean

Public Declare Function ReadFile Lib "kernel32" (ByVal hFile As Long, _
    lpBuffer As Any, ByVal nNumberOfBytesToRead As Long, _
    lpNumberOfBytesRead As Long, ByVal lpOverlapped As Long) As Long
                
Public Function trigger_error(sString As String, typeError As Integer)

End Function

Public Function Max(ByVal i As Long, ByVal j As Long) As Long
    If i > j Then
        Max = i
    Else
        Max = j
    End If
End Function

'Function ShiftRight(ByVal lngNumber As Long, ByVal intNumBits As Integer) As Long
'    ShiftRight = lngNumber \ 2 ^ intNumBits 'note the integer division op
'
'End Function

Function ShiftLeft(ByVal lngNumber As Long, ByVal intNumBits As Integer) As Long
ShiftLeft = lngNumber * 2 ^ intNumBits

End Function

'debug.Print drawdatamatrix("123456789012",300,300)
Public Function DrawDataMatrix(pictureB As PictureBox, strValue As String, width As Integer, height As Integer) As PictureBox
'Dim apicOb As Object
Dim xx, yy, angle As Integer
        apt = getDigit(strValue, False)
        Set apic = pictureB
        angle = 0 '90
        apic.AutoSize = 1
        apic.AutoRedraw = True
        apic.ScaleMode = vbPixels
        apic.width = width * 1.5
        apic.height = height * 1.5
        apic.ScaleWidth = width
        apic.ScaleHeight = height
        apic.DrawStyle = 0
        apic.FillStyle = 0
        apic.foreColor = vbBlack
        apic.FillColor = vbBlack
        apic.DrawWidth = 1

        apic.Cls
        drw = Ceil(apic.ScaleWidth / (UBound(apt, 1)) + 1) / 10
        xx = drw * UBound(apt, 1) / 2
        yy = xx
        apic.DrawWidth = 1 'drw
        apic.DrawStyle = 0
        digitToRenderer xx, yy, angle, drw, drw, apt
        apic.Refresh
        'apic.Picture = apic.image
        'SavePicture apic.image, "test.bmp"
        'Form1.Picture1.Picture = LoadPicture("test.bmp")
        Set DrawDataMatrix = apic
        Debug.Print "bb"
End Function

Function Ceil(n As Double) As Long
    If Not Int(n) = n Then
        Ceil = Int(n) + 1
    Else
        Ceil = n
    End If
    
    '-> test
    Ceil = n
    
    
End Function

Private Function fn(points As Variant)
    Dim pts(3) As POINTAPI
    Dim i As Integer
    
    i = 1
    pts(0).X = points(0) * i
    pts(0).Y = points(1) * i
    pts(1).X = points(2) * i
    pts(1).Y = points(3) * i
    pts(2).X = points(4) * i
    pts(2).Y = points(5) * i
    pts(3).X = points(6) * i
    pts(3).Y = points(7) * i
    
    Polygon apic.hdc, pts(0), UBound(pts) + 1
    'apic.PSet (points(0) * drw, points(1) * drw), QBColor(0)
End Function

Private Function Deg2Rad(ByVal degrees As Single) As Single
    Deg2Rad = degrees / 57.29578
End Function

Private Function digitToRenderer(xi, yi, angle, mw, mh, digit)
    'Dim X As Integer
    'Dim Y As Integer
    Dim z As Integer
    Dim xA As Integer
    Dim xB As Integer
    Dim xC As Integer
    Dim xD As Integer
    Dim yA As Integer
    Dim yB As Integer
    Dim yC As Integer
    Dim yD As Integer
    
    lines = UBound(digit) + 1
    columns = UBound(digit, 1) + 1
    angle = Deg2Rad(-angle)
    vcos = Cos(angle)
    vsin = sIn(angle)

    rotate columns * mw / 2, lines * mh / 2, vcos, vsin, X, Y
    xi = xi - X
    yi = yi - Y
    For Y = 0 To lines - 1
        X = -1
        While (X < columns - 1)
            X = X + 1
            If digit(Y, X) = "1" Then
                z = X
                While (z + 1 < columns) And (digit(Y, z + 1) = "1")
                    z = z + 1
                Wend
                x1 = X * mw
                y1 = Y * mh
                x2 = (z + 1) * mw
                y2 = (Y + 1) * mh
                rotate x1, y1, vcos, vsin, xA, yA
                rotate x2, y1, vcos, vsin, xB, yB
                rotate x2, y2, vcos, vsin, xC, yC
                rotate x1, y2, vcos, vsin, xD, yD
'Debug.Print X & "  xAyA:(" & (xA + xi) & "," & (yA + yi) & ") xByB:(" & (xB + xi) & "," & (yB + yi) & ") xCyC:(" & (xC + xi) & "," & (yC + yi) & ") xDyD:(" & (xD + xi) & "," & (yD + yi) & ")" & Chr(13)
                fn (Array(xA + xi, yA + yi, xB + xi, yB + yi, xC + xi, yC + yi, xD + xi, yD + yi))
                X = z + 1
            End If
        Wend
    Next
    digitToRenderer = Result(xi, yi, columns, lines, mw, mh, vcos, vsin)
End Function

Private Function Result(xi, yi, columns, lines, mw, mh, vcos, vsin)
'       rotate 0, 0, vcos, vsin, x1, y1
'        rotate columns * mw, 0, vcos, vsin, x2, y2
'        rotate columns * mw, lines * mh, vcos, vsin, X3, Y3
'        rotate 0, lines * mh, vcos, vsin, x4, y4

'        return Array(
'            'width' => $columns * $mw,
'            'height'=> $lines * $mh,
'            'p1' => array(
'                'x' => $xi + $x1,
'                'y' => $yi + $y1
'            ),
'            'p2' => array(
'                'x' => $xi + $x2,
'                'y' => $yi + $y2
'            ),
'            'p3' => array(
'                'x' => $xi + $x3,
'                'y' => $yi + $y3
'            ),
'            'p4' => array(
'                'x' => $xi + $x4,
'                'y' => $yi + $y4
'            )
'        );
End Function

Private Function rotate(x1, y1, vcos, vsin, ByRef X, ByRef Y)
        X = x1 * vcos - y1 * vsin
        Y = x1 * vsin + y1 * vcos
End Function

Public Function rotateP(x1, y1, angle, X, Y)
    angle = Deg2Rad(-angle)
    vcos = Cos(angle)
    vsin = sIn(angle)
    X = x1 * vcos - y1 * vsin
    Y = x1 * vsin + y1 * vcos
End Function


Public Sub GetPrinterList(PrintFile As String)

'---> Cette proc�dure affiche la liste des imprimante pour la s�lection

Dim NomImp As String
Dim NbCopies As Integer
Dim LigneCommande As String
Dim RectoVerso As String

'-> Vider la variable d'�change
strRetour = ""

'-> Charger avec la liste des imprimantes
Load frmPrint

'-> Initialiser avec la fonction qui va bien
frmPrint.InitialisationGetPrint

'-> mettre la feuille au premier plan
SetWindowPos frmPrint.Hwnd, -1, 0, 0, 0, 0, 2 Or 1
If nRectoVerso Then frmPrint.Check1.Value = 1
'-> Afficher la feuille
frmPrint.Show vbModal

If strRetour = "" Then End

If InStr(1, PrintFile, "content.ie5", vbTextCompare) <> 0 Then
    PrintFile = Replace(PrintFile, "content.ie5", "CONTENT.IE5", , , vbTextCompare)
End If

'-> Get du nom de l'imprimante
NomImp = Entry(1, strRetour, "|")
NbCopies = CInt(Entry(3, strRetour, "|"))
'-> recup du parampetre recto-verso
RectoVerso = Entry(4, strRetour, "|")

If UCase$(NomImp) = "DEALVIEW" Then
    LigneCommande = "ECRAN$" & IndexLangue & "|" & PrintFile
Else
    LigneCommande = NomImp & "~DIRECT~" & NbCopies & "|" & PrintFile & "|" & RectoVerso
End If

'-> Lancer le turbo avec la nouvelle
Shell App.Path & "\TurboGraph.exe " & LigneCommande
End

End Sub

Public Function InitTurboVersion()

Dim strPath As String
Dim i As Integer
Dim Res As Long
Dim lpBuffer As String
Dim TempFileName As String

'->  D�tection de la version Professionelle: Pr�sence du fichier Turbo.ver et Path

On Error Resume Next

'-> Recherche du fichier Version :
If Dir$(App.Path & "\Turbo.ver") = "" Then
    '-> On ne trouve pas le fichier Ver donc on est en version Internet.
    VersionTurbo = 1 'Version internet
Else
    VersionTurbo = 0 'Version Path v6
End If

'-> Par d�faut on est en fran�ais
IndexLangue = 1

'-> On est en version Entreprise : recherche du path des fichiers Ini et des fichiers Ressources
If VersionTurbo = 1 Then 'Version internet
    TurboGraphIniFile = "" '-> Pas d'acc�s au fichier TurboGrpah.ini
    TurboGraphIniFile = App.Path & "/" & "Turbograph.ini" 'MESSPROG
    '-> Setting du fichier TM_Picture.ini
    Tm_PictureIniFile = App.Path & "\Tm_Picture.ini"
    If Dir$(Tm_PictureIniFile, vbNormal) = "" Then Tm_PictureIniFile = ""
    'TelechargeFile(TurboGraphWebFile & "/deallogo/Tm_Picture.ini" , App.Path & "\Tm_Picture.ini")
    'Tm_PictureIniFile = "" '-> Pas d'acc�s au fichier Tm_picture.ini
    TurboMaqIniFile = "" '-> Pas d'acc�s au fichier TurboMaq.ini
    FichierLangue = "" '-> Pas de fichiers Langue - > Charger en fichier Ressource
    AskForLangue = True '-> Demander le fichier Langue
    '-> on regarde si on a un code langue de sp�cifi�
    lpBuffer = GetIniString("PARAM", "LANGUE", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then IndexLangue = val(lpBuffer)
    If GetVariableEnv("DEALLANGUE") <> "" Then
        IndexLangue = val(GetVariableEnv("DEALLANGUE"))
    End If
    '-> on reccupere ou telecharger les images
    lpBuffer = GetIniString("SITE", "HTTP", App.Path & "\Turbograph.ini", False)
    If lpBuffer <> "NULL" Then
        TurboGraphWebFile = Trim(lpBuffer)
        If Right(TurboGraphWebFile, 1) = "/" Then TurboGraphWebFile = Mid(TurboGraphWebFile, 1, Len(TurboGraphWebFile) - 1)
    End If
    '-> on reccupere si on a une imprimante par defaut
    lpBuffer = GetIniString("PRINTER", "DEFAULT", App.Path & "\Turbograph.ini", False)
    If lpBuffer <> "NULL" Then TurboGraphWebPrinter = Trim(lpBuffer)
    '-> on reccupere si on doit envoyer directement l'impression
    lpBuffer = GetIniString("PRINTER", "DIRECT", App.Path & "\Turbograph.ini", False)
    If lpBuffer <> "NULL" Then TurboGraphWebDirect = Trim(lpBuffer)
    '-> on regarde si on est en mode trace
    lpBuffer = GetIniString("PARAM", "TRACE", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then IsMouchard = Trim(lpBuffer)
    '-> on regarde le niveau de trace
    'lpBuffer = GetIniString("PARAM", "TRACELEVEL", TurboGraphIniFile, False)
    'If lpBuffer <> "NULL" Then MouchardLevel = Val(Trim(lpBuffer))
    '-> on regarde si on doit afficher l'indicateur de depassement
    lpBuffer = GetIniString("PARAM", "DEPASS", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then noDepass = Trim(lpBuffer)
    '-> on reccupere ou telecharger les images
    lpBuffer = GetIniString("SITE", "HTTP", App.Path & "\Turbograph.ini", False)
    If lpBuffer <> "NULL" Then
        TurboGraphWebFile = Trim(lpBuffer)
        If Right(TurboGraphWebFile, 1) = "/" Then TurboGraphWebFile = Mid(TurboGraphWebFile, 1, Len(TurboGraphWebFile) - 1)
    End If
    TurboOverWrite = True
    '-> on reccupere le repertoire de sauvegarde par defaut
    lpBuffer = GetIniString("PARAM", "OVERWRITE", App.Path & "\Turbograph.ini", False)
    If lpBuffer <> "NULL" Then
        If Trim(lpBuffer) = "0" Then
            TurboOverWrite = False
        Else
            TurboOverWrite = True
        End If
    End If
    '-> on reccupere le repertoire de sauvegarde par defaut
    lpBuffer = GetIniString("PARAM", "SAVE", App.Path & "\Turbograph.ini", False)
    If lpBuffer <> "NULL" Then
        TurbosavePath = Trim(lpBuffer)
    End If
    '-> on regarde si il n'y a pas une variable d'environnement dedans du style %username%
    If NumEntries(TurbosavePath, "%") = 3 Then
        TurbosavePath = Entry(1, TurbosavePath, "%") & GetVariableEnv(Entry(2, TurbosavePath, "%")) & Entry(3, TurbosavePath, "%")
        Trace "avec variable env : " & TurbosavePath
        'If Dir(TurbosavePath, vbDirectory) = "" Then TurbosavePath = ""
    End If
    If TurbosavePath = "" Then TurbosavePath = GetSpecialfolder(CSIDL_PERSONAL) & "\mes spools\"
    
    If Dir(GetSpecialfolder(CSIDL_PERSONAL) & "\mes spools\") = "" Then GetMyDocumentsPathVB
    '-> on verifie que le dernier caractere est bien un '\'
    If Right(TurbosavePath, 1) <> "/" And Right(TurbosavePath, 1) <> "\" Then TurbosavePath = TurbosavePath & "\"
    Trace "InitTurboVersion Turbosavepath = " & TurbosavePath
    '-> on regarde si on doit verifier la presence de tableur
    lpBuffer = GetIniString("PARAM", "VERIFYTABLEUR", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then TestTableur = Trim(lpBuffer)
    '-> on regarde si on doit verifier la presence de tableur
    lpBuffer = GetIniString("PARAM", "VERIFYOUTLOOK", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then TestOutlook = Trim(lpBuffer)
    '-> on regarde si on doit afficher outlook
    lpBuffer = GetIniString("PARAM", "HideMessagerie", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then HideMessagerie = Trim(lpBuffer)
    
    lpBuffer = GetIniString("PARAM", "ISEXCEL", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then
        If Trim(lpBuffer) = "1" Then ExcelOk = True
    End If
    lpBuffer = GetIniString("PARAM", "ISOPENOFFICE", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then
        If Trim(lpBuffer) = "1" Then OpenOfficeOk = True
    End If
    Exit Function
Else 'Verdion Entreprise
    '-> Le path du r�pertoire doit �tre renseign� MESSPROG
    If V6TurboIniPath = "" Then
        If Dir$(App.Path & "\Turbograph.ini") <> "" Then
                V6TurboIniPath = App.Path
        Else
            lpBuffer = ShowOpenFolder("Recherche du r�pertoire de param�trage ", 0)
            If lpBuffer = "" Then
                MsgBox "Impossible de continuer sans le r�pertoire de param�trage", vbCritical + vbOKOnly, "Erreur"
                End
            End If
            V6TurboIniPath = lpBuffer
        End If
    End If
    If Right$(V6TurboIniPath, 1) <> "\" Then V6TurboIniPath = V6TurboIniPath & "\"
    If (GetAttr(V6TurboIniPath) And vbDirectory) <> vbDirectory Then 'MESSPROG
        MsgBox "Impossible de trouver le r�pertoire des fichiers langue." & vbCrLf & V6TurboIniPath, vbCritical + vbOKOnly, "Erreur"
        End
    End If
    TurboGraphIniFile = V6TurboIniPath & "Turbograph.ini" 'MESSPROG
    If Dir$(TurboGraphIniFile) = "" Then
        MsgBox "Impossible de trouver le fichier de param�trage : " & vbCrLf & TurboGraphIniFile, vbCritical + vbOKOnly, "Erreur"
        End
    End If
    '-> Path des fichiers langue
    lpBuffer = App.Path
    i = InStrRev(lpBuffer, "\")
    lpBuffer = Mid$(lpBuffer, 1, i) & "Mqt\"
    If (GetAttr(lpBuffer) And vbDirectory) <> vbDirectory Then 'MESSPROG
        MsgBox "Impossible de trouver le r�pertoire des fichiers langue." & vbCrLf & lpBuffer, vbCritical + vbOKOnly, "Erreur"
        End
    End If
    V6MqtPath = lpBuffer
    FichierLangue = lpBuffer
    
    '-> Setting du fichier TM_Picture.ini
    Tm_PictureIniFile = V6TurboIniPath & "Tm_Picture.ini"
    If Dir$(Tm_PictureIniFile, vbNormal) = "" Then Tm_PictureIniFile = ""
    
    '-> Setting du fichier TurboMaq.ini
    TurboMaqIniFile = V6TurboIniPath & "TurboMaq.ini"
    If Dir$(TurboMaqIniFile, vbNormal) = "" Then TurboMaqIniFile = ""
    '-> on regarde si on doit verifier la presence de tableur
    lpBuffer = GetIniString("PARAM", "VERIFYTABLEUR", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then TestTableur = Trim(lpBuffer)
    '-> on regarde si on doit verifier la presence de tableur
    lpBuffer = GetIniString("PARAM", "VERIFYOUTLOOK", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then TestOutlook = Trim(lpBuffer)
    '-> on regarde si on doit afficher outlook
    lpBuffer = GetIniString("PARAM", "HideMessagerie", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then HideMessagerie = Trim(lpBuffer)
    
    lpBuffer = GetIniString("PARAM", "ISEXCEL", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then
        If Trim(lpBuffer) = "1" Then ExcelOk = True
    End If
    lpBuffer = GetIniString("PARAM", "ISOPENOFFICE", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then
        If Trim(lpBuffer) = "1" Then OpenOfficeOk = True
    End If
End If

'-> on regarde si on est en mode trace
lpBuffer = GetIniString("PARAM", "TRACE", TurboGraphIniFile, False)
If lpBuffer <> "NULL" Then IsMouchard = Trim(lpBuffer)

'-> on regarde le niveau de trace
lpBuffer = GetIniString("PARAM", "TRACELEVEL", TurboGraphIniFile, False)
If lpBuffer <> "NULL" Then MouchardLevel = val(Trim(lpBuffer))

'-> on regarde si on doit afficher l'indicateur de depassement
lpBuffer = GetIniString("PARAM", "DEPASS", TurboGraphIniFile, False)
If lpBuffer <> "NULL" Then noDepass = Trim(lpBuffer)

'-> on regarde si on doit verifier la presence de tableur
lpBuffer = GetIniString("PARAM", "VERIFYTABLEUR", TurboGraphIniFile, False)
If lpBuffer <> "NULL" Then TestTableur = Trim(lpBuffer)
'-> on regarde si on doit verifier la presence de tableur
lpBuffer = GetIniString("PARAM", "VERIFYOUTLOOK", TurboGraphIniFile, False)
If lpBuffer <> "NULL" Then TestOutlook = Trim(lpBuffer)
'-> on regarde si on doit afficher outlook
lpBuffer = GetIniString("PARAM", "HideMessagerie", TurboGraphIniFile, False)
If lpBuffer <> "NULL" Then HideMessagerie = Trim(lpBuffer)
'-> on regarde si on doit specifier un nom de repertoire temporaire autre que celui par defaut
lpBuffer = GetIniString("PARAM", "TEMPPATHLOCAL", TurboGraphIniFile, False)
If lpBuffer <> "NULL" Then
    If Trim(lpBuffer) <> "" Then tempPathLocal = Trim(lpBuffer)
End If


'-> Initialisation de la version entreprise

'-> Recherche du fichier Langue
If TurboGraphIniFile <> "" Then
    lpBuffer = GetIniString("PARAM", "LANGUE", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then
        '-> Tester si la zone est num�rique
        If IsNumeric(lpBuffer) Then
            IndexLangue = CInt(lpBuffer)
            AskForLangue = False
        Else
            '-> Il faut demander la langue de l'utilisateur
            AskForLangue = True
            IndexLangue = 0
        End If
    Else
        '-> Demander le fichier langue � charger
        AskForLangue = True
    End If
    If GetVariableEnv("DEALLANGUE") <> "" Then
        IndexLangue = CInt(GetVariableEnv("DEALLANGUE"))
    End If
    '-> Taille du spool
    lpBuffer = GetIniString("PARAM", "SPOOLSIZE", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then
        If IsNumeric(lpBuffer) Then SpoolSize = CLng(lpBuffer)
    End If
        
    '-> R�pertoire d'export HTML
    lpBuffer = GetIniString("PARAM", "EXPORTHTML", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then
        PathToExport = Trim(lpBuffer)
        '-> V�rifier si on ne demande pas de travailler dans le r�pertoire de travail de Progress
        If UCase$(Trim(Entry(1, lpBuffer, "$"))) = "ENV" Then
            '-> R�cup�rer la variable d'environnement
            lpBuffer = GetVariableEnv(Entry(2, lpBuffer, "$"))
            '-> V�rifier la valeur
            If Trim(lpBuffer) <> "" Then
                If Dir$(lpBuffer, vbDirectory) <> "" Then
                    PathToExport = lpBuffer
                Else
                    PathToExport = ""
                End If
            Else
                PathToExport = ""
            End If
        Else
            If Dir$(PathToExport, vbDirectory) = "" Then PathToExport = ""
        End If
    End If
End If 'Si on a acc�s au fichier Ini

End Function

Public Function GetVariableEnv(strVariable As String) As String

Dim Res As Long
Dim lpBuffer As String

lpBuffer = Space$(500)
Res = GetEnvironmentVariable(strVariable, lpBuffer, Len(lpBuffer))
If Res <> 0 Then
    '-> Faire un trim sur le buffer
    lpBuffer = Mid$(lpBuffer, 1, Res)
Else
    lpBuffer = ""
End If

'-> Retouner la valeur
GetVariableEnv = lpBuffer

End Function



Private Sub Initialisation()

'---> Cette proc�dure est charg�e d'initialiser les diff�rentes variables n�c�ssaires au moteur d'impression

Dim Res As Long
Dim lpBuffer As String

On Error Resume Next
'-> Attention, il existe 2 versions du TURBO.

'-> Version Professionelle  -> Libre
'-> Version Entreprise - > Dans architecture

'-> on regarde si on a pas une sauvegarde du fichier ini
If Dir(App.Path & "\TurboGraph.inisov") <> "" Then
    If MsgBox("Un ancien fichier de configuration a �t� trouv�." & Chr(13) & "Si une mise � jour du programme vient d'�tre effectu�e, il est conseill� de r�pondre 'OK'" & Chr(13) & "Ecraser le fichier de configuration par sa sauvegarde?", vbOKCancel, "Sauvegarde du fichier de configuration trouv�e") = vbOK Then
        FileCopy App.Path & "\TurboGraph.inisov", App.Path & "\TurboGraph.ini"
        Kill App.Path & "\TurboGraph.inisov"
    Else
        Kill App.Path & "\TurboGraph.inisov"
    End If
End If

'-> Initialiser les index de cr�ation des prochains objets RichTextFormat et BITMAP
IdRTf = 1
IdBmp = 1

'-> Initialiser la collection des fichiers ouverts dans le TurboGraph
Set Fichiers = New Collection

'-> R�cup�ration des informations de formatage num�rique
lpBuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, lpBuffer, Len(lpBuffer))
SepDec = Mid$(lpBuffer, 1, Res - 1)
lpBuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, lpBuffer, Len(lpBuffer))
SepMil = Mid$(lpBuffer, 1, Res - 1)

'-> R�cup�ration du r�pertoire d'ouverture par d�faut
lpBuffer = Space$(255)
Res = GetEnvironmentVariable("DEALTEMPO", lpBuffer, Len(lpBuffer))
If Res <> 0 Then
    OpenPath = Mid$(lpBuffer, 1, Res)
    If Dir$(OpenPath, vbDirectory) = "" Then OpenPath = ""
End If

'-> R�cup�ration du r�pertoire du param�trage du Turbo dans le cas ou version = 0
lpBuffer = Space$(255)
Res = GetEnvironmentVariable("TURBOINI", lpBuffer, Len(lpBuffer))
If Res <> 0 Then V6TurboIniPath = Mid$(lpBuffer, 1, Res)

'-> on regarde si on doit specifier un nom de repertoire temporaire autre que celui par defaut
lpBuffer = GetIniString("PARAM", "TEMPPATHLOCAL", App.Path & "\TurboGraph.ini", False)
If lpBuffer <> "NULL" Then
    If Trim(lpBuffer) <> "" Then tempPathLocal = Trim(lpBuffer)
End If

'-> R�cup�rer la path du r�pertoire temporaire de windows
TempDirectory = GetTempFileNameVB("", True)

'-> Lancer l'initialisation de la gestion des versions du Turbo
Call InitTurboVersion

End Sub


Public Sub Main()
'---> Point d'entr�e du nouveau module
Dim lpBuffer As String
Dim Res As Long
Dim LigneCommande As String
Dim Param1 As String
'Dim RectoVerso As Boolean
Dim DeviceName As String
Dim ModeName As String
Dim aPrinter As Printer
Dim FindPrinter As Boolean
Dim Ligne As String
Dim i As Integer, j As Integer, k As Integer, l As Integer
Dim PageDebut As Integer
Dim aPath As String
Dim Tempo As String
Dim TempFileName As String
Dim NomFichier As String
Dim aFichier As Fichier
Dim aSpool As Spool
Dim CountPage As Integer

'-> Pour argument sur la ligne de command epour IE, OutLook , HTML
Dim pFile As String
Dim pTo As String
Dim pCopies As String
Dim pFrom As String
Dim pObject As String
Dim pBody As String
Dim pBodyFile As String
Dim pFormat As String
Dim pFormatBody As String
Dim pMode As String
Dim pSpoolName As String
Dim pDir As String
Dim KeyValue As String
Dim pValue As String
Dim pRun As String
Dim pCrypt As String
Dim pPrinter As String
Dim hWndParent
Dim MyCodeLangue As String


' Descriptif de la ligne de commande
'[Param1]|[Param2]

'************************************
'* Envoie de fichier sur imprimante *
'************************************

'PRINTER$DIRECT[$NBCOPIES]|Fichier
'PRINTER$BATCH[$NBCOPIES]|Fichier
'ECRAN|Fichier
'DEFAULT|FICHIER

' \\DEALNT16\HP LaserJet 4000 Maintenance$DIRECT|C:\Travail\OF02362.lgr
' \\DELL_177\EPSON Stylus COLOR 900$DIRECT|C:\Travail\OF02362.lgr
' DIRECT|C:\Travail\OF02362.lgr
' Acrobat PdfWriter~DIRECT~1|D:\Travailv51\infusio.turbo*FileName*Kill[0/1]

'*********************************************************
'* Envoie de spool sur le net, Messagerie ou export HTML *
'*********************************************************

'OUTLOOK|FILE= TO= COPIES= OBJET= BODY= BODYFILE= FORMAT= MODE= SPOOLNAME= CRYPT=
'INTERNET|FILE= TO= COPIES= OBJET= BODY= BODYFILE= FORMAT= MODE= SPOOLNAME= CRYPT=
'HTML|FILE= DIR= MODE= SPOOLNAME= RUN=

' S�parateur $
' FORMAT : 0 -> Format Turbo , 1 -> Format HTML
' CRYPT : indique si le spool doit �tre crypt� : valable que pour le format Turbo
' FLAG pour le format HTML
' MODE : 0 -> Utiliser un navigateur , 1 -> Fichier unique , 2-> Simples Pages
' SPOOLNAME : 0 -> Pad de nom de spool , 1 -> Inclure le nom du spool
' DIR : R�pertoire d'export des fichiers HTML
' Si cet argument est omis , l'argument lu sera celui du fichier Turbograph.ini, Section PARAM , Cl� ExportHTML
' RUN =  0 -> Non , 1 -> OUI

'Ligne commande Export HTML
'HTML|file=D:\travailv51\test.cot$mode=0$spoolname=1run=0

'Ligne Commande Envoie Internet format Turbo
'INTERNET|File=D:\travailv51\test.cot$to=t.cot@deal.fr$objet=Test messagerie$body=Voici le fichier deamnd�$format=0
'avec fichier joint pour le body
'INTERNET|File=D:\travailv51\test.cot$to=t.cot@deal.fr$objet=Test messagerie$bodyfile=d:\travailv51\body.txt$format=0

'Ligne Commande Envoie Internet format HTML
'INTERNET|File=D:\travailv51\test.cot$to=t.cot@deal.fr$objet=Test messagerie$body=Voici le fichier deamnd�$format=1
'avec fichier joint pour le body
'INTERNET|File=D:\travailv51\test.cot$to=t.cot@deal.fr$objet=Test messagerie$bodyfile=d:\travailv51\body.txt$format=1

'Ligne Commande Envoie Outlook format Turbo
'OUTLOOK|File=D:\travailv51\test.cot$to=thierry cot$objet=Test messagerie$body=Voici le fichier demand�$format=0
'avec fichier joint pour le body
'OUTLOOK|File=D:\travailv51\test.cot$to=thierry cot$objet=Test messagerie$bodyfile=d:\travailv51\body.txt$format=0

'Ligne Commande Envoie Outlook format HTML
'OUTLOOK|File=D:\travailv51\test.cot$to=thierry cot$objet=Test messagerie$body=Voici le fichier demand�$format=1
'avec fichier joint pour le body
'OUTLOOK|File=D:\travailv51\test.cot$to=thierry cot$objet=Test messagerie$bodyfile=d:\travailv51\body.txt$format=1

'MESSAGERIE|File=D:\spool\test.turbo$to=d.marchal@deal.fr$objet=Test messagerie$bodyfile=d:\spool\body.txt$format=1

'Ligne Commande de batch envoi Email avec pdf
'BATCH|File=D:\travail\test.text$Suppr=1

'Mailing en automatique
'MAILING|File=D:\travail\test.text$Suppr=1

'-> On ajoute une entr�e a la ligne de commande : |RV si recto verso demand�

'-> Activer la gestion des erreurs
On Error GoTo InterceptError
Trace "Version du programme : " & App.Major & "." & App.Minor & "." & App.Revision

Zoom = 1
curPage = 1
TestTableur = True
TestOutlook = True
topDirect = False
HideMessagerie = False
topForce = False
lockToolbar = True
'-> Lancer l'initialisation
Call Initialisation

'-> on memorise la liste des imprimantes
On Error Resume Next
getPrintersList
On Error GoTo InterceptError

wVersion = GetWindowsVersion

If GetIniString("PARAM", "LOCKTOOLBAR", TurboGraphIniFile, False) = "0" Then lockToolbar = False

'-> Si on est en version internet
If VersionTurbo = 1 Then
    On Error Resume Next
    '-> V�rifier que l'on trouve le fichier langue
    If Dir$(App.Path & "\TurboPrint1-" & FORMAT(IndexLangue, "00") & ".lng", vbNormal) = "" Then
        '-> On ne trouve pas le fichier : Charger la ressource en interne
        LoadInternetMessProg
    Else
        '-> Charger le fichier messprog
        RessourcePath = App.Path & "\"
        If Not (CreateMessProg("TurboPrint1-" & FORMAT(IndexLangue, "00") & ".lng", True)) Then LoadInternetMessProg
    End If
    On Error GoTo InterceptError
    Verify_Update
    '-> on regarde si on faire le nettoyage
    GarbageSpool
Else
    '-> V�rifier que l'on trouve le fichier langue
    If Dir$(FichierLangue & "\TurboPrint1-" & FORMAT(IndexLangue, "00") & ".lng", vbNormal) = "" Then
        '-> On ne trouve pas le fichier : Charger la ressource en interne
        LoadInternetMessProg
    Else
        '-> Charger le fichier messprog
        RessourcePath = FichierLangue & "\"
        If Not (CreateMessProg("TurboPrint1-" & FORMAT(IndexLangue, "00") & ".lng", True)) Then LoadInternetMessProg
    End If
End If 'Selon la version


'********************************************************************************
'* ANALYSE DE LA LIGNE DE COMMANDE ET GESTION DES DIFFERENTS MODES D'IMPRESSION *
'********************************************************************************

'-> R�cup�ration de la ligne de commande
LigneCommande = Command()
LigneCommande = LTrim(LigneCommande)
Trace "Ligne de commande :" + LigneCommande, 1

'-> Supprimer les Quotters s'il y en a
If InStr(1, LigneCommande, """") = 1 Then _
    LigneCommande = Mid$(LigneCommande, 2, Len(LigneCommande) - 1)
If InStr(1, LigneCommande, """") <> 0 Then _
LigneCommande = Mid$(LigneCommande, 1, Len(LigneCommande) - 1)

'-> Initialisation des param�tres par d�faut
mode = 1 'Mode ECRAN
nCopies = 1 'Nombre de copies
InitConvertion
If Trim(LigneCommande) <> "" Then

    '-> Recherche d'un "|"
    If InStr(1, LigneCommande, "|") <> 0 Then
        
        '-> R�cup�ration du nom de fichier pass� en param�tre
        NomFichier = Entry(2, LigneCommande, "|")
        '-> on lui rajoute eventuellement le nom du repertoire
        If InStr(1, NomFichier, "/") = 0 And InStr(1, NomFichier, "\") = 0 Then
            NomFichier = OpenPath + "\" + NomFichier
            Trace NomFichier, 1
            'bon on sait jamais par precaution
            If Dir$(NomFichier, vbNormal) = "" Then
                NomFichier = LigneCommande
            End If
        End If
        
        '-> R�cup�ration des param�tres d'impression
        Param1 = Entry(1, LigneCommande, "|")
        
        '-> Recup du parametre recto verso
        If Trim(UCase$(Entry(3, LigneCommande, "|"))) = "RV" Or Trim(UCase$(Entry(3, LigneCommande, "|"))) = "RVP" Then nRectoVerso = True
        
        '-> Tester si on a pass� le code op�rateur sur la ligne
        If InStr(1, Param1, "$") <> 0 Then
            '-> Si entry 1 = "ECRAN, on a pass� le code langue
            If UCase$(Trim(Entry(1, Param1, "$"))) = "ECRAN" Or UCase$(Trim(Entry(1, Param1, "$"))) = "GETPRINTERS" Then
                '-> R�cup�rer le code langue sp�cifi�
                MyCodeLangue = Trim(Entry(2, Param1, "$"))
                If IsNumeric(MyCodeLangue) Then
                    '-> V�rifier si on trouve le fichier langue associ�
                    RessourcePath = FichierLangue & "\"
                    Do While Libelles.Count <> 0
                        Libelles.Remove (1)
                    Loop
                    '-> V�rifier que l'on trouve le fichier langue
                    If Dir$(FichierLangue & "\TurboPrint1-" & FORMAT(MyCodeLangue, "00") & ".lng", vbNormal) = "" Then
                        '-> On ne trouve pas le fichier : Charger la ressource en interne
                        LoadInternetMessProg
                    Else
                        '-> Charger le fichier messprog
                        RessourcePath = FichierLangue & "\"
                        If Not (CreateMessProg("TurboPrint1-" & FORMAT(MyCodeLangue, "00") & ".lng", True)) Then LoadInternetMessProg
                    End If
                    '-> Poser les bonnes valeurs
                    If UCase$(Trim(Entry(1, Param1, "$"))) = "GETPRINTERS" Then
                        Param1 = "GETPRINTERS"
                        IndexLangue = CInt(MyCodeLangue)
                    End If
                End If '-> Si on a pass� un code langue correcte
            End If 'Si on est en mode �cran
        End If
            
        '-> Cas de l'internet et du HTML
        If UCase$(Param1) = "INTERNET" Then
            mode = 4
            GoTo suite
        ElseIf UCase$(Param1) = "OUTLOOK" Then
            mode = 5
            GoTo suite
        ElseIf UCase$(Param1) = "HTML" Then
            mode = 6
            GoTo suite
        ElseIf UCase$(Param1) = "LOTUS" Then
            mode = 7
            GoTo suite
        ElseIf UCase$(Param1) = "MESSAGERIE" Then
            mode = 8
            GoTo suite
        ElseIf UCase$(Param1) = "DEALPDF" Then
            mode = 9
            GoTo suite
        ElseIf Entry(1, UCase$(Param1), "~") = "TURBOPDF" Then
            mode = 9
        ElseIf Entry(1, UCase$(Param1), "~") = "GETPRINTERS" Then
            GetPrinterList NomFichier
        ElseIf UCase$(Param1) = "BATCH" Then
            '-> on lance en batch les lignes de commande
            RunBatch
            '-> quitter une fois les lignes traitees
            End
        ElseIf UCase$(Param1) = "MAILING" Then
            mode = 10
            GoTo suite
        ElseIf UCase$(Param1) = "GESTSPOOL" Then
            mode = 11
            GoTo suite
        ElseIf UCase$(Param1) = "TABLEUR" Then
            mode = 12
            GoTo suite
        ElseIf UCase$(Entry(1, Param1, "~")) = "ACROBAT PDFWRITER" Or UCase$(Entry(1, Param1, "~")) = "ACROBAT DISTILLER" Or InStr(1, UCase$(Entry(1, Param1, "~")), "ADOBE PDF") <> 0 Or UCase$(Entry(1, Param1, "~")) = "WIN2PDF" Or UCase$(Entry(1, Param1, "~")) = "PDFCREATOR" Then
            '-> Indiquer que l'on est en mode PDF
            IsPDF = True
            '-> Tester si on a sp�cifi� le nom du fichier PDF
            If InStr(1, NomFichier, "*") = 0 Then
                '-> Composer le nom du fichier PDF
                PdfDestinationFile = NomFichier & ".pdf"
                '-> On ne supprime pas
                KillSpool = False
            Else
                '-> R�cup�rer le fichier Destination
                PdfDestinationFile = Entry(2, NomFichier, "*")
                '-> Si on doit supprimer ou non
                If Trim(Entry(3, NomFichier, "*")) = "1" Then
                    KillSpool = True
                Else
                    KillSpool = False
                End If
                '-> Recomposer le nom du fichier
                NomFichier = Entry(1, NomFichier, "*")
            End If 'Si on a specifi� le nom du fichier PDF
            '-> Modifier le Registre si necessaire

            If Trim(PdfDestinationFile) <> "" Then
                If UCase$(Entry(1, Param1, "~")) = "WIN2PDF" Then
                    SetWin2PDFWriterFileName PdfDestinationFile
                Else
                    SetPdfWriterFileName PdfDestinationFile
                End If
                If UCase$(Entry(1, Param1, "~")) = "PDFCREATOR" Then SetPdfCreatorFileName PdfDestinationFile
                If UCase$(Entry(1, Param1, "~")) = "ADOBE PDF" Then SetAdobePdfFileName PdfDestinationFile
           End If
        End If
        '-> V�rifier que le fichier existe
        If Trim(Dir$(NomFichier, vbNormal)) <> "" Then
                        
            '-> Traiter la ligne de commande pour les imprimantes
            If InStr(1, Param1, "ECRAN", vbTextCompare) = 0 And InStr(1, Param1, "VISUALISATION", vbTextCompare) = 0 Then
                '-> Tester si on a trouv� un ~
                If InStr(1, Param1, "~") <> 0 Then
                    DeviceName = Entry(1, Param1, "~") 'Nom de l'imprimante
                    ModeName = UCase$(Entry(2, Param1, "~")) 'Mode d'impression BATCH/DIRECT
                    Trace "Param�trage avec des ~", 1
                    If NumEntries(Param1, "~") = 3 Then
                        '-> Le nombre de copie est sp�cifi�
                        Tempo = Entry(3, Param1, "~")
                        If IsNumeric(Tempo) Then nCopies = CInt(Tempo)
                    End If
                Else
                    DeviceName = Entry(1, Param1, "$") 'Nom de l'imprimante
                    ModeName = UCase$(Entry(2, Param1, "$")) 'Mode d'impression BATCH/DIRECT
                    Trace "Param�trage avec des $", 1
                    If NumEntries(Param1, "$") = 3 Then
                        '-> Le nombre de copie est sp�cifi�
                        Tempo = Entry(3, Param1, "$")
                        If IsNumeric(Tempo) Then nCopies = CInt(Tempo)
                    End If
                End If
                On Error Resume Next
                'Tester si on demande d'imprimer sur l'imprimante par d�faut
                If UCase$(Trim(DeviceName)) = "DEFAULT" Then
                    DeviceName = "SCREEN" 'pour le cas ou le printer explose
                    DeviceName = Printer.DeviceName
                    If DeviceName = "SCREEN" Then mode = 1
                End If
                If UCase$(Trim(DeviceName)) = "DEFAULT" And nPrinter <> "" Then DeviceName = nPrinter
                    Trace "Recherche de l'imprimante : " & DeviceName, 1
                    Trace "Mode d'impression : " & ModeName, 1
                On Error GoTo InterceptError
                '-> Modifier le printer pour le mettre en ad�quation avec la demande
                FindPrinter = False
                On Error Resume Next
                For Each TempPrinter In Printers
                        Trace "On analyse l'imprimante : " & TempPrinter.DeviceName, 1
                        Trace "On cherche l'imprimante : " & DeviceName, 1
                    If UCase$(TempPrinter.DeviceName) = UCase$(DeviceName) Then
                        FindPrinter = True
                        Set Printer = TempPrinter
                        Trace "Imprimante trouv�e : " & Printer.DeviceName, 1
                        Exit For
                    End If
                Next 'Pour toutes les imprimantes
                On Error GoTo InterceptError
                'If Trim(UCase(DeviceName)) = "TURBOPDF" Then FindPrinter = True
                
                '-> V�rifier si imprimante Ok
                If FindPrinter Then
                    '-> On a trouver le printer sp�cifi� : s�lectionner un mode d'impression
                    Select Case UCase$(ModeName)
                        Case "BATCH"
                            mode = 3
                        Case Else 'Mode DIRECT ou inconnu
                            mode = 2
                    End Select
                Else
                    If UCase(ModeName) = "BATCH" Then End
                End If 'Si on a trouv� un printer
                
                '-> Tester si le nombre de copies est renseign�
                If NumEntries(Param1, "$") = 3 Then
                    '-> Le nombre de copie est sp�cifi�
                    Tempo = Entry(3, Param1, "$")
                    If IsNumeric(Tempo) Then nCopies = CInt(Tempo)
                    
                End If 'Si le nombre de copie est sp�cifi�
            End If 'Si on passe un mode <> �cran
        End If 'Si on trouve le fichier pass� en param�tre
    Else
        '-> Pas de "|" donc le paramettre est sens� �tre un fichier : v�rifier s'il existe
        If Dir$(LigneCommande, vbNormal) <> "" Then
            NomFichier = LigneCommande
            '-> on lui rajoute eventuellement le nom du repertoire
            If InStr(1, NomFichier, "/") = 0 And InStr(1, NomFichier, "\") = 0 Then
                NomFichier = OpenPath + "\" + NomFichier
                Trace NomFichier, 1
                'bon on sait jamais par precaution
                If Dir$(NomFichier, vbNormal) = "" Then
                    NomFichier = LigneCommande
                End If
            End If
        Else
            NomFichier = ""
        End If
    End If 'S'il y a un "|"
End If 'Si la ligne de commande est vide
        
'****************************************************************
'* ANALYSE DU FICHIER ASCII POUR CREATION DES DIFFERENTS OBJETS *
'****************************************************************

suite:

'-> Dans tous les cas, charger la feuille bibliotheque : elle servira � stocker les diff�rents objets
Load frmLib

'******************************
'* TRAITEMENT DE L'IMPRESSION *
'******************************

If IsMouchard Then
    Trace "Mode d'impression : " & mode, 1
    'Close #hdlMouchard
End If

'ATTENTION : V6 Modification pour la gestion du ZIP : on teste si le fichier est ZIPE. S'il est ZIP,
'-> pour les fichiers internet les copier dans le bon repertoire
If InStr(1, NomFichier, ".IE5", vbTextCompare) <> 0 Or InStr(1, NomFichier, "inetcache", vbTextCompare) <> 0 Or InStr(1, NomFichier, "appdata", vbTextCompare) <> 0 Or InStr(1, NomFichier, "Downloads", vbTextCompare) <> 0 Then
    If TurbosavePath <> "" Then
        On Error GoTo suite2
        Trace "On est en internet"
        Dim strFileNameTemp As String
        strFileNameTemp = Replace(NomFichier, "[1]", "")
        strFileNameTemp = Replace(strFileNameTemp, "[2]", "")
        strFileNameTemp = Replace(strFileNameTemp, "[3]", "")
        strFileNameTemp = Replace(strFileNameTemp, "[4]", "")
        strFileNameTemp = Replace(strFileNameTemp, " (1)", "")
        strFileNameTemp = Replace(strFileNameTemp, " (2)", "")
        strFileNameTemp = Replace(strFileNameTemp, " (3)", "")
        strFileNameTemp = Replace(strFileNameTemp, " (4)", "")
        If FileExist(TurbosavePath & GetFileName(Replace(strFileNameTemp, "[1]", ""))) And Not TurboOverWrite Then
            Trace "pas de copie car fichier deja present"
        Else
            Trace "avant le copy" & NomFichier & "#??#" & TurbosavePath & GetFileName(strFileNameTemp)
            FileCopy NomFichier, TurbosavePath & GetFileName(strFileNameTemp)
            Trace "apres le copy"
        End If
        NomFichier = TurbosavePath & GetFileName(strFileNameTemp)
suite2:
    Trace "description erreur eventuelle" & Err.Description
    'If Err.Description = "" Then
    '        NomFichier = Entry(2, LigneCommande, "|")
    '        Trace "tentative avec " & NomFichier
    'End If
    End If
End If
'decompression dans le fichier temporaire
FilePath = Mid(NomFichier, 1, InStrRev(Replace(NomFichier, "\", "/"), "/"))
NomFichier = GetUnzipFileName(NomFichier)
If InStr(1, LigneCommande, "topForce", vbTextCompare) <> 0 Then topForce = True
Select Case mode

    Case 1 '-> Impression ECRAN
    
        'dans le cas des packages
        If InStrRev(NomFichier, ".zip", , vbTextCompare) = Len(NomFichier) - 3 Then
            Dim fileName As String
            NomFichier = uZipInfo2
            For i = 1 To NumEntries(NomFichier, ",")
                '-> Test sur le ZIP
                fileName = GetUnzipFileName(Entry(i, NomFichier, ","))
                '-> Lancer le chargement du fichier
                If i = 1 Then
                    DisplayFileGUI fileName
                Else
                    DisplayFileTree fileName
                End If
                '-> Charger eventuellement au menu les fichiers joints
                LoadJoinFile uZipInfo2, fileName
            Next
            NomFichier = ""
        End If
        '-> Afficher dans un premier temps l'interface de chargement
        'MDIMain.Show
        '-> Charger le fichier dans l'interface
        If NomFichier <> "" Then
            '-> Bloquer la feuille MDI
            MDIMain.Enabled = False
            '-> Afficher le fichier
            DisplayFileGUI NomFichier
            '-> Debloquer la feuille MDI
            MDIMain.Enabled = True
        End If
        '-> Charger eventuellement au menu les fichiers joints
        LoadJoinFile uZipInfo2, NomFichier
        MDIMain.Show
        Unload frmWait
  Case 2, 3  '-> Impression en mode direct ,  Impression en batch
    
        '-> Setting du p�riph�rique de sortie
        Set Sortie = Printer
        
        '-> Passer le printer en mode pixel
        Printer.ScaleMode = 3
                
        '-> Lancer l'analyse du spool
        NomFichier = AnalyseFileToPrint(NomFichier)
        
        '-> si une imprimante est sp�cifi� dans le spool et qu'elle existe on pointe dessus en priorit�
        If nPrinter <> "" Then
                For Each TempPrinter In Printers
                    If UCase$(TempPrinter.DeviceName) = UCase$(nPrinter) Then
                        Set Printer = TempPrinter
                        Trace "Imprimante prioritaire trouv�e : " & Printer.DeviceName, 1
                        '-> Passer le printer en mode pixel
                        Printer.ScaleMode = 3
                        Exit For
                    End If
                Next 'Pour toutes les imprimantes
        End If
        
        '-> si on doit calculer la hauteur reelle du spool
        If Fichiers(UCase$(NomFichier)).Spools(1).maquette.Hauteur = 99 Then
            PreprintPageSpool Fichiers(UCase$(NomFichier)).Spools(1), 1
            Printer.height = Sortie.ScaleY(Fichiers(UCase$(NomFichier)).Spools(1).maquette.HauteurVar, 7, 1)
            Set Sortie = Printer
        End If
        
        '-> Pointer sur l'objet fichier
        Set aFichier = Fichiers(UCase$(NomFichier))
        
        '-> on regarde si on a des copies assembl�es
        If Trim(UCase$(Entry(4, LigneCommande, "|"))) = "A" Then copyAssemb = True
        
        '-> on regarde si on doit pas imprimer la page de garde
        If Trim(UCase$(Entry(5, LigneCommande, "|"))) = "NOGARDE" Then noGarde = True
        
        '-> on regarde si on doit pas imprimer la page de garde
        If Trim(UCase$(Entry(6, LigneCommande, "|"))) = "FILESJOINS" Then printFilesJoins = True
        
        '-> Pour le nombre de copies sp�cifi�
Trace ("nCopies")
        For j = 1 To nCopies
            curPage = j
            '-> Lancer l'impression des pages
            For Each aSpool In aFichier.Spools
                On Error Resume Next
                '-> Initialisation du mode de l'imprimante
                Trace ("av init")
                If aSpool.maquette Is Nothing Then InitCom aSpool
                Trace ("ap init")
                If aSpool.maquette.Orientation = 1 Then
                    Printer.Orientation = 1
                Else
                    Printer.Orientation = 2
                End If
                Trace ("ap orientation")
                '-> PIERROT DUPLEX selon que portrait ou paysage
                Printer.Duplex = 1
                If nRectoVerso Then
                    If Printer.Orientation = 1 Then
                        Printer.Duplex = 2
                    Else
                        Printer.Duplex = 3
                    End If
                End If
                Trace ("ap recto verso")
                On Error GoTo InterceptError
                '-> Pour g�rer la mise en page
                If Trim(UCase$(Entry(3, LigneCommande, "|"))) = "RVP" Or Trim(UCase$(Entry(3, LigneCommande, "|"))) = "NORVP" Then
                    Dim dlg As New PropertiesDialog
                    dlg.Hwnd = frmPrint.Hwnd 'Me.hWnd
                    dlg.ShowPrinter
                    Set dlg = Nothing
                    Trace ("show printer ????")
                End If
                Trace ("avant printer")
                '-> Initialiser le printer
                'Trace "Relecture du registre avant printer.print : " + GetRegKeyValue(HKEY_CURRENT_USER, "SoftWare\Adobe\Acrobat Distiller\PrinterJobControl", App.Path + "\" + App.EXEName & ".exe") + " et le command$=" + Command$
                Printer.Print ""
Trace ("retour printer")
                '-> cause bug microsoft on definit bien le fond transparent
                SetBkMode Sortie.hdc, 1
                
                '-> Initialiser le compteur de page imprim� dans le spool
                SpoolPrint = 0
                            
                '-> Tester s'il ya des erreurs
                If aSpool.NbError = 0 Then
                    '-> Tester si on doit imprimer la page des s�lections
                    If aSpool.IsSelectionPage Then
                        CountPage = 0
                    Else
                        CountPage = 1
                    End If
                    
                    If noGarde Then CountPage = 1
                    
                    For i = CountPage To aSpool.nbPage
                        '-> Tester s'il y a des erreurs sur la page sp�cifi�e
                        If aSpool.GetErrorPage(i) <> "" Then
                            '-> R�cup�rer la liste des erreurs
                            Tempo = aSpool.GetErrorPage(i)
                            '-> Imprrmer la liste des erreurs
                            For k = 1 To NumEntries(Tempo, Chr(0))
                                Printer.Print Entry(i, Tempo, Chr(0))
                            Next '-> Pour toutes les erreurs
                        Else
                            '-> Tester la taille du lot de page
                            If SpoolSize <> 0 Then
                                If SpoolPrint = SpoolSize Then
                                    '-> Terminer le document en cours
                                    Trace "Avant impression si spoolsize", 1
                                    Printer.EndDoc
                                    '-> Initialisation du mode de l'imprimante
                                    If aSpool.maquette.Orientation = 1 Then
                                        Printer.Orientation = 1
                                    Else
                                        Printer.Orientation = 2
                                    End If
                                    
                                    '-> Initialiser le printer
                                    Printer.Print ""
                                    '-> Initialiser le compteur de page imprim� dans le spool
                                    SpoolPrint = 0
                                End If 'Si on atteint le nombre limitte de page
                            End If 'Si param�trage du spooler par lot
                            If copyAssemb Then
                                For l = 1 To nCopies
                                    '-> Imprimer la page
                                    curPage = l
                                    PrintPageSpool aSpool, i
                                    If l <> nCopies Then Printer.NewPage
                                Next
                            Else
                                '-> Imprimer la page
                                PrintPageSpool aSpool, i
                            End If
                            '-> Gerer le debordement si le top de debordement est activer on refait l'impression avec une translation
                            If DebordementTop And Not TypeOf Sortie Is PictureBox Then
                                '-> on reinitialise le top
                                i = i - 1
                            End If
                            '-> Incr�menter le compteur de page imprim�
                            SpoolPrint = SpoolPrint + 1
                        End If
                        '-> Envoyer un saut de page si pas derni�re page
                        If i <> aSpool.nbPage Then Printer.NewPage
                        '-> on nettoye
                        'Set aSpool = Nothing
                    Next 'Pour toutes les pages
                Else
                    '-> Imprrmer la liste des erreurs
                    For k = 1 To aSpool.NbError
                        Printer.Print aSpool.GetErrorMaq(k)
                    Next '-> Pour toutes les erreurs
                End If
                '-> Fin du document pour ce spool
                Trace "Avant l'impression", 1
                Printer.EndDoc
                '-> si on doit imprimer les pi�ces jointes
                If printFilesJoins Then
                    If aSpool.Num_Spool = 1 Then
                        For Each aPrinter In Printers
                            If aPrinter.DeviceName = DeviceName Then Set Printer = aPrinter
                        Next
                        For i = 1 To NumEntries(Fichiers(UCase$(aSpool.fileName)).filesJoins, ",")
                            Trace Entry(i, Fichiers(UCase$(aSpool.fileName)).filesJoins, ",")
                            ShellExecute 0, "Print", Entry(i, Fichiers(UCase$(aSpool.fileName)).filesJoins, ","), DeviceName, App.Path, 1
                        Next
                    End If
                End If
            Next
            If copyAssemb Then j = nCopies
        Next 'Pour toutes les copies
        
        '-> Si on est en mode PDF et que l'on doit supprimer la source
        If IsPDF And KillSpool Then Kill NomFichier
        
        '-> Fin du programme
        End
        
    Case 4, 5, 6, 7, 8 'INTERNET, OUTLOOK , HTML, MESSAGERIE
    
        '-> Initialiser le p�riph�rique de sortie
        Set Sortie = frmLib.PicObj(0)
    
        '-> Analyse des param�tres pass�s en ligne de commande
        For i = 1 To NumEntries(NomFichier, "$")
            KeyValue = UCase$(Trim(Entry(1, Entry(i, NomFichier, "$"), "=")))
            pValue = Trim(Entry(2, Entry(i, NomFichier, "$"), "="))
            
            Select Case KeyValue
                Case "FILE"
                    pFile = pValue
                Case "TO"
                    pTo = pValue
                Case "COPIES"
                    pCopies = pValue
                Case "FROM"
                    pFrom = pValue
                Case "OBJET"
                    pObject = pValue
                Case "BODY"
                    pBody = pValue
                Case "BODYFILE"
                    pBodyFile = pValue
                Case "FORMAT"
                    pFormat = pValue
                Case "FORMATBODY"
                    pFormatBody = pValue
                Case "MODE"
                    pMode = pValue
                Case "SPOOLNAME"
                    pSpoolName = pValue
                Case "DIR"
                    pDir = pValue
                Case "RUN"
                    pRun = pValue
                Case "CRYPT"
                    pRun = pValue
                Case "PRINTER"
                    pPrinter = pValue
            End Select
        Next
    
        '-> Dans tous les cas, v�rifier si le fichier pass� en argument existe
        If Dir$(pFile, vbNormal) = "" Then End
        FilePath = Mid(pFile, 1, InStrRev(Replace(pFile, "\", "/"), "/"))
        '-> Setting du nom de fichier
        FileToExport = GetFileName(pFile)
        FileToExport = GetSpoolName(FileToExport) & ".html"
        
        '-> Setting de la cl� d'acc�s au fichier format HTML
        FileKeyExport = UCase$(Trim(pFile))

        '-> Setting de la cl� d'acces au fichier export Messagerie
        FileKeyMail = FileKeyExport

        '-> En mode  Internet, Outlook
        If mode = 4 Or mode = 5 Then
            '-> v�rifier si destinataire saisi
            If Trim(pTo) = "" Then End
            '-> V�rifier le format par d�faut
            If Not IsNumeric(pFormat) Then
                '-> Turbo par d�faut
                pFormat = "0"
            Else
                '-> Convertir
                If CLng(pFormat) <> 1 And CLng(pFormat) <> 4 And CLng(pFormat) <> 3 Then
                    pFormat = "0" 'Turbo par def
                End If
            End If
        End If
        
        '-> Tester si crypter ou non
        If Not IsNumeric(pCrypt) Then
            IsCryptedFile = False
        Else
            If CLng(pCrypt) <> "1" Then
                IsCryptedFile = False
            Else
                IsCryptedFile = True
            End If
        End If
        
        '-> Pour format HTML : format par d�faut Fichier Unique
        If pFormat = "1" Then
            If Not IsNumeric(pMode) Then
                '-> Fichier unique
                pMode = "1"
            Else
                If CLng(pMode) < 0 Or CLng(pMode) > 2 Then
                    pMode = "1"
                End If
            End If
        Else
            If pFormat <> "4" And CLng(pFormat) <> 3 Then pFormat = "0"
        End If
        
        '-> type de navigation
        If pMode <> "" Then NavigationType = CInt(pMode)
        
        '-> Export du fichier dans sa totalit�
        ExportType = 2
        ExportMail = 2
            
        '-> Inclure le nom du spool
        If pSpoolName = "" Then
            pSpoolName = "0"
        Else
            If Not IsNumeric(pSpoolName) Then
                pSpoolName = "0"
            Else
                If CLng(pSpoolName) <> 0 And CLng(pSpoolName) <> 1 Then pSpoolName = "0"
            End If
        End If
        
        '-> Inclusion des noms des spools
        IncluseSpoolNumber = CBool(pSpoolName)
    
        '-> Run after
        If IsNumeric(pRun) Then
            If CLng(pRun) = 1 Then
                RunAfterExport = True
            Else
                RunAfterExport = False
            End If
        Else
            RunAfterExport = False
        End If
            
        '-> R�pertoire d'export si export Format HTML
        If Param1 = "HTML" Then
            If pDir = "" Then
                If PathToExport = "" Then
                    End
                Else
                    If GetAttr(PathToExport) And vbDirectory <> vbDirectory Then End
                End If
            Else
                If GetAttr(pDir) And vbDirectory <> vbDirectory Then
                    End
                Else
                    PathToExport = pDir
                End If
            End If
        
            '-> Traitement du "\" du fin de path
            If Right$(PathToExport, 1) <> "\" Then PathToExport = PathToExport & "\"
        End If
    
        '-> Pas de temporisation �cran
        IsTempo = False
        
        '-> Setting de l'origine export
        OrigineExport = 3
        OrigineMail = OrigineExport
        
        '-> Cr�ation du mod�le objet en m�moire
        AnalyseFileToPrint pFile
        
        '-> si internet on regarde la messagerie possible existante
        If mode = 4 Then
            If IsLotus Then mode = 7
            If IsOutLook Then mode = 5
        End If
        
        '-> Lancement du traitement
        If mode = 6 Then
            '-> Mode Export HTML
            CreateEditionHTML
        Else
            Select Case mode
                Case 4  'Internet
                    '-> Setting de la variable d'export
                    NetParam = pTo & Chr(0) & pObject & Chr(0) & pBody
                    Select Case pFormat
                        Case "0"   'Format Turbo
                            SendToInternet FileKeyExport, pBodyFile
                        Case "4" 'format pdf
                            CreatePdfToMessagerie pFile, pPrinter
                            '-> on pointe sur le nouveau fichier pdf cr��
                            'FileKeyExport = FileKeyExport & ".PDF"
                            CreateHTMLToBody False, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile & Chr(0) & pFormatBody & Chr(0) & pFormat
                        Case "3" 'sans piece jointe
                            CreateHTMLToBody False, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile & Chr(0) & pFormatBody & Chr(0) & pFormat
                        Case Else 'Format HTML
                            CreateHTMLToMessagerie False, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile
                    End Select
                Case 5, 7 'Outlook, Lotus Note
                    '-> Envoyer le mail par outlook
                    If mode = 5 Then
                        SendOutLook = True
                        SendLotus = False
                    End If
                    If mode = 7 Then
                        SendLotus = True
                        SendOutLook = False
                    End If
                    
                    
                    
                    Select Case pFormat 'format piece jointe
                        Case "0"  'Format Turbo
                            If pFormatBody <> "1" Then
                                '-> corps rtf
                                SendToOutLook FileKeyExport, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile
                            Else
                                '-> corps HTML
                                CreateHTMLToBody True, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile & Chr(0) & pFormatBody & Chr(0) & pFormat
                            End If
                        Case "3" 'sans piece jointe
                            CreateHTMLToBody True, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile & Chr(0) & pFormatBody & Chr(0) & pFormat
                        Case "4" 'Format Pdf
                            CreatePdfToMessagerie pFile, pPrinter
                            '-> on pointe sur le nouveau fichier pdf cr��
                            'FileKeyExport = FileKeyExport & ".PDF"
                            CreateHTMLToBody True, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile & Chr(0) & pFormatBody & Chr(0) & pFormat
                        Case Else 'Format HTML
                            If pFormatBody <> "1" Then
                                '-> corps rtf
                                CreateHTMLToMessagerie True, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile
                            Else
                                '-> corps Html
                                CreateHTMLToBody True, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile & Chr(0) & pFormatBody & Chr(0) & pFormat
                            End If
                    End Select
                Case 8 'par messagerie
                    sendToSMTP pFormat, pFrom, pFile, pCopies, pTo, pObject, pBody, pBodyFile
            End Select
        End If
            
        '-> End fin du traitement
        End
    Case 9 'DEALPDF cr�ation d'un fichier PDF en natif

        Shell Chr(34) & App.Path & "\TurboPdf.exe" & Chr(34) & " �fileToConvert=" & NomFichier & "�SORTIE=ECRAN�embedded"
        End
    Case 10 'Mailing eclatement et envoi des fichiers en mailing
        '-> analyse des param�tres
        For i = 1 To NumEntries(NomFichier, "$")
            KeyValue = UCase$(Trim(Entry(1, Entry(i, NomFichier, "$"), "=")))
            pValue = Trim(Entry(2, Entry(i, NomFichier, "$"), "="))
            Select Case KeyValue
                Case "FILE"
                    pFile = pValue
                Case "TO"
                    pTo = pValue
                Case "COPIES"
                    pCopies = pValue
                Case "FROM"
                    pFrom = pValue
                Case "OBJET"
                    pObject = pValue
                Case "BODY"
                    pBody = pValue
                Case "FORMAT"
                    pFormat = pValue
            End Select
        Next
        If Dir(pFile) = "" Then
            MDIMain.Caption = "**FIN**"
            MDIMain.TerminateApp "**FIN**"
            Exit Sub
        End If
        '-> Afficher dans un premier temps l'interface de chargement
        MDIMain.Show
        '-> Charger le fichier dans l'interface
        If NomFichier <> "" Then
            '-> Bloquer la feuille MDI
            MDIMain.Enabled = False
            MDIMain.Visible = False
            '-> Afficher le fichier
            DisplayFileGUI pFile
            '-> Debloquer la feuille MDI
            MDIMain.Enabled = True
            MDIMain.Visible = False
            '-> Charger eventuellement au menu les fichiers joints
            LoadJoinFile uZipInfo2, pFile
            '-> on envoye les mails
            sendMailing pFile, pCopies, pFrom, pObject, pBody, pFormat
        End If
        If pFrom <> "" Then
            MDIMain.Caption = "**FIN**"
            MDIMain.TerminateApp "**FIN**"
            End
        End If
    Case 11 'le gestionnaire des spools
        frmGestSpool.Show
    Case 12 'les export excel
        For i = 1 To NumEntries(NomFichier, "$")
            KeyValue = UCase$(Trim(Entry(1, Entry(i, NomFichier, "$"), "=")))
            pValue = Trim(Entry(2, Entry(i, NomFichier, "$"), "="))
            Select Case KeyValue
                Case "FORMAT"
                    pFormat = pValue
                Case "REGROUP"
                    pFormatBody = pValue
            End Select
        Next
        NomFichier = Entry(1, NomFichier, "$")
        If IsExcel Then
            '-> Lancer l'analyse du spool
            NomFichier = AnalyseFileToPrint(NomFichier)
            Set aSpool = Fichiers(UCase$(NomFichier)).Spools(1)
            frmExcel.FichierName = UCase$(Trim(aSpool.fileName))
            frmExcel.SpoolKey = UCase$(Trim(aSpool.Key))
            frmExcel.Init
            Select Case pFormat
                Case "0"
                    frmExcel.isFormat = False
                Case "1"
                    frmExcel.isFormat = True
            End Select
            Select Case pFormatBody
                Case "0"
                    frmExcel.Option8(0).Value = True
                Case "1"
                    frmExcel.Option8(0).Value = False
                    frmExcel.pNomFeuille = True
            End Select
            frmExcel.ExportAllWithFormat
        Else
        
        End If
        End
End Select

Exit Sub
 

InterceptError:


    
    If Err.Number <> 482 And Not FindPrinter Then MsgBox "Erreur dans la procedure MAIN" & Chr(13) & Err.Number & " - " & Err.Description, vbCritical + vbOKOnly, "Deal Informatique"
    '-> Appeler la gestion des erreurs
    If FileExist(Mouchard) Then
        Close #hdlMouchard
        hdlMouchard = 0
        CopyFile Mouchard, Mouchard & ".txt", False
        ShellExecute 0, "Open", Mouchard & ".txt", vbNullString, "", 1
    End If
    End

End Sub

Private Sub sendMailing(pFile As String, pCopies As String, pFrom As String, pObject As String, pBody As String, pFormat As String)
'--> cette procedure permet d'envoyer les mailingh par la ligne de commande
Dim aFichier As Fichier
Dim aNode As Node

On Error Resume Next

'-> Pointer sur l'objet fichier
Set aFichier = Fichiers(1)
Set frmMailList.aSpool = aFichier.Spools(1)
        
'-> on charge la feuille
iniName = "TurboMail.ini"
frmMailList.Show
frmMailList.FichierName = UCase$(Trim(aFichier.Spools(1).fileName))
frmMailList.SpoolKey = UCase$(Trim(aFichier.Spools(1).Key))
frmMailList.Init
'-> on se place sur le bon onglet
frmMailList.SSTab1.Tab = 2
frmMailList.Text3.Text = pFrom
frmMailList.Text1.Text = pObject
frmMailList.RichTextBox1.Text = pBody
frmMailList.Text2.Text = pCopies

If pFrom = "" Then Exit Sub

Select Case pFormat
    Case "HTML"
        frmMailList.FormatHtml = True
        frmMailList.FormatPDF = False
        frmMailList.FormatNothing = False
        frmMailList.FormatTurbo = False
    Case "TURBO"
        frmMailList.FormatHtml = False
        frmMailList.FormatPDF = False
        frmMailList.FormatNothing = False
        frmMailList.FormatTurbo = True
    Case "NONE"
        frmMailList.FormatHtml = False
        frmMailList.FormatPDF = False
        frmMailList.FormatNothing = False
        frmMailList.FormatTurbo = False
    Case "PDF"
        frmMailList.FormatHtml = False
        frmMailList.FormatPDF = True
        frmMailList.FormatNothing = False
        frmMailList.FormatTurbo = False
End Select


'-> on selectionne les mails
For Each aNode In frmMailList.TreeView1.Nodes
    '-> on inverse la selection
    DoEvents
    If aNode.Tag = "PAGE" Then
        If aNode.Parent.Text = frmMailList.TreeView1.SelectedItem.Text Then
            If aNode.Image = 1 Then aNode.Checked = True
        End If
    End If
Next

'-> on envoi les mails
frmMailList.sendEmail

End Sub

Private Sub sendToSMTP(sFormat As String, sfrom As String, sFile As String, sCopie As String, sTo As String, sObject As String, sBody As String, sBodyFile As String)
'--> cette proc�dure va permettre d'envoyer le message par le serveur de messagerie
Dim aFichier As Fichier
Dim aSpool As Spool

'-> Lancer l'analyse du spool
Call AnalyseFileToPrint(sFile)

'-> Pointer sur l'objet fichier
Set aFichier = Fichiers(UCase$(sFile))
Set frmMail.aSpool = aFichier.Spools(1)

'-> on charge la feuille
Load frmMail

Select Case sFormat
    Case "0"
        frmMail.FormatTurbo = True
    Case "1"
        frmMail.FormatHtml = True
    Case "3"
        frmMail.FormatNothing = True
    Case "4"
        frmMail.FormatPDF = True
End Select

frmMail.Text4.Text = sfrom
frmMail.Text7.Text = sTo
frmMail.Text6.Text = sCopie
frmMail.Text5.Text = sObject
If sBodyFile <> "" Then
    frmMail.RichTextBox2.LoadFile sBodyFile
End If
frmMail.RichTextBox2.Text = frmMail.RichTextBox2.Text & Chr(13) & sBody
'-> on envoi le mail
frmMail.sendBySMTP

End Sub

Private Sub SetPdfWriterFileName(fileName As String)

'---> Cette proc�dure initialise la cl� qui va bien dans le registre
'Acrobat PdfWriter~DIRECT~1|D:\Travailv51\2001-07-06-16-55-22.turbo

'-> Setting se sa valeur
SaveString HKEY_CURRENT_USER, "SoftWare\Adobe\Acrobat PDFWriter", "PDFFileName", fileName

End Sub

'*** PIERROT : modif pour gerer le soft : WIN2PDF
Private Sub SetWin2PDFWriterFileName(fileName As String)

'---> Cette proc�dure initialise la cl� qui va bien dans le registre
'Acrobat PdfWriter~DIRECT~1|D:\Travailv51\2001-07-06-16-55-22.turbo

'-> Setting se sa valeur
SaveString HKEY_CURRENT_USER, "SoftWare\Dane Prairie Systems\Win2PDF", "PDFFileName", fileName

End Sub
'*** PIERROT : modif pour gerer le soft : WIN2PDF

Private Sub SetPdfCreatorFileName(fileName As String)

'---> Cette proc�dure initialise la cl� qui va bien dans le registre
Dim strName As String

If GetIniString("PARAM", "KEEPPDFCREATORSETTING", App.Path & "\Turbograph.ini", False) = "1" Then Exit Sub

'-> Setting se sa valeur si imprimante locale
strName = Dir$(Mid(fileName, 1, Len(fileName) - 4), vbNormal)
SaveString HKEY_CURRENT_USER, "SoftWare\PDFCreator\Program", "AutosaveFilename", strName
strName = Replace(fileName, strName + ".pdf", "", , , vbTextCompare)
SaveString HKEY_CURRENT_USER, "SoftWare\PDFCreator\Program", "AutosaveDirectory", Mid(strName, 1, Len(strName) - 1)
SaveString HKEY_CURRENT_USER, "SoftWare\PDFCreator\Program", "LastsaveDirectory", Mid(strName, 1, Len(strName) - 1)
SaveString HKEY_CURRENT_USER, "SoftWare\PDFCreator\Program", "UseAutosave", "1"
SaveString HKEY_CURRENT_USER, "SoftWare\PDFCreator\Program", "RemoveAllKnownFileExtensions", "0"

'-> Setting se sa valeur si imprimante reseau
strName = Dir$(Mid(fileName, 1, Len(fileName) - 4), vbNormal)
SaveString HKEY_LOCAL_MACHINE, "SOFTWARE\PDFCreator\Program", "AutosaveFilename", strName
strName = Replace(fileName, strName + ".pdf", "", , , vbTextCompare)
SaveString HKEY_LOCAL_MACHINE, "SoftWare\PDFCreator\Program", "AutosaveDirectory", Mid(strName, 1, Len(strName) - 1)
SaveString HKEY_LOCAL_MACHINE, "SoftWare\PDFCreator\Program", "LastsaveDirectory", Mid(strName, 1, Len(strName) - 1)
SaveString HKEY_LOCAL_MACHINE, "SoftWare\PDFCreator\Program", "UseAutosave", "1"
SaveString HKEY_LOCAL_MACHINE, "SoftWare\PDFCreator\Program", "RemoveAllKnownFileExtensions", "0"
SaveString HKEY_LOCAL_MACHINE, "SoftWare\PDFCreator\Program", "AutosaveFormat", "0"

'-> On regarde si le parametrage est dans un fichier ini
If GetRegKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\" & "{0001B4FD-9EA3-4D90-A79E-FD14BA3AB01D}", "UseINI") = "1" Then
    If GetRegKeyValue(HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Common AppData") <> "" Then
        strName = Dir$(Mid(fileName, 1, Len(fileName) - 4), vbNormal)
        SetIniString "Options", "AutosaveFilename", Replace(GetRegKeyValue(HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Common AppData") & "\PDFCreator.ini", "\\", "\"), strName
        strName = Replace(fileName, strName + ".pdf", "", , , vbTextCompare)
        SetIniString "Options", "AutosaveDirectory", Replace(GetRegKeyValue(HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Common AppData") & "\PDFCreator.ini", "\\", "\"), Mid(strName, 1, Len(strName) - 1)
        SetIniString "Options", "LastsaveDirectory", Replace(GetRegKeyValue(HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Common AppData") & "\PDFCreator.ini", "\\", "\"), Mid(strName, 1, Len(strName) - 1)
        SetIniString "Options", "UseAutosave", Replace(GetRegKeyValue(HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Common AppData") & "\PDFCreator.ini", "\\", "\"), "1"
        SetIniString "Options", "RemoveAllKnownFileExtensions", Replace(GetRegKeyValue(HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Common AppData") & "\PDFCreator.ini", "\\", "\"), "0"
        SetIniString "Options", "AutosaveFormat", Replace(GetRegKeyValue(HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Common AppData") & "\PDFCreator.ini", "\\", "\"), "0"
    End If
End If

End Sub

Private Sub SetAdobePdfFileName(fileName As String)

'---> Cette proc�dure initialise la cl� qui va bien dans le registre
Trace "Filename du setadobepdf : " + fileName, 1

'-> Setting de la valeur
SaveString HKEY_CURRENT_USER, "SoftWare\Adobe\Acrobat Distiller\PrinterJobControl", App.Path + "\" + App.EXEName & ".exe", fileName
Trace "Relecture du registre : " + GetRegKeyValue(HKEY_CURRENT_USER, "SoftWare\Adobe\Acrobat Distiller\PrinterJobControl", App.Path + "\" + App.EXEName & ".exe"), 3
End Sub

Private Sub GestError(ByVal ErrorCode As Integer, Optional strError As String, Optional MySpool As Spool)

'---> Cette proc�dure  effectue la gestion centralis�e des erreurs

Dim aLb As Libelle
Dim aFichier As Fichier
Dim aSpool As Spool
Dim Tempo As String
Dim aNode As Node

On Error Resume Next

'-> Pointer sur le libelle Gestion des Erreurs
Set aLb = Libelles("ERRORS")
    
'-> Si on est en mode Visu : Cr�er l'icone du fichier dans le treeview et celle de l'erreur
If mode = 1 Then
    Select Case ErrorCode 'Les erreurs 1 et 5 ne sont pas fatales -> Trait�es dans la proc�dure d'origine
        Case 2 '2-> Impossible d'ouvrir le fichier
            '-> Cr�er un nouveau Fichier
            Set aFichier = New Fichier
            aFichier.fileName = strError
            Fichiers.Add aFichier, UCase$(Trim(strError))
            '-> Cr�er un nouveau Spool
            Set aSpool = aFichier.AddSpool
            aSpool.fileName = strError
            '-> Ajouter une erreur dans le spool
            aSpool.SetErrorMaq Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", strError)
        Case 4, 6
            '-> Erreur lors de la lecture d'une ligne du fichier
            '-> Pointer sur l'objet fichier correspondant au fichier
            Set aFichier = Fichiers(UCase(Trim(strError)))
            
            '-> S'il n'y a pas de spools : il faut en cr�er un
            If aFichier.Spools.Count = 0 Then
                Set aSpool = aFichier.AddSpool
                aSpool.fileName = strError
            End If
            
            '-> Ajouter l'erreur dans le spool
            aSpool.SetErrorMaq Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", strError)
        Case 7, 8, 9, 10
            '-> Gestion des erreurs dans la proc�dure MAQLECT
            '-> Pointer sur le fichier
            Set aFichier = Fichiers(UCase$(Trim(Entry(1, strError, "|"))))
            '-> Pointer sur le spool : pr�ciser en entry 3
            Set aSpool = MySpool
            '-> Faire le setting d'une erreur
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", aSpool.fileName)
            Tempo = Replace(Tempo, "$MAQ$", aSpool.MaquetteASCII)
            Tempo = Replace(Tempo, "$INI$", Entry(3, strError, "|"))
            aSpool.SetErrorMaq Tempo
        Case 11, 12
            '-> Gestion des erreurs dans la proc�dure INITCOM
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", strError)
            Tempo = Replace(Tempo, "$TABLEAU$", strError)
            Tempo = Replace(Tempo, "$MAQ$", MySpool.MaquetteASCII)
            MySpool.SetErrorMaq Tempo
        Case 14, 15, 16
            '-> Gestion des erreurs dans la proc�dure INITCOM
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$TABLEAU$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$MAQ$", MySpool.MaquetteASCII)
            MySpool.SetErrorMaq Tempo
        Case 17
            '-> Gestion des erreurs dans la proc�dure INITFILETEMPORTF
            MySpool.SetErrorMaq aLb.GetCaption(ErrorCode)
        Case 18
            '-> Gestion des erreurs dans la proc�dure INITMAQENTETE
            MySpool.SetErrorMaq aLb.GetCaption(ErrorCode) & " " & strError
        Case 19
            '-> Gestion des erreurs dans la proc�dure CREATEBMPOBJ
            MySpool.SetErrorMaq aLb.GetCaption(ErrorCode)
        Case 20, 21, 22
            '-> Gestion des erreurs dans la proc�dure CREATEBMPOBJ
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
            If NumEntries(strError, "|") > 3 Then
                Tempo = Replace(Tempo, "$HAUTEUR$", Entry(3, strError, "|"))
                Tempo = Replace(Tempo, "$LARGEUR$", Entry(4, strError, "|"))
            Else
                Tempo = Replace(Tempo, "$FICHIER$", Entry(3, strError, "|"))
            End If
            MySpool.SetErrorMaq Tempo
        Case 23
            '-> Gestion des erreurs dans la procedure CREATECADREOBJ
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 24
            '-> Gestion des erreurs dans la proc�dure CreateRTFCadre
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", Entry(3, strError, "|"))
            Tempo = Replace(Tempo, "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 25
            '-> Gestion des erreurs dans la proc�dure CreateRTFSection
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$SECTION$", Entry(1, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 26
            '-> Gestion des erreurs dans la proc�dure CreateSectionObj
            MySpool.SetErrorMaq Replace(aLb.GetCaption(ErrorCode), "$SECTION$", strError)
        Case 27
            '-> Gestion des erreurs dans la proc�dure InitSectionEntete
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$LIG$", Entry(2, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 28, 29
            '-> Gestion des erreurs dans la proc�dure InitBmpEntete et InitCadreEntete
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$LIG$", Entry(3, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 30
            '-> Gestion des erreurs dans la procedure InitTableauEntete
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$LIG$", Entry(2, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 31
            '-> Gestion des erreurs dans la procedure InitBlockEntete
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$LIG$", Entry(3, strError, "|"))
            MySpool.SetErrorMaq Tempo
         Case 32, 33, 34
            '-> Gestion des erreurs dans la proc�dure InitCreateBlock et InitBlock (34)
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
            If ErrorCode = 33 Then Tempo = Replace(Tempo, "$LIG$", Entry(3, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 35, 36, 37, 38, 39, 40, 41
            '-> Gestion des erreurs dans la proc�dure d'affichage d'une page : MODE VISU
            Set aNode = MDIMain.TreeNaviga.Nodes(UCase$(Trim(MySpool.fileName)) & "�" & UCase$(Trim(MySpool.Key)) & "�PAGE|" & MySpool.CurrentPage)
            aNode.Image = "Warning"
            aNode.Tag = "ERROR"
                        
            '-> Faire le setting de l'erreur
            Select Case ErrorCode
                Case 35, 36
                    MySpool.SetErrorPage MySpool.CurrentPage, Replace(aLb.GetCaption(ErrorCode), "$SECTION$", strError)
                Case 37
                    MySpool.SetErrorPage MySpool.CurrentPage, Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", strError)
                Case 38
                    MySpool.SetErrorPage MySpool.CurrentPage, Replace(aLb.GetCaption(ErrorCode), "$OBJECT$", strError)
                Case 39
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
                    MySpool.SetErrorPage MySpool.CurrentPage, Tempo
                Case 40
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
                    MySpool.SetErrorPage MySpool.CurrentPage, Tempo
                Case 41
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
                    Tempo = Replace(Tempo, "$CELLULE$", Entry(3, strError, "|"))
                    MySpool.SetErrorPage MySpool.CurrentPage, Tempo
            End Select
            
            '-> Afficher la page
            MySpool.DisplayInterfaceByPage (MySpool.CurrentPage)
    End Select
Else
    '-> On est en mode impression : initialiser une page : imprimer une erreur
    
End If








End Sub


Public Function AnalyseFileToPrint(ByVal NomFichier As String) As String

'---> Cette proc�dure charge le ficher � analyser, cr�er la collection des spools et les maquettes

Dim hdlFile As Integer ' handle d'ouverture de fichier
Dim Ligne As String 'lecture d'une ligne du fichier ascii
Dim aSpool As Spool 'pointeur vers un objet spool
Dim NomMaquette As String 'nom de la maquette � ouvrir pour ancienne version
Dim aFichier As Fichier 'Pointeur vers un objet fichier
Dim LectureMaquette As Boolean 'Indique que l'on est en train de lire la d�finition de la maquette
Dim LectureSelection As Boolean 'Indique que l'on est en train de lire une page de s�lection
Dim LectureRTF As Boolean 'indique que l'on est en train de lire la d�finition RTF d'une maquette
Dim hdlRtf As Integer 'handle du fichier RTF dans lequel on �crit

Dim isSpool As Boolean 'cette variable indique que l'on est en cours de cr�ation d'un spool
Dim Page As Long 'Compteur de page par spool
Dim iPage As Long
Dim FindMaqChar As Boolean 'indique de plus charger les lignes de la maquettes car on analyse la maquette caract�re
Dim Res As Long
Dim hdlFichier As Long
Dim aOf As OFSTRUCT
Dim ErrorCode As Integer
Dim i As Integer

Dim IsAccDet As Boolean  '-> Indique que l'on est dans le fichier acc�s d�tail
Dim IsCopies As Boolean
Dim IsSelection As Boolean
Dim strSelection As String
Dim nCmd As String
Dim IsConfig As Boolean
Dim isTopPosFichier As Boolean
Dim FileToPrint As String
Dim PageMin As Long
Dim PageMax As Long
Dim ligne2 As String
Dim ligneUTF8 As String
Dim ifrm As Integer

On Error GoTo GestError

ErrorCode = 1

Dim Extension As String
Dim strBuffer As String
Dim lngResult As Long
Dim TempFile As String

Extension = Mid$(NomFichier, i + 1, Len(NomFichier) - i)

If LCase(Mid(NomFichier, InStrRev(NomFichier, "."))) = ".csv" Then
    If IsExcel() Then
        '-> Dans le cas des fichiers utf8 sans bom, si l'application associ�e est excel on duplique et rajoute bom
        NomFichier = convertToUtf8(NomFichier)
        
        '-> On regarde si il y a un programme associ� different de turbo
        strBuffer = Space$(260)
        CopyFile NomFichier, NomFichier & ".xlsx", 0
        lngResult = FindExecutable(NomFichier & ".xlsx", "", strBuffer)
        Kill NomFichier & ".xlsx"
        If strBuffer <> "" And InStr(1, strBuffer, "turbograph", vbTextCompare) = 0 And Len(Trim(strBuffer)) > 6 Then
            '-> on lance l'application associ�e
            strBuffer = Left(strBuffer, Len(strBuffer) - Len(Dir(strBuffer)))
            strBuffer = Left$(strBuffer, InStr(strBuffer, Chr$(0)) - 1)
            Shell Trim(strBuffer) & " " & Chr(34) & NomFichier & Chr(34)
            End
        End If
    Else
        '-> si pas excel on ouvre avec l'application associ�e
        strBuffer = Space$(260)
        lngResult = FindExecutable(NomFichier, "", strBuffer)
        If strBuffer <> "" And InStr(1, strBuffer, "turbograph", vbTextCompare) = 0 And Len(Trim(strBuffer)) > 6 Then
            '-> on lance l'application associ�e
            strBuffer = Left(strBuffer, Len(strBuffer) - Len(Dir(strBuffer)))
            strBuffer = Left$(strBuffer, InStr(strBuffer, Chr$(0)) - 1)
            Shell Trim(strBuffer) & " " & Chr(34) & NomFichier & Chr(34)
            End
        End If
    End If
End If

'-> R�cup�rer la taille du fichier que l'on charge
hdlFichier = OpenFile(NomFichier, aOf, OF_READ)
If hdlFichier <> -1 Then
    Res = GetFileSize(hdlFichier, 0)
    TailleTotale = Res
    Res = CloseHandle(hdlFichier)
End If
PageMax = 50000000
If (TailleTotale > 150000000 Or TailleTotale < 0) And topForce = False Then
        '-> on determine l'intervalle des feuilles a afficher
        '-> on va compter les pages pffffffff
        hdlFile = FreeFile
        frmWait.Show
        Open NomFichier For Input As #hdlFile
        '-> Boucle d'analyse du fichier
        Do While Not EOF(hdlFile)
            '-> Lecture de la ligne
            Line Input #hdlFile, Ligne
            '-> Decrypter la source
            Ligne = DeCrypt(Ligne)
            If Ligne = "[PAGE]" Then iPage = iPage + 1
            If iPage Mod 789 = 0 Then
                frmWait.Label1.Caption = "Lecture en cours... " & iPage
                DoEvents
            End If
        Loop
        Close hdlFile
        Unload frmWait
        frmPage.Text1.Text = iPage
        Call frmPage.initialize
        frmPage.MousePointer = 1
        frmPage.Show vbModal
        PageMin = CLng(strRetour)
        If PageMin <> iPage Then
            '-> on genere les fichiers
            For i = 1 To -1 * Int(-1 * iPage / PageMin)
                '-> Construire le nom du fichier
                FileToPrint = CreateSpool(NomFichier, (i - 1) * PageMin + 1, PageMin * i, Replace(NomFichier, ".", "_" & i & "."))
            Next
            '-> on ouvre le nouveau fichier ainsi cr��
            Shell App.Path & "\TurboGraph.exe " & "~DIRECT~" & "|" & Replace(NomFichier, ".", "_" & 1 & ".") & "|topForce", vbNormalFocus
            '-> on quitte
            End
        Else
            frmWait.Show
            iPage = 0
        End If
End If

'-> verification duu format de fichier

Ligne = OpenFileAPI(NomFichier, 500000)
'-> Decrypter la source
If InStr(1, Ligne, Chr(13)) = 0 And InStr(1, Ligne, Chr(10)) <> 0 Then
    UnixDos NomFichier, NomFichier, unixtodos
End If

'-> Initialisation de la taille lue
TailleLue = 0

ErrorCode = 2
'-> Obtenir un handle de fichier et ouvrir le spool � imprimer
hdlFile = FreeFile
Open NomFichier For Input As #hdlFile

'-> Cr�er un nouvel objet Fichier et l'ajouter dans la collection
Set aFichier = New Fichier
aFichier.fileName = NomFichier
aFichier.filesJoins = uZipInfo2
aFichier.FileNameZip = FileNameZip
Fichiers.Add aFichier, Trim(UCase$(NomFichier))

'-> Boucle d'analyse du fichier
Do While Not EOF(hdlFile)
    'DoEvents
    ErrorCode = 4
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> Decrypter la source
    Ligne = DeCrypt(Ligne, True)
    ligneUTF8 = Ligne
    Ligne = DeCrypt(Ligne)
    '-> Analyse des diff�rents cas que l'on peut rencontrer
    If InStr(1, Ligne, "%%GUI%%") <> 0 Then
        '-> R�cup�rer le nom de la maquette
        NomMaquette = Trim(Mid$(Ligne, 8, Len(Ligne) - 7))
        '-> Cr�er un nouvel objet Spool
        Set aSpool = aFichier.AddSpool
        '-> Indiquer le num�ro du spool
        aSpool.Num_Spool = aFichier.Spools.Count
        aSpool.fileName = aFichier.fileName
        aSpool.MaquetteASCII = NomMaquette
        aSpool.AddPage
        '-> Ancienne version : charger la maquette depuis son adresse
        aFichier.IsOldVersion = True
        MaqLect aSpool, NomMaquette
        '-> Indiquer que l'on est en cr�ation d'un spool
        isSpool = True
        '-> Premi�re page
        Page = 1
        '-> Ajouter une nouvelle page dans le spool
        'aSpool.AddPage
        '-> RAZ des variables de positionnement
        LectureMaquette = False
        LectureSelection = False
    ElseIf Trim(UCase$(Mid(Ligne, 1, 7))) = "[SPOOL]" And Ligne <> "[SPOOL]" Then
        '-> Cr�ation d'un nouveau spool
        Set aSpool = aFichier.AddSpool
        aSpool.SpoolText = Mid(Ligne, 8)
        '-> on se reprend la derniere maquette s'il y a
        If aFichier.NbSpool > 1 Then
            For i = 1 To NumEntries(aFichier.Spools(aFichier.NbSpool - 1).GetMaq, Chr(0))
                'DoEvents
                '-> R�cup�ration de la d�finition de la ligne
                aSpool.SetMaq (Entry(i, aFichier.Spools(aFichier.NbSpool - 1).GetMaq, Chr(0)))
            Next
        End If
        aSpool.AddPage
        '-> Indiquer le nom du fichier maitre
        aSpool.fileName = aFichier.fileName
        '-> Indiquer le num�ro du spool
        aSpool.Num_Spool = aFichier.Spools.Count
        '-> Indiquer que l'on est en cr�ation de spool
        isSpool = True
        '-> Indiquer que l'on n'est plus dans l'acc�s au d�tail
        IsAccDet = False
        '-> On n'est pas encore en train de lire la maquette
        LectureMaquette = False
        LectureSelection = False
        FindMaqChar = False
        '-> Init du compteur de page
        Page = 1
        '-> Init de la premi�re page
        'aSpool.AddPage
        If IsSelection Then
            aSpool.SetPage 0, strSelection
            aSpool.IsSelectionPage = True
            IsSelection = False
            strGarde = strSelection
        End If
    Else 'si on n'a pas trouv� %%GUI%%
        Select Case Trim(UCase$(Ligne))
            Case "[SPOOL]"
                '-> Cr�ation d'un nouveau spool
                Set aSpool = aFichier.AddSpool
                aSpool.AddPage
                '-> Indiquer le nom du fichier maitre
                aSpool.fileName = aFichier.fileName
                '-> Indiquer le num�ro du spool
                aSpool.Num_Spool = aFichier.Spools.Count
                '-> Indiquer que l'on est en cr�ation de spool
                isSpool = True
                '-> Indiquer que l'on n'est plus dans l'acc�s au d�tail
                IsAccDet = False
                '-> On n'est pas encore en train de lire la maquette
                LectureMaquette = False
                LectureSelection = False
                FindMaqChar = False
                '-> Init du compteur de page
                Page = 1
                '-> Init de la premi�re page
                'aSpool.AddPage
                If IsSelection Then
                    aSpool.SetPage 0, strSelection
                    aSpool.IsSelectionPage = True
                    IsSelection = False
                    strGarde = strSelection
                End If
            Case "[/SPOOL]"
                '-> Indiquer que 'on n'est plus dans un spool
                isSpool = False
            Case "[CONFIG]"
                IsConfig = True
            Case "[/CONFIG]"
                IsConfig = False
            Case "[MAQ]"
                LectureMaquette = True
                LectureSelection = False
            Case "[/MAQ]"
                LectureMaquette = False
            Case "[MAQCHAR]"
                FindMaqChar = True
            Case "[GARDEOPEN]"
                LectureSelection = True
                LectureMaquette = False
                '-> Indiquer  dans le spool en cours qu'il ya une page de s�lection
                On Error GoTo suite
                isGarde = True
                IsSelection = True
                aSpool.IsSelectionPage = True
suite:
                On Error GoTo GestError
            Case "<[SELECTIONS]>"
                IsSelection = True
            Case "[GARDECLOSE]"
                LectureSelection = False
                IsSelection = False
                strGarde = strSelection
            Case "\Description�Fichier Texte"
                aSpool.IsTexte = True
            Case "[PAGE]"
                '-> Incr�menter le compteur de page
                Page = Page + 1
                aSpool.AddPage
                If Page > 32000 Then
                    isTopPosFichier = True
                End If
                iPage = iPage + 1
                '-> Ajouter une nouvelle page dans le spool
                '-> au ndela d'un certain nombre de pages on met en place le systeme de buffer
                If iPage Mod 789 = 0 Then
                    frmWait.Label1.Caption = "Chargement en cours... " & iPage
                    DoEvents
                End If
            Case "[DETAIL]"
                '-> On passe en mode lecture sur le spool en cours
                IsAccDet = True
            Case "[/DETAIL]"
                '-> Fin du mode lecture de l'acc�s au d�tail
                IsAccDet = False
            Case "[NBCOPIES]"
                IsCopies = True
            Case "[/NBCOPIES]"
                IsCopies = False
                '-> on ouvre le nouveau fichier ainsi cr��
                If mode = 1 And Trim(UCase(nPrinter)) <> "SCREEN" And Trim(UCase(nPrinter)) <> "ECRAN" And Trim(UCase(nPrinter)) <> "" And Not forceScreen Then
                    Dim TempPrinter
                    Dim FindPrinter As Boolean
                    FindPrinter = False
                    For Each TempPrinter In Printers
                        If UCase$(TempPrinter.DeviceName) = UCase$(nPrinter) Then
                            FindPrinter = True
                            Exit For
                        End If
                    Next 'Pour toutes les imprimantes
                    If UCase$(nPrinter) = "DEFAULT" Then
                        FindPrinter = True
                        nPrinter = "SCREEN"
                        nPrinter = Printer.DeviceName
                    End If
                    If FindPrinter Then
                        If nRectoVerso Then
                            Shell App.Path & "\TurboGraph.exe " & nPrinter & "$DIRECT" & "|" & NomFichier & "|RV", vbNormalFocus
                        Else
                            Shell App.Path & "\TurboGraph.exe " & nPrinter & "$DIRECT" & "|" & NomFichier, vbNormalFocus
                        End If
                        '-> on quitte
                        End
                    Else
                        If Trim(UCase(nPrinter)) = "TURBOPDF" Then
                            Shell App.Path & "\TurboPdf.exe " & "�fileToConvert=" & NomFichier & "�SORTIE=ECRAN"
                            End
                        End If
                    End If
                End If
            Case Else
                '-> Ne traiter la ligne que si on est dans un spool
                If isSpool And Not isTopPosFichier Then
                    '-> Selon la nature du traitement
                    If IsAccDet Then
                        '-> Ne rien faire si on est en cours de lecture acces d�tail
                        If Trim(Ligne) <> "" Then aSpool.AddLigneDetail Ligne
                    ElseIf LectureMaquette Then
                        '-> Ajouter une ligne dans la d�finition de la maquette
                        If Not FindMaqChar Then
                            
                            Dim ii As Integer
                            Dim strTrav As String
                            Dim strTemp
                            strTemp = Split(Ligne, "<frm:")
                            strTrav = ""
                            If UBound(strTemp) > 0 Then
                                If InStr(Mid(strTemp(1), 1, 3), ":") = 0 Then
                                    For ii = 0 To UBound(strTemp) - 1
                                        strTrav = strTrav & strTemp(ii) & "<frm:" & ifrm & ":"
                                        ifrm = ifrm + 1
                                    Next
                                    strTrav = strTrav & strTemp(ii)
                                    Ligne = strTrav
                                End If
                            End If
                            aSpool.SetMaq Ligne
                        End If
                    ElseIf LectureSelection Then
                        '-> Ajouter une ligne dans la page de s�lection
                        aSpool.SetPage 0, Ligne
                    Else
                        '-> ajouter une ligne de datas dans la page en cours
                        If Not IsCopies Then aSpool.SetPage Page, Ligne
                    End If 'Selon la nature de la ligne
                End If 'si on est dans un spool
                If IsSelection Then
                    If strSelection = "" Then
                        strSelection = Ligne
                    Else
                        strSelection = strSelection + Chr(0) + Ligne
                    End If
                End If
                If IsCopies Then
                    If InStr(1, Ligne, "\NbCopies�") <> 0 Or InStr(1, Ligne, "NbCopies�") <> 0 Then
                        nCopies = val(Entry(2, Ligne, "�"))
                        If nCopies = 0 Then nCopies = 1
                    End If
                    If InStr(1, Ligne, "\Printer�") <> 0 Or InStr(1, Ligne, "Printer�") <> 0 Then
                        nPrinter = Entry(2, Ligne, "�")
                    End If
                    If InStr(1, Ligne, "\RectoVerso�") <> 0 Or InStr(1, Ligne, "RectoVerso�") <> 0 Then
                        nRectoVerso = True
                    End If
                End If
                If IsConfig Then
                    On Error Resume Next
                    '-> on va pouvoir executer des operations de maintenance de turbograph pour les versions internet
                    If VersionTurbo = 1 Then
                        If InStr(1, Ligne, "\KillIniTurbo�", vbTextCompare) <> 0 Then
                            Kill App.Path & "\Turbograph.ini"
                        End If
                        'Mise a jour du fichier de configuration de turbo
                        If InStr(1, Ligne, "\SetIniTurbo�", vbTextCompare) <> 0 Then
                            'SetIniString "PARAM", "FULLSCREEN", App.Path & "\Turbograph.ini", Me.Check3.Value
                            SetIniString Entry(2, Ligne, "�"), Replace(Entry(3, Ligne, "�"), "�", "�"), App.Path & "\Turbograph.ini", Entry(4, Ligne, "�")
                        End If
                        If InStr(1, Ligne, "\KillIniVue�", vbTextCompare) <> 0 Then
                            Kill App.Path & "\TurboVue.ini"
                        End If
                        'Mise a jour du fichier des vues
                        If InStr(1, Ligne, "\SetIniVue�", vbTextCompare) <> 0 Then
                            'SetIniString Trim(UCase(sName)), "R_CLASSE", App.Path & "\TurboVue.ini", Me.Check1.Value & "�" & Me.Label4.Caption
                            SetIniString Entry(2, Ligne, "�"), Entry(3, Ligne, "�"), App.Path & "\TurboVue.ini", Entry(4, Ligne, "�")
                        End If
                        'Mise a jour du fichier des eclatements
                        If InStr(1, Ligne, "\SetIniDispatch�", vbTextCompare) <> 0 Then
                            'SetIniString Trim(UCase(sName)), "R_CLASSE", App.Path & "\TurboVue.ini", Me.Check1.Value & "�" & Me.Label4.Caption
                            SetIniString Entry(2, Ligne, "�"), Entry(3, Ligne, "�"), App.Path & "\TurboDispatch.ini", Entry(4, Ligne, "�")
                        End If
                        'Mise � jour des images par telechargement (ou autres fichiers)
                        If InStr(1, Ligne, "\SetImage�", vbTextCompare) <> 0 Then
                            TelechargeFile Entry(2, Ligne, "�"), App.Path & Entry(3, Ligne, "�")
                        End If
                        'Mise � jour de fichiers
                        If InStr(1, Ligne, "\SetFichier�", vbTextCompare) <> 0 Then
                            FileCopy Entry(2, Ligne, "�"), App.Path & Entry(3, Ligne, "�")
                        End If
                        'Renommer un fichier
                        If InStr(1, Ligne, "\SetRenameFichier�", vbTextCompare) <> 0 Then
                            Name App.Path & Entry(2, Ligne, "�") As App.Path & Entry(3, Ligne, "�")
                        End If
                        'Supprimer un fichier
                        If InStr(1, Ligne, "\SetKillFichier�", vbTextCompare) <> 0 Then
                            Kill App.Path & Entry(2, Ligne, "�")
                        End If
                    End If
                End If
        End Select 'Selon la ligne que l'on est en train de lire
    End If 'si on a trouv� %%GUI%%
    
    '-> Gestion de la temprisation
    If mode = 1 Then
        TailleLue = TailleLue + Len(Ligne) + 2
        If Rnd < 0.005 Then DrawWait
        If aSpool Is Nothing And TailleLue > 500000 Then GoTo suite2
    End If
    If Rnd < 0.02 Then DoEvents
Loop 'Boucle d'analyse du fichier
suite2:
ErrorCode = 5
'-> Fermer le fichier ascii
Close #hdlFile

'-> Si on n' a pas trouv� de spool, en cr�er un pour l'erreur
If aFichier.Spools.Count = 0 Then
    '-> on a pas trouv� de spools on le converti en un spool fichier texte
    Dim repfic As String
    repfic = createFileText(NomFichier)
    If repfic <> "" Then
        repfic = AnalyseFileToPrint(repfic)
        AnalyseFileToPrint = repfic
        Fichiers.Remove (Trim(UCase$(NomFichier)))
        Fichiers.Item(Trim(UCase(repfic))).FileNameZip = Fichiers.Item(Trim(UCase(repfic))).fileName
        Fichiers.Item(Trim(UCase(repfic))).Spools(1).TexteFileName = NomFichier
        ErrorCode = 0
        Exit Function
    End If
    '-> D�router sur la gestion d'erreur
    ErrorCode = 6
    GoTo GestError
End If

'-> Supprimer la derni�re page si elle est � blanc
For Each aSpool In aFichier.Spools
    'DoEvents
    For i = aSpool.nbPage To 1 Step -1
        If Trim(aSpool.GetPage(i)) = "" Then
            aSpool.nbPage = aSpool.nbPage - 1
        Else
            '-> Ne supprimer qe les pages de fin qui sont blanches
            Exit For
        End If
    Next
Next

 '-> supprimer le spool si il est vide
i = 1
Do While i <= aFichier.Spools.Count
    If aFichier.Spools(i).nbPage = 0 And aFichier.Spools(i).IsSelectionPage = False Then
        aFichier.NbSpool = aFichier.NbSpool - 1
        aFichier.Spools.Remove i
    Else
        '-> dans le cas ou on a un spool avec que la garde on le remonte en haut
        If aFichier.Spools(i).nbPage = 0 And aFichier.Spools(i).IsSelectionPage = True Then
            aFichier.Spools(i).Key = "SPOOL|0"
            aFichier.Spools(i).Num_Spool = 1
            aFichier.Spools.Add aFichier.Spools(i), "SPOOL|0", 1
            Dim strtempo As String
            strtempo = aFichier.Spools(1).GetPage(0)
            aFichier.Spools.Remove (i + 1)
            For Each aSpool In aFichier.Spools
                If aSpool.Key <> "SPOOL|0" Then
                    aSpool.SetPage 0, strtempo
                End If
            Next
        End If
        i = i + 1
    End If
Loop

'-> Gestion de la temporisation
If mode = 1 Then
    MDIMain.StatusBar1.Refresh
    '-> R�initialiser les variables de lecture
    TailleLue = 0
    TailleTotale = aFichier.Spools.Count
End If

'-> G�n�rer les maquettes tous les spools
For Each aSpool In aFichier.Spools
    TailleLue = TailleLue + 1
    If mode = 1 Then
        If Rnd < 0.01 Then DrawWait
    End If
    '-> test pour accelerer apres on lit en direct
    If TailleLue < 10 Then InitCom aSpool
Next

'-> Vider la toolbar
If mode = 1 Then
    MDIMain.StatusBar1.Refresh
    MDIMain.picSplit.Visible = IsNaviga
    MDIMain.picNaviga.Visible = IsNaviga
End If

'-> on regarde si il y a des notes a charger
Trace ("av notesload")
If aFichier.FileNameZip <> NomFichier Then
    Call notesLoad(aFichier.FileNameZip)
Else
    Call notesLoad(NomFichier)
End If
Trace ("ap notesload")
Unload frmWait
AnalyseFileToPrint = NomFichier
Exit Function

GestError:
        
'-> Fermer le fichier ascii
Close #hdlFile
        
    '-> Traitement de l'erreur
    Select Case ErrorCode
        Case 1
            '-> Pb lors de la r�cup�ration de la taille du fichier : ne rien faire
            Resume Next
        Case 2, 3, 4, 6
            '-> Erreur fatale
            Call GestError(ErrorCode, NomFichier)
        Case 5
            '-> Erreur lors de la fermture du fichier ASCII : Fermer le fichier
            Reset
            '-> Rendre la main
    End Select
                                

End Function

Public Function createFileText(ByVal NomFichier As String) As String
'---> Cette fonction cr�er un fichier tempo et retourne le nom du fichier � linker

Dim TempFile As String
Dim hdlFile As Integer
Dim hdlFile2 As Integer
Dim DefMaq As String
Dim i As Integer
Dim iCount As Integer
Dim Ligne As String
Dim Orientation As String
Dim police As String
Dim taille As String
Dim maxligne As Integer
Dim hauteurligne As String
Dim lfeuille As String
Dim hfeuille As String
Dim MargeTop As String
Dim MargeLeft As String
Dim skipFirst As Integer
Dim skipLast As Integer
Dim skipEndPage As Boolean
Dim F As Integer
Dim e As Integer
Dim isGarde As Boolean

On Error GoTo GestError

'-> Obtenir un handle de fichier et ouvrir le spool � imprimer
Orientation = "1"
police = "Arial"
taille = "9"
maxligne = 62
hauteurligne = "0.4"
lfeuille = ""
hfeuille = ""
MargeTop = "1"
MargeLeft = "1.2"
skipFirst = 0
skipLast = 0
skipEndPage = False
isGarde = False

If GetIniString("PARAM", "FORMATFILETEXT", App.Path & "\Turbograph.ini", False) <> "" Then
    Ligne = GetIniString("PARAM", "FORMATFILETEXT", App.Path & "\Turbograph.ini", False)
    Orientation = Entry(2, Ligne, "@")
    police = Entry(3, Ligne, "@")
    taille = Entry(4, Ligne, "@")
    maxligne = val(Entry(5, Ligne, "@"))
    hauteurligne = Entry(6, Ligne, "@")
    MargeTop = Entry(7, Ligne, "@")
    MargeLeft = Entry(8, Ligne, "@")
    skipFirst = val(Entry(9, Ligne, "@"))
    skipLast = val(Entry(10, Ligne, "@"))
    skipEndPage = val(Entry(11, Ligne, "@"))
End If

hdlFile2 = FreeFile
iCount = 0
Open NomFichier For Input As #hdlFile2
'-> Lecture de la premiere ligne
If Not EOF(hdlFile2) Then
    Line Input #hdlFile2, Ligne
End If
Close #hdlFile2
If Mid(Ligne, 1, 6) = "TURBO@" Then
    Orientation = Entry(2, Ligne, "@")
    police = Entry(3, Ligne, "@")
    taille = Entry(4, Ligne, "@")
    maxligne = val(Entry(5, Ligne, "@"))
    hauteurligne = Entry(6, Ligne, "@")
    MargeTop = Entry(7, Ligne, "@")
    MargeLeft = Entry(8, Ligne, "@")
    skipFirst = val(Entry(9, Ligne, "@"))
    skipLast = val(Entry(10, Ligne, "@"))
    skipEndPage = val(Entry(11, Ligne, "@"))
End If

If Orientation = "1" Then
    lfeuille = "21"
    hfeuille = "29.7"
Else
    hfeuille = "29.7"
    lfeuille = "21"
End If

'-> Obtenir un nom de fichier tempotaire
TempFile = Year(Now) & "-" & FORMAT(Month(Now), "00") & "-" & FORMAT(Day(Now), "00") & "-" & FORMAT(Hour(Now), "00") & "-" & FORMAT(Minute(Now), "00") & "-" & FORMAT(Second(Now), "00") & Right(CStr(GetTickCount), 2) & ".turbo"
TempFile = GetTempFileNameVB("WWW", True) & TempFile

'-> Obtenir un handle de fichier
hdlFile = FreeFile

'-> Ouverture du fichier ascii et �criture des pages
Open TempFile For Output As #hdlFile

'-> La maquette
DefMaq = "[SPOOL]" & Chr(0) & "[MAQ]" & Chr(0) & "\DefEntete�Begin" & Chr(0) & "\Version�2" & Chr(0) & "\FieldListe�" & Chr(0) & "\Description�" + "Fichier Texte" & Chr(0) & "\Largeur�" & lfeuille & Chr(0) & "\Hauteur�" & hfeuille & Chr(0) & "\Orientation�" & Orientation & Chr(0) & "\MargeTop�" & MargeTop & Chr(0) & "\MargeLeft�" & MargeLeft & Chr(0) & "\DefEntete�End" & Chr(0) & "[TB-Tableau]" & Chr(0) & "\LargeurTb�0" & Chr(0) & "\OrientationTB�0" & Chr(0) & "\Begin�Entete" & Chr(0) & "\Largeur�30" & Chr(0) & "\Hauteur�1" & Chr(0) & "\AlignTop�1" & Chr(0) & "\Top�0" & Chr(0) & "\AlignLeft�0" & Chr(0) & "\Left�0" & Chr(0) & "\RgChar�" & Chr(0) & "\Ligne�1" & Chr(0) & "\Colonne�1" & Chr(0) & "\Col�" & hauteurligne & "�1;^0001;40;FAUX,FAUX,FAUX,FAUX;" & police & ";" & taille & ";FAUX;FAUX;FAUX;0;16777215;FAUX;0@0@1;0@1" & Chr(0) & "\Champs�0001;1" & Chr(0) & "\End�Entete" & Chr(0) & "\Tableau�End" & Chr(0) & "[/MAQ]"

'-> Impression de la maquette
For i = 1 To NumEntries(DefMaq, Chr(0))
    If IsCryptedFile Then
        Print #hdlFile, Crypt(Entry(i, DefMaq, Chr(0)))
    Else
        Print #hdlFile, Entry(i, DefMaq, Chr(0))
    End If
Next 'Pour toutes les lignes de la maquette

'-> Obtenir un handle de fichier et ouvrir le spool � imprimer
hdlFile2 = FreeFile
iCount = 0
Open NomFichier For Input As #hdlFile2
e = skipFirst
F = skipLast
'-> Boucle d'analyse du fichier
Do While Not EOF(hdlFile2)
    If iCount = maxligne Then
        If skipLast <> 0 Then
            On Error Resume Next
            For i = 1 To F
                Line Input #hdlFile2, Ligne
            Next
            On Error GoTo GestError
        End If
    End If
    If iCount > maxligne Then
        iCount = 0
        If IsCryptedFile Then
            Print #hdlFile, Crypt("[PAGE]")
        Else
            Print #hdlFile, "[PAGE]"
        End If
    End If
    '-> Lecture de la ligne
    Line Input #hdlFile2, Ligne
    If Trim(UCase(Ligne)) = "[GARDEOPEN]" Then isGarde = True
    If Not isGarde Then
        If e <> 0 And iCount = 0 Then
            On Error Resume Next
            For i = 1 To e
                Line Input #hdlFile2, Ligne
            Next
            If Not skipEndPage Then
                e = 0
            End If
            On Error GoTo GestError
        End If
        If Mid(Ligne, 1, 6) <> "TURBO@" Then
            '-> Analyse des diff�rents cas que l'on peut rencontrer
            If IsCryptedFile Then
                Print #hdlFile, Crypt("[TB-Tableau(BLK-Entete)][\1]{^0001" & Ligne & " }")
            Else
                Print #hdlFile, "[TB-Tableau(BLK-Entete)][\1]{^0001" & Ligne & " }"
            End If
            iCount = iCount + 1
        End If
    Else
        Print #hdlFile, Ligne
    End If
    If Trim(UCase(Ligne)) = "[GARDECLOSE]" Then isGarde = False
Loop

'-> Tag de fin de spool
If IsCryptedFile Then
    Print #hdlFile, Crypt("[/SPOOL]")
Else
    Print #hdlFile, "[/SPOOL]"
End If

'-> Fermer le fichier ouvert
Close #hdlFile
Close #hdlFile2

'-> Renvoyer le nom du fichier
createFileText = TempFile
Exit Function

GestError:
createFileText = ""
End Function

Public Sub AnalyseFileToPreview(ByVal NomFichier As String)

'---> Cette proc�dure charge le ficher � analyser, cr�er la collection des spools et les maquettes

Dim hdlFile As Integer ' handle d'ouverture de fichier
Dim Ligne As String 'lecture d'une ligne du fichier ascii
Dim aSpool As Spool 'pointeur vers un objet spool
Dim NomMaquette As String 'nom de la maquette � ouvrir pour ancienne version
Dim aFichier As Fichier 'Pointeur vers un objet fichier
Dim LectureMaquette As Boolean 'Indique que l'on est en train de lire la d�finition de la maquette
Dim LectureSelection As Boolean 'Indique que l'on est en train de lire une page de s�lection
Dim LectureRTF As Boolean 'indique que l'on est en train de lire la d�finition RTF d'une maquette
Dim hdlRtf As Integer 'handle du fichier RTF dans lequel on �crit

Dim isSpool As Boolean 'cette variable indique que l'on est en cours de cr�ation d'un spool
Dim Page As Integer 'Compteur de page par spool
Dim FindMaqChar As Boolean 'indique de plus charger les lignes de la maquettes car on analyse la maquette caract�re
Dim Res As Long
Dim hdlFichier As Long
Dim aOf As OFSTRUCT
Dim ErrorCode As Integer
Dim i As Integer

Dim IsAccDet As Boolean  '-> Indique que l'on est dans le fichier acc�s d�tail

On Error GoTo GestError

ErrorCode = 1
'-> R�cup�rer la taille du fichier que l'on charge
hdlFichier = OpenFile(NomFichier, aOf, OF_READ)
If hdlFichier <> -1 Then
    Res = GetFileSize(hdlFichier, 0)
    TailleTotale = Res
    Res = CloseHandle(hdlFichier)
End If

'-> Initialisation de la taille lue
TailleLue = 0

ErrorCode = 2
'-> Obtenir un handle de fichier et ouvrir le spool � imprimer
hdlFile = FreeFile
Open NomFichier For Input As #hdlFile

'-> Cr�er un nouvel objet Fichier et l'ajouter dans la collection
Set aFichier = New Fichier
aFichier.fileName = NomFichier
aFichier.filesJoins = uZipInfo2
aFichier.FileNameZip = FileNameZip
Fichiers.Add aFichier, Trim(UCase$(NomFichier))

'-> Boucle d'analyse du fichier
Do While Not EOF(hdlFile)
    ErrorCode = 4
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> Decrypter la source
    Ligne = DeCrypt(Ligne)
    '-> Analyse des diff�rents cas que l'on peut rencontrer
    If InStr(1, Ligne, "%%GUI%%") <> 0 Then
        '-> R�cup�rer le nom de la maquette
        NomMaquette = Trim(Mid$(Ligne, 8, Len(Ligne) - 7))
        '-> Cr�er un nouvel objet Spool
        Set aSpool = aFichier.AddSpool
        '-> Indiquer le num�ro du spool
        aSpool.Num_Spool = aFichier.Spools.Count
        aSpool.fileName = aFichier.fileName
        aSpool.MaquetteASCII = NomMaquette
        aSpool.AddPage
        '-> Ancienne version : charger la maquette depuis son adresse
        aFichier.IsOldVersion = True
        MaqLect aSpool, NomMaquette
        '-> Indiquer que l'on est en cr�ation d'un spool
        isSpool = True
        '-> Premi�re page
        Page = 1
        '-> Ajouter une nouvelle page dans le spool
        'aSpool.AddPage
        '-> RAZ des variables de positionnement
        LectureMaquette = False
        LectureSelection = False
    Else 'si on n'a pas trouv� %%GUI%%
        Select Case Trim(UCase$(Ligne))
            Case "[SPOOL]"
                '-> Cr�ation d'un nouveau spool
                Set aSpool = aFichier.AddSpool
                aSpool.AddPage
                '-> Indiquer le nom du fichier maitre
                aSpool.fileName = aFichier.fileName
                '-> Indiquer le num�ro du spool
                aSpool.Num_Spool = aFichier.Spools.Count
                '-> Indiquer que l'on est en cr�ation de spool
                isSpool = True
                '-> Indiquer que l'on n'est plus dans l'acc�s au d�tail
                IsAccDet = False
                '-> On n'est pas encore en train de lire la maquette
                LectureMaquette = False
                LectureSelection = False
                FindMaqChar = False
                '-> Init du compteur de page
                Page = 1
                '-> Init de la premi�re page
                'aSpool.AddPage
            Case "[/SPOOL]"
                '-> Indiquer que 'on n'est plus dans un spool
                isSpool = False
            Case "[MAQ]"
                LectureMaquette = True
                LectureSelection = False
            Case "[/MAQ]"
                LectureMaquette = False
            Case "[MAQCHAR]"
                FindMaqChar = True
            Case "[GARDEOPEN]"
                LectureSelection = True
                LectureMaquette = False
                '-> Indiquer  dans le spool en cours qu'il ya une page de s�lection
                aSpool.IsSelectionPage = True
            Case "[GARDECLOSE]"
                LectureSelection = False
            Case "[PAGE]"
                '-> Incr�menter le compteur de page
                Page = Page + 1
                '-> Ajouter une nouvelle page dans le spool
                aSpool.AddPage
            Case "[DETAIL]"
                '-> On passe en mode lecture sur le spool en cours
                IsAccDet = True
            Case "[/DETAIL]"
                '-> Fin du mode lecture de l'acc�s au d�tail
                IsAccDet = False
            Case Else
                '-> Ne traiter la ligne que si on est dans un spool
                If isSpool Then
                    '-> Selon la nature du traitement
                    If IsAccDet Then
                        '-> Ne rien faire si on est en cours de lecture acces d�tail
                        If Trim(Ligne) <> "" Then aSpool.AddLigneDetail Ligne
                    ElseIf LectureMaquette Then
                        '-> Ajouter une ligne dans la d�finition de la maquette
                        If Not FindMaqChar Then aSpool.SetMaq Ligne
                    ElseIf LectureSelection Then
                        '-> Ajouter une ligne dans la page de s�lection
                        aSpool.SetPage 0, Ligne
                    Else
                        '-> ajouter une ligne de datas dans la page en cours
                        aSpool.SetPage Page, Ligne
                    End If 'Selon la nature de la ligne
                End If 'si on est dans un spool
        End Select 'Selon la ligne que l'on est en train de lire
    End If 'si on a trouv� %%GUI%%
    
Loop 'Boucle d'analyse du fichier

ErrorCode = 5
'-> Fermer le fichier ascii
Close #hdlFile

'-> Supprimer la derni�re page si elle est � blanc
For Each aSpool In aFichier.Spools
    For i = aSpool.nbPage To 1 Step -1
        If Trim(aSpool.GetPage(i)) = "" Then
            aSpool.nbPage = aSpool.nbPage - 1
        Else
            '-> Ne supprimer qe les pages de fin qui sont blanches
            Exit For
        End If
    Next
Next

'-> supprimer le spool si il est vide
i = 1
Do While i <= aFichier.Spools.Count
    If aFichier.Spools(i).nbPage = 0 Then
        aFichier.NbSpool = aFichier.NbSpool - 1
        aFichier.Spools.Remove i
    Else
        i = i + 1
    End If
Loop

'-> G�n�rer les maquettes tous les spools
For Each aSpool In aFichier.Spools
    If mode = 1 Then
        TailleLue = TailleLue + 1
        DrawWait
    End If
    InitCom aSpool
Next

Exit Sub

GestError:
        
'-> Fermer le fichier ascii
Close #hdlFile
        
    '-> Traitement de l'erreur
    Select Case ErrorCode
        Case 1
            '-> Pb lors de la r�cup�ration de la taille du fichier : ne rien faire
            Resume Next
        Case 2, 3, 4, 6
            '-> Erreur fatale
            Call GestError(ErrorCode, NomFichier)
        Case 5
            '-> Erreur lors de la fermture du fichier ASCII : Fermer le fichier
            Reset
            '-> Rendre la main
    End Select
                                

End Sub

Private Function OpenRTFSpool(ByVal RtfName As String) As Integer

'---> Ouverture d'un spool pour stocker le contenu RTF de la section/cadre

Dim RtfFile As String
Dim hdlRtf As Integer

'-> R�cup�ration du r�pertoire temporaire de windows
RtfFile = GetTempFileNameVB("", True)
hdlRtf = FreeFile
Open RtfFile & "\" & RtfName For Output As #hdlRtf

'-> Renvoyer son handle
OpenRTFSpool = hdlRtf

End Function

Private Sub MaqLect(ByRef aSpool As Spool, ByVal aMaq As String)

'---> Cette proc�dure charge le fichier ASCII d'une maquette dans un objet spool : compatibilite ancienne version

Dim hdlFile As Integer
Dim TmpFile As String
Dim Ligne As String
Dim ErrorCode As Integer
Dim TmpError As String

On Error GoTo GestError

'-> Setting du nom de la maquette dans l'objet spool
aSpool.MaquetteASCII = aMaq

'-> V�rifier que le fichier Maquette est bien pr�sent
ErrorCode = 7
TmpError = aSpool.fileName & "|" & aMaq
If Dir$(aMaq, vbNormal) = "" Then
    '-> Setting d'une erreur
    Call GestError(7, TmpError, aSpool)
    '-> Sortir de la proc�dure
    Exit Sub
End If

'-> Cr�er un fichier Tempo et faire une copie de la maquette
ErrorCode = 8
TmpFile = GetTempFileNameVB("MAQ")
CopyFile aMaq, TmpFile, False
    

ErrorCode = 9
'-> Ouvrir le fichier Tempo
hdlFile = FreeFile
Open TmpFile For Input As #hdlFile

'-> Transf�rer la maquette dans l'objet Spool
Do While Not EOF(hdlFile)
    'DoEvents
    '-> Lecture de la ligne
    ErrorCode = 10
    Line Input #hdlFile, Ligne
    If Trim(Ligne) = "" Then GoTo NextLigne
    '-> Decrypter la maquette
    Ligne = DeCrypt(Ligne)
    '-> Quitter si on arrive � la d�finition de la maquette caract�re
    If UCase$(Trim(Ligne)) = "[MAQCHAR]" Then Exit Do
    '-> Transfert de la ligne
    aSpool.SetMaq (Ligne)
NextLigne:
Loop

ErrorCode = 11
'-> Fermer le fichier ASCII
Close #hdlFile

ErrorCode = 12
'-> Supprimer le fichier Tempo
Kill TmpFile

'-> Indiquer que la maquette est OK
aSpool.Ok = True

Exit Sub

GestError:

    '-> Traiter l'erreur selon le cas
    Select Case ErrorCode
        Case 7
            Call GestError(ErrorCode, TmpError, aSpool)
                                    
        Case 11, 12  '-> Erreurs r�cup�rables
            '-> Fermer tous les fichiers ouverts
            Reset
            '-> Continuer
            Resume Next
        Case Else
            '-> Appeler la gestion des erreurs
            Call GestError(ErrorCode, aSpool.fileName & "|" & aMaq & "|" & TmpFile, aSpool)
    End Select

End Sub

Public Sub InitCom(aSpool As Spool)

'---> Cette proc�dure initialise pour un fichier donn� la maquette associ�e

Dim Ligne As String '-> Pour lecture d'une ligne de maquette
Dim i As Long '-> Compteur de ligne
Dim SwitchLect As Integer
'-> Valeur de SwitchLect :
    '0  -> Nothing
    '1  -> Entete Maquette
    '2  -> Section PROGICIEL
    '3  -> Section de Texte : Propri�t�s
    '4  -> D�finition BMP
    '5  -> Definition d'un Cadre
    '6  -> Definition RTF d'un cadre
    '7  -> D�finition RTF d'une section
    '8  -> D�fintion d'un tableau
    '9  -> D�fintion d'un block de tableau
    '10 -> D�fintion d'une cellule de tableau
    '11 -> Ecran de s�lection

Dim LibError As String '-> Libelle si erreur
Dim TempoStr As String '-> Variable de stockage tempo

'-> D�finition du model COM
Dim aSection As Section
Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim aTb As Tableau
Dim aBlock As Block
'Dim aCell As Cellule
Dim ErrorCode As Integer
Dim TmpError As String

On Error GoTo GestError

'-> Type de ligne par d�faut
SwitchLect = 0

'-> Initialisation de la maquette
Set aSpool.maquette = New maquette

'-> Analyse de la maquette propre au Spool
Dim iNum As Long
Dim MyArray() As String
iNum = NumEntries(aSpool.GetMaq, Chr(0))
MyArray = Split(aSpool.GetMaq, Chr(0), -1)

For i = 0 To iNum - 1
    If i Mod 100 = 0 Then
        DoEvents
    End If
    '-> R�cup�ration de la d�finition de la ligne
    Ligne = MyArray(i) 'Entry(i, aSpool.GetMaq, Chr(0))
    '-> Tester le premier caract�re de la ligne
    If Left$(Ligne, 1) = "[" Then
        '-> Analyse du type d'objet
        If UCase$(Mid$(Ligne, 2, 2)) = "NV" Then
            '-> Si on attaque le descriptif OBJECT ou la definition TRF
            If UCase$(Entry(3, Ligne, "-")) = "STD]" Then
                '-> Cr�ation du nouvel objet Section de texte
                Set aSection = New Section
                aSection.Nom = Entry(2, Ligne, "-")
                '-> Ajout dans la collection
                ErrorCode = 11
                TmpError = UCase$(Trim(aSection.Nom))
                aSpool.maquette.Sections.Add aSection, UCase$(Trim(aSection.Nom))
                '-> Ligne de d�finition d'une section de texte
                SwitchLect = 3
            ElseIf UCase$(Entry(3, Ligne, "-")) = "RTF]" Then
                '-> Initialiser pour la section en cours un fichier et l'ouvrir
                TempoStr = InitFileTempoRtf(aSpool)
                aSection.TempoRTF = CInt(Entry(1, TempoStr, "|"))
                aSection.TempoRtfString = Entry(2, TempoStr, "|")
                '-> Ligne de d�finition RTF de la section
                SwitchLect = 7
            End If
        ElseIf UCase$(Mid$(Ligne, 2, 2)) = "TB" Then
            '-> Cr�ation du nouvel objet tableau
            Set aTb = New Tableau
            aTb.Nom = Mid$(Entry(2, Ligne, "-"), 1, Len(Entry(2, Ligne, "-")) - 1)
            aTb.IdAffichage = aSpool.maquette.AddOrdreAffichage("TB-" & UCase$(Trim(aTb.Nom)))
            '-> Ajout dans la collection
            ErrorCode = 12
            TmpError = UCase$(Trim(aTb.Nom))
            
            aSpool.maquette.Tableaux.Add aTb, UCase$(Trim(aTb.Nom))
            '-> Positionner le pointeur de lecture
            SwitchLect = 8
        Else
            If UCase$(Trim(Ligne)) = "[PROGICIEL]" Then
                '-> analyse de la cle PROGICIEL de la maquette
                SwitchLect = 2
            End If
        End If '-> Si [
    Else
    'ElseIf InStr(1, Ligne, "\") <> 0 Then
        Select Case UCase$(Trim(Ligne))
            Case "\DEFENTETE�BEGIN"
                '-> D�finition de l'entete de la maquette
                SwitchLect = 1
            Case "\DEFENTETE�END"
                '-> Envoyer dans la proc�dure d'ajout adaptation des dimensions
                InitMaqEntete aSpool.maquette, Ligne, aSpool
                '-> Fin de la d�inition de l'entete de la maquette
                SwitchLect = 0
            Case "\BMP�END"
                '-> Lancer la cr�ation physique de l'objet BMP
                CreateBmpObj aBmp, aSpool
                '-> Fin de d�fintion d'un objet BMP : repositionner sur pointeur de section
                SwitchLect = 3
            Case "\CADRE�END"
                '-> Lancer la cr�ation physique du cadre
                CreateCadreObj aCadre, aSpool
                '-> Fin de d�finition d'un objet cadre : repositionner sur pointeur de section
                SwitchLect = 3
            Case "\CADRERTF�BEGIN"
                '-> Initialiser le fichier RTF pour lecture
                TempoStr = InitFileTempoRtf(aSpool)
                aCadre.TempoRTF = CInt(Entry(1, TempoStr, "|"))
                aCadre.TempoRtfString = Entry(2, TempoStr, "|")
                '-> D�but de d�finition RTF d'un cadre
                SwitchLect = 6
            Case "\CADRERTF�END"
                '-> Fermer le fichier tempo RTF affect�
                ErrorCode = 13
                Close #aCadre.TempoRTF
                '-> Lancer la g�n�ration physique du contenu RTF du cadre
                CreateRTFCadre aCadre, aSpool
                '-> Fin de d�fintion RTF d'un cadre : repositionner sur pointeur de section
                SwitchLect = 3
            Case "\RTF�END"
                '-> Fermer le fichier tempo RTF affect�
                ErrorCode = 13
                Close #aSection.TempoRTF
                '-> Lancer la g�n�ration physique du contenu RTF de la section de texte
                CreateRTFSection aSection, aSpool
                '-> Fin de d�finition RTF d'une section
                SwitchLect = 0
            Case "\TABLEAU�END"
                '-> Fin de d�finition d'un tableau
                SwitchLect = 0
            Case "\RANG�END"
                '-> Fin de d�finition d'une section de texte
                SwitchLect = 0
            Case Else
                Select Case SwitchLect
                    Case 1 'Analyse d'une ligne d'entete
                        InitMaqEntete aSpool.maquette, Ligne, aSpool
                    Case 2 'Analyse de la section PROGICIEL
                        If UCase$(Trim(Entry(1, Ligne, "="))) = "\PROG" Then
                            aSpool.maquette.Progiciel = Entry(2, Ligne, "=")
                        ElseIf UCase$(Trim(Entry(1, Ligne, "="))) = "\CLI" Then
                            aSpool.maquette.Client = Entry(2, Ligne, "=")
                        End If
                    Case 3 'On est dans une section de texte : analyse des tag <CADRE> et <BMP>  et <TEXTE>
                        Select Case UCase$(Trim(Entry(1, Ligne, "�")))
                            Case "\BMP"
                                '-> Cr�ation d'un nouvel objet BMP
                                Set aBmp = New ImageObj
                                aBmp.Nom = Entry(2, Ligne, "�")
                                aBmp.IdOrdreAffichage = aSection.AddOrdreAffichage("BMP-" & UCase$(Trim(aBmp.Nom)))
                                aBmp.SectionName = aSection.Nom
                                '-> Ajout dans la collection des Bmps
                                ErrorCode = 14
                                TmpError = aSection.Nom & "|" & UCase$(Trim(aBmp.Nom))
                                aSection.Bmps.Add aBmp, UCase$(Trim(aBmp.Nom))
                                '-> Positionnement du pointeur de lecture
                                SwitchLect = 4
                            Case "\CADRE"
                                '-> Cr�ation d'un nouvel objet cadre
                                Set aCadre = New Cadre
                                aCadre.Nom = Entry(2, Ligne, "�")
                                aCadre.IdAffichage = aSection.AddOrdreAffichage("CDR-" & UCase$(Trim(aCadre.Nom)))
                                aCadre.SectionName = aSection.Nom
                                ErrorCode = 15
                                TmpError = aSection.Nom & "|" & UCase$(Trim(aCadre.Nom))
                                aSection.Cadres.Add aCadre, UCase$(Trim(aCadre.Nom))
                                '-> Positionnement du pointeur de lecture
                                SwitchLect = 5
                            Case "\TEXTE"
                                '-> Fin de d�finition d'un objet Section: Cr�ation de la repr�sentation physique
                                CreateSectionObj aSection, aSpool
                                '-> on initialise les libell�s variables
                                GetLibelVariableForSection aSpool, aSection
                            Case Else
                                '->Dans ce cas, on lit une propri�t� de la section: ajouter dans la d�fintion de la section
                                InitSectionEntete aSpool, aSection, Ligne
                        End Select 'Selon la nature de la ligne
                    Case 4
                        '-> Ligne de d�finition d'un BMP
                        InitBmpEntete aBmp, aSpool, Ligne
                    Case 5
                        '-> Ligne de d�finition d'un cadre
                        InitCadreEntete aCadre, aSpool, Ligne
                    Case 6
                        '-> Imprimer la ligne RTF du cadre dans son fichier TEMPO
                        Print #aCadre.TempoRTF, Ligne
                    Case 7
                        '-> Imprimer la ligne RTF de la section texte dans son fichier tempo
                        Print #aSection.TempoRTF, Ligne
                    Case 8
                        If UCase$(Trim(Entry(1, Ligne, "�"))) = "\BEGIN" Then
                            '-> Cr�ation d'un nouvel objet Block
                            Set aBlock = New Block
                            aBlock.Nom = Entry(2, Ligne, "�")
                            aBlock.NomTb = Trim(UCase$(aTb.Nom))
                            aBlock.AlignementLeft = 3
                            aBlock.Left = 0
                            aBlock.IdOrdreAffichage = aTb.AddOrdreAffichage("BL-" & UCase$(Trim(aBlock.Nom)))
                            '-> Ajouter le block dans la collection des tableaux
                            ErrorCode = 16
                            TmpError = aSpool.MaquetteASCII & "|" & Trim(UCase$(aTb.Nom)) & UCase$(Trim(aBlock.Nom))
                            aTb.blocks.Add aBlock, "BL-" & UCase$(Trim(aBlock.Nom))
                            '-> Initialiser les matrices des lignes et colonnes
                            Erase sCol()
                            Erase sLigne()
                            LigLue = 1
                            '-> Postionnement du pointeur de lecture sur l'analyse des bloks
                            SwitchLect = 9
                        Else
                            '-> Ajouter � la d�finition du tableau en cours
                            InitTableauEntete aTb, aSpool, Ligne
                        End If
                    Case 9
                        If UCase$(Trim(Entry(1, Ligne, "�"))) = "\END" Then
                            '-> Fin de d�finition d'un objet block : lancer les initialisations et les cr�ations
                            InitCreateBlock aBlock, aTb, aSpool
                            '-> Se repositionner sur la lecture du tableau
                            SwitchLect = 8
                        Else
                            '-> Analyse d'un ligne d'un block
                            InitBlockEntete aBlock, aTb, aSpool, Ligne
                        End If
                End Select 'Selon la nature de la lecture de la ligne
        End Select
    End If '-> Si premier caract�re est un [
Next 'Pour toutes les lignes de la maquette


'-> G�n�ration ici de la section de texte pour la page des s�lections
'-> Cr�er nouvel objet RTF
Set aSection = New Section
aSection.Largeur = aSpool.maquette.Largeur - aSpool.maquette.MargeLeft * 2
aSection.Hauteur = aSpool.maquette.Hauteur - aSpool.maquette.MargeTop * 2
aSection.AlignementLeft = 2
aSection.AlignementTop = 2
aSection.BackColor = &HFFFFFF

'-> Charger un nouvel �diteur RTF dans la biblioth�que
Load frmLib.Rtf(IdRTf)
frmLib.Rtf(IdRTf).Font.Name = "Lucida Console"
aSection.IdRTf = IdRTf

'-> Incr�menter le compteur d'objet RTF
IdRTf = IdRTf + 1

'-> Ajouter dans la collection des secstion de la maquette
aSpool.maquette.Sections.Add aSection, SelectRTFKey
                

Exit Sub

GestError:
    
    If ErrorCode = 13 Then
        Reset
        Resume Next
    Else
        Call GestError(ErrorCode, TmpError, aSpool)
    End If

End Sub
Private Function InitFileTempoRtf(ByRef aSpool As Spool) As String

'---> Cette fonction creer un fichier tempo pour lecture des RTF et renvoie :
' Handle|NomFichier

On Error GoTo GestError

Dim TempFile As String
Dim hdlFile As Integer

TempFile = GetTempFileNameVB("RTF")
hdlFile = FreeFile
Open TempFile For Output As #hdlFile

InitFileTempoRtf = hdlFile & "|" & TempFile

Trace "Fichier temporaire du rtf page" & TempFile, 4

Exit Function

GestError:
    Call GestError(17, , aSpool)
    
End Function

Private Sub CreateRTFSection(ByRef aSection As Section, aSpool As Spool)

'---> Cette proc�dure charge le contenu du fichier TEmpoRTF adns l'objet affect� et le supprime
Dim ErrorCode As Integer

On Error GoTo GestError

'-> Charger le fichier RTF
frmLib.Rtf(aSection.IdRTf).LoadFile aSection.TempoRtfString
ErrorCode = -1
'-> Supprimer le fichier
Kill aSection.TempoRtfString

Exit Sub

GestError:
    
    If ErrorCode = -1 Then Resume Next
    Call GestError(25, aSection.Nom & "|" & aSection.TempoRtfString, aSpool)
    

End Sub

Private Sub CreateRTFCadre(ByRef aCadre As Cadre, aSpool As Spool)

'---> Cette proc�dure charge le contenu du fichier TEmpoRTF adns l'objet affect� et le supprime

On Error GoTo GestError

Dim ErrorCode As Integer

'-> Charger le fichier RTF
frmLib.Rtf(aCadre.IdRTf).LoadFile aCadre.TempoRtfString

'-> Supprimer le fichier
ErrorCode = -1
Kill aCadre.TempoRtfString

Exit Sub

GestError:

    If ErrorCode = -1 Then Resume Next
    Call GestError(24, aCadre.SectionName & "|" & aCadre.Nom & "|" & aCadre.TempoRtfString, aSpool)
    


End Sub


Private Sub InitMaqEntete(aMaq As maquette, Ligne As String, aSpool As Spool)

'---> Cette proc�dure effectue le setting des propri�t�s de l'entete de la maquette

Dim DefLigne As String
Dim ValueLigne As String
Dim Tempo As Single
Dim ErrorCode As Integer

On Error GoTo GestError

'-> Tester que la ligne commence par un "\"
If InStr(1, Ligne, "\") = 0 Then Exit Sub

'-> R�cup�ration des valeurs
DefLigne = Entry(1, Ligne, "�")
ValueLigne = Entry(2, Ligne, "�")
'-> Traiter la valeur de la ligne
Select Case UCase$(DefLigne)
    Case "\NOM"
        aMaq.Nom = ValueLigne
    Case "\HAUTEUR"
        aMaq.Hauteur = CSng(Convert(ValueLigne))
    Case "\LARGEUR"
        aMaq.Largeur = CSng(Convert(ValueLigne))
    Case "\MARGETOP"
        aMaq.MargeTop = CSng(Convert(ValueLigne))
    Case "\MARGELEFT"
        aMaq.MargeLeft = CSng(Convert(ValueLigne))
    Case "\ORIENTATION"
        aMaq.Orientation = CInt(ValueLigne)
    Case "\PAGEGARDE" 'Version 2.0
        If UCase$(ValueLigne) = "VRAI" Then
            aMaq.PageGarde = True
        Else
            aMaq.PageGarde = False
        End If
    Case "\RIGHTPAGE"
        If UCase$(ValueLigne) = "VRAI" Then
            AllowDebordement = True
        Else
            AllowDebordement = False
        End If
    Case "\NAVIGATION"
        If UCase$(ValueLigne) = "VRAI" Then
            IsNaviga = True
        Else
            'IsNaviga = False
        End If
    Case "\PAGESELECTION" 'Version 2.0
        If UCase$(ValueLigne) = "VRAI" Then
            aMaq.PageSelection = True
        Else
            aMaq.PageSelection = False
        End If
    Case "\DEFENTETE"
        '-> Ajuster les dimensions
        If aMaq.Orientation <> 1 Then
            Tempo = aMaq.Largeur
            aMaq.Largeur = aMaq.Hauteur
            aMaq.Hauteur = Tempo
        End If
    Case "\NAVIGAHTML"
        '-> Setting du navigateur
        aMaq.NavigaHTML = Trim(ValueLigne)
    Case "\CMD"
        If topDirect = False And ValueLigne <> "" And InStr(1, Command$, "Content.IE5", vbTextCompare) <> 0 And InStr(1, Command$, "CONTENT.IE5") = 0 Then
            '-> on relance le turbograph
            Shell App.Path & "\TurboGraph.exe " & ValueLigne & "|" & UCase(aSpool.fileName), vbNormalFocus
            MDIMain.Caption = "**FIN**"
            MDIMain.TerminateApp "**FIN**"
        End If
End Select

Exit Sub

'-> Gestion des erreurs
GestError:

    Call GestError(18, Ligne, aSpool)
    

End Sub

Private Sub InitSectionEntete(ByRef aSpool As Spool, ByRef aSection As Section, Ligne As String)

'---> Cette proc�dure effectue le setting des propri�t�s d'une section de texte

Dim DefLigne As String
Dim ValueLigne As String

On Error GoTo GestError

If InStr(1, Ligne, "\") = 0 Then Exit Sub

 '-> R�cup�rer les valeurs
DefLigne = UCase$(Entry(1, Ligne, "�"))
ValueLigne = Entry(2, Ligne, "�")

'-> Analyse
Select Case DefLigne
    Case "\CONTOUR"
        If CInt(ValueLigne) = 0 Then
            aSection.Contour = False
        Else
            aSection.Contour = True
        End If
    Case "\HAUTEUR"
        aSection.Hauteur = CSng(Convert(ValueLigne))
    Case "\LARGEUR"
        aSection.Largeur = CSng(Convert(ValueLigne))
    Case "\ALIGNEMENTTOP"
        aSection.AlignementTop = CInt(ValueLigne)
    Case "\TOP"
        aSection.Top = CSng(Convert(ValueLigne))
    Case "\ALIGNEMENTLEFT"
        aSection.AlignementLeft = CInt(ValueLigne)
    Case "\LEFT"
        aSection.Left = CSng(Convert(ValueLigne))
    Case "\BACKCOLOR"
        aSection.BackColor = CLng(ValueLigne)
    Case "\TEXTE"
End Select

Exit Sub

GestError:
    
    Call GestError(27, aSection.Nom & "|" & Ligne, aSpool)

End Sub

Private Sub InitBmpEntete(aBmp As ImageObj, aSpool As Spool, Ligne As String)

'---> Cette proc�dure effectue le setting des propri�t�s d'un objet BMP

Dim DefLigne As String
Dim ValueLigne As String

On Error GoTo GestError

If InStr(1, Ligne, "\") = 0 Then Exit Sub

 '-> R�cup�rer les valeurs
DefLigne = UCase$(Entry(1, Ligne, "�"))
ValueLigne = Entry(2, Ligne, "�")

'-> Analyse
Select Case DefLigne
    Case "\LEFT"
        aBmp.Left = CSng(Convert(ValueLigne))
    Case "\TOP"
        aBmp.Top = CSng(Convert(ValueLigne))
    Case "\LARGEUR"
        aBmp.Largeur = CSng(Convert(ValueLigne))
    Case "\HAUTEUR"
        aBmp.Hauteur = CSng(Convert(ValueLigne))
    Case "\DEFAULT"
        aBmp.Default = ValueLigne
    Case "\FICHIER"
        aBmp.Fichier = ValueLigne
    Case "\CONTOURBMP"
        If CInt(ValueLigne) = 1 Then
            aBmp.Contour = True
        Else
            aBmp.Contour = False
        End If
    Case "\VARIABLE"
        If CInt(ValueLigne) = 1 Then
            aBmp.isVariable = True
        Else
            aBmp.isVariable = False
        End If
    Case "\USEASSOCIATION"
        If CInt(ValueLigne) = 1 Then
            aBmp.UseAssociation = True
        Else
            aBmp.UseAssociation = False
        End If
    Case "\PATH"
        aBmp.Path = ValueLigne
End Select

Trace "InitBmpEntete      " & "Path=" & aBmp.Path & "UseAssociation" & aBmp.UseAssociation & "Variable" & aBmp.isVariable & "Fichier=" & aBmp.Fichier

Exit Sub

GestError:
    
    Call GestError(28, aBmp.SectionName & "|" & aBmp.Nom & "|" & Ligne, aSpool)
    

End Sub

Private Sub InitCadreEntete(ByRef aCadre As Cadre, ByRef aSpool As Spool, Ligne As String)

'---> Cette proc�dure effectue le setting des propri�t�s d'un cadre

Dim DefLigne As String
Dim ValueLigne As String

On Error GoTo GestError

If InStr(1, Ligne, "\") = 0 Then Exit Sub

 '-> R�cup�rer les valeurs
DefLigne = UCase$(Entry(1, Ligne, "�"))
ValueLigne = Entry(2, Ligne, "�")

'-> Analyse
Select Case DefLigne
    Case "\LEFT"
        aCadre.Left = CSng(Convert(ValueLigne))
    Case "\TOP"
        aCadre.Top = CSng(Convert(ValueLigne))
    Case "\DISTANCE" 'Equivalent � MargeInterne
        aCadre.MargeInterne = CSng(Convert(ValueLigne))
    Case "\HAUTEUR"
        aCadre.Hauteur = CSng(Convert(ValueLigne))
    Case "\LARGEUR"
        aCadre.Largeur = CSng(Convert(ValueLigne))
    Case "\LARGEURBORDURE"
            aCadre.LargeurTrait = CInt(ValueLigne)
    Case "\ROUNDRECT"
            aCadre.IsRoundRect = CInt(ValueLigne)
    Case "\CONTOUR"
        '-> Bordure haut
        If Entry(1, ValueLigne, ",") = "1" Then
            aCadre.Haut = True
        Else
            aCadre.Haut = False
        End If
        '-> Bordure bas
        If Entry(2, ValueLigne, ",") = "1" Then
            aCadre.Bas = True
        Else
            aCadre.Bas = False
        End If
        '-> Bordure gauche
        If Entry(3, ValueLigne, ",") = "1" Then
            aCadre.Gauche = True
        Else
            aCadre.Gauche = False
        End If
        '-> Bordure droite
        If Entry(4, ValueLigne, ",") = "1" Then
            aCadre.Droite = True
        Else
            aCadre.Droite = False
        End If
    Case "\COULEUR"
        aCadre.BackColor = CLng(ValueLigne)
End Select

Exit Sub

GestError:

    Call GestError(29, aCadre.SectionName & "|" & aCadre.Nom & "|" & Ligne, aSpool)
    

End Sub

Private Sub CreateSectionObj(ByRef aSection As Section, ByRef aSpool As Spool)

'---> Cette proc�dure cr�er les diff�rents objets pour impression de la section

On Error GoTo GestError

'-> Cr�ation d'un contenuer RTF
Load frmLib.Rtf(IdRTf)
aSection.IdRTf = IdRTf

'-> Appliquer les propri�t�s
frmLib.Rtf(IdRTf).BackColor = aSection.BackColor
If aSection.Contour Then
    frmLib.Rtf(IdRTf).BorderStyle = rtfFixedSingle
Else
    frmLib.Rtf(IdRTf).BorderStyle = rtfNoBorder
End If

'-> Incr�menter le compteur d'objets RTF
IdRTf = IdRTf + 1
Exit Sub

GestError:

    Call GestError(26, aSection.Nom, aSpool)

End Sub

Private Sub CreateCadreObj(ByRef aCadre As Cadre, ByRef aSpool As Spool)

'---> Cette proc�dure cr�er une instance d'un objet RTF pour impression du cadre

On Error GoTo GestError

'-> charger un nouvel objet du type RTF
Load frmLib.Rtf(IdRTf)
aCadre.IdRTf = IdRTf

'-> Setting des prori�t�s
frmLib.Rtf(IdRTf).BackColor = aCadre.BackColor
frmLib.Rtf(IdRTf).Visible = True

'-> Incr�menter le compteur d'objets
IdRTf = IdRTf + 1

Exit Sub

GestError:

    Call GestError(23, aCadre.SectionName & "|" & aCadre.Nom, aSpool)
    


End Sub

Private Sub CreateBmpObj(ByRef aBmp As ImageObj, ByRef aSpool As Spool)

'---> Cette proc�dure cr�er une instance d'un objet picturebox pour impression du BMP

Dim Res As Long
Dim lpBuffer As String
Dim TempoStr As String
Dim version As String
Dim Lecteur As String
Dim i As Integer
Dim ErrorCode As Integer
Dim TmpError As String
Dim V6Root As String
Dim ImagePath As String


On Error GoTo GestError

'-> Chargement de la repr�sentation physique
ErrorCode = 19
TmpError = ""
Load frmLib.PicObj(IdBmp)

'-> Contour du Bmp
If aBmp.Contour Then
    frmLib.PicObj(IdBmp).BorderStyle = 1
Else
    frmLib.PicObj(IdBmp).BorderStyle = 0
End If

'-> Donner ses dimensions au Bmp
ErrorCode = 20
TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Largeur & "|" & aBmp.Hauteur
frmLib.PicObj(IdBmp).height = frmLib.ScaleX(aBmp.Hauteur, 7, 1)
frmLib.PicObj(IdBmp).width = frmLib.ScaleY(aBmp.Largeur, 7, 1)

'-> Selon la version
If VersionTurbo = 0 Then 'On est en version Pathv6

    '-> V�rifier que l'on trouve le fichier Ini
    If TurboMaqIniFile = "" Then GoTo AfterLoading
    
    '-> R�cup�rer le path Root
    lpBuffer = Space$(100)
    Res = GetPrivateProfileString("Param", "$ROOT", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
    If Res <> 0 Then V6Root = Mid$(lpBuffer, 1, Res)
    Trace "aBmp.Path : " + aBmp.Path, 1
    Select Case UCase$(aBmp.Path)
        Case "PARAM"
            Res = GetPrivateProfileString("Param", "$MAQGUIPATH", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
            If Res = 0 Then
                aBmp.Fichier = ""
                GoTo AfterLoading
            End If
            ImagePath = Replace(Replace(UCase$(Mid$(lpBuffer, 1, Res)), "$ROOT", V6Root), "$APP", "emilie")
        Case "APP"
            Res = GetPrivateProfileString("Param", "$MAQGUIPATH", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
            If Res = 0 Then
                aBmp.Fichier = ""
                GoTo AfterLoading
            End If
            ImagePath = Replace(Replace(UCase$(Mid$(lpBuffer, 1, Res)), "$ROOT", V6Root), "$APP", aSpool.maquette.Progiciel)
        Case "CLIENT"
            Res = GetPrivateProfileString("Param", "$IDENTPATH", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
            If Res = 0 Then
                aBmp.Fichier = ""
                GoTo AfterLoading
            End If
            ImagePath = Replace(Replace(Replace(UCase$(Mid$(lpBuffer, 1, Res)), "$ROOT", V6Root), "$APP", aSpool.maquette.Progiciel), "$IDENT", aSpool.maquette.Client)
    End Select
    Trace "ImagePath : " + ImagePath, 1
    '-> V�rifier si on trouve l'image
    If Dir$(ImagePath & "\" & aBmp.Fichier) <> "" Then
        ErrorCode = 22
        TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Fichier
        frmLib.PicObj(IdBmp).Picture = LoadPicture(ImagePath & "\" & aBmp.Fichier)
'        aBmp.Fichier = lpBuffer & aBmp.Fichier PIERROT : client-serveur en VERSION V6
        aBmp.Fichier = ImagePath & "\" & aBmp.Fichier
   ' Else   PIERROT : client-serveur en VERSION V6 : ne pas changer le nom du fichier de forme ^0001 si path parametr� => MODE PARAM
   '     aBmp.Fichier = ""
    Else
        '-> le plan B
        If Dir$(App.Path & "\Images\" & aBmp.Fichier) <> "" Then
            ErrorCode = 22
            TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Fichier
            frmLib.PicObj(IdBmp).Picture = LoadPicture(App.Path & "\Images\" & aBmp.Fichier)
    '        aBmp.Fichier = lpBuffer & aBmp.Fichier PIERROT : client-serveur en VERSION V6
            aBmp.Fichier = App.Path & "\Images\" & aBmp.Fichier
        End If
    End If 'Si on trouve l'image
Else
    '-> On est en version professionnelle
    If aBmp.isVariable Then
        aBmp.IsAutosize = True
    Else
        aBmp.IsAutosize = False
        Trace "CreateBmpObj non variable aBmp.Fichier : " + aBmp.Fichier, 1
        If Trim(aBmp.Fichier) <> "" Then
            '-> Rechercher l'image dans le sous r�pertoire Images
            lpBuffer = App.Path & "\Images\" & aBmp.Fichier
            '-> Tester que l'on trouve l'image
            If Dir$(lpBuffer, vbNormal) <> "" Then
                '-> Essayer de charger l'image
                ErrorCode = 22
                TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Fichier
                frmLib.PicObj(IdBmp).Picture = LoadPicture(lpBuffer)
                aBmp.Fichier = lpBuffer
            Else
                '-> on essaye eventuellement de telecharger le fichier
                Trace "CreateBmpObj telechargement : " + TurboGraphWebFile & "/deallogo/" & aBmp.Fichier & " vers " & App.Path & "\Images\" & aBmp.Fichier, 1
                If TelechargeFile(TurboGraphWebFile & "/deallogo/" & aBmp.Fichier, App.Path & "\Images\" & aBmp.Fichier) Then
                    '-> Rechercher l'image dans le sous r�pertoire Images
                    lpBuffer = App.Path & "\Images\" & aBmp.Fichier
                    '-> Tester que l'on trouve l'image
                    If Dir$(lpBuffer, vbNormal) <> "" Then
                        '-> Essayer de charger l'image
                        ErrorCode = 22
                        TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Fichier
                        frmLib.PicObj(IdBmp).Picture = LoadPicture(lpBuffer)
                        aBmp.Fichier = lpBuffer
                    End If
                    Trace "Telechargement OK", 2
                Else
                    '-> impossible de trouver le fichier associ�
                    aBmp.Fichier = ""
                    Trace "impossible de telecharger l'image suivante" & TurboGraphWebFile & "/deallogo/" & aBmp.Fichier, 1
                    If TypeOf Sortie Is PictureBox Then
                        MDIMain.StatusBar1.Panels(1).Text = "Impossible de telecharger l'image suivante : " & TurboGraphWebFile & "/deallogo/" & aBmp.Fichier
                    End If
                End If
            End If
        Else 'La propri�t� fichier est � blanc
            aBmp.Fichier = ""
        End If
    End If
End If 'Selon la version

Trace "CreateBmpObj chemin de l'image aBmp.Fichier : " + aBmp.Fichier, 3

AfterLoading:

'-> Positionner l'objet
frmLib.PicObj(IdBmp).Top = frmLib.PicObj(IdBmp).ScaleY(aBmp.Top, 7, 1)
frmLib.PicObj(IdBmp).Left = frmLib.PicObj(IdBmp).ScaleX(aBmp.Left, 7, 1)
frmLib.PicObj(IdBmp).AutoSize = aBmp.IsAutosize

'-> Enregistrer l'index du picture box associ�
aBmp.IdPic = IdBmp

'-> Rendre l'objet visible
frmLib.PicObj(IdBmp).Visible = True

'-> Incr�menter
IdBmp = IdBmp + 1


Exit Sub

GestError:

    Call GestError(ErrorCode, TmpError, aSpool)
    

End Sub

Private Sub InitTableauEntete(ByRef aTb As Tableau, ByRef aSpool As Spool, Ligne As String)

'---> Cette proc�dure effectue le settin g des prorpi�t�s d'un tableau

Dim DefLigne As String
Dim ValueLigne As String

On Error GoTo GestError

If InStr(1, Ligne, "\") = 0 Then Exit Sub

 '-> R�cup�rer les valeurs
DefLigne = UCase$(Entry(1, Ligne, "�"))
ValueLigne = Entry(2, Ligne, "�")

'-> Analyse
Select Case DefLigne
    Case "\LARGEURTB"
        aTb.Largeur = CSng(Convert(ValueLigne))
    Case "\ORIENTATIONTB"
        aTb.Orientation = CInt(ValueLigne)
End Select


Exit Sub



GestError:
    Call GestError(30, aTb.Nom & "|" & Ligne, aSpool)
    


End Sub

Private Sub InitBlockEntete(ByRef aBlock As Block, aTb As Tableau, aSpool As Spool, Ligne As String)

'---> Cette proc�dure effectue le setting des propri�t�s d'un block

Dim DefLigne As String
Dim ValueLigne As String

On Error GoTo GestError

If InStr(1, Ligne, "\") = 0 Then Exit Sub

 '-> R�cup�rer les valeurs
DefLigne = UCase$(Entry(1, Ligne, "�"))
ValueLigne = Entry(2, Ligne, "�")

'-> Analyse
Select Case DefLigne
    Case "\ALIGNEMENT" 'Garder pour compatibilit� 1.00
        If CInt(ValueLigne) = 0 Then 'Marge gauche
            aBlock.AlignementLeft = 2
        ElseIf CInt(ValueLigne) = 1 Then 'Marge Droite
            aBlock.AlignementLeft = 4
        ElseIf CInt(ValueLigne) = 2 Then 'Centr�
            aBlock.AlignementLeft = 3
        ElseIf CInt(ValueLigne) = 3 Then 'Alignement Libre
            aBlock.AlignementLeft = 5 'La valeur Left est aliment�e par la valeur "\DISTANCE"
        End If
    Case "\ALIGNLEFT" 'Version 2.00 de "\Alignement"
        aBlock.AlignementLeft = CInt(ValueLigne)
    Case "\LEFT" 'Valeur de AlignLeft quand il est sp�cifi� : valeur 5
        aBlock.Left = CSng(Convert(ValueLigne))
    Case "\ALIGNTOP" 'Version  2.00 de "\AlignementBlockVertical"
        aBlock.AlignementTop = CInt(ValueLigne)
    Case "\TOP" 'Valeur de AlignTop quand il est sp�cifi� : valeur 5
        aBlock.Top = CSng(Convert(ValueLigne))
    Case "\LARGEUR"
        aBlock.Largeur = CSng(Convert(ValueLigne))
    Case "\HAUTEUR"
        aBlock.Hauteur = CSng(Convert(ValueLigne))
    Case "\LIGNE"
        aBlock.NbLigne = CInt(ValueLigne)
        ReDim sLigne(1 To CInt(ValueLigne))
    Case "\COLONNE"
        aBlock.NbCol = CInt(ValueLigne)
        ReDim sCol(1 To CInt(ValueLigne))
    Case "\EXPORTLIG"
        aBlock.ListeExcel = ValueLigne
    Case "\ACCESDET"
        aBlock.KeyAccesDet = Trim(ValueLigne)
    Case "\COL"
        sLigne(LigLue) = Ligne
        LigLue = LigLue + 1
End Select

Exit Sub

GestError:
    Call GestError(31, aTb.Nom & "|" & aBlock.Nom & "|" & Ligne, aSpool)

End Sub
Private Sub InitCreateBlock(ByRef aBlock As Block, ByRef aTb As Tableau, ByRef aSpool As Spool)

'---> Cette proc�dure cr�er les diff�rentes cellules � partir des matrices sCol et sLigne

Dim aCell As Cellule
Dim i As Integer
Dim j As Integer
Dim DefLigne As String
Dim DefCellule As String
Dim DefBordure As String
Dim ErrorCode As Integer
Dim AccesDetail As String
Dim ListeCell As String

On Error GoTo GestError

'-> Initialiser les colonnes et les lignes
aBlock.Init

'-> Gestion des exports des lignes vers Excel Attention , cette propri�t� _
ne contient que la liste des lignes non exportables vers EXCEL
If Trim(aBlock.ListeExcel) <> "" Then
    For i = 1 To NumEntries(aBlock.ListeExcel, "|")
        aBlock.SetExportExcel CInt(Entry(i, aBlock.ListeExcel, "|")), False
    Next
End If

'-> Alimenter les Matrices de ligne te de colonne et cr�ation des cellules
For i = 1 To LigLue - 1
    '-> R�cup�ration de la d�finition de la ligne
    ErrorCode = 32
    DefLigne = sLigne(i)
    '-> Setting de la hauteur de ligne
    aBlock.SetHauteurLig i, CSng(Convert(Entry(2, DefLigne, "�")))
    '-> Recup�rer QUE la liste des colonnes
    DefLigne = Entry(3, DefLigne, "�")
    For j = 1 To aBlock.NbCol
        ErrorCode = 33
        '-> Recup de la d�finition de la colonne
        DefCellule = Entry(j, DefLigne, "|")
        '-> Setting de la largeur
        aBlock.SetLargeurCol j, CSng(Convert(Entry(3, DefCellule, ";")))
        '-> Cr�ation de la cellule associ�e
        Set aCell = New Cellule
        aCell.Ligne = i
        aCell.Colonne = j
        '-> Ajout dans le tableau
        aBlock.Cellules.Add aCell, "L" & i & "C" & j
        '-> setting des propri�t�s
        aCell.CellAlign = CInt(Entry(1, DefCellule, ";"))
        aCell.Contenu = Entry(2, DefCellule, ";")
        '-> Initialisation des champs
        aCell.InitChamp
        '-> Gestion des bordures
        DefBordure = Entry(4, DefCellule, ";")
        If UCase$(Entry(1, DefBordure, ",")) = "VRAI" Then
            aCell.BordureHaut = True
        Else
            aCell.BordureHaut = False
        End If
        If UCase$(Entry(2, DefBordure, ",")) = "VRAI" Then
            aCell.BordureBas = True
        Else
            aCell.BordureBas = False
        End If
        If UCase$(Entry(3, DefBordure, ",")) = "VRAI" Then
            aCell.BordureGauche = True
        Else
            aCell.BordureGauche = False
        End If
        If UCase$(Entry(4, DefBordure, ",")) = "VRAI" Then
            aCell.BordureDroite = True
        Else
            aCell.BordureDroite = False
        End If
        '-> Propri�t�s de Font
        aCell.FontName = Entry(5, DefCellule, ";")
        aCell.FontSize = CSng(Convert(Entry(6, DefCellule, ";")))
        If UCase$(Trim(Entry(7, DefCellule, ";"))) = "VRAI" Then
            aCell.FontBold = True
        Else
            aCell.FontBold = False
        End If
        If UCase$(Trim(Entry(8, DefCellule, ";"))) = "VRAI" Then
            aCell.FontItalic = True
        Else
            aCell.FontItalic = False
        End If
        If UCase$(Trim(Entry(9, DefCellule, ";"))) = "VRAI" Then
            aCell.FontUnderline = True
        Else
            aCell.FontUnderline = False
        End If
        aCell.FontColor = CLng(Entry(10, DefCellule, ";"))
        If CLng(Entry(11, DefCellule, ";")) = 999 Then
            aCell.BackColor = 16777215
            aCell.FondTransparent = True
        Else
            aCell.BackColor = CLng(Entry(11, DefCellule, ";"))
            aCell.FondTransparent = False
        End If
        '-> AutoAjust
        If UCase$(Entry(12, DefCellule, ";")) = "VRAI" Then
            aCell.AutoAjust = True
        Else
            aCell.AutoAjust = False
        End If
        '-> Format et Type
        DefBordure = Entry(13, DefCellule, ";")
        If Entry(1, DefBordure, "@") <> "" Then aCell.TypeValeur = CInt(Entry(1, DefBordure, "@"))
        aCell.Msk = Entry(2, DefBordure, "@")
        If NumEntries(DefBordure, "@") = 3 Then aCell.PrintZero = True
        
        '-> Export vers Excel Fusion
        If NumEntries(DefCellule, ";") > 13 Then
            '-> R�cup�rer le param�trage
            DefBordure = Entry(14, DefCellule, ";")
            aCell.IsFusion = CBool(Entry(1, DefBordure, "@"))
            aCell.ColFusion = CInt(Entry(2, DefBordure, "@"))
        End If
        
        '-> Impl�mnter le mod�le objet associ� � excel
        aCell.TurboToExcel
        '-> on pose les libell�s variables
        GetLibelVariableForCell aBlock, aSpool, aCell
    Next 'Pour toute les colonnes
Next 'Pour toutes les lignes

'-> Gestion de l'acc�s au d�tail
If aBlock.KeyAccesDet <> "" Then
    '-> Basculer
    AccesDetail = aBlock.KeyAccesDet
    '-> Setting des param�tres du block
    aBlock.UseAccesDet = True
    aBlock.KeyAccesDet = Entry(1, AccesDetail, "�")
    '-> Setting des cellules servant � l'acc�s au d�tail
    ListeCell = Entry(2, AccesDetail, "�")
    For i = 1 To NumEntries(ListeCell, "|")
        '-> Pointeur vers une cellule
        Set aCell = aBlock.Cellules("L" & Entry(1, Entry(i, ListeCell, "|"), "-") & "C" & Entry(2, Entry(i, ListeCell, "|"), "-"))
        '-> forcer le format
        aCell.FontColor = rgb(0, 0, 255)
        aCell.FontUnderline = True
        '-> Indiquer qu'elle sert d'acc�s au d�tail
        aCell.UseAccesDet = True
    Next 'Pour toutes les d�finitions de cellules
End If 'S'il y a l'acc�s au d�tail sur ce block

'-> Convertion des dimensions des cellules en pixels
InitBlock aBlock, aSpool


Exit Sub

GestError:
    If ErrorCode = 32 Then
        Call GestError(ErrorCode, aTb.Nom & "|" & aBlock.Nom, aSpool)
    Else
        Call GestError(ErrorCode, aTb.Nom & "|" & aBlock.Nom & "|" & DefCellule, aSpool)
    End If
    
End Sub



Public Sub InitBlock(ByVal BlockCours As Block, aSpool As Spool)

'---> Cette proc�dure initialise les matrices Lignes (), Colonnes () et Cells _
en partant des largeurs de colonnes et hauteur de lignes

Dim i As Integer
Dim j As Integer
Dim NbCol As Integer
Dim NbLig As Integer
'-> Dimensions en CM
Dim HauteurCm As Single
Dim LargeurCm As Single
'-> Convertion en Pixels pour dessin
Dim HauteurPix As Long
Dim LargeurPix As Long
Dim hPix As Long
Dim lPix As Long
'-> Matrices des lignes et colonnes
Dim Lignes() As Long
Dim Colonnes() As Long

Dim LargeurColPix As Long
Dim HauteurLigPix As Long

Dim aCell As Cellule
Dim PosX As Long
Dim PosY As Long

On Error GoTo ErrorOpen

'-> R�cup�ration des propri�t�s
NbCol = BlockCours.NbCol
NbLig = BlockCours.NbLigne
HauteurCm = BlockCours.Hauteur
LargeurCm = BlockCours.Largeur

'-> Convertion en pixels
If TypeOf Sortie Is PictureBox Then
    '-> Ecran
    LargeurPix = CLng(frmLib.ScaleX(LargeurCm, 7, 3))
    HauteurPix = CLng(frmLib.ScaleY(HauteurCm, 7, 3))
Else
    'Printer.Copies = 1
    LargeurPix = CLng(Printer.ScaleX(LargeurCm, 7, 3))
    HauteurPix = CLng(Printer.ScaleY(CDbl(HauteurCm), 7, 3))
End If

'-> Affecter la valeur au block
BlockCours.LargeurPix = LargeurPix
BlockCours.HauteurPix = HauteurPix

'-> Calcul des dimensions de base
ReDim Lignes(1 To NbLig)
ReDim Colonnes(1 To NbCol)

'-> Largeur de base des colonnes
For i = 1 To NbCol
    Colonnes(i) = Fix((BlockCours.GetLargeurCol(i) / LargeurCm) * ((LargeurPix - NbCol + 1)))
    lPix = lPix + Colonnes(i)
Next

'-> Hauteur de base des lignes
 For i = 1 To NbLig
    Lignes(i) = Fix((BlockCours.GetHauteurLig(i) / HauteurCm) * (HauteurPix - (NbLig + 1)))
    hPix = hPix + Lignes(i)
Next

Dim aCumul As Double
Dim bCumul  As Long
Dim a As Double

'-> Lissage des colonnes
bCumul = 1

For i = 1 To NbCol
    '-> Largeur en pixel normale
    aCumul = aCumul + BlockCours.GetLargeurCol(i)
    LargeurColPix = CLng(Sortie.ScaleX(aCumul, 7, 3))
    bCumul = bCumul + Colonnes(i) + 1
    a = LargeurColPix - bCumul
    Colonnes(i) = Colonnes(i) + a
    bCumul = LargeurColPix
Next

'-> Lissage des lignes
aCumul = 0
bCumul = 1

For i = 1 To NbLig
    '-> Hauteur de pixel normale
    aCumul = aCumul + BlockCours.GetHauteurLig(i)
    HauteurLigPix = CInt(Sortie.ScaleY(aCumul, 7, 3))
    bCumul = bCumul + Lignes(i) + 1
    a = HauteurLigPix - bCumul
    Lignes(i) = Lignes(i) + a
    bCumul = HauteurLigPix
Next

'-> Pixel de d�part
PosY = 1
PosX = 1

For i = 1 To NbCol
    For j = 1 To NbLig
        '-> Mettre � jour les valeurs des cellules
        Set aCell = BlockCours.Cellules("L" & j & "C" & i)
        aCell.x1 = PosX
        aCell.y1 = PosY
        aCell.x2 = PosX + Colonnes(i) + 1
        aCell.y2 = PosY + Lignes(j) + 1
        '-> Incr�menter le compteur de position
        PosY = PosY + Lignes(j) + 1
    Next 'pour toutes les lignes
    '-> initialisation des valeurs
    PosX = PosX + Colonnes(i) + 1
    PosY = 1
Next 'Pour toutes les colonnes

'-> Lib�rer le pointeur de cellule
Set aCell = Nothing

'-> Sortir de la fonction
Exit Sub

ErrorOpen:

    Call GestError(34, BlockCours.NomTb & "|" & BlockCours.Nom, aSpool)

End Sub

Public Sub DisplayFileGUI(ByVal fileName As String)

'---> Cette proc�dure charge et affiche un fichier en particulier

Dim aNode As Node
Dim aNode2 As Node
Dim aNode3 As Node
Dim aFichier As Fichier
Dim aSpool As Spool
Dim i As Long
Dim aLb As Libelle
Dim aFrm As frmDisplaySpool
Dim IsTitrePage As Boolean
Dim IsTitreMail As Boolean
Dim aBool As Boolean

On Error Resume Next

'-> Bloquer l'interface
MDIMain.Enabled = False
Screen.MousePointer = 11

'-> Positionner le p�riph�rique d'impression sur un objet de type PictureBox
Set Sortie = frmLib.PicObj(0)

'-> Lancer le chargement et l'analyse du fichier s�lectionn�
If MDIMain.Visible Then MDIMain.Show
fileName = AnalyseFileToPrint(fileName)
'-> on regarde si cela n'existe pas deja
On Error GoTo suite
'-> Cr�er son icone dans le treeview
Set aNode = MDIMain.TreeNaviga.Nodes.Add(, , UCase$(Trim(fileName)), fileName, "Close")
If 1 = 0 Then
suite:
Set aNode = MDIMain.TreeNaviga.Nodes(UCase$(Trim(fileName)))
End If

aNode.Tag = "FICHIER"
aNode.ExpandedImage = "Open"
aNode.Expanded = True

'-> Pointer sur la classe libell�
Set aLb = Libelles("MDIMAIN")

'-> Pointer sur le fichier
Set aFichier = Fichiers(UCase$(Trim(fileName)))
Trace ("DisplayFileGUI")
'-> Cr�er une icone par spool
LockWindowUpdate MDIMain.TreeNaviga.Hwnd
For Each aSpool In aFichier.Spools
    DoEvents
    '-> on regarde si on a specifi� un titre
    If InStr(1, aSpool.GetMaq, "[TITRE]") <> 0 Then
        IsTitrePage = True
    Else
        IsTitrePage = False
    End If
    '-> on regarde si on a specifi� un titre
    If InStr(1, aSpool.GetMaq, "[MAIL]") <> 0 Then
        IsTitreMail = True
    Else
        IsTitreMail = False
    End If
    '->on verifie qu'on a pas deja
    For Each aNode2 In MDIMain.TreeNaviga.Nodes
        DoEvents
        If aNode2.Key = aNode.Key & "�" & aSpool.Key Then
            aBool = True
            Screen.MousePointer = 0
            MDIMain.Enabled = True
            Exit Sub
        End If
    Next
    If aBool = False Then Set aNode2 = MDIMain.TreeNaviga.Nodes.Add(aNode.Key, 4, aNode.Key & "�" & aSpool.Key, aSpool.SpoolText, "Spool")
    aNode2.Tag = "SPOOL"
    aNode2.Expanded = True
    '-> Soit la liste des erreurs
    If aSpool.NbError = 0 Then
        '-> Afficher une page pour la liste des s�lections utilisateur
        If aSpool.IsSelectionPage Then
            Set aNode3 = MDIMain.TreeNaviga.Nodes.Add(aNode2.Key, 4, aNode2.Key & "�PAGE|0", aLb.GetCaption(12) & "S�lection", "Page")
            aNode3.Tag = "PAGE"
        End If
        
        '-> Afficher une icone par page
        For i = 1 To aSpool.nbPage
            If i Mod 500 = 0 Then DoEvents
            '-> Recuperer eventuellement le titre de la feuille
            If IsTitrePage And GetTitrePage(aSpool, i) <> "" Then
                Set aNode3 = MDIMain.TreeNaviga.Nodes.Add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, GetTitrePage(aSpool, i), "Page")
            Else
                Set aNode3 = MDIMain.TreeNaviga.Nodes.Add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, aLb.GetCaption(12) & i, "Page")
            End If
            aNode3.Tag = "PAGE"
            '-> a partir d'une valeur on arrete
            If i = 32000 Then
                Set aNode3 = MDIMain.TreeNaviga.Nodes.Add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & (i + 1), "...", "Page")
                Exit For
            End If
        Next
    Else
        aBool = False
        For Each aNode3 In MDIMain.TreeNaviga.Nodes
            If aNode3.Key = aNode2.Key & "�ERROR" Then
                aBool = True
                Exit For
            End If
        Next
        '-> Afficher l'icone de la page d'erreur
        If aBool = False Then Set aNode3 = MDIMain.TreeNaviga.Nodes.Add(aNode2.Key, 4, aNode2.Key & "�ERROR", aLb.GetCaption(11), "Warning")
        aNode3.Tag = "ERROR"
    End If 'S'il y a des erreurs
Next 'Pour tous les spools
'LockWindowUpdate 0

'-> Afficher le premier Spool dans l'interface
If aFichier.NbSpool <> 0 Then
    If aFichier.Spools(1).IsSelectionPage And aFichier.Spools(1).nbPage = 0 And aFichier.NbSpool >= 2 Then
        Set aSpool = aFichier.Spools(2)
    Else
        Set aSpool = aFichier.Spools(1)
    End If
    '-> Cr�er une nouvelle instance
    Set aFrm = New frmDisplaySpool
    '-> Affect� le spool
    Set aFrm.aSpool = aSpool
    '-> Affecter la feuille au spool
    Set aSpool.frmdisplay = aFrm
    '-> Imprimer si necessaire
    If aSpool.NbError <> 0 Then
        aSpool.DisplayInterfaceByPage (1)
        '-> Charger le fichier en erreur
        If Dir(aSpool.fileName) <> "" Then aFrm.RtfError.LoadFile aSpool.fileName
    Else
        '-> Imprimer la premi�re page
        PrintPageSpool aSpool, 1
        '-> Afficher la feuille en fonction du r�sultat de l'impression
        aSpool.DisplayInterfaceByPage (1)
    End If
End If
GestError:
    '-> Bloquer l'interface
    MDIMain.Enabled = True
    Screen.MousePointer = 0


End Sub

Public Sub DisplayFileTree(ByVal fileName As String)

'---> Cette proc�dure charge et affiche un fichier en particulier

Dim aNode As Node
Dim aNode2 As Node
Dim aNode3 As Node
Dim aFichier As Fichier
Dim aSpool As Spool
Dim i As Integer
Dim aLb As Libelle
Dim aFrm As frmDisplaySpool
Dim IsTitrePage As Boolean
Dim IsTitreMail As Boolean

On Error Resume Next

'-> Bloquer l'interface
MDIMain.Enabled = False
Screen.MousePointer = 11

'-> Positionner le p�riph�rique d'impression sur un objet de type PictureBox
Set Sortie = frmLib.PicObj(0)

'-> Lancer le chargement et l'analyse du fichier s�lectionn�
'Call AnalyseFileToPrint(FileName)

'-> Cr�er son icone dans le treeview
Set aNode = MDIMain.TreeNaviga.Nodes.Add(, , UCase$(Trim(fileName)), fileName, "Close")
aNode.Tag = "FICHIER"
aNode.ExpandedImage = "Open"
aNode.Expanded = False

'-> Pointer sur la classe libell�
Set aLb = Libelles("MDIMAIN")

'-> Pointer sur le fichier
Set aFichier = Fichiers(UCase$(Trim(fileName)))

'-> Cr�er une icone par spool
'For Each aSpool In aFichier.Spools
'    '-> on regarde si on a specifi� un titre
'    If InStr(1, aSpool.GetMaq, "[TITRE]") <> 0 Then
'        IsTitrePage = True
'    Else
'        IsTitrePage = False
'    End If
'    '-> on regarde si on a specifi� un titre
'    If InStr(1, aSpool.GetMaq, "[MAIL]") <> 0 Then
'        IsTitreMail = True
'    Else
'        IsTitreMail = False
'    End If
'
'    Set aNode2 = MDIMain.TreeNaviga.Nodes.add(aNode.Key, 4, aNode.Key & "�" & aSpool.Key, aSpool.SpoolText, "Spool")
'    aNode2.Tag = "SPOOL"
'    aNode2.Expanded = True
'    '-> Soit la liste des erreurs
'    If aSpool.NbError = 0 Then
'        '-> Afficher une page pour la liste des s�lections utilisateur
'        If aSpool.IsSelectionPage Then
'            Set aNode3 = MDIMain.TreeNaviga.Nodes.add(aNode2.Key, 4, aNode2.Key & "�PAGE|0", aLb.GetCaption(12) & "S�lection", "Page")
'            aNode3.Tag = "PAGE"
'        End If
'
'        '-> Afficher une icone par page
'        For i = 1 To aSpool.NbPage
'            '-> Recuperer eventuellement le titre de la feuille
'            If IsTitrePage Then
'                Set aNode3 = MDIMain.TreeNaviga.Nodes.add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, GetTitrePage(aSpool, i), "Page")
'            Else
'                Set aNode3 = MDIMain.TreeNaviga.Nodes.add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, aLb.GetCaption(12) & i, "Page")
'            End If
'            aNode3.Tag = "PAGE"
'        Next
'    Else
'        '-> Afficher l'icone de la page d'erreur
'        Set aNode3 = MDIMain.TreeNaviga.Nodes.add(aNode2.Key, 4, aNode2.Key & "�ERROR", aLb.GetCaption(11), "Warning")
'        aNode3.Tag = "ERROR"
'    End If 'S'il y a des erreurs
'Next 'Pour tous les spools

'LoadJoinFile uZipInfo2, aSpool.FileName

'-> on affiche les notes de la page
'notesDisplay aSpool

GestError:
    '-> Bloquer l'interface
    MDIMain.Enabled = True
    Screen.MousePointer = 0


End Sub

Public Sub PrintPageSpool(ByRef aSpool As Spool, ByVal pageToPrint As Long)

'---> Cette proc�dure imprime une page d'un spool

Dim i As Integer, j As Integer
Dim PositionX As Long
Dim PositionY As Long
Dim IsPaysage As Boolean
Dim Ligne As String
Dim TypeObjet As String
Dim NomObjet As String
Dim TypeSousObjet As String
Dim NomSousObjet As String
Dim Param As String
Dim DataFields As String
Dim FirstObj As Boolean
Dim aNode As Node
Dim ErrorCode As Integer
Dim aSection As Section
Dim X As Control
Dim MargeOld As Integer
Dim isHTML As Boolean

On Error GoTo GestError
Trace "Debut PrintPageSpool --------------------" & pageToPrint & " --------------------", 1


topCXL = False
'-> on se recharge la feuille et la maquette pour le spool en cours
If aSpool.frmdisplay Is Nothing Then MDIMain.InitFRMDISPLAY aSpool
If aSpool.maquette Is Nothing Then InitCom aSpool

Trace "PrintPageSpool --------------------" & pageToPrint & " --------------------", 1

'-> Initialisation du p�riph�rique selon la nature de la sortie

If TypeOf Sortie Is PictureBox And Not GetProp Then
    LockWindowUpdate MDIMain.Hwnd
    ErrorCode = 35
    '-> Conserver la valeur du zoom
    Zoom = val(aSpool.frmdisplay.Combo1.Text) / 100
    'Zoom = 1
    '-> Vider les erreurs de page
    aSpool.InitErrorPage pageToPrint
    '-> Modifier la page courrante de l'objet Spool
    aSpool.CurrentPage = pageToPrint
    '-> Mode Visualisation �cran
    aSpool.InitDisplayPage
    '-> Positionner le pointeur de sortie vers le picturebox de la feuille
    Set Sortie = aSpool.frmdisplay.Page
    '-> Supprimer les labels d'acc�s au d�tail
    For Each X In aSpool.frmdisplay.Controls
        If UCase$(X.Name) = "LBLACCESDET" Then
            If X.Index <> 0 Then Unload aSpool.frmdisplay.lblAccesDet(X.Index)
        End If
    Next
    For Each X In aSpool.frmdisplay.Controls
        If UCase$(X.Name) = "LBLPICTURE" Then
            If X.Index <> 0 Then aSpool.frmdisplay.lblPicture(X.Index).Visible = False
        End If
    Next
    '-> supprimer les zones de saisies
    On Error Resume Next
    For Each X In aSpool.frmdisplay.Controls
        If UCase$(X.Name) = "LBLFORM" Then
            If X.Index <> 0 Then
                aSpool.frmdisplay.lblForm(X.Index).Visible = False
                Unload aSpool.frmdisplay.lblForm(X.Index)
            End If
        End If
    Next
    On Error GoTo GestError
    '-> Gestion de l'icone de la page s�lectionn�e
    If aSpool.IsSelectionPage Then
        j = 0
    Else
        j = 1
    End If
    If pageToPrint = 1 And aSpool.IsSelectionPage And aSpool.GetPage(pageToPrint) = "" Then
        pageToPrint = 0
    End If
    'For i = j To aSpool.NbPage
        '-> Tester si une page est en erreur ou non avant de modifier sa page
        On Error Resume Next
        If MDIMain.TreeNaviga.Nodes(UCase(Trim(aSpool.fileName)) & "�" & UCase$(Trim(aSpool.Key)) & "�PAGE|" & aSpool.CurrentPage).Tag <> "ERROR" Then
            'If i = pageToPrint Then
                MDIMain.TreeNaviga.Nodes(UCase(Trim(aSpool.fileName)) & "�" & UCase$(Trim(aSpool.Key)) & "�PAGE|" & aSpool.CurrentPage).Image = "PageSelected"
            'Else
                If (aSpool.LastPage <> 0 And aSpool.LastPage <> aSpool.CurrentPage) Or (aSpool.LastPage = 0 And aSpool.IsSelectionPage) Then
                    MDIMain.TreeNaviga.Nodes(UCase(Trim(aSpool.fileName)) & "�" & UCase$(Trim(aSpool.Key)) & "�PAGE|" & aSpool.LastPage).Image = "Page"
                End If
                aSpool.LastPage = aSpool.CurrentPage
                
            'End If
        End If
        On Error GoTo GestError
    'Next
    'LockWindowUpdate 0
    '-> Modifier l'affichage de la page en cours
    aSpool.frmdisplay.DisplayCurrentPage
    '-> Bloquer la feuille
    aSpool.frmdisplay.Enabled = False
    '-> Gestion de la temporisation
    TailleLue = 0
    TailleTotale = NumEntries(aSpool.GetPage(pageToPrint), Chr(0))
    '-> Bloquer l'interface
    MDIMain.Enabled = False
    Screen.MousePointer = 11
Else
    '-> Mode Impression direct ou batch sur une imprimante
End If


'-> Initialiser le RTF d'impression de la page des s�lections
If pageToPrint = 0 Then
    '-> Pointer sur la section texte
    Set aSection = aSpool.maquette.Sections(SelectRTFKey)
    '-> Vider le RTF associ�
    frmLib.Rtf(aSection.IdRTf).Text = ""
End If

'-> Sauvegarde de l'ancienne marge
If DebordementTop And Not TypeOf Sortie Is PictureBox Then
    MargeOld = MargeX - GetDeviceCaps(Sortie.hdc, PHYSICALOFFSETX)
End If

'-> R�cup�ration des marges internes du contexte de p�riph�rique
MargeX = GetDeviceCaps(Sortie.hdc, PHYSICALOFFSETX)
MargeY = GetDeviceCaps(Sortie.hdc, PHYSICALOFFSETY)

'-> Initialiser la position X , y du pointeur sur les marges du document
PositionY = -MargeY + Sortie.ScaleY(aSpool.maquette.MargeTop, 7, 3)
PositionX = -MargeX + Sortie.ScaleX(aSpool.maquette.MargeLeft, 7, 3)

PositionX = PositionX * Zoom
PositionY = PositionY * Zoom

'-> Gerer le debordement si le top de debordement est activer on refait l'impression avec une translation
If DebordementTop And Not TypeOf Sortie Is PictureBox Then
    MargeX = MargeX + Sortie.ScaleWidth + MargeOld
    '-> on reinitialise le top
    DebordementTop = False
End If

'-> Indiquer l'�tat du premier objet que l'on trouve
FirstObj = True

'-> Lecture des lignes de la page
For i = 1 To NumEntries(aSpool.GetPage(pageToPrint), Chr(0))
    Trace "boucle des lignes" & i, 1
    '-> Gestion de la temporisation
    If mode = 1 Then
        TailleLue = i
        If Rnd < 0.05 Then DrawWait
    End If
    '-> R�cup�ration de la ligne en cours
    Ligne = Trim(Entry(i, aSpool.GetPage(pageToPrint), Chr(0)))
    '-> Ne rien imprimer si page � blanc
    If Trim(Ligne) = "" Then GoTo NextLigne
    
    If GetProp Then tempLigProp = Ligne
    
    If pageToPrint = 0 Then
        frmLib.Rtf(aSection.IdRTf).Text = frmLib.Rtf(aSection.IdRTf).Text & Chr(13) & Chr(10) & Ligne
        frmLib.Rtf(aSection.IdRTf).SelStart = 0
        frmLib.Rtf(aSection.IdRTf).SelLength = Len(frmLib.Rtf(aSection.IdRTf).Text)
        frmLib.Rtf(aSection.IdRTf).SelFontName = "Lucida Console"
        If InStr(1, Ligne, "<HEADER>", vbTextCompare) <> 0 Then isHTML = True
    Else
        If InStr(1, Ligne, "[") = 1 Then
            '-> r�cup�ration des param�tres
            AnalyseObj Ligne, TypeObjet, NomObjet, TypeSousObjet, NomSousObjet, Param, DataFields
            '-> Selon le type d'objet
            If UCase$(TypeObjet) = "ST" Then
                If Not PrintSection(NomObjet, Param, DataFields, FirstObj, PositionX, PositionY, aSpool) Then Exit For
                FirstObj = False
            ElseIf UCase$(TypeObjet) = "TB" Then
                If Not PrintTableau(NomObjet, Param, DataFields, PositionX, PositionY, NomSousObjet, FirstObj, aSpool) Then Exit For
                FirstObj = False
            End If
        End If 'Si premier caract�re = "["
    End If 'Si on imprime la page de s�lection
NextLigne:
Next 'Pour toutes les lignes de la page

If Not listImage Is Nothing Then
    If listImage.Count <> 0 Then
        Dim aBmp As ImageObj
        Dim x1 As Long
        Dim y1 As Long
        Dim apic As Object
        Dim zoomFit As Double
        For Each aBmp In listImage
            x1 = aBmp.Left
            y1 = aBmp.Top
            If TypeOf Sortie Is PictureBox Then
                Set apic = aSpool.frmdisplay.lblPicture(aBmp.IdAffichage)
                zoomFit = aBmp.Largeur
                If Zoom * zoomFit <> 1 Then
                    If Zoom * zoomFit < 1 Then
                        '-> on redimensionne l'image
                        StretchBlt apic.hdc, 0, 0, apic.width * Zoom * zoomFit, apic.height * Zoom * zoomFit, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
                        StretchBlt Sortie.hdc, x1, y1, apic.ScaleWidth * Zoom * zoomFit, apic.ScaleHeight * Zoom * zoomFit, apic.hdc, 0, 0, apic.ScaleWidth * Zoom * zoomFit, apic.ScaleHeight * Zoom * zoomFit, vbSrcCopy
                    Else
                        '-> on redimensionne l'image
                        apic.width = apic.width * Zoom * zoomFit
                        apic.height = apic.height * Zoom * zoomFit
                        StretchBlt apic.hdc, 0, 0, apic.width * Zoom * zoomFit, apic.height * Zoom * zoomFit, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
                        '-> on ne conserve que la partie interessante
                        StretchBlt Sortie.hdc, x1, y1, apic.ScaleWidth, apic.ScaleHeight * Zoom * zoomFit, apic.hdc, 0, 0, apic.ScaleWidth, apic.ScaleHeight * Zoom * zoomFit, vbSrcCopy
                    End If
                    Sortie.Refresh
                Else
                    StretchBlt apic.hdc, 0, 0, apic.width * Zoom * zoomFit, apic.height * Zoom * zoomFit, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
                    StretchBlt Sortie.hdc, x1, y1, apic.ScaleWidth, apic.ScaleHeight * Zoom * zoomFit, apic.hdc, 0, 0, apic.ScaleWidth, apic.ScaleHeight * Zoom * zoomFit, vbSrcCopy
                    Sortie.Refresh
                End If
            Else
                Set apic = frmLib.PicObj(0)
                'aPic.AutoSize = 1
                apic.Picture = LoadPicture(aBmp.Nom)
                zoomFit = aBmp.Largeur
                If Zoom * zoomFit < 1 Then
                    apic.width = apic.width * zoomFit
                    apic.height = apic.height * zoomFit
                    Trace "drawcell image " & apic.width & " " & apic.height
                    Sortie.PaintPicture apic.Picture, x1, y1, Sortie.ScaleX(apic.width, 1, 3), Sortie.ScaleY(apic.height, 1, 3)
                Else
                    apic.width = apic.width * zoomFit
                    apic.height = apic.height * zoomFit
                    Trace "drawcell image " & apic.width & " " & apic.height
                    Sortie.PaintPicture apic.Picture, x1, y1, Sortie.ScaleX(apic.width, 1, 3), Sortie.ScaleY(apic.height, 1, 3)
                End If
            End If
        Next
    End If
    Set listImage = Nothing
End If

'-> Tester si le bas de page est d�pass�
If Sortie.ScaleY(PositionY, 3, 7) > aSpool.maquette.Hauteur * Zoom Then
    If Not noDepass Then Sortie.PaintPicture frmLib.pibPage.Picture, Sortie.ScaleX(0.5, 7, 3), Sortie.ScaleY(aSpool.maquette.Hauteur, 7, 3) - Sortie.ScaleY(frmLib.pibPage.height, 1, 3) - Sortie.ScaleY(1, 7, 3)
End If 'Si on a d�pass� le bas de page

'-> Imprimer la page de s�lection
If pageToPrint = 0 Then
    On Error GoTo suite
    If Printer.Orientation <> 1 Then Printer.Orientation = 1
suite:
    If isHTML = False Then
        '-> imprimer la section
        PrintObjRtf 0, 0, aSection, "", 0, 0, aSpool
    Else
        '-> on se formate la page de garde si besoin
        If aSpool.GardeRTF = "" Then
            parseGarde frmLib.Rtf(aSection.IdRTf).Text, aSection.IdRTf
            aSpool.GardeRTF = frmLib.Rtf(aSection.IdRTf).TextRTF
        Else
            frmLib.Rtf(aSection.IdRTf).TextRTF = aSpool.GardeRTF
        End If
        '-> imprimer la section
        PrintObjRtf 0, 0, aSection, "", 0, 0, aSpool
    End If
End If

'-> R�afficher la page si on est en mode Visu
If TypeOf Sortie Is PictureBox Then
    '-> D�bloquer la feuille
    aSpool.frmdisplay.Enabled = True
End If

'-> Gestion de la temporisation
If mode = 1 Then
    MDIMain.StatusBar1.Refresh
    '-> Bloquer l'interface
    MDIMain.Enabled = True
    Screen.MousePointer = 0
End If

LoadJoinFile uZipInfo2, aSpool.fileName

'-> on affiche les notes de la page
notesDisplay aSpool

'-> on affiche les images de premier plan
LockWindowUpdate 0

Exit Sub
       
GestError:
    LockWindowUpdate 0
    MsgBox Err.Number & " " & Err.Description
    '-> Gestion de la temporisation
    If mode = 1 Then
        MDIMain.StatusBar1.Refresh
        '-> Bloquer l'interface
        MDIMain.Enabled = True
        Screen.MousePointer = 0
    End If

    '-> Setting d'une erreur
    Call GestError(ErrorCode, CStr(pageToPrint), aSpool)
    
    

End Sub

Private Function parseGarde(strGarde As String, RtfId As String)
'--> cette fonction va permettre de parser le texte de garde issu des batchs en quelque chose de lisible
Dim i As Integer
Dim expgarde
Dim strLigne As String
Dim strValue As String
Dim strField As String
Dim strLastField As String
Dim dogIdent As String
Dim dogLangue As String
Dim typSel As String
Dim strTemp As String
Dim strGroup As String
Dim strVue As String
Dim strNomspool As String
Dim cptField As Integer

'If frmLib.Rtf(RtfId).text <> "" Then Exit Function
frmLib.Rtf(RtfId).Text = ""
expgarde = Split(strGarde, Chr(10))

For i = 1 To UBound(expgarde)
    If cptField > 300 Then Exit For
    strLigne = expgarde(i)
    strLigne = Replace(strLigne, Chr(9), "")
    strLigne = Replace(strLigne, Chr(13), "")
    strLigne = Replace(strLigne, "\", "/")
    
    Select Case UCase(Entry(1, strLigne, ">"))
        Case "<DOG-IDENT/":
            dogIdent = Trim(Entry(2, strLigne, ">"))
        Case "<DOG-LANGUE/":
            dogLangue = Trim(Entry(2, strLigne, ">"))
            If dogLangue <> "" Then
                If Len(dogLangue) = 1 Then
                    dogLangue = "0" & dogLangue
                End If
            End If
        Case "<BATDATE/":
            strTemp = strTemp & "Cr�� le : " & Entry(2, strLigne, ">")
        Case "<DOG-OPERAT/":
            strTemp = strTemp & "           par : " & Entry(2, strLigne, ">") & Chr(13) & Chr(10)
        Case "<LIBELVUE/":   '->libell� programme
            strVue = Entry(2, strLigne, ">")
        Case "<NOMSPOOL/":
            strNomspool = Entry(2, strLigne, ">")
        Case "<LIBPROG/":   '->libell� programme
            If strVue <> "" Then
                strVue = Entry(2, strLigne, ">") & "      avec la vue '" & strVue & "'"
            Else
                strVue = Entry(2, strLigne, ">")
            End If
            frmLib.Rtf(RtfId).SelBold = True
            frmLib.Rtf(RtfId).SelFontSize = 10
            frmLib.Rtf(RtfId).SelText = strVue
            frmLib.Rtf(RtfId).SelBold = False
            frmLib.Rtf(RtfId).SelColor = vbBlue
            frmLib.Rtf(RtfId).SelFontSize = 8.75
            frmLib.Rtf(RtfId).SelText = Chr(13) & Chr(10) & "______________________________________________________________________________________________________"
            frmLib.Rtf(RtfId).SelText = Chr(13) & Chr(10) & Chr(13) & Chr(10) & strNomspool & Chr(13) & Chr(10) & strTemp & Chr(13) & Chr(10)
            
            'on se commence un tableau
            frmLib.Rtf(RtfId).TextRTF = Left(frmLib.Rtf(RtfId).TextRTF, Len(frmLib.Rtf(RtfId).TextRTF) - 3) & " \trowd\ \trgaph20 \cellx3250 \intbl\cellx10800 \intbl \b Libell\'e9\b0 \cell \b Valeur                               Ordre   Saut      \b0 \cell\row" & "}"
        Case "<KEYWORD/":   '->champ
            '-> on va essayer de lire le libell� du champ
            strField = Entry(2, strLigne, ">")
            If Printer.Orientation = 1 Then
                If cptField > 48 Then strValue = "..."
            Else
                If cptField > 80 Then strValue = "..."
            End If
            If Entry(1, strField, "_") <> Entry(1, strLastField, "_") And strValue <> "..." Then
                strValue = "" & lectbin(strField, dogIdent, dogLangue, "")
            Else
                strValue = ""
            End If
            strLastField = strField
            typSel = ""
        Case "<DOG-PROGICIEL/":
            strTemp = strTemp & "Progiciel :" & Entry(2, strLigne, ">") & Chr(13) & Chr(10) & Chr(13) & Chr(10)
        Case "<DOG-CODSOC/":
            strTemp = strTemp & Space(20) & " soci�t�/�tablissement : " & Entry(2, strLigne, ">")
        Case "<DOG-CODETB/":
            strTemp = strTemp & "/" & Entry(2, strLigne, ">")
        Case "<TYPSEL/":
            typSel = Entry(2, strLigne, ">")
            Select Case typSel
                Case "":
                    strValue = Left(strValue & Space(30), 30)
                Case "EQ":
                    strValue = Left(strValue & Space(30), 30) & " �gale � : "
                Case "INC":
                    strValue = Left(strValue & Space(30), 30) & " compris entre : "
                Case "DIF":
                    strValue = Left(strValue & Space(30), 30) & " diff�rent de : "
                Case "NIN":
                    strValue = Left(strValue & Space(30), 30) & " non inclus dans : "
            End Select
        Case "<VALMIN/":
            If Printer.Orientation = 1 Then
                If cptField > 48 Then strValue = "..."
            Else
                If cptField > 80 Then strValue = "..."
            End If
            If strGroup = "COMBO" And strValue <> "..." Then
                strValue = strValue & lectbin(strField, dogIdent, dogLangue, Entry(2, strLigne, ">"))
            End If
            strValue = strValue & Entry(2, strLigne, ">")
            strValue = Replace(strValue, ",,", "")
            strValue = Replace(strValue, "\", "\\")
            If Len(strValue) > 100 Then
                strValue = Mid(strValue, 1, 100) & "..."
            End If
        Case "<GROUPE/":
            strGroup = UCase(Entry(2, strLigne, ">"))
        Case "<VALMAX/":
            If typSel <> "EQ" Then strValue = strValue & " / " & Replace(Entry(2, strLigne, ">"), "\", "\\")
        Case "<ORDER/":
            If Entry(2, strLigne, ">") <> "0" Then
                strValue = Left(Left(strValue & Space(68), 69) & Space(7) & Entry(2, strLigne, ">") & Space(50), 85)
            Else
                strValue = Left(Left(strValue & Space(68), 69) & Space(50), 85)
            End If
        Case "<SAUT/":
            strValue = Left(strValue & Space(80), 81)
            If Entry(2, strLigne, ">") <> "0" Then
                strValue = Left(strValue & Space(4) & "X" & Space(20), 86)
            Else
                strValue = Left(strValue & Space(20), 86)
            End If
        Case "<EDITABLE/":
            strValue = Left(strValue & Space(90), 86)
            If Entry(2, strLigne, ">") <> "0" Then
                strValue = Left(strValue & Space(5) & "X" & Space(7), 92)
            Else
                strValue = Left(strValue & Space(20), 92)
            End If
        Case "</FIELD":
            cptField = cptField + 1
            If frmLib.Rtf(RtfId).SelColor = vbBlue Then
                frmLib.Rtf(RtfId).SelColor = vbBlack
            Else
                frmLib.Rtf(RtfId).SelColor = vbBlue
            End If
            Dim isB As Boolean
            If isB = False Then
                frmLib.Rtf(RtfId).TextRTF = Left(frmLib.Rtf(RtfId).TextRTF, Len(frmLib.Rtf(RtfId).TextRTF) - 27) & "\cf0\intbl " & Mid(strValue, 1, 27) & "  \cell " & Mid(strValue, 32) & "\cell \row " & "}"
                isB = True
            Else
                frmLib.Rtf(RtfId).TextRTF = Left(frmLib.Rtf(RtfId).TextRTF, Len(frmLib.Rtf(RtfId).TextRTF) - 27) & "\cf1\intbl " & Mid(strValue, 1, 27) & "  \cell " & Mid(strValue, 32) & "\cell \row " & "}"
                isB = False
            End If
            If Printer.Orientation = 1 Then
                If cptField > 40 Then i = 1000
            Else
                If cptField > 81 Then i = 1000
            End If
    End Select
Next

End Function

Private Function lectbin(strField As String, strIdent As String, strLangue As String, strValue As String) As String
'-> cette fonction va essayer de trouver sur le site la definition du bin
Dim Ligne As String
Dim hdlFile As Integer
Dim topOK As Boolean

If TurboGraphWebFile = "" Then
    lectbin = strField
    Exit Function
End If

If strLangue = "" Then strLangue = "01"
strField = Trim(strField)
'-> le spe client eventuel
If strIdent <> "DEAL" Then
    If TelechargeFile(TurboGraphWebFile & "/sidata/" & strIdent & "/label/" & strLangue & "/" & LCase(strField) & ".bin", GetTempFileNameVB("", True) & LCase(strField) & ".bin") Then topOK = True
End If
'-> on cherche dans le standard
If topOK = False Then
    If TelechargeFile(TurboGraphWebFile & "/sidata/deal/label/" & strLangue & "/" & LCase(strField) & ".bin", GetTempFileNameVB("", True) & LCase(strField) & ".bin") Then topOK = True
End If

If topOK = False Then
    lectbin = strField
    Exit Function
End If

'-> get d'un handle de fichier
hdlFile = FreeFile

'-> on lit le fichier
Open GetTempFileNameVB("", True) & LCase(strField) & ".bin" For Input As #hdlFile
'-> Lecture s�quentielle du buffer
topOK = False
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    Ligne = Replace(Ligne, Chr(9), "")
    Ligne = Replace(Ligne, Chr(13), "")
    Ligne = Replace(Ligne, Chr(10), "")
    
    '-> Selon la ligne
    Select Case Entry(1, UCase$(Trim(Entry(1, Ligne, ">"))), "_")
        Case "<SIDE-LABEL/"
            lectbin = Trim(Entry(2, Ligne, ">"))
            If strValue = "" Then Exit Function
            If topOK Then Exit Function
        Case "<RETURN-VALUE/"
            '-> Enregistrer son libelle court
            If strValue = Trim(Entry(2, Ligne, ">")) Then
                Exit Function
            End If
        Case "<LABEL"
            If Entry(2, UCase$(Trim(Entry(1, Ligne, "/>"))), "_") = strValue And strValue <> "" Then topOK = True
    End Select
Loop 'Pour tout le buffer
'-> Fermer le fichier binaire
Close #hdlFile

End Function

Private Function getFieldGarde(sField As String, sKey As String) As String
'--> cette fonction va permettre de recuperer le texte de garde issu des batchs
Dim i As Integer
Dim expgarde
Dim strLigne As String
Dim strValue As String
Dim strField As String
Dim strLastField As String
Dim dogIdent As String
Dim dogLangue As String
Dim typSel As String
Dim strTemp As String
Dim strGroup As String
Dim strVue As String
Dim strNomspool As String
Dim topField As Boolean

getFieldGarde = ""
expgarde = Split(strGarde, Chr(0))

For i = 1 To UBound(expgarde)
    strLigne = expgarde(i)
    strLigne = Replace(strLigne, Chr(9), "")
    strLigne = Replace(strLigne, Chr(13), "")
    Select Case Trim(UCase(Entry(1, sField, "=")))
        Case "KEYWORD":
            'si on est sur la bonne section on cherche la valeur
            If topField Then
                If UCase(Entry(1, strLigne, ">")) = UCase("<" & sKey & "/") Then
                    getFieldGarde = Trim(Entry(2, strLigne, ">"))
                    GoTo GestError
                End If
            End If
            'on regarde si on est sur la bonne section
            If UCase(Entry(1, strLigne, ">")) = "<KEYWORD/" Then
                If Trim(UCase(Entry(2, sField, "="))) = Trim(Entry(2, Trim(UCase(strLigne)), ">")) Then
                    topField = True
                Else
                    topField = False
                End If
            End If
        Case Else:
            If UCase(Entry(1, strLigne, ">")) = "<" & Trim(UCase(sField)) & "/" Then
                getFieldGarde = Trim(Entry(2, strLigne, ">"))
                GoTo GestError
            End If
    End Select
        
Next
GestError:
End Function

Private Function PrintBlock(ByRef NomSousObjet As String, ByRef Param As String, _
                        ByRef DataFields As String, ByVal PosX As Long, _
                        ByRef PosY As Long, ByRef aTb As Tableau, ByVal Ligne As Integer, ByRef aSpool As Spool) As Long

'---> Cette Proc�dure imprime un block de ligne

Dim aBlock As Block
Dim Champ As String
Dim aField() As String
Dim i As Long
Dim aCell As Cellule
Dim HauteurLigPix As Long
Dim ListeField() As String
Dim Tempo
Dim strtempo As String
Dim topColor As Boolean
Dim topBold As Boolean
Dim topItalic As Boolean
Dim topUnderline As Boolean
Dim topSize As Integer
Dim topName As String
Dim apic As PictureBox
Dim TempFile As String

On Error GoTo GestBlock

'-> Pointer sur le block de tableau � �diter
Set aBlock = aTb.blocks("BL-" & UCase$(NomSousObjet))

'-> Initaliser le block
InitBlock aBlock, aSpool

'-> Calculer la modification des positions X et Y des blocks de ligne. _
Les coordonn�es de chaque cellule �tant calcul�e sur un X et Y = 0 de base

Select Case aBlock.AlignementLeft
    Case 2 '-> Marge gauche
        PosX = Sortie.ScaleX(aSpool.maquette.MargeLeft * Zoom, 7, 3)
    Case 3 '-> Centr�
        PosX = Sortie.ScaleX((aSpool.maquette.Largeur * Zoom - aBlock.Largeur * Zoom) / 2, 7, 3)
    Case 4 '-> Marge droite
        PosX = Sortie.ScaleX(aSpool.maquette.Largeur * Zoom - aBlock.Largeur * Zoom - aSpool.maquette.MargeLeft * Zoom, 7, 3)
    Case 5 '-> Sp�cifi�
        PosX = Sortie.ScaleX(aBlock.Left * Zoom, 7, 3)
End Select

'-> Tenir compte de la marge interne
PosX = (PosX - MargeX)

If aBlock.AlignementTop = 5 And Ligne <> 1 Then
Else
    Select Case aBlock.AlignementTop
        Case 1 '-> Libre
            '-> RAS le pointeur est bien positionn�
        Case 2 '-> Marge haut
            PosY = Sortie.ScaleY(aSpool.maquette.MargeTop * Zoom, 7, 3)
        Case 3 '-> Centr�
            PosY = Sortie.ScaleY((aSpool.maquette.Hauteur * Zoom - aBlock.Hauteur * Zoom) / 2, 7, 3)
        Case 4 '-> Marge Bas
            PosY = Sortie.ScaleY(aSpool.maquette.Hauteur * Zoom - aBlock.Hauteur * Zoom, 7, 3)
        Case 5 '-> Sp�cifi�
            PosY = Sortie.ScaleY(aBlock.Top * Zoom, 7, 3)
    End Select

    '-> Modifier l'alignement
    If aBlock.AlignementTop <> 1 Then PosY = PosY - MargeY

End If

'-> Impression du block de ligne
For i = 1 To aBlock.NbCol
    '-> Pointer sur la cellule � dessiner
    Set aCell = aBlock.Cellules("L" & Ligne & "C" & i)
    '-> Remplacer les champs par leur valeur
    If aCell.FontName = "Code 128" Then
        aCell.ReplaceField DataFields, True
    Else
        aCell.ReplaceField DataFields
    End If
    If InStr(1, aCell.ContenuCell, "<") <> 0 Then
        '-> on regarde si on a un lien hypertext
        If InStr(1, aCell.ContenuCell, "<url:", vbTextCompare) <> 0 Then
            aCell.UseHyperText = True
            aCell.KeyLink = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<url:", vbTextCompare))
            aCell.KeyLink = Mid(aCell.KeyLink, 1, InStr(1, aCell.KeyLink, ">"))
            aCell.ContenuCell = Replace(aCell.ContenuCell, aCell.KeyLink, "")
            aCell.KeyLink = Trim(Mid(aCell.KeyLink, 6, Len(aCell.KeyLink) - 6))
        End If
        '-> on regarde si on a pas une balise form
        If InStr(1, aCell.ContenuCell, "<frm:", vbTextCompare) <> 0 Then
            aCell.UseFormText = NumEntries(" " & aCell.ContenuCell & " ", "<frm:") - 1
            'aCell.KeyForm = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<frm:", vbTextCompare))
            aCell.keyFormIndex = val(Replace(Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<frm:") + 5, 2), ":", ""))
            
            Dim ii As Integer
            Dim keyFormIndex As String
            For ii = 1 To CInt(aCell.UseFormText)
                keyFormIndex = Replace(Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<frm:") + 5, 2), ":", "")
                If InStr(1, aCell.ContenuCell, "<frm:" & keyFormIndex & ":") <> 0 Then
                    aCell.KeyForm = Replace(aCell.ContenuCell, "<frm:" & keyFormIndex & ":", "", 1, 1)
                Else
                    aCell.KeyForm = Replace(aCell.ContenuCell, "<frm:", "", 1, 1)
                End If
                aCell.KeyForm = Replace(aCell.KeyForm, ">", "", 1, 1)
                'aCell.ContenuCell = Replace(aCell.ContenuCell, aCell.KeyForm, "")
                'aCell.KeyForm = Trim(Mid(aCell.KeyForm, 6, Len(aCell.KeyForm) - 6))
                aCell.ContenuCell = aCell.KeyForm
                'If aCell.ContenuCell <> "" And aCell.KeyForm = "" Then aCell.KeyForm = aCell.ContenuCell
            
            Next
        End If
        '-> on regarde si on doit tout passer en majusules
        If InStr(1, aCell.ContenuCell, "<upc:", vbTextCompare) <> 0 Then
            strtempo = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<upc:", vbTextCompare))
            strtempo = Mid(strtempo, 1, InStr(1, strtempo, ">"))
            aCell.ContenuCell = Replace(aCell.ContenuCell, strtempo, UCase(Mid(strtempo, 6, Len(strtempo) - 6)))
        End If
        '-> on regarde si on doit tout passer en minuscules
        If InStr(1, aCell.ContenuCell, "<upl:", vbTextCompare) <> 0 Then
            strtempo = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<upl:", vbTextCompare))
            strtempo = Mid(strtempo, 1, InStr(1, strtempo, ">"))
            aCell.ContenuCell = Replace(aCell.ContenuCell, strtempo, LCase(Mid(strtempo, 6, Len(strtempo) - 6)))
        End If
        '-> on regarde si on doit tout passer en italique
        If InStr(1, aCell.ContenuCell, "<sti:>", vbTextCompare) <> 0 Then
            aCell.ContenuCell = Replace(aCell.ContenuCell, "<sti:>", "")
            aCell.FontItalic = True
            If aCell.FontItalic = False Then topItalic = True
        End If
        '-> on regarde si on doit tout passer en gras
        If InStr(1, aCell.ContenuCell, "<stb:>", vbTextCompare) <> 0 Then
            aCell.ContenuCell = Replace(aCell.ContenuCell, "<stb:>", "")
            aCell.FontBold = True
            If aCell.FontBold = False Then topBold = True
        End If
        '-> on regarde si on doit tout passer en soulign�
        If InStr(1, aCell.ContenuCell, "<stu:>", vbTextCompare) <> 0 Then
            aCell.ContenuCell = Replace(aCell.ContenuCell, "<stu:>", "")
            aCell.FontUnderline = True
            If aCell.FontUnderline = False Then topUnderline = True
        End If
        '-> on regarde si on doit appliquer une couleur particuliere style color = stc
        If InStr(1, aCell.ContenuCell, "<stc:", vbTextCompare) <> 0 Then
            strtempo = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<stc:", vbTextCompare))
            aCell.ContenuCell = Replace(aCell.ContenuCell, strtempo, "")
            strtempo = Mid(strtempo, 1, InStr(1, strtempo, ">"))
            strtempo = Mid(strtempo, 6, Len(strtempo) - 6)
            aCell.BackColor = val(strtempo)
            topColor = True
        End If
        '-> on regarde si on doit afficher le total des pages
        If InStr(1, aCell.ContenuCell, "<tot:>", vbTextCompare) <> 0 Then
            aCell.ContenuCell = Replace(aCell.ContenuCell, "<tot:>", aSpool.nbPage)
        End If
        '-> on regarde si on a pass� une image
        If InStr(1, aCell.ContenuCell, "<img:", vbTextCompare) <> 0 Then
            aCell.Image = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<img:", vbTextCompare))
            If InStr(1, aCell.Image, ">") <> 0 Then
                aCell.Image = Mid(aCell.Image, 1, InStr(1, aCell.Image, ">"))
            Else
                aCell.Image = aCell.Image & ">"
            End If
            aCell.ContenuCell = Replace(aCell.ContenuCell, aCell.Image, "")
            aCell.Image = Trim(Mid(aCell.Image, 6, Len(aCell.Image) - 6))
        End If
        '-> on regarde si on a pass� une image
        If InStr(1, aCell.ContenuCell, "<qrcode:", vbTextCompare) <> 0 Then
            aCell.Image = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<qrcode:", vbTextCompare))
            strtempo = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<qrcode:", vbTextCompare))
            strtempo = Mid(strtempo, 1, InStr(1, strtempo, ">"))
            aCell.ContenuCell = Replace(aCell.ContenuCell, strtempo, "")
            
            strtempo = Mid(strtempo, 9, Len(strtempo) - 9)
            
            '-> on va creer l'image correspondant au qrcode
            Dim qr As QRCode
            
            Set apic = frmLib.PicObj(0)
            apic.AutoRedraw = True
            apic.AutoSize = True
            apic.height = apic.width
            Set qr = New QRCode
            qr.setErrorCorrectLevel QR_ERROR_CORRECT_LEVEL_L
            qr.setTypeNumber 3
            qr.addData strtempo, 4
            Call qr.make
            Call qr.printQR(apic, 2)
            TempFile = GetTempFileNameVB("bmp")
            SavePicture apic.Image, TempFile
            aCell.Image = TempFile
        End If
        
        '-> on regarde si on a pass� une image
        If InStr(1, aCell.ContenuCell, "<datamatrix:", vbTextCompare) <> 0 Then
            aCell.Image = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<datamatrix:", vbTextCompare))
            strtempo = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<datamatrix:", vbTextCompare))
            strtempo = Mid(strtempo, 1, InStr(1, strtempo, ">"))
            aCell.ContenuCell = Replace(aCell.ContenuCell, strtempo, "")
           
            strtempo = Mid(strtempo, 13, Len(strtempo) - 13)
            
            '-> on va creer l'image correspondant au qrcode
            Set apic = frmLib.PicObj(0)
            apic.AutoRedraw = True
            apic.AutoSize = True
            apic.height = apic.width
            Dim apic2 As PictureBox
            Set apic2 = DrawDataMatrix(apic, strtempo, 4800, 4800)
            TempFile = GetTempFileNameVB("bmp")
            SavePicture apic2.Image, TempFile
            aCell.Image = TempFile
        End If
        
        '-> on regarde si on a pass� une hauteur image
        If InStr(1, aCell.ContenuCell, "<imh:", vbTextCompare) <> 0 Then
            aCell.imageHeight = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<imh:", vbTextCompare))
            If InStr(1, aCell.imageHeight, ">") <> 0 Then
                aCell.imageHeight = Mid(aCell.imageHeight, 1, InStr(1, aCell.imageHeight, ">"))
            Else
                aCell.imageHeight = aCell.imageHeight & ">"
            End If
            aCell.ContenuCell = Replace(aCell.ContenuCell, aCell.imageHeight, "")
            aCell.imageHeight = Trim(Mid(aCell.imageHeight, 6, Len(aCell.imageHeight) - 6))
        End If
        '-> on regarde si on a pass� une largeur image
        If InStr(1, aCell.ContenuCell, "<imw:", vbTextCompare) <> 0 Then
            aCell.imageWidth = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<imw:", vbTextCompare))
            If InStr(1, aCell.imageWidth, ">") <> 0 Then
                aCell.imageWidth = Mid(aCell.imageWidth, 1, InStr(1, aCell.imageWidth, ">"))
            Else
                aCell.imageWidth = aCell.imageWidth & ">"
            End If
            aCell.ContenuCell = Replace(aCell.ContenuCell, aCell.imageWidth, "")
            aCell.imageWidth = Trim(Mid(aCell.imageWidth, 6, Len(aCell.imageWidth) - 6))
        End If
        '-> on regarde si on a pass� un fichier
        If InStr(1, aCell.ContenuCell, "<fic:", vbTextCompare) <> 0 Then
            aCell.Fichier = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<fic:", vbTextCompare))
            If InStr(1, aCell.Fichier, ">") <> 0 Then
                aCell.Fichier = Mid(aCell.Fichier, 1, InStr(1, aCell.Fichier, ">"))
            Else
                aCell.Fichier = aCell.Fichier & ">"
            End If
            aCell.ContenuCell = Replace(aCell.ContenuCell, aCell.Fichier, "")
            aCell.Fichier = Trim(Mid(aCell.Fichier, 6, Len(aCell.Fichier) - 6))
        End If
        '-> on regarde si on doit afficher des informations de la page de garde
        If InStr(1, aCell.ContenuCell, "<gar:", vbTextCompare) <> 0 Then
            Dim strGar As String
            Dim sField As String
            Dim sKey As String
            Dim j As Integer
            For j = 1 To NumEntries(aCell.ContenuCell, "<gar:") - 1
                strGar = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<gar:", vbTextCompare))
                If InStr(1, strGar, ">") <> 0 Then
                    strGar = Mid(strGar, 1, InStr(1, strGar, ">"))
                Else
                    strGar = strGar & ">"
                End If
                sField = Entry(1, Replace(Replace(strGar, "<gar:", ""), ">", ""), "�")
                sKey = Entry(2, Replace(Replace(strGar, "<gar:", ""), ">", ""), "�")
                If Entry(3, Replace(Replace(strGar, "<gar:", ""), ">", "") + "��", "�") <> "" Then
                    aCell.ContenuCell = Replace(aCell.ContenuCell, strGar, Mid(getFieldGarde(sField, sKey), val(Entry(3, Replace(Replace(strGar, "<gar:", ""), ">", ""), "�"))))
                Else
                    aCell.ContenuCell = Replace(aCell.ContenuCell, strGar, getFieldGarde(sField, sKey))
                End If
            Next
        End If
        '-> on regarde si on doit nommer d'une maniere sp�ciale la page excel
        If InStr(1, aCell.ContenuCell, "<pex:>", vbTextCompare) <> 0 Then
            aCell.ContenuCell = Replace(aCell.ContenuCell, "<pex:>", "")
        End If
        '-> on regarde si on a pass� une image
        If InStr(1, aCell.ContenuCell, "<cxl:", vbTextCompare) <> 0 Then
            topCXL = True
            aCell.CXL = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<cxl:", vbTextCompare))
            If InStr(1, aCell.CXL, ">") <> 0 Then
                aCell.CXL = Mid(aCell.CXL, 1, InStr(1, aCell.CXL, ">"))
            Else
                aCell.CXL = aCell.CXL & ">"
            End If
            aCell.ContenuCell = Replace(aCell.ContenuCell, aCell.CXL, "")
            aCell.CXL = Trim(Mid(aCell.CXL, 6, Len(aCell.CXL) - 6))
        End If
        If InStr(1, aCell.ContenuCell, "<cxf:", vbTextCompare) <> 0 Then
            strtempo = Mid(aCell.ContenuCell, InStr(1, aCell.ContenuCell, "<cxf:", vbTextCompare))
            If InStr(1, strtempo, ">") <> 0 Then
                strtempo = Mid(strtempo, 1, InStr(1, strtempo, ">"))
            Else
                strtempo = strtempo & ">"
            End If
            aCell.ContenuCell = Replace(aCell.ContenuCell, strtempo, "")
        End If
    End If
    '-> Imprimer la cellule
    If Not DrawCell(aCell, PosX, PosY, aSpool, aTb.Nom, NomSousObjet) Then GoTo GestBlock
    If topColor Then aCell.BackColor = 16777215
    If topBold Then aCell.FontBold = False
    If topItalic Then aCell.FontItalic = False
    If topUnderline Then aCell.FontUnderline = False
    '-> R�cup�rer la hauteur de la ligne
    HauteurLigPix = (aCell.y2 - aCell.y1) * Zoom
Next

'-> forcer le rafraichissement
If TypeOf Sortie Is PictureBox Then Sortie.Refresh

If Ligne = aBlock.NbLigne Then PosY = PosY + 1 * Zoom
PrintBlock = PosY + HauteurLigPix

Exit Function

GestBlock:
    
    Call GestError(40, aTb.Nom & "|" & NomSousObjet, aSpool)
    PrintBlock = -9999
    
End Function

Private Function IsGoodFont(ByVal MyFont As String) As String

'---> Cette fonction d�termine si une font est valide
On Error GoTo GestError

frmLib.FontName = MyFont
IsGoodFont = MyFont

Exit Function

GestError:

    IsGoodFont = "Arial"
    

End Function


Public Function DrawCell(ByRef aCell As Cellule, ByVal PosX As Long, ByVal PosY As Long, ByRef aSpool As Spool, ByRef NomTb As String, Optional NomBlock As String) As Boolean

'---> Fonction qui dessine une cellule

Dim hdlPen As Long
Dim hdlBrush As Long
Dim hdlBordure As Long
Dim hdlFont As Long
Dim OldPen As Long
Dim oldBrush As Long
Dim oldFont As Long
Dim aPoint As POINTAPI
Dim aRect As RECT
Dim Res As Long
Dim aFt As FormatedCell
Dim xTemp
Dim PrintSigne As Boolean
Dim Couleur As Long
Dim aLbl As label
Dim aTb As Tableau
Dim aBlock As Block
Dim aCellContenuTemp As String
Dim FindTextTemp As String
Dim i As Integer
Dim apic As Object
Dim topImage As Boolean

On Error GoTo GestErr

Dim x1 As Long, x2 As Long, y1 As Long, y2 As Long

x1 = aCell.x1 * Zoom + PosX
x2 = aCell.x2 * Zoom + PosX
y1 = PosY
y2 = (aCell.y2 * Zoom) + PosY - (aCell.y1 * Zoom)

If GetProp Then
    If hProp > x1 And hProp < x2 And vProp > y1 And vProp < y2 And aCell.ContenuCell <> "" Then
        strProp = aCell.Contenu & Chr(0) & NomTb & Chr(0) & NomBlock
        strProp = strProp & Chr(0) & aCell.ContenuCell
        If aCell.Msk <> "0" Then strProp = strProp & Chr(13) & Chr(10) & "Msk : " & aCell.Msk
        curLigProp = tempLigProp
    End If
End If

'-> Cr�ation des objets GDI pour dessin des cellules
hdlBrush = CreateSolidBrush(aCell.BackColor)
hdlPen = CreatePen(PS_NULL, 1, QBColor(15))
hdlBordure = CreatePen(PS_SOLID, 1, &HE0E0E0)
'-> S�lection du pinceau dans le Contexte
oldBrush = SelectObject(Sortie.hdc, hdlBrush)

'-> Dessin de la cellule
OldPen = SelectObject(Sortie.hdc, hdlPen)
If Not aCell.FondTransparent Then Res = Rectangle(Sortie.hdc, x1, y1, x2, y2)

'-> Setting du Rect de la cellule
aRect.Left = x1
aRect.Top = y1
aRect.Right = x2 - 1
aRect.Bottom = y2 - 1


Dim pDrawText As Long
Dim Rect2 As RECT
Dim DifL As Long
Dim DifH As Long
Dim IsCellAjust As Boolean
Dim hdlRgn As Long
Dim AlignBase(1 To 2, 3 To 6) As Long
Dim strTemp As String
Dim acellcontenu As String

'-> Appliquer les options de font
Sortie.FontName = IsGoodFont(aCell.FontName)
Sortie.FontSize = aCell.FontSize * Zoom
Sortie.FontBold = aCell.FontBold
Sortie.FontItalic = aCell.FontItalic
Sortie.FontUnderline = aCell.FontUnderline
Sortie.foreColor = 1000 'on fait ceci car sinon bug si couleur precedente differente du noir
Sortie.foreColor = aCell.FontColor

'***********************************
'* Dessin du contenu de la cellule *
'***********************************
    
'-> R�cup�rer l'option de d'alignement interne
IsCellAjust = aCell.AutoAjust
    
'-> Faire une copie de Rect
Rect2.Left = aRect.Left
Rect2.Right = aRect.Right
Rect2.Top = aRect.Top
Rect2.Bottom = aRect.Bottom
    
'If Not TypeOf Sortie Is PictureBox Then
    If aCell.KeyForm <> "" Then aCell.ContenuCell = aCell.KeyForm
'End If
    
'-> Dans un premier temps, calculer la taille necessaire pour afficher si ajustement automatique
'If IsCellAjust Then DrawText Sortie.hdc, aCell.ContenuCell, Len(aCell.ContenuCell), Rect2, DT_CALCRECT Or DT_WORDBREAK
Dim lPtr As Long
If isUTF8(aCell.ContenuCell) Then
    acellcontenu = UTF8ToA(aCell.ContenuCell)
Else
    acellcontenu = aCell.ContenuCell
End If

lPtr = StrPtr(acellcontenu)

If lPtr <> 0 And IsCellAjust Then
    DrawTextW Sortie.hdc, lPtr, Len(acellcontenu + " "), Rect2, DT_CALCRECT Or DT_WORDBREAK
End If

'-> R�cup�ration des diff�rences de largeur et de hauteur
DifL = (aRect.Right - aRect.Left) - (Rect2.Right - Rect2.Left)
DifH = (aRect.Bottom - aRect.Top) - (Rect2.Bottom - Rect2.Top)
    
'-> Calcul des alignements de base
AlignBase(1, 3) = Rect2.Left + DifL / 2
AlignBase(2, 3) = Rect2.Right + DifL / 2

AlignBase(1, 4) = Rect2.Left + DifL
AlignBase(2, 4) = Rect2.Right + DifL

AlignBase(1, 5) = Rect2.Top + DifH / 2
AlignBase(2, 5) = Rect2.Bottom + DifH / 2

AlignBase(1, 6) = Rect2.Top + DifH
AlignBase(2, 6) = Rect2.Bottom + DifH
        
'-> Alignement interne dans le rectangle
Select Case aCell.CellAlign
    
    Case 1
        If IsCellAjust Then
            pDrawText = DT_LEFT
            '-> Pas necessaire de modifier les alignements
        Else
            pDrawText = DT_LEFT Or DT_TOP
        End If
        
    Case 2
        If IsCellAjust Then
            pDrawText = DT_CENTER
            Rect2.Left = AlignBase(1, 3)
            Rect2.Right = AlignBase(2, 3)
        Else
            pDrawText = DT_CENTER Or DT_TOP
        End If
    
    Case 3
        If IsCellAjust Then
            pDrawText = DT_RIGHT
            Rect2.Left = AlignBase(1, 4)
            Rect2.Right = AlignBase(2, 4)
        Else
            pDrawText = DT_RIGHT Or DT_TOP
        End If
                
    Case 4
        If IsCellAjust Then
            pDrawText = DT_LEFT
            Rect2.Top = AlignBase(1, 5)
            Rect2.Bottom = AlignBase(2, 5)
        Else
            pDrawText = DT_VCENTER Or DT_LEFT
        End If
    
    Case 5
        If IsCellAjust Then
            pDrawText = DT_CENTER
            Rect2.Top = AlignBase(1, 5)
            Rect2.Bottom = AlignBase(2, 5)
            Rect2.Left = AlignBase(1, 3)
            Rect2.Right = AlignBase(2, 3)
        Else
            pDrawText = DT_VCENTER Or DT_CENTER
        End If
    
    Case 6
        If IsCellAjust Then
            pDrawText = DT_RIGHT
            Rect2.Top = AlignBase(1, 5)
            Rect2.Bottom = AlignBase(2, 5)
            Rect2.Left = AlignBase(1, 4)
            Rect2.Right = AlignBase(2, 4)
        Else
            pDrawText = DT_VCENTER Or DT_RIGHT
        End If
    
    Case 7
        If IsCellAjust Then
            pDrawText = DT_LEFT
            Rect2.Top = AlignBase(1, 6)
            Rect2.Bottom = AlignBase(2, 6)
        Else
            pDrawText = DT_BOTTOM Or DT_LEFT
        End If
    
    Case 8
        If IsCellAjust Then
            pDrawText = DT_CENTER
            Rect2.Top = AlignBase(1, 6)
            Rect2.Bottom = AlignBase(2, 6)
            Rect2.Left = AlignBase(1, 3)
            Rect2.Right = AlignBase(2, 3)
        Else
            pDrawText = DT_BOTTOM Or DT_CENTER
        End If
    
    Case 9
        If IsCellAjust Then
            pDrawText = DT_RIGHT
            Rect2.Top = AlignBase(1, 6)
            Rect2.Bottom = AlignBase(2, 6)
            Rect2.Left = AlignBase(1, 4)
            Rect2.Right = AlignBase(2, 4)
        Else
            pDrawText = DT_BOTTOM Or DT_RIGHT
        End If
    
End Select
    
'-> on verifie si l'impression ne deborde pas
If Rect2.Right > Sortie.ScaleWidth And Not DebordementTop Then
    If AllowDebordement Then DebordementTop = True
End If
    
If IsCellAjust Then
    pDrawText = pDrawText Or DT_WORDBREAK
Else
    pDrawText = pDrawText Or DT_SINGLELINE
End If
    
'-> on regarde si on a une image sur la cellule
If aCell.Image <> "" Then
    '-> cr�er une nouvelle image
    Dim acellimage As String
    If InStr(1, aCell.Image, "/") <> 0 Or InStr(1, aCell.Image, "\") <> 0 Then
        If InStr(1, aCell.Image, ":", vbTextCompare) = 0 Then
                acellimage = aCell.Image
                '-> dans le cas ou on ne trouve pas l'image on essaye de la telecharger
                If Dir$(acellimage) = "" Then
                    If Dir$(GetFileName(acellimage)) = "" Then
                        If TurboGraphWebFile <> "" Then
                            If TelechargeFile(TurboGraphWebFile & acellimage, App.Path & "\Images\" & GetFileName(acellimage)) Then
                                acellimage = App.Path & "\Images\" & GetFileName(acellimage)
                            Else
                                Trace "Impossible de telecharger le fichier de " & TurboGraphWebFile & "/deallogo/" & GetFileName(acellimage)
                                If TypeOf Sortie Is PictureBox Then
                                    MDIMain.StatusBar1.Panels(1).Text = "Impossible de telecharger l'image suivante : " & TurboGraphWebFile & "/deallogo/" & GetFileName(acellimage)
                                End If
                            End If
                        End If
                    Else
                        acellimage = App.Path & "\Images\" & GetFileName(acellimage)
                    End If
                End If
        Else
            If InStr(1, aCell.Image, "http:", vbTextCompare) <> 0 Then
                strTemp = App.Path & "\Images\" & GetFileName(aCell.Image) 'GetTempFileNameVB("img") + ".gif"
                If TelechargeFile(aCell.Image, strTemp) Then
                    acellimage = strTemp
                End If
            Else
                acellimage = aCell.Image
                '-> dans le cas ou on ne trouve pas l'image on essaye de la telecharger
                On Error Resume Next
                If Dir$(acellimage) = "" Then
                    If TurboGraphWebFile <> "" Then
                        If TelechargeFile(TurboGraphWebFile & "/deallogo/" & GetFileName(acellimage), App.Path & "\Images\" & GetFileName(acellimage)) Then
                            acellimage = App.Path & "\Images\" & GetFileName(acellimage)
                        Else
                            Trace "Impossible de telecharger le fichier de " & TurboGraphWebFile & "/deallogo/" & GetFileName(acellimage)
                            If TypeOf Sortie Is PictureBox Then
                                MDIMain.StatusBar1.Panels(1).Text = "Impossible de telecharger l'image suivante : " & TurboGraphWebFile & "/deallogo/" & GetFileName(acellimage)
                            End If
                        End If
                    End If
                End If
                On Error GoTo GestErr
            End If
        End If
    Else
        acellimage = App.Path & "/images/" & aCell.Image
        '-> dans le cas ou on ne trouve pas l'image on essaye de la telecharger
        If Dir$(acellimage) = "" Then
            If TurboGraphWebFile <> "" Then
                If TelechargeFile(TurboGraphWebFile & "/deallogo/" & GetFileName(acellimage), App.Path & "\Images\" & GetFileName(acellimage)) Then
                    acellimage = App.Path & "\Images\" & GetFileName(acellimage)
                Else
                    Trace "Impossible de telecharger le fichier de " & TurboGraphWebFile & "/deallogo/" & GetFileName(acellimage)
                    If TypeOf Sortie Is PictureBox Then
                        MDIMain.StatusBar1.Panels(1).Text = "Impossible de telecharger l'image suivante : " & TurboGraphWebFile & "/deallogo/" & GetFileName(acellimage)
                    End If
                End If
            End If
        End If
    End If
    On Error Resume Next
    Dim zoomFit As Double
    If TypeOf Sortie Is PictureBox Then
        Load aSpool.frmdisplay.lblPicture(aSpool.frmdisplay.lblPicture.Count + 1)
        '-> Positionner l'objet
        aSpool.frmdisplay.lblPicture(aSpool.frmdisplay.lblPicture.Count).Top = y1
        aSpool.frmdisplay.lblPicture(aSpool.frmdisplay.lblPicture.Count).Left = x1
        aSpool.frmdisplay.lblPicture(aSpool.frmdisplay.lblPicture.Count).Visible = False
        
        '->on commence par ajuster la taille de l'immage par rapport � la cellule
        aSpool.frmdisplay.lblPicture(aSpool.frmdisplay.lblPicture.Count).Picture = LoadPicture(acellimage)
        aSpool.frmdisplay.lblPicture(aSpool.frmdisplay.lblPicture.Count).ScaleMode = vbPixels
        Set apic = aSpool.frmdisplay.lblPicture(aSpool.frmdisplay.lblPicture.Count)
        If apic.height > aCell.y2 - aCell.y1 Then
            zoomFit = (aCell.y2 - aCell.y1) / apic.height
        Else
            zoomFit = 1
        End If
        If apic.width > aCell.x2 - aCell.x1 Then
            If (aCell.x2 - aCell.x1) / apic.width < zoomFit Then zoomFit = (aCell.x2 - aCell.x1) / apic.width
        End If
        If aCell.imageHeight <> "" Then
            topImage = True
            zoomFit = val(aCell.imageHeight) / apic.height
        End If
        If aCell.imageWidth <> "" Then
            topImage = True
            zoomFit = val(aCell.imageWidth) / apic.width
        End If
        If topImage Then GoTo suite
        If Zoom * zoomFit <> 1 Then
            'aSpool.frmdisplay.lblPicture(aSpool.frmdisplay.lblPicture.Count).Picture = LoadPicture(acellimage)
            'aSpool.frmdisplay.lblPicture(aSpool.frmdisplay.lblPicture.Count).ScaleMode = vbPixels
            'Set aPic = aSpool.frmdisplay.lblPicture(aSpool.frmdisplay.lblPicture.Count)
            If Zoom * zoomFit < 1 Then
                '-> on redimensionne l'image
                StretchBlt apic.hdc, 0, 0, apic.width * Zoom * zoomFit, apic.height * Zoom * zoomFit, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
                '-> on ne conserve que la partie interessante
                'aSpool.frmdisplay.lblPicture(aSpool.frmdisplay.lblPicture.Count).Height = aPic.Height * zoom * zoomfit
                'aSpool.frmdisplay.lblPicture(aSpool.frmdisplay.lblPicture.Count).Width = aPic.Width * zoom * zoomfit
                StretchBlt Sortie.hdc, x1, y1, apic.ScaleWidth * Zoom * zoomFit, apic.ScaleHeight * Zoom * zoomFit, apic.hdc, 0, 0, apic.ScaleWidth * Zoom * zoomFit, apic.ScaleHeight * Zoom * zoomFit, vbSrcCopy
            Else
                '-> on redimensionne l'image
                apic.width = apic.width * Zoom * zoomFit
                apic.height = apic.height * Zoom * zoomFit
                StretchBlt apic.hdc, 0, 0, apic.width * Zoom * zoomFit, apic.height * Zoom * zoomFit, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
                '-> on ne conserve que la partie interessante
                StretchBlt Sortie.hdc, x1, y1, apic.ScaleWidth, apic.ScaleHeight * Zoom * zoomFit, apic.hdc, 0, 0, apic.ScaleWidth, apic.ScaleHeight * Zoom * zoomFit, vbSrcCopy
            End If
            'Sortie.PaintPicture aPic.Picture, X1, Y1
            Sortie.Refresh
        Else
            'aSpool.frmdisplay.lblPicture(aSpool.frmdisplay.lblPicture.Count).Picture = LoadPicture(acellimage)
            'Sortie.PaintPicture aSpool.frmdisplay.lblPicture(aSpool.frmdisplay.lblPicture.Count).Picture, X1, Y1
            StretchBlt apic.hdc, 0, 0, apic.width * Zoom * zoomFit, apic.height * Zoom * zoomFit, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
            StretchBlt Sortie.hdc, x1, y1, apic.ScaleWidth, apic.ScaleHeight * Zoom * zoomFit, apic.hdc, 0, 0, apic.ScaleWidth, apic.ScaleHeight * Zoom * zoomFit, vbSrcCopy
            Sortie.Refresh
        End If
    Else
        Set apic = frmLib.PicObj(0)
        apic.AutoSize = 1
        apic.ScaleMode = vbPixels
        apic.Picture = LoadPicture(acellimage)
        If apic.height > aCell.y2 - aCell.y1 Then
            zoomFit = (aCell.y2 - aCell.y1) / apic.height
        Else
            zoomFit = 1
        End If
        If apic.width > aCell.x2 - aCell.x1 Then
            If (aCell.x2 - aCell.x1) / apic.width < zoomFit Then zoomFit = (aCell.x2 - aCell.x1) / apic.width
        End If
        If aCell.imageHeight <> "" Then
            topImage = True
            zoomFit = val(aCell.imageHeight) / apic.ScaleHeight
        End If
        If aCell.imageWidth <> "" Then
            topImage = True
            zoomFit = val(aCell.imageWidth) / apic.ScaleWidth
        End If
        apic.width = apic.width * zoomFit
        apic.height = apic.height * zoomFit
        If topImage Then GoTo suite
        Trace "drawcell image " & apic.width & " " & apic.height
        Sortie.PaintPicture apic.Picture, x1, y1, Sortie.ScaleX(apic.width, 3, 3), Sortie.ScaleY(apic.height, 3, 3)
    End If
    aCell.Image = ""
End If

suite:
If topImage Then
    Dim aBmp As ImageObj
    Set aBmp = New ImageObj
    aCell.Image = ""
    aBmp.Left = x1
    aBmp.Top = y1
    If TypeOf Sortie Is PictureBox Then aBmp.IdAffichage = aSpool.frmdisplay.lblPicture.Count
    'aSpool.frmdisplay.Visible = False
    aBmp.Nom = acellimage
    aBmp.Largeur = zoomFit
    If listImage Is Nothing Then Set listImage = New Collection
    listImage.Add aBmp
End If

On Error GoTo GestErr

'-> Gestion des formats num�rics
Select Case aCell.TypeValeur
    Case 0 '-> RAS
        PrintSigne = False
    Case 1 '-> Caract�re
        '-> Faire un susbstring
        On Error Resume Next
        aCell.ContenuCell = Mid$(aCell.ContenuCell, 1, CInt(aCell.Msk))
        On Error GoTo GestErr
        PrintSigne = False
    Case 2 '-> Numeric
'-> V�rifier s'il y a quelquechose � formater
        If Trim(aCell.ContenuCell) = "" Then
        Else
'            If GetWindowsVersion >= 6 Then
'                Rect2.Left = Rect2.Left - 5
'                Rect2.Right = Rect2.Right - 5
'            End If
            On Error Resume Next
            aFt = FormatNumTP(Trim(aCell.ContenuCell), aCell.Msk, NomBlock, "L" & aCell.Ligne & "C" & aCell.Colonne)
            '-> Rajouter Trois blancs au contenu de la cellule
            If aFt.Ok Then
                '-> Mettre e rouge si necessaire
                If aFt.Value < 0 And aFt.idNegatif > 1 Then Sortie.foreColor = QBColor(12)
                '-> affecter le contenu de la cellule
                aCell.ContenuCell = aFt.strFormatted
                '-> Si c'est un 0 mettre � blanc si pas imprimer
                If val(Replace(Replace(aCell.ContenuCell, ",", ""), ".", "")) = 0 And Not (aCell.PrintZero) Then
                    Dim rex As RegExp
                    Set rex = New RegExp
                    rex.Pattern = "[^a-zA-Z]"
                    If Not rex.test(aCell.ContenuCell) Then
                        aCell.ContenuCell = ""
                    End If
                Else
                    If aFt.Value < 0 Then
                        If aFt.idNegatif = 0 Or aFt.idNegatif = 3 Then
                            aCell.ContenuCell = "- " & aCell.ContenuCell
                            PrintSigne = False
                            Rect2.Left = aRect.Left - 4
                            Rect2.Right = aRect.Right - 4
                        ElseIf aFt.idNegatif = 1 Or aFt.idNegatif = 4 Then
                            aCell.ContenuCell = RTrim(aCell.ContenuCell) & "- "
                            PrintSigne = True
                        End If
                    Else
                        Rect2.Left = aRect.Left - 4
                        Rect2.Right = aRect.Right - 4
                    End If
                End If
                If aFt.idNegatif <> 4 Then
                    '-> on vire des blancs inutiles a droite
                    aCell.ContenuCell = RTrim(aCell.ContenuCell) & " "
                End If
            Else
                '-> Ne pas imprimer  de signe
                PrintSigne = False
            End If
            On Error GoTo GestErr
            'If wVersion >= 6 Then
            aCell.ContenuCell = "  " & aCell.ContenuCell '& "  "
            'If Mid(aCell.ContenuCell, Len(aCell.ContenuCell) - 1, Len(aCell.ContenuCell)) <> " " Then aCell.ContenuCell = aCell.ContenuCell & "  "
        End If
        If isUTF8(aCell.ContenuCell) Then
            acellcontenu = UTF8ToA(aCell.ContenuCell)
        Else
            acellcontenu = aCell.ContenuCell
        End If
End Select


'-> Faire de rect la zone de clipping en cours
hdlRgn = CreateRectRgn&(aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
SelectClipRgn Sortie.hdc, hdlRgn

'-> Postionnement du rectangle de dessin
'DrawText Sortie.hdc, aCell.ContenuCell, Len(aCell.ContenuCell), Rect2, pDrawText Or DT_NOPREFIX
lPtr = StrPtr(acellcontenu)
If aCell.UseFormText <> 0 And TypeOf Sortie Is PictureBox Then
Else
    If lPtr <> 0 Then DrawTextW Sortie.hdc, lPtr, Len(acellcontenu), Rect2, pDrawText Or DT_NOPREFIX
End If
'If PrintSigne Then DrawText Sortie.hDC, "- ", 2, Rect2, pDrawText Or DT_NOPREFIX

'-> Redessiner le texte de la cellule si on recherche du texte
If FindTop Then
    '-> On g�re le cas du num�rique avec les s�parateur des milliers
    aCellContenuTemp = Replace(aCell.ContenuCell, SepMil, "")
    '-> On fait la recherche
    If InStr(1, aCellContenuTemp, FindText, vbTextCompare) <> 0 Then
        'on compte le nombre d'occurences dans la chaine
        i = NumEntries(UCase(aCellContenuTemp), UCase(FindText)) - 1
        FindPos2 = FindPos2 + i
        '-> on regarde si c'est bien celui que l'on veut dessiner
        If FindPage = aSpool.CurrentPage And FindPos < FindPos2 Or FindPage < aSpool.CurrentPage Then
            FindPos2 = FindPos2 - i + 1
            'a cause du s�parateur millier on redefini eventuellement le texte recherch�
            FindTextTemp = GetNewFindText(FindText, aCell.ContenuCell, FindPos2 - FindPos)
            DrawFindText FindTextTemp, pDrawText Or DT_NOPREFIX, Rect2.Left, Rect2.Right, Rect2.Top, Rect2.Bottom, aCell
            FindPos = FindPos + 1
            FindPage = aSpool.CurrentPage
            FindTop = False
            '-> assigner une valeur au scroll
            FindScrollH = aRect.Left
            FindScrollV = aRect.Top
        End If
    End If
End If

'-> Faire de la fen�tre entiere la zone de clipping
SelectClipRgn Sortie.hdc, 0

'-> Dessin des bordures de la cellule
SelectObject Sortie.hdc, hdlBordure

'-> Dessin de la bordure Bas
If aCell.BordureBas Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureBas Then
Else
    DrawBordure x1 - 1, y2 - 1, x2, y2 - 1, Couleur
    'DrawBordure X1 - 1, Y2 - 1, X2 - 1, Y2 - 1, Couleur
End If

'-> Dessin de la bordure haut
If aCell.BordureHaut Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureHaut Then
Else
    DrawBordure x1 - 1, y1 - 1, x2, y1 - 1, Couleur
End If


'-> Dessin de la bordure gauche
If aCell.BordureGauche Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureGauche Then
Else
    DrawBordure x1 - 1, y1 - 1, x1 - 1, y2, Couleur
End If

'-> Dessin de la bordure Droite
If aCell.BordureDroite Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureDroite Then
Else
    DrawBordure x2 - 1, y1 - 1, x2 - 1, y2, Couleur
End If

'-> Gestion des angles des bordures
If aCell.BordureGauche Or aCell.BordureHaut Then
    '-> dessiner le coins sup�rieur gauche
    Couleur = 0
    DrawBordure x1 - 1, y1 - 1, x1 - 1, y1, Couleur
End If

If aCell.BordureDroite Or aCell.BordureHaut Then
    '-> Dessiner le coin sup�rieur droit
    Couleur = 0
    DrawBordure x2 - 1, y1 - 1, x2 - 1, y1, Couleur
End If

If aCell.BordureBas Or aCell.BordureGauche Then
    '-> Dessin du coin inf�rieur gauche
    Couleur = 0
    DrawBordure x1 - 1, y2 - 1, x1, y2 - 1, Couleur
End If

If aCell.BordureDroite Or aCell.BordureBas Then
    '-> Dessin du coin inf�rieur droit
    Couleur = 0
    DrawBordure x2 - 1, y2 - 1, x2 - 1, y2, Couleur
End If

'-> Supprimer la r�gion
DeleteObject hdlRgn
               
'-> Res�lectionner l'ancien stylo
If OldPen <> 0 Then
    SelectObject Sortie.hdc, OldPen
    DeleteObject hdlPen
End If

'-> Res�lectionner l'ancien pinceau
If oldBrush <> 0 Then
    SelectObject Sortie.hdc, oldBrush
    DeleteObject hdlBrush
End If

'-> Supprimer le stylo de dessin des bordures
DeleteObject hdlBordure

'-> Positionnement du label pour l'acces au d�tail
If aCell.UseAccesDet Then
    '-> V�rifier que l'on soit en visu
    If TypeOf Sortie Is PictureBox Then
        '-> cr�er un nouveau Label
        Load aSpool.frmdisplay.lblAccesDet(aSpool.frmdisplay.lblAccesDet.Count + 1)
        '-> Positionner l'objet
        aSpool.frmdisplay.lblAccesDet(aSpool.frmdisplay.lblAccesDet.Count).Left = x1
        aSpool.frmdisplay.lblAccesDet(aSpool.frmdisplay.lblAccesDet.Count).width = x2 - x1
        aSpool.frmdisplay.lblAccesDet(aSpool.frmdisplay.lblAccesDet.Count).Top = y1
        aSpool.frmdisplay.lblAccesDet(aSpool.frmdisplay.lblAccesDet.Count).height = y2 - y1
        '-> Acc�der au tableau de premier niveau
        Set aTb = aSpool.maquette.Tableaux(UCase$(NomTb))
        '-> Pointer sur le block de tableau � �diter
        Set aBlock = aTb.blocks("BL-" & UCase$(NomBlock))
        '-> Positionner la cl� d'acc�s au d�tail
        aSpool.frmdisplay.lblAccesDet(aSpool.frmdisplay.lblAccesDet.Count).Tag = aCell.KeyAccesDet & Chr(0) & Entry(2, Entry(1, aBlock.KeyAccesDet, "�"), "|")
        '-> Le rendre visible
        aSpool.frmdisplay.lblAccesDet(aSpool.frmdisplay.lblAccesDet.Count).Visible = True
    End If 'Si on est en vosu ou en impression
End If 'Si on a acc�s au d�tail

'-> on regarde si on a un lien hypertext
If aCell.UseHyperText Then
    '-> V�rifier que l'on soit en visu
    If TypeOf Sortie Is PictureBox Then
        '-> cr�er un nouveau Label
        Load aSpool.frmdisplay.lblHyperText(aSpool.frmdisplay.lblHyperText.Count + 1)
        '-> Positionner l'objet
        aSpool.frmdisplay.lblHyperText(aSpool.frmdisplay.lblHyperText.Count).Left = x1
        aSpool.frmdisplay.lblHyperText(aSpool.frmdisplay.lblHyperText.Count).width = x2 - x1
        aSpool.frmdisplay.lblHyperText(aSpool.frmdisplay.lblHyperText.Count).Top = y1
        aSpool.frmdisplay.lblHyperText(aSpool.frmdisplay.lblHyperText.Count).height = y2 - y1
        aSpool.frmdisplay.lblHyperText(aSpool.frmdisplay.lblHyperText.Count).Tag = aCell.KeyLink
        aSpool.frmdisplay.lblHyperText(aSpool.frmdisplay.lblHyperText.Count).ToolTipText = "Lien vers : " & aCell.KeyLink
        '-> Le rendre visible
        aSpool.frmdisplay.lblHyperText(aSpool.frmdisplay.lblHyperText.Count).Visible = True
        aCell.UseHyperText = False
    End If 'Si on est en vosu ou en impression
End If

'-> on regarde si on a un lien hypertext
If aCell.UseFormText <> 0 Then
    Dim j As Integer
    For j = 1 To aCell.UseFormText
        '-> V�rifier que l'on soit en visu
        If TypeOf Sortie Is PictureBox Then
            '-> cr�er un nouveau Label
            Load aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count + 1)
            '-> Positionner l'objet
            aSpool.frmdisplay.label.FontName = aCell.FontName
            aSpool.frmdisplay.label.FontSize = aCell.FontSize
            aSpool.frmdisplay.label.FontBold = aCell.FontBold
            aSpool.frmdisplay.label.FontItalic = aCell.FontItalic
            aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).Left = x1 + (1 * Zoom)
            aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).width = x2 - x1 - (2 * Zoom)
            aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).Top = y1 + 6 - (5 * Zoom)
            aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).height = y2 - y1 - (4.1 * Zoom)
            aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).Tag = aCell.keyFormIndex
            aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).Text = aCell.KeyForm
            aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).FontName = aCell.FontName
            aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).FontSize = aCell.FontSize * Zoom
            aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).FontItalic = aCell.FontItalic
            aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).FontBold = aCell.FontBold
            If aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).height <> aSpool.frmdisplay.label.height Then
                aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).Top = aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).Top + (aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).height - aSpool.frmdisplay.label.height) / 2
                aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).height = aSpool.frmdisplay.label.height
            End If
            Select Case aCell.CellAlign
                Case 1, 4, 7
                    aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).Alignment = 0
                Case 3, 6, 9
                    aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).Alignment = 1
                Case 2, 5, 8
                    aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).Alignment = 2
            End Select
            aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).ToolTipText = "Zone de saisie"
            '-> Le rendre visible
            If j = 1 Then
                aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).Visible = True
            Else
                aSpool.frmdisplay.lblForm(aSpool.frmdisplay.lblForm.Count).Visible = False
            End If
        Else
        
        End If 'Si on est en vosu ou en impression
    Next
    '-> on eteind le top
    aCell.UseFormText = 0
    aCell.KeyForm = ""

End If

'-> Renvoyer une valeur de succ�s
DrawCell = True

Exit Function

GestErr:

    Call GestError(41, NomTb & "|" & NomBlock & "|" & "L" & aCell.Ligne & "C" & aCell.Colonne, aSpool)
    

End Function

Private Sub DrawBordure(ByVal Move1, Move2, Line1, Line2, Couleur)

Dim hdlBordure As Long
Dim aPoint As POINTAPI
Dim Res As Long
Dim Old As Long

hdlBordure = CreatePen(PS_SOLID, 1 * Zoom, Couleur)
Old = SelectObject(Sortie.hdc, hdlBordure)
Res = MoveToEx(Sortie.hdc, Move1, Move2, aPoint)
LineTo Sortie.hdc, Line1, Line2
SelectObject Sortie.hdc, Old
DeleteObject hdlBordure


End Sub


Public Sub AnalyseObj(ByVal Ligne As String, TypeObjet As String, _
                       ByRef NomObjet As String, ByRef TypeSousObjet As String, _
                       ByRef NomSousObjet As String, ByRef Param As String, _
                       ByRef DataFields As String)

Dim Param1 As String
Dim Param2 As String
Dim Param3 As String
Dim Param4 As String

'---> Procedure qui r�cupre les d�finitions des objets dans une ligne
On Error Resume Next

If Ligne = "" Then Exit Sub

Param1 = Entry(1, Ligne, "]")
Param2 = Entry(2, Ligne, "]")
'Param3 = Entry(2, Ligne, "{")
'-> pb sur le caractere { present dans les valeurs
Param3 = Mid(Ligne, InStr(1, Ligne, "{") + 1)

Param1 = Mid$(Param1, 2, Len(Param1) - 2)
'-> R�cup�ration du champ de donn�es
If InStr(1, Param3, "}") <> 0 Then
    DataFields = Mid$(Param3, 1, Len(Param3) - 1)
Else
    DataFields = Param3 + " "
End If
'-> R�cup�ration des param�tres d'application
Param = Mid$(Param2, 2, Len(Param2) - 1)
'-> R�cup�ration du nom des objets
Param2 = Entry(1, Param1, "(")
Param3 = Entry(2, Param1, "(")

'-> Objet de premier niveau
TypeObjet = Entry(1, Param2, "-")
NomObjet = Entry(2, Param2, "-")
If isUTF8(NomObjet) Then NomObjet = UTF8ToA(NomObjet)

'-> Objet de second niveau
TypeSousObjet = Entry(1, Param3, "-")
NomSousObjet = Entry(2, Param3, "-")
If isUTF8(NomSousObjet) Then NomSousObjet = UTF8ToA(NomSousObjet)

End Sub
Public Function PrintSection(ByVal SectionName As String, ByRef Param As String, _
                         ByRef DataFields As String, ByRef FirstObj As Boolean, _
                         ByRef PositionX As Long, ByRef PositionY As Long, ByRef aSpool As Spool) As Boolean

'---> Cette proc�dure est charg�e d'imprimer une section

Dim aSection As Section
Dim i As Integer
Dim NomObjet As String
Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim DebutRangX As Long
Dim DebutRangY As Long

On Error GoTo GestError

Trace "PrintSection " & SectionName, 2
Trace DataFields, 5

'-> Pointer sur la section � imprimer
Set aSection = aSpool.maquette.Sections(UCase$(Trim(SectionName)))

'-> on g�re ici les libell�s variables
Dim strTemp As String
strTemp = DataFields + "^"
For i = 0 To NumEntries(aSection.LibelVariable, "�") - 1
    If InStr(1, strTemp, Entry(i * 2 + 1, aSection.LibelVariable, "�")) <> 0 Then
        If Entry(curPage, Entry(i * 2 + 2, aSection.LibelVariable, "�"), "�") <> "" Then
            DataFields = Mid(strTemp, 1, InStr(1, strTemp, Entry(i * 2 + 1, aSection.LibelVariable, "�")) + 4) + Replace(Entry(curPage, Entry(i * 2 + 2, aSection.LibelVariable, "�"), "�"), Chr(0), "") + Mid(Mid(strTemp, InStr(1, strTemp, Entry(i * 2 + 1, aSection.LibelVariable, "�")) + 4, 3200), InStr(1, Mid(strTemp, InStr(1, strTemp, Entry(i * 2 + 1, aSection.LibelVariable, "�")) + 4, 3200), "^"), 3200)
            DataFields = Mid(DataFields, 1, Len(DataFields) - 1)
        End If
    End If
Next

'-> Imprimer la section
If Not PrintObjRtf(PositionX, PositionY, aSection, DataFields, DebutRangX, DebutRangY, aSpool) Then GoTo GestError

'-> Impression des objets associ�s dans l'ordre d'affichage
For i = 1 To aSection.nEntries - 1
    NomObjet = aSection.GetOrdreAffichage(i)
    If UCase$(Entry(1, NomObjet, "-")) = "CDR" Then
        '-> Pointer sur le cadre
        Set aCadre = aSection.Cadres(UCase$(Entry(2, NomObjet, "-")))
        '-> Imprimer l'objet
        Trace "Printsection RTF valeurs " & NomObjet & DataFields, 4
        If Not PrintObjRtf(PositionX, PositionY, aCadre, DataFields, DebutRangX, DebutRangY, aSpool, aCadre.MargeInterne) Then GoTo GestError
        '-> Liberer le pointeur
        Set aCadre = Nothing
    ElseIf UCase$(Entry(1, NomObjet, "-")) = "BMP" Then
        Set aBmp = aSection.Bmps(UCase$(Entry(2, NomObjet, "-")))
        If Not PrintBmp(aBmp, DebutRangX, DebutRangY, DataFields, aSpool) Then GoTo GestError
    End If
Next

'-> Positionner le pointeur de position apr�s l'�dition de la section
PositionY = Sortie.ScaleY(aSection.Hauteur * Zoom, 7, 3) + PositionY 'Sortie.ScaleX(DebutRangY, 1, 3)

'-> Liberer les pointeurs
Set aSection = Nothing

PrintSection = True

Exit Function

GestError:

    Call GestError(36, aSection.Nom, aSpool)
    PrintSection = False

End Function

Private Function PrintBmp(aBmp As ImageObj, DebutRangX As Long, DebutRangY As Long, ByVal DataFields As String, ByRef aSpool As Spool) As Boolean

'---> Impression d'un BMP

Dim PosX As Long
Dim PosY As Long
Dim apic As PictureBox
Dim Champ As String
Dim NomFichier As String
Dim aRect As RECT
Dim hdlPen As Long
Dim OldPen As Long
Dim hdlBrush As Long
Dim oldBrush As Long
Dim i As Integer
Dim strFichier As String
Dim zoomFit As Double

On Error GoTo GestErr

Trace "PrintBmp", 2
Trace DataFields

'-> Il faut modifier la position X et Y du cadre
PosX = Sortie.ScaleX(DebutRangX, 1, 3) + Sortie.ScaleX(aBmp.Left * Zoom, 7, 3)
PosY = Sortie.ScaleY(DebutRangY, 1, 3) + Sortie.ScaleY(aBmp.Top * Zoom, 7, 3)

'*****PENSER A LA VERSION ENTREPRISE*******


'-> si on a une Image prioritaire on essai de voir si on la trouve
If aBmp.Default <> "" Then
    Trace "Traitement par l'image Default"
    '-> si on a une image
    If InStr(1, aBmp.Default, "^", vbTextCompare) <> 0 Then
        '-> on est sur un dieze on recupere sa valeur
        '-> R�cup�ration du nombre de champs
        For i = 2 To NumEntries(DataFields, "^")
            Champ = Entry(i, DataFields, "^")
            If UCase$(aBmp.Default) = "^" & UCase$(Mid$(Champ, 1, 4)) Then
                '-> R�cup�ration du nom du fichier
                NomFichier = RTrim(Mid$(Champ, 5, Len(Champ) - 1))
                '-> Test de l'utilisation de l'association
                strFichier = GetPictureAssociation(NomFichier)
                If strFichier <> "" Then
                    '-> R�cup�rer le nom du fichier dans le fichier tm_picture.ini
                    If NomFichier = strFichier Then Trace "Image non trouv�e dans le tm_picture"
                    NomFichier = strFichier
                Else
                    If Dir$(NomFichier) = "" Then NomFichier = App.Path & "\Images\" & NomFichier
                End If
                Exit For
            End If
        Next
    Else
        '-> on est sur un nom de fichier
        NomFichier = Trim(aBmp.Default)
    End If
        
    '-> on teste si l'image est dans /images/
    If Dir$(NomFichier) = "" Then
        If Dir$(App.Path & "\Images\" & GetFileName(NomFichier)) <> "" Then
            NomFichier = App.Path & "\Images\" & GetFileName(NomFichier)
        End If
    End If
        
    '-> dans le cas ou on ne trouve pas l'image on essaye de la telecharger
    If Dir$(NomFichier) = "" Then
        If TurboGraphWebFile <> "" Then
            If TelechargeFile(TurboGraphWebFile & "/deallogo/" & GetFileName(NomFichier), App.Path & "\Images\" & GetFileName(NomFichier)) Then
                NomFichier = App.Path & "\Images\" & GetFileName(NomFichier)
            Else
                Trace "Impossible de telecharger le fichier de " & TurboGraphWebFile & "/deallogo/" & GetFileName(NomFichier)
                If TypeOf Sortie Is PictureBox Then
                    MDIMain.StatusBar1.Panels(1).Text = "Impossible de telecharger le fichier de " & TurboGraphWebFile & "/deallogo/" & GetFileName(NomFichier)
                End If
            End If
        End If
    End If
    
    If Dir$(NomFichier) <> "" And Trim(NomFichier <> "") Then
        '-> si on a ramen� un nom de fichier on passe en image fixe pour continuer le traitement
        '-> V�rifier que le fichier existe
        Trace "Image : " + NomFichier, 1
        '-> Imprimer le bmp
        Set apic = frmLib.PicObj(aBmp.IdPic)
        If Dir$(NomFichier) = "" Or Trim(NomFichier) = "" Then
        Else
            If Zoom <> 1 Then
                apic.AutoSize = True
                apic.AutoRedraw = True
                apic.Picture = LoadPicture(NomFichier)
                apic.ScaleMode = vbPixels
                If Zoom < 1 Then
                    '-> on redimensionne l'image
                    StretchBlt apic.hdc, 0, 0, apic.width * Zoom, apic.height * Zoom, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
                    '-> on ne conserve que la partie interessante
                    StretchBlt Sortie.hdc, PosX, PosY, apic.ScaleWidth * Zoom, apic.ScaleHeight * Zoom, apic.hdc, 0, 0, apic.ScaleWidth * Zoom, apic.ScaleHeight * Zoom, vbSrcCopy
                Else
                    '-> on redimensionne l'image
                    apic.width = apic.width * Zoom
                    apic.height = apic.height * Zoom
                    StretchBlt apic.hdc, 0, 0, apic.width * Zoom, apic.height * Zoom, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
                    '-> on ne conserve que la partie interessante
                    StretchBlt Sortie.hdc, PosX, PosY, apic.ScaleWidth, apic.ScaleHeight * Zoom, apic.hdc, 0, 0, apic.ScaleWidth, apic.ScaleHeight * Zoom, vbSrcCopy
                End If
                Sortie.Refresh
            Else
                apic.Picture = LoadPicture(NomFichier)
                If aBmp.fitSize Then
                    apic.AutoSize = True
                    apic.AutoRedraw = True
                    apic.Picture = LoadPicture(NomFichier)
                    apic.ScaleMode = vbPixels
                    '-> on redimensionne l'image
                    StretchBlt apic.hdc, 0, 0, aBmp.Largeur, aBmp.Hauteur, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
                    '-> on ne conserve que la partie interessante
                    StretchBlt Sortie.hdc, PosX, PosY, apic.ScaleWidth * Zoom, apic.ScaleHeight * Zoom, apic.hdc, 0, 0, apic.ScaleWidth * Zoom, apic.ScaleHeight * Zoom, vbSrcCopy
                    Sortie.Refresh
                End If
                Sortie.PaintPicture apic.Picture, PosX, PosY
            End If
        End If
        If Not aBmp.Primary Then GoTo suite
    End If
End If

'-> Imprimer le bmp
Set apic = frmLib.PicObj(aBmp.IdPic)
If aBmp.isVariable Then
    '-> Venir charg� le BMP associ�
    Trace "Image variable", 3
    '-> R�cup�ration du nombre de champs
    For i = 2 To NumEntries(DataFields, "^")
        Champ = Entry(i, DataFields, "^")
        If UCase$(aBmp.Fichier) = "^" & UCase$(Mid$(Champ, 1, 4)) Then
            '-> R�cup�ration du nom du fichier
            NomFichier = RTrim(Mid$(Champ, 5, Len(Champ) - 1))
            '-> Test de l'utilisation de l'association
            If aBmp.UseAssociation Then
                '-> R�cup�rer le nom du fichier dans le fichier ini
                NomFichier = GetPictureAssociation(NomFichier)
            Else
                'If Dir$(NomFichier) = "" Then NomFichier = App.Path & "\Images\" & NomFichier
            End If
            Exit For
        End If
    Next
    '-> on teste si l'image est dans /images/
    If Dir$(NomFichier) = "" Then
        If IsFile(App.Path & "\Images\" & GetFileName(NomFichier)) Then
            NomFichier = App.Path & "\Images\" & GetFileName(NomFichier)
        End If
    End If
    
    If Not IsFile(NomFichier) Or Trim(NomFichier) = "" Then
        '-> en mode internet on essaye de telecharger le fichier
        If TurboGraphWebFile <> "" Then
            If TelechargeFile(TurboGraphWebFile & "/deallogo/" & GetFileName(NomFichier), App.Path & "\Images\" & GetFileName(NomFichier)) Then
                NomFichier = App.Path & "\Images\" & GetFileName(NomFichier)
            Else
                Trace "Impossible de telecharger le fichier de " & TurboGraphWebFile & "/deallogo/" & GetFileName(NomFichier)
                If TypeOf Sortie Is PictureBox Then
                    MDIMain.StatusBar1.Panels(1).Text = "Impossible de telecharger le fichier de " & TurboGraphWebFile & "/deallogo/" & GetFileName(NomFichier)
                End If
            End If
        End If
    End If
    
    '-> V�rifier que le fichier existe
    Trace "Image : " + NomFichier, 1
    If Not IsFile(NomFichier) Or Trim(NomFichier) = "" Then
    Else
        If Zoom <> 1 And aBmp.Hauteur <> 0 Then
            apic.AutoSize = True
            apic.AutoRedraw = True
            apic.Picture = LoadPicture(NomFichier)
            apic.ScaleMode = vbPixels
            If Zoom < 1 Then
                '-> on redimensionne l'image
                StretchBlt apic.hdc, 0, 0, apic.width * Zoom, apic.height * Zoom, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
                '-> on ne conserve que la partie interessante
                StretchBlt Sortie.hdc, PosX, PosY, apic.ScaleWidth * Zoom, apic.ScaleHeight * Zoom, apic.hdc, 0, 0, apic.ScaleWidth * Zoom, apic.ScaleHeight * Zoom, vbSrcCopy
            Else
                '-> on redimensionne l'image
                apic.width = apic.width * Zoom
                apic.height = apic.height * Zoom
                StretchBlt apic.hdc, 0, 0, apic.width * Zoom, apic.height * Zoom, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
                '-> on ne conserve que la partie interessante
                StretchBlt Sortie.hdc, PosX, PosY, apic.ScaleWidth, apic.ScaleHeight * Zoom, apic.hdc, 0, 0, apic.ScaleWidth, apic.ScaleHeight * Zoom, vbSrcCopy
            End If
            Sortie.Refresh
        Else
            If aBmp.Hauteur = 0 Then
                'ce cas permet d'eviter les pb de pixels
                If Zoom < 1 Then
                    apic.AutoSize = True
                    apic.AutoRedraw = True
                    apic.Picture = LoadPicture(NomFichier)
                    apic.ScaleMode = vbCentimeters
                    zoomFit = aBmp.Largeur / apic.ScaleWidth
                    zoomFit = zoomFit * Zoom
                    apic.ScaleMode = vbPixels
                    
                    '-> on redimensionne l'image
                    StretchBlt apic.hdc, 0, 0, apic.width * zoomFit, apic.height * zoomFit, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
                    '-> on ne conserve que la partie interessante
                    'StretchBlt Sortie.hdc, PosX, PosY, aBmp.Largeur * 1000, aBmp.Hauteur * 1000, apic.hdc, 0, 0, apic.ScaleWidth * Zoom, apic.ScaleHeight * Zoom, vbSrcCopy
                    StretchBlt Sortie.hdc, PosX, PosY, apic.ScaleWidth * zoomFit, apic.ScaleHeight * zoomFit, apic.hdc, 0, 0, apic.ScaleWidth * zoomFit, apic.ScaleHeight * zoomFit, vbSrcCopy
                    Sortie.Refresh
                Else
                    apic.AutoSize = True
                    apic.AutoRedraw = True
                    apic.Picture = LoadPicture(NomFichier)
                    apic.ScaleMode = vbCentimeters
                    zoomFit = aBmp.Largeur / apic.ScaleWidth
                    zoomFit = zoomFit * Zoom
                    apic.ScaleMode = vbPixels
                    '-> on redimensionne l'image
                    StretchBlt apic.hdc, 0, 0, apic.width * zoomFit, apic.height * zoomFit, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
                    '-> on ne conserve que la partie interessante
                    'StretchBlt Sortie.hdc, PosX, PosY, aBmp.Largeur * 1000, aBmp.Hauteur * 1000, apic.hdc, 0, 0, apic.ScaleWidth * Zoom, apic.ScaleHeight * Zoom, vbSrcCopy
                    StretchBlt Sortie.hdc, PosX, PosY, apic.ScaleWidth * zoomFit, apic.ScaleHeight * zoomFit, apic.hdc, 0, 0, apic.ScaleWidth * zoomFit, apic.ScaleHeight * zoomFit, vbSrcCopy
                    Sortie.Refresh
                End If
            Else
                apic.AutoSize = True
                apic.AutoRedraw = True
                apic.Picture = LoadPicture(NomFichier)
                Sortie.PaintPicture apic.Picture, PosX, PosY
            End If
        End If
    End If
Else
    Trace "Image non variable : " + aBmp.Fichier, 1
    If Trim(aBmp.Fichier) = "" Then
    Else
        If Dir$(aBmp.Fichier, vbNormal) = "" Then
        Else
            If Zoom <> 1 Then
                apic.AutoSize = True
                apic.AutoRedraw = True
                apic.ScaleMode = vbPixels
                apic.Picture = LoadPicture(aBmp.Fichier)
                If Zoom < 1 Then
                    '-> on redimensionne l'image
                    StretchBlt apic.hdc, 0, 0, apic.width * Zoom, apic.height * Zoom, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
                    '-> on ne conserve que la partie interessante
                    StretchBlt Sortie.hdc, PosX, PosY, apic.ScaleWidth * Zoom, apic.ScaleHeight * Zoom, apic.hdc, 0, 0, apic.ScaleWidth * Zoom, apic.ScaleHeight * Zoom, vbSrcCopy
                Else
                    '-> on redimensionne l'image
                    apic.width = apic.width * Zoom
                    apic.height = apic.height * Zoom
                    StretchBlt apic.hdc, 0, 0, apic.width * Zoom, apic.height * Zoom, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
                    '-> on ne conserve que la partie interessante
                    StretchBlt Sortie.hdc, PosX, PosY, apic.ScaleWidth, apic.ScaleHeight * Zoom, apic.hdc, 0, 0, apic.ScaleWidth, apic.ScaleHeight * Zoom, vbSrcCopy
                End If
                Sortie.Refresh
            Else
                If aBmp.fitSize Then
                    apic.AutoSize = True
                    apic.AutoRedraw = True
                    apic.Picture = LoadPicture(aBmp.Fichier)
                    apic.ScaleMode = vbPixels
                    '-> on redimensionne l'image
                    StretchBlt apic.hdc, 0, 0, aBmp.Largeur * 1000, aBmp.Hauteur * 1000, apic.hdc, 0, 0, apic.width, apic.height, ScrCopy
                    '-> on ne conserve que la partie interessante
                    StretchBlt Sortie.hdc, PosX, PosY, aBmp.Largeur * 1000, aBmp.Hauteur * 1000, apic.hdc, 0, 0, apic.ScaleWidth * Zoom, apic.ScaleHeight * Zoom, vbSrcCopy
                    Sortie.Refresh
                Else
                    Sortie.PaintPicture apic.Picture, PosX, PosY
                End If
            End If
        End If
    End If
End If
suite:
'-> Dessiner la bordure si necessaire
If aBmp.Contour Then
    hdlBrush = CreateSolidBrush(0)
    aRect.Left = PosX
    aRect.Top = PosY
    aRect.Right = aRect.Left + Sortie.ScaleX(apic.width, 1, 3)
    aRect.Bottom = aRect.Top + Sortie.ScaleY(apic.height, 1, 3)
    FrameRect Sortie.hdc, aRect, hdlBrush
    DeleteObject hdlBrush
End If

If GetProp Then
    If hProp > aRect.Left And hProp < (aRect.Left + Sortie.ScaleX(apic.Image.width * Zoom, 1, 3)) And vProp < (aRect.Top + Sortie.ScaleY(apic.Image.height * Zoom, 1, 3)) And vProp > aRect.Top Then
        strProp = DataFields & Chr(0) & Chr(0)
        strProp = strProp & Chr(0) & aBmp.Fichier & " " & aBmp.Nom & " " & aBmp.SectionName & " isVariable :" & aBmp.isVariable
        curLigProp = tempLigProp
        GetProp = False
    End If
End If


Set apic = Nothing

'-> Renvoyer une valeur de succ�s
PrintBmp = True

Exit Function

GestErr:

    'Call GestError(39, aBmp.SectionName & "|" & aBmp.Nom, aSpool)
    'PrintBmp = False
    MDIMain.StatusBar1.Panels(1).Text = "Error on PrintBmp" & aBmp.SectionName & "|" & aBmp.Nom
    '-> Renvoyer une valeur de succ�s
    PrintBmp = True

End Function

Private Function PrintObjRtf(PositionX As Long, PositionY As Long, _
                        aObject As Object, ByVal DataFields As String, _
                        DebutRangX As Long, DebutRangY As Long, ByRef aSpool As Spool, Optional MargeInterne As Single) As Boolean


'---> Cette fonction imprime un cadre ou une section ( Code RTF + Bordures )

Dim aField() As String
Dim aRtf As RichTextBox
Dim PosX As Long, PosY As Long
Dim i As Integer
Dim Champ As String
Dim RTFValue As String
Dim fr As FORMATRANGE
Dim lTextOut As Long, lTextAmt As Long, Res As Long
Dim aPoint As POINTAPI
Dim aRect As RECT
Dim hdlPen As Long
Dim OldPen As Long
Dim hdlBrush As Long
Dim oldBrush As Long
Dim NbChamp As Integer
Dim IsCadre As Boolean
Dim IsBordure As Boolean
Dim iniformat As FORMAT

On Error GoTo GestErr

Trace "PrintObjRtf", 4

'-> Mettre l'�diteur RTF aux bonnes dimensions
Set aRtf = frmLib.Rtf(aObject.IdRTf)

aRtf.width = frmLib.ScaleX(aObject.Largeur * Zoom, 7, 1)
aRtf.height = frmLib.ScaleY(aObject.Hauteur * Zoom, 7, 1)

'-> R�cup�ration du code RTF
RTFValue = aRtf.TextRTF
If GetProp Then
    Dim strTemp As String
    strTemp = aRtf.Text
End If

If Zoom <> 1 Then aRtf.TextRTF = ZoomRTF(aRtf.TextRTF)

'-> Cr�er une base de champs
If Trim(DataFields) = "" Then
Else
    '-> R�cup�ration du nombre de champs
    NbChamp = NumEntries(DataFields, "^")
    For i = 2 To NbChamp
        Champ = Entry(i, DataFields, "^")
        ReDim Preserve aField(1 To 2, i - 1)
        aField(1, i - 1) = "^" & Mid$(Champ, 1, 4)
        aField(2, i - 1) = RTrim(Mid$(Champ, 5, Len(Champ) - 1))
    Next
    '-> Faire un remplacement des champs par leur valeur
    For i = 1 To NbChamp - 1
        aRtf.TextRTF = Replace(aRtf.TextRTF, aField(1, i), aField(2, i))
    Next
End If

'-> on insere eventuellement le nombre de pages
If InStr(1, aRtf.TextRTF, "<tot:>", vbTextCompare) <> 0 Then
    aRtf.TextRTF = Replace(aRtf.TextRTF, "<tot:>", aSpool.nbPage)
End If

Trace aRtf.TextRTF, 5

'-> Calcul des positions d'impression de l'objet selon le type d'objet � imprimer
If TypeOf aObject Is Section Then
    Select Case aObject.AlignementLeft
        Case 2 'Marge gauche
            PosX = Sortie.ScaleX(aSpool.maquette.MargeLeft * Zoom, 7, 1)
        Case 4 'Centr�
            PosX = Sortie.ScaleX((aSpool.maquette.Largeur * Zoom - aObject.Largeur * Zoom) / 2, 7, 1)
        Case 3 'Marge Droite
            PosX = Sortie.ScaleX(aSpool.maquette.Largeur * Zoom - aObject.Largeur * Zoom - aSpool.maquette.MargeLeft * Zoom, 7, 1)
        Case 5 'Sp�cifi�
            PosX = Sortie.ScaleX(aObject.Left * Zoom, 7, 1)
    End Select
    Select Case aObject.AlignementTop
        Case 1 'Libre
            PosY = Sortie.ScaleY(PositionY, 3, 1)
        Case 2 'Marge haut
            PosY = Sortie.ScaleY(aSpool.maquette.MargeTop * Zoom, 7, 1)
        Case 4 'Centr�
            PosY = Sortie.ScaleY((aSpool.maquette.Hauteur * Zoom - aObject.Hauteur * Zoom) / 2, 7, 1)
        Case 3 'Marge bas
            PosY = Sortie.ScaleY(aSpool.maquette.Hauteur * Zoom - aObject.Hauteur * Zoom - aSpool.maquette.MargeTop * Zoom, 7, 1)
        Case 5 'Sp�cifi�
            PosY = Sortie.ScaleY(aObject.Top * Zoom, 7, 1)
    End Select

    '-> Tenir compte des variations des marges internes
    PosX = PosX - Sortie.ScaleX(MargeX, 3, 1)
    If aObject.AlignementTop <> 1 Then PosY = PosY - Sortie.ScaleY(MargeY, 3, 1)
    
    '-> Setting du rect de dessin
    aRect.Left = Sortie.ScaleX(PosX, 1, 3)
    aRect.Top = Sortie.ScaleY(PosY, 1, 3)
    aRect.Right = Sortie.ScaleX(aObject.Largeur * Zoom, 7, 3) + Sortie.ScaleX(PosX, 1, 3)
    aRect.Bottom = Sortie.ScaleY(aObject.Hauteur * Zoom, 7, 3) + Sortie.ScaleY(PosY, 1, 3)

    '-> Sauvagarder la position de d�but du rang
    DebutRangX = PosX
    DebutRangY = PosY
    
ElseIf TypeOf aObject Is Cadre Then
    '-> Il faut modifier la position X et Y du cadre
    PosX = DebutRangX + Sortie.ScaleX(aObject.Left * Zoom, 7, 1)
    PosY = DebutRangY + Sortie.ScaleY(aObject.Top * Zoom, 7, 1)
    '-> Setting du rect de dessin
    aRect.Left = Sortie.ScaleX(PosX, 1, 3)
    aRect.Top = Sortie.ScaleY(PosY, 1, 3)
    aRect.Right = Sortie.ScaleX(aObject.Largeur * Zoom, 7, 3) + Sortie.ScaleX(PosX, 1, 3)
    aRect.Bottom = Sortie.ScaleY(aObject.Hauteur * Zoom, 7, 3) + Sortie.ScaleY(PosY, 1, 3)
    '-> Indiquer que c'est un cadre
    IsCadre = True
End If

'-> on verifie si l'impression ne deborde pas
If aRect.Right > Sortie.ScaleWidth And Not DebordementTop Then
    If AllowDebordement Then DebordementTop = True
End If

'-> Imprimer le fond de l'objet
If aObject.BackColor <> 999 Then hdlBrush = CreateSolidBrush(aObject.BackColor)
hdlPen = CreatePen(PS_NULL, 1, QBColor(15))

'-> S�lection du pinceau dans le Contexte
oldBrush = SelectObject(Sortie.hdc, hdlBrush)
OldPen = SelectObject(Sortie.hdc, hdlPen)

'-> Dessin de l'objet
If IsCadre Then
    If Not aObject.IsRoundRect Then
        Res = Rectangle(Sortie.hdc, aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
    Else
        '-> Imprimer les bordures
        PrintBordure aObject, aRect
        IsBordure = True
    End If
        
Else
    Res = Rectangle(Sortie.hdc, aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
End If

If GetProp Then
    If hProp > aRect.Left And hProp < aRect.Right And vProp > aRect.Top And vProp < aRect.Bottom Then
        strProp = Chr(0) & strTemp & Chr(0) & Chr(0) & aRtf.Text
        curLigProp = tempLigProp
        'getProp = False
    End If
End If



'iniformat.crBackColor = aObject.BackColor
'iniformat.dwMask = CFM_BACKCOLOR
'iniformat.cbSize = Len(iniformat)
'SendMessage aRtf.hWnd, EM_SETCHARFORMAT, SCF_ALL, iniformat
'-> on rend le fond du rtf transparent pour eviter les problemes d'impression cadre blanc
Const GWL_EXSTYLE = (-20)
Const WS_EX_TRANSPARENT = &H20&
SetWindowLong aRtf.Hwnd, GWL_EXSTYLE, WS_EX_TRANSPARENT

'-> Redessiner le texte de la cellule si on recherche du texte
If FindTop Then
    If InStr(1, aRtf.Text, FindText, vbTextCompare) <> 0 Then
        'on compte le nombre d'occurence dans la chaine
        i = NumEntries(UCase(aRtf.Text), UCase(FindText)) - 1
        FindPos2 = FindPos2 + i
        '-> on regarde si c'est bien celui que l'on veut dessiner
        If FindPage = aSpool.CurrentPage And FindPos < FindPos2 Or FindPage < aSpool.CurrentPage Then
            FindPos2 = FindPos2 - i + 1
            '-> on pointe sur la premiere occurence du texte recherch�
            aRtf.SelStart = aRtf.find(FindText)
            '-> Dans le cas ou on a plusieurs occurences, selectionner le bon texte
            Do While FindPos2 <= FindPos
                FindPos2 = FindPos2 + 1
                aRtf.SelStart = aRtf.find(FindText, aRtf.SelStart + Len(FindText))
            Loop
            aRtf.SelLength = Len(FindText)
            '-> On met en evidence le texte recherch�
            aRtf.SelColor = vbWhite
            Call SetBackColorSel(aRtf.Hwnd, vbBlack)
            FindPos = FindPos2
            FindPage = aSpool.CurrentPage
            FindTop = False
            '-> assigner une valeur au scroll
            FindScrollH = aRect.Left
            FindScrollV = aRect.Top
        End If
    End If
End If

'-> S�lectionner les anciens objets et supprimer les objets GDI non utilis�s
If OldPen <> 0 Then
    SelectObject Sortie.hdc, OldPen
    DeleteObject hdlPen
End If

If oldBrush <> 0 Then
    SelectObject Sortie.hdc, oldBrush
    DeleteObject hdlBrush
End If

'-> Initialiser la srtucture Formatrange
fr.hdc = Sortie.hdc
fr.hdcTarget = Sortie.hdc
fr.chrg.cpMin = 0
fr.chrg.cpMax = -1

'-> Intialisation du rectangle destination
fr.rc.Left = Sortie.ScaleX(MargeInterne, 7, 1) + PosX
fr.rc.Top = Sortie.ScaleX(MargeInterne, 7, 1) + PosY
fr.rc.Right = Sortie.ScaleX(aObject.Largeur * Zoom, 7, 1) + PosX - Sortie.ScaleX(MargeInterne, 7, 1)
fr.rc.Bottom = Sortie.ScaleY(aObject.Hauteur * 2 * Zoom, 7, 1) + PosY - Sortie.ScaleY(MargeInterne, 7, 1)

'-> Initialisation du rectangle de source
fr.rcPage.Left = 0
fr.rcPage.Top = 0
fr.rcPage.Right = Sortie.ScaleX(aObject.Largeur * Zoom, 7, 1)
fr.rcPage.Bottom = Sortie.ScaleY(aObject.Hauteur * Zoom, 7, 1)

'-> Faire un setting du mode de restitution
Res = SetMapMode(aRtf.Hwnd, MM_TEXT)

'-> initialisation des variables de pointage de texte
lTextOut = 0
lTextAmt = SendMessage(aRtf.Hwnd, WM_GETTEXTLENGTH, 0, 0)

'-> Impression du Rtf
Do While lTextOut < lTextAmt
    lTextOut = SendMessage(aRtf.Hwnd, EM_FORMATRANGE, -1, fr)
    If lTextOut < lTextAmt Then
        fr.chrg.cpMin = lTextOut
        fr.chrg.cpMax = -1
    End If
Loop

'-> Lib�rer la ressource associ�e au RTF : VERSION COMPILEE
Res = SendMessage(aRtf.Hwnd, EM_FORMATRANGE, -1, Null)
If Res = 0 Then
    'MsgBox "Erreur dans la lib�ration du context"
    Res = SendMessage(aRtf.Hwnd, EM_FORMATRANGE, -1, vbNullString)
    'MsgBox "Apr�s Seconde tentative " & Res
End If

'-> VERSION INTERPRETEE testtttttttttttt
'Res = SendMessage(aRtf.hWnd, EM_FORMATRANGE, False, ByVal CLng(0))

'-> Imprimer les bordures
If Not IsBordure Then PrintBordure aObject, aRect

'-> Redonner son vrai contenu au controle RTF
aRtf.TextRTF = RTFValue
aRtf.Refresh

'-> Lib�rer le pointeur sur le controle RTF
Set aRtf = Nothing

'-> Renvoyer une valeur de suvv�s
PrintObjRtf = True

Exit Function

GestErr:

    If Err.Number = 11 Then
        PrintObjRtf = True
        Exit Function
    End If

    Call GestError(38, aObject.Nom, aSpool)
    PrintObjRtf = False
    


End Function

Private Function ZoomRTF(TextRTF As String) As String
'--> cette fonction effectue un zoom proportionnel sur un rtf
Dim i As Integer
Dim strTaille As String
Dim taille As Integer
Dim StrTabulation As String
Dim Decalage As Integer

i = 1

'-> on gere ici la taille de la police
Do While InStr(i, TextRTF, "\fs") <> 0
    '-> on reccupere la taille
    strTaille = Mid(TextRTF, InStr(i, TextRTF, "\fs") + 3, 2)
    If val(strTaille) <> 0 Then
        taille = Mid(TextRTF, InStr(i, TextRTF, "\fs") + 3, 2)
        i = InStr(i, TextRTF, "\fs") + 1
        '-> on applique le zoom
        If Zoom < 1 Then
            taille = taille * Zoom * 0.9 'empirique mais aide bien
        Else
            taille = taille * Zoom
        End If
        '-> on modifie la taille dans le rtf
        TextRTF = Mid(TextRTF, 1, i - 2) & "\fs" & taille & Mid(TextRTF, i + 4)
    Else
        i = i + 1
    End If
Loop

'-> on gere ici les tabulations
For i = 1 To 10
    StrTabulation = StrTabulation & "\tx" & CInt(val(710 * i * Zoom))
Next
TextRTF = Replace(TextRTF, "\pard", "\pard" + StrTabulation)

'-> on gere ici l'espace interligne avant et apres
Do While InStr(i, TextRTF, "\sb") <> 0
    '-> on reccupere la taille
    strTaille = Mid(TextRTF, InStr(i, TextRTF, "\sb") + 3, 3)
    Decalage = 3
    If val(strTaille) <> 0 Then
        If InStr(1, strTaille, "\") Then Decalage = 2
        taille = Mid(TextRTF, InStr(i, TextRTF, "\sb") + 3, Decalage)
        i = InStr(i, TextRTF, "\sb") + 1
        '-> on applique le zoom
        taille = taille * Zoom
        '-> on modifie la taille dans le rtf
        TextRTF = Mid(TextRTF, 1, i - 2) & "\sb" & taille & Mid(TextRTF, i + 2 + Decalage)
    Else
        i = i + 1
    End If
Loop

i = 1
  Do While InStr(i, TextRTF, "\sa") <> 0
    '-> on reccupere la taille
    strTaille = Mid(TextRTF, InStr(i, TextRTF, "\sa") + 3, 3)
    Decalage = 3
    If val(strTaille) <> 0 Then
        If InStr(1, strTaille, "\") Then Decalage = 2
        taille = Mid(TextRTF, InStr(i, TextRTF, "\sa") + 3, Decalage)
        i = InStr(i, TextRTF, "\sa") + 1
        '-> on applique le zoom
        taille = taille * Zoom
        '-> on modifie la taille dans le rtf
        TextRTF = Mid(TextRTF, 1, i - 2) & "\sa" & taille & Mid(TextRTF, i + 2 + Decalage)
    Else
        i = i + 1
    End If
Loop

ZoomRTF = TextRTF

End Function

Private Sub PrintBordure(aObject As Object, aRect As RECT)

Dim Temp1 As Long
Dim Temp2 As Long
Dim ClipRgn As Long
Dim OldClip As Long
Dim Pix1X As Long
Dim Pix1Y As Long
Dim aPoint As POINTAPI

Dim hdlBrush As Long
Dim oldBrush As Long
Dim hdlPen As Long
Dim OldPen As Long
Dim Res As Long

On Error Resume Next

'-> Dessin du contour de l'objet si necessaire
If TypeOf aObject Is Section Then
    If aObject.Contour Then
        hdlBrush = CreateSolidBrush(0)
        oldBrush = SelectObject(Sortie.hdc, hdlBrush)
        FrameRect Sortie.hdc, aRect, hdlBrush
    End If
ElseIf TypeOf aObject Is Cadre Then


    '-> Tenir compte de la largeur du trait
    Temp2 = frmLib.ScaleX(aObject.LargeurTrait, 3, 1)
    Temp2 = Sortie.ScaleX(Temp2, 1, 3)

    Pix1X = frmLib.ScaleX(1, 3, 1)
    Pix1X = Sortie.ScaleX(Pix1X, 1, 3)

    Pix1Y = frmLib.ScaleY(1, 3, 1)
    Pix1Y = Sortie.ScaleY(Pix1Y, 1, 3)

    '-> cr�er un stylo de dessin
    hdlPen = CreatePen(PS_SOLID, Temp2, 0)
    OldPen = SelectObject(Sortie.hdc, hdlPen)
    
    '-> Cr�er le fond
    hdlBrush = CreateSolidBrush(aObject.BackColor)
    oldBrush = SelectObject(Sortie.hdc, hdlBrush)
    
    '-> S�lectionner le rectangle comme zone de clipping
    ClipRgn = CreateRectRgn(aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
    SelectClipRgn Sortie.hdc, ClipRgn

    If aObject.IsRoundRect Then
        If Temp2 / 2 <> Fix(Temp2 / 2) Then Temp2 = Temp2 + 1
        
        Pix1X = frmLib.ScaleX(50, 3, 1)
        Pix1X = Sortie.ScaleX(Pix1X, 1, 3)

        Pix1Y = frmLib.ScaleY(50, 3, 1)
        Pix1Y = Sortie.ScaleY(Pix1Y, 1, 3)
        Res = RoundRect(Sortie.hdc, aRect.Left + Temp2 / 2, aRect.Top + Temp2 / 2, aRect.Right - Temp2 / 2, aRect.Bottom - Temp2 / 2, Pix1X, Pix1Y)
    Else
    
        '-> Bordure gauche
        If aObject.Gauche Then
            '-> Dessin des bordures
            Temp1 = Fix(Temp2 / 2)
            MoveToEx Sortie.hdc, aRect.Left + Temp1, aRect.Top, aPoint
            LineTo Sortie.hdc, aRect.Left + Temp1, aRect.Bottom - 1
        End If
        
        '-> Bordure haut
        If aObject.Haut Then
            Temp1 = Fix(Temp2 / 2)
            MoveToEx Sortie.hdc, aRect.Left, aRect.Top + Temp1, aPoint
            LineTo Sortie.hdc, aRect.Right, aRect.Top + Temp1
        End If
        
        '-> Bordure Droite
        If aObject.Droite Then
            If (Temp2 / 2) = Fix(Temp2 / 2) Then
                Temp1 = Temp2 / 2
            Else
                Temp1 = (Temp2 + 1) / 2
            End If
            MoveToEx Sortie.hdc, aRect.Right - Temp1, aRect.Top, aPoint
            LineTo Sortie.hdc, aRect.Right - Temp1, aRect.Bottom
        End If
        
        '-> Bordure Bas
        If aObject.Bas Then
            If (Temp2 / 2) = Fix(Temp2 / 2) Then
                Temp1 = Temp2 / 2
            Else
                Temp1 = (Temp2 + 1) / 2
            End If
            MoveToEx Sortie.hdc, aRect.Left, aRect.Bottom - Temp1, aPoint
            LineTo Sortie.hdc, aRect.Right, aRect.Bottom - Temp1
        End If
        
    End If
    
    '-> Restituer la zone de clipping
    OldClip = CreateRectRgn(0, 0, Sortie.ScaleWidth, Sortie.ScaleHeight)
    SelectClipRgn Sortie.hdc, OldClip
    DeleteObject ClipRgn
    DeleteObject OldClip
End If
 
'-> S�lectionner les anciens objets et supprimer les objets GDI non utilis�s
If OldPen <> 0 Then
    SelectObject Sortie.hdc, OldPen
    DeleteObject hdlPen
End If

If oldBrush <> 0 Then
    SelectObject Sortie.hdc, oldBrush
    DeleteObject hdlBrush
End If


End Sub



Public Function FormatNumTP(ByVal ToFormat As String, ByVal Msk As String, Optional NomBlock As String, Optional Cell As String) As FormatedCell

'---> Cette fonction a pour but d'analyser une chaine de caract�re et de la retouner
'formatt�e en s�parateur de milier et en nombre de d�cimal

Dim strFormat As String
Dim strtempo As String
Dim strPartieEntiere As String
Dim StrPartieDecimale As String
Dim i As Integer, j As Integer
Dim nbDec As Integer
Dim strSep As String
Dim idNegatif As Integer
Dim Masque As String
Dim Tempo As String
Dim FindSepDec As Boolean
Dim sTemp As String
Dim sTemp2 As String

On Error GoTo GestErr

sTemp = Trim(ToFormat)
sTemp2 = Trim(ToFormat)
'si on a un msk � 2 decimale et un le dernier

For i = Len(ToFormat) To 1 Step -1
    If IsNumeric(Mid$(ToFormat, i, 1)) Then
        Tempo = Mid$(ToFormat, i, 1) & Tempo
    Else
        '-> Gestion du s�parateur d�cimal
        If Mid$(ToFormat, i, 1) = "." Or Mid$(ToFormat, i, 1) = "," Then
            If Not FindSepDec Then
                Tempo = SepDec & Tempo
                FindSepDec = True
            End If
        Else
            Tempo = Mid$(ToFormat, i, 1) & Tempo
        End If
    End If
Next 'Pour tous les caract�res

ToFormat = Tempo

If Trim(ToFormat) = "" Then
Else
    '-> Tester si on envoie une zone num�rique
    If Not IsNumeric(ToFormat) Then
        FormatNumTP.Ok = True
        FormatNumTP.strFormatted = sTemp2
        Exit Function
    End If
End If

'-> Charger le masque de la cellule
nbDec = CInt(Entry(1, Msk, "�"))
If Entry(2, Msk, "�") <> "" Then
    strSep = SepMil 'Entry(2, Msk, "�")
Else
    strSep = ""
End If
idNegatif = CInt(Entry(3, Msk, "�"))

FormatNumTP.Ok = True
FormatNumTP.nbDec = nbDec
FormatNumTP.Value = CDbl(ToFormat)
FormatNumTP.idNegatif = idNegatif

'-> Analyse si d�cimale
If nbDec = 0 Then
    '-> Arrondir
    strtempo = CStr(CDbl(ToFormat))
    Masque = "#########################################0"
Else
    Masque = "#########################################0." & String(nbDec, "0")
    strtempo = ToFormat
End If

'-> Construction d'un masque assez grand pour formatter n'importe qu'elle zone
strtempo = FORMAT(Abs(strtempo), Masque)

If SepMil = SepDec Then
    SepMil = " "
End If

'-> Construction de la partie enti�re
If nbDec <> 0 Then
    strPartieEntiere = Mid$(strtempo, 1, InStr(1, strtempo, SepDec) - 1)
    StrPartieDecimale = SepDec & Mid$(strtempo, InStr(strtempo, SepDec) + 1, nbDec)
Else
    strPartieEntiere = strtempo
    StrPartieDecimale = ""
End If

j = 1
For i = Len(strPartieEntiere) To 1 Step -1
    strFormat = Mid$(strtempo, i, 1) & strFormat
    If j = 3 And i <> 1 Then
        strFormat = strSep & strFormat
        j = 1
    Else
        j = j + 1
    End If
Next

FormatNumTP.strFormatted = strFormat & StrPartieDecimale

If Trim(FormatNumTP.strFormatted) = "" Then FormatNumTP.strFormatted = sTemp2

Exit Function

GestErr:
    FormatNumTP.Ok = True
    FormatNumTP.strFormatted = sTemp2
    
End Function

Public Function PrintTableau(ByRef NomObjet As String, ByRef Param As String, _
                         ByRef DataFields As String, ByRef PositionX As Long, _
                         ByRef PositionY As Long, ByRef NomSousObjet As String, ByRef FirstObj As Boolean, ByRef aSpool As Spool) As Boolean

Dim aTb As Tableau
Dim nLig As Integer


On Error GoTo GestError

Trace "PrintTableau " & NomObjet, 2
Trace DataFields, 5

'-> Pointer sur le tableau pass� en argument
Set aTb = aSpool.maquette.Tableaux(UCase$(NomObjet))

'-> R�cup�ration de la ligne de tableau � imprimer
nLig = CInt(Entry(2, Param, "\"))

'-> Imprimer le block de ligne
PositionY = PrintBlock(NomSousObjet, Param, DataFields, PositionX, PositionY, aTb, nLig, aSpool)

'-> Quitter si valeur d'erreur
If PositionY = -9999 Then GoTo GestError

'-> Renvoyer une valeur de succ�s
PrintTableau = True

Exit Function

GestError:
    Call GestError(37, aTb.Nom, aSpool)
    PrintTableau = False


End Function

Public Function GetPictureAssociation(ValueToSearch As String) As String

Dim Res As Long
Dim lpBuffer As String

On Error Resume Next

'-> reccuperer le tm_picture.ini si besoin
If VersionTurbo = 1 And Tm_PictureIniFile = "" Then
    TelechargeFile TurboGraphWebFile & "/deallogo/Tm_Picture.txt", App.Path & "\Tm_Picture.ini"
    Tm_PictureIniFile = App.Path & "\Tm_Picture.ini"
    'If Not IsFile(Tm_PictureIniFile) Then Tm_PictureIniFile = ""
End If
'-> V�rifier que l'on ait bien acc�s au fichier TM_Picture.ini
'If Tm_PictureIniFile = "" Then Exit Function

lpBuffer = Space$(5000)
Res = GetPrivateProfileString("IMAGES", ValueToSearch, "", lpBuffer, Len(lpBuffer), Tm_PictureIniFile)
If Res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Res)
    '-> Cr�er la liste des mots cl�s
    GetPathMotCles
    '-> Faire le remplacement
    ReplacePathMotCles lpBuffer
    GetPictureAssociation = lpBuffer
End If

Exit Function

GestError:
    GetPictureAssociation = ""

End Function

Public Sub CloseFichier(ByVal FileToclose As String)

'---> Cette d�charge le fichier sp�cifi�

Dim aFichier As Fichier
Dim aSpool As Spool


On Error Resume Next

'-> Pointer sur le fichier sp�cifi�
Set aFichier = Fichiers(UCase$(Trim(FileToclose)))

'-> supprimer toutes les pages actives
For Each aSpool In aFichier.Spools
    If Not aSpool.frmdisplay Is Nothing Then Unload aSpool.frmdisplay
Next

'-> Supprimer l'objet de la collection des fichiers
Fichiers.Remove (UCase$(Trim(FileToclose)))

'-> Supprimer les nodes
MDIMain.TreeNaviga.Nodes.Remove (UCase$(Trim(FileToclose)))

End Sub



Public Sub DrawWait()

'---> Proc�dure qui dessine la temporisation

Dim hdlBrush As Long
Dim hdlPen As Long
Dim oldBrush As Long
Dim OldPen As Long
Dim Res As Long
Dim hdc As Long
Dim StartPanel As Long

On Error Resume Next

'-> Get du Dc du pannel
hdc = GetDC(MDIMain.StatusBar1.Hwnd)

'-> Cr�ation des objets GDI pour dessin des cellules
hdlBrush = CreateSolidBrush(&HFF8080)
hdlPen = CreatePen(PS_NULL, 1, 0)

'-> S�lection du pinceau dans le Contexte
oldBrush = SelectObject(hdc, hdlBrush)

'-> Dessin de la cellule
OldPen = SelectObject(hdc, hdlPen)

'-> Depart du dessin
StartPanel = frmLib.ScaleX(MDIMain.StatusBar1.Panels(1).width, 1, 3) + 4

'-> Dessin du rectangle
Res = Rectangle(hdc, StartPanel, 3, StartPanel + ((TailleLue / TailleTotale) * (frmLib.ScaleX(MDIMain.StatusBar1.Panels(2).width, 1, 3) - 2)), 17)

'-> Liberer les objets GDI
SelectObject hdc, oldBrush
SelectObject hdc, OldPen
DeleteObject hdlPen
DeleteObject hdlBrush
ReleaseDC MDIMain.StatusBar1.Hwnd, hdc

End Sub
    
Private Sub LoadInternetMessProg()

Dim aLb As Libelle
Dim strLib As String

Select Case IndexLangue
    Case "1"
        '********************
        '* MESSPROG MDIMAIN *
        '********************
        
        Set aLb = New Libelle
        strLib = "1=&Fichier" & Chr(0) & "2=&Ouvrir" & Chr(0) & "3=&Imprimer" & Chr(0) & _
                 "4=&Quitter" & Chr(0) & "5=F&en�tre" & Chr(0) & "6=&Navigation" & Chr(0) & _
                 "7=Mosa�que &horizontale" & Chr(0) & "8=Mosa�que &verticale" & Chr(0) & _
                 "9=&En cascade" & Chr(0) & "10=Visionneuse DEAL INFORMATIQUE." & Chr(0) & _
                 "11=Erreur sur le fichier." & Chr(0) & "12=Page N� : " & Chr(0) & "13=Fermer le fichier" & Chr(0) & _
                 "14=Imprimer" & Chr(0) & "15=Propri�t�s" & Chr(0) & "16=Envoyer Vers" & Chr(0) & _
                 "17=Internet" & Chr(0) & "18=Messagerie" & Chr(0) & "19=Editeur de texte" & Chr(0) & _
                 "20=&R�organiser les icones" & Chr(0) & "21=A propos de la visionneuse DEAL Informatique" & Chr(0) & _
                 "22=Supprimer" & Chr(0) & "23=D�finitivement" & Chr(0) & "24=Envoyer � la poubelle" & Chr(0) & "25=Le fichier existe d�ja : d�sirez-vous l'�craser ?�Confirmation" & Chr(0) & _
                 "26=V�rifier les mises � jour" & Chr(0) & "27=Eclatement/Regroupement" & Chr(0) & "28=" & Chr(0) & "29=Configuration" & Chr(0) & "30=Supprimer les fichiers d'origine" & Chr(0) & "31=Restaurer dans les r�pertoires d'origines" & Chr(0) & _
                 "32=Imprimer tous les fichiers" & Chr(0) & "33=Par d�faut les spools ouverts sont conserv�s dans 'mes documents/mes spools'. Au bout de combien de jours souhaitez vous que l'application les supprime?" & Chr(13) & "Ne rien mettre pour que l'application ne supprime pas d'elle m�me les spools." & _
                 Chr(0) & "34=Combien de jours conserver les spools?" & Chr(0) & "35=Fermer tous les fichiers" & Chr(0) & "36=Mode �coute"
                 
        
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "MDIMAIN"
        
        '******************
        '* Ac�s au d�tail *
        '******************
        
        Set aLb = New Libelle
        strLib = "1=Impossible de trouver le fichier d�tail sp�cifi�D�tail" & Chr(0) & "2=Cr�ation de l'acc�s au d�tail en cours..." & _
                  Chr(0) & "3=Acc�s au d�tail"
        
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "ACCESDET"
        
        '********************
        '* MESSPROG FRMMAIL *
        '********************
        Set aLb = New Libelle
        strLib = "1=Envoyer un fichier" & Chr(0) & "2=G�n�ral" & Chr(0) & "3=Envoyer la page en cours" & Chr(0) & _
                 "4=Envoyer le spool en entier" & Chr(0) & "5=Crypter le fichier" & Chr(0) & "6=Internet" & Chr(0) & _
                 "7=OutLook" & Chr(0) & "8=Destinataire" & Chr(0) & "9=Sujet" & Chr(0) & "10=Message" & Chr(0) & _
                 "11=Envoyer" & Chr(0) & "12=Police" & Chr(0) & "13=Couleur" & Chr(0) & "14=Aligner � gauche" & Chr(0) & _
                 "15=Centrer" & Chr(0) & "16=Aligner � droite" & Chr(0) & "17=Veuillez saisir le destinataire du message�Erreur" & Chr(0) & _
                 "18=Envoyer le fichier en entier" & Chr(0) & "19=Options" & Chr(0) & "20=Format du fichier" & Chr(0) & _
                 "21=Utiliser le format Turbo" & Chr(0) & "22=Utiliser le format HTML" & Chr(0) & "23=Navigation" & Chr(0) & _
                 "24=Cr�er une page de navigation" & Chr(0) & "25=G�n�rer un seul fichier contenant toutes les pages" & Chr(0) & _
                 "26=G�n�rer de simples pages" & Chr(0) & "27=Inclure le num�ro du spool dans le nom des pages" & Chr(0) & _
                 "28=Int�grer le spool dans le corps du mail" & Chr(0) & "29=Utiliser le format PDF" & Chr(0) & _
                 "30=Pas de pi�ce jointe" & Chr(0) & "31=Lotus Notes" & Chr(0) & "32=Copies" & Chr(0) & _
                 "33=A :" & Chr(0) & "34=Cc :" & Chr(0) & "35=Obj :" & Chr(0) & "36=Envoi multiple de courriel" & Chr(0) & "37=Destinataires" & Chr(0) & "38=Envoyer" & Chr(0) & "39=Message" & _
                 Chr(0) & "40=Pi�ces jointes" & Chr(0) & "41=Format des fichiers joints" & Chr(0) & "42=Destinataires des messages" & Chr(0) & "43=Message commun � envoyer" & Chr(0) & "44=Cette fonctionnalit� n�cessite Microsoft OutLook" & Chr(0) & "45=Cette �dition n'a pas �t� configur�e pour le mailing" & Chr(0) & "46=Imprimer" & _
                 Chr(0) & "47=Imprimer les destinataires s�lectionn�s" & Chr(0) & "48=Imprimer les destinataires non s�lectionn�s" & Chr(0) & "49=Nom du fichier PDF :" & _
                 Chr(0) & "50=Utiliser le serveur de messagerie" & Chr(0) & "51=De : " & Chr(0) & "52=Obj : " & Chr(0) & "53=Corps : " & Chr(0) & _
                 "54=Envoyer par votre messagerie par d�faut" & Chr(0) & "55=Envoyer en utilisant votre compte Outlook" & Chr(0) & "56=Envoyer en utilisant votre compte Lotus Notes" & Chr(0) & "57=Envoyer en utilisant votre serveur de messagerie" & Chr(0) & _
                 "58=Envoyer par votre messagerie par d�faut" & Chr(0) & "59=Envoyer par Outlook/Lotus Notes" & Chr(0) & "60=Envoyer par un serveur de messagerie" & Chr(0) & "61=Serveur de messagerie" & Chr(0) & "62=Configuration du serveur smtp manquante" & Chr(0) & "63=Regrouper les adresses identiques" & Chr(0) & "64=<option>" & Chr(0) & "65=Regrouper par libell�s" _
                 & Chr(0) & "66=Regrouper par fichier"
                 
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMMAIL"
                 
        '********************
        '* MESSPROG FRMPUBLIPOST *
        '********************
        Set aLb = New Libelle
        strLib = "1=Envoyer un fichier" & Chr(0) & "2=G�n�ral" & Chr(0) & "3=Envoyer la page en cours" & Chr(0) & _
                 "4=Envoyer le spool en entier" & Chr(0) & "5=Crypter le fichier" & Chr(0) & "6=Internet" & Chr(0) & _
                 "7=OutLook" & Chr(0) & "8=Ruptures" & Chr(0) & "9=Sujet" & Chr(0) & "10=Ruptures par fichier" & Chr(0) & _
                 "11=Envoyer" & Chr(0) & "12=Police" & Chr(0) & "13=Couleur" & Chr(0) & "14=Aligner � gauche" & Chr(0) & _
                 "15=Centrer" & Chr(0) & "16=Aligner � droite" & Chr(0) & "17=Veuillez saisir le destinataire du message�Erreur" & Chr(0) & _
                 "18=Envoyer le fichier en entier" & Chr(0) & "19=Options" & Chr(0) & "20=Format du fichier" & Chr(0) & _
                 "21=Utiliser le format Turbo" & Chr(0) & "22=Utiliser le format HTML" & Chr(0) & "23=Navigation" & Chr(0) & _
                 "24=Cr�er une page de navigation" & Chr(0) & "25=G�n�rer un seul fichier contenant toutes les pages" & Chr(0) & _
                 "26=G�n�rer de simples pages" & Chr(0) & "27=Inclure le num�ro du spool dans le nom des pages" & Chr(0) & _
                 "28=Int�grer le spool dans le corps du mail" & Chr(0) & "29=Utiliser le format PDF" & Chr(0) & _
                 "30=Pas de pi�ce jointe" & Chr(0) & "31=Lotus Notes" & Chr(0) & "32=Copies" & Chr(0) & _
                 "33=A :" & Chr(0) & "34=Cc :" & Chr(0) & "35=Obj :" & Chr(0) & "36=Eclatement/Regroupement" & Chr(0) & "37=Eclatement/Regroupement" & Chr(0) & "38=G�n�rer" & Chr(0) & "39=Ruptures" & _
                 Chr(0) & "40=Param�trage" & Chr(0) & "41=Format des fichiers joints" & Chr(0) & "42=Ruptures des pages" & Chr(0) & "43=Choix des ruptures" & Chr(0) & "44=Cette fonctionnalit� n�cessite Microsoft OutLook" & Chr(0) & "45=Choix de rupture erron�!" & Chr(0) & "46=Options" & _
                 Chr(0) & "47=Imprimer les destinataires s�lectionn�s" & Chr(0) & "48=Imprimer les destinataires non s�lectionn�s" & Chr(0) & "49=Nom du fichier PDF :" & _
                 Chr(0) & "50=Utiliser le serveur de messagerie" & Chr(0) & "51=De : " & Chr(0) & "52=Obj : " & Chr(0) & "53=Corps : " & Chr(0) & _
                 "54=Envoyer par votre messagerie par d�faut" & Chr(0) & "55=Envoyer en utilisant votre compte Outlook" & Chr(0) & "56=Envoyer en utilisant votre compte Lotus Notes" & Chr(0) & "57=Envoyer en utilisant votre serveur de messagerie" & Chr(0) & _
                 "58=Envoyer par votre messagerie par d�faut" & Chr(0) & "59=Envoyer par Outlook/Lotus Notes" & Chr(0) & "60=Envoyer par un serveur de messagerie" & Chr(0) & "61=Serveur de messagerie" & Chr(0) & "62=Configuration du serveur smtp manquante" & Chr(0) & "63=Regrouper les adresses identiques" & Chr(0) & "64=<option>" & Chr(0) & "65=Regrouper par libell�s"
                 
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMPUBLIPOST"
        
        '*************************
        '* MESSPROG FRMINTERNET  *
        '*************************
                 
        Set aLb = New Libelle
        strLib = "1=Exporter au format HTML" & Chr(0) & "2=Export :" & Chr(0) & "3=Exporter la page en cours" & Chr(0) & _
                 "4=Exporter le spool en entier" & Chr(0) & "5=Options : " & Chr(0) & "6=Cr�er une page de navigation" & Chr(0) & _
                 "7=Lancer � la fin du traitement" & Chr(0) & "8=Traitement : " & Chr(0) & "9=Cr�ation de la page : " & Chr(0) & _
                 "10=Exporter" & Chr(0) & "11=Navigation : " & Chr(0) & "12=Utiliser une barre de navigation" & Chr(0) & "13=Utiliser une liste de pages" & Chr(0) & _
                 "14=Exporter le fichier en entier" & Chr(0) & "15=Inclure le num�ro du spool dans le nom des pages" & Chr(0) & _
                 "16=G�n�rer un seul fichier contenant toutes les pages" & Chr(0) & "17=G�n�rer de simples pages" & Chr(0) & _
                 "18=Fichier : " & Chr(0) & "19=R�pertoire d'export" & Chr(0) & "20=Nom du fichier � g�n�rer" & Chr(0) & _
                 "21=Nom de fichier incorrect" & Chr(0) & "22=Exporter les bordures"
                 
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMINTERNET"
                          
        '*************************
        '* MESSPROG FRMVISUSPOOL *
        '*************************
        
        Set aLb = New Libelle
        strLib = "1=Page de s�lection" & Chr(0) & "2=Premi�re page" & Chr(0) & "3=Page pr�c�dente" & Chr(0) & _
                 "4=Atteindre une page" & Chr(0) & "5=Page suivante" & Chr(0) & "6=Derni�re page" & Chr(0) & _
                 "7=Imprimer" & Chr(0) & "8=Envoyer un message" & Chr(0) & "9=Exporter au format HTML" & Chr(0) & _
                 "10=Aide" & Chr(0) & "11=Spool : � du fichier : " & Chr(0) & "12=Imprimer le fichier" & Chr(0) & _
                 "13=Imprimer le spool" & Chr(0) & "14=Imprimer la page" & Chr(0) & "15=Exporter au format PDF" & _
                 Chr(0) & "16=Rechercher" & Chr(0) & "17=Rechercher le suivant" & Chr(0) & "18=Exporter au format OpenOffice" & _
                 Chr(0) & "19=Acrobat PdfWriter Distiller, Win2pdf, PdfCreator" & Chr(0) & "20=Courriel" & Chr(0) & "21=Pi�ces jointes" & Chr(0) & "22=Ajuster" & _
                 Chr(0) & "23=Exporter au format EXCEL" & Chr(0) & "24=Enregistrer sous" & Chr(0) & "25=Gestion des spools" & Chr(0) & "26=Insertion de notes"
                 
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMVISUSPOOL"
                 
        '*************************
        '* MESSPROG UPDATE *
        '*************************
        
        Set aLb = New Libelle
        strLib = "1=Une nouvelle mise � jour est disponible." & Chr(0) & "2=Installer la mise � jour ?" & Chr(0) & "3=Vous poss�dez d�j� la version la plus r�cente du logiciel." & Chr(0) & _
                 "4=Impossible de t�l�charger la mise � jour !" & Chr(0) & "5=Impossible de v�rifier la disponibilit� des mises � jour" & Chr(0) & "6=Le programme va �tre ferm� pour pouvoir lancer l'installation." & Chr(0) & _
                 "7=Voulez-vous continuer ?" & Chr(0) & "8=Impossible de r�cup�rer la mise � jour !" & Chr(0) & "9=V�rifiez que vous �tes bien connect� � Internet et r�essayez plus tard !" & Chr(0) & _
                 "10=Attention" & Chr(0) & "11=Appuyez sur 'Non' pour ignorer d�finitivement les mises � jour" & Chr(0) & "Le t�l�chargement pourra prendre quelques instant..." & Chr(0) & "12=Imprimer le fichier"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "UPDATE"
                          
        '**********************
        '* MESSPROG STATUSBAR *
        '**********************
        Set aLb = New Libelle
        strLib = "1=Lecture du fichier $FICHIER$ en cours ..." & Chr(0) & "2=Initialisation des maquettes ..." & Chr(0) & _
                 "3=Lecture de la page en cours ..."
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "STATUSBAR"
        
        '*******************
        '* MESSPROG ERRORS *
        '*******************
        Set aLb = New Libelle
        strLib = "1=" & Chr(0) & _
                 "2=Impossible d'ouvrir le fichier $FICHIER$ pour lecture." & Chr(0) & _
                 "3=Erreur lors de la cr�ation de l'objet de la classe Fichier pour le fichier $FICHIER$." & Chr(0) & _
                 "4=Impossible de lire le fichier ASCII apr�s ouverture." & Chr(0) & "5=" & Chr(0) & _
                 "6=Impossible de trouver un spool dans fichier sp�cifi� $FICHIER$." & Chr(0) & _
                 "7=Impossible de trouver la maquette $MAQ$ du fichier $FICHIER$." & Chr(0) & _
                 "8=Erreur systeme : Impossible de cr�er un fichier temporaire pour lecture de la maquette $MAQ$." & Chr(0) & _
                 "9=Impossible d'ouvrir le fichier temporaire $INI$ de la maquette $MAQ$." & Chr(0) & _
                 "10=Erreur lors de la lecture du fichier temporaire $INI$ de la maquette $MAQ$." & Chr(0) & _
                 "11=Erreur dans la cr�ation d'un objet de type Section : $SECTION$ de la maquette $MAQ$." & _
                 "12=Erreur dansla cr�ation d'un  objet de type Tableau : $TABLEAU$ de la maquette $MAQ$." & Chr(0) & "13=" & Chr(0) & _
                 "14=Erreur dans la cr�ation d'un BMP : $BMP$ de la section : $SECTION$ de la maquette $MAQ$." & Chr(0) & _
                 "15=Erreur dans la cr�ation d'un Cadre : $CADRE$ de la section : $SECTION$ de la maquette $MAQ$." & Chr(0) & _
                 "16=Erreur dans la cr�ation d'un block : $BLOCK$ du tableau $TABLEAU$ de la maquette $MAQ$." & Chr(0) & _
                 "17=Impossible de cr�er un fichier temporaire pour un objet RTF." & Chr(0) & _
                 "18=Erreur dans l'initialisation de l'entete de la maquette : " & Chr(0) & _
                 "19=Erreur dans l'initialisation d'un nouveau conteneur BMP." & Chr(0) & _
                 "20=Erreur dans le setting des dimensions d'un objet BMP : $BMP$ de la section : $SECTION$. Hauteur : $HAUTEUR$   Largeur : $LARGEUR$." & Chr(0) & _
                 "21=Erreur dans le setting du propath d'une image." & Chr(0) & _
                 "22=Erreur lors du chargement du fichier : $FICHIER$ du bmp : $BMP$ de la section : $SECTION$." & Chr(0) & _
                 "23=Erreur lors de la cr�ation de la repr�sentation de l'objet cadre $CADRE$ de la section $SECTION$." & Chr(0) & _
                 "24=Erreur lors du chargement du fichier temporaire $FICHIER$ du cadre : $CADRE$ de la section : $SECTION$." & Chr(0) & _
                 "25=Erreur lors du chargement du fichier temporaire $FICHIER$ de la section : $SECTION$." & Chr(0) & _
                 "26=Erreur lors de la cr�ation de la repr�sentation physique de la section : $SECTION$." & Chr(0) & _
                 "27=Erreur lors de l'initialisation de la section $SECTION$ pour la valeur de ligne : $LIG$." & Chr(0)
                 
                 
        strLib = strLib & "28=Erreur lors de l'initialisation de l'entete de l'image : $BMP$ de la section $SECTION$ pour la valeur : $LIG$" & Chr(0) & Chr(0) & _
                          "29=Erreur lors de l'initialisation de l'entete du cadre : $CADRE$ de la section $SECTION$ pour la valeur : $LIG$" & Chr(0) & _
                          "30=Erreur lors de l'initialisation du tableau $TABLEAU$ pour la valeur : $LIG$." & Chr(0) & _
                          "31=Erreur lors de l'initialisation du block $BLOCK$ pour le tableau $TABLEAU$ pour la valeur $LIG$." & Chr(0) & _
                          "32=Erreur lors de la lecture d'une ligne de d�finition du block $BLOCK$ du tableau $TABLEAU$." & Chr(0) & _
                          "33=Erreur lors de la cr�ation des cellule du block $BLOCK$ du tableau $TABLEAU$ pour la valeur $LIG$." & Chr(0) & _
                          "34=Erreur lors de l'initialisation des dimensions des cellules du block : $BLOCK$ du tableau $TABLEAU$" & Chr(0) & _
                          "35=Erreur dans l'initialisation de la page � imprimer" & Chr(0) & _
                          "36=Erreur dans l'initialisation de l'impression de la section : $SECTION$." & Chr(0) & _
                          "37=Erreur dans l'initialisation de l'impression du tableau : $TABLEAU$." & Chr(0) & _
                          "38=Erreur dans l'impression d'un objet RTF : $OBJECT$." & Chr(0) & _
                          "39=Erreur dans l'impression de l'image : $BMP$ de la section : $SECTION$." & Chr(0) & _
                          "40=Erreur dans l'initialisation du block : $BLOCK$ du tableau $TABLEAU$" & Chr(0) & _
                          "41=Erreur dans le dessin de la cellule : $CELLULE$ du block : $BLOCK$ du tableau $TABLEAU$." & Chr(0) & _
                          "42=Erreur durant l'impression du fichier.�Erreur fatale d'impression"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "ERRORS"
        
        '********************
        '* MESSPROG MESSAGE *
        '********************
        Set aLb = New Libelle
        
        strLib = "1=Quitter maintenant ?�Confirmation" & Chr(0) & "2=Fichier d�ja charg�.�Impossible de charger le fichier." & Chr(0) & _
                 "3=Num�ro de page incorrect.�Erreur" & Chr(0) & "4=Impossible de se connecter � Ms OutLook.�Impossible d'envoyer un message." & Chr(0) & _
                 "5=Impossible de cr�er un nouveau message dans Ms OutLook.�Impossible de cr�er un message." & Chr(0) & _
                 "6=Impossible de joinde le fichier sp�cifi� : $FICHIER$.�Erreur." & Chr(0) & _
                 "7=Envoyer la page en cours [Oui] ou le spool en entier [Non] ?�Question" & Chr(0) & _
                 "8=Veuillez saisir le destinataire du message.�Erreur" & Chr(0) & "9=Impossible d'envoyer le message sur Internet.�Erreur" & Chr(0) & _
                 "10=Impossible d'initialiser la session de messagerie.�Erreur" & Chr(0) & "11=Crypter le fichier ?�Question" & Chr(0) & "12=Saisie incorrecte.�Erreur"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "MESSAGE"
        
        '********************
        '* MESSPROG BOUTONS *
        '********************
        Set aLb = New Libelle
        
        strLib = "1=&Ok" & Chr(0) & "2=&Annuler" & Chr(0) & "3=&Envoyer"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "BOUTONS"
        
        '***************
        '* FRMNETSEND  *
        '***************
        Set aLb = New Libelle
        strLib = "1=Envoyer un e-mail" & Chr(0) & "2=Url : " & Chr(0) & "3=Message : " & Chr(0) & _
                 "4=Objet : " & Chr(0) & "5=Carnet d'adresses" & Chr(0) & "6=Crypter le fichier associ� :"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMNETSEND"
            
        '***************
        '* FRMEDITOR  *
        '***************
        Set aLb = New Libelle
        strLib = "1=&Fichier" & Chr(0) & "2=&Ouvrir" & Chr(0) & "3=&Enregistrer" & Chr(0) & _
                 "4=En&registrer sous" & Chr(0) & "5=&Imprimer" & Chr(0) & "6=&Quitter" & Chr(0) & _
                 "7=&Edition" & Chr(0) & "8=Couper" & Chr(0) & "9=Copier" & Chr(0) & _
                 "10=Coller" & Chr(0) & "11=Rechercher" & Chr(0) & "12=A propos de ..." & Chr(0) & _
                 "13=Chercher : " & Chr(0) & "14=Forma&t" & Chr(0) & "15=&Police" & Chr(0) & _
                 "16=Envoie du fichier : " & Chr(0) & "17=Envoie du spool :" & Chr(0) & "18=Envoie de la page : "
                 
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMEDITOR"
        
        '***************
        '* FRMFONT     *
        '***************
        Set aLb = New Libelle
        strLib = "1=Choix d'une police" & Chr(0) & "2=Police :" & Chr(0) & "3=Style de police :" & Chr(0) & _
                 "4=Taille : " & Chr(0) & "5=Normal" & Chr(0) & "6=Italique" & Chr(0) & "7=Gras" & Chr(0) & _
                 "8=Gras Italique"
        
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMFONT"
        
        '**************************
        '* MESSPROG FRMSEARCHPAGE *
        '**************************
        Set aLb = New Libelle
        
        strLib = "1=Atteindre une page" & Chr(0) & "Num�ro de la page :"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMSEARCHPAGE"
        
        '**************************
        '* MESSPROG FRMSEARCHTEXT *
        '**************************
        Set aLb = New Libelle
        
        strLib = "1=Rechercher" & Chr(0) & "2=Rechercher :" & Chr(0) & "3=La cha�ne recherch�e n'a pas �t� trouv�e�Erreur"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMSEARCHTEXT"
        
        '*************************
        '* MESSPROG D'IMPRESSION *
        '*************************
        Set aLb = New Libelle
        strLib = "1=S�lection d'une imprimante" & Chr(0) & "2=Nom : " & Chr(0) & "3=Entendue de l'impression : " & Chr(0) & _
                "4=Imprimer le fichier" & Chr(0) & "5=Pages" & Chr(0) & "6=de :" & Chr(0) & "7=a :" & Chr(0) & _
                "8=Page en cours" & Chr(0) & "9=Copies" & Chr(0) & "10=" & Chr(0) & "11=&OK" & Chr(0) & "12=&Annuler" & Chr(0) & _
                "13=Veuillez s�lectionner une imprimante�Erreur" & Chr(0) & "14=Veuillez indiquer le nombre de copies�Erreur" & Chr(0) & _
                "15=Veuillez indiquer la page mini � imprimer�Erreur" & Chr(0) & "16=Veuillez indiquer la page maxi � imprimer�Erreur" & Chr(0) & _
                "17=Visualisation �cran" & Chr(0) & _
                "18=Propri�t�s" & Chr(0) & _
                "19=Imprimer en Recto-Verso" & Chr(0) & "20=Spool en cours" & Chr(0) & _
                "21=Mise en page" & Chr(0) & "22=Sans la page des s�lections" & Chr(0) & "23=Imprimer les pi�ces jointes"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMPRINTVISU"
                
        '*********************
        '* EXPORT VERS EXCEL *
        '*********************
        Set aLb = New Libelle
        
        strLib = "1=Export vers Excel" & Chr(0) & "2=Etendue de l'export:" & Chr(0) & "3=Gestion des feuilles" & Chr(0) & _
                "4=Cr�er une feuille � chaque page" & Chr(0) & "5=Regrouper toutes les pages sur une seule feuille" & Chr(0) & _
                "6=Attention : la dur�e du traitement de l'export vers MicroSoft Excel est largement conditionn�e par les options de mise en forme des cellules." & Chr(0) & _
                "7=Maquette" & Chr(0) & "8=Propri�t�s d'export : " & Chr(0) & "9=Tableau :" & Chr(0) & _
                "10=Block :" & Chr(0) & "11=Ligne :" & Chr(0) & "12=N'exporter qu'une seule fois" & Chr(0) & _
                "13=R�f�rence" & Chr(0) & "14=Utiliser un classeur de r�f�rence" & Chr(0) & "15=Nommer la feuille" & Chr(0) & _
                "16=Nommer la plage de cellules" & Chr(0) & "17=Sp�cifier la cellule de d�part" & Chr(0) & "18=Traitement" & Chr(0) & _
                "19=Traitement en cours" & Chr(0) & "20=Exporter le format" & Chr(0) & "21=Valeur de propri�t� incorrecte" & Chr(0) & _
                "22=Erreur dans le param�trage des pages � imprimer" & Chr(0) & "23=Erreur" & Chr(0) & "24=Veuillez saisir le classeur de r�f�rence" & Chr(0) & _
                "25=Impossible de trouver le fichier" & Chr(0) & "26=Veuillez saisir le nom de l'onglet de classeur" & Chr(0) & "27=Veuillez saisir le nom de la plage de donn�es" & Chr(0) & _
                "28=Veuillez saisir la cellule de d�part" & Chr(0) & "29=Il est obligatoire de donner un nom � la feuille lorsque l'on exporte toutes les pages dans une seule feuille." & Chr(0) & _
                "30=Classeur Excel" & Chr(0) & "31=Mod�le de classeur" & Chr(0) & "32=Erreur dans l'ouverture du classeur de r�f�rence" & Chr(0) & "33=Nom de fichier incorrect" & Chr(0) & "34=Visualisation �cran" & Chr(0) & _
                "35=G�n�ral" & Chr(0) & "36=Spool en entier" & Chr(0) & "37=Descriptifs des �l�m�nts � exporter" & Chr(0) & "38=Page" & Chr(0) & _
                "39=Export : " & Chr(0) & "40=Formatage : " & Chr(0) & "41=Donn�es sans format" & Chr(0) & "42=Tous les spools"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMEXCEL"
        
        '*********************
        '* EXPORT VERS OPENOFFICE *
        '*********************
        Set aLb = New Libelle
        
        strLib = "1=Export vers OpenOffice" & Chr(0) & "2=Etendue de l'export:" & Chr(0) & "3=Gestion des feuilles" & Chr(0) & _
                "4=Cr�er une feuille � chaque page" & Chr(0) & "5=Regrouper toutes les pages sur une seule feuille" & Chr(0) & _
                "6=Attention : la dur�e du traitement de l'export vers OpenOffice est largement conditionn�e par les options de mise en forme des cellules." & Chr(0) & _
                "7=Maquette" & Chr(0) & "8=Propri�t�s d'export : " & Chr(0) & "9=Tableau :" & Chr(0) & _
                "10=Block :" & Chr(0) & "11=Ligne :" & Chr(0) & "12=N'exporter qu'une seule fois" & Chr(0) & _
                "13=R�f�rence" & Chr(0) & "14=Utiliser un classeur de r�f�rence" & Chr(0) & "15=Nommer la feuille" & Chr(0) & _
                "16=Nommer la plage de cellules" & Chr(0) & "17=Sp�cifier la cellule de d�part" & Chr(0) & "18=Traitement" & Chr(0) & _
                "19=Traitement en cours" & Chr(0) & "20=Exporter le format" & Chr(0) & "21=Valeur de propri�t� incorrecte" & Chr(0) & _
                "22=Erreur dans le param�trage des pages � imprimer" & Chr(0) & "23=Erreur" & Chr(0) & "24=Veuillez saisir le classeur de r�f�rence" & Chr(0) & _
                "25=Impossible de trouver le fichier" & Chr(0) & "26=Veuillez saisir le nom de l'onglet de classeur" & Chr(0) & "27=Veuillez saisir le nom de la plage de donn�es" & Chr(0) & _
                "28=Veuillez saisir la cellule de d�part" & Chr(0) & "29=Il est obligatoire de donner un nom � la feuille lorsque l'on exporte toutes les pages dans une seule feuille." & Chr(0) & _
                "30=Classeur OpenOffice" & Chr(0) & "31=Mod�le de classeur" & Chr(0) & "32=Erreur dans l'ouverture du classeur de r�f�rence" & Chr(0) & "33=Nom de fichier incorrect" & Chr(0) & "34=Visualisation �cran" & Chr(0) & _
                "35=G�n�ral" & Chr(0) & "36=Spool en entier" & Chr(0) & "37=Descriptifs des �l�m�nts � exporter" & Chr(0) & "38=Page" & Chr(0) & _
                "39=Export : " & Chr(0) & "40=Formatage : "
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMOPENOFFICE"
        
        '********************
        '* MESSPROG FRMGESTSPOOL *
        '********************
        Set aLb = New Libelle
        strLib = "1=Titre :" & Chr(0) & "2=Objet :" & Chr(0) & "3=Auteur :" & Chr(0) & _
                 "4=Cat�gorie :" & Chr(0) & "5=Mots-cl�s :" & Chr(0) & "6=Contenant :" & Chr(0) & _
                 "7=Date min/max" & Chr(0) & "8=Filtrer les fichiers" & Chr(0) & "9=R�sum�" & Chr(0) & "10=Aper�u" & Chr(0) & _
                 "11=Gestion des spools" & Chr(0) & "12=Ouvrir le fichier" & Chr(0) & "13=Pr�visualiser le fichier" & Chr(0) & "14=Supprimer (les) fichier(s) [suppr]" & Chr(0) & _
                 "15=Renommer le fichier [F2]" & Chr(0) & "16=Sauvegarder le fichier [Ctrl+S]" & Chr(0) & "17=Imprimer le fichier Spool [Ctrl+P]" & Chr(0) & _
                 "18=R�actualiser et r�initialiser [F5]" & Chr(0) & "19=Fusionner les spools s�l�ctionn�s" & Chr(0) & "20=Eclater le spool s�lection�" & Chr(0) & _
                 "21=Afficher/Masquer le r�sum� des fichiers" & Chr(0) & "22=Date" & Chr(0) & "23=Nom" & Chr(0) & _
                 "24=Fichier" & Chr(0) & "25=Date de modification" & Chr(0) & _
                 "26=Taille" & Chr(0) & "27=R�sum�" & Chr(0) & _
                 "28=Annuler" & Chr(0) & "29=Appliquer" & Chr(0) & _
                 "30= fichiers analys�(s) " & Chr(0) & "31=[Echap] pour quitter l'analyse des propri�t�s" & Chr(0) & "32= Fichiers trouv�s" & Chr(0) & _
                 "33=Chargement en cours..." & Chr(0) & "34=Fichier selectionn�" & Chr(0) & "35=(Tous)" & Chr(0) & "36=Changer le r�pertoire d'analyse" & Chr(0) & "37=Page pr�c�dente" & Chr(0) & "38=Page suivante" & Chr(0) & "39=Nombre de spools" & _
                 Chr(0) & "40=Page du spool en cours" & Chr(0) & "41=Crypter/d�crypter" & Chr(0) & "42=[OUI] pour crypter, [Non] pour d�crypter" & Chr(0) & "43=G�n�rer des fichiers PDF"
                 
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMGESTSPOOL"

        '********************
        '* MESSPROG FRMNOTE *
        '********************
        Set aLb = New Libelle
        strLib = "1=Ent�te :" & Chr(0) & "2=Opacit�" & Chr(0) & "3=Couleur" & Chr(0) & _
                 "4=Suppression" & Chr(0) & "5=Fermeture" & Chr(0) & "6=Saisie de l'ent�te de l'annotation" & Chr(0) & _
                 "7=" & Chr(0) & "8="
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMNOTE"

        '**************************
        '* MESSPROG FRMVUE *
        '**************************
        Set aLb = New Libelle
        
        strLib = "1=Vue" & Chr(0) & "2=Vue :" & Chr(0) & "3=La vue n'a pas �t� trouv�e�Erreur" & Chr(0) & "4=Supprimer" _
        & Chr(0) & "5=Charger une vue" & Chr(0) & "6=Enregistrer une vue"

        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMVUE"

        '********************
        '* MESSPROG FRMECLAT *
        '********************
        Set aLb = New Libelle
        strLib = "1=Envoyer un fichier" & Chr(0) & "2=G�n�ral" & Chr(0) & "3=Envoyer la page en cours" & Chr(0) & _
                 "4=Envoyer le spool en entier" & Chr(0) & "5=Crypter le fichier" & Chr(0) & "6=Internet" & Chr(0) & _
                 "7=OutLook" & Chr(0) & "8=Destinataire" & Chr(0) & "9=Sujet" & Chr(0) & "10=Format de l'�clatement" & Chr(0) & _
                 "11=Eclater" & Chr(0) & "12=Police" & Chr(0) & "13=Couleur" & Chr(0) & "14=Aligner � gauche" & Chr(0) & _
                 "15=Centrer" & Chr(0) & "16=Aligner � droite" & Chr(0) & "17=�Erreur" & Chr(0) & _
                 "18=Envoyer le fichier en entier" & Chr(0) & "19=Options" & Chr(0) & "20=Format du fichier" & Chr(0) & _
                 "21=Utiliser le format Turbo" & Chr(0) & "22=Utiliser le format HTML" & Chr(0) & "23=Navigation" & Chr(0) & _
                 "24=Cr�er une page de navigation" & Chr(0) & "25=G�n�rer un seul fichier contenant toutes les pages" & Chr(0) & _
                 "26=G�n�rer de simples pages" & Chr(0) & "27=Inclure le num�ro du spool dans le nom des pages" & Chr(0) & _
                 "28=Int�grer le spool dans le corps du mail" & Chr(0) & "29=Utiliser le format PDF" & Chr(0) & _
                 "30=Pas de pi�ce jointe" & Chr(0) & "31=Lotus Notes" & Chr(0) & "32=Copies" & Chr(0) & _
                 "33=A :" & Chr(0) & "34=Cc :" & Chr(0) & "35=Obj :" & Chr(0) & "36=Eclatement du spool" & Chr(0) & "37=Destinataires" & Chr(0) & "38=Eclater" & Chr(0) & "39=Format de l'�clatement" & _
                 Chr(0) & "40=G�n�rer" & Chr(0) & "41=Format des fichiers" & Chr(0) & "42=" & Chr(0) & "43=Message commun � envoyer" & Chr(0) & "44=" & Chr(0) & "45=Cette �dition n'a pas �t� configur�e pour les �clatements" & Chr(0) & "46=Imprimer" & _
                 Chr(0) & "47=" & Chr(0) & "48=" & Chr(0) & "49=Radical du fichier :" & _
                 Chr(0) & "50=Utiliser le serveur de messagerie" & Chr(0) & "51=De : " & Chr(0) & "52=Obj : " & Chr(0) & "53=Corps : " & Chr(0) & _
                 "54=Envoyer par votre messagerie par d�faut" & Chr(0) & "55=Envoyer en utilisant votre compte Outlook" & Chr(0) & "56=Envoyer en utilisant votre compte Lotus Notes" & Chr(0) & "57=Envoyer en utilisant votre serveur de messagerie" & Chr(0) & _
                 "58=Envoyer par votre messagerie par d�faut" & Chr(0) & "59=Envoyer par Outlook/Lotus Notes" & Chr(0) & "60=Envoyer par un serveur de messagerie" & Chr(0) & "61=Serveur de messagerie"
                 
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMECLAT"
                 
Case "2"
        '********************
        '* MESSPROG MDIMAIN *
        '********************
        
        Set aLb = New Libelle
        strLib = "1=&File" & Chr(0) & "2=&Open" & Chr(0) & "3=&Print" & Chr(0) & _
                 "4=&Exit" & Chr(0) & "5=W&indow" & Chr(0) & "6=&Navigating" & Chr(0) & _
                 "7=Windows &across" & Chr(0) & "8=Windows &upright" & Chr(0) & _
                 "9=&cascaded windows" & Chr(0) & "10=Viewer DEAL INFORMATIQUE." & Chr(0) & _
                 "11=File error." & Chr(0) & "12=Page n. : " & Chr(0) & "13=File close" & Chr(0) & _
                 "14=Print" & Chr(0) & "15=Properties" & Chr(0) & "16=Send to" & Chr(0) & _
                 "17=Internet" & Chr(0) & "18=E-mail application" & Chr(0) & "19=Text printer" & Chr(0) & _
                 "20=&Managing the icons" & Chr(0) & "21=About the DEAL Informatique viewer" & Chr(0) & _
                 "22=Cancel" & Chr(0) & "23=Definitely" & Chr(0) & "24=Send to the trash" & Chr(0) & "25=This file already exist : Replace ?�Confirm" & Chr(0) & _
                 "26=Verify update" & Chr(0) & "27=File utility" & Chr(0) & "28=" & Chr(0) & "29=Tools" & Chr(0) & "30=Delete original files" & Chr(0) & "31=Restore to origin folders" & Chr(0) & _
                 "32=Print all files" & Chr(0) & "33=By default spools are keep in 'my documents/my spools'. How many times do you want to keep them?" & Chr(13) & "By default all spools are kept." & _
                 Chr(0) & "34=How many times keep the spools?" & Chr(0) & "35=  " & Chr(0) & "36=Listen mode"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "MDIMAIN"
        
        '******************
        '* Ac�s au d�tail *
        '******************
        
        Set aLb = New Libelle
        strLib = "1=Unable to find specify detail file�Detail" & Chr(0) & "2=In progress ..." & _
                  Chr(0) & "3=Access to detail"
        
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "ACCESDET"
        
        '********************
        '* MESSPROG FRMMAIL *
        '********************
        Set aLb = New Libelle
        strLib = "1=Send a file" & Chr(0) & "2=General :" & Chr(0) & "3=Send active page " & Chr(0) & _
                 "4=Send whole spool" & Chr(0) & "5=Encode file" & Chr(0) & "6=     Internet" & Chr(0) & _
                 "7=     OutLook" & Chr(0) & "8=To (adrressee)" & Chr(0) & "9=Subject" & Chr(0) & "10=Message" & Chr(0) & _
                 "11=Send" & Chr(0) & "12=Font" & Chr(0) & "13=Colour" & Chr(0) & "14=left alignment" & Chr(0) & _
                 "15=entered" & Chr(0) & "16=right alignment" & Chr(0) & "17=Please type in name of addressee�Error" & Chr(0) & _
                 "18=Send whole file" & Chr(0) & "19=Options" & Chr(0) & "20=Format of file" & Chr(0) & _
                 "21=Use format Turbo" & Chr(0) & "22=Use format HTML" & Chr(0) & "23=Navigation" & Chr(0) & _
                 "24=Create a navigation page (a web page ?)" & Chr(0) & "25=Generate only one file including all pages" & Chr(0) & _
                 "26=Generate single pages" & Chr(0) & "27=Insert number of spool in name of pages" & Chr(0) & _
                 "28=Incorporate spool in mail body" & Chr(0) & "29=Use PDF format" & Chr(0) & _
                 "30=No file join" & Chr(0) & "31=Lotus Notes" & Chr(0) & "32=Copies" & Chr(0) & _
                 "33=To :" & Chr(0) & "34=Cc :" & Chr(0) & "35=Obj :" & Chr(0) & "36=Several Mail" & Chr(0) & "37=To" & Chr(0) & "38=Send" & Chr(0) & "39=Message" & _
                 Chr(0) & "40=File join" & Chr(0) & "41=File join Format" & Chr(0) & "42=Mail to" & Chr(0) & "43=Common message" & Chr(0) & "44=Microsoft OutLook is necessary" & Chr(0) & "45=This spool is not configure for mailing" & Chr(0) & "46=Print" & _
                 Chr(0) & "47=Print selection users" & Chr(0) & "48=Print unselection user" & Chr(0) & "49=PDF file name :" & _
                 Chr(0) & "50=Use SMTP serveur" & Chr(0) & "51=From : " & Chr(0) & "52=Obj : " & Chr(0) & "53=Body : " & Chr(0) & _
                 "54=Use default mailer" & Chr(0) & "55=Use Outlook" & Chr(0) & "56=Use Lotus Notes" & Chr(0) & "57=Use SMTP" & Chr(0) & _
                 "58=Use default mailer" & Chr(0) & "59=Use Outlook/Lotus Notes" & Chr(0) & "60=Use SMTP" & Chr(0) & "61=SMTP" & Chr(0) & "62=PLEASE SETUP YOUR SMTP IP" & Chr(0) & "63=Regroup by email" & Chr(0) & "64=<option>" & Chr(0) & "65=Regroup by libel"
                 
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMMAIL"
                 
        '*************************
        '* MESSPROG FRMINTERNET  *
        '*************************
                 
        Set aLb = New Libelle
        strLib = "1=Export as a format HTML" & Chr(0) & "2=Export :" & Chr(0) & "3=Export active page " & Chr(0) & _
                 "4=Export whole spool " & Chr(0) & "5=Options : " & Chr(0) & "6=Create a web page ? a navigation page ?" & Chr(0) & _
                 "7=Start at the end of processing" & Chr(0) & "8=Processing : " & Chr(0) & "9=Creation of page : " & Chr(0) & _
                 "10=Export" & Chr(0) & "11=Navigation : " & Chr(0) & "12=Use a navigation bar" & Chr(0) & "13=Use a page list" & Chr(0) & _
                 "14=Export whole file" & Chr(0) & "15=Insert number of spool in the name of pages" & Chr(0) & _
                 "16=Generate only one file including all pages" & Chr(0) & "17=Generate single pages" & Chr(0) & _
                 "18=File : " & Chr(0) & "19=Export directory" & Chr(0) & "20=Name of file to be generated" & Chr(0) & _
                 "21=Name of file is wrong" & Chr(0) & "22=Print borders"
                 
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMINTERNET"
                          
        '*************************
        '* MESSPROG FRMVISUSPOOL *
        '*************************
        
        Set aLb = New Libelle
        strLib = "1=Selected page" & Chr(0) & "2=First page" & Chr(0) & "3=Previous page" & Chr(0) & _
                 "4=Go to a page" & Chr(0) & "5=Next page" & Chr(0) & "6=Last page" & Chr(0) & _
                 "7=Print" & Chr(0) & "8=Send a message" & Chr(0) & "9=Export as HTML Format" & Chr(0) & _
                 "10=Help" & Chr(0) & "11=Spool : � of file : " & Chr(0) & "12=File print" & Chr(0) & _
                 "13=Spool print" & Chr(0) & "14=Page print" & Chr(0) & "15=Export to PDF" & _
                 Chr(0) & "16=Find" & Chr(0) & "17=Find next" & Chr(0) & "18=Export to OpenOffice" & _
                 Chr(0) & "19=Acrobat PdfWriter Distiller, Win2pdf, PdfCreator" & Chr(0) & "20=Mail" & Chr(0) & "21=File join" & Chr(0) & "22=Adjust" & _
                 Chr(0) & "23=Export to EXCEL" & Chr(0) & "24=Save as" & Chr(0) & "25=Spool manager" & Chr(0) & "25=Vue add"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMVISUSPOOL"
                 
        '*************************
        '* MESSPROG UPDATE *
        '*************************
        
        Set aLb = New Libelle
        strLib = "1=An update is available." & Chr(0) & "2=Install update ?" & Chr(0) & "3=No update." & Chr(0) & _
                 "4=Unable to get an update !" & Chr(0) & "5=Unable to connect site" & Chr(0) & "6=Application will be closed in order to be update." & Chr(0) & _
                 "7=Continue ?" & Chr(0) & "8=Unable to get an update !" & Chr(0) & "9=Verify your connection and retry !" & Chr(0) & _
                 "10=Caution" & Chr(0) & "11=Press 'No' to ignore update" & Chr(0) & "Please wait..." & Chr(0) & "12=Print file"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "UPDATE"
                          
        '**********************
        '* MESSPROG STATUSBAR *
        '**********************
        Set aLb = New Libelle
        strLib = "1=Reading $FICHIER$ file ..." & Chr(0) & "2=Initialization of models ..." & Chr(0) & _
                 "3=Reading active page ..."
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "STATUSBAR"
        
        '*******************
        '* MESSPROG ERRORS *
        '*******************
        Set aLb = New Libelle
        strLib = "1=" & Chr(0) & _
                 "2=Impossible to open $FICHIER$ file for reading." & Chr(0) & _
                 "3=Error when creating  object of the file class for $FICHIER$ file." & Chr(0) & _
                 "4=Impossible to read ASCII file after opening." & Chr(0) & "5=" & Chr(0) & _
                 "6=Impossible to find a spool in specified file $FICHIER$." & Chr(0) & _
                 "7=Impossible to find model $MAQ$ of file $FICHIER$." & Chr(0) & _
                 "8=System error : Impossible to create a temporary file for reading $MAQ$ model." & Chr(0) & _
                 "9=Impossible to open temporary file $INI$ of $MAQ$ model." & Chr(0) & _
                 "10=Error when reading temporary file $INI$ of $MAQ$ model." & Chr(0) & _
                 "11=Error when creating object of type section : $SECTION$ of $MAQ$ model." & _
                 "12=Error when creating object of type data table : $TABLEAU$ of $MAQ$ model." & Chr(0) & "13=" & Chr(0) & _
                 "14=Error when creating a BMP : $BMP$ of section : $SECTION$ of $MAQ$ model." & Chr(0) & _
                 "15=Error when creating a framework : $CADRE$ of  section : $SECTION$ of $MAQ$ model." & Chr(0) & _
                 "16=Error when creating a block : $BLOCK$ of data table $TABLEAU$ of $MAQ$ model." & Chr(0) & _
                 "17=Impossible to create a temporary file for an RTF object." & Chr(0) & _
                 "18=Error when initializing header of model : " & Chr(0) & _
                 "19=Error when initializing a new BMP container." & Chr(0) & _
                 "20=Error when setting dimensions for a  BMP object : $BMP$ of section : $SECTION$. Height : $HAUTEUR$   Width : $LARGEUR$." & Chr(0) & _
                 "21=Error when setting the propath of a picture." & Chr(0) & _
                 "22=Error when loading file : $FICHIER$ of bmp : $BMP$ of section : $SECTION$." & Chr(0) & _
                 "23=Error when creating representation of object framework $CADRE$ of section $SECTION$." & Chr(0) & _
                 "24=Error when loading temporary file $FICHIER$ of framework : $CADRE$ of section : $SECTION$." & Chr(0) & _
                 "25=Error when loading temporary file $FICHIER$ of section : $SECTION$." & Chr(0) & _
                 "26=Error when creating physical representation of section : $SECTION$." & Chr(0) & _
                 "27=Error when initializing section $SECTION$ for row value : $LIG$." & Chr(0)
                 
                 
        strLib = strLib & "28=Error when initializing header of picture : $BMP$ of section $SECTION$ for value : $LIG$" & Chr(0) & Chr(0) & _
                          "29=Error when initializing header of framework : $CADRE$ of section $SECTION$ for value : $LIG$" & Chr(0) & _
                          "30=Error when initializing data table $TABLEAU$ for value : $LIG$." & Chr(0) & _
                          "31=Error when initializing block $BLOCK$ for data table $TABLEAU$ for value $LIG$." & Chr(0) & _
                          "32=Error when reading a setting row of block $BLOCK$ of data table $TABLEAU$." & Chr(0) & _
                          "33=Error when creating cells of block $BLOCK$ of data table $TABLEAU$ for value $LIG$." & Chr(0) & _
                          "34=Error when initializing dimensions of cells of block : $BLOCK$ of data table $TABLEAU$." & Chr(0) & _
                          "35=Error when initializing page to be printed" & Chr(0) & _
                          "36=Error when initializing printing of section : $SECTION$." & Chr(0) & _
                          "37=Error when initializing printing of data table : $TABLEAU$." & Chr(0) & _
                          "38=Error when printing an RTF object : $OBJECT$." & Chr(0) & _
                          "39=Error when printing picture : $BMP$ of section : $SECTION$." & Chr(0) & _
                          "40=Error when initializing block : $BLOCK$ of data table $TABLEAU$" & Chr(0) & _
                          "41=Error in drawing of cell : $CELLULE$ of block : $BLOCK$ of data table $TABLEAU$." & Chr(0) & _
                          "42=Error when printing file.�Fatal error in printing"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "ERRORS"
        
        '********************
        '* MESSPROG MESSAGE *
        '********************
        Set aLb = New Libelle
        
        strLib = "1=Exit now ?�OK" & Chr(0) & "2=File already loaded.�Impossible to load file." & Chr(0) & _
                 "3=Page number is wrong.�Error" & Chr(0) & "4=Impossible to connect to Ms OutLook.�Impossible to send a message." & Chr(0) & _
                 "5=Impossible to create a new message when using Ms OutLook.�Impossible to create a message." & Chr(0) & _
                 "6=Impossible to go to specified file : $FICHIER$.�Error." & Chr(0) & _
                 "7=Send active page [Yes] or whole spool [No] ?�Question" & Chr(0) & _
                 "8=Please type in name of addressee for message.�Error" & Chr(0) & "9=Impossible to send this message when using Internet.�Error" & Chr(0) & _
                 "10=Impossible to initialize an e-mail session.�Error" & Chr(0) & "11=Encode file ?�Question" & Chr(0) & "12=Value error.�Error"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "MESSAGE"
        
        '********************
        '* MESSPROG BOUTONS *
        '********************
        Set aLb = New Libelle
        
        strLib = "1=&Ok" & Chr(0) & "2=&Cancel" & Chr(0) & "3=&Send"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "BOUTONS"
        
        '***************
        '* FRMNETSEND  *
        '***************
        Set aLb = New Libelle
        strLib = "1=Send an e-mail" & Chr(0) & "2=Url : " & Chr(0) & "3=Message : " & Chr(0) & _
                 "4=Subject : " & Chr(0) & "5=Address book" & Chr(0) & "6=Encode joined file :"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMNETSEND"
            
        '***************
        '* FRMEDITOR  *
        '***************
        Set aLb = New Libelle
        strLib = "1=&File" & Chr(0) & "2=&Open" & Chr(0) & "3=&Save" & Chr(0) & _
                 "4=Sa&ve as" & Chr(0) & "5=&Print" & Chr(0) & "6=&Exit" & Chr(0) & _
                 "7=&Edit" & Chr(0) & "8=Cut" & Chr(0) & "9=Copy" & Chr(0) & _
                 "10=Paste" & Chr(0) & "11=Search for" & Chr(0) & "12=About ..." & Chr(0) & _
                 "13=Search : " & Chr(0) & "14=Forma&t" & Chr(0) & "15=&Font" & Chr(0) & _
                 "16=Sending file : " & Chr(0) & "17=Sending spool :" & Chr(0) & "18=Sending page : "
                 
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMEDITOR"
        
        '***************
        '* FRMFONT     *
        '***************
        Set aLb = New Libelle
        strLib = "1=Font selection" & Chr(0) & "2=Font :" & Chr(0) & "3=Font name :" & Chr(0) & _
                 "4=Size : " & Chr(0) & "5=Regular" & Chr(0) & "6=Italic" & Chr(0) & "7=Bold" & Chr(0) & _
                 "8=Bold italic"
        
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMFONT"
        
        '**************************
        '* MESSPROG FRMSEARCHPAGE *
        '**************************
        Set aLb = New Libelle
        
        strLib = "1=Go to a page" & Chr(0) & "Number of page : "
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMSEARCHPAGE"
        
        '**************************
        '* MESSPROG FRMSEARCHTEXT *
        '**************************
        Set aLb = New Libelle
        
        strLib = "1=Find" & Chr(0) & "2=Find :" & Chr(0) & "3=Unable to find text�Error"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMSEARCHTEXT"
        
        '*************************
        '* MESSPROG D'IMPRESSION *
        '*************************
        Set aLb = New Libelle
        strLib = "1=Printer selection" & Chr(0) & "2=Name : " & Chr(0) & "3=Print what : " & Chr(0) & _
                "4=Print file" & Chr(0) & "5=Pages" & Chr(0) & "6=from :" & Chr(0) & "7=to :" & Chr(0) & _
                "8=Active page" & Chr(0) & "9=Copies : " & Chr(0) & "10=Number of copies : " & Chr(0) & "11=&OK" & Chr(0) & "12=&Cancel" & Chr(0) & _
                "13=Please choose a printer�Error" & Chr(0) & "14=Please type in number of copies �Error" & Chr(0) & _
                "15=Please type in minimum number of page to be printed�Error" & Chr(0) & "16=Please type in maximum number of page to be printed�Error" & Chr(0) & _
                "17=Preview" & Chr(0) & _
                "18=Property" & Chr(0) & _
                "19=Duplexer mode" & Chr(0) & "20=Current Spool" & Chr(0) & _
                "21=Page setting" & Chr(0) & "22=Without selection page" & Chr(0) & "23=Print joins files"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMPRINTVISU"
                
        '*********************
        '* EXPORT VERS EXCEL *
        '*********************
        Set aLb = New Libelle
        
        strLib = "1=Export into Excel" & Chr(0) & "2=Export area:" & Chr(0) & "3=Managing the sheets" & Chr(0) & _
                "4=Create a worksheet for each page" & Chr(0) & "5=Group all pages on one worksheet" & Chr(0) & _
                "6=Be careful : processing time for export into Microsoft Excel is widely determined by format options for cells." & Chr(0) & _
                "7=Model" & Chr(0) & "8=Export properties : " & Chr(0) & "9=Data table :" & Chr(0) & _
                "10=Block :" & Chr(0) & "11=Row : " & Chr(0) & "12=Export once" & Chr(0) & _
                "13=Reference" & Chr(0) & "14=Use a workbook as a reference" & Chr(0) & "15=Name the woorksheet" & Chr(0) & _
                "16=Name the cell range" & Chr(0) & "17=Specify first cell" & Chr(0) & "18=Processing" & Chr(0) & _
                "19=Being processed" & Chr(0) & "20=Export format" & Chr(0) & "21=Property value is wrong" & Chr(0) & _
                "22=Error in configuration of pages to be printed" & Chr(0) & "23=Error" & Chr(0) & "24=Please type in workbook as a reference" & Chr(0) & _
                "25=Impossible to find file " & Chr(0) & "26=Please type in tab name of workbook " & Chr(0) & "27=Please type in name of data range" & Chr(0) & _
                "28=Please type in first cell" & Chr(0) & "29=You must name the woorksheet when you want to export all pages in one woorksheet." & Chr(0) & _
                "30=Excel workbook" & Chr(0) & "31=standard workbook (model)" & Chr(0) & "32=Error when opening referenced workbook" & Chr(0) & "33=File name is wrong" & Chr(0) & "34=View" & Chr(0) & _
                "35=General" & Chr(0) & "36=Export whole spool" & Chr(0) & "37=Description of items to be exported" & Chr(0) & "38=Page" & Chr(0) & _
                "39=Export : " & Chr(0) & "40=Formatting : " & Chr(0) & "41=Without format" & Chr(0) & "42=All spools"
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMEXCEL"
        
        '*********************
        '* EXPORT VERS OPENOFFICE *
        '*********************
        Set aLb = New Libelle
        
        strLib = "1=Export into OpenOffice" & Chr(0) & "2=Export area:" & Chr(0) & "3=Managing the sheets" & Chr(0) & _
                "4=Create a worksheet for each page" & Chr(0) & "5=Group all pages on one worksheet" & Chr(0) & _
                "6=Be careful : processing time for export into OpenOffice is widely determined by format options for cells" & Chr(0) & _
                "7=Model" & Chr(0) & "8=Export properties : " & Chr(0) & "9=Data table :" & Chr(0) & _
                "10=Block :" & Chr(0) & "11=Row : " & Chr(0) & "12=Export once" & Chr(0) & _
                "13=Reference" & Chr(0) & "14=Use a workbook as a reference" & Chr(0) & "15=Name the woorksheet" & Chr(0) & _
                "16=Name the cell range" & Chr(0) & "17=Specify first cell" & Chr(0) & "18=Processing" & Chr(0) & _
                "19=Being processed" & Chr(0) & "20=Export format" & Chr(0) & "21=Property value is wrong" & Chr(0) & _
                "22=Error in configuration of pages to be printed" & Chr(0) & "23=Error" & Chr(0) & "24=Please type in workbook as a reference" & Chr(0) & _
                "25=Impossible to find file" & Chr(0) & "26=Please type in tab name of workbook " & Chr(0) & "27=Please type in name of data range" & Chr(0) & _
                "28=Please type in first cell" & Chr(0) & "29=You must name the woorksheet when you want to export all pages in one woorksheet." & Chr(0) & _
                "30=OpenOffice workbook" & Chr(0) & "31=standard workbook (model)" & Chr(0) & "32=Error when opening referenced workbook" & Chr(0) & "33=File name is wrong" & Chr(0) & "34=View" & Chr(0) & _
                "35=General" & Chr(0) & "36=Export whole spool" & Chr(0) & "37=Description of items to be exported" & Chr(0) & "38=Page" & Chr(0) & _
                "39=Export : " & Chr(0) & "40=Formatting : "
        
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMOPENOFFICE"
        
        '********************
        '* MESSPROG FRMGESTSPOOL *
        '********************
        Set aLb = New Libelle
        strLib = "1=Title :" & Chr(0) & "2=Object :" & Chr(0) & "3=Author :" & Chr(0) & _
                 "4=Category :" & Chr(0) & "5=Keywords :" & Chr(0) & "6=Search string :" & Chr(0) & _
                 "7=Date min/max" & Chr(0) & "8=File sp�cifications" & Chr(0) & "9=Summary" & Chr(0) & "10=Preview" & Chr(0) & _
                 "11=Spools analyse" & Chr(0) & "12=File open" & Chr(0) & "13=File preview" & Chr(0) & "14=Delete file(s) [suppr]" & Chr(0) & _
                 "15=File rename [F2]" & Chr(0) & "16=File save [Ctrl+S]" & Chr(0) & "17=File print [Ctrl+P]" & Chr(0) & _
                 "18=Update [F5]" & Chr(0) & "19=Merge selected file" & Chr(0) & "20=Unmerge selected file" & Chr(0) & _
                 "21=Show/hide file summary" & Chr(0) & "22=Date" & Chr(0) & "23=Name" & Chr(0) & _
                 "24=File" & Chr(0) & "25=Date of update" & Chr(0) & _
                 "26=Size" & Chr(0) & "27=Summary" & Chr(0) & _
                 "28=Cancel" & Chr(0) & "29=Apply" & Chr(0) & _
                 "30= Files analyse " & Chr(0) & "31=[Echap] to stop analyse" & Chr(0) & "32= Files found" & Chr(0) & _
                 "33=Loading..." & Chr(0) & "34=Selected file" & Chr(0) & "35=(All)" & Chr(0) & "36=Change directory" & Chr(0) & "37=Previous page" & Chr(0) & "38=Next page" & Chr(0) & "39=Number of spool" & _
                 Chr(0) & "40=Current page" & Chr(0) & "41=Crypt/Decrypt" & Chr(0) & "42=[Yes] to crypt [No] to decrypt" & Chr(0) & "43=Create PDF files"
                 
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMGESTSPOOL"
        
        '********************
        '* MESSPROG FRMNOTE *
        '********************
        Set aLb = New Libelle
        strLib = "1=Header :" & Chr(0) & "2=Opacity" & Chr(0) & "3=Colour" & Chr(0) & _
                 "4=Delete" & Chr(0) & "5=Close" & Chr(0) & "6=Note's subject" & Chr(0) & _
                 "7=" & Chr(0) & "8="
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMNOTE"
        
        '**************************
        '* MESSPROG FRMVUE *
        '**************************
        Set aLb = New Libelle
        
        strLib = "1=Model" & Chr(0) & "2=Model :" & Chr(0) & "3=Unable to find model�Erreur" & Chr(0) & "4=Delete" _
        & Chr(0) & "5=Load model" & Chr(0) & "6=Save model"
        
        '-> Setting des messprog
        aLb.SetKeys strLib
        '-> Ajout dans la collection
        Libelles.Add aLb, "FRMVUE"

End Select
End Sub


Public Function IsOutLook() As Boolean

'---> Cette proc�dure V�rifier si outlook est install�

Dim aObj As Object

On Error GoTo GestError

If HideMessagerie = True Then GoTo GestError

Set aObj = StartOutlook()

Set aObj = Nothing
IsOutLook = True


Exit Function

GestError:
    IsOutLook = False

End Function

Public Function IsLotus() As Boolean

'---> Cette proc�dure V�rifier si lotus est install�
Dim aObj As Object

On Error GoTo GestError

If Not TestOutlook Then
    GoTo GestError
End If

'-> dans le cas ou lotus n'est pas actif verifier si on doit passer le mot de passe
If IsLotus = False Then
'    If GetIniString("LOTUS", "PWD", TurboGraphIniFile, False) <> "" Then
'        '-> on ouvre la session en passant le mot de passe
'        Set aObj = CreateObject("Notes.NotesSession")
'        aObj.Initialize (GetIniString("LOTUS", "PWD", TurboGraphIniFile, False))
'        IsLotus = True
'    Else
        Set aObj = CreateObject("Notes.NotesSession")
        Set aObj = Nothing
        IsLotus = True
'    End If
End If


Exit Function

GestError:
    If GetIniString("PARAM", "ISLOTUS", App.Path & "\Turbograph.ini", False) = "1" Then
        IsLotus = True
    Else
        IsLotus = False
    End If
End Function

Public Function InitMAPISession() As Boolean

'---> Cette proc�dure initialise la session MAPI

Dim aLb As Libelle

On Error GoTo ErrorLogon

'-> Ne pas cr�er de session MAPI s'il y en a d�ja eu une
If frmLib.MAPISession1.NewSession Then
    '-> Ne pas cr�er de session
Else
    With frmLib.MAPISession1
        .DownLoadMail = False '-> Ne pas charger les e-emails � l'ouverture de la session
        .LogonUI = True '-> Indique si une bo�te de dialogue est affich�e au moment de l'ouverture d'une session
        .SignOn '-> Connecte l'utilisateur au compte indiqu� par les propri�t�s UserName et Password.
        .NewSession = True '-> Indique qu'une session MAPI est en cours
        frmLib.MAPIMessages1.SessionID = .SessionID '-> Affecter au message l'ID de session MAPI
    End With
End If

InitMAPISession = True

Exit Function

ErrorLogon:

If Err.Number = 32003 Then
    '-> Ne rien faire : l'utilisateur a cliqu� sur annuler
Else
    '-> Pointer sur la classe message
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(10), vbCritical + vbOKOnly, aLb.GetCaption(10)
End If

InitMAPISession = False

End Function

Public Sub SendToInternet(ByVal FileToSend As String, Optional pBodyFile As String)

'---> Cette proc�dure envoie un e-mail sur Internet via le ctrl MAPI

Dim aLb As Libelle
Dim hdlBody As Integer
Dim Ligne As String

On Error GoTo GestError

'-> Intialiser la session MAPI
If Not InitMAPISession Then Exit Sub

'-> V�rifier la pr�sence du fichier � linker
If Dir$(FileToSend, vbNormal) = "" Then Exit Sub

'-> Cr�er un nouveau message
With frmLib.MAPIMessages1
    If .RecipAddress = "" Then
        .MsgIndex = -1
        .AddressEditFieldCount = 2
        .Compose
        .RecipAddress = Entry(1, NetParam, Chr(0))
        .ResolveName
        .MsgSubject = Entry(2, NetParam, Chr(0))
    Else
        .ResolveName
    End If
End With
    
'-> Corp du message
If Trim(Entry(3, NetParam, Chr(0))) = "" Then
    '-> V�rifier si un fichier BODY file existe
    If pBodyFile <> "" Then
        '-> V�rifier que le fichier existe
        If Dir$(pBodyFile, vbNormal) <> "" Then
            '-> Ouverture du fichier
            hdlBody = FreeFile
            Open pBodyFile For Input As #hdlBody
            Do While Not EOF(hdlBody)
                Line Input #hdlBody, Ligne
                If frmLib.MAPIMessages1.MsgNoteText = "" Then
                    frmLib.MAPIMessages1.MsgNoteText = Ligne
                Else
                    frmLib.MAPIMessages1.MsgNoteText = frmLib.MAPIMessages1.MsgNoteText & Chr(13) + Chr(10) & Ligne
                End If
            Loop
            '-> Fermer le fichier
            Close #hdlBody
        End If 'Si fichier bodyfile existe
    End If 'Si fichier bodyfile sp�cifi�
Else
    '-> Setting du body
    frmLib.MAPIMessages1.MsgNoteText = Entry(3, NetParam, Chr(0))
End If
    
'-> on laisse la place pour inserer les pieces jointes
frmLib.MAPIMessages1.MsgNoteText = Space(20) & vbCrLf & frmLib.MAPIMessages1.MsgNoteText
    
With frmLib.MAPIMessages1
    .AttachmentIndex = frmLib.MAPIMessages1.AttachmentCount
    .AttachmentPosition = frmLib.MAPIMessages1.AttachmentCount
    .AttachmentPathName = FileToSend
    .AttachmentType = 0
End With
    
'If frmLib.MAPIMessages1.MsgNoteText <> "" Then
'    frmLib.MAPIMessages1.AttachmentPosition = Len(frmLib.MAPIMessages1.MsgNoteText) - 1
'Else
'    frmLib.MAPIMessages1.AttachmentPosition = 0
'End If
    
frmLib.MAPIMessages1.Send False
    
Exit Sub

GestError:

    '-> Ne pas afficher de message d'erreur si on vient de la ligne de commande
    If OrigineMail = 1 Then
        MsgBox "Erreur pendant l'envoir du mail :" & Chr(13) & Err.Number & "  " & Err.Description, vbCritical + vbOKOnly, "Erreur"
    End If

End Sub


Public Sub SendToOutLook(ByVal FileToSend As String, Optional pParam As String)

'---> Cette proc�dure ouvre OutLook, cr�er un nouveau message , et joint le fichier Sp�cifi�

Dim aOutLook As Object
Dim aMail As Object
Dim ErrorCode As Integer
Dim aLb As Libelle
Dim i As Integer
Dim hdlBody As Integer
Dim Ligne As String
Dim oSess As Object
Dim oDB As Object
Dim oDoc As Object
Dim oItem As Object
Dim direct As Object
Dim Var As Variant
Dim flag As Boolean
Dim richStyle As Object
Dim AttachME As Object
Dim Bodyitem As Object
Dim Mime As Object
Dim MimeChild As Object
Dim Header As Object
Dim ENC_NONE As Integer
Dim Stream As Object

'-> pParam pour envoie depuis CDE avec
' TO + chr(0) + COPIES + chr(0) + OBJET + chr(0) + BODY + chr(0) + BODYFILE
On Error GoTo GestError

'-> V�rifier que le fichier existe
If Dir$(FileToSend, vbNormal) = "" Then
    ErrorCode = 3
    GoTo GestError
End If

If IsLotus Then
    '-> on initialse la session par defaut
    Set oSess = CreateObject("Notes.NotesSession")
    Set oDB = oSess.GETDATABASE("", "")
    Call oDB.OPENMAIL
    flag = True
    If Not (oDB.IsOpen) Then flag = oDB.Open("", "")
    '-> on verifie si c'est bien ouvert
    If Not flag Then
        MsgBox "Can't open mail file: " & oDB.SERVER & " " & oDB.FilePath
        Exit Sub
    End If
    '-> initialisation du message
    Set oDoc = oDB.CREATEDOCUMENT
    
    'oSess.setConvertMIME = False
    '-> Setting du body
    Set Bodyitem = oSess.CreateStream
    
    '-> pour les envois en copy
    Dim arrNames() As String
    ReDim arrNames(NumEntries(Replace(Entry(2, pParam, Chr(0)), ";", ","), ",") - 1)
    '-> Afficher le corps du message
    If OrigineMail = 0 Then
        '-> Setting des param�tres
        oDoc.Subject = Entry(3, pParam, Chr(0))
        oDoc.sendto = Entry(1, pParam, Chr(0))
        '-> Pour lotus le separateur est la virgule
        For i = 0 To NumEntries(Replace(Entry(2, pParam, Chr(0)), ";", ","), ",") - 1
            arrNames(i) = Entry(i + 1, Replace(Entry(2, pParam, Chr(0)), ";", ","), ",")
        Next
        oDoc.copyto = arrNames
        'oDoc.copyto = Replace(Entry(2, pParam, Chr(0)), ";", ",")
        '-> Envoie depuis la page de visu
        If frmMail.RichTextBox1.Text <> "" Then
            Bodyitem.WriteText frmMail.RichTextBox1.Text
        Else
            Bodyitem.WriteText Entry(4, pParam, Chr(0))
        End If
    Else
        '-> Setting des param�tres
        oDoc.sendto = Entry(1, pParam, Chr(0))
        '-> Pour lotus le separateur est la virgule
        For i = 0 To NumEntries(Replace(Entry(2, pParam, Chr(0)), ";", ","), ",") - 1
            arrNames(i) = Entry(i + 1, Replace(Entry(2, pParam, Chr(0)), ";", ","), ",")
        Next
        oDoc.copyto = arrNames
        'oDoc.copyto = Replace(Entry(2, pParam, Chr(0)), ";", ",")
        oDoc.Subject = Entry(3, pParam, Chr(0))
        '-> Setting du body
        If Trim(Entry(4, pParam, Chr(0))) = "" Then
            '-> V�rifier si un fichier pour body est donn�
            If Trim(Entry(5, pParam, Chr(0))) <> "" Then
                If Dir$(Trim(Entry(5, pParam, Chr(0))), vbNormal) <> "" Then
                    '-> Ouvrir le fichier body
                    hdlBody = FreeFile
                    Open Trim(Entry(5, pParam, Chr(0))) For Input As #hdlBody
                    Do While Not EOF(hdlBody)
                        '-> Lecture de la ligne
                        Line Input #hdlBody, Ligne
                        '-> Ajout dans le body du text
                        Bodyitem.WriteText Ligne
                    Loop
                    '-> Fermer le fichier
                    Close #hdlBody
                End If 'Si fichier body existe
            End If 'Si fichier body sp�cifi�
        Else
            Bodyitem.WriteText Entry(4, pParam, Chr(0))
        End If 'Si pas de body sp�cifi�
    End If 'Selon l'origine du mail
    
    '-> on met le corps du texte au bon format
    Set Mime = oDoc.CreateMIMEEntity '("Body")
    Set Header = Mime.CreateHeader("Content-Type")
    Call Header.SetHeaderVal("multipart/mixed")
    
    Set MimeChild = Mime.CreateChildEntity
    MimeChild.SetContentFromText Bodyitem, "text/HTML;charset=UTF-8", 1729 'modification a cause de problemes de caracteres
    'MimeChild.SetContentFromText Bodyitem, "text/html", ENC_NONE
    Bodyitem.Close
    Bodyitem.Truncate
    
    Set MimeChild = Mime.CreateChildEntity

    Set Header = MimeChild.CreateHeader("Content-Disposition")
    Call Header.setHeaderValAndParams("attachment; filename=" & GetFileName(FileToSend))

    Set Stream = oSess.CreateStream
    If Not Stream.Open(FileToSend, "binary") Then
        'Print "somefile open failed"
        Exit Sub
    End If
    If Stream.Bytes = 0 Then
        Exit Sub
    End If
    Call MimeChild.SetContentFromBytes(Stream, "application/turbo", 1730)
    Call Stream.Close
    Call Stream.Truncate
    
    '-> si il y a des fichiers joints lorsque l on vient d'un fichier zipp�
    For i = 0 To MDIMain.mnuJoin.Count - 1
        If MDIMain.mnuJoin.Item(i).Visible Then
            If Trim(MDIMain.mnuJoin.Item(i).Tag) <> "" Then AttachME.EMBEDOBJECT 1454, "", MDIMain.mnuJoin.Item(i).Tag, "Attachment"
        End If
    Next
        
    '-> mettre le mail dans la boite d'envoi
    oDoc.PostedDate = Now()
    oDoc.SAVEMESSAGEONSEND = True
     
    '-> Afficher le mail
    If SendOutLook Then
        oDoc.Send False
    Else
        oDoc.visable = True
        oDoc.Send False
    End If
        
    '-> Lib�rer les pointeurs
    Set oSess = Nothing
    Set oDB = Nothing
    Set oDoc = Nothing
    Set oItem = Nothing
    Set Mime = Nothing
    Set Bodyitem = Nothing
    Exit Sub
Else
    '-> Obtenir un pointeur vers OutLook
    ErrorCode = 1
    Set aOutLook = StartOutlook()
    
    '-> Cr�er un nouveau message
    ErrorCode = 2
    Set aMail = aOutLook.createitem(0)
    
    '-> Afficher le corps du message
    If OrigineMail = 0 Then
        '-> Envoie depuis la page de visu
        If frmMail.RichTextBox1.Text <> "" Then aMail.body = frmMail.RichTextBox1.Text
        '-> Setting des param�tres
        aMail.To = Entry(1, pParam, Chr(0))
        aMail.CC = Entry(2, pParam, Chr(0))
        aMail.Subject = Entry(3, pParam, Chr(0))
        aMail.body = Entry(4, pParam, Chr(0))
    Else
        '-> Setting des param�tres
        aMail.To = Entry(1, pParam, Chr(0))
        aMail.CC = Entry(2, pParam, Chr(0))
        aMail.Subject = Entry(3, pParam, Chr(0))
        '-> Setting du body
        If Trim(Entry(4, pParam, Chr(0))) = "" Then
            '-> V�rifier si un fichier pour body est donn�
            If Trim(Entry(5, pParam, Chr(0))) <> "" Then
                If Dir$(Trim(Entry(5, pParam, Chr(0))), vbNormal) <> "" Then
                    '-> Ouvrir le fichier body
                    hdlBody = FreeFile
                    Open Trim(Entry(5, pParam, Chr(0))) For Input As #hdlBody
                    Do While Not EOF(hdlBody)
                        '-> Lecture de la ligne
                        Line Input #hdlBody, Ligne
                        '-> Ajout dans le body du text
                        If aMail.body <> "" Then
                            aMail.body = aMail.body & Chr(13) + Chr(10) & Ligne
                        Else
                            aMail.body = Ligne
                        End If
                    Loop
                    '-> Fermer le fichier
                    Close #hdlBody
                End If 'Si fichier body existe
            End If 'Si fichier body sp�cifi�
        Else
            aMail.body = Entry(4, pParam, Chr(0))
        End If 'Si pas de body sp�cifi�
    End If
    
    '-> Attacher le fichier sp�cifi�
    ErrorCode = 3
    aMail.Attachments.Add FileToSend
    
    '-> si il y a des fichiers joints lorsque l on vient d'un fichier zipp�
    For i = 0 To MDIMain.mnuJoin.Count - 1
        If MDIMain.mnuJoin.Item(i).Visible Then
            If Trim(MDIMain.mnuJoin.Item(i).Tag) <> "" Then aMail.Attachments.Add MDIMain.mnuJoin.Item(i).Tag
        End If
    Next
        
    '-> Afficher le mail
    ErrorCode = 4
    If Not SendOutLook Then
        aMail.Send
    Else
        aMail.display
    End If
    
    '-> Lib�rer les pointeurs
    Set aMail = Nothing
    Set aOutLook = Nothing
    
    Exit Sub
End If

GestError:

    '-> N'afficher les messages d'erreur que si on est en visu
    If OrigineMail = 0 Then
    
        '-> Pointer sur la classe libelle
        Set aLb = Libelles("MESSAGE")
                     
        '-> Erreur lors de la connexion : envoyer un message d'erreur
        Select Case ErrorCode
            Case 1
                '-> Afficher un message d'erreur
                MsgBox aLb.GetCaption(4), vbCritical + vbOKOnly, aLb.GetToolTip(4)
            Case 2
                '-> Afficher un message d'erreur
                MsgBox aLb.GetCaption(5), vbCritical + vbOKOnly, aLb.GetToolTip(5)
                '-> Lib�rer le pointeur d'outlook
                Set aOutLook = Nothing
            Case 3
                '-> Afficher un message d'erreur
                MsgBox Replace(aLb.GetCaption(6), "$FICHIER$", FileToSend), vbCritical + vbOKOnly, aLb.GetToolTip(6)
                '-> Lib�rer le pointeur d'outlook
                Set aMail = Nothing
                Set aOutLook = Nothing
        
        End Select
    End If

End Sub


Public Sub CreateFileToSend()

'---> Cette fonction cr�er le fichier � linker pour export OutLook ou Internet
Dim aFichier As Fichier
Dim aSpool As Spool
Dim SpoolName As String

On Error GoTo GestError

'-> Pointer sur l'objet Fichier
Set aFichier = Fichiers(FileKeyMail)

'-> Pointer sur 'objet spool
If SpoolKeyMail <> "" Then Set aSpool = aFichier.Spools(SpoolKeyMail)
    
'-> Analyse selon l'origine
Select Case ExportMail

    Case 0, 1
        If ExportMail = 0 Then
            '-> Envoie de la page en cours
            SpoolName = CreateDetail(aSpool, PageNumMail)
        Else
            '-> Envoie du spool en cours
            SpoolName = CreateDetail(aSpool, -1)
        End If
        
        If OrigineMail = 0 Or OrigineMail = 2 Then
            '-> Envoie dans OutLook
            SendToOutLook SpoolName
        Else
            '-> Envoie dans Internet via MAPI
            SendToInternet SpoolName
        End If
        
    Case 2 '-> Envoie du fichier en cours
        If OrigineMail = 0 Or OrigineMail = 2 Then
            '-> Envoie du spool en entier vers OutLook
            SendToOutLook aFichier.fileName, frmMail.Text2.Text & Chr(0) & frmMail.Text3.Text & Chr(0) & frmMail.Text1.Text & Chr(0) & frmMail.RichTextBox1.Text & Chr(0)
        Else
            '-> Envoie du spool vers Internet
            SendToInternet aFichier.fileName
        End If
    
End Select

GestError:


End Sub

Public Function CreateDetail(ByRef aSpool As Spool, ByVal Page As Integer, Optional PageMin As Integer, Optional PageMax As Integer) As String

'---> Cette fonction cr�er un fichier tempo et retourne le nom du fichier � linker

Dim TempFile As String
Dim hdlFile As Integer
Dim DefMaq As String
Dim i As Integer
Dim PageMini As Integer
Dim PageMaxi As Integer

On Error GoTo GestError

'-> Obtenir un nom de fichier tempotaire
TempFile = Year(Now) & "-" & FORMAT(Month(Now), "00") & "-" & FORMAT(Day(Now), "00") & "-" & FORMAT(Hour(Now), "00") & "-" & FORMAT(Minute(Now), "00") & "-" & FORMAT(Second(Now), "00") & Right(CStr(GetTickCount), 2) & ".turbo"
TempFile = GetTempFileNameVB("WWW", True) & TempFile

'-> Obtenir un handle de fichier
hdlFile = FreeFile

'-> Ouverture du fichier ascii et �criture des pages
Open TempFile For Output As #hdlFile

'-> Ecriture de la balise Spool
If IsCryptedFile Then
    Print #hdlFile, Crypt("[SPOOL]")
Else
    Print #hdlFile, "[SPOOL]"
End If

'-> Il faut dans un premier temps �crire la maquette
If IsCryptedFile Then
    Print #hdlFile, Crypt("[MAQ]")
Else
    Print #hdlFile, "[MAQ]"
End If

'-> R�cup�ration de la maquette
DefMaq = aSpool.GetMaq
DefMaq = Replace(DefMaq, "<tot:>", aSpool.nbPage)

'-> Impression de la maquette
Dim strPattern As String: strPattern = "<frm:[0-9]+:"
Dim regex As New RegExp
Dim strTemp As String
regex.Global = True
regex.Pattern = strPattern

For i = 1 To NumEntries(DefMaq, Chr(0))
    If IsCryptedFile Then
        Print #hdlFile, Crypt(Entry(i, DefMaq, Chr(0)))
    Else
        strTemp = Entry(i, DefMaq, Chr(0))
        If InStr(strTemp, "<frm:") <> 0 Then
            strTemp = regex.Replace(strTemp, "<frm:")
            Print #hdlFile, strTemp
        Else
            Print #hdlFile, strTemp
        End If
    End If
Next 'Pour toutes les lignes de la maquette

'-> Tag de fin de maquette
If IsCryptedFile Then
    Print #hdlFile, Crypt("[/MAQ]")
Else
    Print #hdlFile, "[/MAQ]"
End If

'-> Impression du spool en entier
If Page = -1 Or Page = -2 Then
    If Page = -1 Then
        '-> Indiquer la premi�re page � imprimer
        If aSpool.IsSelectionPage = True Then
            '-> indiquer qu'il faut impimer � partir de la page 0
            PageMini = 0
        Else
            '-> Imprimer � partir de la page 1
            PageMini = 1
        End If
        
        '-> Indiquer la page maxi
        PageMaxi = aSpool.nbPage
        
    Else
        '-> Indiquer la premi�re page � imprimer
        PageMini = PageMin
        PageMaxi = PageMax
        
    End If
    '-> On imprimer toutes les pages
    For i = PageMini To PageMaxi
        '-> Imprimer la page de s�lection
        If i = PageMini And PageMini <> 0 And noGarde = False And (aSpool.IsSelectionPage Or aSpool.GetPage(0) <> "") Then
            If IsCryptedFile Then
                Print #hdlFile, Crypt("[GARDEOPEN]")
            Else
                Print #hdlFile, "[GARDEOPEN]"
            End If
            '-> Impression de la page
            PrintPageToSpool aSpool, hdlFile, 0
            '-> Imprimer le tag de fin de s�lection
            If IsCryptedFile Then
                Print #hdlFile, Crypt("[GARDECLOSE]")
            Else
                Print #hdlFile, "[GARDECLOSE]"
            End If
        End If
        
        '-> Imprimer la tag d'ouverture de la page de s�lection
        If i = 0 Then
            If IsCryptedFile Then
                Print #hdlFile, Crypt("[GARDEOPEN]")
            Else
                Print #hdlFile, "[GARDEOPEN]"
            End If
        End If
        
        '-> Impression de la page
        PrintPageToSpool aSpool, hdlFile, i
        
        '-> Imprimer le tag de fin de s�lection si page = 0
        If i = 0 Then
            If IsCryptedFile Then
                Print #hdlFile, Crypt("[GARDECLOSE]")
            Else
                Print #hdlFile, "[GARDECLOSE]"
            End If
        '-> Imprimer un saut de page si pas derni�re page
        ElseIf i <> aSpool.nbPage Then
            If IsCryptedFile Then
                Print #hdlFile, Crypt("[PAGE]")
            Else
                Print #hdlFile, "[PAGE]"
            End If
        End If
    Next
Else
    '-> Imprimer la tag de la page de s�lection
    If Page = 0 Then
        If IsCryptedFile Then
            Print #hdlFile, Crypt("[GARDEOPEN]")
        Else
            Print #hdlFile, "[GARDEOPEN]"
        End If
    End If

    '-> Imprimer que la page d�sir�e
    PrintPageToSpool aSpool, hdlFile, Page
    
    '-> Imprimer la tag de fermture de la page de s�lection
    If Page = 0 Then
        If IsCryptedFile Then
            Print #hdlFile, Crypt("[GARDECLOSE]")
        Else
            Print #hdlFile, "[GARDECLOSE]"
        End If
    End If
    
End If

'-> Tag de fin de spool
If IsCryptedFile Then
    Print #hdlFile, Crypt("[/SPOOL]")
Else
    Print #hdlFile, "[/SPOOL]"
End If

'-> Fermer le fichier ouvert
Close #hdlFile

'-> Renvoyer le nom du fichier
CreateDetail = TempFile

Exit Function

GestError:

    '-> Renovyer une valeur ""
    CreateDetail = ""

End Function

Public Function CreateSpoolDetail(ByRef aFichier As Fichier, ByVal Page As Integer, Optional PageMin As Integer, Optional PageMax As Integer, Optional curSpool As Spool) As String

'---> Cette fonction cr�er un fichier tempo et retourne le nom du fichier � linker

Dim TempFile As String
Dim hdlFile As Integer
Dim DefMaq As String
Dim i As Integer
Dim PageMini As Integer
Dim PageMaxi As Integer
Dim aSpool As Spool

On Error GoTo GestError

'-> Obtenir un nom de fichier tempotaire
TempFile = Year(Now) & "-" & FORMAT(Month(Now), "00") & "-" & FORMAT(Day(Now), "00") & "-" & FORMAT(Hour(Now), "00") & "-" & FORMAT(Minute(Now), "00") & "-" & FORMAT(Second(Now), "00") & Right(CStr(GetTickCount), 2) & ".turbo"
TempFile = GetTempFileNameVB("WWW", True) & TempFile

'-> Obtenir un handle de fichier
hdlFile = FreeFile

'-> Ouverture du fichier ascii et �criture des pages
Open TempFile For Output As #hdlFile

For Each aSpool In aFichier.Spools
    If Not curSpool Is Nothing Then
        If aSpool.Key <> curSpool.Key Then GoTo suite2
    End If
    '-> Ecriture de la balise Spool
    If IsCryptedFile Then
        Print #hdlFile, Crypt("[SPOOL]")
    Else
        Print #hdlFile, "[SPOOL]"
    End If
    
    '-> Il faut dans un premier temps �crire la maquette
    If IsCryptedFile Then
        Print #hdlFile, Crypt("[MAQ]")
    Else
        Print #hdlFile, "[MAQ]"
    End If
    
    '-> R�cup�ration de la maquette
    DefMaq = aSpool.GetMaq
    DefMaq = Replace(DefMaq, "<tot:>", aSpool.nbPage)
    
    '-> Impression de la maquette
    For i = 1 To NumEntries(DefMaq, Chr(0))
        If IsCryptedFile Then
            Print #hdlFile, Crypt(Entry(i, DefMaq, Chr(0)))
        Else
            Print #hdlFile, Entry(i, DefMaq, Chr(0))
        End If
    Next 'Pour toutes les lignes de la maquette
    
    '-> Tag de fin de maquette
    If IsCryptedFile Then
        Print #hdlFile, Crypt("[/MAQ]")
    Else
        Print #hdlFile, "[/MAQ]"
    End If
    
    '-> Impression du spool en entier
    If Page = -1 Or Page = -2 Then
        If Page = -1 Then
            '-> Indiquer la premi�re page � imprimer
            If aSpool.IsSelectionPage = True Then
                '-> indiquer qu'il faut impimer � partir de la page 0
                PageMini = 0
            Else
                '-> Imprimer � partir de la page 1
                PageMini = 1
            End If
            
            '-> Indiquer la page maxi
            PageMaxi = aSpool.nbPage
            
        Else
            '-> Indiquer la premi�re page � imprimer
            PageMini = PageMin
            PageMaxi = PageMax
            
        End If
        '-> On imprimer toutes les pages
        For i = PageMini To PageMaxi
            '-> Imprimer la page de s�lection
            If i = PageMini And PageMini <> 0 And noGarde = False And aSpool.IsSelectionPage Then
                If IsCryptedFile Then
                    Print #hdlFile, Crypt("[GARDEOPEN]")
                Else
                    Print #hdlFile, "[GARDEOPEN]"
                End If
                '-> Impression de la page
                PrintPageToSpool aSpool, hdlFile, 0
                '-> Imprimer le tag de fin de s�lection
                If IsCryptedFile Then
                    Print #hdlFile, Crypt("[GARDECLOSE]")
                Else
                    Print #hdlFile, "[GARDECLOSE]"
                End If
            End If
            
            '-> Imprimer la tag d'ouverture de la page de s�lection
            If i = 0 Then
                If IsCryptedFile Then
                    Print #hdlFile, Crypt("[GARDEOPEN]")
                Else
                    Print #hdlFile, "[GARDEOPEN]"
                End If
            End If
            
            '-> Impression de la page
            PrintPageToSpool aSpool, hdlFile, i
            
            '-> Imprimer le tag de fin de s�lection si page = 0
            If i = 0 Then
                If IsCryptedFile Then
                    Print #hdlFile, Crypt("[GARDECLOSE]")
                Else
                    Print #hdlFile, "[GARDECLOSE]"
                End If
            '-> Imprimer un saut de page si pas derni�re page
            ElseIf i <> aSpool.nbPage Then
                If IsCryptedFile Then
                    Print #hdlFile, Crypt("[PAGE]")
                Else
                    Print #hdlFile, "[PAGE]"
                End If
            End If
        Next
    Else
        '-> Imprimer la tag de la page de s�lection
        If Page = 0 Then
            If IsCryptedFile Then
                Print #hdlFile, Crypt("[GARDEOPEN]")
            Else
                Print #hdlFile, "[GARDEOPEN]"
            End If
        End If
    
        '-> Imprimer que la page d�sir�e
        PrintPageToSpool aSpool, hdlFile, Page
        
        '-> Imprimer la tag de fermture de la page de s�lection
        If Page = 0 Then
            If IsCryptedFile Then
                Print #hdlFile, Crypt("[GARDECLOSE]")
            Else
                Print #hdlFile, "[GARDECLOSE]"
            End If
        End If
        
    End If
    
    '-> Tag de fin de spool
    If IsCryptedFile Then
        Print #hdlFile, Crypt("[/SPOOL]")
    Else
        Print #hdlFile, "[/SPOOL]"
    End If
suite2:
Next
'-> Fermer le fichier ouvert
Close #hdlFile

'-> Renvoyer le nom du fichier
CreateSpoolDetail = TempFile

Exit Function

GestError:

    '-> Renovyer une valeur ""
    CreateSpoolDetail = ""

End Function

Public Function CreateSpool(NomFichier As String, ByVal PageMin As Long, ByVal PageMax As Long, Optional SaveName As String) As String

'---> Cette fonction cr�er un fichier tempo et retourne le nom du fichier � linker

Dim TempFile As String
Dim hdlFile As Integer
Dim hdlFile2 As Integer
Dim LectureMaquette As Boolean
Dim LecturePage As Boolean
Dim DefMaq As String
Dim i As Long
Dim PageMini As Long
Dim PageMaxi As Long
Dim iPage As Long
Dim Ligne As String

On Error GoTo GestError
frmWait.Show

'-> Obtenir un nom de fichier tempotaire
TempFile = Year(Now) & "-" & FORMAT(Month(Now), "00") & "-" & FORMAT(Day(Now), "00") & "-" & FORMAT(Hour(Now), "00") & "-" & FORMAT(Minute(Now), "00") & "-" & FORMAT(Second(Now), "00") & Right(CStr(GetTickCount), 2) & ".turbo"
TempFile = GetTempFileNameVB("WWW", True) & TempFile

If SaveName <> "" Then TempFile = SaveName

'-> Obtenir un handle de fichier
hdlFile = FreeFile

'-> Ouverture du fichier ascii et �criture des pages
Open TempFile For Output As #hdlFile

'-> Ecriture de la balise Spool
If IsCryptedFile Then
    Print #hdlFile, Crypt("[SPOOL]")
Else
    Print #hdlFile, "[SPOOL]"
End If

'-> Il faut dans un premier temps �crire la maquette
If IsCryptedFile Then
    Print #hdlFile, Crypt("[MAQ]")
Else
    Print #hdlFile, "[MAQ]"
End If

'-> on va cherhcer la maquette dans le fichier d'origine
hdlFile2 = FreeFile
Open NomFichier For Input As #hdlFile2

'-> Boucle d'analyse du fichier pour inserer la maquette
Do While Not EOF(hdlFile2)
    '-> Lecture de la ligne
    Line Input #hdlFile2, Ligne
    '-> Decrypter la source
    If LectureMaquette Then Print #hdlFile, Ligne
    Ligne = DeCrypt(Ligne)
    Select Case Trim(UCase$(Ligne))
        Case "[MAQ]"
            LectureMaquette = True
        Case "[/MAQ]"
            LectureMaquette = False
            iPage = iPage + 1
            If iPage >= PageMin And iPage <= PageMax Then LecturePage = True
            GoTo suite
    End Select 'Selon la ligne que l'on est en train de lire
Loop 'Boucle d'analyse du fichier
suite:
'-> Tag de fin de maquette
If IsCryptedFile Then
    Print #hdlFile, Crypt("[/MAQ]")
Else
    Print #hdlFile, "[/MAQ]"
End If

'-> Boucle d'analyse du fichier pour inserer les pages
Do While Not EOF(hdlFile2)
    '-> Lecture de la ligne
    Line Input #hdlFile2, Ligne
    '-> Decrypter la source
    If LectureMaquette Then Print #hdlFile, Ligne
    Ligne = DeCrypt(Ligne)
    If LecturePage Then Print #hdlFile, Ligne
    Select Case Trim(UCase$(Ligne))
        Case "[PAGE]"
            iPage = iPage + 1
            If iPage >= PageMin And iPage <= PageMax Then LecturePage = True
            If iPage Mod 789 = 0 Then
                frmWait.Label1.Caption = "Cr�ation " & SaveName & " Lecture en cours... " & iPage
                DoEvents
            End If
    
    End Select 'Selon la ligne que l'on est en train de lire
    If iPage > PageMax Then GoTo suite2
Loop 'Boucle d'analyse du fichier
suite2:

'-> Tag de fin de spool
If IsCryptedFile Then
    Print #hdlFile, Crypt("[/SPOOL]")
Else
    Print #hdlFile, "[/SPOOL]"
End If

'-> Fermer le fichier ascii
Close #hdlFile2
Close #hdlFile
Unload frmWait

'-> Renvoyer le nom du fichier
CreateSpool = TempFile

Exit Function

GestError:

    '-> Renovyer une valeur ""
    CreateSpool = ""

End Function

Public Function CreateSpoolNoGarde(NomFichier As String, SaveName As String) As String

'---> Cette fonction cr�er un fichier tempo sans les page de garde

Dim TempFile As String
Dim hdlFile As Integer
Dim hdlFile2 As Integer
Dim LectureGarde As Boolean
Dim DefMaq As String
Dim i As Long
Dim Ligne As String

On Error GoTo GestError
frmWait.Show

'-> Obtenir un nom de fichier tempotaire
TempFile = Year(Now) & "-" & FORMAT(Month(Now), "00") & "-" & FORMAT(Day(Now), "00") & "-" & FORMAT(Hour(Now), "00") & "-" & FORMAT(Minute(Now), "00") & "-" & FORMAT(Second(Now), "00") & Right(CStr(GetTickCount), 2) & ".turbo"
TempFile = GetTempFileNameVB("WWW", True) & TempFile

If SaveName <> "" Then TempFile = SaveName

'-> Obtenir un handle de fichier
hdlFile = FreeFile

'-> Ouverture du fichier ascii et �criture des pages
Open TempFile For Output As #hdlFile

'-> on va cherhcer la maquette dans le fichier d'origine
hdlFile2 = FreeFile
Open NomFichier For Input As #hdlFile2

'-> Boucle d'analyse du fichier pour inserer la maquette
Do While Not EOF(hdlFile2)
    '-> Lecture de la ligne
    Line Input #hdlFile2, Ligne
    '-> Decrypter la source
    If Not LectureGarde And "[GARDEOPEN]" <> Trim(UCase$(Ligne)) Then Print #hdlFile, Ligne
    Ligne = DeCrypt(Ligne)
    Select Case Trim(UCase$(Ligne))
        Case "[GARDEOPEN]"
            LectureGarde = True
        Case "[GARDECLOSE]"
            LectureGarde = False
    End Select 'Selon la ligne que l'on est en train de lire
Loop 'Boucle d'analyse du fichier

'-> Fermer le fichier ascii
Close #hdlFile2
Close #hdlFile
Unload frmWait

'-> Renvoyer le nom du fichier
CreateSpoolNoGarde = TempFile

Exit Function

GestError:

    '-> Renovyer une valeur ""
    CreateSpoolNoGarde = ""

End Function

Private Sub PrintPageToSpool(ByRef aSpool As Spool, ByVal hdlFile As Integer, ByVal pageToPrint As Integer)

'---> Cette fonction imprime le contenu d'une page d'un spool

Dim i As Integer
    
Dim DefPage As String

'-> Recup�ration de la d�finition de la page
DefPage = aSpool.GetPage(pageToPrint)

'-> Impression de la page
For i = 1 To NumEntries(DefPage, Chr(0))
    If IsCryptedFile Then
        Print #hdlFile, Crypt(Entry(i, DefPage, Chr(0)))
    Else
        Print #hdlFile, Entry(i, DefPage, Chr(0))
    End If
Next

End Sub

Public Sub DeleteFile(FileToDelete As String, ActionFlag As Long)
 
'---> Cette proc�dure supprime un fichier

Dim SHFileOp As SHFILEOPSTRUCT
Dim Res As Long
 
'-> Ajouter un caractere de fin de string
FileToDelete = FileToDelete & Chr$(0)
 
'-> Setting de la structure
With SHFileOp
   .wFunc = FO_DELETE
   .pFrom = FileToDelete
   .fFlags = ActionFlag
End With
 
'-> suppression du fichier
Res = SHFileOperation(SHFileOp)

'-> Si on n' a pas fait annuler : virer le fichier de l'interface
If SHFileOp.fAborted = False Then CloseFichier MDIMain.TreeNaviga.SelectedItem.Key

End Sub

Public Sub DrawTempo(apic As PictureBox)

apic.Line (0, 0)-((TailleLue / TailleTotale) * apic.ScaleWidth, apic.ScaleHeight), &HC00000, BF

End Sub

Public Function IsTextInPage(ByVal aSpool As Spool, ByVal i As Integer) As Boolean
'--> on regarde si le texte recherch� est present sur la page
Dim strPage As String
Dim strTemp As String

On Error GoTo GestError

'-> on reccupere la page
strPage = aSpool.GetPage(i)

'-> premiere verification sans tenir compte des caracteres parasites
If InStr(1, strPage + aSpool.GetMaq, Replace(FindText, ",", "."), vbTextCompare) <> 0 Then IsTextInPage = True
If InStr(1, strPage + aSpool.GetMaq, Replace(FindText, ".", ","), vbTextCompare) <> 0 Then IsTextInPage = True
If InStr(1, strPage + aSpool.GetMaq, FindText, vbTextCompare) <> 0 Then IsTextInPage = True
If InStr(1, Replace(strPage, ",", ""), FindText, vbTextCompare) <> 0 Then IsTextInPage = True

GestError:

End Function

Private Function GetNewFindText(TextToFind As String, CellContenu As String, NbOccurence As Integer) As String
'--> cette fonction retravaille le texte recherch� car il peu contenir un separateur de millier
Dim i As Integer
Dim j As Integer
Dim k As Integer
Dim TempCell As String
Dim car As String
Dim find As String

'-> si pas present quitter
If InStr(1, CellContenu, SepMil) = 0 Then GoTo GestError

TextToFind = UCase(TextToFind)
CellContenu = UCase(CellContenu)

'-> on initialise le premier caractere a rechercher
j = 1
find = Mid(TextToFind, j, 1)

'-> on reconstruit la chaine � rechercher en lisant le contenu
For i = 1 To Len(CellContenu)
    car = Mid(CellContenu, i, 1)
    Select Case car
        Case find
            TempCell = TempCell + car
            '-> on regarde si on a fini
            If Replace(TempCell, SepMil, "") = TextToFind Then
                GetNewFindText = TempCell
                Exit Function
            End If
            j = j + 1
            find = Mid(TextToFind, j, 1)
        Case SepMil
            TempCell = TempCell + SepMil
        Case Else '-> c'est pas bon
            j = 1
            TempCell = ""
    End Select
Next
Exit Function
GestError:
GetNewFindText = TextToFind
End Function

Private Sub DrawFindText(TextToFind As String, pDraw, Lrect As Long, Rrect As Long, tRECT As Long, Brect As Long, aCell)

'---> Cette fonction imprime le texte recherch� sous forme RTF

Dim aRtf As RichTextBox
Dim fr As FORMATRANGE
Dim lTextOut As Long, lTextAmt As Long, Res As Long
Dim aRect As RECT
Dim hdlPen As Long
Dim OldPen As Long
Dim hdlBrush As Long
Dim oldBrush As Long
Dim i As Long
Dim DifH As Integer
Dim DifW As Integer
Dim hdlRgn As Long

On Error GoTo GestErr

'-> Mettre l'�diteur RTF aux bonnes dimensions
Load frmLib.Rtf(IdRTf + 1)
Set aRtf = frmLib.Rtf(IdRTf + 1)

aRtf.width = Sortie.ScaleX(Rrect - Lrect, 3, 1)
aRtf.height = Sortie.ScaleX(Brect - tRECT, 3, 1)
aRtf.Font.Name = aCell.FontName
aRtf.Font.size = aCell.FontSize * Zoom
aRtf.Font.Bold = aCell.FontBold
aRtf.Font.Italic = aCell.FontItalic
aRtf.Font.Underline = aCell.FontUnderline
aRtf.BackColor = aCell.BackColor

''-> on bute eventuellement les blancs � dorite
'TextToFind = RTrim(TextToFind)

Select Case aCell.CellAlign
    Case 3, 6, 9
        '-> R�cup�ration du code RTF sans les blancs de droite
        aRtf.Text = RTrim(aCell.ContenuCell)
    Case Else
        '-> R�cup�ration du code RTF
        aRtf.Text = aCell.ContenuCell
End Select
'-> on pointe sur la premiere occurence du texte recherch�
aRtf.SelStart = aRtf.find(TextToFind)
'-> Dans le cas ou on a plusieurs occurences, selectionner le bon texte
Do While FindPos2 <= FindPos
    FindPos2 = FindPos2 + 1
    aRtf.SelStart = aRtf.find(TextToFind, aRtf.SelStart + Len(TextToFind))
Loop

'-> On met en evidence le texte recherch�
aRtf.SelLength = Len(TextToFind)
aRtf.SelColor = vbWhite
Call SetBackColorSel(aRtf.Hwnd, vbBlack)

'-> Setting du rect de dessin
aRect.Left = Lrect
aRect.Top = tRECT
aRect.Right = Rrect
aRect.Bottom = Brect

Dim lPtr As Long
Dim acontenucell As String
If isUTF8(aCell.ContenuCell) Then
    acontenucell = UTF8ToA(aCell.ContenuCell)
Else
    acontenucell = aCell.ContenuCell
End If
lPtr = StrPtr(acontenucell)

'-> dans le cas de l'alignement droite l'api vire les blancs de droite
Select Case aCell.CellAlign
    Case 3, 6, 9
        '-> On calcul les coordonn�es exactes du texte
        DrawTextW Sortie.hdc, lPtr, Len(RTrim(acontenucell)), aRect, DT_CALCRECT  'Or pDraw
        i = aRect.Right
        aRect.Left = Lrect
        aRect.Top = tRECT
        aRect.Right = Rrect
        aRect.Bottom = Brect
        '-> On calcul les coordonn�es exactes du texte
        DrawTextW Sortie.hdc, lPtr, Len(acontenucell), aRect, DT_CALCRECT  'Or pDraw
        i = aRect.Right - i
        '-> on calcul les differences de taille
        DifH = (Brect - tRECT) - (aRect.Bottom - aRect.Top)
        DifW = (Rrect - Lrect) - (aRect.Right - aRect.Left)
        aRect.Top = tRECT
        aRect.Bottom = Brect
    Case Else
        '-> On calcul les coordonn�es exactes du texte
        DrawTextW Sortie.hdc, lPtr, Len(acontenucell), aRect, DT_CALCRECT Or pDraw
        '-> on calcul les differences de taille
        DifH = (Brect - tRECT) - (aRect.Bottom - aRect.Top) '- 1
        DifW = (Rrect - Lrect) - (aRect.Right - aRect.Left)
End Select

Select Case aCell.CellAlign
    Case 1 'Left Top
        aRtf.SelAlignment = 0
    Case 2 'Center Top
        aRtf.SelAlignment = 2
    Case 3 'Right Top
        aRtf.SelAlignment = 1
        Rrect = Rrect - i
    Case 4 'Left Center
        aRtf.SelAlignment = 0
        Lrect = Lrect + i
        tRECT = tRECT + DifH  '/2'+ 1
        Rrect = Rrect + i + 100
        Brect = Brect + DifH
    Case 5 'Center Center
        aRtf.SelAlignment = 2
        tRECT = tRECT + DifH
        Brect = Brect + DifH
        Lrect = Lrect - 1 '- DifW - 1
        Rrect = Rrect + 1 '+ DifW + 1
    Case 6 'Right Center
        aRtf.SelAlignment = 1
        tRECT = tRECT + DifH / 2
        Rrect = Rrect - i
        Lrect = Lrect - i
    Case 7 'Left Bottom
        aRtf.SelAlignment = 0
        tRECT = tRECT + DifH
    Case 8 'Center Bottom
        aRtf.SelAlignment = 2
        tRECT = tRECT + DifH
    Case 9 'Right Bottom
        aRtf.SelAlignment = 1
        Rrect = Rrect - i
        tRECT = tRECT + DifH
End Select

'-> Imprimer le fond de l'objet
hdlBrush = CreateSolidBrush(Sortie.BackColor)
hdlPen = CreatePen(PS_NULL, 1, QBColor(15))

'-> S�lection du pinceau dans le Contexte
oldBrush = SelectObject(Sortie.hdc, hdlBrush)
OldPen = SelectObject(Sortie.hdc, hdlPen)

'-> S�lectionner les anciens objets et supprimer les objets GDI non utilis�s
If OldPen <> 0 Then
    SelectObject Sortie.hdc, OldPen
    DeleteObject hdlPen
End If

If oldBrush <> 0 Then
    SelectObject Sortie.hdc, oldBrush
    DeleteObject hdlBrush
End If

'-> Initialiser la srtucture Formatrange
fr.hdc = Sortie.hdc
fr.hdcTarget = Sortie.hdc
fr.chrg.cpMin = 0
fr.chrg.cpMax = -1

'-> Intialisation du rectangle destination
fr.rc.Left = Sortie.ScaleX(Lrect, 3, 1)  'PosX
fr.rc.Top = Sortie.ScaleY(tRECT, 3, 1)  'posY
fr.rc.Right = Sortie.ScaleX(Rrect, 3, 1)  '- Lrect + PosX
fr.rc.Bottom = Sortie.ScaleY(Brect, 3, 1)  '- Trect + posY

'-> Initialisation du rectangle de source
fr.rcPage.Left = 0
fr.rcPage.Top = 0
fr.rcPage.Right = Sortie.ScaleWidth 'Rrect - Lrect + PosX
fr.rcPage.Bottom = Sortie.ScaleHeight 'Brect + Trect + posY

'-> Faire un setting du mode de restitution
'Res = SetMapMode(Sortie.hdc, MM_TEXT)

'-> initialisation des variables de pointage de texte
lTextOut = 0
lTextAmt = SendMessage(aRtf.Hwnd, WM_GETTEXTLENGTH, 0, 0)

'-> Impression du Rtf
Do While lTextOut < lTextAmt
    lTextOut = SendMessage(aRtf.Hwnd, EM_FORMATRANGE, True, fr)
    If lTextOut < lTextAmt Then
        fr.chrg.cpMin = lTextOut
        fr.chrg.cpMax = -1
    End If
Loop

'-> Lib�rer la ressource associ�e au RTF : VERSION COMPILEE
Res = SendMessage(aRtf.Hwnd, EM_FORMATRANGE, -1, Null)
If Res = 0 Then
    'MsgBox "Erreur dans la lib�ration du context"
    Res = SendMessage(aRtf.Hwnd, EM_FORMATRANGE, -1, vbNullString)
    'MsgBox "Apr�s Seconde tentative " & Res
End If

'-> Lib�rer le pointeur sur le controle RTF
Set aRtf = Nothing

GestErr:
Unload frmLib.Rtf(IdRTf + 1)
End Sub

Private Function GetBackColorSel(ByVal RichHwnd As Long) As OLE_COLOR
    Dim iniformat As FORMAT

    ' Set BackColor a masqu�
    iniformat.dwMask = CFM_BACKCOLOR
    iniformat.cbSize = Len(iniformat)
    ' Obtenez la structure du format des caract�res s�lectionner
    SendMessage RichHwnd, EM_GETCHARFORMAT, SCF_SELECTION, iniformat
    
    ' Retour le BackColour de la s�lection
    GetBackColorSel = iniformat.crBackColor
End Function

Private Function SetBackColorSel(ByVal RichHwnd As Long, ByVal NouveauFontBackColorSel As OLE_COLOR)
    Dim iniformat As FORMAT
    ' Set BackColor a masqu�
    iniformat.dwMask = CFM_BACKCOLOR
    ' Si le nouveau backcolour est mis � -1 alors nous avons mis le
    ' Backcolour RichTextbox a zero (vbwhite)
    If NouveauFontBackColorSel = -1 Then
        iniformat.dwEffects = CFE_AUTOBACKCOLOR
        iniformat.crBackColor = -1
    Else
    ' donner la nouvelle couleur � BackColour
        iniformat.crBackColor = ChangerColor(NouveauFontBackColorSel)
    End If
    ' Nous avons besoin de passer la dimension de la structure comme un
    ' partie de la structure.
    iniformat.cbSize = Len(iniformat)
    ' Envoyez le message et le nouveau format de caract�re au RichTextbox
    SendMessage RichHwnd, EM_SETCHARFORMAT, SCF_SELECTION, iniformat
End Function

Private Function ChangerColor(ByVal Couleur As OLE_COLOR, Optional Palette As Long = 0) As Long
    If OleTranslateColor(Couleur, Palette, ChangerColor) Then
        ChangerColor = -1
    End If
End Function

Public Sub CmdDeZoom(Yvalue As Integer, Xvalue As Integer)
    Dim l As Long
    l = StretchBlt(Sortie.hdc, 0, 0, Xvalue, Yvalue, Sortie.hdc, 0, 0, Sortie.ScaleWidth, Sortie.ScaleHeight, ScrCopy)
    Sortie.Refresh
End Sub

Public Function GetTitrePage(aSpool As Spool, NumPage As Long) As String
'--> Cette fonction r�cup�re le titre d'une page
Dim ParamTitre As String
Dim Ligne As String
Dim Rg As String
Dim Dz As String
Dim i As Integer
Dim j As Integer


'-> on regarde si on a specifi� un titre
If InStr(1, aSpool.GetMaq, "[TITRE]") = 0 Then Exit Function

'-> on r�cup�re le param�trage
ParamTitre = Entry(2, aSpool.GetMaq, "[TITRE]")
ParamTitre = Entry(1, ParamTitre, "\TITRE�End")
ParamTitre = Mid(ParamTitre, 8)

'-> on parcours les titres � r�cuperer
For i = 2 To NumEntries(ParamTitre, "\")
    Rg = Entry(i, ParamTitre, "\")
    Dz = "^" + Entry(1, Entry(2, Rg, "�"), Chr(0))
    Rg = "[" + Entry(1, Rg, "�") + "]"
    '-> on recherche la ligne correspondante
    For j = 1 To NumEntries(aSpool.GetPage(NumPage), Chr(0))
        Ligne = Trim(Entry(j, aSpool.GetPage(NumPage), Chr(0)))
        If InStr(1, UCase(Ligne), UCase(Rg)) Then
            '-> on regarde si on trouve le diez
            If InStr(1, UCase(Ligne), UCase(Rg)) <> 0 And InStr(1, Ligne, Dz) Then
                '-> on est sur une bonne ligne on r�cup�re la valeur du diez
                Dz = RTrim(Mid(Entry(1, Entry(1, Entry(2, Ligne, Dz), "^"), "}"), 5))
                If InStr(1, Dz, "[\") <> 0 Then Dz = ""
                If GetTitrePage = "" Then
                    GetTitrePage = Dz
                Else
                    GetTitrePage = GetTitrePage + " - " + Dz
                End If
                '-> sortir de la boucle
                Exit For
            End If
        End If
    Next
Next

End Function

Public Function GetRupture(aSpool As Spool) As String
'--> Cette fonction r�cup�re la rupture d'une maquette

'-> on regarde si defini dans le fichier de parametrage
GetRupture = GetIniString(Trim(UCase("dispatch")), Trim(UCase(GetMaqName(aSpool))), App.Path & "\TurboDispatch.ini", False)
If GetRupture <> "" Then Exit Function

'-> on regarde si on a specifi� un titre
If InStr(1, aSpool.GetMaq, "[MAIL]") = 0 Then
    GetRupture = "[NO]"
    Exit Function
End If

'-> on r�cup�re le param�trage
GetRupture = Replace(Entry(2, aSpool.GetMaq, "[MAIL]"), Chr(1), "")
GetRupture = Entry(1, GetRupture, "\MAIL�End")
GetRupture = Mid(GetRupture, 8)

End Function

Public Function GetMail(aSpool As Spool) As String
'--> Cette fonction r�cup�re la dieze des mails de la maquette

'-> on regarde si defini dans le fichier de parametrage
GetMail = GetIniString(Trim(UCase("default")), Trim(UCase(GetMaqName(aSpool))), App.Path & "\TurboMail.ini", False)
If GetMail <> "" Then Exit Function

'-> on regarde si on a specifi� un titre
If InStr(1, aSpool.GetMaq, "[MAIL]") = 0 Then
    GetMail = "[NO]"
    Exit Function
End If

'-> on r�cup�re le param�trage
GetMail = Replace(Entry(2, aSpool.GetMaq, "[MAIL]"), Chr(1), "")
GetMail = Entry(1, GetMail, "\MAIL�End")
GetMail = Mid(GetMail, 8)

End Function

Public Function GetMaqName(aSpool As Spool) As String
'--> Cette fonction r�cup�re la rupture d'une maquette

'-> on r�cup�re le param�trage
GetMaqName = Replace(Entry(2, aSpool.GetMaq, "\Nom"), Chr(1), "")
GetMaqName = Entry(1, GetMaqName, Chr(0) & "\Date�")
GetMaqName = Mid(GetMaqName, 5)

End Function

Public Function GetTitreMail(aSpool As Spool, NumPage As Long) As String
'--> Cette fonction r�cup�re le titre d'une page
Dim ParamTitre As String
Dim Ligne As String
Dim Rg As String
Dim Dz As String
Dim i As Integer
Dim j As Integer
Dim topOK As Boolean
Dim topFIND As Boolean

On Error Resume Next

'-> on regarde si defini dans le fichier de parametrage
ParamTitre = GetIniString(Trim(UCase("default")), Trim(UCase(GetMaqName(aSpool))), App.Path & "\TurboMail.ini", False)
If ParamTitre = "" Then
    '-> on regarde si on a specifi� un titre
    If InStr(1, aSpool.GetMaq, "[MAIL]") = 0 Then
            GetTitreMail = "[NO]"
            Exit Function
    Else
        '-> on r�cup�re le param�trage
        ParamTitre = Replace(Entry(2, aSpool.GetMaq, "[MAIL]"), Chr(1), "")
        ParamTitre = Entry(1, ParamTitre, "\MAIL�End")
        ParamTitre = Mid(ParamTitre, 8)
        ParamTitre = Replace(ParamTitre, Chr(0), "")
        '-> on sauvegarde
        SetIniString Trim(UCase("default")), Trim(UCase(GetMaqName(aSpool))), App.Path & "\TurboMail.ini", ParamTitre
    End If
End If

topOK = False
topFIND = False

'-> on parcours les titres � r�cuperer
For i = 1 To NumEntries(ParamTitre, "\")
    Rg = Entry(i, ParamTitre, "\")
    Dz = "^" + Entry(1, Entry(2, Rg, "�"), Chr(0))
    Rg = "[" + Entry(1, Rg, "�") + "]"
    '-> on recherche la ligne correspondante
    For j = 1 To NumEntries(aSpool.GetPage(NumPage), Chr(0))
        Ligne = Trim(Entry(j, aSpool.GetPage(NumPage), Chr(0)))
        If InStr(1, UCase(Ligne), UCase(Rg)) Then
            '-> on regarde si on trouve le diez
            If InStr(1, UCase(Ligne), UCase(Rg)) <> 0 And InStr(1, Ligne, Dz) Then
                '-> on est sur une bonne ligne on r�cup�re la valeur du diez
                topOK = True
                topFIND = True
                Dz = RTrim(Mid(Entry(1, Entry(1, Entry(2, Ligne, Dz), "^"), "}"), 5))
                If InStr(1, Dz, "[\") <> 0 Then Dz = ""
                If Dz = "***" Then topOK = False
                If GetTitreMail = "" Then
                    GetTitreMail = Dz
                Else
                    GetTitreMail = GetTitreMail + "�" + Dz
                End If
                '-> sortir de la boucle
                Exit For
            End If
        End If
    Next
    If topFIND = True And Dz = "" And i = 1 Then GetTitreMail = " "
    topFIND = False
Next
If topOK = False Then GetTitreMail = "[NO]"
If Entry(1, GetTitreMail, "�") = "***" Then GetTitreMail = "[NO]"

End Function

Public Function GetTitreRupture(aSpool As Spool, NumPage As Long) As String
'--> Cette fonction r�cup�re le titre d'une page
Dim ParamTitre As String
Dim Ligne As String
Dim Rg As String
Dim Dz As String
Dim i As Integer
Dim j As Integer
Dim topOK As Boolean
Dim topFIND As Boolean

'-> on regarde si on a specifi� un titre
'If InStr(1, aSpool.GetMaq, "[MAIL]") = 0 Then
    ParamTitre = GetRupture(aSpool)
    If ParamTitre = "[NO]" Then
        GetTitreRupture = "[NO]"
        Exit Function
    End If
'Else
'    '-> on r�cup�re le param�trage
'    ParamTitre = Replace(Entry(2, aSpool.GetMaq, "[MAIL]"), Chr(1), "")
'    ParamTitre = Entry(1, ParamTitre, "\MAIL�End")
'    ParamTitre = Mid(ParamTitre, 8)
'End If

topOK = False
topFIND = False

'-> on parcours les titres � r�cuperer
For i = 1 To NumEntries(ParamTitre, "\")
    Rg = Entry(i, ParamTitre, "\")
    Dz = "^" + Entry(1, Entry(2, Rg, "�"), Chr(0))
    Rg = "[" + Entry(1, Rg, "�") + "]"
    '-> on recherche la ligne correspondante
    For j = 1 To NumEntries(aSpool.GetPage(NumPage), Chr(0))
        Ligne = Trim(Entry(j, aSpool.GetPage(NumPage), Chr(0)))
        If InStr(1, UCase(Ligne), UCase(Rg)) Then
            '-> on regarde si on trouve le diez
            If InStr(1, UCase(Ligne), UCase(Rg)) <> 0 And InStr(1, Ligne, Dz) Then
                '-> on est sur une bonne ligne on r�cup�re la valeur du diez
                topOK = True
                topFIND = True
                Dz = RTrim(Mid(Entry(1, Entry(1, Entry(2, Ligne, Dz), "^"), "}"), 5))
                If InStr(1, Dz, "[\") <> 0 Then Dz = ""
                If Dz = "***" Then topOK = False
                If GetTitreRupture = "" Then
                    GetTitreRupture = Dz
                Else
                    GetTitreRupture = GetTitreRupture + "�" + Dz
                End If
                '-> sortir de la boucle
                Exit For
            End If
        End If
    Next
    If topFIND = True And Dz = "" And i = 1 Then GetTitreRupture = " "
    topFIND = False
Next
If topOK = False Then GetTitreRupture = "[NO]"
If Entry(1, GetTitreRupture, "�") = "***" Then GetTitreRupture = "[NO]"

End Function

Public Function GetTitrePdf(aSpool As Spool, NumPage As Long) As String
'--> Cette fonction r�cup�re le titre d'une page
Dim ParamTitre As String
Dim Ligne As String
Dim Rg As String
Dim Dz As String
Dim i As Integer
Dim j As Integer
Dim topOK As Boolean
Dim topFIND As Boolean

'-> on regarde si on a specifi� un titre
If InStr(1, aSpool.GetMaq, "[MAIL]") = 0 Then
    GetTitrePdf = "[NO]"
    Exit Function
End If

'-> on r�cup�re le param�trage
ParamTitre = Entry(2, aSpool.GetMaq, "[MAIL]")
ParamTitre = Entry(1, ParamTitre, "\MAIL�End")
ParamTitre = Mid(ParamTitre, 8)

topOK = False
topFIND = False

'-> on parcours les titres � r�cuperer
For i = 1 To NumEntries(ParamTitre, "\")
    Rg = Entry(i, ParamTitre, "\")
    Dz = "^" + "dpdf"
    Rg = "[" + Entry(1, Rg, "�") + "]"
    '-> on recherche la ligne correspondante
    For j = 1 To NumEntries(aSpool.GetPage(NumPage), Chr(0))
        Ligne = Trim(Entry(j, aSpool.GetPage(NumPage), Chr(0)))
        If InStr(1, UCase(Ligne), UCase(Rg)) Then
            '-> on regarde si on trouve le diez
            If InStr(1, UCase(Ligne), UCase(Rg)) <> 0 And InStr(1, Ligne, Dz) Then
                '-> on est sur une bonne ligne on r�cup�re la valeur du diez
                topOK = True
                topFIND = True
                Dz = RTrim(Mid(Entry(1, Entry(1, Entry(2, Ligne, Dz), "^"), "}"), 5))
                If InStr(1, Dz, "[\") <> 0 Then Dz = ""
                If Dz = "***" Then topOK = False
                If GetTitrePdf = "" Then
                    GetTitrePdf = Dz
                Else
                    GetTitrePdf = GetTitrePdf + "�" + Dz
                End If
                '-> sortir de la boucle
                Exit For
            End If
        End If
    Next
    If topFIND = True And Dz = "" And i = 1 Then GetTitrePdf = " "
    topFIND = False
Next
If topOK = False Then GetTitrePdf = "[NO]"
End Function

Public Function GetLibelVariableForCell(aBlock As Block, aSpool As Spool, aCell As Cellule) As String
'--> Cette fonction r�cup�re les libell�s variables d'un spool
Dim ParamTitre As String
Dim Ligne As String
Dim Rg As String
Dim Dz As String
Dim i As Integer
Dim j As Integer


'-> on regarde si on a specifi� un titre
If InStr(1, aSpool.GetMaq, "[LIBELVARIABLE]") = 0 Then Exit Function

'-> on r�cup�re le param�trage
ParamTitre = Entry(2, aSpool.GetMaq, "[LIBELVARIABLE]")
ParamTitre = Entry(1, ParamTitre, "\LIBELVARIABLE�End")
ParamTitre = Mid(ParamTitre, 16)

'-> on parcours les titres � r�cuperer
For i = 2 To NumEntries(ParamTitre, "\")
    Rg = Entry(i, ParamTitre, "\")
    Dz = "^" + Entry(1, Entry(2, Rg, "�"), Chr(0))
    Rg = "[" + Entry(1, Rg, "�") + "]"
    If UCase("[TB-" + aBlock.NomTb + "(BLK-" + aBlock.Nom + ")]") = UCase(Rg) Then
        If aCell.LibelVariable = "" Then
            aCell.LibelVariable = "^" + Entry(2, Entry(i, ParamTitre, "\"), "�") & "�" & Entry(3, Entry(i, ParamTitre, "\"), "�")
        Else
            aCell.LibelVariable = aCell.LibelVariable + "�^" + Entry(2, Entry(i, ParamTitre, "\"), "�") & "�" & Entry(3, Entry(i, ParamTitre, "\"), "�")
        End If
    End If
Next

End Function

Public Function GetLibelVariableForSection(aSpool As Spool, aSection As Section) As String
'--> Cette fonction r�cup�re les libell�s variables d'un spool
Dim ParamTitre As String
Dim Ligne As String
Dim Rg As String
Dim Dz As String
Dim i As Integer
Dim j As Integer


'-> on regarde si on a specifi� un titre
If InStr(1, aSpool.GetMaq, "[LIBELVARIABLE]") = 0 Then Exit Function

'-> on r�cup�re le param�trage
ParamTitre = Entry(2, aSpool.GetMaq, "[LIBELVARIABLE]")
ParamTitre = Entry(1, ParamTitre, "\LIBELVARIABLE�End")
ParamTitre = Mid(ParamTitre, 16)

'-> on parcours les titres � r�cuperer
For i = 2 To NumEntries(ParamTitre, "\")
    Rg = Entry(i, ParamTitre, "\")
    Dz = "^" + Entry(1, Entry(2, Rg, "�"), Chr(0))
    Rg = "[" + Entry(1, Rg, "�") + "]"
    If UCase("[NV-" + aSection.Nom + "-STD(TXT-Section)]") = UCase(Rg) Then
        If aSection.LibelVariable = "" Then
            aSection.LibelVariable = "^" + Entry(2, Entry(i, ParamTitre, "\"), "�") & "�" & Entry(3, Entry(i, ParamTitre, "\"), "�")
        Else
            aSection.LibelVariable = aSection.LibelVariable + "�^" + Entry(2, Entry(i, ParamTitre, "\"), "�") & "�" & Entry(3, Entry(i, ParamTitre, "\"), "�")
        End If
    End If
    If UCase("[ST-" + aSection.Nom + "(TXT-Section)]") = UCase(Rg) Then
        If aSection.LibelVariable = "" Then
            aSection.LibelVariable = "^" + Entry(2, Entry(i, ParamTitre, "\"), "�") & "�" & Entry(3, Entry(i, ParamTitre, "\"), "�")
        Else
            aSection.LibelVariable = aSection.LibelVariable + "�^" + Entry(2, Entry(i, ParamTitre, "\"), "�") & "�" & Entry(3, Entry(i, ParamTitre, "\"), "�")
        End If
    End If
Next

End Function

Public Function IsOpenoffice() As Boolean
'--> Cette proc�dure essaye de cr�er un lien OLE vers Openoffice pour tester que les bibli sont install�es
Dim aServiceManager As Object

On Error GoTo OfficeError

'-> Essayer de cr�er un lien OLE
Set aServiceManager = CreateObject("com.sun.star.ServiceManager")

'-> Lib�rer le pointeur
Set aServiceManager = Nothing

'-> Renvoyer une valeur de succ�s
IsOpenoffice = True

Exit Function

OfficeError:
    '-> Si erreur : pas openoffice
    IsOpenoffice = False

End Function

Public Function LoadJoinFile(JoinFich As String, fileName As String)
'--> cette procedure charge les fichiers joints au menu fenetre
Dim i As Integer
Dim Rep As String
Dim aSpool As Spool

On Error GoTo GestError

If Not TypeOf Sortie Is PictureBox Then Exit Function

JoinFich = Fichiers(fileName).filesJoins

'-> on r�initialise
For i = 1 To MDIMain.mnuJoin.Count
    MDIMain.mnuJoin.Item(i - 1).Visible = False
    If Not Fichiers(fileName).Spools(1).frmdisplay Is Nothing Then Fichiers(fileName).Spools(1).frmdisplay.Toolbar1.Buttons("FILEJOIN").ButtonMenus(i).Visible = False
Next
'-> on cache le separateur
MDIMain.mnusep4.Visible = False
For Each aSpool In Fichiers(fileName).Spools
    If Not aSpool.frmdisplay Is Nothing Then aSpool.frmdisplay.Toolbar1.Buttons("FILEJOIN").Visible = False
    If Not aSpool.frmdisplay Is Nothing Then aSpool.frmdisplay.Toolbar1.Buttons("FILEJOIN").Enabled = False
Next

'-> si rien quitter
If JoinFich = "" Then Exit Function

'-> si qque chose rendre le boutton des pieces jointes actif
For Each aSpool In Fichiers(fileName).Spools
    If Not aSpool.frmdisplay Is Nothing Then aSpool.frmdisplay.Toolbar1.Buttons("FILEJOIN").Visible = True
    If Not aSpool.frmdisplay Is Nothing Then aSpool.frmdisplay.Toolbar1.Buttons("FILEJOIN").Enabled = True
Next
'-> on parcourt les fichiers joints
For i = 1 To NumEntries(JoinFich, ",")
    '-> on se limite � 25
    If i > 25 Then
        MDIMain.mnuJoin.Item(i - 1).Visible = True
        MDIMain.mnuJoin.Item(i - 1).Caption = "...  "
        For Each aSpool In Fichiers(fileName).Spools
            If Not aSpool.frmdisplay Is Nothing Then
                aSpool.frmdisplay.Toolbar1.Buttons("FILEJOIN").ButtonMenus(i).Visible = True
                aSpool.frmdisplay.Toolbar1.Buttons("FILEJOIN").ButtonMenus(i).Text = "..."
            End If
        Next
        '-> quitter la boucle
        Exit For
    End If
    
    '-> on pointe sur le fichier en verifiant qu'il existe bien
    Rep = Dir(Entry(i, JoinFich, ","))
    If Rep <> "" Then
        MDIMain.mnusep4.Visible = True
        MDIMain.mnuJoin.Item(i - 1).Visible = True
        MDIMain.mnuJoin.Item(i - 1).Caption = i & "  " & Rep
        MDIMain.mnuJoin.Item(i - 1).Tag = Entry(i, JoinFich, ",")
    End If
    '-> on s'occupe ici du menu
    For Each aSpool In Fichiers(fileName).Spools
        If Not aSpool.frmdisplay Is Nothing Then
            aSpool.frmdisplay.Toolbar1.Buttons("FILEJOIN").ButtonMenus(i).Visible = True
            aSpool.frmdisplay.Toolbar1.Buttons("FILEJOIN").ButtonMenus(i).Text = i & "  " & Rep
            aSpool.frmdisplay.Toolbar1.Buttons("FILEJOIN").ButtonMenus(i).Tag = Entry(i, JoinFich, ",")
        End If
    Next
Next

GestError:

End Function

Private Sub RunBatch()
'-> cette procedure lance differents turbo a partir de lignes de commandes contenus dans un fichier
Dim BatchFile As String
Dim hdlBatch As Integer
Dim Ligne As String
Dim CurrentPIDProcess As Long
Dim fileName As String

'-> on recupere le fichier batch
BatchFile = Entry(1, Entry(2, Command$, "="), "$")

'-> Ouvrir le fichier Batch
hdlBatch = FreeFile
Open BatchFile For Input As #hdlBatch

'-> Transf�rer la maquette dans l'objet Spool
Do While Not EOF(hdlBatch)
    '-> Lecture de la ligne
    Line Input #hdlBatch, Ligne
    If Trim(Ligne) = "" Then GoTo NextLigne
    '-> On lance la ligne de commande
    CurrentPIDProcess = Shell(App.Path & "\TurboGraph.exe " & Ligne, vbNormalFocus)
    'CurrentPIDProcess = Shell("d:\compilation\TurboGraph.exe " & ligne, vbNormalFocus)
    '-> Boucler tant que le processus est actif
    Do While IsPidRunning(CurrentPIDProcess)
        '-> Lib�ration de la pile des messages
        DoEvents
        Sleep 1000
    Loop
NextLigne:
Loop

'-> Fermer le fichier Batch
Close #hdlBatch

'-> on regarde si on doit supprimer les fichiers tempo
If Entry(2, Entry(2, UCase(Command$), "$SUPPR="), "=") <> "1" Then Exit Sub
'-> on attend que tout se termine
Sleep 20000

'-> Ouvrir le fichier Batch
hdlBatch = FreeFile
Open BatchFile For Input As #hdlBatch

'-> Transf�rer la maquette dans l'objet Spool
Do While Not EOF(hdlBatch)
    '-> Lecture de la ligne
    Line Input #hdlBatch, Ligne
    If Trim(Ligne) = "" Then GoTo NextLigne
    '-> On supprime le pdf
    fileName = Entry(2, Entry(1, Entry(2, UCase(Ligne), "FILE="), "$"), "=")
    If Dir(fileName & ".pdf") <> "" Then Kill fileName & ".pdf"
    '-> On supprime le BodyFile
    fileName = Entry(2, Entry(1, Entry(2, UCase(Ligne), "BODYFILE="), "$"), "=")
    If Dir(fileName) <> "" Then Kill fileName
    '-> On supprime le File
    fileName = Entry(2, Entry(1, Entry(2, UCase(Ligne), "FILE="), "$"), "=")
    If Dir(fileName) <> "" Then Kill fileName
Loop

'-> Fermer le fichier Batch
Close #hdlBatch

'-> on supprime le mouchard
If Dir(BatchFile) <> "" Then Kill BatchFile

End Sub

Public Function TelechargeFile(strUrlName As String, strPathNameDestination As String, Optional noTestUrl As Boolean) As Boolean
Dim errcode As Long
Dim localFileName As String
  
'-> On sp�cifie le chemin de l'image web
'url = "http://www.siteweb.com/image.jpg"
On Error Resume Next

'On appelle la fonction api de t�l�chargement
If Not noTestUrl Then
    Dim myMSXML As Object
    Err.Clear
    'Trace ("T-2:" & Err.Description)
    Set myMSXML = CreateObject("MSXML2.XMLHTTP.6.0")
    'Trace ("T-1:" & Err.Description)
    If Err.Description <> "" Then GoTo suite
    myMSXML.Open "HEAD", strUrlName, True
    'Trace ("T0:" & Err.Description)
    myMSXML.Send
    'Trace ("T1:" & Err.Description)
    DoEvents
    'Trace ("TelechargeFile1")
    If myMSXML.Status <> 200 Then
        'Trace ("T2:" & Err.Description)
        If Err.Description <> "" Then GoTo suite
        TelechargeFile = False
        'Trace ("T3:" & Err.Description)
        'Trace ("TelechargeFile3 Status:" & myMSXML.Status)
        'Trace ("T4:" & Err.Description)
        Exit Function
    End If
End If
'Trace ("T5:" & Err.Description)
suite:
Trace ("T6:" & Err.Description)
DoEvents
Err.Clear
errcode = URLDownloadToFile(0, strUrlName & "?&_cachekiller=" & Round(GetTickCount()), strPathNameDestination, 0, 0)
Trace ("T7:" & Err.Description & strUrlName & "#" & strPathNameDestination)
If errcode = 0 Then
    '-> l'image a �t� rapatri�e
    'Trace ("T8:" & Err.Description)
    TelechargeFile = True
Else
    'Trace ("T9:" & Err.Description)
    'Trace ("TelechargeFile4")
    '-> l'image n'a pas pu etre telechargee
    TelechargeFile = False
End If

End Function

Public Sub Trace(Optional sText As String, Optional Level = 5)
'--> cette procedure permet d'alimenter le fichier de debug
'-> si on est en mode trace
'On Error Resume Next
'Exit Sub
'IsMouchard = True
If IsMouchard Then
    'si le fichier n'est pas ouvert l'ouvrir
    If hdlMouchard = 0 Then
        hdlMouchard = FreeFile
        If Mouchard = "" Then
            Mouchard = App.Path & "\log.txt"
            Mouchard = GetTempFileNameVB("TXT")
        End If
        If FileExist(Mouchard) Then
            If FileLen(Mouchard) > 100000 Then Kill Mouchard
        End If
        Open Mouchard For Append As #hdlMouchard
        '-> on initialise le mode trace
        Print #hdlMouchard, ""
        Print #hdlMouchard, "***************************"
        Print #hdlMouchard, "Trace du " & FORMAT(Now, "dd/mm/yyyy � hh:mm:ss")
        Print #hdlMouchard, "Chemin du turbograph : " & App.Path
    End If
    '-> ecrire la ligne
    If sText = "" Then
        If Err.Description <> "" Then Print #hdlMouchard, FORMAT(Now, "hh:mm:ss") & "Err : " & Err.Description
        Err.Clear
    Else
        Print #hdlMouchard, FORMAT(Now, "hh:mm:ss    ") & sText
    End If
End If

If MouchardLevel < Level Then Exit Sub

If MouchardTempsReel Then
    frmConfig.RichTextBox1.Text = frmConfig.RichTextBox1.Text & Chr(13) & sText
End If

End Sub

Public Function SearchForFiles(sRoot As String, sFile As String, Optional sFiltreDirectory As String) As String
    '--> cette fonction cherche des fichiers � partir d'une directorie
   Dim WFD As WIN32_FIND_DATA
   Dim hFile As Long
  
   With fp
      .sFileRoot = QualifyPath(sRoot)      'chemin de d�part
      .sFileNameExt = sFile                'fichier (* ? autoris�
      .bRecurse = 1                             'True = recherche recursive
      .bFindOrExclude = 1                       '0=inclure, 1=exclure
   End With
   
   
   hFile = FindFirstFile(sRoot & "*.*", WFD)
   If hFile <> -1 Then
      Do
        If stopSearch = True Then Exit Function
        DoEvents
        'si c'est un repertoire on boucle
         If (WFD.dwFileAttributes And vbDirectory) Then
            If Asc(WFD.cFileName) <> CLng(46) Then
                If fp.bRecurse Then
                    SearchForFiles = SearchForFiles(sRoot & TrimNull(WFD.cFileName) & vbBackslash, sFile)
                End If
            End If
         Else
           'doit etre un fichier..
            If MatchSpec(WFD.cFileName, fp.sFileNameExt) Then
                If sFiltreDirectory <> "" Then
                    If InStr(1, sRoot & TrimNull(WFD.cFileName), sFiltreDirectory) <> 0 Then
                        SearchForFiles = sRoot & TrimNull(WFD.cFileName)
                        stopSearch = True
                        Exit Do
                    End If
                Else
                    SearchForFiles = sRoot & TrimNull(WFD.cFileName)
                    stopSearch = True
                    Exit Do
                End If
            End If
         End If
      Loop While FindNextFile(hFile, WFD)
   End If
   Call FindClose(hFile)
End Function

Public Function QualifyPath(sPath As String) As String
   If Right$(sPath, 1) <> vbBackslash Then
      QualifyPath = sPath & vbBackslash
   Else
      QualifyPath = sPath
   End If
End Function

Public Function TrimNull(startstr As String) As String
   TrimNull = Left$(startstr, lstrlen(StrPtr(startstr)))
End Function

Public Function MatchSpec(sFile As String, sSpec As String) As Boolean
   MatchSpec = PathMatchSpec(StrPtr(sFile), StrPtr(sSpec)) = fp.bFindOrExclude
End Function

Public Function GetFrameworkVersion(ByRef version As String) As String
    Dim vSize   As Long
    Dim vHandle As Long
    vHandle = LoadLibrary("mscoree.dll")
    If vHandle Then
        vSize = 256
        version = Space(vSize)
        If (GetCORVersion(StrPtr(version), vSize, vSize) = 0) Then
            version = Left$(version, vSize - 1)
            GetFrameworkVersion = version
        End If
        FreeLibrary vHandle
    End If
    If GetFrameworkVersion = "" Then GetFrameworkVersion = "unknown"
End Function

Public Function GetRepertoireWindows() As String
    Dim WindowsDir As String

    WindowsDir = String$(255, " ")
    '-> r�cup�ration du r�pertoire d'installation de Windows par API
    GetWindowsDirectory WindowsDir, Len(WindowsDir)
    WindowsDir = RTrim$(WindowsDir)
    WindowsDir = Left$(WindowsDir, Len(WindowsDir) - 1)
    GetRepertoireWindows = WindowsDir
End Function

Public Function noteAdd(strNote As String) As Form
    '--> cette fonction permet d'ajouter une note dans la collection et la retourne
    '--> strNote du type nomdufichier|numero du spool|asppol.currentpage|top|left|width|height|couleur|transparence|texteentete|texteligne
    Dim sNote() As String
    Dim lpRect As RECT
    
    '-> on recupere la position de la page sur la feuille
    'GetWindowRect Sortie.hWnd, lpRect
    
    '-> gestion des erreurs
    On Error Resume Next
    
    sNote = Split(strNote, "|")
    '-> on initialise si besoin la collection
    If fNotes Is Nothing Then
        Set fNotes = New Collection
    End If
    '-> on initialise une nouvelle feuille
    Set fNote = New frmNote
    fNotes.Add fNote
    fNote.Tag = sNote(0) & "|" & sNote(1) & "|" & sNote(2)
    
    If sNote(3) <> "" Then fNote.Top = val(sNote(3)) '+ fNote.ScaleY(lpRect.Top, 3, 1)
    If sNote(4) <> "" Then fNote.Left = val(sNote(4)) '+ fNote.ScaleX(lpRect.Left, 3, 1)
    If sNote(5) <> "" Then fNote.width = val(sNote(5))
    If sNote(6) <> "" Then fNote.height = val(sNote(6))
    If sNote(7) <> "" Then
        fNote.BackColor = sNote(7)
        fNote.Text2.BackColor = sNote(7)
    End If
    If sNote(8) <> "" Then
        fNote.valTransparence = sNote(8)
        fNote.Slider1.Value = sNote(8) / 2.55
    End If
    If sNote(9) <> "" Then fNote.Text1.Caption = sNote(9)
    If sNote(10) <> "" Then fNote.Text2.Text = sNote(10)
    
    '-> on retourne la feuille
    Set noteAdd = fNote
End Function

Public Sub noteDelete(fHwnd As Long)
'--> cette procedure permet de supprimer une note
Dim i As Integer
Dim strFichier As String

'-> on parcours les notes pour trouver celle a supprimer
For i = 1 To fNotes.Count
    If fNotes(i).Hwnd = fHwnd Then
        strFichier = Entry(1, fNotes(i).Tag, "|")
        Unload fNotes(i)
        fNotes.Remove i
        noteSave strFichier
        Exit For
    End If
Next

End Sub

Public Sub noteSave(strFichier As String)
'--> cette fonction permet de sauvegarder les notes d'un fichier
Dim DSO, DSOprop
Dim strLigne As String
Dim L1, L2, L3, L4, L5, L6 As Long
Dim Ft1 As FILETIME, Ft2 As FILETIME, SysTime As SYSTEMTIME, ft3 As FILETIME
Dim WFD As WIN32_FIND_DATA
Dim hFile As Long
Dim ficCible As Long
Dim RetVal As Long
Dim lpRect As RECT

On Error Resume Next

'-> on recupere la position de la page sur la feuille
'GetWindowRect Sortie.hWnd, lpRect

'-> on recupere les infos sur le fichier
hFile = FindFirstFile(strFichier, WFD)
L1 = WFD.ftCreationTime.dwHighDateTime
L2 = WFD.ftCreationTime.dwLowDateTime
L3 = WFD.ftLastAccessTime.dwHighDateTime
L4 = WFD.ftLastAccessTime.dwLowDateTime
L5 = WFD.ftLastWriteTime.dwHighDateTime
L6 = WFD.ftLastWriteTime.dwLowDateTime

'-> on se construit la chaine qui correspond aux notes
For Each fNote In fNotes
    '-> on regarde si la note appartient bien a notre fichier
    If Entry(1, fNote.Tag, "|") = strFichier Or Entry(1, fNote.Tag, "|") = Fichiers(strFichier).FileNameZip Then
        If strLigne <> "" Then strLigne = strLigne & "�"
        strLigne = strLigne & "|" & Entry(2, fNote.Tag, "|")
        strLigne = strLigne & "|" & Entry(3, fNote.Tag, "|")
        strLigne = strLigne & "|" & fNote.Top '- fNote.ScaleY(lpRect.Top, 3, 1))
        strLigne = strLigne & "|" & fNote.Left '- fNote.ScaleX(lpRect.Left, 3, 1))
        strLigne = strLigne & "|" & fNote.width
        strLigne = strLigne & "|" & fNote.height
        strLigne = strLigne & "|" & fNote.BackColor
        strLigne = strLigne & "|" & fNote.valTransparence
        strLigne = strLigne & "|" & fNote.Text1.Caption
        strLigne = strLigne & "|" & fNote.Text2.Text
    End If
Next

'-> on sauvegarde la chaine dans un des attributs du fichier
Set DSO = CreateObject("DSOFile.OleDocumentProperties")
DSO.Open strFichier
'-> on pointe sur les propri�t�s
Set DSOprop = DSO.SummaryProperties
If DSOprop.Comments <> strLigne Then
    DSOprop.Comments = strLigne
    DSO.Save
End If
DSO.Close
Set DSOprop = Nothing

'-> on redonne la date d'origine au fichier
WFD.ftCreationTime.dwHighDateTime = L1
WFD.ftCreationTime.dwLowDateTime = L2
WFD.ftLastAccessTime.dwHighDateTime = L3
WFD.ftLastAccessTime.dwLowDateTime = L4
WFD.ftLastWriteTime.dwHighDateTime = L5
WFD.ftLastWriteTime.dwLowDateTime = L6
ficCible = CreateFile(strFichier, &H40000000, &H1 Or &H2, ByVal 0&, 3, 0, 0)
RetVal = SetFileTime(ficCible, WFD.ftCreationTime, WFD.ftLastAccessTime, WFD.ftLastWriteTime)
CloseHandle ficCible

End Sub

Public Sub noteUnload(strFichier As String)
'--> cette fonction permet de sortir les notes d'un fichier a sa fermeture de la collection
Dim i As Integer

On Error Resume Next

'-> on se construit la chaine qui correspond aux notes
For i = fNotes.Count To i = 1 Step -1
    '-> on regarde si la note appartient bien a notre fichier
    If Entry(1, fNotes(i).Tag, "|") = strFichier Then
        '-> on supprime la note de la collectgion
        fNotes(i).Visible = False
        fNotes.Remove (i)
        If i = 0 Then Exit For
    End If
Next

End Sub

Private Sub notesLoad(strFichier As String)
'--> cette fonction va permettre de charger la collection notes d'un fichier
Dim aFichier As Fichier
Dim aSpool As Spool
Dim DSO, DSOprop
Dim i As Integer
Dim L1, L2, L3, L4, L5, L6 As Long
Dim Ft1 As FILETIME, Ft2 As FILETIME, SysTime As SYSTEMTIME, ft3 As FILETIME
Dim WFD As WIN32_FIND_DATA
Dim hFile As Long
Dim ficCible As Long
Dim RetVal As Long

On Error GoTo GestError

'-> on recupere les infos sur le fichier
hFile = FindFirstFile(strFichier, WFD)
L1 = WFD.ftCreationTime.dwHighDateTime
L2 = WFD.ftCreationTime.dwLowDateTime
L3 = WFD.ftLastAccessTime.dwHighDateTime
L4 = WFD.ftLastAccessTime.dwLowDateTime
L5 = WFD.ftLastWriteTime.dwHighDateTime
L6 = WFD.ftLastWriteTime.dwLowDateTime

Set DSO = CreateObject("DSOFile.OleDocumentProperties")
DSO.Open strFichier

'-> on pointe sur les propri�t�s
Set DSOprop = DSO.SummaryProperties
If DSOprop.Comments <> "" Then
    For i = 1 To NumEntries(DSOprop.Comments, "�")
        '-> on charge la note
        noteAdd strFichier & Entry(i, DSOprop.Comments, "�")
    Next
End If

GestError:

Set DSOprop = Nothing
Set DSO = Nothing

'-> on redonne la date d'origine au fichier
WFD.ftCreationTime.dwHighDateTime = L1
WFD.ftCreationTime.dwLowDateTime = L2
WFD.ftLastAccessTime.dwHighDateTime = L3
WFD.ftLastAccessTime.dwLowDateTime = L4
WFD.ftLastWriteTime.dwHighDateTime = L5
WFD.ftLastWriteTime.dwLowDateTime = L6
ficCible = CreateFile(strFichier, &H40000000, &H1 Or &H2, ByVal 0&, 3, 0, 0)
RetVal = SetFileTime(ficCible, WFD.ftCreationTime, WFD.ftLastAccessTime, WFD.ftLastWriteTime)
CloseHandle ficCible

End Sub

Public Function notesDisplay(aSpool As Spool)
'--> cette fonction permet d'afficher les notes de la page en cours

On Error Resume Next

For Each fNote In fNotes
    If fNote.Tag = Fichiers(aSpool.fileName).FileNameZip & "|" & aSpool.Num_Spool & "|" & aSpool.CurrentPage Then
        fNote.Init aSpool.frmdisplay
        fNote.Visible = True
    Else
        fNote.Visible = False
    End If
Next

End Function

Public Sub ComboSaisieAuto(aCombo As ImageCombo, SaisieLibre As Boolean)
'--> cette proc�dure permet la saisie automatique dans une combo
Dim i As Long
Dim Sel As Long
Dim aItem As ComboItem

'-> on parcours les elements de la combo
For Each aItem In aCombo.comboItems
    Sel = Len(aCombo.Text)
    '-> si on trouve le texte on l'affiche
    If (StrComp(Left$(aItem.Text, Sel), aCombo.Text, vbTextCompare) = 0) And aCombo.Text <> "" Then
        aItem.Selected = True
        aCombo.SelStart = Sel
        If Len(aCombo.Text) <> Sel Then
            aCombo.SelLength = Len(aCombo.Text) - Sel
        Else
            aCombo.SelStart = 0
            aCombo.SelLength = Len(aCombo.Text)
        End If
        GoTo suite
    End If
Next
'-> on a rien trouv�
If Not SaisieLibre Then
    aCombo.Text = ""
End If
suite:

End Sub

Public Function GarbageSpool()
'-> cette fonction regarde si on doit nettoyer le repertoiredes spools
Dim strKeep As String
Dim aLb As Libelle

On Error Resume Next
'-> Pointer sur la classe libell�
Set aLb = Libelles("MDIMAIN")

'-> lecture du parametrage
strKeep = GetIniString("PARAM", "KEEP", App.Path & "\Turbograph.ini", False)

If strKeep = "" Then
    '-> on pose la question
    strKeep = InputBox(aLb.GetCaption(33), aLb.GetCaption(34))
    If Trim(strKeep) = "" Then strKeep = "ALLWAYS"
    SetIniString "PARAM", "KEEP", App.Path & "\Turbograph.ini", strKeep
End If

If strKeep = "ALLWAYS" Then Exit Function

'-> on lance la procedure de recherche et de suppression des spools
 Dim WFD As WIN32_FIND_DATA
 Dim hFile As Long
 Dim sRoot As String
 Dim sFiltreDirectory As String
 sRoot = TurbosavePath
 sFiltreDirectory = ""
 With fp
    .sFileRoot = QualifyPath(sRoot)      'chemin de d�part
    .sFileNameExt = ""                'fichier (* ? autoris�
    .bRecurse = 0                             'True = recherche recursive
    .bFindOrExclude = 1                       '0=inclure, 1=exclure
 End With
  
 hFile = FindFirstFile(sRoot & "*.*", WFD)
 If hFile <> -1 Then
    Do
      'DoEvents
      'si c'est un repertoire on fait rien
       If (WFD.dwFileAttributes And vbDirectory) Then
       Else
         'doit etre un fichier..
          If MatchSpec(WFD.cFileName, fp.sFileNameExt) Then
              If sFiltreDirectory <> "" Then
                  If InStr(1, sRoot & TrimNull(WFD.cFileName), sFiltreDirectory) <> 0 Then
                      '-> seolon la date on supprime le fichier
                      DeleteFileSave sRoot & TrimNull(WFD.cFileName), strKeep
                      'Exit Do
                  End If
              Else
                  DeleteFileSave sRoot & TrimNull(WFD.cFileName), strKeep
              End If
          End If
       End If
    Loop While FindNextFile(hFile, WFD)
 End If
Call FindClose(hFile)

LockWindowUpdate 0
DoEvents

End Function

Private Sub DeleteFileSave(strFichier As String, strKeep As String)
'--> cette proc�dure permet de supprimer si besion un fichier
Dim aItem As ListItem
Dim i As Integer

If val(strKeep) = 0 Then Exit Sub
'On Error Resume Next
If DateDiff("d", FORMAT(FileDateTime(strFichier), "dd/mm/yyyy"), FORMAT(Now, "dd/mm/yyyy")) > val(strKeep) Then
    Kill strFichier
End If

End Sub


Public Sub convertFile(ByVal fileName As String)
    Const Dos2Unix = 1
    Dim fs As Object, txt As String
    Set fs = CreateObject("Scripting.FileSystemObject")

    txt = fs.OpenTextFile(fileName, 1).ReadAll  'ForReading = 1
    txt = IIf(Dos2Unix = 1, Replace(txt, vbCrLf, vbLf), Replace(txt, vbLf, vbCrLf))
    fs.OpenTextFile(fileName, 2).Write txt      'ForWriting = 2
End Sub

Public Function UnixDos(ByVal OriginalFile As String, ByVal NewFile As String, eConvertType As ConvertType)
    
    Dim OpenFileNum, SaveFileNum As Integer
    Dim NewFileBuffer As String
    
    On Error GoTo GestError
    
    NewFile = NewFile & ".tmp"
    
    NewFileBuffer = OpenFileAPI(OriginalFile)
    NewFileBuffer = Replace(NewFileBuffer, Chr(10), Chr(13) & Chr(10))
    SaveFileNum = FreeFile
    Open NewFile For Binary As #SaveFileNum
    Put #SaveFileNum, , NewFileBuffer
    Close #SaveFileNum
    '-> on ecrase le fichier d'origine
    FileCopy NewFile, OriginalFile
    '-> on butte la sauvegarde
    Kill NewFile
   Exit Function
GestError:
   MsgBox "Error"
 End Function


Public Function unix2dos(strFilename As String)

On Error GoTo Error_Handler

Dim strParameter As String
Dim In_File_Handle
Dim Out_File_Handle
Dim In_FileName As String
Dim Out_FileName As String
Dim FilenameEndCompare As String
Dim a As Long
Dim CR As String
Dim LF As String
Dim strSpc As String
        
In_FileName = strFilename
Out_FileName = strFilename & ".temp" 'temp output file
'open files
In_File_Handle = FreeFile
Open In_FileName For Binary As #In_File_Handle

Out_File_Handle = FreeFile
Open Out_FileName For Binary As #Out_File_Handle
    
CR = Chr$(13) '&h0D
LF = Chr$(10) '&h0A
'Unix textline '0A'
'Dos/windows textline '0D0A'
strSpc = " "
Dim i As Long
While Not EOF(In_File_Handle)
    Get #In_File_Handle, , strSpc$
    i = i + 1
    DoEvents
    If strSpc$ = LF$ Then Put #Out_File_Handle, , CR$
    If Asc(strSpc$) <> 0 Then Put #Out_File_Handle, , strSpc$
Wend
            
Close #In_File_Handle
Close #Out_File_Handle

Kill In_FileName 'Poistetaan alkuper�inen tiedosto
Name Out_FileName As In_FileName 'Nimet��n temp filename alkuper�iseksi filename.temp -> filename
        
Exit Function

Error_Handler:

Dim Msg As String
Msg = "Error:  " & str(Err.Number) & vbCrLf & " was generated by " & Err.Source & vbCrLf & Err.Description & vbCrLf & vbCrLf _
& "Input file: " & In_FileName & vbCrLf & vbCrLf & "Output file: " & Out_FileName
MsgBox Msg, , "Error", Err.HelpFile, Err.HelpContext

End Function



Public Sub PreprintPageSpool(ByRef aSpool As Spool, ByVal pageToPrint As Integer)

'---> Cette proc�dure imprime une page d'un spool

Dim i As Integer, j As Integer
Dim PositionX As Long
Dim PositionY As Long
Dim IsPaysage As Boolean
Dim Ligne As String
Dim TypeObjet As String
Dim NomObjet As String
Dim TypeSousObjet As String
Dim NomSousObjet As String
Dim Param As String
Dim DataFields As String
Dim FirstObj As Boolean
Dim aNode As Node
Dim ErrorCode As Integer
Dim aSection As Section
Dim X As Control
Dim MargeOld As Integer
'--> on charge les libell�s
Dim aLb As Libelle

'-> Pointer sur la classe libell�
Set aLb = Libelles("FRMGESTSPOOL")

On Error GoTo GestError

'-> Conserver la valeur du zoom
Zoom = 1
curPage = pageToPrint

'-> Modifier la page courrante de l'objet Spool
aSpool.CurrentPage = pageToPrint
frmGestSpool.InitFRMDISPLAY aSpool
aSpool.NbError = 0
aSpool.InitPreviewPage
'-> Positionner le pointeur de sortie vers le picturebox de la feuille
Set Sortie = aSpool.frmPreview.Page

'-> R�cup�ration des marges internes du contexte de p�riph�rique
MargeX = GetDeviceCaps(Sortie.hdc, PHYSICALOFFSETX)
MargeY = GetDeviceCaps(Sortie.hdc, PHYSICALOFFSETY)

'-> Initialiser la position X , y du pointeur sur les marges du document
PositionY = -MargeY + Sortie.ScaleY(aSpool.maquette.MargeTop, 7, 3)
PositionX = -MargeX + Sortie.ScaleX(aSpool.maquette.MargeLeft, 7, 3)

PositionX = PositionX * Zoom
PositionY = PositionY * Zoom

'-> Indiquer l'�tat du premier objet que l'on trouve
FirstObj = True

'-> Lecture des lignes de la page
For i = 1 To NumEntries(aSpool.GetPage(pageToPrint), Chr(0))
    '-> R�cup�ration de la ligne en cours
    Ligne = Trim(Entry(i, aSpool.GetPage(pageToPrint), Chr(0)))
    '-> Ne rien imprimer si page � blanc
    If Trim(Ligne) = "" Then GoTo NextLigne
    
    If pageToPrint = 0 Then
        frmLib.Rtf(aSection.IdRTf).Text = frmLib.Rtf(aSection.IdRTf).Text & Chr(13) & Chr(10) & Ligne
        frmLib.Rtf(aSection.IdRTf).SelStart = 0
        frmLib.Rtf(aSection.IdRTf).SelLength = Len(frmLib.Rtf(aSection.IdRTf).Text)
        frmLib.Rtf(aSection.IdRTf).SelFontName = "Lucida Console"
    Else
        If InStr(1, Ligne, "[") = 1 Then
            '-> r�cup�ration des param�tres
            AnalyseObj Ligne, TypeObjet, NomObjet, TypeSousObjet, NomSousObjet, Param, DataFields
            '-> Selon le type d'objet
            If UCase$(TypeObjet) = "ST" Then
                If Not PrintSection(NomObjet, Param, DataFields, FirstObj, PositionX, PositionY, aSpool) Then Exit For
                FirstObj = False
            ElseIf UCase$(TypeObjet) = "TB" Then
                If Not PrintTableau(NomObjet, Param, DataFields, PositionX, PositionY, NomSousObjet, FirstObj, aSpool) Then Exit For
                FirstObj = False
            End If
        End If 'Si premier caract�re = "["
    End If 'Si on imprime la page de s�lection
NextLigne:
Next 'Pour toutes les lignes de la page

'-> Si besoin on redimensionne la feuille en fonction de ce que l'on a imprim�
If aSpool.maquette.Hauteur = 99 Then
    aSpool.maquette.HauteurVar = Sortie.ScaleY(PositionY, 3, 7) + 2
End If

Exit Sub
       
GestError:
    MsgBox Err.Number & " " & Err.Description
    
End Sub

Public Function BuildFileUploadRequest(ByRef strData As String, _
                                        ByRef DestUrl As URL, _
                                        ByVal UploadName As String, _
                                        ByVal fileName As String, _
                                        ByVal MimeType As String) As String
    
    Dim strHttp As String ' holds the entire HTTP request
    Dim strBoundary As String 'the boundary between each entity
    Dim strBody As String ' holds the body of the HTTP request
    Dim lngLength As Long ' the length of the HTTP request
        
    ' create a boundary consisting of a random string
    strBoundary = RandomAlphaNumString(32)
    
    ' create the body of the http request in the form
    '
    ' --boundary
    ' Content-Disposition: form-data; name="UploadName"; filename="FileName"
    ' Content-Type: MimeType
    '
    ' file data here
    '--boundary--
    strBody = "--" & strBoundary & vbCrLf
    strBody = strBody & "Content-Disposition: form-data; name=""" & UploadName & """; filename=""" & _
                    fileName & """" & vbCrLf
    strBody = strBody & "Content-Type: " & MimeType & vbCrLf
    strBody = strBody & vbCrLf & strData
    strBody = strBody & vbCrLf & "--" & strBoundary & "--"
    
    ' find the length of the request body - this is required for the
    ' Content-Length header
    lngLength = Len(strBody)
    
    ' construct the HTTP request in the form:
    '
    ' POST /path/to/reosurce HTTP/1.0
    ' Host: host
    ' Content-Type: multipart-form-data, boundary=boundary
    ' Content-Length: len(strbody)
    '
    ' HTTP request body
    strHttp = "POST " & DestUrl.URI & "?" & DestUrl.Query & " HTTP/1.0" & vbCrLf
    strHttp = strHttp & "Host: " & DestUrl.Host & vbCrLf
    strHttp = strHttp & "Content-Type: multipart/form-data, boundary=" & strBoundary & vbCrLf
    strHttp = strHttp & "Content-Length: " & lngLength & vbCrLf & vbCrLf
    strHttp = strHttp & strBody

    BuildFileUploadRequest = strHttp
End Function


' returns as type URL from a string
Function ExtractUrl(ByVal strUrl As String) As URL
    Dim intPos1 As Integer
    Dim intPos2 As Integer
    
    Dim retURL As URL
    
    '1 look for a scheme it ends with ://
    intPos1 = InStr(strUrl, "://")
    
    If intPos1 > 0 Then
        retURL.Scheme = Mid(strUrl, 1, intPos1 - 1)
        strUrl = Mid(strUrl, intPos1 + 3)
    End If
        
    '2 look for a port
    intPos1 = InStr(strUrl, ":")
    intPos2 = InStr(strUrl, "/")
    
    If intPos1 > 0 And intPos1 < intPos2 Then
        ' a port is specified
        retURL.Host = Mid(strUrl, 1, intPos1 - 1)
        
        If (IsNumeric(Mid(strUrl, intPos1 + 1, intPos2 - intPos1 - 1))) Then
                retURL.Port = CInt(Mid(strUrl, intPos1 + 1, intPos2 - intPos1 - 1))
        End If
    ElseIf intPos2 > 0 Then
        retURL.Host = Mid(strUrl, 1, intPos2 - 1)
    Else
        retURL.Host = strUrl
        retURL.URI = "/"
        
        ExtractUrl = retURL
        Exit Function
    End If
    
    strUrl = Mid(strUrl, intPos2)
    
    ' find a question mark ?
    intPos1 = InStr(strUrl, "?")
    
    If intPos1 > 0 Then
        retURL.URI = Mid(strUrl, 1, intPos1 - 1)
        retURL.Query = Mid(strUrl, intPos1 + 1)
    Else
        retURL.URI = strUrl
    End If
    
    ExtractUrl = retURL
End Function

Private Function RandomAlphaNumString(ByVal intLen As Integer)
    Dim StrReturn As String
    
    Dim X As Integer
    Dim c As Byte
    
    Randomize
    
    For X = 1 To intLen
        c = Int(Rnd() * 127)
    
        If (c >= Asc("0") And c <= Asc("9")) Or _
           (c >= Asc("A") And c <= Asc("Z")) Or _
           (c >= Asc("a") And c <= Asc("z")) Then
           
            StrReturn = StrReturn & Chr(c)
        Else
            X = X - 1
        End If
    Next X
    
    RandomAlphaNumString = StrReturn
End Function

Public Sub getPrintersList()
  Dim PrinterEnum() As Long, Impr() As String
  Dim Needed As Long, Returned As Long, i As Integer
  Dim TempPrinter As Printer
  On Error Resume Next
    Trace "getPrintersList"
    If GetIniString("PARAM", "PRINTEROBJET", App.Path & "\Turbograph.ini", False) = "1" Then
        EnumPrintersA 2, vbNullString, 5, 0, 0, Needed, 0
        ReDim PrinterEnum(Needed / 4)
        EnumPrintersA 2, vbNullString, 5, PrinterEnum(0), _
          Needed, Needed, Returned
        ReDim Impr(1 To Returned)
        For i = 1 To Returned
          Impr(i) = Space$(lstrlenA(PrinterEnum(i * 5 - 5)))
          lstrcpyA Impr(i), PrinterEnum(i * 5 - 5)
        Next i
    Else
        i = 0
        Trace "For each printers"
        For Each TempPrinter In Printers
            i = i + 1
        Next
        ReDim Impr(i)
        i = 1
        For Each TempPrinter In Printers
            Trace TempPrinter.DeviceName
            Impr(i) = TempPrinter.DeviceName
            i = i + 1
        Next
    End If
    listprinter = Impr

End Sub

Public Function AToUTF8(ByVal wText As String) As String
    Dim vNeeded As Long
    Dim vSize   As Long
    vSize = Len(wText)
    vNeeded = WideCharToMultiByte(CP_UTF8, 0, StrPtr(wText), vSize, "", 0, 0, 0)
    AToUTF8 = String(vNeeded, 0)
    WideCharToMultiByte CP_UTF8, 0, StrPtr(wText), vSize, AToUTF8, vNeeded, 0, 0
End Function

Public Function UTF8ToA(ByVal wText As String) As String
    Dim vNeeded As Long
    Dim vSize   As Long
    vSize = Len(wText)
    vNeeded = MultiByteToWideChar(CP_UTF8, 0, wText, vSize, 0, 0)
    UTF8ToA = String(vNeeded, 0)
    MultiByteToWideChar CP_UTF8, 0, wText, vSize, StrPtr(UTF8ToA), vNeeded
End Function

Public Function GetWindowsVersion() As Long
Dim V As OSVERSIONINFO

V.dwOSVersionInfoSize = Len(V)
GetVersionEx V

GetWindowsVersion = V.dwMajorVersion
End Function

'   Char. number range  |        UTF-8 octet sequence
'      (hexadecimal)    |              (binary)
'   --------------------+---------------------------------------------
'   0000 0000-0000 007F | 0xxxxxxx
'   0000 0080-0000 07FF | 110xxxxx 10xxxxxx
'   0000 0800-0000 FFFF | 1110xxxx 10xxxxxx 10xxxxxx
'   0001 0000-0010 FFFF | 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
Public Function Encode_UTF8(astr)
    Dim c
    Dim n
    Dim utftext
     
    utftext = ""
    n = 1
    Do While n <= Len(astr)
        c = AscW(Mid(astr, n, 1))
        If c < 128 Then
            utftext = utftext + Chr(c)
        ElseIf ((c >= 128) And (c < 2048)) Then
            utftext = utftext + Chr(((c \ 64) Or 192))
            utftext = utftext + Chr(((c And 63) Or 128))
        ElseIf ((c >= 2048) And (c < 65536)) Then
            utftext = utftext + Chr(((c \ 4096) Or 224))
            utftext = utftext + Chr((((c \ 64) And 63) Or 128))
            utftext = utftext + Chr(((c And 63) Or 128))
        Else ' c >= 65536
            utftext = utftext + Chr(((c \ 262144) Or 240))
            utftext = utftext + Chr(((((c \ 4096) And 63)) Or 128))
            utftext = utftext + Chr((((c \ 64) And 63) Or 128))
            utftext = utftext + Chr(((c And 63) Or 128))
        End If
        n = n + 1
    Loop
    Encode_UTF8 = utftext
End Function
 
'   Char. number range  |        UTF-8 octet sequence
'      (hexadecimal)    |              (binary)
'   --------------------+---------------------------------------------
'   0000 0000-0000 007F | 0xxxxxxx
'   0000 0080-0000 07FF | 110xxxxx 10xxxxxx
'   0000 0800-0000 FFFF | 1110xxxx 10xxxxxx 10xxxxxx
'   0001 0000-0010 FFFF | 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
Public Function Decode_UTF8(astr)
    Dim c0, c1, c2, c3
    Dim n
    Dim unitext
     
    If isUTF8(astr) = False Then
        Decode_UTF8 = astr
        Exit Function
    End If
     
    unitext = ""
    n = 1
    Do While n <= Len(astr)
        c0 = Asc(Mid(astr, n, 1))
        If n <= Len(astr) - 1 Then
            c1 = Asc(Mid(astr, n + 1, 1))
        Else
            c1 = 0
        End If
        If n <= Len(astr) - 2 Then
            c2 = Asc(Mid(astr, n + 2, 1))
        Else
            c2 = 0
        End If
        If n <= Len(astr) - 3 Then
            c3 = Asc(Mid(astr, n + 3, 1))
        Else
            c3 = 0
        End If
         
        If (c0 And 240) = 240 And (c1 And 128) = 128 And (c2 And 128) = 128 And (c3 And 128) = 128 Then
            unitext = unitext + ChrW((c0 - 240) * 65536 + (c1 - 128) * 4096) + (c2 - 128) * 64 + (c3 - 128)
            n = n + 4
        ElseIf (c0 And 224) = 224 And (c1 And 128) = 128 And (c2 And 128) = 128 Then
            unitext = unitext + ChrW((c0 - 224) * 4096 + (c1 - 128) * 64 + (c2 - 128))
            n = n + 3
        ElseIf (c0 And 192) = 192 And (c1 And 128) = 128 Then
            unitext = unitext + ChrW((c0 - 192) * 64 + (c1 - 128))
            n = n + 2
        ElseIf (c0 And 128) = 128 Then
            unitext = unitext + ChrW(c0 And 127)
            n = n + 1
        Else ' c0 < 128
            unitext = unitext + ChrW(c0)
            n = n + 1
        End If
    Loop
 
    Decode_UTF8 = unitext
End Function
 
'   Char. number range  |        UTF-8 octet sequence
'      (hexadecimal)    |              (binary)
'   --------------------+---------------------------------------------
'   0000 0000-0000 007F | 0xxxxxxx
'   0000 0080-0000 07FF | 110xxxxx 10xxxxxx
'   0000 0800-0000 FFFF | 1110xxxx 10xxxxxx 10xxxxxx
'   0001 0000-0010 FFFF | 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
Public Function isUTF8(astr)
    If isCode128 Then
        isUTF8 = False
        Exit Function
    End If
    Dim c0, c1, c2, c3
    Dim n
     
    isUTF8 = True
    n = 1
    Do While n <= Len(astr)
        c0 = Asc(Mid(astr, n, 1))
        If n <= Len(astr) - 1 Then
            c1 = Asc(Mid(astr, n + 1, 1))
        Else
            c1 = 0
        End If
        If n <= Len(astr) - 2 Then
            c2 = Asc(Mid(astr, n + 2, 1))
        Else
            c2 = 0
        End If
        If n <= Len(astr) - 3 Then
            c3 = Asc(Mid(astr, n + 3, 1))
        Else
            c3 = 0
        End If
         
        If (c0 And 240) = 240 Then
            If (c1 And 128) = 128 And (c2 And 128) = 128 And (c3 And 128) = 128 Then
                n = n + 4
            Else
                isUTF8 = False
                Exit Function
            End If
        ElseIf (c0 And 224) = 224 Then
            If (c1 And 128) = 128 And (c2 And 128) = 128 Then
                n = n + 3
            Else
                isUTF8 = False
                Exit Function
            End If
        ElseIf (c0 And 192) = 192 Then
            If (c1 And 128) = 128 Then
                n = n + 2
            Else
                isUTF8 = False
                Exit Function
            End If
        ElseIf (c0 And 128) = 0 Then
            n = n + 1
        Else
            isUTF8 = False
            Exit Function
        End If
    Loop
End Function


Public Function RTBReadUnicode(ByVal RTB As RichTextLib.RichTextBox) As String
    'Reads Text from RichTextBox as Unicode text on a system with Rich Edit 3.0
    '(Windows Me, Windows 2000, or later).
    Dim gtlUnicode As GETTEXTLENGTHEX
    Dim gtUnicode As GETTEXTEX
    Dim lngChars As Long

    With gtlUnicode
        .Flags = GTL_USECRLF Or GTL_PRECISE Or GTL_NUMCHARS
        .CodePage = CP_UNICODE
    End With
    lngChars = SendMessageWLng(RTB.Hwnd, EM_GETTEXTLENGTHEX, VarPtr(gtlUnicode), 0)

    With gtUnicode
        .cb = (lngChars + 1) * 2
        .Flags = GT_USECRLF
        .CodePage = CP_UNICODE
    End With
    RTBReadUnicode = String$(lngChars, 0)
    SendMessageWLng RTB.Hwnd, EM_GETTEXTEX, VarPtr(gtUnicode), StrPtr(RTBReadUnicode)
End Function

'Getting  the Sine Value,  pie =  4 * Atn(1)
Public Function Sine(ByVal angle As Double) As Double
    Sine = sIn(4 * Atn(1) / 180 * angle)
End Function

'Getting  the CoSine Value,  pie =  4 * Atn(1)
Public Function Cosine(ByVal angle As Double) As Double
    Cosine = Cos(4 * Atn(1) / 180 * angle)
End Function

'Getting  the Tangent value. ( Tan= Sin/Cos )
Public Function Tangent(ByVal angle As Double) As Double
On Error GoTo Handle
    Tangent = Sine(angle) / Cosine(angle)
Exit Function
Handle:
    Tangent = 90 'Because 'cose 90' is zero and will cause an error 'Divisible by Zero'
End Function


Public Function DrawPolygon(Pic As PictureBox, _
                        ByVal X As Double, _
                        ByVal Y As Double, _
                        Optional IsCentre As Boolean = True, _
                        Optional ByVal Sides As Double = 4, _
                        Optional ByVal SideLength As Double = 100, _
                        Optional FillStyle As rgnFillStyle = 0, _
                        Optional ByVal Rotation As Double = 0, _
                        Optional imgData As StdPicture = 0) As Boolean
                        
On Error GoTo Handle
If val(Sides) < 3 Then DrawPolygon = False: Exit Function   'Exiting none valid Sidecount

Dim Side  As Integer
Dim CtrAngle   As Double
Dim Pnt() As COORD

CtrAngle = 360 / Sides 'Getting the Centre angle
ReDim Pnt(1 To Sides) As COORD   'Getting the  vaiables for vertex of polygon

'-------------------------------------------------------------------------
'The bellow centering eqation I derived myself and may contain errors.
'Anyway it works Smoothly and perfectly under normal dimensions
'-------------------------------------------------------------------------
    If IsCentre = True Then 'if the given X,Y are the Orgine
        Dim VDist As Double
         'Determining the distance from the orgin to the centre of on side.
        VDist = SideLength / (2 * Tangent(CtrAngle / 2))
        'Setting the first vertex corresponding to the value of rotation
        Pnt(1).X = X + VDist * Cosine(270 + Rotation) + SideLength / 2 * Cosine(180 + Rotation)
        Pnt(1).Y = Y + VDist * Sine(270 + Rotation) + SideLength / 2 * Sine(180 + Rotation)
    Else 'If the given X,Y are one vertex
        Pnt(1).X = X: Pnt(1).Y = Y
    End If

'-------------------------------------------------------------------------
'Proceeding on determining other vertices ( In Math 'Vector Method' )  < x2 =x1 + (r * Theta) >
'-------------------------------------------------------------------------
    For Side = 2 To Sides
        Pnt(Side).X = Pnt(Side - 1).X + SideLength * Cosine((Side - 2) * CtrAngle + Rotation)
        Pnt(Side).Y = Pnt(Side - 1).Y + SideLength * Sine((Side - 2) * CtrAngle + Rotation)
    Next Side

'-------------------------------------------------------------------------
'Shading the sides ( We need to convert the values to Pixel mode.
'-------------------------------------------------------------------------
Dim Vtx() As COORD 'Defining  new verticies in Pixel Mode
ReDim Vtx(1 To Sides)

For Side = 1 To Sides
    Vtx(Side).X = Pnt(Side).X / Screen.TwipsPerPixelX
    Vtx(Side).Y = Pnt(Side).Y / Screen.TwipsPerPixelY
Next Side

Dim hBrush As Long, hRgn As Long    'Defining color filling Variables
    Select Case FillStyle
        Case 0  'Solid
            hBrush = 0
        Case 8  'Bitmap pattern
            hBrush = CreatePatternBrush(imgData)
        Case Else   'Other Patterns
            Dim Br As LOGBRUSH
            Br.lbColor = Pic.FillColor
            Br.lbHatch = FillStyle - 1
            Br.lbStyle = 2
            hBrush = CreateBrushIndirect(Br)
    End Select
    
    hRgn = CreatePolygonRgn(Vtx(1), Sides, ALTERNATE)   'Getting the polygon region from the Vertieces
    If hRgn Then FillRgn Pic.hdc, hRgn, hBrush  'Filling the determined area
    DeleteObject hRgn
    DeleteObject hBrush
    
'-------------------------------------------------------------------------
'Finally Drawing The each Side borders ( if we first try to draw the border
'then it  may covered by the shading
'-------------------------------------------------------------------------
DrawLines:
    For Side = 2 To Sides
        Pic.Line (Pnt(Side - 1).X, Pnt(Side - 1).Y)-(Pnt(Side).X, Pnt(Side).Y), vbBlack 'Drawing the sides
    Next Side
    Pic.Line (Pnt(Sides).X, Pnt(Sides).Y)-(Pnt(1).X, Pnt(1).Y), vbBlack 'Drawing the last  side and closing  the polygon
    DrawPolygon = True
Exit Function
Handle:
'MsgBox "Sorry ! .Unexpected error occured on drawing polygon." & vbCrLf & "Err." & Err.Number & " : " & Err.Description, vbCritical, "Error"
DrawPolygon = False
End Function

Public Function StartOutlook() As Object
    Dim oOutlook        As Object
    Dim sAPPPath        As String
 
    On Error Resume Next
    
    If IsAppRunning("Outlook.Application") = True Then    'Outlook was already running
        Set oOutlook = GetObject(, "Outlook.Application")    'Bind to existing instance of Outlook
    Else    'Could not get instance of Outlook, so create a new one
        Set oOutlook = CreateObject("Outlook.Application")
        If Not oOutlook Is Nothing Then GoTo suite
        sAPPPath = GetAppExePath("outlook.exe")    'determine outlook's installation path
        Shell (sAPPPath)    'start outlook
        Sleep 1000
        Set oOutlook = GetObject(, "Outlook.Application")    'Bind to existing instance of Outlook
    End If
    Trace "StartOutlook " & Err.Description
    If oOutlook Is Nothing Then Trace "Outlook non charg�"
suite:
    Set StartOutlook = oOutlook
 
End Function
 
Public Function StartExcel() As Object
    Dim oExcel        As Object
    Dim sAPPPath        As String
 
    On Error Resume Next
    
    If IsAppRunning("Excel.Application") = True Then    'Excel was already running
        Set oExcel = GetObject(, "Excel.Application")    'Bind to existing instance of Outlook
    Else    'Could not get instance of Outlook, so create a new one
        Set oExcel = CreateObject("Excel.Application")
        If Not oExcel Is Nothing Then GoTo suite
        sAPPPath = GetAppExePath("excel.exe")    'determine outlook's installation path
        Shell (sAPPPath)    'start outlook
        Sleep 1000
        Set oExcel = GetObject(, "Excel.Application")    'Bind to existing instance of Outlook
    End If
    Trace "StartExcel " & Err.Description
    If oExcel Is Nothing Then Trace "Excel non charg�"
suite:
    Set StartExcel = oExcel
 
End Function
 
Function IsAppRunning(sApp As String) As Boolean
    On Error GoTo Error_Handler
    Dim oApp            As Object
 
    Set oApp = GetObject(, sApp)
    IsAppRunning = True
 
Error_Handler_Exit:
    On Error Resume Next
    Set oApp = Nothing
    Exit Function
 
Error_Handler:
    Resume Error_Handler_Exit
End Function
 
Function GetAppExePath(ByVal sExeName As String) As String
    On Error GoTo Error_Handler
    Dim WSHShell        As Object
 
    Set WSHShell = CreateObject("Wscript.Shell")
    GetAppExePath = WSHShell.RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\" & sExeName & "\")
 
Error_Handler_Exit:
    On Error Resume Next
    Set WSHShell = Nothing
    Exit Function
 
Error_Handler:
    If Err.Number = -2147024894 Then
        'Cannot locate requested exe????
    Else
        MsgBox "The following error has occurred." & vbCrLf & vbCrLf & _
               "Error Number: " & Err.Number & vbCrLf & _
               "Error Source: GetAppExePath" & vbCrLf & _
               "Error Description: " & Err.Description, _
               vbCritical, "An Error has Occurred!"
    End If
    Resume Error_Handler_Exit
End Function

