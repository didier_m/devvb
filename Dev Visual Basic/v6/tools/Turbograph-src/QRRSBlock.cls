VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QRRSBlock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'---------------------------------------------------------------
' QRRSBlock
'---------------------------------------------------------------


Dim QR_RS_BLOCK_TABLE() As Variant
'Dim aQRCode As QRCode
Public totalCount
Public dataCount

Private Sub Class_Initialize()
    
    QR_RS_BLOCK_TABLE = Array( _
        Array(1, 26, 19), Array(1, 26, 16), Array(1, 26, 13), Array(1, 26, 9), Array(1, 44, 34), _
        Array(1, 44, 28), Array(1, 44, 22), Array(1, 44, 16), _
        Array(1, 70, 55), Array(1, 70, 44), Array(2, 35, 17), Array(2, 35, 13), _
        Array(1, 100, 80), Array(2, 50, 32), Array(2, 50, 24), Array(4, 25, 9), _
        Array(1, 134, 108), Array(2, 67, 43), Array(2, 33, 15, 2, 34, 16), Array(2, 33, 11, 2, 34, 12), _
        Array(2, 86, 68), Array(4, 43, 27), Array(4, 43, 19), Array(4, 43, 15), _
        Array(2, 98, 78), Array(4, 49, 31), Array(2, 32, 14, 4, 33, 15), Array(4, 39, 13, 1, 40, 14), _
        Array(2, 121, 97), Array(2, 60, 38, 2, 61, 39), Array(4, 40, 18, 2, 41, 19), Array(4, 40, 14, 2, 41, 15), _
        Array(2, 146, 116), Array(3, 58, 36, 2, 59, 37), Array(4, 36, 16, 4, 37, 17), Array(4, 36, 12, 4, 37, 13), _
        Array(2, 86, 68, 2, 87, 69), Array(4, 69, 43, 1, 70, 44), Array(6, 43, 19, 2, 44, 20), Array(6, 43, 15, 2, 44, 16) _
    )

    'Set aQRCode = New QRCode
End Sub


Public Function QRRSBlock(tC, dC)
    totalCount = tC
    dataCount = dC
End Function

Public Function getDataCount()
    getDataCount = dataCount
End Function

Public Function getTotalCount()
    getTotalCount = totalCount
End Function

Public Static Function getRSBlocks(typeNumber, errorCorrectLevel)
    Dim rsBlock
    Dim length
    Dim List As Collection
    Dim i, j As Integer
    Dim count
    Dim aQRRSBlock As QRRSBlock
    rsBlock = getRsBlockTable(typeNumber, errorCorrectLevel)
    length = (UBound(rsBlock) + 1) / 3
    Set List = New Collection

    For i = 0 To length - 1
        count = rsBlock(i * 3 + 0)
        totalCount = rsBlock(i * 3 + 1)
        dataCount = rsBlock(i * 3 + 2)
        For j = 0 To count - 1
            Set aQRRSBlock = New QRRSBlock
            aQRRSBlock.QRRSBlock totalCount, dataCount
            List.Add aQRRSBlock
        Next
    Next

    Set getRSBlocks = List
End Function

Public Static Function getRsBlockTable(typeNumber, errorCorrectLevel)

    Select Case errorCorrectLevel
    Case QR_ERROR_CORRECT_LEVEL_L
        getRsBlockTable = QR_RS_BLOCK_TABLE((typeNumber - 1) * 4 + 0)
    Case QR_ERROR_CORRECT_LEVEL_M
        getRsBlockTable = QR_RS_BLOCK_TABLE((typeNumber - 1) * 4 + 1)
    Case QR_ERROR_CORRECT_LEVEL_Q
        getRsBlockTable = QR_RS_BLOCK_TABLE((typeNumber - 1) * 4 + 2)
    Case QR_ERROR_CORRECT_LEVEL_H
        getRsBlockTable = QR_RS_BLOCK_TABLE((typeNumber - 1) * 4 + 3)
    Case Else
        trigger_error "tn:typeNumber/ecl:errorCorrectLevel", E_USER_ERROR
    End Select
End Function


