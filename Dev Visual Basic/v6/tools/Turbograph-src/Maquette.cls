VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Maquette"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'**********************************************
'*                                            *
'* Cette classe contient la d�finition d'un   *
'* Objet Maquette                             *
'*                                            *
'**********************************************


'---> Liste des propri�t�s
Public Nom As String
Public Orientation As String
Public Hauteur As Single
Public Largeur As Single
Public HauteurVar As Single
Public MargeTop As Single
Public MargeLeft As Single
Public PageGarde As Boolean
Public PageSelection As Boolean
Public Progiciel As String
Public Client As String
Public NavigaHTML As String

'---> Collections du model objet
Public Sections As Collection
Public Tableaux As Collection

'---> Stocker les ordres d'affichage dans un tableau dynamic
Private pOrdreAffichage() As String
Public nEntries As Integer 'var utilis�e pour le redimensionnement de la matrice des ordres d'affichage

'---> Gestion du Mutlilangue
Private aLb As Libelle

Private Sub Class_Initialize()

'-> Initialisation des classes propri�t�s
    
Set Sections = New Collection
Set Tableaux = New Collection

'-> index pour les ordres d'affichage
nEntries = 1
    
End Sub

Public Function AddOrdreAffichage(ByVal Value As String) As Integer
    ReDim Preserve pOrdreAffichage(1 To nEntries)
    pOrdreAffichage(nEntries) = Value
    AddOrdreAffichage = nEntries
    nEntries = nEntries + 1
End Function

Public Function GetOrdreAffichage(ByVal nIndex As Integer) As String
    GetOrdreAffichage = pOrdreAffichage(nIndex)
End Function
