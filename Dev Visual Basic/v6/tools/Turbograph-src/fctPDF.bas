Attribute VB_Name = "fctPDF"
Option Explicit

Public clPDF As New clsPDF

Public Sub PrintSpoolInitPDF(FileName As String, ByRef aspool As Spool)
'-> on initialise le fichier PDF de sortie
  
clPDF.Title = "Spool DEAL INFORMATIQUE"
clPDF.ScaleMode = pdfCentimeter
clPDF.PaperSize = pdfA4
clPDF.Margin = 0
clPDF.orientation = aspool.Maquette.orientation
clPDF.InitPDFFile FileName


End Sub

Public Sub PrintPageSpoolPDF(ByRef aspool As Spool, ByVal PageToPrint As Integer)

'---> Cette proc�dure imprime une page d'un spool

Dim i As Integer, j As Integer
Dim PositionX As Long
Dim PositionY As Long
Dim IsPaysage As Boolean
Dim Ligne As String
Dim TypeObjet As String
Dim NomObjet As String
Dim TypeSousObjet As String
Dim NomSousObjet As String
Dim Param As String
Dim DataFields As String
Dim FirstObj As Boolean
Dim aNode As Node
Dim ErrorCode As Integer
Dim aSection As Section
Dim x As Control
Dim MargeOld As Integer

On Error GoTo GestError

'-> Initialiser le RTF d'impression de la page des s�lections
If PageToPrint = 0 Then
    '-> Pointer sur la section texte
    Set aSection = aspool.Maquette.Sections(SelectRTFKey)
    '-> Vider le RTF associ�
    frmLib.Rtf(aSection.IdRTf).Text = ""
End If

'-> Sauvegarde de l'ancienne marge
If DebordementTop And Not TypeOf Sortie Is PictureBox Then
    MargeOld = MargeX - GetDeviceCaps(Sortie.hdc, PHYSICALOFFSETX)
End If

'-> R�cup�ration des marges internes du contexte de p�riph�rique
MargeX = GetDeviceCaps(Sortie.hdc, PHYSICALOFFSETX)
MargeY = GetDeviceCaps(Sortie.hdc, PHYSICALOFFSETY)

'-> Initialiser la position X , y du pointeur sur les marges du document
PositionY = -MargeY + Sortie.ScaleY(aspool.Maquette.MargeTop, 7, 3)
PositionX = -MargeX + Sortie.ScaleX(aspool.Maquette.MargeLeft, 7, 3)

PositionX = PositionX * Zoom
PositionY = PositionY * Zoom

'-> Gerer le debordement si le top de debordement est activer on refait l'impression avec une translation
If DebordementTop Then
    MargeX = MargeX + Sortie.ScaleWidth + MargeOld
    '-> on reinitialise le top
    DebordementTop = False
End If

'-> Indiquer l'�tat du premier objet que l'on trouve
FirstObj = True

'-> Lecture des lignes de la page
For i = 1 To NumEntries(aspool.GetPage(PageToPrint), Chr(0))
    '-> Gestion de la temporisation
    If Mode = 1 Then
        TailleLue = i
        DrawWait
    End If
    '-> R�cup�ration de la ligne en cours
    Ligne = Trim(Entry(i, aspool.GetPage(PageToPrint), Chr(0)))
    '-> Ne rien imprimer si page � blanc
    If Trim(Ligne) = "" Then GoTo NextLigne
    
    If PageToPrint = 0 Then
        frmLib.Rtf(aSection.IdRTf).Text = frmLib.Rtf(aSection.IdRTf).Text & Chr(13) & Chr(10) & Ligne
        frmLib.Rtf(aSection.IdRTf).SelStart = 0
        frmLib.Rtf(aSection.IdRTf).SelLength = Len(frmLib.Rtf(aSection.IdRTf).Text)
        frmLib.Rtf(aSection.IdRTf).SelFontName = "Lucida Console"
    Else
        If InStr(1, Ligne, "[") = 1 Then
            '-> r�cup�ration des param�tres
            AnalyseObj Ligne, TypeObjet, NomObjet, TypeSousObjet, NomSousObjet, Param, DataFields
            '-> Selon le type d'objet
            If UCase$(TypeObjet) = "ST" Then
                If Not PrintSectionPDF(NomObjet, Param, DataFields, FirstObj, PositionX, PositionY, aspool) Then Exit For
                FirstObj = False
            ElseIf UCase$(TypeObjet) = "TB" Then
                If Not PrintTableauPDF(NomObjet, Param, DataFields, PositionX, PositionY, NomSousObjet, FirstObj, aspool) Then Exit For
                FirstObj = False
            End If
        End If 'Si premier caract�re = "["
    End If 'Si on imprime la page de s�lection
NextLigne:
Next 'Pour toutes les lignes de la page

'-> Tester si le bas de page est d�pass�
If Sortie.ScaleY(PositionY, 3, 7) > aspool.Maquette.Hauteur * Zoom Then
    Sortie.PaintPicture frmLib.pibPage.Picture, Sortie.ScaleX(0.5, 7, 3), Sortie.ScaleY(aspool.Maquette.Hauteur, 7, 3) - Sortie.ScaleY(frmLib.pibPage.Height, 1, 3) - Sortie.ScaleY(1, 7, 3)
End If 'Si on a d�pass� le bas de page

'-> Imprimer la page de s�lection
If PageToPrint = 0 Then
    '-> imprimer la section
    PrintObjPdf 0, 0, aSection, "", 0, 0, aspool
End If

'-> R�afficher la page si on est en mode Visu
If TypeOf Sortie Is PictureBox Then
    '-> D�bloquer la feuille
    'aspool.frmDisplay.Enabled = True
End If

'-> Gestion de la temporisation
If Mode = 1 Then
    MDIMain.StatusBar1.Refresh
    '-> Bloquer l'interface
    MDIMain.Enabled = True
    Screen.MousePointer = 0
End If

Exit Sub
    
       
GestError:
    MsgBox Err.Number & " " & Err.Description
    '-> Gestion de la temporisation
    If Mode = 1 Then
        MDIMain.StatusBar1.Refresh
        '-> Bloquer l'interface
        MDIMain.Enabled = True
        Screen.MousePointer = 0
    End If

    '-> Setting d'une erreur
    'Call GestError(ErrorCode, CStr(PageToPrint), aspool)
    
    

End Sub

Private Function PrintSectionPDF(ByVal SectionName As String, ByRef Param As String, _
                         ByRef DataFields As String, ByRef FirstObj As Boolean, _
                         ByRef PositionX As Long, ByRef PositionY As Long, ByRef aspool As Spool) As Boolean

'---> Cette proc�dure est charg�e d'imprimer une section

Dim aSection As Section
Dim i As Integer
Dim NomObjet As String
Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim DebutRangX As Long
Dim DebutRangY As Long

On Error GoTo GestError



'-> Pointer sur la section � imprimer
Set aSection = aspool.Maquette.Sections(UCase$(SectionName))

'-> Imprimer la section
If Not PrintObjPdf(PositionX, PositionY, aSection, DataFields, DebutRangX, DebutRangY, aspool) Then GoTo GestError

'-> Impression des objets associ�s dans l'ordre d'affichage
For i = 1 To aSection.nEntries - 1
    NomObjet = aSection.GetOrdreAffichage(i)
    If UCase$(Entry(1, NomObjet, "-")) = "CDR" Then
        '-> Pointer sur le cadre
        Set aCadre = aSection.Cadres(UCase$(Entry(2, NomObjet, "-")))
        '-> Imprimer l'objet
        If Not PrintObjPdf(PositionX, PositionY, aCadre, DataFields, DebutRangX, DebutRangY, aspool, aCadre.MargeInterne) Then GoTo GestError
        '-> Liberer le pointeur
        Set aCadre = Nothing
    ElseIf UCase$(Entry(1, NomObjet, "-")) = "BMP" Then
        Set aBmp = aSection.Bmps(UCase$(Entry(2, NomObjet, "-")))
        If Not PrintBmpPdf(aBmp, DebutRangX, DebutRangY, DataFields, aspool) Then GoTo GestError
    End If
Next

'-> Positionner le pointeur de position apr�s l'�dition de la section
PositionY = Sortie.ScaleY(aSection.Hauteur * Zoom, 7, 3) + PositionY 'Sortie.ScaleX(DebutRangY, 1, 3)

'-> Liberer les pointeurs
Set aSection = Nothing

PrintSectionPDF = True

Exit Function

GestError:

    'Call GestError(36, aSection.Nom, aspool)
    PrintSectionPDF = False

End Function

Private Function PrintTableauPDF(ByRef NomObjet As String, ByRef Param As String, _
                         ByRef DataFields As String, ByRef PositionX As Long, _
                         ByRef PositionY As Long, ByRef NomSousObjet As String, ByRef FirstObj As Boolean, ByRef aspool As Spool) As Boolean

Dim aTb As Tableau
Dim nLig As Integer


On Error GoTo GestError

'-> Pointer sur le tableau pass� en argument
Set aTb = aspool.Maquette.Tableaux(UCase$(NomObjet))

'-> R�cup�ration de la ligne de tableau � imprimer
nLig = CInt(Entry(2, Param, "\"))

'-> Imprimer le block de ligne
PositionY = PrintBlockPdf(NomSousObjet, Param, DataFields, PositionX, PositionY, aTb, nLig, aspool)

'-> Quitter si valeur d'erreur
If PositionY = -9999 Then GoTo GestError

'-> Renvoyer une valeur de succ�s
PrintTableauPDF = True

Exit Function

GestError:
    'Call GestError(37, aTb.Nom, aspool)
    PrintTableauPDF = False


End Function

Private Function PrintBlockPdf(ByRef NomSousObjet As String, ByRef Param As String, _
                        ByRef DataFields As String, ByVal PosX As Long, _
                        ByRef PosY As Long, ByRef aTb As Tableau, ByVal Ligne As Integer, ByRef aspool As Spool) As Long

'---> Cette Proc�dure imprime un block de ligne

Dim aBlock As Block
Dim Champ As String
Dim aField() As String
Dim i As Long
Dim aCell As Cellule
Dim HauteurLigPix As Long
Dim ListeField() As String
Dim Tempo

On Error GoTo GestBlock

'-> Pointer sur le block de tableau � �diter
Set aBlock = aTb.Blocks("BL-" & UCase$(NomSousObjet))

'-> Initaliser le block
InitBlock aBlock, aspool

'-> Calculer la modification des positions X et Y des blocks de ligne. _
Les coordonn�es de chaque cellule �tant calcul�e sur un X et Y = 0 de base

Select Case aBlock.AlignementLeft
    Case 2 '-> Marge gauche
        PosX = Sortie.ScaleX(aspool.Maquette.MargeLeft * Zoom, 7, 3)
    Case 3 '-> Centr�
        PosX = Sortie.ScaleX((aspool.Maquette.Largeur * Zoom - aBlock.Largeur * Zoom) / 2, 7, 3)
    Case 4 '-> Marge droite
        PosX = Sortie.ScaleX(aspool.Maquette.Largeur * Zoom - aBlock.Largeur * Zoom - aspool.Maquette.MargeLeft * Zoom, 7, 3)
    Case 5 '-> Sp�cifi�
        PosX = Sortie.ScaleX(aBlock.Left * Zoom, 7, 3)
End Select

'-> Tenir compte de la marge interne
PosX = (PosX - MargeX)

If aBlock.AlignementTop = 5 And Ligne <> 1 Then
Else
    Select Case aBlock.AlignementTop
        Case 1 '-> Libre
            '-> RAS le pointeur est bien positionn�
        Case 2 '-> Marge haut
            PosY = Sortie.ScaleY(aspool.Maquette.MargeTop * Zoom, 7, 3)
        Case 3 '-> Centr�
            PosY = Sortie.ScaleY((aspool.Maquette.Hauteur * Zoom - aBlock.Hauteur * Zoom) / 2, 7, 3)
        Case 4 '-> Marge Bas
            PosY = Sortie.ScaleY(aspool.Maquette.Hauteur * Zoom - aBlock.Hauteur * Zoom, 7, 3)
        Case 5 '-> Sp�cifi�
            PosY = Sortie.ScaleY(aBlock.Top * Zoom, 7, 3)
    End Select

    '-> Modifier l'alignement
    If aBlock.AlignementTop <> 1 Then PosY = PosY - MargeY

End If

'-> Impression du block de ligne
For i = 1 To aBlock.NbCol
    '-> Pointer sur la cellule � dessiner
    Set aCell = aBlock.Cellules("L" & Ligne & "C" & i)
    '-> Remplacer les champs par leur valeur
    aCell.ReplaceField DataFields
    '-> Imprimer la cellule
    If Not DrawCellPDF(aCell, PosX, PosY, aspool, aTb.Nom, NomSousObjet) Then GoTo GestBlock
    '-> R�cup�rer la hauteur de la ligne
    HauteurLigPix = (aCell.Y2 - aCell.y1) * Zoom
Next

'-> forcer le rafraichissement
If TypeOf Sortie Is PictureBox Then Sortie.Refresh

If Ligne = aBlock.NbLigne Then PosY = PosY + 1 * Zoom
PrintBlockPdf = PosY + HauteurLigPix

Exit Function

GestBlock:
    
    'Call GestError(40, aTb.Nom & "|" & NomSousObjet, aspool)
    PrintBlockPdf = -9999
    
    

End Function

Public Function DrawCellPDF(ByRef aCell As Cellule, ByVal PosX As Long, ByVal PosY As Long, ByRef aspool As Spool, ByRef NomTb As String, Optional NomBlock As String) As Boolean

'---> Fonction qui dessine une cellule

Dim hdlPen As Long
Dim hdlBrush As Long
Dim hdlBordure As Long
Dim hdlFont As Long
Dim OldPen As Long
Dim oldBrush As Long
Dim oldFont As Long
Dim aPoint As POINTAPI
Dim aRect As RECT
Dim Res As Long
Dim aFt As FormatedCell
Dim xTemp
Dim PrintSigne As Boolean
Dim Couleur As Long
Dim aLbl As Label
Dim aTb As Tableau
Dim aBlock As Block
Dim aCellContenuTemp As String
Dim FindTextTemp As String
Dim i As Integer
Dim CellAlign As pdfTextAlign
Dim CellBackColor As String
Dim CellFill As Boolean

On Error GoTo GestErr

Dim x1 As Single, X2 As Long, y1 As Single, Y2 As Long

x1 = aCell.x1 * Zoom + PosX
X2 = aCell.X2 * Zoom + PosX
y1 = PosY
Y2 = (aCell.Y2 * Zoom) + PosY - (aCell.y1 * Zoom)

'-> on dessine la cellule
'If Not aCell.FondTransparent Then clPDF.Rectangle  x1, y1, X2 - x1, Y2 - y1

'-> Setting du Rect de la cellule
aRect.Left = x1
aRect.Top = y1
aRect.Right = X2 - 1
aRect.Bottom = Y2 - 1


Dim pDrawText As Long
Dim Rect2 As RECT
Dim DifL As Long
Dim DifH As Long
Dim IsCellAjust As Boolean
Dim hdlRgn As Long
Dim AlignBase(1 To 2, 3 To 6) As Long

'-> Appliquer les options de font
Sortie.FontName = IsGoodFont(aCell.FontName)
Sortie.FontSize = aCell.FontSize * Zoom
Sortie.FontBold = aCell.FontBold
Sortie.FontItalic = aCell.FontItalic
Sortie.FontUnderline = aCell.FontUnderline
Sortie.ForeColor = aCell.FontColor

'***********************************
'* Dessin du contenu de la cellule *
'***********************************
    
'-> R�cup�rer l'option de d'alignement interne
IsCellAjust = aCell.AutoAjust
    
'-> Faire une copie de Rect
Rect2.Left = aRect.Left
Rect2.Right = aRect.Right
Rect2.Top = aRect.Top
Rect2.Bottom = aRect.Bottom
    
'-> Dans un premier temps, calculer la taille necessaire pour afficher si ajustement automatique
If IsCellAjust Then _
    DrawText Sortie.hdc, aCell.ContenuCell, Len(aCell.ContenuCell), Rect2, DT_CALCRECT Or DT_WORDBREAK
   
'-> R�cup�ration des diff�rences de largeur et de hauteur
DifL = (aRect.Right - aRect.Left) - (Rect2.Right - Rect2.Left)
DifH = (aRect.Bottom - aRect.Top) - (Rect2.Bottom - Rect2.Top)
    
'-> Calcul des alignements de base
AlignBase(1, 3) = Rect2.Left + DifL / 2
AlignBase(2, 3) = Rect2.Right + DifL / 2

AlignBase(1, 4) = Rect2.Left + DifL
AlignBase(2, 4) = Rect2.Right + DifL

AlignBase(1, 5) = Rect2.Top + DifH / 2
AlignBase(2, 5) = Rect2.Bottom + DifH / 2

AlignBase(1, 6) = Rect2.Top + DifH
AlignBase(2, 6) = Rect2.Bottom + DifH
        
'-> Alignement interne dans le rectangle
Select Case aCell.CellAlign
    
    Case 1
        If IsCellAjust Then
            pDrawText = DT_LEFT
            '-> Pas necessaire de modifier les alignements
        Else
            pDrawText = DT_LEFT Or DT_TOP
        End If
        
    Case 2
        If IsCellAjust Then
            pDrawText = DT_CENTER
            Rect2.Left = AlignBase(1, 3)
            Rect2.Right = AlignBase(2, 3)
        Else
            pDrawText = DT_CENTER Or DT_TOP
        End If
    
    Case 3
        If IsCellAjust Then
            pDrawText = DT_RIGHT
            Rect2.Left = AlignBase(1, 4)
            Rect2.Right = AlignBase(2, 4)
        Else
            pDrawText = DT_RIGHT Or DT_TOP
        End If
                
    Case 4
        If IsCellAjust Then
            pDrawText = DT_LEFT
            Rect2.Top = AlignBase(1, 5)
            Rect2.Bottom = AlignBase(2, 5)
        Else
            pDrawText = DT_VCENTER Or DT_LEFT
        End If
    
    Case 5
        If IsCellAjust Then
            pDrawText = DT_CENTER
            Rect2.Top = AlignBase(1, 5)
            Rect2.Bottom = AlignBase(2, 5)
            Rect2.Left = AlignBase(1, 3)
            Rect2.Right = AlignBase(2, 3)
        Else
            pDrawText = DT_VCENTER Or DT_CENTER
        End If
    
    Case 6
        If IsCellAjust Then
            pDrawText = DT_RIGHT
            Rect2.Top = AlignBase(1, 5)
            Rect2.Bottom = AlignBase(2, 5)
            Rect2.Left = AlignBase(1, 4)
            Rect2.Right = AlignBase(2, 4)
        Else
            pDrawText = DT_VCENTER Or DT_RIGHT
        End If
    
    Case 7
        If IsCellAjust Then
            pDrawText = DT_LEFT
            Rect2.Top = AlignBase(1, 6)
            Rect2.Bottom = AlignBase(2, 6)
        Else
            pDrawText = DT_BOTTOM Or DT_LEFT
        End If
    
    Case 8
        If IsCellAjust Then
            pDrawText = DT_CENTER
            Rect2.Top = AlignBase(1, 6)
            Rect2.Bottom = AlignBase(2, 6)
            Rect2.Left = AlignBase(1, 3)
            Rect2.Right = AlignBase(2, 3)
        Else
            pDrawText = DT_BOTTOM Or DT_CENTER
        End If
    
    Case 9
        If IsCellAjust Then
            pDrawText = DT_RIGHT
            Rect2.Top = AlignBase(1, 6)
            Rect2.Bottom = AlignBase(2, 6)
            Rect2.Left = AlignBase(1, 4)
            Rect2.Right = AlignBase(2, 4)
        Else
            pDrawText = DT_BOTTOM Or DT_RIGHT
        End If
    
End Select
    
'-> on verifie si l'impression ne deborde pas
If Rect2.Right > Sortie.ScaleWidth And Not DebordementTop Then
    If AllowDebordement Then DebordementTop = True
End If
    
If IsCellAjust Then
    pDrawText = pDrawText Or DT_WORDBREAK
Else
    pDrawText = pDrawText Or DT_SINGLELINE
End If
    

'-> Gestion des formats num�rics
Select Case aCell.TypeValeur
    Case 0 '-> RAS
        PrintSigne = False
    Case 1 '-> Caract�re
        '-> Faire un susbstring
        aCell.ContenuCell = Mid$(aCell.ContenuCell, 1, CInt(aCell.Msk))
        PrintSigne = False
    Case 2 '-> Numeric
'-> V�rifier s'il y a quelquechose � formater
        If Trim(aCell.ContenuCell) = "" Then
        Else
             aFt = FormatNumTP(Trim(aCell.ContenuCell), aCell.Msk, NomBlock, "L" & aCell.Ligne & "C" & aCell.Colonne)
            '-> Rajouter Trois blancs au contenu de la cellule
            aFt.strFormatted = aFt.strFormatted & "   "
            If aFt.Ok Then
                '-> Mettre e rouge si necessaire
                If aFt.value < 0 And aFt.idNegatif > 1 Then Sortie.ForeColor = QBColor(12)
                '-> affecter le contenu de la cellule
                aCell.ContenuCell = aFt.strFormatted
                '-> Si c'est un 0 mettre � blanc si pas imprimer
                If CDbl(aCell.ContenuCell) = 0 And Not (aCell.PrintZero) Then
                    aCell.ContenuCell = ""
                Else
                    If aFt.value < 0 Then
                        If aFt.idNegatif = 0 Or aFt.idNegatif = 3 Then
                            aCell.ContenuCell = "- " & aCell.ContenuCell
                            PrintSigne = False
                        ElseIf aFt.idNegatif = 1 Or aFt.idNegatif = 4 Then
                            PrintSigne = True
                        End If
                    End If
                End If
            Else
                '-> Ne pas imprimer  de signe
                PrintSigne = False
            End If
        End If
End Select


'-> Faire de rect la zone de clipping en cours
hdlRgn = CreateRectRgn&(aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
SelectClipRgn Sortie.hdc, hdlRgn

'-> alignement de la cellule
Select Case aCell.CellAlign
    Case 1, 4, 7
        CellAlign = pdfAlignleft
    Case 2, 5, 8
        CellAlign = pdfCenter
    Case 3, 6, 9
        CellAlign = pdfAlignRight
End Select

'-> couleur de fond de la cellule
If aCell.BackColor = vbWhite Then
    CellBackColor = ""
    CellFill = False
Else
    CellBackColor = CStr(aCell.BackColor)
    CellFill = True
End If

'-> Postionnement du rectangle de dessin
'DrawText Sortie.hDC, aCell.ContenuCell, Len(aCell.ContenuCell), Rect2, pDrawText Or DT_NOPREFIX
clPDF.DrawCell aCell.ContenuCell, aCell.FontName, aCell.FontSize, vbBlack, CellBackColor, "", 1, CellAlign, "0", CellFill, frmLib.ScaleX(x1, 3, 2), frmLib.ScaleY(aRect.Top, 3, 2) - aCell.y1, frmLib.ScaleX(X2 - x1, 3, 2), frmLib.ScaleY(aCell.Y2 - aCell.y1, 3, 2)

If PrintSigne Then DrawText Sortie.hdc, "- ", 2, Rect2, pDrawText Or DT_NOPREFIX

'-> Faire de la fen�tre entiere la zone de clipping
SelectClipRgn Sortie.hdc, 0

'-> Dessin des bordures de la cellule
SelectObject Sortie.hdc, hdlBordure

'-> Dessin de la bordure Bas
If aCell.BordureBas Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureBas Then
Else
    DrawBordurePdf frmLib.ScaleX(aRect.Left - 1.3, 3, 6), frmLib.ScaleY(aRect.Bottom, 3, 6), frmLib.ScaleX(aRect.Right, 3, 6), frmLib.ScaleY(aRect.Bottom, 3, 6), Couleur
End If

'-> Dessin de la bordure haut
If aCell.BordureHaut Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureHaut Then
Else
    DrawBordurePdf frmLib.ScaleX(aRect.Left - 1.3, 3, 6), frmLib.ScaleY(aRect.Top - 1, 3, 6), frmLib.ScaleX(aRect.Right, 3, 6), frmLib.ScaleY(aRect.Top - 1, 3, 6), Couleur
End If


'-> Dessin de la bordure gauche
If aCell.BordureGauche Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureGauche Then
Else
    DrawBordurePdf frmLib.ScaleX(aRect.Left - 1, 3, 6), frmLib.ScaleY(aRect.Bottom - 0.5, 3, 6), frmLib.ScaleX(aRect.Left - 1, 3, 6), frmLib.ScaleY(aRect.Top - 1.5, 3, 6), Couleur
End If

'-> Dessin de la bordure Droite
If aCell.BordureDroite Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureDroite Then
Else
    DrawBordurePdf frmLib.ScaleX(aRect.Right, 3, 6), frmLib.ScaleY(aRect.Bottom - 1, 3, 6), frmLib.ScaleX(aRect.Right, 3, 6), frmLib.ScaleX(aRect.Top - 1.5, 3, 6), Couleur
End If

''-> Gestion des angles des bordures
'If aCell.BordureGauche Or aCell.BordureHaut Then
'    '-> dessiner le coins sup�rieur gauche
'    Couleur = 0
'    DrawBordurePdf aCell.x1 + aRect.Left - 1, aCell.y1 + aRect.Top - 1, aCell.x1 + aRect.Left - 1, aCell.y1 + aRect.Top, Couleur
'End If
'
'If aCell.BordureDroite Or aCell.BordureHaut Then
'    '-> Dessiner le coin sup�rieur droit
'    Couleur = 0
'    DrawBordurePdf aCell.X2 + aRect.Left - 1, aCell.y1 + aRect.Top - 1, aCell.X2 + aRect.Left - 1, aCell.y1 + aRect.Top, Couleur
'End If
'
'If aCell.BordureBas Or aCell.BordureGauche Then
'    '-> Dessin du coin inf�rieur gauche
'    Couleur = 0
'    DrawBordurePdf aCell.x1 + aRect.Left - 1, aCell.Y2 + aRect.Top - 1, aCell.x1 + aRect.Left, aCell.Y2 + aRect.Top - 1, Couleur
'End If
'
'If aCell.BordureDroite Or aCell.BordureBas Then
'    '-> Dessin du coin inf�rieur droit
'    Couleur = 0
'    DrawBordurePdf aCell.X2 + aRect.Left - 1, aCell.Y2 + aRect.Top - 1, aCell.X2 + aRect.Left - 1, aCell.Y2 + aRect.Top, Couleur
'End If

'-> Supprimer la r�gion
DeleteObject hdlRgn

'-> Res�lectionner l'ancien stylo
If OldPen <> 0 Then
    SelectObject Sortie.hdc, OldPen
    DeleteObject hdlPen
End If

'-> Res�lectionner l'ancien pinceau
If oldBrush <> 0 Then
    SelectObject Sortie.hdc, oldBrush
    DeleteObject hdlBrush
End If

'-> Supprimer le stylo de dessin des bordures
DeleteObject hdlBordure

'-> Positionnement du label pour l'acces au d�tail
If aCell.UseAccesDet Then
    '-> V�rifier que l'on soit en visu
    If TypeOf Sortie Is PictureBox Then
        '-> cr�er un nouveau Label
        Load aspool.frmDisplay.lblAccesDet(aspool.frmDisplay.lblAccesDet.Count + 1)
        '-> Positionner l'objet
        aspool.frmDisplay.lblAccesDet(aspool.frmDisplay.lblAccesDet.Count).Left = x1
        aspool.frmDisplay.lblAccesDet(aspool.frmDisplay.lblAccesDet.Count).Width = X2 - x1
        aspool.frmDisplay.lblAccesDet(aspool.frmDisplay.lblAccesDet.Count).Top = y1
        aspool.frmDisplay.lblAccesDet(aspool.frmDisplay.lblAccesDet.Count).Height = Y2 - y1
        '-> Acc�der au tableau de premier niveau
        Set aTb = aspool.Maquette.Tableaux(UCase$(NomTb))
        '-> Pointer sur le block de tableau � �diter
        Set aBlock = aTb.Blocks("BL-" & UCase$(NomBlock))
        '-> Positionner la cl� d'acc�s au d�tail
        aspool.frmDisplay.lblAccesDet(aspool.frmDisplay.lblAccesDet.Count).Tag = aCell.KeyAccesDet & Chr(0) & Entry(2, Entry(1, aBlock.KeyAccesDet, "�"), "|")
        '-> Le rendre visible
        aspool.frmDisplay.lblAccesDet(aspool.frmDisplay.lblAccesDet.Count).Visible = True
    End If 'Si on est en vosu ou en impression
End If 'Si on a acc�s au d�tail

'-> Renvoyer une valeur de succ�s
DrawCellPDF = True

Exit Function

GestErr:

    'Call GestError(41, NomTb & "|" & NomBlock & "|" & "L" & aCell.Ligne & "C" & aCell.Colonne, aspool)
    

End Function

Private Function PrintObjPdf(PositionX As Long, PositionY As Long, _
                        aObject As Object, ByVal DataFields As String, _
                        DebutRangX As Long, DebutRangY As Long, ByRef aspool As Spool, Optional MargeInterne As Single) As Boolean


'---> Cette fonction imprime un cadre ou une section ( Code RTF + Bordures )

Dim aField() As String
Dim aRtf As RichTextBox
Dim PosX As Long, PosY As Long
Dim i As Integer
Dim Champ As String
Dim RTFValue As String
Dim fr As FORMATRANGE
Dim lTextOut As Long, lTextAmt As Long, Res As Long
Dim aPoint As POINTAPI
Dim aRect As RECT
Dim hdlPen As Long
Dim OldPen As Long
Dim hdlBrush As Long
Dim oldBrush As Long
Dim NbChamp As Integer
Dim IsCadre As Boolean
Dim IsBordure As Boolean

On Error GoTo GestErr


'-> Mettre l'�diteur RTF aux bonnes dimensions
Set aRtf = frmLib.Rtf(aObject.IdRTf)

aRtf.Width = frmLib.ScaleX(aObject.Largeur * Zoom, 7, 1)
aRtf.Height = frmLib.ScaleY(aObject.Hauteur * Zoom, 7, 1)

'-> R�cup�ration du code RTF
RTFValue = aRtf.TextRTF

'-> Cr�er une base de champs
If Trim(DataFields) = "" Then
Else
    '-> R�cup�ration du nombre de champs
    NbChamp = NumEntries(DataFields, "^")
    For i = 2 To NbChamp
        Champ = Entry(i, DataFields, "^")
        ReDim Preserve aField(1 To 2, i - 1)
        aField(1, i - 1) = "^" & Mid$(Champ, 1, 4)
        aField(2, i - 1) = RTrim(Mid$(Champ, 5, Len(Champ) - 1))
    Next
    '-> Faire un remplacement des champs par leur valeur
    For i = 1 To NbChamp - 1
        aRtf.TextRTF = Replace(aRtf.TextRTF, aField(1, i), aField(2, i))
    Next
End If

'-> Calcul des positions d'impression de l'objet selon le type d'objet � imprimer
If TypeOf aObject Is Section Then
    Select Case aObject.AlignementLeft
        Case 2 'Marge gauche
            PosX = Sortie.ScaleX(aspool.Maquette.MargeLeft * Zoom, 7, 1)
        Case 4 'Centr�
            PosX = Sortie.ScaleX((aspool.Maquette.Largeur * Zoom - aObject.Largeur * Zoom) / 2, 7, 1)
        Case 3 'Marge Droite
            PosX = Sortie.ScaleX(aspool.Maquette.Largeur * Zoom - aObject.Largeur * Zoom - aspool.Maquette.MargeLeft * Zoom, 7, 1)
        Case 5 'Sp�cifi�
            PosX = Sortie.ScaleX(aObject.Left * Zoom, 7, 1)
    End Select
    Select Case aObject.AlignementTop
        Case 1 'Libre
            PosY = Sortie.ScaleY(PositionY, 3, 1)
        Case 2 'Marge haut
            PosY = Sortie.ScaleY(aspool.Maquette.MargeTop * Zoom, 7, 1)
        Case 4 'Centr�
            PosY = Sortie.ScaleY((aspool.Maquette.Hauteur * Zoom - aObject.Hauteur * Zoom) / 2, 7, 1)
        Case 3 'Marge bas
            PosY = Sortie.ScaleY(aspool.Maquette.Hauteur * Zoom - aObject.Hauteur * Zoom - aspool.Maquette.MargeTop * Zoom, 7, 1)
        Case 5 'Sp�cifi�
            PosY = Sortie.ScaleY(aObject.Top * Zoom, 7, 1)
    End Select

    '-> Tenir compte des variations des marges internes
    PosX = PosX - Sortie.ScaleX(MargeX, 3, 1)
    If aObject.AlignementTop <> 1 Then PosY = PosY - Sortie.ScaleY(MargeY, 3, 1)
    
    '-> Setting du rect de dessin
    aRect.Left = Sortie.ScaleX(PosX, 1, 3)
    aRect.Top = Sortie.ScaleY(PosY, 1, 3)
    aRect.Right = Sortie.ScaleX(aObject.Largeur * Zoom, 7, 3) + Sortie.ScaleX(PosX, 1, 3)
    aRect.Bottom = Sortie.ScaleY(aObject.Hauteur * Zoom, 7, 3) + Sortie.ScaleY(PosY, 1, 3)

    '-> Sauvagarder la position de d�but du rang
    DebutRangX = PosX
    DebutRangY = PosY
    
ElseIf TypeOf aObject Is Cadre Then
    '-> Il faut modifier la position X et Y du cadre
    PosX = DebutRangX + Sortie.ScaleX(aObject.Left * Zoom, 7, 1)
    PosY = DebutRangY + Sortie.ScaleY(aObject.Top * Zoom, 7, 1)
    '-> Setting du rect de dessin
    aRect.Left = Sortie.ScaleX(PosX, 1, 3)
    aRect.Top = Sortie.ScaleY(PosY, 1, 3)
    aRect.Right = Sortie.ScaleX(aObject.Largeur * Zoom, 7, 3) + Sortie.ScaleX(PosX, 1, 3)
    aRect.Bottom = Sortie.ScaleY(aObject.Hauteur * Zoom, 7, 3) + Sortie.ScaleY(PosY, 1, 3)
    '-> Indiquer que c'est un cadre
    IsCadre = True
End If


'-> Imprimer le fond de l'objet
hdlBrush = CreateSolidBrush(aObject.BackColor)
hdlPen = CreatePen(PS_NULL, 1, QBColor(15))

'-> S�lection du pinceau dans le Contexte
oldBrush = SelectObject(Sortie.hdc, hdlBrush)
OldPen = SelectObject(Sortie.hdc, hdlPen)

'-> Dessin de l'objet
If IsCadre Then
    If Not aObject.IsRoundRect Then
        Res = Rectangle(Sortie.hdc, aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
    Else
        '-> Imprimer les bordures
        PrintBordurePdf aObject, aRect
        IsBordure = True
    End If
        
Else
    Res = Rectangle(Sortie.hdc, aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
End If

'-> S�lectionner les anciens objets et supprimer les objets GDI non utilis�s
If OldPen <> 0 Then
    SelectObject Sortie.hdc, OldPen
    DeleteObject hdlPen
End If

If oldBrush <> 0 Then
    SelectObject Sortie.hdc, oldBrush
    DeleteObject hdlBrush
End If

'-> Initialiser la srtucture Formatrange
fr.hdc = Sortie.hdc
fr.hdcTarget = Sortie.hdc
fr.chrg.cpMin = 0
fr.chrg.cpMax = -1

'-> Intialisation du rectangle destination
fr.rc.Left = Sortie.ScaleX(MargeInterne, 7, 1) + PosX
fr.rc.Top = Sortie.ScaleX(MargeInterne, 7, 1) + PosY
fr.rc.Right = Sortie.ScaleX(aObject.Largeur * Zoom, 7, 1) + PosX - Sortie.ScaleX(MargeInterne, 7, 1)
fr.rc.Bottom = Sortie.ScaleY(aObject.Hauteur * 2 * Zoom, 7, 1) + PosY - Sortie.ScaleY(MargeInterne, 7, 1)

'-> Initialisation du rectangle de source
fr.rcPage.Left = 0
fr.rcPage.Top = 0
fr.rcPage.Right = Sortie.ScaleX(aObject.Largeur * Zoom, 7, 1)
fr.rcPage.Bottom = Sortie.ScaleY(aObject.Hauteur * Zoom, 7, 1)

'-> Faire un setting du mode de restitution
Res = SetMapMode(Sortie.hdc, MM_TEXT)

'-> initialisation des variables de pointage de texte
lTextOut = 0
lTextAmt = SendMessage(aRtf.hWnd, WM_GETTEXTLENGTH, 0, 0)

'-> Impression du Rtf
Do While lTextOut < lTextAmt
    lTextOut = SendMessage(aRtf.hWnd, EM_FORMATRANGE, -1, fr)
    If lTextOut < lTextAmt Then
        fr.chrg.cpMin = lTextOut
        fr.chrg.cpMax = -1
    End If
Loop

'-> Lib�rer la ressource associ�e au RTF : VERSION COMPILEE
Res = SendMessage(aRtf.hWnd, EM_FORMATRANGE, -1, Null)
If Res = 0 Then
    'MsgBox "Erreur dans la lib�ration du context"
    Res = SendMessage(aRtf.hWnd, EM_FORMATRANGE, -1, vbNullString)
    'MsgBox "Apr�s Seconde tentative " & Res
End If

'-> VERSION INTERPRETEE
'Res = SendMessage(aRtf.hwnd, EM_FORMATRANGE, -1, Null)


'-> Imprimer les bordures
If Not IsBordure Then PrintBordurePdf aObject, aRect

'-> Redonner son vrai contenu au controle RTF
aRtf.TextRTF = RTFValue
aRtf.Refresh

'-> Lib�rer le pointeur sur le controle RTF
Set aRtf = Nothing

'-> Renvoyer une valeur de suvv�s
PrintObjPdf = True

Exit Function

GestErr:

    If Err.Number = 11 Then
        PrintObjPdf = True
        Exit Function
    End If

    'Call GestError(38, aObject.Nom, aspool)
    PrintObjPdf = False
    


End Function

Private Function PrintBmpPdf(aBmp As ImageObj, DebutRangX As Long, DebutRangY As Long, ByVal DataFields As String, ByRef aspool As Spool) As Boolean

'---> Impression d'un BMP

Dim PosX As Long
Dim PosY As Long
Dim aPic As PictureBox
Dim Champ As String
Dim NomFichier As String
Dim aRect As RECT
Dim hdlPen As Long
Dim OldPen As Long
Dim hdlBrush As Long
Dim oldBrush As Long
Dim i As Integer

On Error GoTo GestErr


'-> Il faut modifier la position X et Y du cadre
PosX = Sortie.ScaleX(DebutRangX, 1, 3) + Sortie.ScaleX(aBmp.Left * Zoom, 7, 3)
PosY = Sortie.ScaleY(DebutRangY, 1, 3) + Sortie.ScaleY(aBmp.Top * Zoom, 7, 3)

'-> Imprimer le bmp
Set aPic = frmLib.PicObj(aBmp.IdPic)
If aBmp.isVariable Then
    '-> Venir charg� le BMP associ�
    '-> R�cup�ration du nombre de champs
    For i = 2 To NumEntries(DataFields, "^")
        Champ = Entry(i, DataFields, "^")
        If UCase$(aBmp.Fichier) = "^" & UCase$(Mid$(Champ, 1, 4)) Then
            '-> R�cup�ration du nom du fichier
            NomFichier = RTrim(Mid$(Champ, 5, Len(Champ) - 1))
            '-> Test de l'utilisation de l'association
            If aBmp.UseAssociation Then
                '-> R�cup�rer le nom du fichier dans le fichier ini
                NomFichier = GetPictureAssociation(NomFichier)
            End If
            Exit For
        End If
    Next
    '-> V�rifier que le fichier existe
    If Dir$(NomFichier) = "" Or Trim(NomFichier) = "" Then
    Else
        If Zoom <> 1 Then
            aPic.AutoSize = True
            aPic.AutoRedraw = True
            aPic.Picture = LoadPicture(NomFichier)
            aPic.ScaleMode = vbPixels
            If Zoom < 1 Then
                '-> on redimensionne l'image
                StretchBlt aPic.hdc, 0, 0, aPic.Width * Zoom, aPic.Height * Zoom, aPic.hdc, 0, 0, aPic.Width, aPic.Height, ScrCopy
                '-> on ne conserve que la partie interessante
                StretchBlt Sortie.hdc, PosX, PosY, aPic.ScaleWidth * Zoom, aPic.ScaleHeight * Zoom, aPic.hdc, 0, 0, aPic.ScaleWidth * Zoom, aPic.ScaleHeight * Zoom, vbSrcCopy
            Else
                '-> on redimensionne l'image
                aPic.Width = aPic.Width * Zoom
                aPic.Height = aPic.Height * Zoom
                StretchBlt aPic.hdc, 0, 0, aPic.Width * Zoom, aPic.Height * Zoom, aPic.hdc, 0, 0, aPic.Width, aPic.Height, ScrCopy
                '-> on ne conserve que la partie interessante
                StretchBlt Sortie.hdc, PosX, PosY, aPic.ScaleWidth, aPic.ScaleHeight * Zoom, aPic.hdc, 0, 0, aPic.ScaleWidth, aPic.ScaleHeight * Zoom, vbSrcCopy
            End If
            Sortie.Refresh
        Else
            aPic.Picture = LoadPicture(NomFichier)
            Sortie.PaintPicture aPic.Picture, PosX, PosY
        End If
    End If
Else
    If Trim(aBmp.Fichier) = "" Then
    Else
        If Dir$(aBmp.Fichier, vbNormal) = "" Then
        Else
            If Zoom <> 1 Then
                aPic.AutoSize = True
                aPic.AutoRedraw = True
                aPic.ScaleMode = vbPixels
                aPic.Picture = LoadPicture(aBmp.Fichier)
                If Zoom < 1 Then
                    '-> on redimensionne l'image
                    StretchBlt aPic.hdc, 0, 0, aPic.Width * Zoom, aPic.Height * Zoom, aPic.hdc, 0, 0, aPic.Width, aPic.Height, ScrCopy
                    '-> on ne conserve que la partie interessante
                    StretchBlt Sortie.hdc, PosX, PosY, aPic.ScaleWidth * Zoom, aPic.ScaleHeight * Zoom, aPic.hdc, 0, 0, aPic.ScaleWidth * Zoom, aPic.ScaleHeight * Zoom, vbSrcCopy
                Else
                    '-> on redimensionne l'image
                    aPic.Width = aPic.Width * Zoom
                    aPic.Height = aPic.Height * Zoom
                    StretchBlt aPic.hdc, 0, 0, aPic.Width * Zoom, aPic.Height * Zoom, aPic.hdc, 0, 0, aPic.Width, aPic.Height, ScrCopy
                    '-> on ne conserve que la partie interessante
                    StretchBlt Sortie.hdc, PosX, PosY, aPic.ScaleWidth, aPic.ScaleHeight * Zoom, aPic.hdc, 0, 0, aPic.ScaleWidth, aPic.ScaleHeight * Zoom, vbSrcCopy
                End If
                Sortie.Refresh
            Else
                Sortie.PaintPicture aPic.Picture, PosX, PosY
            End If
        End If
    End If
End If

'-> Dessiner la bordure si necessaire
If aBmp.Contour Then
    hdlBrush = CreateSolidBrush(0)
    aRect.Left = PosX
    aRect.Top = PosY
    aRect.Right = aRect.Left + Sortie.ScaleX(aPic.Width, 1, 3)
    aRect.Bottom = aRect.Top + Sortie.ScaleY(aPic.Height, 1, 3)
    FrameRect Sortie.hdc, aRect, hdlBrush
    DeleteObject hdlBrush
End If

Set aPic = Nothing

'-> Renvoyer une valeur de succ�s
PrintBmpPdf = True

Exit Function

GestErr:

    'Call GestError(39, aBmp.SectionName & "|" & aBmp.Nom, aspool)
    PrintBmpPdf = False
    

End Function

Private Function IsGoodFont(ByVal MyFont As String) As String

'---> Cette fonction d�termine si une font est valide
On Error GoTo GestError

frmLib.FontName = MyFont
IsGoodFont = MyFont

Exit Function

GestError:

    IsGoodFont = "Arial"
    

End Function

Private Sub DrawBordurePdf(Move1, Move2, Line1, Line2, ByVal Couleur)
'--> on dessine une ligne
      
'-> on definit l'epaisseur du trait
clPDF.SetDash 0.2
'-> on definit la couleur
clPDF.SetColorStroke CLng(Couleur)
'-> on initialise le point
clPDF.MoveTo CSng(Move1), CSng(Move2)
'-> on relie le point
clPDF.LineTo CSng(Line1), CSng(Line2)
'-> on reinitialise
'clPDF.SetDash 2
'clPDF.SetColorStroke 0

End Sub

Private Sub PrintBordurePdf(aObject As Object, aRect As RECT)

Dim Temp1 As Long
Dim Temp2 As Long
Dim ClipRgn As Long
Dim OldClip As Long
Dim Pix1X As Long
Dim Pix1Y As Long
Dim aPoint As POINTAPI

Dim hdlBrush As Long
Dim oldBrush As Long
Dim hdlPen As Long
Dim OldPen As Long
Dim Res As Long

On Error Resume Next

'-> Dessin du contour de l'objet si necessaire
If TypeOf aObject Is Section Then
    If aObject.Contour Then
        hdlBrush = CreateSolidBrush(0)
        oldBrush = SelectObject(Sortie.hdc, hdlBrush)
        FrameRect Sortie.hdc, aRect, hdlBrush
    End If
ElseIf TypeOf aObject Is Cadre Then


    '-> Tenir compte de la largeur du trait
    Temp2 = frmLib.ScaleX(aObject.LargeurTrait, 3, 1)
    Temp2 = Sortie.ScaleX(Temp2, 1, 3)

    Pix1X = frmLib.ScaleX(1, 3, 1)
    Pix1X = Sortie.ScaleX(Pix1X, 1, 3)

    Pix1Y = frmLib.ScaleY(1, 3, 1)
    Pix1Y = Sortie.ScaleY(Pix1Y, 1, 3)

    '-> cr�er un stylo de dessin
    hdlPen = CreatePen(PS_SOLID, Temp2, 0)
    OldPen = SelectObject(Sortie.hdc, hdlPen)
    
    '-> Cr�er le fond
    hdlBrush = CreateSolidBrush(aObject.BackColor)
    oldBrush = SelectObject(Sortie.hdc, hdlBrush)
    
    '-> S�lectionner le rectangle comme zone de clipping
    ClipRgn = CreateRectRgn(aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
    SelectClipRgn Sortie.hdc, ClipRgn

    If aObject.IsRoundRect Then
        If Temp2 / 2 <> Fix(Temp2 / 2) Then Temp2 = Temp2 + 1
        
        Pix1X = frmLib.ScaleX(50, 3, 1)
        Pix1X = Sortie.ScaleX(Pix1X, 1, 3)

        Pix1Y = frmLib.ScaleY(50, 3, 1)
        Pix1Y = Sortie.ScaleY(Pix1Y, 1, 3)
        Res = RoundRect(Sortie.hdc, aRect.Left + Temp2 / 2, aRect.Top + Temp2 / 2, aRect.Right - Temp2 / 2, aRect.Bottom - Temp2 / 2, Pix1X, Pix1Y)
    Else
    
        '-> Bordure gauche
        If aObject.Gauche Then
            '-> Dessin des bordures
            Temp1 = Fix(Temp2 / 2)
            MoveToEx Sortie.hdc, aRect.Left + Temp1, aRect.Top, aPoint
            LineTo Sortie.hdc, aRect.Left + Temp1, aRect.Bottom - 1
        End If
        
        '-> Bordure haut
        If aObject.Haut Then
            Temp1 = Fix(Temp2 / 2)
            MoveToEx Sortie.hdc, aRect.Left, aRect.Top + Temp1, aPoint
            LineTo Sortie.hdc, aRect.Right, aRect.Top + Temp1
        End If
        
        '-> Bordure Droite
        If aObject.Droite Then
            If (Temp2 / 2) = Fix(Temp2 / 2) Then
                Temp1 = Temp2 / 2
            Else
                Temp1 = (Temp2 + 1) / 2
            End If
            MoveToEx Sortie.hdc, aRect.Right - Temp1, aRect.Top, aPoint
            LineTo Sortie.hdc, aRect.Right - Temp1, aRect.Bottom
        End If
        
        '-> Bordure Bas
        If aObject.Bas Then
            If (Temp2 / 2) = Fix(Temp2 / 2) Then
                Temp1 = Temp2 / 2
            Else
                Temp1 = (Temp2 + 1) / 2
            End If
            MoveToEx Sortie.hdc, aRect.Left, aRect.Bottom - Temp1, aPoint
            LineTo Sortie.hdc, aRect.Right, aRect.Bottom - Temp1
        End If
        
    End If
    
    '-> Restituer la zone de clipping
    OldClip = CreateRectRgn(0, 0, Sortie.ScaleWidth, Sortie.ScaleHeight)
    SelectClipRgn Sortie.hdc, OldClip
    DeleteObject ClipRgn
    DeleteObject OldClip
End If
 
'-> S�lectionner les anciens objets et supprimer les objets GDI non utilis�s
If OldPen <> 0 Then
    SelectObject Sortie.hdc, OldPen
    DeleteObject hdlPen
End If

If oldBrush <> 0 Then
    SelectObject Sortie.hdc, oldBrush
    DeleteObject hdlBrush
End If


End Sub

