VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "comct232.ocx"
Begin VB.Form frmPrint 
   Appearance      =   0  'Flat
   BackColor       =   &H80000004&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3240
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7395
   Icon            =   "frmPrint.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3240
   ScaleWidth      =   7395
   StartUpPosition =   2  'CenterScreen
   Begin VB.OptionButton Option4 
      Height          =   195
      Left            =   1840
      TabIndex        =   23
      Top             =   1440
      Width           =   1575
   End
   Begin VB.OptionButton Option1 
      Height          =   195
      Left            =   160
      TabIndex        =   22
      Top             =   1440
      Value           =   -1  'True
      Width           =   1575
   End
   Begin VB.CheckBox Check3 
      Alignment       =   1  'Right Justify
      Height          =   195
      Left            =   1920
      TabIndex        =   20
      Top             =   2690
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CheckBox Check2 
      Alignment       =   1  'Right Justify
      Height          =   195
      Left            =   1920
      TabIndex        =   19
      Top             =   2960
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CommandButton Command4 
      Height          =   375
      Left            =   120
      TabIndex        =   18
      Top             =   2760
      Width           =   1215
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   2400
      Top             =   1200
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Command3 
      Height          =   375
      Left            =   6120
      TabIndex        =   11
      Top             =   2760
      Width           =   1215
   End
   Begin VB.CommandButton Command2 
      Height          =   375
      Left            =   4800
      TabIndex        =   10
      Top             =   2760
      Width           =   1215
   End
   Begin VB.Frame Frame3 
      Height          =   1455
      Left            =   3840
      TabIndex        =   7
      Top             =   1080
      Width           =   3495
      Begin VB.CheckBox Check1 
         Alignment       =   1  'Right Justify
         Height          =   195
         Left            =   1320
         TabIndex        =   16
         Top             =   1200
         Width           =   2055
      End
      Begin ComCtl2.UpDown UpDown1 
         Height          =   300
         Left            =   2535
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   240
         Width           =   240
         _ExtentX        =   450
         _ExtentY        =   529
         _Version        =   327681
         BuddyControl    =   "Text3"
         BuddyDispid     =   196618
         OrigLeft        =   2520
         OrigTop         =   391
         OrigRight       =   2760
         OrigBottom      =   626
         Max             =   500
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.TextBox Text3 
         Height          =   300
         Left            =   1920
         TabIndex        =   8
         Text            =   "1"
         Top             =   240
         Width           =   615
      End
      Begin VB.Image Image2 
         Height          =   600
         Left            =   120
         Picture         =   "frmPrint.frx":3219
         Top             =   480
         Width           =   1620
      End
      Begin VB.Label Label4 
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   1695
      End
      Begin VB.Image Image1 
         Height          =   1800
         Left            =   120
         Picture         =   "frmPrint.frx":3DE3
         Top             =   480
         Visible         =   0   'False
         Width           =   1800
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1455
      Left            =   60
      TabIndex        =   2
      Top             =   1080
      Width           =   3675
      Begin VB.OptionButton Option5 
         Height          =   255
         Left            =   1800
         TabIndex        =   21
         Top             =   1080
         Visible         =   0   'False
         Width           =   1695
      End
      Begin VB.OptionButton Option3 
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   1080
         Width           =   1695
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   2760
         TabIndex        =   5
         Top             =   720
         Width           =   495
      End
      Begin VB.TextBox Text1 
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   1800
         TabIndex        =   4
         Top             =   720
         Width           =   495
      End
      Begin VB.OptionButton Option2 
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   720
         Width           =   1215
      End
      Begin VB.Label Label3 
         Height          =   255
         Left            =   2400
         TabIndex        =   15
         Top             =   720
         Width           =   375
      End
      Begin VB.Label Label2 
         Height          =   255
         Left            =   1440
         TabIndex        =   13
         Top             =   720
         Width           =   375
      End
   End
   Begin VB.Frame Frame1 
      Height          =   975
      Left            =   60
      TabIndex        =   0
      Top             =   3
      Width           =   7275
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Height          =   375
         Left            =   5950
         TabIndex        =   17
         Top             =   330
         Width           =   1215
      End
      Begin VB.ComboBox Combo1 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   720
         TabIndex        =   1
         Top             =   360
         Width           =   5155
      End
      Begin VB.Label Label1 
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   360
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Indique le fichier en cours de traitement
Public FichierName As String
'-> Indique la cl� du spool actuel
Public SpoolKey As String
Public IsGetPrinter As Boolean
Public IsSelectionPage As Boolean
Public aSpool As Spool

Private Const CCHDEVICENAME As Long = 32
Private Const CCHFORMNAME As Long = 32

Private Type DevMode
    dmDeviceName As String * CCHDEVICENAME
    dmSpecVersion As Integer
    dmDriverVersion As Integer
    dmSize As Integer
    dmDriverExtra As Integer
    dmFields As Long
    dmOrientation As Integer
    dmPaperSize As Integer
    dmPaperLength As Integer
    dmPaperWidth As Integer
    dmScale As Integer
    dmCopies As Integer
    dmDefaultSource As Integer
    dmPrintQuality As Integer
    dmColor As Integer
    dmDuplex As Integer
    dmYResolution As Integer
    dmTTOption As Integer
    dmCollate As Integer
    dmFormName As String * CCHFORMNAME
    dmUnusedPadding As Integer
    dmBitsPerPel As Integer
    dmPelsWidth As Long
    dmPelsHeight As Long
    dmDisplayFlags As Long
    dmDisplayFrequency As Long
End Type


Private Type PRINTER_DEFAULTS
    pDatatype As String
    pDevMode As DevMode
    DesiredAccess As Long
End Type

Private Const DM_PROMPT As Long = 4
Private Const DM_IN_PROMPT As Long = DM_PROMPT

Private Declare Function DocumentProperties Lib "winspool.drv" Alias "DocumentPropertiesA" (ByVal hwnd As Long, ByVal hPrinter As Long, ByVal pDeviceName As String, ByRef pDevModeOutput As DevMode, ByRef pDevModeInput As DevMode, ByVal fMode As Long) As Long
Private Declare Function OpenPrinter Lib "winspool.drv" Alias "OpenPrinterA" (ByVal pPrinterName As String, ByRef phPrinter As Long, ByRef pDefault As PRINTER_DEFAULTS) As Long


'Private Sub Command1_Click()
''-> affichage de la page de proprietes de l'imprimante
'Dim phPrinter As Long
'Dim pDefault As PRINTER_DEFAULTS
'Dim pDevModeIn As DEVMODE
'Dim pDevModeOut As DEVMODE
'
''-> On recupere un pointeur sur l'imprimante selectionn�e
'OpenPrinter Me.Combo1.Text, phPrinter, pDefault
'
''-> On affiche la page de proprietes
'DocumentProperties Me.hwnd, phPrinter, Me.Combo1.Text, pDevModeOut, pDevModeIn, DM_IN_PROMPT
'
'
'End Sub

Public Sub InitialisationGetPrint()

'---> Cette proc�dure initialise la liste des imprimantes

On Error Resume Next

Dim lpBuffer As String
Dim Res As Long
Dim aLb As Libelle
Dim i As Integer

'-> Pointer sur la classe libell�
Set aLb = Libelles("FRMPRINTVISU")

'-> Rajouter en entete la visualisation �cran 'MESSPROG
Me.Combo1.AddItem aLb.GetCaption(17), 0
IsGetPrinter = True

'-> Bloquer la nature � imprimer
Me.Option2.Enabled = False
Me.Option3.Enabled = False

'-> Bloquer les zones de saisie
Me.Text1.Enabled = False
Me.Text2.Enabled = False
Me.Label2.Enabled = False
Me.Label3.Enabled = False

'-> Chercher une variable d'environnement
lpBuffer = Space$(50)
Res = GetEnvironmentVariable("TURBODEFAULT", lpBuffer, Len(lpBuffer))
If Res = 0 Then
    '-> Si on doit s�lectionner l'imprimante par d�faut
    lpBuffer = GetIniString("PRINTER", "DEFAULT", TurboGraphIniFile, False)
    If UCase$(Trim(lpBuffer)) = "SCREEN" Then Me.Combo1.ListIndex = 0
Else
    If UCase$(Trim(Mid$(lpBuffer, 1, Res))) = "SCREEN" Then Me.Combo1.ListIndex = 0
End If
If Entry(2, Entry(1, Command$, "|"), "~") <> "" Then
    If UCase(Entry(2, Entry(1, Command$, "|"), "~")) = "SCREEN" Then Me.Combo1.ListIndex = 0
End If

'-> si on est en local=web est que l'on a une imprimante par defaut
If TurboGraphWebPrinter <> "" Then
    If TurboGraphWebPrinter = "PRINTER" Then TurboGraphWebPrinter = Printer.DeviceName
    For i = 1 To Me.Combo1.ListCount
        If Me.Combo1.List(i) = TurboGraphWebPrinter Then Me.Combo1.ListIndex = i
    Next
End If

'-> si on a specifier une imprimante dans le spool
If nPrinter <> "" Then
    For i = 1 To Me.Combo1.ListCount
        If Me.Combo1.List(i) = nPrinter Then Me.Combo1.ListIndex = i
    Next
End If

DoEvents
'-> ne pas afficher le bouton de mise en page si on vient d'un dealview
If IsGetPrinter Then Me.Command4.Visible = False

'-> si jamais on a une impression directe en local=web envoyer l'impression
If TurboGraphWebDirect = "1" Then
    Shell App.Path & "\TurboGraph.exe " & Me.Combo1.Text & "~DIRECT~1" & "|" & Entry(2, Command$, "|")
    End
End If
End Sub

Private Sub Command1_Click()
Dim p As New PropertiesDialog
p.PrinterName = Me.Combo1.Text
p.hwnd = Me.hwnd
p.ShowPrinterProperties
Set p = Nothing
End Sub

Private Sub Command2_Click()

'---> Renvoyer une valeur

On Error Resume Next

Dim aLb As Libelle

topDirect = True

'-> Pointer sur la classe libelle
Set aLb = Libelles("FRMPRINTVISU")

'-> V�rifier q'une imprimante soit saisie
If Me.Combo1.ListIndex = -1 Then
    MsgBox aLb.GetCaption(13), vbCritical + vbOKOnly, aLb.GetToolTip(13)
    Me.Combo1.SetFocus
    Exit Sub
End If

If Me.Option2.Value Then
    '-> si page mini s�lectionn�e v�rifier les zones
    If Trim(Me.Text1.Text) = "" Then
        MsgBox aLb.GetCaption(15), vbCritical + vbOKOnly, aLb.GetToolTip(15)
        Me.Text1.SetFocus
        Exit Sub
    End If
    
    '-> si page mini s�lectionn�e v�rifier les zones
    If Trim(Me.Text2.Text) = "" Then
        MsgBox aLb.GetCaption(16), vbCritical + vbOKOnly, aLb.GetToolTip(15)
        Me.Text2.SetFocus
        Exit Sub
    End If
End If

'-> V�rifier le nombre de copies
If Trim(Me.Text3.Text) = "" Then
    MsgBox aLb.GetCaption(14), vbCritical + vbOKOnly, aLb.GetToolTip(14)
    Me.Text3.SetFocus
    Exit Sub
End If

'-> Imprimante
If Me.Combo1.ListIndex = 0 And IsGetPrinter Then
    strRetour = "DEALVIEW"
Else
    strRetour = Me.Combo1.Text
End If

'-> Nature d'impression
If Me.Option1.Value Then
    If Me.Option2.Value Then
        strRetour = strRetour & "|" & 5
    Else
        strRetour = strRetour & "|" & 1
    End If
Else
    If Me.Option3.Value Then
        strRetour = strRetour & "|" & 3
    Else
        If Me.Option2.Value Then
            strRetour = strRetour & "|" & 6
        Else
            strRetour = strRetour & "|" & 4
        End If
    End If
End If

If Me.Option2.Value Then
    strRetour = strRetour & "�" & Me.Text1.Text & "�" & Me.Text2.Text
End If

'-> nombre de copies
strRetour = strRetour & "|" & Me.Text3.Text

'-> Teste si impression recto verso
If Me.Check1.Value = 1 Then
    strRetour = strRetour & "|RV"
Else
    strRetour = strRetour & "|NORV"
End If

'-> on regarde si on a des copies assembl�es
If copyAssemb Then
    strRetour = strRetour & "|A"
Else
    strRetour = strRetour & "|"
End If

'-> on regarde si on doit ^pas imprimer la page de garde
If Me.Check2.Value = 1 Then
    strRetour = strRetour & "|NOGARDE"
    noGarde = True
Else
    strRetour = strRetour & "|"
End If

'-> on regarde si on doit imprimer les pi�ces jointes
If Me.Check3.Value = 1 Then
    strRetour = strRetour & "|FILESJOINS"
    noGarde = True
Else
    strRetour = strRetour & "|"
End If

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Command3_Click()
    Unload Me
End Sub

Private Sub Command4_Click()
'---> Renvoyer une valeur

On Error Resume Next

Dim aLb As Libelle

'-> Pointer sur la classe libelle
Set aLb = Libelles("FRMPRINTVISU")

'-> V�rifier q'une imprimante soit saisie
If Me.Combo1.ListIndex = -1 Then
    MsgBox aLb.GetCaption(13), vbCritical + vbOKOnly, aLb.GetToolTip(13)
    Me.Combo1.SetFocus
    Exit Sub
End If

If Me.Option2.Value Then
    '-> si page mini s�lectionn�e v�rifier les zones
    If Trim(Me.Text1.Text) = "" Then
        MsgBox aLb.GetCaption(15), vbCritical + vbOKOnly, aLb.GetToolTip(15)
        Me.Text1.SetFocus
        Exit Sub
    End If
    
    '-> si page mini s�lectionn�e v�rifier les zones
    If Trim(Me.Text2.Text) = "" Then
        MsgBox aLb.GetCaption(16), vbCritical + vbOKOnly, aLb.GetToolTip(15)
        Me.Text2.SetFocus
        Exit Sub
    End If
End If

'-> V�rifier le nombre de copies
If Trim(Me.Text3.Text) = "" Then
    MsgBox aLb.GetCaption(14), vbCritical + vbOKOnly, aLb.GetToolTip(14)
    Me.Text3.SetFocus
    Exit Sub
End If

'-> Imprimante
If Me.Combo1.ListIndex = 0 And IsGetPrinter Then
    strRetour = "DEALVIEW"
Else
    strRetour = Me.Combo1.Text
End If

'-> Nature d'impression
If Me.Option1.Value Then
    strRetour = strRetour & "|" & 1
ElseIf Me.Option2.Value Then
    strRetour = strRetour & "|" & 2 & "�" & Me.Text1.Text & "�" & Me.Text2.Text
Else
    If Me.Option3.Value Then
        strRetour = strRetour & "|" & 3
    Else
        strRetour = strRetour & "|" & 4
    End If
End If

'-> nombre de copies
strRetour = strRetour & "|" & Me.Text3.Text

'-> Teste si impression recto verso
If Me.Check1.Value = 1 Then
    strRetour = strRetour & "|RVP"
Else
    strRetour = strRetour & "|NORVP"
End If

'-> on regarde si on a des copies assembl�es
If copyAssemb Then
    strRetour = strRetour & "|A"
Else
    strRetour = strRetour & "|"
End If

'-> on regarde si on doit ^pas imprimer la page de garde
If Me.Check2.Value = 1 Then
    strRetour = strRetour & "|NOGARDE"
    noGarde = True
Else
    strRetour = strRetour & "|"
End If

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Command2_Click

End Sub

Private Sub Form_Load()

Dim aPrint As Printer
Dim aLb As Libelle
Dim OldExStyle As Long
Dim i As Integer
Dim aFichier As Fichier

On Error Resume Next

topDirect = True

'-> pour certains cas sous citrix on a pas trouv� l'imprimante par ce biais
For i = 1 To UBound(listprinter)
    Me.Combo1.AddItem listprinter(i)
    If Trim(UCase(listprinter(i))) = Trim(UCase(Printer.DeviceName)) Then _
        Me.Combo1.ListIndex = Me.Combo1.ListCount - 1
Next

If Me.Combo1.ListCount = 0 Then
    '-> Charger la liste des imprimantes
    For Each aPrint In Printers
        Me.Combo1.AddItem aPrint.DeviceName
        If Trim(UCase(aPrint.DeviceName)) = Trim(UCase(Printer.DeviceName)) Then _
            Me.Combo1.ListIndex = Me.Combo1.ListCount - 1
    Next
End If

Trace ("Imprimante par defaut du poste : " & Printer.DeviceName)

'-> on ajoute l'imprimante turboPDF
Me.Combo1.AddItem "TurboPdf"

'-> Pointer vers la classe des libelles
Set aLb = Libelles("FRMPRINTVISU")

'-> Charger les libell�s
Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Frame2.Caption = aLb.GetCaption(3)
Me.Option1.Caption = aLb.GetCaption(4)
Me.Option2.Caption = aLb.GetCaption(5)
Me.Label2.Caption = aLb.GetCaption(6)
Me.Label3.Caption = aLb.GetCaption(7)
Me.Option3.Caption = aLb.GetCaption(8)
Me.Frame3.Caption = aLb.GetCaption(9)
Me.Label4.Caption = aLb.GetCaption(10)
Me.Command2.Caption = aLb.GetCaption(11)
Me.Command3.Cancel = True
Me.Command3.Caption = aLb.GetCaption(12)
Me.Command1.Caption = aLb.GetCaption(18)
Me.Command4.Caption = aLb.GetCaption(21)
Me.Check3.Caption = aLb.GetCaption(23)

Me.Check1.Caption = aLb.GetCaption(19)
Me.Option4.Caption = aLb.GetCaption(20)

If Not copyAssemb Then
    Image2.Visible = True
    Image1.Visible = False
Else
    Image1.Visible = True
    Image2.Visible = False
End If

'i = 1
'If Fichiers.Count = 1 Then
'    Set aFichier = Fichiers(1)
'    Do While i <= aFichier.Spools.Count
'        '-> dans le cas ou on a un spool avec que la garde on le remonte en haut
'        If aFichier.Spools(i).nbPage = 0 And aFichier.Spools(i).IsSelectionPage = True Then
'            IsSelectionPage = True
'        End If
'        i = i + 1
'    Loop
'End If
If isGarde Then IsSelectionPage = True

On Error Resume Next
Me.Check2.Caption = aLb.GetCaption(22)
If IsSelectionPage Then
    Me.Check2.Visible = True
    If GetIniString("PARAM", "GARDE", App.Path & "\Turbograph.ini", False) = "1" Or GetIniString("PARAM", "GARDE", App.Path & "\Turbograph.ini", False) = "" Then
        Me.Check2.Value = 1
    End If
End If

End Sub

Private Sub Image1_Click()
Image2.Visible = True
Image1.Visible = False
copyAssemb = False

End Sub

Private Sub Image2_Click()

Image2.Visible = False
Image1.Visible = True
copyAssemb = True

End Sub

Private Sub Option1_Click()

'Me.Text1.Enabled = False
'Me.Text2.Enabled = False
'Me.Label2.Enabled = False
'Me.Label3.Enabled = False
If Me.Option1.Value = True Then
    Me.Option3.Value = False
    Me.Option2.Value = False
End If

If isGarde Then
    Me.Check2.Visible = True
    If GetIniString("PARAM", "GARDE", App.Path & "\Turbograph.ini", False) = "1" Or GetIniString("PARAM", "GARDE", App.Path & "\Turbograph.ini", False) = "" Then
        Me.Check2.Value = 1
    End If
Else
    Me.Check2.Visible = False
    Me.Check2.Value = 0
End If

End Sub

Private Sub Option2_Click()

Me.Text1.Enabled = True
Me.Text2.Enabled = True
Me.Label2.Enabled = True
Me.Label3.Enabled = True

Me.Option1.Value = False

If Me.Option2.Value = True And Me.Option1.Value = False And Me.Option4.Value = False And Me.Option4.Visible = True Then
    Me.Option1.Value = True
End If

End Sub

Private Sub Option3_Click()

If Me.Option3.Value = True Then
    Me.Option1.Value = False
    Me.Option4.Value = False
End If
'Option1_Click

End Sub

Private Sub Option4_Click()

If Me.Option4.Value = True Then
    Me.Option3.Value = False
End If

On Error Resume Next

If aSpool.IsSelectionPage = True Then
    Me.Check2.Visible = True
    If GetIniString("PARAM", "GARDE", App.Path & "\Turbograph.ini", False) = "1" Or GetIniString("PARAM", "GARDE", App.Path & "\Turbograph.ini", False) = "" Then
        Me.Check2.Value = 1
    End If
Else
    Me.Check2.Visible = False
End If

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)

If KeyAscii < 48 Or KeyAscii > 57 Then
    If KeyAscii <> 8 Then KeyAscii = 0
End If
Option2.Value = True

End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)

If KeyAscii < 48 Or KeyAscii > 57 Then
    If KeyAscii <> 8 Then KeyAscii = 0
End If

End Sub

Private Sub Text3_KeyPress(KeyAscii As Integer)

If KeyAscii < 48 Or KeyAscii > 57 Then
    If KeyAscii <> 8 Then KeyAscii = 0
End If

End Sub

Private Sub UpDown1_Change()

Me.Text3.SelStart = 0
Me.Text3.SelLength = Len(Me.Text3.Text)
Me.Text3.SetFocus

End Sub
