VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QRMath"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---------------------------------------------------------------
' QRMath
'---------------------------------------------------------------
Option Explicit

Private QR_MATH_EXP_TABLE
Private QR_MATH_LOG_TABLE

Private Sub Class_Initialize()
    Dim i As Integer
    
    QR_G15 = ShiftLeft(1, 10) Or ShiftLeft(1, 8) Or ShiftLeft(1, 5) Or ShiftLeft(1, 4) Or ShiftLeft(1, 2) Or ShiftLeft(1, 1) Or ShiftLeft(1, 0)
    QR_G15_MASK = ShiftLeft(1, 14) Or ShiftLeft(1, 12) Or ShiftLeft(1, 10) Or ShiftLeft(1, 4) Or ShiftLeft(1, 1)
    QR_G18 = ShiftLeft(1, 12) Or ShiftLeft(1, 11) Or ShiftLeft(1, 10) Or ShiftLeft(1, 9) Or ShiftLeft(1, 8) Or ShiftLeft(1, 5) Or ShiftLeft(1, 2) Or ShiftLeft(1, 0)
   
    '-> on initialise la variable QR_MATH_EXP_TABLE
    QR_MATH_EXP_TABLE = createNumArray(256)
    For i = 0 To 7
        QR_MATH_EXP_TABLE(i) = ShiftLeft(1, i)
    Next
    For i = 8 To 255
        QR_MATH_EXP_TABLE(i) = QR_MATH_EXP_TABLE(i - 4) Xor QR_MATH_EXP_TABLE(i - 5) Xor QR_MATH_EXP_TABLE(i - 6) Xor QR_MATH_EXP_TABLE(i - 8)
    Next

    '-> on initialise la variable QR_MATH_LOG_TABLE
    QR_MATH_LOG_TABLE = createNumArray(256)
    For i = 0 To 254
        QR_MATH_LOG_TABLE(QR_MATH_EXP_TABLE(i)) = i
    Next
    
End Sub

Public Function createNumArray(ByVal length As Integer) As Integer()
    Dim num_array() As Integer
    Dim i As Integer
    
    ReDim num_array(length)
    For i = 0 To length - 1
        num_array(i) = 0
    Next
    createNumArray = num_array
End Function

Public Function glog(ByVal n)
    If n < 1 Then
        trigger_error "log(n)", E_USER_ERROR
    End If
    glog = QR_MATH_LOG_TABLE(n)
End Function

Public Function gexp(n)
    Do While n < 0
        n = n + 255
    Loop
    Do While n >= 256
        n = n - 255
    Loop
    gexp = QR_MATH_EXP_TABLE(n)
End Function


'-> Fonctions pour g�rer les operations binaires

Function ShiftRight(ByVal lngNumber As Long, ByVal intNumBits As Integer) As Long
'--------------
'BIT SHIFT RIGHT
'--------------

ShiftRight = lngNumber \ 2 ^ intNumBits 'note the integer division op

End Function


Function ShiftLeft(ByVal lngNumber As Long, ByVal intNumBits As Integer) As Long
'--------------
'BIT SHIFT LEFT
'--------------

ShiftLeft = lngNumber * 2 ^ intNumBits

End Function

