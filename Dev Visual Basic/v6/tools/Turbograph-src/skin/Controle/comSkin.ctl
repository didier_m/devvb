VERSION 5.00
Begin VB.UserControl comSkin 
   AutoRedraw      =   -1  'True
   ClientHeight    =   1590
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2130
   ScaleHeight     =   1590
   ScaleWidth      =   2130
   Begin VB.PictureBox PicTmp 
      Height          =   255
      Left            =   1800
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   0
      Top             =   480
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.Label lblText 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Texte"
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   270
      TabIndex        =   1
      Top             =   0
      Width           =   420
   End
   Begin VB.Image Img 
      Height          =   240
      Left            =   0
      Top             =   0
      Width           =   240
   End
End
Attribute VB_Name = "comSkin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'D�clarations d'�v�nements:
  Event Click()
  Event DblClick()
  Event KeyDown(KeyCode As Integer, Shift As Integer)
  Event KeyPress(KeyAscii As Integer)
  Event KeyUp(KeyCode As Integer, Shift As Integer)
  Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Event MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Event MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Event Change()
  
  Private Type Coo
    X      As String
    Y      As String
    Width  As String
    Height As String
  End Type

  Dim Info(1) As Coo
  
  Public MaxHeight  As Integer
  Public MaxWidth   As Integer
  
  Public lblColorUp     As String
  Public lblColorDown   As String
  Public lblGras        As Boolean
  
Private Sub Img_Click()
    RaiseEvent Click
End Sub

Private Sub Img_DblClick()
    RaiseEvent DblClick
End Sub

Private Sub Img_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseDown(Button, Shift, X, Y)
End Sub

Private Sub Img_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseMove(Button, Shift, X, Y)
End Sub

Private Sub Img_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseUp(Button, Shift, X, Y)
End Sub

Private Sub lblText_Click()
    RaiseEvent Click
End Sub

Private Sub lblText_DblClick()
    RaiseEvent Click
End Sub

Private Sub lblText_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Call UserControl_MouseDown(0, 0, 0, 0)
    RaiseEvent MouseDown(Button, Shift, X, Y)
End Sub

Private Sub lblText_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseMove(Button, Shift, X, Y)
End Sub

Private Sub lblText_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Call UserControl_MouseUp(0, 0, 0, 0)
    RaiseEvent MouseUp(Button, Shift, X, Y)
End Sub

Private Sub UserControl_Click()
    RaiseEvent Click
End Sub

Private Sub UserControl_DblClick()
    RaiseEvent Click
End Sub

Private Sub UserControl_Initialize()
    Img.Top = 10
    Img.Left = 50
End Sub

Private Sub UserControl_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    'UserControl.PaintPicture PicTmp.Picture, 0, 0, Info(0).Width, Info(0).Height, Info(0).X, Info(0).Y, Info(0).Width, Info(0).Height
    UserControl.PaintPicture PicTmp.Picture, 0, 0, 100, Info(0).Height, Info(0).X, Info(0).Y, 100, Info(0).Height
    UserControl.PaintPicture PicTmp.Picture, 100, 0, UserControl.Width - 200, Info(0).Height, Info(0).X + 100, Info(0).Y, 100, Info(0).Height
    UserControl.PaintPicture PicTmp.Picture, UserControl.Width - 100, 0, 100, Info(0).Height, Info(0).Width - 100, Info(0).Y, 100, Info(0).Height
    
    lblText.ForeColor = lblColorDown
    lblText.FontBold = lblGras
    
    RaiseEvent MouseDown(Button, Shift, X, Y)
End Sub

Private Sub UserControl_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseMove(Button, Shift, X, Y)
End Sub

Private Sub UserControl_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    'UserControl.PaintPicture PicTmp.Picture, 0, 0, Info(1).Width, Info(1).Height, Info(1).X, Info(1).Y, Info(1).Width, Info(1).Height
    UserControl.PaintPicture PicTmp.Picture, 0, 0, 100, Info(1).Height, Info(1).X, Info(1).Y, 100, Info(1).Height
    UserControl.PaintPicture PicTmp.Picture, 100, 0, UserControl.Width - 200, Info(1).Height, Info(1).X + 100, Info(1).Y, 100, Info(1).Height
    UserControl.PaintPicture PicTmp.Picture, UserControl.Width - 100, 0, 100, Info(1).Height, Info(1).Width - 100, Info(1).Y, 100, Info(1).Height
    
    lblText.ForeColor = lblColorUp
    lblText.FontBold = lblGras
    RaiseEvent MouseUp(Button, Shift, X, Y)
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    lblText.Caption = PropBag.ReadProperty("Text", "Texte")
    lblText.ForeColor = PropBag.ReadProperty("Fore_Color", &H0&)
    
    Set lblText.Font = PropBag.ReadProperty("Font", Ambient.Font)
    lblText.FontBold = PropBag.ReadProperty("FontBold", 0)
    lblText.FontItalic = PropBag.ReadProperty("FontItalic", 0)
    lblText.FontName = PropBag.ReadProperty("FontName", "")
    lblText.FontSize = PropBag.ReadProperty("FontSize", 0)
    lblText.FontStrikethru = PropBag.ReadProperty("FontStrikethru", 0)
    lblText.FontUnderline = PropBag.ReadProperty("FontUnderline", 0)
    Img.Picture = PropBag.ReadProperty("Picture", Nothing)
End Sub

Private Sub UserControl_Resize()
On Error Resume Next
 
    If MaxHeight <> 0 Then UserControl.Height = MaxHeight
    'If MaxWidth <> 0 Then UserControl.Width = MaxWidth
    
    lblText.Left = UserControl.Width / 2 - lblText.Width / 2
    lblText.Top = UserControl.Height / 2 - lblText.Height / 2
    
    Img.Top = UserControl.Height / 2 - lblText.Height / 2
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    Call PropBag.WriteProperty("Text", lblText.Caption, "Texte")
    Call PropBag.WriteProperty("Fore_Color", lblText.ForeColor, &H0&)
    
    Call PropBag.WriteProperty("Font", lblText.Font, Ambient.Font)
    Call PropBag.WriteProperty("FontBold", lblText.FontBold, 0)
    Call PropBag.WriteProperty("FontItalic", lblText.FontItalic, 0)
    Call PropBag.WriteProperty("FontName", lblText.FontName, "")
    Call PropBag.WriteProperty("FontSize", lblText.FontSize, 0)
    Call PropBag.WriteProperty("FontStrikethru", lblText.FontStrikethru, 0)
    Call PropBag.WriteProperty("FontUnderline", lblText.FontUnderline, 0)
    Call PropBag.WriteProperty("Picture", Img.Picture, Nothing)
End Sub

Public Property Get Picture() As StdPicture
    Set Picture = Img.Picture
End Property

Public Property Set Picture(New_Pic As StdPicture)
    Set Img.Picture = New_Pic
    PropertyChanged "Picture"
End Property

Public Property Get Text() As String
    Text = lblText.Caption
End Property

Public Property Let Text(ByVal Texte As String)
    lblText.Caption = Texte
    PropertyChanged "Text"
End Property

Public Property Get Fore_Color() As OLE_COLOR
     Fore_Color = lblText.ForeColor
End Property

Public Property Let Fore_Color(New_Color As OLE_COLOR)
    lblText.ForeColor = New_Color
    PropertyChanged "Fore_Color"
End Property

Public Property Get Font() As Font
Attribute Font.VB_Description = "Renvoie un objet Font."
Attribute Font.VB_UserMemId = -512
    Set Font = lblText.Font
End Property

Public Property Set Font(ByVal New_Font As Font)
    Set lblText.Font = New_Font
    PropertyChanged "Font"
End Property

Public Property Get FontBold() As Boolean
Attribute FontBold.VB_Description = "Renvoie ou d�finit les styles de police Gras."
    FontBold = lblText.FontBold
End Property

Public Property Let FontBold(ByVal New_FontBold As Boolean)
    lblText.FontBold() = New_FontBold
    PropertyChanged "FontBold"
End Property

Public Property Get FontItalic() As Boolean
Attribute FontItalic.VB_Description = "Renvoie ou d�finit les styles de police Italique."
    FontItalic = lblText.FontItalic
End Property

Public Property Let FontItalic(ByVal New_FontItalic As Boolean)
    lblText.FontItalic() = New_FontItalic
    PropertyChanged "FontItalic"
End Property

Public Property Get FontName() As String
Attribute FontName.VB_Description = "Indique le nom de la police qui appara�t � chaque ligne du niveau donn�."
    FontName = lblText.FontName
End Property

Public Property Let FontName(ByVal New_FontName As String)
    lblText.FontName() = New_FontName
    PropertyChanged "FontName"
End Property

Public Property Get FontSize() As Single
Attribute FontSize.VB_Description = "Indique la taille (en points) de la police qui appara�t � chaque ligne du niveau donn�."
    FontSize = lblText.FontSize
End Property

Public Property Let FontSize(ByVal New_FontSize As Single)
    lblText.FontSize() = New_FontSize
    PropertyChanged "FontSize"
End Property

Public Property Get FontStrikethru() As Boolean
Attribute FontStrikethru.VB_Description = "Renvoie ou d�finit les styles de police Barr�."
    FontStrikethru = lblText.FontStrikethru
End Property

Public Property Let FontStrikethru(ByVal New_FontStrikethru As Boolean)
    lblText.FontStrikethru() = New_FontStrikethru
    PropertyChanged "FontStrikethru"
End Property

Public Property Get FontUnderline() As Boolean
Attribute FontUnderline.VB_Description = "Renvoie ou d�finit les styles de police Soulign�."
    FontUnderline = lblText.FontUnderline
End Property

Public Property Let FontUnderline(ByVal New_FontUnderline As Boolean)
    lblText.FontUnderline() = New_FontUnderline
    PropertyChanged "FontUnderline"
End Property

Public Sub Pic1_Info(New_Info As String)
 Dim Tmp As String
 
    Info(0).X = Left(New_Info, InStr(1, New_Info, ",") - 1)
    Tmp = Mid(New_Info, InStr(1, New_Info, ",") + 1)
    
    Info(0).Y = Left(Tmp, InStr(1, Tmp, ",") - 1)
    Tmp = Mid(Tmp, InStr(1, Tmp, ",") + 1)
    
    Info(0).Width = Left(Tmp, InStr(1, Tmp, ",") - 1)
    Tmp = Mid(Tmp, InStr(1, Tmp, ",") + 1)
    
    Info(0).Height = Tmp
End Sub

Public Sub Pic2_Info(New_Info As String)
 Dim Tmp As String
 
    Info(1).X = Left(New_Info, InStr(1, New_Info, ",") - 1)
    Tmp = Mid(New_Info, InStr(1, New_Info, ",") + 1)
    
    Info(1).Y = Left(Tmp, InStr(1, Tmp, ",") - 1)
    Tmp = Mid(Tmp, InStr(1, Tmp, ",") + 1)
    
    Info(1).Width = Left(Tmp, InStr(1, Tmp, ",") - 1)
    Tmp = Mid(Tmp, InStr(1, Tmp, ",") + 1)
    
    Info(1).Height = Tmp
End Sub

Public Sub Make_Skin(Path_Image As String)
    UserControl.Height = MaxHeight
    'UserControl.Width = MaxWidth
    
    PicTmp.Picture = LoadPicture(Path_Image)
    Call UserControl_MouseUp(0, 0, 0, 0)
End Sub
