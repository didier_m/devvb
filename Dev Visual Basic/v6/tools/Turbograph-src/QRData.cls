VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QRData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---------------------------------------------------------------
' QRData
'---------------------------------------------------------------

Option Explicit

Public mode
Public data

Public Function QRData(modeB, dataB)
    mode = modeB
    data = dataB
End Function

Public Function getMode()
    getMode = mode
End Function

Public Function getData()
    getData = data
End Function

'-> renamed write to writeB
Public Function write_buffer(ByRef buffer)
    Dim data As String
    Dim i As Integer
    Dim aMode

    Select Case mode
        Case QR_MODE_8BIT_BYTE
            data = getData()
            For i = 0 To Len(data) - 1
                buffer.put_buffer Asc(Mid(data, i + 1, 1)), 8
            Next
        Case Else
            trigger_error "not implemented.", E_USER_ERROR
    End Select

End Function

Public Function getLengthInBits(typeB)
    If 1 <= typeB And typeB < 10 Then
        ' 1 - 9
        Select Case mode
            Case QR_MODE_NUMBER
                getLengthInBits = 10
            Case QR_MODE_ALPHA_NUM
                getLengthInBits = 9
            Case QR_MODE_8BIT_BYTE
                getLengthInBits = 8
            Case QR_MODE_KANJI
                getLengthInBits = 8
            Case Else
                trigger_error "mode:mode", E_USER_ERROR
        End Select

    ElseIf typeB < 27 Then
        ' 10 - 26
        Select Case mode
            Case QR_MODE_NUMBER
                getLengthInBits = 12
            Case QR_MODE_ALPHA_NUM
                getLengthInBits = 11
            Case QR_MODE_8BIT_BYTE
                getLengthInBits = 16
            Case QR_MODE_KANJI
                getLengthInBits = 10
            Case Else
                trigger_error "mode:mode", E_USER_ERROR
        End Select

    ElseIf typeB < 41 Then
        ' 27 - 40
        Select Case mode
            Case QR_MODE_NUMBER
                getLengthInBits = 14
            Case QR_MODE_ALPHA_NUM
                getLengthInBits = 13
            Case QR_MODE_8BIT_BYTE
                getLengthInBits = 16
            Case QR_MODE_KANJI
                getLengthInBits = 12
            Case Else
                trigger_error "mode:mode", E_USER_ERROR
        End Select

    Else
        trigger_error "mode:mode", E_USER_ERROR
    End If
End Function


Public Function getLength()
        Select Case mode
            Case QR_MODE_NUMBER
            Case QR_MODE_ALPHA_NUM
            Case QR_MODE_8BIT_BYTE
                getLength = Len(getData())
            Case QR_MODE_KANJI
            Case Else
                trigger_error "getLength:" & mode, E_USER_ERROR
        End Select
End Function

