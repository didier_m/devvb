VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QRKanji"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---------------------------------------------------------------
' QRKanji
'---------------------------------------------------------------
Option Explicit

Public aQRData As QRData

Public Function QRKanji(data)
    aQRData.QRData QR_MODE_KANJI, data
End Function

'renamed write to writeB
Public Function writeB(ByVal buffer)
    Dim data() As String
    Dim i As Integer
    Dim c As Integer
    
    data = aQRData.getData()
    i = 0
    Do While i + 1 < UBound(data)
        c = ShiftLeft((CLng("&Hff") & Asc(data(i))), 8) Or (CLng("&Hff") & Asc(data(i + 1)))
        If CLng("&H8140") <= c And c <= CLng("&H9FFC") Then
            c = c - CLng("&H8140")
        ElseIf CLng("&HE040") <= c And c <= CLng("&HEBBF") Then
            c = c - CLng("&HC140")
        Else
            trigger_error "illegal char at " & (i + 1) & "/c", E_USER_ERROR
        End If
        c = (ShiftRight(c, 8) & CLng("&Hff")) * CLng("&HC0") + (c & CLng("&Hff"))
        buffer.putB c, 13
        i = i + 2
    Loop
    
    If i < UBound(data) Then
        trigger_error "illegal char at " & (i + 1), E_USER_ERROR
    End If
End Function

Public Function getLength()
    getLength = Int(UBound(aQRData.getData()) / 2)
End Function
