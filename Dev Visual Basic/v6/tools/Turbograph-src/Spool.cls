VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Spool"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Classe servant � d�finir un spool � l'int�rieur du fichier

'-> Indique le num�ro du spool dans le fichier
Public Num_Spool As Integer

'-> Nom de la maquette ASCII pour les cles %%GUI%%
Public MaquetteASCII As String

'-> D�finition ASCII de la maquette � utiliser
Private pMaquette As String

'-> D�finition OBJET de la maquette
Public maquette As maquette

'-> Indique si le spool poss�de une page de s�lection
Public IsSelectionPage As Boolean

'-> Indique si on peut visualiser le fichier SPool ou non ( pb de lecture sur le chargement de la maquette
Public Ok As Boolean

'-> Nombre de pages dans la spool
Public nbPage As Long

'-> D�finition interne des pages : la page 0 �tant la page de s�lection
Private pPages() As String

'-> D�finition des pages de d�tail
Private pLigneDetails() As String

'-> Nombre de lignes de d�tail
Public NbLigDetail As Long

'-> Cl� d'acces du spool dans la collection
Public Key As String

'-> Pour affichage dans le treeview du Spool
Public SpoolText As String

'-> Pour affichage : Nom du fichier Rattach�
Public FileName As String

'-> pour affichage des pages en cours
Public CurrentPage As Long
Public LastPage As Long

'-> Tableau de gestion des erreurs sur chargement de la maquette
Private pErrors() As String
Public NbError  As Integer

'-> Tableau de gestion des erreurs par page
Private pPageErrors() As String

'-> Feuille de visu associ�e au spool
Public frmdisplay As frmDisplaySpool

Public frmPreview As frmPreview

'-> Titre de l'acc�s au d�tail
Public TitreAccesDetail As String
Private IsTitre As Boolean

'-> position de la page dans le fichier
Public PosFichier As Long

Public IsTexte As Boolean
Public TexteFileName As String

Public GardeRTF As String

Public Function AddLigneDetail(LigneDet As String)

'---> Cette proc�dure ajoute une ligne de d�tail dans le spool

'-> Gestion du titre
If Not IsTitre Then
    TitreAccesDetail = LigneDet
    IsTitre = True
    Exit Function
End If
    
'-> Elargir la matrice des lignes de d�tail
NbLigDetail = NbLigDetail + 1
ReDim Preserve pLigneDetails(NbLigDetail)

'-> Ajouter la d�finition
pLigneDetails(NbLigDetail) = LigneDet

End Function

Public Function GetLigDetail(Ligne As Long) As String
GetLigDetail = pLigneDetails(Ligne)
End Function

Public Function AddPage() As Long

'---> Cette fonction ajoute une page dans la matrice des pages et retourne le num de la page

'-> Elargir la matrice des pages
nbPage = nbPage + 1
ReDim Preserve pPages(nbPage)

'-> Elargir en m�me temps la matrice des erreurs par page
ReDim Preserve pPageErrors(nbPage)

AddPage = nbPage

End Function

Public Sub SetPage(ByVal PageNumber As Long, ByVal Ligne As String)

'---> Cette proc�dure stock une ligne dans la matrice des pages
If pPages(PageNumber) = "" Then
    pPages(PageNumber) = Ligne
Else
    pPages(PageNumber) = pPages(PageNumber) & Chr(0) & Ligne
End If

End Sub

Public Sub SetErrorPage(ByVal PageNumber As Long, ByVal Error As String)

If pPageErrors(PageNumber) = "" Then
    pPageErrors(PageNumber) = Error
Else
    pPageErrors(PageNumber) = pPageErrors(PageNumber) & Chr(0) & Error
End If

End Sub
Public Sub InitErrorPage(ByVal PageNumber As Long)

On Error Resume Next
pPageErrors(PageNumber) = ""

End Sub


Public Function GetPage(ByVal PageNumber As Long) As String
On Error Resume Next
'---> Cette fonction retourne le contenu d'une page
GetPage = pPages(PageNumber)

End Function

Public Sub SetMaq(ByVal LigneMaq As String)

'---> Cette proc�dure ajoute une ligne dans la d�finition de la maquette
If pMaquette = "" Then
    pMaquette = LigneMaq
Else
    pMaquette = pMaquette & Chr(0) & LigneMaq
End If
    
End Sub

Public Sub SetMaqAll(ByVal LigneMaq As String)

'---> Cette remplace une maquette (pour les formulaires)
pMaquette = LigneMaq
    
End Sub


Public Function GetMaq() As String

'---> Cette fonction retourne le descriptif ASCII de la maquette ( chr(0) )
GetMaq = pMaquette

End Function

Public Function SetErrorMaq(ByVal LibelError As String)

'---> Cette proc�dure incr�mente le comptage des erreurs

NbError = NbError + 1
ReDim Preserve pErrors(NbError)
pErrors(NbError) = LibelError

'-> Indiquer qu'il ya une erreur sur la lecture de la maquette
Ok = False

End Function

Public Function GetErrorMaq(ByVal nError As Integer) As String

'---> Cette prco�dure retourne une erreur
GetErrorMaq = pErrors(nError)

End Function

Private Sub Class_Initialize()

'-> Initialiser la matrice pour page 0 Si page de s�lection
ReDim pPages(0)

'-> Initialiser la matrice des erreurs
ReDim pErrors(0)

''-> Par d�faut page en cours = 1
'Me.CurrentPage = 1

End Sub

Public Sub InitDisplayPage()

'---> cette proc�dure intialise les dimensions de la page pour impression

'-> Ne rien faire s'il y a des erreurs fatales
If Me.NbError <> 0 Then Exit Sub
'-> masquer la feuille
Me.frmdisplay.Page.Visible = False
'-> effacer la feuille
Me.frmdisplay.Page.Cls
'-> Redimmensionner la page � la taille de la maquette
Me.frmdisplay.Page.width = Me.frmdisplay.ScaleX(maquette.Largeur, 7, 3) * Zoom
Me.frmdisplay.Page.height = Me.frmdisplay.ScaleY(maquette.Hauteur, 7, 3) * Zoom
'-> Intialiser des positions d'affichage
Me.frmdisplay.LargeurX = Me.frmdisplay.Page.width
Me.frmdisplay.MgX = Me.frmdisplay.ScaleX(0.1, 7, 3)
Me.frmdisplay.HauteurY = Me.frmdisplay.Page.height
Me.frmdisplay.MgY = Me.frmdisplay.ScaleY(0.1, 7, 3)
'-> Masquer les barres de d�filement
Me.frmdisplay.HScroll1.Visible = False
Me.frmdisplay.VScroll1.Visible = False
'-> Initialiser les valeurs de d�filement
Me.frmdisplay.HScroll1.SmallChange = Me.frmdisplay.ScaleX(1, 7, 3)
Me.frmdisplay.HScroll1.LargeChange = Me.frmdisplay.ScaleX(5, 7, 3)
Me.frmdisplay.VScroll1.SmallChange = Me.frmdisplay.ScaleY(1, 7, 3)
Me.frmdisplay.VScroll1.LargeChange = Me.frmdisplay.ScaleY(5, 7, 3)
'-> Masquer le picturebox d'angle
Me.frmdisplay.picAngle.Visible = False
'-> Masquer �galement le treeview
Me.frmdisplay.TreeView1.Visible = False

End Sub

Public Sub InitPreviewPage()

'---> cette proc�dure intialise les dimensions de la page pour impression

'-> Ne rien faire s'il y a des erreurs fatales
If Me.NbError <> 0 Then Exit Sub
'-> effacer la feuille
Me.frmPreview.Page.Cls
'-> Redimmensionner la page � la taille de la maquette
Me.frmPreview.Page.width = Me.frmPreview.ScaleX(maquette.Largeur, 7, 3) * Zoom
Me.frmPreview.Page.height = Me.frmPreview.ScaleY(maquette.Hauteur, 7, 3) * Zoom
'-> Intialiser des positions d'affichage
Me.frmPreview.LargeurX = Me.frmPreview.Page.width
Me.frmPreview.MgX = Me.frmPreview.ScaleX(0.1, 7, 3)
Me.frmPreview.HauteurY = Me.frmPreview.Page.height
Me.frmPreview.MgY = Me.frmPreview.ScaleY(0.1, 7, 3)
End Sub

Public Function GetErrorPage(ByVal PageToGet As Long) As String

'---> Cette fonction retoune les erreurs d'un pages
GetErrorPage = pPageErrors(PageToGet)

End Function

Public Sub DisplayInterfaceByPage(ByVal PageToDisplay As Long)

'---> Cette proc�dure est utilis�e uniquement en mode visu pour afficher la bonne repr�sentation de la page

Dim i As Integer

'-> Test sur l'impression des erreurs fatales
If Me.NbError <> 0 Then
    '-> Rendre le treeview visible
    Me.frmdisplay.TreeView1.Visible = True
    '-> Masquer la barre d'outils
    Me.frmdisplay.Toolbar1.Visible = False
    '-> Vider le treeveiew
    Me.frmdisplay.TreeView1.Nodes.Clear
    '-> Charger la liste des erreurs
    For i = 1 To NbError
        Me.frmdisplay.TreeView1.Nodes.add , , "ERR|" & i, Entry(i, pErrors(i), Chr(0)), "Warning"
    Next 'Pour toutes les erreurs
    '-> Rendre le RTF visible
    Me.frmdisplay.RtfError.Visible = True
    Exit Sub
End If

'-> Masquer le RTF
Me.frmdisplay.RtfError.Visible = False

If pPageErrors(PageToDisplay) <> "" Then
    '-> Rendre le treeview visible
    Me.frmdisplay.TreeView1.Visible = True
    '-> Masquer la barre d'outils
    Me.frmdisplay.Toolbar1.Visible = False
    '-> Vider le treeveiew
    Me.frmdisplay.TreeView1.Nodes.Clear
    '-> Charger la liste des erreurs
    For i = 1 To NumEntries(pPageErrors(PageToDisplay), Chr(0))
        Me.frmdisplay.TreeView1.Nodes.add , , "ERR|" & i, Entry(i, pPageErrors(PageToDisplay), Chr(0)), "Warning"
    Next 'Pour toutes les erreurs
Else
    '-> Masquer le treeview
    Me.frmdisplay.TreeView1.Visible = False
    '-> Afficher la barre d'outils
    Me.frmdisplay.Toolbar1.Visible = True
    '-> Positionner la page au bon endroit
    Me.frmdisplay.HScroll1.Value = Me.frmdisplay.HScroll1.Min
    Me.frmdisplay.VScroll1.Value = Me.frmdisplay.VScroll1.Min
    '-> Afficher la feuille
    Me.frmdisplay.Page.Visible = True
    If Me.frmdisplay.Toolbar1.Top = 0 Then
        Me.frmdisplay.Page.Top = 48
    Else
        Me.frmdisplay.Page.Top = 4
    End If
    '-> Forcer le resize de la feuille pour affichage et gestion des scrollBars
    Me.frmdisplay.Form_Resize
End If 'S'il y a des erreurs sur cette page

'-> afficher la page
Me.frmdisplay.WindowState = vbMaximized
Me.frmdisplay.Show


End Sub

Public Sub ErasePage(ByVal PageNumber As Long)

'---> Cette proc�dure supprime le contenu d'une page
pPages(PageNumber) = ""

End Sub


