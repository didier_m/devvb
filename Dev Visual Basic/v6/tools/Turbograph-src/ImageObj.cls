VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ImageObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'**********************************************
'*                                            *
'* Cette classe contient la d�finition d'un   *
'* Objet ImageBmp                             *
'*                                            *
'**********************************************


'---> Propri�t�s de l'objet
Public Nom As String
Public Fichier As String
Public Default As String
Public Primary As Boolean
Public Left As Single
Public Top As Single
Public Largeur As Single
Public Hauteur As Single
Public Contour As Boolean
Public IdAffichage As Integer
'-> index propre � l'objet pour la gestion des ordres d'affichage
Public IdOrdreAffichage As Integer
Public IsRedim As Boolean
Public Path As String

'---> Bmp Variable
Public isVariable As Boolean
Public UseAssociation As Boolean
Public IsAutosize As Boolean
Public fitSize As Boolean

'---> Nom de la section mere
Public SectionName As String

Public IdPic As Integer

Public oPicture As Object
