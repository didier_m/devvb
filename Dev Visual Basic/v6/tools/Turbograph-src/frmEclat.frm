VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form frmEclat 
   AutoRedraw      =   -1  'True
   Caption         =   "Eclatement"
   ClientHeight    =   6120
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8610
   Icon            =   "frmEclat.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   6120
   ScaleWidth      =   8610
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   9720
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEclat.frx":1CCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmEclat.frx":2064
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5895
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8415
      _ExtentX        =   14843
      _ExtentY        =   10398
      _Version        =   393216
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   2
      TabHeight       =   882
      WordWrap        =   0   'False
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "frmEclat.frx":23FE
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "ImageList2"
      Tab(0).Control(1)=   "Frame2"
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "frmEclat.frx":241A
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Frame1"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame1 
         Height          =   5055
         Left            =   240
         TabIndex        =   19
         Top             =   600
         Width           =   5535
         Begin VB.TextBox Text6 
            Height          =   285
            Left            =   2280
            TabIndex        =   26
            Top             =   4560
            Width           =   2295
         End
         Begin VB.CommandButton Command11 
            Caption         =   "..."
            Height          =   255
            Left            =   4680
            TabIndex        =   25
            Top             =   4575
            Width           =   255
         End
         Begin VB.PictureBox Picture1 
            AutoRedraw      =   -1  'True
            Height          =   255
            Left            =   2160
            ScaleHeight     =   195
            ScaleWidth      =   3075
            TabIndex        =   22
            Top             =   4200
            Width           =   3135
         End
         Begin VB.CommandButton Command2 
            Height          =   375
            Left            =   4200
            TabIndex        =   20
            Top             =   4560
            Width           =   1095
         End
         Begin MSComctlLib.TreeView TreeView1 
            Height          =   3255
            Left            =   240
            TabIndex        =   21
            Top             =   360
            Width           =   5055
            _ExtentX        =   8916
            _ExtentY        =   5741
            _Version        =   393217
            LabelEdit       =   1
            Style           =   7
            Checkboxes      =   -1  'True
            ImageList       =   "ImageList1"
            Appearance      =   1
         End
         Begin VB.Label Label32 
            BackStyle       =   0  'Transparent
            Caption         =   "R�pertoire de destination"
            Height          =   255
            Left            =   120
            TabIndex        =   27
            Top             =   4560
            Width           =   2175
         End
         Begin VB.Label Label8 
            BackColor       =   &H00C0C0C0&
            BackStyle       =   0  'Transparent
            Height          =   255
            Left            =   240
            TabIndex        =   24
            Top             =   4680
            Width           =   3855
         End
         Begin VB.Label Label12 
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   360
            TabIndex        =   23
            Top             =   4200
            Width           =   1695
         End
      End
      Begin VB.Frame Frame2 
         Height          =   5055
         Left            =   -74760
         TabIndex        =   1
         Top             =   600
         Width           =   5535
         Begin VB.PictureBox PicNothing 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   2760
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   14
            Top             =   960
            Visible         =   0   'False
            Width           =   735
            Begin VB.Image Image17 
               Height          =   720
               Left            =   0
               Picture         =   "frmEclat.frx":2436
               Top             =   -40
               Visible         =   0   'False
               Width           =   720
            End
         End
         Begin VB.PictureBox PicPdf 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   2760
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   13
            Top             =   1680
            Width           =   735
            Begin VB.Image Image16 
               Height          =   480
               Left            =   105
               Picture         =   "frmEclat.frx":3300
               Top             =   65
               Width           =   480
            End
         End
         Begin VB.Frame Frame4 
            Enabled         =   0   'False
            Height          =   1815
            Left            =   360
            TabIndex        =   8
            Top             =   2520
            Width           =   4455
            Begin VB.CheckBox Check2 
               Height          =   255
               Left            =   240
               TabIndex        =   12
               Top             =   1440
               Value           =   1  'Checked
               Width           =   4095
            End
            Begin VB.OptionButton Option4 
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   11
               Top             =   360
               Width           =   3735
            End
            Begin VB.OptionButton Option5 
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   10
               Top             =   720
               Value           =   -1  'True
               Width           =   3975
            End
            Begin VB.OptionButton Option6 
               CausesValidation=   0   'False
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   9
               Top             =   1080
               Width           =   2415
            End
         End
         Begin VB.PictureBox PicHtml 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   360
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   7
            Top             =   1680
            Width           =   735
            Begin VB.Image Image12 
               Height          =   480
               Left            =   120
               Picture         =   "frmEclat.frx":3BCA
               Top             =   80
               Width           =   480
            End
         End
         Begin VB.PictureBox picTurbo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   360
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   6
            Top             =   960
            Width           =   735
            Begin VB.Image Image3 
               Height          =   480
               Left            =   120
               Picture         =   "frmEclat.frx":4494
               Top             =   50
               Width           =   480
            End
         End
         Begin VB.CommandButton Command1 
            Caption         =   ">>"
            Height          =   375
            Left            =   4200
            TabIndex        =   5
            Top             =   4560
            Width           =   1095
         End
         Begin VB.Frame Frame6 
            Height          =   615
            Left            =   360
            TabIndex        =   2
            Top             =   4320
            Width           =   4455
            Begin VB.TextBox Text4 
               Enabled         =   0   'False
               Height          =   285
               Left            =   2160
               TabIndex        =   3
               Top             =   220
               Width           =   2175
            End
            Begin VB.Label Label11 
               Caption         =   "Nom du fichier pdf"
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   4
               Top             =   220
               Width           =   1575
            End
         End
         Begin VB.Label Label4 
            Height          =   615
            Left            =   3720
            TabIndex        =   18
            Top             =   1755
            Width           =   1215
         End
         Begin VB.Label Label3 
            Height          =   615
            Left            =   3720
            TabIndex        =   17
            Top             =   990
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.Shape Sel 
            BorderColor     =   &H000000FF&
            BorderWidth     =   2
            Height          =   660
            Left            =   345
            Top             =   945
            Width           =   780
         End
         Begin VB.Label Label2 
            Height          =   495
            Left            =   1320
            TabIndex        =   16
            Top             =   1755
            Width           =   1335
         End
         Begin VB.Label Label1 
            Height          =   615
            Left            =   1320
            TabIndex        =   15
            Top             =   990
            Width           =   1335
         End
      End
      Begin MSComctlLib.ImageList ImageList2 
         Left            =   -75000
         Top             =   120
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   2
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmEclat.frx":4D5E
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmEclat.frx":5638
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
End
Attribute VB_Name = "frmEclat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'-> Indique le fichier en cours de traitement
Public FichierName As String
Public aSpool As Spool

Private FormatHtml As Boolean
Private FormatPDF As Boolean
Private FormatNothing As Boolean
Private FormatTurbo As Boolean
Private pFormat As String

'-> Indique la cl� du spool actuel
Public SpoolKey As String
Public strSaisie As String

Dim TempFileName As String
Dim hdlFile As Integer
Dim ReponseFaite As Boolean
Dim iStep As Integer

Dim toto As String

Public Sub Init()

Dim aNode As Node
Dim aNode2 As Node
Dim aNode3 As Node
Dim aFichier As Fichier
Dim i As Long
Dim aLb As Libelle
Dim aSpool2 As Spool
Dim strTitre As String
Dim strPage As String
Dim topOK As Boolean

On Error Resume Next

'-> Pointer sur la gestion des messages
Set aLb = Libelles("MDIMAIN")

'-> Pointer sur l'objet Fichier
Set aFichier = Fichiers(aSpool.FileName)

'-> Cr�er une icone par spool
For Each aSpool2 In aFichier.Spools
    Set aNode2 = Me.TreeView1.Nodes.add(, , aSpool2.Key, aSpool2.SpoolText)
    aNode2.Tag = "SPOOL"
    aNode2.Expanded = True
    '-> Soit la liste des erreurs
    If aSpool.NbError = 0 Then
        '-> Afficher une icone par page
        For i = 1 To aSpool.nbPage
            '-> Recuperer eventuellement le titre de la feuille
            strTitre = GetTitreMail(aSpool2, i)
            strPage = GetTitrePage(aSpool2, i)
            If strPage = "" Then strPage = aLb.GetCaption(12) & i
            '-> si on est bien sur une page
            If strTitre <> "[NO]" Then
                strTitre = strTitre & "�����"
                topOK = True
                'If InStr(1, strTitre, "@") <> 0 Then
                    Set aNode3 = Me.TreeView1.Nodes.add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, strPage & " ~~ " & Entry(1, strTitre, "�"), 1)
                'Else
                '    Set aNode3 = Me.TreeView1.Nodes.add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, strPage & " ~~ " & strTitre, 2)
                'End If
                aNode3.Tag = "PAGE"
            End If
        Next
    Else
        '-> Afficher l'icone de la page d'erreur
        Set aNode3 = Me.TreeView1.Nodes.add(aNode2.Key, 4, aNode2.Key & "�ERROR", aLb.GetCaption(11), "Warning")
        aNode3.Tag = "ERROR"
    End If 'S'il y a des erreurs
Next 'Pour tous les spools

'-> si le spool n'a pas �t� configur� pour le mailing ou les eclatement
If topOK = False Then
    Set aLb = Libelles("FRMECLAT")
    MsgBox aLb.GetCaption(45), vbInformation, ""
    Unload Me
    Exit Sub
End If

End Sub



Private Sub Command1_Click()
'-> on donne le focus � l'onglet suivant
Me.SSTab1.Tab = 1
End Sub

Private Sub eclat()
'---> Eclater un spool

Dim SpoolName As String
Dim aNode As Node
Dim PageMin As Integer
Dim PageMax As Integer
Dim strMe As String

On Error Resume Next

'-> on initialise
strMe = Me.Caption
ListeHtmlFile = ""
Set frmLib.MAPIMessages1 = Nothing
Err.Description = ""
'on met le sablier
Screen.MousePointer = 11

'-> vider le picture de temporisation
Me.Picture1.Cls
TailleTotale = 0
'-> Calcul de la taille total
'-> on parcourt le treeview pour les mails � envoyer
For Each aNode In Me.TreeView1.Nodes
    '-> on on compte
    If aNode.Checked And aNode.Key <> "SPOOL" Then TailleTotale = TailleTotale + 1
Next

TailleLue = 0

'-> on parcourt le treeview pour eclater le spool
For Each aNode In Me.TreeView1.Nodes
    '-> on verifie que l'on est bien sur une page
    If aNode.Checked And aNode.Tag <> "SPOOL" Then
        '-> Ne mettre � jour les variables de traitement que si on vient de la visu et pas du menu
        '-> Cl� du spool et du fichier
        Set aSpool = Fichiers(Entry(1, aNode.Key, "�"))
        PageMin = CInt(Entry(2, Entry(2, aNode.Key, "�"), "|"))
        PageMax = CInt(Entry(2, Entry(2, aNode.Next.Key, "�"), "|")) - 1
        If PageMax < PageMin Then PageMax = aSpool.nbPage
        '-> nom du spool � traiter
        SpoolName = aSpool.FileName
        SpoolName = CreateDetail(aSpool, -2, PageMin, PageMax)
        
        '-> Format de la piece jointe
        If FormatTurbo Then pFormat = "0"
        If FormatHtml = True Then pFormat = "1"
        If FormatNothing Then pFormat = "3"
        If FormatPDF = True Then pFormat = "4"
        
        '-> on eclate le spool
        If Not eclatSpool(SpoolName, Trim(Entry(3, aNode.Text, "~"))) Then Exit For
        '-> Dessin de la temporisation
        TailleLue = TailleLue + 1
        DrawTempo Me.Picture1
        Me.Caption = strMe & "       " & TailleLue & "/" & TailleTotale
        Me.Refresh
        DoEvents
    End If
Next

'on met le sablier
Screen.MousePointer = 0
Me.SetFocus
If Err.Description <> "" Then MsgBox Err.Description, vbExclamation

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Command11_Click()

Dim strPath As String
'-> on lance le changement de repertoire
strPath = BrowseForFolder("Selectionnez le r�pertoire contenant les spools d'�dition", GetPath(Me.Text6.Text), Me.Text6.Text)
If strPath <> "" Then Me.Text6.Text = strPath & "\"

End Sub

Private Sub Command2_Click()
'-> envoyer les mails
eclat
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

Select Case KeyCode

Case vbKeyEscape
    'Unload Me
End Select

End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim PrinterFound As Boolean
Dim Printer As Printer

On Error Resume Next

'-> Gestion des messages
Set aLb = Libelles("FRMECLAT")

Me.Caption = aLb.GetCaption(36)
Me.SSTab1.TabCaption(0) = " " & aLb.GetCaption(39)
Me.SSTab1.TabCaption(1) = " " & aLb.GetCaption(40)

Me.Frame1.Caption = aLb.GetCaption(42)
Me.Frame2.Caption = aLb.GetCaption(41)
Me.Frame4.Caption = aLb.GetCaption(23)
Me.Label1.Caption = aLb.GetCaption(21)
Me.Label2.Caption = aLb.GetCaption(22)
Me.Label3.Caption = aLb.GetCaption(30)
Me.Label4.Caption = aLb.GetCaption(29)
Me.Label11.Caption = aLb.GetCaption(49)
Me.Option4.Caption = aLb.GetCaption(24)
Me.Option5.Caption = aLb.GetCaption(25)
Me.Option6.Caption = aLb.GetCaption(26)
Me.Check2.Caption = aLb.GetCaption(27)
Me.Command2.Caption = aLb.GetCaption(38)

'-> Charger les images
Set Me.SSTab1.TabPicture(0) = Me.ImageList2.ListImages(1).Picture
Set Me.SSTab1.TabPicture(1) = Me.ImageList2.ListImages(2).Picture

'-> S�lectionner le premier onglet
Me.SSTab1.Tab = 0

'-> par defaut on a le format turbo
FormatTurbo = True
FormatPDF = False
FormatHtml = False
FormatNothing = False

'-> pour la piece jointe par defaut
Select Case GetIniString("PARAM", "DEFAUTPJ", App.Path & "\Turbograph.ini", False)
    Case "HTML"
        Call PicHtml_Click
    Case "TURBO"
        Call picTurbo_Click
    Case "NONE"
        Call picNothing_Click
    Case "PDF"
        Call picPdf_Click
End Select

'-> repertoire de destination
Me.Text6.Text = TurbosavePath

'-> initialisation
Init

End Sub

Private Sub Form_Resize()
'--> on fait un resize sur la feuille
FormResize
End Sub

Private Sub FormResize()
'--> on retaille la feuille
On Error Resume Next

Me.SSTab1.Height = Me.Height - 630
Me.SSTab1.Width = Me.Width - 410
Me.Frame1.Width = Me.SSTab1.Width - 450
Me.Frame2.Width = Me.Frame1.Width
Me.Frame1.Height = Me.SSTab1.Height - 820
Me.Frame2.Height = Me.Frame1.Height
Me.TreeView1.Width = Me.Frame1.Width - 450
Me.TreeView1.Height = Me.SSTab1.Height - 2200
Me.Picture1.Top = Me.TreeView1.Top + Me.TreeView1.Height + 200
Me.Picture1.Left = Me.TreeView1.Left + 2500
Me.Picture1.Width = Me.TreeView1.Width - 2500
Me.Label12.Top = Me.Picture1.Top
Me.Label12.Width = 2400
Me.Command2.Left = Me.Frame1.Width - 1280
Me.Command1.Left = Me.Command2.Left
Me.Command2.Top = Me.Frame1.Height - 500
Me.Label32.Top = Me.Command2.Top
Me.Text6.Top = Me.Command2.Top
Me.Text6.Width = Me.Command2.Left - Me.Text6.Left - 500
Me.Command11.Top = Me.Command2.Top
Me.Command11.Left = Me.Text6.Left + Me.Text6.Width + 100
Me.Command1.Top = Me.Command2.Top
End Sub

Private Sub Image12_Click()
    PicHtml_Click
End Sub

Private Sub Image16_Click()
    picPdf_Click
End Sub

Private Sub Image17_Click()
    picNothing_Click
End Sub

Private Sub Image3_Click()
    picTurbo_Click
End Sub

Private Sub PicHtml_Click()

Me.Sel.Left = Me.PicHtml.Left - 20
Me.Sel.Top = Me.PicHtml.Top - 20
FormatHtml = True
FormatPDF = False
FormatNothing = False
FormatTurbo = False

'-> Bloquer la gestion Internet
Me.Frame6.Enabled = True
Me.Label11.Enabled = True
Me.Text4.Enabled = True
'Me.Text4.Text = ""
Me.Frame4.Enabled = True
Me.Option4.Enabled = True
Me.Option5.Enabled = True
Me.Option6.Enabled = True
Me.Check2.Enabled = True

End Sub

Private Sub picTurbo_Click()

Me.Sel.Left = Me.picTurbo.Left - 20
Me.Sel.Top = Me.picTurbo.Top - 20
FormatHtml = False
FormatPDF = False
FormatNothing = False
FormatTurbo = True

'-> Bloquer la gestion Internet
Me.Frame6.Enabled = True
Me.Label11.Enabled = True
Me.Text4.Enabled = True
'Me.Text4.Text = ""
Me.Frame4.Enabled = False
Me.Option4.Enabled = False
Me.Option5.Enabled = False
Me.Option6.Enabled = False
Me.Check2.Enabled = True

End Sub

Private Sub picPdf_Click()

Me.Sel.Left = Me.PicPdf.Left - 20
Me.Sel.Top = Me.PicPdf.Top - 20
FormatPDF = True
FormatHtml = False
FormatNothing = False
FormatTurbo = False

'-> Bloquer la gestion Internet
Me.Frame6.Enabled = True
Me.Label11.Enabled = True
Me.Text4.Enabled = True
'Me.Text4.SetFocus
Me.Frame4.Enabled = False
Me.Option4.Enabled = False
Me.Option5.Enabled = False
Me.Option6.Enabled = False
Me.Check2.Enabled = True

End Sub

Private Sub picNothing_Click()

Me.Sel.Left = Me.PicNothing.Left - 20
Me.Sel.Top = Me.PicNothing.Top - 20
FormatNothing = True
FormatPDF = False
FormatHtml = False
FormatTurbo = False

'-> Bloquer la gestion Internet
Me.Frame6.Enabled = True
Me.Label11.Enabled = True
Me.Text4.Enabled = True
'Me.Text4.Text = ""
Me.Frame4.Enabled = False
Me.Option4.Enabled = False
Me.Option5.Enabled = False
Me.Option6.Enabled = False
Me.Check2.Enabled = False
End Sub

Private Sub TreeView1_AfterLabelEdit(Cancel As Integer, NewString As String)
'-> on verifie pour info la validit� de la saisie
NewString = Entry(1, strSaisie, " ~~ ") & " ~~ " & NewString
'If InStr(1, NewString, "@") <> 0 Then
    Me.TreeView1.SelectedItem.Image = 1
'Else
'    Me.TreeView1.SelectedItem.Image = 2
'End If

End Sub

Private Sub TreeView1_DblClick()
    If Me.TreeView1.SelectedItem.Tag <> "SPOOL" Then
        '-> on se met � gauche la saisie
        strSaisie = Me.TreeView1.SelectedItem.Text
        Me.TreeView1.SelectedItem.Text = Mid(Entry(2, Me.TreeView1.SelectedItem.Text, " ~~ "), 4)
        TreeView1.StartLabelEdit
        Me.TreeView1.SelectedItem.Text = strSaisie
    End If
End Sub

Private Sub TreeView1_KeyPress(KeyAscii As Integer)
Dim aNode As Node

'On Error Resume Next
'-> on selectionne ou deselectionne tous les elements d'un node
'If KeyAscii = 13 Then
'    If Me.TreeView1.SelectedItem.Tag <> "LIG" Then Exit Sub
'    For Each aNode In Me.TreeView1.Nodes
'        '-> on inverse la selection
'        If aNode.Tag = "LIG" Then
'            If aNode.Parent.Text = Me.TreeView1.SelectedItem.Parent.Text Then
'                TreeView1_NodeClick aNode
'            End If
'        End If
'    Next
'End If

End Sub

Private Sub TreeView1_NodeCheck(ByVal Node As MSComctlLib.Node)
Dim aNode As Node

On Error Resume Next
'-> on selectionne ou deselectionne tous les elements d'un node
Set Me.TreeView1.SelectedItem = Node
If Me.TreeView1.SelectedItem.Tag <> "SPOOL" Then Exit Sub
For Each aNode In Me.TreeView1.Nodes
    '-> on inverse la selection
    If aNode.Tag = "PAGE" Then
        If aNode.Parent.Text = Me.TreeView1.SelectedItem.Text Then
            If aNode.Image = 1 Then aNode.Checked = Node.Checked
        End If
    End If
Next

End Sub

Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)

'---> Bloquer ou d�bloquer la gestion des prori�t�s

Dim aBlock As Block


End Sub

Private Function eclatSpool(ByVal FileToSend As String, strName As String) As Boolean
'--> cette fonction nous permet d'envoyer un mail
'--> par le composant winsock
Dim i As Integer
Dim j As Long
Dim m_strEncodedFiles As String
Dim l_destinataire
Dim destinataire
Dim sPieceJointe As String
Dim CurrentPIDProcess As Long
Dim sSMTP As String
Dim pdfName As String
Dim TurbosavePath2 As String

On Error Resume Next

'-> on se connecte au serveur
Label12.Caption = ""

'-> on charge le repertoire de destination
TurbosavePath2 = TurbosavePath

'-> on enregistre si besoin le repertoire destination
If Trim(Me.Text6.Text) <> "" Then
    If Dir$(Trim(Me.Text6.Text), vbDirectory) <> "" Then TurbosavePath2 = Trim(Me.Text6.Text)
End If

'-> Attacher le fichier sp�cifi�
Select Case pFormat
    Case "0" 'turbo
        'on copie le fichier spool
        If Trim(Me.Text4.Text) <> "" Then
            FileCopy FileToSend, TurbosavePath2 & Me.Text4.Text & "-" & GetTickCount & ".turbo"
        Else
            FileCopy FileToSend, TurbosavePath2 & strName & ".turbo"
        End If
        DoEvents
    Case "1" 'html
        '-> on transforme le spool en html
        '-> Inclure le num�ro de page
        IncluseSpoolNumber = CBool(Me.Check2.Value)
        '-> Positionnement de la navigationHTML
        If Me.Option4.Value Then
            NavigationType = 0 'FrameSet
        ElseIf Me.Option5.Value Then
            NavigationType = 1 'Fichier unique
        Else
            NavigationType = 2 'Pages
        End If
        PathToExport = GetTempFileNameVB("WWW", True)
        FileKeyMail = Trim(UCase$(FileToSend))
        '-> Nom du Fichier
        FileToExport = LCase$(GetSpoolName(FileKeyMail)) & ".html"
        '-> Cl� du fichiers
        FileKeyExport = FileKeyMail
        '-> Spool
        SpoolKeyMail = aSpool.Key
        SpoolKeyExport = SpoolKeyMail
        '-> Num�ro de page
        PageToExport = PageNumMail
        ExportType = 1
            
        '-> On ajoute le spool
        AnalyseFileToPrint FileToSend

        CreateEditionHTML
        '-> on copie les fichiers
        For i = 1 To NumEntries(ListeHtmlFile, "|")
            If InStr(1, Entry(i, ListeHtmlFile, "|"), ".html") <> 0 And Trim(Me.Text4.Text) <> "" Then
                FileCopy Entry(i, ListeHtmlFile, "|"), TurbosavePath2 & Trim(Me.Text4.Text) & "-" & GetTickCount & ".html"
            Else
                FileCopy Entry(i, ListeHtmlFile, "|"), TurbosavePath2 & GetFileName(Entry(i, ListeHtmlFile, "|"))
            End If
        Next
    Case "3" 'nothing
    
    Case "4" 'pdf
        '-> on g�n�re le fichier avec le turboPDF
        If FilePath = "" Then FilePath = OpenPath
        '-> on a specifi� eventuellement un nom pour le pdf
        If Trim(Me.Text4.Text) <> "" Then
            pdfName = "�pdfName=" & Me.Text4.Text
            pdfName = Replace(pdfName, ".pdf", "", , , vbTextCompare)
            pdfName = pdfName & GetTickCount
            pdfName = pdfName & ".pdf"
        Else
            '-> on prend le contenu de la zone
            pdfName = strName & ".pdf"
        End If
        Trace "Avant le TurboPDF ligne de commande ci-dessous"
        Label12.Caption = ""
        If TurbosavePath2 <> "" Then
            Trace App.Path & "\TurboPdf.exe " & Chr(34) & "�noShowSystray�fileToConvert=" & FileToSend & Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare) & "�pdfDirectory=" & TurbosavePath2 & "�pdfName=" & pdfName & Chr(34)
            CurrentPIDProcess = Shell(App.Path & "\TurboPdf.exe " & Chr(34) & "�pdfDirectory=" & TurbosavePath2 & "�pdfName=" & pdfName & "�noShowSystray�fileToConvert=" & FileToSend & Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare) & Chr(34))
        Else
            Trace App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & FileToSend & Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare) & "�pdfDirectory=" & Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1) & pdfName & Chr(34)
            CurrentPIDProcess = Shell(App.Path & "\TurboPdf.exe " & Chr(34) & "�pdfDirectory=" & Mid(FilePath, 1, InStrRev(Replace(FilePath, "\", "/"), "/") - 1) & "�pdfName=" & pdfName & "�noShowSystray�fileToConvert=" & FileToSend & Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare) & Chr(34))
        End If
        '-> Boucler tant que le processus est actif
        Do While IsPidRunning(CurrentPIDProcess)
            '-> Lib�ration de la pile des messages
            DoEvents
            Sleep 300
        Loop
        Trace "Apr�s le turbopdf"
End Select

Label12.Caption = ""

eclatSpool = True
End Function

