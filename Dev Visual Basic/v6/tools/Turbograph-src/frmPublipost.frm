VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "mscomctl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form frmPublipost 
   Caption         =   "Form1"
   ClientHeight    =   9480
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   16125
   Icon            =   "frmPublipost.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9480
   ScaleWidth      =   16125
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   15360
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPublipost.frx":1CFA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPublipost.frx":33E4
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   8895
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   15015
      _ExtentX        =   26485
      _ExtentY        =   15690
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   882
      WordWrap        =   0   'False
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "frmPublipost.frx":52CE
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "ImageList2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame3"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "frmPublipost.frx":52EA
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).Control(1)=   "Winsock1"
      Tab(1).ControlCount=   2
      Begin VB.Frame Frame3 
         Height          =   7575
         Left            =   840
         TabIndex        =   8
         Top             =   840
         Width           =   12735
         Begin VB.CommandButton Command6 
            Height          =   375
            Left            =   720
            Picture         =   "frmPublipost.frx":5306
            Style           =   1  'Graphical
            TabIndex        =   15
            Top             =   5760
            Width           =   495
         End
         Begin VB.CommandButton Command5 
            Height          =   375
            Left            =   1200
            Picture         =   "frmPublipost.frx":5890
            Style           =   1  'Graphical
            TabIndex        =   14
            Top             =   5760
            Width           =   375
         End
         Begin VB.CommandButton Command3 
            Caption         =   ">>"
            Height          =   375
            Left            =   11280
            TabIndex        =   13
            Top             =   6960
            Width           =   1095
         End
         Begin VB.PictureBox picTurbo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   9960
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   12
            Top             =   600
            Width           =   735
            Begin VB.Image Image1 
               Height          =   480
               Left            =   120
               Picture         =   "frmPublipost.frx":64F2
               Top             =   50
               Width           =   480
            End
         End
         Begin VB.PictureBox PicPdf 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   8760
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   11
            Top             =   600
            Width           =   735
            Begin VB.Image Image2 
               Height          =   480
               Left            =   120
               Picture         =   "frmPublipost.frx":1AC2B
               Top             =   50
               Width           =   480
            End
         End
         Begin VB.TextBox Text6 
            Height          =   285
            Left            =   2760
            TabIndex        =   10
            Top             =   600
            Width           =   5535
         End
         Begin VB.TextBox Text5 
            Height          =   285
            Left            =   2760
            TabIndex        =   9
            Top             =   975
            Width           =   5535
         End
         Begin MSComctlLib.TreeView TreeView2 
            Height          =   3975
            Left            =   480
            TabIndex        =   16
            Top             =   1560
            Width           =   8775
            _ExtentX        =   15478
            _ExtentY        =   7011
            _Version        =   393217
            LabelEdit       =   1
            Style           =   7
            ImageList       =   "ImageList1"
            Appearance      =   1
         End
         Begin VB.Shape Sel 
            BorderColor     =   &H000000FF&
            BorderWidth     =   2
            Height          =   660
            Left            =   8760
            Top             =   600
            Width           =   780
         End
         Begin VB.Label Label10 
            Caption         =   "Nom des fichiers � g�n�rer "
            Enabled         =   0   'False
            Height          =   255
            Left            =   600
            TabIndex        =   18
            Top             =   600
            Width           =   2175
         End
         Begin VB.Label Label4 
            Caption         =   "R�pertoire de destination"
            Enabled         =   0   'False
            Height          =   255
            Left            =   600
            TabIndex        =   17
            Top             =   960
            Width           =   2055
         End
      End
      Begin VB.Frame Frame1 
         Height          =   6255
         Left            =   -74760
         TabIndex        =   1
         Top             =   600
         Width           =   10695
         Begin VB.Timer Timer1 
            Enabled         =   0   'False
            Interval        =   10
            Left            =   0
            Top             =   0
         End
         Begin VB.PictureBox Picture1 
            AutoRedraw      =   -1  'True
            Height          =   255
            Left            =   4680
            ScaleHeight     =   195
            ScaleWidth      =   555
            TabIndex        =   4
            Top             =   4200
            Width           =   615
         End
         Begin VB.CommandButton Command2 
            Height          =   375
            Left            =   7440
            TabIndex        =   3
            Top             =   5400
            Width           =   1095
         End
         Begin VB.ComboBox Combo1 
            Height          =   315
            Left            =   720
            TabIndex        =   2
            Top             =   5400
            Visible         =   0   'False
            Width           =   2755
         End
         Begin MSComctlLib.TreeView TreeView1 
            Height          =   3255
            Left            =   240
            TabIndex        =   5
            Top             =   360
            Width           =   5055
            _ExtentX        =   8916
            _ExtentY        =   5741
            _Version        =   393217
            LabelEdit       =   1
            Style           =   7
            Checkboxes      =   -1  'True
            ImageList       =   "ImageList1"
            Appearance      =   1
         End
         Begin MSComctlLib.ImageList ImageList3 
            Left            =   3720
            Top             =   4200
            _ExtentX        =   1005
            _ExtentY        =   1005
            BackColor       =   -2147483643
            ImageWidth      =   32
            ImageHeight     =   32
            MaskColor       =   12632256
            _Version        =   393216
            BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
               NumListImages   =   7
               BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPublipost.frx":1E044
                  Key             =   ""
               EndProperty
               BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPublipost.frx":1E35E
                  Key             =   ""
               EndProperty
               BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPublipost.frx":1E678
                  Key             =   "LOTUS"
               EndProperty
               BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPublipost.frx":207B2
                  Key             =   "OUTLOOK"
               EndProperty
               BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPublipost.frx":2108C
                  Key             =   "WWW"
               EndProperty
               BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPublipost.frx":23F1E
                  Key             =   "PJ"
               EndProperty
               BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPublipost.frx":247F8
                  Key             =   "SMTP"
               EndProperty
            EndProperty
         End
         Begin VB.Label Label8 
            BackColor       =   &H00C0C0C0&
            BackStyle       =   0  'Transparent
            Height          =   255
            Left            =   240
            TabIndex        =   7
            Top             =   4680
            Width           =   3855
         End
         Begin VB.Label Label12 
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   360
            TabIndex        =   6
            Top             =   4200
            Width           =   1695
         End
      End
      Begin MSComctlLib.ImageList ImageList2 
         Left            =   240
         Top             =   600
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   5
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPublipost.frx":27FC2
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPublipost.frx":2889C
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPublipost.frx":29176
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPublipost.frx":29A50
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPublipost.frx":2A32A
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin MSWinsockLib.Winsock Winsock1 
         Left            =   -75000
         Top             =   0
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   393216
      End
   End
End
Attribute VB_Name = "frmPublipost"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'-> Indique le fichier en cours de traitement
Public FichierName As String
Public aSpool As Spool

Public FormatPDF As Boolean
Public FormatTurbo As Boolean
Private pFormat As String

'-> Indique la cl� du spool actuel
Public SpoolKey As String
Public strSaisie As String

Dim TempFileName As String
Dim hdlFile As Integer
Dim bRupture As Boolean

Public Sub Init()

Dim aNode As Node
Dim aNode2 As Node
Dim aNode3 As Node
Dim aNode4 As Node
Dim aNode5 As Node
Dim aFichier As Fichier
Dim i As Long
Dim j As Long
Dim aLb As Libelle
Dim aSpool2 As Spool
Dim strTitre As String
Dim strLastTitre As String
Dim strFirstTitre As String
Dim strPage As String
Dim topOK As Boolean
Dim strFichier As String

On Error Resume Next

'-> Pointer sur la gestion des messages
Set aLb = Libelles("MDIMAIN")
strLastTitre = ""
j = 0

Me.TreeView1.Nodes.Clear

For Each aFichier In Fichiers
        j = j + 1
        strFichier = GetUnzipFileName(aFichier.FileName)
        '-> Pointer sur l'objet fichier
        Set aFichier = Fichiers(UCase$(strFichier))
        Set aSpool = aFichier.Spools(1)

        '-> Cr�er une icone par spool
        For Each aSpool2 In aFichier.Spools
            Set aNode2 = Me.TreeView1.Nodes.add(, , aSpool2.Key & "|" & strFichier, aSpool2.SpoolText)
            If bRupture Then Set aNode4 = Me.TreeView2.Nodes.add(, , aSpool2.Key & "|" & strFichier, strFichier & " - " & aSpool2.SpoolText, 1)
            aNode2.Tag = "SPOOL"
            aNode2.Expanded = True
            aNode4.Tag = "SPOOL"
            aNode4.Expanded = True
            strLastTitre = ""
            If bRupture Then
                Set aNode5 = Me.TreeView2.Nodes.add(aNode4.Key, 4, aNode4.Key & "|RUPTURE", GetRupture(aSpool2), 1)
                aNode5.Tag = GetMaqName(aSpool2)
            End If
            '-> Soit la liste des erreurs
            If aSpool2.NbError = 0 Then
                '-> Afficher une icone par page
                For i = 1 To aSpool2.nbPage
                    '-> Recuperer eventuellement le titre de la feuille
                    strTitre = GetRuptureValue(aSpool2, Me.TreeView2.Nodes.Item(aNode2.Key & "|RUPTURE").Text, i)
                    If strFirstTitre = "" Then strFirstTitre = strTitre
                    strPage = GetTitrePage(aSpool2, i)
                    If strPage = "" Then strPage = aLb.GetCaption(12) & i
                    If Me.Combo1.ListIndex = 1 Then strLastTitre = strLastTitre & i
                    '-> si on est bien sur une page
                    If strTitre <> "[NO]" And strTitre <> strLastTitre Then
                        strLastTitre = strTitre
                        If Trim(strLastTitre) = "" Then strLastTitre = "wxw"
                        topOK = True
                        strTitre = strTitre & "�����"
                        'If InStr(1, strTitre, " ") = 0 Then
                            Set aNode3 = Me.TreeView1.Nodes.add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, strPage & " ~~ " & Entry(1, strTitre, "�"), 1)
                        'Else
                        '    Set aNode3 = Me.TreeView1.Nodes.add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, strPage & " ~~ " & Entry(1, strTitre, "�"), 2)
                        'End If
                        aNode3.Tag = "PAGE"
                    End If
                Next
            Else
                '-> Afficher l'icone de la page d'erreur
                Set aNode3 = Me.TreeView1.Nodes.add(aNode2.Key, 4, aNode2.Key & "�ERROR", aLb.GetCaption(11), "Warning")
                aNode3.Tag = "ERROR"
            End If 'S'il y a des erreurs
        Next 'Pour tous les spools
Next

VueCreate ("dispatch")

'-> si le spool n'a pas �t� configur� pour le mailing
If topOK = False Then
    Set aLb = Libelles("FRMPUBLIPOST")
    MsgBox aLb.GetCaption(45), vbInformation, "Publipostage"
    'Unload Me
    Exit Sub
Else
End If

'-> on ferme le composant
'Winsock1.Close

'-> on envoi directement
'SendOutLook = True

End Sub

Private Function GetRuptureValue(aSpool As Spool, strRupture As String, NumPage As Long) As String
'--> Cette fonction r�cup�re le titre d'une page
Dim ParamTitre As String
Dim Ligne As String
Dim Rg As String
Dim Dz As String
Dim i As Integer
Dim j As Integer
Dim topOK As Boolean
Dim topFIND As Boolean

'-> on regarde si on a specifi� un titre
'If InStr(1, aSpool.GetMaq, "[MAIL]") = 0 Then
'    GetRuptureValue = "[NO]"
'    Exit Function
'End If

'-> on r�cup�re le param�trage
ParamTitre = strRupture

topOK = False
topFIND = False

'-> on parcours les titres � r�cuperer
For i = 1 To NumEntries(ParamTitre, "\")
    Rg = Entry(i, ParamTitre, "\")
    Dz = "^" + Entry(1, Entry(2, Rg, "�"), Chr(0))
    Rg = "[" + Entry(1, Rg, "�") + "]"
    '-> on recherche la ligne correspondante
    For j = 1 To NumEntries(aSpool.GetPage(NumPage), Chr(0))
        Ligne = Trim(Entry(j, aSpool.GetPage(NumPage), Chr(0)))
        If InStr(1, UCase(Ligne), UCase(Rg)) Then
            '-> on regarde si on trouve le diez
            If InStr(1, UCase(Ligne), UCase(Rg)) <> 0 And InStr(1, Ligne, Dz) Then
                '-> on est sur une bonne ligne on r�cup�re la valeur du diez
                topOK = True
                topFIND = True
                Dz = RTrim(Mid(Entry(1, Entry(1, Entry(2, Ligne, Dz), "^"), "}"), 5))
                If InStr(1, Dz, "[\") <> 0 Then Dz = ""
                If Dz = "***" Then topOK = False
                If GetRuptureValue = "" Then
                    GetRuptureValue = Dz
                Else
                    GetRuptureValue = GetRuptureValue + "�" + Dz
                End If
                '-> sortir de la boucle
                Exit For
            End If
        End If
    Next
    If topFIND = True And Dz = "" And i = 1 Then GetRuptureValue = " "
    topFIND = False
Next
If topOK = False Then GetRuptureValue = "[NO]"
If Entry(1, GetRuptureValue, "�") = "***" Then GetRuptureValue = "[NO]"
End Function

Public Sub Regroup()

Dim aNode As Node
Dim aNode2 As Node
Dim aNode3 As Node
Dim aFichier As Fichier
Dim i As Long
Dim aLb As Libelle
Dim aSpool2 As Spool
Dim strTitre As String
Dim strLastTitre As String
Dim strPage As String
Dim topOK As Boolean

On Error Resume Next

Me.TreeView1.Nodes.Clear

'-> Pointer sur la gestion des messages
Set aLb = Libelles("MDIMAIN")
strLastTitre = ""
'-> Pointer sur l'objet Fichier
Set aFichier = Fichiers(aSpool.FileName)

'-> Cr�er une icone par spool
For Each aSpool2 In aFichier.Spools
    Set aNode2 = Me.TreeView1.Nodes.add(, , aSpool2.Key, aSpool2.SpoolText)
    aNode2.Tag = "SPOOL"
    aNode2.Expanded = True
    '-> Soit la liste des erreurs
    If aSpool.NbError = 0 Then
        '-> Afficher une icone par page
        For i = 1 To aSpool.nbPage
            '-> Recuperer eventuellement le titre de la feuille
            strTitre = GetTitreRupture(aSpool2, i)
            strPage = GetTitrePage(aSpool2, i)
            If strPage = "" Then strPage = aLb.GetCaption(12) & i
            If Me.Combo1.ListIndex = 0 Then strLastTitre = strLastTitre & i
            If Me.Combo1.ListIndex = 2 Then strTitre = strPage
            '-> si on est bien sur une page
            If strTitre <> "[NO]" And strTitre <> strLastTitre Then
                strLastTitre = strTitre
                If Trim(strLastTitre) = "" Then strLastTitre = "wxw"
                'If Me.Combo1.ListIndex = 2 Then strLastTitre = strPage
                strTitre = GetTitreRupture(aSpool2, i)
                topOK = True
                strTitre = strTitre & "�����"
                'If InStr(1, strTitre, "@") <> 0 And InStr(1, strTitre, " ") = 0 Then
                    Set aNode3 = Me.TreeView1.Nodes.add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, strPage & " ~~ " & Entry(1, strTitre, "�"), 1)
                'Else
                '    Set aNode3 = Me.TreeView1.Nodes.add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, strPage & " ~~ " & Entry(1, strTitre, "�"), 2)
                'End If
                aNode3.Tag = "PAGE"
            End If
        Next
    Else
        '-> Afficher l'icone de la page d'erreur
        Set aNode3 = Me.TreeView1.Nodes.add(aNode2.Key, 4, aNode2.Key & "�ERROR", aLb.GetCaption(11), "Warning")
        aNode3.Tag = "ERROR"
    End If 'S'il y a des erreurs
Next 'Pour tous les spools
End Sub

Private Sub Combo1_Change()
    Regroup
End Sub

Private Sub Combo1_Click()
    Regroup
End Sub

Private Sub Combo1_KeyDown(KeyCode As Integer, Shift As Integer)
    Regroup
End Sub

Private Sub Command1_Click()
'-> on donne le focus � l'onglet suivant
Me.SSTab1.Tab = 2
End Sub


Private Sub Command4_Click()
'-> on donne le focus � l'onglet suivant
Me.SSTab1.Tab = 2

End Sub

Private Sub Image1_Click()
    picTurbo_Click
End Sub

Private Sub Image2_Click()
    picPdf_Click
End Sub

Private Sub picPdf_Click()

Me.Sel.Left = Me.PicPdf.Left - 20
Me.Sel.Top = Me.PicPdf.Top - 20
FormatPDF = True
FormatTurbo = False

End Sub

Private Sub FileFusion(strFileList As String, strFile As String)
'--> cette proc�dure per met de concatener des spools
'--> cette proc�dure recherche du texte dans un spool
Dim hdlFile As Integer
Dim hdlFile2 As Integer
Dim Ligne As String
Dim sFile As String
Dim TempFile As String
Dim strTemp As String
Dim strFichier As String
Dim strFichierSave As String
Dim i As Integer
Dim strFilename As Object
Dim nbFile As Integer

'-------------> on determine le nom du nouveau spool
On Error GoTo GestError

'-> choisir le nouveau nom de fichier
'-> on ouvre le fichier � creer en mode write
hdlFile2 = FreeFile
Open strFile For Output As #hdlFile2

nbFile = NumEntries(strFileList, "|")
For i = 1 To nbFile
    '----------> procedure pour fusionner les documents
    strFichier = Entry(i, strFileList, "|")
    hdlFile = FreeFile
    '-> on ouvre le fichier en mode read
    Open strFichier For Input As #hdlFile
    '-> on parcourt les lignes pour y rechercher des propri�t�s
    Do While Not EOF(hdlFile)
        '-> Lecture de la ligne
        Line Input #hdlFile, Ligne
        '-> on ecrit la ligne
        Print #hdlFile2, Ligne
    Loop
    '-> on ferme le fichier
    Close #hdlFile
Next
'-> on ferme le fichier
Close #hdlFile2

GestError:

End Sub


Private Sub Command2_Click()
'-> envoyer les mails
generateRuptures
End Sub

Private Sub Command3_Click()
'-> on donne le focus � l'onglet suivant
Me.SSTab1.Tab = 1
End Sub

Private Sub Command5_Click()

'--> on lance l'enregistrement des vues
VueSave ("dispatch")

End Sub

Private Sub Command6_Click()

'--> on recharge une vue enregistree
'-> on passe le nom de la maquette pour filtrer dessus
VueLoad ("dispatch")

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

Select Case KeyCode

Case vbKeyEscape
    'Unload Me
End Select

End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim PrinterFound As Boolean
Dim Printer As Printer
Dim strFilename As String

'On Error Resume Next
'-> Gestion des messages
Set aLb = Libelles("FRMPUBLIPOST")
Me.Caption = aLb.GetCaption(36)
Me.SSTab1.TabCaption(0) = " " & aLb.GetCaption(39)
Me.SSTab1.TabCaption(1) = " " & aLb.GetCaption(37)
'Me.SSTab1.TabCaption(3) = " " & aLb.GetCaption(46)
'Me.SSTab1.TabVisible(1) = False
Me.Frame1.Caption = aLb.GetCaption(42)
Me.Frame3.Caption = aLb.GetCaption(43)
Me.Command2.Caption = aLb.GetCaption(38)
'Me.Check5.Caption = aLb.GetCaption(63)
'Me.Combo1.AddItem aLb.GetCaption(64), 0
'Me.Combo1.AddItem aLb.GetCaption(63), 1
'Me.Combo1.AddItem aLb.GetCaption(65), 2
'Me.Combo1.ListIndex = 0
'Me.Frame5.Enabled = True
'-> S�lectionner le premier onglet
Me.SSTab1.Tab = 0

'-> par defaut on a le format turbo
FormatTurbo = True
FormatPDF = False

picPdf_Click

Me.Text5.Text = TurbosavePath
strFilename = Mid(Fichiers(1).FileName, InStrRev(Replace(Fichiers(1).FileName, "\", "/"), "/") + 1)
strFilename = Replace(strFilename, ".turbo", "<rupture>.pdf")
Me.Text6.Text = strFilename
'-> initialisation
bRupture = True
Init
bRupture = False

VueLoad ("dispatch")

End Sub

Private Sub Form_Resize()
'--> on fait un resize sur la feuille
FormResize
End Sub

Private Sub FormResize()
'--> on retaille la feuille
On Error Resume Next

Me.SSTab1.height = Me.height - 750
Me.SSTab1.width = Me.width - 410
Me.Frame1.Top = 600
Me.Frame3.Top = Me.Frame1.Top
Me.Frame1.width = Me.SSTab1.width - 450
Me.Frame3.width = Me.Frame1.width
Me.Frame1.height = Me.SSTab1.height - 820
Me.Frame3.height = Me.Frame1.height
Me.Frame1.Left = 240
Me.Frame3.Left = Me.Frame1.Left
Me.Command3.Top = Me.Frame1.height - 500
Me.Command3.Left = Me.Frame1.width - 1500
Me.Command2.Left = Me.Command3.Left
Me.Command2.Top = Me.Command3.Top
Me.Command5.Top = Me.Command3.Top
Me.Command6.Top = Me.Command3.Top


Me.TreeView1.height = Me.SSTab1.height - 2700
Me.TreeView1.width = Me.Frame1.width - 450
Me.TreeView2.height = Me.SSTab1.height - 3100
Me.TreeView2.width = Me.TreeView1.width
Me.TreeView2.Left = Me.TreeView1.Left

Me.Combo1.Left = Me.TreeView1.Left
Me.Combo1.Top = Me.TreeView1.Top + Me.TreeView1.height + 150
Me.Picture1.Top = Me.Combo1.Top + 450
Me.Label12.Top = Me.Picture1.Top
Me.Label12.width = 2400
Me.Picture1.Left = Me.TreeView1.Left
Me.Picture1.width = Me.TreeView1.width - 0

End Sub

Private Sub picTurbo_Click()
Me.Sel.Left = Me.picTurbo.Left - 20
Me.Sel.Top = Me.picTurbo.Top - 20
FormatPDF = False
FormatTurbo = True

End Sub

Private Sub Timer1_Timer()
 Init
 Me.Timer1.Enabled = False
End Sub

Private Sub TreeView1_AfterLabelEdit(Cancel As Integer, NewString As String)
'-> on verifie pour info la validit� de la saisie
NewString = Entry(1, strSaisie, " ~~ ") & " ~~ " & NewString
'If InStr(1, NewString, "@") <> 0 Then
'    Me.TreeView1.SelectedItem.image = 1
'Else
'    Me.TreeView1.SelectedItem.image = 2
'End If

End Sub

Private Sub TreeView1_DblClick()
    If Me.TreeView1.SelectedItem.Tag <> "SPOOL" Then
        '-> on se met � gauche la saisie
        strSaisie = Me.TreeView1.SelectedItem.Text
        Me.TreeView1.SelectedItem.Text = Mid(Entry(2, Me.TreeView1.SelectedItem.Text, " ~~ "), 4)
        TreeView1.StartLabelEdit
        Me.TreeView1.SelectedItem.Text = strSaisie
    End If
End Sub

Private Sub TreeView1_KeyDown(KeyCode As Integer, Shift As Integer)
Dim aNode As Node

On Error Resume Next
'-> on selectionne ou deselectionne tous les elements d'un node
If KeyCode = vbKeyA And Shift = 2 Then
    For Each aNode In Me.TreeView1.Nodes
        '-> on inverse la selection
        If aNode.Tag = "SPOOL" Then
            aNode.Checked = True
            TreeView1_NodeCheck aNode
        End If
    Next
End If


End Sub

Private Sub TreeView1_KeyPress(KeyAscii As Integer)
Dim aNode As Node

'On Error Resume Next
'-> on selectionne ou deselectionne tous les elements d'un node
'If KeyAscii = 13 Then
'    If Me.TreeView1.SelectedItem.Tag <> "LIG" Then Exit Sub
'    For Each aNode In Me.TreeView1.Nodes
'        '-> on inverse la selection
'        If aNode.Tag = "LIG" Then
'            If aNode.Parent.Text = Me.TreeView1.SelectedItem.Parent.Text Then
'                TreeView1_NodeClick aNode
'            End If
'        End If
'    Next
'End If

End Sub

Private Sub TreeView1_NodeCheck(ByVal Node As MSComctlLib.Node)
Dim aNode As Node

On Error Resume Next
'-> on selectionne ou deselectionne tous les elements d'un node
Set Me.TreeView1.SelectedItem = Node
If Me.TreeView1.SelectedItem.Tag <> "SPOOL" Then Exit Sub
For Each aNode In Me.TreeView1.Nodes
    '-> on inverse la selection
    If aNode.Tag = "PAGE" Then
        If aNode.Parent.Key = Me.TreeView1.SelectedItem.Key Then
            If aNode.Image = 1 Then aNode.Checked = Node.Checked
        End If
    End If
Next

End Sub

Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)

'---> Bloquer ou d�bloquer la gestion des prori�t�s

Dim aBlock As Block


End Sub



Private Sub generateRuptures()
'--> cette proc�dure va permettre de generer des fichiers pdf ou turbo
'---> en fonctin des ruptures choisies

Dim SpoolName As String
Dim aNode As Node
Dim PageMin As Integer
Dim PageMax As Integer
Dim aToPrint As Boolean

Dim DeviceName As String
Dim ChoixPage As String
Dim NbCopies As Integer
Dim RectoVerso As String
Dim Assembl As String
Dim NoGard As String
Dim FileToPrint As String
Dim CurrentPIDProcess As Long
Dim aFichier As Fichier
Dim strFichier As String
Dim strRupture As String
Dim strRuptureList As String
Dim strRuptureFileList As String
Dim nbRupture As Integer
Dim i As Integer
Dim iTailleLue As Integer

On Error Resume Next

VueSave ("dispatch")
'-> on initialise
ListeHtmlFile = ""
Err.Description = ""
'on met le sablier
Screen.MousePointer = 11
'-> vider le picture de temporisation
iTailleLue = 0

'-> Vider la variable de retour
strRetour = ""
iTailleLue = 0
Me.Picture1.Cls

'-> on constitue la liste des ruptures
'-> on parcourt le treeview pour l eclatement/regroupement
For Each aNode In Me.TreeView1.Nodes
    '-> on verifie que l'on est bien sur la valeur de rupture courante
    If aNode.Checked And aNode.Tag <> "SPOOL" Then
        If InStr(1, strRuptureList, Trim(Entry(3, aNode.Text, "~"))) = 0 Then
            If strRuptureList = "" Then
                strRuptureList = Trim(Entry(3, aNode.Text, "~"))
            Else
                strRuptureList = strRuptureList & "|" & Trim(Entry(3, aNode.Text, "~"))
            End If
            nbRupture = nbRupture + 1
        End If
    End If
Next


For i = 1 To nbRupture
    
    strRupture = Entry(i, strRuptureList, "|")
    strRuptureFileList = ""
    '-> on parcourt le treeview pour l eclatement/regroupement
    For Each aNode In Me.TreeView1.Nodes
        If aNode.Tag = "SPOOL" Then
            strFichier = Entry(3, aNode.Key, "|")
            Set aFichier = Fichiers(UCase$(strFichier))
            Set aSpool = aFichier.Spools(1)
        End If
        '-> on verifie que l'on est bien sur la valeur de rupture courante
        If aNode.Checked And aNode.Tag <> "SPOOL" And Trim(Entry(3, aNode.Text, "~")) = strRupture Then
            '-> Ne mettre � jour les variables de traitement que si on vient de la visu et pas du menu
            '-> Cl� du spool et du fichier
            On Error Resume Next
            
            PageMin = CInt(Entry(2, Entry(2, aNode.Key, "�"), "|"))
            PageMax = CInt(Entry(2, Entry(2, aNode.Next.Key, "�"), "|")) - 1
            If PageMax < PageMin Then PageMax = aSpool.nbPage
            '-> nom du spool � traiter
            On Error GoTo GestError
            SpoolName = aSpool.FileName
            SpoolName = CreateDetail(aSpool, -2, PageMin, PageMax)
                    
            If strRuptureFileList = "" Then
                strRuptureFileList = SpoolName
            Else
                strRuptureFileList = strRuptureFileList & "|" & SpoolName
            End If
        End If
    Next

    strFichier = GetTempFileNameVB("turbo") & ".turbo"
    
    '-> on concatene les fichiers dans un meme fichier
    FileFusion strRuptureFileList, strFichier
        
    '-> Traiter les choix d'impression
    FileToPrint = strFichier
        
    '-> Lancer la generation des fichiers
    Dim aSpool2 As Spool
    AnalyseFileToPrint FileToPrint
    Set aSpool2 = Fichiers.Item(Trim(FileToPrint)).Spools(1)
    Fichiers.Remove (FileToPrint)
    Dim pdfDir As String
    Dim pdfName As String
    If GetTitrePdf(aSpool2, 1) <> "[NO]" Then
        pdfName = GetFileName(GetTitrePdf(aSpool2, 1))
        pdfDir = Replace(GetTitrePdf(aSpool2, 1), pdfName, "")
    Else
        pdfDir = Me.Text5.Text
        pdfName = Me.Text6.Text
        Dim output() As String
        output = Split(pdfName, "<")
        Dim ii As Integer
        For ii = 0 To UBound(output)
            Dim outputR() As String
            outputR = Split(output(ii), ">")
            Dim jj As Integer
            Dim strTemp As String
            For jj = 0 To UBound(outputR)
                If outputR(jj) <> "" And outputR(jj) <> "rupture" Then
                    strTemp = GetRuptureValue(aSpool2, outputR(jj), 1)
                    pdfName = Replace(pdfName, "<" & outputR(jj) & ">", strTemp)
                End If
            Next
        Next
    End If
        
    If Right(pdfDir, 1) <> "\" Then
        pdfDir = pdfDir & "\"
    End If
    MakeSureDirectoryPathExists pdfDir

    pdfName = Replace(pdfName, "<rupture>", strRupture)
    pdfDir = Replace(pdfDir, "<rupture>", strRupture)
    If FormatPDF Then
        CurrentPIDProcess = Shell(Chr(34) & App.Path & "\TurboPdf.exe" & Chr(34) & " " & Chr(34) & "�fileToConvert=" & FileToPrint & Replace(Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare), "�pdfDirectory=", "�NoPdfDirectory=", , , vbTextCompare) & "�pdfDirectory=" & pdfDir & "�pdfName=" & pdfName & Chr(34))
    Else
        CopyFile FileToPrint, pdfDir & Replace(pdfName, ".pdf", ".turbo"), False
    End If
    '-> Boucler tant que le processus est actif
    Do While IsPidRunning(CurrentPIDProcess)
        '-> Lib�ration de la pile des messages
        DoEvents
        Sleep 1000
    Loop
    
    '-> Dessin de la temporisation
    TailleTotale = nbRupture
    iTailleLue = iTailleLue + 1
    TailleLue = iTailleLue
    DrawTempo Me.Picture1
    Me.Refresh
    DoEvents


Next

GestError:
'on met le sablier
Screen.MousePointer = 0
Me.SetFocus
If Err.Description <> "" Then MsgBox Err.Description, vbExclamation

End Sub

Private Function VueSave(sName As String)
Dim aNode As Node
Dim sMaquette As String

'--> cette fonction permet d'enrtegistrer une vue
For Each aNode In Me.TreeView2.Nodes
    '-> on verifie que l'on est bien sur la valeur de rupture courante
    If aNode.Tag <> "SPOOL" Then
        SetIniString Trim(UCase(sName)), Trim(UCase(aNode.Tag)), App.Path & "\TurboDispatch.ini", aNode.Text
    End If
Next
    
SetIniString Trim(UCase(sName)), "PDFNAME-" & sName, App.Path & "\TurboDispatch.ini", Me.Text6.Text
SetIniString Trim(UCase(sName)), "PDFDIR", App.Path & "\TurboDispatch.ini", Me.Text5.Text
'SetIniString Trim(UCase(sName)), "RUPTURES", App.Path & "\TurboDispatch.ini", ""

End Function

Private Function VueLoad(sName As String)
Dim i As Integer
Dim aNode As Node
Dim aBlock As Block
Dim sMaquette As String
Dim sTrav As String

On Error Resume Next

'--> cette fonction permet de charger une vue enregistree
sTrav = GetIniString(Trim(UCase(sName)), "PDFNAME-" & sName, App.Path & "\TurboDispatch.ini", False)
Me.Text6.Text = sTrav
sTrav = GetIniString(Trim(UCase(sName)), "PDFDIR", App.Path & "\TurboDispatch.ini", False)
If sTrav <> "" Then Me.Text5.Text = sTrav

For Each aNode In Me.TreeView2.Nodes
    '-> on verifie que l'on est bien sur la valeur de rupture courante
    If aNode.Tag <> "SPOOL" Then
        If GetIniString(Trim(UCase(sName)), Trim(UCase(aNode.Tag)), App.Path & "\TurboDispatch.ini", False) <> "" Then
            aNode.Text = GetIniString(Trim(UCase(sName)), Trim(UCase(aNode.Tag)), App.Path & "\TurboDispatch.ini", False)
        End If
    End If
Next

End Function

Private Function VueCreate(sName As String)
Dim i As Integer
Dim aNode As Node
Dim aBlock As Block
Dim sMaquette As String
Dim sTrav As String

On Error Resume Next

For Each aNode In Me.TreeView2.Nodes
    '-> on verifie que l'on est bien sur la valeur de rupture courante
    If aNode.Tag <> "SPOOL" Then
        If GetIniString(Trim(UCase(sName)), Trim(UCase(aNode.Tag)), App.Path & "\TurboDispatch.ini", False) = "" Then
            SetIniString Trim(UCase(sName)), Trim(UCase(aNode.Tag)), App.Path & "\TurboDispatch.ini", aNode.Text
        End If
    End If
Next

End Function

Private Sub TreeView2_AfterLabelEdit(Cancel As Integer, NewString As String)
    Me.Timer1.Enabled = True
End Sub

Private Sub TreeView2_DblClick()
    If Me.TreeView2.SelectedItem.Tag <> "SPOOL" Then
        '-> on se met � gauche la saisie
        strSaisie = Me.TreeView2.SelectedItem.Text
        Me.TreeView2.SelectedItem.Text = Me.TreeView2.SelectedItem.Text
        TreeView2.StartLabelEdit
        Me.TreeView2.SelectedItem.Text = strSaisie
    End If
End Sub
