VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QR8BitByte"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---------------------------------------------------------------
' QR8BitByte
'---------------------------------------------------------------
Dim aQRData As QRData

Option Explicit

Public Function QR8BitByte(data) As QRData
    Set aQRData = New QRData
    aQRData.QRData QR_MODE_8BIT_BYTE, data
    Set QR8BitByte = aQRData
End Function

'-> before write renamed in writeB
Public Function writeB(ByRef buffer)
    Dim data() As String
    Dim i As Integer
    data = aQRData.getData()
    For i = 0 To UBound(data) - 1
        buffer.put Asc(data(i)), 8
    Next
End Function

Public Function getLength()
    getLength = Len(aQRData.getData())
End Function
