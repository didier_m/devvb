Attribute VB_Name = "datamatrix"
Option Explicit

Private apt As Variant
Private columns As Integer
Private lines As Integer
Private angle As Long
Private vcos As Long
Private vsin As Long
Private drw As Long
Private apic As PictureBox
Private x As Long
Private y As Long
Private x1 As Long
Private y1 As Long
Private x2 As Long
Private y2 As Long

'debug.Print drawdatamatrix("123456789012",300,300)
Public Function DrawDataMatrix(apic As PictureBox, strValue As String, width As Integer, height As Integer) As PictureBox
        
        x = 300
        y = 300
        apt = getDigit(strValue, False)
        'Set apic = frmLib.PicObj(0)
        apic.AutoSize = 1
        apic.AutoRedraw = True
        apic.ScaleMode = vbPixels
        apic.width = width
        apic.height = height
        apic.foreColor = &H80000008
        apic.Cls
        drw = apic.ScaleWidth / UBound(apt, 1)
        apic.DrawWidth = drw
        apic.DrawStyle = 1
        digitToRenderer 0, 0, 0, drw, drw, apt
        apic.Refresh
        apic.Picture = apic.image
        'SavePicture aPic.image, "test.bmp"
        DrawDataMatrix = apic.Picture
End Function

Private Function fn(points As Variant)
    apic.PSet (points(0) * drw, points(1) * drw), QBColor(0)
End Function

Private Function Deg2Rad(ByVal degrees As Single) As Single
    Deg2Rad = degrees / 57.29578
End Function

Private Function digitToRenderer(xi, yi, angle, mw, mh, digit)
    Dim x As Integer
    Dim y As Integer
    Dim z As Integer
    Dim xA As Integer
    Dim xB As Integer
    Dim xC As Integer
    Dim xD As Integer
    Dim yA As Integer
    Dim yB As Integer
    Dim yC As Integer
    Dim yD As Integer
    
    lines = UBound(digit)
    columns = UBound(digit, 1)
    angle = Deg2Rad(-angle)
    vcos = Cos(angle)
    vsin = sIn(angle)

    rotate columns * mw / 2, lines * mh / 2, vcos, vsin, x, y
    xi = xi - x
    yi = yi - y
    For y = 0 To lines - 1
        x = -1
        While (x < columns)
            x = x + 1
            If digit(y, x) = "1" Then
                z = x
                While (z + 1 < columns) And (digit(y, z + 1) = "1")
                    z = z + 1
                Wend
                x1 = x * mw
                y1 = y * mh
                x2 = (z + 1) * mw
                y2 = (y + 1) * mh
                rotate x1, y1, vcos, vsin, xA, yA
                rotate x2, y1, vcos, vsin, xB, yB
                rotate x2, y2, vcos, vsin, xC, yC
                rotate x1, y2, vcos, vsin, xD, yD
                fn (Array(xA + xi, yA + yi, xB + xi, yB + yi, xC + xi, yC + yi, xD + xi, yD + yi))
                x = z + 1
            End If
        Wend
    Next
    digitToRenderer = Result(xi, yi, columns, lines, mw, mh, vcos, vsin)
End Function

Private Function rotate(x1, y1, vcos, vsin, ByRef x, ByRef y)
        x = x1 * vcos - y1 * vsin
        y = x1 * vsin + y1 * vcos
Exit Function
    angle = Deg2Rad(-angle)
    vcos = Cos(angle)
    vsin = sIn(angle)
    x = x1 * vcos - y1 * vsin
    y = x1 * vsin + y1 * vcos
End Function

Private Function Result(xi, yi, columns, lines, mw, mh, vcos, vsin)
'       rotate 0, 0, vcos, vsin, x1, y1
'        rotate columns * mw, 0, vcos, vsin, x2, y2
'        rotate columns * mw, lines * mh, vcos, vsin, X3, Y3
'        rotate 0, lines * mh, vcos, vsin, x4, y4

'        return Array(
'            'width' => $columns * $mw,
'            'height'=> $lines * $mh,
'            'p1' => array(
'                'x' => $xi + $x1,
'                'y' => $yi + $y1
'            ),
'            'p2' => array(
'                'x' => $xi + $x2,
'                'y' => $yi + $y2
'            ),
'            'p3' => array(
'                'x' => $xi + $x3,
'                'y' => $yi + $y3
'            ),
'            'p4' => array(
'                'x' => $xi + $x4,
'                'y' => $yi + $y4
'            )
'        );
End Function

