VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "mscomctl.ocx"
Begin VB.Form frmNote 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H0080FFFF&
   BorderStyle     =   0  'None
   Caption         =   "dddd"
   ClientHeight    =   1950
   ClientLeft      =   6315
   ClientTop       =   4350
   ClientWidth     =   4260
   Icon            =   "frmNote.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   NegotiateMenus  =   0   'False
   ScaleHeight     =   1950
   ScaleWidth      =   4260
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.Slider Slider1 
      Height          =   615
      Left            =   1080
      TabIndex        =   2
      ToolTipText     =   "% d'opacit�"
      Top             =   30
      Visible         =   0   'False
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   1085
      _Version        =   393216
      LargeChange     =   10
      SmallChange     =   5
      Min             =   20
      Max             =   100
      SelStart        =   20
      TickFrequency   =   20
      Value           =   20
   End
   Begin VB.TextBox Text2 
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   120
      MaxLength       =   3000
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   600
      Width           =   3975
   End
   Begin VB.Image Image5 
      Height          =   240
      Left            =   2760
      Picture         =   "frmNote.frx":000C
      Top             =   120
      Width           =   240
   End
   Begin VB.Label Text1 
      BackColor       =   &H0080FFFF&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   120
      MousePointer    =   15  'Size All
      TabIndex        =   1
      Top             =   120
      Width           =   2775
   End
   Begin VB.Image Image4 
      Height          =   240
      Left            =   3120
      Picture         =   "frmNote.frx":0396
      Top             =   120
      Width           =   240
   End
   Begin VB.Image Image3 
      Height          =   240
      Left            =   3480
      Picture         =   "frmNote.frx":04E0
      Top             =   120
      Width           =   240
   End
   Begin VB.Image Image2 
      Height          =   240
      Left            =   3960
      MousePointer    =   8  'Size NW SE
      Picture         =   "frmNote.frx":062A
      Top             =   1680
      Width           =   240
   End
   Begin VB.Image Image1 
      Height          =   240
      Left            =   3840
      Picture         =   "frmNote.frx":0774
      Top             =   120
      Width           =   240
   End
   Begin VB.Line Line1 
      X1              =   0
      X2              =   4200
      Y1              =   480
      Y2              =   480
   End
End
Attribute VB_Name = "frmNote"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fPage As Form
Dim fParent As Form
Dim DepartX As Single
Dim DepartY As Single
Dim DepartZ As Single
Public valTransparence As Long
'Const pour la trensparence de la fen�tre
Private Const GWL_EXSTYLE = (-20)
Private Const WS_EX_LAYERED = &H80000
Private Const LWA_ALPHA = &H2
'-> Api pour la transparence
Private Declare Function SetLayeredWindowAttributes Lib "user32" (ByVal Hwnd As Long, ByVal crKey As Long, ByVal bAlpha As Byte, ByVal dwFlags As Long) As Long

Private Sub Form_Deactivate()
'-> on sauvegarde les notes
On Error Resume Next

If Fichiers(fParent.aSpool.FileName).FileNameZip <> Fichiers(fParent.aSpool.FileName).FileName Then
    noteSave Fichiers(fParent.aSpool.FileName).FileNameZip
Else
    noteSave fParent.aSpool.FileName
End If
End Sub

Public Sub Init(aForm As Form)
'--> procedure d'initialisation de la feuille
Set fParent = aForm
Me.Show , MDIMain
'-> on gere la transparence
If valTransparence = 0 Then valTransparence = 230
Transparence valTransparence, 10000
End Sub

Private Sub Form_Load()
Dim aLb As Libelle

'-> Pointer sur la classe libelles
Set aLb = Libelles("FRMNOTE")

Me.Image5.ToolTipText = aLb.GetCaption(2)
Me.Image4.ToolTipText = aLb.GetCaption(3)
Me.Image3.ToolTipText = aLb.GetCaption(4)
Me.Image1.ToolTipText = aLb.GetCaption(5)

End Sub

Private Sub Form_Resize()
'-> on redessine la feuille
Call FormResize

End Sub

Private Sub FormResize()
'--> cette procedure permet de redessiner la feuille
On Error Resume Next

Me.Text1.width = Me.width - 1280
Me.Text2.width = Me.width
Me.Text2.height = Me.height - Me.Image2.height - Me.Text2.Top
Me.Image2.Top = Me.height - Me.Image2.height
Me.Image2.Left = Me.width - Me.Image2.width
Me.Image3.Left = Me.width - 600
Me.Image4.Left = Me.width - 900
Me.Image5.Left = Me.width - 1200
Me.Slider1.Left = Me.width - 3000
Me.Image1.Left = Me.width - 300
Me.Line1.x2 = Me.width
End Sub

Private Sub Image1_Click()
'-> on ferme la feuille
Me.Visible = False
End Sub

Private Sub Image2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'-> on initialise les variables de depart
DepartX = x
DepartY = y

End Sub

Private Sub Image2_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
'-> on redimensionne
On Error Resume Next
If Button = 1 Then
    
    If Me.width + x - DepartX <= 2500 Or Me.height + y - DepartY <= 1000 Then
        If Me.width + x - DepartX <= 2500 Then Me.width = 2500
        If Me.height + y - DepartY <= 1200 Then Me.height = 1200
        Exit Sub
    End If
    Me.width = Me.width + x - DepartX
    Me.height = Me.height + y - DepartY
    FormResize
End If

End Sub

Private Sub Image3_Click()
'--> on supprime la note
noteDelete (Me.Hwnd)
End Sub

Private Sub Image4_Click()
'--> on peut changer la couleur de la note
Dim aPoint As POINTAPI
Dim Res As Long
strRetour = ""
Res = GetCursorPos(aPoint)
frmPalette.Top = Me.ScaleY(aPoint.y, 3, 1)
frmPalette.Left = Me.ScaleX(aPoint.x, 3, 1)
frmPalette.Couleur(2).Visible = False
strRetour = ""
frmPalette.Show vbModal
If strRetour <> "" Then
    Me.BackColor = CLng(strRetour)
    Me.Text1.BackColor = CLng(strRetour)
    Me.Text2.BackColor = CLng(strRetour)
End If

End Sub

Private Sub Image5_Click()
'--> on affiche le slider ppour gerer la transparence
Me.Slider1.Visible = True
End Sub

Private Sub Slider1_Change()
If Me.Visible = False Then Exit Sub
valTransparence = 2.55 * Slider1.Value
'-> on met a jour l'opacit�
Transparence valTransparence, 10000
Me.Slider1.Visible = False
End Sub

Private Sub Slider1_LostFocus()
'--> on masque le slider de la transparence
Me.Slider1.Visible = False
End Sub

Private Sub Slider1_Scroll()
valTransparence = 2.55 * Slider1.Value
'-> on met a jour l'opacit�
Transparence valTransparence, 10000

End Sub

Private Sub Text1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'-> on initialise les variables de depart
DepartX = x
DepartY = y

End Sub

Private Sub Text1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
'-> on deplace
Dim aRect As RECT
Dim aRect2 As RECT
Dim aRect3 As RECT
Dim Res As Long
On Error Resume Next
'-> Recup�rer la taille de la zone client
Res = GetClientRect(fParent.Hwnd, aRect)
Res = GetClientRect(fParent.Page.Hwnd, aRect2)
Res = GetClientRect(MDIMain.Hwnd, aRect3)
On Error Resume Next
If Button = 1 Then
    '-> on verifie que l'on est bien dans la feuille
'    If MDIMain.picSplit.Left + MDIMain.picSplit.Width > Me.Left + X - DepartX Then
'        Me.Left = MDIMain.picSplit.Left + MDIMain.picSplit.Width
'        Exit Sub
'    End If
    Me.Left = Me.Left + x - DepartX
    Me.Top = Me.Top + y - DepartY
End If

End Sub

Private Function Transparence(TranslucenceLevel As Long, Crk As Long) As Boolean
    Call SetWindowLong(Me.Hwnd, GWL_EXSTYLE, WS_EX_LAYERED)
    If Crk >= 0 Then
        Call SetLayeredWindowAttributes(Me.Hwnd, Crk, TranslucenceLevel, &H3)
    Else
        Call SetLayeredWindowAttributes(Me.Hwnd, Crk, TranslucenceLevel, LWA_ALPHA)
    End If
    
    Transparence = Err.LastDllError = 0
End Function

