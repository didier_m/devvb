Attribute VB_Name = "ApiWin32"
'Option Explicit
 
Private Const GWL_EXSTYLE = (-20)
Private Const WS_EX_CLIENTEDGE = &H200
Private Const WS_EX_STATICEDGE = &H20000
 
Private Const SWP_FRAMECHANGED = &H20 ' Frame Changed
Private Const SWP_NOMOVE = &H2
Private Const SWP_NOOWNERZORDER = &H200
Private Const SWP_NOZORDER = &H4
 
 
'-> S�parateur de millier : gestion des variables d'environnement
Public Const LOCALE_STHOUSAND = &HF
Public Const LOCALE_USER_DEFAULT& = &H400
Public Const LOCALE_SDECIMAL = 14
'-> Pour gestion des process m�moire
Public Const PROCESS_QUERY_INFORMATION = &H400
Public Const STILL_ACTIVE = &H103

'-> Structures pour GDI
Public Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Public Type POINTAPI
    X As Long
    Y As Long
End Type

'-> Structures pour gestion du code RTF
Public Type CHARRANGE
    cpMin As Long
    cpMax As Long
End Type

Public Type FORMATRANGE
    hdc As Long
    hdcTarget As Long
    rc As RECT
    rcPage As RECT
    chrg As CHARRANGE
End Type

'-> API pour fen�tre parcourir
Public Type BrowseInfo
    hwndOwner      As Long
    pIDLRoot       As Long
    pszDisplayName As Long
    lpszTitle      As Long
    ulFlags        As Long
    lpfnCallback   As Long
    lParam         As Long
    iImage         As Long
End Type

Public Const BIF_RETURNONLYFSDIRS = 1
Public Const BIF_DONTGOBELOWDOMAIN = 2
Public Declare Function SHBrowseForFolder Lib "shell32" (lpbi As BrowseInfo) As Long
Public Declare Function SHGetPathFromIDList Lib "shell32" (ByVal pidList As Long, ByVal lpBuffer As String) As Long
Public Declare Function lstrcat Lib "kernel32" Alias "lstrcatA" (ByVal lpString1 As String, ByVal lpString2 As String) As Long
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long

'-> API pour r�cup�ration des variables d'environnement
Public Declare Function GetEnvironmentVariable Lib "kernel32" Alias "GetEnvironmentVariableA" (ByVal lpName As String, ByVal lpBuffer As String, ByVal nSize As Long) As Long

'-> API de lecture des fichiers au format "*.ini"
Public Declare Function GetPrivateProfileSection& Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As Any, ByVal lpFileName As String)
Public Declare Function DeletePrivateProfileSection Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Long, ByVal lpString As Long, ByVal lpFileName As String) As Long


'-> API pour gestion d'une fen�tre
Public Declare Function GetClientRect& Lib "user32" (ByVal Hwnd As Long, lpRect As RECT)
Public Declare Function GetSystemMetrics& Lib "user32" (ByVal nIndex As Long)

'-> API pour gestion des regions
Public Declare Function CreateRectRgn& Lib "gdi32" (ByVal x1 As Long, ByVal y1 As Long, ByVal x2 As Long, ByVal y2 As Long)
Public Declare Function SelectClipRgn& Lib "gdi32" (ByVal hdc As Long, ByVal hRgn As Long)
Public Declare Function GetRgnBox& Lib "gdi32" (ByVal hRgn As Long, lpRect As RECT)
Public Declare Function GetClipRgn& Lib "gdi32" (ByVal hdc As Long, ByVal hRgn As Long)

'-> Api pour Dessin
Public Declare Function CreateSolidBrush& Lib "gdi32" (ByVal crColor As Long)
Public Declare Function CreatePen& Lib "gdi32" (ByVal nPenStyle As Long, ByVal nWidth As Long, ByVal crColor As Long)
Public Declare Function DeleteObject& Lib "gdi32" (ByVal hObject As Long)
Public Declare Function GetStockObject& Lib "gdi32" (ByVal nIndex As Long)
Public Declare Function SelectObject& Lib "gdi32" (ByVal hdc As Long, ByVal hObject As Long)
Public Declare Function Rectangle& Lib "gdi32" (ByVal hdc As Long, ByVal x1 As Long, ByVal y1 As Long, ByVal x2 As Long, ByVal y2 As Long)
Public Declare Function LineTo& Lib "gdi32" (ByVal hdc As Long, ByVal X As Long, ByVal Y As Long)
Public Declare Function DrawText& Lib "user32" Alias "DrawTextA" (ByVal hdc As Long, ByVal lpStr As String, ByVal nCount As Long, lpRect As RECT, ByVal wFormat As Long)
Public Declare Function DrawTextW Lib "user32" (ByVal hdc As Long, ByVal lpStr As Long, ByVal nCount As Long, lpRect As RECT, ByVal wFormat As Long) As Long
Public Declare Function FrameRect& Lib "user32" (ByVal hdc As Long, lpRect As RECT, ByVal hBrush As Long)
Public Declare Function GetDC Lib "user32" (ByVal Hwnd As Long) As Long
Public Declare Function ReleaseDC Lib "user32" (ByVal Hwnd As Long, ByVal hdc As Long) As Long

'-> Api pour gestion du Registre
Public Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
Public Declare Function RegOpenKey Lib "advapi32.dll" Alias "RegOpenKeyA" (ByVal hKey As Long, ByVal lpSubKey As String, phkResult As Long) As Long
Public Declare Function RegSetValueEx Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, ByVal dwType As Long, lpData As Any, ByVal cbData As Long) As Long
Public Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long

Public Const REG_SZ = 1
Public Const HKEY_CURRENT_USER = &H80000001
Public Const HKEY_LOCAL_MACHINE = &H80000002

'-> Pour envoie � la poubelle
Public Type SHFILEOPSTRUCT
   Hwnd        As Long
   wFunc       As Long
   pFrom       As String
   pTo         As String
   fFlags      As Integer
   fAborted    As Boolean
   hNameMaps   As Long
   sProgress   As String
 End Type
  
Public Const FO_DELETE = &H3
Public Const FOF_ALLOWUNDO = &H40
Public Declare Function SHFileOperation Lib "shell32.dll" Alias "SHFileOperationA" (lpFileOp As SHFILEOPSTRUCT) As Long

'-> Pour ouverture du site DEAL INFORMATIQUE
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal Hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

'-> Api gestion des fichiers tempo
Public Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Public Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long

'-> Api param�trage system
Public Const LOCALE_SYSTEM_DEFAULT& = &H800
Public Declare Function GetLocaleInfo& Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long)
Public Declare Function GetSystemDirectory& Lib "kernel32" Alias "GetSystemDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long)
Public Declare Function GetWindowsDirectory Lib "kernel32" Alias "GetWindowsDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long

'-> Api pour gestion des fichiers
Public Declare Function CloseHandle& Lib "kernel32" (ByVal hObject As Long)
Public Declare Function OpenFile& Lib "kernel32" (ByVal lpFileName As String, lpReOpenBuff As OFSTRUCT, ByVal wStyle As Long)
Public Declare Function GetFileSize& Lib "kernel32" (ByVal hFile As Long, lpFileSizeHigh As Long)
Public Declare Function CopyFile& Lib "kernel32" Alias "CopyFileA" (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal bFailIfExists As Long)

'-> Api pour gestion de l'impression au format RTF
Public Declare Function SendMessage& Lib "user32" Alias "SendMessageA" (ByVal Hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any)
Public Declare Function SendMessageBis& Lib "user32" Alias "SendMessageA" (ByVal Hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Long)
Public Declare Function SetMapMode& Lib "gdi32" (ByVal hdc As Long, ByVal nMapMode As Long)
Public Declare Function GetDeviceCaps Lib "gdi32" (ByVal hdc As Long, ByVal nIndex As Long) As Long
Public Declare Function MoveToEx Lib "gdi32" (ByVal hdc As Long, ByVal X As Long, ByVal Y As Long, lpPoint As Any) As Long
Public Declare Function RoundRect Lib "gdi32" (ByVal hdc As Long, ByVal x1 As Long, ByVal y1 As Long, ByVal x2 As Long, ByVal y2 As Long, ByVal X3 As Long, ByVal Y3 As Long) As Long

'-> API de temporisation
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

'-> API pour gestion des process
Public Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Public Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Long, lpExitCode As Long) As Long

'-> API pour le telechargement des images
Public Declare Function URLDownloadToFile Lib "urlmon" Alias "URLDownloadToFileA" (ByVal pCaller As Long, ByVal szURL As String, ByVal szFileName As String, ByVal dwReserved As Long, ByVal lpfnCB As Long) As Long

'-> API pour la version du framework
Public Declare Function LoadLibrary Lib "kernel32" Alias "LoadLibraryA" (ByVal lpLibFileName As String) As Long
Public Declare Function FreeLibrary Lib "kernel32" (ByVal hLibModule As Long) As Long
Public Declare Function GetCORVersion Lib "mscoree" (ByVal pbuffer As Long, ByVal cchBuffer As Long, dwlength As Long) As Long

'-> API pour avoir le repertoire mes documents
Private Type SHITEMID
    cb As Long
    abID As Byte
End Type
Private Type ITEMIDLIST
    mkid As SHITEMID
End Type

Public Declare Function SHGetSpecialFolderLocation Lib "shell32.dll" (ByVal hwndOwner As Long, ByVal nFolder As Long, pidl As ITEMIDLIST) As Long
Public Declare Function GetWindowLong Lib "user32.dll" Alias "GetWindowLongA" (ByVal Hwnd As Long, ByVal nIndex As Long) As Long
Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal Hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long

'Fonctions API
Public Declare Function SetWindowsHookEx Lib "user32" Alias "SetWindowsHookExA" (ByVal idHook As Long, ByVal lpfn As Long, ByVal hmod As Long, ByVal dwThreadId As Long) As Long
Public Declare Function UnhookWindowsHookEx Lib "user32" (ByVal hHook As Long) As Long
Public Declare Function RegisterWindowMessage Lib "user32" Alias "RegisterWindowMessageA" (ByVal lpString As String) As Long
Public Declare Function GetCurrentThreadId Lib "kernel32" () As Long
Public Declare Function CallNextHookEx Lib "user32" (ByVal hHook As Long, ByVal nCode As Long, ByVal wParam As Long, lParam As Any) As Long
Global hHook As Long

'-> Pour gestion des codes RTF et SendMessage
Public Const HORZRES& = 8
Public Const VERTRES& = 10
Public Const MM_TEXT& = 1
Public Const WM_GETTEXTLENGTH& = &HE
Public Const WM_USER& = &H400
Public Const WM_SETTEXT = &HC
Public Const EM_FORMATRANGE = WM_USER + 57
Private Const GWL_WNDPROC = (-4)

'-> Pour Marges internes du p�riph�rique
Public Const PHYSICALOFFSETX& = 112
Public Const PHYSICALOFFSETY& = 113
Public Const PHYSICALWIDTH& = 110
Public Const PHYSICALHEIGHT& = 111

'-> Pour objets GDI
Public Const GRAY_BRUSH& = 2
Public Const PS_SOLID& = 0
Public Const PS_NULL& = 5

'-> Constantes pour Message de Windows
Public Const WM_PAINT& = &HF
Public Const WM_SIZE = &H5

'-> Constantes pour DrawText
Public Const DT_SINGLELINE& = &H20
Public Const DT_TOP& = &H0
Public Const DT_VCENTER& = &H4
Public Const DT_BOTTOM& = &H8
Public Const DT_LEFT& = &H0
Public Const DT_CENTER& = &H1
Public Const DT_RIGHT& = &H2
Public Const DT_WORDBREAK& = &H10
Public Const DT_CALCRECT& = &H400
Public Const DT_NOPREFIX = &H800

'-> Pour caract�ristiques d'un DC
Public Const SM_CXEDGE& = 45

'-> Pour gestion des fichiers
Public Const OFS_MAXPATHNAME& = 128
Public Const OF_EXIST& = &H4000
Public Const OF_READ& = &H0


Public Type OFSTRUCT
    cBytes As Byte
    fFixedDisk As Byte
    nErrCode As Integer
    Reserved1 As Integer
    Reserved2 As Integer
    szPathName(OFS_MAXPATHNAME) As Byte
End Type


Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function ScreenToClient Lib "user32" (ByVal Hwnd As Long, lpPoint As POINTAPI) As Long
Public Declare Function MoveWindow Lib "User" (ByVal Hwnd As Integer, ByVal X As Integer, ByVal Y As Integer, ByVal nWidth As Integer, ByVal nHeight As Integer, ByVal bRepaint As Integer) As Integer

'-> API pour affichage d'une page de propri�t�s
Private Type SHELLEXECUTEINFO
             cbSize As Long
             fMask As Long
             Hwnd As Long
             lpVerb As String
             lpFile As String
             lpParameters As String
             lpDirectory As String
             nShow As Long
             hInstApp As Long
             ' Optional fields
             lpIDList As Long
             lpClass As String
             hkeyClass As Long
             dwHotKey As Long
             hIcon As Long
             hProcess As Long
   End Type
   
'---> Gestion de l'envoie de mail
Private Declare Function ShellExecuteEx Lib "shell32" (lpSEI As SHELLEXECUTEINFO) As Long
Private Const SEE_MASK_INVOKEIDLIST = &HC
Private Const SW_SHOW = 5

'-> API Pour gestion du temps
Public Declare Function GetTickCount& Lib "kernel32" ()

'---> Gestion du press papier
Public Const WM_CUT = &H300
Public Const WM_PASTE = &H302
Public Const WM_COPY = &H301

Public Declare Function OleTranslateColor Lib "oleaut32.dll" (ByVal _
    lOleColor As Long, ByVal lHPalette As Long, lColorRef As Long) As Long

Public Const LF_FACESIZE = 32
Public Const GWL_STYLE = (-16)
Public Const SCF_SELECTION = &H1&
Public Const SCF_ALL = &H4&

' pour recuperer les messages et text avec richtextbox
Public Const EM_SETCHARFORMAT = (WM_USER + 68)
Public Const EM_SETBKGNDCOLOR = (WM_USER + 67)
Public Const EM_GETCHARFORMAT = (WM_USER + 58)

' pour Font et BackColor
Public Const CFM_BACKCOLOR = &H4000000
Public Const CFE_AUTOBACKCOLOR = CFM_BACKCOLOR

'-> Api pour redimensionner un picturebox
Public Const ScrCopy = &HCC0020

Public Declare Function StretchBlt Lib "gdi32" (ByVal hdc As Long, ByVal X As Long, ByVal Y As Long, _
                                         ByVal nWidth As Long, ByVal nHeight As Long, _
                                         ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, _
                                         ByVal nSrcWidth As Long, ByVal nSrcHeight As Long, _
                                         ByVal dwRop As Long) As Long

'-> Api pour gerer la transparence
Public Declare Function SetBkMode Lib "gdi32" (ByVal hdc As Long, ByVal nBkMode As Long) As Long
Public Declare Function SetBkColor Lib "gdi32" (ByVal hdc As Long, ByVal crColor As Long) As Long

'-> Api pour trouver le programme associ�
Public Declare Function FindExecutable Lib "shell32.dll" Alias "FindExecutableA" (ByVal lpFile As String, ByVal lpDirectory As String, ByVal lpResult As String) As Long
Private Declare Function SetProp Lib "user32.dll" Alias "SetPropA" (ByVal Hwnd As Long, ByVal lpString As String, ByVal hData As Long) As Long

Public Type FILETIME
   dwLowDateTime As Long
   dwHighDateTime As Long
End Type

Public Type WIN32_FIND_DATA
   dwFileAttributes As Long
   ftCreationTime As FILETIME
   ftLastAccessTime As FILETIME
   ftLastWriteTime As FILETIME
   nFileSizeHigh As Long
   nFileSizeLow As Long
   dwReserved0 As Long
   dwReserved1 As Long
   cFileName As String * 260
   cAlternate As String * 14
End Type

Public Type FILE_PARAMS
   bRecurse As Boolean
   bFindOrExclude As Long
   nCount As Long
   nSearched As Long
   sFileNameExt As String
   sFileRoot As String
End Type

Public Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Public Type Msg
    Hwnd As Long
    message As Long
    wParam As Long
    lParam As Long
    time As Long
    pt As POINTAPI
End Type

Public Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long
   
Public Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
   
Public Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long

Public Declare Function lstrlen Lib "kernel32" Alias "lstrlenW" (ByVal lpString As Long) As Long

Public Declare Function PathMatchSpec Lib "shlwapi" Alias "PathMatchSpecW" (ByVal pszFileParam As Long, ByVal pszSpec As Long) As Long

Public fp As FILE_PARAMS
Public stopSearch As Boolean
Public Const vbBackslash = "\"
Public Const ALL_FILES = "*.*"

Public Const SWP_NOSIZE = 1
Public Const Flags = 1 Or 2
Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2
Public Const CSIDL_PERSONAL = &H5
'Conctantes de message Windows
Public Const WM_MOUSELAST = &H209
Public Const WM_MOUSEWHEEL = (WM_MOUSELAST + 1)
Public Const WH_GETMESSAGE = 3

Public Declare Function SetWindowPos Lib "user32" (ByVal Hwnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Public Declare Function CreateFile Lib "kernel32" Alias "CreateFileA" (ByVal lpFileName As String, ByVal dwDesiredAccess As Long, ByVal dwShareMode As Long, lpSecurityAttributes As Long, ByVal dwCreationDisposition As Long, ByVal dwFlagsAndAttributes As Long, ByVal hTemplateFile As Long) As Long
Public Declare Function CreateFileW Lib "kernel32" (ByVal lpFileName As Long, ByVal dwDesiredAccess As Long, ByVal dwShareMode As Long, ByVal lpSecurityAttributes As Long, ByVal dwCreationDisposition As Long, ByVal dwFlagsAndAttributes As Long, ByVal hTemplateFile As Long) As Long
Public Declare Function SetFileTime Lib "kernel32" (ByVal hFile As Long, lpcreation As FILETIME, lpLecture As FILETIME, lpLastWriteTime As FILETIME) As Long
Public Declare Function GetFileTime Lib "kernel32" (ByVal hFile As Long, lpCreationTime As FILETIME, lpLastAccessTime As FILETIME, lpLastWriteTime As FILETIME) As Long

Public Declare Function EnumPrintersA Lib "winspool.drv" (ByVal Flags As Long, ByVal Name As String, ByVal Level As Long, pPrinterEnum As Long, ByVal cdBuf As Long, pcbNeeded As Long, pcReturned As Long) As Long
Public Declare Function lstrlenA Lib "kernel32" (ByVal lpString As Any) As Long
Public Declare Function lstrcpyA Lib "kernel32" (ByVal lpString1 As String, ByVal lpString2 As Long) As Long
Public Declare Function OpenPrinter Lib "winspool.drv" Alias "OpenPrinterA" (ByVal pPrinterName As String, phPrinter As Long, pDefault As Any) As Long
Public Declare Function GetPrinter Lib "winspool.drv" Alias "GetPrinterA" (ByVal hPrinter As Long, ByVal Level As Long, buffer As Long, ByVal pbSize As Long, pbSizeNeeded As Long) As Long
Private Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" (ByVal lpPrevWndFunc As Long, ByVal Hwnd As Long, ByVal Msg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Private Declare Function RedrawWindow Lib "user32.dll" (ByVal Hwnd As Long, ByRef lprcUpdate As Any, ByVal hrgnUpdate As Long, ByVal fuRedraw As Long) As Long
Private Declare Function GetProp Lib "user32.dll" Alias "GetPropA" (ByVal Hwnd As Long, ByVal lpString As String) As Long

Private PrevProc As Long

Public Sub SaveString(hKey As Long, strPath As String, strValue As String, strData As String)

Dim hwndKey As Long

'-> Ouvertyre dans la cl� sp�cifi�e
RegOpenKey hKey, strPath, hwndKey

'-> Setting de la valeur
RegSetValueEx hwndKey, strValue, 0, REG_SZ, ByVal strData, Len(strData)

'-> fermer la cl�
RegCloseKey hwndKey

End Sub


Public Function RegQueryStringValue(ByVal hKey As Long, ByVal strValueName As String) As String
    
'---> R�cup�ration d'une valeur d'une cl� de registre
    
Dim lResult As Long
Dim lValueType As Long
Dim strBuf As String
Dim lDataBufSize As Long
Dim REG_BINARY As Long
lResult = RegQueryValueEx(hKey, strValueName, 0, lValueType, ByVal 0, lDataBufSize)
If lResult = 0 Then
    If lValueType = REG_SZ Then
        'Create a buffer
        strBuf = String(lDataBufSize, Chr$(0))
        'retrieve the key's content
        lResult = RegQueryValueEx(hKey, strValueName, 0, 0, ByVal strBuf, lDataBufSize)
        If lResult = 0 Then
            'Remove the unnecessary chr$(0)'s
            RegQueryStringValue = Left$(strBuf, InStr(1, strBuf, Chr$(0)) - 1)
        End If
    ElseIf lValueType = REG_BINARY Then
        Dim strData As Integer
        'retrieve the key's value
        lResult = RegQueryValueEx(hKey, strValueName, 0, 0, strData, lDataBufSize)
        If lResult = 0 Then
            RegQueryStringValue = strData
        End If
    End If
End If

End Function


Public Function GetRegKeyValue(ByVal root As Long, ByVal key_name As String, ByVal subkey_name As String) As String
Dim hKey As Long
Dim Value As String
Dim length As Long
Dim value_type As Long
Const ERROR_SUCCESS = 0&
' Retourne une cle du registre.
    ' ouvriri la cl�.
    If RegOpenKeyEx(root, key_name, _
        0&, &H1, hKey) <> ERROR_SUCCESS _
    Then
        Exit Function
    End If

    ' taille de la cl�.
    If RegQueryValueEx(hKey, subkey_name, _
        0&, value_type, ByVal 0&, length) _
            <> ERROR_SUCCESS _
    Then
    End If

    ' valeur de la cl�.
    Value = Space$(length)
    If RegQueryValueEx(hKey, subkey_name, _
        0&, value_type, ByVal Value, length) _
            <> ERROR_SUCCESS _
    Then
    Else
        ' nettoyer
        GetRegKeyValue = Left$(Value, length - 1)
    End If

    ' fermer.
    If RegCloseKey(hKey) <> ERROR_SUCCESS Then
    End If
End Function

Public Sub ShowFileProperties(ByVal ps_FileName As String)
   
         Dim lu_ShellExUDT As SHELLEXECUTEINFO
         
         With lu_ShellExUDT
             .Hwnd = 0 'hwnd
             .lpVerb = "properties"
             .lpFile = ps_FileName
             .fMask = SEE_MASK_INVOKEIDLIST
             .cbSize = Len(lu_ShellExUDT)
         End With
         
         Call ShellExecuteEx(lu_ShellExUDT)
   
   End Sub

Public Function GetSpecialfolder(CSIDL As Long) As String
'-> pour retrouver le repertoire mes documents
    Dim r As Long
    Dim IDL As ITEMIDLIST
    'Get the special folder
    r = SHGetSpecialFolderLocation(100, CSIDL, IDL)
    If r = NOERROR Then
        'Create a buffer
        Path$ = Space$(512)
        'Get the path from the IDList
        r = SHGetPathFromIDList(ByVal IDL.mkid.cb, ByVal Path$)
        'Remove the unnecessary chr$(0)'s
        GetSpecialfolder = Left$(Path, InStr(Path, Chr$(0)) - 1)
        Exit Function
    End If
    GetSpecialfolder = ""
End Function

Public Function FlatDesign(aForm As Form)
    Dim rtnVal As Long
    
    For Each aControl In aForm
        If TypeOf aControl Is CommandButton Or TypeOf aControl Is ComboBox Then
            rtnVal = GetWindowLong(aControl.Hwnd, GWL_EXSTYLE)
            rtnVal = rtnVal Or WS_EX_STATICEDGE And Not WS_EX_CLIENTEDGE
            SetWindowLong aControl.Hwnd, GWL_EXSTYLE, rtnVal
            SetWindowPos aControl.Hwnd, 0, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE Or _
                         SWP_NOOWNERZORDER Or SWP_NOZORDER Or SWP_FRAMECHANGED
        End If
        If TypeOf aControl Is OptionButton Or TypeOf aControl Is CheckBox Then
            'ThinBorder aControl.Hwnd, True
            aControl.Appearance = 0
            aControl.BackColor = &H8000000F
        End If
        If TypeOf aControl Is TextBox Then
            aControl.Appearance = 0
            SetProp aControl.Hwnd, "BorderColor", &HC0C0C0
            PrevProc = SetWindowLong(aControl.Hwnd, GWL_WNDPROC, AddressOf WindowProc2)
            SetProp aControl.Hwnd, "PrevProc", PrevProc
            RedrawWindow aControl.Hwnd, ByVal 0&, 0&, 1&
        End If
    Next
End Function

Public Function WindowProc2(ByVal Hwnd As Long, ByVal uMsg As Long, _
ByVal wParam As Long, ByVal lParam As Long) As Long
    
    Dim r As RECT
    Dim hPen As Long, OldPen As Long
    Dim lDC As Long
    Dim hBrush As Long, oldBrush As Long
    
    WindowProc2 = CallWindowProc(GetProp(Hwnd, "PrevProc"), Hwnd, uMsg, wParam, lParam)
    
    If uMsg = WM_PAINT Then
        lDC = GetDC(Hwnd)
        
        Call GetClientRect(Hwnd, r)
        hBrush = GetStockObject(NULL_BRUSH)
        hPen = CreatePen(PS_SOLID, 1, GetProp(Hwnd, "BorderColor"))
        
        OldPen = SelectObject(lDC, hPen)
        oldBrush = SelectObject(lDC, hBrush)
        Call Rectangle(lDC, r.Left, r.Top, r.Right, r.Bottom)
        Call SelectObject(lDC, oldBrush)
        Call DeleteObject(SelectObject(lDC, hPen))
        ReleaseDC Hwnd, lDC
    End If
    
End Function

