VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "mscomctl.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.MDIForm MDIMain 
   BackColor       =   &H8000000C&
   Caption         =   "MDIForm1"
   ClientHeight    =   10125
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   15960
   Icon            =   "MDIForm1.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   2  'CenterScreen
   Begin MSComDlg.CommonDialog OpenDlg 
      Left            =   4440
      Top             =   1080
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   4
      Top             =   9870
      Width           =   15960
      _ExtentX        =   28152
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   19738
            MinWidth        =   5292
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   5292
            MinWidth        =   5292
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            TextSave        =   "18:31"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picSplit 
      Align           =   3  'Align Left
      BorderStyle     =   0  'None
      Height          =   9870
      Left            =   2880
      MousePointer    =   9  'Size W E
      ScaleHeight     =   9870
      ScaleWidth      =   105
      TabIndex        =   3
      Top             =   0
      Width           =   105
      Begin VB.Line Line1 
         BorderColor     =   &H80000015&
         BorderWidth     =   3
         X1              =   50
         X2              =   50
         Y1              =   4560
         Y2              =   5340
      End
   End
   Begin VB.PictureBox picNaviga 
      Align           =   3  'Align Left
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   9870
      Left            =   0
      ScaleHeight     =   658
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   192
      TabIndex        =   0
      Top             =   0
      Width           =   2880
      Begin VB.Timer TimerOpenNode 
         Left            =   600
         Top             =   5520
      End
      Begin MSComctlLib.TreeView TreeNaviga 
         DragIcon        =   "MDIForm1.frx":381B
         Height          =   1455
         Left            =   240
         TabIndex        =   2
         Tag             =   "TreeNaviga"
         Top             =   1440
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   2566
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   397
         LabelEdit       =   1
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin VB.PictureBox picTitre 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         Height          =   90
         Left            =   240
         ScaleHeight     =   6
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   128
         TabIndex        =   1
         Top             =   840
         Visible         =   0   'False
         Width           =   1920
      End
      Begin VB.Image imgClose 
         Appearance      =   0  'Flat
         Height          =   165
         Left            =   2400
         Picture         =   "MDIForm1.frx":3965
         Top             =   240
         Width           =   165
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4920
      Top             =   1560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":3B33
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":5F15
            Key             =   "Close"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":9D4B
            Key             =   "Spool"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":DA63
            Key             =   "Page"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":FE45
            Key             =   "Warning"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":14BB7
            Key             =   "PageSelected"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   5520
      Top             =   2160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":16F99
            Key             =   "MAIL"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":17873
            Key             =   "MESSGAERIE"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFichier 
      Caption         =   ""
      Begin VB.Menu mnuLoadFile 
         Caption         =   ""
      End
      Begin VB.Menu mnuCloseFile3 
         Caption         =   ""
      End
      Begin VB.Menu mnuCloseFile2 
         Caption         =   ""
      End
      Begin VB.Menu mnuPrintAll 
         Caption         =   ""
      End
      Begin VB.Menu mnuGestFile 
         Caption         =   ""
      End
      Begin VB.Menu mnuListen 
         Caption         =   ""
         Begin VB.Menu mnuListenOption 
            Caption         =   "Notification"
            Index           =   1
         End
         Begin VB.Menu mnuListenOption 
            Caption         =   "Ouverture"
            Index           =   2
         End
         Begin VB.Menu mnuListenOption 
            Caption         =   "Impression"
            Index           =   3
         End
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPRINT 
         Caption         =   ""
         Visible         =   0   'False
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuExit 
         Caption         =   ""
      End
   End
   Begin VB.Menu mnuFenetre 
      Caption         =   ""
      Begin VB.Menu mnuSpool 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuMosaiqueH 
         Caption         =   ""
      End
      Begin VB.Menu mnuMosaiqueV 
         Caption         =   ""
      End
      Begin VB.Menu mnuCascade 
         Caption         =   ""
      End
      Begin VB.Menu mnuReorIcone 
         Caption         =   ""
      End
      Begin VB.Menu mnusep4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   0
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   1
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   2
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   3
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   4
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   5
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   6
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   7
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   8
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   9
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   10
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   11
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   12
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   13
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   14
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   15
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   16
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   17
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   18
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   19
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   20
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   21
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   22
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   23
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   24
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   25
      End
      Begin VB.Menu mnuJoin 
         Caption         =   ""
         Index           =   26
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "mnuPopUp"
      Visible         =   0   'False
      Begin VB.Menu mnuCloseFile 
         Caption         =   ""
      End
      Begin VB.Menu mnuDelFile 
         Caption         =   ""
         Begin VB.Menu mnuDeleteFile 
            Caption         =   ""
         End
         Begin VB.Menu mnuPoubelleFile 
            Caption         =   ""
         End
      End
      Begin VB.Menu mnuPrintObj 
         Caption         =   ""
      End
      Begin VB.Menu mnuProp 
         Caption         =   ""
      End
      Begin VB.Menu mnuSendTo 
         Caption         =   ""
         Begin VB.Menu mnuInternet 
            Caption         =   ""
            Visible         =   0   'False
         End
         Begin VB.Menu mnuMessagerie 
            Caption         =   ""
         End
         Begin VB.Menu mnuNotePad 
            Caption         =   ""
         End
      End
   End
   Begin VB.Menu mnuDeal 
      Caption         =   "?"
      Begin VB.Menu mnuApropos 
         Caption         =   ""
      End
      Begin VB.Menu mnuConfig 
         Caption         =   ""
      End
      Begin VB.Menu mnuMaj 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "MDIMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Private Declare Function TerminateProcess Lib "kernel32" (ByVal hProcess As Long, ByVal uExitCode As Long) As Long
Private Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As Long, ByRef lpdwProcessId As Long) As Long
Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Private Const SYNCHRONIZE = &H100000
Private Const PROCESS_TERMINATE As Long = &H1
Private WithEvents My_Instance As InstanceClass
Attribute My_Instance.VB_VarHelpID = -1

Dim NbNode As Long
Public NodeDrag As Node
Public NodeDrop As Node
Public Neu As Node
Private ChildNodes As CLS_Nodes
Public WithEvents m_fMessage As frmMessage
Attribute m_fMessage.VB_VarHelpID = -1

Public Sub TerminateApp(strProg As String)
Dim target_hwnd As Long
Dim target_process_id As Long
Dim target_process_handle As Long

    '-> on pointe sur la fenetre.
    target_hwnd = FindWindow(vbNullString, strProg)
    If target_hwnd = 0 Then
        Exit Sub
    End If

    '-> on reccupere l'ID.
    GetWindowThreadProcessId target_hwnd, target_process_id
    If target_process_id = 0 Then
        Exit Sub
    End If

    ' ouvrir le process.
    target_process_handle = OpenProcess(SYNCHRONIZE Or PROCESS_TERMINATE, ByVal 0&, target_process_id)
    If target_process_handle = 0 Then
        Exit Sub
    End If

    '-> terminer process.
    TerminateProcess target_process_handle, 0&

    '-> fermer le process.
    CloseHandle target_process_handle
End Sub

Private Sub imgClose_Click()

'-> Simuler  un click sur le menu Fermeture de l'explorateur de spool
mnuSpool_Click

End Sub

Private Sub MDIForm_Resize()
Dim aFichier As Fichier
Dim aSpool As Spool
Dim aNode As Node
Dim strKey As String

On Error Resume Next

'-> Quitter si pas de nodes
If Me.TreeNaviga.SelectedItem Is Nothing Then
    '->on verifie qu'on a pas deja
    For Each aNode In MDIMain.TreeNaviga.Nodes
        'DoEvents
        If aNode.Image = "PageSelected" Then
            strKey = aNode.Key
            Exit For
        End If
    Next
Else
    strKey = Me.TreeNaviga.SelectedItem.Key
End If
If strKey <> "" Then
    '-> Pointer sur le fichier associ�
    Set aFichier = Fichiers(Entry(1, strKey, "�"))
    '-> Pointer sur le spool associ�
    Set aSpool = aFichier.Spools(Entry(2, strKey, "�"))
    aSpool.frmdisplay.Resize
End If

End Sub

Private Sub mnuCloseFile3_Click()

Dim aFichier As Fichier

For Each aFichier In Fichiers
    CloseFichier aFichier.fileName
Next

End Sub

Private Sub My_Instance_ReceiveOutputs(CommandOutputs As String)
    ExecuterCommande CommandOutputs
End Sub

Private Sub ExecuterCommande(commande As String)
Dim NomFichier As String
    'ICI, mettez la routine de traitement des lignes de commandes (ouverture de fichier ou autre)
    commande = LTrim(commande)
    '-> Supprimer les Quotters s'il y en a
    If InStr(1, commande, """") = 1 Then commande = Mid$(commande, 2, Len(commande) - 1)
    If InStr(1, commande, """") <> 0 Then commande = Mid$(commande, 1, Len(commande) - 1)
    '-> Test surt le ZIP
    FilePath = Mid(commande, 1, InStrRev(Replace(commande, "\", "/"), "/"))
    NomFichier = GetUnzipFileName(commande)
    
            'dans le cas des packages
            If InStrRev(NomFichier, ".zip", , vbTextCompare) = Len(NomFichier) - 3 Then
                NomFichier = uZipInfo2
                For i = 1 To NumEntries(NomFichier, ",")
                    '-> Test sur le ZIP
                    commande = GetUnzipFileName(Entry(i, NomFichier, ","))
                    '-> Lancer le chargement du fichier
                    DisplayFileGUI commande
                    
                    '-> Charger eventuellement au menu les fichiers joints
                    LoadJoinFile uZipInfo2, commande
                Next
                NomFichier = ""
                Exit Sub
            End If
    '-> Lancer le chargement du fichier
    DisplayFileGUI NomFichier
    
    '-> Charger eventuellement au menu les fichiers joints
    LoadJoinFile uZipInfo2, NomFichier
End Sub

Private Sub MDIForm_Load()

Dim aLb As Libelle
Dim aListen As Integer

'-> Activer la gestion des erreurs
On Error Resume Next
'-> on gere l'unicit� de l'instance**************
If GetIniString("PARAM", "SESSION", TurboGraphIniFile, False) = "1" Then
    If InStr(1, Command, "�cran") <> 0 Then
        Set My_Instance = New InstanceClass
        'Initialisation de la classe (Cr�ation du TextBox de reception)
        If Not (My_Instance.PrevInstace) Then My_Instance.InitClass Me.hwnd
        'Renseigne le texte � faire passer au textbox de l'autre instance
        If Command <> "" Then My_Instance.CommandLine = Entry(2, Command, "|")
        'Ici on anvoie le texte � l'instance d�ja en m�moire et on arr�te le prog
        My_Instance.ActionCmd = Send_Killme
        If My_Instance.PrevInstace Then End
        'If App.PrevInstance Then End
        'If Command <> "" Then ExecuterCommande Command
    Else
    End If
End If
App.Title = "Turbograph Deal Informatiqe"
'************************************************

aListen = CInt(GetIniString("PARAM", "WATCHER", TurboGraphIniFile, False))
If aListen <> 0 Then Call mnuListenOption_Click(aListen)

If TestOutlook Then
    '-> Tester si Outlook est connect�
    OutLookOk = IsOutLook
    SetIniString "PARAM", "VERIFYOUTLOOK", App.Path & "\Turbograph.ini", "0"
    If IsOutLook Then
        SetIniString "PARAM", "ISOUTLOOK", App.Path & "\Turbograph.ini", "1"
    Else
        SetIniString "PARAM", "ISOUTLOOK", App.Path & "\Turbograph.ini", "0"
    End If
    If IsLotus Then
        SetIniString "PARAM", "ISLOTUS", App.Path & "\Turbograph.ini", "1"
    Else
        SetIniString "PARAM", "ISLOTUS", App.Path & "\Turbograph.ini", "0"
    End If
End If

'-> Tester si Excel OK
If TestTableur Then
    ExcelOk = IsExcel
    OpenOfficeOk = IsOpenoffice
    '-> on ne reverifiera plus
    SetIniString "PARAM", "VERIFYTABLEUR", App.Path & "\Turbograph.ini", "0"
    If IsExcel Then
        SetIniString "PARAM", "ISEXCEL", App.Path & "\Turbograph.ini", "1"
    Else
        SetIniString "PARAM", "ISEXCEL", App.Path & "\Turbograph.ini", "0"
    End If
    If IsOpenoffice Then
        SetIniString "PARAM", "ISOPENOFFICE", App.Path & "\Turbograph.ini", "1"
    Else
        SetIniString "PARAM", "ISOPENOFFICE", App.Path & "\Turbograph.ini", "0"
    End If
End If

'-> Charger les libelles par langue
Set aLb = Libelles("MDIMAIN")

'-> Titre de la fen�tre
Me.Caption = aLb.GetCaption(10)

'-> Menu fichier
Me.mnuFichier.Caption = aLb.GetCaption(1)
Me.mnuLoadFile.Caption = aLb.GetCaption(2)
Me.mnuCloseFile2.Caption = aLb.GetCaption(13)
Me.mnuCloseFile3.Caption = aLb.GetCaption(35)
Me.mnuGestFile.Caption = aLb.GetCaption(27)
Me.mnuListen.Caption = aLb.GetCaption(36)
Me.mnuPrint.Caption = aLb.GetCaption(3)
Me.mnuExit.Caption = aLb.GetCaption(4)
Me.mnuPrintAll.Caption = aLb.GetCaption(32)

'-> Menu Fen�tre
Me.mnuFenetre.Caption = aLb.GetCaption(5)
Me.mnuSpool.Caption = aLb.GetCaption(6)
Me.mnuMosaiqueH.Caption = aLb.GetCaption(7)
Me.mnuMosaiqueV.Caption = aLb.GetCaption(8)
Me.mnuCascade.Caption = aLb.GetCaption(9)
Me.mnuReorIcone.Caption = aLb.GetCaption(20)

'-> Menu DEAL
Me.mnuApropos.Caption = aLb.GetCaption(21)
Me.mnuMaj.Caption = aLb.GetCaption(26)

If VersionTurbo = 1 And GetIniString("PARAM", "SHOWCONFIG", TurboGraphIniFile, False) <> "0" Then
    Me.mnuMaj.Visible = True
    Me.mnuConfig.Visible = True
    Me.mnuConfig.Caption = aLb.GetCaption(29)
Else
    Me.mnuConfig.Visible = False
    Me.mnuMaj.Visible = False
End If

'-> on r�initialise
For i = 1 To MDIMain.mnuJoin.Count
    MDIMain.mnuJoin.Item(i - 1).Visible = False
    Fichiers(fileName).Spools(1).frmdisplay.Toolbar1.Buttons("FILEJOIN").ButtonMenus(i).Visible = False
Next

'-> menu POPUP
Me.mnuCloseFile.Caption = aLb.GetCaption(13)
Me.mnuPrintObj.Caption = aLb.GetCaption(14)
Me.mnuProp.Caption = aLb.GetCaption(15)
Me.mnuSendTo.Caption = aLb.GetCaption(16)
Me.mnuInternet.Caption = aLb.GetCaption(17)
Me.mnuMessagerie.Caption = aLb.GetCaption(18)
Me.mnuNotePad.Caption = aLb.GetCaption(19)
Me.mnuDelFile.Caption = aLb.GetCaption(22)
Me.mnuDeleteFile.Caption = aLb.GetCaption(23)
Me.mnuPoubelleFile.Caption = aLb.GetCaption(24)

'-> Masquer le mnue envoyer vers OutLook si pas outlook
If HideMessagerie = False Then Me.mnuMessagerie.Visible = OutLookOk

'-> Liberer le pointeur sur le libelle
Set aLb = Nothing

'-> Par d�faut, palette de navigation ferm�e
If GetIniString("PARAM", "NAVIGA", TurboGraphIniFile, False) = "1" Then
    IsNaviga = True
Else
    IsNaviga = False
End If
Me.picSplit.Visible = IsNaviga
Me.Line1.Visible = IsNaviga
Me.picNaviga.Visible = IsNaviga

'-> on regarde si on ouvre en plein ecran
Dim lpBuffer As String
lpBuffer = GetIniString("PARAM", "FULLSCREEN", App.Path & "\Turbograph.ini", False)
If lpBuffer <> "NULL" Then
    If Trim(lpBuffer) = "1" Then Me.WindowState = 2
End If

Me.StatusBar1.Panels(1).Text = "Version : " & App.Major & "." & App.Minor & "." & App.Revision

End Sub


Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)

End

End Sub

Private Sub MDIForm_Unload(Cancel As Integer)

Me.Caption = "**FIN**"
TerminateApp "**FIN**"

End Sub

Private Sub mnuApropos_Click()

'---> Afficher la page Apropos de
Load AproposDe
AproposDe.Show vbModal

End Sub

Private Sub mnuCloseFile2_Click()

'-> Demande de fermeture de fichier
On Error Resume Next
CloseFichier Me.ActiveForm.aSpool.fileName

End Sub

Private Sub mnuConfig_Click()

'---> Afficher la page Apropos de
Load frmConfig
frmConfig.Show

End Sub

Private Sub mnuFichier_Click()

If Me.TreeNaviga.Nodes.Count = 0 Then
    mnuCloseFile2.Enabled = False
Else
    mnuCloseFile2.Enabled = True
End If

End Sub

Private Sub mnuGestFile_Click()

'--> ouvrir le filtre des spools
'LockWindowUpdate Me.Hwnd
iniName = "TurboPublipostage.ini"
frmPublipost.Show vbModal
'LockWindowUpdate 0

End Sub

Private Sub mnuListenOption_Click(index As Integer)
        
    If Not mnuListenOption(index).Checked Then
        mnuListenOption(1).Checked = False
        mnuListenOption(2).Checked = False
        mnuListenOption(3).Checked = False
        frmListen.aListen = index
        mnuListenOption(index).Checked = True
        Load frmListen
        Set m_fMessage = New frmMessage
        
        'frmListen.Show
    Else
        mnuListenOption(1).Checked = False
        mnuListenOption(2).Checked = False
        mnuListenOption(3).Checked = False

        'frmListen.Sck.Close
        MDIMain.StatusBar1.Panels(2).Text = "Dblclick to open connexion"
        Sleep (500)
        mnuListenOption(index).Checked = False
        Unload frmListen
        Unload m_fMessage
        Set m_fMessage = Nothing

    End If
End Sub

Private Sub m_fMessage_TextClick()
    frmListen.traiteListen m_fMessage.Label1.Caption, True
End Sub

Private Sub mnuMaj_Click()

'---> Afficher la page Apropos de
Find_Update
End Sub

Private Sub mnuCascade_Click()

Me.Arrange vbCascade

End Sub

Private Sub mnuCloseFile_Click()

'-> Demande de fermeture de fichier
CloseFichier Me.TreeNaviga.SelectedItem.Key

End Sub

Private Sub mnuDeleteFile_Click()

'-> Supprimer de mani�re d�finitive le fichier
DeleteFile Me.TreeNaviga.SelectedItem.Key, 0

End Sub

Private Sub mnuExit_Click()

Unload Me

End Sub

Private Sub mnuInternet_Click()

'-> Setting de la valeur � envoyer
strRetour = Me.TreeNaviga.SelectedItem.Key

'-> Envoyer la cr�ation du fichier
frmMail.Show vbModal

End Sub

Private Sub mnuJoin_Click(index As Integer)
'-> Lancer si necessaire
ShellExecute MDIMain.hwnd, "Open", mnuJoin.Item(index).Tag, vbNullString, App.Path, 1
End Sub

Private Sub mnuLoadFile_Click()

Dim lpBuffer As String
Dim Res As Long
Dim IniPath As String
Dim Extension As String
Dim ExtensionOperat As String
Dim DefFileName As String
Dim aLb As Libelle
Dim fileName As String
Dim lngResult As Long
Dim strBuffer As String
Dim NomFichier As String

'On Error GoTo GestError

'-> R�pertoire par d�faut sp�cifi� dans le registre par la cle DEALTEMPO
If Not FirstOpen Then
    '-> V�rifier si un path est sp�cifi�
    If OpenPath <> "" Then
        Me.OpenDlg.InitDir = OpenPath
        FirstOpen = True
    End If
Else
    Me.OpenDlg.InitDir = ""
End If

'-> G�n�rer une erreur sur ANNULER
Me.OpenDlg.CancelError = True

'-> R�cup�rer le code op�rateur pour l'extension par d�faut
DefFileName = "*.Turbo"
Extension = "Fichiers Turbo (*.Turbo)|*.Turbo|Tous les fichiers (*.*)|*.*"
lpBuffer = GetVariableEnv("DEALOPERAT")
If lpBuffer <> "" Then
    ExtensionOperat = "Fichiers op�rateur (" & lpBuffer & ")|" & lpBuffer
    Extension = ExtensionOperat & "|" & Extension
    DefFileName = lpBuffer
Else
    ExtensionOperat = ""
End If

Me.OpenDlg.fileName = DefFileName
Me.OpenDlg.DefaultExt = DefFileName
Me.OpenDlg.Filter = Extension
Me.OpenDlg.MaxFileSize = 15000

'-> Flags d'ouverture
Me.OpenDlg.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly Or _
                   cdlOFNPathMustExist Or cdlOFNAllowMultiselect Or cdlOFNLongNames Or cdlOFNExplorer

'-> Afficher la feuille
Me.OpenDlg.ShowOpen

strtemp = Split(Me.OpenDlg.fileName, vbNullChar)

'Dim strFileName As String

For j = 0 To UBound(strtemp)
    If UBound(strtemp) > 1 Then
        If j = 0 Then j = j + 1
        fileName = strtemp(0) & "\" & strtemp(j)
    Else
        fileName = strtemp(0)
    End If
    '-> V�rifier ici que le fichier n'est pas d�ja charg� THIERRY
    If IsLoadedFile(fileName) Then
        Set aLb = Libelles("MESSAGE")
        MsgBox aLb.GetCaption(2), vbExclamation + vbOKOnly, aLb.GetToolTip(2)
        GoTo suite
    End If
    
    '-> Affecter le nom
    'FileName = Me.OpenDlg.FileName
    
    '-> On regarde si il y a un programme associ� different de turbo
    strBuffer = Space$(260)
    lngResult = FindExecutable(fileName, "", strBuffer)
    If strBuffer <> "" And InStr(1, strBuffer, "turbograph", vbTextCompare) = 0 And Len(Trim(strBuffer)) > 6 Then
        '-> on lance l'application associ�e
        strBuffer = Left(strBuffer, Len(strBuffer) - Len(Dir(strBuffer)))
        lngResult = ShellExecute(Me.hwnd, "Open", fileName, vbNullString, strBuffer, 1)
        GoTo suite
    End If
    
    '-> Test surt le ZIP
    FilePath = Mid(fileName, 1, InStrRev(Replace(fileName, "\", "/"), "/"))
    NomFichier = GetUnzipFileName(fileName)
    
            'dans le cas des packages
            If InStrRev(NomFichier, ".zip", , vbTextCompare) = Len(NomFichier) - 3 Then
                NomFichier = uZipInfo2
                For i = 1 To NumEntries(NomFichier, ",")
                    '-> Test sur le ZIP
                    fileName = GetUnzipFileName(Entry(i, NomFichier, ","))
                    '-> Lancer le chargement du fichier
                    DisplayFileGUI fileName
                    
                    '-> Charger eventuellement au menu les fichiers joints
                    LoadJoinFile uZipInfo2, fileName
                Next
                NomFichier = ""
                GoTo suite
            End If
    '-> Lancer le chargement du fichier
    DisplayFileGUI NomFichier
    
    '-> Charger eventuellement au menu les fichiers joints
    LoadJoinFile uZipInfo2, NomFichier
suite:
Next

GestError:

Exit Sub
    

End Sub

Public Function IsLoadedFile(ByVal NomFile As String) As Boolean

'---> Cette proc�dure v�rifie si un fichier est d�ja charg�

Dim aFichier As Fichier

On Error GoTo GestError

'-> Pointer sur le fichier
Set aFichier = Fichiers(Trim(UCase$(NomFile)))

'-> Renvoyer une valeur de succ�s
IsLoadedFile = True

Exit Function

GestError:
    IsLoadedFile = False


End Function

Private Sub mnuMessagerie_Click()

'-> Setting de la valeur � envoyer
strRetour = Me.TreeNaviga.SelectedItem.Key

If InStr(1, strRetour, "�PAGE") <> 0 Then
    '-> Envoie d'une page
    frmMail.lvSend = 2
ElseIf InStr(1, strRetour, "�SPOOL") <> 0 Then
    '-> Envoie du spool
    frmMail.lvSend = 1
Else
    '-> Envoie du fichier
    frmMail.lvSend = 0
End If

'-> Envoyer la cr�ation du fichier
frmMail.Show vbModal

End Sub

Private Sub mnuMosaiqueH_Click()

Me.Arrange vbTileHorizontal

End Sub

Private Sub mnuMosaiqueV_Click()

Me.Arrange vbTileVertical

End Sub

Private Sub mnuNotePad_Click()
    
Dim aFichier As Fichier
Dim aSpool As Spool
Dim aPage As Integer
Dim i As Integer, j As Integer
Dim aLb As Libelle
Dim tmpPafe As String


On Error GoTo GestError

'-> Pointer sur le fichier
Set aFichier = Fichiers(Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))

'-> Charger l'�diteur
Load frmEditor

'-> Pointer sur la classe libelle
Set aLb = Libelles("FRMEDITOR")

MDIMain.Enabled = False
Screen.MousePointer = 11

'-> Envoyer le titre de l'impression
AddTexte aLb.GetCaption(16) & " " & aFichier.fileName & Chr(13) & Chr(10)

'-> Gestion de la temporisation
TailleLue = 0
TailleTotale = 0

'-> Envoie vers l'�diteur de texte
Select Case NumEntries(Me.TreeNaviga.SelectedItem.Key, "�")
    Case 1 '-> Envoie du fichier en entier
                
        '-> Calcul du nombre de lignes � imprimer
        For i = 1 To aFichier.Spools.Count
            Set aSpool = aFichier.Spools(i)
            For j = 1 To aSpool.nbPage
                InitTailleTotale aSpool, j
            Next
        Next
        '-> Pour tous les spools du fichier
        For i = 1 To aFichier.Spools.Count
            '-> Pointer sur le spool
            Set aSpool = aFichier.Spools(i)
            '-> Envoyer le titre de l'impression
            AddTexte aLb.GetCaption(17) & " " & aSpool.Key & Chr(13) & Chr(10)
            '-> Imprimer toutes les pages
            For j = 1 To aSpool.nbPage
                SendPage aSpool, j
                '-> Ajouter le mot cle de saut de page
                AddTexte "[PAGE]"
            Next
        Next
    Case 2 '-> Envoie d'un spool
        '-> Pointer sur le spool
        Set aSpool = aFichier.Spools(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�"))
        '-> Inti de la temporisation
        For i = 1 To aSpool.nbPage
            InitTailleTotale aSpool, i
        Next
        '-> Imprimer toutes les pages du spool
        '-> Envoyer le titre de l'impression
        AddTexte aLb.GetCaption(17) & " " & aSpool.Key & Chr(13) & Chr(10)
        For j = 1 To aSpool.nbPage
            SendPage aSpool, j
            '-> Ajouter le mot cle de saut de page
            AddTexte "[PAGE]"
        Next '-> Envoie d'une page
    Case 3
        '-> Pointer sur le spool
        Set aSpool = aFichier.Spools(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�"))
        '-> Envoyer le titre de l'impression
        AddTexte aLb.GetCaption(17) & " " & aSpool.Key & Chr(13) & Chr(10)
        AddTexte aLb.GetCaption(18) & " " & CInt(Entry(2, Entry(3, Me.TreeNaviga.SelectedItem.Key, "�"), "|")) & Chr(13) & Chr(10)
        '-> Init de la temporisation
        InitTailleTotale aSpool, CInt(Entry(2, Entry(3, Me.TreeNaviga.SelectedItem.Key, "�"), "|"))
        '-> Imprimer la page sp�cifi�e
        SendPage aSpool, CInt(Entry(2, Entry(3, Me.TreeNaviga.SelectedItem.Key, "�"), "|"))
End Select
    
GestError:

    MDIMain.Enabled = True
    Screen.MousePointer = 0
    
    MDIMain.StatusBar1.Refresh
    
    '-> Afficher l'�diteur
    frmEditor.Show

    
End Sub

Private Function InitTailleTotale(ByRef aSpool As Spool, pageToPrint As Integer) As Integer

Dim tmpPage As String


'-> V�rifier s'il y a des erreurs sur la page
If aSpool.GetErrorPage(pageToPrint) <> "" Then
    tmpPage = aSpool.GetErrorPage(pageToPrint)
Else
    tmpPage = aSpool.GetPage(pageToPrint)
End If
TailleTotale = TailleTotale + NumEntries(tmpPage, Chr(0))

End Function

Private Sub SendPage(ByRef aSpool As Spool, ByVal pageToPrint As Integer)

'---> Cette proc�dure envoie une page vers l'�diteur de texte

Dim i As Integer
Dim tmpPage As String

'-> R�cup�rer la page � imprimer ou ses erreurs
If aSpool.GetErrorPage(pageToPrint) <> "" Then
    tmpPage = aSpool.GetErrorPage(pageToPrint)
Else
    tmpPage = aSpool.GetPage(pageToPrint)
End If

For i = 1 To NumEntries(tmpPage, Chr(0))
    If frmEditor.RichTextBox1.Text = "" Then
        frmEditor.RichTextBox1.Text = Entry(i, tmpPage, Chr(0))
    Else
        frmEditor.RichTextBox1.Text = frmEditor.RichTextBox1.Text & Chr(13) & Chr(10) & Entry(i, tmpPage, Chr(0))
    End If
    TailleLue = TailleLue + 1
    Call DrawWait
Next


End Sub

Private Sub AddTexte(ByVal TextToAdd As String)

'---> Cette proc�dure ajoute une ligne de texte � l'�diteur

If frmEditor.RichTextBox1.Text = "" Then
    frmEditor.RichTextBox1.Text = TextToAdd
Else
    frmEditor.RichTextBox1.Text = frmEditor.RichTextBox1.Text & Chr(13) & Chr(10) & TextToAdd
End If

End Sub


Private Sub mnuPoubelleFile_Click()

'-> Envoyer le fichier vers la poubelle
DeleteFile Me.TreeNaviga.SelectedItem.Key, FOF_ALLOWUNDO

End Sub

Private Sub mnuPrintAll_Click()
'->on imprime tous les fichiers ouverts
PrintAll
End Sub

Private Sub mnuPrintObj_Click()

'---> Cette proc�dure imprimer l'�l�ment s�lectionn�

Dim PrinterDevice As String
Dim FileToPrint As String
Dim aFichier As Fichier
Dim aSpool As Spool
Dim NumPage As Integer
Dim aLb As Libelle
Dim NbCopies As Integer
Dim RectoVerso As String


'-> Vider la variable de retour
strRetour = ""

'-> Charger la feuille de l'imprimante
Load frmPrint

'-> Bloquer les mauvaises zones
frmPrint.Frame2.Enabled = False
frmPrint.Option1.Value = True
frmPrint.Option1.Enabled = False
frmPrint.Option2.Enabled = False
frmPrint.Option3.Enabled = False
frmPrint.Option4.Enabled = False
If nRectoVerso Then frmPrint.Check1.Value = 1

'-> Selon le node sr lequel on a imprim�
Select Case Me.TreeNaviga.SelectedItem.Tag
    Case "FICHIER"
        
        '-> Afficher la feuille
        frmPrint.Show vbModal
        
        '-> Quitter si annuler
        If strRetour = "" Then Exit Sub
        
        '-> R�cup du nom du printer
        PrinterDevice = Entry(1, strRetour, "|")
        
        '-> Recup du nombre de copies
        NbCopies = CInt(Entry(3, strRetour, "|"))
        
        '-> recup du parampetre recto-verso
        RectoVerso = Entry(4, strRetour, "|")
        
        '-> Faire un run directement de l'impression du fichier sp�cifi�
        Shell App.Path & "\TurboGraph.exe " & PrinterDevice & "~DIRECT~" & NbCopies & "|" & Me.TreeNaviga.SelectedItem.Key & "|" & RectoVerso, vbNormalFocus
        
    Case "SPOOL"
        '-> Imprimer le fichier en entier s'il n'y a qu'un seul spool dans le fichier
        
        '-> Afficher la feuille
        frmPrint.Show vbModal
        
        '-> Quitter si annuler
        If strRetour = "" Then Exit Sub
        
        '-> R�cup du nom du printer
        PrinterDevice = Entry(1, strRetour, "|")
        
        '-> Recup du nombre de copies
        NbCopies = CInt(Entry(3, strRetour, "|"))
             
        '-> recup du parampetre recto-verso
        RectoVerso = Entry(4, strRetour, "|")
             
        '-> Pointer sur l'objet sp�cifi�
        Set aFichier = Fichiers(Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))
        
        If aFichier.Spools.Count = 1 Then
            '-> Impression du fichier
            Shell App.Path & "\TurboGraph.exe " & PrinterDevice & "~DIRECT~" & NbCopies & "|" & aFichier.fileName & "|" & RectoVerso, vbNormalFocus
        Else
            '-> Pointer sur le spool
            Set aSpool = aFichier.Spools(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�"))
            '-> Cr�er le fichier � imprimer
            FileToPrint = CreateDetail(aSpool, -1)
            '-> Lancer l'impression
            Shell App.Path & "\TurboGraph.exe " & PrinterDevice & "~DIRECT~" & NbCopies & "|" & FileToPrint & "|" & RectoVerso, vbNormalFocus
        End If
                
    Case "PAGE"
    
        '-> Setting de l'option 3
        frmPrint.Option3.Value = True
    
        '-> Afficher la feuille
        frmPrint.Show vbModal
        
        '-> Quitter si annuler
        If strRetour = "" Then Exit Sub
        
        '-> R�cup du nom du printer
        PrinterDevice = Entry(1, strRetour, "|")
        
        '-> Recup du nombre de copies
        NbCopies = CInt(Entry(3, strRetour, "|"))
        
        '-> recup du parampetre recto-verso
        RectoVerso = Entry(4, strRetour, "|")
        
        '-> Pointer sur l'objet sp�cifi�
        Set aFichier = Fichiers(Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))
        '-> Pointer sur le spool
        Set aSpool = aFichier.Spools(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�"))
        '-> R�cup�rer le num�ro de la page � imprimer
        NumPage = CInt(Entry(2, Entry(3, Me.TreeNaviga.SelectedItem.Key, "�"), "|"))
        '-> Cr�er le fichier � imprimer
        FileToPrint = CreateDetail(aSpool, NumPage)
        '-> Lancer l'impression
        Shell App.Path & "\TurboGraph.exe " & PrinterDevice & "~DIRECT~" & NbCopies & "|" & FileToPrint & "|" & RectoVerso, vbNormalFocus
End Select


End Sub

Private Sub mnuProp_Click()

'---> Affiche la page de propri�t� d'un fichier

On Error Resume Next

ShowFileProperties Me.TreeNaviga.SelectedItem.Key

End Sub

Private Sub mnuReorIcone_Click()
Me.Arrange vbArrangeIcons
End Sub

Private Sub mnuSpool_Click()

'-> Afficher ou ne pas afficher le volet expmloration de spool
IsNaviga = Not IsNaviga
Me.mnuSpool.Checked = IsNaviga
Me.picSplit.Visible = IsNaviga
Me.Line1.Visible = IsNaviga
Me.picNaviga.Visible = IsNaviga

End Sub

Private Sub picNaviga_Resize()

Dim aRect As RECT
Dim Res As Long

On Error Resume Next

'-> R�cup�rer la taille de la zone cliente
Res = GetClientRect(Me.picNaviga.hwnd, aRect)

'-> Positionner l'entete de la fen�tre
Me.picTitre.Left = 3
Me.picTitre.Top = 7
Me.picTitre.width = aRect.Right - 6 - imgClose.width
Me.imgClose.Left = aRect.Right - imgClose.width
Me.imgClose.Top = Me.picTitre.Top - 2

'-> Positionner le treeview
Me.TreeNaviga.Left = Me.picTitre.Left
Me.TreeNaviga.Top = Me.picTitre.Top * 2 + Me.picTitre.height
Me.TreeNaviga.width = Me.picTitre.width + Me.imgClose.width + 6
Me.TreeNaviga.height = Me.picNaviga.ScaleY(Me.picNaviga.height, 1, 3) - 21
Me.Line1.y1 = Me.picNaviga.height / 2
Me.Line1.y2 = Me.Line1.y1 + 800

End Sub

Private Sub picSplit_Click()

Dim apt As POINTAPI
Dim Res As Long

'-> R�cup�rer la position du curseur
Res = GetCursorPos(apt)
'-> Convertir en coordonn�es clientes
Res = ScreenToClient(Me.hwnd, apt)
'-> Tester les positions mini et maxi
If apt.y > Me.picNaviga.ScaleY(Me.picNaviga.height, 1, 3) / 2 And apt.y < Me.picNaviga.ScaleY(Me.picNaviga.height, 1, 3) / 2 + 60 Then
    If Me.picNaviga.width > 100 Then
        Do While Me.picNaviga.width > 100
            Me.picNaviga.width = Me.picNaviga.width - Me.picNaviga.width / 10
            Sleep 30
            DoEvents
        Loop
    Else
        Do While Me.picNaviga.width < 3000
            Me.picNaviga.width = Me.picNaviga.width + Me.picNaviga.width / 10 + 50
            Sleep 30
            DoEvents
        Loop
    End If
End If

End Sub

Private Sub picSplit_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)

Dim apt As POINTAPI
Dim Res As Long

'-> R�cup�rer la position du curseur
Res = GetCursorPos(apt)
'-> Convertir en coordonn�es clientes
Res = ScreenToClient(Me.hwnd, apt)

If Button = vbLeftButton Then
    '-> Tester les positions mini et maxi
    If apt.X > Me.picNaviga.ScaleX(2, 7, 3) And apt.X < Me.picNaviga.ScaleX(Me.width, 1, 3) - Me.picNaviga.ScaleX(2, 7, 3) Then Me.picNaviga.width = Me.picNaviga.ScaleX(apt.X, 3, 1)
End If

If apt.y > Me.picNaviga.ScaleY(Me.picNaviga.height, 1, 3) / 2 And apt.y < Me.picNaviga.ScaleY(Me.picNaviga.height, 1, 3) / 2 + 60 Then
    Me.picSplit.MousePointer = 1
Else
    Me.picSplit.MousePointer = 9
End If


End Sub

Private Sub picTitre_Resize()

'---> Dessiner la barre de titre
Me.picTitre.Line (0, 0)-(Me.picTitre.width - 1, 0), rgb(255, 255, 255)
Me.picTitre.Line (0, 1)-(Me.picTitre.width - 1, 1), rgb(255, 255, 255)
Me.picTitre.Line (1, 1)-(Me.picTitre.width - 1, 1), &HE0E0E0
Me.picTitre.Line (0, 3)-(Me.picTitre.width - 1, 3), rgb(255, 255, 255)
Me.picTitre.Line (0, 4)-(Me.picTitre.width - 1, 4), rgb(255, 255, 255)
Me.picTitre.Line (1, 4)-(Me.picTitre.width - 1, 4), &HE0E0E0

End Sub

Private Sub StatusBar1_PanelDblClick(ByVal Panel As MSComctlLib.Panel)
    If Panel.index = 2 Then
        Call mnuListenOption_Click(1)
        Exit Sub
    End If
    If FileExist(Mouchard) Then
        Close #hdlMouchard
        frmEditor.RichTextBox1.LoadFile Mouchard
        hdlMouchard = 0
        'ShellExecute Me.Hwnd, "Open", Mouchard, vbNullString, strBuffer, 1
    End If

End Sub

Public Sub TreeNaviga_DblClick()

'---> Traiter l'activation des spools et des pages

Dim aFichier As Fichier
Dim aSpool As Spool

'-> Quitter si pas de nodes
If Me.TreeNaviga.SelectedItem Is Nothing Then Exit Sub

'-> Pointer sur le fichier associ�
On Error GoTo suite
Set aFichier = Fichiers(Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))
If 1 = 0 Then
suite:
    DisplayFileGUI (Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))
    Set aFichier = Fichiers(Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))
End If
'-> Pointer sur le spool associ�
If Me.TreeNaviga.SelectedItem.Tag <> "FICHIER" Then Set aSpool = aFichier.Spools(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�")) 'THIERRY

'-> selon le node surlequel on click
Select Case Me.TreeNaviga.SelectedItem.Tag

    Case "FICHIER"
        '-> Ne rien faire
    Case "SPOOL"
        '-> V�rifier si la visu de ce fichier est en cours
        If aSpool.frmdisplay Is Nothing Then InitFRMDISPLAY aSpool
        
        '-> V�rifier s'il y a des erreurs
        If aSpool.NbError = 0 Then
            '-> Imprimer la page surlaquelle on a cliqu�
            PrintPageSpool aSpool, 1 'CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|"))
            '-> Afficher la page
            aSpool.DisplayInterfaceByPage 1 'CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|"))
        Else
            aSpool.DisplayInterfaceByPage 1
        End If
                    
        '-> Afficher la page
        aSpool.frmdisplay.ZOrder
    
    Case "ERROR"
        
        '-> V�rifier si la visu de ce fichier est en cours
        If aSpool.frmdisplay Is Nothing Then InitFRMDISPLAY aSpool
        
        '-> 2 types d'erreurs : soit fatale soit page
        If InStr(1, Me.TreeNaviga.SelectedItem.Key, "PAGE|") <> 0 Then
            '-> C'est une erreur de page : Afficher la page
            '-> V�rifier que l'on demande d'afficher une page diff�rente
            If CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|")) <> aSpool.CurrentPage Then
                '-> Imprimer la page
                PrintPageSpool aSpool, CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|"))
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage aSpool.CurrentPage
            End If
            Exit Sub
        Else
            '-> Erreur fatale
            
            '-> Afficher la feuille en fonction du r�sultat de l'impression
            aSpool.DisplayInterfaceByPage (1)
        End If
        
        '-> Afficher la page
        aSpool.frmdisplay.ZOrder
            
    Case "PAGE"
        '-> V�rifier si la visu de ce fichier est en cours
        If aSpool.frmdisplay Is Nothing Then
            InitFRMDISPLAY aSpool
            '-> Imprimer la page surlaquelle on a cliqu�
            PrintPageSpool aSpool, CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|"))
            '-> Afficher la page
            aSpool.DisplayInterfaceByPage CInt(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|"))
        Else
            '-> V�rifier que l'on demande d'afficher une page diff�rente
            If CLng(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|")) <> aSpool.CurrentPage Then
                '-> Imprimer la page
                PrintPageSpool aSpool, CLng(Entry(NumEntries(Me.TreeNaviga.SelectedItem.Key, "|"), Me.TreeNaviga.SelectedItem.Key, "|"))
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage aSpool.CurrentPage
            End If
        End If
        
        '-> Mettre la feuille au premier plan
        aSpool.frmdisplay.ZOrder
        '-> on affiche les notes de la page
        DisplayNotes aSpool
        
End Select


End Sub

Public Sub InitFRMDISPLAY(ByRef aSpool As Spool)

Dim aFrm As frmDisplaySpool

'-> Cr�er une nouvelle instance
Set aFrm = New frmDisplaySpool
'-> Affect� le spool
Set aFrm.aSpool = aSpool
'-> Affecter la feuille au spool
Set aSpool.frmdisplay = aFrm

End Sub

Private Sub TimerOpenNode_Timer()
    On Error Resume Next
    Me.TreeNaviga.DropHighlight.Expanded = True
    Me.TimerOpenNode.Enabled = False
End Sub

Private Sub TreeNaviga_DragDrop(Source As Control, X As Single, y As Single)
    If Source.Tag = "TreeNaviga" Then
    
        If Not Me.TreeNaviga.HitTest(X, y) Is Nothing Then
            Set NodeDrop = Me.TreeNaviga.HitTest(X, y)
        End If
        
        If Not NodeDrag Is Nothing And Not NodeDrop Is Nothing Then
            NodeDrop.Expanded = True
            If NodeDrag.Tag = NodeDrop.Tag Then
                If Not DragAndDrop(Me.TreeNaviga, NodeDrag, NodeDrop) Then MsgBox "Impossible de le d�placer ici", vbCritical
                Set Me.TreeNaviga.DropHighlight = Nothing
            End If
        End If
        
    End If
    
    TimerOpenNode.Enabled = False

End Sub

Private Sub TreeNaviga_DragOver(Source As Control, X As Single, y As Single, State As Integer)
    If Source.Tag = "TreeNaviga" Then
        Set TreeNaviga.DropHighlight = TreeNaviga.HitTest(X, y)
        If Not TreeNaviga.DropHighlight Is Nothing Then
            TimerOpenNode.Enabled = False
            If TreeNaviga.DropHighlight.Expanded = False Then
                TimerOpenNode.Enabled = True
            End If
        End If
    End If

End Sub

Private Sub TreeNaviga_KeyDown(KeyCode As Integer, Shift As Integer)

Dim aSpool As Spool
Dim aFichier As Fichier

If Me.TreeNaviga.SelectedItem Is Nothing Then Exit Sub
If Me.TreeNaviga.SelectedItem.Tag <> "SPOOL" Then Exit Sub

'-> Pointer sur le fichier
Set aFichier = Fichiers(Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))

'-> Pointer sur le spool
Set aSpool = aFichier.Spools(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�"))

If KeyCode = 112 Then
    If aSpool.MaquetteASCII = "" Then
        MsgBox "Maquette sp�cifi�e dans le fichier : " & Chr(13) + Chr(10) & aSpool.maquette.Nom, vbInformation + vbOKOnly, "Nom de la maquette"
    Else
        MsgBox aSpool.MaquetteASCII, vbInformation + vbOKOnly, "Nom de la maquette"
    End If
End If


End Sub

Private Sub TreeNaviga_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)

'-> pour la gestion du drag&drop
If Not Me.TreeNaviga.HitTest(X, y) Is Nothing Then
    Me.TreeNaviga.DropHighlight = Me.TreeNaviga.HitTest(X, y)
    Set NodeDrag = Me.TreeNaviga.HitTest(X, y)
End If

'-> Teste sur le bouton
If Button <> vbRightButton Then Exit Sub

'-> S�lectionner le node
Set Me.TreeNaviga.SelectedItem = Me.TreeNaviga.HitTest(X, y)

'-> Quitter si pas de node
If Me.TreeNaviga.SelectedItem Is Nothing Then Exit Sub

'-> Afficher le menu selon le node
Select Case Me.TreeNaviga.SelectedItem.Tag
    Case "FICHIER"
        Me.mnuCloseFile.Visible = True
        Me.mnuProp.Visible = True
        Me.mnuDelFile.Visible = True
    Case "SPOOL", "PAGE", "ERROR"
        Me.mnuCloseFile.Visible = False
        Me.mnuProp.Visible = False
        Me.mnuDelFile.Visible = False
End Select

'-> Afficher le mennu contextuel
Me.PopupMenu Me.mnuPopup

End Sub

Public Function DisplayNotes(aSpool As Spool)
'--> cette fonction permet d'afficher les notes de la page en cours
On Error Resume Next

If fNotes Is Nothing Then Exit Function

For Each fNote In fNotes
    If fNote.Tag = aSpool.fileName & "|" & aSpool.Num_Spool & "|" & aSpool.CurrentPage Then
        fNote.Init aSpool.frmdisplay
        fNote.Visible = True
    Else
        fNote.Visible = False
    End If
Next

End Function

Public Function PrintAll()
'--> cette procedure permet d'imprimer un fichier
Dim strFichier As String
Dim aFichier As Fichier
Dim aSpool As Spool
Dim MinPage As Integer
Dim DeviceName As String
Dim ChoixPage As String
Dim PageMin As Integer
Dim PageMax As Integer
Dim NbCopies As Integer
Dim RectoVerso As String
Dim Assembl As String
Dim NoGard As String
Dim FileToPrint As String
Dim CurrentPIDProcess As Long
Dim aItem As ListItem
Dim i As Integer
Dim j As Integer
Dim printFilesJoins As String

On Error GoTo GestError

'->Vider la variable de retour
strRetour = ""

'-> on verifie si on a des fichiers
i = Fichiers.Count
If i = 0 Then Exit Function

frmPrint.Check3.Visible = True

'-> Afficher le choix de l'imprimante
frmPrint.Show vbModal

'-> Quitter si annuler
If strRetour = "" Then Exit Function

'-> Traiter les choix d'impression
DeviceName = Entry(1, strRetour, "|")
ChoixPage = Entry(2, strRetour, "|")
NbCopies = Entry(3, strRetour, "|")
RectoVerso = Entry(4, strRetour, "|")
If copyAssemb Then Assembl = Entry(5, strRetour, "|")
If noGarde Then NoGard = Entry(6, strRetour, "|")
If Entry(7, strRetour, "|") <> "" And NumEntries(strRetour, "|") >= 7 Then printFilesJoins = Entry(7, strRetour, "|")

'-> Bloquer la feuille
Me.Enabled = False
Screen.MousePointer = 11

frmWait.Show

'on parcours les fichiers
For Each aFichier In Fichiers
        j = j + 1
        frmWait.Label1.Caption = "" & j & "/" & i & "   " & aFichier.fileName
        DoEvents
        strFichier = GetUnzipFileName(aFichier.fileName)
        Call AnalyseFileToPrint(strFichier)
        
        '-> Pointer sur l'objet fichier
        Set aFichier = Fichiers(UCase$(strFichier))
        Set aSpool = aFichier.Spools(1)
        
        frmPrint.FichierName = aSpool.fileName
        frmPrint.SpoolKey = aSpool.Key
        frmPrint.IsSelectionPage = aSpool.IsSelectionPage
        
        '-> afficher ou pas la zone d'impression de la globalit� des spools
        If Fichiers(UCase$(aSpool.fileName)).Spools.Count = 1 Then
            frmPrint.Option4.Visible = False
        End If
        
        If aSpool.nbPage = 0 And Fichiers(UCase$(aSpool.fileName)).Spools.Count = 1 Then
            MsgBox "Ce fichier ne contient pas un spool avec des pages � imprimer", vbInformation, "Imprimer un fichier"
            Exit Function
        End If
                
        '-> Analyse du choix d'impression
        Select Case Entry(1, ChoixPage, "�")
            Case 1 '-> Fichier en entier
                '-> Construire le nom du fichier
                'FileToPrint = CreateDetail(Me.aSpool, -1)
                If aFichier.FileNameZip <> "" Then
                    FileToPrint = aFichier.FileNameZip
                Else
                    FileToPrint = aFichier.fileName
                End If
            Case 2 '-> Page mini � page maxi
                '-> R�cup�rer les pages � imprimer
                PageMin = CInt(Entry(2, ChoixPage, "�"))
                PageMax = CInt(Entry(3, ChoixPage, "�"))
                
                '-> V�rifier la page min
                If PageMin < 1 Then
                    '-> V�rifier si le fichier poss�de une page de s�lection
                    If aSpool.IsSelectionPage Then
                        PageMin = 0
                    Else
                        PageMin = 1
                    End If
                End If
                
                '-> V�rifier la page maxi
                If PageMax > aSpool.nbPage Then PageMax = aSpool.nbPage
                    
                '-> V�rifier que la page mini est < � la page maxi
                If PageMin > PageMax Then PageMin = PageMax
                    
                '-> Construire le nom du fichier
                FileToPrint = CreateDetail(aSpool, -2, PageMin, PageMax)
                            
            Case 3  '-> Page en cours
                '-> Construire le nom du fichier
                FileToPrint = CreateDetail(aSpool, aSpool.CurrentPage)
            Case 4 '-> Spool en cours
                'FileToPrint = aSpool.FileName
                FileToPrint = CreateDetail(aSpool, -1)
        End Select
            
        '-> Tester le cas de l'impression PDF
        
        ' *** PIERROT : tjrs pour gerer le soft WIN2PDF
        If UCase$(Trim(DeviceName)) = "WIN2PDF" Then
            CurrentPIDProcess = Shell(App.Path & "\TurboGraph.exe " & "WIN2PDF" & "~DIRECT~" & NbCopies & "|" & FileToPrint & "**0", vbNormalFocus)
        End If
        If UCase$(Trim(DeviceName)) = "ACROBAT PDFWRITER" Then
            '-> Lancer l'�dition
            CurrentPIDProcess = Shell(App.Path & "\TurboGraph.exe " & "ACROBAT PDFWRITER" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso & "||" & NoGard, vbNormalFocus)
        ElseIf UCase$(Trim(DeviceName)) = "ACROBAT DISTILLER" Then
            CurrentPIDProcess = Shell(App.Path & "\TurboGraph.exe " & "ACROBAT DISTILLER" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso & "||" & NoGard, vbNormalFocus)
        ElseIf UCase$(Trim(DeviceName)) = "ADOBE PDF" Then
            CurrentPIDProcess = Shell(App.Path & "\TurboGraph.exe " & "ADOBE PDF" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso & "||" & NoGard, vbNormalFocus)
        ElseIf UCase$(Trim(DeviceName)) = "PDFCREATOR" Then
            '-> on sort la sauvegarde automatique
            CurrentPIDProcess = Shell(App.Path & "\TurboGraph.exe " & "PDFCREATOR" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso & "||" & NoGard, vbNormalFocus)
        Else
            '-> Lancer l'impression
            CurrentPIDProcess = Shell(App.Path & "\TurboGraph.exe " & DeviceName & "~DIRECT~" & NbCopies & "|" & FileToPrint & "|" & RectoVerso & "|" & Assembl & "|" & NoGard & "|" & printFilesJoins, vbNormalFocus)
        End If
        Do While IsPidRunning(CurrentPIDProcess)
            '-> Lib�ration de la pile des messages
            DoEvents
            Sleep 500
        Loop
Next

GestError:
'-> Bloquer la feuille
Me.Enabled = True
Screen.MousePointer = 0

Unload frmWait


End Function

Private Sub TreeNaviga_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
    '-> drag & drop des fichiers
    If Button = 1 Then
        If Not NodeDrag Is Nothing Then
            If NodeDrag.Tag = "FICHIER" Or NodeDrag.Tag = "SPOOL" Or NodeDrag.Tag = "PAGE" Then
                If Not Me.TreeNaviga.HitTest(X, y) Is Nothing Then
                    If NodeDrag.index <> Me.TreeNaviga.HitTest(X, y).index Then
                        Me.TreeNaviga.Drag vbBeginDrag
                    End If
                End If
            End If
        End If
    End If

End Sub

Public Function DragAndDrop(tTreeNaviga As TreeView, _
            NodeDrag As Node, _
            NodeDrop As Node) As Boolean

    On Error GoTo gestion_erreur

    Dim i As Long
    Dim Node As CLS_Node
    
    DragAndDrop = True

    Set ChildNodes = New CLS_Nodes

    If NodeDrag.index <> NodeDrop.index Then
    
        If FindNodeBelow(NodeDrag, NodeDrop, ChildNodes) Then

            '   CLEAR NodeDrag
            tTreeNaviga.Nodes.Remove NodeDrag.index

            '   MOVE  NodeDrag
            Set Neu = tTreeNaviga.Nodes.add(NodeDrop, tvwPrevious, ChildNodes(1).Key, ChildNodes(1).Text, ChildNodes(1).Image, ChildNodes(1).Selectedimage)
            Neu.Tag = NodeDrop.Tag
            Neu.Expanded = True
            ChildNodes.Remove 1
            Dim aNode As Node
            For Each Node In ChildNodes
                With Node
                    Set aNode = tTreeNaviga.Nodes.add(.Parent, tvwChild, .Key, .Text, .Image, .Selectedimage)
                    aNode.Tag = Node.Tag
                End With
            Next
            
        Else
            ' CAN NOT MOVE NODE IF NodeDrop is in ChildNodes of NodeDrag
            DragAndDrop = False
        End If
    Else
        DragAndDrop = False
    End If

    Set ChildNodes = Nothing
    Exit Function

gestion_erreur:
    DragAndDrop = False
    MsgBox Err.Description, vbCritical, "ERREUR n�" & CStr(Err.Number)
End Function

'==================================================================================
'   Save all Children NODE of StartNode in a Collection
'==================================================================================
Public Function FindNodeBelow(StartNode As Node, _
        ForbidenNode As Node, _
        branche As CLS_Nodes) As Boolean

    Dim strErrmsg  As String
    Dim aNode As Node
    Dim Continue As Boolean

    On Error GoTo gestion_erreur

    FindNodeBelow = True
    
    With StartNode
        'branche.Add .Text, .Index, .Tag, "", .Key, .Image, .Selectedimage, .Key
        branche.add .Text, .index, .Tag, "", .Key, .Image, .Selectedimage, .Key
    End With
    
    Set aNode = StartNode
    Continue = True
    
    If StartNode.Children <> 0 Then
    
        While Continue
        
            If aNode.Children <> 0 Then
                Set aNode = aNode.Child
            Else
            
                If aNode = aNode.LastSibling Then
                
                    While aNode = aNode.LastSibling And Continue
                    
                        If aNode.Parent <> StartNode Then
                            Set aNode = aNode.Parent
                        Else
                            Continue = False
                        End If
                        
                    Wend
                    
                End If
                
                If Continue Then Set aNode = aNode.Next
                
            End If
            
            If aNode = ForbidenNode Then
                FindNodeBelow = False
                Continue = False
            End If
            
            If Continue Then
                With aNode
                    branche.add .Text, .index, .Tag, IIf(.Parent Is Nothing, "", .Parent.Key), .Key, .Image, .Selectedimage, .Key
                End With
            End If
            
        Wend

    End If
    
    Exit Function

gestion_erreur:
    MsgBox Err.Description, vbCritical, "ERREUR n�" & CStr(Err.Number)

End Function




