Attribute VB_Name = "Module1"
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal Hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Private Declare Function URLDownloadToFile Lib "urlmon" Alias "URLDownloadToFileA" (ByVal pCaller As Long, ByVal szURL As String, ByVal szFileName As String, ByVal dwReserved As Long, ByVal lpfnCB As Long) As Long
Private Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Private Declare Function GetEnvironmentVariable Lib "kernel32" Alias "GetEnvironmentVariableA" (ByVal lpName As String, ByVal lpBuffer As String, ByVal nSize As Long) As Long
Private Declare Function SHGetSpecialFolderLocation Lib "shell32.dll" (ByVal hWndOwner As Long, ByVal nFolder As Long, pidl As ITEMIDLIST) As Long
Public Declare Function SHGetPathFromIDList Lib "shell32" (ByVal pidList As Long, ByVal lpBuffer As String) As Long
Public Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As Any, ByVal lpFileName As String)

'-> API pour avoir le repertoire mes documents
Private Type SHITEMID
    cb As Long
    abID As Byte
End Type
Private Type ITEMIDLIST
    mkid As SHITEMID
End Type
Public Const CSIDL_PERSONAL = &H5

Public Function TelechargeFile(strUrlName As String, strPathNameDestination As String) As Boolean
Dim errcode As Long
Dim localFileName As String
  
'-> On sp�cifie le chemin de l'image web
'url = "http://www.siteweb.com/image.jpg"
  
'On appelle la fonction api de t�l�chargement
errcode = URLDownloadToFile(0, strUrlName, strPathNameDestination, 0, 0)
  
If errcode = 0 Then
    '-> l'image a �t� rapatri�e
    TelechargeFile = True
Else
    '-> l'image n'a pas pu etre telechargee
    TelechargeFile = False
End If

End Function

Public Function SetIniString(AppName As String, lpKeyName As String, IniFile As String, Value As String)

Dim Res As Long
Dim lpBuffer As String


Res = WritePrivateProfileString(AppName, lpKeyName, Value, IniFile)

End Function

Public Function GetIniString(AppName As String, lpKeyName As String, IniFile As String, Error As Boolean) As String

Dim Res As Long
Dim lpBuffer As String


lpBuffer = Space$(2000)
Res = GetPrivateProfileString(AppName, lpKeyName, "NULL", lpBuffer, Len(lpBuffer), IniFile)
lpBuffer = Entry(1, lpBuffer, Chr(0))
If lpBuffer = "NULL" Then
    If Error Then
        MsgBox "Impossible de trouver la cl� : " & lpKeyName & _
               " dans la section : " & AppName & " du fichier : " & _
                IniFile, vbCritical + vbOKOnly, "Erreur de lecture"
    End If
    GetIniString = ""
Else
    lpBuffer = Mid$(lpBuffer, 1, Res)
    GetIniString = lpBuffer
End If


End Function

Public Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Variant

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Long
Dim i As Long
Dim PosAnalyse As Long

On Error GoTo GestError

PosAnalyse = 1

Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1

Exit Function

GestError:

    '-> Dans ce cas la, renvoyer 1
    NumEntries = 1
    

End Function

Public Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Long
Dim PosEnCour As Long
Dim i As Long
Dim CHarDeb As Long
Dim CharEnd As Long

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + 1 > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid$(Vecteur, PosEnCour + 1, Len(Vecteur) - PosEnCour + 1)
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid$(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + 1
Loop
        
End Function

Public Function GetVariableEnv(strVariable As String) As String

Dim Res As Long
Dim lpBuffer As String

lpBuffer = Space$(500)
Res = GetEnvironmentVariable(strVariable, lpBuffer, Len(lpBuffer))
If Res <> 0 Then
    '-> Faire un trim sur le buffer
    lpBuffer = Mid$(lpBuffer, 1, Res)
Else
    lpBuffer = ""
End If

'-> Retouner la valeur
GetVariableEnv = lpBuffer

End Function

Public Function GetSpecialfolder(CSIDL As Long) As String
'-> pour retrouver le repertoire mes documents
    Dim r As Long
    Dim IDL As ITEMIDLIST
    'Get the special folder
    r = SHGetSpecialFolderLocation(100, CSIDL, IDL)
    If r = NOERROR Then
        'Create a buffer
        Path$ = Space$(512)
        'Get the path from the IDList
        r = SHGetPathFromIDList(ByVal IDL.mkid.cb, ByVal Path$)
        'Remove the unnecessary chr$(0)'s
        GetSpecialfolder = Left$(Path, InStr(Path, Chr$(0)) - 1)
        Exit Function
    End If
    GetSpecialfolder = ""
End Function

Public Function GetFileName(MyFile As String) As String

Dim i As Integer
MyFile = Replace(MyFile, "/", "\")
i = InStrRev(MyFile, "\")
If i = 0 Then
    GetFileName = MyFile
Else
    GetFileName = Mid$(MyFile, i + 1, Len(MyFile) - i)
End If


End Function

