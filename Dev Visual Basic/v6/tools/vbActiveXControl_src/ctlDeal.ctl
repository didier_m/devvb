VERSION 5.00
Begin VB.UserControl ctlDeal 
   ClientHeight    =   1125
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   9720
   ScaleHeight     =   1125
   ScaleWidth      =   9720
   Begin VB.TextBox urlFile 
      Height          =   435
      Left            =   120
      TabIndex        =   1
      Top             =   330
      Width           =   7575
   End
   Begin VB.CommandButton cmdUrl 
      Caption         =   "Ouvrir le fichier"
      Height          =   585
      Left            =   8040
      TabIndex        =   0
      Top             =   240
      Width           =   1470
   End
   Begin VB.Label Label1 
      Caption         =   "Deal Informatique"
      ForeColor       =   &H0000C000&
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   840
      Width           =   3495
   End
End
Attribute VB_Name = "ctlDeal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public turboSavePath As String

Public Function downloadAndOpenFile(txt As String)
    Dim strName As String
    '-> on charge l'url
    urlFile.Text = txt
    strName = GetFileName(txt)
    '-> on va chercher le repertoire de destination de spools
    '-> on reccupere le repertoire de sauvegarde par defaut
    lpBuffer = GetIniString("PARAM", "SAVE", App.Path & "\Turbograph.ini", False)
    If lpBuffer <> "NULL" Then
        turboSavePath = Trim(lpBuffer)
    End If
    '-> on regarde si il n'y a pas une variable d'environnement dedans du style %username%
    If NumEntries(turboSavePath, "%") = 3 Then
        turboSavePath = Entry(1, turboSavePath, "%") & GetVariableEnv(Entry(2, turboSavePath, "%")) & Entry(3, turboSavePath, "%")
    End If
    If turboSavePath = "" Then turboSavePath = GetSpecialfolder(CSIDL_PERSONAL) & "\mes spools\"
    '-> on verifie que le dernier caractere est bien un '\'
    If Right(turboSavePath, 1) <> "/" And Right(turboSavePath, 1) <> "\" Then turboSavePath = turboSavePath & "\"
    If TelechargeFile(urlFile, turboSavePath & strName) Then
        ShellExecute 0, "open", turboSavePath & strName, vbNullString, App.Path, 1
    Else
        MsgBox "Impossible de tÚlÚcharger le fichier " + urlFile.Text, vbCritical
    End If
End Function

Public Function info()
MsgBox "Deal Informatique 2015 (DMZ)"
End Function

Public Function GetPoids(Optional strPath As String) As String
Dim strVal As String
'-> on reccupere les resultats
strVal = GetIniString("POIDS", "BRUT", strPath & "\poids.txt", False)
strVal = strVal + "|" + GetIniString("POIDS", "NET", strPath & "\poids.txt", False)
strVal = strVal + "|" + GetIniString("POIDS", "TARE", strPath & "\poids.txt", False)
strVal = strVal + "|" + GetIniString("POIDS", "TICKET", strPath & "\poids.txt", False)
strVal = strVal + "|" + GetIniString("POIDS", "DATE", strPath & "\poids.txt", False)
strVal = strVal + "|" + GetIniString("POIDS", "HEURE", strPath & "\poids.txt", False)
strVal = strVal + "|" + GetIniString("POIDS", "STATUT", strPath & "\poids.txt", False)
strVal = strVal + "|" + GetIniString("POIDS", "BRUT", strPath & "\poids.txt", False)
strVal = strVal + "|" + GetIniString("POIDS", "NETSIGNE", strPath & "\poids.txt", False)
strVal = strVal + "|" + GetIniString("POIDS", "BRUTSIGNE", strPath & "\poids.txt", False)
strVal = strVal + "|" + GetIniString("POIDS", "TARESIGNE", strPath & "\poids.txt", False)

GetPoids = strVal

End Function

Public Function AskPoids(Optional strPath As String) As String
Dim strVal As String
'-> on reccupere les resultats
SetIniString "POIDS", "ASK", strPath & "\poids.txt", "TRUE"

End Function

Public Function Telecharge(urlFile As String, strSave As String) As String
    If TelechargeFile(urlFile, strSave) Then
        Telecharge = "OK"
    Else
        MsgBox "Impossible de tÚlÚcharger le fichier " + urlFile, vbCritical
        Telecharge = "ERROR"
    End If
End Function

Private Sub cmdUrl_Click()
    downloadAndOpenFile (urlFile.Text)
End Sub
