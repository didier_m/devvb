Imports System.Resources

Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' Les informations g�n�rales relatives � un assembly d�pendent de l'ensemble
' d'attributs suivant. Pour modifier les informations associ�es � un assembly,
'changez les valeurs de ces attributs.


' TODO�: v�rifiez les valeurs des attributs de l'assembly


<Assembly: AssemblyTitle("TurboPDF")> 
<Assembly: AssemblyDescription("G�n�rateur de spool PDF")> 
<Assembly: AssemblyCompany("DEAL INFORMATIQUE")> 
<Assembly: AssemblyProduct("Turbopdf")> 
<Assembly: AssemblyCopyright("Deal Informatique ")> 
<Assembly: AssemblyTrademark("Deal Informatique")> 
<Assembly: AssemblyCulture("")> 
<Assembly: CLSCompliant(True)> 
<Assembly: AssemblyDelaySign(False)> 

<Assembly: Guid("DC7F64B9-515C-40CC-865A-A7CCD5EE86DD")> 

'Strong Naming key-pair
'<Assembly: AssemblyKeyFile("TurboPDF.snk")> 

' Les informations de version pour un assembly se composent des quatre valeurs suivantes�:

'	Version principale
'	Version secondaire
'	Num�ro de build
'	R�vision

' Vous pouvez sp�cifier toutes les valeurs ou indiquer les num�ros de build et de r�vision par d�faut
' en utilisant '*' comme indiqu� ci-dessous�:

<Assembly: AssemblyVersion("1.0.0.1")> 

<Assembly: NeutralResourcesLanguageAttribute("fr")> 
<Assembly: AssemblyFileVersionAttribute("1.0.0.1")> 
<Assembly: ComVisibleAttribute(True)> 
