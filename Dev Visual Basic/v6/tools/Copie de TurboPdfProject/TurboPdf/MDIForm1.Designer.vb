<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class MDIMain
#Region "Code g�n�r� par le Concepteur Windows Form "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Cet appel est requis par le Concepteur Windows Form.
		InitializeComponent()
	End Sub
	'Form remplace la m�thode Dispose pour nettoyer la liste des composants.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requise par le Concepteur Windows Form
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public OpenDlgOpen As System.Windows.Forms.OpenFileDialog
	Public WithEvents _StatusBar1_Panel1 As System.Windows.Forms.ToolStripStatusLabel
	Public WithEvents _StatusBar1_Panel2 As System.Windows.Forms.ToolStripStatusLabel
	Public WithEvents _StatusBar1_Panel3 As System.Windows.Forms.ToolStripStatusLabel
	Public WithEvents StatusBar1 As System.Windows.Forms.StatusStrip
	Public WithEvents picSplit As System.Windows.Forms.PictureBox
	Public WithEvents TreeNaviga As System.Windows.Forms.TreeView
	Public WithEvents picTitre As System.Windows.Forms.PictureBox
	Public WithEvents imgClose As System.Windows.Forms.PictureBox
	Public WithEvents picNaviga As System.Windows.Forms.Panel
	Public WithEvents ImageList1 As System.Windows.Forms.ImageList
	Public WithEvents ImageList2 As System.Windows.Forms.ImageList
	Public WithEvents mnuJoin As Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray
	Public WithEvents mnuLoadFile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSep1 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSep2 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuExit As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFichier As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSpool As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSep3 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuMosaiqueH As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuMosaiqueV As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuCascade As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuReorIcone As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnusep4 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents _mnuJoin_0 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_1 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_2 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_3 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_4 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_5 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_6 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_7 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_8 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_9 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_10 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_11 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_12 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_13 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_14 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_15 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_16 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_17 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_18 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_19 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_20 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_21 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_22 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_23 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_24 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_25 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuJoin_26 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFenetre As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuCloseFile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuDeleteFile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuPoubelleFile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuDelFile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuPrintObj As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuProp As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuInternet As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuMessagerie As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuNotePad As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSendTo As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuPopup As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuApropos As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuDeal As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MainMenu1 As System.Windows.Forms.MenuStrip
	'REMARQUE�: la proc�dure suivante est requise par le Concepteur Windows Form
	'Elle peut �tre modifi�e � l'aide du Concepteur Windows Form.
	'Ne la modifiez pas � l'aide de l'�diteur de code.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(MDIMain))
		Me.IsMDIContainer = True
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.OpenDlgOpen = New System.Windows.Forms.OpenFileDialog
		Me.StatusBar1 = New System.Windows.Forms.StatusStrip
		Me._StatusBar1_Panel1 = New System.Windows.Forms.ToolStripStatusLabel
		Me._StatusBar1_Panel2 = New System.Windows.Forms.ToolStripStatusLabel
		Me._StatusBar1_Panel3 = New System.Windows.Forms.ToolStripStatusLabel
		Me.picSplit = New System.Windows.Forms.PictureBox
		Me.picNaviga = New System.Windows.Forms.Panel
		Me.TreeNaviga = New System.Windows.Forms.TreeView
		Me.picTitre = New System.Windows.Forms.PictureBox
		Me.imgClose = New System.Windows.Forms.PictureBox
		Me.ImageList1 = New System.Windows.Forms.ImageList
		Me.ImageList2 = New System.Windows.Forms.ImageList
		Me.mnuJoin = New Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray(components)
		Me.MainMenu1 = New System.Windows.Forms.MenuStrip
		Me.mnuFichier = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuLoadFile = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuSep1 = New System.Windows.Forms.ToolStripSeparator
		Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuSep2 = New System.Windows.Forms.ToolStripSeparator
		Me.mnuExit = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuFenetre = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuSpool = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuSep3 = New System.Windows.Forms.ToolStripSeparator
		Me.mnuMosaiqueH = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuMosaiqueV = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuCascade = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuReorIcone = New System.Windows.Forms.ToolStripMenuItem
		Me.mnusep4 = New System.Windows.Forms.ToolStripSeparator
		Me._mnuJoin_0 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_1 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_2 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_3 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_4 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_5 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_6 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_7 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_8 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_9 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_10 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_11 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_12 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_13 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_14 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_15 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_16 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_17 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_18 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_19 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_20 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_21 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_22 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_23 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_24 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_25 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuJoin_26 = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuPopup = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuCloseFile = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuDelFile = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuDeleteFile = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuPoubelleFile = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuPrintObj = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuProp = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuSendTo = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuInternet = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuMessagerie = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuNotePad = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuDeal = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuApropos = New System.Windows.Forms.ToolStripMenuItem
		Me.StatusBar1.SuspendLayout()
		Me.picNaviga.SuspendLayout()
		Me.MainMenu1.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.mnuJoin, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.BackColor = System.Drawing.SystemColors.AppWorkspace
		Me.Text = "MDIForm1"
		Me.ClientSize = New System.Drawing.Size(831, 504)
		Me.Location = New System.Drawing.Point(11, 37)
		Me.Icon = CType(resources.GetObject("MDIMain.Icon"), System.Drawing.Icon)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "MDIMain"
		Me.StatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.StatusBar1.Size = New System.Drawing.Size(831, 17)
		Me.StatusBar1.Location = New System.Drawing.Point(0, 487)
		Me.StatusBar1.TabIndex = 4
		Me.StatusBar1.Name = "StatusBar1"
		Me._StatusBar1_Panel1.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
		Me._StatusBar1_Panel1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me._StatusBar1_Panel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._StatusBar1_Panel1.Size = New System.Drawing.Size(516, 17)
		Me._StatusBar1_Panel1.Spring = True
		Me._StatusBar1_Panel1.AutoSize = True
		Me._StatusBar1_Panel1.BorderSides = CType(System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom, System.Windows.Forms.ToolStripStatusLabelBorderSides)
		Me._StatusBar1_Panel1.Margin = New System.Windows.Forms.Padding(0)
		Me._StatusBar1_Panel1.AutoSize = False
		Me._StatusBar1_Panel2.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
		Me._StatusBar1_Panel2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me._StatusBar1_Panel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._StatusBar1_Panel2.Size = New System.Drawing.Size(201, 17)
		Me._StatusBar1_Panel2.BorderSides = CType(System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom, System.Windows.Forms.ToolStripStatusLabelBorderSides)
		Me._StatusBar1_Panel2.Margin = New System.Windows.Forms.Padding(0)
		Me._StatusBar1_Panel2.AutoSize = False
		Me._StatusBar1_Panel3.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
		Me._StatusBar1_Panel3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me._StatusBar1_Panel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me._StatusBar1_Panel3.BorderSides = CType(System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom, System.Windows.Forms.ToolStripStatusLabelBorderSides)
		Me._StatusBar1_Panel3.Margin = New System.Windows.Forms.Padding(0)
		Me._StatusBar1_Panel3.Size = New System.Drawing.Size(96, 17)
		Me._StatusBar1_Panel3.AutoSize = False
		Me.picSplit.Dock = System.Windows.Forms.DockStyle.Left
		Me.picSplit.Size = New System.Drawing.Size(7, 487)
		Me.picSplit.Location = New System.Drawing.Point(192, 0)
		Me.picSplit.Cursor = System.Windows.Forms.Cursors.SizeWE
		Me.picSplit.TabIndex = 3
		Me.picSplit.BackColor = System.Drawing.SystemColors.Control
		Me.picSplit.CausesValidation = True
		Me.picSplit.Enabled = True
		Me.picSplit.ForeColor = System.Drawing.SystemColors.ControlText
		Me.picSplit.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.picSplit.TabStop = True
		Me.picSplit.Visible = True
		Me.picSplit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal
		Me.picSplit.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.picSplit.Name = "picSplit"
		Me.picNaviga.Dock = System.Windows.Forms.DockStyle.Left
		Me.picNaviga.Size = New System.Drawing.Size(192, 487)
		Me.picNaviga.Location = New System.Drawing.Point(0, 0)
		Me.picNaviga.TabIndex = 0
		Me.picNaviga.BackColor = System.Drawing.SystemColors.Control
		Me.picNaviga.CausesValidation = True
		Me.picNaviga.Enabled = True
		Me.picNaviga.ForeColor = System.Drawing.SystemColors.ControlText
		Me.picNaviga.Cursor = System.Windows.Forms.Cursors.Default
		Me.picNaviga.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.picNaviga.TabStop = True
		Me.picNaviga.Visible = True
		Me.picNaviga.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.picNaviga.Name = "picNaviga"
		Me.TreeNaviga.CausesValidation = True
		Me.TreeNaviga.Size = New System.Drawing.Size(153, 97)
		Me.TreeNaviga.Location = New System.Drawing.Point(16, 96)
		Me.TreeNaviga.TabIndex = 2
		Me.TreeNaviga.HideSelection = False
		Me.TreeNaviga.Indent = 27
		Me.TreeNaviga.LabelEdit = False
		Me.TreeNaviga.ImageList = ImageList1
		Me.TreeNaviga.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.TreeNaviga.Name = "TreeNaviga"
		Me.picTitre.BackColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.picTitre.Size = New System.Drawing.Size(128, 6)
		Me.picTitre.Location = New System.Drawing.Point(16, 56)
		Me.picTitre.TabIndex = 1
		Me.picTitre.Dock = System.Windows.Forms.DockStyle.None
		Me.picTitre.CausesValidation = True
		Me.picTitre.Enabled = True
		Me.picTitre.ForeColor = System.Drawing.SystemColors.ControlText
		Me.picTitre.Cursor = System.Windows.Forms.Cursors.Default
		Me.picTitre.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.picTitre.TabStop = True
		Me.picTitre.Visible = True
		Me.picTitre.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal
		Me.picTitre.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.picTitre.Name = "picTitre"
		Me.imgClose.Size = New System.Drawing.Size(11, 11)
		Me.imgClose.Location = New System.Drawing.Point(160, 16)
		Me.imgClose.Image = CType(resources.GetObject("imgClose.Image"), System.Drawing.Image)
		Me.imgClose.Enabled = True
		Me.imgClose.Cursor = System.Windows.Forms.Cursors.Default
		Me.imgClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal
		Me.imgClose.Visible = True
		Me.imgClose.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.imgClose.Name = "imgClose"
		Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
		Me.ImageList1.TransparentColor = System.Drawing.Color.FromARGB(192, 192, 192)
		Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
		Me.ImageList1.Images.SetKeyName(0, "Open")
		Me.ImageList1.Images.SetKeyName(1, "Close")
		Me.ImageList1.Images.SetKeyName(2, "Spool")
		Me.ImageList1.Images.SetKeyName(3, "Page")
		Me.ImageList1.Images.SetKeyName(4, "Warning")
		Me.ImageList1.Images.SetKeyName(5, "PageSelected")
		Me.ImageList2.ImageSize = New System.Drawing.Size(32, 32)
		Me.ImageList2.TransparentColor = System.Drawing.Color.FromARGB(192, 192, 192)
		Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
		Me.ImageList2.Images.SetKeyName(0, "MAIL")
		Me.ImageList2.Images.SetKeyName(1, "MESSGAERIE")
		Me.mnuFichier.Name = "mnuFichier"
		Me.mnuFichier.Text = ""
		Me.mnuFichier.Checked = False
		Me.mnuFichier.Enabled = True
		Me.mnuFichier.Visible = True
		Me.mnuLoadFile.Name = "mnuLoadFile"
		Me.mnuLoadFile.Text = ""
		Me.mnuLoadFile.Checked = False
		Me.mnuLoadFile.Enabled = True
		Me.mnuLoadFile.Visible = True
		Me.mnuSep1.Enabled = True
		Me.mnuSep1.Visible = True
		Me.mnuSep1.Name = "mnuSep1"
		Me.mnuPrint.Name = "mnuPrint"
		Me.mnuPrint.Text = ""
		Me.mnuPrint.Visible = False
		Me.mnuPrint.Checked = False
		Me.mnuPrint.Enabled = True
		Me.mnuSep2.Visible = False
		Me.mnuSep2.Enabled = True
		Me.mnuSep2.Name = "mnuSep2"
		Me.mnuExit.Name = "mnuExit"
		Me.mnuExit.Text = ""
		Me.mnuExit.Checked = False
		Me.mnuExit.Enabled = True
		Me.mnuExit.Visible = True
		Me.mnuFenetre.Name = "mnuFenetre"
		Me.mnuFenetre.Text = ""
		Me.mnuFenetre.Checked = False
		Me.mnuFenetre.Enabled = True
		Me.mnuFenetre.Visible = True
		Me.mnuSpool.Name = "mnuSpool"
		Me.mnuSpool.Text = ""
		Me.mnuSpool.Checked = False
		Me.mnuSpool.Enabled = True
		Me.mnuSpool.Visible = True
		Me.mnuSep3.Enabled = True
		Me.mnuSep3.Visible = True
		Me.mnuSep3.Name = "mnuSep3"
		Me.mnuMosaiqueH.Name = "mnuMosaiqueH"
		Me.mnuMosaiqueH.Text = ""
		Me.mnuMosaiqueH.Checked = False
		Me.mnuMosaiqueH.Enabled = True
		Me.mnuMosaiqueH.Visible = True
		Me.mnuMosaiqueV.Name = "mnuMosaiqueV"
		Me.mnuMosaiqueV.Text = ""
		Me.mnuMosaiqueV.Checked = False
		Me.mnuMosaiqueV.Enabled = True
		Me.mnuMosaiqueV.Visible = True
		Me.mnuCascade.Name = "mnuCascade"
		Me.mnuCascade.Text = ""
		Me.mnuCascade.Checked = False
		Me.mnuCascade.Enabled = True
		Me.mnuCascade.Visible = True
		Me.mnuReorIcone.Name = "mnuReorIcone"
		Me.mnuReorIcone.Text = ""
		Me.mnuReorIcone.Checked = False
		Me.mnuReorIcone.Enabled = True
		Me.mnuReorIcone.Visible = True
		Me.mnusep4.Enabled = True
		Me.mnusep4.Visible = True
		Me.mnusep4.Name = "mnusep4"
		Me._mnuJoin_0.Name = "_mnuJoin_0"
		Me._mnuJoin_0.Text = ""
		Me._mnuJoin_0.Checked = False
		Me._mnuJoin_0.Enabled = True
		Me._mnuJoin_0.Visible = True
		Me._mnuJoin_1.Name = "_mnuJoin_1"
		Me._mnuJoin_1.Text = ""
		Me._mnuJoin_1.Checked = False
		Me._mnuJoin_1.Enabled = True
		Me._mnuJoin_1.Visible = True
		Me._mnuJoin_2.Name = "_mnuJoin_2"
		Me._mnuJoin_2.Text = ""
		Me._mnuJoin_2.Checked = False
		Me._mnuJoin_2.Enabled = True
		Me._mnuJoin_2.Visible = True
		Me._mnuJoin_3.Name = "_mnuJoin_3"
		Me._mnuJoin_3.Text = ""
		Me._mnuJoin_3.Checked = False
		Me._mnuJoin_3.Enabled = True
		Me._mnuJoin_3.Visible = True
		Me._mnuJoin_4.Name = "_mnuJoin_4"
		Me._mnuJoin_4.Text = ""
		Me._mnuJoin_4.Checked = False
		Me._mnuJoin_4.Enabled = True
		Me._mnuJoin_4.Visible = True
		Me._mnuJoin_5.Name = "_mnuJoin_5"
		Me._mnuJoin_5.Text = ""
		Me._mnuJoin_5.Checked = False
		Me._mnuJoin_5.Enabled = True
		Me._mnuJoin_5.Visible = True
		Me._mnuJoin_6.Name = "_mnuJoin_6"
		Me._mnuJoin_6.Text = ""
		Me._mnuJoin_6.Checked = False
		Me._mnuJoin_6.Enabled = True
		Me._mnuJoin_6.Visible = True
		Me._mnuJoin_7.Name = "_mnuJoin_7"
		Me._mnuJoin_7.Text = ""
		Me._mnuJoin_7.Checked = False
		Me._mnuJoin_7.Enabled = True
		Me._mnuJoin_7.Visible = True
		Me._mnuJoin_8.Name = "_mnuJoin_8"
		Me._mnuJoin_8.Text = ""
		Me._mnuJoin_8.Checked = False
		Me._mnuJoin_8.Enabled = True
		Me._mnuJoin_8.Visible = True
		Me._mnuJoin_9.Name = "_mnuJoin_9"
		Me._mnuJoin_9.Text = ""
		Me._mnuJoin_9.Checked = False
		Me._mnuJoin_9.Enabled = True
		Me._mnuJoin_9.Visible = True
		Me._mnuJoin_10.Name = "_mnuJoin_10"
		Me._mnuJoin_10.Text = ""
		Me._mnuJoin_10.Checked = False
		Me._mnuJoin_10.Enabled = True
		Me._mnuJoin_10.Visible = True
		Me._mnuJoin_11.Name = "_mnuJoin_11"
		Me._mnuJoin_11.Text = ""
		Me._mnuJoin_11.Checked = False
		Me._mnuJoin_11.Enabled = True
		Me._mnuJoin_11.Visible = True
		Me._mnuJoin_12.Name = "_mnuJoin_12"
		Me._mnuJoin_12.Text = ""
		Me._mnuJoin_12.Checked = False
		Me._mnuJoin_12.Enabled = True
		Me._mnuJoin_12.Visible = True
		Me._mnuJoin_13.Name = "_mnuJoin_13"
		Me._mnuJoin_13.Text = ""
		Me._mnuJoin_13.Checked = False
		Me._mnuJoin_13.Enabled = True
		Me._mnuJoin_13.Visible = True
		Me._mnuJoin_14.Name = "_mnuJoin_14"
		Me._mnuJoin_14.Text = ""
		Me._mnuJoin_14.Checked = False
		Me._mnuJoin_14.Enabled = True
		Me._mnuJoin_14.Visible = True
		Me._mnuJoin_15.Name = "_mnuJoin_15"
		Me._mnuJoin_15.Text = ""
		Me._mnuJoin_15.Checked = False
		Me._mnuJoin_15.Enabled = True
		Me._mnuJoin_15.Visible = True
		Me._mnuJoin_16.Name = "_mnuJoin_16"
		Me._mnuJoin_16.Text = ""
		Me._mnuJoin_16.Checked = False
		Me._mnuJoin_16.Enabled = True
		Me._mnuJoin_16.Visible = True
		Me._mnuJoin_17.Name = "_mnuJoin_17"
		Me._mnuJoin_17.Text = ""
		Me._mnuJoin_17.Checked = False
		Me._mnuJoin_17.Enabled = True
		Me._mnuJoin_17.Visible = True
		Me._mnuJoin_18.Name = "_mnuJoin_18"
		Me._mnuJoin_18.Text = ""
		Me._mnuJoin_18.Checked = False
		Me._mnuJoin_18.Enabled = True
		Me._mnuJoin_18.Visible = True
		Me._mnuJoin_19.Name = "_mnuJoin_19"
		Me._mnuJoin_19.Text = ""
		Me._mnuJoin_19.Checked = False
		Me._mnuJoin_19.Enabled = True
		Me._mnuJoin_19.Visible = True
		Me._mnuJoin_20.Name = "_mnuJoin_20"
		Me._mnuJoin_20.Text = ""
		Me._mnuJoin_20.Checked = False
		Me._mnuJoin_20.Enabled = True
		Me._mnuJoin_20.Visible = True
		Me._mnuJoin_21.Name = "_mnuJoin_21"
		Me._mnuJoin_21.Text = ""
		Me._mnuJoin_21.Checked = False
		Me._mnuJoin_21.Enabled = True
		Me._mnuJoin_21.Visible = True
		Me._mnuJoin_22.Name = "_mnuJoin_22"
		Me._mnuJoin_22.Text = ""
		Me._mnuJoin_22.Checked = False
		Me._mnuJoin_22.Enabled = True
		Me._mnuJoin_22.Visible = True
		Me._mnuJoin_23.Name = "_mnuJoin_23"
		Me._mnuJoin_23.Text = ""
		Me._mnuJoin_23.Checked = False
		Me._mnuJoin_23.Enabled = True
		Me._mnuJoin_23.Visible = True
		Me._mnuJoin_24.Name = "_mnuJoin_24"
		Me._mnuJoin_24.Text = ""
		Me._mnuJoin_24.Checked = False
		Me._mnuJoin_24.Enabled = True
		Me._mnuJoin_24.Visible = True
		Me._mnuJoin_25.Name = "_mnuJoin_25"
		Me._mnuJoin_25.Text = ""
		Me._mnuJoin_25.Checked = False
		Me._mnuJoin_25.Enabled = True
		Me._mnuJoin_25.Visible = True
		Me._mnuJoin_26.Name = "_mnuJoin_26"
		Me._mnuJoin_26.Text = ""
		Me._mnuJoin_26.Checked = False
		Me._mnuJoin_26.Enabled = True
		Me._mnuJoin_26.Visible = True
		Me.mnuPopup.Name = "mnuPopup"
		Me.mnuPopup.Text = "mnuPopUp"
		Me.mnuPopup.Visible = False
		Me.mnuPopup.Checked = False
		Me.mnuPopup.Enabled = True
		Me.mnuCloseFile.Name = "mnuCloseFile"
		Me.mnuCloseFile.Text = ""
		Me.mnuCloseFile.Checked = False
		Me.mnuCloseFile.Enabled = True
		Me.mnuCloseFile.Visible = True
		Me.mnuDelFile.Name = "mnuDelFile"
		Me.mnuDelFile.Text = ""
		Me.mnuDelFile.Checked = False
		Me.mnuDelFile.Enabled = True
		Me.mnuDelFile.Visible = True
		Me.mnuDeleteFile.Name = "mnuDeleteFile"
		Me.mnuDeleteFile.Text = ""
		Me.mnuDeleteFile.Checked = False
		Me.mnuDeleteFile.Enabled = True
		Me.mnuDeleteFile.Visible = True
		Me.mnuPoubelleFile.Name = "mnuPoubelleFile"
		Me.mnuPoubelleFile.Text = ""
		Me.mnuPoubelleFile.Checked = False
		Me.mnuPoubelleFile.Enabled = True
		Me.mnuPoubelleFile.Visible = True
		Me.mnuPrintObj.Name = "mnuPrintObj"
		Me.mnuPrintObj.Text = ""
		Me.mnuPrintObj.Checked = False
		Me.mnuPrintObj.Enabled = True
		Me.mnuPrintObj.Visible = True
		Me.mnuProp.Name = "mnuProp"
		Me.mnuProp.Text = ""
		Me.mnuProp.Checked = False
		Me.mnuProp.Enabled = True
		Me.mnuProp.Visible = True
		Me.mnuSendTo.Name = "mnuSendTo"
		Me.mnuSendTo.Text = ""
		Me.mnuSendTo.Checked = False
		Me.mnuSendTo.Enabled = True
		Me.mnuSendTo.Visible = True
		Me.mnuInternet.Name = "mnuInternet"
		Me.mnuInternet.Text = ""
		Me.mnuInternet.Visible = False
		Me.mnuInternet.Checked = False
		Me.mnuInternet.Enabled = True
		Me.mnuMessagerie.Name = "mnuMessagerie"
		Me.mnuMessagerie.Text = ""
		Me.mnuMessagerie.Checked = False
		Me.mnuMessagerie.Enabled = True
		Me.mnuMessagerie.Visible = True
		Me.mnuNotePad.Name = "mnuNotePad"
		Me.mnuNotePad.Text = ""
		Me.mnuNotePad.Checked = False
		Me.mnuNotePad.Enabled = True
		Me.mnuNotePad.Visible = True
		Me.mnuDeal.Name = "mnuDeal"
		Me.mnuDeal.Text = "?"
		Me.mnuDeal.Checked = False
		Me.mnuDeal.Enabled = True
		Me.mnuDeal.Visible = True
		Me.mnuApropos.Name = "mnuApropos"
		Me.mnuApropos.Text = ""
		Me.mnuApropos.Checked = False
		Me.mnuApropos.Enabled = True
		Me.mnuApropos.Visible = True
		Me.Controls.Add(StatusBar1)
		Me.Controls.Add(picSplit)
		Me.Controls.Add(picNaviga)
		Me.StatusBar1.Items.AddRange(New System.Windows.Forms.ToolStripItem(){Me._StatusBar1_Panel1})
		Me.StatusBar1.Items.AddRange(New System.Windows.Forms.ToolStripItem(){Me._StatusBar1_Panel2})
		Me.StatusBar1.Items.AddRange(New System.Windows.Forms.ToolStripItem(){Me._StatusBar1_Panel3})
		Me.picNaviga.Controls.Add(TreeNaviga)
		Me.picNaviga.Controls.Add(picTitre)
		Me.picNaviga.Controls.Add(imgClose)
		Me.mnuJoin.SetIndex(_mnuJoin_0, CType(0, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_1, CType(1, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_2, CType(2, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_3, CType(3, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_4, CType(4, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_5, CType(5, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_6, CType(6, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_7, CType(7, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_8, CType(8, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_9, CType(9, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_10, CType(10, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_11, CType(11, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_12, CType(12, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_13, CType(13, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_14, CType(14, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_15, CType(15, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_16, CType(16, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_17, CType(17, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_18, CType(18, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_19, CType(19, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_20, CType(20, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_21, CType(21, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_22, CType(22, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_23, CType(23, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_24, CType(24, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_25, CType(25, Short))
		Me.mnuJoin.SetIndex(_mnuJoin_26, CType(26, Short))
		CType(Me.mnuJoin, System.ComponentModel.ISupportInitialize).EndInit()
		Me.mnuFichier.MergeAction = System.Windows.Forms.MergeAction.Remove
		Me.mnuFenetre.MergeAction = System.Windows.Forms.MergeAction.Remove
		Me.mnuPopup.MergeAction = System.Windows.Forms.MergeAction.Remove
		Me.mnuDeal.MergeAction = System.Windows.Forms.MergeAction.Remove
		MainMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem(){Me.mnuFichier, Me.mnuFenetre, Me.mnuPopup, Me.mnuDeal})
		mnuFichier.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem(){Me.mnuLoadFile, Me.mnuSep1, Me.mnuPrint, Me.mnuSep2, Me.mnuExit})
		mnuFenetre.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem(){Me.mnuSpool, Me.mnuSep3, Me.mnuMosaiqueH, Me.mnuMosaiqueV, Me.mnuCascade, Me.mnuReorIcone, Me.mnusep4, Me._mnuJoin_0, Me._mnuJoin_1, Me._mnuJoin_2, Me._mnuJoin_3, Me._mnuJoin_4, Me._mnuJoin_5, Me._mnuJoin_6, Me._mnuJoin_7, Me._mnuJoin_8, Me._mnuJoin_9, Me._mnuJoin_10, Me._mnuJoin_11, Me._mnuJoin_12, Me._mnuJoin_13, Me._mnuJoin_14, Me._mnuJoin_15, Me._mnuJoin_16, Me._mnuJoin_17, Me._mnuJoin_18, Me._mnuJoin_19, Me._mnuJoin_20, Me._mnuJoin_21, Me._mnuJoin_22, Me._mnuJoin_23, Me._mnuJoin_24, Me._mnuJoin_25, Me._mnuJoin_26})
		mnuPopup.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem(){Me.mnuCloseFile, Me.mnuDelFile, Me.mnuPrintObj, Me.mnuProp, Me.mnuSendTo})
		mnuDelFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem(){Me.mnuDeleteFile, Me.mnuPoubelleFile})
		mnuSendTo.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem(){Me.mnuInternet, Me.mnuMessagerie, Me.mnuNotePad})
		mnuDeal.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem(){Me.mnuApropos})
		Me.Controls.Add(MainMenu1)
		Me.StatusBar1.ResumeLayout(False)
		Me.picNaviga.ResumeLayout(False)
		Me.MainMenu1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class