Option Strict Off
Option Explicit On
Public Class clsRange
    'UPGRADE_WARNING: La limite inf�rieure du tableau pBorderLineStyle est pass�e de 7 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    Private pBorderLineStyle(10) As Object
    'UPGRADE_WARNING: La limite inf�rieure du tableau pBorderColorIndex est pass�e de 7 � 0. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    Private pBorderColorIndex(10) As Object
    Public FontName As String
    Public FontSize As Short
    Public FontBold As Boolean
    Public FontItalic As Boolean
    Public FontUnderline As Boolean
    Public BackColor As Integer
    Public VerticalAlignment As Object
    Public HorizontalAlignment As Object
    Public WrapText As Boolean


    Public Sub SetBorderLineStyle(ByRef Border As Integer, ByRef Value As Object)
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Value. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet pBorderLineStyle(Border). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        pBorderLineStyle(Border) = Value
    End Sub

    Public Sub SetBorderColorIndex(ByRef Border As Integer, ByRef Value As Object)
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet Value. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet pBorderColorIndex(Border). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        pBorderColorIndex(Border) = Value
    End Sub

    Public Function GetBorderLineStyle(ByRef Border As Integer) As Object
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet pBorderLineStyle(Border). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        GetBorderLineStyle = pBorderLineStyle(Border)
    End Function

    Public Function GetBorderColorIndex(ByRef Border As Integer) As Object
        'UPGRADE_WARNING: Impossible de r�soudre la propri�t� par d�faut de l'objet pBorderColorIndex(Border). Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        GetBorderColorIndex = pBorderColorIndex(Border)
    End Function
End Class