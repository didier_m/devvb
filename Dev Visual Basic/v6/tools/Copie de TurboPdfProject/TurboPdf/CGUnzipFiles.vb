Option Strict Off
Option Explicit On
Public Class CGUnzipFiles

    '-> Extraction de l'atchive sp�cifi�e dans le r�pertoire sp�cifi�
    Public Function ExtractFile(ByRef NomArchive As String, ByRef ExtractDirectory As String) As Boolean

        Dim RetTestZip As Integer

        On Error GoTo GestError

        '->Initialisation des variables globales
        uZipInfo = ""
        uZipNumber = 0
        uPromptOverWrite = 0
        uOverWriteFiles = 1
        uDisplayComment = 0
        uExtractList = 0
        uHonorDirectories = 0
        'uZipNames.uzFiles(0) = vbNullString
        uNumberFiles = 0
        'uExcludeNames.uzFiles(0) = vbNullString
        uNumberXFiles = 0
        '-> On sp�cifie le nom du fichier
        uZipFileName = NomArchive
        uExtractDir = ExtractDirectory
        uTestZip = 0
        '-> Lancer le test pour v�rifier si le fichier est zip�
        RetTestZip = VBUnZip32()
        If RetTestZip = 0 Then ExtractFile = True

        Exit Function

GestError:

        ExtractFile = False



    End Function

    '-> Test si l'archive est OK et zipp�e
    Public Function TestArchive(ByRef NomFichier As String) As Boolean

        Dim RetTestZip As Integer

        'On Error GoTo GestError

        '-> PAs d'archive � blanc
        If Trim(NomFichier) = "" Then GoTo GestError
        'UPGRADE_WARNING: Dir a un nouveau comportement. Cliquez ici�: 'ms-help://MS.VSExpressCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        If Dir(NomFichier) = "" Then GoTo GestError

        '->Initialisation des variables globales
        uZipInfo = ""
        uZipNumber = 0
        uPromptOverWrite = 0
        uOverWriteFiles = 1
        uDisplayComment = 0
        uExtractList = 1
        uHonorDirectories = 0
        'uZipNames.uzFiles(0) = vbNullString
        uNumberFiles = 0
        'uExcludeNames.uzFiles(0) = vbNullString
        uNumberXFiles = 0
        '-> On sp�cifie le nom du fichier
        uZipFileName = NomFichier
        uExtractDir = ""
        uTestZip = 1
        '-> Lancer le test pour v�rifier si le fichier est zip�
        RetTestZip = VBUnZip32()
        If RetTestZip = 0 Then TestArchive = True

        Exit Function

GestError:

        TestArchive = False


    End Function
End Class