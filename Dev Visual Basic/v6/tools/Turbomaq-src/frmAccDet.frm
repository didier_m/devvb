VERSION 5.00
Begin VB.Form frmAccDet 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   3705
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5235
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   247
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   349
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   2895
      Left            =   0
      TabIndex        =   1
      Top             =   75
      Width           =   5175
      Begin VB.CheckBox Check1 
         Caption         =   "Check1"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   0
         Width           =   2535
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   2520
         TabIndex        =   3
         Top             =   360
         Width           =   2535
      End
      Begin VB.ListBox List1 
         Height          =   2010
         Left            =   2520
         TabIndex        =   2
         Top             =   720
         Width           =   2535
      End
      Begin VB.Label Label1 
         Caption         =   "Champ de l'acc�s au d�tail : "
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   2175
      End
      Begin VB.Label Label2 
         Caption         =   "Condition : "
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   720
         Width           =   2295
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   4440
      ScaleHeight     =   615
      ScaleWidth      =   735
      TabIndex        =   0
      Top             =   3000
      Width           =   735
   End
End
Attribute VB_Name = "frmAccDet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private MyBlock As Block

Private Sub Check1_Click()

If Me.Check1.Value = 1 Then
    Me.Text1.Enabled = True
    Me.Label1.Enabled = True
    Me.Label2.Enabled = True
    Me.List1.Enabled = True
Else
    Me.Text1.Enabled = False
    Me.Label1.Enabled = False
    Me.Label2.Enabled = False
    Me.List1.Enabled = False
End If

End Sub

Private Sub Form_Load()

Dim aLb As Libelle

'-> Titre de la feuille
Set aLb = Libelles("MNUACCDET")
Me.Caption = aLb.GetCaption(1)

'-> Messprog de la feuille
Me.Check1.Caption = aLb.GetCaption(2)
Me.Label1.Caption = aLb.GetCaption(3)
Me.Label2.Caption = aLb.GetCaption(4)

'-> Chargement des conditions
Set aLb = Libelles("CONDITIONS")
Me.List1.AddItem aLb.GetCaption(1)
Me.List1.AddItem aLb.GetCaption(2)
Me.List1.AddItem aLb.GetCaption(3)
Me.List1.AddItem aLb.GetCaption(4)
Me.List1.AddItem aLb.GetCaption(5)
Me.List1.AddItem aLb.GetCaption(6)
Me.List1.AddItem aLb.GetCaption(7)

'-> gestion des images
Me.Picture1.Picture = Turbo_Maq.Ok.Picture

End Sub

Public Sub Init(aBlock As Block)

'---> Proc�dure d'initialisation

If aBlock.UseAccesDet Then
    '-> Positionner le code
    Me.Text1.Text = Entry(1, aBlock.KeyAccesDet, "|")
    '-> Posiitonner la condition
    Me.List1.ListIndex = CInt(Entry(2, aBlock.KeyAccesDet, "|")) - 1
    '-> D�bloquer la case � cocher
    Me.Check1.Value = 1
Else
    Me.Check1.Value = 0
    Me.List1.ListIndex = 0
End If 'Si on utilise l'acc�s au d�tail

'-> Simuler un click
Check1_Click

'-> Pointeur vers le block en cours
Set MyBlock = aBlock

End Sub


Private Sub Picture1_Click()

'---> Mise � jour de l'acc�s au d�tail

Dim aLb As Libelle
Dim aCell As Cellule

'-> Pointer sur la classe libell�
Set aLb = Libelles("MESSAGE")

If Me.Check1.Value = 1 Then
    '-> V�rifier que l'on ait sp�cifi� la cl� d'acc�s au d�tail
    If Trim(Me.Text1.Text) = "" Or Len(Trim(Me.Text1.Text)) > 4 Then
        MsgBox aLb.GetCaption(48), vbExclamation + vbOKOnly, aLb.GetToolTip(48)
        Me.Text1.SetFocus
        Exit Sub
    End If
    '-> Setting des valeurs
    MyBlock.UseAccesDet = True
    MyBlock.KeyAccesDet = Trim(Me.Text1.Text) & "|" & CStr(Me.List1.ListIndex + 1)
Else
    '-> On n'utilse pas l'acc�s au d�tail
    MyBlock.UseAccesDet = False
    MyBlock.KeyAccesDet = ""
    '-> Virer tous les acc�s au d�tail des cellules du block
    For Each aCell In MyBlock.Cellules
        aCell.UseAccesDet = False
    Next
End If

'-> D�charger la feuille
Unload Me

End Sub
