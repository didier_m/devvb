VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmBasMaquette 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   213
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   312
   StartUpPosition =   2  'CenterScreen
   Begin RichTextLib.RichTextBox Text1 
      Height          =   1815
      Left            =   0
      TabIndex        =   1
      Top             =   480
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   3201
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"frmBasMaquette.frx":0000
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4680
      _ExtentX        =   8255
      _ExtentY        =   741
      ButtonWidth     =   609
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.ToolTipText     =   "Annuler"
            Object.Tag             =   "CANCEL"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.ToolTipText     =   "Mettre � jour"
            Object.Tag             =   "OK"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmBasMaquette"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()

Me.Text1.Text = MaqBasPage

End Sub

Private Sub Form_Resize()

Dim Res As Long
Dim aRect As Rect

Res = GetClientRect(Me.hwnd, aRect)
Me.Text1.Top = Me.Toolbar1.Height
Me.Text1.Left = 0
Me.Text1.Width = aRect.Right
Me.Text1.Height = aRect.Bottom - Me.Text1.Top

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

If Button.Tag = "OK" Then MaqBasPage = Me.Text1.Text
        
Unload Me

End Sub
