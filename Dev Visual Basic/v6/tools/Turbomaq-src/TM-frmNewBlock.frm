VERSION 5.00
Begin VB.Form frmNewBlock 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   3675
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   2910
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3675
   ScaleWidth      =   2910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   2175
      Left            =   120
      TabIndex        =   2
      Top             =   840
      Width           =   2655
      Begin VB.TextBox LargeurCol 
         Height          =   285
         Left            =   2040
         TabIndex        =   10
         Top             =   1560
         Width           =   495
      End
      Begin VB.TextBox NbCol 
         Height          =   285
         Left            =   2040
         TabIndex        =   8
         Top             =   1200
         Width           =   495
      End
      Begin VB.TextBox HauteurLig 
         Height          =   285
         Left            =   2040
         TabIndex        =   6
         Top             =   840
         Width           =   495
      End
      Begin VB.TextBox NbLigne 
         Height          =   285
         Left            =   2040
         TabIndex        =   4
         Top             =   480
         Width           =   495
      End
      Begin VB.Label Label5 
         Caption         =   "Label5"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   1560
         Width           =   1815
      End
      Begin VB.Label Label4 
         Caption         =   "Label4"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   1200
         Width           =   1935
      End
      Begin VB.Label Label3 
         Caption         =   "Label3"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   840
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "Label2"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   480
         Width           =   1935
      End
   End
   Begin VB.TextBox NomBlock 
      Height          =   285
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   2655
   End
   Begin VB.Image Annuler 
      Height          =   480
      Left            =   720
      Top             =   3120
      Width           =   630
   End
   Begin VB.Image Aide 
      Height          =   480
      Left            =   1440
      Top             =   3120
      Width           =   630
   End
   Begin VB.Image Ok 
      Height          =   480
      Left            =   2160
      Top             =   3120
      Width           =   630
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2055
   End
End
Attribute VB_Name = "frmNewBlock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public TableauCours As Tableau
Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub Annuler_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Unload Me
End Sub

Private Sub Form_Load()

Dim aLb As Libelle

Set aLb = Libelles("FRMNEWBLOCK")

Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.NomBlock.Text = aLb.GetCaption(3) & Me.TableauCours.nBlocks
Me.Frame1.Caption = aLb.GetCaption(4)
Me.Label2.Caption = aLb.GetCaption(5)
Me.Label3.Caption = aLb.GetCaption(7)
Me.Label4.Caption = aLb.GetCaption(6)
Me.Label5.Caption = aLb.GetCaption(8)

Me.NbLigne.Text = DefNbLig
Me.HauteurLig = DefHauteurLig
Me.NbCol.Text = DefNbCol
Me.LargeurCol = DefLargeurCol

Set aLb = Libelles("BOUTONS")
Me.Annuler.ToolTipText = aLb.GetCaption(1)
Me.Aide.ToolTipText = aLb.GetCaption(2)
Me.Ok.ToolTipText = aLb.GetCaption(3)

Me.Annuler.Picture = Turbo_Maq.Annuler.Picture
Me.Aide.Picture = Turbo_Maq.Aide.Picture
Me.Ok.Picture = Turbo_Maq.Ok.Picture

Set aLb = Nothing


End Sub

Private Sub HauteurLig_GotFocus()

    Me.HauteurLig.SelStart = 0
    Me.HauteurLig.SelLength = 65535

End Sub

Private Sub HauteurLig_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then Ok_Click
    '-> V�rifier que le s�parateur d�cimal n'a pas d�ja �t� saisi
    If KeyAscii = Asc(SepDec) Then
        If InStr(1, Me.HauteurLig.Text, SepDec) <> 0 Then KeyAscii = 0
    Else
        If Not LimitSaisie(KeyAscii) Then KeyAscii = 0
    End If
    
End Sub

Private Sub LargeurCol_GotFocus()

    Me.LargeurCol.SelStart = 0
    Me.LargeurCol.SelLength = 65535

End Sub

Private Sub LargeurCol_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then Ok_Click
    '-> V�rifier que le s�parateur d�cimal n'a pas d�ja �t� saisi
    If KeyAscii = Asc(SepDec) Then
        If InStr(1, Me.LargeurCol.Text, SepDec) <> 0 Then KeyAscii = 0
    Else
        If Not LimitSaisie(KeyAscii) Then KeyAscii = 0
    End If
    
End Sub

Private Sub NbCol_GotFocus()

    Me.NbCol.SelStart = 0
    Me.NbCol.SelLength = 65535

End Sub

Private Sub NbCol_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then Ok_Click
    If Not LimitSaisie(KeyAscii) Then KeyAscii = 0
    
End Sub

Private Sub NbLigne_GotFocus()
    
    Me.NbLigne.SelStart = 0
    Me.NbLigne.SelLength = 65535

End Sub

Private Sub NbLigne_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Ok_Click
    If Not LimitSaisie(KeyAscii) Then KeyAscii = 0
    
End Sub

Private Sub NomBlock_GotFocus()

    Me.NomBlock.SelStart = 0
    Me.NomBlock.SelLength = 65535

End Sub

Private Sub NomBlock_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Ok_Click
End Sub

Private Sub Ok_Click()
    
Dim aLb As Libelle
Dim aBlock As Block
Dim aCell As Cellule
Dim aForm As FrmBlock
Dim aFrmTab As frmTableur

    Me.NomBlock.Text = Trim(Me.NomBlock.Text)

    '-> V�rifier si toutes les zones sont saisies
    If Trim(Me.NomBlock.Text) = "" Or _
       Trim(Me.NbLigne.Text) = "" Or _
       Trim(Me.HauteurLig.Text) = "" Or _
       Trim(Me.NbCol.Text) = "" Or _
       Trim(Me.LargeurCol.Text) = "" Then
           
        Set aLb = Libelles("MESSAGE")
        MsgBox aLb.GetCaption(34), vbCritical + vbOKOnly, aLb.GetToolTip(34)
        Me.NomBlock.SetFocus
        Set aLb = Nothing
        Exit Sub
    End If
            
    '-> V�rifier si le nom est l�gal
    If Not IsLegalName(Me.NomBlock.Text) Then
        Set aLb = Libelles("MESSAGE")
        MsgBox aLb.GetCaption(5) & Chr(13) & aLb.GetCaption(6), vbCritical + vbOKOnly, aLb.GetToolTip(6)
        Me.NomBlock.SetFocus
        Set aLb = Nothing
        Exit Sub
    End If
    
    '-> V�rifier si le nom n'existe pas d�ja
    If Me.TableauCours.GetBlockExist(Me.NomBlock) Then
        Set aLb = Libelles("MESSAGE")
        MsgBox aLb.GetCaption(12), vbCritical + vbOKOnly, aLb.GetToolTip(12)
        Me.NomBlock.SetFocus
        Set aLb = Nothing
        Exit Sub
    End If
    
    '-> V�rifier le mot cl�
    If Not GetValidName(Me.NomBlock) Then
        ErrorName
        Exit Sub
    End If
    
    If InStr(1, Me.NomBlock, "-") <> 0 Then
        Set aLb = Libelles("MESSAGE")
        MsgBox aLb.GetCaption(47), vbCritical + vbOKOnly, aLb.GetToolTip(47)
        Exit Sub
    End If
    
    '-> V�rifier les valeurs <> 0
    
    If CInt(Me.NbLigne.Text) = 0 Or _
       CSng(Me.HauteurLig.Text) = 0 Or _
       CInt(Me.NbCol.Text) = 0 Or _
       CSng(Me.LargeurCol.Text) = 0 Then
           
        Set aLb = Libelles("MESSAGE")
        MsgBox aLb.GetCaption(35), vbCritical + vbOKOnly, aLb.GetToolTip(35)
        Me.NbLigne.SetFocus
        Set aLb = Nothing
        Exit Sub
    End If

    
    '-> Hauteru de ligne > 0.1
    If CSng(Me.HauteurLig.Text < 0.1) Then
        Set aLb = Libelles("FRMBLOCK")
        MsgBox aLb.GetCaption(13), vbCritical + vbOKOnly, aLb.GetToolTip(13)
        Me.HauteurLig.SetFocus
        Set aLb = Nothing
        Exit Sub
    End If
    
    If CSng(Me.LargeurCol.Text < 0.1) Then
        Set aLb = Libelles("FRMBLOCK")
        MsgBox aLb.GetCaption(12), vbCritical + vbOKOnly, aLb.GetToolTip(12)
        Me.LargeurCol.SetFocus
        Set aLb = Nothing
        Exit Sub
    End If
    
    '-> Cr�er le nouvel objet
    
    Set aBlock = New Block
    
    aBlock.Nom = Me.NomBlock.Text
    aBlock.NbCol = CInt(Me.NbCol.Text)
    aBlock.NbLigne = CInt(Me.NbLigne.Text)
    aBlock.Hauteur = CSng(Me.HauteurLig) * aBlock.NbLigne
    aBlock.Largeur = CSng(Me.LargeurCol) * aBlock.NbCol
    aBlock.AlignementLeft = 3 'Centr� au milieu
    aBlock.AlignementTop = 1 'Positionnement libre
    
    '-> Gestion des ordres d'affichage
    aBlock.IdOrdreAffichage = Me.TableauCours.AddOrdreAffichage("BL-" & Me.NomBlock.Text)
    
    '-> Ajout dans la collection
    Me.TableauCours.Blocks.Add aBlock, "BL-" & UCase$(aBlock.Nom)

    '-> Initialiser les diff�rentes matrices
    aBlock.Init
                
    '-> Pointer sur la nouvelle feuille
    Set aForm = New FrmBlock
    
    '-> Ajouter dans la collection
    Me.TableauCours.frmBlocks.Add aForm, UCase$(aBlock.Nom)
                
    '-> Cr�ation des Diff�rents objets
    For i = 1 To aBlock.NbCol
        '-> Setting des largeurs de colonne
        aBlock.SetLargeurCol i, Me.LargeurCol
        For j = 1 To aBlock.NbLigne
            '-> Setting des hauteurs de ligne
            aBlock.SetHauteurLig j, Me.HauteurLig
            '-> Cr�ation de la cellule associ�e
            Set aCell = AddCell(j, i, True, 0, 0, 0, 0)
            '-> Ajout dans la collection
            aBlock.Cellules.Add aCell, "L" & j & "C" & i
        Next
    Next
    
    '-> Sp�cifier le nom du parent
    aBlock.NomTb = Me.TableauCours.Nom
    
    '-> Ajouter l'icone dans le treeview
    aBlock.AddTreeView Me.TableauCours.Nom
        
    '-> Initialiser le block en cours dans la feuille
    Unload Me
    
    aForm.TableauCours = Me.TableauCours.Nom
    aForm.BlockCours = aBlock.Nom
    aForm.Initialisation
    aForm.Show
    aForm.BlockOut.SetFocus

    '-> Indiquer que le block est en cours d'�dition
    aBlock.IsEdit = True

    Set aLb = Nothing
    Set aBlock = Nothing
    Set aCell = Nothing
    Set aForm = Nothing
    Set aFrmTab = Nothing

    

End Sub

Private Sub Ok_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Ok_Click
End Sub
