VERSION 5.00
Begin VB.Form frmLigMax 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   1725
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   2850
   LinkTopic       =   "Form1"
   ScaleHeight     =   1725
   ScaleWidth      =   2850
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   720
      Picture         =   "TM-frmLigMax.frx":0000
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   42
      TabIndex        =   8
      Top             =   1200
      Width           =   630
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      Picture         =   "TM-frmLigMax.frx":1042
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   42
      TabIndex        =   7
      Top             =   1200
      Width           =   630
   End
   Begin VB.PictureBox Ok 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   2160
      Picture         =   "TM-frmLigMax.frx":2084
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   42
      TabIndex        =   6
      Top             =   1200
      Width           =   630
   End
   Begin VB.PictureBox SaveBt 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   1440
      Picture         =   "TM-frmLigMax.frx":30C6
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   42
      TabIndex        =   5
      Top             =   1200
      Width           =   630
   End
   Begin VB.TextBox Text2 
      Height          =   285
      Left            =   2280
      TabIndex        =   4
      Top             =   600
      Width           =   495
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   720
      TabIndex        =   2
      Top             =   600
      Width           =   495
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Label3"
      Height          =   255
      Left            =   1560
      TabIndex        =   3
      Top             =   600
      Width           =   735
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Label2"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   495
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2655
   End
End
Attribute VB_Name = "frmLIGMAX"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public NomMaq As String
Public PathApp As String
Private Sub Form_Load()

Dim aLb As Libelle
Dim Res As Long
Dim lpBuffer As String

Set aLb = Libelles("FRMLIGMAX")

Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Label2.Caption = aLb.GetCaption(3)
Me.Label3.Caption = aLb.GetCaption(4)


Set aLb = Nothing

Set aLb = Libelles("BOUTONS")
Me.Ok.ToolTipText = aLb.GetCaption(3)
Me.SaveBt.ToolTipText = aLb.GetCaption(4)
Me.Picture1.ToolTipText = aLb.GetCaption(6)
Me.Picture2.ToolTipText = aLb.GetCaption(7)

Set aLb = Nothing

'-> Chercher si un param�trage existe dans le fichier param.cfg
lpBuffer = Space$(200)
Res = GetPrivateProfileString(NomMaq, "LigMax", "", lpBuffer, Len(lpBuffer), PathApp)
If Res = 0 Then
    '-> il n'y a pas de param�trage : charger les infos de base de la maquette
    Me.Text1.Text = MaqChars("MAQCHAR").Lig
    Me.Text2.Text = MaqChars("MAQCHAR").Max
Else
    lpBuffer = Mid$(lpBuffer, 1, Res)
    '-> Charger le informations
    Me.Text1.Text = Entry(1, lpBuffer, "|")
    Me.Text2.Text = Entry(2, lpBuffer, "|")
End If

End Sub

Private Sub Ok_Click()

RetourBmp = Me.Text1.Text & "|" & Me.Text2.Text
Unload Me

End Sub

Private Sub Picture1_Click()

Dim Res As Long
Res = WritePrivateProfileString(NomMaq, vbNullString, vbNullString, PathApp)

End Sub

Private Sub Picture2_Click()

Me.Text1.Text = MaqChars("MAQCHAR").Lig
Me.Text2.Text = MaqChars("MAQCHAR").Max

End Sub

Private Sub SaveBt_Click()

Dim i As Integer
Dim KeyValue As String
Dim aLb As Libelle

Set aLb = Libelles("FRMLIGMAX")

'-> Enregistrer les segments charg�s
If Dir$(PathApp) = "" Then
    '-> Le fichier n'existe pas, le cr�er
    i = FreeFile
    Open PathApp For Output As #i
    '-> Enregistrer les informations
    Print #i, "[" & NomMaq & "]"
    Print #i, "LigMax=" & Me.Text1.Text & "|" & Me.Text2.Text
    '-> Fermer le fichier
    Close #i
Else
    '-> Le fichier existe mettre � jour la cl�
    KeyValue = Me.Text1.Text & "|" & Me.Text2.Text
    Res = WritePrivateProfileString(NomMaq, "LigMax", KeyValue, PathApp)
    If Res = 0 Then
        MsgBox aLb.GetCaption(5) & Chr(13) & PathApp, vbCritical + vbOKOnly, aLb.GetToolTip(5)
        Set aLb = Nothing
        Exit Sub
    End If
End If

MsgBox aLb.GetCaption(6), vbInformation + vbOKOnly, aLb.GetToolTip(6)
Set aLb = Nothing

End Sub

Private Sub Text1_GotFocus()
    Me.Text1.SelStart = 0
    Me.Text1.SelLength = 400
    
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
    If KeyAscii < 48 Or KeyAscii > 57 Then KeyAscii = 0
End Sub

Private Sub Text2_GotFocus()
    Me.Text2.SelStart = 0
    Me.Text2.SelLength = 400
End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)
    If KeyAscii < 48 Or KeyAscii > 57 Then KeyAscii = 0
End Sub
