VERSION 5.00
Begin VB.Form frmNewTableau 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   2985
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   3225
   ClipControls    =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2985
   ScaleWidth      =   3225
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox Text2 
      Enabled         =   0   'False
      Height          =   285
      Left            =   6240
      TabIndex        =   1
      Top             =   600
      Width           =   735
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   120
      TabIndex        =   0
      Text            =   "Text1"
      Top             =   600
      Width           =   3015
   End
   Begin VB.Frame Frame3 
      Enabled         =   0   'False
      Height          =   1335
      Left            =   120
      TabIndex        =   2
      Top             =   960
      Width           =   3015
      Begin VB.OptionButton Option1 
         Enabled         =   0   'False
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   360
         Width           =   1095
      End
      Begin VB.OptionButton Option2 
         Enabled         =   0   'False
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   840
         Width           =   1095
      End
      Begin VB.Image Portrait 
         Height          =   480
         Left            =   1440
         Picture         =   "TM-frmNewTableau.frx":0000
         Top             =   480
         Width           =   480
      End
      Begin VB.Image Paysage 
         Height          =   480
         Left            =   1440
         Picture         =   "TM-frmNewTableau.frx":0742
         Top             =   480
         Width           =   480
      End
   End
   Begin VB.Image Annuler 
      Height          =   480
      Left            =   1080
      Top             =   2400
      Width           =   630
   End
   Begin VB.Image Aide 
      Height          =   480
      Left            =   1800
      Top             =   2400
      Width           =   630
   End
   Begin VB.Image Ok 
      Height          =   480
      Left            =   2520
      Top             =   2400
      Width           =   630
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      Enabled         =   0   'False
      Height          =   255
      Left            =   5040
      TabIndex        =   6
      Top             =   600
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   2055
   End
End
Attribute VB_Name = "frmNewTableau"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then Unload Me
End Sub

Private Sub Form_Load()

Dim aLb As Libelle

'---> Gestion des messprog
Set aLb = Libelles("FRMNEWTABLEAU")

Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Text1.Text = aLb.GetCaption(3) & Maquettes("MAQUETTE").nEntries
Me.Option1.Caption = aLb.GetCaption(4)
Me.Option2.Caption = aLb.GetCaption(5)
Me.Label2.Caption = aLb.GetCaption(6)

Set aLb = Libelles("BOUTONS")

Me.Annuler.ToolTipText = aLb.GetCaption(1)
Me.Aide.ToolTipText = aLb.GetCaption(2)
Me.Ok.ToolTipText = aLb.GetCaption(3)

Me.Annuler.Picture = Turbo_Maq.Annuler.Picture
Me.Aide.Picture = Turbo_Maq.Aide.Picture
Me.Ok.Picture = Turbo_Maq.Ok.Picture

Set aLb = Libelles("FRMPROP")



If Maquettes("MAQUETTE").Orientation = aLb.GetCaption(12) Then
    Me.Option1.Value = True
Else
    Me.Option2.Value = True
End If

Set aLb = Nothing

End Sub

Private Sub Ok_Click()

Dim aLb As Libelle
Dim aMaq As Maquette
Dim aTb As Tableau
Dim aForm As frmTableur

'---> Cr�ation d'un nouveau tableau

'-> V�rif si nom de tableau saisi
Me.Text1.Text = Trim(Me.Text1.Text)
If Me.Text1.Text = "" Then
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(33), vbCritical + vbOKOnly, aLb.GetToolTip(33)
    Me.Text1.SetFocus
    Set aLb = Nothing
    Exit Sub
End If

'-> V�rifier si nom d'objet correct
If Not IsLegalName(Me.Text1.Text) Then
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(5) & Chr(13) & aLb.GetCaption(6), vbCritical + vbOKOnly, aLb.GetToolTip(6)
    Me.Text1.SetFocus
    Set aLb = Nothing
    Exit Sub
End If
    
'-> V�rifier le mot cl�
If Not GetValidName(Me.Text1.Text) Then
    ErrorName
    Exit Sub
End If
    
If InStr(1, Me.Text1.Text, "-") <> 0 Then
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(47), vbCritical + vbOKOnly, aLb.GetToolTip(47)
    Exit Sub
End If
    
'-> V�rifier que le nom n'existe pas d�ja
Set aMaq = Maquettes("MAQUETTE")
If aMaq.GetSectionExist(Me.Text1.Text) Or aMaq.GetTableauExist(Me.Text1.Text) Then
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(12), vbCritical + vbOKOnly, aLb.GetToolTip(12)
    Me.Text1.SetFocus
    Set aLb = Nothing
    Set aMaq = Nothing
    Exit Sub
End If

'-> Cr�ation du nouvel objet
Set aTb = New Tableau
Set aLb = Libelles("FRMPROP")

'-> Setting des propri�t�s
aTb.Nom = Me.Text1.Text
If Me.Option1.Value Then
    aTb.Orientation = aLb.GetCaption(12)
Else
    aTb.Orientation = aLb.GetCaption(13)
End If
'aTb.Largeur = CSng(Me.Text2.Text)
aTb.IdAffichage = aMaq.AddOrdreAffichage("TB-" & UCase$(Text1.Text))

'-> Ajout dans la collection
aMaq.Tableaux.Add aTb, UCase$(Text1.Text)

'-> intialiser la feuille qui va bien
Set aForm = New frmTableur

'-> Ajouter dans la collection des feuilles
aMaq.frmTableurs.Add aForm, UCase$(Me.Text1.Text)

'-> Ajouter l'icone dans le treeview
aTb.AddTreeView

'-> D�charger la feuille
Unload Me

'-> Afficher le nouveau tableau
Set aForm.TableauCours = aTb
Set ObjCours = aTb
aForm.Tag = UCase$(Me.Text1.Text)
aForm.Show

'-> Liberrer les pointeurs
Set aForm = Nothing
Set aMaq = Nothing
Set aLb = Nothing
Set aTb = Nothing

'-> G�rer les menus
GestMenu

End Sub

Private Sub Option1_Click()

    Me.Portrait.Visible = True
    Me.Paysage.Visible = False

End Sub

Private Sub Option2_Click()
    Me.Portrait.Visible = False
    Me.Paysage.Visible = True
End Sub

Private Sub Text1_GotFocus()
    Me.Text1.SelStart = 0
    Me.Text1.SelLength = 65535
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Ok_Click
End Sub

Private Sub Text2_GotFocus()
    Me.Text2.SelStart = 0
    Me.Text2.SelLength = 65535
End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 48 Then
        If InStr(1, Text2.Text, ".") <> 0 Then KeyAscii = 0
    Else
        If Not LimitSaisie(KeyAscii) Then KeyAscii = 0
    End If
End Sub
