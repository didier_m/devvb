Attribute VB_Name = "fctTriASCIIFile"
Dim aCol As Collection
Public Sub TriASCII(pOutil As String)

'---> Fonction tri un fichier ASCII selon un crit�re

' pOutil comptient le nom du fichier ASCII + "�" + Colonne de tri + "�" + TypeCol +  "�" + Fichier R�ponse

Dim hdlFile As Integer
Dim Ligne As String
Dim i As Long
Dim TempFileName As String
Dim FileName As String
Dim FichierReponse As String
Dim Col As Integer
Dim TypeCol As String
Dim strTri As String
Dim strNonTri As String
Dim KeyTri As String

'-> Eclater le param�trage
FileName = Entry(1, pOutil, "�")
Col = CInt(Entry(2, pOutil, "�"))
TypeCol = Entry(3, pOutil, "�")
FichierReponse = Entry(4, pOutil, "�")

'-> Initialiser la collection des lignes
Set aCol = New Collection

'-> Obtenir un fichier temporaire
TempFileName = GetTempFileNameVB("TMP")

'-> V�rifier si le fichier r�ponse est sp�cifi�
If Trim(FichierReponse) = "" Then
    End
End If

'-> Supprimer le fichier r�ponse s'il existe d�ja
If Dir$(FichierReponse) <> "" Then Kill FichierReponse

'-> V�rifier que le fichier ASCII soit rens�ign�
If FileName = "" Then
    '-> Fichier non rens�ign�
    GoTo ErrorProg
End If 'Si fichier renseign�

'-> V�rifier que le fichier ASCII existe
If Dir$(FileName) = "" Then
    GoTo ErrorProg
End If

'-> Lecture en Boucle du fichier ASCII
hdlFile = FreeFile
Open FileName For Input As #hdlFile
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> Ne pas traiter les lignes � blanc
    If Trim(Ligne) = "" Then GoTo NextLig
    '-> Ajouter dans la collection
    aCol.Add Ligne
    '-> Get de la cl� de tri
    KeyTri = Entry(Col, Ligne, "|")
    '-> Doit on convertir en entier si type Date
    Select Case TypeCol
    
        Case "DEC"
            '-> Formatter en entier
            If IsNumeric(KeyTri) Then
                KeyTri = Format(KeyTri, "000000000000000000.000000000000000")
            End If
            
        Case "DATE"
            KeyTri = CStr(CDbl(CDate(KeyTri)))
            KeyTri = Format(KeyTri, "000000000000000000.000000000000000")
                
    End Select
    '-> Ajouter dans la chaine � trier
    i = i + 1
    If strNonTri = "" Then
        strNonTri = KeyTri & "�" & i
    Else
        strNonTri = strNonTri & "|" & KeyTri & "�" & i
    End If
NextLig:
Loop

'-> Fermer le fichier
Close #hdlFile

'-> Erreur si pas de lignes
If aCol.Count = 0 Then GoTo ErrorProg

'-> Effectuer le tri de la collection
strTri = Tri(strNonTri, "|", "�")

'-> Ouverture du fichier d'origine
hdlFile = FreeFile
Open FileName For Output As #hdlFile

'-> Cr�ation du fichier retour selon l'ordre de tri
For i = 1 To NumEntries(strTri, "|")
    '-> Imprimer la ligne sp�cifi�e dans le bon fichier
    Print #hdlFile, aCol(CInt(Entry(i, strTri, "|")))
Next

'-> Fermer le fichier
Close #hdlFile

'-> Supprimer le fichier temporaire
If Dir$(TempFileName) <> "" Then Kill TempFileName

'-> Cr�er le fichier r�ponse pour indiquer que le travail est fini
hdlFile = FreeFile
Open FichierReponse For Output As #hdlFile
Print #hdlFile, "OK"
Close #hdlFile

'-> Quitter la fonction
Exit Sub



ErrorProg:
    On Error Resume Next
    '-> Fermer tous les handles et Obtenir un handle de fichier
    Reset
    hdlFile = FreeFile
    Open TempFileName For Output As #hdlFile
    '-> Ecrire une erreur
    Print #hdlFile, "ERROR"
    '-> Fermer le fichier
    Close #hdlFile
    '-> Le renommer en fichier r�ponse
    If Dir$(FichierReponse) = "" Then Kill FichierReponse
    Name TempFileName As FichierReponse
    
End Sub

