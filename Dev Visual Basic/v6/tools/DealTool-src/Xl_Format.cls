VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Xl_Format"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'*******************************************************
'* Module de classe reprennant le format d'une cellule *
'*                                                     *
'*                                                     *
'* Cl�s acc�s : Ucase$[Nom Format]                     *
'*                                                     *
'* 07/10/99                                            *
'*******************************************************

Private tBordure As Integer
'-> Nom du format
Public Name As String

'-> Variables pour restitution des bordures selon le type

Public xlLineStyle As Integer 'Type de ligne
Public XlBorderWeight As Integer 'Epaisseur de ligne

Public ColorBordure As Integer 'valeur de 0 � 56

'-> Variables pour le fond de la cellule

Public BackColor As Integer  'valeur de 0 � 56

'-> Variables de font

Public FontName As String
Public FontBold As Boolean
Public FontItalic As Boolean
Public FontUnderline As Boolean
Public FontColor As Integer 'valeur de 0 � 56
Public FontSize As Integer

'-> Variable d'alignement
Private IntAlign As Integer
Public DisplayAlign As Integer


Public Property Get TypeBordure() As Integer

    '-> Restituer le type de bordure
    TypeBordure = tBordure

End Property

Public Property Let TypeBordure(ByVal vNewValue As Integer)

    '-> Affectation de la propri�t�
    tBordure = vNewValue
    
    '-> Chargement de la valeur de xlLineStyle selon la bordure
    Select Case vNewValue
        
        Case 0
        
            xlLineStyle = xlLineStyleNone
            XlBorderWeight = xlHairline
        
        Case 1
        
            xlLineStyle = xlContinuous
            XlBorderWeight = xlHairline
                    
        Case 2
        
            xlLineStyle = xlDot
            XlBorderWeight = xlThin
        
        Case 3
        
            xlLineStyle = xlDashDotDot
            XlBorderWeight = xlThin
            
        Case 4
        
            xlLineStyle = xlDashDot
            XlBorderWeight = xlThin
            
        Case 5
        
            xlLineStyle = xlDash
            XlBorderWeight = xlThin
            
        Case 6
        
            xlLineStyle = xlContinuous
            XlBorderWeight = xlThin
            
        Case 7
        
            xlLineStyle = xlDashDotDot
            XlBorderWeight = xlMedium
            
        Case 8
        
            xlLineStyle = xlSlantDashDot
            XlBorderWeight = xlMedium
            
        Case 9
        
            xlLineStyle = xlDashDot
            XlBorderWeight = xlMedium
        
        Case 10
        
            xlLineStyle = xlDash
            XlBorderWeight = xlMedium
            
        Case 11
        
            xlLineStyle = xlContinuous
            XlBorderWeight = xlMedium
            
        Case 12
        
            xlLineStyle = xlContinuous
            XlBorderWeight = xlThick
            
        Case 13
    
            xlLineStyle = xlDouble
            XlBorderWeight = xlThick
            
    End Select 'selon le type de bordure

End Property


Public Property Get AlignementH() As Integer

    AlignementH = IntAlign

End Property

Public Property Let AlignementH(ByVal vNewValue As Integer)

    DisplayAlign = vNewValue
    Select Case vNewValue
    
        Case 0
        
            IntAlign = xlHAlignLeft
            
        Case 1
        
            IntAlign = xlHAlignRight
            
        Case 2
        
            IntAlign = xlHAlignCenter
        
    End Select

End Property
