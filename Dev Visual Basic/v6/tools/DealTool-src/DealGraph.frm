VERSION 5.00
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form DealGraph 
   BackColor       =   &H00C0C0C0&
   ClientHeight    =   5460
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   11565
   Icon            =   "DealGraph.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5460
   ScaleWidth      =   11565
   StartUpPosition =   3  'Windows Default
   Begin MSChart20Lib.MSChart Graph 
      Height          =   5415
      Left            =   0
      OleObjectBlob   =   "DealGraph.frx":030A
      TabIndex        =   0
      Top             =   0
      Width           =   11535
   End
   Begin VB.Menu mnuImprimer 
      Caption         =   "Imprimer"
   End
   Begin VB.Menu mnuVue 
      Caption         =   "Vue"
      Begin VB.Menu mnu2D 
         Caption         =   "2D"
         Begin VB.Menu mnuBarres2D 
            Caption         =   "Barres"
         End
         Begin VB.Menu mnuLignes2D 
            Caption         =   "Lignes"
         End
         Begin VB.Menu mnuSurfaces2D 
            Caption         =   "Surfaces"
         End
         Begin VB.Menu mnuSecteurs2D 
            Caption         =   "Secteurs"
         End
         Begin VB.Menu mnuEscaliers2D 
            Caption         =   "Escaliers"
         End
      End
      Begin VB.Menu mnu3D 
         Caption         =   "3D"
         Begin VB.Menu mnuBarres3D 
            Caption         =   "Barres"
         End
         Begin VB.Menu mnuLignes3D 
            Caption         =   "Lignes"
         End
         Begin VB.Menu mnuSurfaces3D 
            Caption         =   "Surfaces"
         End
         Begin VB.Menu mnuEscaliers3D 
            Caption         =   "Escaliers"
         End
      End
   End
   Begin VB.Menu mnuBackColor 
      Caption         =   "BackColor"
      Begin VB.Menu mnuGrisClair 
         Caption         =   "Gris Clair"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuBlanc 
         Caption         =   "Blanc"
      End
   End
End
Attribute VB_Name = "DealGraph"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()

'Me.Graph.Width = Me.Width - DifX
'Me.Graph.Height = Me.Height - DifY


End Sub

Private Sub Form_Resize()

On Error Resume Next
    
    Me.Graph.Width = Me.Width - DifX
    Me.Graph.Height = Me.Height - DifY


End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub mnuBarres2D_Click()

    Me.Graph.ChartType = VtChChartType2dBar

End Sub

Private Sub mnuBarres3D_Click()

    Me.Graph.ChartType = VtChChartType3dBar
    
End Sub

Private Sub mnuBlanc_Click()

    '-> D�cocher les autres menus
    Me.mnuGrisClair.Checked = False

    '-> Cocher le blanc
    Me.mnuBlanc.Checked = True
    Me.BackColor = &HFFFFFF
    

End Sub

Private Sub mnuEscaliers2D_Click()

    Me.Graph.ChartType = VtChChartType2dStep

End Sub

Private Sub mnuEscaliers3D_Click()

    Me.Graph.ChartType = VtChChartType3dStep
    
End Sub

Private Sub mnuGrisClair_Click()

'-> D�cocher les autres menus
    Me.mnuBlanc.Checked = False

    '-> Cocher le blanc
    Me.mnuGrisClair.Checked = True
    Me.BackColor = 12632256
    
End Sub

Private Sub mnuImprimer_Click()

    PrintList.Show vbModal

End Sub

Private Sub mnuLignes2D_Click()
    
    Me.Graph.ChartType = VtChChartType2dLine

End Sub

Private Sub mnuLignes3D_Click()

    Me.Graph.ChartType = VtChChartType3dLine
    
End Sub

Private Sub mnuSecteurs2D_Click()

    Me.Graph.ChartType = VtChChartType2dPie
    
End Sub

Private Sub mnuSurfaces2D_Click()

    Me.Graph.ChartType = VtChChartType2dArea
    
End Sub

Private Sub mnuSurfaces3D_Click()

    Me.Graph.ChartType = VtChChartType3dArea
    
End Sub
