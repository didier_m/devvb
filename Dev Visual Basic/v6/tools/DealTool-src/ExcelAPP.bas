Attribute VB_Name = "ExcelAPP"
'***********************************************************************
'* Module de gestion des fonctions Excel par import d'un fichier Ascii *
'***********************************************************************

'-> Variable pour collection des formats excel
Public xl_Formats As Collection

'-> Variable de r�cup�ration du fichier d�tail
Private DetailFic As String
Private SepFic As String 'S�parateur du fichier pour r�partition des colonnes
Private nbCol As Variant 'Nombre de colonne pour d�callage du format via Offset (Excel8.OLB)
Private AutoAjust As Boolean 'Indique si ajustement des colonnes par d�faut
Private CellDep As String 'Adresse de la cellule de d�part
Private FormatEntete As String 'Nom du format � appliquer pour l'entete
Private FormatLigne As String 'Nom du format � appliquer pour les lignes
Private NomFeuille As String 'Nom de la feuille
Private Formattage As Boolean 'indique si on applique le format d�sir�
Private ClasseurRef As String 'Nom d'un classeur mod�le
Private FormatCol() As Integer 'Liste des formats � appliquer en colonne
Private WidthCol() As Long 'Liste des largeurs colonnes
Private FindWidthCol As Boolean
Private AutoFilter As Boolean 'Indique si on doit appliquer un filtre automatique
Private SetName As String 'Indique si diff�rent de "" que l'on doit donner un nom � la plage de donn�es
Private SetCodeLangue As Integer 'Indiquer le code langue � utliser pour les exports
Private FichierLangue As String 'Fichier Langue
Dim KillSpool As Boolean 'Indique si on doit supprimer les spools apr�s traitement
Dim RunMacro As String 'Indique que l'on doit lancer une macro dans le classeur
Dim OpenFile As String

'-> openoffice
Dim aDesktop As Object
Dim aSpreadSheet As Object
Dim pNomFeuille As Boolean

'-> Indique si on doit se servir de la premi�re ligne de d�tail pour indiquer le nombre de colonnes
Private FindNbCol As Boolean

Private toLock As Boolean

Public Sub OpenEntete(ByVal NomFichier As String)
'-> Cette proc�dure ouvre le descriptif de fichier Entete et met � jour les
' diff�rents param�tres
Dim Ligne As String
Dim NumFic As Integer
Dim i As Long
Dim Tempo As String
Dim FindFormatCol As Boolean
Dim CommunPath As String
Dim ToFormatCol As Boolean
Dim DestinationFile As String

'-> Test si fichier existant
If Dir$(NomFichier) = "" Then
    MsgBoxMouchard "Impossible de trouver le fichier d'entete" & NomFichier, vbCritical + vbOKOnly, "Erreur"
    End
End If

'-> Initialiser la collection des formats de cellule
Set xl_Formats = New Collection

'-> Ouverture du fichier pour lecture
NumFic = FreeFile
Open NomFichier For Input As #NumFic

'-> De base supprimer le fichier detail
KillSpool = True

'-> De base on postionner le code langue en fran�ais
SetCodeLangue = 1

'-> Lecture du fichier
Do While Not EOF(NumFic)

    '-> Lecture d'une ligne
    Line Input #NumFic, Ligne
    
    If Trim(Ligne) = "" Then
    Else
    
        '- Ne lire que les lignes qui commencent par un "\"
        If Mid$(Ligne, 1, 1) = "\" Then
        
            Select Case UCase$(Entry(1, Ligne, "$"))
                Case "\FORMULAIRE"
                Case "\CLASSEUR"
                    '-> modif didier car dans le chemin on peut avoir $
                    'ClasseurRef = Entry(2, Ligne, "$")
                    ClasseurRef = Mid(Ligne, InStr(1, Ligne, "$") + 1)
                Case "\SETNAME"
                    SetName = Entry(2, Ligne, "$")
                Case "\FORMATAGE"
                    Formattage = CBool(Entry(2, Ligne, "$"))
                Case "\CODELANGUE"
                    SetCodeLangue = CInt(Entry(2, Ligne, "$"))
                Case "\NOMFEUILLE"
                    NomFeuille = Entry(2, Ligne, "$")
                Case "\FICHIERDETAIL"
                    DetailFic = Entry(2, Ligne, "$")
                    If Dir$(DetailFic) = "" Then
                        MsgBoxMouchard "Impossible de trouver le fichier d�tail :" & DetailFic, vbCritical + vbOKOnly, "Erreur"
                        End
                    End If
                Case "\SEPARATEUR"
                    SepFic = Entry(2, Ligne, "$")
                Case "\NBCOL"
                    nbCol = Entry(2, Ligne, "$")
                    If Trim(nbCol) = "" Or CInt(nbCol) > 255 Then
                        MsgBoxMouchard "Nombre de colonnes incorrect :" & Ligne, vbCritical + vbOKOnly, "Erreur"
                        End
                    End If
                    nbCol = CInt(nbCol)
                    FindNbCol = True
                    
                Case "\FORMATCOL"
                
                    ReDim FormatCol(1 To nbCol)
                    Ligne = Entry(2, Ligne, "$")
                    For i = 1 To nbCol
                        '-> Recup�ration de la description du format
                        Tempo = Entry(i, Ligne, "|")
                        Select Case Trim(UCase$(Tempo))
                            Case "CHA"
                                FormatCol(i) = 1
                            Case "DAT"
                                FormatCol(i) = 2
                            Case "DEC"
                                FormatCol(i) = 4
                                ToFormatCol = True
                            Case "INT"
                                FormatCol(i) = 3
                                ToFormatCol = True
                            Case Else
                                FormatCol(i) = 1
                        End Select
                    Next
                    FindFormatCol = True
                
                Case "\WIDTHCOL"
                
                    ReDim WidthCol(1 To nbCol)
                    Ligne = Entry(2, Ligne, "$")
                    For i = 1 To nbCol
                        '-> Recup�ration de la description du format
                        Tempo = Entry(i, Ligne, "|")
                        WidthCol(i) = Tempo
                    Next
                    FindWidthCol = True
                
                Case "\AUTOAJUST"
                
                    If UCase$(Entry(2, Ligne, "$")) = "VRAI" Then
                        AutoAjust = True
                    Else
                        AutoAjust = False
                    End If
                    
                Case "\FILTRE"
                
                    If UCase$(Entry(2, Ligne, "$")) = "VRAI" Then
                        AutoFilter = True
                    Else
                        AutoFilter = False
                    End If
                
                    
                Case "\CELLULEDEPART"
                
                    CellDep = Entry(2, Ligne, "$")
                    If Trim(CellDep) = "" Then
                        MsgBoxMouchard "Cellule de d�part incorrecte :" & Ligne, vbCritical + vbOKOnly, "Erreur"
                        End
                    End If
                                
                Case "\FORMATENTETE"
                
                    FormatEntete = Entry(2, Ligne, "$")
                    If Trim(FormatEntete) = "" Then FormatEntete = "EnteteDefaut"
                    AnalyseFormat FormatEntete
                                                    
                Case "\FORMATLIGNE"
                    
                    FormatLigne = Entry(2, Ligne, "$")
                    If Trim(FormatLigne) = "" Then FormatLigne = "LigneDefaut"
                    AnalyseFormat FormatLigne
                    
                Case "\KILL"
                    If Trim(Entry(2, Ligne, "$")) = "0" Then KillSpool = False
                    
                Case "\RUNMACRO"
                    If Trim(Entry(2, Ligne, "$")) <> "" Then
                        RunMacro = Entry(2, Ligne, "$")
                    Else
                        RunMacro = ""
                    End If
                Case "\DESTINATION"
                    DestinationFile = Entry(2, Ligne, "$")
                Case "\OPEN"
                    OpenFile = Entry(2, Ligne, "$")
            End Select 'selon le type de ligne
            
        End If 'Si ligne commence par "\"
        
    End If 'Si ligne � blanc

Loop 'Boucle d'analyse

'-> Fermer les divers fichiers
Reset


'-> Cr�taion du path du fichier Langue
'i = InStr(UCase$(App.Path), "\EMILIEGUI\") PIERROT V6
CommunPath = Replace(App.Path, "exe", "mqt")

'CommunPath = Mid$(App.Path, 1, i + 10) & "Mqt\Dealtool-" PIERROT V6
CommunPath = CommunPath & "\Dealtool-"


FichierLangue = CommunPath & Format(SetCodeLangue, "00") & ".lng"

If Dir$(CommunPath & Format(SetCodeLangue, "00") & ".lng") <> "" Then
    'FichierLangue = Mid$(App.Path, 1, i + 10) & "Mqt\Dealtool-" & Format(SetCodeLangue, "00") & ".lng"
    FichierLangue = CommunPath & Format(SetCodeLangue, "00") & ".lng"
Else
    'FichierLangue = Mid$(App.Path, 1, i + 10) & "Mqt\Dealtool-01.lng"
    FichierLangue = CommunPath & "01.lng"
End If

If Not FindFormatCol Then
    ToFormatCol = False
    ReDim FormatCol(1 To nbCol)
    For i = 1 To nbCol
        FormatCol(i) = 1
    Next
End If

'-> Si on doit , reformatter les colonnes necessaires
If ToFormatCol Then Call FormatNumValue

'-> Supprimer le fichier d'ordre
On Error Resume Next

'-> Supprimer le fichier d'entete
If Trim(NomFichier) <> "" Then
    If KillSpool Then If Dir$(NomFichier, vbNormal) <> "" Then Kill NomFichier
End If

'-> Lancer Excel (si pas present on essaye de lancer l'export openoffice
If IsExcel Then
    LinkExcel DestinationFile
Else
    If IsOpenoffice Then LinkOpenOffice (DestinationFile)
End If

End Sub

Public Function IsExcel() As Boolean

'---> Cette proc�dure essaye de cr�er un lien OLE vers Excel pour tester que les _
bibli sont install�es

Dim aExcel As Object

On Error GoTo ExcelError

'-> Essayer de cr�er un lien OLE
Set aExcel = CreateObject("Excel.Application")

'-> Lib�rer le pointeur
Set aExcel = Nothing

'-> Renvoyer une valeur de succ�s
IsExcel = True

Exit Function

ExcelError:
    '-> Si erreur : pas Excel
    IsExcel = False

End Function

Public Function IsOpenoffice() As Boolean
'--> Cette proc�dure essaye de cr�er un lien OLE vers Openoffice pour tester que les bibli sont install�es
Dim aServiceManager As Object

On Error GoTo OfficeError

'-> Essayer de cr�er un lien OLE
Set aServiceManager = CreateObject("com.sun.star.ServiceManager")

'-> Lib�rer le pointeur
Set aServiceManager = Nothing

'-> Renvoyer une valeur de succ�s
IsOpenoffice = True

Exit Function

OfficeError:
    '-> Si erreur : pas openoffice
    IsOpenoffice = False

End Function

Private Sub FormatNumValue()

'---> Cette procedure applique un format � la data dans un fichier d�tail

Dim hdlFileRef As Integer
Dim hdlFile As Integer
Dim TempFileName As String
Dim Ligne As String
Dim i As Long
Dim LigneDest As String
Dim ValueCell As String

On Error Resume Next

'-> R�cup�rer un fichier Temporaire
TempFileName = GetTempFileNameVB("EXC")

'-> Ouvrir le fichier que l'on vient de cr�er
hdlFile = FreeFile
Open TempFileName For Output As #hdlFile

'-> Ouverture du fichier de r�f�rence
hdlFileRef = FreeFile
Open DetailFic For Input As #hdlFileRef

'-> Retraitement de tout le fichier
Do While Not EOF(hdlFileRef)
    '-> Raz de la ligne de destination
    LigneDest = ""
    '-> Lecture de la ligne
    Line Input #hdlFileRef, Ligne
    '-> Ne pas traiter les lignes � blanc
    If Trim(Ligne) = "" Then GoTo NextRow
    '-> Analyse de la ligne
    For i = 1 To nbCol
        '-> Get de la valeur
        ValueCell = Entry(i, Ligne, SepFic)
        '-> Ne retraiter que si format d�cimal
        If FormatCol(i) = 3 Or FormatCol(i) = 4 Then
            '-> Convertir la valeur
            ValueCell = Trim(Convert(ValueCell))
        End If
        '-> Rajouter dans la ligne en cours
        If Trim(LigneDest) = "" Then
            LigneDest = ValueCell
        Else
            LigneDest = LigneDest & SepFic & ValueCell
        End If
    Next
    '-> Imprimer dans le fichier destination
    Print #hdlFile, LigneDest
NextRow:
Loop 'Pour tous le fichier d'origine

'-> Fermer les 2 fichiers
Close #hdlFile
Close #hdlFileRef

'-> supprimer l'ancien fichier d�tail
Kill DetailFic

'-> Copier le nouveau fichier
Name TempFileName As DetailFic

End Sub


Public Sub AnalyseFormat(ByVal NomFormat As String)

'---> Proc�dure qui recherche et cr�� la classe format en fonction de son param�trage

Dim ListeCle As String
Dim xlF As Xl_Format
Dim Res As Long
Dim i As Long
Dim DefCle As String
Dim NomCle As String
Dim ValueCle As String


Dim TempFileName As String

On Error GoTo ErrorFormat

'-> Il faut obtenir un nom de fihcier temporaire pour ouverture du fichier .ini PB WIN95
TempFileName = GetTempFileNameVB("LIB")
Res = CopyFile(App.Path & "\DealTool1.lib", TempFileName, 0)

'-> Initialisation du buffer
ListeCle = Space$(1000)
Res = GetPrivateProfileSection(NomFormat, ListeCle, Len(ListeCle), TempFileName)

If Res = 0 Then
    MsgBoxMouchard "Impossible de lire le format sp�cifi� : " & NomFormat & ".Aucun format ne sera appliqu�.", vbCritical + vbOKOnly, "Erreur de format"
Else
    '-> Cr�er une nouvelle classe
    Set xlF = New Xl_Format
    xlF.Name = NomFormat
    '-> Formater la liste
    ListeCle = Mid$(ListeCle, 1, Res - 1)
    If Trim(ListeCle) <> "" Then
        '-> r�cup�ration des diff�rentes cl�s
        For i = 1 To NumEntries(ListeCle, Chr(0))
            '-> R�cup�ration de la d�finition de la cle
            DefCle = Entry(i, ListeCle, Chr(0))
            NomCle = Entry(1, DefCle, "=")
            ValueCle = Entry(2, DefCle, "=")
            '-> Cr�ation de la valeur
            Select Case UCase$(NomCle)
                    
                Case "TYPEBORDURE"
                
                    xlF.TypeBordure = CInt(ValueCle)
                    
                Case "COULEURBORDURE"
                    
                    xlF.ColorBordure = CInt(ValueCle)
                    
                Case "BACKCOLOR"
                    
                    xlF.BackColor = CInt(ValueCle)
                    
                Case "FONTNAME"
                    
                    xlF.FontName = ValueCle
                    
                Case "FONTSIZE"
                
                    xlF.FontSize = CInt(ValueCle)
                    
                Case "FONTCOLOR"
                
                    xlF.FontColor = CInt(ValueCle)
                
                Case "FONTBOLD"
                    If UCase$(Trim(ValueCle)) = "FAUX" Then
                        xlF.FontBold = False
                    Else
                        xlF.FontBold = True
                    End If
                                    
                Case "FONTITALIC"
                
                    If UCase$(Trim(ValueCle)) = "FAUX" Then
                        xlF.FontItalic = False
                    Else
                        xlF.FontItalic = True
                    End If
                
                Case "FONTUNDERLINE"
                
                    If UCase$(Trim(ValueCle)) = "FAUX" Then
                        xlF.FontUnderline = False
                    Else
                        xlF.FontUnderline = True
                    End If
                    
                
                Case "ALIGNEMENT"
                
                    xlF.AlignementH = CInt(ValueCle)
            
            End Select 'selon la nature de la cl�
            
        Next 'pour toutes les cl�s
    End If 'si valeur <> ""
    
    '-> Ajouter le nouveau format
    xl_Formats.Add xlF, UCase$(NomFormat)
    
    '-> Lib�rer le pointeur
    Set xlF = Nothing
    
End If 'Si on trouve la rubrique

If Dir$(TempFileName, vbNormal) <> "" Then Kill TempFileName

Exit Sub

ErrorFormat:

    MsgBoxMouchard "Erreur durant la lecture des diff�rents formats." & vbClrf & Err.Number & " -> " & Err.Description, vbCritical + vbOKOnly, "Erreur fatale"
    End

End Sub

Private Sub SetSheetName(aFeuille As Object, strName As String)

On Error Resume Next

aFeuille.Name = Trim(strName)


End Sub

Private Sub RunMacroInExcelApp(ExcelApp As Object)

'---> Cette procedure lance une macro dans un classeur Excel

On Error GoTo GestError

'-> Essayer de lancer la macro
ExcelApp.Run RunMacro

Exit Sub


GestError:
    

End Sub

Private Sub LinkExcel(Optional DestinationFile As String)

'-> Proc�dure qui r�alise le lien avec Excel
Dim ExcelApp As Object
Dim Classeur As Object
Dim Feuille As Object
Dim aRange As Object
Dim xlF As Xl_Format
Dim Res As Long
Dim lpBuffer As String
Dim ActiveCol As Long
Dim ActiveRow As Long
Dim aRange2 As Object
Dim NbLigne As Long
Dim FindSheet As Boolean
Dim HideSheet As Integer
Dim hdlFile As Integer
Dim Ligne As String
Dim RowPos As Long
Dim ColPos As Long
Dim i As Long
Dim ValueField As String
Dim nbDec As String
Dim iPos As Long

On Error GoTo EndError

'-> Afficher la feuille de temporisation
If Trim(DestinationFile) = "" Or OpenFile <> "" Then
    DealAnim.Caption = GetIniFileValue("LANGUE", "1", FichierLangue)
    DealAnim.Label1 = Replace(GetIniFileValue("LANGUE", "2", FichierLangue), "$FILE$", DetailFic)
    DealAnim.Show
    DoEvents
End If

'-> Ouverture d'excel et cr�ation des objets
Set ExcelApp = CreateObject("Excel.Application")
ExcelApp.DisplayAlerts = False
DoEvents

If Trim(ClasseurRef) = "" Or Dir$(ClasseurRef) = "" Then
    On Error Resume Next
    Set Classeur = ExcelApp.Workbooks.Add
    If Trim(DestinationFile) = "" Then
        DealAnim.Label1.Caption = GetIniFileValue("LANGUE", "4", FichierLangue)
        DoEvents
    End If
    '-> Suppression des feuilles en trop
    Set Feuille = Classeur.Worksheets(2)
    Feuille.Delete
    DoEvents
    Set Feuille = Classeur.Worksheets(2)
    Feuille.Delete
    DoEvents
    '-> S�lectionner la feuille Feuil1
    Set Feuille = Classeur.Worksheets(1)
    '-> Maj du nom
    If Trim(NomFeuille) <> "" Then SetSheetName Feuille, NomFeuille
    
    On Error GoTo EndError

Else
    
    '-> Ouverture du classeur de r�f�rence
    Set Classeur = ExcelApp.Workbooks.Open(ClasseurRef)
    
    '-> Si le nom de la feuille est sp�cifi�e
    If Trim(NomFeuille) <> "" Then
        FindSheet = False
        '-> V�rifier si on a trouv� la feuille sp�cifi�e
        For Each Feuille In Classeur.Worksheets
            '-> Tester si correspondance
            If UCase$(Trim(NomFeuille)) = UCase$(Trim(Feuille.Name)) Then
                FindSheet = True
                Exit For
            End If
        Next
        If Not FindSheet Then
            '-> Si on n' pas trouv� la feuille sp�cifi�e, ajouter une feuille et leui donn� le nom
            Set Feuille = Classeur.Worksheets.Add
            SetSheetName Feuille, NomFeuille
        End If
    End If
    
End If

'-> V�rification si feuille masqu�e ou non
HideSheet = Feuille.Visible
Feuille.Visible = -1
Feuille.Activate

'-> S�lection de la cellule active
If Trim(CellDep) = "" Then CellDep = "A1"

'-> S�lectionner la cellule de d�part
ExcelApp.Application.Range(CellDep).Select

'-> R�cup�rer la ligne de d�part
RowPos = ExcelApp.Application.ActiveCell.Row
ColPos = ExcelApp.Application.ActiveCell.Column

'-> Ouverture du fichier
hdlFile = FreeFile
Open DetailFic For Input As #hdlFile

'-> Lecture s�quentielle du fichier
Do While Not EOF(hdlFile)
    '-> Mettre � jour le libelle�
    DealAnim.Label1 = Replace(GetIniFileValue("LANGUE", "8", FichierLangue), "$FILE$", DetailFic) & " " & NbLigne
    DoEvents
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> Redispatcher les colonnes
    For i = 1 To nbCol
        '-> get de la valeur
        ValueField = Entry(i, Ligne, SepFic)
        '-> rajout de ' ' si commence par '=' Didier
        If Len(ValueField) > 2 Then
            If Mid(ValueField, 1, 1) = "=" Then ValueField = " " & ValueField
            If Mid(ValueField, 1, 2) = ".=" Then
                ValueField = Mid(ValueField, 2)
                '-> on se debranche sur la gestion du format de la cellule
                ValueField = StyleCellule(Feuille.cells(RowPos + NbLigne, ColPos + i - 1), ValueField)
            End If
        End If
        '-> Poser le Format
        If FormatCol(i) = 3 Or FormatCol(i) = 4 Then
            If IsNumeric(ValueField) And InStr(1, ValueField, "E", vbTextCompare) = 0 Then
                If FormatCol(i) = 4 Then
                    '-> R�cup�rer la partie d�cimale
                    iPos = InStr(1, ValueField, SepDec)
                    If iPos <> 0 Then
                        iPos = Len(ValueField) - iPos
                        If iPos <> 0 Then
                            nbDec = "0." & String(iPos, "0")
                        Else
                            nbDec = "0"
                        End If
                    Else
                        nbDec = "0"
                    End If
                Else
                    nbDec = "0"
                End If
                Feuille.cells(RowPos + NbLigne, ColPos + i - 1) = CDec(Entry(i, Ligne, SepFic))
                Feuille.cells(RowPos + NbLigne, ColPos + i - 1).NumberFormat = nbDec
            Else
                Feuille.cells(RowPos + NbLigne, ColPos + i - 1) = Entry(i, Ligne, SepFic)
            End If
        Else
            If FormatCol(i) = 2 Then
                If IsDate(ValueField) Then
                    Feuille.cells(RowPos + NbLigne, ColPos + i - 1) = CDate(Trim(Entry(i, Ligne, SepFic)))
                Else
                    Feuille.cells(RowPos + NbLigne, ColPos + i - 1) = Entry(i, Ligne, SepFic)
                End If
            Else
                If IsDate(ValueField) Then
                    Feuille.cells(RowPos + NbLigne, ColPos + i - 1).formular1c1 = " " & ValueField 'Entry(i, Ligne, SepFic) didier
                Else
                    Feuille.cells(RowPos + NbLigne, ColPos + i - 1).formular1c1 = ValueField 'Entry(i, Ligne, SepFic) didier
                End If
            End If
        End If
    Next 'Pour toutes les colonnes
    '-> Incr�menter le compteur de ligne
    NbLigne = NbLigne + 1
Loop

'-> pour la largeur des colonnes
On Error Resume Next
If FindWidthCol Then
    For i = 1 To nbCol
        Feuille.Columns(i).ColumnWidth = WidthCol(i)
    Next
End If
On Error GoTo EndError

'-> Fermer le fichier
Close #hdlFile

'-> Si on doit donner un nom
If SetName <> "" Then
    '-> S�lectionner la plage entiere des donn�es
    Set aRange = ExcelApp.Application.Range(ExcelApp.Application.ActiveCell, ExcelApp.Application.ActiveCell.Offset(ExcelApp.Application.selection.Areas.Item(1).Rows.Count - 1, nbCol - 1))
    aRange.Select
    '-> Donner un nom
    SetGoodName aRange
End If

DoEvents

If Formattage Then
    '-> Faire la mise en forme
    DealAnim.Label1.Caption = GetIniFileValue("LANGUE", "7", FichierLangue)

    'D�callage de la cellule active pour formattage des lignes de d�tail
    Set aRange = ExcelApp.Application.ActiveCell.CurrentRegion
    Set aRange = aRange.Offset(1, 0).Resize(aRange.Rows.Count - 1, aRange.Columns.Count)
    
    'Formattage des cellules de d�tail
    If Trim(FormatLigne) <> "" Then
        Set xlF = xl_Formats(UCase$(FormatLigne))
        Call ApplicFormat(xlF, aRange)
    End If
    
    '-> Formattage de la ligne d'entete
    Set aRange = Feuille.Range(CellDep)
    aRange.Activate
    
    '-> S�lectionner l'entete
    Set aRange = ExcelApp.Application.ActiveCell.CurrentRegion
    Set aRange = aRange.Resize(1, aRange.Columns.Count)

    '-> Appliquer le tri automatique des donn�es si necessaire
    If AutoFilter Then aRange.AutoFilter

    'Formattage des cellules de l'entete si on a sp�cifi� ou non une value
    If FormatEntete <> "" Then
        Set xlF = xl_Formats(UCase$(FormatEntete))
        Call ApplicFormat(xlF, aRange)
    End If
End If 'Si on doit formatter

'-> Redimensionnement automatique des colonnes
If AutoAjust Then
    If Trim(CellDep) <> "" Then Feuille.Range(CellDep).Activate
    ActiveCol = ExcelApp.Application.ActiveCell.Column
    For i = ActiveCol To ActiveCol + nbCol
        ExcelApp.Application.Columns(i).Activate
        ExcelApp.Application.Columns(i).EntireColumn.AutoFit
    Next
    ActiveRow = ExcelApp.Application.ActiveCell.Row
    For i = ActiveCol To ActiveCol + NbLigne
        ExcelApp.Application.Rows(i).Activate
        ExcelApp.Application.Rows(i).EntireRow.AutoFit
    Next
End If

'-> S�lectionner la cellule de d�part
If Trim(CellDep) <> "" Then Feuille.Range(CellDep).Activate

'-> Ex�cuter une macro si necessaire
If RunMacro <> "" Then RunMacroInExcelApp ExcelApp

'-> Masquer la feuille si necessaire
Feuille.Visible = HideSheet
    
'-> si on doit proteger la feuille
If toLock Then
    Feuille.protect PassWord:="deal", DrawingObjects:=True, Contents:=True, Scenarios:=True
End If
    
    
'-> Rendre Excel visible si demand� - PIERROT : modif pour generation de fichiers XL sans affichage de XL
If Trim(DestinationFile) <> "" Then
    '-> On sauvegarde le fichier au format XLS
    ExcelApp.Workbooks(1).SaveAs DestinationFile
    If OpenFile <> "" Then
        ExcelApp.Visible = True
        ExcelApp.DisplayAlerts = True
    Else
        ExcelApp.Workbooks(1).Close
    End If
Else
    ExcelApp.Visible = True
    ExcelApp.DisplayAlerts = True
End If

'-> Supprimer le fichier des donn�es
If Trim(DetailFic) <> "" Then
    If KillSpool Then If Dir$(DetailFic, vbNormal) <> "" Then Kill DetailFic
End If

EndError:
    
    
    If Err.Number <> 0 Then MsgBoxMouchard Err.Number & " -> " & Err.Description

    '-> Lib�rer les pointeurs
    Set Feuille = Nothing
    Set Classeur = Nothing
    Set ExcelApp = Nothing
    Set aRange = Nothing
    
    '-> d�charger la feuille tempo
    Unload DealAnim


End Sub

Private Function StyleCellule(cellule As Object, strValue As String) As String
'--> cette fonction va permettre d'appliquer un style a la cellule
If InStr(1, strValue, "fctdeal", vbTextCompare) <> 0 Then
    '-> on se vide la variable pour ne pas tout s'ecraser
    FormatLigne = ""
    '-> on applique la valeur
    '-> on applique la fusion
    If InStr(1, strValue, "�ME=") <> 0 Then
        cellule.Parent.Range(cellule.Parent.cells(cellule.Row, cellule.Column), cellule.Parent.cells(Val(Entry(1, Entry(1, Entry(1, Mid(Entry(2, strValue, "�ME="), 4), "�"), ")"), ",")), Val(Entry(2, Entry(1, Entry(1, Mid(Entry(2, strValue, "�ME="), 4), "�"), ")"), ",")))).mergecells = True
    End If
    If InStr(1, strValue, "V=") <> 0 Then
        If NumEntries(strValue, ")") = 1 Then
            StyleCellule = Entry(1, Entry(1, Mid(Entry(2, strValue, "V="), 2), "�"), ")")
        Else
            StyleCellule = Entry(1, Mid(Entry(2, strValue, "V="), 2), "�")
        End If
        StyleCellule = Replace(StyleCellule, "�", ";")
    End If
    '-> on applique le nom de la police
    If InStr(1, strValue, "�FN=") <> 0 Then
        cellule.Font.Name = Entry(1, Entry(1, Mid(Entry(2, strValue, "�FN="), 4), "�"), ")")
    End If
    
    '-> on applique un commentaire
    If InStr(1, strValue, "�TI=") <> 0 Then
        cellule.AddComment
        cellule.Comment.Visible = False
        cellule.Comment.Text Text:="" & Entry(1, Entry(1, Mid(Entry(2, strValue, "�TI="), 4), "�"), ")")
    End If
    '-> on applique la taille de la police
    If InStr(1, strValue, "�FS=") <> 0 Then
        cellule.Font.Size = Entry(1, Entry(1, Mid(Entry(2, strValue, "�FS="), 4), "�"), ")")
    End If
    '-> on applique la couleur
    If InStr(1, strValue, "�FC=") <> 0 Then
        cellule.Font.ColorIndex = CDbl(Entry(1, Entry(1, Mid(Entry(2, strValue, "�FC="), 4), "�"), ")"))
    End If
    '-> on applique un format aux nombres
    If InStr(1, strValue, "�FO=") <> 0 Then
        cellule.NumberFormat = Entry(1, Entry(1, Mid(Entry(2, strValue, "�FO="), 4), "�"), ")")
    End If
    '-> on applique l allignement
    If InStr(1, strValue, "�AL=") <> 0 Then
        Select Case CDbl(Entry(1, Entry(1, Mid(Entry(2, strValue, "�AL="), 4), "�"), ")"))
            Case 1
                cellule.VerticalAlignment = -4160
                cellule.HorizontalAlignment = -4131
            Case 2
                cellule.VerticalAlignment = -4160
                cellule.HorizontalAlignment = -4108
            Case 3
                cellule.VerticalAlignment = -4160
                cellule.HorizontalAlignment = -4152
            Case 4
                cellule.VerticalAlignment = -4108
                cellule.HorizontalAlignment = -4131
            Case 5
                cellule.VerticalAlignment = -4108
                cellule.HorizontalAlignment = -4108
            Case 6
                cellule.VerticalAlignment = -4108
                cellule.HorizontalAlignment = -4152
            Case 7
                cellule.VerticalAlignment = -4107
                cellule.HorizontalAlignment = -4131
            Case 8
                cellule.VerticalAlignment = -4107
                cellule.HorizontalAlignment = -4108
            Case 9
                cellule.VerticalAlignment = -4107
                cellule.HorizontalAlignment = -4152
        End Select
    End If
    '-> on applique le style italique
    If InStr(1, strValue, "�FI=") <> 0 Then
        cellule.Font.Italic = True
    End If
    '-> on applique le style gras
    If InStr(1, strValue, "�FB=") <> 0 Then
        cellule.Font.Bold = True
    End If
    '-> on applique le style souligne
    If InStr(1, strValue, "�FU=") <> 0 Then
        cellule.Font.Underline = True
    End If
    '-> on applique la couleur de fond
    If InStr(1, strValue, "�BC=") <> 0 Then
        cellule.Interior.ColorIndex = CDbl(Entry(1, Entry(1, Mid(Entry(2, strValue, "�BC="), 4), "�"), ")"))
        cellule.Interior.Pattern = 1
    End If
    '-> on applique le retour a la ligne
    If InStr(1, strValue, "�WR=") <> 0 Then
        cellule.WrapText = True
    End If
    '-> on applque les bordures
    If InStr(1, strValue, "�BL=") <> 0 Then
        cellule.borders(7).LineStyle = 1
        cellule.borders(7).Color = CDbl(Entry(1, Entry(1, Mid(Entry(2, strValue, "�BL="), 4), "�"), ")"))
    End If
    If InStr(1, strValue, "�BT=") <> 0 Then
        cellule.borders(8).LineStyle = 1
        cellule.borders(8).Color = CDbl(Entry(1, Entry(1, Mid(Entry(2, strValue, "�BT="), 4), "�"), ")"))
    End If
    If InStr(1, strValue, "�BB=") <> 0 Then
        cellule.borders(9).LineStyle = 1
        cellule.borders(9).Color = CDbl(Entry(1, Entry(1, Mid(Entry(2, strValue, "�BB="), 4), "�"), ")"))
    End If
    If InStr(1, strValue, "�BR=") <> 0 Then
        cellule.borders(10).LineStyle = 1
        cellule.borders(10).Color = CDbl(Entry(1, Entry(1, Mid(Entry(2, strValue, "�BR="), 4), "�"), ")"))
    End If
    If InStr(1, strValue, "�CO=") <> 0 Then
        cellule.borders(7).LineStyle = 1
        cellule.borders(8).LineStyle = 1
        cellule.borders(9).LineStyle = 1
        cellule.borders(10).LineStyle = 1
        cellule.borders(7).Color = CDbl(Entry(1, Entry(1, Mid(Entry(2, strValue, "�CO="), 4), "�"), ")"))
        cellule.borders(8).Color = CDbl(Entry(1, Entry(1, Mid(Entry(2, strValue, "�CO="), 4), "�"), ")"))
        cellule.borders(9).Color = CDbl(Entry(1, Entry(1, Mid(Entry(2, strValue, "�CO="), 4), "�"), ")"))
        cellule.borders(10).Color = CDbl(Entry(1, Entry(1, Mid(Entry(2, strValue, "�CO="), 4), "�"), ")"))
    End If
    If InStr(1, strValue, "�LOCK=") <> 0 Then
        toLock = True
        cellule.Locked = True
    Else
        'cellule.Locked = False
        'cellule.AllowEdit = True
    End If
Else
    StyleCellule = strValue
    'cellule.Locked = False
End If

End Function

Private Sub LinkOpenOffice(Optional DestinationFile As String)

'-> Proc�dure qui r�alise le lien avec Excel
Dim aRange As Object
Dim NbLigne As Long
Dim FindSheet As Boolean
Dim HideSheet As Integer
Dim hdlFile As Integer
Dim Ligne As String
Dim RowPos As Long
Dim ColPos As Long
Dim i As Long
Dim ValueField As String
Dim nbDec As String
Dim iPos As Long

Dim aServiceManager
Dim SpreadSheets
Dim objDocument
Dim oPropertyValue
Dim oWindow As Object
Dim oColumns As Object
Dim oColumn As Object

On Error GoTo EndError

'-> Afficher la feuille de temporisation
If Trim(DestinationFile) = "" Or OpenFile <> "" Then
    DealAnim.Caption = GetIniFileValue("LANGUE", "1", FichierLangue)
    DealAnim.Label1 = Replace(GetIniFileValue("LANGUE", "2", FichierLangue), "$FILE$", DetailFic)
    DealAnim.Show
    DoEvents
End If

'Lien vers le service manager de oppenoffice
Set aServiceManager = CreateObject("com.sun.star.ServiceManager")
Set oPropertyValue = aServiceManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue")
'Lien Vers le bureau
Set aDesktop = aServiceManager.createInstance("com.sun.star.frame.Desktop")
DoEvents

If Trim(ClasseurRef) = "" Or Dir$(ClasseurRef) = "" Then
    On Error Resume Next
    '-> Cr�er un nouveau classeur
    'Cr�er une nouvelle instance du tableau
    Dim args()
    Set objDocument = aDesktop.loadComponentFromURL("private:factory/scalc", "_blank", 0, args)
    Set oWindow = objDocument.CurrentController.Frame.ContainerWindow
    oWindow.Visible = False
    
    'R�cup�rer la collection des feuille
    Set SpreadSheets = objDocument.getSheets()
    '-> si on passe pas par un classeur de reference
    If Not pClasseurRef Then
        '-> on supprime les feuilles par defaut
        For i = SpreadSheets.Count - 1 To 1 Step -1
            SpreadSheets.removeByName SpreadSheets.getByIndex(i).Name
        Next
    End If
    'R�cup�rer le premi�re feuille
    Set aSpreadSheet = SpreadSheets.getByIndex(0)
    '-> Donner son nom � la feuille par d�faut
    SetNameFeuille = True
    On Error GoTo EndError

Else
    '-> Ouvrir le classeur de r�f�rence
    Set objDocument = OpenClasseurRef()
    '-> Tester le retour
    If objDocument Is Nothing Then
        MsgBox aLb.GetCaption(32), vbCritical + vbOKOnly, aLb.GetCaption(23)
        Exit Sub
    End If
    'R�cup�rer la collection des feuille
    Set SpreadSheets = objDocument.getSheets()
    '-> Tester si on doit pointer sur une feuille
    If pNomFeuille Then
        '-> pointer sur la feuille
        For i = 0 To SpreadSheets.Count - 1
            Set aSpreadSheet = SpreadSheets.getByIndex(i)
            If aSpreadSheet.Name = Trim(NomFeuille) Then Exit For
        Next
    Else
        Set aSpreadSheet = SpreadSheets.getByIndex(0)
        '-> donner un nom a la feuille
        If pClasseurRef Then
            aSpreadSheet.Name = aLb.GetCaption(38) & (SpreadSheets.Count)
        Else
            aSpreadSheet.Name = "Feuille" & i
        End If
    End If
End If

'-> Initialiser la cellule de d�part
If CellDep <> "" Then SetActiveCell

'-> R�cup�rer la ligne de d�part
'RowPos = ExcelApp.Application.ActiveCell.Row
'ColPos = ExcelApp.Application.ActiveCell.Column

'-> Ouverture du fichier
hdlFile = FreeFile
Open DetailFic For Input As #hdlFile

'-> Lecture s�quentielle du fichier
Do While Not EOF(hdlFile)
    '-> Mettre � jour le libelle�
    DealAnim.Label1 = Replace(GetIniFileValue("LANGUE", "8", FichierLangue), "$FILE$", DetailFic) & " " & NbLigne
    DoEvents
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> Redispatcher les colonnes
    For i = 1 To nbCol
        '-> get de la valeur
        ValueField = Entry(i, Ligne, SepFic)
        '-> rajout de ' ' si commence par '=' Didier
        If Len(ValueField) > 2 Then
            If Mid(ValueField, 1, 1) = "=" Then ValueField = " " & ValueField
        End If
        '-> Poser le Format
        If FormatCol(i) = 3 Or FormatCol(i) = 4 Then
            If IsNumeric(Trim(ValueField)) Then
                If FormatCol(i) = 4 Then
                    '-> R�cup�rer la partie d�cimale
                    iPos = InStr(1, ValueField, SepDec)
                    If iPos <> 0 Then
                        iPos = Len(ValueField) - iPos
                        If iPos <> 0 Then
                            nbDec = "0." & String(iPos, "0")
                        Else
                            nbDec = "0"
                        End If
                    Else
                        nbDec = "0"
                    End If
                Else
                    nbDec = "0"
                End If
                '-> on ecrit la valeur sur la feuille
                Set aRange = aSpreadSheet.getCellByPosition(ColPos + i - 1, RowPos + NbLigne)
                Call SetRangeValueForOpenOffice(aRange, Trim(Entry(i, Ligne, SepFic)))
                '-> Lib�rer
                Set aRange = Nothing
            Else
                '-> on ecrit la valeur sur la feuille
                Set aRange = aSpreadSheet.getCellByPosition(ColPos + i - 1, RowPos + NbLigne)
                Call SetRangeValueForOpenOffice(aRange, Trim(Entry(i, Ligne, SepFic)))
                '-> Lib�rer
                Set aRange = Nothing
            End If
        Else
            If FormatCol(i) = 2 Then
                If IsDate(ValueField) Then
                    '-> on ecrit la valeur sur la feuille
                    Set aRange = aSpreadSheet.getCellByPosition(ColPos + i - 1, RowPos + NbLigne)
                    Call SetRangeValueForOpenOffice(aRange, CDate(Entry(i, Ligne, SepFic)))
                    '-> Lib�rer
                    Set aRange = Nothing
                Else
                    '-> on ecrit la valeur sur la feuille
                    Set aRange = aSpreadSheet.getCellByPosition(ColPos + i - 1, RowPos + NbLigne)
                    Call SetRangeValueForOpenOffice(aRange, Entry(i, Ligne, SepFic))
                    '-> Lib�rer
                    Set aRange = Nothing
                End If
            Else
                If IsDate(ValueField) Then
                    '-> on ecrit la valeur sur la feuille
                    Set aRange = aSpreadSheet.getCellByPosition(ColPos + i - 1, RowPos + NbLigne)
                    Call SetRangeValueForOpenOffice(aRange, " " & ValueField)
                    '-> Lib�rer
                    Set aRange = Nothing
                Else
                    '-> on ecrit la valeur sur la feuille
                    Set aRange = aSpreadSheet.getCellByPosition(ColPos + i - 1, RowPos + NbLigne)
                    aRange.setString ValueField
                    '-> Lib�rer
                    Set aRange = Nothing
                End If
            End If
        End If
    Next 'Pour toutes les colonnes
    '-> Incr�menter le compteur de ligne
    NbLigne = NbLigne + 1
Loop

'-> Fermer le fichier
Close #hdlFile

'-> Si on doit donner un nom
If SetName <> "" Then
    '-> S�lectionner la plage entiere des donn�es
    'Set aRange = ExcelApp.Application.Range(ExcelApp.Application.ActiveCell, ExcelApp.Application.ActiveCell.Offset(ExcelApp.Application.Selection.Areas.Item(1).Rows.Count - 1, nbCol - 1))
    'aRange.Select
    '-> Donner un nom
    'SetGoodName aRange
End If

DoEvents

If Formattage Then
    '-> Faire la mise en forme
    DealAnim.Label1.Caption = GetIniFileValue("LANGUE", "7", FichierLangue)

    'Formattage des cellules de l'entete si on a sp�cifi� ou non une value
    If FormatEntete <> "" Then
        Set oColumns = aSpreadSheet.getColumns()
        For i = 1 To nbCol
            '-> on pointe sur la cellule de l'entete
            Set aRange = aSpreadSheet.getCellByPosition(ColPos + i - 1, RowPos + 0)
            Call SetFormat(aRange)
            Set aRange = Nothing
            Set oColumn = oColumns.getByIndex(ColPos + i - 1)
            oColumn.OptimalWidth = True
            DoEvents
            Set oColumn = Nothing
        Next
    End If
End If 'Si on doit formatter

''-> S�lectionner la cellule de d�part
'If Trim(CellDep) <> "" Then Feuille.Range(CellDep).Activate

'-> Ex�cuter une macro si necessaire
'If RunMacro <> "" Then RunMacroInExcelApp ExcelApp

'-> Masquer la feuille si necessaire
'Feuille.Visible = HideSheet

'-> rendre visible ou pas la feuille
If Trim(DestinationFile) <> "" Then
    '-> On sauvegarde le fichier au format XLS
    'ExcelApp.Workbooks(1).SaveAs DestinationFile
    'ExcelApp.Workbooks(1).Close
    objDocument.CurrentController.Frame.ContainerWindow.Visible = False
    objDocument.storeToURL ConvertToUrl(DestinationFile), args
    objDocument.Close (True)
Else
    objDocument.CurrentController.Frame.ContainerWindow.Visible = True
    'objDocument.storeToURL ConvertToUrl(DestinationFile), args
    'objDocument.Close (True)
End If

'-> Supprimer le fichier des donn�es
If Trim(DetailFic) <> "" Then
    If KillSpool Then If Dir$(DetailFic, vbNormal) <> "" Then Kill DetailFic
End If

EndError:
    
    
    If Err.Number <> 0 Then MsgBoxMouchard Err.Number & " -> " & Err.Description

    '-> Lib�rer les pointeurs
    Set Feuille = Nothing
    Set Classeur = Nothing
    'Set ExcelApp = Nothing
    Set aRange = Nothing
    
    '-> d�charger la feuille tempo
    Unload DealAnim


End Sub

Private Sub SetFormat(RangeDest As Object)

'---> Cette proc�dure duplique un format
Dim i As Long
Dim aBorder As Object

''-> Bordures
'For i = 7 To 10
'    Select Case i
'        Case 7
'            'Bordure Gauche
'            If Not IsEmpty(RangeRef.GetBorderColorIndex(i)) Then
'                Set aBorder = RangeDest.getPropertyValue("LeftBorder")
'                aBorder.Color = RGB(0, 0, 0)
'                aBorder.InnerLineWidth = 0
'                aBorder.OuterLineWidth = 10
'                aBorder.LineDistance = 10
'                Call RangeDest.setPropertyValue("LeftBorder", aBorder)
'            Else
''                Set aBorder = RangeDest.getPropertyValue("LeftBorder")
''                aBorder.Color = RGB(255, 255, 255)
''                aBorder.InnerLineWidth = 0
''                aBorder.OuterLineWidth = 10
''                aBorder.LineDistance = 10
''                Call RangeDest.setPropertyValue("LeftBorder", aBorder)
'            End If
'        Case 10
'            'Bordure Droite
'            If Not IsEmpty(RangeRef.GetBorderColorIndex(i)) Then
'                Set aBorder = RangeDest.getPropertyValue("RightBorder")
'                aBorder.Color = RGB(0, 0, 0)
'                aBorder.InnerLineWidth = 0
'                aBorder.OuterLineWidth = 10
'                aBorder.LineDistance = 10
'                Call RangeDest.setPropertyValue("RightBorder", aBorder)
'            Else
''                Set aBorder = RangeDest.getPropertyValue("RightBorder")
''                aBorder.Color = RGB(255, 255, 255)
''                aBorder.InnerLineWidth = 0
''                aBorder.OuterLineWidth = 10
''                aBorder.LineDistance = 10
''                Call RangeDest.setPropertyValue("RightBorder", aBorder)
'            End If
'        Case 8
'            'Bordure Haut
'            If Not IsEmpty(RangeRef.GetBorderColorIndex(i)) Then
'                Set aBorder = RangeDest.getPropertyValue("TopBorder")
'                aBorder.Color = RGB(0, 0, 0)
'                aBorder.InnerLineWidth = 0
'                aBorder.OuterLineWidth = 10
'                aBorder.LineDistance = 10
'                Call RangeDest.setPropertyValue("TopBorder", aBorder)
'            Else
''                Set aBorder = RangeDest.getPropertyValue("TopBorder")
''                aBorder.Color = RGB(255, 255, 255)
''                aBorder.InnerLineWidth = 0
''                aBorder.OuterLineWidth = 10
''                aBorder.LineDistance = 10
''                Call RangeDest.setPropertyValue("TopBorder", aBorder)
'            End If
'        Case 9
'            'Bordure Bas
'            If Not IsEmpty(RangeRef.GetBorderColorIndex(i)) Then
'                Set aBorder = RangeDest.getPropertyValue("BottomBorder")
'                aBorder.Color = RGB(0, 0, 0)
'                aBorder.InnerLineWidth = 0
'                aBorder.OuterLineWidth = 10
'                aBorder.LineDistance = 10
'                Call RangeDest.setPropertyValue("BottomBorder", aBorder)
'            Else
''                Set aBorder = RangeDest.getPropertyValue("BottomBorder")
''                aBorder.Color = RGB(255, 255, 255)
''                aBorder.InnerLineWidth = 0
''                aBorder.OuterLineWidth = 10
''                aBorder.LineDistance = 10
''                Call RangeDest.setPropertyValue("BottomBorder", aBorder)
'            End If
'    End Select
'Next

''-> Font
'RangeDest.CharFontName = RangeRef.FontName
'RangeDest.CharHeight = RangeRef.FontSize
'-> le gras
'If RangeRef.FontBold Then
    RangeDest.CharWeight = 150
'Else
'    RangeDest.CharWeight = 100
'End If
'-> l'italique
'If RangeRef.FontItalic Then
    RangeDest.CharPosture = 2
'Else
'    RangeDest.CharPosture = 0
'End If
'-> le soulign�
'If RangeRef.FontUnderline Then
'    RangeDest.CharUnderline = 1
'Else
'    RangeDest.CharUnderline = 0
'End If
'-> Couleur de fond
RangeDest.CellBackColor = RGB(192, 192, 192)

''-> Alignement interne de la cellule
'Select Case RangeRef.VerticalAlignment
'    Case -4160
'        RangeDest.VertJustify = 1
'    Case -4108
'        RangeDest.VertJustify = 2
'    Case -4107
'        RangeDest.VertJustify = 3
'End Select

'Select Case RangeRef.HorizontalAlignment
'    Case -4131
'        RangeDest.HoriJustify = 1
'    Case -4108
        RangeDest.HoriJustify = 2
'    Case -4152
'        RangeDest.HoriJustify = 3
'End Select

Set aBorder = Nothing

End Sub

Sub SetRangeValueForOpenOffice(aRange, valueToSet)
'-> Proc�dure qui pose le contenu d'une cellule

    On Error Resume Next

    '-> poser en num�rique ou en texte selon la valeur
    If IsNumeric(valueToSet) Then
        aRange.SetValue (valueToSet)
    Else
        aRange.setString (valueToSet)
    End If

End Sub

Private Function SetActiveCell() As Boolean
Dim aCellule As Object
Dim aRange As Object
On Error GoTo GestError

'-> Essayer de positionner la cellule active
Set aRange = aSpreadSheet.getCellRangeByName(CellDep)
'Set aCellule = aRange.getCellByPosition(0, 0)
ColDep = aRange.CellAddress.Column
RowDep = aRange.CellAddress.Row
Exit Function

GestError:
    SetActiveCell = False

End Function

Private Function OpenClasseurRef() As Object

Dim aW As Object
Dim AdresseDoc As String
Dim OpenParam()

On Error GoTo GestError
'Set OpenParam(0) = MakePropertyValue("Hidden", True)
'Set OpenParam(1) = MakePropertyValue("ReadOnly", False)

AdresseDoc = ClasseurRef
'-> Essayer d'ouvrir le classeur sp�cifi�
Set aW = aDesktop.loadComponentFromURL(ConvertToUrl(AdresseDoc), "_blank", 0, OpenParam)
'-> Renvoyer la r�f�rence
Set OpenClasseurRef = aW

'-> on masque le classeur
aW.CurrentController.Frame.ContainerWindow.Visible = False
Exit Function

GestError:
        Set OpenClasseurRef = Nothing

End Function

Private Function ConvertToUrl(strFile) As String
    '-> fonction pour transformer le chemin du fichier
    strFile = Replace(strFile, "\", "/")
    strFile = Replace(strFile, ":", "|")
    strFile = Replace(strFile, " ", "%20")
    strFile = "file:///" + strFile
    ConvertToUrl = strFile
End Function

Private Function SetGoodName(aRange As Object)

'---> Cette proc�dure d�termine si le nom est bon ou non

On Error GoTo NameError

aRange.Name = SetName

Exit Function

NameError:


End Function

Private Sub ApplicFormat(ByVal xlF As Xl_Format, ByVal aRange As Object)

On Error Resume Next

    'Bordures
    If xlF.xlLineStyle = -4142 Then
    Else
        aRange.borders.LineStyle = xlF.xlLineStyle
        aRange.borders.Weight = xlF.XlBorderWeight
        aRange.borders.ColorIndex = xlF.ColorBordure
    End If
    'Int�rieur
    aRange.Interior.ColorIndex = xlF.BackColor
    aRange.Interior.Pattern = 1
    
    If aRange.Font.Name <> xlF.FontName Then aRange.Font.Name = xlF.FontName
    aRange.Font.ColorIndex = xlF.FontColor
    If aRange.Font.Size <> xlF.FontSize Then aRange.Font.Size = xlF.FontSize
    If aRange.Font.Bold <> xlF.FontBold Then aRange.Font.Bold = xlF.FontBold
    If aRange.Font.Italic <> xlF.FontItalic Then aRange.Font.Italic = xlF.FontItalic
    If aRange.Font.Underline <> xlF.FontUnderline Then aRange.Font.Underline = xlF.FontUnderline
    'If aRange.HorizontalAlignment <> xlF.AlignementH Then aRange.HorizontalAlignment = xlF.AlignementH


Err.Number = 0

End Sub


Public Sub GenerateTemplate()

'-> Fonction qui g�n�re un exemple pour tester un format de cellule

Dim ExcelApp As Object
Dim Classeur As Object
Dim Feuille As Object
Dim aRange As Object

'-> Cr�ation du lien avec Excel
Set ExcelApp = CreateObject("Excel.Application")
ExcelApp.Visible = False
Set Classeur = ExcelApp.Workbooks.Add
Set Feuille = Classeur.Worksheets("Feuil1")
Feuille.Range("B3").Activate

'-> Copie de la chaine test dans la m�moire
Clipboard.Clear
Clipboard.SetText "Cellule1|Cellule2|Cellule3" & Chr(13) + Chr(10) & "Cellule1|Cellule2|Cellule3"

'-> Collage des donn�es
Feuille.Paste

'-> R�partition des donn�es
ExcelApp.Application.selection.TextToColumns DataType:=xlDelimited, _
    ConsecutiveDelimiter:=False, Other:=True, OtherChar:="|"

'-> S�lection des cellules
Set aRange = Feuille.Range("B3:D4")
aRange.Activate

'-> R�cup�ration du format de cellule
Set xlFormat = xl_Formats(UCase$(EditLib.List2.Text))
Call ApplicFormat(xlFormat, aRange)

'-> S�lectionner la premi�re cellule
Feuille.Range("A1").Activate
 
ExcelApp.Visible = True


Set ExcelApp = Nothing

End Sub

Public Function Trace(strTemp As String, ii As Integer)

End Function

Public Function GetMyDocumentsPathVB() As String

End Function
