VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form EditLib 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Librairie de format Excel"
   ClientHeight    =   5775
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9915
   Icon            =   "EditLib.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5775
   ScaleWidth      =   9915
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Height          =   975
      Left            =   1800
      Picture         =   "EditLib.frx":0442
      Style           =   1  'Graphical
      TabIndex        =   25
      ToolTipText     =   "Tester le format"
      Top             =   4680
      Width           =   1095
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2160
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   24
      ImageHeight     =   22
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "EditLib.frx":130C
            Key             =   "A2"
            Object.Tag             =   "2"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "EditLib.frx":14A6
            Key             =   "A0"
            Object.Tag             =   "0"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "EditLib.frx":1640
            Key             =   "A1"
            Object.Tag             =   "1"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "EditLib.frx":17DA
            Key             =   "A2#"
            Object.Tag             =   "2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "EditLib.frx":1974
            Key             =   "A0#"
            Object.Tag             =   "0"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "EditLib.frx":1B0E
            Key             =   "A1#"
            Object.Tag             =   "1"
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Modifier 
      Caption         =   "Modifier"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   5400
      Width           =   1455
   End
   Begin VB.CommandButton Supprimer 
      Caption         =   "Supprimer"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   5040
      Width           =   1455
   End
   Begin VB.CommandButton Ajouter 
      Caption         =   "&Ajouter"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   4680
      Width           =   1455
   End
   Begin VB.Frame Frame1 
      Caption         =   "Propri�t�s : "
      Enabled         =   0   'False
      Height          =   5655
      Left            =   3000
      TabIndex        =   4
      Top             =   120
      Width           =   6855
      Begin VB.CommandButton Enregistrer 
         DisabledPicture =   "EditLib.frx":1CA8
         Enabled         =   0   'False
         Height          =   570
         Left            =   6000
         Picture         =   "EditLib.frx":2AEA
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   4920
         Width           =   735
      End
      Begin VB.CommandButton Annuler 
         DisabledPicture =   "EditLib.frx":3B2C
         Enabled         =   0   'False
         Height          =   570
         Left            =   5160
         Picture         =   "EditLib.frx":4B6E
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   4920
         Width           =   735
      End
      Begin VB.ListBox List4 
         Height          =   2010
         Left            =   5760
         TabIndex        =   22
         Top             =   720
         Width           =   855
      End
      Begin VB.PictureBox Picture4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   345
         Left            =   1995
         Picture         =   "EditLib.frx":5BB0
         ScaleHeight     =   345
         ScaleWidth      =   510
         TabIndex        =   20
         Top             =   4920
         Width           =   510
         Begin VB.Image ColorCelluleRef 
            Height          =   180
            Left            =   165
            Picture         =   "EditLib.frx":654A
            Top             =   60
            Width           =   180
         End
      End
      Begin VB.PictureBox Picture3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   345
         Left            =   5235
         Picture         =   "EditLib.frx":673C
         ScaleHeight     =   345
         ScaleWidth      =   510
         TabIndex        =   18
         Top             =   4275
         Width           =   510
         Begin VB.Image ColorPoliceRef 
            Height          =   180
            Left            =   160
            Picture         =   "EditLib.frx":70D6
            Top             =   60
            Width           =   180
         End
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Souslign�"
         Height          =   255
         Left            =   3480
         TabIndex        =   16
         Top             =   3720
         Width           =   1095
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Italic"
         Height          =   255
         Left            =   3480
         TabIndex        =   15
         Top             =   3360
         Width           =   1095
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Gras"
         Height          =   255
         Left            =   3480
         TabIndex        =   14
         Top             =   3000
         Width           =   1095
      End
      Begin VB.ListBox List3 
         Height          =   2010
         Left            =   3480
         Sorted          =   -1  'True
         TabIndex        =   13
         Top             =   720
         Width           =   2175
      End
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   345
         Left            =   2000
         Picture         =   "EditLib.frx":72C8
         ScaleHeight     =   345
         ScaleWidth      =   510
         TabIndex        =   11
         Top             =   4400
         Width           =   510
         Begin VB.Image ColorBordureRef 
            Height          =   180
            Left            =   160
            Picture         =   "EditLib.frx":7C62
            Top             =   60
            Width           =   180
         End
      End
      Begin VB.PictureBox Picture1 
         Height          =   3495
         Left            =   240
         ScaleHeight     =   3435
         ScaleWidth      =   2835
         TabIndex        =   9
         Top             =   720
         Width           =   2895
         Begin VB.Shape Shape1 
            BackColor       =   &H000000FF&
            BorderColor     =   &H000000FF&
            Height          =   495
            Left            =   1400
            Top             =   500
            Width           =   1400
         End
         Begin VB.Image Bordure 
            Height          =   330
            Index           =   13
            Left            =   1440
            Picture         =   "EditLib.frx":7E54
            Top             =   3000
            Width           =   1245
         End
         Begin VB.Image Bordure 
            Height          =   330
            Index           =   12
            Left            =   1440
            Picture         =   "EditLib.frx":943E
            Top             =   2520
            Width           =   1245
         End
         Begin VB.Image Bordure 
            Appearance      =   0  'Flat
            Height          =   300
            Index           =   11
            Left            =   1440
            Picture         =   "EditLib.frx":AA28
            Top             =   2040
            Width           =   1230
         End
         Begin VB.Image Bordure 
            Height          =   300
            Index           =   10
            Left            =   1440
            Picture         =   "EditLib.frx":BDCA
            Top             =   1560
            Width           =   1230
         End
         Begin VB.Image Bordure 
            Height          =   300
            Index           =   9
            Left            =   1440
            Picture         =   "EditLib.frx":D16C
            Top             =   1080
            Width           =   1230
         End
         Begin VB.Image Bordure 
            Height          =   300
            Index           =   8
            Left            =   1440
            Picture         =   "EditLib.frx":E50E
            Top             =   600
            Width           =   1230
         End
         Begin VB.Image Bordure 
            Height          =   300
            Index           =   7
            Left            =   1440
            Picture         =   "EditLib.frx":F8B0
            Top             =   120
            Width           =   1230
         End
         Begin VB.Image Bordure 
            Height          =   270
            Index           =   6
            Left            =   120
            Picture         =   "EditLib.frx":10C52
            Top             =   3000
            Width           =   1215
         End
         Begin VB.Image Bordure 
            Height          =   270
            Index           =   5
            Left            =   120
            Picture         =   "EditLib.frx":11DBC
            Top             =   2520
            Width           =   1215
         End
         Begin VB.Image Bordure 
            Height          =   270
            Index           =   4
            Left            =   120
            Picture         =   "EditLib.frx":12F26
            Top             =   2040
            Width           =   1215
         End
         Begin VB.Image Bordure 
            Height          =   270
            Index           =   3
            Left            =   120
            Picture         =   "EditLib.frx":14090
            Top             =   1560
            Width           =   1215
         End
         Begin VB.Image Bordure 
            Height          =   270
            Index           =   2
            Left            =   120
            Picture         =   "EditLib.frx":151FA
            Top             =   1080
            Width           =   1215
         End
         Begin VB.Image Bordure 
            Height          =   270
            Index           =   1
            Left            =   120
            Picture         =   "EditLib.frx":16364
            Top             =   600
            Width           =   1215
         End
         Begin VB.Image Bordure 
            Height          =   330
            Index           =   0
            Left            =   120
            Picture         =   "EditLib.frx":174CE
            Top             =   120
            Width           =   1245
         End
      End
      Begin VB.Image Image3 
         Height          =   330
         Left            =   6120
         Top             =   3360
         Width           =   360
      End
      Begin VB.Label Label8 
         Caption         =   "Couleur de la cellule :"
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   4965
         Width           =   1815
      End
      Begin VB.Image Image7 
         Height          =   345
         Left            =   2520
         Picture         =   "EditLib.frx":18AB8
         Top             =   4920
         Width           =   150
      End
      Begin VB.Label Label7 
         Caption         =   "Couleur de la police :"
         Height          =   255
         Left            =   3480
         TabIndex        =   19
         Top             =   4320
         Width           =   1815
      End
      Begin VB.Image Image5 
         Height          =   345
         Left            =   5760
         Picture         =   "EditLib.frx":18DDA
         Top             =   4275
         Width           =   150
      End
      Begin VB.Shape Shape4 
         Height          =   1095
         Left            =   4680
         Top             =   2880
         Width           =   1935
      End
      Begin VB.Label Label6 
         Caption         =   "Alignement"
         Height          =   255
         Left            =   4800
         TabIndex        =   17
         Top             =   3000
         Width           =   1815
      End
      Begin VB.Image Image2 
         Height          =   330
         Left            =   5640
         Top             =   3360
         Width           =   360
      End
      Begin VB.Image Image1 
         Height          =   330
         Left            =   5160
         Top             =   3360
         Width           =   360
      End
      Begin VB.Label Label5 
         Caption         =   "Police :"
         Height          =   255
         Left            =   3480
         TabIndex        =   12
         Top             =   360
         Width           =   1815
      End
      Begin VB.Shape Shape3 
         Height          =   4575
         Left            =   3360
         Top             =   240
         Width           =   3375
      End
      Begin VB.Shape Shape2 
         Height          =   5295
         Left            =   120
         Top             =   240
         Width           =   3135
      End
      Begin VB.Image DisplayBackcolor 
         Height          =   345
         Left            =   2520
         Picture         =   "EditLib.frx":190FC
         Top             =   4400
         Width           =   150
      End
      Begin VB.Label Label4 
         Caption         =   "Couleur de la bordure :"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   4440
         Width           =   1815
      End
      Begin VB.Label Label3 
         Caption         =   "Bordures :"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   360
         Width           =   1695
      End
   End
   Begin VB.ListBox List2 
      Height          =   2205
      Left            =   120
      Sorted          =   -1  'True
      TabIndex        =   3
      Top             =   2280
      Width           =   2775
   End
   Begin VB.ListBox List1 
      Height          =   1230
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   2775
   End
   Begin VB.Label Label2 
      Caption         =   "Formats existants :"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1920
      Width           =   2775
   End
   Begin VB.Label Label1 
      Caption         =   "Librairie � ouvrir :"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1335
   End
End
Attribute VB_Name = "EditLib"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim xlFormat As Xl_Format
Dim AlignH As Integer 'Indique l'alignement interne du format en cours

Private Sub Ajouter_Click()

'-> Bordure Aucune
Me.Bordure_Click (0)

'-> Couleur de bordure Noir
Me.ColorBordureRef.Picture = PaletteColor.ImageList1.ListImages("C1").Picture
Me.ColorBordureRef.Tag = 1
'-> Fond de cellule blanc
Me.ColorCelluleRef.Picture = PaletteColor.ImageList1.ListImages("C2").Picture
Me.ColorCelluleRef.Tag = 2
'-> Couleur de police Noir
Me.ColorPoliceRef.Picture = PaletteColor.ImageList1.ListImages("C1").Picture
Me.ColorPoliceRef.Tag = 1
'-> Police = "Times
Me.List3.Text = "Times New Roman"
'-> Size 10
Me.List4.Text = "10"
'-> Pas d'option de police
Me.Check1.Value = 0
Me.Check2.Value = 0
Me.Check3.Value = 0
'-> Alignement gauche
Me.Image1_Click

'-> Rendre la frame OK
Me.Frame1.Enabled = True

'-> Blocker les deux listes
Me.List1.Enabled = False
Me.List2.Enabled = False

'-> Blocker tous les boutons
Me.Ajouter.Enabled = False
Me.Supprimer.Enabled = False
Me.Modifier.Enabled = False

'-> Rendre les boutons de la frame OK
Me.Enregistrer.Enabled = True
Me.Enregistrer.Tag = 1
Me.Annuler.Enabled = True


End Sub

Private Sub Annuler_Click()

Me.List1.Enabled = True
Me.List2.Enabled = True
If Me.List2.ListCount <> 0 Then
    Me.List2_Click
End If

Me.Annuler.Enabled = False
Me.Enregistrer.Enabled = False

End Sub

Public Sub Bordure_Click(Index As Integer)

'-> Sauvegarder l'index de la bordure
Me.Picture1.Tag = Index

Me.Shape1.Left = Me.Bordure(Index).Left - 50
Me.Shape1.Top = Me.Bordure(Index).Top - 50
Me.Shape1.Height = Me.Bordure(Index).Height + 100
Me.Shape1.Width = Me.Bordure(Index).Width + 100

End Sub


Private Sub Command1_Click()
    
    Me.MousePointer = 11
    GenerateTemplate
    Me.MousePointer = 0
    
End Sub

Private Sub DisplayBackcolor_Click()

Dim aPoint As POINTAPI
Dim Res As Long

    '-> Recherche de la position du pointeur
    Res = GetCursorPos(aPoint)
    '-> Propri�t�s de position
    PaletteColor.Left = Screen.TwipsPerPixelX * aPoint.x
    PaletteColor.Top = Screen.TwipsPerPixelY * aPoint.Y
    '-> Nature de la couleur
    PaletteColor.TypeColor = 2 'Bordures
    '-> Affichage
    PaletteColor.Show vbModal

End Sub

Private Sub Enregistrer_Click()

Dim NomFormat As String
Dim i As Integer

If Me.Enregistrer.Tag = 1 Then 'Ajouter
    NomFormat = InputBox("Veuillez saisir le nom de votre nouveau format.", "Nouveau Format", "Nouveau")
    If Trim(NomFormat) = "" Then
    Else
        '-> V�rifier que le  format n'existe pas
        For i = 0 To Me.List2.ListCount - 1
            If UCase$(NomFormat) = UCase$(Me.List2.List(i)) Then
                MsgBoxMouchard "Format d�ja existant.", vbCritical + vbOKOnly, "Erreur sur le nom"
                Exit Sub
            End If
        Next
        
        '-> V�rifier que ce n'est pas un format num�rique
        If IsNumeric(NomFormat) Then
            MsgBoxMouchard "Les noms de format ne peuvent �tre num�riques.", vbCritical + vbOKOnly, "Erreur sur le nom"
            Exit Sub
        End If
        
        '-> Ajouter le format
        Set xlFormat = New Xl_Format
        xlFormat.Name = NomFormat
        xl_Formats.Add xlFormat, UCase$(NomFormat)
        
        '-> Enregistrer le format
        SaveFormat xlFormat
        
        '-> Ajouter dans la liste
        Me.List2.AddItem NomFormat
                                                
    End If
    
Else

    Set xlFormat = xl_Formats(UCase$(Me.Frame1.Tag))
    
    NomFormat = xlFormat.Name
    
    '-> Enregistrer le format
    SaveFormat xlFormat


End If 'Selon Modif/Ajouter

'-> Enregistrer la librairie
SaveLib App.Path & "\DealTool1.lib"

'-> Lib�rer les listes
Me.List1.Enabled = True
Me.List2.Enabled = True

'-> Click sur la liste
Me.List2.Text = NomFormat
Me.List2_Click

End Sub

Private Sub Form_Load()

'-> Affichage des polices dans la liste
For i = 1 To Screen.FontCount - 1
    Me.List3.AddItem Screen.Fonts(i)
Next
'-> Affichage des tailles de police
For i = 6 To 72 Step 2
    Me.List4.AddItem i
Next

'-> V�rifier l'existence de la librairie
If Dir$(App.Path & "\DealTool1.lib") = "" Then
    MsgBoxMouchard "Impossible d'ouvrir le fichier : " & App.Path & "\DealTool.lib1", vbCritical + vbOKOnly, "Erreur"
Else
    '-> Ajouter la liste des librairies
    Me.List1.AddItem "DealTool1.lib"
    '-> S�lectionner la librairie
    Me.List1.Selected(0) = True
End If 'Si on trouve la librairie

End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Public Sub Image1_Click()

'-> Alignements internes
AlignH = 0
Me.Image1.Picture = Me.ImageList1.ListImages("A0#").Picture
Me.Image2.Picture = Me.ImageList1.ListImages("A2").Picture
Me.Image3.Picture = Me.ImageList1.ListImages("A1").Picture



End Sub

Public Sub Image2_Click()

'-> Alignements internes
AlignH = 2
Me.Image1.Picture = Me.ImageList1.ListImages("A0").Picture
Me.Image2.Picture = Me.ImageList1.ListImages("A2#").Picture
Me.Image3.Picture = Me.ImageList1.ListImages("A1").Picture

End Sub

Public Sub Image3_Click()

'-> Alignements internes
AlignH = 1
Me.Image1.Picture = Me.ImageList1.ListImages("A0").Picture
Me.Image2.Picture = Me.ImageList1.ListImages("A2").Picture
Me.Image3.Picture = Me.ImageList1.ListImages("A1#").Picture

End Sub


Private Sub Image5_Click()

Dim aPoint As POINTAPI
Dim Res As Long

    '-> Recherche de la position du pointeur
    Res = GetCursorPos(aPoint)
    '-> Propri�t�s de position
    PaletteColor.Left = Screen.TwipsPerPixelX * aPoint.x
    PaletteColor.Top = Screen.TwipsPerPixelY * aPoint.Y
    '-> Nature de la couleur
    PaletteColor.TypeColor = 3 'Police
    '-> Affichage
    PaletteColor.Show vbModal
    
End Sub

Private Sub Image7_Click()

Dim aPoint As POINTAPI
Dim Res As Long

    '-> Recherche de la position du pointeur
    Res = GetCursorPos(aPoint)
    '-> Propri�t�s de position
    PaletteColor.Left = Screen.TwipsPerPixelX * aPoint.x
    PaletteColor.Top = Screen.TwipsPerPixelY * aPoint.Y
    '-> Nature de la couleur
    PaletteColor.TypeColor = 1 'BackColor
    '-> Affichage
    PaletteColor.Show vbModal
    
End Sub

Public Sub List1_Click()

Dim lpBuffer As String
Dim Res As Long
Dim i As Integer

'-> Vides la liste des formats existants
Me.List2.Clear

'-> Ouvrir la librairie sp�cifi�e pour r�cup�rer la liste des formats existants
lpBuffer = Space$(20000)
Res = GetPrivateProfileSectionNames&(lpBuffer, Len(lpBuffer), App.Path & "\DealTool1.lib")
    
'-> Initialisation de la collection
Set xl_Formats = New Collection
'-> Cr�er les diff�rents formats de fichier
For i = 1 To NumEntries(lpBuffer, Chr(0)) - 2
    Me.List2.AddItem Entry(i, lpBuffer, Chr(0))
    AnalyseFormat Entry(i, lpBuffer, Chr(0))
Next
'-> S�lectionner le premier format
If Me.List2.ListCount <> 0 Then Me.List2.Selected(0) = True

End Sub


Public Sub DisplayFormat(ByVal NomFormat As String)

'--> Affiche le contenu d'un format � l'�cran

'-> R�cup�ration du nom du format
Set xlFormat = xl_Formats(UCase$(NomFormat))

'-> Affichage des infos

'Type de Bordure
Me.Bordure_Click (xlFormat.TypeBordure)

'Couleur de type de bordure
Me.ColorBordureRef.Picture = PaletteColor.ImageList1.ListImages("C" & xlFormat.ColorBordure).Picture
Me.ColorBordureRef.Tag = xlFormat.ColorBordure

'Couleur du fond de la cellule
Me.ColorCelluleRef.Picture = PaletteColor.ImageList1.ListImages("C" & xlFormat.BackColor).Picture
Me.ColorCelluleRef.Tag = xlFormat.BackColor

'Police
Me.List3.Text = xlFormat.FontName

'Size
Me.List4.Text = xlFormat.FontSize

'Couleur de la police
Me.ColorPoliceRef.Picture = PaletteColor.ImageList1.ListImages("C" & xlFormat.FontColor).Picture
Me.ColorPoliceRef.Tag = xlFormat.FontColor

'Gras
If xlFormat.FontBold Then
    Me.Check1.Value = 1
Else
    Me.Check1.Value = 0
End If
'Italic
If xlFormat.FontItalic Then
    Me.Check2.Value = 1
Else
    Me.Check2.Value = 0
End If
'Underline
If xlFormat.FontUnderline Then
    Me.Check3.Value = 1
Else
    Me.Check3.Value = 0
End If

'-> Load des images d'alignement
Me.Image1.Picture = Me.ImageList1.ListImages("A0").Picture
Me.Image2.Picture = Me.ImageList1.ListImages("A2").Picture
Me.Image3.Picture = Me.ImageList1.ListImages("A1").Picture

'-> Alignements internes
AlignH = xlFormat.DisplayAlign
Select Case AlignH

    Case 0
        Me.Image1_Click
    Case 1
        Me.Image3_Click
    Case 2
        Me.Image2_Click
        
End Select
        

End Sub

Public Sub List2_Click()

'-> Affichage du format des cellules
DisplayFormat Me.List2.Text
'-> Gestion des boutons
Me.Annuler.Enabled = False
Me.Enregistrer.Enabled = False
Me.Frame1.Enabled = False
If UCase$(Me.List2.Text) = "ENTETEDEFAUT" Or UCase$(Me.List2.Text) = "LIGNEDEFAUT" Then
    Me.Ajouter.Enabled = True
    Me.Supprimer.Enabled = False
    Me.Modifier.Enabled = False
Else
    If Me.List2.ListCount <> 0 Then
        Me.Supprimer.Enabled = True
        Me.Modifier.Enabled = True
    Else
        Me.Supprimer.Enabled = False
        Me.Modifier.Enabled = False
    End If
End If

End Sub

Public Sub SaveFormat(ByVal xlF As Xl_Format)

'-> Enregistre le format sp�cifi� en fonction des saisies ecrans

'-> Format de bordure
xlF.TypeBordure = CInt(Me.Picture1.Tag)

'-> Couleur de bordure
xlF.ColorBordure = CInt(Me.ColorBordureRef.Tag)

'-> Couleur du fond de la cellule
xlF.BackColor = CInt(Me.ColorCelluleRef.Tag)

'-> Police
xlF.FontName = Me.List3.Text

'-> Taille de la police
xlF.FontSize = CInt(Me.List4.Text)

'-> Gras
xlF.FontBold = CBool(Me.Check1.Value)

'-> Italic
xlF.FontItalic = CBool(Me.Check2.Value)

'-> Underline
xlF.FontUnderline = CBool(Me.Check3.Value)

'-> Couleur de la cellule
xlF.FontColor = CInt(Me.ColorPoliceRef.Tag)

'-> Alignement interne
xlF.DisplayAlign = AlignH


End Sub

Private Sub Modifier_Click()

'-> Garder le nom du format
Me.Frame1.Tag = Me.List2.Text

'-> Rendre la frame OK
Me.Frame1.Enabled = True

'-> Blocker les deux listes
Me.List1.Enabled = False
Me.List2.Enabled = False

'-> Blocker tous les boutons
Me.Ajouter.Enabled = False
Me.Supprimer.Enabled = False
Me.Modifier.Enabled = False

'-> Rendre les boutons de la frame OK
Me.Enregistrer.Enabled = True
Me.Enregistrer.Tag = 2
Me.Annuler.Enabled = True

End Sub


Sub SaveLib(ByVal Lib As String)

'-> Fonction qui enregistre une librairie


Dim i As Integer
Dim FicLib As Integer


'-> Ouverture du fichier
FicLib = FreeFile
Open Lib For Output As #FicLib

'-> Enregistrement
For Each xlFormat In xl_Formats

    '-> Nom de la librairie
    Print #FicLib, "[" & xlFormat.Name & "]"
    Print #FicLib, "TypeBordure=" & xlFormat.TypeBordure
    Print #FicLib, "CouleurBordure=" & xlFormat.ColorBordure
    Print #FicLib, "BackColor=" & xlFormat.BackColor
    Print #FicLib, "FontName=" & xlFormat.FontName
    Print #FicLib, "FontSize=" & xlFormat.FontSize
    Print #FicLib, "Fontcolor=" & xlFormat.FontColor
    Print #FicLib, "FontItalic=" & xlFormat.FontItalic
    Print #FicLib, "FontBold=" & xlFormat.FontBold
    Print #FicLib, "Fontunderline=" & xlFormat.FontUnderline
    Print #FicLib, "Alignement=" & xlFormat.DisplayAlign

Next

'-> Fermer le fichier
Close #FicLib


End Sub

Private Sub Supprimer_Click()

Dim Res

    Res = MsgBoxMouchard("Supprimer le format : " & Me.List2.Text, vbQuestion + vbYesNo, "Confirmation")
    If Res = vbNo Then Exit Sub
    xl_Formats.Remove (UCase$(Me.List2.Text))
    
    SaveLib App.Path & "\DealTool1.lib"
    
    Me.List1_Click
    

End Sub
