VERSION 5.00
Begin VB.Form frmHTML 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   4155
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7500
   Icon            =   "frmHTML.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4155
   ScaleWidth      =   7500
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox List1 
      Height          =   1230
      Left            =   120
      TabIndex        =   1
      Top             =   1320
      Width           =   7215
   End
   Begin VB.TextBox Text2 
      Height          =   285
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   3975
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   120
      TabIndex        =   2
      Top             =   3120
      Width           =   7215
   End
   Begin VB.Image Annuler 
      Height          =   480
      Left            =   5280
      Top             =   3600
      Width           =   630
   End
   Begin VB.Image Aide 
      Height          =   480
      Left            =   6000
      Top             =   3600
      Width           =   630
   End
   Begin VB.Image Ok 
      Height          =   480
      Left            =   6720
      Top             =   3600
      Width           =   630
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Label3"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   960
      Width           =   3375
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Label2"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   1815
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   2760
      Width           =   3855
   End
End
Attribute VB_Name = "frmHTML"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub Form_Load()

On Error Resume Next

Dim aLb As Libelle
Dim aTb As Tableau
Dim aRg As Section
Dim aLig As Block

Set aLb = Libelles("FRMHTML")
Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Label2.Caption = aLb.GetCaption(3)
Me.Label3.Caption = aLb.GetCaption(4)

Set aLb = Nothing

Set aLb = Libelles("BOUTONS")
Me.Annuler.ToolTipText = aLb.GetCaption(1)
Me.Aide.ToolTipText = aLb.GetCaption(2)
Me.Ok.ToolTipText = aLb.GetCaption(3)

Me.Annuler.Picture = Turbo_Maq.Annuler.Picture
Me.Aide.Picture = Turbo_Maq.Aide.Picture
Me.Ok.Picture = Turbo_Maq.Ok.Picture


Set aLb = Nothing

'-> Charger la liste avec tous les objets
For Each aRg In Maquettes("MAQUETTE").Sections
    Me.List1.AddItem "ST-" & aRg.Nom
Next

For Each aTb In Maquettes("MAQUETTE").Tableaux
    For Each aLig In aTb.Blocks
        Me.List1.AddItem "TB-" & aTb.Nom & "-BLK-" & aLig.Nom
    Next
Next

Me.Text2.Text = FileHTML
Me.Text1.Text = BaliseHTML

End Sub

Private Sub List1_DblClick()
    Me.Text1.Text = Me.Text1.Text & Me.List1.Text
    Me.Text1.SetFocus
End Sub

Private Sub Ok_Click()

BaliseHTML = Me.Text1.Text
FileHTML = Me.Text2.Text

Unload Me

End Sub
