VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cellule"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'**********************************************
'*                                            *
'* Cette classe contient la d�finition d'un   *
'* Objet Cellule                              *
'*                                            *
'**********************************************

'-> Variables de position
Public X1 As Double
Public Y1 As Double
Public X2 As Double
Public Y2 As Double

'-> indique si la cellule est active
Public IsActive As Boolean

'-> Coordonn�es en Ligne/Colonne
Public Ligne As Integer
Public Colonne As Integer

'-> Couleur de fond de la cellule
Public BackColor As Long

'-> Bordures stock�es en type priv� pour utilisation des Property
Public BordureBas As Boolean
Public BordureHaut As Boolean
Public BordureGauche As Boolean
Public BordureDroite As Boolean

'-> Propri�t�s de Font
Public FontName As String
Public FontSize As Integer
Public FontColor As Long
Public FontBold As Boolean
Public FontItalic As Boolean
Public FontUnderline As Boolean

Public FondTransParent As Boolean

'-> Alignement interne : Valeurs de 1 � 9
Public CellAlign As Integer

'-> Contenu de la cellule
Public Contenu As String

'-> Retour � la ligne automatique du contenu
Public AutoAjust As Boolean

'-> Format li� � la cellule
Public TypeValeur As Integer 'Valeur des diff�rentes formats
 'Format � appliquer avec : _
    0 -> Pas de format _
    1 -> Num�rique _
    2 -> Caractere
Public Msk As String
' -> pour msk si le format est numrique :
' 3 entr�es s�par�es par des �
' 1 : Nombre de d�cimal
' 2 : S�parateur de milier '-> "" = pas de s�parateur de milliers
' 3 : Signe n�gatif : Index de 0 � 4 ( voir fichier MessProg [frmFormatCell]
' -> pour msk si le format est caract�re  :
' 1 entr�e qui contient le nombre de caract�re � garder

'-> Indique dans le cadre ou la cellule est num�rique si on doit imprimer 0
Public PrintZero As Boolean

'-> Dans le cas des exports Excel
Public IsFusion As Boolean
Public ColFusion As Integer

'---> Collection des champs associ�s � une cellule
Public Fields As Collection

'---> Autoriser l'acc�s au d�tail depuis cette cellule
Public UseAccesDet As Boolean

Public Function GetBordureByIndex(ByVal Index As Integer)

Dim Bas As Boolean
Dim Haut As Boolean
Dim Gauche As Boolean
Dim Droite As Boolean


Select Case Index

    Case 0
    
        Bas = False
        Haut = False
        Gauche = False
        Droite = False
                
    Case 1
    
        Bas = True
        Haut = True
        Gauche = True
        Droite = False
        
    Case 2
    
        Bas = True
        Haut = True
        Gauche = False
        Droite = True
            
    Case 3
    
        Bas = False
        Haut = True
        Gauche = True
        Droite = True
        
    Case 4
    
        Bas = True
        Haut = False
        Gauche = True
        Droite = True
    Case 5
    
        Bas = False
        Haut = False
        Gauche = True
        Droite = False
    
    Case 6
    
        Bas = False
        Haut = True
        Gauche = False
        Droite = False
    
    Case 7
    
        Bas = False
        Haut = False
        Gauche = False
        Droite = True
    
    Case 8
    
        Bas = True
        Haut = False
        Gauche = False
        Droite = False
    
    Case 9
    
        Bas = False
        Haut = False
        Gauche = True
        Droite = True
    
    Case 10
    
        Bas = True
        Haut = True
        Gauche = False
        Droite = False
    
    Case 11
    
        Bas = False
        Haut = True
        Gauche = True
        Droite = False
    
    Case 12
    
        Bas = False
        Haut = True
        Gauche = False
        Droite = True
    
    Case 13
    
        Bas = True
        Haut = False
        Gauche = True
        Droite = False
    
    Case 14
    
        Bas = True
        Haut = False
        Gauche = False
        Droite = True
    
    Case 15
    
        Bas = True
        Haut = True
        Gauche = True
        Droite = True
        
End Select

BordureBas = Bas
BordureDroite = Droite
BordureGauche = Gauche
BordureHaut = Haut
    
End Function


Private Sub Class_Initialize()
    Set Fields = New Collection
    Me.ColFusion = 1
    Me.IsFusion = False
End Sub
