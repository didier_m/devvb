VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.MDIForm Turbo_Maq 
   Appearance      =   0  'Flat
   BackColor       =   &H80000004&
   ClientHeight    =   8460
   ClientLeft      =   705
   ClientTop       =   2310
   ClientWidth     =   14235
   Icon            =   "TM-Turbo_Maq.frx":0000
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Picture2 
      Align           =   3  'Align Left
      Height          =   7410
      Left            =   0
      ScaleHeight     =   7350
      ScaleWidth      =   1635
      TabIndex        =   3
      Top             =   0
      Visible         =   0   'False
      Width           =   1695
   End
   Begin MSComDlg.CommonDialog OpenDlg 
      Left            =   5880
      Top             =   4680
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1050
      Left            =   0
      ScaleHeight     =   1050
      ScaleWidth      =   14235
      TabIndex        =   0
      Top             =   7410
      Visible         =   0   'False
      Width           =   14235
      Begin VB.PictureBox Tempo 
         AutoRedraw      =   -1  'True
         FillColor       =   &H00C00000&
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   1995
         TabIndex        =   2
         Top             =   720
         Width           =   2055
      End
      Begin VB.Image Ok 
         Height          =   480
         Left            =   1440
         Picture         =   "TM-Turbo_Maq.frx":5C12
         Top             =   0
         Visible         =   0   'False
         Width           =   630
      End
      Begin VB.Image Aide 
         Height          =   480
         Left            =   720
         Picture         =   "TM-Turbo_Maq.frx":6C54
         Top             =   0
         Visible         =   0   'False
         Width           =   630
      End
      Begin VB.Image Annuler 
         Height          =   480
         Left            =   0
         Picture         =   "TM-Turbo_Maq.frx":7C96
         Top             =   0
         Visible         =   0   'False
         Width           =   630
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   3855
      End
   End
   Begin VB.Menu mnuFichier 
      Caption         =   ""
      Begin VB.Menu mnuOpenSample 
         Caption         =   ""
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuSep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSave 
         Caption         =   ""
      End
      Begin VB.Menu mnuSaveQuick 
         Caption         =   ""
         Visible         =   0   'False
      End
      Begin VB.Menu menuExportWord 
         Caption         =   "Exporter vers WORD"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuQuit 
         Caption         =   ""
         Shortcut        =   ^Q
      End
   End
   Begin VB.Menu mnuOutils 
      Caption         =   ""
      Begin VB.Menu mnuSections 
         Caption         =   ""
         Begin VB.Menu mnuAddSection 
            Caption         =   ""
            Shortcut        =   {F2}
         End
         Begin VB.Menu mnuDelSection 
            Caption         =   ""
            Shortcut        =   ^{F2}
         End
      End
      Begin VB.Menu mnuTableaux 
         Caption         =   ""
         Begin VB.Menu mnuAddTableau 
            Caption         =   ""
            Shortcut        =   {F5}
         End
         Begin VB.Menu mnuDeltableau 
            Caption         =   ""
            Shortcut        =   ^{F5}
         End
         Begin VB.Menu mnuSepTab 
            Caption         =   "-"
         End
         Begin VB.Menu mnuOptionTab 
            Caption         =   ""
            Shortcut        =   +{F5}
         End
      End
   End
   Begin VB.Menu mnuAffichage 
      Caption         =   ""
      Begin VB.Menu mnuStructure 
         Caption         =   ""
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuPropriete 
         Caption         =   ""
         Shortcut        =   ^P
      End
      Begin VB.Menu mnufield 
         Caption         =   ""
         Shortcut        =   {F6}
      End
      Begin VB.Menu mnuProgiciel 
         Caption         =   ""
         Shortcut        =   {F7}
      End
      Begin VB.Menu mnuMapField 
         Caption         =   ""
      End
      Begin VB.Menu mnuView 
         Caption         =   ""
      End
      Begin VB.Menu mnuTitre 
         Caption         =   ""
      End
      Begin VB.Menu mnuLibelVariable 
         Caption         =   ""
      End
      Begin VB.Menu mnuMail 
         Caption         =   ""
      End
   End
   Begin VB.Menu mnuApropos 
      Caption         =   ""
      Begin VB.Menu mnuAppDeal 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "Turbo_Maq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub MDIForm_Activate()
Dim strLang As String
Dim lpBuffer As String
Dim lpKey As String
Dim Res As Long

On Error Resume Next

'-> R�cup�ration de la variable Langue
LangueLoad (0)

'On Error Resume Next

Dim aLb As Libelle

'-> Cr�er l'objet de r�f�rence
Set aLb = Libelles("FRMEASYPRINT")

'-> Charger les libell�s des objets de la feuille
Me.Caption = aLb.GetCaption(1)

'Fichier
Set aLb = Libelles("MNUFICHIER")
Me.mnuFichier.Caption = aLb.GetCaption(1)
Me.mnuSave.Caption = aLb.GetCaption(4)
Me.mnuQuit.Caption = aLb.GetCaption(6)
Me.mnuOpenSample.Caption = aLb.GetCaption(7)
Me.mnuSaveQuick.Caption = aLb.GetCaption(11)

'Outils
Set aLb = Libelles("MNUOUTILS")
Me.mnuOutils.Caption = aLb.GetCaption(1)
Me.mnuSections.Caption = aLb.GetCaption(2)
Me.mnuAddSection.Caption = aLb.GetCaption(3)
Me.mnuDelSection.Caption = aLb.GetCaption(4)
Me.mnuTableaux.Caption = aLb.GetCaption(5)
Me.mnuAddTableau.Caption = aLb.GetCaption(6)
Me.mnuDeltableau.Caption = aLb.GetCaption(7)
Me.mnuOptionTab.Caption = aLb.GetCaption(8)

'Affichage
Set aLb = Libelles("MNUAFFICHAGE")
Me.mnuAffichage.Caption = aLb.GetCaption(1)
Me.mnuStructure.Caption = aLb.GetCaption(2)
Me.mnuPropriete.Caption = aLb.GetCaption(3)
Me.mnufield.Caption = aLb.GetCaption(4)
Me.mnuProgiciel.Caption = aLb.GetCaption(5)
Me.mnuMapField.Caption = aLb.GetCaption(6)
Me.mnuView.Caption = aLb.GetCaption(7)
Me.mnuTitre.Caption = aLb.GetCaption(8)
Me.mnuLibelVariable.Caption = aLb.GetCaption(9)
Me.mnuMail.Caption = aLb.GetCaption(10)

'Maquette de transition
Set aLb = Libelles("MNUMAQUETTETRANSITION")

'Fen�tre
Set aLb = Libelles("MNUFENETRE")

'A propos de
Set aLb = Libelles("MNUAPROPOS")
Me.mnuApropos.Caption = aLb.GetCaption(1)
Me.mnuAppDeal.Caption = aLb.GetCaption(2)

'-> Chargement des options par d�faut des tableaux
Set aLb = Libelles("FRMPARAMTABLEAU")
DefNbLig = CInt(aLb.GetToolTip(1))
DefNbCol = CInt(aLb.GetToolTip(2))
DefLargeurCol = CSng(Convert(aLb.GetToolTip(3)))
DefHauteurLig = CSng(Convert(aLb.GetToolTip(4)))
DefBold = CBool(aLb.GetToolTip(5))
DefItalic = CBool(aLb.GetToolTip(6))
DefUnderline = CBool(aLb.GetToolTip(7))
DefALignCell = CInt(aLb.GetToolTip(8))
DefBackcolor = CLng(aLb.GetToolTip(18))
DefFontColor = CLng(aLb.GetToolTip(19))
DefGauche = CBool(aLb.GetToolTip(20))
DefDroite = CBool(aLb.GetToolTip(21))
DefHaut = CBool(aLb.GetToolTip(22))
DefBas = CBool(aLb.GetToolTip(23))
DefRetourLig = CBool(aLb.GetToolTip(28))
DefFontName = aLb.GetToolTip(29)
DefFontSize = CInt(aLb.GetToolTip(30))

'-> Liberer le pointeur
Set aLb = Nothing

GestMenu

End Sub

Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)

Dim Res As Boolean

Res = GestOuverture(4)
If Res Then
    Cancel = 1
Else
    End
End If


End Sub

Private Sub MDIForm_Resize()
On Error Resume Next
If Maquettes.Count <> 0 Then
    Me.Picture2.Height = Me.Height - 1000
    frmNaviga.Height = Me.Height - 1000
    frmWeb.Width = Turbo_Maq.Width - frmNaviga.TreeNaviga.Width - 290
    frmWeb.Height = Turbo_Maq.Height - 865
    frmWeb.Left = frmNaviga.Width
    frmWeb.Top = 0
End If
End Sub

Private Sub MDIForm_Unload(Cancel As Integer)
    End
End Sub

Private Sub menuExportWord_Click()
'
''---> Cette proc�dure cr�er un r�f�renciel d'objets Word � utiliser pour les exports _
'vers MS Word
'
'Dim aMaq As Maquette
'Dim NomObj As String
'Dim aSection As Section
'Dim aTb As Tableau
'Dim Doc As Document
'
'Me.MousePointer = 11
''-> Pointer sur la maquette
'Set aMaq = Maquettes("MAQUETTE")
'
''-> Cr�er un nouveau document
'Set Doc = New Document
'
''-> On enregistrer dans C:\Trvaialv51
'For i = 1 To aMaq.nEntries - 1
'    '-> R�cup�rer l'entr�e
'    NomObj = aMaq.GetOrdreAffichage(i)
'    If UCase$(Entry(1, NomObj, "-")) = "SCT" Then
'        '-> Pointer sur l'objet section
'        Set aSection = aMaq.Sections(UCase$(Entry(2, NomObj, "-")))
'        '-> Exporter la section
'        ExporteSectionWord aSection, Doc, aMaq
'        Set aSection = Nothing
'    ElseIf UCase$(Entry(1, NomObj, "-")) = "TB" Then
'        '-> Pointer sur l'objet tableau
'        Set aTb = aMaq.Tableaux(UCase$(Entry(2, NomObj, "-")))
'        '-> Exporter l'ensemble du tableau
'        ExporteTableauWord aTb, Doc, aMaq
'        Set aTb = Nothing
'    End If
'
'Next
'
''-> Il faut maintenant enregistrer le document sur le disque
'Doc.SaveAs "C:\TravailV51\Bal01-01.DOC"
'Doc.Application.Visible = True
'
'Set Doc = Nothing
'
'
'Me.MousePointer = 0
'
'End Sub
'Private Sub ExporteTableauWord(aTb As Tableau, Doc As Document, aMaq As Maquette)
'
''---> Proc�dure qui exporte un tableau sous WORD
'Dim aBlock As Block
'Dim i As Integer
'
'For i = 1 To aTb.Blocks.Count
'    '-> Pointer sur le block de cellule
'    Set aBlock = aTb.Blocks(i)
'    '-> Exporter le block de cellule
'    ExporteBlockWord aBlock, Doc, aMaq
'Next
'
'
''-> Lib�rer le pointeur
'Set aBlock = Nothing
'
'End Sub
'Private Sub ExporteBlockWord(aBlock As Block, Doc As Document, aMaq As Maquette)
'
''---> Proc�dure qui exporte un block sous WORD
'Dim aShape As Object
'Dim aTable As Table
'
''-> Cr�ation de la shape qui va recevoir le block de donn�es
'Set aShape = Doc.Shapes.AddTextbox(msoTextOrientationHorizontal, 0, 0, _
'             Doc.Application.CentimetersToPoints(aBlock.Largeur), _
'             Doc.Application.CentimetersToPoints(aBlock.Hauteur))
'
''-> Supprimer les marges internes de la shape que l'on vient de cr�er
'aShape.TextFrame.MarginBottom = 0
'aShape.TextFrame.MarginRight = 0
'aShape.TextFrame.MarginLeft = 0
'aShape.TextFrame.MarginTop = 0
'
''-> Fond Transparent
'aShape.Fill.Visible = msoFalse
'aShape.Fill.Transparency = 0#
'
''-> Supprimer les bordures de l'objet Shape
'aShape.Line.Visible = msoFalse
'
''-> Pointer sur l'objet shape
'aShape.Select
'
''-> Ajouter le tableau qui correspond au block
'Set aTable = Doc.Tables.Add(Doc.Application.Selection.Range, aBlock.NbLigne, aBlock.NbCol)
'
''-> Donner un nom � la shape
'aShape.Name = UCase$(aBlock.NomTb) & "|" & UCase$(aBlock.Nom)
'
''-> Donner la bonne largeur aux diff�rentes colonnes
'For i = 1 To aBlock.NbCol
'    aTable.Columns(i).Width = Doc.Application.CentimetersToPoints(aBlock.GetLargeurCol(i))
'Next
'
'
'End Sub
'
'
'Private Sub ExporteSectionWord(aSection As Section, Doc As Document, aMaq As Maquette)
'
''---> Cette fonction Exporte le contenu de la section dans le document sp�cifi�
'Dim CurrentShape As Object
'Dim aRtf As RichTextBox
'Dim i As Integer
'Dim aCadre As Cadre
'Dim NomObjet As String
'
''-> Cr�er dans un premier temps la shape qui recevra la section
'Set CurrentShape = Doc.Shapes.AddTextbox(msoTextOrientationHorizontal, 0, 0, Doc.Application.CentimetersToPoints(aSection.Largeur), Doc.Application.CentimetersToPoints(aSection.Hauteur))
'CurrentShape.Select
'
''-> Lui donner un nom
'CurrentShape.Name = UCase$(aSection.Nom)
'
''-> Gestion du contour
'If Not aSection.Contour Then
'    CurrentShape.Line.Visible = msoFalse
'    CurrentShape.TextFrame.MarginBottom = 0#
'    CurrentShape.TextFrame.MarginRight = 0#
'    CurrentShape.TextFrame.MarginLeft = 0#
'    CurrentShape.TextFrame.MarginTop = 0#
'Else
'    CurrentShape.TextFrame.MarginBottom = 0
'    CurrentShape.TextFrame.MarginRight = 0
'    CurrentShape.TextFrame.MarginLeft = 0
'    CurrentShape.TextFrame.MarginTop = 0
'End If
'
''-> Setting de la couleur de fonds
'CurrentShape.Fill.ForeColor.RGB = aSection.BackColor
'CurrentShape.Fill.Transparency = 0#
'CurrentShape.Fill.Solid
'CurrentShape.Fill.Visible = msoTrue
'
''-> Res�lectionner l'objet shape maitre
'CurrentShape.Select
'
''-> Impression des cadres associ�s les images seront elles cr�es en dynamique lors de l'impression finale
'For i = 1 To aSection.nEntries - 1
'    DoEvents
'    NomObjet = aSection.GetOrdreAffichage(i)
'    If UCase$(Entry(1, NomObjet, "-")) = "CDR" Then
'        '-> Pointer sur le cadre
'        Set aCadre = aSection.Cadres(UCase$(Entry(2, NomObjet, "-")))
'        '-> Exporter l'objet
'        ExporteCadreWord aCadre, Doc, aMaq, aSection
'        '-> Liberer le pointeur
'        Set aCadre = Nothing
'    End If
'Next
'
'
''-> Lib�rer pointeur de cadre
'Set aCadre = Nothing
'
''-> Lib�rer le pointeur sur la feuille maitre
'Set aFrmSection = Nothing
'
'End Sub
'Private Sub ExporteCadreWord(aCadre As Cadre, Doc As Document, aMaq As Maquette, aSection As Section)
'
''---> Cette fonction exporte un cadre dans Word
'Dim aCadreWord As Object
'Dim BordureHaut As Object
'Dim BordureBas As Object
'Dim BordureGauche   As Object
'Dim BordureDroite As Object
'Dim TOTO As Object
'
''On Error GoTo ErrorObj
'
''-> Cr�er le nouveau cadre et le s�lectionner
'Set aCadreWord = Doc.Shapes.AddTextbox(msoTextOrientationHorizontal, Doc.Application.CentimetersToPoints(aCadre.Left), Doc.Application.CentimetersToPoints(aCadre.Top), Doc.Application.CentimetersToPoints(aCadre.Largeur), Doc.Application.CentimetersToPoints(aCadre.Hauteur))
'aCadreWord.Select
'aCadreWord.Name = UCase$(aSection.Nom) & "|" & UCase$(aCadre.Nom)
'
'aCadreWord.Select
'
''-> Couleur de fond
'Doc.Application.Selection.ShapeRange.Fill.ForeColor.RGB = aCadre.BackColor
'
''-> Supprimer les contours
'Doc.Application.Selection.ShapeRange.Line.Visible = msoFalse
'
''-> Positionner la marge interne
'Doc.Application.Selection.ShapeRange.TextFrame.MarginBottom = Doc.Application.CentimetersToPoints(aCadre.MargeInterne)
'Doc.Application.Selection.ShapeRange.TextFrame.MarginRight = Doc.Application.CentimetersToPoints(aCadre.MargeInterne)
'Doc.Application.Selection.ShapeRange.TextFrame.MarginLeft = Doc.Application.CentimetersToPoints(aCadre.MargeInterne)
'Doc.Application.Selection.ShapeRange.TextFrame.MarginTop = Doc.Application.CentimetersToPoints(aCadre.MargeInterne)
'
''---> GESTION DES BORDURES DU CADRE
'
''Haut
'Set BordureHaut = Doc.Shapes.AddLine( _
'    Doc.Application.CentimetersToPoints(aCadre.Left), _
'    Doc.Application.CentimetersToPoints(aCadre.Top), _
'    Doc.Application.CentimetersToPoints(aCadre.Left) + Doc.Application.CentimetersToPoints(aCadre.Largeur), _
'    Doc.Application.CentimetersToPoints(aCadre.Top))
'BordureHaut.Name = UCase$(aSection.Nom) & "|" & UCase$(aCadre.Nom) & "|HAUT"
'BordureHaut.Select
'
'If aCadre.BordureHaut Then
'    Doc.Application.Selection.ShapeRange.Line.ForeColor.RGB = 0
'Else
'    Doc.Application.Selection.ShapeRange.Line.ForeColor.RGB = aCadre.BackColor
'End If
'
''Bas
'Set BordureBas = Doc.Shapes.AddLine( _
'    Doc.Application.CentimetersToPoints(aCadre.Left), _
'    Doc.Application.CentimetersToPoints(aCadre.Top) + Doc.Application.CentimetersToPoints(aCadre.Hauteur), _
'    Doc.Application.CentimetersToPoints(aCadre.Left) + Doc.Application.CentimetersToPoints(aCadre.Largeur), _
'    Doc.Application.CentimetersToPoints(aCadre.Top) + Doc.Application.CentimetersToPoints(aCadre.Hauteur))
'BordureBas.Name = UCase$(aSection.Nom) & "|" & UCase$(aCadre.Nom) & "|BAS"
'BordureBas.Select
'
'If aCadre.BordureBas Then
'    Doc.Application.Selection.ShapeRange.Line.ForeColor = 0
'Else
'    Doc.Application.Selection.ShapeRange.Line.ForeColor = aCadre.BackColor
'End If
'
''Gauche
'Set BordureGauche = Doc.Shapes.AddLine( _
'    Doc.Application.CentimetersToPoints(aCadre.Left), _
'    Doc.Application.CentimetersToPoints(aCadre.Top), _
'    Doc.Application.CentimetersToPoints(aCadre.Left), _
'    Doc.Application.CentimetersToPoints(aCadre.Top) + Doc.Application.CentimetersToPoints(aCadre.Hauteur))
'BordureGauche.Name = UCase$(aSection.Nom) & "|" & UCase$(aCadre.Nom) & "|GAUCHE"
'BordureGauche.Select
'
'If aCadre.BordureGauche Then
'    Doc.Application.Selection.ShapeRange.Line.ForeColor = 0
'Else
'    Doc.Application.Selection.ShapeRange.Line.ForeColor = aCadre.BackColor
'End If
'
''Droite
'Set BordureDroite = Doc.Shapes.AddLine( _
'    Doc.Application.CentimetersToPoints(aCadre.Left) + Doc.Application.CentimetersToPoints(aCadre.Largeur), _
'    Doc.Application.CentimetersToPoints(aCadre.Top), _
'    Doc.Application.CentimetersToPoints(aCadre.Left) + Doc.Application.CentimetersToPoints(aCadre.Largeur), _
'    Doc.Application.CentimetersToPoints(aCadre.Top) + Doc.Application.CentimetersToPoints(aCadre.Hauteur))
'BordureDroite.Name = UCase$(aSection.Nom) & "|" & UCase$(aCadre.Nom) & "|DROITE"
'BordureDroite.Select
'
'If aCadre.BordureDroite Then
'    Doc.Application.Selection.ShapeRange.Line.ForeColor = 0
'Else
'    Doc.Application.Selection.ShapeRange.Line.ForeColor = aCadre.BackColor
'End If
'
''-> S�lectionner les bordures pour y appliquer la largueur des bordures
'Doc.Shapes.Range(Array(BordureHaut.Name, BordureBas.Name, BordureDroite.Name, BordureGauche.Name)).Select
'
'Select Case aCadre.LargeurBordure
'    Case 1
'        Doc.Application.Selection.ShapeRange.Line.Weight = 0.75
'    Case 2
'        Doc.Application.Selection.ShapeRange.Line.Weight = 1.5
'    Case 3
'        Doc.Application.Selection.ShapeRange.Line.Weight = 2.25
'    Case 4
'        Doc.Application.Selection.ShapeRange.Line.Weight = 3
'    Case 5
'        Doc.Application.Selection.ShapeRange.Line.Weight = 3.75
'    Case 6
'        Doc.Application.Selection.ShapeRange.Line.Weight = 4.5
'    Case 7
'        Doc.Application.Selection.ShapeRange.Line.Weight = 5.25
'    Case 8
'        Doc.Application.Selection.ShapeRange.Line.Weight = 6
'    Case 9
'        Doc.Application.Selection.ShapeRange.Line.Weight = 6.75
'    Case 10
'        Doc.Application.Selection.ShapeRange.Line.Weight = 7.5
'End Select
'
''-> Retraiter les bordures
'BordureHaut.Select
'If Not aCadre.BordureHaut Then
'    Doc.Application.Selection.ShapeRange.ZOrder msoSendToBack
'    Doc.Application.Selection.ShapeRange.IncrementTop (Doc.Application.Selection.ShapeRange.Line.Weight * 2)
'Else
'    Doc.Application.Selection.ShapeRange.IncrementTop (Doc.Application.Selection.ShapeRange.Line.Weight / 2)
'End If
'
'BordureDroite.Select
'If Not aCadre.BordureDroite Then
'    Doc.Application.Selection.ShapeRange.ZOrder msoSendToBack
'    Doc.Application.Selection.ShapeRange.IncrementLeft -(Doc.Application.Selection.ShapeRange.Line.Weight * 2)
'Else
'    Doc.Application.Selection.ShapeRange.IncrementLeft -(Doc.Application.Selection.ShapeRange.Line.Weight / 2)
'End If
'
'BordureGauche.Select
'If Not aCadre.BordureGauche Then
'    Doc.Application.Selection.ShapeRange.ZOrder msoSendToBack
'    Doc.Application.Selection.ShapeRange.IncrementLeft (Doc.Application.Selection.ShapeRange.Line.Weight * 2)
'Else
'    Doc.Application.Selection.ShapeRange.IncrementLeft (Doc.Application.Selection.ShapeRange.Line.Weight / 2)
'End If
'
'BordureBas.Select
'If Not aCadre.BordureBas Then
'    Doc.Application.Selection.ShapeRange.ZOrder msoSendToBack
'    Doc.Application.Selection.ShapeRange.IncrementTop -(Doc.Application.Selection.ShapeRange.Line.Weight * 2)
'Else
'    Doc.Application.Selection.ShapeRange.IncrementTop -(Doc.Application.Selection.ShapeRange.Line.Weight / 2)
'End If
'
'Dim aGroup As Object
'
''-> Ne cr�er qu'un seul objet entre le cadre et les bordures en les groupant
'Doc.Shapes.Range(Array(aCadreWord.Name, BordureHaut.Name, BordureBas.Name, BordureDroite.Name, BordureGauche.Name)).Select
'Set aGroup = Doc.Application.Selection.ShapeRange.Group
'aGroup.Name = "GROUPE|" & UCase$(aSection.Nom) & "|" & UCase$(aCadre.Nom)
'
'
'''-> Pointer sur le groupe pour le d�grouper
''Set TOTO = Doc.Shapes("GROUPE|" & UCase$(aSection.Nom) & "|" & UCase$(aCadre.Nom))
''TOTO.Ungroup
''
'''-> Pointer sur le cadre
''Set aCadreWord = Doc.Shapes(UCase$(aSection.Nom) & "|" & UCase$(aCadre.Nom))
''aCadreWord.TextFrame.TextRange.Select
''Doc.Application.Selection.Text = "Ciucou"
''
'''-> Regrouper les objets
''Doc.Shapes.Range(Array(aCadreWord.Name, BordureHaut.Name, BordureBas.Name, BordureDroite.Name, BordureGauche.Name)).Select
''Set TOTO = Doc.Application.Selection.ShapeRange.Group
'
'
'Exit Sub
'
'ErrorObj:
'    Resume
'
End Sub



Private Sub mnuAddSection_Click()
    
Dim aLb As Libelle
Dim aSection As Section
Dim NomObject As String
Dim aMaq As Maquette
Dim aForm As frmSection

    
    Set aLb = Libelles("MESSAGE")
    Set aMaq = Maquettes("MAQUETTE")
    
    '-> Cr�er un nouvel objet
    NomObject = InputBox(aLb.GetCaption(11), aLb.GetCaption(14), aLb.GetCaption(15) & aMaq.nEntries)
    NomObject = Trim(NomObject)
    If NomObject = "" Then
        Set aLb = Nothing
        Set aMaq = Nothing
        Exit Sub
    End If
    
    '-> Pas de caract�re interdits
    If Not IsLegalName(NomObject) Then
        Set aLb = Libelles("MESSAGE")
        MsgBox aLb.GetCaption(5) & Chr(13) & aLb.GetCaption(6), vbCritical + vbOKOnly, aLb.GetToolTip(6)
        Set aLb = Nothing
        Set aMaq = Nothing
        Exit Sub
    End If
    
    If InStr(1, NomObject, "-") <> 0 Then
        Set aLb = Libelles("MESSAGE")
        MsgBox aLb.GetCaption(47), vbCritical + vbOKOnly, aLb.GetToolTip(47)
        Exit Sub
    End If
    
    '-> V�rifier le mot cl�
    If Not GetValidName(NomObject) Then
        ErrorName
        Exit Sub
    End If
        
    '-> V�rifier que le nom n'existe pas d�ja
    If aMaq.GetSectionExist(NomObject) Or aMaq.GetTableauExist(NomObject) Then
        MsgBox aLb.GetCaption(12), vbCritical + vbOKOnly, aLb.GetToolTip(12)
        Set aLb = Nothing
        Set aMaq = Nothing
        Exit Sub
    End If
    
    '-> Cr�er le nouvel objet
    Set aSection = New Section
    aSection.Nom = NomObject
    aSection.Contour = False
    aSection.Hauteur = DefHauteurSection
    aSection.Largeur = aMaq.Largeur - (aMaq.MargeLeft * 2)
    aSection.AlignementLeft = 4
    aSection.AlignementTop = 1
    aSection.BackColor = 16777215 'Blanc
    aSection.IdAffichage = aMaq.AddOrdreAffichage("SCT-" & UCase$(NomObject))

    '-> Ajouter le nouvel objet dans la collection
    aMaq.Sections.Add aSection, UCase$(NomObject)
    
    '-> Initialiser la feuille qui va bien
    Set aForm = New frmSection
    
    '-> Ajouer dans la collection des feuilles
    aMaq.frmSections.Add aForm, UCase$(NomObject)
            
    '-> Afficher la nouvelle feuille de section
    Set aForm.SectionCours = aSection
    Set ObjCours = aMaq
    aForm.Tag = UCase$(NomObject)
    aForm.RangText.Tag = "SCT-" & UCase$(NomObject)
    aForm.Show
    
    '-> Lib�rer le pointeur sur la maquette et sur le libelle, et sur la section
    Set aMaq = Nothing
    Set aLb = Nothing
    Set aSection = Nothing
    Set aForm = Nothing
    
    '-> G�rer les menus
    GestMenu
    
    
End Sub



Private Sub mnuAddTableau_Click()
    frmNewTableau.Show vbModal
End Sub

Private Sub mnuAppDeal_Click()
    
Load AproposDe
AproposDe.Show vbModal
    
End Sub


Private Sub mnuDelSection_Click()

Dim aLb As Libelle
Dim Rep

'-> Pointer sur le libelle
Set aLb = Libelles("MESSAGE")

'-> V�rifier que l'on pointe sur une section
If TypeOf ObjCours Is Section Then
Else
    MsgBox aLb.GetCaption(42), vbInformation + vbOKOnly, aLb.GetToolTip(42)
    Set aLb = Nothing
    Exit Sub
End If

'-> Demander la confirmation
Rep = MsgBox(aLb.GetCaption(40) & Chr(13) & ObjCours.Nom, vbQuestion + vbYesNo, aLb.GetToolTip(40))
If Rep = vbYes Then
    '-> Supprimer la section
    EraseSection ObjCours.Nom, Maquettes("MAQUETTE")
    '-> G�rer les menu
    GestMenu
    '-> Rendre l'objet en cours la maquette
    Set ObjCours = Maquettes("MAQUETTE")
    '-> Afficher les prorpi�t�s
    If frmPropVisible Then frmProp.DisplayProperties
    
End If

Set aLb = Nothing

End Sub

Private Sub mnuDeltableau_Click()

Dim aLb As Libelle
Dim Rep

'-> Pointer sur le libelle
Set aLb = Libelles("MESSAGE")

'-> V�rifier que l'on pointe sur une section
If TypeOf ObjCours Is Tableau Then
Else
    MsgBox aLb.GetCaption(43), vbInformation + vbOKOnly, aLb.GetToolTip(43)
    Set aLb = Nothing
    Exit Sub
End If

'-> Demander la confirmation
Rep = MsgBox(aLb.GetCaption(40) & Chr(13) & ObjCours.Nom, vbQuestion + vbYesNo, aLb.GetToolTip(40))
If Rep = vbYes Then
    '-> Supprimer le tableau
    EraseFrm = True
    EraseTableau ObjCours.Nom, Maquettes("MAQUETTE")
    EraseFrm = False
    '-> G�rer les menu
    GestMenu
    '-> Rendre l'objet en cours la maquette
    Set ObjCours = Maquettes("MAQUETTE")
End If


Set aLb = Nothing

End Sub

Private Sub mnufield_Click()
    frmFields.Show
    frmFields.ZOrder
End Sub

Private Sub mnuMapField_Click()
    frmMapFields.Show vbModal
End Sub

Private Sub mnuOpenSample_Click()
    GestOuverture 3
End Sub

Private Sub mnuOptionTab_Click()
    frmParamTableau.Show vbModal
End Sub

Private Sub mnuProgiciel_Click()

'-> Afficher la feuille
strRetour = ""
frmChoixApp.Show vbModal
If strRetour <> "" Then V6Rub = strRetour

End Sub

Private Sub mnuPropriete_Click()


If TypeOf ObjCours Is Block Then Exit Sub

frmProp.Show
frmProp.ZOrder

End Sub

Private Sub mnuQuit_Click()
    GestOuverture 4
End Sub

Private Sub mnuSave_Click()
    
    save ""
End Sub

Private Sub mnuTitre_Click()
    
    frmTitre.Show vbModal
End Sub

Private Sub mnuLibelVariable_Click()
    
    frmLibelVariable.Show vbModal
End Sub

Private Sub mnuMail_Click()
    
    frmMail.Show vbModal
End Sub

Private Sub mnuStructure_Click()
    
    frmNaviga.Show
    frmNaviga.ZOrder
    
End Sub

Private Function GestOuverture(ByVal idOpen As Integer) As Boolean

'-> Proc�dure charg�e de g�rer les diff�rents type d'ouverture

Dim aLb As Libelle
Dim Rep
Dim lpBuffer As String
Dim Res As Long
Dim IniPath As String
Dim Extension As String
Dim ExtensionOperat As String
Dim DefFileName As String
Dim FileName As String
Dim lngResult As Long
Dim strBuffer As String
Dim NomFichier As String

On Error GoTo ErrorOpen

'-> Initialiser les variables de dessin
TailleLue = 0
TailleTotale = 0
Turbo_Maq.Tempo.Cls

'-> v�rifier s'il y a une maquette en cours
If Maquettes.Count = 0 Then
Else
    '-> Supprimer la maquette existante
    EraseMaquette
    '-> Gestion des menus
    GestMenu
End If
    
'-> traiter la demande
Select Case idOpen
            
    Case 3 'Ouverture � partir de
        '-> R�pertoire par d�faut sp�cifi� dans le registre par la cle DEALTEMPO
        If Not FirstOpen Then
            '-> V�rifier si un path est sp�cifi�
            If OpenPath <> "" Then
                Me.OpenDlg.InitDir = OpenPath
                FirstOpen = True
            End If
        Else
            Me.OpenDlg.InitDir = ""
        End If
        
        '-> G�n�rer une erreur sur ANNULER
        Me.OpenDlg.CancelError = True
        
        '-> R�cup�rer le code op�rateur pour l'extension par d�faut
        DefFileName = "*.MaqGui"
        Extension = "Fichiers Maquette (*.MaqGui|*.MaqGui|Tous les fichiers (*.*)|*.*"
        ExtensionOperat = ""
        
        Me.OpenDlg.FileName = DefFileName
        Me.OpenDlg.DefaultExt = DefFileName
        Me.OpenDlg.Filter = Extension
        
        '-> Flags d'ouverture
        Me.OpenDlg.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly Or _
                           cdlOFNPathMustExist
        
        '-> Afficher la feuille
        Me.OpenDlg.ShowOpen
        
        '-> V�rifier ici que le fichier n'est pas d�ja charg� THIERRY
        If IsLoadedFile(Me.OpenDlg.FileName) Then
            Set aLb = Libelles("MESSAGE")
            MsgBox aLb.GetCaption(2), vbExclamation + vbOKOnly, aLb.GetToolTip(2)
            Exit Function
        End If
        
        '-> Affecter le nom
        FileName = Me.OpenDlg.FileName
        OpenMaq FileName
    Case 4
        Unload Me

End Select

Set aLb = Nothing


Exit Function

ErrorOpen:

    
    MsgBox Err.Description & Chr(13) & Err.Number
    Set aLb = Nothing

End Function

Private Function IsLoadedFile(ByVal NomFile As String) As Boolean

'---> Cette proc�dure v�rifie si un fichier est d�ja charg�

On Error GoTo GestError

'-> Pointer sur le fichier
'Set aFichier = Fichiers(Trim(UCase$(NomFile)))

'-> Renvoyer une valeur de succ�s
IsLoadedFile = False

Exit Function

GestError:
    IsLoadedFile = False


End Function

Private Sub mnuView_Click()

Dim SpoolFile As String
Dim IntFile As Integer
Dim TempMaq As String
Dim Res As Long
Dim aSection As Section
Dim aTb As Tableau
Dim i As Integer
Dim aMaq As Maquette
Dim Tempo As String


On Error Resume Next

'-> Obtenir un nom de fichier temporaire pour la maquette
TempMaq = GetTempFileNameVB("MAQ")

'-> Enregistrer la maquette
save TempMaq, True

'-> Obtenir un nom de fichier temporaire pour le spool
SpoolFile = GetTempFileNameVB("DAT")

'-> Ouverture du fichier
IntFile = FreeFile
Open SpoolFile For Output As #IntFile

'-> Impression de l'entete
Print #IntFile, "%%GUI%%" & TempMaq

'-> Pointer sur la maquette
Set aMaq = Maquettes("MAQUETTE")

For i = 1 To aMaq.nEntries - 1
    Tempo = aMaq.GetOrdreAffichage(i)
    If UCase$(Entry(1, Tempo, "-")) = "SCT" Then
        Set aSection = aMaq.Sections(Entry(2, Tempo, "-"))
        aSection.PrintView IntFile
    Else
        Set aTb = aMaq.Tableaux(Entry(2, Tempo, "-"))
        aTb.PrintView IntFile
    End If
Next 'Pour toutes les entr�es

'-> Fermer tous les fichiers
Reset

'-> Rendre la main � la CPU
DoEvents

'-> Lancer la visu
Shell App.Path & "\Turbograph.exe ECRAN|" & SpoolFile, vbNormalFocus


End Sub

Public Sub LangueLoad(Index As Integer)

On Error GoTo GestError

Dim Pos As Integer
Dim Res As Long
Dim lpBuffer As String
Dim Result As Long

Me.MousePointer = 11

'-> Recherche du r�pertoire des fichiers langue
lpBuffer = App.Path & "\"
V6MqtPath = lpBuffer

'-> Charger le fichier du Turbo
If Not LoadMessprog(lpBuffer & "EasyPrint-0" & Index + 1 & ".lng") Then
    MsgBox "Impossible de charger le fichier langue : " & vbCrLf & lpBuffer & "EasyPrint-0" & Index + 1 & ".lng", vbCritical + vbOKOnly, "Erreur"
    End
End If
If Not LoadMessprog(lpBuffer & "Common-0" & Index + 1 & ".lng") Then
    MsgBox "Impossible de charger le fichier langue : " & vbCrLf & lpBuffer & "Common-0" & Index + 1 & ".lng", vbCritical + vbOKOnly, "Erreur"
    End
End If
    
'-> Setting du code langue
CodeLangue = "0" & Index + 1

'-> Chargement des s�parateurs de chiffres
lpBuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, lpBuffer, Len(lpBuffer))
SepDec = Mid$(lpBuffer, 1, Res - 1)
lpBuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, lpBuffer, Len(lpBuffer))
SepMil = Mid$(lpBuffer, 1, Res - 1)


'-> Redonner son aspect au pointeur de souris
Me.MousePointer = 0

Exit Sub

GestError:

    MsgBox "Erreur de chargement dans la page d'accueil", vbCritical, "Erreur"
    End
    
End Sub

