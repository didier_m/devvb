VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cadre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'**********************************************
'*                                            *
'* Cette classe contient la d�finition d'un   *
'* Objet cadre                                *
'*                                            *
'**********************************************

'---> D�finition des propri�t�s
Public Nom As String
Public Hauteur As Single
Public Largeur As Single
Public Top As Single
Public Left As Single
Public BackColor As Long
Public IdAffichage As Integer
Public MargeInterne As Single
Public IsRoundRect As Boolean

'-> index propre � l'objet pour la gestion des ordres d'affichage
Public IdOrdreAffichage As Integer

'---> Variable de stockage priv�
Private pBas As Boolean
Private pHaut As Boolean
Private pGauche As Boolean
Private pDroite As Boolean
Private pLargeur As Integer


'---> Pour Gestion des alignements
Public Locked As Boolean 'indique si l'alignement est verrouill�
Public MasterAlign As String 'R�f�rence de l'objet d'alignement sous la forme :
                ' [OBJ]|[NOMOBJ]-[IdAlign]-[Icone]
                ' [PAGE]-[IdAlign]-[Icone]
                
Public SlaveAlign As String 'R�f�rence de l'objet qui est align� sur lui
Public MasterIndexAlign As Integer 'Index de l'alignement
Public AlignX As Single
Public AlignY As Single

'---> Picturebox associ�
Public pBox As PictureBox
'---> Nom de la section mere
Public SectionName As String

Public Fields As Collection

Public Property Get LargeurBordure() As Integer
    LargeurBordure = pLargeur
End Property

Public Property Let LargeurBordure(ByVal vNewValue As Integer)
    On Error Resume Next
    pBox.DrawWidth = vNewValue
    pLargeur = vNewValue
End Property

Public Property Get BordureBas() As Boolean
    BordureBas = pBas
End Property

Public Property Let BordureBas(ByVal vNewValue As Boolean)
    
Dim Couleur As Long
Dim i As Integer
    
    '-> Enregistrer la valeur
    pBas = vNewValue
    If IsRoundRect Then Exit Property
    
    '-> Dessiner une bordure  dans tous les cas
    If vNewValue = True Then
        Couleur = 0
    Else
        Couleur = BackColor
    End If

    
    '-> Ajouter + 1 si la largeur est paire
    If (pLargeur / 2) = Fix(pLargeur / 2) Then
        i = pLargeur / 2
    Else
        i = (pLargeur + 1) / 2
    End If
    
    If i < 1 Then i = 1
    
    pBox.Line (0, pBox.ScaleHeight - i)-(pBox.ScaleWidth, pBox.ScaleHeight - i), Couleur
    
    
    
End Property

Public Property Get BordureHaut() As Boolean
    BordureHaut = pHaut
End Property

Public Property Let BordureHaut(ByVal vNewValue As Boolean)
    
Dim Couleur As Long
Dim i As Integer
    
    '-> Dessiner une bordure  dans tous les cas
    If vNewValue = True Then
        Couleur = 0
    Else
        Couleur = BackColor
    End If
    
    i = Fix(pLargeur / 2)
    
    pBox.Line (0, i)-(pBox.ScaleWidth, i), Couleur
    
    pHaut = vNewValue

End Property
Public Property Get BordureDroite() As Boolean
    BordureDroite = pDroite
End Property

Public Property Let BordureDroite(ByVal vNewValue As Boolean)

Dim Couleur As Long
Dim i As Integer
    
    pDroite = vNewValue
    If IsRoundRect Then Exit Property
    
    '-> Dessiner une bordure  dans tous les cas
    If vNewValue = True Then
        Couleur = 0
    Else
        Couleur = BackColor
    End If
        
    '-> Ajouter + 1 si la largeur est paire
    If (pLargeur / 2) = Fix(pLargeur / 2) Then
        i = pLargeur / 2
    Else
        i = (pLargeur + 1) / 2
    End If
    
    If i < 1 Then i = 1
        
    pBox.Line (pBox.ScaleWidth - i, 0)-(pBox.ScaleWidth - i, pBox.ScaleHeight), Couleur
    

End Property
Public Property Get BordureGauche() As Boolean
    BordureGauche = pGauche
End Property

Public Property Let BordureGauche(ByVal vNewValue As Boolean)

Dim Couleur As Long
Dim i As Integer
    
    pGauche = vNewValue
    If IsRoundRect Then Exit Property
    
    '-> Dessiner une bordure  dans tous les cas
    If vNewValue = True Then
        Couleur = 0
    Else
        Couleur = BackColor
    End If
    
    i = Fix(pLargeur / 2)
    
    pBox.Line (i, 0)-(i, pBox.ScaleHeight), Couleur
    

End Property

Public Sub AddTreeView()

'---> Proc�dure charg�e d'ajouer l'icone dans la feuille de navigation

Dim aNode As Node

Set aNode = frmNaviga.TreeNaviga.Nodes.Add(UCase$(SectionName), 4, UCase$(SectionName) & "|" & UCase$(Nom), Nom, "Cadre")
aNode.Tag = "CADRE"
aNode.Selected = True
Set aNode = Nothing


End Sub
Public Sub DelTreeView()

'---> Proc�dure charg�e d'ajouer l'icone dans la feuille de navigation

On Error Resume Next

Dim aNode As Node

frmNaviga.TreeNaviga.Nodes.Remove UCase$(SectionName) & "|" & UCase$(Nom)
Set aNode = Nothing


End Sub

Public Function GetBordureByIndex(ByVal Index As Integer)

Dim Bas As Boolean
Dim Haut As Boolean
Dim Gauche As Boolean
Dim Droite As Boolean


Select Case Index

    Case 0
    
        Bas = False
        Haut = False
        Gauche = False
        Droite = False
                
    Case 1
    
        Bas = True
        Haut = True
        Gauche = True
        Droite = False
        
    Case 2
    
        Bas = True
        Haut = True
        Gauche = False
        Droite = True
            
    Case 3
    
        Bas = False
        Haut = True
        Gauche = True
        Droite = True
        
    Case 4
    
        Bas = True
        Haut = False
        Gauche = True
        Droite = True
    Case 5
    
        Bas = False
        Haut = False
        Gauche = True
        Droite = False
    
    Case 6
    
        Bas = False
        Haut = True
        Gauche = False
        Droite = False
    
    Case 7
    
        Bas = False
        Haut = False
        Gauche = False
        Droite = True
    
    Case 8
    
        Bas = True
        Haut = False
        Gauche = False
        Droite = False
    
    Case 9
    
        Bas = False
        Haut = False
        Gauche = True
        Droite = True
    
    Case 10
    
        Bas = True
        Haut = True
        Gauche = False
        Droite = False
    
    Case 11
    
        Bas = False
        Haut = True
        Gauche = True
        Droite = False
    
    Case 12
    
        Bas = False
        Haut = True
        Gauche = False
        Droite = True
    
    Case 13
    
        Bas = True
        Haut = False
        Gauche = True
        Droite = False
    
    Case 14
    
        Bas = True
        Haut = False
        Gauche = False
        Droite = True
    
    Case 15
    
        Bas = True
        Haut = True
        Gauche = True
        Droite = True
        
End Select

BordureBas = Bas
BordureDroite = Droite
BordureGauche = Gauche
BordureHaut = Haut
    
End Function


Public Sub RefreshBordures()

Dim aFrmSection As frmSection
Dim aMaq As Maquette

    '-> Effacer les anciennes bordures
    Set aMaq = Maquettes("MAQUETTE")
    Set aFrmSection = aMaq.frmSections(UCase$(SectionName))
    aFrmSection.Cadre(IdAffichage).Cls
    Set aFrmSection = Nothing
    Set aMaq = Nothing
    
    If IsRoundRect Then
        SetRoundRect
    Else
        '-> Redessiner les bordures
        BordureGauche = pGauche
        BordureDroite = pDroite
        BordureHaut = pHaut
        BordureBas = pBas
    End If

End Sub


Public Sub MoveSlave()

'---> Cette proc�dure est appel�e par un objet maitre qui est d�plac�. _
Elle a pour but de d�placer son escalve s'il y en a un

If SlaveAlign = "" Then Exit Sub



Dim DefObject As String
Dim TypeObject As String
Dim NomObject As String
Dim ObjectToMove
Dim aSection As Section
Dim i As Integer

'-> Pointer sur la section
Set aSection = Maquettes("MAQUETTE").Sections(UCase$(SectionName))

For i = 1 To NumEntries(SlaveAlign, "-")

    '-> D�finitions
    DefObject = Entry(i, SlaveAlign, "-")
    TypeObject = Entry(1, DefObject, "|")
    NomObject = Entry(2, DefObject, "|")
    
    '-> Pointer sur l'objet � d�placer
    If UCase$(TypeObject) = "CDR" Then
        Set ObjectToMove = aSection.Cadres(UCase$(NomObject))
    ElseIf UCase$(TypeObject) = "BMP" Then
        Set ObjectToMove = aSection.Bmps(UCase$(NomObject))
    End If
    '-> D�placer l'esclave
    AlignOnObject ObjectToMove, Nom, ObjectToMove.MasterIndexAlign, ObjectToMove.AlignX, ObjectToMove.AlignY
    
    '-> D�placver les esclaves de l'escale
    ObjectToMove.MoveSlave
Next

'-> Lib�rer les ressources
Set ObjectToMove = Nothing
Set aSection = Nothing

End Sub


Public Sub ClearAlign()

'---> Cette fonction est appel�e pour supprimer les liens d'alignement chez les esclaves _
et chez les maitres

Dim aObject
Dim TypeObject As String
Dim NomObject As String
Dim DefObject As String
Dim aSection As Section
Dim i As Integer

Set aSection = Maquettes("MAQUETTE").Sections(UCase$(SectionName))

'-> Suppression chez le maitre.
If MasterAlign <> "" Then
    '-> V�rifier qu'il s'agit d'un lien objet et non d'un lien page
    If UCase$(Entry(1, MasterAlign, "-")) = "PAGE" Then
    Else
        '-> il faut supprimer l'entr�e dans la liste des esclaves du maitre
        DefObject = Entry(1, MasterAlign, "-")
        TypeObject = Entry(1, DefObject, "|")
        NomObject = Entry(2, DefObject, "|")
        '-> Pointer sur le maitre
        If UCase$(TypeObject) = "CDR" Then
            Set aObject = aSection.Cadres(UCase$(NomObject))
        ElseIf UCase$(TypeObject) = "BMP" Then
            Set aObject = aSection.Bmps(UCase$(NomObject))
        End If
        '-> supprimer l'entr�e dans la propri�t� SlaveAlign
        i = GetEntryIndex(aObject.SlaveAlign, "CDR|" & UCase$(Nom), "-")
        aObject.SlaveAlign = DeleteEntry(aObject.SlaveAlign, i, "-")
    End If
End If


'-> Suppression chez les esclaves
If SlaveAlign <> "" Then
    For i = 1 To NumEntries(SlaveAlign, "-")
        '-> il faut supprimer la r�f�rence au maitre chez les esclaves
        DefObject = Entry(i, SlaveAlign, "-")
        TypeObject = Entry(1, DefObject, "|")
        NomObject = Entry(2, DefObject, "|")
        '-> Pointer sur le maitre
        If UCase$(TypeObject) = "CDR" Then
            Set aObject = aSection.Cadres(UCase$(NomObject))
        ElseIf UCase$(TypeObject) = "BMP" Then
            Set aObject = aSection.Bmps(UCase$(NomObject))
        End If
        '-> Suppression du lien
        aObject.MasterAlign = ""
        aObject.MasterIndexAlign = 0
        aObject.Locked = False
        aObject.AlignX = 0
        aObject.AlignY = 0
    Next
End If
        
Set aSection = Nothing
Set aObject = Nothing


End Sub

Private Sub Class_Initialize()
    Set Fields = New Collection
    IsRoundRect = False
    LargeurBordure = 1
End Sub

Public Sub SetRoundRect()

Dim aRect As Rect
Dim Res As Long
Dim hdlPen As Long
Dim oldPen As Long
Dim hdlBrush As Long
Dim oldBrush As Long

'-> Effacer le picture box
pBox.Cls
pBox.BackColor = RGB(255, 255, 255)
'-> Cr�er le stylo qui va bien
hdlPen = CreatePen(PS_SOLID, LargeurBordure, 0)
'-> Cr�re le brush qui va bien
hdlBrush = CreateSolidBrush(BackColor)
'-> S�lectionner les objets
oldPen = SelectObject(pBox.hDC, hdlPen)
oldBrush = SelectObject(pBox.hDC, hdlBrush)
Res = GetClientRect(pBox.hwnd, aRect)
Res = RoundRect(pBox.hDC, aRect.Left + (LargeurBordure / 2), aRect.Top + (LargeurBordure / 2), aRect.Right - (LargeurBordure / 2), aRect.Bottom - (LargeurBordure / 2), 50, 50)
'-> Restituer les objets GDI
If oldPen <> 0 Then
    SelectObject pBox.hDC, oldPen
    DeleteObject hdlPen
End If
If oldBrush <> 0 Then
    SelectObject pBox.hDC, oldBrush
    DeleteObject hdlBrush
End If

'-> Indiquer que la bordure est ronde
IsRoundRect = True

'-> Modifier les bordures du cadre
Gauche = True
Haut = True
Bas = True
Droite = True

End Sub
