VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTypeAlign 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   4710
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5625
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4710
   ScaleWidth      =   5625
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3720
      Top             =   3000
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   48
      ImageHeight     =   48
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   27
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":0000
            Key             =   "Align1"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":1CDA
            Key             =   "Align2"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":39B4
            Key             =   "Align3"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":568E
            Key             =   "Align4"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":7368
            Key             =   "Align5"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":9042
            Key             =   "Align6"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":AD1C
            Key             =   "Align7"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":C9F6
            Key             =   "Align8"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":E6D0
            Key             =   "Align9"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":103AA
            Key             =   "Align10"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":12084
            Key             =   "Align11"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":13D5E
            Key             =   "Align16"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":15A38
            Key             =   "Align13"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":17712
            Key             =   "Align14"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":193EC
            Key             =   "Align15"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":1B0C6
            Key             =   "Align12"
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":1CDA0
            Key             =   "AlignPage9"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":1EA7A
            Key             =   "AlignPage2"
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":20754
            Key             =   "AlignPage3"
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":2242E
            Key             =   "AlignPage4"
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":24108
            Key             =   "AlignPage5"
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":25DE2
            Key             =   "AlignPage6"
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":27ABC
            Key             =   "AlignPage7"
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":29796
            Key             =   "AlignPage8"
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":2B470
            Key             =   "AlignPage1"
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":2D14A
            Key             =   "SectionRef"
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmTypeAlign.frx":2DE24
            Key             =   "ObjToAlign"
         EndProperty
      EndProperty
   End
   Begin VB.Shape Shape1 
      Height          =   855
      Left            =   240
      Top             =   120
      Width           =   5055
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      Height          =   615
      Left            =   3480
      TabIndex        =   2
      Top             =   240
      Width           =   1695
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      Height          =   255
      Left            =   1080
      TabIndex        =   1
      Top             =   600
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Left            =   1080
      TabIndex        =   0
      Top             =   240
      Width           =   1815
   End
   Begin VB.Image Image4 
      Height          =   480
      Left            =   2880
      Picture         =   "TM-frmTypeAlign.frx":2EAFE
      Top             =   120
      Width           =   480
   End
   Begin VB.Image Image3 
      Height          =   480
      Left            =   360
      Picture         =   "TM-frmTypeAlign.frx":2F7C8
      Top             =   480
      Width           =   480
   End
   Begin VB.Image Image2 
      Height          =   480
      Left            =   360
      Picture         =   "TM-frmTypeAlign.frx":30492
      Top             =   120
      Width           =   480
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   16
      Left            =   4560
      Top             =   3840
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   15
      Left            =   3480
      Top             =   3840
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   14
      Left            =   2400
      Top             =   3840
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   13
      Left            =   1320
      Top             =   3840
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   12
      Left            =   240
      Top             =   3840
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   11
      Left            =   3480
      Top             =   3000
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   10
      Left            =   2400
      Top             =   3000
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   9
      Left            =   1320
      Top             =   3000
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   8
      Left            =   3480
      Top             =   2160
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   7
      Left            =   2400
      Top             =   2160
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   6
      Left            =   1320
      Top             =   2160
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   5
      Left            =   4560
      Top             =   1320
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   4
      Left            =   3480
      Top             =   1320
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   3
      Left            =   2400
      Top             =   1320
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   2
      Left            =   1320
      Top             =   1320
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Index           =   1
      Left            =   240
      Top             =   1320
      Visible         =   0   'False
      Width           =   720
   End
End
Attribute VB_Name = "frmTypeAlign"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-> Cette variable indique si l'alignement se fait sur la page ou sur un objet
Public TypeAlign As Integer

Private Sub Form_Load()

Dim aLb As Libelle
Dim j As Integer

'---> Gestion des messprog
Set aLb = Libelles("FRMTYPEALIGN")

Me.Label2.Caption = aLb.GetCaption(4)

If TypeAlign = 0 Then 'Alignement Page
    '-> Messprog
    Me.Caption = aLb.GetCaption(2)
    Me.Label1.Caption = aLb.GetCaption(5)
    Me.Label3.Caption = aLb.GetCaption(6)
    Me.Image4.Picture = Me.ImageList1.ListImages("SectionRef").Picture
    For i = 1 To 9
        If i < 4 Then
            j = 1
        Else
            j = 2
        End If
        Me.Image1(i + j).Picture = Me.ImageList1.ListImages("AlignPage" & i).Picture
        Me.Image1(i + j).Tag = "AlignPage" & i
        Me.Image1(i + j).Visible = True
    Next
    
Else 'Alignement Objet
    Me.Caption = aLb.GetCaption(1)
    Me.Label1.Caption = aLb.GetCaption(3)
    Me.Label3.Caption = aLb.GetCaption(5)
    Me.Image4.Picture = Me.ImageList1.ListImages("ObjToAlign").Picture
    For i = 1 To 16
        '---> Ajout des alignements objets
        Me.Image1(i).Picture = Me.ImageList1.ListImages("Align" & i).Picture
        Me.Image1(i).Tag = "Align" & i
        Me.Image1(i).Visible = True
    Next
End If


Set aLb = Nothing

End Sub

Private Sub Image1_DblClick(Index As Integer)

frmAlign.Image1(TypeAlign).Picture = Me.Image1(Index).Picture
frmAlign.Image1(TypeAlign).Tag = Image1(Index).Tag

'-> Indiquer la nature de l'alignement
If TypeAlign = 0 Then
    If Index <= 4 Then
        frmAlign.RetourAlign = Index - 1
    Else
        frmAlign.RetourAlign = Index - 2
    End If
    frmAlign.Option1(0).Tag = frmAlign.RetourAlign
Else
    frmAlign.RetourAlign = Index
    frmAlign.Option1(0).Tag = Index
End If


Unload Me

End Sub
