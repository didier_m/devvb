VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmParamTableau 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   5970
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6480
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5970
   ScaleWidth      =   6480
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame3 
      Height          =   3855
      Left            =   120
      TabIndex        =   19
      Top             =   2040
      Width           =   6255
      Begin VB.CheckBox Check1 
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   3480
         Width           =   3855
      End
      Begin VB.Frame Frame6 
         Height          =   2295
         Left            =   120
         TabIndex        =   29
         Top             =   120
         Width           =   3975
         Begin ComctlLib.Toolbar ToolText2 
            Height          =   420
            Left            =   120
            TabIndex        =   4
            Top             =   240
            Width           =   3720
            _ExtentX        =   6562
            _ExtentY        =   741
            ButtonWidth     =   609
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            ImageList       =   "ImageList2"
            _Version        =   327682
            BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
               NumButtons      =   4
               BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
                  Key             =   ""
                  Object.ToolTipText     =   "Gras"
                  Object.Tag             =   ""
                  Style           =   3
                  MixedState      =   -1  'True
               EndProperty
               BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
                  Key             =   "Gras"
                  Object.Tag             =   ""
                  ImageIndex      =   1
                  Style           =   1
               EndProperty
               BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
                  Key             =   "Italic"
                  Object.ToolTipText     =   "Italic"
                  Object.Tag             =   ""
                  ImageIndex      =   2
                  Style           =   1
               EndProperty
               BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
                  Key             =   "SousLigne"
                  Object.ToolTipText     =   "Sousligne"
                  Object.Tag             =   ""
                  ImageIndex      =   3
                  Style           =   1
               EndProperty
            EndProperty
            BorderStyle     =   1
            Begin VB.ComboBox cbAlignement 
               Height          =   315
               Left            =   1200
               TabIndex        =   5
               Top             =   50
               Width           =   2415
            End
         End
         Begin VB.ListBox ListePolice 
            Height          =   1425
            Left            =   1320
            Sorted          =   -1  'True
            TabIndex        =   7
            Top             =   720
            Width           =   2415
         End
         Begin VB.ListBox ListeTaille 
            Height          =   1425
            Left            =   120
            TabIndex        =   6
            Top             =   720
            Width           =   1095
         End
      End
      Begin VB.Frame Frame4 
         Height          =   1335
         Left            =   4320
         TabIndex        =   24
         Top             =   1560
         Width           =   1815
         Begin VB.CommandButton Command2 
            Height          =   525
            Left            =   960
            TabIndex        =   13
            Top             =   600
            Width           =   735
         End
         Begin VB.PictureBox FontColor 
            Appearance      =   0  'Flat
            AutoRedraw      =   -1  'True
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   525
            Left            =   120
            ScaleHeight     =   35
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   49
            TabIndex        =   25
            TabStop         =   0   'False
            Top             =   600
            Width           =   735
         End
         Begin VB.Label Label6 
            Height          =   255
            Left            =   120
            TabIndex        =   26
            Top             =   240
            Width           =   1575
         End
      End
      Begin VB.Frame Frame1 
         Height          =   1335
         Left            =   4320
         TabIndex        =   21
         Top             =   120
         Width           =   1815
         Begin VB.PictureBox FondColor 
            Appearance      =   0  'Flat
            AutoRedraw      =   -1  'True
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   525
            Left            =   120
            ScaleHeight     =   35
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   49
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   600
            Width           =   735
         End
         Begin VB.CommandButton Command1 
            Height          =   525
            Left            =   960
            TabIndex        =   12
            Top             =   600
            Width           =   735
         End
         Begin VB.Label Label4 
            Height          =   255
            Left            =   120
            TabIndex        =   23
            Top             =   240
            Width           =   1575
         End
      End
      Begin VB.Frame Frame5 
         Height          =   975
         Left            =   120
         TabIndex        =   20
         Top             =   2400
         Width           =   3975
         Begin VB.CheckBox Check2 
            Height          =   195
            Left            =   120
            TabIndex        =   8
            Top             =   360
            Width           =   1455
         End
         Begin VB.CheckBox Check5 
            Height          =   255
            Left            =   1920
            TabIndex        =   11
            Top             =   600
            Width           =   1215
         End
         Begin VB.CheckBox Check4 
            Height          =   255
            Left            =   1920
            TabIndex        =   10
            Top             =   360
            Width           =   1215
         End
         Begin VB.CheckBox Check3 
            Height          =   195
            Left            =   120
            TabIndex        =   9
            Top             =   600
            Width           =   1455
         End
      End
      Begin VB.Image Annuler 
         Height          =   480
         Left            =   4320
         Top             =   3120
         Width           =   630
      End
      Begin VB.Image Aide 
         Height          =   480
         Left            =   4920
         Top             =   3120
         Width           =   630
      End
      Begin VB.Image Ok 
         Height          =   480
         Left            =   5520
         Top             =   3120
         Width           =   630
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1815
      Left            =   120
      TabIndex        =   15
      Top             =   120
      Width           =   6255
      Begin VB.TextBox HauteurLig 
         Height          =   285
         Left            =   5280
         TabIndex        =   3
         Top             =   1320
         Width           =   735
      End
      Begin VB.TextBox LargeurCol 
         Height          =   285
         Left            =   5280
         TabIndex        =   2
         Top             =   960
         Width           =   735
      End
      Begin VB.TextBox NbColonne 
         Height          =   285
         Left            =   5280
         TabIndex        =   1
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox NbLigne 
         Height          =   285
         Left            =   5280
         TabIndex        =   0
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label7 
         BackColor       =   &H008080FF&
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   1320
         Width           =   5055
      End
      Begin VB.Label Label5 
         BackColor       =   &H008080FF&
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   960
         Width           =   5055
      End
      Begin VB.Label Label3 
         Height          =   375
         Left            =   120
         TabIndex        =   18
         Top             =   1080
         Width           =   2175
      End
      Begin VB.Label Label2 
         BackColor       =   &H008080FF&
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   600
         Width           =   5055
      End
      Begin VB.Label Label1 
         BackColor       =   &H008080FF&
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   5055
      End
   End
   Begin ComctlLib.ImageList ImageList2 
      Left            =   1920
      Top             =   2160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   3
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "TM-frmParamTableau.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "TM-frmParamTableau.frx":031A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "TM-frmParamTableau.frx":0634
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmParamTableau"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub Command1_Click()

Dim aPoint As POINTAPI

Res = GetCursorPos(aPoint)
frmPalette.Top = Me.ScaleY(aPoint.Y, 3, 1)
frmPalette.Left = Me.ScaleX(aPoint.X, 3, 1)
frmPalette.Show vbModal
If RetourColor <> -1 Then Me.FondColor.BackColor = RetourColor

End Sub

Private Sub Command2_Click()

Dim aPoint As POINTAPI

Res = GetCursorPos(aPoint)
frmPalette.Top = Me.ScaleY(aPoint.Y, 3, 1)
frmPalette.Left = Me.ScaleX(aPoint.X, 3, 1)
frmPalette.Show vbModal
If RetourColor <> -1 Then Me.FontColor.BackColor = RetourColor


End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim i As Integer


'-> Charger les libelles
Set aLb = Libelles("FRMPARAMTABLEAU")
Me.Caption = aLb.GetCaption(27)
Me.Label1.Caption = aLb.GetCaption(1)
Me.Label2.Caption = aLb.GetCaption(2)
Me.Label5.Caption = aLb.GetCaption(3)
Me.Label7.Caption = aLb.GetCaption(4)
Me.ToolText2.Buttons("Gras").ToolTipText = aLb.GetCaption(5)
Me.ToolText2.Buttons("Italic").ToolTipText = aLb.GetCaption(6)
Me.ToolText2.Buttons("SousLigne").ToolTipText = aLb.GetCaption(7)
Me.cbAlignement.ToolTipText = aLb.GetCaption(8)
For i = 9 To 17
    Me.cbAlignement.AddItem aLb.GetCaption(i)
Next
Me.Label4.Caption = aLb.GetCaption(18)
Me.Label6.Caption = aLb.GetCaption(19)
Me.Check2.Caption = aLb.GetCaption(20)
Me.Check3.Caption = aLb.GetCaption(21)
Me.Check4.Caption = aLb.GetCaption(22)
Me.Check5.Caption = aLb.GetCaption(23)
Me.Check1.Caption = aLb.GetCaption(28)
Me.Ok.ToolTipText = aLb.GetCaption(24)
Me.Annuler.ToolTipText = aLb.GetCaption(25)
Me.Aide.ToolTipText = aLb.GetCaption(31)
Me.Command1.Caption = aLb.GetCaption(26)
Me.Command2.Caption = aLb.GetCaption(26)
Me.ListePolice.ToolTipText = aLb.GetCaption(29)
Me.ListeTaille.ToolTipText = aLb.GetCaption(30)

Me.Annuler.Picture = Turbo_Maq.Annuler.Picture
Me.Aide.Picture = Turbo_Maq.Aide.Picture
Me.Ok.Picture = Turbo_Maq.Ok.Picture

'---> Charger les polices
For i = 1 To Screen.FontCount - 1
    Me.ListePolice.AddItem Screen.Fonts(i)
Next

'-> Ajout des tailles de police
For i = 8 To 76 Step 2
    Me.ListeTaille.AddItem i
Next

'-> Charger les valeurs
Me.NbLigne.Text = DefNbLig
Me.NbColonne.Text = DefNbCol
Me.LargeurCol.Text = DefLargeurCol
Me.HauteurLig.Text = DefHauteurLig
If DefBold Then Me.ToolText2.Buttons("Gras").Value = tbrPressed Else Me.ToolText2.Buttons("Gras").Value = tbrUnpressed
If DefItalic Then Me.ToolText2.Buttons("Italic").Value = tbrPressed Else Me.ToolText2.Buttons("Italic").Value = tbrUnpressed
If DefUnderline Then Me.ToolText2.Buttons("SousLigne").Value = tbrPressed Else Me.ToolText2.Buttons("SousLigne").Value = tbrUnpressed
Me.cbAlignement.ListIndex = DefALignCell - 1
Me.ListePolice.Text = DefFontName
Me.ListeTaille.Text = DefFontSize
If DefGauche Then Me.Check2.Value = 1 Else Me.Check2.Value = 0
If DefDroite Then Me.Check3.Value = 1 Else Me.Check3.Value = 0
If DefHaut Then Me.Check4.Value = 1 Else Me.Check4.Value = 0
If DefBas Then Me.Check5.Value = 1 Else Me.Check5.Value = 0
If DefRetourLig Then Me.Check1.Value = 1 Else Me.Check1.Value = 0
Me.FondColor.BackColor = DefBackcolor
Me.FontColor.BackColor = DefFontColor

Set aLb = Nothing

End Sub

Private Sub HauteurLig_GotFocus()
    SelectText Me.HauteurLig
End Sub

Private Sub LargeurCol_GotFocus()
    SelectText LargeurCol
End Sub

Private Sub NbColonne_GotFocus()
    SelectText Me.NbColonne
End Sub

Private Sub NbLigne_GotFocus()
    SelectText Me.NbLigne
End Sub

Private Sub Ok_Click()

Dim aLb As Libelle
Dim Fillin As TextBox

'-> Effectuer les vérifications
Set aLb = Libelles("MESSAGE")


'-> Test si numéric
If Not IsNumeric(Me.NbLigne.Text) Then
    Set Fillin = Me.NbLigne
    GoTo ErrorProp
End If
    
If Not IsNumeric(Me.NbColonne.Text) Then
    Set Fillin = Me.NbColonne
    GoTo ErrorProp
End If

'-> Test validiter du nombre de ligne et de colonne
If CInt(Me.NbLigne.Text) < 1 Or CInt(Me.NbColonne.Text) < 1 Then
    MsgBox aLb.GetCaption(35), vbOKOnly + vbCritical, aLb.GetToolTip(35)
    Exit Sub
End If

'-> Test hauteur de ligne
If Not IsNumeric(Me.HauteurLig.Text) Then
    Set Fillin = Me.HauteurLig
    GoTo ErrorProp
Else
    If CSng(Me.HauteurLig.Text < 0.3) Then
        Set Fillin = Me.HauteurLig
        GoTo ErrorProp
    End If
End If

'-> Test Largeur colonhne
If Not IsNumeric(Me.LargeurCol.Text) Then
    Set Fillin = Me.LargeurCol
    GoTo ErrorProp
Else
    If CSng(Me.LargeurCol.Text < 0.3) Then
        Set Fillin = Me.LargeurCol
        GoTo ErrorProp
    End If
End If

'-> Charger les nouvelles valeurs
DefNbLig = CInt(Me.NbLigne.Text)
DefNbCol = CInt(Me.NbColonne.Text)
DefLargeurCol = CSng(Me.LargeurCol.Text)
DefHauteurLig = CSng(Me.HauteurLig.Text)
If Me.ToolText2.Buttons("Gras").Value = tbrPressed Then DefBold = True Else DefBold = False
If Me.ToolText2.Buttons("Italic").Value = tbrPressed Then DefItalic = True Else DefItalic = False
If Me.ToolText2.Buttons("SousLigne").Value = tbrPressed Then DefUnderline = True Else DefUnderline = False
DefBackcolor = Me.FondColor.BackColor
DefFontColor = Me.FontColor.BackColor
DefFontName = Me.ListePolice.Text
DefFontSize = CInt(Me.ListeTaille.Text)
If Me.Check2.Value = 1 Then DefGauche = True Else DefGauche = False
If Me.Check3.Value = 1 Then DefDroite = True Else DefDroite = False
If Me.Check4.Value = 1 Then DefHaut = True Else DefHaut = False
If Me.Check5.Value = 1 Then DefBas = True Else DefBas = False
If Me.Check1.Value = 1 Then DefRetourLig = True Else DefRetourLig = False


Set aLb = Nothing
Set Fillin = Nothing

Unload Me

Exit Sub

ErrorProp:

    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
    Fillin.SetFocus
    Set aLb = Nothing
    Set Fillin = Nothing
    
End Sub
Private Sub SelectText(ByVal aTextBox As TextBox)

aTextBox.SelStart = 0
aTextBox.SelLength = 65535

End Sub
