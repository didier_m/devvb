VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Tableau"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'**********************************************
'*                                            *
'* Cette classe contient la d�finition d'un   *
'* Objet Tableau                              *
'*                                            *
'**********************************************


'---> Propri�t�s de l'objet
Public Nom As String
Public Orientation As String
Public Largeur As Single
Public IdAffichage As Integer
Private pOrdreAffichage() As String

'-> Cette variable indique l'index du prochain block � cr�er
Public nBlocks As Integer
Public nEntries As Integer

'---> Objets internes
Public Blocks As Collection


'---> Feuilles de repr�sentation
Public frmBlocks As Collection

Private Sub Class_Initialize()
    
    Set Blocks = New Collection
    Set frmBlocks = New Collection
    nBlocks = 1
    nEntries = nBlock
    
End Sub

Public Function GetBlockExist(ByVal NomBlock As String) As Boolean

'---> Fonction qui d�termine si un nom de block est d�ja utilis�

Dim i As Integer
Dim aBlock As Block

For Each aBlock In Blocks
    If UCase$(aBlock.Nom) = UCase$(NomBlock) Then
        GetBlockExist = True
        Exit Function
    End If
Next

GetBlockExist = False
    
End Function
Public Sub AddTreeView()

'-> Ajout dans le treeview de l'icone de la maquette

Dim aNode As Node

Set aNode = frmNaviga.TreeNaviga.Nodes.Add("MAQUETTE", 4, UCase$(Nom), Nom, "Tableau")
aNode.Tag = "TABLEAU"
aNode.Selected = True
Set aNode = Nothing

End Sub
Public Sub DelTreeView()

'-> Supprime le node du tableau
frmNaviga.TreeNaviga.Nodes.Remove UCase$(Nom)

End Sub
Public Function GetOrdreAffichage(ByVal nIndex As Integer) As String

    GetOrdreAffichage = pOrdreAffichage(nIndex)

End Function

Public Function AddOrdreAffichage(ByVal Value As String) As Integer

ReDim Preserve pOrdreAffichage(1 To nBlocks)
pOrdreAffichage(nBlocks) = Value
AddOrdreAffichage = nBlocks
nBlocks = nBlocks + 1
nEntries = nBlocks

End Function

Public Sub ClearOrdreAffichage()

    Erase pOrdreAffichage()
    nBlocks = 1
    nEntries = 1
    
End Sub

Public Sub DelOrdreAffichage(ByVal nIndex As Integer)

'---> Cette proc�dure supprime une entr�e dans la matrice des ordres d'affichage

Dim DupOrdre() As String
Dim i As Integer
Dim j As Integer
Dim aBlock As Block

'-> Dans un premier temps, faire une copie du tableau des ordres d'affichage
ReDim DupOrdre(1 To nBlocks)
For i = 1 To nBlocks - 1
    DupOrdre(i) = pOrdreAffichage(i)
Next

'-> Ecraser la matrice
Erase pOrdreAffichage()

'-> Ne rien faire s'il n'y a plus d'entr�es
If nBlocks - 1 = 1 Then
    nBlocks = 1
    nEntries = 1
    Exit Sub
End If

'-> La reconstituer en supprimant l'entr�e
ReDim pOrdreAffichage(1 To nBlocks - 2)
j = 1
For i = 1 To nBlocks - 1
    If i = nIndex Then
    Else
        '-> Ajouter l'entr�e dans la matrice
        pOrdreAffichage(j) = DupOrdre(i)
        '-> Modifier l'ordre de l'objet concern�
        Set aBlock = Blocks(UCase$(pOrdreAffichage(j)))
        aBlock.IdOrdreAffichage = j
        '-> Incr�menter le compteur d'ordre
        j = j + 1
    End If
Next

'-> Indiquer qu'il y a une entr�e en mois
nBlocks = nBlocks - 1
nEntries = nBlocks

End Sub

Private Sub Class_Terminate()

Set Blocks = Nothing
Set frmBlocks = Nothing

End Sub


Public Sub PrintView(IntFile As Integer)

Dim aBlock As Block

'-> Pour tous les blocks du tableau
For Each aBlock In Blocks
    '-> Pour toutes les lignes du tableau
    For i = 1 To aBlock.NbLigne
        Print #IntFile, "[TB-" & UCase$(Nom) & "(BLK-" & aBlock.Nom & ")][\" & i & "]{}"
    Next '-> Pour toutes les lignes du tableau
Next 'Pour tous les blocks

End Sub

