VERSION 5.00
Begin VB.Form frmBordure 
   BackColor       =   &H80000004&
   BorderStyle     =   5  'Sizable ToolWindow
   ClientHeight    =   1485
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   3390
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1485
   ScaleWidth      =   3390
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   330
      Index           =   16
      Left            =   1030
      OLEDropMode     =   2  'Automatic
      Picture         =   "TM-frmBordure.frx":0000
      ScaleHeight     =   330
      ScaleWidth      =   360
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   1100
      Width           =   360
   End
   Begin VB.CommandButton Command1 
      Cancel          =   -1  'True
      Caption         =   "Command1"
      Default         =   -1  'True
      Height          =   435
      Left            =   4320
      TabIndex        =   16
      Top             =   600
      Width           =   735
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   15
      Left            =   480
      Picture         =   "TM-frmBordure.frx":0672
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   1005
      Width           =   480
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   14
      Left            =   0
      Picture         =   "TM-frmBordure.frx":097C
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   1005
      Width           =   480
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   13
      Left            =   2880
      Picture         =   "TM-frmBordure.frx":0C86
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   525
      Width           =   480
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   12
      Left            =   2400
      Picture         =   "TM-frmBordure.frx":0F90
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   525
      Width           =   480
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   11
      Left            =   1920
      Picture         =   "TM-frmBordure.frx":129A
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   525
      Width           =   480
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   10
      Left            =   1440
      Picture         =   "TM-frmBordure.frx":15A4
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   525
      Width           =   480
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   9
      Left            =   960
      Picture         =   "TM-frmBordure.frx":18AE
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   525
      Width           =   480
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   8
      Left            =   480
      Picture         =   "TM-frmBordure.frx":1BB8
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   525
      Width           =   480
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   7
      Left            =   0
      Picture         =   "TM-frmBordure.frx":1EC2
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   525
      Width           =   480
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   6
      Left            =   2880
      Picture         =   "TM-frmBordure.frx":21CC
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   45
      Width           =   480
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   5
      Left            =   2400
      Picture         =   "TM-frmBordure.frx":24D6
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   45
      Width           =   480
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   4
      Left            =   1920
      Picture         =   "TM-frmBordure.frx":27E0
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   45
      Width           =   480
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   3
      Left            =   1440
      Picture         =   "TM-frmBordure.frx":2AEA
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   45
      Width           =   480
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   2
      Left            =   960
      Picture         =   "TM-frmBordure.frx":2DF4
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   45
      Width           =   480
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   1
      Left            =   480
      Picture         =   "TM-frmBordure.frx":30FE
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   45
      Width           =   480
   End
   Begin VB.PictureBox Bordures 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Index           =   0
      Left            =   0
      Picture         =   "TM-frmBordure.frx":3408
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   45
      Width           =   480
   End
End
Attribute VB_Name = "frmBordure"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Bordures_Click(Index As Integer)
    
    RetourBordure = Index
    If Index = 16 Then
        If TypeOf ObjCours Is Cadre Then
            ObjCours.IsRoundRect = False
            RetourBordure = 15
        Else
            RetourBordure = 99
        End If
    End If
    Unload Me
    
End Sub

Private Sub Command1_Click()
    RetourBordure = -1
    Unload Me
End Sub

Private Sub Form_Load()

Dim aLb As Libelle

Set aLb = Libelles("FRMBORDURE")
Me.Caption = aLb.GetCaption(1)


End Sub
