VERSION 5.00
Begin VB.Form frmFormatCell 
   Caption         =   "Form1"
   ClientHeight    =   7875
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4545
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7875
   ScaleWidth      =   4545
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame4 
      Caption         =   "Frame3"
      Height          =   855
      Left            =   0
      TabIndex        =   18
      Top             =   6360
      Width           =   4455
      Begin VB.CheckBox Check4 
         Caption         =   "Check3"
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   360
         Width           =   3975
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Frame3"
      Height          =   855
      Left            =   60
      TabIndex        =   14
      Top             =   5400
      Width           =   4455
      Begin VB.VScrollBar VScroll2 
         Height          =   285
         Left            =   3960
         Max             =   250
         Min             =   1
         TabIndex        =   17
         Top             =   360
         Value           =   1
         Width           =   255
      End
      Begin VB.TextBox Text3 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3480
         TabIndex        =   16
         Top             =   360
         Width           =   495
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Check3"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   360
         Width           =   3135
      End
   End
   Begin VB.Frame Frame2 
      Height          =   3855
      Left            =   60
      TabIndex        =   4
      Top             =   1440
      Width           =   4455
      Begin VB.CheckBox Check2 
         Caption         =   "Check2"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   3360
         Width           =   3975
      End
      Begin VB.VScrollBar VScroll1 
         Height          =   280
         Left            =   3960
         Max             =   10
         TabIndex        =   12
         Top             =   360
         Width           =   255
      End
      Begin VB.TextBox Text2 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3360
         TabIndex        =   11
         Text            =   "0"
         Top             =   360
         Width           =   615
      End
      Begin VB.ListBox List2 
         Height          =   1035
         Left            =   120
         TabIndex        =   8
         Top             =   1680
         Width           =   4095
      End
      Begin VB.CheckBox Check1 
         Alignment       =   1  'Right Justify
         Caption         =   "Check1"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   840
         Width           =   4095
      End
      Begin VB.Label Apercu 
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   2880
         Width           =   4095
      End
      Begin VB.Label Label2 
         Caption         =   "Label2"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1320
         Width           =   2655
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   1935
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1335
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   4455
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   3600
         TabIndex        =   9
         Top             =   600
         Width           =   735
      End
      Begin VB.OptionButton Option3 
         Caption         =   "Option2"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   960
         Width           =   3375
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Option2"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Width           =   3375
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Option1"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   3135
      End
   End
   Begin VB.Image Annuler 
      Height          =   480
      Left            =   2400
      Top             =   7320
      Width           =   630
   End
   Begin VB.Image Aide 
      Height          =   480
      Left            =   3120
      Top             =   7320
      Width           =   630
   End
   Begin VB.Image Ok 
      Height          =   480
      Left            =   3840
      Top             =   7320
      Width           =   630
   End
End
Attribute VB_Name = "frmFormatCell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public aCell As Cellule
Public Action As Integer
Public ColMin As Integer, ColMax As Integer, LigMin As Integer, LigMax As Integer
Public BlockCours As Block

Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub Check3_Click()
If Me.Check3.Value = 1 Then
    Me.VScroll2.Enabled = True
Else
    Me.VScroll2.Enabled = False
End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then Annuler_Click
End Sub

Private Sub Form_Load()

Dim aLb As Libelle, i As Integer
Dim lpbuffer As String
Dim Res As String



'-> Libelles des objets
Set aLb = Libelles("FRMFORMATCELL")
Me.Caption = aLb.GetCaption(1) & "  L" & aCell.Ligne & "C" & aCell.Colonne
Me.Option1.Caption = aLb.GetCaption(2)
Me.Option2.Caption = aLb.GetCaption(3)
Me.Option3.Caption = aLb.GetCaption(4)
Me.Frame2.Caption = aLb.GetCaption(5)
Me.Label1.Caption = aLb.GetCaption(6)
Me.Check1.Caption = aLb.GetCaption(7)
Me.Label2.Caption = aLb.GetCaption(8)
For i = 9 To 13
    Me.List2.AddItem aLb.GetCaption(i)
Next
Me.List2.Selected(1) = True

Me.Check1.Value = 0
Me.Check2.Caption = aLb.GetCaption(18)

'-> Acc�s au d�tail
Me.Frame4.Caption = aLb.GetCaption(20)
Me.Check4.Caption = aLb.GetCaption(19)


'-> Affichage des options
If aCell.TypeValeur = 0 Then
    Me.Option1.Value = True
Else
    If aCell.TypeValeur = 2 Then
        Me.Option3.Value = True
        '-> charger les options
        Me.Text2.Text = CInt(Entry(1, aCell.Msk, "�"))
'        Me.strSepMil.Text = SepMil 'Entry(2, aCell.Msk, "�")
        Me.List2.Selected(CInt(Entry(3, aCell.Msk, "�"))) = True
        'If Me.strSepMil.Text <> "" Then Me.Check1.Value = 1
        '-> Gestion de l'utilisation du s�parateur de milliers
        If Entry(2, aCell.Msk, "�") <> "" Then Me.Check1.Value = 1
    Else
        Me.Option2.Value = True
        Me.Text1.Text = aCell.Msk
    End If
End If

If aCell.PrintZero Then
    Me.Check2.Value = 1
Else
    Me.Check2.Value = 0
End If

Set aLb = Libelles("BOUTONS")
Me.Annuler.ToolTipText = aLb.GetCaption(1)
Me.Aide.ToolTipText = aLb.GetCaption(2)
Me.Ok.ToolTipText = aLb.GetCaption(3)

Me.Annuler.Picture = Turbo_Maq.Annuler.Picture
Me.Aide.Picture = Turbo_Maq.Aide.Picture
Me.Ok.Picture = Turbo_Maq.Ok.Picture

Set aLb = Nothing

'---> Formatage des exports vers Excel
Me.Frame3.Caption = "Export vers Excel"
Me.Check3.Caption = "Fusionner avec les cellules suivantes"

'-> Init des valeurs selon la cellule que l'on modifie
Me.Check3.Value = Abs(CInt(aCell.IsFusion))
If aCell.ColFusion <> -1 Then
    Me.VScroll2.Value = aCell.ColFusion
Else
    Me.Text3.Text = 1
End If

Check3_Click

'-> Acc�s au d�tail
Me.Check4.Value = Abs(CInt(aCell.UseAccesDet))

End Sub


Private Sub EnableNum(ByVal En As Boolean)

Me.Frame2.Enabled = En
Me.Label1.Enabled = En
Me.Text1.Enabled = En
Me.Check1.Enabled = En
Me.Label2.Enabled = En
Me.List2.Enabled = En
Me.Apercu.Enabled = En
Me.Apercu.Enabled = En
Me.Check2.Enabled = En
'If Me.Check1.Value = 0 Then Me.strSepMil.Enabled = False

End Sub

Private Sub List2_Click()

Dim Rep
Dim strFormat As String
Dim Msq As String

'-> Construction du masque
If Me.Check1.Value = 1 Then
    Msk = Me.Text2.Text & "�" & SepMil & "�" & Me.List2.ListIndex
Else
    Msk = Me.Text2.Text & "��" & Me.List2.ListIndex
End If

Rep = "-12845600" & SepDec & "45"
strFormat = FormatNum(Rep, Msk)

If CDbl(Rep) < 0 Then
    '-> Effectuer le formattage des chiffres n�gatifs
    Select Case Me.List2.ListIndex
        Case 0 'Signe � gauche
            Me.Apercu.ForeColor = QBColor(0)
            Me.Apercu.Caption = "- " & strFormat
        Case 1
            Me.Apercu.ForeColor = QBColor(0)
            Me.Apercu.Caption = strFormat & " -"
        Case 2
            Me.Apercu.ForeColor = QBColor(12)
            Me.Apercu.Caption = strFormat
        Case 3
            Me.Apercu.ForeColor = QBColor(12)
            Me.Apercu.Caption = "- " & strFormat
        Case 4
            Me.Apercu.ForeColor = QBColor(12)
            Me.Apercu.Caption = strFormat & " -"
    End Select
Else
    Me.Apercu.ForeColor = QBColor(0)
    Me.Apercu.Caption = strFormat
End If
        
Set aLb = Nothing

End Sub

Private Sub Ok_Click()

Dim i As Integer, j As Integer

If Action = 2 Then
    '-> Travaille avec une plage
    For i = ColMin To ColMax
        For j = LigMin To LigMax
            Set aCell = BlockCours.Cellules("L" & j & "C" & i)
            SaveMsq aCell
        Next 'Pour toutes les lignes
    Next 'Pour toutes les colonnes
Else
    SaveMsq aCell
End If

Unload Me

End Sub

Private Sub SaveMsq(ByRef aCell As Cellule)

'-> Enregistrement
If Me.Option1.Value Then
    aCell.TypeValeur = 0
    aCell.Msk = ""
End If

If Me.Option2.Value Then
    aCell.TypeValeur = 1
    aCell.Msk = Me.Text1.Text
End If

If Me.Option3.Value Then
    aCell.TypeValeur = 2
    If Me.Check1.Value = 1 Then
        aCell.Msk = Me.Text2.Text & "�1�" & Me.List2.ListIndex
    Else
        aCell.Msk = Me.Text2.Text & "��" & Me.List2.ListIndex
    End If
    '-> On modifie l'alignement interne de la cellule
    aCell.CellAlign = 6
    If Me.Check2.Value = 1 Then
        aCell.PrintZero = True
    Else
        aCell.PrintZero = False
    End If
End If

'-> Enregistrer les param des exports vers Excel
If Me.Check3.Value = 1 Then
    '-> En cas d'export , on fusionne :
    aCell.IsFusion = True
    aCell.ColFusion = Me.VScroll2.Value
Else
    '-> Ne pas fusionner en cas d'export vers Excel
    aCell.IsFusion = False
    aCell.ColFusion = 1
End If

'-> Acc�s au d�tail
If Me.Check4.Value = 1 Then
    aCell.UseAccesDet = True
Else
    aCell.UseAccesDet = False
End If

End Sub

Private Sub Option1_Click()

EnableNum False
Me.Text1.Enabled = False

End Sub

Private Sub Option2_Click()

EnableNum False
Me.Text1.Enabled = True


End Sub

Private Sub Option3_Click()

EnableNum True
Me.Text1.Enabled = False

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Ok_Click
End Sub

Private Sub VScroll1_Change()
    Me.Text2.Text = Me.VScroll1.Value
End Sub

Private Sub VScroll2_Change()
Me.Text3.Text = Me.VScroll2.Value
End Sub
