/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 620/nathalie/inc/n-tabsoc-spe.i                                                             !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 10389 03-09-04!Evo!bp             !spe Sade, societe de regroupement article a chercher dans une table   !
!V52 10384 03-09-04!Evo!bp             !mise en place d'un include pour gerer du specif dans n-tabsoc.i       !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
/* specif pour gerer du spe dans n-tabsoc.i - voir client 620 */

FIND tabges WHERE tabges.codsoc = ""
            AND   tabges.etabli = ""
            AND   tabges.typtab = "PRM"
            AND   tabges.prefix = "PARETB"
            AND   tabges.codtab = glo-codsoc + glo-etabli
            NO-LOCK NO-ERROR.
IF AVAILABLE tabges THEN 
do:
  IF tabges.libel1[1] <> "" THEN articl-soc = tabges.libel1[1] .
END.