/*---------------------------------------------------------*/
/*           Include cadrage code ARTICLE                  */
/*                      n-cadarti.i                        */
/*---------------------------------------------------------*/

/*   Standard : Numerique - Cadre Droite - Pre Zeros - 6 Caracteres   */

assign  {1}  =  trim( {1} )
        {1}  =  fill( "0" , 8 - length( {1} ) ) + {1} .
