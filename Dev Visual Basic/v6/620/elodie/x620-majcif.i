/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 620/elopro/inc/x620-majcif.i                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 11319 16-11-04!New!air            !OP 13039 : Moulinette de MAJ du code CIF                              !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End
**************************************************************************************************************/
DEFINE {1} SHARED VARIABLE codsoc-mini AS CHARACTER  NO-UNDO.
DEFINE {1} SHARED VARIABLE codsoc-maxi AS CHARACTER  NO-UNDO init "ZZZZ".
DEFINE {1} SHARED VARIABLE codetb-mini AS CHARACTER  NO-UNDO.
DEFINE {1} SHARED VARIABLE codetb-maxi AS CHARACTER  NO-UNDO init "ZZZZ".
DEFINE {1} SHARED VARIABLE codaff-mini AS CHARACTER  NO-UNDO.
DEFINE {1} SHARED VARIABLE codaff-maxi AS CHARACTER  NO-UNDO init "ZZZZZZZZZZ".
DEFINE {1} SHARED VARIABLE ii          AS INTEGER    NO-UNDO.
DEFINE {1} SHARED VARIABLE lib-codaxe  AS CHARACTER  NO-UNDO.
DEFINE {1} SHARED VARIABLE t-journal   AS CHARACTER  NO-UNDO.
DEFINE {1} SHARED VARIABLE t-typie     AS CHARACTER  NO-UNDO.
def buffer maj-afx for eloafx.


