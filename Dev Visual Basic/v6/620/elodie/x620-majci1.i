/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 620/elopro/inc/x620-majci1.i                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 11319 16-11-04!New!air            !OP 13039 : Moulinette de MAJ du code CIF                              !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!                      !                                     !eloafx                                          !
!                      !                                     !analaxe                                         !
!                      !                                     !tables                                          !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
!_____________________________________________________________________________________________________________!
!                                                  P R O C E D U R E S                                        !
!_____________________________________________________________________________________________________________!
!Trt-CIF               !                                                                                      !
&End
**************************************************************************************************************/
procedure Trt-CIF :
DEFINE INPUT  PARAMETER codsoc-trt AS CHARACTER  NO-UNDO.
DEFINE INPUT  PARAMETER codetb-trt AS CHARACTER  NO-UNDO.
DEFINE INPUT  PARAMETER codaff-trt AS CHARACTER  NO-UNDO.
DEFINE INPUT  PARAMETER i-jnal     AS CHARACTER  NO-UNDO.
DEFINE INPUT  PARAMETER i-typie    AS CHARACTER  NO-UNDO.

    for each eloafx where eloafx.codsoc = codsoc-trt
                      and eloafx.codetb = codetb-trt
                      and eloafx.codaff = codaff-trt
                      no-lock :


         /* acces au rattachement de cette affaire � l'axe */                
         FIND FIRST analaxe where analaxe.axe       = "Z01"
                              and analaxe.typlien   = "04"
                              and analaxe.codsoc    = eloafx.codsoc
                              and analaxe.codetb    = eloafx.codetb
                              and analaxe.code-don  = eloafx.codaff
                              and analaxe.typaux    = ""
                              no-lock no-error .
         if not available analaxe then next.

         do ii = 1 to num-entries ( analaxe.hierarchie ) :


            /* recherche du libell� du code axe rattach� */
            Find Tables where tables.codsoc = ""
                          and tables.etabli = ""
                          and tables.typtab = "ANA"
                          and tables.prefix = "AXE-Z01" + string ( ii , "99" )
                          and tables.codtab = entry ( ii , analaxe.hierarchie )
            no-lock no-wait no-error.
            if available tables then lib-codaxe = tables.libel1[1].



            /* creation de la table des code CIF */
            find tables where tables.codsoc = ""
                          and tables.etabli = ""
                          and tables.typtab = "ABA"
                          and tables.prefix = "CIF"
                          and tables.codtab = entry ( ii , analaxe.hierarchie )
                          no-lock no-error.
            if available tables then next.
            create tables.
            assign tables.codsoc    = ""                               
                   tables.etabli    = ""                               
                   tables.typtab    = "ABA"                            
                   tables.prefix    = "CIF"                            
                   tables.codtab    = entry ( ii , analaxe.hierarchie )
                   tables.libel1[1] = i-jnal        /* journal */
                   tables.libel1[2] = i-typie       /* typie */
                   tables.libel1[3] = lib-codaxe
                   tables.nombre[1] = 0             /* gestion SEP */
                   tables.nombre[2] = 0             /* traitement standard */
                   .

         end. /* do ii = 1 to num-entries */

         /* on verifie quand meme si le code CIF deja rattache est coherent */
         if analaxe.hierarchie <> eloafx.libel[29] 
         then do :
             find maj-afx where rowid ( maj-afx ) = rowid ( eloafx ) exclusive-lock no-error.
             if available maj-afx then maj-afx.libel[29] = string ( analaxe.hierarchie , "x(30)" ).
         end.

    end. /* each eloafx */

end procedure.

