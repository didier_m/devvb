/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 620/elopro/src/x620-b0b.p                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!V52 10871 05-10-04!New!lau            !Edition des flux intra-groupe                                         !
!V52 10367 02-09-04!New!lbo            !TSPE38 - Flux intragroupe chez Sade.                                  !
!_____________________________________________________________________________________________________________!
!                             I N C L U D E S                !                    T A B L E S                 !
!____________________________________________________________+________________________________________________!
!connect.i             !                                     !messprog                                        !
!maqtdef.i             !new                                  !tables                                          !
!select.i              !new                                  !soldes                                          !
!bat-ini.i             !new                                  !auxili                                          !
!x620-b00.i            !new                                  !G-TABLES                                        !
!acc-mess.i            !"x620-b00"                           !mvtcpt                                          !
!v6frame.i             !                                     !                                                !
!regs-cha.i            !                                     !                                                !
!maq-out.i             !"maquette" "ma-maquet" "ma-impri" "0"!                                                !
!select-z.i            !"societe" "soc-tables.codtab"        !                                                !
!regs-rec.i            !                                     !                                                !
!idx-relev.i           !                                     !                                                !
!selectpt.i            !                                     !                                                !
!maqtmajm.i            !"01" "edit" "maqtchar" "maqtecrit" "m!                                                !
!maqtedir.i            !rang                                 !                                                !
!bat-cha.i             !"lib-prog" "lib-prog" "a"            !                                                !
!bat-cham.i            !"zone-charg" "zone-charg" "a" "m" "8"!                                                !
!maqtedir.i            !"90" "maqtchar" "x620-log" "ma-maquet!                                                !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
!_____________________________________________________________________________________________________________!
!                                                  P R O C E D U R E S                                        !
!_____________________________________________________________________________________________________________!
!Edition-fig           !                                                                                      !
!edit-entete           !                                                                                      !
!edit-rupture          !                                                                                      !
!edit-ligne            !                                                                                      !
!edimaq-detail         !                                                                                      !
!edimaq                !                                                                                      !
!charge-batch          !                                                                                      !
!log-edit-entete       !                                                                                      !
!log-edit-ligne        !                                                                                      !
&End
**************************************************************************************************************/
/*- x620-b0b.p : Flux intragroupe [ TRAITEMENT ] -*/

{ connect.i }
{ maqtdef.i  new } 
{ select.i   new }
{ bat-ini.i  new }
{ x620-b00.i new }

{ acc-mess.i "x620-b00" }
If not available Messprog Then Return.

Def input parameter fichier as char.
Def var lig-info as char.

Form lig-info format "x(70)" 
     With frame fr-info row 19 centered overlay {v6frame.i} no-labels color message.

Form lig-info format "x(50)"
     With frame fr-info2 row 8 centered 8 down overlay {v6frame.i} no-labels color message
     With title Messprog.mes[7].

Def var a          as int.
Def var b          as int.


                                        /*----------------------------------------*/
Def buffer soc-tables  for tables.      /* Buffer pour lecture des societes       */
Def buffer etb-tables  for tables.      /* Buffer pour lecture des etablissements */
Def buffer b-tables    for tables.      /* Buffer pour lecture tables divers      */
Def buffer x620-Tables for Tables.      /* La carte                               */
                                        /*----------------------------------------*/
Def stream maqdet.
Def stream x620-log.

Def var edit                  as char extent 100.
Def var no-page               as int no-undo.
Def var no-page-det           as int no-undo.


Def buffer lmvtcpt            for mvtcpt.
Def buffer mvtcpt-ttc         for mvtcpt.
Def buffer mvtcpt-ht          for mvtcpt.

Def var x620-ordre            as char.
Def var list-typie-tva        as char no-undo. /* Liste des types de piece tva     */

Def var x620-siret            like Auxili.siret   no-undo.
Def var x620-tva-encaissement as log             no-undo.
Def var x620-nb-trt           as int no-undo.
Def var x620-nb-ok            as int no-undo.
Def var vecteur-rowidht       as char no-undo.
Def var ma-ficlog             as char no-undo.
Def var ma-detail             as char no-undo.
Def var sov-codsoc            like mvtcpt.codsoc no-undo.
Def var sov-codetb            like mvtcpt.codetb no-undo.
Def var w-an-cpt              as dec no-undo.
Def var w-an-pie              as dec no-undo.
Def var Ma-nblig-recap        as int no-undo.
Def var Ma-nblig-det          as int no-undo.
Def var cum-ttc-cpt           as dec extent 5 no-undo.
Def var cum-ttc-pie           as dec extent 5 no-undo.
Def var cum-ttc-ini           as dec extent 5 no-undo.
Def var cum-ht-cpt            as dec extent 5 no-undo.
Def var cum-ht-pie            as dec extent 5 no-undo.
Def var cum-ht-ini            as dec extent 5 no-undo.
Def var cum-an-cpt            as dec extent 5 no-undo.
Def var cum-an-pie            as dec extent 5 no-undo.



Def temp-table t-fig
  field codsoc       like mvtcpt.codsoc
  field codetb       like mvtcpt.codetb
  field docume       like mvtcpt.docume
  field chrono       like mvtcpt.chrono
  field c-group      as char
  field siret        like auxili.siret
  field typaux       like auxili.typaux
  field codaux       like auxili.codaux
  field libabr       like auxili.libabr
  field adresse      like auxili.adres
  field devcpt      like mvtcpt.devcpt
  field devpie      like mvtcpt.devpie
  field devini      like mvtcpt.devini
  field r-ttc       as rowid   /* rowid du ttc */
  field v-ht        as char    /* Liste des rowid ht */
  field ttc-cpt     as dec
  field ttc-pie     as dec
  field ttc-ini     as dec
  field ht-cpt      as dec
  field ht-pie      as dec
  field ht-ini      as dec
  field an-cpt      as dec
  field an-pie      as dec

  INDEX PRIMAIRE    codsoc codetb docume  chrono
  INDEX SECONDAIRE  codsoc c-group siret   
  .

run bat-lec.p (fichier).
run charge-batch. /* bat-cha */


/**********************************************************/
/* Recuperation des selections du .chx a partir de seledi */
/**********************************************************/
run select-c.p.

run maqtchar.p.
if ma-anom <> "" then return.

/* acces au societe de regroupement */
regs-app = "elodie".
{regs-cha.i}



                        /*-------------------------*/
                        /*- L E S     S P O O L S -*/
/*-------------------------------------------------------------------------------*/
/*- Spool des Flux intra groupe recap -*/


/* output stream maquette to value (ma-impri).  */

{maq-out.i "maquette" "ma-maquet" "ma-impri" "0"}.
run bat-gar.p (fichier).

/*- Spool des Flux intra groupe detail -*/
ma-detail = "fi2" + substr(ma-impri , 3). 
maqgui = yes.
/*output stream maqdet to value (ma-detail). */
{maq-out.i "maqdet" "ma-maquet" "ma-detail" "0"}. 

/*- Spool du log du traitement -*/
ma-ficlog = "fi9" + substr(ma-impri , 3).
maqgui = yes. 
/* output stream x620-log to value (ma-ficlog). */
{maq-out.i "x620-log" "ma-maquet" "ma-ficlog" "0"}. 
x620-trt-log = zone-charg[2]. /* Si <> "" --> il y aura un log du traitement */
/*-------------------------------------------------------------------------------*/

/***********************************************************/
/* Chargement des valeurs MINI/MAXI des selections du .chx */
/***********************************************************/
run select-v.p ("societe"    , input-output codsoc-min      , input-output codsoc-max  ).
run select-v.p ("etablis"    , input-output codetb-min      , input-output codetb-max  ).
run select-v.p ("journal"    , input-output journal-min     , input-output journal-max ).
run select-v.p ("cptgen"     , input-output codgen-min      , input-output codgen-max  ).
run select-v.p ("codgrp"     , input-output codgrp-min      , input-output codgrp-max  ).

/***************/
/* Parametrage */
/***************************************************************************/
/* Def var x620-racine-cg-fact    like mvtcpt.codgen no-undo.  */
Def var x620-racine-cg-ht      as char no-undo.
Def var x620-liste-jnx         as char no-undo. /* Liste des journaux a traiter              */
Def var x620-code-groupe       as char no-undo. /* Code groupe issu du Siret de l'auxiliaire */
Def var x620-list-typie-exclus as char no-undo. /* Liste des types de piece a exclure        */

Def var w-sim-permax as int.
w-sim-permax = int ( s-permax ).

/*- Init sel-persim -*/
Do a = 1 to int (s-permax):
   sel-persim[a] = 1.
End.

assign sov-codsoc = codsoc-soc
       sov-codetb = codetb-etb.


/*******************************/
/* 1 - Boucle sur les societes */
/*******************************************************************************************************/
LECT-SOCIETE:
for each soc-tables where soc-tables.codsoc  = ""           
                      and soc-tables.etabli  = ""           
                      and soc-tables.typtab  = "SOC"        
                      and soc-tables.prefix  = "SOCIETE"  
                      and soc-tables.codtab >= codsoc-min  
                    no-lock:

 if soc-tables.codtab > codsoc-max then Leave LECT-SOCIETE.

 /*********************************/
 /* Doit-on traiter cette societe */
 /*********************************/
 {select-z.i "societe" "soc-tables.codtab"}
 run select-t.p. 
 if selection-ok <> "" then next LECT-SOCIETE.



 codsoc-soc = soc-tables.codtab .
 regs-app   = "elodie".
 {regs-cha.i}


 regs-fileacc = "CPTGEN" .
 { regs-rec.i }


 Find tables where tables.codsoc = regs-soc     
               and tables.etabli = ""           
               and tables.typtab = "TVA"        
               and tables.prefix = "RACINE-CG"  
               and tables.codtab = ""
 no-lock no-error.
 if available tables
 Then Assign racine-4       = tables.libel[1]
             racine-tva     = tables.libel[2]
             racine-7       = tables.libel[3]
             list-typie-tva = tables.tecnic[4] + tables.tecnic[5] + tables.tecnic[6] 
                            + tables.tecnic[7] + tables.tecnic[8] + tables.tecnic[9] + tables.tecnic[10]
             .
 Else Assign racine-4       = "4"
             racine-tva     = "44"
             racine-7       = "7"
             list-typie-tva = "".


 Assign /* x620-racine-cg-fact    = "" */
        x620-racine-cg-ht      = ""
        x620-liste-jnx         = ""
        x620-list-typie-exclus = "".

 Find x620-Tables where x620-Tables.codsoc = regs-soc
                   and x620-Tables.etabli = ""
                   and x620-Tables.typtab = "620"
                   and x620-Tables.prefix = "CARTE"
                   and x620-Tables.codtab = code-vue
 no-lock no-error.
 if not available x620-Tables
 then Find x620-Tables where x620-Tables.codsoc = regs-soc
                        and x620-Tables.etabli = ""
                        and x620-Tables.typtab = "620"
                        and x620-Tables.prefix = "CARTE"
                        and x620-Tables.codtab = ""
 no-lock no-error.
 /* if not available x620-Tables Then Next.       */

 /* Racine de compte facture */
 if available x620-Tables then Assign x620-liste-jnx         = x620-Tables.tecnic[1] + x620-Tables.tecnic[2] + x620-Tables.tecnic[3]
                                      /* x620-racine-cg-fact    = x620-Tables.tecnic[4] + x620-Tables.tecnic[5] */
                                      x620-racine-cg-ht      = x620-Tables.tecnic[6] + x620-Tables.tecnic[7]
                                      x620-list-typie-exclus = x620-Tables.tecnic[8] + x620-Tables.tecnic[9] + x620-Tables.tecnic[10].
 /* if x620-racine-cg-fact = "" then x620-racine-cg-fact = "411". */
 if x620-racine-cg-ht   = "" then x620-racine-cg-ht   = "6,7".

 /* Recherche exercice */
 find b-tables where b-tables.codsoc  = soc-tables.codtab   
                 and b-tables.etabli = ""              
                 and b-tables.typtab = "PAR"           
                 and b-tables.prefix = "EXERCICE"      
                 and b-tables.codtab = s-exe
 no-lock no-error .
 if not available b-tables then next .

 Assign indice-an  = b-tables.nombre[4]
        indice-max = b-tables.nombre[1].



 /*************************************/
 /* 2 - Boucle sur les etablissements */
 /*****************************************************************************************************/
 LECT-ETABLIS:
 for each etb-tables where etb-tables.codsoc = soc-tables.codtab   
                       and etb-tables.etabli = ""              
                       and etb-tables.typtab = "ETA"           
                       and etb-tables.prefix = "ETABLIS"
                       and etb-tables.codtab >= codetb-min       
                     no-lock :

   if etb-tables.codtab > codetb-max then Leave LECT-ETABLIS.

   /*************************************/
   /* Doit-on traiter cet etablissement */
   /*************************************/
   {select-z.i "etablis" "etb-tables.codtab"}
   run select-t.p.
   if selection-ok <> "" then next LECT-ETABLIS.


   /***************************************/
   /* 3 - Boucle sur les comptes generaux */
   /***************************************************************************************************/
   /* Recherche des soldes*/
   LECT-SOLDES:
   For each soldes where soldes.codsoc  = soc-tables.codtab    
                     and soldes.codetb  = etb-tables.codtab   
                     and soldes.execpt  = s-exe            
                     and soldes.codgen >= codgen-min
                     and soldes.codgen <= codgen-max
                     and soldes.typaux > ""                /* que les comptes auxiliarises : 401 , 411 */                      
                   no-lock:

    /*----------------------*/
    /*- Recherche du siret -*/
    /*------------------------------------------------------------*/
    x620-siret = "".
    regs-fileacc = "AUXILI" + soldes.Typaux.
    { regs-rec.i }
    for first auxili field( siret libabr adres[1] adres[2] adres[3] adres[4] adres[5] )
                     where auxili.codsoc = regs-soc     
                       and auxili.typaux = soldes.typaux 
                       and auxili.codaux = soldes.codaux
                     no-lock:
      x620-siret = auxili.siret.  
    end.

    /* Recherche du code groupe */
    x620-code-groupe = "".
    for first G-Tables field(Alpha[1])
                     where G-Tables.ident     = "620"
                       and G-Tables.progiciel = "Elodie"
                       and G-Tables.motcle    = "Param"
                       and G-Tables.codsoc    = ""
                       and G-Tables.etabli    = ""
                       and G-Tables.typtab    = "620"      
                       and G-Tables.prefix    = "FLUX-IG"  
                       and G-Tables.codtab    = x620-siret
                     no-lock:
      x620-code-groupe = G-Tables.Alpha[1].
    end.

      /**********************************/
     /* Doit-on traiter ce code groupe */
    /**********************************/
    {select-z.i "codgrp" "x620-code-groupe"}
    run select-t.p.

      /*************************************/
     /* Doit-on traiter ce compte general */
    /*************************************/
    {select-z.i "cptgen" "soldes.codgen"}
    run select-t.p.
    if selection-ok <> "" then next LECT-SOLDES.


    lig-info = "[1] " + soldes.codgen + " " + soldes.typaux + " " + soldes.codaux + " " 
             + soldes.devise + " " + soldes.execpt.
    If Zone-Charg[9] <> "BATCH"
    Then Do:
         Display lig-info With frame fr-info. Readkey pause 0.
         If keyfunction ( lastkey ) = "end-error" Then Leave LECT-SOCIETE.
    End.

    Assign w-an-cpt = Soldes.Dt-cpt[indice-an] - Soldes.Ct-cpt[indice-an]
           w-an-pie = Soldes.Dt-dev[indice-an] - Soldes.Ct-dev[indice-an].

    /******************************************************/
    /*- 3 - Boucle sur les mouvements comptables les TTC -*/
    /***************************************************************************************************/    
    LECT-MVTTTC:
    For each mvtcpt-ttc Use-Index {idx-relev.i}
                    where mvtcpt-ttc.codsoc = soldes.codsoc                        
                      and mvtcpt-ttc.codgen = soldes.codgen  
                      and mvtcpt-ttc.typaux = soldes.typaux  
                      and mvtcpt-ttc.codaux = soldes.codaux  
                      and mvtcpt-ttc.devpie = soldes.devise
                      /* and mvtcpt-ttc.top-lettrage = ""      /* < "2" */ */
                      and mvtcpt-ttc.codetb = soldes.codetb  
                      /* and mvtcpt.execpt + mvtcpt.percpt <= string ( s-exe , "XXXX" ) + string ( w-sim-permax , "999" )  */
                      and mvtcpt-ttc.execpt = string ( s-exe , "XXXX" ) 
                      and mvtcpt-ttc.percpt <= string ( w-sim-permax , "999" ) 
                      /* and mvtcpt-ttc.seq-releve = 0 */
                      and mvtcpt-ttc.valid-top = ""
                      and mvtcpt-ttc.piece     > ""
                      and ( x620-list-typie-exclus = "" or lookup ( trim (mvtcpt-ttc.typie  ) , x620-list-typie-exclus ) = 0 ) 
                      and ( x620-liste-jnx         = "" or lookup ( trim (mvtcpt-ttc.journal) , x620-liste-jnx    ) <> 0 )
                    no-lock :

      /*- Ne prendre que certains journaux -*/
      /* if x620-liste-jnx > "" and lookup ( mvtcpt.journal , x620-liste-jnx ) = 0 then Next. */

        /******************************/
       /* Doit-on traiter ce journal */
      /******************************/
      {select-z.i "journal" "mvtcpt-ttc.journal"}
      run select-t.p.
      if selection-ok <> "" then next LECT-MVTTTC.

      /*------------------------------------------------------------------------------*/  
      /*- Ce mouvement est bon a prendre                                             -*/
      /*- Est-ce que je retrouve le/les ht                                           -*/
      /*------------------------------------------------------------------------------*/ 
      x620-mvtok = "".
      /* Ici partir du 4xx et aller sommer les 6xx & 7xx pour HT */
      Assign vecteur-rowidht = ""
             tot-HT-devcpt   = 0
             tot-HT-devpie   = 0
             tot-HT-devini   = 0.

      /*- Vu Fred, il n'y a qu'un seul TTC par document -*/
      LECT-MVTHT:
      For each mvtcpt-ht where mvtcpt-ht.codsoc = mvtcpt-ttc.codsoc
                           and mvtcpt-ht.codetb = mvtcpt-ttc.codetb
                           and mvtcpt-ht.docume = mvtcpt-ttc.docume 
                           and mvtcpt-ht.valid-top     = ""
                         no-lock:

        /*- On ne prend que les Ht -*/
        if lookup ( substring(mvtcpt-ht.codgen,1,1), x620-racine-cg-ht ) = 0 then next.

        Assign tot-HT-devcpt = tot-HT-devcpt + mvtcpt-ht.dt-devcpt - mvtcpt-ht.ct-devcpt
               tot-HT-devpie = tot-HT-devpie + mvtcpt-ht.dt-devpie - mvtcpt-ht.ct-devpie
               tot-HT-devini = tot-HT-devini + mvtcpt-ht.dt-devini - mvtcpt-ht.ct-devini
               vecteur-rowidht = vecteur-rowidht + String(rowid (mvtcpt-ht)) + ",".

      end.


      lig-info = String(mvtcpt-ttc.Codsoc,"xxxx") + " " + String(mvtcpt-ttc.Codetb,"xxxx") + " " + String(mvtcpt-ttc.Codgen,"x(10)") + " " 
               + String(mvtcpt-ttc.Typaux,"xxx") + " " + String(mvtcpt-ttc.Codaux,"x(10)") + " " + x620-mvtok.
      If Zone-Charg[9] <> "BATCH"
      Then Do:
           Display lig-info With frame fr-info2.
           Down with frame fr-info2.  Readkey pause 0.
           If keyfunction ( lastkey ) = "end-error" Then Leave LECT-SOCIETE.     
      End.

      run log-edit-ligne. /* Edition du log */
      x620-nb-trt = x620-nb-trt + 1.
      If x620-mvtok > "" Then Next LECT-MVTTTC.

        /*-----------------------*/
       /*- Creation de precomp -*/
      /*-----------------------*/
      if w-an-cpt <> 0 or w-an-pie <> 0
      then do:
           Create t-fig.
           Assign t-fig.codsoc      = mvtcpt-ttc.codsoc        
                  t-fig.codetb      = mvtcpt-ttc.codetb        
                  t-fig.docume      = "      AN"        
                  t-fig.chrono      = "00000"        
                  t-fig.c-group     = x620-code-groupe   
                  t-fig.typaux      = mvtcpt-ttc.typaux                
                  t-fig.codaux      = mvtcpt-ttc.codaux                    
                  t-fig.devcpt      = mvtcpt-ttc.devcpt        
                  t-fig.devpie      = mvtcpt-ttc.devpie        
                  t-fig.devini      = mvtcpt-ttc.devini        
                  t-fig.an-cpt      = w-an-cpt 
                  t-fig.an-pie      = w-an-pie 
                  .                   

           If available Auxili
           Then Assign t-fig.siret       = auxili.siret  
                       t-fig.libabr      = auxili.libabr
                       t-fig.adresse[1]  = auxili.adres[1]
                       t-fig.adresse[2]  = auxili.adres[2]
                       t-fig.adresse[3]  = auxili.adres[3]
                       t-fig.adresse[4]  = auxili.adres[4]
                       t-fig.adresse[5]  = auxili.adres[5].

           Assign w-an-cpt   = 0 /* On ne reprend l'a nouveau que sur le premier */
                  w-an-pie   = 0.

      end.

      Create t-fig.
      Assign t-fig.codsoc      = mvtcpt-ttc.codsoc        
             t-fig.codetb      = mvtcpt-ttc.codetb        
             t-fig.docume      = mvtcpt-ttc.docume        
             t-fig.chrono      = mvtcpt-ttc.chrono        
             t-fig.c-group     = x620-code-groupe   
             t-fig.typaux      = mvtcpt-ttc.typaux                
             t-fig.codaux      = mvtcpt-ttc.codaux                    
             t-fig.devcpt      = mvtcpt-ttc.devcpt        
             t-fig.devpie      = mvtcpt-ttc.devpie        
             t-fig.devini      = mvtcpt-ttc.devini        
             t-fig.r-ttc       = rowid (mvtcpt-ttc)                 
             t-fig.v-ht        = vecteur-rowidht
             t-fig.ttc-cpt     = mvtcpt-ttc.dt-devcpt - mvtcpt-ttc.ct-devcpt
             t-fig.ttc-pie     = mvtcpt-ttc.dt-devpie - mvtcpt-ttc.ct-devpie                    
             t-fig.ttc-ini     = mvtcpt-ttc.dt-devini - mvtcpt-ttc.ct-devini                    
             t-fig.ht-cpt      = tot-HT-devcpt                    
             t-fig.ht-pie      = tot-HT-devpie                    
             t-fig.ht-ini      = tot-HT-devini
             .                   

      If available Auxili
      Then Assign t-fig.siret       = auxili.siret  
                  t-fig.libabr      = auxili.libabr
                  t-fig.adresse[1]  = auxili.adres[1]
                  t-fig.adresse[2]  = auxili.adres[2]
                  t-fig.adresse[3]  = auxili.adres[3]
                  t-fig.adresse[4]  = auxili.adres[4]
                  t-fig.adresse[5]  = auxili.adres[5].

      x620-nb-ok = x620-nb-ok + 1.


    End. /* Boucle sur les mouvements */

   End. /* Boucle sur les soldes */

 End. /* Each etb-tables : Boucle sur les etablissements */

End. /* Each soc-tables : Boucle sur les societe */


Assign ma-nblig = 0 Ma-nblig-recap = 0 Ma-nblig-det = 0.
run Edition-fig. /* Edition */ 

assign codsoc-soc = sov-codsoc
       codetb-etb = sov-codetb.

/*- Suppression de la selection de preparation -*/
RUN sel-del.p(selection-applic,selection-filtre,selection-sequence,YES).



                                /*---------------------------------------*/
                                /*-     L E S   P R O C E D U R E S     -*/
/*------------------------------------------------------------------------------------------------------------*/
{ selectpt.i }

Procedure Edition-fig:

  For Each t-fig use-index SECONDAIRE
                 no-lock 
                 break by t-fig.codsoc 
                       by t-fig.c-group:


    if first-of ( t-fig.codsoc ) 
    then do:
         run edit-entete (1).
    end.

    if first-of ( t-fig.c-group ) 
    then do:
         run edit-entete (2).
    end.

    /*-----------------------------*/
    /*- Edition des lignes detail -*/
    /*-----------------------------------------------------------------------*/


    run edit-ligne.



    if last-of ( t-fig.c-group ) 
    then do:
         Assign cum-ttc-cpt[ 2] = cum-ttc-cpt[ 2] + cum-ttc-cpt[ 3] 
                cum-ttc-pie[ 2] = cum-ttc-pie[ 2] + cum-ttc-pie[ 3] 
                cum-ttc-ini[ 2] = cum-ttc-ini[ 2] + cum-ttc-ini[ 3] 
                cum-ht-cpt [ 2] = cum-ht-cpt [ 2] + cum-ht-cpt [ 3] 
                cum-ht-pie [ 2] = cum-ht-pie [ 2] + cum-ht-pie [ 3] 
                cum-ht-ini [ 2] = cum-ht-ini [ 2] + cum-ht-ini [ 3] 
                cum-an-cpt [ 2] = cum-an-cpt [ 2] + cum-an-cpt [ 3] 
                cum-an-pie [ 2] = cum-an-pie [ 2] + cum-an-pie [ 3]

                cum-ttc-cpt[ 3] = 0
                cum-ttc-pie[ 3] = 0
                cum-ttc-ini[ 3] = 0
                cum-ht-cpt [ 3] = 0
                cum-ht-pie [ 3] = 0
                cum-ht-ini [ 3] = 0
                cum-an-cpt [ 3] = 0
                cum-an-pie [ 3] = 0.

         run edit-rupture (2).
    end.

    if last-of ( t-fig.codsoc ) 
    then do:
         Assign cum-ttc-cpt[ 1] = cum-ttc-cpt[ 1] + cum-ttc-cpt[ 2] 
                cum-ttc-pie[ 1] = cum-ttc-pie[ 1] + cum-ttc-pie[ 2] 
                cum-ttc-ini[ 1] = cum-ttc-ini[ 1] + cum-ttc-ini[ 2] 
                cum-ht-cpt [ 1] = cum-ht-cpt [ 1] + cum-ht-cpt [ 2] 
                cum-ht-pie [ 1] = cum-ht-pie [ 1] + cum-ht-pie [ 2] 
                cum-ht-ini [ 1] = cum-ht-ini [ 1] + cum-ht-ini [ 2] 
                cum-an-cpt [ 1] = cum-an-cpt [ 1] + cum-an-cpt [ 2] 
                cum-an-pie [ 1] = cum-an-pie [ 1] + cum-an-pie [ 2]

                cum-ttc-cpt[ 2] = 0
                cum-ttc-pie[ 2] = 0
                cum-ttc-ini[ 2] = 0
                cum-ht-cpt [ 2] = 0
                cum-ht-pie [ 2] = 0
                cum-ht-ini [ 2] = 0
                cum-an-cpt [ 2] = 0
                cum-an-pie [ 2] = 0.

         run edit-rupture (1).
    end.

  end.

end procedure. /* Edition-fig */


Procedure edit-entete:

  Def input parameter p1 as int.

  Def buffer l-tables for tables.

  If p1 < 3 and Ma-nblig-recap /* ma-nblig */ > ma-liguti 
  Then Do:
       run edimaq (10).
       Ma-nblig-recap /* ma-nblig */ = 0.       
  End.
  If p1 > 3 and Ma-nblig-det > ma-liguti
  Then Do:
       run edimaq-detail (30).
       Ma-nblig-det = 0.       
  End.

  Assign no-page     = no-page     + 1 when p1 < 3
         no-page-det = no-page-det + 1 when p1 > 3
         .
  assign edit[ 1] = t-fig.codsoc
         edit[ 5] = string ( today , "99/99/9999" )
         edit[ 6] = string ( time  , "hh:mm" )
         edit[ 7] = string ( no-page     )
         edit[ 8] = string ( no-page-det )
         edit[ 9] = s-exe
         edit[10] = s-permax    
         edit[11] = t-fig.c-group
         edit[13] = String ( today , "99/99/9999" )
         edit[14] = string ( time  , "hh:mm:ss" )
         edit[15] = Operat
         .


  /* Libelle Societe */
  for first l-tables field (libel1[1])
                     where l-tables.codsoc = ""            
                       and l-tables.etabli = ""            
                       and l-tables.typtab = "SOC"         
                       and l-tables.prefix = "SOCIETE"     
                       and l-tables.codtab = t-fig.Codsoc
                     no-lock:
    edit[3] = l-tables.libel1[1].
  end.

  case p1:
       when 1 then do: /* Societe */
            {maqtmajm.i "01" "edit" "maqtchar" "maqtecrit" "ma-maquet" }
            run edimaq (01).
       end.
       when 2 then do: /* Code regroupement */
            {maqtmajm.i "02" "edit" "maqtchar" "maqtecrit" "ma-maquet" }
            run edimaq (02).
       end.
       when 21 then do: /* Societe */
            {maqtmajm.i "21" "edit" "maqtchar" "maqtecrit" "ma-maquet" }
            run edimaq-detail (21).
       end.
       when 22 then do: /* Code regroupement */
            {maqtmajm.i "22" "edit" "maqtchar" "maqtecrit" "ma-maquet" }
            run edimaq-detail (22).
       end.
  end case.

  edit = "" .

end procedure.

Procedure edit-rupture:

  Def input parameter p1 as int.

  Def buffer l-tables for tables.

  If Ma-nblig-recap /* ma-nblig */ > ma-liguti 
  Then Do:
       run edit-entete (1). 
  End.

  Assign edit[ 1] = t-fig.codsoc     
         edit[ 2] = t-fig.codetb     
         edit[ 3] = t-fig.docume     
         edit[ 4] = t-fig.chrono     
         edit[ 5] = t-fig.c-group    
         edit[ 6] = t-fig.siret      
         edit[ 7] = t-fig.typaux     
         edit[ 8] = t-fig.codaux     
         edit[ 9] = t-fig.libabr     
         edit[10] = t-fig.adresse [1]
         edit[11] = t-fig.adresse [2]
         edit[12] = t-fig.adresse [3]
         edit[13] = t-fig.adresse [4]
         edit[14] = t-fig.adresse [5]
         edit[15] = t-fig.devcpt     
         edit[16] = t-fig.devpie     
         edit[17] = t-fig.devini.    

    If p1 > 0 
    then Assign edit[18] = String ( cum-ttc-cpt[p1] )
                edit[19] = String ( cum-ttc-pie[p1] )
                edit[20] = String ( cum-ttc-ini[p1] )
                edit[21] = String ( cum-ht-cpt [p1] )
                edit[22] = String ( cum-ht-pie [p1] )
                edit[23] = String ( cum-ht-ini [p1] )
                edit[24] = String ( cum-an-cpt [p1] )
                edit[25] = String ( cum-an-pie [p1] )
                .    

  if p1 = 1 /* Societe */
  then Do:
       {maqtmajm.i "09" "edit" "maqtchar" "maqtecrit" "ma-maquet" }
       run edimaq (09).

       {maqtmajm.i "29" "edit" "maqtchar" "maqtecrit" "ma-maquet" }
       run edimaq-detail (29).

  end.
  else do: /* Code regroupement */
       {maqtmajm.i "08" "edit" "maqtchar" "maqtecrit" "ma-maquet" }
       run edimaq (08).

       {maqtmajm.i "28" "edit" "maqtchar" "maqtecrit" "ma-maquet" }
       run edimaq-detail (28).

  end.
  edit = "" .

end procedure.

Procedure edit-ligne:

  If Ma-nblig-det > ma-liguti 
  Then Do:
       run edit-entete (21). 
  End.

/* Si l'on doit lire le TTC 
  Find mvtcpt-ttc where rowid ( mvtcpt-ttc ) = t-fig.r-ttc no-lock no-error.
  If not available mvtcpt-ttc then next.
*/

  Assign edit[ 1] = t-fig.codsoc             
         edit[ 2] = t-fig.codetb             
         edit[ 3] = t-fig.docume             
         edit[ 4] = t-fig.chrono             
         edit[ 5] = t-fig.c-group            
         edit[ 6] = t-fig.siret              
         edit[ 7] = t-fig.typaux             
         edit[ 8] = t-fig.codaux             
         edit[ 9] = t-fig.libabr             
         edit[10] = t-fig.adresse [1]        
         edit[11] = t-fig.adresse [2]        
         edit[12] = t-fig.adresse [3]        
         edit[13] = t-fig.adresse [4]        
         edit[14] = t-fig.adresse [5]        
         edit[15] = t-fig.devcpt             
         edit[16] = t-fig.devpie             
         edit[17] = t-fig.devini             
         edit[18] = String ( t-fig.ttc-cpt ) 
         edit[19] = String ( t-fig.ttc-pie ) 
         edit[20] = String ( t-fig.ttc-ini ) 
         edit[21] = String ( t-fig.ht-cpt  ) 
         edit[22] = String ( t-fig.ht-pie  ) 
         edit[23] = String ( t-fig.ht-ini  ) 
         edit[24] = String ( t-fig.an-cpt  ) 
         edit[25] = String ( t-fig.an-pie  ) 
         .

  Assign cum-ttc-cpt[ 3] = cum-ttc-cpt[ 3] + t-fig.ttc-cpt
         cum-ttc-pie[ 3] = cum-ttc-pie[ 3] + t-fig.ttc-pie
         cum-ttc-ini[ 3] = cum-ttc-ini[ 3] + t-fig.ttc-ini
         cum-ht-cpt [ 3] = cum-ht-cpt [ 3] + t-fig.ht-cpt
         cum-ht-pie [ 3] = cum-ht-pie [ 3] + t-fig.ht-pie
         cum-ht-ini [ 3] = cum-ht-ini [ 3] + t-fig.ht-ini
         cum-an-cpt [ 3] = cum-an-cpt [ 3] + t-fig.an-cpt
         cum-an-pie [ 3] = cum-an-pie [ 3] + t-fig.an-pie.


  {maqtmajm.i "25" "edit" "maqtchar" "maqtecrit" "ma-maquet" }
  run edimaq-detail (25).
  edit = "" .

end procedure.

procedure edimaq-detail:
  def input parameter rang as int .
  { maqtedir.i rang "maqtchar" "maqdet" "ma-maquet" }
  Ma-nblig-det = Ma-nblig-det + 1. 
end procedure .

procedure edimaq :
  def input parameter rang as int .
  { maqtedir.i rang "maqtchar" "maquette" "ma-maquet" }
  Ma-nblig-recap = Ma-nblig-recap + 1. 
end procedure .


Procedure charge-batch:

    {bat-cha.i  "lib-prog"    "lib-prog"    "a" }
    {bat-cha.i  "ma-impri"    "ma-impri"    "a" }
    {bat-cha.i  "ma-maquet"   "ma-maquet"   "a" }
    {bat-cha.i  "ma-esc"      "ma-esc"      "a" }
    {bat-cha.i  "mem-langue"  "mem-langue"  "n" }
    {bat-cha.i  "mem-lang"    "mem-lang"    "a" }
    {bat-cha.i  "s-exe"       "s-exe"       "a" }
    {bat-cha.i  "s-permax"    "s-permax"    "a" }
    {bat-cha.i  "lib-vue"     "lib-vue"     "a" }
    {bat-cha.i  "code-vue"    "code-vue"    "a" }    
    {bat-cha.i  "x620-seq"     "x620-seq"     "n" }

/*    
    {bat-cham.i "sel-persim"  "sel-persim"  "n" "m"  "110" }
*/    

    /*********************************************************/
    /* Zones utilisees pour recuperer les selections du .chx */
    /*********************************************************/
    {bat-cha.i "selection-applic"   "selection-applic"   "a"}
    {bat-cha.i "selection-filtre"   "selection-filtre"   "a"}
    {bat-cha.i "selection-sequence" "selection-sequence" "a"}
    { bat-cham.i "zone-charg"     "zone-charg" "a" "m" "8"  }

end procedure. /* charge-batch */


/*- Edition du log : Trace le cheminement de la recherche des documents -*/
Procedure log-edit-entete:
    Def buffer l-tables for tables.

    If ma-nblig > ma-liguti 
    Then Do:
         {maqtedir.i "90" "maqtchar" "x620-log" "ma-maquet" }       
         ma-nblig = 0.
    End.

    no-page = no-page + 1 .
    assign edit[ 1] = mvtcpt-ttc.Codsoc
           edit[ 2] = mvtcpt-ttc.Codetb
           edit[ 5] = string ( today , "99/99/9999" )
           edit[ 6] = string ( time  , "hh:mm" )
           edit[ 7] = string ( no-page , "ZZZ9" )
           edit[ 9] = s-exe
           edit[10] = s-permax    
           edit[11] = code-vue            
           edit[12] = lib-vue 
           edit[13] = String ( today , "99/99/9999" )
           edit[14] = string ( time  , "hh:mm:ss" )
           edit[15] = Operat
           edit[16] = String ( x620-seq )
           .


    /* Libelle Societe */
    find l-tables where l-tables.codsoc = ""            
                  and l-tables.etabli = ""            
                  and l-tables.typtab = "SOC"         
                  and l-tables.prefix = "SOCIETE"     
                  and l-tables.codtab = mvtcpt-ttc.Codsoc
    no-lock no-error.
    if available l-tables then edit[3] =  l-tables.libel1[1] .

     /* Libelle Etablissement */
    find l-tables where l-tables.codsoc = mvtcpt-ttc.Codsoc
                  and l-tables.etabli = ""            
                  and l-tables.typtab = "ETA"         
                  and l-tables.prefix = "ETABLIS"     
                  and l-tables.codtab = mvtcpt-ttc.Codetb
    no-lock no-error.
    if available l-tables then edit[4] =  l-tables.libel1[1] .

    {maqtmajm.i "91" "edit" "maqtchar" "maqtecrit" "ma-maquet" }
    {maqtedir.i "91" "maqtchar" "x620-log" "ma-maquet" }
    edit = "" .

end procedure.


Procedure log-edit-ligne:

  if x620-trt-log = "" then return. /* Traitement sans gestion du log */ 

  def buffer x-mvtcpt for mvtcpt.
  def var x1          as int.
  def var x2          as int.

  If ma-nblig > ma-liguti 
  Then Do:
       {maqtedir.i "90" "maqtchar" "x620-log" "ma-maquet" }       
       ma-nblig = 0.
  End.
  If ma-nblig = 0 Then run log-edit-entete.


  Assign /* Mouvement d'impaye analyse */
         Edit[ 1] = String ( mvtcpt-ttc.Datpie )
         Edit[ 2] = mvtcpt-ttc.Codgen
         Edit[ 3] = mvtcpt-ttc.Piece
         Edit[ 4] = mvtcpt-ttc.Docume
         Edit[ 5] = mvtcpt-ttc.chrono
         Edit[ 6] = mvtcpt-ttc.Num-releve
         /*- En devcpt -*/
         Edit[10] = mvtcpt-ttc.devcpt
         Edit[11] = String ( mvtcpt-ttc.dt-devcpt )
         Edit[12] = String ( mvtcpt-ttc.ct-devcpt )
         Edit[13] = String ( mvtcpt-ttc.dt-devcpt - mvtcpt-ttc.ct-devcpt ) 
         /*- En devpie -*/         
         Edit[20] = mvtcpt-ttc.devpie                                 
         Edit[21] = String ( mvtcpt-ttc.dt-devpie )                   
         Edit[22] = String ( mvtcpt-ttc.ct-devpie )                   
         Edit[23] = String ( mvtcpt-ttc.dt-devpie - mvtcpt-ttc.ct-devpie )
         /*- En devini -*/
         Edit[30] = mvtcpt-ttc.devini                                 
         Edit[31] = String ( mvtcpt-ttc.dt-devini )                   
         Edit[32] = String ( mvtcpt-ttc.ct-devini )                   
         Edit[33] = String ( mvtcpt-ttc.dt-devini - mvtcpt-ttc.ct-devini )

         Edit[43] = mvtcpt-ttc.num-releve 
         .


    regs-fileacc = "AUXILI" + mvtcpt-ttc.Typaux.
    { regs-rec.i }
    find auxili where auxili.codsoc = regs-soc     
                  and auxili.typaux = mvtcpt-ttc.typaux 
                  and auxili.codaux = mvtcpt-ttc.codaux
    no-lock no-error.
    if available auxili then edit[41] = auxili.adres[1] .



    {maqtmajm.i "92" "edit" "maqtchar" "maqtecrit" "ma-maquet" }
    {maqtedir.i "92" "maqtchar" "x620-log" "ma-maquet" }
    edit = "" .

End procedure.



