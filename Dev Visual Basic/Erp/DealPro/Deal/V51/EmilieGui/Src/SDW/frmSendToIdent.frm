VERSION 5.00
Begin VB.Form frmSendToIdent 
   Caption         =   "Envoyer un message vers un autre client"
   ClientHeight    =   5490
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7230
   LinkTopic       =   "Form1"
   ScaleHeight     =   5490
   ScaleWidth      =   7230
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1080
      ScaleHeight     =   345
      ScaleWidth      =   5265
      TabIndex        =   18
      Top             =   3240
      Visible         =   0   'False
      Width           =   5295
      Begin VB.Label Label15 
         BackStyle       =   0  'Transparent
         Caption         =   "En attente de la r�ponse du serveur ....................................................."
         Height          =   375
         Left            =   120
         TabIndex        =   19
         Top             =   60
         Width           =   5055
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Envoyer"
      Height          =   255
      Left            =   5640
      TabIndex        =   17
      Top             =   5160
      Width           =   1455
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   120
      MaxLength       =   250
      TabIndex        =   16
      Top             =   4680
      Width           =   6975
   End
   Begin VB.Frame Frame1 
      Caption         =   "Param�tres : "
      Height          =   2175
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   6975
      Begin VB.Label Label13 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00FF0000&
         Height          =   615
         Left            =   1440
         TabIndex        =   14
         Top             =   1440
         Width           =   5415
      End
      Begin VB.Label Label12 
         Caption         =   "Libell� : "
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label Label11 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   1440
         TabIndex        =   12
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Label10 
         Caption         =   "Message N� :"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label Label9 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   3960
         TabIndex        =   10
         Top             =   720
         Width           =   2775
      End
      Begin VB.Label Label8 
         Caption         =   "Client Emetteur : "
         Height          =   255
         Left            =   2640
         TabIndex        =   9
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   3960
         TabIndex        =   8
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label6 
         Caption         =   "Progiciel : "
         Height          =   255
         Left            =   2640
         TabIndex        =   7
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   1440
         TabIndex        =   6
         Top             =   720
         Width           =   855
      End
      Begin VB.Label Label4 
         Caption         =   "Version : "
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   1440
         TabIndex        =   4
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Code Langue : "
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   360
         Width           =   2055
      End
   End
   Begin VB.ListBox List1 
      Height          =   1230
      Left            =   120
      TabIndex        =   1
      Top             =   2880
      Width           =   6975
   End
   Begin VB.Label Label14 
      Caption         =   "Libell� : "
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   4320
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Veuillez s�lectionner un client de destination : "
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   2520
      Width           =   3735
   End
End
Attribute VB_Name = "frmSendToIdent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

Dim Rep As VbMsgBoxResult
Dim Ident As String
Dim StrMess As String
Dim hdlFile As Integer
Dim Ligne As String

On Error GoTo GestError

'-> V�rifier qu'un ident soit saisi
If Me.List1.ListIndex = -1 Then
    MsgBox "Veuillez s�lectionner un client de r�ception", vbCritical + vbOKOnly, "Erreur"
    Me.List1.SetFocus
    Exit Sub
End If

'-> R�cup�rer le code de l'ident
Ident = UCase$(Trim(Entry(1, Me.List1.Text, "-")))

'-> Demander confirmation
If Trim(Me.Text1.Text) = "" Then
    StrMess = "Attention : demande de suppression du message : " & Me.Label11.Caption & " pour le client : " & Ident
Else
    StrMess = "Transf�rer le message N� : " & Me.Label11.Caption & " : " & Me.Text1.Text & Chr(13) & Chr(10) & "vers l'ident : " & Ident & Chr(13) & Chr(10) & "Attention : si le message existe d�ja, il sera modifi�"
End If
    
Rep = MsgBox(StrMess, vbExclamation + vbYesNo, "Confirmation")
If Rep = vbNo Then Exit Sub

'-> Bloquer l'interface
Me.Enabled = False
Me.MousePointer = 11

'-> Afficher le message d'attente
Me.Picture2.Visible = True

'-> Cr�ation d'une demande
hdlFile = OpenDmd
Print #hdlFile, "MESS_SEND~" & Me.Label3.Caption & "~" & Ident & "~" & Me.Label5.Caption & "~" & Me.Label7.Caption & "~" & Me.Label11.Caption & "|" & Me.Text1.Text
Close #hdlFile

'-> Faire un rename dans le r�pertoire de transfert
Name TempoFileName As DmdFileName

'-> Se mettre en attente de la r�ponse
Do
    '-> Rendre la main � la CPU
    DoEvents
    '-> V�rifier si on trouve le fichier r�ponse
    If Dir$(RspFileName, vbNormal) <> "" Then Exit Do
Loop

'-> Lire la r�ponse
hdlFile = FreeFile
Open RspFileName For Input As #hdlFile
Line Input #hdlFile, Ligne
Close #hdlFile

'-> Supprimer le fichier RSP
Kill RspFileName

'-> Analyser la r�ponse
Select Case Entry(3, Ligne, "~")
    Case "MODIFY"
        MsgBox "Message modifi�", vbInformation + vbOKOnly, "Confirmation"
    Case "CREATE"
        MsgBox "Message transf�r� et cr��", vbInformation + vbOKOnly, "Confirmation"
    Case "DELETE"
        MsgBox "Message supprim�", vbInformation + vbOKOnly, "Confirmation"
End Select

GestError:

    '-> D�bloquer l'interface
    Me.Enabled = True
    Me.MousePointer = 0

    '-> D�charger la feuille
    Unload Me

End Sub

Private Sub Form_Load()

'-> Charger la liste des clients
For i = 1 To NumEntries(frmMess.ListeClient, "|")
    If UCase$(Entry(i, frmMess.ListeClient, "|")) <> "DEAL" Then
        Me.List1.AddItem Entry(i, frmMess.ListeClient, "|") & " - " & Entry(i, frmMess.LibClient, "|")
    End If
Next

'-> S�lectionner le premier Ident
If Me.List1.ListCount <> 0 Then
    Me.List1.Selected(0) = True
Else
    Me.Command1.Enabled = False
End If

End Sub

Private Sub Text1_GotFocus()

Me.Text1.SelStart = 0
Me.Text1.SelLength = Len(Me.Text1.Text)


End Sub
