Attribute VB_Name = "fctSdw"
'-> Param�tres de l'utilisateur
Public UserName As String '-> Nom
Public UserPwd As String '-> Mot de passe
Public UserApp As String '-> Liste des applicatifs autoris�s
Public UserLib As String '-> Nom en clair de l'utilisateur
Public UserValidDmd As Boolean '-> Indique si la personne peut valider une demande
Public ComputerName As String '-> Nom de la machine
Public CreateMessage As Boolean  '-> Indique si on peut cr�er des messages

'-> Variables de gestion des demandes
Public ListeServices As String
Public ListeProgiciels As String
Public ListeNatures As String
Public ListeVersions As String
Public ListeClients As String

'-> D�claration des API
Public Declare Function GetComputerName& Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long)
'-> API
Public Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Public Declare Function GetClientRect& Lib "user32" (ByVal hwnd As Long, lpRect As RECT)
Public strRetour As String

Public DmdFileName As String
Public TempoFileName As String
Public RspFileName As String

Sub Main()

'---> Point d'entr�e du programme

Dim Res As Long
Dim lpBuffer As String
Dim lpBufferA As String


'-> R�cup�rer le nom de la machine
lpBuffer = Space$(250)
Res = GetComputerName(lpBuffer, Len(lpBuffer))
ComputerName = Entry(1, lpBuffer, Chr(0))

'-> Charger la liste des services
lpBuffer = Space$(5000)
Res = GetPrivateProfileString("DMD", "SERVICES", "", lpBuffer, Len(lpBuffer), App.Path & "\param\sdw.ini")
ListeServices = Mid$(lpBuffer, 1, Res)

'-> Charger la liste des natures
lpBuffer = Space$(5000)
Res = GetPrivateProfileString("DMD", "NATURES", "", lpBuffer, Len(lpBuffer), App.Path & "\param\sdw.ini")
ListeNatures = Mid$(lpBuffer, 1, Res)

'-> charger la liste des progiciels
lpBuffer = Space$(5000)
Res = GetPrivateProfileString("SDW", "SDW_APPS", "", lpBuffer, Len(lpBuffer), App.Path & "\param\sdw.ini")
ListeProgiciels = Mid$(lpBuffer, 1, Res)
    
'-> Charger la liste des versions
lpBuffer = Space$(5000)
Res = GetPrivateProfileString("SDW", "SDW_VERSIONS", "", lpBuffer, Len(lpBuffer), App.Path & "\param\sdw.ini")
ListeVersions = Mid$(lpBuffer, 1, Res)

'-> Charger la liste des clients
lpBuffer = Space$(5000)
Res = GetPrivateProfileString("SDW", "LISTE_CLIENTS", "", lpBuffer, Len(lpBuffer), App.Path & "\param\sdw.ini")
lpBuffer = Mid$(lpBuffer, 1, Res)
    
'-> R�cup�ration des lib�ll�s des clients
For i = 1 To NumEntries(lpBuffer, "|")
    lpBufferA = Space$(2000)
    Res = GetPrivateProfileString("SDW", "LIB", "", lpBufferA, Len(lpBufferA), App.Path & "\param\" & Entry(i, lpBuffer, "|") & ".ini")
    If ListeClients = "" Then
        ListeClients = Entry(i, lpBuffer, "|") & "�" & Mid$(lpBufferA, 1, Res)
    Else
        ListeClients = ListeClients & "|" & Entry(i, lpBuffer, "|") & "�" & Mid$(lpBufferA, 1, Res)
    End If
Next 'Pour tous les clients

'-> Afficher l'interface client
MdiMain.Show

'-> Afficher la feuille de controle
frmPass.Show vbModal


End Sub

Public Function GetLibel(ByVal Id As String, ByVal TypeAnalyse As Integer) As String

'---> cette fonction retourne le libelle d'un client par analyse de la matrice

Dim i As Integer
Dim DefObject As String
Dim Matrice As String


'-> Selon l'analyse
Select Case TypeAnalyse
    Case 0 'Service
        Matrice = ListeServices
    Case 1 'Progiciels
        Matrice = ListeProgiciels
    Case 2 'Nature
        Matrice = ListeNatures
    Case 3 'Clients
        Matrice = ListeClients
End Select

For i = 1 To NumEntries(Matrice, "|")
    '-> Analyse de la matrice
    DefObject = Entry(i, Matrice, "|")
    '-> Retourner le code
    If UCase$(Entry(1, DefObject, "�")) = UCase$(Id) Then
        GetLibel = Entry(2, DefObject, "�")
        Exit Function
    End If
Next 'Pour tous les clients

    

End Function


Public Function OpenDmd() As Integer

'---> Cette fonction retourne le handle d'un fichier demande

Dim hdlFile As Integer

'-> Cr�ation des noms de fichier
DmdFileName = App.Path & "\messages\trans\" & ComputerName & "_" & MdiMain.hwnd & ".dmd"
TempoFileName = App.Path & "\temp\" & ComputerName & "_" & MdiMain.hwnd & ".tmp"
RspFileName = App.Path & "\messages\trans\" & ComputerName & "_" & MdiMain.hwnd & ".rsp"

'-> V�rifier qu'il n'y a pas une demande ou une r�ponse qui traine
If Dir$(DmdFileName, vbNormal) <> "" Then Kill DmdFileName
If Dir$(RspFileName, vbNormal) <> "" Then Kill RspFileName

'-> Cr�ation d'un fichier tempo
hdlFile = FreeFile
Open TempoFileName For Output As #hdlFile

'-> Renvoyer le handle
OpenDmd = hdlFile

End Function
