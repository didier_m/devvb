VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Client"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'-> Propri�t� d'un client
Public IdClient As String '-> Code client
Public ListeApp As String '-> Liste des app du client
Public Apps As Collection '-> Collection des applicatifs
Public IniFile As String '-> Fichier ini de param�trage
Public MessageFile As String '-> fichier des messages associ�


Private pLangue(1 To 10) As Boolean

Public Function IsCodeLangue(IdLangue As Integer) As Boolean

'---> Cette fonction indique s'il existe une traduction dans cette langue
IsCodeLangue = pLangue(IdLangue)

End Function

Public Sub SetCodeLangue(IdLangue As Integer, IsLangue As Boolean)

'---> Cette fonction fait le setting de la pr�sence d'un fichier langue
pLangue(IdLangue) = IsLangue

End Sub

Private Sub Class_Initialize()

'---> Intialiser la collection des applicatifs d'un client
Set Apps = New Collection

End Sub
