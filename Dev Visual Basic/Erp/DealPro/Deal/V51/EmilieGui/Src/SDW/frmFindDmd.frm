VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmFindDmd 
   Caption         =   "Recherche des demandes en cours"
   ClientHeight    =   6510
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6750
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   434
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   450
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
      Height          =   2895
      Left            =   0
      TabIndex        =   2
      Top             =   1200
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   5106
      _Version        =   393216
      FixedCols       =   0
      ScrollBars      =   2
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   1560
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFindDmd.frx":0000
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6750
      _ExtentX        =   11906
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   1
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FIND"
            Object.ToolTipText     =   "Recherche"
            ImageIndex      =   1
         EndProperty
      EndProperty
   End
   Begin VB.Label lblMouse 
      Caption         =   "infos@deal-informatique.com"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   375
      Left            =   3000
      MouseIcon       =   "frmFindDmd.frx":031A
      MousePointer    =   99  'Custom
      TabIndex        =   1
      Top             =   5760
      Width           =   2535
   End
End
Attribute VB_Name = "frmFindDmd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()

'-> Nombre de colonnes
Me.MSFlexGrid1.Cols = 6

'-> Que la ligne d'entete
Me.MSFlexGrid1.Rows = 1

'-> Inscrire les titres de colonnes
Me.MSFlexGrid1.Col = 0
Me.MSFlexGrid1.Text = "N� dmd"
Me.MSFlexGrid1.Col = 1
Me.MSFlexGrid1.Text = "Valid"
Me.MSFlexGrid1.Col = 2
Me.MSFlexGrid1.Text = "Client"
Me.MSFlexGrid1.Col = 3
Me.MSFlexGrid1.Text = "Progiciel"
Me.MSFlexGrid1.Col = 4
Me.MSFlexGrid1.Text = "Descriptif"

'-> Bloquer le flexGrid
Me.MSFlexGrid1.Enabled = False

End Sub

Private Sub Form_Resize()

Dim Res As Long
Dim aRect As RECT

On Error Resume Next

'-> Bien positionner le flex grid
Res = GetClientRect(Me.hwnd, aRect)
Me.MSFlexGrid1.Left = 0
Me.MSFlexGrid1.Width = aRect.Right
Me.MSFlexGrid1.Top = Me.Toolbar1.Height
Me.MSFlexGrid1.Height = aRect.Bottom - Me.MSFlexGrid1.Top


'-> R�cup�rer les dimensions interne du flex grid
Res = GetClientRect(Me.MSFlexGrid1.hwnd, aRect)

'-> Redimensionner la taille des diff�rentes colonnes
Me.MSFlexGrid1.ColWidth(0) = Me.ScaleX(3, 7, 1)
Me.MSFlexGrid1.ColWidth(1) = Me.ScaleX(40, 3, 1)
Me.MSFlexGrid1.ColWidth(2) = Me.ScaleX(2, 7, 1)
Me.MSFlexGrid1.ColWidth(3) = Me.ScaleX(2, 7, 1)
Me.MSFlexGrid1.ColWidth(4) = Me.ScaleX(aRect.Right, 4, 1) - Me.MSFlexGrid1.ColWidth(0) - Me.MSFlexGrid1.ColWidth(1) - Me.MSFlexGrid1.ColWidth(0) - Me.MSFlexGrid1.ColWidth(3)

End Sub

Private Sub MSFlexGrid1_Click()

If Me.MSFlexGrid1.Col = 0 Then
    frmDmd.DmdFile = Me.MSFlexGrid1.Text
    Me.MSFlexGrid1.Col = 5
    frmDmd.Version = Me.MSFlexGrid1.Text
    frmDmd.BrowseLig = Me.MSFlexGrid1.Row
    Load frmDmd
    frmDmd.LoadDmd
    frmDmd.Show vbModal
End If

End Sub


Private Sub MSFlexGrid1_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)

If X < Me.MSFlexGrid1.ColWidth(0) Then
    Me.MSFlexGrid1.MousePointer = flexCustom
    Set Me.MSFlexGrid1.MouseIcon = Me.lblMouse.MouseIcon
Else
    Me.MSFlexGrid1.MousePointer = flexDefault
End If

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

Select Case Button.Key
    Case "FIND"
        frmSelDmd.Show vbModal

End Select

End Sub




