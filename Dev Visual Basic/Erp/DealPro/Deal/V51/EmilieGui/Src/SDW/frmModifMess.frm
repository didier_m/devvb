VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmModifMessage 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Modification d'un message"
   ClientHeight    =   3525
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8520
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3525
   ScaleWidth      =   8520
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox Text3 
      Enabled         =   0   'False
      Height          =   285
      Left            =   240
      MaxLength       =   250
      TabIndex        =   6
      Top             =   1440
      Width           =   7335
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1800
      ScaleHeight     =   345
      ScaleWidth      =   5265
      TabIndex        =   4
      Top             =   2760
      Visible         =   0   'False
      Width           =   5295
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "En attente de la r�ponse du serveur ....................................................."
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   60
         Width           =   5055
      End
   End
   Begin VB.TextBox Text2 
      Height          =   285
      Left            =   240
      MaxLength       =   250
      TabIndex        =   2
      Top             =   2280
      Width           =   7335
   End
   Begin VB.TextBox Text1 
      Enabled         =   0   'False
      Height          =   285
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   2655
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7680
      Top             =   5520
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmModifMess.frx":0000
            Key             =   ""
            Object.Tag             =   "Fran�ais"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmModifMess.frx":0452
            Key             =   ""
            Object.Tag             =   "Anglais"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmModifMess.frx":08A4
            Key             =   ""
            Object.Tag             =   "Allemand"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmModifMess.frx":0CF6
            Key             =   ""
            Object.Tag             =   "Portugais"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmModifMess.frx":1010
            Key             =   ""
            Object.Tag             =   "Espagnol"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmModifMess.frx":1462
            Key             =   ""
            Object.Tag             =   "Italien"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "Ancien libell�"
      Enabled         =   0   'False
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   1080
      Width           =   2775
   End
   Begin VB.Label Label5 
      Caption         =   "Libell� du message : "
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   1920
      Width           =   2775
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   7800
      Picture         =   "frmModifMess.frx":18B4
      ToolTipText     =   "Cliquer ici pour modifier le message"
      Top             =   2160
      Width           =   480
   End
   Begin VB.Label Label4 
      Caption         =   "N� du message � modifier : "
      Enabled         =   0   'False
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   2535
   End
   Begin VB.Shape Shape1 
      Height          =   3435
      Left            =   60
      Top             =   60
      Width           =   8415
   End
End
Attribute VB_Name = "frmModifMessage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public CodeLangue As Integer
Public Ident As String
Public Version As String
Public Progiciel As String
Public Param As String
Public FlexRow As Integer



Private Sub Image1_Click()

'---> Cette proc�dure met � jour un message particulier

Dim FindApp As Boolean
Dim i As Integer
Dim CodeApp As String
Dim Res As Long
Dim lpBuffer As String
Dim IniFile As String
Dim hdlFile As Integer
Dim DmdFileName As String
Dim TempoFileName As String
Dim RspFileName As String
Dim Ligne As String

'-> V�rifier que le nouveau message est saisi
If Trim(Me.Text2.Text) = "" Then
    MsgBox "Veuillez saisir le message", vbCritical + vbOKOnly, "Erreur"
    Me.Text2.SetFocus
    Exit Sub
End If

'-> Bloquer l'interface
MdiMain.Enabled = False
Me.MousePointer = 11

'-> Afficher le message d'attente
Me.Picture2.Visible = True

'-> Cr�er un fichier temporaire
DmdFileName = App.Path & "\messages\trans\" & ComputerName & "_" & Me.hwnd & ".dmd"
TempoFileName = App.Path & "\temp\" & ComputerName & "_" & Me.hwnd & ".tmp"
RspFileName = App.Path & "\messages\trans\" & ComputerName & "_" & Me.hwnd & ".rsp"

'-> V�rifier qu'il n'y a pas une demande ou une r�ponse qui traine
If Dir$(DmdFileName, vbNormal) <> "" Then Kill DmdFileName
If Dir$(RspFileName, vbNormal) <> "" Then Kill RspFileName

'-> Cr�ation d'un fichier tempo
hdlFile = FreeFile
Open TempoFileName For Output As #hdlFile

'-> Cr�ation de la demande
Print #hdlFile, "MESS_UPDATE~" & Me.CodeLangue & "~" & Me.Ident & "~" & Me.Version & "~" & Me.Progiciel & "~" & Trim(Me.Text1.Text) & "|" & Trim(Me.Text2.Text)
Close #hdlFile

'-> Faire un rename dans le r�pertoire de transfert
Name TempoFileName As DmdFileName

'-> Se mettre en attente de la r�ponse
Do
    '-> Rendre la main � la CPU
    DoEvents
    '-> V�rifier si on trouve le fichier r�ponse
    If Dir$(RspFileName, vbNormal) <> "" Then Exit Do
Loop

'-> Lire la r�ponse
hdlFile = FreeFile
Open RspFileName For Input As #hdlFile
Line Input #hdlFile, Ligne
Close #hdlFile

'-> Supprimer le fichier RSP
Kill RspFileName

'-> Afficher le r�sultat
If Entry(2, Ligne, "~") = "OK" Then
    MsgBox "Message modifi�", vbInformation + vbOKOnly, "R�ponse"
    frmFindMessage.MSFlexGrid1.Text = Me.Text2.Text
Else
    MsgBox "Erreur lors de la modification du message : " & Chr(13) & Entry(3, Ligne, "~"), vbCritical + vbOKOnly, "Erreur"
End If

'-> Masquer le message d'attente
Me.Picture2.Visible = False

'-> D�bloquer l'interface
MdiMain.Enabled = True
Me.MousePointer = 0

'-> D�charger la feuille
Unload Me

End Sub
