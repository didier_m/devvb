VERSION 5.00
Begin VB.Form frmPass 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Identification"
   ClientHeight    =   1725
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3990
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1725
   ScaleWidth      =   3990
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "Ok"
      Height          =   375
      Left            =   3240
      TabIndex        =   2
      Top             =   1200
      Width           =   615
   End
   Begin VB.TextBox Text2 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   840
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   720
      Width           =   3015
   End
   Begin VB.TextBox Text1 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   840
      TabIndex        =   0
      Top             =   240
      Width           =   3015
   End
   Begin VB.Image Image2 
      Height          =   480
      Left            =   120
      Picture         =   "frmPass.frx":0000
      Top             =   720
      Width           =   480
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   120
      Picture         =   "frmPass.frx":030A
      Top             =   120
      Width           =   480
   End
End
Attribute VB_Name = "frmPass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-> Indique si l'utilisateur � correctement franchi le controle
Private Ctrl_Ok As Boolean

Private Sub Command1_Click()

Dim lpBuffer As String
Dim Res As Long
Dim FileOperat As String


'-> Messages de v�rification
If Trim(Me.Text1.Text) = "" Then
    MsgBox "Veuillez saisir le code op�rateur", vbExclamation + vbOKOnly, "Erreur"
    Me.Text1.SetFocus
    Exit Sub
End If

If Trim(Me.Text2.Text) = "" Then
    MsgBox "Veuillez saisir le mot de passe", vbExclamation + vbOKOnly, "Erreur"
    Me.Text2.SetFocus
    Exit Sub
End If

'-> Construire le nom du fichier � analyser
FileOperat = App.Path & "\param\" & Trim(Me.Text1.Text) & ".ini"

'-> V�rifier que l'utilisateur existe
If Dir$(FileOperat, vbNormal) = "" Then
    MsgBox "Op�rateur inconnu", vbCritical + vbOKOnly, "Erreur"
    Me.Text1.Text = ""
    Me.Text1.SetFocus
    Exit Sub
End If

'-> Lire le mot de passe dans son fichier
lpBuffer = Space$(200)
Res = GetPrivateProfileString("SDW", "PWD", "", lpBuffer, Len(lpBuffer), FileOperat)
If Res = 0 Then
    MsgBox "Probl�me de configuration pour cet utilisateur", vbExclamation + vbOKOnly, "Erreur"
    Me.Text1.SetFocus
    Exit Sub
End If

'-> V�rifier le mot de passe
If Mid$(lpBuffer, 1, Res) <> Me.Text2.Text Then
    MsgBox "Mot de passe incorrect", vbCritical + vbOKOnly, "Erreur"
    Me.Text2.Text = ""
    Me.Text2.SetFocus
    Exit Sub
End If

'-> Tout est Ok : faire un setting des variables g�n�rales
UserName = Trim(Me.Text1.Text)
UserPwd = Me.Text2.Text

'-> Liste des app autoris�es
lpBuffer = Space$(200)
Res = GetPrivateProfileString("AUTO_APP", "APPS", "", lpBuffer, Len(lpBuffer), FileOperat)
If Res = 0 Then
    MsgBox "Probl�me de configuration pour cet utilisateur : il n'y a pas d'applications autoris�es.", vbExclamation + vbOKOnly, "Erreur"
    Me.Text1.SetFocus
    Exit Sub
End If

UserApp = Mid$(lpBuffer, 1, Res)

'-> autorisation de cr�ation des messages
lpBuffer = Space$(200)
Res = GetPrivateProfileString("SDW", "MESSAGES", "", lpBuffer, Len(lpBuffer), FileOperat)
If Res = 0 Then
    CreateMessage = False
Else
    If CInt(Mid$(lpBuffer, 1, Res)) <> 0 Then
        CreateMessage = True
    Else
        CreateMessage = False
    End If
End If

lpBuffer = Space$(200)
Res = GetPrivateProfileString("SDW", "DMD", "", lpBuffer, Len(lpBuffer), FileOperat)
If Res = 0 Then
    UserValidDmd = False
Else
    lpBuffer = Mid$(lpBuffer, 1, Res)
    If lpBuffer = "1" Then UserValidDmd = True
End If


'-> Indiquer que le ctrl est Ok
Ctrl_Ok = True

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Form_Unload(Cancel As Integer)

'-> Quitter si examne de passage non Ok
If Not Ctrl_Ok Then End

End Sub

Private Sub Text1_GotFocus()

Me.Text1.SelStart = 0
Me.Text1.SelLength = Len(Me.Text1.Text)

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Me.Text2.SetFocus

End Sub

Private Sub Text2_GotFocus()

Me.Text2.SelStart = 0
Me.Text2.SelLength = Len(Me.Text2.Text)

End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Command1_Click

End Sub
