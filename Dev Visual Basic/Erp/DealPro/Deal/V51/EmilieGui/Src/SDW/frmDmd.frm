VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDmd 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Demande de d�veloppement WEB"
   ClientHeight    =   8475
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7815
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8475
   ScaleWidth      =   7815
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox Combo5 
      Height          =   315
      ItemData        =   "frmDmd.frx":0000
      Left            =   2160
      List            =   "frmDmd.frx":0002
      Sorted          =   -1  'True
      TabIndex        =   5
      Top             =   1920
      Width           =   5535
   End
   Begin VB.ListBox List1 
      Height          =   1230
      Left            =   2160
      TabIndex        =   9
      Top             =   5400
      Width           =   5535
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   600
      Top             =   3840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDmd.frx":0004
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDmd.frx":031E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      HotImageList    =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "WEB"
            Object.ToolTipText     =   "Envpoyer un message dans OutLook"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "WORD"
            Object.ToolTipText     =   "Ouvrir Word"
            ImageIndex      =   2
         EndProperty
      EndProperty
   End
   Begin VB.ComboBox Combo4 
      Height          =   315
      ItemData        =   "frmDmd.frx":0638
      Left            =   2160
      List            =   "frmDmd.frx":063A
      TabIndex        =   2
      Top             =   1200
      Width           =   5535
   End
   Begin VB.ComboBox Combo3 
      Height          =   315
      ItemData        =   "frmDmd.frx":063C
      Left            =   5040
      List            =   "frmDmd.frx":063E
      TabIndex        =   7
      Top             =   2280
      Width           =   2655
   End
   Begin VB.ComboBox Combo2 
      Height          =   315
      ItemData        =   "frmDmd.frx":0640
      Left            =   2160
      List            =   "frmDmd.frx":0642
      TabIndex        =   3
      Top             =   1560
      Width           =   2655
   End
   Begin VB.TextBox Text4 
      Enabled         =   0   'False
      Height          =   285
      Left            =   5040
      TabIndex        =   4
      Top             =   1560
      Width           =   2655
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   2160
      TabIndex        =   6
      Top             =   2280
      Width           =   2655
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Envoyer la demande"
      Height          =   375
      Left            =   6000
      TabIndex        =   11
      Top             =   8040
      Width           =   1695
   End
   Begin VB.TextBox Text7 
      Height          =   1215
      Left            =   2160
      MultiLine       =   -1  'True
      TabIndex        =   10
      Top             =   6720
      Width           =   5535
   End
   Begin VB.TextBox Text5 
      Height          =   2655
      Left            =   2160
      MultiLine       =   -1  'True
      TabIndex        =   8
      Top             =   2640
      Width           =   5535
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   2160
      TabIndex        =   1
      Top             =   840
      Width           =   5535
   End
   Begin VB.Label label8 
      Caption         =   "Progiciel / version : "
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   2280
      Width           =   1575
   End
   Begin VB.Label Label7 
      Caption         =   "D�lai : "
      Height          =   255
      Left            =   120
      TabIndex        =   18
      Top             =   6840
      Width           =   855
   End
   Begin VB.Label Label6 
      Caption         =   "Fichiers joints :"
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   5400
      Width           =   1695
   End
   Begin VB.Label Label5 
      Caption         =   "Descriptif : "
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   2640
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "Nature : "
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   1560
      Width           =   1335
   End
   Begin VB.Label Label3 
      Caption         =   "Client : "
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   1920
      Width           =   1335
   End
   Begin VB.Label Label2 
      Caption         =   "Service : "
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   1200
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   "Nom du demandeur :"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   2175
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "Menu"
      Visible         =   0   'False
      Begin VB.Menu mnuAddFile 
         Caption         =   "Ajouter un fichier"
      End
      Begin VB.Menu mnuDelFile 
         Caption         =   "Supprimer un fichier"
      End
   End
End
Attribute VB_Name = "frmDmd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private ListeServices As String
Private ListeNatures As String
Private ListeProgiciels As String

Private Sub Combo2_Click()

If Me.Combo2.ListIndex = 0 Or Me.Combo2.ListIndex = 3 Then
    Me.Text4.Enabled = True
Else
    Me.Text4.Enabled = False
    Me.Text4.Text = ""
End If

End Sub

Private Sub Command1_Click()

'---> Cette proc�dure envoie la demande au service de recherche et de devellopement service Internet

Dim hdlFile As Integer
Dim FileName As String
Dim FileJoint As String
Dim ListFileJoint As String
Dim aTx As Control
Dim i As Integer

On Error GoTo GestError

'-> V�rification des zones saisies
If Trim(Me.Text1.Text) = "" Then
    MsgBox "Veuillez saisir votre nom.", vbCritical + vbOKOnly, "Erreur"
    Me.Text1.SetFocus
    Exit Sub
End If

'-> V�rifier le service
If Me.Combo4.ListIndex = -1 Then
    MsgBox "Veuillez s�lectionner votre service.", vbCritical + vbOKOnly, "Erreur"
    Me.Combo4.SetFocus
    Exit Sub
End If

'-> Controler la nature
If Me.Combo2.ListIndex = -1 Then
    MsgBox "Veuillez saisir la nature de la demande", vbCritical + vbOKOnly, "Erreur"
    Me.Combo2.SetFocus
    Exit Sub
Else
    '-> V�rifier que le num�ro de l'appel est renseign�
    If Me.Combo2.ListIndex = 0 Or Me.Combo2.ListIndex = 3 Then
        If Me.Text4.Text = "" Then
            MsgBox "Veuillez saisir la r�f�rence de la demande", vbCritical + vbOKOnly, "Erreur"
            Me.Text4.SetFocus
            Exit Sub
        End If
    End If
End If
    
'-> Veuillez saisir le client
If Me.Combo5.ListIndex = -1 Then
    MsgBox "Veuillez s�lectionner le client .", vbCritical + vbOKOnly, "Erreur"
    Me.Combo5.SetFocus
    Exit Sub
End If

'-> Controle sur le progiciel
If Me.Combo1.ListIndex = -1 Then
    MsgBox "Veuillez s�lectionner un applicatif", vbCritical + vbOKOnly, "Erreur"
    Me.Combo1.SetFocus
    Exit Sub
End If

'-> V�rification du num�ro de version
If Me.Combo3.ListIndex = -1 Then
    MsgBox "Veuillez saisir le num�ro de la version", vbCritical + vbOKOnly, "Erreur"
    Me.Combo3.SetFocus
    Exit Sub
End If

'-> Controle sur le descriptif
If Trim(Me.Text5.Text) = "" Then
    MsgBox "Veuillez saisir le descriptif de la demande.", vbCritical + vbOKOnly, "Erreur"
    Me.Text5.SetFocus
    Exit Sub
End If


'-> V�rifier que les fichiers sp�cifi�s existents
For i = 0 To Me.List1.ListCount - 1
    If Dir$(Me.List1.List(i), vbNormal) = "" Then
        MsgBox "Impossible de trouver le fichier sp�cifi� : " & Me.List1.List(i), vbCritical + vbOKOnly, "Erreur"
        Me.List1.SetFocus
        Exit Sub
    End If
Next

'-> Bloquer l'�cran
Me.Enabled = False
Me.MousePointer = 11

'-> Cr�er le nom de la demande
FileName = Format(Year(Now), "0000")
FileName = FileName & Format(Month(Now), "00")
FileName = FileName & Format(Day(Now), "00")
FileName = FileName & Format(Hour(Now), "00")
FileName = FileName & Format(Minute(Now), "00")
FileName = FileName & Format(Second(Now), "00")


If Me.List1.ListCount <> 0 Then
    '-> Cr�er un r�pertoire de ce nom si necessaire
    MkDir App.Path & "\demandes\" & Me.Combo3.Text & "\dmd\" & FileName
    '-> Copier les fichiers sp�cifi�s dedans
    For i = 0 To Me.List1.ListCount - 1
        '-> R�cup�rer le nom du fichier
        FileJoint = Entry(NumEntries(Me.List1.List(i), "\"), Me.List1.List(i), "\")
        '-> Faire une copie du fichier
        FileCopy Me.List1.List(i), App.Path & "\demandes\" & Me.Combo3.Text & "\dmd\" & FileName & "\" & FileJoint
        '-> Ajouter dans la liste des fichiers joints
        If ListFileJoint = "" Then
            ListFileJoint = FileJoint
        Else
            ListFileJoint = ListFileJoint & "|" & FileJoint
        End If
    Next 'Pout tous les ficheirs joints
End If

'-> Cr�er le nom du fichier
FileName = App.Path & "\demandes\" & Me.Combo3.Text & "\dmd\" & FileName & ".dmd"

'-> Cr�er le fichier de demande
hdlFile = FreeFile
Open FileName For Output As #hdlFile

'-> Imprimer l'entete de la demande
Print #hdlFile, "[DMD]"
Print #hdlFile, "Name=" & Trim(Me.Text1.Text)
'-> Recherche du service
Print #hdlFile, "Service=" & Entry(1, Entry(Me.Combo4.ListIndex + 1, ListeServices, "|"), "�")
Print #hdlFile, "Nature=" & Entry(1, Entry(Me.Combo2.ListIndex + 1, ListeNatures, "|"), "�")
Print #hdlFile, "N�=" & Trim(Me.Text4.Text)
Print #hdlFile, "Client=" & Trim(Entry(1, Me.Combo5.Text, "-"))
Print #hdlFile, "Progiciel=" & Entry(1, Entry(Me.Combo1.ListIndex + 1, ListeProgiciels, "|"), "�")
Print #hdlFile, "Version=" & Trim(Me.Combo3.Text)
Print #hdlFile, "Files=" & ListFileJoint
Print #hdlFile, "Valid=0"
Print #hdlFile, "ValidOperat="
Print #hdlFile, "ValidDate="
Print #hdlFile, "ValidCom="

'-> Imprimer le descriptif
Print #hdlFile, "[DESCRIPTIF]"
Print #hdlFile, "DESCRO=" & Trim(Me.Text5.Text)
Print #hdlFile, "[DELAI]"
Print #hdlFile, "DELAI=" & Trim(Me.Text7.Text)

'-> Fermer le fichier
Close #hdlFile

'-> Vider toutes les zones
For Each aTx In Me.Controls
    If TypeOf aTx Is TextBox Then aTx.Text = ""
Next

'-> D�selectionner le progiciel
Me.Combo1.ListIndex = -1
Me.Combo2.ListIndex = -1
Me.Combo3.ListIndex = -1
Me.Combo4.ListIndex = -1
Me.Combo5.ListIndex = -1

'-> Supprimer la liste des fichiers joints
For i = 0 To Me.List1.ListCount - 1
    Me.List1.RemoveItem (0)
Next


'-> Message de succ�s
MsgBox "Demande transf�r�e", vbInformation + vbOKOnly, "Message"

'-> Sortir du prog
GoTo EndProg

GestError:
    
    '-> Fermer tous les fichiers ouverts
    Reset
    '-> Afficher un message d'erreur
    MsgBox "Erreur dans la cr�ation de la demande : demande supprim�e", vbCritical + vbOKOnly, "Erreur"
    '-> Supprimer le fichier demande s'il existe
    If Dir$(FileName, vbNormal) <> "" Then Kill FileName
    
EndProg:

    '-> D�bloquer l'�cran
    Me.Enabled = True
    Me.MousePointer = 0
    
    '-> Rendre le focus sur la premiere zone
    Me.Text1.SetFocus



End Sub

Private Sub Form_Load()

Dim Res As Long
Dim lpBuffer As String
Dim lpBuffer2 As String
Dim i As Integer

'-> Charger la liste des services
lpBuffer = Space$(5000)
Res = GetPrivateProfileString("DMD", "SERVICES", "", lpBuffer, Len(lpBuffer), App.Path & "\param\sdw.ini")
If Res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Res)
    '-> Mettre � jour la variable
    ListeServices = lpBuffer
    For i = 1 To NumEntries(lpBuffer, "|")
        '-> Ajouter dans la liste
        Me.Combo4.AddItem Entry(2, Entry(i, lpBuffer, "|"), "�")
    Next 'Pour tous les services
Else
    '-> Afficher un message d'erreur
    MsgBox "Impossible de charger la liste des services", vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

'-> Charger la liste des Nature de demande
lpBuffer = Space$(5000)
Res = GetPrivateProfileString("DMD", "NATURES", "", lpBuffer, Len(lpBuffer), App.Path & "\param\sdw.ini")
If Res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Res)
    '-> Mettre � jour la variable
    ListeNatures = lpBuffer
    For i = 1 To NumEntries(lpBuffer, "|")
        '-> Ajouter dans la liste
        Me.Combo2.AddItem Entry(2, Entry(i, lpBuffer, "|"), "�")
    Next 'Pour toutes les natures
Else
    '-> Afficher un message d'erreur
    MsgBox "Impossible de charger la liste des natures", vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

'-> Charger la liste des clients
lpBuffer = Space$(5000)
Res = GetPrivateProfileString("SDW", "LISTE_CLIENTS", "", lpBuffer, Len(lpBuffer), App.Path & "\param\sdw.ini")
If Res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Res)
    For i = 1 To NumEntries(lpBuffer, "|")
        '-> Recherche le libell�
        lpBuffer2 = Space$(5000)
        Res = GetPrivateProfileString("SDW", "LIB", "", lpBuffer2, Len(lpBuffer2), App.Path & "\param\" & Entry(2, Entry(i, lpBuffer, "|"), "�") & ".ini")
        If Res <> 0 Then
            lpBuffer2 = Mid$(lpBuffer2, 1, Res)
        Else
            lpBuffer2 = ""
        End If
        '-> Ajouter dans la liste
        Me.Combo5.AddItem Entry(2, Entry(i, lpBuffer, "|"), "�") & " - " & lpBuffer2
    Next 'Pour tous les services
Else
    '-> Afficher un message d'erreur
    MsgBox "Impossible de charger la liste des clients", vbCritical + vbOKOnly, "Erreur fatale"
    End
End If



'-> Charger la liste des progiciels
lpBuffer = Space$(5000)
Res = GetPrivateProfileString("SDW", "SDW_APPS", "", lpBuffer, Len(lpBuffer), App.Path & "\param\sdw.ini")
If Res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Res)
    '-> Mettre � jour la variable
    ListeProgiciels = lpBuffer
    For i = 1 To NumEntries(lpBuffer, "|")
        '-> Ajouter dans la liste
        Me.Combo1.AddItem Entry(2, Entry(i, lpBuffer, "|"), "�")
    Next 'Pour tous les services
Else
    '-> Afficher un message d'erreur
    MsgBox "Impossible de charger la liste des applications", vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

'-> Charger la liste des versions
lpBuffer = Space$(5000)
Res = GetPrivateProfileString("SDW", "SDW_VERSIONS", "", lpBuffer, Len(lpBuffer), App.Path & "\param\sdw.ini")
If Res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Res)
    For i = 1 To NumEntries(lpBuffer, "|")
        '-> Ajouter dans la liste
        Me.Combo3.AddItem Entry(2, Entry(i, lpBuffer, "|"), "�")
    Next 'Pour tous les services
Else
    '-> Afficher un message d'erreur
    MsgBox "Impossible de charger la liste des applications", vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

End Sub

Private Sub List1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

'---> Afficher le menu contextuel

If Me.List1.ListIndex = -1 Then
    Me.mnuDelFile.Enabled = False
Else
    Me.mnuDelFile.Enabled = True
End If

'-> Afficher le menu
If Button = vbRightButton Then Me.PopupMenu Me.mnuMenu


End Sub

Private Sub mnuAddFile_Click()

'---> Afficher le menu parcourir

Dim Rep As String
Dim i As Integer

'-> Afficher la fen�tre de parcourir
Rep = GetPath(Me.hwnd, True)
If Rep <> "" Then
    '-> V�rifier que le fichier n'existe pas d�ja
    For i = 0 To Me.List1.ListCount - 1
        If Me.List1.List(i) = Rep Then
            MsgBox "Ce fichier est d�ja ajout� dans la liste des fichiers joints.", vbExclamation + vbOKOnly, "Erreur"
            Exit Sub
        End If
    Next
    
    '-> Ajouter le fichier dans la liste
    Me.List1.AddItem Rep

    '-> Le s�lectionner
    Me.List1.Selected(Me.List1.ListCount - 1) = True

End If

End Sub

Private Sub mnuDelFile_Click()

'---> Supprimer un message de la liste des fichiers joints

Dim Rep As VbMsgBoxResult

'-> Demander confirmation
Rep = MsgBox("Supprimer le fichier : " & Me.List1.Text & " de la liste des fichiers joints ?", vbQuestion + vbYesNo, "Confirmation")
If Rep = vbNo Then Exit Sub

'-> supprimer  de la liste des fichiers joints
Me.List1.RemoveItem (Me.List1.ListIndex)

'-> S�lectionner un autre fichier par d�faut
If Me.List1.ListCount <> 0 Then Me.List1.Selected(0) = True


End Sub

Private Sub Text5_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then KeyAscii = 0

End Sub

Private Sub Text7_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then KeyAscii = 0

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

Dim AppWord As Object
Dim aOutLook As Object
Dim aMail As Object

On Error GoTo GestError

Select Case Button.Key

    Case "WEB"
        Set aOutLook = CreateObject("Outlook.Application")
        '-> Cr�er un nouveau message
        Set aMail = aOutLook.CreateItem(0)
        '-> Afficher le mail
        aMail.Display
        Set aMail = Nothing
        Set aOutLook = Nothing

    
    Case "WORD"
        '-> Cr�er un pointeur
        Set AppWord = CreateObject("Word.Application")
        AppWord.Visible = True
        Set AppWord = Nothing

Exit Sub


GestError:
    MsgBox "Impossible de cr�er le composant souhait�", vbCritical + vbOKOnly, "Erreur"
    Set AppWord = Nothing
    Set aMail = Nothing
    Set aOutLook = Nothing

End Select

End Sub
