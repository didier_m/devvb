VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMess 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Gestionnaire de messages"
   ClientHeight    =   600
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   2370
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   600
   ScaleWidth      =   2370
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4680
      Top             =   480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMessage.frx":0000
            Key             =   "NEWFILE"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMessage.frx":0452
            Key             =   "NEWMESS"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMessage.frx":08A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMessage.frx":0CF6
            Key             =   "FILETRANSLATE"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMessage.frx":1010
            Key             =   "FINDMESS"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMessage.frx":132A
            Key             =   "STOPSERVER"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMessage.frx":1C04
            Key             =   "GOSERVER"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   600
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2370
      _ExtentX        =   4180
      _ExtentY        =   1058
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "NEWFILE"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "NEWMESS"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FINDMESS"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "TRANSLATE"
            ImageIndex      =   4
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmMess"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-> Chaine de param�trage
Public ListeVersion As String
Public ListeClient As String
Public LibClient As String
Public ListeApp As String
Public LibApp As String

'-> Indique si la fen�tre de cr�ation de nouveau fichier est visible
Public IsFrmNewFile As Boolean
'-> Indique si la fen�tre de cr�ation de nouveau message est visible
Public IsFrmNewMess As Boolean
'-> Indique si la fen�tre de cr�ation de modif de message est visible
Public IsFrmModifMess As Boolean
'-> Indique si la fen�tre de recherche de message est visible
Public IsFrmFindMess As Boolean
'-> Indique si la fen�tre de traduction  est visible
Public IsFrmTraduct As Boolean
Private Function IsServeurActif() As Boolean

'-> il faut v�rifier dans un premier temps que le serveur de messages existe bien
'If Dir$(App.Path & "\messages\batch.lck", vbNormal) = "" Then
'    IsServeurActif = False
'Else
    IsServeurActif = True
'End If

End Function


Private Sub Form_Load()

'---> Faire ici le chargement des clients , des versions , des applicatifs , des codes langues

Dim lpBuffer As String
Dim Res As Long
Dim IniFile As String
Dim i As Integer

'-> Setting de la variable de lecture du fichier ini
IniFile = App.Path & "\param\sdw.ini"

'-> Charger la liste des clients
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("SDW", "Liste_Clients", "", lpBuffer, Len(lpBuffer), IniFile)
ListeClient = Mid$(lpBuffer, 1, Res)

'-> Charger la liste des versions
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("SDW", "Sdw_Versions", "", lpBuffer, Len(lpBuffer), IniFile)
ListeVersion = Mid$(lpBuffer, 1, Res)

'-> Analyser le fichier Deal.ini pour la liste des applicatifs
IniFile = App.Path & "\param\deal.ini"

'-> Charger la liste des applicatifs
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("SDW", "Message_Apps", "", lpBuffer, Len(lpBuffer), IniFile)
ListeApp = Mid$(lpBuffer, 1, Res)

'-> Charger le libell� de chaque application
For i = 1 To NumEntries(ListeApp, "|")
    '-> Recherche le libell� dans le fichier ini
    lpBuffer = Space$(2000)
    Res = GetPrivateProfileString(Entry(i, ListeApp, "|"), "APP", "", lpBuffer, Len(lpBuffer), IniFile)
    '-> Concatainer la chaine r�sultat
    If LibApp = "" Then
        LibApp = Mid$(lpBuffer, 1, Res)
    Else
        LibApp = LibApp & "|" & Mid$(lpBuffer, 1, Res)
    End If
Next 'Pour tous les applicatifs

'-> charger les libell�s clients
For i = 1 To NumEntries(ListeClient, "|")
    '-> Cr�er le nom du fichier � ouvrir
    IniFile = App.Path & "\param\" & Entry(i, ListeClient, "|") & ".ini"
    '-> Charger le libell�
    lpBuffer = Space$(2000)
    Res = GetPrivateProfileString("SDW", "LIB", "", lpBuffer, Len(lpBuffer), IniFile)
    '-> concatainer dans la variable
    If LibClient = "" Then
        LibClient = Mid$(lpBuffer, 1, Res)
    Else
        LibClient = LibClient & "|" & Mid$(lpBuffer, 1, Res)
    End If
Next 'Pour tous les clients

'-> Bloquer les boutons si le serveur n'est pas lanc�
If Not IsServeurActif Then
    For i = 1 To Me.Toolbar1.Buttons.Count
        Me.Toolbar1.Buttons(i).Enabled = False
    Next
Else
    '-> Autorisation sur la cr�ation et modification des messages
    Me.Toolbar1.Buttons(2).Enabled = CreateMessage
    Me.Toolbar1.Buttons(3).Enabled = CreateMessage
End If

End Sub

Private Sub Form_Unload(Cancel As Integer)

'-> Fermer les diff�rentes feuilles
If IsFrmNewMess Then Unload frmNewMessage
If IsFrmFindMess Then Unload frmFindMessage
If IsFrmModifMess Then Unload frmModifMessage

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

Select Case Button.Key

    Case "NEWFILE"
        
    Case "NEWMESS" '-> Cr�ation d'un nouveau message
        frmNewMessage.Show
        frmNewMessage.ZOrder
        
    Case "UPDATEMESS" '-> Modification d'un message
        frmModifMessage.Show
        frmModifMessage.ZOrder
    
    Case "FINDMESS" '-> Recherche d'un message
        frmFindMessage.Show
        frmFindMessage.ZOrder
    
    Case "TRANSLATE"
    




End Select 'Selon la cl� du boutton




End Sub
