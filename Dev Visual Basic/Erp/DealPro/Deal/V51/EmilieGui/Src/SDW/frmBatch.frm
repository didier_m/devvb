VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Batch d'analyse des messages "
   ClientHeight    =   2700
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9060
   LinkTopic       =   "Form1"
   ScaleHeight     =   2700
   ScaleWidth      =   9060
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text2 
      Enabled         =   0   'False
      Height          =   285
      Left            =   120
      TabIndex        =   4
      Top             =   1200
      Width           =   8775
   End
   Begin VB.Timer Timer1 
      Left            =   360
      Top             =   1800
   End
   Begin VB.CommandButton Stop 
      Caption         =   "Arreter le serveur de messages"
      Height          =   375
      Left            =   6120
      TabIndex        =   3
      Top             =   2160
      Width           =   2775
   End
   Begin VB.CommandButton Go 
      Caption         =   "Lancer le serveur de messages"
      Height          =   375
      Left            =   6120
      TabIndex        =   2
      Top             =   1680
      Width           =   2775
   End
   Begin VB.TextBox Text1 
      Enabled         =   0   'False
      Height          =   285
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   8775
   End
   Begin VB.Label Label2 
      Caption         =   "Fichiers ressources :"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   840
      Width           =   2895
   End
   Begin VB.Label Label1 
      Caption         =   "R�pertoire d'analyse des demandes : "
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2895
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-> Indique si le serveur est lanc�
Private IsRunning As Boolean
'-> Interval d'analyse du r�pertoire en millis
Private Delai As Long
'-> Indique une demande d'arret du serveur des messages
Private AscForEnd As Boolean
'-> Indique le r�pertoire d'analyse des fichiers de demande
Private Trans_Directory As String
'-> Indique le r�pertoire des fichiers de ressources
Private Mess_Directory As String
'-> Indique l'emplacement du fichier de ressources � alimenter
Private Mess_File As String
'-> Indique la liste des versions en cours de d�veloppement
Private Liste_Version As String
'-> Indique la liste des clients g�r�es par cette applications
Private Liste_Client As String
'-> Variable pour cr�ation des messages
Private Type MessageType
    Libel As String * 259
End Type
'-> Variable qui indique le fichier ini � lire
Private IniFile As String
'-> Collection des clients de la soci�t�
Private Versions As Collection


Private Sub Form_Load()

'---> charger le r�pertoire d'analyse � l'ouverture de la feuille

Dim Res As Long
Dim lpBuffer As String
Dim i As Integer

'-> Initialiser la collection des versions
Set Versions = New Collection

'----------------------------------------------------
'| Initialisation des param�tres du programme batch |
'----------------------------------------------------

'-> Cr�er le nom du fichier ascii � analyser
IniFile = App.Path & "\param\sdw.ini"


'-> Charger la liste des versions en cours de d�veloppement
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("SDW", "Sdw_Versions", "", lpBuffer, Len(lpBuffer), IniFile)
If Res = 0 Then
    MsgBox "Impossible de lire le fichier de ressource", vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

'-> Setting de la variable
Liste_Version = Mid$(lpBuffer, 1, Res)

'-> Charger la liste des clients en cours de d�veloppement
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("SDW", "Sdw_Clients", "", lpBuffer, Len(lpBuffer), IniFile)
If Res = 0 Then
    MsgBox "Impossible de lire le fichier de ressource", vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

'-> Setting de la variable
Liste_Client = Mid$(lpBuffer, 1, Res)

'-> Charger le nom du r�pertoire
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("SDW", "Message_Transaction", "", lpBuffer, Len(lpBuffer), IniFile)
If Res = 0 Then
    MsgBox "Impossible de lire le fichier de ressource", vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

'-> Setting de la variable
Trans_Directory = Mid$(lpBuffer, 1, Res)

'-> Afficher
Me.Text1.Text = Trans_Directory

'-> Charger le path d'analyse des fichiers ressources
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("SDW", "Message_Files", "", lpBuffer, Len(lpBuffer), IniFile)
If Res = 0 Then
    MsgBox "Impossible de lire le fichier de ressource", vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

'-> Positionner la variable
Mess_File = Mid$(lpBuffer, 1, Res)

'-> Afficher
Me.Text2.Text = Mess_File

'-> Charger le d�lai de temporisation
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("SDW", "Message_Delai", "", lpBuffer, Len(lpBuffer), IniFile)
If Res = 0 Then
    Delai = 100
Else
    Delai = CLng(Mid$(lpBuffer, 1, Res))
    If Delai = 0 Then Delai = 100
End If

'-> Bloquer le bouton d'arreter l'analyse du r�pertoire
Me.Stop.Enabled = IsRunning

'-> Initialiser les fichiers Messprog pour les versions en cours
For i = 1 To NumEntries(Liste_Version, "|")
    LoadVersion Entry(i, Liste_Version, "|")
Next


End Sub

Private Sub LoadVersion(IdVersion As String)

'---> Cette fonction cr�er un nouveau client et charge le descro de toutes ces applicatifs _
et tous les messages en m�moire

Dim aVersion As Version
Dim aClient As Client
Dim Res As Long
Dim lpBuffer As String
Dim i As Integer, j As Integer, k As Integer
Dim CodeApp As String
Dim aApp As ApplicMess
Dim CodeClient As String
Dim aLangue As Langue

'-> Cr�er une nouvelle version
Set aVersion = New Version

'-> Setting des propri�t�s
aVersion.IdVersion = IdVersion

'-> Ajout dans la collection
Versions.Add aVersion, UCase$(IdVersion)


'-> Cr�er la liste de tous les clients
For j = 1 To NumEntries(Liste_Client, "|")
    '-> R�cup�ration du code du client
    CodeClient = Entry(j, Liste_Client, "|")
    
    '-> Cr�er un  nouveau client
    Set aClient = New Client
    
    '-> Initialiser les propri�t�s du client
    aClient.IdClient = CodeClient
    aClient.IniFile = App.Path & "\param\" & CodeClient & ".ini"
    aClient.MessageFile = Replace(Mess_File, "$IDENT$", CodeClient)
    aClient.MessageFile = Replace(aClient.MessageFile, "$VERSION$", IdVersion)
    
    '-> Ajouter dans la collection des clients
    aVersion.Clients.Add aClient, UCase$(CodeClient)
    
    '-> Initaliser la liste des applications autoris�es
    lpBuffer = Space$(200)
    Res = GetPrivateProfileString("SDW", "MESSAGE_APPS", "", lpBuffer, Len(lpBuffer), aClient.IniFile)
    aClient.ListeApp = Mid$(lpBuffer, 1, Res)
    
    '-> Pour chaque application , cr�er l'objet applicatif ainsi que les messages de cette application
    For i = 1 To NumEntries(aClient.ListeApp, "|")
        '-> R�cup�rer l'applic
        CodeApp = Entry(i, aClient.ListeApp, "|")
        '-> Cr�er une nouvelle applic
        Set aApp = New ApplicMess
        '-> Setting de ses propri�t�s
        aApp.Code = UCase$(CodeApp)
        '-> Lecture de sa plage de message
        lpBuffer = Space$(200)
        Res = GetPrivateProfileString(CodeApp, "RANGE", "", lpBuffer, Len(lpBuffer), aClient.IniFile)
        aApp.RangeMin = CLng(Entry(1, Mid$(lpBuffer, 1, Res), "|"))
        aApp.RangeMax = CLng(Entry(2, Mid$(lpBuffer, 1, Res), "|"))
        '-> Lecture de l'index du prochain num�ro
        lpBuffer = Space$(200)
        Res = GetPrivateProfileString(CodeApp, IdVersion, "", lpBuffer, Len(lpBuffer), aClient.IniFile)
        aApp.IdMessage = CLng(Mid$(lpBuffer, 1, Res))
        '-> Initialiser toutes les collections par langue
        For k = 1 To 10
            Set aLangue = New Langue
            aLangue.IdLangue = k
            aApp.Langues.Add aLangue, "LNG|" & k
        Next 'Pour tous les codes langues
        '-> Ajout dans la collection des applicatifs du client
        aClient.Apps.Add aApp, UCase$(CodeApp)
    Next 'Pour toutes les applications d'un client
    
    '-> Charger la liste des messages
    For i = 1 To 10
        '-> V�rifier si on trouve le fichier ressource pour la langue sp�cifi�e
        If Dir$(aClient.MessageFile & "\p-messages-" & Format(i, "00") & ".data", vbNormal) <> "" Then
            '-> Faire le setting de la pr�sence du code langue pour le client
            aClient.SetCodeLangue i, True
            '-> Charger la liste des messages pour ce client et pour ce code langue
            Load_Messprog aClient, i
        End If 'Si le fichier existe
    Next
Next 'Pour tous les clients

End Sub

Private Sub Load_Messprog(aClient As Client, IdLangue As Integer)

'---> Cette fonction charge les messages d'un client pour un code langue en particulier

Dim hdlFile As Integer
Dim aMes As MessageType
Dim Num_Enreg As Long
Dim FichierLng As String
Dim CodeApp As String
Dim aApp As ApplicMess
Dim aMess As Message
Dim aLangue As Langue

'-> Settign du nom du fichier
FichierLng = aClient.MessageFile & "\p-messages-" & Format(IdLangue, "00") & ".data"

'-> Calcul du nombre d'enregistrements du fichier
Num_Enreg = FileLen(FichierLng) / Len(aMes)

'-> Ouverture du fichier pour get de ses enreg
hdlFile = FreeFile
Open FichierLng For Random As #hdlFile Len = Len(aMes)

'-> Lecture des enreg
For i = 1 To Num_Enreg
    '-> Get du message
    Get #hdlFile, i, aMes
    '-> Get de son applicatif attach�
    CodeApp = GetAppByNum(aClient, Mid$(aMes.Libel, 1, 9))
    '-> Pointer sur son applicatif
    Set aApp = aClient.Apps(UCase$(CodeApp))
    '-> Pointer sur le fichier langue en cours d'analyse
    Set aLangue = aApp.Langues("LNG|" & IdLangue)
    '-> Ajouter le message
    Set aMess = New Message
    aMess.NumMess = CStr(CLng(Mid$(aMes.Libel, 1, 9)))
    aMess.Libel = Trim(Mid$(aMes.Libel, 10, 250))
    '-> Ajout dans la collection
    aLangue.Messages.Add aMess, "MESS|" & aMess.NumMess
Next 'Pour tous les enregistrements

End Sub

Private Function GetAppByNum(aClient As Client, NumMessage As String) As String

'---> Cette focntion retourne l'applicatif d'appartenance � une app en fonction de son num�ro

Dim aApp As ApplicMess
Dim aMes As Long

For Each aApp In aClient.Apps
    '-> Retraiter le message
    If UCase$(aClient.IdClient) <> "DEAL" Then
        If CLng(NumMessage) < 1000000 Then
            aMes = CLng(NumMessage) + 1000000
        Else
            aMes = CLng(NumMessage)
        End If
    Else
        aMes = CLng(NumMessage)
    End If
    
    
    '-> V�rifier la page des messages
    If aMes >= aApp.RangeMin And aMes <= aApp.RangeMax Then
        GetAppByNum = aApp.Code
        Exit Function
    End If
Next 'Pour tous les applicatifs

End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

DoEvents

If IsRunning Then
    MsgBox "Veuillez arreter le serveur", vbCritical + vbOKOnly, "erreur"
    Cancel = 1
    Exit Sub
End If

End Sub

Private Sub Go_Click()

'---> Cette proc�dure lance le timer d'analyse des fichiers de demande

Dim hdlFile As Integer

'-> Positionner la variable sur en cours
IsRunning = True
AscForEnd = False

'-> Bloquer le bouton
Me.Go.Enabled = Not IsRunning
Me.Stop.Enabled = IsRunning

'-> Cr�ation d'un fichier ASCII pour indiquer que le serveur est en train de tourner
hdlFile = FreeFile
Open App.Path & "\messages\batch.lck" For Output As #hdlFile
Close #hdlFile

'-> Lancer le timer
Me.Timer1.Interval = Delai

End Sub

Private Sub Stop_Click()

'---> Faire une demande d'arret du serveur
AscForEnd = True

End Sub

Private Sub Timer1_Timer()


'-> Pour ouverture du fichier demande
Dim hdlFile As Integer
Dim FileName As String

'-> Pour cr�ation fichier temporaire
Dim TempFile As String

'-> Pour lecture de la demande
Dim Ligne As String
Dim Instruction As String
Dim Langue As Integer
Dim Ident As String
Dim Version As String
Dim Progiciel As String
Dim Param_Mess As String

'-> Pour compteur
Dim i As Long

'-> Pour MESS_SELECT
Dim TypeFind As Integer
Dim MessFind As String
Dim strMessage As String

'-> Pour impl�mentation du mod�le objet
Dim aVersion As Version
Dim aClient As Client
Dim aApp As ApplicMess
Dim aLangue As Langue
Dim aMess As Message

'-> Nom du fichier des message d'un client selon la langue
Dim FichierMessage As String

'-> Rendre la main � la CPU pour traitement des autres messages
DoEvents

'-> Faire ici traitement
FileName = Dir$(Trans_Directory & "*.dmd", vbNormal)
If Trim(FileName) = "" Then GoTo endProg

'-> Ouverture du fichier ASCII
hdlFile = FreeFile
Open Trans_Directory & "\" & FileName For Input As #hdlFile

'-> Lecture de la ligne
Line Input #hdlFile, Ligne
Close #hdlFile

'-> Suppression du fichier ASCII
Kill Trans_Directory & "\" & FileName

'-> R�cup�ration des param�tres
Instruction = Entry(1, Ligne, "~")
Langue = CInt(Entry(2, Ligne, "~"))
Ident = Entry(3, Ligne, "~")
Version = Entry(4, Ligne, "~")
Progiciel = Entry(5, Ligne, "~")
Param_Mess = Trim(Entry(6, Ligne, "~"))



'-> Traitement selon l'instruction
Select Case UCase$(Trim(Instruction))

    Case "MESS_CREATE" 'Cr�ation d'un nouveau message
    
        '-> Impl�menter le mod�le objet
        Set aVersion = Versions(UCase$(Version))
        Set aClient = aVersion.Clients(UCase$(Ident))
        Set aApp = aClient.Apps(UCase$(Progiciel))
    
        '-> Cr�ation du nom de fichier � ouvrir
        FichierMessage = aClient.MessageFile & "\p-messages-" & Format(Langue, "00") & ".data"
    
        '-> Il faut que le fichier de message en Fran�ais existe
        If Dir$(FichierMessage, vbNormal) = "" Then
            '-> Cr�er le fichier
            hdlFile = FreeFile
            Open FichierMessage For Output As #hdlFile
            Close #hdlFile
        End If
        
        '-> Pointer sur l'applicatif/langue du client
        Set aLangue = aApp.Langues("LNG|" & Langue)
        
        '-> Ajouter un nouveau message en m�moire
        Set aMess = New Message
        aMess.NumMess = aApp.IdMessage
        aMess.Libel = Param_Mess
        aLangue.Messages.Add aMess, "MESS|" & aMess.NumMess
        
        '-> Retourner une valeur de succ�s avec le num�ro du message
        ReturnFile "MESS_CREATE~OK~" & aMess.NumMess, FileName
        
        '-> Reg�n�rer le fichier ASCII de message
        GenerateFileMess aClient, FichierMessage, Langue
        
        '-> Incr�menter l'index de message en m�moire
        aApp.IdMessage = aApp.IdMessage + 1
        
        '-> Incr�menter l'index de message dans le fichier ASCII
        WritePrivateProfileString Progiciel, Version, CStr(aApp.IdMessage), aClient.IniFile
    
        '-> Il faut g�n�rer le message en m�moire pour les autres codes langues
        
        '-> Il faut reg�n�rer les fichiers sur le disque pour les autres codes langues
    
    Case "MESS_SEND"
    
        strMessage = ""
    
        '-> Impl�menter le mod�le objet
        Set aVersion = Versions(UCase$(Version))
        Set aClient = aVersion.Clients(UCase$(Ident))
        Set aApp = aClient.Apps(UCase$(Progiciel))
    
        '-> Cr�ation du nom de fichier � ouvrir
        FichierMessage = aClient.MessageFile & "\p-messages-" & Format(Langue, "00") & ".data"
        
        '-> Il faut que le fichier de message en Fran�ais existe
        If Dir$(FichierMessage, vbNormal) = "" Then
            '-> Cr�er le fichier
            hdlFile = FreeFile
            Open FichierMessage For Output As #hdlFile
            Close #hdlFile
        End If
        
        '-> Pointer sur l'applicatif/langue du client
        Set aLangue = aApp.Langues("LNG|" & Langue)
                
        '-> Tester si doit supprimer, mettre � jour ou modifier
        If Trim(Entry(2, Param_Mess, "|")) = "" Then
            '-> Suppression du message
            For Each aMess In aLangue.Messages
                If CLng(aMess.NumMess) = CLng(Entry(1, Param_Mess, "|")) Then
                    '-> Supprimer le fichier
                    aLangue.Messages.Remove ("MESS|" & aMess.NumMess)
                    '-> pr�parer le r�ponse retour
                    strMessage = "MESS_SEND~OK~DELETE"
                    '-> Quitter la boucle
                    Exit For
                End If
            Next
        Else
            '-> V�rifier si le message existe
            For Each aMess In aLangue.Messages
                If CLng(aMess.NumMess) = CLng(Entry(1, Param_Mess, "|")) Then
                    '-> Modifier le contenu du message
                    aMess.Libel = Entry(2, Param_Mess, "|")
                    '-> pr�parer le r�ponse retour
                    strMessage = "MESS_SEND~OK~MODIFY"
                    '-> Quitter la boucle
                    Exit For
                End If
            Next
            
            '-> Cr�ation d'un nouveau message
            If strMessage = "" Then
                Set aMess = New Message
                aMess.Libel = Entry(2, Param_Mess, "|")
                aMess.NumMess = Entry(1, Param_Mess, "|")
                aLangue.Messages.Add aMess, "MESS|" & aMess.NumMess
                strMessage = "MESS_SEND~OK~CREATE"
            End If
        End If 'Si suppression de message, modification ou cr�ation
        
        '-> Envoyer une r�ponse
        ReturnFile strMessage, FileName
        
        '-> Reg�n�rer le fichier ASCII de message
        GenerateFileMess aClient, FichierMessage, Langue
        
    Case "MESS_UPDATE"
        '-> Met � jour un message
        
        '-> Impl�menter le mod�le objet
        Set aVersion = Versions(UCase$(Version))
        Set aClient = aVersion.Clients(UCase$(Ident))
        Set aApp = aClient.Apps(UCase$(Progiciel))
        
        '-> Lancer la proc�dure
        ModifMess aClient, aApp, Langue, Param_Mess, FileName
        
        
    
    Case "MESS_SELECT"
        '-> Retourne la liste des messages demand�s
        GenerateSel Ligne, FileName
        
    Case "MESS_FIND"
        '-> Recherche d'un message par son num�ro
        FindMess Ligne, FileName
    
    Case "MESS_FILETRANSLATE"
    
        '-> Cette proc�dure doit �tre appel�e pour cr�er un fichier dans une langue qui n'existe pas
        '-> Cette proc�dure duplique le fichier Fran�ais dans la langue sp�cifi�
        '-> Ne dupliquer que les messages qui n'exsitent pas dans le cas ou le fichier est d�ja existant
    
    
    
        
End Select 'Selon l'instruction

endProg:

'-> Tester si une demande de fin de traitement est effective
If AscForEnd Then

    '-> Positionner la variable
    IsRunning = False
            
    '-> Bloquer les boutons
    Me.Go.Enabled = Not IsRunning
    Me.Stop.Enabled = IsRunning
    
    '-> Arreter le timer
    Me.Timer1.Interval = 0
    
    '-> Supprimer le fichier lock
    Kill App.Path & "\messages\batch.lck"
    
End If 'Si demande de fin de traitement

End Sub
Private Sub ModifMess(ByRef aClient As Client, ByRef aApp As ApplicMess, ByVal IdLangue As Integer, ParamMess As String, ByVal FileName As String)

'---> cette proc�dure Modifie le libell� d'un messages

Dim aMess As Message
Dim Num_Mess As Long
Dim LibelMess As String
Dim aLangue As Langue


'-> Get du num�ro du message
Num_Mess = CLng(Entry(1, ParamMess, "|"))
LibelMess = Entry(2, ParamMess, "|")

On Error GoTo GestError

'-> Poinetr sur la langue sp�cifi�e
Set aLangue = aApp.Langues("LNG|" & IdLangue)

'-> Pointer sur le message de l'application
Set aMess = aLangue.Messages("MESS|" & Num_Mess)

'-> Modifier en m�moire
aMess.Libel = LibelMess

'-> Reg�n�rer le fichier des messages
GenerateFileMess aClient, aClient.MessageFile & "p-messages-" & Format(IdLangue, "00") & ".data", IdLangue

'-> Reg�nrer un fichier de retour OK
ReturnFile "MESS_UPDATE~OK", FileName

'-> Quitter la fonction
Exit Sub

GestError:

    '-> Imprimer une erreur dans le fichier retour
    ReturnFile "MESS_UPDATE~ERROR~Message inexistant", FileName
    


End Sub

Private Sub FindMess(Ligne As String, InitFile As String)

Dim hdlFile As Integer
Dim TempfileName As String
Dim FileName As String
Dim aClient As Client
Dim aApp As ApplicMess
Dim aVersion As Version
Dim aLangue As Langue
Dim aMess As Message

'-> R�cup�rer le nom du fichier d'origine sans l'extension
FileName = Entry(1, InitFile, ".")

'-> Cr�er un fichier temporaire
hdlFile = FreeFile
Open App.Path & "\temp\" & FileName & ".tmp" For Output As #hdlFile

'-> Pointer sur la version
Set aVersion = Versions(UCase$(Entry(4, Ligne, "~")))

'-> Pointer sur le client
Set aClient = aVersion.Clients(UCase$(Entry(3, Ligne, "~")))

'-> Analyse � la recherche du message
For Each aApp In aClient.Apps
    '-> Pointer sur la classe langue sp�cifi�e
    Set aLangue = aApp.Langues("LNG|" & CInt(Entry(2, Ligne, "~")))
    '-> Pour tous les messages dans la langue
    For Each aMess In aLangue.Messages
        If CLng(aMess.NumMess) = CLng(Entry(6, Ligne, "~")) Then
            '-> On a trouv� le message : Ecrire la r�ponse te se barrer
            Print #hdlFile, aMess.NumMess & "�" & aApp.Code & "�" & Trim(aMess.Libel)
            '-> Quitter l'analyse
            GoTo EndAnalyse
        End If
    Next 'Pour tous les messages
Next

EndAnalyse:

'-> fermer le fichier
Close #hdlFile

'-> Faire une copie sous forme de RSP
If Dir$(Trans_Directory & FileName & ".rsp", vbNormal) <> "" Then Kill Trans_Directory & FileName & ".rsp"
Name App.Path & "\temp\" & FileName & ".tmp" As Trans_Directory & FileName & ".rsp"


End Sub

Private Sub GenerateSel(Ligne As String, InitFile As String)

'---> Proc�dure g�n�re un fichier de s�lection

Dim hdlFile As Integer
Dim TempfileName As String
Dim FileName As String
Dim aClient As Client
Dim aApp As ApplicMess
Dim aVersion As Version

'-> R�cup�rer le nom du fichier d'origine sans l'extension
FileName = Entry(1, InitFile, ".")

'-> Cr�er un fichier temporaire
hdlFile = FreeFile
Open App.Path & "\temp\" & FileName & ".tmp" For Output As #hdlFile

'-> Pointer sur la version
Set aVersion = Versions(UCase$(Entry(4, Ligne, "~")))

'-> Pointer sur le client
Set aClient = aVersion.Clients(UCase$(Entry(3, Ligne, "~")))

'-> Doit-on analyser tous les applicatifs
If Trim(Entry(5, Ligne, "~")) = "*" Then
    '-> Renseigner pour tous les applicatifs
    For Each aApp In aClient.Apps
        CreateSel hdlFile, aApp, CInt(Entry(2, Ligne, "~")), Entry(6, Ligne, "~")
    Next 'Pour tous les applicatifs
Else
    '-> Pointer sur l'applicatif
    Set aApp = aClient.Apps(UCase$(Trim(Entry(5, Ligne, "~"))))
    '-> G�n�rer les messages pour cette appliaction
    CreateSel hdlFile, aApp, CInt(Entry(2, Ligne, "~")), Entry(6, Ligne, "~")
End If

'-> fermer le fichier
Close #hdlFile

'-> Faire une copie sous forme de RSP
If Dir$(Trans_Directory & FileName & ".rsp", vbNormal) <> "" Then Kill Trans_Directory & FileName & ".rsp"
Name App.Path & "\temp\" & FileName & ".tmp" As Trans_Directory & FileName & ".rsp"

End Sub

Private Sub CreateSel(ByVal hdlFile As Integer, ByRef aApp As ApplicMess, IdLangue As Integer, Selection As String)

'---> Cette proc�dure �crit dans un fichier les messages d'un applicatif

Dim aLangue As Langue
Dim aMess As Message
Dim TypeSel As Integer
Dim ParamSel As String
Dim ToPrint As Boolean

'-> Pointer  sur la langue sp�cifi�e
Set aLangue = aApp.Langues("LNG|" & IdLangue)

'-> R�cup�ration des param�tres
TypeSel = CInt(Entry(1, Selection, "|"))
ParamSel = Entry(2, Selection, "|")

'-> Pour tous les messages
For Each aMess In aLangue.Messages
    '-> de base ne pas imprimer
    ToPrint = False
    Select Case TypeSel
        Case 0 '-> Tous les messprog
            ToPrint = True
        Case 1 '-> Contenant
            If InStr(1, UCase$(aMess.Libel), UCase$(ParamSel)) <> 0 Then ToPrint = True
        Case 2 '-> Commen�ant par
            If UCase$(Mid$(aMess.Libel, 1, Len(ParamSel))) = UCase$(ParamSel) Then ToPrint = True
    End Select 'Selon la nature de la s�lection
    '-> Imprimer si on doit
    If ToPrint Then Print #hdlFile, aMess.NumMess & "�" & aApp.Code & "�" & Trim(aMess.Libel)
Next 'Pour tous les messages de l'application


End Sub

Private Sub GenerateFileMess(aClient As Client, FichierMessage As String, IdLangue As Integer)

'---> Cette proc�dure reg�nere le fichier des messprogs
Dim hdlFile As Integer
Dim i As Long, j As Long
Dim aMes As MessageType
Dim aApp As ApplicMess
Dim CodeApp As String
Dim RowId As Integer
Dim aLangue As Langue
Dim aMess As Message
Dim LibelMessage As String
Dim strTri As String
Dim strNonTri As String

On Error GoTo GestError

'-> Ouvrir le fichier langue
hdlFile = FreeFile
Open FichierMessage For Random As #hdlFile Len = Len(aMes)

'-> Initialiser le positionnement par enregistrement
RowId = 1

strNonTri = ""
strTri = ""
'-> Reg�n�rer les messages dans l'odres des applicatifs
For i = 1 To NumEntries(aClient.ListeApp, "|")
    '-> Pointer sur l'application
    Set aApp = aClient.Apps(UCase$(Entry(i, aClient.ListeApp, "|")))
    '-> pointer sur la langue
    Set aLangue = aApp.Langues("LNG|" & IdLangue)
    '-> Inscrire le message
    For Each aMess In aLangue.Messages
        If strNonTri = "" Then
            strNonTri = Format(aMess.NumMess, "000000000") & "�" & Format(aMess.NumMess, "000000000")
        Else
            strNonTri = strNonTri & "|" & Format(aMess.NumMess, "000000000") & "�" & Format(aMess.NumMess, "000000000")
        End If
    Next
Next

'-> Faire le tri
strTri = Tri(strNonTri, "|", "�")
        
If strTri <> "" Then
    '-> Effectuer l'impression
    For j = 1 To NumEntries(strTri, "|")
        '-> Pointer sur l'application
        Set aApp = aClient.Apps(GetAppByNum(aClient, Entry(j, strTri, "|")))
        '-> pointer sur la langue
        Set aLangue = aApp.Langues("LNG|" & IdLangue)
        '-> Pointer sur le message
        Set aMess = aLangue.Messages("MESS|" & CLng(Entry(j, strTri, "|")))
        '-> Construire le message
        LibelMessage = Format(aMess.NumMess, "000000000")
        LibelMessage = LibelMessage & aMess.Libel
        '-> compl�ter avec des blancs
        aMes.Libel = LibelMessage & Space$(259 - Len(LibelMessage))
        '-> Enregistrer le message
        Put #hdlFile, RowId, aMes
        '-> Incr�menter le rowid
        RowId = RowId + 1
    Next 'Pour tous les messages d'une langue donn�e
End If

'-> Fermer le fichier
Close #hdlFile

Exit Sub

GestError:
    Resume

End Sub


Private Sub ReturnFile(strToReturn As String, InitFile As String)

'---> Cette fonction cr�er un fichier r�ponse et �crit dedans la chaine pass�e en param

Dim hdlFile As Integer
Dim TempfileName As String
Dim FileName As String

'-> R�cup�rer le nom du fichier d'origine sans l'extension
FileName = Entry(1, InitFile, ".")

'-> Cr�er un fichier temporaire
hdlFile = FreeFile
Open App.Path & "\temp\" & FileName & ".tmp" For Output As #hdlFile

'-> �crire la r�ponse
Print #hdlFile, strToReturn

'-> fermer le fichier
Close #hdlFile

'-> Faire une copie sous forme de RSP
If Dir$(Trans_Directory & FileName & ".rsp", vbNormal) <> "" Then Kill Trans_Directory & FileName & ".rsp"
Name App.Path & "\temp\" & FileName & ".tmp" As Trans_Directory & FileName & ".rsp"


End Sub


