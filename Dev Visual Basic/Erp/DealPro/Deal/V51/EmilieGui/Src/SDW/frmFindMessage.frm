VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmFindMessage 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recherche d'un message"
   ClientHeight    =   8565
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12105
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8565
   ScaleWidth      =   12105
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   3240
      ScaleHeight     =   345
      ScaleWidth      =   5265
      TabIndex        =   9
      Top             =   4920
      Visible         =   0   'False
      Width           =   5295
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "En attente de la r�ponse du serveur ....................................................."
         Height          =   375
         Left            =   120
         TabIndex        =   10
         Top             =   60
         Width           =   5055
      End
   End
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
      Height          =   5535
      Left            =   120
      TabIndex        =   6
      Top             =   3000
      Width           =   11895
      _ExtentX        =   20981
      _ExtentY        =   9763
      _Version        =   393216
      Cols            =   3
      FixedCols       =   0
      ScrollBars      =   2
   End
   Begin VB.Frame Frame1 
      Caption         =   "Crit�res de recherche :"
      Height          =   2775
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   11895
      Begin VB.ListBox List3 
         Height          =   2010
         Left            =   9840
         Sorted          =   -1  'True
         TabIndex        =   15
         Top             =   600
         Width           =   1935
      End
      Begin VB.ListBox List2 
         Height          =   2010
         Left            =   2880
         Sorted          =   -1  'True
         TabIndex        =   13
         Top             =   600
         Width           =   3255
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Sp�cifier l'applicatif"
         Height          =   255
         Left            =   6240
         TabIndex        =   12
         Top             =   300
         Width           =   2055
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   2160
         Picture         =   "frmFindMessage.frx":0000
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   7
         ToolTipText     =   "Cliqer ici pour lancer la recherche"
         Top             =   1200
         Width           =   480
      End
      Begin VB.Frame Frame2 
         Height          =   1575
         Left            =   120
         TabIndex        =   2
         Top             =   960
         Width           =   2655
         Begin VB.OptionButton Option4 
            Caption         =   "Num�ro : "
            Height          =   255
            Left            =   120
            TabIndex        =   17
            Top             =   1200
            Width           =   2055
         End
         Begin VB.OptionButton Option3 
            Caption         =   "Commen�ant : "
            Height          =   255
            Left            =   120
            TabIndex        =   8
            Top             =   840
            Width           =   1335
         End
         Begin VB.TextBox strFind 
            Height          =   285
            Left            =   1560
            TabIndex        =   5
            Top             =   840
            Width           =   975
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Contenant :"
            Height          =   255
            Left            =   120
            TabIndex        =   4
            Top             =   550
            Width           =   1215
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Tous les messages"
            Height          =   255
            Left            =   120
            TabIndex        =   3
            Top             =   240
            Value           =   -1  'True
            Width           =   1815
         End
      End
      Begin VB.ListBox List1 
         Enabled         =   0   'False
         Height          =   2010
         Left            =   6240
         Sorted          =   -1  'True
         TabIndex        =   1
         Top             =   600
         Width           =   3495
      End
      Begin MSComctlLib.ImageCombo ImageCombo1 
         Height          =   570
         Left            =   120
         TabIndex        =   11
         Top             =   360
         Width           =   2655
         _ExtentX        =   4683
         _ExtentY        =   1005
         _Version        =   393216
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Text            =   "ImageCombo1"
         ImageList       =   "ImageList1"
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   7800
         Top             =   840
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   6
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFindMessage.frx":030A
               Key             =   ""
               Object.Tag             =   "Fran�ais"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFindMessage.frx":075C
               Key             =   ""
               Object.Tag             =   "Anglais"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFindMessage.frx":0BAE
               Key             =   ""
               Object.Tag             =   "Allemand"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFindMessage.frx":1000
               Key             =   ""
               Object.Tag             =   "Portugais"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFindMessage.frx":131A
               Key             =   ""
               Object.Tag             =   "Espagnol"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFindMessage.frx":176C
               Key             =   ""
               Object.Tag             =   "Italien"
            EndProperty
         EndProperty
      End
      Begin VB.Label Label3 
         Caption         =   "S�lection de la version : "
         Height          =   255
         Left            =   9840
         TabIndex        =   16
         Top             =   300
         Width           =   1935
      End
      Begin VB.Label Label1 
         Caption         =   "S�lection d'un client : "
         Height          =   255
         Left            =   2880
         TabIndex        =   14
         Top             =   300
         Width           =   1815
      End
   End
   Begin VB.Menu mnuPOPUP 
      Caption         =   "mnuPOPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuModif 
         Caption         =   "Modifer"
      End
      Begin VB.Menu mnuSendToclient 
         Caption         =   "Envoyer vers un autre client"
      End
   End
End
Attribute VB_Name = "frmFindMessage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Check1_Click()

If Me.Check1.Value = 1 Then
    Me.List1.Enabled = True
Else
    Me.List1.Enabled = False
End If


End Sub

Private Sub Form_Load()

Dim Res As Long
Dim aRect As RECT
Dim i As Integer

'-> Get de la taille interne du flex
Res = GetClientRect(Me.MSFlexGrid1.hwnd, aRect)

'-> Tailler les colonnes du flex
Me.MSFlexGrid1.ColWidth(0) = Me.TextWidth("000000000") * 1.5
Me.MSFlexGrid1.ColWidth(1) = Me.TextWidth("WWW") * 1.5
Me.MSFlexGrid1.ColWidth(2) = Me.ScaleX(aRect.Right, 3, 1) - Me.MSFlexGrid1.ColWidth(0) - Me.MSFlexGrid1.ColWidth(1)

'-> titres des colonnes
Me.MSFlexGrid1.Row = 0
Me.MSFlexGrid1.Col = 0
Me.MSFlexGrid1.Text = "Num�ro"
Me.MSFlexGrid1.Col = 1
Me.MSFlexGrid1.Text = "App"
Me.MSFlexGrid1.Col = 2
Me.MSFlexGrid1.Text = "Message"

'-> Charger la liste des applicatifs
For i = 1 To NumEntries(frmMess.ListeApp, "|")
    Me.List1.AddItem Entry(i, frmMess.ListeApp, "|") & " - " & Entry(i, frmMess.LibApp, "|")
Next

'-> Charger la liste des clients
For i = 1 To NumEntries(frmMess.ListeClient, "|")
    Me.List2.AddItem Entry(i, frmMess.ListeClient, "|") & " - " & Entry(i, frmMess.LibClient, "|")
    If UCase$(Entry(i, frmMess.ListeClient, "|")) = "DEAL" Then Me.List2.Selected(Me.List2.ListCount - 1) = True
Next

'-> S�lectionner le premier �l�ment
Me.List1.Selected(0) = True

'-> Charger le fichier langue
For i = 1 To 6
    Me.ImageCombo1.ComboItems.Add i, , Me.ImageList1.ListImages(i).Tag, i
Next
Me.ImageCombo1.ComboItems(1).Selected = True

'-> Charger la liste des versions
For i = 1 To NumEntries(frmMess.ListeVersion, "|")
    Me.List3.AddItem Entry(i, frmMess.ListeVersion, "|")
Next
Me.List3.Selected(0) = True

'-> Indiquer que la feuille est visible
frmMess.IsFrmFindMess = True

End Sub

Private Sub Form_Unload(Cancel As Integer)

'-> Indiquer que la feuille n'est plus visible
frmMess.IsFrmFindMess = False

End Sub

Private Sub List2_Click()

'-> Supprimer la liste des messages
Me.MSFlexGrid1.Rows = 1


End Sub

Private Sub mnuModif_Click()
strRetour = "1"
End Sub

Private Sub mnuSendToclient_Click()
strRetour = "2"
End Sub

Private Sub MSFlexGrid1_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)

'---> Partir en modification

If Me.MSFlexGrid1.Row = 0 Then Exit Sub

'-> Setting des variables
Me.MSFlexGrid1.Col = 0
If Me.MSFlexGrid1.Text = "" Then Exit Sub

'-> Tester le bouton
If Button <> vbRightButton Then Exit Sub

strRetour = ""

'-> Afficher le menu
Me.PopupMenu Me.mnuPOPUP


If strRetour = "" Then Exit Sub

Select Case strRetour

    Case "1"
    
        frmModifMessage.Text1.Text = Me.MSFlexGrid1.Text
        Me.MSFlexGrid1.Col = 1
        frmModifMessage.Progiciel = Me.MSFlexGrid1.Text
        Me.MSFlexGrid1.Col = 2
        frmModifMessage.Text3.Text = Me.MSFlexGrid1.Text
        frmModifMessage.Text2.Text = Me.MSFlexGrid1.Text
        frmModifMessage.CodeLangue = Me.ImageCombo1.SelectedItem.Index
        frmModifMessage.Ident = Trim(Entry(1, Me.List2.Text, "-"))
        frmModifMessage.Version = Me.List3.Text
        
        '-> Afficher la feuille
        frmModifMessage.Show vbModal

    Case "2"
        
        '-> Il vaut v�rifier que l'emetteur soit DEAL
        If UCase$(Trim(Entry(1, Me.List2.Text, "-"))) <> "DEAL" Then
            MsgBox "Veuillez s�lectionner DEAL comme client emetteur.", vbExclamation + vbOKOnly, "Avertissement"
            Exit Sub
        End If
                
        '-> Setting des valeurs
        frmSendToIdent.Label11.Caption = Me.MSFlexGrid1.Text
        frmSendToIdent.Label3.Caption = Me.ImageCombo1.SelectedItem.Index
        Me.MSFlexGrid1.Col = 1
        frmSendToIdent.Label7.Caption = Me.MSFlexGrid1.Text
        frmSendToIdent.Label5.Caption = Me.List3.Text
        frmSendToIdent.Label9.Caption = UCase$(Entry(1, Me.List2.Text, "-"))
        Me.MSFlexGrid1.Col = 2
        frmSendToIdent.Label13.Caption = Me.MSFlexGrid1.Text
        frmSendToIdent.Text1.Text = Me.MSFlexGrid1.Text
    
        '-> Afficher la feuille
        frmSendToIdent.Show vbModal
        
End Select

End Sub

Private Sub Picture1_Click()

Dim Ligne As String
Dim strTempo As String
Dim i As Long
Dim hdlFile As Integer
Dim Num_Mess As String


'-> V�rifier si code langue rens�ign�
If Me.ImageCombo1.SelectedItem Is Nothing Then
    MsgBox "Veuillez s�lectionner une langue", vbCritical + vbOKOnly, "Erreur"
    Me.ImageCombo1.SetFocus
    Exit Sub
End If

'-> V�rifier si client saisi
If Me.List2.ListIndex = -1 Then
    MsgBox "Veuillez s�lectionner un client", vbCritical + vbOKOnly, "Erreur"
    Me.List2.SetFocus
    Exit Sub
End If

'-> V�rifier si version saisie
If Me.List3.ListIndex = -1 Then
    MsgBox "Veuillez s�lectionner une version", vbCritical + vbOKOnly, "Erreur"
    Me.List3.SetFocus
    Exit Sub
End If

If Me.Option4.Value Then
    '-> Demander le num�ro du message � recherche
    Num_Mess = InputBox("VEuillez saisir le num�ro du message recherch�", "Recherche d'un message par num�ro", "")
    If Num_Mess = "" Then Exit Sub
    
    If Not IsNumeric(Num_Mess) Then
        MsgBox "Num�ro de message incorrect", vbCritical + vbOKOnly, "Erreur"
        Exit Sub
    End If
    
    '-> Chaine de demande
    strTempo = "MESS_FIND~"
    
Else

    '-> Controler la saisie d'un app
    If Me.Check1.Value = 1 And Me.List1.ListIndex = -1 Then
        MsgBox "Veuillez s�lectionner un applicatif"
        Me.List1.SetFocus
        Exit Sub
    End If
    
    '-> Tester si crit�re de recherche rens�ign�
    If Me.Option2.Value Or Me.Option3.Value Then
        If Me.strFind.Text = "" Then
            MsgBox "Pour ce type de recherche, veuillez s�lectionner un crit�re", vbCritical + vbOKOnly, "Erreur"
            Me.strFind.SetFocus
            Exit Sub
        End If
    End If
        
    '-> Chaine de la demande
    strTempo = "MESS_SELECT~"

End If

'-> Ouvrir la demande
hdlFile = OpenDmd

'-> Cr�ation de la demande
strTempo = strTempo & Me.ImageCombo1.SelectedItem.Index & "~" & Trim(Entry(1, Me.List2.Text, "-")) & "~" & Me.List3.Text & "~"

'-> Choix du progiciel
If Me.Check1.Value Then
    strTempo = strTempo & Trim(Entry(1, Me.List1.Text, "-")) & "~"
Else
    strTempo = strTempo & "*~"
End If

'-> Crit�re de s�lection
If Me.Option1.Value Then
    strTempo = strTempo & "0|0"
ElseIf Me.Option2.Value Then
    strTempo = strTempo & "1|" & Me.strFind.Text
ElseIf Me.Option3.Value Then
    strTempo = strTempo & "2|" & Me.strFind.Text
Else
    strTempo = strTempo & Num_Mess
End If

'-> Imprimer la demande
Print #hdlFile, strTempo

'-> Fermer le fichier tempo
Close #hdlFile

'-> Faire un rename dans le r�pertoire de transfert
Name TempoFileName As DmdFileName

'-> bloquer l'interface
MdiMain.Enabled = False

'-> Modifier le pointeur
Me.MousePointer = 11

'-> Afficher un message d'attente
Picture2.Visible = True

'-> Se mettre en attente de la r�ponse
Do
    Sleep 100
    '-> Rendre la main � la CPU
    DoEvents
    '-> V�rifier si on trouve le fichier r�ponse
    If Dir$(RspFileName, vbNormal) <> "" Then Exit Do
Loop

'-> Vider la grille
Me.MSFlexGrid1.Rows = 1
Me.MSFlexGrid1.ColAlignment(2) = 1

'-> Erreur si pas de r�ponse
If FileLen(RspFileName) <> 0 Then

    '-> Ouvrir le fichier
    hdlFile = FreeFile
    Open RspFileName For Input As hdlFile
    
    '-> Afficher le r�sultat
    Do While Not EOF(hdlFile)
        '-> Lecture de la ligne
        Line Input #hdlFile, Ligne
        
        If Trim(Ligne) <> "" Then
            '-> Ajouter une ligne dans le browse
            Me.MSFlexGrid1.Rows = Me.MSFlexGrid1.Rows + 1
            Me.MSFlexGrid1.Row = Me.MSFlexGrid1.Rows - 1
            '-> Setting du texte
            Me.MSFlexGrid1.Col = 0
            Me.MSFlexGrid1.Text = Entry(1, Ligne, "�")
            Me.MSFlexGrid1.Col = 1
            Me.MSFlexGrid1.Text = Entry(2, Ligne, "�")
            Me.MSFlexGrid1.Col = 2
            Me.MSFlexGrid1.Text = Entry(3, Ligne, "�")
        End If
    Loop 'Boucle d'analyse du fichier r�ponse
        
    '-> Fermer le fichier
    Close #hdlFile
    
End If 'S'il ya des r�ponses

'-> Supprimer le fichier r�ponse
Kill RspFileName

'-> Debloquer l'interface
MdiMain.Enabled = True
Me.MousePointer = 0
Me.Picture2.Visible = False


End Sub



Private Sub strFind_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Picture1_Click

End Sub
