VERSION 5.00
Begin VB.Form frmSelDmd 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recherche d'une demande"
   ClientHeight    =   5475
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9735
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5475
   ScaleWidth      =   9735
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   5295
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   9615
      Begin VB.ListBox List5 
         Enabled         =   0   'False
         Height          =   1425
         Left            =   240
         TabIndex        =   20
         Top             =   3720
         Width           =   3255
      End
      Begin VB.CheckBox Check8 
         Caption         =   "Sp�cifier une version :"
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   3360
         Width           =   3015
      End
      Begin VB.CheckBox Check7 
         Caption         =   "Nature de validation"
         Height          =   255
         Left            =   3600
         TabIndex        =   18
         Top             =   2520
         Width           =   2775
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Lancer la recherche"
         Height          =   255
         Left            =   6600
         TabIndex        =   17
         Top             =   4850
         Width           =   2895
      End
      Begin VB.Frame Frame2 
         Height          =   495
         Left            =   3600
         TabIndex        =   14
         Top             =   2700
         Width           =   2895
         Begin VB.OptionButton Option2 
            Caption         =   "Non Valid�es"
            Enabled         =   0   'False
            Height          =   195
            Left            =   1320
            TabIndex        =   16
            Top             =   200
            Width           =   1455
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Valid�es"
            Enabled         =   0   'False
            Height          =   195
            Left            =   120
            TabIndex        =   15
            Top             =   200
            Value           =   -1  'True
            Width           =   1455
         End
      End
      Begin VB.TextBox Text3 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3600
         TabIndex        =   13
         Top             =   4800
         Width           =   2895
      End
      Begin VB.TextBox Text2 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3600
         TabIndex        =   12
         Top             =   4080
         Width           =   2895
      End
      Begin VB.CheckBox Check6 
         Caption         =   "Recherche par date : "
         Enabled         =   0   'False
         Height          =   255
         Left            =   3600
         TabIndex        =   11
         Top             =   3360
         Width           =   1815
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3600
         TabIndex        =   10
         Top             =   2160
         Width           =   2895
      End
      Begin VB.CheckBox Check5 
         Caption         =   "Sp�cifier une nature :"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   1800
         Width           =   3255
      End
      Begin VB.CheckBox Check4 
         Caption         =   "Sp�cifier un client : "
         Height          =   255
         Left            =   6600
         TabIndex        =   8
         Top             =   240
         Width           =   2295
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Sp�cifier un progiciel"
         Height          =   255
         Left            =   3600
         TabIndex        =   7
         Top             =   240
         Width           =   2895
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Sp�cifier un service"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   240
         Width           =   3015
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Sp�cifier un num�ro en particulier : "
         Height          =   255
         Left            =   3600
         TabIndex        =   5
         Top             =   1800
         Width           =   2775
      End
      Begin VB.ListBox List4 
         Enabled         =   0   'False
         Height          =   1035
         Left            =   240
         TabIndex        =   4
         Top             =   2160
         Width           =   3255
      End
      Begin VB.ListBox List3 
         Enabled         =   0   'False
         Height          =   3765
         Left            =   6600
         TabIndex        =   3
         Top             =   600
         Width           =   2895
      End
      Begin VB.ListBox List2 
         Enabled         =   0   'False
         Height          =   1035
         Left            =   3600
         TabIndex        =   2
         Top             =   600
         Width           =   2895
      End
      Begin VB.ListBox List1 
         Enabled         =   0   'False
         Height          =   1035
         Left            =   240
         TabIndex        =   1
         Top             =   600
         Width           =   3255
      End
      Begin VB.Label Label2 
         Caption         =   "Date maxi : "
         Enabled         =   0   'False
         Height          =   255
         Left            =   3600
         TabIndex        =   22
         Top             =   4440
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Date mini : "
         Enabled         =   0   'False
         Height          =   255
         Left            =   3600
         TabIndex        =   21
         Top             =   3720
         Width           =   1695
      End
   End
End
Attribute VB_Name = "frmSelDmd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Check1_Click()

If Me.Check1.Value = 1 Then
    Me.Text1.Enabled = True
Else
    Me.Text1.Text = ""
    Me.Text1.Enabled = False
End If

End Sub

Private Sub Check2_Click()

If Me.Check2.Value = 1 Then
    Me.List1.Enabled = True
    Me.List1.Selected(0) = True
Else
    Me.List1.Enabled = False
    Me.List1.ListIndex = -1
End If


End Sub

Private Sub Check3_Click()

If Me.Check3.Value = 1 Then
    Me.List2.Enabled = True
    Me.List2.Selected(0) = True
Else
    Me.List2.Enabled = False
    Me.List2.ListIndex = -1
End If


End Sub

Private Sub Check4_Click()

If Me.Check4.Value = 1 Then
    Me.List3.Enabled = True
    Me.List3.Selected(0) = True
Else
    Me.List3.Enabled = False
    Me.List3.ListIndex = -1
End If


End Sub

Private Sub Check5_Click()

If Me.Check5.Value = 1 Then
    Me.List4.Enabled = True
    Me.List4.Selected(0) = True
Else
    Me.List4.Enabled = False
    Me.List4.ListIndex = -1
End If


End Sub

Private Sub Check6_Click()

If Me.Check6.Value = 1 Then
    Me.Text2.Enabled = True
    Me.Text3.Enabled = True
Else
    Me.Text2.Text = ""
    Me.Text2.Enabled = False
    Me.Text3.Text = ""
    Me.Text3.Enabled = False
End If

End Sub

Private Sub Check7_Click()

If Me.Check7.Value = 1 Then
    Me.Option1.Enabled = True
    Me.Option2.Enabled = True
Else
    Me.Option1.Enabled = False
    Me.Option2.Enabled = False
End If


End Sub

Private Sub Check8_Click()

If Me.Check8.Value = 1 Then
    Me.List5.Enabled = True
    Me.List5.Selected(0) = True
Else
    Me.List5.Enabled = False
    Me.List5.ListIndex = -1
End If

End Sub

Private Sub Command1_Click()

Dim Res As Long
Dim lpBuffer As String
Dim PathToAnalyse As String
Dim FileName As String
Dim ListeFile As String
Dim aPath As String


On Error GoTo GestError

'-> V�rifier la coh�rence de la saisie
If Me.Check1.Value = 1 And Trim(Me.Text1.Text) = "" Then
    MsgBox "Veuillez saisir le num�ro � rechercher", vbCritical + vbOKOnly, "Erreur"
    Me.Text1.SetFocus
    Exit Sub
End If

If Me.Check6.Value = 1 Then
    If Trim(Me.Text2.Text) = "" Then
        MsgBox "Veuillez saisir la date mini d'analyse", vbCritical + vbOKOnly, "Erreur"
        Me.Text2.SetFocus
        Exit Sub
    Else
        If Not IsDate(Me.Text2.Text) Then
            MsgBox "Date incorrecte", vbCritical + vbOKOnly, "Erreur"
            Me.Text2.SetFocus
            Exit Sub
        End If
    End If
    If Trim(Me.Text3.Text) = "" Then
        MsgBox "Veuillez saisir la date maxi d'analyse", vbCritical + vbOKOnly, "Erreur"
        Me.Text3.SetFocus
        Exit Sub
    Else
        If Not IsDate(Me.Text3.Text) Then
            MsgBox "Date incorrecte", vbCritical + vbOKOnly, "Erreur"
            Me.Text3.SetFocus
            Exit Sub
        End If
    End If
End If 'Si s�lection sur les dates

'-> Bloquer l'�cran
Me.Enabled = False
Me.MousePointer = 11

'-> Vider le flexgrid de recherche
frmFindDmd.MSFlexGrid1.Rows = 1

'-> Analyse de tous les fichiers de demande pour les versions
If Check8.Value = 0 Then
    For i = 0 To Me.List5.ListCount - 1
        If PathToAnalyse = "" Then
            PathToAnalyse = App.Path & "\demandes\" & Me.List5.List(i) & "\dmd\"
        Else
            PathToAnalyse = PathToAnalyse & "|" & App.Path & "\demandes\" & Me.List5.List(i) & "\dmd\"
        End If
    Next 'Pour toutes les versions
Else
    PathToAnalyse = App.Path & "\demandes\" & Me.List5.Text & "\dmd\"
End If

'-> anlayse de tous les paths de version
For i = 1 To NumEntries(PathToAnalyse, "|")
    
    '-> R�cup�rer le path
    aPath = Entry(i, PathToAnalyse, "|")
        
    '-> On cr�er la liste des fichiers � analyser car comme on fait un dir dans l'autre proc�dure -> Erreur compilateur
    FileName = Dir(aPath & "*.dmd")
    Do While FileName <> ""
        If ListeFile = "" Then
            ListeFile = FileName
        Else
            ListeFile = ListeFile & "|" & FileName
        End If
        '-> Analyser le fichier suivant
        FileName = Dir
    Loop 'Pour toutes les demandes
    
    '-> Analyse de la liste des fichiers demandes
    If ListeFile <> "" Then
        For j = 1 To NumEntries(ListeFile, "|")
            '-> Ajouter dans le treeview de recherche
            Call IsOkFile(Entry(j, ListeFile, "|"), aPath)
        Next 'Pour toutes les demandes
    End If 'S'il y a des fichiers � analyser
    
Next 'Pour toutes les versions � analyser

'-> Aller � la fin du programme
GoTo EndProg

Exit Sub

GestError:

    MsgBox Err.Number & "    Erreur durant l'analyse des demandes", vbCritical + vbOKOnly, "Erreur"

EndProg:

    '-> D�bloquer l'�cran
    Me.Enabled = True
    Me.MousePointer = 0
    
    '-> D�bloquer le browse
    If frmFindDmd.MSFlexGrid1.Rows = 1 Then
        frmFindDmd.MSFlexGrid1.Enabled = False
    Else
        frmFindDmd.MSFlexGrid1.Enabled = True
    End If
    
    '-> D�charger la feuille
    Unload Me



End Sub

Private Sub IsOkFile(ByVal FileName As String, Path As String)

'---> Cette fonction d�termine si le fichier sp�cifi� est conforme � la demande de recherche

Dim IsValidDmd As Boolean
Dim strTempo As String
Dim Res As Long
Dim lpBuffer As String
Dim Progiciel As String
Dim Client As String
Dim Descriptif As String


'-> Tester si le fichier est valid�
lpBuffer = Space$(100)
Res = GetPrivateProfileString("DMD", "VALID", "", lpBuffer, Len(lpBuffer), Path & FileName)
If Mid$(lpBuffer, 1, Res) = "1" Then IsValidDmd = True
    

'*********************************
'* Test si fichier valid� ou non *
'*********************************

'-> Recherche des fichiers � analyser
If Me.Check7.Value = 1 Then
    '-> Indiquer si on doit ajouter ou non ce fichier
    If Me.Option1.Value Then
        '-> analyse des  valid�es
        If Not IsValidDmd Then Exit Sub
    Else
        '-> analyse que des non valid�es
        If IsValidDmd Then Exit Sub
    End If 'Selon la nature de la validation
End If

'***********************
'* Test sur le service *
'***********************

If Me.Check2.Value = 1 Then
    '-> R�cup�rer le code du service s�lectionn�
    strTempo = UCase$(Entry(1, Entry(Me.List1.ListIndex + 1, ListeServices, "|"), "�"))
    '-> R�cup�ration du code service de la demande
    lpBuffer = Space$(100)
    Res = GetPrivateProfileString("DMD", "SERVICE", "", lpBuffer, Len(lpBuffer), Path & FileName)
    If Res = 0 Then
        '-> Quitter
        Exit Sub
    Else
        '-> Comparer les 2 codes
        lpBuffer = UCase$(Mid$(lpBuffer, 1, Res))
        If lpBuffer <> strTempo Then Exit Sub
    End If 'Si on trouve le code du service
End If 'si on doit tester le service


'***********************************
'* Test sur le progiciel de la dmd *
'***********************************

'-> R�cup�rer le progiciel
lpBuffer = Space$(100)
Res = GetPrivateProfileString("DMD", "PROGICIEL", "", lpBuffer, Len(lpBuffer), Path & FileName)
lpBuffer = UCase$(Mid$(lpBuffer, 1, Res))
Progiciel = lpBuffer

If Me.Check3.Value = 1 Then
    '-> R�cup�rer le code du progiciel s�lectionn�
    strTempo = UCase$(Entry(1, Entry(Me.List2.ListIndex + 1, ListeProgiciels, "|"), "�"))
    '-> Comparer les 2 codes
    If lpBuffer <> strTempo Then Exit Sub
End If 'si on doit tester le progiciel
    
'*********************************
'* Test sur le client de la DMD  *
'*********************************

'-> R�cup�rer le code client
lpBuffer = Space$(100)
Res = GetPrivateProfileString("DMD", "CLIENT", "", lpBuffer, Len(lpBuffer), Path & FileName)
lpBuffer = UCase$(Mid$(lpBuffer, 1, Res))
Client = lpBuffer

If Me.Check4.Value = 1 Then
    '-> R�cup�rer le code du client s�lectionn�
    strTempo = UCase$(Trim(Entry(1, Me.List3.Text, "-")))
    '-> Comparer les 2 codes
    If lpBuffer <> strTempo Then Exit Sub
End If 'si on doit tester le client

    
'*********************************
'* Test sur la nature de la dmd  *
'*********************************

If Me.Check5.Value = 1 Then
    '-> R�cup�rer le code de la nature s�lectionn�e
    strTempo = UCase$(Entry(1, Entry(Me.List4.ListIndex + 1, ListeNatures, "|"), "�"))
    '-> R�cup�ration du code nature de la demande
    lpBuffer = Space$(100)
    Res = GetPrivateProfileString("DMD", "NATURE", "", lpBuffer, Len(lpBuffer), Path & FileName)
    If Res = 0 Then
        '-> Quitter
        Exit Sub
    Else
        '-> Comparer les 2 codes
        lpBuffer = UCase$(Mid$(lpBuffer, 1, Res))
        If lpBuffer <> strTempo Then Exit Sub
    End If 'Si on trouve le code nature de la demande
End If 'si on doit tester la nature

    
'***********************************
'* Test sur le num�ro de la nature *
'***********************************

If Me.Check1.Value = 1 Then
    '-> R�cup�rer le num�ro de la nature s�lectionn�e
    strTempo = UCase$(Trim(Me.Text1.Text))
    '-> R�cup�ration du N� nature de la demande
    lpBuffer = Space$(100)
    Res = GetPrivateProfileString("DMD", "N�", "", lpBuffer, Len(lpBuffer), Path & FileName)
    If Res = 0 Then
        '-> Quitter
        Exit Sub
    Else
        '-> Comparer les 2 codes
        lpBuffer = UCase$(Mid$(lpBuffer, 1, Res))
        If lpBuffer <> strTempo Then Exit Sub
    End If 'Si on trouve le N� nature de la demande
End If 'si on doit tester la N�

'***********************************
'* Test sur les dates mini et maxi *
'***********************************
'-> THIERRY
    
    
    
'*************************************
'* Ajout dans la grille de recherche *
'*************************************
    
'-> R�cup�ration du descriptif de la demande
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("DESCRIPTIF", "DESCRO", "", lpBuffer, Len(lpBuffer), Path & FileName)
Descriptif = UCase$(Mid$(lpBuffer, 1, Res))

'-> Ajouter dans le flexgrid
frmFindDmd.MSFlexGrid1.Rows = frmFindDmd.MSFlexGrid1.Rows + 1
frmFindDmd.MSFlexGrid1.Row = frmFindDmd.MSFlexGrid1.Rows - 1
frmFindDmd.MSFlexGrid1.RowHeight(frmFindDmd.MSFlexGrid1.Row) = Me.ScaleY(34, 3, 1)
frmFindDmd.MSFlexGrid1.Col = 0

'-> Ajout du num�ro de la demande
frmFindDmd.MSFlexGrid1.Text = Entry(1, FileName, ".")
frmFindDmd.MSFlexGrid1.CellForeColor = RGB(0, 0, 255)
frmFindDmd.MSFlexGrid1.CellFontUnderline = True
frmFindDmd.MSFlexGrid1.CellAlignment = 1

'-> Ajout de la bonne icone
frmFindDmd.MSFlexGrid1.Col = 1
If IsValidDmd Then
    Set frmFindDmd.MSFlexGrid1.CellPicture = LoadPicture(App.Path & "\bmp\ok_32x32.ico")
Else
    Set frmFindDmd.MSFlexGrid1.CellPicture = LoadPicture(App.Path & "\bmp\croix_32x32.ico")
End If

'-> Ajout du client
frmFindDmd.MSFlexGrid1.Col = 2
frmFindDmd.MSFlexGrid1.Text = " " & Client

'-> Ajout du progiciel
frmFindDmd.MSFlexGrid1.Col = 3
frmFindDmd.MSFlexGrid1.Text = Progiciel

'-> Ajout du descriptif
frmFindDmd.MSFlexGrid1.Col = 4
frmFindDmd.MSFlexGrid1.Text = Descriptif

'-> Ajout de la version
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("DMD", "VERSION", "", lpBuffer, Len(lpBuffer), Path & FileName)
frmFindDmd.MSFlexGrid1.Col = 5
frmFindDmd.MSFlexGrid1.Text = Mid$(lpBuffer, 1, Res)


End Sub

Private Sub Form_Load()

Dim Res As Long
Dim lpBuffer As String
Dim lpBuffer2 As String
Dim i As Integer
Dim IsError As Boolean

'-> Charger la liste des services
If ListeServices <> "" Then
    For i = 1 To NumEntries(ListeServices, "|")
        '-> Ajouter dans la liste
        Me.List1.AddItem Entry(2, Entry(i, ListeServices, "|"), "�")
    Next 'Pour tous les services
Else
    '-> Afficher un message d'erreur
    MsgBox "Impossible de charger la liste des services", vbCritical + vbOKOnly, "Erreur fatale"
    IsError = True
End If

'-> Charger la liste des Nature de demande
If ListeNatures <> "" Then
    For i = 1 To NumEntries(ListeNatures, "|")
        '-> Ajouter dans la liste
        Me.List4.AddItem Entry(2, Entry(i, ListeNatures, "|"), "�")
    Next 'Pour toutes les natures
Else
    '-> Afficher un message d'erreur
    MsgBox "Impossible de charger la liste des natures", vbCritical + vbOKOnly, "Erreur fatale"
    IsError = True
End If

'-> Charger la liste des progiciels
If ListeProgiciels <> "" Then
    For i = 1 To NumEntries(ListeProgiciels, "|")
        '-> Ajouter dans la liste
        Me.List2.AddItem Entry(2, Entry(i, ListeProgiciels, "|"), "�")
    Next 'Pour tous les services
Else
    '-> Afficher un message d'erreur
    MsgBox "Impossible de charger la liste des applications", vbCritical + vbOKOnly, "Erreur fatale"
    IsError = True
End If

'-> Charger la liste des versions
If ListeVersions <> "" Then
    For i = 1 To NumEntries(ListeVersions, "|")
        '-> Ajouter dans la liste
        Me.List5.AddItem Entry(2, Entry(i, ListeVersions, "|"), "�")
    Next 'Pour tous les services
Else
    '-> Afficher un message d'erreur
    MsgBox "Impossible de charger la liste des applications", vbCritical + vbOKOnly, "Erreur fatale"
    IsError = True
End If

'-> Charger la liste des clients
If ListeClients <> "" Then
    For i = 1 To NumEntries(ListeClients, "|")
        '-> Ajouter dans la liste
        Me.List3.AddItem Entry(1, Entry(i, ListeClients, "|"), "�") & " - " & GetLibel(Entry(1, Entry(i, ListeClients, "|"), "�"), 3)
    Next 'Pour tous les services
Else
    '-> Afficher un message d'erreur
    MsgBox "Impossible de charger la liste des clients", vbCritical + vbOKOnly, "Erreur fatale"
    IsError = True
End If


Me.Command1.Enabled = Not IsError

End Sub

