VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmDmd 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Demande : "
   ClientHeight    =   8400
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8415
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8400
   ScaleWidth      =   8415
   StartUpPosition =   1  'CenterOwner
   Begin TabDlg.SSTab SSTab1 
      Height          =   8295
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   14631
      _Version        =   393216
      Style           =   1
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "Demande"
      TabPicture(0)   =   "frmSdwDmd.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Text3"
      Tab(0).Control(1)=   "List1"
      Tab(0).Control(2)=   "Text1"
      Tab(0).Control(3)=   "Label1"
      Tab(0).Control(4)=   "Label2"
      Tab(0).Control(5)=   "Label3"
      Tab(0).Control(6)=   "Label4"
      Tab(0).Control(7)=   "Label5"
      Tab(0).Control(8)=   "Label6"
      Tab(0).Control(9)=   "Label8"
      Tab(0).Control(10)=   "Label7"
      Tab(0).Control(11)=   "Label9"
      Tab(0).Control(12)=   "Label10"
      Tab(0).Control(13)=   "Label11"
      Tab(0).Control(14)=   "Label13"
      Tab(0).Control(15)=   "Label12"
      Tab(0).Control(16)=   "Label15"
      Tab(0).Control(17)=   "Label16"
      Tab(0).ControlCount=   18
      TabCaption(1)   =   "Validation"
      TabPicture(1)   =   "frmSdwDmd.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Label17"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Label18"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Label19"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Label20"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "Label21"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "Text2"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "Command1"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).ControlCount=   7
      TabCaption(2)   =   "R�solution"
      TabPicture(2)   =   "frmSdwDmd.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      Begin VB.TextBox Text3 
         Height          =   1935
         Left            =   -73560
         MultiLine       =   -1  'True
         TabIndex        =   25
         Top             =   2400
         Width           =   6735
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Valider la demande"
         Height          =   495
         Left            =   6240
         TabIndex        =   19
         Top             =   7560
         Width           =   1935
      End
      Begin VB.TextBox Text2 
         Height          =   4815
         Left            =   240
         MultiLine       =   -1  'True
         TabIndex        =   18
         Top             =   2520
         Width           =   7935
      End
      Begin VB.ListBox List1 
         Height          =   1425
         Left            =   -73560
         TabIndex        =   2
         Top             =   4440
         Width           =   6735
      End
      Begin VB.TextBox Text1 
         Height          =   1815
         Left            =   -73560
         MultiLine       =   -1  'True
         TabIndex        =   1
         Top             =   6000
         Width           =   6735
      End
      Begin VB.Label Label21 
         Caption         =   "Commentaire : "
         Height          =   255
         Left            =   240
         TabIndex        =   24
         Top             =   2280
         Width           =   1815
      End
      Begin VB.Label Label20 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   1800
         Width           =   2535
      End
      Begin VB.Label Label19 
         Caption         =   "Par : "
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   1440
         Width           =   735
      End
      Begin VB.Label Label18 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   960
         Width           =   2415
      End
      Begin VB.Label Label17 
         Caption         =   "Date : "
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   720
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Demandeur : "
         Height          =   255
         Left            =   -74880
         TabIndex        =   17
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label2 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   -73560
         TabIndex        =   16
         Top             =   600
         Width           =   6735
      End
      Begin VB.Label Label3 
         Caption         =   "Service : "
         Height          =   255
         Left            =   -74880
         TabIndex        =   15
         Top             =   960
         Width           =   1215
      End
      Begin VB.Label Label4 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   -73560
         TabIndex        =   14
         Top             =   960
         Width           =   6735
      End
      Begin VB.Label Label5 
         Caption         =   "Nature : "
         Height          =   255
         Left            =   -74880
         TabIndex        =   13
         Top             =   1320
         Width           =   1215
      End
      Begin VB.Label Label6 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   -73560
         TabIndex        =   12
         Top             =   1320
         Width           =   4215
      End
      Begin VB.Label Label8 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   -69240
         TabIndex        =   11
         Top             =   1320
         Width           =   2415
      End
      Begin VB.Label Label7 
         Caption         =   "Client : "
         Height          =   255
         Left            =   -74880
         TabIndex        =   10
         Top             =   1680
         Width           =   1215
      End
      Begin VB.Label Label9 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   -73560
         TabIndex        =   9
         Top             =   1680
         Width           =   6735
      End
      Begin VB.Label Label10 
         Caption         =   "Progiciel : "
         Height          =   255
         Left            =   -74880
         TabIndex        =   8
         Top             =   2040
         Width           =   1215
      End
      Begin VB.Label Label11 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   -73560
         TabIndex        =   7
         Top             =   2040
         Width           =   4215
      End
      Begin VB.Label Label13 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   -69240
         TabIndex        =   6
         Top             =   2040
         Width           =   2415
      End
      Begin VB.Label Label12 
         Caption         =   "Descriptif : "
         Height          =   255
         Left            =   -74880
         TabIndex        =   5
         Top             =   2400
         Width           =   1215
      End
      Begin VB.Label Label15 
         Caption         =   "Fichiers joints : "
         Height          =   255
         Left            =   -74880
         TabIndex        =   4
         Top             =   4440
         Width           =   1215
      End
      Begin VB.Label Label16 
         Caption         =   "D�lais : "
         Height          =   255
         Left            =   -74880
         TabIndex        =   3
         Top             =   6000
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmDmd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public DmdFile As String
Public Version As String
Public BrowseLig As Integer

Dim IsDmdValid As Boolean 'Indique si la demande est valid�e
Dim DmdFichier As String 'Nom du fichier demande

Public Sub LoadDmd()

'---> Cette proc�dure charge le descriptif d'une demande

Dim Res As Long
Dim lpBuffer As String


If Me.DmdFile = "" Then
    Unload Me
    Exit Sub
End If

'-> Setting du fichier � analyser
DmdFichier = App.Path & "\demandes\" & Version & "\dmd\" & DmdFile & ".dmd"


Me.Caption = Me.Caption & DmdFile

'-> V�rifier que l'on trouve le fichier demande dans un premier temps
If Dir$(DmdFichier, vbNormal) = "" Then
    MsgBox "Impossible de trouver le fichier demande", vbCritical + vbOKOnly, "Erreur"
    Unload Me
    Exit Sub
End If

'-> Demandeur
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("DMD", "NAME", "", lpBuffer, Len(lpBuffer), DmdFichier)
Me.Label2.Caption = Mid$(lpBuffer, 1, Res)

'-> Service
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("DMD", "SERVICE", "", lpBuffer, Len(lpBuffer), DmdFichier)
Me.Label4.Caption = GetLibel(Mid$(lpBuffer, 1, Res), 0)

'-> Nature
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("DMD", "NATURE", "", lpBuffer, Len(lpBuffer), DmdFichier)
Me.Label6.Caption = GetLibel(Mid$(lpBuffer, 1, Res), 2)

'-> N�
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("DMD", "N�", "", lpBuffer, Len(lpBuffer), DmdFichier)
Me.Label8.Caption = Mid$(lpBuffer, 1, Res)

'-> Client
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("DMD", "CLIENT", "", lpBuffer, Len(lpBuffer), DmdFichier)
Me.Label9.Caption = Mid$(lpBuffer, 1, Res) & " -" & GetLibel(Mid$(lpBuffer, 1, Res), 3)

'-> Progiciel
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("DMD", "PROGICIEL", "", lpBuffer, Len(lpBuffer), DmdFichier)
Me.Label11.Caption = GetLibel(Mid$(lpBuffer, 1, Res), 1)

'-> Version
Me.Label13.Caption = Version

'-> Descriptif
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("DESCRIPTIF", "DESCRO", "", lpBuffer, Len(lpBuffer), DmdFichier)
Me.Text3.Text = Mid$(lpBuffer, 1, Res)

'-> D�lais
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("DELAI", "DELAI", "", lpBuffer, Len(lpBuffer), DmdFichier)
Me.Text1.Text = Mid$(lpBuffer, 1, Res)

'-> Fichiers joints
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("DMD", "FILES", "", lpBuffer, Len(lpBuffer), DmdFichier)
If Res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Res)
    For i = 1 To NumEntries(lpBuffer, "|")
        Me.List1.AddItem Entry(i, lpBuffer, "|")
    Next
End If

'-> Validation
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("DMD", "VALID", "", lpBuffer, Len(lpBuffer), DmdFichier)
lpBuffer = Mid$(lpBuffer, 1, Res)
If lpBuffer = "0" Then
    '-> Si le user est autoris� : afficher le bouton de validation
    Me.Command1.Visible = UserValidDmd
Else
    '-> Indiquer que la demande est valid�e
    IsDmdValid = True
    '-> Le fichier est valid�
    Me.Command1.Visible = False
    
    '-> Op�rateur de validation
    lpBuffer = Space$(2000)
    Res = GetPrivateProfileString("DMD", "VALIDOPERAT", "", lpBuffer, Len(lpBuffer), DmdFichier)
    If Res <> 0 Then Me.Label20.Caption = Mid$(lpBuffer, 1, Res)
    
    '-> Date de validation
    lpBuffer = Space$(2000)
    Res = GetPrivateProfileString("DMD", "VALIDDATE", "", lpBuffer, Len(lpBuffer), DmdFichier)
    If Res <> 0 Then Me.Label18.Caption = Mid$(lpBuffer, 1, Res)
    
    '-> Commentaire
    lpBuffer = Space$(2000)
    Res = GetPrivateProfileString("DMD", "VALIDCOM", "", lpBuffer, Len(lpBuffer), DmdFichier)
    If Res <> 0 Then Me.Text2.Text = Mid$(lpBuffer, 1, Res)
    
End If

'-> Activer le premier onglet
Me.SSTab1.Tab = 0

End Sub


Private Sub Command1_Click()

'---> Cette proc�dure valide la demande

Dim Rep As VbMsgBoxResult
Dim Res As Long

Rep = MsgBox("Valider la demande ?", vbQuestion + vbYesNo, "Confirmation")
If Rep = vbNo Then Exit Sub

'-> Mettre � jour le fichier demande
Res = WritePrivateProfileString("DMD", "VALID", "1", DmdFichier)
Res = WritePrivateProfileString("DMD", "VALIDOPERAT", UserName, DmdFichier)
Me.Label20.Caption = UserName
Res = WritePrivateProfileString("DMD", "VALIDDAT", UserName, DmdFichier)
Me.Label18.Caption = Now
Res = WritePrivateProfileString("DMD", "VALIDCOM", Me.Text2.Text, DmdFichier)

'-> Modifier l'icone dans la feuille de recherche des demandes
frmFindDmd.MSFlexGrid1.Col = 1
frmFindDmd.MSFlexGrid1.Row = Me.BrowseLig
Set frmFindDmd.MSFlexGrid1.CellPicture = LoadPicture(App.Path & "\bmp\ok_32x32.ico")

'-> Indiquer que la demande est valid�e
IsDmdValid = True

End Sub

Private Sub Form_Load()

'-> bloquer l'onglet de r�solution
Me.SSTab1.TabEnabled(2) = False

End Sub


Private Sub Text1_KeyPress(KeyAscii As Integer)

KeyAscii = 0

End Sub


Private Sub Text2_KeyPress(KeyAscii As Integer)
If IsDmdValid Then
    KeyAscii = 0
Else
    If KeyAscii = 13 Then KeyAscii = 0
End If

End Sub

Private Sub Text3_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub
