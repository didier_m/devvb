VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmNewMessage 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cr�ation d'un nouveau message"
   ClientHeight    =   5385
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8625
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5385
   ScaleWidth      =   8625
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command1 
      Caption         =   "Transf�rer"
      Height          =   375
      Left            =   6480
      TabIndex        =   10
      Top             =   4800
      Width           =   2055
   End
   Begin VB.TextBox strMessage 
      Height          =   285
      Left            =   360
      MaxLength       =   250
      TabIndex        =   9
      Top             =   4320
      Width           =   8175
   End
   Begin MSComctlLib.ImageCombo ImageCombo1 
      Height          =   570
      Left            =   4560
      TabIndex        =   7
      Top             =   3120
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   1005
      _Version        =   393216
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Text            =   "ImageCombo1"
      ImageList       =   "ImageList1"
   End
   Begin VB.ListBox List3 
      Height          =   645
      Left            =   360
      Sorted          =   -1  'True
      TabIndex        =   5
      Top             =   3120
      Width           =   3975
   End
   Begin VB.ListBox List2 
      Height          =   2010
      Left            =   4440
      Sorted          =   -1  'True
      TabIndex        =   3
      Top             =   600
      Width           =   3975
   End
   Begin VB.ListBox List1 
      Height          =   2010
      Left            =   360
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   600
      Width           =   3975
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7920
      Top             =   3240
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNewMessage.frx":0000
            Key             =   ""
            Object.Tag             =   "Fran�ais"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNewMessage.frx":0452
            Key             =   ""
            Object.Tag             =   "Anglais"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNewMessage.frx":08A4
            Key             =   ""
            Object.Tag             =   "Allemand"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNewMessage.frx":0CF6
            Key             =   ""
            Object.Tag             =   "Portugais"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNewMessage.frx":1010
            Key             =   ""
            Object.Tag             =   "Espagnol"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNewMessage.frx":1462
            Key             =   ""
            Object.Tag             =   "Italien"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblMess 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   " En attente de la r�ponse du serveur ..............."
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   360
      TabIndex        =   11
      Top             =   4800
      Visible         =   0   'False
      Width           =   5535
   End
   Begin VB.Label Label5 
      Caption         =   "Libell� du message : "
      Height          =   255
      Left            =   360
      TabIndex        =   8
      Top             =   3960
      Width           =   2295
   End
   Begin VB.Label Label4 
      Caption         =   "Choix de la langue : "
      Height          =   255
      Left            =   4560
      TabIndex        =   6
      Top             =   2760
      Width           =   1575
   End
   Begin VB.Label Label3 
      Caption         =   "Choix de la version : "
      Height          =   255
      Left            =   360
      TabIndex        =   4
      Top             =   2760
      Width           =   1935
   End
   Begin VB.Label Label2 
      Caption         =   "Choix d'un applicatif : "
      Height          =   255
      Left            =   4560
      TabIndex        =   2
      Top             =   240
      Width           =   3735
   End
   Begin VB.Label Label1 
      Caption         =   "S�lection d'un client : "
      Height          =   255
      Left            =   360
      TabIndex        =   0
      Top             =   240
      Width           =   1815
   End
   Begin VB.Shape Shape1 
      Height          =   5295
      Left            =   60
      Top             =   60
      Width           =   8535
   End
End
Attribute VB_Name = "frmNewMessage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()

Dim hdlFile As Integer
Dim DmdFileName As String
Dim TempoFileName As String
Dim RspFileName As String
Dim Ligne As String
Dim strTempo As String

'-> Faire les v�rifications
If Me.List1.ListIndex = -1 Then
    MsgBox "Veuillez s�lectionner un client", vbCritical + vbOKOnly, "Erreur"
    Me.List1.SetFocus
    Exit Sub
End If

If Me.List2.ListIndex = -1 Then
    MsgBox "Veuillez s�lectionner un applicatif", vbCritical + vbOKOnly, "Erreur"
    Me.List2.SetFocus
    Exit Sub
End If

If Me.List3.ListIndex = -1 Then
    MsgBox "Veuillez s�lectionner une version", vbCritical + vbOKOnly, "Erreur"
    Me.List3.SetFocus
    Exit Sub
End If

If Me.ImageCombo1.SelectedItem Is Nothing Then
    MsgBox "Veuillez s�lectionner une langue", vbCritical + vbOKOnly, "Erreur"
    Me.ImageCombo1.SetFocus
    Exit Sub
End If

'-> V�rifier qu'un message est saisi
If Trim(Me.strMessage) = "" Then
    MsgBox "Veuillez saisir un libell�", vbCritical + vbOKOnly, "Erreur"
    Me.strMessage.SetFocus
    Exit Sub
End If

'-> Cr�ation des noms de fichier
DmdFileName = App.Path & "\messages\trans\" & ComputerName & "_" & Me.hwnd & ".dmd"
TempoFileName = App.Path & "\temp\" & ComputerName & "_" & Me.hwnd & ".tmp"
RspFileName = App.Path & "\messages\trans\" & ComputerName & "_" & Me.hwnd & ".rsp"

'-> V�rifier qu'il n'y a pas une demande ou une r�ponse qui traine
If Dir$(DmdFileName, vbNormal) <> "" Then Kill DmdFileName
If Dir$(RspFileName, vbNormal) <> "" Then Kill RspFileName

'-> Cr�ation d'un fichier tempo
hdlFile = FreeFile
Open TempoFileName For Output As #hdlFile

'-> Cr�ation de la demande
Print #hdlFile, "MESS_CREATE~" & Me.ImageCombo1.SelectedItem.Index & "~" & Trim(Entry(1, Me.List1.Text, "-")) & "~" & Me.List3.Text & "~" & Trim(Entry(1, Me.List2.Text, "-")) & "~" & Trim(Me.strMessage.Text)
Close #hdlFile

'-> Faire un rename dans le r�pertoire de transfert
Name TempoFileName As DmdFileName

'-> bloquer l'interface
MdiMain.Enabled = False

'-> Modifier le pointeur
Me.MousePointer = 11

'-> Afficher un message d'attente
lblMess.Visible = True

'-> Se mettre en attente de la r�ponse
Do
    '-> Rendre la main � la CPU
    DoEvents
    '-> V�rifier si on trouve le fichier r�ponse
    If Dir$(RspFileName, vbNormal) <> "" Then Exit Do
Loop

'-> Lire la r�ponse
hdlFile = FreeFile
Open RspFileName For Input As #hdlFile
Line Input #hdlFile, Ligne
Close #hdlFile

'-> Supprimer le fichier RSP
Kill RspFileName

'-> Supprimer le message d'attente
Me.lblMess.Visible = False

'-> Cr�er la r�ponse
If Entry(2, Ligne, "~") = "OK" Then
    strTempo = "Message cr�� --> OK" & Chr(13) & Entry(3, Ligne, "~")
Else
    strTempo = "Erreur lors de la cr�ation du message " & Chr(13) & Entry(3, Ligne, "~")
End If

'-> Afficher la r�ponse
MsgBox strTempo
    
'-> Supprimer le message
Me.strMessage = ""

'-> Rendre le focus
MdiMain.Enabled = True
Me.strMessage.SetFocus

'-> Modifier le pointeur
Me.MousePointer = 0


End Sub

Private Sub Form_Load()

Dim i As Integer, j As Integer
Dim FindApp As Boolean

'-> Charger la liste des clients
For i = 1 To NumEntries(frmMess.ListeClient, "|")
    Me.List1.AddItem Entry(i, frmMess.ListeClient, "|") & " - " & Entry(i, frmMess.LibClient, "|")
    If UCase$(Entry(i, frmMess.ListeClient, "|")) = "DEAL" Then Me.List1.Selected(Me.List1.ListCount - 1) = True
Next

'-> Charger la liste des applicatifs
For i = 1 To NumEntries(frmMess.ListeApp, "|")
    '-> Ne charger que les applicatifs autoris�s pour cet user
    FindApp = False
    For j = 1 To NumEntries(UserApp, "|")
        If UCase$(Entry(i, frmMess.ListeApp, "|")) = UCase$(Entry(j, UserApp, "|")) Then
            FindApp = True
            Exit For
        End If
    Next
    If FindApp Then Me.List2.AddItem Entry(i, frmMess.ListeApp, "|") & " - " & Entry(i, frmMess.LibApp, "|")
Next

'-> S�lectionner le premier �l�ment
Me.List2.Selected(0) = True

'-> Charger la liste des versions
For i = 1 To NumEntries(frmMess.ListeVersion, "|")
    Me.List3.AddItem Entry(i, frmMess.ListeVersion, "|")
Next

'-> S�lectionner le premier �l�ment
Me.List3.Selected(0) = True

For i = 1 To 6
    Me.ImageCombo1.ComboItems.Add i, , Me.ImageList1.ListImages(i).Tag, i
Next
Me.ImageCombo1.ComboItems(1).Selected = True

'-> Indiquer que la feuille est visible
frmMess.IsFrmNewMess = True

End Sub

Private Sub Form_Unload(Cancel As Integer)

'-> Indiquer que la feuille n'est plus visible
frmMess.IsFrmNewMess = False


End Sub
