VERSION 5.00
Begin VB.MDIForm MdiMain 
   BackColor       =   &H8000000C&
   Caption         =   "Studio de d�veloppement Internet"
   ClientHeight    =   4935
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   8760
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.Menu mnuFichier 
      Caption         =   "&Fichier"
      Begin VB.Menu mnuExit 
         Caption         =   "Quitter"
      End
   End
   Begin VB.Menu mnuDmd 
      Caption         =   "Demandes"
      Visible         =   0   'False
      Begin VB.Menu mnuFindDMD 
         Caption         =   "Consulter les demandes"
      End
      Begin VB.Menu mnuNewDmd 
         Caption         =   "Nouvelle Demande"
      End
   End
   Begin VB.Menu mnuMessages 
      Caption         =   "Messages"
      Begin VB.Menu mnuGestMess 
         Caption         =   "Gestion des messages"
      End
   End
   Begin VB.Menu mnuWWW 
      Caption         =   "Internet"
      Begin VB.Menu mnuSiteDev 
         Caption         =   "Acc�s au site de d�vellopement"
      End
   End
End
Attribute VB_Name = "MdiMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private ModeFind  As Integer '-> 1 indique qu'il faut afficher la fen�tre de recherche et retourner une valeur

Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)

Dim Rep As VbMsgBoxResult

Rep = MsgBox("Quitter maintenant ?", vbQuestion + vbYesNo, "Quitter")
If Rep = vbNo Then
    Cancel = 1
    Exit Sub
End If

End


End Sub

Private Sub mnuExit_Click()

Unload Me

End Sub

Private Sub mnuFindDMD_Click()

'-> Afficher la feuille de recherche des demandes
frmFindDmd.Show
frmFindDmd.ZOrder


End Sub

Private Sub mnuGestMess_Click()

'-> Afficher le gestionnaire des messages
frmMess.Show

End Sub

Private Sub mnuNewDmd_Click()

If Dir$(App.Path & "\GestDmd.exe", vbNormal) = "" Then
    MsgBox "Impossible de cr�er une nouvelle demande : programme introuvable", vbCritical + vbOKOnly, "Erreur"
    Exit Sub
Else
    Shell App.Path & "\GestDmd.exe", vbNormalFocus
End If

End Sub

Private Sub mnuSiteDev_Click()

frmWWW.Show
frmWWW.ZOrder

End Sub
