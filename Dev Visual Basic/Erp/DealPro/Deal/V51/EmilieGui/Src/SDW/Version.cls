VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Version"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'-> Code de la version
Public IdVersion As String
'-> Liste des clients pour cette version
Public Clients As Collection

Private Sub Class_Initialize()

'-> Initialiser la liste des clients par version
Set Clients = New Collection

End Sub
