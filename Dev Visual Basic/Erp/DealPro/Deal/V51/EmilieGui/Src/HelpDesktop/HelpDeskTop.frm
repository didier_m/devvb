VERSION 5.00
Begin VB.MDIForm HelpDeskTop 
   BackColor       =   &H8000000C&
   Caption         =   "HelpDeskTop"
   ClientHeight    =   4455
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   7650
   Icon            =   "HelpDeskTop.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Menu mnuFichier 
      Caption         =   "&Fichier"
      Begin VB.Menu mnuSave 
         Caption         =   "Enregistrer"
         Shortcut        =   ^S
      End
      Begin VB.Menu MnuQuitter 
         Caption         =   "Quitter"
         Shortcut        =   ^Q
      End
   End
   Begin VB.Menu mnuEdition 
      Caption         =   "&Edition"
      Visible         =   0   'False
      Begin VB.Menu mnuCopy 
         Caption         =   "Copier"
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuPaste 
         Caption         =   "Coller"
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuDel 
         Caption         =   "Supprimer"
         Shortcut        =   {DEL}
      End
      Begin VB.Menu mnuSepEdit 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFind 
         Caption         =   "Rechercher ..."
         Shortcut        =   {F3}
      End
      Begin VB.Menu mnuSep34 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAddChapitre 
         Caption         =   "Ajouter un chapitre"
      End
      Begin VB.Menu mnuAddDoc 
         Caption         =   "Ajouter un document d'aide"
      End
      Begin VB.Menu mnuModifier 
         Caption         =   "Modifier"
      End
      Begin VB.Menu mnuSep12345 
         Caption         =   "-"
      End
      Begin VB.Menu mnuKeyWords 
         Caption         =   "Ouvrir le catalogue des mots cl�s"
      End
      Begin VB.Menu mnuSep34567 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPrintRub 
         Caption         =   "Imprimer la liste des rubriques"
      End
      Begin VB.Menu mnuPrintDoc 
         Caption         =   "Imprimer la documentation"
      End
   End
   Begin VB.Menu mnuAffichage 
      Caption         =   "&Affichage"
      Begin VB.Menu mnuToolBar 
         Caption         =   "Barres d'outils"
         Begin VB.Menu mnuDisplayToolBar 
            Caption         =   "Langue"
            Index           =   0
         End
      End
      Begin VB.Menu mnuPoubelle 
         Caption         =   "Poubelle"
      End
      Begin VB.Menu mnuListeClient 
         Caption         =   "Liste des clients"
      End
      Begin VB.Menu mnuPropClient 
         Caption         =   "Propri�t�s du client en cours"
      End
      Begin VB.Menu mnuListeApp 
         Caption         =   "Liste des applicatifs"
      End
      Begin VB.Menu mnuAnnClient 
         Caption         =   "Annotations documentation Deal"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuPropAffichage 
         Caption         =   "Propri�t�s de l'affichage en cours"
      End
   End
   Begin VB.Menu mnuOutils 
      Caption         =   "Outils"
      Begin VB.Menu mnuTraduction 
         Caption         =   "Traduction"
      End
   End
   Begin VB.Menu mnuFenetre 
      Caption         =   "Fen�tre"
      Begin VB.Menu mnuFenetreH 
         Caption         =   "Mosa�que horizontale"
      End
      Begin VB.Menu mnuFenetreV 
         Caption         =   "Mosa�que verticale"
      End
      Begin VB.Menu mnuCascade 
         Caption         =   "En cascade"
      End
      Begin VB.Menu mnuReorganise 
         Caption         =   "R�organiser les icones"
      End
      Begin VB.Menu mnuSepI 
         Caption         =   "-"
      End
      Begin VB.Menu mnuListProgi 
         Caption         =   ""
         Index           =   0
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFenetrePoubelle 
         Caption         =   "Poubelle"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFenetreListeCli 
         Caption         =   "Liste des clients"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFenetreListeApp 
         Caption         =   "Liste des applicatifs"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFenetreWord 
         Caption         =   "Microsoft Word"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFenetreTraduction 
         Caption         =   "Traduction"
         Visible         =   0   'False
      End
   End
   Begin VB.Menu mnuClient 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu mnuLoadClient 
         Caption         =   "Charger le client"
      End
   End
   Begin VB.Menu mnufrmPoubelle 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu mnuClearBasket 
         Caption         =   "Vider la poubelle ..."
      End
      Begin VB.Menu mnuDelItemPoubelle 
         Caption         =   "Supprimer l'�l�ment s�lectionn�"
      End
      Begin VB.Menu mnuCopierBasKet 
         Caption         =   "Copier"
      End
   End
   Begin VB.Menu mnutoto 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu mnuSousTOTO 
         Caption         =   "bidon"
      End
   End
   Begin VB.Menu Aide 
      Caption         =   "?"
      Begin VB.Menu HelpDeal 
         Caption         =   "AIde en ligne"
      End
   End
End
Attribute VB_Name = "HelpDeskTop"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub HelpDeal_Click()


Shell App.Path & "\HelpDeal.exe DEAL|1|DEAL|HELPDESKTOP", vbNormalFocus


End Sub

Private Sub MDIForm_Load()

'-> Intialisation des variables de positionnement
GetBordures
'-> Interdire la fermeture des barres d'outils
IsTerminate = False

'-> Cr�ation des propaths
If InStr(1, UCase$(App.Path), "EMILIEGUI\SRC") <> 0 Then
    IniPath = "E:\Erp\Turbo"
Else
    CreatePath IniPath
End If

'-> Construire le chemin complet
IniPath = IniPath & "\TurboMaq.ini"

'-> Quitter si on ne trouve pas ce fichier
If Dir$(IniPath, vbNormal) = "" Then
    MsgBox "Impossible de trouver le fichier " & Chr(13) & IniPath, vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

'-> R�cup�ration des valeurs des mots cl�s
GetPathMotCles

'-> Initialiser les collections
Set Clients = New Collection
Set Applicatifs = New Collection

'-> Au d�marrage de la feuille , charger toutes les barres d'outils
Load frmLangue
'-> Clients
Load frmPropClient

'-> En faire des fen�tres filles de la feuille MDI
SetParent frmLangue.hWnd, Me.hWnd
SetParent frmPropClient.hWnd, Me.hWnd

'-> Code langue par d�faut Fran�ais
CurCodeLangue = 1
frmLangue.Toolbar1.Buttons(CurCodeLangue).Value = tbrPressed

'-> Initialisation des traductions
InitDefautTraduction

End Sub
Private Sub ClearPoubelle()

'-> Supprimer la poubelle
frmPoubelle.TreeView1.Nodes.Clear

'-> Vider le menu aussi
Do While frmPoubelle.Menus.Count <> 0
    frmPoubelle.Menus.Remove (1)
Loop

'-> Supprimer la liste des liens
Do While frmPoubelle.Liens.Count <> 0
    frmPoubelle.Liens.Remove (1)
Loop


End Sub


Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)

Dim Rep

Rep = MsgBox("Quitter maintenant ?", vbYesNo + vbQuestion, "Quitter")
If Rep = vbNo Then
    Cancel = 1
    Exit Sub
End If

'-> Lancer l'enregistrement
LaunchSave

'-> Autoriser la fermeture des barres d'outils
IsTerminate = True

'-> Fermer toutes les fen�tres filles
Unload frmLangue
Unload frmListeClient
Unload frmPropClient
Unload frmListeApp
Unload frmPoubelle
Unload frmKeyWords

'-> Terminer
End

End Sub


Private Sub mnuAddChapitre_Click()

'---> Cette proc�dure ajoute un chapitre dans la documentation en cours
Set frmInsertChapitre.NodeChapitre = Nothing
Set frmInsertChapitre.Keyword = Nothing
frmInsertChapitre.Show vbModal

End Sub

Private Sub mnuAddDoc_Click()

'---> Cette proc�dure ajoute un chapitre dans la documentation en cours
Set frmInsertDoc.NodeDoc = Nothing

frmInsertDoc.Show vbModal

End Sub

Private Sub mnuAffichage_Click()

If Trim(CurClient) = "" Then
    Me.mnuPropClient.Enabled = False
    Me.mnuListeApp.Enabled = False
    Me.mnuPoubelle.Enabled = False
    For i = 0 To Me.mnuDisplayToolBar.Count - 1
        Me.mnuDisplayToolBar(i).Enabled = False
    Next
    Me.mnuPropAffichage.Enabled = False
Else
    Me.mnuPropClient.Enabled = True
    Me.mnuListeApp.Enabled = True
    Me.mnuPoubelle.Enabled = True
    For i = 0 To Me.mnuDisplayToolBar.Count - 1
        Me.mnuDisplayToolBar(i).Enabled = True
    Next
    Me.mnuPropAffichage.Enabled = True
End If

End Sub


Private Sub mnuCascade_Click()
Me.Arrange 0
End Sub

Private Sub mnuClearBasket_Click()

Dim Rep As Integer

'-> Demander confirmation
Rep = MsgBox("Vider la poubelle maintenant ? ( les donn�es ne seront pas r�cup�rables ) ", vbExclamation + vbYesNo, "Confirmation")
If Rep = vbNo Then Exit Sub

ClearPoubelle

End Sub

Private Sub mnuCopierBasKet_Click()

Set CopyNode = frmPoubelle.TreeView1.SelectedItem
AppCopy = "POUBELLE"


End Sub

Public Sub mnuCopy_Click()

Dim aApp As Applicatif

'-> Pointer sur l'applicatif
Set aApp = Applicatifs(UCase$(CurApp))

Set CopyNode = aApp.Tree.SelectedItem
AppCopy = CurApp


End Sub

Public Sub mnuDel_Click()

Dim Res As Integer

Res = GetKeyState(VK_SHIFT)

'-> V�rifier si le  bit 7 ( 8eme Bit ) est activ�
If Res And &H80 Then
    frmDEl.intSup = 1
Else
    frmDEl.intSup = 0
End If
frmDEl.lbApp = CurApp

frmDEl.Show vbModal

End Sub

Private Sub mnuDelItemPoubelle_Click()

'---> Supprimer un �l�ment de la poubelle


If Not frmPoubelle.TreeView1.SelectedItem Is Nothing Then
    '-> Tester la nature de l'�l�ment
    Select Case frmPoubelle.TreeView1.SelectedItem.Tag
        Case "CHAPITRE", "DOCUMENT"
            Set SourceNode = frmPoubelle.TreeView1.SelectedItem
            frmDEl.intSup = 1
            frmDEl.lbApp = "POUBELLE"
            frmDEl.Show vbModal
        
    End Select
End If 'Si un �l�ment est s�lectionn�

End Sub

Private Sub mnuDisplayToolBar_Click(Index As Integer)

Dim aForm As Form

Select Case Index

   
    Case 0
        Set aForm = frmLangue
        
End Select

'---> Affiche ou masque la barre d'outils
If mnuDisplayToolBar(Index).Checked = True Then
    '-> Masquer la barre d'outils
    aForm.Hide
    '-> Decocher la case
    mnuDisplayToolBar(Index).Checked = False
Else
    aForm.Show
    SetWindowPos aForm.hWnd, 0, 0, 0, aForm.ScaleX(aForm.Left, 1, 3), aForm.ScaleY(aForm.Height, 1, 3), SWP_NOSIZE&
    
    mnuDisplayToolBar(Index).Checked = True
End If


End Sub


Private Sub mnuFenetreH_Click()
    Me.Arrange 1
End Sub

Private Sub mnuFenetreListeApp_Click()
    frmListeApp.ZOrder
End Sub

Private Sub mnuFenetreListeCli_Click()
    frmListeClient.ZOrder
End Sub

Private Sub mnuFenetrePoubelle_Click()
    frmPoubelle.ZOrder
End Sub

Private Sub mnuFenetreV_Click()
Me.Arrange 2
End Sub

Private Sub mnuFind_Click()
    frmSearch.Show vbModal
End Sub

Private Sub mnuKeyWords_Click()

'---> Afficher le catalogue
frmKeyWords.Show vbModal


End Sub

Private Sub mnuListeApp_Click()

frmListeApp.Visible = Not Me.mnuListeApp.Checked
Me.mnuListeApp.Checked = frmListeApp.Visible
Me.mnuFenetreListeApp.Visible = frmListeApp.Visible

End Sub

Private Sub mnuListeClient_Click()

frmListeClient.Visible = Not Me.mnuListeClient.Checked
Me.mnuListeClient.Checked = frmListeClient.Visible
Me.mnuFenetreListeCli.Visible = frmListeClient.Visible

End Sub

Private Sub mnuListProgi_Click(Index As Integer)

Dim strIdent As String
Dim aApp As Applicatif

'-> V�rifier la feuille que l'on affiche
Select Case UCase$(mnuListProgi(Index).Caption)

    Case "POUBELLE"
        frmPoubelle.ZOrder
    
    Case "LISTE DES APPLICATIFS"
        frmListeApp.ZOrder
    
    Case "LISTE DES CLIENTS"
        frmListeClient.ZOrder
    
    Case Else

        '-> R�cup�ration de l'ident du progiciel
        strApp = UCase$(Trim(Entry(1, mnuListProgi(Index).Caption, "-")))
        
        '-> Pointer sur l'applicatif
        Set aApp = Applicatifs(UCase$(strApp))
        
        '-> Mettre au premier plan la feuille
        BringWindowToTop aApp.HwndForm
        
        Set aApp = Nothing

End Select


End Sub

Private Sub mnuLoadClient_Click()

Dim strIdent As String
Dim aNode As Node

'-> Pointer sur le node enfant
Set aNode = frmListeClient.TreeView1.SelectedItem.Child
strIdent = UCase$(Trim(Entry(2, aNode.Text, ":")))

'---> Cette proc�dure change de client
If Trim(CurClient) <> "" Then
    '-> Tester si le nouveau client est le m�me que l'ancien
    If CurClient = strIdent Then Exit Sub
    
    '-> Lancer l'enregistrement
    LaunchSave
    
    '-> Indiquer que l'on peut fermer les fen�tres
    IsTerminate = True
    
    '-> Fermer toutes les feuilles
    For i = 1 To Applicatifs.Count
        If IsWindow(Applicatifs(i).HwndForm) Then SendMessage Applicatifs(i).HwndForm, WM_CLOSE, 0, 0
        'DestroyWindow& Applicatifs(i).HwndForm
    Next
    
    '-> Vider les collections de mots cl�s et de contextes
    frmKeyWords.ClearKeyWords
            
End If

'-> Positionner la variable globale sur le code du client en cours
CurClient = strIdent
'-> Positionner la variable de path
DocPath = Clients(UCase$(CurClient)).Serveur

'-> Charger la feuille des mots cl�s
Load frmKeyWords

'-> ICI Chargement des fichiers GLOBAL.DAT , KEY.DAT , CT.DAT du client s�lectionn�
LoadGlobalFile

'-> Mettre � jour le titre de l'application
Me.Caption = "HelpDeskTop - [ " & CurClient & " - " & frmListeClient.TreeView1.SelectedItem & " ] "

'-> Ouvrir la poubelle
Load frmPoubelle
mnuFenetrePoubelle.Visible = True
Me.mnuPoubelle.Checked = True

'-> Afficher la liste des applicatifs si ce n'est le cas
mnuListeApp_Click
mnuListeApp.Visible = True

'-> Mettre la liste des applicatifs au niveau + 1
frmListeApp.Visible = True
frmListeApp.ZOrder
frmListeApp.SetFocus

'-> Ne pas fermer les fen�tres
IsTerminate = False

'-> Vider la poubelle
ClearPoubelle

'-> D�selectionner les sources de D&D
Set SourceNode = Nothing
Set CopyNode = Nothing
CurApp = ""
AppDrag = ""
AppCopy = ""

End Sub


Private Sub mnuModifier_Click()

'---> Modifier le node s�lectionn�

Dim aApp As Applicatif
Dim aNode As Node
Dim Rep
Dim Invite As String

'-> Pointer sur l'applicatif
Set aApp = Applicatifs(UCase$(CurApp))

'-> Selon le node � modifier
Set aNode = aApp.Tree.SelectedItem

If aNode.Tag = "CHAPITRE" Then
    Set frmInsertChapitre.NodeChapitre = aNode
    Set frmInsertChapitre.Keyword = Nothing
    frmInsertChapitre.Caption = "Modification d'un chapitre"
    frmInsertChapitre.Show vbModal
Else
    Set frmInsertDoc.NodeDoc = aNode
    frmInsertDoc.Caption = "Modification d'un document d'aide"
    frmInsertDoc.Show vbModal
End If

End Sub

Private Sub mnuOutils_Click()

'-> Ne pouvoir ouvrir l'outil de trduction que si un applicatif est en cours
If CurApp = "" Then
    Me.mnuTraduction.Enabled = False
Else
    Me.mnuTraduction.Enabled = True
End If

End Sub

Public Sub mnuPaste_Click()

Dim Res As Integer
Dim aMode As Integer
Dim aApp As Applicatif

'-> V�rifier si la touche Shift est enfonc�e
Res = GetKeyState(VK_SHIFT)
If Res And &H80 Then
    '-> Il faire un couper
    aMode = 2
Else
    '-> Sinon faire un simple copier
    aMode = 1
End If

'-> Pointer sur l'applicatif
Set aApp = Applicatifs(CurApp)

'-> Lancer la feuille du choix s'il y a un node
If aApp.Tree.Nodes.Count = 0 Then
    AnalyseNode AppCopy, CopyNode, aMode, CurApp, Nothing, 4
Else
    '-> Afficher la feuille de choix
    RetourChx = -1
    frmChxDest.Show vbModal
    If RetourChx <> -1 Then _
        AnalyseNode AppCopy, CopyNode, aMode, CurApp, aApp.Tree.SelectedItem, RetourChx
End If


End Sub

Private Sub mnuPoubelle_Click()

frmPoubelle.Visible = Not Me.mnuPoubelle.Checked
Me.mnuPoubelle.Checked = frmPoubelle.Visible
Me.mnuFenetrePoubelle.Visible = frmPoubelle.Visible

End Sub



Private Sub mnuPrintDoc_Click()

frmPrint.TypeImp = 0
frmPrint.Show vbModal

End Sub

Private Sub mnuPrintRub_Click()

frmPrint.TypeImp = 1
frmPrint.Show vbModal


End Sub

Private Sub mnuPropAffichage_Click()

Dim aClient As Client

'---> Afficher la feuille de propri�t� de l'affichage en cours

Set aClient = Clients(UCase$(CurClient))

frmAffichage.Progiciel.Text = CurApp
frmAffichage.Client.Text = aClient.Ident & " - " & aClient.Libel
frmAffichage.Image1.Picture = frmLangue.ImageList1.ListImages(CurCodeLangue).Picture
frmAffichage.Label6.Caption = frmLangue.ImageList1.ListImages(CurCodeLangue).Tag
frmAffichage.Show vbModal

End Sub

Private Sub mnuPropClient_Click()

Dim aClient As Client

frmPropClient.Visible = Not Me.mnuPropClient.Checked
Me.mnuPropClient.Checked = frmPropClient.Visible

'-> Afficher les propri�t�s du client en cours s'il y en a  un
If CurClient <> "" And frmPropClient.Visible = True Then
    Set aClient = Clients(UCase$(CurClient))
    DisplayClientProp aClient
End If


End Sub
Private Sub MnuQuitter_Click()

SendMessage Me.hWnd, WM_CLOSE, 0, 0


End Sub

Private Sub mnuReorganise_Click()
Me.Arrange 3
End Sub

Private Sub DisplayClientProp(aClient As Client)

'---> Cette proc�dure affiche les prorpi�t�s d'un client

'-> Charger les propri�t�s du client
frmPropClient.Text1.Text = aClient.Ident
frmPropClient.Text2.Text = aClient.Libel
frmPropClient.Text3.Text = aClient.Serveur
frmPropClient.Text4.Text = aClient.Doc_Wait

'-> Indiquer dans le menu que la fen�tre est s�lectionn�
Me.mnuPropClient.Checked = True

End Sub

Private Sub mnuSave_Click()

'-> Ne rien faire s'il n'y a pas de clients
If Trim(CurClient) = "" Then Exit Sub

'-> Lancer l'enregistrement
LaunchSave



End Sub

Private Sub mnuTraduction_Click()

Dim i As Integer
Dim aApp As Applicatif
Dim aNode As Node

'-> Pointer sur l'applicatif
Set aApp = Applicatifs(UCase$(CurApp))

'-> Quitter si rien � traduire
If aApp.Tree.Nodes.Count = 0 Then Exit Sub

frmTraduct.IsLoading = True

'-> Charger le treeview avec le menu
For i = 1 To aApp.Tree.Nodes.Count
    If aApp.Tree.Nodes(i).Parent Is Nothing Then
        Set aNode = frmTraduct.TreeView1.Nodes.Add(, , aApp.Tree.Nodes(i).Key, aApp.Tree.Nodes(i).Text, aApp.Tree.Nodes(i).Image)
    Else
        Set aNode = frmTraduct.TreeView1.Nodes.Add(aApp.Tree.Nodes(i).Parent.Key, 4, aApp.Tree.Nodes(i).Key, aApp.Tree.Nodes(i).Text, aApp.Tree.Nodes(i).Image)
    End If
    aNode.ExpandedImage = aApp.Tree.Nodes(i).ExpandedImage
    aNode.Tag = aApp.Tree.Nodes(i).Tag
Next 'Pour tous les niveaux du menu

Set frmTraduct.aApp = aApp
frmTraduct.IsLoading = False

'-> Charger le menu de base
frmTraduct.TreeView1.Nodes(1).Selected = True
Set frmTraduct.MenuCours = aApp.Menus(frmTraduct.TreeView1.SelectedItem.Key)
frmTraduct.Show vbModal

End Sub
