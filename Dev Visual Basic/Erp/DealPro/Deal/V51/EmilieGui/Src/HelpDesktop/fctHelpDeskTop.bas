Attribute VB_Name = "fctHelpDeskTop"
'-> Indique les largeurs et hauteur d'une bordure simple dans Windows en pixels
Public HauteurBordure As Integer
Public LargeurBordure As Integer
Public hCaption As Integer
Public nnmCaption As Integer

'-> Cette variable indique aux barres d'outils qu'elles peuvent se d�charger
Public IsTerminate As Boolean

'-> Variable Tempo
Public strTempoTxt As String

'-> Variables pour le proptah
Public IniPath As String 'Adresse du fichier HelpDeal.ini
Public DealPath As String 'Adresse de la documenatation Deal
Public DocPath As String 'Adresse ou chercher la documentation en cours

'-> Collections globales au projet
Public Clients As Collection
Public Applicatifs As Collection

'-> Variables de pointage sur le client en cours
Public CurClient As String
'Public IsCLientDeal As Bookmark 'Indique si le client en cours est le client DEAL

'-> Index de la langue en cours pour le client
Public CurCodeLangue As Integer

'-> Indique l'applicatif en cours de modification
Public CurApp As String

'-> Cette variable sert � cr�er les nodes des divers objets
Public IndexNodeCreate As Long
Public IndexCtCreate As Long

'-> Chaine � afficher lors de la cr�ation d'un node non traduit
Public strTraduction(1 To 6) As String
Public idTraduction(1 To 6) As Integer

'-> Couleur des nodes non traduits
Public lngTraduction(1 To 6) As Long

'-> Variables pour Drag/Drop
Public SourceNode As Node
Public NvSource  As Integer
Public AppDrag As String 'Indique l'applicatif d'origine du D&D
Public DragMode As Integer 'Indique la nature de la source 1 -> Supprime , 0 -> Laisse

'-> Variable pour copier / coller
Public CopyNode As Node
Public AppCopy As String 'Indique l'applicatif d'origine du Copier / Coller

'-> Variable pour copier coller, deplacer ....
Public KeyNodes() As String
Public Niveaux() As Integer

'-> Indique que la poubelle est charg�e
Public IsPoubelle As Boolean

'-> Pour choix de la destination dans les drag & drop
Public RetourChx As Integer

'-> Variable d'enregistrement globale
Public Type SaveGlobal
    IndexNodeCreate As Long
    IndexCtCreate As Long
End Type

'-> Variable d'enregistrement des mots cl�s
Public Type SaveKeyWord
    KeyName As String * 50 'Nom du mot cl�
End Type

'-> Variable d'enregistrement des contextes
Public Type SaveContexte
    Libelle1 As String * 70 'Libell�
    Libelle2 As String * 70 'Libell�
    Libelle3 As String * 70 'Libell�
    Libelle4 As String * 70 'Libell�
    Libelle5 As String * 70 'Libell�
    Libelle6 As String * 70 'Libell�
    Libelle7 As String * 70 'Libell�
    Libelle8 As String * 70 'Libell�
    Libelle9 As String * 70 'Libell�
    Libelle10 As String * 70 'Libell�
    Keyword As String * 50 'Mot cl� associ�
    Index As Integer
End Type

'-> Variable d'enregistrement d'un menu
Public Type SaveMenu
    Libelle1 As String * 70 'Libell�
    Libelle2 As String * 70 'Libell�
    Libelle3 As String * 70 'Libell�
    Libelle4 As String * 70 'Libell�
    Libelle5 As String * 70 'Libell�
    Libelle6 As String * 70 'Libell�
    Libelle7 As String * 70 'Libell�
    Libelle8 As String * 70 'Libell�
    Libelle9 As String * 70 'Libell�
    Libelle10 As String * 70 'Libell�
    FichierHTML As String * 50 'Nom du fichier HTML
    Key As String * 15 'Cl� d'acc�s au menu
    KeyParent As String * 15 'Cl� d'acc�s du niveau sup�rieur
    IsUrl As Boolean 'Indique qu'un menu est un fichier
    IsLien As Boolean 'Indique s'il y a un lien ou non
End Type

'-> Variable d'enregistrement association Lien Fichiuer HTML
Public Type SaveLien
    KeyMenu As String * 15 'Cl� du menu associ�
    Keyword As String * 50 'Mot cl� associ�
    KeyCt As String * 10 'Cl� du contexte associ�
End Type

'-> Variable d'enregistrement des constantes de traduction
Public Type SaveTraduct
    idTraduction As Integer
    strTraduction As String * 50
    lngTraduction As Long
End Type


Public Sub LoadGlobalFile()

'---> Ce fichier charge le fichier Global.dat du client que l'on vient de sp�cifier
Dim aClient As Client
Dim Rep

'-> Pointer sur le client
Set aClient = Clients(UCase$(CurClient))

'---> Charge le fichier des mots cl�s KeyW.dat
Load frmKeyWords

'-> V�rifier si on trouve le fichier Global.dat
If Dir$(aClient.Serveur & "Global.dat", vbNormal) <> "" Then
    '-> On a trouv� le fichier global.dat : le charger
    LoadGlobal
    '-> Charger le fichier des mots cl�s si on le trouve
    LoadKeyWordFile
    '-> Charger le fichier des contextes si on le trouve
    LoadCtFile
    '-> Charger le fichier des traductions si on le trouve
    LoadTraductFile
Else
    '-> On a pas trouv� le fichier global.dat : initialiser les valeurs par d�faut
    IndexNodeCreate = 1
    IndexCtCreate = 1
    '-> Nettoyer la feuille des mots cles
    frmKeyWords.ClearKeyWords
    '-> Initialiser la collection des mots cl�s propre au client en cours
    Set frmKeyWords.Keywords = New Collection
    '-> Initialiser les codes de traduction par d�faut
    InitDefautTraduction
End If


End Sub
Public Sub LoadTraductFile()

'---> Charge le fichier des constantes de traduction des menus
Dim NumFic As Integer
Dim aSaveTraduct As SaveTraduct
Dim nbEnreg As Integer, i As Integer

If Dir$(DocPath & "Traduct.dat", vbNormal) <> "" Then
    '-> Ouvrir le fichier
    NumFic = FreeFile
    Open DocPath & "Traduct.dat" For Random As #NumFic Len = Len(aSaveTraduct)
    nbEnreg = LOF(NumFic) / Len(aSaveTraduct)
    For i = 1 To nbEnreg
        Get #NumFic, i, aSaveTraduct
        idTraduction(i) = aSaveTraduct.idTraduction
        strTraduction(i) = Trim(aSaveTraduct.strTraduction)
        lngTraduction(i) = aSaveTraduct.lngTraduction
    Next
    '-> Fermer le fichier
    Close #NumFic
Else
    '-> Lancer l'initialisation par defaut des valeurs
    InitDefautTraduction
End If

End Sub


Public Sub LoadKeyWordFile()

'---> Charger la liste des mots cl�s
Dim NumFic As Integer
Dim aSaveKey As SaveKeyWord
Dim nbEnreg As Long, i As Long
Dim aKeyWord As Keyword

If Dir$(DocPath & "key.dat") <> "" Then
    '-> Ouvrir le fichier
    NumFic = FreeFile
    Open DocPath & "Key.dat" For Random As #NumFic Len = Len(aSaveKey)
    nbEnreg = LOF(NumFic) / Len(aSaveKey)
    For i = 1 To nbEnreg
        '-> Lecture de l'enregistrement
        Get #NumFic, i, aSaveKey
        '-> Cr�ation du menu
        Set aKeyWord = New Keyword
        aKeyWord.Name = Trim(aSaveKey.KeyName)
        '-> Ajout du menu dans la collection
        frmKeyWords.Keywords.Add aKeyWord, UCase$(aKeyWord.Name)
    Next 'Pour tous les mots cl�s
    '-> Fermer le fichier
    Close #NumFic
    '-> Afficher la collection
    frmKeyWords.LoadCollection
End If


End Sub

Public Sub LoadCtFile()

'---> Charge le fichier des contextes associ�s au mots cl�s Ct.dat

Dim NumFic As Integer
Dim aSaveCt As SaveContexte
Dim nbEnreg As Long
Dim i As Long
Dim aCt As Contexte
Dim aKeyWord As Keyword
Dim Rep
Dim AddCt As Boolean

'-> Charger le fichier
If Dir$(DocPath & "Ct.dat", vbNormal) <> "" Then
    '-> Ouverture du fichier
    NumFic = FreeFile
    Open DocPath & "Ct.dat" For Random As #NumFic Len = Len(aSaveCt)
    nbEnreg = LOF(NumFic) / Len(aSaveCt)
    For i = 1 To nbEnreg
        '-> Charger le contexte
        Get #NumFic, i, aSaveCt
        '-> V�rifier que le mot cl� existe avant d'ajouter le contexte
        If Not GetKeyWordExiste(aSaveCt.Keyword) Then
            Rep = MsgBox("Impossible de cr�er le contexte : " & Chr(13) & Trim(aSaveCt.Libelle1) & " car le mot cl� rattach� : " & Chr(13) & aSaveCt.Keyword & " n'existe pas. D�sirez-vous cr�er ce mot cl� ?", vbQuestion + vbYesNo, "Avertissement")
            If Rep = vbYes Then
                Set aKeyWord = New Keyword
                aKeyWord.Name = Trim(aSaveCt.Keyword)
                frmKeyWords.Keywords.Add aKeyWord, UCase$(aKeyWord.Name)
                AddCt = True
            Else
                '-> Ne pas ajouter le contexte
                AddCt = False
            End If 'Si le mot cl� n'existe pas et que l'on doit le cr�er
        Else
            '-> Il faut pointer sur le mot cl�
            Set aKeyWord = frmKeyWords.Keywords(UCase$(Trim(aSaveCt.Keyword)))
            AddCt = True
        End If 'Si le mot cl� n'existe pas
        '-> Cr�er le contexte
        Set aCt = New Contexte
        aCt.Keyword = aKeyWord.Name
        aCt.Index = aSaveCt.Index
        aCt.SetLbByLangue 1, Trim(aSaveCt.Libelle1)
        aCt.SetLbByLangue 2, Trim(aSaveCt.Libelle2)
        aCt.SetLbByLangue 3, Trim(aSaveCt.Libelle3)
        aCt.SetLbByLangue 4, Trim(aSaveCt.Libelle4)
        aCt.SetLbByLangue 5, Trim(aSaveCt.Libelle5)
        aCt.SetLbByLangue 6, Trim(aSaveCt.Libelle6)
        '-> Ajouter le contexte
        aKeyWord.Contextes.Add aCt, "CT" & aCt.Index
    Next 'Pour tous les enregistrements
End If 'Si le fichier existe


End Sub


Private Function GetKeyWordExiste(aKeyWord As String) As Boolean

On Error GoTo GestError

Dim aKey As Keyword

Set aKey = frmKeyWords.Keywords(UCase$(Trim(aKeyWord)))
Set aKey = Nothing
GetKeyWordExiste = True

Exit Function

GestError:
    GetKeyWordExiste = False

End Function

Public Sub DisplayAppByLangue()

'---> Cette proc�dure est charg�e d'afficher les nodes dans la langue sp�cifi�e

Dim i As Integer
Dim aMenu As Menu
Dim LibToDisplay As String
Dim aColor As String

Dim aApp As Applicatif
Dim aFrm As frmDoc
Dim aKeyWord As Keyword
Dim aStrCt As String
Dim aCt As Contexte
Dim TempoLib As String

'-> Pour tous les applicatifs
For Each aApp In Applicatifs
    If aApp.IsLoaded Then
        '-> Changer les libelles
        For i = 1 To aApp.Tree.Nodes.Count
            '-> Ne rien faire pour les noms de fichier
            If aApp.Tree.Nodes(i).Tag <> "HTML" Then
                If aApp.Tree.Nodes(i).Tag = "KEYWORD" Then
                    '-> Pointer sur le menu
                    Set aMenu = aApp.Menus(aApp.Tree.Nodes(i).Parent.Key)
                    '-> R�cup�rer le lien
                    If Not aMenu.aLien Is Nothing Then
                        Set aKeyWord = frmKeyWords.Keywords(aMenu.aLien.KeyWordKey)
                        TempoLib = aKeyWord.Contextes(aMenu.aLien.ContexteKey).Libelle
                    Else
                        If CurCodeLangue = 1 Then
                            TempoLib = "<Pas d'affectation>"
                        Else
                            TempoLib = ""
                        End If
                    End If
                Else
                    '-> Pointer sur le menu associ�
                    Set aMenu = aApp.Menus(aApp.Tree.Nodes(i).Key)
                    TempoLib = aMenu.Libelle
                End If
                If Trim(TempoLib) = "" Then
                    '-> Afficher la chaine de remplacment
                    If idTraduction(CurCodeLangue) <> 4 Then
                        LibToDisplay = strTraduction(CurCodeLangue)
                    Else
                        '-> Afficher la chaine du menu d'origine
                        LibToDisplay = aMenu.GetLbByLangue(1)
                    End If
                    aColor = lngTraduction(CurCodeLangue)
                Else
                    LibToDisplay = TempoLib
                    aColor = 0
                End If
                aApp.Tree.Nodes(i).Text = LibToDisplay
                aApp.Tree.Nodes(i).ForeColor = aColor
            End If
        Next
    End If
Next

If Not IsPoubelle Then Exit Sub

'-> Pour la poubelle
For i = 1 To frmPoubelle.TreeView1.Nodes.Count
    '-> Tester l'affichage si mot cl�
    If frmPoubelle.TreeView1.Nodes(i).Tag = "KEYWORD" Then
        '-> Pointer sur le menu
        Set aMenu = frmPoubelle.Menus(frmPoubelle.TreeView1.Nodes(i).Parent.Key)
        '-> R�cup�rer le lien
        If Not aMenu.aLien Is Nothing Then
            Set aKeyWord = frmKeyWords.Keywords(aMenu.aLien.KeyWordKey)
            TempoLib = aKeyWord.Contextes(aMenu.aLien.ContexteKey).Libelle
        Else
            If CurCodeLangue = 1 Then
                TempoLib = "<Pas d'affectation>"
            Else
                TempoLib = ""
            End If
        End If
    Else
        Set aMenu = frmPoubelle.Menus(frmPoubelle.TreeView1.Nodes(i).Key)
        TempoLib = aMenu.Libelle
    End If
    If Trim(TempoLib) = "" Then
        LibToDisplay = strTraduction(CurCodeLangue)
        aColor = lngTraduction(CurCodeLangue)
    Else
        LibToDisplay = TempoLib
        aColor = 0
    End If
    frmPoubelle.TreeView1.Nodes(i).Text = LibToDisplay
    frmPoubelle.TreeView1.Nodes(i).ForeColor = aColor
Next

End Sub


Public Sub GetBordures()

'---> Cette fonction r�cup�re les dimensions des bordures de windows

LargeurBordure = GetSystemMetrics(SM_CXEDGE)
HauteurBordure = GetSystemMetrics(SM_CYEDGE)
hCaption = GetSystemMetrics(SM_CYSMCAPTION&)
nnmCaption = GetSystemMetrics(SM_CYCAPTION&)

End Sub

Public Sub GestResize(aForm As Form, aTool As ToolBar)

'---> Faire en sorte que la zone client permette toujours d'afficher la barre d'outils
Dim Res As Long
Dim aRect As Rect
Dim aRect2 As Rect
Dim lButton As Integer
Dim hButton  As Integer


'-> R�cup�ration de la zone cliente de la fen�tre
Res = GetClientRect(aForm.hWnd, aRect)
Res = GetWindowRect(aForm.hWnd, aRect2)

'-> R�cup�ration de la largeur et de la hauteur d'un bouton et d'une barre de titre
lButton = aTool.ButtonWidth + LargeurBordure * 4
hButton = aTool.ButtonHeight + HauteurBordure * 4


'-> V�rifier si la largeur est suffisante
If aForm.ScaleX(aForm.Width, 1, 3) < lButton Then aForm.Width = aForm.ScaleX(lButton, 3, 1)

'-> V�rifier si la hauteur est suffisante
If aForm.ScaleY(aForm.Height, 1, 3) < hButton Then aForm.Height = aForm.ScaleY(hButton + hCaption, 3, 1)

End Sub

Public Sub CloseMe(Cancel As Integer, aForm As Form)

'---> Proc�dure qui interdit la fermeture d'une barre d'outils
If Not IsTerminate Then
    Cancel = 1
    aForm.Hide
End If

End Sub

Public Sub LoadListeClient()

'-> Pour lecture du fichier TruboMaq.ini
Dim TempFileName As String
Dim Res As Long
Dim lpKey As String
Dim lpBuffer As String
Dim NomSection As String
Dim strLibel As String
Dim strPath As String
Dim FindApp As Boolean
Dim strWaitDoc As String

'-> Pour compteur
Dim i As Integer, j As Integer, nEntries As Integer

'-> Pour cr�ation des applicatifs
Dim aApp As Applicatif

'-> Pour cr�ation des clients
Dim aClient As Client
Dim aNode As Node
Dim aNode2 As Node

'-> Effectuer une copie du fichier ini dans le r�pertoire tempo
TempFileName = GetTempFileNameVB("INI")
Res = CopyFile(IniPath, TempFileName, 0)
    
'-> R�cup�ration de la liste des logiciels
lpKey = Space(500)
Res = GetPrivateProfileString("Progiciels", "App", "", lpKey, Len(lpKey), TempFileName)
If lpKey = "" Then
    '-> Impossible de trouver la cl� ou la section : Quitter
    MsgBox "Impossible de trouver la liste des progiciels DEAL INFORMATIQUE", vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

Dim ToAdd As Boolean

'-> Cr�ation de la liste des progiciels
lpKey = Mid$(lpKey, 1, Res)
For i = 1 To NumEntries(lpKey, "|")
    ToAdd = True
    Set aApp = New Applicatif
    aApp.Name = Entry(i, lpKey, "|")
    '-> Indiquer que le progiciel n'est pas charg�
    aApp.IsLoaded = False
    '-> Cr�ation des identifiant par lettre
    Select Case UCase$(aApp.Name)
        Case "DEAL"
            aApp.Identifiant = "dd"
        Case "ELODIE"
            aApp.Identifiant = "ed"
        Case "SOPHIE"
            aApp.Identifiant = "sd"
        Case "LUCIE"
            aApp.Identifiant = "gd"
            ToAdd = False
        Case "NATHALIE"
            aApp.Identifiant = "gd"
            ToAdd = False
        Case "BACCHUS"
            aApp.Identifiant = "bd"
        Case "PLANNING"
            aApp.Identifiant = "pd"
        Case "KS"
            aApp.Identifiant = "kd"
        Case "JULIE"
            aApp.Identifiant = "gd"
            ToAdd = True
        Case "GESCOM"
            aApp.Identifiant = "gd"
        Case "IMMOBILISATIONS"
            aApp.Identifiant = "md"
    End Select
    '-> Ajouter dans la collection
'    ToAdd = True
    If ToAdd Then Applicatifs.Add aApp, UCase$(aApp.Name)
Next

'-> R�cup�ration des propri�t�s des divers applicatifs
For i = 1 To Applicatifs.Count
    '-> R�cup�ration de la cl� Libell�
    lpKey = Space(500)
    Res = GetPrivateProfileString(Applicatifs(i).Name, "Libel", "", lpKey, Len(lpKey), TempFileName)
    If lpKey <> "" Then Applicatifs(i).Descriptif = Mid$(lpKey, 1, Res)
Next

'-> R�cup�ration de la liste des clients DEAL INFORMATIQUE
lpBuffer = Space$(32765)
Res = GetPrivateProfileString(vbNullString, "", "Erreur", lpBuffer, Len(lpBuffer), TempFileName)
lpBuffer = Mid$(lpBuffer, 1, Res - 1)
If lpBuffer = "Erreur" Then Exit Sub
'-> Charger le tableau des sections
nEntries = NumEntries(lpBuffer, Chr(0))
For i = 1 To nEntries
    '-> R�cup�rer le nom de la section
    NomSection = Entry(i, lpBuffer, Chr(0))
    '-> Pb WIN 95 : Gestion des blancs
    If Trim(NomSection) = "" Then Exit For
    '-> Selon la cl� que l'on est en train de lire
    If UCase$(NomSection) = "PARAM" Or UCase$(NomSection) = "PROGICIELS" Then
    Else
        '-> Traiter le cas de regroupement des versions de gestion commerciale
        If UCase$(NomSection) = "LUCIE" Or UCase$(NomSection) = "NATHALIE" Or UCase$(NomSection) = "JULIE" Then NomSection = "GESCOM"
                    
        FindApp = False
        For j = 1 To Applicatifs.Count
            If UCase$(NomSection) = UCase$(Applicatifs(j).Name) Then
                FindApp = True
                Exit For
            End If
        Next 'Pour tous les progiciels
        If Not FindApp Then
            '-> Il faut r�cup�rer les informations du client
            strLibel = GetIniString(NomSection, "LIBEL", TempFileName, False)
            strPath = GetIniString(NomSection, "HELP", TempFileName, False)
            strWaitDoc = GetIniString(NomSection, "DOC_ATTENTE", TempFileName, False)
            '-> Remplacer dans le path les mots cl�s
            ReplacePathMotCles strPath
            strPath = Replace(strPath, "$IDENT$", NomSection)
            '-> Ajouter dans la liste des clients
            Set aClient = New Client
            aClient.Ident = NomSection
            aClient.Libel = strLibel
            aClient.Serveur = strPath
            If strWaitDoc <> "NULL" Then aClient.Doc_Wait = strWaitDoc
            Clients.Add aClient, UCase$(aClient.Ident)
            '-> Ajouter le client dans le treeview
            Set aNode = frmListeClient.TreeView1.Nodes.Add(, , "CLI" & UCase$(aClient.Ident), aClient.Libel)
            '-> Ajouter les infos
            frmListeClient.TreeView1.Nodes.Add aNode.Index, 4, , "Code client : " & aClient.Ident
            Set aNode2 = frmListeClient.TreeView1.Nodes.Add(aNode.Index, 4, , "Path d'enregistrement : " & aClient.Serveur)
            '-> V�rifier que la cl� path a �t� trouv�e
            If Dir$(aClient.Serveur, vbDirectory) = "" Then
                aNode2.Text = aNode2.Text & "   ---> Introuvable"
                aNode.ForeColor = &HFF&
            Else
                aNode.Image = "Client"
            End If
            Set aNode2 = frmListeClient.TreeView1.Nodes.Add(aNode.Index, 4, , "Path de la documentation en attente : " & aClient.Doc_Wait)
            '-> V�rifier que la cl� path a �t� trouv�e
            If Dir$(aClient.Doc_Wait, vbDirectory) = "" Or aClient.Doc_Wait = "" Then
                aNode2.Text = aNode2.Text & "   ---> Introuvable"
            End If
        End If
    End If
Next 'Pour toutes les sections du fichier ini

GetPathMotCles
NomSection = PathLecteur & "\DEALPRO\DEAL\" & PathVersion & "\AIDE\"


'-> Ajouter le client DEAL
Set aClient = New Client
aClient.Ident = "DEAL"
aClient.Libel = "Soci�t� DEAL Informatique"
aClient.Serveur = NomSection
aClient.Doc_Wait = "\\DEALNT16\E16\Service Methode\Documentation en Attente\"
DealPath = NomSection
Clients.Add aClient, "DEAL"

'-> Ajouter les nodes
Set aNode = frmListeClient.TreeView1.Nodes.Add(, , "CLIDEAL", "Deal Informatique", "Client")
frmListeClient.TreeView1.Nodes.Add aNode.Index, 4, , "Code client : " & "DEAL", "Code"
frmListeClient.TreeView1.Nodes.Add aNode.Index, 4, , "Path d'enregistrement : " & aClient.Serveur, "Rep"
frmListeClient.TreeView1.Nodes.Add aNode.Index, 4, , "Path de la documentation en attente : " & aClient.Doc_Wait, "Rep"

'-> Ajouter l'applicatif Cmm qui contient toutes les explications communes aux progiciels
Set aApp = New Applicatif
aApp.Descriptif = "Documentation commune"
aApp.Identifiant = "Cd"
aApp.Name = "Cmm"
Applicatifs.Add aApp, "CMM"

'-> supprimer le fichier temporaire
If Dir$(TempFileName) <> "" Then Kill TempFileName

End Sub

Public Sub CleanClient()

'---> Cette proc�dure Ferme toutes les fen�tres de documentation en cours


End Sub

Public Sub ActiveMenuForm(Index As Integer)

'-> Ch�ckuer la fen�tre appropri�e

On Error Resume Next

Dim i As Integer

For i = 1 To HelpDeskTop.mnuListProgi.Count - 1
    If i = Index Then
        HelpDeskTop.mnuListProgi(i).Checked = True
    Else
        HelpDeskTop.mnuListProgi(i).Checked = False
    End If
Next

End Sub
Public Sub AnalyseNode(Origine As String, aNode As Node, Mode As Integer, Destination As String, RootNode As Node, Position As Integer)

'-> D�fintion des modes
' 1 = Copie la source et la recr�er sur destination
' 2 = Supprime la source et la copie sur la destination
' 3 = Supprime la source

Dim IsAnalysedNode As Boolean
Dim AnalyseNode As Node
Dim i As Integer
Dim IdNode As Integer
Dim FindNode As Boolean
Dim aApp As Applicatif
Dim aAppDest As Applicatif
Dim LibDest As String

Dim MenuOrigine As Collection
Dim MenuDestination As Collection

Dim TreeOrigine As TreeView
Dim TreeDestination As TreeView

Dim RefNode As Node
Dim KeyNodesDest() As String

Dim NodeChild As Node
Dim aNode2 As Node

Dim KillCopyNode As Boolean
Dim Niveau As Integer

'-> Quitter si la r�f�rence n'existe plus
If aNode Is Nothing Then Exit Sub

'-> Pointer sur le treeview d'origine
If UCase$(Origine) = "POUBELLE" Then
    Set TreeOrigine = frmPoubelle.TreeView1
    Set MenuOrigine = frmPoubelle.Menus
Else
    Set aApp = Applicatifs(UCase$(Origine))
    Set TreeOrigine = aApp.Tree
    Set MenuOrigine = aApp.Menus
    
End If

'-> Initialiser la collection de menu de destination
If Destination <> "" Then
    If Destination = "POUBELLE" Then
        Set MenuDestination = frmPoubelle.Menus
    Else
        Set aAppDest = Applicatifs(UCase$(Destination))
        Set MenuDestination = aAppDest.Menus
    End If
End If

'-> Faire pointer le node d'analyse du node en cours d'utilisation
Set AnalyseNode = aNode

'-> Initialiser la matrice de gestion des NodeKey
ReDim KeyNodes(1)
KeyNodes(1) = aNode.Key

'-> Initialiser le compteur de niveau
ReDim Niveaux(1)
Niveaux(1) = 1
Niveau = 1

'-> Pointer sur le node fils
If aNode.Children <> 0 Then
    '-> Pointer sur le premier node
    Set AnalyseNode = aNode.Child
    Niveau = Niveau + 1
    '-> Anlayse du niveau
    Do
        '-> Tester s'il est analys�
        If IsAnalysed(AnalyseNode.Key) Then
            '-> le node est analys� : pointer sur les nodes enfants pour trouver un node non analys�
            'et s 'il y en a pas remonter sur le node parent
            FindNode = False
            IdNode = AnalyseNode.Index
            If AnalyseNode.Children <> 0 Then
                '-> R�cup�rer l'index du premier node
                IdNode = AnalyseNode.Child.FirstSibling.Index
                Do
                    '-> Pointer sur le node
                    If Not IsAnalysed(TreeOrigine.Nodes(IdNode).Key) Then
                        FindNode = True
                        Exit Do
                    Else
                        FindNode = False
                    End If
                    '-> Tester si on est sur le dernier node
                    Set NodeChild = TreeOrigine.Nodes(IdNode)
                    '-> Sortir si dernier niveau
                    If IdNode = AnalyseNode.Child.LastSibling.Index Then Exit Do
                    '-> R�cup�rer l'index du node suivant
                    IdNode = NodeChild.Next.Index
                Loop 'Boucle d'analyse des nodes enfants
            End If
            '-> V�rifier si on a trouv� un node non analys�
            If FindNode Then
                '-> Pointer sur cet objet
                Set AnalyseNode = TreeOrigine.Nodes(IdNode)
                Niveau = Niveau + 1
            Else
                If AnalyseNode Is aNode Then
                    '-> On analys� le sous niveau : il n' ya plus de node non analys� quitter
                    Exit Do
                Else
                    '-> Pointer sur le node parent du node
                    Set AnalyseNode = AnalyseNode.Parent
                    Niveau = Niveau - 1
                End If
            End If
        Else
            '-> Redimmensionner la matrice des sauvegardes
            ReDim Preserve KeyNodes(UBound(KeyNodes()) + 1)
            '-> Sauvegard� sa cl� d'acc�s
            KeyNodes(UBound(KeyNodes())) = AnalyseNode.Key
            '-> Redimensionner la matrice des niveaux
            ReDim Preserve Niveaux(UBound(KeyNodes()))
            Niveaux(UBound(Niveaux())) = Niveau
            '-> Cas du copier / coller
            If Not CopyNode Is Nothing Then
                '-> Supprimer La r�f�rence Copy node si elle va �tre d�plac�e
                If AnalyseNode.Key = CopyNode.Key And (Mode = 3 Or Mode = 2) Then KillCopyNode = True
            End If
        End If
    Loop 'Boucle d'analyse tant que l'on trouve des nodes non analys�

Else
    '-> test dans le cas du copier / coller
    If Not CopyNode Is Nothing Then
        '-> Supprimer La r�f�rence Copy node si elle va �tre d�plac�e
        If aNode.Key = CopyNode.Key And (Mode = 3 Or Mode = 2) Then KillCopyNode = True
    End If
End If 'Si le node � d�placer a des enfants

'-> Quitter si le mode est 999 : Impression
If Mode = 999 Then Exit Sub

'-> On a r�cup�r� la liste des nodes concern�s : traitement selectif delon le mode
If Destination = "" Then
    '-> On supprime de mani�re d�finitive les nodes :
    '-> Suppression des menus
    DeleteMenuOrigine MenuOrigine, TreeOrigine, Destination, Origine
    'Suppression du node s�lectionn�
    TreeOrigine.Nodes.Remove aNode.Key
Else
    '-> Pointer sur le treeview de destination
    If Destination = "POUBELLE" Then
        Set TreeDestination = frmPoubelle.TreeView1
    Else
        Set TreeDestination = aAppDest.Tree
    End If
    '-> Cr�er une matrice des cles de nodes de destination
    ReDim KeyNodesDest(UBound(KeyNodes()))
    ReDim NiveauxDest(UBound(KeyNodes()))
    For i = 1 To UBound(KeyNodes())
        KeyNodesDest(i) = "MENU|" & IndexNodeCreate
        IndexNodeCreate = IndexNodeCreate + 1
    Next
    '-> Recr�er les nodes vers la destination
    For i = 1 To UBound(KeyNodes())
        '-> Pointer sur le node d'origine
        Set RefNode = TreeOrigine.Nodes(KeyNodes(i))
        '-> Tester si on cr�er le node d'origine
        If i = 1 Then
            '-> Si Rootnode est renseign� : faire du nouveau node un node fils
            If Not RootNode Is Nothing Then
                '-> Cr�er le node destination
                Set aNode2 = TreeDestination.Nodes.Add(RootNode.Index, Position, KeyNodesDest(i), RefNode.Text, RefNode.Image)
            Else
                '-> Cr�er le node destination
                Set aNode2 = TreeDestination.Nodes.Add(, , KeyNodesDest(i), RefNode.Text, RefNode.Image)
            End If
            aNode2.Tag = RefNode.Tag
            aNode2.Selected = True
        Else
            '-> On cr�er un sous nodes du node d'origine
            Set aNode2 = TreeDestination.Nodes.Add(KeyNodesDest(GetNodeKey(RefNode.Parent.Key)), 4, KeyNodesDest(i), RefNode.Text, RefNode.Image)
            aNode2.Tag = RefNode.Tag
        End If
        If aNode2.Tag = "CHAPITRE" Then aNode2.ExpandedImage = "ChapitreOuvert"
    Next
    
    '-> Il faut cr�er les menus de destination si necessaire
    If Mode <> 3 Then
        If Destination = "POUBELLE" Then
            DupMenu MenuDestination, MenuOrigine, KeyNodesDest, KeyNodes, TreeOrigine, TreeDestination, True, Destination
        Else
            DupMenu MenuDestination, MenuOrigine, KeyNodesDest, KeyNodes, TreeOrigine, TreeDestination, False, Destination
        End If
    End If
    
    '-> Supprimer l'origine si necessaire
    If Mode <> 1 Then
        '-> Suppression des menus
        DeleteMenuOrigine MenuOrigine, TreeOrigine, Destination, Origine
        '-> Icone ferm�e si necessaire
        If Not aNode.Parent Is Nothing Then aNode.Parent.Expanded = False
        'Suppression du node s�lectionn�
        TreeOrigine.Nodes.Remove aNode.Key
        '-> Affichage dans le Browser
        If TreeOrigine.Nodes.Count = 0 Then
            '-> Ne rien afficher si la source est la poubelle
            If Origine <> "POUBELLE" Then aApp.Web.Navigate DealPath & "Blanck.htm"
        Else
            If UCase$(Origine) <> "POUBELLE" Then
                Select Case TreeOrigine.SelectedItem.Tag
                    Case "CHAPITRE"
                        aApp.Web.Navigate DealPath & "Blanck.htm"
                    Case "DOCUMENT"
                        DisplayHtmlFile TreeOrigine.SelectedItem.Child.Text, aApp.Name
                    Case "HTML"
                        DisplayHtmlFile TreeOrigine.SelectedItem.Text, aApp.Name
                End Select
            End If
        End If
    End If
End If

If KillCopyNode Then Set CopyNode = Nothing

'-> Indiquer dans l'applicatif d'origine que l'on doit enregistrer
If Not aApp Is Nothing Then aApp.ToSave = True

'-> Indiquer dans l'applivatif de destination que l'on doit enregistrer
If Destination <> "" And Destination <> "POUBELLE" Then aAppDest.ToSave = True

End Sub
Private Function DupMenu(MenuDestination As Collection, MenuOrigine As Collection, _
                         KeyNodesDestination() As String, KeyNodesOrigine() As String, _
                         TreeOrigine As TreeView, TreeDestination As TreeView, _
                         Poubelle As Boolean, Destination As String)

'---> Cette fonction Cr�er dans le menu de destination les nouveaux Nodes
Dim i As Integer, j As Integer
Dim MenuRef As Menu
Dim NewMenu As Menu
Dim aNode As Node
Dim LienRef As Collection
Dim aLien As Lien

For i = 1 To UBound(KeyNodesDestination())
    '-> Ne rien faire si la r�f�rence pointe sur un node fichier HTML qui n'a pas de menus
    If TreeOrigine.Nodes(KeyNodesOrigine(i)).Tag <> "HTML" And TreeOrigine.Nodes(KeyNodesOrigine(i)).Tag <> "KEYWORD" Then
        '-> Pointer sur le menu d'origine
        Set MenuRef = MenuOrigine(KeyNodesOrigine(i))
        '-> Cr�er le nouvel objet
        Set NewMenu = New Menu
        '-> Dupliquer les libelles
        For j = 1 To 10
            NewMenu.SetLbByLangue j, MenuRef.GetLbByLangue(j)
        Next
        '-> Code langue d'origine
        NewMenu.idLangueOrigine = MenuRef.idLangueOrigine
        '-> Si lien ou non
        If Not MenuRef.aLien Is Nothing Then
            '-> Il existe un lien : le cr�er dans l'applicatif de destination
            If Destination = "POUBELLE" Then
                Set LienRef = frmPoubelle.Liens
            Else
                Set LienRef = Applicatifs(UCase$(Destination)).Liens
            End If
            '-> Ajouter le lien dans la collection des liens de l'applicatif d'origine
            Set aLien = New Lien
            aLien.Applicatif = Destination
            aLien.ContexteKey = MenuRef.aLien.ContexteKey
            aLien.KeyWordKey = MenuRef.aLien.KeyWordKey
            LienRef.Add aLien, KeyNodesDestination(i) & "|" & UCase$(aLien.KeyWordKey & "|" & aLien.ContexteKey)
            '-> Ajouter le lien dans le nouveau menu
            Set NewMenu.aLien = New Lien
            NewMenu.aLien.Applicatif = Destination
            NewMenu.aLien.KeyWordKey = MenuRef.aLien.KeyWordKey
            NewMenu.aLien.ContexteKey = MenuRef.aLien.ContexteKey
        End If 'Si un lien existe
        '-> Ajouter le nouveau menu
        MenuDestination.Add NewMenu, KeyNodesDestination(i)
    Else
        '-> Ajouter un menu si la destination est la poubelle
        If Poubelle Then
            Set NewMenu = New Menu
            '-> Dupliquer les libelles
            For j = 1 To 10
                NewMenu.SetLbByLangue j, TreeOrigine.Nodes(KeyNodesOrigine(i)).Text
            Next
            '-> Code langue d'origine
            NewMenu.idLangueOrigine = MenuRef.idLangueOrigine
            '-> Ajouter le nouveau menu
            MenuDestination.Add NewMenu, KeyNodesDestination(i)
        End If
    End If
Next

End Function

Private Function IsAnalysed(aNodeKey As String) As Boolean

'---> Cette fonction retourne vrai si le node sp�cifi� est d�ja anamys�

Dim i As Integer

If UBound(KeyNodes()) = 0 Then
    IsAnalysed = False
    Exit Function
End If

For i = 1 To UBound(KeyNodes())
    If UCase$(aNodeKey) = KeyNodes(i) Then
        IsAnalysed = True
        Exit Function
    End If
Next

End Function
Private Function GetNodeKey(KeyToSearch As String) As Integer

'---> Cette fonction retourne l'index de la matrice qui correspond � la cl�
Dim i As Integer

For i = 1 To UBound(KeyNodes())
    If KeyToSearch = KeyNodes(i) Then
        GetNodeKey = i
        Exit Function
    End If
Next

End Function

Private Sub DeleteMenuOrigine(MenuOrigine As Collection, TreeOrigine As TreeView, _
                              AppName As String, Origine As String)

Dim aMenu As Menu
Dim aApp As Applicatif
Dim aLiens As Collection

'---> Cette fonction supprime les objets du menu d'origine
For i = 1 To UBound(KeyNodes())
    '-> V�rifier que le menu existe � l'orgine ( pour fichier HTML)
    If TreeOrigine.Nodes(KeyNodes(i)).Tag <> "HTML" And TreeOrigine.Nodes(KeyNodes(i)).Tag <> "KEYWORD" Then
        Set aMenu = MenuOrigine(KeyNodes(i))
        '-> Tester si une association de mot cl� est en cours
        If Not aMenu.aLien Is Nothing Then
            '-> Tester si on est dans le m�me applicatif ou pas
            If UCase$(Origine) <> UCase$(AppName) Then
                If Origine = "POUBELLE" Then
                    Set aLiens = frmPoubelle.Liens
                Else
                    Set aLiens = Applicatifs(Origine).Liens
                End If
                aLiens.Remove (TreeOrigine.Nodes(KeyNodes(i)).Key & "|" & aMenu.aLien.KeyWordKey & "|" & aMenu.aLien.ContexteKey)
            End If 'si m�me applicatif
            '-> Supprimer le lien
            Set aMenu.aLien = Nothing
        End If 's'il y a des liens
        '-> Maintenant on peut supprimer l'item menu dans le menu d'origine
        MenuOrigine.Remove KeyNodes(i)
    End If 'Si on doit supprimer le node
Next

'-> Supprimer la matrice des cles
Erase KeyNodes

End Sub

Public Sub DisplayHtmlFile(htmlFile As String, aAppName As String)

Dim aApp As Applicatif
Dim aClient As Client
Dim strTmp As String

On Error GoTo GestError

'-> Pointer sur l'applicatif en cours
Set aApp = Applicatifs(aAppName)

'-> Pointer sur le client en cours
Set aClient = Clients(CurClient)

'-> Cas ou nom de fichier non renseign�
If Trim(htmlFile) = "" Then GoTo GestError

'-> Retraiter le fichier pour supprimer les signets et v�rifier s'il existe
If InStr(1, htmlFile, "#") <> 0 Then
    strTmp = Mid$(htmlFile, 1, InStr(1, htmlFile, "#") - 1)
    If Dir$(aClient.Serveur & strTmp, vbNormal) = "" Then GoTo GestError
End If

    
'-> Afficher la bonne feuille HTML
aApp.Web.Navigate2 aClient.Serveur & htmlFile

Exit Sub

GestError:

        DisplayBlanckHtmlFile aApp.Web, aClient


End Sub

Public Sub DisplayBlanckHtmlFile(aWeb As WebBrowser, aClient As Client)


'-> V�rifier si on trouve une feuille Blanck.hmt dans le path client sinon celle de DEAL
If Dir$(aClient.Serveur & "Blanck.htm", vbNormal) <> "" Then
    aWeb.Navigate aClient.Serveur & "UnBlanck.htm"
Else
    aWeb.Navigate DealPath & "UnBlanck.htm"
End If


End Sub


Public Sub LaunchSave()

'---> Cette fonction v�rifie si on doit enregistrer

Dim MustSave As Boolean
Dim aApp As Applicatif
Dim aNode As Node

frmSave.TreeView1.Nodes.Clear
For Each aApp In Applicatifs
    '-> Ajouter dans le treeview1
    Set aNode = frmSave.TreeView1.Nodes.Add(, , aApp.Name, aApp.Name & " - " & aApp.Descriptif)
    aNode.Checked = aApp.ToSave
    If aApp.ToSave Then MustSave = True
Next

'-> Ajouter la ligne des mots cl�s
Set aNode = frmSave.TreeView1.Nodes.Add(, , "KEYWORD", "Mots cl�s et contextes")
aNode.Checked = frmKeyWords.ToSave
If frmKeyWords.ToSave Then MustSave = True

'-> Afficher la feuille de sauvegarde
If MustSave Then frmSave.Show vbModal

'-> Fermer tous les fichiers ouverts
Reset

End Sub


Public Function SaveGlobal() As Boolean

'---> Enregistre les donn�es globales � la documentation

Dim NumFic As Integer
Dim aSaveGlobal As SaveGlobal

On Error GoTo GestError

'-> Faire une sauvegarde si le fichier existe
If Dir$(DocPath & "Global.dat", vbNormal) <> "" Then
    CopyFile DocPath & "Global.dat", DocPath & "Global.sav", 0
    '-> Supprimer le fichier
    Kill DocPath & "Global.dat"
End If

'-> Obtenir un handle de fichier
NumFic = FreeFile

'-> Cr�ation du nouveau fichier
Open DocPath & "Global.dat" For Random As #NumFic Len = Len(aSaveGlobal)

'-> Affectation des valeurs
aSaveGlobal.IndexCtCreate = IndexCtCreate
aSaveGlobal.IndexNodeCreate = IndexNodeCreate

'-> Enregistrer la valeur
Put #NumFic, 1, aSaveGlobal

'-> Fermer le fichier
Close #NumFic

'-> Supprimer la sauvegarde
If Dir$(DocPath & "Global.sav", vbNormal) <> "" Then Kill DocPath & "Global.sav"

'-> Renvoyer une valeur de succes
SaveGlobal = True

'-> Quitter la fonction
Exit Function

GestError:

    '-> Une erreur s'est produit : Restaurer l'ancien fichier
    Close #NumFic
    If Dir$(DocPath & "Global.dat", vbNormal) <> "" Then Kill DocPath & "Global.dat"
    CopyFile DocPath & "Global.sav", DocPath & "Global.dat", 0
    If Dir$(DocPath & "Global.sav", vbNormal) <> "" Then Kill DocPath & "Global.sav"
    '-> Message d'erreur + fin
    MsgBox "Une erreur est intervenue dans l'enregistrement du fichier : " & Chr(13) & DocPath & "Global.dat" & Chr(13) & "V�rifiez si le disque n'est pas plein, si vous disposez des privil�ges n�cessaire ou si le fichier n'est pas en cours d'utlisation", vbCritical + vbOKOnly, "Erreur fatale"
    '-> Renvoyer une valeur d'erreur
    SaveGlobal = False
    
End Function


Public Function SaveApp(aApp As Applicatif) As Boolean

'---> Enregistre un applicatif

Dim NumFic As Integer
Dim NumFic2 As Integer
Dim aSaveMenu As SaveMenu
Dim aSaveLien As SaveLien
Dim NomMenu As String
Dim NomLien As String
Dim NomSavLien As String
Dim NomSavMenu As String
Dim aMenu As Menu
Dim i As Integer, j As Integer
Dim aNode As Node

On Error GoTo GestError

'-> Contruire le nom du menu
NomMenu = DocPath & aApp.Identifiant & "-Menu.dat"
NomLien = DocPath & aApp.Identifiant & "-Lien.dat"
NomSavMenu = Replace(UCase$(NomMenu), ".DAT", ".sav")
NomSavLien = Replace(UCase$(NomLien), ".DAT", ".sav")

'-> Faire des sauvegardes des fichiers s'ils existent
If Dir$(NomMenu, vbNormal) <> "" Then
    CopyFile NomMenu, NomSavMenu, 0
    Kill NomMenu
End If
If Dir$(NomLien, vbNormal) <> "" Then
    CopyFile NomLien, NomSavLien, 0
    Kill NomLien
End If

'-> Faire une simple suppression de fichier s'il n'y a rien � enregistrer
If aApp.Tree.Nodes.Count = 0 Then
    If Dir$(NomMenu, vbNormal) <> "" Then Kill NomMenu
    If Dir$(NomLien, vbNormal) <> "" Then Kill NomLien

Else
    '-> Enregistrement du fichier menu
    NumFic = FreeFile
    Open NomMenu For Random As #NumFic Len = Len(aSaveMenu)
    '-> Enregistrement du fichier Lien
    NumFic2 = FreeFile
    Open NomLien For Random As #NumFic2 Len = Len(aSaveLien)
    
    '-> Initialier les variables
    i = 1
    j = 1
    
    '-> Boucle d'analyse
    For Each aNode In aApp.Tree.Nodes
        Select Case aNode.Tag
            Case "CHAPITRE", "DOCUMENT"
                '-> Pointer sur le menu
                Set aMenu = aApp.Menus(UCase$(aNode.Key))
                '-> Faire le setting de l'objet � imprimer
                aSaveMenu.Key = aNode.Key
                If Not aNode.Parent Is Nothing Then
                    aSaveMenu.KeyParent = aNode.Parent.Key
                Else
                    aSaveMenu.KeyParent = Space$(50)
                End If
                aSaveMenu.Libelle1 = aMenu.GetLbByLangue(1)
                aSaveMenu.Libelle2 = aMenu.GetLbByLangue(2)
                aSaveMenu.Libelle3 = aMenu.GetLbByLangue(3)
                aSaveMenu.Libelle4 = aMenu.GetLbByLangue(4)
                aSaveMenu.Libelle5 = aMenu.GetLbByLangue(5)
                aSaveMenu.Libelle6 = aMenu.GetLbByLangue(6)
                aSaveMenu.Libelle7 = ""
                aSaveMenu.Libelle8 = ""
                aSaveMenu.Libelle9 = ""
                aSaveMenu.Libelle10 = ""
                If aNode.Tag = "DOCUMENT" Then
                    '-> Charger le fichier HTML
                    aSaveMenu.FichierHTML = aNode.Child.Text
                    '-> Enregistrer le lien s'il y a un
                    If Not aMenu.aLien Is Nothing Then
                        '-> Setting du lien
                        aSaveLien.KeyMenu = aNode.Key
                        aSaveLien.Keyword = aMenu.aLien.KeyWordKey
                        aSaveLien.KeyCt = aMenu.aLien.ContexteKey
                        aSaveMenu.IsLien = True
                        '-> Enregistrer le lien
                        Put #NumFic2, j, aSaveLien
                        '-> Incr�menter le compteur de lien
                        j = j + 1
                    Else
                        aSaveMenu.IsLien = False
                    End If
                    '-> Indiquer que c'est un fichier
                    aSaveMenu.IsUrl = True
                Else
                    aSaveMenu.FichierHTML = ""
                    aSaveMenu.IsUrl = False
                End If
                '-> Enregistrer le menu
                Put #NumFic, i, aSaveMenu
                '-> Incr�menter le compteur
                i = i + 1
        End Select 'Selon la nature du node
    Next 'Pour tous les nodes du menu
    '-> Fermer les fichiers
    Close #NumFic
    Close #NumFic2
End If 's'il y a des nodes

'-> Supprimer les sauvegardes
If Dir$(NomSavMenu, vbNormal) <> "" Then Kill NomSavMenu
If Dir$(NomSavLien, vbNormal) <> "" Then Kill NomSavLien

'-> Renvoyer une valeur de succ�s
SaveApp = True
aApp.ToSave = False

'-> Quitter la fonction
Exit Function

GestError:

    '-> Message d'erreur
    MsgBox "Impossible d'enregistrer le menu de l'application : " & Chr(13) & aApp.Name, vbCritical + vbOKOnly, "Erreur"
    '-> Supprimer les fichiers
    If Dir$(NomMenu, vbNormal) <> "" Then Kill NomMenu
    If Dir$(NomLien, vbNormal) <> "" Then Kill NomLien
    '-> Restituer les sauvegardes
    CopyFile NomSavMenu, NomMenu, 0
    CopyFile NomSavLien, NomLien, 0
    '-> Renvoyer une valeur d'erreur
    SaveApp = False
    MsgBox Err.Description
    
End Function

Public Function SaveKey() As Boolean

'---> Enregistre les mots cl�s et les contextes

Dim NumFic As Integer
Dim NumFic2 As Integer
Dim NomKey As String
Dim NomCt As String
Dim NomSavKey As String
Dim NomSavCt As String
Dim i As Integer, j As Integer
Dim aSaveKey As SaveKeyWord
Dim aSaveCt As SaveContexte
Dim aKeyWord As Keyword
Dim aCt As Contexte
Dim aSaveK As SaveKeyWord


On Error GoTo GestError

'-> Contruire le nom du menu
NomKey = DocPath & "Key.dat"
NomCt = DocPath & "Ct.dat"
NomSavKey = Replace(UCase$(NomKey), ".DAT", ".sav")
NomSavCt = Replace(UCase$(NomCt), ".DAT", ".sav")

'-> Faire des sauvegardes des fichiers s'ils existent
If Dir$(NomKey, vbNormal) <> "" Then
    CopyFile NomKey, NomSavKey, 0
    Kill NomKey
End If
If Dir$(NomCt, vbNormal) <> "" Then
    CopyFile NomCt, NomSavCt, 0
    Kill NomCt
End If

'-> Enregistrement du fichier menu
NumFic = FreeFile
Open NomKey For Random As #NumFic Len = Len(aSaveKey)
'-> Enregistrement du fichier Lien
NumFic2 = FreeFile
Open NomCt For Random As #NumFic2 Len = Len(aSaveCt)

'-> Initialier les variables
i = 1
j = 1

'-> Boucle d'analyse
For Each aKeyWord In frmKeyWords.Keywords
    '-> Enregistrer le mot cl�
    aSaveKey.KeyName = aKeyWord.Name
    Put #NumFic, i, aSaveKey
    '-> Incr�menter le compteur
    i = i + 1
    '-> Enregistrer les contextes associ�s
    For Each aCt In aKeyWord.Contextes
        '-> Setting de la variable d'enregistrement
        aSaveCt.Index = aCt.Index
        aSaveCt.Keyword = aSaveKey.KeyName
        aSaveCt.Libelle1 = aCt.GetLbByLangue(1)
        aSaveCt.Libelle2 = aCt.GetLbByLangue(2)
        aSaveCt.Libelle3 = aCt.GetLbByLangue(3)
        aSaveCt.Libelle4 = aCt.GetLbByLangue(4)
        aSaveCt.Libelle5 = aCt.GetLbByLangue(5)
        aSaveCt.Libelle6 = aCt.GetLbByLangue(6)
        aSaveCt.Libelle7 = ""
        aSaveCt.Libelle8 = ""
        aSaveCt.Libelle9 = ""
        aSaveCt.Libelle10 = ""
        '-> Enregistrer le contexte
        Put #NumFic2, j, aSaveCt
        '-> Incr�menter le compteur d'enregistrement
        j = j + 1
    Next 'Pour tous les contextes associ�s � un mot cl�
Next 'Pour tous les contextes

'-> Fermer les fichiers
Close #NumFic
Close #NumFic2

'-> Supprimer les sauvegardes
If Dir$(NomSavKey, vbNormal) <> "" Then Kill NomSavKey
If Dir$(NomSavCt, vbNormal) <> "" Then Kill NomSavCt

'-> Renvoyer une valeur de succ�s
SaveKey = True

'-> Indiquer que l'enregistrement est Ik
frmKeyWords.ToSave = False

'-> Quitter la fonction
Exit Function

GestError:
    '-> Fermer tous les fichiers
    Reset
    '-> Message d'erreur
    MsgBox "Impossible d'enregistrer les mots cl�s et les contextes.", vbCritical + vbOKOnly, "Erreur"
    '-> Supprimer les fichiers
    If Dir$(NomKey, vbNormal) <> "" Then Kill NomKey
    If Dir$(NomCt, vbNormal) <> "" Then Kill NomCt
    '-> Restituer les sauvegardes
    CopyFile NomSavKey, NomKey, 0
    CopyFile NomSavCt, NomCt, 0
    '-> Renvoyer une valeur d'erreur
    SaveKey = False

End Function

Public Function LoadGlobal()

'---> Chargement du fichier GLOBAL.DAT

Dim aSaveGlobal As SaveGlobal
Dim NumFic As Integer

If Dir$(DocPath & "Global.dat", vbNormal) <> "" Then
    '-> Ouverture du fichier
    NumFic = FreeFile
    Open DocPath & "Global.dat" For Random As #NumFic Len = Len(aSaveGlobal)
    Get #NumFic, 1, aSaveGlobal
    IndexCtCreate = aSaveGlobal.IndexCtCreate
    IndexNodeCreate = aSaveGlobal.IndexNodeCreate
    '-> Fermer le fichier
    Close #NumFic
End If
    
End Function

Public Sub LoadMenuApp(aApp As Applicatif)

'---> Proc�dure qui charge un applicatif

Dim NumFic As Integer
Dim i As Long
Dim NomFichier As String
Dim aSaveMenu As SaveMenu
Dim aMenu As Menu
Dim nbEnreg As Long
Dim aNode As Node
Dim aNode2 As Node

'-> Construire le nom du fichier � ouvrir
NomFichier = DocPath & aApp.Identifiant & "-Menu.dat"

If Dir$(NomFichier, vbNormal) <> "" Then
    '-> Ouvrir le fichier
    NumFic = FreeFile
    Open NomFichier For Random As #NumFic Len = Len(aSaveMenu)
    nbEnreg = LOF(NumFic) / Len(aSaveMenu)
    '-> Boucle d'analyse
    For i = 1 To nbEnreg
        '-> Lecture de l'enregistrement
        Get #NumFic, i, aSaveMenu
        '-> Cr�ation de l'enregistrement menu
        Set aMenu = New Menu
        aMenu.SetLbByLangue 1, Trim(aSaveMenu.Libelle1)
        aMenu.SetLbByLangue 2, Trim(aSaveMenu.Libelle2)
        aMenu.SetLbByLangue 3, Trim(aSaveMenu.Libelle3)
        aMenu.SetLbByLangue 4, Trim(aSaveMenu.Libelle4)
        aMenu.SetLbByLangue 5, Trim(aSaveMenu.Libelle5)
        aMenu.SetLbByLangue 6, Trim(aSaveMenu.Libelle6)
        '-> Ajout dans la collection
        aApp.Menus.Add aMenu, Trim(aSaveMenu.Key)
        '-> Cr�ation de la repr�sentation physique
        If Trim(aSaveMenu.KeyParent) = "" Then
            Set aNode = aApp.Tree.Nodes.Add(, , Trim(aSaveMenu.Key), aMenu.Libelle)
        Else
            Set aNode = aApp.Tree.Nodes.Add(Trim(aSaveMenu.KeyParent), 4, Trim(aSaveMenu.Key), aMenu.Libelle)
        End If
        '-> Setting de l'image
        If aSaveMenu.IsUrl Then
            aNode.Image = "Document"
            aNode.Tag = "DOCUMENT"
            '-> Ajouter le node enfant pour le fichier
            Set aNode2 = aApp.Tree.Nodes.Add(aNode.Key, 4, "MENU|" & IndexNodeCreate, Trim(aSaveMenu.FichierHTML), "FichierHTM")
            aNode2.Tag = "HTML"
            IndexNodeCreate = IndexNodeCreate + 1
            '-> Ajouter le node enfant pour le Mot cl� s'il n'existe pas, sinon il sera cr�� lors de la remont�e des liens
            If Not aSaveMenu.IsLien Then
                Set aNode2 = aApp.Tree.Nodes.Add(aNode.Key, 4, "MENU|" & IndexNodeCreate, "Pas d'affectation", "KeyWord#")
                aNode2.Tag = "KEYWORD"
                IndexNodeCreate = IndexNodeCreate + 1
            End If
        Else
            aNode.Image = "ChapitreFerme"
            aNode.ExpandedImage = "ChapitreOuvert"
            aNode.Tag = "CHAPITRE"
        End If
    Next 'pour tous les enregistrements
End If

'-> Fermer le fichier
Reset

'-> Indiquer que le menu de l'applicatif est charg�
aApp.IsLoaded = True

'-> Charger les liens
LoadLienApp aApp

End Sub


Public Sub LoadLienApp(aApp As Applicatif)

'---> Proc�dure qui cr�er les liens propres � un applicatifs

Dim NumFic As Integer
Dim i As Long
Dim NomFichier As String
Dim aSaveLien As SaveLien
Dim aMenu As Menu
Dim nbEnreg As Long
Dim aNode As Node
Dim aCt As Contexte
Dim aKeyWord As Keyword
Dim aLien As Lien

'On Error Resume Next

'-> Cr�ation du nom de fichier
NomFichier = DocPath & aApp.Identifiant & "-Lien.dat"

If Dir$(NomFichier, vbNormal) <> "" Then
    '-> Ouvrir le fichier
    NumFic = FreeFile
    Open NomFichier For Random As #NumFic Len = Len(aSaveLien)
    nbEnreg = LOF(NumFic) / Len(aSaveLien)
    '-> Boucle d'analyse
    For i = 1 To nbEnreg
        '-> Lecture de l'enregistrement
        Get #NumFic, i, aSaveLien
        '-> Tester si l'applicatif est charg�
        If aApp.IsLoaded Then
            '-> Pointer sur le menu associ� si le menu est charg�
            Set aMenu = aApp.Menus(Trim(UCase$(aSaveLien.KeyMenu)))
            '-> Initialiser le lien
            Set aMenu.aLien = New Lien
            aMenu.aLien.Applicatif = aApp.Name
            aMenu.aLien.ContexteKey = Trim(UCase$(aSaveLien.KeyCt))
            aMenu.aLien.KeyWordKey = Trim(UCase$(aSaveLien.Keyword))
            '-> Pointer sur le Mot cl� associ�
            Set aKeyWord = frmKeyWords.Keywords(aMenu.aLien.KeyWordKey)
            '-> Pointer sur le contexte associ�
            Set aCt = aKeyWord.Contextes(aMenu.aLien.ContexteKey)
            '-> Cr�er le node dans le menu
            Set aNode = aApp.Tree.Nodes.Add(Trim(UCase$(aSaveLien.KeyMenu)), 4, "MENU|" & IndexNodeCreate, aKeyWord.Name & " - " & aCt.Libelle, "KeyWord")
            aNode.Tag = "KEYWORD"
            '-> Incr�menter le compteur
            IndexNodeCreate = IndexNodeCreate + 1
        End If
        '-> Cr�ation de la notion de lien dans le menu de l'applicatif
        If Not aApp.LinkLoaded Then
            Set aLien = New Lien
            aLien.Applicatif = aApp.Name
            aLien.ContexteKey = Trim(UCase$(aSaveLien.KeyCt))
            aLien.KeyWordKey = Trim(UCase$(aSaveLien.Keyword))
            aApp.Liens.Add aLien, Trim(aSaveLien.KeyMenu) & "|" & aLien.KeyWordKey & "|" & aLien.ContexteKey 'Trim(UCase$(aSaveLien.KeyMenu))
        End If
    Next 'Por tous les liens
End If 'Si on trouve le fichier

'-> Indiquer pour cet applicatif que les liens sont charg�s
aApp.LinkLoaded = True

'-> Fermer les fichiers
Close #NumFic

End Sub

Public Sub InitDefautTraduction()

'-> Propri�t�s de traduction
For i = 1 To 6
    Select Case i
        Case 1
            idTraduction(i) = 2
            strTraduction(i) = "<Pas de traduction>"
        Case 2
            idTraduction(i) = 3
            strTraduction(i) = "<No translation>"
        Case 3
            idTraduction(i) = 3
            strTraduction(i) = "<Keine �bersetzung>"
        Case 4
            idTraduction(i) = 3
            strTraduction(i) = "<No traduc�o>"
        Case 5
            idTraduction(i) = 3
            strTraduction(i) = "<No traducion>"
        Case 6
            idTraduction(i) = 3
            strTraduction(i) = "<No traduzione esiste>"
    End Select
    lngTraduction(i) = &HC00000    'bleu
Next


End Sub
