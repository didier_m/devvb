VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPoubelle 
   Caption         =   "Poubelle"
   ClientHeight    =   6975
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9390
   Icon            =   "frmPoubelle.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   465
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   626
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   3495
      Left            =   0
      TabIndex        =   0
      Top             =   480
      Width           =   3135
      _ExtentX        =   5530
      _ExtentY        =   6165
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
      OLEDragMode     =   1
      OLEDropMode     =   1
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6360
      Top             =   1680
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPoubelle.frx":0442
            Key             =   "ChapitreFerme"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPoubelle.frx":111C
            Key             =   "ChapitreOuvert"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPoubelle.frx":19F6
            Key             =   "Document"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPoubelle.frx":22D0
            Key             =   "FichierHTM"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPoubelle.frx":2FAA
            Key             =   "KeyWord"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPoubelle.frx":3C84
            Key             =   "KeyWord#"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   4800
      Top             =   3120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPoubelle.frx":495E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPoubelle.frx":5638
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPoubelle.frx":6312
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmPoubelle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Menus As Collection
Public Liens As Collection

Private Sub Form_Activate()

ActiveMenuForm 999
HelpDeskTop.mnuFenetreListeApp.Checked = False
HelpDeskTop.mnuFenetreListeCli.Checked = False
HelpDeskTop.mnuFenetrePoubelle.Checked = True


End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

If KeyCode = 112 Then Shell App.Path & "\HELPDEAL.EXE DEAL|1|DEAL|HELPDESKTOP|CT35", vbNormalFocus

End Sub

Private Sub Form_Load()

Set Menus = New Collection
Set Liens = New Collection
Me.Width = Screen.Width / 4
Me.Height = Screen.Height / 4

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

'---> Ne fermer la documentation que si on en a l'autorisation
If Not IsTerminate Then
    Cancel = 1
    Me.Hide
    '-> Supprimer du menu d'affichage
    HelpDeskTop.mnuFenetrePoubelle.Visible = False
    HelpDeskTop.mnuPoubelle.Checked = False
End If

End Sub

Private Sub Form_Resize()

On Error Resume Next

Dim Res As Long
Dim aRect As Rect

'-> R�cup�rer la taille de la zone cliente
Res = GetClientRect(Me.hWnd, aRect)

'-> Positionner le treeview1
Me.TreeView1.Left = 0
Me.TreeView1.Top = 0
Me.TreeView1.Height = aRect.Bottom
Me.TreeView1.Width = aRect.Right



End Sub

Private Sub TreeView1_DragDrop(Source As Control, x As Single, y As Single)

Dim Res1 As Long
Dim Res As Long

'-> V�rifier que le D&D a bien �t� initialis�
If SourceNode Is Nothing Then Exit Sub

Res1 = GetKeyState%(VK_CONTROL)
If Not Res1 And &H80 Then Exit Sub


Res = GetKeyState%(VK_SHIFT)
If Res And &H80 Then
    AnalyseNode AppDrag, SourceNode, 2, "POUBELLE", Nothing, 4
Else
    AnalyseNode AppDrag, SourceNode, 1, "POUBELLE", Nothing, 4
End If

End Sub

Private Sub TreeView1_DragOver(Source As Control, x As Single, y As Single, State As Integer)

'-> Mettre la feuille au premier plan
Me.ZOrder

End Sub

Private Sub TreeView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

'-> Ne rien faire si pas de nodes
If Me.TreeView1.Nodes.Count = 0 Then Exit Sub

'-> Supprimer une source pr�c�dente de D&D
Set Me.TreeView1.DropHighlight = Nothing

'-> Sauvegarder le node source si la touche ctrl est affect�e
Set SourceNode = Me.TreeView1.HitTest(x, y)

If Button = vbRightButton Then
    Me.PopupMenu HelpDeskTop.mnufrmPoubelle
End If

End Sub

Private Sub TreeView1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

'---> Gestion du gliss� d�plac�

Dim aPt As POINTAPI
Dim Res As Long

If Me.TreeView1.Nodes.Count = 0 Then Exit Sub

If Button = vbLeftButton Then

    '-> Tester que la source est valide avant de lancer le drag & drop
    If SourceNode Is Nothing Then Exit Sub

    '-> Ne pas se d�placer si on bouge un fichier HTML
    If SourceNode.Tag = "HTML" Or SourceNode.Tag = "KEYWORD" Then Exit Sub
    
    '-> S�lectionner le node source
    Set Me.TreeView1.SelectedItem = SourceNode

    If SourceNode.Tag = "CHAPITRE" Then
        Me.TreeView1.DragIcon = Me.ImageList2.ListImages(1).ExtractIcon
    Else
        Me.TreeView1.DragIcon = Me.ImageList2.ListImages(3).ExtractIcon
    End If
    
    '-> Indiquer l'applicatif d'origine
    AppDrag = "POUBELLE"
    
    '-> Commencer le Drag & Drop si touche Ctrl activ�e
    Res = GetKeyState%(VK_CONTROL)
    If Res And &H80 Then Me.TreeView1.Drag 1
End If

End Sub
