VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmListeClient 
   Caption         =   "Liste des clients "
   ClientHeight    =   2175
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4710
   Icon            =   "frmListeClient.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   145
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   314
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3000
      Top             =   240
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmListeClient.frx":014A
            Key             =   "Client"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmListeClient.frx":02A4
            Key             =   "Rep"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmListeClient.frx":06F6
            Key             =   "NoRep"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmListeClient.frx":0850
            Key             =   "Code"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmListeClient.frx":09AA
            Key             =   "NoClient"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   2175
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   3836
      _Version        =   393217
      LabelEdit       =   1
      Sorted          =   -1  'True
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
End
Attribute VB_Name = "frmListeClient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public IndexMenu As Integer

Private Sub Form_Activate()

Dim aNode As Node

'-> Pointer sur le node propre au client si celui-ci est renseign�
If CurClient <> "" Then
    Set aNode = Me.TreeView1.Nodes("CLI" & UCase$(CurClient))
    aNode.Selected = True
    aNode.Expanded = True
End If
    
'-> Desactiver tous les autres noms de feuille
ActiveMenuForm 9999
'-> Activer celui de la liste des clients
HelpDeskTop.mnuFenetreListeApp.Checked = False
HelpDeskTop.mnuFenetreListeCli.Checked = True
HelpDeskTop.mnuFenetrePoubelle.Checked = False


End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

If KeyCode = 112 Then Shell App.Path & "\HELPDEAL.EXE DEAL|1|DEAL|HELPDESKTOP|CT30", vbNormalFocus

End Sub

Private Sub Form_Load()

'-> Charger la liste des cldients de DEAL
LoadListeClient

Me.Height = Screen.Height / 4
Me.Width = Screen.Width / 4

End Sub

Private Sub Form_LostFocus()
    If Me.Visible = False Then HelpDeskTop.mnuFenetreListeCli.Visible = False
End Sub

Private Sub Form_Resize()

Dim aRect As Rect
Dim Res As Long

Res = GetClientRect(Me.hWnd, aRect)
Me.TreeView1.Left = 0
Me.TreeView1.Top = 0
Me.TreeView1.Width = aRect.Right
Me.TreeView1.Height = aRect.Bottom

End Sub

Private Sub Form_Unload(Cancel As Integer)

CloseMe Cancel, Me
If Cancel = 1 Then
    HelpDeskTop.mnuListeClient.Checked = False
    Form_LostFocus
End If

End Sub

Private Sub TreeView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

'---> Afficher le menu d�roulant

If Me.TreeView1.Nodes.Count = 0 Then Exit Sub

'-> Afficher les propri�t�s du client en cours
If Button = vbRightButton Then
    '-> V�rifier que le client est enabled
    If Me.TreeView1.SelectedItem.Image = "Client" Then
        HelpDeskTop.mnuLoadClient.Enabled = True
    Else
        HelpDeskTop.mnuLoadClient.Enabled = False
    End If
    Me.PopupMenu HelpDeskTop.mnuClient
End If

End Sub
