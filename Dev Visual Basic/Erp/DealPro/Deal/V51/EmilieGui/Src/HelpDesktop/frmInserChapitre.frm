VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmInsertChapitre 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Insertion d'un chapitre"
   ClientHeight    =   1950
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5340
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1950
   ScaleWidth      =   5340
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      Height          =   1935
      Left            =   30
      TabIndex        =   0
      Top             =   0
      Width           =   5295
      Begin VB.TextBox lblLibelle 
         Height          =   285
         Left            =   120
         TabIndex        =   1
         Top             =   1500
         Width           =   4935
      End
      Begin MSComctlLib.ImageCombo ImageCombo1 
         Height          =   330
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Width           =   2655
         _ExtentX        =   4683
         _ExtentY        =   582
         _Version        =   393216
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Locked          =   -1  'True
      End
      Begin VB.Image Image4 
         Height          =   480
         Left            =   4560
         Picture         =   "frmInserChapitre.frx":0000
         ToolTipText     =   "Ok"
         Top             =   240
         Width           =   630
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Libell� associ�  : (maximum 70 caract�res )"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   4215
      End
   End
End
Attribute VB_Name = "frmInsertChapitre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private pLibelle(6) As String
Public NodeChapitre As Node
Private idLangue As Integer
Public Keyword As Keyword
Public Contexte As Contexte

Private Sub Command1_Click()


End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

If KeyAscii = 27 Then Unload Me

End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

If KeyCode = 112 Then Shell App.Path & "\HELPDEAL.EXE DEAL|1|DEAL|HELPDESKTOP|CT31", vbNormalFocus

End Sub

Private Sub Form_Load()

Dim aApp As Applicatif
Dim aMenu As Menu

'-> Charger la Combo1
Me.ImageCombo1.ImageList = frmLangue.ImageList1
For i = 1 To 6
    Me.ImageCombo1.ComboItems.Add i, , frmLangue.ImageList1.ListImages(i).Tag, i
Next

Erase pLibelle

idLangue = 1

If NodeChapitre Is Nothing Then
    '-> Soit c'est un ajout de mot cl� , soit de contexte
    If Me.Keyword Is Nothing Then
        '-> Ajout d'un Nouveau Node
        pLibelle(1) = "Nouveau Chapitre"
        Me.ImageCombo1.ComboItems(1).Selected = True
        Me.lblLibelle.Text = pLibelle(1)
    Else
        
        If Contexte Is Nothing Then
            '-> Ajout d'un nouveau contexte
            Me.Caption = "Ajout d'un nouveau contexte"
            pLibelle(1) = "Nouveau Contexte"
            Me.ImageCombo1.ComboItems(1).Selected = True
            Me.lblLibelle.Text = pLibelle(1)
        Else
            '-> Intialiser les libelles
            Me.Caption = "Modification d'un contexte"
            For i = 1 To 6
                pLibelle(i) = Contexte.GetLbByLangue(i)
            Next
            idLangue = CurCodeLangue
            Me.ImageCombo1.ComboItems(CurCodeLangue).Selected = True
            Me.lblLibelle.Text = pLibelle(CurCodeLangue)
        End If
    End If
Else
    '-> Pointer sur l'applicatif
    Set aApp = Applicatifs(CurApp)
    '-> Cr�er les menus
    Set aMenu = aApp.Menus(NodeChapitre.Key)
    For i = 1 To 6
        pLibelle(i) = aMenu.GetLbByLangue(i)
    Next
    '-> S�lectionner le code langue par d�faut
    Me.ImageCombo1.ComboItems(CurCodeLangue).Selected = True
    Me.lblLibelle.Text = pLibelle(CurCodeLangue)
    '-> Positionner l'index de la langue en cours
    idLangue = CurCodeLangue
    
End If 'Si aout d'un nouveau chapitre

End Sub

Private Sub Image4_Click()

Dim aApp As Applicatif
Dim aMenu As Menu
Dim aNode As Node
Dim aCt As Contexte

'-> Pointer sur l'applicatif
Set aApp = Applicatifs(UCase$(CurApp))

'-> V�rifier que le libell� en fran�ais est r�f�renc�
If Trim(pLibelle(1)) = "" Then
    MsgBox "Veuillez renseigner le libell� fran�ais.", vbExclamation + vbOKOnly, "Erreur"
    Me.ImageCombo1.SetFocus
    Exit Sub
End If

'-> Selon ajout ou modification
If NodeChapitre Is Nothing Then

    '-> Si ajout de  nouveau chapitre
    If Me.Keyword Is Nothing Then
        '-> Cr�er le node qui va bien
        If aApp.Tree.Nodes.Count = 0 Then
            '-> A ajouter � la racine
            Set aNode = aApp.Tree.Nodes.Add(, , "MENU|" & IndexNodeCreate, pLibelle(CurCodeLangue), "ChapitreFerme")
        Else
            '-> Faire la demande du niveau d'insertion
            RetourChx = -1
            If aApp.Tree.SelectedItem.Tag = "DOCUMENT" Then
                frmChxDest.Option1(4).Enabled = False
                frmChxDest.Option1(1).Value = True
            End If
            frmChxDest.Show vbModal
            If RetourChx = -1 Then
                Unload Me
                Exit Sub
            End If
            '-> Ajouter sous le node en cours de s�lection
            Set aNode = aApp.Tree.Nodes.Add(aApp.Tree.SelectedItem.Index, RetourChx, "MENU|" & IndexNodeCreate, pLibelle(CurCodeLangue), "ChapitreFerme")
        End If
    
        '-> Image pour ouverture
        aNode.ExpandedImage = "ChapitreOuvert"
        '-> Ouvrir le node en cours de s�lection
        aNode.Selected = True
        '-> Faire en sorte que le nouveau node soit visible
        aNode.EnsureVisible
        '-> Indiquer que c'est un chapitre
        aNode.Tag = "CHAPITRE"
    
        '-> Initialiser le nouvel objet menu
        Set aMenu = New Menu
        
        '-> Ajouter dans la collection des menus de l'applicatif en cours
        aApp.Menus.Add aMenu, "MENU|" & IndexNodeCreate
    
        '-> Incr�menter le compteur de node cr��
        IndexNodeCreate = IndexNodeCreate + 1
    
    Else '-> C'est un ajout de contexte
                
         '-> Selon la nature
        If Not Me.Contexte Is Nothing Then
            For i = 1 To 6
                Contexte.SetLbByLangue i, pLibelle(i)
            Next
            '-> Modifier l'affichae du treeview
            frmKeyWords.TreeView1.Nodes("CT" & Me.Contexte.Index).Text = pLibelle(CurCodeLangue)
            
            Unload Me
            Exit Sub
        End If
                
        '-> V�rifier si le contexte n'existe pas d�ja
        For Each aCt In Keyword.Contextes
            If Trim(UCase$(pLibelle(1))) = UCase$(aCt.GetLbByLangue(1)) Then
                MsgBox "Impossible de cr�er le contexte : celui-ci existe d�ja.", vbCritical + vbOKOnly, "Erreur"
                Me.lblLibelle.SetFocus
                Exit Sub
            End If
        Next
        '-> cr�er le nouveau contexte et l'ajouter
        Set aCt = New Contexte
        aCt.Keyword = Keyword.Name
        aCt.Index = IndexCtCreate
        IndexCtCreate = IndexCtCreate + 1
        For i = 1 To 6
            aCt.SetLbByLangue i, pLibelle(i)
        Next
        Keyword.Contextes.Add aCt, "CT" & aCt.Index
        
        '-> l'ajouter dans la liste des contextes
        Set aNode = frmKeyWords.TreeView1.Nodes.Add(, , "CT" & aCt.Index, aCt.Libelle, "CT")
        aNode.Selected = True
        '-> D�charger la feuille
        Unload Me
        frmKeyWords.TreeView1.SetFocus
        '-> Quitter la proc�dure
        Exit Sub
        
    End If
    
Else
   
    '-> Modification
    Set aNode = Me.NodeChapitre
    
    '-> Pointer sur le menu
    Set aMenu = aApp.Menus(aNode.Key)
    
    aNode.Text = pLibelle(CurCodeLangue)
        
End If

For i = 1 To 6
    aMenu.SetLbByLangue i, pLibelle(i)
Next
aMenu.idLangueOrigine = 1

'-> Decharger la feuille
Unload Me

'-> Indiquer que le nouveau chapitre est la source de DD
Set SourceNode = aNode

'-> Indiquer dans l'application d'origine que l'on doit enregistrer
aApp.ToSave = True

aApp.Tree.SetFocus
aNode.Selected = True



End Sub

Private Sub ImageCombo1_Click()

pLibelle(idLangue) = Me.lblLibelle.Text
Me.lblLibelle.Text = pLibelle(Me.ImageCombo1.SelectedItem.Index)
idLangue = Me.ImageCombo1.SelectedItem.Index


End Sub

Private Sub ImageCombo1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then
    Me.lblLibelle.SetFocus
    If Me.ImageCombo1.SelectedItem Is Nothing Then _
    Me.ImageCombo1.ComboItems(idLangue).Selected = True
End If


End Sub

Private Sub lblLibelle_Change()

pLibelle(Me.ImageCombo1.SelectedItem.Index) = Me.lblLibelle.Text

End Sub

Private Sub lblLibelle_GotFocus()

Me.lblLibelle.SelStart = 0
Me.lblLibelle.SelLength = Len(Me.lblLibelle.Text)

End Sub

Private Sub lblLibelle_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Image4_Click

End Sub
