VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSearch 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Recherche"
   ClientHeight    =   2415
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4680
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2415
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   1900
      Left            =   4800
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   480
      Width           =   5055
      _ExtentX        =   8916
      _ExtentY        =   3360
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   4200
      Picture         =   "frmSearch.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Commencer la recherche"
      Top             =   430
      Width           =   375
   End
   Begin VB.TextBox strFind 
      Height          =   285
      Left            =   60
      TabIndex        =   0
      Top             =   480
      Width           =   4095
   End
   Begin VB.Frame Frame1 
      Caption         =   "Rechercher dans : "
      Height          =   1530
      Left            =   60
      TabIndex        =   5
      Top             =   840
      Width           =   4575
      Begin VB.CheckBox Check1 
         Caption         =   "Dans les mots cl�s , les contextes associ�s"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   3
         Top             =   1080
         Value           =   1  'Checked
         Width           =   3855
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Dans les noms de fichier HTML"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   2
         Top             =   720
         Value           =   1  'Checked
         Width           =   3855
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Dans les titres des chapitres , documents d'aide"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Value           =   1  'Checked
         Width           =   3855
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSearch.frx":054E
            Key             =   "ChapitreFerme"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSearch.frx":1228
            Key             =   "ChapitreOuvert"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSearch.frx":1B02
            Key             =   "Document"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSearch.frx":23DC
            Key             =   "FichierHTM"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSearch.frx":30B6
            Key             =   "KeyWord"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSearch.frx":3D90
            Key             =   "KeyWord#"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label2 
      Caption         =   "R�sultat de la recherche : "
      Height          =   255
      Left            =   4800
      TabIndex        =   7
      Top             =   120
      Width           =   3975
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Texte � rechercher : "
      Height          =   255
      Left            =   60
      TabIndex        =   6
      Top             =   120
      Width           =   1935
   End
End
Attribute VB_Name = "frmSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim aApp As Applicatif
Private Sub Command1_Click()

Dim i As Integer
Dim AddNode As Boolean
Dim aNode As Node


'-> Vider le treeview
Me.TreeView1.Nodes.Clear

'-> Ne pas lancer la recherche si critere = "" quitter
If Trim(strFind) = "" Then Exit Sub

Me.Enabled = False
Me.MousePointer = 11

'-> Analyse des nodes
For i = 1 To aApp.Tree.Nodes.Count
    AddNode = False
    If InStr(1, UCase$(aApp.Tree.Nodes(i).Text), UCase$(Me.strFind.Text)) <> 0 Then
        Select Case aApp.Tree.Nodes(i).Tag
            Case "CHAPITRE", "DOCUMENT"
                If Me.Check1(0).Value = 1 Then AddNode = True
            Case "HTML"
                If Me.Check1(1).Value = 1 Then AddNode = True
            Case "KEYWORD", "CONTEXTE"
                If Me.Check1(2).Value = 1 Then AddNode = True
        End Select
        '-> Cr�er le node dans le treeview
        If AddNode Then Set aNode = Me.TreeView1.Nodes.Add(, , aApp.Tree.Nodes(i).Key, aApp.Tree.Nodes(i).Text, aApp.Tree.Nodes(i).Image)
    End If
Next 'pour tous les nodes

'-> Redimensionner la feuille
Me.Width = Me.TreeView1.Left + Me.TreeView1.Width + Me.strFind.Left + Me.ScaleX(LargeurBordure * 2, 3, 1)

'-> S�lectionner le premier node s'il y en a
If Me.TreeView1.Nodes.Count <> 0 Then Me.TreeView1.Nodes(1).Selected = True

Me.MousePointer = 0
Me.Enabled = True

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then Unload Me
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

    If KeyCode = 112 Then Shell App.Path & "\HELPDEAL.EXE DEAL|1|DEAL|HELPDESKTOP|CT39", vbNormalFocus

End Sub

Private Sub Form_Load()

'-> Pointer sur l'applicatif
Set aApp = Applicatifs(CurApp)

End Sub

Private Sub strFind_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Command1_Click

End Sub

Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)

Dim aNode As Node



'-> Atteindre le bon node
aApp.Tree.Nodes(Me.TreeView1.SelectedItem.Key).Selected = True
aApp.Tree.Nodes(Me.TreeView1.SelectedItem.Key).EnsureVisible

'-> D�charger la feuille
Unload Me

'-> Donner le focus au bon treeview
aApp.Tree.SetFocus

End Sub
