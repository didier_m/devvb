Attribute VB_Name = "ApiWin32"
'-> R�cup�ration des caract�ristiques d'une fen�tre
Public Declare Function GetSystemMetrics& Lib "user32" (ByVal nIndex As Long)
'-> Constantes pour GetSystemMetrics
Public Const SM_CXEDGE& = 45 'Largeur d'une bordure double
Public Const SM_CYEDGE& = 46
Public Const SM_CYSMCAPTION& = 51
Public Const SM_CYCAPTION& = 4

'-> API pour GDI
Public Type Rect
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Public Type POINTAPI
    x As Long
    y As Long
End Type

Public Type FILETIME
    dwLowDateTime As Long
    dwHighDateTime As Long
End Type



'-> API pour lecture du clavier
Public Const VK_SHIFT& = &H10
Public Const VK_CONTROL& = &H11
Public Declare Function GetKeyState% Lib "user32" (ByVal nVirtKey As Long)

'-> API pour concertion de coordonn�es
Public Declare Function ScreenToClient& Lib "user32" (ByVal hWnd As Long, lpPoint As POINTAPI)
Public Declare Function ClientToScreen& Lib "user32" (ByVal hWnd As Long, lpPoint As POINTAPI)

'-> API de lecture des fichiers au format "*.ini"
Public Declare Function GetPrivateProfileSection& Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As Any, ByVal lpFileName As String)

'-> API pour gestion d'une fen�tre
Public Declare Function GetClientRect& Lib "user32" (ByVal hWnd As Long, lpRect As Rect)
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function GetWindowRect Lib "user32" (ByVal hWnd As Long, lpRect As Rect) As Long
Public Declare Function SendMessage& Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any)
Public Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long) As Long
Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Public Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Public Declare Function IsWindow& Lib "user32" (ByVal hWnd As Long)
Public Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long
Public Declare Function BringWindowToTop& Lib "user32" (ByVal hWnd As Long)
Public Declare Function ChildWindowFromPoint Lib "user32" (ByVal hWnd As Long, ByVal xPoint As Long, ByVal yPoint As Long) As Long
Public Declare Function WindowFromPoint& Lib "user32" (ByVal x As Long, ByVal y As Long)
Public Declare Function SetActiveWindow& Lib "user32" (ByVal hWnd As Long)
Public Declare Function DestroyWindow& Lib "user32" (ByVal hWnd As Long)
Public Declare Function ShowWindow& Lib "user32" (ByVal hWnd As Long, ByVal nCmdShow As Long)

'-> Api gestion des fichiers tempo
Public Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Public Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long

'-> Api pour gestion des fichiers
Public Declare Function CloseHandle& Lib "kernel32" (ByVal hObject As Long)
Public Declare Function OpenFile& Lib "kernel32" (ByVal lpFileName As String, lpReOpenBuff As OFSTRUCT, ByVal wStyle As Long)
Public Declare Function GetFileSize& Lib "kernel32" (ByVal hFile As Long, lpFileSizeHigh As Long)
Public Declare Function CopyFile& Lib "kernel32" Alias "CopyFileA" (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal bFailIfExists As Long)

'-> Pour gestion des fichiers
Public Const OFS_MAXPATHNAME& = 128
Public Const OF_EXIST& = &H4000
Public Const OF_READ& = &H0
Public Const FILE_SHARE_READ& = &H1
Public Const FILE_SHARE_WRITE& = &H2
Public Const CREATE_ALWAYS& = 2
Public Const CREATE_NEW& = 1
Public Const FILE_ATTRIBUTE_ARCHIVE& = &H20
Public Const FILE_ATTRIBUTE_COMPRESSED& = &H800
Public Const FILE_ATTRIBUTE_DIRECTORY& = &H10
Public Const FILE_ATTRIBUTE_HIDDEN& = &H2
Public Const FILE_ATTRIBUTE_NORMAL& = &H80
Public Const FILE_ATTRIBUTE_READONLY& = &H1
Public Const FILE_ATTRIBUTE_SYSTEM& = &H4
Public Const FILE_ATTRIBUTE_TEMPORARY& = &H100
Public Const GENERIC_WRITE& = &H40000000
Public Const GENERIC_READ& = &H80000000
Public Const GENERIC_ALL& = &H10000000
Public Const GENERIC_EXECUTE& = &H20000000

'-> Message de fermeture
Public Const WM_CLOSE& = &H10

Public Type OFSTRUCT
    cBytes As Byte
    fFixedDisk As Byte
    nErrCode As Integer
    Reserved1 As Integer
    Reserved2 As Integer
    szPathName(OFS_MAXPATHNAME) As Byte
End Type

'-> Pour StWindowPos
Public Const SWP_NOSIZE& = &H1
Public Const SW_SHOW& = 5

