VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "SHDOCVW.DLL"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPrint 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Impression"
   ClientHeight    =   4275
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4290
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4275
   ScaleWidth      =   4290
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin SHDocVwCtl.WebBrowser WebBrowser1 
      Height          =   1095
      Left            =   5760
      TabIndex        =   6
      Top             =   480
      Width           =   2055
      ExtentX         =   3625
      ExtentY         =   1931
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
   Begin VB.PictureBox Picture1 
      AutoRedraw      =   -1  'True
      Height          =   255
      Left            =   120
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   269
      TabIndex        =   5
      Top             =   3480
      Visible         =   0   'False
      Width           =   4095
   End
   Begin VB.ListBox List1 
      Height          =   1230
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   4095
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Imprimer"
      Height          =   375
      Left            =   3000
      TabIndex        =   2
      Top             =   3840
      Width           =   1215
   End
   Begin MSComctlLib.ImageCombo ImageCombo1 
      Height          =   330
      Left            =   120
      TabIndex        =   1
      Top             =   2400
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   582
      _Version        =   393216
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Locked          =   -1  'True
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Choix de la langue"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   2040
      Width           =   3615
   End
   Begin VB.Label lblTitre 
      Caption         =   "Choix de l'imprimante :"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4095
   End
End
Attribute VB_Name = "frmPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---> Selon le type d'impression 0 -> Menu 1 ,Mots cl�s , 2
Public TypeImp As Integer
Dim idLangue As Integer

Private Sub Command1_Click()

'---> Lancer l'impression
On Error Resume Next

Select Case TypeImp
    
    Case 0 'Impression de la documentation
            PrintDocumentation
    Case 1 'Impression des rubriques du menu
            PrintMenu
    Case 2 'Impression des mots cl�s
            PrintKeyWord
End Select

End Sub
Private Sub PrintDocumentation()

Dim i As Integer
Dim aApp As Applicatif
Dim aNode As Node

'-> Pointer sur l'applicatif
Set aApp = Applicatifs(CurApp)

'-> Afficher le progress de travail
Me.Picture1.Visible = True
Me.Picture1.Cls

For i = 1 To aApp.Tree.Nodes.Count
    '-> Pointer sur le node
    Set aNode = aApp.Tree.Nodes(i)
    '-> V�rifier si c'est un fichier HTM
    If aNode.Tag = "HTML" Then
        If Dir$(DocPath & aNode.Text, vbNormal) <> "" And aNode.Text <> "" Then
            Me.WebBrowser1.Navigate DocPath & aNode.Text
            DoEvents
            Me.WebBrowser1.ExecWB OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER
        End If
    End If
    DoEvents
    Me.Picture1.Line (0, 0)-((i / aApp.Tree.Nodes.Count) * Me.Picture1.ScaleWidth, Me.Picture1.ScaleHeight), &HC00000, BF
Next

'-> D�charger la feuille
Unload Me

End Sub


Private Sub Form_Load()

Dim x As Printer

'-> Charger la Combo1 pour les codes langues
Me.ImageCombo1.ImageList = frmLangue.ImageList1
For i = 1 To 6
    Me.ImageCombo1.ComboItems.Add i, , frmLangue.ImageList1.ListImages(i).Tag, i
Next
Me.ImageCombo1.ComboItems(CurCodeLangue).Selected = True


'-> Ajouter les imprimantes dans la liste
For Each x In Printers
    Me.List1.AddItem x.DeviceName
    '-> S�lectionner dans la liste si c'est l'imprimante par d�faut
    If x.DeviceName = Printer.DeviceName Then _
        Me.List1.Selected(Me.List1.ListCount - 1) = True
Next

'-> Ajouter la visualisation �cran
If Me.TypeImp = 2 Then Me.List1.AddItem "Visualisation �cran"

idLangue = CurCodeLangue

End Sub
Private Sub PrintMenu()

'---> Impression du menu de l'applicatif s�lectionn�
Dim aApp As Applicatif
Dim aNode As Node
Dim FindPrinter As Boolean
Dim i As Integer, j As Integer, K As Integer
Dim aMenu As Menu
Dim strIndent As String
Dim PosY As Long
Dim PosX As Long
Dim LibToPrint As String
Dim IdNode As Integer

'-> Pointer sur l'applicatif
Set aApp = Applicatifs(CurApp)

'-> Pointer sur la bonne imprimante
FindPrinter = False
For Each x In Printers
    If x.DeviceName = Me.List1.Text Then
        Set Printer = x
        FindPrinter = True
        Exit For
    End If
Next

'-> Quitter si pas trouv� l'imprimante
If Not FindPrinter Then
    MsgBox "Impossible de trouver l'imprimante sp�cifi�e : " & Chr(13) & Me.List1.Text, vbCritical + vbOKOnly, "Impossible d'imprimer"
    Unload Me
    Exit Sub
End If

'-> Init de la feuille
Me.Enabled = False
Me.MousePointer = 11

'-> Rendre la barre de dessin visible
Me.Picture1.Visible = True
Me.Picture1.Cls

'-> Intialiser le printer
Printer.ScaleMode = 3 'Pixel

'-> Imprimer le titre du treeview
Printer.Print "Impression du menu de l'applicatif : "
Printer.Print aApp.Name & "  -  " & aApp.Descriptif
Printer.Print "Code langue : " & Me.ImageCombo1.Text
Printer.Print Chr(13) & Chr(10)

'-> Positionner � 3 cm
PosY = Printer.ScaleY(3, 7, 3)

'-> S�lectionner le premier node
aApp.Tree.Nodes(1).Selected = True

'-> R�cup�rer le premier index
IdNode = aApp.Tree.SelectedItem.Root.FirstSibling.Index

'-> Compteur d'impression
K = 1

'-> Boucle d'analyse
Do
    '-> Lancer l'analyse du treeview
    AnalyseNode CurApp, aApp.Tree.Nodes(IdNode), 999, "", Nothing, 0

    '-> Imprimer le treeview
    For i = 1 To UBound(KeyNodes())
        Set aNode = aApp.Tree.Nodes(KeyNodes(i))
        strIndent = Space$(5 * Niveaux(i))
        If aNode.Tag = "CHAPITRE" Then
            '-> Pointer sur le menu
            Set aMenu = aApp.Menus(KeyNodes(i))
            '-> Charger le libelle � imprimer
            LibToPrint = strIdent & aMenu.Libelle
        Else
            '-> Charger le libelle � imprimer
            LibToPrint = strIdent & aNode.Text
        End If
        '-> Position de d�but d'impression
        PosX = Printer.ScaleX(Niveaux(i) * (1 / 2), 7, 3)
        '-> Position de l'icone
        Printer.PaintPicture aApp.Tree.ImageList.ListImages(aNode.Image).Picture, PosX, PosY
        '-> Position d'impression
        Printer.CurrentY = PosY + (Printer.TextHeight(LibToPrint) / 2)
        Printer.CurrentX = PosX + Printer.ScaleX(aApp.Tree.ImageList.ListImages(aNode.Image).Picture.Width, 1, 3)
        '-> Imprimer le libelle
        Printer.Print LibToPrint
        '-> R�ajuster la position d'impression
        PosY = PosY + Printer.ScaleY(1, 7, 3)
        Printer.CurrentY = PosY
        '-> Mettre � jour la barre d'impression
        DoEvents
        Me.Picture1.Line (0, 0)-((K / aApp.Tree.Nodes.Count) * Me.Picture1.ScaleWidth, Me.Picture1.ScaleHeight), &HC00000, BF
        '-> Gestion des sauts de page
        If PosY >= Printer.ScaleHeight - Printer.ScaleY(1, 7, 3) Then
            '-> Faire un saut de page
            Printer.NewPage
            '-> Imprimer le titre du treeview
            Printer.Print "Impression du menu de l'applicatif : "
            Printer.Print aApp.Name & "  -  " & aApp.Descriptif
            Printer.Print "Code langue : " & Me.ImageCombo1.Text
            Printer.Print Chr(13) & Chr(10)
            '-> Positionner � 3 cm
            PosY = Printer.ScaleY(3, 7, 3)
        End If
        '-> Incr�menter le compteur
        K = K + 1
    Next 'Pour tous les nodes
    
    '-> Quitter si dernier node du premier niveau
    If aApp.Tree.Nodes(IdNode).Root.LastSibling.Index = IdNode Then Exit Do
    
    '-> R�cup�rer l'index du node de premier niveau suivant
    IdNode = aApp.Tree.Nodes(IdNode).Root.Next.Next.Index
    
Loop 'Pour tous les premiers niveaux

'-> Fin de l'impression
Printer.EndDoc

'-> Fin de l'impression
Unload Me

End Sub


Private Sub PrintKeyWord()

'---> Impression de la liste par le turbograph


'-> G�n�ration du fichier
Dim NumFic As Integer
Dim TempFileName As String
Dim MaqPath As String
Dim i As Integer 'Pour compter les lignes
Dim aKeyWord As Keyword, aCt As Contexte
Dim aNode As Node
Dim j As Integer, K As Integer
Dim Hauteur As Double
Dim PathPrint As String
Dim PathBmp As String
Dim StrLangue As String
Dim strEntete As String

'-> Obtenir un nom de fichier temporaire
TempFileName = GetTempFileNameVB("TBG")

'-> R�cup�rer les valeurs des mots cl�s
GetPathMotCles

'-> Construire le path de l'emplacement maquette
MaqPath = PathLecteur & "\DEALPRO\DEAL\" & PathVersion & "\EMILIE\BMP\td-editkey.maqgui"
PathBmp = PathLecteur & "\DEALPRO\DEAL\" & PathVersion & "\EMILIE\BMP\"
Select Case Me.ImageCombo1.SelectedItem.Index
    Case 1
        PathBmp = PathBmp & "CtrFrance.ico"
        StrLangue = "Fran�ais"
    Case 2
        PathBmp = PathBmp & "CtrAngleterre.ico"
        StrLangue = "Anglais"
    Case 3
        PathBmp = PathBmp & "CtrAllemagne.ico"
        StrLangue = "Allemand"
    Case 4
        PathBmp = PathBmp & "CtrPortugal.ico"
        StrLangue = "Portugais"
    Case 5
        PathBmp = PathBmp & "CtrEspagne.ico"
        StrLangue = "Espagnol"
    Case 6
        PathBmp = PathBmp & "CtrItaly.ico"
        StrLangue = "Italien"
End Select

'-> Ouverture du fichier Temporaire
NumFic = FreeFile
Open TempFileName For Output As #NumFic

'-> Impression de la chaine de localisation de la maquette
Print #NumFic, "%%GUI%%" & MaqPath

'-> Imprimer l'entete
strEntete = "[ST-ENTETE(TXT-SECTION)[]{^0001" & CurClient & "  " & frmListeClient.TreeView1.SelectedItem & " ^0004" & PathBmp & " ^0005" & StrLangue & "  }"
Print #NumFic, strEntete

'-> Rendre l'affichage visible
Me.Picture1.Visible = True
Me.Picture1.Cls

i = 1
For K = 1 To frmKeyWords.TreeView2.Nodes.Count
    '-> Pointer sur le node
    Set aNode = frmKeyWords.TreeView2.Nodes(K)
    '-> Pointer sur le menu associ�
    Set aKeyWord = frmKeyWords.Keywords(aNode.Key)
    '-> Imprimer une ligne
    If i = 25 Then
        PrintEntete NumFic, strEntete
        i = 1
    End If
    
    '-> Imprimer la ligne du mot cl�
    Print #NumFic, "[ST-EDITKEYWORD(TXT-SECTION)[]{^0002" & aKeyWord.Name & "    }"
    '-> Incr�menter le compteur
    i = i + 1
        
    '-> Imprimer la liste des contextes
    For j = 1 To aKeyWord.Contextes.Count
        If i = 25 Then
            PrintEntete NumFic, strEntete
            i = 1
        End If
        Set aCt = aKeyWord.Contextes(j)
        Print #NumFic, "[ST-EDITCONTEXTE(TXT-SECTION)[]{^0003" & aCt.GetLbByLangue(Me.ImageCombo1.SelectedItem.Index) & "    }"
        i = i + 1
        
    Next
    DoEvents
    Me.Picture1.Line (0, 0)-((K / frmKeyWords.TreeView2.Nodes.Count) * Me.Picture1.ScaleWidth, Me.Picture1.ScaleHeight), &HC00000, BF
    
Next 'Pour tous les nodes

'-> Fermer le fichier tempo
Close #NumFic

'-> Path du TurboPrint
If InStr(1, UCase$(App.Path), "EMILIEGUI\SRC") <> 0 Then
    PathPrint = "D:\ERP\DEALPRO\DEAL\V51\EMILIEGUI\EXE\Turbograph.exe "
Else
    PathPrint = App.Path & "\TurboGraph.exe "
End If

'-> Imprimer
If Me.List1.Text = "Visualisation �cran" Then
    PathPrint = PathPrint & "ECRAN|" & TempFileName
Else
    PathPrint = PathPrint & Me.List1.Text & "$ECRAN|" & TempFileName
End If

'-> Lancer l'ex�cution
Shell PathPrint, vbNormalFocus

'-> D�charger la feuille
Unload Me

End Sub
Private Sub PrintEntete(NumFic As Integer, strEntete As String)

'-> Imprimer un saut de page
Print #NumFic, "[PAGE]"
'-> Imprimer l'entete
Print #NumFic, strEntete

End Sub

Private Sub ImageCombo1_Click()

If Not Me.ImageCombo1.SelectedItem Is Nothing Then _
    idLangue = Me.ImageCombo1.SelectedItem.Index

End Sub

Private Sub ImageCombo1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then
    If Me.ImageCombo1.SelectedItem Is Nothing Then _
    Me.ImageCombo1.ComboItems(idLangue).Selected = True
End If


End Sub
