VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmKeyWords 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Catalogue des mots cl�s"
   ClientHeight    =   6075
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5310
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6075
   ScaleWidth      =   5310
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   5280
      Top             =   360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmKeyWords.frx":0000
            Key             =   "CT"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmKeyWords.frx":0CDA
            Key             =   "KW"
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame2 
      Caption         =   "Contextes associ�s : "
      Height          =   2895
      Left            =   60
      TabIndex        =   4
      Top             =   3120
      Width           =   5175
      Begin VB.CommandButton ModifCt 
         Caption         =   "Modifier"
         Height          =   375
         Left            =   1200
         TabIndex        =   8
         Top             =   2400
         Width           =   1215
      End
      Begin MSComctlLib.TreeView TreeView1 
         Height          =   1935
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   4935
         _ExtentX        =   8705
         _ExtentY        =   3413
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Sorted          =   -1  'True
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin VB.CommandButton DelCt 
         Caption         =   "Supprimer"
         Height          =   375
         Left            =   2520
         TabIndex        =   6
         Top             =   2400
         Width           =   1215
      End
      Begin VB.CommandButton AddCt 
         Caption         =   "Ajouter"
         Height          =   375
         Left            =   3840
         TabIndex        =   5
         Top             =   2400
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Mots cl�s : "
      Height          =   2895
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   5175
      Begin MSComctlLib.TreeView TreeView2 
         Height          =   1935
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   4935
         _ExtentX        =   8705
         _ExtentY        =   3413
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Sorted          =   -1  'True
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin VB.CommandButton AddKeyWord 
         Caption         =   "Ajouter"
         Height          =   375
         Left            =   3840
         TabIndex        =   3
         Top             =   2400
         Width           =   1215
      End
      Begin VB.CommandButton DelKeyWord 
         Caption         =   "Supprimer"
         Height          =   375
         Left            =   2520
         TabIndex        =   2
         Top             =   2400
         Width           =   1215
      End
      Begin VB.CommandButton PrintKeyWord 
         Caption         =   "Imprimer..."
         Height          =   375
         Left            =   240
         TabIndex        =   1
         Top             =   2400
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmKeyWords"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Keywords As Collection
'-> Cette variable indique que l'on doit demander l'enregistrement quand on quitte
Public ToSave As Boolean
Public Sub LoadCollection()

Dim aKeyWord As Keyword

For Each aKeyWord In Me.Keywords
    Me.TreeView2.Nodes.Add , , aKeyWord.Name, aKeyWord.Name, "KW"
Next

'-> S�lectionner le premier objet
If Me.Keywords.Count <> 0 Then Me.TreeView2.Nodes(1).Selected = True

TreeView2_NodeClick Me.TreeView2.SelectedItem

GestBoutons

End Sub

Private Sub AddCt_Click()

Dim aKeyWord As Keyword


'-> V�rifier si un mot cl� est s�lectionn�
If Me.TreeView2.SelectedItem Is Nothing Then
    MsgBox "Veuillez s�lectionner le mot cl� auquel on doit ajouter un contexte.", vbExclamation + vbOKOnly, "Message"
    Me.TreeView2.SetFocus
    Exit Sub
End If
    
'-> Pointer sur le mot cl� maitre
Set aKeyWord = Me.Keywords(UCase$(Me.TreeView2.SelectedItem.Text))
    
'-> Lancer la feuille de cr�ation du contexte
Set frmInsertChapitre.NodeChapitre = Nothing
Set frmInsertChapitre.Keyword = aKeyWord
Set frmInsertChapitre.Contexte = Nothing
frmInsertChapitre.Show vbModal

'-> Gestion des boutons
GestBoutons

'-> Indiquer que l'on doit sauvegarder
ToSave = True

End Sub

Private Sub AddKeyWord_Click()

'---> Ajout d'un mot cle

Dim Rep As String
Dim aKeyWord As Keyword
Dim aNode As Node

Rep = InputBox("Veuillez saisir le code du nouveau mot cl� ( maximum 50 caract�res ) :", "Insertion d'un nouveau mot cl�", "Nouveau mot cl�")
If Trim(Rep) = "" Then Exit Sub

'-> Il faut v�rifier si le mot cl� n'existe pas d�ja
For i = 1 To Me.Keywords.Count
    If UCase$(Me.Keywords(i).Name) = Trim(UCase$(Rep)) Then
        MsgBox "Impossible de cr�er le mot cl� : celui-ci existe d�ja.", vbCritical + vbOKOnly, "Erreur"
        Exit Sub
    End If
Next

If Len(Rep) > 50 Then
    MsgBox "Erreur : le mot cl� ne doit pas ex�der 50 caract�res.", vbExclamation + vbOKOnly, "Erreur"
    Exit Sub
End If

'-> Ajouter le mot cl� dans la collection
Set aKeyWord = New Keyword
aKeyWord.Name = Trim(Rep)
Me.Keywords.Add aKeyWord, UCase$(Trim(Rep))

'-> Ajouter le nouveau mot cl� dans la collection
Set aNode = Me.TreeView2.Nodes.Add(, , aKeyWord.Name, aKeyWord.Name, "KW")
aNode.Selected = True
Me.TreeView2.SetFocus
TreeView2_NodeClick aNode

'-> Indiquer que l'on doit enregistrer
ToSave = True

'-> Lancer la gestion des boutons
GestBoutons

End Sub

Private Sub GestBoutons()

'---> G�re l'�tat d'affichage des boutons

'-> MotsCl�s
If Me.TreeView2.Nodes.Count = 0 Then
    Me.DelKeyWord.Enabled = False
    Me.PrintKeyWord.Enabled = False
    Me.AddCt.Enabled = False
Else
    Me.DelKeyWord.Enabled = True
    Me.PrintKeyWord.Enabled = True
    Me.AddCt.Enabled = True
End If

'-> Contextes
If Me.TreeView1.Nodes.Count = 0 Then
    Me.DelCt.Enabled = False
    Me.ModifCt.Enabled = False
Else
    Me.DelCt.Enabled = True
    Me.ModifCt.Enabled = True
End If

End Sub

Private Sub DelCt_Click()

'-> Proc�dure qui supprimer un contexte

Dim aKeyWord As Keyword
Dim aLien As Lien
Dim aCt As Contexte

'-> Quitter si rien � supprimer
If Me.TreeView2.Nodes.Count = 0 Then Exit Sub

'-> V�rifier que le mot cl� est s�lectionn�
If Me.TreeView2.SelectedItem Is Nothing Then
    MsgBox "Veuillez s�lectionner dans un permier temps le mot cl�.", vbExclamation + vbOKOnly, "Avertissement"
    Me.TreeView2.SetFocus
    Exit Sub
End If

'-> V�rifier qu'un contexte est s�lectionn�
If TreeView1.SelectedItem Is Nothing Then
    MsgBox "Veuillez s�lectionner dans un permier temps le contexte � supprimer.", vbExclamation + vbOKOnly, "Avertissement"
    Me.TreeView1.SetFocus
    Exit Sub
End If

'-> Demander la confirmation
Rep = MsgBox("Supprimer le contexte : " & Me.TreeView1.SelectedItem.Text, vbQuestion + vbYesNo, "Confirmation")
If Rep = vbNo Then Exit Sub

'-> Pointer sur le mot cle
Set aKeyWord = Keywords(UCase$(Me.TreeView2.SelectedItem.Key))

'-> Il faut v�rifier si le mot cl� sp�cifi� et le contexte ne sont pas utilis�s dans un applicatif
For i = 1 To Applicatifs.Count
    For Each aLien In Applicatifs(i).Liens
        If UCase$(Me.TreeView2.SelectedItem.Key) = UCase$(aLien.KeyWordKey) And UCase$(Me.TreeView1.SelectedItem.Key) = UCase$(aLien.ContexteKey) Then
            '-> Indiquer le mot cl� est toujours utilis�
            MsgBox "Impossible de supprimer le contexte. Celui-ci est encore r�f�renc� dans l'applicatif : " & Applicatifs(i).Name, vbExclamation + vbOKOnly, "Impossible de supprimer le contexte"
            Exit Sub
        End If
    Next 'Pour toutes les associations
Next

'-> V�rification �galement dans la poubelle
For Each aLien In frmPoubelle.Liens
    If UCase$(Me.TreeView1.SelectedItem.Key) = aLien.ContexteKey Then
        '-> Indiquer le mot cl� est toujours utilis�
        MsgBox "Impossible de supprimer le contexte. Celui-ci est encore r�f�renc� dans la poubelle.", vbExclamation + vbOKOnly, "Impossible de supprimer le mot cl�"
        Exit Sub
    End If
Next 'Pour toutes les associations

'-> Supprimer le contexte
aKeyWord.Contextes.Remove Me.TreeView1.SelectedItem.Key

'-> Suppression de la liste
Me.TreeView1.Nodes.Remove Me.TreeView1.SelectedItem.Key

'-> Gestion des boutons
GestBoutons

'-> Indiquer que l'on doit sauvegarder
ToSave = True


End Sub

Private Sub DelKeyWord_Click()

Dim i As Integer
Dim aLien As Lien
Dim aKeyWord As Keyword

'-> Ne rien faire s'il n' y a pas de s�lections
If Me.TreeView2.SelectedItem Is Nothing Then
    MsgBox "Veuillez s�lectionner dans un permier temps le mot cl� � supprimer.", vbExclamation + vbOKOnly, "Avertissement"
    Me.TreeView2.SetFocus
    Exit Sub
End If

'-> Demander la confirmation
Rep = MsgBox("Supprimer le mot cl� : " & Me.TreeView2.SelectedItem.Text, vbQuestion + vbYesNo, "Confirmation")
If Rep = vbNo Then Exit Sub

'-> Pointer sur le mot cle
Set aKeyWord = Keywords(UCase$(Me.TreeView2.SelectedItem.Key))

'-> Il faut v�rifier si le mot cl� sp�cifi� n'est pas utilis� dans un applicatif
For i = 1 To Applicatifs.Count
    '-> Charger les liens dans un premier temps s' l'applicatif n'est pas ouvert
    If Not Applicatifs(i).LinkLoaded Then LoadLienApp Applicatifs(i)
    For Each aLien In Applicatifs(i).Liens
        If UCase$(Me.TreeView2.SelectedItem.Key) = aLien.KeyWordKey Then
            '-> Indiquer le mot cl� est toujours utilis�
            MsgBox "Impossible de supprimer le mot cl�. Celui-ci est encore r�f�renc� dans l'applicatif : " & Applicatifs(i).Name, vbExclamation + vbOKOnly, "Impossible de supprimer le mot cl�"
            Exit Sub
        End If
    Next 'Pour toutes les associations
Next

'-> Il faut aussi v�rifier dans l'applicatif poubelle
For Each aLien In frmPoubelle.Liens
    If UCase$(Me.TreeView2.SelectedItem.Key) = aLien.KeyWordKey Then
        '-> Indiquer le mot cl� est toujours utilis�
        MsgBox "Impossible de supprimer le mot cl�. Celui-ci est encore r�f�renc� dans la poubelle.", vbExclamation + vbOKOnly, "Impossible de supprimer le mot cl�"
        Exit Sub
    End If
Next 'Pour toutes les associations


'-> Le mot cl� n'est plus r�f�renc� on peut supprimer tous les contextes et le mot cl�
aKeyWord.ClearContextes

'-> Suppression du mot cl�
Me.Keywords.Remove UCase$(aKeyWord.Name)

'-> Suppression de l'affichage de la liste
Me.TreeView2.Nodes.Remove aKeyWord.Name

'-> Vider la liste 2
Me.TreeView1.Nodes.Clear

'-> Gestion des boutons
GestBoutons

'-> Indiquer que l'on doit sauvegarder
ToSave = True

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then Unload Me
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

If KeyCode = 112 Then Shell App.Path & "\HELPDEAL.EXE DEAL|1|DEAL|HELPDESKTOP|CT36", vbNormalFocus

End Sub

Private Sub Form_Load()

'-> Intialiser les collections
Set Keywords = New Collection
Set Contextes = New Collection

'-> G�rer l'�tat des boutons
GestBoutons

End Sub

Public Sub ClearKeyWords()

'---> Cette fonction nettoie la biblioth�que de mots cl�s

'-> Vider les collections
Do While Me.Keywords.Count <> 0
    Me.Keywords(1).ClearContextes
    Me.Keywords.Remove (1)
Loop

'-> Vider les listes
Me.TreeView2.Nodes.Clear
Me.TreeView1.Nodes.Clear

'-> G�rer l'�tat des boutons
GestBoutons


End Sub

Private Sub Form_Unload(Cancel As Integer)

If Not IsTerminate Then
    Cancel = 1
    Me.Hide
End If

End Sub



Private Sub ModifCt_Click()

Dim aKeyWord As Keyword

'-> Ne rien faire si pad de s�lection
If Me.TreeView1.SelectedItem Is Nothing Then Exit Sub


'-> Pointer sur le mot cl� maitre
Set aKeyWord = Me.Keywords(UCase$(Me.TreeView2.SelectedItem.Text))

Set frmInsertChapitre.Contexte = aKeyWord.Contextes(Me.TreeView1.SelectedItem.Key)
Set frmInsertChapitre.Keyword = aKeyWord

frmInsertChapitre.Show vbModal

'-> Indiquer que l'on doit enregistrer
ToSave = True

End Sub

Private Sub PrintKeyWord_Click()

frmPrint.TypeImp = 2
frmPrint.Show vbModal

End Sub



Private Sub TreeView1_DblClick()

On Error Resume Next

If Me.TreeView1.Nodes.Count = 0 Then Exit Sub

MsgBox Me.TreeView1.SelectedItem.Key

End Sub

Private Sub TreeView2_NodeClick(ByVal Node As MSComctlLib.Node)

Dim aKeyWord As Keyword
Dim aCt As Contexte


'-> Pointer sur le mot cl�
Set aKeyWord = Me.Keywords(UCase$(Node.Text))

'-> Vider la liste des contextes
Me.TreeView1.Nodes.Clear

'-> Afficher les contextes li�s au mot cl�
For Each aCt In aKeyWord.Contextes
    Me.TreeView1.Nodes.Add , , "CT" & aCt.Index, aCt.Libelle, "CT"
Next

'-> S�lectionner le premier s'il y en a
If Me.TreeView1.Nodes.Count <> 0 Then Me.TreeView1.Nodes(1).Selected = True


'-> Lancer la gestion des boutons
GestBoutons

End Sub
