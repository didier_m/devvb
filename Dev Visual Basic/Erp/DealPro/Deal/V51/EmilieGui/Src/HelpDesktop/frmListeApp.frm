VERSION 5.00
Begin VB.Form frmListeApp 
   Caption         =   "Liste des applicatifs Deal Informatique"
   ClientHeight    =   2415
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3600
   Icon            =   "frmListeApp.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   161
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   240
   Begin VB.ListBox List1 
      Height          =   2400
      Left            =   0
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   0
      Width           =   3615
   End
End
Attribute VB_Name = "frmListeApp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public IndexMenu As Integer

Private Sub Form_Activate()

HelpDeskTop.mnuFenetreListeApp.Checked = True
'-> Desactiver tous les autres noms de feuille
ActiveMenuForm 9999
'-> Activer celui de la liste des clients
HelpDeskTop.mnuFenetreListeApp.Checked = True
HelpDeskTop.mnuFenetreListeCli.Checked = False
HelpDeskTop.mnuFenetrePoubelle.Checked = False



End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

    If KeyCode = 112 Then Shell App.Path & "\HELPDEAL.EXE DEAL|1|DEAL|HELPDESKTOP|CT30", vbNormalFocus

End Sub

Private Sub Form_Load()

'---> Charger la liste des applicatifs DEAL
For i = 1 To Applicatifs.Count
    Me.List1.AddItem Applicatifs(i).Name & " - " & Applicatifs(i).Descriptif
Next

Me.Width = Screen.Width / 4
Me.Height = Screen.Height / 4

End Sub

Private Sub Form_LostFocus()

If Not Me.Visible Then HelpDeskTop.mnuFenetreListeApp.Visible = False


End Sub

Private Sub Form_Resize()

Dim aRect As Rect
Dim Res As Long

Res = GetClientRect(Me.hWnd, aRect)
Me.List1.Left = 0
Me.List1.Top = 0
Me.List1.Width = aRect.Right
Me.List1.Height = aRect.Bottom


End Sub

Private Sub Form_Unload(Cancel As Integer)

CloseMe Cancel, Me
If Cancel = 1 Then
    HelpDeskTop.mnuListeApp.Checked = False
    Form_LostFocus
End If

End Sub

Private Sub List1_DblClick()

'---> Charger le menu de l'applicatif s'il n'est pas d�ja en cours d'utilisation

Dim strApp As String
Dim aApp As Applicatif
Dim aFrm As frmDoc

'-> Pointer sur l'applicatif pour v�rifier s'il l'app n'est pas d�ja charg�
strApp = UCase$(Trim(Entry(1, Me.List1.Text, "-")))
Set aApp = Applicatifs(UCase$(strApp))
If aApp.IsLoaded Then
    '-> L'applicatif est d�ja charg� : le ramener au premier plan
    ShowWindow& aApp.HwndForm, SW_SHOW&
    BringWindowToTop& aApp.HwndForm
    Load HelpDeskTop.mnuListProgi(HelpDeskTop.mnuListProgi.Count)
    aApp.IndexMenu = HelpDeskTop.mnuListProgi.Count - 1
    HelpDeskTop.mnuListProgi(HelpDeskTop.mnuListProgi.Count - 1).Caption = aApp.Name & " - " & aApp.Descriptif
    HelpDeskTop.mnuListProgi(HelpDeskTop.mnuListProgi.Count - 1).Visible = True
Else
    '-> Charger la feuille
    Set aFrm = New frmDoc
    '-> Initialiser la variable de pointage applicatif
    Set aApp.Tree = aFrm.TreeView1
    Set aApp.Web = aFrm.WebBrowser1
    Set aFrm.aApp = aApp
    aFrm.Caption = aApp.Name & " - " & aApp.Descriptif
    aApp.HwndForm = aFrm.hWnd
    aApp.IsLoaded = True
    '-> Rajouter dans la liste des fen�tres ouvertes
    Load HelpDeskTop.mnuListProgi(HelpDeskTop.mnuListProgi.Count)
    aApp.IndexMenu = HelpDeskTop.mnuListProgi.Count - 1
    HelpDeskTop.mnuListProgi(HelpDeskTop.mnuListProgi.Count - 1).Caption = aApp.Name & " - " & aApp.Descriptif
    HelpDeskTop.mnuListProgi(HelpDeskTop.mnuListProgi.Count - 1).Visible = True
End If


End Sub
