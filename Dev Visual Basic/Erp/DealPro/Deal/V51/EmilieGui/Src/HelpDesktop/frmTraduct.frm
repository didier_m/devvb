VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTraduct 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Traduction du menu"
   ClientHeight    =   7155
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   9810
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   477
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   654
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture2 
      Height          =   4695
      Left            =   3240
      ScaleHeight     =   309
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   429
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   480
      Width           =   6495
      Begin VB.TextBox lblLangue 
         Height          =   285
         Index           =   6
         Left            =   840
         MaxLength       =   70
         TabIndex        =   8
         Top             =   3840
         Width           =   5055
      End
      Begin VB.TextBox lblLangue 
         Height          =   285
         Index           =   5
         Left            =   840
         MaxLength       =   70
         TabIndex        =   7
         Top             =   3120
         Width           =   5055
      End
      Begin VB.TextBox lblLangue 
         Height          =   285
         Index           =   4
         Left            =   840
         MaxLength       =   70
         TabIndex        =   6
         Top             =   2400
         Width           =   5055
      End
      Begin VB.TextBox lblLangue 
         Height          =   285
         Index           =   3
         Left            =   840
         MaxLength       =   70
         TabIndex        =   5
         Top             =   1680
         Width           =   5055
      End
      Begin VB.TextBox lblLangue 
         Height          =   285
         Index           =   2
         Left            =   840
         MaxLength       =   70
         TabIndex        =   4
         Top             =   960
         Width           =   5055
      End
      Begin VB.TextBox lblLangue 
         Height          =   285
         Index           =   1
         Left            =   840
         MaxLength       =   70
         TabIndex        =   3
         Top             =   240
         Width           =   5055
      End
      Begin VB.Image Image1 
         Height          =   615
         Index           =   6
         Left            =   120
         Top             =   3720
         Width           =   615
      End
      Begin VB.Image Image1 
         Height          =   615
         Index           =   5
         Left            =   120
         Top             =   3000
         Width           =   615
      End
      Begin VB.Image Image1 
         Height          =   615
         Index           =   4
         Left            =   120
         Top             =   2280
         Width           =   615
      End
      Begin VB.Image Image1 
         Height          =   615
         Index           =   3
         Left            =   120
         Top             =   1560
         Width           =   615
      End
      Begin VB.Image Image1 
         Height          =   615
         Index           =   2
         Left            =   120
         Top             =   840
         Width           =   615
      End
      Begin VB.Image Image1 
         Height          =   480
         Index           =   1
         Left            =   120
         Top             =   120
         Width           =   480
      End
   End
   Begin VB.PictureBox Spliter 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   975
      Left            =   2760
      MousePointer    =   9  'Size W E
      ScaleHeight     =   975
      ScaleWidth      =   60
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Width           =   60
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   4695
      Left            =   0
      TabIndex        =   2
      Top             =   480
      Width           =   3135
      _ExtentX        =   5530
      _ExtentY        =   8281
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
      OLEDragMode     =   1
      OLEDropMode     =   1
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2400
      Top             =   4080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTraduct.frx":0000
            Key             =   "ChapitreFerme"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTraduct.frx":0CDA
            Key             =   "ChapitreOuvert"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTraduct.frx":15B4
            Key             =   "Document"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTraduct.frx":1E8E
            Key             =   "FichierHTM"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTraduct.frx":2B68
            Key             =   "KeyWord"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTraduct.frx":3842
            Key             =   "KeyWord#"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmTraduct"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public aApp As Applicatif
Public MenuCours As Menu
Public IsLoading As Boolean

Private Sub Annuler_Click()
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

If KeyCode = 112 Then Shell App.Path & "\HELPDEAL.EXE DEAL|1|DEAL|HELPDESKTOP|CT37", vbNormalFocus

End Sub

Private Sub Form_Load()

For i = 1 To 6
    Me.Image1(i).Picture = frmLangue.ImageList1.ListImages(i).Picture
Next

End Sub

Private Sub Form_Resize()

On Error Resume Next

Dim Res As Long
Dim aRect As Rect

'-> R�cup�rer la taille de la zone cliente
Res = GetClientRect(Me.hWnd, aRect)

'-> V�rifier que le split est bien positionn�
If Me.Spliter.Left > Me.ScaleX(Me.Width, 1, 3) Then Spliter.Left = Me.ScaleX(Me.Width / 2, 1, 3)

'-> positionner le spliter
Me.Spliter.Top = 0
Me.Spliter.Height = aRect.Bottom

'-> Positionner le treeview1
Me.TreeView1.Left = 0
Me.TreeView1.Top = 0
Me.TreeView1.Height = aRect.Bottom
Me.TreeView1.Width = Me.Spliter.Left

'-> Positionner le PictureBox
Me.Picture2.Left = Me.Spliter.Left + Me.Spliter.Width
Me.Picture2.Top = Me.TreeView1.Top
Me.Picture2.Height = Me.TreeView1.Height
Me.Picture2.Width = aRect.Right - Me.Picture2.Left

End Sub

Private Sub Form_Unload(Cancel As Integer)

DisplayAppByLangue

End Sub

Private Sub lblLangue_Change(Index As Integer)

If IsLoading Then Exit Sub
MenuCours.SetLbByLangue Index, Me.lblLangue(Index).Text

aApp.ToSave = True

End Sub

Private Sub lblLangue_GotFocus(Index As Integer)

Me.lblLangue(Index).SelStart = 0
Me.lblLangue(Index).SelLength = Len(Me.lblLangue(Index).Text)

End Sub

Private Sub Spliter_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim aPt As POINTAPI
Dim Res As Long

Me.Spliter.MousePointer = 9

If Button = vbLeftButton Then
    '-> R�cup�rer dans un premier temps la position de la souris
    Res = GetCursorPos(aPt)
    '-> La convertir au coordonn�es clientes
    Res = ScreenToClient(Me.hWnd, aPt)
    '-> Poistionner le spilt en fonction
    If aPt.x > Me.ScaleX(2, 7, 3) And aPt.x < Me.ScaleX(Me.Width, 1, 3) - Me.ScaleX(2, 7, 3) Then Me.Spliter.Left = aPt.x
    Me.Spliter.BackColor = &H404040
    Me.Spliter.ZOrder
End If


End Sub

Private Sub Spliter_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)

'-> Repositionner les diff�rents objets

If Button = vbLeftButton Then Form_Resize
Me.Spliter.BackColor = &H8000000F

End Sub

Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)

'---> Afficher le menu pour les libelles
Dim aMenu As Menu
Dim i As Integer

'-> Tester la nature du node
If Node.Tag <> "CHAPITRE" And Node.Tag <> "DOCUMENT" Then
    IsLoading = True
    For i = 1 To 6
        Me.lblLangue(i).Enabled = False
        Me.lblLangue(i).Text = ""
    Next
    IsLoading = False
    Exit Sub
End If

Set aMenu = aApp.Menus(Node.Key)
Set MenuCours = aMenu
For i = 1 To 6
    Me.lblLangue(i).Enabled = True
    Me.lblLangue(i).Text = aMenu.GetLbByLangue(i)
Next

End Sub
