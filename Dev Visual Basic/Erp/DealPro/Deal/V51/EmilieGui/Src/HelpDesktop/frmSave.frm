VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSave 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Enregistrer"
   ClientHeight    =   3285
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4710
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   219
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   314
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   4080
      Picture         =   "frmSave.frx":0000
      ScaleHeight     =   480
      ScaleWidth      =   630
      TabIndex        =   1
      ToolTipText     =   "Enregistrer"
      Top             =   2760
      Width           =   630
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   2655
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   4683
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   1
      Checkboxes      =   -1  'True
      Appearance      =   1
   End
End
Attribute VB_Name = "frmSave"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

If KeyCode = 112 Then Shell App.Path & "\HELPDEAL.EXE DEAL|1|DEAL|HELPDESKTOP|CT30", vbNormalFocus

End Sub

Private Sub Form_Resize()

Dim aRect As Rect
Dim Res As Long

Res = GetClientRect(Me.hWnd, aRect)
Me.TreeView1.Left = 0
Me.TreeView1.Top = 0
Me.TreeView1.Width = aRect.Right
Me.TreeView1.Height = aRect.Bottom

End Sub

Private Sub Picture1_Click()

Dim aNode As Node
Dim Rep
Dim aApp As Applicatif

'-> Lancer l'enregistrement des propri�t�s globales
If Not SaveGlobal Then Exit Sub

'-> Lancer les enregistrements
For Each aNode In Me.TreeView1.Nodes
    If aNode.Checked Then
        If aNode.Key <> "KEYWORD" Then
            '-> Lancer la proc�dure d'enregiqtrement de l'applicqtif
            Set aApp = Applicatifs(UCase$(aNode.Key))
            If Not SaveApp(aApp) Then Exit Sub
        Else
            If Not SaveKey Then Exit Sub
        End If
    Else
        '-> Le node n'est pas coch� : v�rifier pour les mots cl�s
        If aNode.Key = "KEYWORD" And frmKeyWords.ToSave Then
            Rep = MsgBox("Attention : il est indispensable d'enregistrer les modifications apport�es aux mots cl�. Ne pas le faire peut rendre le menu inop�rant. Enregistrer ?", vbExclamation + vbYesNo, "Confirmation")
            If Rep = vbYes Then
                '-> Enregistrer les mots cl�s
                If Not SaveKey Then Exit Sub
            End If
        End If
    End If
          
Next

'-> D�charger la feuille
Unload Me

End Sub

