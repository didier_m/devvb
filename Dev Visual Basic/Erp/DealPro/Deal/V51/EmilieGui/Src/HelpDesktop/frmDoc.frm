VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "SHDOCVW.DLL"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDoc 
   ClientHeight    =   6105
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8910
   Icon            =   "frmDoc.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   407
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   594
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   6240
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDoc.frx":0CCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDoc.frx":19A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDoc.frx":267E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox Spliter 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   975
      Left            =   6360
      MousePointer    =   9  'Size W E
      ScaleHeight     =   975
      ScaleWidth      =   60
      TabIndex        =   3
      Top             =   960
      Width           =   60
   End
   Begin VB.PictureBox Picture2 
      Height          =   1095
      Left            =   5640
      ScaleHeight     =   69
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   173
      TabIndex        =   0
      Top             =   4200
      Width           =   2655
      Begin SHDocVwCtl.WebBrowser WebBrowser1 
         CausesValidation=   0   'False
         Height          =   1815
         Left            =   0
         TabIndex        =   1
         Top             =   0
         Width           =   1215
         ExtentX         =   2143
         ExtentY         =   3201
         ViewMode        =   0
         Offline         =   0
         Silent          =   0
         RegisterAsBrowser=   0
         RegisterAsDropTarget=   1
         AutoArrange     =   0   'False
         NoClientEdge    =   0   'False
         AlignLeft       =   0   'False
         ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
         Location        =   ""
      End
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   3495
      Left            =   720
      TabIndex        =   2
      Top             =   1440
      Width           =   3135
      _ExtentX        =   5530
      _ExtentY        =   6165
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
      OLEDropMode     =   1
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7080
      Top             =   2640
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDoc.frx":2F58
            Key             =   "ChapitreFerme"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDoc.frx":3C32
            Key             =   "ChapitreOuvert"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDoc.frx":450C
            Key             =   "Document"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDoc.frx":4DE6
            Key             =   "FichierHTM"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDoc.frx":5AC0
            Key             =   "KeyWord"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDoc.frx":679A
            Key             =   "KeyWord#"
         EndProperty
      EndProperty
   End
   Begin VB.Image Image1 
      Height          =   720
      Left            =   0
      Picture         =   "frmDoc.frx":7474
      Top             =   0
      Width           =   720
   End
End
Attribute VB_Name = "frmDoc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-> Variables de gestion des objets
Public aApp As Applicatif
'-> Indique s'il s'agit d'une documentation DEAL ou s'il s'agit ou Cliente
Public IsDocCliente
'-> Indique si l'autorisation de d�chargement de la feuille
Public IsUnload As Boolean


Private Sub Form_Activate()

ActiveMenuForm Me.aApp.IndexMenu
HelpDeskTop.mnuFenetreListeApp.Checked = False
HelpDeskTop.mnuFenetreListeCli.Checked = False
HelpDeskTop.mnuFenetrePoubelle.Checked = False
CurApp = aApp.Name

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

'-> Lancer la recherche
If Me.TreeView1.Nodes.Count = 0 Then Exit Sub
If KeyCode = 114 Then _
    frmSearch.Show vbModal


End Sub

Private Sub Form_Load()

'-> Afficher une page blanche dans le navigateur
Me.WebBrowser1.Navigate DealPath & "Blanck.htm"

'-> Charger le fichier
LoadMenuApp aApp

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

'On Error Resume Next

'---> Ne fermer la documentation que si on en a l'autorisation
If Not IsTerminate Then
    Cancel = 1
    Me.Hide
    '-> Supprimer du menu d'affichage
    Unload HelpDeskTop.mnuListProgi(aApp.IndexMenu)
    
End If

End Sub

Private Sub Form_Resize()

On Error Resume Next

Dim Res As Long
Dim aRect As Rect

'-> R�cup�rer la taille de la zone cliente
Res = GetClientRect(Me.hWnd, aRect)

'-> V�rifier que le split est bien positionn�
If Me.Spliter.Left > Me.ScaleX(Me.Width, 1, 3) Then Spliter.Left = Me.ScaleX(Me.Width / 2, 1, 3)

'-> positionner le spliter
Me.Spliter.Top = 0
Me.Spliter.Height = aRect.Bottom

'-> Positionner le treeview1
Me.TreeView1.Left = 0
Me.TreeView1.Top = 0
Me.TreeView1.Height = aRect.Bottom
Me.TreeView1.Width = Me.Spliter.Left

'-> Positionner le PictureBox
Me.Picture2.Left = Me.Spliter.Left + Me.Spliter.Width
Me.Picture2.Top = Me.TreeView1.Top
Me.Picture2.Height = Me.TreeView1.Height
Me.Picture2.Width = aRect.Right - Me.Picture2.Left

End Sub

Private Sub Form_Unload(Cancel As Integer)

'---> Supprimer du menu d'affichage
If Me.Visible = True Then _
Unload HelpDeskTop.mnuListProgi(aApp.IndexMenu)

'-> Vider les menus de l'applicatif
Do While aApp.Menus.Count <> 0
    aApp.Menus.Remove (1)
Loop
'-> Vider la liste des liens
Do While aApp.Liens.Count <> 0
    aApp.Liens.Remove (1)
Loop

'-> Supprimer les pointeurs associ�s aux objets de l'applicatif
Set aApp.Tree = Nothing
Set aApp.Web = Nothing

'-> Indiquer que l'applicatif n'est plus charg�
aApp.IsLoaded = False

'-> Indiqu� que l'on ne doit plus enregistrer
aApp.ToSave = False

End Sub

Private Sub Picture2_Resize()

'-> Positionner le browser
Me.WebBrowser1.Top = 0
Me.WebBrowser1.Left = 0
Me.WebBrowser1.Width = Me.Picture2.Width - LargeurBordure
Me.WebBrowser1.Height = Me.Picture2.Height - HauteurBordure

End Sub

Private Sub Spliter_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim aPt As POINTAPI
Dim Res As Long

Me.Spliter.MousePointer = 9

If Button = vbLeftButton Then
    '-> R�cup�rer dans un premier temps la position de la souris
    Res = GetCursorPos(aPt)
    '-> La convertir au coordonn�es clientes
    Res = ScreenToClient(Me.hWnd, aPt)
    '-> Poistionner le spilt en fonction
    If aPt.x > Me.ScaleX(2, 7, 3) And aPt.x < Me.ScaleX(Me.Width, 1, 3) - Me.ScaleX(2, 7, 3) Then Me.Spliter.Left = aPt.x
    Me.Spliter.BackColor = &H404040
    Me.Spliter.ZOrder
End If

End Sub

Private Sub Spliter_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)

'-> Repositionner les diff�rents objets

If Button = vbLeftButton Then Form_Resize
Me.Spliter.BackColor = &H8000000F

End Sub

Private Sub TreeView1_DragDrop(Source As Control, x As Single, y As Single)

Dim Res As Long
Dim Res1 As Long

'Lorsqu'on lache un gliser d�pass�
'-> On v�rifie que la source est diff�rente de la destination
If SourceNode Is Me.TreeView1.DropHighlight Then Exit Sub

'-> V�rifier que le D&D a bien �t� initialis�
If SourceNode Is Nothing Then Exit Sub

'-> On v�rifie qu'on lache bien sur node
If Me.TreeView1.HitTest(x, y) Is Nothing And Me.TreeView1.Nodes.Count <> 0 Then Exit Sub

Res1 = GetKeyState%(VK_CONTROL)
If Not Res1 And &H80 Then Exit Sub

Res = GetKeyState%(VK_SHIFT)

RetourChx = -1
frmChxDest.Show vbModal

If RetourChx = -1 Then Exit Sub

If Res And &H80 Then
    AnalyseNode AppDrag, SourceNode, 2, Me.aApp.Name, Me.TreeView1.DropHighlight, RetourChx
Else
    AnalyseNode AppDrag, SourceNode, 1, Me.aApp.Name, Me.TreeView1.DropHighlight, RetourChx
End If

End Sub

Private Sub TreeView1_DragOver(Source As Control, x As Single, y As Single, State As Integer)

Dim DestNode As Node
Dim HighLight As Boolean

Me.ZOrder

HighLight = False
'-> R�cup�ration du node sur lequel on passe
Set DestNode = Me.TreeView1.HitTest(x, y)
'-> V�rifier que l'on passe sur un node
If Not (DestNode Is Nothing) Then
    '-> V�rifier que l'on se copie pas sur soi m�me
    If Not DestNode Is SourceNode Then
        '-> R�cup�ration de la nature du node sur lequel on passe
        If DestNode.Tag = "CHAPITRE" Then HighLight = True
    End If
End If
If HighLight Then
    Set Me.TreeView1.DropHighlight = DestNode
Else
    Set Me.TreeView1.DropHighlight = Nothing
End If
                    

End Sub

Private Sub TreeView1_KeyDown(KeyCode As Integer, Shift As Integer)

Dim Res As Integer

If Me.TreeView1.Nodes.Count = 0 Then Exit Sub


If KeyCode = 46 Then
    '-> V�rifier que l'on ne pointe pas sur un fichier HTML
    If Me.TreeView1.SelectedItem.Tag = "HTML" Or Me.TreeView1.SelectedItem.Tag = "KEYWORD" Then Exit Sub
    
    Set SourceNode = Me.TreeView1.SelectedItem
    
    Res = GetKeyState%(VK_SHIFT)

    '-> V�rifier si le  bit 7 ( 8eme Bit ) est activ�
    If Res And &H80 Then
        frmDEl.intSup = 1
    Else
        frmDEl.intSup = 0
    End If
    frmDEl.lbApp = CurApp
    frmDEl.Show vbModal
End If


End Sub

Private Sub TreeView1_KeyPress(KeyAscii As Integer)

'-> gestion du copier coller

'-> Quitter si pas de nodes
If Me.TreeView1.Nodes.Count = 0 Then Exit Sub

'-> Ne rien faire si node de fichier HTML
If Me.TreeView1.SelectedItem.Tag = "HTML" Or Me.TreeView1.SelectedItem.Tag = "KEYWORD" Then Exit Sub
    
Select Case KeyAscii

    Case 3 'Copier
    
        '-> Lancer la copie
        HelpDeskTop.mnuCopy_Click
        
    Case 22 'Coller
    
        '-> Traiter le cas du menu Coller
        If CopyNode Is Nothing Then Exit Sub
            
        '-> V�rifier si on pointe sur chapitre
        If Me.TreeView1.SelectedItem.Tag = "CHAPITRE" Then HelpDeskTop.mnuPaste_Click
            
End Select



End Sub

Private Sub TreeView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim KeyNode As String
Dim aNode As Node
Dim aMenu As Menu

'-> Supprimer une source pr�c�dente de D&D
Set Me.TreeView1.DropHighlight = Nothing

'Select Case Shift
'
'    Case 2 'Ctrl
'        '-> Sauvegarder le node source
'        Set SourceNode = Me.TreeView1.HitTest(x, y)
'
'    Case 3 'Ctrl + Shift
'        '-> Sauvegarder le node source
'        Set SourceNode = Me.TreeView1.HitTest(x, y)
'
'
'    Case Else
'        Set SourceNode = Nothing
'
'End Select

If Button = vbRightButton Then

    '-> Gestion du copier/couper/supprimer
    If Me.TreeView1.Nodes.Count <> 0 Then
        
        '-> Autoriser l'impression
        HelpDeskTop.mnuPrintDoc.Enabled = True
        HelpDeskTop.mnuPrintRub.Enabled = True
        
        '-> 'Dans tous les cas on peut modifier le libelle
        HelpDeskTop.mnuModifier.Enabled = True
        
        '-> Menu de recherche OK
        HelpDeskTop.mnuFind.Enabled = True
        
        '-> intialiser variable
        strTempoTxt = ""
              
        '-> Si fran�ais : OK
        If CurCodeLangue = 1 Then
            HelpDeskTop.mnuKeyWords.Enabled = True
            If Me.TreeView1.SelectedItem.Tag = "HTML" Or Me.TreeView1.SelectedItem.Tag = "KEYWORD" Then
                HelpDeskTop.mnuModifier.Enabled = False
                HelpDeskTop.mnuAddDoc.Enabled = False
                HelpDeskTop.mnuAddChapitre.Enabled = False
            Else
                HelpDeskTop.mnuModifier.Enabled = True
                HelpDeskTop.mnuAddDoc.Enabled = True
                strTempoTxt = Me.TreeView1.SelectedItem.Text
                HelpDeskTop.mnuAddChapitre.Enabled = True
            End If
        Else
            HelpDeskTop.mnuAddDoc.Enabled = False
            HelpDeskTop.mnuAddChapitre.Enabled = False
            HelpDeskTop.mnuKeyWords.Enabled = False
            If Me.TreeView1.SelectedItem.Tag = "HTML" Or Me.TreeView1.SelectedItem.Tag = "KEYWORD" Then
                HelpDeskTop.mnuModifier.Enabled = False
            Else
                HelpDeskTop.mnuModifier.Enabled = True
            End If
        End If
        
        '-> Menu Copier et Supprimer
        If Me.TreeView1.SelectedItem.Tag = "HTML" Or Me.TreeView1.SelectedItem.Tag = "KEYWORD" Then
            '-> On ne peut ni copier ni supprimer un node fichier HTML
            HelpDeskTop.mnuCopy.Enabled = False
            HelpDeskTop.mnuDel.Enabled = False
        Else
            HelpDeskTop.mnuCopy.Enabled = True
            HelpDeskTop.mnuDel.Enabled = True
        End If
        
    Else '-> Cas ou il n'y a pas de nodes
        HelpDeskTop.mnuAddChapitre.Enabled = True
        HelpDeskTop.mnuAddDoc.Enabled = True
        HelpDeskTop.mnuCopy.Enabled = False
        HelpDeskTop.mnuDel.Enabled = False
        HelpDeskTop.mnuModifier.Enabled = False
        
        '-> pas  d'impression
        HelpDeskTop.mnuPrintDoc.Enabled = False
        HelpDeskTop.mnuPrintRub.Enabled = False
        
        '-> Menu de recherche OK
        HelpDeskTop.mnuFind.Enabled = False
    End If '-> S'il y a des nodes
    
    '-> Traiter le cas du menu Coller
    If CopyNode Is Nothing Then
        HelpDeskTop.mnuPaste.Enabled = False
    Else
        '-> V�rifier s'il y a des nodes dans le treeview
        If Me.TreeView1.Nodes.Count <> 0 Then
            '-> V�rifier si on pointe sur chapitre
            If Me.TreeView1.SelectedItem.Tag = "CHAPITRE" Then
                HelpDeskTop.mnuPaste.Enabled = True
            Else
                HelpDeskTop.mnuPaste.Enabled = False
            End If
        Else
            HelpDeskTop.mnuPaste.Enabled = True
        End If
    End If
    
EndError:
    
    '-> Afficher le menu
    Me.SetFocus
    Me.PopupMenu HelpDeskTop.mnuEdition


Else 'Si bouton gauche
    If Not Me.TreeView1.SelectedItem Is Nothing Then
        '-> Pointer dessus si c'est diff�rent de KEYWORD ou de fichier
        If Me.TreeView1.SelectedItem.Tag <> "HTML" And Me.TreeView1.SelectedItem.Tag <> "KEYWORD" Then
            Set SourceNode = Me.TreeView1.SelectedItem
            AppDrag = Me.aApp.Name
        Else
            Set SourceNode = Nothing
        End If
    Else
        Set SourceNode = Nothing
    End If
End If

End Sub

Private Sub TreeView1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

'---> Gestion du gliss� d�plac�

Dim aPt As POINTAPI
Dim aAppt As Applicatif
Dim Res As Long

If Me.TreeView1.Nodes.Count = 0 Then Exit Sub

If Button = vbLeftButton Then

    '-> Tester que la source est valide avant de lancer le drag & drop
    If SourceNode Is Nothing Then Exit Sub

    '-> Ne pas se d�placer si on bouge un fichier HTML
    If SourceNode.Tag = "HTML" Or SourceNode.Tag = "KEYWORD" Then Exit Sub
    
    '-> S�lectionner le node source
    Set Me.TreeView1.SelectedItem = SourceNode

    If SourceNode.Tag = "CHAPITRE" Then
        Me.TreeView1.DragIcon = Me.ImageList2.ListImages(1).ExtractIcon
    Else
        Me.TreeView1.DragIcon = Me.ImageList2.ListImages(3).ExtractIcon
    End If
    
    '-> Commencer le Drag & Drop si touche Ctrl activ�e
    Res = GetKeyState%(VK_CONTROL)
    If Res And &H80 Then Me.TreeView1.Drag vbBeginDrag
    
End If

End Sub

Private Sub TreeView1_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Me.TreeView1.Drag vbEndDrag
End Sub



Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)

Set SourceNode = Node
Dim aClient As Client

Select Case Node.Tag
    Case "CHAPITRE"
        Set aClient = Clients(UCase$(CurClient))
        DisplayHtmlFile "Blanck.htm", aApp.Name
    Case "DOCUMENT"
        DisplayHtmlFile Node.Child.Text, CurApp
    Case "HTML"
        DisplayHtmlFile Node.Text, CurApp
End Select


End Sub
