VERSION 5.00
Begin VB.Form frmDel 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Suppression"
   ClientHeight    =   1065
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5340
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1065
   ScaleWidth      =   5340
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command1 
      Caption         =   "&Ok"
      Height          =   375
      Left            =   3960
      TabIndex        =   0
      Top             =   600
      Width           =   1335
   End
   Begin VB.Label lbSup 
      BackStyle       =   0  'Transparent
      Caption         =   "Supprimer de mani�re d�finitive ?"
      Height          =   375
      Left            =   840
      TabIndex        =   2
      Top             =   240
      Width           =   4455
   End
   Begin VB.Label lbPoubelle 
      BackStyle       =   0  'Transparent
      Caption         =   "Envoyer vers la poubelle ?"
      Height          =   375
      Left            =   840
      TabIndex        =   1
      Top             =   240
      Width           =   4455
   End
   Begin VB.Image imgPoubelle 
      Height          =   480
      Left            =   120
      Picture         =   "frmDel.frx":0000
      Top             =   120
      Width           =   480
   End
   Begin VB.Image imgSup 
      Height          =   480
      Left            =   120
      Picture         =   "frmDel.frx":0442
      Top             =   120
      Width           =   480
   End
End
Attribute VB_Name = "frmDEl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public intSup As Integer '0 -> Poubelle
Public lbApp As String

Private Sub Command1_Click()

Unload Me

If intSup = 0 Then
    DestDel = "POUBELLE"
    If Not IsPoubelle Then frmPoubelle.Show
    AnalyseNode lbApp, SourceNode, 2, "POUBELLE", Nothing, 4
Else
    AnalyseNode lbApp, SourceNode, 3, "", Nothing, 4
End If

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

If KeyAscii = 27 Then Unload Me

End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

If KeyCode = 112 Then Shell App.Path & "\HELPDEAL.EXE DEAL|1|DEAL|HELPDESKTOP|CT35", vbNormalFocus

End Sub

Private Sub Form_Load()

If intSup = 0 Then
    Me.lbPoubelle.Visible = True
    Me.imgPoubelle.Visible = True
    Me.lbSup.Visible = False
    Me.imgSup.Visible = False
Else
    Me.lbPoubelle.Visible = False
    Me.imgPoubelle.Visible = False
    Me.lbSup.Visible = True
    Me.imgSup.Visible = True
End If

End Sub
