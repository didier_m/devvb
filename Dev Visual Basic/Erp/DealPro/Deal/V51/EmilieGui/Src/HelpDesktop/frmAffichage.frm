VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmAffichage 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Affichage en cours"
   ClientHeight    =   4425
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6750
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4425
   ScaleWidth      =   6750
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WhatsThisHelp   =   -1  'True
   Begin VB.Frame Frame3 
      Height          =   975
      Left            =   1560
      TabIndex        =   19
      Top             =   3360
      Width           =   1935
      Begin VB.CommandButton Command3 
         Caption         =   "Enregistrer"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   600
         Width           =   1695
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Defaut"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.Frame Frame2 
      Height          =   2895
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   3495
      Begin VB.TextBox Client 
         Enabled         =   0   'False
         Height          =   285
         Left            =   120
         TabIndex        =   14
         Top             =   600
         Width           =   3135
      End
      Begin VB.TextBox Progiciel 
         Enabled         =   0   'False
         Height          =   285
         Left            =   120
         TabIndex        =   13
         Top             =   1440
         Width           =   3135
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Client : "
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Progiciel : "
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Code langue : "
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   1920
         Width           =   1935
      End
      Begin VB.Image Image1 
         Height          =   615
         Left            =   2640
         Top             =   2160
         Width           =   615
      End
      Begin VB.Label Label6 
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   2400
         Width           =   1935
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Actualiser l'affichage"
      Height          =   1335
      Left            =   120
      Picture         =   "frmAffichage.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   3000
      Width           =   1215
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   2760
      Top             =   1680
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame1 
      Caption         =   "Traduction : "
      Height          =   4335
      Left            =   3600
      TabIndex        =   0
      Top             =   0
      Width           =   3135
      Begin VB.OptionButton Option4 
         Caption         =   "Afficher le menu de la langue d'origine "
         Height          =   495
         Left            =   360
         TabIndex        =   10
         Top             =   3120
         Width           =   2655
      End
      Begin MSComctlLib.ImageCombo ImageCombo1 
         Height          =   330
         Left            =   120
         TabIndex        =   9
         Top             =   480
         Width           =   2895
         _ExtentX        =   5106
         _ExtentY        =   582
         _Version        =   393216
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Locked          =   -1  'True
      End
      Begin VB.PictureBox Picture1 
         Height          =   375
         Left            =   2040
         ScaleHeight     =   315
         ScaleWidth      =   555
         TabIndex        =   6
         Top             =   3780
         Width           =   615
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   600
         MaxLength       =   50
         TabIndex        =   5
         Top             =   2640
         Width           =   2055
      End
      Begin VB.OptionButton Option3 
         Caption         =   "Afficher la chaine suivante :"
         Height          =   255
         Left            =   360
         TabIndex        =   4
         Top             =   2280
         Width           =   2655
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Afficher : <Pas de traduction>"
         Height          =   255
         Left            =   360
         TabIndex        =   3
         Top             =   1920
         Width           =   2655
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Ne rien afficher"
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   1560
         Value           =   -1  'True
         Width           =   1815
      End
      Begin VB.Label Label7 
         Caption         =   "Choix de la langue :"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   2895
      End
      Begin VB.Label Label5 
         Caption         =   "Couleur des niveaux :"
         Height          =   255
         Left            =   360
         TabIndex        =   7
         Top             =   3840
         Width           =   1695
      End
      Begin VB.Label Label4 
         Caption         =   "Si pas de traduction : "
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   1200
         Width           =   2175
      End
   End
End
Attribute VB_Name = "frmAffichage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
DisplayAppByLangue
End Sub

Private Sub Command2_Click()

'-> Donner les valeurs par d�faut
InitDefautTraduction

'-> Vider l'image liste
Me.ImageCombo1.ComboItems.Clear

'-> R�afficher les prorpi�t�s
Form_Load

End Sub

Private Sub Command3_Click()

'---> Cette fonction enregistre les constantes de traduction
Dim NumFic As Integer
Dim aSaveTraduct As SaveTraduct
Dim i As Integer

'-> Ouvrir le fichier
NumFic = FreeFile
Open DocPath & "Traduct.dat" For Random As #NumFic Len = Len(aSaveTraduct)
For i = 1 To 6
    aSaveTraduct.idTraduction = idTraduction(i)
    If Trim(strTraduction(i)) = "" Then
        strTraduction(i) = Space$(50)
    Else
        aSaveTraduct.strTraduction = strTraduction(i)
    End If
    aSaveTraduct.lngTraduction = lngTraduction(i)
    Put #NumFic, i, aSaveTraduct
Next

'-> Fermer le fichier
Close #NumFic

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

If KeyAscii = 27 Then Unload Me

End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

If KeyCode = 112 Then _
    Shell App.Path & "\HELPDEAL.EXE DEAL|1|DEAL|HELPDESKTOP|CT37", vbNormalFocus
    



End Sub

Private Sub Form_Load()

'-> Charger la COmbo1
Me.ImageCombo1.ImageList = frmLangue.ImageList1
For i = 1 To 6
    Me.ImageCombo1.ComboItems.Add i, , frmLangue.ImageList1.ListImages(i).Tag, i
Next
Me.ImageCombo1.ComboItems(CurCodeLangue).Selected = True
ImageCombo1_Click

End Sub



Private Sub ImageCombo1_Click()

'---> Charger les propri�t�s de traduction via le code langue
Select Case idTraduction(Me.ImageCombo1.SelectedItem.Index)
    Case 1
        Me.Option1.Value = True
        Me.Text1.Text = ""
    Case 2
        Me.Option2.Value = True
        Me.Text1.Text = ""
    Case 3
        Me.Option3.Value = True
        Me.Text1.Text = strTraduction(Me.ImageCombo1.SelectedItem.Index)
    Case 4
        Me.Option4.Value = True
        Me.Text1.Text = ""
    Case Else
        Exit Sub
End Select
Me.Picture1.BackColor = lngTraduction(Me.ImageCombo1.SelectedItem.Index)

End Sub

Private Sub ImageCombo1_KeyPress(KeyAscii As Integer)

On Error Resume Next

If KeyAscii = 13 Then
    Me.Option1.SetFocus
    If Me.ImageCombo1.SelectedItem Is Nothing Then _
    Me.ImageCombo1.ComboItems(idLangue).Selected = True
End If

End Sub

Private Sub Option1_Click()

On Error Resume Next

Me.Text1.Enabled = False
idTraduction(Me.ImageCombo1.SelectedItem.Index) = 1
strTraduction(Me.ImageCombo1.SelectedItem.Index) = ""

End Sub

Private Sub Option2_Click()

On Error Resume Next

Me.Text1.Enabled = False
idTraduction(Me.ImageCombo1.SelectedItem.Index) = 2
strTraduction(Me.ImageCombo1.SelectedItem.Index) = "<Pas de traduction>"

End Sub

Private Sub Option3_Click()

On Error Resume Next

Me.Text1.Enabled = True
idTraduction(Me.ImageCombo1.SelectedItem.Index) = 3

End Sub

Private Sub Option4_Click()

On Error Resume Next

Me.Text1.Enabled = False
idTraduction(Me.ImageCombo1.SelectedItem.Index) = 4
strTraduction(Me.ImageCombo1.SelectedItem.Index) = ""


End Sub

Private Sub Picture1_DblClick()

On Error GoTo GestError

Me.CommonDialog1.CancelError = True

'---> Afficher la boite de couleur commune
Me.CommonDialog1.ShowColor
Me.Picture1.BackColor = Me.CommonDialog1.Color
lngTraduction(Me.ImageCombo1.SelectedItem.Index) = Me.Picture1.BackColor


Exit Sub

GestError:
    Me.Picture1.BackColor = lngTraduction(Me.ImageCombo1.SelectedItem.Index)

End Sub

Private Sub Text1_Change()

On Error Resume Next

strTraduction(Me.ImageCombo1.SelectedItem.Index) = Me.Text1.Text

End Sub
