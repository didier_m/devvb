VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmInsertDoc 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Insertion d'un document d'aide"
   ClientHeight    =   8010
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5745
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8010
   ScaleWidth      =   5745
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   4440
      Top             =   7440
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame2 
      Height          =   5415
      Left            =   120
      TabIndex        =   6
      Top             =   0
      Width           =   5535
      Begin VB.Frame Frame3 
         Height          =   2295
         Left            =   120
         TabIndex        =   9
         Top             =   3000
         Width           =   5295
         Begin VB.CommandButton Command2 
            Caption         =   "Catalogue des mots cl�s"
            Height          =   255
            Left            =   2640
            TabIndex        =   16
            Top             =   1920
            Width           =   2415
         End
         Begin MSComctlLib.TreeView TreeView1 
            Height          =   1215
            Left            =   2640
            TabIndex        =   14
            Top             =   600
            Width           =   2415
            _ExtentX        =   4260
            _ExtentY        =   2143
            _Version        =   393217
            HideSelection   =   0   'False
            LabelEdit       =   1
            Appearance      =   1
         End
         Begin VB.CommandButton Command1 
            Caption         =   "Ne rien s�lectionner"
            Height          =   255
            Left            =   120
            TabIndex        =   13
            Top             =   1920
            Width           =   2415
         End
         Begin VB.ListBox List1 
            Height          =   1230
            Left            =   120
            Sorted          =   -1  'True
            TabIndex        =   10
            Top             =   600
            Width           =   2415
         End
         Begin VB.Label Label5 
            Caption         =   "Contexte associ� : "
            Height          =   255
            Left            =   2640
            TabIndex        =   12
            Top             =   240
            Width           =   1575
         End
         Begin VB.Label Label4 
            Caption         =   "Mot cl� : "
            Height          =   255
            Left            =   120
            TabIndex        =   11
            Top             =   240
            Width           =   1575
         End
      End
      Begin VB.TextBox lblLibelle 
         Height          =   285
         Left            =   120
         TabIndex        =   1
         Top             =   1740
         Width           =   4935
      End
      Begin MSComctlLib.ImageCombo ImageCombo1 
         Height          =   330
         Left            =   120
         TabIndex        =   0
         Top             =   600
         Width           =   2655
         _ExtentX        =   4683
         _ExtentY        =   582
         _Version        =   393216
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Locked          =   -1  'True
      End
      Begin VB.TextBox FichierHTML 
         Height          =   285
         Left            =   120
         MaxLength       =   50
         TabIndex        =   2
         Top             =   2520
         Width           =   4935
      End
      Begin VB.Label Label6 
         Caption         =   "Choix de la langue : "
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   2895
      End
      Begin VB.Image Image5 
         Height          =   585
         Left            =   4440
         Picture         =   "frmInsrtDoc.frx":0000
         ToolTipText     =   "Visualiser le fichier"
         Top             =   960
         Width           =   630
      End
      Begin VB.Image Image1 
         Height          =   240
         Left            =   5160
         Picture         =   "frmInsrtDoc.frx":13C2
         ToolTipText     =   "Parcourir"
         Top             =   2520
         Width           =   240
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Fichier HTML associ� au document d'aide : "
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   2160
         Width           =   5175
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Libelle associ� au document : (maximum 70 caract�res )"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1440
         Width           =   4215
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Cr�er � partir d'un fichier WORD : "
      Height          =   1815
      Left            =   120
      TabIndex        =   4
      Top             =   5520
      Width           =   5535
      Begin VB.TextBox fileWord 
         Height          =   285
         Left            =   120
         TabIndex        =   3
         Top             =   720
         Width           =   4935
      End
      Begin VB.Image Image6 
         Height          =   585
         Left            =   840
         Picture         =   "frmInsrtDoc.frx":150C
         ToolTipText     =   "Convertir le fichier WORD en format HTML"
         Top             =   1080
         Width           =   1380
      End
      Begin VB.Image Image3 
         Height          =   585
         Left            =   120
         Picture         =   "frmInsrtDoc.frx":3F5A
         ToolTipText     =   "Visualiser le fichier"
         Top             =   1080
         Width           =   630
      End
      Begin VB.Image Image2 
         Height          =   240
         Left            =   5160
         Picture         =   "frmInsrtDoc.frx":531C
         ToolTipText     =   "Parcourir"
         Top             =   720
         Width           =   240
      End
      Begin VB.Label Label3 
         Caption         =   "Fichier Word de r�f�rence : "
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   2775
      End
   End
   Begin VB.Image Image4 
      Height          =   480
      Left            =   5040
      Picture         =   "frmInsrtDoc.frx":5466
      ToolTipText     =   "Ok"
      Top             =   7440
      Width           =   630
   End
End
Attribute VB_Name = "frmInsertDoc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private pLibelle(6) As String
Private idLangue As Integer
Private aClient As Client
Public NodeDoc As Node


Private Sub Command1_Click()

Me.List1.ListIndex = -1
Set Me.TreeView1.SelectedItem = Nothing

End Sub

Private Sub Command2_Click()
    frmKeyWords.Show vbModal
End Sub

Private Sub FichierHTML_GotFocus()

Me.FichierHTML.SelStart = 0
Me.FichierHTML.SelLength = Len(Me.FichierHTML.Text)

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

    If KeyAscii = 27 Then Unload Me

End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

If KeyCode = 112 Then Shell App.Path & "\HELPDEAL.EXE DEAL|1|DEAL|HELPDESKTOP|CT33", vbNormalFocus

End Sub

Private Sub Form_Load()

Dim aApp As Applicatif
Dim aMenu As Menu

'-> Charger la Combo1
Me.ImageCombo1.ImageList = frmLangue.ImageList1
For i = 1 To 6
    Me.ImageCombo1.ComboItems.Add i, , frmLangue.ImageList1.ListImages(i).Tag, i
Next
Me.ImageCombo1.ComboItems(CurCodeLangue).Selected = True
ImageCombo1_Click

'-> Charger la liste des mots cl�s
For i = 1 To frmKeyWords.Keywords.Count
    Me.List1.AddItem frmKeyWords.Keywords(i).Name
Next

If Me.NodeDoc Is Nothing Then
    pLibelle(1) = strTempoTxt
    Me.lblLibelle.Text = strTempoTxt
Else
    '-> Charger les libelles
    Set aApp = Applicatifs(CurApp)
    Set aMenu = aApp.Menus(NodeDoc.Key)
    For i = 1 To 6
        pLibelle(i) = aMenu.GetLbByLangue(i)
    Next
    '-> Charger la langue courrante
    Me.ImageCombo1.ComboItems(CurCodeLangue).Selected = True
    Me.lblLibelle.Text = pLibelle(CurCodeLangue)
    Me.FichierHTML.Text = Me.NodeDoc.Child.Text
    '-> S�lectionner les mots cl�s
    If Not aMenu.aLien Is Nothing Then
        '-> S�lectionner le mot cl�
        Me.List1.Text = aMenu.aLien.KeyWordKey
        '-> Charger le contexte associ� au mot cl�
        Me.TreeView1.Nodes(aMenu.aLien.ContexteKey).Selected = True
    Else
        Me.List1.ListIndex = -1
    End If
End If

'-> Pointer sur le client en cours
Set aClient = Clients(UCase$(CurClient))

'-> Rendre non senstitif le bouton si pas de conte
If Me.List1.ListCount = 0 Then Me.Command1.Enabled = False

End Sub

Private Sub Image1_DblClick()

Dim htmFile As String

Me.CommonDialog1.FileName = ""
Me.CommonDialog1.DialogTitle = "S�lection d'un fichier au format internet"
Me.CommonDialog1.Filter = "Format HTML (*.html)|*.html|Format HTM (*.htm)|*.htm|Tous les fichiers|*.*"
Me.CommonDialog1.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly Or cdlOFNPathMustExist
Me.CommonDialog1.InitDir = aClient.Serveur
Me.CommonDialog1.ShowOpen

If Me.CommonDialog1.FileName = "" Then Exit Sub

'-> V�rifier que le nom de fichier ne depasse pas 50 caract�re
htmFile = Entry(NumEntries(Me.CommonDialog1.FileName, "\"), Me.CommonDialog1.FileName, "\")

If Len(htmlFile) > 50 Then
    MsgBox "Le nom du fichier HTML ne doit pas d�passer 50 caract�res. Veuillez changer le fichier associ�", vbExclamation + vbOKOnly, "Avertissement"
    Exit Sub
End If

Me.FichierHTML.Text = htmFile

End Sub

Private Sub Image2_DblClick()

On Error Resume Next

Dim aClient As Client

Me.CommonDialog1.FileName = ""
Me.CommonDialog1.DialogTitle = "S�lection d'un fichier au format WORD"
Me.CommonDialog1.Filter = "Format WORD (*.Doc)|*.doc|Tous les fichiers|*.*"
Me.CommonDialog1.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly Or cdlOFNPathMustExist

Set aClient = Clients(CurClient)
If aClient.Doc_Wait <> "" Then
    If Dir$(aClient.Doc_Wait, vbDirectory) <> "" Then
        If Dir$(aClient.Doc_Wait & CurApp, vbDirectory) <> "" Then
            Me.CommonDialog1.InitDir = aClient.Doc_Wait & CurApp
        Else
            Me.CommonDialog1.InitDir = aClient.Doc_Wait
        End If
    End If
End If
Me.CommonDialog1.ShowOpen

If Me.CommonDialog1.FileName = "" Then Exit Sub

Me.fileWord.Text = Me.CommonDialog1.FileName


End Sub

Private Sub Image3_Click()

Dim aWord As Object

On Error GoTo GestError

If Trim(Me.fileWord.Text) = "" Then Exit Sub
If Dir$(Me.fileWord.Text, vbNormal) = "" Then Exit Sub

'-> Lancer Word
Me.MousePointer = 11
Set aWord = CreateObject("Word.Application")
aWord.Documents.Open Me.fileWord.Text
aWord.Visible = True
Set aWord = Nothing
Me.MousePointer = 0

Exit Sub

GestError:

    MsgBox "Une erreur est intervenue lors de l'ouverture de MsWord", vbCritical + vbOKOnly, "Impossible de lire le fichier"
    Set aWord = Nothing
    
End Sub

Private Sub Image4_Click()

Dim aNode As Node
Dim aNode2 As Node
Dim NodeKey As Node
Dim aApp As Applicatif
Dim fileHtml As String
Dim aMenu As Menu
Dim aLien As Lien

'-> V�rifier que le libelle Fran�ais est renseign�
If Trim(pLibelle(1)) = "" Then
    MsgBox "Le libell� de la langue fran�aise est obligatoire veuillez le renseigner.", vbCritical + vbOKOnly, "Erreur"
    Me.ImageCombo1.ComboItems(1).Selected = True
    Me.lblLibelle.SetFocus
    Exit Sub
End If

'-> R�cup�ration du nom du fichier HTMl de r�f�rence
fileHtml = Entry(NumEntries(Me.FichierHTML.Text, "\"), Me.FichierHTML.Text, "\")

'-> Pointer sur l'applicatif
Set aApp = Applicatifs(CurApp)

'-> Gestion si cr�tation ou modification
If Me.NodeDoc Is Nothing Then
    '-> Cr�ation
    If aApp.Tree.SelectedItem Is Nothing Then
        '-> Ajouter le node sur la racine
        Set aNode = aApp.Tree.Nodes.Add(, , "MENU|" & IndexNodeCreate, pLibelle(1), "Document")
    Else
        RetourChx = -1
        If aApp.Tree.SelectedItem.Tag = "DOCUMENT" Then
            frmChxDest.Option1(4).Enabled = False
            frmChxDest.Option1(1).Value = True
        End If
        frmChxDest.Show vbModal
        If RetourChx = -1 Then
            Unload Me
            Exit Sub
        End If
        
        '-> Ajouter sous le node en cours de s�lection s'il existe
        Set aNode = aApp.Tree.Nodes.Add(aApp.Tree.SelectedItem.Index, RetourChx, "MENU|" & IndexNodeCreate, pLibelle(1), "Document")
    
        '-> Ouvrir le node en cours de s�lection
        If RetourChx = 4 Then aApp.Tree.SelectedItem.Expanded = True
    End If
    
    '-> Faire en sorte que le nouveau node soit visible
    aNode.EnsureVisible
    
    '-> Indiquer que c'est un fichier d'aide
    aNode.Tag = "DOCUMENT"
    
    '-> Initialiser le nouvel objet menu
    Set aMenu = New Menu
    aMenu.idLangueOrigine = CurCodeLangue
        
    '-> Ajouter dans la collection des menus de l'applicatif en cours
    aApp.Menus.Add aMenu, "MENU|" & IndexNodeCreate
    
    '-> Incr�menter le compteur de node cr��
    IndexNodeCreate = IndexNodeCreate + 1
    
    aNode.Expanded = True
           
    '-> AJoyer le node qui correspond au fichier HTML
    Set aNode2 = aApp.Tree.Nodes.Add(aNode.Index, 4, "MENU|" & IndexNodeCreate, fileHtml, "FichierHTM")
    aNode2.Tag = "HTML"
    
    '-> Incr�menter le compteur de node cr��
    IndexNodeCreate = IndexNodeCreate + 1
    
    '-> Cr�er le node du mot cl�
    Set NodeKey = aApp.Tree.Nodes.Add(aNode.Index, 4, "MENU|" & IndexNodeCreate, "")
    NodeKey.Tag = "KEYWORD"
        
    '-> Incr�menter le compteur de node cr��
    IndexNodeCreate = IndexNodeCreate + 1
Else
    '-> Modification d'un node d�ja existant
    NodeDoc.Text = pLibelle(CurCodeLangue)
    NodeDoc.Child.Text = fileHtml
    '-> Mettre � jour le menu
    Set aMenu = aApp.Menus(NodeDoc.Key)
    '-> Enregistrer les langues
    For i = 1 To 6
        aMenu.SetLbByLangue i, pLibelle(i)
    Next
    Set aNode = NodeDoc
    '-> Pointer sur le node du mot cle
    Set NodeKey = NodeDoc.Child.LastSibling
End If 'Si modification ou cr�ation

Dim ToCreate As Boolean

'-> Afficher le libell� de l'affectation s'il elle existe
If Me.List1.ListIndex <> -1 And Not Me.TreeView1.SelectedItem Is Nothing Then

    '-> V�rifier s'il n'existe pas d�ja un mot cl�
    If Not aMenu.aLien Is Nothing Then
        If aMenu.aLien.KeyWordKey = UCase$(Me.List1.Text) And _
           aMenu.aLien.ContexteKey = UCase$(Me.TreeView1.SelectedItem.Key) Then
            '-> Ne rien faire au niveau des mots cl�s
            ToCreate = False
        Else
            '-> Supprimer l'ancienne association
            aApp.Liens.Remove aNode.Key & "|" & UCase$(aMenu.aLien.KeyWordKey & "|" & aMenu.aLien.ContexteKey)
            ToCreate = True
        End If
    Else
        ToCreate = True
    End If
        
    If ToCreate Then
        '-> Les 2 sont renseign�s : on peut cr�er l'objet
        Set aMenu.aLien = New Lien
        aMenu.aLien.KeyWordKey = UCase$(Me.List1.Text)
        aMenu.aLien.ContexteKey = UCase$(Me.TreeView1.SelectedItem.Key)
        aMenu.aLien.Applicatif = aApp.Name
            
        '-> Ajouter le lien dans la collection des liens de l'applicatif
        Set aLien = New Lien
        aLien.ContexteKey = aMenu.aLien.ContexteKey
        aLien.KeyWordKey = aMenu.aLien.KeyWordKey
        aApp.Liens.Add aLien, aNode.Key & "|" & UCase$(aLien.KeyWordKey & "|" & aLien.ContexteKey)
        
        '-> Donner la bonne icone
        NodeKey.Image = "KeyWord"
        NodeKey.Text = Me.List1.Text & " - " & Me.TreeView1.SelectedItem.Text
    End If
    
Else
    '-> Supprimer l'affectation au niveau applicatif si elle existe
    If Not aMenu.aLien Is Nothing Then _
        aApp.Liens.Remove aNode.Key & "|" & UCase$(aMenu.aLien.KeyWordKey & "|" & aMenu.aLien.ContexteKey)

    '-> Les deux ne sont pas renseign�s : supprimer l'affectation
    Set aMenu.aLien = Nothing
    
    '-> Modifier l'icone du node du mot cl�
    NodeKey.Image = "KeyWord#"
    
    '-> Afficher le texte
    NodeKey.Text = "<Pas d'affectation>"

End If

'-> Enregistrer les langues pour le menu
For i = 1 To 6
    aMenu.SetLbByLangue i, pLibelle(i)
Next

'-> D�charger la feuille
Unload Me

'-> Indiquer dans l'application d'origine que l'on doit enregistrer
aApp.ToSave = True

aApp.Tree.SetFocus
aNode.Selected = True
Set SourceNode = aNode

'-> Afficher le fichier HTML
DisplayHtmlFile fileHtml, CurApp

End Sub

Private Sub Image5_Click()

Dim FileToSearch  As String

'-> Quitter si nom de fichier pas renseign�
If Trim(Me.FichierHTML.Text) = "" Then Exit Sub

'-> V�rifier si nom fichier ou Chemin + nom fichier
If InStr(1, Me.FichierHTML.Text, "\") <> 0 Then
    FileToSearch = Me.FichierHTML.Text
Else
    '-> Juste le nom de fichier : ajouter le path pour voir s'il existe
    FileToSearch = aClient.Serveur & Format(CurCodeLangue, "00") & "\" & Me.FichierHTML.Text
End If

'-> Retraiter le fichier pour supprimer les signets et v�rifier s'il existe
If InStr(1, FileToSearch, "#") <> 0 Then
    strTmp = Mid$(FileToSearch, 1, InStr(1, FileToSearch, "#") - 1)
    If Dir$(strTmp, vbNormal) = "" Then
        MsgBox "Impossible de trouver le fichier HTMl." & Chr(13) & strTmp, vbExclamation + vbOKOnly, "Impossible de visualiser"
        Me.FichierHTML.SetFocus
        Exit Sub
    End If
End If

frmViewHtm.Caption = frmViewHtm.Caption & FileToSearch
frmViewHtm.WebBrowser1.Navigate FileToSearch
frmViewHtm.Show vbModal

End Sub

Private Sub Image6_Click()

Dim Rep
Dim DocFile As String
Dim wwwFile As String
Dim aWord As Object
Dim aDoc As Object
Dim TempoFile As String

On Error GoTo GestError

If Trim(Me.fileWord.Text) = "" Then Exit Sub
If Dir$(Me.fileWord.Text, vbNormal) = "" Then Exit Sub

Me.MousePointer = 11

'-> R�cup�rer le nom du fichier Doc
DocFile = Entry(NumEntries(Me.fileWord.Text, "\"), Me.fileWord.Text, "\")
wwwFile = Entry(1, DocFile, ".") & ".Html"

'-> Demander le nom du fichier
TempoFile = wwwFile
Rep = InputBox("Cr�er le fichier : ", "Cr�ation d'un nouveau fichier HTML", TempoFile)
If Trim(Rep) = "" Then Exit Sub
wwwFile = Rep

'-> V�rification de la longueur du fichier
If Len(Rep) > 50 Then
    MsgBox "Attention : le nom du fichier ne doit pas ex�der 50 carcat�res.", vbCritical + vbOKOnly, "Erreur"
    Me.MousePointer = 0
    Exit Sub
End If

Me.MousePointer = 11
DoEvents

'-> Masquer Word
Set aWord = CreateObject("Word.Application")
aWord.Visible = False

'-> Ouvrir le fichier Doc
Set aDoc = aWord.Documents.Open(Me.fileWord.Text)

'-> G�n�rer le fichier d'aide
aDoc.SaveAs aClient.Serveur & wwwFile, 8

'-> Fermer word
aWord.Quit

Set aDoc = Nothing
Set aWord = Nothing

'-> Mettre le fichier dans le bon text box
Me.FichierHTML.Text = wwwFile

'-> Ouvrir le fichier g�n�rer dans la visionneuse
frmViewHtm.Caption = frmViewHtm.Caption & aClient.Serveur & wwwFile
frmViewHtm.WebBrowser1.Navigate aClient.Serveur & wwwFile
frmViewHtm.Show vbModal

Me.MousePointer = 0

Exit Sub

GestError:

    Set aDoc = Nothing
    Set aWord = Nothing
    Me.MousePointer = 0
    MsgBox "Une erreur est intervenue dans l'ouverture ou la g�n�ration du fichier HTML. V�rfiez que le fichier WORD n'est pas prot�g� par un mot de passe ou n'est pas en cours d'utilisation.", vbCritical + vbOKOnly, "Erreur"
    Exit Sub
    

End Sub

Private Sub ImageCombo1_Click()

pLibelle(idLangue) = Me.lblLibelle.Text
Me.lblLibelle.Text = pLibelle(Me.ImageCombo1.SelectedItem.Index)
idLangue = Me.ImageCombo1.SelectedItem.Index

End Sub

Private Sub ImageCombo1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then
    Me.lblLibelle.SetFocus
    If Me.ImageCombo1.SelectedItem Is Nothing Then _
    Me.ImageCombo1.ComboItems(idLangue).Selected = True
End If

End Sub

Private Sub lblLibelle_Change()

pLibelle(Me.ImageCombo1.SelectedItem.Index) = Me.lblLibelle.Text

End Sub

Private Sub lblLibelle_GotFocus()

Me.lblLibelle.SelStart = 0
Me.lblLibelle.SelLength = Len(Me.lblLibelle.Text)

End Sub

Private Sub Text1_Change()

End Sub

Private Sub List1_Click()

Dim aKeyWord As Keyword
Dim aCt As Contexte

'-> Quitter si pas de mots cl�s
If Me.List1.ListCount = 0 Then Exit Sub

If Me.List1.ListIndex = -1 Then Exit Sub

'-> Nettoyer la liste
Me.TreeView1.Nodes.Clear

'-> Afficher la liste des mots cl�s
Set aKeyWord = frmKeyWords.Keywords(UCase$(Me.List1.Text))
For Each aCt In aKeyWord.Contextes
    Me.TreeView1.Nodes.Add , , "CT" & aCt.Index, aCt.Libelle
Next

'-> S�lectionner le premier s'il y en a un
If Me.TreeView1.Nodes.Count <> 0 Then Me.TreeView1.Nodes(1).Selected = True

End Sub
