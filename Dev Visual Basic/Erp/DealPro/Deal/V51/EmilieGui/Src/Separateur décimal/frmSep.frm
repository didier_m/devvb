VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Analyse du registre"
   ClientHeight    =   1500
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6555
   Icon            =   "frmSep.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1500
   ScaleWidth      =   6555
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   "LOCALE_USER_DEFAULT : "
      Height          =   1335
      Left            =   3360
      TabIndex        =   5
      Top             =   120
      Width           =   3135
      Begin VB.Label Label8 
         Caption         =   "Séparateur de décimale :"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label Label7 
         Caption         =   "Séparateur de miliiers :"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   840
         Width           =   1935
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   2280
         TabIndex        =   7
         Top             =   840
         Width           =   615
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   2280
         TabIndex        =   6
         Top             =   480
         Width           =   615
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "LOCALE_SYSTEM_DEFAULT : "
      Height          =   1335
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3135
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   2280
         TabIndex        =   4
         Top             =   840
         Width           =   615
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   2280
         TabIndex        =   3
         Top             =   480
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "Séparateur de miliiers :"
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   840
         Width           =   2295
      End
      Begin VB.Label Label1 
         Caption         =   "Séparateur de décimale :"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   480
         Width           =   2175
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()

Dim Res As Long
Dim lpBuffer As String

'---> Analyse du registre avec la cle LOCALE_SYSTEM_DEFAULT

'-> Séparateur décimal
lpBuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SDECIMAL, lpBuffer, Len(lpBuffer))
Me.Label3.Caption = Mid$(lpBuffer, 1, 1)

'-> Groupement de milliers
lpBuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STHOUSAND, lpBuffer, Len(lpBuffer))
Me.Label4.Caption = Mid$(lpBuffer, 1, 1)


'---> Analyse du registre avec la cle LOCALE_USER_DEFAULT
'-> Séparateur décimal
lpBuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, lpBuffer, Len(lpBuffer))
Me.Label5.Caption = Mid$(lpBuffer, 1, 1)

'-> Groupement de milliers
lpBuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, lpBuffer, Len(lpBuffer))
Me.Label6.Caption = Mid$(lpBuffer, 1, 1)


End Sub
