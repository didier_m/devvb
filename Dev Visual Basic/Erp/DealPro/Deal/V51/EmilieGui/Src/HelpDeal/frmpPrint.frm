VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "SHDOCVW.DLL"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPrint 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Impression"
   ClientHeight    =   4275
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4290
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4275
   ScaleWidth      =   4290
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin SHDocVwCtl.WebBrowser WebBrowser1 
      Height          =   1095
      Left            =   5760
      TabIndex        =   6
      Top             =   480
      Width           =   2055
      ExtentX         =   3625
      ExtentY         =   1931
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
   Begin VB.PictureBox Picture1 
      AutoRedraw      =   -1  'True
      Height          =   255
      Left            =   120
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   269
      TabIndex        =   5
      Top             =   3480
      Visible         =   0   'False
      Width           =   4095
   End
   Begin VB.ListBox List1 
      Height          =   1230
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   4095
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Imprimer"
      Height          =   375
      Left            =   3000
      TabIndex        =   2
      Top             =   3840
      Width           =   1215
   End
   Begin MSComctlLib.ImageCombo ImageCombo1 
      Height          =   330
      Left            =   120
      TabIndex        =   1
      Top             =   2400
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   582
      _Version        =   393216
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Locked          =   -1  'True
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Choix de la langue"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   2040
      Width           =   3615
   End
   Begin VB.Label lblTitre 
      Caption         =   "Choix de l'imprimante :"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4095
   End
End
Attribute VB_Name = "frmPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---> Selon le type d'impression 0 -> Menu 1 ,Mots cl�s , 2
Public TypeImp As Integer
Dim idLangue As Integer
Public NaturePrint As String

Private Sub Command1_Click()

'---> Lancer l'impression

Select Case TypeImp
    
    Case 0 'Impression de la documentation
            PrintDocumentation
    Case 1 'Impression des rubriques du menu
            'PrintMenu
End Select

End Sub
Private Sub PrintDocumentation()

Dim i As Integer
Dim aApp As Applicatif
Dim aNode As Node
Dim TempPath As String
Dim aMenu As Object
Dim ToPrint As Boolean

'-> Afficher le progress de travail
Me.Picture1.Visible = True
Me.Picture1.Cls

For i = 1 To HelpDeal.TreeView1.Nodes.Count
    '-> Pointer sur le node
    Set aNode = HelpDeal.TreeView1.Nodes(i)
    '-> N'imprimer que les nodes de la nature s�lectionn�
    If NaturePrint = "CLI" Then
        If Left(aNode.Key, 3) = "CLI" Then
            ToPrint = True
        Else
            ToPrint = False
        End If
    Else
        If Left(aNode.Key, 3) = "CLI" Then
            ToPrint = False
        Else
            ToPrint = True
        End If
    End If
    '-> V�rifier si c'est un fichier
    If aNode.Tag <> "CHAPITRE" And ToPrint Then
        '-> V�rifier la nature du menu
        If Left(aNode.Key, 3) = "CLI" And aNode.Key <> "CLIENTMENU" Then
            '-> Pointer sur l'applicatif
            Set aApp = CliApplicatifs(CurApp)
            Set aMenu = aApp.Menus(aNode.Key)
            If aMenu.IsUrl Then
                TempPath = PathDocClient & aMenu.FichierHTML
            Else
                TempPath = ""
            End If 'Si client
        ElseIf aNode.Key <> "DEALMENU" And aNode.Key <> "CLIENTMENU" Then  '-> Cas de la documentation DEAL
            '-> Pointer sur l'applicatif
            Set aApp = Applicatifs(CurApp)
            Set aMenu = aApp.Menus(aNode.Key)
            If aMenu.IsUrl Then
                TempPath = PathDocDeal & aMenu.FichierHTML
            Else
                TempPath = ""
            End If 'Si client
        End If 'Selon la nature de l'impression
        '-> Impression du fichier
        If TempPath <> "" Then
            Me.WebBrowser1.Navigate TempPath
            DoEvents
            Me.WebBrowser1.ExecWB OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER
        End If
    End If 'Si c'est un document
    DoEvents
    Me.Picture1.Line (0, 0)-((i / HelpDeal.TreeView1.Nodes.Count) * Me.Picture1.ScaleWidth, Me.Picture1.ScaleHeight), &HC00000, BF
Next 'Pour tous les nodes

'-> D�charger la feuille
Unload Me

End Sub


Private Sub Form_Load()

Dim x As Printer

'-> Charger la Combo1 pour les codes langues
Me.ImageCombo1.ImageList = frmLangue.ImageList1
For i = 1 To 6
    Me.ImageCombo1.ComboItems.Add i, , frmLangue.ImageList1.ListImages(i).Tag, i
Next
Me.ImageCombo1.ComboItems(CurCodeLangue).Selected = True


'-> Ajouter les imprimantes dans la liste
For Each x In Printers
    Me.List1.AddItem x.DeviceName
    '-> S�lectionner dans la liste si c'est l'imprimante par d�faut
    If x.DeviceName = Printer.DeviceName Then _
        Me.List1.Selected(Me.List1.ListCount - 1) = True
Next

'-> Ajouter la visualisation �cran
If Me.TypeImp = 2 Then Me.List1.AddItem "Visualisation �cran"

idLangue = CurCodeLangue

End Sub

Private Sub ImageCombo1_Click()

If Not Me.ImageCombo1.SelectedItem Is Nothing Then _
    idLangue = Me.ImageCombo1.SelectedItem.Index

End Sub

Private Sub ImageCombo1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then
    If Me.ImageCombo1.SelectedItem Is Nothing Then _
    Me.ImageCombo1.ComboItems(idLangue).Selected = True
End If


End Sub
