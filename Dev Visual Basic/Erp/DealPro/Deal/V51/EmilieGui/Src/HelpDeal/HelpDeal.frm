VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "shdocvw.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form HelpDeal 
   Caption         =   "Moteur d'aide en ligne DEAL Informatique"
   ClientHeight    =   7695
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11430
   Icon            =   "HelpDeal.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   513
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   762
   Visible         =   0   'False
   WindowState     =   2  'Maximized
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8520
      Top             =   1320
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Height          =   1200
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   630
      _ExtentX        =   1111
      _ExtentY        =   2117
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList3"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "LANGUE"
            Object.ToolTipText     =   "Choix du code langue"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "APPLICATIF"
            Object.ToolTipText     =   "Choix de l'applicatif"
            ImageIndex      =   7
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox SpliterH 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   60
      Left            =   5160
      MousePointer    =   7  'Size N S
      ScaleHeight     =   60
      ScaleWidth      =   2100
      TabIndex        =   6
      Top             =   4080
      Width           =   2100
   End
   Begin VB.PictureBox DocDeal 
      Height          =   1935
      Left            =   4080
      ScaleHeight     =   125
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   85
      TabIndex        =   4
      Top             =   3840
      Width           =   1335
      Begin SHDocVwCtl.WebBrowser BrowserDeal 
         CausesValidation=   0   'False
         Height          =   1815
         Left            =   0
         TabIndex        =   5
         Top             =   0
         Width           =   1215
         ExtentX         =   2143
         ExtentY         =   3201
         ViewMode        =   0
         Offline         =   0
         Silent          =   0
         RegisterAsBrowser=   0
         RegisterAsDropTarget=   1
         AutoArrange     =   0   'False
         NoClientEdge    =   0   'False
         AlignLeft       =   0   'False
         ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
         Location        =   "res://C:\WINNT\system32\shdoclc.dll/dnserror.htm#http:///"
      End
   End
   Begin VB.PictureBox Spliter 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   975
      Left            =   3120
      MousePointer    =   9  'Size W E
      ScaleHeight     =   975
      ScaleWidth      =   60
      TabIndex        =   3
      Top             =   720
      Width           =   60
   End
   Begin VB.PictureBox DocClient 
      Height          =   1935
      Left            =   3960
      ScaleHeight     =   125
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   85
      TabIndex        =   0
      Top             =   1440
      Width           =   1335
      Begin SHDocVwCtl.WebBrowser BrowserClient 
         CausesValidation=   0   'False
         Height          =   1815
         Left            =   0
         TabIndex        =   1
         Top             =   0
         Width           =   1215
         ExtentX         =   2143
         ExtentY         =   3201
         ViewMode        =   0
         Offline         =   0
         Silent          =   0
         RegisterAsBrowser=   0
         RegisterAsDropTarget=   1
         AutoArrange     =   0   'False
         NoClientEdge    =   0   'False
         AlignLeft       =   0   'False
         ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
         Location        =   "res://C:\WINNT\system32\shdoclc.dll/dnserror.htm#http:///"
      End
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   3375
      Left            =   720
      TabIndex        =   2
      Top             =   1440
      Width           =   3135
      _ExtentX        =   5530
      _ExtentY        =   5953
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      OLEDragMode     =   1
      OLEDropMode     =   1
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6240
      Top             =   720
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":437A
            Key             =   "ChapitreFerme"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":5054
            Key             =   "ChapitreOuvert"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":592E
            Key             =   "Document"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":6208
            Key             =   "OpenDoc"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":665A
            Key             =   "CloseDoc"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   2760
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":6974
            Key             =   ""
            Object.Tag             =   "Fran�ais"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":6DC6
            Key             =   ""
            Object.Tag             =   "Anglais"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":7218
            Key             =   ""
            Object.Tag             =   "Allemand"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":766A
            Key             =   ""
            Object.Tag             =   "Portuguais"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":7984
            Key             =   ""
            Object.Tag             =   "Espagnol"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":7DD6
            Key             =   ""
            Object.Tag             =   "Italien"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":8228
            Key             =   "LogoDeal"
            Object.Tag             =   "LogoDeal"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList3 
      Left            =   5880
      Top             =   2040
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":8B02
            Key             =   "FR"
            Object.Tag             =   "Fran�ais"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":8F54
            Key             =   "GB"
            Object.Tag             =   "Anglais"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":93A6
            Key             =   "AL"
            Object.Tag             =   "Allemand"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":97F8
            Key             =   "PT"
            Object.Tag             =   "Portuguais"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":9B12
            Key             =   "ES"
            Object.Tag             =   "Espagnol"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":9F64
            Key             =   "IT"
            Object.Tag             =   "Italien"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "HelpDeal.frx":A3B6
            Key             =   "Logo"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuAide 
      Caption         =   "Aide"
      Visible         =   0   'False
      Begin VB.Menu mnuDisplayNavigation 
         Caption         =   "Palette de de navigation"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPrintFile 
         Caption         =   "Imprimer les fichiers d'aide"
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFind 
         Caption         =   "Rechercher"
         Shortcut        =   {F3}
      End
      Begin VB.Menu mnuSep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuVisible 
         Caption         =   "Aide toujours visible"
      End
   End
End
Attribute VB_Name = "HelpDeal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub BrowserClient_GotFocus()

frmNaviga.Caption = "Palette de navigation : CLIENT"
Set frmNaviga.Browser = Me.BrowserClient

End Sub

Private Sub BrowserDeal_GotFocus()

frmNaviga.Caption = "Palette de navigation : DEAL"
Set frmNaviga.Browser = Me.BrowserDeal

End Sub

Private Sub DocClient_Resize()

'-> Positionner le browser
Me.BrowserClient.Top = 0
Me.BrowserClient.Left = 0
Me.BrowserClient.Width = Me.DocClient.Width - LargeurBordure
Me.BrowserClient.Height = Me.DocClient.Height - HauteurBordure


End Sub

Private Sub Form_Load()

Dim LigneCommande As String
Dim Temp
Dim Res As Long
Dim RECT As RECT

'-> Positionner le spliter au milieu
Me.Spliter.Left = Me.Width / 2

'-> Indiquer que l'on est en cours de chargement
IsLoading = True

'-> R�cup�rer les dimensions des bordures
LargeurBordure = GetSystemMetrics(SM_CXEDGE)
HauteurBordure = GetSystemMetrics(SM_CYEDGE)

'-> Initialiser le compteur de nodes
IndexNodeCreate = 1000000000

'-> Cr�ation des propaths
If InStr(1, UCase$(App.Path), "EMILIEGUI\SRC") <> 0 Then
    IniPath = "M:\DealVB\Dev Visual Basic\Erp\Turbo"
Else
    CreatePath IniPath
End If

'-> Construire le chemin complet
IniPath = IniPath & "\TurboMaq.ini"

'-> Quitter si on ne trouve pas ce fichier
If Dir$(IniPath, vbNormal) = "" Then
    MsgBox "Impossible de trouver le fichier " & Chr(13) & IniPath, vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

'-> Charger les noms logiques par leur valeur
GetPathMotCles

'-> Initialiser la collection des applicatifs
Set Applicatifs = New Collection
Set CliApplicatifs = New Collection
Set KeyWords = New Collection
Set CliKeyWords = New Collection

'-> Charger la feuille des applicatifs
Load frmApp
'-> Charger la palette de navigation
Load frmNaviga

'-> Placer le feuilles au d�marrage
'-> R�cup�rer les dimensions de la feuille
Res = GetClientRect(Me.hwnd, RECT)
frmApp.Top = 0

frmNaviga.Top = 0
frmNaviga.Left = Me.ScaleX(RECT.Right, 3, 1) - frmNaviga.Width
'frmApp.Left = frmNaviga.Left - frmApp.Width '- Me.ScaleX(LargeurBordure * 2, 3, 1)


SetParent frmNaviga.hwnd, Me.hwnd
frmNaviga.Visible = True

'SetParent frmApp.hWnd, Me.hWnd
'frmApp.Visible = True

'-> Charger la liste des clients
LoadListeClient

'-> R�cup�ration de la ligne de commande
LigneCommande = Command$

'-> R�cup�ration du code client
If NumEntries(LigneCommande, "|") >= 1 Then CurClient = Trim(UCase$(Entry(1, LigneCommande, "|")))

'-> R�cup�ration du code langue
If NumEntries(LigneCommande, "|") >= 2 Then CurLangue = Trim(UCase$(Entry(2, LigneCommande, "|")))

'-> R�cup�ration du code applicatif
If NumEntries(LigneCommande, "|") >= 3 Then CurApp = Trim(UCase$(Entry(3, LigneCommande, "|")))

'-> R�cup�ration du code du mot cl�
If NumEntries(LigneCommande, "|") >= 4 Then CurKeyWord = Trim(UCase$(Entry(4, LigneCommande, "|")))

'-> R�cup�ration du contexte
If NumEntries(LigneCommande, "|") >= 5 Then CurCt = Trim(UCase$(Entry(5, LigneCommande, "|")))

'-> Construire le path de la documentation DEAL
PathDocDeal = PathLecteur & "\Dealpro\Deal\" & PathVersion & "\Aide\"

'*****************************************
'* V�rification de la variable CurCLIENT *
'*****************************************

'-> V�rifier si on trouve la documentation DEAL INFORMATIQUE
If Dir$(PathDocDeal, vbDirectory) <> "." Then
    PathDocDeal = ""
    OkDocDeal = False
Else
    OkDocDeal = True
End If

'-> Tester si on analyse le client DEAL
If CurClient = "DEAL" Then
    '-> V�rifier si on a trouv� la docdeal
    If OkDocDeal Then
        '-> Rendre inop�rant le client : on n'affichera que la doc DEAL
        PathDocClient = ""
        OkDocClient = False
    Else
EndDoc:
        '-> Il n'y a pa s de doc � afficher : on se barre
        MsgBox "Impossible d'afficher la documentation : Client DEAL introuvable.", vbCritical + vbOKOnly, "Erreur fatale"
        Unload Me
    End If
Else
    If Not IsClient(CurClient) Then
        '-> On ne peut afficher l'aide en ligne du client
        PathDocClient = ""
        OkDocClient = False
        '-> V�rifier si a trouv� la doc DEAL
        If Not OkDocDeal Then GoTo EndDoc '-> Pas de doc � afficher : on se barre
    Else
        '-> On peut afficher la documentation du client
        OkDocClient = True
        '-> Ne pas affecter le path Doc : il est D�ja affect� par la fonction isClient
    End If
End If

'*****************************************
'* V�rification de la variable CurLangue *
'*****************************************

If CurLangue = "" Then
    '-> Fran�ais par d�faut
    CurCodeLangue = 1
Else
    '-> V�rifier le code langue
    If Not IsCodeLangue(CurLangue) Then
        '-> Fran�ais par d�faut
        CurCodeLangue = 1
    Else
        '-> Code langue sp�cifi�
        CurCodeLangue = CInt(CurLangue)
    End If
End If

'******************************************
'* V�rification de la variable Applicatif *
'******************************************

If CurApp = "" Then
    '-> Ne rien faire : la variable applicatif n'est pas renseign�e
Else
    '-> V�rifier si la variable pass�e correspond � un applicatif R�el
    Select Case UCase$(CurApp)
        Case "EMILIE"
            CurApp = "DEAL"
        Case "NATHALIE", "JULIE", "LUCIE"
            CurApp = "GESCOM"
    End Select
    If Not IsApp(CurApp) Then CurApp = ""
End If
    
'-> S�lectionn� le code langue renseign�
Me.Toolbar1.Buttons(1).Image = Me.ImageList3.ListImages(CurCodeLangue).Index

'-> Indiquer que le chargement est finit
IsLoading = False

'-> Charg� le module de traduction du client si renseign� et ses mots cl�s
If OkDocClient Then
    LoadTraductFile PathDocClient
    LoadKeyWordFile True
    LoadCtFile True
    LoadLienApp True
Else
    LoadTraductFile PathDocDeal
End If

'-> Charg� les mots cl�s de DEAL
If OkDocDeal Then
    LoadKeyWordFile False
    LoadCtFile False
    LoadLienApp False
End If

'-> Indiquateur d'affichage
DisplayDocDeal = True
DisplayDocClient = False

'-> Si l'applicatif est renseign�, charg� l'applicatif sp�cifi�
If CurApp <> "" Then
    '-> Afficher l'applicatif sp�cifi�
    DisplayApp CurApp
    Me.Toolbar1.Buttons(2).Image = Me.ImageList3.ListImages(7).Index
    frmApp.ImageCombo1.ComboItems(Applicatifs(CurApp).IndexMenu).Selected = True
    '-> Afficher les contextes si necessaire
    If CurKeyWord <> "" Then
        SearchContexte
    Else
        HelpDeal.Visible = True
    End If
Else
    HelpDeal.Visible = True
End If


End Sub

Private Sub Form_Resize()

On Error Resume Next

Dim Res As Long
Dim aRect As RECT

'-> R�cup�rer la taille de la zone cliente
Res = GetClientRect(Me.hwnd, aRect)

'-> V�rifier que le split est bien positionn�
If Me.Spliter.Left > Me.ScaleX(Me.Width, 1, 3) Then Spliter.Left = Me.ScaleX(Me.Width / 2, 1, 3)

'-> Positionner le spliter Vertical
Me.Spliter.Top = 0
Me.Spliter.Height = aRect.Bottom

'-> Hauteru de la barre d'outils
Me.Toolbar1.Height = aRect.Bottom

'-> Positionner le treeview1
Me.TreeView1.Left = Me.Toolbar1.Width
Me.TreeView1.Top = 0
Me.TreeView1.Height = aRect.Bottom
Me.TreeView1.Width = Me.Spliter.Left - Me.Toolbar1.Width

'-> Positionner le PictureBox
Me.DocDeal.Left = Me.Spliter.Left + Me.Spliter.Width
Me.DocDeal.Top = Me.TreeView1.Top
Me.DocDeal.Width = aRect.Right - Me.DocDeal.Left

'-> Positionner le spliter Horizontal
Me.SpliterH.Left = Me.TreeView1.Width
Me.SpliterH.Width = aRect.Right - Me.SpliterH.Left


'-> Positionner la doc client
Me.DocClient.Left = Me.DocDeal.Left
Me.DocClient.Width = Me.DocDeal.Width

'-> Gestion des treeviews
If DisplayDocDeal And DisplayDocClient Then
    '-> Rendre le spliter visible
    Me.SpliterH.Visible = True
    '-> Positionner la doc de DEAL en haut
    Me.DocDeal.Top = Me.TreeView1.Top
    Me.DocDeal.Height = Me.SpliterH.Top - DocDeal.Top
    '-> Positionner la doc CLIENT en bas
    Me.DocClient.Top = Me.DocDeal.Top + Me.DocDeal.Height + Me.SpliterH.Height
    Me.DocClient.Height = aRect.Bottom - Me.DocClient.Top
Else
    Me.SpliterH.Visible = False
    DocClient.Height = Me.TreeView1.Height
    DocDeal.Height = Me.TreeView1.Height
    DocClient.Top = Me.TreeView1.Top
    DocDeal.Top = Me.TreeView1.Top
End If

DocDeal.Visible = DisplayDocDeal
DocClient.Visible = DisplayDocClient

End Sub

Private Sub DocDeal_Resize()

'-> Positionner le browser
Me.BrowserDeal.Top = 0
Me.BrowserDeal.Left = 0
Me.BrowserDeal.Width = Me.DocDeal.Width - LargeurBordure
Me.BrowserDeal.Height = Me.DocDeal.Height - HauteurBordure

End Sub

Private Sub Form_Unload(Cancel As Integer)

Dim Res As Long

'---> Fermer les fen�tres enfants
'Res = IsWindow(frmLangue.hWnd)
'If Res = 1 Then SendMessage frmLangue.hWnd, WM_CLOSE, 0, 0
'Res = IsWindow(frmApp.hWnd)
'If Res = 1 Then SendMessage frmApp.hWnd, WM_CLOSE, 0, 0
Res = IsWindow(frmNaviga.hwnd)
If Res = 1 Then SendMessage frmNaviga.hwnd, WM_CLOSE, 0, 0

'-> Fin de l'application
End

End Sub


Private Sub mnuDisplayNavigation_Click()

frmNaviga.Visible = Not Me.mnuDisplayNavigation.Checked
Me.mnuDisplayNavigation.Checked = Not Me.mnuDisplayNavigation.Checked


End Sub

Private Sub mnuFind_Click()
frmSearch.Show vbModal
End Sub


Private Sub mnuPrintFile_Click()

'---> Cette proc�dure imprime le contenu des fichiers ascii
If Me.TreeView1.SelectedItem.Key = "DEALMENU" Then
    frmPrint.NaturePrint = ""
Else
    frmPrint.NaturePrint = "CLI"
End If
frmPrint.TypeImp = 0
frmPrint.Show vbModal

End Sub

Private Sub mnuPrintRub_Click()

frmPrint.TypeImp = 1
frmPrint.Show vbModal

End Sub

Private Sub mnuVisible_Click()

If Not Me.mnuVisible.Checked Then
    Res = SetWindowPos(HelpDeal.hwnd, HWND_TOPMOST&, Me.ScaleX(HelpDeal.Left, 1, 3), Me.ScaleY(HelpDeal.Top, 1, 3), 0, 0, 33)
    mnuVisible.Checked = True
Else
    Res = SetWindowPos(HelpDeal.hwnd, HWND_NOTOPMOST&, Me.ScaleX(HelpDeal.Left, 1, 3), Me.ScaleY(HelpDeal.Top, 1, 3), 0, 0, 33)
    mnuVisible.Checked = False
End If

End Sub

Private Sub Spliter_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)

Dim aPt As POINTAPI
Dim Res As Long

Me.Spliter.MousePointer = 9

If Button = vbLeftButton Then
    '-> R�cup�rer dans un premier temps la position de la souris
    Res = GetCursorPos(aPt)
    '-> La convertir au coordonn�es clientes
    Res = ScreenToClient(Me.hwnd, aPt)
    '-> Poistionner le spilt en fonction
    If aPt.x > Me.ScaleX(2, 7, 3) And aPt.x < Me.ScaleX(Me.Width, 1, 3) - Me.ScaleX(2, 7, 3) Then Me.Spliter.Left = aPt.x
    Me.Spliter.BackColor = &H404040
    Me.Spliter.ZOrder
End If

End Sub

Private Sub Spliter_MouseUp(Button As Integer, Shift As Integer, x As Single, Y As Single)

'-> Repositionner les diff�rents objets

If Button = vbLeftButton Then Form_Resize
Me.Spliter.BackColor = &H8000000F
BringWindowToTop frmLangue.hwnd
BringWindowToTop frmApp.hwnd
Me.TreeView1.SetFocus

End Sub

Private Sub SpliterH_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)

Dim aPt As POINTAPI
Dim Res As Long

Me.Spliter.MousePointer = 9

If Button = vbLeftButton Then
    '-> R�cup�rer dans un premier temps la position de la souris
    Res = GetCursorPos(aPt)
    '-> La convertir au coordonn�es clientes
    Res = ScreenToClient(Me.hwnd, aPt)
    '-> Poistionner le spilt en fonction
    If aPt.Y > Me.ScaleY(2, 7, 3) And aPt.Y < Me.ScaleY(Me.Height, 1, 3) - Me.ScaleX(2, 7, 3) Then Me.SpliterH.Top = aPt.Y
    Me.SpliterH.BackColor = &H404040
    Me.SpliterH.ZOrder
End If

End Sub

Private Sub SpliterH_MouseUp(Button As Integer, Shift As Integer, x As Single, Y As Single)

'-> Repositionner les diff�rents objets
If Button = vbLeftButton Then Form_Resize
Me.SpliterH.BackColor = &H8000000F
BringWindowToTop frmLangue.hwnd
BringWindowToTop frmApp.hwnd
Me.TreeView1.SetFocus

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

Dim Res As Long
Dim Res1 As Long

Select Case Button.Key
    Case "LANGUE"
        Res1 = GetSystemMetrics(SM_CXEDGE&)
        frmLangue.Left = Me.ScaleX(Me.Toolbar1.Width + Res1, 3, 1) + Me.Left
        Res = GetSystemMetrics(SM_CYCAPTION&)
        Res1 = GetSystemMetrics(SM_CYEDGE&)
        frmLangue.Top = Me.ScaleY(Res + Res1 * 3, 3, 1) + Me.Top
        frmLangue.Show vbModal
    Case "APPLICATIF"
        Res1 = GetSystemMetrics(SM_CXEDGE&)
        frmApp.Left = Me.ScaleX(Me.Toolbar1.Width + Res1, 3, 1) + Me.Left
        Res = GetSystemMetrics(SM_CYCAPTION&)
        Res1 = GetSystemMetrics(SM_CYEDGE&)
        frmApp.Top = Me.ScaleY(Res + Res1 * 3 + Me.Toolbar1.ButtonHeight, 3, 1) + Me.Top
        frmApp.Show vbModal
End Select


End Sub

Private Sub TreeView1_KeyDown(KeyCode As Integer, Shift As Integer)

If KeyCode = 114 Then frmSearch.Show vbModal

End Sub

Private Sub TreeView1_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)

If Button = vbRightButton Then
    If Me.TreeView1.Nodes.Count = 0 Then
        Me.mnuFind.Enabled = False
        Me.mnuPrintFile.Enabled = False
    Else
        Me.mnuFind.Enabled = True
        '-> V�rifier que l'on click sur le bon node
        Me.mnuPrintFile.Enabled = False
        If Not Me.TreeView1.SelectedItem Is Nothing Then
            If Me.TreeView1.SelectedItem.Key = "DEALMENU" Or _
               Me.TreeView1.SelectedItem.Key = "CLIENTMENU" Then
                Me.mnuPrintFile.Enabled = True
            End If
        End If
    End If
    Me.PopupMenu Me.mnuAide
End If

End Sub

Public Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)

Dim DealApp As Applicatif
Dim CliApp As Applicatif
Dim aMenu As MenuDeal
Dim strTmp As String

'-> Selon la nature du node sur lequel on click
If Left(Node.Key, 3) = "CLI" Then
    '-> On est en affichage de la documentation client
    DisplayDocDeal = False
    DisplayDocClient = True
    Set frmNaviga.Browser = Me.BrowserClient
    frmNaviga.Caption = "Palette de navigation : CLIENT"
    '-> Pointer sur l'applicatif associ�
    Set CliApp = CliApplicatifs(CurApp)
    '-> Tester si on n'est pas sur le node ROOT
    If Node.Key = "CLIENTMENU" Then
        DisplayBlanckFile Me.BrowserClient
    Else
        '-> Pointer sur le menu associ�
        Set aMenu = CliApp.Menus(Node.Key)
        '-> Afficher si necessaire le fichier HTML associ�
        If aMenu.IsUrl Then
            '-> Afficher le fichier associ�
            If Dir$(PathDocClient & Format(CurCodeLangue, "00") & "\" & aMenu.FichierHTML, vbNormal) <> "" And Trim(aMenu.FichierHTML) <> "" Then
                BrowserClient.Navigate PathDocClient & Format(CurCodeLangue, "00") & "\" & aMenu.FichierHTML
            Else
                DisplayUnBlanckFile Me.BrowserClient
            End If
        Else
            '-> On est sur un chapitre
            DisplayBlanckFile Me.BrowserClient
        End If
    End If
Else
    frmNaviga.Caption = "Palette de navigation : DEAL"
    Set frmNaviga.Browser = Me.BrowserDeal
    '-> On doit afficher la doc de DEAL
    DisplayDocDeal = True
    DisplayDocClient = False
    '-> Pointer sur l'applicatif associ�
    Set DealApp = Applicatifs(CurApp)
    '-> Tester si on n'est pas sur le node ROOT
    If Node.Key = "DEALMENU" Then
        DisplayBlanckFile Me.BrowserDeal
    Else
        '-> Pointer sur le menu associ�
        Set aMenu = DealApp.Menus(Node.Key)
        '-> Si menu est un URL
        If aMenu.IsUrl Then
            '-> Afficher le fichier associ� : attention aux ancres
            If Trim(aMenu.FichierHTML) = "" Then
                DisplayUnBlanckFile Me.BrowserDeal
            Else
                '-> Afficher la documentation DEAL Informatique
                If InStr(1, aMenu.FichierHTML, "#") <> 0 Then
                    strTmp = Mid$(aMenu.FichierHTML, 1, InStr(1, aMenu.FichierHTML, "#") - 1)
                Else
                    strTmp = aMenu.FichierHTML
                End If
                If Dir$(PathDocDeal & Format(CurCodeLangue, "00") & "\" & strTmp, vbNormal) <> "" Then
                    BrowserDeal.Navigate PathDocDeal & Format(CurCodeLangue, "00") & "\" & aMenu.FichierHTML
                Else
                    DisplayUnBlanckFile Me.BrowserDeal
                End If
                If Dir$(PathDocClient & Format(CurCodeLangue, "00") & "\" & strTmp, vbNormal) <> "" Then
                    Me.BrowserClient.Navigate PathDocClient & Format(CurCodeLangue, "00") & "\" & aMenu.FichierHTML
                    '-> Afficher la doc client
                    DisplayDocClient = True
                Else
                    DisplayDocClient = False
                End If
            End If
        Else
            '-> On affiche un chapitre
            DisplayBlanckFile Me.BrowserDeal
        End If
    End If
        
End If

'-> Lancer le resize
Form_Resize




End Sub
