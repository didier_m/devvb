VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   1500
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4635
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1500
   ScaleWidth      =   4635
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Convertir"
      Height          =   375
      Left            =   2160
      TabIndex        =   4
      Top             =   1080
      Width           =   2415
   End
   Begin VB.TextBox Text2 
      Enabled         =   0   'False
      Height          =   285
      Left            =   2160
      TabIndex        =   3
      Top             =   600
      Width           =   2415
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   2160
      TabIndex        =   1
      Top             =   240
      Width           =   2415
   End
   Begin VB.Label Label2 
      Caption         =   "Valeur D�cimale :"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   "Valeur Hexad�cimale :"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   1815
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

Dim ValToAnalyse As String
Dim i As Integer, j As Integer
Dim Total As Double
Dim aChar As String
Dim aCoef As Long
Dim aValue As Long

If Trim(Me.Text1) = "" Then Exit Sub

ValToAnalyse = Trim(Me.Text1)

For i = Len(ValToAnalyse) To 1 Step -1
    '-> Appliquer une base 16
    aCoef = 16
    '-> Lire le caract�re
    aChar = UCase$(Mid$(ValToAnalyse, i, 1))
    If IsNumeric(aChar) Then
        aValue = CInt(aChar)
    Else
        '-> On a une lettre
        Select Case aChar
            Case "A"
                aValue = 10
            Case "B"
                aValue = 11
            Case "C"
                aValue = 12
            Case "D"
                aValue = 13
            Case "E"
                aValue = 14
            Case "F"
                aValue = 15
        End Select
    End If
        
    '-> Appliquer le coef selon le rang
    If i = Len(ValToAnalyse) Then
        Total = aValue
    Else
        '-> Faire un d�calage de BIT
        For j = 2 To (Len(ValToAnalyse) - i)
            aCoef = aCoef * 16
        Next
        Total = Total + (aValue * aCoef)
    End If
Next

Me.Text2.Text = Total


End Sub
