VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmCreateParFile 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Export vers Excel"
   ClientHeight    =   7875
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7170
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7875
   ScaleWidth      =   7170
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2520
      Top             =   7200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCreateParFile.frx":0000
            Key             =   "Excel"
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   5280
      Top             =   6960
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Command1 
      Enabled         =   0   'False
      Height          =   855
      Left            =   6000
      Picture         =   "frmCreateParFile.frx":015A
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   6960
      Width           =   975
   End
   Begin VB.Frame Frame3 
      Caption         =   "Choix du classeur de r�f�rence : "
      Height          =   3135
      Left            =   0
      TabIndex        =   13
      Top             =   3720
      Width           =   7095
      Begin MSComctlLib.TreeView TreeView1 
         Height          =   1455
         Left            =   120
         TabIndex        =   14
         Top             =   960
         Width           =   6855
         _ExtentX        =   12091
         _ExtentY        =   2566
         _Version        =   393217
         LabelEdit       =   1
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin VB.Label Label6 
         Caption         =   "R�pertoire de destination : "
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   2520
         Width           =   2055
      End
      Begin VB.Label lblDestRef 
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   2760
         Width           =   6855
      End
      Begin VB.Label lblPathRef 
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   600
         Width           =   6855
      End
      Begin VB.Label Label3 
         Caption         =   "R�pertoire de r�f�rence : "
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Width           =   1815
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Options : "
      Height          =   2055
      Left            =   0
      TabIndex        =   5
      Top             =   1560
      Width           =   7095
      Begin VB.CheckBox Check4 
         Caption         =   "Demander la confirmation si le fichier existe d�ja"
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   1080
         Width           =   4935
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Ouvrir Excel � la fin du traitement"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   1440
         Width           =   3255
      End
      Begin VB.TextBox strFileName 
         Height          =   285
         Left            =   2760
         TabIndex        =   10
         Top             =   720
         Width           =   3855
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Enregistrer : "
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   720
         Width           =   1695
      End
      Begin VB.TextBox strConfirm 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   4800
         MaxLength       =   15
         PasswordChar    =   "*"
         TabIndex        =   8
         Top             =   360
         Width           =   1815
      End
      Begin VB.TextBox strPasse 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2760
         MaxLength       =   15
         PasswordChar    =   "*"
         TabIndex        =   7
         Top             =   360
         Width           =   1815
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Prot�ger par un mot de passe :"
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   2535
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Choix du fichier DATA : "
      Height          =   1455
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7095
      Begin VB.TextBox strData_Source 
         Height          =   285
         Left            =   4200
         TabIndex        =   18
         Top             =   1080
         Width           =   2415
      End
      Begin VB.TextBox strOnglet 
         Height          =   285
         Left            =   4200
         TabIndex        =   4
         Top             =   720
         Width           =   2415
      End
      Begin VB.TextBox strDataFile 
         Height          =   285
         Left            =   1560
         TabIndex        =   2
         Top             =   360
         Width           =   5055
      End
      Begin VB.Label Label4 
         Caption         =   "Source de donn�es :"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   1080
         Width           =   1935
      End
      Begin VB.Label Label2 
         Caption         =   "Onglet : "
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   720
         Width           =   1215
      End
      Begin VB.Image Image1 
         Height          =   240
         Left            =   6720
         Picture         =   "frmCreateParFile.frx":1024
         Top             =   360
         Width           =   240
      End
      Begin VB.Label Label1 
         Caption         =   "Fichier � int�grer :"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   1455
      End
   End
End
Attribute VB_Name = "frmCreateParFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Check1_Click()

If Me.Check1.Value = 1 Then
    Me.strPasse.Enabled = True
    Me.strConfirm.Enabled = True
Else
    Me.strPasse.Enabled = False
    Me.strConfirm.Enabled = False
End If

Me.strPasse = ""
Me.strConfirm = ""

End Sub

Private Sub Check2_Click()

If Me.Check2.Value = 1 Then
    Me.strFileName.Enabled = True
Else
    Me.strFileName.Enabled = False
End If
Me.strFileName = ""

End Sub

Private Sub Command1_Click()

Dim ToSave As Boolean
Dim Pwd As Boolean
Dim Passe As String
Dim bOpen As Boolean
Dim bConfirm As Boolean
Dim FileName As String
Dim TempFileName As String
Dim hdlFile As Integer
Dim Retour As String

'-> V�rifier si on a saisi le fichier de data
If Trim(Me.strDataFile) = "" Then
    MsgBox "Veuillez saisir le fichier de donn�es", vbCritical + vbOKOnly, "Erreur"
    Me.strDataFile.SetFocus
    Exit Sub
End If

'-> V�rifier que le fichier saisi existe bien
If Dir$(Me.strDataFile, vbNormal) = "" Then
    MsgBox "Nom ou emplacement de fichier incorrect", vbCritical + vbOKOnly, "Erreur"
    Me.strDataFile.SetFocus
    Exit Sub
End If

'-> Nom de l'onglet
If Trim(Me.strOnglet) = "" Then
    MsgBox "Veuillez saisir le nom de l'onglet", vbCritical + vbOKOnly, "Erreur"
    Me.strOnglet.SetFocus
    Exit Sub
End If

'-> Source de donn�es
If Trim(Me.strData_Source) = "" Then
    MsgBox "Veuillez saisir le nom de la source de donn�es", vbCritical + vbOKOnly, "Erreur"
    Me.strData_Source.SetFocus
    Exit Sub
End If

'-> Mot de passe
If Me.Check1.Value = 1 Then
    '-> V�rif du mot de passe
    If Trim(Me.strPasse) = "" Then
        MsgBox "Veuillez saisir le mot de passe", vbCritical + vbOKOnly, "Erreur"
        Me.strPasse.SetFocus
        Exit Sub
    End If
    '-> Verif de la confirmation
    If Trim(Me.strConfirm) = "" Then
        MsgBox "Veuillez saisir la confirmation du mot de passe", vbCritical + vbOKOnly, "Erreur"
        Me.strConfirm.SetFocus
        Exit Sub
    End If
    '-> Verif si �galit� entre les deux
    If Me.strConfirm <> Me.strPasse Then
        MsgBox "Erreur : la confirmation n'est pas �gale au mot de passe", vbCritical + vbOKOnly, "Erreur"
        Me.strConfirm.SetFocus
        Exit Sub
    End If
    Pwd = True
    Passe = Me.strPasse
Else
    Pwd = False
    Passe = ""
End If

'-> Si on doit enregistrer
If Me.Check2.Value = 1 Then
    If Trim(Me.strFileName) = "" Then
        MsgBox "Veuillez saisir le nom du fichier de destination", vbCritical + vbOKOnly, "Erreur"
        Me.strFileName.SetFocus
        Exit Sub
    End If
    ToSave = True
    FileName = Trim(Me.strFileName)
Else
    ToSave = False
End If

'-> Demande de confirmation
If Me.Check4.Value = 1 Then
    bConfirm = True
Else
    bConfirm = False
End If

'-> Si on laisse excel ouvert
If Me.Check3.Value = 1 Then
    bOpen = True
Else
    bOpen = False
End If


'-> G�n�rer le fichier par
TempFileName = GetTempFileNameVB("PAR")
hdlFile = FreeFile
Open TempFileName For Output As #hdlFile

Print #hdlFile, "[GENERAL]"
Print #hdlFile, "IDENT=DEAL"
Print #hdlFile, "PROGICIEL=PROLUC"
Print #hdlFile, "REFERENCE=" & Me.TreeView1.SelectedItem.Text
Print #hdlFile, "DESTINATION=" & FileName
Print #hdlFile, "NATURE="
Print #hdlFile, "PASSE=" & Passe
If ToSave Then
    Print #hdlFile, "SAVE=OUI"
Else
    Print #hdlFile, "SAVE=NON"
End If
If bOpen Then
    Print #hdlFile, "OPEN=OUI"
Else
    Print #hdlFile, "OPEN=NON"
End If
If bConfirm Then
    Print #hdlFile, "CONFIRM=OUI"
Else
    Print #hdlFile, "CONFIRM=NON"
End If
Print #hdlFile, "STOP=NON"

Print #hdlFile, "[MSO]"
Print #hdlFile, "SOURCEXLS=" & Me.lblPathRef
Print #hdlFile, "DESTINATIONXLS=" & Me.lblDestRef

Print #hdlFile, "[CHARGEMENT_1]"
Print #hdlFile, "DATA=" & Trim(Me.strDataFile)
Print #hdlFile, "ONGLET=" & Trim(Me.strOnglet)
Print #hdlFile, "NAME=" & Trim(Me.strData_Source)
Print #hdlFile, "KILL=NON"
Print #hdlFile, "EXCEL97=OUI"

'-> Fermer le fichier
Close #hdlFile

'-> D�charger la feuille
Unload Me

'-> Cr�er un fichier de retour
Retour = GetTempFileNameVB("RTR")

'-> Setting des variables globales
ParFile = TempFileName
ReturnFile = Retour

'-> Lancer la cr�ation du classeur EXCEL
GenerateTableauXls

End Sub

Public Sub Init()

'---> anlayse le r�peroitre de r�f�rence et affiche tous les classeurs

Dim FileName As String

FileName = Dir$(Me.lblPathRef & "*.xls")
Do While FileName <> ""
    Me.TreeView1.Nodes.Add , , , FileName, "Excel"
    FileName = Dir
Loop

End Sub

Private Sub Form_Load()

'---> analyse de la ligne de commande
SourceXLS = Entry(1, Command$, "@")
DestinationXls = Entry(2, Command$, "@")

'-> Setting des datas
frmCreateParFile.lblPathRef = SourceXLS
frmCreateParFile.lblDestRef = DestinationXls
frmCreateParFile.strData_Source = "Source_Donn�es"
frmCreateParFile.strOnglet = "DATA"
frmCreateParFile.Check2.Value = 1
frmCreateParFile.Check3.Value = 1

Call Init

End Sub

Private Sub Image1_DblClick()

On Error GoTo GestError

Me.CommonDialog1.DialogTitle = "S�lection d'un fichier texte"
Me.CommonDialog1.FileName = "*.txt"
Me.CommonDialog1.Filter = "Texte (*.txt)|*.txt"
Me.CommonDialog1.CancelError = True
Me.CommonDialog1.Flags = cdlOFNFileMustExist
Me.CommonDialog1.ShowOpen

Me.strDataFile = Me.CommonDialog1.FileName

Exit Sub

GestError:

    Exit Sub


End Sub

Private Sub TreeView1_Click()

If Me.TreeView1.Nodes.Count = 0 Then Exit Sub

If Me.TreeView1.SelectedItem Is Nothing Then Exit Sub

Me.Command1.Enabled = True

End Sub
