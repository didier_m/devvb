Attribute VB_Name = "fctMessagerie"
Option Explicit

'---> Ce module permet de g�rer l'envoie d'un message sur OutLook 97

'-> Variables r�utilis�es pour compatibilit� avec Classe Messprog
Public ClientPath As String '-> Path des fichiers langues du client
Public RessourcePath As String '-> Path des fichiers Langues de DEAL

Dim CodeLangue As Integer '-> Code langue des messprog
Dim CarnetAdresse As String '-> Carnet d'adresse � utiliser
Dim NatureMessage As Integer '-> 1 pour messagerie , 2 pour Internet
Dim Destinataire As String '-> Codes des destinataires s�parateur |
Dim Emetteur As String '-> Code de l'emetteur
Dim Copies As String '-> Code des personnes � envoyer pour copie s�parateur |
Dim MailObject As String '-> Object du mail
Dim Message As String '-> Message � envoyer
Dim JoinFiles As String '-> Liste des fichiers joins s�parateur |

Public Sub Main()

'---> Point d'entr�e du prog

'-> Lancer l'analyse du fichier ASCII
GetASCIIFile App.Path & "\Test.txt"

End Sub

Public Sub GetASCIIFile(ByVal FichierASCII As String)

'---> Cette fonction r�cup�re le fichier ASCII et l'analyse
Dim aLb As Libelle '-> Gestion des messprog
Dim hdlFile As Integer
Dim Res As Long
Dim lpBuffer As String
Dim lpKey As String
Dim Ligne As String
Dim IsMessage As Boolean
Dim i As Integer

'-> Il faut dans un premier temps r�cup�rer le code langue de l'op�rateur
lpBuffer = Space$(255)
Res = GetPrivateProfileString("PARAM", "CODELANGUE", "", lpBuffer, Len(lpBuffer), FichierASCII)
If Res = 0 Then
    '-> On n'a pas trouv� le code langue : fran�ais par d�faut
    CodeLangue = 1
Else
    lpBuffer = Mid$(lpBuffer, 1, Res)
    '-> Tester si code langyue correct
    If Not IsNumeric(lpBuffer) Then
        CodeLangue = 1
    Else
        CodeLangue = CInt(lpBuffer)
    End If
End If

'-> Initialiser le path des messprog
Call GetPath

'-> charger les messprog
Call CreateMessProg("DEALWEB-" & Format(CodeLangue, "00") & ".LNG")

'-> Pointer sur la classe libelle Message
Set aLb = Libelles("MESSAGES")
    
'-> R�cup�ration  du carnet d'adresses
lpBuffer = Space$(300)
Res = GetPrivateProfileString("PARAM", "CARNET", "", lpBuffer, Len(lpBuffer), FichierASCII)
If Res = 0 Then
    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetCaption(15)
    End
End If

'-> R�cup�ration du type de messagerie
lpBuffer = Space$(300)
Res = GetPrivateProfileString("PARAM", "TYPE", "", lpBuffer, Len(lpBuffer), FichierASCII)
If Res = 0 Then
    MsgBox aLb.GetCaption(5), vbCritical + vbOKOnly, aLb.GetCaption(15)
    End
End If
lpBuffer = UCase$(Mid$(lpBuffer, 1, Res))

Select Case lpBuffer
    Case "MESSAGERIE"
        NatureMessage = 1
    Case "INTERNET"
        NatureMessage = 2
    Case Else
        MsgBox aLb.GetCaption(6), vbCritical + vbOKOnly, aLb.GetCaption(15)
        End
End Select

'-> R�cup�ration de l'emetteur
lpBuffer = Space$(300)
Res = GetPrivateProfileString("PARAM", "FROM", "", lpBuffer, Len(lpBuffer), FichierASCII)
If Res = 0 Then
    MsgBox aLb.GetCaption(9), vbCritical + vbOKOnly, aLb.GetCaption(15)
    End
End If
Emetteur = Mid$(lpBuffer, 1, Res)

'-> R�cup�ration du destinataire
lpBuffer = Space$(300)
Res = GetPrivateProfileString("PARAM", "TO", "", lpBuffer, Len(lpBuffer), FichierASCII)
If Res = 0 Then
    MsgBox aLb.GetCaption(7), vbCritical + vbOKOnly, aLb.GetCaption(15)
    End
End If
Destinataire = Mid$(lpBuffer, 1, Res)

'-> R�cup�ration des copies
lpBuffer = Space$(300)
Res = GetPrivateProfileString("PARAM", "CC", "", lpBuffer, Len(lpBuffer), FichierASCII)
If Res <> 0 Then
    Copies = Mid$(lpBuffer, 1, Res)
End If

'-> R�cup�ration de l'object
lpBuffer = Space$(300)
Res = GetPrivateProfileString("PARAM", "OBJECT", "", lpBuffer, Len(lpBuffer), FichierASCII)
If Res <> 0 Then
    MailObject = Mid$(lpBuffer, 1, Res)
End If

'-> Ouverture du message
hdlFile = FreeFile
Open FichierASCII For Input As #hdlFile

'-> R�cup�ration du contenu du message
Do While Not EOF(hdlFile)
    Line Input #hdlFile, Ligne
    If IsMessage Then
        '-> Tester si balise de fermeture de message
        If UCase$(Ligne) = "[\MESSAGE]" Then Exit Do
        If Message = "" Then
            Message = Ligne
        Else
            Message = Message & Chr(13) + Chr(10) & Ligne
        End If
    Else
        If UCase$(Ligne) = "[MESSAGE]" Then IsMessage = True
    End If
NextLigne:

Loop 'Boucle d'analyse du fichier pour get du message

'-> Fermer le fichier
Close #hdlFile

'-> R�cup�rer la liste des fichiers joints
lpBuffer = Space$(32765)
Res = GetPrivateProfileSection("FILES", lpBuffer, Len(lpBuffer), FichierASCII)
If Res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Res)
    '-> Chargement de la liste des fichiers joints
    For i = 1 To NumEntries(lpBuffer, Chr(0))
        lpKey = Entry(i, lpBuffer, Chr(0))
        If lpKey <> "" Then
            If JoinFiles = "" Then
                JoinFiles = Entry(2, lpKey, "=")
            Else
                JoinFiles = JoinFiles & "|" & Entry(2, lpKey, "=")
            End If
        End If
    Next 'Pour tous les fichiers joints
End If


'-> analyse effectu�e : lancer la cration du message
Call SendOutLook

End Sub

Private Sub SendOutLook()

'---> Cette fonction envoie un message sur la messagerie sp�cifi�e

Dim aLb As Libelle
Dim OutLookObj As Outlook.Application
Dim aMail As MailItem
Dim aRecp As Recipient
Dim i As Integer
Dim Resolve As Boolean

'-> Cr�er un lien vers OutLook
Set OutLookObj = New Outlook.Application

'-> Cr�er un nouveua message
Set aMail = OutLookObj.CreateItem(olMailItem)

If NatureMessage = 1 Then
    
    For i = 1 To NumEntries(Destinataire, "|")
        '-> Ajouter le destinataire
        Set aRecp = aMail.Recipients.Add(Entry(i, Destinataire, "|"))
        '-> V�rifier s'il existe
        Resolve = aRecp.Resolve
    Next 'Pour tous les destinataires
    
    
    
    

'-> Il faut v�rifier si tous les destinataires sont OK

'-> V�rifier si les copies sont bonnes

End If

'-> Cr�er un nouveau message

'-> Setting des propri�t�s

'-> Joindre les fichiers

'-> Envoyer le message



End Sub

Public Function GetPath() As String

'---> Cette fonction renvois le path de \MQT en fonction de l'endroit d'ou _
est appell�e le programme ( utilis� pour faire la diff�rence en DVL et EXE)

'-> La diff�rence se fait par : Si on trouve le fichier VBP : on est en DVL

Dim Pos As Integer
Dim BasePath As String

If Dir$(App.Path & "\*.vbp") = "" Then
    '-> On est en mode exe
    Pos = InStrRev(App.Path, "\")
    BasePath = Mid$(App.Path, 1, Pos)
    RessourcePath = App.Path & "Mqt\"
Else
    '-> On est en mode interpr�t�
    RessourcePath = "E:\Erp\DealPro\Deal\V51\EmilieGui\Mqt\"
End If

GetPath = RessourcePath

End Function


