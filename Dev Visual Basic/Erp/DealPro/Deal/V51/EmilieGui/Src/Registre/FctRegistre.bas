Attribute VB_Name = "fctRegistre"
Dim Root As String
Dim Cle As String
Dim Value As String
Dim Setting As String
Dim Action As String
Dim IniPath As String
Dim NomSection As String
Dim Param As String

Sub Main()


Dim ListeCle As String
Dim Res As Long
Dim LpBuffer As String
Dim i As Integer
Dim LigneCommande As String

On Error GoTo GestError

'-> R�cup�rer la ligne de commande
LigneCommande = Command$

'-> Tester le privil�ge administrateur
ListeCle = Space$(500)
Res = GetSystemDirectory(ListeCle, Len(ListeCle))
ListeCle = Mid$(ListeCle, 1, Res) & "\drivers\etc\"

'-> V�rifier si le r�pertoire existe
If Dir$(ListeCle, vbDirectory) = "" Then
    '-> Ne rien faire WIN95 ou WIN98
Else
    '-> Essayer de cr�er un fichier dans le r�pertoire syst�me
    Res = CreateFile(ListeCle & "TestReg.deal", GENERIC_WRITE&, FILE_SHARE_WRITE&, 0, CREATE_ALWAYS&, FILE_ATTRIBUTE_NORMAL&, GENERIC_WRITE&)
    If Res = -1 Then
        '-> Cr�ation impossible : n'a pas les privil�ges n�cessaires
        If UCase$(LigneCommande) <> "/S" Then MsgBox "Mise � jour des polices impossible.Cette op�ration n�cessite un privil�ge d'administrateur sur la machine.", vbCritical + vbOKOnly, "Erreur fatale"
        '-> Quitter
        End
    End If
    '-> Fermer le fichier
    Res = CloseHandle&(Res)
    '-> Supprimer le fichier que l'on vient de cr�er
    Res = DeleteFile&(ListeCle & "TestReg.deal")
End If


If InStr(1, UCase$(App.Path), "D:\ERP\DEALPRO\DEAL\V51\EMILIEGUI\SRC") <> 0 Then
    IniPath = "D:\ERP\DEALPRO\DEAL\V51\EMILIEGUI\MQT"
Else
    IniPath = GetPath(App.Path)
End If

'-> Construire le r�pertoire
IniPath = IniPath & "\Maj_Registre.ini"

'-> Quitter si on ne trouve pas le fichier ini
If Dir$(IniPath) = "" Then End

'-> R�cup�ration de la liste des cl�s
ListeCle = Space$(500)
Res = GetPrivateProfileString(vbNullString, "", "", ListeCle, Len(ListeCle), IniPath)
If Res = 0 Then End
ListeCle = Mid$(ListeCle, 1, Res)

'-> Analyse des cl�s
For i = 1 To NumEntries(ListeCle, Chr(0)) - 1
    NomSection = Entry(i, ListeCle, Chr(0))
    '-> Cle ROOT
    GetValue NomSection, "ROOT", Root
    '-> Cle CLE
    GetValue NomSection, "CLE", Cle
    '-> Cle Value
    GetValue NomSection, "VALUE", Value
    '-> Cle SETTING
    GetValue NomSection, "SETTING", Setting
    '-> Cle Action
    GetValue NomSection, "ACTION", Action
    '-> Les valeurs sont retenues : Traitement des cl�s
    Exec
Next


If UCase$(LigneCommande) <> "/S" Then _
    MsgBox "Mise � jour des polices effectu�e.", vbInformation + vbOKOnly, "Op�ration termin�e"
    
End

Exit Sub

GestError:

    MsgBox "Erreur impr�vue : traitement interrompu.", vbCritical + vbOKOnly, "Erreur fatale"
    End



End Sub

Sub Exec()

'---> Cette proc�dure analyse le registre � la recherche d'une cl� :

Dim i As Integer
Dim ListeSubKey As String
Dim SubKey1 As String
Dim Subkey2 As String
Dim CleToGet As String

'-> Faire quelques v�rifications
If Trim(Root) = "" Or Trim(Cle) = "" Or Trim(Action) = "" Then End

'-> V�rifier s'il y a une �num�ration
If InStr(1, UCase$(Cle), "%%ENUMERATION%%") <> 0 Then
    '-> D�termination des sous cl�s
    '-> Il faut cr�er la liste des sous cl�s
    i = InStr(1, UCase$(Cle), "%%ENUMERATION%%")
    SubKey1 = Mid$(Cle, 1, i - 1)
    ListeSubKey = GetListeKey(SubKey1)
    '-> Faire un remplacement de la valeur
    For i = 1 To NumEntries(ListeSubKey, "|")
        '-> On a r�cup�rer la cl�
        CleToGet = Replace(UCase$(Cle), "%%ENUMERATION%%", Entry(i, ListeSubKey, "|"))
        '-> Traiter l'action
        Select Case UCase$(Action)
            Case "CREATE"
                DonnerValeur CleToGet, Value, Setting
            Case "DELETE"
                DelKey CleToGet
        End Select
    Next 'pour toutes les sous cl�s
Else
    '-> S'il n'y a pas d'�num�ration
    Select Case UCase$(Action)
        Case "CREATE"
            DonnerValeur Cle, Value, Setting
        Case "DELETE"
            DelKey Cle
        Case "SAVE"
            'SaveCle Cle
    End Select
End If




End Sub
Private Function SaveCle(Chemin As String)

Dim Res As Long
Dim TempFile As String
Dim hdlFile As Integer
Dim Lng As Long
Dim aSA As SECURITY_ATTRIBUTES

'-> Obtenir un nom de fichier temporaire
TempFile = Entry(1, GetTempFileNameVB("REG"), ".")

'-> Ouvrir la cle
Res = RegOpenKeyEx(GetRoot, Chemin, 0, KEY_ALL_ACCESS, Lng)

aSA.nLength = Len(aSA)
aSA.lpSecurityDescriptor = 0
aSA.bInheritHandle = 0

'-> Sauvegarder la cl�


Res = RegSaveKey(Lng, TempFile, aSA)

'-> Ouvrir le fichier pour rajouter les donn�es
hdlFile = FreeFile
Open TempFile For Append As #hdlFile
Print #hdlFile, Chr(13) & Chr(10)
Print #hdlFile, "Mise � jour du registre le : ---->  " & Now
Close #hdlFile

End Function


Private Function DonnerValeur(Chemin As String, Valeur As String, Donnee As String)
    RegCreateKey GetRoot, Chemin, Lng
    RegSetValueEx Lng, Valeur, 0&, 1, Donnee, Len(Donnee) + 1
End Function
Private Function DelKey(Chemin As String)

Dim ListeCle As String
Dim ListeCle2 As String
Dim Racine As String
Dim CleToDel As String
Dim i As Integer

'---> Il faut faire la liste des sous cl�s existantes pour une cl� donn�e (On ne peut _
supprimer une cl� sous NT s'il y a des sous cl�s)

ListeCle = GetListeKey(Chemin)
If ListeCle = "" Then
Else
    CleToDel = Chemin & Entry(1, GetListeKey(Chemin), "|")
    Do While GetListeKey(Chemin) <> ""
        '-> Essayer de supprimer la cle
        Res = RegDeleteKey(GetRoot, CleToDel)
        If Res = 0 Then
            '-> Suppression r�ussie
            CleToDel = Chemin & Entry(1, GetListeKey(Chemin), "|")
        Else
            '-> la Suppression n' a pas r�ussie r�cup�rer la liste des sous cl�s
            ListeCle2 = GetListeKey(CleToDel)
            If ListeCle2 <> "" Then CleToDel = CleToDel & "\" & Entry(1, ListeCle2, "|") & "\"
        End If
    Loop
End If

'-> Supprimer la cle
Res = RegDeleteKey(GetRoot, Chemin)


    
End Function
Private Function GetListeKey(ByVal KeyToCheck As String) As String

'--> Cette fonction renvoi la liste des sous - cl�s avec un s�parateur "|"

'-> Variable Globale
Dim lngRoot As Long
Dim hdlKey As Long
Dim Res As Long
Dim i As Integer
Dim ListeCle As String

'-> Variables pour r�cup�ration de la liste des souscl�s
Dim LpBuffer As String
Dim lpClass As String
Dim lpcbClass As Long
Dim lpcSubKeys As Long
Dim lpcbMaxSubKeyLen As Long
Dim lpcbMaxClassLen As Long
Dim lpcValues As Long
Dim lpcbMaxValueNameLen As Long
Dim lpcbMaxValueLen As Long
Dim lpcbSecurityDescriptor As Long
Dim lpftLastWriteTime As FILETIME

'-> Variables de r�cup�ration

Dim lpName As String
Dim lpcbName As Long
Dim lpClass2 As String
Dim lpcbClass2 As Long
Dim lpftLastWriteTime2 As FILETIME

'-> Initialisation de la valeur de retour
ListeCle = ""

'-> Recup�ration du premier niveau
lngRoot = GetRoot

'-> Ouverture de la cl� sp�cifi�e
Res = RegOpenKeyEx(lngRoot, KeyToCheck, 0, KEY_ALL_ACCESS&, hdlKey)
If Res <> 0 Then Exit Function

'-> Enum�ration des sous cl�
lpClass = Space$(2000)
lpcbClass = Len(lpClass)
Res = RegQueryInfoKey(hdlKey, lpClass, lpcbClass, 0, lpcSubKeys, lpcbMaxSubKeyLen, _
                      lpcbMaxClassLen, lpcValues, lpcbMaxValueNameLen, lpcbMaxValueLen, _
                      lpcbSecurityDescriptor, lpftLastWriteTime)
If Res = 0 Then
    '-> Analyse de l'ensemble des sous cl�s
    For i = 0 To lpcSubKeys - 1
        '-> initialiser les variables
        lpName = Space$(lpcbMaxSubKeyLen + 1)
        lpClass2 = Space$(lpcbMaxClassLen + 1)
        lpcbName = lpcbMaxSubKeyLen + 1
        lpcbClass2 = lpcbMaxClassLen + 1
        '-> R�cup�ration des informations
        Res = RegEnumKeyEx(hdlKey, i, lpName, lpcbName, 0, lpClass2, lpcbClass2, lpftLastWriteTime2)
        '-> R�cup�ration du nom
        lpName = Mid$(lpName, 1, lpcbName)
        '-> Ajout � la liste de retour
        If ListeCle = "" Then
            ListeCle = lpName
        Else
            ListeCle = ListeCle & "|" & lpName
        End If
    Next
End If

GetListeKey = ListeCle

End Function

Private Function GetRoot() As Long

Select Case UCase$(Root)
    Case "HKEY_CLASSES_ROOT"
        GetRoot = HKEY_CLASSES_ROOT
    Case "HKEY_CURRENT_USER"
        GetRoot = HKEY_CURRENT_USER
    Case "HKEY_LOCAL_MACHINE"
        GetRoot = HKEY_LOCAL_MACHINE
    Case "HKEY_USERS"
        GetRoot = HKEY_USERS
    Case "HKEY_DYN_DATA"
        GetRoot = HKEY_DYN_DATA
End Select



End Function



Private Sub GetValue(NomSection As String, NomCle As String, Buffer As String)

Dim LpBuffer As String

LpBuffer = Space$(500)
Res = GetPrivateProfileString(NomSection, NomCle, "", LpBuffer, Len(LpBuffer), IniPath)
If Res <> 0 Then Buffer = Mid$(LpBuffer, 1, Res)
    
End Sub
