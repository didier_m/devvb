Attribute VB_Name = "FctTbXls"
'-> Propath des Tableaux de Bords
Public SourceXLS As String '-> Emplacement des classeurs R�f�rence
Public SourceData As String '-> Emplacement du fichier *.par
Public XlsTb As String '-> Emplacement de destination des classeurs
Public XlsData As String '-> Emplacement de destination des donn�es

'-> Variable de r�cup�ration des propri�t�s des tableaux
Public Type_Entite As String
Public Entite As String
Public Titre As String
Public Mois As String
Public Dernier_Jour As String
Public Source As String

'-> Variable de localisation du fichier Ini
Public IniPath As String

'-> Variable de la ligne de commande
Public Applicatif As String
Public ParFile As String

Sub Main()

Dim TempFileName As String
Dim Res As Long
Dim lpBuffer As String
Dim LigneCommande As String


'-> R�cup�ration des donn�es de la ligne de commande
LigneCommande = Command$
If NumEntries(LigneCommande, "|") <> 2 Then
    MsgBox "Ligne de commande invalide : " & Chr(13) & ligncommande & Chr(13) & "Celle-ci doit �tre : " & Chr(13) & "[Applicatif] | [Emplacement du fichier .PAR]", vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

Applicatif = Entry(1, LigneCommande, "|")
ParFile = Entry(2, LigneCommande, "|")

'-> V�rifier l'existance du fichier PAR
If Dir$(ParFile) = "" Then
    MsgBox "Impossible de trouver le fichier PAR : " & Chr(13) & ParFile, vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

'-> Cr�ation du proptah du fichier ini d'origine
If InStr(1, UCase$(App.Path), "EMILIEGUI\SRC") <> 0 Then
    IniPath = "D:\Erp\Turbo"
Else
    CreatePath IniPath
End If

'-> Obtenir un fichier temporaire
TempFileName = GetTempFileNameVB("INI")

'-> Copier le fichier dans le r�pertoire temporaire
CopyFile IniPath & "\TBXls.ini", TempFileName, 0

'-> Analyse du fichier TBXLS.Ini pour r�cup�ration des valeurs de param�trage

'-> SOURCEXLS
lpBuffer = Space$(2000)
Res = GetPrivateProfileString(Applicatif, "SourceXls", "", lpBuffer, Len(lpBuffer), TempFileName)
If Res = 0 Then
    MsgBox "Impossible de trouver la source des fichiers Excel dans le fichier : " & Chr(13) & TempFileName, vbCritical + vbOKOnly, "Erreur fatale"
    End
End If
lpBuffer = Mid$(lpBuffer, 1, Res)
SourceXLS = lpBuffer

'-> SOURCEDATA
lpBuffer = Space$(2000)
Res = GetPrivateProfileString(Applicatif, "SourceData", "", lpBuffer, Len(lpBuffer), TempFileName)
If Res = 0 Then
    MsgBox "Impossible de trouver la source des donn�es dans le fichier : " & Chr(13) & TempFileName, vbCritical + vbOKOnly, "Erreur fatale"
    End
End If
lpBuffer = Mid$(lpBuffer, 1, Res)
SourceData = lpBuffer

'-> XLSTB
lpBuffer = Space$(2000)
Res = GetPrivateProfileString(Applicatif, "XlsTb", "", lpBuffer, Len(lpBuffer), TempFileName)
If Res = 0 Then
    MsgBox "Impossible de trouver la destination des fichiers Excel dans le fichier : " & Chr(13) & TempFileName, vbCritical + vbOKOnly, "Erreur fatale"
    End
End If
lpBuffer = Mid$(lpBuffer, 1, Res)
XlsTb = lpBuffer

'-> XLSDATA
lpBuffer = Space$(2000)
Res = GetPrivateProfileString(Applicatif, "XlsData", "", lpBuffer, Len(lpBuffer), TempFileName)
If Res = 0 Then
    MsgBox "Impossible de trouver la destination des fichiers Excel dans le fichier : " & Chr(13) & TempFileName, vbCritical + vbOKOnly, "Erreur fatale"
    End
End If
lpBuffer = Mid$(lpBuffer, 1, Res)
XlsTb = lpBuffer


'-> Obtenir un fichier temporaire
TempFileName = GetTempFileNameVB("INI")

'-> Copier le fichier dans le r�pertoire temporaire
CopyFile ParFile, TempFileName, 0

'-> Analyse du fichier PAR pour trouver la section de param�trage du classeur

'-> TYPE_ENTITE
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("PRESENTATION", "Type-Entite", "", lpBuffer, Len(lpBuffer), TempFileName)
lpBuffer = Mid$(lpBuffer, 1, Res)
Type_Entite = lpBuffer

'-> ENTITE
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("PRESENTATION", "Entite", "", lpBuffer, Len(lpBuffer), TempFileName)
lpBuffer = Mid$(lpBuffer, 1, Res)
Entite = lpBuffer

'-> TITRE
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("PRESENTATION", "Titre", "", lpBuffer, Len(lpBuffer), TempFileName)
lpBuffer = Mid$(lpBuffer, 1, Res)
Titre = lpBuffer

'-> MOIS
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("PRESENTATION", "Mois", "", lpBuffer, Len(lpBuffer), TempFileName)
lpBuffer = Mid$(lpBuffer, 1, Res)
Mois = lpBuffer

'-> DERNIER-JOUR
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("PRESENTATION", "Dernier-jour", "", lpBuffer, Len(lpBuffer), TempFileName)
lpBuffer = Mid$(lpBuffer, 1, Res)
Dernier_Jour = lpBuffer

'-> SOURCE
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("PRESENTATION", "Source", "", lpBuffer, Len(lpBuffer), TempFileName)
lpBuffer = Mid$(lpBuffer, 1, Res)
Source = lpBuffer


'-> Affichage de la feuille de d�marrage
frmTempo.Show







End Sub
