VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Export des donn�es LUCIE vers MS EXCEL"
   ClientHeight    =   6120
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5265
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6120
   ScaleWidth      =   5265
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Height          =   255
      Index           =   4
      Left            =   120
      ScaleHeight     =   195
      ScaleWidth      =   4995
      TabIndex        =   7
      Top             =   3480
      Width           =   5055
   End
   Begin VB.Frame Frame1 
      Caption         =   "Param�trage : "
      Height          =   3015
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   5175
      Begin MSComDlg.CommonDialog CommonDialog1 
         Left            =   1200
         Top             =   2160
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         CancelError     =   -1  'True
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Lancer le traitement"
         Enabled         =   0   'False
         Height          =   735
         Left            =   3000
         Picture         =   "frmLaunch.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   2160
         Width           =   2055
      End
      Begin VB.Image Image1 
         Height          =   240
         Left            =   1560
         Picture         =   "frmLaunch.frx":014A
         ToolTipText     =   "Double click pour changer le nom du fichier"
         Top             =   360
         Width           =   240
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Height          =   1215
         Left            =   120
         TabIndex        =   6
         Top             =   720
         Width           =   4935
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Fichier � int�grer : "
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   1455
      End
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   "D�but du traitement : "
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   5760
      Width           =   2895
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   5
      Left            =   3120
      TabIndex        =   16
      Top             =   5760
      Width           =   1935
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   4
      Left            =   3120
      TabIndex        =   15
      Top             =   5280
      Width           =   1935
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   3
      Left            =   3120
      TabIndex        =   14
      Top             =   4920
      Width           =   1935
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   2
      Left            =   3120
      TabIndex        =   13
      Top             =   4560
      Width           =   1935
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   1
      Left            =   3120
      TabIndex        =   12
      Top             =   4200
      Width           =   1935
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Index           =   0
      Left            =   3120
      TabIndex        =   11
      Top             =   3840
      Width           =   1935
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Indice Achat par Famile et filiale :"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   3840
      Width           =   2895
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Indice Achat par Fournisseur et Chantier :"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   4920
      Width           =   2895
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Analyse du fichier de travail : "
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   3240
      Width           =   2895
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Indice Achat par Fournisseur et Famille 2 :"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   5280
      Width           =   2895
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Indice Achat par Famile et Chantier :"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   4560
      Width           =   2895
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Indice Achat par Fournisseur :"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   4200
      Width           =   2295
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()

Me.Label9(5).Caption = Now

'-> Lancer le traitement du tableau 1
If Not InitExcel Then
    MsgBox "Erreur sur l'initialisation d'Excel.", vbCritical + vbOKOnly, "Erreur fatale"
    EndJob
End If

'-> Bloquer le bouton de commande
Me.Command1.Enabled = False

'-> Cr�ation des tableaux
CreateIGXLS
CreateIFOGXLS
CreateIFIXLS
CreateIFOFIXLS
CreateFOFAINDXLS

'-> Fermer le classeur
TerminateExcel

'-> Afficher Excel
ExcelApp.Visible = True

'-> Fin du programme
End

End Sub




Private Sub Image1_DblClick()

'---> Afficher la fen�tre de recherche

Dim WorkFile As String
  
On Error GoTo GestError
  
Me.CommonDialog1.FileName = "*.*"
Me.CommonDialog1.Filter = "Tous les fichiers (*.*)|*.*"
Me.CommonDialog1.DialogTitle = "S�lection d'un fichier"
Me.CommonDialog1.ShowOpen

  
Me.Label6.Caption = Me.CommonDialog1.FileName
Me.Command1.Enabled = True

GestError:
    
    Exit Sub

End Sub




Private Sub Label11_Click()

End Sub
