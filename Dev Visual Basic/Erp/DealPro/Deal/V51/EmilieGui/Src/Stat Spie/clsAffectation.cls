VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsAffectation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Cette classe sert � faire le lien entre les diff�rentes valeurs du tableau crois� dynamique


'-> indique le type d'objet d'affectation
'0 -> Famille
'1 -> Filiale
'2 -> Fournisseur
'3 -> Affaire
Public clsType As Integer

'-> Cl�s d'acc�s � la d�finition de cet objet dans sa collection d'origine
Public Key As String

'-> Gestion des cumuls
Public MontantHT As Double
Public MontantRef As Double
