Attribute VB_Name = "FCTsPIE"
Option Explicit

'-> Mod�le objet Excel
Public ExcelApp As Excel.Application
Dim aWorkBook As Workbook
Dim aSheet As Excel.Worksheet

'-> Mod�le objet tableau crois�
Dim Lignes As Collection '-> Liste des lignes du fichier texte
Dim Familles As Collection '-> Liste des Famille dans le fichier
Dim Filiales As Collection '-> Liste des Filliales dans le fichier
Dim Fournisseurs As Collection '-> Liste des fournisseurs dans le fichier
Dim Affaires  As Collection '-> Liste des affaires dans le fichier

'-> Pour cumul
Dim CumulHT As Double
Dim CumulRef As Double

Dim CumulAutreHT As Double
Dim CumulAutreRef As Double

'-> Pour gestion du Tri
Dim strTri As String
Dim strToTri As String

'-> Pour gestion des 90% sur montant HT
Dim strNonAutre As String
Dim strAutre As String

'-> API
Private Const LOCALE_STHOUSAND = &HF
Private Const LOCALE_USER_DEFAULT& = &H400
Private Const LOCALE_SDECIMAL = 14
Private Declare Function GetLocaleInfo& Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long)

'-> Pour gestion des convertions
Dim SepDec As String
Dim SepMil As String

'-> Pour gestion de la cellule de d�part
Const LigneDep = "5"
Const ColDep = "B"
Dim ColFin As String

'-> Pour gestion de la temporisation
Public TailleTotale As Long
Public TailleLue As Long

'-> Pour gestion en KF, MF
Const Groupe = 1000

'-> Indique si la gestion des AUTRES est OK
Public IsAutreGest As Boolean

Public Sub EndJob()

'---> Point Final de l'applicatif

'-> Fermer tous les fichiers
Reset

'-> Lib�rer le pointeur sur le lien OLe vers EXCEL
Set aWorkBook = Nothing
Set ExcelApp = Nothing

'-> Fin
End

End Sub

Public Function InitExcel() As Boolean

'---> Cette proc�dure initialise Excel


On Error GoTo GestError

'-> Initialiser l'application
Set ExcelApp = New Excel.Application

'-> Initialiser le classeur
Set aWorkBook = ExcelApp.Workbooks.Add

'-> Initialiser les collections du mod�le objet de travail
Set Lignes = New Collection
Set Filiales = New Collection
Set Familles = New Collection
Set Fournisseurs = New Collection
Set Affaires = New Collection

'-> Lecture du fichier ascii de travail
If Not AnalyseFile(Form1.Label6.Caption) Then
    MsgBox "Erreur durant l'analyse du fichier de travail : " & Chr(13) & Form1.Label6.Caption, vbCritical + vbOKOnly, "Erreur fatale"
    EndJob
End If

'-> Cr�ation du mod�le objet dans le programme
Call FileToCom

'-> Renvoyer une valeur de succ�s
InitExcel = True

Exit Function

GestError:
    InitExcel = False


End Function

Public Sub FileToCom()

'---> Cette fonction Analyse les lignes de la collection pour cr�er le mod�le objet

'-> Etablir les cumuls :

'- Filiale et affaire par famille
'- Filiale et affaire et  famille par fournisseur

' Descriptif Fichier Data Lucie
' Code Famille : 31, Libel Famille  : 32
' Typaux : 1, Codaux : 2 , Libelle : 3
' Code affaire : 40 , Libell� : 41
' Fililale : 35 , Libell� : 36

Dim aLig As clsLigne
Dim aFam As clsFamille
Dim aAff As clsAffaire
Dim aFrs As clsFournisseur
Dim aFil As clsFiliale
Dim aAffect As clsAffectation

Dim Typaux As String
Dim Codaux As String
Dim Libel As String

Dim MontantHT As Double
Dim MontantRef As Double
Dim UnMontant As Variant

'-> Gestion des Erreurs
On Error GoTo GestError

'-> Pour toutes les lignes du fichier ASCII
For Each aLig In Lignes

    '-> Dessin de la temporisation
    TailleLue = TailleLue + Len(aLig.Contenu)
    DrawTempo 4

    '-> R�cup�ration du montant HT pour cette ligne
    UnMontant = Trim(Entry(8, aLig.Contenu, "|"))
    If Trim(UnMontant) = "" Then UnMontant = "0"
    MontantHT = CDbl(Convert(UnMontant))
    
    '-> R�cup�ration du montant REF pour cette ligne
    UnMontant = Trim(Entry(48, aLig.Contenu, "|"))
    If Trim(UnMontant) = "" Then UnMontant = "0"
    MontantRef = CDbl(Convert(UnMontant))
    
    '-> Mise � jour des cumuls
    CumulHT = CumulHT + MontantHT
    CumulRef = CumulRef + MontantRef
    
    '***********
    '* FAMILLE *
    '***********
    
    '-> R�cup�ration de la famille de la ligne
    Codaux = UCase$(RTrim(Entry(31, aLig.Contenu, "|")))
    Libel = RTrim(Entry(32, aLig.Contenu, "|"))
        
    '-> V�rifier si cette famille existe ou non
    If Not IsObject("FAM", "FAM|" & Codaux) Then
        '-> Ajouter dans la collection des familles
        Set aFam = New clsFamille
        aFam.Code = Codaux
        aFam.Libell� = Libel
        Familles.Add aFam, "FAM|" & Codaux
    Else
        '-> Pointer sur la famille
        Set aFam = Familles("FAM|" & Codaux)
    End If
        
    '***************
    '* FOURNISSEUR *
    '***************
    
    '-> R�cup�ration du fournisseur de la ligne
    Typaux = UCase$(RTrim(Entry(1, aLig.Contenu, "|")))
    Codaux = UCase$(RTrim(Entry(2, aLig.Contenu, "|")))
    Libel = UCase$(RTrim(Entry(3, aLig.Contenu, "|")))
                
    '-> V�rifier si le fournisseur existe d�ja
    If Not IsObject("TIE", "TIE|" & Typaux & "|" & Codaux) Then
        '-> Cr�ation d'un nouveau Tiers
        Set aFrs = New clsFournisseur
        aFrs.Typaux = Typaux
        aFrs.Codaux = Codaux
        aFrs.Libell� = Libel
        Fournisseurs.Add aFrs, "TIE|" & Typaux & "|" & Codaux
    Else
        '-> Pointer sur le fournisseur
        Set aFrs = Fournisseurs("TIE|" & Typaux & "|" & Codaux)
    End If
                                
    '-> Tableau FOFAIND
    
    '-> V�rifier si cette famille est d�ja affect�e � ce fournisseur
    If Not IsAffectation(aFrs, 0, "FAM|" & aFam.Code) Then
        '-> Cr�er un nouvelle affectation Frs/Famille
        Set aAffect = New clsAffectation
        aAffect.clsType = 0 'Type Famille
        aAffect.Key = "FAM|" & aFam.Code
        aFrs.Familles.Add aAffect, aAffect.Key
        '-> Mettre � jour le cumul
        aAffect.MontantHT = MontantHT
        aAffect.MontantRef = MontantRef
    Else
        '-> L'affectation existe d�ja : pointer dessus et mettre � jour le cumul
        Set aAffect = aFrs.Familles("FAM|" & aFam.Code)
        '-> Mettre � jour le cumul
        aAffect.MontantHT = aAffect.MontantHT + MontantHT
        aAffect.MontantRef = aAffect.MontantRef + MontantRef
    End If
    
    '-> Mise � jour du cumul g�n�ral ( pour toutes les familles de ce fournisseur)
    aFrs.MontantHT = aFrs.MontantHT + MontantHT
    aFrs.MontantRef = aFrs.MontantRef + MontantRef
                                
    '***********
    '* AFFAIRE *
    '***********
                                                            
    '-> R�cup�ration de l'affaire de la ligne
    Codaux = UCase$(RTrim(Entry(40, aLig.Contenu, "|")))
    Libel = RTrim(Entry(41, aLig.Contenu, "|"))
    
    '-> Tester si l'affaire existe d�ja
    If Not IsObject("AFF", "AFF|" & Codaux) Then
        '-> Cr�er une nouvelle instance
        Set aAff = New clsAffaire
        aAff.Code = Codaux
        aAff.Libelle = Libel
        '-> R�aliser l'affectation
        Affaires.Add aAff, "AFF|" & Codaux
    Else
        '-> Pointer sur l'affaire
        Set aAff = Affaires("AFF|" & Codaux)
    End If
    
    '-> Tableau IFI
    
    '-> V�rifier si cette affaire est d�ja affect�e � cette famille
    If Not IsAffectation(aFam, 3, "AFF|" & aAff.Code) Then
        '-> Cr�er une nouvelle instance
        Set aAffect = New clsAffectation
        aAffect.clsType = 3 'Type affaire
        aAffect.Key = "AFF|" & aAff.Code
        aFam.Affaires.Add aAffect, aAffect.Key
        '-> Mettre � jour le cumul
        aAffect.MontantHT = MontantHT
        aAffect.MontantRef = MontantRef
    Else
        '-> L'affectation pour cette affaire existe d�ja : pointer dessus
        Set aAffect = aFam.Affaires("AFF|" & aAff.Code)
        '-> Mise � jour des cumul pour cette affaire de cette famille
        aAffect.MontantHT = aAffect.MontantHT + MontantHT
        aAffect.MontantRef = aAffect.MontantRef + MontantRef
    End If
    
    '-> Mise � jour du cumul de toutes les affaires pour cette famille
    aFam.MontantHT = aFam.MontantHT + MontantHT
    aFam.MontantRef = aFam.MontantRef + MontantRef
    
    
    '-> Tableau IFOFI
    
    '-> V�rifier si cette affaire est d�ja affect� au fournisseur en cours
    If Not IsAffectation(aFrs, 3, "AFF|" & aAff.Code) Then
        '-> Cr�er une nouvelle instance
        Set aAffect = New clsAffectation
        aAffect.clsType = 3 'Type affaire
        aAffect.Key = "AFF|" & aAff.Code
        aFrs.Affaires.Add aAffect, aAffect.Key
        '-> Mettre � jour le cumul
        aAffect.MontantHT = MontantHT
        aAffect.MontantRef = MontantRef
    Else
        '-> L'affectation pour cette affaire existe d�ja : pointer dessus
        Set aAffect = aFrs.Affaires("AFF|" & aAff.Code)
        '-> Mise � jour des cumul pour cette affaire de ce fournisseur
        aAffect.MontantHT = aAffect.MontantHT + MontantHT
        aAffect.MontantRef = aAffect.MontantRef + MontantRef
    End If
        
        
    '***********
    '* FILIALE *
    '***********
        
    '-> R�cup�ration de la fililale de la ligne
    Codaux = UCase$(RTrim(Entry(35, aLig.Contenu, "|")))
    Libel = RTrim(Entry(36, aLig.Contenu, "|"))

    '-> V�rifier si la filiale existe
    If Not IsObject("FIL", "FIL|" & Codaux) Then
        '-> Cr�ation d'une nouvelle instance
        Set aFil = New clsFiliale
        aFil.Code = Codaux
        aFil.Libell� = Libel
        Filiales.Add aFil, "FIL|" & aFil.Code
    Else
        '-> Pointer sur la filiale
        Set aFil = Filiales("FIL|" & Codaux)
    End If
    
    '-> V�rifier si la filiale est affect�e � la famille en cours
    If Not IsAffectation(aFam, 1, "FIL|" & aFil.Code) Then
        '-> Cr�er l'affectation de cette filiale pour la famille en cours
        Set aAffect = New clsAffectation
        aAffect.clsType = 1 'Type filiale
        aAffect.Key = "FIL|" & aFil.Code
        aFam.Filiales.Add aAffect, aAffect.Key
        '-> Mettre � jour le cumul
        aAffect.MontantHT = MontantHT
        aAffect.MontantRef = MontantRef
    Else
        '-> L'affectation pour cette filiale existe d�ja : pointer dessus
        Set aAffect = aFam.Filiales("FIL|" & aFil.Code)
        '-> Mise � jour des cumul pour cette filiale de cette famille
        aAffect.MontantHT = aAffect.MontantHT + MontantHT
        aAffect.MontantRef = aAffect.MontantRef + MontantRef
    End If
        
    '-> V�rifier si la filiale est affect�e au fournisseur en cours
    If Not IsAffectation(aFrs, 1, "FIL|" & aFil.Code) Then
        '-> Cr�er l'affectation de cette filiale pour le fournisseur en cours
        Set aAffect = New clsAffectation
        aAffect.clsType = 1 'Type filiale
        aAffect.Key = "FIL|" & aFil.Code
        aFrs.Filiales.Add aAffect, aAffect.Key
        '-> Mettre � jour le cumul
        aAffect.MontantHT = MontantHT
        aAffect.MontantRef = MontantRef
    Else
        '-> L'affectation pour cette filiale existe d�ja : pointer dessus
        Set aAffect = aFrs.Filiales("FIL|" & aFil.Code)
        '-> Mise � jour des cumul pour cette filiale de cette famille
        aAffect.MontantHT = aAffect.MontantHT + MontantHT
        aAffect.MontantRef = aAffect.MontantRef + MontantRef
    End If
    
Next 'Pour toutes les lignes

'-> finir le dessin de la temporisation
TailleLue = TailleTotale
DrawTempo 4

Exit Sub

GestError:

    MsgBox "Erreur dans l'impl�mentation du mod�le objet " & Chr(13) & aLig.Contenu & Chr(13) & Err.Number & "  " & Err.Description, vbCritical + vbOKOnly, "Erreur fatale"
    Call EndJob

End Sub

Private Function IsAffectation(ParentObj As Object, IdSsObj As Integer, KeySsObj As String) As Boolean

'---> Cette proc�dure v�rifie si un object d'affectation existe pour un objet de premier niveau

On Error GoTo GestError

Dim aAffect As clsAffectation

'-> Selon le tyope de sous objet
Select Case IdSsObj

    Case 0 ' Famille
        '-> pointer sur l'objet sp�cifi�
        Set aAffect = ParentObj.Familles(KeySsObj)
    Case 1 'Filiale
        '-> pointer sur l'objet sp�cifi�
        Set aAffect = ParentObj.Filiales(KeySsObj)
    Case 3 'Affaire
        '-> pointer sur l'objet sp�cifi�
        Set aAffect = ParentObj.Affaires(KeySsObj)

End Select

'-> Renvoyer une valeur de succ�s
IsAffectation = True

Exit Function

GestError:

    '-> Renvoyer une valeur d'erreur
    IsAffectation = False

End Function

Private Function IsObject(ByVal ObjectType As String, ByVal Key As String) As Boolean

'---> Cette proc�dure d�termine si l'objet sp�cifi� est d�ja pr�sent dans la collection affect�e

Dim aObject As Object

On Error GoTo GestError

'-> Selon le type de l'objet

Select Case ObjectType
    Case "AFF"
        '-> V�rifier dans la collection des affaires
        Set aObject = Affaires(Key)
    Case "FAM"
        '-> V�rifier dans la classe des familles
        Set aObject = Familles(Key)
    Case "FIL"
        '-> V�rifier dans la classe des filiales
        Set aObject = Filiales(Key)
    Case "TIE"
        '-> V�rifier dans la collection des fournisseurs
        Set aObject = Fournisseurs(Key)
End Select

'-> Renvoyer une vameur de succ�s
IsObject = True

Exit Function

GestError:

    '->Renvoyer une valeur d'erreur
    IsObject = False
    




End Function



Private Sub DrawTempo(IdTempo As Integer)

'---> Cette proc�dure Dessine une barre de temporisation


'0 -> Indice Achat par Famille
'1 -> Indice Achat par fournisseur
'2 -> Indice Achat par Famille et chantier
'3 -> Indice Achat par Fournisseur et Chantier
'4 -> Analyse du fichier ASCII de travail
'5 -> Indice Achat par Fournisseur et Famille2

DoEvents
Form1.Picture1(IdTempo).Line (0, 0)-((TailleLue / TailleTotale) * Form1.Picture1(IdTempo).ScaleWidth, Form1.Picture1(IdTempo).ScaleHeight), &HC00000, BF



End Sub

Private Function AnalyseFile(FileToAnalyse As String) As Boolean

Dim hdlFile As Integer
Dim Ligne As String
Dim aLigne As clsLigne
Dim lpBuffer As String
Dim Res As Long
Dim i As Long

'-> Gestion des erreurs
On Error GoTo GestError

'-> Initialisation des variables de temporisation
TailleTotale = FileLen(Form1.Label6.Caption) * 2
TailleLue = 0

'-> Ouvrir le fichier ascii pour charger toutes les lignes
hdlFile = FreeFile
Open FileToAnalyse For Input As #hdlFile
i = 1
Do While Not EOF(hdlFile)
    DoEvents
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> Mettre � jour la temporisation
    TailleLue = TailleLue + Len(Ligne)
    DrawTempo 4
    '-> Ne pas tenir compte de la premi�re ligne
    If i = 1 Then
        i = i + 1
        GoTo NextLigne
    End If
    '-> Charger dans la collection
    Set aLigne = New clsLigne
    aLigne.Contenu = Ligne
    aLigne.Key = "LIG|" & Lignes.Count + 1
    Lignes.Add aLigne, aLigne.Key
NextLigne:
Loop 'Pour tout le fichier

'*******************************************
'* Initialisation des s�parateurs d�cimaux *
'*******************************************

lpBuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, lpBuffer, Len(lpBuffer))
SepDec = Mid$(lpBuffer, 1, Res - 1)
lpBuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, lpBuffer, Len(lpBuffer))
SepMil = Mid$(lpBuffer, 1, Res - 1)

'-> Renvoyer une valeur de succ�s
AnalyseFile = True

'-> Quitter la fonction
Exit Function

GestError:

'-> Renvoyer une valeur d'erreur
AnalyseFile = False

End Function

Private Sub FormatRange(aRange As Range, ByVal FormatType As Integer)

'---> Cette proc�dure formatte une cellule : formatType : 0 -> Titre , & , d�tail

'-> Titre
If FormatType = 0 Then
    '-> Aligner au centre
    aRange.HorizontalAlignment = xlCenter
    '-> Font du titre
    aRange.Font.FontStyle = "Gras"
    aRange.Font.Size = 14
    '-> Fond de la cellule
    aRange.Interior.ColorIndex = 15
    aRange.Interior.Pattern = xlSolid
    aRange.Interior.PatternColorIndex = xlAutomatic
End If

'-> Contour pour toutes les cellules
aRange.Borders(xlEdgeLeft).LineStyle = xlContinuous
aRange.Borders(xlEdgeLeft).Weight = xlThin
aRange.Borders(xlEdgeTop).LineStyle = xlContinuous
aRange.Borders(xlEdgeTop).Weight = xlThin
aRange.Borders(xlEdgeBottom).LineStyle = xlContinuous
aRange.Borders(xlEdgeBottom).Weight = xlThin
aRange.Borders(xlEdgeRight).LineStyle = xlContinuous
aRange.Borders(xlEdgeRight).Weight = xlThin


End Sub



Public Function Convert(ByVal StrToAnalyse As String) As String

Dim i As Integer
Dim Tempo As String
Dim IsSep As Boolean

For i = Len(StrToAnalyse) To 1 Step -1
    If Mid$(StrToAnalyse, i, 1) = "." Then
        If IsSep Then GoTo NextChar
        Tempo = SepDec & Tempo
        IsSep = True
    ElseIf Mid$(StrToAnalyse, i, 1) = "," Then
        If IsSep Then GoTo NextChar
        Tempo = SepDec & Tempo
    Else
        Tempo = Mid$(StrToAnalyse, i, 1) & Tempo
    End If
NextChar:
Next 'Pour tous les caract�res � analyser

Convert = Tempo

End Function


Public Sub CreateIGXLS()

'---> Cette proc�dure cr�er le tableau excel IG.XLS

Dim aFam As clsFamille, aFam2 As clsFamille
Dim aAffect As clsAffectation, aAffect2 As clsAffectation
Dim aFil As clsFiliale
Dim aRange As Range
Dim i As Integer, j As Integer
Dim ListeFiliale As String
Dim Tempo1 As Double
Dim Tempo2 As Double
Dim Tempo3 As Double

On Error GoTo GestError

'-> Mise � jour de l'interface
Form1.Label9(0).Caption = "En cours"
DoEvents

'-> Ajouter une feuille au classeur en cours
Set aSheet = aWorkBook.Worksheets.Add
aSheet.Name = "Tableau IG"

'-> Tri des familles dans l'ordre d�croissant du Montant HT
For Each aFam In Familles
    If strToTri = "" Then
        strToTri = Format(aFam.MontantHT, "000000000000000.00") & "�" & "FAM|" & aFam.Code
    Else
        strToTri = strToTri & "�" & Format(aFam.MontantHT, "000000000000000.00") & "�" & "FAM|" & aFam.Code
    End If
Next

'-> Trier les familles
strTri = Tri(strToTri, "�", "�")

'-> Gestion des 90%
Call Gestion90

'-> Tri des filiales dans l'ordre alphab�tique
strToTri = ""
For Each aFil In Filiales
    If strToTri = "" Then
        strToTri = aFil.Libell� & "�FIL|" & aFil.Code
    Else
        strToTri = strToTri & "�" & aFil.Libell� & "�FIL|" & aFil.Code
    End If
Next
ListeFiliale = Tri(strToTri, "�", "�")

'************************
'* Libell� des colonnes *
'************************

'-> Se positionner sur la cellule de d�part
Set aRange = aSheet.Range(ColDep & LigneDep)

'-> Libell� FAMILLE
aRange.Value = "Famille"
FormatRange aRange, 0

'-> Libell� KF
Set aRange = aRange.Offset(0, 1)
aRange.Value = "KF"
FormatRange aRange, 0

'-> Libell� %
Set aRange = aRange.Offset(0, 1)
aRange.Value = "%"
FormatRange aRange, 0

'-> Mise � jour de la liste des filiales
For i = 1 To NumEntries(ListeFiliale, "�")
    '-> Pointer sur l'objet filiale
    Set aFil = Filiales(Entry(i, ListeFiliale, "�"))
    '-> Valeur et formattage
    Set aRange = aRange.Offset(0, 1)
    aRange.Value = aFil.Code & " : " & aFil.Libell�
    FormatRange aRange, 0
Next 'Pour toutes les filiales


'-> Libell� Groupe
Set aRange = aRange.Offset(0, 1)
aRange.Value = "GROUPE"
FormatRange aRange, 0

'**********************
'* Lib�ll� des lignes *
'**********************

'-> Se positionner sur la cellule de d�part
Set aRange = aSheet.Range(ColDep & LigneDep)

'-> Liste des familles
For i = 1 To NumEntries(strNonAutre, "�")
    '-> Pointer sur la famille
    Set aFam = Familles(Entry(i, strNonAutre, "�"))
    '-> Valeur et formattage
    Set aRange = aRange.Offset(1, 0)
    aRange.Value = aFam.Code & " : " & aFam.Libell�
    FormatRange aRange, 0
Next

'-> Libelle de AUTRE
If IsAutreGest Then
    Set aRange = aRange.Offset(1, 0)
    aRange.Value = "AUTRES"
    FormatRange aRange, 0
End If

'-> Libell� Total
Set aRange = aRange.Offset(1, 0)
aRange.Value = "Total"
FormatRange aRange, 0

'-> Libell� Economie (KF)
Set aRange = aRange.Offset(1, 0)
aRange.Value = "Economie (KF)"
FormatRange aRange, 0

'-> Libell� %
Set aRange = aRange.Offset(1, 0)
aRange.Value = "%"
FormatRange aRange, 0

'-> Indice
Set aRange = aRange.Offset(1, 0)
aRange.Value = "Indice"
FormatRange aRange, 0

'*************************************
'* Calcul de la zone 2  , 3 , 1  , 5 *
'*************************************

'-> Se positionner sur la cellule de d�part avec un decallage de : _
1 colonne pour zone KF _
1 colonne pour zone % _
1 colonne pour zone groupe _
1 colonne pour chaque filiale
Set aRange = aSheet.Columns(aSheet.Columns(ColDep).Column + Filiales.Count + 3)
Set aRange = aSheet.Range(Entry(2, aRange.Address, ":") & LigneDep)


'-> Liste des familles
For i = 1 To NumEntries(strNonAutre, "�")
    '-> Init des variables
    Tempo1 = 0
    Tempo2 = 0
    '-> Pointer sur la famille
    Set aFam = Familles(Entry(i, strNonAutre, "�"))
    '-> Calcul de la zone 2
    Set aRange = aRange.Offset(1, -(Filiales.Count + 2))
    aRange.Value = aFam.MontantHT / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    '-> Calcul de la zone 3
    Set aRange = aRange.Offset(0, 1)
    If CumulHT <> 0 Then aRange.Value = aFam.MontantHT / CumulHT
    aRange.NumberFormat = "0.00 %"
    FormatRange aRange, 1
    
    '-> Calcul de la zone 1
    For j = 1 To NumEntries(ListeFiliale, "�")
        '-> D�caler la cellule
        Set aRange = aRange.Offset(0, 1)
        '-> Pointer sur la filiale sp�cifi�e si elle est renseign�e
        If IsAffectation(aFam, 1, Entry(j, ListeFiliale, "�")) Then
            Set aAffect = aFam.Filiales(Entry(j, ListeFiliale, "�"))
            '-> Mettre � jour la valeur
            If aAffect.MontantRef <> 0 Then aRange.Value = (aAffect.MontantHT / aAffect.MontantRef) * 100
            aRange.NumberFormat = "0.0"
            '-> cumul des valeurs pour la zone groupe
            Tempo1 = Tempo1 + aAffect.MontantHT
            Tempo2 = Tempo2 + aAffect.MontantRef
        End If
        '-> Formattage de la zone
        FormatRange aRange, 1
    Next
    '-> Calcul de la zone 5
    Set aRange = aRange.Offset(0, 1)
    If Tempo2 <> 0 Then aRange.Value = (Tempo1 / Tempo2) * 100
    aRange.NumberFormat = "0.0"
    '-> Formattage de la zone
    FormatRange aRange, 1
Next


If IsAutreGest Then

    '-> Pour autres familles calcul de la zone 2
    Set aRange = aRange.Offset(1, -(Filiales.Count + 2))
    aRange.Value = CumulAutreHT / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1

    '-> Pour autres familles calcul de la zone 3
    Set aRange = aRange.Offset(0, 1)
    If CumulHT <> 0 Then aRange.Value = CumulAutreHT / CumulHT
    aRange.NumberFormat = "0.00 %"
    FormatRange aRange, 1

End If

'-> Init des cumuls
Tempo1 = 0
Tempo2 = 0

'-> V�rifier que la famille autre existe
If IsObject("FAM", "FAM|AUTRE_DEAL") Then
    '-> Pointer sur la famille Autre
    Set aFam = Familles("FAM|AUTRE_DEAL")
    '-> Ajouter de base toutes les filiales � cette famille
    For j = 1 To NumEntries(ListeFiliale, "�")
        '-> Cr�er un objet d'affectation
        Set aAffect = New clsAffectation
        aAffect.clsType = 1
        aAffect.Key = Entry(j, ListeFiliale, "�")
        aFam.Filiales.Add aAffect, aAffect.Key
    Next 'Pour toutes les filiales
    
    '-> Faire le cumul pour la famille autre
    For i = 1 To NumEntries(strAutre, "�")
        '-> Pointer sur la famille
        Set aFam2 = Familles(Entry(i, strAutre, "�"))
        '-> Faire le cumul pour toutes les filiales de cette famille
        For Each aAffect In aFam2.Filiales
            '-> Pointer sur l'affectation de la famille autre
            Set aAffect2 = aFam.Filiales(aAffect.Key)
            '-> Faire le cumul
            aAffect2.MontantHT = aAffect2.MontantHT + aAffect.MontantHT
            aAffect2.MontantRef = aAffect2.MontantRef + aAffect.MontantRef
        Next
    Next 'Pour toutes les familles de autre
    
    '-> Mise � jour de la zone 1 dans Excel
    For j = 1 To NumEntries(ListeFiliale, "�")
        '-> Pointer sur l'aobjet d'affectation de la filiale autre
        Set aAffect = aFam.Filiales(Entry(j, ListeFiliale, "�"))
        '-> Se d�caller d'une colonne
        Set aRange = aRange.Offset(0, 1)
        '-> Tester si on positionner la valeur
        If aAffect.MontantRef <> 0 Then
            '-> Contenu de la zone 1
            If aAffect.MontantRef <> 0 Then aRange.Value = (aAffect.MontantHT / aAffect.MontantRef) * 100
            aRange.NumberFormat = "0.0"
        End If
        '-> Formattage de la zone
        FormatRange aRange, 1
        '-> Mise � jour des cumuls
        Tempo1 = Tempo1 + aAffect.MontantHT
        Tempo2 = Tempo2 + aAffect.MontantRef
    Next
    
    '-> Mise � jour de la zone 5 total Groupe
    Set aRange = aRange.Offset(0, 1)
    If Tempo2 <> 0 Then
        If Tempo2 <> 0 Then aRange.Value = (Tempo1 / Tempo2) * 100
        aRange.NumberFormat = "0.0"
    End If
    FormatRange aRange, 1
    
End If 'Si la famille autre existe

'**************************
'* Calcul de la zone 2bis *
'**************************
'-> En KF
Set aRange = aSheet.Range(Chr(Asc(ColDep) + 1) & aRange.Row + 1)
aRange.Value = CumulHT / Groupe
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> En %
Set aRange = aRange.Offset(0, 1)
If CumulHT <> 0 Then aRange.Value = CumulHT / CumulHT
aRange.NumberFormat = "0.00 %"
FormatRange aRange, 1

'-> Pour toutes les filiales
For i = 1 To NumEntries(ListeFiliale, "�")
    '-> Pointer sur la filiale
    Set aFil = Filiales(Entry(i, ListeFiliale, "�"))
    '-> Mettre le cumulo � 0
    Tempo1 = 0
    Tempo2 = 0
    '-> Pour toutes les familles
    For Each aFam In Familles
        '-> Ne pas prendre la famille autre
        If aFam.Code <> "AUTRE_DEAL" Then
            '-> V�rifier que cette filiale est affect�e � cette famille
            If IsAffectation(aFam, 1, "FIL|" & aFil.Code) Then
                '-> Pointer vers l'objet d'affectation
                Set aAffect = aFam.Filiales("FIL|" & aFil.Code)
                '-> Faire le cumul
                Tempo1 = Tempo1 + aAffect.MontantHT
                Tempo2 = Tempo2 + aAffect.MontantRef
                Tempo3 = Tempo3 + aAffect.MontantHT
            End If
        End If 'Si on ne traite pas la famille autre
    Next 'Pour toutes les familles
    
    '-> D�caller la cellule et mise � jour de la zone 8
    Set aRange = aRange.Offset(0, 1)
    aRange.Value = Tempo1 / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> D�caller et mise � jour de la zone 7
    Set aRange = aRange.Offset(1, 0)
    aRange.Value = (-Tempo1 + Tempo2) / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> D�caller et mise � jour de la zone 9
    Set aRange = aRange.Offset(1, 0)
    aRange.Value = Tempo1 / CumulHT
    aRange.NumberFormat = "0.00 %"
    FormatRange aRange, 1
    
    '-> D�caller et mise � jour de la zone 6
    Set aRange = aRange.Offset(1, 0)
    If Tempo2 <> 0 Then aRange.Value = (Tempo1 / Tempo2) * 100
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> Se repositionner
    Set aRange = aRange.Offset(-3, 0)
    
Next 'Pour toutes les filiales
    
'-> Mettre � jour le cumul groupe pour la zone 8
Set aRange = aRange.Offset(0, 1)
aRange.Value = Tempo3 / Groupe
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> Mettre � jour le cumul groupe pour la zone 7
Set aRange = aRange.Offset(1, 0)
aRange.Value = (-CumulHT + CumulRef) / Groupe
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> Mettre � jour le cumul groupe pour la zone 9
Set aRange = aRange.Offset(1, 0)
aRange.Value = CumulHT / CumulHT
aRange.NumberFormat = "0.00 %"
FormatRange aRange, 1

'-> Mettre � jour le cumul groupe pour la zone 6
Set aRange = aRange.Offset(1, 0)
aRange.Value = (CumulHT / CumulRef) * 100
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> Se repositionner au d�but
Set aRange = aSheet.Range(Chr(Asc(ColDep) + 1) & aRange.Row - 2)

'-> Fermeture du tableau
For i = 1 To 3
    '-> Colonne KF
    FormatRange aRange, 1
    '-> Colonne %
    Set aRange = aRange.Offset(0, 1)
    FormatRange aRange, 1
    '-> Se d�caller
    Set aRange = aRange.Offset(1, -1)
Next


'-> Largeur automatique des colonnes
Set aRange = aSheet.Columns(aSheet.Columns(ColDep).Column + Filiales.Count + 3)
ExcelApp.Columns(ColDep & ":" & Entry(2, aRange.Address, ":")).EntireColumn.AutoFit

'-> Supprimer la famille autre
If IsObject("FAM", "FAM|AUTRE_DEAL") Then Familles.Remove ("FAM|AUTRE_DEAL")

'-> Mise � jour de l'interface
Form1.Label9(0).Caption = "Termin�"
DoEvents

Exit Sub

GestError:

    '-> Mise � jour de l'interface
    Form1.Label9(0).Caption = "Erreur"
    DoEvents
    

End Sub

Public Sub CreateIFOGXLS()

'---> Cette proc�dure cr�er le tableau excel IFOG.XLS

Dim aFrs As clsFournisseur, aFrs2 As clsFournisseur

Dim aAffect As clsAffectation, aAffect2 As clsAffectation
Dim aFil As clsFiliale
Dim aRange As Range
Dim i As Integer, j As Integer
Dim ListeFiliale As String
Dim Tempo1 As Double
Dim Tempo2 As Double
Dim Tempo3 As Double

On Error GoTo GestError

'-> Mise � jour de l'interface
Form1.Label9(1).Caption = "En cours"
DoEvents

'-> Ajouter une feuille au classeur en cours
Set aSheet = aWorkBook.Worksheets.Add
aSheet.Name = "Tableau IFOG"

'-> Remise � jour des variables
strTri = ""
strToTri = ""

'-> Tri des fournisseurs dans l'ordre d�croissant du Montant HT
For Each aFrs In Fournisseurs
    If strToTri = "" Then
        strToTri = Format(aFrs.MontantHT, "000000000000000.00") & "�" & "TIE|" & aFrs.Typaux & "|" & aFrs.Codaux
    Else
        strToTri = strToTri & "�" & Format(aFrs.MontantHT, "000000000000000.00") & "�" & "TIE|" & aFrs.Typaux & "|" & aFrs.Codaux
    End If
Next

'-> Trier les familles
strTri = Tri(strToTri, "�", "�")

'-> Gestion des 90%
Call Gestion90

'-> Tri des filiales dans l'ordre alphab�tique
strToTri = ""
For Each aFil In Filiales
    If strToTri = "" Then
        strToTri = aFil.Libell� & "�FIL|" & aFil.Code
    Else
        strToTri = strToTri & "�" & aFil.Libell� & "�FIL|" & aFil.Code
    End If
Next
ListeFiliale = Tri(strToTri, "�", "�")

'************************
'* Libell� des colonnes *
'************************

'-> Se positionner sur la cellule de d�part
Set aRange = aSheet.Range(ColDep & LigneDep)

'-> Libell� Code Auxiliaire
Set aRange = aRange.Offset(0, -1)
aRange.Value = "Type Tiers"
FormatRange aRange, 0

'-> Libell� FAMILLE
Set aRange = aRange.Offset(0, 1)
aRange.Value = "Code Fournisseur"
FormatRange aRange, 0

'-> Libell� KF
Set aRange = aRange.Offset(0, 1)
aRange.Value = "KF"
FormatRange aRange, 0

'-> Libell� %
Set aRange = aRange.Offset(0, 1)
aRange.Value = "%"
FormatRange aRange, 0

'-> Mise � jour de la liste des filiales
For i = 1 To NumEntries(ListeFiliale, "�")
    '-> Pointer sur l'objet filiale
    Set aFil = Filiales(Entry(i, ListeFiliale, "�"))
    '-> Valeur et formattage
    Set aRange = aRange.Offset(0, 1)
    aRange.Value = aFil.Code & " : " & aFil.Libell�
    FormatRange aRange, 0
Next 'Pour toutes les filiales

'-> Libell� Groupe
Set aRange = aRange.Offset(0, 1)
aRange.Value = "GROUPE"
FormatRange aRange, 0

'**********************
'* Lib�ll� des lignes *
'**********************

'-> Se positionner sur la cellule de d�part
Set aRange = aSheet.Range(ColDep & LigneDep)

'-> Liste des familles
For i = 1 To NumEntries(strNonAutre, "�")
    '-> Pointer sur la famille
    Set aFrs = Fournisseurs(Entry(i, strNonAutre, "�"))
    '-> Valeur et formattage
    Set aRange = aRange.Offset(1, -1)
    aRange.Value = aFrs.Typaux
    FormatRange aRange, 0
    '-> Libell�
    Set aRange = aRange.Offset(0, 1)
    aRange.Value = aFrs.Codaux & " " & aFrs.Libell�
    FormatRange aRange, 0
Next

'-> Libelle de AUTRE
If IsAutreGest Then
    Set aRange = aRange.Offset(1, 0)
    aRange.Value = "AUTRES"
    FormatRange aRange, 0
End If

'-> Libell� Total
Set aRange = aRange.Offset(1, 0)
aRange.Value = "Total"
FormatRange aRange, 0

'-> Libell� Economie (KF)
Set aRange = aRange.Offset(1, 0)
aRange.Value = "Economie (KF)"
FormatRange aRange, 0

'-> Libell� %
Set aRange = aRange.Offset(1, 0)
aRange.Value = "%"
FormatRange aRange, 0

'-> Indice
Set aRange = aRange.Offset(1, 0)
aRange.Value = "Indice"
FormatRange aRange, 0

'*************************************
'* Calcul de la zone 2  , 3 , 1  , 5 *
'*************************************

'-> Se positionner sur la cellule de d�part avec un decallage de : _
1 colonne pour zone KF _
1 colonne pour zone % _
1 colonne pour zone groupe _
1 colonne pour chaque filiale
Set aRange = aSheet.Columns(aSheet.Columns(ColDep).Column + Filiales.Count + 3)
Set aRange = aSheet.Range(Entry(2, aRange.Address, ":") & LigneDep)

'-> Liste des familles
For i = 1 To NumEntries(strNonAutre, "�")
    '-> Init des variables
    Tempo1 = 0
    Tempo2 = 0
    '-> Pointer sur la famille
    Set aFrs = Fournisseurs(Entry(i, strNonAutre, "�"))
    '-> Calcul de la zone 2
    Set aRange = aRange.Offset(1, -(Filiales.Count + 2))
    aRange.Value = aFrs.MontantHT / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    '-> Calcul de la zone 3
    Set aRange = aRange.Offset(0, 1)
    If CumulHT <> 0 Then aRange.Value = aFrs.MontantHT / CumulHT
    aRange.NumberFormat = "0.00 %"
    FormatRange aRange, 1
    
    '-> Calcul de la zone 1
    For j = 1 To NumEntries(ListeFiliale, "�")
        '-> D�caler la cellule
        Set aRange = aRange.Offset(0, 1)
        '-> Pointer sur la filiale sp�cifi�e si elle est renseign�e
        If IsAffectation(aFrs, 1, Entry(j, ListeFiliale, "�")) Then
            Set aAffect = aFrs.Filiales(Entry(j, ListeFiliale, "�"))
            '-> Mettre � jour la valeur
            If aAffect.MontantRef <> 0 Then aRange.Value = (aAffect.MontantHT / aAffect.MontantRef) * 100
            aRange.NumberFormat = "0.0"
            '-> cumul des valeurs pour la zone groupe
            Tempo1 = Tempo1 + aAffect.MontantHT
            Tempo2 = Tempo2 + aAffect.MontantRef
        End If
        '-> Formattage de la zone
        FormatRange aRange, 1
    Next
    '-> Calcul de la zone 5
    Set aRange = aRange.Offset(0, 1)
    If Tempo2 <> 0 Then aRange.Value = (Tempo1 / Tempo2) * 100
    aRange.NumberFormat = "0.0"
    '-> Formattage de la zone
    FormatRange aRange, 1
Next

If IsAutreGest Then

    '-> Pour autres fournisseurs calcul de la zone 2
    Set aRange = aRange.Offset(1, -(Filiales.Count + 2))
    aRange.Value = CumulAutreHT / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> Pour autres fournisseurs calcul de la zone 3
    Set aRange = aRange.Offset(0, 1)
    If CumulHT <> 0 Then aRange.Value = CumulAutreHT / CumulHT
    aRange.NumberFormat = "0.00 %"
    FormatRange aRange, 1

End If

'-> Init des cumuls
Tempo1 = 0
Tempo2 = 0

'-> V�rifier que le fournisseur autre existe
If IsObject("FRS", "TIE|DEAL|AUTRE_DEAL") Then
    '-> Pointer sur le fournisseur Autre
    Set aFrs = Fournisseurs("TIE|DEAL|AUTRE_DEAL")
    '-> Ajouter de base toutes les filiales � ce fournisseur
    For j = 1 To NumEntries(ListeFiliale, "�")
        '-> Cr�er un objet d'affectation
        Set aAffect = New clsAffectation
        aAffect.clsType = 1
        aAffect.Key = Entry(j, ListeFiliale, "�")
        aFrs.Filiales.Add aAffect, aAffect.Key
    Next 'Pour toutes les filiales
    
    '-> Faire le cumul pour le fournisseur autre
    For i = 1 To NumEntries(strAutre, "�")
        '-> Pointer sur le fournisseur
        Set aFrs2 = Fournisseurs(Entry(i, strAutre, "�"))
        '-> Faire le cumul pour toutes les filiales de ce fournisseur
        For Each aAffect In aFrs2.Filiales
            '-> Pointer sur l'affectation du fournisseur autre
            Set aAffect2 = aFrs.Filiales(aAffect.Key)
            '-> Faire le cumul
            aAffect2.MontantHT = aAffect2.MontantHT + aAffect.MontantHT
            aAffect2.MontantRef = aAffect2.MontantRef + aAffect.MontantRef
        Next
    Next 'Pour toutes les fournisseurs de autre
    
    '-> Mise � jour de la zone 1 dans Excel
    For j = 1 To NumEntries(ListeFiliale, "�")
        '-> Pointer sur l'aobjet d'affectation de la filiale autre
        Set aAffect = aFrs.Filiales(Entry(j, ListeFiliale, "�"))
        '-> Se d�caller d'une colonne
        Set aRange = aRange.Offset(0, 1)
        '-> Tester si on positionner la valeur
        If aAffect.MontantRef <> 0 Then
            '-> Contenu de la zone 1
            If aAffect.MontantRef <> 0 Then aRange.Value = (aAffect.MontantHT / aAffect.MontantRef) * 100
            aRange.NumberFormat = "0.0"
        End If
        '-> Formattage de la zone
        FormatRange aRange, 1
        '-> Mise � jour des cumuls
        Tempo1 = Tempo1 + aAffect.MontantHT
        Tempo2 = Tempo2 + aAffect.MontantRef
    Next
    
    '-> Mise � jour de la zone 5 total Groupe
    Set aRange = aRange.Offset(0, 1)
    If Tempo2 <> 0 Then
        If Tempo2 <> 0 Then aRange.Value = (Tempo1 / Tempo2) * 100
        aRange.NumberFormat = "0.0"
    End If
    FormatRange aRange, 1
    
End If 'Si le frs autre existe

'**************************
'* Calcul de la zone 2bis *
'**************************
'-> En KF
Set aRange = aSheet.Range(Chr(Asc(ColDep) + 1) & aRange.Row + 1)
aRange.Value = CumulHT / Groupe
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> En %
Set aRange = aRange.Offset(0, 1)
If CumulHT <> 0 Then aRange.Value = CumulHT / CumulHT
aRange.NumberFormat = "0.00 %"
FormatRange aRange, 1

'-> Pour toutes les filiales
For i = 1 To NumEntries(ListeFiliale, "�")
    '-> Pointer sur la filiale
    Set aFil = Filiales(Entry(i, ListeFiliale, "�"))
    '-> Mettre le cumulo � 0
    Tempo1 = 0
    Tempo2 = 0
    '-> Pour toutes les fournisseurs
    For Each aFrs In Fournisseurs
        '-> Ne pas prendre la famille autre
        If aFrs.Codaux <> "AUTRE_DEAL" Then
            '-> V�rifier que cette filiale est affect�e � ce fournisseur
            If IsAffectation(aFrs, 1, "FIL|" & aFil.Code) Then
                '-> Pointer vers l'objet d'affectation
                Set aAffect = aFrs.Filiales("FIL|" & aFil.Code)
                '-> Faire le cumul
                Tempo1 = Tempo1 + aAffect.MontantHT
                Tempo2 = Tempo2 + aAffect.MontantRef
                Tempo3 = Tempo3 + aAffect.MontantHT
            End If
        End If 'Si on ne traite pas le fournisseur autre
    Next 'Pour tous les fournisseurs
    
    '-> D�caller la cellule et mise � jour de la zone 8
    Set aRange = aRange.Offset(0, 1)
    aRange.Value = Tempo1 / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> D�caller et mise � jour de la zone 7
    Set aRange = aRange.Offset(1, 0)
    aRange.Value = (-Tempo1 + Tempo2) / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> D�caller et mise � jour de la zone 9
    Set aRange = aRange.Offset(1, 0)
    If CumulHT <> 0 Then aRange.Value = Tempo1 / CumulHT
    aRange.NumberFormat = "0.00 %"
    FormatRange aRange, 1
    
    '-> D�caller et mise � jour de la zone 6
    Set aRange = aRange.Offset(1, 0)
    If Tempo2 <> 0 Then aRange.Value = (Tempo1 / Tempo2) * 100
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> Se repositionner
    Set aRange = aRange.Offset(-3, 0)
    
Next 'Pour toutes les filiales
    
'-> Mettre � jour le cumul groupe pour la zone 8
Set aRange = aRange.Offset(0, 1)
aRange.Value = Tempo3 / Groupe
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> Mettre � jour le cumul groupe pour la zone 7
Set aRange = aRange.Offset(1, 0)
aRange.Value = (-CumulHT + CumulRef) / Groupe
aRange.NumberFormat = "0.0"
FormatRange aRange, 1


'-> Mettre � jour le cumul groupe pour la zone 9
Set aRange = aRange.Offset(1, 0)
If CumulHT <> 0 Then aRange.Value = CumulHT / CumulHT
aRange.NumberFormat = "0.00 %"
FormatRange aRange, 1

'-> Mettre � jour le cumul groupe pour la zone 6
Set aRange = aRange.Offset(1, 0)
If CumulRef <> 0 Then aRange.Value = (CumulHT / CumulRef) * 100
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> Se repositionner au d�but
Set aRange = aSheet.Range(Chr(Asc(ColDep) + 1) & aRange.Row - 2)

'-> Fermeture du tableau
For i = 1 To 3
    '-> Colonne KF
    FormatRange aRange, 1
    '-> Colonne %
    Set aRange = aRange.Offset(0, 1)
    FormatRange aRange, 1
    '-> Se d�caller
    Set aRange = aRange.Offset(1, -1)
Next


'-> Largeur automatique des colonnes
Set aRange = aSheet.Columns(aSheet.Columns(ColDep).Column + Filiales.Count + 3)
ExcelApp.Columns(ColDep & ":" & Entry(2, aRange.Address, ":")).EntireColumn.AutoFit

'-> Supprimer le fournisseur autre
If IsObject("TIE", "TIE|DEAL|AUTRE_DEAL") Then Fournisseurs.Remove ("TIE|DEAL|AUTRE_DEAL")

Form1.Label9(1).Caption = "Termin�"
DoEvents

Exit Sub

GestError:

    '-> Mise � jour de l'interface
    Form1.Label9(1).Caption = "Erreur"
    DoEvents


End Sub




Private Sub Gestion90()

'---> Cette proc�dure analyse la chaine StrTri pour cr�er les deux matrices _
de gestion � concurrence de 90% du montant total HT

'-> La matrice StrTri est Tri�e par ordre croissant de montant HT (Frs ou Fam)

Dim i As Integer
Dim aObject As Object
Dim DefString As String

Dim IsAutre As Boolean
Dim Cumul As Double

Dim aAutre As Object
Dim aAffect As clsAffectation

'-> Vider les 2 matrices de s�paration
strAutre = ""
strNonAutre = ""

'-> Par d�faut, pas de gestion des AUTRES
IsAutreGest = False

'-> Mettre � jour les cumuls
CumulAutreHT = 0
CumulAutreRef = 0

'-> Analyse de la matrice de TRI
For i = NumEntries(strTri, "�") To 1 Step -1
    '-> R�cup�rer le type d'objet
    DefString = Entry(i, strTri, "�")
    Select Case Entry(1, DefString, "|")
        Case "FAM"
            Set aObject = Familles(DefString)
        Case "TIE"
            Set aObject = Fournisseurs(DefString)
    End Select
    
    '-> Mettre � jour le cumul
    Cumul = Cumul + aObject.MontantHT

    '-> Tester si on est en cas autre
    If Not IsAutre Then

        '-> Ajouter simplement dans la matrice des nons autres
        If strNonAutre = "" Then
            strNonAutre = DefString
        Else
            strNonAutre = strNonAutre & "�" & DefString
        End If

        '-> V�rifier le cumul en cours
        If Cumul / CumulHT > 0.9 And i <> 1 Then
            '-> Indiquer que l'on analyse autre
            IsAutre = True
            IsAutreGest = True
            
            If TypeOf aObject Is clsFamille Then
                '-> Cr�er la famille Autre
                Set aAutre = New clsFamille
                aAutre.Code = "AUTRE_DEAL"
                aAutre.Libell� = "Autres"
                Familles.Add aAutre, "FAM|AUTRE_DEAL"
            Else
                '-> Cr�er le fournisseur Autre
                Set aAutre = New clsFournisseur
                aAutre.Codaux = "AUTRE_DEAL"
                aAutre.Libell� = "Autres"
                Fournisseurs.Add aAutre, "TIE|DEAL|AUTRE_DEAL"
            End If
        End If
    Else
        '-> On ajoute dans la liste des autres
        If strAutre = "" Then
            strAutre = DefString
        Else
            strAutre = strAutre & "�" & DefString
        End If
        '-> Mettre � jour le cumul des autres
        CumulAutreHT = CumulAutreHT + aObject.MontantHT
        CumulAutreRef = CumulAutreRef + aObject.MontantRef
    End If
    
Next 'Pour toutes les entr�es

End Sub


Public Sub CreateIFIXLS()

'---> Cette proc�dure cr�er le tableau excel IFI.XLS

Dim aFam As clsFamille, aFam2 As clsFamille
Dim aAffect As clsAffectation, aAffect2 As clsAffectation
Dim aAff As clsAffaire
Dim aRange As Range
Dim i As Integer, j As Integer
Dim ListeAffaire As String
Dim Tempo1 As Double
Dim Tempo2 As Double
Dim Tempo3 As Double

On Error GoTo GestError

'-> Mise � jour de l'interface
Form1.Label9(2).Caption = "En cours"
DoEvents

'-> Ajouter une feuille au classeur en cours
Set aSheet = aWorkBook.Worksheets.Add
aSheet.Name = "Tableau IFI"

'-> RAZ des matrices
strTri = ""
strToTri = ""

'-> Tri des familles dans l'ordre d�croissant du Montant HT
For Each aFam In Familles
    If strToTri = "" Then
        strToTri = Format(aFam.MontantHT, "000000000000000.00") & "�" & "FAM|" & aFam.Code
    Else
        strToTri = strToTri & "�" & Format(aFam.MontantHT, "000000000000000.00") & "�" & "FAM|" & aFam.Code
    End If
Next

'-> Trier les familles
strTri = Tri(strToTri, "�", "�")

'-> Gestion des 90%
Call Gestion90

'-> Tri des affaires dans l'ordre alphab�tique
strToTri = ""
For Each aAff In Affaires
    If strToTri = "" Then
        strToTri = aAff.Libelle & "�AFF|" & aAff.Code
    Else
        strToTri = strToTri & "�" & aAff.Libelle & "�AFF|" & aAff.Code
    End If
Next
ListeAffaire = Tri(strToTri, "�", "�")

'************************
'* Libell� des colonnes *
'************************

'-> Se positionner sur la cellule de d�part
Set aRange = aSheet.Range(ColDep & LigneDep)

'-> Libell� FAMILLE
aRange.Value = "Famille"
FormatRange aRange, 0

'-> Libell� KF
Set aRange = aRange.Offset(0, 1)
aRange.Value = "KF"
FormatRange aRange, 0

'-> Libell� %
Set aRange = aRange.Offset(0, 1)
aRange.Value = "%"
FormatRange aRange, 0

'-> Mise � jour de la liste des affaires
For i = 1 To NumEntries(ListeAffaire, "�")
    '-> Pointer sur l'objet affaire
    Set aAff = Affaires(Entry(i, ListeAffaire, "�"))
    '-> Valeur et formattage
    Set aRange = aRange.Offset(0, 1)
    aRange.Value = aAff.Code & " : " & aAff.Libelle
    FormatRange aRange, 0
Next 'Pour toutes les affaires


'-> Libell� Groupe
Set aRange = aRange.Offset(0, 1)
aRange.Value = "GROUPE"
FormatRange aRange, 0

'**********************
'* Lib�ll� des lignes *
'**********************

'-> Se positionner sur la cellule de d�part
Set aRange = aSheet.Range(ColDep & LigneDep)

'-> Liste des familles
For i = 1 To NumEntries(strNonAutre, "�")
    '-> Pointer sur la famille
    Set aFam = Familles(Entry(i, strNonAutre, "�"))
    '-> Valeur et formattage
    Set aRange = aRange.Offset(1, 0)
    aRange.Value = aFam.Code & " : " & aFam.Libell�
    FormatRange aRange, 0
Next

If IsAutreGest Then
    '-> Libelle de AUTRE
    Set aRange = aRange.Offset(1, 0)
    aRange.Value = "AUTRES"
    FormatRange aRange, 0
End If

'-> Libell� Total
Set aRange = aRange.Offset(1, 0)
aRange.Value = "Total"
FormatRange aRange, 0

'-> Libell� Economie (KF)
Set aRange = aRange.Offset(1, 0)
aRange.Value = "Economie (KF)"
FormatRange aRange, 0

'-> Libell� %
Set aRange = aRange.Offset(1, 0)
aRange.Value = "%"
FormatRange aRange, 0

'-> Indice
Set aRange = aRange.Offset(1, 0)
aRange.Value = "Indice"
FormatRange aRange, 0

'*************************************
'* Calcul de la zone 2  , 3 , 1  , 5 *
'*************************************

'-> Se positionner sur la cellule de d�part avec un decallage de : _
1 colonne pour zone KF _
1 colonne pour zone % _
1 colonne pour zone groupe _
1 colonne pour chaque affaire


Set aRange = aSheet.Columns(aSheet.Columns(ColDep).Column + Affaires.Count + 3)
Set aRange = aSheet.Range(Entry(2, aRange.Address, ":") & LigneDep)

'-> Liste des familles
For i = 1 To NumEntries(strNonAutre, "�")
    '-> Init des variables
    Tempo1 = 0
    Tempo2 = 0
    '-> Pointer sur la famille
    Set aFam = Familles(Entry(i, strNonAutre, "�"))
    '-> Calcul de la zone 2
    Set aRange = aRange.Offset(1, -(Affaires.Count + 2))
    aRange.Value = aFam.MontantHT / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    '-> Calcul de la zone 3
    Set aRange = aRange.Offset(0, 1)
    If CumulHT <> 0 Then aRange.Value = aFam.MontantHT / CumulHT
    aRange.NumberFormat = "0.00 %"
    FormatRange aRange, 1
    
    '-> Calcul de la zone 1
    For j = 1 To NumEntries(ListeAffaire, "�")
        '-> D�caler la cellule
        Set aRange = aRange.Offset(0, 1)
        '-> Pointer sur l'affaire sp�cifi�e si elle est renseign�e
        If IsAffectation(aFam, 3, Entry(j, ListeAffaire, "�")) Then
            Set aAffect = aFam.Affaires(Entry(j, ListeAffaire, "�"))
            '-> Mettre � jour la valeur
            If aAffect.MontantRef <> 0 Then aRange.Value = (aAffect.MontantHT / aAffect.MontantRef) * 100
            aRange.NumberFormat = "0.0"
            '-> cumul des valeurs pour la zone groupe
            Tempo1 = Tempo1 + aAffect.MontantHT
            Tempo2 = Tempo2 + aAffect.MontantRef
        End If
        '-> Formattage de la zone
        FormatRange aRange, 1
    Next
    '-> Calcul de la zone 5
    Set aRange = aRange.Offset(0, 1)
    If Tempo2 <> 0 Then aRange.Value = (Tempo1 / Tempo2) * 100
    aRange.NumberFormat = "0.0"
    '-> Formattage de la zone
    FormatRange aRange, 1
Next

If IsAutreGest Then

    '-> Pour autres familles calcul de la zone 2
    Set aRange = aRange.Offset(1, -(Affaires.Count + 2))
    aRange.Value = CumulAutreHT / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> Pour autres familles calcul de la zone 3
    Set aRange = aRange.Offset(0, 1)
    If CumulHT <> 0 Then aRange.Value = CumulAutreHT / CumulHT
    aRange.NumberFormat = "0.00 %"
    FormatRange aRange, 1
End If

'-> Init des cumuls
Tempo1 = 0
Tempo2 = 0

'-> V�rifier que la famille autre existe
If IsObject("FAM", "FAM|AUTRE_DEAL") Then
    '-> Pointer sur la famille Autre
    Set aFam = Familles("FAM|AUTRE_DEAL")
    '-> Ajouter de base toutes les affaires � cette famille
    For j = 1 To NumEntries(ListeAffaire, "�")
        '-> Cr�er un objet d'affectation
        Set aAffect = New clsAffectation
        aAffect.clsType = 3
        aAffect.Key = Entry(j, ListeAffaire, "�")
        aFam.Affaires.Add aAffect, aAffect.Key
    Next 'Pour toutes les affaires
    
    '-> Faire le cumul pour la famille autre
    For i = 1 To NumEntries(strAutre, "�")
        '-> Pointer sur la famille
        Set aFam2 = Familles(Entry(i, strAutre, "�"))
        '-> Faire le cumul pour toutes les affaires de cette famille
        For Each aAffect In aFam2.Affaires
            '-> Pointer sur l'affectation de la famille autre
            Set aAffect2 = aFam.Affaires(aAffect.Key)
            '-> Faire le cumul
            aAffect2.MontantHT = aAffect2.MontantHT + aAffect.MontantHT
            aAffect2.MontantRef = aAffect2.MontantRef + aAffect.MontantRef
        Next
    Next 'Pour toutes les familles de autre
    
    '-> Mise � jour de la zone 1 dans Excel
    For j = 1 To NumEntries(ListeAffaire, "�")
        '-> Pointer sur l'objet d'affectation de la famille autre
        Set aAffect = aFam.Affaires(Entry(j, ListeAffaire, "�"))
        '-> Se d�caller d'une colonne
        Set aRange = aRange.Offset(0, 1)
        '-> Tester si on positionner la valeur
        If aAffect.MontantRef <> 0 Then
            '-> Contenu de la zone 1
            If aAffect.MontantRef <> 0 Then aRange.Value = (aAffect.MontantHT / aAffect.MontantRef) * 100
            aRange.NumberFormat = "0.0"
        End If
        '-> Formattage de la zone
        FormatRange aRange, 1
        '-> Mise � jour des cumuls
        Tempo1 = Tempo1 + aAffect.MontantHT
        Tempo2 = Tempo2 + aAffect.MontantRef
    Next
    
    '-> Mise � jour de la zone 5 total Groupe
    Set aRange = aRange.Offset(0, 1)
    If Tempo2 <> 0 Then
        If Tempo2 <> 0 Then aRange.Value = (Tempo1 / Tempo2) * 100
        aRange.NumberFormat = "0.0"
    End If
    FormatRange aRange, 1
    
End If 'Si la famille autre existe

'**************************
'* Calcul de la zone 2bis *
'**************************
'-> En KF
Set aRange = aSheet.Range(Chr(Asc(ColDep) + 1) & aRange.Row + 1)
aRange.Value = CumulHT / Groupe
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> En %
Set aRange = aRange.Offset(0, 1)
If CumulHT <> 0 Then aRange.Value = CumulHT / CumulHT
aRange.NumberFormat = "0.00 %"
FormatRange aRange, 1

'-> Pour toutes les affaires
For i = 1 To NumEntries(ListeAffaire, "�")
    '-> Pointer sur l'affaire
    Set aAff = Affaires(Entry(i, ListeAffaire, "�"))
    '-> Mettre le cumulo � 0
    Tempo1 = 0
    Tempo2 = 0
    '-> Pour toutes les familles
    For Each aFam In Familles
        '-> Ne pas prendre la famille autre
        If aFam.Code <> "AUTRE_DEAL" Then
            '-> V�rifier que cette affaire est affect�e � cette famille
            If IsAffectation(aFam, 3, "AFF|" & aAff.Code) Then
                '-> Pointer vers l'objet d'affectation
                Set aAffect = aFam.Affaires("AFF|" & aAff.Code)
                '-> Faire le cumul
                Tempo1 = Tempo1 + aAffect.MontantHT
                Tempo2 = Tempo2 + aAffect.MontantRef
                Tempo3 = Tempo3 + aAffect.MontantHT
            End If
        End If 'Si on ne traite pas la famille autre
    Next 'Pour toutes les familles
    
    '-> D�caller la cellule et mise � jour de la zone 8
    Set aRange = aRange.Offset(0, 1)
    aRange.Value = Tempo1 / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> D�caller et mise � jour de la zone 7
    Set aRange = aRange.Offset(1, 0)
    aRange.Value = (-Tempo1 + Tempo2) / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> D�caller et mise � jour de la zone 9
    Set aRange = aRange.Offset(1, 0)
    If CumulHT <> 0 Then aRange.Value = Tempo1 / CumulHT
    aRange.NumberFormat = "0.00 %"
    FormatRange aRange, 1
    
    '-> D�caller et mise � jour de la zone 6
    Set aRange = aRange.Offset(1, 0)
    If Tempo2 <> 0 Then aRange.Value = (Tempo1 / Tempo2) * 100
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> Se repositionner
    Set aRange = aRange.Offset(-3, 0)
    
Next 'Pour toutes les affaires
    
'-> Mettre � jour le cumul groupe pour la zone 8
Set aRange = aRange.Offset(0, 1)
aRange.Value = Tempo3 / Groupe
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> Mettre � jour le cumul groupe pour la zone 7
Set aRange = aRange.Offset(1, 0)
aRange.Value = (-CumulHT + CumulRef) / Groupe
aRange.NumberFormat = "0.0"
FormatRange aRange, 1


'-> Mettre � jour le cumul groupe pour la zone 9
Set aRange = aRange.Offset(1, 0)
If CumulHT <> 0 Then aRange.Value = CumulHT / CumulHT
aRange.NumberFormat = "0.00 %"
FormatRange aRange, 1

'-> Mettre � jour le cumul groupe pour la zone 6
Set aRange = aRange.Offset(1, 0)
If CumulRef <> 0 Then aRange.Value = (CumulHT / CumulRef) * 100
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> Se repositionner au d�but
Set aRange = aSheet.Range(Chr(Asc(ColDep) + 1) & aRange.Row - 2)

'-> Fermeture du tableau
For i = 1 To 3
    '-> Colonne KF
    FormatRange aRange, 1
    '-> Colonne %
    Set aRange = aRange.Offset(0, 1)
    FormatRange aRange, 1
    '-> Se d�caller
    Set aRange = aRange.Offset(1, -1)
Next


'-> Largeur automatique des colonnes
Set aRange = aSheet.Columns(aSheet.Columns(ColDep).Column + Affaires.Count + 3)
ExcelApp.Columns(ColDep & ":" & Entry(2, aRange.Address, ":")).EntireColumn.AutoFit

'-> Supprimer la famille autre
If IsObject("FAM", "FAM|AUTRE_DEAL") Then Familles.Remove ("FAM|AUTRE_DEAL")

'-> Mise � jour de l'interface
Form1.Label9(2).Caption = "Termin�"
DoEvents

Exit Sub

GestError:

    '-> Mise � jour de l'interface
    Form1.Label9(2).Caption = "Erreur"
    DoEvents
    
End Sub


Public Sub CreateIFOFIXLS()

'---> Cette proc�dure cr�er le tableau excel IFOFI

Dim aFrs As clsFournisseur, aFrs2 As clsFournisseur
Dim aAffect As clsAffectation, aAffect2 As clsAffectation
Dim aAff As clsAffaire
Dim aRange As Range
Dim i As Integer, j As Integer
Dim ListeAffaire As String
Dim Tempo1 As Double
Dim Tempo2 As Double
Dim Tempo3 As Double

On Error GoTo GestError

'-> Mise � jour de l'interface
Form1.Label9(3).Caption = "En cours"
DoEvents

'-> Ajouter une feuille au classeur en cours
Set aSheet = aWorkBook.Worksheets.Add
aSheet.Name = "Tableau IFOFI"

'-> Remise � jour des variables
strTri = ""
strToTri = ""

'-> Tri des fournisseurs dans l'ordre d�croissant du Montant HT
For Each aFrs In Fournisseurs
    If strToTri = "" Then
        strToTri = Format(aFrs.MontantHT, "000000000000000.00") & "�" & "TIE|" & aFrs.Typaux & "|" & aFrs.Codaux
    Else
        strToTri = strToTri & "�" & Format(aFrs.MontantHT, "000000000000000.00") & "�" & "TIE|" & aFrs.Typaux & "|" & aFrs.Codaux
    End If
Next

'-> Trier les fournisseurs
strTri = Tri(strToTri, "�", "�")

'-> Gestion des 90%
Call Gestion90

'-> Tri des affaires dans l'ordre alphab�tique
strToTri = ""
For Each aAff In Affaires
    If strToTri = "" Then
        strToTri = aAff.Libelle & "�AFF|" & aAff.Code
    Else
        strToTri = strToTri & "�" & aAff.Libelle & "�AFF|" & aAff.Code
    End If
Next
ListeAffaire = Tri(strToTri, "�", "�")

'************************
'* Libell� des colonnes *
'************************

'-> Se positionner sur la cellule de d�part
Set aRange = aSheet.Range(ColDep & LigneDep)

'-> Libell� Code Auxiliaire
Set aRange = aRange.Offset(0, -1)
aRange.Value = "Type Tiers"
FormatRange aRange, 0

'-> Libell� FAMILLE
Set aRange = aRange.Offset(0, 1)
aRange.Value = "Code Fournisseur"
FormatRange aRange, 0

'-> Libell� KF
Set aRange = aRange.Offset(0, 1)
aRange.Value = "KF"
FormatRange aRange, 0

'-> Libell� %
Set aRange = aRange.Offset(0, 1)
aRange.Value = "%"
FormatRange aRange, 0

'-> Mise � jour de la liste des affaires
For i = 1 To NumEntries(ListeAffaire, "�")
    '-> Pointer sur l'objet affaire
    Set aAff = Affaires(Entry(i, ListeAffaire, "�"))
    '-> Valeur et formattage
    Set aRange = aRange.Offset(0, 1)
    aRange.Value = aAff.Code & " : " & aAff.Libelle
    FormatRange aRange, 0
Next 'Pour toutes les affaires

'-> Libell� Groupe
Set aRange = aRange.Offset(0, 1)
aRange.Value = "GROUPE"
FormatRange aRange, 0

'**********************
'* Lib�ll� des lignes *
'**********************

'-> Se positionner sur la cellule de d�part
Set aRange = aSheet.Range(ColDep & LigneDep)

'-> Liste des fournisseurs
For i = 1 To NumEntries(strNonAutre, "�")
    '-> Pointer sur la famille
    Set aFrs = Fournisseurs(Entry(i, strNonAutre, "�"))
    '-> Valeur et formattage
    Set aRange = aRange.Offset(1, -1)
    aRange.Value = aFrs.Typaux
    FormatRange aRange, 0
    '-> Libell�
    Set aRange = aRange.Offset(0, 1)
    aRange.Value = aFrs.Codaux & " " & aFrs.Libell�
    FormatRange aRange, 0
Next

If IsAutreGest Then
    '-> Libelle de AUTRE
    Set aRange = aRange.Offset(1, 0)
    aRange.Value = "AUTRES"
    FormatRange aRange, 0
End If


'-> Libell� Total
Set aRange = aRange.Offset(1, 0)
aRange.Value = "Total"
FormatRange aRange, 0

'-> Libell� Economie (KF)
Set aRange = aRange.Offset(1, 0)
aRange.Value = "Economie (KF)"
FormatRange aRange, 0

'-> Libell� %
Set aRange = aRange.Offset(1, 0)
aRange.Value = "%"
FormatRange aRange, 0

'-> Indice
Set aRange = aRange.Offset(1, 0)
aRange.Value = "Indice"
FormatRange aRange, 0

'*************************************
'* Calcul de la zone 2  , 3 , 1  , 5 *
'*************************************

'-> Se positionner sur la cellule de d�part avec un decallage de : _
1 colonne pour zone KF _
1 colonne pour zone % _
1 colonne pour zone groupe _
1 colonne pour chaque affaire
Set aRange = aSheet.Columns(aSheet.Columns(ColDep).Column + Affaires.Count + 3)
Set aRange = aSheet.Range(Entry(2, aRange.Address, ":") & LigneDep)

'-> Liste des fournisseurs
For i = 1 To NumEntries(strNonAutre, "�")
    '-> Init des variables
    Tempo1 = 0
    Tempo2 = 0
    '-> Pointer sur la famille
    Set aFrs = Fournisseurs(Entry(i, strNonAutre, "�"))
    '-> Calcul de la zone 2
    Set aRange = aRange.Offset(1, -(Affaires.Count + 2))
    aRange.Value = aFrs.MontantHT / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    '-> Calcul de la zone 3
    Set aRange = aRange.Offset(0, 1)
    If CumulHT <> 0 Then aRange.Value = aFrs.MontantHT / CumulHT
    aRange.NumberFormat = "0.00 %"
    FormatRange aRange, 1
    
    '-> Calcul de la zone 1
    For j = 1 To NumEntries(ListeAffaire, "�")
        '-> D�caler la cellule
        Set aRange = aRange.Offset(0, 1)
        '-> Pointer sur l'affaire sp�cifi�e si elle est renseign�e
        If IsAffectation(aFrs, 3, Entry(j, ListeAffaire, "�")) Then
            Set aAffect = aFrs.Affaires(Entry(j, ListeAffaire, "�"))
            '-> Mettre � jour la valeur
            If aAffect.MontantRef <> 0 Then aRange.Value = (aAffect.MontantHT / aAffect.MontantRef) * 100
            aRange.NumberFormat = "0.0"
            '-> cumul des valeurs pour la zone groupe
            Tempo1 = Tempo1 + aAffect.MontantHT
            Tempo2 = Tempo2 + aAffect.MontantRef
        End If
        '-> Formattage de la zone
        FormatRange aRange, 1
    Next
    '-> Calcul de la zone 5
    Set aRange = aRange.Offset(0, 1)
    If Tempo2 <> 0 Then aRange.Value = (Tempo1 / Tempo2) * 100
    aRange.NumberFormat = "0.0"
    '-> Formattage de la zone
    FormatRange aRange, 1
Next

If IsAutreGest Then

    '-> Pour autres fournisseurs calcul de la zone 2
    Set aRange = aRange.Offset(1, -(Affaires.Count + 2))
    aRange.Value = CumulAutreHT / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> Pour autres fournisseurs calcul de la zone 3
    Set aRange = aRange.Offset(0, 1)
    If CumulHT <> 0 Then aRange.Value = CumulAutreHT / CumulHT
    aRange.NumberFormat = "0.00 %"
    FormatRange aRange, 1

End If

'-> Init des cumuls
Tempo1 = 0
Tempo2 = 0

'-> V�rifier que le fournisseur autre existe
If IsObject("FRS", "TIE|DEAL|AUTRE_DEAL") Then
    '-> Pointer sur le fournisseur Autre
    Set aFrs = Fournisseurs("TIE|DEAL|AUTRE_DEAL")
    '-> Ajouter de base toutes les affaires � ce fournisseur
    For j = 1 To NumEntries(ListeAffaire, "�")
        '-> Cr�er un objet d'affectation
        Set aAffect = New clsAffectation
        aAffect.clsType = 3
        aAffect.Key = Entry(j, ListeAffaire, "�")
        aFrs.Affaires.Add aAffect, aAffect.Key
    Next 'Pour toutes les affaires
    
    '-> Faire le cumul pour le fournisseur autre
    For i = 1 To NumEntries(strAutre, "�")
        '-> Pointer sur le fournisseur
        Set aFrs2 = Fournisseurs(Entry(i, strAutre, "�"))
        '-> Faire le cumul pour toutes les affaires de ce fournisseur
        For Each aAffect In aFrs2.Affaires
            '-> Pointer sur l'affectation du fournisseur autre
            Set aAffect2 = aFrs.Affaires(aAffect.Key)
            '-> Faire le cumul
            aAffect2.MontantHT = aAffect2.MontantHT + aAffect.MontantHT
            aAffect2.MontantRef = aAffect2.MontantRef + aAffect.MontantRef
        Next
    Next 'Pour toutes les fournisseurs de autre
    
    '-> Mise � jour de la zone 1 dans Excel
    For j = 1 To NumEntries(ListeAffaire, "�")
        '-> Pointer sur l'aobjet d'affectation du fournisseur autre
        Set aAffect = aFrs.Affaires(Entry(j, ListeAffaire, "�"))
        '-> Se d�caller d'une colonne
        Set aRange = aRange.Offset(0, 1)
        '-> Tester si on positionner la valeur
        If aAffect.MontantRef <> 0 Then
            '-> Contenu de la zone 1
            If aAffect.MontantRef <> 0 Then aRange.Value = (aAffect.MontantHT / aAffect.MontantRef) * 100
            aRange.NumberFormat = "0.0"
        End If
        '-> Formattage de la zone
        FormatRange aRange, 1
        '-> Mise � jour des cumuls
        Tempo1 = Tempo1 + aAffect.MontantHT
        Tempo2 = Tempo2 + aAffect.MontantRef
    Next
    
    '-> Mise � jour de la zone 5 total Groupe
    Set aRange = aRange.Offset(0, 1)
    If Tempo2 <> 0 Then
        If Tempo2 <> 0 Then aRange.Value = (Tempo1 / Tempo2) * 100
        aRange.NumberFormat = "0.0"
    End If
    FormatRange aRange, 1
    
End If 'Si le frs autre existe

'**************************
'* Calcul de la zone 2bis *
'**************************
'-> En KF
Set aRange = aSheet.Range(Chr(Asc(ColDep) + 1) & aRange.Row + 1)
aRange.Value = CumulHT / Groupe
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> En %
Set aRange = aRange.Offset(0, 1)
If CumulHT <> 0 Then aRange.Value = CumulHT / CumulHT
aRange.NumberFormat = "0.00 %"
FormatRange aRange, 1

'-> Pour toutes les affaires
For i = 1 To NumEntries(ListeAffaire, "�")
    '-> Pointer sur l'affaire
    Set aAff = Affaires(Entry(i, ListeAffaire, "�"))
    '-> Mettre le cumulo � 0
    Tempo1 = 0
    Tempo2 = 0
    '-> Pour toutes les fournisseurs
    For Each aFrs In Fournisseurs
        '-> Ne pas prendre la famille autre
        If aFrs.Codaux <> "AUTRE_DEAL" Then
            '-> V�rifier que cette affaire est affect�e � ce fournisseur
            If IsAffectation(aFrs, 3, "AFF|" & aAff.Code) Then
                '-> Pointer vers l'objet d'affectation
                Set aAffect = aFrs.Affaires("AFF|" & aAff.Code)
                '-> Faire le cumul
                Tempo1 = Tempo1 + aAffect.MontantHT
                Tempo2 = Tempo2 + aAffect.MontantRef
                Tempo3 = Tempo3 + aAffect.MontantHT
            End If
        End If 'Si on ne traite pas le fournisseur autre
    Next 'Pour tous les fournisseurs
    
    '-> D�caller la cellule et mise � jour de la zone 8
    Set aRange = aRange.Offset(0, 1)
    aRange.Value = Tempo1 / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> D�caller et mise � jour de la zone 7
    Set aRange = aRange.Offset(1, 0)
    aRange.Value = (-Tempo1 + Tempo2) / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> D�caller et mise � jour de la zone 9
    Set aRange = aRange.Offset(1, 0)
    If CumulHT <> 0 Then aRange.Value = Tempo1 / CumulHT
    aRange.NumberFormat = "0.00 %"
    FormatRange aRange, 1
    
    '-> D�caller et mise � jour de la zone 6
    Set aRange = aRange.Offset(1, 0)
    If Tempo2 <> 0 Then aRange.Value = (Tempo1 / Tempo2) * 100
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> Se repositionner
    Set aRange = aRange.Offset(-3, 0)
    
Next 'Pour toutes les affaires
    
'-> Mettre � jour le cumul groupe pour la zone 8
Set aRange = aRange.Offset(0, 1)
aRange.Value = Tempo3 / Groupe
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> Mettre � jour le cumul groupe pour la zone 7
Set aRange = aRange.Offset(1, 0)
aRange.Value = (-CumulHT + CumulRef) / Groupe
aRange.NumberFormat = "0.0"
FormatRange aRange, 1


'-> Mettre � jour le cumul groupe pour la zone 9
Set aRange = aRange.Offset(1, 0)
If CumulHT <> 0 Then aRange.Value = CumulHT / CumulHT
aRange.NumberFormat = "0.00 %"
FormatRange aRange, 1

'-> Mettre � jour le cumul groupe pour la zone 6
Set aRange = aRange.Offset(1, 0)
If CumulRef <> 0 Then aRange.Value = (CumulHT / CumulRef) * 100
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> Se repositionner au d�but
Set aRange = aSheet.Range(Chr(Asc(ColDep) + 1) & aRange.Row - 2)

'-> Fermeture du tableau
For i = 1 To 3
    '-> Colonne KF
    FormatRange aRange, 1
    '-> Colonne %
    Set aRange = aRange.Offset(0, 1)
    FormatRange aRange, 1
    '-> Se d�caller
    Set aRange = aRange.Offset(1, -1)
Next


'-> Largeur automatique des colonnes
Set aRange = aSheet.Columns(aSheet.Columns(ColDep).Column + Affaires.Count + 3)
ExcelApp.Columns(ColDep & ":" & Entry(2, aRange.Address, ":")).EntireColumn.AutoFit

'-> Supprimer le fournisseur autre
If IsObject("TIE", "TIE|DEAL|AUTRE_DEAL") Then Fournisseurs.Remove ("TIE|DEAL|AUTRE_DEAL")

'-> Mise � jour de l'interface
Form1.Label9(3).Caption = "Termin�"
DoEvents

Exit Sub

GestError:

    '-> Mise � jour de l'interface
    Form1.Label9(3).Caption = "Erreur"
    DoEvents
    
End Sub

Public Sub CreateFOFAINDXLS()

'---> Cette proc�dure cr�er le tableau excel FOFAIND

Dim aFrs As clsFournisseur, aFrs2 As clsFournisseur
Dim aAffect As clsAffectation, aAffect2 As clsAffectation
Dim aFam As clsFamille
Dim aRange As Range
Dim i As Integer, j As Integer
Dim ListeFamille As String
Dim Tempo1 As Double
Dim Tempo2 As Double
Dim Tempo3 As Double

On Error GoTo GestError

'-> Mise � jour de l'interface
Form1.Label9(4).Caption = "En cours"
DoEvents

'-> Ajouter une feuille au classeur en cours
Set aSheet = aWorkBook.Worksheets.Add
aSheet.Name = "Tableau FOFAIND"

'-> Remise � jour des variables
strTri = ""
strToTri = ""

'-> Tri des fournisseurs dans l'ordre d�croissant du Montant HT
For Each aFrs In Fournisseurs
    If strToTri = "" Then
        strToTri = Format(aFrs.MontantHT, "000000000000000.00") & "�" & "TIE|" & aFrs.Typaux & "|" & aFrs.Codaux
    Else
        strToTri = strToTri & "�" & Format(aFrs.MontantHT, "000000000000000.00") & "�" & "TIE|" & aFrs.Typaux & "|" & aFrs.Codaux
    End If
Next

'-> Trier les fournisseurs
strTri = Tri(strToTri, "�", "�")

'-> Gestion des 90%
Call Gestion90

'-> Tri des familles dans l'ordre alphab�tique
strToTri = ""
For Each aFam In Familles
    If strToTri = "" Then
        strToTri = aFam.Libell� & "�FAM|" & aFam.Code
    Else
        strToTri = strToTri & "�" & aFam.Libell� & "�FAM|" & aFam.Code
    End If
Next
ListeFamille = Tri(strToTri, "�", "�")

'************************
'* Libell� des colonnes *
'************************

'-> Se positionner sur la cellule de d�part
Set aRange = aSheet.Range(ColDep & LigneDep)

'-> Libell� Code Auxiliaire
Set aRange = aRange.Offset(0, -1)
aRange.Value = "Type Tiers"
FormatRange aRange, 0

'-> Libell� FAMILLE
Set aRange = aRange.Offset(0, 1)
aRange.Value = "Code Fournisseur"
FormatRange aRange, 0

'-> Libell� KF
Set aRange = aRange.Offset(0, 1)
aRange.Value = "KF"
FormatRange aRange, 0

'-> Libell� %
Set aRange = aRange.Offset(0, 1)
aRange.Value = "%"
FormatRange aRange, 0

'-> Mise � jour de la liste des familles
For i = 1 To NumEntries(ListeFamille, "�")
    '-> Pointer sur l'objet famille
    Set aFam = Familles(Entry(i, ListeFamille, "�"))
    '-> Valeur et formattage
    Set aRange = aRange.Offset(0, 1)
    aRange.Value = aFam.Code & " : " & aFam.Libell�
    FormatRange aRange, 0
Next 'Pour toutes les familles

'-> Libell� Groupe
Set aRange = aRange.Offset(0, 1)
aRange.Value = "GROUPE"
FormatRange aRange, 0

'**********************
'* Lib�ll� des lignes *
'**********************

'-> Se positionner sur la cellule de d�part
Set aRange = aSheet.Range(ColDep & LigneDep)

'-> Liste des fournisseurs
For i = 1 To NumEntries(strNonAutre, "�")
    '-> Pointer sur la famille
    Set aFrs = Fournisseurs(Entry(i, strNonAutre, "�"))
    '-> Valeur et formattage
    Set aRange = aRange.Offset(1, -1)
    aRange.Value = aFrs.Typaux
    FormatRange aRange, 0
    '-> Libell�
    Set aRange = aRange.Offset(0, 1)
    aRange.Value = aFrs.Codaux & " " & aFrs.Libell�
    FormatRange aRange, 0
Next

If IsAutreGest Then

    '-> Libelle de AUTRE
    Set aRange = aRange.Offset(1, 0)
    aRange.Value = "AUTRES"
    FormatRange aRange, 0

End If

'-> Libell� Total
Set aRange = aRange.Offset(1, 0)
aRange.Value = "Total"
FormatRange aRange, 0

'-> Libell� Economie (KF)
Set aRange = aRange.Offset(1, 0)
aRange.Value = "Economie (KF)"
FormatRange aRange, 0

'-> Libell� %
Set aRange = aRange.Offset(1, 0)
aRange.Value = "%"
FormatRange aRange, 0

'-> Indice
Set aRange = aRange.Offset(1, 0)
aRange.Value = "Indice"
FormatRange aRange, 0

'*************************************
'* Calcul de la zone 2  , 3 , 1  , 5 *
'*************************************

'-> Se positionner sur la cellule de d�part avec un decallage de : _
1 colonne pour zone KF _
1 colonne pour zone % _
1 colonne pour zone groupe _
1 colonne pour chaque famille
Set aRange = aSheet.Columns(aSheet.Columns(ColDep).Column + Familles.Count + 3)
Set aRange = aSheet.Range(Entry(2, aRange.Address, ":") & LigneDep)

'-> Liste des fournisseurs
For i = 1 To NumEntries(strNonAutre, "�")
    '-> Init des variables
    Tempo1 = 0
    Tempo2 = 0
    '-> Pointer sur la famille
    Set aFrs = Fournisseurs(Entry(i, strNonAutre, "�"))
    '-> Calcul de la zone 2
    Set aRange = aRange.Offset(1, -(Familles.Count + 2))
    aRange.Value = aFrs.MontantHT / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    '-> Calcul de la zone 3
    Set aRange = aRange.Offset(0, 1)
    If CumulHT <> 0 Then aRange.Value = aFrs.MontantHT / CumulHT
    aRange.NumberFormat = "0.00 %"
    FormatRange aRange, 1
    
    '-> Calcul de la zone 1
    For j = 1 To NumEntries(ListeFamille, "�")
        '-> D�caler la cellule
        Set aRange = aRange.Offset(0, 1)
        '-> Pointer sur la famille sp�cifi�e si elle est renseign�e
        If IsAffectation(aFrs, 0, Entry(j, ListeFamille, "�")) Then
            Set aAffect = aFrs.Familles(Entry(j, ListeFamille, "�"))
            '-> Mettre � jour la valeur
            If aAffect.MontantRef <> 0 Then aRange.Value = (aAffect.MontantHT / aAffect.MontantRef) * 100
            aRange.NumberFormat = "0.0"
            '-> cumul des valeurs pour la zone groupe
            Tempo1 = Tempo1 + aAffect.MontantHT
            Tempo2 = Tempo2 + aAffect.MontantRef
        End If
        '-> Formattage de la zone
        FormatRange aRange, 1
    Next
    '-> Calcul de la zone 5
    Set aRange = aRange.Offset(0, 1)
    If Tempo2 <> 0 Then aRange.Value = (Tempo1 / Tempo2) * 100
    aRange.NumberFormat = "0.0"
    '-> Formattage de la zone
    FormatRange aRange, 1
Next

If IsAutreGest Then

    '-> Pour autres fournisseurs calcul de la zone 2
    Set aRange = aRange.Offset(1, -(Familles.Count + 2))
    aRange.Value = CumulAutreHT / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> Pour autres fournisseurs calcul de la zone 3
    Set aRange = aRange.Offset(0, 1)
    If CumulHT <> 0 Then aRange.Value = CumulAutreHT / CumulHT
    aRange.NumberFormat = "0.00 %"
    FormatRange aRange, 1

End If

'-> Init des cumuls
Tempo1 = 0
Tempo2 = 0

'-> V�rifier que le fournisseur autre existe
If IsObject("FRS", "TIE|DEAL|AUTRE_DEAL") Then
    '-> Pointer sur le fournisseur Autre
    Set aFrs = Fournisseurs("TIE|DEAL|AUTRE_DEAL")
    '-> Ajouter de base toutes les familles � ce fournisseur
    For j = 1 To NumEntries(ListeFamille, "�")
        '-> Cr�er un objet d'affectation
        Set aAffect = New clsAffectation
        aAffect.clsType = 0
        aAffect.Key = Entry(j, ListeFamille, "�")
        aFrs.Familles.Add aAffect, aAffect.Key
    Next 'Pour toutes les familles
    
    '-> Faire le cumul pour le fournisseur autre
    For i = 1 To NumEntries(strAutre, "�")
        '-> Pointer sur le fournisseur
        Set aFrs2 = Fournisseurs(Entry(i, strAutre, "�"))
        '-> Faire le cumul pour toutes les familles de ce fournisseur
        For Each aAffect In aFrs2.Familles
            '-> Pointer sur l'affectation du fournisseur autre
            Set aAffect2 = aFrs.Familles(aAffect.Key)
            '-> Faire le cumul
            aAffect2.MontantHT = aAffect2.MontantHT + aAffect.MontantHT
            aAffect2.MontantRef = aAffect2.MontantRef + aAffect.MontantRef
        Next
    Next 'Pour toutes les fournisseurs de autre
    
    '-> Mise � jour de la zone 1 dans Excel
    For j = 1 To NumEntries(ListeFamille, "�")
        '-> Pointer sur l'aobjet d'affectation du fournisseur autre
        Set aAffect = aFrs.Familles(Entry(j, ListeFamille, "�"))
        '-> Se d�caller d'une colonne
        Set aRange = aRange.Offset(0, 1)
        '-> Tester si on positionner la valeur
        If aAffect.MontantRef <> 0 Then
            '-> Contenu de la zone 1
            If aAffect.MontantRef <> 0 Then aRange.Value = (aAffect.MontantHT / aAffect.MontantRef) * 100
            aRange.NumberFormat = "0.0"
        End If
        '-> Formattage de la zone
        FormatRange aRange, 1
        '-> Mise � jour des cumuls
        Tempo1 = Tempo1 + aAffect.MontantHT
        Tempo2 = Tempo2 + aAffect.MontantRef
    Next
    
    '-> Mise � jour de la zone 5 total Groupe
    Set aRange = aRange.Offset(0, 1)
    If Tempo2 <> 0 Then
        If Tempo2 <> 0 Then aRange.Value = (Tempo1 / Tempo2) * 100
        aRange.NumberFormat = "0.0"
    End If
    FormatRange aRange, 1
    
End If 'Si le frs autre existe

'**************************
'* Calcul de la zone 2bis *
'**************************
'-> En KF
Set aRange = aSheet.Range(Chr(Asc(ColDep) + 1) & aRange.Row + 1)
aRange.Value = CumulHT / Groupe
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> En %
Set aRange = aRange.Offset(0, 1)
If CumulHT <> 0 Then aRange.Value = CumulHT / CumulHT
aRange.NumberFormat = "0.00 %"
FormatRange aRange, 1

'-> Pour toutes les familles
For i = 1 To NumEntries(ListeFamille, "�")
    '-> Pointer sur la famille
    Set aFam = Familles(Entry(i, ListeFamille, "�"))
    '-> Mettre le cumulo � 0
    Tempo1 = 0
    Tempo2 = 0
    '-> Pour toutes les fournisseurs
    For Each aFrs In Fournisseurs
        '-> Ne pas prendre la famille autre
        If aFrs.Codaux <> "AUTRE_DEAL" Then
            '-> V�rifier que cette famille est affect�e � ce fournisseur
            If IsAffectation(aFrs, 0, "FAM|" & aFam.Code) Then
                '-> Pointer vers l'objet d'affectation
                Set aAffect = aFrs.Familles("FAM|" & aFam.Code)
                '-> Faire le cumul
                Tempo1 = Tempo1 + aAffect.MontantHT
                Tempo2 = Tempo2 + aAffect.MontantRef
                Tempo3 = Tempo3 + aAffect.MontantHT
            End If
        End If 'Si on ne traite pas le fournisseur autre
    Next 'Pour tous les fournisseurs
    
    '-> D�caller la cellule et mise � jour de la zone 8
    Set aRange = aRange.Offset(0, 1)
    aRange.Value = Tempo1 / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> D�caller et mise � jour de la zone 7
    Set aRange = aRange.Offset(1, 0)
    aRange.Value = (-Tempo1 + Tempo2) / Groupe
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> D�caller et mise � jour de la zone 9
    Set aRange = aRange.Offset(1, 0)
    If CumulHT <> 0 Then aRange.Value = Tempo1 / CumulHT
    aRange.NumberFormat = "0.00 %"
    FormatRange aRange, 1
    
    '-> D�caller et mise � jour de la zone 6
    Set aRange = aRange.Offset(1, 0)
    If Tempo2 <> 0 Then aRange.Value = (Tempo1 / Tempo2) * 100
    aRange.NumberFormat = "0.0"
    FormatRange aRange, 1
    
    '-> Se repositionner
    Set aRange = aRange.Offset(-3, 0)
    
Next 'Pour toutes les familles
    
'-> Mettre � jour le cumul groupe pour la zone 8
Set aRange = aRange.Offset(0, 1)
aRange.Value = Tempo3 / Groupe
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> Mettre � jour le cumul groupe pour la zone 7
Set aRange = aRange.Offset(1, 0)
aRange.Value = (-CumulHT + CumulRef) / Groupe
aRange.NumberFormat = "0.0"
FormatRange aRange, 1


'-> Mettre � jour le cumul groupe pour la zone 9
Set aRange = aRange.Offset(1, 0)
If CumulHT <> 0 Then aRange.Value = CumulHT / CumulHT
aRange.NumberFormat = "0.00 %"
FormatRange aRange, 1

'-> Mettre � jour le cumul groupe pour la zone 6
Set aRange = aRange.Offset(1, 0)
If CumulRef <> 0 Then aRange.Value = (CumulHT / CumulRef) * 100
aRange.NumberFormat = "0.0"
FormatRange aRange, 1

'-> Se repositionner au d�but
Set aRange = aSheet.Range(Chr(Asc(ColDep) + 1) & aRange.Row - 2)

'-> Fermeture du tableau
For i = 1 To 3
    '-> Colonne KF
    FormatRange aRange, 1
    '-> Colonne %
    Set aRange = aRange.Offset(0, 1)
    FormatRange aRange, 1
    '-> Se d�caller
    Set aRange = aRange.Offset(1, -1)
Next


'-> Largeur automatique des colonnes
Set aRange = aSheet.Columns(aSheet.Columns(ColDep).Column + Familles.Count + 3)
ExcelApp.Columns(ColDep & ":" & Entry(2, aRange.Address, ":")).EntireColumn.AutoFit


'-> Supprimer le fournisseur autre
If IsObject("TIE", "TIE|DEAL|AUTRE_DEAL") Then Fournisseurs.Remove ("TIE|DEAL|AUTRE_DEAL")

'-> Mise � jour de l'interface
Form1.Label9(4).Caption = "Termin�"
DoEvents

Exit Sub

GestError:

    '-> Mise � jour de l'interface
    Form1.Label9(4).Caption = "Erreur"
    DoEvents

End Sub



Public Sub TerminateExcel()

'---> Supprimer du classeur les feuilles en trop

Dim i As Integer

ExcelApp.DisplayAlerts = False

For i = 1 To 3
    aWorkBook.Worksheets("feuil" & i).Delete
Next

ExcelApp.DisplayAlerts = True

End Sub
