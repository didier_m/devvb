VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsFournisseur"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Classe de gestion des fournisseurs

'-> Cl�s d'acc�s � l'instance
' TIE|TYPAUX|CODAUX

'-> Identification du fournisseur
Public Typaux As String
Public Codaux As String
Public Libell� As String

'-> Liste des Filiales pour ce fournisseur
Public Filiales As Collection

'-> Liste des Familles associ�es � ce fournisseur
Public Familles As Collection

'-> Liste des Affaires affect�es au fournisseur
Public Affaires As Collection

'-> Montants cumul�s en ligne
Public MontantHT As Double
Public MontantREF As Double




Private Sub Class_Initialize()

'-> Initialiser les sous-collections
Set Filiales = New Collection
Set Familles = New Collection
Set Affaires = New Collection

End Sub
