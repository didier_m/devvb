VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsFamille"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Cette classe est utilis�e pour le chargement des familles
'-> Traitements des donn�es en Ligne

'-> Cl�s d'acc�s � l'instance
' FAM|Code Famille

Public Libell� As String
Public Code As String

'-> Collection Filiales affect�es � cette famille
Public Filiales As Collection

'-> Montants cumul�s en ligne
Public MontantHT As Double
Public MontantRef As Double

'-> Collection des Affaires affect�es � cette famille
Public Affaires As Collection

Private Sub Class_Initialize()

'-> Initialiser les collections
Set Filiales = New Collection
Set Affaires = New Collection

End Sub
