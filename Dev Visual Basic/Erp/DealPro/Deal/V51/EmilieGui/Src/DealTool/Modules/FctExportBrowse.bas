Attribute VB_Name = "FctExportBrowse"


'-> Liste des vues load�es en m�moire
Public Vues As New Collection

'-> Path de l'applicatif s�lectionn�
Public PathApp As String

'-> Libell� associ� au progiciel
Dim LibelProgiciel As String

'-> Liste des rubriques li�es � l'applicatif s�lectionn�
Public ListeRubrique As String

'-> Mots cl�s du fichier txt de pilotage
Dim strPRO As String
Dim strRUB As String
Dim strCHX As String
Dim strSAV As String
Dim strVUE As String
Dim strOPE As String
Dim strGUI As String
Dim strTIT As String
Dim strREP As String
Dim strIDE As String
Public strLNG As String

'-> Fichier de pilotage � charger
Dim FileToLoad As String

'-> Variables de lecture du fichier
Dim hdlFile As Integer
Dim Ligne As String
Dim LigneEntete As String

'-> Donn�es de la ligne d'entete
Dim DataEntete As String

'-> Vue s�lectionn�e pour afficher le Browse
Public SelectedVue As String

'-> Variable de Lecture de la maquette : Les Objets Suivants sont export�s :

    ' Section de texte ENTETE avec les # suivants
        '^Date= Date de l'�dition
        '^HOUR= Heure de l'�dition
        '^OPE = op�rateur Progress
        '^TIT = Titre de l'�dition
        '^VUE = Vue utilis�e pour l'export
        '^LIBV= Libelle de la vue
        '^RUB = Rubrique de la vue
        '^MAQ = Maquette de r�f�rence
        '^IDE = Code de l'identifiant Progress ( Ex : DEAL )
        '^PRO = Progiciel
        '^PAGE=Page en cours d'impression
        
        
    ' Tableau BROWSE avec les blocks suivants :
        'Entete, Detail, FinTableau
    
    ' Section de texte FIN_PAGE qui sera utilis�e pour les reports de page
        '^REP = Libell� de report de page
        
    ' Ces deux sections de texte sont utilis�es si elles sont pr�sentes dans la maquette

Dim hMaquette As Double 'Hauteur de la maquette
Dim hMargeTop As Double 'MargeTop
Dim IsEntete As Boolean 'Indique si on utilise l'entete
Dim hEntete As Double 'Indique la hauteur de la page
Dim hEnteteTb As Double 'Hauteur de l'entete du tableau
Dim hDetailTb As Double 'Hauteur de la ligne de d�tail
Dim hFinTb As Double 'Hauteur de la ligne de fin de tableau
Dim IsFinPage As Boolean 'Indique si on doit imprimer un saut de page
Dim hFinPage As Double 'Hauteur de la section de fin de page

'-> Variables de calcul de saut de page
Dim LimitteBas As Double 'Indique la limitte en CM jusqu'ou on peut �diter
Dim DepartPage As Double 'Indique le d�but de l'impression pour le calcul des sauts de page
Dim HauteurImprimee As Double 'Indique la hauteur imprimee dans un document

Dim i As Integer

Public Sub CreateExportBrowse(FicSpool As String)

'---> Cette proc�dure exporte Un Browse PROGRESS


Dim FindObj As Boolean

'-> Variables de controle
Dim boolEntete As Boolean

'-> Variable de Path
Dim IniPath As String

'-> Variable pour lecture du TurboMaq.ini
Dim Res As Long
Dim lpbuffer As String

'-> Variable pour fichier temporaire
Dim TempFileName As String

'-> V�rifier l'existence de ficspool
If Dir$(FicSpool, vbNormal) = "" Then
    MsgBox "Impossible de trouver le fichier sp�cifi� : " & Chr(13) & FicSpool, vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

'-> Analyse du fichier pour lecture des param�tres
hdlFile = FreeFile
Open FicSpool For Input As #hdlFile
Do While Not EOF(hdlFile)
    Line Input #hdlFile, Ligne
    If Trim(Ligne) <> "" Then
        If InStr(1, Ligne, "%%") <> 0 Then
            Select Case UCase$(Mid$(Ligne, 1, 7))
                Case "%%GUI%%"
                    strGUI = Trim(Mid$(Ligne, 8, Len(Ligne) - 7))
                Case "%%VUE%%"
                    strVUE = Trim(Mid$(Ligne, 8, Len(Ligne) - 7))
                Case "%%IDE%%"
                    strIDE = Trim(Mid$(Ligne, 8, Len(Ligne) - 7))
                Case "%%PRO%%"
                    strPRO = Trim(Mid$(Ligne, 8, Len(Ligne) - 7))
                Case "%%TIT%%"
                    strTIT = Trim(Mid$(Ligne, 8, Len(Ligne) - 7))
                Case "%%REP%%"
                    strREP = Trim(Mid$(Ligne, 8, Len(Ligne) - 7))
                Case "%%CHX%%"
                    strCHX = Trim(Mid$(Ligne, 8, Len(Ligne) - 7))
                Case "%%RUB%%"
                    strRUB = Trim(Mid$(Ligne, 8, Len(Ligne) - 7))
                Case "%%SAV%%"
                    strSAV = Trim(Mid$(Ligne, 8, Len(Ligne) - 7))
                Case "%%OPE%%"
                    strOPE = Trim(Mid$(Ligne, 8, Len(Ligne) - 7))
                Case "%%LNG%%"
                    strLNG = Trim(Mid$(Ligne, 8, Len(Ligne) - 7))
            End Select 'Selon le mot cl� %%
        Else
            '-> Il faut d�tecter la fin de lecture de l'entete
            If InStr(1, UCase$(Ligne), "{ENTETE}") <> 0 Then
                '-> R�cup�ration de la ligne d'entete
                LigneEntete = Trim(Ligne)
                '-> R�cup�ration des donn�es de l'entete
                DataEntete = Mid$(LigneEntete, 10, Len(LigneEntete) - 10)
                '-> On a trouv� le d�but des donn�es, sortir de la boucle d'analyse
                boolEntete = True
                Exit Do
            End If
        End If 'Si commence par %%
    End If 'Si ligne <> ""
Loop 'Analyse de l'entete du fichier de donn�es

'-> A ce niveau la, boolEntete doit �tre � true
If Not boolEntete Then
    MsgBox "Impossible de trouver le descriptif des donn�es � imprimer. Fin de programme.", vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

'-> Tester le niveau de version de l'export
If Trim(strIDE) = "" Then
OldVersion:
    '-> il n'y a pas d'ident : on est sur un acien export
    '-> Fermer le fichier
    Reset
    '-> Lancer l'ancienne proc�dure
    PrintFichier FicSpool
End If

'-> Controle de coh�rence des informations
If strGUI = "" Or strIDE = "" Or strPRO = "" Then
    MsgBox "Une information de configuration de la maquette est introuvable. Impossible d'utiliser une maquette : cr�ation d'une �dition dynamique.", vbExclamation + vbOKOnly, "Avertissement"
    GoTo OldVersion
End If

'-> Cr�ation du path du fichier ini
If InStr(1, UCase$(App.Path), "EMILIEGUI\SRC") <> 0 Then
    IniPath = "E:\ERP\TURBO"
Else
    CreatePath IniPath
End If
IniPath = IniPath & "\TurboMaq.ini"

'-> Lecture du param�trage pour r�cup�ration des paths
lpbuffer = Space$(300)
Res = GetPrivateProfileString(strPRO, "PATH", "", lpbuffer, Len(lpbuffer), IniPath)
If Res = 0 Then
    MsgBox "Impossible de trouver le path (cle PATH ) associ� � la section : " & strPRO & " dans le fichier d'initialisation : " & Chr(13) & IniPath & Chr(13) & "Cr�ation d'une �dition dynamique.", vbCritical + vbOKOnly, "Avertissement"
    GoTo OldVersion
End If

'-> R�cup�ration et v�rification du path
PathApp = Mid$(lpbuffer, 1, Res)

'-> R�cup�ration des rubriques
lpbuffer = Space$(5000)
Res = GetPrivateProfileString(strPRO, "RUBRIQUES-01", "", lpbuffer, Len(lpbuffer), IniPath)
If Res = 0 Then
    MsgBox "Impossible de trouver la liste des rubriques attach�e au progiciel : " & strPRO & " dabs le fichier : " & Chr(13) & IniPath & Chr(13) & "Impossible d'utiliser une maquette : cr�ation d'une �dition dynamique.", vbCritical + vbOKOnly, "Avertissement"
    GoTo OldVersion
End If
'-> cr�er la liste des rubriques
ListeRubrique = Mid$(lpbuffer, 1, Res)

'-> R�cup�ration du libelle
lpbuffer = Space$(500)
Res = GetPrivateProfileString(strPRO, "LIBEL", "", lpbuffer, Len(lpbuffer), IniPath)
If Res <> 0 Then LibelProgiciel = Mid$(lpbuffer, 1, Res)

'-> Donner une valeur aux diff�rents noms logiques ( $LECTEUR$) et cr�ation du path des maquettes
GetPathMotCles
PathIdent = strIDE
ReplacePathMotCles PathApp

'-> Initialiser la recherche � FALSE
FindObj = False

'-> V�rifier s'il faut afficher ou non le menu
For i = 1 To NumEntries(ListeRubrique, "|")
    If UCase$(Entry(1, Entry(i, ListeRubrique, "|"), "@")) = UCase$(strRUB) Then
        FindObj = True
        Exit For
    End If
Next

'-> Tester si on a trouv� la rubrique ou non
If Not FindObj Then
    '-> La rubrique n'est pas sp�cifi�e donc v�rifier si on peut modifier les vues ou non
    If UCase$(strCHX) <> "OUI" Then
        '-> On ne peut modifier la vue donc partir sur une edition dynamique
        MsgBox "Rubrique d'impression introuvable. Cr�ation d'une �dition dynamique.", vbExclamation + vbOKOnly, "Avertissement"
        GoTo OldVersion
    Else
        '-> Il a le droit de modifier donc afficher le choix du menu
        Call CreateMenu
    End If
Else
    '-> La rubrique est trouv�e : Voir si la vue existe bien
    If strVUE = "" Or Dir$(PathApp & strVUE, vbNormal) = "" Then
        If UCase$(strCHX) <> "OUI" Then
            MsgBox "Impossible de trouver la vue : " & strVUE & " dans le r�pertoire : " & PathApp & Chr(13) & "Cr�ation d'une �dition dynamique.", vbExclamation + vbOKOnly, "Avertissement"
            GoTo OldVersion
        Else
            '-> On peut modifier le choix de la vue donc afficher le menu
            Call CreateMenu
        End If
    Else
        '-> Si l'op�rateur a le choix , lui donner le choix de la vue
        If UCase$(strCHX) = "OUI" Then
            '-> Afficher le menu
            CreateMenu
        Else
            '-> Affecter la vue s�lectionn�e
            SelectedVue = strVUE
        End If
    End If
End If

'-> Cr�ation du document en utilisant la vue
If Not CreateEditionBrowse Then GoTo OldVersion

'-> Suppression du fichier de donn�es du browse de PROGRESS
If Trim(FicSpool) <> "" Then
    If Dir$(FicSpool, vbNormal) <> "" Then Kill FicSpool
End If

End Sub
Private Function CreateEditionBrowse() As Boolean

'---> Cette proc�dure cr�er le document d�finitif

'-> Pour fichier de donn�es temporaire
Dim TempFileName As String
Dim aVue As clsVue
Dim hdlData As Integer
Dim aLigne As String
Dim PageEnCours As Integer

'-> Variables de lecture du fichier pilote
Dim NomBlock As String
Dim DataBlock As String

'-> Variable d'�criture des donn�es de la ligne d�tail
Dim strDetail As String

'-> Intercepter les erreurs
On Error GoTo ErrorBrowse

'-> Pointer sur la vue de r�f�rence
Set aVue = Vues(SelectedVue)

'-> Initialiser le compteur de page
PageEnCours = 1

'-> Il faut dans un premier temps r�cup�rer les informations de la maquette
If Not GetMaqDimensions(aVue) Then GoTo ErrorBrowse
  
'-> Obtenir un nom de fichier temporaire pour le fichier de donn�es et l'ouvrir
TempFileName = GetTempFileNameVB("BRW")
hdlData = FreeFile
Open TempFileName For Output As #hdlData

'-> Imprimer le nom de la maquette
Print #hdlData, "%%GUI%%" & PathApp & aVue.Maquette

'-> Imprimer l'entete
PrintEntete aVue, PageEnCours, hdlData

'-> Lecture en boucle de la ligne
Do While Not EOF(hdlFile)
    '-> Lire la ligne suivante du fichier pilote
    Line Input #hdlFile, Ligne
    If Trim(Ligne) <> "" Then
        '-> R�cup�ration des donn�es
        NomBlock = Mid$(Entry(1, Ligne, "}"), 2, Len(Entry(1, Ligne, "}")))
        DataBlock = Mid$(Entry(3, Ligne, "{"), 1, Len(Entry(3, Ligne, "{")) - 1)
        '-> Tester si on peut imprimer la ligne de d�tail
        If HauteurImprimee + hDetailTb > LimitteBas Then
            '-> Imprimer la ligne de fin de tableau
            Print #hdlData, "[TB-BROWSE(BLK-FINTABLEAU)][\1]{}"
            '-> Imprimer la section de bas de page
            If IsFinPage Then _
                Print #hdlData, "[ST-FINPAGE(TXT-SECTION)][]{^REP " & strREP & "   }"
            '-> Imprimer le saut de page
            Print #hdlData, "[PAGE]"
            '-> Imprimer l'entete
            PageEnCours = PageEnCours + 1
            PrintEntete aVue, PageEnCours, hdlData
        Else
            '-> Imprimer la ligne de d�tail
            strDetail = "[TB-BROWSE(BLK-DETAIL)][\1]{"
            For i = 1 To NumEntries(aVue.Colonnes, ",")
                strDetail = strDetail & "  ^" & Format(i, "0000") & Entry(i, DataBlock, "|")
            Next
            Print #hdlData, strDetail & "  }"
            '-> Mettre � jour la hauteur
            HauteurImprimee = HauteurImprimee + hDetailTb
        End If
    End If 'Si ligne � blanc
Loop 'Pout tout le fichier de donn�es

'-> Imprimer la ligne de fin de tableau
Print #hdlData, "[TB-BROWSE(BLK-FINTABLEAU)][\1]{}"

'-> Imprimer la section de bas de page
If IsFinPage Then _
    Print #hdlData, "[ST-FINPAGE(TXT-SECTION][]{^REP        }"
    
'-> fermer tous les fichiers
Reset

If strLNG = "" Then strLNG = "01"

'-> Lancer le turbograph avec
Shell strGUI & " " & "ECRAN$" & CodeLangue & "|" & TempFileName

'-> Renvoyer une valeur de succ�s
CreateEditionBrowse = True
        
'-> Quitter la fonction
Exit Function

ErrorBrowse:
    MsgBox "Impossible de cr�er l'�dition. Export dynamique du Browse", vbCritical + vbOKOnly, "Erreur de cr�ation"
    CreateEditionBrowse = False
    


End Function

Private Sub PrintEntete(aVue As clsVue, PageEnCours As Integer, hdlData As Integer)

Dim strData As String

'-> Imprimer la section d'entete s'il y a en une
If IsEntete Then
    Print #hdlData, "[ST-ENTETE(TXT-SECTION)][]{^DATE" & Format(Now, "dd/mm/yyyy") & "  " & _
                                                "^HOUR" & Format(Now, "hh:mm:ss") & "  " & _
                                                "^OPE " & strOPE & "  " & _
                                                "^TIT " & strTIT & "  " & _
                                                "^VUE " & strVUE & "  " & _
                                                "^LIBV" & aVue.Libelle & "  " & _
                                                "^RUB " & aVue.Rubrique & "  " & _
                                                "^MAQ " & aVue.Maquette & "  " & _
                                                "^IDE " & strIDE & "  " & _
                                                "^PRO " & strPRO & "  " & _
                                                "^PAGE" & PageEnCours & "  }"
    HauteurImprimee = DepartPage + hEntete

End If

'-> Imprimer l'entete
strData = "[TB-BROWSE(BLK-ENTETE)][\1]{"
For i = 1 To NumEntries(aVue.Colonnes, ",")
    strData = strData & "  ^" & Format(i, "0000") & Entry(1, Entry(i, DataEntete, "|"), "#")
Next
Print #hdlData, strData & "  }"
    
'-> Mettre � jour la hauteur imprim�e
HauteurImprimee = HauteurImprimee + hEnteteTb

End Sub

Private Function GetMaqDimensions(aVue As clsVue) As Boolean

'---> Cette fonction est charg�e de r�cup�rer les informations sur les diff�rents objets de la maquette

'-> Pour gestion des param�tres r�gionnaux
Dim lpbuffer As String
Dim OldFormat As String
Dim RestaureNumericFormat As Boolean
Dim OldSepMil As String
Dim RestaureMilierFormat As Boolean

'-> Lecture de la maquette
Dim hdlMaq As Integer
Dim aLigne As String

'-> Indique la nature de l'objet que l'on lit :
    '0 Entete de la maquette
    '1 Section d'entete
    '2 Block Entete
    '3 Block Detail
    '4 Block Fin_Tableau
    '5 Section FinPage
    '6 Section FinDoc
    
Dim IndexObj As Integer

'-> Indique si on a trouv� la hauteur de la maquette et la MargeTop
Dim fMaquette As Boolean
Dim fMargeTop As Boolean

'-> Propri�t�s Hauteur et margeTop de la maquette et des objets
Dim Dimensions(5) As Double

'-> Propri�t�s AlignementTop des sections
Dim AlignTop(1) As Double

'-> Propri�t�s Top des sections
Dim Tops(1) As Double

'-> Indique si on a trouv� les propri�t�s d'alignement
Dim fTops(5, 1 To 2) As Boolean

'-> Varaibles pour test des objets
Dim TypeObj As String
Dim NomObj As String

'-> Indique que l'on lit le tableau Browse
Dim IsBrowse As Boolean

'-> Intercepter les erreurs
On Error GoTo ErrorMaq

'-> V�rifier si la maquette existe
If Dir$(PathApp & aVue.Maquette, vbNormal) = "" Then
    MsgBox "Impossible de trouver la maquette sp�cifi�e : " & Chr(13) & PathApp & aVue.Maquette, vbCritical + vbOKOnly, "Erreur de maquette"
    GoTo ErrorMaq
End If


'-> Modificatrion des param�tres r�gionnaux
'lpBuffer = Space$(10)
'Res = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, 14, lpBuffer, Len(lpBuffer))
'lpBuffer = Mid$(lpBuffer, 1, Res - 1)
'If lpBuffer <> "." Then
'    Res = SetLocaleInfo&(LOCALE_SYSTEM_DEFAULT, 14, ".")
'    '-> indiquer qu'il faut restaurer les param�tres en fin
'    OldFormat = lpBuffer
'    RestaureNumericFormat = True
'End If

'-> V�rification du s�parteur de milier qui doit �tre une ","
'ErrorLibelle = "Pb dans la r�cup�ration du s�parateur de miliers"
'lpBuffer = Space$(10)
'Res = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STHOUSAND, lpBuffer, Len(lpBuffer))
'lpBuffer = Mid$(lpBuffer, 1, Res - 1)
'If lpBuffer <> "," Then
'    Res = SetLocaleInfo&(LOCALE_SYSTEM_DEFAULT, LOCALE_STHOUSAND, ",")
'    '-> indiquer qu'il faut restaurer les param�tres en fin
'    OldSepMil = lpBuffer
'    RestaureMilierFormat = True
'End If


'-> Par d�faut maquette au format A4
hMaquette = 29.7
hMargeTop = 2.5

'-> Ouverture de la maquette
hdlMaq = FreeFile
Open PathApp & aVue.Maquette For Input As #hdlMaq
Do While Not EOF(hdlMaq)
    Line Input #hdlMaq, aLigne
    If Trim(aLigne) <> "" Then
        '-> Selon la nature de la ligne
        Select Case UCase$(Entry(1, aLigne, "�"))
        
            '-> R�cup�ration des valeurs
            
            Case "\HAUTEUR"
                '-> R�cup�rer la valeur et indiquer que l'on a trouv� la valeur
                If IndexObj <> 99 Then Dimensions(IndexObj) = CDbl(Entry(2, aLigne, "�"))
                If IndexObj = 0 Then fMaquette = True
                
            Case "\ALIGNEMENTTOP"
                '-> Ne rien faire si on est pas dans une section de texte
                Select Case IndexObj
                    Case 1
                        AlignTop(0) = CDbl(Entry(2, aLigne, "�"))
                        fTops(0, 1) = True
                    Case 5
                        AlignTop(1) = CDbl(Entry(2, aLigne, "�"))
                        fTops(1, 1) = True
                End Select
                                
            Case "\TOP"
                '-> Ne rien faire si on est pas dans une section de texte
                Select Case IndexObj
                    Case 1
                        Tops(0) = CDbl(Entry(2, aLigne, "�"))
                        fTops(0, 2) = True
                    Case 5
                        Tops(1) = CDbl(Entry(2, aLigne, "�"))
                        fTops(1, 2) = True
                End Select
                        
            Case "\MARGETOP"
                hMargeTop = CDbl(Entry(2, aLigne, "�"))
                fMargeTop = True
                
            '-> Gestion des changements d'objets
            
            Case "\BEGIN"
                '-> Si tableau en cours , changer l'index de l'objet
                If IsBrowse Then
                    Select Case UCase$(Entry(2, aLigne, "�"))
                        Case "ENTETE"
                            IndexObj = 2
                        Case "DETAIL"
                            IndexObj = 3
                        Case "FINTABLEAU"
                            IndexObj = 4
                    End Select 'Selon la ligne de tableau
                End If
              
            Case "\TEXTE"
                '-> Indiquer que l'on est plus dans la section de texte
                IndexObj = 99
            
            Case Else
                '-> Tester si on change d'objet
                If InStr(1, aLigne, "[") <> 0 Then
                    '-> R�cup�ration de la nature de l'objet
                    TypeObj = Mid$(Entry(1, aLigne, "-"), 2, Len(Entry(1, aLigne, "-")) - 1)
                    '-> R�cup�ration du nom de l'objet
                    NomObj = Entry(2, aLigne, "-")
                    If Right$(NomObj, 1) = "]" Then NomObj = Mid$(NomObj, 1, Len(NomObj) - 1)
                    '-> Tester le nom de l'objet
                    Select Case UCase$(TypeObj)
                        Case "TB"
                            '-> Selon le tableau
                            Select Case UCase$(NomObj)
                                Case "BROWSE"
                                    '-> Lecture du bon tableau
                                    IsBrowse = True
                                Case Else
                                    '-> Lecture d'un autre tableau
                                    IndexObj = 99
                                    IsBrowse = False
                            End Select 'Selon le nom de l'objet
                        Case "NV"
                            '-> Ne rien faire si on tombe sur une section RTF
                            If UCase$(Entry(3, aLigne, "-")) <> "RTF]" Then
                                '-> Selon la section de texte
                                Select Case UCase$(NomObj)
                                    Case "ENTETE"
                                        IndexObj = 1
                                    Case "FINPAGE"
                                        IndexObj = 5
                                    Case Else
                                        IndexObj = 99
                                End Select 'Selon le nom de l'objet
                            End If 'Si descriptif RTF
                        Case Else
                            IndexObj = 99
                            IsBrowse = False
                    End Select 'Si tableau ou zone de texte
                End If 'Si premier caract�re = [
        End Select 'Selon la nature de l'objet
    End If 'Si ligne � blanc
Loop 'Boucle d'analyse de la maquette

'-> V�rifier que l'on a trouv� les �lements du tableau
If Dimensions(2) = 0 Or Dimensions(3) = 0 Or Dimensions(4) = 0 Then
    MsgBox "Impossible de trouver la hauteur d'une des lignes du tableau.", vbCritical + vbOKOnly, "Erreur"
    GoTo ErrorMaq
Else
    '-> Faire le setting des dimensions
    hEnteteTb = Dimensions(2)
    hDetailTb = Dimensions(3)
    hFinTb = Dimensions(4)
End If
    
'-> Hauteur de la maquette si on l'a trouv�e
If fMaquette And Dimensions(0) <> 0 Then hMaquette = Dimensions(0)

'-> MargeTop si on l'a trouv�e
If Not fMargeTop Then hMargeTop = 2.5

'-> Affecter les hauteurs des rangs
hEntete = Dimensions(1)
If Dimensions(1) <> 0 Then IsEntete = True
    
hFinPage = Dimensions(5)
If Dimensions(5) <> 0 Then IsFinPage = True

'-> Effectuer des controles de validit� sur la section d'entete
If IsEntete Then
    If AlignTop(0) = 4 Then
        MsgBox "Impossible d'aligner la section d'entete sur la marge du bas. Elle sera align�e sur la marge haut", vbExclamation + vbOKOnly, "Avertissement"
        AlignTop(0) = 2
    End If
    If AlignTop(0) = 5 And hEntete + Tops(0) > hMaquette Then
        MsgBox "L'alignement de la section d'entete est incorrect. Elle sera align�e sur la marge haut", vbExclamation + vbOKOnly, "Avertissement"
        AlignTop(0) = 2
    End If
End If

'-> Controle sur la section de bas de page
If IsFinPage Then
    '-> V�rifier qu'elle soit align�e sur la marge du bas
    If AlignTop(1) <> 3 Then
        MsgBox "La section de bas de page doit �tre align�e sur la marge du bas. Correction apport�e pour l'�dition en cours.", vbExclamation + vbOKOnly, "Avertissement"
        AlignTop(1) = 3
    End If
End If

'-> Calcul du depart de l'impression
If IsEntete Then
    Select Case AlignTop(0)
        Case 1, 2
            DepartPage = hMargeTop
        Case 3
            DepartPage = hMaquette \ 2
        Case 5
            DepartPage = hEntete + Tops(0)
    End Select
Else
    DepartPage = hMargeTop
End If

'-> Calcul de la limitte editable
LimitteBas = hMaquette - hMargeTop
LimitteBas = LimitteBas - hFinPage - hFinTb

'-> Renvoyer une valeur de succ�s
GetMaqDimensions = True

'-> Restaurer les anciens formats num�riques
'---> Fonction qui restitue les param�trages systemes si necessaire
If RestaureNumericFormat Then Res = SetLocaleInfo&(LOCALE_SYSTEM_DEFAULT, 14, OldFormat)
If RestaureMilierFormat Then Res = SetLocaleInfo&(LOCALE_SYSTEM_DEFAULT, LOCALE_STHOUSAND, OldSepMil)

'-> Quitter la fonction
Exit Function

ErrorMaq:
    GetMaqDimensions = False
    '-> Restaurer les anciens formats num�riques
    If RestaureNumericFormat Then Res = SetLocaleInfo&(LOCALE_SYSTEM_DEFAULT, 14, OldFormat)
    If RestaureMilierFormat Then Res = SetLocaleInfo&(LOCALE_SYSTEM_DEFAULT, LOCALE_STHOUSAND, OldSepMil)

End Function


Public Sub CreateMenu()

Dim DefRubrique As String
Dim aFile As String

Dim Rubrique As String
Dim Colonnes As String
Dim Libel As String
Dim Maquette As String
Dim Vue As String

Dim aVue As clsVue

Dim aNode As Node

'---> Cette fonction cr�er la feuille de choix de rubrique

'-> Supprimer tous les nodes
frmChxCol.TreeView1.Nodes.Clear

'-> Supprimer toutes les entr�es dans la liste
frmChxCol.List1(0).Clear

'-> Ajouter dans un premier temps le node de l'application
Set aNode = frmChxCol.TreeView1.Nodes.Add(, , "APP", strPRO & " - " & LibelProgiciel, "App")
aNode.Expanded = True

'-> Ajouter toutes les rubriques
For i = 1 To NumEntries(ListeRubrique, "|")
    DefRubrique = Entry(i, ListeRubrique, "|")
    frmChxCol.TreeView1.Nodes.Add "APP", 4, UCase$(Entry(1, DefRubrique, "@")), Entry(2, DefRubrique, "@"), "Fonction"
Next 'Pour toutes les rubriques

'-> Faire la liste de toutes les rubriques existantes dans le r�pertoire en cours
aFile = ""
aFile = Dir$(PathApp & "*.vis", vbNormal)
i = 0

On Error Resume Next
Do While aFile <> ""

    '-> R�cup�ration des infos sur la rubrique en cours
    Vue = aFile
    Rubrique = Trim(GetIniString("GENERAL", "RUB", PathApp & aFile, False))
    Colonnes = Trim(GetIniString("GENERAL", "COL", PathApp & aFile, False))
    Libel = Trim(GetIniString("GENERAL", "LIB", PathApp & aFile, False))
    Maquette = Trim(GetIniString("GENERAL", "MAQ", PathApp & aFile, False))
    
    '-> Si la rubrique est renseign�e OK
    If Rubrique <> "" Then
        '-> Ajouter le node de la vue
        Set aNode = frmChxCol.TreeView1.Nodes.Add(UCase$(Rubrique), 4, Vue, Vue, "Vue")
        '-> Ajouter le libelle de la vue s'il y en a un
        If Libel <> "" Then _
            frmChxCol.TreeView1.Nodes.Add Vue, 4, "T" & i, Libel, "Libel"
        '-> Ajouter la maquette associ�e
        frmChxCol.TreeView1.Nodes.Add Vue, 4, "MAQ" & i, Maquette, "Doc"
        '-> Positionner la propri�t� TAG du node sur VUE
        aNode.Tag = "VUE"
        frmChxCol.TreeView1.Nodes(UCase$(Rubrique)).Expanded = True
        '-> Ajouter la vue dans la collection
        Set aVue = New clsVue
        aVue.Name = Vue
        aVue.Libelle = Libel
        aVue.Colonnes = Colonnes
        aVue.Rubrique = Rubrique
        aVue.Maquette = Maquette
        Vues.Add aVue, UCase$(Vue)
        '-> Le s�lectionner si c'est celui pass� en parametre
        If UCase$(strVUE) = UCase$(Vue) Then
            aNode.Expanded = True
            aNode.Selected = True
            aNode.EnsureVisible
        End If
    End If
    '-> Pointer sur le fichier suivant
    aFile = Dir
    '-> Incr�menter le conpteur de node
    i = i + 1
Loop 'Pour tous les fichiers

'-> Afficher la liste des colonnes qui sont export�es depuis PROGRESS
For i = 1 To NumEntries(DataEntete, "|") - 1
    frmChxCol.List1(0).AddItem Entry(1, Entry(i, DataEntete, "|"), "#")
Next

'-> Afficher le choix de sauvegarde si autoris�
If UCase$(strSAV) = "OUI" Then
    frmChxCol.Vue.ZOrder
Else
    frmChxCol.Vue_.ZOrder
End If

frmChxCol.Show vbModal

End Sub

Public Sub DisplaySelCol(NomVue As String)

'---> Cette fonction affiche les colonnes s�lectionn�es dans la liste

Dim aVue As clsVue
Dim i As Integer


'-> effacer toutes les selections
For i = 0 To frmChxCol.List1(0).ListCount - 1
    frmChxCol.List1(0).Selected(i) = False
Next

'-> Pointer sur la vue
Set aVue = Vues(NomVue)

'-> Afficher les s�lections de colonnes
For i = 1 To NumEntries(aVue.Colonnes, ",")
    If CInt(Entry(i, aVue.Colonnes, ",")) <= frmChxCol.List1(0).ListCount + 1 Then _
        frmChxCol.List1(0).Selected(CInt(Entry(i, aVue.Colonnes, ",")) - 1) = True
Next 'Pour toutes les colonnes s�lectionn�es
End Sub

