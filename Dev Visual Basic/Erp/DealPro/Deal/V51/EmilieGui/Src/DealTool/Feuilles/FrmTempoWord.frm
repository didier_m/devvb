VERSION 5.00
Begin VB.Form frmTempoWord 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Extraction Word 2000"
   ClientHeight    =   2250
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7755
   ControlBox      =   0   'False
   Icon            =   "FrmTempoWord.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2250
   ScaleWidth      =   7755
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Tempo2 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1560
      ScaleHeight     =   225
      ScaleWidth      =   5985
      TabIndex        =   4
      Top             =   1920
      Width           =   6015
   End
   Begin VB.PictureBox Tempo 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   1560
      ScaleHeight     =   465
      ScaleWidth      =   5985
      TabIndex        =   1
      Top             =   480
      Width           =   6015
   End
   Begin VB.Label Label4 
      Caption         =   "Fichier en cours : "
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   1920
      Width           =   1335
   End
   Begin VB.Image Image1 
      Height          =   720
      Left            =   120
      Picture         =   "FrmTempoWord.frx":12FA
      Top             =   120
      Width           =   720
   End
   Begin VB.Label Label3 
      Height          =   375
      Left            =   6600
      TabIndex        =   3
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label2 
      Height          =   615
      Left            =   1560
      TabIndex        =   2
      Top             =   1080
      Width           =   6015
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Lecture du fichier et cr�ation du fichier Word en cours ..."
      Height          =   255
      Left            =   1560
      TabIndex        =   0
      Top             =   120
      Width           =   4695
   End
End
Attribute VB_Name = "frmTempoWord"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Form_KeyPress(KeyAscii As Integer)

If KeyAscii = 27 Then End

End Sub

