Attribute VB_Name = "FctWord"
Option Explicit

Private DataCols As Collection

Private NbRowData As Long

Private Sub DrawWait()

On Error Resume Next

'---> Proc�dure qui dessine dans Accueil.picture1 le pourcentage r�alis�
'-> ( TailleLue / TailleTotale )
DoEvents
frmTempoWord.Tempo.Line (0, 0)-((TailleLue / TailleTotale) * frmTempoWord.Tempo.ScaleWidth, frmTempoWord.Tempo.ScaleHeight), &HC00000, BF

End Sub

Private Sub DrawCurrentFile(TailleLue As Long, TailleTotale As Long)

On Error Resume Next

'---> Proc�dure qui dessine dans Accueil.picture1 le pourcentage r�alis�
'-> ( TailleLue / TailleTotale )
DoEvents
frmTempoWord.Tempo2.Line (0, 0)-((TailleLue / TailleTotale) * frmTempoWord.Tempo2.ScaleWidth, frmTempoWord.Tempo2.ScaleHeight), &HC00000, BF
frmTempoWord.Label4.Caption = "Fichier : " & Format(CStr(TailleLue / TailleTotale) * 100, "00.00") & " %"
DoEvents

End Sub

Private Sub CreateObjectDataFromFile(dataFile As String)

'---> Cette proc�dure convertit un fichier De donn�es en Mod�le objet

Dim aData As srcData
Dim hdlFile As Integer
Dim Ligne As String

On Error Resume Next

'-> Vider la collection existante
If DataCols Is Nothing Then
    Set DataCols = New Collection
Else
    Do While DataCols.Count <> 0
        DataCols.Remove 1
    Loop
End If
    
'-> Raz des variables
NbRowData = 0
    
'-> Ouverture du fichier Ascii pass� en param�tres
hdlFile = FreeFile
Open dataFile For Input As #hdlFile
Do While Not EOF(hdlFile)
    '-> Lecture d'une ligne
    Line Input #hdlFile, Ligne
    '-> Ne pas traiter les lignes � blanc
    If Trim(Ligne) = "" Then GoTo NextLigne
    '-> Cr�er un nouvl objetd de donn�es
    Set aData = New srcData
    '-> Donn�es ses valeurs
    aData.KeyWord = UCase$(Trim(Entry(1, Ligne, "|")))
    aData.ValueData = Entry(2, Ligne, "|")
    '-> Ajouter dans la collection
    DataCols.Add aData, "DATA|" & aData.KeyWord
    '-> Incr�menter le compteur de ligne
    NbRowData = NbRowData + 1
NextLigne:
Loop

'-> Fermer le fichier
Close #hdlFile

End Sub




Public Sub CreateDocumentWord(ParFile As String)

'---> G�n�ration de documents Word

'-> Pour gestion des variables d'environnement
Dim lpbuffer As String
Dim Res As Long
Dim IsLoadingTempo As Boolean
Dim Rep As VbMsgBoxResult

'-> Pour gestion du fichier Ini
Dim ParamPath As String

'-> Pour lecture du fichier Par
Dim hdlFile As Integer
Dim Ligne As String

'-> Variables de param�trage du fichier PAR
Dim Ident As String
Dim Progiciel As String
Dim ListeDocument As String
Dim NbDocument As Integer
Dim SectionWord As String

'-> Pour lecture d'une section du fichier de param�trage
Dim strRef As String
Dim strDest As String
Dim strData As String
Dim strPasse As String
Dim strKillDataFile As String
Dim strShowAfter As String

'-> Pour r�cup�ration du param�trage du fichier Ini
Dim MsoRef As String
Dim MsoDest As String

'-> Pour gestion des variables d'environnement
Dim VarDealTempo As String
Dim VarUserName As String

'-> Pour gestion de Word
Dim WordApp As Object
Dim WordFileRef As String
Dim WordFileDest As String

'-> Pour get du contenu du fichier Word
Dim WordDocumentText As String

'-> Variables de lecture
Dim i As Integer, j As Integer


On Error GoTo GestError

'-> V�rifier si on trouve le fichier
If ParFile = "" Then
    MsgBox "Veuillez sp�cifier un fichier de param�trage", vbCritical + vbOKOnly, "Erreur fatale"
    End
Else
    If Dir$(ParFile) = "" Then
        MsgBox "Impossible de trouver le fichier de param�trage" & Chr(13) & ParFile, vbCritical + vbOKOnly, "Erreur Fatale"
        End
    End If
End If

'-> R�cup�ration des param�tres d'entete
Ident = GetIniFileValue("GENERAL", "IDENT", ParFile)
Progiciel = GetIniFileValue("GENERAL", "PROGICIEL", ParFile)

'-> Tester l'ident
If Ident = "" Then
    MsgBox "Veuillez renseigner l'ident", vbCritical + vbOKOnly, "Erreur Fatale"
    Exit Sub
End If

If Progiciel = "" Then
    MsgBox "Veuillez renseigner le progiciel", vbCritical + vbOKOnly, "Erreur Fatale"
    Exit Sub
End If

'-> R�cup�rer les mots cl�s d'adressage virtuels
GetPathMotCles

'-> Construire le path du fichier PARAM.INI
ParamPath = "$LECTEUR$\DEALPRO\$IDENT$\$VERSION$\Param.ini"
ReplacePathMotCles ParamPath

'-> Remplcer $IDENT$ par l'ident pass� en param�tre dans le *.par
ParamPath = Replace(ParamPath, "$IDENT$", Ident)

'-> R�cup�rer le param�trage du fichier ini de r�f�rence
MsoRef = GetIniString("MSO_" & Progiciel, "$REFERENCE$", ParamPath, False)
MsoDest = GetIniString("MSO_" & Progiciel, "$DESTINATION$", ParamPath, False)

'-> R�cup�rer les variables d'environnement
lpbuffer = Space$(300)
Res = GetEnvironmentVariable("DEALTEMPO", lpbuffer, Len(lpbuffer))
If Res <> 0 Then VarDealTempo = Mid$(lpbuffer, 1, Res)
lpbuffer = Space$(300)
Res = GetEnvironmentVariable("USERNAME", lpbuffer, Len(lpbuffer))
If Res <> 0 Then VarUserName = Mid$(lpbuffer, 1, Res)
    
'-> Faire un replace des mots cl�s
ReplacePathMotCles MsoRef
ReplacePathMotCles MsoDest
    
'-> Faire un replace dans les 2 paths
MsoRef = Trim(Replace(UCase$(MsoRef), "%DEALTEMPO%", VarDealTempo))
MsoRef = Trim(Replace(UCase$(MsoRef), "%USERNAME%", VarUserName))
MsoDest = Trim(Replace(UCase$(MsoDest), "%DEALTEMPO%", VarDealTempo))
MsoDest = Trim(Replace(UCase$(MsoDest), "%USERNAME%", VarUserName))

'-> Rajouter un \ si necessaire
If MsoRef <> "" And Right(MsoRef, 1) <> "\" Then MsoRef = MsoRef & "\"
If MsoDest <> "" And Right(MsoDest, 1) <> "\" Then MsoDest = MsoDest & "\"

MsgBox "procedure createdocumentword => " & MsoRef & " / " & MsoDest

'-> R�cup�rer la liste des documents
ListeDocument = Space(64000)
Res = GetPrivateProfileSectionNames(ListeDocument, Len(ListeDocument), ParFile)
If Res = 0 Then
    '-> Message d'erreur
    MsgBox "Pas de document � traiter", vbExclamation + vbOKOnly, "Fin du traitement"
    End
Else
    ListeDocument = Mid$(ListeDocument, 1, Res)
End If

'-> Traitement de toutes les sections du fichier Par
NbDocument = NumEntries(ListeDocument, Chr(0)) - 1

'-> Cr�er un point d'entr�e vers Word
Set WordApp = CreateObject("Word.Application")

'-> Tester si cela a march�
If WordApp Is Nothing Then
    '-> Message d'erreur
    MsgBox "Impossible de r�cup�rer un pointeur vers Mircosoft Word", vbCritical + vbOKOnly, "Erreur Fatale"
    GoTo GestError
End If

'-> Charger la feuille de temporisation �cran
frmTempoWord.Show
frmTempoWord.Enabled = False

'-> Indiquer que la feuille est charg�e
IsLoadingTempo = True

'-> Poser la taille max
TailleTotale = NbDocument - 2

'-> Analyse de toutes les sections � traiter
For i = 1 To NbDocument
    '-> R�cup�rer la section � trairer
    SectionWord = Trim(Entry(i, ListeDocument, Chr(0)))
    '-> Ne pas traiter la section g�n�ral
    If UCase$(Trim(SectionWord)) = "GENERAL" Then GoTo NextSection
    
    '-> Setting de la temporisation
    frmTempoWord.Label3.Caption = i - 1 & "/" & NbDocument - 1
    
    '-> Sortir de la boucle si necessaire
    If Trim(SectionWord) = "" Then Exit For
    
    '-> R�cup�rer le param�trage du document
    strRef = GetIniFileValue(SectionWord, "REFERENCE", ParFile)
    strDest = GetIniFileValue(SectionWord, "DESTINATION", ParFile)
    strData = GetIniFileValue(SectionWord, "DATA", ParFile)
    strPasse = GetIniFileValue(SectionWord, "PASSE", ParFile)
    strKillDataFile = UCase$(GetIniFileValue(SectionWord, "KILL", ParFile))
    strShowAfter = UCase$(GetIniFileValue(SectionWord, "SHOW", ParFile))
    
    '-> V�rifier si on trouve le fichier de r�f�rence
    WordFileRef = MsoRef & strRef
    If Dir$(WordFileRef) = "" Then
        MsgBox "Impossible de traiter le document : " & SectionWord & Chr(13) & "Fichier de r�f�rence introuvable" & Chr(13) & WordFileRef, vbCritical + vbCritical, "Impossible de traiter le document"
        GoTo NextSection
    End If
    
    '-> V�rifier si on trouve le fichier de donn�es sp�cifi�
    If strData = "" Then
        MsgBox "Fichier de donn�es non rens�ign� pour le document : " & SectionWord, vbExclamation + vbOKOnly, "Erreur"
        GoTo NextSection
    Else
        '-> V�rifier si on trouve le fichier
        If Dir$(strData) = "" Then
            MsgBox "Impossible de trouver le fichier de donn�es : " & strData, vbCritical + vbCritical, "Impossible de traiter le document"
            GoTo NextSection
        End If
    End If
    
    '-> Composer le fichier de destination
    WordFileDest = MsoDest & strDest
    
    '-> V�rifier si le fichier destination existe
    If Dir$(WordFileDest) <> "" Then
        '-> Demander confirmation avant de supprimer
        Rep = MsgBox("Le fichier " & Chr(13) & WordFileDest & " existe d�ja. Le supprimer ?", vbExclamation + vbYesNo, "Confirmation")
        If Rep = vbNo Then GoTo NextSection
        '-> Kill du fichier
        Kill WordFileDest
    End If
    
    '-> Indiquer le nom du fichier destination
    frmTempoWord.Label2.Caption = WordFileDest
    
    '-> Lib�r�r la cpu
    frmTempoWord.Show
    DoEvents
    
    '-> Ouvrir le fichier de donn�es et cr�er son mod�le objet
    Call CreateObjectDataFromFile(strData)
    
    '-> Ouvrir le document de r�f�rence en tant que template ou Document
    WordApp.Application.Documents.Open Filename:="" & WordFileRef & ""
    
    '-> Viderle presspapier
    Clipboard.Clear
        
    '-> Copy dans le presspapier
    WordApp.Application.Selection.WholeStory
   
    '-> Faitre un replace du contenu
    Call ReplaceKeyWord(WordApp)
        
    '-> Enregistrer le document word
    WordApp.Application.ActiveDocument.SaveAs Filename:="" & WordFileDest, _
    LockComments:=False, PassWord:="" & strPasse, AddToRecentFiles:= _
    True, WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts:= _
    False, SaveNativePictureFormat:=False, SaveFormsData:=False, SaveAsAOCELetter:=False

    '-> Fermer le document
    WordApp.Application.ActiveDocument.Close
    
    '-> Supprimer le fichier de donn�es si necessaire
    If strKillDataFile = "OUI" Then Kill strData
    
    '-> Indiquer la taille Lue
    TailleLue = TailleLue + 1
    DrawWait
    
NextSection:
Next 'Pour tous les documents � traiter

'-> Rendre word visible si necessaire
If UCase$(Trim(strShowAfter)) = "OUI" Then
    '-> Ouvrir le document de r�f�rence THIERRY
    WordApp.Application.Documents.Open Filename:="" & WordFileDest & ""
    WordApp.Visible = True
End If

'-> Supprimer le fichier PAR
Kill ParFile

GestError:
    '-> Fermer l'application Word
    If UCase$(Trim(strShowAfter)) <> "OUI" Then WordApp.Application.Quit
    '-> Lib�rer le pointeur vers Word
    Set WordApp = Nothing
    '-> Si on a charg� la feuille de temporisation , la d�charger
    If IsLoadingTempo Then Unload frmTempoWord
    


End Sub


Public Sub ReplaceKeyWord(WordApp As Object)

'---> Cette proc�dure remplace dans une chaine de caract�re les mots cl�s par leur valeur

Dim i As Integer, j As Integer, k As Integer
Dim KeyWord As String
Dim iLig As Long
Dim TempoValue As String
Dim ToPrint As String
Dim ValueToReplace As String

'-> R�cup�rer la premi�re position
i = InStr(1, WordApp.Application.Selection, "<�")

'-> R�cup�rer tous les mots cl�s
Do While i <> 0

    
    '-> R�cup�rer le mot cl� dans sa totalit�
    j = InStr(i, WordApp.Application.Selection, ">")
    KeyWord = Mid$(WordApp.Application.Selection, i, j - i + 1)
    '-> R�cup�rer la valeur du mot cl�
    TempoValue = GetKeyWordValue(KeyWord)
    ValueToReplace = KeyWord
    If Len(TempoValue) = 0 Then
        ReplaceMotCleByValue ValueToReplace, "", WordApp
    Else
        Do While Len(TempoValue) <> 0
            '-> On travaille par block de 200
            If Len(TempoValue) <= 200 Then
                ToPrint = TempoValue
                TempoValue = ""
                ReplaceMotCleByValue ValueToReplace, ToPrint, WordApp
            Else
                '-> Get de la valeur � imprimer
                ToPrint = Mid$(TempoValue, 1, 200)
                '-> Virer les 200 premiers caract�res
                TempoValue = Replace(TempoValue, ToPrint, "", , 1)
                '-> Faire le replace
                ReplaceMotCleByValue ValueToReplace, ToPrint & "<�%GWX%R�>", WordApp
                '-> Modifier le keyword
                ValueToReplace = "<�%GWX%R�>"
            End If 'Si la variable est Sup � 200
        Loop 'Pour tous les segments de text
    End If
    '-> Incr�menter le compteur
    i = InStr(i + 1, WordApp.Application.Selection, "<�")
    '-> Dessin de la temporisation
    iLig = iLig + 1
    DrawCurrentFile iLig, NbRowData
Loop 'Pour tous les mots cl� r�f�renc�s

'-> Faire ensuite ici un replace g�n�ral des chr(13)
Call ReplaceMotCleByValue("$CHR(13)$", Chr(13), WordApp)

End Sub

Private Sub ReplaceMotCleByValue(KeyWord As String, ValueToReplace As String, WordApp As Object)

'-> Gestion des sauts de ligne
'ValueToReplace = Replace(ValueToReplace, Chr(13), "")
'ValueToReplace = Replace(ValueToReplace, Chr(10), "")
'ValueToReplace = Replace(ValueToReplace, "$CHR(13)$", vbCrLf)

'MsgBox ValueToReplace

'-> Faire un replace
WordApp.Application.Selection.Find.ClearFormatting
WordApp.Application.Selection.Find.Replacement.ClearFormatting
With WordApp.Application.Selection.Find
    .Text = KeyWord
    .Replacement.Text = ValueToReplace
End With
WordApp.Application.Selection.Find.Execute Replace:=2

End Sub

Private Function GetKeyWordValue(KeyWord As String) As String

Dim aData As srcData

On Error GoTo GestError

'-> Essayer de pointer sur l'objet
Set aData = DataCols("DATA|" & UCase$(Trim(KeyWord)))

'-> Retourner sa valeur
GetKeyWordValue = aData.ValueData

'-> Lib�rer le pointeur
Set aData = Nothing

GestError:

End Function

