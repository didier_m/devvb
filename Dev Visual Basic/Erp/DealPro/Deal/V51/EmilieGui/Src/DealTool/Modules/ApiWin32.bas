Attribute VB_Name = "ApiWin32"
Public Declare Function GetSystemMetrics& Lib "user32" (ByVal nIndex As Long)
Public Declare Function ScreenToClient& Lib "user32" (ByVal Hwnd As Long, lpPoint As POINTAPI)
'-> Constantes pour GetSystemMetrics
Public Const SM_CXEDGE& = 45 'Largeur d'une bordure double
Public Const SM_CYEDGE& = 46

Public Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type


Public Type POINTAPI
    X As Long
    Y As Long
End Type

'-> API pour gestion des fen�tres
Public Declare Function GetClientRect& Lib "user32" (ByVal Hwnd As Long, lpRect As RECT)

'-> API pour r�cup�ration des variables d'environnement
Public Declare Function GetEnvironmentVariable Lib "kernel32" Alias "GetEnvironmentVariableA" (ByVal lpName As String, ByVal lpbuffer As String, ByVal nSize As Long) As Long

'-> API pour gestion des ressources locales
Public Const LOCALE_SYSTEM_DEFAULT& = &H800
Public Const LOCALE_STHOUSAND& = &HF
Public Declare Function GetLocaleInfo& Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long)
Public Declare Function SetLocaleInfo& Lib "kernel32" Alias "SetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String)

'-> Api gestion des fichiers tempo
Public Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpbuffer As String) As Long
Public Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long

'-> Pour ex�cution d'un fichier
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal Hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

'-> Api pour lecture des fichiers INI
Public Declare Function GetCursorPos& Lib "user32" (lpPoint As POINTAPI)
Public Declare Function GetPrivateProfileSection& Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function GetPrivateProfileSectionNames& Lib "kernel32" Alias "GetPrivateProfileSectionNamesA" (ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As Any, ByVal lpFileName As String)


'-> Api pour gestion des fichiers
Public Declare Function CloseHandle& Lib "kernel32" (ByVal hObject As Long)
Public Declare Function OpenFile& Lib "kernel32" (ByVal lpFileName As String, lpReOpenBuff As OFSTRUCT, ByVal wStyle As Long)
Public Declare Function GetFileSize& Lib "kernel32" (ByVal hFile As Long, lpFileSizeHigh As Long)
Public Declare Function CopyFile& Lib "kernel32" Alias "CopyFileA" (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal bFailIfExists As Long)


'-> Pour gestion des fichiers
Public Const OFS_MAXPATHNAME& = 128
Public Const OF_EXIST& = &H4000
Public Const OF_READ& = &H0


Public Type OFSTRUCT
    cBytes As Byte
    fFixedDisk As Byte
    nErrCode As Integer
    Reserved1 As Integer
    Reserved2 As Integer
    szPathName(OFS_MAXPATHNAME) As Byte
End Type


