Attribute VB_Name = "FctTbXls"
'***********************************************************
'* Module utilis� pour le bilan social : export vers Excel *
'***********************************************************

'-> Propath des Tableaux de Bords
Public SourceXLS As String '-> Emplacement des classeurs R�f�rence
Public DestinationXls As String '-> Emplacement de destination des classeurs

'-> Variables globales r�cup�r�es dans la section GENERAL
Private strIdent As String
Private strProgiciel As String
Private strPasse As String
Private strNature As String
Private strSave As String
Private strOpen As String
Private boolSave As Boolean
Private boolOpen As Boolean
Private strConfirm As String
Private boolConfirm As Boolean
Private strStop As String
Private boolStop As Boolean
Private strKill As String
Private boolKill As Boolean

'-> Variable de retour de l'outils
Private ReturnFile As String
Private hdlFile As Integer

'-> Variable de localisation du fichier Ini
Public IniPath As String

'-> Variable du classeur en cours de traitement
Private ExcelApp As Object
Private aWorkBook As Object

Public Sub OpenExcel(LigneCommande As String)

Dim Excel As Object
Dim NomClasseur As String
Dim PassWord As String

On Error GoTo GestError

'-> R�cup�ration des param�tres
NomClasseur = Entry(1, LigneCommande, "@")
PassWord = Entry(2, LigneCommande, "@")

'-> V�rifier si le classeur existe
If Dir$(NomClasseur, vbNormal) = "" Then GoTo GestError
    
Set Excel = CreateObject("Excel.Application")
    
Excel.Workbooks.Open NomClasseur, , , , PassWord
Excel.Visible = True
Set Excel = Nothing

Exit Sub

GestError:

    MsgBox "Une erreur est intervenue dans l'ouverture du classeur : " & Chr(13) & NomClasseur, vbCritical + vbOKOnly, "Erreur d'ouverture"
    Set Excel = Nothing
    MsgBox Err.Number & Err.Description


End Sub


Sub GenerateTableauXls(LigneCommande As String)

'Dim TempFileName As String
'Dim ParamPath As String
'Dim i As Integer, j As Integer
'Dim ParFile As String
'Dim DateTempo
'Dim SavePath As String
'Dim LectParam As Boolean
'Dim ToSave As Boolean
'
''-> Mettre � jour l'affichage
'frMTempoExcel.Show
'DoEvents
'
''-> V�rifier le nombre d'argument de la ligne de commande
'If NumEntries(LigneCommande, "@") <> 2 Then
'    MsgBox "Erreur de passage d'arguments dans la ligne de commande des exports Excel.", vbCritical + vbOKOnly, "Erreur fatale"
'    End
'End If
'
''-> R�cup�ration du fichier par
'ParFile = Entry(1, LigneCommande, "@")
'
'frMTempoExcel.Label3.Caption = ParFile
'
''-> V�rifier l'existance du fichier PAR
'If Dir$(ParFile, vbNormal) = "" Then
'    MsgBox "Impossible de trouver le fichier PAR : " & Chr(13) & ParFile, vbCritical + vbOKOnly, "Erreur fatale"
'    End
'End If
'
''-> R�cup�ration du fichier de retour des informations
'ReturnFile = Entry(2, LigneCommande, "@")
'
''-> V�rifier l'existence du fichier de retour des informations
'If Dir$(ReturnFile, vbNormal) = "" Then
'    MsgBox "Impossible de trouver le fichier de retour d'information : " & Chr(13) & ReturnFile, vbCritical + vbOKOnly, "Erreur fatale"
'    End
'End If
'
''-> R�cup�ration des valeurs de la cl� g�r�rale du fichier PAR
'strIdent = GetIniString("GENERAL", "IDENT", ParFile, True)
'strProgiciel = GetIniString("GENERAL", "PROGICIEL", ParFile, True)
'strPasse = GetIniString("GENERAL", "PASSE", ParFile, True)
'strSave = GetIniString("GENERAL", "SAVE", ParFile, True)
'strOpen = GetIniString("GENERAL", "OPEN", ParFile, True)
'strConfirm = GetIniString("GENERAL", "CONFIRM", ParFile, True)
'strStop = GetIniString("GENERAL", "STOP", ParFile, True)
'
''-> Modif 14/09/2000
'
''-> R�cup�ration  des cl�s REFRENCE , DESTINATION , NATURE dans la section GENERALE
'strReference = GetIniString("GENERAL", "REFERENCE", ParFile, False)
'strDestination = GetIniString("GENERAL", "DESTINATION", ParFile, False)
'strNature = GetIniString("GENERAL", "NATURE", ParFile, False)
'
'DoEvents
'
''-> Tester les valeurs de retour
'If strIdent = "" Or strProgiciel = "" Then
'    MsgBox "Une des informations de configuration est introuvable.Chargement arret�.", vbCritical + vbOKOnly, "Erreur fatale"
'    End
'End If
'
''-> Gestion de la demande de confirmation
'If UCase$(strConfirm) = "OUI" Then
'    boolConfirm = True
'Else
'    boolConfirm = False
'End If
'
''-> Gestion de l'ouverture / Enregistrement
'If UCase$(strSave) = "OUI" Then
'    boolSave = True
'Else
'    boolSave = False
'End If
'
'If UCase$(strOpen) = "OUI" Then
'    boolOpen = True
'Else
'    '-> Attention : si on n'enregistre pas : laisser le classeur OBLIGATOIREMENT Ouvert
'    If Not boolSave Then
'        boolOpen = True
'    Else
'        boolOpen = False
'    End If
'End If
'
'If UCase$(strStop) = "OUI" Then
'    boolStop = True
'Else
'    boolStop = False
'End If
'DoEvents
'
''-> Tester le path Src du Path Exe
'If InStr(1, UCase$(App.Path), "EMILIEGUI\SRC") <> 0 Then
'    ParamPath = "D:\Erp\DealPro\Deal\V51\EMILIEGUI\EXE"
'Else
'    CreatePath ParamPath
'End If
'
'
'frMTempoExcel.WebBrowser1.Visible = False
'
'
''-> Modif 31/10/2000
''Possibilit� de lancer cet outil sans �tre dans une architecture V51 -> On sp�cifie _
'donc les r�f�rences et les destinations directement dans une section MSO dans le fichier PAR
'
''-> On v�rifie si on trouve les cl�s
'SourceXLS = GetIniString("MSO", "SourceXLS", ParFile, False)
'DestinationXls = GetIniString("MSO", "DestinationXLS", ParFile, False)
'
'If Trim(SourceXLS) = "" Then
'    '-> Lecture normale dans Param.ini
'    LectParam = True
'Else
'    '-> Pas de lecture dans le param.ini
'    LectParam = False
'End If
'
'If LectParam Then
'
'    '-> R�cup�rer les mots cl�s d'adressage virtuels
'    GetPathMotCles
'
'    '-> Construire le path du fichier PARAM.INI
'    ParamPath = "$LECTEUR$\DEALPRO\$IDENT$\$VERSION$\Param.ini"
'    ReplacePathMotCles ParamPath
'    DoEvents
'
'    '-> Remplcer $IDENT$ par l'ident pass� en param�tre dans le *.par
'    ParamPath = Replace(ParamPath, "$IDENT$", strIdent)
'
'    '-> Obtenir le nom d'un fichier Temporaire de Windows
'    TempFileName = GetTempFileNameVB("INI")
'    DoEvents
'
'    '-> Faire une copie de param.ini dans le fichier TEMPO
'    CopyFile ParamPath, TempFileName, False
'    DoEvents
'
'    '-> Ouverture du fichier Temporaire PARAM.INI pour r�cup�rer les path de d�part et de destination
'    SourceXLS = GetIniString("MSO_" & strProgiciel, "$REFERENCE$", TempFileName, True)
'    DestinationXls = GetIniString("MSO_" & strProgiciel, "$DESTINATION$", TempFileName, True)
'    DoEvents
'
'
'    '-> Tester les valeurs de retour
'    If Trim(SourceXLS) = "" Or Trim(DestinationXls) = "" Then
'        MsgBox "Une information de configuration est introuvable dans le fichier : " & _
'                Chr(13) & ParamPath, vbCritical + vbOKOnly, "Erreur fatale"
'        End
'    End If
'
'    '-> Remplacer les noms logiques par leur valeur
'    ReplacePathMotCles SourceXLS
'    ReplacePathMotCles DestinationXls
'    DoEvents
'
'End If 'Si Lecture du param.ini
'
''-> V�rifier la fin des r�pertoires
'If Right$(SourceXLS, 1) <> "\" Then SourceXLS = SourceXLS & "\"
'If Right$(DestinationXls, 1) <> "\" Then DestinationXls = DestinationXls & "\"
'DoEvents
'
'
''-> V�rifier l'existance des r�pertoires sp�cifi�s
'If Dir$(SourceXLS, vbDirectory) = "" Then
'    MsgBox "Impossible de trouver le r�pertoire de r�f�rence :" & Chr(13) & SourceXLS, vbCritical + vbOKOnly, "Erreur fatale"
'    End
'End If
'
'If Dir$(DestinationXls, vbDirectory) = "" Then
'    MsgBox "Impossible de trouver le r�pertoire de destination :" & Chr(13) & DestinationXls, vbCritical + vbOKOnly, "Erreur fatale"
'    End
'End If
'
''-> V�rification du classeur de r�f�rence
'If strReference = "" Then
'    MsgBox "Erreur de param�trage : Le classeur de r�f�rence n'est pas renseign�", vbCritical + vbOKOnly, "Erreur"
'    End
'Else
'    '-> Le classeur est r�f�renc� : v�rifi� si on le trouve dans le Path
'    If Dir$(SourceXLS & strReference) = "" Then
'        '-> Lancer un message d'erreur
'        MsgBox "Impossible de trouver le classeur de r�f�rence : " & Chr(13) & SourceXLS & strReference, vbCritical + vbOKOnly, "Erreur fatale"
'        End
'    End If
'End If
'
''-> V�rification du classeur de destination
'If strDestination = "" Then
'    MsgBox "Incoh�rence des param�tres pass�s dans la section : " & lpSection & ". Si la propri�t� Reference est rempli�, Destination doit l'�tre �galement.", vbCritical + vbOKOnly, "Erreur fatale"
'    End
'End If
'
''-> Cr�er un nom de fichier Unique
'DateTempo = Year(Now) & "_" & Month(Now) & "_" & Day(Now) & "_" & Hour(Now) & "_" & Minute(Now) & "_" & Second(Now)
'
''-> faire une copie du classeur de r�f�rence dans le r�pertoire de r�f�rence
'CopyFile SourceXLS & strReference, SourceXLS & DateTempo, False
'
''-> 0uverture du nouveau classeur temporaire sous Excel
'Set aWorkBook = ExcelApp.Workbooks.Open(SourceXLS & DateTempo)
'
''-> Les paths sont v�rifi�s : proc�der au chargement des diff�rentes sections du fichier PAR
'CreateDocumentXls ParFile
'DoEvents
'
''-> Enregistrer le classeur si necessaire
'
''-> Concat�ner la nature avec le path de destination
'If strNature <> "" Then
'    SavePath = DestinationXls & strNature
'    If Right$(SavePath, 1) <> "\" Then SavePath = SavePath & "\"
'Else
'    '-> Mettre � jour le path d'enregistrement
'    SavePath = DestinationXls
'    If Right$(SavePath, 1) <> "\" Then SavePath = SavePath & "\"
'End If
'
''-> V�rifier l'existence du R�pertoire de destination
'If Dir$(SavePath, vbDirectory) = "" Then
'    MsgBox "Le repertoire de destination du classeur Excel est introuvable : " & Chr(13) & SavePath & Chr(13) & "Op�ration annul�e.", vbCritical + vbOKOnly, "Erreur fatale"
'    End
'End If
'
''-> Le classeur est cr�� : il faut maintenant enregistrer le classeur � l'endroit _
'sp�cifi� si necessaire
'If strDestination <> "" And boolSave Then
'    '-> V�rifier si le classeur Destination n'existe pas d�ja pour demander confirmation
'    If boolConfirm And Dir$(SavePath & strDestination, vbNormal) <> "" Then
'        Rep = MsgBox("Le classeur de destination existe d�ja. Voulez-vous l'�craser ?" & Chr(13) & "ATTENTION : NON entrainera l'arret et la suppression du traitement.", vbQuestion + vbYesNo, "Confirmation")
'        If Rep = vbNo Then
'            MsgBox "Impossible d'enregistrer le classeur : celui-ci existe d�ja", vbExclamation + vbOKOnly, "Erreur"
'            ToSave = False
'        Else
'            ToSave = True
'        End If
'    Else
'        ToSave = True
'    End If
'
'    '-> Enregistrer si necessaire
'    If ToSave Then
'
'        '-> supprimer le fichier r�sultat s'il existe
'        If Dir$(SavePath & strDestination, vbNormal) <> "" Then Kill SavePath & strDestination
'
'        If Trim(strPasse) = "" Then
'            '-> Enregistrer le classeur sous le nouveau Nom
'            aWorkBook.SaveAs SavePath & strDestination
'        Else
'            aWorkBook.SaveAs SavePath & strDestination, , strPasse
'        End If
'
'        '-> Supprimer le fichier temporaire
'        Kill SourceXLS & DateTempo
'    End If
'
'End If
'
'
''-> Rendre Excel visible si necessaire
'If boolOpen Then
'    ExcelApp.Visible = True
'Else
'    ExcelApp.Visible = False
'End If
'
'
''-> Lib�rer le pointeur sur l'application
'If Not boolOpen Then ExcelApp.Quit
'Set ExcelApp = Nothing
'
'
''-> Envoyer le message de retour que tout va bien
'hdlFile = FreeFile
'Open ReturnFile For Output As #hdlFile
'Print #hdlFile, "OK"
'Close #hdlFile
'
''-> Afficher le bouton de fin si boolStop = true
'If boolStop Then
'    frMTempoExcel.Command1.Visible = True
'    frMTempoExcel.ZOrder
'Else
'    Unload frMTempoExcel
'    End
'End If

End Sub

'Private Sub CreateDocumentXls(ParamPath As String)
'
''---> Cette proc�dure Analyse le fichier ini pass� en param�tre pour r�cup�rer les diff�rentes sections
'
''-> Variable pour controle
'Dim IsRef As Boolean
'
''-> Variables globales � la section
'Dim lpSectionNames As String
'Dim Res As Long
'Dim i As Integer, j As Integer, k As Integer, l As Integer
'Dim lpSection As String
'
''-> Variables pour Section de chargement
'Dim strReference As String
'Dim strDestination As String
'Dim strData As String
'Dim strOnglet As String
'Dim strName As String
'
''-> Variabsle pour nom de r�pertoire temporaire
'Dim aWorkSheet As Object
'Dim FindSheet As Boolean
'Dim aRange As Object
'Dim CelluleDep As String
'
''-> Variable pour retour
'Dim Rep
'
''-> Variable pour lecture des lignes
'Dim nbLig As Integer
'Dim nbCol As Integer
'Dim Ligne As String
'Dim hdlFile As Integer
'Dim aName As Object
'Dim FindLigne As Boolean
'
''-> Variable pour nom de NOM
'Dim FindName As Boolean
'Dim lpNameToReplace As String
'Dim strCellule As String
'Dim strValue As String
'Dim DefCellule As String
'Dim DefValue As String
'Dim StrNameDep As String
'
''-> Pour gestion des versions
'Dim Excel97 As Boolean
'Dim TempoStr As String
'Dim Tempo As String
'
'
''-> Variables pour lecture du fichier
'
''On Error Resume Next
''-> R�cup�rer la liste des sections
'lpSectionNames = Space$(2000)
'Res = GetPrivateProfileSectionNames(lpSectionNames, Len(lpSectionNames), ParamPath)
'If Res = 0 Then
'    MsgBox "Impossible de trouver les ordres de chargement du classeur Excel dans le fichier PAR.", vbCritical + vbOKOnly, "Erreur fatale"
'    End
'End If
'DoEvents
'
'ExcelApp.DisplayAlerts = False
'
''-> Analyse de l'ensemble des sections
'For i = 1 To NumEntries(lpSectionNames, Chr(0))
'    DoEvents
'    '-> R�cup�ration du nom de la section
'    lpSection = Entry(i, lpSectionNames, Chr(0))
'    '-> Tester le contenu de la section
'    Select Case UCase$(Entry(1, lpSection, "_"))
'
'        Case ""
'            Exit For
'
'        Case "GENERAL"
'            '-> Ne rien faire
'
'        Case "CHARGEMENT"
'
'            '-> R�cup�ration des cl�s pour la section de chargement
'            strData = GetIniString(lpSection, "DATA", ParamPath, True)
'            strOnglet = GetIniString(lpSection, "ONGLET", ParamPath, True)
'            strName = GetIniString(lpSection, "NAME", ParamPath, True)
'            strKill = GetIniString(lpSection, "KILL", ParamPath, True)
'
'            DoEvents
'
'            If UCase$(strKill) = "OUI" Then
'                boolKill = True
'            Else
'                boolKill = False
'            End If
'
'            '-> V�rification que l'on trouve le fichier de donn�es
'            If Dir$(strData, vbNormal) = "" Then
'                MsgBox "Impossible de trouver le fichier source de donn�es : " & Chr(13) & strData, vbCritical + vbOKOnly, "Erreur fatale"
'                GoTo EndError2
'            End If
'
'            DoEvents
'
'            '-> V�rifier si l'onglet sp�cifi� existe
'            FindSheet = False
'            For j = 1 To aWorkBook.Sheets.Count
'                If UCase$(aWorkBook.Sheets(j).Name) = UCase$(strOnglet) Then
'                    '-> Pointer sur la feuille
'                    Set aWorkSheet = aWorkBook.Sheets(j)
'                    '-> Indiquer que l'on a trouv� la feuille
'                    FindSheet = True
'                    '-> Sortir de la boucle
'                    Exit For
'                End If
'            Next
'
'            '-> Cr�er le nouvel Onglet si on ne l'a pas trouv�
'            If Not FindSheet Then
'                Set aWorkSheet = aWorkBook.Worksheets.Add
'                aWorkSheet.Name = strOnglet
'            End If
'
'            '-> Activer la feuille
'            aWorkSheet.Visible = xlSheetVisible
'            aWorkSheet.Activate
'
'            '-> Charger le fichier de r�f�rence dans la feuille
'            With aWorkSheet.QueryTables.Add(Connection:="TEXT;" & strData, Destination:=Range("A1"))
'                .Name = "LOADING" & i
'                .FieldNames = True
'                .RowNumbers = False
'                .FillAdjacentFormulas = False
'                .PreserveFormatting = True
'                .RefreshOnFileOpen = True
'                .RefreshStyle = xlInsertDeleteCells
'                .SavePassword = False
'                .SaveData = True
'                .RefreshPeriod = 0
'                .TextFilePromptOnRefresh = False
'                .TextFilePlatform = xlWindows
'                .TextFileTextQualifier = xlTextQualifierDoubleQuote
'                .TextFileTabDelimiter = False
'                .TextFileSemicolonDelimiter = False
'                .TextFileCommaDelimiter = False
'                .TextFileSpaceDelimiter = False
'                .TextFileOtherDelimiter = "|"
'                .Refresh BackgroundQuery:=False
'            End With
'
'            DoEvents
'
'            '-> V�rifier si on doit donner un nom � la plage
'            If strName <> "" Then
'
'                '-> R�cup�ration de la zone s�lectionn�e
'                Set aRange = aWorkSheet.QueryTables("LOADING" & i).ResultRange
'
'                '-> Donner un nom � la plage de donn�es
'                aWorkBook.Names.Add strName, "=" & strOnglet & "!" & aRange.Address
'
'            End If
'
'            '-> Masquer la feuille des donn�es
'            aWorkSheet.Visible = False
'
'            '-> Essayer de supprimer le fichier de donn�es si necessaire
'            If Dir$(strData) <> "" And boolKill Then Kill strData
'
'        Case "SUBSTITUTION"
'
'            '-> Dans tous les cas, il faut r�cup�rer la liste des subsitutions
'            lpNameToReplace = Space$(2000)
'            Res = GetPrivateProfileSection(lpSection, lpNameToReplace, Len(lpNameToReplace), ParamPath)
'            If Res <> 0 Then
'                '-> Pour toutes les cl�s � remplacer
'                For k = 1 To NumEntries(lpNameToReplace, Chr(0)) - 1
'                    '-> R�cup�ration de la cellule
'                    DefCellule = Entry(k, lpNameToReplace, Chr(0))
'                    strCellule = Entry(1, DefCellule, "=")
'                    DefValue = Entry(2, DefCellule, "=")
'                    If strCellule <> "" Then
'                        '-> Selon le type de subsitution
'                        If UCase$(Entry(2, lpSection, "_")) = "NAME" Then '-> On fait la substitution en se basant sur un nom
'                            '-> Remplacer la valeur
'                            aWorkBook.Names(UCase$(strCellule)).RefersToRange.Value = DefValue
'                        Else '-> On fait la substitution en se basant sur nom feuille + adresse
'                            strOnglet = Entry(1, DefValue, "|")
'                            strValue = Entry(2, DefValue, "|")
'                            '-> Modifier la valeur
'                            aWorkBook.Sheets(strOnglet).Range(strCellule).Value = strValue
'                        End If '-> Selon le type de remplacement
'                    End If 'Si strCellule <> ""
'                    DoEvents
'                Next 'Pour tous les remlacements de la section
'            End If 'S'il y a des remplacements
'
'        Case "INSERTION"
'
'            '-> R�cup�ration des informations
'            strData = Trim(GetIniString(lpSection, "DATA", ParamPath, True))
'            strOnglet = Trim(GetIniString(lpSection, "ONGLET", ParamPath, True))
'            strKill = Trim(GetIniString(lpSection, "KILL", ParamPath, True))
'            strCellule = Trim(GetIniString(lpSection, "CELLULE", ParamPath, True))
'            DoEvents
'
'            '-> V�rification des informations
'            If Trim(strData) = "" Then
'                MsgBox "Erreur de param�trage : la cle DATA n'est pas renseign�e. Impossible de trouver le fichier de don�es", vbCritical + vbOKOnly, "Erreur"
'                GoTo EndError2
'            Else
'                '-> V�rifier si le fichier existe
'                If Dir$(strData, vbNormal) = "" Then
'                    MsgBox "Impossible de trouver le fichier de donn�es : " & strData, vbCritical + vbOKOnly, "Erreur"
'                    GoTo EndError2
'                End If
'            End If
'
'            '-> V�rification de l'onglet
'            If Trim(strOnglet) = "" Then
'                MsgBox "Erreur de param�trage : Le nom de l'onglet n'est pas sp�cifi�", vbCritical + vbOKOnly, "Erreur"
'                GoTo EndError2
'            End If
'
'            '-> V�rifier si l'onglet existe
'            FindSheet = False
'            For j = 1 To aWorkBook.Sheets.Count
'                If UCase$(aWorkBook.Sheets(j).Name) = UCase$(strOnglet) Then
'                    '-> Pointer sur la feuille
'                    Set aWorkSheet = aWorkBook.Sheets(j)
'                    aWorkSheet.Activate
'                    '-> Indiquer que l'on a trouv� la feuille
'                    FindSheet = True
'                    '-> Sortir de la boucle
'                    Exit For
'                End If
'            Next
'
'            DoEvents
'
'            '-> Quitter si on n'a pas trouv� l'onglet
'            If Not FindSheet Then
'                MsgBox "Impossible de trouver l'onglet sp�cifi� dans le classeur", vbCritical + vbOKOnly, "Erreur"
'                GoTo EndError2
'            End If
'
'            '-> V�rifier que l'on a sp�cifi� la cellule de d�part
'            If Trim(strCellule) = "" Then
'                MsgBox "Erreur de param�trage : Cellule de d�part non sp�cifi�e", vbCritical + vbOKOnly, "Erreur"
'                GoTo EndError2
'            Else
'                '-> Rechercher si le nom sp�cifi� existe
'                For Each aName In aWorkBook.Names
'                    If Trim(UCase$(aName.Name)) = Trim(UCase$(strCellule)) Then
'                        FindName = True
'                        Exit For
'                    Else
'                        FindName = False
'                    End If
'                Next
'
'                '-> Tester si le nom est OK
'                If Not FindName Then
'                    MsgBox "Impossible de trouver le nom sp�cifi� (" & strCellule & ")dans la cl� cellule", vbCritical + vbOKOnly, "Erreur"
'                    GoTo EndError2
'                End If
'
'                '-> R�cup�rer l'adresse de la cellule
'                StrNameDep = UCase$(Trim(aName.Name))
'                strCellule = Entry(2, aName.RefersToRange.Address, "$") & Entry(3, aName.RefersToRange.Address, "$")
'                CelluleDep = strCellule
'
'            End If
'
'            DoEvents
'
'            '-> Traitement de la suppression
'            If UCase$(strKill) = "OUI" Then
'                boolKill = True
'            Else
'                boolKill = False
'            End If
'
'            '-> Ouverture fichier
'            hdlFile = FreeFile
'            Open strData For Input As #hdlFile
'
'            '-> Initialiser le nombre de lignes
'            j = 0
'
'            '-> Lecture des lignes
'            Do While Not EOF(hdlFile)
'                DoEvents
'                '-> Lecture de la ligne
'                Line Input #hdlFile, Ligne
'                If Trim(Ligne) <> "" Then
'                    FindLigne = True
'                    '-> Incr�menter le compteur de ligne
'                    j = j + 1
'                    '-> Se postionner sur la cellule de d�part si = 1 sinon, cellule de d�part + offset
'                    If j = 1 Then
'                        aWorkSheet.Range(strCellule).Select
'                        nbCol = NumEntries(Ligne, "|")
'                    Else
'                        '-> R�cup�ration de la cellule suivante
'                        strCellule = aWorkSheet.Range(strCellule).Offset(1).Address
'                        '-> S�lectionner toutes les colonnes
'                        aWorkSheet.Range(strCellule & ":" & aWorkSheet.Range(strCellule).Offset(0, nbCol - 1).Address).Select
'
'                        '-> Insertion d'une ligne
'                        ExcelApp.Selection.Insert Shift:=xlDown
'
'                    End If
'                    '-> S�lectionner la cellule active
'                    aWorkSheet.Range(strCellule).Select
'                    '-> Donner une valeur
'                    ExcelApp.ActiveCell.Value = Ligne
'                    '-> R�partition par colonne
'                    ExcelApp.Application.Selection.TextToColumns DataType:=xlDelimited, _
'                        ConsecutiveDelimiter:=False, Other:=True, OtherChar:="|", FieldInfo:=Array(Array(1, xlGeneralFormat), Array(5, xlGeneralFormat))
'                    '-> S�lectionner toute la nouvelle zone
'                    aWorkSheet.Range(CelluleDep & ":" & aWorkSheet.Range(CelluleDep).Offset(j - 1, nbCol - 1).Address).Select
'
'                    '-> Appliquer un format au load g�n�ral
'                    ExcelApp.Selection.NumberFormat = "General"
'
'                End If
'            Loop
'
'            '-> Fermer le fichier
'            Close #hdlFile
'
'            If FindLigne Then
'                ExcelApp.Selection.Borders(xlDiagonalDown).LineStyle = xlNone
'                ExcelApp.Selection.Borders(xlDiagonalUp).LineStyle = xlNone
'                With ExcelApp.Selection.Borders(xlEdgeLeft)
'                    .LineStyle = xlContinuous
'                    '.Weight = xlThin
'                    '.ColorIndex = xlAutomatic
'                End With
'                With ExcelApp.Selection.Borders(xlEdgeTop)
'                    .LineStyle = xlContinuous
'                    '.Weight = xlThin
'                    '.ColorIndex = xlAutomatic
'                End With
'                With ExcelApp.Selection.Borders(xlEdgeBottom)
'                    .LineStyle = xlContinuous
'                    '.Weight = xlThin
'                    '.ColorIndex = xlAutomatic
'                End With
'                With ExcelApp.Selection.Borders(xlEdgeRight)
'                    .LineStyle = xlContinuous
'                    '.Weight = xlThin
'                    '.ColorIndex = xlAutomatic
'                End With
'                With ExcelApp.Selection.Borders(xlInsideVertical)
'                    .LineStyle = xlContinuous
'                    '.Weight = xlThin
'                    '.ColorIndex = xlAutomatic
'                End With
'                With ExcelApp.Selection.Borders(xlInsideHorizontal)
'                    .LineStyle = xlContinuous
'                    '.Weight = xlThin
'                    '.ColorIndex = xlAutomatic
'                End With
'
'                '-> Largeur AUtomatic
'                ExcelApp.Selection.Columns.AutoFit
'                '-> Charger
'                aWorkSheet.Range(CelluleDep).Select
'            End If
'            DoEvents
'            FindLigne = False
'
'    End Select
'
'Next
'
'Exit Sub
'
'
'EndError:
'
'    If Err.Number = 70 Then
'        '-> Cela veut dire que le classeur est en cours d'utilisation : Permission refus�e
'        Rep = MsgBox("Attention : Impossible d'�craser le fichier : " & Chr(13) & DestinationXls & strDestination & Chr(13) & "Le fichier doit �tre en cours d'utilisation. Fermer la session Excel en cours et ressayez.", vbExclamation + vbYesNo, "Avertissement")
'        If Rep = vbYes Then
'            Resume
'        Else
'            GoTo EndError2
'        End If
'    End If
'
'    '-> Avertir l'utilisateur
'    MsgBox "Une erreur impr�vue est survenue lors de la cr�ation du tableau de Bords.", vbCritical + vbOKOnly, "Erreur fatale"
'
'EndError2:
'
'    On Error Resume Next
'
'    '-> Envoyer un message d'erreur de retour
'    hdlFile = FreeFile
'    Open ReturnFile For Output As #hdlFile
'    Print #hdlFile, "ERROR"
'    Close #hdlFile
'
'    '-> Enregistrer le classeur en cours
'    aWorkBook.Close False
'
'    If Dir$(strData) <> "" And boolKill Then Kill strData
'
'    '-> Quitter Excel
'    ExcelApp.Quit
'
'    '-> Liberrer le pointeur sur Excel
'    Set ExcelApp = Nothing
'
'    '-> Supprimer le classeur temporaire
'    If Dir$(SourceXLS & DateTempo) <> "" Then Kill SourceXLS & DateTempo
'
'    '-> Quitter l'application
'    End
'
'
'End Sub
'
'
