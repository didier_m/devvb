VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "shdocvw.dll"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmVisu 
   Caption         =   "Form1"
   ClientHeight    =   3795
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5730
   LinkTopic       =   "Form1"
   ScaleHeight     =   253
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   382
   StartUpPosition =   2  'CenterScreen
   Begin SHDocVwCtl.WebBrowser Web 
      Height          =   1575
      Left            =   4560
      TabIndex        =   2
      Top             =   1800
      Width           =   735
      ExtentX         =   1296
      ExtentY         =   2778
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   "res://C:\WINNT\system32\shdoclc.dll/dnserror.htm#http:///"
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   4920
      Picture         =   "frmVisu.frx":0000
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   1
      ToolTipText     =   "Imprimer le fichier"
      Top             =   120
      Width           =   480
   End
   Begin RichTextLib.RichTextBox RichTextBox1 
      Height          =   2295
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   4048
      _Version        =   393217
      Enabled         =   -1  'True
      ReadOnly        =   -1  'True
      ScrollBars      =   2
      AutoVerbMenu    =   -1  'True
      TextRTF         =   $"frmVisu.frx":0442
   End
End
Attribute VB_Name = "frmVisu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public RetourImp As String

Private Sub Form_Resize()

On Error Resume Next

Dim aRect As RECT

Res = GetClientRect(Me.hwnd, aRect)
Me.RichTextBox1.Left = 0
Me.Web.Left = 0
Me.RichTextBox1.Top = 0
Me.Web.Top = 0
Me.RichTextBox1.Width = aRect.Right
Me.Web.Width = aRect.Right
Me.RichTextBox1.Height = aRect.Bottom
Me.Web.Height = aRect.Bottom
Me.Picture1.Top = Me.Picture1.Height / 2
Me.Picture1.Left = aRect.Right - Me.Picture1.Width * 2

End Sub

Private Sub Picture1_DblClick()

On Error Resume Next

RetourImp = ""
PrintList.ToPrint = -9999
PrintList.Show vbModal

If RetourImp <> "" Then
    Me.RichTextBox1.SelStart = 0
    Me.RichTextBox1.SelLength = Len(Me.RichTextBox1.Text)
    Me.RichTextBox1.SelPrint Printer.hDC
End If

End Sub

